#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sbiap/SBIAP_Server/Base/Server/Interface/excmgr.c 1.2 2005/08/24 17:42:51SGT heb Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program EXCMGR                                                  */
/*                                                                            */
/* Author         :  HEB                                                      */
/* Date           :  20050208                                                 */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */

/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I skeleton.c 44.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>

#ifdef _HPUX_SOURCE
	extern int daylight;
	extern long timezone;
	extern char *tzname[2];
#else
	extern int _daylight;
	extern long _timezone;
	extern char *_tzname[2];
#endif

int debug_level = 0;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */

static long lgEvtCnt = 0;

#define MAX_STATUS_LINES 500
#define MAX_LINES 2000
#define LINE_LEN 97


typedef struct
{
	char clTag[40];
	char clName[40];
	char clValue[1000];
} _LINE;

typedef struct
{
	_LINE rlLine[MAX_LINES];
} _Config;

_Config rgConfig;

FILE *fpStatusFile = NULL;


#define	LINE_POS		0
#define TYPE_POS		5
#define R_POS			10
#define S_POS			12
#define PROC_POS		14
#define STEP_POS		19
#define TASK_POS		24
#define PREF_POS		29
#define SREF_POS		34
#define NTSK_POS		39
#define TRIG_KEY_POS	44
#define ACK_POS			61
#define TIOUT_POS		65
#define PCMD_POS		71
#define SENT_POS		77
#define ACK_ID_POS		82



static char cgFreeList[512] = "\0";
static char cgBlankLine[100] = "...........................................................................................";
static int igSFOffset = 0; 
static int igMaxConfigLine = 0;
static int igPrevStatTaskLine = 0;
static char cgDebugLevel[20] = "\0";
static char cgWhere[1024] = "\0";


/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
			   char *pcpCfgBuffer);


static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/


static int	HandleData(EVENT *prpEvent);       /* Handles event data     */
static int  GetCommand(char *pcpCommand, int *pipCmd);
static void TrimRight(char *pcpBuffer);
static int  getSpecialConfig();
static void CheckTimeout();
static void HandleCommand(char *pcpSelection);
static void HandleProcedure(int ipProcLine, char *pcpSelection);
static int  HandleStep(int ipStepLine, int ipProcLine);
static void HandleTask(int ipTaskLine, int ipStatStepLine);
static void HandleAcknowledge(char *pcpTws, char *pcpFileName);
static int  CreateStatusFile();
static int  GetStatLine(char *pcpBuffer, char cpSearchChar);
static void WriteStatItem(int ipLine, int ipPos, char *pcpValue);
static void ReadStatusLine (char *pcpStatLine, int ipLine);
static void RemoveStep(int ipStatStepLine, int pblContinue);
static void ReadStatItem(int ipLine, int ipPos, int ipNumBytes, char *pcpValue);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int ilOldDebugLevel = 0;


	INITIALIZE;			/* General initialization	*/



	debug_level = DEBUG;

	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",sccs_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
			ilRc = Init_Process();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Init_Process: init failed!");
			} /* end of if */

	} else {
		Terminate(30);
	}/* end of if */

	if(strcmp(cgDebugLevel,"DEBUG") == 0)
	{
		debug_level = DEBUG;
	}
	else
	{
		if(strcmp(cgDebugLevel,"TRACE") == 0)
		{
			debug_level = TRACE;
		}
		else
		{
			if(strcmp(cgDebugLevel,"OFF") == 0)
			{
				debug_level = 0;
			}
		}
	}

	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");
	

	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* Acknowledge the item */
		ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
		if( ilRc != RC_SUCCESS ) 
		{
			/* handle que_ack error */
			HandleQueErr(ilRc);
		} /* fi */

		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */

		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(1);
				break;
					
			case	RESET		:
				ilRc = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
				

			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */

		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/



static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Process()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
    char clSection[64] = "\0";
	char clKeyword[64] = "\0";
	int ilOldDebugLevel = 0;
	long pclAddFieldLens[12];
	char pclAddFields[256] = "\0";
	char pclSqlBuf[2560] = "\0";
	char pclSelection[1024] = "\0";
 	short slCursor = 0;
	short slSqlFunc = 0;
	char  clBreak[24] = "\0";


	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
		}
	}

	
	if(ilRc == RC_SUCCESS)
	{

		ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
		}
	}

	ilRc = ReadConfigEntry("MAIN", "DEBUG_LEVEL", cgDebugLevel);

	if(ilRc == RC_SUCCESS)
	{
			ilRc = getSpecialConfig();
	}
	if(ilRc == RC_SUCCESS)
	{
			ilRc = CreateStatusFile();
	}



	if (ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"Init_Process failed");
	}



		return(ilRc);
	
} /* end of initialize */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);
	fclose(fpStatusFile);


	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{

	default	:
		Terminate(1);
		break;
	} /* end of switch */

	exit(1);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

			ilRc = Init_Process();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemhdl: init failed!");
			} 
     

} /* end of HandleQueues */
	



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      ilCmd          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRow          = NULL;
	char 		clUrnoList[2400];
	char 		clTable[34];
	int			ilUpdPoolJob = TRUE;
	char clTemp[100] = "\0";
	char clWhere[1024] = "";

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;


	strcpy(clTable,prlCmdblk->obj_name);

	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

	/****************************************/
	DebugPrintBchead(TRACE,prlBchead);
	DebugPrintCmdblk(TRACE,prlCmdblk);
	dbg(TRACE,"Command: <%s>",prlCmdblk->command);
	dbg(TRACE,"originator follows event = %p ",prpEvent);

	dbg(TRACE,"originator<%d>",prpEvent->originator);
	dbg(TRACE,"selection follows Selection = %p ",pclSelection);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	/****************************************/
	cgWhere[0] = '\0';

	if(strcmp(prlCmdblk->command,"TRIG")==0)
	{
		CheckTimeout();
	}
	else
	{
		/*to be removed when ntisch will be able to send SELECTION*/
		if(prpEvent->originator == 7850)
		{
			ilRc = ReadConfigEntry("CMD_MAPPING", prlCmdblk->command, pclSelection);
			if (ilRc == RC_SUCCESS)
			{
				dbg(TRACE,"%05d: mapped selection: <%s>",__LINE__,pclSelection);
				HandleCommand(pclSelection);
			}
		}
		if(prpEvent->originator == 7800)
		{
			strcpy(cgWhere,pclSelection);
			ilRc = ReadConfigEntry("CMD_MAPPING", prlCmdblk->command, pclSelection);
			if (ilRc == RC_SUCCESS)
			{
				dbg(TRACE,"%05d: mapped selection: <%s>",__LINE__,pclSelection);
				dbg(TRACE,"%05d: *WHERE*: <%s>",__LINE__,cgWhere);
				HandleCommand(pclSelection);
			}
		}
			if(strcmp(prlCmdblk->command,"PCC")==0)
			{
				HandleCommand(pclSelection);
			}
			else
			{
				HandleAcknowledge(prlCmdblk->tw_start,pclSelection);
			}
	}



	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

	return(RC_SUCCESS);
	
} /* end of HandleData */

static void TrimRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
/*
  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer, " ");
  }
  else
  {
     while (isspace(*pclBlank) && pclBlank != pcpBuffer)
     {
        *pclBlank = '\0';
        pclBlank--;
     }
  }
  */
     while (isspace(*pclBlank) && pclBlank != pcpBuffer)
     {
        *pclBlank = '\0';
        pclBlank--;
     }

} /* End of TrimRight */



static int getSpecialConfig()
{
	char clNameStack[100][32];
	char clCfgFile[100];
	FILE *fp;
	char clLine[100] = "\0";
	char clTag[40] = "\0";
	char clName[40] = "\0";
	char clValue[1000] = "\0";
	int ilNoEle = 0;
	int ilCurrLine = 0;
	int ilCurrLevel = 0;

	char clPrevTag[40] = "\0";

	sprintf(clCfgFile,"%s/%s.csv",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"%05d: Special Config File is <%s>",__LINE__,clCfgFile);
	if ((fp = (FILE *)fopen(clCfgFile,"r")) == (FILE *)NULL)
	{
		dbg(TRACE,"%05d: Special Config File <%s> does not exist",__LINE__,clCfgFile);
		return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"%05d: Special Config File <%s> opend",__LINE__,clCfgFile);
	}


	memset((char *)&rgConfig.rlLine[0].clTag[0],0x00,sizeof(_LINE)*MAX_LINES);

	while (fgets(clLine,100,fp))
	{
		clLine[strlen(clLine)-1] = '\0';
		ilNoEle = GetNoOfElements(clLine,';');
		dbg(DEBUG,"%05d: Current Line = (%d)<%s>",__LINE__,ilNoEle,clLine);
		get_item(1,clLine,clTag,0,";","\0","\0");
		TrimRight(clTag);
		get_item(2,clLine,clName,0,";","\0","\0");
		TrimRight(clTag);
		get_item(3,clLine,clValue,0,";","\0","\0");
		TrimRight(clTag);

		strcpy(rgConfig.rlLine[ilCurrLine].clTag,clTag);
		strcpy(rgConfig.rlLine[ilCurrLine].clName,clName);
		strcpy(rgConfig.rlLine[ilCurrLine].clValue,clValue);

		ilCurrLine++;
		igMaxConfigLine++;
	}

	for(ilCurrLine = 0; ilCurrLine < igMaxConfigLine; ilCurrLine++)
	{
		dbg(DEBUG,"%05d: rgConfig.rlLine[%d] <%s,%s,%s>",__LINE__,ilCurrLine,
											rgConfig.rlLine[ilCurrLine].clTag,
											rgConfig.rlLine[ilCurrLine].clName,
											rgConfig.rlLine[ilCurrLine].clValue);
	}
	fclose(fp);

	return RC_SUCCESS;
}

static int CreateStatusFile()
{
	char clStatusFile[100] = "\0";
	int ilRc = RC_SUCCESS;
	int ilNB = 0;
	int ili = 0;
	int blNewFile = TRUE;
	char clTemp[20] = "\0";

	dbg(TRACE,"%05d: --------------------- CreateStatusFile Start ----------------------",__LINE__);
	sprintf(clStatusFile,"%s/%s.ctl",getenv("DBG_PATH"),mod_name);
	fpStatusFile = fopen(clStatusFile,"r+");
	if(fpStatusFile == NULL)
	{
		dbg(TRACE,"%05d: StatusFile <%s> could not be opened, create new one",__LINE__,clStatusFile);
		fpStatusFile = fopen(clStatusFile,"w+");
/*		fclose(fpStatusFile);
		fpStatusFile = fopen(clStatusFile,"r+");*/
		blNewFile = TRUE;
	}
	else
	{
		dbg(TRACE,"%05d: StatusFile <%s> opened",__LINE__,clStatusFile);
		ilRc = fseek(fpStatusFile,MAX_STATUS_LINES+1,SEEK_SET);
		ilRc = fread(clTemp,1,10,fpStatusFile);
		clTemp[10] = '\0';
		igSFOffset = atoi(clTemp);
		blNewFile = FALSE;
	}
	if(blNewFile == TRUE)
	{
		memset(cgFreeList,'.',MAX_STATUS_LINES-1);
		cgFreeList[MAX_STATUS_LINES -1] = '\n';
		cgFreeList[MAX_STATUS_LINES] = '\0';
		cgFreeList[0] = 'L';
		ilNB = fseek(fpStatusFile,0,SEEK_SET);
		ilNB = fprintf(fpStatusFile,cgFreeList);
		ilNB = fprintf(fpStatusFile,"\n             !!!!!!!! NEVER SAVE THIS FILE WHEN OPEND WITH EDITOR !!!!!!!\n");
		ilNB = fprintf(fpStatusFile,"\n\n================================================================================================\n");
		ilNB = fprintf(fpStatusFile,"-- RUNTIME: TRIGGERED PROCEDURES / STARTED STEPS / INVOKED TASKS -------------------------------\n");
		ilNB = fprintf(fpStatusFile,"------------------------------------------------------------------------------------------------\n");
		ilNB = fprintf(fpStatusFile,"LINE.TYPE.R.S.PROC.STEP.TASK.PREF.SREF.NTSK.TRIGGER-KEYWORDS.ACK.TIOUT.PCMD..SENT.ACK-ID........\n");
		ilNB = fprintf(fpStatusFile,"====.====.=.=.====.====.====.====.====.====.================.===.=====.=====.====.==============\n");
		ilRc = fflush(fpStatusFile);
		igSFOffset = ftell(fpStatusFile);
		sprintf(clTemp,"%010d",igSFOffset);
		ilRc = fseek(fpStatusFile,MAX_STATUS_LINES+1,SEEK_SET);
		ilRc = fprintf(fpStatusFile,clTemp);
		ilRc = fflush(fpStatusFile);
		ilRc = fseek(fpStatusFile,igSFOffset,SEEK_SET);

		for(ili = 1 ; ili <= MAX_STATUS_LINES ; ili++)
		{
			ilNB = fprintf(fpStatusFile,"%04d.%s\n",ili,cgBlankLine);
		}
		ilRc = fflush(fpStatusFile);
	}

	dbg(TRACE,"%05d: --------------------- CreateStatusFile End   ----------------------",__LINE__);
	return RC_SUCCESS;
} /*end of CreateStatusFile*/


static void CheckTimeout()
{
	char clFreeList[512] = "\0";
	int ilRc = 0;
	int ili = 0;
	char clTemp[10] = "\0";
	int ilTimeOut = 0;
	int ilConfStepLine = 0;


	dbg(TRACE,"%05d: --------------------- CheckTimeout Start ----------------------",__LINE__);
	ilRc = fseek(fpStatusFile,0,SEEK_SET);
	ilRc = fread(clFreeList,1,500,fpStatusFile);
	clFreeList[500] = '\0';

	for(ili=1; ili < 500; ili++)
	{
		if(clFreeList[ili] == 'S')
		{
			ReadStatItem(ili, STEP_POS, 4, clTemp);
			ilConfStepLine = atoi(clTemp);
			ReadStatItem(ili, TIOUT_POS, 5, clTemp);
			ilTimeOut = atoi(clTemp);
			ilTimeOut--;
			if(ilTimeOut == 0)
			{
				dbg(TRACE,"%05d: TimeOut occured for Step in Configline <%d>",__LINE__,ilConfStepLine);
				RemoveStep(ili, FALSE); /*FALSE= no further step of this procedure will be processed*/
			}
			else
			{
				sprintf(clTemp,"%05d",ilTimeOut);
				WriteStatItem(ili, TIOUT_POS, clTemp);
			}
		}
	}



	dbg(TRACE,"%05d: --------------------- CheckTimeout End   ----------------------",__LINE__);
} /*end of CheckTimeOut*/

static void HandleCommand(char *pcpSelection)
{
	int ilCurrLine = 0;
	int ilCommandLine = 0;
	int blLineFound = FALSE;
	int ilLevel = 0;

	dbg(TRACE,"%05d: --------------------- HandleCommand Start ----------------------",__LINE__);
	dbg(TRACE,"%05d: CMDS_BGN <%s> will be processd",__LINE__,pcpSelection);
	
	/* find first occurance of CMDS_BGN combined with the received command/procedure*/
	for (ilCurrLine = 0; (ilCurrLine <= igMaxConfigLine) && (blLineFound == FALSE); ilCurrLine++)
	{
		if ((strcmp(rgConfig.rlLine[ilCurrLine].clName,pcpSelection) == 0) && 
			 (strcmp(rgConfig.rlLine[ilCurrLine].clTag,"CMDS_BGN") == 0))
		{
			ilCommandLine = ilCurrLine;
			blLineFound = TRUE;
			dbg(TRACE,"%05d: CMDS_BGN <%s> found in Line: <%d>",__LINE__,pcpSelection,ilCurrLine);
			ilLevel++;
		}
	}
	if (blLineFound == FALSE)
	{
		dbg(TRACE,"%05d: CMDS_BGN <%s> NOT FOUND !!! in configuration ",__LINE__,pcpSelection);
		dbg(TRACE,"%05d: --------------------- HandleCommand End   ----------------------",__LINE__);
		return;
	}


	/* if the received command is marked as valid, process all procedures for this command*/
	if((strcmp(rgConfig.rlLine[ilCurrLine].clValue,"YES") == 0) || (strcmp(rgConfig.rlLine[ilCurrLine].clValue,"yes") == 0))
	{
        while (strcmp(rgConfig.rlLine[ilCurrLine].clTag,"CMDS_END") != 0)
		{
			ilCurrLine++;
			if(strcmp(rgConfig.rlLine[ilCurrLine].clTag,"PROC_BGN") == 0)
			{
				HandleProcedure(ilCurrLine,pcpSelection);
				ilCurrLine++;
			}
		}
	}
	else
	{
		dbg(TRACE,"%05d: CMDS_BGN <%s> NOT VALID in configuration",__LINE__,pcpSelection);	
	}


	dbg(TRACE,"%05d: --------------------- HandleCommand End   ----------------------",__LINE__);
} /*end of HandleCommand*/

static void HandleProcedure(int ipProcLine, char *pcpSelection)
{
	/*int ilCurrLine = 0;*/
	int ilCurrLine = ipProcLine;
	int blLineFound = FALSE;
	char clFreeList[512] = "\0";
	int ilTemp = 0;
	int ilFreeLine = 0;
	int ilRc = RC_SUCCESS;
    char clTemp[50] = "\0";
	int blStepAck = FALSE;
	int ilStatStepLine = 0;


	dbg(TRACE,"%05d: --------------------- HandleProcedure Start ----------------------",__LINE__);
	dbg(TRACE,"%05d: PROC_BGN <%s> will be processd",__LINE__,rgConfig.rlLine[ipProcLine].clName);
	
	/*find free statusline in file and mark as procedure*/
	ilTemp = fseek(fpStatusFile,0,SEEK_SET);
	ilTemp = fread(clFreeList,1,500,fpStatusFile);
	dbg(DEBUG,"%05d: FreeList <%s>",__LINE__,clFreeList);
	ilFreeLine = GetStatLine(clFreeList,'.');
	dbg(DEBUG,"%05d: Free Status Line found at <%d>",__LINE__,ilFreeLine);
	ilRc = fseek(fpStatusFile,ilFreeLine,SEEK_SET);
	ilRc = fprintf(fpStatusFile,"P");
	ilRc = fflush(fpStatusFile);

	/*write StatusLineItems*/
	WriteStatItem(ilFreeLine, TYPE_POS,"PROC");
	WriteStatItem(ilFreeLine, R_POS,"Y");
	sprintf(clTemp,"%04d",ipProcLine);
	WriteStatItem(ilFreeLine, PROC_POS, clTemp);
	strcpy(clTemp,pcpSelection);
	clTemp[16] = '\0';
	WriteStatItem(ilFreeLine, TRIG_KEY_POS, clTemp);



	while ((strcmp(rgConfig.rlLine[ilCurrLine].clTag,"PROC_END") != 0) && (blStepAck == FALSE))
	{
		ilCurrLine++;
		if(strcmp(rgConfig.rlLine[ilCurrLine].clTag,"STEP_BGN") == 0)
		{
			ilStatStepLine = HandleStep(ilCurrLine, ilFreeLine);
			ReadStatItem(ilStatStepLine, ACK_POS, 3, clTemp);
			if(atoi(clTemp) == 0)
			{
				RemoveStep(ilStatStepLine, FALSE);
			}
			else
			{
				blStepAck = TRUE;	
			}
			ilCurrLine++;
		}
	}

	

	dbg(TRACE,"%05d: --------------------- HandleProcedure End   ----------------------",__LINE__);
} /*end of HandleProcedure*/



static void WriteStatItem(int ipLine, int ipPos, char *pcpValue)
{	
	int ilRc = 0;
	dbg(DEBUG,"%05d: go to Statusline <%d>, Position <%d>, write <%s>",__LINE__,ipLine,ipPos,pcpValue);
	ilRc = fseek(fpStatusFile,igSFOffset+(ipLine-1)*LINE_LEN+ipPos,SEEK_SET);
	ilRc = fprintf(fpStatusFile,pcpValue);
	ilRc = fflush(fpStatusFile);
	return;
}

static void ReadStatItem(int ipLine, int ipPos, int ipNumBytes, char *pcpValue)
{	
	int ilRc = 0;
	dbg(DEBUG,"%05d: Read at Statusline <%d>, Position <%d>",__LINE__,ipLine,ipPos);
	ilRc = fseek(fpStatusFile,igSFOffset+(ipLine-1)*LINE_LEN+ipPos,SEEK_SET);
	ilRc = fread(pcpValue,1,ipNumBytes,fpStatusFile);
	pcpValue[ipNumBytes] = '\0';
	return;
}

static int GetStatLine(char *pcpBuffer, char cpSearchChar)
{
	int ilCurrChar = 0;

	while(pcpBuffer[ilCurrChar] != cpSearchChar)
	{
		ilCurrChar++;	
	}

	return ilCurrChar;
}


static int HandleStep(int ipStepLine, int ipStatProcLine)
{
	int ilCurrLine = 0;
	int blLineFound = FALSE;
	int ilTemp = 0;
	int ilFreeLine = 0;
	int ilRc = RC_SUCCESS;
    char clTemp[100] = "\0";
	char clStatLine[100] = "\0";
	char *pclTemp;
	char clFreeList[512] = "\0";
	pclTemp = &clStatLine[0];
	igPrevStatTaskLine = 0;

	dbg(TRACE,"%05d: --------------------- HandleStep Start ----------------------",__LINE__);
	dbg(TRACE,"%05d: STEP_BGN <%s> will be processd",__LINE__,rgConfig.rlLine[ipStepLine].clName);
	ilCurrLine = ipStepLine;
	ReadStatusLine(clStatLine, ipStatProcLine);
	dbg(DEBUG,"%05d: ProcLine: <%s>",__LINE__,clStatLine);

	/*find free statusline in file and mark as procedure*/
	ilTemp = fseek(fpStatusFile,0,SEEK_SET);
	ilTemp = fread(clFreeList,1,500,fpStatusFile);
	dbg(DEBUG,"%05d: FreeList <%s>",__LINE__,clFreeList);
	ilFreeLine = GetStatLine(clFreeList,'.');
	dbg(DEBUG,"%05d: Free Status Line found at <%d>",__LINE__,ilFreeLine);
	ilRc = fseek(fpStatusFile,ilFreeLine,SEEK_SET);
	ilRc = fprintf(fpStatusFile,"S");
	ilRc = fflush(fpStatusFile);

	/*write StatusLineItems*/
	WriteStatItem(ilFreeLine, TYPE_POS,"STEP");
	WriteStatItem(ilFreeLine, R_POS,"Y");
	memset(clTemp,'\0',100);
	pclTemp = &clStatLine[PROC_POS];
	strncpy(clTemp,pclTemp,4);
	clTemp[16] = '\0';
	WriteStatItem(ilFreeLine, PROC_POS, clTemp);
	memset(clTemp,'\0',100);
	pclTemp = &clStatLine[TRIG_KEY_POS];
	strncpy(clTemp,pclTemp,16);
	clTemp[16] = '\0';
	WriteStatItem(ilFreeLine, TRIG_KEY_POS, clTemp);
	memset(clTemp,'\0',100);
	sprintf(clTemp,"%04d",ipStepLine);
	WriteStatItem(ilFreeLine, STEP_POS, clTemp);
	memset(clTemp,'\0',100);
	pclTemp = &clStatLine[LINE_POS];
	strncpy(clTemp,pclTemp,4);
	clTemp[16] = '\0';
	WriteStatItem(ilFreeLine, PREF_POS, clTemp);
	memset(clTemp,'\0',100);
	ilTemp = atoi(rgConfig.rlLine[ipStepLine+1].clValue);
	if(ilTemp > 0)
	{
		sprintf(clTemp,"%05d",ilTemp);
		WriteStatItem(ilFreeLine, TIOUT_POS, clTemp);
	}




	while (strcmp(rgConfig.rlLine[ilCurrLine].clTag,"STEP_END") != 0)
	{
		ilCurrLine++;
		if(strcmp(rgConfig.rlLine[ilCurrLine].clTag,"TASK_BGN") == 0)
		{
			HandleTask(ilCurrLine, ilFreeLine);
			ilCurrLine++;
		}
	}


	dbg(TRACE,"%05d: --------------------- HandleStep End   ----------------------",__LINE__);

	return ilFreeLine;
} /*end of HandleStep*/


static void ReadStatusLine (char *pcpStatLine, int ipLine)
{
	int ilRc = 0;

	ilRc = fseek(fpStatusFile,igSFOffset+(ipLine-1)*LINE_LEN,SEEK_SET);
	ilRc = fread(pcpStatLine,1,LINE_LEN,fpStatusFile);

	return;
}


static void HandleTask(int ipTaskLine, int ipStatStepLine)
{
	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char clCurrTime[20] = "\0";
	int ilCurrLine = 0;
	int blLineFound = FALSE;
	char clFreeList[512] = "\0";
	char *pclTemp;
	char clStatLine[100] = "\0";
	char clTemp[100] = "\0";
	int ilTemp = 0;
	int ilFreeLine = 0;
	int ilRc = 0;
	char clType[1000];
	char clWait_Ack[1000] = "\0";
	char clDest[1000] = "\0";
	char clCmd[1000] = "\0";
	char clTab[1000] = "\0";
	char clSel[1000] = "\0";
	char clFld[1000] = "\0";
	char clDat[1000] = "\0";
	char clPrio[10] = "\0";
	char clAck_Id[16]  = "\0";
	char clTws[40] = "\0";
	char clBigTemp[5000] = "\0";
	char clPid[24];
	char clProc[10];
	char clFileName[300] = "\0";
	int ili = 0;

	pid_t plChild = 0;
	int ilCPid = 0;

	pclTemp = &clStatLine[0];
	dbg(TRACE,"%05d: --------------------- HandleTask Start ----------------------",__LINE__);
	dbg(TRACE,"%05d: TASK_BGN <%s> will be processed",__LINE__,rgConfig.rlLine[ipTaskLine].clName);
	
	ilCurrLine = ipTaskLine;
	ReadStatusLine(clStatLine, ipStatStepLine);
	dbg(DEBUG,"%05d: StepLine: <%s>",__LINE__,clStatLine);

	/*find free statusline in file and mark as procedure*/
	ilTemp = fseek(fpStatusFile,0,SEEK_SET);
	ilTemp = fread(clFreeList,1,500,fpStatusFile);
	dbg(DEBUG,"%05d: FreeList <%s>",__LINE__,clFreeList);
	ilFreeLine = GetStatLine(clFreeList,'.');
	dbg(DEBUG,"%05d: Free Status Line found at <%d>",__LINE__,ilFreeLine);

	ilRc = fseek(fpStatusFile,ilFreeLine,SEEK_SET);
	ilRc = fprintf(fpStatusFile,"T");
	ilRc = fflush(fpStatusFile);

	/*write StatusLineItems*/
	WriteStatItem(ilFreeLine, TYPE_POS,"TASK");
	pclTemp =&clStatLine[R_POS];
	/*50 char von statline nach R_POS kopieren, da sowieso gleich*/
	memset(clTemp,'\0',100);
	strncpy(clTemp,pclTemp,50);
	WriteStatItem(ilFreeLine, R_POS,clTemp);
	WriteStatItem(ilFreeLine, ACK_POS, "...");
	memset(clTemp,'\0',100);
	sprintf(clTemp,"%04d",ipTaskLine);
	WriteStatItem(ilFreeLine, TASK_POS, clTemp);
	WriteStatItem(ilFreeLine, NTSK_POS, "....");
	pclTemp = &clStatLine[LINE_POS];
	strncpy(clTemp,pclTemp,4);
	clTemp[16] = '\0';
	WriteStatItem(ilFreeLine, SREF_POS, clTemp);
	if(igPrevStatTaskLine > 0)
	{
		memset(clTemp,'\0',100);
		sprintf(clTemp,"%04d",ilFreeLine);
		WriteStatItem(igPrevStatTaskLine, NTSK_POS, clTemp);
	}
	else
	{
		if(igPrevStatTaskLine == 0)
		{
			memset(clTemp,'\0',100);
			sprintf(clTemp,"%04d",ilFreeLine);
			WriteStatItem(ipStatStepLine, NTSK_POS, clTemp);
		}
	}

	ilRc = fseek(fpStatusFile,igSFOffset+(ilFreeLine-1)*LINE_LEN+PROC_POS ,SEEK_SET);
	ilRc = fread(clProc,1,4,fpStatusFile);

	for(ili = atoi(clProc); strcmp(rgConfig.rlLine[ili].clTag,"STEP_BGN") != 0 ; ili++)
	{
		if(strcmp(rgConfig.rlLine[ili].clName,"DEF-FILE") == 0)
		{
			strcpy(clFileName,rgConfig.rlLine[ili].clValue);
		}
	}

	/* now process TASK-items */
	ilCurrLine = ipTaskLine;
	while(strcmp(rgConfig.rlLine[ilCurrLine].clTag,"TASK_END") != 0)
	{
		if(strcmp(rgConfig.rlLine[ilCurrLine].clName,"TYPE") == 0)
		{
			strcpy(clType, rgConfig.rlLine[ilCurrLine].clValue);
			TrimRight(clType);
		}
		else
		{
			if(strcmp(rgConfig.rlLine[ilCurrLine].clName,"WAIT-ACK") == 0)
			{
				strcpy(clWait_Ack, rgConfig.rlLine[ilCurrLine].clValue);
				TrimRight(clWait_Ack);
			}
			else
			{
				if(strcmp(rgConfig.rlLine[ilCurrLine].clName,"DEST") == 0)
				{
					strcpy(clDest, rgConfig.rlLine[ilCurrLine].clValue);
					TrimRight(clDest);
					if(strcmp(clDest,"*DEF-FILE*") == 0)
					{
						strcpy(clDest,clFileName);
					}
					if(strcmp(clDest,"*WHERE*") == 0)
					{
						strcpy(clDest,cgWhere);
					}

				}
				else
				{
					if(strcmp(rgConfig.rlLine[ilCurrLine].clName,"CMD") == 0)
					{
						strcpy(clCmd, rgConfig.rlLine[ilCurrLine].clValue);
						TrimRight(clCmd);
						if(strcmp(clCmd,"*DEF-FILE*") == 0)
						{
							strcpy(clCmd,clFileName);
						}
						if(strcmp(clCmd,"*WHERE*") == 0)
						{
							strcpy(clCmd,cgWhere);
						}
					}
					else
					{
						if(strcmp(rgConfig.rlLine[ilCurrLine].clName,"TAB") == 0)
						{
							strcpy(clTab, rgConfig.rlLine[ilCurrLine].clValue);
							TrimRight(clTab);
							if(strcmp(clTab,"*DEF-FILE*") == 0)
							{
								strcpy(clTab,clFileName);
							}
							if(strcmp(clTab,"*WHERE*") == 0)
							{
								strcpy(clTab,cgWhere);
							}
						}
						else
						{
							if(strcmp(rgConfig.rlLine[ilCurrLine].clName,"SEL") == 0)
							{
								strcpy(clSel, rgConfig.rlLine[ilCurrLine].clValue);
								TrimRight(clSel);
								if(strcmp(clSel,"*DEF-FILE*") == 0)
								{
									strcpy(clSel,clFileName);
								}
								if(strcmp(clSel,"*WHERE*") == 0)
								{
									strcpy(clSel,cgWhere);
								}
							}
							else
							{
								if(strcmp(rgConfig.rlLine[ilCurrLine].clName,"FLD") == 0)
								{
									strcpy(clFld, rgConfig.rlLine[ilCurrLine].clValue);
									TrimRight(clFld);
									if(strcmp(clFld,"*DEF-FILE*") == 0)
									{
										strcpy(clFld,clFileName);
									}
									if(strcmp(clFld,"*WHERE*") == 0)
									{
										strcpy(clFld,cgWhere);
									}
								}
								else
								{
									if(strcmp(rgConfig.rlLine[ilCurrLine].clName,"DAT") == 0)
									{
										strcpy(clDat, rgConfig.rlLine[ilCurrLine].clValue);
										TrimRight(clDat);
										if(strcmp(clDat,"*DEF-FILE*") == 0)
										{
											strcpy(clDat,clFileName);
										}
										if(strcmp(clDat,"*WHERE*") == 0)
										{
											strcpy(clDat,cgWhere);
										}
									}
									else
									{
										if(strcmp(rgConfig.rlLine[ilCurrLine].clName,"PRIO") == 0)
										{
											strcpy(clPrio, rgConfig.rlLine[ilCurrLine].clValue);
											TrimRight(clPrio);
											if(strcmp(clPrio,"*DEF-FILE*") == 0)
											{
												strcpy(clPrio,clFileName);
											}
											if(strcmp(clPrio,"*WHERE*") == 0)
											{
												strcpy(clPrio,cgWhere);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		ilCurrLine++;
	}

	if ((strcmp(clWait_Ack,"YES")==0) || (strcmp(clWait_Ack,"yes")==0) || (strcmp(clWait_Ack,"y")==0) || (strcmp(clWait_Ack,"Y")==0))
	{
		WriteStatItem(ilFreeLine, ACK_POS, "Y");
		memset(clTemp,'\0',100);
		pclTemp = &clStatLine[ACK_POS];
		strncpy(clTemp,pclTemp,3);
		clTemp[3] = '\0';
		ilTemp = atoi(clTemp);
		ilTemp++;
		sprintf(clTemp,"%03d",ilTemp);
		WriteStatItem(ipStatStepLine, ACK_POS, clTemp);
		GetServerTimeStamp("UTC", 1, 0, clCurrTime);
		WriteStatItem(ilFreeLine, ACK_ID_POS, clCurrTime);
		sprintf(clTws,"%04d,%s",ilFreeLine,clCurrTime);
	}
	else
	{
		strcpy(clTws,"NETOUT_NO_ACK");
		WriteStatItem(ilFreeLine, R_POS, "N");
	}
	
	if (strcmp(clType,"SYS") == 0)
	{
		strcpy(clTemp,"SCRI");
		WriteStatItem(ilFreeLine, PCMD_POS, clTemp);
		sprintf(clBigTemp,"nohup %s %s %s %s %s %s %s &",clDest,clCmd,clTws,clTab,clSel,clFld,clDat);
			
		dbg(TRACE,"%05d: now start Script: <%s>",__LINE__, clBigTemp);
		ilRc = system(clBigTemp);
	}
	else
	{
		strcpy(clTemp,clCmd);
		clTemp[5] = '\0';
		WriteStatItem(ilFreeLine, PCMD_POS, clTemp);
		strcpy(clTemp,clDest);
		clTemp[4] = '\0';
		WriteStatItem(ilFreeLine, SENT_POS, clTemp);


		dbg(TRACE,"%05d: now SendCedaEvent:\nclDest:<%d>\nOriginator:<%d>\nclTws<%s>\nclCmd:<%s>\nclTab:<%s>\nclSel:<%s>\nclFld:<%s>\nclDat:<%s>\n",
			__LINE__,atoi(clDest),mod_id,clTws,clCmd, clTab, clSel, clFld, clDat);
		SendCedaEvent (atoi(clDest), mod_id, "", "",clTws, "", clCmd, clTab, clSel, clFld, clDat, " ", atoi(clPrio), RC_SUCCESS);
	}


	igPrevStatTaskLine = ilFreeLine;
	dbg(TRACE,"%05d: --------------------- HandleTask End   ----------------------",__LINE__);
} /*end of HandleTask*/



static void HandleAcknowledge(char *pcpTws, char *pcpFileName)
{
	char clAckLine[10] = "\0";
	int ilAckLine = 0;
	char clAckValue[40] = "\0";
	int ilRc = RC_SUCCESS;
	char clAckValueInFile[40] = "\0";
	char clSref[6] = "\0";
	char clAck[10] = "\0";
	char clProc[10] = "\0";
	int ili = 0;
	int ilAck = 0;
	int ilSref = 0;
	


	dbg(TRACE,"%05d: --------------------- HandleAcknowlege Start ----------------------",__LINE__);
	dbg(TRACE,"%05d: Acknowlede-ID <%s> will be processd",__LINE__,pcpTws);
	


	get_item(1,pcpTws,clAckLine,0,",","\0","\0");
	get_item(2,pcpTws,clAckValue,0,",","\0","\0");
	dbg(TRACE,"%05d: AckLine  <%s> ",__LINE__,clAckLine);
	dbg(TRACE,"%05d: AckValue <%s> ",__LINE__,clAckValue);

	ilAckLine = atoi(clAckLine);	
	ilRc = fseek(fpStatusFile,igSFOffset+(ilAckLine-1)*LINE_LEN+ACK_ID_POS ,SEEK_SET);
	ilRc = fread(clAckValueInFile,1,14,fpStatusFile);
	
	ilRc = fseek(fpStatusFile,igSFOffset+(ilAckLine-1)*LINE_LEN+PROC_POS ,SEEK_SET);
	ilRc = fread(clProc,1,4,fpStatusFile);
	

	if(strcmp(clAckValueInFile,clAckValue) == 0)
	{
		if(strlen(pcpFileName) > 0)
		{
			for(ili = atoi(clProc); strcmp(rgConfig.rlLine[ili].clTag,"STEP_BGN") != 0 ; ili++)
			{
				if(strcmp(rgConfig.rlLine[ili].clName,"DEF-FILE") == 0)
				{
					strcpy(rgConfig.rlLine[ili].clValue,pcpFileName);
				}
			}
		}
		WriteStatItem(ilAckLine, ACK_ID_POS, "..............");
		WriteStatItem(ilAckLine, ACK_POS+2, "A");
		ilRc = fseek(fpStatusFile,igSFOffset+(ilAckLine-1)*LINE_LEN+SREF_POS ,SEEK_SET);
		ilRc = fread(clSref,1,4,fpStatusFile);
		ilSref = atoi(clSref);
		ilRc = fseek(fpStatusFile,igSFOffset+(ilSref-1)*LINE_LEN+ACK_POS ,SEEK_SET);
		ilRc = fread(clAck,1,4,fpStatusFile);
		ilAck = atoi(clAck);
		ilAck--;
		if(ilAck == 0)
		{
			RemoveStep(ilSref, TRUE);
		}
		else
		{
			sprintf(clAck,"%03d",ilAck);
			WriteStatItem(ilSref,ACK_POS,clAck);
      	}

	}
	else
	{
		dbg(TRACE,"%05d: Unknown ACKNOWLEDE received <%s>",__LINE__,pcpTws);
	}




	dbg(TRACE,"%05d: --------------------- HandleAcknowlege End   ----------------------",__LINE__);
} /*end of HandleAcknowledge */

static void RemoveStep(int ipStatStepLine , int pblContinue)
{
	int ilRc = 0;
	char clNtsk[10] = "\0";
	int ilNtsk = 0;
	char clTemp[10] = "\0";
	char clPref[10] = "\0";
	int ilPref = 0;
	int ili = 0;
	int blFound = FALSE;
	int ilConfigStepLine = 0;
	int ilStatProcLine = 0;
	int blNoNextStep = FALSE;
	int ilStatStepLine = 0;

	ReadStatItem(ipStatStepLine, PREF_POS, 4, clTemp);
	ilStatProcLine = atoi(clTemp);

	ReadStatItem(ipStatStepLine, STEP_POS, 4, clTemp);
	ilConfigStepLine = atoi(clTemp);

	ReadStatItem(ipStatStepLine, NTSK_POS, 4, clNtsk);
	ilNtsk = atoi(clNtsk);

	while (ilNtsk != 0)	
	{
		ilRc = fseek(fpStatusFile,igSFOffset+(ilNtsk-1)*LINE_LEN+NTSK_POS ,SEEK_SET);
		ilRc = fread(clTemp,1,4,fpStatusFile);

		WriteStatItem(ilNtsk,TYPE_POS, cgBlankLine);
		ilRc = fseek(fpStatusFile,ilNtsk ,SEEK_SET);
		ilRc = fprintf(fpStatusFile,".");
		ilRc = fflush(fpStatusFile);
						
		ilNtsk = atoi(clTemp);
	}
	/*find out PREF to identify Procedure.... I have to check if this was the last step. if so, remove the procedure*/
	ilRc = fseek(fpStatusFile,igSFOffset+(ipStatStepLine-1)*LINE_LEN+PREF_POS ,SEEK_SET);
	ilRc = fread(clPref,1,4,fpStatusFile);

	WriteStatItem(ipStatStepLine,TYPE_POS, cgBlankLine);
	ilRc = fseek(fpStatusFile,ipStatStepLine,SEEK_SET);
	ilRc = fprintf(fpStatusFile,".");
	ilRc = fflush(fpStatusFile);

	if(pblContinue == TRUE)
	{
		/*find next step if available*/
		ilConfigStepLine++;
		while((strcmp(rgConfig.rlLine[ilConfigStepLine].clTag,"STEP_BGN") != 0) && (blNoNextStep == FALSE))
		{
			if(strcmp(rgConfig.rlLine[ilConfigStepLine].clTag,"PROC_END") == 0)
			{
				blNoNextStep = TRUE;
				WriteStatItem(ilStatProcLine, TYPE_POS, cgBlankLine);
				ilRc = fseek(fpStatusFile,ilStatProcLine,SEEK_SET);
				ilRc = fprintf(fpStatusFile,".");
				ilRc = fflush(fpStatusFile);
			}
			ilConfigStepLine++;
		}
		if(strcmp(rgConfig.rlLine[ilConfigStepLine].clTag,"STEP_BGN") == 0)
		{
			ilStatStepLine = HandleStep(ilConfigStepLine, ilStatProcLine);
			ReadStatItem(ilStatStepLine, ACK_POS, 3, clTemp);
			if(atoi(clTemp) == 0)
			{
				RemoveStep(ilStatStepLine, TRUE);
			}
		}
	}
	else
	{
		ilRc = fseek(fpStatusFile,ilStatProcLine,SEEK_SET);
		ilRc = fprintf(fpStatusFile,".");
		ilRc = fflush(fpStatusFile);
		WriteStatItem(ilStatProcLine, TYPE_POS, cgBlankLine);
	}

	return;
}




