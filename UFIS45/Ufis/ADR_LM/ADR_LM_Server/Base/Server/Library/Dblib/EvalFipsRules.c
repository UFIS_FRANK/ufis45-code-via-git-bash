#ifndef _DEF_mks_version_EvalFipsRules_c
#define _DEF_mks_version_EvalFipsRules_c
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version_EvalFipsRules_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/ADR_LM/ADR_LM_Server/Base/Server/Library/Dblib/EvalFipsRules.c 1.2 2005/02/24 20:34:32SGT mcu Exp  $";
#endif /* _DEF_mks_version */
/*******************************************************************************
 *
/*********************************************************************************/
/* 20050126: new File */

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "db_if.h"
#include "EvalFipsRules.h"

#define DATFIELDS "URNO,ACGR,ALGR,NAGR,DURA,FLTY"

static int igDatUrno = 1;
static int igDatAcgr = 2;
static int igDatAlgr = 3;
static int igDatNagr = 4;
static int igDatDura = 5;
static int igDatFlty = 6;

static int cgErrMsg[1024];

static int CheckFieldsInSystab(char *pcpTable,char *pcpFields)
{
	int ilRc = RC_SUCCESS;
	short slCursor = 0;
	char clFina[124];
	char clBuf[124];
	int ilFieldCount;
	int ilLc;
	char clSqlBuf[512];
	char clTana[24];

	strcpy(clTana,pcpTable);
	clTana[3] = '\0';
	sprintf(clSqlBuf,"SELECT SYST FROM SYSTAB WHERE TANA = '%s' "
		"AND FINA = ':Val1'",clTana);
  ilFieldCount = get_no_of_items(pcpFields);
  for (ilLc = 1; ilLc <= ilFieldCount; ilLc++)
	{
			if (get_real_item(clBuf,pcpFields,ilLc))
			{
					sprintf(clSqlBuf,"SELECT SYST FROM SYSTAB WHERE TANA = '%s' "
										"AND FINA = '%s'",clTana,clBuf);
					dbg(DEBUG,"<%s>",clSqlBuf);
					ilRc = sql_if(START, &slCursor, clSqlBuf,clBuf);
					close_my_cursor(&slCursor);
					slCursor = 0;
					if (ilRc == RC_SUCCESS)
					{
						if (*clBuf == 'Y')
						{
							continue;
						}
						else
						{
							dbg(TRACE,"Field %s for Table %s not in shared memory",
										clBuf,pcpTable);
							ilRc = RC_FAIL;
						}
					} 
					else if (ilRc == RC_FAIL)
					{
						char clErrMsg[1024];

						get_ora_err (ilRc, clErrMsg);
						dbg(TRACE,"Reading SYST from SYSTAB failed %d<%s>",ilRc,cgErrMsg);
						ilRc = RC_FAIL;
					}
					else if (ilRc == SQL_NOTFOUND)
					{
						dbg(TRACE,"Field %s for Table %s not found in SYSTAB",clBuf,clTana);
						ilRc = RC_FAIL;
					}
			}
		if (ilRc == RC_FAIL)
		{
			break;
		}
	}
	close_my_cursor(&slCursor);
	return ilRc;
}



static int GetActUrno(char *pcpAct3,char *pcpAct5,char *pcpActUrno)
{
		static int blUseShmForActtab = -1;
		int ilCount = 1;
		int ilRc;

		if (blUseShmForActtab == -1)
		{
				blUseShmForActtab = CheckFieldsInSystab("ACTTAB","URNO,ACT3,ACT5");
		}
		
		if (blUseShmForActtab == TRUE)
		{
			ilRc = syslibSearchDbData("ACTTAB","ACT3,ACT5",pcpAct3,pcpAct5,"URNO",pcpActUrno,&ilCount,"");
			dbg(TRACE,"RC from bSearchDbData %d ActUrno <%s>",ilRc,pcpActUrno);
			return ilRc;
		}
		else
		{
			short slCursor = 0;
			char clSqlBuf[248];

			sprintf(clSqlBuf,"SELECT URNO FROM ACTTAB WHERE ACT3 = '%s' AND ACT5 = '%s'",
							pcpAct3,pcpAct5);
			ilRc = sql_if(START, &slCursor, clSqlBuf, pcpActUrno);
			close_my_cursor(&slCursor);
			if (ilRc == RC_FAIL)
			{
				char clErrMsg[1024];

				get_ora_err (ilRc, clErrMsg);
				dbg(TRACE,"Reading URNO from ACTTAB failed %d<%s>",ilRc,cgErrMsg);
				return RC_FAIL;
			}
			else if (ilRc == SQL_NOTFOUND)
			{
				dbg(TRACE,"URNO for <%s/%s> not found in ACTTAB",pcpAct3,pcpAct5);
				return RC_FAIL;
			}
			else
			{
				return ilRc;
			}
		}
}

static int GetAlcUrno(char *pcpAlc3,char *pcpAlcUrno)
{
		static int blUseShmForAlttab = -1;
		int ilRc;
		int ilCount = 1;

		if (blUseShmForAlttab == -1)
		{
				blUseShmForAlttab = CheckFieldsInSystab("ALTTAB","URNO,ALC3");
		}
		
		if (blUseShmForAlttab == TRUE)
		{
			ilRc = syslibSearchDbData("ALTTAB","ALC3",pcpAlc3,"URNO",pcpAlcUrno,&ilCount,"");
			dbg(TRACE,"RC from bSearchDbData %d AlcUrno <%s>",ilRc,pcpAlcUrno);
			
		}
		else
		{
			short slCursor = 0;
			char clSqlBuf[248];

			sprintf(clSqlBuf,"SELECT URNO FROM ALTTAB WHERE ALC3 = '%s'",pcpAlc3);
			ilRc = sql_if(START, &slCursor, clSqlBuf, pcpAlcUrno);
			close_my_cursor(&slCursor);
			if (ilRc == RC_FAIL)
			{
				char clErrMsg[1024];

				get_ora_err (ilRc, clErrMsg);
				dbg(TRACE,"Reading URNO from ALTTAB failed %d<%s>",ilRc,cgErrMsg);
				return RC_FAIL;
			}
			else if (ilRc == SQL_NOTFOUND)
			{
				dbg(TRACE,"URNO for <%s> not found in ALTTAB",pcpAlc3);
				return RC_FAIL;
			}
			else
			{
				return RC_SUCCESS;
			}
		}
}

static int GetGatUrno(char *pcpGnam,char *pcpGatUrno)
{
		static int blUseShmForGattab = -1;
		int ilRc,ilCount;

		if (blUseShmForGattab == -1)
		{
				blUseShmForGattab = CheckFieldsInSystab("GATTAB","URNO,GNAM");
		}
		
		if (blUseShmForGattab == TRUE)
		{
			ilRc = syslibSearchDbData("GATTAB","GNAM","URNO",pcpGatUrno,&ilCount,"");
			dbg(TRACE,"RC from bSearchDbData %d GatUrno <%s>",ilRc,pcpGatUrno);
			return ilRc;
		}
		else
		{
			short slCursor = 0;
			char clSqlBuf[248];

			sprintf(clSqlBuf,"SELECT URNO FROM GATTAB WHERE GNAM = '%s'",pcpGnam);
			ilRc = sql_if(START, &slCursor, clSqlBuf, pcpGatUrno);
			close_my_cursor(&slCursor);
			if (ilRc == RC_FAIL)
			{
				char clErrMsg[1024];

				get_ora_err (ilRc, clErrMsg);
				dbg(TRACE,"Reading URNO from GATTAB failed %d<%s>",ilRc,cgErrMsg);
				return RC_FAIL;
			}
			else if (ilRc == SQL_NOTFOUND)
			{
				dbg(TRACE,"URNO for <%s> not found in GATTAB",pcpGnam);
				return RC_FAIL;
			}
			else
			{
				return ilRc;
			}
		}
}

static int GetPstUrno(char *pcpPnam,char *pcpPstUrno)
{
		static int blUseShmForPsttab = -1;
		int ilRc,ilCount;

		if (blUseShmForPsttab == -1)
		{
				blUseShmForPsttab = CheckFieldsInSystab("PSTTAB","URNO,PNAM");
		}
		
		if (blUseShmForPsttab == TRUE)
		{
			ilRc = syslibSearchDbData("PSTTAB","PNAM","URNO",pcpPstUrno,&ilCount,"");
			dbg(TRACE,"RC from bSearchDbData %d PstUrno <%s>",ilRc,pcpPstUrno);
			return ilRc;
		}
		else
		{
			short slCursor = 0;
			char clSqlBuf[248];

			sprintf(clSqlBuf,"SELECT URNO FROM PSTTAB WHERE PNAM = '%s'",pcpPnam);
			ilRc = sql_if(START, &slCursor, clSqlBuf, pcpPstUrno);
			close_my_cursor(&slCursor);
			if (ilRc == RC_FAIL)
			{
				char clErrMsg[1024];

				get_ora_err (ilRc, clErrMsg);
				dbg(TRACE,"Reading URNO from PSTTAB failed %d<%s>",ilRc,cgErrMsg);
				return RC_FAIL;
			}
			else if (ilRc == SQL_NOTFOUND)
			{
				dbg(TRACE,"URNO for <%s> not found in PSTTAB",pcpPnam);
				return RC_FAIL;
			}
			else
			{
				return ilRc;
			}
		}
}

static int GetNatUrno(char *pcpTtyp,char *pcpNatUrno)
{
		static int blUseShmForNattab = -1;
		int ilRc,ilCount;

		if (blUseShmForNattab == -1)
		{
				blUseShmForNattab = CheckFieldsInSystab("NATTAB","URNO,TTYP");
		}
		
		if (blUseShmForNattab == TRUE)
		{
			ilRc = syslibSearchDbData("NATTAB","TTYP","URNO",pcpNatUrno,&ilCount,"");
			dbg(TRACE,"RC from bSearchDbData %d NatUrno <%s>",ilRc,pcpNatUrno);
			return ilRc;
		}
		else
		{
			short slCursor = 0;
			char clSqlBuf[248];

			sprintf(clSqlBuf,"SELECT URNO FROM NATTAB WHERE TTYP = '%s'",pcpTtyp);
			ilRc = sql_if(START, &slCursor, clSqlBuf, pcpNatUrno);
			close_my_cursor(&slCursor);
			if (ilRc == RC_FAIL)
			{
				char clErrMsg[1024];

				get_ora_err (ilRc, clErrMsg);
				dbg(TRACE,"Reading URNO from NATTAB failed %d<%s>",ilRc,cgErrMsg);
				return ilRc;
			}
			else if (ilRc == SQL_NOTFOUND)
			{
				dbg(TRACE,"URNO for <%s> not found in NATTAB",pcpTtyp);
				return RC_FAIL;
			}
			else
			{
				return ilRc;
			}
		}
}

static int CheckGroup(char *pcpGurn,char *pcpValu)
{
		static int blUseShmForGrmtab = -1;
		static char clGrmUrno[24];
		int ilRc,ilCount;

		if (blUseShmForGrmtab == -1)
		{
				blUseShmForGrmtab = CheckFieldsInSystab("GRMTAB","URNO,GURN,VALU");
		}
		
		if (blUseShmForGrmtab == TRUE)
		{
			ilRc = syslibSearchDbData("GRMTAB","GURN,VALU","URNO",clGrmUrno,&ilCount,"");
			dbg(TRACE,"RC from bSearchDbData %d GrmUrno <%s>",ilRc,clGrmUrno);
			return (ilRc);
		}
		else
		{
			short slCursor = 0;
			char clSqlBuf[248];

			sprintf(clSqlBuf,"SELECT URNO FROM GRMTAB WHERE GURN = '%s' AND VALU = '%s'",
				pcpGurn,pcpValu);
			ilRc = sql_if(START, &slCursor, clSqlBuf, clGrmUrno);
			close_my_cursor(&slCursor);
			if (ilRc == RC_FAIL)
			{
				char clErrMsg[1024];

				get_ora_err (ilRc, clErrMsg);
				dbg(TRACE,"Reading URNO from GRMTAB failed %d<%s>",ilRc,cgErrMsg);
				return ilRc;
			}
			else if (ilRc == SQL_NOTFOUND)
			{
				dbg(TRACE,"URNO for <%s/%s> not found in GRMTAB",pcpGurn,pcpValu);
				return RC_FAIL;
			}
			else
			{
				return RC_SUCCESS;
			}
		}
}

int GetDefAllocTime(int ipAlocType,char *pcpAdid,char *pcpAlid,
	char *pcpAlc3,char *pcpAct3,char *pcpAct5,char *pcpTtyp,
	int *pipAllocTime)
{
		int ilRc = RC_SUCCESS;
		static char clAlocUrno[24] = "";
		char clSqlBuf[1024];
		char clBuf[1024];
		char clAlidUrno[24];
		char clAlgrUrno[24];
		char clAcgrUrno[24];
		char clNagrUrno[24];
		char clAlcUrno[24];
		char clActUrno[24];
		char clNatUrno[24];
		char clDatFlty[24];
		static int ilDatFieldCount = -1;
		int ilGroupCount,ilMaxGroupCount;
		int blIsDefault = FALSE;
		short slCursor;
		short slFunc;

		*pipAllocTime = '\0';
		if (ilDatFieldCount == -1)
		{
			ilDatFieldCount = get_no_of_items(DATFIELDS);
		}
		switch (ipAlocType)
		{
			case ALOC_PST :
					ilRc = GetPstUrno(pcpAlid,clAlidUrno);
					break;
			case ALOC_GAT :
					ilRc = GetGatUrno(pcpAlid,clAlidUrno);
					break;
			default:
					ilRc = RC_FAIL;
					dbg(TRACE,"Unknown Type of allocation unit: %d",ipAlocType);
		}
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Allocation Unit <%s> Type %s not found",
					pcpAlid,ipAlocType == ALOC_PST ? "PST" : ipAlocType == ALOC_GAT ? "GATE" : "UNKNOWN");
			return ilRc;
		}

		GetAlcUrno(pcpAlc3,clAlcUrno);
		GetActUrno(pcpAct3,pcpAct5,clActUrno);
		GetNatUrno(pcpTtyp,clNatUrno);

		sprintf(clSqlBuf,"SELECT %s from DATTAB WHERE ALID = '%s' ORDER BY URNO ",
					DATFIELDS,clAlidUrno);
		dbg(DEBUG,"%s",clSqlBuf);
		slFunc = START;
		slCursor = 0;
		ilGroupCount = 0;
		ilMaxGroupCount = 0;
		do 
		{
			char *pclBuf;
			int ilLc;

			ilRc = sql_if(slFunc,&slCursor,clSqlBuf,clBuf);
			slFunc = NEXT;
			if (ilRc == RC_FAIL)
			{
				char clErrMsg[1024];

				get_ora_err (ilRc, clErrMsg);
				dbg(TRACE,"Reading DATTAB failed %d<%s>",ilRc,cgErrMsg);
			}
			else if (ilRc == NOTFOUND)
			{
				dbg(TRACE,"ALID <%s> not found in DATTAB",clAlidUrno);
				break;
			}
			else
			{
			pclBuf = clBuf;
			for (ilLc = 1; ilLc < ilDatFieldCount; ilLc++)
			{
				int ilLen;
				ilLen = strlen(pclBuf);
				pclBuf[ilLen] = ',';
				pclBuf += ilLen;
			}
			get_real_item(clDatFlty,clBuf,igDatFlty);
			if(*pcpAdid == 'A' && clDatFlty[0] != '1')
			{
				dbg(DEBUG,"This Rule is not for Inbound");
				continue;
			}
			if(*pcpAdid == 'D' && clDatFlty[1] != '1')
			{
				dbg(DEBUG,"This Rule is not for Outbound");
				continue;
			}
			dbg(DEBUG,"<%s>",clBuf);
			ilGroupCount = 0;
			get_real_item(clAlgrUrno,clBuf,igDatAlgr);
			dbg(DEBUG,"%05d:",__LINE__);
			if (*clAlgrUrno != '0')
			{
				if (CheckGroup(clAlgrUrno,clAlcUrno) == RC_SUCCESS)
				{
					ilGroupCount++;
				}
				else
				{
					continue;
				}
			}
			get_real_item(clAcgrUrno,clBuf,igDatAcgr);
			if (*clAcgrUrno != '0')
			{
				if (CheckGroup(clAcgrUrno,clActUrno) == RC_SUCCESS)
				{
					ilGroupCount++;
				}
				else
				{
					continue;
				}
			}
			get_real_item(clNagrUrno,clBuf,igDatNagr);
			if (*clNagrUrno != '0')
			{
				if (CheckGroup(clNagrUrno,clNatUrno) == RC_SUCCESS)
				{
					ilGroupCount++;
				}
				else
				{
					continue;
				}
			}

			if (ilGroupCount == 3)
			{
					char clAllocTime[24];
	
					get_real_item(clAllocTime,clBuf,igDatDura);
					*pipAllocTime = atoi(clAllocTime);
					break;
			}
			else if (ilGroupCount > ilMaxGroupCount || ilMaxGroupCount == 0)
			{
				char clAllocTime[24];

				if (ilGroupCount == 0)
				{
					if (blIsDefault == TRUE)
					{
						/*** default already set, don't overwrite **/
						continue;
					}
					blIsDefault = TRUE;
				}
				get_real_item(clAllocTime,clBuf,igDatDura);
				ilMaxGroupCount = ilGroupCount;
				*pipAllocTime = atoi(clAllocTime);
			}
		}
		} while (ilRc == RC_SUCCESS);
		close_my_cursor(&slCursor);
		if (*pipAllocTime != '\0')
		{
			ilRc = RC_SUCCESS;
		}
		else
		{
			ilRc = RC_FAIL;
		}
	  return ilRc;
}



int GetDefAllocTimeXXX(int ipAlocType,char *pcpAdid,char *pcpAlid,
	char *pcpAlc3,char *pcpAct3,char *pcpAct5,char *pcpTtyp,
	int *pipAllocTime)
	{
		int ilRc = RC_SUCCESS;
		static char clAlocUrno[24] = "";
		char clSqlBuf[1024];
		char clBuf[1024];
		char clAlidUrno[24];
		char clAlgrUrno[24];
		char clAcgrUrno[24];
		char clNagrUrno[24];
		char clAlcUrno[24];
		char clActUrno[24];
		char clNatUrno[24];
		char clDatFlty[24];
		static int ilDatFieldCount = -1;
		int ilGroupCount,ilMaxGroupCount;
		short slCursor;
		short slFunc;

		*pipAllocTime = '\0';
		if (ilDatFieldCount == -1)
		{
			ilDatFieldCount = get_no_of_items(DATFIELDS);
		}
		switch (ipAlocType)
		{
			case ALOC_PST :
					ilRc = GetPstUrno(pcpAlid,clAlidUrno);
					break;
			case ALOC_GAT :
					ilRc = GetGatUrno(pcpAlid,clAlidUrno);
					break;
			default:
					ilRc = RC_FAIL;
					dbg(TRACE,"Unknown Type of allocation unit: %d",ipAlocType);
		}
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Allocation Unit <%s> Type %s not found",
					pcpAlid,ipAlocType == ALOC_PST ? "PST" : ipAlocType == ALOC_GAT ? "GATE" : "UNKNOWN");
			return ilRc;
		}

		GetAlcUrno(pcpAlc3,clAlcUrno);
		GetActUrno(pcpAct3,pcpAct5,clActUrno);
		GetNatUrno(pcpTtyp,clNatUrno);

		sprintf(clSqlBuf,"SELECT %s from DATTAB WHERE ALID = '%s' ",
					DATFIELDS,clAlidUrno);
		dbg(DEBUG,"%s",clSqlBuf);
		slFunc = START;
		slCursor = 0;
		ilGroupCount = 0;
		ilMaxGroupCount = 0;
		do 
		{
			char *pclBuf;
			int ilLc;

			ilRc = sql_if(slFunc,&slCursor,clSqlBuf,clBuf);
			slFunc = NEXT;
			if (ilRc == RC_FAIL)
			{
				char clErrMsg[1024];

				get_ora_err (ilRc, clErrMsg);
				dbg(TRACE,"Reading DATTAB failed %d<%s>",ilRc,cgErrMsg);
			}
			else if (ilRc == NOTFOUND)
			{
				dbg(TRACE,"ALID <%s> not found in DATTAB",clAlidUrno);
				break;
			}
			else
			{
			pclBuf = clBuf;
			for (ilLc = 1; ilLc < ilDatFieldCount; ilLc++)
			{
				int ilLen;
				ilLen = strlen(pclBuf);
				pclBuf[ilLen] = ',';
				pclBuf += ilLen;
			}
			get_real_item(clDatFlty,clBuf,igDatFlty);
			if(*pcpAdid == 'A' && clDatFlty[0] != '1')
			{
				dbg(DEBUG,"This Rule is not for Inbound");
				continue;
			}
			if(*pcpAdid == 'D' && clDatFlty[1] != '1')
			{
				dbg(DEBUG,"This Rule is not for Outbound");
				continue;
			}
			dbg(DEBUG,"<%s>",clBuf);
			ilGroupCount = 0;
			get_real_item(clAlgrUrno,clBuf,igDatAlgr);
			dbg(DEBUG,"%05d:",__LINE__);
			if (*clAlgrUrno != '0')
			{
				if (CheckGroup(clAlgrUrno,clAlcUrno) == RC_SUCCESS)
				{
					ilGroupCount++;
				}
				else
				{
					continue;
				}
			}
			get_real_item(clAcgrUrno,clBuf,igDatAcgr);
			if (*clAcgrUrno != '0')
			{
				if (CheckGroup(clAcgrUrno,clActUrno) == RC_SUCCESS)
				{
					ilGroupCount++;
				}
				else
				{
					continue;
				}
			}
			get_real_item(clNagrUrno,clBuf,igDatNagr);
			if (*clNagrUrno != '0')
			{
				if (CheckGroup(clNagrUrno,clNatUrno) == RC_SUCCESS)
				{
					ilGroupCount++;
				}
				else
				{
					continue;
				}
			}

			if (ilGroupCount == 3)
			{
					char clAllocTime[24];
	
					get_real_item(clAllocTime,clBuf,igDatDura);
					*pipAllocTime = atoi(clAllocTime);
					break;
			}
			else if (ilGroupCount > ilMaxGroupCount || ilMaxGroupCount == 0)
			{
				char clAllocTime[24];
	
				get_real_item(clAllocTime,clBuf,igDatDura);
				ilMaxGroupCount = ilGroupCount;
				*pipAllocTime = atoi(clAllocTime);
			}
		}
		} while (ilRc == RC_SUCCESS);
		close_my_cursor(&slCursor);
		if (*pipAllocTime != '\0')
		{
			ilRc = RC_SUCCESS;
		}
		else
		{
			ilRc = RC_FAIL;
		}
	  return ilRc;
}
