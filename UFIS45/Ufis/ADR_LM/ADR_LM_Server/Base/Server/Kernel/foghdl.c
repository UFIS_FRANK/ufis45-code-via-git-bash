#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/ADR_LM/ADR_LM_Server/Base/Server/Kernel/foghdl.c 1.2 2005/02/24 20:34:31SGT mcu Exp  $";
#endif /* _DEF_mks_version */

/*                                                                            */
/* ABB AAT/I FOGHDL.C                                                         */
/*                                                                            */
/* Author         : Andreas Kloidt                                            */
/* Date           : March 2002                                                */
/* Description    : Process to maintain Flight On Ground Table (FOGTAB)       */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_foghdl[]="%Z% UFIS 4.4 (c) ABB AAT/I %M% %I% / %E% %U% / AKL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <netdb.h>
#include <time.h>
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h" 
#include "tools.h"
#include "helpful.h"
#include "timdef.h"
 

#define XS_BUFF  128
#define S_BUFF   512
#define M_BUFF   1024
#define L_BUFF   2048
#define XL_BUFF  4096
#define XXL_BUFF 8192 



/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int SetSignals(void (*HandleSignal)(int));
extern int DebugPrintItem(int,ITEM *);
extern int DebugPrintEvent(int,EVENT *);
extern int init_db(void);
extern int  ResetDBCounter(void);
extern void HandleRemoteDB(EVENT*);
extern int  sql_if(short ,short* ,char* ,char* ); 
extern int close_my_cursor(short *cursor); 
extern void snap(char*,int,FILE*);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
extern int get_item_no(char *s, char *f, short elem_len);
extern void  GetServerTimeStamp(char*,int,long,char*);
extern int AddSecondsToCEDATime(char *,long,int);
extern int get_real_item(char *, char *, int);
extern long nap(long);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;        /* The queue item pointer  */
static EVENT *prgEvent     = NULL;        /* The event pointer       */
static int   igItemLen     = 0;           /* length of incoming item */
static int   igInitOK      = FALSE;      /* Flag for init */

/*   static CFG   *prgCfg; */                     /* structure of the cfg-file */
static char  pcgHomeAp[XS_BUFF];      /* buffer for home airport */
static char  pcgTabEnd[XS_BUFF];      /* buffer for TABEND */
static char  pcgTwStart[XS_BUFF] = "";
static char  pcgTwEnd[XS_BUFF];
static char  pcgConfFile[S_BUFF];      /* buffer for config-file name */
static int   igUseHopo = FALSE;          /* flag for use of HOPO-field */
static int   igModID_Router  = 1200;      /* MOD-ID of Router  */

static char      pcgHostName[XS_BUFF];
static char      pcgHostIp[XS_BUFF];

static int    igQueCounter=0;

/*entry's from configfile*/
static char pcgConfigFile[512];
static char pcgUfisToUfisConfigFile[512];
static char pcgCfgBuffer[512];
static int igPabsArr = 10;
static int igPaesArr = 10;
static int igPaesArrOri = 10;
static char pcgPaesArr[32] = "20201231235959";
static int igPabsRot = 10;
static int igPaesRot = 10;
static int igPdbsDep = 10;
static int igPdbsDepOri = 10;
static int igPdesDep = 10;
static int igPdbsRot = 10;
static int igPdesRot = 10;
static int igRotTimeArrPerc = 50;
static char pcgInitHisStart[32] = "20020101";
static char pcgLastDayForInit[32] = "20021231";
static int igOnGroundTimeLimit = 60;
static char pcgInitFog[32] = "NO";
static char pcgExclFtyp[512] = "";
static char pcgRogBegin[32] = "";
static char pcgRogEnd[32] = "";
static char pcgInitRog[32] = "NO";
static char pcgTriggerAction[32] = "NO";
static char pcgTriggerBchdl[32] = "NO";
static int igBcOutMode = 0;
static char pcgDebugLevel[32] = "TRACE";
static char pcgDebugLevelInit[32] = "TRACE";
static int igSSIHDL = 7760;
static int igCedaEventInterface = -1;
static int igRemoteTarget = -1;

static int igSendRACBroadcast = FALSE;
static char pcgRACCommand[16];
static char pcgRACDataList[32000];
static char pcgRACBegin[32];
static char pcgRACEnd[32];
static int igTotCountRead;
static int igTotCountIns;
static int igRegnCountRead;
static int igRegnCountIns;
static int igDiffRegn;
static int igAutoJoin = TRUE;
static char pcgUrnoList[32000] = "";

static int igWaitBeforeROGU = 0;  /* Wait im ms before ROGU command is processed */

static char pcgCurrentTime[32] = "";

static int igDiffUtcToLocal;   /* Time diff between Utc and Local Time */

/* Begin Array Insert */
#define S_FOR  1
#define S_BACK 2
REC_DESC rgRecDesc;
static int igUseArrayInsert = FALSE;
static char pcgNewUrnos[1000*16];
static int igLastUrno = 0;
static int igLastUrnoIdx = -1;
typedef struct
{
  char pclUrno[16];
  char pclHopo[8];
  char pclRegn[16];
  char pclAurn[16];
  char pclDurn[16];
  char pclRkey[16];
  char pclLand[16];
  char pclAirb[16];
  char pclFtyp[4];
  char pclStat[4];
  char pclFtpa[4];
  char pclFtpd[4];
  char pclSeqn[16];
  char pclSeqs[4];
  char pclFlag[4];
} STR_ROG;
static STR_ROG rgRogDat[10000];
static int igCurRogDat = -1;
static char pcgRogBuf[10000*150];
static char pcgRogFld[1024] = "URNO,HOPO,REGN,AURN,DURN,RKEY,LAND,AIRB,FTYP,STAT,FTPA,FTPD,SEQN,SEQS";
static char pcgRogVal[1024] = ":VURNO,:VHOPO,:VREGN,:VAURN,:VDURN,:VRKEY,:VLAND,:VAIRB,:VFTYP,:VSTAT,:VFTPA,:VFTPD,:VSEQN,:VSEQS";
/* End Array Insert */

/* for trigger action */
static EVENT *prgOutEvent = NULL;

static int    Init_foghdl();
static int    Reset(void);                        /* Reset program          */
static void   Terminate(int);                     /* Terminate program      */
static void   HandleSignal(int);                  /* Handles signals        */
static void   HandleErr(int);                     /* Handles general errors */
static void   HandleQueErr(int);                  /* Handles queuing errors */
static int    HandleInternalData(void);           /* Handles event data     */
static void   HandleQueues(void);                 /* Waiting for Sts.-switch*/
/******************************************************************************/
/* Function prototypes by AKL                                                 */
/******************************************************************************/
/* Init-functions  */

static int GetQueues();
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
                     char *pcpTwStart,char* pcpTwEnd,
		     char *pcpSelection,char *pcpFields,char *pcpData,
                     char *pcpAddStruct,int ipAddstructSize); 
static int GetConfig();
static int TimeToStr(char *pcpTime,time_t lpTime);
static int InitFog();
static int BuildGroundTimes(char *pclWorkDay);
static int BuildGroundTimesArrivals(char *pclWorkDay);
static int BuildGroundTimesDepartures(char *pclWorkDay);
static int BuildGroundTimesRotations(char *pclWorkDay);
static void SetDefaultAllocationTimes(char *pcpBAA4,char *pcpBAA5);
static void ResetDefaultAllocationTimes();
static int CalculateGroundTimes(char *pcpTifa, char *pcpTifaPrev,
                                char *pcpTifd, char *pcpTifdNext,
                                char *pcpOnbl, char *pcpOnblPrev,
                                char *pcpOfbl, char *pcpOfblNext,
                                char *pcpType, int ipCur, int ipMax,
                                char *pcpPabs, char *pcpPaes, char *pcpPdbs, char *pcpPdes);
static int InsFOGTAB(char *pcpUrno, char *pcpType, char *pcpStat,
                     char *pcpFirstDay, char *pcpLastDay);
static int CheckFOGTAB();
static int UpdateFOGTAB(char *pcpUrno);
static int UpdateArrival(char *pcpUrno, char *pcpTifa, char *pcpTifd, char *pcpPabs,
                         char *pcpPaes, char *pcpPdbs, char *pcpPdes, char *pcpOnbl,
                         char *pcpRkey, char *pcpFtyp);
static int UpdateDeparture(char *pcpUrno, char *pcpTifa, char *pcpTifd, char *pcpPabs,
                           char *pcpPaes, char *pcpPdbs, char *pcpPdes, char *pcpRkey,
                           char *pcpFtyp);
static int UpdateRotation(char *pcpUrno, char *pcpRkey);
static int UpdFOGTAB(char *pcpUrno, char *pcpOgbs, char *pcpOges, char *pcpType,
                     char *pcpOnbl, char *pcpOfbl);
static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction);
static long GetSecondsFromCEDATime(char *pcpDateTime);
static int InitRog();
static int BuildROGTAB(char *pcpRegn, char *pcpStartTime);
static int InsROGTAB(char *pcpRegn, char *pcpAurn, char *pcpDurn, char *pcpRkey,
                     char *pcpLand, char *pcpAirb, char *pcpFtyp, char *pcpStat,
                     char *pcpFtpa, char *pcpFtpd);
static int UpdROGTAB(char *pcpAdid, char *pcpUrno,  char *pcpRurn, char *pcpTime,
                     char *pcpFtyp, char *pcpStat, char *pcpFtpad);
static int SortCheckRegn(char *pcpRegn, char *pcpTime, char *pcpSeqn);
static int UpdSEQN(char *pcpUrno, int ipSeqn, char *pcpSeqn);
static int GetCorrectedFormatZero(char *pcpResult, char *pcpString, int ipLen);
static int UpdSEQS(char *pcpUrno, char *pcpSeqs, char *pcpPrevSeqs);
static int UpdateROGTAB(char *pcpUrno);
static int DelROG(char *pcpUrno);
static void TrimRight(char *pcpBuffer);
static int JoinAftRecs(char *pcpArrUrno, char *pcpDepUrno, char *pcpRkey);
static int SplitAftRecs(char *pcpArrUrno, char *pcpDepUrno);
static int CheckROGvsAFT(char *pcpRegn, char *pcpSeqn);
static int CompressDataList(char *pcpData);
static int AppendUrnoList();
static int CalculateMaxCEDATime(char *pcpResult, char *pcpVal1, char *pcpVal2, char *pcpVal3,
                                char *pcpVal4);
static int MyAddSecondsToCEDATime(char *pcpTime, long lpValue, int ipP);
static int SqlArrayInsert();
static int SqlArrayFill();
static int SearchByKey(char *pcpKey, char * pcpVal, int ilDirection, int *pipIndex);
static int SearchByAIRB(char *pcpAirb,int *pipIndex);
static int SearchByRKEYDep(char *pcpRkey,int *pipIndex);
static int SearchByLAND(char *pcpLand,int *pipIndex);
static int SearchByRKEYArr(char *pcpRkey,int *pipIndex);
static int SearchByLANDSeqs(char *pcpLand,int *pipIndex);
static int SearchByLANDSeqn(char *pcpSeqn,int *pipIndex);
static int SearchByAIRBSeqn(char *pcpSeqn,int *pipIndex);
static int SearchBySEQN(char *pcpSeqn,int *pipIndex);
static int CheckBcMode(char *pcpNPabs, char *pcpOPabs, char *pcpNPaes, char *pcpOPaes,
                       char *pcpNPdbs, char *pcpOPdbs, char *pcpNPdes, char *pcpOPdes,
                       char *pcpFieldList, char *pcpDataList);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int    ilRc = RC_SUCCESS;            /* Return code            */
  int    ilCnt = 0;
  int   ilItemFlag=TRUE; 
  time_t now = 0;
  INITIALIZE;            /* General initialization    */

  /* signal handling of SIGPIPE,SIGCHLD,SIGALRM,SIGTERM */
  SetSignals(HandleSignal);

  dbg(TRACE,"------------------------------------------");
  dbg(TRACE,"MAIN: version <%s>",sccs_foghdl);

  /* Attach to the MIKE queues */
  do{
    ilRc = init_que();
    if(ilRc != RC_SUCCESS)
      {
	dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	sleep(6);
	ilCnt++;
      }/* end of if */
  }while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }else{
      dbg(TRACE,"MAIN: init_que() OK!");
      dbg(TRACE,"MAIN: mod_id   <%d>",mod_id);
      dbg(TRACE,"MAIN: mod_name <%s>",mod_name);
    }/* end of if */
  do
    {
      ilRc = init_db();
      if (ilRc != RC_SUCCESS)
	{
	  dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	  sleep(6);
	  ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
      sleep(60);
      exit(2);
    }else{
      dbg(TRACE,"MAIN: init_db()  OK!");
    } /* end of if */

  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
  *pcgConfFile = 0x00;
  sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),mod_name);
  ilRc = TransferFile(pcgConfFile);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
    } /* end of if */
  dbg(TRACE,"MAIN: Binary-file = <%s>",pcgConfFile);
  ilRc = SendRemoteShutdown(mod_id);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    }
  if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: waiting for status switch ...");
      HandleQueues();
      dbg(TRACE,"MAIN: now running ...");
    }/* end of if */
  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: initializing ...");
      dbg(TRACE,"------------------------------------------");
      if(igInitOK == FALSE)
	{
	  ilRc = Init_foghdl();
	  if(ilRc == RC_SUCCESS)
	    {
	      dbg(TRACE,"");
	      dbg(TRACE,"------------------------------------------");
	      dbg(TRACE,"MAIN: initializing OK");
	      igInitOK = TRUE;
	    } 
	}
    }else{
      Terminate(1);
    }
  dbg(TRACE,"------------------------------------------");
    
  if (igInitOK == TRUE)
    {
      now = time(NULL);
      while(TRUE)
	{
	  memset(prgItem,0x00,igItemLen);
	  ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	  dbg(DEBUG,"QUE Counter %d",++igQueCounter);
	  /* depending on the size of the received item  */
	  /* a realloc could be made by the que function */
	  /* so do never forget to set event pointer !!! */
	  prgEvent = (EVENT *) prgItem->text;
	  if( ilRc == RC_SUCCESS )
	    {
	      /* Acknowledge the item */
	      ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	      if( ilRc != RC_SUCCESS ) 
		{
		  /* handle que_ack error */
		  HandleQueErr(ilRc);
		} /* fi */
	      switch( prgEvent->command )
		{
		case    HSB_STANDBY    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_COMING_UP    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_ACTIVE    :
		  ctrl_sta = prgEvent->command;
		  break;    
		case    HSB_ACT_TO_SBY    :
		  ctrl_sta = prgEvent->command;
		  /* CloseConnection(); */
		  HandleQueues();
		  break;    
		case    HSB_DOWN    :
		  /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		  ctrl_sta = prgEvent->command;
		  Terminate(1);
		  break;    
		case    HSB_STANDALONE    :
		  ctrl_sta = prgEvent->command;
		  ResetDBCounter();
		  break;    
		case    REMOTE_DB :
		  /* ctrl_sta is checked inside */
		  HandleRemoteDB(prgEvent);
		  break;
		case    SHUTDOWN    :
		  /* process shutdown - maybe from uutil */
		  Terminate(10);
		  break;
		case    RESET        :
		  ilRc = Reset();
		  break;
		case    EVENT_DATA    :
		  if((ctrl_sta == HSB_STANDALONE) ||
		     (ctrl_sta == HSB_ACTIVE) ||
		     (ctrl_sta == HSB_ACT_TO_SBY))
		    {
		      ilItemFlag=TRUE;
		      ilRc = HandleInternalData();
		      if(ilRc != RC_SUCCESS)
			{
			  HandleErr(ilRc);
			}/* end of if */
		    }
		  else
		    {
		      dbg(TRACE,"MAIN: wrong HSB-status <%d>",ctrl_sta);
		      DebugPrintItem(TRACE,prgItem);
		      DebugPrintEvent(TRACE,prgEvent);
		    }/* end of if */
		  break; 
		case    TRACE_ON :
		  dbg_handle_debug(prgEvent->command);
		  break;
		case    TRACE_OFF :
		  dbg_handle_debug(prgEvent->command);
		  break;
                case  111 :
                  ilRc = iGetConfigEntry(pcgConfigFile,"FOGHDL","REPAIR_REGN",
                                         CFG_STRING,pcgCfgBuffer);
                  if (ilRc == RC_SUCCESS)
                  {
                     igSendRACBroadcast = TRUE;
                     strcpy(pcgRACCommand,"RAC");
                     strcpy(pcgRACBegin,"");
                     strcpy(pcgRACEnd,"");
                     strcpy(pcgRACDataList,"");
                     ilRc = BuildROGTAB(pcgCfgBuffer,NULL);
                     strcpy(pcgRACDataList,"");
                     igSendRACBroadcast = FALSE;
                  }
                  break;
		default            :
		  dbg(TRACE,"MAIN: unknown event");
		  DebugPrintItem(TRACE,prgItem);
		  DebugPrintEvent(TRACE,prgEvent);
		  break;
		} /* end switch */
	    }else{
	      /* Handle queuing errors */
	      HandleQueErr(ilRc);
	    } /* end else */



	  /**************************************************************/
	  /* time parameter for cyclic actions                          */
	  /**************************************************************/
    
	  now = time(NULL);

	} /* end while */
    }else{
      dbg(TRACE,"MAIN: Init_foghdl() failed with <%d> Sleeping 30 sec.! Then terminating ...",ilRc);
      sleep(30);
    }
  exit(0);
  return 0;
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_foghdl()
{
  int    ilRc = RC_SUCCESS;            /* Return code */

  GetQueues();
  /* reading default home-airport from sgs.tab */
  memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
  ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_foghdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_foghdl : HOMEAP = <%s>",pcgHomeAp);
    }
  /* reading default table-extension from sgs.tab */
  memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
  ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_foghdl : No TABEND entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_foghdl : TABEND = <%s>",pcgTabEnd);
      memset(pcgTwEnd,0x00,XS_BUFF);
      sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
      dbg(TRACE,"Init_foghdl : TW_END = <%s>",pcgTwEnd);
      
      if (strcmp(pcgTabEnd,"TAB") == 0)
	{
	  igUseHopo = TRUE;
	  dbg(TRACE,"Init_foghdl: use HOPO-field!");
	}
    }

  ilRc = GetConfig();

  ilRc = TimeToStr(pcgCurrentTime,time(NULL));

  InitRecordDescriptor(&rgRecDesc,1024,0,FALSE);

  ilRc = InitFog();

  ilRc = InitRog();

  return(ilRc);
} /* end of initialize */
/*********************************************************************
Function : GetQueues()
Paramter :
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Gets all necessary queue-ID's for CEDA-internal
             communication!
*********************************************************************/
static int GetQueues()
{
  int ilRc = RC_FAIL;

  /* get mod-id of router */
  if ((igModID_Router = tool_get_q_id("router")) == RC_NOT_FOUND ||
      igModID_Router == RC_FAIL || igModID_Router == 0)
    {
      dbg(TRACE,"GetQueues   : tool_get_q_id(router) returns: <%d>",igModID_Router);
      ilRc = RC_FAIL;
    }else{
      dbg(TRACE,"GetQueues   : <router> mod_id <%d>",igModID_Router);
      ilRc = RC_SUCCESS;
    } 
  return ilRc;
}

static int CheckCfg(void)
{
  int ilRc = RC_SUCCESS;
  return ilRc;
}
/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{

  int    ilRc = RC_SUCCESS;    /* Return code */
    
  dbg(TRACE,"Reset: now reseting ...");

  return ilRc;
    
} /* end of Reset */
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
  dbg(TRACE,"Terminate: now leaving ...");
 
  sleep(ipSleep);
  
  exit(0);
    
} /* end of Terminate */
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      break;
    case SIGPIPE:
      break;
    case SIGCHLD:
      break;
    case SIGTERM:
      Terminate(1);
      break;
    default    :
      Terminate(10);
      break;
    } /* end of switch */
} /* end of HandleSignal */
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
  /*    int    ilRc = RC_SUCCESS; */
    return;
} /* end of HandleErr */
/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
            dbg(TRACE,"<%d> : msgsnd failed",pipErr);
            break;
    case    QUE_E_GET    :    /* Error using msgrcv */
            if(pipErr != 4)        
             dbg(DEBUG,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    : 
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :   unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        /*dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    case    QUE_E_PRIORITY    :
        dbg(TRACE,"<%d> : wrong priority was send ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */
/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  int    ilBreakOut = FALSE;
    
  do{
    memset(prgItem,0x00,igItemLen);
    ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text;    
    if( ilRc == RC_SUCCESS )
      {
    /* Acknowledge the item */
    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
    if( ilRc != RC_SUCCESS ) 
      {
        /* handle que_ack error */
        HandleQueErr(ilRc);
      } /* fi */
        
    switch( prgEvent->command )
      {
      case    HSB_STANDBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_COMING_UP    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_ACTIVE    :
        ctrl_sta = prgEvent->command;
        ilBreakOut = TRUE;
        break;    
      case    HSB_ACT_TO_SBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_DOWN    :
        /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
        ctrl_sta = prgEvent->command;
        Terminate(10);
        break;    
      case    HSB_STANDALONE    :
        ctrl_sta = prgEvent->command;
        ResetDBCounter();
        ilBreakOut = TRUE;
        break;    
      case    REMOTE_DB :
        /* ctrl_sta is checked inside */
        HandleRemoteDB(prgEvent);
        break;
      case    SHUTDOWN    :
        Terminate(1);
        break;
      case    RESET        :
        ilRc = Reset();
        break;
      case    EVENT_DATA    :
        dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
        DebugPrintItem(TRACE,prgItem);
        DebugPrintEvent(TRACE,prgEvent);
        break;
      case    TRACE_ON :
        dbg_handle_debug(prgEvent->command);
        break;
      case    TRACE_OFF :
        dbg_handle_debug(prgEvent->command);
        break;
      default            :
        dbg(TRACE,"HandleQueues: unknown event");
        DebugPrintItem(TRACE,prgItem);

        DebugPrintEvent(TRACE,prgEvent);

        break;
      } /* end switch */
      }else{
    /* Handle queuing errors */
    HandleQueErr(ilRc);
      } /* end else */
  } while (ilBreakOut == FALSE);
  if(igInitOK == FALSE)
    {
      ilRc = Init_foghdl();
      if(ilRc == RC_SUCCESS)
    {
      dbg(TRACE,"HandleQueues: Init_foghdl() OK!");
      igInitOK = TRUE;
    }else{ /* end of if */
      dbg(TRACE,"HandleQueues: Init_foghdl() failed!");
      igInitOK = FALSE;
    } /* end of if */
    }/* end of if */
  /* OpenConnection(); */
} /* end of HandleQueues */
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
  int  ilRC = RC_SUCCESS;      /* Return code */
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  char pclNewData[L_BUFF];
  char pclOldData[L_BUFF];
  char pclUrno[50];
  char *pclTmpPtr=NULL;
  BC_HEAD *bchd = NULL;          /* Broadcast header*/
  CMDBLK  *cmdblk = NULL; 
  char pclTmpInitFog[32];
  char pclRegn[32];
  char pclStartTime[32];
  int ilLen;

  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);

  strcpy(pcgTwStart,cmdblk->tw_start);
  strcpy(pcgTwEnd,cmdblk->tw_end);

  /***********************************/
  /*    DebugPrintItem(DEBUG,prgItem);   */
  /*    DebugPrintEvent(DEBUG,prgEvent); */
  /***********************************/
  memset(pclOldData,0x00,L_BUFF);
  pclSelection = cmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
  pclData = (char *)pclFields + strlen(pclFields) + 1;

  strcpy(pclNewData,pclData);
  pclTmpPtr = strstr(pclNewData,"\n");
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
     pclTmpPtr++;
     strcpy(pclOldData,pclTmpPtr);
  }
  *pclUrno = '\0';
  pclTmpPtr = strstr(pclSelection,"\n");
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
     pclTmpPtr++;
     strcpy(pclUrno,pclTmpPtr);
  }
  
  dbg(DEBUG,"Command:   <%s>",cmdblk->command);
  dbg(DEBUG,"Selection: <%s><%s>",pclSelection,pclUrno);
  dbg(DEBUG,"Fields:    <%s>",pclFields);
  dbg(DEBUG,"New Data:  <%s>",pclNewData);
  dbg(DEBUG,"Old Data:  <%s>",pclOldData);
  dbg(DEBUG,"TwStart:   <%s>",pcgTwStart);
  dbg(DEBUG,"TwEnd:     <%s>",pcgTwEnd);

  ilRC = TimeToStr(pcgCurrentTime,time(NULL));

  igAutoJoin = TRUE;

  if (strcmp(cmdblk->command,"FOGU") == 0)
  {
     ilRC = UpdateFOGTAB(pclUrno);
  }

  if (strcmp(cmdblk->command,"ROGU") == 0)
  {
     if (igWaitBeforeROGU > 0)
     {
        nap(igWaitBeforeROGU); /* Wait a little bit */
     }
     igSendRACBroadcast = TRUE;
     strcpy(pcgRACCommand,"RAC");
     strcpy(pcgRACBegin,"");
     strcpy(pcgRACEnd,"");
     strcpy(pcgRACDataList,"");
     ilRC = UpdateROGTAB(pclUrno);
     igSendRACBroadcast = FALSE;
  }

  if (strcmp(cmdblk->command,"FOGC") == 0)
  {
     ilRC = CheckFOGTAB();
  }

  if (strcmp(cmdblk->command,"FOGI") == 0)
  {
     strcpy(pclTmpInitFog,pcgInitFog);
     strcpy(pcgInitFog,"YES");
     ilRC = InitFog();
     strcpy(pcgInitFog,pclTmpInitFog);
  }

  if (strcmp(cmdblk->command,"ROGI") == 0)
  {
     strcpy(pclTmpInitFog,pcgInitRog);
     strcpy(pcgInitRog,"YES");
     ilRC = InitRog();
     strcpy(pcgInitRog,pclTmpInitFog);
  }

  if (strcmp(cmdblk->command,"ROGX") == 0)
  {
     igAutoJoin = FALSE;
  }
  if (strcmp(cmdblk->command,"ROGR") == 0 || strcmp(cmdblk->command,"ROGX") == 0)
  {
     *pclRegn = '\0';
     *pclStartTime = '\0';
     BuildItemBuffer(pclSelection,"",2,",");
     ilLen = get_real_item(pclRegn,pclSelection,1);
     ilLen = get_real_item(pclStartTime,pclSelection,2);
     strcpy(pcgRACBegin,pclStartTime);
     ilLen = get_real_item(pcgRACEnd,pclSelection,3);
     TrimRight(pclRegn);
     TrimRight(pclStartTime);
     dbg(DEBUG,"Rebuild ROGTAB for Registration <%s> starting at <%s>",pclRegn,pclStartTime);
     if (*pclRegn != ' ' && *pclStartTime != ' ')
     {
        strcpy(pcgRACDataList,pclNewData);
        if (strlen(pcgRACDataList) > 0)
        {
           strcat(pcgRACDataList,",");
        }
        igSendRACBroadcast = TRUE;
        if (strcmp(cmdblk->command,"ROGR") == 0)
        {
           strcpy(pcgRACCommand,"RAC");
        }
        else
        {
           strcpy(pcgRACCommand,"RAX");
        }
        ilRC = BuildROGTAB(pclRegn,pclStartTime);
        ilRC = AppendUrnoList();
        strcpy(pcgRACDataList,"");
        igSendRACBroadcast = FALSE;
     }
     else
     {
        dbg(TRACE,"Invalid Registration <%s> or StartTime <%s>",pclRegn,pclStartTime);
     }
  }

  if (strcmp(cmdblk->command,"EXCO") == 0)
  {
     ilRC = TriggerBchdlAction(cmdblk->command,cmdblk->obj_name,pclSelection,pclFields,
                               pclNewData,TRUE,FALSE);
  }

  if (strcmp(cmdblk->command,"ACCE") == 0)
  {
     if (strlen(pcgUrnoList) > 0)
     {
        dbg(TRACE,"Send ACCE to %d , Urno List = \n<%s>",igSSIHDL,pcgUrnoList);
        (void) tools_send_info_flag(igSSIHDL,0,"roghdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                                    "ACCE","",pcgUrnoList,"","",0);
        strcpy(pcgUrnoList,"");
     }
  }

  igAutoJoin = TRUE;

  dbg(DEBUG,"========================= START / END =========================");
  return ilRC;
} /* end of HandleInternalData */
/*********************************************************************
Function : SendEvent()
Paramter : IN: pcpCmd = command for cmdblk->command
           IN: ipModID = process-ID where the event is send to
       IN: ipPriority = priority for sending ( 1- 5, usuallay 3)
       IN: pcpTable = Name (3 letters) of cmdblk->obj_name (if
           necessary), will be expanded with "pcgTabEnd".
       IN: pcpTwStart = cmdblk->twstart
       IN: pcpTwEnd = cmdblk->twend (always HOMEAP,TABEND,processname)
       IN: pcpSelection = selection for event (cmdblk->data)
       IN: pcpFields = fieldlist (corresponding to pcpdata)
       IN: pcpData = datalist (comma separated, corresponding to 
                                   pcpFields)
       IN: pcpAddStruct = additional structure to be transmitted
       IN: ipAddStructSize = size of the additional structure
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: Sends an event to another CEDA-process using que(QUE_PUT).
             Sends the event in standard CEDA-format (BCHEAD,CMDBLK,
         selection,fieldlist,datalist) or sends a different
         data structure (special) at CMDBLK->data. !! Sends always
         only one type, standard OR special, with one event !!
*********************************************************************/
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;

  if (pcpAddStruct == NULL)
    ipAddStructSize = 0;
  if (ipAddStructSize == 0)
    pcpAddStruct = NULL;

  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 
    ipAddStructSize + 128;

  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);

      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command    = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)NETOUT_NO_ACK;/*spaeter nur bei disconnect*/
      strncpy(prlOutBCHead->dest_name,mod_name,10);
      strncpy(prlOutBCHead->recv_name, "EXCO",10);
 
      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCmd);
      if (pcpTable != NULL)
    {
      strcpy(prlOutCmdblk->obj_name,pcpTable);
      strcat(prlOutCmdblk->obj_name,pcgTabEnd);
    }
        
      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTwStart,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwEnd,32);
        
      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      if (pcpAddStruct == NULL)
    {
      /* setting selection inside event */
      strcpy(prlOutCmdblk->data,pcpSelection);
      /* setting field-list inside event */
      strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
      /* setting data-list inside event */
      strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
    }else{
      /*an additional structure is used and will be copied to */
      /*cmdblk + sizeof(CMDBLK).!!! No STANDARD CEDA-ipcs is used !!!! */
      memcpy(prlOutCmdblk->data,(char*)pcpAddStruct,ipAddStructSize);    
    }

      /*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
      /*snapit((char*)prlOutEvent,ilLen,outp);*/
      dbg(DEBUG,"SendEvent: sending event to mod_id <%d>",ipModID);

      if (ipModID != 0)
    {
      if ((ilRc = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
          != RC_SUCCESS)
        {
          dbg(TRACE,"SendEvent: QUE_PUT returns: <%d>", ilRc);
          Terminate(1);
        }
    }else{
      dbg(TRACE,"SendEvent: mod_id = <%d>! Can't send!",ipModID);
    }
      /* free memory */
      free((void*)prlOutEvent); 
    }
  return ilRc;
}

/*
	Get Config Entries
*/
static int GetConfig()
{
  int ilRC = RC_SUCCESS;
  int ilI;
  char pclTmpBuf[128];
  int ilLen;

  sprintf(pcgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
  dbg(TRACE,"Config File is <%s>",pcgConfigFile);
  sprintf(pcgUfisToUfisConfigFile,"%s/UfisToUfis.cfg",getenv("CFG_PATH"));
  dbg(TRACE,"UfisToUfisConfig File is <%s>",pcgUfisToUfisConfigFile);

  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PABS_ARR",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPabsArr = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PAES_ARR_S",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPaesArr = atoi(pcgCfgBuffer);
     igPaesArrOri = igPaesArr;
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PAES_ARR_L",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgPaesArr,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PABS_ROT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPabsRot = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PAES_ROT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPaesRot = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PDBS_DEP",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPdbsDep = atoi(pcgCfgBuffer);
     igPdbsDepOri = igPdbsDep;
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PDES_DEP",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPdesDep = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PDBS_ROT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPdbsRot = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","PDES_ROT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igPdesRot = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","ROT_TIME_ARR_PERC",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igRotTimeArrPerc = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","INIT_HIS_START",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgInitHisStart,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","LAST_DAY_FOR_INIT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgLastDayForInit,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","ON_GROUND_TIME_LIMIT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igOnGroundTimeLimit = atoi(pcgCfgBuffer);
     igOnGroundTimeLimit *= 60;
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","INIT_FOG",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgInitFog,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","EXCL_FTYP",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     memset(pcgExclFtyp,0x00,512);
     for (ilI = 0; ilI < strlen(pcgCfgBuffer); ilI += 2)
     {
        strcat(pcgExclFtyp,"'");
        pcgExclFtyp[strlen(pcgExclFtyp)] = pcgCfgBuffer[ilI];
        strcat(pcgExclFtyp,"',");
     }
     pcgExclFtyp[strlen(pcgExclFtyp)-1] = '\0';
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","ROG_BEGIN",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgRogBegin,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","ROG_END",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgRogEnd,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","INIT_ROG",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgInitRog,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","TRIGGER_ACTION",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgTriggerAction,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","TRIGGER_BCHDL",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgTriggerBchdl,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","BC_OUT_MODE",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igBcOutMode = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","DEBUG_LEVEL",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgDebugLevel,pcgCfgBuffer);
  }
  if (strcmp(pcgDebugLevel,"DEBUG") == 0)
  {
     debug_level = DEBUG;
  }
  else
  {
     if (strcmp(pcgDebugLevel,"TRACE") == 0)
     {
        debug_level = TRACE;
     }
     else
     {
        if (strcmp(pcgDebugLevel,"NULL") == 0)
        {
           debug_level = 0;
        }
        else
        {
           debug_level = TRACE;
        }
     }
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","DEBUG_LEVEL_INIT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgDebugLevelInit,pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","SSIHDL",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igSSIHDL = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","WAIT_BEFORE_ROGU",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     igWaitBeforeROGU = atoi(pcgCfgBuffer);
  }
  ilRC = iGetConfigEntry(pcgUfisToUfisConfigFile,"CEI",mod_name,CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     ilLen = get_real_item(pclTmpBuf,pcgCfgBuffer,2);
     igCedaEventInterface = atoi(pclTmpBuf);
     ilLen = get_real_item(pclTmpBuf,pcgCfgBuffer,3);
     igRemoteTarget = atoi(pclTmpBuf);
  }
  else
  {
     igCedaEventInterface = -1;
     ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","REMOTE_TARGET_FOR_RAC",CFG_STRING,pcgCfgBuffer);
     if (ilRC == RC_SUCCESS)
     {
        igRemoteTarget = atoi(pcgCfgBuffer);
     }
     else
     {
        igRemoteTarget = -1;
     }
  }
  ilRC = iGetConfigEntry(pcgConfigFile,"FOGHDL","USE_ARRAY_INSERT",CFG_STRING,pcgCfgBuffer);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pcgCfgBuffer,"YES") == 0)
     {
        igUseArrayInsert = TRUE;
     }
  }

  dbg(TRACE,"PABS_ARR = %d",igPabsArr);
  dbg(TRACE,"PAES_ARR_S = %d",igPaesArr);
  dbg(TRACE,"PAES_ARR_L = %s",pcgPaesArr);
  dbg(TRACE,"PABS_ROT = %d",igPabsRot);
  dbg(TRACE,"PAES_ROT = %d",igPaesRot);
  dbg(TRACE,"PDBS_DEP = %d",igPdbsDep);
  dbg(TRACE,"PDES_DEP = %d",igPdesDep);
  dbg(TRACE,"PDBS_ROT = %d",igPdbsRot);
  dbg(TRACE,"PDES_ROT = %d",igPdesRot);
  dbg(TRACE,"ROT_TIME_ARR_PERC = %d",igRotTimeArrPerc);
  dbg(TRACE,"INIT_HIS_START = %s",pcgInitHisStart);
  dbg(TRACE,"LAST_DAY_FOR_INIT = %s",pcgLastDayForInit);
  dbg(TRACE,"ON_GROUND_TIME_LIMIT = %d",igOnGroundTimeLimit/60);
  dbg(TRACE,"INIT_FOG = %s",pcgInitFog);
  dbg(TRACE,"EXCL_FTYP = %s",pcgExclFtyp);
  dbg(TRACE,"ROG_BEGIN = %s",pcgRogBegin);
  dbg(TRACE,"ROG_END = %s",pcgRogEnd);
  dbg(TRACE,"INIT_ROG = %s",pcgInitRog);
  dbg(TRACE,"SSIHDL = %d",igSSIHDL);
  dbg(TRACE,"ID Ceda Event Interface = %d",igCedaEventInterface);
  dbg(TRACE,"ID Remote Target for RAC = %d",igRemoteTarget);
  dbg(TRACE,"Wait before ROGU = %d",igWaitBeforeROGU);
  dbg(TRACE,"Use Array Insert = %d",igUseArrayInsert);
  dbg(TRACE,"TRIGGER_ACTION = %s",pcgTriggerAction);
  dbg(TRACE,"TRIGGER_BCHDL = %s",pcgTriggerBchdl);
  dbg(TRACE,"BROADCAST OUTPUT MODE = %d",igBcOutMode);
  dbg(TRACE,"DEBUG_LEVEL = %s",pcgDebugLevel);
  dbg(TRACE,"DEBUG_LEVEL_INIT = %s",pcgDebugLevelInit);

  return RC_SUCCESS;
} /* Enf of GetConfig */

/******************************************************************************/
/* The TimeToStr routine                                                      */
/******************************************************************************/
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                              
  struct tm *_tm;
    
  _tm = (struct tm *)gmtime(&lpTime);

  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
          _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
          _tm->tm_min,_tm->tm_sec);

  return RC_SUCCESS;  
                        
}     /* end of TimeToStr */


static int InitFog()
{
  int ilRC = RC_SUCCESS;
  int ilI;
  char pclCurrentTimeEnd[32];
  char pclWorkDay[32];
  char pclLastDay[32];
  int ilSavDebugLevel;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclDataBuf[L_BUFF];

  dbg(TRACE,"================ Now Start Initializing FOGTAB ================");
  if (strcmp(pcgInitFog,"YES") == 0)
  {
     ilSavDebugLevel = debug_level;
     if (strcmp(pcgDebugLevelInit,"DEBUG") == 0)
     {
        debug_level = DEBUG;
     }
     else
     {
        debug_level = TRACE;
     }
     dbg(DEBUG,"Current Time = <%s>",pcgCurrentTime);

     sprintf(pclSqlBuf,"DELETE FROM FOGTAB");
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"<%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);

     strcpy(pclLastDay,pcgLastDayForInit);
     strcpy(pclWorkDay,pcgInitHisStart);
     while (strcmp(pclWorkDay,pclLastDay) <= 0)
     {
        ilRC = BuildGroundTimes(pclWorkDay);
        strcat(pclWorkDay,"120000");
        ilRC = MyAddSecondsToCEDATime(pclWorkDay,24 * 60 * 60,1);
        pclWorkDay[8] = '\0';
     }

     dbg(TRACE," ");
     dbg(TRACE,"Start: %s",pcgCurrentTime);
     ilRC = TimeToStr(pclCurrentTimeEnd,time(NULL));
     dbg(TRACE,"End:   %s",pclCurrentTimeEnd);
     debug_level = ilSavDebugLevel;
  }

  dbg(TRACE,"================ Initializing FOGTAB Finished =================");

  return RC_SUCCESS;
} /* End of InitFog */


static int BuildGroundTimes(char *pclWorkDay)
{
  int ilRC = RC_SUCCESS;

  dbg(TRACE," ");
  dbg(TRACE,"Build Ground Times for Day <%s>",pclWorkDay);

  ilRC = BuildGroundTimesArrivals(pclWorkDay);
  ilRC = BuildGroundTimesDepartures(pclWorkDay);
  ilRC = BuildGroundTimesRotations(pclWorkDay);
  
  return ilRC;
} /* End of BuildGroundTimes */


static int BuildGroundTimesArrivals(char *pclWorkDay)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFktRd1;
  short slCursorRd1;
  short slFktWr;
  short slCursorWr;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclUpdSqlBuf[L_BUFF];
  char pclUpdSelectBuf[L_BUFF];
  char pclUpdTmpBuf[L_BUFF];
  char pclTmpBuf[XS_BUFF];
  char pclUrno[32];
  char pclTifa[32];
  char pclTifd[32];
  char pclOnbl[32];
  char pclRkey[32];
  char pclFtyp[32];
  char pclBaa4[32];
  char pclBaa5[32];
  char pclNewPabs[32];
  char pclNewPaes[32];
  char pclNewPdbs[32];
  char pclNewPdes[32];
  int ilLen;
  int ilTotRecs = 0;
  int ilInsRecs = 0;
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];

  strcpy(pclSqlBuf,"SELECT URNO,TIFA,TIFD,ONBL,RKEY,FTYP,BAA4,BAA5 FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE TIFA LIKE '%s%%' AND DES3 = '%s' AND (RTYP = 'S' OR RTYP = ' ') AND ADID = 'A'",
                       pclWorkDay,pcgHomeAp);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursorRd1 = 0;
  slFktRd1 = START;
  ilRCdb = sql_if(slFktRd1,&slCursorRd1,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     ilTotRecs++;
     BuildItemBuffer(pclDataBuf,"",8,",");
     ilLen = get_real_item(pclUrno,pclDataBuf,1);
     ilLen = get_real_item(pclTifa,pclDataBuf,2);
     ilLen = get_real_item(pclTifd,pclDataBuf,3);
     ilLen = get_real_item(pclOnbl,pclDataBuf,4);
     ilLen = get_real_item(pclRkey,pclDataBuf,5);
     ilLen = get_real_item(pclFtyp,pclDataBuf,6);
     ilLen = get_real_item(pclBaa4,pclDataBuf,7);
     ilLen = get_real_item(pclBaa5,pclDataBuf,8);

		SetDefaultAllocationTimes(pclBaa4,pclBaa5);
     ilRC = CalculateGroundTimes(pclTifa,NULL,NULL,NULL,pclOnbl,NULL,NULL,NULL,"A",1,1,
                                 pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);

		ResetDefaultAllocationTimes();

     sprintf(pclUpdTmpBuf,"PABS='%s',PAES='%s',PDBS='%s',PDES='%s'",
             pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
     strcpy(pclFieldList,"PABS,PAES,PDBS,PDES,TIFA,TIFD,ADID,RKEY,FTYP,URNO");
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes,
             pclTifa,pclTifd,"A",pclRkey,pclFtyp,pclUrno);
     sprintf(pclUpdSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdTmpBuf);
     sprintf(pclUpdSelectBuf,"WHERE URNO = %s",pclUrno);
     strcat(pclUpdSqlBuf,pclUpdSelectBuf);
     slCursorWr = 0;
     slFktWr = START;
     dbg(DEBUG,"<%s>",pclUpdSqlBuf);
     ilRCdb = sql_if(slFktWr,&slCursorWr,pclUpdSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("UFR","AFTTAB",pclUrno,pclFieldList,
                                  pclDataList,FALSE,FALSE);
     }
     close_my_cursor(&slCursorWr);
     if (strlen(pclOnbl) > 0)
     {
        ilRC = InsFOGTAB(pclUrno,"A","O",pclNewPabs,pclNewPaes);
        ilInsRecs++;
     }
     slFktRd1 = NEXT;
     ilRCdb = sql_if(slFktRd1,&slCursorRd1,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursorRd1);
  dbg(TRACE,"There were %d arrival records of AFT updated",ilTotRecs);
  dbg(TRACE,"There were %d arrival records into FOG inserted",ilInsRecs);
  return ilRC;
} /* End of BuildGroundTimesArrivals */


static int BuildGroundTimesDepartures(char *pclWorkDay)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFktRd1;
  short slCursorRd1;
  short slFktWr;
  short slCursorWr;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclUpdSqlBuf[L_BUFF];
  char pclUpdSelectBuf[L_BUFF];
  char pclUpdTmpBuf[L_BUFF];
  char pclTmpBuf[XS_BUFF];
  char pclUrno[32];
  char pclTifa[32];
  char pclTifd[32];
  char pclRkey[32];
  char pclFtyp[32];
  char pclBaa4[32];
  char pclBaa5[32];
  char pclNewPabs[32];
  char pclNewPaes[32];
  char pclNewPdbs[32];
  char pclNewPdes[32];
  int ilLen;
  int ilTotRecs = 0;
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];

  strcpy(pclSqlBuf,"SELECT URNO,TIFA,TIFD,RKEY,FTYP,BAA4,BAA5 FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE TIFD LIKE '%s%%' AND ORG3 = '%s' AND (RTYP = 'S' OR RTYP = ' ') AND ADID = 'D'",
                       pclWorkDay,pcgHomeAp);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursorRd1 = 0;
  slFktRd1 = START;
  ilRCdb = sql_if(slFktRd1,&slCursorRd1,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     ilTotRecs++;
     BuildItemBuffer(pclDataBuf,"",7,",");
     ilLen = get_real_item(pclUrno,pclDataBuf,1);
     ilLen = get_real_item(pclTifa,pclDataBuf,2);
     ilLen = get_real_item(pclTifd,pclDataBuf,3);
     ilLen = get_real_item(pclRkey,pclDataBuf,4);
     ilLen = get_real_item(pclFtyp,pclDataBuf,5);
     ilLen = get_real_item(pclBaa4,pclDataBuf,6);
     ilLen = get_real_item(pclBaa5,pclDataBuf,7);
		 SetDefaultAllocationTimes(pclBaa4,pclBaa5);
     ilRC = CalculateGroundTimes(NULL,NULL,pclTifd,NULL,NULL,NULL,NULL,NULL,"D",1,1,
                                 pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
		ResetDefaultAllocationTimes();

     sprintf(pclUpdTmpBuf,"PDBS='%s',PDES='%s',PABS='%s',PAES='%s'",
             pclNewPdbs,pclNewPdes,pclNewPabs,pclNewPaes);
     strcpy(pclFieldList,"PDBS,PDES,PABS,PAES,TIFA,TIFD,ADID,RKEY,FTYP,URNO");
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",pclNewPdbs,pclNewPdes,pclNewPabs,pclNewPaes,
             pclTifa,pclTifd,"D",pclRkey,pclFtyp,pclUrno);
     sprintf(pclUpdSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdTmpBuf);
     sprintf(pclUpdSelectBuf,"WHERE URNO = %s",pclUrno);
     strcat(pclUpdSqlBuf,pclUpdSelectBuf);
     slCursorWr = 0;
     slFktWr = START;
     dbg(DEBUG,"<%s>",pclUpdSqlBuf);
     ilRCdb = sql_if(slFktWr,&slCursorWr,pclUpdSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("UFR","AFTTAB",pclUrno,pclFieldList,
                                  pclDataList,FALSE,FALSE);
     }
     close_my_cursor(&slCursorWr);
     slFktRd1 = NEXT;
     ilRCdb = sql_if(slFktRd1,&slCursorRd1,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursorRd1);
  dbg(TRACE,"There were %d departure records of AFT updated",ilTotRecs);
  return ilRC;
} /* End of BuildGroundTimesDepartures */


static int BuildGroundTimesRotations(char *pclWorkDay)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFktRd1;
  short slCursorRd1;
  short slFktRd2;
  short slCursorRd2;
  short slFktWr;
  short slCursorWr;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclSqlBuf2[L_BUFF];
  char pclSelectBuf2[L_BUFF];
  char pclDataBuf2[XXL_BUFF];
  char pclUpdSqlBuf[L_BUFF];
  char pclUpdSelectBuf[L_BUFF];
  char pclUpdTmpBuf[L_BUFF];
  char pclTmpBuf[XS_BUFF];
  char pclRkey[32];
  char pclNewPabs[32];
  char pclNewPaes[32];
  char pclNewPdbs[32];
  char pclNewPdes[32];
  int ilLen;
  int ilTotRecs = 0;
  int ilJoinedRecs = 0;
  char pclUrnoList[100][32];
  char pclTifaList[100][32];
  char pclTifdList[100][32];
  char pclOnblList[100][32];
  char pclOfblList[100][32];
  char pclAdidList[100][32];
  char pclFtypList[100][32];
  char pclUrnoDep[32];
  char pclTifaDep[32];
  char pclTifdDep[32];
  char pclOnblDep[32];
  char pclOfblDep[32];
  char pclAdidDep[32];
  char pclFtypDep[32];
  int ilI;
  int ilInsRecs = 0;
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];
  char pclFirstDay[32];
  char pclLastDay[32];
  long llTime1;
  long llTime2;
  long llTime;
  char pclStat[8];
  char pclNewLastDay[32];
  int ilDepRecExists;

  strcpy(pclSqlBuf,"SELECT DISTINCT RKEY FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE TIFA LIKE '%s%%' AND DES3 = '%s' AND ADID = 'A' AND RTYP = 'J'",
                       pclWorkDay,pcgHomeAp,pclWorkDay,pcgHomeAp);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursorRd1 = 0;
  slFktRd1 = START;
  ilRCdb = sql_if(slFktRd1,&slCursorRd1,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     ilJoinedRecs = 0;
     strcpy(pclRkey,pclDataBuf);

     /* Read Arrival Record */
     strcpy(pclSqlBuf2,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,FTYP FROM AFTTAB ");
     sprintf(pclSelectBuf2,"WHERE RKEY = %s AND ADID = 'A'",pclRkey);
     strcat(pclSqlBuf2,pclSelectBuf2);
     slCursorRd2 = 0;
     slFktRd2 = START;
     ilRCdb = sql_if(slFktRd2,&slCursorRd2,pclSqlBuf2,pclDataBuf2);
     if (ilRCdb == DB_SUCCESS)
     {
        ilTotRecs++;
        BuildItemBuffer(pclDataBuf2,"",7,",");
        ilLen = get_real_item(&pclUrnoList[ilJoinedRecs][0],pclDataBuf2,1);
        ilLen = get_real_item(&pclTifaList[ilJoinedRecs][0],pclDataBuf2,2);
        ilLen = get_real_item(&pclTifdList[ilJoinedRecs][0],pclDataBuf2,3);
        ilLen = get_real_item(&pclOnblList[ilJoinedRecs][0],pclDataBuf2,4);
        ilLen = get_real_item(&pclOfblList[ilJoinedRecs][0],pclDataBuf2,5);
        ilLen = get_real_item(&pclAdidList[ilJoinedRecs][0],pclDataBuf2,6);
        strcat(&pclAdidList[ilJoinedRecs][0],"R");
        ilLen = get_real_item(&pclFtypList[ilJoinedRecs][0],pclDataBuf2,7);
        ilJoinedRecs++;
     }
     close_my_cursor(&slCursorRd2);
     /* Read Departure Record */
     strcpy(pclSqlBuf2,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,FTYP FROM AFTTAB ");
     sprintf(pclSelectBuf2,"WHERE RKEY = %s AND ADID = 'D'",pclRkey);
     strcat(pclSqlBuf2,pclSelectBuf2);
     slCursorRd2 = 0;
     slFktRd2 = START;
     ilRCdb = sql_if(slFktRd2,&slCursorRd2,pclSqlBuf2,pclDataBuf2);
     if (ilRCdb == DB_SUCCESS)
     {
        ilTotRecs++;
        BuildItemBuffer(pclDataBuf2,"",7,",");
        ilLen = get_real_item(&pclUrnoDep[0],pclDataBuf2,1);
        ilLen = get_real_item(&pclTifaDep[0],pclDataBuf2,2);
        ilLen = get_real_item(&pclTifdDep[0],pclDataBuf2,3);
        ilLen = get_real_item(&pclOnblDep[0],pclDataBuf2,4);
        ilLen = get_real_item(&pclOfblDep[0],pclDataBuf2,5);
        ilLen = get_real_item(&pclAdidDep[0],pclDataBuf2,6);
        strcat(&pclAdidDep[0],"R");
        ilLen = get_real_item(&pclFtypDep[0],pclDataBuf2,7);
        ilDepRecExists = TRUE;
     }
     else
     {
        ilDepRecExists = FALSE;
     }
     close_my_cursor(&slCursorRd2);
     /* Read Towing Records , etc which are OnBlock */
     strcpy(pclSqlBuf2,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,FTYP FROM AFTTAB ");
     sprintf(pclSelectBuf2,"WHERE RKEY = %s AND ADID = 'B' AND ONBL <> ' ' ORDER BY ADID,TIFA",pclRkey);
     strcat(pclSqlBuf2,pclSelectBuf2);
     slCursorRd2 = 0;
     slFktRd2 = START;
     ilRCdb = sql_if(slFktRd2,&slCursorRd2,pclSqlBuf2,pclDataBuf2);
     while (ilRCdb == DB_SUCCESS)
     {
        ilTotRecs++;
        BuildItemBuffer(pclDataBuf2,"",7,",");
        ilLen = get_real_item(&pclUrnoList[ilJoinedRecs][0],pclDataBuf2,1);
        ilLen = get_real_item(&pclTifaList[ilJoinedRecs][0],pclDataBuf2,2);
        ilLen = get_real_item(&pclTifdList[ilJoinedRecs][0],pclDataBuf2,3);
        ilLen = get_real_item(&pclOnblList[ilJoinedRecs][0],pclDataBuf2,4);
        ilLen = get_real_item(&pclOfblList[ilJoinedRecs][0],pclDataBuf2,5);
        ilLen = get_real_item(&pclAdidList[ilJoinedRecs][0],pclDataBuf2,6);
        strcat(&pclAdidList[ilJoinedRecs][0],"R");
        ilLen = get_real_item(&pclFtypList[ilJoinedRecs][0],pclDataBuf2,7);
        ilJoinedRecs++;
        slFktRd2 = NEXT;
        ilRCdb = sql_if(slFktRd2,&slCursorRd2,pclSqlBuf2,pclDataBuf2);
     }
     close_my_cursor(&slCursorRd2);
     /* Insert Departure Record here, if it is OffBlock */
     if (ilDepRecExists == TRUE && strlen(&pclOfblDep[0]) > 0)
     {
        strcpy(&pclUrnoList[ilJoinedRecs][0],pclUrnoDep);
        strcpy(&pclTifaList[ilJoinedRecs][0],pclTifaDep);
        strcpy(&pclTifdList[ilJoinedRecs][0],pclTifdDep);
        strcpy(&pclOnblList[ilJoinedRecs][0],pclOnblDep);
        strcpy(&pclOfblList[ilJoinedRecs][0],pclOfblDep);
        strcpy(&pclAdidList[ilJoinedRecs][0],pclAdidDep);
        strcpy(&pclFtypList[ilJoinedRecs][0],pclFtypDep);
        ilJoinedRecs++;
     }
     else
     {
        /* Read Towing Records , etc which are not yet OnBlock */
        strcpy(pclSqlBuf2,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,FTYP FROM AFTTAB ");
        sprintf(pclSelectBuf2,"WHERE RKEY = %s AND ADID = 'B' AND ONBL = ' ' ORDER BY ADID,TIFA",pclRkey);
        strcat(pclSqlBuf2,pclSelectBuf2);
        slCursorRd2 = 0;
        slFktRd2 = START;
        ilRCdb = sql_if(slFktRd2,&slCursorRd2,pclSqlBuf2,pclDataBuf2);
        while (ilRCdb == DB_SUCCESS)
        {
           ilTotRecs++;
           BuildItemBuffer(pclDataBuf2,"",7,",");
           ilLen = get_real_item(&pclUrnoList[ilJoinedRecs][0],pclDataBuf2,1);
           ilLen = get_real_item(&pclTifaList[ilJoinedRecs][0],pclDataBuf2,2);
           ilLen = get_real_item(&pclTifdList[ilJoinedRecs][0],pclDataBuf2,3);
           ilLen = get_real_item(&pclOnblList[ilJoinedRecs][0],pclDataBuf2,4);
           ilLen = get_real_item(&pclOfblList[ilJoinedRecs][0],pclDataBuf2,5);
           ilLen = get_real_item(&pclAdidList[ilJoinedRecs][0],pclDataBuf2,6);
           strcat(&pclAdidList[ilJoinedRecs][0],"R");
           ilLen = get_real_item(&pclFtypList[ilJoinedRecs][0],pclDataBuf2,7);
           ilJoinedRecs++;
           slFktRd2 = NEXT;
           ilRCdb = sql_if(slFktRd2,&slCursorRd2,pclSqlBuf2,pclDataBuf2);
        }
        close_my_cursor(&slCursorRd2);
     }
     /* Insert Departure Record here, if it is not OffBlock */
     if (ilDepRecExists == TRUE && strlen(&pclOfblDep[0]) == 0)
     {
        strcpy(&pclUrnoList[ilJoinedRecs][0],pclUrnoDep);
        strcpy(&pclTifaList[ilJoinedRecs][0],pclTifaDep);
        strcpy(&pclTifdList[ilJoinedRecs][0],pclTifdDep);
        strcpy(&pclOnblList[ilJoinedRecs][0],pclOnblDep);
        strcpy(&pclOfblList[ilJoinedRecs][0],pclOfblDep);
        strcpy(&pclAdidList[ilJoinedRecs][0],pclAdidDep);
        strcpy(&pclFtypList[ilJoinedRecs][0],pclFtypDep);
        ilJoinedRecs++;
     }

     if (ilJoinedRecs == 1)
     {
        pclAdidList[0][1] = '\0';
        strcpy(pclLastDay,pcgPaesArr);
     }

     for (ilI = 0; ilI < ilJoinedRecs; ilI++)
     {
        if (ilI == 0)
        {
           ilRC = CalculateGroundTimes(&pclTifaList[ilI][0],NULL,
                                       &pclTifdList[ilI][0],&pclTifdList[ilI+1][0],
                                       &pclOnblList[ilI][0],NULL,
                                       &pclOfblList[ilI][0],&pclOfblList[ilI+1][0],
                                       &pclAdidList[ilI][0],ilI,ilJoinedRecs,
                                       pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
           strcpy(pclFirstDay,pclNewPabs);
        }
        else
        {
           if (ilI == ilJoinedRecs-1)
           {
              ilRC = CalculateGroundTimes(&pclTifaList[ilI][0],&pclTifaList[ilI-1][0],
                                          &pclTifdList[ilI][0],NULL,
                                          &pclOnblList[ilI][0],&pclOnblList[ilI-1][0],
                                          &pclOfblList[ilI][0],NULL,
                                          &pclAdidList[ilI][0],ilI,ilJoinedRecs,
                                          pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
              strcpy(pclLastDay,pclNewPdes);
           }
           else
           {
              ilRC = CalculateGroundTimes(&pclTifaList[ilI][0],&pclTifaList[ilI-1][0],
                                          &pclTifdList[ilI][0],&pclTifdList[ilI+1][0],
                                          &pclOnblList[ilI][0],&pclOnblList[ilI-1][0],
                                          &pclOfblList[ilI][0],&pclOfblList[ilI+1][0],
                                          &pclAdidList[ilI][0],ilI,ilJoinedRecs,
                                          pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
           }
        }
        sprintf(pclUpdTmpBuf,"PABS='%s',PAES='%s',PDBS='%s',PDES='%s'",
                pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
        strcpy(pclFieldList,"PABS,PAES,PDBS,PDES,TIFA,TIFD,ADID,RKEY,FTYP,URNO");
        sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%c,%s,%s,%s",pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes,
                &pclTifaList[ilI][0],&pclTifdList[ilI][0],
                pclAdidList[ilI][0],pclRkey,&pclFtypList[ilI][0],&pclUrnoList[ilI][0]);
        sprintf(pclUpdSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdTmpBuf);
        sprintf(pclUpdSelectBuf,"WHERE URNO = %s",&pclUrnoList[ilI][0]);
        strcat(pclUpdSqlBuf,pclUpdSelectBuf);
        slCursorWr = 0;
        slFktWr = START;
        dbg(DEBUG,"<%s>",pclUpdSqlBuf);
        ilRCdb = sql_if(slFktWr,&slCursorWr,pclUpdSqlBuf,pclDataBuf);
        if (ilRCdb == DB_SUCCESS)
        {
           commit_work();
           ilRC = TriggerBchdlAction("UFR","AFTTAB",&pclUrnoList[ilI][0],pclFieldList,
                                     pclDataList,FALSE,FALSE);
        }
        close_my_cursor(&slCursorWr);
     }
     if (strcmp(pclLastDay,pcgPaesArr) != 0)
     {
        llTime1 = GetSecondsFromCEDATime(pclFirstDay);
        llTime2 = GetSecondsFromCEDATime(pclLastDay);
        llTime = llTime2 - llTime1;
        if (llTime > igOnGroundTimeLimit)
        {
           if (strlen(&pclOnblList[0][0]) == 0)
           {
              strcpy(pclStat,"S");
           }
           else
           {
              if (strlen(&pclOfblList[ilJoinedRecs-1][0]) == 0)
              {
                 strcpy(pclStat,"O");
                 ilRC = TimeToStr(pclNewLastDay,time(NULL));
                 if (strcmp(pclNewLastDay,pclLastDay) > 0)
                 {
                    strcpy(pclLastDay,pclNewLastDay);
                 }
              }
              else
              {
                 strcpy(pclStat,"C");
              }
           }
           ilRC = InsFOGTAB(&pclUrnoList[0][0],"R",pclStat,pclFirstDay,pclLastDay);
           ilInsRecs++;
        }
        else
        {
           if (strlen(&pclOnblList[0][0]) > 0 &&
               strlen(&pclOfblList[ilJoinedRecs-1][0]) == 0)
           {
              ilRC = TimeToStr(pclNewLastDay,time(NULL));
              if (strcmp(pclNewLastDay,pclLastDay) > 0)
              {
                 strcpy(pclLastDay,pclNewLastDay);
              }
              ilRC = InsFOGTAB(&pclUrnoList[0][0],"P","O",pclFirstDay,pclLastDay);
              ilInsRecs++;
           }
        }
     }
     else
     {
        if (strlen(&pclOnblList[0][0]) > 0)
        {
           ilRC = InsFOGTAB(&pclUrnoList[0][0],"R","O",pclFirstDay,pclLastDay);
           ilInsRecs++;
        }
     }
     slFktRd1 = NEXT;
     ilRCdb = sql_if(slFktRd1,&slCursorRd1,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursorRd1);
  dbg(TRACE,"There were %d rotation records of AFT updated",ilTotRecs);
  dbg(TRACE,"There were %d rotation records into FOG inserted",ilInsRecs);
  return ilRC;
} /* End of BuildGroundTimesRotations */


static void SetDefaultAllocationTimes(char *pcpBAA4,char *pcpBAA5)
{
		igPaesArr = atoi(pcpBAA4);
		igPdbsDep = atoi(pcpBAA5);
}

static void ResetDefaultAllocationTimes()
{
		igPaesArr = igPaesArrOri;
		igPdbsDep = igPdbsDepOri;
}

static int CalculateGroundTimes(char *pcpTifa, char *pcpTifaPrev,
                                char *pcpTifd, char *pcpTifdNext,
                                char *pcpOnbl, char *pcpOnblPrev,
                                char *pcpOfbl, char *pcpOfblNext,
                                char *pcpType, int ipCur, int ipMax,
                                char *pcpPabs, char *pcpPaes, char *pcpPdbs, char *pcpPdes)
{
  int ilRC = RC_SUCCESS;
  long llTime1;
  long llTime2;
  long llTime;
  char pclCurTime[32];

  if (strcmp(pcpType,"A") == 0)
  {
/********** Single Arrival Record **********/
     strcpy(pcpPabs,pcpTifa);
     ilRC = MyAddSecondsToCEDATime(pcpPabs,igPabsArr*60,1);
     if (strlen(pcpOnbl) == 0)
     {
        strcpy(pcpPaes,pcpTifa);
        ilRC = MyAddSecondsToCEDATime(pcpPaes,igPaesArr*60,1);
     }
     else
     {
        strcpy(pcpPaes,pcgPaesArr);
     }
     strcpy(pcpPdbs," ");
     strcpy(pcpPdes," ");
  }
  else
  {
/********** Single Departure Record **********/
     if (strcmp(pcpType,"D") == 0)
     {
        strcpy(pcpPdbs,pcpTifd);
        ilRC = MyAddSecondsToCEDATime(pcpPdbs,igPdbsDep*(-60),1);
        strcpy(pcpPdes,pcpTifd);
        ilRC = MyAddSecondsToCEDATime(pcpPdes,igPdesDep*(-60),1);
        strcpy(pcpPabs," ");
        strcpy(pcpPaes," ");
     }
     else
     {
        if (ipCur == 0)
        {
/********** First Record of a Rotation Chain **********/
           strcpy(pcpPabs,pcpTifa);
           ilRC = MyAddSecondsToCEDATime(pcpPabs,igPabsArr*60,1);
           llTime1 = GetSecondsFromCEDATime(pcpTifdNext);
           llTime = time(NULL);
           if (strlen(pcpOnbl) > 0 && strlen(pcpOfblNext) == 0 && llTime1 < llTime)
           {  /* current time is after scheduled departure time */
              llTime1 = llTime;
           }
           llTime2 = GetSecondsFromCEDATime(pcpTifa);
           llTime = (llTime1 - llTime2) * igRotTimeArrPerc / 100 + igPaesRot * 60;
           strcpy(pcpPaes,pcpTifa);
           if ((llTime1 - llTime2) > 0)
           {
              ilRC = MyAddSecondsToCEDATime(pcpPaes,llTime,1);
           }
           strcpy(pcpPdbs," ");
           strcpy(pcpPdes," ");
        }
        else
        {
           if (ipCur == ipMax-1)
/********** Last Record of a Rotation Chain **********/
           {
              if (strcmp(pcpType,"DR") == 0)
              {
/********** Last Record is a Departure Record **********/
                 llTime1 = GetSecondsFromCEDATime(pcpTifd);
                 llTime = time(NULL);
                 if (strlen(pcpOnblPrev) > 0 && strlen(pcpOfbl) == 0 && llTime1 < llTime)
                 {  /* current time is after scheduled departure time */
                    llTime1 = llTime;
                 }
                 llTime2 = GetSecondsFromCEDATime(pcpTifaPrev);
                 llTime = (llTime1 - llTime2) * igRotTimeArrPerc / 100 + igPdbsRot * 60;
                 strcpy(pcpPdbs,pcpTifaPrev);
                 if ((llTime1 - llTime2) > 0)
                 {
                    ilRC = MyAddSecondsToCEDATime(pcpPdbs,llTime,1);
                 }
                 ilRC = TimeToStr(pclCurTime,time(NULL));
                 strcpy(pcpPdes,pcpTifd);
                 if (strlen(pcpOnblPrev) > 0 && strlen(pcpOfbl) == 0 && 
                     strcmp(pcpTifd,pclCurTime) < 0)
                 {  /* current time is after scheduled departure time */
                    strcpy(pcpPdes,pclCurTime);
                 }
                 ilRC = MyAddSecondsToCEDATime(pcpPdes,igPdesDep*60*(-1),1);
                 strcpy(pcpPabs," ");
                 strcpy(pcpPaes," ");
              }
              else
              {
/********** Last Record is a Return Flight or Towing Record , etc **********/
                 llTime1 = GetSecondsFromCEDATime(pcpTifd);
                 llTime = time(NULL);
                 if (strlen(pcpOnblPrev) > 0 && strlen(pcpOfbl) == 0 && llTime1 < llTime)
                 {  /* current time is after scheduled departure time */
                    llTime1 = llTime;
                 }
                 llTime2 = GetSecondsFromCEDATime(pcpTifaPrev);
                 llTime = (llTime1 - llTime2) * igRotTimeArrPerc / 100 + igPdbsRot * 60;
                 strcpy(pcpPdbs,pcpTifaPrev);
                 ilRC = MyAddSecondsToCEDATime(pcpPdbs,llTime,1);
                 ilRC = TimeToStr(pclCurTime,time(NULL));
                 strcpy(pcpPdes,pcpTifd);
                 if (strlen(pcpOnblPrev) > 0 && strlen(pcpOfbl) == 0 && 
                     strcmp(pcpTifd,pclCurTime) < 0)
                 {  /* current time is after scheduled departure time */
                    strcpy(pcpPdes,pclCurTime);
                 }
                 ilRC = MyAddSecondsToCEDATime(pcpPdes,igPdesRot*60*(-1),1);
                 strcpy(pcpPabs,pcpTifa);
                 ilRC = MyAddSecondsToCEDATime(pcpPabs,igPabsArr*60,1);
                 if (strlen(pcpOnbl) == 0)
                 {
                    strcpy(pcpPaes,pcpTifa);
                    ilRC = MyAddSecondsToCEDATime(pcpPaes,igPaesArr*60,1);
                 }
                 else
                 {
                    strcpy(pcpPaes,pcgPaesArr);
                 }
              }
           }
           else
           {
/********** Return Flights or Towing Records ,etc in middle of a Rotation Chain **********/

/********** Calculate PABS **********/
              if (strcmp(pcpType,"DR") == 0)
              {
                 strcpy(pcpPabs," ");
              }
              else
              {
                 strcpy(pcpPabs,pcpTifa);
                 ilRC = MyAddSecondsToCEDATime(pcpPabs,igPabsRot*60,1);
              }
/********** Calculate PAES **********/
              if (strcmp(pcpType,"DR") == 0)
              {
                 strcpy(pcpPaes," ");
              }
              else
              {
                 llTime1 = GetSecondsFromCEDATime(pcpTifdNext);
                 llTime = time(NULL);
                 if (strlen(pcpOnbl) > 0 && strlen(pcpOfblNext) == 0 && llTime1 < llTime)
                 {  /* current time is after scheduled departure time */
                    llTime1 = llTime;
                 }
                 llTime2 = GetSecondsFromCEDATime(pcpTifa);
                 llTime = (llTime1 - llTime2) * igRotTimeArrPerc / 100 + igPaesRot * 60;
                 strcpy(pcpPaes,pcpTifa);
                 if ((llTime1 - llTime2) > 0)
                 {
                    ilRC = MyAddSecondsToCEDATime(pcpPaes,llTime,1);
                 }
              }
/********** Calculate PDBS **********/
              llTime1 = GetSecondsFromCEDATime(pcpTifd);
              llTime = time(NULL);
              if (strlen(pcpOnblPrev) > 0 && strlen(pcpOfbl) == 0 && llTime1 < llTime)
              {  /* current time is after scheduled departure time */
                 llTime1 = llTime;
              }
              llTime2 = GetSecondsFromCEDATime(pcpTifaPrev);
              llTime = (llTime1 - llTime2) * igRotTimeArrPerc / 100 + igPdbsRot * 60;
              strcpy(pcpPdbs,pcpTifaPrev);
              if ((llTime1 - llTime2) > 0)
              {
                 ilRC = MyAddSecondsToCEDATime(pcpPdbs,llTime,1);
              }
/********** Calculate PDES **********/
              ilRC = TimeToStr(pclCurTime,time(NULL));
              strcpy(pcpPdes,pcpTifd);
              if (strlen(pcpOnblPrev) > 0 && strlen(pcpOfbl) == 0 && 
                  strcmp(pcpTifd,pclCurTime) < 0)
              {  /* current time is after scheduled departure time */
                 strcpy(pcpPdes,pclCurTime);
              }
              ilRC = MyAddSecondsToCEDATime(pcpPdes,igPdesRot*60*(-1),1);
           }
        }
     }
  }

  return ilRC;
} /* End of Calculate GroundTimes */


static int InsFOGTAB(char *pcpUrno, char *pcpType, char *pcpStat, 
                     char *pcpFirstDay, char *pcpLastDay)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBufIns[L_BUFF];
  char pclSqlBufFields[L_BUFF];
  char pclSqlBufValues[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclNewUrno[32];
  int ilDebugLevel;
  char pclDataList[L_BUFF];

  ilDebugLevel = debug_level;
  debug_level = 0;
  GetNextValues(pclNewUrno,1);
  debug_level = ilDebugLevel;
  strcpy(pclSqlBufFields,"URNO,HOPO,FLNU,OGBS,OGES,FTYP,STAT");
  sprintf(pclSqlBufValues,"%s,'%s',%s,'%s','%s','%s','%s'",
          pclNewUrno,pcgHomeAp,pcpUrno,pcpFirstDay,pcpLastDay,pcpType,pcpStat);
  sprintf(pclSqlBufIns,"INSERT INTO FOGTAB FIELDS(%s) VALUES(%s)",
          pclSqlBufFields,pclSqlBufValues);
  sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s",
          pclNewUrno,pcgHomeAp,pcpUrno,pcpFirstDay,pcpLastDay,pcpType,pcpStat);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"InsFOGTAB: <%s>",pclSqlBufIns);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBufIns,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     commit_work();
     ilRC = TriggerBchdlAction("IRT","FOGTAB",pclNewUrno,pclSqlBufFields,
                               pclDataList,FALSE,TRUE);
  }
  close_my_cursor(&slCursor);
  
  return ilRC;
} /* End of InsFOGTAB */


static int CheckFOGTAB()
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  short slFktWr;
  short slCursorWr;
  char pclSqlBufWr[L_BUFF];
  int ilNoRecs = 0;
  int ilUpdRecs = 0;
  char pclUrno[32];
  char pclFlnu[32];
  char pclOgbs[32];
  char pclOges[32];
  char pclFtyp[32];
  char pclLastDay[32];
  char pclNewFtyp[32];
  int ilLen;
  long llTime1;
  long llTime2;
  long llTime;
  char pclFieldList[256];
  char pclDataList[256];
  char pclUpdBuf[256];

  strcpy(pclSqlBuf,"SELECT URNO,FLNU,OGBS,OGES,FTYP FROM FOGTAB ");
  sprintf(pclSelectBuf,"WHERE STAT = 'O' AND (FTYP = 'R' OR FTYP = 'P')");
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     ilNoRecs++;
     BuildItemBuffer(pclDataBuf,"",5,",");
     ilLen = get_real_item(pclUrno,pclDataBuf,1);
     ilLen = get_real_item(pclFlnu,pclDataBuf,2);
     ilLen = get_real_item(pclOgbs,pclDataBuf,3);
     ilLen = get_real_item(pclOges,pclDataBuf,4);
     ilLen = get_real_item(pclFtyp,pclDataBuf,5);
     ilRC = TimeToStr(pclLastDay,time(NULL));
     if (strcmp(pclLastDay,pclOges) > 0)
     {
        ilUpdRecs++;
        if (strcmp(pclFtyp,"P") == 0)
        {
           llTime1 = GetSecondsFromCEDATime(pclOgbs);
           llTime2 = GetSecondsFromCEDATime(pclLastDay);
           llTime = llTime2 - llTime1;
           if (llTime > igOnGroundTimeLimit)
           {
              strcpy(pclNewFtyp,"R");
           }
           else
           {
              strcpy(pclNewFtyp,"P");
           }
        }
        else
        {
           strcpy(pclNewFtyp,"R");
        }
        sprintf(pclUpdBuf,"OGES='%s',FTYP='%s'",pclLastDay,pclNewFtyp);
        strcpy(pclFieldList,"OGES,FTYP");
        sprintf(pclDataList,"%s,%s",pclLastDay,pclNewFtyp);
        sprintf(pclSqlBufWr,"UPDATE FOGTAB SET %s WHERE URNO = %s",
                pclUpdBuf,pclUrno);
        slCursorWr = 0;
        slFktWr = START;
        dbg(DEBUG,"CheckFOGTAB: <%s>",pclSqlBufWr);
        ilRCdb = sql_if(slFktWr,&slCursorWr,pclSqlBufWr,pclDataBuf);
        if (ilRCdb == DB_SUCCESS)
        {
           commit_work();
           ilRC = TriggerBchdlAction("URT","FOGTAB",pclUrno,pclFieldList,
                                     pclDataList,FALSE,TRUE);
        }
        close_my_cursor(&slCursorWr);
     }
     ilRC = UpdateFOGTAB(pclFlnu);   /* Now recalculate Position Times */
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);
  
  dbg(TRACE,"CheckFOGTAB: There were %d recs found and %d recs updated",ilNoRecs,ilUpdRecs);
  
  return ilRC;
} /* End of CheckFOGTAB */


static int UpdateFOGTAB(char *pcpUrno)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclDataBuf2[XXL_BUFF];
  char pclTifa[32];
  char pclTifd[32];
  char pclPabs[32];
  char pclPaes[32];
  char pclPdbs[32];
  char pclPdes[32];
  char pclRtyp[32];
  char pclRkey[32];
  char pclAdid[32];
  char pclOnbl[32];
  char pclFtyp[32];
  char pclBaa4[32];
  char pclBaa5[32];
  int ilLen;
  int ilJoinedArrivals = FALSE;

  strcpy(pclSqlBuf,"SELECT TIFA,TIFD,PABS,PAES,PDBS,PDES,RTYP,RKEY,ADID,ONBL,FTYP,BAA4,BAA5 FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb != DB_SUCCESS)
  {
     close_my_cursor(&slCursor);
     strcpy(pclSqlBuf,"DELETE FROM FOGTAB ");
     sprintf(pclSelectBuf,"WHERE FLNU = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"UpdateFOGTAB: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);
     return ilRC;
  }
  close_my_cursor(&slCursor);

  BuildItemBuffer(pclDataBuf,"",13,",");
  ilLen = get_real_item(pclTifa,pclDataBuf,1);
  ilLen = get_real_item(pclTifd,pclDataBuf,2);
  ilLen = get_real_item(pclPabs,pclDataBuf,3);
  if (ilLen == 0)
  {
     strcpy(pclPabs," ");
  }
  ilLen = get_real_item(pclPaes,pclDataBuf,4);
  if (ilLen == 0)
  {
     strcpy(pclPaes," ");
  }
  ilLen = get_real_item(pclPdbs,pclDataBuf,5);
  if (ilLen == 0)
  {
     strcpy(pclPdbs," ");
  }
  ilLen = get_real_item(pclPdes,pclDataBuf,6);
  if (ilLen == 0)
  {
     strcpy(pclPdes," ");
  }
  ilLen = get_real_item(pclRtyp,pclDataBuf,7);
  if (ilLen == 0)
  {
     strcpy(pclRtyp,"S");
  }
  ilLen = get_real_item(pclRkey,pclDataBuf,8);
  ilLen = get_real_item(pclAdid,pclDataBuf,9);
  ilLen = get_real_item(pclOnbl,pclDataBuf,10);
  ilLen = get_real_item(pclFtyp,pclDataBuf,11);
  ilLen = get_real_item(pclBaa4,pclDataBuf,12);
  ilLen = get_real_item(pclBaa5,pclDataBuf,13);

  if (strcmp(pclAdid,"A") == 0 && strcmp(pclRtyp,"S") == 0)
  {
     strcpy(pclSqlBuf,"SELECT URNO FROM AFTTAB ");
     sprintf(pclSelectBuf,"WHERE RKEY = %s ORDER BY TIFA",pclRkey);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf2);
     if (ilRCdb == DB_SUCCESS)
     {
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf2);
        if (ilRCdb == DB_SUCCESS)
        {
           ilJoinedArrivals = TRUE;
        }
     }
     close_my_cursor(&slCursor);
     if (ilJoinedArrivals == TRUE)
     {
        ilRC = UpdateRotation(pcpUrno,pclRkey);
     }
     else
     {
				SetDefaultAllocationTimes(pclBaa4,pclBaa5);
        ilRC = UpdateArrival(pcpUrno,pclTifa,pclTifd,pclPabs,pclPaes,pclPdbs,pclPdes,pclOnbl,
                             pclRkey,pclFtyp);
			  ResetDefaultAllocationTimes();
     }
  }
  else
  {
     if (strcmp(pclAdid,"D") == 0 && strcmp(pclRtyp,"S") == 0)
     {
				SetDefaultAllocationTimes(pclBaa4,pclBaa5);
        ilRC = UpdateDeparture(pcpUrno,pclTifa,pclTifd,pclPabs,pclPaes,pclPdbs,pclPdes,pclRkey,
                               pclFtyp);
		    ResetDefaultAllocationTimes();

     }
     else
     {
        if (strcmp(pclRtyp,"J") == 0)
        {
           ilRC = UpdateRotation(pcpUrno,pclRkey);
        }
     }
  }

  return ilRC;
} /* End of UpdateFOGTAB */


static int UpdateArrival(char *pcpUrno, char *pcpTifa, char *pcpTifd, char *pcpPabs,
                         char *pcpPaes, char *pcpPdbs, char *pcpPdes, char *pcpOnbl,
                         char *pcpRkey, char *pcpFtyp)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slCursor;
  short slFkt;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclNewPabs[32];
  char pclNewPaes[32];
  char pclNewPdbs[32];
  char pclNewPdes[32];
  char pclUpdBuf[L_BUFF];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];

  dbg(DEBUG,"UpdateArrival:");
  ilRC = CalculateGroundTimes(pcpTifa,NULL,NULL,NULL,pcpOnbl,NULL,NULL,NULL,"A",1,1,
                              pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
  if (strcmp(pclNewPabs,pcpPabs) != 0 || strcmp(pclNewPaes,pcpPaes) != 0 ||
      strcmp(pclNewPdbs,pcpPdbs) != 0 || strcmp(pclNewPdes,pcpPdes) != 0)
  {
     sprintf(pclUpdBuf,"PABS='%s',PAES='%s',PDBS='%s',PDES='%s'",
             pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
     strcpy(pclFieldList,"PABS,PAES,PDBS,PDES,TIFA,TIFD,ADID,RKEY,FTYP,URNO");
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
             pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes,pcpTifa,pcpTifd,"A",pcpRkey,
             pcpFtyp,pcpUrno);
     ilRC = CheckBcMode(pclNewPabs,pcpPabs,pclNewPaes,pcpPaes,
                        pclNewPdbs,pcpPdbs,pclNewPdes,pcpPdes,
                        pclFieldList,pclDataList);
     sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(DEBUG,"UpdateArrival: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("UFR","AFTTAB",pcpUrno,pclFieldList,
                                  pclDataList,TRUE,TRUE);
     }
     close_my_cursor(&slCursor);
  }

  ilRC = UpdFOGTAB(pcpUrno,pclNewPabs,pclNewPaes,"A",pcpOnbl,"");

  return ilRC;
} /* End of UpdateArrival */


static int UpdateDeparture(char *pcpUrno, char *pcpTifa, char *pcpTifd, char *pcpPabs,
                           char *pcpPaes, char *pcpPdbs, char *pcpPdes, char *pcpRkey,
                           char *pcpFtyp)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slCursor;
  short slFkt;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclNewPabs[32];
  char pclNewPaes[32];
  char pclNewPdbs[32];
  char pclNewPdes[32];
  char pclUpdBuf[L_BUFF];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];

  dbg(DEBUG,"UpdateDeparture:");
  ilRC = CalculateGroundTimes(NULL,NULL,pcpTifd,NULL,NULL,NULL,NULL,NULL,"D",1,1,
                              pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
  if (strcmp(pclNewPabs,pcpPabs) != 0 || strcmp(pclNewPaes,pcpPaes) != 0 ||
      strcmp(pclNewPdbs,pcpPdbs) != 0 || strcmp(pclNewPdes,pcpPdes) != 0)
  {
     sprintf(pclUpdBuf,"PABS='%s',PAES='%s',PDBS='%s',PDES='%s'",
             pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
     strcpy(pclFieldList,"PABS,PAES,PDBS,PDES,TIFA,TIFD,ADID,RKEY,FTYP,URNO");
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
             pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes,pcpTifa,pcpTifd,"D",pcpRkey,
             pcpFtyp,pcpUrno);
     ilRC = CheckBcMode(pclNewPabs,pcpPabs,pclNewPaes,pcpPaes,
                        pclNewPdbs,pcpPdbs,pclNewPdes,pcpPdes,
                        pclFieldList,pclDataList);
     sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(DEBUG,"UpdateDeparture: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("UFR","AFTTAB",pcpUrno,pclFieldList,
                                  pclDataList,TRUE,TRUE);
     }
     close_my_cursor(&slCursor);
  }

  ilRC = UpdFOGTAB(pcpUrno,pclNewPdbs,pclNewPdes,"D","","");

  return ilRC;
} /* End of UpdateDeparture */


static int UpdateRotation(char *pcpUrno, char *pcpRkey)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclUrnoList[100][32];
  char pclTifaList[100][32];
  char pclTifdList[100][32];
  char pclOnblList[100][32];
  char pclOfblList[100][32];
  char pclAdidList[100][32];
  char pclPabsList[100][32];
  char pclPaesList[100][32];
  char pclPdbsList[100][32];
  char pclPdesList[100][32];
  char pclFtypList[100][32];
  char pclUrnoDep[32];
  char pclTifaDep[32];
  char pclTifdDep[32];
  char pclOnblDep[32];
  char pclOfblDep[32];
  char pclAdidDep[32];
  char pclPabsDep[32];
  char pclPaesDep[32];
  char pclPdbsDep[32];
  char pclPdesDep[32];
  char pclFtypDep[32];
  char pclNewPabs[32];
  char pclNewPaes[32];
  char pclNewPdbs[32];
  char pclNewPdes[32];
  int ilLen;
  int ilJoinedRecs = 0;
  int ilI;
  char pclUpdBuf[L_BUFF];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];
  char pclFirstDay[32];
  char pclLastDay[32];
  int ilDepRecExists;

  dbg(DEBUG,"UpdateRotation:");
  /* Read Arrival Record */
  strcpy(pclSqlBuf,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,PABS,PAES,PDBS,PDES,FTYP FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE RKEY = %s AND ADID = 'A'",pcpRkey);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",11,",");
     ilLen = get_real_item(&pclUrnoList[ilJoinedRecs][0],pclDataBuf,1);
     ilLen = get_real_item(&pclTifaList[ilJoinedRecs][0],pclDataBuf,2);
     ilLen = get_real_item(&pclTifdList[ilJoinedRecs][0],pclDataBuf,3);
     ilLen = get_real_item(&pclOnblList[ilJoinedRecs][0],pclDataBuf,4);
     ilLen = get_real_item(&pclOfblList[ilJoinedRecs][0],pclDataBuf,5);
     ilLen = get_real_item(&pclAdidList[ilJoinedRecs][0],pclDataBuf,6);
     strcat(&pclAdidList[ilJoinedRecs][0],"R");
     ilLen = get_real_item(&pclPabsList[ilJoinedRecs][0],pclDataBuf,7);
     ilLen = get_real_item(&pclPaesList[ilJoinedRecs][0],pclDataBuf,8);
     ilLen = get_real_item(&pclPdbsList[ilJoinedRecs][0],pclDataBuf,9);
     ilLen = get_real_item(&pclPdesList[ilJoinedRecs][0],pclDataBuf,10);
     ilLen = get_real_item(&pclFtypList[ilJoinedRecs][0],pclDataBuf,11);
     ilJoinedRecs++;
  }
  close_my_cursor(&slCursor);
  /* Read Departure Record */
  strcpy(pclSqlBuf,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,PABS,PAES,PDBS,PDES,FTYP FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE RKEY = %s AND ADID = 'D'",pcpRkey);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",11,",");
     ilLen = get_real_item(&pclUrnoDep[0],pclDataBuf,1);
     ilLen = get_real_item(&pclTifaDep[0],pclDataBuf,2);
     ilLen = get_real_item(&pclTifdDep[0],pclDataBuf,3);
     ilLen = get_real_item(&pclOnblDep[0],pclDataBuf,4);
     ilLen = get_real_item(&pclOfblDep[0],pclDataBuf,5);
     ilLen = get_real_item(&pclAdidDep[0],pclDataBuf,6);
     strcat(&pclAdidDep[0],"R");
     ilLen = get_real_item(&pclPabsDep[0],pclDataBuf,7);
     ilLen = get_real_item(&pclPaesDep[0],pclDataBuf,8);
     ilLen = get_real_item(&pclPdbsDep[0],pclDataBuf,9);
     ilLen = get_real_item(&pclPdesDep[0],pclDataBuf,10);
     ilLen = get_real_item(&pclFtypDep[0],pclDataBuf,11);
     ilDepRecExists = TRUE;
  }
  else
  {
     ilDepRecExists = FALSE;
  }
  close_my_cursor(&slCursor);
  /* Read Towing Records , etc which are OnBlock */
  strcpy(pclSqlBuf,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,PABS,PAES,PDBS,PDES,FTYP FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE RKEY = %s AND ADID = 'B' AND ONBL <> ' ' ORDER BY ADID,TIFA",pcpRkey);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",11,",");
     ilLen = get_real_item(&pclUrnoList[ilJoinedRecs][0],pclDataBuf,1);
     ilLen = get_real_item(&pclTifaList[ilJoinedRecs][0],pclDataBuf,2);
     ilLen = get_real_item(&pclTifdList[ilJoinedRecs][0],pclDataBuf,3);
     ilLen = get_real_item(&pclOnblList[ilJoinedRecs][0],pclDataBuf,4);
     ilLen = get_real_item(&pclOfblList[ilJoinedRecs][0],pclDataBuf,5);
     ilLen = get_real_item(&pclAdidList[ilJoinedRecs][0],pclDataBuf,6);
     strcat(&pclAdidList[ilJoinedRecs][0],"R");
     ilLen = get_real_item(&pclPabsList[ilJoinedRecs][0],pclDataBuf,7);
     ilLen = get_real_item(&pclPaesList[ilJoinedRecs][0],pclDataBuf,8);
     ilLen = get_real_item(&pclPdbsList[ilJoinedRecs][0],pclDataBuf,9);
     ilLen = get_real_item(&pclPdesList[ilJoinedRecs][0],pclDataBuf,10);
     ilLen = get_real_item(&pclFtypList[ilJoinedRecs][0],pclDataBuf,11);
     ilJoinedRecs++;
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);
  /* Insert Departure Record here, if it is OffBlock */
  if (ilDepRecExists == TRUE && strlen(&pclOfblDep[0]) > 0)
  {
     strcpy(&pclUrnoList[ilJoinedRecs][0],pclUrnoDep);
     strcpy(&pclTifaList[ilJoinedRecs][0],pclTifaDep);
     strcpy(&pclTifdList[ilJoinedRecs][0],pclTifdDep);
     strcpy(&pclOnblList[ilJoinedRecs][0],pclOnblDep);
     strcpy(&pclOfblList[ilJoinedRecs][0],pclOfblDep);
     strcpy(&pclAdidList[ilJoinedRecs][0],pclAdidDep);
     strcpy(&pclPabsList[ilJoinedRecs][0],pclPabsDep);
     strcpy(&pclPaesList[ilJoinedRecs][0],pclPaesDep);
     strcpy(&pclPdbsList[ilJoinedRecs][0],pclPdbsDep);
     strcpy(&pclPdesList[ilJoinedRecs][0],pclPdesDep);
     strcpy(&pclFtypList[ilJoinedRecs][0],pclFtypDep);
     ilJoinedRecs++;
  }
  else
  {
     /* Read Towing Records , etc whic are not yet OnBlock */
     strcpy(pclSqlBuf,"SELECT URNO,TIFA,TIFD,ONBL,OFBL,ADID,PABS,PAES,PDBS,PDES,FTYP FROM AFTTAB ");
     sprintf(pclSelectBuf,"WHERE RKEY = %s AND ADID = 'B' AND ONBL = ' ' ORDER BY ADID,TIFA",pcpRkey);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        BuildItemBuffer(pclDataBuf,"",11,",");
        ilLen = get_real_item(&pclUrnoList[ilJoinedRecs][0],pclDataBuf,1);
        ilLen = get_real_item(&pclTifaList[ilJoinedRecs][0],pclDataBuf,2);
        ilLen = get_real_item(&pclTifdList[ilJoinedRecs][0],pclDataBuf,3);
        ilLen = get_real_item(&pclOnblList[ilJoinedRecs][0],pclDataBuf,4);
        ilLen = get_real_item(&pclOfblList[ilJoinedRecs][0],pclDataBuf,5);
        ilLen = get_real_item(&pclAdidList[ilJoinedRecs][0],pclDataBuf,6);
        strcat(&pclAdidList[ilJoinedRecs][0],"R");
        ilLen = get_real_item(&pclPabsList[ilJoinedRecs][0],pclDataBuf,7);
        ilLen = get_real_item(&pclPaesList[ilJoinedRecs][0],pclDataBuf,8);
        ilLen = get_real_item(&pclPdbsList[ilJoinedRecs][0],pclDataBuf,9);
        ilLen = get_real_item(&pclPdesList[ilJoinedRecs][0],pclDataBuf,10);
        ilLen = get_real_item(&pclFtypList[ilJoinedRecs][0],pclDataBuf,11);
        ilJoinedRecs++;
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
     close_my_cursor(&slCursor);
  }
  /* Insert Departure Record here, if it is not OffBlock */
  if (ilDepRecExists == TRUE && strlen(&pclOfblDep[0]) == 0)
  {
     strcpy(&pclUrnoList[ilJoinedRecs][0],pclUrnoDep);
     strcpy(&pclTifaList[ilJoinedRecs][0],pclTifaDep);
     strcpy(&pclTifdList[ilJoinedRecs][0],pclTifdDep);
     strcpy(&pclOnblList[ilJoinedRecs][0],pclOnblDep);
     strcpy(&pclOfblList[ilJoinedRecs][0],pclOfblDep);
     strcpy(&pclAdidList[ilJoinedRecs][0],pclAdidDep);
     strcpy(&pclPabsList[ilJoinedRecs][0],pclPabsDep);
     strcpy(&pclPaesList[ilJoinedRecs][0],pclPaesDep);
     strcpy(&pclPdbsList[ilJoinedRecs][0],pclPdbsDep);
     strcpy(&pclPdesList[ilJoinedRecs][0],pclPdesDep);
     strcpy(&pclFtypList[ilJoinedRecs][0],pclFtypDep);
     ilJoinedRecs++;
  }

  for (ilI = 0; ilI < ilJoinedRecs; ilI++)
  {
     if (strlen(&pclPabsList[ilI][0]) == 0)
     {
        strcpy(&pclPabsList[ilI][0]," ");
     }
     if (strlen(&pclPaesList[ilI][0]) == 0)
     {
        strcpy(&pclPaesList[ilI][0]," ");
     }
     if (strlen(&pclPdbsList[ilI][0]) == 0)
     {
        strcpy(&pclPdbsList[ilI][0]," ");
     }
     if (strlen(&pclPdesList[ilI][0]) == 0)
     {
        strcpy(&pclPdesList[ilI][0]," ");
     }
  }

  *pclLastDay = '\0';
  if (ilJoinedRecs == 1)
  {
     pclAdidList[0][1] = '\0';
     strcpy(pclLastDay,pcgPaesArr);
  }
  for (ilI = 0; ilI < ilJoinedRecs; ilI++)
  {
     if (ilI == 0)
     {
        ilRC = CalculateGroundTimes(&pclTifaList[ilI][0],NULL,
                                    &pclTifdList[ilI][0],&pclTifdList[ilI+1][0],
                                    &pclOnblList[ilI][0],NULL,
                                    &pclOfblList[ilI][0],&pclOfblList[ilI+1][0],
                                    &pclAdidList[ilI][0],ilI,ilJoinedRecs,
                                    pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
        strcpy(pclFirstDay,pclNewPabs);
     }
     else
     {
        if (ilI == ilJoinedRecs-1)
        {
           ilRC = CalculateGroundTimes(&pclTifaList[ilI][0],&pclTifaList[ilI-1][0],
                                       &pclTifdList[ilI][0],NULL,
                                       &pclOnblList[ilI][0],&pclOnblList[ilI-1][0],
                                       &pclOfblList[ilI][0],NULL,
                                       &pclAdidList[ilI][0],ilI,ilJoinedRecs,
                                       pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
           /* strcpy(pclLastDay,pclNewPdes); */
           ilRC = CalculateMaxCEDATime(pclLastDay,pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
        }
        else
        {
           ilRC = CalculateGroundTimes(&pclTifaList[ilI][0],&pclTifaList[ilI-1][0],
                                       &pclTifdList[ilI][0],&pclTifdList[ilI+1][0],
                                       &pclOnblList[ilI][0],&pclOnblList[ilI-1][0],
                                       &pclOfblList[ilI][0],&pclOfblList[ilI+1][0],
                                       &pclAdidList[ilI][0],ilI,ilJoinedRecs,
                                       pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
           ilRC = CalculateMaxCEDATime(pclLastDay,pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
        }
     }
     if (strcmp(pclNewPabs,&pclPabsList[ilI][0]) != 0 ||
         strcmp(pclNewPaes,&pclPaesList[ilI][0]) != 0 ||
         strcmp(pclNewPdbs,&pclPdbsList[ilI][0]) != 0 ||
         strcmp(pclNewPdes,&pclPdesList[ilI][0]) != 0)
     {
        sprintf(pclUpdBuf,"PABS='%s',PAES='%s',PDBS='%s',PDES='%s'",
                pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes);
        strcpy(pclFieldList,"PABS,PAES,PDBS,PDES,TIFA,TIFD,ADID,RKEY,FTYP,URNO");
        sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%c,%s,%s,%s",pclNewPabs,pclNewPaes,pclNewPdbs,pclNewPdes,
                &pclTifaList[ilI][0],&pclTifdList[ilI][0],
                pclAdidList[ilI][0],pcpRkey,&pclFtypList[ilI][0],&pclUrnoList[ilI][0]);
       ilRC = CheckBcMode(pclNewPabs,&pclPabsList[ilI][0],pclNewPaes,&pclPaesList[ilI][0],
                          pclNewPdbs,&pclPdbsList[ilI][0],pclNewPdes,&pclPdesList[ilI][0],
                          pclFieldList,pclDataList);
        sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
        sprintf(pclSelectBuf,"WHERE URNO = %s",&pclUrnoList[ilI][0]);
        strcat(pclSqlBuf,pclSelectBuf);
        dbg(DEBUG,"UpdateRotation: <%s>",pclSqlBuf);
        slCursor = 0;
        slFkt = START;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        if (ilRCdb == DB_SUCCESS)
        {
           commit_work();
           ilRC = TriggerBchdlAction("UFR","AFTTAB",&pclUrnoList[ilI][0],pclFieldList,
                                     pclDataList,TRUE,TRUE);
        }
        close_my_cursor(&slCursor);
     }
  }
  if (ilJoinedRecs > 1)
  {
     ilRC = UpdFOGTAB(&pclUrnoList[0][0],pclFirstDay,pclLastDay,"R",
                      &pclOnblList[0][0],&pclOfblList[ilJoinedRecs-1][0]);
  }
  else
  {
     ilRC = UpdFOGTAB(&pclUrnoList[0][0],pclFirstDay,pclLastDay,&pclAdidList[0][0],
                      &pclOnblList[0][0],"");
  }
 
  return ilRC;
} /* End of UpdateRotation */


static int UpdFOGTAB(char *pcpUrno, char *pcpOgbs, char *pcpOges, char *pcpType,
                     char *pcpOnbl, char *pcpOfbl)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  int ilRecInFOG;
  int ilLen;
  char pclFogUrno[32];
  char pclFogOgbs[32];
  char pclFogOges[32];
  char pclFogFtyp[32];
  char pclFogStat[32];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];
  char pclFieldValues[L_BUFF];
  char pclNewUrno[32];
  char pclUpdBuf[L_BUFF];
  char pclType[8];
  char pclStat[8];
  int ilInsert;
  int ilDelete;
  long llTime1;
  long llTime2;
  long llTime;
  int ilDebugLevel;

  dbg(DEBUG,"UpdFOGTAB: <%s> <%s> <%s> <%s> <%s> <%s>",
      pcpUrno,pcpOgbs,pcpOges,pcpType,pcpOnbl,pcpOfbl);
  strcpy(pclSqlBuf,"SELECT URNO,OGBS,OGES,FTYP,STAT FROM FOGTAB ");
  sprintf(pclSelectBuf,"WHERE FLNU = %s",pcpUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  ilRecInFOG = FALSE;
  if (ilRCdb == DB_SUCCESS)
  {
     ilRecInFOG = TRUE;
     BuildItemBuffer(pclDataBuf,"",5,",");
     ilLen = get_real_item(pclFogUrno,pclDataBuf,1);
     ilLen = get_real_item(pclFogOgbs,pclDataBuf,2);
     ilLen = get_real_item(pclFogOges,pclDataBuf,3);
     ilLen = get_real_item(pclFogFtyp,pclDataBuf,4);
     ilLen = get_real_item(pclFogStat,pclDataBuf,5);
  }

  ilInsert = FALSE;
  ilDelete = FALSE;

  if (strcmp(pcpType,"D") == 0)
  {
     if (ilRecInFOG == TRUE)
     {
        ilDelete = TRUE;
     }
  }
  else
  {
     if (strcmp(pcpType,"A") == 0)
     {
        strcpy(pclType,"A");
        strcpy(pclStat,"O");
        if (ilRecInFOG == FALSE)
        {
           if (strlen(pcpOnbl) > 0)
           {
              ilInsert = TRUE;
           }
        }
        else
        {
           if (strlen(pcpOnbl) == 0)
           {
              ilDelete = TRUE;
           }
        }
     }
     else
     {
        llTime1 = GetSecondsFromCEDATime(pcpOgbs);
        llTime2 = GetSecondsFromCEDATime(pcpOges);
        llTime = llTime2 - llTime1;
        if (llTime > igOnGroundTimeLimit)
        {
           strcpy(pclType,"R");
           if (strlen(pcpOnbl) == 0)
           {
              strcpy(pclStat,"S");
           }
           else
           {
              if (strlen(pcpOfbl) == 0)
              {
                 strcpy(pclStat,"O");
              }
              else
              {
                 strcpy(pclStat,"C");
              }
           }
           if (ilRecInFOG == FALSE)
           {
              ilInsert = TRUE;
           }
        }
        else
        {
           strcpy(pclType,"P");
           strcpy(pclStat,"O");
           if (strlen(pcpOnbl) == 0)
           {
              if (ilRecInFOG == TRUE)
              {
                 ilDelete = TRUE;
              }
           }
           else
           {
              if (strlen(pcpOfbl) != 0)
              {
                 if (ilRecInFOG == TRUE)
                 {
                    ilDelete = TRUE;
                 }
              }
              else
              {
                 if (ilRecInFOG == FALSE)
                 {
                    ilInsert = TRUE;
                 }
              }
           }
        }
     }
  }

  if (ilDelete == TRUE)
  {
     strcpy(pclSqlBuf,"DELETE FROM FOGTAB ");
     sprintf(pclSelectBuf,"WHERE FLNU = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"UpdFOGTAB: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);
     return ilRC;
  }

  if (ilInsert == TRUE)
  {
     ilDebugLevel = debug_level;
     debug_level = 0;
     GetNextValues(pclNewUrno,1);
     debug_level = ilDebugLevel;
     strcpy(pclFieldList,"URNO,HOPO,FLNU,OGBS,OGES,FTYP,STAT");
     sprintf(pclFieldValues,"%s,'%s',%s,'%s','%s','%s','%s'",
             pclNewUrno,pcgHomeAp,pcpUrno,pcpOgbs,pcpOges,pclType,pclStat);
     sprintf(pclSqlBuf,"INSERT INTO FOGTAB FIELDS(%s) VALUES(%s)",
             pclFieldList,pclFieldValues);
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s",
             pclNewUrno,pcgHomeAp,pcpUrno,pcpOgbs,pcpOges,pclType,pclStat);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"UpdFOGTAB: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("IRT","FOGTAB",pclNewUrno,pclFieldList,
                                  pclDataList,FALSE,TRUE);
     }
     close_my_cursor(&slCursor);
  }
  else
  {
     if (ilRecInFOG == TRUE)
     {
        if (strcmp(pclFogOgbs,pcpOgbs) != 0 || strcmp(pclFogOges,pcpOges) != 0 ||
            strcmp(pclFogFtyp,pclType) != 0 || strcmp(pclFogStat,pclStat) != 0)
        {
           sprintf(pclUpdBuf,"OGBS='%s',OGES='%s',FTYP='%s',STAT='%s'",
                   pcpOgbs,pcpOges,pclType,pclStat);
           sprintf(pclSqlBuf,"UPDATE FOGTAB SET %s ",pclUpdBuf);
           sprintf(pclSelectBuf,"WHERE URNO = %s",pclFogUrno);
           strcat(pclSqlBuf,pclSelectBuf);
           strcpy(pclFieldList,"OGBS,OGES,FTYP,STAT,URNO");
           sprintf(pclDataList,"%s,%s,%s,%s,%s",pcpOgbs,pcpOges,pclType,pclStat,pclFogUrno);
           dbg(DEBUG,"UpdFOGTAB: <%s>",pclSqlBuf);
           slCursor = 0;
           slFkt = START;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
           if (ilRCdb == DB_SUCCESS)
           {
              commit_work();
              ilRC = TriggerBchdlAction("URT","FOGTAB",pclFogUrno,pclFieldList,
                                        pclDataList,FALSE,TRUE);
           }
           close_my_cursor(&slCursor);
        }
     }
  }

  return ilRC;
} /* End of UpdFOGTAB */


static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction)
{
  int ilRC = RC_SUCCESS;

  if (ipBchdl == TRUE && strcmp(pcgTriggerBchdl,"YES") == 0 &&
      strstr(pcgTwStart,".NBC.") == NULL)
  {
     (void) tools_send_info_flag(1900,0,"foghdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                                 pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData,0);
     dbg(DEBUG,"Send Broadcast: <%s><%s><%s><%s><%s>",
         pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
  }
  if (ipAction == TRUE && strcmp(pcgTriggerAction,"YES") == 0)
  {
     (void) tools_send_info_flag(7400,0,"foghdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                                 pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData,0);
     dbg(DEBUG,"Send to ACTION: <%s><%s><%s><%s><%s>",
         pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
  }

  return ilRC;
} /* End of TriggerBchdlAction */


/*******************************************++++++++++*************************/
/*   GetSecondsFromCEDATime                                                   */
/*                                                                            */
/*   Serviceroutine, rechnet einen Datumseintrag im Cedaformat in Seconds um  */
/*   Input:  char *  Zeiger auf CEDA-Zeitpuffer                               */
/*   Output: long    Zeitwert                                                 */
/******************************************************************************/

static long GetSecondsFromCEDATime(char *pcpDateTime)
{
  long rc = 0;
  int year;
  char ch_help[5];
  struct tm *tstr1 = NULL, t1;
  struct tm *CurTime, t2;
  time_t llTime;
  time_t llUtcTime;
  time_t llLocalTime;

  CurTime = &t2;
  llTime = time(NULL);
  CurTime = (struct tm *)gmtime(&llTime);
  CurTime->tm_isdst = 0;
  llUtcTime = mktime(CurTime);
  CurTime = (struct tm *)localtime(&llTime);
  CurTime->tm_isdst = 0;
  llLocalTime = mktime(CurTime);
  igDiffUtcToLocal = llLocalTime - llUtcTime;
  /* dbg(TRACE,"UTC: <%ld> , Local: <%ld> , Diff: <%ld>",llUtcTime,llLocalTime,igDiffUtcToLocal); */

  memset(&t1,0x00,sizeof(struct tm));

  tstr1 = &t1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,pcpDateTime,4);
  year = atoi(ch_help);
  tstr1->tm_year = year - 1900;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[4],2);
  tstr1->tm_mon = atoi(ch_help) -1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[6],2);
  tstr1->tm_mday = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[8],2);
  tstr1->tm_hour = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[10],2);
  tstr1->tm_min = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[12],2);
  tstr1->tm_sec = atoi(ch_help);

  tstr1->tm_wday = 0;
  tstr1->tm_yday = 0;
  tstr1->tm_isdst = 0;
  rc = mktime(tstr1);

  rc = rc + igDiffUtcToLocal; /* add difference between local and utc */

  return(rc);
} /* end of GetSecondsFromCEDATime() */


static int InitRog()
{
  int ilRC = RC_SUCCESS;
  int ilI;
  char pclCurrentTimeEnd[32];
  int ilSavDebugLevel;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  int ilLen;
  char pclRegn[32];

  dbg(TRACE,"================ Now Start Initializing ROGTAB ================");
  if (strcmp(pcgInitRog,"YES") == 0)
  {
     ilSavDebugLevel = debug_level;
     if (strcmp(pcgDebugLevelInit,"DEBUG") == 0)
     {
        debug_level = DEBUG;
     }
     else
     {
        debug_level = TRACE;
     }
     dbg(DEBUG,"Current Time = <%s>",pcgCurrentTime);

     sprintf(pclSqlBuf,"TRUNCATE TABLE ROGTAB");
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"<%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);

     igTotCountRead = 0;
     igTotCountIns = 0;
     igRegnCountRead = 0;
     igRegnCountIns = 0;
     igDiffRegn = 0;
     strcpy(pclSqlBuf,"SELECT DISTINCT REGN FROM AFTTAB ");
     if (strlen(pcgRogBegin) == 0)
     {
        if (strlen(pcgRogEnd) == 0)
        {
           sprintf(pclSelectBuf,"");
        }
        else
        {
           sprintf(pclSelectBuf,"WHERE TIFA <= '%s'",pcgRogEnd);
        }
     }
     else
     {
        if (strlen(pcgRogEnd) == 0)
        {
           sprintf(pclSelectBuf,"WHERE TIFA >= '%s'",pcgRogBegin);
        }
        else
        {
           sprintf(pclSelectBuf,"WHERE TIFA BETWEEN '%s' AND '%s'",pcgRogBegin,pcgRogEnd);
        }
     }
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(TRACE,"<%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        BuildItemBuffer(pclDataBuf,"",1,",");
        ilLen = get_real_item(pclRegn,pclDataBuf,1);
        if (ilLen > 0)
        {
           if (strlen(pcgRogBegin) == 0)
           {
              ilRC = BuildROGTAB(pclRegn,NULL);
              strcpy(pcgRACDataList,"");
           }
           else
           {
              ilRC = BuildROGTAB(pclRegn,pcgRogBegin);
              strcpy(pcgRACDataList,"");
           }
           dbg(TRACE,"There were %d / %d records for REGN <%s> read / inserted",
               igRegnCountRead,igRegnCountIns,pclRegn);
           igRegnCountRead = 0;
           igRegnCountIns = 0;
           igDiffRegn++;
        }
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
     close_my_cursor(&slCursor);
     dbg(TRACE," ");
     dbg(TRACE,"There were %d / %d records for %d registrations read / inserted",
         igTotCountRead,igTotCountIns,igDiffRegn);

     dbg(TRACE," ");
     dbg(TRACE,"Start: %s",pcgCurrentTime);
     ilRC = TimeToStr(pclCurrentTimeEnd,time(NULL));
     dbg(TRACE,"End:   %s",pclCurrentTimeEnd);
     debug_level = ilSavDebugLevel;
  }

  dbg(TRACE,"================ Initializing ROGTAB Finished =================");

  return RC_SUCCESS;
} /* End of InitRog */


static int BuildROGTAB(char *pcpRegn, char *pcpStartTime)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelectBuf[XL_BUFF];
  char pclSqlBuf[XL_BUFF];
  char pclDataBuf[512000];
  int ilRCdbRd = DB_SUCCESS;
  short slFktRd;
  short slCursorRd;
  char pclSelectBufRd[XL_BUFF];
  char pclSqlBufRd[XL_BUFF];
  char pclDataBufRd[512000];
  int ilRCdbFirst = DB_SUCCESS;
  short slFktFirst;
  short slCursorFirst;
  char pclSelectBufFirst[XL_BUFF];
  char pclSqlBufFirst[XL_BUFF];
  char pclDataBufFirst[512000];
  int ilLen;
  char pclUrno[32];
  char pclRegn[32];
  char pclLand[32];
  char pclAirb[32];
  char pclTifa[32];
  char pclTifd[32];
  char pclFtyp[32];
  char pclRkey[32];
  char pclRtyp[32];
  char pclAdid[32];
  char pclOrg3[32];
  char pclDes3[32];
  char pclRogUrno[32];
  char pclRogAurn[32];
  char pclRogDurn[32];
  char pclRogRkey[32];
  char pclRogLand[32];
  char pclRogAirb[32];
  char pclRogFtyp[32];
  char pclRogStat[32];
  char pclRogSeqn[32];
  char pclNewRogFtyp[32];
  char pclNewRogStat[32];
  char pclTime[32];
  char pclSeqn[32];
  char pclSelList[128];
  char pclFieldList[32];
  char pclStartTime[32];
  int ilDoOwnCheck;
  int ilTakeRecord;
  int ilFirstRecord;
  int ilMissingRecordFound;
  int ilContinue;
  int ilRogIdx;
  int ilRogIdxNew;

  igCurRogDat = -1;

  if (pcpStartTime == NULL)
  {
     strcpy(pclSqlBuf,"DELETE FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE REGN = '%s'",
             pcpRegn);
  }
  else
  {
     strcpy(pclStartTime,pcpStartTime);
     strcpy(pclSqlBuf,"SELECT LAND,AIRB FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE REGN = '%s' AND (LAND >= '%s' OR AIRB >= '%s') ORDER BY SEQN",
             pcpRegn,pclStartTime,pclStartTime);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     /* dbg(TRACE,"BuildROGTAB: <%s>",pclSqlBuf); */
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        BuildItemBuffer(pclDataBuf,"",2,",");
        ilLen = get_real_item(pclRogLand,pclDataBuf,1);
        ilLen = get_real_item(pclRogAirb,pclDataBuf,2);
        if (strlen(pclRogLand) > 0)
        {
           if (strcmp(pclRogLand,pclStartTime) < 0)
           {
              strcpy(pclStartTime,pclRogLand);
           }
        }
        if (strlen(pclRogAirb) > 0)
        {
           if (strcmp(pclRogAirb,pclStartTime) < 0)
           {
              strcpy(pclStartTime,pclRogAirb);
           }
        }
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
     close_my_cursor(&slCursor);
     strcpy(pclSqlBuf,"DELETE FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE REGN = '%s' AND (LAND >= '%s' OR AIRB >= '%s')",
             pcpRegn,pclStartTime,pclStartTime);
  }
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"BuildROGTAB: <%s>",pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  commit_work();
  close_my_cursor(&slCursor);

  if (pcpStartTime != NULL && igUseArrayInsert == TRUE)
  {
     strcpy(pclRogSeqn,"0000000000");
     strcpy(pclSqlBuf,"SELECT URNO,HOPO,REGN,AURN,DURN,RKEY,LAND,AIRB,FTYP,STAT,FTPA,FTPD,SEQN,SEQS FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE REGN = '%s' ORDER BY SEQN DESC",
             pcpRegn);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     /* dbg(TRACE,"BuildROGTAB: <%s>",pclSqlBuf); */
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        BuildItemBuffer(pclDataBuf,"",14,",");
        ilLen = get_real_item(pclRogStat,pclDataBuf,10);
        ilLen = get_real_item(pclRogSeqn,pclDataBuf,13);
        if (*pclRogStat == 'B')
        {
           ilLen = get_real_item(&rgRogDat[0].pclUrno[0],pclDataBuf,1);
           ilLen = get_real_item(&rgRogDat[0].pclHopo[0],pclDataBuf,2);
           ilLen = get_real_item(&rgRogDat[0].pclRegn[0],pclDataBuf,3);
           ilLen = get_real_item(&rgRogDat[0].pclAurn[0],pclDataBuf,4);
           ilLen = get_real_item(&rgRogDat[0].pclDurn[0],pclDataBuf,5);
           ilLen = get_real_item(&rgRogDat[0].pclRkey[0],pclDataBuf,6);
           ilLen = get_real_item(&rgRogDat[0].pclLand[0],pclDataBuf,7);
           ilLen = get_real_item(&rgRogDat[0].pclAirb[0],pclDataBuf,8);
           ilLen = get_real_item(&rgRogDat[0].pclFtyp[0],pclDataBuf,9);
           ilLen = get_real_item(&rgRogDat[0].pclStat[0],pclDataBuf,10);
           ilLen = get_real_item(&rgRogDat[0].pclFtpa[0],pclDataBuf,11);
           ilLen = get_real_item(&rgRogDat[0].pclFtpd[0],pclDataBuf,12);
           strcpy(&rgRogDat[0].pclSeqn[0]," ");
           strcpy(&rgRogDat[0].pclSeqs[0]," ");
           strcpy(&rgRogDat[0].pclFlag[0]," ");
           igCurRogDat = 1;
           ilLen = get_real_item(pclStartTime,pclDataBuf,7);
           ilRCdb = DB_ERROR;
        }
        else
        {
           slFkt = NEXT;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        }
     }
     strcpy(pclSqlBuf,"DELETE FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE REGN = '%s' AND SEQN >= '%s'",
             pcpRegn,pclRogSeqn);
     close_my_cursor(&slCursor);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"BuildROGTAB: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);
  }

  strcpy(pclSqlBuf,"SELECT URNO,REGN,LAND,AIRB,TIFA,TIFD,FTYP,RKEY,RTYP,ADID,ORG3,DES3 FROM AFTTAB ");
  if (pcpStartTime == NULL)
  {
     sprintf(pclSelectBuf,"WHERE REGN = '%s' AND FTYP NOT IN (%s) ORDER BY TIFA",
             pcpRegn,pcgExclFtyp);
     ilDoOwnCheck = FALSE;
  }
  else
  {
/*
     sprintf(pclSelectBuf,"WHERE REGN = '%s' AND (TIFA >= '%s' OR TIFD >= '%s') AND FTYP NOT IN (%s) ORDER BY TIFA",
             pcpRegn,pclStartTime,pclStartTime,pcgExclFtyp);
*/
/*
     sprintf(pclSelectBuf,"WHERE REGN = '%s' AND FTYP NOT IN (%s) ORDER BY TIFA",
             pcpRegn,pcgExclFtyp);
*/
     sprintf(pclSelectBuf,"WHERE ((REGN = '%s' AND TIFA >= '%s') OR (REGN = '%s' AND TIFD >= '%s')) AND FTYP NOT IN (%s) ORDER BY TIFA",
             pcpRegn,pclStartTime,pcpRegn,pclStartTime,pcgExclFtyp);
     ilDoOwnCheck = TRUE;
  }
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  dbg(TRACE,"<%s>",pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  dbg(TRACE,"End of Selection");
  ilFirstRecord = TRUE;
  while (ilRCdb == DB_SUCCESS)
  {
     ilMissingRecordFound = FALSE;
     BuildItemBuffer(pclDataBuf,"",12,",");
     ilLen = get_real_item(pclUrno,pclDataBuf,1);
     ilLen = get_real_item(pclRegn,pclDataBuf,2);
     ilLen = get_real_item(pclLand,pclDataBuf,3);
     ilLen = get_real_item(pclAirb,pclDataBuf,4);
     ilLen = get_real_item(pclTifa,pclDataBuf,5);
     ilLen = get_real_item(pclTifd,pclDataBuf,6);
     ilLen = get_real_item(pclFtyp,pclDataBuf,7);
     ilLen = get_real_item(pclRkey,pclDataBuf,8);
     ilLen = get_real_item(pclRtyp,pclDataBuf,9);
     ilLen = get_real_item(pclAdid,pclDataBuf,10);
     ilLen = get_real_item(pclOrg3,pclDataBuf,11);
     ilLen = get_real_item(pclDes3,pclDataBuf,12);
     if (ilDoOwnCheck == TRUE)
     {
        if ((strcmp(pclTifa,pclStartTime) >= 0 && strcmp(pclDes3,pcgHomeAp) == 0) ||
            (strcmp(pclTifd,pclStartTime) >= 0 && strcmp(pclOrg3,pcgHomeAp) == 0))
        {
           ilTakeRecord = TRUE;
        }
        else
        {
           ilTakeRecord = FALSE;
        }
     }
     else
     {
        ilTakeRecord = TRUE;
     }
     if (ilTakeRecord == TRUE)
     {
        if (ilFirstRecord == TRUE)
        {
           if (strcmp(pclAdid,"D") == 0 && strcmp(pclRtyp,"J") == 0)
           {
              strcpy(pclSqlBufFirst,"SELECT URNO,REGN,LAND,AIRB,TIFA,TIFD,FTYP,RKEY,RTYP,ADID,ORG3,DES3 FROM AFTTAB ");
              sprintf(pclSelectBufFirst,"WHERE REGN = '%s' AND FTYP NOT IN (%s) AND ADID = 'A' AND RKEY = %s",
                      pcpRegn,pcgExclFtyp,pclRkey);
              strcat(pclSqlBufFirst,pclSelectBufFirst);
              slCursorFirst = 0;
              slFktFirst = START;
              dbg(TRACE,"Get missing arrival record: <%s>",pclSqlBufFirst);
              ilRCdbFirst = sql_if(slFktFirst,&slCursorFirst,pclSqlBufFirst,pclDataBufFirst);
              if (ilRCdbFirst == DB_SUCCESS)
              {
                 dbg(TRACE,"Missing arrival record found");
                 ilMissingRecordFound = TRUE;
                 BuildItemBuffer(pclDataBufFirst,"",12,",");
                 ilLen = get_real_item(pclUrno,pclDataBufFirst,1);
                 ilLen = get_real_item(pclRegn,pclDataBufFirst,2);
                 ilLen = get_real_item(pclLand,pclDataBufFirst,3);
                 ilLen = get_real_item(pclAirb,pclDataBufFirst,4);
                 ilLen = get_real_item(pclTifa,pclDataBufFirst,5);
                 ilLen = get_real_item(pclTifd,pclDataBufFirst,6);
                 ilLen = get_real_item(pclFtyp,pclDataBufFirst,7);
                 ilLen = get_real_item(pclRkey,pclDataBufFirst,8);
                 ilLen = get_real_item(pclRtyp,pclDataBufFirst,9);
                 ilLen = get_real_item(pclAdid,pclDataBufFirst,10);
                 ilLen = get_real_item(pclOrg3,pclDataBufFirst,11);
                 ilLen = get_real_item(pclDes3,pclDataBufFirst,12);
              }
              else
              {
                 dbg(TRACE,"Missing arrival record not found");
              }
              close_my_cursor(&slCursorFirst);
           }
           ilFirstRecord = FALSE;
        }
        igTotCountRead++;
        igRegnCountRead++;
        if (strcmp(pclAdid,"A") == 0)
        { /* arrival record */
           if (igUseArrayInsert == TRUE)
           {
              ilRCdbRd = DB_ERROR;
              ilRogIdx = -1;
              if (igAutoJoin == TRUE)
                 ilRC = SearchByAIRB(pclTifa,&ilRogIdx);
              else
                 ilRC = SearchByRKEYDep(pclRkey,&ilRogIdx);
              if (ilRogIdx >= 0)
                 ilRCdbRd = DB_SUCCESS;
           }
           else
           {
              strcpy(pclSqlBufRd,"SELECT URNO,AURN,DURN,RKEY,LAND,AIRB,FTYP,STAT,SEQN FROM ROGTAB ");
              if (igAutoJoin == TRUE)
              {
                 sprintf(pclSelectBufRd,
                         "WHERE REGN = '%s' AND LAND = ' ' AND STAT = 'D' AND AIRB >= '%s' ORDER BY AIRB",
                         pclRegn,pclTifa);
              }
              else
              {
                 sprintf(pclSelectBufRd,
                         "WHERE REGN = '%s' AND RKEY = %s AND LAND = ' ' AND STAT = 'D'",
                         pclRegn,pclRkey);
              }
              strcat(pclSqlBufRd,pclSelectBufRd);
              slCursorRd = 0;
              slFktRd = START;
              /* dbg(TRACE,"<%s>",pclSqlBufRd); */
              ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
              close_my_cursor(&slCursorRd);
           }
           if (ilRCdbRd == DB_SUCCESS && strcmp(pclFtyp,"X") != 0 &&
               strcmp(pclFtyp,"N") != 0 && strcmp(pclFtyp,"D") != 0)
           {
              if (igUseArrayInsert == TRUE)
              {
                 strcpy(pclRogUrno,&rgRogDat[ilRogIdx].pclUrno[0]);
                 strcpy(pclRogAurn,&rgRogDat[ilRogIdx].pclAurn[0]);
                 strcpy(pclRogDurn,&rgRogDat[ilRogIdx].pclDurn[0]);
                 strcpy(pclRogRkey,&rgRogDat[ilRogIdx].pclRkey[0]);
                 strcpy(pclRogLand,&rgRogDat[ilRogIdx].pclLand[0]);
                 strcpy(pclRogAirb,&rgRogDat[ilRogIdx].pclAirb[0]);
                 strcpy(pclRogFtyp,&rgRogDat[ilRogIdx].pclFtyp[0]);
                 strcpy(pclRogStat,&rgRogDat[ilRogIdx].pclStat[0]);
                 strcpy(pclRogSeqn,&rgRogDat[ilRogIdx].pclSeqn[0]);
              }
              else
              {
                 BuildItemBuffer(pclDataBufRd,"",9,",");
                 ilLen = get_real_item(pclRogUrno,pclDataBufRd,1);
                 ilLen = get_real_item(pclRogAurn,pclDataBufRd,2);
                 ilLen = get_real_item(pclRogDurn,pclDataBufRd,3);
                 ilLen = get_real_item(pclRogRkey,pclDataBufRd,4);
                 ilLen = get_real_item(pclRogLand,pclDataBufRd,5);
                 ilLen = get_real_item(pclRogAirb,pclDataBufRd,6);
                 ilLen = get_real_item(pclRogFtyp,pclDataBufRd,7);
                 ilLen = get_real_item(pclRogStat,pclDataBufRd,8);
                 ilLen = get_real_item(pclRogSeqn,pclDataBufRd,9);
              }
              if (strcmp(pclRogFtyp,"S") == 0)
              {
                 if (strlen(pclLand) > 0)
                 {
                    strcpy(pclNewRogFtyp,"O");
                    strcpy(pclTime,pclLand);
                 }
                 else
                 {
                    strcpy(pclNewRogFtyp,"S");
                    strcpy(pclTime,pclTifa);
                 }
              }
              else
              {
                 if (strlen(pclLand) > 0)
                 {
                    strcpy(pclNewRogFtyp,"H");
                    strcpy(pclTime,pclLand);
                 }
                 else
                 {
                    strcpy(pclNewRogFtyp,"E");
                    strcpy(pclTime,pclTifa);
                 }
              }
              if (strcmp(pclRogAirb,pclTime) >= 0)
              {
                 strcpy(pclNewRogStat,"B");
                 ilRC = UpdROGTAB("A",pclRogUrno,pclUrno,pclTime,pclNewRogFtyp,
                                  pclNewRogStat,pclFtyp);
              }
              else
              {
                 if (igAutoJoin == TRUE)
                 {
                    if (strlen(pclLand) == 0)
                    {
                       strcpy(pclTime,pclTifa);
                       ilRC = InsROGTAB(pclRegn,pclUrno,"0",pclRkey,pclTifa,
                                        " ","S","A",pclFtyp," ");
                    }
                    else
                    {
                       strcpy(pclTime,pclLand);
                       ilRC = InsROGTAB(pclRegn,pclUrno,"0",pclRkey,pclLand,
                                        " ","O","A",pclFtyp," ");
                    }
                    igTotCountIns++;
                    igRegnCountIns++;
                 }
                 else
                 {
                    strcpy(pclNewRogStat,"E");
                    ilRC = UpdROGTAB("A",pclRogUrno,pclUrno,pclTime,pclNewRogFtyp,
                                     pclNewRogStat,pclFtyp);
                 }
              }
           }
           else
           {
              if (strlen(pclLand) == 0)
              {
                 strcpy(pclTime,pclTifa);
                 ilRC = InsROGTAB(pclRegn,pclUrno,"0",pclRkey,pclTifa,
                                  " ","S","A",pclFtyp," ");
              }
              else
              {
                 strcpy(pclTime,pclLand);
                 ilRC = InsROGTAB(pclRegn,pclUrno,"0",pclRkey,pclLand,
                                  " ","O","A",pclFtyp," ");
              }
              igTotCountIns++;
              igRegnCountIns++;
           }
        }
        else
        {
           if (strcmp(pclAdid,"D") == 0)
           { /* departure record */
              strcpy(pclSqlBufRd,"SELECT URNO,AURN,DURN,RKEY,LAND,AIRB,FTYP,STAT FROM ROGTAB ");
              if (igAutoJoin == TRUE)
              {
                 if (igUseArrayInsert == TRUE)
                 {
                    ilRCdbRd = DB_ERROR;
                    ilRogIdx = -1;
                    ilRC = SearchByLAND(pclTifd,&ilRogIdx);
                    if (ilRogIdx >= 0)
                       ilRCdbRd = DB_SUCCESS;
                 }
                 else
                 {
                    sprintf(pclSelectBufRd,
                            "WHERE REGN = '%s' AND LAND <= '%s' AND FTPA <> 'X' AND FTPA <> 'N' AND FTPA <> 'D' ORDER BY LAND DESC",
                            pclRegn,pclTifd);
                    strcat(pclSqlBufRd,pclSelectBufRd);
                    slCursorRd = 0;
                    slFktRd = START;
                    /* dbg(TRACE,"<%s>",pclSqlBufRd); */
                    ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
                 }
                 ilContinue = TRUE;
                 while (ilRCdbRd == DB_SUCCESS && ilContinue == TRUE)
                 {
                    if (igUseArrayInsert == TRUE)
                    {
                       strcpy(pclRogUrno,&rgRogDat[ilRogIdx].pclUrno[0]);
                       strcpy(pclRogAurn,&rgRogDat[ilRogIdx].pclAurn[0]);
                       strcpy(pclRogDurn,&rgRogDat[ilRogIdx].pclDurn[0]);
                       strcpy(pclRogRkey,&rgRogDat[ilRogIdx].pclRkey[0]);
                       strcpy(pclRogLand,&rgRogDat[ilRogIdx].pclLand[0]);
                       strcpy(pclRogAirb,&rgRogDat[ilRogIdx].pclAirb[0]);
                       strcpy(pclRogFtyp,&rgRogDat[ilRogIdx].pclFtyp[0]);
                       strcpy(pclRogStat,&rgRogDat[ilRogIdx].pclStat[0]);
                    }
                    else
                    {
                       BuildItemBuffer(pclDataBufRd,"",8,",");
                       ilLen = get_real_item(pclRogUrno,pclDataBufRd,1);
                       ilLen = get_real_item(pclRogAurn,pclDataBufRd,2);
                       ilLen = get_real_item(pclRogDurn,pclDataBufRd,3);
                       ilLen = get_real_item(pclRogRkey,pclDataBufRd,4);
                       ilLen = get_real_item(pclRogLand,pclDataBufRd,5);
                       ilLen = get_real_item(pclRogAirb,pclDataBufRd,6);
                       ilLen = get_real_item(pclRogFtyp,pclDataBufRd,7);
                       ilLen = get_real_item(pclRogStat,pclDataBufRd,8);
                    }
                    if (strlen(pclRogAirb) > 1)
                    {
                       if (strcmp(pclRogAirb,pclTifd) < 0)
                       {
                          ilContinue = FALSE;
                          ilRCdbRd = DB_ERROR;
                       }
                    }
                    else
                    {
                       ilContinue = FALSE;
                    }
                    if (ilContinue == TRUE)
                    {
                       if (igUseArrayInsert == TRUE)
                       {
                          ilRCdbRd = DB_ERROR;
                          ilRogIdxNew = ilRogIdx;
                          ilRC = SearchByLAND(pclTifd,&ilRogIdxNew);
                          if (ilRogIdxNew != ilRogIdx)
                          {
                             ilRCdbRd = DB_SUCCESS;
                             ilRogIdx = ilRogIdxNew;
                          }
                       }
                       else
                       {
                          slFktRd = NEXT;
                          ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
                       }
                    }
                 }
              }
              else
              {
                 if (igUseArrayInsert == TRUE)
                 {
                    ilRCdbRd = DB_ERROR;
                    ilRogIdx = -1;
                    ilRC = SearchByRKEYArr(pclRkey,&ilRogIdx);
                    if (ilRogIdx >= 0)
                       ilRCdbRd = DB_SUCCESS;
                 }
                 else
                 {
                    sprintf(pclSelectBufRd,
                            "WHERE REGN = '%s' AND RKEY = %s AND AIRB = ' ' AND STAT = 'A'",
                            pclRegn,pclRkey);
                    strcat(pclSqlBufRd,pclSelectBufRd);
                    slCursorRd = 0;
                    slFktRd = START;
                    /* dbg(TRACE,"<%s>",pclSqlBufRd); */
                    ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
                 }
              }
              if (igUseArrayInsert == FALSE)
                 close_my_cursor(&slCursorRd);
              if (ilRCdbRd == DB_SUCCESS && strcmp(pclFtyp,"X") != 0 &&
                  strcmp(pclFtyp,"N") != 0 && strcmp(pclFtyp,"D") != 0)
              {
                 if (igUseArrayInsert == TRUE)
                 {
                    strcpy(pclRogUrno,&rgRogDat[ilRogIdx].pclUrno[0]);
                    strcpy(pclRogAurn,&rgRogDat[ilRogIdx].pclAurn[0]);
                    strcpy(pclRogDurn,&rgRogDat[ilRogIdx].pclDurn[0]);
                    strcpy(pclRogRkey,&rgRogDat[ilRogIdx].pclRkey[0]);
                    strcpy(pclRogLand,&rgRogDat[ilRogIdx].pclLand[0]);
                    strcpy(pclRogAirb,&rgRogDat[ilRogIdx].pclAirb[0]);
                    strcpy(pclRogFtyp,&rgRogDat[ilRogIdx].pclFtyp[0]);
                    strcpy(pclRogStat,&rgRogDat[ilRogIdx].pclStat[0]);
                 }
                 else
                 {
                    BuildItemBuffer(pclDataBufRd,"",8,",");
                    ilLen = get_real_item(pclRogUrno,pclDataBufRd,1);
                    ilLen = get_real_item(pclRogAurn,pclDataBufRd,2);
                    ilLen = get_real_item(pclRogDurn,pclDataBufRd,3);
                    ilLen = get_real_item(pclRogRkey,pclDataBufRd,4);
                    ilLen = get_real_item(pclRogLand,pclDataBufRd,5);
                    ilLen = get_real_item(pclRogAirb,pclDataBufRd,6);
                    ilLen = get_real_item(pclRogFtyp,pclDataBufRd,7);
                    ilLen = get_real_item(pclRogStat,pclDataBufRd,8);
                 }
                 if (strcmp(pclRogFtyp,"S") == 0)
                 {
                    if (strlen(pclAirb) > 0)
                    {
                       strcpy(pclNewRogFtyp,"E");
                       strcpy(pclTime,pclAirb);
                    }
                    else
                    {
                       strcpy(pclNewRogFtyp,"S");
                       strcpy(pclTime,pclTifd);
                    }
                 }
                 else
                 {
                    if (strlen(pclAirb) > 0)
                    {
                       strcpy(pclNewRogFtyp,"H");
                       strcpy(pclTime,pclAirb);
                    }
                    else
                    {
                       strcpy(pclNewRogFtyp,"O");
                       strcpy(pclTime,pclTifd);
                    }
                 }
                 if (strcmp(pclTime,pclRogLand) >= 0)
                 {
                    strcpy(pclNewRogStat,"B");
                    ilRC = UpdROGTAB("D",pclRogUrno,pclUrno,pclTime,pclNewRogFtyp,
                                     pclNewRogStat,pclFtyp);
                 }
                 else
                 {
                    if (igAutoJoin == TRUE)
                    {
                       if (strlen(pclAirb) == 0)
                       {
                          strcpy(pclTime,pclTifd);
                          ilRC = InsROGTAB(pclRegn,"0",pclUrno,pclRkey," ",
                                           pclTifd,"S","D"," ",pclFtyp);
                       }
                       else
                       {
                          strcpy(pclTime,pclAirb);
                          ilRC = InsROGTAB(pclRegn,"0",pclUrno,pclRkey," ",
                                        pclAirb,"H","D"," ",pclFtyp);
                       }
                       igTotCountIns++;
                       igRegnCountIns++;
                    }
                    else
                    {
                       strcpy(pclNewRogStat,"E");
                       ilRC = UpdROGTAB("D",pclRogUrno,pclUrno,pclTime,pclNewRogFtyp,
                                        pclNewRogStat,pclFtyp);
                    }
                 }
              }
              else
              {
                 if (strlen(pclAirb) == 0)
                 {
                    strcpy(pclTime,pclTifd);
                    ilRC = InsROGTAB(pclRegn,"0",pclUrno,pclRkey," ",
                                     pclTifd,"S","D"," ",pclFtyp);
                 }
                 else
                 {
                    strcpy(pclTime,pclAirb);
                    ilRC = InsROGTAB(pclRegn,"0",pclUrno,pclRkey," ",
                                     pclAirb,"H","D"," ",pclFtyp);
                 }
                 igTotCountIns++;
                 igRegnCountIns++;
              }
           }
           else
           {
              if (strcmp(pclAdid,"B") == 0)
              { /* Return flight record , etc */
                 if (strlen(pclAirb) > 0)
                 {
                    if (strlen(pclLand) > 0)
                    {
                       strcpy(pclNewRogFtyp,"H");
                    }
                    else
                    {
                       strcpy(pclNewRogFtyp,"O");
                       strcpy(pclLand,pclTifa);
                    }
                 }
                 else
                 {
                    if (strlen(pclLand) > 0)
                    {
                       strcpy(pclNewRogFtyp,"E");
                    }
                    else
                    {
                       strcpy(pclNewRogFtyp,"S");
                       strcpy(pclLand,pclTifa);
                    }
                    strcpy(pclAirb,pclTifd);
                 }
                 if (strcmp(pclLand,pclAirb) >= 0)
                 {
                    strcpy(pclNewRogStat,"R");
                 }
                 else
                 {
                    strcpy(pclNewRogStat,"E");
                 }
                 strcpy(pclTime,pclAirb);
                 ilRC = InsROGTAB(pclRegn,pclUrno,pclUrno,pclRkey,pclLand,
                                  pclAirb,pclNewRogFtyp,pclNewRogStat,pclFtyp,pclFtyp);
                 igTotCountIns++;
                 igRegnCountIns++;
              }
              else
              {
                 dbg(TRACE,"Record <%s><%s><%s><%s><%s><%s><%s><%s> was not inserted / updated",
                     pclUrno,pclRegn,pclLand,pclAirb,pclFtyp,pclRkey,pclRtyp,pclAdid);
              }
           }
        }
     }
     if (ilMissingRecordFound == TRUE)
     {
        slFkt = START;
     }
     else
     {
        slFkt = NEXT;
     }
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  if (pclStartTime == NULL)
  {
     ilRC = SortCheckRegn(pcpRegn,"",pclSeqn);
  }
  else
  {
     ilRC = SortCheckRegn(pcpRegn,pclStartTime,pclSeqn);
  }

  if (igUseArrayInsert == TRUE && igCurRogDat >= 0)
  {
     ilRC = SqlArrayFill();
     ilRC = SqlArrayInsert();
  }

  if (igAutoJoin == TRUE)
  {
     ilRC = CheckROGvsAFT(pcpRegn,pclSeqn);
  }

  if (igSendRACBroadcast == TRUE)
  {
     strcpy(pclSelList,pcpRegn);
     strcat(pclSelList,",");
     strcat(pclSelList,pcgRACBegin);
     strcat(pclSelList,",");
     strcat(pclSelList,pcgRACEnd);
     strcpy(pclFieldList,"RKEY");
     pcgRACDataList[strlen(pcgRACDataList)-1] = '\0';
     if (strlen(pcgRACDataList) > 0)
     {
        ilRC = CompressDataList(pcgRACDataList);
        ilRC = TriggerBchdlAction(pcgRACCommand,"AFTTAB",pclSelList,pclFieldList,
                                  pcgRACDataList,TRUE,FALSE);
        if (igCedaEventInterface > 0)
        {
           (void) tools_send_info_flag(igCedaEventInterface,igRemoteTarget,"roghdl",
                                       "","CEDA","","",pcgTwStart,pcgTwEnd,
                                       pcgRACCommand,"AFTTAB",pclSelList,pclFieldList,
                                       pcgRACDataList,0);
           dbg(DEBUG,"Send to CEDA Event Interface: <%s><%s><%s><%s><%s>",
               pcgRACCommand,"AFTTAB",pclSelList,pclFieldList,pcgRACDataList);
        }
        else
        {
           if (igRemoteTarget > 0)
           {
              (void) tools_send_info_flag(igRemoteTarget,0,"roghdl",
                                          "","CEDA","","",pcgTwStart,pcgTwEnd,
                                          pcgRACCommand,"AFTTAB",pclSelList,pclFieldList,
                                          pcgRACDataList,0);
              dbg(DEBUG,"Send to (%d): <%s><%s><%s><%s><%s>",
                  igRemoteTarget,pcgRACCommand,"AFTTAB",
                  pclSelList,pclFieldList,pcgRACDataList);
           }
        }
     }
  }

  return RC_SUCCESS;
} /* End of BuildROGTAB */



static int InsROGTAB(char *pcpRegn, char *pcpAurn, char *pcpDurn, char *pcpRkey,
                     char *pcpLand, char *pcpAirb, char *pcpFtyp, char *pcpStat,
                     char *pcpFtpa, char *pcpFtpd)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBufIns[L_BUFF];
  char pclSqlBufFields[L_BUFF];
  char pclSqlBufValues[L_BUFF];
  char pclDataBuf[XXL_BUFF];
  char pclNewUrno[32];
  int ilDebugLevel;
  char pclDataList[L_BUFF];
  char pclSqlBuf[L_BUFF];
  int ilRogIdx;

  if (strcmp(pcpAurn,"0") == 0)
  {
     if (igUseArrayInsert == TRUE)
     {
        ilRCdb = DB_ERROR;
        ilRogIdx = -1;
        ilRC = SearchByKey("DURN",pcpDurn,S_FOR,&ilRogIdx);
        if (ilRogIdx >= 0)
           ilRCdb = DB_SUCCESS;
     }
     else
     {
        sprintf(pclSqlBuf,"SELECT URNO FROM ROGTAB WHERE REGN = '%s' AND DURN = %s",
                pcpRegn,pcpDurn);
        slCursor = 0;
        slFkt = START;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        close_my_cursor(&slCursor);
     }
     if (ilRCdb == DB_SUCCESS)
     {
        dbg(TRACE,"InsROGTAB: Dep Record exists already");
        return ilRC;
     }
  }
  else
  {
     if (strcmp(pcpDurn,"0") == 0)
     {
        if (igUseArrayInsert == TRUE)
        {
           ilRCdb = DB_ERROR;
           ilRogIdx = -1;
           ilRC = SearchByKey("AURN",pcpAurn,S_FOR,&ilRogIdx);
           if (ilRogIdx >= 0)
              ilRCdb = DB_SUCCESS;
        }
        else
        {
           sprintf(pclSqlBuf,"SELECT URNO FROM ROGTAB WHERE REGN = '%s' AND AURN = %s",
                   pcpRegn,pcpAurn);
           slCursor = 0;
           slFkt = START;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
           close_my_cursor(&slCursor);
        }
        if (ilRCdb == DB_SUCCESS)
        {
           dbg(TRACE,"InsROGTAB: Arr Record exists already");
           return ilRC;
        }
     }
  }

  if (igUseArrayInsert == TRUE)
  {
     if (igLastUrnoIdx == 1000 || igLastUrnoIdx < 0)
     {
        ilDebugLevel = debug_level;
        debug_level = 0;
        GetNextValues(pcgNewUrnos,1010);
        debug_level = ilDebugLevel;
        igLastUrnoIdx = 0;
        igLastUrno = atoi(pcgNewUrnos);
     }
     else
     {
        igLastUrnoIdx++;
        igLastUrno++;
     }
     if (igCurRogDat < 0)
        igCurRogDat = 0;
     sprintf(&rgRogDat[igCurRogDat].pclUrno[0],"%d",igLastUrno);
     strcpy(&rgRogDat[igCurRogDat].pclHopo[0],pcgHomeAp);
     strcpy(&rgRogDat[igCurRogDat].pclRegn[0],pcpRegn);
     if (*pcpAurn != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclAurn[0],pcpAurn);
     else
        strcpy(&rgRogDat[igCurRogDat].pclAurn[0]," ");
     if (*pcpDurn != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclDurn[0],pcpDurn);
     else
        strcpy(&rgRogDat[igCurRogDat].pclDurn[0]," ");
     if (*pcpRkey != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclRkey[0],pcpRkey);
     else
        strcpy(&rgRogDat[igCurRogDat].pclRkey[0]," ");
     if (*pcpLand != '\0')
     {
        strcpy(&rgRogDat[igCurRogDat].pclLand[0],pcpLand);
        if (strlen(pcpLand) == 12)
           strcat(&rgRogDat[igCurRogDat].pclLand[0],"00");
     }
     else
        strcpy(&rgRogDat[igCurRogDat].pclLand[0]," ");
     if (*pcpAirb != '\0')
     {
        strcpy(&rgRogDat[igCurRogDat].pclAirb[0],pcpAirb);
        if (strlen(pcpAirb) == 12)
           strcat(&rgRogDat[igCurRogDat].pclAirb[0],"00");
     }
     else
        strcpy(&rgRogDat[igCurRogDat].pclAirb[0]," ");
     if (*pcpFtyp != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclFtyp[0],pcpFtyp);
     else
        strcpy(&rgRogDat[igCurRogDat].pclFtyp[0]," ");
     if (*pcpStat != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclStat[0],pcpStat);
     else
        strcpy(&rgRogDat[igCurRogDat].pclStat[0]," ");
     if (*pcpFtpa != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclFtpa[0],pcpFtpa);
     else
        strcpy(&rgRogDat[igCurRogDat].pclFtpa[0]," ");
     if (*pcpFtpd != '\0')
        strcpy(&rgRogDat[igCurRogDat].pclFtpd[0],pcpFtpd);
     else
        strcpy(&rgRogDat[igCurRogDat].pclFtpd[0]," ");
     strcpy(&rgRogDat[igCurRogDat].pclSeqn[0]," ");
     strcpy(&rgRogDat[igCurRogDat].pclSeqs[0]," ");
     strcpy(&rgRogDat[igCurRogDat].pclFlag[0]," ");
     igCurRogDat++;
  }
  else
  {
     ilDebugLevel = debug_level;
     debug_level = 0;
     GetNextValues(pclNewUrno,1);
     debug_level = ilDebugLevel;
     strcpy(pclSqlBufFields,"URNO,HOPO,REGN,AURN,DURN,RKEY,LAND,AIRB,FTYP,STAT,FTPA,FTPD");
     sprintf(pclSqlBufValues,"%s,'%s','%s',%s,%s,%s,'%s','%s','%s','%s','%s','%s'",
             pclNewUrno,pcgHomeAp,pcpRegn,pcpAurn,pcpDurn,pcpRkey,pcpLand,pcpAirb,
             pcpFtyp,pcpStat,pcpFtpa,pcpFtpd);
     sprintf(pclSqlBufIns,"INSERT INTO ROGTAB FIELDS(%s) VALUES(%s)",
             pclSqlBufFields,pclSqlBufValues);
     sprintf(pclDataList,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
             pclNewUrno,pcgHomeAp,pcpRegn,pcpAurn,pcpDurn,pcpRkey,pcpLand,pcpAirb,
             pcpFtyp,pcpStat,pcpFtpa,pcpFtpd);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"InsROGTAB: <%s>",pclSqlBufIns);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBufIns,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("IRT","ROGTAB",pclNewUrno,pclSqlBufFields,
                                  pclDataList,FALSE,FALSE);
     }
     close_my_cursor(&slCursor);
  }
  
  return ilRC;
} /* End of InsROGTAB */



static int UpdROGTAB(char *pcpAdid, char *pcpUrno,  char *pcpRurn, char *pcpTime,
                     char *pcpFtyp, char *pcpStat, char *pcpFtpad)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclUpdBuf[L_BUFF];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];
  int ilRogIdx;
  char pclTime[16];

  strcpy(pclTime,pcpTime);
  if (strlen(pclTime) == 12)
     strcat(pclTime,"00");
  if (igUseArrayInsert == TRUE)
  {
     ilRogIdx = -1;
     ilRC = SearchByKey("URNO",pcpUrno,S_BACK,&ilRogIdx);
     if (ilRogIdx >= 0)
     {
        if (strcmp(pcpAdid,"A") == 0)
        {
           if (*pcpRurn != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclAurn[0],pcpRurn);
           else
              strcpy(&rgRogDat[ilRogIdx].pclAurn[0]," ");
           if (*pcpTime != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclLand[0],pclTime);
           else
              strcpy(&rgRogDat[ilRogIdx].pclLand[0]," ");
           if (*pcpFtyp != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclFtyp[0],pcpFtyp);
           else
              strcpy(&rgRogDat[ilRogIdx].pclFtyp[0]," ");
           if (*pcpStat != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclStat[0],pcpStat);
           else
              strcpy(&rgRogDat[ilRogIdx].pclStat[0]," ");
           if (*pcpFtpad != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclFtpa[0],pcpFtpad);
           else
              strcpy(&rgRogDat[ilRogIdx].pclFtpa[0]," ");
        }
        else
        {
           if (*pcpRurn != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclDurn[0],pcpRurn);
           else
              strcpy(&rgRogDat[ilRogIdx].pclDurn[0]," ");
           if (*pcpTime != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclAirb[0],pclTime);
           else
              strcpy(&rgRogDat[ilRogIdx].pclAirb[0]," ");
           if (*pcpFtyp != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclFtyp[0],pcpFtyp);
           else
              strcpy(&rgRogDat[ilRogIdx].pclFtyp[0]," ");
           if (*pcpStat != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclStat[0],pcpStat);
           else
              strcpy(&rgRogDat[ilRogIdx].pclStat[0]," ");
           if (*pcpFtpad != '\0')
              strcpy(&rgRogDat[ilRogIdx].pclFtpd[0],pcpFtpad);
           else
              strcpy(&rgRogDat[ilRogIdx].pclFtpd[0]," ");
        }
     }
  }
  else
  {
     if (strcmp(pcpAdid,"A") == 0)
     {
        sprintf(pclUpdBuf,"AURN=%s,LAND='%s',FTYP='%s',STAT='%s',FTPA='%s'",
                pcpRurn,pcpTime,pcpFtyp,pcpStat,pcpFtpad);
        strcpy(pclFieldList,"AURN,LAND,FTYP,STAT,FTPA,URNO");
        sprintf(pclDataList,"%s,%s,%s,%s,%s,%s",
                pcpRurn,pcpTime,pcpFtyp,pcpStat,pcpFtpad,pcpUrno);
     }
     else
     {
        sprintf(pclUpdBuf,"DURN=%s,AIRB='%s',FTYP='%s',STAT='%s',FTPD='%s'",
                pcpRurn,pcpTime,pcpFtyp,pcpStat,pcpFtpad);
        strcpy(pclFieldList,"DURN,AIRB,FTYP,STAT,FTPD,URNO");
        sprintf(pclDataList,"%s,%s,%s,%s,%s,%s",
                pcpRurn,pcpTime,pcpFtyp,pcpStat,pcpFtpad,pcpUrno);
     }
     sprintf(pclSqlBuf,"UPDATE ROGTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(DEBUG,"UpdROGTAB: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("URT","ROGTAB",pcpUrno,pclFieldList,
                                  pclDataList,FALSE,FALSE);
     }
     close_my_cursor(&slCursor);
  }

  return ilRC;
} /* End of UpdROGTAB */



static int SortCheckRegn(char *pcpRegn, char *pcpTime, char *pcpSeqn)
{
  int ilRC = RC_SUCCESS;
  int ilRCdbLand = DB_SUCCESS;
  short slFktLand;
  short slCursorLand;
  char pclSqlBufLand[L_BUFF];
  char pclSelectBufLand[L_BUFF];
  char pclDataBufLand[L_BUFF];
  int ilRCdbAirb = DB_SUCCESS;
  short slFktAirb;
  short slCursorAirb;
  char pclSqlBufAirb[L_BUFF];
  char pclSelectBufAirb[L_BUFF];
  char pclDataBufAirb[L_BUFF];
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  int ilLen;
  char pclUrnoLand[32];
  char pclLandLand[32];
  char pclAirbLand[32];
  char pclFtypLand[32];
  char pclStatLand[32];
  char pclFtpaLand[32];
  char pclSeqnLand[32];
  char pclUrnoAirb[32];
  char pclLandAirb[32];
  char pclAirbAirb[32];
  char pclFtypAirb[32];
  char pclStatAirb[32];
  char pclFtpdAirb[32];
  char pclSeqnAirb[32];
  char pclDurnAirb[32];
  int ilIdx;
  int ilReadLand;
  int ilReadAirb;
  char pclTimeLand[32];
  char pclTimeAirb[32];
  char pclUrno[32];
  char pclAurn[32];
  char pclRkey[32];
  char pclLand[32];
  char pclAirb[32];
  char pclFtyp[32];
  char pclStat[32];
  char pclFtpa[32];
  char pclFtpd[32];
  char pclSeqn[32];
  char pclSeqs[32];
  char pclLastLand[32];
  char pclLastAirb[32];
  int ilLand;
  int ilAirb;
  char pclStatus[32];
  char pclIdx[32];
  char pclUpdType[32];
  int ilJoin;
  int ilJoinDone;
  char pclLastArrUrno[2000][32];
  char pclLastArrTime[2000][32];
  int ilNoOpenArr;
  char pclDelListUrno[2000][32];
  int ilDelListNo;
  int ilI;
  char pclNewFtyp[8];
  int ilJ;
  int ilRogIdx;
  int ilRogIdxLand;
  int ilRogIdxAirb;
  int ilRogIdxNew;
  int ilRogIdxLandNew;
  int ilRogIdxAirbNew;

  strcpy(pclIdx," ");
  ilIdx = 0;
  if (strlen(pcpTime) > 0)
  {
     if (igUseArrayInsert == TRUE)
     {
        strcpy(pclSqlBufLand,"SELECT MAX(SEQN) FROM ROGTAB ");
        sprintf(pclSelectBufLand,
                "WHERE REGN = '%s'",pcpRegn);
     }
     else
     {
        strcpy(pclSqlBufLand,"SELECT AIRB,SEQN FROM ROGTAB ");
        sprintf(pclSelectBufLand,
                "WHERE REGN = '%s' AND LAND <> ' ' AND LAND < '%s' AND FTYP <> 'E' AND STAT = 'B' AND SEQS = 'O' ORDER BY LAND DESC",
                pcpRegn,pcpTime);
     }
     strcat(pclSqlBufLand,pclSelectBufLand);
     slCursorLand = 0;
     slFktLand = START;
     /* dbg(TRACE,"<%s>",pclSqlBufLand); */
     ilRCdbLand = sql_if(slFktLand,&slCursorLand,pclSqlBufLand,pclDataBufLand);
     if (ilRCdbLand == DB_SUCCESS)
     {
        if (igUseArrayInsert == TRUE)
        {
           strcpy(pclIdx,pclDataBufLand);
           ilIdx = atoi(pclIdx);
        }
        else
        {
           BuildItemBuffer(pclDataBufLand,"",2,",");
           ilLen = get_real_item(pclAirb,pclDataBufLand,1);
           ilLen = get_real_item(pclIdx,pclDataBufLand,2);
           ilIdx = atoi(pclIdx) - 1;
           if (strlen(pclAirb) == 0 || strcmp(pclAirb,pcpTime) >= 0)
           {
              slFktLand = NEXT;
              ilRCdbLand = sql_if(slFktLand,&slCursorLand,pclSqlBufLand,pclDataBufLand);
              if (ilRCdbLand == DB_SUCCESS)
              {
                 BuildItemBuffer(pclDataBufLand,"",2,",");
                 ilLen = get_real_item(pclAirb,pclDataBufLand,1);
                 ilLen = get_real_item(pclIdx,pclDataBufLand,2);
                 ilIdx = atoi(pclIdx) - 1;
              }
              else
              {
                 strcpy(pclIdx," ");
                 ilIdx = 0;
              }
           }
        }
     }
     close_my_cursor(&slCursorLand);
  }

dbg(TRACE,"Sequence Start = <%s><%d>",pclIdx,ilIdx);
  if (igUseArrayInsert == TRUE)
  {
     ilRCdbLand = DB_ERROR;
     ilRogIdxLand = -1;
     ilRC = SearchByLANDSeqn(pclIdx,&ilRogIdxLand);
     if (ilRogIdxLand >= 0)
        ilRCdbLand = DB_SUCCESS;
     ilRCdbAirb = DB_ERROR;
     ilRogIdxAirb = -1;
     ilRC = SearchByAIRBSeqn(pclIdx,&ilRogIdxAirb);
     if (ilRogIdxAirb >= 0)
        ilRCdbAirb = DB_SUCCESS;
  }
  else
  {
     strcpy(pclSqlBufLand,"SELECT URNO,LAND,AIRB,FTYP,STAT,FTPA,SEQN FROM ROGTAB ");
     sprintf(pclSelectBufLand,
             "WHERE REGN = '%s' AND STAT IN ('A','B','E') AND (SEQN = ' ' OR SEQN >= '%s') ORDER BY LAND",
             pcpRegn,pclIdx);
     strcat(pclSqlBufLand,pclSelectBufLand);
     slCursorLand = 0;
     slFktLand = START;
     /* dbg(TRACE,"<%s>",pclSqlBufLand); */

     strcpy(pclSqlBufAirb,"SELECT URNO,LAND,AIRB,FTYP,STAT,FTPD,SEQN,DURN FROM ROGTAB ");
     sprintf(pclSelectBufAirb,
             "WHERE REGN = '%s' AND STAT IN ('D','R') AND (SEQN = ' ' OR SEQN >= '%s') ORDER BY AIRB",
             pcpRegn,pclIdx);
     strcat(pclSqlBufAirb,pclSelectBufAirb);
     slCursorAirb = 0;
     slFktAirb = START;
     /* dbg(TRACE,"<%s>",pclSqlBufAirb); */

     ilRCdbLand = sql_if(slFktLand,&slCursorLand,pclSqlBufLand,pclDataBufLand);
     ilRCdbAirb = sql_if(slFktAirb,&slCursorAirb,pclSqlBufAirb,pclDataBufAirb);
  }
  ilNoOpenArr = 0;
  ilDelListNo = 0;
  ilReadLand = TRUE;
  ilReadAirb = TRUE;
  while (ilRCdbLand == DB_SUCCESS || ilRCdbAirb == DB_SUCCESS)
  {
     if (ilRCdbLand == DB_SUCCESS && ilReadLand == TRUE)
     {
        if (igUseArrayInsert == TRUE)
        {
           strcpy(pclUrnoLand,&rgRogDat[ilRogIdxLand].pclUrno[0]);
           strcpy(pclLandLand,&rgRogDat[ilRogIdxLand].pclLand[0]);
           strcpy(pclAirbLand,&rgRogDat[ilRogIdxLand].pclAirb[0]);
           strcpy(pclFtypLand,&rgRogDat[ilRogIdxLand].pclFtyp[0]);
           strcpy(pclStatLand,&rgRogDat[ilRogIdxLand].pclStat[0]);
           strcpy(pclFtpaLand,&rgRogDat[ilRogIdxLand].pclFtpa[0]);
           strcpy(pclSeqnLand,&rgRogDat[ilRogIdxLand].pclSeqn[0]);
        }
        else
        {
           BuildItemBuffer(pclDataBufLand,"",7,",");
           ilLen = get_real_item(pclUrnoLand,pclDataBufLand,1);
           ilLen = get_real_item(pclLandLand,pclDataBufLand,2);
           ilLen = get_real_item(pclAirbLand,pclDataBufLand,3);
           ilLen = get_real_item(pclFtypLand,pclDataBufLand,4);
           ilLen = get_real_item(pclStatLand,pclDataBufLand,5);
           ilLen = get_real_item(pclFtpaLand,pclDataBufLand,6);
           ilLen = get_real_item(pclSeqnLand,pclDataBufLand,7);
        }
     }
     if (ilRCdbAirb == DB_SUCCESS && ilReadAirb == TRUE)
     {
        if (igUseArrayInsert == TRUE)
        {
           strcpy(pclUrnoAirb,&rgRogDat[ilRogIdxAirb].pclUrno[0]);
           strcpy(pclLandAirb,&rgRogDat[ilRogIdxAirb].pclLand[0]);
           strcpy(pclAirbAirb,&rgRogDat[ilRogIdxAirb].pclAirb[0]);
           strcpy(pclFtypAirb,&rgRogDat[ilRogIdxAirb].pclFtyp[0]);
           strcpy(pclStatAirb,&rgRogDat[ilRogIdxAirb].pclStat[0]);
           strcpy(pclFtpdAirb,&rgRogDat[ilRogIdxAirb].pclFtpd[0]);
           strcpy(pclSeqnAirb,&rgRogDat[ilRogIdxAirb].pclSeqn[0]);
           strcpy(pclDurnAirb,&rgRogDat[ilRogIdxAirb].pclDurn[0]);
        }
        else
        {
           BuildItemBuffer(pclDataBufAirb,"",8,",");
           ilLen = get_real_item(pclUrnoAirb,pclDataBufAirb,1);
           ilLen = get_real_item(pclLandAirb,pclDataBufAirb,2);
           ilLen = get_real_item(pclAirbAirb,pclDataBufAirb,3);
           ilLen = get_real_item(pclFtypAirb,pclDataBufAirb,4);
           ilLen = get_real_item(pclStatAirb,pclDataBufAirb,5);
           ilLen = get_real_item(pclFtpdAirb,pclDataBufAirb,6);
           ilLen = get_real_item(pclSeqnAirb,pclDataBufAirb,7);
           ilLen = get_real_item(pclDurnAirb,pclDataBufAirb,8);
        }
     }
     if (ilRCdbLand == DB_SUCCESS && ilRCdbAirb != DB_SUCCESS)
     {
        strcpy(pclUpdType,"A");
        ilReadLand = TRUE;
        ilReadAirb = FALSE;
     }
     if (ilRCdbAirb == DB_SUCCESS && ilRCdbLand != DB_SUCCESS)
     {
        strcpy(pclUpdType,"D");
        ilReadAirb = TRUE;
        ilReadLand = FALSE;
     }
     if (ilRCdbLand == DB_SUCCESS && ilRCdbAirb == DB_SUCCESS)
     {
        if (strcmp(pclStatLand,"B") == 0)
        {
           strcpy(pclTimeLand,pclAirbLand);
        }
        else
        {
           strcpy(pclTimeLand,pclLandLand);
        }
        if (strcmp(pclStatAirb,"R") == 0)
        {
           strcpy(pclTimeAirb,pclLandAirb);
        }
        else
        {
           strcpy(pclTimeAirb,pclAirbAirb);
        }
        if (strcmp(pclTimeLand,pclTimeAirb) <= 0)
        {
           strcpy(pclUpdType,"A");
           ilReadLand = TRUE;
           ilReadAirb = FALSE;
        }
        else
        {
           strcpy(pclUpdType,"D");
           ilReadAirb = TRUE;
           ilReadLand = FALSE;
        }
     }
     ilJoin = FALSE;
     if (ilNoOpenArr > 0 && strcmp(pclUpdType,"D") == 0)
     {
        if (strcmp(pclStatAirb,"D") == 0 &&
           strcmp(pclFtypAirb,"E") != 0 &&
           strcmp(pclFtpdAirb,"X") != 0 &&
           strcmp(pclFtpdAirb,"N") != 0 &&
           strcmp(pclFtpdAirb,"D") != 0)
        {
           ilJoin = TRUE;
        }
     }
     if (ilJoin == TRUE && igAutoJoin == TRUE)
     {
        ilJoinDone = FALSE;
        for (ilI = ilNoOpenArr-1; ilI >= 0 && ilJoinDone == FALSE; ilI--)
        {
           if (strcmp(&pclLastArrTime[ilI][0],pclAirbAirb) < 0)
           {   /* do join */
              if (igUseArrayInsert == TRUE)
              {
                 ilRCdb = DB_ERROR;
                 ilRogIdx = -1;
                 ilRC = SearchByKey("URNO",&pclLastArrUrno[ilI][0],S_BACK,&ilRogIdx);
                 if (ilRogIdx >= 0)
                    ilRCdb = DB_SUCCESS;
              }
              else
              {
                 strcpy(pclSqlBuf,"SELECT AURN,RKEY,LAND,FTYP,STAT FROM ROGTAB ");
                 sprintf(pclSelectBuf,"WHERE URNO = %s",&pclLastArrUrno[ilI][0]);
                 strcat(pclSqlBuf,pclSelectBuf);
                 slCursor = 0;
                 slFkt = START;
                 ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
                 close_my_cursor(&slCursor);
              }
              if (ilRCdb == DB_SUCCESS)
              {
                 if (igUseArrayInsert == TRUE)
                 {
                    strcpy(pclAurn,&rgRogDat[ilRogIdx].pclAurn[0]);
                    strcpy(pclRkey,&rgRogDat[ilRogIdx].pclRkey[0]);
                    strcpy(pclLand,&rgRogDat[ilRogIdx].pclLand[0]);
                    strcpy(pclFtyp,&rgRogDat[ilRogIdx].pclFtyp[0]);
                    strcpy(pclStat,&rgRogDat[ilRogIdx].pclStat[0]);
                 }
                 else
                 {
                    BuildItemBuffer(pclDataBuf,"",5,",");
                    ilLen = get_real_item(pclAurn,pclDataBuf,1);
                    ilLen = get_real_item(pclRkey,pclDataBuf,2);
                    ilLen = get_real_item(pclLand,pclDataBuf,3);
                    ilLen = get_real_item(pclFtyp,pclDataBuf,4);
                    ilLen = get_real_item(pclStat,pclDataBuf,5);
                 }
                 strcpy(pclNewFtyp,"E");
                 if (strcmp(pclFtyp,"S") == 0)
                 {
                    if (strcmp(pclFtypAirb,"H") == 0)
                    {
                       strcpy(pclNewFtyp,"E");
                    }
                    else
                    {
                       strcpy(pclNewFtyp,"S");
                    }
                 }
                 else
                 {
                    if (strcmp(pclFtyp,"O") == 0)
                    {
                       if (strcmp(pclFtypAirb,"S") == 0)
                       {
                          strcpy(pclNewFtyp,"O");
                       }
                       else
                       {
                          strcpy(pclNewFtyp,"H");
                       }
                    }
                 }
                 if (strcmp(pclNewFtyp,"E") != 0)
                 {
                    ilRC = UpdROGTAB("D",&pclLastArrUrno[ilI][0],pclDurnAirb,
                                     pclAirbAirb,pclNewFtyp,"B",pclFtpdAirb);
                    for (ilJ = ilI; ilJ < ilNoOpenArr-1; ilJ++)
                    {
                       strcpy(&pclLastArrUrno[ilJ][0],&pclLastArrUrno[ilJ+1][0]);
                       strcpy(&pclLastArrTime[ilJ][0],&pclLastArrTime[ilJ+1][0]);
                    }
                    ilNoOpenArr--;
                    ilJoinDone = TRUE;
                    strcpy(&pclDelListUrno[ilDelListNo][0],pclUrnoAirb);
                    ilDelListNo++;
                 }
              }
           }
        }
        if (ilJoinDone == FALSE)
        {
           ilIdx++;
           ilRC = UpdSEQN(pclUrnoAirb,ilIdx,pclSeqnAirb);
        }
     }
     else
     {
        ilIdx++;
        if (strcmp(pclUpdType,"A") == 0)
        {
           ilRC = UpdSEQN(pclUrnoLand,ilIdx,pclSeqnLand);
        }
        else
        {
           ilRC = UpdSEQN(pclUrnoAirb,ilIdx,pclSeqnAirb);
        }
     }
     if (strcmp(pclUpdType,"A") == 0)
     {
        if (strcmp(pclStatLand,"B") == 0)
        {
           ilNoOpenArr = 0;
        }
        else
        {
           if (strcmp(pclStatLand,"A") == 0 &&
              strcmp(pclFtypLand,"E") != 0 &&
              strcmp(pclFtpaLand,"X") != 0 &&
              strcmp(pclFtpaLand,"N") != 0 &&
              strcmp(pclFtpaLand,"D") != 0)
           {
              strcpy(&pclLastArrUrno[ilNoOpenArr][0],pclUrnoLand);
              strcpy(&pclLastArrTime[ilNoOpenArr][0],pclLandLand);
              ilNoOpenArr++;
           }
        }
     }
     if (ilReadLand == TRUE)
     {
        if (igUseArrayInsert == TRUE)
        {
           ilRCdbLand = DB_ERROR;
           ilRogIdxLandNew = ilRogIdxLand;
           ilRC = SearchByLANDSeqn(pclIdx,&ilRogIdxLandNew);
           if (ilRogIdxLandNew != ilRogIdxLand)
           {
              ilRCdbLand = DB_SUCCESS;
              ilRogIdxLand = ilRogIdxLandNew;
           }
        }
        else
        {
           slFktLand = NEXT;
           ilRCdbLand = sql_if(slFktLand,&slCursorLand,pclSqlBufLand,pclDataBufLand);
        }
     }
     if (ilReadAirb == TRUE)
     {
        if (igUseArrayInsert == TRUE)
        {
           ilRCdbAirb = DB_ERROR;
           ilRogIdxAirbNew = ilRogIdxAirb;
           ilRC = SearchByAIRBSeqn(pclIdx,&ilRogIdxAirbNew);
           if (ilRogIdxAirbNew != ilRogIdxAirb)
           {
              ilRCdbAirb = DB_SUCCESS;
              ilRogIdxAirb = ilRogIdxAirbNew;
           }
        }
        else
        {
           slFktAirb = NEXT;
           ilRCdbAirb = sql_if(slFktAirb,&slCursorAirb,pclSqlBufAirb,pclDataBufAirb);
        }
     }
  }
  if (igUseArrayInsert == FALSE)
  {
     close_my_cursor(&slCursorLand);
     close_my_cursor(&slCursorAirb);
  }

  if (ilDelListNo > 0)
  {
     for (ilI = 0; ilI < ilDelListNo; ilI++)
     {
       ilRC = DelROG(&pclDelListUrno[ilI][0]);
     }
  }

  strcpy(pcpSeqn,pclIdx);
  strcpy(pclLastLand,"19000101000000");
  strcpy(pclLastAirb,"19000101000000");
  ilLand = TRUE;
  ilAirb = TRUE;
  strcpy(pclStatus,"");
  if (igUseArrayInsert == TRUE)
  {
     ilRCdb = DB_ERROR;
     ilRogIdx = -1;
     ilRC = SearchBySEQN(pclIdx,&ilRogIdx);
     if (ilRogIdx >= 0)
        ilRCdb = DB_SUCCESS;
  }
  else
  {
     strcpy(pclSqlBuf,"SELECT URNO,LAND,AIRB,FTYP,STAT,FTPA,FTPD,SEQN,SEQS FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE REGN = '%s' AND SEQN >= '%s' ORDER BY SEQN",
             pcpRegn,pclIdx);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     /* dbg(TRACE,"<%s>",pclSqlBuf); */
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  while (ilRCdb == DB_SUCCESS)
  {
     if (igUseArrayInsert == TRUE)
     {
        strcpy(pclUrno,&rgRogDat[ilRogIdx].pclUrno[0]);
        strcpy(pclLand,&rgRogDat[ilRogIdx].pclLand[0]);
        strcpy(pclAirb,&rgRogDat[ilRogIdx].pclAirb[0]);
        strcpy(pclFtyp,&rgRogDat[ilRogIdx].pclFtyp[0]);
        strcpy(pclStat,&rgRogDat[ilRogIdx].pclStat[0]);
        strcpy(pclFtpa,&rgRogDat[ilRogIdx].pclFtpa[0]);
        strcpy(pclFtpd,&rgRogDat[ilRogIdx].pclFtpd[0]);
        strcpy(pclSeqn,&rgRogDat[ilRogIdx].pclSeqn[0]);
        strcpy(pclSeqs,&rgRogDat[ilRogIdx].pclSeqs[0]);
     }
     else
     {
        BuildItemBuffer(pclDataBuf,"",9,",");
        ilLen = get_real_item(pclUrno,pclDataBuf,1);
        ilLen = get_real_item(pclLand,pclDataBuf,2);
        ilLen = get_real_item(pclAirb,pclDataBuf,3);
        ilLen = get_real_item(pclFtyp,pclDataBuf,4);
        ilLen = get_real_item(pclStat,pclDataBuf,5);
        ilLen = get_real_item(pclFtpa,pclDataBuf,6);
        ilLen = get_real_item(pclFtpd,pclDataBuf,7);
        ilLen = get_real_item(pclSeqn,pclDataBuf,8);
        ilLen = get_real_item(pclSeqs,pclDataBuf,9);
     }
     if (strcmp(pclFtyp,"E") == 0 || strcmp(pclStat,"E") == 0)
     {
        ilRC = UpdSEQS(pclUrno,"E",pclSeqs);
     }
     else
     {
        if (strlen(pclAirb) == 0 || *pclAirb == ' ')
        {
           if (strcmp(pclFtpa,"X") == 0 || strcmp(pclFtpa,"N") == 0 ||
               strcmp(pclFtpa,"D") == 0)
           {
              ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
           }
           else
           {
              if (strcmp(pclLand,pclLastAirb) >= 0 && ilAirb == TRUE)
              {
                 if (strcmp(pclFtyp,"O") == 0)
                 {
                    if (strlen(pclStatus) == 0 || strcmp(pclStatus,"AIR") == 0)
                    {
                       ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                    }
                    else
                    {
                       ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
                    }
                 }
                 else
                 {
                    ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                 }
              }
              else
              {
                 ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
              }
              ilLand = TRUE;
              ilAirb = FALSE;
              strcpy(pclLastLand,pclLand);
              if (strcmp(pclFtyp,"O") == 0)
              {
                 strcpy(pclStatus,"LAN");
              }
              else
              {
                 strcpy(pclStatus,"SCH");
              }
           }
        }
        else
        {
           if (strlen(pclLand) == 0 || *pclLand == ' ')
           {
              if (strcmp(pclFtpd,"X") == 0 || strcmp(pclFtpd,"N") == 0 ||
                  strcmp(pclFtpd,"D") == 0)
              {
                 ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
              }
              else
              {
                 if (strcmp(pclAirb,pclLastLand) >= 0 && ilLand == TRUE)
                 {
                    if (strcmp(pclFtyp,"H") == 0)
                    {
                       if (strlen(pclStatus) == 0 || strcmp(pclStatus,"LAN") == 0)
                       {
                          ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                       }
                       else
                       {
                          ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
                       }
                    }
                    else
                    {
                       ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                    }
                 }
                 else
                 {
                    ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
                 }
                 ilLand = FALSE;
                 ilAirb = TRUE;
                 strcpy(pclLastAirb,pclAirb);
                 if (strcmp(pclFtyp,"H") == 0)
                 {
                    strcpy(pclStatus,"AIR");
                 }
                 else
                 {
                    strcpy(pclStatus,"SCH");
                 }
              }
           }
           else
           {
              if (strcmp(pclStat,"B") == 0)
              {
                 if (strcmp(pclLand,pclLastAirb) >= 0 && ilAirb == TRUE)
                 {
                    if (strcmp(pclFtyp,"H") == 0 || strcmp(pclFtyp,"O") == 0)
                    {
                       if (strlen(pclStatus) == 0 || strcmp(pclStatus,"AIR") == 0)
                       {
                          ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                       }
                       else
                       {
                          ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
                       }
                    }
                    else
                    {
                       ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                    }
                 }
                 else
                 {
                    ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
                 }
                 ilLand = FALSE;
                 ilAirb = TRUE;
                 strcpy(pclLastLand,pclLand);
                 strcpy(pclLastAirb,pclAirb);
                 if (strcmp(pclFtyp,"H") == 0)
                 {
                    strcpy(pclStatus,"AIR");
                 }
                 else
                 {
                    if (strcmp(pclFtyp,"O") == 0)
                    {
                       strcpy(pclStatus,"LAN");
                    }
                    else
                    {
                       strcpy(pclStatus,"SCH");
                    }
                 }
              }
              else
              {
                 if (strcmp(pclAirb,pclLastLand) >= 0)
                 {
                    ilRC = UpdSEQS(pclUrno,"O",pclSeqs);
                 }
                 else
                 {
                    ilRC = UpdSEQS(pclUrno,"N",pclSeqs);
                 }
              }
           }
        }
     }
     if (igUseArrayInsert == TRUE)
     {
        ilRCdb = DB_ERROR;
        ilRogIdxNew = ilRogIdx;
        ilRC = SearchBySEQN(pclIdx,&ilRogIdxNew);
        if (ilRogIdxNew != ilRogIdx)
        {
           ilRCdb = DB_SUCCESS;
           ilRogIdx = ilRogIdxNew;
        }
     }
     else
     {
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
  }
  if (igUseArrayInsert == FALSE)
     close_my_cursor(&slCursor);

  dbg(TRACE,"All records of REGN <%s> sorted",pcpRegn);

  return ilRC;
} /* End of SortCheckRegn */



static int UpdSEQN(char *pcpUrno, int ipSeqn, char *pcpSeqn)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclUpdBuf[L_BUFF];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];
  char pclSeqn[32];
  char pclTmpSeqn[32];
  int ilSeqn;
  int ilRogIdx;

  ilSeqn = atoi(pcpSeqn);
  if (ipSeqn == ilSeqn)
  {
     return ilRC;
  }

  sprintf(pclTmpSeqn,"%d",ipSeqn);
  ilRC = GetCorrectedFormatZero(pclSeqn,pclTmpSeqn,10);
  if (igUseArrayInsert == TRUE)
  {
     ilRogIdx = -1;
     ilRC = SearchByKey("URNO",pcpUrno,S_BACK,&ilRogIdx);
     if (ilRogIdx >= 0)
     {
        strcpy(&rgRogDat[ilRogIdx].pclSeqn[0],pclSeqn);
     }
  }
  else
  {
     sprintf(pclUpdBuf,"SEQN='%s'",pclSeqn);
     strcpy(pclFieldList,"SEQN,URNO");
     sprintf(pclDataList,"%s,%s",pclSeqn,pcpUrno);
     sprintf(pclSqlBuf,"UPDATE ROGTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(DEBUG,"UpdSEQN: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("URT","ROGTAB",pcpUrno,pclFieldList,
                                  pclDataList,FALSE,FALSE);
     }
     close_my_cursor(&slCursor);
  }

  return ilRC;
} /* End of UpdSEQN */



static int GetCorrectedFormatZero(char *pcpResult, char *pcpString, int ipLen)
{
  int ilRC = RC_SUCCESS;
  int ilI;

  if (strlen(pcpString) >= ipLen)
  {
     strncpy(pcpResult,pcpString,ipLen);
     pcpResult[ipLen] = '\0';
  }
  else
  {
     pcpResult[0] = '\0';
     for (ilI = 0; ilI < ipLen - strlen(pcpString); ilI++)
     {
        strcat(pcpResult,"0");
     }
     strcat(pcpResult,pcpString);
  }

  return ilRC;
} /* End of GetCorrectedFormatZero */



static int UpdSEQS(char *pcpUrno, char *pcpSeqs, char *pcpPrevSeqs)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclUpdBuf[L_BUFF];
  char pclFieldList[L_BUFF];
  char pclDataList[L_BUFF];
  int ilRogIdx;

  if (strcmp(pcpSeqs,pcpPrevSeqs) == 0)
  {
     return ilRC;
  }

  if (igUseArrayInsert == TRUE)
  {
     ilRogIdx = -1;
     ilRC = SearchByKey("URNO",pcpUrno,S_BACK,&ilRogIdx);
     if (ilRogIdx >= 0)
     {
        strcpy(&rgRogDat[ilRogIdx].pclSeqs[0],pcpSeqs);
     }
  }
  else
  {
     sprintf(pclUpdBuf,"SEQS='%s'",pcpSeqs);
     strcpy(pclFieldList,"SEQS,URNO");
     sprintf(pclDataList,"%s,%s",pcpSeqs,pcpUrno);
     sprintf(pclSqlBuf,"UPDATE ROGTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(DEBUG,"UpdSEQS: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
        ilRC = TriggerBchdlAction("URT","ROGTAB",pcpUrno,pclFieldList,
                                  pclDataList,FALSE,FALSE);
     }
     close_my_cursor(&slCursor);
  }

  return ilRC;
} /* End of UpdSEQS */



static int UpdateROGTAB(char *pcpUrno)
{
  int ilRC = RC_SUCCESS;
  int ilRCdbAft = DB_SUCCESS;
  int ilRCdbRog = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBufAft[XXL_BUFF];
  char pclDataBufRog[XXL_BUFF];
  int ilLen;
  char pclRogRegn[32] = "";
  char pclRogAurn[32] = "";
  char pclRogDurn[32] = "";
  char pclRogLand[32] = "";
  char pclRogAirb[32] = "";
  char pclRogRkey[32] = "";
  char pclAftRegn[32] = "";
  char pclAftTifa[32] = "";
  char pclAftTifd[32] = "";
  char pclAftAdid[32] = "";
  char pclAftFtyp[32] = "";
  char pclAftTime[32] = "";
  char pclRogTime[32] = "";
  char pclTime[32];
  char pclExclFtyp[512] = "";
  char pclTmpFtyp[32] = "";
  int ilCount;
  int ilI;

  strcpy(pclAftRegn,"");
  strcpy(pclSqlBuf,"SELECT REGN,TIFA,TIFD,ADID,FTYP FROM AFTTAB ");
  sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdbAft = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBufAft);
  close_my_cursor(&slCursor);
  if (ilRCdbAft == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBufAft,"",5,",");
     ilLen = get_real_item(pclAftRegn,pclDataBufAft,1);
     ilLen = get_real_item(pclAftTifa,pclDataBufAft,2);
     ilLen = get_real_item(pclAftTifd,pclDataBufAft,3);
     ilLen = get_real_item(pclAftAdid,pclDataBufAft,4);
     ilLen = get_real_item(pclAftFtyp,pclDataBufAft,5);
     strcpy(pclExclFtyp,pcgExclFtyp);
     ilCount = GetNoOfElements(pclExclFtyp,',');
     for (ilI = 1; ilI <= ilCount; ilI++)
     {
        ilLen = get_real_item(pclTmpFtyp,pclExclFtyp,ilI);
        if (ilLen > 0)
        {
           if (pclTmpFtyp[1] == pclAftFtyp[0])
           {
              dbg(DEBUG,"UpdateROGTAB: Ftyp <%s> is excluded",pclAftFtyp);
              return ilRC;
           }
        }
     }
     if (strcmp(pclAftAdid,"D") == 0)
     {
        strcpy(pclAftTime,pclAftTifd);
     }
     else
     {
        strcpy(pclAftTime,pclAftTifa);
     }
  }

  strcpy(pclSqlBuf,"SELECT REGN,AURN,DURN,LAND,AIRB,RKEY FROM ROGTAB ");
  sprintf(pclSelectBuf,"WHERE AURN = %s OR DURN = %s",pcpUrno,pcpUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  /* dbg(TRACE,"<%s>",pclSqlBuf); */
  ilRCdbRog = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBufRog);
  while (ilRCdbRog == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBufRog,"",6,",");
     ilLen = get_real_item(pclRogRegn,pclDataBufRog,1);
     ilLen = get_real_item(pclRogAurn,pclDataBufRog,2);
     ilLen = get_real_item(pclRogDurn,pclDataBufRog,3);
     ilLen = get_real_item(pclRogLand,pclDataBufRog,4);
     ilLen = get_real_item(pclRogAirb,pclDataBufRog,5);
     ilLen = get_real_item(pclRogRkey,pclDataBufRog,6);
     if (strlen(pclRogLand) > 0 && strlen(pclRogAirb) > 0)
     {
        if (strcmp(pclRogLand,pclRogAirb) < 0)
        {
           strcpy(pclRogTime,pclRogLand);
        }
        else
        {
           strcpy(pclRogTime,pclRogAirb);
        }
     }
     else
     {
        if (strlen(pclRogLand) > 0)
        {
           strcpy(pclRogTime,pclRogLand);
        }
        else
        {
           strcpy(pclRogTime,pclRogAirb);
        }
     }
     strcpy(pclTime,pclRogTime);
     if (strlen(pclAftRegn) > 0 && strcmp(pclRogRegn,pclAftRegn) == 0)
     {
        strcpy(pclAftRegn,"");
        if (strcmp(pclAftTime,pclTime) < 0)
        {
           strcpy(pclTime,pclAftTime);
        }
     }
/* Coding produced unnecessary broadcasts
     strcat(pcgRACDataList,pclRogRkey);
     strcat(pcgRACDataList,",");
     if (strlen(pcgRACBegin) == 0)
     {
        strcpy(pcgRACBegin,pclTime);
     }
     else
     {
        if (strcmp(pcgRACBegin,pclTime) > 0)
        {
           strcpy(pcgRACBegin,pclTime);
        }
     }
     if (strlen(pcgRACEnd) == 0)
     {
        strcpy(pcgRACEnd,pclTime);
     }
     else
     {
        if (strcmp(pcgRACEnd,pclTime) < 0)
        {
           strcpy(pcgRACEnd,pclTime);
        }
     }
*/
     ilRC = BuildROGTAB(pclRogRegn,pclTime);
     strcpy(pcgRACDataList,"");
     slFkt = NEXT;
     ilRCdbRog = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBufRog);
  }
  close_my_cursor(&slCursor);

  if (strlen(pclAftRegn) > 0)
  {
     ilRC = BuildROGTAB(pclAftRegn,pclAftTime);
     strcpy(pcgRACDataList,"");
  }

  return ilRC;
} /* End of UpdateROGTAB */


static int DelROG(char *pcpUrno)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  int ilRogIdx;

  if (igUseArrayInsert == TRUE)
  {
     ilRogIdx = -1;
     ilRC = SearchByKey("URNO",pcpUrno,S_BACK,&ilRogIdx);
     if (ilRogIdx >= 0)
     {
        strcpy(&rgRogDat[ilRogIdx].pclFlag[0],"D");
     }
  }
  else
  {
     strcpy(pclSqlBuf,"DELETE FROM ROGTAB ");
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"DelROG: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);
  }

  return ilRC;
} /* End of DelROG */


static void TrimRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer, " ");
  }
  else
  {
     while (isspace(*pclBlank) && pclBlank != pcpBuffer)
     {
        *pclBlank = '\0';
        pclBlank--;
     }
  }
} /* End of TrimRight */


static int JoinAftRecs(char *pcpArrUrno, char *pcpDepUrno, char *pcpRkey)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclUpdBuf[L_BUFF];
  char pclSelListArr[L_BUFF];
  char pclFieldListArr[L_BUFF];
  char pclDataListArr[L_BUFF];
  char pclSelListDep[L_BUFF];
  char pclFieldListDep[L_BUFF];
  char pclDataListDep[L_BUFF];

  strcpy(pclUpdBuf,"RTYP='J'");
  strcpy(pclFieldListArr,"RTYP");
  strcpy(pclDataListArr,"'J'");
  sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
  sprintf(pclSelectBuf,"WHERE URNO = %s",pcpArrUrno);
  sprintf(pclSelListArr,"WHERE URNO = %s \n%s",pcpArrUrno,pcpArrUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  dbg(DEBUG,"JoinAftRecs: <%s>",pclSqlBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     commit_work();
  }
  close_my_cursor(&slCursor);

  sprintf(pclUpdBuf,"RKEY=%s,RTYP='J',AURN=%s",pcpArrUrno,pcpArrUrno);
  /*      pcpRkey,pcpArrUrno);*/
  strcpy(pclFieldListDep,"RKEY,RTYP,AURN");
  sprintf(pclDataListDep,"%s,'J',%s",pcpArrUrno,pcpArrUrno);
  /*      pcpRkey,pcpArrUrno);*/
  sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
  sprintf(pclSelectBuf,"WHERE URNO = %s",pcpDepUrno);
  sprintf(pclSelListDep,"WHERE URNO = %s \n%s",pcpDepUrno,pcpDepUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  dbg(DEBUG,"JoinAftRecs: <%s>",pclSqlBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     commit_work();
  }
  close_my_cursor(&slCursor);

  ilRC = TriggerBchdlAction("UFR","AFTTAB",pclSelListArr,pclFieldListArr,
                            pclDataListArr,FALSE,TRUE);
  ilRC = TriggerBchdlAction("UFR","AFTTAB",pclSelListDep,pclFieldListDep,
                            pclDataListDep,FALSE,TRUE);

  return ilRC;
} /* End of JoinAftRecs */


static int SplitAftRecs(char *pcpArrUrno, char *pcpDepUrno)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclSelectBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclUpdBuf[L_BUFF];
  char pclSelListArr[L_BUFF];
  char pclFieldListArr[L_BUFF];
  char pclDataListArr[L_BUFF];
  char pclSelListDep[L_BUFF];
  char pclFieldListDep[L_BUFF];
  char pclDataListDep[L_BUFF];

  if (pcpArrUrno != NULL)
  {
     strcpy(pclUpdBuf,"RTYP='S'");
     strcpy(pclFieldListArr,"RTYP");
     strcpy(pclDataListArr,"'S'");
     sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpArrUrno);
     sprintf(pclSelListArr,"WHERE URNO = %s \n%s",pcpArrUrno,pcpArrUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(DEBUG,"SplitAftRecs: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
     }
     close_my_cursor(&slCursor);
  }

  if (pcpDepUrno != NULL)
  {
     sprintf(pclUpdBuf,"RKEY=%s,RTYP='S',AURN=' '",
             pcpDepUrno);
     strcpy(pclFieldListDep,"RKEY,RTYP,AURN");
     sprintf(pclDataListDep,"%s,'S',' '",
             pcpDepUrno);
     sprintf(pclSqlBuf,"UPDATE AFTTAB SET %s ",pclUpdBuf);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpDepUrno);
     sprintf(pclSelListDep,"WHERE URNO = %s \n%s",pcpDepUrno,pcpDepUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     dbg(DEBUG,"SplitAftRecs: <%s>",pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        commit_work();
     }
     close_my_cursor(&slCursor);
  }

  if (pcpArrUrno != NULL)
  {
     ilRC = TriggerBchdlAction("UFR","AFTTAB",pclSelListArr,pclFieldListArr,
                               pclDataListArr,FALSE,TRUE);
  }
  if (pcpDepUrno != NULL)
  {
     ilRC = TriggerBchdlAction("UFR","AFTTAB",pclSelListDep,pclFieldListDep,
                               pclDataListDep,FALSE,TRUE);
  }

  return ilRC;
} /* End of SplitAftRecs */


static int CheckROGvsAFT(char *pcpRegn, char *pcpSeqn)
{
  int ilRC = RC_SUCCESS;
  int ilRCdbROG = DB_SUCCESS;
  short slFktROG;
  short slCursorROG;
  char pclSqlBufROG[L_BUFF];
  char pclSelectBufROG[L_BUFF];
  char pclDataBuf[L_BUFF];
  int ilRCdbAFT = DB_SUCCESS;
  short slFktAFT;
  short slCursorAFT;
  char pclSqlBufAFT[L_BUFF];
  char pclSelectBufAFT[L_BUFF];
  int ilLen;
  char pclRogAurn[32];
  char pclRogDurn[32];
  char pclRogRkey[32];
  char pclAftaUrno[32];
  char pclAftaRegn[32];
  char pclAftaRkey[32];
  char pclAftaRtyp[32];
  char pclAftaTifa[32];
  char pclAftdUrno[32];
  char pclAftdRegn[32];
  char pclAftdRkey[32];
  char pclAftdRtyp[32];
  char pclAftdTifd[32];
  char pclAftdAurn[32];
  int ilDoSplit;
  int ilDoJoin;
  char pclSeqn[32];

  dbg(TRACE,"Check ROG vs AFT for registration <%s> starting at no. <%s>",pcpRegn,pcpSeqn);

/*
  strcpy(pcgRACDataList,"");
  strcpy(pcgRACBegin,"");
  strcpy(pcgRACEnd,"");
*/
  strcpy(pclSeqn,pcpSeqn);
  if (strlen(pclSeqn) == 0)
     strcpy(pclSeqn," ");
  strcpy(pclSqlBufROG,"SELECT AURN,DURN,RKEY FROM ROGTAB ");
  sprintf(pclSelectBufROG,"WHERE REGN = '%s' AND SEQN >= '%s' ORDER BY SEQN",pcpRegn,pclSeqn);
  strcat(pclSqlBufROG,pclSelectBufROG);
  slCursorROG = 0;
  slFktROG = START;
  /* dbg(TRACE,"<%s>",pclSqlBufROG); */
  ilRCdbROG = sql_if(slFktROG,&slCursorROG,pclSqlBufROG,pclDataBuf);
  while (ilRCdbROG == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",3,",");
     ilLen = get_real_item(pclRogAurn,pclDataBuf,1);
     ilLen = get_real_item(pclRogDurn,pclDataBuf,2);
     ilLen = get_real_item(pclRogRkey,pclDataBuf,3);
     strcpy(pclAftaRegn,"");
     strcpy(pclAftaRkey,"");
     strcpy(pclAftaRtyp,"");
     strcpy(pclAftaTifa,"");
     strcpy(pclAftdRegn,"");
     strcpy(pclAftdRkey,"");
     strcpy(pclAftdRtyp,"");
     strcpy(pclAftdTifd,"");
     strcpy(pclAftdAurn,"");

     if (strcmp(pclRogAurn,"0") != 0)
     {
        strcpy(pclSqlBufAFT,"SELECT REGN,RKEY,RTYP,TIFA FROM AFTTAB ");
        sprintf(pclSelectBufAFT,"WHERE URNO = %s",pclRogAurn);
        strcat(pclSqlBufAFT,pclSelectBufAFT);
        slCursorAFT = 0;
        slFktAFT = START;
        /* dbg(TRACE,"<%s>",pclSqlBufAFT); */
        ilRCdbAFT = sql_if(slFktAFT,&slCursorAFT,pclSqlBufAFT,pclDataBuf);
        if (ilRCdbAFT == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBuf,"",4,",");
           ilLen = get_real_item(pclAftaRegn,pclDataBuf,1);
           ilLen = get_real_item(pclAftaRkey,pclDataBuf,2);
           ilLen = get_real_item(pclAftaRtyp,pclDataBuf,3);
           ilLen = get_real_item(pclAftaTifa,pclDataBuf,4);
        }
        close_my_cursor(&slCursorAFT);
     }

     if (strcmp(pclRogDurn,"0") != 0)
     {
        strcpy(pclSqlBufAFT,"SELECT REGN,RKEY,RTYP,TIFD,AURN FROM AFTTAB ");
        sprintf(pclSelectBufAFT,"WHERE URNO = %s",pclRogDurn);
        strcat(pclSqlBufAFT,pclSelectBufAFT);
        slCursorAFT = 0;
        slFktAFT = START;
        /* dbg(TRACE,"<%s>",pclSqlBufAFT); */
        ilRCdbAFT = sql_if(slFktAFT,&slCursorAFT,pclSqlBufAFT,pclDataBuf);
        if (ilRCdbAFT == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBuf,"",5,",");
           ilLen = get_real_item(pclAftdRegn,pclDataBuf,1);
           ilLen = get_real_item(pclAftdRkey,pclDataBuf,2);
           ilLen = get_real_item(pclAftdRtyp,pclDataBuf,3);
           ilLen = get_real_item(pclAftdTifd,pclDataBuf,4);
           ilLen = get_real_item(pclAftdAurn,pclDataBuf,5);
        }
        close_my_cursor(&slCursorAFT);
     }

     if (strcmp(pclRogAurn,"0") != 0 && strcmp(pclRogDurn,"0") == 0)
     {  /* Arrival only */
        ilDoSplit = FALSE;
        strcpy(pclSqlBufAFT,"SELECT URNO FROM AFTTAB ");
        sprintf(pclSelectBufAFT,"WHERE RKEY = %s AND ADID = 'D'",pclRogRkey);
        strcat(pclSqlBufAFT,pclSelectBufAFT);
        slCursorAFT = 0;
        slFktAFT = START;
        /* dbg(TRACE,"<%s>",pclSqlBufAFT); */
        ilRCdbAFT = sql_if(slFktAFT,&slCursorAFT,pclSqlBufAFT,pclDataBuf);
        close_my_cursor(&slCursorAFT);
        if (ilRCdbAFT == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBuf,"",1,",");
           ilLen = get_real_item(pclAftdUrno,pclDataBuf,1);
           ilRC = SplitAftRecs(pclRogAurn,pclAftdUrno);
           strcat(pcgRACDataList,pclRogAurn);
           strcat(pcgRACDataList,",");
           strcat(pcgRACDataList,pclAftdUrno);
           strcat(pcgRACDataList,",");
           ilDoSplit = TRUE;
        }
        else
        {
           if (strcmp(pclAftaRtyp,"S") != 0)
           {
              ilRC = SplitAftRecs(pclRogAurn,NULL);
              strcat(pcgRACDataList,pclRogAurn);
              strcat(pcgRACDataList,",");
              ilDoSplit = TRUE;
           }
        }
        if (ilDoSplit == TRUE)
        {
           strcat(pcgRACDataList,pclRogRkey);
           strcat(pcgRACDataList,",");
           if (strlen(pcgRACBegin) == 0)
           {
              strcpy(pcgRACBegin,pclAftaTifa);
           }
           else
           {
              if (strcmp(pcgRACBegin,pclAftaTifa) > 0)
              {
                 strcpy(pcgRACBegin,pclAftaTifa);
              }
           }
           if (strlen(pcgRACEnd) == 0)
           {
              strcpy(pcgRACEnd,pclAftaTifa);
           }
           else
           {
              if (strcmp(pcgRACEnd,pclAftaTifa) < 0)
              {
                 strcpy(pcgRACEnd,pclAftaTifa);
              }
           }
        }
     }
     else
     {
        if (strcmp(pclRogAurn,"0") == 0 && strcmp(pclRogDurn,"0") != 0)
        {  /* Departure only */
           ilDoSplit = FALSE;
           strcpy(pclSqlBufAFT,"SELECT URNO FROM AFTTAB ");
           sprintf(pclSelectBufAFT,"WHERE RKEY = %s AND ADID = 'A'",pclRogRkey);
           strcat(pclSqlBufAFT,pclSelectBufAFT);
           slCursorAFT = 0;
           slFktAFT = START;
           /* dbg(TRACE,"<%s>",pclSqlBufAFT); */
           ilRCdbAFT = sql_if(slFktAFT,&slCursorAFT,pclSqlBufAFT,pclDataBuf);
           close_my_cursor(&slCursorAFT);
           if (ilRCdbAFT == DB_SUCCESS)
           {
              BuildItemBuffer(pclDataBuf,"",1,",");
              ilLen = get_real_item(pclAftaUrno,pclDataBuf,1);
              ilRC = SplitAftRecs(pclAftaUrno,pclRogDurn);
              strcat(pcgRACDataList,pclAftaUrno);
              strcat(pcgRACDataList,",");
              strcat(pcgRACDataList,pclRogDurn);
              strcat(pcgRACDataList,",");
              ilDoSplit = TRUE;
           }
           else
           {
              if (strcmp(pclAftdRtyp,"S") != 0 || strlen(pclAftdAurn) > 0)
              {
                 ilRC = SplitAftRecs(NULL,pclRogDurn);
                 strcat(pcgRACDataList,pclRogDurn);
                 strcat(pcgRACDataList,",");
                 ilDoSplit = TRUE;
              }
           }
           if (ilDoSplit == TRUE)
           {
              strcat(pcgRACDataList,pclRogRkey);
              strcat(pcgRACDataList,",");
              if (strlen(pcgRACBegin) == 0)
              {
                 strcpy(pcgRACBegin,pclAftdTifd);
              }
              else
              {
                 if (strcmp(pcgRACBegin,pclAftdTifd) > 0)
                 {
                    strcpy(pcgRACBegin,pclAftdTifd);
                 }
              }
              if (strlen(pcgRACEnd) == 0)
              {
                 strcpy(pcgRACEnd,pclAftdTifd);
              }
              else
              {
                 if (strcmp(pcgRACEnd,pclAftdTifd) < 0)
                 {
                    strcpy(pcgRACEnd,pclAftdTifd);
                 }
              }
           }
        }
        else
        {  /* Rotation */
           ilDoJoin = FALSE;
           if (strcmp(pclAftaRkey,pclRogRkey) != 0 ||
               strcmp(pclAftdRkey,pclRogRkey) != 0)
           {
              ilDoJoin = TRUE;
              strcpy(pclSqlBufAFT,"SELECT URNO FROM AFTTAB ");
              sprintf(pclSelectBufAFT,"WHERE RKEY = %s AND ADID = 'D'",pclRogRkey);
              strcat(pclSqlBufAFT,pclSelectBufAFT);
              slCursorAFT = 0;
              slFktAFT = START;
              /* dbg(TRACE,"<%s>",pclSqlBufAFT); */
              ilRCdbAFT = sql_if(slFktAFT,&slCursorAFT,pclSqlBufAFT,pclDataBuf);
              close_my_cursor(&slCursorAFT);
              if (ilRCdbAFT == DB_SUCCESS)
              {
                 BuildItemBuffer(pclDataBuf,"",1,",");
                 ilLen = get_real_item(pclAftdUrno,pclDataBuf,1);
                 ilRC = SplitAftRecs(pclRogAurn,pclAftdUrno);
              }
              ilRC = JoinAftRecs(pclRogAurn,pclRogDurn,pclRogRkey);
           }
           else
           {
              if (strcmp(pclAftaRtyp,"J") != 0 || strcmp(pclAftdRtyp,"J") != 0 || 
                  strlen(pclAftdAurn) == 0)
              {
                 ilDoJoin = TRUE;
                 ilRC = JoinAftRecs(pclRogAurn,pclRogDurn,pclRogRkey);
              }
           }
           if (ilDoJoin == TRUE)
           {
              strcat(pcgRACDataList,pclRogAurn);
              strcat(pcgRACDataList,",");
              strcat(pcgRACDataList,pclRogDurn);
              strcat(pcgRACDataList,",");
              strcat(pcgRACDataList,pclRogRkey);
              strcat(pcgRACDataList,",");
              if (strlen(pcgRACBegin) == 0)
              {
                 strcpy(pcgRACBegin,pclAftaTifa);
              }
              else
              {
                 if (strcmp(pcgRACBegin,pclAftaTifa) > 0)
                 {
                    strcpy(pcgRACBegin,pclAftaTifa);
                 }
              }
              if (strlen(pcgRACEnd) == 0)
              {
                 strcpy(pcgRACEnd,pclAftdTifd);
              }
              else
              {
                 if (strcmp(pcgRACEnd,pclAftdTifd) < 0)
                 {
                    strcpy(pcgRACEnd,pclAftdTifd);
                 }
              }
           }
        }
     }

     slFktROG = NEXT;
     ilRCdbROG = sql_if(slFktROG,&slCursorROG,pclSqlBufROG,pclDataBuf);
  }
  close_my_cursor(&slCursorROG);

  dbg(TRACE,"All records for registration <%s> checked",pcpRegn);

  return ilRC;
} /* End of CheckROGvsAFT */



static int CompressDataList(char *pcpData)
{
  int ilRC = RC_SUCCESS;
  char pclData[XXL_BUFF];
  char *pclPtrS;
  char *pclPtrE;

  strcpy(pclData,pcpData);
  strcpy(pcpData,"");
  pclPtrS = pclData;
  pclPtrE = strstr(pclPtrS,",");
  while (pclPtrE != NULL)
  {
     *pclPtrE = '\0';
     if (strstr(pcpData,pclPtrS) == NULL)
     {
        strcat(pcpData,pclPtrS);
        strcat(pcpData,",");
     }
     pclPtrS = pclPtrE + 1;
     pclPtrE = strstr(pclPtrS,",");
  }
  if (strstr(pcpData,pclPtrS) == NULL)
  {
     strcat(pcpData,pclPtrS);
     strcat(pcpData,",");
  }
  if (pcpData[strlen(pcpData)-1] == ',')
  {
     pcpData[strlen(pcpData)-1] = '\0';
  }

  return ilRC;
} /* End of CompressData */



static int AppendUrnoList()
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[L_BUFF];
  char pclDataBuf[L_BUFF];
  char pclTmpRkey[16];
  int ilCount;
  int ilI;
  int ilLen;

  ilCount = GetNoOfElements(pcgRACDataList,',');
  for (ilI = 1; ilI <= ilCount; ilI++)
  {
     ilLen = get_real_item(pclTmpRkey,pcgRACDataList,ilI);
     if (ilLen > 0)
     {
        sprintf(pclSqlBuf,
                "SELECT URNO FROM AFTTAB WHERE RKEY = %s AND (ADID = 'A' OR ADID = 'D')",
                pclTmpRkey);
        slCursor = 0;
        slFkt = START;
        /* dbg(TRACE,"<%s>",pclSqlBuf); */
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        while (ilRCdb == DB_SUCCESS)
        {
           if (strstr(pcgUrnoList,pclDataBuf) == NULL)
           {
              if (strlen(pcgUrnoList) == 0)
              {
                 strcpy(pcgUrnoList,pclDataBuf);
              }
              else
              {
                 strcat(pcgUrnoList,",");
                 strcat(pcgUrnoList,pclDataBuf);
              }
           }
           slFkt = NEXT;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        }
        close_my_cursor(&slCursor);
     }
  }

  return ilRC;
} /* End of AppendUrnoList */



static int CalculateMaxCEDATime(char *pcpResult, char *pcpVal1, char *pcpVal2, char *pcpVal3,
                                char *pcpVal4)
{
  int ilRC = RC_SUCCESS;
  char pclTmpResult[32];
  char pclTmpVal1[32];
  char pclTmpVal2[32];
  char pclTmpVal3[32];
  char pclTmpVal4[32];

  strcpy(pclTmpResult,"              ");
  strcpy(pclTmpVal1,"              ");
  strcpy(pclTmpVal2,"              ");
  strcpy(pclTmpVal3,"              ");
  strcpy(pclTmpVal4,"              ");
  if (strlen(pcpResult) == 14)
  {
     strcpy(pclTmpResult,pcpResult);
  }
  if (strlen(pcpVal1) == 14)
  {
     strcpy(pclTmpVal1,pcpVal1);
  }
  if (strlen(pcpVal2) == 14)
  {
     strcpy(pclTmpVal2,pcpVal2);
  }
  if (strlen(pcpVal3) == 14)
  {
     strcpy(pclTmpVal3,pcpVal3);
  }
  if (strlen(pcpVal4) == 14)
  {
     strcpy(pclTmpVal4,pcpVal4);
  }

  if (strcmp(pclTmpResult,pclTmpVal1) < 0)
  {
     strcpy(pclTmpResult,pclTmpVal1);
  }
  if (strcmp(pclTmpResult,pclTmpVal2) < 0)
  {
     strcpy(pclTmpResult,pclTmpVal2);
  }
  if (strcmp(pclTmpResult,pclTmpVal3) < 0)
  {
     strcpy(pclTmpResult,pclTmpVal3);
  }
  if (strcmp(pclTmpResult,pclTmpVal4) < 0)
  {
     strcpy(pclTmpResult,pclTmpVal4);
  }

  strcpy(pcpResult,pclTmpResult);

  return ilRC;
} /* End of CalculateMaxCEDATime */


static int MyAddSecondsToCEDATime(char *pcpTime, long lpValue, int ipP)
{
  int ilRC = RC_SUCCESS;

  if (strlen(pcpTime) == 12)
     strcat(pcpTime,"00");
  ilRC = AddSecondsToCEDATime(pcpTime,lpValue,ipP);
  return ilRC;
} /* End of MyAddSecondsToCEDATime */


static int SqlArrayInsert()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SqlArrayInsert:";
  int ilRCdb = DB_SUCCESS;
  char pclSqlBuf[1024];
  short slFkt = 0;
  short slCursor = 0;

  sprintf(pclSqlBuf,"INSERT INTO ROGTAB (%s) VALUES (%s)",pcgRogFld,pcgRogVal);
  slCursor = 0;
  slFkt = 0;
  if (strcmp(pcgInitRog,"NO") == 0)
     dbg(DEBUG,"%s <%s>\n%s",pclFunc,pclSqlBuf,pcgRogBuf);
  ilRCdb = SqlIfArray(slFkt,&slCursor,pclSqlBuf,pcgRogBuf,0,&rgRecDesc);
  if (ilRCdb == DB_SUCCESS)
  {
     commit_work();
  }
  else
  {
     rollback();
     dbg(TRACE,"%s Error = %d",pclFunc,ilRCdb);
  }
  close_my_cursor(&slCursor);

  return ilRC;
} /* End of SqlArrayInsert */


static int SqlArrayFill()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SqlArrayFill:";
  char pclRogRec[256];
  int ilI;
  int ilArrayPtr;

  ilArrayPtr = 0;
  for (ilI = 0; ilI < igCurRogDat; ilI++)
  {
     if (rgRogDat[ilI].pclFlag[0] == ' ')
     {
        sprintf(pclRogRec,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                &rgRogDat[ilI].pclUrno[0],
                &rgRogDat[ilI].pclHopo[0],
                &rgRogDat[ilI].pclRegn[0],
                &rgRogDat[ilI].pclAurn[0],
                &rgRogDat[ilI].pclDurn[0],
                &rgRogDat[ilI].pclRkey[0],
                &rgRogDat[ilI].pclLand[0],
                &rgRogDat[ilI].pclAirb[0],
                &rgRogDat[ilI].pclFtyp[0],
                &rgRogDat[ilI].pclStat[0],
                &rgRogDat[ilI].pclFtpa[0],
                &rgRogDat[ilI].pclFtpd[0],
                &rgRogDat[ilI].pclSeqn[0],
                &rgRogDat[ilI].pclSeqs[0]);
        StrgPutStrg(pcgRogBuf,&ilArrayPtr,pclRogRec,0,-1,"\n");
     }
  }
  ilArrayPtr--;
  pcgRogBuf[ilArrayPtr] = '\0';

  return ilRC;
} /* End of SqlArrayFill */


static int SearchByKey(char *pcpKey, char * pcpVal, int ilDirection, int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByKey:";
  int ilI;
  int ilFound;

  if (ilDirection == S_FOR)
     ilI = 0;
  else
     ilI = igCurRogDat - 1;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI < igCurRogDat && ilI >= 0)
  {
     if (strcmp(pcpKey,"URNO") == 0)
        ilRC = strcmp(&rgRogDat[ilI].pclUrno[0],pcpVal);
     else if (strcmp(pcpKey,"AURN") == 0)
             ilRC = strcmp(&rgRogDat[ilI].pclAurn[0],pcpVal);
     else if (strcmp(pcpKey,"DURN") == 0)
             ilRC = strcmp(&rgRogDat[ilI].pclDurn[0],pcpVal);
     if (ilRC == 0)
     {
        ilFound = TRUE;
        *pipIndex = ilI;
     }
     else
     {
        if (ilDirection == S_FOR)
           ilI++;
        else
           ilI--;
     }
  }

  return ilRC;
} /* End of SearchByKey */


static int SearchByAIRB(char *pcpAirb,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByAIRB:";
  int ilI;
  int ilFound;
  int ilLastIdx;
  char pclAirb[16];
  char pclLastAirb[16];

  strcpy(pclAirb,pcpAirb);
  if (strlen(pclAirb) < 14)
     strcat(pclAirb,"00");
  ilLastIdx = -1;
  ilI = 0;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI < igCurRogDat)
  {
     if (rgRogDat[ilI].pclLand[0] == ' ' &&
         rgRogDat[ilI].pclStat[0] == 'D')
     {
        ilRC = strcmp(&rgRogDat[ilI].pclAirb[0],pclAirb);
        if (ilRC > 0)
        {
           if (ilLastIdx == -1)
           {
              ilLastIdx = ilI;
              strcpy(pclLastAirb,&rgRogDat[ilI].pclAirb[0]);
           }
           else
           {
              if (strcmp(&rgRogDat[ilI].pclAirb[0],pclLastAirb) < 0)
              {
                 ilLastIdx = ilI;
                 strcpy(pclLastAirb,&rgRogDat[ilI].pclAirb[0]);
              }
           }
        }
     }
     ilI++;
  }
  *pipIndex = ilLastIdx;

  return ilRC;
} /* End of SearchByAIRB */


static int SearchByRKEYDep(char *pcpRkey,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByRKEYDep:";
  int ilI;
  int ilFound;

  ilI = igCurRogDat - 1;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI >= 0)
  {
     ilRC = strcmp(&rgRogDat[ilI].pclRkey[0],pcpRkey);
     if (ilRC == 0)
     {
        if (rgRogDat[ilI].pclLand[0] == ' ' &&
            rgRogDat[ilI].pclStat[0] == 'D')
        {
           ilFound = TRUE;
           *pipIndex = ilI;
        }
     }
     ilI--;
  }

  return ilRC;
} /* End of SearchByRKEYDep */


static int SearchByLAND(char *pcpLand,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByLAND:";
  int ilI;
  int ilFound;
  int ilLastIdx;
  char pclLastLand[16];
  int ilLastIdxRead;
  char pclLastLandRead[16];

  ilLastIdx = -1;
  ilLastIdxRead = -1;
  if (*pipIndex >= 0)
  {
     ilLastIdx = *pipIndex;
     strcpy(pclLastLand,&rgRogDat[*pipIndex].pclLand[0]);
     ilLastIdxRead = *pipIndex;
     strcpy(pclLastLandRead,&rgRogDat[*pipIndex].pclLand[0]);
  }
  else
  {
     strcpy(pclLastLandRead,pcpLand);
  }
  if (strlen(pclLastLandRead) < 14)
     strcat(pclLastLandRead,"00");
  ilI = igCurRogDat - 1;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI >= 0)
  {
     if (rgRogDat[ilI].pclFtpa[0] != 'X' &&
         rgRogDat[ilI].pclFtpa[0] != 'N' &&
         rgRogDat[ilI].pclFtpa[0] != 'D')
     {
        ilRC = strcmp(&rgRogDat[ilI].pclLand[0],pclLastLandRead);
        if (ilRC < 0)
        {
           if (ilLastIdx == -1)
           {
              ilLastIdx = ilI;
              strcpy(pclLastLand,&rgRogDat[ilI].pclLand[0]);
           }
           else
           {
              if (strcmp(&rgRogDat[ilI].pclLand[0],pclLastLand) > 0)
              {
                 ilLastIdx = ilI;
                 strcpy(pclLastLand,&rgRogDat[ilI].pclLand[0]);
              }
           }
        }
     }
     ilI--;
  }
  if (ilLastIdx >= 0)
     *pipIndex = ilLastIdx;

  return ilRC;
} /* End of SearchByLAND */


static int SearchByRKEYArr(char *pcpRkey,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByRKEYArr:";
  int ilI;
  int ilFound;

  ilI = igCurRogDat - 1;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI >= 0)
  {
     ilRC = strcmp(&rgRogDat[ilI].pclRkey[0],pcpRkey);
     if (ilRC == 0)
     {
        if (rgRogDat[ilI].pclAirb[0] == ' ' &&
            rgRogDat[ilI].pclStat[0] == 'A')
        {
           ilFound = TRUE;
           *pipIndex = ilI;
        }
     }
     ilI--;
  }

  return ilRC;
} /* End of SearchByRKEYArr */


static int SearchByLANDSeqs(char *pcpLand,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByLANDSeqs:";
  int ilI;
  int ilFound;
  int ilLastIdx;
  char pclLastLand[16];
  int ilLastIdxRead;
  char pclLastLandRead[16];

  ilLastIdx = -1;
  ilLastIdxRead = -1;
  if (*pipIndex >= 0)
  {
     ilLastIdx = *pipIndex;
     strcpy(pclLastLand,&rgRogDat[*pipIndex].pclLand[0]);
     ilLastIdxRead = *pipIndex;
     strcpy(pclLastLandRead,&rgRogDat[*pipIndex].pclLand[0]);
  }
  else
  {
     strcpy(pclLastLandRead,pcpLand);
  }
  if (strlen(pclLastLandRead) < 14)
     strcat(pclLastLandRead,"00");
  ilI = igCurRogDat - 1;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI >= 0)
  {
     if (rgRogDat[ilI].pclFtyp[0] != 'E' &&
         rgRogDat[ilI].pclStat[0] == 'B' &&
         rgRogDat[ilI].pclSeqs[0] == 'O' &&
         rgRogDat[ilI].pclLand[0] != ' ')
     {
        ilRC = strcmp(&rgRogDat[ilI].pclLand[0],pclLastLandRead);
        if (ilRC < 0)
        {
           if (ilLastIdx == -1)
           {
              ilLastIdx = ilI;
              strcpy(pclLastLand,&rgRogDat[ilI].pclLand[0]);
           }
           else
           {
              if (strcmp(&rgRogDat[ilI].pclLand[0],pclLastLand) > 0)
              {
                 ilLastIdx = ilI;
                 strcpy(pclLastLand,&rgRogDat[ilI].pclLand[0]);
              }
           }
        }
     }
     ilI--;
  }
  if (ilLastIdx >= 0)
     *pipIndex = ilLastIdx;

  return ilRC;
} /* End of SearchByLANDSeqs */


static int SearchByLANDSeqn(char *pcpSeqn,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByLANDSeqn:";
  int ilI;
  int ilFound;
  int ilLastIdx;
  char pclLastLand[16];
  int ilLastIdxRead;
  char pclLastLandRead[16];
  int ilSeqn;
  int ilRogSeqn;

  ilLastIdx = -1;
  ilLastIdxRead = -1;
  if (*pipIndex >= 0)
  {
     ilLastIdxRead = *pipIndex;
     strcpy(pclLastLandRead,&rgRogDat[*pipIndex].pclLand[0]);
  }
  else
  {
     strcpy(pclLastLandRead,"00000000000000");
  }
  if (strlen(pclLastLandRead) < 14)
     strcat(pclLastLandRead,"00");
  ilSeqn = atoi(pcpSeqn);
  ilI = 0;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI < igCurRogDat)
  {
     ilRogSeqn = atoi(&rgRogDat[ilI].pclSeqn[0]);
     if ((rgRogDat[ilI].pclStat[0] == 'A' ||
          rgRogDat[ilI].pclStat[0] == 'B' ||
          rgRogDat[ilI].pclStat[0] == 'E') &&
         (rgRogDat[ilI].pclSeqn[0] == ' ' ||
          ilRogSeqn >= ilSeqn))
     {
        ilRC = strcmp(&rgRogDat[ilI].pclLand[0],pclLastLandRead);
        if (ilRC > 0)
        {
           if (ilLastIdx == -1)
           {
              ilLastIdx = ilI;
              strcpy(pclLastLand,&rgRogDat[ilI].pclLand[0]);
           }
           else
           {
              if (strcmp(&rgRogDat[ilI].pclLand[0],pclLastLand) < 0)
              {
                 ilLastIdx = ilI;
                 strcpy(pclLastLand,&rgRogDat[ilI].pclLand[0]);
              }
           }
        }
     }
     ilI++;
  }
  if (ilLastIdx >= 0)
     *pipIndex = ilLastIdx;

  return ilRC;
} /* End of SearchByLANDSeqn */


static int SearchByAIRBSeqn(char *pcpSeqn,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchByAIRBSeqn:";
  int ilI;
  int ilFound;
  int ilLastIdx;
  char pclLastAirb[16];
  int ilLastIdxRead;
  char pclLastAirbRead[16];
  int ilSeqn;
  int ilRogSeqn;

  ilLastIdx = -1;
  ilLastIdxRead = -1;
  if (*pipIndex >= 0)
  {
     ilLastIdxRead = *pipIndex;
     strcpy(pclLastAirbRead,&rgRogDat[*pipIndex].pclAirb[0]);
  }
  else
  {
     strcpy(pclLastAirbRead,"00000000000000");
  }
  if (strlen(pclLastAirbRead) < 14)
     strcat(pclLastAirbRead,"00");
  ilSeqn = atoi(pcpSeqn);
  ilI = 0;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI < igCurRogDat)
  {
     ilRogSeqn = atoi(&rgRogDat[ilI].pclSeqn[0]);
     if ((rgRogDat[ilI].pclStat[0] == 'D' ||
          rgRogDat[ilI].pclStat[0] == 'R') &&
         (rgRogDat[ilI].pclSeqn[0] == ' ' ||
          ilRogSeqn >= ilSeqn))
     {
        ilRC = strcmp(&rgRogDat[ilI].pclAirb[0],pclLastAirbRead);
        if (ilRC > 0)
        {
           if (ilLastIdx == -1)
           {
              ilLastIdx = ilI;
              strcpy(pclLastAirb,&rgRogDat[ilI].pclAirb[0]);
           }
           else
           {
              if (strcmp(&rgRogDat[ilI].pclAirb[0],pclLastAirb) < 0)
              {
                 ilLastIdx = ilI;
                 strcpy(pclLastAirb,&rgRogDat[ilI].pclAirb[0]);
              }
           }
        }
     }
     ilI++;
  }
  if (ilLastIdx >= 0)
     *pipIndex = ilLastIdx;

  return ilRC;
} /* End of SearchByAIRBSeqn */


static int SearchBySEQN(char *pcpSeqn,int *pipIndex)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchBySEQN:";
  int ilI;
  int ilFound;
  int ilLastIdx;
  char pclLastSeqn[16];
  int ilLastIdxRead;
  char pclLastSeqnRead[16];
  int ilSeqn;
  int ilRogSeqn;

  ilLastIdx = -1;
  ilLastIdxRead = -1;
  if (*pipIndex >= 0)
  {
     ilLastIdxRead = *pipIndex;
     strcpy(pclLastSeqnRead,&rgRogDat[*pipIndex].pclSeqn[0]);
  }
  else
  {
     strcpy(pclLastSeqnRead,"0000000000");
  }
  ilSeqn = atoi(pcpSeqn);
  ilI = 0;
  ilFound = FALSE;
  while (ilFound == FALSE && igCurRogDat >= 0 && ilI < igCurRogDat)
  {
     ilRogSeqn = atoi(&rgRogDat[ilI].pclSeqn[0]);
     if (rgRogDat[ilI].pclFlag[0] == ' ')
     {
        ilRC = strcmp(&rgRogDat[ilI].pclSeqn[0],pclLastSeqnRead);
        if (ilRC > 0)
        {
           if (ilLastIdx == -1)
           {
              ilLastIdx = ilI;
              strcpy(pclLastSeqn,&rgRogDat[ilI].pclSeqn[0]);
           }
           else
           {
              if (strcmp(&rgRogDat[ilI].pclSeqn[0],pclLastSeqn) < 0)
              {
                 ilLastIdx = ilI;
                 strcpy(pclLastSeqn,&rgRogDat[ilI].pclSeqn[0]);
              }
           }
        }
     }
     ilI++;
  }
  if (ilLastIdx >= 0)
     *pipIndex = ilLastIdx;

  return ilRC;
} /* End of SearchBySEQN */


static int CheckBcMode(char *pcpNPabs, char *pcpOPabs, char *pcpNPaes, char *pcpOPaes,
                       char *pcpNPdbs, char *pcpOPdbs, char *pcpNPdes, char *pcpOPdes,
                       char *pcpFieldList, char *pcpDataList)
{
  int ilRC = RC_SUCCESS;

  if (igBcOutMode == 1)
  {
     if (strcmp(pcpNPabs,pcpOPabs) != 0)
     {
        strcat(pcpFieldList,",#PABS");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpOPabs);
     }
     if (strcmp(pcpNPaes,pcpOPaes) != 0)
     {
        strcat(pcpFieldList,",#PAES");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpOPaes);
     }
     if (strcmp(pcpNPdbs,pcpOPdbs) != 0)
     {
        strcat(pcpFieldList,",#PDBS");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpOPdbs);
     }
     if (strcmp(pcpNPdes,pcpOPdes) != 0)
     {
        strcat(pcpFieldList,",#PDES");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpOPdes);
     }
  }
  if (igBcOutMode == 2)
  {
     strcat(pcpFieldList,",[CF]");
     strcat(pcpDataList,",[CF]");
     if (strcmp(pcpNPabs,pcpOPabs) != 0)
     {
        strcat(pcpFieldList,",PABS");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpNPabs);
     }
     if (strcmp(pcpNPaes,pcpOPaes) != 0)
     {
        strcat(pcpFieldList,",PAES");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpNPaes);
     }
     if (strcmp(pcpNPdbs,pcpOPdbs) != 0)
     {
        strcat(pcpFieldList,",PDBS");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpNPdbs);
     }
     if (strcmp(pcpNPdes,pcpOPdes) != 0)
     {
        strcat(pcpFieldList,",PDES");
        strcat(pcpDataList,",");
        strcat(pcpDataList,pcpNPdes);
     }
  }

  return ilRC;
} /* End of CheckBcMode */

