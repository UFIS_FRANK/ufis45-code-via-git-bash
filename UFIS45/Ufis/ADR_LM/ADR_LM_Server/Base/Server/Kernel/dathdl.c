
#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/ADR_LM/ADR_LM_Server/Base/Server/Kernel/dathdl.c 1.1 2005/02/23 16:54:53SGT mcu Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : MCU                                                       */
/* Date           : 07.02.2005                                                */
/* Description    : dathdl gets default allocation time for positions and gates*/
/*                                                                            */
/* Update history :                                                           */
/******************************************************************************/
/*                                                                            */
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "EvalFipsRules.h"

#define CMD_GFC     (0)
#define CMD_PFC     (1)
#define CMD_GDA     (2)

static char *prgCmdTxt[] = {"GFC","PFC","GDA",""};
static int    rgCmdDef[] = {CMD_GFC,CMD_PFC,CMD_GDA,0};


/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = DEBUG;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static char cgTwStart[128];
static char cgTwEnd[128];
static char cgRecvName[128];
static char cgDestName[128];

static int igDefPsta = 30;
static int igDefPstd = 30;
static int igDefGata = 30;
static int igDefGatd = 30;


/******************************************************************************/
/* Function prototypes                                                          */
static int  GetCommand(char *pcpCommand, int *pipCmd);


/******************************************************************************/
static int InitDathdl();
static int    Reset(void);                       /* Reset program          */
static void    Terminate(void);                   /* Terminate program      */
static void    HandleSignal(int);                 /* Handles signals        */
static void    HandleErr(int);                    /* Handles general errors */
static void    HandleQueErr(int);                 /* Handles queuing errors */
static int HandleData(EVENT *prpEvent);
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/**************************************************************************/
MAIN
{
    int    ilRC = RC_SUCCESS;            /* Return code            */
    int    ilCnt = 0;
    
    INITIALIZE;            /* General initialization    */

    dbg(TRACE,"MAIN: version <%s>",mks_version);
    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

  do
  {
    ilRC = init_db();
    if (ilRC != RC_SUCCESS)
    {
      check_ret(ilRC);
      dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
      sleep(6);
      ilCnt++;
    } /* end of if */
  } while((ilCnt < 10) && (ilRC != RC_SUCCESS));


    sprintf(cgConfigFile,"%s/dathdl.cfg",getenv("CFG_PATH")); 
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRC = InitDathdl();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"InitDathdl: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate();
    }/* end of if */
    dbg(TRACE,"MAIN: initializing OK");
    for(;;)
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
            
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                break;    

            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate();
                break;
                    
            case    RESET        :
                ilRC = Reset();
                break;
                    
            case    EVENT_DATA    :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRC = HandleData(prgEvent);
                    if(ilRC != RC_SUCCESS)
                    {
                        HandleErr(ilRC);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
        
    } /* end for */
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int InitDathdl()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
		char clResult[512];


    /* now reading from configfile or from database */
    SetSignals(HandleSignal);
    igInitOK = TRUE;
  /* Read debug_level*/
  ilRC=iGetConfigEntry(cgConfigFile,"SYSTEM","debug_level",CFG_STRING,clResult);
  if(ilRC == RC_SUCCESS) 
  {
   dbg(DEBUG,"debug_level = <%s>",clResult); 
   debug_level = TRACE ;
   if(strcmp(clResult,"DEBUG") == 0)
   {
    debug_level = DEBUG;
   }
   else if(strcmp(clResult,"OFF") == 0) debug_level = 0 ;
  }
  ilRC=iGetConfigEntry(cgConfigFile,"DEFAULTDAT","PSTA",CFG_STRING,clResult);
  if(ilRC == RC_SUCCESS) 
  {
		igDefPsta = atoi(clResult);
    dbg(TRACE,"default PSTA = <%d/%s>",igDefPsta,clResult); 
	}   
	else
	{
    dbg(TRACE,"default PSTA not found in config file set to %d",igDefPsta); 
	}
  ilRC=iGetConfigEntry(cgConfigFile,"DEFAULTDAT","PSTD",CFG_STRING,clResult);
  if(ilRC == RC_SUCCESS) 
  {
		igDefPstd = atoi(clResult);
    dbg(TRACE,"default PSTD = <%d/%s>",igDefPstd,clResult); 
	}   
	else
	{
    dbg(TRACE,"default PSTD not found in config file set to %d",igDefPstd); 
	}

  ilRC=iGetConfigEntry(cgConfigFile,"DEFAULTDAT","GATA",CFG_STRING,clResult);
  if(ilRC == RC_SUCCESS) 
  {
		igDefGata = atoi(clResult);
    dbg(TRACE,"default GATA = <%d/%s>",igDefGata,clResult); 
	}   
	else
	{
    dbg(TRACE,"default GATA not found in config file set to %d",igDefGata); 
	}

  ilRC=iGetConfigEntry(cgConfigFile,"DEFAULTDAT","GATD",CFG_STRING,clResult);
  if(ilRC == RC_SUCCESS) 
  {
		igDefGatd = atoi(clResult);
    dbg(TRACE,"default GATD = <%d/%s>",igDefGatd,clResult); 
	}   
	else
	{
    dbg(TRACE,"default GATD not found in config file set to %d",igDefGatd); 
	}


    return(ilRC);
    
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRC = RC_SUCCESS;                /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRC;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{

    dbg(TRACE,"Terminate: now leaving ...");
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
    switch(pipSig)
    {
    default    :
        Terminate();
        break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                ilBreakOut = TRUE;
                break;    

            case    SHUTDOWN    :
                Terminate();
                break;
                        
            case    RESET        :
                ilRC = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = InitDathdl();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"InitDathdl: init failed!");
            } /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */
    
/*****************************************************************************
 * Function ReplaceNthData: locate Nth data and replace it
 * in/out: pclSrcTrg: data string, caller must ensure that adding / replacing 
 *                    pcpRepl does not exceed size
 * in:  ipPos:        positional number of data field
 * in:  cSep:         use this char as Seperator
 * in:  pcpRepl:      string to insert instead of data field [ipPos]
 *****************************************************************************
 */
static void ReplaceNthData(char *pclSrcTrg, int ipPos, char cSep, char *pcpRepl)
{
  char *pclTmp;
  char *pclCopy;
  int ii;
  
  pclCopy = strdup(pclSrcTrg);
  pclTmp  = pclCopy;
  if (ipPos>1)
  {
    for (ii= 1; ii < ipPos ; ii++)
    {
      pclTmp= strchr(pclTmp,cSep);
      pclTmp++;
    }
  }
  ii= pclTmp-pclCopy;
  pclTmp= strchr(pclTmp,cSep);
  pclSrcTrg[0]= 0;
  if (ii>0)
  {
     strncpy(pclSrcTrg,pclCopy,ii);
     pclSrcTrg[ii] = 0;
  }
  strcat(pclSrcTrg,pcpRepl);
  if (pclTmp!=NULL)
  {
    strcat(pclSrcTrg,pclTmp);
  }
  free (pclCopy);
}

static void ReplaceDataByFieldName(char *pcpFieldList,char *pcpDataList,
			char *pcpFieldName, char *pcpData)
{

	int ilItemNo;

	ilItemNo = get_item_no(pcpFieldList,pcpFieldName,5);
	if (ilItemNo > -1)
	{
		ReplaceNthData(pcpDataList,ilItemNo+1,',',pcpData);
	}
}

static int GetCommand(char *pcpCommand, int *pipCmd)
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int ilLoop = 0;
	char clCommand[48];
	
	memset(&clCommand[0],0x00,8);

	ilRc = get_real_item(&clCommand[0],pcpCommand,1);
	if(ilRc > 0)
	{
		ilRc = RC_FAIL;
	}/* end of if */

	while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
	{
		ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
		ilLoop++;
	}/* end of while */

	if(ilRc == 0)
	{
		ilLoop--;
		dbg(DEBUG,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
		*pipCmd = rgCmdDef[ilLoop];
	}else{
		dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]);
		ilRc = RC_FAIL;
	}/* end of if */

	return(ilRc);
	
}

static int GetItemValue(char *pcpDest,char *pcpFieldList,char *pcpData,char *pcpField)
{
	int ilRc = RC_FAIL;

	int ilItemNo;

	ilItemNo = get_item_no(pcpFieldList,pcpField,5);
	if (ilItemNo > -1)
	{
		get_real_item(pcpDest,pcpData,ilItemNo+1);
		ilRc = RC_SUCCESS;
	}
	return ilRc;
}

static int GetDatForGates(char *pcpDestFields,char *pcpDestData,char *pcpFields,char *pcpData)
{
	int ilRc = RC_SUCCESS;

	char clAdid[24] = "";
	char clAloc[24] = "";
	char clAlid[24] = "";
	char clAlc3[24] = "";
	char clAlc2[24] = "";
	char clAct3[24] = "";
	char clAct5[24] = "";
	char clTtyp[24] = "";
	int ilAllocTime = 0;
	char clTmpData[24];

	if (ilRc == RC_SUCCESS)
	{
		GetItemValue(clAlc3,pcpFields,pcpData,"ALC3");
	}
	if (ilRc == RC_SUCCESS)
	{
		GetItemValue(clAct3,pcpFields,pcpData,"ACT3");
	}
	if (ilRc == RC_SUCCESS)
	{
		GetItemValue(clAct5,pcpFields,pcpData,"ACT5");
	}
	if (ilRc == RC_SUCCESS)
	{
		GetItemValue(clTtyp,pcpFields,pcpData,"TTYP");
	}
	if (ilRc == RC_SUCCESS)
	{
		ilRc = GetItemValue(clAdid,pcpFields,pcpData,"ADID");
	}

	dbg(DEBUG,"ALC3=<%s>,ACT3=<%s>,ACT5=<%s>,TTYP=<%s>,ADID=<%s>",
			clAlc3,clAct3,clAct5,clTtyp,clAdid);
	if (ilRc == RC_SUCCESS)
	{

		switch(*clAdid)
		{
		case 'A' :
					ilRc = GetItemValue(clAlid,pcpFields,pcpData,"GTA1");
					if (ilRc == RC_SUCCESS)
					{
							ilRc = GetDefAllocTime(ALOC_GAT,clAdid,clAlid,clAlc3,clAct3,clAct5,
										clTtyp,&ilAllocTime);
							if (ilRc != RC_SUCCESS)
							{
								ilAllocTime = igDefGata;
							}
							sprintf(clTmpData,"%d",ilAllocTime);
							ReplaceDataByFieldName(pcpDestFields,pcpDestData,"BAA4",clTmpData);
					}
					ilRc = GetItemValue(clAlid,pcpFields,pcpData,"GTA2");
					if (ilRc == RC_SUCCESS)
					{
							ilRc = GetDefAllocTime(ALOC_GAT,clAdid,clAlid,clAlc3,clAct3,clAct5,
										clTtyp,&ilAllocTime);
							if (ilRc != RC_SUCCESS)
							{
								ilAllocTime = igDefGata;
							}
							sprintf(clTmpData,"%d",ilAllocTime);
							ReplaceDataByFieldName(pcpDestFields,pcpDestData,"BAA5",clTmpData);
					}
				break;
		case 'D' :
					ilRc = GetItemValue(clAlid,pcpFields,pcpData,"GTD1");
					if (ilRc == RC_SUCCESS)
					{
							ilRc = GetDefAllocTime(ALOC_GAT,clAdid,clAlid,clAlc3,clAct3,clAct5,
										clTtyp,&ilAllocTime);
							if (ilRc != RC_SUCCESS)
							{
								ilAllocTime = igDefGatd;
							}
							sprintf(clTmpData,"%d",ilAllocTime);
							ReplaceDataByFieldName(pcpDestFields,pcpDestData,"BAD4",clTmpData);
					}
					ilRc = GetItemValue(clAlid,pcpFields,pcpData,"GTD2");
					if (ilRc == RC_SUCCESS)
					{
							ilRc = GetDefAllocTime(ALOC_GAT,clAdid,clAlid,clAlc3,clAct3,clAct5,
										clTtyp,&ilAllocTime);
							if (ilRc != RC_SUCCESS)
							{
								ilAllocTime = igDefGatd;
							}
							sprintf(clTmpData,"%d",ilAllocTime);
							ReplaceDataByFieldName(pcpDestFields,pcpDestData,"BAD5",clTmpData);
					}
				break;
		case 'B' :
					strcpy(clAdid,"A");
					ilRc = GetItemValue(clAlid,pcpFields,pcpData,"GTA1");
					if (ilRc == RC_SUCCESS)
					{
							ilRc = GetDefAllocTime(ALOC_GAT,clAdid,clAlid,clAlc3,clAct3,clAct5,
										clTtyp,&ilAllocTime);
							if (ilRc != RC_SUCCESS)
							{
								ilAllocTime = igDefGata;
							}
							sprintf(clTmpData,"%d",ilAllocTime);
							ReplaceDataByFieldName(pcpDestFields,pcpDestData,"BAA4",clTmpData);
					}
					ilRc = GetItemValue(clAlid,pcpFields,pcpData,"GTA2");
					if (ilRc == RC_SUCCESS)
					{
							ilRc = GetDefAllocTime(ALOC_GAT,clAdid,clAlid,clAlc3,clAct3,clAct5,
										clTtyp,&ilAllocTime);
							if (ilRc != RC_SUCCESS)
							{
								ilAllocTime = igDefGata;
							}
							sprintf(clTmpData,"%d",ilAllocTime);
							ReplaceDataByFieldName(pcpDestFields,pcpDestData,"BAA5",clTmpData);
					strcpy(clAdid,"D");
					ilRc = GetItemValue(clAlid,pcpFields,pcpData,"GTD1");
					if (ilRc == RC_SUCCESS)
					{
							ilRc = GetDefAllocTime(ALOC_GAT,clAdid,clAlid,clAlc3,clAct3,clAct5,
										clTtyp,&ilAllocTime);
							if (ilRc != RC_SUCCESS)
							{
								ilAllocTime = igDefGatd;
							}
							sprintf(clTmpData,"%d",ilAllocTime);
							ReplaceDataByFieldName(pcpDestFields,pcpDestData,"BAD4",clTmpData);
					}
					ilRc = GetItemValue(clAlid,pcpFields,pcpData,"GTD2");
					if (ilRc == RC_SUCCESS)
					{
							ilRc = GetDefAllocTime(ALOC_GAT,clAdid,clAlid,clAlc3,clAct3,clAct5,
										clTtyp,&ilAllocTime);
							if (ilRc != RC_SUCCESS)
							{
								ilAllocTime = igDefGatd;
							}
							sprintf(clTmpData,"%d",ilAllocTime);
							ReplaceDataByFieldName(pcpDestFields,pcpDestData,"BAD5",clTmpData);
						}
				break;
	  }
	}
	}
	return ilRc;
}

static int GetDatForPosition(char *pcpDestFields,char *pcpDestData,char *pcpFields,char *pcpData)
{
	int ilRc = RC_SUCCESS;

	char clAdid[24] = "";
	char clAloc[24] = "";
	char clAlid[24] = "";
	char clAlc3[24] = "";
	char clAlc2[24] = "";
	char clAct3[24] = "";
	char clAct5[24] = "";
	char clTtyp[24] = "";
	int ilAllocTime = 0;
	char clTmpData[24];

	if (ilRc == RC_SUCCESS)
	{
		GetItemValue(clAlc3,pcpFields,pcpData,"ALC3");
	}
	if (ilRc == RC_SUCCESS)
	{
		GetItemValue(clAct3,pcpFields,pcpData,"ACT3");
	}
	if (ilRc == RC_SUCCESS)
	{
		GetItemValue(clAct5,pcpFields,pcpData,"ACT5");
	}
	if (ilRc == RC_SUCCESS)
	{
		GetItemValue(clTtyp,pcpFields,pcpData,"TTYP");
	}
	if (ilRc == RC_SUCCESS)
	{
		ilRc = GetItemValue(clAdid,pcpFields,pcpData,"ADID");
	}

	dbg(DEBUG,"ALC3=<%s>,ACT3=<%s>,ACT5=<%s>,TTYP=<%s>,ADID=<%s>",
			clAlc3,clAct3,clAct5,clTtyp,clAdid);
	if (ilRc == RC_SUCCESS)
	{
		switch(*clAdid)
		{
		case 'A' :
					ilRc = GetItemValue(clAlid,pcpFields,pcpData,"PSTA");
					if (ilRc == RC_SUCCESS)
					{
							ilRc = GetDefAllocTime(ALOC_PST,clAdid,clAlid,clAlc3,clAct3,clAct5,
										clTtyp,&ilAllocTime);
							if (ilRc != RC_SUCCESS)
							{
								ilAllocTime = igDefPsta;
							}
							sprintf(clTmpData,"%d",ilAllocTime);
							ReplaceDataByFieldName(pcpDestFields,pcpDestData,"BAA4",clTmpData);
					}
				break;
		case 'D' :
					ilRc = GetItemValue(clAlid,pcpFields,pcpData,"PSTD");
					if (ilRc == RC_SUCCESS)
					{
							ilRc = GetDefAllocTime(ALOC_PST,clAdid,clAlid,clAlc3,clAct3,clAct5,
										clTtyp,&ilAllocTime);
							if (ilRc != RC_SUCCESS)
							{
								ilAllocTime = igDefPsta;
							}
							sprintf(clTmpData,"%d",ilAllocTime);
							ReplaceDataByFieldName(pcpDestFields,pcpDestData,"BAA5",clTmpData);
					}
				break;
		case 'B' :
					ilRc = GetItemValue(clAlid,pcpFields,pcpData,"PSTA");
					if (ilRc == RC_SUCCESS)
					{
							ilRc = GetDefAllocTime(ALOC_PST,clAdid,clAlid,clAlc3,clAct3,clAct5,
										clTtyp,&ilAllocTime);
							if (ilRc != RC_SUCCESS)
							{
								ilAllocTime = igDefPsta;
							}
							sprintf(clTmpData,"%d",ilAllocTime);
							ReplaceDataByFieldName(pcpDestFields,pcpDestData,"BAA4",clTmpData);
					}
					ilRc = GetItemValue(clAlid,pcpFields,pcpData,"PSTD");
					if (ilRc == RC_SUCCESS)
					{
							int ilOutbound;

							ilRc = GetDefAllocTime(ALOC_PST,clAdid,clAlid,clAlc3,clAct3,clAct5,
										clTtyp,&ilOutbound);
							if (ilRc != RC_SUCCESS)
							{
								ilAllocTime = igDefPsta;
							}
							sprintf(clTmpData,"%d",ilAllocTime);
							ReplaceDataByFieldName(pcpDestFields,pcpDestData,"BAA5",clTmpData);
					}
				break;
	  }
	}
	return ilRc;
}


/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      ilCmd          = 0;

	BC_HEAD *prlBchd       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRow          = NULL;
	char 		clTable[34];
	char    clTmpJob[12];
	char    clOabs[12] = "";
	char    clFields[2048] = "";
	char    clData[2048] = "";
	char clDrrUrno[24];

	prlBchd    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchd->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	strcpy(clTable,prlCmdblk->obj_name);

	dbg(TRACE,"==========START ==========");


  /* Save Global Elements of CMDBLK */
  strcpy(cgTwStart,prlCmdblk->tw_start);
  strcpy(cgTwEnd,prlCmdblk->tw_end);
  /* Save Global Elements of BC_HEAD */
  strcpy(cgRecvName,prlBchd->recv_name);
  strcpy(cgDestName,prlBchd->dest_name);
 
	/****************************************/
	DebugPrintBchead(DEBUG,prlBchd);
	DebugPrintCmdblk(DEBUG,prlCmdblk);
  dbg(DEBUG,"TwStart   <%s>",cgTwStart);
  dbg(DEBUG,"TwEnd     <%s>",cgTwEnd);
	dbg(DEBUG,"originator<%d>",prpEvent->originator);
	dbg(DEBUG,"selection <%s>",pclSelection);
	dbg(DEBUG,"fields    <%s>",pclFields);
	dbg(DEBUG,"data      <%s>",pclData);
	/****************************************/

  
	ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
	dbg(DEBUG,"RC from GetCommand(%s) is %d",&ilCmd,ilRc);
	strcpy(clFields,pclFields);
	strcpy(clData,pclData);
	if(ilRc == RC_SUCCESS)
	{
		switch (ilCmd)
		{
   	case CMD_PFC :
				dbg(TRACE,"Command is PFC");
				GetDatForPosition(clFields,clData,pclFields,pclData);
				dbg(DEBUG,"SendAnswer to %d: <%s> <%s> rc=%d",
							prpEvent->originator,clFields,clData,ilRc);
				tools_send_sql_rc(prpEvent->originator,"PFC","AFTTAB",
					 				"",clFields,clData,ilRc);
			 break;
   	case CMD_GFC :
				dbg(TRACE,"Command is GFC");
				GetDatForGates(clFields,clData,pclFields,pclData);
				dbg(DEBUG,"SendAnswer to %d: <%s> <%s> rc=%d",
							prpEvent->originator,clFields,clData,ilRc);
				tools_send_sql_rc(prpEvent->originator,"GFC","AFTTAB",
					 				"",clFields,clData,ilRc);
			 break;
   	case CMD_GDA :
				dbg(TRACE,"Command is GDA");
				strcpy(clFields,"PSTA,PSTD,GTA1,GTD1");
				sprintf(clData,"%d,%d,%d,%d",igDefPsta,igDefPstd,igDefGata,igDefGatd);
				ilRc = RC_SUCCESS;
				dbg(DEBUG,"SendAnswer to %d: <%s> <%s> rc=%d",
							prpEvent->originator,clFields,clData,ilRc);
				tools_send_sql_rc(prpEvent->originator,"GDA","AFTTAB",
					 				"",clFields,clData,ilRc);
			 break;
		default:
			dbg(TRACE,"unknown command <%s>",prlCmdblk->command);
		}
	}

	dbg(TRACE,"==========END ==========");

	return(RC_SUCCESS);
	
}
