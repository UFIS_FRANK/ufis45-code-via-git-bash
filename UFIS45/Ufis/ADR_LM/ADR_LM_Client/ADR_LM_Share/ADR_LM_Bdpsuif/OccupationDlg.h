#ifndef AFX_OCCUPATIONDLG_H__1FA7A531_43D0_11D2_802A_004095434A85__INCLUDED_
#define AFX_OCCUPATIONDLG_H__1FA7A531_43D0_11D2_802A_004095434A85__INCLUDED_

// OccupationDlg.h : Header-Datei
//
#include <CedaOccData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld OccupationDlg 

class OccupationDlg : public CDialog
{
// Konstruktion
public:
	OccupationDlg(OCCDATA *popOcc,CWnd* pParent = NULL);   // standard constructor

	CString omName;

// Dialogfelddaten
	//{{AFX_DATA(OccupationDlg)
	enum { IDD = IDD_OCCUPATIONDLG };
	CComboBox	m_HandlingAgents;
	CButton	m_OK;
	CButton	m_DAYS_T;
	CButton	m_DAYS_7;
	CButton	m_DAYS_6;
	CButton	m_DAYS_5;
	CButton	m_DAYS_4;
	CButton	m_DAYS_2;
	CButton	m_DAYS_3;
	CButton	m_DAYS_1;
	CCSEdit	m_NAME;
	CCSEdit	m_NAFRD;
	CCSEdit	m_NAFRT;
	CCSEdit	m_NATOD;
	CCSEdit	m_NATOT;
	CCSEdit	m_RESN;
	CCSEdit	m_TIFR;
	CCSEdit	m_TITO;
	CButton	m_UTC;
	CButton	m_LOCAL;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(OccupationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(OccupationDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg	void OnUTC();
	afx_msg void OnLocal();	
	afx_msg void OnDaysT();
	afx_msg void OnDays17();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	OccupationDlg(OCCDATA *popOcc,int ipDlg,CWnd* pParent = NULL);   // standard constructor

protected:

	OCCDATA *pomOcc;
	int		imLastSelection;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_OCCUPATIONDLG_H__1FA7A531_43D0_11D2_802A_004095434A85__INCLUDED_
