// ExtLMGatPosCheckinCounterDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <ExtLMGatPosCheckinCounterDlg.h>
#include <PrivList.h>
#include <OccupationDlg.h>
#include <CedaHAGData.h>
#include <CedaAloData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ExtLMGatPosCheckinCounterDlg dialog


ExtLMGatPosCheckinCounterDlg::ExtLMGatPosCheckinCounterDlg(CICDATA *popCIC,CWnd* pParent /*=NULL*/)
	: GatPosCheckinCounterDlg(popCIC,ExtLMGatPosCheckinCounterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ExtLMGatPosCheckinCounterDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomOccTable = new CCSTable;
}

ExtLMGatPosCheckinCounterDlg::~ExtLMGatPosCheckinCounterDlg()
{
	delete pomOccTable;
}


void ExtLMGatPosCheckinCounterDlg::DoDataExchange(CDataExchange* pDX)
{
	GatPosCheckinCounterDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ExtLMGatPosCheckinCounterDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Control(pDX, IDC_CATR,	 m_CATR);
	DDX_Control(pDX, IDC_CSEQ,	 m_CSEQ);
	DDX_Control(pDX, IDC_HA,	 m_OCCFRAME);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ExtLMGatPosCheckinCounterDlg, GatPosCheckinCounterDlg)
	//{{AFX_MSG_MAP(ExtLMGatPosCheckinCounterDlg)
		// NOTE: the ClassWizard will add message map macros here
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_HADEL, OnOccDel)
	ON_BN_CLICKED(IDC_HANEW, OnOccNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ExtLMGatPosCheckinCounterDlg message handlers
BOOL ExtLMGatPosCheckinCounterDlg::OnInitDialog() 
{
	BOOL blResult = GatPosCheckinCounterDlg::OnInitDialog();

	m_CATR.SetTypeToString("X(6)",6,0);
	m_CATR.SetTextErrColor(RED);
	m_CATR.SetInitText(pomCIC->Catr);

	m_CSEQ.SetFormat("x|#x|#x|#");
	m_CSEQ.SetTextLimit(0,3);
	m_CSEQ.SetTextErrColor(RED);
	m_CSEQ.SetInitText(pomCIC->Cseq);

	CString olAlid;
	olAlid.Format("%d",pomCIC->Urno);
	MakeOccTable("CIC", olAlid);

	return blResult;

}

void ExtLMGatPosCheckinCounterDlg::OnOK() 
{
	CString olErrorText;
	bool ilStatus = true;

	if(m_CATR.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING1090) + ogNotFormat;
	}

	if(m_CSEQ.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING1091) + ogNotFormat;
	}

	if(ilStatus == true)
	{
		m_CATR.GetWindowText(pomCIC->Catr,7);
		m_CSEQ.GetWindowText(pomCIC->Cseq,4);
		GatPosCheckinCounterDlg::OnOK();
	}	
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_CNAM.SetFocus();
		m_CNAM.SetSel(0,-1);
	}


}


//----------------------------------------------------------------------------------------

void ExtLMGatPosCheckinCounterDlg::MakeOccTable(CString opAloc, CString opAlid)
{
	CRect olRectBorder;
	m_OCCFRAME.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

    pomOccTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomOccTable->SetSelectMode(0);
	pomOccTable->SetShowSelection(TRUE);
	pomOccTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1095),ilPos++,true,'|');	// Handling agent
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1095),ilPos++,true,'|');	// Occupied from
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1095),ilPos++,true,'|');	// Occupied to
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1095),ilPos++,true,'|');	// Days
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1095),ilPos++,true,'|');	// Tifr
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1095),ilPos++,true,'|');	// Tito
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1095),ilPos++,true,'|');	// Rema
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	pomOccTable->SetHeaderFields(olHeaderDataArray);
	pomOccTable->SetDefaultSeparator();
	olHeaderDataArray.DeleteAll();

	char clWhere[100];
	CCSPtrArray<OCCDATA> olOccCPA;
	sprintf(clWhere,"WHERE ALOC='%ld' AND ALID='%s'",ogAloData.GetAloUrnoByName(opAloc),opAlid);
	ogOccData.ReadSpecialData(&omOccPtrA,clWhere,"",false);
	int ilLines = omOccPtrA.GetSize();

	pomOccTable->SetDefaultSeparator();
	pomOccTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		HAGDATA *prlHAG = ogHAGData.GetHAGByUrno(omOccPtrA[ilLineNo].Ocby);
		if (prlHAG != NULL)
			rlColumnData.Text = prlHAG->Hnam;
		else
			rlColumnData.Text = "";

		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omOccPtrA[ilLineNo].Ocfr.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omOccPtrA[ilLineNo].Octo.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omOccPtrA[ilLineNo].Days;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		char pclTmp[10]="";
		rlColumnData.Text = "";
		if(strlen(omOccPtrA[ilLineNo].Tifr) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(omOccPtrA[ilLineNo].Tifr).Left(2), CString(omOccPtrA[ilLineNo].Tifr).Right(2));
			rlColumnData.Text = pclTmp;
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = "";
		if(strlen(omOccPtrA[ilLineNo].Tito) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(omOccPtrA[ilLineNo].Tito).Left(2), CString(omOccPtrA[ilLineNo].Tito).Right(2));
			rlColumnData.Text = pclTmp;
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omOccPtrA[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomOccTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomOccTable->DisplayTable();
}

//----------------------------------------------------------------------------------------

void ExtLMGatPosCheckinCounterDlg::ChangeOccTable(OCCDATA *prpOcc, int ipLineNo)
{
	if (prpOcc != NULL)
	{
		TABLE_COLUMN rlColumn;
		CCSPtrArray <TABLE_COLUMN> olLine;

		rlColumn.Lineno = 0;
		rlColumn.VerticalSeparator = SEPA_NONE;
		rlColumn.SeparatorType = SEPA_NONE;
		rlColumn.BkColor = WHITE;
		rlColumn.Font = &ogCourier_Regular_10;
		rlColumn.Alignment = COLALIGN_LEFT;

		int ilColumnNo = 0;

		HAGDATA *prlHAG = ogHAGData.GetHAGByUrno(prpOcc->Ocby);
		if (prlHAG != NULL)
			rlColumn.Text = prlHAG->Hnam;
		else
			rlColumn.Text = "";

		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text = prpOcc->Ocfr.Format("%d.%m.%Y %H:%M");
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text = prpOcc->Octo.Format("%d.%m.%Y %H:%M"); 
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text = prpOcc->Days;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		char pclTmp[10]="";
		rlColumn.Text = "";
		if(strlen(prpOcc->Tifr) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(prpOcc->Tifr).Left(2), CString(prpOcc->Tifr).Right(2));
			rlColumn.Text = pclTmp;
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text = "";
		if(strlen(prpOcc->Tito) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(prpOcc->Tito).Left(2), CString(prpOcc->Tito).Right(2));
			rlColumn.Text = pclTmp;
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text = prpOcc->Rema;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		if(ipLineNo>-1)
		{
			pomOccTable->ChangeTextLine(ipLineNo, &olLine, (void *)prpOcc);
			pomOccTable->DisplayTable();
		}
		else
		{
			if(prpOcc != NULL)
			{
				pomOccTable->AddTextLine(olLine, (void *)prpOcc);
				pomOccTable->DisplayTable();
			}
		}
		olLine.DeleteAll();
	}
	else
	{
		if(ipLineNo>-1)
		{
			pomOccTable->DeleteTextLine(ipLineNo);
			pomOccTable->DisplayTable();
		}
	}
}

//----------------------------------------------------------------------------------------

LONG ExtLMGatPosCheckinCounterDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *)lParam;
	if (polNotify == NULL)
		return 0;

	if (polNotify->SourceTable == this->pomOccTable)
	{
		if(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_OCCCHA") == '1')
		{
			int ilLineNo = pomOccTable->GetCurSel();
			if (ilLineNo == -1)
			{
				MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
			}
			else
			{
				OCCDATA *prlOcc = &omOccPtrA[ilLineNo];
				if (prlOcc != NULL)
				{
					OCCDATA rlOcc = *prlOcc;
					OccupationDlg *polDlg = new OccupationDlg(&rlOcc,this);
					m_CNAM.GetWindowText(polDlg->omName);
					if(polDlg->DoModal() == IDOK)
					{
						*prlOcc = rlOcc;
						if(prlOcc->IsChanged != DATA_NEW) prlOcc->IsChanged = DATA_CHANGED;
						ChangeOccTable(prlOcc, ilLineNo);
					}
					delete polDlg;
				}
			}
		}
	}
	else
		return GatPosCheckinCounterDlg::OnTableLButtonDblclk(wParam,lParam);

	return 0L;
}

//----------------------------------------------------------------------------------------

void ExtLMGatPosCheckinCounterDlg::OnOccNew() 
{
	OCCDATA rlOcc;

	if (ogPrivList.GetStat("CHECKINCOUNTERDLG.m_OCCNEW") == '1')
	{
		OccupationDlg *polDlg = new OccupationDlg(&rlOcc,this);
		m_CNAM.GetWindowText(polDlg->omName);
		if(polDlg->DoModal() == IDOK)
		{
			OCCDATA *prlOcc = new OCCDATA;
			*prlOcc = rlOcc;
			omOccPtrA.Add(prlOcc);
			prlOcc->IsChanged = DATA_NEW;
			ChangeOccTable(prlOcc, -1);
		}
		delete polDlg;
	}
}

//----------------------------------------------------------------------------------------

void ExtLMGatPosCheckinCounterDlg::OnOccDel() 
{
	int ilLineNo = pomOccTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
	}
	else
	{
		if(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_OCCDEL") == '1')
		{
			if (&omOccPtrA[ilLineNo] != NULL)
			{
				OCCDATA *prlOcc = new OCCDATA;
				*prlOcc = omOccPtrA[ilLineNo];
				prlOcc->IsChanged = DATA_DELETED;
				omDeleteOccPtrA.Add(prlOcc);
				omOccPtrA.DeleteAt(ilLineNo);
				ChangeOccTable(NULL, ilLineNo);
			}
		}
	}
}

//----------------------------------------------------------------------------------------
