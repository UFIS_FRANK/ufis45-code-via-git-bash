// CedaWROData.cpp
 
#include <stdafx.h>
#include <CedaWROData.h>
#include <resource.h>
#include <BasicData.h>

// Local function prototype
static void ProcessWROCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaWROData::CedaWROData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for WRODATA
	BEGIN_CEDARECINFO(WRODATA,WRODataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Wnam,"WNAM")
//		FIELD_DATE		(Nafr,"NAFR")
//		FIELD_DATE		(Nato,"NATO")
//		FIELD_CHAR_TRIM	(Resn,"RESN")
		FIELD_CHAR_TRIM	(Gte1,"GTE1")
		FIELD_CHAR_TRIM	(Gte2,"GTE2")
		FIELD_CHAR_TRIM	(Brca,"BRCA")
		FIELD_CHAR_TRIM	(Tele,"TELE")
		FIELD_CHAR_TRIM	(Term,"TERM")
		FIELD_CHAR_TRIM	(Home,"HOME")
		FIELD_CHAR_TRIM	(Wrot,"WROT")
		FIELD_CHAR_TRIM	(Defd,"DEFD")
		FIELD_CHAR_TRIM	(Maxf,"MAXF")
		FIELD_CHAR_TRIM	(Shgn,"SHGN")
		FIELD_LONG		(Ibit,"IBIT")
	END_CEDARECINFO //(WRODataAStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(WRODataRecInfo)/sizeof(WRODataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&WRODataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"WRO");
	strcpy(pcmWROFieldList,"CDAT,LSTU,PRFL,URNO,USEC,USEU,VAFR,VATO,WNAM,GTE1,GTE2,BRCA,TELE,TERM,HOME,WROT,DEFD,MAXF");
	pcmFieldList = pcmWROFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//-------------------------------------------------------------------------------------------------------

void CedaWROData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("BRCA");
	ropFields.Add("CDAT");
	ropFields.Add("GTE1");
	ropFields.Add("GTE2");
	ropFields.Add("LSTU");
	ropFields.Add("NAFR");
	ropFields.Add("NATO");
	ropFields.Add("PRFL");
	ropFields.Add("RESN");
	ropFields.Add("TELE");
	ropFields.Add("TERM");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");
	ropFields.Add("WNAM");
	ropFields.Add("HOME");
	ropFields.Add("WROT");
	ropFields.Add("DEFD");
	ropFields.Add("MAXF");
	if (ogBasicData.IsGatPosEnabled())
	{
		ropFields.Add("SHGN");
		ropFields.Add("IBIT");
	}
	
	ropDesription.Add(LoadStg(IDS_STRING467));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING457));
	ropDesription.Add(LoadStg(IDS_STRING458));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING364));
	ropDesription.Add(LoadStg(IDS_STRING365));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING366));
	ropDesription.Add(LoadStg(IDS_STRING294));
	ropDesription.Add(LoadStg(IDS_STRING296));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING468));
	ropDesription.Add(LoadStg(IDS_STRING711));
	ropDesription.Add(LoadStg(IDS_STRING1035));
	ropDesription.Add(LoadStg(IDS_STRING856));
	ropDesription.Add(LoadStg(IDS_STRING894));
	if (ogBasicData.IsGatPosEnabled())
	{
		ropDesription.Add(LoadStg(IDS_STRING895));
		ropDesription.Add(LoadStg(IDS_STRING909));
	}

	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	if (ogBasicData.IsGatPosEnabled())
	{
		ropType.Add("String");
		ropType.Add("String");
	}
}

//-------------------------------------------------------------------------------------------------------

void CedaWROData::Register(void)
{
	ogDdx.Register((void *)this,BC_WRO_CHANGE,CString("WRODATA"), CString("WRO-changed"),ProcessWROCf);
	ogDdx.Register((void *)this,BC_WRO_DELETE,CString("WRODATA"), CString("WRO-deleted"),ProcessWROCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaWROData::~CedaWROData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaWROData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
	omNameMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaWROData::Read(char *pcpWhere)
{
	if (ogBasicData.IsGatPosEnabled())
		strcpy(pcmWROFieldList,"CDAT,LSTU,PRFL,URNO,USEC,USEU,VAFR,VATO,WNAM,GTE1,GTE2,BRCA,TELE,TERM,HOME,WROT,DEFD,MAXF,SHGN,IBIT");
	pcmFieldList = pcmWROFieldList;

    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
	omNameMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		WRODATA *prpWRO = new WRODATA;
		if ((ilRc = GetFirstBufferRecord(prpWRO)) == true)
		{
			prpWRO->IsChanged = DATA_UNCHANGED;
			omData.Add(prpWRO);//Update omData
			omUrnoMap.SetAt((void *)prpWRO->Urno,prpWRO);
			omNameMap.SetAt(prpWRO->Wnam,prpWRO);
		}
		else
		{
			delete prpWRO;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaWROData::InsertWRO(WRODATA *prpWRO,BOOL bpSendDdx)
{
	prpWRO->IsChanged = DATA_NEW;
	if(SaveWRO(prpWRO) == false) return false; //Update Database
	InsertWROInternal(prpWRO);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaWROData::InsertWROInternal(WRODATA *prpWRO)
{
	//PrepareWROData(prpWRO);
	ogDdx.DataChanged((void *)this, WRO_CHANGE,(void *)prpWRO ); //Update Viewer
	omData.Add(prpWRO);//Update omData
	omUrnoMap.SetAt((void *)prpWRO->Urno,prpWRO);
	omNameMap.SetAt(prpWRO->Wnam,prpWRO);
	return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaWROData::DeleteWRO(long lpUrno)
{
	WRODATA *prlWRO = GetWROByUrno(lpUrno);
	if (prlWRO != NULL)
	{
		prlWRO->IsChanged = DATA_DELETED;
		if(SaveWRO(prlWRO) == false) return false; //Update Database
		DeleteWROInternal(prlWRO);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaWROData::DeleteWROInternal(WRODATA *prpWRO)
{
	ogDdx.DataChanged((void *)this,WRO_DELETE,(void *)prpWRO); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpWRO->Urno);
	omNameMap.RemoveKey(prpWRO->Wnam);
	int ilWROCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilWROCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpWRO->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaWROData::PrepareWROData(WRODATA *prpWRO)
{
	// TODO: add code here
}

//--UPDATE---------------------------------------------------------------------------------------------

bool CedaWROData::UpdateWRO(WRODATA *prpWRO,BOOL bpSendDdx)
{
	if (GetWROByUrno(prpWRO->Urno) != NULL)
	{
		if (prpWRO->IsChanged == DATA_UNCHANGED)
		{
			prpWRO->IsChanged = DATA_CHANGED;
		}
		if(SaveWRO(prpWRO) == false) return false; //Update Database
		UpdateWROInternal(prpWRO);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaWROData::UpdateWROInternal(WRODATA *prpWRO)
{
	WRODATA *prlWRO = GetWROByUrno(prpWRO->Urno);
	if (prlWRO != NULL)
	{
		*prlWRO = *prpWRO; //Update omData
		ogDdx.DataChanged((void *)this,WRO_CHANGE,(void *)prlWRO); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

WRODATA *CedaWROData::GetWROByUrno(long lpUrno)
{
	WRODATA  *prlWRO;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlWRO) == TRUE)
	{
		return prlWRO;
	}
	return NULL;
}

//--GET-BY-NAME--------------------------------------------------------------------------------------------

WRODATA *CedaWROData::GetWROByName(const CString& ropName)
{
	WRODATA  *prlWRO;
	if (omNameMap.Lookup(ropName,(void *& )prlWRO) == TRUE)
	{
		return prlWRO;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaWROData::ReadSpecialData(CCSPtrArray<WRODATA> *popWro,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","WRO",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","WRO",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popWro != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			WRODATA *prpWro = new WRODATA;
			if ((ilRc = GetBufferRecord(ilLc,prpWro,CString(pclFieldList))) == true)
			{
				popWro->Add(prpWro);
			}
			else
			{
				delete prpWro;
			}
		}
		if(popWro->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaWROData::SaveWRO(WRODATA *prpWRO)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpWRO->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpWRO->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpWRO);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpWRO->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWRO->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpWRO);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpWRO->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWRO->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessWROCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_WRO_CHANGE :
	case BC_WRO_DELETE :
		((CedaWROData *)popInstance)->ProcessWROBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaWROData::ProcessWROBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlWROData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlWROData->Selection);

	WRODATA *prlWRO = GetWROByUrno(llUrno);
	if(ipDDXType == BC_WRO_CHANGE)
	{
		if(prlWRO != NULL)
		{
			GetRecordFromItemList(prlWRO,prlWROData->Fields,prlWROData->Data);
			UpdateWROInternal(prlWRO);
		}
		else
		{
			prlWRO = new WRODATA;
			GetRecordFromItemList(prlWRO,prlWROData->Fields,prlWROData->Data);
			InsertWROInternal(prlWRO);
		}
	}
	if(ipDDXType == BC_WRO_DELETE)
	{
		if (prlWRO != NULL)
		{
			DeleteWROInternal(prlWRO);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
int	 CedaWROData::GetWROList(CStringArray& ropList,const CString& ropIgnoreName)
{
	for (int i = 0; i < omData.GetSize(); i++)
	{
		if (omData[i].Wnam != ropIgnoreName)
			ropList.Add(omData[i].Wnam);
	}

	return ropList.GetSize();
}

//---------------------------------------------------------------------------------------------------------
bool CedaWROData::MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct)
{
    pcpListOfData = "";

	CString olFieldData;

    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();
    for (int fieldno = 0; fieldno < nfields; fieldno++)
    {

        CString s;
        char buf[33];   // ltoa() need buffer 33 bytes
        int type = (*pomRecInfo)[fieldno].Type;
        void *p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[fieldno].Offset);
        int length = (*pomRecInfo)[fieldno].Length;

		if (strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == NULL)
			continue;

        switch (type) {
        case CEDACHAR:
            s = (length == 0)? CString(" "): CString((char *)p).Left(length);
			MakeCedaString(s);
            olFieldData = s + ",";
            break;
        case CEDAINT:
            olFieldData = CString(itoa(*(int *)p, buf, 10)) + ",";
            break;
        case CEDALONG:
            olFieldData = CString(ltoa(*(long *)p, buf, 10)) + ",";
            break;
        case CEDADATE:  // format: YYYYMMDDHHMMSS
			olFieldData = ((CTime *)p)->Format("%Y%m%d%H%M%S") + ",";
            break;
        case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
			if(((COleDateTime *)p)->GetStatus() == COleDateTime::valid)
			{
				olFieldData = ((COleDateTime *)p)->Format("%Y%m%d%H%M%S") + ",";
			}
			else
			{
				olFieldData = " ,"; 
			}
            break;
		case CEDADOUBLE:
			char pclBuf[128];
//MWO 18.12.98
			sprintf(pclBuf,"%lf",(double *)p);
//			sprintf(pclBuf,"%.2f",(double *)p);
            olFieldData = CString(pclBuf) + ",";
			break;
        default:    // invalid field type
            return false;
        }
		pcpListOfData += olFieldData;
    }

	if(pcpListOfData.GetLength() > 0)
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);

	//pcpListOfData.SetAt(pcpListOfData.GetLength()-1,' ');
	//pcpListOfData.TrimRight( );

    return true;
}
