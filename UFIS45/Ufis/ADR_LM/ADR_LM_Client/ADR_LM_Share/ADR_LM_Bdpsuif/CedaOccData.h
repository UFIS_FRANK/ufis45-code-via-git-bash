// CedaBlkData.h

#ifndef __CEDAOCCDATA__
#define __CEDAOCCDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct OCCDATA 
{
	long 	 Urno;
	CTime 	 Ocfr;
	CTime 	 Octo;
	char 	 Days[9];
	char 	 Rema[42];
	long	 Aloc;
	long 	 Alid;
	long	 Ocby;	
	char 	 Octa[4];
	char 	 Tifr[6];
	char 	 Tito[6];

	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	OCCDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Ocfr		=	TIMENULL;
		Octo		=	TIMENULL;
		Urno		=	0;
		Aloc		=	0;
		Alid		=	0;
		Ocby		=	0;
	}

}; // end BlkDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaOccData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<OCCDATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaOccData();
	~CedaOccData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(OCCDATA *prpBlk);
	bool InsertInternal(OCCDATA *prpOcc);
	bool Update(OCCDATA *prpBlk);
	bool UpdateInternal(OCCDATA *prpOcc);
	bool Delete(long lpUrno);
	bool DeleteInternal(OCCDATA *prpOcc);
	bool ReadSpecialData(CCSPtrArray<OCCDATA> *popOcc,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(OCCDATA *prpBlk);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	OCCDATA  *GetOccByUrno(long lpUrno);


	// Private methods
private:
    void PrepareOccData(OCCDATA *prpOccData);

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAOCCDATA__
