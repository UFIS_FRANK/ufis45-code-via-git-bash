// CedaWROData.h

#ifndef __CEDAWRODATA__
#define __CEDAWRODATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct WRODATA 
{
	CTime	 Cdat; 		// Erstellungsdatum
	CTime	 Lstu; 		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr;		// G�ltig von
	CTime	 Vato;		// G�ltig bis
	char 	 Wnam[7]; 	// Warteraum Name
//	CTime	 Nafr;		// Nicht verf�gbar von
//	CTime	 Nato;		// Nicht verf�gbar bis
//	char 	 Resn[42]; 	// Grund f�r die Sperrung
	char 	 Gte1[7]; 	// Verkn�pftes Gate 1
	char 	 Gte2[7]; 	// Verkn�pftes Gate 2
	char 	 Brca[7]; 	// Kapazit�t Warteraum
	char 	 Tele[12]; 	// Telefonnummer
	char 	 Term[3]; 	// Terminal
	char 	 Home[4]; 	// Terminal
	char 	 Wrot[2]; 	// Terminal
	char	 Defd[5];   // Default allocation duration
	char	 Maxf[3];	// maximum number of flights per waiting room
	char	 Shgn[2];	// Schengen (D) oder Non Schengen (N) oder Both (Empty)
	long	 Ibit;		// Index of bitmap for blocked times	
	
	//DataCreated by this class
	int      IsChanged;

	WRODATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
//		Nafr=-1;
//		Nato=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end WRODataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaWROData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr		omUrnoMap;
	CMapStringToPtr		omNameMap;

    CCSPtrArray<WRODATA>omData;

	char pcmWROFieldList[2048];

// Operations
public:
    CedaWROData();
	~CedaWROData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<WRODATA> *popWro,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertWRO(WRODATA *prpWRO,BOOL bpSendDdx = TRUE);
	bool InsertWROInternal(WRODATA *prpWRO);
	bool UpdateWRO(WRODATA *prpWRO,BOOL bpSendDdx = TRUE);
	bool UpdateWROInternal(WRODATA *prpWRO);
	bool DeleteWRO(long lpUrno);
	bool DeleteWROInternal(WRODATA *prpWRO);
	WRODATA  *GetWROByUrno(long lpUrno);
	WRODATA	 *GetWROByName(const CString& ropName);
	bool SaveWRO(WRODATA *prpWRO);
	void ProcessWROBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	int	 GetWROList(CStringArray& ropList,const CString& ropIgnoreName = "");

	// Private methods
private:
    void PrepareWROData(WRODATA *prpWROData);
    bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct);


};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAWRODATA__
