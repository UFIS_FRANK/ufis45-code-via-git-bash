#if !defined(AFX_STAMMDATEN_H__13692422_5C44_11D1_B3C9_0000C016B067__INCLUDED_)
#define AFX_STAMMDATEN_H__13692422_5C44_11D1_B3C9_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// Stammdaten.h : header file
//
#include <resource.h>
#include <CCSTable.h>
#include <BasicData.h>
#include <CedaBlkData.h>
#include <CedaEqaData.h>
#include <CedaDatData.h>
#include <CedaOccData.h>

#include <AirlineTableViewer.h>
#include <AircraftTableViewer.h>
#include <LFZRegiTableViewer.h>
#include <AirportTableViewer.h>
#include <RunwayTableViewer.h>
#include <TaxiwayTableViewer.h>
#include <PositionTableViewer.h>
#include <GateTableViewer.h>
#include <CheckinCounterTableViewer.h>
#include <BeggagebeltTableViewer.h>
#include <ExitTableViewer.h>
#include <DelaycodeTableViewer.h>
#include <TelexadressTableViewer.h>
#include <TraffictypeTableViewer.h>
#include <HandlingagentTableViewer.h>
#include <WaitingroomTableViewer.h>
#include <HandlingtypeTableViewer.h>
#include <ServicetypeTableViewer.h>
#include <FidsCommandTableViewer.h>
#include <ErgVerkehrsartenTableViewer.h>
#include <StandardverkettungenTableViewer.h>
#include <FlugplansaisonTableViewer.h>
#include <LeistungskatalogTableViewer.h>
#include <QualifikationenTableViewer.h>
#include <OrganisationseinheitenTableViewer.h>
#include <WegezeitenTableViewer.h>
#include <FunktionenTableViewer.h>
#include <ArbeitsvertragsartenTableViewer.h>
#include <BewertungsfaktorenTableViewer.h>
#include <BasisschichtenTableViewer.h>
#include <DiensteUndAbwesenheitenTableViewer.h>
#include <FahrgemeinschaftenTableViewer.h>
#include <MitarbeiterstammTableViewer.h>
#include <ErgAbfertigungsartenTableViewer.h>
#include <ReduktionenTableViewer.h>
#include <GeraetegruppenTableViewer.h>
#include <OrganizerTableViewer.h>
#include <TimeParametersTableViewer.h>
#include <HolidayTableViewer.h>
#include <ArbeitsgruppenTableViewer.h>
#include <PlanungsgruppenTableViewer.h>
#include <AwiTableViewer.h>
#include <CCCTableViewer.h>
#include <VipTableViewer.h>
#include <AircraftFamTableViewer.h>
#include <EngineTypeTableViewer.h>
#include <SrcTableViewer.h>
#include <ParTableViewer.h>
#include <PolTableViewer.h>
#include <MfmTableViewer.h>
#include <StsTableViewer.h>
#include <PmxTableViewer.h>
#include <WisTableViewer.h>
#include <CohTableViewer.h>
#include <NwhTableViewer.h>
#include <EquipmentTableViewer.h>
#include <EquipmentTypeTableViewer.h>
#include <AbsencePatternTableViewer.h>
#include <DeviceTableViewer.h>
#include <DisplayTableViewer.h>
#include <PageTableViewer.h>
#include <DlgResizeHelper.h>

enum							
{								//ON_COMMAND_RANGE auf 20000,20000+nMax setzen!!!!!!!!!
	SEPARATOR=20000,			// 0
	TAB_NO_TAB,					// 1
	TAB_HANDLINGAGENT,			// 2 Abfertigungsagenten
	TAB_HANDLINGTYPE,			// 3 Abfertigungsarten
	TAB_EXIT,					// 4 Ausg�nge
	TAB_CHECKINCOUNTER,			// 5 Check-In Schalter
	TAB_DELAYCODE,				// 6 Delaycodes
	TAB_ERGVERKEHRSARTEN,		// 7 Erg�nzung Verkehrsarten
	TAB_FIDSCOMMAND,			// 8 FIDS-Bemerkungen
	TAB_AIRLINE,				// 9 Fluggesellschaften
	TAB_AIRPORT,				//10 Flugh�fen
	TAB_FLUGPLANSAISON,			//11 Flugplansaison
	TAB_AIRCRAFT,				//12 Flugzeugtypen
	TAB_GATE,					//13 Gates
	TAB_BEGGAGEBELT,			//14 Gep�ckb�nder (Ankunft)
	TAB_LFZREGISTRATION,		//15 LFZ-Kennzeichen
	TAB_POSITION,				//16 Positionen
	TAB_SERVICETYPE,			//17 Servicetypen
	TAB_RUNWAY,					//19 Start- und Landebahnen
	TAB_TAXIWAY,				//20 Taxiways
	TAB_TELEXADRESS,			//21 Telexadressen
	TAB_TRAFFICTYPE,			//22 Verkehrsarten
	TAB_WAITINGROOM,			//23 Warter�ume
	TAB_LEISTUNGSKATALOG,		//24 Leistungskatalog
	TAB_QUALIFIKATIONEN,		//25 Qualifikationen
	TAB_ORGANISATIONSEINHEITEN,	//26 Organisationseinheiten
	TAB_WEGEZEITEN,				//27 Wegezeiten
	TAB_FUNKTIONEN,				//28 Funktionen
	TAB_ARBEITSVERTRAGSARTEN,	//29 Arbeitsvertragsarten
	TAB_BEWERTUNGSFAKTOREN,		//30 Bewertungsfaktoren,
	TAB_BASISSCHICHTEN,			//32 Dynamischeschichten,	
	TAB_DIENSTEUNDABWESENHEITEN,//33 Sonstigedienste,
	TAB_FAHRGEMEINSCHAFTEN,		//34 Fahrgemeinschaften,
	TAB_MITARBEITERSTAMM,		//35 Mitarbeiter,
	TAB_REDUKTIONEN,			//37 Reduktionsstufen	
	TAB_GERAETEGRUPPEN,			//38 Ger�tegruppen
	TAB_ORGANIZER,				//39 Veranstalter
	TAB_TIMEPARAMETERS,			//40 Zeit Parameter
	TAB_HOLIDAY,				//41 Zeit Parameter
	TAB_ARBEITSGRUPPEN,			//42 Zeit Parameter
	TAB_PLANNING_GROUPS,		//43 Planungsgruppen
	TAB_AIRPORTWET,			    //44 Flughafen Wetter Info
	TAB_COUNTERCLASS,			//45 Check-In Counter Klassen
	TAB_VERYIMPPERS,			//46 Very important persons
	TAB_AIRCRAFTFAM,			//47 
	TAB_ENGINETYPE,			    //48 
	TAB_PARAMETER,			    //49 PARAMETER
	TAB_MFM,					//50 Free days
	TAB_STS,					//51 Statusanhang
	TAB_NWH,					//52 Arbeitsstunden
	TAB_COH,					//53 Urlaubsanspruch
	TAB_PMX,					//54 Punkt Matrix
	TAB_WIS,					//56 W�nsche
	TAB_SRC,				    //57 
	TAB_POOL,					//58 Employee Pools
	TAB_EQUIPMENT,				//59
	TAB_EQUIPMENTTYPE,			//60
	TAB_ABSENCEPATTERN,			//61
	TAB_DEV,					//62 Device table for fids
	TAB_DSP,					//63 Display table for fids
	TAB_PAG						//64 Page table for fids 
	//Wenn man hier �ber 100 kommt Commandrange erh�hen in Stammdaten.cpp
	//MAP oder was das ist
};

enum							
{	
	SUBMENUE_GENERAL=100,
	SUBMENUE_RMS,
	SUBMENUE_POPS,
	SUBMENUE_FPMS,
	SUBMENUE_FIDS
};


struct TABLES
{
	UINT MenueID;
	UINT SubMenueID;
	CString InitModuName;
	CString TableName;
	CString TableCode;
	
	TABLES(void)
	{ 
		MenueID = 0;
		SubMenueID = 0;
		InitModuName = "";
		TableName = "";
		TableCode = "";
	}
};

/////////////////////////////////////////////////////////////////////////////
// CStammdaten dialog

class CStammdaten : public CDialog
{
// Construction
public:
	CStammdaten(CWnd* pomParent = NULL);   // standard constructor
	~CStammdaten();
// Dialog Data
	//{{AFX_DATA(CStammdaten)
	enum { IDD = IDD_STAMMDATEN };
	CComboBox	m_AnzeigeComboBox;
	CButton		m_ANSICHT;
	CButton		m_EINFUEGEN;
	CButton		m_AENDERN;
	CButton		m_LOESCHEN;
	CButton		m_KOPIEREN;
	CButton		m_ERSETZEN;
	CButton		m_DRUCKEN;
	CButton		m_EXCEL;
	CButton		m_FIND;
	CStatic	m_Status;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStammdaten)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CStammdaten)
	afx_msg void OnAendern();
	afx_msg void OnSelchangeAnzeige();
	afx_msg void OnDrucken();
	afx_msg void OnEinfuegen();
	afx_msg void OnErsetzen();
	afx_msg void OnKopieren();
	afx_msg void OnBeenden();
	afx_msg void OnLoeschen();
	afx_msg void OnHilfe();
	afx_msg void OnAllgemein();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnPaint();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnDestroy();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnExcel();
	afx_msg void OnFind();
    afx_msg LONG OnEvaluateCmdLine(UINT wParam, LONG lParam);
	afx_msg void OnTimer(UINT nIDEvent);

//	afx_msg void OnAendern();
//	afx_msg void OnSelchangeAnzeige();
//	afx_msg void OnDrucken();
	afx_msg void OnGruppen();
//	afx_msg void OnEinfuegen();
//	afx_msg void OnErsetzen();
//	afx_msg void OnKopieren();
//	afx_msg void OnBeenden();
//	afx_msg void OnLoeschen();
//	afx_msg void OnHilfe();
//	afx_msg void OnAllgemein();
//	virtual BOOL OnInitDialog();
//	virtual void OnCancel();
//	virtual void OnOK();
//	afx_msg void OnPaint();
//    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
//	afx_msg void OnDestroy();
//	afx_msg HCURSOR OnQueryDragIcon();
//	afx_msg void OnAendern();
//	afx_msg void OnSelchangeAnzeige();
//	afx_msg void OnDrucken();
//	afx_msg void OnEinfuegen();
//	afx_msg void OnErsetzen();
//	afx_msg void OnKopieren();
//	afx_msg void OnBeenden();
//	afx_msg void OnLoeschen();
//	afx_msg void OnHilfe();
//	afx_msg void OnAllgemein();
//	virtual BOOL OnInitDialog();
//	virtual void OnCancel();
//	virtual void OnOK();
//	afx_msg void OnPaint();
//    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
//	afx_msg void OnDestroy();
//	afx_msg HCURSOR OnQueryDragIcon();

	afx_msg LRESULT OnFindMsg(WPARAM, LPARAM);
	afx_msg void	OnSize(UINT nType,int cx,int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg void OnChangeMenu(UINT nID);

	CString omCurrentView;
	CViewer *pomCurrentViewer;
	HICON m_hIcon;
    CCSPtrArray<TABLES> omTables;
	CMenu	omRMSMenu;
	CMenu	omFPMSMenu;
	CMenu	omAllgemeinMenu;
	CMenu	omFIDSMenu;

public:
	void ShowAnsicht();
	void UpdateComboAnsicht();
	void ChangeViewer();
	void ClearData(int ilSelectTabOld);
	bool SetStatusText(UINT olTextID = ST_OK, CString olText = "NOTEXT");
	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 
	void OnInfo();
	void ChangeViewTo(char *pspViewName);
	bool UpdateBlkTable(CCSPtrArray<BLKDATA> *popBlkPtrA, CCSPtrArray<BLKDATA> *popDeleteBlkPtrA, CString opBurn, CString opTabn);
	bool UpdateEqaTable(CCSPtrArray<EQADATA> *popEqaPtrA, CCSPtrArray<EQADATA> *popDeleteBlkPtrA, CString opBurn);
	bool UpdateDatTable(CCSPtrArray<DATDATA> *popDatPtrA, CCSPtrArray<DATDATA> *popDeleteDatPtrA, CString opAlid, CString opAloc);
	bool UpdateOccTable(CCSPtrArray<OCCDATA> *popOccPtrA, CCSPtrArray<OCCDATA> *popDeleteOccPtrA, CString opAlid, CString opAloc);

public:

	CCSTable *pomTable;

	AirlineTableViewer					*pomAirlineViewer;
	AircraftTableViewer					*pomAircraftViewer;
	LFZRegiTableViewer					*pomLFZRegiViewer;
	AirportTableViewer					*pomAirportViewer;
	RunwayTableViewer					*pomRunwayViewer;
	TaxiwayTableViewer					*pomTaxiwayViewer;
	PositionTableViewer					*pomPositionViewer;
	GateTableViewer						*pomGateViewer;
	CheckinCounterTableViewer			*pomCheckinCounterViewer;
	BeggagebeltTableViewer				*pomBeggagebeltViewer;
	ExitTableViewer						*pomExitViewer;
	DelaycodeTableViewer				*pomDelaycodeViewer;
	TelexadressTableViewer				*pomTelexadressViewer;
	TraffictypeTableViewer				*pomTraffictypeViewer;
	HandlingagentTableViewer			*pomHandlingagentViewer;
	WaitingroomTableViewer				*pomWaitingroomViewer;
	HandlingtypeTableViewer				*pomHandlingtypeViewer;
	ServicetypeTableViewer				*pomServicetypeViewer;
	FidsCommandTableViewer				*pomFidsCommandViewer;
	ErgVerkehrsartenTableViewer			*pomErgVerkehrsartenViewer;
	FlugplansaisonTableViewer			*pomFlugplansaisonViewer;
	LeistungskatalogTableViewer			*pomLeistungskatalogViewer;
	QualifikationenTableViewer			*pomQualifikationenViewer;
	OrganisationseinheitenTableViewer	*pomOrganisationseinheitenViewer;
	FunktionenTableViewer				*pomFunktionenViewer;
	ArbeitsvertragsartenTableViewer		*pomArbeitsvertragsartenViewer;
	BewertungsfaktorenTableViewer		*pomBewertungsfaktorenViewer;
	BasisschichtenTableViewer			*pomBasisschichtenViewer;
	DiensteUndAbwesenheitenTableViewer	*pomDiensteUndAbwesenheitenViewer;
	FahrgemeinschaftenTableViewer		*pomFahrgemeinschaftenViewer;
	MitarbeiterstammTableViewer			*pomMitarbeiterstammViewer;
	ReduktionenTableViewer				*pomReduktionenViewer;
	GeraetegruppenTableViewer			*pomGeraetegruppenViewer;
	WegezeitenTableViewer				*pomWegezeitenViewer;
	OrganizerTableViewer				*pomOrganizerViewer;
	TimeParametersTableViewer			*pomTimeParametersViewer;
	HolidayTableViewer					*pomHolidayViewer;
	ArbeitsgruppenTableViewer			*pomArbeitsgruppenViewer;
	PlanungsgruppenTableViewer			*pomPlanungsgruppenViewer;
	VeryImpPersTableViewer				*pomVeryimppersViewer;
	CounterclassTableViewer				*pomCounterclassViewer;
	AirportwetTableViewer				*pomAirportwetViewer;
	AircraftFamTableViewer				*pomAircraftFamViewer;
	EngineTypeTableViewer				*pomEngineTypeViewer;
	SrcTableViewer						*pomSrcViewer;
	ParTableViewer						*pomParViewer;
	PolTableViewer						*pomPolViewer;
	MfmTableViewer						*pomMfmViewer;
	StsTableViewer						*pomStsViewer;
	NwhTableViewer						*pomNwhViewer;
	CohTableViewer						*pomCohViewer;
	PmxTableViewer						*pomPmxViewer;
	WisTableViewer						*pomWisViewer;
	EquipmentTableViewer				*pomEquipmentViewer;
	EquipmentTypeTableViewer			*pomEquipmentTypeViewer;
	AbsencePatternTableViewer			*pomAbsencePatternViewer;
	DeviceTableViewer					*pomDeviceTableViewer;
	DisplayTableViewer					*pomDisplayTableViewer;
	PageTableViewer						*pomPageTableViewer;

private:
	CString				omErrorTxt;
	CString				omReadErrTxt;
	CString				omInsertErrTxt;
	CString				omUpdateErrTxt;
	CString				omDeleteErrTxt;
	CString				omTabTxt;
	CString				omTableName;
	CMenu				omUIFMenu;
	int					imSelectTabOld;
	CFindReplaceDialog	*pomFindDlg;
	DlgResizeHelper		m_resizeHelper;
};

extern int igSelectTab;
extern bool bgIsDialogOpen;
//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STAMMDATEN_H__13692422_5C44_11D1_B3C9_0000C016B067__INCLUDED_)
