#if !defined(AFX_POSITIONDLG_H__56BF0B44_2049_11D1_B38A_0000C016B067__INCLUDED_)
#define AFX_POSITIONDLG_H__56BF0B44_2049_11D1_B38A_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// PositionDlg.h : header file
//
#include <CedaPSTData.h>
#include <CCSEdit.h>
#include <CCSTable.h>
#include <CedaBlkData.h>
#include <CedaDatData.h>

/////////////////////////////////////////////////////////////////////////////
// PositionDlg dialog

class PositionDlg : public CDialog
{
// Construction
public:
	PositionDlg(PSTDATA *popPST,CWnd* pParent = NULL);   // standard constructor
	virtual	~PositionDlg();

	CCSPtrArray<BLKDATA> omBlkPtrA;
	CCSPtrArray<BLKDATA> omDeleteBlkPtrA;
	CCSPtrArray<DATDATA> omDatPtrA;
	CCSPtrArray<DATDATA> omDeleteDatPtrA;

// Dialog Data
	//{{AFX_DATA(PositionDlg)
	enum { IDD = IDD_POSITIONDLG };
	CCSEdit	m_DEFD;
	CButton	m_OK;
	CButton	m_NOAVNEW;
	CButton	m_NOAVDEL;
	CStatic	m_NOAVFRAME;
	CButton	m_BRGS;
	CButton	m_DGSS;
	CButton	m_PUBK;
	CButton	m_PCAS;
	CButton	m_GPUS;
	CButton	m_FULS;
	CButton	m_ACUS;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_ATYP;
	CCSEdit	m_GRUP;
	CCSEdit	m_NPOS;
	CCSEdit	m_PNAM;
	CCSEdit	m_TAXI;
	CCSEdit	m_TELE;
	CCSEdit	m_HOME;
	CCSEdit	m_MXAC;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PositionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PositionDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnNoavDel();
	afx_msg void OnNoavNew();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:

	PSTDATA *pomPST;
	CCSTable	*pomTable;

	void MakeNoavTable(CString opTabn, CString opBurn);
	void ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo);
	PositionDlg(PSTDATA *popPST,int ipDlg,CWnd* pParent = NULL);   // standard constructor

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_POSITIONDLG_H__56BF0B44_2049_11D1_B38A_0000C016B067__INCLUDED_)
