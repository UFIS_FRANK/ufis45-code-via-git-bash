#if !defined(AFX_WAITINGROOMDLG_H__56BF0B41_2049_11D1_B38A_0000C016B067__INCLUDED_)
#define AFX_WAITINGROOMDLG_H__56BF0B41_2049_11D1_B38A_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// WaitingroomDlg.h : header file
//
#include <CedaWROData.h>
#include <CCSEdit.h>
#include <CCSTable.h>
#include <CedaBlkData.h>
#include <CedaDatData.h>
/////////////////////////////////////////////////////////////////////////////
// WaitingroomDlg dialog

class WaitingroomDlg : public CDialog
{
// Construction
public:
	WaitingroomDlg(WRODATA *popWRO,CWnd* pParent = NULL);   // standard constructor
	WaitingroomDlg(WRODATA *popWRO,int ipDlg,CWnd* pParent = NULL);   // standard constructor
	~WaitingroomDlg();

	CCSPtrArray<BLKDATA> omBlkPtrA;
	CCSPtrArray<BLKDATA> omDeleteBlkPtrA;
	CCSPtrArray<DATDATA> omDatPtrA;
	CCSPtrArray<DATDATA> omDeleteDatPtrA;

// Dialog Data
	//{{AFX_DATA(WaitingroomDlg)
	enum { IDD = IDD_WAITINGROOMDLG };
	CCSEdit	m_DEFD;
	CButton	m_OK;
	CButton	m_NOAVNEW;
	CButton	m_NOAVDEL;
	CStatic	m_NOAVFRAME;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_WNAM;
	CCSEdit	m_GRUP;
	CCSEdit	m_BRCA;
	CCSEdit	m_GTE1;
	CCSEdit	m_GTE2;
	CCSEdit	m_TELE;
	CCSEdit	m_TERM;
	CCSEdit	m_HOME;
	CCSEdit	m_WROT;
	CCSEdit	m_MAXF;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(WaitingroomDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(WaitingroomDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnNoavDel();
	afx_msg void OnNoavNew();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:

	WRODATA *pomWRO;
	CCSTable	*pomTable;

	void MakeNoavTable(CString opTabn, CString opBurn);
	void ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WAITINGROOMDLG_H__56BF0B41_2049_11D1_B38A_0000C016B067__INCLUDED_)
