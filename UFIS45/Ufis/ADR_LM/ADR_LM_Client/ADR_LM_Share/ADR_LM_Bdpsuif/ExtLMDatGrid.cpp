// DatGrid.cpp : implementation file
//

#include <stdafx.h>
#include <ExtLMDatGrid.h>
#include <ccsglobl.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <resrc1.h>
#include <bdpsuif.h>
#include <CedaGrnData.h>
#include <CedaAloData.h>
#include <CedaAltData.h>
#include <CedaActData.h>
#include <CedaNatData.h>
#include <AWDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ExtLMDatGrid

ExtLMDatGrid::ExtLMDatGrid()
{
	this->lmAlid = -1;
	this->lmAloc = -1;
}

ExtLMDatGrid::~ExtLMDatGrid()
{

}


BEGIN_MESSAGE_MAP(ExtLMDatGrid, CDatGrid)
	//{{AFX_MSG_MAP(ExtLMDatGrid)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


bool ExtLMDatGrid::Initialize(const CString& ropAloc,long lpAlid,bool bpCopy,CCSPtrArray<DATDATA>& ropDatPtr,bool bpReadOnly)
{
	this->CGXGridWnd::Initialize();
	this->SetColCount(8);

	this->GetParam()->EnableTrackRowHeight(FALSE);		//disable rowsizing
	this->GetParam()->EnableMoveRows(FALSE);			//disable rowmoving
	this->GetParam()->EnableSelection(GX_SELROW);

	//*** HIDE URNO AND BSDU ***
	this->SetColWidth(0,0,20);	// Row number
	this->SetColWidth(1,1,0);	// URNO
	this->SetColWidth(2,2,110);	// Airline group
	this->SetColWidth(3,3,110);	// A/C group
	this->SetColWidth(4,4,100);	// Nature group
	this->SetColWidth(5,5,60);	// Minutes
	this->SetColWidth(6,6,60);	// Arrival
	this->SetColWidth(7,7,60);	// Departure
	this->SetColWidth(8,8,0);	// Status (NEW,MODIFIED,DELETED)

	this->HideCols(1,1);	// URNO
	this->HideCols(8,8);	// STATUS

	this->SetValueRange(CGXRange(0,1), GetListItem(LoadStg(IDS_STRING1092),1,true,'|'));	// URNO
	this->SetValueRange(CGXRange(0,2), GetListItem(LoadStg(IDS_STRING1092),2,true,'|'));	// Airline group	
	this->SetValueRange(CGXRange(0,3), GetListItem(LoadStg(IDS_STRING1092),3,true,'|'));	// A/C group	
	this->SetValueRange(CGXRange(0,4), GetListItem(LoadStg(IDS_STRING1092),4,true,'|'));	// Nature group
	this->SetValueRange(CGXRange(0,5), GetListItem(LoadStg(IDS_STRING1092),5,true,'|'));	// Minutes
	this->SetValueRange(CGXRange(0,6), GetListItem(LoadStg(IDS_STRING1092),6,true,'|'));	// Arrival
	this->SetValueRange(CGXRange(0,7), GetListItem(LoadStg(IDS_STRING1092),7,true,'|'));	// Departure

	this->omAloc = ropAloc;
	this->lmAlid = lpAlid;
	this->bmCopy = bpCopy;

	this->Fill(ropDatPtr,bpReadOnly);


	return true;
}


void ExtLMDatGrid::Fill(CCSPtrArray<DATDATA>& ropDatPtr,bool bpReadOnly)
{
	int ilDataCount;
	CString olWhere;

	long llUrno = ogAloData.GetAloUrnoByName(this->omAloc);
	if (llUrno == 0)
		return;
	else
	{
		this->lmAloc = llUrno;
	}

	olWhere.Format("WHERE ALOC = '%ld' AND ALID = '%ld'",llUrno,this->lmAlid);
	ogDatData.ReadSpecialData(&ropDatPtr,olWhere.GetBuffer(0),"",false);

	// get number of rows
	ilDataCount = ropDatPtr.GetSize();
	int	iDatCount	= ilDataCount;
	this->SetRowCount(ilDataCount);

	// fill the grid with the already existing data
	for(int i = 0; i < ilDataCount; i++)
	{
		if (bmCopy == 1)
		{
			this->SetValueRange(CGXRange(i+1,1),"0");
		}
		else
		{
			this->SetValueRange(CGXRange(i+1,1),ropDatPtr[i].Urno);
		}

		GRNDATA *prlGrn = ogGrnData.GetGrnByUrno(ropDatPtr[i].Algr);
		if (prlGrn == NULL)
			this->SetValueRange(CGXRange(i+1,2),"");
		else
			this->SetValueRange(CGXRange(i+1,2),prlGrn->Grpn);

		prlGrn = ogGrnData.GetGrnByUrno(ropDatPtr[i].Acgr);
		if (prlGrn == NULL)
			this->SetValueRange(CGXRange(i+1,3),"");
		else
			this->SetValueRange(CGXRange(i+1,3),prlGrn->Grpn);

		prlGrn = ogGrnData.GetGrnByUrno(ropDatPtr[i].Nagr);
		if (prlGrn == NULL)
			this->SetValueRange(CGXRange(i+1,4),"");
		else
			this->SetValueRange(CGXRange(i+1,4),prlGrn->Grpn);

		this->SetValueRange(CGXRange(i+1,5),ropDatPtr[i].Dura);

		if (ropDatPtr[i].Flty[0] == '1')
			this->SetValueRange(CGXRange(i+1,6),"1");
		else
			this->SetValueRange(CGXRange(i+1,6),"0");

		if (ropDatPtr[i].Flty[1] == '1')
			this->SetValueRange(CGXRange(i+1,7),"1");
		else
			this->SetValueRange(CGXRange(i+1,7),"0");

		if (bmCopy == 1)
		{
			ropDatPtr[i].IsChanged = DATA_NEW;
			this->SetValueRange(CGXRange(i+1,8),"NEW");
		}
		else
		{
			this->SetValueRange(CGXRange(i+1,8),"OK");
		}
	}

	omAllALT	= ogGrnData.GetSortedGrpnList("ALTTAB","\n");

	omAllACT	= ogGrnData.GetSortedGrpnList("ACTTAB","\n");

	omAllNAT	= ogGrnData.GetSortedGrpnList("NATTAB","\n");

	// set cell styles

	if (iDatCount > 0)
	{
		// airline group
		this->SetStyleRange(CGXRange(1,2,iDatCount,2),
				CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetControl(GX_IDS_CTRL_COMBOBOX) 
				.SetChoiceList(omAllALT)); 

		// A/C group
		this->SetStyleRange(CGXRange(1,3,iDatCount,3),
				CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetControl(GX_IDS_CTRL_COMBOBOX) 
				.SetChoiceList(omAllACT)); 

		// Nature group
		this->SetStyleRange(CGXRange(1,4,iDatCount,4),
				CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetControl(GX_IDS_CTRL_COMBOBOX) 
				.SetChoiceList(omAllNAT)); 

		// Minutes
		this->SetStyleRange(CGXRange(1,5,iDatCount,5),
			  CGXStyle()
			  .SetControl(GX_IDS_CTRL_MASKEDIT)
			  .SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

		// Arrival
		this->SetStyleRange(CGXRange(1,6,iDatCount,6),
			  CGXStyle()
			  .SetVerticalAlignment(DT_VCENTER)
			  .SetControl(GX_IDS_CTRL_CHECKBOX));

		// Departure
		this->SetStyleRange(CGXRange(1,7,iDatCount,7),
			  CGXStyle()
			  .SetVerticalAlignment(DT_VCENTER)
			  .SetControl(GX_IDS_CTRL_CHECKBOX));
	}

	if (bpReadOnly)
		this->SetReadOnly(true);
}

//---------------------------------------------------------------------------
void ExtLMDatGrid::Delete(CCSPtrArray<DATDATA>& ropDatPtr,CCSPtrArray<DATDATA>& ropDeleteDatPtr) 
{
	ROWCOL iCol,iRow;
	this->GetCurrentCell(iRow,iCol);

	if (iRow > 0)
	{
		if (&ropDatPtr[iRow - 1] != NULL)
		{
			DATDATA *prlDat = new DATDATA;
			*prlDat = ropDatPtr[iRow - 1];
			prlDat->IsChanged = DATA_DELETED;
			ropDeleteDatPtr.Add(prlDat);
			ropDatPtr.DeleteAt(iRow - 1);

			this->RemoveRows(iRow,iRow);

			this->SetCurrentCell(0,0);
		}
	}
			
}

//---------------------------------------------------------------------------
void ExtLMDatGrid::Insert(CCSPtrArray<DATDATA>& ropDatPtr) 
{
	long iRows = this->GetRowCount()+1;
	this->InsertRows(iRows,1);

	DATDATA *prlDat = new DATDATA;
	prlDat->Urno	= 0;
	prlDat->IsChanged = DATA_NEW;

	ropDatPtr.Add(prlDat);

	this->SetValueRange(CGXRange(iRows,1),prlDat->Urno);

	GRNDATA *prlGrn = ogGrnData.GetGrnByUrno(prlDat->Algr);
	if (prlGrn == NULL)
		this->SetValueRange(CGXRange(iRows,2),"");
	else
		this->SetValueRange(CGXRange(iRows,2),prlGrn->Grpn);

	prlGrn = ogGrnData.GetGrnByUrno(prlDat->Acgr);
	if (prlGrn == NULL)
		this->SetValueRange(CGXRange(iRows,3),"");
	else
		this->SetValueRange(CGXRange(iRows,3),prlGrn->Grpn);

	prlGrn = ogGrnData.GetGrnByUrno(prlDat->Nagr);
	if (prlGrn == NULL)
		this->SetValueRange(CGXRange(iRows,4),"");
	else
		this->SetValueRange(CGXRange(iRows,4),prlGrn->Grpn);

	this->SetValueRange(CGXRange(iRows,5),prlDat->Dura);

	if (prlDat->Flty[0] == '1')
		this->SetValueRange(CGXRange(iRows,6),"1");
	else
		this->SetValueRange(CGXRange(iRows,6),"0");

	if (prlDat->Flty[1] == '1')
		this->SetValueRange(CGXRange(iRows,7),"1");
	else
		this->SetValueRange(CGXRange(iRows,7),"0");

	this->SetValueRange(CGXRange(iRows,8),"NEW");


	this->SetStyleRange(CGXRange(1,2,iRows,2),
			CGXStyle()
			.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_COMBOBOX) 
			.SetChoiceList(omAllALT)); 

	this->SetStyleRange(CGXRange(1,3,iRows,3),
			CGXStyle()
			.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_COMBOBOX) 
			.SetChoiceList(omAllACT)); 

	this->SetStyleRange(CGXRange(1,4,iRows,4),
			CGXStyle()
			.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_COMBOBOX) 
			.SetChoiceList(omAllNAT)); 


	this->SetStyleRange(CGXRange(1,5,iRows,5),
			CGXStyle()
			.SetControl(GX_IDS_CTRL_MASKEDIT)
			.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

	this->SetStyleRange(CGXRange(1,6,iRows,6),
			CGXStyle()
    		.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_CHECKBOX));

	this->SetStyleRange(CGXRange(1,7,iRows,7),
			CGXStyle()
    		.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_CHECKBOX));

	this->SetCurrentCell(iRows,2);
}

//---------------------------------------------------------------------------
void ExtLMDatGrid::Copy(CCSPtrArray<DATDATA>& ropDatPtr) 
{
	ROWCOL iCol,iRow;
	this->GetCurrentCell(iRow,iCol);

	if (iRow > 0)
	{
		if (&ropDatPtr[iRow - 1] != NULL)
		{
			DATDATA *prlDat = new DATDATA;
			*prlDat = ropDatPtr[iRow - 1];
			prlDat->IsChanged = DATA_NEW;
			ropDatPtr.Add(prlDat);

			long iRows = this->GetRowCount()+1;
			this->InsertRows(iRows,1);

			if (this->CopyCells(CGXRange(iRow,1,iRow,8),iRows,1))
			{
				this->SetValueRange(CGXRange(iRows,1),"0");
				this->SetValueRange(CGXRange(iRows,8),"NEW");
			}
			
			this->SetStyleRange(CGXRange(1,2,iRows,2),
					CGXStyle()
					.SetVerticalAlignment(DT_VCENTER)
					.SetControl(GX_IDS_CTRL_COMBOBOX) 
					.SetChoiceList(omAllALT)); 

			this->SetStyleRange(CGXRange(1,3,iRows,3),
					CGXStyle()
					.SetVerticalAlignment(DT_VCENTER)
					.SetControl(GX_IDS_CTRL_COMBOBOX) 
					.SetChoiceList(omAllACT)); 

			this->SetStyleRange(CGXRange(1,4,iRows,4),
					CGXStyle()
					.SetVerticalAlignment(DT_VCENTER)
					.SetControl(GX_IDS_CTRL_COMBOBOX) 
					.SetChoiceList(omAllNAT)); 


			this->SetStyleRange(CGXRange(1,5,iRows,5),
					CGXStyle()
					.SetControl(GX_IDS_CTRL_MASKEDIT)
					.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

			this->SetStyleRange(CGXRange(1,6,iRows,6),
					CGXStyle()
		    		.SetVerticalAlignment(DT_VCENTER)
					.SetControl(GX_IDS_CTRL_CHECKBOX));

			this->SetStyleRange(CGXRange(1,7,iRows,7),
					CGXStyle()
		    		.SetVerticalAlignment(DT_VCENTER)
					.SetControl(GX_IDS_CTRL_CHECKBOX));

			this->SetCurrentCell(iRows,2);
		}
	}
			
}


//---------------------------------------------------------------------------
void ExtLMDatGrid::Update(CCSPtrArray<DATDATA>& ropDatPtr)
{
	long iRows = this->GetRowCount()+1;
	this->InsertRows(iRows,1);

	this->SetValueRange(CGXRange(iRows,1),ogBasicData.GetNextUrno());
	this->SetValueRange(CGXRange(iRows,8),"NEW");


	this->SetStyleRange(CGXRange(1,2,iRows,2),
			CGXStyle()
			.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_COMBOBOX) 
			.SetChoiceList(omAllALT)); 

	this->SetStyleRange(CGXRange(1,3,iRows,3),
			CGXStyle()
			.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_COMBOBOX) 
			.SetChoiceList(omAllACT)); 

	this->SetStyleRange(CGXRange(1,4,iRows,4),
			CGXStyle()
			.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_COMBOBOX) 
			.SetChoiceList(omAllNAT)); 


	this->SetStyleRange(CGXRange(1,5,iRows,5),
			CGXStyle()
			.SetControl(GX_IDS_CTRL_MASKEDIT)
			.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("####")));

	this->SetStyleRange(CGXRange(1,6,iRows,6),
			CGXStyle()
    		.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_CHECKBOX));

	this->SetStyleRange(CGXRange(1,7,iRows,7),
			CGXStyle()
    		.SetVerticalAlignment(DT_VCENTER)
			.SetControl(GX_IDS_CTRL_CHECKBOX));

	this->SetCurrentCell(iRows,2);
}

//---------------------------------------------------------------------------
bool ExtLMDatGrid::FinalCheck(CCSPtrArray<DATDATA>& ropDatPtr,CCSPtrArray<DATDATA>& ropDeleteDatPtr,CString& ropErrorText)
{
	bool blStatus = true;
	CString olLine;
	CString olStatus; 
	for (int i = 1; i <= this->GetRowCount(); i++)
	{
		olLine.Format("%d ",i);

		olStatus = GetValueRowCol(i,8);
		if (olStatus == "NEW" || olStatus == "UPD")
		{
//			perform logical checks

			CString olGrpn = this->GetValueRowCol(i,2);
			if (!olGrpn.IsEmpty())
			{
				if (this->omAllALT.Find(olGrpn+"\n") < 0 && this->omAllALT.Find(olGrpn) + olGrpn.GetLength() != this->omAllALT.GetLength())
				{
					blStatus = false;
					ropErrorText += LoadStg(IDS_STRING85) + olLine + LoadStg(IDS_STRING28);	// invalid group code
				
				}
			}

			olGrpn = this->GetValueRowCol(i,3);
			if (!olGrpn.IsEmpty())
			{
				if (this->omAllACT.Find(olGrpn+"\n") < 0 && this->omAllACT.Find(olGrpn) + olGrpn.GetLength() != this->omAllACT.GetLength())
				{
					blStatus = false;
					ropErrorText += LoadStg(IDS_STRING85) + olLine + LoadStg(IDS_STRING29);	// invalid group code
				
				}
			}

			olGrpn = this->GetValueRowCol(i,4);
			if (!olGrpn.IsEmpty())
			{
				if (this->omAllNAT.Find(olGrpn+"\n") < 0 && this->omAllNAT.Find(olGrpn) + olGrpn.GetLength() != this->omAllNAT.GetLength())
				{
					blStatus = false;
					ropErrorText += LoadStg(IDS_STRING85) + olLine + LoadStg(IDS_STRING1094);	// invalid group code
				
				}
			}

			if (blStatus == true)
			{
				CString olGrpn = this->GetValueRowCol(i,2);
				GRNDATA *prlGrn = ogGrnData.GetGrnByGrpn("ALTTAB",olGrpn);				
				if (prlGrn == NULL)
					ropDatPtr[i-1].Algr = 0;
				else
					ropDatPtr[i-1].Algr = prlGrn->Urno;

				olGrpn = this->GetValueRowCol(i,3);
				prlGrn = ogGrnData.GetGrnByGrpn("ACTTAB",olGrpn);				
				if (prlGrn == NULL)
					ropDatPtr[i-1].Acgr = 0;
				else
					ropDatPtr[i-1].Acgr = prlGrn->Urno;

				olGrpn = this->GetValueRowCol(i,4);
				prlGrn = ogGrnData.GetGrnByGrpn("NATTAB",olGrpn);				
				if (prlGrn == NULL)
					ropDatPtr[i-1].Nagr = 0;
				else
					ropDatPtr[i-1].Nagr = prlGrn->Urno;


				ropDatPtr[i-1].Dura = atol(GetValueRowCol(i,5));

				CString olDuumy = GetValueRowCol(i,6);
				if (GetValueRowCol(i,6) == '1')
					ropDatPtr[i-1].Flty[0] = '1';
				else
					ropDatPtr[i-1].Flty[0] = '0';

				if (GetValueRowCol(i,7) == '1')
					ropDatPtr[i-1].Flty[1] = '1';
				else
					ropDatPtr[i-1].Flty[1] = '0';


				if (ropDatPtr[i-1].IsChanged == DATA_NEW)
				{
					ropDatPtr[i-1].Aloc = this->lmAloc;
					ropDatPtr[i-1].Alid = this->lmAlid;
					ropDatPtr[i-1].Cdat = CTime::GetCurrentTime();
					ropDatPtr[i-1].Cdat = CTime::GetCurrentTime();
					strcpy(ropDatPtr[i-1].Usec,cgUserName);
				}
				else if (ropDatPtr[i-1].IsChanged == DATA_UNCHANGED)
				{
					ropDatPtr[i-1].IsChanged = DATA_CHANGED;
				}
			}
		}
	}

	return blStatus;
}
/////////////////////////////////////////////////////////////////////////////
// ExtLMDatGrid message handlers
BOOL ExtLMDatGrid::SubClassDlgItem(UINT nID, CWnd *pParent)
{
	CWnd::SubclassDlgItem(nID,pParent);
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
BOOL ExtLMDatGrid::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)	
{
	//--- Check f�r StingRay Fehler
	if (nCol > GetColCount())
		return FALSE;


	//--- Sortieren je nachdem welcher Header gedr�ckt wurde
	if (nRow == 0 && nCol != 0)
	{
		CGXSortInfoArray  sortInfo;
		sortInfo .SetSize(1);
		
		// switch between sorting in ascending / descending order with each click
		if (bmSortAscend == TRUE)
		{
			sortInfo[0].sortOrder = CGXSortInfo::descending;
			bmSortAscend = FALSE;
		}
		else
		{
			sortInfo[0].sortOrder = CGXSortInfo::ascending;
			bmSortAscend = TRUE;
		}
		
		sortInfo[0].nRC = nCol;                       
		sortInfo[0].sortType = CGXSortInfo::autodetect;  
		SortRows( CGXRange().SetTable(), sortInfo); 

		//--- Merke welche Spalte f�r sorting verwendet wurde

		//--- Check ob numerisch sortiert wurde 
		CGXStyle  il_Cell_Style;
		GetStyleRowCol(1, nCol, il_Cell_Style);


		if(il_Cell_Style.GetValueType() ==  GX_VT_NUMERIC )
		  bmSortNumerical = TRUE;
		else
		  bmSortNumerical = FALSE;
	}
	return FALSE;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BOOL ExtLMDatGrid::OnGridKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{

	if (nChar==9)
		return FALSE;
	else
	{
		CGXGridCore::OnGridKeyDown(nChar,nRepCnt,nFlags);
		return TRUE;
	}
}

BOOL ExtLMDatGrid::OnDeleteCell()
{
	return true;
}

BOOL ExtLMDatGrid::OnEndEditing(ROWCOL nRow, ROWCOL nCol)
{
	if (nCol == 2)
	{
		CString olValue = this->GetValueRowCol(nRow,nCol);
		olValue.MakeUpper();
		this->SetValueRange(CGXRange(nRow,nCol),olValue);
	}

	SetValueRange(CGXRange(nRow,8),"UPD");
	return true;
}

void ExtLMDatGrid::OnModifyCell (ROWCOL nRow, ROWCOL nCol)
{
#if	0
	if (nCol == 2)
	{
		CGXComboBox *polCtrl = (CGXComboBox *)this->GetControl(nRow,nCol);
		if (polCtrl != NULL)
		{
			CString olValue;
			if (polCtrl->GetValue(olValue))
			{
				olValue.MakeUpper();
				polCtrl->SetValue(olValue);
			}
		}
	}
#endif
}

//---------------------------------------------------------------------------
void ExtLMDatGrid::Assign(CCSPtrArray<DATDATA>& ropDatPtr,CCSPtrArray<DATDATA>& ropDeleteDatPtr,CStringArray& olCol1,CStringArray& olCol3,CString& olMsg3) 
{
	ROWCOL iCol,iRow;
	this->GetCurrentCell(iRow,iCol);

	if (iRow > 0 && &ropDatPtr[iRow - 1] != NULL)
	{
		CString olStatus = GetValueRowCol(iRow,8);
		if (olStatus == "NEW" || olStatus == "UPD")	// must perform a final check before assign
		{
			CString olErrorMsg;
			if (!this->FinalCheck(ropDatPtr,ropDeleteDatPtr,olErrorMsg))
			{
				MessageBox(olErrorMsg,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
				return;
			}
		}

		AwDlg olDlg(NULL,&olCol1,&olCol3,"",olMsg3,this);
		olDlg.bmMultipleSelection = true;
		if (olDlg.DoModal() == IDOK)
		{
			for (int i = 0; i < olDlg.omReturnUrnos.GetSize(); i++)
			{
				DATDATA *prlDat = new DATDATA;
				*prlDat = ropDatPtr[iRow - 1];
				prlDat->IsChanged = DATA_NEW;
				prlDat->Urno = ogBasicData.GetNextUrno();	//new URNO
				prlDat->Alid = olDlg.omReturnUrnos[i];
				prlDat->Cdat = CTime::GetCurrentTime();
				prlDat->Cdat = CTime::GetCurrentTime();
				strcpy(prlDat->Usec,cgUserName);

				ogDatData.Insert(prlDat);
			}
		}
	}
			
}
