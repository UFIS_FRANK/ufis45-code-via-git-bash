// CedaDatData.h

#ifndef __CEDADATDATA__
#define __CEDADATDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct DATDATA 
{
	long 	 Urno;
	long 	 Aloc;		// Urno of ALOTAB record
	long 	 Alid;		//		
	char 	 Alc3[4];
	long 	 Algr;
	long 	 Dura;
	long 	 Pafr;
	long 	 Pato;
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	long	 Acgr;
	long	 Nagr;
	char	 Flty[4];

	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	DATDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Pafr		= -1;
		Pato		= -1;
		IsChanged	= DATA_UNCHANGED;
		Cdat		= -1;
	}

}; // end DatDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaDatData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<DATDATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaDatData();
	~CedaDatData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(DATDATA *prpDat);
	bool InsertInternal(DATDATA *prpDat);
	bool Update(DATDATA *prpDat);
	bool UpdateInternal(DATDATA *prpDat);
	bool Delete(long lpUrno);
	bool DeleteInternal(DATDATA *prpDat);
	bool ReadSpecialData(CCSPtrArray<DATDATA> *popDat,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(DATDATA *prpDat);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	DATDATA  *GetDatByUrno(long lpUrno);


	// Private methods
private:
    void PrepareDatData(DATDATA *prpDatData);

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDADATDATA__
