// OccupationDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <OccupationDlg.h>
#include <CedaHAGData.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld OccupationDlg 
//----------------------------------------------------------------------------------------


OccupationDlg::OccupationDlg(OCCDATA *popOcc,CWnd* pParent /*=NULL*/) 
: CDialog(OccupationDlg::IDD, pParent)
{
	pomOcc = popOcc;
	imLastSelection = -1;

	//{{AFX_DATA_INIT(OccupationDlg)
	//}}AFX_DATA_INIT
}


OccupationDlg::OccupationDlg(OCCDATA *popOcc,int ipDlg,CWnd* pParent /*=NULL*/) 
: CDialog(ipDlg, pParent)
{
	pomOcc = popOcc;
	imLastSelection = -1;
}


void OccupationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(OccupationDlg)
	DDX_Control(pDX, IDC_COMBO_HANDLINGAGENTS, m_HandlingAgents);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_DAYS_T, m_DAYS_T);
	DDX_Control(pDX, IDC_DAYS_7, m_DAYS_7);
	DDX_Control(pDX, IDC_DAYS_6, m_DAYS_6);
	DDX_Control(pDX, IDC_DAYS_5, m_DAYS_5);
	DDX_Control(pDX, IDC_DAYS_4, m_DAYS_4);
	DDX_Control(pDX, IDC_DAYS_2, m_DAYS_2);
	DDX_Control(pDX, IDC_DAYS_3, m_DAYS_3);
	DDX_Control(pDX, IDC_DAYS_1, m_DAYS_1);
	DDX_Control(pDX, IDC_NAME,	 m_NAME);
	DDX_Control(pDX, IDC_NAFR_D, m_NAFRD);
	DDX_Control(pDX, IDC_NAFR_T, m_NAFRT);
	DDX_Control(pDX, IDC_NATO_D, m_NATOD);
	DDX_Control(pDX, IDC_NATO_T, m_NATOT);
	DDX_Control(pDX, IDC_RESN,	 m_RESN);
	DDX_Control(pDX, IDC_TIFR,	 m_TIFR);
	DDX_Control(pDX, IDC_TITO,	 m_TITO);
	DDX_Control(pDX, IDC_RADIO_UTC,	 m_UTC);
	DDX_Control(pDX, IDC_RADIO_LOCAL,m_LOCAL);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(OccupationDlg, CDialog)
	//{{AFX_MSG_MAP(OccupationDlg)
	ON_BN_CLICKED(IDC_RADIO_UTC,	OnUTC)
	ON_BN_CLICKED(IDC_RADIO_LOCAL,	OnLocal)
	ON_BN_CLICKED(IDC_DAYS_T,	OnDaysT)
	ON_BN_CLICKED(IDC_DAYS_7,	OnDays17)
	ON_BN_CLICKED(IDC_DAYS_6,	OnDays17)
	ON_BN_CLICKED(IDC_DAYS_5,	OnDays17)
	ON_BN_CLICKED(IDC_DAYS_4,	OnDays17)
	ON_BN_CLICKED(IDC_DAYS_2,	OnDays17)
	ON_BN_CLICKED(IDC_DAYS_3,	OnDays17)
	ON_BN_CLICKED(IDC_DAYS_1,	OnDays17)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten OccupationDlg 
//----------------------------------------------------------------------------------------

BOOL OccupationDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_NAME.SetBKColor(SILVER);
	m_NAME.SetInitText(omName);


	for (int i = 0; i < ogHAGData.omData.GetSize(); i++)
	{
		int ilIndex = m_HandlingAgents.AddString(ogHAGData.omData[i].Hnam);
		if (ilIndex != CB_ERR)
		{
			m_HandlingAgents.SetItemData(ilIndex,ogHAGData.omData[i].Urno);
		}
	}

	HAGDATA *prlHAG = ogHAGData.GetHAGByUrno(pomOcc->Ocby);
	if (prlHAG != NULL)
	{
		int ilIndex = m_HandlingAgents.SelectString(-1,prlHAG->Hnam);
		m_HandlingAgents.SetCurSel(ilIndex);
	}
	else
	{
		m_HandlingAgents.SetCurSel(-1);
	}
	//------------------------------------
	m_NAFRD.SetTypeToDate(true);
	m_NAFRD.SetTextErrColor(RED);
	m_NAFRD.SetBKColor(YELLOW);
	if (ogBasicData.UseLocalTimeAsDefault())
	{
		CTime olNafr = pomOcc->Ocfr;
		ogBasicData.UtcToLocal(olNafr);
		m_NAFRD.SetInitText(olNafr.Format("%d.%m.%Y"));
	}
	else
	{
		m_NAFRD.SetInitText(pomOcc->Ocfr.Format("%d.%m.%Y"));
	}
	// - - - - - - - - - - - - - - - - - -
	m_NAFRT.SetTypeToTime(true);
	m_NAFRT.SetTextErrColor(RED);
	m_NAFRT.SetBKColor(YELLOW);
	if (ogBasicData.UseLocalTimeAsDefault())
	{
		CTime olNafr = pomOcc->Ocfr;
		ogBasicData.UtcToLocal(olNafr);
		m_NAFRT.SetInitText(olNafr.Format("%H:%M"));
	}
	else
	{
		m_NAFRT.SetInitText(pomOcc->Ocfr.Format("%H:%M"));
	}
	//------------------------------------
	m_NATOD.SetTypeToDate();
	m_NATOD.SetTextErrColor(RED);

	if (ogBasicData.UseLocalTimeAsDefault())
	{
		CTime olNato = pomOcc->Octo;
		ogBasicData.UtcToLocal(olNato);
		m_NATOD.SetInitText(olNato.Format("%d.%m.%Y"));
	}
	else
	{
		m_NATOD.SetInitText(pomOcc->Octo.Format("%d.%m.%Y"));
	}
	// - - - - - - - - - - - - - - - - - -
	m_NATOT.SetTypeToTime();
	m_NATOT.SetTextErrColor(RED);
	if (ogBasicData.UseLocalTimeAsDefault())
	{
		CTime olNato = pomOcc->Octo;
		ogBasicData.UtcToLocal(olNato);
		m_NATOT.SetInitText(olNato.Format("%H:%M"));
	}
	else
	{
		m_NATOT.SetInitText(pomOcc->Octo.Format("%H:%M"));
	}

	//------------------------------------
	m_TIFR.SetTypeToTime();
	m_TIFR.SetTextErrColor(RED);
	if(strlen(pomOcc->Tifr) > 0)
	{
		if (ogBasicData.UseLocalTimeAsDefault())
		{
			CString olText = pomOcc->Tifr;
			CTime olCurrent = CTime::GetCurrentTime();
			CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

			ogBasicData.UtcToLocal(olTifr);
			m_TIFR.SetInitText(olTifr.Format("%H:%M"));

		}
		else
		{
			char pclTmp[10]="";
			sprintf(pclTmp, "%s:%s", CString(pomOcc->Tifr).Left(2), CString(pomOcc->Tifr).Right(2));
			m_TIFR.SetInitText(pclTmp);
		}
	}
	//------------------------------------
	m_TITO.SetTypeToTime();
	m_TITO.SetTextErrColor(RED);
	if(strlen(pomOcc->Tito) > 0)
	{
		if (ogBasicData.UseLocalTimeAsDefault())
		{
			CString olText = pomOcc->Tito;
			CTime olCurrent = CTime::GetCurrentTime();
			CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

			ogBasicData.UtcToLocal(olTito);
			m_TITO.SetInitText(olTito.Format("%H:%M"));
		}
		else
		{
			char pclTmp[10]="";
			sprintf(pclTmp, "%s:%s", CString(pomOcc->Tito).Left(2), CString(pomOcc->Tito).Right(2));
			m_TITO.SetInitText(pclTmp);
		}
	}
	//------------------------------------
	m_RESN.SetTypeToString("X(40)",40,0);
	m_RESN.SetTextErrColor(RED);
	m_RESN.SetInitText(pomOcc->Rema);
	//------------------------------------

	if(strchr(pomOcc->Days,'1') != NULL) m_DAYS_1.SetCheck(1);
	if(strchr(pomOcc->Days,'2') != NULL) m_DAYS_2.SetCheck(1);
	if(strchr(pomOcc->Days,'3') != NULL) m_DAYS_3.SetCheck(1);
	if(strchr(pomOcc->Days,'4') != NULL) m_DAYS_4.SetCheck(1);
	if(strchr(pomOcc->Days,'5') != NULL) m_DAYS_5.SetCheck(1);
	if(strchr(pomOcc->Days,'6') != NULL) m_DAYS_6.SetCheck(1);
	if(strchr(pomOcc->Days,'7') != NULL) m_DAYS_7.SetCheck(1);
	if(CString(pomOcc->Days).Find("1234567") != -1) m_DAYS_T.SetCheck(1);
	//------------------------------------

	if (ogBasicData.UseLocalTimeAsDefault())
	{
		this->m_UTC.SetCheck(0);
		this->m_LOCAL.SetCheck(1);
		imLastSelection = 2;
	}
	else
	{
		this->m_UTC.SetCheck(1);
		this->m_LOCAL.SetCheck(0);
		imLastSelection = 1;
	}
	return TRUE;
}

//----------------------------------------------------------------------------------------

void OccupationDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;

	if(m_TIFR.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText +=  LoadStg(IDS_STRING38) + ogNotFormat;
	}
	if(m_TITO.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText +=  LoadStg(IDS_STRING39) + ogNotFormat;
	}
	if(m_RESN.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText +=  LoadStg(IDS_STRING13) + ogNotFormat;
	}
	if(m_NAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_NAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_NAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_NAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_NATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_NATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	///////////////////////////////////////////////////////////////////////////
	CString olNafrd,olNafrt,olNatod,olNatot,olTifr,olTito;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_NAFRD.GetWindowText(olNafrd);
	m_NAFRT.GetWindowText(olNafrt);
	m_NATOD.GetWindowText(olNatod);
	m_NATOT.GetWindowText(olNatot);
	m_TIFR.GetWindowText(olTifr);
	m_TITO.GetWindowText(olTito);

	if((m_NAFRD.GetWindowTextLength() != 0 && m_NAFRT.GetWindowTextLength() == 0) || (m_NAFRD.GetWindowTextLength() == 0 && m_NAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_NATOD.GetWindowTextLength() != 0 && m_NATOT.GetWindowTextLength() == 0) || (m_NATOD.GetWindowTextLength() == 0 && m_NATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_NAFRD.GetStatus() == true && m_NAFRT.GetStatus() == true && m_NATOD.GetStatus() == true && m_NATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olNafrd,olNafrt);
		olTmpTimeTo = DateHourStringToDate(olNatod,olNatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}
	if(m_TIFR.GetStatus() == true && m_TITO.GetStatus() == true && olTifr.IsEmpty() == FALSE && olTito.IsEmpty() == FALSE)
	{
		olTmpTimeFr = HourStringToDate(olTifr);
		olTmpTimeTo = HourStringToDate(olTito);
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		
		CString olDays,olText;
		if(m_DAYS_1.GetCheck()==1) olDays  = "1";
		if(m_DAYS_2.GetCheck()==1) olDays += "2";
		if(m_DAYS_3.GetCheck()==1) olDays += "3";
		if(m_DAYS_4.GetCheck()==1) olDays += "4";
		if(m_DAYS_5.GetCheck()==1) olDays += "5";
		if(m_DAYS_6.GetCheck()==1) olDays += "6";
		if(m_DAYS_7.GetCheck()==1) olDays += "7";

		if(olDays.IsEmpty() == TRUE)
			olDays = "1234567";
		strcpy(pomOcc->Days,olDays);
		////////////////////////////

		pomOcc->Ocfr = DateHourStringToDate(olNafrd,olNafrt);
		pomOcc->Octo = DateHourStringToDate(olNatod,olNatot);
		if (this->m_UTC.GetCheck() == 0)	// Local !
		{
			ogBasicData.LocalToUtc(pomOcc->Ocfr);
			ogBasicData.LocalToUtc(pomOcc->Octo);
		}


		m_RESN.GetWindowText(pomOcc->Rema,41);

		m_TIFR.GetWindowText(olText); 
		olText = olText.Left(2) + olText.Right(2);

		if (!olText.IsEmpty() && this->m_UTC.GetCheck() == 0)	// Local !
		{
			CTime olCurrent = CTime::GetCurrentTime();
			CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

			ogBasicData.LocalToUtc(olTifr);
			olText.Format("%02d%02d",olTifr.GetHour(),olTifr.GetMinute());
		}

		strcpy(pomOcc->Tifr, olText);

		m_TITO.GetWindowText(olText); 
		olText = olText.Left(2) + olText.Right(2);

		if (!olText.IsEmpty() && this->m_UTC.GetCheck() == 0)	// Local !
		{
			CTime olCurrent = CTime::GetCurrentTime();
			CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

			ogBasicData.LocalToUtc(olTito);
			olText.Format("%02d%02d",olTito.GetHour(),olTito.GetMinute());
		}


		strcpy(pomOcc->Tito, olText);


		int ilIndex = m_HandlingAgents.GetCurSel();
		if (ilIndex != CB_ERR)
		{
			pomOcc->Ocby = m_HandlingAgents.GetItemData(ilIndex);
		}
		else
		{
			pomOcc->Ocby = 0;
		}

		strcpy(pomOcc->Octa,"HAG");

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_TIFR.SetFocus();
	}
}

//----------------------------------------------------------------------------------------

void OccupationDlg::OnDaysT() 
{
	if(m_DAYS_T.GetCheck() == 1)
	{
		m_DAYS_1.SetCheck(1);
		m_DAYS_2.SetCheck(1);
		m_DAYS_3.SetCheck(1);
		m_DAYS_4.SetCheck(1);
		m_DAYS_5.SetCheck(1);
		m_DAYS_6.SetCheck(1);
		m_DAYS_7.SetCheck(1);
	}
	if(m_DAYS_T.GetCheck() == 0)
	{
		m_DAYS_1.SetCheck(0);
		m_DAYS_2.SetCheck(0);
		m_DAYS_3.SetCheck(0);
		m_DAYS_4.SetCheck(0);
		m_DAYS_5.SetCheck(0);
		m_DAYS_6.SetCheck(0);
		m_DAYS_7.SetCheck(0);
	}
}

//----------------------------------------------------------------------------------------

void OccupationDlg::OnDays17() 
{
	if(m_DAYS_T.GetCheck() == 1)
	{
		m_DAYS_T.SetCheck(0);
	}
}

//----------------------------------------------------------------------------------------

void OccupationDlg::OnUTC()
{
	if (imLastSelection == 1)
		return;

	CString olErrorText;
	bool ilStatus = true;

	if(m_TIFR.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText +=  LoadStg(IDS_STRING38) + ogNotFormat;
	}

	if(m_TITO.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText +=  LoadStg(IDS_STRING39) + ogNotFormat;
	}

	if(m_NAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_NAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}

	if(m_NAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_NAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}

	if(m_NATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}

	if(m_NATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olNafrd,olNafrt,olNatod,olNatot,olTifr,olTito;
	CTime olTmpTimeFr,olTmpTimeTo;

	m_NAFRD.GetWindowText(olNafrd);
	m_NAFRT.GetWindowText(olNafrt);
	m_NATOD.GetWindowText(olNatod);
	m_NATOT.GetWindowText(olNatot);
	m_TIFR.GetWindowText(olTifr);
	m_TITO.GetWindowText(olTito);

	if((m_NAFRD.GetWindowTextLength() != 0 && m_NAFRT.GetWindowTextLength() == 0) || (m_NAFRD.GetWindowTextLength() == 0 && m_NAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}

	if((m_NATOD.GetWindowTextLength() != 0 && m_NATOT.GetWindowTextLength() == 0) || (m_NATOD.GetWindowTextLength() == 0 && m_NATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}

	if(m_NAFRD.GetStatus() == true && m_NAFRT.GetStatus() == true && m_NATOD.GetStatus() == true && m_NATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olNafrd,olNafrt);
		olTmpTimeTo = DateHourStringToDate(olNatod,olNatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	if(m_TIFR.GetStatus() == true && m_TITO.GetStatus() == true && olTifr.IsEmpty() == FALSE && olTito.IsEmpty() == FALSE)
	{
		olTmpTimeFr = HourStringToDate(olTifr);
		olTmpTimeTo = HourStringToDate(olTito);
	}

	///////////////////////////////////////////////////////////////////////////
	if (ilStatus == true)
	{
		CString olText;
		CTime olNafr = DateHourStringToDate(olNafrd,olNafrt);
		CTime olNato = DateHourStringToDate(olNatod,olNatot);

		ogBasicData.LocalToUtc(olNafr);
		ogBasicData.LocalToUtc(olNato);

		m_NAFRD.SetInitText(olNafr.Format("%d.%m.%Y"));
		m_NAFRT.SetInitText(olNafr.Format("%H:%M"));

		m_NATOD.SetInitText(olNato.Format("%d.%m.%Y"));
		m_NATOT.SetInitText(olNato.Format("%H:%M"));
		
		m_TIFR.GetWindowText(olText); 
		olText = olText.Left(2) + olText.Right(2);
		if (!olText.IsEmpty())
		{
			CTime olCurrent = CTime::GetCurrentTime();
			CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

			ogBasicData.LocalToUtc(olTifr);
			m_TIFR.SetInitText(olTifr.Format("%H:%M"));
		}

		m_TITO.GetWindowText(olText); 
		olText = olText.Left(2) + olText.Right(2);

		if (!olText.IsEmpty())
		{
			CTime olCurrent = CTime::GetCurrentTime();
			CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);
			ogBasicData.LocalToUtc(olTito);
			m_TITO.SetInitText(olTito.Format("%H:%M"));
		}

		imLastSelection = 1;
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_TIFR.SetFocus();

		this->m_UTC.SetCheck(0);
		this->m_LOCAL.SetCheck(1);
	}
}
//----------------------------------------------------------------------------------------

void OccupationDlg::OnLocal()
{
	if (imLastSelection == 2)
		return;

	CString olErrorText;
	bool ilStatus = true;

	if(m_TIFR.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText +=  LoadStg(IDS_STRING38) + ogNotFormat;
	}

	if(m_TITO.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText +=  LoadStg(IDS_STRING39) + ogNotFormat;
	}

	if(m_NAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_NAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}

	if(m_NAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_NAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}

	if(m_NATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}

	if(m_NATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olNafrd,olNafrt,olNatod,olNatot,olTifr,olTito;
	CTime olTmpTimeFr,olTmpTimeTo;

	m_NAFRD.GetWindowText(olNafrd);
	m_NAFRT.GetWindowText(olNafrt);
	m_NATOD.GetWindowText(olNatod);
	m_NATOT.GetWindowText(olNatot);
	m_TIFR.GetWindowText(olTifr);
	m_TITO.GetWindowText(olTito);

	if((m_NAFRD.GetWindowTextLength() != 0 && m_NAFRT.GetWindowTextLength() == 0) || (m_NAFRD.GetWindowTextLength() == 0 && m_NAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}

	if((m_NATOD.GetWindowTextLength() != 0 && m_NATOT.GetWindowTextLength() == 0) || (m_NATOD.GetWindowTextLength() == 0 && m_NATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}

	if(m_NAFRD.GetStatus() == true && m_NAFRT.GetStatus() == true && m_NATOD.GetStatus() == true && m_NATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olNafrd,olNafrt);
		olTmpTimeTo = DateHourStringToDate(olNatod,olNatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	if(m_TIFR.GetStatus() == true && m_TITO.GetStatus() == true && olTifr.IsEmpty() == FALSE && olTito.IsEmpty() == FALSE)
	{
		olTmpTimeFr = HourStringToDate(olTifr);
		olTmpTimeTo = HourStringToDate(olTito);
	}

	///////////////////////////////////////////////////////////////////////////
	if (ilStatus == true)
	{
		CString olText;
		CTime olNafr = DateHourStringToDate(olNafrd,olNafrt);
		CTime olNato = DateHourStringToDate(olNatod,olNatot);

		ogBasicData.UtcToLocal(olNafr);
		ogBasicData.UtcToLocal(olNato);

		m_NAFRD.SetInitText(olNafr.Format("%d.%m.%Y"));
		m_NAFRT.SetInitText(olNafr.Format("%H:%M"));

		m_NATOD.SetInitText(olNato.Format("%d.%m.%Y"));
		m_NATOT.SetInitText(olNato.Format("%H:%M"));
		
		m_TIFR.GetWindowText(olText); 
		olText = olText.Left(2) + olText.Right(2);
		if (!olText.IsEmpty())
		{
			CTime olCurrent = CTime::GetCurrentTime();
			CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

			ogBasicData.UtcToLocal(olTifr);
			m_TIFR.SetInitText(olTifr.Format("%H:%M"));
		}

		m_TITO.GetWindowText(olText); 
		olText = olText.Left(2) + olText.Right(2);
		if (!olText.IsEmpty())
		{
			CTime olCurrent = CTime::GetCurrentTime();
			CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

			ogBasicData.UtcToLocal(olTito);
			m_TITO.SetInitText(olTito.Format("%H:%M"));
		}

		imLastSelection = 2;
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_TIFR.SetFocus();

		this->m_UTC.SetCheck(1);
		this->m_LOCAL.SetCheck(0);
	}
}
