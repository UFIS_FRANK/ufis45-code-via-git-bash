// PositionDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <PositionDlg.h>
#include <CedaTWYData.h>
#include <PrivList.h>
#include <NotAvailableDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PositionDlg dialog


PositionDlg::PositionDlg(PSTDATA *popPST,CWnd* pParent /*=NULL*/) 
: CDialog(PositionDlg::IDD, pParent)
{
	pomPST = popPST;
	pomTable = NULL;
	pomTable = new CCSTable;
	//{{AFX_DATA_INIT(PositionDlg)
	//}}AFX_DATA_INIT
}

PositionDlg::PositionDlg(PSTDATA *popPST,int ipIDDlg,CWnd* pParent /*=NULL*/) 
: CDialog(ipIDDlg, pParent)
{
	pomPST = popPST;
	pomTable = NULL;
	pomTable = new CCSTable;
	//{{AFX_DATA_INIT(PositionDlg)
	//}}AFX_DATA_INIT
}

PositionDlg::~PositionDlg()
{
	omBlkPtrA.DeleteAll();
	omDeleteBlkPtrA.DeleteAll();
	delete pomTable;

}

void PositionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PositionDlg)
	DDX_Control(pDX, IDC_DEFD, m_DEFD);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_NOAVNEW, m_NOAVNEW);
	DDX_Control(pDX, IDC_NOAVDEL, m_NOAVDEL);
	DDX_Control(pDX, IDC_NOAV,	 m_NOAVFRAME);
	DDX_Control(pDX, IDC_BRGS,	 m_BRGS);
	DDX_Control(pDX, IDC_DGSS,	 m_DGSS);
	DDX_Control(pDX, IDC_BUBK,	 m_PUBK);
	DDX_Control(pDX, IDC_PCAS,	 m_PCAS);
	DDX_Control(pDX, IDC_GPUS,	 m_GPUS);
	DDX_Control(pDX, IDC_FULS,	 m_FULS);
	DDX_Control(pDX, IDC_ACUS,	 m_ACUS);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_VAFR_D1,m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_PNAM,	 m_PNAM);
	DDX_Control(pDX, IDC_TAXI,	 m_TAXI);
	DDX_Control(pDX, IDC_TELE,	 m_TELE);
	DDX_Control(pDX, IDC_HOME,	 m_HOME);
	DDX_Control(pDX, IDC_MXAC,	 m_MXAC);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PositionDlg, CDialog)
	//{{AFX_MSG_MAP(PositionDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVDEL, OnNoavDel)
	ON_BN_CLICKED(IDC_NOAVNEW, OnNoavNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PositionDlg message handlers
//----------------------------------------------------------------------------------------

BOOL PositionDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING185) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("POSITIONDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomPST->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomPST->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomPST->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomPST->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomPST->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomPST->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomPST->);
	m_GRUP.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_PNAM.SetFormat("x|#x|#x|#x|#x|#");
	m_PNAM.SetTextLimit(1,5);
	m_PNAM.SetBKColor(YELLOW);
	m_PNAM.SetTextErrColor(RED);
	m_PNAM.SetInitText(pomPST->Pnam);
	m_PNAM.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_PNAM"));
	//------------------------------------
	if (pomPST->Gpus[0] == 'X') m_GPUS.SetCheck(1);
	clStat = ogPrivList.GetStat("POSITIONDLG.m_GPUS");
	SetWndStatAll(clStat,m_GPUS);
	if (pomPST->Acus[0] == 'X') m_ACUS.SetCheck(1);
	clStat = ogPrivList.GetStat("POSITIONDLG.m_ACUS");
	SetWndStatAll(clStat,m_ACUS);
	if (pomPST->Fuls[0] == 'X') m_FULS.SetCheck(1);
	clStat = ogPrivList.GetStat("POSITIONDLG.m_FULS");
	SetWndStatAll(clStat,m_FULS);
	if (pomPST->Pcas[0] == 'X') m_PCAS.SetCheck(1);
	clStat = ogPrivList.GetStat("POSITIONDLG.m_PCAS");
	SetWndStatAll(clStat,m_PCAS);
	if (pomPST->Pubk[0] == 'X') m_PUBK.SetCheck(1);
	clStat = ogPrivList.GetStat("POSITIONDLG.m_PUBK");
	SetWndStatAll(clStat,m_PUBK);
	if (pomPST->Dgss[0] == 'X') m_DGSS.SetCheck(1);
	clStat = ogPrivList.GetStat("POSITIONDLG.m_DGSS");
	SetWndStatAll(clStat,m_DGSS);
	if (pomPST->Brgs[0] == 'X') m_BRGS.SetCheck(1);
	clStat = ogPrivList.GetStat("POSITIONDLG.m_BRGS");
	SetWndStatAll(clStat,m_BRGS);
	//------------------------------------
	m_HOME.SetTypeToString("X(3)",3,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomPST->Home);
	m_HOME.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_HOME"));
	//------------------------------------
	m_MXAC.SetTypeToString("X(5)",5,1);
	m_MXAC.SetTextErrColor(RED);
	m_MXAC.SetInitText(pomPST->Mxac);
	m_MXAC.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_MXAC"));
	//------------------------------------
//	m_NPOS.SetBKColor(SILVER);
	//m_NPOS.SetInitText(pomPST->);
//	m_NPOS.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_NPOS"));
	//------------------------------------
//	m_ATYP.SetBKColor(SILVER);
	//m_ATYP.SetInitText(pomPST->);
//	m_ATYP.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_ATYP"));
	//------------------------------------
	m_TELE.SetTypeToString("X(10)",10,0);
	m_TELE.SetTextErrColor(RED);
	m_TELE.SetInitText(pomPST->Tele);
	m_TELE.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_TELE"));
	//------------------------------------
	m_TAXI.SetFormat("x|#x|#x|#x|#x|#");
	m_TAXI.SetTextLimit(0,5);
	m_TAXI.SetTextErrColor(RED);
	m_TAXI.SetInitText(pomPST->Taxi);
	m_TAXI.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_TAXI"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomPST->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomPST->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomPST->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomPST->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_VATO"));
	//------------------------------------
	m_DEFD.SetFormat("###");
	m_DEFD.SetTextLimit(0,3);
	m_DEFD.SetTextErrColor(RED);
	m_DEFD.SetInitText(pomPST->Defd);
	m_DEFD.SetSecState(ogPrivList.GetStat("POSITIONDLG.m_DEFD"));

	CString olUrno;
	olUrno.Format("%d",pomPST->Urno);
	MakeNoavTable("PST", olUrno);

	return TRUE;  
}

//----------------------------------------------------------------------------------------

void PositionDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_PNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_TELE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING294) + ogNotFormat;
	}
	if(m_TAXI.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING99) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	
	if(m_DEFD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING856) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olPnam,olTaxi;
		char clWhere[100];
		CCSPtrArray<PSTDATA> olPstCPA;

		m_PNAM.GetWindowText(olPnam);
		sprintf(clWhere,"WHERE PNAM='%s'",olPnam);
		if(ogPSTData.ReadSpecialData(&olPstCPA,clWhere,"URNO,PNAM",false) == true)
		{
			if(olPstCPA[0].Urno != pomPST->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
			}
		}
		olPstCPA.DeleteAll();

		if(m_TAXI.GetWindowTextLength() != 0)
		{
			m_TAXI.GetWindowText(olTaxi);
			sprintf(clWhere,"WHERE TNAM='%s'",olTaxi);
			if(ogTWYData.ReadSpecialData(NULL,clWhere,"TNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING100);
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_PNAM.GetWindowText(pomPST->Pnam,6);
		m_TAXI.GetWindowText(pomPST->Taxi,6);
		(m_GPUS.GetCheck() == 1) ? pomPST->Gpus[0] = 'X' : pomPST->Gpus[0] = ' ';
		(m_ACUS.GetCheck() == 1) ? pomPST->Acus[0] = 'X' : pomPST->Acus[0] = ' ';
		(m_FULS.GetCheck() == 1) ? pomPST->Fuls[0] = 'X' : pomPST->Fuls[0] = ' ';
		(m_PCAS.GetCheck() == 1) ? pomPST->Pcas[0] = 'X' : pomPST->Pcas[0] = ' ';
		(m_PUBK.GetCheck() == 1) ? pomPST->Pubk[0] = 'X' : pomPST->Pubk[0] = ' ';
		(m_DGSS.GetCheck() == 1) ? pomPST->Dgss[0] = 'X' : pomPST->Dgss[0] = ' ';
		(m_BRGS.GetCheck() == 1) ? pomPST->Brgs[0] = 'X' : pomPST->Brgs[0] = ' ';
		m_TELE.GetWindowText(pomPST->Tele,11);
		m_MXAC.GetWindowText(pomPST->Mxac,5);
		m_HOME.GetWindowText(pomPST->Home,4);
		pomPST->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomPST->Vato = DateHourStringToDate(olVatod,olVatot);
		m_DEFD.GetWindowText(pomPST->Defd,4);
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_PNAM.SetFocus();
		m_PNAM.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------
void PositionDlg::MakeNoavTable(CString opTabn, CString opBurn)
{
	CRect olRectBorder;
	m_NOAVFRAME.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);
	pomTable->SetShowSelection(TRUE);
	pomTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	pomTable->SetHeaderFields(olHeaderDataArray);
	pomTable->SetDefaultSeparator();
	olHeaderDataArray.DeleteAll();

	char clWhere[100];
	CCSPtrArray<BLKDATA> olBlkCPA;
	sprintf(clWhere,"WHERE TABN='%s' AND BURN='%s'",opTabn,opBurn);
	ogBlkData.ReadSpecialData(&omBlkPtrA,clWhere,"",false);
	int ilLines = omBlkPtrA.GetSize();

	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omBlkPtrA[ilLineNo].Nafr.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Nato.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Days;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		char pclTmp[10]="";
		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tifr) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tifr).Left(2), CString(omBlkPtrA[ilLineNo].Tifr).Right(2));
			rlColumnData.Text = pclTmp;
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tito) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tito).Left(2), CString(omBlkPtrA[ilLineNo].Tito).Right(2));
			rlColumnData.Text = pclTmp;
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		(*omBlkPtrA[ilLineNo].Type == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Resn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomTable->DisplayTable();
}

//----------------------------------------------------------------------------------------

void PositionDlg::ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo)
{
	if(prpBlk != NULL)
	{
		TABLE_COLUMN rlColumn;
		CCSPtrArray <TABLE_COLUMN> olLine;

		rlColumn.Lineno = 0;
		rlColumn.VerticalSeparator = SEPA_NONE;
		rlColumn.SeparatorType = SEPA_NONE;
		rlColumn.BkColor = WHITE;
		rlColumn.Font = &ogCourier_Regular_10;
		rlColumn.Alignment = COLALIGN_LEFT;

		int ilColumnNo = 0;
		rlColumn.Text = prpBlk->Nafr.Format("%d.%m.%Y %H:%M");
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Nato.Format("%d.%m.%Y %H:%M"); 
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Days;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		char pclTmp[10]="";
		rlColumn.Text = "";
		if(strlen(prpBlk->Tifr) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(prpBlk->Tifr).Left(2), CString(prpBlk->Tifr).Right(2));
			rlColumn.Text = pclTmp;
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text = "";
		if(strlen(prpBlk->Tito) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(prpBlk->Tito).Left(2), CString(prpBlk->Tito).Right(2));
			rlColumn.Text = pclTmp;
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		(*prpBlk->Type == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Resn;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		if(ipLineNo>-1)
		{
			pomTable->ChangeTextLine(ipLineNo, &olLine, (void *)prpBlk);
			pomTable->DisplayTable();
		}
		else
		{
			if(prpBlk != NULL)
			{
				pomTable->AddTextLine(olLine, (void *)prpBlk);
				pomTable->DisplayTable();
			}
		}
		olLine.DeleteAll();
	}
	else
	{
		if(ipLineNo>-1)
		{
			pomTable->DeleteTextLine(ipLineNo);
			pomTable->DisplayTable();
		}
	}
}

//----------------------------------------------------------------------------------------

LONG PositionDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(ogPrivList.GetStat("POSITIONDLG.m_NOAVCHA") == '1')
	{
		int ilLineNo = pomTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
		}
		else
		{
			BLKDATA *prlBlk = &omBlkPtrA[ilLineNo];
			if (prlBlk != NULL)
			{
				BLKDATA rlBlk = *prlBlk;
				NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
				m_PNAM.GetWindowText(polDlg->omName);
				if(polDlg->DoModal() == IDOK)
				{
					*prlBlk = rlBlk;
					if(prlBlk->IsChanged != DATA_NEW) prlBlk->IsChanged = DATA_CHANGED;
					ChangeNoavTable(prlBlk, ilLineNo);
				}
				delete polDlg;
			}
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void PositionDlg::OnNoavNew() 
{
	BLKDATA rlBlk;

	if(ogPrivList.GetStat("POSITIONDLG.m_NOAVNEW") == '1')
	{
		NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
		m_PNAM.GetWindowText(polDlg->omName);
		if(polDlg->DoModal() == IDOK)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = rlBlk;
			omBlkPtrA.Add(prlBlk);
			prlBlk->IsChanged = DATA_NEW;
			ChangeNoavTable(prlBlk, -1);
		}
		delete polDlg;
	}
}

//----------------------------------------------------------------------------------------

void PositionDlg::OnNoavDel() 
{
	int ilLineNo = pomTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
	}
	else
	{
		if(ogPrivList.GetStat("POSITIONDLG.m_NOAVDEL") == '1')
		{
			if (&omBlkPtrA[ilLineNo] != NULL)
			{
				BLKDATA *prlBlk = new BLKDATA;
				*prlBlk = omBlkPtrA[ilLineNo];
				prlBlk->IsChanged = DATA_DELETED;
				omDeleteBlkPtrA.Add(prlBlk);
				omBlkPtrA.DeleteAt(ilLineNo);
				ChangeNoavTable(NULL, ilLineNo);
			}
		}
	}
}
