#ifndef __CHECKINCOUNTERTABLEVIEWER_H__
#define __CHECKINCOUNTERTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaCICData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct CHECKINCOUNTERTABLE_LINEDATA
{
	long	Urno;
	CString Cnam;
	CString	Catr;
	CString	Edpe;
	CString Rgbl;
	CString	Term;
	CString	Tele;
	CString Tel2;
	CString	Cicr;
	CString Hall;
	CString Vafr;
	CString Vato;
	CString Home;
	CString Cbaz;
	CString Cicl;
};

/////////////////////////////////////////////////////////////////////////////
// CheckinCounterTableViewer

class CheckinCounterTableViewer : public CViewer
{
// Constructions
public:
    CheckinCounterTableViewer(CCSPtrArray<CICDATA> *popData);
    ~CheckinCounterTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	BOOL IsPassFilter(CICDATA *prpCheckinCounter);
	int CompareCheckinCounter(CHECKINCOUNTERTABLE_LINEDATA *prpCheckinCounter1, CHECKINCOUNTERTABLE_LINEDATA *prpCheckinCounter2);
    void MakeLines();
	void MakeLine(CICDATA *prpCheckinCounter);
// Operations
public:
	BOOL FindLine(long lpUrno, int &rilLineno);
	void DeleteAll();
	void CreateLine(CHECKINCOUNTERTABLE_LINEDATA *prpCheckinCounter);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(CHECKINCOUNTERTABLE_LINEDATA *prpLine);
	void ProcessCheckinCounterChange(CICDATA *prpCheckinCounter);
	void ProcessCheckinCounterDelete(CICDATA *prpCheckinCounter);
	BOOL FindCheckinCounter(char *prpCheckinCounterKeya, char *prpCheckinCounterKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	BOOL bmIsFromSearch;
// Attributes
private:
    CCSTable *pomCheckinCounterTable;
	CCSPtrArray<CICDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<CHECKINCOUNTERTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(CHECKINCOUNTERTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__CHECKINCOUNTERTABLEVIEWER_H__
