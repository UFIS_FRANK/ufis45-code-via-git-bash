rem Copies all generated files from the SCM-Structue into the Install-Shield directory.
rem 
rem Copy ini- and config-files
copy c:\ufis_bin\ceda.ini f:\installshield\bkk_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\tagslogo.bmp f:\installshield\bkk_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\grp.ini f:\installshield\bkk_build\ufis_client_appl\grp\*.*
copy c:\ufis_bin\bdps_sec.cfg f:\installshield\bkk_build\ufis_client_appl\bdps-sec\*.*
copy c:\ufis_bin\RosteringPrint.ulf f:\installshield\bkk_build\ufis_client_appl\Rostering\*.*
copy c:\ufis_bin\loatabcfg.cfg f:\installshield\bkk_build\ufis_client_appl\loatabviewer\*.*
copy c:\ufis_bin\loatabviewer.ini f:\installshield\bkk_build\ufis_client_appl\loatabviewer\*.*
copy c:\ufis_bin\telexpool.ini f:\installshield\bkk_build\ufis_client_appl\telexpool\*.*

rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe f:\installshield\bkk_build\ufis_client_appl\bdps-sec\*.*
copy c:\ufis_bin\debug\bdpspass.exe f:\installshield\bkk_build\ufis_client_appl\bdps-pass\*.*
copy c:\ufis_bin\debug\bdpsuif.exe f:\installshield\bkk_build\ufis_client_appl\bdps-uif\*.*
copy c:\ufis_bin\debug\coco3.exe f:\installshield\bkk_build\ufis_client_appl\languagetool\*.*
copy c:\ufis_bin\debug\coverage.exe f:\installshield\bkk_build\ufis_client_appl\coverage\*.*
copy c:\ufis_bin\debug\fips.exe f:\installshield\bkk_build\ufis_client_appl\fips\*.*
copy c:\ufis_bin\debug\grp.exe f:\installshield\bkk_build\ufis_client_appl\grp\*.*
copy c:\ufis_bin\debug\opsspm.exe f:\installshield\bkk_build\ufis_client_appl\opsspm\*.*
copy c:\ufis_bin\debug\regelwerk.exe f:\installshield\bkk_build\ufis_client_appl\regelwerk\*.*
copy c:\ufis_bin\debug\rules.exe f:\installshield\bkk_build\ufis_client_appl\fips-rules\*.*
copy c:\ufis_bin\debug\servicecatalog.exe f:\installshield\bkk_build\ufis_client_appl\servicecatalog\*.*
copy c:\ufis_bin\debug\poolalloc.exe f:\installshield\bkk_build\ufis_client_appl\poolalloc\*.*
copy c:\ufis_bin\debug\profileeditor.exe f:\installshield\bkk_build\ufis_client_appl\profileeditor\*.*
copy c:\ufis_bin\debug\wgrtool.exe f:\installshield\bkk_build\ufis_client_appl\wgrtool\*.*
copy c:\ufis_bin\release\rostering.exe f:\installshield\bkk_build\ufis_client_appl\rostering\*.*
copy c:\ufis_bin\release\rosteringprint.exe f:\installshield\bkk_build\ufis_client_appl\rostering\*.*
copy c:\ufis_bin\release\telexpool.exe f:\installshield\bkk_build\ufis_client_appl\telexpool\*.*
copy c:\ufis_bin\release\loadtabviewer.exe f:\installshield\bkk_build\ufis_client_appl\loatabviewer\*.*
copy c:\ufis_bin\release\bcproxy.exe f:\installshield\bkk_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb f:\installshield\bkk_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe f:\installshield\bkk_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll f:\installshield\bkk_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe f:\installshield\bkk_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb f:\installshield\bkk_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.exe f:\installshield\bkk_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.tlb f:\installshield\bkk_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx f:\installshield\bkk_build\ufis_client_appl\aat_ocx\*.*
copy c:\ufis_bin\release\tab.tlb f:\installshield\bkk_build\ufis_client_appl\aat_ocx\*.*
copy c:\ufis_bin\release\ufiscom.ocx f:\installshield\bkk_build\ufis_client_appl\aat_ocx\*.*
copy c:\ufis_bin\release\ufiscom.tlb f:\installshield\bkk_build\ufis_client_appl\aat_ocx\*.*
copy c:\ufis_bin\release\aatlogincontrol.ocx f:\installshield\bkk_build\ufis_client_appl\aat_ocx\*.*

rem Copy ReleaseNotes
rem copy c:\ufis_bin\releasenotes.zip f:\installshield\bkk_build\ufis_client_appl\rel_notes\*.*


rem Copy Help-Files
copy c:\ufis_bin\Help\*.chm f:\installshield\bkk_build\ufis_client_appl\help\*.*
