Rem BEGIN
date /T
time /T 


Rem ---Check for folder c:\ufis\system---
IF EXIST c:\ufis\system\nul GOTO cont1
Echo Create folder C:\ufis\system
md c:\ufis\system
:cont1

IF EXIST C:\Ufis_Bin\Runtimedlls\nul goto cont2
md C:\Ufis_Bin\Runtimedlls
:cont2

IF EXIST C:\tmp\nul goto cont3
md C:\tmp
:cont3

ECHO Project TAGS > C:\Ufis_Bin\Bkk.txt

mkdir c:\Ufis_Bin\ClassLib
mkdir c:\Ufis_Bin\ClassLib\Include
mkdir c:\Ufis_Bin\ClassLib\Include\jm
mkdir C:\Ufis_Bin\ForaignLibs
mkdir C:\Ufis_Bin\Help

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.lib C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.pdb C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.dll C:\Ufis_Bin\Runtimedlls
copy ..\..\..\_Standard\_Standard_client\_Standard_configuration\Clientruntime\*.* C:\Ufis_Bin\Runtimedlls
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\include\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\include\jm\*.h c:\Ufis_Bin\ClassLib\Include\jm
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_ClassLib\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Wrapper\*.h c:\Ufis_Bin\ClassLib\Include

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Classlib\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Classlib\*.rc c:\Ufis_Bin\ClassLib\Include

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_HtmlHelp\lib\*.lib C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_HtmlHelp\include\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_help\*.chm c:\Ufis_Bin\help

copy ..\..\..\_Standard\_Standard_client\_Standard_Dutyroster\_Standard_RosteringPrint\RosteringPrint.ulf c:\Ufis_Bin

copy ..\BKK_configuration\ceda.ini c:\Ufis_Bin
copy ..\BKK_configuration\Bdps_sec.cfg c:\Ufis_Bin
copy ..\BKK_configuration\tagslogo.bmp c:\Ufis_Bin
copy ..\BKK_configuration\grp.ini c:\Ufis_Bin
copy ..\BKK_configuration\telexpool.ini c:\Ufis_Bin
copy ..\BKK_Configuration\Loatabviewer.ini c:\Ufis_Bin
copy ..\BKK_configuration\Loatabcfg.cfg c:\Ufis_Bin
copy copy_for_install.bat c:\Ufis_Bin

REM ------------------Cients build from _Standard-Project-----------------------------------

cd ..\..\..\_Standard\_Standard_client


REM ------------------------Share-----------------------------------------------------------
ECHO Build Ufis32.dll... > con
cd _Standard_share\_Standard_Ufis32dll
msdev Ufis32.dsp /MAKE "Ufis32 - Debug" /REBUILD
msdev Ufis32.dsp /MAKE "Ufis32 - Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Ufis32.ilk del c:\Ufis_Bin\Debug\Ufis32.ilk
If exist c:\Ufis_Bin\Debug\Ufis32.pdb del c:\Ufis_Bin\Debug\Ufis32.pdb
If exist c:\Ufis_Bin\Debug\Ufis32.pdb del c:\Ufis_Bin\Debug\Ufis32.exp
If exist c:\Ufis_Bin\Release\Ufis32.ilk del c:\Ufis_Bin\Release\Ufis32.ilk
If exist c:\Ufis_Bin\Release\Ufis32.pdb del c:\Ufis_Bin\Release\Ufis32.pdb
If exist c:\Ufis_Bin\Release\Ufis32.pdb del c:\Ufis_Bin\Release\Ufis32.exp
copy c:\ufis_bin\release\Ufis32.dll c:\ufis_bin\debug\*.*
copy c:\ufis_bin\release\Ufis32.lib c:\ufis_bin\debug\*.*
cd ..\..

ECHO Build Classlib... > con
cd _Standard_share\_Standard_ClassLib
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Debug" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Release" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Dll Debug" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Dll Release" /REBUILD
cd ..\..

ECHO Build BcServ... > con
cd _Standard_share\_Standard_BcServ
rem msdev BcServ32.dsp /MAKE "BcServ - Debug" /REBUILD
msdev BcServ32.dsp /MAKE "BcServ - Release" /REBUILD
If exist c:\Ufis_Bin\Release\BcServ32.ilk del c:\Ufis_Bin\Release\BcServ32.ilk
If exist c:\Ufis_Bin\Release\BcServ32.pdb del c:\Ufis_Bin\Release\BcServ32.pdb
cd ..\..

cd _Standard_share\_Standard_BcServ
rem msdev BcServ32.dsp /MAKE "BcServ - Debug" /REBUILD
msdev BcServ32.dsp /MAKE "BcServ - Release" /REBUILD
If exist c:\Ufis_Bin\Release\BcServ32.ilk del c:\Ufis_Bin\Release\BcServ32.ilk
If exist c:\Ufis_Bin\Release\BcServ32.pdb del c:\Ufis_Bin\Release\BcServ32.pdb
cd ..\..

ECHO Build BcProxy... > con
cd _Standard_share\_Standard_BcProxy
Rem ---remove read-only state---
attrib *.* -r
rem msdev BcProxy.dsp /MAKE "BcProxy - Win32 Debug" /REBUILD
msdev BcProxy.dsp /MAKE "BcProxy - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\BcProxy.ilk del c:\Ufis_Bin\Release\BcProxy.ilk
If exist c:\Ufis_Bin\Release\BcProxy.pdb del c:\Ufis_Bin\Release\BcProxy.pdb
cd ..\..

ECHO Build Tabocx... > con
cd _Standard_share\_Standard_TABocx
rem msdev TAB.dsp /MAKE "TAB - Win32 Debug" /REBUILD
msdev TAB.dsp /MAKE "TAB - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\TAB.ilk del c:\Ufis_Bin\Release\TAB.ilk
If exist c:\Ufis_Bin\Release\TAB.pdb del c:\Ufis_Bin\Release\TAB.pdb
cd ..\..

ECHO Build UfisCedaocx... > con
cd _Standard_share\_Standard_Ufiscedaocx
rem msdev UfisCom.dsp /MAKE "UfisCom - Win32 Debug" /REBUILD
msdev UfisCom.dsp /MAKE "UfisCom - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\UfisCom.ilk del c:\Ufis_Bin\Release\UfisCom.ilk
If exist c:\Ufis_Bin\Release\UfisCom.pdb del c:\Ufis_Bin\Release\UfisCom.pdb
cd ..\..

ECHO Build UfisAppMng... > con
cd _Standard_share\_Standard_UfisApManager
Rem ---remove read-only state---
attrib *.* -r
rem msdev UfisAppMng.dsp /MAKE "UfisAppMng - Win32 Debug" /REBUILD
msdev UfisAppMng.dsp /MAKE "UfisAppMng - Win32 Release" /REBUILD
copy c:\ufis_bin\release\UfisAppMng.tlb c:\ufis\system
If exist c:\Ufis_Bin\Release\UfisAppMng.ilk del c:\Ufis_Bin\Release\UfisAppMng.ilk
If exist c:\Ufis_Bin\Release\UfisAppMng.pdb del c:\Ufis_Bin\Release\UfisAppMng.pdb
cd ..\..

ECHO Build UfisApplMgr... > con
cd _Standard_share\_Standard_UfisApplMgr
Rem ---remove read-only state---
attrib *.* -r
rem msdev UfisApplMgr.dsp /MAKE "UfisApplMgr - Win32 Debug" /REBUILD
msdev UfisApplMgr.dsp /MAKE "UfisApplMgr - Win32 Release" /REBUILD
copy c:\ufis_bin\release\UfisApplMgr.tlb c:\ufis\system
If exist c:\Ufis_Bin\Release\UfisApplMgr.ilk del c:\Ufis_Bin\Release\UfisApplMgr.ilk
If exist c:\Ufis_Bin\Release\UfisApplMgr.pdb del c:\Ufis_Bin\Release\UfisApplMgr.pdb
cd ..\..

ECHO Build BdpsPass... > con
cd _Standard_share\_Standard_Bdpspass
msdev Bdpspass.dsp /MAKE "Bdpspass - Win32 Debug" /REBUILD
rem msdev Bdpspass.dsp /MAKE "Bdpspass - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdpspass.ilk del c:\Ufis_Bin\Debug\Bdpspass.ilk
If exist c:\Ufis_Bin\Debug\Bdpspass.pdb del c:\Ufis_Bin\Debug\Bdpspass.pdb
cd ..\..

ECHO Build BdpsSec... > con
cd _Standard_share\_Standard_Bdpssec
msdev Bdps_sec.dsp /MAKE "Bdps_sec - Win32 Debug" /REBUILD
rem msdev Bdps_sec.dsp /MAKE "Bdps_sec - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdps_sec.ilk del c:\Ufis_Bin\Debug\Bdps_sec.ilk
If exist c:\Ufis_Bin\Debug\Bdps_sec.pdb del c:\Ufis_Bin\Debug\Bdps_sec.pdb
cd ..\..

ECHO Build BdpsUif... > con
cd _Standard_share\_Standard_Bdpsuif
msdev Bdpsuif.dsp /MAKE "Bdpsuif - Win32 Debug" /REBUILD
rem msdev Bdpsuif.dsp /MAKE "Bdpsuif - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdpsuif.ilk del c:\Ufis_Bin\Debug\Bdpsuif.ilk
If exist c:\Ufis_Bin\Debug\Bdpsuif.pdb del c:\Ufis_Bin\Debug\Bdpsuif.pdb
cd ..\..

ECHO Build LanguageTool... > con
cd _Standard_share\_Standard_Languagetool
msdev CoCo3.dsp /MAKE "CoCo3 - Win32 Debug" /REBUILD
rem msdev CoCo3.dsp /MAKE "CoCo3 - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\CoCo3.ilk del c:\Ufis_Bin\Debug\CoCo3.ilk
If exist c:\Ufis_Bin\Debug\CoCo3.pdb del c:\Ufis_Bin\Debug\CoCo3.pdb
cd ..\..


REM ------------------------Flight--------------------------------------

ECHO Build Fips... > con
cd _Standard_flight\_Standard_Fips
msdev FPMS.dsp /MAKE "FPMS - Win32 Debug" /REBUILD
rem msdev FPMS.dsp /MAKE "FPMS - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\FIPS.ilk del c:\Ufis_Bin\Debug\FIPS.ilk
If exist c:\Ufis_Bin\Debug\FIPS.pdb del c:\Ufis_Bin\Debug\FIPS.pdb
cd ..\..

ECHO Build RulesFips... > con
cd _Standard_flight\_Standard_Ruleseditorflight
msdev Rules.dsp /MAKE "Rules - Win32 Debug" /REBUILD
rem msdev Rules.dsp /MAKE "Rules - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Rules.ilk del c:\Ufis_Bin\Debug\Rules.ilk
If exist c:\Ufis_Bin\Debug\Rules.pdb del c:\Ufis_Bin\Debug\Rules.pdb
cd ..\..

REM -----------------------Dutyroster-------------------------------------

ECHO Build Coverage... > con
cd _Standard_Dutyroster\_Standard_Coverage
msdev Coverage.dsp /MAKE "Coverage - Win32 Debug" /REBUILD
rem msdev Coverage.dsp /MAKE "Coverage - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Coverage.ilk del c:\Ufis_Bin\Debug\Coverage.ilk
If exist c:\Ufis_Bin\Debug\Coverage.pdb del c:\Ufis_Bin\Debug\Coverage.pdb
cd ..\..

ECHO Build Rostering... > con
cd _Standard_Dutyroster\_Standard_Rostering
rem msdev Rostering.dsp /MAKE "Rostering - Win32 Debug" /REBUILD
msdev Rostering.dsp /MAKE "Rostering - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\Rostering.ilk del c:\Ufis_Bin\Release\Rostering.ilk
If exist c:\Ufis_Bin\Release\Rostering.pdb del c:\Ufis_Bin\Release\Rostering.pdb
cd ..\..

REM ---------------------------RMS---------------------------------------------------

ECHO Build StaticGroupEditor... > con
cd _Standard_RMS\_Standard_Staticgroupeditor
msdev Grp.dsp /MAKE "Grp - Win32 Debug" /REBUILD
rem msdev Grp.dsp /MAKE "Grp - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Grp.ilk del c:\Ufis_Bin\Debug\Grp.ilk
If exist c:\Ufis_Bin\Debug\Grp.pdb del c:\Ufis_Bin\Debug\Grp.pdb
cd ..\..

ECHO Build ServiceCatalog... > con
cd _Standard_RMS\_Standard_Servicecatalog
msdev ServiceCatalog.dsp /MAKE "ServiceCatalog - Win32 Debug" /REBUILD
rem msdev ServiceCatalog.dsp /MAKE "ServiceCatalog - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\ServiceCatalog.ilk del c:\Ufis_Bin\Debug\ServiceCatalog.ilk
If exist c:\Ufis_Bin\Debug\ServiceCatalog.pdb del c:\Ufis_Bin\Debug\ServiceCatalog.pdb
cd ..\..

ECHO Build RulesRms... > con
cd _Standard_RMS\_Standard_RulesEditorRms
msdev Regelwerk.dsp /MAKE "Regelwerk - Win32 Debug" /REBUILD
rem msdev Regelwerk.dsp /MAKE "Regelwerk - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Regelwerk.ilk del c:\Ufis_Bin\Debug\Regelwerk.ilk
If exist c:\Ufis_Bin\Debug\Regelwerk.pdb del c:\Ufis_Bin\Debug\Regelwerk.pdb
cd ..\..

ECHO Build OpssPm... > con
cd _Standard_RMS\_Standard_Opsspm
msdev OpssPm.dsp /MAKE "OpssPm - Win32 Debug" /REBUILD
rem msdev OpssPm.dsp /MAKE "OpssPm - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\OpssPm.ilk del c:\Ufis_Bin\Debug\OpssPm.ilk
If exist c:\Ufis_Bin\Debug\OpssPm.pdb del c:\Ufis_Bin\Debug\OpssPm.pdb
cd ..\..

ECHO Build ProfileEditor... > con
cd _Standard_RMS\_Standard_ProfileEditor
msdev ProfileEditor.dsp /MAKE "ProfileEditor - Win32 Debug" /REBUILD
rem msdev ProfileEditor.dsp /MAKE "ProfileEditor - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\ProfileEditor.ilk del c:\Ufis_Bin\Debug\ProfileEditor.ilk
If exist c:\Ufis_Bin\Debug\ProfileEditor.pdb del c:\Ufis_Bin\Debug\ProfileEditor.pdb
cd ..\..

ECHO Build PoolAllocation... > con
cd _Standard_RMS\_Standard_PoolAloc
msdev PoolAlloc.dsp /MAKE "PoolAlloc - Win32 Debug" /REBUILD
rem msdev PoolAlloc.dsp /MAKE "PoolAlloc - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\PoolAlloc.ilk del c:\Ufis_Bin\Debug\PoolAlloc.ilk
If exist c:\Ufis_Bin\Debug\PoolAlloc.pdb del c:\Ufis_Bin\Debug\PoolAlloc.pdb
cd ..\..

ECHO Build WgrTool... > con
cd _Standard_Rms\_Standard_Wgrtool
msdev Wgrtool.dsp /MAKE "Wgrtool - Win32 Debug" /REBUILD
rem msdev Wgrtool.dsp /MAKE "Wgrtool - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Wgrtool.ilk del c:\Ufis_Bin\Debug\Wgrtool.ilk
If exist c:\Ufis_Bin\Debug\Wgrtool.pdb del c:\Ufis_Bin\Debug\Wgrtool.pdb
cd ..\..

ECHO ...Done > con

cd ..\..\BKK\BKK_Client\BKK_ClientMake

:end
Rem END
time/T

REM Visal Bacic Clients have to be build:
REM AATLoginControl - build from _Standard
REM TelexPool - build from _Standard
REM RosteringPrint - build from _Standard
REM LoadTabviewer - build from _Standard