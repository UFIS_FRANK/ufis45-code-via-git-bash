#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Bkk/BKK_Server/Base/Server/Rms/azehdl.c 1.1 2003/05/14 16:32:26SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : RSC                                                       */
/* Date           : 21.03.2000                                                */
/* Description    : Handles AZE telegramms for POPS project at FAG            */
/*                                                                            */
/* Update history : MCU 2002.05.17: ISLOCAL = TRUE in config file             */
/*                  prevents Local to Utc converting                          */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_version[] ="@(#) MIKE (c) ABB AAT/I azehdl.c 1.31 " __DATE__ "   " __TIME__ " / VBL / MCU";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "helpful.h"
#include "AATArray.h"
#include "send.h"

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = DEBUG;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static int   igQueOut;
static int   igNewLineSeperated = TRUE;
static int   igUseSQLHDL   = TRUE;                /* Use SQLHDL instead of CedaArray */
static int   igUseCANO     = FALSE;               /* Use CANO instead of 
                                                     PENO */
static char cgKeyField[12] = "PENO";
static int   igSQLHDLId    = 0;
static char  cgConfigFile[512];
static char  pcgHOPO[8]    = "";                  /* HOPO */
static char  pcgTabEnd[8]  = "";
static char  pcgTabUse[8]  = ""; 
static char  pcgHOPOUse[8] = "";

static char pcgRecvName[64]; /* BC-Head used as WKS-Name */
static char pcgDestName[64]; /* BC-Head used as USR-Name */
static char pcgTwStart[64];  /* BC-Head used as Stamp */   
static char pcgTwEnd[64];    /* BC-Head used as Stamp */   

static int bgTimeIsLocal = FALSE;               /* TRUE if time in import file is local */

static	char  	cgTdi1[24];
static	char  	cgTdi2[24];
static	char  	cgTich[24];



/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int      Init_azehdl();
static int	Reset(void);                       /* Reset program          */
static void	Terminate(void);                   /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static int	HandleData(void);                  /* Handles event data     */
static void     HandleQueues(void);                /* Waiting for Sts.-switch*/
static int      HandleAZE(char *pcpData);          /* Handles AZE data */
static int      CheckTableExt(char *pcpTblNam,     /* Checks, if the table
                                                      extension is correc */
                              char *pcpDefTblExt);

static int      InsertToSPR(char *pcpUseC,        /* Inserts new SPR record */
                            char *pcpUseU,
                            char *pcpPKNo,
                            char *pcpUSTF,
                            char *pcpAcTi,
                            char *pcpFCOL);


/* Telegram handlers: */
static int      HandleBU(char *pcpTelegram, 
                         int *pipTelegramSize);    /* Handles a single BU tel*/

static int      HandleST(char *pcpTelegram,
                         int *pipTelegramSize);    /* Handles a single ST tel*/

/* IN:  pcpTelegram:     Contains the telegram string including the 2 char
                         telegram identifier. There may be text after
                         the telegram data. DO NOT modify this string in own
                         handlers ! A handler MUST do a size check, if
                         pcpTelegram has the minimum size of the telegram.
                         If not, a handler must return a value different 
                         from RC_SUCCESS !

   OUT: pipTelegramSize: Ignored, if NULL. If not NULL the exact telegram
                         size is returned, including the 2 char telegram 
                         identifier. (e.g. 37 in HandleBU). */

static int	LocalToUtc(char *);
static int	UtcToLocal(char *);
static int ReadTich();



/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRC = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	
	INITIALIZE;			/* General initialization	*/
	dbg(TRACE,"MAIN: version <%s>",sccs_version);
	/* Attach to the MIKE queues */
	do
	{
		ilRC = init_que();
		if(ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */
	do
	{
		ilRC = init_db();
		if (ilRC != RC_SUCCESS)
		{
			check_ret(ilRC);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
		ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */
	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
	sprintf(cgConfigFile,"%s/azehdl",getenv("BIN_PATH"));
	ilRC = TransferFile(cgConfigFile);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */
	/* uncomment if necessary */
	sprintf(cgConfigFile,"%s/azehdl.cfg",getenv("CFG_PATH"));
	ilRC = TransferFile(cgConfigFile);
	if(ilRC != RC_SUCCESS)
	{
	 	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */
	ilRC = SendRemoteShutdown(mod_id);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */
	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */
	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRC = Init_azehdl();
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"Init_azehdl: init failed!");
			} /* end of if */
		}/* end of if */
	} else {
		Terminate();
	}/* end of if */
	dbg(TRACE,"MAIN: initializing OK");
	for(;;)
	{
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			} /* fi */
			
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate();
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate();
				break;
					
			case	RESET		:
				ilRC = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRC = HandleData();
					if(ilRC != RC_SUCCESS)
					{
						HandleErr(ilRC);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		} /* end else */
		
	} /* end for */
	
/*	exit(0); */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_azehdl()
{
  int  ilRC            = RC_SUCCESS;  /* Return code */
  char pclCfgLine[512] = "";

  /* now reading from configfile or from database */

  dbg(DEBUG, "Reading from configfile <%s>...", cgConfigFile);

  memset(pclCfgLine, '\0', 512);

  ilRC = iGetConfigEntry(cgConfigFile, 
                         "MAIN", 
                         "Newline_Seperated", 
                         CFG_STRING, 
                         pclCfgLine);
  if (ilRC != RC_SUCCESS)
  {
    dbg(TRACE, "error in MAIN-section of cfg file, reading <Newline_Seperated>: (%d) ! (TRUE used)", ilRC);
    igNewLineSeperated = TRUE;
    ilRC = RC_FAIL;
  } /* end of if */
  else
  {
    StringUPR((UCHAR*)pclCfgLine);
    if (strcmp(pclCfgLine, "TRUE") == 0)
    {
      igNewLineSeperated = TRUE;
    } /* end of if */
    else if (strcmp(pclCfgLine, "FALSE") == 0)
    {
      igNewLineSeperated = FALSE;
    } /* end of else-if */
    else
    {
      dbg(TRACE, "error in MAIN-section of cfg file, reading <Newline_Seperated>: not TRUE or FALSE ! (TRUE used)");
      igNewLineSeperated = TRUE;
    } /* end of else-if-else */
  } /* end of if */

  memset(pclCfgLine, '\0', 512);

  ilRC = iGetConfigEntry(cgConfigFile, 
                         "MAIN", 
                         "Debug_Level", 
                         CFG_STRING, 
                         pclCfgLine);

  if (ilRC != RC_SUCCESS)
  {
    dbg(TRACE, "error in MAIN-section of cfg file, reading <Debug_Level>: (%d) ! (DEBUG used)", ilRC);
    debug_level = DEBUG;
    ilRC = RC_FAIL;
  } /* end of if */
  else
  {
    StringUPR((UCHAR*)pclCfgLine);
    if (strcmp(pclCfgLine, "DEBUG") == 0)
    {
      debug_level = DEBUG;
    } /* end of if */
    else if (strcmp(pclCfgLine, "TRACE") == 0)
    {
      debug_level = TRACE;
    } /* end of else-if */
    else
    {
      dbg(TRACE, "error in MAIN-section of cfg file, reading <Debug_Level>: not DEBUG or TRACE ! (DEBUG used)");
      debug_level = DEBUG;
    } /* end of else-if-else */
  } /* end of if */

  if (igNewLineSeperated == TRUE)
  {
    dbg(TRACE, "Newline seperated mode");
  } /* end of if */
  else
  {
    dbg(TRACE, "Not newline seperated mode");
  } /* end of if-else */

  if (debug_level == DEBUG)
  {
    dbg(TRACE, "Debug level is DEBUG");
  } /* end of if */
  else
  {
    dbg(TRACE, "Debug level is TRACE");
  } /* end of if-else */

  memset(pclCfgLine, '\0', 512);

  ilRC = iGetConfigEntry(cgConfigFile, 
                         "MAIN", 
                         "Use_SQLHDL", 
                         CFG_STRING, 
                         pclCfgLine);
  if (ilRC != RC_SUCCESS)
  {
    dbg(TRACE, "error in MAIN-section of cfg file, reading <Use_SQLHDL>: (%d) ! (TRUE used)", ilRC);
    igUseSQLHDL = TRUE;
    ilRC = RC_FAIL;
  } /* end of if */
  else
  {
    StringUPR((UCHAR*)pclCfgLine);
    if (strcmp(pclCfgLine, "TRUE") == 0)
    {
      igUseSQLHDL = TRUE;
    } /* end of if */
    else if (strcmp(pclCfgLine, "FALSE") == 0)
    {
      igUseSQLHDL = FALSE;
    } /* end of else-if */
    else
    {
      dbg(TRACE, "error in MAIN-section of cfg file, reading <Use_SQLHDL>: not TRUE or FALSE ! (TRUE used)");
      igUseSQLHDL = TRUE;
    } /* end of else-if-else */
  } /* end of if */

  ilRC = iGetConfigEntry(cgConfigFile, 
                         "MAIN", 
                         "USE_CANO", 
                         CFG_STRING, 
                         pclCfgLine);
  if (ilRC != RC_SUCCESS)
  {
    dbg(TRACE, "error in MAIN-section of cfg file, reading <USE_CANO>: (%d) ! (TRUE used)", ilRC);
    igUseCANO = FALSE;
    ilRC = RC_FAIL;
  } /* end of if */
  else
  {
    StringUPR((UCHAR*)pclCfgLine);
    if (strcmp(pclCfgLine, "TRUE") == 0)
    {
      igUseCANO = TRUE;
			strcpy(cgKeyField,"CANO");
    } /* end of if */
    else if (strcmp(pclCfgLine, "FALSE") == 0)
    {
      igUseCANO = FALSE;
			strcpy(cgKeyField,"PENO");
    } /* end of else-if */
    else
    {
      dbg(TRACE, "error in MAIN-section of cfg file, reading <USE_CANO>: not TRUE or FALSE ! (TRUE used)");
      igUseSQLHDL = TRUE;
    } /* end of else-if-else */
  } /* end of if */

  if (igUseSQLHDL == TRUE)
  {
    memset(pclCfgLine, '\0', 512);

    ilRC = iGetConfigEntry(cgConfigFile, 
                           "MAIN", 
                           "SQLHDL_ID", 
                           CFG_STRING, 
                           pclCfgLine);
    if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE, "error in MAIN-section of cfg file, reading <SQLHDL_ID>: (%d) ! (7150 used)", ilRC);
      igSQLHDLId = 7150;
      ilRC = RC_FAIL;
    } /* end of if */
    else
    {
      igSQLHDLId = atoi(pclCfgLine);
      if (igSQLHDLId < 1000)
      {
        dbg(TRACE, "error in MAIN-section of cfg file, reading <SQLHDL_ID>: not valid ! (7150 used)");
      } /* end of else-if-else */
    } /* end of if */
  } /* end of if */

  if (igUseSQLHDL == TRUE)
  {
    dbg(TRACE, "Using SQLHDL for writing to DB, MOD_ID (%d)", igSQLHDLId);
  } /* end of if */
  else
  {
    dbg(TRACE, "Using CEDAArray for writing to DB");
  } /* end of if */

  pcgHOPO[0] = '\0'; 
  pcgTabEnd[0] = '\0';
 
  tool_search_exco_data("ALL", "TABEND", pcgTabEnd);
  tool_search_exco_data("SYS", "HOMEAP", pcgHOPO);
  ilRC = CheckTableExt("SYS", pcgTabEnd);                                
  if(ilRC != RC_SUCCESS)                                                  
  {                                                                       
    dbg(TRACE, "Error in initialization: Table <%s%s> not exists", "SYS", pcgTabEnd); 
    strcpy(pcgTabEnd, pcgHOPO);                                        
    dbg(TRACE, "Table extension corrected to: <%s>", pcgTabEnd);               
  }  /* end of if */

  StringUPR((UCHAR*)pcgHOPO);
  StringUPR((UCHAR*)pcgTabEnd);
  
  dbg(TRACE, "Using home <%s> ext <%s>", pcgHOPO, pcgTabEnd);
 
  dbg(DEBUG, "Initializing CEDAArray...");     
  ilRC = CEDAArrayInitialize(10, 10);
  if (ilRC != RC_SUCCESS)
  {                                            
    dbg(TRACE, "Failed to create CEDAArray !");
    ilRC = RC_FAIL;
    Terminate();
  } /* end of if */                            

	ReadTich();

	ilRC = iGetConfigEntry(cgConfigFile,"MAIN","ISLOCAL",
			CFG_STRING,pclCfgLine);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,"MAIN","ISLOCAL");
	  dbg(TRACE,"Using default: FALSE");
	} 
	else
	{
			dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
					"MAIN","ISLOCAL",pclCfgLine,cgConfigFile);
			if (strcmp(pclCfgLine,"TRUE") == 0)
			{
				bgTimeIsLocal = TRUE;
			}
	}

  igInitOK = TRUE;
  return(ilRC);
	
} /* end of initialize */

static void TrimRight(char *s)
{    /* search for last non-space character */
    int i = 0;
    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--);
	s[++i] = '\0'; /* trim off right spaces */    
}


static int ReadTich()
{
  int  ilRC           = RC_SUCCESS; 

	
	/** read TICH ***********/
		char pclDataArea[2256];
		char pclSqlBuf[256];
		short slCursor = 0;
		sprintf(pclSqlBuf,
			"SELECT TICH,TDI1,TDI2 FROM APTTAB WHERE APC3 = '%s'",pcgHOPO);
		ilRC = sql_if(START,&slCursor,pclSqlBuf,pclDataArea);
		if (ilRC != DB_SUCCESS)
		{
			dbg(TRACE,"Home Airport <%s> not found in APTTAB %d",pcgHOPO,ilRC);
			check_ret(ilRC);
		} /* end while */
		else
		{
			strcpy(cgTdi1,"60");
			strcpy(cgTdi2,"60");
			get_fld(pclDataArea,0,STR,14,cgTich);
			get_fld(pclDataArea,1,STR,14,cgTdi1);
			get_fld(pclDataArea,2,STR,14,cgTdi2);
			TrimRight(cgTich);
			if (*cgTich != '\0')
			{
				TrimRight(cgTdi1);
				TrimRight(cgTdi2);
				sprintf(cgTdi1,"%d",atoi(cgTdi1)*60);
				sprintf(cgTdi2,"%d",atoi(cgTdi2)*60);
			}
			dbg(TRACE,"Tich: <%s> Tdi1: <%s> Tdi2 <%s>",
				cgTich,cgTdi1,cgTdi2);
		}
		close_my_cursor(&slCursor);
		slCursor = 0;
	


  return(ilRC);
}


static int CheckTableExt(char *pcpTblNam, char *pcpDefTblExt)
{                                                                               
  int  ilRC           = RC_SUCCESS; 
  char pclSqlBuf[100] = "";
  char pclData[100]   = "";
  short slFkt         = START;
  short slCursor      = 0;
                                                                                
  sprintf(pclSqlBuf,"SELECT URNO FROM %s%s WHERE URNO=0",                       
                    pcpTblNam, pcpDefTblExt);                                   
  ilRC = sql_if(slFkt, &slCursor, pclSqlBuf, pclData);                          
  if (ilRC == DB_SUCCESS)                                                       
  {                                                                             
     ilRC = RC_SUCCESS;                                                         
  } /* end if */                                                                
  else                                                                          
  {                                                                             
    if (ilRC != NOTFOUND) /* NOTFOUND is ok Urno = 0 doesn\264t exist */                                                                             
    {
      ilRC = RC_FAIL;     /*  table doesn\264t exist (SQL ERROR) */ 
    }
    else                    
    {                       
      ilRC = RC_SUCCESS;   
    }                       
  } /* end else */           
                             
  close_my_cursor(&slCursor);
                             
  return ilRC;               

} /* end of CheckTableExt */ 

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRC = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRC;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{
	/* unset SIGCHLD ! DB-Child will terminate ! */
	logoff();
	dbg(TRACE,"Terminate: now leaving ...");
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
	switch(pipSig)
	{
	default	:
		Terminate();
		break;
	} /* end of switch */
	exit(0);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	int	ilRC = RC_SUCCESS;
	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	int	ilRC = RC_SUCCESS;
	
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate();
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				Terminate();
				break;
						
			case	RESET		:
				ilRC = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		} /* end else */
	} while (ilBreakOut == FALSE);
	if(igInitOK == FALSE)
	{
			ilRC = Init_azehdl();
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"Init_azehdl: init failed!");
			} /* end of if */
	}/* end of if */
	/* OpenConnection(); */
} /* end of HandleQueues */
	

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
  int      ilRC          = RC_SUCCESS; /* Return code */
  int      ilNoAck       = FALSE;                       
  BC_HEAD *prlBchd       = NULL;                        
  CMDBLK  *prlCmdblk     = NULL;                        
  char    *pclSelKey     = NULL;                        
  char    *pclFields     = NULL;                        
  char    *pclData       = NULL;                        
  char     pclHost[16]   = "";                          
  char     pclTable[16]  = "";                          
  char     pclActCmd[16] = "";                          
  char     pclChkCmd[16] = "";                          
  int      ilPos;                                       
                                                        
  dbg(TRACE, "----Starting AZEHDL Event----");          

  /* Que-ID of event sender, so data can be sent back */
  igQueOut = prgEvent->originator;                      

  /* Want answers */                                                   
  ilNoAck = FALSE;                                                     
                                                                       
  /* Get broadcast header */                                           
  prlBchd = (BC_HEAD *)((char *)prgEvent + sizeof(EVENT));             
                                                                       
  /* Save elements of BC_HEAD */                                       
  /* 1. RecvName -> WKS-Name */                                        
  memset(pcgRecvName, '\0', (sizeof(prlBchd->recv_name) + 1));         
  strncpy(pcgRecvName, prlBchd->recv_name, sizeof(prlBchd->recv_name));
                                                                       
  /* 2. DestName -> User login name */                                 
  strcpy(pcgDestName, prlBchd->dest_name);                             
                                                                       
  /* 3. OrigName -> Host */                                            
  strcpy(pclHost, prlBchd->orig_name);                                 
                                                                       
  /* Get command block */                                              
  prlCmdblk = (CMDBLK *)((char *)prlBchd->data);                       
                                                                       
  /* Save elements of CMDBLK */                                        
  /* 1. TwStart -> Stamp */                                            
  strcpy(pcgTwStart, prlCmdblk->tw_start);              
                                                        
  /* 2. TwEnd -> Stamp with Tabname, HOPO and other */  
  strcpy(pcgTwEnd, prlCmdblk->tw_end);                  
                                                        
  /* 3. ObjName -> Table */                             
  strcpy(pclTable, prlCmdblk->obj_name);                
                                                        
  /* 4. Command (THE command) */                        
  strcpy(pclActCmd, prlCmdblk->command);                
                                                        
  /* Getting SQL-Strings from CMDBLK */                 
  /* 1. Selection */                                    
  pclSelKey = (char *)prlCmdblk->data;                  
                                                        
  /* 2. Fields */                                       
  pclFields = (char *)pclSelKey + strlen(pclSelKey) + 1;
                                                        
  /* 3. Data */                                         
  pclData = (char *)pclFields + strlen(pclFields) + 1;  
                                                        
  if (strcmp(pclActCmd, "AZE") == 0) /* AZE telegram event received ? */
  { /* yes */
    ilRC = HandleAZE(pclData);
  } /* end if if */
  else if (strcmp(pclActCmd, "IRT") == 0) /* Reply from SQLHDL */
  {
    dbg(DEBUG, "Received reply from SQLHDL !");
  } /* end of else-if */
  else
  { /* no */
    dbg(TRACE, "Unknown command <%s> !", pclActCmd);
    dbg(TRACE, "Data : <%s>", pclData);
    ilRC = RC_FAIL;
  } /* end of else-if-else */ 
                                                       
  dbg(TRACE, "----AZEHDL Event complete----");          

  return ilRC;
	
} /* end of HandleData */



/******************************************************************************/
/*                                                                            */
/* HandleAZE() extracts the AZE telegrams from pcpData and Handles ech of it, */
/* using HandleBU() or HandleST().                                            */
/*                                                                            */
/* IN:        pcpData: The data field of the event, received by NFLHDL.       */
/*                     Contains the AZE telegram file newline seperated and   */
/*                     NULL terminated                                        */
/* OUT: -----                                                                 */
/*                                                                            */
/******************************************************************************/

static int HandleAZE(char *pcpData)
{
  /* AZE telegram format: first 2 chars identify the telegram type:
     "BU" : Coming or going registration (thats what we need)
     "ST" : Status check for dispo system (currently ignored)

     the rest of the telegram depends on the type see HandleBU and HandleST 
     commentary for the format description

     Newlines may be DOS newlines. That means \r\n. AZEHDL handles \r like
     \n. That means it find the double number of lines, where each 2nd line
     is empty. Empty lines are ignored... */


  int   ilRC              = RC_SUCCESS;
  int   ilPos             = 0;     /* actual position in string */
  int   ilLastNL          = -1;    /* position of last new line in newline
                                      seperated mode */
  char  pclTelegram[1024] = "";    /* temporarily receives the telegram line
                                      in newline seperated mode */
  int   ilTelegramSize    = 0;     /* receives the telegram size in not newline
                                      seperated mode */
  int   ilUCmd            = FALSE; /* Unknown command flag in not newline 
                                      seperated mode */

  dbg(DEBUG, "--Starting AZE work--");

  if (igNewLineSeperated == TRUE) /* newline seperated mode ? */
  { /* yes */

    ilPos = -1;
  
    do /* loops until end of pcpData, or error */
    {
      ilPos++;

      if ((pcpData[ilPos] == '\0') || 
         (pcpData[ilPos] == '\n') ||
         (pcpData[ilPos] == '\r'))     /* if DOS newlines */ 
      /* end of line ? */

      { /* yes */
        ilLastNL++; /* Telegram starts 1 char after last newline ! */

        memset(pclTelegram, '\0', 1024);
        strncpy(pclTelegram, &pcpData[ilLastNL], ilPos - ilLastNL);
        /* pclTelegram now contains the complete line, without newline char. */

        if (strlen(pclTelegram) > 0) /* empty line in file ? */
        { /* no */
          if (strlen(pclTelegram) < 2) /* minimum telegram size MUST be 2 chars
                                          (first 2 chars identify the type of
                                           the telegram) */
          {
            dbg(TRACE, "Wrong telegram size (%d) !", strlen(pclTelegram));
            ilRC = RC_FAIL;
          } /* end of if */
          else
          {
            if (strncmp(pclTelegram, "BU", 2) == 0) /* BU telegram ? */
            { /* yes */
              ilRC = HandleBU(pclTelegram, NULL);
              ilRC = RC_SUCCESS; /* Don't abort on single telegram failures */
            } /* end of if */
            else if (strncmp(pclTelegram, "ST", 2) == 0) /* ST telegram ? */
            { /* yes */
              ilRC = HandleST(pclTelegram, NULL);
              ilRC = RC_SUCCESS; /* Don't abort on single telegram failures */
            } /* end of else-if */
            else
            { /* any other telegram identifier */
              dbg(TRACE, "Unknown telegram type <%s> !", pclTelegram);
            } /* end of else-if-else */
          } /* end of if-else */
        } /* end of if */
        else
        { /* yes... ignore it */
          dbg(DEBUG, "empty line found...");
        } /* end of if-else */

        ilLastNL = ilPos; /* Next telegram starts at the end of the actual */
      } /* end of if */
    } while ((pcpData[ilPos] != '\0') && (ilRC == RC_SUCCESS)); 
  } /* end of if */

  else

  { /* Not newline seperated mode */
    ilPos = 0;

    while ((strlen(&pcpData[ilPos]) > 0) && (ilRC == RC_SUCCESS))
    /* till string ends or error occurs */
    {
      /* if there are any newlines: ignore them... */
      while ((pcpData[ilPos] == '\n') || (pcpData[ilPos] == '\r'))
      {
        ilPos++;
      } /* end of while */

      if (strlen(&pcpData[ilPos]) < 2) /* is there a telegram id minimum ? */
      { /* no */ 
        dbg(TRACE, "Wrong telegram size (%d) !", strlen(&pcpData[ilPos]));
        ilRC = RC_FAIL;
      } /* end of if */
      else
      { /* yes */
        ilUCmd = FALSE;
        if (strncmp(&pcpData[ilPos], "BU", 2) == 0) /* BU telegram ? */
        { /* yes */
          ilRC = HandleBU(&pcpData[ilPos], &ilTelegramSize);
        } /* end of if */
        else if (strncmp(&pcpData[ilPos], "ST", 2) == 0) /* ST telegram ? */
        { /* yes */
          ilRC = HandleST(&pcpData[ilPos], &ilTelegramSize);
        } /* end of else-if */
        else
        { /* unknwon command */
          ilUCmd = TRUE;
          dbg(TRACE, 
              "Unknown telegram type <%c%c> !", 
              pcpData[ilPos], 
              pcpData[ilPos + 1]);
          ilRC = RC_FAIL;
        } /* end of else-if-else */
        if ((ilRC != RC_SUCCESS) && 
           ((int)strlen(&pcpData[ilPos]) >= ilTelegramSize) &&
           (ilUCmd == FALSE))
        {
          ilRC = RC_SUCCESS; /* abort only if the telegram size is wrong,
                                its impossilbe to continue if this happens,
                                but continue if other failures on single
                                telegrams occur */
        } /* end of if */
        if (ilRC == RC_SUCCESS)
        {
          ilPos += ilTelegramSize; /* step to next telegram */
        } /* end of if */
      } /* end of if-else */

      /* if there are any newlines: ignore them: */
      while ((pcpData[ilPos] == '\n') || (pcpData[ilPos] == '\r'))
      {
        ilPos++;
      } /* end of while */

    } /* end of while */
    
  } /* end of if-else */

  dbg(DEBUG, "--AZE work complete--");

  return ilRC;
} /* end of HandleAZE */



/******************************************************************************/
/*                                                                            */
/* HandleBU() handles a single "BU" telegram. That means: Extracting fields   */
/* looking for PENO in STF table and insert new record to SPR table via       */
/* InsertSPR().                                                               */
/*                                                                            */
/******************************************************************************/

static int HandleBU(char *pcpTelegram, int *pipTelegramSize)
{
/* BU telegrams are the registrations of coming and going employees,
   so the thing we want.
 
   BU telegram format:
   pcpTelegram contains the BU telegram (MUST have a minimum of 37 chars !):

   char[2]  telegram identifier ("BU")
   char[8]  personal number
   char[2]  present flag: "  " not present (going)
                          "A " present (coming)
   char[8]  registration date ddmmyyyy
   char[6]  registration time hhmmss
   char     Time ID (daylight saving time flag, currently ignored)
   char[10] cost centre, ignored   */


  int ilRC = RC_SUCCESS;

  char   pclPNo[9]           = "00000000";  /* Personal number */
  char   pclSTFPNo[512]      = "";          /* PENO from STF table */
  char   pclPresent[2]       = "0";    /* 0=leaving, 1=coming */
  char   pclRegTime[15]      = "";     /* time and date of registration */
  char   clTimeID            = '\0';   /* Time ID of telegram */
  char   pclCostCentre[11]   = "";     /* Where the employee gets his money
                                          from, ignored */

  char   pclSTFURNO[3000]    = "";     /* receives the URNO of STF table record
                                          for the employee */

  HANDLE slSTFHandle         = -1;     /* Receives CEDAArray handle */
  char   pclSTFArrayName[12] = "STF";  /* Name of CEDAArray */
  char   pclSTFTabName[7]    = "STF";  /* Table name (table extension will be
                                          added later) */
  char   pclSTFSelect[512]   = "";     /* Selection, will be inserted later */
  char   pclSTFFieldList[512]= "URNO,PENO"; /* Fields needed */


  if (igUseCANO == TRUE)
	{
  	strcpy(pclSTFFieldList,"URNO,PENO,CANO");
	}
	else
	{
  	strcpy(pclSTFFieldList,"URNO,PENO");
	}

  if (pipTelegramSize != NULL)
  {
    *pipTelegramSize = 37;    /* return telegram size */
  } /* end of if */

  if (strlen(pcpTelegram) < 37) /* telegram too small ? */
  {
    dbg(TRACE, "BU telegram <%s> has wrong size (must be 37 chars) !", pcpTelegram);
    ilRC = RC_FAIL;
  } /* end of if */
  else
  {
    dbg(DEBUG, "extracting BU telegram fields...");

    pclPNo[8] = '\0';                   /* Extracting Personal number */
    memcpy(pclPNo, &pcpTelegram[2], 8);

    switch (pcpTelegram[10])            /* Extracting presence flag */
    {
      case ' ':
        pclPresent[0] = '0';
        break;
      case 'A':
        pclPresent[0] = '1';
        break;
      default:
        ilRC = RC_FAIL;
        dbg(TRACE, "Illegal presence state in BU telegram <%s> !", pcpTelegram);
        break;
    } /* end of switch */
    
    if (ilRC == RC_SUCCESS) 
    {
      pclRegTime[14] = '\0'; /* creating CEDA time from date/time fields */
      memcpy(pclRegTime, &pcpTelegram[12], 4);     /* copying year */
      memcpy(&pclRegTime[4], &pcpTelegram[16], 2); /* copying month */
      memcpy(&pclRegTime[6], &pcpTelegram[18], 2); /* copying day */
      memcpy(&pclRegTime[8], &pcpTelegram[20], 6); /* copying time */

      clTimeID = pcpTelegram[26];  /* extracting TimeID */

      pclCostCentre[10] = '\0';    /* extracting cost centre */
      memcpy(pclCostCentre, &pcpTelegram[27], 10);

      dbg(DEBUG, "extracting of telegram fields completed");

  if (igUseCANO == TRUE)
	{
      dbg(DEBUG, "Personal number: <%s>", pclPNo);
	}
	else
	{
      dbg(DEBUG, "TimeCard number: <%s>", pclPNo);
	}
      dbg(DEBUG, "Coming (1) or leaving (0): <%s>", pclPresent);
      dbg(DEBUG, "Time / date    : <%s>", pclRegTime);
      dbg(DEBUG, "TimeID         : <%c>", clTimeID);
      dbg(DEBUG, "Cost centre    : <%s>", pclCostCentre);


      /* building Selection: */
      if (strcmp(pcgTabEnd, "TAB") == 0) /* TAB modell ? */
      {
        sprintf(pclSTFSelect,       /* HOPO needs to be in WHERE clause */
                "WHERE HOPO='%s' AND %s='%s'", 
                 pcgHOPO,cgKeyField,pclPNo);
      } /* end of if */
      else
      {
        sprintf(pclSTFSelect,      /* No HOPO field in WHERE clause needed */
                "WHERE %s='%s'",cgKeyField,pclPNo);
      } /* end of if-else */

      strcat(pclSTFTabName, pcgTabEnd); /* Table name completed */

      dbg(DEBUG, 
          "Creating CEDAArray for <%s> with <%s> fields: <%s>...", 
          pclSTFTabName,
          pclSTFSelect,
          pclSTFFieldList);

      ilRC = CEDAArrayCreate(&slSTFHandle, 
                             pclSTFArrayName, 
                             pclSTFTabName,
                             pclSTFSelect,
                             NULL,
                             NULL,
                             pclSTFFieldList,
                             NULL,
                             NULL);

      if (ilRC != RC_SUCCESS)
      {
        dbg(TRACE, 
            "Fatal error ! Can't create CEDAArray for <%s> !", 
            pclSTFTabName);
      } /* end of if */
      else
      {
        dbg(DEBUG, "Creating CEDAArray successful");
        dbg(DEBUG, "Filling CEDAArray...");
        ilRC = CEDAArrayFill(&slSTFHandle, pclSTFArrayName, NULL);

        if (ilRC != RC_SUCCESS)
        {
          dbg(TRACE, 
              "Fatal error ! Can't fill CEDAArray from <%s> !", 
              pclSTFTabName);

        } /* end of if */
        else
        {
          dbg(DEBUG, "Filling CEDAArray successful");
          dbg(DEBUG, "Getting URNO field from CEDAArray...");
          ilRC = CEDAArrayGetField(&slSTFHandle, 
                                   pclSTFArrayName,
                                   NULL,
                                   "URNO",
                                   3000,
                                   ARR_FIRST,
                                   pclSTFURNO);
          /* URNO is needed to be placed into SPR table */
          if (ilRC == RC_SUCCESS)
          {
            dbg(DEBUG, "Got URNO field from CEDAArray: <%s>", pclSTFURNO);
            dbg(DEBUG, "Getting PENO field from CEDAArray...");
            ilRC = CEDAArrayGetField(&slSTFHandle, 
                                     pclSTFArrayName,
                                     NULL,
                                     "PENO",
                                     512,
                                     ARR_FIRST,
                                     pclSTFPNo);
            /* PENO from STF table is needed to be placed into SPR table. 
               Because there are 5 and 8 char versions of the Personal number
               the number from STF tab is used instead of the PeNo from
               the telegram, that is always 8 chars. */
          } /* end of if */
          if (ilRC != RC_SUCCESS)
          {
            dbg(TRACE, 
                "Error: can't get URNO or PENO field, so employee not found !");
          } /* end of if */
          else
          {
            dbg(DEBUG, "Got PENO field from CEDAArray: <%s>", pclSTFPNo);
						if (bgTimeIsLocal == FALSE)
						{
							  LocalToUtc(pclRegTime);
						}
            ilRC = InsertToSPR("azehdl",    /* Adding new record to SPR table */
                               "azehdl",
                               pclSTFPNo,
                               pclSTFURNO,
                               pclRegTime,
                               pclPresent);
          } /* end of if-else (CEDAArrayGetField) */

          ilRC = CEDAArrayDelete(&slSTFHandle, pclSTFArrayName);

        } /* end of else (CEDAArrayFill) */

        ilRC = CEDAArrayDestroy(&slSTFHandle, pclSTFArrayName);
        if (ilRC != RC_SUCCESS)
        {
          dbg(TRACE, "error, destroying CEDAArray !");
        } /* end of if */
      } /* end of if-else (CEDAArrayCreate) */

      if (ilRC == RC_SUCCESS)
      {
        dbg(DEBUG, "--> BU handling successful !");
      } /* end of if */
    } /* end of if */
  } /* end of if */

  return ilRC;
} /* end of HandleBU */



/******************************************************************************/
/*                                                                            */
/* HandleST() handles a single "ST" telegram. That means: Just ignore it !    */
/*                                                                            */
/******************************************************************************/

static int HandleST(char *pcpTelegram, int *pipTelegramSize)
{
/* ST telegrams are status check requests to the dispo system.
   Currently azehdl just ignores them.
 
   ST telegram format:
   pcpTelegram contains the ST telegram (MUST have a minimum of 2 chars !):

   char[2]  telegram identifier ("ST")
*/

  int ilRC = RC_SUCCESS;

  if (pipTelegramSize != NULL)
  {
    *pipTelegramSize = 2;
  } /* end of if */

  if (strlen(pcpTelegram) < 2)
  {
    dbg(TRACE, "ST telegram <%s> has wrong size !", pcpTelegram);
    ilRC = RC_FAIL;
  } /* end of if */
  else
  {
    dbg(DEBUG, "ST received... and ignored !");
  } /* end of if-else */

  return ilRC;
} /* end of HandleST */



/******************************************************************************/
/*                                                                            */
/* InsertSPR() addes a new record to SPR table.                               */
/*                                                                            */
/* IN:    pcpUseC: User, who created the record                               */
/*        pcpUseU: User, who updated the record                               */
/*        pcpPKNo: Personal number like in STF table                          */
/*        pcpUSTF: URNO of corresponding record in STF table                  */
/*        pcpAcTi: Time of registration (from telegram)                       */
/*        pcpFCOL: Coming (=1) or leaving (=0) flag                           */
/* OUT:   ------                                                              */
/*                                                                            */
/******************************************************************************/

static int InsertToSPR(char *pcpUseC, 
                       char *pcpUseU,
                       char *pcpPKNo,
                       char *pcpUSTF,
                       char *pcpAcTi,
                       char *pcpFCOL)
{
  int        ilRC                = RC_SUCCESS;

  char       pclADate[64]        = "";      /* Receives the actual time/date in
                                               CEDA format */
  char       pclURNO[3000]       = "";      /* Receives new URNO for new record                                              */
  long       llSec               = 0;       /* Receives the absolute number of
                                               seconds scince 01.01.1900 */
  struct tm *slTime              = NULL;    /* Receives pointer to extracted
                                               time information */

  HANDLE     slSPRHandle         = -1;      /* Handle for CEDAArray */
  char       pclSPRArrayName[12] = "SPR";   /* Name of CEDAArray */
  char       pclSPRTabName[7]    = "SPR";   /* Table name (table extension will
                                               be added later */
  char      *pclSPRFieldList     = NULL;    /* Field list, will be assigned 
                                               later */
  long       llRow               = ARR_FIRST; /* Temporary used row index */
  char       pclSPRRowData[2048] = "";      /* Receives data for new row */

  dbg(DEBUG, "Building date via time(), localtime() and sprintf()");

  time(&llSec);                  /* Get actual time */
  slTime = localtime(&llSec);    /* Extract it */
  sprintf(pclADate,              /* And make CEDA format out of it */
          "%04d%02d%02d%02d%02d%02d", 
          slTime->tm_year + 1900,
          slTime->tm_mon + 1,
          slTime->tm_mday,
          slTime->tm_hour,
          slTime->tm_min,
          slTime->tm_sec);

  dbg(DEBUG, "Getting new URNO via GetNextNumbers...");

  ilRC = GetNextNumbers("SNOTAB", pclURNO, 1, "GNV");

  if (ilRC != RC_SUCCESS)
  {
    dbg(TRACE, "Error getting URNO with GetNextNumbers !");
  } /* emd of if */
  else
  {
    dbg(DEBUG, "Got new URNO: <%s> !", pclURNO);

    dbg(DEBUG, "Building data string for CEDAArrayPutFields...");

    strcpy(pclSPRRowData, pclURNO);    /* URNO */

    if (strcmp(pcgTabEnd, "TAB") == 0)
    {
      strcat(pclSPRRowData, ",");
      strcat(pclSPRRowData, pcgHOPO);  /* HOPO */
    } /* end of if */

    strcat(pclSPRRowData, ",");
    strcat(pclSPRRowData, pclADate);   /* CDAT */

    strcat(pclSPRRowData, ",");
    strcat(pclSPRRowData, pclADate);   /* LSTU */

    strcat(pclSPRRowData, ",");
    strcat(pclSPRRowData, pcpUseC);    /* USEC */

    strcat(pclSPRRowData, ",");
    strcat(pclSPRRowData, pcpUseU);    /* USEU */

    strcat(pclSPRRowData, ",");
    strcat(pclSPRRowData, pcpPKNo);    /* PKNO */

    strcat(pclSPRRowData, ",");
    strcat(pclSPRRowData, pcpUSTF);    /* USTF */

    strcat(pclSPRRowData, ",");
    strcat(pclSPRRowData, pcpAcTi);    /* ACTI */

    strcat(pclSPRRowData, ",");
    strcat(pclSPRRowData, pcpFCOL);    /* FCOL */

    /* assigning field list: */
    if (strcmp(pcgTabEnd, "TAB") == 0) /* tab model ? */
    {
      /* HOPO is needed */
      pclSPRFieldList = "URNO,HOPO,CDAT,LSTU,USEC,USEU,PKNO,USTF,ACTI,FCOL";
    } /* end of if */
    else
    {
      /* no HOPO needed */
      pclSPRFieldList = "URNO,CDAT,LSTU,USEC,USEU,PKNO,USTF,ACTI,FCOL";
    } /* end of if-else */

    strcat(pclSPRTabName, pcgTabEnd); /* complete table name */

    if (igUseSQLHDL == TRUE)  /* Using SQLHDL ? */
    { /* yes */
      dbg(DEBUG, 
          "Sending to SQLHDL (%d): Cmd: <IRT>, table: <%s>, FieldList: <%s>, Dtata: <%s>...",
          igSQLHDLId,
          pclSPRTabName,
          pclSPRFieldList,
          pclSPRRowData);

      ilRC = SendCedaEvent(igSQLHDLId,
                           0,
                           "azehdl",
                           pcgRecvName,
                           "",
                           "",
                           "IRT",
                           pclSPRTabName,
                           "",
                           pclSPRFieldList,
                           pclSPRRowData,
                           "",
                           0,
                           RC_SUCCESS);
      if (ilRC != RC_SUCCESS)
      {
        dbg(TRACE, "Error sending <IRT> to SQLHDL (%d) !", igSQLHDLId);
      } /* end of if */
      else
      {
        dbg(DEBUG, "Event sended correctly to SQLHDL");
      } /* end of if-else */
    }
    else
    { /* Use CEDAArray */
      dbg(DEBUG, 
          "Creating CEDAArray for <%s> fields: <%s>...", 
          pclSPRTabName,
          pclSPRFieldList);

      ilRC = CEDAArrayCreate(&slSPRHandle,
                             pclSPRArrayName,
                             pclSPRTabName,
                             "",
                             NULL,
                             NULL,
                             pclSPRFieldList,
                             NULL,
                             NULL); 
      if (ilRC != RC_SUCCESS)
      {
        dbg(TRACE, "Error creating CEDAArray  for <%s> !", pclSPRTabName);
      } /* end of if */
      else
      {
        dbg(DEBUG, "Creating CEDAArray successful");
     
        dbg(DEBUG, "Adding row to CEDAArray...");
 
        llRow = ARR_FIRST;
        ilRC = CEDAArrayAddRow(&slSPRHandle, 
                               pclSPRArrayName,
                               &llRow,
                               NULL);
        if (ilRC != RC_SUCCESS)
        {
          dbg(TRACE, "Error adding row to CEDAArray !");
        } /* end of if */
        else
        {
          dbg(DEBUG, "Adding row to CEDAArray successful");
  
          llRow = ARR_FIRST;       

          dbg(DEBUG, 
              "Putting fileds to CEDAArray: <%s> to <%s>...", 
              &pclSPRRowData,
              &pclSPRFieldList);

          ilRC = CEDAArrayPutFields(&slSPRHandle,
                                    pclSPRArrayName,
                                    NULL,
                                    &llRow,
                                    pclSPRFieldList,
                                    pclSPRRowData);
          if (ilRC != RC_SUCCESS)
          {
            dbg(TRACE, "Error putting fileds to CEDAArray");
          } /* end of if */
          else
          {
            dbg(DEBUG, "Fields putted to CEDAArray");
            dbg(DEBUG, "Writing to <%s>...", pclSPRTabName);
            ilRC = CEDAArrayWriteDB(&slSPRHandle,
                                    pclSPRArrayName,
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL,
                                    ARR_COMMIT_TILL_ERR);
            if (ilRC != RC_SUCCESS)
            {
              dbg(TRACE, "Error writing to <%s> !", pclSPRTabName);
            } /* end of if */
            else
            {
              dbg(DEBUG, "Writing to <%s> successful", pclSPRTabName);
            } /* end of if */
          } /* end of if-else */
          ilRC = CEDAArrayDelete(&slSPRHandle, pclSPRArrayName);
          if (ilRC != RC_SUCCESS)
          {
            dbg(TRACE, "Error deleting CEDAArray !");
          } /* end of if */
        } /* end of if-else */

        ilRC = CEDAArrayDestroy(&slSPRHandle, pclSPRArrayName);
        if (ilRC != RC_SUCCESS)
        {
          dbg(TRACE, "Error destroying CEDAArray !");
        } /* end of if */
      } /* end of if-else */
    } /* end of if-else (UseSQLHDL) */
  } /* end of if-else */

  return ilRC;

} /* end of InsertToSPR */

static int	LocalToUtc(char *);
static int	UtcToLocal(char *);

static int LocalToUtc(char *pcpTime)
{
	int ilRc = RC_SUCCESS;
	char *pclTdi;
	char clLocal[32];

	strcpy(clLocal,pcpTime);
	pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;

	AddSecondsToCEDATime(pcpTime,(time_t)0-atol(pclTdi),1);
	dbg(TRACE,"2UTC  : from <%s> to <%s> ",clLocal,pcpTime);

	return ilRc;
}

static int UtcToLocal(char *pcpTime)
{
	int ilRc = RC_SUCCESS;
	char *pclTdi;
	char clUtc[32];

	strcpy(clUtc,pcpTime);
	pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;

	AddSecondsToCEDATime(pcpTime,(time_t)0+atol(pclTdi),1);
	dbg(TRACE,"2LOCAL  : <%s> ",pcpTime);

	return ilRc;
}


