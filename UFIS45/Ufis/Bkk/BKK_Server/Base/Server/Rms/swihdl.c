#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Bkk/BKK_Server/Base/Server/Rms/swihdl.c 1.1 2003/05/14 16:32:26SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/
/*                                                                           */
/* ABB ACE/FC Program Skeleton                                               */
/*                                                                           */
/* Author         : rkl                                                      */
/* Date           : 22.12.99                                                 */
/* Description    : Swissport Interface Handler                              */
/*                  Personaldaten ; PAX-Prognosedaten                        */
/*                                                                           */
/* Function       : HandlePersonal() - Bearbeitung Mitarbeiterstammdaten     */
/*                    HandleSTF(),HandleADR(),HandleCON()                    */
/*                  HandleMUA() - Bearbeitung Mutationen Adressdaten         */
/*                  HandleMUC() - Bearbeitung Mutationen Vertragsdaten       */
/*                  HandleMOR() - Bearbeitung Monatsabschluss                */
/*                  HandlePGN() - Bearbeitung Prognose Daten                 */
/*                                                                           */
/* Update history : 22.12.99 rkl written                                     */
/*                  04.05.00 rkl HandleSTF() ohne Telefon dienstl.           */
/*                  16.05.00 rkl read initial debug_level from configfile    */
/*                  xx.03.2001 smi did something                             */
/*                                                                           */
/*                  10.04.2001 eth removed re-sending of adr data            */
/*                             eth remove "detached" data fields             */
/*                             eth re-send SalaerCode in field FunctionCode  */
/*                                                                           */
/*                  18.06.2001 eth changed format of date                    */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/* source-code-control-system version string                                 */
static char sccs_swihdl_c[] ="@(#) UFIS 4.4 (c) ABB AAT/I swihdl.c 44.14 / 01/03/13 14:08:24 / RKL";

/* be carefule with strftime or similar functions !!!                        */
/*                                                                           */
/*****************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <dirent.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "fditools.h"
#include "buffer.h"
#include "send.h"
#include "swihdl.h"
#include "cedatime.h"
#include "syslib.h"
#include "helpful.h"


#define FOR_NORMAL 0
#define FOR_UPDATE 1
#define FOR_INSERT 2
#define FOR_SEARCH 3
#define FOR_TABLES 4

#define FIELD_STR_LEN 4096


/*****************************************************************************/
/* External variables                                                        */
/*****************************************************************************/
FILE           *outp = NULL;
int             debug_level = TRACE;
/*****************************************************************************/
/* Global variables                                                          */
/*****************************************************************************/
static ITEM    *prgItem = NULL;	/* The queue item pointer  */
static EVENT   *prgEvent = NULL;/* The event pointer       */
static int      igItemLen = 0;	/* length of incoming item */
static int      igInitOK = FALSE;
static char     pcgConfigFile[512];

static short    sgModId = 0;	/* queue ID of this process    */
static int      igQueOut = 0;	/* queue ID of event sender    */
static int      igToBcHdl = 0;	/* queue ID broadcast handler  */
static int      igToLogHdl = 0;	/* queue ID Log handler        */
static int      igToStaHdl = 0;	/* queue ID Status handler     */
static int      igToAction = 0;	/* queue ID Action handler     */
static int      igToFtphdl = 0;	/* queue ID FTP handler     */
static int      igAdrUp = TRUE;
static int      igDayofMUC = 2; /* Day when MUC is accepted */
static short    asgRoutes[10];
static char     pcgOutRoute[64];

static char     pcgH3LC[8] = "";		/* home port                   */
static char     pcgDefTblExt[8] = "";	/* default table extension     */
static char     pcgDefH3LC[8] = "";		/* default home port           */

static char     pcgRecvName[64] = "";	/* BC-Head used as WKS-Name    */
static char     pcgDestName[64] = "";	/* BC-Head used as UserName    */
static char     pcgTwStart[64] = "";	/* BC-Head used as Stamp       */
static char     pcgTwEnd[64] = "";		/* BC-Head used as Stamp       */

static char     pcgPrsImpPath[512];
static char     pcgPrsExpPath[512];
static char     pcgPrsArcPath[512];
static char     pcgStammDatFile[512];
static char     pcgMutAdrFile[512];
static char     pcgMutConFile[512];
static char     pcgMonthReportFile[512];
static char     pcgPrsFilter[128];
static char     pcgPrsFilterPosition[16];
static char     pcgFuncCodes[128];
static int       igPrsFilterPosition = 0;
static int       igPrsFilterFlag = FALSE;

static char    *pcgResultBuffer = NULL;	/* Pointer to global Result Buffer */
static int      igAlcResBufSize = 0;	/* Allocated Buffersize in Byte    */
static int      igUsedResBufSize = 0;	/* Used Buffersize in Byte         */


/*****************************************************************************/
/* Function prototypes	                                                     */
/*****************************************************************************/
static int      Init_swihdl();
static int      Reset(void);	/* Reset program          */
static void     Terminate(void);/* Terminate program      */
static void     HandleSignal(int);	/* Handles signals        */
static void     HandleErr(int);	/* Handles general errors */
static void     HandleQueErr(int);	/* Handles queuing errors */
static int      HandleData(void);	/* Handles event data     */
static void     HandleQueues(void);	/* Waiting for Sts.-switch */

/*****************************************************************************/
/* Function prototypes	                                                     */
/*****************************************************************************/
static int      ReadCfg(char *, char *, char *, char *, char *);
static void     CheckHomePort(char *, char *);
static int      CheckTableExt(char *, char *);
static int      HandlePersonal(void);
static int      HandleSTF(char*,char*,int*);
static int      HandleADR(char*,char*);
static int      HandleCON(char*, char*);
static int      HandleMUA(void);
static int      HandleMUC(void);
static int      HandleMOR(void);
static int      HandlePGN(char*);
static int      Del2NewDel(char *, char, char);
static int      BuildSqlFldStr(char *, char *, int, char *);
static int      CountDelimiter(char *, char *);
static int      GetSubStrg(char *, int, int, int, char *);
static int      UpdateShm (char *, char *, char *);
static void     AddMinToCedaTime(char *,char *,char *);
static int      GetUrnos (char*,char*,char*,char*);
static int 		GetFlightUrnos (char*,char*,char*,char*,char*,char*,char**,long*);
static int 		GetFlightDay (char*,char*,char*,char*,char*,char*,char**,long*);
static int 		GetUrnoByFkey (char*,char*,char*,char*,char*,char*);
static int 		FormatDayFreq(char*,char*);

extern int      StrgPutStrg(char *, int *, char *, int, int, char *);
extern int      BuildItemBuffer(char *, char *, int, char *);

/*****************************************************************************/
/*                                                                           */
/* The MAIN program                                                          */
/*                                                                           */
/*****************************************************************************/
MAIN
{
	int             ilRC = RC_SUCCESS;	/* Return code			 */
	int             ilCnt = 0;

	INITIALIZE;		/* General initialization	 */
	dbg(TRACE, "MAIN: version <%s>", sccs_swihdl_c);
	/* Attach to the MIKE queues */
	do
	{
		ilRC = init_que();
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE, "MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}		/* end of if */
	} while ((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE, "MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	} else
	{
		dbg(TRACE, "MAIN: init_que() OK! mod_id <%d>", mod_id);
	}			/* end of if */
	do
	{
		ilRC = init_db();
		if (ilRC != RC_SUCCESS)
		{
			check_ret(ilRC);
			dbg(TRACE, "MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}		/* end of if */
	} while ((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE, "MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	} else
	{
		dbg(TRACE, "MAIN: init_db() OK!");
	}			/* end of if */
	/*
	 * logon to DB is ok, but do NOT use DB while ctrl_sta ==
	 * HSB_COMING_UP !!!
	 */

	/* transfer binary file to remote machine... */
	sprintf(pcgConfigFile, "%s/swihdl", getenv("BIN_PATH"));
	ilRC = TransferFile(pcgConfigFile);
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE, "MAIN: %05d TransferFile: <%s> failed!", __LINE__, pcgConfigFile);
		set_system_state(HSB_STANDALONE);
	}			/* end of if */
	/* transfer configuration file to remote machine... */
	sprintf(pcgConfigFile, "%s/swihdl.cfg", getenv("CFG_PATH"));
	ilRC = TransferFile(pcgConfigFile);
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE, "MAIN: %05d TransferFile: <%s> failed!", __LINE__, pcgConfigFile);
		set_system_state(HSB_STANDALONE);
	}			/* end of if */
	/* ask VBL about this!! */
	dbg(DEBUG, "<MAIN> %05d SendRemoteShutdown: %d", __LINE__, mod_id);
	if ((ilRC = SendRemoteShutdown(mod_id)) != RC_SUCCESS)
	{
		dbg(TRACE, "<MAIN> SendRemoteShutdown(%d) returns: %d", mod_id, ilRC);
		set_system_state(HSB_STANDALONE);
	}
	if ((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG, "MAIN: waiting for status switch ...");
		HandleQueues();
	}			/* end of if */
	if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE, "MAIN: initializing ...");
		if (igInitOK == FALSE)
		{
			ilRC = Init_swihdl();
			if (ilRC != RC_SUCCESS)
			{
				dbg(TRACE, "Init_swihdl: init failed!");
			}	/* end of if */
		}		/* end of if */
	} else
	{
		Terminate();
	}			/* end of if */
	dbg(TRACE, "MAIN: initializing OK");
	for (;;)
	{
		ilRC = que(QUE_GETBIG, 0, mod_id, PRIORITY_3, igItemLen, (char *) &prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;

		if (ilRC == RC_SUCCESS)
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK, 0, mod_id, 0, 0, NULL);
			if (ilRC != RC_SUCCESS)
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			}	/* fi */
			switch (prgEvent->command)
			{
			case HSB_STANDBY:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;
			case HSB_COMING_UP:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;
			case HSB_ACTIVE:
				ctrl_sta = prgEvent->command;
				break;
			case HSB_ACT_TO_SBY:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;
			case HSB_DOWN:
				/*
				 * whole system shutdown - do not further use
				 * que(), send_message() or timsch() !
				 */
				ctrl_sta = prgEvent->command;
				Terminate();
				break;
			case HSB_STANDALONE:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;
			case REMOTE_DB:
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case SHUTDOWN:
				/* process shutdown - maybe from uutil */
				Terminate();
				break;

			case RESET:
				ilRC = Reset();
				break;

			case EVENT_DATA:
				if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRC = HandleData();
					if (ilRC != RC_SUCCESS)
					{
						HandleErr(ilRC);
					}	/* end of if */
				} else
				{
					dbg(TRACE, "MAIN: wrong hsb status <%d>", ctrl_sta);
					DebugPrintItem(TRACE, prgItem);
					DebugPrintEvent(TRACE, prgEvent);
				}	/* end of if */
				break;

			case TRACE_ON:
				dbg_handle_debug(prgEvent->command);
				break;
			case TRACE_OFF:
				dbg_handle_debug(prgEvent->command);
				break;
			default:
				dbg(TRACE, "MAIN: unknown event");
				DebugPrintItem(TRACE, prgItem);
				DebugPrintEvent(TRACE, prgEvent);
				break;
			}	/* end switch */
		} else
		{
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		}		/* end else */

	}			/* end for */

	exit(0);

}				/* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_swihdl()
{
  char           *pclFct = "Init_swihdl";
  int             ilRC = RC_SUCCESS;	/* Return code */
  int             ilRCCfg = RC_SUCCESS;
  char            pclTmpStrg[256];
  char            pclTmpBuf[256];
  char            *pclPtr;

  /* now reading from configfile */
  dbg(TRACE, "%s: Configfile: <%s>", pclFct, pcgConfigFile);

  /* -------------------------------------------------------------- */
  /* debug_level setting ------------------------------------------ */
  /* -------------------------------------------------------------- */
  pclTmpBuf[0] = 0x00;
  if ((ilRC = iGetConfigEntry(pcgConfigFile, "MAIN", "debug_level", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
    {
      /* default */
      dbg(DEBUG,"<init_swihdl> no debug_level in section <MAIN>");
      strcpy(pclTmpBuf, "OFF");
    }

  /* which debug_level is set in the Configfile ? */
  StringUPR((UCHAR*)pclTmpBuf);
  if (!strcmp(pclTmpBuf,"DEBUG"))
    {
      debug_level = DEBUG;
      dbg(TRACE,"<init_swihdl> debug_level set to DEBUG");
    }
  else if (!strcmp(pclTmpBuf,"TRACE"))
    {
      debug_level = TRACE;
      dbg(TRACE,"<init_swihdl> debug_level set to TRACE");
    }
  else if (!strcmp(pclTmpBuf,"OFF"))
    {
      debug_level = 0;
      dbg(TRACE,"<init_swihdl> debug_level set to OFF");
    }
  else
    {
      debug_level = 0;
      dbg(TRACE,"<init_swihdl> unknown debug_level set to default OFF");
    }

  /* Update Adressdata ? */
  igAdrUp = TRUE; /* erstmal ja ... */
  if ((ilRC = iGetConfigEntry(pcgConfigFile, "MAIN", "adr_up", CFG_STRING, pclTmpBuf))
            != RC_SUCCESS)
  {
    /* default */
    dbg(DEBUG,"<init_swihdl> no adr_up in section <MAIN>");
    dbg(DEBUG,"<init_swihdl> default is TRUE");
  }
  else
  {
    StringUPR((UCHAR*)pclTmpBuf);
    if (!strcmp(pclTmpBuf,"NO"))
    {
      igAdrUp = FALSE;
    }
  }

  if (igAdrUp == FALSE){
    dbg(DEBUG,"<init_swihdl> adr_up in section <MAIN> is <NO>");
  } else {
    dbg(DEBUG,"<init_swihdl> adr_up in section <MAIN> is <YES>");
  }

  /* read filepathes from configfile */
  ilRCCfg = ReadCfg(pcgConfigFile, "FILEPATH", "PRSIMPPATH",
                 "/home/ceda/exco/PERSONAL", pcgPrsImpPath);
  dbg(TRACE, "%s: Import Pfad: <%s>", pclFct, pcgPrsImpPath);

  ilRCCfg = ReadCfg(pcgConfigFile, "FILEPATH", "PRSEXPPATH",
                 "/home/ceda/exco/PERSONAL", pcgPrsExpPath);
  dbg(TRACE, "%s: Export Pfad: <%s>", pclFct, pcgPrsExpPath);

  ilRCCfg = ReadCfg(pcgConfigFile, "FILEPATH", "PRSARCPATH",
          "/home/ceda/exco/PERSONAL/archiv", pcgPrsArcPath);
  dbg(TRACE, "%s: Archiv Pfad: <%s>", pclFct, pcgPrsArcPath);

  /* read filenames from configfile */
  ilRCCfg = ReadCfg(pcgConfigFile, "FILENAMES", "STAMMDAT", "",pcgStammDatFile);
  dbg(TRACE, "%s: Personalstammdatendatei : <%s>", pclFct, pcgStammDatFile);

  ilRCCfg = ReadCfg(pcgConfigFile, "FILENAMES", "MUTADR", "", pcgMutAdrFile);
  dbg(TRACE, "%s: Mutationen Adressdaten  : <%s>", pclFct, pcgMutAdrFile);

  ilRCCfg = ReadCfg(pcgConfigFile, "FILENAMES", "MUTCON", "", pcgMutConFile);
  dbg(TRACE, "%s: Mutationen Vertragsdaten: <%s>", pclFct, pcgMutConFile);

  ilRCCfg = ReadCfg(pcgConfigFile, "FILENAMES", "MONREP", "", pcgMonthReportFile);
  dbg(TRACE, "%s: Monatsabschschluss : <%s>", pclFct, pcgMonthReportFile);

  /* read the functioncode */
  ilRCCfg = ReadCfg(pcgConfigFile, "MUC", "FCODES", "", pcgFuncCodes);
  if (ilRCCfg != RC_SUCCESS)
  {
    strcpy(pcgFuncCodes,"230");
    dbg(TRACE, "%s: Benutze default Funktionscode : <230>", pclFct);
  }
  else
  {
    pclPtr = pcgFuncCodes;
    strcpy(pclTmpStrg,"'");
    while (*pclPtr != 0x00)
    {
      GetNextDataItem(pclTmpBuf,&pclPtr,",","","  ");
      strcat(pclTmpStrg,pclTmpBuf);
      strcat(pclTmpStrg,"','");
    }
    pclTmpStrg[strlen(pclTmpStrg)-2] = 0x00;
    strcpy(pcgFuncCodes,pclTmpStrg);
    dbg(TRACE, "%s: Funktionscodes : <%s>", pclFct, pcgFuncCodes);
  } /* end else */

  /* read the "Erstellungs" day */
  ilRCCfg = ReadCfg(pcgConfigFile, "MUC", "DAY", "", pclTmpStrg);
  if (ilRCCfg != RC_SUCCESS)
  {
    igDayofMUC = 2;
    dbg(TRACE, "%s: Benutze default Day : <2>", pclFct);
  }
  else
  {
    if ((igDayofMUC = atoi(pclTmpStrg)) == 0)
    {
	  igDayofMUC = 2;
	  dbg(TRACE, "%s: Benutze default Day : <2>", pclFct);
    }
    else
    {
      if (igDayofMUC == 32)
      {
	    dbg(TRACE,"%s: Test mode Create MUC at any day of month: <%d>", pclFct,igDayofMUC);
      }
      else
      {
	    dbg(TRACE, "%s: Create MUC at day of month  : <%d>", pclFct, igDayofMUC);
      }
    }
  }



  ilRCCfg = ReadCfg(pcgConfigFile, "FILTER", "PRS_FILTER", "", pcgPrsFilter);
  dbg(TRACE, "%s: FilterStrg : <%s>", pclFct, pcgPrsFilter);
  if(strcmp(pcgPrsFilter,"") == 0)
    {
      igPrsFilterFlag = FALSE;
      dbg(TRACE,"%s: No PersonalFilter set",pclFct);
    }
  else
    {
      igPrsFilterFlag = TRUE;
		
	   
      ilRCCfg = ReadCfg(pcgConfigFile, "FILTER", "PRS_FILTER_POSITION", "", pcgPrsFilterPosition);
      if(ilRCCfg == RC_SUCCESS)
	{
	  igPrsFilterPosition = atoi(pcgPrsFilterPosition);
	  dbg(TRACE,"%s: Personal Filter set to <%s> at Position <%d>",
	      pclFct, pcgPrsFilter, igPrsFilterPosition);
		
	}
      else
	{   
	  igPrsFilterFlag = FALSE;
	  dbg(TRACE, "%s: ERROR - No Personal Filter Position", pclFct);
	  ilRC = RC_FAIL;
	}
    }
	
	
  /* Initialize Global Resultbuffer */
  dbg(TRACE, "%s: Initialize", pclFct);
  dbg(TRACE, "%s: Allocating global Buffer", pclFct);
  ilRC = HandleBuffer(&pcgResultBuffer, &igAlcResBufSize, igUsedResBufSize,
		      (512 * 1024), (64 * 1024));

  pcgDefTblExt[0] = 0x00;
  pcgDefH3LC[0] = 0x00;
  ilRC = tool_search_exco_data("ALL", "TABEND", pcgDefTblExt);
  ilRC = tool_search_exco_data("SYS", "HOMEAP", pcgDefH3LC);
  ilRC = CheckTableExt("SYS", pcgDefTblExt);
  if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE, "%s: ERROR SGS CONFIGURATION! Table <%s%s> not exists",
	  pclFct, "SYS", pcgDefTblExt);
      strcpy(pcgDefTblExt, pcgDefH3LC);
      dbg(TRACE, "%s: TABLE EXT CORRECTED TO: <%s>", pclFct, pcgDefTblExt);
      ilRC = RC_FAIL;
    }
  dbg(TRACE, "%s: DEFAULTS: HOME <%s> EXT <%s>", pclFct, pcgDefH3LC, pcgDefTblExt);


  if (ilRC == RC_SUCCESS)
    {
      igToBcHdl = tool_get_q_id("bchdl");

      igToLogHdl = tool_get_q_id("loghdl");

      igToStaHdl = tool_get_q_id("stahdl");

      igToAction = tool_get_q_id("action");

      igToFtphdl = tool_get_q_id("ftphdl");

      dbg(TRACE, "OUT QUEUES:");
      dbg(TRACE, "BCHDL=%d LOGHDL=%d ACTION=%d STAHDL=%d FTPHDL=%d",
	  igToBcHdl, igToLogHdl, igToAction, igToStaHdl,igToFtphdl);
      sprintf(pcgOutRoute, "%d,%d", igToAction, igToLogHdl);
    }			/* end if */
  ilRC = SendStatus("PROCESS", "", "START", 1, sccs_swihdl_c, "", "");


  igInitOK = TRUE;
  return (ilRC);

} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int             ilRC = RC_SUCCESS;	/* Return code */

	dbg(TRACE, "Reset: now resetting");

	return ilRC;

}				/* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void 
Terminate()
{
	/* unset SIGCHLD ! DB-Child will terminate ! */
	logoff();
	dbg(TRACE, "Terminate: now leaving ...");

	exit(0);

}				/* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
	int             ilRC = RC_SUCCESS;	/* Return code */
	dbg(TRACE, "HandleSignal: signal <%d> received", pipSig);
	switch (pipSig)
	{
	default:
		Terminate();
		break;
	}			/* end of switch */
	exit(0);

}				/* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	int             ilRC = RC_SUCCESS;
	return;
}				/* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	int             ilRC = RC_SUCCESS;

	switch (pipErr)
	{
	case QUE_E_FUNC:	/* Unknown function */
		dbg(TRACE, "ERROR - <%d> : unknown function", pipErr);
		break;
	case QUE_E_MEMORY:	/* Malloc reports no memory */
		dbg(TRACE, "ERROR - <%d> : malloc failed", pipErr);
		break;
	case QUE_E_SEND:	/* Error using msgsnd */
		dbg(TRACE, "ERROR - <%d> : msgsnd failed", pipErr);
		break;
	case QUE_E_GET:	/* Error using msgrcv */
		dbg(TRACE, "ERROR - <%d> : msgrcv failed", pipErr);
		break;
	case QUE_E_EXISTS:
		dbg(TRACE, "ERROR - <%d> : route/queue already exists ", pipErr);
		break;
	case QUE_E_NOFIND:
		dbg(TRACE, "ERROR - <%d> : route not found ", pipErr);
		break;
	case QUE_E_ACKUNEX:
		dbg(TRACE, "ERROR - <%d> : unexpected ack received ", pipErr);
		break;
	case QUE_E_STATUS:
		dbg(TRACE, "ERROR - <%d> :  unknown queue status ", pipErr);
		break;
	case QUE_E_INACTIVE:
		dbg(TRACE, "ERROR - <%d> : queue is inaktive ", pipErr);
		break;
	case QUE_E_MISACK:
		dbg(TRACE, "ERROR - <%d> : missing ack ", pipErr);
		break;
	case QUE_E_NOQUEUES:
		dbg(TRACE, "ERROR - <%d> : queue does not exist", pipErr);
		break;
	case QUE_E_RESP:	/* No response on CREATE */
		dbg(TRACE, "ERROR - <%d> : no response on create", pipErr);
		break;
	case QUE_E_FULL:
		dbg(TRACE, "ERROR - <%d> : too many route destinations", pipErr);
		break;
	case QUE_E_NOMSG:	/* No message on queue */
		/* dbg(TRACE, "ERROR - <%d> : no messages on queue", pipErr); */
		break;
	case QUE_E_INVORG:	/* Mod id by que call is 0 */
		dbg(TRACE, "ERROR - <%d> : invalid originator=0", pipErr);
		break;
	case QUE_E_NOINIT:	/* Queues is not initialized */
		dbg(TRACE, "ERROR - <%d> : queues are not initialized", pipErr);
		break;
	case QUE_E_ITOBIG:
		dbg(TRACE, "ERROR - <%d> : requestet itemsize to big ", pipErr);
		break;
	case QUE_E_BUFSIZ:
		dbg(TRACE, "ERROR - <%d> : receive buffer to small ", pipErr);
		break;
	default:		/* Unknown queue error */
		dbg(TRACE, "ERROR - <%d> : unknown error", pipErr);
		break;
	}			/* end switch */

	return;
}				/* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int             ilRC = RC_SUCCESS;	/* Return code */
	int             ilBreakOut = FALSE;

	do
	{
		ilRC = que(QUE_GETBIG, 0, mod_id, PRIORITY_3, igItemLen, (char *) &prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
		if (ilRC == RC_SUCCESS)
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK, 0, mod_id, 0, 0, NULL);
			if (ilRC != RC_SUCCESS)
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			}	/* fi */
			switch (prgEvent->command)
			{
			case HSB_STANDBY:
				ctrl_sta = prgEvent->command;
				break;

			case HSB_COMING_UP:
				ctrl_sta = prgEvent->command;
				break;

			case HSB_ACTIVE:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;
			case HSB_ACT_TO_SBY:
				ctrl_sta = prgEvent->command;
				break;

			case HSB_DOWN:
				/*
				 * whole system shutdown - do not further use
				 * que(), send_message() or timsch() !
				 */
				ctrl_sta = prgEvent->command;
				Terminate();
				break;

			case HSB_STANDALONE:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;
			case REMOTE_DB:
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case SHUTDOWN:
				Terminate();
				break;

			case RESET:
				ilRC = Reset();
				break;

			case EVENT_DATA:
				dbg(TRACE, "HandleQueues: wrong hsb status <%d>", ctrl_sta);
				DebugPrintItem(TRACE, prgItem);
				DebugPrintEvent(TRACE, prgEvent);
				break;

			case TRACE_ON:
				dbg_handle_debug(prgEvent->command);
				break;
			case TRACE_OFF:
				dbg_handle_debug(prgEvent->command);
				break;
			default:
				dbg(TRACE, "HandleQueues: unknown event");
				DebugPrintItem(TRACE, prgItem);
				DebugPrintEvent(TRACE, prgEvent);
				break;
			}	/* end switch */
		} else
		{
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		}		/* end else */
	} while (ilBreakOut == FALSE);
	if (igInitOK == FALSE)
	{
		ilRC = Init_swihdl();
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE, "Init_swihdl: init failed!");
		}		/* end of if */
	}			/* end of if */
	/* OpenConnection(); */
}				/* end of HandleQueues */


/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
	char           *pclFct = "HandleData";
	int             ilRC = RC_SUCCESS;	/* Return code */
	int             ilRCSend = RC_SUCCESS;	/* Return code */
	int             ilAddHopo = FALSE;
	int             ilAddCdat = FALSE;
	int             ilNoAck = FALSE;
	int             ilUpdShm = TRUE;
	BC_HEAD        *prlBchd = NULL;
	CMDBLK         *prlCmdblk = NULL;
	char            pclTable[16] = "";
	char            pclActCmd[16] = "";
	char            pclChkCmd[16] = "";
	char           *pclHlpPtr = NULL;
	int             i;
	static char    *pclSelect = NULL;	/* Selectionbuffer            */
	static char    *pclFields = NULL;	/* Fieldlistbuffer            */
	static char    *pclData = NULL;	/* Databuffer                 */
	static int      ilSelectSize = 0;
	static int      ilFieldsSize = 0;
	static int      ilDataSize = 0;
	char            pclH3LC[8] = "";
	long            llNoOfChgRec = 0;
	int             ilStrgLen = 0;




	dbg(TRACE, "=== SWIHDL EVENT BEGIN =================");

	/* Reset ResultBuffer */
	pcgResultBuffer[0] = 0x00;
	igUsedResBufSize = 0;

	/* Queue-id of event sender */
	igQueOut = prgEvent->originator;

	/* get broadcast header */
	prlBchd = (BC_HEAD *) ((char *) prgEvent + sizeof(EVENT));

	/* Save Global Elements of BC_HEAD */
	memset(pcgRecvName, '\0', (sizeof(prlBchd->recv_name) + 1));	/* Workstation */
	strcpy(pcgRecvName, "swihdl");
	strcpy(pcgDestName, "EXCO");	/* UserLoginName */

	/* get command block  */
	prlCmdblk = (CMDBLK *) ((char *) prlBchd->data);

	/* Save Global Elements of CMDBLK */
	strcpy(pcgTwStart, prlCmdblk->tw_start);
	strcpy(pcgTwEnd, prlCmdblk->tw_end);

	/* 3 Strings in CMDBLK -> data  */

	/* GET SELECTION */
	pclHlpPtr = (char *) prlCmdblk->data;
	ilStrgLen = strlen(pclHlpPtr);
	ilRC = HandleBuffer(&pclSelect, &ilSelectSize, 0, (ilStrgLen + 1), 256);
	strcpy(pclSelect, pclHlpPtr);

	/* GET FIELDS */
	pclHlpPtr = pclHlpPtr + strlen(pclSelect) + 1;
	ilStrgLen = strlen(pclHlpPtr);
	ilRC = HandleBuffer(&pclFields, &ilFieldsSize, 0, (ilStrgLen + 1), 256);
	strcpy(pclFields, pclHlpPtr);

	/* GET DATA  */
	pclHlpPtr = (char *) pclHlpPtr + strlen(pclFields) + 1;
	ilStrgLen = strlen(pclHlpPtr);
	ilRC = HandleBuffer(&pclData, &ilDataSize, 0, (ilStrgLen + 1), 256);
	strcpy(pclData, pclHlpPtr);

	strcpy(pclTable, prlCmdblk->obj_name);          /* table name  */
	strcpy(pclActCmd, prlCmdblk->command);          /* actual command  */
	sprintf(pclChkCmd, ",%s,", prlCmdblk->command); /* for strstr ident */

	dbg(TRACE, "CMD <%s> TBL <%s>", prlCmdblk->command, prlCmdblk->obj_name);
	dbg(TRACE, "FROM <%d> WKS <%s> USR <%s>",
	    igQueOut, prlBchd->recv_name, prlBchd->dest_name);

	strncpy(pcgTwStart, prlCmdblk->tw_start, sizeof(prlCmdblk->tw_start));
	pcgTwEnd[sizeof(prlCmdblk->tw_start)] = 0x00;
	strncpy(pcgTwEnd, prlCmdblk->tw_end, sizeof(prlCmdblk->tw_end));
	pcgTwEnd[sizeof(prlCmdblk->tw_end)] = 0x00;

	dbg(TRACE, "SPECIAL INFO TWS <%s> TWE <%s>", pcgTwStart, pcgTwEnd);
	dbg(TRACE, "SEL <%s>", pclSelect);
	dbg(TRACE, "FLD <%s>", pclFields);

	if ((ilStrgLen = strlen(pclData)) < 512)
		dbg(TRACE, "DAT <%s>", pclData);
	else
		dbg(TRACE, "DATA = %d Bytes > 512 Bytes => not printed", ilStrgLen);

	/* Add HOPO to SQL Strings?  */
	ilAddHopo = TRUE;
	dbg(TRACE, "ADD HOPO is set to TRUE");

	strcpy(pclH3LC, pcgDefH3LC);
	CheckHomePort(pcgTwEnd, pclH3LC);
	strcpy(pcgH3LC, pclH3LC);	/* for SYS-Command */

	/* get acknowlege status  */
	if (prlBchd->rc == NETOUT_NO_ACK)
	{
		ilNoAck = TRUE;
	} /* end if */ 
	else
	{
		ilNoAck = FALSE;
	} /* end else */


	if (strstr(",IMP,STF,", pclChkCmd) != NULL)
	{
		ilRC = HandlePersonal();     /* Bearbeitung Mitarbeiterstammdaten Import */
	}
	else if (strstr(",MUA,", pclChkCmd) != NULL)
	{
		ilRC = HandleMUA();          /* Bearbeitung Mutationen Adressdaten */
	}
	else if (strstr(",MUC,", pclChkCmd) != NULL)
	{
		ilRC = HandleMUC();          /* Bearbeitung Mutationen Vertragsdaten */
	}
	else if (strstr(",MOR,", pclChkCmd) != NULL)
	{
		ilRC = HandleMOR();          /* Bearbeitung Monatsabschluss */
	}
	else if (strstr(",PGN,", pclChkCmd) != NULL)
	{
   		ilRC = HandlePGN(pclData);   /* Bearbeitung Prognose Daten */
	}
	else
	{
		dbg(TRACE, "%s: ERROR - Wrong command <%s>", pclFct, pclActCmd);
		ilRC = RC_FAIL;
	} /* end else */

	dbg(TRACE, "=== SWIHDL EVENT END  =================");

	return ilRC;

}				/* end of HandleData */


/*****************************************************************************/
/* Function:   ReadCfg()                                                     */
/*****************************************************************************/
/* Parameter:                                                                */
/* In    : char *pcpFile           - Filename                                */
/* In    : char *pcpSection        - Section-Name in File                    */
/* In    : char *pcpEntry		   - Entry-Name in File                      */
/* In    : char *pcpDefValue       - Defaultvalue                            */
/* Out   : char *pcpValue          - Config Value                            */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure no value -> default value copied        */
/*                                                                           */
/* Description: read in file in Section Value of Entry;                      */
/*              if no Entryvalue  defaultvalue will be set                   */
/*                                                                           */
/*                                                                           */
/* History:                                                                  */
/*           22.12.99  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int ReadCfg(char *pcpFile, char *pcpSection, char *pcpEntry,
	               char *pcpDefValue, char *pcpValue)
{
	char  *pclFct = "ReadCfg";
	int     ilRC  = RC_SUCCESS;

	ilRC = iGetConfigRow(pcpFile, pcpSection, pcpEntry, CFG_STRING, pcpValue);
	if (ilRC != RC_SUCCESS || strlen(pcpValue) <= 0)
	{
	    dbg(TRACE,"%s: WARNING - Entry <%s> not Found using Default <%s>",
		          pclFct,pcpEntry,pcpDefValue);
		strcpy(pcpValue, pcpDefValue);
		ilRC = RC_FAIL;
	} /* end if */
	
	/*
	 * dbg(TRACE,"SECTION: <%s>  ENTRY: <%s> VALUE: <%s>",
	 * pcpSection,pcpEntry,pcpValue);
	 */
	return ilRC;

} /* end of ReadCfg */

 
/*****************************************************************************/
/* Function:   CheckHomePort()                                               */
/*****************************************************************************/
/* Parameter:                                                                */
/* In    : char  *pcpTwEnd       - special string                            */
/* Out   : char  *pcpH3LC        - home port                                 */
/*                                                                           */
/* Global Variables:                                                         */
/* In    : char  *pcgDefH3LC     - Default Home Port                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - All Ok                                          */
/* 				RC_FAIL    - Failed                                          */
/*                                                                           */
/* Description:                                                              */
/*              Takes first item of special string for home port             */
/*                                                                           */
/*                                                                           */
/* History:                                                                  */
/*           04.05.99  twe  Written                                          */
/*                                                                           */
/*****************************************************************************/
static void CheckHomePort(char *pcpTwEnd, char *pcpH3LC)
{
	char           *pclFct = "CheckHomePort";
	int             ilHomLen = 0;
	char            pclDel[5] = ",";
	char           *pclItem = NULL;

	/* take first item of special string for home port  */
	pclItem = pcpTwEnd;
	ilHomLen = GetNextDataItem(pcpH3LC, &pclItem, pclDel, "\0", "  ");
	if (ilHomLen != 3)
	{
		strcpy(pcpH3LC, pcgDefH3LC);	/* take default  */
	}			/* end if */
	dbg(DEBUG, "%s: EXTENSION SET TO <%s>", pclFct, pcgDefTblExt);
	dbg(DEBUG, "%s: HOMEPORT SET TO <%s>", pclFct, pcpH3LC);

}				/* end CheckHomePort */

/*****************************************************************************/
/* Function:   CheckTableExt()                                               */
/*****************************************************************************/
/* Parameter:                                                                */
/* In    : char  *pcpTblNam      - Table Name                                */
/* Out   : char  *pcpDefTblExt   - Default Table Extension                   */
/* */
/* Global Variables:                                                         */
/* */
/* Returncode:  RC_SUCCESS - All Ok                                          */
/*              RC_FAIL    - Failed                                          */
/* */
/* Description:                                                              */
/*              Selects URNO=0 in table+ext of DB (check if table exists)    */
/* */
/* */
/* History:                                                                  */
/*           04.05.99  twe  Written                                          */
/* */
/*****************************************************************************/
static int CheckTableExt(char *pcpTblNam, char *pcpDefTblExt)
{
	char    *pclFct = "CheckTableExt";
	int       ilRC = RC_SUCCESS;
	char     pclSqlBuf[100] = "";
	char     pclData[100] = "";
	short     slFkt = START;
	short     slCursor = 0;

	sprintf(pclSqlBuf, "SELECT URNO FROM %s%s WHERE URNO=0",
		pcpTblNam, pcpDefTblExt);
	ilRC = sql_if(slFkt, &slCursor, pclSqlBuf, pclData);
	if (ilRC == RC_SUCCESS)
	{
		ilRC = RC_SUCCESS;
	}
	 /* end if */ 
	else
	{
		if (ilRC != SQL_NOTFOUND)	/* NOTFOUND is ok Urno = 0 doesn't exist */
		{
			ilRC = RC_FAIL;	/* table doesn't exist (SQL ERROR)  */
		} 
		else
		{
			ilRC = RC_SUCCESS;
		}
	}			/* end else */

	close_my_cursor(&slCursor);

	return ilRC;
} /* end of CheckTableExt */


/*****************************************************************************/
/* Function:   HandlePersonal()                                                   */
/*****************************************************************************/
/* Parameter:                                                                */
/*                                                                           */
/* Global Variables:                                                         */
/* Read  : char  *pcgStammDatFile   - filename stammdata                     */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/* RC_FAIL    - Failure                                                      */
/*                                                                           */
/* Description: Insert or Update Records from File in Table STFTAB           */
/*              Updates Shared Memory for Swissport                          */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*          22.12.99  rkl  Written                                           */
/*                                                                           */
/*****************************************************************************/
static int HandlePersonal(void)
{
  char    *pclFct = "HandlePersonal";
  int       ilRC     = RC_SUCCESS;
  int       ilCount  = 0;
  int       ilMatch  = FALSE;
  int       ilUpdate = FALSE;
	
  DIR		*prlAktDir = NULL;
  struct dirent *prlDirent = NULL;
	
  FILE    *fp_stm = NULL;
  char     pclCmd[8] = "";
  char     pclFileName[1024];
  char     pclPathFile[1024];
  char     pclTmpStrg[1024];
  char     pclErrMsg[512];
		
  char     pclCedaTimestamp[24];
  char    *pclCPtr = NULL;
  char    *pclFltPtr = NULL;
  char     pclDummy[6];

  dbg(TRACE,"%s: Starte Import Personaldaten",pclFct);

  ilRC = GetCedaTimestamp(&pclCedaTimestamp[0], 15, 0);


  /* open this directory... */
  dbg(DEBUG,"%s: open directory <%s>",pclFct,pcgPrsImpPath);
  if ((prlAktDir = opendir(pcgPrsImpPath)) == NULL)
    {
      dbg(TRACE,"%s: opendir <%s> returns NULL", pclFct,pcgPrsImpPath);
      return RC_FAIL;
    }

  /* search file */
  while ((prlDirent = readdir(prlAktDir)) != NULL)
    {
            
      /* don't use .xxxx files (.profile, .exrc, .cshrc, ...) */

      if ((prlDirent->d_name)[0] == '.')
	{
	  dbg(DEBUG,"<HandleCommand> continue because file is <%s>", prlDirent->d_name);
	  continue;
	}

      /* check file -> match it pattern? */
      dbg(DEBUG,"<HandleCommand> compare <%s> and <%s>", 
	  prlDirent->d_name, pcgStammDatFile);

      if ((ilRC = MatchPattern(prlDirent->d_name, pcgStammDatFile)) == 1)
	{
	  /* found file... */
	  dbg(DEBUG,"%s: ...MATCH...",pclFct);
	  dbg(TRACE,"%s: found file <%s>", pclFct,prlDirent->d_name);
	  ilMatch = TRUE;
	
	  /* clear Filename */
	  strcpy(pclFileName,prlDirent->d_name);
	  pclPathFile[0] = 0x00;
	  /* build path/filename... */
	  strcpy(pclPathFile, pcgPrsImpPath);
	  if (pclPathFile[strlen(pclPathFile)-1] != '/')
	    strcat(pclPathFile, "/");

	  strcat(pclPathFile, pclFileName);

	  if(ilMatch==TRUE)
	    {
	      dbg(DEBUG,"%s: Path and File <%s>",pclFct,pclPathFile);
	      fp_stm = fopen(pclPathFile, "r");
	      if (fp_stm != NULL)
		{
		  /* Read until end of line or max. 1024 chars*/
		  while (fgets(pclTmpStrg, 1024, fp_stm) != NULL)
		    {
		      if(strncmp(pclTmpStrg,"END ",4) == 0)
             		break;

		      /* Pruefung ob Komma in den Daten -> Umwandlung nach Semicolon */
		      do
			{
			  if((pclCPtr = strstr(pclTmpStrg,",")) != NULL)
			    {
			      /* dbg(TRACE,"Komma in Data");*/
			      *pclCPtr = ';';
			      /* dbg(TRACE,"<%s>",pclTmpStrg);*/
			      /* dbg(TRACE,"Replace Komma -> Semicolon");*/
			    }
			} while (pclCPtr != NULL);
				
		      dbg(TRACE,"%s: ------------ Start/End -------------" ,pclFct);
		      dbg(TRACE,"%s: Dataline: <%s> " ,pclFct,pclTmpStrg);
				
		      pclFltPtr = pclTmpStrg;
		      if (igPrsFilterFlag == TRUE)
			{
			  pclFltPtr += igPrsFilterPosition;

              /* das ist der Inhalt des Feldes PKZ ...
                 nur die ersten beiden werden benoetigt
                 z.B. BZ fuer Zuerich oder BL fuer Basel */

              strncpy(pclDummy, pclFltPtr, 2);
              pclDummy[2]='\0';

			  dbg(DEBUG,"%s: search for <%s> in <%s>", pclFct, pclDummy, pcgPrsFilter);

/* Baustelle eth 23.05.2001 */

			  /* if((strncmp(pclFltPtr,pcgPrsFilter,strlen(pcgPrsFilter))) == 0) */

                if (strstr(pcgPrsFilter, pclDummy) != NULL)
			    {
			      dbg(DEBUG,"%s: Filter found",pclFct);
			      /* Verarbeite Mitarbeiterdaten */
			      ilRC = HandleSTF(pclTmpStrg,pclCedaTimestamp,&ilUpdate);
			      dbg(DEBUG," update is <%d>",ilUpdate);
			      if (ilUpdate == FALSE || igAdrUp == TRUE)
                  {
            		/* Verarbeite Adressdaten */            
					ilRC = HandleADR(pclTmpStrg,pclCedaTimestamp);
			      } else {
					dbg(DEBUG,"%s: Employee in DB do not update Adressdata",pclFct);
			      }
    	        	/* Verarbeite Vertragsdaten */            
			      ilRC = HandleCON(pclTmpStrg,pclCedaTimestamp);
			    }
				else
			    {
			      dbg(DEBUG,"%s: Filter fail",pclFct);
			    }
			}
		      else
			{
			  /* Verarbeite Mitarbeiterdaten */
    	    	  	  ilRC = HandleSTF(pclTmpStrg,pclCedaTimestamp,&ilUpdate);
			  if (ilUpdate == FALSE || igAdrUp == TRUE){
			    /* Verarbeite Adressdaten */            
			    ilRC = HandleADR(pclTmpStrg,pclCedaTimestamp);
			  }
    	        	  /* Verarbeite Vertragsdaten */            
	            	  ilRC = HandleCON(pclTmpStrg,pclCedaTimestamp);
					
			}
			
		    } /* end of while */


		} 
	      else
		{
		  dbg(TRACE, "%s: ERROR - Can't open File <%s>", pclFct, pclFileName);
		  ilRC = RC_FAIL;
		}

	      fclose(fp_stm);
	      /* build path/filename... */
	      strcpy(pclTmpStrg, pcgPrsArcPath);
	      if (pclTmpStrg[strlen(pclTmpStrg)-1] != '/')
		strcat(pclTmpStrg, "/");

	      strcat(pclTmpStrg, pclFileName);
		
	      ilRC = rename(pclPathFile,pclTmpStrg);
	      if(ilRC == -1)
		{
		  dbg(TRACE,"%s: ERROR - Moving File <%s> => <%s>",
		      pclFct,pclPathFile,pclTmpStrg);
		  ilRC = RC_FAIL;
		}
	      else
      		{
		  dbg(TRACE,"%s: Move File <%s> => <%s>",
		      pclFct,pclPathFile,pclTmpStrg);
		}

	    }
	  /*else
	   * {
	   * 	dbg(TRACE,"%s: ERROR - No File matched",pclFct);
	   * 	ilRC = RC_FAIL;
	   * }
	   */
	}

    } /* end of while */

  /* close the directory */
  if (prlAktDir != NULL)
    {
      dbg(DEBUG,"%s: close directory...",pclFct);
      closedir(prlAktDir);
      prlAktDir = NULL;
    }


  dbg(TRACE,"%s: Fertig Import Personaldaten",pclFct);

  return ilRC;


} /* end of HandlePersonal() */


/*****************************************************************************/
/* Function:   HandleSTF()                                                   */
/*****************************************************************************/
/* Parameter:                                                                */
/* IN    : char *pcpDataStrg        - Datastring from File                   */
/* IN    : char *pcpCedaTimestamp   - Ceda Time for LSTU,CDAT                */
/*                                                                           */
/* Global Variables:                                                         */
/* Read  : char  *pcgStammDatFile   - filename stammdata                     */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/* RC_FAIL    - Failure                                                      */
/*                                                                           */
/* Description: Insert or Update Records from File in Table STFTAB           */
/*              Updates Shared Memory for Swissport                          */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*          22.12.99  rkl  Written                                           */
/*                                                                           */
/*****************************************************************************/
static int HandleSTF(char *pcpDataStrg,char *pcpCedaTimestamp, int *pipUpdate)
{
  char    *pclFct = "HandleSTF";
  char    *pclStfTable = "STFTAB";
  /* mit Telefon dienstlich 	
   *	char    *pclStfFld = "LANM,FINM,GEBU,KIND,NATI,PENO,DOBK,DODM,DOEM,TELD";
   */
  char    *pclStfFld = "LANM,FINM,GEBU,KIND,NATI,PENO,DOBK,DODM,DOEM";
  char    *pclStfWhere = "WHERE PENO=:VPENO";
  int       ilRC     = RC_SUCCESS;
  int       ilRCSend = RC_SUCCESS;
  int       ilSqlRC  = RC_SUCCESS;
  int       ilRCSel  = RC_SUCCESS;
  int       ilLen = 0;
  int 	  ilItmLen = 0;
  short     slCurSel = 0;
  short     slCurUpd = 0;
  short     slCurIns = 0;
  short     slFkt = START;
  char    *pclItem= NULL;
  char     pclCmd[8] = "";
  char     pclTmpStrg[1024];
  char     pclSqlBuf[4096];
  char     pclSqlData[4096];
  char     pclWhereClause[128];
  char     pclErrMsg[512];
  char     pclFields[2048];
  char     pclFldData[4096];
  char     pclNewUrno[64];
  char     pclUrno[64];
		

  STAMM    pclStamm;
  

  *pipUpdate = FALSE;
  /*
   * dbg(TRACE,"%s:  pcpDataStrg: <%s> " ,pclFct,pcpDataStrg);
   */
	 
  /* pruefen sind Personaldaten fuer die STFTAB im DataStrg */			
  if(strncmp(pcpDataStrg,"                                     ",30) == 0)
    {
      /* keine Personaldaten fuer STFDAT */
      dbg(TRACE,"%s:ERROR -  Kein Name vorhanden: <%s> " ,pclFct,pcpDataStrg);
      ilRC = RC_FAIL;
      return ilRC;
    }
  else
    {
      /* Personaldaten vorhanden */ 
      ilRC = GetSubStrg(pcpDataStrg,  0,26, TRUE, pclStamm.lanm);
      ilRC = GetSubStrg(pcpDataStrg, 26,26, TRUE, pclStamm.finm);
      ilRC = GetSubStrg(pcpDataStrg, 52, 8, TRUE, pclStamm.gebu);
      ilRC = GetSubStrg(pcpDataStrg, 60, 1, TRUE, pclStamm.kind);
      ilRC = GetSubStrg(pcpDataStrg, 61, 3, TRUE, pclStamm.nati);
      ilRC = GetSubStrg(pcpDataStrg, 64, 6, TRUE, pclStamm.peno);
      ilRC = GetSubStrg(pcpDataStrg,158, 8, TRUE, pclStamm.dobk);
      if((strcmp(pclStamm.dobk,"        ")== 0) ||
	 (strcmp(pclStamm.dobk,"")        == 0) || 
	 (strcmp(pclStamm.dobk,"00000000")== 0))
	{
	  strcpy(pclStamm.dobk," ");
	}
      else
	{   
	  strcat(pclStamm.dobk,"000000");
	}
      ilRC = GetSubStrg(pcpDataStrg,166, 8, TRUE, pclStamm.dodm);
      if((strcmp(pclStamm.dodm,"        ")== 0) || 
	 (strcmp(pclStamm.dodm,"")        == 0) || 
	 (strcmp(pclStamm.dodm,"00000000")== 0))
	{
	  strcpy(pclStamm.dodm," ");
	}
      else
	{   
	  strcat(pclStamm.dodm,"000000");
	}

      ilRC = GetSubStrg(pcpDataStrg,174, 8, TRUE, pclStamm.doem);
      if((strcmp(pclStamm.doem,"        ")== 0) || 
	 (strcmp(pclStamm.doem,"")        == 0) || 
	 (strcmp(pclStamm.doem,"00000000")== 0))
	{
	  strcpy(pclStamm.doem," ");
	}
      else
	{   
	  strcat(pclStamm.doem,"000000");
	}
      ilRC = GetSubStrg(pcpDataStrg,218,60, TRUE, pclStamm.teld);

      /*
       * TRY UPDATE STFTAB
       */
      strcpy(pclCmd,"URT");
      BuildSqlFldStr(pclStfFld,"USEU,LSTU", FOR_UPDATE, pclFields);
			
      dbg(DEBUG,"%s: FldList <%s>",pclFct,pclFields);
      sprintf(pclFldData, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
	      pclStamm.lanm,
	      pclStamm.finm,
	      pclStamm.gebu,
	      pclStamm.kind,
	      pclStamm.nati,
	      pclStamm.peno,                
	      pclStamm.dobk,
	      pclStamm.dodm,
	      pclStamm.doem,
	      "EXCO",
	      pcpCedaTimestamp);


      sprintf(pclSqlData,"%s,%s",pclFldData,pclStamm.peno);
			
      dbg(DEBUG,"%s: Update-SqlData:<%s>",pclFct,pclSqlData);

      ilLen = Del2NewDel(pclSqlData, ',', '\0');
      strcpy(pclWhereClause,pclStfWhere);
      /*
       * Build SQL-String for update 
       */
      sprintf(pclSqlBuf, "UPDATE %s SET %s %s", pclStfTable,
	      pclFields, pclWhereClause);

			
      dbg(DEBUG,"%s: Update-SqlCmd:<%s>",pclFct,pclSqlBuf);
			 
      slFkt = START;
      slCurUpd = 0;
      dbg(DEBUG, "%s: TABLE <%s> -> TRY UPDATE PENO<%s> ... ", 
	  pclFct, pclStfTable, pclStamm.peno);
				 
      ilSqlRC = sql_if(slFkt, &slCurUpd, pclSqlBuf, pclSqlData);
				
      dbg(DEBUG,"%s: UPDATE returned <%d>",pclFct,ilSqlRC);
				
      if (ilSqlRC != RC_SUCCESS)
	{
	  get_ora_err(ilSqlRC, pclErrMsg);
	} 
      else
	{   
	  *pipUpdate = TRUE;
	  commit_work();
	  /* Read Urno for Broadcast etc. */					
	  sprintf(pclSqlBuf, "SELECT URNO FROM %s %s", 
		  pclStfTable,pclWhereClause);
	  dbg(DEBUG,"%s: Read URNO for Broadcast SQL: <%s>",pclFct,pclSqlBuf);

	  strcpy(pclSqlData,pclStamm.peno);
	  /* dbg(DEBUG,"%s: SQL-Data: <%s>",pclFct,pclSqlData);*/

	  slFkt = START;
	  slCurSel = 0;
	  ilRCSel = sql_if(slFkt, &slCurSel, pclSqlBuf, pclSqlData);
			    
	  dbg(DEBUG,"%s: SELECT URNO  returned <%d>",pclFct,ilRCSel);
			    
	  if (ilRCSel != RC_SUCCESS)
	    {
	      get_ora_err(ilRCSel, pclErrMsg);
	      dbg(TRACE,"%s: SELECT STFTAB FOR URNO FAILED",pclFct);
	      dbg(TRACE,"%s:  ErrMsg:<%s>",pclFct,pclErrMsg);
	    } 
	  else
	    {
	      pclItem = pclSqlData;
	      ilItmLen = GetNextDataItem(pclUrno, &pclItem, ",", "\0", "\0\0");
	      dbg(TRACE, "%s: <%s> UPDATE OK - PENO<%s>",
		  pclFct,pclStfTable,pclStamm.peno);
	    }
					
	}

      /* 
       * INSERT 
       */
      if (ilSqlRC != RC_SUCCESS)
	{
	  if (ilSqlRC == SQL_NOTFOUND)
	    {
	      strcpy(pclCmd,"IRT");
	      dbg(DEBUG,"%s: ...  NOT FOUND IN TABLE => INSERT DATA FOR <%s>",
		  pclFct, pclStamm.peno);
						 
	      /*
	       * ADD HOPO,CDAT,USEC to Fieldslist
	       */
	      BuildSqlFldStr(pclStfFld, "HOPO,USEC,CDAT,USEU,LSTU,URNO", FOR_INSERT, pclFields);
					
	      dbg(TRACE,"%s: FldList <%s>",pclFct,pclFields);
					
	      ilRC = GetNextValues(pclNewUrno, 1);
	      tool_filter_spaces(pclNewUrno); /* trim number string */
						
	      dbg(TRACE,"%s: Get URNO <%s>",pclFct,pclNewUrno);
						
	      sprintf(pclFldData, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
		      pclStamm.lanm,
		      pclStamm.finm,
		      pclStamm.gebu,
		      pclStamm.kind,
		      pclStamm.nati,
		      pclStamm.peno,                
		      pclStamm.dobk,
		      pclStamm.dodm,
		      pclStamm.doem,
		      pcgH3LC,
		      "EXCO",
		      pcpCedaTimestamp,
		      "EXCO",
		      pcpCedaTimestamp,
		      pclNewUrno);

	      dbg(TRACE,"%s: INSERT-SqlData:<%s>",pclFct,pclFldData);
					
	      strcpy(pclSqlData,pclFldData);
	      ilLen = Del2NewDel(pclSqlData, ',', '\0');
	      sprintf(pclSqlBuf, "INSERT INTO %s %s", pclStfTable, pclFields);

					
	      dbg(TRACE,"%s: INSERT-SqlCmd :<%s>",pclFct,pclSqlBuf);
					
	      slFkt = START;
	      slCurIns = 0;
	      ilSqlRC = sql_if(slFkt, &slCurIns, pclSqlBuf, pclSqlData);
	      if (ilSqlRC != RC_SUCCESS)
		{
		  get_ora_err(ilSqlRC, pclErrMsg);
		  dbg(TRACE, "%s: ERROR - INSERT FAILED <%s> ", pclFct, pclErrMsg);
		  ilRC=RC_FAIL;
		} 
	      else
		{
		  commit_work();
		  dbg(TRACE,"%s: <%s> INSERT OK - PENO<%s> URNO<%s>", 
		      pclFct,pclStfTable,pclStamm.peno,pclNewUrno);
		  strcpy(pclUrno,pclNewUrno);
		}					
	    }
	  else
	    {
	      dbg(TRACE, "%s: ERROR - UPDATE-ERROR <%s> ", pclFct, pclErrMsg);
	      ilRC = RC_FAIL;
	    }
	} /* end of if */
		
      commit_work();
      if(ilRC == RC_SUCCESS)
	{
		
				/*
				 * Updating Shared Memory
				 */

	  ilRC = UpdateShm(pclUrno,pclStfTable,pclCmd);
				/*
				 * send message to 
				 BROADCASTHANDLER 
				*/ 
	  if(strcmp(pclCmd,"URT") == 0)        /* Update */
	    {
	      /* Append URNO to Fields and Data for Broadcast */
	      sprintf(pclFields,"%s,USEU,LSTU,URNO",pclStfFld);
	      strcat(pclFldData,",");
	      strcat(pclFldData,pclUrno);
	    }
	  else							/* Insert */
	    {
	      sprintf(pclFields,"%s,HOPO,USEC,CDAT,USEU,LSTU,URNO",pclStfFld);	  
	      pclWhereClause[0] = 0x00;   /* for Broadcast */
	    }
	  dbg(DEBUG,"%s: Send message to BROADCASTHANDLER",pclFct);

	  ilRCSend = SendCedaEvent ( igToBcHdl, 0, pcgDestName, pcgRecvName,
				     pcgTwStart, pcgTwEnd, pclCmd,
				     pclStfTable, pclWhereClause, pclFields,
				     pclFldData, "", 0, ilRC);
					
	  if(ilRCSend != RC_SUCCESS)
	    {
	      dbg(TRACE,"%s: ERROR - SEND BROADCAST FAILS",pclFct);
	      ilRC = RC_FAIL;
	    }

	} /* end of if */

    } 

  close_my_cursor(&slCurSel);
  close_my_cursor(&slCurUpd);
  close_my_cursor(&slCurIns);

  return ilRC;


} /* end of HandleSTF() */


/*****************************************************************************/
/* Function:   HandleADR()                                                   */
/*****************************************************************************/
/* Parameter:                                                                */
/* IN    : char *pcpDataStrg        - Data                                   */
/* IN    : char *pcpCedaTimestamp   - Ceda Time                              */
/*                                                                           */
/* Global Variables:                                                         */
/* Read  :                                                                   */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description: Insert or Update Records from File in Table ADRTAB           */
/*              Updates Shared Memory for Swissport                          */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*          22.12.99  rkl  Written                                           */
/*                                                                           */
/*****************************************************************************/
static int HandleADR(char *pcpDataStrg,char *pcpCedaTimestamp)
{
	char    *pclFct = "HandleADR";
	char    *pclAdrTable = "ADRTAB";
	char    *pclAdrFld   = "STFU,PENO,STRA,ADRC,ZIPA,CITY,LANA,VWT1,TEL1,VWT2,TEL2,VAFR";
    char    *pclAdrWhere = "WHERE PENO=:VPENO AND VAFR=:VVAFR";
	int       ilRC     = RC_SUCCESS;
	int       ilRCSend = RC_SUCCESS;
	int       ilSqlRC  = RC_SUCCESS;
	int       ilRCSel  = RC_SUCCESS;
	int       ilLen    = 0;
	int 	  ilItmLen = 0;
	short     slCurSel = 0;
	short     slCurUpd = 0;
	short     slCurIns = 0;
	short     slFkt = START;
	char    *pclItem= NULL;
	char     pclCmd[8] = "";
	char     pclTmpStrg[1024];
	char     pclSqlBuf[4096];
	char     pclSqlData[4096];
	char     pclWhereClause[128];
	char     pclErrMsg[512];
	char     pclFields[2048];
	char     pclFldData[4096];
	char     pclNewUrno[64];
	char     pclUrno[64];
	char     pclStfUrno[64];

	ADRESS   pclAdr;

    dbg(DEBUG,"%s: Starte Bearbeitung Adressdaten",pclFct);

    /* pruefen sind Mutationen der Adressdaten im DataStrg */			
/*    if(strncmp(&pcpDataStrg[96],"                                     ",30) == 0)
 *   {
 *           /+* keine Mutationen Adressdaten *+/
 *           return ilRC;
 *   }
 *   else
 *   { 
 */
            /* MutationenAdressdaten vorhanden */ 
		    pclUrno[0] = 0x00;
			
			/* dbg(TRACE,"%s: pcpDataStrg: <%s> " ,pclFct,pcpDataStrg);*/
			ilRC = GetSubStrg(pcpDataStrg, 64, 6, TRUE, pclAdr.peno);		
			ilRC = GetSubStrg(pcpDataStrg, 70,26, TRUE, pclAdr.stra);
			ilRC = GetSubStrg(pcpDataStrg, 96,26, TRUE, pclAdr.adrc);
			ilRC = GetSubStrg(pcpDataStrg,122, 7, TRUE, pclAdr.zipa);
			ilRC = GetSubStrg(pcpDataStrg,129,26, TRUE, pclAdr.city);
			ilRC = GetSubStrg(pcpDataStrg,155, 3, TRUE, pclAdr.lana);
			ilRC = GetSubStrg(pcpDataStrg,278,10, TRUE, pclAdr.vwt1);
			ilRC = GetSubStrg(pcpDataStrg,288,11, TRUE, pclAdr.tel1);
			ilRC = GetSubStrg(pcpDataStrg,299,10, TRUE, pclAdr.vwt2);
			ilRC = GetSubStrg(pcpDataStrg,309,11, TRUE, pclAdr.tel2);
			ilRC = GetSubStrg(pcpDataStrg,333, 8, TRUE, pclAdr.vafr);
			if((strcmp(pclAdr.vafr,"00000000") == 0) || 
			   (strcmp(pclAdr.vafr,"        ") == 0))
			{
			  dbg(DEBUG,"%s: No Adress change VAFR=<%s>",pclFct,pclAdr.vafr);
			  strcpy(pclAdr.vafr,"00000000000000"); /* zeit anhaengen */
			  return ilRC;
            }
			else
			{
			  strcat(pclAdr.vafr,"000000"); /* zeit anhaengen */
            }
						
            /* suche zur peno die Urno in der STFTAB */
            sprintf(pclSqlBuf,"SELECT URNO FROM STFTAB WHERE PENO=:VPENO");
		    strcpy(pclSqlData,pclAdr.peno);
			/* dbg(TRACE,"%s: SQL-Data: PENO<%s>",pclFct,pclSqlData);*/
        
			slFkt = START;
			ilRCSel = sql_if(slFkt, &slCurSel, pclSqlBuf, pclSqlData);
		    if (ilRCSel != RC_SUCCESS)
		    {
			  get_ora_err(ilRCSel, pclErrMsg);
			  dbg(TRACE,"%s: SELECT STFTAB FOR URNO FROM PENO=<%s> FAILED",pclFct,
			                                                  pclSqlData);
			  dbg(TRACE,"%s:  ErrMsg:<%s>",pclFct,pclErrMsg);
			  return RC_FAIL;
		    } 
			else
			{
      			pclItem = pclSqlData;
				ilItmLen = GetNextDataItem(pclAdr.stfu, &pclItem, ",", "\0", "\0\0");
				dbg(DEBUG, "%s: SELECT OK - PENO<%s> URNO<%s>",
			             pclFct,pclAdr.peno,pclAdr.stfu);
			}

			/*
			 * TRY UPDATE ADRTAB
			 */
			strcpy(pclCmd,"URT");
			BuildSqlFldStr(pclAdrFld,"USEU,LSTU", FOR_UPDATE, pclFields);
			/*
			 * dbg(TRACE,"%s: FldList <%s>",pclFct,pclFields);
			 */
			sprintf(pclFldData, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                    pclAdr.stfu,
					pclAdr.peno,
					pclAdr.stra,
					pclAdr.adrc,
					pclAdr.zipa,
					pclAdr.city,
					pclAdr.lana,
					pclAdr.vwt1,            
					pclAdr.tel1,
					pclAdr.vwt2,
					pclAdr.tel2,
					pclAdr.vafr,
					"EXCO",
					pcpCedaTimestamp);
                    

                sprintf(pclSqlData,"%s,%s,%s",pclFldData,
				                              pclAdr.peno,
											  pclAdr.vafr);
				
				dbg(DEBUG,"%s: Update-SqlData=<%s>",pclFct,pclSqlData);
				
				ilLen = Del2NewDel(pclSqlData, ',', '\0');
                strcpy(pclWhereClause,pclAdrWhere);
				/*
				 * Build SQL-String for update 
				 */
				sprintf(pclSqlBuf, "UPDATE %s SET %s %s", pclAdrTable,
					pclFields, pclWhereClause);


				dbg(DEBUG,"%s: SQL-Update:<%s>",pclFct,pclSqlBuf);

			    dbg(DEBUG, "%s: TABLE <%s> -> TRY UPDATE PENO<%s> ... ", 
				             pclFct, pclAdrTable, pclAdr.peno);

				slFkt = START;
				ilSqlRC = sql_if(slFkt, &slCurUpd, pclSqlBuf, pclSqlData);
				
				dbg(DEBUG,"%s: UPDATE returned <%d>",pclFct,ilSqlRC);
				
				if (ilSqlRC != RC_SUCCESS)
				{
					get_ora_err(ilSqlRC, pclErrMsg);
				} 
				else
				{   
				    commit_work();
				
					/* Read Urno for Broadcast etc. */					
         			sprintf(pclSqlBuf, "SELECT URNO FROM %s %s", 
					                   pclAdrTable,pclWhereClause);
                    sprintf(pclSqlData,"%s,%s",pclAdr.peno,pclAdr.vafr);
	    			ilLen = Del2NewDel(pclSqlData, ',', '\0');
                    strcpy(pclWhereClause,pclAdrWhere);

     				slFkt = START;
				    ilRCSel = sql_if(slFkt, &slCurSel, pclSqlBuf, pclSqlData);
				    
				    dbg(DEBUG,"%s: SELECT URNO  returned <%d>",pclFct,ilSqlRC);
				    
				    if (ilRCSel != RC_SUCCESS)
				    {
					  get_ora_err(ilRCSel, pclErrMsg);
					  dbg(TRACE,"%s: SELECT ADRTAB FOR URNO FAILED",pclFct);
					  dbg(TRACE,"%s:  ErrMsg:<%s>",pclFct,pclErrMsg);
				    } 
					else
					{
						pclItem = pclSqlData;
						ilItmLen = GetNextDataItem(pclUrno, &pclItem, ",", "\0", "\0\0");
     					dbg(DEBUG, "%s: <%s> UPDATE OK - PENO<%s>",
					             pclFct,pclAdrTable,pclAdr.peno);
				}
					
				}
				/* 
                 * INSERT 
                 */
				if (ilSqlRC != RC_SUCCESS)
				{
					if (ilSqlRC == SQL_NOTFOUND)
					{

         				strcpy(pclCmd,"IRT");
						dbg(DEBUG,"%s: ...  NOT FOUND IN TABLE => INSERT DATA FOR <%s>",
						           pclFct, pclAdr.peno);
						 
						/*
						 * ADD HOPO,CDAT,USEC to Fieldslist
						 */
						BuildSqlFldStr(pclAdrFld, "HOPO,USEC,CDAT,USEU,LSTU,URNO", FOR_INSERT, pclFields);
						/*
						 * dbg(TRACE,"%s: FldList <%s>",pclFct,pclFields);
						 */
						ilRC = GetNextValues(pclNewUrno, 1);
						tool_filter_spaces(pclNewUrno); /* trim number string */
						
						dbg(DEBUG,"%s: Get URNO <%s>",pclFct,pclNewUrno);
						
            			sprintf(pclFldData, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                            pclAdr.stfu,
							pclAdr.peno,
					        pclAdr.stra,
					        pclAdr.adrc,
					        pclAdr.zipa,
					        pclAdr.city,
					        pclAdr.lana,
					        pclAdr.vwt1,                
					        pclAdr.tel1,
					        pclAdr.vwt2,
					        pclAdr.tel2,
					        pclAdr.vafr,
							pcgH3LC,
							"EXCO",
							pcpCedaTimestamp,
							"EXCO",
							pcpCedaTimestamp,
							pclNewUrno);

                        strcpy(pclSqlData,pclFldData);
						
						dbg(DEBUG,"%s: Insert-SqlData <%s>",pclFct,pclSqlData);
						
						ilLen = Del2NewDel(pclSqlData, ',', '\0');
						sprintf(pclSqlBuf, "INSERT INTO %s %s", pclAdrTable, pclFields);

						dbg(DEBUG,"%s: SQL-INSERT:<%s>",pclFct,pclSqlBuf);
						
						slFkt = START;
						ilSqlRC = sql_if(slFkt, &slCurIns, pclSqlBuf, pclSqlData);

						if (ilSqlRC != RC_SUCCESS)
						{
							get_ora_err(ilSqlRC, pclErrMsg);
							dbg(TRACE, "%s: ERROR - INSERT FAILED <%s> ", pclFct, pclErrMsg);
						} 
						else
						{
						    commit_work();
							dbg(DEBUG,"%s: <%s> INSERT OK - PENO<%s> URNO<%s>", 
							          pclFct,pclAdrTable,pclAdr.peno,pclNewUrno);
							strcpy(pclUrno,pclNewUrno);
						}

					}
				} /* end of if */
			
				commit_work();
          		if(ilRC == RC_SUCCESS)
				{
				
					/*
				     * Updating Shared Memory
				     */
  
					/* ilRC = UpdateShm(pclUrno,pclAdrTable,pclCmd);*/
					/*
				     * send message to BROADCASTHANDLER 
				     */ 
			  		if(strcmp(pclCmd,"URT") == 0)        /* Update */
					{
					  /* Append URNO to Fields and Data for Broadcast */
      				  sprintf(pclFields,"%s,USEU,LSTU,URNO",pclAdrFld);
					  strcat(pclFldData,",");
			  		  strcat(pclFldData,pclUrno);
					}
					else							/* Insert */
					{
    				  sprintf(pclFields,"%s,HOPO,USEC,CDAT,USEU,LSTU,URNO",pclAdrFld);	  
                      pclWhereClause[0] = 0x00;   /* for Broadcast */
					}
          			dbg(DEBUG,"%s: Send message to BROADCASTHANDLER",pclFct);

	        		ilRCSend = SendCedaEvent ( igToBcHdl, 0, pcgDestName, pcgRecvName,
  	    	      	                           pcgTwStart, pcgTwEnd, pclCmd,
  		            	                       pclAdrTable, pclWhereClause, pclFields,
    		          	                       pclFldData, "", 0, ilRC);
					
	        	  	if(ilRCSend != RC_SUCCESS)
			  		{
			    	  dbg(TRACE,"%s: ERROR - SEND BROADCAST FAILS",pclFct);
		    		  ilRC = RC_FAIL;
    				}
			    } /* end if */	
/*
 *	} 
 */


    close_my_cursor(&slCurSel);
    close_my_cursor(&slCurUpd);
    close_my_cursor(&slCurIns);

    dbg(DEBUG,"%s: Fertig Bearbeitung Adressdaten",pclFct);

	return ilRC;


} /* end of HandleADR() */


/*****************************************************************************/
/* Function:   HandleCON()                                                   */
/*****************************************************************************/
/* Parameter:                                                                */
/* Read  : char *pcpDataStrg        - Data                                   */
/* Read  : char *pcpCedaTimestamp   - Ceda Time                              */
/*                                                                           */
/* Global Variables:                                                         */
/* Read  :                                                                   */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description: Insert or Update Records from File in Table CONTAB,SCO       */
/*              Updates Shared Memory for Swissport                          */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*          22.12.99  rkl  Written                                           */
/*                                                                           */
/*****************************************************************************/
static int HandleCON(char *pcpDataStrg,char *pcpCedaTimestamp)
{
  char    *pclFct = "HandleCON";
  char    *pclCotTable = "COTTAB";
  char    *pclScoTable = "SCOTAB";
  char    *pclScoFld   = "KOST,KSTN,STSC,ZIFI,CWEH,VPFR,KADC,CPFC,CPFN,PKZU,PKZN,CODE,SURN";
  char    *pclScoWhere = "WHERE SURN=:VSURN AND VPFR=:VVPFR";
  char    *pclSorTable = "SORTAB";
  char    *pclSorFld   = "CODE,SURN,VPFR";  /* CODE = PKZU */
  char    *pclSorWhere = "WHERE SURN=:VSURN AND VPFR=:VVPFR";

  int       ilRC      = RC_SUCCESS;
  int       ilRCSend  = RC_SUCCESS;
  int       ilRCSel1  = RC_SUCCESS;
  int       ilRCSel2  = RC_SUCCESS;
  int       ilRCSel3  = RC_SUCCESS;
  int       ilRCSel4  = RC_SUCCESS;
  int       ilRCSel5  = RC_SUCCESS;
  int		  ilRCIns1  = RC_SUCCESS;
  int       ilRCIns2  = RC_SUCCESS;
  int       ilRCIns3  = RC_SUCCESS;
  int       ilRCIns4  = RC_SUCCESS;
  int       ilRCUpd1  = RC_SUCCESS;
  int       ilRCUpd2  = RC_SUCCESS;
  int       ilLen     = 0;
  int 	  ilItmLen  = 0;
  short     slCurSel1 = 0;
  short     slCurSel2 = 0;
  short     slCurSel3 = 0;
  short     slCurSel4 = 0;
  short     slCurSel5 = 0;
  short     slCurIns1 = 0;
  short     slCurIns2 = 0;
  short     slCurIns3 = 0;
  short     slCurIns4 = 0;
  short     slCurUpd1 = 0;
  short     slCurUpd2 = 0;
  short     slFkt = START;
  char    *pclItem= NULL;
  char     pclCmd[8] = "";
  char     pclTmpStrg[1024];
  char     pclSqlBuf[4096];
  char     pclSqlData[4096];
  char     pclWhereClause[128];
  char     pclErrMsg[512];
  char     pclFields[2048];
  char     pclFldData[4096];
  char     pclNewUrno[64];
  char     pclUrno[64];
  char     pclStfUrno[64];

  CONTRACT   pclCot;

  dbg(DEBUG,"%s: Starte Bearbeitung Vertragsdaten",pclFct);
  /* Vertragsdaten aus String holen */
  /* dbg(DEBUG,"%s: Parameter DataStrg: <%s> " ,pclFct,pcpDataStrg);*/

  ilRC = GetSubStrg(pcpDataStrg, 64,  6, TRUE, pclCot.peno);
  ilRC = GetSubStrg(pcpDataStrg,182,  8, TRUE, pclCot.kost);		
  ilRC = GetSubStrg(pcpDataStrg,190, 26, TRUE, pclCot.kstn);		
  ilRC = GetSubStrg(pcpDataStrg,216,  2, TRUE, pclCot.stsc);
  ilRC = GetSubStrg(pcpDataStrg,320,  1, TRUE, pclCot.zifi);
  ilRC = GetSubStrg(pcpDataStrg,321,  4, TRUE, pclCot.cweh);
  ilRC = GetSubStrg(pcpDataStrg,325,  8, TRUE, pclCot.vafr);
  if((strcmp(pclCot.vafr,"00000000") == 0) || 
     (strcmp(pclCot.vafr,"        ") == 0))
    { 
      strcpy(pclCot.vafr,"00000000000000");
    }
  else
    {
      strcat(pclCot.vafr,"000000"); /* zeit anhaengen */
    }

  ilRC = GetSubStrg(pcpDataStrg,341,  1, TRUE, pclCot.kadc);
  ilRC = GetSubStrg(pcpDataStrg,342,  4, TRUE, pclCot.cpfc);
  ilRC = GetSubStrg(pcpDataStrg,346, 26, TRUE, pclCot.cpfn);
  ilRC = GetSubStrg(pcpDataStrg,372,  7, TRUE, pclCot.pkzu);
  ilRC = GetSubStrg(pcpDataStrg,379, 26, TRUE, pclCot.pkzn);
  ilRC = GetSubStrg(pcpDataStrg,405,  3, TRUE, pclCot.ctrc);

  dbg(DEBUG,"%s: Data: <%s><%s><%s><%s><%s><%s><%s><%s><%s><%s><%s><%s><%s>",pclFct,
      pclCot.peno,
      pclCot.kost,
      pclCot.kstn,
      pclCot.stsc,
      pclCot.zifi,
      pclCot.cweh,
      pclCot.vafr,
      pclCot.kadc,
      pclCot.cpfc,
      pclCot.cpfn,
      pclCot.pkzu,
      pclCot.pkzn,
      pclCot.ctrc);

  /* 
   * suche zur PENO die URNO in der STFTAB
   */
  sprintf(pclSqlBuf,"SELECT URNO FROM STFTAB WHERE PENO=:VPENO");
  strcpy(pclSqlData,pclCot.peno);
  dbg(DEBUG,"%s: SQL:<%s><%s>",pclFct,pclSqlBuf,pclSqlData);

  slFkt = START;
  ilRCSel1 = sql_if(slFkt, &slCurSel1, pclSqlBuf, pclSqlData);
  switch(ilRCSel1)
    {
    case RC_SUCCESS:
      pclItem = pclSqlData;
      ilItmLen = GetNextDataItem(pclCot.surn, &pclItem, ",", "\0", "\0\0");
      dbg(DEBUG, "%s: SELECT STFTAB for PENO<%s> OK -> URNO<%s>",
	  pclFct,pclCot.peno,pclCot.surn);
      ilRC = RC_SUCCESS;
      break;
    case SQL_NOTFOUND:
      dbg(DEBUG,"%s: ERROR - SELECT URNO in STFTAB for PENO=<%s> NOTHING FOUND",
	  pclFct, pclSqlData);
      ilRC = RC_FAIL;
      break;
		
    default:		/* DB - ERROR */
      get_ora_err(ilRCSel1, pclErrMsg);
      dbg(TRACE,"%s: ERROR - SELECT URNO in STFTAB for PENO=<%s> FAILED",
	  pclFct, pclSqlData);
      dbg(TRACE,"%s: ErrMsg:<%s>",pclFct,pclErrMsg);
      ilRC = RC_FAIL;
					
    } /* end switch */
  if(ilRC == RC_SUCCESS)
    {
    
      /* 
       * pruefen ist Vertragsart in der Cottab vorhanden 
       */
      sprintf(pclSqlBuf,"SELECT URNO FROM COTTAB WHERE CTRC=:VCTRC");
      strcpy(pclSqlData,pclCot.ctrc);
      dbg(DEBUG,"%s: SQL:<%s><%s>",pclFct,pclSqlBuf,pclSqlData);

      slFkt = START;
      ilRCSel2 = sql_if(slFkt, &slCurSel2, pclSqlBuf, pclSqlData);
      switch (ilRCSel2)
	{
	case RC_SUCCESS: 
	  dbg(DEBUG, "%s: SELECT OK - CTRC<%s> is in COTTAB URNO<%s>",
	      pclFct,pclCot.ctrc,pclSqlData);
	  break;
		 
	case SQL_NOTFOUND:   /* INSERT CTRC in  COTTAB */
	  dbg(DEBUG, "%s: CTRC<%s> NOT FOUND in COTTAB -> INSERT",
	      pclFct,pclCot.ctrc);
	  /* Get new Urno */
	  GetNextValues(pclNewUrno,1);
	  tool_filter_spaces(pclNewUrno);

	  /* Building SQL-Cmd for INSERT */
	  sprintf(pclSqlBuf,"INSERT INTO COTTAB (CTRC,URNO,CDAT,USEC,LSTU,USEU,HOPO) VALUES (:VCTRC,:VURNO,:CDAT,:USEC,:VLSTU,:VUSEU,:VHOPO)");
                
	  sprintf(pclSqlData,"%s,%s,%s,%s,%s,%s,%s",
		  pclCot.ctrc,pclNewUrno,pcpCedaTimestamp,"EXCO",
		  pcpCedaTimestamp,"EXCO",pcgH3LC);
	  dbg(DEBUG,"%s: SQL:<%s><%s>",pclFct,pclSqlBuf,pclSqlData);
	  ilLen = Del2NewDel(pclSqlData, ',', '\0');
	  slFkt = START;
	  ilRCIns1 = sql_if(slFkt, &slCurIns1, pclSqlBuf, pclSqlData);
	  if(ilRCIns1 != RC_SUCCESS)
	    {
	      get_ora_err(ilRCIns1, pclErrMsg);
	      dbg(TRACE, "%s: INSERT COTTAB FAILED",pclFct);
	      dbg(TRACE,"%s: ErrMsg:<%s>",pclFct,pclErrMsg);

	      ilRC = RC_FAIL;
	    }
	  else
	    {
	      dbg(DEBUG, "%s: INSERT CTRC<%s> in COTTAB OK",pclFct,pclCot.ctrc);
	      commit_work();
	      ilRC = RC_SUCCESS;
						
	    }
	  break;
	default:
	  get_ora_err(ilRCSel2, pclErrMsg);
	  dbg(TRACE,"%s: ERROR - SELECT CTRC<%s> in COTTAB FAILED",
	      pclFct,pclCot.ctrc);
	  dbg(TRACE,"%s: ErrMsg:<%s>",pclFct,pclErrMsg);
	  ilRC = RC_FAIL;
		
	} /* end switch */

      if(ilRC == RC_SUCCESS)
	{
	  /* 
	   * pruefen ist Organisationseinheit in der Orgtab vorhanden 
	   */
	  sprintf(pclSqlBuf,"SELECT URNO FROM ORGTAB WHERE DPT1=:VDPT1");
	  strcpy(pclSqlData,pclCot.pkzu);
	  dbg(DEBUG,"%s: SQL:<%s><%s>",pclFct,pclSqlBuf,pclSqlData);

	  slFkt = START;
	  ilRCSel4 = sql_if(slFkt, &slCurSel4, pclSqlBuf, pclSqlData);
	  switch (ilRCSel4)
	    {
	    case RC_SUCCESS: 
	      dbg(DEBUG, "%s: SELECT OK - DPT1<%s> is in ORGTAB URNO<%s>",
		  pclFct,pclCot.ctrc,pclSqlData);
	      break;
		 
	    case SQL_NOTFOUND:   /* INSERT PKZU in Field DPT1 in ORGTAB */
	      dbg(DEBUG, "%s: PKZU<%s> NOT FOUND in ORGTAB -> INSERT",
		  pclFct,pclCot.ctrc);
	      /* Get new Urno */
	      GetNextValues(pclNewUrno,1);
	      tool_filter_spaces(pclNewUrno);

	      /* Building SQL-Cmd for INSERT */
	      sprintf(pclSqlBuf,"INSERT INTO ORGTAB (DPT1,DPTN,URNO,CDAT,USEC,LSTU,USEU,HOPO) VALUES (:VDPT1,:VDPTN,:VURNO,:CDAT,:USEC,:VLSTU,:VUSEU,:VHOPO)");
                
	      sprintf(pclSqlData,"%s,%s,%s,%s,%s,%s,%s,%s",
		      pclCot.pkzu,pclCot.pkzn,pclNewUrno,pcpCedaTimestamp,"EXCO",
		      pcpCedaTimestamp,"EXCO",pcgH3LC);
	      dbg(DEBUG,"%s: SQL:<%s><%s>",pclFct,pclSqlBuf,pclSqlData);
	      ilLen = Del2NewDel(pclSqlData, ',', '\0');
	      slFkt = START;
	      ilRCIns3 = sql_if(slFkt, &slCurIns3, pclSqlBuf, pclSqlData);
	      if(ilRCIns3 != RC_SUCCESS)
		{
		  get_ora_err(ilRCIns3, pclErrMsg);
		  dbg(TRACE, "%s: INSERT DPT1<%s> in ORGTAB FAILED",
		      pclFct,pclCot.pkzu);
		  dbg(TRACE,"%s: ErrMsg:<%s>",pclFct,pclErrMsg);

		  ilRC = RC_FAIL;
		}
	      else
		{
		  dbg(DEBUG, "%s: INSERT DPT1<%s> in ORGTAB OK",pclFct,pclCot.pkzu);
		  commit_work();
		  ilRC = RC_SUCCESS;
						
		}
	      break;
	    default:
	      get_ora_err(ilRCSel4, pclErrMsg);
	      dbg(TRACE,"%s: ERROR - SELECT DPT1<%s> in ORGTAB FAILED",
		  pclFct,pclCot.ctrc);
	      dbg(TRACE,"%s: ErrMsg:<%s>",pclFct,pclErrMsg);
	      ilRC = RC_FAIL;
		
	    } /* end switch */
        }

      if(ilRC == RC_SUCCESS)
	{

	  /*
	   * TRY UPDATE SCOTAB
	   */
	  strcpy(pclCmd,"URT");
	  BuildSqlFldStr(pclScoFld,"USEU,LSTU,HOPO", FOR_UPDATE, pclFields);
	  /*
	   * dbg(TRACE,"%s: FldList <%s>",pclFct,pclFields);
	   */

	  /* KOST,KSTN,STSC,ZIFI,CWEH,VPFR,KADC,CPFC,CPFN,PKZU,PKZN,CODE,SURN */
	  sprintf(pclFldData, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
		  pclCot.kost,
		  pclCot.kstn,
		  pclCot.stsc,
		  pclCot.zifi,
		  pclCot.cweh,
		  pclCot.vafr,
		  pclCot.kadc,
		  pclCot.cpfc,
		  pclCot.cpfn,
		  pclCot.pkzu,
		  pclCot.pkzn,
		  pclCot.ctrc,
		  pclCot.surn,
		  "EXCO",
		  pcpCedaTimestamp,
		  pcgH3LC);
                    

	  sprintf(pclSqlData,"%s,%s,%s",pclFldData, pclCot.surn, pclCot.vafr);
				
	  dbg(DEBUG,"%s: Update <%s> SqlData=<%s>",pclFct,
	      pclScoTable,pclSqlData);
				
	  ilLen = Del2NewDel(pclSqlData, ',', '\0');
	  strcpy(pclWhereClause,pclScoWhere);
	  /*
	   * Build SQL-String for update 
	   */
	  sprintf(pclSqlBuf, "UPDATE %s SET %s %s", pclScoTable,
		  pclFields, pclWhereClause);


	  dbg(DEBUG,"%s: Update <%s> SQL-Cmd:<%s>",pclFct,
	      pclScoTable, pclSqlBuf);

	  dbg(DEBUG, "%s: TABLE <%s> -> TRY UPDATE <%s><%s> ... ", 
	      pclFct, pclScoTable, pclCot.surn,pclCot.vafr);

	  slFkt = START;
	  ilRCUpd1 = sql_if(slFkt, &slCurUpd1, pclSqlBuf, pclSqlData);
				
	  /* dbg(DEBUG,"%s: UPDATE returned <%d>",pclFct,ilRCUpd1);*/
					
	  if (ilRCUpd1 != RC_SUCCESS)
	    {
	      get_ora_err(ilRCUpd1, pclErrMsg);
	    } 
	  else
	    {   
	      commit_work();
				
				/* Read Urno for Broadcast etc. */					
	      sprintf(pclSqlBuf, "SELECT URNO FROM %s %s", 
		      pclScoTable,pclWhereClause);
	      sprintf(pclSqlData,"%s,%s",pclCot.surn,pclCot.vafr);
	      ilLen = Del2NewDel(pclSqlData, ',', '\0');
	      strcpy(pclWhereClause,pclScoWhere);

	      slFkt = START;
	      ilRCSel3 = sql_if(slFkt, &slCurSel3, pclSqlBuf, pclSqlData);
	      if (ilRCSel3 != RC_SUCCESS)
		{
		  get_ora_err(ilRCSel3, pclErrMsg);
		  dbg(TRACE,"%s: SELECT SCOTAB FOR URNO FAILED",pclFct);
		  dbg(TRACE,"%s:  ErrMsg:<%s>",pclFct,pclErrMsg);
		} 
	      else
		{
		  pclItem = pclSqlData;
		  ilItmLen = GetNextDataItem(pclUrno, &pclItem, ",", "\0", "\0\0");
		  dbg(DEBUG, "%s: <%s> UPDATE OK - SURN<%s>VPFR<%s>",
		      pclFct,pclCotTable,pclCot.surn,pclCot.vafr);
		}
			
	    }
	  /* 
	   * INSERT 
	   */
	  if (ilRCUpd1 != RC_SUCCESS)
	    {
	      if (ilRCUpd1 == SQL_NOTFOUND)
		{
		  strcpy(pclCmd,"IRT");
		  dbg(DEBUG,"%s: ... INSERT DATA <%s>",
		      pclFct, pclScoTable);
					 
		  /*
		   * ADD HOPO,CDAT,USEC to Fieldslist
		   */
		  BuildSqlFldStr(pclScoFld, "HOPO,USEC,CDAT,USEU,LSTU,URNO", FOR_INSERT, pclFields);
		  /*
		   * dbg(DEBUG,"%s: FldList <%s>",pclFct,pclFields);
		   */
		  ilRC = GetNextValues(pclNewUrno, 1);
		  tool_filter_spaces(pclNewUrno); /* trim number string */
							
		  dbg(DEBUG,"%s: Get URNO <%s>",pclFct,pclNewUrno);
							
		  /* KOST,KSTN,STSC,ZIFI,CWEH,VPFR,KADC,CPFC,CPFN,PKZU,PKZN,CODE,SURN */
		  sprintf(pclFldData, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
			  pclCot.kost,
			  pclCot.kstn,
			  pclCot.stsc,
			  pclCot.zifi,
			  pclCot.cweh,
			  pclCot.vafr,
			  pclCot.kadc,
			  pclCot.cpfc,
			  pclCot.cpfn,
			  pclCot.pkzu,
			  pclCot.pkzn,
			  pclCot.ctrc,
			  pclCot.surn,
			  pcgH3LC,
			  "EXCO",
			  pcpCedaTimestamp,
			  "EXCO",
			  pcpCedaTimestamp,
			  pclNewUrno);

		  strcpy(pclSqlData,pclFldData);
						
		  dbg(DEBUG,"%s: Insert-SqlData <%s>",pclFct,pclSqlData);
						
		  ilLen = Del2NewDel(pclSqlData, ',', '\0');
		  sprintf(pclSqlBuf, "INSERT INTO %s %s", pclScoTable, pclFields);

		  dbg(DEBUG,"%s: INSERT SQL-Cmd:<%s>",pclFct,pclSqlBuf);
		  slFkt = START;
		  ilRCIns2 = sql_if(slFkt, &slCurIns2, pclSqlBuf, pclSqlData);
		  if (ilRCIns2 != RC_SUCCESS)
		    {
		      get_ora_err(ilRCIns2, pclErrMsg);
		      dbg(TRACE, "%s: ERROR - INSERT FAILED <%s> ", 
			  pclFct, pclErrMsg);
		    } 
		  else
		    {
		      commit_work();
		      dbg(DEBUG,"%s: <%s> INSERT OK - PENO<%s> URNO<%s>", 
			  pclFct,pclScoTable,pclCot.peno,pclNewUrno);
		      strcpy(pclUrno,pclNewUrno);
		    }
		}
	    } /* end of if */
			
	  commit_work();
	  if(ilRC == RC_SUCCESS)
	    {
		
				/*
				 * Updating Shared Memory
				 */

				/* ilRC = UpdateShm(pclUrno,pclAdrTable,pclCmd);*/
				/*
				 * send message to BROADCASTHANDLER 
				 */ 
	      if(strcmp(pclCmd,"URT") == 0)        /* Update */
		{
		  /* Append URNO to Fields and Data for Broadcast */
		  sprintf(pclFields,"%s,USEU,LSTU,URNO",pclScoFld);
		  strcat(pclFldData,",");
		  strcat(pclFldData,pclUrno);
		}
	      else							/* Insert */
		{
		  sprintf(pclFields,"%s,HOPO,USEC,CDAT,USEU,LSTU,URNO",pclScoFld);	  
		  pclWhereClause[0] = 0x00;   /* for Broadcast */
		}
          	
	      dbg(DEBUG,"%s: Send message to BROADCASTHANDLER",pclFct);

	      ilRCSend = SendCedaEvent ( igToBcHdl, 0, pcgDestName, pcgRecvName,
					 pcgTwStart, pcgTwEnd, pclCmd,
					 pclScoTable, pclWhereClause, pclFields,
					 pclFldData, "", 0, ilRC);
					
	      if(ilRCSend != RC_SUCCESS)
		{
		  dbg(TRACE,"%s: ERROR - SEND BROADCAST FAILS",pclFct);
		  ilRC = RC_FAIL;
		}
	    } /* end if */	
	} /* end if */

      /*****************************************************************************/
      if(ilRC == RC_SUCCESS)
	{

	  /*
	   * TRY UPDATE SORTAB
	   */
	  strcpy(pclCmd,"URT");
	  BuildSqlFldStr(pclSorFld,"HOPO", FOR_UPDATE, pclFields);
	  /*
	   * dbg(TRACE,"%s: FldList <%s>",pclFct,pclFields);
	   */

	  /* PKZU,SURN,VPFR */
	  sprintf(pclFldData, "%s,%s,%s,%s",
		  pclCot.pkzu,
		  pclCot.surn,
		  pclCot.vafr,
		  pcgH3LC);
                    

	  sprintf(pclSqlData,"%s,%s,%s",pclFldData, pclCot.surn, pclCot.vafr);
				
	  dbg(DEBUG,"%s: Update <%s> SqlData=<%s>",pclFct,
	      pclSorTable,pclSqlData);
				
	  ilLen = Del2NewDel(pclSqlData, ',', '\0');
	  strcpy(pclWhereClause,pclSorWhere);
	  /*
	   * Build SQL-String for update 
	   */
	  sprintf(pclSqlBuf, "UPDATE %s SET %s %s", pclSorTable,
		  pclFields, pclWhereClause);


	  dbg(DEBUG,"%s: Update <%s> SQL-Cmd:<%s>",pclFct,
	      pclSorTable, pclSqlBuf);

	  dbg(DEBUG, "%s: TABLE <%s> -> TRY UPDATE <%s><%s> ... ", 
	      pclFct, pclSorTable, pclCot.surn,pclCot.vafr);

	  slFkt = START;
	  ilRCUpd2 = sql_if(slFkt, &slCurUpd2, pclSqlBuf, pclSqlData);
				
	  dbg(DEBUG,"%s: UPDATE returned <%d>",pclFct,ilRCUpd2);
					
	  if (ilRCUpd2 != RC_SUCCESS)
	    {
	      get_ora_err(ilRCUpd2, pclErrMsg);
	    } 
	  else
	    {   
	      dbg(DEBUG,"%s: Before commit work",pclFct);
	      commit_work();
				
				/* Read Urno for Broadcast etc. */					
	      sprintf(pclSqlBuf, "SELECT URNO FROM %s %s", 
		      pclSorTable,pclWhereClause);
	      sprintf(pclSqlData,"%s,%s",pclCot.surn,pclCot.vafr);
	      ilLen = Del2NewDel(pclSqlData, ',', '\0');
	      strcpy(pclWhereClause,pclScoWhere);

	      slFkt = START;
	      ilRCSel5 = sql_if(slFkt, &slCurSel5, pclSqlBuf, pclSqlData);
	      if (ilRCSel5 != RC_SUCCESS)
		{
		  get_ora_err(ilRCSel5, pclErrMsg);
		  dbg(TRACE,"%s: SELECT SORTAB FOR URNO FAILED",pclFct);
		  dbg(TRACE,"%s:  ErrMsg:<%s>",pclFct,pclErrMsg);
		} 
	      else
		{
		  pclItem = pclSqlData;
		  ilItmLen = GetNextDataItem(pclUrno, &pclItem, ",", "\0", "\0\0");
		  dbg(DEBUG, "%s: <%s> UPDATE OK - SURN<%s>VPFR<%s>",
		      pclFct,pclSorTable,pclCot.surn,pclCot.vafr);
		}
			
	    }
	  /* 
	   * INSERT 
	   */
	  if (ilRCUpd2 != RC_SUCCESS)
	    {
	      if (ilRCUpd2 == SQL_NOTFOUND)
		{
		  strcpy(pclCmd,"IRT");
		  dbg(DEBUG,"%s: ... INSERT DATA <%s>",
		      pclFct, pclSorTable);
					 
		  /*
		   * ADD HOPO to Fieldslist
		   */
		  BuildSqlFldStr(pclSorFld, "HOPO,URNO", FOR_INSERT, pclFields);
		  /*
		   * dbg(DEBUG,"%s: FldList <%s>",pclFct,pclFields);
		   */
		  ilRC = GetNextValues(pclNewUrno, 1);
		  tool_filter_spaces(pclNewUrno); /* trim number string */
							
		  dbg(DEBUG,"%s: Get URNO <%s>",pclFct,pclNewUrno);
							
		  /* PKZU,SURN,VPFR */
		  sprintf(pclFldData, "%s,%s,%s,%s,%s",
			  pclCot.pkzu,
			  pclCot.surn,
			  pclCot.vafr,
			  pcgH3LC,
			  pclNewUrno);

		  strcpy(pclSqlData,pclFldData);
						
		  dbg(DEBUG,"%s: Insert-SqlData <%s>",pclFct,pclSqlData);
						
		  ilLen = Del2NewDel(pclSqlData, ',', '\0');
		  sprintf(pclSqlBuf, "INSERT INTO %s %s", pclSorTable, pclFields);

		  dbg(DEBUG,"%s: INSERT SQL-Cmd:<%s>",pclFct,pclSqlBuf);
		  slFkt = START;
		  ilRCIns4 = sql_if(slFkt, &slCurIns4, pclSqlBuf, pclSqlData);
		  if (ilRCIns4 != RC_SUCCESS)
		    {
		      get_ora_err(ilRCIns4, pclErrMsg);
		      dbg(TRACE, "%s: ERROR - INSERT FAILED <%s> ", 
			  pclFct, pclErrMsg);
		    } 
		  else
		    {
		      commit_work();
		      dbg(DEBUG,"%s: <%s> INSERT OK - PENO<%s> URNO<%s>", 
			  pclFct,pclSorTable,pclCot.peno,pclNewUrno);
		      strcpy(pclUrno,pclNewUrno);
		    }
		}
	    } /* end of if */
			
	  commit_work();
	  if(ilRC == RC_SUCCESS)
	    {
		
				/*
				 * Updating Shared Memory
				 */

				/* ilRC = UpdateShm(pclUrno,pclAdrTable,pclCmd);*/
				/*
				 * send message to BROADCASTHANDLER 
				 */ 
	      if(strcmp(pclCmd,"URT") == 0)        /* Update */
		{
		  /* Append URNO to Fields and Data for Broadcast */
		  sprintf(pclFields,"%s,URNO",pclSorFld);
		  strcat(pclFldData,",");
		  strcat(pclFldData,pclUrno);
		}
	      else							/* Insert */
		{
		  sprintf(pclFields,"%s,HOPO,URNO",pclSorFld);	  
		  pclWhereClause[0] = 0x00;   /* for Broadcast */
		}
          	
	      dbg(DEBUG,"%s: Send message to BROADCASTHANDLER",pclFct);

	      ilRCSend = SendCedaEvent ( igToBcHdl, 0, pcgDestName, pcgRecvName,
					 pcgTwStart, pcgTwEnd, pclCmd,
					 pclSorTable, pclWhereClause, pclFields,
					 pclFldData, "", 0, ilRC);
					
	      if(ilRCSend != RC_SUCCESS)
		{
		  dbg(TRACE,"%s: ERROR - SEND BROADCAST FAILS",pclFct);
		  ilRC = RC_FAIL;
		}
	    } /* end if */	
	} /* end if */

      /*****************************************************************************/
    } /* end if */
 
  close_my_cursor(&slCurSel1);
  close_my_cursor(&slCurSel2);
  close_my_cursor(&slCurSel3);
  close_my_cursor(&slCurSel4);
  close_my_cursor(&slCurSel5);
  close_my_cursor(&slCurIns1);
  close_my_cursor(&slCurIns2);
  close_my_cursor(&slCurIns3);
  close_my_cursor(&slCurIns4);
  close_my_cursor(&slCurUpd1);
  close_my_cursor(&slCurUpd2);

  dbg(DEBUG,"%s: Fertig Bearbeitung Vertragsdaten",pclFct);

  return ilRC;


} /* end of HandleCON() */



/*****************************************************************************/
/* Function:   HandleMUA()                                                   */
/*****************************************************************************/
/* Parameter:                                                                */
/*                                                                           */
/* Global Variables:                                                         */
/* Read  : char  *pcgStammDatFile   - filename stammdata                     */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description: Insert or Update Records from File in Table ADRTAB           */
/*              Updates Shared Memory for Swissport                          */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*          22.12.99  rkl  Written                                           */
/*          03.03.00  rkl  Mutation from the Same Day                        */
/*                                                                           */
/*****************************************************************************/
static int HandleMUA(void)
{
  char    *pclFct = "HandleMUA";
  char    *pclAdrTable = "ADRTAB";
  char    *pclAdrFld   = "PENO,STRA,ADRC,ZIPA,CITY,LANA,VWT1,TEL1,VWT2,TEL2,VAFR";
  char    *pclAdrWhereTpl = "WHERE (LSTU LIKE '%s%%' AND USEU <> 'EXCO')";
  char    *pclDel = ",";
  int       ilRC  = RC_SUCCESS;
  int       ilRCDB= RC_SUCCESS;
  short     slCursor = 0;
  short     slFkt    = START;
  int       ilRecCount = 0;
  int       ilFldCnt = 0;
  int       ilItmLen = 0;
  char     pclPathFile[1024]  = "";
  char     pclFileName[512]  = "";
  char     pclErrTxt[512]     = "";
  char     pclSqlBuf[12*1024] = "";
  char     pclSqlData[12*024] = "";
  char     pclWhereClause[512]= "";
  char     pclActCedaTime[24] = "";
  char     pclAktDayDate[24] = "";
  char    *pclItem = NULL;
  char    *pclCPtr = NULL;
	
  ADRESS   pclAdr;
  FILE    *fp_Adr;
    
     
  dbg(TRACE,"%s: Starte Suche nach Mutationen Adressdaten",pclFct);

  /* Get Actual CEDA Time */
  ilRC = GetCedaTimestamp(&pclActCedaTime[0], 15, 0);
  dbg(DEBUG,"%s: Akt.    Datum: <%s>",pclFct,pclActCedaTime);
  strcpy(pclAktDayDate, pclActCedaTime);
  pclAktDayDate[8] = 0x00; /* only date */

  /* Build Filename with EXPORTPATH and Mutation Adressfilename from ConfigFile */
  strcpy(pclFileName,pcgMutAdrFile);
  if((pclCPtr = strstr(pclFileName,"YYMMDD")) != NULL)
    {
      strncpy(pclCPtr,&pclAktDayDate[2],6);
    }
  else
    {
      dbg(TRACE,"%s: ERROR - Building Filename for Mutation Adress failed !",pclFct); 
      ilRC = RC_FAIL;
    }
		
  sprintf(pclPathFile,"%s/%s",pcgPrsExpPath,pclFileName);
  if((fp_Adr = fopen(pclPathFile,"w+")) == (FILE *) NULL)
    {
      dbg(TRACE,"%s: ERROR - Can't open File <%s> for writing",pclFct);
      ilRC = RC_FAIL;
    }
  else
    {
      /* Building SELECT Statement */
      sprintf(pclWhereClause,pclAdrWhereTpl,pclAktDayDate);
      sprintf(pclSqlBuf,"SELECT %s FROM %s %s",pclAdrFld,pclAdrTable,pclWhereClause);
      ilFldCnt = CountDelimiter(pclAdrFld, ",");
   
      dbg(DEBUG,"%s: SQL-Cmd: <%s>",pclFct,pclSqlBuf);
      ilRecCount = 0;
      slCursor = 0;
      slFkt = START;

    do
	{
	  ilRCDB = sql_if(slFkt,&slCursor,pclSqlBuf,pclSqlData);
	  if (ilRCDB == RC_SUCCESS)
	    {
	      ilRecCount++;
	      BuildItemBuffer(pclSqlData, pclSqlBuf, ilFldCnt, pclDel); 

	      pclItem = pclSqlData;
	      /* Get Values from Data */			
	      ilItmLen = GetNextDataItem(pclAdr.peno, &pclItem, pclDel, "\0", "  ");
	      ilItmLen = GetNextDataItem(pclAdr.stra, &pclItem, pclDel, "\0", "  ");
	      ilItmLen = GetNextDataItem(pclAdr.adrc, &pclItem, pclDel, "\0", "  ");
	      ilItmLen = GetNextDataItem(pclAdr.zipa, &pclItem, pclDel, "\0", "  ");
	      ilItmLen = GetNextDataItem(pclAdr.city, &pclItem, pclDel, "\0", "  ");
	      ilItmLen = GetNextDataItem(pclAdr.lana, &pclItem, pclDel, "\0", "  ");
	      ilItmLen = GetNextDataItem(pclAdr.vwt1, &pclItem, pclDel, "\0", "  ");
	      ilItmLen = GetNextDataItem(pclAdr.tel1, &pclItem, pclDel, "\0", "  ");
	      ilItmLen = GetNextDataItem(pclAdr.vwt2, &pclItem, pclDel, "\0", "  ");
	      ilItmLen = GetNextDataItem(pclAdr.tel2, &pclItem, pclDel, "\0", "  ");
	      ilItmLen = GetNextDataItem(pclAdr.vafr, &pclItem, pclDel, "\0", "  ");

/* eth MUA */
       /* Schreibe Daten in Datei zukuenftig 136 spaces am Ende wg. detachierter felder*/
	      fprintf(fp_Adr,"%64s%6s%26s%26s%7s%26s%3s%120s%10s%11s%10s%11s%13s%.8s%69s\n",
		      " ",
		      pclAdr.peno,
		      pclAdr.stra,
		      pclAdr.adrc,
		      pclAdr.zipa,
		      pclAdr.city,
		      pclAdr.lana,
		      " ",
		      pclAdr.vwt1,
		      pclAdr.tel1,
		      pclAdr.vwt2,
		      pclAdr.tel2,
		      " ",
		      pclAdr.vafr,
		      " ");

	    } /* end if */
	    else if ( ilRCDB != SQL_NOTFOUND )
	    {
	      /* SQL - Error */
	      dbg(TRACE,"%s: SQL-ERROR sql_if fails ilRCDB = <%d>",pclFct,ilRCDB);
	      dbg(TRACE,"%s: SQL-Command = <%s>",pclFct,pclSqlBuf);
	      get_ora_err (ilRCDB, pclErrTxt);     
	      ilRC = RC_FAIL;
	    } /* end else if */ 

	  slFkt = NEXT;
	} while (ilRCDB == RC_SUCCESS);
  
    close_my_cursor(&slCursor);

    fprintf(fp_Adr,"END\n");	    
    fclose(fp_Adr);  

    if( ilRecCount == 0 )  /* No Data found */
	{
	  dbg(DEBUG,"%s: No Data found !",pclFct);  
	}
    else
	{
	  dbg(TRACE,"%s: Number of Records found  <%d>", pclFct, ilRecCount);
	  SendCedaEvent ( igToFtphdl, 0, pcgDestName, pcgRecvName,
			  pcgTwStart, pcgTwEnd,"MUA",
			  " ","         "," ",
			  " ",NULL,4,NETOUT_NO_ACK);
	  dbg(DEBUG,"%s:Send Event <MUA> to ftphdl ", pclFct);
	}
    

    } /* end of if */

  return ilRC;

} /* end of HandleMUA() */



/*****************************************************************************/
/* Function:   HandleMUC()                                                   */
/*****************************************************************************/
/* Parameter:                                                                */
/*                                                                           */
/* Global Variables:                                                         */
/* Read  : char  *pcgStammDatFile   - filename stammdata                     */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description: Insert or Update Records from File in Table ADRTAB           */
/*              Updates Shared Memory for Swissport                          */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*          22.12.99  rkl  Written                                           */
/*                                                                           */
/*****************************************************************************/
static int HandleMUC(void)
{
  char    *pclFct = "HandleMUC";
  char    *pclScoTable = "SCOTAB";

/* char *pclScoFld = "SURN,KOST,KSTN,STSC,ZIFI,CWEH,VPFR,KADC,CPFC,CPFN,PKZU,PKZN,CODE"; */
/* char *pclScoWhereTpl = "WHERE (LSTU LIKE '%s%%' AND USEU <> 'EXCO')"; */

  char    *pclSelSco = "select vpfr from scotab where surn = :VSUR1 and vpfr = (select max(vpfr) from scotab where surn = :VSUR2 and vpfr <= :VDAT1) and (vpto >= :VDAT2 OR vpto like ' %')";

  char    *pclSelectStf = "SELECT PENO FROM STFTAB WHERE URNO=:VURNO";

  char    *pclSelAcc = "SELECT CO%.2s from acctab where type = '44' and year = %.4s and stfu = :VSTFU";

  char    *pclDel  = ",";

  int       ilRC   = RC_SUCCESS;
  int       ilRCSco1 = RC_SUCCESS;
  int       ilRCSco2 = RC_SUCCESS;
  int       ilRCStf  = RC_SUCCESS;
  int       ilRCAcc  = RC_SUCCESS;

  int	ilMonat; /* der Monat */
  int	ilJahr;  /* das Jahr */

  short     slCurStf= 0;
  short     slCurSco1 = 0;
  short     slCurSco2 = 0;
  short     slCurAcc = 0;
  short     slFkt    = START;
  int       ilRecCount = 0;
  int       ilFldCnt = 0;
  int       ilItmLen = 0;
  char     pclPathFile[1024]  = "";
  char     pclFileName[512]   = "";
  char     pclErrTxt[512]     = "";
  char     pclSqlBuf[12*1024] = "";
  char     pclSqlData[12*1024] = "";
  char     pclWhereClause[512]= "";
  char     pclActCedaTime[24] = "";
  char     pclAktDayDate[24] = "";
  char     pclSelection[128];
  char    *pclItem = NULL;
  char    *pclCPtr = NULL;
  char     pclDummy[32];
	
  CONTRACT pclSco;
  FILE    *fp_Sco;
    
  memset(&pclSelection,0x00,sizeof(pclSelection));

     
  dbg(TRACE,"%s: Starte Suche nach Mutationen Vertragsdaten",pclFct);

  /* Get Actual CEDA Time */
  ilRC = GetCedaTimestamp(&pclActCedaTime[0], 15, 0);

  dbg(DEBUG,"%s: Aktuelles Ceda Datum: <%s>", pclFct, pclActCedaTime);

  strcpy(pclAktDayDate, pclActCedaTime);

  pclAktDayDate[8] = 0x00; /* only date */
  
  if (atoi(&pclAktDayDate[7]) != igDayofMUC){
    /* 32 is debug mode*/
    if (igDayofMUC != 32){
      dbg(DEBUG,"%s: Create file only day of month <%d> today is <%s>",
        pclFct, igDayofMUC, &pclAktDayDate[7]);
      return RC_SUCCESS;
    }
  } 

  strncpy(pclDummy, &pclActCedaTime[6], 2);

  dbg(DEBUG,"%s: ActTime before <%s>", pclFct, pclActCedaTime); 

  AddSecondsToCEDATime(pclActCedaTime,-((atoi(pclDummy))*SECONDS_PER_DAY),1);

  dbg(DEBUG,"%s: ActTime after  <%s>", pclFct, pclActCedaTime); 
  
  /* Build Filename with EXPORTPATH and Mutation Adressfilename from ConfigFile */
  strcpy(pclFileName,pcgMutConFile);
  if((pclCPtr = strstr(pclFileName,"YYMMDD")) != NULL)
  {
    strncpy(pclCPtr,&pclAktDayDate[2],6);
  }
  else
  {
    dbg(TRACE,"%s: ERROR - Building Filename for Mutation Contract failed !",pclFct); 
    ilRC = RC_FAIL;
  }
		
  sprintf(pclPathFile,"%s/%s", pcgPrsExpPath, pclFileName);
  if((fp_Sco = fopen(pclPathFile,"w+")) == (FILE *) NULL)
  {
    dbg(TRACE,"%s: ERROR - Can't open File <%s> for writing",pclFct);
    ilRC = RC_FAIL;
  }
  else
  { /* Building SELECT Statement */

    sprintf(pclSqlBuf,"SELECT distinct SURN FROM SCOTAB where CODE in (%s)",pcgFuncCodes); 
   
    dbg(DEBUG,"%s: SQL-Cmd: <%s>", pclFct, pclSqlBuf);
    slCurStf = 0;
    slCurSco1 = 0;
    slCurSco2 = 0;
    slCurAcc = 0;
    slFkt = START;

    do
    {
      ilRC = RC_SUCCESS;

      /* get all employees*/
      ilRCSco1 = sql_if(slFkt, &slCurSco1, pclSqlBuf, pclSqlData);

      /* set search function to NEXT */
      slFkt = NEXT;

      if (ilRCSco1 == RC_SUCCESS)
      {
	    /* entry of first employee */ 
	    strcpy(pclSco.surn, pclSqlData);

	    sprintf(pclSqlData,"%s%c%s%c%s%c%s",
          pclSco.surn, 0x00, pclSco.surn, 0x00, pclActCedaTime, 0x00, pclActCedaTime);

	    /* Search in scotab for valid timeframe*/
	    dbg(DEBUG,"%s: SQL-Cmd SCOTAB  <%s><%s>", pclFct, pclSelSco, pclSqlData);

	    ilRCSco2 = sql_if(START, &slCurSco2, pclSelSco, pclSqlData);
	    if (ilRCSco2 != RC_SUCCESS)
        {
	      ilRC = RC_FAIL;
	      if(ilRCSco2 != SQL_NOTFOUND){
	        /* SQL - Error */
	        get_ora_err (ilRCStf, pclErrTxt);     
	        dbg(TRACE,"%s: SQL-ERROR - <%s>",pclFct,pclErrTxt);
	      } else {
	        dbg(TRACE,"%s: SURN no valid line found <%s> in SCOTAB",pclFct,pclSco.surn);
	      } /* end != SQL_NOTFOUND*/
	    }
        else
        {
	      /* Found valid timeframe*/

/* das gefundene Vertragsdatum festhalten */
	      /* strncpy(pclSco.vafr, pclSqlData, 8); */
/* es soll aber das Erstellungs datum rein ... */

/* Baustelle 28.05.2001 */
/* 1. des Vormonats ...

          sscanf(&pclActCedaTime[4],"%2d", &ilMonat);
          sscanf(&pclActCedaTime[0],"%4d", &ilJahr);

          ilMonat -= 1;
          if (ilMonat < 1)
          {
            ilMonat = 12;
            ilJahr -= 1;
          }

          strcpy(pclSco.vafr[0], "01");

          sprintf(pclSco.vafr[2], "%2s", ilMonat);
          sprintf(pclSco.vafr[4], "%4s", ilJahr);
 */

/* Baustelle eth 29.05.2001 */
/* Format 01MMJJJJ */
          /* strcpy(pclSco.vafr, "01"); */
          /* strncat(pclSco.vafr, &pclActCedaTime[4], 2); */
          /* strncat(pclSco.vafr, &pclActCedaTime[0], 4); */

/* Baustelle eth 18.06.2001 */
/* neues Format JJJJMM01 */
          strncpy(pclSco.vafr, &pclActCedaTime[0], 4);
          strncat(pclSco.vafr, &pclActCedaTime[4], 2);
          strcat(pclSco.vafr, "01");

          /* Search for entry in acctab */ 
          /* SQL Statement zusammenbauen */
          /*                               Monat               Jahr */
	      sprintf(pclSelection, pclSelAcc, &pclActCedaTime[4], &pclActCedaTime[0]);

	      dbg(DEBUG,"%s: SQL-Cmd  <%s> for SalaerCode surn <%s> in acctab",
               pclFct, pclSelection, pclSco.surn);

	      strcpy(pclSqlData, pclSco.surn);

/* read in ACCTAB field CO?? */
	      ilRCAcc = sql_if(START, &slCurAcc, pclSelection, pclSqlData);
	      if (ilRCAcc == RC_SUCCESS)
          {
	        strcpy(pclSco.cpfc, pclSqlData);
	        dbg(DEBUG,"%s: Found Salaercode <%s>", pclFct, pclSqlData);
	      }
          else
          {
	        ilRC = RC_FAIL;
	        if(ilRCAcc != SQL_NOTFOUND) {
	          /* SQL - Error */
	          get_ora_err (ilRCStf, pclErrTxt);     
	          dbg(TRACE,"%s: SQL-ERROR - <%s>",pclFct,pclErrTxt);
	        } else {
	          dbg(TRACE,"%s: SURN no ACC line found <%s>",pclFct,pclSco.surn);
	        }
	      } /* end of RCAcc == RC_SUCCESS*/

	      if (ilRC == RC_SUCCESS){
	        /* Pick PENO for entry in STFTAB */
	        strcpy(pclSqlData, pclSco.surn);
		  
	        ilRCStf = sql_if(START, &slCurStf, pclSelectStf, pclSqlData);
	        if (ilRCStf == RC_SUCCESS) {
	          strcpy(pclSco.peno, pclSqlData);
	          dbg(DEBUG,"%s: Found Peno <%s>",pclFct,pclSqlData);
	        } else {
	          if(ilRCStf != SQL_NOTFOUND){
		        /* SQL - Error */
		        get_ora_err (ilRCStf, pclErrTxt);     
		        dbg(TRACE,"%s: SQL-ERROR - <%s>",pclFct,pclErrTxt);
		        ilRC = RC_FAIL;
	          } else {
		        dbg(TRACE,"%s: SURN no PENO line found  in STFTAB ERROR <%s>",
                  pclFct,pclSco.surn);
		        ilRC = RC_FAIL;
	          }
	        } /* RCStf == RC_SUCCESS*/
	      } /* end of ilRC == RC_SUCCESS*/
	    } /* end else */

/* eth MUC write */
	    if (ilRC == RC_SUCCESS)
        {
	      /* Schreibe Daten in Datei */
	      fprintf(fp_Sco,"%64s%.6s%255s%.8s%9s%.4s%62s\n",
		          " ",
		          pclSco.peno,
		          " ",
		          pclSco.vafr,
		          " ",
		          pclSco.cpfc,
		          " ");
	      ilRecCount++;
	    }
      }  /* end if ilRCSco1 == RC_SUCCESS */ 
    } while (ilRCSco1 == RC_SUCCESS);

    close_my_cursor(&slCurSco1);
    close_my_cursor(&slCurSco2);
    close_my_cursor(&slCurStf);
    close_my_cursor(&slCurAcc);
    
    fprintf(fp_Sco,"END\n");	    
    fclose(fp_Sco);  

    if (ilRecCount > 0){
      dbg(TRACE,"%s: Number of Records found  <%d>", pclFct, ilRecCount);
      SendCedaEvent( igToFtphdl, 0, pcgDestName, pcgRecvName, pcgTwStart, pcgTwEnd,
                     "MUC", " ", "         ", " ", " ", NULL, 4, NETOUT_NO_ACK);

      dbg(DEBUG,"%s:Send Event <MUC> to ftphdl ", pclFct);

    } else {
      dbg(TRACE,"%s: No Records found ", pclFct);
    }

  } /* end of if  file opened*/
    
  return ilRC;
} /* end of HandleMUC() */

/*****************************************************************************/
/* Function:   HandleMOR()                                                   */
/*****************************************************************************/
/* Parameter:                                                                */
/*                                                                           */
/* Global Variables:                                                         */
/* Read  : char  *pcgStammDatFile   - filename stammdata                     */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description: Insert or Update Records from File in Table ADRTAB           */
/*              Updates Shared Memory for Swissport                          */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*          22.12.99  rkl  Written                                           */
/*                                                                           */
/*****************************************************************************/
static int HandleMOR(void)
{
	char    *pclFct = "HandleMOR";
    int       ilRC  = RC_SUCCESS;
    
    return ilRC;
} /* end of HandleMOR() */

/*****************************************************************************/
/* Function:   HandlePGN()                                                   */
/*****************************************************************************/
/* Parameter:                                                                */
/* IN    :  char *pcpData  - Data										     */
/*                                                                           */
/* Global Variables:                                                         */
/* Read  :                                                                   */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description: Insert PrognoseData into Table PGNTAB                        */
/*              First Line of Data contains Fieldslist,other lines Data      */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*          22.12.99  rkl  Written                                           */
/*                                                                           */
/*****************************************************************************/
static int HandlePGN( char *pcpData)
{
  char *pclFct  = "HandlePGN";
  char *pclPgnTable = "PGNTAB";
  int    ilRC   = RC_SUCCESS;
  int    ilRCIns= RC_SUCCESS;
  int    ilSend = RC_SUCCESS;
  int    ilItemLen  = 0;
  int    ilLen      = 0;
  int    ilCount    = 0;
  int    ilInsert   = 0;
  int    ilInsertDB = 0;
  int    ilNoAft    = 0;
  int    ili        = 0;
  int    ilj        = 0;
  int    ilWeekday  = 0;
  short  slFkt      = START;
  short  slCurIns   = 0;
  long   llUrno     = 0;
  long   llNumOfUrnos = 0;
  char *pclDataPtr  = NULL;
  char *pclPtrStrg  = NULL;
  char  pclUrno[16] = "";
  char  pclAftUrno[16] = "";
  char  pclAlc2[8] = "";
  char  pclAlc3[8] = "";
  char  pclAirl[8] = "";
  char  pclFltNum[8] = "";
  char  pclFltSuf[8] = "";
  char  pclArrDep[8] = "";
  char  pclMonth[8] = "";
  char  pclWeekday[8]= "";
  char  pclCedaTimestamp[24] = "";
  char  pclErrTxt[256]      = "";
  char  pclTmpStr[4096]     = "";
  char  pclFields[12*1024]  = "";
  char  pclSqlBuf[12*1024]  = "";
  char  pclSqlData[12*1024] = "";
  char  pclDataTmp[12*1024] = "";
  static time_t tlBeginStamp=0;
  time_t        tlEndStamp=0;
  time_t        tlStampDiff=0;

  char  *pclUrnoList = NULL;

  static char *pclFldList = NULL;
  static int    ilAlcFldListSize = 0;
  static char *pclDataLine = NULL;
  static int    ilAlcDataLineSize = 0;
  
	ilRC = GetCedaTimestamp(&pclCedaTimestamp[0],15,0);

  	/* Set pointer to Data */
  	pclDataPtr = pcpData;
  
  	/* Get FieldList from Data */
  	ilItemLen = strcspn(pclDataPtr,"\n");
  	ilRC = HandleBuffer(&pclFldList,&ilAlcFldListSize,0,ilItemLen+32,128);
  	if(ilRC != RC_SUCCESS)
  	{
    	dbg(TRACE,"%s: ERROR - Function HandleBuffer() failed for Fieldlist",pclFct);
    	ilRC = RC_FAIL;
  	} /* end if */
  	else
  	{
    	ilItemLen = GetNextDataItem(pclFields, &pclDataPtr, "\n", "\0", "  ");

    	if (ilItemLen > 0)
    	{
       		/* Add AFTU,USEC,CDAT,HOPO,URNO to FieldList */
	    	sprintf(pclFldList,"%s%s",pclFields,",AFTU,USEC,CDAT,HOPO,URNO");
      
    	} /* end if */
		dbg(DEBUG,"<%s>: FieldList <%s>",pclFct,pclFldList);
    
    	tlBeginStamp = time(0L);
    	/* count Data Lines  and Get URNO's */
    	ilCount = CountDelimiter(pclDataPtr,"\n");
		dbg(TRACE,"<%s>: %d data lines found",pclFct,ilCount);
    	sprintf(pclTmpStr,"%d",ilCount);
		if(ilCount == 0)
		{
	   		dbg(TRACE,"<%s> ERROR - No Dataline found",pclFct);
       		ilRC = RC_FAIL;
		}

		for (ili=1; ili <= ilCount; ili++)
    	{
   			dbg(DEBUG,"<%s> ---------- Start Dataline %d ----------",pclFct,ili);

	  		if(ili%100 == 0)
	  		{
	   			dbg(TRACE,"<%s> Working  Dataline %d from %d",pclFct,ili,ilCount);
      		}
	  
	  		ilItemLen = strcspn(pclDataPtr,"\n");
      		ilRC = HandleBuffer(&pclDataLine,&ilAlcDataLineSize,0,ilItemLen+24+16,128);
      		if(ilRC != RC_SUCCESS) 
      		{
        		dbg(TRACE,"%s: ERROR - Function HandleBuffer failed for DataLine %d",
		   			       pclFct,ilCount);
        		ilRC = RC_FAIL;
      		} /* end if */

      		ilItemLen = GetNextDataItem(pclDataLine, &pclDataPtr, "\n", "\0", "  ");
      		if (ilItemLen > 0)
      		{
        		pclPtrStrg = pclDataLine;
        		ilItemLen = GetNextDataItem(pclAlc2   ,&pclPtrStrg, ",", "\0", "  ");
				if(ilItemLen > 0)
		  		{
					strcpy(pclAirl,pclAlc2);
				}
        		ilItemLen = GetNextDataItem(pclAlc3   ,&pclPtrStrg, ",", "\0", "  ");
				if(ilItemLen > 0)
				{
    				strcpy(pclAirl,pclAlc3);
				}

        		ilItemLen = GetNextDataItem(pclFltNum ,&pclPtrStrg, ",", "\0", "  ");
        		ilItemLen = GetNextDataItem(pclFltSuf ,&pclPtrStrg, ",", "\0", "  ");
        		ilItemLen = GetNextDataItem(pclArrDep ,&pclPtrStrg, ",", "\0", "  ");
        		ilItemLen = GetNextDataItem(pclMonth  ,&pclPtrStrg, ",", "\0", "  ");
        		ilItemLen = GetNextDataItem(pclTmpStr ,&pclPtrStrg, ",", "\0", "  ");
        		if(ilItemLen==1)
				{
					ilWeekday =atoi(pclTmpStr);
		    		switch(ilWeekday)
					{
						case 1:
				   			strcpy(pclWeekday,"1      ");
							break;
						case 2:
				        	strcpy(pclWeekday," 2     ");
							break;
						case 3:
				        	strcpy(pclWeekday,"  3    ");
							break;
						case 4:
				        	strcpy(pclWeekday,"   4   ");
							break;
						case 5:
				        	strcpy(pclWeekday,"    5  ");
							break;
						case 6:
				        	strcpy(pclWeekday,"     6 ");
							break;
						case 7:
				        	strcpy(pclWeekday,"      7");
							break;
						default:
				        	dbg(TRACE,"%s: ERROR - Unknown Weekday <%d>",pclFct,ilWeekday);
				        	strcpy(pclWeekday,"       ");
							break;
					} /* end Switch */
				} 
			
        		dbg(DEBUG,"%s: GetFlightUrnos for:<%s><%s><%s><%s><%s><%s>",pclFct,
            			     pclMonth,pclAirl,pclFltNum,pclFltSuf,pclWeekday,pclArrDep);

        		ilRC = GetFlightUrnos(pclMonth,pclAirl,pclFltNum,pclFltSuf,
				                      pclWeekday,pclArrDep,&pclUrnoList,
									  &llNumOfUrnos);
				if(ilRC!= RC_SUCCESS)
				{
           			dbg(TRACE,"%s: ERROR - GetFlightUrnos() failed",pclFct);
		   			ilRC = RC_FAIL;
				}
				  
				dbg(DEBUG,"%s: GetFlightUrnos result: Anz<%d> List<%s>",pclFct,
		      	           llNumOfUrnos, pclUrnoList);
				pclPtrStrg = pclUrnoList;

				if(llNumOfUrnos == 0)
				{
		          dbg(TRACE,"%s:<%d> FLIGHT <%s><%s><%s><%s><%s><%s> NOT IN AFTTAB",pclFct,
				       ilCount,pclAirl,pclFltNum,pclFltSuf,pclArrDep,pclMonth,pclWeekday);
			      ilNoAft++;
			    }
				else
				{
				  ilInsert++;
				}
        		/* Build Sql-String for INSERT */
				BuildSqlFldStr(pclFldList, "", FOR_INSERT, pclFields);
				sprintf(pclSqlBuf, "INSERT INTO %s %s", pclPgnTable, pclFields);
        		/* dbg(DEBUG,"%s: SQL-Cmd <%s>",pclFct,pclSqlBuf);*/

				for(ilj=0;ilj<llNumOfUrnos;ilj++)
				{
		
					/* get next Urno */
	 				ilRC = GetNextValues(pclUrno,1);
					if(ilRC != RC_SUCCESS)
					{
	   					dbg(TRACE,"%s: ERROR - Unable to generate URNO",pclFct);
       					ilRC = RC_FAIL;
					}

	            	/* get AFT Urno */
    	        	ilItemLen = GetNextDataItem(pclAftUrno ,&pclPtrStrg, ",", "\0", "  ");
			
        
	    	    	/* Append AFTU,USEC,CDAT,HOPO,URNO to DataLine */
	        		sprintf(pclSqlData,"%s,%s,%s,%s,%s,%s",
	                            pclDataLine,pclAftUrno,"EXCO", 
								pclCedaTimestamp,pcgH3LC,pclUrno);      
       
	   
        			/* dbg(DEBUG,"%s: Insert-DataLine <%s>",pclFct,pclSqlData);*/


	        		strcpy(pclDataTmp,pclSqlData); /* Save Data for Broadcast etc */
					ilLen = Del2NewDel(pclSqlData, ',', '\0');

        			/* write Data to DB */
					slFkt = START;
					ilRCIns = sql_if(slFkt, &slCurIns, pclSqlBuf, pclSqlData);
					if (ilRCIns != RC_SUCCESS)
					{
						get_ora_err(ilRCIns, pclErrTxt);
		    			dbg(TRACE, "%s: ERROR - INSERT FAILED <%s> ", pclFct, pclErrTxt);
            			ilRC = RC_FAIL;
					}
					else
					{
					  dbg(DEBUG,"%s: INSERT OK - URNO <%s>",pclFct,pclUrno);
					  ilInsertDB++;
					}
        			commit_work();

        			/* Send Broadcast */
        			if (ilRCIns == RC_SUCCESS)         
        			{
          				/* send message to BROADCASTHANDLER  */ 
          				/* dbg(DEBUG,"%s: Send message to BROADCASTHANDLER",pclFct);*/
		  				ilSend = SendCedaEvent( igToBcHdl, 0, pcgDestName, pcgRecvName,
               			                pcgTwStart, pcgTwEnd, "IRT",
                        		        pclPgnTable, "", pclFldList,
                                		pclDataTmp, pclErrTxt, 0, ilRC);
		    
        			} /* end if */    
				} /* end of for */
			} /* end of if */
      	} /* end of for */	  
	    tlEndStamp = time(0L);
    	tlStampDiff = tlEndStamp - tlBeginStamp;
    	if(ilRC == RC_SUCCESS)
    	{
      		dbg(TRACE,"%s: %d Records - %d inserted (DB %d)/ %d not insert; Need %d s",pclFct,
			               ilCount,ilInsert,ilInsertDB,ilNoAft,tlStampDiff);
    	}

	} /* end else */

  dbg(TRACE,"%s: Fertig Import Prognosedaten",pclFct);
    
  return ilRC;

} /* end of HandlePGN() */


/*****************************************************************************/
/* Function:   GetSubStrg()                                                  */
/*****************************************************************************/
/* Parameter:                                                                */
/* In    : char  *pcpSrcStrg - Pointer to SourceString                       */
/* In    : int     ipPosition- Position in SourceString                      */
/* In    : int     ipLength  - Length of SubString                           */
/* In    : int     ipTrim    - TRUE/FALSE                                    */
/* OUT   : char  *pcpSubStrg - Pointer to SubString (Result)                 */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:                                                              */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           22.12.99  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int GetSubStrg(char *pcpSrcStrg, int ipPosition, int ipLength,
                      int ipTrim, char *pcpSubStrg)
{
	char           *pclFct = "GetSubStrg";
	int             ilRC = RC_SUCCESS;
	int             ili;
	char           *pclStrgPtr = NULL;
	char            pclTmpStrg[1024];


	if (strlen(pcpSrcStrg) < 1024)	/* check Stringlength */
	{
		pclStrgPtr = pcpSrcStrg;
		/*
		 * dbg(DEBUG,"%s: Source <%s> PositionString <%d> length
		 * <%d>", pclFct,pclStrgPtr,ipPosition,ipLength);
		 */
		pclStrgPtr += ipPosition;

		strncpy(pclTmpStrg, pclStrgPtr, ipLength);
		pclTmpStrg[ipLength] = 0x00;

		if (ipTrim == TRUE)
		{
			pclStrgPtr = pclTmpStrg;

			/* trim left side of String */
			for (ili = 0; ili <= (int) strlen(pclTmpStrg) && isspace(pclTmpStrg[ili]); ili++)
			{
				pclStrgPtr++;
			}	/* end of for */

			/* trim right side of String */
			for (ili = strlen(pclTmpStrg) - 1; ili >= 0 && isspace(pclTmpStrg[ili]); ili--)
			{
			};
			pclTmpStrg[++ili] = 0x00;	/* string end */

		}
		strcpy(pcpSubStrg, pclStrgPtr);
		/* dbg(DEBUG,"%s: Return <%s> ",pclFct,pcpSubStrg); */

		ilRC = RC_SUCCESS;

	} 
	else
	{
		dbg(TRACE, "%s: ERROR - Sourcestring is to long", pclFct);
		ilRC = RC_FAIL;
	}

	return ilRC;

} /* end of GetSubStrg() */



/*****************************************************************************
 * Function:    Del2NewDel
 * Parameter:   IN/OUT : pcpDelList     String to prepare
 *              IN     : pcpDel         Delimiter to change
 *                       pcpNewDel      New delimiter
 * Return:      Length of prepared string, RC_FAIL (-1)
 * Description: This function turns delimiters to new delimiter.
 *              If there is one delimiter directly behind
 *              another a blanc will be added. Also if first or
 *              last sign are delimiters.
 * History:     10.05.99 TWE Written
 *
 *****************************************************************************/
static int Del2NewDel(char *pcpDelList, char pcpDel, char pcpNewDel)
{
	int             ilRC = RC_SUCCESS;
	int             ilLen = 0;
	int             ilBufCount = 0;
	int             ilCount = 0;
	short           slFlag = FALSE;
	char            pclDel[2] = "";
	static char    *pclBuf = NULL;
	static int      ilBufSize = 0;

	if ((pcpDelList == NULL) || (*pcpDelList == '\0'))
	{
		dbg(TRACE, "Del2NewDel: Delimiter List is empty or NULL");
		ilRC = RC_FAIL;
		return ilRC;
	}
	/* get size to alloc for local buffer */
	ilLen = strlen(pcpDelList);
	pclDel[0] = pcpDel;
	pclDel[1] = 0x00;
	ilCount = CountDelimiter(pcpDelList, pclDel);
	/* alloc local buffer */
	ilRC = HandleBuffer(&pclBuf, &ilBufSize, 0, ilLen + ilCount + 1, 512);
	if (ilRC != RC_SUCCESS)	/* only if wrong parameters send */
	{
		dbg(TRACE, "HandleBuffer failed");
		ilRC = RC_FAIL;
		return ilRC;
	}
	ilBufCount = 0;
	if (*(pcpDelList) == pcpDel)	/* insert blanc if first is delimiter */
	{
		pclBuf[ilBufCount] = ' ';
		ilBufCount++;
		pclBuf[ilBufCount] = pcpNewDel;
		ilBufCount++;
	}
	 /* end if */ 
	else
	{
		pclBuf[ilBufCount] = *(pcpDelList);
		ilBufCount++;
	}			/* end else */

	if ((ilLen > 0) && (*(pcpDelList + ilLen - 1) == pcpDel))	/* get last sign */
	{
		slFlag = TRUE;	/* last is delimiter */
	}			/* end if */
	for (ilCount = 1; ilCount < ilLen; ilCount++)	/* from second to last */
	{
		if (*(pcpDelList + ilCount) == pcpDel)
		{
			if (*(pcpDelList + ilCount - 1) == pcpDel)	/* insert blanc */
			{
				pclBuf[ilBufCount] = ' ';
				ilBufCount++;
			}	/* end if */
			pclBuf[ilBufCount] = pcpNewDel;
			ilBufCount++;
		}
		 /* end if */ 
		else
		{
			pclBuf[ilBufCount] = *(pcpDelList + ilCount);
			ilBufCount++;
		}		/* end else */
	}			/* end for */

	/* check if last is komma */
	if (slFlag == TRUE)	/* insert blanc if last = delimiter */
	{
		pclBuf[ilBufCount] = ' ';
		ilBufCount++;
	}			/* end if */
	memcpy(pcpDelList, pclBuf, ilBufCount);
	pcpDelList[ilBufCount] = 0x00;	/* terminate (delimit != NULL) */

	return ilBufCount;

}				/* end of Del2NewDel */

/*****************************************************************************/
/* Function:   BuildSqlFldStr()                                              */
/*****************************************************************************/
/* Parameter:                                                                */
/* In    : char  *pcpFldLst      - Field List                                */
/* In    : char  *pcpAddLst      - items to add to field list                */
/* In    : int    ipTyp          - kind of SQL string                        */
/* Out   : char  *pcpSqlStrg     - resulting SQL string                      */
/* */
/* Global Variables:                                                         */
/* */
/* Returncode:  RC_SUCCESS - All Ok                                          */
/* */
/* */
/* Description: Builds up the field list parts of insert                     */
/*              and update SQL statements.                                   */
/* */
/* History:                                                                  */
/*           04.05.99  twe  Written                                          */
/* */
/*****************************************************************************/
static int BuildSqlFldStr(char *pcpFldLst, char *pcpAddLst, 
int ipTyp, char *pcpSqlStrg)
{
	char           *pclFct = "BuildSqlFieldStr";
	int             ilRC = RC_SUCCESS;
	int             ilItemCnt = 0;	/* num item in item strings       */
	int             ilItem = 0;	/* index counter for item strings */
	int             ilItmLen = 0;	/* length of one fieldstr item    */
	int             ilStrLen = 0;	/* index counter in sql string    */
	int             ilAddLen = 0;	/* length of add string           */
	char            pclTmpBuf[FIELD_STR_LEN] = "";	/* help buffer for
							 * sqlstr         */
	char            pclItemNam[8] = "";	/* name of actual item            */
	char            pclItemVal[16] = "";	/* sql string for this item       */
	char            pclDel[5] = ",";
	char           *pclItem = NULL;

	ilAddLen = strlen(pcpAddLst);
	ilItemCnt = CountDelimiter(pcpFldLst, ",");
	switch (ipTyp)
	{
	case FOR_UPDATE:
		ilStrLen = 0;
		pclTmpBuf[ilStrLen] = 0x00;
		/* for all items found in field list  */
		pclItem = pcpFldLst;
		for (ilItem = 1; ilItem <= ilItemCnt; ilItem++)
		{
			ilItmLen = GetNextDataItem(pclItemNam, &pclItem, pclDel, "\0", "  ");
			sprintf(pclItemVal, "%s=:V%s", pclItemNam, pclItemNam);
			StrgPutStrg(pclTmpBuf, &ilStrLen, pclItemVal, 0, 2 * ilItmLen + 2, ",");
		}		/* end for */
		/* for all items found in add list  */
		if (ilAddLen > 0)
		{
			if (pcpAddLst[ilAddLen - 1] == ',')
			{
				pcpAddLst[ilAddLen - 1] = 0x00;	/* rem last komma  */
				ilAddLen--;
			}
			ilItemCnt = CountDelimiter(pcpAddLst, ",");
			pclItem = pcpAddLst;
			for (ilItem = 1; ilItem <= ilItemCnt; ilItem++)
			{
				ilItmLen = GetNextDataItem(pclItemNam, &pclItem, pclDel, "\0", "  ");
				sprintf(pclItemVal, "%s=:V%s", pclItemNam, pclItemNam);
				StrgPutStrg(pclTmpBuf, &ilStrLen,
				      pclItemVal, 0, 2 * ilItmLen + 2, ",");
			}	/* end for */
		}		/* end if */
		/* terminate pcpSqlStrg */
		if (ilStrLen > 0)	/* item found  */
		{
			ilStrLen--;
			pclTmpBuf[ilStrLen] = 0x00;
		}		/* end if */
		strcpy(pcpSqlStrg, pclTmpBuf);

		/*
		 * RESULT:
		 * >Field1=:VField1,Field2=:VField2,...,Fieldn=:VFieldn<
		 */
		break;

	case FOR_INSERT:
		ilStrLen = 0;
		pclTmpBuf[ilStrLen] = 0x00;
		/* for all items found in field list  */
		pclItem = pcpFldLst;
		for (ilItem = 1; ilItem <= ilItemCnt; ilItem++)
		{
			ilItmLen = GetNextDataItem(pclItemNam, &pclItem, pclDel, "\0", "  ");
			sprintf(pclItemVal, ":V%s", pclItemNam);
			StrgPutStrg(pclTmpBuf, &ilStrLen,
				    pclItemVal, 0, ilItmLen + 1, ",");
		}		/* end for */

		/* for all items found in add list  */
		if (ilAddLen > 0)
		{
			if (pcpAddLst[ilAddLen - 1] == ',')
			{
				pcpAddLst[ilAddLen - 1] = 0x00;	/* rem last komma  */
				ilAddLen--;
			}
			ilItemCnt = CountDelimiter(pcpAddLst, ",");
			pclItem = pcpAddLst;
			for (ilItem = 1; ilItem <= ilItemCnt; ilItem++)
			{
				ilItmLen = GetNextDataItem(pclItemNam, &pclItem, pclDel, "\0", "  ");
				sprintf(pclItemVal, ":V%s", pclItemNam);
				StrgPutStrg(pclTmpBuf, &ilStrLen,
					  pclItemVal, 0, ilItmLen + 1, ",");
			}	/* end for */
		}		/* end if */
		/* terminate pclTmpBuf */
		if (ilStrLen > 0)	/* item found  */
		{
			ilStrLen--;
			pclTmpBuf[ilStrLen] = 0x00;
		}		/* end if */
		if (ilAddLen > 0)
		{
			sprintf(pcpSqlStrg, "(%s,%s) VALUES (%s)",
				pcpFldLst, pcpAddLst, pclTmpBuf);
		}
		 /* end if */ 
		else
		{
			sprintf(pcpSqlStrg, "(%s) VALUES (%s)",
				pcpFldLst, pclTmpBuf);
		}		/* end else */
		/*
		 * RESULT: >(Field1,Field2,...,Fieldn) VALUES
		 * (:VField1,:VField2,...,:VFieldn)<
		 */
		break;

	default:
		dbg(TRACE, "%s: No defined SQL type", pclFct);
		break;
	}			/* end switch */

	return ilRC;
}				/* end of BuildSqlFieldStr */

/*****************************************************************************
 * Function:    CountDelimiter
 * Parameter:   IN/OUT : pcpChkStr      check string
 *                       pcpDel         Delimiter string (1 or more)
 *
 * Return:      int - Number of items
 *
 * Description: Returns the Number of items in check string
 *              that are seperated by the delimiter.
 *              Examples: "abc"          => 1
 *                        "abc,def"      => 2
 *                        "abc,"         => 2
 *                        ",abc"         => 2
 *                        ",abc,"        => 3
 *                        "abc,def,ghi"  => 3
 *
 * History:      10.05.99 TWE Written
 *
 *****************************************************************************/
static int CountDelimiter(char *pcpChkStr, char *pcpDel)
{
	int             ilCount = 0;
	int             ilDelLen = 0;
	char           *pclStrPtr = NULL;

	pclStrPtr = pcpChkStr;

	if (*pclStrPtr != '\0')
	{
		ilDelLen = strlen(pcpDel);
		if (ilDelLen > 1)	/* words  */
		{
			while ((pclStrPtr = strstr(pclStrPtr, pcpDel)) != NULL)	/* Del found */
			{
				pclStrPtr += ilDelLen;
				ilCount++;
			}
		} else		/* one sign  */
		{
			while (*pclStrPtr != '\0')
			{
				if (*pclStrPtr == pcpDel[0])	/* Del found */
				{
					ilCount++;
				}
				pclStrPtr++;
			}
		}
		ilCount++;
	}
	return ilCount;

}				/* end of CountDelimiter */


/*****************************************************************************/
/* Function:   UpdateShm()                                                   */
/*****************************************************************************/
/* Parameter:                                                                */
/* In    : char  *pcpUrno   - Pointer to URNO                                */
/* In    : char  *pcpTable  - Pointer to Tablename                           */
/* In    : char  *pcpType   - Pointer to Type Update/Insert                  */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:      Update Shared Memory                                    */
/*                   if URNO is "LOAD_ALL_TABLE  Table will be reloaded      */
/*                   Type is "IRT" or "URT" or "DEL"                         */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           11.01.00  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
static int UpdateShm (char *pcpUrno, char *pcpTable, char *pcpCmd)
{
  char *pclFct = "UpdateShm";
  int   ilRC          = RC_SUCCESS;
  int   ilShm         = RC_SUCCESS;
  int   ilShm2        = RC_SUCCESS;
  int   ilNumUrno     = 0;
  int   ilCount       = 0;
  int   ilItmLen      = 0;
  char  pclUrnoStr[16];
/*
 *  dbg(DEBUG,"%s: Parameter: pcpUrno  <%s>",pclFct,pcpUrno);
 *  dbg(DEBUG,"%s: Parameter: pcpTable <%s>",pclFct,pcpTable);
 *  dbg(DEBUG,"%s: Parameter: pcpType  <%s>",pclFct,pcpType);
 */


  if (strcmp("LOAD_ALL_TAB",pcpUrno) == NULL)
  {
     dbg(TRACE,"%s: UPDATE SHARED MEMORY ->RELOADING TABLE <%s>", pclFct, pcpTable);
     if ((ilShm2 = syslibReloadDbData (pcpTable)) != RC_SUCCESS)
     {
       dbg(TRACE,"%s: ERROR - Reloading Table %s", pclFct,pcpTable);
	   ilRC = RC_FAIL;
     } /* end if */
  }
  else
  {
  
        dbg(DEBUG,"%s: URNO = <%s>", pclFct, pcpUrno);

        if (strcmp("IRT", pcpCmd) == NULL)
        {
           if ((ilShm=syslibUpdateDbData ('I', pcpTable, "URNO", pcpUrno))
                != RC_SUCCESS)
           {
              dbg(TRACE,"%s: ERROR - Update Shared Memory (IRT) Table<%s> URNO<%s> rc<%d>", 
			            pclFct,pcpTable,pcpUrno,ilShm);
              ilRC= RC_FAIL;
           }
        } /* end if */
        else if (strcmp("URT", pcpCmd) == NULL)
        {
           if ((ilShm=syslibUpdateDbData ('U', pcpTable, "URNO", pcpUrno))
                != RC_SUCCESS)
           {
              dbg(TRACE,"%s: ERROR - Update Shared Memory (URT) Table<%s> URNO<%s> rc<%d>", 
			            pclFct,pcpTable,pcpUrno,ilShm);
              ilRC= RC_FAIL;
           }
        } /* end else if */
        else if (strcmp("DRT", pcpCmd) == NULL)
        {
           if ((ilShm=syslibUpdateDbData ('D', pcpTable, "URNO", pcpUrno))
                != RC_SUCCESS)
           {
              dbg(TRACE,"%s: ERROR - Update Shared Memory (DRT) Table<%s> URNO<%s> rc<%d>", 
			            pclFct,pcpTable,pcpUrno,ilShm);
              ilRC= RC_FAIL;
           }
        } /* end else if */
        else 
        {
              dbg(TRACE,"%s: ERROR - Unknown Cmd <%s>", 
			            pclFct,pcpCmd);
              ilRC= RC_FAIL;
        
        } /* end  if */

        /* if something fails reload table */
        if (ilShm != RC_SUCCESS)         
		{
            dbg(TRACE,"%s: UPDATE SHARED MEMORY FAILURE -> RELOADING TABLE <%s>", pclFct, pcpTable);
            if ((ilShm2 = syslibReloadDbData (pcpTable)) == RC_FAIL)
            {
               dbg(TRACE,"%s: ERROR - Reloading Table <%s>", pclFct,pcpTable);
	           ilRC = RC_FAIL;
            } /* end if */
        } /* end if */

  } /* end else */

  return ilRC;

} /* end of UpdateShm */

/*****************************************************************************/ 
/* Function:   GetUrnos()                                                    */
/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : char  *pcpActCmd       - Pointer to Actual Comman                 */
/* In    : char  *pcpData         - Number of URNO as char                   */
/* In    : char  *pcpFlag         - FIRST or ALL                             */
/* Out   : char  *pcpUrnos        - One Urno or List of Urnos (',' seperated)*/
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - All Ok                                          */
/*              RC_FAIL    - Failed                                          */
/*                                                                           */
/* Description: Fetches next number(s) of URNOS using Lib function.          */
/*              if Command = GMU : Number is written in Parameter Data.      */
/* Attention.   It's the callers responsibility That OutputBuffer is large   */
/*              enough                                                       */
/* History:                                                                  */
/*           15.06.99  rkl  Written (Replaces functions GetNextUrno(),       */
/*                          GetManyUrnos()                                   */
/*           04.08.99  rkl  removed global Variable insert Outputparameter   */
/*            9.9.99   rkl  insert pcpFlag                                   */
/*                                                                           */
/*****************************************************************************/
static int GetUrnos (char *pcpActCmd, char *pcpData, char *pcpFlag, char *pcpUrnos)
{
  char  *pclFct        = "GetUrnos";
  char  *pclNumCirc    = "SNOTAB";       /*  Indicator for Number table   */
  char  *pclFlag       = "GNV";          /*  Indicator for GetNextValues  */
  int    ilRC           = RC_SUCCESS;
  int    ilNeedBufSize  = 0;
  int    ilNoOfUrnos    = 1;    /* default is one Urno */
  long   llNextUrno     = 0;
  int    ilStrgLen      = 0;
  int    ili;


  if(strcmp(pcpActCmd,"GMU") == 0)
  {
    ilNoOfUrnos=atoi(pcpData);      /*  get requested num of URNOS  */
  } /* end if */
  
  if(ilRC == RC_SUCCESS)
  {
      dbg(DEBUG,"%s: Processing request for %d Urnos", pclFct, ilNoOfUrnos);
      /*  GetNextValues mapped to GetNextNumbers.  */
      ilRC = GetNextNumbers(pclNumCirc, pcpUrnos, ilNoOfUrnos, pclFlag);
      if (ilRC != RC_SUCCESS) 
      {
        dbg(TRACE,"%s: ERROR - GetNextNumbers failed <%d>", pclFct,  ilRC);
        dbg(TRACE,"%s: CMD <%s> - Unable to generate URNO", pclFct, pcpActCmd);

        ilRC = RC_FAIL;
      } /* end if */
      else 
      {
        if(strcmp(pcpFlag,"ALL") == 0)
		{
		  llNextUrno=atol(pcpUrnos);       /* only first valid URNO  */
          pcpUrnos[0] = 0x00;              /* Reset ResultBuffer     */
          ilStrgLen = 0;
          for (ili = 0; ili < ilNoOfUrnos; ili++) /* add up to wished number  */
          {
            sprintf(&pcgResultBuffer[ilStrgLen],"%ld,",llNextUrno++);
            ilStrgLen = strlen(pcpUrnos);
          } /* end for */
        	
          ilStrgLen--;                            /* Remove last comma */
	      pcpUrnos[ilStrgLen] = 0x00;  	
        } /* end if */
     } /* end else  */
  } /* end if */

  return ilRC;

} /* end of GetUrnos  */



/*****************************************************************************/
/* GetFlightUrnos  ask Torge for following Functions                         */ 
/*****************************************************************************/


/*****************************************************************************/
/* Function:   GetFlightUrnos()                                              */
/*****************************************************************************/
/* Parameter:                                                                */
/* IN    : char  *pcpMonth                                                   */
/* IN    : char  *pcpAirl     - Airline                                      */
/* IN    : char  *pcpFltNum   - Flightnumber                                 */
/* IN    : char  *pcpFltSuf   - FlightSuffix                                 */
/* IN    : char  *pcpWeekDays - Weekdays                                     */
/* IN    : char  *pcpAdid     - Arrival or Depature Identifier               */
/* OUT   : char **pcpUrnoList - Commaseperated UrnoList                      */
/* OUT   : long  *plpNumUrnos - Number of Urnos returned                     */
/*                                                                           */
/* Global Variables:                                                         */
/* In    : char                                                              */
/* Out   : long                                                              */
/* In/Out: int                                                               */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Failure                                         */
/*                                                                           */
/* Description:  Return Urnolist for flights                                 */
/*                                                                           */
/* Note:                                                                     */
/*                                                                           */
/* History:                                                                  */
/*           10.04.00  twe  Written                                          */
/*                                                                           */
/*****************************************************************************/

static int GetFlightUrnos (char *pcpMonth, char *pcpAirl, char *pcpFltNum,
                           char *pcpFltSuf, char *pcpWeekDays, char *pcpAdid,
                           char **pcpUrnoList, long *plpNumUrnos)
{
int ilRC = RC_SUCCESS;

  ilRC = GetFlightDay (pcpMonth, pcpAirl, pcpFltNum, pcpFltSuf,
                       pcpWeekDays, pcpAdid, pcpUrnoList, plpNumUrnos);

  
  return ilRC;

} /* GetFlightUrnos */

static int GetFlightDay (char *pcpMonth, char *pcpAirl, char *pcpFltNum,
                         char *pcpFltSuf, char *pcpWeekDays, char *pcpAdid,
                         char **pcpUrnoList, long *plpNumUrnos)
{
int ilGetRc = RC_SUCCESS;
int ilRC = RC_SUCCESS;
int ilYear;
int ilVpfrVal;
int ilMinVal;
int ilVpfrDay;
int ilVptoVal;
int ilVptoDay;
int ilFreaCount;
int ilFreqVal;
int ilValidDays;
int ilWeeks;
int ilUrnos;
int ilWeekDay;
int ilValidWeek;
int ilDayCount;
int ilLoopDay;
int ilChckDay;
long llHelpSize;
char pclFreaStrg[20];
char pclFreaChrs[20];
char pclVpfrStrg[20];
char pclVptoStrg[20];
char pclYear[10];
char pclMon[10];
char pclMonat[10];
char pclFreqStrg[20];
char pclDaofDat[20]; 
char pclNewtime[20]; 
static char *pclUrnoBuffer = NULL;
static long llSizeUrnoBuffer = 0;

  *plpNumUrnos = 0;
  strcpy(pclDaofDat, "0");
  strcpy(pclYear, "2000");
  strcpy(pclMon, pcpMonth);
  if (strlen(pclMon) == 1)
  {
     sprintf(pclMon, "0%s", pcpMonth);
  }
  strcpy(pclMonat, pclMon);
  strcat (pclMon, ","); 

  sprintf(pclVpfrStrg, "%s%s01000000", pclYear,pclMonat);  

  if (strstr("01,03,05,07,08,10,12,", pclMon) != NULL)
  {
     sprintf(pclVptoStrg, "%s%s31235900", pclYear, pclMonat);
  }
  else if (strstr("04,06,09,11,", pclMon) != NULL)
  {
     sprintf(pclVptoStrg, "%s%s30235900", pclYear, pclMonat);
  }
  else
  {
     ilYear = atoi(pclYear);
     if (!(ilYear%4))
     {
        sprintf(pclVptoStrg, "%s%s29235900", pclYear, pclMonat);
     }
     else
     {
        sprintf(pclVptoStrg, "%s%s28235900", pclYear, pclMonat);
     }
  }
     
  ilGetRc = GetFullDay(pclVpfrStrg,&ilVpfrVal, &ilMinVal, &ilVpfrDay);
  if (ilGetRc == RC_SUCCESS)
  {
    ilGetRc = GetFullDay(pclVptoStrg,&ilVptoVal, &ilMinVal, &ilVptoDay);
  } /* end if */

  if (ilGetRc == RC_SUCCESS)
  {
    strcpy(pclFreaStrg, pcpWeekDays);
    strcpy(pclFreqStrg, "1");
    ilFreaCount = FormatDayFreq(pclFreaStrg, pclFreaChrs);
    ilFreqVal = atoi(pclFreqStrg);
    if (ilFreqVal < 1)
    {
      ilFreqVal = 1;
    } /* end if */
    if (ilFreqVal > 1)
    {
      dbg(TRACE,"WEEKLY FREQUENCY IS %d",ilFreqVal);
    } /* end if */
  } /* end if */
  else
  {
    dbg(TRACE,"ERROR IN PERIOD FROM <%s> TO <%s>", pclVpfrStrg,pclVptoStrg);
  } /* end else */

  if (ilGetRc == RC_SUCCESS)
  {
    /* GET NUMBER OF URNOS FOR WEEKLY FREQUENCY */
    ilWeeks = ((ilVptoVal - ilVpfrVal + 1) / 7) / ilFreqVal;
    ilWeeks++; 
    ilValidDays = ilWeeks * ilFreaCount;

    ilUrnos = ilValidDays;
    llHelpSize = ilUrnos * 11;
    if (llHelpSize > llSizeUrnoBuffer)
    {
       llSizeUrnoBuffer = ilUrnos * 11;
       if (pclUrnoBuffer == NULL)
       {
          pclUrnoBuffer = (char *)(malloc(llSizeUrnoBuffer)); 
       }
       else
       {
          pclUrnoBuffer = (char *)(realloc(pclUrnoBuffer, llSizeUrnoBuffer)); 
       }
    } 
    if (pclUrnoBuffer != NULL)
    {
       pclUrnoBuffer[0] = 0x00;
    }
    else
    {
       dbg(TRACE, "ALLOC / REALLOC OF pclUrnoBuffer failed");
       ilGetRc = RC_FAIL;
    }
  }

  if (ilGetRc == RC_SUCCESS)
  {
    ilUrnos = 0;
    pclVpfrStrg[8] = 0x00;
    ilWeekDay = ilVpfrDay;
    ilValidWeek = TRUE;
    ilValidDays = 0;
    ilDayCount = -1;
    for (ilLoopDay = ilVpfrVal; ilLoopDay <= ilVptoVal; ilLoopDay++)
    {
      ilDayCount++;
      if (ilValidWeek == TRUE)
      {
        ilChckDay = ilWeekDay - 1;
        if (pclFreaChrs[ilChckDay] != ' ')       /* if valid weekday */
        {
           strcpy(pclNewtime, pclVpfrStrg);
           strcat(pclNewtime, "000000");
           AddSecondsToCEDATime2(pclNewtime, ilDayCount*24*60*60, "LOC", 1);
           pclNewtime[8] = 0x00;
           ilRC = GetUrnoByFkey(pcpAirl, pcpFltNum, pcpFltSuf, pclNewtime,
                                   pcpAdid, pclUrnoBuffer);
           if (ilRC == RC_SUCCESS)
           {
             ilUrnos++; 
           }
        }
        ilValidDays++;
        if (ilValidDays == 7)
        {
          ilValidDays = (ilFreqVal - 1) * 7;
          if (ilValidDays > 0)
          {
            ilValidWeek = FALSE;
          } /* end if */
        } /* end if */
      } /* end if */
      else
      {
        /* Skip Over Not Valid Days */
        ilValidDays--;
        if (ilValidDays < 1)
        {
          ilValidWeek = TRUE;
        } /* end if */
      } /* end else */

      ilWeekDay++;
      if (ilWeekDay > 7)
      {
        ilWeekDay = 1;
      } /* end if */
    } /* end for */
  }
  if (ilUrnos > 0)
  {
     *plpNumUrnos = ilUrnos;
     pclUrnoBuffer[strlen(pclUrnoBuffer) -1] = 0x00;
     *pcpUrnoList = pclUrnoBuffer;
  }

dbg(DEBUG," Anzahl Urno(s)<%d> UrnoList <%s>",ilUrnos,pclUrnoBuffer);

  return ilGetRc;

}  /* GetFlightDay */


static int GetUrnoByFkey (char *pcpAirl, char *pcpFltNum, char *pcpFltSuf,
                          char *pcpNewtime, char *pcpAdid, char *pcpUrnBuf)
{
int  ilRC = RC_SUCCESS;
int  ilLen;
int  ilSysCnt;
short slFkt = START;
short slCursor = 0;
char pclUrno[20];
char pclTmpBuf[20];
char pclSqlBuf[200];
char pclAlc2[5];
char pclAlc3[5];

  /* Build FKEY FROM DB */
  strcpy(pclTmpBuf,"00000####YYYYMMDD#");

  ilLen = strlen(pcpFltNum);
  memcpy(&(pclTmpBuf[5-ilLen]), pcpFltNum, ilLen);

  ilLen = strlen(pcpAirl);
  if (ilLen == 2)
  {
     strcpy(pclAlc2, pcpAirl);
     ilSysCnt = 1;
     ilRC = syslibSearchDbData("ALTTAB", "ALC2",
                                pclAlc2, "ALC3",
                                pclAlc3, &ilSysCnt,"");
     if (ilRC == RC_SUCCESS)
     {
        ilLen = 3;
        memcpy(&(pclTmpBuf[8-ilLen]), pclAlc3, ilLen);
     }
     else
     {
        memcpy(&(pclTmpBuf[8-ilLen]), pcpAirl, ilLen);
     }
  }
  else
  {
     memcpy(&(pclTmpBuf[8-ilLen]), pcpAirl, ilLen);
  }

  if ((pcpFltSuf[0] != ' ') && (pcpFltSuf[0] != '\0'))
  {
     pclTmpBuf[8] = pcpFltSuf[0];
  }

  ilLen = strlen(pcpNewtime);
  memcpy(&(pclTmpBuf[17-ilLen]), pcpNewtime, ilLen);

  pclTmpBuf[17] = pcpAdid[0];
  sprintf(pclSqlBuf, 
     "SELECT URNO FROM AFTTAB WHERE FKEY = '%s' AND (FTYP = 'O' OR FTYP = 'S' OR FTYP = 'X')",
     pclTmpBuf);
/* dbg(DEBUG,"SQL: <%s>",pclSqlBuf); */
  ilRC = sql_if(slFkt, &slCursor, pclSqlBuf, pclUrno);

/* dbg(TRACE,"SELECT return %d", ilRC); */

  if ((ilRC == DB_SUCCESS)||(ilRC == RC_SUCCESS))
  { 
     strcat(pcpUrnBuf, pclUrno);
     strcat(pcpUrnBuf, ",");
     ilRC = RC_SUCCESS;
  }
/* dbg(TRACE,"pcpUrnBuf <%s>", pcpUrnBuf); */

  close_my_cursor(&slCursor);
  
  return ilRC;

}  /* GetUrnoByFkey */

/******************************************************************************/
static int FormatDayFreq(char *pcpFreq,char *pcpFreqChr)
{
  int ilIdx;
  int ilChrPos;
  int ilStrLen;
  int ilDayCnt = 0;
  char pclTmpChr[] = "X";
  strcpy(pcpFreqChr,"       ");
  ilStrLen = strlen(pcpFreq);
  for (ilChrPos=0;ilChrPos<ilStrLen;ilChrPos++)
  {
    if (((pcpFreq[ilChrPos] >= '1') && (pcpFreq[ilChrPos] <= '7')) ||
         (pcpFreq[ilChrPos] == 'X'))
    {
      if (pcpFreq[ilChrPos] == 'X')
      {
        pcpFreqChr[ilChrPos] = ilChrPos + '1';
      } /* end if */
      else
      {
        pclTmpChr[0] = pcpFreq[ilChrPos];
        ilIdx = atoi(pclTmpChr) - 1;
        pcpFreqChr[ilIdx] = pcpFreq[ilChrPos];
      } /* end else */
      ilDayCnt++;
    } /* end if */
  } /* end for */
  return ilDayCnt;
} /* end FormatDayFreq */


/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
