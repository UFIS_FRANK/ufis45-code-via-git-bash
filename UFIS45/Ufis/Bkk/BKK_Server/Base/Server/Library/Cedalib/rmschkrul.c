
# line 2 "rmschkrul.y"
#include <stdio.h>
#include <string.h>
#include "glbdef.h"

void yyerror(char *s);
int  rc(int a, char c, int b);
int  fc(float a, char c, float b);
int  sc(char *s1, char c, char *s2);

static char sccs_vers_yacc[]="@(#) UFIS 4.3 (c) ABB AAT/I rmschkrul.y 44.1 / 00/01/10 12:10:17 / VBL"; 

static int igYaccRC = 0;  /* 0 if false, 1 if rule is true , -1 is syntax error */

int RmsCheckRule(char *pcpRule, int *pipRuleResult) ;


# line 19 "rmschkrul.y"
typedef union
#ifdef __cplusplus
	YYSTYPE
#endif
 {
        int     no ;
        float   real ;
        char    string[2048] ;
} YYSTYPE;
# define UNGLEICH 257
# define ODER 258
# define UND 259
# define REAL 260
# define STRING 261
# define VORZEICHEN 262

#include <inttypes.h>

#ifdef __STDC__
#include <stdlib.h>
#include <string.h>
#define	YYCONST	const
#else
#include <malloc.h>
#include <memory.h>
#define	YYCONST
#endif

#include <values.h>

#if defined(__cplusplus) || defined(__STDC__)

#if defined(__cplusplus) && defined(__EXTERN_C__)
extern "C" {
#endif
#ifndef yyerror
#if defined(__cplusplus)
	void yyerror(YYCONST char *);
#endif
#endif
#ifndef yylex
	int yylex(void);
#endif
	int yyparse(void);
#if defined(__cplusplus) && defined(__EXTERN_C__)
}
#endif

#endif

#define yyclearin yychar = -1
#define yyerrok yyerrflag = 0
extern int yychar;
extern int yyerrflag;
YYSTYPE yylval;
YYSTYPE yyval;
typedef int yytabelem;
#ifndef YYMAXDEPTH
#define YYMAXDEPTH 150
#endif
#if YYMAXDEPTH > 0
int yy_yys[YYMAXDEPTH], *yys = yy_yys;
YYSTYPE yy_yyv[YYMAXDEPTH], *yyv = yy_yyv;
#else	/* user does initial allocation */
int *yys;
YYSTYPE *yyv;
#endif
static int yymaxdepth = YYMAXDEPTH;
# define YYERRCODE 256

# line 79 "rmschkrul.y"




/******************************************************************************/
/* unary compare                                                              */
/******************************************************************************/
int rc(int a, char c, int b)
{
  switch (c)
  {
   case '&': return a&&b?1:0 ; 
   case '|': return a||b?1:0 ;
   default : return 1 ;  
  }
} /* function rc(...) */



/******************************************************************************/
/* float compare                                                              */
/******************************************************************************/
int fc(float a, char c, float b)
{ 
  switch (c)
  {
   case '>': return a>b?1:0 ;
   case '<': return a<b?1:0 ;
   case '=': return a==b?1:0 ;
   case '!': return a!=b?1:0 ; 
   default : return 1 ;  
  }
} /* function fc(...) */


/******************************************************************************/
/* string compare                                                             */
/******************************************************************************/
int sc(char *s1, char c, char *s2)
{ 
  int	ilRC ;

  ilRC = strcmp (s1,s2) ; 
  switch (c)
  {
   case '>': return ilRC>0?1:0 ;
   case '<': return ilRC<0?1:0 ;
   case '=': return ilRC==0?1:0 ;
   case '!': return ilRC!=0?1:0 ;
   default : return 1 ;  
  }
} /* function sc(...) */



#include "lex.rmschkrul.h"
#include "glbdef.h"

/***********************************************************************/
/* call parser for string                                              */
/***********************************************************************/
int RmsCheckRule(char *pcpRule, int *pipRuleResult)
{
  int  ilRC = 0 ;

  rmschkrulstring = pcpRule ;
  igYaccRC = 0 ;

  ilRC = yyparse() ;
  if (ilRC == 0)
  {
   switch (igYaccRC)
   {
    case -1 : /* syntax error in expression */
              *pipRuleResult = -1 ;
               ilRC = -1 ;
            break ;

    case 0 :  /* expression is true */
              *pipRuleResult = TRUE;
            break ;

    case 1 :  /* expression is false */
              *pipRuleResult = FALSE ;
            break ;

    default : /* error in source */
              dbg(TRACE,"RmsCheckRule: unexpected RC <%d> from yyparse()", igYaccRC) ;
              *pipRuleResult = -1 ;
               ilRC = -1 ;
            break ;

   } /* end of switch */
  } 
  else 
  {
   *pipRuleResult = -1 ;
  } /* end of if */

  return ilRC ;

} /* end of RmsCheckRule */



/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void yyerror(char *s)
{
 dbg (TRACE,"yyerror: <%s>",s) ;
 return ;
}


static YYCONST yytabelem yyexca[] ={
-1, 1,
	0, -1,
	-2, 0,
	};
# define YYNPROD 26
# define YYLAST 237
static YYCONST yytabelem yyact[]={

    36,    28,    26,     7,    27,     7,    29,    38,    11,    31,
    11,     1,    28,    26,    11,    27,    35,    29,    34,    23,
    24,    22,    14,    13,    19,    20,    18,     6,    28,    35,
    23,    24,    22,    29,     5,    19,    20,    18,    36,    28,
    26,     8,    27,     9,    29,    28,    26,     0,    27,    16,
    29,    17,     0,     0,     0,    30,     0,     2,     0,     0,
    37,    39,    40,    41,     4,    15,    42,    43,    44,    45,
    46,    47,    48,    49,     0,    50,     0,     0,    32,    33,
    51,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,    25,     0,     0,     3,
     0,    21,     0,    12,    10,    12,    10,    25,    10,    12,
     0,     0,    21,     0,     0,    14,    13 };
static YYCONST yytabelem yypact[]={

   -37,-10000000,  -236,-10000000,-10000000,-10000000,-10000000,   -35,   -36,   -30,
-10000000,   -31,-10000000,   -35,   -35,   -23,   -25,   -41,   -33,   -33,
   -33,   -33,   -31,   -31,   -31,   -31,   -31,   -31,   -31,   -31,
-10000000,   -31,-10000000,-10000000,-10000000,-10000000,-10000000,-10000000,   -33,-10000000,
-10000000,-10000000,     3,     3,     3,     3,   -14,   -14,-10000000,-10000000,
    -3,   -12 };
static YYCONST yytabelem yypgo[]={

     0,    34,    57,    64,    27,    11,    41,    43 };
static YYCONST yytabelem yyr1[]={

     0,     5,     5,     2,     2,     2,     3,     3,     3,     1,
     1,     1,     1,     6,     6,     4,     4,     4,     4,     7,
     7,     7,     7,     7,     7,     7 };
static YYCONST yytabelem yyr2[]={

     0,     3,     3,     7,     7,     3,     3,     3,     7,     7,
     7,     7,     7,     7,     3,     7,     7,     7,     7,     7,
     7,     7,     7,     7,     5,     3 };
static YYCONST yytabelem yychk[]={

-10000000,    -5,    -2,   256,    -3,    -1,    -4,    40,    -6,    -7,
   261,    45,   260,   259,   258,    -2,    -6,    -7,    62,    60,
    61,   257,    62,    60,    61,   257,    43,    45,    42,    47,
    -7,    40,    -3,    -3,    41,    41,    41,    -6,    40,    -6,
    -6,    -6,    -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,
    -7,    -6 };
static YYCONST yytabelem yydef[]={

     0,    -2,     1,     2,     5,     6,     7,     0,     0,     0,
    14,     0,    25,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    24,     0,     3,     4,     8,    13,    19,     9,     0,    10,
    11,    12,    15,    16,    17,    18,    20,    21,    22,    23,
     0,     0 };
typedef struct
#ifdef __cplusplus
	yytoktype
#endif
{ char *t_name; int t_val; } yytoktype;
#ifndef YYDEBUG
#	define YYDEBUG	0	/* don't allow debugging */
#endif

#if YYDEBUG

yytoktype yytoks[] =
{
	"UNGLEICH",	257,
	"ODER",	258,
	"UND",	259,
	"REAL",	260,
	"STRING",	261,
	"+",	43,
	"-",	45,
	"*",	42,
	"/",	47,
	"VORZEICHEN",	262,
	"-unknown-",	-1	/* ends search */
};

char * yyreds[] =
{
	"-no such reduction-",
	"gesamtregel : regel",
	"gesamtregel : error",
	"regel : regel UND teilregel",
	"regel : regel ODER teilregel",
	"regel : teilregel",
	"teilregel : alphaausdruck",
	"teilregel : realausdruck",
	"teilregel : '(' regel ')'",
	"alphaausdruck : alphaterm '>' alphaterm",
	"alphaausdruck : alphaterm '<' alphaterm",
	"alphaausdruck : alphaterm '=' alphaterm",
	"alphaausdruck : alphaterm UNGLEICH alphaterm",
	"alphaterm : '(' alphaterm ')'",
	"alphaterm : STRING",
	"realausdruck : realterm '>' realterm",
	"realausdruck : realterm '<' realterm",
	"realausdruck : realterm '=' realterm",
	"realausdruck : realterm UNGLEICH realterm",
	"realterm : '(' realterm ')'",
	"realterm : realterm '+' realterm",
	"realterm : realterm '-' realterm",
	"realterm : realterm '*' realterm",
	"realterm : realterm '/' realterm",
	"realterm : '-' realterm",
	"realterm : REAL",
};
#endif /* YYDEBUG */
# line	1 "/usr/ccs/bin/yaccpar"
/*
 * Copyright (c) 1993 by Sun Microsystems, Inc.
 */

#pragma ident	"@(#)yaccpar	6.16	99/01/20 SMI"

/*
** Skeleton parser driver for yacc output
*/

/*
** yacc user known macros and defines
*/
#define YYERROR		goto yyerrlab
#define YYACCEPT	return(0)
#define YYABORT		return(1)
#define YYBACKUP( newtoken, newvalue )\
{\
	if ( yychar >= 0 || ( yyr2[ yytmp ] >> 1 ) != 1 )\
	{\
		yyerror( "syntax error - cannot backup" );\
		goto yyerrlab;\
	}\
	yychar = newtoken;\
	yystate = *yyps;\
	yylval = newvalue;\
	goto yynewstate;\
}
#define YYRECOVERING()	(!!yyerrflag)
#define YYNEW(type)	malloc(sizeof(type) * yynewmax)
#define YYCOPY(to, from, type) \
	(type *) memcpy(to, (char *) from, yymaxdepth * sizeof (type))
#define YYENLARGE( from, type) \
	(type *) realloc((char *) from, yynewmax * sizeof(type))
#ifndef YYDEBUG
#	define YYDEBUG	1	/* make debugging available */
#endif

/*
** user known globals
*/
int yydebug;			/* set to 1 to get debugging */

/*
** driver internal defines
*/
#define YYFLAG		(-10000000)

/*
** global variables used by the parser
*/
YYSTYPE *yypv;			/* top of value stack */
int *yyps;			/* top of state stack */

int yystate;			/* current state */
int yytmp;			/* extra var (lasts between blocks) */

int yynerrs;			/* number of errors */
int yyerrflag;			/* error recovery flag */
int yychar;			/* current input token number */



#ifdef YYNMBCHARS
#define YYLEX()		yycvtok(yylex())
/*
** yycvtok - return a token if i is a wchar_t value that exceeds 255.
**	If i<255, i itself is the token.  If i>255 but the neither 
**	of the 30th or 31st bit is on, i is already a token.
*/
#if defined(__STDC__) || defined(__cplusplus)
int yycvtok(int i)
#else
int yycvtok(i) int i;
#endif
{
	int first = 0;
	int last = YYNMBCHARS - 1;
	int mid;
	wchar_t j;

	if(i&0x60000000){/*Must convert to a token. */
		if( yymbchars[last].character < i ){
			return i;/*Giving up*/
		}
		while ((last>=first)&&(first>=0)) {/*Binary search loop*/
			mid = (first+last)/2;
			j = yymbchars[mid].character;
			if( j==i ){/*Found*/ 
				return yymbchars[mid].tvalue;
			}else if( j<i ){
				first = mid + 1;
			}else{
				last = mid -1;
			}
		}
		/*No entry in the table.*/
		return i;/* Giving up.*/
	}else{/* i is already a token. */
		return i;
	}
}
#else/*!YYNMBCHARS*/
#define YYLEX()		yylex()
#endif/*!YYNMBCHARS*/

/*
** yyparse - return 0 if worked, 1 if syntax error not recovered from
*/
#if defined(__STDC__) || defined(__cplusplus)
int yyparse(void)
#else
int yyparse()
#endif
{
	register YYSTYPE *yypvt = 0;	/* top of value stack for $vars */

#if defined(__cplusplus) || defined(lint)
/*
	hacks to please C++ and lint - goto's inside
	switch should never be executed
*/
	static int __yaccpar_lint_hack__ = 0;
	switch (__yaccpar_lint_hack__)
	{
		case 1: goto yyerrlab;
		case 2: goto yynewstate;
	}
#endif

	/*
	** Initialize externals - yyparse may be called more than once
	*/
	yypv = &yyv[-1];
	yyps = &yys[-1];
	yystate = 0;
	yytmp = 0;
	yynerrs = 0;
	yyerrflag = 0;
	yychar = -1;

#if YYMAXDEPTH <= 0
	if (yymaxdepth <= 0)
	{
		if ((yymaxdepth = YYEXPAND(0)) <= 0)
		{
			yyerror("yacc initialization error");
			YYABORT;
		}
	}
#endif

	{
		register YYSTYPE *yy_pv;	/* top of value stack */
		register int *yy_ps;		/* top of state stack */
		register int yy_state;		/* current state */
		register int  yy_n;		/* internal state number info */
	goto yystack;	/* moved from 6 lines above to here to please C++ */

		/*
		** get globals into registers.
		** branch to here only if YYBACKUP was called.
		*/
	yynewstate:
		yy_pv = yypv;
		yy_ps = yyps;
		yy_state = yystate;
		goto yy_newstate;

		/*
		** get globals into registers.
		** either we just started, or we just finished a reduction
		*/
	yystack:
		yy_pv = yypv;
		yy_ps = yyps;
		yy_state = yystate;

		/*
		** top of for (;;) loop while no reductions done
		*/
	yy_stack:
		/*
		** put a state and value onto the stacks
		*/
#if YYDEBUG
		/*
		** if debugging, look up token value in list of value vs.
		** name pairs.  0 and negative (-1) are special values.
		** Note: linear search is used since time is not a real
		** consideration while debugging.
		*/
		if ( yydebug )
		{
			register int yy_i;

			printf( "State %d, token ", yy_state );
			if ( yychar == 0 )
				printf( "end-of-file\n" );
			else if ( yychar < 0 )
				printf( "-none-\n" );
			else
			{
				for ( yy_i = 0; yytoks[yy_i].t_val >= 0;
					yy_i++ )
				{
					if ( yytoks[yy_i].t_val == yychar )
						break;
				}
				printf( "%s\n", yytoks[yy_i].t_name );
			}
		}
#endif /* YYDEBUG */
		if ( ++yy_ps >= &yys[ yymaxdepth ] )	/* room on stack? */
		{
			/*
			** reallocate and recover.  Note that pointers
			** have to be reset, or bad things will happen
			*/
			long yyps_index = (yy_ps - yys);
			long yypv_index = (yy_pv - yyv);
			long yypvt_index = (yypvt - yyv);
			int yynewmax;
#ifdef YYEXPAND
			yynewmax = YYEXPAND(yymaxdepth);
#else
			yynewmax = 2 * yymaxdepth;	/* double table size */
			if (yymaxdepth == YYMAXDEPTH)	/* first time growth */
			{
				char *newyys = (char *)YYNEW(int);
				char *newyyv = (char *)YYNEW(YYSTYPE);
				if (newyys != 0 && newyyv != 0)
				{
					yys = YYCOPY(newyys, yys, int);
					yyv = YYCOPY(newyyv, yyv, YYSTYPE);
				}
				else
					yynewmax = 0;	/* failed */
			}
			else				/* not first time */
			{
				yys = YYENLARGE(yys, int);
				yyv = YYENLARGE(yyv, YYSTYPE);
				if (yys == 0 || yyv == 0)
					yynewmax = 0;	/* failed */
			}
#endif
			if (yynewmax <= yymaxdepth)	/* tables not expanded */
			{
				yyerror( "yacc stack overflow" );
				YYABORT;
			}
			yymaxdepth = yynewmax;

			yy_ps = yys + yyps_index;
			yy_pv = yyv + yypv_index;
			yypvt = yyv + yypvt_index;
		}
		*yy_ps = yy_state;
		*++yy_pv = yyval;

		/*
		** we have a new state - find out what to do
		*/
	yy_newstate:
		if ( ( yy_n = yypact[ yy_state ] ) <= YYFLAG )
			goto yydefault;		/* simple state */
#if YYDEBUG
		/*
		** if debugging, need to mark whether new token grabbed
		*/
		yytmp = yychar < 0;
#endif
		if ( ( yychar < 0 ) && ( ( yychar = YYLEX() ) < 0 ) )
			yychar = 0;		/* reached EOF */
#if YYDEBUG
		if ( yydebug && yytmp )
		{
			register int yy_i;

			printf( "Received token " );
			if ( yychar == 0 )
				printf( "end-of-file\n" );
			else if ( yychar < 0 )
				printf( "-none-\n" );
			else
			{
				for ( yy_i = 0; yytoks[yy_i].t_val >= 0;
					yy_i++ )
				{
					if ( yytoks[yy_i].t_val == yychar )
						break;
				}
				printf( "%s\n", yytoks[yy_i].t_name );
			}
		}
#endif /* YYDEBUG */
		if ( ( ( yy_n += yychar ) < 0 ) || ( yy_n >= YYLAST ) )
			goto yydefault;
		if ( yychk[ yy_n = yyact[ yy_n ] ] == yychar )	/*valid shift*/
		{
			yychar = -1;
			yyval = yylval;
			yy_state = yy_n;
			if ( yyerrflag > 0 )
				yyerrflag--;
			goto yy_stack;
		}

	yydefault:
		if ( ( yy_n = yydef[ yy_state ] ) == -2 )
		{
#if YYDEBUG
			yytmp = yychar < 0;
#endif
			if ( ( yychar < 0 ) && ( ( yychar = YYLEX() ) < 0 ) )
				yychar = 0;		/* reached EOF */
#if YYDEBUG
			if ( yydebug && yytmp )
			{
				register int yy_i;

				printf( "Received token " );
				if ( yychar == 0 )
					printf( "end-of-file\n" );
				else if ( yychar < 0 )
					printf( "-none-\n" );
				else
				{
					for ( yy_i = 0;
						yytoks[yy_i].t_val >= 0;
						yy_i++ )
					{
						if ( yytoks[yy_i].t_val
							== yychar )
						{
							break;
						}
					}
					printf( "%s\n", yytoks[yy_i].t_name );
				}
			}
#endif /* YYDEBUG */
			/*
			** look through exception table
			*/
			{
				register YYCONST int *yyxi = yyexca;

				while ( ( *yyxi != -1 ) ||
					( yyxi[1] != yy_state ) )
				{
					yyxi += 2;
				}
				while ( ( *(yyxi += 2) >= 0 ) &&
					( *yyxi != yychar ) )
					;
				if ( ( yy_n = yyxi[1] ) < 0 )
					YYACCEPT;
			}
		}

		/*
		** check for syntax error
		*/
		if ( yy_n == 0 )	/* have an error */
		{
			/* no worry about speed here! */
			switch ( yyerrflag )
			{
			case 0:		/* new error */
				yyerror( "syntax error" );
				goto skip_init;
			yyerrlab:
				/*
				** get globals into registers.
				** we have a user generated syntax type error
				*/
				yy_pv = yypv;
				yy_ps = yyps;
				yy_state = yystate;
			skip_init:
				yynerrs++;
				/* FALLTHRU */
			case 1:
			case 2:		/* incompletely recovered error */
					/* try again... */
				yyerrflag = 3;
				/*
				** find state where "error" is a legal
				** shift action
				*/
				while ( yy_ps >= yys )
				{
					yy_n = yypact[ *yy_ps ] + YYERRCODE;
					if ( yy_n >= 0 && yy_n < YYLAST &&
						yychk[yyact[yy_n]] == YYERRCODE)					{
						/*
						** simulate shift of "error"
						*/
						yy_state = yyact[ yy_n ];
						goto yy_stack;
					}
					/*
					** current state has no shift on
					** "error", pop stack
					*/
#if YYDEBUG
#	define _POP_ "Error recovery pops state %d, uncovers state %d\n"
					if ( yydebug )
						printf( _POP_, *yy_ps,
							yy_ps[-1] );
#	undef _POP_
#endif
					yy_ps--;
					yy_pv--;
				}
				/*
				** there is no state on stack with "error" as
				** a valid shift.  give up.
				*/
				YYABORT;
			case 3:		/* no shift yet; eat a token */
#if YYDEBUG
				/*
				** if debugging, look up token in list of
				** pairs.  0 and negative shouldn't occur,
				** but since timing doesn't matter when
				** debugging, it doesn't hurt to leave the
				** tests here.
				*/
				if ( yydebug )
				{
					register int yy_i;

					printf( "Error recovery discards " );
					if ( yychar == 0 )
						printf( "token end-of-file\n" );
					else if ( yychar < 0 )
						printf( "token -none-\n" );
					else
					{
						for ( yy_i = 0;
							yytoks[yy_i].t_val >= 0;
							yy_i++ )
						{
							if ( yytoks[yy_i].t_val
								== yychar )
							{
								break;
							}
						}
						printf( "token %s\n",
							yytoks[yy_i].t_name );
					}
				}
#endif /* YYDEBUG */
				if ( yychar == 0 )	/* reached EOF. quit */
					YYABORT;
				yychar = -1;
				goto yy_newstate;
			}
		}/* end if ( yy_n == 0 ) */
		/*
		** reduction by production yy_n
		** put stack tops, etc. so things right after switch
		*/
#if YYDEBUG
		/*
		** if debugging, print the string that is the user's
		** specification of the reduction which is just about
		** to be done.
		*/
		if ( yydebug )
			printf( "Reduce by (%d) \"%s\"\n",
				yy_n, yyreds[ yy_n ] );
#endif
		yytmp = yy_n;			/* value to switch over */
		yypvt = yy_pv;			/* $vars top of value stack */
		/*
		** Look in goto table for next state
		** Sorry about using yy_state here as temporary
		** register variable, but why not, if it works...
		** If yyr2[ yy_n ] doesn't have the low order bit
		** set, then there is no action to be done for
		** this reduction.  So, no saving & unsaving of
		** registers done.  The only difference between the
		** code just after the if and the body of the if is
		** the goto yy_stack in the body.  This way the test
		** can be made before the choice of what to do is needed.
		*/
		{
			/* length of production doubled with extra bit */
			register int yy_len = yyr2[ yy_n ];

			if ( !( yy_len & 01 ) )
			{
				yy_len >>= 1;
				yyval = ( yy_pv -= yy_len )[1];	/* $$ = $1 */
				yy_state = yypgo[ yy_n = yyr1[ yy_n ] ] +
					*( yy_ps -= yy_len ) + 1;
				if ( yy_state >= YYLAST ||
					yychk[ yy_state =
					yyact[ yy_state ] ] != -yy_n )
				{
					yy_state = yyact[ yypgo[ yy_n ] ];
				}
				goto yy_stack;
			}
			yy_len >>= 1;
			yyval = ( yy_pv -= yy_len )[1];	/* $$ = $1 */
			yy_state = yypgo[ yy_n = yyr1[ yy_n ] ] +
				*( yy_ps -= yy_len ) + 1;
			if ( yy_state >= YYLAST ||
				yychk[ yy_state = yyact[ yy_state ] ] != -yy_n )
			{
				yy_state = yyact[ yypgo[ yy_n ] ];
			}
		}
					/* save until reenter driver code */
		yystate = yy_state;
		yyps = yy_ps;
		yypv = yy_pv;
	}
	/*
	** code supplied by user is placed in this switch
	*/
	switch( yytmp )
	{
		
case 1:
# line 40 "rmschkrul.y"
{yyval.no = yypvt[-0].no; igYaccRC = yyval.no;} break;
case 2:
# line 41 "rmschkrul.y"
{ igYaccRC = -1; *rmschkrulstring = 0x00;} break;
case 3:
# line 44 "rmschkrul.y"
{yyval.no = rc(yypvt[-2].no,'&',yypvt[-0].no);} break;
case 4:
# line 45 "rmschkrul.y"
{yyval.no = rc(yypvt[-2].no,'|',yypvt[-0].no);} break;
case 5:
# line 46 "rmschkrul.y"
{yyval.no = yypvt[-0].no;} break;
case 6:
# line 49 "rmschkrul.y"
{yyval.no = yypvt[-0].no;} break;
case 7:
# line 50 "rmschkrul.y"
{yyval.no = yypvt[-0].no;} break;
case 8:
# line 51 "rmschkrul.y"
{yyval.no = yypvt[-1].no;} break;
case 9:
# line 54 "rmschkrul.y"
{yyval.no = sc(yypvt[-2].string,'>',yypvt[-0].string);} break;
case 10:
# line 55 "rmschkrul.y"
{yyval.no = sc(yypvt[-2].string,'<',yypvt[-0].string);} break;
case 11:
# line 56 "rmschkrul.y"
{yyval.no = sc(yypvt[-2].string,'=',yypvt[-0].string);} break;
case 12:
# line 57 "rmschkrul.y"
{yyval.no = sc(yypvt[-2].string,'!',yypvt[-0].string);} break;
case 13:
# line 60 "rmschkrul.y"
{strcpy(yyval.string,yypvt[-1].string);} break;
case 14:
# line 61 "rmschkrul.y"
{strcpy(yyval.string,yypvt[-0].string);} break;
case 15:
# line 64 "rmschkrul.y"
{yyval.no = fc(yypvt[-2].real,'>',yypvt[-0].real);} break;
case 16:
# line 65 "rmschkrul.y"
{yyval.no = fc(yypvt[-2].real,'<',yypvt[-0].real);} break;
case 17:
# line 66 "rmschkrul.y"
{yyval.no = fc(yypvt[-2].real,'=',yypvt[-0].real);} break;
case 18:
# line 67 "rmschkrul.y"
{yyval.no = fc(yypvt[-2].real,'!',yypvt[-0].real);} break;
case 19:
# line 70 "rmschkrul.y"
{yyval.real = yypvt[-1].real;} break;
case 20:
# line 71 "rmschkrul.y"
{yyval.real = yypvt[-2].real+yypvt[-0].real;} break;
case 21:
# line 72 "rmschkrul.y"
{yyval.real = yypvt[-2].real-yypvt[-0].real;} break;
case 22:
# line 73 "rmschkrul.y"
{yyval.real = yypvt[-2].real*yypvt[-0].real;} break;
case 23:
# line 74 "rmschkrul.y"
{yyval.real = yypvt[-2].real/yypvt[-0].real;} break;
case 24:
# line 75 "rmschkrul.y"
{yyval.real = -yypvt[-0].real;} break;
case 25:
# line 76 "rmschkrul.y"
{yyval.real = yypvt[-0].real;} break;
# line	531 "/usr/ccs/bin/yaccpar"
	}
	goto yystack;		/* reset registers in driver code */
}

