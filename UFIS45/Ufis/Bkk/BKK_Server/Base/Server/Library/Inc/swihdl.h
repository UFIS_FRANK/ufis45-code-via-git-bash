#ifndef _DEF_mks_version_swihdl_h
  #define _DEF_mks_version_swihdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_swihdl_h[] = "@(#) "UFIS_VERSION" $Id: Ufis/Bkk/BKK_Server/Base/Server/Library/Inc/swihdl.h 1.1 2003/05/14 16:32:25SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       : swihdl                                                */
/*                                                                        */
/*  Revision date : 09.02.2000                                            */
/*                                                                        */
/*  Author    	  : rkl                                                   */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
#ifndef _SWIHDL_H
#define _SWIHDL_H

#include <stdio.h>

static char sccs_swihdl_inc[] ="@(#) UFIS 4.4 (c) ABB AAT/I swihdl.h 44.1 / 01/03/13 11:49:39 / RKL";

/* STFTAB */
typedef struct stamm  
{
   char lanm[32];     
   char finm[32];
   char gebu[16];
   char kind[8];
   char nati[8];
   char peno[8];
   char dobk[16];
   char dodm[16];
   char doem[16];
   char teld[64];
   char stfu[16];
} STAMM;

/* ADRTAB */
typedef struct adress
{
   char stfu[32];  		/* Urno in STFTAB */
   char peno[32];  		/* Personalnummer */
   char stra[32];  		/* Strasse */
   char adrc[32];		/* Adressen zusatz c/o */
   char zipa[16]; 		/* Zip-Code   */
   char city[32];       /* Ort */ 
   char lana[8];        /* time shift ends */
   char vwt1[16];      	/* Vorwahl Telefon 1*/
   char tel1[16];	    /* Telefon 1 */
   char vwt2[16];	    /* Vorwahl Telefon 2 */
   char tel2[16];       /* Telefon 2 */
   char vafr[16];       /* 2nd pause length */

} ADRESS;

/* SCOTAB */
typedef struct contract
{
	char peno[32];  	/* Personalnummer */
    char surn[16];		/* Urno in der STFTAB */
  	char kost[16];     	/* Kostenstelle */
  	char kstn[32];     	/* Kostenstellenname */
  	char stsc[8];		/* Statusanhang */
  	char zifi[8];		/* ZIF-Indikator 0,1 oder 2 Wochen */
  	char cweh[8];		/* vertragliche Wochenstunden HHMM */
  	char vafr[16];		/* gueltigkeit an der Vertragsdaten */
  	char kadc[8];		/* Kadercode */
  	char pkzu[8];       /* PKZ = Organisationseinheit */
  	char pkzn[32];      /* Organisationseinheitenname */
	char cpfc[8];       /* Vertragsfunktionscode */
	char cpfn[32];  	/* Vertragsfunktionsname */
  	char ctrc[8];       /* Vertragsart = Code in SCOTAB */
    
} CONTRACT;





/* 
typedef struct contract
{
  char modell[8];
  char lfdNr[8];
  char begin[8];
  char end[8];
  char yesterday[8];
  char timepaid[8];
  char timenotpaid[8];
  char dyn_pause[8];
    
} CONTRACT;
*/
#endif /* _SWIHDL_H */
