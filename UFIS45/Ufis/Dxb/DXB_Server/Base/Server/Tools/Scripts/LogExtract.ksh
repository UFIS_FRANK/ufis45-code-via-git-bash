###===========================================================================
###
### $Id: Ufis/Dxb/DXB_Server/Base/Server/Tools/Scripts/LogExtract.ksh 1.1 2004/12/14 00:26:36SGT jwe Exp  $
###
###     #    ######  ######
###    # #   #     # #     #
###   #   #  #     # #     #
###  #     # ######  ######
###  ####### #     # #     #
###  #     # #     # #     #
###  #     # ######  ######  GmbH (c) 1995-2004
###
### D-63150 Heusenstamm
###
### Script to display received updates for flight given as parameter
###
###===========================================================================
#
# ===========================================================================
# The routine ShowUsage() displays usage help
# ===========================================================================
ShowUsage()
{
   cat <<EOF

   `basename $0`: check received updates for flight given as parameter
   Usage: `basename $0` ALC FLTN DAY A/D
           ALC:  airline code
           FLTN: flight number (digits only)
           DAY:  day of arrival / departure
           A/D:  A for arrival, D for departure

EOF
}

# ===========================================================================
# The routine getUrno() returns the URNO of the AFTTAB record fitting ALC, 
# FLTN, FLDA and ADID 
# ===========================================================================
getUrno()
{
  ALC=$1
  FLTN=$2
  FLDA=$3
  ADID=$4
  STO="STO${ADID}"
  if [ ! -z "${ALC}" ] ; then
    LEN=`echo $ALC | awk '{print length($1)}'`
    if [ ${LEN} -eq 3 ] ; then
      kshsql "select urno from afttab 
            where alc3='${ALC}' and fltn='${FLTN}' and 
            ${STO} like '${FLDA}%' and adid='${ADID}';"
    fi
    if [ ${LEN} -eq 2 ] ; then
      kshsql "select urno from afttab 
            where alc2='${ALC}' and fltn='${FLTN}' and 
            ${STO} like '${FLDA}%' and adid='${ADID}';"
    fi
  fi
}

# ===========================================================================
# The routine filter_flight() is specialized to filter FLIGHT logs
# ===========================================================================
filter_flight()
{
  URNO=$1
  FILE=$2
  case $2 in
     *.Z) CAT=zcat
          ;;
     *)   CAT=cat
          ;;
  esac

  echo "================== ${FILE} =================="
  ${CAT} ${FILE} | awk '
  BEGIN {
    TR="========================================================================"
  }
  {
    if (index($0,":CMD <") > 0)
    {
      if (do_print==1)
      {
        print TR ; print CMD ; print FRM ; print TAB ; print FLD ; print DAT ; print SEL
      }
      do_print=0;
      CMD=$0
      getline ; FRM=$0
    }
    else if (index($0,"DATA ELEMENTS FROM CLIENT") > 0)
    {
      getline ; TAB= $0 
      getline ; FLD= $0 
      getline ; DAT= $0 
      getline ; SEL= $0 
    }
    else if (index($0,"'${URNO}'") > 0)
    {
      do_print=1;
    }
  }
  END { 
    if (do_print==1)
    {
       print TR ; print CMD ; print FRM ; print TAB ; print FLD ; print DAT ; print SEL
    }
  }'
}

# ===========================================================================
# The routine ShowReceivedUpdates() evaluates URNO of AFTTAB record and 
# filters FLIGHT logs for this URNO. It may be usefull to sort the output
# by time by pipeng to  'sort -k 2'
# ===========================================================================
ShowReceivedUpdates()
{
   echo
   if [ "$4" = "A" ] ; then
      echo Checking arrival of $1 $2 at $3
   else
      echo Checking departure of $1 $2 at $3
   fi
   URN_SPC=`getUrno $1 $2 $3 $4`
   URNO=`echo ${URN_SPC}`
   if [ -z "${URNO}" ] ; then
     echo Filght record not found in AFTTAB! Please check input!.
   else
     echo "=== UFIS flight record: ==="
     SEL="select fkey||','||urno||','||stoa||','||stod||','||regn||','||act3 from afttab where urno=${URNO};"
     echo "${SEL}"
     kshsql "${SEL}"
     echo "=== BEGIN checking flight updates ==="
     # files with this prefix should be checked via filter_flight():
     FLIGHT_FILES="flight flisea"
     # check logfiles of current debug directory:
     for prefix in ${FLIGHT_FILES} ; do
       for file in ${DBG_PATH}/${prefix}*.log* ; do
         filter_flight ${URNO} ${file}
       done
     done
     # check logfiles of older debug directories:
     for prefix in ${FLIGHT_FILES} ; do
       for file in ${DBG_PATH}_200*/${prefix}*.log* ; do
         filter_flight ${URNO} ${file}
       done
     done
     echo "=== END   checking flight updates ==="
     echo
     # here may follow more filter, i.e. for bchdl.....
   fi
}

# ===========================================================================
# This is the MAIN of this script. First check for parameter 4 in A/D:
# ===========================================================================
if [ "$4" = "A" -o "$4" = "D" ] ; then
   # hopefully other parameter also ar right....
   ShowReceivedUpdates $1 $2 $3 $4
else
   # fourth parameter is none of A/D, so show usage
   ShowUsage
fi
