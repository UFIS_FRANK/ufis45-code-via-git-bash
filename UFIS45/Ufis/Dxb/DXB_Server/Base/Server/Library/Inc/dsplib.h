#ifndef _DEF_mks_version_dsplib_h
  #define _DEF_mks_version_dsplib_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_dsplib_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/Dxb/DXB_Server/Base/Server/Library/Inc/dsplib.h 1.1 2010/11/29 15:46:45SGT otr Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/
/*                                                                           */
/* ABB AAT/I dsplib.h                                                        */
/* UFIS DATABASE EXTENSION for DZINE PAGE FILES (=> insert into dsplib.h)    */
/*                                                                           */
/* Author         : Janos Heilig                                             */
/* Creation Date  : 23. March 2001                                           */
/* Description    : parser & display handler utilities                       */
/*                                                                           */
/* Update history : jhe 13 Oct 04 host ip lookup function declaration        */
/*                  j. heilig 23 Aug 2004 added interface name to _socket    */
/*                  j. hiller marged dsplib network and misc functions       */
/*                  j. heilig types and fcts to support native db format     */
/*                  j. heilig changes for dxb project                        */
/*                  j. heilig types J and LJ                                 */
/*                  j. heilig fct to get separator string or char            */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
                                                                                


#ifndef dsplib_included
#define dsplib_included

#include <assert.h>

#ifdef STANDALONE_PARSER	/*************** standalone ********************/


#define RC_SUCCESS	0
#define RC_FAIL 		1

#define TRACE			1
#define DEBUG			2
#define WARNING		3
#define ERROR			4

static int debug_level = DEBUG;
void dbg(int, char *,...);

#else 						/*************** display handler ********************/

#define WARNING TRACE
#define ERROR TRACE

#endif 						/*************** end standalone/display handler ********************/

#define Assert			assert

/*
 * dynamic string utility to handle growing strings
 */

typedef struct
{
	unsigned int blksz, nblk, nfree, length;
	char *chars;
} DynStr;

DynStr *DynStr_Create(void);
DynStr *DynStr_CreateQ(unsigned int);
void DynStr_Destroy(DynStr *);
void DynStr_AppendStr(DynStr *, char *);
void DynStr_AppendC(DynStr *, char c);
void DynStr_Reset(DynStr *);
#define DynStr_Cpy(d,s) {DynStr_Reset((d));DynStr_AppendStr((d),(s));}
#define DynStr_Cat(d,s) DynStr_AppendStr((d),(s));

/*
 * token class to parse string
 */

typedef DynStr TokenZ;

TokenZ *TokenZ_CreateFromString(char **, char *, char *);
void TokenZ_Destroy(TokenZ *);
int TokenZ_GetStrings(char **, char *, char ***);

/* field specification (FLDN of FLD) */

typedef struct
{
	char *pcTableName, *pcFieldName;
	int iStart, iEnd;
} FieldReference;

/* field display type consts (FLDT of FLD) */

#define  UDBX_DT_ANY		0
#define  UDBX_DT_N		1
#define  UDBX_DT_S		2
#define  UDBX_DT_L		3
#define  UDBX_DT_T		4
#define  UDBX_DT_D		5
#define  UDBX_DT_DT		6
#define  UDBX_DT_CT		7
#define  UDBX_DT_CD		8
#define  UDBX_DT_CDT		9
#define  UDBX_DT_TT		10
#define  UDBX_DT_TD		11
#define  UDBX_DT_TDT		12
#define  UDBX_DT_R		13
#define  UDBX_DT_B		14
#define  UDBX_DT_A		15
#define  UDBX_DT_AL		16
#define  UDBX_DT_M		17
#define  UDBX_DT_J		18
#define  UDBX_DT_LJ		19
#define  UDBX_DT_F 		20
/* field display type structs */

typedef struct
{
	int	iType;
} DpyType_Any;

typedef struct
{
	int	iType;
} DpyType_N, DpyType_M, DpyType_J;

typedef struct 
{
	int	iType;
	char	*pcText;
} DpyType_S;

typedef struct
{
	int	iType;
	char	*pcExtension;
} DpyType_L, DpyType_LJ;

typedef struct
{
	int	iType;
	char	*pcFormat;
} DpyType_T, DpyType_D, DpyType_DT, DpyType_CT, DpyType_CD, DpyType_CDT,
  DpyType_TT, DpyType_TD, DpyType_TDT;

typedef struct
{
	int	iType;
	char	*pcSeparator;
	FieldReference rFieldRef;
} DpyType_R;

typedef struct
{
	int	iType;
	char	*pcType;
	int	iBlinkColor;
	char	cTimeConversion;
	int	iNFieldNames;
	char	**p2cFieldNames;
} DpyType_B;
	
typedef struct
{
	int 	iType;
	int	iNoOfAirports;
	char	cSequence;
	char	*pcSeparator;
	int	iNFieldNames;
	char	**p2cFieldNames;
} DpyType_A;

typedef struct
{
	int iType;
	char *pcFieldName;
} DpyType_AL;
	
typedef union
{
  DpyType_Any	rAny;
  DpyType_N	rN;
  DpyType_M	rM;
  DpyType_M	rJ;
  DpyType_S	rS;
  DpyType_L	rL;
  DpyType_L	rLJ;
  DpyType_T	rT;
  DpyType_D	rD;
  DpyType_DT	rDT;
  DpyType_CT	rCT;
  DpyType_CD	rCD;
  DpyType_CDT	rCDT;
  DpyType_TT	rTT;
  DpyType_TD	rTD;
  DpyType_TDT	rTDT;
  DpyType_R	rR;
  DpyType_B	rB;
  DpyType_A	rA;
  DpyType_AL	rAL;
}	FieldDisplayType;

typedef struct
{
	FieldReference rFieldRef;
	FieldDisplayType rDpyType;
	int iSize;
} FieldDescription;

typedef struct
{
  char *pcObjectId;
  int iLanguage;
  int iNFieldDesc;
  FieldDescription **prFieldDesc;
  char *pcDZCommand;
  int iNDZParms;
  char **p2cDZParms;
  DynStr *prDZFile;
  int iNError;
} UDBXCommand;

typedef struct
{
	int iNCommands;
	char *pcFileName;
	UDBXCommand **prCommands;
} UDBXPage;
	

void FieldReference_Init(FieldReference *);
int FieldReference_Set(FieldReference *, char **);
void FieldReference_Reset(FieldReference *);

void FieldDisplayType_Init(FieldDisplayType *);
int FieldDisplayType_Set(FieldDisplayType *, char **);
void FieldDisplayType_Reset(FieldDisplayType *);
int FieldDisplayType_GetByName(char *);
char *FieldDisplayType_GetByType(int );
char *FieldDisplayType_GetSep(char **);

FieldDescription *FieldDescription_Create(void);
void FieldDescription_Destroy(FieldDescription *);

/*
 * native layout format support -- June 09 2001 j.heilig
 */

#define GTP_MAXLSIZE 1024    
typedef char GTPField[GTP_MAXLSIZE+1];

typedef struct
{

	GTPField f1, f2, f3, cmd;
} GTPLine; 
 
typedef struct
{
	int ncols, nrows, cix;
	int nparms;
	char **pparms;
} GT2Table;

typedef struct
{
	int nparms;
	char **pparms;
} GT2Cmdmap;

typedef DynStr *PEDField;
typedef struct PEDREC
{
	PEDField name, grp, desc, def, lang; 
	int nlang, id;
} PEDRec;

typedef struct PEDTABLE
{
	int nrec, blksize, nfree;
	PEDRec **prec;
} PEDTable;

typedef struct
{
  GT2Table *gt_table;
  PEDTable *dd_table;
  GT2Cmdmap *gt_cmdmap;
} UDBXContext;

PEDTable *PEDTable_CreateFromDB(const char *);
PEDTable *PEDTable_Create(void);
void PEDTable_AddRec(PEDTable *, PEDRec *);
void PEDTable_Destroy(PEDTable *);

PEDRec *PEDRec_CreateI(char *, char *, int);
PEDRec *PEDRec_CreateC(char *, char *, char *, char *, char *);
void PEDRec_Destroy(PEDRec *);
 
UDBXCommand *UDBXCommand_Create(void);
void UDBXCommand_Destroy(UDBXCommand *);

UDBXPage *UDBXPage_CreateFromFile(char *, PEDTable *);
int UDBXPage_PreprocessFile(FILE *, FILE *, PEDTable *, int *,char *,char*);
void UDBXPage_Destroy(UDBXPage *);

int UDBXPage_InsertCommand(int, UDBXCommand *);
int UDBXPage_DeleteCommand(int);

GT2Table *GT2Table_Create(void);
void GT2Table_Destroy(GT2Table *);
int GT2Table_Set(GT2Table *, char *);
void GT2Table_Reset(GT2Table *);

GT2Cmdmap *GT2Cmdmap_Create(void);
void GT2Cmdmap_Destroy(GT2Cmdmap *);
int GT2Cmdmap_Set(GT2Cmdmap *, char *);
void GT2Cmdmap_Reset(GT2Cmdmap *);

UDBXContext *UDBXContext_Create(void);
void UDBXContext_Destroy(UDBXContext *);

/* defines for config-entry types */
#define CFG_ALPHA 		1200
#define CFG_NUM   		1201
#define CFG_ALPHANUM  1202
#define CFG_IGNORE  	1203
#define CFG_PRINT 		1204


/*========== <<<JHE/JHI>>> ==========*/

typedef struct
{
  char grpn[13];      /* name of dsphdl */
  int mod_id;         /* mod_id of dsphdl */
}DSPHDL_INFO;

typedef struct
{
  unsigned long urno;   /* urno of device record */
  char dadr[16];        /* IP-address of device */
  unsigned short dprt;  /* device port to use */
  DSPHDL_INFO group;    /* display handler informations */
}SRV_DEV_INFO;


/*declarations for network part of dsplib*/
/* jhe 23 aug 04: added interface name parameter */
int dsplib_socket(int *ipSock,int ipType, char *pcpService,int ipPort,int ipDoBind, char *ifname);
/* jhe 13 oct 04: added function to lookup ip addr by interface or host name */
int dsplib_gethostaddr(int, char *, struct in_addr *);

void dsplib_shutdown(int ipSock,int ipHow);
void dsplib_close(int ipSock);
int CheckValue(char *pcpEntry,char *pcpValue,int ipType);
int GetdZParameter(char *pcpResult,UDBXCommand* pcpPC,int ilen);
void SetPipeToItem(char *pcpData,int ipHexChar);
void SetPipeToComma(char *pcpData,int ipHexChar);
int MapToArabic(char* pcpResult,int ipLen,char *pcpCodePage);
int MapToArabicN(char* pcpResult,int ipLen,char *pcpCodePage);
int GetQuotasForWhereClause(char* pcpIn,char* pcpOut,char* pcpField,int iplen,char* pcpSign);

#endif /* dsplib_included */









