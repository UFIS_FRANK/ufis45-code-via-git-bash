#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_htmhdl_h[] = "@(#) "UFIS_VERSION" $Id: Ufis/Dxb/DXB_Server/Base/Server/Library/Inc/htmhdl.h 1.1 2004/11/26 23:25:44SGT jim Exp  $";
#endif _DEF_mks_version
static char sccs_htmhdl_inc[]="%Z% UFIS 4.4 (c) ABB AAT/I %M% %I% / %E% %U% / JWE"; 
/* **********************************************
 * Library modulname: htmlhdl header file	*
 * Author: 	Joern Weerts 			*
 * Date:		19/12/97		*		
 * State:					*
 * Description:  				*
 * History: 					*
 * 	01 Nov 99 mos				*
 *	changes for ftphdl call implementation	*
 *		tools.h included		*
 *		added ftp_client_os to DYNAMIC_CFG
 *						*
 *	18 Jan 00 mos				*
 *	changes for Check in informations	*
 *		added checkin_gap to DYNAMIC_CFG*
 *		AATArray.h included		*
 *		buffersize defines		*
 *		CCATAB_STRUCT added		*
 *						*
 *	27 Jan 00 mos				*
 *	changes for additional row adding	*
 *		added addrow to DYNAMIC_CFG	*
 *		added fixrowamount to DYNAMIC_CFG*
 *		added fixrowstart to DYNAMIC_CFG*
 *		added fixrowend to DYNAMIC_CFG	*
 *		added jfno_len to DYNAMIC_CFG	*
*		added jfno_nr to DYNAMIC_CFG	*
 *						*
 *												*
 *	Nov 2000 mos								*
 *	changes for additional row adding	*
 *		added checkin_calc to DYNAMIC_CFG	*
 *		added processcheckin to DYNAMIC_CFG*
 *		added use_flti to DYNAMIC_CFG*
 *												*
 *	Jul 2003 jim								*
 *		added rcvtimeout	to DYNAMIC_CFG										*
 *  Aug 8, 2004 jim: added some bools to DYNAMIC_CFG *
 * *********************************************/

/* The system header files */
#include  <stdio.h>
#include  <malloc.h>
#include  <errno.h>
#include  <signal.h>
#include  <time.h>

/* The CCS (ABB-ACE/FC) header files */
#include  "ugccsma.h"
#include  "msgno.h"
#include  "glbdef.h"
#include  "quedef.h"
#include  "uevent.h"
#include  "helpful.h"
#include  "sthdef.h"
#include  "db_if.h"
#include  "list.h"
#include  "hsbsub.h"
#include  "debugrec.h"

/*********change by MOS********/
#include  "tools.h"
/*********EOChange************/


/* defines for buffer sizes */
#define XS_BUFF 			128
#define S_BUFF  			512
#define M_BUFF  			1024
#ifndef MAX_BUFFER_LEN
	#define MAX_BUFFER_LEN 		2048
#endif
#define c_COMMA				","


/* defines for types of table-definitions */
#define TABLE				100
#define TABLE_ROW			101
#define TABLE_HEAD			102
#define TABLE_DATA			103
#define FONT				104
#define IMAGE				106

/* defines for types of html-tag-definitions */
#define OPEN				150
#define OPEN_TAG			151
#define CLOSE_TAG			152
#define END_TAG				153



/****************************************/
/*   used structures inside htmlhdl.c   */
/****************************************/

/* static structure for reading parts of htmlhdl.cfg file */
/* only read at startup or reset of htmhdl                */
typedef struct
{
	char *sections;
	char *int_tables;
	char *int_fields;
	char *field_len;
}STATIC_CFG;

/* dynamic structure for reading parts of htmlhdl.cfg file */
/* read every time htmhdl is called to create a html-table */
/* for every section that is defined in STATIC_CFG-sections*/
typedef struct 
{
  int  ftp_mod_id;
  int  iFieldCount;
	BOOL bData_del;
	BOOL bFtp;
	BOOL bAsciiFile;
	BOOL bHtmlTable;
	BOOL bHtmlDestFile;
	BOOL bHead;
	BOOL bCreateImgFields;
	BOOL bFormatDate;
	BOOL bTableHeader;
	BOOL bTableData;
/*	BOOL bResolv; should be used, but results may be different as when reading
                  config on any event */
	/**/	
	char *command;
	char *rhost;
	char *data_del;
	char *ascii_path;
	char *ascii_file;
	char *eol_sign;
	char *eof_sign;
	char *html_path;
	char *html_table;
	char *html_src_files;
	char *html_dest_file;
	/**/	
	char *ftp_path;
	char *ftp_client_os;
	char *ftptimeout;
	char *ftpfiletimer;	
	char *rcvtimeout;	
	char *pcFtpRenameDestFile;
	char *ftp_file;
	char *ftp_user;
	char *ftp_pass;
	char *ftpmode;
	char *mod_id;
	char *snd_cmd;
	/**/
	char *fields;
	char *field_head;
	char *img_path;
	char *img_fields;
	char *img_ext;
	/**/
	char *checkin_gap;
	char *checkin_dom_string;
	char *checkin_dom_start;
	char *addrows;
	char *fixrowamount;
	char *jfno_len;
	char *jfno_nr;
	char *fixrowstart;
	char *fixrowend;
	char *fixrowformat;
	char *checkin_calc;
	char *processcheckin;
	char *use_flti;
	/**/
	char *repl_fields;
	char *replacement;
	char *repl_table;
	char *reference;
	char *no_txt;
	char *date_fields;
	char *date_format;
	/**/
	char *table_b;
	char *table_w;
	char *table_h;
	char *table_cs;
	char *table_cp;
	char *table_bgc;
	char *table_bc;
	char *table_bcd;
	char *table_bcl;
	char *table_backg;
	char *th_align;
	char *th_valign;
	char *th_width;
	char *th_height;
	char *th_bgc;
	char *th_nowrap;
	char *th_fontf;
	char *th_fontc;
	char *th_fonts;
	char *td_align;
	char *td_valign;
	char *td_height;
	char *td_bgc1;
	char *td_bgc2;
	char *td_nowrap;
	char *td_fontf;
	char *td_fontc1;
	char *td_fonts;
	/**/
	char *highlightupdate_difference;
	char *highlightupdate;
	char *highlightupdate_bgcolor1;
	char *highlightupdate_bgcolor2;
}DYNAMIC_CFG;

/* structure for describing the data which are stored */
/* static inside the htmhdl */
typedef struct 
{
	char *tana;
	char *fields;
	char *len;
	char *data;
	int bytes;
}TAB_DEF;

/*********change by MOS 19 Jan 2000********/
/* structures for the common check in counter data */
/* this data will be received via sql_if call with a JOIN statement    */
typedef struct
{
	char 	pcflnu[30];	/* Flightnumber in ccatab 	*/
	char	pcckic[20];	/* counter name in ccatab	*/
	char	pcalc2[20];	/* Airline code in alttab	*/
	char	pcalc3[20];	/* dito				*/
}CCATAB_DATA;

#define MAX_NUM_OF_CCA_ROWS 500
#define MAX_NUM_OF_ROWS 400
typedef struct
{
	int 		iNoofCCARows;	/* no of received rows		*/
	CCATAB_DATA	prRow[MAX_NUM_OF_CCA_ROWS];		/* row containing datafields	*/
}CCATAB_STRUCT;

typedef struct
{
	int 		iNoofDEDRows;	/* no of received rows		*/
	int 	ildedcounters[MAX_NUM_OF_ROWS];	/* array for dedicated counter values */
}CCATAB_DED;

typedef struct
{
	char 	pcpflno[20];	
	char	pcpact3[20];	
	char	pcpregn[20];	
	char	pcppsta[20];	
	char	pcppstd[20];	
	char	pcpland[20];	
	char	pcpairb[20];	
	char	pcpstoa[20];	
	char	pcpstod[20];	
	char	pcponbl[20];	
	char	pcpofbl[20];	
	char	pcpftyp[20];
	char	pcpetai[20];
	char	pcpetdi[20];	
}TOWING_DATA;

typedef struct
{
	int 		iNoofTOWSRows;	/* no of received rows		*/
	TOWING_DATA	prRow[MAX_NUM_OF_ROWS];	/* row containing datafields	*/
}TOWING_STRUCT;

typedef struct
{
	char		pcpDate[20];
	char		pcpRows[MAX_BUFFER_LEN];
}PAGE_DATA;

typedef struct
{
	int 		iNoofTOWPRows;	/* no of received rows		*/
	PAGE_DATA	prRow[MAX_NUM_OF_ROWS];	/* row containing datafields	*/
}TOWING_PAGE;
/*********EOChange************/

/****************************************************************/
/****************************************************************/
/*			EOF HTMHDL.H				*/
/****************************************************************/
/****************************************************************/
