#ifndef _DEF_mks_version_dsplib_c
  #define _DEF_mks_version_dsplib_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_dsplib_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/Dxb/DXB_Server/Base/Server/Library/Cedalib/dsplib.c 1.1 2010/11/29 15:50:53SGT otr Exp  $";
#endif /*_DEF_mks_version_dsplib_c*/
/*****************************************************************************/
/*                                                                           */
/* ABB AAT/I dsplib.c                                                        */
/*                                                                           */
/* Author         : Janos Heilig                                             */
/* Creation Date  : 23. March 2001                                           */
/* Description    : display handler utilities                                */
/*                                                                           */
/* Update history : jhe 13 Oct 04 host ip lookup by interface or host name   */
/*                  j. heilig 23 Aug 2004 added interface name to _socket    */
/*                  from June 8 2001: adding support for indirect DB refs    */
/*                  June 8 2001 types + functions PEDRec, PEDRecTable        */
/*                  June 9 2001 reading pedtab, standalone test              */
/*                  June 15 2001 field def resolution using db table         */
/*                  July  4 2001 1x1 table handling, provis. TAB removal     */
/*                  Aug 30 2001 replacing input file, J and LJ types         */
/*                  Sep 23 2001 merge with version with correct _R format    */
/*                              for sequence string                          */
/*****************************************************************************/

static char *dsplib_c_sccsid="%Z% %P% %R%%S% %G% %U%";


#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <malloc.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

/* jhe 23 aug 04 */
#include <arpa/inet.h>
#include <sys/socket.h>
#if !defined(_HPUX_SOURCE) && !defined(_LINUX)
	#include <sys/sockio.h>
#endif
#include <sys/ioctl.h>
#include <net/if.h>


#ifndef STANDALONE_PARSER
#include <signal.h>
#include <netdb.h>
#include "glbdef.h"
#include "msgno.h"
#include "quedef.h"
#include "uevent.h"
#include "netin.h"
#include "tcputil.h"
#include "ugccsma.h"
#endif /* notdef STANDALONE_PARSER */

#include "dsplib.h"
#ifndef STANDALONE_PARSER
#include "dsphdl.h"
extern int debug_level;
#endif /* notdef STANDALONE_PARSER */
#include "lexdsp.h"

#ifdef LINUX
# include <string.h>
#endif /* LINUX */

/*
 *
 * TODO: (1) Fix page editor output format for TDT -- pending (as of May 8 01)
 *       (2) Check display type union members initialization -- done 
 *       (3) No time conversion for display type B ? -- done 
 *       (4) Complete _Destroy function -- done 
 */

static int trim_str(char *);
static FILE *fget_line(char *, int, FILE *);
static void PEDTable_print(PEDTable *);
static int get_GTPLine(char *, GTPLine *);
static int process_GTPLine(FILE *, GTPLine *, UDBXContext *, PEDTable *,int*,char*);
static int parse_DataDefine(GTPLine *, UDBXContext *);
static PEDRec *match_PEDRec(UDBXContext *, PEDTable *, int);
void UDBXPage_print(UDBXPage *);
static PEDRec *get_PEDRec(const char *, int);
static char *filter_SepCharsIn(char *);
static int move_files(char *, char *);
static int UDBXPage_Build_LookupTable(int *pLC,int piObjectID,int *ppiSourceID,char*);
static int process_GTPLineSearch(int piObjectID,int *ppiSourceID, GTPLine *);
#define STR0(x)	x?x:"(NULL)"

#ifdef STANDALONE_PARSER /*************** standalone ********************/

#include <stdarg.h>
#include <errno.h>


int main(int argc, char **argv)
{
  int ix;
  UDBXPage *page;
  PEDTable *table;

  if (argc < 2)
  {
    fprintf(stderr,"file\n");
    exit(1);
  }

  table = PEDTable_CreateFromDB("pedtab");
	
	for(ix=1; ix<argc; ++ix)
	{
		printf("====================\nfile #%d %s\n====================\n",
           ix, argv[ix]);
  	page = UDBXPage_CreateFromFile(argv[ix], table);
  	if (page)
   	{
   		UDBXPage_print(page);
			UDBXPage_Destroy(page);
   	}
  	else
    	fprintf(stderr, "\n****%s: error ***\n",argv[ix]);
	}
	PEDTable_Destroy(table); /* NULL safe */
  return 0; /* cc */
}

void UDBXPage_print(UDBXPage *pg)
{
  int ix, nx, fx;
  UDBXCommand *pc;
  FieldDescription *pf;
  FieldDisplayType *pd;
  char *dn;

  for(ix=0; ix<pg->iNCommands; ++ix)
  {
    pc = pg->prCommands[ix];
    printf("\n Command # %d\n--------------\n",ix);
    printf("ObjectId=`%s'\n",pc->pcObjectId);
    printf("Language=%d\n",pc->iLanguage);
    printf("NFieldDesc=%d\n", pc->iNFieldDesc);
    for(nx=0; nx<pc->iNFieldDesc; ++nx)
		{
		  pf = pc->prFieldDesc[nx];
		  printf("FieldDesc[%d]:\n", nx);
		  printf("..TableName=`%s'\n",STR0(pf->rFieldRef.pcTableName));
		  printf("..FieldName=`%s'\n",STR0(pf->rFieldRef.pcFieldName));
		  printf("..Start/End=%d/%d\n",pf->rFieldRef.iStart, pf->rFieldRef.iEnd);
		  pd = &(pc->prFieldDesc[nx]->rDpyType);
		  dn = FieldDisplayType_GetByType(pd->rAny.iType);
		  printf("..DisplayType=`%s'\n",STR0(dn));
		  switch (pd->rAny.iType)
	    {
		    case UDBX_DT_N:
		    case UDBX_DT_M:
		    case UDBX_DT_J:
		    case UDBX_DT_F:
		      printf("....(no data)\n");
		      break;
		    case UDBX_DT_S:
		      printf("....(?static?)\n");
		      break;
		    case UDBX_DT_L:
		      printf("....Extension=`%s'\n",STR0(pd->rL.pcExtension));
		      break;
		    case UDBX_DT_LJ:
		      printf("....Extension=`%s'\n",STR0(pd->rLJ.pcExtension));
		      break;
		    case UDBX_DT_T:
		    case UDBX_DT_D:
		    case UDBX_DT_DT:
		    case UDBX_DT_CT:
		    case UDBX_DT_CD:
		    case UDBX_DT_CDT:
		    case UDBX_DT_TT:
		    case UDBX_DT_TD:
		    case UDBX_DT_TDT:
		      printf("....Format=`%s'\n",STR0(pd->rT.pcFormat));
		      break;
		    case UDBX_DT_R:
		      printf("....Separator=`%s'\n",STR0(pd->rR.pcSeparator));
		      printf("....TableName=`%s'\n",STR0(pd->rR.rFieldRef.pcTableName));
		      printf("....FieldName=`%s'\n",STR0(pd->rR.rFieldRef.pcFieldName));
		      printf("....Start/End=%d/%d\n",pd->rR.rFieldRef.iStart,
                 pd->rR.rFieldRef.iEnd);
		      break;
		    case UDBX_DT_B:
		      printf("....Type=`%s'\n",STR0(pd->rB.pcType));
		      printf("....BlinkColor=%d\n",pd->rB.iBlinkColor);
		      printf("....TimeConversion=`%c'\n",pd->rB.cTimeConversion);
		      printf("....NFieldNames=%d\n",pd->rB.iNFieldNames);
		      for(fx=0; fx<pd->rB.iNFieldNames; ++fx)
					printf("......FieldNames[%d]=`%s'\n", fx,
                 STR0(pd->rB.p2cFieldNames[fx]));
		      break;
		    case UDBX_DT_A:
		      printf("....NoOfAirports=%d\n",pd->rA.iNoOfAirports);
		      printf("....Sequence=`%c'\n",pd->rA.cSequence);
		      printf("....Separator=`%s'\n",STR0(pd->rA.pcSeparator));
		      printf("....NFieldNames=%d\n",pd->rA.iNFieldNames);
		      for(fx=0; fx<pd->rA.iNFieldNames; ++fx)
						printf("......FieldNames[%d]=`%s'\n", fx,
                 STR0(pd->rA.p2cFieldNames[fx]));
		      break;
		    case UDBX_DT_AL:
		      printf("....FieldName=`%s'\n",STR0(pd->rAL.pcFieldName));
		      break;
		    default:
		      break;
		   }
		   printf("..Size=%d\n",pf->iSize);
		}
    printf("DZCommand=`%s'\n",STR0(pc->pcDZCommand));
    printf("NDZParms=%d\n",pc->iNDZParms);
    for(fx=0; fx<pc->iNDZParms; ++fx)
			printf("DZParms[%d]=`%s'\n",fx,STR0(pc->p2cDZParms[fx]));
	}
}

void dbg(int level, char *fmt,...)
{
  va_list args;
  char fb[1000];
  FILE *outp;

  va_start(args, fmt);

  if (level == ERROR || level < debug_level)
  {
	 	if (level == ERROR)
		{
		  outp = stderr;
		  strcpy(fb,"*** Error ");
		}
    else
		{
		  fb[0] = '\0';
		  outp = stdout;
		}
    strcat(fb, fmt);
    strcat(fb, "\n");
    vfprintf(outp, fb, args);
    fflush(outp);
  }
  va_end(args);
}

#endif /* STANDALONE_PARSER */


PEDRec *get_PEDRec(const char *db, int init)
{
static FILE *fp = NULL;
static char *leftsep = "\033", *rightsep = "\033";
#define PEDREC_BUFFSIZE	4096
char buff[PEDREC_BUFFSIZE+1], *p;
PEDRec *res;
TokenZ *p1, *p2, *p3, *p4, *p5;

	if (init)
	{
		if (fp)
		{
			fclose(fp);
			fp = NULL;
		}
		if (db)
			fp = fopen(db,"r");
		if (fp == NULL)
			dbg(ERROR,"get_PEDRec: cannot open '%s': errno=%d",db?db:"(null)",errno);
	}
	if (fp == NULL)
	{
		dbg(ERROR, "get_PEDRec: bad file or function");
		return NULL;

	}

	res = NULL;

	if (fget_line(buff,PEDREC_BUFFSIZE,fp))
	{
		p = buff;
		p1 = TokenZ_CreateFromString(&p, leftsep, rightsep);
		p2 = TokenZ_CreateFromString(&p, leftsep, rightsep);
		p3 = TokenZ_CreateFromString(&p, leftsep, rightsep);
		p4 = TokenZ_CreateFromString(&p, leftsep, rightsep);
		p5 = TokenZ_CreateFromString(&p, leftsep, rightsep);

		if (p1 && p2 && p3 && p4 && p5)
			res = PEDRec_CreateC(p1->chars,p2->chars,p3->chars,p4->chars,p5->chars);
		if(res == NULL)
			dbg(ERROR," get_PEDRec: bad format in '%s'",buff);

		TokenZ_Destroy(p1);
		TokenZ_Destroy(p2);
		TokenZ_Destroy(p3);
		TokenZ_Destroy(p4);
		TokenZ_Destroy(p5);
	}

	if (res == NULL)
	{
		fclose(fp);
		fp = NULL;
	}

	return res;
}

/*
 * some static, "non-class" utility functions
 */

static int trim_str(char *s)
{
char *p;
int ix, nzc;
	if(s==NULL)
		return 0;
	p = s;
	while(*p == ' ' || *p == '\t')
		++p;
	for(ix=nzc=0; *p; ++p,++ix)
	{
		s[ix] = *p;
		if(*p != ' ' && *p != '\t')
			nzc = ix+1;
	}
	s[nzc] = '\0';
	return nzc;
}

static FILE *fget_line(char *buff, int n, FILE *fp)
{
int len;
	Assert(fp);
	Assert(n>0);
	buff[n-1] = '\0';
	if(fgets(buff,n,fp) == NULL)
		return NULL;
	Assert(buff[n-1] == '\0');
	len = strlen(buff);
	if(len > 0 && buff[len-1] == '\n')
		buff[len-1] = '\0';
	if(len > 1 && buff[len-2] == '\r')
		buff[len-2] = '\0';
	return fp;
}

int move_files(char *fnin, char *fnout)
{
#define mv_buffsize (20*1024)
char buff[mv_buffsize];
int h1, h2, n;

	if( (h1 = open(fnin, O_RDONLY)) < 0)
		return -2;
	if( (h2 = open(fnout, O_WRONLY|O_TRUNC)) < 0)

	{
		close(h1);
		return -3;
	}

	while( (n = read(h1, buff, mv_buffsize)) > 0)
	{
		if(write(h2, buff, n) < n)
			break;
	}
	close(h1);
	close(h2);

	if(n > 0)
		return -4;

	return unlink(fnin);
}


static void PEDTable_print(PEDTable *pt)
{
int ix;
	if(pt == NULL)
	{
		dbg(TRACE, "PEDTable: NULL");
		return;
	}

	dbg(TRACE, "PEDTable: %d record(s)", pt->nrec);
#define PTR0STR0(p)	p?STR0(p->chars):"<nil>"
	for(ix=0; ix<pt->nrec; ++ix)
	{
		dbg(DEBUG, "-- Nr. %d", ix+1);
/* some fields maybe nil (CreateI)
		dbg(TRACE, "   SNAM=%s", pt->prec[ix]->name->chars);
		dbg(TRACE, "   GNAM=%s", pt->prec[ix]->grp->chars);
		dbg(TRACE, "   DSCR=%s", pt->prec[ix]->desc->chars);
		dbg(TRACE, "   FSTR=%s", pt->prec[ix]->def->chars);
		dbg(TRACE, "   LANG=%s", pt->prec[ix]->lang->chars);
*/
		dbg(DEBUG, "   SNAM=%s", PTR0STR0(pt->prec[ix]->name));
		dbg(DEBUG, "   GNAM=%s", PTR0STR0(pt->prec[ix]->grp));
		dbg(DEBUG, "   DSCR=%s", PTR0STR0(pt->prec[ix]->desc));
		dbg(DEBUG, "   FSTR=%s", PTR0STR0(pt->prec[ix]->def));
		dbg(DEBUG, "   LANG=%s (%d)", PTR0STR0(pt->prec[ix]->lang),
	      pt->prec[ix]->nlang);
		dbg(DEBUG, "   ID=%d",   pt->prec[ix]->id);
	}
}

/*
 * fill rawcmd struct with field and command data from a line expected
 * to be in the format either
 *   '"//" ... '
 * or
 *   '"[oid]" ["[field2]"] ["[field3]"] command'
 * where * unquoted square brackets initicate an optional part.
 *
 * The return value is 0 if a comment '//' is recognized, 1 to 3 if a 
 * corresponding number of fields is recognized and -1 if an error is
 * detected.
 */

static int get_GTPLine(char *buff, GTPLine *pLine)
{
char *pc, *pdst, c;
int st, fcnt, idst, res;
const int stNEXT=0, stINSFLD=1, stINSCMD=2, stERR=3;

	pLine->f1[0] = pLine->f2[0] = pLine->f3[0] = pLine->cmd[0] = '\0';

	pc = buff;
	st = stNEXT;
	fcnt = 0;

	do
	{
		if (st == stNEXT)
		{
/* remove leading and trailing blanks */
			res = trim_str(pc);
/* see if empty or comment line */
			if(fcnt==0 && ((res==0) || (pc[0]=='/' && pc[1]=='/')))
				break;
/* see if starting new field (it should) */
			if(pc[0] == '[')
			{
				st = stINSFLD;
				idst = 0;
				if (fcnt == 0)
					pdst = pLine->f1;
				else if (fcnt == 1)
					pdst = pLine->f2;
				else if (fcnt == 2)
					pdst = pLine->f3;
				else
				{
					dbg(ERROR,"get_GTPLine: too many fields in '%s'", buff);
					st = stERR;
				}
				continue;
			}
			else if (fcnt>0 && ((*pc>='a' && *pc<='z') || (*pc>='A' && *pc<='Z')))
			{
				st = stINSCMD;
				idst = 0;
				pdst = pLine->cmd;
/* NO continue */
			}
			else
			{
				dbg(ERROR,"get_GTPLine: bad command or no field in '%s'", buff);
				st = stERR;
				break;
			}
		}
		Assert(st == stINSFLD || st == stINSCMD);
		if (*pc == ']' && st == stINSFLD)
		{
			st = stNEXT;
			c = '\0';
			++fcnt;
		}
		else
		{
			c = *pc;
		}
		Assert(idst < sizeof(GTPField));
		pdst[idst++] = c;
	} while (*pc++ && st != stERR);

	if(st == stINSFLD)
	{
		dbg(ERROR,"get_GTPLine: unterminated field in '%s'", buff);
		st = stERR;
	}

	trim_str(pLine->f1);
	trim_str(pLine->f2);
	trim_str(pLine->f3);
	trim_str(pLine->cmd);

	if(st == stERR)
		return -1;

	return fcnt;
}

/*
 * process native command and output the result into the file given by fp.
 *
 * 1) srvDataSourceDefine: scan id, table and field name and
 *    insert into lookup (data definition) table (comment on output)
 *
 * 2) OBSOLETE -- #undef UFIS_TABLE_TOPDEFS to activate it
 *
 *    gtTextPutFormattedFromDataSource, gtGIFPutFromDataSource: these are
 *    expected in tables only. Then for the first row, the db field
 *    references are resolved using the lookup table and the UFIS fields
 *    (language and db field) are filled correspondingly. For successive
 *    rows, the commands are commented out.
 *
 * 2) NEW -- #define UFIS_TABLE_TOPDEFS to activate it
 *
 *    After some discussions, it's not the ...FromDataSource commands in the
 *    first row that are replaced but the ...Define... commands that are present
 *    once for each column in the table. Note that these commands have OIDs equal
 *    to the data source ids used in the srvDataSourceDefine commands (and the 
 *    ...FromDataSource commands) so we can use them to lookup the field definition.
 *    The ...FromDataSource commands inside the table are simply commented out.
 *
 * 3) OLD -- 
 *    gtTableDefine: 1x1 tables are converted into simple objects by simply
 *    omitting this and the successive gtTable* commands, up to and
 *    including gtTablePut. (comment on output)
 *
 * 3) NEW---
 *    1x1 tables: TableDefine is converted into WindowDefine. A succesive
 *    PageSetCurrent command inside this table table definition is delayed
 *    until the TablePut command which is then replaced by it. All other
 *    gtTable command in between are commented out.
 *
 * 4) all other commands not starting with "gt": commented out
 *
 * Return value: 0 means special action done, caller may output the original
 *                 line with prepended comment
 *             > 0 means done, the function has output the required line
 *             < 0 indicates an error
 */

#define UFIS_TABLE_TOPDEFS /* extend top element defitions by ufis field defs */

static int process_GTPLine(FILE *fp, GTPLine *pLine, UDBXContext *pContext,
                           PEDTable *pDBTable,int *pTF,char* pclFile)
{
  const char *fct = "process_GTPLine";
  char *pa, c1, c2, c3, **pp;
  int lup, id, i1, i2, nr, nc, intab;
  int ilSourceID;
  int ilLineCounter;

  PEDRec *pRec;
  Assert(pLine->cmd[0]);

  lup = 0;
  intab = pContext->gt_table->nrows > 0 && pContext->gt_table->ncols > 0 ;

  /*
   * (1) srvDataSourceDefine -- build lookup table
   */
 
  if(strstr(pLine->cmd,"srvDataSourceDefine") == pLine->cmd)
    return parse_DataDefine(pLine, pContext) ? -1 : 0;
  /*
   * (2) TextDefine or GIFDefine inside table (top row definition) => resolve field refs
   */
  if(/*  intab &&  */(strstr(pLine->cmd,"gtTextDefineWithAttributes") == pLine->cmd ||
		      strstr(pLine->cmd,"gtGIFDefine") == pLine->cmd ||
		      strstr(pLine->cmd,"gtTextDefineEx") == pLine->cmd ||
		      strstr(pLine->cmd,"gtImageDefine") == pLine->cmd ))
    {
      if(sscanf(pLine->f1,"%d", &id) != 1)
	{
	  dbg(ERROR,"%s: expected id in '%s'", fct, pLine->f1);
	  return -1;
	}
  /*dbg(TRACE,"*****JHI%05d    ilLineCounter %d  ilSourceID %d",__LINE__,ilLineCounter,ilSourceID);*/
      /*UDBXPage_Build_LookupTable(&ilLineCounter,id,&ilSourceID,pclFile); JWE: 12.08.2003*/
      UDBXPage_Build_LookupTable(&ilLineCounter,id,&ilSourceID,pclFile);
/*  dbg(TRACE,"*****JHI%05d    ilLineCounter %d  ilSourceID %d",__LINE__,ilLineCounter,ilSourceID); */

      if( (pRec = match_PEDRec(pContext, pDBTable, ilSourceID)) == NULL)
	{
	  dbg(WARNING,"%s: can't match DataSource id in [%d] '%s'", fct,id, pLine->cmd);
	  fprintf(fp,"[%s] [0] [-] %s\n",pLine->f1, pLine->cmd);
	  return 0;
	}

      fprintf(fp,"[%s] [%d] [%s] %s\n",pLine->f1, pRec->nlang,
	      pRec->def->chars, pLine->cmd);
      return 1;
    }

  if(strstr(pLine->cmd,"gtTextPutFormattedFromDataSource") == pLine->cmd ||
     strstr(pLine->cmd,"gtGIFPutFromDataSource") == pLine->cmd||
     strstr(pLine->cmd,"gtImagePutFromDataSource") == pLine->cmd )
    {
      return 0;
    }
			
  if(strstr(pLine->cmd,"gtTextPutFormattedUnicode") == pLine->cmd)
    {
      pa = pLine->cmd + 26;
      GT2Cmdmap_Set(pContext->gt_cmdmap, pa);
      pp = pContext->gt_cmdmap->pparms;
      if(strstr(pp[0],"DATAFIELD")!=NULL)
	return 0;
    }
  else if(strstr(pLine->cmd,"gtTextPutFormatted") == pLine->cmd)
    {
      pa = pLine->cmd + 19;
      GT2Cmdmap_Set(pContext->gt_cmdmap, pa);
      pp = pContext->gt_cmdmap->pparms;
      if(strstr(pp[0],"DATAFIELD")!=NULL)
	return 0;
    }
  /*
   * (3) TableDefine -- get parameters and ignore 1x1 table
   */
  if(strstr(pLine->cmd,"gtTableDefine") == pLine->cmd)
    {
/*        Assert( pContext->gt_table->nrows == 0 && pContext->gt_table->ncols == 0); */
      pa = pLine->cmd + 14;
      GT2Table_Set(pContext->gt_table, pa);
      if(pContext->gt_table->nparms < 12)
	{
	  dbg(ERROR,"%s: to few table parms in '%s'", fct, pLine->cmd);
	  return -1;
	}
      if(sscanf(pa,"%d%c%d%c%d%c%d",&i1,&c1,&i2,&c2,&nr,&c3,&nc) != 7)
	{
	  dbg(ERROR,"%s: bad table format in '%s'", fct, pLine->cmd);
	  return -1;
	}
      if(nr < 1 || nc < 1)
	{
	  dbg(ERROR,"%s: bad table data in '%s'", fct, pLine->cmd);
	  return -1;
	}
      pContext->gt_table->nrows = nr;
      pContext->gt_table->ncols = nc;
      pContext->gt_table->cix = 0;
      /*
       * parm mapping      0    1    2    3   4     5     6     7     8     9     10    11
       * WindowDef Parms   x    y    w    h   fillc linec lines linew
       * TableDef Parms    x    y    nr   nc  vpr   h     colw  ???   fillc linec linst linew
       */
#if 0     
      if(nr == 1 && nc == 1)
	{
	  *pTF=1;
	  pp = pContext->gt_table->pparms;
	  fprintf(fp,"[%s] [0] [-] gtWindowDefine(%s,%s,%s,%s,%s,%s,%s,%s);\n",
		  pLine->f1, pp[0], pp[1], pp[6], pp[5], pp[8], pp[9], pp[10], pp[11]);
	  return 0;
	}
#endif
      fprintf(fp,"[%s] [0] [-] %s\n",pLine->f1, pLine->cmd);
      return 1;
    }
  /*
   *   Command mapping when object is changed (1x1 table)
   *
   *   jhi 10.10.2001 dxb
   */
#if 0
  if(strstr(pLine->cmd,"gtTableBringToFront") == pLine->cmd&&*pTF==1)
    {
    
      fprintf(fp,"[%s] [0] [-] gtWindowBringToFront();\n",
		  pLine->f1);
	  return 0;
    }

  if(strstr(pLine->cmd,"gtTableSetCurrent") == pLine->cmd&&*pTF==1)
    {
    
      fprintf(fp,"[%s] [0] [-] gtWindowSetCurrent(-1);\n",
		  pLine->f1);
	  return 0;
    }
#endif
  /*
   *   Truetypefont mapping because missing params -> pageeditor bug
   *   missing codepage 1256 and "b"
   *   jhi 10.10.2001 dxb
   */
 if(strstr(pLine->cmd,"gtTrueTypeFontDefine") == pLine->cmd)
    {
      pa = pLine->cmd + 21;
      GT2Cmdmap_Set(pContext->gt_cmdmap, pa);
      pp = pContext->gt_cmdmap->pparms;
      fprintf(fp,"[%s] [0] [-] gtTrueTypeFontDefine(%s,%s,1256,%s,%s,%s,0,%s,%s,%s);\n",
		  pLine->f1,pp[0], pp[1], pp[2], pp[4], pp[5], pp[7], pp[8], pp[9]);
	  return 0;
    }
 /*
  *   ignore this command !
  *   jhi 10.10.2001 dxb
  */
 if(strstr(pLine->cmd,"gtFilerRemoveFile") == pLine->cmd)
    {
      return 0;
    }

 /*
  *   map this command, we don't need the filesize!
  *   jhi 10.10.2001 dxb
  *   we also manage the data transfer
  *   jhi 14.8.2002
  */
 if(strstr(pLine->cmd,"gtFilerPutFile") == pLine->cmd)
    {
      pa = pLine->cmd + 15;
      GT2Cmdmap_Set(pContext->gt_cmdmap, pa);
      pp = pContext->gt_cmdmap->pparms;
      /*fprintf(fp,"[%s] [0] [-] gtFilerPutFile(%s,%s,-1,%s);\n",
	pLine->f1,pp[0], pp[1], pp[3]);*/
      fprintf(fp,"[%s] [0] [-] gtFilerPutFile(%s,%s,-1,%s);\n[%s] [0] [-] gtFilerPutData(-1,%s);\n",
	      pLine->f1,pp[0], pp[1], pp[3],pLine->f1, pp[3]);
      return 0;
    }
 if(strstr(pLine->cmd,"gtPageUnlockColors") == pLine->cmd)
    {
      pa = pLine->cmd + 19;
     /*   GT2Cmdmap_Set(pContext->gt_cmdmap, pa); */
/*        pp = pContext->gt_cmdmap->pparms; */
      /*fprintf(fp,"[%s] [0] [-] gtFilerPutFile(%s,%s,-1,%s);\n",
	pLine->f1,pp[0], pp[1], pp[3]);*/
      fprintf(fp,"[0] [0] [-] gtPageUnlockColors();\n[0] [0] [-] gtPageSetBlinking(250,255,255,255);\n");
      return 0;
    }
 if(strstr(pLine->cmd,"gtFilerPutData") == pLine->cmd)
    {
      return 0;
    }
  /*
   * (3) filter some other gtTable commands inside 1x1 tables
   */
#if 0
  if(strstr(pLine->cmd,"gtTable") == pLine->cmd)
    {
      nr = pContext->gt_table->nrows;
      nc = pContext->gt_table->ncols;
      if(strstr(pLine->cmd,"gtTablePut") == pLine->cmd)
	{
	  pContext->gt_table->nrows = pContext->gt_table->ncols = 0;
	  if(nr == 1 && nc == 1)
	    return 0;
	}
      else if(strstr(pLine->cmd,"gtTableAdjustColumn") == pLine->cmd)
	{
	  if(nr == 1 && nc == 1)
	    return 0;
	}
    }
#endif

  /* ======================== until aug 31 01 ==========================*/

  /*
   * (4) any command other than starting with "gt" -- ignore
   */

  if(strncmp(pLine->cmd,"gt",2))
    return 0;

  fprintf(fp,"[%s] [0] [-] %s\n",pLine->f1, pLine->cmd);
  return 1;
}
static int process_GTPLineSearch(int piObjectID,int *ppiSourceID, GTPLine *pLine)
{
  const char *fct = "process_GTPLineSearch";
  char *pa, c1, c2, c3, **pp,*pcltmpptr;
  int lup, id, i1, i2, nr, nc, intab;
  UDBXContext *pContext;
  PEDRec *pRec;
  Assert(pLine->cmd[0]);
  pContext = UDBXContext_Create();
	dbg(TRACE,"%s: line is <%s><%s><%s> cmd=<%s>", fct, pLine->f1,pLine->f2,pLine->f3,pLine->cmd);
  if(strstr(pLine->cmd,"gtTextPutFormattedUnicode") == pLine->cmd ||
     strstr(pLine->cmd,"gtTextPutFormatted") == pLine->cmd)
  {
		if(sscanf(pLine->f1,"%d", &id) != 1)
		{
			dbg(ERROR,"%s: expected id in '%s'", fct, pLine->f1);
			return -1;
		}
		/*dbg(TRACE,"%s: id is <%d>, object-id is <%d>", fct, id,piObjectID);*/

		if(id==piObjectID)
		{
                        if (strstr(pLine->cmd,"gtTextPutFormattedUnicode") != NULL)
			   pa = pLine->cmd + 26;
                        else
			   pa = pLine->cmd + 19;
			GT2Cmdmap_Set(pContext->gt_cmdmap, pa);
			pp = pContext->gt_cmdmap->pparms;
			if((pcltmpptr=strstr(pp[0],"DATAFIELD"))!=NULL)
			{ 
				pcltmpptr+=10;
				
				*ppiSourceID=atoi(pcltmpptr);
				UDBXContext_Destroy(pContext);
				return 0;
			}
		}
  }

  if(strstr(pLine->cmd,"gtTextPutFormattedFromDataSource") == pLine->cmd)
  {
    if(sscanf(pLine->f1,"%d", &id) != 1)
		{
			dbg(ERROR,"%s: expected id in '%s'", fct, pLine->f1);
			return -1;
		}

		/*dbg(TRACE,"%s: id is <%d>, object-id is <%d>", fct, id,piObjectID);*/
    if(id==piObjectID)
		{
			pa = pLine->cmd + 33;
			GT2Cmdmap_Set(pContext->gt_cmdmap, pa);
			pp = pContext->gt_cmdmap->pparms;
			*ppiSourceID=atoi(pp[0]);
			UDBXContext_Destroy(pContext);
			return 0;
		}     
  }

  if(strstr(pLine->cmd,"gtGIFPutFromDataSource") == pLine->cmd)
  {
		if(sscanf(pLine->f1,"%d", &id) != 1)
		{
			dbg(ERROR,"%s: expected id in '%s'", fct, pLine->f1);
			return -1;
		}

		/*dbg(TRACE,"%s: id is <%d>, object-id is <%d>", fct, id,piObjectID);*/
		if(id==piObjectID)
		{
			pa = pLine->cmd + 23;
			GT2Cmdmap_Set(pContext->gt_cmdmap, pa);
			pp = pContext->gt_cmdmap->pparms;
			*ppiSourceID=atoi(pp[0]);
			UDBXContext_Destroy(pContext);
			return 0;
		}     
  }

  if(strstr(pLine->cmd,"gtImagePutFromDataSource") == pLine->cmd )
  {
		if(sscanf(pLine->f1,"%d", &id) != 1)
		{
			dbg(ERROR,"%s: expected id in '%s'", fct, pLine->f1);
			return -1;
		}

		/*dbg(TRACE,"%s: id is <%d>, object-id is <%d>", fct, id,piObjectID);*/
		if(id==piObjectID)
		{
			pa = pLine->cmd + 25;
			GT2Cmdmap_Set(pContext->gt_cmdmap, pa);
			pp = pContext->gt_cmdmap->pparms;
			*ppiSourceID=atoi(pp[0]);
			UDBXContext_Destroy(pContext);
			return 0;
		}     
  }
  UDBXContext_Destroy(pContext);
  return 1;
}


static int UDBXPage_Build_LookupTable(int *pLC,int piObjectID,int *ppiSourceID,char *pclFile)
{
  char buff[GTP_MAXLSIZE];
  int result, rc, xlat,ilSourceID;
  GTPLine gtpl;
  /*   UDBXContext *pContext; */
  FILE *fpIn;
  const char *fct = "UDBXPage_Build_LookupTable";
 
  int rewrite=0;
  result = 0;
  xlat = -1; 	/* contains field number (1 or 3) if detected */
  rewrite=0;
  /*   pContext = UDBXContext_Create(); */
  if (pclFile == NULL || (fpIn = fopen(pclFile,"r")) == NULL)
    {
      dbg(ERROR,"%s: cannot open '%s': errno=%d", fct, STR0(pclFile), errno);
      return 0;
    }
  
  for(*pLC = 1;fget_line(buff,GTP_MAXLSIZE,fpIn); ++(*pLC) )
    {
      /*    strcpy(org_buff, buff); */	/* buff gets trimmed */
      rc = get_GTPLine(buff, &gtpl);
      
      if(rc < 0)		/* error */
	{
	  dbg(ERROR,"%s:Parser error bad input file (line=%d)", fct, *pLC);
	  result = -1;
	  fclose(fpIn);
	  return 0;
	}
      if(rc==1)
	{
	  /*rc = process_GTPLineSearch(piObjectID,ppiSourceID,&gtpl); JWE: 12.08.2003*/
	  rc = process_GTPLineSearch(piObjectID,&ilSourceID,&gtpl);
	  if(rc == 0)					/* comment out */
	    {
				*ppiSourceID = ilSourceID;
	      fclose(fpIn);
	      return 0;
	    }
	}/*else =comment or something else*/
    }
  fclose(fpIn);
  return 0;
}




static int parse_DataDefine(GTPLine *pLine, UDBXContext *pContext)
{
const char *fct = "parse_DataDefine";
int id, result, ix;
TokenZ *ptz[6], *pg, *pn;
char *p;
PEDRec *pRec;

	result = 0;
	if(sscanf(pLine->f1,"%d", &id) != 1)
	{
		dbg(ERROR,"%s: expected id in '%s'", fct, pLine->f1);
		return -1;
	}
	p = pLine->cmd;
	for(ix=0; ix<6; ++ix)
		ptz[ix] = TokenZ_CreateFromString(&p, "\"", "\"");
	pg = ptz[3];
	pn = ptz[5];
	if(!pg || !pn)
	{
		dbg(ERROR,"%s: expected table and field in '%s'", fct, pLine->cmd);
		result = -1;
	}
	else
	{
		pRec = PEDRec_CreateI(pn->chars, pg->chars, id);
		if(pRec)
		{
			PEDTable_AddRec(pContext->dd_table, pRec);
/*  		PEDTable_print(pContext->dd_table); */
		}
		else
			result = -1;
	}

	for(ix=0; ix<6; ++ix)
		TokenZ_Destroy(ptz[ix]);

	return result;
}

static PEDRec *match_PEDRec(UDBXContext *pContext, PEDTable *pDBTable, int id)
{
int ix;
PEDRec *res, *p1, *p2;
	for(p1=NULL, ix=0; ix<pContext->dd_table->nrec; ++ix)
	{
  /*dbg(TRACE,"******JHI%05d id %d",__LINE__,id);*/
		p1 = pContext->dd_table->prec[ix];
  /*dbg(TRACE,"******JHI%05d id %d",__LINE__,id);*/ 
		if(p1->id == id)
			break;
	}
  /*dbg(TRACE,"******JHI id %d ix %d pcontext %d",id,ix,pContext->dd_table->nrec); */
	if(ix >= pContext->dd_table->nrec)
		return NULL;

	for(res=NULL, ix=0; ix < pDBTable->nrec; ++ix)
	{
		p2 = pDBTable->prec[ix];
/* Jun 28 01 ORA generates table names with capitals: strcmp -> strcasecmp */
		if(strcasecmp(p1->name->chars,p2->name->chars) == 0 &&
		   strcasecmp(p1->grp->chars,p2->grp->chars) == 0)
		{
			res = p2;
			dbg(TRACE,"match_PEDRec: found DATASOURCE/PEDTAB match <%s><%s> -- <%s><%s>",p1->name->chars,p1->grp->chars,p2->name->chars,p2->grp->chars);
			/*dbg(TRACE,"******JHI%05d <%s>",__LINE__,res->grp->chars);  */
			break;
		}
	}
	return res;
}

/*
 * === API functions ===
 */

UDBXPage *UDBXPage_CreateFromFile(char *pcFileName, PEDTable *pTab)
{
  FILE *fpIn, *fpTmp;
  UDBXPage *page;
  int rc, line;
  char TmpFile[100],  buff[500];
  static int cnt = 0;
  static PEDTable *pTab_printed = NULL;
  static char *fct = "UDBXPage_CreateFromFile";
  char p[2];

  if(pTab_printed != pTab)
    {
      PEDTable_print(pTab); 
      pTab_printed = pTab;
    }

  if (pcFileName == NULL || (fpIn = fopen(pcFileName,"r")) == NULL)
    {
      dbg(ERROR,"%s: cannot open '%s': errno=%d", fct, STR0(pcFileName), errno);
      return NULL;
    }

  sprintf(TmpFile,"/tmp/dsplib%d_%d.tmp", getpid(), cnt++);
  dbg(TRACE,"%s: preprocessing into %s", fct, TmpFile);
  if( (fpTmp = fopen(TmpFile,"w")) == NULL)
    {
      dbg(ERROR,"%s: cannot open '%s' for write: errno=%d", fct, TmpFile, errno);
      fclose(fpIn);
      return NULL;
    }

  rc = UDBXPage_PreprocessFile(fpIn, fpTmp, pTab, &line,p,pcFileName);
  fclose(fpIn);
  fclose(fpTmp);

  if(rc)
    {
      dbg(ERROR,"%s: parse error in %s, line %d", fct, pcFileName, line);
      return NULL;
    }

  if( (fpTmp = fopen(TmpFile,"r")) == NULL)
    {
      dbg(ERROR,"%s: cannot open '%s' for read: errno=%d", fct, TmpFile, errno);
      page = NULL;
    }
  else
    {
      page = malloc(sizeof(UDBXPage));
      Assert(page);
      page->pcFileName = strdup(pcFileName);
      page->iNCommands = 0;
      page->prCommands = NULL;
      dsplex(fpTmp, page);
      fclose(fpTmp);	/* Jun 29 01 jhe close was missing */
    }

  /*
   * 010830 jhi/jhe preprocessed file replaces input file, unless the environment
   * variable UDBX_PAGE_CREATE_OPTS exists and contains the character 'k' telling 
   * to keep both the input and the tmp file.
   */ 
  /*   jhi 11.10.2001 depends on file only from pageedit should */
  /*   p = getenv("UDBX_PAGE_CREATE_OPTS"); */
#if 0
  if(page != NULL && (p == NULL || strchr(p,'k') == NULL) )
    {
      if (move_files(TmpFile, pcFileName))
	dbg(ERROR,"can't move %s to %s (errno=%d)", TmpFile, pcFileName, errno);
    }
#endif
  /*not set*/
  if(page != NULL && ( strchr(p,'k') == NULL) )
    {
      if (remove(TmpFile))
	dbg(ERROR,"can't  remove tmp file  %s (errno=%d)", TmpFile, errno);
    }
  /*set*/
 if(page != NULL && ( strchr(p,'k') != NULL) )
    {
      if (move_files(TmpFile, pcFileName))
	dbg(ERROR,"can't move %s to %s (errno=%d)", TmpFile, pcFileName, errno);
    }
  return page;
}

 





void UDBXPage_Destroy(UDBXPage *pThis)
{
int ix;
	if (pThis == NULL)
		return;
	if (pThis->prCommands)
	{
		for(ix=0; ix<pThis->iNCommands; ++ix)
		{
			UDBXCommand_Destroy(pThis->prCommands[ix]);
		}
		free(pThis->prCommands);
	}

	if(pThis->pcFileName)
		free(pThis->pcFileName);
	free(pThis);
}
/*
 * Preprocess page layout file: reading the fle specified by fpIn, the
 * function recognizes the file format and translates it upon output into
 * fpOut if necessary. Files in 3 field UFIS format are left unchanged
 * (i.e. simply copied). Files containing one  field in square brackets
 * followed by a command are extended to 3-fields format upon output.
 *
 * For table objects, data source lookup is performed based on the table
 * specified by pTab. Line number information is returned in pLC.
 * 
 * Return values:  0 success
 *                -1 format of the file is not recognized or 
 *                   another error is detected.
 */

int UDBXPage_PreprocessFile(FILE *fpIn, FILE *fpOut, PEDTable *pTab, int *pLC,char *p,char *pclFile)
{
char buff[GTP_MAXLSIZE], org_buff[GTP_MAXLSIZE];
int result, rc, xlat;
GTPLine gtpl;
UDBXContext *pContext;
const char *fct = "UDBXPage_PreprocessFile";
int onebyonetable=0;
int rewrite=0;
	result = 0;
	xlat = -1; 	/* contains field number (1 or 3) if detected */
        rewrite=0;
	pContext = UDBXContext_Create();
	onebyonetable=0;
	p[0]='\0';
	memset(buff, 0x00,GTP_MAXLSIZE);
	memset(org_buff, 0x00,GTP_MAXLSIZE);

	for(*pLC = 1; fget_line(buff,GTP_MAXLSIZE,fpIn); ++(*pLC) )
	{
		strcpy(org_buff, buff);	/* buff gets trimmed */
		rc = get_GTPLine(buff, &gtpl);

		if(rc < 0)		/* error */
		{
			dbg(ERROR,"%s:Parser error bad input file (line=%d)", fct, *pLC);
			result = -1;
			break;
		}

		else if (rc == 0)	/* comment or empty line */
		{
			fprintf(fpOut,"%s\n",org_buff);
			continue;
		}
		else if (xlat == -1)
		{
			if(rc == 3 || rc == 1)
			{
				xlat = rc;
			}
			else								/* unsupported */
			{
				dbg(ERROR,"UDBXPage_TranslateFile: unsupported format (%d fields)",rc);
				result = -1;
				break;
			}
			if(xlat == 1)
			{
			  rewrite=1;
			  dbg(TRACE,"Starting one-field page layout translation");
			  fprintf(fpOut,"// ABB Ufis translated page layout file V1\n");
			}
		}
		else if (rc != xlat)
		{
			dbg(ERROR,"UDBXPage_TranslateFile: inconsistent number of fields");
			result = -1;
			break;
		}

		if (xlat == 1)
		{
		        if( rewrite==1)
			  rc = process_GTPLine(fpOut, &gtpl, pContext, pTab,&onebyonetable,pclFile);
			if(rc == 0)					/* comment out */
			  {
			    strcpy(p,"k");
			    /*
			    if(debug_level==DEBUG)
			      fprintf(fpOut,"//ufis_xlat %s\n",org_buff);
			    */
			      }else if(rc < 0)
			    {
			      result = -1;
			      break;
			    }
		}
		 else 
		 fprintf(fpOut,"%s\n",org_buff); 

		memset(buff, 0x00,GTP_MAXLSIZE);
		memset(org_buff, 0x00,GTP_MAXLSIZE);		 
	}

	UDBXContext_Destroy(pContext);
	return result;
}

/*
 * ========== basic types oo-like implementation ==========
 */

static char *pcgIDEL = " \t,"; 	/* standard delimiters */
static char *pcgLDEL = " \t";		/* standard delimiters at beginning/end of line */

/*
 * ===== FieldReference
 */

void FieldReference_Init(FieldReference *pThis)
{
	Assert(pThis);

  pThis->pcTableName = NULL;
  pThis->pcFieldName = NULL;
  pThis->iStart = 0;
  pThis->iEnd = 0;
}

int FieldReference_Set(FieldReference *pThis, char **pcData)
{
  TokenZ *pTTab, *pTField, *pTRange;
  int s, e, res, l;

  Assert(pThis);

  FieldReference_Reset(pThis);

  pTTab = TokenZ_CreateFromString(pcData, pcgLDEL, ".");
  pTField = TokenZ_CreateFromString(pcData, ".", " \t(");
  pTRange = TokenZ_CreateFromString(pcData, "(", " \t)");

  res = 0;

  if (pTTab && pTTab->chars)
  {
    pThis->pcTableName = strdup(pTTab->chars);
/*
 * Jun 30 01 provisoric removal of "TAB" ending
 */
		l = strlen(pThis->pcTableName);
		if (l > 3 && strcasecmp(pThis->pcTableName+l-3,"TAB") == 0)
			pThis->pcTableName[l-3] = '\0';
    ++res;
  }

  if (pTField && pTField->chars)
  {
    pThis->pcFieldName = strdup(pTField->chars);
    ++res;
  }
  if (pTRange && pTRange->chars && sscanf(pTRange->chars,"%d,%d",&s, &e)==2)
  {
    pThis->iStart = s;
    pThis->iEnd = e;
    ++res;
  }

  TokenZ_Destroy(pTTab);
  TokenZ_Destroy(pTField);
  TokenZ_Destroy(pTRange);
  return res;
}

void FieldReference_Reset(FieldReference *pThis)
{
	Assert(pThis);

	if(pThis->pcTableName)
		free(pThis->pcTableName);
	if(pThis->pcFieldName)
		free(pThis->pcFieldName);

	FieldReference_Init(pThis);

}

/*
 * ===== FieldDisplayType
 */

static struct
{
	char *n;
	int t;
} FieldDisplayType_map[] = 
{
  { "N", UDBX_DT_N },{ "M", UDBX_DT_M }, { "S", UDBX_DT_S }, { "L", UDBX_DT_L },
  { "T", UDBX_DT_T }, { "D", UDBX_DT_D }, { "DT", UDBX_DT_DT },
  { "CT", UDBX_DT_CT }, { "CD", UDBX_DT_CD }, { "CDT", UDBX_DT_CDT },
  { "TT", UDBX_DT_TT }, { "TD", UDBX_DT_TD }, { "TDT", UDBX_DT_TDT },
  { "R", UDBX_DT_R }, { "B", UDBX_DT_B }, { "A", UDBX_DT_A },
  { "AL", UDBX_DT_AL }, {"J", UDBX_DT_J }, {"LJ", UDBX_DT_LJ }, {"F", UDBX_DT_F },
  { NULL, UDBX_DT_ANY }
};


void FieldDisplayType_Init(FieldDisplayType *pThis)
{
	Assert(pThis);
  pThis->rAny.iType = UDBX_DT_ANY;
}

int FieldDisplayType_Set(FieldDisplayType *pThis, char **pcData)
{  
  TokenZ *pTDispType, *pTD1, *pTD2, *pTD3;
  int res, iv;

  Assert(pThis);

	FieldDisplayType_Reset(pThis);

  res = 0;
  pTDispType = TokenZ_CreateFromString(pcData, pcgLDEL, pcgIDEL);
  pTD1 = pTD2 = pTD3 = NULL;

  if (pTDispType->chars	)
  {
    pThis->rAny.iType = FieldDisplayType_GetByName(pTDispType->chars);
    switch (pThis->rAny.iType)
		{
			case UDBX_DT_N:	/* normal text -- data */
		        case UDBX_DT_F:
				break;
			case UDBX_DT_M:	/* normal text without BLANK -- data */
				break;
			case UDBX_DT_J:	/* new format - for whatever */
				break;
			case UDBX_DT_S:	/* static text not yet supported */
				pThis->rS.pcText = NULL;
				break;
			case UDBX_DT_L:
			case UDBX_DT_LJ:
				pTD1 = TokenZ_CreateFromString(pcData, pcgIDEL, pcgLDEL);
/* rLJ is identical -> no need to distinguish for pThis->rLJ.pcExtension */
				pThis->rL.pcExtension = pTD1 && pTD1->chars ? strdup(pTD1->chars):NULL;
				break;
			case UDBX_DT_T:
			case UDBX_DT_D:
			case UDBX_DT_DT:
			case UDBX_DT_CT:
			case UDBX_DT_CD:
			case UDBX_DT_CDT:
			case UDBX_DT_TT:
			case UDBX_DT_TD:
			case UDBX_DT_TDT:
				pTD1 = TokenZ_CreateFromString(pcData, pcgIDEL, pcgLDEL);
				pThis->rT.pcFormat = pTD1 && pTD1->chars ? strdup(pTD1->chars) : NULL;
				break;
			case UDBX_DT_R:
#ifdef brute_force_separator
        pTD1 = TokenZ_CreateFromString(pcData, pcgIDEL, ","); /* maybe blank */
        pThis->rR.pcSeparator = pTD1 && pTD1->chars ? strdup(pTD1->chars):NULL;
#else 
				pThis->rR.pcSeparator = FieldDisplayType_GetSep(pcData);
#endif /* good_old_days */
        FieldReference_Init(&(pThis->rR.rFieldRef)); /* jhe Jun 27 01 bug */
        FieldReference_Set(&(pThis->rR.rFieldRef), pcData);
        break;
			case UDBX_DT_B:
				pTD1 = TokenZ_CreateFromString(pcData, pcgIDEL, pcgIDEL);
				pThis->rB.pcType = pTD1 && pTD1->chars ? strdup(pTD1->chars) : NULL;
				pTD2 = TokenZ_CreateFromString(pcData, pcgIDEL, pcgIDEL);
				if(pTD2 && pTD2->chars && sscanf(pTD2->chars, "%d", &iv) == 1)
					pThis->rB.iBlinkColor = iv;
				else
					pThis->rB.iBlinkColor = 0;
				pTD3 = TokenZ_CreateFromString(pcData, pcgIDEL, pcgIDEL);
				pThis->rB.cTimeConversion = pTD3 && pTD3->chars ? pTD3->chars[0] : '\0';
				pThis->rB.iNFieldNames = TokenZ_GetStrings(pcData,",",&(pThis->rB.p2cFieldNames));
				break;
			case UDBX_DT_A:
				pTD1 = TokenZ_CreateFromString(pcData, pcgIDEL, pcgIDEL);
				if(pTD1 && pTD1->chars && sscanf(pTD1->chars, "%d", &iv) == 1)
					pThis->rA.iNoOfAirports = iv;
				else
					pThis->rA.iNoOfAirports = 0;
				pTD2 = TokenZ_CreateFromString(pcData, pcgIDEL, pcgIDEL);
				pThis->rA.cSequence = pTD2 && pTD2->chars ? pTD2->chars[0] : '\0';
#ifdef brute_force_separator
				pTD3 = TokenZ_CreateFromString(pcData, pcgIDEL, pcgIDEL);
				pThis->rA.pcSeparator = pTD3 && pTD3->chars ? strdup(pTD3->chars) : NULL;
#else 
				pThis->rA.pcSeparator = FieldDisplayType_GetSep(pcData);
#endif /* good_old_days */
				pThis->rA.iNFieldNames = TokenZ_GetStrings(pcData,pcgIDEL,&(pThis->rA.p2cFieldNames));
				break;
			case UDBX_DT_AL:
				pTD1 = TokenZ_CreateFromString(pcData, pcgIDEL, pcgLDEL);
				pThis->rAL.pcFieldName = pTD1 && pTD1->chars ? strdup(pTD1->chars) : NULL;
				break;
			default:
				break;
		}
  }

  TokenZ_Destroy(pTDispType);
  TokenZ_Destroy(pTD1);
  TokenZ_Destroy(pTD2);
  TokenZ_Destroy(pTD3);
  return res;
}

void FieldDisplayType_Reset(FieldDisplayType *pThis)
{
int ix;
	Assert(pThis);

	switch (pThis->rAny.iType)
	{
		case UDBX_DT_N:	/* normal text -- data */
		case UDBX_DT_F:
			break;
		case UDBX_DT_M:	/* normal text without BLANK -- data */
			break;
		case UDBX_DT_J:
			break;
		case UDBX_DT_S:	/* static text not yet supported */
			if(pThis->rS.pcText)
				free(pThis->rS.pcText);
			break;
		case UDBX_DT_L:
		case UDBX_DT_LJ:
			if(pThis->rL.pcExtension)
				free(pThis->rL.pcExtension);
			break;
		case UDBX_DT_T:
		case UDBX_DT_D:
		case UDBX_DT_DT:
		case UDBX_DT_CT:
		case UDBX_DT_CD:
		case UDBX_DT_CDT:
		case UDBX_DT_TT:
		case UDBX_DT_TD:
		case UDBX_DT_TDT:
			if(pThis->rT.pcFormat)
				free(pThis->rT.pcFormat);
			break;
		case UDBX_DT_R:
			if(pThis->rR.pcSeparator)
				free(pThis->rR.pcSeparator);
			FieldReference_Reset(&(pThis->rR.rFieldRef));
			break;
		case UDBX_DT_B:
			if(pThis->rB.pcType)
				free(pThis->rB.pcType);
			if(pThis->rB.p2cFieldNames)
			{
				for(ix=0; ix<pThis->rB.iNFieldNames; ++ix)
				{
 					free(pThis->rB.p2cFieldNames[ix]);
				}
				free(pThis->rB.p2cFieldNames);
			}
			break;
		case UDBX_DT_A:
			if(pThis->rA.pcSeparator)
				free(pThis->rA.pcSeparator);
			if(pThis->rA.p2cFieldNames)
			{
				for(ix=0; ix<pThis->rA.iNFieldNames; ++ix)
				{
 					free(pThis->rA.p2cFieldNames[ix]);
				}
				free(pThis->rA.p2cFieldNames);
			}
			break;
		case UDBX_DT_AL:
			if(pThis->rAL.pcFieldName)
				free(pThis->rAL.pcFieldName);
			break;
		case UDBX_DT_ANY:  /* May 8 01 jhe any is permitted (initial state) */
			break;
		default:
			dbg(WARNING,"FieldDisplayType_Reset: unexpected type %d", pThis->rAny.iType);
			break;
  }

	FieldDisplayType_Init(pThis);
}

int FieldDisplayType_GetByName(char *pcName)
{
  int ix;
  for(ix=0; FieldDisplayType_map[ix].n; ++ix)
    {
      if (strcmp(pcName,FieldDisplayType_map[ix].n) == 0)
	break;
    }
  return FieldDisplayType_map[ix].t;
}

char *FieldDisplayType_GetByType(int iType)
{
  int ix;
  for(ix=0; FieldDisplayType_map[ix].n; ++ix)
    {
      if (FieldDisplayType_map[ix].t == iType)
  break;
    }
  return FieldDisplayType_map[ix].n;
}

/*
 * Get separator which my be a quoted string or a single char including comma. The
 * return value is a dynamically allocated copy of the separator string or NULL when
 * no separator string was found
 */

char *FieldDisplayType_GetSep(char **pcData)
{
TokenZ *pTDispType, *pTD1, *pTD2;
char *sep;
    pTD1 = pTD2 = NULL;
    sep = NULL;
    while(**pcData)    /* find next ',' */
    {
/* break on comma, point to next char in each case */
        if(*(*pcData)++ == ',')
            break;
    }

/* special case: separator is a ',' char */
    if(**pcData == ',')
    {
         sep = strdup(",");
         ++*pcData; /* skip this ',' then everything until the next ','*/
         pTD2 = TokenZ_CreateFromString(pcData, "", ","); /* see above*/
    }

/* separator is a quoted string */
    else if(**pcData == '"')
    {
         pTD1 = TokenZ_CreateFromString(pcData, "\"", "\""); /* until next  */
         sep = pTD1 && pTD1->chars ? strdup(pTD1->chars) : NULL;
         pTD2 = TokenZ_CreateFromString(pcData, "",","); /* skip until  */
    }
/* separator is anything from the current pos up to and excluding the next comma */
    else
    {
         pTD1 = TokenZ_CreateFromString(pcData, "", ","); /* maybe blank */
         sep = pTD1 && pTD1->chars ? strdup(pTD1->chars) : NULL;
    }
    if(**pcData == ',') /* skip delimiter if any */
      ++*pcData;

    TokenZ_Destroy(pTD1);
    TokenZ_Destroy(pTD2);

    return sep;
}
          
/*
 * ===== FieldDescription
 */

FieldDescription *FieldDescription_Create()
{
  FieldDescription *p;
  p = malloc(sizeof(FieldDescription));
  Assert(p);

  FieldReference_Init(&(p->rFieldRef));
	FieldDisplayType_Init(&(p->rDpyType));
  p->iSize = 0;

  return p;
}

void FieldDescription_Destroy(FieldDescription *pThis)
{
	if (pThis == NULL)
		return;
  FieldReference_Reset(&(pThis->rFieldRef));
  FieldDisplayType_Reset(&(pThis->rDpyType));
	free(pThis);
}

/*
 * ===== UDBXCommand
 */


UDBXCommand *UDBXCommand_Create()
{
UDBXCommand *p;

	p = malloc(sizeof(UDBXCommand));
	Assert(p != NULL);

	p->pcObjectId = NULL;
	p->iLanguage = 0;

	p->iNFieldDesc = 0;
	p->prFieldDesc = NULL;

	p->pcDZCommand = NULL;
	p->iNDZParms = 0;
	p->p2cDZParms = NULL;
	p->prDZFile = DynStr_Create();

	p->iNError = 0;
	return p;
}

void UDBXCommand_Destroy(UDBXCommand *pThis)
{
int ix;

	if(pThis == NULL)
		return;

	if(pThis->pcObjectId)
		free(pThis->pcObjectId);

	if (pThis->prFieldDesc)
	{
		for(ix=0; ix<pThis->iNFieldDesc; ++ix)
			FieldDescription_Destroy(pThis->prFieldDesc[ix]);
		free(pThis->prFieldDesc);
	}

	if (pThis->pcDZCommand)
		free(pThis->pcDZCommand);

	if (pThis->p2cDZParms)
	{
		for(ix=0; ix < pThis->iNDZParms; ++ix)
			free (pThis->p2cDZParms[ix]);
		free(pThis->p2cDZParms);
	}

	DynStr_Destroy(pThis->prDZFile);
}


/*
 * ===== DynStr: dynamic string utility to handle growing strings
 */


DynStr *DynStr_Create()
{
	return DynStr_CreateQ(0);
}

DynStr *DynStr_CreateQ(unsigned int q)      
{
DynStr *p;
	p = malloc(sizeof(DynStr));
	Assert(p);
	p->chars = NULL;
	p->blksz = q > 1 ? q : 64;
	DynStr_Reset(p);
	return p;
}

void DynStr_Destroy(DynStr *pThis)
{
	if(pThis == NULL)
		return;
	if(pThis->chars)
		free(pThis->chars);
	free(pThis);
}

void DynStr_AppendStr(DynStr *pThis, char *s)
{
	if (s == NULL)
		return;
	while(*s)
		DynStr_AppendC(pThis, *s++);
}

    
void DynStr_AppendC(DynStr *pThis, char c)        
{
	if (pThis->nfree <= 1) /* one for \0 */
	{
		++(pThis->nblk);
		pThis->chars = realloc(pThis->chars, pThis->nblk * pThis->blksz);
		Assert(pThis->chars);
		pThis->nfree += pThis->blksz;
	}
	pThis->chars[pThis->length++] = c;
	pThis->chars[pThis->length  ] = '\0';
	--(pThis->nfree);
	
}

void DynStr_Reset(DynStr *pThis)
{
	if (pThis->chars)
		free(pThis->chars);
	pThis->nblk = 0;
	pThis->nfree = 0;
	pThis->length = 0;
	pThis->chars = NULL;
}

/*
 * ===== TokenZ: Token class to get tokens as null terminated string
 */

TokenZ *TokenZ_CreateFromString(char **p2cSrc, char *pcSkip, char *pcTerm)
{
DynStr *tok;
char *pp;
/*
 * no source, no token
 */
	if (p2cSrc == NULL)
		return NULL;

/*
 * skip unwished leading chars
 */

	while(**p2cSrc)
	{
		for(pp=pcSkip; *pp; ++pp)
		{
			if (**p2cSrc == *pp)
				break;
		}
		if (!*pp)	/* then it was not a char from the skip set */
			break;
		++*p2cSrc;
	}
	if(!**p2cSrc)
		return NULL;
/*
 * get token char until either any of the delimiters or null
 */
	tok = NULL;
	while(**p2cSrc)
	{
		for(pp=pcTerm; pp && *pp; ++pp)
		{
			if (**p2cSrc == *pp)
				break;
		}
		if (pp && *pp) /* then it was a char from the term set */
		{
			break;
		}
		else
		{
			if (tok == NULL)
			{
				tok = DynStr_Create();
				Assert(tok);
			}
			DynStr_AppendC(tok, **p2cSrc);
		}
		++*p2cSrc;
	}
	return tok;
}

void TokenZ_Destroy(TokenZ *pThis)
{
	if(pThis == NULL)
		return;
	if (pThis->chars)
		free(pThis->chars);
	free(pThis);
}

int TokenZ_GetStrings(char **p2cSrc, char *pcDel, char ***p3cList)
{
int n;
TokenZ *tz;

	*p3cList = NULL;
	for(n=0; (tz = TokenZ_CreateFromString(p2cSrc, pcDel, pcDel)) != NULL; ++n)
	{
		Assert(tz->chars);
		*p3cList = realloc(*p3cList, (n+1) * sizeof(char *));
		Assert(*p3cList);
		(*p3cList)[n] = strdup(tz->chars);
		TokenZ_Destroy(tz);
	}
	return n;
}

PEDTable *PEDTable_CreateFromDB(const char *db)
{
PEDTable *pThis;
int init;
PEDRec *prec;
	pThis = PEDTable_Create();

	init = 1;
	while ( (prec = get_PEDRec(db, init)) != NULL)
	{
	  PEDTable_AddRec(pThis, prec);
		init = 0;
	}

	return pThis;
}


PEDTable *PEDTable_Create()
{
PEDTable *pThis;
	pThis = malloc(sizeof(PEDTable));
	Assert(pThis);
	pThis->nrec = 0;
	pThis->prec = NULL;
	pThis->nfree = 0;
	pThis->blksize = 64;

	return pThis;
}

void PEDTable_Destroy(PEDTable *pThis)
{
int ix;
	if(pThis == NULL)
		return;
	for(ix=0; ix<pThis->nrec; ++ix)
		PEDRec_Destroy(pThis->prec[ix]);
	if(pThis->prec)
		free(pThis->prec);
	free(pThis);
}

void PEDTable_AddRec(PEDTable *pThis, PEDRec *pRec)
{
int sz;
	Assert(pThis);
	if (pThis->nfree == 0)
	{
		sz = (pThis->nrec + pThis->blksize) * sizeof(PEDRec *);
		pThis->nfree = pThis->blksize;
		pThis->prec = realloc(pThis->prec, sz);
		Assert(pThis->prec);
	}
	pThis->prec[pThis->nrec++] = pRec;
	pThis->nfree--;
}

PEDRec *PEDRec_CreateI(char *pNm, char *pGrp, int id)
{
PEDRec *pThis;
int l1, l2;

	if(!pNm || !pGrp)
	{
		dbg(ERROR,"PEDRec_CreateI: mandatory field missing");
		return NULL;
	}

	l1 = trim_str(pNm); 
	l2 = trim_str(pGrp);

	if( l1 == 0 || l2 == 0)
	{
		dbg(ERROR,"PEDRec_CreateI: mandatory field empty");
		return NULL;
	}

	pThis = malloc(sizeof(PEDRec));
	Assert(pThis);

	pThis->id = id;

	pThis->name = DynStr_Create();
	DynStr_AppendStr(pThis->name,pNm);

	pThis->grp = DynStr_Create();
	DynStr_AppendStr(pThis->grp,pGrp);

	pThis->desc = NULL;
	pThis->def = NULL;
	pThis->lang = NULL;
	pThis->nlang = 0;

	return pThis;
}

PEDRec *PEDRec_CreateC(char *pNm,char *pGrp,char *pDesc,char *pDef,char *pLang)
{
PEDRec *pThis;
int l1, l2, l3, l4, l5;
	if(!pNm || !pGrp || !pDef || !pLang)
	{
		dbg(ERROR,"PEDRec_CreateC: mandatory field missing");
		return NULL;
	}

	l1 = trim_str(filter_SepCharsIn(pNm)); 
	l2 = trim_str(filter_SepCharsIn(pGrp));
	l3 = trim_str(filter_SepCharsIn(pDef));
  l4 = trim_str(filter_SepCharsIn(pLang));
	l5 = trim_str(filter_SepCharsIn(pDesc));

	if( l1 == 0 || l2 == 0 || l3 == 0 || l4 == 0)
	{
		dbg(ERROR,"PEDRec_CreateC: mandatory field empty");
		return NULL;
	}
	
	pThis = malloc(sizeof(PEDRec));
	Assert(pThis);

	pThis->id = 0;

	pThis->name = DynStr_Create();
	DynStr_AppendStr(pThis->name,pNm);

	pThis->grp = DynStr_Create();
	DynStr_AppendStr(pThis->grp,pGrp);

	pThis->desc = DynStr_Create();
	DynStr_AppendStr(pThis->desc,pDesc);

	pThis->def = DynStr_Create();
	DynStr_AppendStr(pThis->def,pDef);

	pThis->lang = DynStr_Create();
	DynStr_AppendStr(pThis->lang,pLang);
	pThis->nlang = strcmp(pLang,"latin") ? 1 : 0;

	return pThis;
}

void PEDRec_Destroy(PEDRec *pThis)
{
	if(pThis == NULL)
		return;
	DynStr_Destroy(pThis->name);
	DynStr_Destroy(pThis->desc);
	DynStr_Destroy(pThis->def);
	DynStr_Destroy(pThis->grp);
	DynStr_Destroy(pThis->lang);
	free(pThis);
}
	
GT2Table *GT2Table_Create()
{
GT2Table *pThis;

	pThis = malloc(sizeof(GT2Table));
	Assert(pThis);
	pThis->pparms = NULL;
	GT2Table_Reset(pThis);
	return pThis;
}
GT2Cmdmap *GT2Cmdmap_Create()
{
GT2Cmdmap *pThis;

	pThis = malloc(sizeof(GT2Cmdmap));
	Assert(pThis);
	pThis->pparms = NULL;
	GT2Cmdmap_Reset(pThis);
	return pThis;
}

void GT2Table_Destroy(GT2Table *pThis)
{
	if(pThis == NULL)
		return;
	GT2Table_Reset(pThis);
	free(pThis);
}
void GT2Cmdmap_Destroy(GT2Cmdmap *pThis)
{
	if(pThis == NULL)
		return;
	GT2Cmdmap_Reset(pThis);
	free(pThis);
}

void GT2Table_Reset(GT2Table *pThis)
{
int ix;
	Assert(pThis);
	if(pThis->pparms)
	{
		for(ix=0; ix<pThis->nparms; ++ix)
		{
			Assert(pThis->pparms[ix]);
			free(pThis->pparms[ix]);
		}
		free(pThis->pparms);
	}
	pThis->nrows = pThis->ncols = pThis->cix = 0;
	pThis->nparms = 0;
}
void GT2Cmdmap_Reset(GT2Cmdmap *pThis)
{
int ix;
	Assert(pThis);
	if(pThis->pparms)
	{
		for(ix=0; ix<pThis->nparms; ++ix)
		{
			Assert(pThis->pparms[ix]);
			free(pThis->pparms[ix]);
		}
		free(pThis->pparms);
	}
}

int GT2Table_Set(GT2Table *pThis, char *pstr)
{
	Assert(pThis);
	Assert(pstr);
	GT2Table_Reset(pThis);

	pThis->nparms = TokenZ_GetStrings(&pstr,",)",&(pThis->pparms));
	return pThis->nparms;
}
int GT2Cmdmap_Set(GT2Cmdmap *pThis, char *pstr)
{
	Assert(pThis);
	Assert(pstr);
	GT2Cmdmap_Reset(pThis);

	pThis->nparms = TokenZ_GetStrings(&pstr,",)",&(pThis->pparms));
	return pThis->nparms;
}

UDBXContext *UDBXContext_Create()
{
UDBXContext *pThis;
	pThis = malloc(sizeof(UDBXContext));
	Assert(pThis);

	pThis->gt_table = GT2Table_Create();
	pThis->dd_table = PEDTable_Create();
	pThis->gt_cmdmap = GT2Cmdmap_Create();
	return pThis;
}

void UDBXContext_Destroy(UDBXContext *pThis)
{
	if(pThis == NULL)
		return;
	GT2Table_Destroy(pThis->gt_table);
	PEDTable_Destroy(pThis->dd_table);
	GT2Cmdmap_Destroy(pThis->gt_cmdmap);
	free(pThis);
}
            
static const char *OurSepChars = "\042\047\054\012\015";
static const char *DBSepChars = "\260\261\262\264\263";

/*
 * replace comma placeholder chars by commas
 */

char *filter_SepCharsIn(char *pin)
{
int lr, ix, sx;

	lr = strlen(OurSepChars);
	Assert(lr=strlen(DBSepChars));

	for(sx=0; pin[sx]; ++sx)
	{
		for(ix=0; ix<lr; ++ix)
			if (pin[sx] == DBSepChars[ix])
				break;
		if(ix < lr)
			pin[sx] = OurSepChars[ix];
	}
	return pin;
}

/*========== <<< JHE/JHI >>> ==========*/

int GetdZParameter(char *pcpResult,UDBXCommand* pcpPC,int ilen)
{
  char pclTmp[1024];
  int fx;
  memset(pclTmp,0x00,1024);
  for(fx=0; fx<pcpPC->iNDZParms; ++fx)
  {
    if(fx==0)
			strcpy(pclTmp,pcpPC->p2cDZParms[fx]);
    else
			sprintf(pclTmp,"%s,%s",pclTmp,pcpPC->p2cDZParms[fx]);
  }
  if((int) strlen(pclTmp)<ilen)
  {
    strcpy(pcpResult,pclTmp);
    return RC_SUCCESS;
  }else{
    dbg(TRACE,"dsplib: GetdZParameter Buffer to short");
    return RC_FAIL;
  }
}

#ifndef STANDALONE_PARSER

/* ******************************************************************** */
/* external funtions     						*/
/* ******************************************************************** */
/*extern ccs_htons(short);                                              */
/* ******************************************************************** */
/* Following the dsplib_socket function					*/
/* ******************************************************************** */
int dsplib_socket(int *ipSock, int ipType, char *pcpServiceName,
		    int ipServicePort,int ipDoBind, char *ifname)
{
  int	             ilRc = RC_SUCCESS;
#ifdef _UNIXWARE
  size_t ilLen =0;
#else
  int	             ilLen = 0;
#endif
  struct servent     *prlSp;
  struct sockaddr_in rlName;

#if defined(_HPUX_SOURCE) || defined(_WINNT) || defined(_SNI) || defined(_SOLARIS) || defined(_UNIXWARE)
  int   ilOption=1;
#else
  short ilOption=1;	
#endif
#if defined(_WINNTX)
  struct linger lingstruct;	
  struct linger *prlLing = &lingstruct;
#else
  struct linger prlLing;	
#endif
  ilLen = sizeof(ilOption);

  errno = 0;
  *ipSock = 0;
  *ipSock = socket(AF_INET,ipType,0); /*create socket*/
  if (*ipSock <= 0 )
    {
      dbg(TRACE,"dsplib_socket: OS-ERROR socket(): <%s>!",strerror( errno));
      if (*ipSock != 0)
	{
	  /*shutdown(*ipSock,2);*/
	  close(*ipSock);
	}
      ilRc = RC_FAIL;
    }else{ /* setting all necessary socket options */
      dbg(DEBUG,"dsplib_socket: socket <%d (ifname='%s')> created. Setting socket options.",
          *ipSock, STR0(ifname));
/*
 * jhe 23 aug 04: preset bind address and select interface if specified
 */
    rlName.sin_addr.s_addr = INADDR_ANY;
    rlName.sin_family = AF_INET;
    if(ifname != NULL && strlen(ifname) != 0 && ifname[0] != '*')
    {
#ifdef remove_this_after_test
        struct ifreq ifr;
        strncpy(ifr.ifr_name, ifname, IFNAMSIZ);
        errno = 0;
        if (ioctl(*ipSock, SIOCGIFADDR, &ifr) < 0)
        {
            dbg(TRACE,"dsplib_socket: OS-ERROR ioctl(CGIFADDR): <%s>!",strerror( errno));
        }
        else
        {
/*
 * jhe 26 aug 04: get the address of the selected interface and bind socket
 * here if bind would not be performed otherwise
 */
            memcpy(&rlName.sin_addr.s_addr,&((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr,
                   sizeof(struct in_addr));
#else /* remove_this_after_test */
        struct in_addr inaddr;
        if(dsplib_gethostaddr(*ipSock, ifname, &inaddr) != 0)
		    {
            dbg(TRACE,"dsplib_socket: ERROR in dsplib_gethostaddr: <%s>!",
                strerror( errno));
		    }
        else
        {
            memcpy(&rlName.sin_addr.s_addr, &inaddr, sizeof(struct in_addr));
#endif /* remove_this_after_test */
            if (ipDoBind!=TRUE || (pcpServiceName==NULL && ipServicePort==0))
            {
              rlName.sin_port   = (u_short) 0;
#ifdef _WINNT
	      ilRc=bind(*ipSock,&rlName,sizeof(struct sockaddr_in));
#else
	      ilRc=bind(*ipSock,(struct sockaddr *)&rlName,sizeof(struct sockaddr_in));
#endif
              if(ilRc < 0)
              {
                dbg(TRACE,"dsplib_socket: OS-ERROR bind(): <%s>!",strerror( errno));
              }
            }
        }
    }

      /**************************************/
      /* setting socket option SO_BROADCAST */
      errno = 0;
      ilOption = 0;
      if ((ilRc=getsockopt(*ipSock,SOL_SOCKET,SO_BROADCAST,(void*)&ilOption,&ilLen)) == 0)
	{
	  dbg(DEBUG,"dsplib_socket: getsockopt(SO_BROADCAST) = <%d>.",ilOption);
	}
      if (ilOption == 0)
	{
	  ilOption = 1; /* enable broadcast messages for socket */
	  dbg(DEBUG,"dsplib_socket: setsockopt(SO_BROADCAST) = <%d>.",ilOption); 
	  errno = 0;
	  ilLen = sizeof(int);
	  if ((ilRc = setsockopt(*ipSock,SOL_SOCKET,SO_BROADCAST,(char *)&ilOption,ilLen)) < 0)
	    {
	      dbg(TRACE,"dsplib_socket: setsockopt(SO_BROADCAST):failed in Line  <%d>!",__LINE__);
	      /*dbg(TRACE,"dsplib_socket: OS-ERROR setsockopt(SO_BROADCAST): <%s>!",strerror(errno));*/
	      ilRc = RC_FAIL;
	    }
	}
	
#if !defined(_SOLARIS) && !defined(_LINUX)
      /**************************************/
      /* setting socket option SO_REUSEPORT */
      if (ilRc == RC_SUCCESS)
	{
	  errno = 0;
	  ilOption = 0;
	  if ((ilRc=getsockopt(*ipSock,SOL_SOCKET,SO_REUSEPORT,(void*)&ilOption,&ilLen)) == 0)
	    {
				/*dbg(DEBUG,"dsplib_socket: getsockopt(SO_REUSEPORT) = <%d>.",ilOption); */
	      if (ilOption == 0)
		{
		  ilOption = 1; /* enable port reuse of the socket */
		  /*dbg(DEBUG,"dsplib_socket: setsockopt(SO_REUSEPORT) = <%d>.",ilOption); */
		  errno = 0;
		  if ((ilRc=setsockopt(*ipSock,SOL_SOCKET,SO_REUSEPORT,(char *)&ilOption,ilLen)) <0)
		    {
		      dbg(TRACE,"dsplib_socket: OS-ERROR setsockopt(SO_REUSEPORT): <%s>!",strerror(errno));
		      ilRc = RC_FAIL;
		    }
		}
	    }
	}
#endif 
      /**************************************/
      /* setting socket option SO_KEEPALIVE */
      if (ilRc == RC_SUCCESS)
	{
	  errno = 0;
	  ilOption = 0;
	  if ((ilRc=getsockopt(*ipSock,SOL_SOCKET,SO_KEEPALIVE,(void*)&ilOption,&ilLen)) == 0)
	    {
	      dbg(DEBUG,"dsplib_socket: getsockopt(SO_KEEPALIVE) = <%d>.",ilOption); 
	      if (ilOption == 0)
		{
		  ilOption = 1; /* keeping the socket alive */
		  /*dbg(DEBUG,"dsplib_socket: setsockopt(SO_KEEPALIVE) = <%d>.",ilOption); */
		  errno = 0;
		  if ((ilRc=setsockopt(*ipSock,SOL_SOCKET,SO_KEEPALIVE,(char *)&ilOption,ilLen)) <0)
		    {

		      dbg(TRACE,"dsplib_socket: setsockopt(SO_KEEPALIVE):failed in Line  <%d>!",__LINE__);
		      /*dbg(TRACE,"dsplib_socket: OS-ERROR setsockopt(SO_KEEPALIVE): <%s>!",strerror(errno));*/
		      ilRc = RC_FAIL;
		    }
		}
	    }
	}
      /***********************************/
      /* setting socket option SO_LINGER */
      if (ilRc == RC_SUCCESS)
	{
#if defined(_WINNTX)
	  lingstruct.l_onoff = 0;
	  lingstruct.l_linger = 0;
#else
	  prlLing.l_onoff = 0;
	  prlLing.l_linger = 0;
#endif
	  ilLen = sizeof(prlLing);
	  errno = 0;
	  if ((ilRc=getsockopt(*ipSock,SOL_SOCKET,SO_LINGER,(void*)&prlLing,&ilLen)) == 0)
	    {
	      dbg(DEBUG,"dsplib_socket: getsockopt(SO_LINGER) = <%d>.",prlLing.l_onoff); 
	      errno = 0;
	      prlLing.l_onoff = 1;	
	      prlLing.l_linger = 0;	
				/*dbg(DEBUG,"dsplib_socket: setsockopt(SO_LINGER,onoff) = <%d>.",prlLing.l_onoff); */
				/*dbg(DEBUG,"dsplib_socket: setsockopt(SO_LINGER,linger) = <%d>.",prlLing.l_linger); */
		 
	      if ((ilRc = setsockopt(*ipSock,SOL_SOCKET,SO_LINGER,(char *)&prlLing,ilLen)) < 0)
		{
		  dbg(TRACE,"dsplib_socket: setsockopt(SO_LINGER):failed in Line  <%d>!",__LINE__);					
		  /*dbg(TRACE,"dsplib_socket: OS-ERROR setsockopt(SO_LINGER): <%s>!",strerror(errno));*/
		  ilRc = RC_FAIL;
#if defined(_WINNT)
		  ilRc = RC_SUCCESS;
#endif
		}
	    }
	}
    }
  /********************************/
  /* performing bind() for socket */
  if (ilRc==RC_SUCCESS && ipDoBind==TRUE &&
      (pcpServiceName!=NULL || ipServicePort!=0))
    {
      errno = 0;
      if (pcpServiceName != NULL)
	{
	  dbg(TRACE,"dsplib_socket: getservbyname(%s)",pcpServiceName);
	  prlSp = getservbyname(pcpServiceName,NULL); 
	}else{
	  dbg(TRACE,"dsplib_socket: getservbyport(%d)",ipServicePort);
	  prlSp = getservbyport(htons(ipServicePort),"tcp"); 
	}

      if (prlSp == NULL) 
	{
	  dbg(TRACE,"dsplib_socket: getservbyname():failed in Line  <%d>!",__LINE__);
	  ilRc = RC_FAIL;
	}
      else
	{
	  /*dbg(DEBUG,"dsplib_socket: got <%s>-service <%s> for bind()."
	    ,prlSp->s_proto,prlSp->s_name); */

/* jhe 26 aug 04 preset in "ifname" block
    rlName.sin_addr.s_addr = INADDR_ANY;
	  rlName.sin_family = AF_INET;
*/
	  rlName.sin_port   = (u_short)prlSp->s_port;
			
	  errno = 0;
#ifdef _WINNT
	  if ((ilRc=bind(*ipSock,&rlName,sizeof(struct sockaddr_in))) <0) 
#else
	  if ((ilRc=bind(*ipSock,(struct sockaddr *)&rlName,sizeof(struct sockaddr_in))) <0)
#endif
	      {
		dbg(TRACE,"dsplib_socket: bind():failed in Line  <%d>! with <%d>",__LINE__,ilRc);
		dbg(TRACE,"dsplib_socket: OS-ERROR bind(): <%s>!",strerror(errno));
	      }else {
		dbg(DEBUG,"dsplib_socket: bind() for port<%d> to socket<%d> returns <%d>!"
		    ,rlName.sin_port,*ipSock,ilRc);
	      }
	}
    }
  if (ilRc < 0)
    return RC_FAIL;
  else
    return ilRc;
} 

/* jhe 12 oct 04 lookup host in address by interface or logical hostname */
int dsplib_gethostaddr(int sock, char *name, struct in_addr *pia)
{
struct ifreq ifr;
struct hostent *hp;
	strncpy(ifr.ifr_name, name, IFNAMSIZ);
	if (ioctl(sock,SIOCGIFADDR, &ifr) < 0)
  {
/* notice on ioctl failure */
		dbg(DEBUG,"dsplib_gethostaddr: ioctl for <%s> failed, trying host list!",
        name);
    hp = gethostbyname(name);
    if(hp == NULL)
    {
/* notice on gethostbyname failure */
		  dbg(DEBUG,"dsplib_gethostaddr: gethostbyname for <%s> failed!", name);
      return -1;
    }
/* note: we always use the first list entry */
    memcpy(pia, hp->h_addr_list[0], sizeof(struct in_addr));
  }
  else
  {
    *pia = ((struct sockaddr_in *) &(ifr.ifr_addr))->sin_addr;
  }
  return 0;
}

  /* ******************************************************************** */
  /* The CheckValue() routine																						*/
  /* ******************************************************************** */
void dsplib_shutdown(int ipSock,int ipHow)
{
  int ilRc;
  if (ipSock != 0)
    {
      if ((ilRc = shutdown(ipSock,ipHow)) != 0)
	dbg(TRACE,"dsplib_shutdown: failed in Line  <%d>!",__LINE__);
      /*dbg(TRACE,"dsplib_shutdown: OS-ERROR shutdown(%d,%d): <%s>!"
	,ipSock,ipHow,strerror(errno));*/
    }
}
  /* ******************************************************************** */
  /* The CheckValue() routine																						*/
  /* ******************************************************************** */
void dsplib_close(int ipSock)
{
  int ilRc;

/*dbg(TRACE,"dsplib_close: ipSock = (%d)",ipSock);*/
  if (ipSock != 0)
  {
/*
     ilRc = shutdown(ipSock,2);
dbg(TRACE,"dsplib_close: RC from shutdown = (%d)",ilRc);
*/
     if ((ilRc = close(ipSock)) != 0)
     {
         dbg(TRACE,"dsplib_close: OS-ERROR close(%d): <%s>!",ipSock,strerror(errno));
     }
  }
}

  /* ******************************************************************** */
  /* The CheckValue() routine																						*/
  /* ******************************************************************** */
int CheckValue(char *pcpEntry,char *pcpValue,int ipType)
{
  int ilRc = RC_SUCCESS;

  switch(ipType)
    {
    case CFG_NUM:
      while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
	{
	  if (isdigit(*pcpValue)==0)
	    {
	      /*dbg(TRACE,"CheckValue : NOT A NUMBER! <%s>!"
		,pcpEntry);*/ 
	      ilRc = RC_FAIL;
	    }
	  pcpValue++;
	}
      break;
    case CFG_ALPHA:
      while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
	{
	  if (isalpha(*pcpValue)==0)
	    {
	      /*dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
		,*pcpValue,pcpEntry); */
	      ilRc = RC_FAIL;
	    }
	  pcpValue++;
	} 
      break;
    case CFG_ALPHANUM:
      while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
	{
	  if (isalnum(*pcpValue)==0)
	    {
	      /*dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
		,*pcpValue,pcpEntry);*/ 
	      ilRc = RC_FAIL;
	    }
	  pcpValue++;
	}
      break;
    case CFG_PRINT:
      while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
	{
	  if (isprint(*pcpValue)==0)
	    {
	      /*dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
		,*pcpValue,pcpEntry); */
	      ilRc = RC_FAIL;
	    }
	  pcpValue++;
	}
      break;
    default:
      break;
    }
  return ilRc;
}
/***************************************************************/
/*This function built up a datafilter for sql whereclause and  */   
/*put comma separated items in quotas                          */
/* input: pcpIn = raw data comma separated like "A,B,C,D,E"    */
/*        pcpField = Fieldname like "FTYP"                     */
/*        iplen = Buffsize from pcpOut                         */
/* output:pcpOut = "AND ('Fieldname' in ('A','B','C',...))"    */
/***************************************************************/
int GetQuotasForWhereClause(char* pcpIn,char* pcpOut,char* pcpField,int iplen,char* pcpSign)
{
  char pclTmpData[iMAXIMUM];
  char *pclS;
  char *pclD;


  memset(pclTmpData,0x00,iMAXIMUM);  
  memset(pcpOut,0x00,iplen);
  pclS = pcpIn;
  pclD = pclTmpData;
  *pclD++ = 0x27;
  do{
      if(*pclS == 0x2c) 
        { 
      *pclD++ = 0x27;
      *pclD++ = *pclS++;
      *pclD++ = 0x27;
        }else{
      *pclD++ = *pclS++;
        }
    }while((*pclS) != '\0');
  *pclD++ = 0x27;
  *pclD = '\0';

  strcpy(pcpOut," AND (");

  strcat(pcpOut,pcpField);

  if(pcpSign==NULL)
    { 
      strcat(pcpOut," IN (");

    }else{
      strcat(pcpOut," NOT IN (");
    }
  strcat(pcpOut,pclTmpData);
  strcat(pcpOut,")) ");
  return 0;
}

void SetPipeToItem(char *pcpData,int ipHexChar)
{
  char pclTmpData[M_BUFF];
  char *pclS;
  char *pclD;
  
  pclS = pcpData;
  pclD = pclTmpData;       
  do{
      if(*pclS == ipHexChar)
        {
      *pclD = *pclS++; 
      *pclD++ = 0x7b;
      *pclD++ = 0x49;
      *pclD++ = 0x54;
      *pclD++ = 0x45;
      *pclD++ = 0x4d;
      *pclD++ = 0x7d;
        }else{
      *pclD++ = *pclS++;
        }
    }while((*pclS) != '\0');
  
  *pclD = '\0';

  strcpy(pcpData,pclTmpData);
  return;
  

} 
void SetPipeToComma(char *pcpData,int ipHexChar)
{
  char pclTmpData[M_BUFF];
  char *pclS;
  char *pclD;
  
  pclS = pcpData;
  pclD = pclTmpData;       
  do{
      if(*pclS == ipHexChar)
        {
      *pclD = *pclS++; 
      *pclD++ = 0x2c;
        }else{
      *pclD++ = *pclS++;
        }
    }while((*pclS) != '\0');
  
  *pclD = '\0';

  strcpy(pcpData,pclTmpData);
  return;
} 

int MapToArabic(char* pcpResult, int ipLen, char *pcpCodePage)
{
  int ilRc = RC_SUCCESS;
  int ilCnt= 0;
  int ilCnt2=0;
  int ilOffset=0;
  char* pclO=NULL;
  char* pclE=NULL;
  char pclSourceBuf[M_BUFF];
  char pclDestBuf[M_BUFF];
  char *pclS;
  char *pclD;
  char pcldZ[20];
  memset(pclSourceBuf,0x00,M_BUFF);
  memset(pclDestBuf,0x00,M_BUFF);
  strcpy(pclSourceBuf,pcpResult);
   pclO=NULL;
  if((pclO=strstr(pcpResult,"{BG"))!=NULL)
    {
      pclE=strchr(pcpResult,'}');
      ilOffset=pclE-pclO;
      strncpy(pcldZ,pcpResult,ilOffset+1);
      pcldZ[ilOffset+1]='\0';
      strcpy(pclSourceBuf,++pclE);
      pclSourceBuf[strlen(pclSourceBuf)-5]='\0';
     /*   dbg(TRACE,"pcdZ <%s>,pclSourceBuf <%s>,ilOffset %d",pcldZ,pclSourceBuf,ilOffset); */
    }

  pclS=pclSourceBuf;
  pclD=pclDestBuf;
  if(*pcpResult=='\0')
    return RC_SUCCESS;



  do{
    /* dbg(TRACE,"pclS = <%c> pclS+1 = <%c>",*pclS,*(pclS+1));*/
    if(*pclS<0x2f||*pclS>0x39)
      {
	/*aksdgasglfb...*/
	if(ilCnt2==0)
	  {
	    strcpy(pclD,"{RTL}");
	    pclD+=5;
	  }else{
	    *pclD++ = *pclS++;
	  }

	ilCnt2++;
      }else{
	if(ilCnt2 != 0) 
	  {     
	    ilCnt2=0;
	    strcpy(pclD,"{rtl}"); 
	    pclD+=5;
	  } else{
	    *pclD++ = *pclS++;    
	  }
      }
  }while((*pclS) != '\0');

  if(*(pclS-1)<0x2f||*(pclS)-1>0x39)
    {
      strcpy(pclD,"{rtl}");
    }else{
      *pclD = '\0';
    }
  memset(pclSourceBuf,0x00,M_BUFF);
  strcpy(pclSourceBuf,pclDestBuf);
  memset(pclDestBuf,0x00,M_BUFF);
  pclS=pclSourceBuf;
  pclD=pclDestBuf;
  do{
    if(*pclS >= 0x2f && *pclS <= 0x39 )
      {
	/*0123456789*/
	if(ilCnt==0)
	  {
	    /*strcpy(pclD,"{CP 32000}");*/
	    /*pclD+=10;*/
            sprintf(pclD,"{CP %s}",pcpCodePage);
            pclD += strlen(pcpCodePage) + 5;
	  }else{
	    *pclD++ = *pclS++;
	  }
	ilCnt++;
      }else{
	if(ilCnt!= 0) 
	  {     
	    ilCnt=0;
	    strcpy(pclD,"{cp}"); 
	    pclD+=4;
	  } else{
	    /*rest*/
	    *pclD++ = *pclS++; 
	  }
      }
  }while((*pclS) != '\0');
  if(*(pclS-1) >= 0x2f && *(pclS-1) <= 0x39 )
    {
      strcpy(pclD,"{cp}");
    }else{ 
      *pclD = '\0';
    }
  if(ipLen > strlen(pclDestBuf))
    {
      if(pclO!=NULL)
	{
	  strcpy(pcpResult,pcldZ);
	  strcat(pcpResult,pclDestBuf);
	  strcat(pcpResult,"{bg}");
	}else{
	  strcpy(pcpResult,pclDestBuf);
	}
    }else{
      dbg(TRACE,"MapToArabic %05d string to long",__LINE__);
    }
  return ilRc;

}

int MapToArabicN(char* pcpResult, int ipLen, char *pcpCodePage)
{
  int ilRc = RC_SUCCESS;
  int ilCnt= 0;
  int ilCnt2=0;
  char pclSourceBuf[M_BUFF];
  char pclDestBuf[M_BUFF];
  char *pclS;
  char *pclD;
  memset(pclSourceBuf,0x00,M_BUFF);
  memset(pclDestBuf,0x00,M_BUFF);
  strcpy(pclSourceBuf,pcpResult);
  pclS=pclSourceBuf;
  pclD=pclDestBuf;
	if(*pcpResult=='\0')
		return RC_SUCCESS;
  do{
    if(*pclS >= 0x2f && *pclS <= 0x39 )
      {
    /*0123456789*/
    if(ilCnt==0)
      {
        /*strcpy(pclD,"{CP 32000}");*/
        /*pclD+=10;*/
        sprintf(pclD,"{CP %s}",pcpCodePage);
        pclD += strlen(pcpCodePage) + 5;
      }else{
        *pclD++ = *pclS++;
      }
    ilCnt++;
      }else{
      if(ilCnt!= 0) 
      {     
        ilCnt=0;
          strcpy(pclD,"{cp}"); 
          pclD+=4;
      } else{
        /*rest*/
        *pclD++ = *pclS++; 
      }
      }
  }while((*pclS) != '\0');
  if(*(pclS-1) >= 0x2f && *(pclS-1) <= 0x39 )
    {
      strcpy(pclD,"{cp}");
    }else{ 
      *pclD = '\0';
    }
  if(ipLen > strlen(pclDestBuf))
    {
      strcpy(pcpResult,pclDestBuf);
    }else{
      dbg(TRACE,"MapToArabic %05d string to long",__LINE__);
    }
  return ilRc;
}

#endif /* notdef STANDALONE_PARSER */

