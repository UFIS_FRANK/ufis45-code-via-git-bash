#ifndef _DEF_mks_version_dsphdl_h
  #define _DEF_mks_version_dsphdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_dsphdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/dsphdl.h 1.20 2009/07/22 08:54:39CEST akl Exp otr (Oct 2010) $";
#endif /* _DEF_mks_version */
/************************************************************************/
/* The Master include file.                                             */
/*                                                                      */
/*  Program       :                                                     */
/*                                                                      */
/*  Revision date :                                                     */
/*                                                                      */
/*  Author        :                                                     */
/*                                                                      */
/* 20041206 JIM: sizes of XS_BUFF to XXL_BUFF reset to previous values  */
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/

#ifndef __DSPHDL_H
#define __DSPHDL_H

/* source-code-control-system version string */
#include "dsplib.h"
/* ---------------------------------------------------------------------- */
/* following entires by RBI */
/* ---------------------------------------------------------------------- */
#define iARRAY_COUNT        1000
#define iINDEX_COUNT        2
/* ---------------------------------------------------------------------- */
/* above entires by RBI */
/* ---------------------------------------------------------------------- */

/***************/
/* ALL DEFINES */
/***************/
/* defines for array allocation sizes */
#define DEV_MAXFIELDS 100
#define DSP_MAXFIELDS 150
#define PAG_MAXFIELDS 100
/* defines for buffer sizes */
#define XS_BUFF  128
#define S_BUFF   512
#define M_BUFF   1024
#define L_BUFF   2048
#define XL_BUFF  4096
#define XXL_BUFF 8192  
#define RES_BUF_SIZE 512000
/* defines for connection establisment */
#define PASSIV 1300
#define ACTIVE 1301

/* defines for recv() or send() timeouts */
#define TCP_ACCEPT_TIMEOUT 1

/* defines for device status */
#define UP     1310
#define DOWN   1311
#define REPLY  1312
#define CLOSED 0
#define OPEND  1
#define RC_FIRSTBAG 1313 
#define REQUEST_ID      "dZ"
#define HEX_RESPONSE_ID  0x4349
#define HEX_TIME_SYNC_ID 0x54696D65
/*********************************/
/* dZine communication structures*/
/*********************************/
typedef struct
{
    unsigned char sec;
    unsigned char min;
    unsigned char hour;
    unsigned char day;
    unsigned char month;
    unsigned char year;
}IDS_TIME;
typedef struct 
{
    unsigned short id; /* has to be 0x645A (dZ) for dZine */
    unsigned short seqno;
    unsigned long  clientaddr;
    IDS_TIME       currenttime; 
    char           reserved[16];
}IDS_UDP_REQUEST;

typedef struct
{
    unsigned short *id; /* has to be 0x4349 ("CI") for answer to dZine */
    unsigned long  *clientaddr;
    unsigned long  *serveraddr;
    unsigned short *tcpport;
    unsigned short *changenetworkpart;
    char           *reserved;
}IDS_UDP_RESPONSE;
#define IDS_UDP_RESPONSE_SIZE 30

typedef struct 
{
    unsigned long id; /* has to be 0x54696D65 */
    IDS_TIME      currenttime; 
    char          reserved[16];
}IDS_TIME_SYNC;

#define IDS_TIME_SYNC_SIZE 26


/* config-file entries */
typedef struct
{
  char *dsp_group;    
  char *BroadCastIntervall;
  char *dummy;
}CFG;

typedef struct
{
  char a_dadr[16]; /* IP-Address in ascii-format */
  int  n_dadr;     /* IP-Address as integer*/
  char socket[11]; /* connected socket */
  char state;       /* state of the device */
  char dpid[13];   /* display ID of dsptab */
}DEV_INFO;

#define DEV_DB_KEY "DADR"
#define DEV_DB_UP "DLCT,STAT,LSTU"
#define DEV_DB_DOWN "DLDC,STAT,LSTU"
#define DEV_DB_REPLY "LSTU,DLRD"
#define DEV_INITIAL    1
#define DEV_EXTENT    DEV_INITIAL
#define DEV_DB_DEGAUSS "DLDT"
#define BELT_MID_FIELDS "AURN"
#if defined _ATH
#define BELT_MID_FLIGHTS 3
#endif
#if defined(_WAW) || defined(_STD) || defined(_DXB)
#define BELT_MID_FLIGHTS 4
#endif
#define PAG_DB_FIELDS "PAGI,PCFN"
/*Fields from AFTTAB*/
/***********************1****2****3****4****5****6****7****8****9****10***11***12***13***14***15***16***17***18***19***20***21***22***23***24***25***26***27**/                  
#define ARRIVALFIELDS "URNO,FLNO,FLTN,FLNS,STOA,ORG3,VIA3,ETOA,ONBL,TTYP,REMP,BLT1,ACT3,JFNO,ETAI,TMB1,TIFA,TISA,B1BA,B1EA,LAND,EXT1,REGN,PSTA,TMOA,FTYP,TMAU"


/*Fields from AFTTAB*/
#if defined _ATH
/*******************1****2****3****4****5****6****7****8****9****10***11***12***13***14***15***16***17***18***19***20***21***22****23***24***25***26***27***28**/                 
#define BELTFIELDS "URNO,FLNO,FLTN,FLNS,STOA,ORG3,VIA3,ETOA,ONBL,TTYP,REMP,BLT1,BLT2,ACT3,JFNO,ETAI,TMB1,TIFA,TISA,B1BA,B1EA,B2BA,B2EA,LAND,EXT1,B1BS,B1ES,VIA4"
#endif
#if defined (_STD) || defined(_WAW) || defined (_DXB) 
#define BELTFIELDS "URNO,FLNO,STOA,ORG3,VIA3,ETOA,ONBL,TIFA,RNAM,RTYP,DSEQ,CREC,ACTI,AOTI"
#endif
typedef struct _FieldList
{
  char pcTabName[M_BUFF];
  char pcFields[L_BUFF];
  char pcUrno[XS_BUFF];
  char pcRecordset[L_BUFF];
  int  iDisplayFlag;
}FieldList;
/*Fields from AFTTAB*/
/*************************1****2****3****4****5****6****7****8****9****10***11***12***13***14***15***16***17***18***19***20***21***22***23***24***25***26***27***28***29*/        
#define DEPARTUREFIELDS "URNO,FLNO,FLTN,FLNS,STOD,DES3,VIA3,ETOD,OFBL,TTYP,REMP,GTD1,ACT3,JFNO,ETDI,TIFD,TISD,CKIF,CKIT,WRO1,GD1X,GD1Y,GDT2,GD2X,GD2Y,AIRB,PSTD,REGN,FTYP"

#define FDDFIELDS "FTYP,INDO,DSEQ,DACO,AURN,RMTI,DNAT,FLNO,TIFF,STOF,ETOF,ONBL,OFBL,LAND,AIRB,BLT1,BLT2,REMP,APC3,WRO1,VIA3,GTD1,GTD2,CKIF,CKIT,LSTU,CDAT,USEU,USEC,HOPO,URNO"
#ifdef _DXB
#define FDVFIELDS "FTYP,INDO,DSEQ,DACO,ACT3,REGN,ETAI,ETDI,TMOA,EXT1,LAND,ONBL,OFBL,PSTA,PSTD,TTYP,AURN,RMTI,DNAT,FLNO,TIFF,STOF,ETOF,ONBL,OFBL,LAND,AIRB,BLT1,BLT2,REMP,APC3,WRO1,VIA3,VIAN,VIAL,GTD1,GTD2,CKIF,CKIT,ALC2,ALC3,CSGN,URNO"
#else
#define FDVFIELDS "FTYP,INDO,DSEQ,DACO,ACT3,REGN,ETAI,ETDI,TMOA,EXT1,LAND,ONBL,OFBL,PSTA,PSTD,TTYP,AURN,RMTI,DNAT,FLNO,TIFF,STOF,ETOF,ONBL,OFBL,LAND,AIRB,BLT1,BLT2,REMP,APC3,WRO1,VIA3,GTD1,GTD2,CKIF,CKIT,ALC2,ALC3,URNO"
#endif
/*Fields from CCATAB*/
/************************1****2****3****4****5****6****7****8****9***10*/
#define COUNTERFIELDS "URNO,CKIC,FLNU,FLNO,CKBS,CKES,CTYP,CKIF,LSTU,DISP"


/*Fields from FLDTAB*/
/********************1****2****3****4****5****6****7****8****9****10***11***12***13***14***15**16***17***18**/
#define FLDFIELDS "URNO,RNAM,RTYP,AURN,CREC,STAT,RTAB,RURN,SCTI,SOTI,DOTI,DCTI,AOTI,ABTI,AFTI,ACTI,DSEQ,LSTU"
/*  URNO -> unique record number */

/* RNAM -> RESSOURCE NAME*/
/* RTYP -> RESSOURCE TYPE*/
/* AURN -> AFT URNO      */
/* CREC -> COUNTER REMARK CODE */
/* STAT -> STATUS OF RECORD    */
/* RTAB -> RELATED TABLE */
/* RURN -> URNO OF RELATED TABLE*/
/* SOTI -> SCHEDULED OPEN TIME*/
/* SCTI -> SCHEDULED CLOSING TIME*/
/* DOTI -> DEVICE OPEN TIME*/
/* DCTI -> DEVICE CLOSING TIME*/
/* AOTI -> ACTUAL OPEN TIME*/
/* ABTI -> ACTUAL BOARDING TIME*/
/* AFTI -> ACTUAL FINAL CALL TIME*/
/* ACTI -> ACTUAL CLOSING TIME*/
/* DSEQ -> DISPLAY SEQUENCE*/

/*Fields for BaggageSummary*/
/*FROM FLVTAB*/
#if defined (_DXB) || defined (_TLL)
#define BAGSUMFIELDS "URNO,STOA,TIFA,FLNO,ORG3,VIA3,VIA4,VIAN,VIAL,JFNO,JCNT,BLT1,RNAM,B1BA,B1EA,CREC"
#endif
#if defined (_STD) || defined(_WAW)
#define BAGSUMFIELDS "URNO,STOA,TIFA,FLNO,ORG3,VIA3,VIA4,JFNO,JCNT,BLT1,RNAM,B1BA,B1EA,CREC"
#endif
#if defined (_ATH)
#define BAGSUMFIELDS "URNO,STOA,TIFA,FLNO,ORG3,VIA3,VIA4,BLT1,RNAM"
#endif

#define CHUTEFIELDS "URNO,STOD,TIFD,STAT,FLNO,RNAM,PSTD,DSEQ"

#if defined (_ATH)
/*Fields from FLVTAB(view)*/
/********************1****2****3****4****5****6****7****8****9****10***11***12***13***14***15**16***17***18**|*****AFTTABFIELDS*************************************************|*/
#define FLVFIELDS "URNO,RNAM,RTYP,AURN,JCNT,DSEQ,FLNO,CREC,STAT,SCTI,SOTI,DOTI,DCTI,AOTI,ABTI,AFTI,ACTI,LSTU,GTD1,GTD2,GD1Y,GD2Y,CKIF,CKIT,REMP,JFNO,STOD,STOA,TIFD,TIFA,DES3,DES4,ORG3,ORG4,VIA3,VIA4,FXT1,FXT2,FLG1,CTY1,REMP,ETOD"
#endif
#if defined(_STD) ||defined (_WAW)
/*Fields from FLVTAB(view)*/
/********************1****2****3****4****5****6****7****8****9****10***11***12***13***14***15**16***17***18**|*****AFTTABFIELDS*************************************************|*/
#define FLVFIELDS "URNO,RNAM,RTYP,AURN,JCNT,DSEQ,FLNO,B1BA,B1EA,CREC,ETOD,RTAB,RURN,SCTI,SOTI,DOTI,DCTI,AOTI,ABTI,AFTI,ACTI,GTD1,GTD2,GD1Y,GD2Y,CKIF,CKIT,REMP,DISP,JFNO,STOD,STOA,TIFD,TIFA,DES3,DES4,ORG3,ORG4,VIA3,VIA4,FLG1,FLG2,CTY1,CTYP"
#endif
#ifdef _DXB
/*Fields from FLVTAB(view)*/
/********************1****2****3****4****5****6****7****8****9****10***11***12***13***14***15**16***17***18**|*****AFTTABFIELDS*************************************************|*/
#define FLVFIELDS "URNO,RNAM,RTYP,AURN,JCNT,DSEQ,FLNO,B1BA,B1EA,CREC,ETOD,RTAB,RURN,SCTI,SOTI,DOTI,DCTI,AOTI,ABTI,AFTI,ACTI,GTD1,GTD2,GD1Y,GD2Y,CKIF,CKIT,REMP,DISP,JFNO,STOD,STOA,TIFD,TIFA,DES3,DES4,ORG3,ORG4,VIA3,VIA4,VIAN,VIAL,FLG1,FLG2,CTY1,CTYP"
#endif
#ifdef _ATH
#define FLV1FIELDS "flv1.URNO,flv1.RNAM,flv1.RTYP,flv1.AURN,flv1.JCNT,flv1.DSEQ,flv1.FLNO,flv1.CREC,flv1.STAT,flv1.SCTI,flv1.SOTI,flv1.DOTI,flv1.DCTI,flv1.AOTI,flv1.ABTI,flv1.AFTI,flv1.ACTI,flv1.LSTU,flv1.GTD1,flv1.GTD2,flv1.GD1Y,flv1.GD2Y,flv1.CKIF,flv1.CKIT,flv1.REMP,flv1.JFNO,flv1.STOD,flv1.STOA,flv1.TIFD,flv1.TIFA,flv1.DES3,flv1.DES4,flv1.ORG3,flv1.ORG4,flv1.VIA3,flv1.VIA4,flv2.FXT1,flv2.FXT2,flv1.FLG1,flv1.CTY1,flv1.REMP,flv1.ETOD"
#endif

/*FIELDS FROM CCATAB*/
#define CCAFIELDS "URNO,CKIC,CTYP,CKEA,CKBA,FLNU"


/*Fields from FLGTAB*/
#define AIRLINELOGOFIELDS "URNO,LGSN,LGFN"

typedef struct _DepartureFields
{
  int iNoOfRows;
  FieldList *prField;
}Fields;

typedef struct
{
  char pclFGColor[8];
  char pclBGColor[8];
} PageColors;

typedef struct
{
  int iCarouselStatus;
  int iNoOfPageRows;
  char pcUrnoList[100*11];
} PageContent;
 
/*display -> pages*/
typedef struct _PObjStruct
{
  char *pcPageId;

  char pcCarouselTime[8];        /*PCTI*/
  char pcNumberOfFlights[8];     /*PNOF*/
  char pcDisplayType[8];         /*PDPT*/
  char pcDisplayTypeInternal[8]; /*PDPI*/
  char pcTimeFrameBegin[8];      /*PTFB*/
  char pcTimeFrameEnd[8];        /*PTFE*/
  char pcTimeParamDel1[8];       /*PTD1*/
  char pcTimeParamDel2[8];       /*PTD2*/
  char pcTimeParamCancelled[8];  /*PTDC*/
  char pcReferenceField[8];      /*PRFN*/
  char pcDisplaySeq[8];          /*PDSS*/
  char pcFlightNature[8];        /*PFNT*/
  char pcColTab[8];              /*COTB*/
  /*information from pagefile*/

  int iTableColumns;
  int iTableVisibleRows;

  int iTableRowsMax;
  int iTableRowsNow;
  int iNoOfObjPerRec;
  int iNumberOfFlights1;
  int iNumberOfFlights2;
  int iNoOfObjPerRec1;
  int iNoOfObjPerRec2;
  char pcTableFirstId[L_BUFF];
 
  char pcFieldsFromSelection[L_BUFF];
  FieldList *prPageTable;

  char pcTimeParameterFields[L_BUFF];
  char pcTimeParameterData[L_BUFF]; 

  UDBXPage *prPagefile;

/*    int iNoOfObj; */
  char pcTableObjNoList[L_BUFF];
  PageColors prPageColors[50];
  char pcTableObjectId[L_BUFF];
}PObjStruct;

typedef struct _PMStruct
{
  
  int iNoOfPages;
  PObjStruct *prPages;

}PMStruct;
/*end display -> pages*/
/*devices -> pages*/
typedef struct _DevPag
{
  char pcCarouselType[8];
  char pcIP[16];
  int  iSocket;
  time_t iKeepalive;
  int  iInitialize;
  int  *piDefaultPage;
  int  iNoOfDefPags;
  int  iAlarmPage;
  int  iConfigPage;
  int  iDefaultFlag;
  int  iNoOfLocations;
  int  iAlarmFlag;
  int  iLostConnections;
  int  iMaxNumberOfFlights;
  int  iDisplayState;
  int iActivePage;
  int iOldPage;
  char pcLocationLogo[XS_BUFF];
  int  iLogo;
  int  iGateState;
  int iCarouselState;
  short sPort;  
  char pcDeviceState[8];
  char pcDevUrno[20];
  char pcRowInCluster[16];
  char pcDegaussIntervall[8];
  char pcLastDegaussTime[16];
  char pcBrightness[16];
  char pcAlarmPageNo[8];
  char pcDefaultPage[XS_BUFF];
  char pcInitCfgFile[16];
  char pcNumberOfPages[8];
  char pcFirstPageNumber[8];
  char pcFilterField[16];
  char pcFilterContents[256];
  char pcTerminal[16];
  char pcAirline[16];
  int  iInvertAirline;
  int  iCodeShares;
  char pcDeviceArea[32];
  char pcDisplayId[16];
  char pcUrnoList[M_BUFF];/*01.12.00 **->[n]*/ 
  PageContent prPageContent[50];
  int iNoOfRec;
  FieldList *prRecord;
 
  int iNoOfPageInCarousel;
  int  *piPagNo;/*->Number in PageStruct*/
  char **pcPageObjectId;/*->Number in DSPTAB (PN01 .... PN50)*/
 
  char **pcPageId;
  char **pcDisplayType;
      
}DevPag;

typedef struct _DeviceStruct
{
  int iNoOfDevices;
  DevPag *prDevPag;
}DeviceStruct;
/*end devices -> pages*/
/*define for remark triggering*/
#define ARRIVED "ARR"
#define DELAYED "DLY"
#define CANCELLED "CNL"
#define LANDED "LND"
#define EXPECTED "EXP"
#define DIVERTED "DIV"
#define DEPARTED "DEP"
#define ARRIVALTIME "ETOA"
#define DEPARTURETIME "STOA"

#define BELT "B"
#define GATE "G"
#define DEPARTURE "D"
#define ARRIVAL "A"
#define COUNTER "C"
#define MIXED "M"
#define CHUTE "H"
#define STAFF "S"
#define PUBLIC "P"
#define BAGGAGESUMMARY "U"
#define FDDTYPE "F"
/*  #define COMMONCOUNTER "" */
 
/*this fields should be selectet*/



#define AIRPORTSQL "SELECT %s FROM APT%s WHERE APC3='%s'"

#define REMARKSQL  "SELECT %s FROM FID%s WHERE CODE='%s'"
/*some sql statements*/
/*standard select for belt info*/
/*usage: two parameters (TimeFrame)  must add for sql statement char* 1->start_minute, char* 2->end_minute*/                     
/*  #define BELTWHERE " WHERE (((TIFA BETWEEN '%s' AND '%s') AND (ADID = 'A')AND (b1ea > '%s')AND  (onbl <> ' ') AND (blt1 <> ' ') ) OR ((b1ea = ' ') and (onbl > '%s') and (blt1 <= '11'))) ORDER BY BLT1,ONBL,STOA" */

/*Beltwhere1*/
/*leider nicht in Athen */
#if defined(_STD) || defined(_WAW) || defined(_DXB)
#define BELTWHERE "select %s from FLVTAB WHERE DESQ > 0 AND RTYP ='BLT1' ORDER BY DSEQ "
#endif


/*Beltwhere2*/
/* Athen Version*/
#if defined(_ATH)
#define BELTWHERE "select %s from afttab WHERE ((TIFA BETWEEN '%s' AND '%s') AND (ADID = 'A')  AND(((blt1 != ' ') and (b1ba !=' ') and b1ba <= '%s'and b1ba > '%s' and ((b1ea >= '%s') or (b1ea = ' '))) or((blt2 != ' ') and (b2ba !=' ') and b2ba <= '%s'and b2ba > '%s' and ((b2ea >= '%s') or (b2ea = ' '))))) order  by tifa,onbl,stoa"
#endif
#if 0
/*Beltwhere3*/
#define BELTWHERE "select %s from afttab WHERE ((TIFA BETWEEN '%s' AND '%s') AND (ADID = 'A') AND(((blt1 != ' ') and (b1ba !=' ') and b1ba <= '%s'and b1ba > '%s' and ((b1ea >= '%s') or (b1ea = ' '))))) union  select %s from afttab WHERE ((TIFA BETWEEN '%s' AND '%s') AND (ADID = 'A') AND(((blt2 != ' ') and (b2ba !=' ') and b2ba <= '%s'and b2ba > '%s' and ((b2ea >= '%s') or (b2ea = ' '))))) order  by tifa,onbl,stoa"
#endif 

/*BAGGAGE SUMMARY WHERECLAUSE*/
#if defined (_DXB)
#define BAGSUMWHERE "where rtyp = 'BELT' and dseq > 0 order by tifa,stoa"
#endif
#if defined (_STD) || defined(_WAW)||defined (_ATH)
#define BAGSUMWHERE "where rtyp = '%s' and dseq > 0 order by tifa,stoa"
#endif
#if defined (_ATH)
#define FDDWHERE " WHERE (((TIFF BETWEEN '%s' AND '%s' )AND ADID = '%s') AND (%s = ' ' OR %s > TO_CHAR(SYSDATE %s,'YYYYMMDDHH24MISS'))AND (REMP!='CNL' or TIFF > TO_CHAR(SYSDATE %s,'YYYYMMDDHH24MISS'))) ORDER BY %s,FLNO,INDO desc,FTYP desc,DSEQ"
#endif
#if defined (_DXB) || defined (_TLL)||defined (_WAW) || defined(_STD)
#define FDDWHERE " WHERE (((TIFF BETWEEN '%s' AND '%s' )AND ADID = '%s')AND DNAT='%s' AND (%s = ' ' OR %s > TO_CHAR(SYSDATE %s,'YYYYMMDDHH24MISS'))AND (REMP!='CXX' or TIFF > TO_CHAR(SYSDATE %s,'YYYYMMDDHH24MISS'))) ORDER BY %s,FLNO,INDO desc,APC3,FTYP desc,DSEQ"
#endif
#define FDDWHERE_NEW " WHERE (((TIFF BETWEEN '%s' AND '%s' )AND ADID = '%s')AND DNAT='%s' AND (%s = ' ' OR %s > '%s')AND (REMP!='CXX' or TIFF > '%s')) ORDER BY %s,FLNO,INDO desc,APC3,FTYP desc,DSEQ"
#if defined (_ATH)
#define FDVWHERE " WHERE (((TIFF BETWEEN '%s' AND '%s' )AND ADID = '%s' AND DNAT ='%s') AND (%s = ' ' OR %s > TO_CHAR(SYSDATE %s,'YYYYMMDDHH24MISS'))AND (REMP!='CNL' or TIFF > TO_CHAR(SYSDATE %s,'YYYYMMDDHH24MISS'))) ORDER BY %s,FLNO,INDO desc,APC3,FTYP desc,DSEQ"
#else
#define FDVWHERE " WHERE (((TIFF BETWEEN '%s' AND '%s' )AND ADID = '%s' AND DNAT = '%s') AND (%s = ' ' OR %s > TO_CHAR(SYSDATE %s,'YYYYMMDDHH24MISS')) AND (REMP!='CXX' or TIFF > TO_CHAR(SYSDATE %s,'YYYYMMDDHH24MISS'))) ORDER BY %s,FLNO,INDO desc,APC3,FTYP desc,DSEQ"
#endif
#define FDVWHERE_ST_P " WHERE (((TIFF BETWEEN '%s' AND '%s' ) AND ADID = '%s' AND DNAT = '%s') AND (%s = ' ' OR %s > TO_CHAR(SYSDATE %s,'YYYYMMDDHH24MISS')) AND (REMP!='CXX' or TIFF > TO_CHAR(SYSDATE %s,'YYYYMMDDHH24MISS'))) ORDER BY %s,FLNO,INDO desc,APC3,FTYP desc,DSEQ"
#define FDVWHERE_ST_S " WHERE (((TIFF BETWEEN '%s' AND '%s' ) AND ADID = '%s') AND (%s = ' ' OR %s > TO_CHAR(SYSDATE %s,'YYYYMMDDHH24MISS'))AND (REMP!='CXX' or TIFF > TO_CHAR(SYSDATE %s,'YYYYMMDDHH24MISS'))) ORDER BY %s,FLNO,INDO desc,APC3,FTYP desc,DSEQ"
#define CHUTEWHERE "where rtyp = 'CHUTE' and dseq > 0 order by tifd,stod"
#endif /* __DSPHDL_H */
