#ifndef _DEF_mks_version_fldhdl_h
  #define _DEF_mks_version_fldhdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fldhdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/fldhdl.h 1.2 2004/07/27 10:47:45CEST jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       : fldhdl.h                                              */
/*                                                                        */
/*  Revision date : 07.02.2001                                            */
/*                                                                        */
/*  Author    	  : Hans-J�rgen Ebert                                     */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */

typedef struct     /*Counter Display Information from ConfigFile*/
{
  char Section[10];  /*here the name of the section in the configfile will be stored*/
  char COUNTERS[10]; /*common or dedicated conter for main or codeshare-fl*/
  char LOGO[10];     /*information which logo will be displayed (main or codeshare)*/
  char FLINUM[10];   /*information which flight-number will be displayed (main or codeshare)*/
  char PUST[10];    /*public or staff - display*/
}CNTDISP;

typedef struct     /*Domestic and Mixed-Flight information from ConfigFile*/
{
  char Section[10]; /*here the name of the section in the configfile will be stored*/
  char DOMVIA[10];  /*number of domestic vias to be displayed*/
  char ITIP[10];    /*in the International Terminal International Part of the fligh should be displayed*/
  char ITDP[10];    /*in the International Terminal Domestic Part of the flight should be displayed*/
  char DTIP[10];    /*in the Domestic Terminal International Part of the flight should be displayed*/
  char DTDP[10];    /*in the Domestic Terminal Domestic Part of the flight should be displayed*/
}DOMIX;

typedef struct		/*Code-share airlines for common checkin counters */
{
  char MainAirline[4];	/*Main airline code */
  char CodeShare[20][4]; /*Code-share airline codes */
  int  ilCntCS;		/*Number of code-shares */
}COMCODESHARE;






