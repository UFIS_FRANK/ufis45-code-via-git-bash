rem Copies all generated files from the SCM-Structue into the Install-Shield directory.


rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:

rem Copy ini- and config-files
copy c:\ufis_bin\*.ini %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.cfg %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\dxb_build\ufis_client_appl\system\*.*

rem Copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\dxb_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\dxb_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\dxb_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\dxb_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\dxb_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fidas.exe %drive%\dxb_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\dxb_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\importflights.exe %drive%\dxb_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\dxb_build\ufis_client_appl\applications\*.*

REM Copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\dxb_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\dxb_build\ufis_client_appl\system\*.*

rem copy files for .NET Applications
copy c:\ufis_bin\release\*lib.dll %drive%\dxb_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\ufis.utils.dll %drive%\dxb_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\UserControls.dll %drive%\dxb_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\ZedGraph.dll %drive%\dxb_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\status_manager.exe %drive%\dxb_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\checklistprocessing.exe %drive%\dxb_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\data_changes.exe %drive%\dxb_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fips_reports.exe %drive%\dxb_build\ufis_client_appl\applications\*.*

rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\dxb_build\ufis_client_appl\help\*.*
