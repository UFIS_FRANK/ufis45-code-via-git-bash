<?php
session_name("Staff");
session_start();
session_register("Session");

function IsNumeric($n)
{
  $p=trim($n);
  $l=strlen($p);

  for ($t=0; $t<$l; $t++)
  {
    $c=substr($p,$t,1);
    if ($c<'0' || $c>'9')
    { if ($c!='.') return 0; }
  }

  return 1;
}

function len($str) {
		return strlen($str);
}


//20070221 GFO: WAW Project : Add APC3 as a Global variable
Function FindTDI () {
 Global $db, $TimeChange, $TimeChangeDate, $TimeDiffBefore, $TimeDiffAfter, $TimeDiffCurrent;
 Global $hopo;
 $cmdTempCommandText = "SELECT TICH,TDI1,TDI2 from CEDA.APTTAB where APC3='$hopo'";	
 /* using cache to keep the info for 30 min. */
 $TimeInfo=&$db->CacheExecute(1800,$cmdTempCommandText);

	if (! $TimeInfo->EOF)
	{
	 /* TICH is used as local date */
	$year = substr ($TimeInfo->fields("TICH"), 0, 4); 
	$month = substr ($TimeInfo->fields("TICH"), 4,2); 
	$day = substr ($TimeInfo->fields("TICH"), 6, 2); 		
	$hour = substr ($TimeInfo->fields("TICH"), 8, 2); 
	$min = substr ($TimeInfo->fields("TICH"), 10, 2); 
	$sec = substr ($TimeInfo->fields("TICH"), 12, 2); 
  $TimeChange=$TimeInfo->fields("TICH");
	$TimeChangeDate=mktime($hour,$min,$sec,$month,$day,$year);
  $TimeDiffBefore=$TimeInfo->fields("TDI1");
  $TimeDiffAfter=$TimeInfo->fields("TDI2");

   $hh=gmstrftime("%H",time());
   $mm=gmstrftime("%M",time());
   $ss=gmstrftime("%S",time());
   $Month=gmstrftime("%m",time());
   $Day=gmstrftime("%d",time());
   $Year=gmstrftime("%Y",time());

	 /* generate local date (now) */
   $tnow=mktime($hh,$mm,$ss,$Month,$Day,$Year);

	 /* use time diff. after date change */
   if ($tnow>$TimeChangeDate)
	 {
	    $TimeDiffCurrent=$TimeInfo->fields("TDI2");
   }
	 /* use time diff. before date change */
	 else
	 {
     $TimeDiffCurrent=$TimeInfo->fields("TDI1");
   }	
	}// If End

  if ($TimeInfo)
		$TimeInfo->Close();
}

function ChopString ($interval,$conv,$str) {
	Global $db, $TimeChange, $TimeChangeDate, $TimeDiffBefore, $TimeDiffAfter, $TimeDiffCurrent;

	if (len($str)>0)
	{
		$year = substr ($str, 0, 4); 
		$month = substr ($str, 4,2); 
		$day = substr ($str, 6, 2); 		
		$hour = substr ($str, 8, 2); 
		$min = substr ($str, 10, 2); 
		$sec = substr ($str, 12, 2); 
		$date=mktime ($hour,$min,$sec,$month,$day,$year);
	}
	else
	{
		$date="";
	}
		
	if ($date>=$TimeChangeDate && $conv='Y' && len($str)>0)
	{
		//echo "DB:[".$TimeChange." Before:".$TimeDiffBefore." After:".$TimeDiffAfter."]==>After Old(".$str.") ";
		return AddDiff ("M",$TimeDiffAfter, $date);
	}
	if ($date<$TimeChangeDate && $conv='Y' && len($str)>0)
	{
		//echo "DB:[".$TimeChange." Before:".$TimeDiffBefore." After:".$TimeDiffAfter."]==>Before Old(".$str.") ";
		return AddDiff ("M",$TimeDiffBefore, $date);
	}
	else
	{
		return $date;	
	}
}

function AddDiff ($type,$diff, $date) {

		//echo "Time-In:(".$date.") - ";
    switch ($type) {
        case "D":
						$datenew = $date + ($diff*60*60*24);
            break;        
        case "H":
						$datenew = $date + ($diff*60*60);
            break;        
        case "M":
						$datenew = $date + ($diff*60);
            break;        
        case "S":
				default :
						$datenew = $date + $diff;
            break;        
    }    
		//echo "Time-Out:(".$datenew.") - ";

		$date_time_array = getdate($datenew);

		$hours =  $date_time_array["hours"];
		$minutes =  $date_time_array["minutes"];
		$seconds =  $date_time_array["seconds"];
		$month =  $date_time_array["mon"];
		$day =  $date_time_array["mday"];
		$year =  $date_time_array["year"];

		$timestamp =  mktime($hours,$minutes, $seconds,$month ,$day, $year);
		//echo "Time-New:(".$timestamp.")-(".$year."|".$month."|".$day."|".$hours."|".$minutes."|".$seconds.")<br>";
    return $timestamp;
}

	function IndexMenu() {
	Global $dbS;
		$tmpq="SELECT MENUNAME,TMPL_MENUID";
		$tmpq .=	" FROM  S_TMPL_MENUS";
		$tmpq .=	" ORDER by  ROWNUMBER";
	  	$cmdTempCommandText = $tmpq;
		$rs=$dbS->Execute($cmdTempCommandText);
		if (!$rs->EOF)  {
			while (!$rs->EOF) {
				echo "<tr>\n";
				echo "	<td  class=\"Frontmenu\">&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"inf1.php?TMPL_MENUID=".$rs->Fields("TMPL_MENUID")."\">".$rs->Fields("MENUNAME")."</a></td>\n";
				echo "</tr>\n";
				echo "<tr>\n";
				echo "	<td><img src=\"pics/dot.gif\" width=\"1\" height=\"10\" alt=\"\" border=\"0\"></td>\n";
				echo "</tr>\n";
				$rs->MoveNext();
			}
		  }// If End
    	if ($rs) $rs->Close();

}
	function errorMsg() {
	  Global $Session,$Error;
	  if ($Session["Type"]=="GATE") {
	    $tmp="Gate";		
	  } else {
	    $tmp="CheckIn";
          }
          switch ($Error) {
            case "1":
	      $strlogin="You have entered a wrong password. <BR>Please try again.";
	      break;
	    case "2":
              $strlogin="You are not allowed to log in from this terminal.<BR> Please contact IT Help Desk (Tel. 7777) for more information.";
	      break;
	    case "99":
	      $strlogin="The system is currently not availiable .<BR> Please contact IT Help Desk (Tel. 7777) for more information.";
	      break;
	    default:
	      $strlogin="Please log in.";
         }
	 return $strlogin;
       }
	
?>
