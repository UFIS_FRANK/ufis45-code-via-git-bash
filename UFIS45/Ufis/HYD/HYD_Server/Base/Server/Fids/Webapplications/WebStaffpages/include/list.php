<?php 

/*
V0.60 08 Nov 2000 (c) 2000 John Lim. All rights reserved.
  Released under Lesser GPL library license. See License.txt.
*/ 
  
// 20060621 JIM: PRF 8203: correct sorting via "ORDER BY" instead of sortColumn() 

// specific code for tohtml

// RecordSet to HTML
//------------------------------------------------------------
// Convert a recordset to a html table. Multiple tables are generated
// if the number of rows is > $gSQLBlockRows. This is because
// web browsers normally require the whole table to be downloaded
// before it can be rendered, so we break the output into several
// smaller faster rendering tables.
//
// $rs: the recordset
// $ztabhtml: the table tag attributes
// $zheaderarray: contains the replacement strings for the headers
//
// RETURNS: number of rows displayed

 
 /* 
 Break the results into pages , we use the $gSQLBlockRows
 as the maximum records per page we also use the $RecordCount var 
 in order to known what is the number of records tha the select statement returns
 */
 
//20060710 JIM: PRF 8204: excel export: added $Export
//function listpages(&$rs,$RecordCount,$ztabhtml="")
function listpages(&$rs,$RecordCount,$ztabhtml="",$Export)
{

$s =''; $flds; $ncols; $i;$rows=0;$hdr;$docnt = false;
GLOBAL $gSQLMaxRows,$gSQLBlockRows,$DatabaseType,$listElement,$movenext,$db,$list_PagingMove,$lastpage,$prevrows;
/* var for the search form */
GLOBAL $allcategory,$todateplus,$airline,$flno,$city,$dadr,$stat,$PHP_SELF;
GLOBAL $SelectedDate;
GLOBAL $TfVal1,$TfVal1Month,$TfVal1Year,$TfVal1Day,$TfVal1Hour,$TfVal1Min;
GLOBAL $TfVal2,$TfVal2Month,$TfVal2Year,$TfVal2Day,$TfVal2Hour,$TfVal2Min;
// 20060621 JIM: PRF 8203: added $OrderBy
GLOBAL $ol,$masktitle,$type,$debug,$tdi,$tdiminus,$HeadingTitle,$OrderBy,$SearchValues;
GLOBAL $tpl,$currentpagetitle,$pagecounttitle,$TMPL_MENUID,$login,$FldArray,$login,$RowsPerPage,$RefreshRate;

//20060710 JIM: PRF 8204: excel export: table length unlimited
  if ($Export=="Export")
	{
		$RowsPerPage=$RecordCount;
	}
	if (! $ztabhtml) $ztabhtml = " BORDER=3 width=50%  bgcolor=\"#77B0E4\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\"' ";

		$cr=1;
		if ($RowsPerPage >=1)
		{
			$gSQLBlockRows = $RowsPerPage;
		}
		$pagecount=round($RecordCount/$gSQLBlockRows);

		if ($pagecount < $RecordCount/$gSQLBlockRows) { 
		 $pagecount = $pagecount +1;
		}

	//echo "list.php(".$TfVal1."-".$TfVal2.")<br>";
	//echo "list.php(".$list_PagingMove.")<br>";
    $list_PagingMove = trim($list_PagingMove);
	
	switch ($list_PagingMove) {
        case "<<":
			$rs->movefirst();			
			$cr=1;
			 break;
		case "<":
        case "<Prev":
        case "<  PREVIOUS":
		
			if ($prevrows<20) {
				$result=$movenext-$gSQLBlockRows-$prevrows;
			} else {
				$result=$movenext-($gSQLBlockRows*2);
			}
			if ( $result < 1) {
				$rs->movefirst();
                        	$cr=1;
			} else {
                       		$rs->move($result);
				$cr=($result)+$gSQLBlockRows;
			}
			 break;
		case ">":
        case "Next>":
        case "NEXT  >":
			if ($movenext>=$RecordCount) { $movenext=0;}
			$rs->move($movenext);
			$cr=($movenext-1)+$gSQLBlockRows;
			 break;
        case ">>": 
				$result=$pagecount*$gSQLBlockRows;
				if ($result > $RecordCount) {
					$result=$result- $RecordCount;
					$result=$gSQLBlockRows-$result;
					$result=$RecordCount-$result;
				} else {
					$result= $RecordCount - $result;
					$result=$gSQLBlockRows-$result;
					$result=$RecordCount-$result;
				echo $result;
				}
				$rs->move($result);
				$cr=($result)+$gSQLBlockRows;
	 		 break;
    }


		if  ($rs->EOF && $RecordCount >0)  {
			$rs->movefirst();
			$cr=1;
		}

		$currentpage=round( $cr/ $gSQLBlockRows );
		if ($currentpage<=0) {$currentpage=1;}
		//echo "count:$RecordCount,BlockR=$gSQLBlockRows<br>";
		if ($RecordCount>$gSQLBlockRows) { 
			// Assign the values 
			$tpl->set_var( "RecordCount",$RecordCount );		
			$tpl->set_var( "HeadingTitle",$HeadingTitle );		
			$tpl->set_var( "currentpage",$currentpagetitle.$currentpage);		
			$tpl->set_var( "pagecount",$pagecounttitle.$pagecount );				
			$tpl->set_var( "FormType","submit" );				
		}  else {
			$tpl->set_var( "RecordCount",$RecordCount );		
			$tpl->set_var( "HeadingTitle",$HeadingTitle );		
			$tpl->set_var( "currentpage",$currentpagetitle.$currentpage);		
			$tpl->set_var( "pagecount",$pagecounttitle.$pagecount );				
			$tpl->set_var( "FormType","hidden" );				
			//$tpl->set_var( "RecordCount","" );		
			//$tpl->set_var( "HeadingTitle","" );		
			//$tpl->set_var( "currentpage","");		
			//$tpl->set_var( "pagecount","" );				
			//$tpl->set_var( "FormType","hidden" );				
		}

		// We Must change the "result" to the value from the table		
		// We include the columns 
		$tpl->set_var( "ztabhtml",$ztabhtml );			
		$TableHeader="";

//20060710 JIM: PRF 8204: excel export: added $Export
//		$tpl->set_var( "TableHeader",$login->FldHeader ($FldArray) );				
		$tpl->set_var( "TableHeader",$login->FldHeader ($FldArray,$Export) );				
		
		if ($rs->EOF) {
		 		// We include the NoRows 
				$TableNoRows="";
		        $TableNoRows=$TableNoRows."<td colspan=\"20\" align=\"center\" valign=\"top\">	<B class=\"norows\">We are sorry but there are no results for you - Please Try Next Option</B></td>";
				$tpl->set_var( "TableRows",$TableNoRows );		
				$tpl->set_var( "SearchValues","" );	
			 	$tpl->parse("Rows",true);
		 } 
 
	while (!$rs->EOF) {
	
	// We include the Rows 
	$TableRows="";		
		 
	// each single line is passed once to be manipulated by FldRows() - method
	$tpl->set_var( "TableRows",	$login->FldRows ($FldArray,$rs,$rows));
	$tpl->parse("Rows",true);

	$rows += 1;
	if ($rows >= $gSQLMaxRows) {
			$rows = "<p>Truncated at $gSQLMaxRows</p>";
		break;
	}
	$rs->MoveNext();

		// additional EOF check to prevent a window header
	  if (( !$rs->EOF && $rows == $gSQLBlockRows)|| ($rs->EOF &&$pagecount>1) )
		{

				$tpl->set_var("CurrentRow",$rs->CurrentRow() );		
				$tpl->set_var( "Prevrows",$rows );		
				// Assign Search Values
				$SearchValues="";
				if ($allcategory) {
					$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"allcategory\" value=\"". $allcategory."\">\n";
				}
				if ($flno) {
					$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"flno\" value=\"". $flno."\">\n";
				}
				if ($dadr) {
					$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"dadr\" value=\"". $dadr."\">\n";
				}
				if ($stat) {
					$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"stat\" value=\"". $stat."\">\n";
				}
				if ($airline) {
					$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"airline\" value=\"". $airline."\">\n";
				}

				if ($city) {
					$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"city\" value=\"". $city."\">\n";
				}
				if ($SelectedDate) {
					$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"SelectedDate\" value=\"". $SelectedDate."\">\n";
				}

				if ($TfVal1Month) {
					$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"TfVal1Month\" value=\"". $TfVal1Month."\">\n";
				}
				if ($TfVal1Year) {
					$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"TfVal1Year\" value=\"". $TfVal1Year."\">\n";
				}
				if ($TfVal1Day) {
					$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"TfVal1Day\" value=\"". $TfVal1Day."\">\n";
				}
				if ($TfVal1Hour) {
						$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"TfVal1Hour\" value=\"". $TfVal1Hour."\">\n";
				}
				if ($TfVal1Min) {
						$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"TfVal1Min\" value=\"". $TfVal1Min."\">\n";
				}

				if ($TfVal2Month) {
					$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"TfVal2Month\" value=\"". $TfVal2Month."\">\n";
				}
				if ($TfVal2Year) {
					$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"TfVal2Year\" value=\"". $TfVal2Year."\">\n";
				}
				if ($TfVal2Day) {
					$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"TfVal2Day\" value=\"". $TfVal2Day."\">\n";
				}
				if ($TfVal2Hour) {
						$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"TfVal2Hour\" value=\"". $TfVal2Hour."\">\n";
				}
				if ($TfVal2Min) {
						$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"TfVal2Min\" value=\"". $TfVal2Min."\">\n";
				}

				if ($todateplus) {
						$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"todateplus\" value=\"". $todateplus."\">\n";
				}
				else
				{
					$todateplus="";
				}
				if ($debug==true) {
						$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"debug\" value=\"". $debug."\">\n";
				}
				// 20060621 JIM: PRF 8203: $OrderBy
				$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"OrderBy\" value=\"".$OrderBy."\">\n";
					$SearchValues=	$SearchValues. $login->FldSrows ($FldArray);

					$tpl->set_var( "SearchValues",$SearchValues );
					return $rows;
		}
	} //end while 
	return $rows;
 }//listpages
?>
