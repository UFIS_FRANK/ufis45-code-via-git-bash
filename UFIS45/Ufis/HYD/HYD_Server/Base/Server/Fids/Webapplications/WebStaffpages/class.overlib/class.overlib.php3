<?
	class Overlib {
	    var $ol_fcolor     = "#ccccff";
	    var $ol_backcolor  = "#333399";
	    var $ol_textcolor  = "#000000";
	    var $ol_capcolor   = "#ffffff";
	    var $ol_closecolor = "#9999ff";
	    var $ol_width      = 200;
	    var $ol_border     = 1;
	    var $ol_offsetx    = 10;
	    var $ol_offsety    = 50;
	    var $ol_path       = "class.overlib";

	  function init ( ) {
		$tmp="";
			
		$tmp=$tmp."<link rel='stylesheet' href='".$this->ol_path."/overlib.css'  type='text/css'>\n";
		$tmp=$tmp."<div id='overDiv' style='position:absolute; visibility:hide; z-index: 1;'>\n";
		$tmp=$tmp."</div>\n";
		$tmp=$tmp."<script language='javascript'>\n";
		$tmp=$tmp."if (typeof fcolor == 'undefined') { var fcolor = \"".$this->ol_fcolor."\";}\n";
		$tmp=$tmp."if (typeof backcolor == 'undefined') { var backcolor = \"".$this->ol_backcolor."\";}\n";
		$tmp=$tmp."if (typeof textcolor == 'undefined') { var textcolor = \"".$this->ol_textcolor."\";}\n";
		$tmp=$tmp."if (typeof capcolor == 'undefined') { var capcolor =\"".$this->ol_capcolor."\";}\n";
		$tmp=$tmp."if (typeof closecolor == 'undefined') { var closecolor =\"".$this->ol_closecolor."\";}\n";
		$tmp=$tmp."if (typeof width == 'undefined') { var width = \"".$this->ol_width."\";}\n";
		$tmp=$tmp."if (typeof border == 'undefined') { var border =\"".$this->ol_border."\";}\n";
		$tmp=$tmp."if (typeof offsetx == 'undefined') { var offsetx = ". $this->ol_offsetx.";}\n";
		$tmp=$tmp."if (typeof offsety == 'undefined') { var offsety = ". $this->ol_offsety.";}\n";
		$tmp=$tmp."</script><script language='javascript' src='".$this->ol_path."/overlib.js'>\n";
		$tmp=$tmp."</script>\n";

		return ($tmp);
	  }

	  function over ($title, $text, $autoclose=true, $align=2,$follow=false)
	  {
	    if ($autoclose == true) {
		$func_1 = "d";

	    	if ($follow == true)
			$func_3 = "c";
	    	else
			$func_3 = "s";
	    } else {
		$func_1 = "s";
		$func_3 = "c";
	    }

	    switch ($align) {
		case 0:		$func_2 = "l"; break;
		case 1:		$func_2 = "c"; break;
		default:	$func_2 = "r"; break;
	    } 

	    $jsfunc = $func_1 . $func_2 . $func_3;

	    $output=" onMouseOver=\"$jsfunc('$text','$title'); return true;\" ";
	    $output.=" onMouseOut=\"nd(); return true;\" ";

	    return ($output);
	  }
	}
?>
