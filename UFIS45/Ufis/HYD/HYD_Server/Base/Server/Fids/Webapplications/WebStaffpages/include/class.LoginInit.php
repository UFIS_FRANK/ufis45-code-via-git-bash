<?
 //JWE 01.07.2004
// 20060621 JIM: PRF 8203: correct sorting via "ORDER BY" instead of sortColumn() 
 include('tohtml.inc.php');
 include('rsfilter.inc.php');
 
class LoginInit
{

//20060710 JIM: PRF 8204: excel export: added $Export
//function Login($UserName,$Password)
function Login($UserName,$Password,$Export)
{
	Global $dbS,$Session,$TMPL_MENUID,$ClientIP;

//20060711 JIM: PRF 8200: define user profiles: use prof_id and s_users_new
	//$cmdTempCommandText = "SELECT prof_id,username  from s_users_new where username='".$UserName."' and passwd='".$Password."' and USERIP='".$ClientIP."'";
	$cmdTempCommandText = "SELECT prof_id,username,userip  from s_users_new where username='".$UserName."' and passwd='".$Password."'";
	//echo "cmdTempCommandText : $cmdTempCommandText <br>";
	$rs=$dbS->Execute($cmdTempCommandText);
	if (!$rs)
	{
		$err=$dbS->ErrorMsg();
		echo "Error: $err<br>";
	}
	if (!$rs->EOF)
	{
//20060711 JIM: PRF 8200: define user profiles: store prof_id as userid for less code changes
	
	        $tempIP=$rs->fields("userip");		
		if ($ClientIP==$tempIP) {
		  $Session["userid"]=$rs->fields("prof_id");
		  $Session["username"]=$rs->fields("username");		
		  // Find the Tmpl's for this user
//		  $Query=$this->Tmpl($rs->fields("prof_id"));
//20060710 JIM: PRF 8204: excel export: added $Export
//20060711 JIM: PRF 8200: define user profiles: use prof_id
		  $Query=$this->Tmpl($rs->fields("prof_id"),$Export);
		}
		else
	        {
		  header ("Location: index.php?Error=2&TMPL_MENUID=".$TMPL_MENUID); 
		  exit ;
	        }
	}
	else
	{
			header ("Location: index.php?Error=1&TMPL_MENUID=".$TMPL_MENUID); 
			exit ;
	}
	if ($rs) $rs->Close();
		return $Query;
} 


//20060710 JIM: PRF 8204: excel export: added $Export
//20060711 JIM: PRF 8200: define user profiles: use prof_id
//20070221 GFO: WAW Project: add variable $LangTR to change the Language 
// function Tmpl($userid) {
function Tmpl($prof_id,$Export) {
	
	Global $dbS,$Session,$tpl,$tmpl_array,$TMPL_MENUID,$PHP_SELF,$maintmpl,$OrgDes,$PageTitle,$StyleSheet,$RowsPerPage,$RefreshRate,$HlCycle,$TfVal1,$TfVal2,$SearchUsed,$ShowTimeValues,$database,$databaseName;
	Global $LangTR;

		$tmpq="SELECT S_TMPL_MENUS.QUERY QUERY,S_TMPL_MENUS.ORGDES ORGDES,S_TMPL_MENUS.PAGETITLE PAGETITLE,S_TMPL_MENUS.TMPL_MENUID TMPL_MENUID, S_TMPL_MENUS.MENUNAME".$LangTR." MENUNAME, S_TMPL_MENUS.ROWNUMBER ROWNUMBER, S_TMPL_MENUS.ROWSPERPAGE ROWSPERPAGE, S_TMPL_MENUS.REFRESHRATE REFRESHRATE, S_TMPL_MENUS.HLCYCLE HLCYCLE, S_TMPL_MENUS.TFVAL1 TFVAL1,S_TMPL_MENUS.TFVAL2 TFVAL2, S_TMPL_MENUS.SHOWTIMEVALUES SHOWTIMEVALUES, S_TMPL_MENUS_USERS.STYLESHEET STYLESHEET, S_TMPL.TMPLNAME  TMPLNAME";
		$tmpq .=	" FROM S_TMPL S_TMPL,  S_TMPL_MENUS, S_TMPL_MENUS_USERS";
		$tmpq .=	" WHERE S_TMPL_MENUS_USERS.TMPL_MENUID = S_TMPL_MENUS.TMPL_MENUID AND S_TMPL_MENUS_USERS.TMPLID = S_TMPL.TMPLID";
		$tmpq .=	" AND ((S_TMPL_MENUS_USERS.USERID=".$prof_id.")) ORDER BY S_TMPL_MENUS.ROWNUMBER";

	  $cmdTempCommandText = $tmpq;
		$rs=$dbS->Execute($cmdTempCommandText);
	if (!$rs)
	{
		$err=$dbS->ErrorMsg();
		echo "cmdTempCommandText: $cmdTempCommandText<br>";
		echo "Error: $err<br>";
	}
		$i=0;

		$MenuTitle=array();
		
		if (!$rs->EOF)  {
			while (!$rs->EOF) {
				if (!$TMPL_MENUID) {
				   if ($i==0) {
					
					$FldArray=$this->Fld($rs->fields("TMPL_MENUID"),$prof_id);
//20060710 JIM: PRF 8204: excel export: added $Export
//					$this->FldHeader($FldArray);
					$this->FldHeader($FldArray,$Export);
					$maintmpl=$rs->fields("menuname");
					$PageTitle=$rs->fields("PAGETITLE");
					$StyleSheet=$rs->fields("STYLESHEET");
					$RowsPerPage=$rs->fields("ROWSPERPAGE");
					$RefreshRate=$rs->fields("REFRESHRATE");
					$HlCycle=$rs->fields("HLCYCLE");
					$TfVal1=$rs->fields("TFVAL1");
					$TfVal2=$rs->fields("TFVAL2");
					$ShowTimeValues=$rs->fields("SHOWTIMEVALUES");
					$OrgDes=$rs->fields("ORGDES");
					$Query=trim($rs->fields("QUERY"));
					$array2=array($rs->fields("tmplname")=>$rs->fields("menuname"));
					$tmplname=$rs->fields("tmplname");
					Global $menuname;
					$menuname=$rs->fields("menuname");
					$MenuTitle[$i]=$rs->fields("menuname");
					//20070314 GFO: Fixing the Bug  for the Order By statement
					$TMPL_MENUID = $rs->fields("TMPL_MENUID");
				   } else {
					$MenuTitle[$i]="<a href=\"".$PHP_SELF."?TMPL_MENUID=".$rs->fields("TMPL_MENUID")."\" class=\"menu\" onMouseover=\"hidep('all')\">".$rs->fields("menuname")."</a>";		
	
				   }	
					
				
				} else {
				   if ($rs->fields("TMPL_MENUID")==$TMPL_MENUID) {


					$FldArray=$this->Fld($rs->fields("TMPL_MENUID"),$prof_id);
//20060710 JIM: PRF 8204: excel export: added $Export
//					$this->FldHeader($FldArray);
					$this->FldHeader($FldArray,$Export);
					$maintmpl=$rs->fields("menuname");
					$PageTitle=$rs->fields("PAGETITLE");
					$StyleSheet=$rs->fields("STYLESHEET");
					$RowsPerPage=$rs->fields("ROWSPERPAGE");
					$RefreshRate=$rs->fields("REFRESHRATE");
					$HlCycle=$rs->fields("HLCYCLE");
					$TfVal1=$rs->fields("TFVAL1");
					$TfVal2=$rs->fields("TFVAL2");
					$ShowTimeValues=$rs->fields("SHOWTIMEVALUES");
					$OrgDes=$rs->fields("ORGDES");
					$Query=trim($rs->fields("QUERY"));
	

					$array2=array($rs->fields("tmplname")=>$rs->fields("menuname"));
					$tmplname=$rs->fields("tmplname");
					Global $menuname;
					$menuname=$rs->fields("menuname");

					 $MenuTitle[$i]=$rs->fields("menuname");

				   } else {
					   $MenuTitle[$i]="<a href=\"".$PHP_SELF."?TMPL_MENUID=".$rs->fields("TMPL_MENUID")."\" class=\"menu\" onMouseover=\"hidep('all')\">".$rs->fields("menuname")."</a>";		
	
				   }	
					
				}
				$rs->MoveNext();
				$i++;
		}

//20060710 JIM: PRF 8204: excel export: using other templates, TODO: dynamic name change
		if ($Export=="Export" )
		{
			$tmplname="xls_".$tmplname;
			//$tmplname="moccon_xls.tmpl";
		}
		/* Assign The Menu Table*/
		$tpl->load_file($tmplname,$menuname);
		while (list ($key,$val) = each ($MenuTitle)) {
			
			//$ConnectionInfo=$Session["username"]."@".$database;
			$ConnectionInfo=$Session["username"]."@".$databaseName;
			$tpl->set_var("ConnectionInfo",$ConnectionInfo);
			$tpl->set_var("MenuTitle", $val);
			$tpl->parse("Menus", true);
		}

	  }// If End
    if ($rs) $rs->Close();

		return $Query;
	}// End Function 

	
//20060711 JIM: PRF 8200: define user profiles: use prof_id
//20070221 GFO: WAW Project: add variable $LangTR to change the Language 
	function Fld($TMPL_MENUID,$prof_id,$SEARCH=1) {
		global $dbS;
		global $FldArray,$ExtraQuery;
		global $LangTR;
		$tmp="";
		//$tmp.="SELECT S_FLDS.QUERY,S_FLDS.DISPLAYFIELD,S_FLDS.FUNCTION,S_FLDS.FLDDISPLAYNAME, S_FLDS.FLDEXPTEXT, S_FLDS.FLDFORMAT, S_FLDS.FLDID, S_FLDS.FLDNAME, S_FLDS.FLDTABLE,S_TMPL_FLD.SEARCH,S_TMPL_FLD.CSS_CLASSNAME ";
		//$tmp.=" FROM S_FLDS S_FLDS, S_TMPL_FLD S_TMPL_FLD";
		//$tmp.=" WHERE S_FLDS.FLDID = S_TMPL_FLD.FLD_ID ";
		//$tmp.=" AND ((S_TMPL_FLD.TMPL_MENUID=$TMPL_MENUID) ";
		//$tmp.=" AND (S_TMPL_FLD.USERID=$USERID)) order by S_FLDS.ROWNUMBER";

		$tmp.="SELECT S_FLDS.QUERY,S_FLDS.DISPLAYFIELD,S_FLDS.FUNCTION,S_FLDS.FLDDISPLAYNAME".$LangTR." FLDDISPLAYNAME, S_FLDS.FLDEXPTEXT".$LangTR." FLDEXPTEXT, S_TMPL_FLD.FLDFORMAT, S_FLDS.FLDID, S_FLDS.FLDNAME, S_FLDS.FLDTABLE,S_TMPL_FLD.SEARCH,S_TMPL_FLD.CSS_CLASSNAME ";
		$tmp.=" FROM S_FLDS S_FLDS, S_TMPL_FLD S_TMPL_FLD";
		$tmp.=" WHERE S_FLDS.FLDID = S_TMPL_FLD.FLD_ID ";
		$tmp.=" AND ((S_TMPL_FLD.TMPL_MENUID=$TMPL_MENUID) ";
		$tmp.=" AND (S_TMPL_FLD.USERID=$prof_id)) order by S_TMPL_FLD.ROWNUMBER";

		$rs=&$dbS->Execute($tmp);

		if (!$rs->EOF)  {
			while (!$rs->EOF) {

				$FLDDISPLAYNAME=trim($rs->fields("FLDDISPLAYNAME"));
				$FLDEXPTEXT=trim($rs->fields("FLDEXPTEXT"));
				$FLDFORMAT=trim($rs->fields("FLDFORMAT"));
				$FLDID=trim($rs->fields("FLDID"));
				$FLDNAME=trim($rs->fields("FLDNAME"));
				$FLDTABLE=trim($rs->fields("FLDTABLE"));
				$FLDSEARCH=trim($rs->fields("SEARCH"));
				$CSS_CLASSNAME=trim($rs->fields("CSS_CLASSNAME"));
				$QFUNCTION=trim($rs->fields("FUNCTION"));
				$DISPLAYFIELD=$rs->fields("DISPLAYFIELD");
				$FQUERY=trim($rs->fields("QUERY"));
				$FldArraytmp=array($FLDNAME =>array($FLDDISPLAYNAME,$FLDEXPTEXT,$CSS_CLASSNAME,$FLDFORMAT,$FLDID,$FLDNAME,$FLDTABLE,$FLDSEARCH,$QFUNCTION,$DISPLAYFIELD,$FQUERY));
				$FldArray=array_merge ($FldArray, $FldArraytmp);

				$rs->MoveNext();
			}// While End

		  }// If End
	    if ($rs)$rs->Close();
			return 		$FldArray;
	}// End Function 

//20060710 JIM: PRF 8204: excel export: added $Export
//	function FldHeader ($FldArray){
	function FldHeader ($FldArray,$Export){
// 20060621 JIM: PRF 8203: correct sorting: $OrderBy
		global $ol,$masktitle,$OrderBy,$SearchValues,$ShortSearchValues;	
		global $tpl,$TMPL_MENUID;
		
	  $Dtmp = $this->DynamicFldSrows ($FldArray);
	 
	  reset($FldArray);
	  for ( $i = 0; $i < count($FldArray); $i++ ) {
		     while ( $column = each($FldArray) ) {   
		     $data = $column['value'];
				 $displayfield=$data[9];

				 if ($displayfield==1) {
					// 20060621 JIM: PRF 8203: correct sorting, $field_name:
					$field_name=$data[0];
					if ($OrderBy) 
					{
						if ($data[5]==$OrderBy)
						{
							$field_name=$field_name."&nbsp;&nbsp;&uarr;";
						}
						else if ($data[5]."_desc"==$OrderBy)
						{
							$field_name=$field_name."&nbsp;&nbsp;&darr;";
						}
					}
		 			//$tmp.="<TD VALIGN=TOP  class=\"$data[2]Title\"><a href=\"#\" class=\"$data[2]Title\" tooltip=\"$data[1]\" >$data[0]</a></TD>\n";
// 20060621 JIM: PRF 8203: correct sorting: $NewOrderBy and $OrderBy:
//  ( does not deliver SearchValues )
//		 			$tmp.="<TD VALIGN=TOP  class=\"TableColHeader\"><form ACTION=\"$PHP_SELF\" METHOD=\"GET\" >
//					<input type=\"hidden\" name=\"NewOrderBy\" value=\"$data[5]\" >
//					<input type=\"submit\" name=\"NewOrderDummy\" value=\"$field_name\" class=\"TableColHeader\" tooltip=\"$data[1]\" >
//					$SearchValues</form></TD>\n";
//20060710 JIM: PRF 8204: excel export: in case of Export don't load graphic and pass link
					if ($Export=="Export")
					{
		 				$tmp.="<TD VALIGN=TOP >$data[0]</TD>\n";
					}
					else
					{
						//20070314 GFO: Fixing the Bug  for the Order By statement
			 			$tmp.="<TD VALIGN=TOP  class=\"TableColHeader\"><a href=\"?TMPL_MENUID=$TMPL_MENUID&NewOrderBy=$data[5]&OrderBy=$OrderBy$Dtmp"."$ShortSearchValues\" class=\"TableColHeader\" tooltip=\"$data[1]\" >$field_name</a></TD>\n";
					}
				}
		   }// end while
		 }// end for
		 return $tmp;
	}// End Function 
	
//20060710 JIM: PRF 8204: excel export: added $Export
//function QFld($Query,$WhereP) {
function QFld($Query,$WhereP,$Export) {
		global $db,$TMPL_MENUID,$RefreshRate;
		global $FldArray,$HlCycle,$TfVal1,$TfVal2,$SearchUsed,$ShowTimeValues;

// 20060621 JIM: PRF 8203: old counting:
//		$cmdTempCommandTextCount = "SELECT Count(*)  recordcount ". $WhereP;
//		$Count= &$db->Execute($cmdTempCommandTextCount);
//		echo "<br>LoginInit: QFld: cmdTempCommandTextCount : $cmdTempCommandTextCount <br>";
//		echo "LoginInit: QFld: Count: $Count<br>";
//		$RecordCount=htmlspecialchars(trim($Count->fields[0]));
//		if ($Count) $Count->Close();
		//echo "class.logininit.php-SQL:(".$Query.")";

		//20070315 GFO: We need that if ORACLE is started with Local Time
		//Please use CURRENT_DATE Instead of SYSDATE then
		
		$AlterQuery = "ALTER SESSION SET TIME_ZONE='GMT'";
		$rs = &$db->Execute($AlterQuery);
		
		//	$Query = "select TO_CHAR (current_date, 'YYYYMMDDHH24MISS') as mytime from dual";

		//Replace SYSDATE with CURRENT_DATE
		$Query=eregi_replace("SYSDATE","CURRENT_DATE",$Query);

		//echo "class.logininit.php-SQL:(".$Query.")";
		$rs = &$db->Execute($Query);

// 20060621 JIM: PRF 8203: new counting:
//          Counting also must be done in PHP, because the DB count fails, when the sort 
//          field name included in $WhereP is an alias and no DB field. 
	$RecordCount=0;

	//$rs = my_RSFilter($rs,$fn);
	
	if ($rs)
	{
		while (!$rs->EOF) 
		{
			//echo $rs->Fields[1];
			//echo $rs->Fields[1]."<br>";
			$RecordCount++;

			$rs->MoveNext();
		}
		$rs->movefirst();
	}
//	echo $RecordCount;
	//echo "LoginInit: $RecordCount: $RecordCount<br>";


		// JWE 200407: Included for update-highlighting
		// Adjustments for update highlithing
		// Old-data are stored inside a session variable for later comparsion
		// Next refresh time is stored in session variable for refresh of old-data cache
		$Menu_olddata     = $TMPL_MENUID."_OLDDATA";
		$Menu_nextrefresh = $TMPL_MENUID."_NEXTREFRESH";
		//echo "OLD-DATA-CACHE-CYCLE(".$_SESSION[$Menu_nextrefresh]."/".$HlCycle.")<br>";
		//echo "ShowTimeValues(".$ShowTimeValues.")<br>";
		//GFO 20060326 : Bug when the $rs was empty 
		if ($rs) {	

			if ((!isset($_SESSION[$Menu_olddata]) && $HlCycle == 0))
			{
				$_SESSION[$Menu_olddata] = $rs->GetRows();
				$_SESSION[$Menu_nextrefresh] = 0;
			}

			//rs2html($rs);
//20060710 JIM: PRF 8204: excel export: added $Export
//			listpages($rs,$RecordCount," BORDER=2 width=80%  bgcolor=\"#ffffff\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\"");
			listpages($rs,$RecordCount," BORDER=2 width=80%  bgcolor=\"#ffffff\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\"",$Export);
			$rs->MoveFirst();
			//if ($_SESSION[$Menu_nextrefresh] >= $HlCycle)
			if ($_SESSION[$Menu_nextrefresh] >= 1)
			{
				if ($HlCycle == 0 && $SearchUsed != "TRUE")
				{
					$_SESSION[$Menu_olddata] = $rs->GetRows();
					$_SESSION[$Menu_nextrefresh] = 0;
				}
			}
			else
			{
				if ($HlCycle == 0)
				{
					$_SESSION[$Menu_nextrefresh] = $_SESSION[$Menu_nextrefresh] + 1;
				}
			}
		}
		else
		{
			 print "<b>Error in Execute of SELECT</b></p>";
		}
}// End Function 
	

function FldRows ($FldArray,&$rs,$rows){
		global $db, $tdi, $TMPL_MENUID, $HlCycle, $SearchUsed,$ShowTimeValues;	

		$Menu_olddata = $TMPL_MENUID."_OLDDATA";

		// rewind to start of array of fields
	  reset($FldArray);

		if ($HlCycle == 0 && isset($_SESSION[$Menu_olddata]))
		{
			$newdatavalue=$rs->Fields("URNO");

			// rewind to start of array of old-data records
			reset($_SESSION[$Menu_olddata]);

			while ($oldrec = each($_SESSION[$Menu_olddata]))
			{
				$olddatavalue = $oldrec['value'];
				if ($olddatavalue["URNO"] == $newdatavalue)
				{
					//echo "Record [".$recval["URNO"].$recval["FLNO"]."] exists in old-data<br>";
					break;
				}
			}
		}

		//Set $x for different css-style class reference
		$x=$rows % 2;
		while ( $column = each($FldArray) )
		{   
			$highlight=FALSE;
			$Key = strtoupper($column['key']);
			$data = $column['value'];

			if ($HlCyle == 0)
			{
				$newdatavalue=$rs->Fields($data[5]);
				//echo "[".$Key."-".$newdatavalue."-".$olddatavalue[$Key]."]<br>";
				if (strcasecmp($newdatavalue,$olddatavalue[$Key])!=0)
				{
					//echo "<br>DIFF: [".$Key."-".$newdatavalue."-".$olddatavalue[$Key]."]<br>";
					$highlight=TRUE;
					//$olddatavalue[$key] = $newdatavalue;
				}
			}

			$datavalue=$rs->Fields($data[5]);
			$datatype=$data[3];
			$displayfield=$data[9];
			if (len(trim($datavalue))>0 ) {
				switch ($datatype) {
					case "1":
					 // Date Type H:i 
						if ($ShowTimeValues == 0) // Utc value to use
						{
							$datavalue=date("H:i", ChopString ("n","N",$rs->Fields($data[5])));
						}
						else // convert to local
						{
							$datavalue=date("H:i", ChopString ("n","Y",$rs->Fields($data[5]))  );
						}
						break;
					case "2":
						 // Date Type H:i &nbsp; d.m
						if ($ShowTimeValues == 0) // Utc value to use
						{
							$datavalue=date("H:i  ", ChopString ("n","N",$rs->Fields($data[5])))."&nbsp;&nbsp;".date(" d.m ",ChopString("n","N",$rs->Fields($data[5])));
						}
						else // convert to local
						{
							$datavalue=date("H:i  ", ChopString ("n","Y", $rs->Fields($data[5]))  ). "&nbsp;&nbsp;". date(" d.m ", ChopString ("n","Y",$rs->Fields($data[5]))  );
						}
						break;
					case "3":
						 // Date Type H:i +-n days
						if ($ShowTimeValues == 0) // Utc value to use
						{
						 	$Today_utc=gmmktime(0,0,0);
						 	$Date_utc =gmmktime( 0, 0, 0,substr($rs->Fields($data[5]),4,2),
																					substr($rs->Fields($data[5]),6,2),
																					substr($rs->Fields($data[5]),0,4));
							$DAYVAL=($Date_utc - $Today_utc)/86400;
							if ($DAYVAL>0)
							 $DAYVAL="+".$DAYVAL;
							if ($DAYVAL==0)
							 $DAYVAL="";
							$datavalue=date("H:i", ChopString ("n","N",$rs->Fields($data[5])))." ".$DAYVAL;
						}
						else // convert to local
						{
						 	$Today_loc=mktime(0,0,0);
							$TmpDate=ChopString("n","Y",$rs->Fields($data[5]));
							$TmpDate2=strftime("%Y%m%d",$TmpDate);
						 	$Date_loc=mktime(0,0,0,substr($TmpDate2,4,2),substr($TmpDate2,6,2),substr($TmpDate2,0,4));

							$DAYVAL=($Date_loc - $Today_loc)/86400;
							if ($DAYVAL>0)
							 $DAYVAL="+".$DAYVAL;
							if ($DAYVAL==0)
							 $DAYVAL="";
							$datavalue=date("H:i", ChopString ("n","Y",$rs->Fields($data[5])))." ".$DAYVAL;
						}
						break;
					case "4":
						// PopUp Text
						$datavalue="<a href=\"#\"  tooltip=\"".$datavalue."\" tooltipstyle=\"Width: 200px;\"><strong>Comments</strong></a>";
						break;

					// Text Is the default
					default:
						$datavalue=$rs->Fields($data[5]);
						break;
					}
				} else {
					$datavalue="&nbsp;";
				}
			if ($displayfield==1)
			{
			  if ($Export=="Export") {
					// GFO 20070313: Remove the Images from the datavalue 
					
					$datavalue=eregi_replace("<img *src.*>","",$datavalue);
					//$datavalue=eregi_replace("<img *src.*>","",$datavalue);
					//$datavalue=eregi_replace("<img *src=pics/","<img src=/pics/",$datavalue);
	   
			   }

				if ($highlight == FALSE || $SearchUsed=="TRUE") // data are equal or shall not be highlighted on change, so don't use diff. color
				{
						$tmp.="<TD VALIGN=TOP  class=\"$data[2]$x\">".$datavalue."</TD>\n";
				}
				else // data are different, so highliting the updated value
				{
					if ($highlight == TRUE && $HlCyle > 1) 
					{
						$tmp.="<TD VALIGN=TOP  class=\"TDHL\">".$datavalue."</TD>\n";
					}
					else
					{
						$tmp.="<TD VALIGN=TOP  class=\"$data[2]$x\">".$datavalue."</TD>\n";
					}
				}
			}
		 }// end while
		 return $tmp;
	}// End Function 

	function FldSrows ($FldArray){
		Global $tpl,$maintmpl;
		// Display the Values   
	    reset($FldArray);
		  for ( $i = 0; $i < count($FldArray); $i++ ) {
		  
		      while ( $column = each($FldArray) ) {   
		         $data = $column['value'];
					if ($data[7]==1) {
							$var=&$GLOBALS["Dynamic".$data[5]];
							if (len($var)>0) { 
								$tmp=	$tmp."<input type=\"hidden\" name=\"Dynamic$data[5]\" value=\"". $var."\">\n";
							}
					}
		      }// end while
		   }// end for
		   return $tmp;
	}// End Function 
	
	function DynamicFldSrows ($FldArray){
//		Global $tpl,$maintmpl;
		// Display the Values   
	    reset($FldArray);
		  for ( $i = 0; $i < count($FldArray); $i++ ) {
		      while ( $mycolumn = each($FldArray) ) {   
		         $mydata = $mycolumn['value'];
					if ($mydata[7]==1) {
							$myvar=&$GLOBALS["Dynamic".$mydata[5]];
							if (len($myvar)>0) { 
								$mytmp=$mytmp."&Dynamic$mydata[5]=$myvar";
							}
					}
		      }// end while
		   }// end for
		   return $mytmp;
	}// End Function 
	
	function FldDSrows ($FldArray){
// 20060727 JIM: PRF 8202: added global $FirstSearchField and lokal $TmpFirstSearchField:
		Global $tpl,$maintmpl,$ExtraQuery,$city,$airline,$flno,$FirstSearchField;
		$TmpFirstSearchField="";
		// Display the Values   

	    reset($FldArray);
		  for ( $i = 0; $i < count($FldArray); $i++ ) {
		  
		      while ( $column = each($FldArray) ) {   
		         $data = $column['value'];
					if ($data[7]==1) {
						$var=&$GLOBALS["Dynamic".$data[5]];

// 20060727 JIM: PRF 8202: Begin: set focus to first search field Dynamic Search Box
						if ( $TmpFirstSearchField=="")
						{
							$FirstSearchField="Dynamic".$data[5];
							$TmpFirstSearchField="Dynamic".$data[5];
						}
// 20060727 JIM: PRF 8202: End: set focus to first search field Dynamic Search Box

// 20060727 JIM: PRF 8208: Begin: Calendar style date input
						// Setting the Name inside the search window
						// Will be a button if field type is date ($datatype)
						// Prev. code at this place:
						//$tpl->set_var("DSearchTitle", $data[0]);
						$datatype=$data[3];
						switch ($datatype) {
							case "1":
							case "2":
								$tpl->set_var("DSearchTitle","<button style=\"visibility:show;\" type=\"reset\" id=\"trigger_".$data[5]."\"><font size=1>".trim($data[0])."</font></button>");
								break;
							default:
								$tpl->set_var("DSearchTitle", $data[0]);
								break;
						}

						// Creating a calendar if the field is a date field ($datatype)
						// Prev. code at this place:
						//	$tpl->set_var("DSearchString", "<input type=\"text\" name=\"Dynamic$data[5]\" value=\"$var\">");
						switch ($datatype) {
							case "1":
							case "2":
							$tpl->set_var("DSearchString", 
									"<input tabindex=\"$i\" type=\"text\" name=\"Dynamic".$data[5]."\" id=\"Dynamic".$data[5]."\" value=\"$var\">\n".
									"<script type=\"text/javascript\">\n".
									"Calendar.setup({\n".
									"  inputField  : \"Dynamic".$data[5]."\",\n". 
									"  ifFormat    : \"%Y%m%d\",\n".
									"  daFormat	   : \"%d.%m.%Y\",\n".
									"  button      : \"trigger_".$data[5]."\",\n".
									"  eventName	 : \"click\",\n".
									"  firstDay	   :	6,\n".
									"  step        :  1\n".
									"});\n".
									//"Calendar.setFirstDayOfWeek(6);\n".
									"</script>\n");
								break;
							default:
								// Issue 3: case insensitiv input. Input will always be transformed to uppercase.
								$var=strtoupper($var);
								//$tpl->set_var("DSearchString", "<input type=\"text\" name=\"Dynamic$data[5]\" value=\"$var\">");
								$tpl->set_var("DSearchString", "<input tabindex=\"$i\" type=\"text\" name=\"Dynamic$data[5]\" value=\"$var\">");
								break;
						}
// 20060727 JIM: PRF 8208: End: Calendar style date input

						$tpl->parse("DynamicSearch", true);
				
						// Create The ExtraQuery
						//if (len($var)>0 || len($data[10])>0 ) { 
						if (len($var)>0  ) { 
							$ExtraQuery.=" ".$data[8]." ".$data[10];
						}

					} else  {
						// Create The ExtraQuery
						if (len($data[10])>0) {
							$ExtraQuery.=" ".$data[8]." ".$data[10];
						}
						//$tpl->set_var("DynamicSearch", "");
					}
					
		      }// end while
			  if (len($var)<=0)  {
					//$tpl->set_var("DynamicSearch", "");
				}
		   }// end for
	}// End Function 
} // End Class
?>
