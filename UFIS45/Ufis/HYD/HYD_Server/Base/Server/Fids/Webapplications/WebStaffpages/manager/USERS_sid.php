<?php
/*********************************************************************************
 *       Filename: USERS.php
 *       Generated with CodeCharge 1.1.16
 *       PHP & Templates build 03/28/2001
 *********************************************************************************/

include ("./common.php");
include ("./Admin.php");

session_start();

$filename = "USERS.php";



check_security(3);

$tpl = new Template($app_path);
$tpl->load_file("USERS.html", "main");
$tpl->load_file($header_filename, "Header");

$tpl->set_var("FileName", $filename);


$sS_USERS1Err = "";

$sAction = get_param("FormAction");
$sForm = get_param("FormName");
switch ($sForm) {
  case "S_USERS1":
    S_USERS1_action($sAction);
  break;
}Administration_show();
Search_show();
S_USERS_show();
S_USERS1_show();

$tpl->parse("Header", false);

$tpl->pparse("main", false);

//********************************************************************************


function Search_show()
{
  global $db;
  global $tpl;
  
  $tpl->set_var("ActionPage", "USERS.php");
	
  // Set variables with search parameters
  $flds_USERNAME = strip(get_param("s_USERNAME"));
    // Show fields
    $tpl->set_var("s_USERNAME", tohtml($flds_USERNAME));
  $tpl->parse("FormSearch", false);
}



function S_USERS_show()
{

  
  global $tpl;
  global $db;
  global $sS_USERSErr;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $HasParam = false;

  
  $tpl->set_var("TransitParams", "s_USERNAME=" . tourl(strip(get_param("s_USERNAME"))) . "&");
  $tpl->set_var("FormParams", "s_USERNAME=" . tourl(strip(get_param("s_USERNAME"))) . "&");
  // Build WHERE statement
  
  $ps_USERNAME = get_param("s_USERNAME");
  if(strlen($ps_USERNAME)) 
  {
    $HasParam = true;
    $sWhere .= "S.USERNAME like " . tosql("%".$ps_USERNAME ."%", "Text");
  }
  if($HasParam)
    $sWhere = " WHERE (" . $sWhere . ")";

  $sDirection = "";
  $sSortParams = "";
  
  // Build ORDER statement
  $sOrder = " order by S.USERNAME Asc";
  $iSort = get_param("FormS_USERS_Sorting");
  $iSorted = get_param("FormS_USERS_Sorted");
  if(!$iSort)
  {
    $tpl->set_var("Form_Sorting", "");
  }
  else
  {
    if($iSort == $iSorted)
    {
      $tpl->set_var("Form_Sorting", "");
      $sDirection = " DESC";
      $sSortParams = "FormS_USERS_Sorting=" . $iSort . "&FormS_USERS_Sorted=" . $iSort . "&";
    }
    else
    {
      $tpl->set_var("Form_Sorting", $iSort);
      $sDirection = " ASC";
      $sSortParams = "FormS_USERS_Sorting=" . $iSort . "&FormS_USERS_Sorted=" . "&";
    }
    
    if ($iSort == 1) $sOrder = " order by S.USERNAME" . $sDirection;
  }

  // Build full SQL statement
  
  $sSQL = "select S.USERID as S_USERID, " . 
    "S.USERNAME as S_USERNAME " . 
    " from S_USERS S ";
  
  $sSQL .= $sWhere . $sOrder;
  $tpl->set_var("FormAction", "USERS.php");
  $tpl->set_var("SortParams", $sSortParams);

  // Execute SQL statement
  $db->query($sSQL);
  
  // Select current page
  $iPage = get_param("FormS_USERS_Page");
  if(!strlen($iPage)) $iPage = 1;
  $RecordsPerPage = 10;
  if(($iPage - 1) * $RecordsPerPage != 0)
    $db->seek(($iPage - 1) * $RecordsPerPage);
  $iCounter = 0;

  if($db->next_record())
  {  
    // Show main table based on SQL query
    do
    {
      $fldUSERID = $db->f("S_USERID");
      $fldUSERNAME = $db->f("S_USERNAME");
    $tpl->set_var("USERID", tohtml($fldUSERID));
      $tpl->set_var("USERNAME", tohtml($fldUSERNAME));
      $tpl->set_var("USERNAME_URLLink", "USERS.php");
      $tpl->set_var("Prm_USERID", tourl($db->f("S_USERID"))); 
      $tpl->parse("DListS_USERS", true);
      $iCounter++;
    } while($iCounter < $RecordsPerPage &&$db->next_record());
  }
  else
  {
    // No Records in DB
    $tpl->set_var("DListS_USERS", "");
    $tpl->parse("S_USERSNoRecords", false);
    $tpl->set_var("S_USERSScroller", "");
    $tpl->parse("FormS_USERS", false);
    return;
  }
  
  // Parse scroller
  if(@$db->next_record())
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_USERSScrollerPrevSwitch", "_");
    }
    else
    {
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_USERSScrollerPrevSwitch", "");
    }
    $tpl->set_var("NextPage", ($iPage + 1));
    $tpl->set_var("S_USERSScrollerNextSwitch", "");
    $tpl->set_var("S_USERSCurrentPage", $iPage);
    $tpl->parse("S_USERSScroller", false);
  }
  else
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_USERSScroller", "");
    }
    else
    {
      $tpl->set_var("S_USERSScrollerNextSwitch", "_");
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_USERSScrollerPrevSwitch", "");
      $tpl->set_var("S_USERSCurrentPage", $iPage);
      $tpl->parse("S_USERSScroller", false);
    }
  }
  $tpl->set_var("S_USERSNoRecords", "");
  $tpl->parse("FormS_USERS", false);
  
}



function S_USERS1_action($sAction)
{
  global $db;
  global $tpl;
  global $sForm;
  global $sS_USERS1Err;
  
  $sParams = "";
  $sActionFileName = "USERS.php";

  

  $sWhere = "";
  $bErr = false;

  if($sAction == "cancel")
    header("Location: " . $sActionFileName); 

  
  // Create WHERE statement
  if($sAction == "update" || $sAction == "delete") 
  {
    $pPKUSERID = get_param("PK_USERID");
    if( !strlen($pPKUSERID)) return;
    $sWhere = "USERID=" . tosql($pPKUSERID, "Text");
  }

  // Load all form fields into variables
  
  $fldUSERNAME = get_param("USERNAME");
  $fldPASSWD = get_param("PASSWD");
  $fldIS_PUBLIC = get_checkbox_value(get_param("IS_PUBLIC"), "1", "0", "Number");
  $fldSECURITY_LEVEL = get_param("SECURITY_LEVEL");
  // Validate fields
  if($sAction == "insert" || $sAction == "update") 
  {
    if(!is_number($fldSECURITY_LEVEL))
      $sS_USERS1Err .= "The value in field Security Level is incorrect.<br>";
    if(!strlen($fldUSERNAME))
      $sS_USERS1Err .= "No User Name set! Please correct.<br>";
    if(!strlen($fldPASSWD))
      $sS_USERS1Err .= "No Password set! Please correct.<br>";

    if(strlen($sS_USERS1Err)) return;
  }
  

  $sSQL = "";
  // Create SQL statement
  
  switch(strtolower($sAction)) 
  {
    case "insert":
      
        $sSQL = "insert into S_USERS (" . 
          "USERNAME," . 
          "PASSWD," . 
          "IS_PUBLIC," . 
          "SECURITY_LEVEL)" . 
          " values (" . 
          tosql($fldUSERNAME, "Text") . "," .
          tosql($fldPASSWD, "Text") . "," .
          $fldIS_PUBLIC . "," .
          tosql($fldSECURITY_LEVEL, "Number") . ")";    
    break;
    case "update":
      
        $sSQL = "update S_USERS set " .
          "USERNAME=" . tosql($fldUSERNAME, "Text") .
          ",PASSWD=" . tosql($fldPASSWD, "Text") .
          ",IS_PUBLIC=" . $fldIS_PUBLIC .
          ",SECURITY_LEVEL=" . tosql($fldSECURITY_LEVEL, "Number");
        $sSQL .= " where " . $sWhere;
    break;
    case "delete":
      
        $sSQL = "delete from S_USERS where " . $sWhere;
    break;
  }
  // Execute SQL statement
  if(strlen($sS_USERS1Err)) return;
  $db->query($sSQL);
  
  header("Location: " . $sActionFileName);
  
}

function S_USERS1_show()
{
  global $db;
  global $tpl;
  global $sAction;
  global $sForm;
  global $sS_USERS1Err;

  $sWhere = "";
  
  $bPK = true;
  $fldUSERNAME = "";
  $fldPASSWD = "";
  $fldIS_PUBLIC = "";
  $fldUSERID = "";
  $fldSECURITY_LEVEL = "";
  

  if($sS_USERS1Err == "")
  {
    // Load primary key and form parameters
    $fldUSERID = get_param("USERID");
    $pUSERID = get_param("USERID");
    $tpl->set_var("S_USERS1Error", "");
  }
  else
  {
    // Load primary key, form parameters and form fields
    $fldUSERNAME = strip(get_param("USERNAME"));
    $fldPASSWD = strip(get_param("PASSWD"));
    $fldIS_PUBLIC = strip(get_param("IS_PUBLIC"));
    $fldUSERID = strip(get_param("USERID"));
    $fldSECURITY_LEVEL = strip(get_param("SECURITY_LEVEL"));
    $pUSERID = get_param("PK_USERID");
    $tpl->set_var("sS_USERS1Err", $sS_USERS1Err);
    $tpl->parse("S_USERS1Error", false);
  }

  
  if( !strlen($pUSERID)) $bPK = false;
  
  $sWhere .= "USERID=" . tosql($pUSERID, "Text");
  $tpl->set_var("PK_USERID", $pUSERID);

  $sSQL = "select * from S_USERS where " . $sWhere;

  

  if($bPK && !($sAction == "insert" && $sForm == "S_USERS1"))
  {
    // Execute SQL statement
    $db->query($sSQL);
    $db->next_record();
    
    $fldUSERID = $db->f("USERID");
    if($sS_USERS1Err == "") 
    {
      // Load data from recordset when form displayed first time
      $fldUSERNAME = $db->f("USERNAME");
      $fldPASSWD = $db->f("PASSWD");
      $fldIS_PUBLIC = $db->f("IS_PUBLIC");
      $fldSECURITY_LEVEL = $db->f("SECURITY_LEVEL");
    }
    $tpl->set_var("S_USERS1Insert", "");
    $tpl->parse("S_USERS1Edit", false);
  }
  else
  {
    if($sS_USERS1Err == "")
    {
      $fldUSERID = tohtml(get_param("USERID"));
      $fldSECURITY_LEVEL= "0";
    }
    $tpl->set_var("S_USERS1Edit", "");
    $tpl->parse("S_USERS1Insert", false);
  }
  $tpl->parse("S_USERS1Cancel", false);

  // Show form field
  
    $tpl->set_var("USERNAME", tohtml($fldUSERNAME));
    $tpl->set_var("PASSWD", tohtml($fldPASSWD));
      if(strtolower($fldIS_PUBLIC) == strtolower("1")) 
        $tpl->set_var("IS_PUBLIC_CHECKED", "CHECKED");
      else
        $tpl->set_var("IS_PUBLIC_CHECKED", "");

    $tpl->set_var("USERID", tohtml($fldUSERID));
    $tpl->set_var("LBSECURITY_LEVEL", "");
    $tpl->set_var("ID", "");
    $tpl->set_var("Value", "");
    $tpl->parse("LBSECURITY_LEVEL", true);
    //$LOV = split(";", "0;High;3;Low");
    $LOV = split(";","0;User;3;Admin");
  
    if(sizeof($LOV)%2 != 0) 
      $array_length = sizeof($LOV) - 1;
    else
      $array_length = sizeof($LOV);
    reset($LOV);
    for($i = 0; $i < $array_length; $i = $i + 2)
    {
      $tpl->set_var("ID", $LOV[$i]);
      $tpl->set_var("Value", $LOV[$i + 1]);
      if($LOV[$i] == $fldSECURITY_LEVEL) 
        $tpl->set_var("Selected", "SELECTED");
      else
        $tpl->set_var("Selected", "");
      $tpl->parse("LBSECURITY_LEVEL", true);
    }
  $tpl->parse("FormS_USERS1", false);
  

}

?>
