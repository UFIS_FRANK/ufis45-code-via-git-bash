<?php 
	// 20060621 JIM: PRF 8203: correct sorting via "ORDER BY" instead 
	//                         of sortColumn(). Note: To avoid sorting
	//                         by tooltips ("New Dehli" instead of "DEL")
	//                         before the Tooltip a HTML Comment has to be
	//                         added ('<!--'||APC3||'--><font tooltip=...')

	

	
	include("include/manager.php");
	include("$ADODB_RootPath/list.php"); 
	include("include/template.php"); 
	include("include/class.overlib/class.overlib.php3"); 


	include("include/class.LoginInit.php"); 

	/* Login Check */
	/* Set Login Instance*/
	date_default_timezone_set('Asia/Calcutta');

	$login = new  LoginInit;
	$tpl = new Template($tmpl_path);

	// Over lib 
	$ol = new Overlib;
	$ol->ol_backcolor = $maskbgcolor;
	$overlib=$ol->init();
//20060710 JIM: PRF 8204: excel export: set default
	if (!isset($Export)) 
	{
		$Export="No";
	}
	
			 
	//20070221 GFO: WAW Project : Added $LangTR for the Second Language Support
	/*
	if (!isset($Lang)) {
		if (!isset($Session["Lang"])) { 
			 $Session["Lang"] = "";
		}
	} else {
		if ($Lang != "ENG" && len($Lang)>0) {
			 $Session["Lang"] =  "_TR";
		} 
		if ($Lang == "ENG") {
			 $Session["Lang"] = "";
		}
	}
	
	$LangTR = $Session["Lang"];
	*/
	 //20070221 GFO: WAW Project : Added $LangTR for the Second Language Support^M
        if (!isset($Lang)) {
//                      $LangTR = "";   
                if (!isset($Session["Lang_TR"])) {
                        $Session["Lang_TR"]="";
                }
        } else {
                if ($Lang != "ENG" && len($Lang)>0) {
                        $Session["Lang_TR"]="_TR";
                }       else {
                        $Session["Lang_TR"]="";
                }
        }
        
        $LangTR = $Session["Lang_TR"];

		$apfn = "apfn";
		$beme = "beme";
		
		if ($LangTR=="_TR") {
			$apfn = "apsn";	
			$beme = "bemd";
		}
	
	if (isset($Session["userid"])) { 
		// The user has already been Logged
//20060710 JIM: PRF 8204: excel export: added $Export
//		$Query=$login->Tmpl($Session["userid"]);
		$Query=$login->Tmpl($Session["userid"],$Export);
	    $tpl->set_var("DateTime", strftime("created at %d.%m.%Y, %H:%M",time()) );
	} else {
//20060710 JIM: PRF 8204: excel export: added $Export
//		$Query=	$login->Login($UserName,$Password);
		$Query=	$login->Login($UserName,$Password,$Export);
	}

	//$debug = true;
	/* Start Debug  if requested */
	if ($debug=="true")
	{
		$tmpdebug="?debug=true";
		$db->debug=$debug;
	}
	else
	{
		$debug="false";
	}			 
	//$debug=true;
	//$db->debug=$debug;
			 
			 
	
			 
			 
	/* Init the time-diff. variables */
	FindTDI();

	// Expires the Page
	$Cexpires=date("D, d M Y H:i:s ",AddDiff("S",$Refresh,time()))." GMT";

	/* setting a default of Main and codeshare search if not set using the search mask */
	if (empty($SearchType))
		$SearchType="MAC";

	$SearchFlno="";
	// Building SearchFlno variable if main-carrier shall be searched
	if ($SearchType=="MC" || $SearchType=="MAC")
	{
		if  (len($airline)>0)
		{
			$airline=strtoupper($airline);
			$SearchFlno=$airline;
		}
		if (len($flightnumber)>0)
		{
				if (len($airline)>2) {
					$SearchFlno=$airline."%".$flightnumber;
				}
				if (len($airline)<=2) {
					$SearchFlno=$airline." "."%".$flightnumber;
				}
				if (len($airline)==0) {
					$SearchFlno=$flightnumber;
				}
		}
	}
	else
	{
		// on empty SearchType the user requests everything
		if (!empty($SearchType))
			$SearchFlno="<not used>";
	}

	// Building SearchJfno variable if codeshares shall be searched
	$SearchJfno="";
	if ($SearchType=="CS" || $SearchType=="MAC")
	{
		if (len($airline)>0)
		{
			$airline=strtoupper($airline);
			$SearchJfno=$airline;
		}
		if (len($flightnumber)>0)
		{
				if (len($airline)>2) {
					$SearchJfno=$airline."%".$flightnumber;
				}
				if (len($airline)<=2) {
					$SearchJfno=$airline." "."%".$flightnumber;
				}
				if (len($airline)==0) {
					$SearchJfno=$flightnumber;
				}
		}
	}
	else
	{
		// on empty SearchType the user requests everything
		if (!empty($SearchType))
			$SearchJfno="<not used>";
	}
	// ##################################################################
	// ### starting to check if times have been set using the search mask
	// ##################################################################

// 20060307 JIM: PRF 8201: BEGIN Step 1 Timeframe settings
	if (! $TfVal1_curr)
	{
		$TfVal1_curr=$TfVal1 / 60 ; 
	}
	if (! $TfVal2_curr)
	{
		$TfVal2_curr=$TfVal2 / 60 ; 
	}
	if ($TfVal1_curr)
	{
		$TfVal1=$TfVal1_curr * 60; 
	}
	if ($TfVal2_curr)
	{
		$TfVal2=$TfVal2_curr * 60; 
	}
// 20060307 JIM: PRF 8201: END Step 1 Timeframe settings

	$TfVal1=time(0) + ($TfVal1*60);
	$TfVal1Month=date("m",$TfVal1);
	$TfVal1Year=date("Y",$TfVal1);
	$TfVal1Day=date("d",$TfVal1);
	$TfVal1Hour=date("H",$TfVal1);
	$TfVal1Min=date("i",$TfVal1);

//	$TfVal1=date("YmdHis",$TfVal1);

// 20070314 GFO: Solving the UTC Problem for the select statement	
	$TfVal1=gmdate("YmdHis",$TfVal1);
	
	$TfVal2=time(0) + ($TfVal2*60);
	$TfVal2Month=date("m",$TfVal2);
	$TfVal2Year=date("Y",$TfVal2);
	$TfVal2Day=date("d",$TfVal2);
	$TfVal2Hour=date("H",$TfVal2);
	$TfVal2Min=date("i",$TfVal2);

//	$TfVal2=date("YmdHis",$TfVal2);
// 20070314 GFO: Solving the UTC Problem for the select statement 
	$TfVal2=gmdate("YmdHis",$TfVal2);
	
	// Setting search mask defaults on todays date at 00:00
	$SearchstartdateMonth=date("m",time(0));
	$SearchstartdateYear=date("Y",time(0));
	$SearchstartdateDay=date("d",time(0));
	$SearchstartdateHour="00";
	$SearchstartdateMin="00";

	//echo "Search:(".$SEARCHUSED.")<br>";
	//echo "TfVal1:(".$TfVal1.")<br>";
	//echo "TfVal2:(".$TfVal2.")<br>";
	// Start Header
	// $start = $tpl->utime();

	/* We Must reverse the values of the array in order to pass  the results  
	to the parse functions in the correct order */
	
	$tmpl_array=array_reverse ($tmpl_array);
	// values for HTTP header
	if ($Export=="Export")
	{
	// GFO 20070313: Remove the Images from the datavalue 
		$PageTitle=eregi_replace('<[^>]*img*\"?[^>]*>', "",$PageTitle);
	}	

	// values for HTTP header
	$tpl->set_var("PageTitle", $PageTitle );
	$tpl->set_var("STYLESHEET", $StyleSheet );
	$tpl->set_var("Refresh",$RefreshRate);
	$tpl->set_var("Cexpires",$Cexpires );

	// values for div search mask
	$tpl->set_var("SearchUsed",$SearchUsed);
	$tpl->set_var("SearchstartdateMonth",$SearchstartdateMonth);
	$tpl->set_var("SearchstartdateYear",$SearchstartdateYear);
	$tpl->set_var("SearchstartdateDay",$SearchstartdateDay);
	$tpl->set_var("SearchstartdateHour",$SearchstartdateHour);
	$tpl->set_var("SearchstartdateMin",$SearchstartdateMin);
	$tpl->set_var("SearchenddateMonth",$SearchenddateMonth);
	$tpl->set_var("SearchenddateYear",$SearchenddateYear);
	$tpl->set_var("SearchenddateDay",$SearchenddateDay);
	$tpl->set_var("SearchenddateHour",$SearchenddateHour);
	$tpl->set_var("SearchenddateMin",$SearchenddateMin);
	// values for div search mask
	$tpl->set_var("overlib",$overlib);
	$tpl->set_var("alfn",$alfn );	
	$tpl->set_var("alfnspaces",$alfnspaces );			
	$tpl->set_var("apfn",$apfn );	
	$tpl->set_var("apfnspaces",$apfnspaces );				
	//$tpl->set_var("FromDateHeader",$FromDateHeader );			
	//$tpl->set_var("ToDateHeader",$ToDateHeader );			
	$tpl->set_var("dateerror",$dateerror );			
	$tpl->set_var("type",$type );					
	$tpl->set_var("TMPL_MENUID",$TMPL_MENUID );					
	$tpl->set_var("TfVal1",$TfVal1);
// 20060307 JIM: PRF 8201: Step 2 Timeframe settings
	$tpl->set_var("TfVal1_curr",$TfVal1_curr);
	$tpl->set_var("TfVal1Month",$TfVal1Month );
	$tpl->set_var("TfVal1Year",$TfVal1Year );
	$tpl->set_var("TfVal1Day",$TfVal1Day );
	$tpl->set_var("TfVal1Hour",$TfVal1Hour );
	$tpl->set_var("TfVal1Min",$TfVal1Min );
	$tpl->set_var("TfVal2",$TfVal2);
// 20060307 JIM: PRF 8201: Step 3 Timeframe settings
	$tpl->set_var("TfVal2_curr",$TfVal2_curr);
	$tpl->set_var("TfVal2Month",$TfVal2Month );
	$tpl->set_var("TfVal2Year",$TfVal2Year );
	$tpl->set_var("TfVal2Day",$TfVal2Day );
	$tpl->set_var("TfVal2Hour",$TfVal2Hour );
	$tpl->set_var("TfVal2Min",$TfVal2Min );	
	$tpl->set_var("OrgDes",$OrgDes );	
	$airline=strtoupper($airline);
	$tpl->set_var("airline",$airline );	
	//$tpl->set_var("flno",$flno );		
	$flightnumber=strtoupper($flightnumber);
	$tpl->set_var("flightnumber",$flightnumber);		
	$tpl->set_var("SearchFlno",$SearchFlno);		
	$tpl->set_var("SearchJfno",$SearchJfno);		
	$city=strtoupper($city);
	$tpl->set_var("city",$city );		
	$tpl->set_var("PHP_SELF",$PHP_SELF );		
	$tpl->set_var("debug",$debug);		
	// 20060621 JIM: PRF 8203: Step No 1: correct sorting 
	$tpl->set_var("OrderBy",$OrderBy);		
	
	$tpl->set_var("DynamicSearch","");
	
	// End Header

	// We assign the values to the TDplus Select 
	$i = -36;
	while ($i <=36) {
		$tmps="";
		// if ($i == $todateplus) {
		// default selected entry of the std_search-window
		if ($i == 24) {
			$tmps= " selected";
		}
		if ($i<0)
			$tpl->set_var( "todateplus","<option value=\"$i\" $tmps>-$i" );
		else
			$tpl->set_var( "todateplus","<option value=\"$i\" $tmps>+$i" );
		$tpl->parse("TDplus", true);
		$i++;
	}
//	echo "OrderBy: #$OrderBy#<br>";
//	echo "NewOrderBy: #$NewOrderBy#<br>";
	// 20060621 JIM: PRF 8203: BEGIN Step 2: correct sorting 
	if ($NewOrderBy)
	{
		if ($OrderBy)
		{
			if ($OrderBy==$NewOrderBy)
			{
				$OrderBy=$OrderBy."_desc";
			}
			else
			{
				$OrderBy=$NewOrderBy;
			}
		}
		else
		{
			$OrderBy=$NewOrderBy;
		}
	}
	// 20060621 JIM: PRF 8203: END Step 2: correct sorting 
	$login->FldDSrows ($FldArray);	
	if ($debug == "true" )
	{
		echo "inf1.php: QUERY:$Query<br>";
		echo "<br>";
		echo "inf1.php: EQUERY:$ExtraQuery<br>";
		echo "inf1.php: PHP_SELF:$PHP_SELF<br>";
		echo "inf1.php: OrderBy:$OrderBy<br>";
		echo "inf1.php: TfVal1: $TfVal1<br>";
	}

	// Start Results
	//if ($debug == "true" )
	//{
	//	echo "inf1.php: QUERY:$Query<br>";
	//	echo "<br>";
	//	echo "inf1.php: EQUERY:$ExtraQuery<br>";
	//}

	// 20060621 JIM: PRF 8203: moved 'strupper' down to avoid uppercase PHP fieldnames
	//$Query=strtoupper($Query);
	$Query=ereg_replace("\"","'",$Query);
	
	// GFO 20070313: the Query may contain lower letter , search for the ORDER BY in UPPER mode 
	$pos=strpos(strtoupper($Query)," ORDER BY" );
	// 20060621 JIM: PRF 8203: BEGIN Step 3: correct sorting 
	$tmpOrder="";
	if ($OrderBy && $OrderBy!="")
	{
		$tmpOrder=$OrderBy.",";
		if (($ppp=strpos($OrderBy,"_desc"))>0)
		{
			$tmpOrder=substr($OrderBy,0,$ppp)." desc,";
		}
	}
	// 20060621 JIM: PRF 8203: END Step 3: correct sorting 

	if  ( $pos>0)  {
// 20060621 JIM: PRF 8203: Step 4: correct sorting:
//		$Query=substr ($Query,0,$pos).$ExtraQuery.substr ($Query,$pos, len($Query));
		$Query=substr ($Query,0,$pos).$ExtraQuery.substr ($Query,$pos, 10).$tmpOrder. substr ($Query,$pos+10,len($Query));
	} else {
		$Query.=$ExtraQuery;
		if ($tmpOrder!="")
		{
// 20060621 JIM: PRF 8203: Step 5: correct sorting:
			$Query.=$ExtraQuery." ORDER BY ".substr($tmpOrder,0,len($tmpOrder)-1) ;
		}
	}				
//		echo "inf1.php: QUERY:$Query<br>";
//		echo "<br>";

	if (len($Query)>0) {
		eval("\$Query = \"$Query\";");
		// 20060621 JIM: PRF 8203: moved 'strupper' to here to avoid uppercase PHP fieldnames
		// 20060727 JIM: PRF 8207: use 'strupper' only behind FROM to avoid uppercase JPG file names
		$Query=ereg_replace(" from "," FROM ",$Query);
		$Wpos=strpos ($Query," FROM " );
		// 20070314 GFO:  strupper was causing wrong slect statements
		$Query=substr($Query,0,$Wpos).substr($Query,$Wpos);

		// Show the Results
//		$login->QFld($Query,$WhereP);
//20060710 JIM: PRF 8204: excel export: added $Export
//		$login->QFld($Query,stristr($Query," FROM "));
		$login->QFld($Query,stristr($Query," FROM "),$Export);
	}
	// End  Results

// 20060727 JIM: PRF 8202: Begin: set focus to first search field of Dynamic Search Box
// 20070905 GFO: BugFix : Ww had a JS error when the FirstSearchField had 0 len
if (len($FirstSearchField)>0) {
$SetMyFocus="<script language=\"JavaScript\"><!--
function SetMyFocus() {
      document.action_form.".$FirstSearchField.".focus();
}
-->
</script>
";
} else {
	$SetMyFocus="";
}

$tpl->set_var("SetFocus", $SetMyFocus);
// 20060727 JIM: PRF 8202: End: set focus to first search field Dynamic Search Box

	// Start Footer
	$tpl->pparse($menuname,false);			
	// End Footer
	exit;
?>
