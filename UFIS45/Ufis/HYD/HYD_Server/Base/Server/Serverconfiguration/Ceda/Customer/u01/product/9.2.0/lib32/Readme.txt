This Version of CEDA is linked against Oracle 9.2.0 libraries. To use
this CEDA Version with Oracle 10g login to the server as root,
create the directory /u01/product/9.2.0/lib32/ and copy the files 
libclntsh.sl.9.0 and libwtc9.sl to that directory:

  mkdir -p /u01/product/9.2.0/lib32/
  cp libclntsh.sl.9.0 libwtc9.sl /u01/product/9.2.0/lib32/

A later version will be linked against Oracle 10g libraries.

