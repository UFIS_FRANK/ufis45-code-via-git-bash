rem Copies all generated files from the SCM-Structue into the Install-Shield directory.


rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:

rem Copy ini- and config-files
copy c:\ufis_bin\*.ini %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.cfg %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\hyd_build\ufis_client_appl\system\*.*

rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\hyd_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\hyd_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\hyd_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\hyd_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\hyd_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\ucdialog.exe %drive%\hyd_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\alerter.exe %drive%\hyd_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fidas.exe %drive%\hyd_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\hyd_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\hyd_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\importflights.exe %drive%\hyd_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\flightdatagenerator.exe %drive%\hyd_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fips_reports.exe %drive%\hyd_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\data_changes.exe %drive%\hyd_build\ufis_client_appl\applications\*.*

REM copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\trafficlight.ocx %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\trafficlight.tlb %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucedit.ocx %drive%\hyd_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucedit.tlb %drive%\hyd_build\ufis_client_appl\system\*.*

rem copy interop dll's
copy c:\ufis_bin\release\*lib.dll %drive%\hyd_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\ufis.utils.dll %drive%\hyd_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\usercontrols.dll %drive%\hyd_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\zedgraph.dll %drive%\hyd_build\ufis_client_appl\applications\interopdll\*.*

rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\hyd_build\ufis_client_appl\help\*.*

rem Copy ReleaseNotes
rem copy c:\ufis_bin\releasenotes.zip %drive%\hyd_build\ufis_client_appl\rel_notes\*.*
