rem Copies all generated files from the SCM-Structue into the Install-Shield directory.


rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:

rem Copy ini- and config-files
copy c:\ufis_bin\ceda.ini %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\bdps_sec.cfg %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\loatab*.* %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\telexpool.ini %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\importtool.ini %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\datachange.ini %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\lis_build\ufis_client_appl\system\*.*

rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\lis_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\lis_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\lis_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\lis_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\lis_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fidas.exe %drive%\lis_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\lis_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\lis_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\importflights.exe %drive%\lis_build\ufis_client_appl\applications\*.*

REM copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.exe %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.tlb %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\lis_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\lis_build\ufis_client_appl\system\*.*


copy c:\ufis_bin\release\data_changes.exe %drive%\lis_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fips_reports.exe %drive%\lis_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\Ufis.Utils.dll %drive%\lis_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\UserControls.dll %drive%\lis_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\ZedGraph.dll %drive%\lis_build\ufis_client_appl\applications\interopdll\*.*

rem copy interop dll's
copy c:\ufis_bin\release\*lib.dll %drive%\lis_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\ufis.utils.dll %drive%\lis_build\ufis_client_appl\applications\interopdll\*.*

rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\lis_build\ufis_client_appl\help\*.*


rem Copy ReleaseNotes
rem copy c:\ufis_bin\releasenotes.zip %drive%\lis_build\ufis_client_appl\rel_notes\*.*
