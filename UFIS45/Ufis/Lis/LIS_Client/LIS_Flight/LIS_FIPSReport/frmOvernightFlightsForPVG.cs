using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmOvernightFlightsForPVG.
	/// </summary>
	public class frmOvernightFlightsForPVG : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		
		private string strWhereRawA =  "((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))";

		private string strWhereRawD =  "((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))))" ;

		private string strWhereRawB = string.Format("({0} OR {1})","((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))","((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))))");
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes ==> for Rotation
		/// </summary>
		
		//DB-Fields
		// 10 , 10 , 9  ,  8 , 1  , 1  , 14 , 1  ,  5 , 1  , 2  , 3  , 4  , 3  , 4  , 2  , 5  , 5  , 5  , 5  , 5  , 1  , 3  , 14 , 1  , 3  , 4  , 5  ,  5 , 5  , 5  , 5
		//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT"
		private string strLogicalFieldsRotation = "Arrival_A,Departure_D,REGN,Day_A,ETA,ATA,Day_D,ETD,ATD,PST_A,PST_D,REMA";
		/// The header columns, which must be used for the report output
		/// for rotation
		/// </summary>
		private string strTabHeaderRotation = "Arrival,Departure,Registration,Arrival day,ETA,ATA,Departure day,ETD,ATD,Arr.  position,Dep.  position,Remark";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// for rotation
		/// </summary>
		private string strTabHeaderLensRotation = "70,70,80,100,50,50,100,50,50,100,100,100";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		/// <summary>
		/// variable to store the arrival and departure flights
		/// </summary>
		private int[] NumberofFlights;

		private int imTotalFlights = 0;
		#endregion _My Members

		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtDuration;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtLens;
		private System.Windows.Forms.Button button1;
		private UserControls.ucView ucView1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmOvernightFlightsForPVG()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			myDB = UT.GetMemDB();
			this.ucView1.Report = "OvernightFlights";
			this.ucView1.viewEvent +=new UserControls.ucView.ViewChangedEvent(ucView1_viewEvent);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmOvernightFlightsForPVG));
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			this.txtLens = new System.Windows.Forms.TextBox();
			this.dtDuration = new System.Windows.Forms.DateTimePicker();
			this.label5 = new System.Windows.Forms.Label();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.ucView1 = new UserControls.ucView();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.panelTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 152);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(780, 430);
			this.panelBody.TabIndex = 3;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(780, 414);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(780, 414);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(780, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.ucView1);
			this.panelTop.Controls.Add(this.button1);
			this.panelTop.Controls.Add(this.txtLens);
			this.panelTop.Controls.Add(this.dtDuration);
			this.panelTop.Controls.Add(this.label5);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(780, 152);
			this.panelTop.TabIndex = 2;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(224)), ((System.Byte)(192)));
			this.button1.Location = new System.Drawing.Point(456, 48);
			this.button1.Name = "button1";
			this.button1.TabIndex = 20;
			this.button1.Text = "button1";
			this.button1.Visible = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtLens
			// 
			this.txtLens.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(224)), ((System.Byte)(192)));
			this.txtLens.Location = new System.Drawing.Point(276, 72);
			this.txtLens.Name = "txtLens";
			this.txtLens.Size = new System.Drawing.Size(488, 20);
			this.txtLens.TabIndex = 19;
			this.txtLens.Text = "textBox1";
			this.txtLens.Visible = false;
			// 
			// dtDuration
			// 
			this.dtDuration.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtDuration.Location = new System.Drawing.Point(80, 68);
			this.dtDuration.Name = "dtDuration";
			this.dtDuration.ShowUpDown = true;
			this.dtDuration.Size = new System.Drawing.Size(128, 20);
			this.dtDuration.TabIndex = 2;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(12, 72);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 16);
			this.label5.TabIndex = 18;
			this.label5.Text = "Duration >=";
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(412, 96);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 12;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(412, 112);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 10;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 112);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 8;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 112);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 7;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 112);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 9;
			this.btnCancel.Text = "&Close";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 112);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 6;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// ucView1
			// 
			this.ucView1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucView1.Location = new System.Drawing.Point(12, 12);
			this.ucView1.Name = "ucView1";
			this.ucView1.Report = "";
			this.ucView1.Size = new System.Drawing.Size(304, 40);
			this.ucView1.TabIndex = 21;
			// 
			// frmOvernightFlightsForPVG
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(780, 582);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmOvernightFlightsForPVG";
			this.Text = "Overnight Flights ...";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmOvernightFlightsForPVG_Closing);
			this.Load += new System.EventHandler(this.frmOvernightFlightsForPVG_Load);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.panelTop.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
		
		/// <summary>
		/// Loads the from and inits the necessary things.
		/// </summary>
		/// <param name="sender">Originator of the method call</param>
		/// <param name="e">Event args</param>
		private void frmOvernightFlightsForPVG_Load(object sender, System.EventArgs e)
		{
			InitTimePickers();
			InitTab();
			PrepareFilterData();
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olDura;
			DateTime olFrom = DateTime.Now;
			olDura = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 3,0,0);
			dtDuration.Value = olDura;
			dtDuration.CustomFormat = "HH:mm";
		}

		/// <summary>
		/// Initializes the tab control.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString = strTabHeaderRotation;
			tabResult.LogicalFieldList = strLogicalFieldsRotation;
			tabResult.HeaderLengthString = strTabHeaderLensRotation;
			PrepareReportDataRotations();
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
		}

		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			InitTab();
			LoadReportData();
			PrepareReportDataRotations();
			//RunReport();
			this.Cursor = Cursors.Arrow;
			txtLens.Text = tabResult.HeaderLengthString;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			if (this.ucView1.Viewer.CurrentView == "")
			{
				strRet += ilErrorCount.ToString() +  ". A view must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.PeriodFrom >= this.ucView1.Viewer.PeriodTo)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Arrival == false)
			{
				strRet += ilErrorCount.ToString() +  ". Arrival must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Departure == false)
			{
				strRet += ilErrorCount.ToString() +  ". Departure must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Rotation == false)
			{
				strRet += ilErrorCount.ToString() +  ". Rotation must be selected!\n";
				ilErrorCount++;
			}

			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere;
			//Clear the tab
			tabResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			string strFtyp = this.ucView1.Viewer.FlightTypes;
			datFrom = this.ucView1.Viewer.PeriodFrom;
			datTo   = this.ucView1.Viewer.PeriodTo;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(this.ucView1.Viewer.PeriodFrom);
				datTo   = UT.LocalToUtc(this.ucView1.Viewer.PeriodTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodFrom);
				strDateTo = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodTo);
			}

			//Now reading day by day the flight records in order to 
			//not stress the database too much
			myDB.Unbind("AFT");
			// 10 , 10 , 9  ,  8 , 1  , 1  , 14 , 1  ,  5 , 1  , 2  , 3  , 4  , 3  , 4  , 2  , 5  , 5  , 5  , 5  , 5  , 1  , 3  , 14 , 1  , 3  , 4  , 5  ,  5 , 5  , 5  , 5
			//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT
			myAFT = myDB.Bind("AFT", "AFT", 
				"RKEY,URNO,ADID,FLNO,CSGN,FLTI,JCNT,STOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT,ETAI,ETDI,REM1,REGN", 
				"10,10,1,9,8,1,1,14,14,1,5,1,2,3,4,3,4,2,5,5,5,5,5,1,3,14,14,1,3,4,5,5,5,5,5,14,14,256,12",
				"RKEY,URNO,ADID,FLNO,CSGN,FLTI,JCNT,STOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT,ETAI,ETDI,REM1,REGN");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL,ETAI,ETDI";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 0;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);

				strTmpWhere = "WHERE ";
				// check on code share enabled
				if (this.ucView1.Viewer.IncludeCodeShare)
				{
					string strTmpWhere1 = "";
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere1 = strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawD;
					strTmpWhere1+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere1+= this.ucView1.Viewer.GeneralFilter;

					string strTmpWhere2 = "";
					//patch the where statement according to the user's entry
					strTmpWhere2+= this.ucView1.Viewer.AdditionalSelection(1);

					strTmpWhere = string.Format("WHERE ({0}) AND ({1})",strTmpWhere1,strTmpWhere2);
				}
				else
				{
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere += strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere += strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere += strWhereRawD;
					strTmpWhere+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere+= this.ucView1.Viewer.GeneralFilter;

				}

				//patch the where statement according to the user's entry
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );

				if (this.ucView1.Viewer.Rotation)
				{
					strTmpWhere += "[ROTATIONS]";
					if (strFtyp.Length > 0)
						strTmpWhere += string.Format("[FILTER=FTYP IN ({0})]",strFtyp);
				}

				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();

			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

		}

		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// First organize the rotations and then put the result
		/// into the tabResult
		/// </summary>
		private void PrepareReportDataRotations()
		{
			NumberofFlights = new int [4];
			if(myAFT == null)
				return;

			myAFT.Sort("RKEY", true);
			int llCurr = 0;
			int ilStep = 1;
			string strCurrAdid = "";
			string strNextAdid = "";
			string strCurrRkey = "";
			string strNextRkey = "";
			string strTime = "";
			DateTime tmpDate;
			imArrivals = 0;
			imDepartures =0 ;
			imRotations = 0;
			imTotalFlights = 0;
			StringBuilder sb = new StringBuilder(10000);
			while(llCurr < myAFT.Count)
			{
				string strValues = "";
				strCurrRkey = myAFT[llCurr]["RKEY"];
				strCurrAdid = myAFT[llCurr]["ADID"];
				if ((llCurr + 1) < myAFT.Count)
				{
					strNextRkey = myAFT[llCurr+1]["RKEY"];
					strNextAdid = myAFT[llCurr+1]["ADID"];
				}
				else
				{
					strNextRkey = "";
					strNextAdid = "";
				}

				if(strCurrRkey == strNextRkey)
				{//Is a rotation
					ilStep = 2;
					if( strCurrAdid == "A" && strNextAdid == "D")
					{
						DateTime timeARR = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOA"]);
						DateTime timeDEP = UT.CedaFullDateToDateTime(myAFT[llCurr+1]["STOD"]);
						strTime = myAFT[llCurr]["TIFA"];
						if(strTime != "")
							timeARR = UT.CedaFullDateToDateTime(strTime);
						strTime = myAFT[llCurr+1]["TIFD"];
						if(strTime != "")
							timeDEP = UT.CedaFullDateToDateTime(strTime);
						int ilDura = (dtDuration.Value.Hour * 60) + dtDuration.Value.Minute;
						TimeSpan ts = timeDEP - timeARR;
						if( (ts.TotalMinutes >= ilDura) && timeARR.Day != timeDEP.Day )
						{
							NumberofFlights[0]++;
							NumberofFlights[1]++;

							strValues += myAFT[llCurr]["FLNO"] + ",";
							strValues += myAFT[llCurr+1]["FLNO"] + ",";
							strValues += myAFT[llCurr]["REGN"] + ",";
							strValues += Helper.DateString(myAFT[llCurr]["STOA"], "dd'/'MM") + ",";
							strValues += Helper.DateString(myAFT[llCurr]["ETAI"], "HH:mm") + ",";
							strValues += Helper.DateString(myAFT[llCurr]["TIFA"], "HH:mm") + ",";
							strValues += Helper.DateString(myAFT[llCurr+1]["STOD"], "dd'/'MM") + ",";
							strValues += Helper.DateString(myAFT[llCurr+1]["STOD"],myAFT[llCurr+1]["ETDI"], "HH:mm") + ",";
							strValues += Helper.DateString(myAFT[llCurr+1]["STOD"],myAFT[llCurr+1]["TIFD"], "HH:mm") + ",";
							strValues += myAFT[llCurr]["PSTA"] + ",";
							strValues += myAFT[llCurr+1]["PSTD"] + ",";
							string strRem = myAFT[llCurr]["REM1"] + "-" + myAFT[llCurr+1]["REM1"];
							strValues += strRem;
							strValues += "\n";
							sb.Append(strValues);
						}
					}
					if( strCurrAdid == "D" && strNextAdid == "A")
					{
						DateTime timeARR = UT.CedaFullDateToDateTime(myAFT[llCurr+1]["STOA"]);
						DateTime timeDEP = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOD"]);
						strTime = myAFT[llCurr+1]["TIFA"];
						if(strTime != "")
							timeARR = UT.CedaFullDateToDateTime(strTime);
						strTime = myAFT[llCurr]["TIFD"];
						if(strTime != "")
							timeDEP = UT.CedaFullDateToDateTime(strTime);
						int ilDura = (dtDuration.Value.Hour * 60) + dtDuration.Value.Minute;
						TimeSpan ts = timeDEP - timeARR;
						if( (ts.TotalMinutes >= ilDura) && timeARR.Day != timeDEP.Day )
						{
							NumberofFlights[2]++;
							NumberofFlights[3]++;
							strValues += myAFT[llCurr+1]["FLNO"] + ",";
							strValues += myAFT[llCurr]["FLNO"] + ",";
							strValues += myAFT[llCurr]["REGN"] + ",";
							strValues += Helper.DateString(myAFT[llCurr+1]["STOA"], "dd'/'MM") + ",";
							strValues += Helper.DateString(myAFT[llCurr+1]["ETAI"], "HH:mm") + ",";
							strValues += Helper.DateString(myAFT[llCurr+1]["TIFA"], "HH:mm") + ",";
							strValues += Helper.DateString(myAFT[llCurr]["STOD"], "dd'/'MM") + ",";
							strValues += Helper.DateString(myAFT[llCurr]["STOD"],myAFT[llCurr]["ETDI"], "HH:mm") + ",";
							strValues += Helper.DateString(myAFT[llCurr]["STOD"],myAFT[llCurr]["TIFD"], "HH:mm") + ",";
							strValues += myAFT[llCurr+1]["PSTA"] + ",";
							strValues += myAFT[llCurr]["PSTD"] + ",";
							string strRem = myAFT[llCurr+1]["REM1"] + "-" + myAFT[llCurr]["REM1"];
							strValues += strRem;
							strValues += "\n";
							sb.Append(strValues);
						}
					}

				}//if(strCurrRkey == strNextRkey)
				llCurr = llCurr + ilStep;
			}//while(llCurr < myAFT.Count)
			tabResult.InsertBuffer(sb.ToString(), "\n");
			lblProgress.Text = "";
			progressBar1.Visible = false;
			tabResult.Sort("3,19", true, true);
			//tabResult.AutoSizeColumns();
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			imTotalFlights = NumberofFlights[0] +NumberofFlights[2] + NumberofFlights[1] +NumberofFlights[3];
			strSubHeader = "From: " + this.ucView1.Viewer.PeriodFrom.ToString("dd.MM.yy'/'HH:mm") + " to " + this.ucView1.Viewer.PeriodTo.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += " View: " + this.ucView1.Viewer.CurrentView;
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " "+" ARR: " + (NumberofFlights[0] +NumberofFlights[2]).ToString();
			strSubHeader += " "+" DEP: " + (NumberofFlights[1] +NumberofFlights[3]).ToString() + ")";
			prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult, 
				"Statistics of overnight flights: " + " Duration: " + dtDuration.Value.ToShortTimeString(), 
				strSubHeader, "", 8);
			rpt.TextBoxCanGrow = false;
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}

		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}

		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			InitTab();
			LoadReportData();
			PrepareReportDataRotations();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}

		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmOvernightFlightsForPVG_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("AFT");
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			txtLens.Text = tabResult.HeaderLengthString;
		}

		private void ucView1_viewEvent(string view)
		{
			btnOK_Click(this,null);
		}
	}
}
