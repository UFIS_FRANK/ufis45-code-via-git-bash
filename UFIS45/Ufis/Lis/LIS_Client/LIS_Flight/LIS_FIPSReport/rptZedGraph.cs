using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using ZedGraph;
using Ufis.Utils;

namespace FIPS_Reports
{

	public class rptZedGraph : ActiveReport
	{
		#region _My Members
		/// <summary>
		/// The 1. Header of the Report
		/// </summary>
		private string mainHeader = "";
		/// <summary>
		/// The 2. Header of the Report
		/// </summary>
		private string subHeader = "";
		/// <summary>
		/// Header of graphic
		/// </summary>
		private string graphHeader = "";
		/// <summary>
		/// Header for X-Axis
		/// </summary>
		private string xAxisHeader = "";
		/// <summary>
		/// Array to store the values to be represented by a bar in the chart
		/// each value will be shown as one bar.
		/// </summary>
		private double [] yArray;	
		/// <summary>
		/// Start date of the shown timeframe
		/// </summary>
		public DateTime datStart;
		/// <summary>
		/// Number of Hour-Bars to be shown in the chart (number of bars)
		/// </summary>
	
		private int numberOfHours;
		public int NumberOfHours
		{
			get{return numberOfHours;}
			set{numberOfHours = value;}
		}
		/// <summary>
		/// 
		/// </summary>
		private int MaxNumberOfMovements = 0;

		public int MaximumNumberOfMovements
		{
			get{return MaxNumberOfMovements;}
			set{MaxNumberOfMovements = value;}
		}

		#endregion _My Members

		/// <summary>
		/// 
		/// </summary>
		private ZedGraph.ZedGraphControl myGraph;
		/// <summary>
		/// Constructor of the rptZedGraph-Report
		/// </summary>
		public rptZedGraph()
		{

			InitializeReport();

		}
		/// <summary>
		/// The 1. Header of the Report
		/// </summary>
		public string MainHeader
		{
			get{return mainHeader;}
			set{mainHeader = value;}
		}
		/// <summary>
		/// The 2. Header of the Report
		/// </summary>
		public string SubHeader
		{
			get{return subHeader;}
			set{subHeader = value;}
		}
		/// <summary>
		/// Header of graphic
		/// </summary>
		public string GraphHeader
		{
			get{return graphHeader;}
			set{graphHeader = value;}
		}
		/// <summary>
		/// Header for X-Axis
		/// </summary>
		public string XAxisHeader
		{
			get{return xAxisHeader;}
			set{xAxisHeader = value;}
		}
		/// <summary>
		/// Array to store the values to be represented by a bar in the chart
		/// each value will be shown as one bar.
		/// </summary>
		public double [] YArray
		{
			get{return yArray;}
			set{yArray = value;}
		}
		/// <summary>
		/// this method creates the chart
		/// </summary>
		/// <param name="sender">Originatorm, who calls the method</param>
		/// <param name="eArgs">Event Arguments</param>
		/// 



		private void rptZedGraph_ReportStart(object sender, System.EventArgs eArgs)
		{
			//*** Read the logo if exists and set it
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strPrintLogoPath = myIni.IniReadValue("GLOBAL", "PRINTLOGO");
			Image myImage = null;
			try
			{
				if(strPrintLogoPath != "")
				{
					myImage = new Bitmap(strPrintLogoPath);
					Picture1.Image = myImage;
				}
			}
			catch(Exception)
			{
			}

			lblHeader1.Text = mainHeader;
			lblHeader2.Text = subHeader;
			txtDate.Text = DateTime.Now.ToString();
			myGraph = ((ZedGraph.ZedGraphControl)this.zGraph.Control);
			double [] YArray = yArray;
			ZedGraph.GraphPane myPane = myGraph.GraphPane;
			myPane.CurveList.Clear();



			// Set the titles and axis labels
			myPane.Title = graphHeader;
			myPane.MarginLeft = 0.5f;
			myPane.MarginRight = 0f;
			myPane.XAxis.Title = xAxisHeader;
			myPane.XAxis.TitleFontSpec.Size = 10f;
			myPane.YAxis.Title = "";
			myPane.YAxis.ScaleFontSpec.Size = 10f;

			string [] str;

			DateTime datCurrent = datStart;




			int ilLoopCnt = 0;
			str = new string[numberOfHours];
			while(ilLoopCnt < numberOfHours)//ilHours)
			{
				str[ilLoopCnt] = datCurrent.Hour.ToString();
				ilLoopCnt++;
				datCurrent = datCurrent.AddHours(1);
			}


			// Add a bar to the graph
			BarItem myCurve = myPane.AddBar( "Curve 1", null, YArray, Color.White );
			// Access a image from the resources
			Image image = Picture1.Image;//Bitmap.FromStream(GetType().Assembly.GetManifestResourceStream("ZedGraph.Demo.ngc4414.jpg") );
			// create a brush with the image
			TextureBrush brush = new TextureBrush( image);
			// use the image for the bar fill
			//myCurve.Bar.Fill = new Fill( Brushes.Blue  );
			myCurve.Bar.Fill = new Fill( Color.Blue, Color.White, Color.Blue, 0F );
			// turn off the bar border
			myCurve.Bar.Border.IsVisible = false;

			
			// Draw the X tics between the labels instead of at the labels
			myPane.XAxis.IsTicsBetweenLabels = true;
			myPane.XAxis.ScaleFontSpec.Size = 8f;

			// Set the XAxis labels
			myPane.XAxis.TextLabels = str;

			// Set the XAxis to Text type
			myPane.XAxis.Type = AxisType.Text;

			// Fill the axis background with a color gradient
			myPane.AxisFill = new Fill( Color.White, Color.LightGray, 45.0f );

			// disable the legend
			myPane.Legend.IsVisible = false;
			
			myPane.AxisChange( myGraph.CreateGraphics());

			if(numberOfHours <= 72)
			{
				// The ValueHandler is a helper that does some position calculations for us.
				ValueHandler valueHandler = new ValueHandler( myPane, true );
				// Display a value for the maximum of each bar cluster
				// Shift the text items by 5 user scale units above the bars
				//const float shift = 0.5f;
				float shift = (3/100*MaxNumberOfMovements)*MaxNumberOfMovements;
				int ord = 0;
				foreach ( CurveItem curve in myPane.CurveList )
				{
					BarItem bar = curve as BarItem;

					if ( bar != null )
					{
						PointPairList points = curve.Points;

						for ( int i=0; i<points.Count; i++ )
						{
							double xVal = points[i].X ;//- 0.3d;


							// Calculate the Y value at the center of each bar
							//double yVal = valueHandler.BarCenterValue( curve, curve.GetBarWidth( myPane ),i, points[i].Y, ord );
							double yVal = points[i].Y;

							// format the label string to have 1 decimal place
							//string lab = xVal.ToString( "F0" );
							string lab = yVal.ToString();

							// don't show value "0" in the chart
							if(yVal != 0)
							{

								// create the text item (assumes the x axis is ordinal or text)
								// for negative bars, the label appears just above the zero value
								TextItem text = new TextItem( lab,(float) xVal ,(float) yVal + ( yVal >= 0 ? shift : -shift ));
	
								// tell Zedgraph to use user scale units for locating the TextItem
								text.Location.CoordinateFrame = CoordType.AxisXYScale;
								text.FontSpec.Size = 6;
								// AlignH the left-center of the text to the specified point
								//						text.Location.AlignH =  xVal > 0 ? AlignH.Left : AlignH.Right;
								//						text.Location.AlignV = AlignV.Center;
								text.Location.AlignH =  AlignH.Center;
								text.Location.AlignV =  yVal > 0 ? AlignV.Bottom : AlignV.Top;
								text.FontSpec.Border.IsVisible = false;
								// rotate the text 90 degrees
								text.FontSpec.Angle = 0;
								text.FontSpec.Fill.IsVisible = false;
								// add the TextItem to the list
								myPane.GraphItemList.Add( text );
							}
						}
					}

					ord++;
				}
			}

		}


		#region ActiveReports Designer generated code
		private PageHeader PageHeader = null;
		private Picture Picture1 = null;
		private Label lblHeader1 = null;
		private Label lblHeader2 = null;
		private Detail Detail = null;
		private CustomControl zGraph = null;
		private PageFooter PageFooter = null;
		private TextBox txtDate = null;
		private Label Label34 = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "FIPS_Reports.rptZedGraph.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Picture1 = ((DataDynamics.ActiveReports.Picture)(this.PageHeader.Controls[0]));
			this.lblHeader1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
			this.lblHeader2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
			this.zGraph = ((DataDynamics.ActiveReports.CustomControl)(this.Detail.Controls[0]));
			this.txtDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[0]));
			this.Label34 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
			// Attach Report Events
			this.ReportStart += new System.EventHandler(this.rptZedGraph_ReportStart);
		}

		#endregion
	}
}
