using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Data;
using Ufis.Utils;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmFlightStatisticsDomesticInternationalCombined : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statement: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		//private string strWhere = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO') AND (STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO'))";// AND (ALC2='@@ALC' OR ALC3='@@ALC')" ;
//		private string strWhere = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO'))";// AND (ALC2='@@ALC' OR ALC3='@@ALC')" ;

		//Where condition with a Airline
		private string strWhereAftAirline= "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO')) AND FTYP IN ('O','S')) AND (ALC2 IN ( '@@AIRLINE') OR ALC3 IN ('@@AIRLINE'))"; 
		
		//Where condition without a Airline
		private string strWhereAftWithoutAirline= "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO')) AND FTYP IN ('O','S'))"; 
		
		//Where condition with a Handling agent
		private string strWhereHaiAgent = "WHERE  (((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO'))AND FTYP IN ('O','S')) AND (URNO IN (SELECT FLNU FROM HAITAB WHERE HSNA IN ('@@HSNA')) OR ALC2 IN (SELECT ALC2  FROM ALTTAB WHERE URNO IN ( SELECT ALTU FROM HAITAB WHERE  HSNA IN ('@@HSNA')) AND ALC2 <>' ') "
			                + " OR ALC3 IN (SELECT ALC3  FROM ALTTAB WHERE URNO IN ( SELECT ALTU FROM HAITAB WHERE  HSNA IN ('@@HSNA')) AND ALC3 <>' ')) ";

		//Where condition without a Handling agent
		private string strWhereWithoutHaiAgent = "WHERE  (((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO'))AND FTYP IN ('O','S')) AND (URNO IN (SELECT FLNU FROM HAITAB) OR ALC2 IN (SELECT ALC2  FROM ALTTAB WHERE URNO IN ( SELECT ALTU FROM HAITAB ) AND ALC2 <>' ') "
			+ " OR ALC3 IN (SELECT ALC3  FROM ALTTAB WHERE URNO IN ( SELECT ALTU FROM HAITAB) AND ALC3 <>' ')) ";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "Date,DomArrival,DomDeparture,DomTotal,IntArrival,IntDeparture,IntTotal,RegArrival,RegDeparture,RegTotal,MixArrival,MixDeparture,MixTotal,Arrival,Departure,Total";
		
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		private string strTabHeader =     "Date, Arr.     , Dep.       , Tot. Dom. , Arr.     , Dep.      , Tot. Internl. , Arr.    , Dep.      ,Tot. Regnl. , Arr.    , Dep.      ,Tot. Combnd., Tot. Arr.   , Tot. Dep.   , Total Flights";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "70,50,50,75,50,50,78,50,50,78,50,50,82,55,55,70";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITAble object for the HAITAB
		/// </summary>
		private ITable myHAI;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Variables to capture number of flights
		/// </summary>
		private double [] NumberofFlights;
		///<summary>
		///Variable to capture number of flights in vertical
		///</summary>
		private double [] NumberofFlightsinVertical;
		
		/// <summary>
		/// String builder
		/// </summary>
		private StringBuilder sb = new StringBuilder(10000);
		/// <summary>
		/// variable to modify the Airline entry
		/// </summary>
		private string strTxtAirline;
		/// <summary>
		/// variable to modify the Handling Agent entry
		/// </summary>
		private string strTxtAgent;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;

		#endregion _My Members

		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.TextBox txtAirline;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RadioButton rdbAirlines;
		private System.Windows.Forms.RadioButton rdbHandlingAgents;
		private System.Windows.Forms.TextBox txtHandlingAgents;
		private System.Windows.Forms.Label label3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmFlightStatisticsDomesticInternationalCombined()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmFlightStatisticsDomesticInternationalCombined));
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop = new System.Windows.Forms.Panel();
			this.label3 = new System.Windows.Forms.Label();
			this.txtHandlingAgents = new System.Windows.Forms.TextBox();
			this.rdbHandlingAgents = new System.Windows.Forms.RadioButton();
			this.rdbAirlines = new System.Windows.Forms.RadioButton();
			this.txtAirline = new System.Windows.Forms.TextBox();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.panelTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 160);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(1192, 493);
			this.panelBody.TabIndex = 5;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(1192, 477);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(1192, 477);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.tabResult_SendLButtonClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(1192, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.label3);
			this.panelTop.Controls.Add(this.txtHandlingAgents);
			this.panelTop.Controls.Add(this.rdbHandlingAgents);
			this.panelTop.Controls.Add(this.rdbAirlines);
			this.panelTop.Controls.Add(this.txtAirline);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(1192, 160);
			this.panelTop.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(12, 28);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(320, 16);
			this.label3.TabIndex = 20;
			this.label3.Text = "Note: Use comma separation for multiple values";
			// 
			// txtHandlingAgents
			// 
			this.txtHandlingAgents.BackColor = System.Drawing.SystemColors.Window;
			this.txtHandlingAgents.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtHandlingAgents.Location = new System.Drawing.Point(176, 87);
			this.txtHandlingAgents.Name = "txtHandlingAgents";
			this.txtHandlingAgents.TabIndex = 6;
			this.txtHandlingAgents.Text = "";
			// 
			// rdbHandlingAgents
			// 
			this.rdbHandlingAgents.Location = new System.Drawing.Point(56, 87);
			this.rdbHandlingAgents.Name = "rdbHandlingAgents";
			this.rdbHandlingAgents.Size = new System.Drawing.Size(108, 24);
			this.rdbHandlingAgents.TabIndex = 19;
			this.rdbHandlingAgents.Text = "Handling Agents:";
			this.rdbHandlingAgents.CheckedChanged += new System.EventHandler(this.rdbHandlingAgents_CheckedChanged);
			// 
			// rdbAirlines
			// 
			this.rdbAirlines.Location = new System.Drawing.Point(56, 55);
			this.rdbAirlines.Name = "rdbAirlines";
			this.rdbAirlines.TabIndex = 18;
			this.rdbAirlines.Text = "Airlines:";
			this.rdbAirlines.CheckedChanged += new System.EventHandler(this.rdbAirlines_CheckedChanged);
			// 
			// txtAirline
			// 
			this.txtAirline.BackColor = System.Drawing.SystemColors.Window;
			this.txtAirline.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtAirline.Location = new System.Drawing.Point(176, 55);
			this.txtAirline.Name = "txtAirline";
			this.txtAirline.TabIndex = 3;
			this.txtAirline.Text = "";
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(400, 100);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 12;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(400, 116);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 13;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(232, 116);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 10;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(152, 116);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 9;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(312, 116);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "&Close";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(72, 116);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 8;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(268, 4);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(136, 20);
			this.dtTo.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(92, 4);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(132, 20);
			this.dtFrom.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			// 
			// frmFlightStatisticsDomesticInternationalCombined
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(1192, 653);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Name = "frmFlightStatisticsDomesticInternationalCombined";
			this.Text = "Flight Statistics according to domestic/international/regional/combined";
			this.Load += new System.EventHandler(this.frmFlightStatisticsDomesticInternationalCombined_Load);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.panelTop.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmFlightStatisticsDomesticInternationalCombined_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
			PrepareFilterData();
		}

		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}


		/// <summary>
		/// Initializes the tab controll.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;

			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
			rdbAirlines.Checked =true;
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			strTxtAirline ="";
			strTxtAgent ="";
			NumberofFlightsinVertical = new double[15];
			string strError = ValidateUserEntry();
			
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent();
			if (txtAirline.Enabled )
			{

				strTxtAirline =  txtAirline.Text ;
				strTxtAirline = strTxtAirline.Replace(",","','").ToUpper();

			}
			if(txtHandlingAgents.Enabled)
			{
				strTxtAgent = txtHandlingAgents.Text;
				strTxtAgent = strTxtAgent.Replace(",","','").ToUpper();
			}
			if (txtAirline.Enabled && txtAirline.Text.Length !=0)	LoadReportData(strWhereAftAirline);
			if (txtAirline.Enabled && txtAirline.Text.Length ==0)	LoadReportData(strWhereAftWithoutAirline);
			if (txtHandlingAgents.Enabled && txtHandlingAgents.Text.Length !=0) LoadHaiReportData(strWhereHaiAgent);
			if (txtHandlingAgents.Enabled && txtHandlingAgents.Text.Length ==0) LoadHaiReportData(strWhereWithoutHaiAgent);
			PrepareFillReportData();
			this.Cursor = Cursors.Arrow;
		}

		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			return strRet;
		}

		private void LoadReportData(string strWhere)
		{
			string strTmpAFTWhere = strWhere;
		//	StringBuilder sb = new StringBuilder(10000);
			//Clear the tab
			tabResult.ResetContent();
			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			//Load the data from AFTTAB
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,ADID,ALC2,ALC3,STOA,STOD,FLTI", 
				"10,1,2,3,14,14,1", 
				"URNO,ADID,ALC2,ALC3,STOA,STOD,FLTI");
			myAFT.TimeFields = "STOA,STOD";
			myAFT.Clear();
			myAFT.TimeFieldsInitiallyInUtc = true;
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Show();
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpAFTWhere = strWhere;
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@TO", strDateTo );
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@AIRLINE", strTxtAirline);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@HOPO", UT.Hopo);
				//Load the data from AFTTAB
				myAFT.Load(strTmpAFTWhere);
				myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
				NumberofFlights= new double[15];
				for(int i = 0; i < myAFT.Count; i++)
				{
					PrepareReportCount(i,datReadFrom);
				}
				myAFT.Clear();
				//New function added
				FillGrid (datReadFrom);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
		}
		///<summary>
		///This method loads data for the Handling agents
		///</summary>
		
		private void LoadHaiReportData(string strWhere)
		{
			string strTmpAFTWhere = strWhere;
			 StringBuilder sb = new StringBuilder(10000);
			//Clear the tab
			tabResult.ResetContent();
			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			//Load the data from AFTTAB
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,ADID,ALC2,ALC3,STOA,STOD,FLTI", 
				"10,1,2,3,14,14,1", 
				"URNO,ADID,ALC2,ALC3,STOA,STOD,FLTI");
			myAFT.TimeFields = "STOA,STOD";
			myAFT.Clear();
			myAFT.TimeFieldsInitiallyInUtc = true;
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Show();
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpAFTWhere = strWhere;
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@TO", strDateTo );
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@AIRLINE", txtAirline.Text);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@HSNA", strTxtAgent);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@HOPO", UT.Hopo);
				//Load the data from AFTTAB
				myAFT.Load(strTmpAFTWhere);
				myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
				NumberofFlights= new double[15];
				for(int i = 0; i < myAFT.Count; i++)
				{
					//	datTMP = UT.CedaDateToDateTime( myAFT[i][strDepartureTimeKey]);
					PrepareReportCount(i,datReadFrom );
				}
				myAFT.Clear();
				//New function added
				FillGrid (datReadFrom);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
	//		myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
		}

		/// <summary>
		/// This method counts the  number of flights
		/// </summary>
		private void PrepareReportCount(int i,DateTime From)
		{
			//Start with the count of Arrival flights
			if (myAFT[i]["ADID"] == "A")
			{
				//Check for Domestic flights
				if (myAFT[i]["FLTI"] == "D")
				{
					NumberofFlights[0]++;
					NumberofFlights[2]++;
					NumberofFlights[12]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[0]++;
					NumberofFlightsinVertical[2]++;
					NumberofFlightsinVertical[12]++;
					NumberofFlightsinVertical[14]++; 

				}
				//Check for International Flights
				if (myAFT[i]["FLTI"] == "I")
				{
					NumberofFlights[3]++;
					NumberofFlights[5]++;
					NumberofFlights[12]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[3]++;
					NumberofFlightsinVertical[5]++;
					NumberofFlightsinVertical[12]++;
					NumberofFlightsinVertical[14]++;
				}
				//Check for Regional Flights
				if (myAFT[i]["FLTI"] == "R")
				{
					NumberofFlights[6]++;
					NumberofFlights[8]++;
					NumberofFlights[12]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[6]++;
					NumberofFlightsinVertical[8]++;
					NumberofFlightsinVertical[12]++;
					NumberofFlightsinVertical[14]++;
				}
				//Check for combined Flights
				if (myAFT[i]["FLTI"] == "M")
				{
					NumberofFlights[9]++;
					NumberofFlights[11]++;
					NumberofFlights[12]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[9]++;
					NumberofFlightsinVertical[11]++;
					NumberofFlightsinVertical[12]++;
					NumberofFlightsinVertical[14]++;
				}
			}
			//Check for Departure flights
			if (myAFT[i]["ADID"] == "D")
			{
				//Check for Domestic flights
				if (myAFT[i]["FLTI"] == "D")
				{
					NumberofFlights[1]++;
					NumberofFlights[2]++;
					NumberofFlights[13]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[1]++;
					NumberofFlightsinVertical[2]++;
					NumberofFlightsinVertical[13]++;
					NumberofFlightsinVertical[14]++;
				}
				//Check for International Flights
				if (myAFT[i]["FLTI"] == "I")
				{
					NumberofFlights[4]++;
					NumberofFlights[5]++;
					NumberofFlights[13]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[4]++;
					NumberofFlightsinVertical[5]++;
					NumberofFlightsinVertical[13]++;
					NumberofFlightsinVertical[14]++;
				}
				//Check for Regional Flights
				if (myAFT[i]["FLTI"] == "R")
				{
					NumberofFlights[7]++;
					NumberofFlights[8]++;
					NumberofFlights[13]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[7]++;
					NumberofFlightsinVertical[8]++;
					NumberofFlightsinVertical[13]++;
					NumberofFlightsinVertical[14]++;
				}
				//Check for combined Flights
				if (myAFT[i]["FLTI"] == "M")
				{
					NumberofFlights[10]++;
					NumberofFlights[11]++;
					NumberofFlights[13]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[10]++;
					NumberofFlightsinVertical[11]++;
					NumberofFlightsinVertical[13]++;
					NumberofFlightsinVertical[14]++;
				}
			}
			//Check for Rotation flights
			if (myAFT[i]["ADID"] == "B")
			{
				DateTime dtStoa = UT.CedaFullDateToDateTime(myAFT[i]["STOA"]);
				
				DateTime dtStod = UT.CedaFullDateToDateTime(myAFT[i]["STOD"]);

				if (Helper.DateString(" dtStoa","ddMMyyyy") ==Helper.DateString("From","ddMMyyyy")) // Arrival Flights
				{
					//Check for Domestic flights
					if (myAFT[i]["FLTI"] == "D")
					{
						NumberofFlights[0]++;
						NumberofFlights[2]++;
						NumberofFlights[12]++;
						NumberofFlights[14]++; 

						NumberofFlightsinVertical[0]++;
						NumberofFlightsinVertical[2]++;
						NumberofFlightsinVertical[12]++;
						NumberofFlightsinVertical[14]++; 

					}
					//Check for International Flights
					if (myAFT[i]["FLTI"] == "I")
					{
						NumberofFlights[3]++;
						NumberofFlights[5]++;
						NumberofFlights[12]++;
						NumberofFlights[14]++; 

						NumberofFlightsinVertical[3]++;
						NumberofFlightsinVertical[5]++;
						NumberofFlightsinVertical[12]++;
						NumberofFlightsinVertical[14]++;
					}
					//Check for Regional Flights
					if (myAFT[i]["FLTI"] == "R")
					{
						NumberofFlights[6]++;
						NumberofFlights[8]++;
						NumberofFlights[12]++;
						NumberofFlights[14]++; 

						NumberofFlightsinVertical[6]++;
						NumberofFlightsinVertical[8]++;
						NumberofFlightsinVertical[12]++;
						NumberofFlightsinVertical[14]++;
					}
					//Check for combined Flights
					if (myAFT[i]["FLTI"] == "M")
					{
						NumberofFlights[9]++;
						NumberofFlights[11]++;
						NumberofFlights[12]++;
						NumberofFlights[14]++; 

						NumberofFlightsinVertical[9]++;
						NumberofFlightsinVertical[11]++;
						NumberofFlightsinVertical[12]++;
						NumberofFlightsinVertical[14]++;
					}
				}
		
				if (Helper.DateString( "dtStod","ddMMyyyy") ==Helper.DateString("From","ddMMyyyy"))
				{
					//Check for Domestic flights
					if (myAFT[i]["FLTI"] == "D")
					{
						NumberofFlights[1]++;
						NumberofFlights[2]++;
						NumberofFlights[13]++;
						NumberofFlights[14]++; 
					}
					//Check for International Flights
					if (myAFT[i]["FLTI"] == "I")
					{
						NumberofFlights[4]++;
						NumberofFlights[5]++;
						NumberofFlights[13]++;
						NumberofFlights[14]++; 
					}
					//Check for Regional Flights
					if (myAFT[i]["FLTI"] == "R")
					{
						NumberofFlights[7]++;
						NumberofFlights[8]++;
						NumberofFlights[13]++;
						NumberofFlights[14]++; 
					}
					//Check for combined Flights
					if (myAFT[i]["FLTI"] == "M")
					{
						NumberofFlights[10]++;
						NumberofFlights[11]++;
						NumberofFlights[13]++;
						NumberofFlights[14]++; 
					}
				}
			}
		}
		private void FillGrid(DateTime From)
		{
			StringBuilder sb = new StringBuilder(10000);
			string strValues = "";
			strValues += From.ToLocalTime().Date.ToShortDateString() + "," + NumberofFlights[0].ToString()+"," + NumberofFlights[1].ToString()+","; 
			strValues += NumberofFlights[2].ToString() +","+ NumberofFlights[3].ToString()+","+ NumberofFlights[4].ToString()+",";
			strValues += NumberofFlights[5].ToString()+"," + NumberofFlights[6].ToString()+","+ NumberofFlights[7].ToString()+",";
			strValues += NumberofFlights[8].ToString()+"," + NumberofFlights[9].ToString()+","+ NumberofFlights[10].ToString()+",";
			strValues += NumberofFlights[11].ToString()+"," + NumberofFlights[12].ToString()+"," + NumberofFlights[13].ToString()+",";
			strValues += NumberofFlights[14].ToString()+ "\n";
			sb.Append(strValues);
			tabResult.InsertBuffer(sb.ToString(), "\n");
		}
		private void PrepareFillReportData()
		{
			StringBuilder sb1 = new StringBuilder(10000);
			string strValuesTotal = "";
		//	tabResult.Sort("0", true, true);
			strValuesTotal += "-- Total --" + "," + NumberofFlightsinVertical[0].ToString()+"," + NumberofFlightsinVertical[1].ToString()+","; 
			strValuesTotal += NumberofFlightsinVertical[2].ToString() +","+ NumberofFlightsinVertical[3].ToString()+","+ NumberofFlightsinVertical[4].ToString()+",";
			strValuesTotal += NumberofFlightsinVertical[5].ToString()+"," + NumberofFlightsinVertical[6].ToString()+","+ NumberofFlightsinVertical[7].ToString()+",";
			strValuesTotal += NumberofFlightsinVertical[8].ToString()+"," + NumberofFlightsinVertical[9].ToString()+","+ NumberofFlightsinVertical[10].ToString()+",";
			strValuesTotal += NumberofFlightsinVertical[11].ToString()+"," + NumberofFlightsinVertical[12].ToString()+"," + NumberofFlightsinVertical[13].ToString()+",";
			strValuesTotal += NumberofFlightsinVertical[14].ToString()+ "\n";
			sb1.Append(strValuesTotal);
			tabResult.InsertBuffer(sb1.ToString(), "\n");
			lblProgress.Text = "";
			lblProgress.Refresh ();
			progressBar1.Visible = false;
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		private void rdbAirlines_CheckedChanged(object sender, System.EventArgs e)
		{
			txtHandlingAgents.Enabled=false;
			txtAirline.Enabled=true;
		}
		private void rdbHandlingAgents_CheckedChanged(object sender, System.EventArgs e)
		{
			txtAirline.Enabled  =false;
			txtHandlingAgents.Enabled=true;
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + NumberofFlightsinVertical[14].ToString();
			strSubHeader += " " + " Tot. ARR: " + NumberofFlightsinVertical[12].ToString();
			strSubHeader += " " + " Tot. DEP: " + NumberofFlightsinVertical[13].ToString() + ")";
			prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult, 
				"Flight Statistics according to domestic/international/regional/combined: " , strSubHeader, "", 9);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			strTxtAirline ="";
			strTxtAgent ="";
			NumberofFlightsinVertical = new double[15];
			string strError = ValidateUserEntry();
			
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent();
			if (txtAirline.Enabled )
			{

				strTxtAirline =  txtAirline.Text ;
				strTxtAirline = strTxtAirline.Replace(",","','").ToUpper();

			}
			if(txtHandlingAgents.Enabled)
			{
				strTxtAgent = txtHandlingAgents.Text;
				strTxtAgent = strTxtAgent.Replace(",","','").ToUpper();
			}
			if (txtAirline.Enabled && txtAirline.Text.Length !=0)	LoadReportData(strWhereAftAirline);
			if (txtAirline.Enabled && txtAirline.Text.Length ==0)	LoadReportData(strWhereAftWithoutAirline);
			if (txtHandlingAgents.Enabled && txtHandlingAgents.Text.Length !=0) LoadHaiReportData(strWhereHaiAgent);
			if (txtHandlingAgents.Enabled && txtHandlingAgents.Text.Length ==0) LoadHaiReportData(strWhereWithoutHaiAgent);
			PrepareFillReportData();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}

		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			if(tabResult.GetLineCount ()==0)
			{
				
				MessageBox.Show(this, "No Data to preview");
			}
			else
			{
				RunReport();
			}
		
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			myDB.Unbind("AFT");
			this.Close ();
		}

		private void tabResult_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if(tabResult.SortOrderASC == true)
					{
						tabResult.DeleteLine(tabResult.GetLineCount()-1);
						tabResult.Sort(e.colNo.ToString(), false, true);
						PrepareFillReportData();
					}
					else
					{
						tabResult.DeleteLine(tabResult.GetLineCount()-1);
						tabResult.Sort(e.colNo.ToString(), true, true);
						PrepareFillReportData();
					}
				}
				else
				{
					tabResult.DeleteLine(tabResult.GetLineCount()-1);
					tabResult.Sort( e.colNo.ToString(), true, true);
					PrepareFillReportData();
				}
				tabResult.Refresh();
			}
		}
	}
}
