using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmOverNight_FlightSummary.
	/// </summary>
	public class frmOverNight_FlightSummaryForPVG : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		
		private string strWhereRawA =  "((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))";

		private string strWhereRawD =  "((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))))" ;

		private string strWhereRawB = string.Format("({0} OR {1})","((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))","((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))))");

		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string stractLogicalFields = "ARRIVAL,STA,COUNT";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes ==> for Rotation
		/// </summary>
		
		//DB-Fields
		// 10 , 10 , 9  ,  8 , 1  , 1  , 14 , 1  ,  5 , 1  , 2  , 3  , 4  , 3  , 4  , 2  , 5  , 5  , 5  , 5  , 5  , 1  , 3  , 14 , 1  , 3  , 4  , 5  ,  5 , 5  , 5  , 5
		//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT"
		private string strLogicalFieldsRotation = "ALC2,Arrival_A,ID_A,J_A,Sched_A,Departure_D,ID_D,J_D,STD_D";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		/// <summary>
		private string stractTabHeader =     "Airlines,  Date  ,Overnight flights";
		
		/// The header columns, which must be used for the report output
		/// for rotation
		/// </summary>
		private string strTabHeaderRotation = "Airlines,ADid,Arrival,STA,Departure,STD";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string stractTabHeaderLens = "60,80,100";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// for rotation
		/// </summary>
		private string strTabHeaderLensRotation = "40,20,50,60,50,60";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// Different variables to be used for counts
		/// </summary>
		private int iCount = 0;
		private int iarrv=0;
		private int iprevcount=0;
		private int iTotal=0;
		private string prevAirline;
		private string currAirline;
		private string currDate;
		private string	prevDate;
		private double [] NumberofFlights;
		StringBuilder sb = new StringBuilder(10000);
		private string strValues;
		private string strEmptyLine = ",,";
		//End here
		
		#endregion _My Members

		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtDuration;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtLens;
		private System.Windows.Forms.Button button1;
		private AxTABLib.AxTAB actualtabResults;
		private UserControls.ucView ucView1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmOverNight_FlightSummaryForPVG()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			myDB = UT.GetMemDB();
			this.ucView1.Report = "OvernightFlightSummary";
			this.ucView1.viewEvent +=new UserControls.ucView.ViewChangedEvent(ucView1_viewEvent);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmOverNight_FlightSummaryForPVG));
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.actualtabResults = new AxTABLib.AxTAB();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop = new System.Windows.Forms.Panel();
			this.ucView1 = new UserControls.ucView();
			this.button1 = new System.Windows.Forms.Button();
			this.txtLens = new System.Windows.Forms.TextBox();
			this.dtDuration = new System.Windows.Forms.DateTimePicker();
			this.label5 = new System.Windows.Forms.Label();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.actualtabResults)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.panelTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 128);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(780, 394);
			this.panelBody.TabIndex = 3;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.actualtabResults);
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(780, 378);
			this.panelTab.TabIndex = 2;
			// 
			// actualtabResults
			// 
			this.actualtabResults.ContainingControl = this;
			this.actualtabResults.Dock = System.Windows.Forms.DockStyle.Fill;
			this.actualtabResults.Location = new System.Drawing.Point(0, 0);
			this.actualtabResults.Name = "actualtabResults";
			this.actualtabResults.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("actualtabResults.OcxState")));
			this.actualtabResults.Size = new System.Drawing.Size(780, 378);
			this.actualtabResults.TabIndex = 1;
			this.actualtabResults.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.actualtabResults_SendLButtonClick);
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(780, 378);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(780, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.ucView1);
			this.panelTop.Controls.Add(this.button1);
			this.panelTop.Controls.Add(this.txtLens);
			this.panelTop.Controls.Add(this.dtDuration);
			this.panelTop.Controls.Add(this.label5);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(780, 128);
			this.panelTop.TabIndex = 2;
			// 
			// ucView1
			// 
			this.ucView1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucView1.Location = new System.Drawing.Point(20, 8);
			this.ucView1.Name = "ucView1";
			this.ucView1.Report = "";
			this.ucView1.Size = new System.Drawing.Size(304, 40);
			this.ucView1.TabIndex = 21;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(224)), ((System.Byte)(192)));
			this.button1.Location = new System.Drawing.Point(456, 36);
			this.button1.Name = "button1";
			this.button1.TabIndex = 20;
			this.button1.Text = "button1";
			this.button1.Visible = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtLens
			// 
			this.txtLens.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(224)), ((System.Byte)(192)));
			this.txtLens.Location = new System.Drawing.Point(276, 60);
			this.txtLens.Name = "txtLens";
			this.txtLens.Size = new System.Drawing.Size(488, 20);
			this.txtLens.TabIndex = 19;
			this.txtLens.Text = "textBox1";
			this.txtLens.Visible = false;
			// 
			// dtDuration
			// 
			this.dtDuration.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtDuration.Location = new System.Drawing.Point(80, 56);
			this.dtDuration.Name = "dtDuration";
			this.dtDuration.ShowUpDown = true;
			this.dtDuration.Size = new System.Drawing.Size(128, 20);
			this.dtDuration.TabIndex = 2;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(12, 60);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 16);
			this.label5.TabIndex = 18;
			this.label5.Text = "Duration >=";
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(412, 72);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 12;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(412, 88);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 10;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 88);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 8;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 88);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 7;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 88);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 9;
			this.btnCancel.Text = "&Close";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 88);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 6;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// frmOverNight_FlightSummaryForPVG
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(780, 522);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Name = "frmOverNight_FlightSummaryForPVG";
			this.Text = "Statistics of overnight Arrival flights(summary)";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmOverNight_FlightSummary_Closing);
			this.Load += new System.EventHandler(this.frmOverNight_FlightSummary_Load);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.actualtabResults)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.panelTop.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
		
		/// <summary>
		/// Loads the from and inits the necessary things.
		/// </summary>
		/// <param name="sender">Originator of the method call</param>
		/// <param name="e">Event args</param>
		private void frmOverNight_FlightSummary_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
			PrepareFilterData();
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			DateTime olDura;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			olDura = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 3,0,0);
			dtDuration.Value = olDura;
			dtDuration.CustomFormat = "HH:mm";
		}
		/// <summary>
		/// Initializes the tab control.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString = strTabHeaderRotation;
			tabResult.LogicalFieldList = strLogicalFieldsRotation;
			tabResult.HeaderLengthString = strTabHeaderLensRotation;

			actualtabResults.ResetContent();
			actualtabResults.ShowHorzScroller(true);
			actualtabResults.EnableHeaderSizing(true);
			actualtabResults.SetTabFontBold(true);
			actualtabResults.LifeStyle = true;
			actualtabResults.LineHeight = 16;
			actualtabResults.FontName = "Arial";
			actualtabResults.FontSize = 14;
			actualtabResults.HeaderFontSize = 14;
			actualtabResults.AutoSizeByHeader = true;
			actualtabResults.HeaderString = stractTabHeader;
			actualtabResults.LogicalFieldList = stractLogicalFields;
			actualtabResults.HeaderLengthString = stractTabHeaderLens;
			PrepareReportDataRotations();
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
		}

		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent();
			actualtabResults.ResetContent();
			LoadReportData();
			PrepareReportCount();
			PrepareFillReportData();
		//	PrepareReportDataRotations();
			//RunReport();
			this.Cursor = Cursors.Arrow;
			txtLens.Text = tabResult.HeaderLengthString;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;
			if (this.ucView1.Viewer.CurrentView == "")
			{
				strRet += ilErrorCount.ToString() +  ". A view must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.PeriodFrom >= this.ucView1.Viewer.PeriodTo)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			if (this.ucView1.Viewer.Arrival == false)
			{
				strRet += ilErrorCount.ToString() +  ". Arrival must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Departure == false)
			{
				strRet += ilErrorCount.ToString() +  ". Departure must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Rotation == false)
			{
				strRet += ilErrorCount.ToString() +  ". Rotation must be selected!\n";
				ilErrorCount++;
			}

			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere;
			//Clear the tab
			//tabResult.ResetContent();
			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			string strFtyp = this.ucView1.Viewer.FlightTypes;
			datFrom = this.ucView1.Viewer.PeriodFrom;
			datTo   = this.ucView1.Viewer.PeriodTo;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(this.ucView1.Viewer.PeriodFrom);
				datTo   = UT.LocalToUtc(this.ucView1.Viewer.PeriodTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodFrom);
				strDateTo = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodTo);
			}

			//Now reading day by day the flight records in order to 
			//not stress the database too much
			myDB.Unbind("AFT");
			// 10 , 10 , 9  ,  8 , 1  , 1  , 14 , 1  ,  5 , 1  , 2  , 3  , 4  , 3  , 4  , 2  , 5  , 5  , 5  , 5  , 5  , 1  , 3  , 14 , 1  , 3  , 4  , 5  ,  5 , 5  , 5  , 5
			//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT
			myAFT = myDB.Bind("AFT", "AFT", 
				"ALC2,RKEY,URNO,ADID,FLNO,CSGN,FLTI,JCNT,STOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT", 
				"2,10,10,1,9,8,1,1,14,14,1,5,1,2,3,4,3,4,2,5,5,5,5,5,1,3,14,14,1,3,4,5,5,5,5,5",
				"ALC2,RKEY,URNO,ADID,FLNO,CSGN,FLTI,JCNT,STOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpWhere = "WHERE ";

				// check on code share enabled
				if (this.ucView1.Viewer.IncludeCodeShare)
				{
					string strTmpWhere1 = "";
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere1 = strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawD;
					strTmpWhere1+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere1+= this.ucView1.Viewer.GeneralFilter;

					string strTmpWhere2 = "";
					//patch the where statement according to the user's entry
					strTmpWhere2+= this.ucView1.Viewer.AdditionalSelection(1);

					strTmpWhere = string.Format("WHERE ({0}) AND ({1})",strTmpWhere1,strTmpWhere2);
				}
				else
				{
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere += strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere += strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere += strWhereRawD;
					strTmpWhere+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere+= this.ucView1.Viewer.GeneralFilter;

				}

				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );

				if (this.ucView1.Viewer.Rotation)
				{
					strTmpWhere += "[ROTATIONS]";
					if (strFtyp.Length > 0)
						strTmpWhere += string.Format("[FILTER=FTYP IN ({0})]",strFtyp);
				}
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);

				NumberofFlights= new double[2];
				myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
				PrepareReportDataRotations();
					//	datTMP = UT.CedaDateToDateTime( myAFT[i][strDepartureTimeKey]);
	//			PrepareReportCount();
				tabResult.Visible = false;
				myAFT.Clear();
				//New function added
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();

			//Filter out the FTYPs which are selected due to rotations
			for(int i = myAFT.Count-1; i >= 0; i--)
			{
				switch(myAFT[i]["FTYP"])
				{
					case "S":
					case "O":
					case "X":
						break;
					default:
						myAFT.Remove(i);
					break;
				}
			}


		//	myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
		}

		///<summary>
		///Test function to count 
		///PrepareReportCount
		///</summary>
		private void PrepareReportCount()
		{
			StringBuilder sb = new StringBuilder(10000);

			for(int k=0;k<tabResult.GetLineCount ();k++)
			{

				currAirline = 	tabResult .GetColumnValue(k,0);
				currDate = tabResult .GetColumnValue(k,3);
			//	From.ToLocalTime().Date.ToShortDateString();
			 
				if (k==0 && tabResult .GetColumnValue(k,1) == "A")
				{
					prevAirline = currAirline ;
					prevDate = currDate;
				}
				if (k==32)
				{
					tabResult.GetLineCount ();
				}
				if (prevAirline == currAirline)
				{
					//Start with the count of Arrival flights
					if (tabResult .GetColumnValue(k,1) == "A")
					{
						if (prevDate == currDate)
						{
							NumberofFlights[0]++;
							NumberofFlights[1]++;
						}
						if (prevDate != currDate)
						{
							strValues += prevAirline + "," + Helper.DateString(prevDate, "yyyyMMdd") +"," + NumberofFlights[0].ToString () +" \n" ;
							sb.Append(strValues);
							strValues = "";
							NumberofFlights[0] = 1;
							NumberofFlights[1]++;
							prevAirline=currAirline;
							prevDate = currDate;
						}

						if (k ==tabResult .GetLineCount() -1)
						{
							strValues += currAirline + "," + Helper.DateString(tabResult .GetColumnValue(k,3), "yyyyMMdd") +"," + NumberofFlights[0].ToString () +" \n" ;
							sb.Append(strValues);
							strValues = "";
						}
					}
				}
				if (prevAirline != currAirline)
				{
					strValues += prevAirline + "," + Helper.DateString(prevDate, "yyyyMMdd") +"," + NumberofFlights[0].ToString () +" \n" ;
					NumberofFlights[0] = 0;
					sb.Append(strValues);
					strValues = "";
					//Start with the count of Arrival flights
					if (tabResult .GetColumnValue(k,1) == "A")
					{
						NumberofFlights[0]++;
						NumberofFlights[1]++;
						prevAirline = currAirline;
						prevDate = currDate;
					
						if (k ==tabResult .GetLineCount() -1)
						{
							strValues += currAirline + "," + Helper.DateString(tabResult .GetColumnValue(k,3), "yyyyMMdd") +"," + NumberofFlights[0].ToString () +" \n" ;
							sb.Append(strValues);
							strValues = "";
						}
					}
				}
			}
				actualtabResults .InsertBuffer(sb.ToString(), "\n");
		}

/// <summary>
/// 
/// </summary>

		private void PrepareFillReportData()
		{
			lblProgress.Text = "";
			progressBar1.Visible = false;
			actualtabResults.Sort ("0",true,true);
			actualtabResults.Sort ("1",true,true);
			insertTotal();
			actualtabResults.Refresh();
			tabResult.Visible =false;
			actualtabResults.Visible =true;
			lblResults.Text = "Report Results: (" + actualtabResults.GetLineCount().ToString() + ")";
		}
		///<summary>
		///This function is written to insert Total at end
		///</summary>
		private void insertTotal()
		{
			StringBuilder sb1 = new StringBuilder(10000);
			string strValuesactTotal = "";
			strValuesactTotal  +=  "--" +","+"Total" +","+ NumberofFlights[1].ToString()+"\n" ;
			sb1.Append(strValuesactTotal);
			actualtabResults .InsertBuffer(sb1.ToString(), "\n");
			strValuesactTotal ="";
		}
		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// First organize the rotations and then put the result
		/// into the tabResult
		/// </summary>
		private void PrepareReportDataRotations()
		{
			if(myAFT == null)
				return;

			myAFT.Sort("RKEY", true);
			int llCurr = 0;
			int ilStep = 1;
			string strCurrAdid = "";
			string strNextAdid = "";
			string strCurrRkey = "";
			string strNextRkey = "";
			string strTime = "";
			DateTime tmpDate;
			StringBuilder sb = new StringBuilder(10000);
			while(llCurr < myAFT.Count)
			{
				string strValues = "";
				strCurrRkey = myAFT[llCurr]["RKEY"];
				strCurrAdid = myAFT[llCurr]["ADID"];
				if ((llCurr + 1) < myAFT.Count)
				{
					strNextRkey = myAFT[llCurr+1]["RKEY"];
					strNextAdid = myAFT[llCurr+1]["ADID"];
				}
				else
				{
					strNextRkey = "";
					strNextAdid = "";
				}

				if(strCurrRkey == strNextRkey)
				{//Is a rotation
					ilStep = 2;
					if( strCurrAdid == "A" && strNextAdid == "D")
					{
						DateTime timeARR = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOA"]);
						DateTime timeDEP = UT.CedaFullDateToDateTime(myAFT[llCurr+1]["STOD"]);
						strTime = myAFT[llCurr]["TIFA"];
						if(strTime != "")
							timeARR = UT.CedaFullDateToDateTime(strTime);
						strTime = myAFT[llCurr+1]["TIFD"];
						if(strTime != "")
							timeDEP = UT.CedaFullDateToDateTime(strTime);
						int ilDura = (dtDuration.Value.Hour * 60) + dtDuration.Value.Minute;
						TimeSpan ts = timeDEP - timeARR;
						if( (ts.TotalMinutes >= ilDura) && timeARR.Day != timeDEP.Day )
						{
							//The arrival part of rotation
							//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT
							//Arrival,ID,Sched,A,Na,K,S,ORG,VIA,POS,Gate1,Gate2,Belt,I,Type,Departure,ID,J,STD,A,Na,K,S,VIA,DES,POS,Gate1,Gate2,I
							//ARR:,,,,,,,,,,,,,,,
							//DEP:,,,,,,,,,,,,,
							string strFLNO = myAFT[llCurr]["FLNO"];
							if(strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
							string strALC2A = myAFT[llCurr]["ALC2"];
							strValues += strALC2A + ",";
							strValues += myAFT[llCurr]["ADID"] +",";
							strValues += strFLNO + ",";
							strValues += Helper.DateString(myAFT[llCurr]["STOA"], "yyyyMMdd") + ",";
							//-----------------------------------The departure part of rotation
							strFLNO = myAFT[llCurr+1]["FLNO"];
							if(strFLNO == "") strFLNO = myAFT[llCurr+1]["CSGN"];
							strValues += myAFT[llCurr]["ADID"] +",";
							strValues += strFLNO + "\n";
						//	strValues += Helper.DateString(myAFT[llCurr+1]["STOD"], "yyyyMMdd") +  "\n";
							sb.Append(strValues);
						}
					}
//					if( strCurrAdid == "D" && strNextAdid == "A")
//					{
//						DateTime timeARR = UT.CedaFullDateToDateTime(myAFT[llCurr+1]["STOA"]);
//						DateTime timeDEP = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOD"]);
//						strTime = myAFT[llCurr+1]["TIFA"];
//						if(strTime != "")
//							timeARR = UT.CedaFullDateToDateTime(strTime);
//						strTime = myAFT[llCurr]["TIFD"];
//						if(strTime != "")
//							timeDEP = UT.CedaFullDateToDateTime(strTime);
//						int ilDura = (dtDuration.Value.Hour * 60) + dtDuration.Value.Minute;
//						TimeSpan ts = timeDEP - timeARR;
//						if( (ts.TotalMinutes >= ilDura) && timeARR.Day != timeDEP.Day )
//						{
//							//The arrival part of rotation
//							string strFLNO = myAFT[llCurr+1]["FLNO"];
//							if(strFLNO == "") strFLNO = myAFT[llCurr+1]["CSGN"];
//							string strALC2AA = myAFT[llCurr]["ALC2"];
//							strValues += strALC2AA + ",";
//							strValues += myAFT[llCurr]["ADID"] +",";
//							strValues += strFLNO + ",";
//							strValues += Helper.DateString(myAFT[llCurr+1]["STOA"], "yyyyMMdd") + ",";
//				//-----------------------------------The departure part of rotation
//							strFLNO = myAFT[llCurr]["FLNO"];
//							if(strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
//							strValues += myAFT[llCurr]["ADID"] +",";
//							strValues += strFLNO + "\n";
//						//	strValues += Helper.DateString(myAFT[llCurr]["STOD"], "yyyyMMdd") + "\n";
//							sb.Append(strValues);
//						}
//					}
				}//if(strCurrRkey == strNextRkey)
				llCurr = llCurr + ilStep;
			}//while(llCurr < myAFT.Count)
			tabResult.InsertBuffer(sb.ToString(), "\n");
			lblProgress.Text = "";
			progressBar1.Visible = true;
			tabResult.Sort("0", true, true);
			tabResult.Sort("3", true, true);
			tabResult.Refresh();
			tabResult.Visible = false;
	//		actualtabResults.Visible =true;
	//		prepareActualReportdata();
		}
	
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			strSubHeader = "From: " + this.ucView1.Viewer.PeriodFrom.ToString("dd.MM.yy'/'HH:mm") + " to " + this.ucView1.Viewer.PeriodTo.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += " View: " + this.ucView1.Viewer.CurrentView;
			strSubHeader += "(Duration: " + dtDuration.Value.ToShortTimeString()+"  " +" Overnight Flights: " + NumberofFlights[1].ToString() + ")";
			
			rptFIPS rpt = new rptFIPS(actualtabResults , 
				"Statistics of Overnight Arrival Flights (summary):" , 
				strSubHeader, "", 10);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}

		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			if(actualtabResults.GetLineCount ()==0)
			{
				
				MessageBox.Show(this, "No Data to preview");
			}
			else
			{
				RunReport();
			}
		
		}

		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent();
			actualtabResults.ResetContent ();
			LoadReportData();
			PrepareFillReportData();
		//	PrepareReportDataRotations();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}

		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmOverNight_FlightSummary_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("AFT");
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			txtLens.Text = tabResult.HeaderLengthString;
		}

		private void actualtabResults_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == actualtabResults.CurrentSortColumn)
				{
					if( actualtabResults.SortOrderASC == true)
					{
						actualtabResults.DeleteLine(actualtabResults.GetLineCount()-1);
						actualtabResults.Sort(e.colNo.ToString(), false, true);
						insertTotal();
					}
					else
					{
						actualtabResults.DeleteLine(actualtabResults.GetLineCount()-1);
						actualtabResults.Sort(e.colNo.ToString(), true, true);
						insertTotal();
					}
				}
				else
				{
					actualtabResults.DeleteLine(actualtabResults.GetLineCount()-1);
					actualtabResults.Sort( e.colNo.ToString(), true, true);
				insertTotal();
				}
				actualtabResults.Refresh();
			}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			myDB.Unbind("AFT");
			this.Close ();
		}

		private void ucView1_viewEvent(string view)
		{
			btnOK_Click(this,null);
		}

	}
}
