using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
    	/// <summary>
	/// Summary description for frmStatisticsAccordingToArrivalDeparture.
	/// </summary>
	public class frmStatisticsAccordingToArrivalDeparture : System.Windows.Forms.Form
	{
		#region _My Members
		/// <summary>
		/// Filter condition for AFT load. 
		/// The current implementation doesn't display the rotational flights. To include the rotational 
		/// flight add FTYP and also include ADID type as part of the Arrival / Departure display
		/// 
		/// </summary>
		private string strAFTWhereRaw = "WHERE ( (STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@DEST') " +
								"OR (STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@ORIG') ) "+
								"AND (FTYP='O' OR FTYP='S') ";
		/// <summary>
		/// Filter condition for CCA load
		/// </summary>
		private string strCCAWhereRaw = "where FLNU in (@@FLNU)";
		/// <summary>
		/// Filter condition for APX load
		/// </summary>
		private string strAPXWhereRaw = "where FLNU in (@@FLNU)";
		/// <summary>
		/// Filter condition for LOA load
		/// </summary>
		private string strLOAWhereRaw = "where FLNU in (@@FLNU) "+
										"AND DSSN IN ('USR','LDM','MVT','KRI') "+
										"AND TYPE IN ('PAX','PAD','LOA') "+
										"AND APC3=' '";
		/// <summary>
		/// Single object of the internal memory object.
		/// </summary>
		private IDatabase myDB;

		/// <summary>
		/// Heading of the columns as it needs to be in the report output - 21 Columns
		/// </summary>
		private string strTabHeaderArrival = "Flight ,ORG ,STA ,ATA ,A/C ,REG ,POS ,Gate ,Belt ,F ,C ,Y ,Inf ,Total ,ID ,TR ,TRF ,BAG ,CGO ,MAIL ,Total";

		/// <summary>
		/// The logical mapping of the columns
		/// </summary>
		private string strLogicalFieldsArrival = "FLNO,ORG,STOA,ATA,AC_TYP,REG,POS,GATE,BELT,PAX_F,PAX_C,PAX_Y,PAX_INF,P_TTL,ID,TR,TRF,BAG,CGO,MAIL,TTL";

		/// <summary>
		/// The length of each column
		/// </summary>
		private string strTabHeaderLensArrival = "60,30,50,50,40,50,40,40,80,40,40,40,40,40,40,40,40,40,40,40,40";

		/// <summary>
		/// Heading of the columns as it needs to be in the report output - 21 Columns
		/// </summary>
		private string strTabHeaderDeparture = "Flight ,DES ,STD ,ATD ,A/C ,REG ,Cki ,Gate ,POS ,F ,C ,Y ,Inf ,Total ,ID ,TR ,TRF ,BAG ,CGO ,MAIL ,Total";

		/// <summary>
		/// The logical mapping of the columns
		/// </summary>
		private string strLogicalFieldsDeparture = "FLNO,DES,STOD,ATD,AC_TYP,REG,CKIN,GATE,POS,PAX_F,PAX_C,PAX_Y,PAX_INF,P_TTL,ID,TR,TRF,BAG,CGO,MAIL,TTL";
		
		/// <summary>
		/// The length of each column
		/// </summary>
		private String strTabHeaderLensDeparture = "60,30,50,50,40,40,80,40,40,40,40,40,40,40,40,40,40,40,40,40,40";

		/// <summary>
		/// Instance of the table
		/// </summary>
		ITable myAFT;
		ITable myCCA;
		ITable myAPX;
		ITable myLOA;
		/// <summary>
		/// Count of total flights displayed on the list
		/// </summary>
		private int iTotalFlights = 0;

		#endregion _My Members

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RadioButton rbArrival;
		private System.Windows.Forms.RadioButton rbDeparture;
		private System.Windows.Forms.CheckBox cbLoadInfo;

		public frmStatisticsAccordingToArrivalDeparture()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmStatisticsAccordingToArrivalDeparture));
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop = new System.Windows.Forms.Panel();
			this.cbLoadInfo = new System.Windows.Forms.CheckBox();
			this.rbDeparture = new System.Windows.Forms.RadioButton();
			this.rbArrival = new System.Windows.Forms.RadioButton();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.panelTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 160);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(944, 365);
			this.panelBody.TabIndex = 3;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(944, 349);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(944, 349);
			this.tabResult.TabIndex = 0;
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(944, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.cbLoadInfo);
			this.panelTop.Controls.Add(this.rbDeparture);
			this.panelTop.Controls.Add(this.rbArrival);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(944, 160);
			this.panelTop.TabIndex = 2;
			// 
			// cbLoadInfo
			// 
			this.cbLoadInfo.Checked = true;
			this.cbLoadInfo.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbLoadInfo.Location = new System.Drawing.Point(272, 40);
			this.cbLoadInfo.Name = "cbLoadInfo";
			this.cbLoadInfo.Size = new System.Drawing.Size(144, 24);
			this.cbLoadInfo.TabIndex = 16;
			this.cbLoadInfo.Text = "Use loading Information";
			// 
			// rbDeparture
			// 
			this.rbDeparture.Location = new System.Drawing.Point(96, 56);
			this.rbDeparture.Name = "rbDeparture";
			this.rbDeparture.TabIndex = 15;
			this.rbDeparture.Text = "&Departure";
			this.rbDeparture.CheckedChanged += new System.EventHandler(this.rbDeparture_CheckedChanged);
			// 
			// rbArrival
			// 
			this.rbArrival.Checked = true;
			this.rbArrival.Location = new System.Drawing.Point(96, 32);
			this.rbArrival.Name = "rbArrival";
			this.rbArrival.TabIndex = 14;
			this.rbArrival.TabStop = true;
			this.rbArrival.Text = "&Arrival";
			this.rbArrival.CheckedChanged += new System.EventHandler(this.rbArrival_CheckedChanged);
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(412, 88);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 12;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(412, 104);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 13;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 104);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 10;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 104);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 9;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 104);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "&Close";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 104);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 8;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(268, 4);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(92, 4);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			// 
			// frmStatisticsAccordingToArrivalDeparture
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(944, 525);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Name = "frmStatisticsAccordingToArrivalDeparture";
			this.Text = "Statistics according To Arrival/Departure";
			this.Load += new System.EventHandler(this.frmStatisticsAccordingToArrivalDeparture_Load);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.panelTop.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmStatisticsAccordingToArrivalDeparture_Load(object sender, System.EventArgs e)
		{	
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
		}

		/// <summary>
		/// Initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}

		/// <summary>
		/// Initializes the tab control
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			if (rbArrival.Checked == true) 
			{
				tabResult.HeaderString = strTabHeaderArrival;
				tabResult.LogicalFieldList = strLogicalFieldsArrival;
				tabResult.HeaderLengthString = strTabHeaderLensArrival;
				PrepareReportDataArrival();
			} 
			else 
			{
				tabResult.HeaderString = strTabHeaderDeparture;
				tabResult.LogicalFieldList = strLogicalFieldsDeparture;
				tabResult.HeaderLengthString = strTabHeaderLensDeparture;
				PrepareReportDataDeparture();
			}
		}

		/// <summary>
		/// Loads the data for the list from the data store in the following 
		/// sequence:
		/// 1. Loads the flight records from AFT (day wise)
		/// 2. Loads the Checkin Counter information corresponding to the 
		///		flight selected in step 1.
		///	3. Loads the Pax and Load information corresponding to the flights
		///		selected in step 1.
		/// 
		/// </summary>
		private void LoadReportData()
		{
			tabResult.ResetContent();
			DateTime datFrom, datTo, datReadTo, datReadFrom;
			string strDateFrom = "";
			string strDateTo = "";
			string strTmpQry = "";
			int ilTotal;
			int loopCnt = 1;
			int percent;
			StringBuilder strFlnus = new StringBuilder(10000);

			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;

			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}

			// Reading flight information
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT"
							, "URNO,FLNO,ORG3,STOA,LAND,DES3,STOD,AIRB,ACT3,REGN,PSTD,PSTA,GTA1,GTA2,GTD1,GTD2,BLT1,BLT2,ADID,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,BAGN,BAGW,MAIL,CGOT"
							, "10,9,3,14,14,3,14,14,3,12,5,5,5,5,5,5,5,5,1,3,3,3,3,3,3,6,7,6,6"
							, "URNO,FLNO,ORG3,STOA,LAND,DES3,STOD,AIRB,ACT3,REGN,PSTD,PSTA,GTA1,GTA2,GTD1,GTD2,BLT1,BLT2,ADID,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,BAGN,BAGW,MAIL,CGOT");
			
			myAFT.Clear();
			myAFT.TimeFields = "STOA,LAND,STOD,AIRB";
			myAFT.Command("read",",GFR,");
			myAFT.TimeFieldsInitiallyInUtc = true;

			TimeSpan tsDays = (datTo - datFrom);
			ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) 
				ilTotal = 1;

			progressBar1.Value = 0;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			datReadFrom = datFrom;
			do
			{
				percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) 
					datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);

				strTmpQry = strAFTWhereRaw;
				strTmpQry = strTmpQry.Replace("@@FROM",strDateFrom);
				strTmpQry = strTmpQry.Replace("@@TO",strDateTo);
				strTmpQry = strTmpQry.Replace("@@ORIG",UT.Hopo);
				strTmpQry = strTmpQry.Replace("@@DEST",UT.Hopo);

				myAFT.Load(strTmpQry);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			} while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();

			LoadCheckInCounterData();
			LoadPaxAndLoadData();
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
		}
		
		/// <summary>
		/// Loads the Check-In counter data corresponding to the flights 
		/// selected from the AFT table.
		/// </summary>
		private void LoadCheckInCounterData() 
		{
			int ilTotal = 0;
			int loopCnt = 0;
			int percent = 0;
			StringBuilder strFlnus = new StringBuilder();
			string strTmpQry;

			progressBar1.Value = 0;
			lblProgress.Text = "Loading Counter Data";
			lblProgress.Refresh();
			progressBar1.Show();
			myDB.Unbind("CCA");
			myCCA = myDB.Bind("CCA", "CCA"
								, "FLNU,CKIC"
								, "10,5"
								, "FLNU,CKIC");
			myCCA.Clear();

			ilTotal = (int)(myAFT.Count/300);
			if(ilTotal == 0) 
				ilTotal = 1;
			loopCnt = 0;
			for(int i = 0; i < myAFT.Count; i++)
			{
				strFlnus.Append(myAFT[i]["URNO"]).Append(",");
				if((i % 300) == 0 && i > 0)
				{
					loopCnt++;
					percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;

					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strCCAWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myCCA.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
			}
			if(strFlnus.Length > 0)
			{
				strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
				strTmpQry = strCCAWhereRaw;
				strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
				myCCA.Load(strTmpQry);
				strFlnus.Remove(0,strFlnus.Length);
			}
			myCCA.CreateIndex("FLNU", "FLNU");
			lblProgress.Text = "";
			progressBar1.Hide();
		}

		/// <summary>
		/// Loads the Pax Count and Load information for the flights 
		/// selected from AFT table.
		/// </summary>
		private void LoadPaxAndLoadData()
		{
			int ilTotal = 0;
			int loopCnt = 0;
			int percent = 0;
			string strTmpQry;
			StringBuilder strFlnus = new StringBuilder();

			progressBar1.Value = 0;
			lblProgress.Text = "Loading Load Data";
			lblProgress.Refresh();
			progressBar1.Show();

			if (cbLoadInfo.Checked) 
			{
				myDB.Unbind("LOA");
				myLOA = myDB.Bind("LOA","LOA"
					,"FLNU,DSSN,TYPE,STYP,SSTP,SSST,VALU"
					,"10,10,14,40,1,3,3,3,3,6"
					,"FLNU,DSSN,TYPE,STYP,SSTP,SSST,VALU");
				myLOA.Clear();

				ilTotal = (int)(myAFT.Count/300);
				if(ilTotal == 0) 
					ilTotal = 1;
				loopCnt = 0;
				for(int i = 0; i < myAFT.Count; i++)
				{
					strFlnus.Append(myAFT[i]["URNO"]).Append(",");
					if((i % 300) == 0 && i > 0)
					{
						loopCnt++;
						percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
						if (percent > 100) 
							percent = 100;
						progressBar1.Value = percent;

						strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
						strTmpQry = strLOAWhereRaw;
						strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
						myLOA.Load(strTmpQry);
						strFlnus.Remove(0,strFlnus.Length);
					}
				}
				if(strFlnus.Length > 0)
				{
					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strLOAWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myLOA.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
				myLOA.CreateIndex("FLNU", "FLNU");
				lblProgress.Text = "";
				progressBar1.Hide();
			}
			else
			{
				myDB.Unbind("APX");
				myAPX = myDB.Bind("APX","APX"
					,"FLNU,URNO,TYPE,PAXC"
					,"10,10,3,6"
					,"FLNU,URNO,TYPE,PAXC");
				myAPX.Clear();

				ilTotal = (int)(myAFT.Count/300);
				if(ilTotal == 0) 
					ilTotal = 1;
				loopCnt = 0;
				for(int i = 0; i < myAFT.Count; i++)
				{
					strFlnus.Append(myAFT[i]["URNO"]).Append(",");
					if((i % 300) == 0 && i > 0)
					{
						loopCnt++;
						percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
						if (percent > 100) 
							percent = 100;
						progressBar1.Value = percent;

						strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
						strTmpQry = strAPXWhereRaw;
						strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
						myAPX.Load(strTmpQry);
						strFlnus.Remove(0,strFlnus.Length);
					}
				}
				if(strFlnus.Length > 0)
				{
					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strAPXWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myAPX.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
				myAPX.CreateIndex("FLNU", "FLNU");
				lblProgress.Text = "";
				progressBar1.Hide();
			}
		}
		/// <summary>
		/// Returns the correct load value for type, styp, sstp,ssst
		/// according to the priority.
		/// </summary>
		/// <param name="dType">The TYPE of LOATAB</param>
		/// <param name="strAftUrno">Urno of AFT</param>
		/// <param name="styp">STYP of LOATAB</param>
		/// <param name="sstp">SSTP of LOATAB</param>
		/// <param name="ssst">SSST of LOATAB</param>
		/// <returns></returns>
		private string GetLoadData(string dType, string strAftUrno, string styp, string sstp, string ssst)
		{
			string strLDM = "";
			string strMVT = "";
			string strKRI = "";
			
			IRow [] rows = myLOA.RowsByIndexValue("FLNU", strAftUrno);
			for(int i = 0; i < rows.Length; i++)
			{
				if (rows[i]["TYPE"].Equals(dType))
				{
					if ( rows[i]["STYP"].Equals(styp) 
							&& rows[i]["SSTP"].Equals(sstp) 
							&& rows[i]["SSST"].Equals(ssst))
					{
						if(rows[i]["DSSN"]=="USR")
							return rows[i]["VALU"];
						if(rows[i]["DSSN"] == "LDM")
							strLDM = rows[i]["VALU"];
						if(rows[i]["DSSN"] == "MVT")
							strMVT = rows[i]["VALU"];
						if(rows[i]["DSSN"] == "KRI")
							strKRI = rows[i]["VALU"];
					}
				}
			}
			if(strLDM != "") return strLDM;
			if(strMVT != "") return strMVT;
			if(strKRI != "") return strKRI;
			return "";
		}

		/// <summary>
		/// Formats the Departure data for display in the list.
		/// </summary>
		private void PrepareReportDataDeparture()
		{
			if (myAFT == null)
				return;
			StringBuilder strData = new StringBuilder(200);
			StringBuilder sb = new StringBuilder(100000);
			string strCknCtr;
			string strId = "";
			string strInf = "";
			iTotalFlights = 0;
			IRow [] rows;
			for (int i = 0; i<myAFT.Count ; i++ )
			{
				if (myAFT[i]["ADID"].Equals("D"))
					// || myAFT[i]["ADID"].Equals("B") 
				{
					strData.Append(myAFT[i]["FLNO"]).Append(",");
					strData.Append(myAFT[i]["DES3"]).Append(",");
					strData.Append(Helper.DateString(myAFT[i]["STOD"],"dd'/'HH:mm")).Append(",");
					strData.Append(Helper.DateString(myAFT[i]["AIRB"],"dd'/'HH:mm")).Append(",");
					strData.Append(myAFT[i]["ACT3"]).Append(",");
					strData.Append(myAFT[i]["REGN"]).Append(",");

					rows = myCCA.RowsByIndexValue("FLNU", myAFT[i]["URNO"]);
					strCknCtr = "";
					for(int j = 0; j < rows.Length; j++)
						strCknCtr += rows[j]["CKIC"] + "/ ";

					if(rows.Length == 0)
						strData.Append(",");
					else
						strData.Append(strCknCtr.Substring(0,strCknCtr.Length-2)).Append(",");
					if ( !myAFT[i]["GTD2"].Equals(""))
						strData.Append(myAFT[i]["GTD1"]).Append("/").Append(myAFT[i]["GTD2"]).Append(",");
					else
						strData.Append(myAFT[i]["GTD1"]).Append(",");
					strData.Append(myAFT[i]["PSTD"]).Append(",");
					if (cbLoadInfo.Checked) 
					{
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"F","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"B","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"E","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"","","I")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","","")).Append(",");
						strData.Append(GetLoadData("PAD",myAFT[i]["URNO"],"E","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","R","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","T","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"B","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"C","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"M","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"T","",""));
					} 
					else
					{
						strData.Append(myAFT[i]["PAX1"]).Append(",");
						strData.Append(myAFT[i]["PAX2"]).Append(",");
						strData.Append(myAFT[i]["PAX3"]).Append(",");

						rows = myAPX.RowsByIndexValue("FLNU", myAFT[i]["URNO"]);
						for(int j = 0; j < rows.Length; j++)
						{
							if (rows[j]["TYPE"].Equals("INF"))
								strInf = rows[j]["PAXC"];

							if (rows[j]["TYPE"].Equals("ID"))
								strId = rows[j]["PAXC"];
						}
						strData.Append(strInf).Append(",");
						strData.Append(myAFT[i]["PAXT"]).Append(",");
						strData.Append(strId).Append(",");
						strData.Append(myAFT[i]["PAXI"]).Append(",");
						strData.Append(myAFT[i]["PAXF"]).Append(",");
						strData.Append(myAFT[i]["BAGN"]).Append(",");
						strData.Append(myAFT[i]["CGOT"]).Append(",");
						strData.Append(myAFT[i]["MAIL"]).Append(",");
						strData.Append(myAFT[i]["BAGW"]);
					}
					strData.Append("\n");
					sb.Append(strData.ToString());
					strData.Remove(0,strData.Length);
					iTotalFlights++;
				}
			}
			tabResult.InsertBuffer(sb.ToString(),"\n");
			tabResult.Sort("2", true, true);
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}

		/// <summary>
		/// Formats the Arrival data for display in the list.
		/// </summary>
		private void PrepareReportDataArrival()
		{
			if (myAFT == null)
				return;
			StringBuilder strData = new StringBuilder(200);
			StringBuilder sb = new StringBuilder(100000);
			string strId = "";
			string strInf = "";
			iTotalFlights = 0;
			IRow [] rows;

			for (int i = 0; i<myAFT.Count ; i++ )
			{
				if (myAFT[i]["ADID"] == "A") 
					// || myAFT[i]["ADID"].Equals("B") 
				{
					strData.Append(myAFT[i]["FLNO"]).Append(",");
					strData.Append(myAFT[i]["ORG3"]).Append(",");
					strData.Append(Helper.DateString(myAFT[i]["STOA"],"dd'/'HH:mm")).Append(",");
					strData.Append(Helper.DateString(myAFT[i]["LAND"],"dd'/'HH:mm")).Append(",");
					strData.Append(myAFT[i]["ACT3"]).Append(",");
					strData.Append(myAFT[i]["REGN"]).Append(",");
					strData.Append(myAFT[i]["PSTA"]).Append(",");
					if ( !myAFT[i]["GTA2"].Equals(""))
						strData.Append(myAFT[i]["GTA1"]).Append("/").Append(myAFT[i]["GTA2"]).Append(",");
					else
						strData.Append(myAFT[i]["GTA1"]).Append(",");
					if (!myAFT[i]["BLT2"].Equals(""))
							strData.Append(myAFT[i]["BLT1"]).Append("-").Append(myAFT[i]["BLT2"]).Append(",");
					else
						strData.Append(myAFT[i]["BLT1"]).Append(",");

					if (cbLoadInfo.Checked) 
					{
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"F","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"B","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"E","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"","","I")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","","")).Append(",");
						strData.Append(GetLoadData("PAD",myAFT[i]["URNO"],"E","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","R","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","T","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"B","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"C","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"M","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"T","",""));
					}
					else
					{
						strData.Append(myAFT[i]["PAX1"]).Append(",");
						strData.Append(myAFT[i]["PAX2"]).Append(",");
						strData.Append(myAFT[i]["PAX3"]).Append(",");

						rows = myAPX.RowsByIndexValue("FLNU", myAFT[i]["URNO"]);
						for(int j = 0; j < rows.Length; j++)
						{
							if (rows[j]["TYPE"].Equals("INF"))
								strInf = rows[j]["PAXC"];

							if (rows[j]["TYPE"].Equals("ID"))
								strId = rows[j]["PAXC"];
						}
						strData.Append(strInf).Append(",");
						strData.Append(myAFT[i]["PAXT"]).Append(",");
						strData.Append(strId).Append(",");
						strData.Append(myAFT[i]["PAXI"]).Append(",");
						strData.Append(myAFT[i]["PAXF"]).Append(",");
						strData.Append(myAFT[i]["BAGN"]).Append(",");
						strData.Append(myAFT[i]["CGOT"]).Append(",");
						strData.Append(myAFT[i]["MAIL"]).Append(",");
						strData.Append(myAFT[i]["BAGW"]);
					}
					strData.Append("\n");

					sb.Append(strData.ToString());
					strData.Remove(0,strData.Length);
					iTotalFlights++;
				}
			}
			tabResult.InsertBuffer(sb.ToString(),"\n");
			tabResult.Sort("2", true, true);
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}

		/// <summary>
		/// Generates the report in a popup window
		/// 
		/// Format - A4 Landscape
		/// </summary>
		private void RunReport() 
		{
			StringBuilder strSubHeader = new StringBuilder();

			//strSubHeader.Append("Load and Pax Report with ");
			if (rbArrival.Checked)
				strSubHeader.Append("Arrival ");
			else
				strSubHeader.Append("Departure ");

			strSubHeader.Append("From: ")
						.Append(dtFrom.Value.ToString("dd.MM.yy'/'HH:mm"))
						.Append(" To: ")
						.Append(dtTo.Value.ToString("dd.MM.yy'/'HH:mm"));
			strSubHeader.Append(" (Flights: ").Append(iTotalFlights.ToString());
			if (rbArrival.Checked)
				strSubHeader.Append("/ARR:").Append(iTotalFlights.ToString())
							.Append(" /DEP:0)"); 
			else
				strSubHeader.Append("/ARR:0")
							.Append(" /DEP:").Append(iTotalFlights.ToString()).Append(")"); 

			//rptA3_Landscape rpt = new rptA3_Landscape(tabResult, "Statistics - Arrival / Departure", strSubHeader.ToString(), "", 8);
			prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult,"Statistics - Arrival/Departure",strSubHeader.ToString(),"",8);
			rpt.TextBoxCanGrow = false;
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}

		/// <summary>
		/// Validates the user entry for the following fields:
		/// 1. FROM and TO fields
		/// 
		/// Displays the error message.
		/// </summary>
		/// <returns>TRUE - If entries are correct, else FALSE</returns>
		private bool validEntry() 
		{
			string strErr = "";
			if (dtFrom.Value > dtTo.Value)
				strErr = "Date From is later than Date To!\n";

			if (strErr.Equals(""))
				return true;
			else 
			{
				MessageBox.Show(this, strErr
					,"FIPS Reports" ,MessageBoxButtons.OK ,MessageBoxIcon.Error);
				return false;
			}
		}
		
		private void rbArrival_CheckedChanged(object sender, System.EventArgs e)
		{
			InitTab();
		}

		private void rbDeparture_CheckedChanged(object sender, System.EventArgs e)
		{
			InitTab();
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			if(validEntry())
			{
				this.Cursor = Cursors.WaitCursor;
				LoadReportData();
				if(this.rbArrival.Checked == false)
					PrepareReportDataDeparture();
				else
					PrepareReportDataArrival();
				this.Cursor = Cursors.Arrow;
			}
		}

		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}

		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			if(validEntry())
			{
				this.Cursor = Cursors.WaitCursor;
				LoadReportData();
				if(this.rbArrival.Checked == false)
					PrepareReportDataDeparture();
				else
					PrepareReportDataArrival();
				RunReport();
				this.Cursor = Cursors.Arrow;
			}
		}
	}
}
