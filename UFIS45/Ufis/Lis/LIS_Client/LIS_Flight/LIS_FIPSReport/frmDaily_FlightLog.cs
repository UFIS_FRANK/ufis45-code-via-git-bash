using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmDaily_FlightLog.
	/// </summary>
	public class frmDaily_FlightLog : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		//private string strWhereRaw = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO')) AND (FTYP<>'S' AND FTYP<>'G' AND FTYP<>'T')" ;
		private string strWhereRaw = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO') OR " + 
									 "(STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO')) AND (FTYP='O' OR FTYP='S')" ;
		/// <summary>
		/// Where statement for the LOATAB. @@FLNU will be replaced by the
		/// AFT.URNO list.
		/// </summary>
		private string strWhereLoa = "WHERE FLNU IN (@@FLNU) AND TYPE='PAX' AND STYP='T' AND SSTP=' ' AND SSST=' '";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes ==> for Rotation
		/// </summary>
		private string strLogicalFieldsRotation = "Sort_key,Arrival_A,Org_A,Via_A,Na_A,Sta_A,Eta_A,Land_A,Onbl_A,Pax_T_A,Pos_A,Gate_A,Belt_A,Ftp_A,A_C_A,Reg_A,Depart__D,Des_D,Via_D,Na_D,Std_D,Etd_D,Slot_D,Ofbl_D,Airb_D,Del1_D,Min1_D,Del2_D,Min2_D,Pax_T_D,Pos_D,Gate_D,Wro_D,Rem__D";
		/// The header columns, which must be used for the report output
		/// for rotation
		/// </summary>
		private string strTabHeaderRotation = "sort,Arrival,Org,Via,Na,STA,ETA,Land,Onbl,Pax T,Pos,Gate,Belt,Ftp,A/C,Reg,Depart.,Des,Via,Na,STD,ETD,Slot,Ofbl,Airb,Del1,Min,Del2,Min,Pax T,Pos,Gate,Wro,Rem.";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// for rotation
		/// </summary>
		private string strTabHeaderLensRotation = "0,54,36,30,28,57,58,58,47,34,33,33,33,27,33,62,58,36,29,27,57,55,52,55,56,38,30,34,30,38,33,33,40,65";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the nature codes of NATTAB
		/// </summary>
		private ITable myLOA;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		#endregion _My Members

		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtLens;
		private System.Windows.Forms.Button btnLens;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmDaily_FlightLog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmDaily_FlightLog));
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop = new System.Windows.Forms.Panel();
			this.btnLens = new System.Windows.Forms.Button();
			this.txtLens = new System.Windows.Forms.TextBox();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.panelTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 80);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(796, 454);
			this.panelBody.TabIndex = 5;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(796, 438);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(796, 438);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(796, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.btnLens);
			this.panelTop.Controls.Add(this.txtLens);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(796, 80);
			this.panelTop.TabIndex = 4;
			// 
			// btnLens
			// 
			this.btnLens.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(224)), ((System.Byte)(192)));
			this.btnLens.Location = new System.Drawing.Point(680, 28);
			this.btnLens.Name = "btnLens";
			this.btnLens.TabIndex = 24;
			this.btnLens.Text = "Lens";
			this.btnLens.Visible = false;
			this.btnLens.Click += new System.EventHandler(this.btnLens_Click);
			// 
			// txtLens
			// 
			this.txtLens.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(224)), ((System.Byte)(192)));
			this.txtLens.Location = new System.Drawing.Point(404, 4);
			this.txtLens.Name = "txtLens";
			this.txtLens.Size = new System.Drawing.Size(376, 20);
			this.txtLens.TabIndex = 23;
			this.txtLens.Text = "textBox1";
			this.txtLens.Visible = false;
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(412, 28);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 22;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(412, 44);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 21;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 44);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 10;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 44);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 9;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 44);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 8;
			this.btnCancel.Text = "&Close";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 44);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 7;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(268, 4);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(92, 4);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			// 
			// frmDaily_FlightLog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(796, 534);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmDaily_FlightLog";
			this.Text = "Daily Flight Log ...";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmDaily_FlightLog_Closing);
			this.Load += new System.EventHandler(this.frmDaily_FlightLog_Load);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.panelTop.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion


		/// <summary>
		/// Loads the from and inits the necessary things.
		/// </summary>
		/// <param name="sender">Originator of the method call</param>
		/// <param name="e">Event args</param>
		private void frmDaily_FlightLog_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
			PrepareFilterData();
			//tabResult.AutoSizeColumns();
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}
		/// <summary>
		/// Initializes the tab control.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString = strTabHeaderRotation;
			tabResult.LogicalFieldList = strLogicalFieldsRotation;
			tabResult.HeaderLengthString = strTabHeaderLensRotation;
			PrepareReportDataRotations();
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
		}
		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportDataRotations();
			//RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere = strWhereRaw;
			//Clear the tab
			tabResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}

			//Now reading day by day the flight records in order to 
			//not stress the database too much
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", 
				"RKEY,URNO,FTYP,ADID,FLNO,CSGN,FLTI,JCNT,STOA,ETAI,LAND,ONBL,TMOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,ETDI,OFBL,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT,REGN,SLOT,AIRB,DCD1,DCD2,DTD1,DTD2,WRO1,WRO2,REM1,PAXT", 
				"10,10,1,1,9,8,1,1,14,14,14,14,14,14,1,5,1,2,3,4,3,4,2,5,5,5,5,5,1,3,14,14,14,14,1,3,4,5,5,5,5,5,12,14,14,2,2,4,4,5,5,256,7",
				"RKEY,URNO,FTYP,ADID,FLNO,CSGN,FLTI,JCNT,STOA,ETAI,LAND,ONBL,TMOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,ETDI,OFBL,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT,REGN,SLOT,AIRB,DCD1,DCD2,DTD1,DTD2,WRO1,WRO2,REM1,PAXT");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL,TMOA,ETAI,LAND,ETDI,SLOT,AIRB";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpWhere = strWhereRaw;
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				strTmpWhere = strTmpWhere.Replace("@@HOPO", UT.Hopo);
				strTmpWhere += "[ROTATIONS]";
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			//progressBar1.Hide();
			//----------------------  LOATAB Data
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

			//Filter out the FTYPs which are selected due to rotations
			for(int i = myAFT.Count-1; i >= 0; i--)
			{
				if(myAFT[i]["FTYP"] == "T" || myAFT[i]["FTYP"] == "G" || 
				   myAFT[i]["FTYP"] == "N" || myAFT[i]["FTYP"] == "X" ||
				   myAFT[i]["FTYP"] == "B" ||  myAFT[i]["FTYP"] == "Z" )
				{
					myAFT.Remove(i);
				}
			}

			myDB.Unbind("LOA");
			myLOA = myDB.Bind("LOA", "LOA", "URNO,FLNU,DSSN,VALU", "10,10,3,6", "URNO,FLNU,DSSN,VALU");

			lblProgress.Text = "Loading Pax Data";
			lblProgress.Refresh();
			int loops = 0;
			progressBar1.Value = 0;
			progressBar1.Refresh();
			ilTotal = (int)(myAFT.Count/300);
			if(ilTotal == 0) ilTotal = 1;
			string strFlnus = "";
			strTmpWhere = "";
			for(int i = 0; i < myAFT.Count; i++)
			{
				strFlnus += myAFT[i]["URNO"] + ",";
				if((i % 300) == 0 && i > 0)
				{
					loops++;
					int percent = Convert.ToInt32((loops * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;
					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpWhere = strWhereLoa;
					strTmpWhere = strTmpWhere.Replace("@@FLNU", strFlnus);
					myLOA.Load(strTmpWhere);
					strFlnus = "";
				}
			}
			//Load the rest of the GPAs.
			if(strFlnus.Length > 0)
			{
				strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
				strTmpWhere = strWhereLoa;
				strTmpWhere = strTmpWhere.Replace("@@FLNU", strFlnus);
				myLOA.Load(strTmpWhere);
				strFlnus = "";
			}
			progressBar1.Hide();
			lblProgress.Text = "";
			myLOA.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
			myLOA.CreateIndex("FLNU", "FLNU");
		}

		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// First organize the rotations and then put the result
		/// into the tabResult
		/// </summary>
		private void PrepareReportDataRotations()
		{
			if(myAFT == null)
				return;

			imArrivals=0;
			imDepartures=0;
			imTotalFlights=0;
			myAFT.Sort("RKEY", true);
			int llCurr = 0;
			int ilStep = 1;
			string strCurrAdid = "";
			string strNextAdid = "";
			string strCurrRkey = "";
			string strNextRkey = "";
			StringBuilder sb = new StringBuilder(10000);
			while(llCurr < myAFT.Count)
			{
				string strValues = "";
				strCurrRkey = myAFT[llCurr]["RKEY"];
				strCurrAdid = myAFT[llCurr]["ADID"];
				if ((llCurr + 1) < myAFT.Count)
				{
					strNextRkey = myAFT[llCurr+1]["RKEY"];
					strNextAdid = myAFT[llCurr+1]["ADID"];
				}
				else
				{
					strNextRkey = "";
					strNextAdid = "";
				}
				string strGate = "";
				string strBlt = "";
				if(strCurrRkey == strNextRkey)
				{//Is a rotation
					ilStep = 2;
					if( strCurrAdid == "A" && strNextAdid == "D")
					{
						imArrivals++;
						imDepartures++;
						//Arrival,Org,Via,Na,Sta,Eta,Land,Onbl,Pax T,Pos,Gate,Belt,Ftp,A/C,Reg
						//ARR:,,,,,,,,,,,,,,,
						//DEP:,,,,,,,,,,,,,,,,,
						string strFLNO = myAFT[llCurr]["FLNO"];
						if(strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
						strValues += myAFT[llCurr]["STOA"] + myAFT[llCurr+1]["STOD"] + ",";
						strValues += strFLNO + ",";
						strValues += myAFT[llCurr]["ORG3"] + ",";
						strValues += myAFT[llCurr]["VIA3"] + ",";
						strValues += myAFT[llCurr]["TTYP"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["STOA"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["ETAI"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["LAND"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["ONBL"], "dd'/'HH:mm") + ",";
						string strPax = GetPaxData(myAFT[llCurr]["URNO"]);
						if(strPax == "") 
							strPax = myAFT[llCurr]["PAXT"];
						strValues += strPax + ",";
						strValues += myAFT[llCurr]["PSTA"] + ",";
						strGate = myAFT[llCurr]["GTA1"];
						if(strGate == "") strGate = myAFT[llCurr]["GTA2"];
						strValues += strGate + ",";
						strBlt = myAFT[llCurr]["BLT1"];
						if(strBlt == "") strBlt = myAFT[llCurr]["BLT2"];
						strValues += strBlt + ",";
						strValues += myAFT[llCurr]["FTYP"] + ",";
						strValues += myAFT[llCurr]["ACT3"] + ",";
						strValues += myAFT[llCurr]["REGN"] + ",";
						//-----------------------------------The departure part of rotation
						//Depart.,Des,Via,Na,Std,Etd,Slot,Ofbl,Airb,Del1,Min,Del2,Min,Pax T,Pos,Gate,Wro,Rem.
						//ARR:,,,,,,,,,,,,,,,
						strFLNO = myAFT[llCurr+1]["FLNO"];
						if(strFLNO == "") strFLNO = myAFT[llCurr+1]["CSGN"];
						strValues += strFLNO + ",";
						strValues += myAFT[llCurr+1]["DES3"] + ",";
						strValues += myAFT[llCurr+1]["VIA3"] + ",";
						strValues += myAFT[llCurr+1]["TTYP"] + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["STOD"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["ETDI"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["SLOT"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["OFBL"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["AIRB"], "dd'/'HH:mm") + ",";
						strValues += myAFT[llCurr+1]["DCD1"] + ",";
						strValues += myAFT[llCurr+1]["DTD1"] + ",";
						strValues += myAFT[llCurr+1]["DCD2"] + ",";
						strValues += myAFT[llCurr+1]["DTD2"] + ",";
						strPax = GetPaxData(myAFT[llCurr+1]["URNO"]);
						if(strPax == "") 
							strPax = myAFT[llCurr+1]["PAXT"];
						strValues += strPax + ",";
						strValues += myAFT[llCurr+1]["PSTD"] + ",";
						strGate = myAFT[llCurr+1]["GTD1"];
						if(strGate == "") strGate = myAFT[llCurr+1]["GTD2"];
						strValues += strGate + ",";
						strBlt = myAFT[llCurr+1]["WRO1"];
						if(strBlt == "") strBlt = myAFT[llCurr+1]["WRO2"];
						strValues += strBlt + ",";
						string strRem = myAFT[llCurr+1]["REM1"] + "-" + myAFT[llCurr]["REM1"];
//						strRem = strRem.Replace("\n", " ");
//						strRem = strRem.Replace("\r", " ");
						strValues += strRem + "\n";
						sb.Append(strValues);
					}
					if( strCurrAdid == "D" && strNextAdid == "A")
					{
						imArrivals++;
						imDepartures++;
						//The arrival part of rotation
						string strFLNO = myAFT[llCurr+1]["FLNO"];
						if(strFLNO == "") strFLNO = myAFT[llCurr+1]["CSGN"];
						strValues += myAFT[llCurr+1]["STOA"] + myAFT[llCurr]["STOD"]  + ",";
						strValues += strFLNO + ",";
						strValues += myAFT[llCurr+1]["ORG3"] + ",";
						strValues += myAFT[llCurr+1]["VIA3"] + ",";
						strValues += myAFT[llCurr+1]["TTYP"] + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["STOA"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["ETAI"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["LAND"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["ONBL"], "dd'/'HH:mm") + ",";
						string strPax = GetPaxData(myAFT[llCurr+1]["URNO"]);
						if(strPax == "") 
							strPax = myAFT[llCurr+1]["PAXT"];
						strValues += strPax + ",";
						strValues += myAFT[llCurr+1]["PSTA"] + ",";
						strGate = myAFT[llCurr+1]["GTA1"];
						if(strGate == "") strGate = myAFT[llCurr+1]["GTA2"];
						strValues += strGate + ",";
						strBlt = myAFT[llCurr+1]["BLT1"];
						if(strBlt == "") strBlt = myAFT[llCurr+1]["BLT2"];
						strValues += strBlt + ",";
						strValues += myAFT[llCurr+1]["FTYP"] + ",";
						strValues += myAFT[llCurr+1]["ACT3"] + ",";
						strValues += myAFT[llCurr+1]["REGN"] + ",";
						//-----------------------------------The departure part of rotation
						strFLNO = myAFT[llCurr]["FLNO"];
						if(strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
						strValues += strFLNO + ",";
						strValues += myAFT[llCurr]["DES3"] + ",";
						strValues += myAFT[llCurr]["VIA3"] + ",";
						strValues += myAFT[llCurr]["TTYP"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["STOD"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["ETDI"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["SLOT"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["OFBL"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["AIRB"], "dd'/'HH:mm") + ",";
						strValues += myAFT[llCurr]["DCD1"] + ",";
						strValues += myAFT[llCurr]["DTD1"] + ",";
						strValues += myAFT[llCurr]["DCD2"] + ",";
						strValues += myAFT[llCurr]["DTD2"] + ",";
						strPax = GetPaxData(myAFT[llCurr]["URNO"]);
						if(strPax == "") 
							strPax = myAFT[llCurr]["PAXT"];
						strValues += strPax + ",";
						strValues += myAFT[llCurr]["PSTD"] + ",";
						strGate = myAFT[llCurr]["GTD1"];
						if(strGate == "") strGate = myAFT[llCurr]["GTD2"];
						strValues += strGate + ",";
						strBlt = myAFT[llCurr]["WRO1"];
						if(strBlt == "") strBlt = myAFT[llCurr]["WRO2"];
						strValues += strBlt + ",";
						string strRem = myAFT[llCurr+1]["REM1"] + "-" + myAFT[llCurr]["REM1"];
//						strRem = strRem.Replace("\n", " ");
//						strRem = strRem.Replace("\r", " ");
						strValues += strRem + "\n";
						sb.Append(strValues);
					}

				}//if(strCurrRkey == strNextRkey)
				else
				{
					ilStep = 1;
					if( strCurrAdid == "A")
					{
						imArrivals++;
						string strFLNO = myAFT[llCurr]["FLNO"];
						if(strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
						strValues += myAFT[llCurr]["STOA"] + ",";
						strValues += strFLNO + ",";
						strValues += myAFT[llCurr]["ORG3"] + ",";
						strValues += myAFT[llCurr]["VIA3"] + ",";
						strValues += myAFT[llCurr]["TTYP"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["STOA"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["ETAI"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["LAND"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["ONBL"], "dd'/'HH:mm") + ",";
						string strPax = GetPaxData(myAFT[llCurr]["URNO"]);
						if(strPax == "") 
							strPax = myAFT[llCurr]["PAXT"];
						strValues += strPax + ",";
						strValues += myAFT[llCurr]["PSTA"] + ",";
						strGate = myAFT[llCurr]["GTA1"];
						if(strGate == "") strGate = myAFT[llCurr]["GTA2"];
						strValues += strGate + ",";
						strBlt = myAFT[llCurr]["BLT1"];
						if(strBlt == "") strBlt = myAFT[llCurr]["BLT2"];
						strValues += strBlt + ",";
						strValues += myAFT[llCurr]["FTYP"] + ",";
						strValues += myAFT[llCurr]["ACT3"] + ",";
						strValues += myAFT[llCurr]["REGN"] + ",";
						strValues += ",,,,,,,,,,,,,,,," + myAFT[llCurr]["REM1"] + ",\n";
						sb.Append(strValues);
					}
					if( strCurrAdid == "D")
					{
						imDepartures++;
						strValues += myAFT[llCurr]["STOD"] + ",,,,,,,,,,,,,,,,";
						string strFLNO = myAFT[llCurr]["FLNO"];
						if(strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
						//strValues += myAFT[llCurr]["STOD"] + ",";
						strValues += strFLNO + ",";
						strValues += myAFT[llCurr]["DES3"] + ",";
						strValues += myAFT[llCurr]["VIA3"] + ",";
						strValues += myAFT[llCurr]["TTYP"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["STOD"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["ETDI"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["SLOT"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["OFBL"], "dd'/'HH:mm") + ",";
						strValues += Helper.DateString(myAFT[llCurr]["AIRB"], "dd'/'HH:mm") + ",";
						strValues += myAFT[llCurr]["DCD1"] + ",";
						strValues += myAFT[llCurr]["DTD1"] + ",";
						strValues += myAFT[llCurr]["DCD2"] + ",";
						strValues += myAFT[llCurr]["DTD2"] + ",";
						string strPax = GetPaxData(myAFT[llCurr]["URNO"]);
						if(strPax == "") 
							strPax = myAFT[llCurr]["PAXT"];
						strValues += strPax + ",";
						strValues += myAFT[llCurr]["PSTD"] + ",";
						strGate = myAFT[llCurr]["GTD1"];
						if(strGate == "") strGate = myAFT[llCurr]["GTD2"];
						strValues += strGate + ",";
						strBlt = myAFT[llCurr]["WRO1"];
						if(strBlt == "") strBlt = myAFT[llCurr]["WRO2"];
						strValues += strBlt + ",";
						string strRem = myAFT[llCurr]["REM1"];
//						strRem = strRem.Replace("\n", " ");
//						strRem = strRem.Replace("\r", " ");
						strValues += strRem + "\n";
						sb.Append(strValues);
					}
				}
				llCurr = llCurr + ilStep;
			}//while(llCurr < myAFT.Count)
			tabResult.InsertBuffer(sb.ToString(), "\n");
			lblProgress.Text = "";
			progressBar1.Visible = false;
			//tabResult.Sort("4,19", true, true);
			tabResult.Sort("0", true, true);
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}

		private string GetPaxData(string strAftUrno)
		{
			string strLDM = "";
			string strMVT = "";
			string strKRI = "";
			
			IRow [] rows = myLOA.RowsByIndexValue("FLNU", strAftUrno);
			for(int i = 0; i < rows.Length; i++)
			{
				if(rows[i]["DSSN"] == "USR")
					return rows[i]["VALU"];
				if(rows[i]["DSSN"] == "LDM")
					strLDM = rows[i]["VALU"];
				if(rows[i]["DSSN"] == "MVT")
					strMVT = rows[i]["VALU"];
				if(rows[i]["DSSN"] == "KRI")
					strKRI = rows[i]["VALU"];
			}
			if(strLDM != "") return strLDM;
			if(strMVT != "") return strMVT;
			if(strKRI != "") return strKRI;
			return "";
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			imTotalFlights = imArrivals + imDepartures;
			string strSubHeader = "";
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
							dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " ARR: " + imArrivals.ToString();
			strSubHeader += " DEP: " + imDepartures.ToString() + ")";

			rptA3_Landscape rpt = new rptA3_Landscape(tabResult, "Daily Flight Log", strSubHeader, "", 8);
			rpt.TextBoxCanGrow = false;
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}

		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}


		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportDataRotations();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmDaily_FlightLog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("AFT");
			myDB.Unbind("LOA");
		}

		private void btnLens_Click(object sender, System.EventArgs e)
		{
			txtLens.Text = tabResult.HeaderLengthString;
		}
	}
}
