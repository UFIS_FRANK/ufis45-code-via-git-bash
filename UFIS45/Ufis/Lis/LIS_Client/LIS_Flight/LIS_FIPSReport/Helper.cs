using System;
using Ufis.Utils;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for Helper.
	/// </summary>
	public class Helper
	{
		public Helper()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		/// <summary>
		/// Converts a ceda date in a readable string according to the format.
		/// </summary>
		/// <param name="pDateString">The ceda string</param>
		/// <param name="strFormat">The format string</param>
		/// <returns>A readable string or ""</returns>
		public static string DateString(string CedaString, string strFormat)
		{
			string strRet = "";
			if (CedaString == null)
				return strRet;

			strRet = CedaString;

			// add seconds in case of using ETAI or ETDI
			if (strRet.Length == 12)
				strRet += "00";

			if (strRet != null && strRet != "")
			{
				DateTime tmpDate;
				tmpDate = UT.CedaFullDateToDateTime(strRet);
				strRet = tmpDate.ToString(strFormat);
			}
			else
			{
				strRet = "";
			}
			return strRet;
		}

		/// <summary>
		/// Converts a ceda date in a readable string according to the format.
		/// </summary>
		/// <param name="pDateString">The ceda string</param>
		/// <param name="strFormat">The format string</param>
		/// <returns>A readable string or ""</returns>
		public static string DateString(string CedaScheduledString, string CedaActualString,string strFormat)
		{
			string strScheduled = "";
			strScheduled = CedaScheduledString;

			// add seconds in case of using ETAI or ETDI
			if (strScheduled.Length == 12)
				strScheduled += "00";

			string strActual = "";
			strActual = CedaActualString;

			// add seconds in case of using ETAI or ETDI
			if (strActual.Length == 12)
				strActual += "00";

			string strRet = "";

			if (strActual != "")
			{
				DateTime tmpActual;
				tmpActual = UT.CedaFullDateToDateTime(strActual);
				if (strScheduled != "")
				{
					DateTime tmpScheduled = UT.CedaFullDateToDateTime(strScheduled);
					if (tmpScheduled.Date != tmpActual.Date)
					{
						TimeSpan diff = tmpActual.Date - tmpScheduled.Date;
						int days = (int)diff.Days;
						string strExtFormat = strFormat;
						if (days > 0)
							strExtFormat += "+" + days.ToString();
						else
							strExtFormat += "-" + days.ToString();
						strRet = tmpActual.ToString(strExtFormat);
					}
					else
					{
						strRet = tmpActual.ToString(strFormat);
					}
				}
				else
				{
					strRet = tmpActual.ToString(strFormat);
				}
			}
			return strRet;
		}
	}
}
