using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using Ufis.Utils;
using Ufis.Data;

namespace UserControls
{
	public struct DataField
	{
		public string name;
		public string description;
		public string type;
	}

	/// <summary>
	/// Summary description for ucGeneralFilter.
	/// </summary>
	public class ucGeneralFilter : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label lblFilter;
		private System.Windows.Forms.Label lblDataField;
		private System.Windows.Forms.ComboBox cbDataField;
		private System.Windows.Forms.Label lblOperator;
		private System.Windows.Forms.GroupBox grpFunction;
		private System.Windows.Forms.RadioButton rbAnd;
		private System.Windows.Forms.RadioButton rbOr;
		private System.Windows.Forms.Label lblCondition;
		private System.Windows.Forms.TextBox txtCondition;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnSet;
		private System.ComponentModel.IContainer components;

		private	ViewFilter					filter	= null;
		private System.Windows.Forms.ListBox lbFilter;
		private System.Windows.Forms.ComboBox cbOperator;
		private ViewFilter.GeneralFilterRow view	= null;
		private	DataField[]					fields	= null;
		private System.Windows.Forms.ToolTip toolTip1;
		private	System.Globalization.DateTimeFormatInfo	dtfi = new DateTimeFormatInfo();

		public ucGeneralFilter()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			this.cbOperator.Items.Add(" = ");
			this.cbOperator.Items.Add(" <= ");
			this.cbOperator.Items.Add(" >= ");
			this.cbOperator.Items.Add(" < ");
			this.cbOperator.Items.Add(" > ");
			this.cbOperator.Items.Add(" <> ");
			this.cbOperator.Items.Add(" LIKE ");
			this.cbOperator.Items.Add("EMPTY");

			this.cbOperator.Text = "EMPTY";

			dtfi.FullDateTimePattern = "dd.MM.yyyy HH:mm";
			dtfi.LongDatePattern	 = "dd.MM.yyyy";
			dtfi.ShortDatePattern	 = "dd.MM.yyyy";

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ucGeneralFilter));
			this.lblFilter = new System.Windows.Forms.Label();
			this.lbFilter = new System.Windows.Forms.ListBox();
			this.lblDataField = new System.Windows.Forms.Label();
			this.cbDataField = new System.Windows.Forms.ComboBox();
			this.cbOperator = new System.Windows.Forms.ComboBox();
			this.lblOperator = new System.Windows.Forms.Label();
			this.grpFunction = new System.Windows.Forms.GroupBox();
			this.rbOr = new System.Windows.Forms.RadioButton();
			this.rbAnd = new System.Windows.Forms.RadioButton();
			this.lblCondition = new System.Windows.Forms.Label();
			this.txtCondition = new System.Windows.Forms.TextBox();
			this.btnNew = new System.Windows.Forms.Button();
			this.btnSet = new System.Windows.Forms.Button();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.grpFunction.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblFilter
			// 
			this.lblFilter.AccessibleDescription = resources.GetString("lblFilter.AccessibleDescription");
			this.lblFilter.AccessibleName = resources.GetString("lblFilter.AccessibleName");
			this.lblFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblFilter.Anchor")));
			this.lblFilter.AutoSize = ((bool)(resources.GetObject("lblFilter.AutoSize")));
			this.lblFilter.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblFilter.Dock")));
			this.lblFilter.Enabled = ((bool)(resources.GetObject("lblFilter.Enabled")));
			this.lblFilter.Font = ((System.Drawing.Font)(resources.GetObject("lblFilter.Font")));
			this.lblFilter.Image = ((System.Drawing.Image)(resources.GetObject("lblFilter.Image")));
			this.lblFilter.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFilter.ImageAlign")));
			this.lblFilter.ImageIndex = ((int)(resources.GetObject("lblFilter.ImageIndex")));
			this.lblFilter.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblFilter.ImeMode")));
			this.lblFilter.Location = ((System.Drawing.Point)(resources.GetObject("lblFilter.Location")));
			this.lblFilter.Name = "lblFilter";
			this.lblFilter.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblFilter.RightToLeft")));
			this.lblFilter.Size = ((System.Drawing.Size)(resources.GetObject("lblFilter.Size")));
			this.lblFilter.TabIndex = ((int)(resources.GetObject("lblFilter.TabIndex")));
			this.lblFilter.Text = resources.GetString("lblFilter.Text");
			this.lblFilter.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFilter.TextAlign")));
			this.toolTip1.SetToolTip(this.lblFilter, resources.GetString("lblFilter.ToolTip"));
			this.lblFilter.Visible = ((bool)(resources.GetObject("lblFilter.Visible")));
			// 
			// lbFilter
			// 
			this.lbFilter.AccessibleDescription = resources.GetString("lbFilter.AccessibleDescription");
			this.lbFilter.AccessibleName = resources.GetString("lbFilter.AccessibleName");
			this.lbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lbFilter.Anchor")));
			this.lbFilter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lbFilter.BackgroundImage")));
			this.lbFilter.ColumnWidth = ((int)(resources.GetObject("lbFilter.ColumnWidth")));
			this.lbFilter.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lbFilter.Dock")));
			this.lbFilter.Enabled = ((bool)(resources.GetObject("lbFilter.Enabled")));
			this.lbFilter.Font = ((System.Drawing.Font)(resources.GetObject("lbFilter.Font")));
			this.lbFilter.HorizontalExtent = ((int)(resources.GetObject("lbFilter.HorizontalExtent")));
			this.lbFilter.HorizontalScrollbar = ((bool)(resources.GetObject("lbFilter.HorizontalScrollbar")));
			this.lbFilter.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lbFilter.ImeMode")));
			this.lbFilter.IntegralHeight = ((bool)(resources.GetObject("lbFilter.IntegralHeight")));
			this.lbFilter.ItemHeight = ((int)(resources.GetObject("lbFilter.ItemHeight")));
			this.lbFilter.Location = ((System.Drawing.Point)(resources.GetObject("lbFilter.Location")));
			this.lbFilter.Name = "lbFilter";
			this.lbFilter.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lbFilter.RightToLeft")));
			this.lbFilter.ScrollAlwaysVisible = ((bool)(resources.GetObject("lbFilter.ScrollAlwaysVisible")));
			this.lbFilter.Size = ((System.Drawing.Size)(resources.GetObject("lbFilter.Size")));
			this.lbFilter.TabIndex = ((int)(resources.GetObject("lbFilter.TabIndex")));
			this.toolTip1.SetToolTip(this.lbFilter, resources.GetString("lbFilter.ToolTip"));
			this.lbFilter.Visible = ((bool)(resources.GetObject("lbFilter.Visible")));
			// 
			// lblDataField
			// 
			this.lblDataField.AccessibleDescription = resources.GetString("lblDataField.AccessibleDescription");
			this.lblDataField.AccessibleName = resources.GetString("lblDataField.AccessibleName");
			this.lblDataField.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblDataField.Anchor")));
			this.lblDataField.AutoSize = ((bool)(resources.GetObject("lblDataField.AutoSize")));
			this.lblDataField.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblDataField.Dock")));
			this.lblDataField.Enabled = ((bool)(resources.GetObject("lblDataField.Enabled")));
			this.lblDataField.Font = ((System.Drawing.Font)(resources.GetObject("lblDataField.Font")));
			this.lblDataField.Image = ((System.Drawing.Image)(resources.GetObject("lblDataField.Image")));
			this.lblDataField.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblDataField.ImageAlign")));
			this.lblDataField.ImageIndex = ((int)(resources.GetObject("lblDataField.ImageIndex")));
			this.lblDataField.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblDataField.ImeMode")));
			this.lblDataField.Location = ((System.Drawing.Point)(resources.GetObject("lblDataField.Location")));
			this.lblDataField.Name = "lblDataField";
			this.lblDataField.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblDataField.RightToLeft")));
			this.lblDataField.Size = ((System.Drawing.Size)(resources.GetObject("lblDataField.Size")));
			this.lblDataField.TabIndex = ((int)(resources.GetObject("lblDataField.TabIndex")));
			this.lblDataField.Text = resources.GetString("lblDataField.Text");
			this.lblDataField.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblDataField.TextAlign")));
			this.toolTip1.SetToolTip(this.lblDataField, resources.GetString("lblDataField.ToolTip"));
			this.lblDataField.Visible = ((bool)(resources.GetObject("lblDataField.Visible")));
			// 
			// cbDataField
			// 
			this.cbDataField.AccessibleDescription = resources.GetString("cbDataField.AccessibleDescription");
			this.cbDataField.AccessibleName = resources.GetString("cbDataField.AccessibleName");
			this.cbDataField.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbDataField.Anchor")));
			this.cbDataField.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbDataField.BackgroundImage")));
			this.cbDataField.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbDataField.Dock")));
			this.cbDataField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbDataField.Enabled = ((bool)(resources.GetObject("cbDataField.Enabled")));
			this.cbDataField.Font = ((System.Drawing.Font)(resources.GetObject("cbDataField.Font")));
			this.cbDataField.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbDataField.ImeMode")));
			this.cbDataField.IntegralHeight = ((bool)(resources.GetObject("cbDataField.IntegralHeight")));
			this.cbDataField.ItemHeight = ((int)(resources.GetObject("cbDataField.ItemHeight")));
			this.cbDataField.Location = ((System.Drawing.Point)(resources.GetObject("cbDataField.Location")));
			this.cbDataField.MaxDropDownItems = ((int)(resources.GetObject("cbDataField.MaxDropDownItems")));
			this.cbDataField.MaxLength = ((int)(resources.GetObject("cbDataField.MaxLength")));
			this.cbDataField.Name = "cbDataField";
			this.cbDataField.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbDataField.RightToLeft")));
			this.cbDataField.Size = ((System.Drawing.Size)(resources.GetObject("cbDataField.Size")));
			this.cbDataField.TabIndex = ((int)(resources.GetObject("cbDataField.TabIndex")));
			this.cbDataField.Text = resources.GetString("cbDataField.Text");
			this.toolTip1.SetToolTip(this.cbDataField, resources.GetString("cbDataField.ToolTip"));
			this.cbDataField.Visible = ((bool)(resources.GetObject("cbDataField.Visible")));
			this.cbDataField.SelectedIndexChanged += new System.EventHandler(this.cbDataField_SelectedIndexChanged);
			// 
			// cbOperator
			// 
			this.cbOperator.AccessibleDescription = resources.GetString("cbOperator.AccessibleDescription");
			this.cbOperator.AccessibleName = resources.GetString("cbOperator.AccessibleName");
			this.cbOperator.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbOperator.Anchor")));
			this.cbOperator.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbOperator.BackgroundImage")));
			this.cbOperator.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbOperator.Dock")));
			this.cbOperator.Enabled = ((bool)(resources.GetObject("cbOperator.Enabled")));
			this.cbOperator.Font = ((System.Drawing.Font)(resources.GetObject("cbOperator.Font")));
			this.cbOperator.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbOperator.ImeMode")));
			this.cbOperator.IntegralHeight = ((bool)(resources.GetObject("cbOperator.IntegralHeight")));
			this.cbOperator.ItemHeight = ((int)(resources.GetObject("cbOperator.ItemHeight")));
			this.cbOperator.Location = ((System.Drawing.Point)(resources.GetObject("cbOperator.Location")));
			this.cbOperator.MaxDropDownItems = ((int)(resources.GetObject("cbOperator.MaxDropDownItems")));
			this.cbOperator.MaxLength = ((int)(resources.GetObject("cbOperator.MaxLength")));
			this.cbOperator.Name = "cbOperator";
			this.cbOperator.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbOperator.RightToLeft")));
			this.cbOperator.Size = ((System.Drawing.Size)(resources.GetObject("cbOperator.Size")));
			this.cbOperator.TabIndex = ((int)(resources.GetObject("cbOperator.TabIndex")));
			this.cbOperator.Text = resources.GetString("cbOperator.Text");
			this.toolTip1.SetToolTip(this.cbOperator, resources.GetString("cbOperator.ToolTip"));
			this.cbOperator.Visible = ((bool)(resources.GetObject("cbOperator.Visible")));
			this.cbOperator.SelectedIndexChanged += new System.EventHandler(this.cbOperator_SelectedIndexChanged);
			// 
			// lblOperator
			// 
			this.lblOperator.AccessibleDescription = resources.GetString("lblOperator.AccessibleDescription");
			this.lblOperator.AccessibleName = resources.GetString("lblOperator.AccessibleName");
			this.lblOperator.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblOperator.Anchor")));
			this.lblOperator.AutoSize = ((bool)(resources.GetObject("lblOperator.AutoSize")));
			this.lblOperator.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblOperator.Dock")));
			this.lblOperator.Enabled = ((bool)(resources.GetObject("lblOperator.Enabled")));
			this.lblOperator.Font = ((System.Drawing.Font)(resources.GetObject("lblOperator.Font")));
			this.lblOperator.Image = ((System.Drawing.Image)(resources.GetObject("lblOperator.Image")));
			this.lblOperator.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblOperator.ImageAlign")));
			this.lblOperator.ImageIndex = ((int)(resources.GetObject("lblOperator.ImageIndex")));
			this.lblOperator.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblOperator.ImeMode")));
			this.lblOperator.Location = ((System.Drawing.Point)(resources.GetObject("lblOperator.Location")));
			this.lblOperator.Name = "lblOperator";
			this.lblOperator.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblOperator.RightToLeft")));
			this.lblOperator.Size = ((System.Drawing.Size)(resources.GetObject("lblOperator.Size")));
			this.lblOperator.TabIndex = ((int)(resources.GetObject("lblOperator.TabIndex")));
			this.lblOperator.Text = resources.GetString("lblOperator.Text");
			this.lblOperator.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblOperator.TextAlign")));
			this.toolTip1.SetToolTip(this.lblOperator, resources.GetString("lblOperator.ToolTip"));
			this.lblOperator.Visible = ((bool)(resources.GetObject("lblOperator.Visible")));
			// 
			// grpFunction
			// 
			this.grpFunction.AccessibleDescription = resources.GetString("grpFunction.AccessibleDescription");
			this.grpFunction.AccessibleName = resources.GetString("grpFunction.AccessibleName");
			this.grpFunction.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("grpFunction.Anchor")));
			this.grpFunction.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grpFunction.BackgroundImage")));
			this.grpFunction.Controls.Add(this.rbOr);
			this.grpFunction.Controls.Add(this.rbAnd);
			this.grpFunction.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("grpFunction.Dock")));
			this.grpFunction.Enabled = ((bool)(resources.GetObject("grpFunction.Enabled")));
			this.grpFunction.Font = ((System.Drawing.Font)(resources.GetObject("grpFunction.Font")));
			this.grpFunction.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("grpFunction.ImeMode")));
			this.grpFunction.Location = ((System.Drawing.Point)(resources.GetObject("grpFunction.Location")));
			this.grpFunction.Name = "grpFunction";
			this.grpFunction.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("grpFunction.RightToLeft")));
			this.grpFunction.Size = ((System.Drawing.Size)(resources.GetObject("grpFunction.Size")));
			this.grpFunction.TabIndex = ((int)(resources.GetObject("grpFunction.TabIndex")));
			this.grpFunction.TabStop = false;
			this.grpFunction.Text = resources.GetString("grpFunction.Text");
			this.toolTip1.SetToolTip(this.grpFunction, resources.GetString("grpFunction.ToolTip"));
			this.grpFunction.Visible = ((bool)(resources.GetObject("grpFunction.Visible")));
			// 
			// rbOr
			// 
			this.rbOr.AccessibleDescription = resources.GetString("rbOr.AccessibleDescription");
			this.rbOr.AccessibleName = resources.GetString("rbOr.AccessibleName");
			this.rbOr.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("rbOr.Anchor")));
			this.rbOr.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("rbOr.Appearance")));
			this.rbOr.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rbOr.BackgroundImage")));
			this.rbOr.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbOr.CheckAlign")));
			this.rbOr.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("rbOr.Dock")));
			this.rbOr.Enabled = ((bool)(resources.GetObject("rbOr.Enabled")));
			this.rbOr.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("rbOr.FlatStyle")));
			this.rbOr.Font = ((System.Drawing.Font)(resources.GetObject("rbOr.Font")));
			this.rbOr.Image = ((System.Drawing.Image)(resources.GetObject("rbOr.Image")));
			this.rbOr.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbOr.ImageAlign")));
			this.rbOr.ImageIndex = ((int)(resources.GetObject("rbOr.ImageIndex")));
			this.rbOr.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("rbOr.ImeMode")));
			this.rbOr.Location = ((System.Drawing.Point)(resources.GetObject("rbOr.Location")));
			this.rbOr.Name = "rbOr";
			this.rbOr.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("rbOr.RightToLeft")));
			this.rbOr.Size = ((System.Drawing.Size)(resources.GetObject("rbOr.Size")));
			this.rbOr.TabIndex = ((int)(resources.GetObject("rbOr.TabIndex")));
			this.rbOr.Text = resources.GetString("rbOr.Text");
			this.rbOr.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbOr.TextAlign")));
			this.toolTip1.SetToolTip(this.rbOr, resources.GetString("rbOr.ToolTip"));
			this.rbOr.Visible = ((bool)(resources.GetObject("rbOr.Visible")));
			// 
			// rbAnd
			// 
			this.rbAnd.AccessibleDescription = resources.GetString("rbAnd.AccessibleDescription");
			this.rbAnd.AccessibleName = resources.GetString("rbAnd.AccessibleName");
			this.rbAnd.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("rbAnd.Anchor")));
			this.rbAnd.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("rbAnd.Appearance")));
			this.rbAnd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rbAnd.BackgroundImage")));
			this.rbAnd.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbAnd.CheckAlign")));
			this.rbAnd.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("rbAnd.Dock")));
			this.rbAnd.Enabled = ((bool)(resources.GetObject("rbAnd.Enabled")));
			this.rbAnd.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("rbAnd.FlatStyle")));
			this.rbAnd.Font = ((System.Drawing.Font)(resources.GetObject("rbAnd.Font")));
			this.rbAnd.Image = ((System.Drawing.Image)(resources.GetObject("rbAnd.Image")));
			this.rbAnd.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbAnd.ImageAlign")));
			this.rbAnd.ImageIndex = ((int)(resources.GetObject("rbAnd.ImageIndex")));
			this.rbAnd.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("rbAnd.ImeMode")));
			this.rbAnd.Location = ((System.Drawing.Point)(resources.GetObject("rbAnd.Location")));
			this.rbAnd.Name = "rbAnd";
			this.rbAnd.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("rbAnd.RightToLeft")));
			this.rbAnd.Size = ((System.Drawing.Size)(resources.GetObject("rbAnd.Size")));
			this.rbAnd.TabIndex = ((int)(resources.GetObject("rbAnd.TabIndex")));
			this.rbAnd.Text = resources.GetString("rbAnd.Text");
			this.rbAnd.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbAnd.TextAlign")));
			this.toolTip1.SetToolTip(this.rbAnd, resources.GetString("rbAnd.ToolTip"));
			this.rbAnd.Visible = ((bool)(resources.GetObject("rbAnd.Visible")));
			// 
			// lblCondition
			// 
			this.lblCondition.AccessibleDescription = resources.GetString("lblCondition.AccessibleDescription");
			this.lblCondition.AccessibleName = resources.GetString("lblCondition.AccessibleName");
			this.lblCondition.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblCondition.Anchor")));
			this.lblCondition.AutoSize = ((bool)(resources.GetObject("lblCondition.AutoSize")));
			this.lblCondition.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblCondition.Dock")));
			this.lblCondition.Enabled = ((bool)(resources.GetObject("lblCondition.Enabled")));
			this.lblCondition.Font = ((System.Drawing.Font)(resources.GetObject("lblCondition.Font")));
			this.lblCondition.Image = ((System.Drawing.Image)(resources.GetObject("lblCondition.Image")));
			this.lblCondition.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCondition.ImageAlign")));
			this.lblCondition.ImageIndex = ((int)(resources.GetObject("lblCondition.ImageIndex")));
			this.lblCondition.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblCondition.ImeMode")));
			this.lblCondition.Location = ((System.Drawing.Point)(resources.GetObject("lblCondition.Location")));
			this.lblCondition.Name = "lblCondition";
			this.lblCondition.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblCondition.RightToLeft")));
			this.lblCondition.Size = ((System.Drawing.Size)(resources.GetObject("lblCondition.Size")));
			this.lblCondition.TabIndex = ((int)(resources.GetObject("lblCondition.TabIndex")));
			this.lblCondition.Text = resources.GetString("lblCondition.Text");
			this.lblCondition.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCondition.TextAlign")));
			this.toolTip1.SetToolTip(this.lblCondition, resources.GetString("lblCondition.ToolTip"));
			this.lblCondition.Visible = ((bool)(resources.GetObject("lblCondition.Visible")));
			// 
			// txtCondition
			// 
			this.txtCondition.AccessibleDescription = resources.GetString("txtCondition.AccessibleDescription");
			this.txtCondition.AccessibleName = resources.GetString("txtCondition.AccessibleName");
			this.txtCondition.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtCondition.Anchor")));
			this.txtCondition.AutoSize = ((bool)(resources.GetObject("txtCondition.AutoSize")));
			this.txtCondition.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCondition.BackgroundImage")));
			this.txtCondition.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtCondition.Dock")));
			this.txtCondition.Enabled = ((bool)(resources.GetObject("txtCondition.Enabled")));
			this.txtCondition.Font = ((System.Drawing.Font)(resources.GetObject("txtCondition.Font")));
			this.txtCondition.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtCondition.ImeMode")));
			this.txtCondition.Location = ((System.Drawing.Point)(resources.GetObject("txtCondition.Location")));
			this.txtCondition.MaxLength = ((int)(resources.GetObject("txtCondition.MaxLength")));
			this.txtCondition.Multiline = ((bool)(resources.GetObject("txtCondition.Multiline")));
			this.txtCondition.Name = "txtCondition";
			this.txtCondition.PasswordChar = ((char)(resources.GetObject("txtCondition.PasswordChar")));
			this.txtCondition.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtCondition.RightToLeft")));
			this.txtCondition.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtCondition.ScrollBars")));
			this.txtCondition.Size = ((System.Drawing.Size)(resources.GetObject("txtCondition.Size")));
			this.txtCondition.TabIndex = ((int)(resources.GetObject("txtCondition.TabIndex")));
			this.txtCondition.Text = resources.GetString("txtCondition.Text");
			this.txtCondition.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtCondition.TextAlign")));
			this.toolTip1.SetToolTip(this.txtCondition, resources.GetString("txtCondition.ToolTip"));
			this.txtCondition.Visible = ((bool)(resources.GetObject("txtCondition.Visible")));
			this.txtCondition.WordWrap = ((bool)(resources.GetObject("txtCondition.WordWrap")));
			// 
			// btnNew
			// 
			this.btnNew.AccessibleDescription = resources.GetString("btnNew.AccessibleDescription");
			this.btnNew.AccessibleName = resources.GetString("btnNew.AccessibleName");
			this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNew.Anchor")));
			this.btnNew.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNew.BackgroundImage")));
			this.btnNew.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNew.Dock")));
			this.btnNew.Enabled = ((bool)(resources.GetObject("btnNew.Enabled")));
			this.btnNew.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNew.FlatStyle")));
			this.btnNew.Font = ((System.Drawing.Font)(resources.GetObject("btnNew.Font")));
			this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
			this.btnNew.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNew.ImageAlign")));
			this.btnNew.ImageIndex = ((int)(resources.GetObject("btnNew.ImageIndex")));
			this.btnNew.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNew.ImeMode")));
			this.btnNew.Location = ((System.Drawing.Point)(resources.GetObject("btnNew.Location")));
			this.btnNew.Name = "btnNew";
			this.btnNew.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNew.RightToLeft")));
			this.btnNew.Size = ((System.Drawing.Size)(resources.GetObject("btnNew.Size")));
			this.btnNew.TabIndex = ((int)(resources.GetObject("btnNew.TabIndex")));
			this.btnNew.Text = resources.GetString("btnNew.Text");
			this.btnNew.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNew.TextAlign")));
			this.toolTip1.SetToolTip(this.btnNew, resources.GetString("btnNew.ToolTip"));
			this.btnNew.Visible = ((bool)(resources.GetObject("btnNew.Visible")));
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// btnSet
			// 
			this.btnSet.AccessibleDescription = resources.GetString("btnSet.AccessibleDescription");
			this.btnSet.AccessibleName = resources.GetString("btnSet.AccessibleName");
			this.btnSet.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnSet.Anchor")));
			this.btnSet.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSet.BackgroundImage")));
			this.btnSet.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnSet.Dock")));
			this.btnSet.Enabled = ((bool)(resources.GetObject("btnSet.Enabled")));
			this.btnSet.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnSet.FlatStyle")));
			this.btnSet.Font = ((System.Drawing.Font)(resources.GetObject("btnSet.Font")));
			this.btnSet.Image = ((System.Drawing.Image)(resources.GetObject("btnSet.Image")));
			this.btnSet.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSet.ImageAlign")));
			this.btnSet.ImageIndex = ((int)(resources.GetObject("btnSet.ImageIndex")));
			this.btnSet.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnSet.ImeMode")));
			this.btnSet.Location = ((System.Drawing.Point)(resources.GetObject("btnSet.Location")));
			this.btnSet.Name = "btnSet";
			this.btnSet.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnSet.RightToLeft")));
			this.btnSet.Size = ((System.Drawing.Size)(resources.GetObject("btnSet.Size")));
			this.btnSet.TabIndex = ((int)(resources.GetObject("btnSet.TabIndex")));
			this.btnSet.Text = resources.GetString("btnSet.Text");
			this.btnSet.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSet.TextAlign")));
			this.toolTip1.SetToolTip(this.btnSet, resources.GetString("btnSet.ToolTip"));
			this.btnSet.Visible = ((bool)(resources.GetObject("btnSet.Visible")));
			this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
			// 
			// ucGeneralFilter
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.Controls.Add(this.btnSet);
			this.Controls.Add(this.btnNew);
			this.Controls.Add(this.txtCondition);
			this.Controls.Add(this.lblCondition);
			this.Controls.Add(this.grpFunction);
			this.Controls.Add(this.cbOperator);
			this.Controls.Add(this.lblOperator);
			this.Controls.Add(this.cbDataField);
			this.Controls.Add(this.lblDataField);
			this.Controls.Add(this.lbFilter);
			this.Controls.Add(this.lblFilter);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.Name = "ucGeneralFilter";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.Size = ((System.Drawing.Size)(resources.GetObject("$this.Size")));
			this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
			this.Load += new System.EventHandler(this.ucGeneralFilter_Load);
			this.grpFunction.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		public ViewFilter	Filter
		{
			get
			{
				return this.filter;
			}
			set
			{
				if (this.filter != null)
				{
					this.filter.Filter.FilterRowChanged -=new UserControls.ViewFilter.FilterRowChangeEventHandler(Filter_FilterRowChanged);
					this.filter.Filter.FilterRowDeleted -=new UserControls.ViewFilter.FilterRowChangeEventHandler(Filter_FilterRowDeleted);
				}
				this.filter = value;
				this.view   = null;
				if (this.filter != null)
				{
					this.filter.Filter.FilterRowChanged +=new UserControls.ViewFilter.FilterRowChangeEventHandler(Filter_FilterRowChanged);
					this.filter.Filter.FilterRowDeleted +=new UserControls.ViewFilter.FilterRowChangeEventHandler(Filter_FilterRowDeleted);
				}
				UpdateData();
			}
		}

		public ViewFilter.GeneralFilterRow View
		{
			get
			{
				return this.view;
			}

			set
			{
				this.view = value;
				SetReportData();
				UpdateData();
			}
		}

		public DataField[]	DataFields
		{
			get
			{
				return this.fields;
			}
			set
			{
				this.fields = value;
				UpdateData();
			}
		}

		public string Where
		{
			get
			{
				System.Text.StringBuilder sb = new System.Text.StringBuilder();
				if (this.view != null)
				{
					ViewFilter.FilterRow[] rows = this.view.GetFilterRows();
					foreach (ViewFilter.FilterRow row in rows)
					{
						if (sb.Length == 0)
							sb.Append(string.Format("{0} {1} {2} ",row.Field,row.Operator, row.Condition));
						else
							sb.Append(string.Format("{0} {1} {2} {3} ",row.Connection,row.Field,row.Operator, row.Condition));
					}

				}
				return sb.ToString();
			}
		}

		private void DefaultData()
		{
			this.lbFilter.Items.Clear();
			this.cbDataField.Items.Clear();

			this.txtCondition.Text = "";
			this.rbAnd.Checked = true;
			
		}

		private void UpdateData()
		{
			DefaultData();
			if (this.filter == null || this.view == null || this.fields == null)
				return;

			string line;
			for (int i = 0; i < this.fields.Length; i++)
			{
				line = string.Format("{0}...{1}",this.fields[i].name,this.fields[i].description);
				this.cbDataField.Items.Add(line);
			}

			ViewFilter.FilterRow[] rows = this.view.GetFilterRows();
			foreach (ViewFilter.FilterRow row in rows)
			{
				if (this.lbFilter.Items.Count == 0)
					line = string.Format("{0} {1} {2} ",row.Field,row.Operator, row.Condition);
				else
					line = string.Format("{0} {1} {2} {3} ",row.Connection,row.Field,row.Operator, row.Condition);
				this.lbFilter.Items.Add(line);
			}
		}

		private void btnSet_Click(object sender, System.EventArgs e)
		{
			string olFilter		= "";
			string olOperator	= "";
			string olField		= "";
			string olValue		= "";
			string olPercent	= "";

			olOperator = this.cbOperator.Text;
			int ilIdx = this.cbDataField.SelectedIndex;
			if (ilIdx < 0)
				return;
			else
				olField = this.fields[ilIdx].name;

			olValue = this.txtCondition.Text;

			if (this.fields[ilIdx].type == "Date" && olOperator != "EMPTY")
			{
				try
				{
					DateTime olDateTime = DateTime.Parse(olValue,this.dtfi,DateTimeStyles.None);
					olValue = UT.DateTimeToCeda(olDateTime);
				}
				catch(Exception exc)
				{
					MessageBox.Show(this,"A valid date is missing in your input.");
					return;
				}
			}

			string olConnection = "";
			if (this.rbAnd.Checked == true)
			{
				olConnection = "AND";
			}
			else if (this.rbOr.Checked == true)
			{
				olConnection = "OR";
			}

			if (olConnection.Length == 0)
				return;

			if (olOperator == " LIKE ")
			{
				olPercent = "%";
			}

			if (olOperator == "EMPTY")
			{
				olOperator = " = ";
				olValue = " ";
			}

			ViewFilter.FilterRow row = this.filter.Filter.NewFilterRow();
			row.GeneralFilterRow = this.view;
			row.Connection = olConnection;
			row.Field = olField;
			row.Operator = olOperator;
			row.Condition = "'" + olValue + olPercent + "'";
			this.filter.Filter.AddFilterRow(row);
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			ViewFilter.FilterRow[] rows = this.view.GetFilterRows();
			foreach (ViewFilter.FilterRow row in rows)
			{
				row.Delete();
			}

			this.cbDataField.SelectedIndex = -1;
			this.cbDataField.Refresh();

			this.cbOperator.Text = "EMPTY";
			this.cbOperator.Refresh();

			this.txtCondition.Text = "";
			this.txtCondition.Refresh();
		}

		private void Filter_FilterRowChanged(object sender, UserControls.ViewFilter.FilterRowChangeEvent e)
		{
			if (e.Action == DataRowAction.Add)
			{
				string line = "";
				if (this.lbFilter.Items.Count > 0)
					line = string.Format("{0} {1} {2} {3} ",e.Row.Connection,e.Row.Field,e.Row.Operator, e.Row.Condition);
				else
					line = string.Format("{0} {1} {2} ",e.Row.Field,e.Row.Operator, e.Row.Condition);

				this.lbFilter.Items.Add(line);
			}
			else if (e.Action == DataRowAction.Delete)
			{

			}

		}

		private void Filter_FilterRowDeleted(object sender, UserControls.ViewFilter.FilterRowChangeEvent e)
		{
			if (e.Action == DataRowAction.Delete)
			{
				this.lbFilter.Items.Clear();					
				ViewFilter.FilterRow[] rows = this.view.GetFilterRows();
				string line = "";
				foreach (ViewFilter.FilterRow row in rows)
				{
					if (this.lbFilter.Items.Count > 0)
						line = string.Format("{0} {1} {2} {3} ",row.Connection,row.Field,row.Operator,row.Condition);
					else
						line = string.Format("{0} {1} {2} ",row.Field,row.Operator, row.Condition);
					this.lbFilter.Items.Add(line);
				}
			}
		}

		private void cbOperator_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string olOperator = this.cbOperator.Text;

			if (olOperator == "EMPTY")
			{
				this.txtCondition.Enabled = false;
			}
			else
			{
				this.txtCondition.Enabled = true;
			}
		}

		private void SetReportData()
		{
			UserControls.DataField[] datafields = new UserControls.DataField[136];
			datafields[0].name = "ACT3";
			datafields[1].name = "ACT5";
			datafields[2].name = "AIRB";
			datafields[3].name = "AIRU";
			datafields[4].name = "ALC2";
			datafields[5].name = "ALC3";
			datafields[6].name = "B1BA";
			datafields[7].name = "B1EA";
			datafields[8].name = "B2BA";
			datafields[9].name = "B2EA";
			datafields[10].name = "BAGN";
			datafields[11].name = "BLT1";
			datafields[12].name = "BLT2";
			datafields[13].name = "CDAT";
			datafields[14].name = "CGOT";
			datafields[15].name = "CKIF";
			datafields[16].name = "CKIT";
			datafields[17].name = "CSGN";
			datafields[18].name = "CTOT";
			datafields[19].name = "DCD1";
			datafields[20].name = "DCD2";
			datafields[21].name = "DES3";
			datafields[22].name = "DES4";
			datafields[23].name = "DIVR";
			datafields[24].name = "DOOA";
			datafields[25].name = "DOOD";
			datafields[26].name = "DTD1";
			datafields[27].name = "DTD2";
			datafields[28].name = "ETAC";
			datafields[29].name = "ETAI";
			datafields[30].name = "ETAU";
			datafields[31].name = "ETDC";
			datafields[32].name = "ETDI";
			datafields[33].name = "ETDU";
			datafields[34].name = "ETOA";
			datafields[35].name = "ETOD";
			datafields[36].name = "EXT1";
			datafields[37].name = "EXT2";
			datafields[38].name = "FDAT";
			datafields[39].name = "FLNO";
			datafields[40].name = "FLNS";
			datafields[41].name = "FLTN";
			datafields[42].name = "FTYP";
			datafields[43].name = "GA1X";
			datafields[44].name = "GA1Y";
			datafields[45].name = "GA2X";
			datafields[46].name = "GA2Y";
			datafields[47].name = "GD1X";
			datafields[48].name = "GD1Y";
			datafields[49].name = "GD2X";
			datafields[50].name = "GD2Y";
			datafields[51].name = "GTA1";
			datafields[52].name = "GTA2";
			datafields[53].name = "GTD1";
			datafields[54].name = "GTD2";
			datafields[55].name = "HTYP";
			datafields[56].name = "IFRA";
			datafields[57].name = "IFRD";
			datafields[58].name = "ISKD";
			datafields[59].name = "ISRE";
			datafields[60].name = "JCNT";
			datafields[61].name = "JFNO";
			datafields[62].name = "LAND";
			datafields[63].name = "LNDU";
			datafields[64].name = "LSTU";
			datafields[65].name = "MAIL";
			datafields[66].name = "MING";
			datafields[67].name = "NXTI";
			datafields[68].name = "OFBL";
			datafields[69].name = "OFBU";
			datafields[70].name = "ONBE";
			datafields[71].name = "ONBL";
			datafields[72].name = "ONBU";
			datafields[73].name = "ORG3";
			datafields[74].name = "ORG4";
			datafields[75].name = "PABA";
			datafields[76].name = "PAEA";
			datafields[77].name = "PAID";
			datafields[78].name = "PAX1";
			datafields[79].name = "PAX2";
			datafields[80].name = "PAX3";
			datafields[81].name = "PDBA";
			datafields[82].name = "PDEA";
			datafields[83].name = "PRFL";
			datafields[84].name = "PSTA";
			datafields[85].name = "PSTD";
			datafields[86].name = "RACO";
			datafields[87].name = "REGN";
			datafields[88].name = "REM1";
			datafields[89].name = "REMP";
			datafields[90].name = "RTYP";
			datafields[91].name = "RWYA";
			datafields[92].name = "RWYD";
			datafields[93].name = "SEAS";
			datafields[94].name = "SLOT";
			datafields[95].name = "SLOU";
			datafields[96].name = "SSRC";
			datafields[97].name = "STAB";
			datafields[98].name = "STAT";
			datafields[99].name = "STEA";
			datafields[100].name = "STED";
			datafields[101].name = "STEV";
			datafields[102].name = "STHT";
			datafields[103].name = "STLD";
			datafields[104].name = "STOA";
			datafields[105].name = "STOD";
			datafields[106].name = "STOF";
			datafields[107].name = "STON";
			datafields[108].name = "STSL";
			datafields[109].name = "STTM";
			datafields[110].name = "STTT";
			datafields[111].name = "STYP";
			datafields[112].name = "TET1";
			datafields[113].name = "TET2";
			datafields[114].name = "TGA1";
			datafields[115].name = "TGA2";
			datafields[116].name = "TGD1";
			datafields[117].name = "TGD2";
			datafields[118].name = "TIFA";
			datafields[119].name = "TIFD";
			datafields[120].name = "TISA";
			datafields[121].name = "TISD";
			datafields[122].name = "TMAU";
			datafields[123].name = "TMB1";
			datafields[124].name = "TMB2";
			datafields[125].name = "TMOA";
			datafields[126].name = "TTYP";
			datafields[127].name = "TWR1";
			datafields[128].name = "USEC";
			datafields[129].name = "USEU";
			datafields[130].name = "VERS";
			datafields[131].name = "VIAL";
			datafields[132].name = "VIAN";
			datafields[133].name = "W1BA";
			datafields[134].name = "W1EA";
			datafields[135].name = "WRO1";

			datafields[0].description = "A/C-Type 3-Letter ode (IATA)";
			datafields[1].description = "A/C-Type 5-Letter code";
			datafields[2].description = "Airborne (best time)";
			datafields[3].description = "Airborne - User";
			datafields[4].description = "Airline 2-Letter code";
			datafields[5].description = "Airline 3-Letter code";
			datafields[6].description = "Baggage belt 1 occupied from (actual)";
			datafields[7].description = "Baggage belt 1 occupied til (actual)";
			datafields[8].description = "Baggage belt 1 occupied from (actual)";
			datafields[9].description = "Baggage belt 2 occupied til (actual)";
			datafields[10].description = "Baggage in KG";
			datafields[11].description = "Baggage belt 1";
			datafields[12].description = "Baggage belt 2";
			datafields[13].description = "Created on";
			datafields[14].description = "Cargo in KG";
			datafields[15].description = "Check-In from";
			datafields[16].description = "Check-In til";
			datafields[17].description = "Call-Sign";
			datafields[18].description = "Slot-Time from Telex";
			datafields[19].description = "Delay code 1 Departure";
			datafields[20].description = "Delay code 2 Departure";
			datafields[21].description = "Destination 3-Letter code";
			datafields[22].description = "Destination 4-Letter code";
			datafields[23].description = "Diverted Remark";
			datafields[24].description = "Day of Arrival";
			datafields[25].description = "Day of Departure";
			datafields[26].description = "Delay time 1 Departure";
			datafields[27].description = "Delay time 2 Departure";
			datafields[28].description = "ETA-Confirmed";
			datafields[29].description = "ETA-Internal (best time)";
			datafields[30].description = "ETA-User";
			datafields[31].description = "ETD-Confirmed";
			datafields[32].description = "ETD-Internal (best time)";
			datafields[33].description = "ETD-User";
			datafields[34].description = "ETA-Public Displays";
			datafields[35].description = "ETD-Public Displays";
			datafields[36].description = "Exit 1";
			datafields[37].description = "Exit 2";
			datafields[38].description = "Import date";
			datafields[39].description = "Flight number (Airline, Number, Suffix)";
			datafields[40].description = "Suffix";
			datafields[41].description = "Flight number";
			datafields[42].description = "Type of flight [F, P, R, X, D, T, ...]";
			datafields[43].description = "Arrival: Gate 1 occupied from (actual)";
			datafields[44].description = "Arrival: Gate 1 occupied til(actual)";
			datafields[45].description = "Arrival: Gate 2 occupied from (actual)";
			datafields[46].description = "Arrival: Gate 2 occupied til (actual)";
			datafields[47].description = "Departure: Gate 1 occupied from (actual)";
			datafields[48].description = "Departure: Gate 1 occupied til (actual)";
			datafields[49].description = "Departure: Gate 2 occupied from (actual)";
			datafields[50].description = "Departure: Gate 2 occupied til (actual)";
			datafields[51].description = "Gate 1 Arrival";
			datafields[52].description = "Gate 2 Arrival";
			datafields[53].description = "Gate 1 Departure";
			datafields[54].description = "Gate 2 Departure";
			datafields[55].description = "Handling type";
			datafields[56].description = "Approach Rule";
			datafields[57].description = "Departure Rule";
			datafields[58].description = "Informative Schedule Departure";
			datafields[59].description = "Remark flag";
			datafields[60].description = "No.of Joined flights";
			datafields[61].description = "Joined flights (max. 10)";
			datafields[62].description = "Touch down (best time)";
			datafields[63].description = "Touch down - User";
			datafields[64].description = "Last update";
			datafields[65].description = "Mail in KG";
			datafields[66].description = "Min.G/T";
			datafields[67].description = "Next Info";
			datafields[68].description = "Offblock (best Time)";
			datafields[69].description = "OFBL-User";
			datafields[70].description = "Estimated Onblock";
			datafields[71].description = "Onblock (best time)";
			datafields[72].description = "ONBL-User";
			datafields[73].description = "Origin 3-Letter code";
			datafields[74].description = "Origin 4-Letter code";
			datafields[75].description = "Arrival: Position occupied from (actual)";
			datafields[76].description = "Arrival: Position occupied til (actual)";
			datafields[77].description = "Cash flag";
			datafields[78].description = "No.of PAX 1. Class";
			datafields[79].description = "No.of PAX 2. Class";
			datafields[80].description = "No.of PAX 3. Class";
			datafields[81].description = "Departure: Position occupied from (actual)";
			datafields[82].description = "Departure: Position occupied til (actual)";
			datafields[83].description = "Logging flag";
			datafields[84].description = "Position Arrival";
			datafields[85].description = "Position Departure";
			datafields[86].description = "A/C on radar";
			datafields[87].description = "A/C-Registration";
			datafields[88].description = "Remark";
			datafields[89].description = "Public Remark FIDS";
			datafields[90].description = "Type of Rotation (Source)";
			datafields[91].description = "Runway Arrival";
			datafields[92].description = "Runway Departure";
			datafields[94].description = "Season";
			datafields[94].description = "actual Slot-Time";
			datafields[95].description = "Slot-User";
			datafields[96].description = "SSR-Code";
			datafields[97].description = "Data status AIRB";
			datafields[98].description = "Status";
			datafields[99].description = "Data status ETAI";
			datafields[100].description = "Data status ETDI";
			datafields[101].description = "Keycode";
			datafields[102].description = "Data status HTYP";
			datafields[103].description = "Data status LAND";
			datafields[104].description = "Scheduled time of Arrival";
			datafields[105].description = "Scheduled time of Departure";
			datafields[106].description = "Data status OFBL";
			datafields[107].description = "Data status ONBL";
			datafields[108].description = "Data status SLOT";
			datafields[109].description = "Data status TMOA";
			datafields[110].description = "Data status TTYP";
			datafields[111].description = "Service Typ (Slot/SSIM)";
			datafields[112].description = "Terminal Exit 1";
			datafields[113].description = "Terminal Exit 2";
			datafields[114].description = "Terminal Gate 1 Arrival";
			datafields[115].description = "Terminal Gate 2 Arrival";
			datafields[116].description = "Terminal Gate 1 Departure";
			datafields[117].description = "Terminal Gate 2 Departure";
			datafields[118].description = "Timeframe Arrival";
			datafields[119].description = "Timeframe Departure";
			datafields[120].description = "Status data for TIFA";
			datafields[121].description = "Status data for TIFD";
			datafields[122].description = "TMO-User";
			datafields[123].description = "Terminal Baggage belt 1";
			datafields[124].description = "Terminal Baggage belt 2";
			datafields[125].description = "TMO (best time)";
			datafields[126].description = "Nature";
			datafields[127].description = "Terminal Boarding lounge";
			datafields[128].description = "Created by";
			datafields[129].description = "Updated by";
			datafields[130].description = "Flight schedule version";
			datafields[131].description = "List of Vias and Times";
			datafields[132].description = "No.of Vias  - 1";
			datafields[133].description = "Departure: Boarding Lounge 1 occupied from (actual)";
			datafields[134].description = "Departure: Boarding Lounge 1 occupied till (actual)";
			datafields[135].description = "Boarding lounge";

			datafields[0].type = "String";
			datafields[1].type = "String";
			datafields[2].type = "Date";
			datafields[3].type = "Date";
			datafields[4].type = "String";
			datafields[5].type = "String";
			datafields[6].type = "Date";
			datafields[7].type = "Date";
			datafields[8].type = "Date";
			datafields[9].type = "Date";
			datafields[10].type = "String";
			datafields[11].type = "String";
			datafields[12].type = "String";
			datafields[13].type = "Date";
			datafields[14].type = "String";
			datafields[15].type = "String";
			datafields[16].type = "String";
			datafields[17].type = "String";
			datafields[18].type = "Date";
			datafields[19].type = "String";
			datafields[20].type = "String";
			datafields[21].type = "String";
			datafields[22].type = "String";
			datafields[23].type = "String";
			datafields[24].type = "String";
			datafields[25].type = "String";
			datafields[26].type = "String";
			datafields[27].type = "String";
			datafields[28].type = "Date";
			datafields[29].type = "Date";
			datafields[30].type = "Date";
			datafields[31].type = "Date";
			datafields[32].type = "Date";
			datafields[33].type = "Date";
			datafields[34].type = "Date";
			datafields[35].type = "Date";
			datafields[36].type = "String";
			datafields[37].type = "String";
			datafields[38].type = "Date";
			datafields[39].type = "String";
			datafields[40].type = "String";
			datafields[41].type = "String";
			datafields[42].type = "String";
			datafields[43].type = "Date";
			datafields[44].type = "Date";
			datafields[45].type = "Date";
			datafields[46].type = "Date";
			datafields[47].type = "Date";
			datafields[48].type = "Date";
			datafields[49].type = "Date";
			datafields[50].type = "Date";
			datafields[51].type = "String";
			datafields[52].type = "String";
			datafields[53].type = "String";
			datafields[54].type = "String";
			datafields[55].type = "String";
			datafields[56].type = "String";
			datafields[57].type = "String";
			datafields[58].type = "Date";
			datafields[59].type = "String";
			datafields[60].type = "String";
			datafields[61].type = "String";
			datafields[62].type = "Date";
			datafields[63].type = "Date";
			datafields[64].type = "Date";
			datafields[65].type = "String";
			datafields[66].type = "String";
			datafields[67].type = "Date";
			datafields[68].type = "Date";
			datafields[69].type = "Date";
			datafields[70].type = "Date";
			datafields[71].type = "Date";
			datafields[72].type = "Date";
			datafields[73].type = "String";
			datafields[74].type = "String";
			datafields[75].type = "Date";
			datafields[76].type = "Date";
			datafields[77].type = "String";
			datafields[78].type = "String";
			datafields[79].type = "String";
			datafields[80].type = "String";
			datafields[81].type = "Date";
			datafields[82].type = "Date";
			datafields[83].type = "String";
			datafields[84].type = "String";
			datafields[85].type = "String";
			datafields[86].type = "String";
			datafields[87].type = "String";
			datafields[88].type = "String";
			datafields[89].type = "String";
			datafields[90].type = "String";
			datafields[91].type = "String";
			datafields[92].type = "String";
			datafields[93].type = "String";
			datafields[94].type = "Date";
			datafields[95].type = "Date";
			datafields[96].type = "String";
			datafields[97].type = "String";
			datafields[98].type = "String";
			datafields[99].type = "String";
			datafields[100].type = "String";
			datafields[101].type = "String";
			datafields[102].type = "String";
			datafields[103].type = "String";
			datafields[104].type = "Date";
			datafields[105].type = "Date";
			datafields[106].type = "String";
			datafields[107].type = "String";
			datafields[108].type = "String";
			datafields[109].type = "String";
			datafields[110].type = "String";
			datafields[111].type = "String";
			datafields[112].type = "String";
			datafields[113].type = "String";
			datafields[114].type = "String";
			datafields[115].type = "String";
			datafields[116].type = "String";
			datafields[117].type = "String";
			datafields[118].type = "Date";
			datafields[119].type = "Date";
			datafields[120].type = "String";
			datafields[121].type = "String";
			datafields[122].type = "Date";
			datafields[123].type = "String";
			datafields[124].type = "String";
			datafields[125].type = "Date";
			datafields[126].type = "String";
			datafields[127].type = "String";
			datafields[128].type = "String";
			datafields[129].type = "String";
			datafields[130].type = "String";
			datafields[131].type = "String";
			datafields[132].type = "String";
			datafields[133].type = "Date";
			datafields[134].type = "Date";
			datafields[135].type = "String";
			
			this.fields = datafields;
		}

		private void ucGeneralFilter_Load(object sender, System.EventArgs e)
		{
			SetReportData();
			UpdateData();
		}

		private void cbDataField_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int ind = this.cbDataField.SelectedIndex;
			if (ind >= 0)
			{
				switch(this.fields[ind].type)
				{
					case "Date":
					this.toolTip1.SetToolTip(this.txtCondition,"For Date/Time conditions please use the following format: dd.mm.yyyy HH:MM");
						break;
					case "String":
						this.toolTip1.SetToolTip(this.txtCondition,"For String conditions please check the correct length");
						break;
				}
			}
			else
			{
				this.toolTip1.SetToolTip(this.txtCondition,"");
			}
		}
	}
}
