using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace UserControls
{
	/// <summary>
	/// Summary description for frmView.
	/// </summary>
	public class frmView : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ImageList imageButtons;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnApply;
		private System.Windows.Forms.Label lblViewName;
		private System.Windows.Forms.ComboBox cbViews;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Panel panel2;
		private UserControls.ucSelection ucSelection2;
		private System.ComponentModel.IContainer components;

		#region implementation data
		private	ReportViewer		 viewer	= null;
		#endregion
		public frmView()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmView));
			this.imageButtons = new System.Windows.Forms.ImageList(this.components);
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.btnNew = new System.Windows.Forms.Button();
			this.btnApply = new System.Windows.Forms.Button();
			this.lblViewName = new System.Windows.Forms.Label();
			this.cbViews = new System.Windows.Forms.ComboBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.ucSelection2 = new UserControls.ucSelection();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// imageButtons
			// 
			this.imageButtons.ImageSize = ((System.Drawing.Size)(resources.GetObject("imageButtons.ImageSize")));
			this.imageButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageButtons.ImageStream")));
			this.imageButtons.TransparentColor = System.Drawing.Color.White;
			// 
			// panel1
			// 
			this.panel1.AccessibleDescription = resources.GetString("panel1.AccessibleDescription");
			this.panel1.AccessibleName = resources.GetString("panel1.AccessibleName");
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panel1.Anchor")));
			this.panel1.AutoScroll = ((bool)(resources.GetObject("panel1.AutoScroll")));
			this.panel1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panel1.AutoScrollMargin")));
			this.panel1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panel1.AutoScrollMinSize")));
			this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.btnDelete);
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Controls.Add(this.btnApply);
			this.panel1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panel1.Dock")));
			this.panel1.Enabled = ((bool)(resources.GetObject("panel1.Enabled")));
			this.panel1.Font = ((System.Drawing.Font)(resources.GetObject("panel1.Font")));
			this.panel1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panel1.ImeMode")));
			this.panel1.Location = ((System.Drawing.Point)(resources.GetObject("panel1.Location")));
			this.panel1.Name = "panel1";
			this.panel1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panel1.RightToLeft")));
			this.panel1.Size = ((System.Drawing.Size)(resources.GetObject("panel1.Size")));
			this.panel1.TabIndex = ((int)(resources.GetObject("panel1.TabIndex")));
			this.panel1.Text = resources.GetString("panel1.Text");
			this.panel1.Visible = ((bool)(resources.GetObject("panel1.Visible")));
			// 
			// btnSave
			// 
			this.btnSave.AccessibleDescription = resources.GetString("btnSave.AccessibleDescription");
			this.btnSave.AccessibleName = resources.GetString("btnSave.AccessibleName");
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnSave.Anchor")));
			this.btnSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSave.BackgroundImage")));
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnSave.Dock")));
			this.btnSave.Enabled = ((bool)(resources.GetObject("btnSave.Enabled")));
			this.btnSave.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnSave.FlatStyle")));
			this.btnSave.Font = ((System.Drawing.Font)(resources.GetObject("btnSave.Font")));
			this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
			this.btnSave.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSave.ImageAlign")));
			this.btnSave.ImageIndex = ((int)(resources.GetObject("btnSave.ImageIndex")));
			this.btnSave.ImageList = this.imageButtons;
			this.btnSave.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnSave.ImeMode")));
			this.btnSave.Location = ((System.Drawing.Point)(resources.GetObject("btnSave.Location")));
			this.btnSave.Name = "btnSave";
			this.btnSave.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnSave.RightToLeft")));
			this.btnSave.Size = ((System.Drawing.Size)(resources.GetObject("btnSave.Size")));
			this.btnSave.TabIndex = ((int)(resources.GetObject("btnSave.TabIndex")));
			this.btnSave.Text = resources.GetString("btnSave.Text");
			this.btnSave.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSave.TextAlign")));
			this.btnSave.Visible = ((bool)(resources.GetObject("btnSave.Visible")));
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnClose
			// 
			this.btnClose.AccessibleDescription = resources.GetString("btnClose.AccessibleDescription");
			this.btnClose.AccessibleName = resources.GetString("btnClose.AccessibleName");
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnClose.Anchor")));
			this.btnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClose.BackgroundImage")));
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnClose.Dock")));
			this.btnClose.Enabled = ((bool)(resources.GetObject("btnClose.Enabled")));
			this.btnClose.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnClose.FlatStyle")));
			this.btnClose.Font = ((System.Drawing.Font)(resources.GetObject("btnClose.Font")));
			this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
			this.btnClose.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnClose.ImageAlign")));
			this.btnClose.ImageIndex = ((int)(resources.GetObject("btnClose.ImageIndex")));
			this.btnClose.ImageList = this.imageButtons;
			this.btnClose.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnClose.ImeMode")));
			this.btnClose.Location = ((System.Drawing.Point)(resources.GetObject("btnClose.Location")));
			this.btnClose.Name = "btnClose";
			this.btnClose.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnClose.RightToLeft")));
			this.btnClose.Size = ((System.Drawing.Size)(resources.GetObject("btnClose.Size")));
			this.btnClose.TabIndex = ((int)(resources.GetObject("btnClose.TabIndex")));
			this.btnClose.Text = resources.GetString("btnClose.Text");
			this.btnClose.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnClose.TextAlign")));
			this.btnClose.Visible = ((bool)(resources.GetObject("btnClose.Visible")));
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.AccessibleDescription = resources.GetString("btnDelete.AccessibleDescription");
			this.btnDelete.AccessibleName = resources.GetString("btnDelete.AccessibleName");
			this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnDelete.Anchor")));
			this.btnDelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDelete.BackgroundImage")));
			this.btnDelete.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnDelete.Dock")));
			this.btnDelete.Enabled = ((bool)(resources.GetObject("btnDelete.Enabled")));
			this.btnDelete.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnDelete.FlatStyle")));
			this.btnDelete.Font = ((System.Drawing.Font)(resources.GetObject("btnDelete.Font")));
			this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
			this.btnDelete.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDelete.ImageAlign")));
			this.btnDelete.ImageIndex = ((int)(resources.GetObject("btnDelete.ImageIndex")));
			this.btnDelete.ImageList = this.imageButtons;
			this.btnDelete.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnDelete.ImeMode")));
			this.btnDelete.Location = ((System.Drawing.Point)(resources.GetObject("btnDelete.Location")));
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnDelete.RightToLeft")));
			this.btnDelete.Size = ((System.Drawing.Size)(resources.GetObject("btnDelete.Size")));
			this.btnDelete.TabIndex = ((int)(resources.GetObject("btnDelete.TabIndex")));
			this.btnDelete.Text = resources.GetString("btnDelete.Text");
			this.btnDelete.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDelete.TextAlign")));
			this.btnDelete.Visible = ((bool)(resources.GetObject("btnDelete.Visible")));
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// btnNew
			// 
			this.btnNew.AccessibleDescription = resources.GetString("btnNew.AccessibleDescription");
			this.btnNew.AccessibleName = resources.GetString("btnNew.AccessibleName");
			this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNew.Anchor")));
			this.btnNew.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNew.BackgroundImage")));
			this.btnNew.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNew.Dock")));
			this.btnNew.Enabled = ((bool)(resources.GetObject("btnNew.Enabled")));
			this.btnNew.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNew.FlatStyle")));
			this.btnNew.Font = ((System.Drawing.Font)(resources.GetObject("btnNew.Font")));
			this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
			this.btnNew.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNew.ImageAlign")));
			this.btnNew.ImageIndex = ((int)(resources.GetObject("btnNew.ImageIndex")));
			this.btnNew.ImageList = this.imageButtons;
			this.btnNew.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNew.ImeMode")));
			this.btnNew.Location = ((System.Drawing.Point)(resources.GetObject("btnNew.Location")));
			this.btnNew.Name = "btnNew";
			this.btnNew.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNew.RightToLeft")));
			this.btnNew.Size = ((System.Drawing.Size)(resources.GetObject("btnNew.Size")));
			this.btnNew.TabIndex = ((int)(resources.GetObject("btnNew.TabIndex")));
			this.btnNew.Text = resources.GetString("btnNew.Text");
			this.btnNew.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNew.TextAlign")));
			this.btnNew.Visible = ((bool)(resources.GetObject("btnNew.Visible")));
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// btnApply
			// 
			this.btnApply.AccessibleDescription = resources.GetString("btnApply.AccessibleDescription");
			this.btnApply.AccessibleName = resources.GetString("btnApply.AccessibleName");
			this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnApply.Anchor")));
			this.btnApply.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnApply.BackgroundImage")));
			this.btnApply.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnApply.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnApply.Dock")));
			this.btnApply.Enabled = ((bool)(resources.GetObject("btnApply.Enabled")));
			this.btnApply.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnApply.FlatStyle")));
			this.btnApply.Font = ((System.Drawing.Font)(resources.GetObject("btnApply.Font")));
			this.btnApply.Image = ((System.Drawing.Image)(resources.GetObject("btnApply.Image")));
			this.btnApply.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnApply.ImageAlign")));
			this.btnApply.ImageIndex = ((int)(resources.GetObject("btnApply.ImageIndex")));
			this.btnApply.ImageList = this.imageButtons;
			this.btnApply.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnApply.ImeMode")));
			this.btnApply.Location = ((System.Drawing.Point)(resources.GetObject("btnApply.Location")));
			this.btnApply.Name = "btnApply";
			this.btnApply.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnApply.RightToLeft")));
			this.btnApply.Size = ((System.Drawing.Size)(resources.GetObject("btnApply.Size")));
			this.btnApply.TabIndex = ((int)(resources.GetObject("btnApply.TabIndex")));
			this.btnApply.Text = resources.GetString("btnApply.Text");
			this.btnApply.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnApply.TextAlign")));
			this.btnApply.Visible = ((bool)(resources.GetObject("btnApply.Visible")));
			this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
			// 
			// lblViewName
			// 
			this.lblViewName.AccessibleDescription = resources.GetString("lblViewName.AccessibleDescription");
			this.lblViewName.AccessibleName = resources.GetString("lblViewName.AccessibleName");
			this.lblViewName.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblViewName.Anchor")));
			this.lblViewName.AutoSize = ((bool)(resources.GetObject("lblViewName.AutoSize")));
			this.lblViewName.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblViewName.Dock")));
			this.lblViewName.Enabled = ((bool)(resources.GetObject("lblViewName.Enabled")));
			this.lblViewName.Font = ((System.Drawing.Font)(resources.GetObject("lblViewName.Font")));
			this.lblViewName.Image = ((System.Drawing.Image)(resources.GetObject("lblViewName.Image")));
			this.lblViewName.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblViewName.ImageAlign")));
			this.lblViewName.ImageIndex = ((int)(resources.GetObject("lblViewName.ImageIndex")));
			this.lblViewName.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblViewName.ImeMode")));
			this.lblViewName.Location = ((System.Drawing.Point)(resources.GetObject("lblViewName.Location")));
			this.lblViewName.Name = "lblViewName";
			this.lblViewName.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblViewName.RightToLeft")));
			this.lblViewName.Size = ((System.Drawing.Size)(resources.GetObject("lblViewName.Size")));
			this.lblViewName.TabIndex = ((int)(resources.GetObject("lblViewName.TabIndex")));
			this.lblViewName.Text = resources.GetString("lblViewName.Text");
			this.lblViewName.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblViewName.TextAlign")));
			this.lblViewName.Visible = ((bool)(resources.GetObject("lblViewName.Visible")));
			// 
			// cbViews
			// 
			this.cbViews.AccessibleDescription = resources.GetString("cbViews.AccessibleDescription");
			this.cbViews.AccessibleName = resources.GetString("cbViews.AccessibleName");
			this.cbViews.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbViews.Anchor")));
			this.cbViews.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbViews.BackgroundImage")));
			this.cbViews.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbViews.Dock")));
			this.cbViews.Enabled = ((bool)(resources.GetObject("cbViews.Enabled")));
			this.cbViews.Font = ((System.Drawing.Font)(resources.GetObject("cbViews.Font")));
			this.cbViews.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbViews.ImeMode")));
			this.cbViews.IntegralHeight = ((bool)(resources.GetObject("cbViews.IntegralHeight")));
			this.cbViews.ItemHeight = ((int)(resources.GetObject("cbViews.ItemHeight")));
			this.cbViews.Location = ((System.Drawing.Point)(resources.GetObject("cbViews.Location")));
			this.cbViews.MaxDropDownItems = ((int)(resources.GetObject("cbViews.MaxDropDownItems")));
			this.cbViews.MaxLength = ((int)(resources.GetObject("cbViews.MaxLength")));
			this.cbViews.Name = "cbViews";
			this.cbViews.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbViews.RightToLeft")));
			this.cbViews.Size = ((System.Drawing.Size)(resources.GetObject("cbViews.Size")));
			this.cbViews.Sorted = true;
			this.cbViews.TabIndex = ((int)(resources.GetObject("cbViews.TabIndex")));
			this.cbViews.Text = resources.GetString("cbViews.Text");
			this.cbViews.Visible = ((bool)(resources.GetObject("cbViews.Visible")));
			this.cbViews.TextChanged += new System.EventHandler(this.cbViews_TextChanged);
			this.cbViews.SelectedValueChanged += new System.EventHandler(this.cbViews_SelectedValueChanged);
			this.cbViews.SelectedIndexChanged += new System.EventHandler(this.cbViews_SelectedIndexChanged);
			// 
			// panel2
			// 
			this.panel2.AccessibleDescription = resources.GetString("panel2.AccessibleDescription");
			this.panel2.AccessibleName = resources.GetString("panel2.AccessibleName");
			this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panel2.Anchor")));
			this.panel2.AutoScroll = ((bool)(resources.GetObject("panel2.AutoScroll")));
			this.panel2.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panel2.AutoScrollMargin")));
			this.panel2.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panel2.AutoScrollMinSize")));
			this.panel2.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
			this.panel2.Controls.Add(this.ucSelection2);
			this.panel2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panel2.Dock")));
			this.panel2.Enabled = ((bool)(resources.GetObject("panel2.Enabled")));
			this.panel2.Font = ((System.Drawing.Font)(resources.GetObject("panel2.Font")));
			this.panel2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panel2.ImeMode")));
			this.panel2.Location = ((System.Drawing.Point)(resources.GetObject("panel2.Location")));
			this.panel2.Name = "panel2";
			this.panel2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panel2.RightToLeft")));
			this.panel2.Size = ((System.Drawing.Size)(resources.GetObject("panel2.Size")));
			this.panel2.TabIndex = ((int)(resources.GetObject("panel2.TabIndex")));
			this.panel2.Text = resources.GetString("panel2.Text");
			this.panel2.Visible = ((bool)(resources.GetObject("panel2.Visible")));
			// 
			// ucSelection2
			// 
			this.ucSelection2.AccessibleDescription = resources.GetString("ucSelection2.AccessibleDescription");
			this.ucSelection2.AccessibleName = resources.GetString("ucSelection2.AccessibleName");
			this.ucSelection2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("ucSelection2.Anchor")));
			this.ucSelection2.AutoScroll = ((bool)(resources.GetObject("ucSelection2.AutoScroll")));
			this.ucSelection2.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("ucSelection2.AutoScrollMargin")));
			this.ucSelection2.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("ucSelection2.AutoScrollMinSize")));
			this.ucSelection2.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucSelection2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ucSelection2.BackgroundImage")));
			this.ucSelection2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("ucSelection2.Dock")));
			this.ucSelection2.Enabled = ((bool)(resources.GetObject("ucSelection2.Enabled")));
			this.ucSelection2.Filter = null;
			this.ucSelection2.Font = ((System.Drawing.Font)(resources.GetObject("ucSelection2.Font")));
			this.ucSelection2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("ucSelection2.ImeMode")));
			this.ucSelection2.Location = ((System.Drawing.Point)(resources.GetObject("ucSelection2.Location")));
			this.ucSelection2.Name = "ucSelection2";
			this.ucSelection2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("ucSelection2.RightToLeft")));
			this.ucSelection2.Size = ((System.Drawing.Size)(resources.GetObject("ucSelection2.Size")));
			this.ucSelection2.TabIndex = ((int)(resources.GetObject("ucSelection2.TabIndex")));
			this.ucSelection2.View = null;
			this.ucSelection2.Visible = ((bool)(resources.GetObject("ucSelection2.Visible")));
			// 
			// frmView
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.cbViews);
			this.Controls.Add(this.lblViewName);
			this.Controls.Add(this.panel1);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "frmView";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Load += new System.EventHandler(this.frmView_Load);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		public ReportViewer Viewer
		{
			get
			{
				return this.viewer;
			}

			set
			{
				this.viewer = value;
			}
		}

		private void btnApply_Click(object sender, System.EventArgs e)
		{
			this.viewer.UpdateCurrentViewAs(this.cbViews.Text,true);			
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			if (this.viewer.CreateView(""))
			{
				UpdateData();
			}
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			DialogResult olResult;
			olResult = MessageBox.Show(this, "Do you really want to delete the current view?", "Question", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			if( olResult != DialogResult.Yes)
			{
				return;
			}

			string strName = this.cbViews.Text;
			if (strName == "<DEFAULT>")
			{
				return; //Nothing to do because it's <DEFAULT>
			}

			int idx = this.cbViews.FindStringExact(strName, -1);
			if (idx > -1)
			{
				this.cbViews.Items.RemoveAt(idx);
				if (this.viewer.DeleteView(strName))
				{
					btnNew_Click(this,null);
				}
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if (this.CheckValidity())
			{
				this.viewer.SaveCurrentViewAs(this.cbViews.Text);
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			if (this.viewer.UpdateCurrentViewAs(this.cbViews.Text,false))
			{
				this.Close();
			}
		}

		private void cbViews_TextChanged(object sender, System.EventArgs e)
		{
			if (this.cbViews.Items.Contains(this.cbViews.Text))
			{
				if (this.viewer.View != this.cbViews.Text)
				{
					this.viewer.View = this.cbViews.Text;
					this.ucSelection2.Filter = this.viewer.CurrentFilter;
					this.ucSelection2.View = this.viewer.CurrentViewRow;
				}

				if (this.viewer.View == "<DEFAULT>")
				{
					btnApply.Enabled = false;
					btnDelete.Enabled = false;
					btnSave.Enabled = false;
				}
				else if (this.cbViews.Text.Length == 0)
				{
					this.btnApply.Enabled = false;
					this.btnDelete.Enabled = false;
					this.btnSave.Enabled = false;
				}
				else
				{
					this.btnApply.Enabled = true;
					this.btnDelete.Enabled = true;
					this.btnSave.Enabled = true;
				}
			}
			else if (this.cbViews.Text.Length == 0)
			{
				this.btnApply.Enabled = false;
				this.btnDelete.Enabled = false;
				this.btnSave.Enabled = false;
			}
			else if (this.cbViews.Text == "<DEFAULT>")
			{
				btnApply.Enabled = false;
				btnDelete.Enabled = false;
				btnSave.Enabled = false;
			}
			else
			{
				this.btnApply.Enabled = true;
				this.btnDelete.Enabled = true;
				this.btnSave.Enabled = true;
			}
		}

		private bool CheckValidity()
		{
			string strMessage = "";
			if (this.cbViews.Text == "")
			{
				strMessage = "Please insert a unique View Name!\n";
				MessageBox.Show(this,strMessage);
				return false;
			}

			return true;
		}

		private void frmView_Load(object sender, System.EventArgs e)
		{
			// initialize view filter
			UpdateData();
		}

		private void cbViews_SelectedValueChanged(object sender, System.EventArgs e)
		{
		}

		private void UpdateButtons()
		{
		}

		private void UpdateData()
		{
			// initialize view filter
			this.cbViews.Items.Clear();

			this.cbViews.Items.AddRange(this.viewer.Views);
			if (this.cbViews.FindStringExact("<DEFAULT>") < 0)
				this.cbViews.Items.Add("<DEFAULT>");

			this.cbViews.Text = this.viewer.View;

			if (this.viewer.View == "<DEFAULT>")
			{
				btnApply.Enabled = false;
				btnDelete.Enabled = false;
				btnSave.Enabled = false;
			}
			else if (this.cbViews.Text.Length == 0)
			{
				btnApply.Enabled = false;
				btnDelete.Enabled = true;
				btnSave.Enabled = false;
			}
			else
			{
				btnApply.Enabled = true;
				btnDelete.Enabled = true;
				btnSave.Enabled = true;
			}

			this.cbViews.Refresh();

		}

		private void cbViews_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.viewer.View = this.cbViews.Text;
			this.ucSelection2.Filter = this.viewer.CurrentFilter;
			this.ucSelection2.View = this.viewer.CurrentViewRow;

			if (this.viewer.View == "<DEFAULT>")
			{
				btnApply.Enabled = false;
				btnDelete.Enabled = false;
				btnSave.Enabled = false;
			}
			else if (this.cbViews.Text.Length == 0)
			{
				this.btnApply.Enabled = false;
				this.btnDelete.Enabled = true;
				this.btnSave.Enabled = false;
			}
			else
			{
				this.btnApply.Enabled = true;
				this.btnDelete.Enabled = true;
				this.btnSave.Enabled = true;
			}
		
		}

	}
}
