using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace UserControls
{
	/// <summary>
	/// Summary description for ucSelection.
	/// </summary>
	public class ucSelection : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.TabControl tabPages;
		private System.Windows.Forms.TabPage tbpSearch;
		private UserControls.ucSelectionPage ucSelectionPage2;
		private System.Windows.Forms.TabPage tbpGeneralFilter;
		private System.Windows.Forms.TabPage tbpGeneralSort;
		private System.Windows.Forms.TabPage tbpSpecificSort;
		private UserControls.ucGeneralFilter ucGeneralFilter1;
		private UserControls.ucGeneralSort ucGeneralSort1;

		#region internal members
		private	ViewFilter			filter = null;
		private ViewFilter.ViewRow	view = null;
		#endregion
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ucSelection()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			// remove non supported pages
			this.tabPages.TabPages.Remove(this.tbpGeneralSort);
			this.tabPages.TabPages.Remove(this.tbpSpecificSort);
			this.tabPages.SelectedTab = this.tbpSearch;

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ucSelection));
			this.tabPages = new System.Windows.Forms.TabControl();
			this.tbpSearch = new System.Windows.Forms.TabPage();
			this.ucSelectionPage2 = new UserControls.ucSelectionPage();
			this.tbpGeneralFilter = new System.Windows.Forms.TabPage();
			this.ucGeneralFilter1 = new UserControls.ucGeneralFilter();
			this.tbpGeneralSort = new System.Windows.Forms.TabPage();
			this.ucGeneralSort1 = new UserControls.ucGeneralSort();
			this.tbpSpecificSort = new System.Windows.Forms.TabPage();
			this.tabPages.SuspendLayout();
			this.tbpSearch.SuspendLayout();
			this.tbpGeneralFilter.SuspendLayout();
			this.tbpGeneralSort.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabPages
			// 
			this.tabPages.AccessibleDescription = resources.GetString("tabPages.AccessibleDescription");
			this.tabPages.AccessibleName = resources.GetString("tabPages.AccessibleName");
			this.tabPages.Alignment = ((System.Windows.Forms.TabAlignment)(resources.GetObject("tabPages.Alignment")));
			this.tabPages.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("tabPages.Anchor")));
			this.tabPages.Appearance = ((System.Windows.Forms.TabAppearance)(resources.GetObject("tabPages.Appearance")));
			this.tabPages.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPages.BackgroundImage")));
			this.tabPages.Controls.Add(this.tbpSearch);
			this.tabPages.Controls.Add(this.tbpGeneralFilter);
			this.tabPages.Controls.Add(this.tbpGeneralSort);
			this.tabPages.Controls.Add(this.tbpSpecificSort);
			this.tabPages.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("tabPages.Dock")));
			this.tabPages.Enabled = ((bool)(resources.GetObject("tabPages.Enabled")));
			this.tabPages.Font = ((System.Drawing.Font)(resources.GetObject("tabPages.Font")));
			this.tabPages.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("tabPages.ImeMode")));
			this.tabPages.ItemSize = ((System.Drawing.Size)(resources.GetObject("tabPages.ItemSize")));
			this.tabPages.Location = ((System.Drawing.Point)(resources.GetObject("tabPages.Location")));
			this.tabPages.Name = "tabPages";
			this.tabPages.Padding = ((System.Drawing.Point)(resources.GetObject("tabPages.Padding")));
			this.tabPages.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("tabPages.RightToLeft")));
			this.tabPages.SelectedIndex = 0;
			this.tabPages.ShowToolTips = ((bool)(resources.GetObject("tabPages.ShowToolTips")));
			this.tabPages.Size = ((System.Drawing.Size)(resources.GetObject("tabPages.Size")));
			this.tabPages.TabIndex = ((int)(resources.GetObject("tabPages.TabIndex")));
			this.tabPages.Text = resources.GetString("tabPages.Text");
			this.tabPages.Visible = ((bool)(resources.GetObject("tabPages.Visible")));
			// 
			// tbpSearch
			// 
			this.tbpSearch.AccessibleDescription = resources.GetString("tbpSearch.AccessibleDescription");
			this.tbpSearch.AccessibleName = resources.GetString("tbpSearch.AccessibleName");
			this.tbpSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("tbpSearch.Anchor")));
			this.tbpSearch.AutoScroll = ((bool)(resources.GetObject("tbpSearch.AutoScroll")));
			this.tbpSearch.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("tbpSearch.AutoScrollMargin")));
			this.tbpSearch.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("tbpSearch.AutoScrollMinSize")));
			this.tbpSearch.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.tbpSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tbpSearch.BackgroundImage")));
			this.tbpSearch.Controls.Add(this.ucSelectionPage2);
			this.tbpSearch.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("tbpSearch.Dock")));
			this.tbpSearch.Enabled = ((bool)(resources.GetObject("tbpSearch.Enabled")));
			this.tbpSearch.Font = ((System.Drawing.Font)(resources.GetObject("tbpSearch.Font")));
			this.tbpSearch.ImageIndex = ((int)(resources.GetObject("tbpSearch.ImageIndex")));
			this.tbpSearch.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("tbpSearch.ImeMode")));
			this.tbpSearch.Location = ((System.Drawing.Point)(resources.GetObject("tbpSearch.Location")));
			this.tbpSearch.Name = "tbpSearch";
			this.tbpSearch.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("tbpSearch.RightToLeft")));
			this.tbpSearch.Size = ((System.Drawing.Size)(resources.GetObject("tbpSearch.Size")));
			this.tbpSearch.TabIndex = ((int)(resources.GetObject("tbpSearch.TabIndex")));
			this.tbpSearch.Text = resources.GetString("tbpSearch.Text");
			this.tbpSearch.ToolTipText = resources.GetString("tbpSearch.ToolTipText");
			this.tbpSearch.Visible = ((bool)(resources.GetObject("tbpSearch.Visible")));
			// 
			// ucSelectionPage2
			// 
			this.ucSelectionPage2.AccessibleDescription = resources.GetString("ucSelectionPage2.AccessibleDescription");
			this.ucSelectionPage2.AccessibleName = resources.GetString("ucSelectionPage2.AccessibleName");
			this.ucSelectionPage2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("ucSelectionPage2.Anchor")));
			this.ucSelectionPage2.AutoScroll = ((bool)(resources.GetObject("ucSelectionPage2.AutoScroll")));
			this.ucSelectionPage2.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("ucSelectionPage2.AutoScrollMargin")));
			this.ucSelectionPage2.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("ucSelectionPage2.AutoScrollMinSize")));
			this.ucSelectionPage2.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucSelectionPage2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ucSelectionPage2.BackgroundImage")));
			this.ucSelectionPage2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("ucSelectionPage2.Dock")));
			this.ucSelectionPage2.Enabled = ((bool)(resources.GetObject("ucSelectionPage2.Enabled")));
			this.ucSelectionPage2.Filter = null;
			this.ucSelectionPage2.Font = ((System.Drawing.Font)(resources.GetObject("ucSelectionPage2.Font")));
			this.ucSelectionPage2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("ucSelectionPage2.ImeMode")));
			this.ucSelectionPage2.Location = ((System.Drawing.Point)(resources.GetObject("ucSelectionPage2.Location")));
			this.ucSelectionPage2.Name = "ucSelectionPage2";
			this.ucSelectionPage2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("ucSelectionPage2.RightToLeft")));
			this.ucSelectionPage2.Size = ((System.Drawing.Size)(resources.GetObject("ucSelectionPage2.Size")));
			this.ucSelectionPage2.TabIndex = ((int)(resources.GetObject("ucSelectionPage2.TabIndex")));
			this.ucSelectionPage2.View = null;
			this.ucSelectionPage2.Visible = ((bool)(resources.GetObject("ucSelectionPage2.Visible")));
			// 
			// tbpGeneralFilter
			// 
			this.tbpGeneralFilter.AccessibleDescription = resources.GetString("tbpGeneralFilter.AccessibleDescription");
			this.tbpGeneralFilter.AccessibleName = resources.GetString("tbpGeneralFilter.AccessibleName");
			this.tbpGeneralFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("tbpGeneralFilter.Anchor")));
			this.tbpGeneralFilter.AutoScroll = ((bool)(resources.GetObject("tbpGeneralFilter.AutoScroll")));
			this.tbpGeneralFilter.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("tbpGeneralFilter.AutoScrollMargin")));
			this.tbpGeneralFilter.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("tbpGeneralFilter.AutoScrollMinSize")));
			this.tbpGeneralFilter.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.tbpGeneralFilter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tbpGeneralFilter.BackgroundImage")));
			this.tbpGeneralFilter.Controls.Add(this.ucGeneralFilter1);
			this.tbpGeneralFilter.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("tbpGeneralFilter.Dock")));
			this.tbpGeneralFilter.Enabled = ((bool)(resources.GetObject("tbpGeneralFilter.Enabled")));
			this.tbpGeneralFilter.Font = ((System.Drawing.Font)(resources.GetObject("tbpGeneralFilter.Font")));
			this.tbpGeneralFilter.ImageIndex = ((int)(resources.GetObject("tbpGeneralFilter.ImageIndex")));
			this.tbpGeneralFilter.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("tbpGeneralFilter.ImeMode")));
			this.tbpGeneralFilter.Location = ((System.Drawing.Point)(resources.GetObject("tbpGeneralFilter.Location")));
			this.tbpGeneralFilter.Name = "tbpGeneralFilter";
			this.tbpGeneralFilter.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("tbpGeneralFilter.RightToLeft")));
			this.tbpGeneralFilter.Size = ((System.Drawing.Size)(resources.GetObject("tbpGeneralFilter.Size")));
			this.tbpGeneralFilter.TabIndex = ((int)(resources.GetObject("tbpGeneralFilter.TabIndex")));
			this.tbpGeneralFilter.Text = resources.GetString("tbpGeneralFilter.Text");
			this.tbpGeneralFilter.ToolTipText = resources.GetString("tbpGeneralFilter.ToolTipText");
			this.tbpGeneralFilter.Visible = ((bool)(resources.GetObject("tbpGeneralFilter.Visible")));
			// 
			// ucGeneralFilter1
			// 
			this.ucGeneralFilter1.AccessibleDescription = resources.GetString("ucGeneralFilter1.AccessibleDescription");
			this.ucGeneralFilter1.AccessibleName = resources.GetString("ucGeneralFilter1.AccessibleName");
			this.ucGeneralFilter1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("ucGeneralFilter1.Anchor")));
			this.ucGeneralFilter1.AutoScroll = ((bool)(resources.GetObject("ucGeneralFilter1.AutoScroll")));
			this.ucGeneralFilter1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("ucGeneralFilter1.AutoScrollMargin")));
			this.ucGeneralFilter1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("ucGeneralFilter1.AutoScrollMinSize")));
			this.ucGeneralFilter1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucGeneralFilter1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ucGeneralFilter1.BackgroundImage")));
			this.ucGeneralFilter1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("ucGeneralFilter1.Dock")));
			this.ucGeneralFilter1.Enabled = ((bool)(resources.GetObject("ucGeneralFilter1.Enabled")));
			this.ucGeneralFilter1.Filter = null;
			this.ucGeneralFilter1.Font = ((System.Drawing.Font)(resources.GetObject("ucGeneralFilter1.Font")));
			this.ucGeneralFilter1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("ucGeneralFilter1.ImeMode")));
			this.ucGeneralFilter1.Location = ((System.Drawing.Point)(resources.GetObject("ucGeneralFilter1.Location")));
			this.ucGeneralFilter1.Name = "ucGeneralFilter1";
			this.ucGeneralFilter1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("ucGeneralFilter1.RightToLeft")));
			this.ucGeneralFilter1.Size = ((System.Drawing.Size)(resources.GetObject("ucGeneralFilter1.Size")));
			this.ucGeneralFilter1.TabIndex = ((int)(resources.GetObject("ucGeneralFilter1.TabIndex")));
			this.ucGeneralFilter1.View = null;
			this.ucGeneralFilter1.Visible = ((bool)(resources.GetObject("ucGeneralFilter1.Visible")));
			// 
			// tbpGeneralSort
			// 
			this.tbpGeneralSort.AccessibleDescription = resources.GetString("tbpGeneralSort.AccessibleDescription");
			this.tbpGeneralSort.AccessibleName = resources.GetString("tbpGeneralSort.AccessibleName");
			this.tbpGeneralSort.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("tbpGeneralSort.Anchor")));
			this.tbpGeneralSort.AutoScroll = ((bool)(resources.GetObject("tbpGeneralSort.AutoScroll")));
			this.tbpGeneralSort.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("tbpGeneralSort.AutoScrollMargin")));
			this.tbpGeneralSort.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("tbpGeneralSort.AutoScrollMinSize")));
			this.tbpGeneralSort.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.tbpGeneralSort.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tbpGeneralSort.BackgroundImage")));
			this.tbpGeneralSort.Controls.Add(this.ucGeneralSort1);
			this.tbpGeneralSort.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("tbpGeneralSort.Dock")));
			this.tbpGeneralSort.Enabled = ((bool)(resources.GetObject("tbpGeneralSort.Enabled")));
			this.tbpGeneralSort.Font = ((System.Drawing.Font)(resources.GetObject("tbpGeneralSort.Font")));
			this.tbpGeneralSort.ImageIndex = ((int)(resources.GetObject("tbpGeneralSort.ImageIndex")));
			this.tbpGeneralSort.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("tbpGeneralSort.ImeMode")));
			this.tbpGeneralSort.Location = ((System.Drawing.Point)(resources.GetObject("tbpGeneralSort.Location")));
			this.tbpGeneralSort.Name = "tbpGeneralSort";
			this.tbpGeneralSort.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("tbpGeneralSort.RightToLeft")));
			this.tbpGeneralSort.Size = ((System.Drawing.Size)(resources.GetObject("tbpGeneralSort.Size")));
			this.tbpGeneralSort.TabIndex = ((int)(resources.GetObject("tbpGeneralSort.TabIndex")));
			this.tbpGeneralSort.Text = resources.GetString("tbpGeneralSort.Text");
			this.tbpGeneralSort.ToolTipText = resources.GetString("tbpGeneralSort.ToolTipText");
			this.tbpGeneralSort.Visible = ((bool)(resources.GetObject("tbpGeneralSort.Visible")));
			// 
			// ucGeneralSort1
			// 
			this.ucGeneralSort1.AccessibleDescription = resources.GetString("ucGeneralSort1.AccessibleDescription");
			this.ucGeneralSort1.AccessibleName = resources.GetString("ucGeneralSort1.AccessibleName");
			this.ucGeneralSort1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("ucGeneralSort1.Anchor")));
			this.ucGeneralSort1.AutoScroll = ((bool)(resources.GetObject("ucGeneralSort1.AutoScroll")));
			this.ucGeneralSort1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("ucGeneralSort1.AutoScrollMargin")));
			this.ucGeneralSort1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("ucGeneralSort1.AutoScrollMinSize")));
			this.ucGeneralSort1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucGeneralSort1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ucGeneralSort1.BackgroundImage")));
			this.ucGeneralSort1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("ucGeneralSort1.Dock")));
			this.ucGeneralSort1.Enabled = ((bool)(resources.GetObject("ucGeneralSort1.Enabled")));
			this.ucGeneralSort1.Filter = null;
			this.ucGeneralSort1.Font = ((System.Drawing.Font)(resources.GetObject("ucGeneralSort1.Font")));
			this.ucGeneralSort1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("ucGeneralSort1.ImeMode")));
			this.ucGeneralSort1.Location = ((System.Drawing.Point)(resources.GetObject("ucGeneralSort1.Location")));
			this.ucGeneralSort1.Name = "ucGeneralSort1";
			this.ucGeneralSort1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("ucGeneralSort1.RightToLeft")));
			this.ucGeneralSort1.Size = ((System.Drawing.Size)(resources.GetObject("ucGeneralSort1.Size")));
			this.ucGeneralSort1.TabIndex = ((int)(resources.GetObject("ucGeneralSort1.TabIndex")));
			this.ucGeneralSort1.View = null;
			this.ucGeneralSort1.Visible = ((bool)(resources.GetObject("ucGeneralSort1.Visible")));
			// 
			// tbpSpecificSort
			// 
			this.tbpSpecificSort.AccessibleDescription = resources.GetString("tbpSpecificSort.AccessibleDescription");
			this.tbpSpecificSort.AccessibleName = resources.GetString("tbpSpecificSort.AccessibleName");
			this.tbpSpecificSort.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("tbpSpecificSort.Anchor")));
			this.tbpSpecificSort.AutoScroll = ((bool)(resources.GetObject("tbpSpecificSort.AutoScroll")));
			this.tbpSpecificSort.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("tbpSpecificSort.AutoScrollMargin")));
			this.tbpSpecificSort.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("tbpSpecificSort.AutoScrollMinSize")));
			this.tbpSpecificSort.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.tbpSpecificSort.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tbpSpecificSort.BackgroundImage")));
			this.tbpSpecificSort.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("tbpSpecificSort.Dock")));
			this.tbpSpecificSort.Enabled = ((bool)(resources.GetObject("tbpSpecificSort.Enabled")));
			this.tbpSpecificSort.Font = ((System.Drawing.Font)(resources.GetObject("tbpSpecificSort.Font")));
			this.tbpSpecificSort.ImageIndex = ((int)(resources.GetObject("tbpSpecificSort.ImageIndex")));
			this.tbpSpecificSort.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("tbpSpecificSort.ImeMode")));
			this.tbpSpecificSort.Location = ((System.Drawing.Point)(resources.GetObject("tbpSpecificSort.Location")));
			this.tbpSpecificSort.Name = "tbpSpecificSort";
			this.tbpSpecificSort.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("tbpSpecificSort.RightToLeft")));
			this.tbpSpecificSort.Size = ((System.Drawing.Size)(resources.GetObject("tbpSpecificSort.Size")));
			this.tbpSpecificSort.TabIndex = ((int)(resources.GetObject("tbpSpecificSort.TabIndex")));
			this.tbpSpecificSort.Text = resources.GetString("tbpSpecificSort.Text");
			this.tbpSpecificSort.ToolTipText = resources.GetString("tbpSpecificSort.ToolTipText");
			this.tbpSpecificSort.Visible = ((bool)(resources.GetObject("tbpSpecificSort.Visible")));
			// 
			// ucSelection
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.Controls.Add(this.tabPages);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.Name = "ucSelection";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.Size = ((System.Drawing.Size)(resources.GetObject("$this.Size")));
			this.tabPages.ResumeLayout(false);
			this.tbpSearch.ResumeLayout(false);
			this.tbpGeneralFilter.ResumeLayout(false);
			this.tbpGeneralSort.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region	public members
		public ViewFilter	Filter
		{
			get
			{
				return this.filter;
			}
			set
			{
				this.filter = value;
			}
		}

		public ViewFilter.ViewRow	View
		{
			get
			{
				return this.view;
			}
			set
			{
				this.view = value;
				UpdateData();
			}
		}

		#endregion

		#region	private members
		private	void	UpdateData()
		{
			if (this.filter == null || this.view == null)
			{
				DefaultData();
			}
			else
			{
				ViewFilter.ViewSelectionRow[] rows1 = this.view.GetViewSelectionRows();
				if (rows1 == null || rows1.Length == 0)
				{
					this.ucSelectionPage2.Filter = this.filter;
					this.ucSelectionPage2.View = this.filter.ViewSelection.NewViewSelectionRow();
					this.ucSelectionPage2.View.ViewRow = this.view;
					this.filter.ViewSelection.AddViewSelectionRow(this.ucSelectionPage2.View);
				}
				else
				{
					ViewFilter.ViewSelectionRow row = rows1[0];
					this.ucSelectionPage2.Filter = this.filter;
					this.ucSelectionPage2.View = row;
				}

				ViewFilter.GeneralFilterRow[] rows2 = this.view.GetGeneralFilterRows();
				if (rows2 == null || rows2.Length == 0)
				{
					this.ucGeneralFilter1.Filter = this.filter;
					this.ucGeneralFilter1.View = this.filter.GeneralFilter.NewGeneralFilterRow();
					this.ucGeneralFilter1.View.ViewRow = this.view;
					this.filter.GeneralFilter.AddGeneralFilterRow(this.ucGeneralFilter1.View);
				}
				else
				{
					ViewFilter.GeneralFilterRow row = rows2[0];
					this.ucGeneralFilter1.Filter = this.filter;
					this.ucGeneralFilter1.View = row;
				}

				ViewFilter.GeneralSortRow[] rows3 = this.view.GetGeneralSortRows();
				if (rows3 == null || rows3.Length == 0)
				{
					this.ucGeneralSort1.Filter = this.filter;
					this.ucGeneralSort1.View = this.filter.GeneralSort.NewGeneralSortRow();
					this.ucGeneralSort1.View.ViewRow = this.view;
					this.filter.GeneralSort.AddGeneralSortRow(this.ucGeneralSort1.View);
				}
				else
				{
					ViewFilter.GeneralSortRow row = rows3[0];
					this.ucGeneralSort1.View = row;
					this.ucGeneralSort1.Filter = this.filter;
				}
			}
		}

		private void DefaultData()
		{
			this.ucSelectionPage2.Filter = this.filter;
			this.ucSelectionPage2.View = null;
			this.ucGeneralFilter1.View = null;
			this.ucGeneralSort1.View   = null;
		}
		#endregion
	}
}
