using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Windows.Forms;

namespace UserControls
{
	/// <summary>
	/// Summary description for ucAbnormalFlights.
	/// </summary>
	public class ucAbnormalFlights : System.Windows.Forms.UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ucAbnormalFlights()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// ucAbnormalFlights
			// 
			this.Name = "ucAbnormalFlights";
			this.Size = new System.Drawing.Size(512, 200);
			this.Load += new System.EventHandler(this.ucAbnormalFlights_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.ucAbnormalFlights_Paint);

		}
		#endregion

		private void ucAbnormalFlights_Load(object sender, System.EventArgs e)
		{
		
		}

		private void ucAbnormalFlights_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Rectangle rect = this.ClientRectangle;

			int height = rect.Size.Width / 25;
			int width  = rect.Size.Height;

			rect.Width = 24 * height;
			System.Drawing.Pen pen = new Pen(Color.Black);
			e.Graphics.DrawRectangle(pen,rect);


			int x = 8 * height;
			int y = 0;
			width  = rect.Size.Width - 10 * height;
			Rectangle rect1 = new Rectangle(x,y,width,height);

			e.Graphics.DrawString("Delay reason",this.Font,System.Drawing.Brushes.Black,rect1);

			Matrix myMatrix = new Matrix();

			for (int i = 0; i < 25; i++)
			{
				if (i < 8 || i > 22)
				{
					myMatrix.Reset();

					x = (i+1) * height;
					y = 0;
					width  = rect.Size.Height;

					myMatrix.RotateAt(90, new PointF(x,y),MatrixOrder.Append);
					e.Graphics.Transform = myMatrix;

					rect1 = new Rectangle(x,y,width,height);
					e.Graphics.DrawRectangle(pen,rect1);

					switch(i)
					{
						case 24:
							e.Graphics.DrawString("Count of VIP groups",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 23:
							e.Graphics.DrawString("Count of special planes",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 7:
							e.Graphics.DrawString("Nominal rating of flights",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 6:
							e.Graphics.DrawString("Count of cancel flights",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 5:
							e.Graphics.DrawString("Count of training flights",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 4:
							e.Graphics.DrawString("Count of W/Z and Delay departure flights",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 3:
							e.Graphics.DrawString("Count of W/Z departure flights",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 2:
							e.Graphics.DrawString("Count of Departure flights",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 1:
							e.Graphics.DrawString("Count of Arrival flights",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 0:
							e.Graphics.DrawString("Date",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
					}
				}
				else
				{
					myMatrix.Reset();

					x = (i+1) * height;
					y = height;
					width  = rect.Size.Height - height;

					myMatrix.RotateAt(90, new PointF(x,y),MatrixOrder.Append);
					e.Graphics.Transform = myMatrix;

					rect1 = new Rectangle(x,y,width,height);
					e.Graphics.DrawRectangle(pen,rect1);

					switch(i)
					{
						case 22:
							e.Graphics.DrawString("Due to other reasons",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 21:
							e.Graphics.DrawString("Due to accident of the ground guarantee",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 20:
							e.Graphics.DrawString("Due to the joint inspection",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 19:
							e.Graphics.DrawString("Due to the lights of the ground and runway",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 18:
							e.Graphics.DrawString("Due to the passengers",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 17:
							e.Graphics.DrawString("Due to the aircrew",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 16:
							e.Graphics.DrawString("Due to the communication navigation",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 15:
							e.Graphics.DrawString("Due to the food guarantee",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 14:
							e.Graphics.DrawString("Due to the ground guarantee",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 13:
							e.Graphics.DrawString("Due to the air business",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 12:
							e.Graphics.DrawString("Due to air flow control",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 11:
							e.Graphics.DrawString("Due to air route control",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 10:
							e.Graphics.DrawString("Due to the airplan schedule",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 9:
							e.Graphics.DrawString("Due to the wheather condition of that airport",this.Font,System.Drawing.Brushes.Black,rect1);
							break;
						case 8:
							e.Graphics.DrawString("Due to the wheather condition of this airport",this.Font,System.Drawing.Brushes.Black,rect1);
							break;

					}
				}
			}

			pen.Dispose();
		
		}
	}
}
