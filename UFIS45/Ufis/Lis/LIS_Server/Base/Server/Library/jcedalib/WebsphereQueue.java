
package jcedalib; 
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Level;

import java.io.*;
import java.net.*;
import java.lang.Thread;
import java.util.Date;

public class WebsphereQueue
{
  private static String mks_version = "@(#) UFIS_VERSION $Id: Ufis/Lis/LIS_Server/Base/Server/Library/jcedalib/WebsphereQueue.java 1.5 2006/05/26 15:55:33SGT mcu Exp  $";

  private Socket hWorkSocket = null;
  private ServerSocket hStartSocket = null;
	private String slHostname = null;
	private int ilService;
	private int ilTimeout = 120000;
	private String slPutGet;
	private BufferedReader hReader = null;
	private PrintStream hWriter = null;
	private String slType = "QUEUE";
	private String slSockType = "SERVER";
	private int ilCheckInterval = 0;
	private long llNextCheckTime;
  private String slCheckString;
	private String slHostAllowed = null;

static Logger dbg,msglog,failed_msglog;
  	
      public WebsphereQueue(String hostname, int port, String SockType, String put_get,int ipCheckInterval,String spHostAllowed,int ipTimeout) 
			throws Exception
			{
            try 
            {
								
									slHostAllowed = spHostAllowed;
               dbg = Logger.getLogger("jxmlif");
    							slHostname = hostname;
                  ilService  = port;
                  slType = "SOCKET";
                  slSockType = SockType;
									ilTimeout = ipTimeout;

                  if (SockType.equals("SERVER"))
                  {
                        dbg.debug("new ServerSocket follows");
                        hStartSocket = new ServerSocket(ilService); 
                        dbg.debug("accept follows");
                        hWorkSocket = hStartSocket.accept();
                        hStartSocket.close();
												String slWorkSocket = hWorkSocket.toString();
												hWorkSocket.setSendBufferSize(256);
												hWorkSocket.setSoTimeout(ilTimeout);
												int iSoTimeout = 	hWorkSocket.getSoTimeout();

												int iSendBufferSize = hWorkSocket.getSendBufferSize();
												String slBufferSize = "Buffersize=" + iSendBufferSize + " Timeout: " + iSoTimeout;
                        dbg.debug("after accept Socket: " + slWorkSocket + slBufferSize);
												if (slHostAllowed != null)
												{
													if(slWorkSocket.indexOf(slHostAllowed) < 0)
													{
                  						throw new Exception("Illegal connect from: " + slWorkSocket);
													}
												}
                        hReader = new BufferedReader( new InputStreamReader(hWorkSocket.getInputStream()));
                        hWriter = new PrintStream( hWorkSocket.getOutputStream(), true);
                 }
                  else
                  {
                      boolean blConnected = false;
                      do
                      {
                        try
                        {
                              hWorkSocket = new Socket(slHostname,ilService);  
                              blConnected = true;
                              hWriter = new PrintStream( hWorkSocket.getOutputStream(), true);
												hWorkSocket.setSendBufferSize(512);
												int iSendBufferSize = hWorkSocket.getSendBufferSize();
												hWorkSocket.setSoTimeout(ilTimeout);
												int iSoTimeout = 	hWorkSocket.getSoTimeout();
												dbg.debug("CLIENT:SendBufferSize=" + iSendBufferSize + " Timeout=" + iSoTimeout);
                              hReader = new BufferedReader( new InputStreamReader(hWorkSocket.getInputStream()));
                        }
                        catch (Exception e)
                        {
                              dbg.debug("Exception while connecting to WQOutput1Queue:" + e);
                              blConnected = false;
                              java.lang.Thread.sleep(10000);
                        }
                      } while (blConnected == false);
                      
        						}
                    if (ipCheckInterval > 0)
                    {
                            ilCheckInterval = ipCheckInterval;
                            Date rlActualTime = new Date();
                            llNextCheckTime = rlActualTime.getTime();
                            llNextCheckTime += ilCheckInterval;
                    }
   //               slCheckString = spCheckString;
            }
            catch (Exception e)
            {
                  throw new Exception(e);
            }
      } // end WebsphereQueue


	public void setTimeout(int ipTimeout)
	{
		ilTimeout = ipTimeout;
	}

  public void DisconnectQueue()
  throws Exception 
  {
    try
    {
        if (hStartSocket != null)
        {
                hStartSocket.close();
        }
        if (hWorkSocket != null)
        {
                hWorkSocket.close();
        }
    }
    catch (Exception e)
    {
      throw new Exception("Exception in DisconnectQueue(): " + e);
    }
  } // end DisconnectQueue()
	   			
  public void sendMsgNew(String msg)
  throws Exception 
  {
    try 
    {
		  dbg.debug("	hWriter.format follows: length=" +
					 msg.length() + " msg=<" + msg + ">");
			hWriter.format("MSGLEN:%05d%s",msg.length(),msg);
			hWriter.flush();
			if (hWriter.checkError())
			{
				dbg.debug("Error writing message");
      	throw new Exception("Error in sendMsg: ");
			}
    }
    catch (Exception ex) 
    {
		  dbg.debug("Exception in sendMsg: " + ex);
      throw new Exception("Exception in sendMsg: " + ex);
    }
		dbg.debug("Message successfully sent");
  } // end sendMsg
	

  public void sendMsg(String msg)
  throws Exception 
  {
    try 
    {
		  dbg.debug("	hWriter.format follows: length=" +
					 msg.length() + " msg=<" + msg + ">");
			hWriter.format("MSGLEN:%08d%s",msg.length(),msg);
			//hWriter.flush();
			if (hWriter.checkError())
			{
				dbg.debug("Error writing message");
      	throw new Exception("Error in sendMsg: ");
			}
    }
    catch (Exception ex) 
    {
		  dbg.debug("Exception in sendMsg: " + ex);
      throw new Exception("Exception in sendMsg: " + ex);
    }
		dbg.debug("Message successfully sent");
		return;
  } // end sendMsg
	

	
  public boolean isConnected()
	{
      //return (!hWorkSocket.isClosed());
      return (!hWorkSocket.isConnected());
  }
  
	
  public String receiveMsg()
  throws Exception 
  {
        int i = 0;
        int ilLen;
        StringBuffer clHead = new StringBuffer();
        StringBuffer msgBuf  = new StringBuffer();
        String msg;
        int ilByte = 0;

        try 
        {
    		char clHeadBuffer[] = new char[15];
                do
                {
                        if (hReader.ready())
                        {
        ilByte = hReader.read(clHeadBuffer,0,15);
                        }
                        else
                        {
													if (ilCheckInterval > 0)
													{
                                Date rlActualTime = new Date();
                                long llActualTime = rlActualTime.getTime();
                                if (llActualTime > llNextCheckTime)
                                {
                                    llNextCheckTime += ilCheckInterval;
                                    
                                    throw new Exception("TimeOut Exception in receiveMsg while receiving Header: ");
                                }
													}
                        }
								if (ilByte < 15)
								{
									Thread.sleep(1000);
								}
               } while (ilByte < 15);


        clHead.append(clHeadBuffer);
        String clMsgLen = clHead.substring(0,7);
        //if (clHead.        if (clHead.substring(0,6).equals("MSGLEN:"))substring(0,6).equals("MSGLEN:"))
        if (clMsgLen.equals("MSGLEN:"))
        {
        	ilLen = Integer.parseInt(clHead.substring(7));
					dbg.debug("MSG-Header received <" + clHead + "> len=" + ilLen + " <" + clHead.substring(7)); 
	        try 
	        {
					  int ilBytesToRead = ilLen;	
						int ilOffset = 0;
	        	char clMsgBuffer[] = new char[ilBytesToRead];
						do
						{
               if (hReader.ready())
               {
	            		ilByte = hReader.read(clMsgBuffer,ilOffset,ilBytesToRead);
									if (ilByte > 0)
									{
										ilBytesToRead -= ilByte;
										ilOffset += ilByte;
										dbg.debug("receiveMsg: read " + ilByte + " bytes, " + ilBytesToRead + 
												" bytes left to read ");
									}
							}
							else
							{
                  java.lang.Thread.sleep(100);
									if (ilCheckInterval > 0)
									{
                    Date rlActualTime = new Date();
                    long llActualTime = rlActualTime.getTime();
                    if (llActualTime > llNextCheckTime)
                    {
                      llNextCheckTime += ilCheckInterval;
                      throw new Exception("TimeOut Exception in receiveMsg while receiving Message: ");
                    }
								}

							}
						} while (ilBytesToRead > 0);
	          msgBuf.append(clMsgBuffer);
	         }
	        catch (Exception ex) 
	        {
						 dbg.debug("1.Exception in receiveMessage: " + ex );
	           throw new Exception("1.Exception" + ex);
	        }
        }
        }
        catch (Exception ex) 
        {
					dbg.debug("2.Exception in receiveMessage: " + ex );
          throw new Exception("2.Exception in receiveMsg: " + ex);
        }

				Date rlActualTime = new Date();
				llNextCheckTime = rlActualTime.getTime();
				llNextCheckTime += ilCheckInterval;
							
        return msgBuf.toString();
  } // end recvMsg
    
} // end Class WebsphereQueue

