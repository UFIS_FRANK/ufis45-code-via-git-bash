#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Lis/LIS_Server/Base/Server/Library/Cedalib/nat.c 1.3 2004/04/02 20:19:16SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/*
 * ABB/AAT New AAT Tools 
 *
 * Author         : Janos Heilig
 * Creation Date  : Aug 17 01
 * Description    : new tools
 *
 * Update history : 
 *
 * $Log: Ufis/Lis/LIS_Server/Base/Server/Library/Cedalib/nat.c  $
 * Revision 1.3 2004/04/02 20:19:16SGT jwe 
 * changed includes for HP-UX compliance
 * Revision 1.2 2004/04/02 14:12:33CEST jwe 
 * added mks-version string to sources/headers
 * Revision 1.1 2004/04/01 17:12:54CEST jwe 
 * Initial revision
 * Member added to project /usr1/projects/UFIS4.5/LIS_Server.pj
 * Revision 1.1 2002/04/29 09:03:18CEST jwe 
 * Initial revision
 * Member added to project /usr1/projects/LISBON/Cedalib.pj
 * Revision 1.1 2001/09/28 16:31:37GMT+02:00 jwe 
 * Initial revision
 * Revision 1.3  2001/08/31 20:45:48  project
 * que sys running at abb, pe no ready
 *
 * Revision 1.2  2001/08/29 20:40:09  project
 * before changing to sigaction
 *
 * Revision 1.1  2001/08/29 19:35:19  project
 * Initial revision
 *
 * -----
 * $Id: Ufis/Lis/LIS_Server/Base/Server/Library/Cedalib/nat.c 1.3 2004/04/02 20:19:16SGT jwe Exp  $
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdarg.h>
#include <unistd.h>

#include <fcntl.h>
#include <string.h>
#include <time.h>

#include <signal.h>

#include <netinet/in.h>

#include <sys/timeb.h>
#if defined(_HPUX_SOURCE)
	#include <time.h>
#else
	#include <sys/select.h>
#endif
#include <sys/time.h>

#if defined(_HPUX_SOURCE)
	#include <arpa/inet.h>
#else
	#include <inet.h>
#endif
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include <sys/ipc.h>
#include <sys/msg.h>

#include <sys/wait.h>

#if !defined(_LINUX) && !defined(_HPUX_SOURCE)
# include <sys/sockio.h> /* solaris/unixware */
#endif

#ifndef LINUX
#include "ugccsma.h"
#endif
#include "glbdef.h"
#include "uevent.h"
#include "quedef.h"
#include "nat.h"

/*
 *=== private declarations
 */

static void nat_Asctime(char *);

static int nat_logmask = NAT_LERR;
static FILE *nat_logfp = NULL;
static int nat_errno = 0;
static char nat_errtext[256];
static char nat_pidstr[32] = "()";
static char nat_modstr[128];

/*
 *=== Public API
 */

int NatSetLog(int flags, const char *fname, const char *modstr)
{
FILE *fp;
int rc, pid;

	rc = 0;

	nat_logmask = flags & (NAT_LLMASK | NAT_LOMASK);
	if(modstr)
		strncpy(nat_modstr, modstr, sizeof(nat_modstr)-1);
	else
		nat_modstr[0] = '\0';

	if(fname == NULL && nat_logfp != stderr)
	{
		if(nat_logfp)
			fclose(nat_logfp);
		nat_logfp = stderr;
	}

	if(fname != NULL)
	{
		fp = fopen(fname,"w");
		if (fp != NULL)
		{
			if(nat_logfp && nat_logfp != stderr)
				fclose(nat_logfp);
			nat_logfp = fp;
		}
		else
		{
			rc = -1;
		}
	}

	if(nat_logmask & NAT_LPID)
	{
		pid = getpid();
		sprintf(nat_pidstr,"%d", pid);
	}

	return rc;
}

void NatLog(int level, const char *fmt, ...)
{
  va_list args;
	char timestr[100];

  va_start(args, fmt);

	if(nat_logfp == NULL)
		nat_logfp = stderr;

  if (level & nat_logmask & NAT_LLMASK)
  {
		if(nat_modstr[0])
			fprintf(nat_logfp,"%s ", nat_modstr);
		if(nat_logmask & NAT_LPID)
			fprintf(nat_logfp,"(%s) ", nat_pidstr);
		if(nat_logmask & NAT_LTIME)
		{
			nat_Asctime(timestr);
			fprintf(nat_logfp,"%s ", timestr);
		}

    vfprintf(nat_logfp, fmt, args);
		fprintf(nat_logfp,"\n");
		if(nat_logmask & NAT_LFLUSH)
    	fflush(nat_logfp);
  }
  va_end(args);
}

int NatErrNo()
{
	return nat_errno;
}

char *NatErrText()
{
	return nat_errtext;
}

int nat_Error(int en, char *msg)
{
char *msgsys;
int maxl;

	if(en == NAT_E_SYSTEM)
	{
		nat_errno = errno;
		msgsys = strerror(errno);
	}
	else
	{
		nat_errno = en;
		msgsys = NULL;
	}
		
	memset(nat_errtext, 0 , sizeof(nat_errtext));
	maxl = sizeof(nat_errtext)/2 - 9; /* xxx:''z */

	if(msg)
	{
		strcat(nat_errtext,"msg:\"");
		strncat(nat_errtext, msg, maxl);
		strcat(nat_errtext,"\"");
	}

	if(msgsys)
	{
		if(msg)
			strcat(nat_errtext," ");
		strcat(nat_errtext,"sys:\"");
		strncat(nat_errtext, msgsys, maxl);
		strcat(nat_errtext,"\"");
	}
	
	NatLog(NAT_LLIBERR, "nat_Error: <%d> <%s>", nat_errno, nat_errtext);
	return -1;
}

int nat_Assert(const char* ex, const char *f, int l)
{
	NatLog(NAT_LERR,"ASSERT(%s) failed, file %s line %d -- aborting", ex, f, l);
	abort();
}

/*
 * === nat private part
 */

static void nat_Asctime(char *buff)
{
time_t t;
struct tm *p;

	t = time(NULL);
	p = localtime(&t);

	sprintf(buff,"%02d.%02d.%04d %02d:%02d:%02d", p->tm_mday, p->tm_mon+1,
          p->tm_year+1900, p->tm_hour, p->tm_min, p->tm_sec);
	return;
}

/*
 *=== DynStr: dynamic string utility to handle growing strings
 */


DynStr *DynStr_Create()
{
	return DynStr_CreateQ(0);
}

DynStr *DynStr_CreateQ(unsigned int q)      
{
DynStr *p;
	p = malloc(sizeof(DynStr));
	ASSERT(p);
	p->chars = NULL;
	p->blksz = q > 1 ? q : 64;
	DynStr_Reset(p);
	return p;
}

void DynStr_Destroy(DynStr *pThis)
{
	if(pThis == NULL)
		return;
	if(pThis->chars)
		free(pThis->chars);
	free(pThis);
}

void DynStr_AppendStr(DynStr *pThis, char *s)
{
	if (s == NULL)
		return;
	while(*s)
		DynStr_AppendC(pThis, *s++);
}

    
void DynStr_AppendC(DynStr *pThis, char c)        
{
	if (pThis->nfree <= 1) /* one for \0 */
	{
		++(pThis->nblk);
		pThis->chars = realloc(pThis->chars, pThis->nblk * pThis->blksz);
		ASSERT(pThis->chars);
		pThis->nfree += pThis->blksz;
	}
	pThis->chars[pThis->length++] = c;
	pThis->chars[pThis->length  ] = '\0';
	--(pThis->nfree);
	
}

void DynStr_Reset(DynStr *pThis)
{
	if (pThis->chars)
		free(pThis->chars);
	pThis->nblk = 0;
	pThis->nfree = 0;
	pThis->length = 0;
	pThis->chars = NULL;
}

/*
 * ===== TokenZ: Token class to get tokens as null terminated string
 */

TokenZ *TokenZ_CreateFromString(char **p2cSrc, char *pcSkip, char *pcTerm)
{
DynStr *tok;
char *pp;
/*
 * no source, no token
 */
	if (p2cSrc == NULL)
		return NULL;

/*
 * skip unwished leading chars
 */

	while(**p2cSrc)
	{
		for(pp=pcSkip; *pp; ++pp)
		{
			if (**p2cSrc == *pp)
				break;
		}
		if (!*pp)	/* then it was not a char from the skip set */
			break;
		++*p2cSrc;
	}
	if(!**p2cSrc)
		return NULL;
/*
 * get token char until either any of the delimiters or null
 */
	tok = NULL;
	while(**p2cSrc)
	{
		for(pp=pcTerm; pp && *pp; ++pp)
		{
			if (**p2cSrc == *pp)
				break;
		}
		if (pp && *pp) /* then it was a char from the term set */
		{
			break;
		}
		else
		{
			if (tok == NULL)
			{
				tok = DynStr_Create();
				ASSERT(tok);
			}
			DynStr_AppendC(tok, **p2cSrc);
		}
		++*p2cSrc;
	}
	return tok;
}

void TokenZ_Destroy(TokenZ *pThis)
{
	if(pThis == NULL)
		return;
	if (pThis->chars)
		free(pThis->chars);
	free(pThis);
}

int TokenZ_GetStrings(char **p2cSrc, char *pcDel, char ***p3cList)
{
int n;
TokenZ *tz;

	*p3cList = NULL;
	for(n=0; (tz = TokenZ_CreateFromString(p2cSrc, pcDel, pcDel)) != NULL; ++n)
	{
		ASSERT(tz->chars);
		*p3cList = realloc(*p3cList, (n+1) * sizeof(char *));
		ASSERT(*p3cList);
		(*p3cList)[n] = strdup(tz->chars);
		TokenZ_Destroy(tz);
	}
	return n;
}

/*
 *=== Event Handling API
 */

/* implementation (private) declarations */
#define eh_MAXINPUTS	512
#define eh_MAXOUTPUTS	512
#define eh_MAXTIMERS	100
#define eh_MAXSIGNALS	100
#define eh_MAXSIGNUM	32

typedef struct
{
	int in_use;
	int fd;
	EHInputCB cb;
	void *data;
} EHInputCtl;

typedef struct
{
	int in_use;
	int fd;
	EHOutputCB cb;
	void *data;
} EHOutputCtl;

typedef struct
{
	int in_use;
	unsigned long val_ms, trg_time, trg_millitm;
	EHTimerCB cb;
	void *data;
} EHTimerCtl;

typedef struct
{
	int in_use;
	int signo;
	int hit;
	EHSignalCB cb;
	void *data;
} EHSignalCtl;
	
typedef struct
{
	int count;
	struct sigaction old_sa;
} EHSignalEntry;

 

static EHInputCtl eh_inputs[eh_MAXINPUTS];
static EHOutputCtl eh_outputs[eh_MAXOUTPUTS];
static EHTimerCtl eh_timers[eh_MAXOUTPUTS];
static EHSignalCtl eh_signals[eh_MAXSIGNALS];
static EHSignalEntry eh_sigtable[eh_MAXSIGNUM];

static int eh_init = 0;

static void eh_SigHandler(int);
static int eh_HandleEvents(void);
static void eh_Reset(EHTimerCtl *, struct timeb *);
static unsigned long eh_StepTimer(EHTimerCtl *, struct timeb *);


/*
 * Should be called before any other EH function.
 * Currently it only sets an init flag (no need to init bss structs).
 * Real initialization required if called more than once.
 */

int EHInitialize()
{
	eh_init = 1;
	return 0;
}

EHInputId EHAddInput(int fd, EHInputCB cb, void *data_ptr)
{
int ix;

	if(!eh_init)
		return nat_Error(NAT_E_INIT,NULL);
	if(fd < 0)
		return nat_Error(NAT_E_BADFILE,NULL);

	for(ix=0; ix<eh_MAXINPUTS; ++ix)
	{
		if(!eh_inputs[ix].in_use)
			break;
	}
	if(ix >= eh_MAXINPUTS)
		return nat_Error(NAT_E_NOSPACE,NULL);

	eh_inputs[ix].in_use = 1;
	eh_inputs[ix].fd = fd;
	eh_inputs[ix].cb = cb;
	eh_inputs[ix].data = data_ptr;
	return ix;
}

int EHRemoveInput(EHInputId id)
{
	if(!eh_init)
		return nat_Error(NAT_E_INIT,NULL);
	if(id < 0 || id >= eh_MAXINPUTS || !eh_inputs[id].in_use)
		return nat_Error(NAT_E_BADVAL,NULL);
	eh_inputs[id].in_use = 0;
	return 0;
}

EHOutputId EHAddOutput(int fd, EHOutputCB cb, void *data_ptr)
{
int ix;

	if(!eh_init)
		return nat_Error(NAT_E_INIT,NULL);
	if(fd < 0)
		return nat_Error(NAT_E_BADFILE,NULL);

	for(ix=0; ix<eh_MAXOUTPUTS; ++ix)
	{
		if(!eh_outputs[ix].in_use)
			break;
	}
	if(ix >= eh_MAXOUTPUTS)
		return nat_Error(NAT_E_NOSPACE,NULL);

	eh_outputs[ix].in_use = 1;
	eh_outputs[ix].fd = fd;
	eh_outputs[ix].cb = cb;
	eh_outputs[ix].data = data_ptr;
	return ix;
}

int EHRemoveOutput(EHOutputId id)
{
	if(!eh_init)
		return nat_Error(NAT_E_INIT,NULL);
	if(id < 0 || id >= eh_MAXOUTPUTS || !eh_outputs[id].in_use)
		return nat_Error(NAT_E_BADVAL,NULL);
	eh_outputs[id].in_use = 0;
	return 0;
}

EHTimerId EHAddTimer(unsigned long ms, EHTimerCB cb, void *data_ptr)
{
int ix;
	if(!eh_init)
		return nat_Error(NAT_E_INIT,NULL);

	for(ix=0; ix<eh_MAXTIMERS; ++ix)
	{
		if(!eh_timers[ix].in_use)
			break;
	}

	if(ix >= eh_MAXTIMERS)
		return nat_Error(NAT_E_NOSPACE,NULL);

	eh_timers[ix].in_use = 1;
	eh_timers[ix].val_ms = ms;
	eh_timers[ix].cb = cb;
	eh_timers[ix].data = data_ptr;
	eh_Reset(eh_timers+ix, NULL);
	return ix;
}

int EHRemoveTimer(EHTimerId id)
{
	if(!eh_init)
		return nat_Error(NAT_E_INIT,NULL);
	if(id < 0 || id >= eh_MAXTIMERS || !eh_timers[id].in_use)
		return nat_Error(NAT_E_BADVAL,NULL);
	eh_timers[id].in_use = 0;
	return 0;
}

int EHResetTimer(EHTimerId id)
{
	if(!eh_init)
		return nat_Error(NAT_E_INIT,NULL);
	if(id < 0 || id >= eh_MAXTIMERS || !eh_timers[id].in_use)
		return nat_Error(NAT_E_BADVAL,NULL);
	eh_Reset(eh_timers+id, NULL);
	return 0;
}

EHSignalId EHAddSignal(int signo, EHSignalCB cb, void *data_ptr)
{
int ix, sx;
struct sigaction sa;

	if(!eh_init)
		return nat_Error(NAT_E_INIT,NULL);
	if(signo < 1 || signo > eh_MAXSIGNUM)
		return nat_Error(NAT_E_BADVAL,NULL);

	for(ix=0; ix<eh_MAXSIGNALS; ++ix)
	{
		if(!eh_signals[ix].in_use)
			break;
	}

	if(ix >= eh_MAXSIGNALS)
		return nat_Error(NAT_E_NOSPACE,NULL);

	eh_signals[ix].in_use = 1;
	eh_signals[ix].signo = signo;
	eh_signals[ix].hit = 0;
	eh_signals[ix].cb = cb;
	eh_signals[ix].data = data_ptr;

	sx = signo - 1;

	if (eh_sigtable[sx].count == 0)
	{
		sa.sa_handler = eh_SigHandler;
		sigemptyset(&sa.sa_mask); 
		sa.sa_flags = 0;
		if(sigaction(signo, &sa, &eh_sigtable[sx].old_sa) == -1)
			return nat_Error(NAT_E_SYSTEM, NULL);
	}

	++eh_sigtable[sx].count;

	return ix;
}

int EHRemoveSignal(EHSignalId id)
{
int ix;
	if(!eh_init)
		return nat_Error(NAT_E_INIT,NULL);
	if(id < 0 || id >= eh_MAXSIGNALS || !eh_signals[id].in_use)
		return nat_Error(NAT_E_BADVAL,NULL);

	eh_signals[id].in_use = 0;

	ix = eh_signals[id].signo - 1;
	ASSERT(ix >= 0 && ix < eh_MAXSIGNUM);

	ASSERT(eh_sigtable[ix].count > 0);
	--eh_sigtable[ix].count;

	if(eh_sigtable[ix].count == 0)
		sigaction(ix+1, NULL, &eh_sigtable[ix].old_sa);

	return 0;
}

static void eh_SigHandler(int signo)
{
int ix;
	for(ix=0; ix<eh_MAXSIGNALS; ++ix)
	{
		if(eh_signals[ix].in_use && eh_signals[ix].signo == signo)
			++eh_signals[ix].hit;
	}
}

int EHHandleEvents(int loop)
{
int res;
	do
	{
		res = eh_HandleEvents();
	} while(loop && res != -1);
	return res;
}

int eh_HandleEvents()
{
fd_set fdr, fdw, fdx;
int ix, tmc, numfd, res;
unsigned long t, tmin; 
struct timeval tmv, *ptmv;
struct timeb tmb;

	if(!eh_init)
		return nat_Error(NAT_E_INIT,NULL);

	numfd = 0;
	FD_ZERO(&fdr);
	FD_ZERO(&fdw);
	FD_ZERO(&fdx);

	for(ix=0; ix<eh_MAXINPUTS; ++ix)
	{
		if(eh_inputs[ix].in_use)
		{
			FD_SET(eh_inputs[ix].fd, &fdr);
			if (eh_inputs[ix].fd >= numfd)
				numfd = eh_inputs[ix].fd + 1;
		}
	}

	for(ix=0; ix<eh_MAXOUTPUTS; ++ix)
	{
		if(eh_outputs[ix].in_use)
		{
			FD_SET(eh_outputs[ix].fd, &fdw);
			if (eh_outputs[ix].fd >= numfd)
				numfd = eh_outputs[ix].fd + 1;
		}
	}

	ftime(&tmb);
	for(ix = tmc = 0; ix<eh_MAXTIMERS; ++ix)
	{
		if(eh_timers[ix].in_use)
		{
			t = eh_StepTimer(eh_timers+ix, &tmb);
			if(tmc == 0)
				tmin = t;
			else if(t < tmin)
				tmin = t;
			++tmc;
		}
	}

	if(tmc == 0)
	{
		ptmv = NULL;
	}
	else
	{
		tmv.tv_sec = tmin/1000;
    tmv.tv_usec = 1000 * (tmin%1000);
		ptmv = &tmv;
/* dbg
printf("select with %d s %d u\n", tmv.tv_sec, tmv.tv_usec);
dbg */
	}

	res = select(numfd, &fdr, &fdw, &fdx, ptmv);

/* dbg
printf("res=%d errno=%d\n", res, errno);
dbg */
	if(res == -1 && errno != EINTR)
		return nat_Error(NAT_E_SYSTEM,"select");

	for(ix=0; ix<eh_MAXINPUTS && res != -1; ++ix)
	{
		if(eh_inputs[ix].in_use && FD_ISSET(eh_inputs[ix].fd, &fdr))
			eh_inputs[ix].cb(eh_inputs[ix].fd, eh_inputs[ix].data);
	}

	for(ix=0; ix<eh_MAXOUTPUTS && res != -1; ++ix)
	{
		if(eh_outputs[ix].in_use && FD_ISSET(eh_outputs[ix].fd, &fdw))
			eh_outputs[ix].cb(eh_outputs[ix].fd, eh_outputs[ix].data);
	}

	ftime(&tmb);
	for(ix = tmc = 0; ix<eh_MAXTIMERS; ++ix)
	{
		if(eh_timers[ix].in_use && eh_StepTimer(eh_timers+ix, &tmb) == 0)
		{
			eh_timers[ix].cb(eh_timers[ix].data);
			eh_Reset(eh_timers+ix, &tmb);
		}
	}

	for(ix=0; ix<eh_MAXSIGNALS; ++ix)
	{
		if(eh_signals[ix].in_use)
		{
			while(eh_signals[ix].hit)
			{
				eh_signals[ix].cb(eh_signals[ix].signo, eh_signals[ix].data);
				--eh_signals[ix].hit;
			}
		}
	}
	
	return 0;
}

void eh_Reset(EHTimerCtl *ptc, struct timeb *ptb)
{
struct timeb tmb;
unsigned long m;
	if(ptb == NULL)
	{
		ftime(&tmb);
		ptb = &tmb;
	}
	m = ptb->millitm + ptc->val_ms;
	ptc->trg_time = ptb->time + m/1000;
	ptc->trg_millitm = m%1000;

/* dbg
printf("resettimer: cur_time=%u cur_millitime=%u  ",
        ptb->time, ptb->millitm);
printf("resettimer: trg_time=%u trg_millitime=%u\n",
        ptc->trg_time, ptc->trg_millitm);
dbg */
	return;
}

/*
 * step timer and return remaining millisecs
 */

unsigned long eh_StepTimer(EHTimerCtl *ptc, struct timeb *ptb)
{
unsigned long ts, tm, cs, cm;
struct timeb tmb;

	if(ptb == NULL)
	{
		ftime(&tmb);
		ptb = &tmb;
	}

	ts = ptc->trg_time;
	tm = ptc->trg_millitm;
	cs = ptb->time;
	cm = ptb->millitm;

	if(cs > ts || (cs == ts && cm >= tm))
		return 0;
/* dbg
printf("steptimer: remaining=%lu\n", 1000 * (ts - cs) + tm - cm);
 dbg */

	return 1000 * (ts - cs) + tm - cm;
}

/*
 *=== Multicating Socket
 */

typedef struct sockaddr SOA;
typedef struct sockaddr_in SOAIN;
typedef struct in_addr INAD;

typedef struct
{
	int s;
	SOAIN sa, ba;
} mc_Socket;

MCSocket *MCSocket_Create(char *mcaddr, char *ifname, int mcport, int opts)
{
struct ip_mreq mreq;
struct ifreq ifr;
u_char loopfl;
mc_Socket *ptr;
char *errmsg;

	ptr = malloc(sizeof(mc_Socket));
	ASSERT(ptr);

/*
 * get datagram socket
 */
	ptr->s = socket(AF_INET, SOCK_DGRAM, 0);
	if(ptr->s < 0)
	{
		nat_Error(NAT_E_SYSTEM, "socket");
		return NULL;
	}
/*
 * set address required for group membership (and for connecting, if any)
 */
	bzero((char *) &ptr->sa, sizeof(ptr->sa));
  ptr->sa.sin_family = AF_INET;
  ptr->sa.sin_addr.s_addr = inet_addr(mcaddr);
  ptr->sa.sin_port = htons(mcport);

/*
 * use one pass loop --  break on error
 */
	errmsg = NULL;
	do
	{
/*
 * select interface (not required for hosts with one network card)
 */
		if(ifname)
		{
			strncpy(ifr.ifr_name, ifname, IFNAMSIZ);
			if (ioctl(ptr->s,SIOCGIFADDR, &ifr) < 0)
			{
				errmsg =  "ioctl CGIFADDR";
				break;
			}
			memcpy(&mreq.imr_interface,&((SOAIN*)&ifr.ifr_addr)->sin_addr,
             sizeof(INAD));
		}
		else
		{
			mreq.imr_interface.s_addr = htonl(INADDR_ANY);
		}
/*
 * option: bind to port (otherwise datagrams won't come up from the IP layer)
 */
		if(opts & MC_BIND)
		{
			bzero((char *) &ptr->ba, sizeof(ptr->ba));
  		ptr->ba.sin_family = AF_INET;
			ptr->ba.sin_addr.s_addr = htonl(INADDR_ANY);
  		ptr->ba.sin_port = htons(mcport);
	
			if(bind(ptr->s,(SOA *) (void *) &ptr->ba, sizeof(ptr->ba)) < 0)
  		{
				errmsg = "bind";
				break;
  		}
		}
/*
 * option: connect to get errors and eliminate kernel connecting overhead
 */
		if(opts & MC_CONNECT)
		{
			if (connect(ptr->s, (SOA *) &ptr->sa, sizeof(ptr->sa)) < 0)
			{
				errmsg = "connect";
				break;
			}
		}
/*
 * set multicast group membership
 */
		memcpy(&(mreq.imr_multiaddr), &ptr->sa.sin_addr, sizeof(INAD));
		if(setsockopt(ptr->s, IPPROTO_IP, IP_ADD_MEMBERSHIP, (const char *) &mreq,
   		sizeof(mreq)) < 0)
		{
			errmsg = "setsockopt IP_MC_MBRSHIP";
			break;
		}
/*
 * set looping back of datagrams to sending host (sys defaults to yes) 
 */
		loopfl = opts & MC_LOOPBACK;
		if(setsockopt(ptr->s,IPPROTO_IP,IP_MULTICAST_LOOP, (const char *) &loopfl,
      	sizeof(loopfl)) < 0)
		{
			errmsg = "setsockopt IP_MC_LOOP";
			break;
		}
/*dbg
		{
		int rc;
		size_t s;
      	s = sizeof(loopfl);
		rc = getsockopt(ptr->s,IPPROTO_IP,IP_MULTICAST_LOOP, (const char *) &loopfl,
				&s);
				printf(">>>> LOOPFL=%d (%d)\n",loopfl, rc);
		}
dbg*/
	} while(0);
	if(errmsg)
	{
			close(ptr->s);
			nat_Error(NAT_E_SYSTEM, errmsg);
			free(ptr);
			return NULL;
	}
	return ptr;
}

int MCSocket_Destroy(MCSocket *pThis)
{
int s;
mc_Socket *ptr;
	ASSERT(pThis);
	ptr = (mc_Socket *) pThis;
	s = ptr->s;
	free(ptr);
	if(close(s) == -1)
		return nat_Error(NAT_E_SYSTEM,NULL);
	return 0;
}

int MCSocket_Write(MCSocket *pThis, char *buff, size_t len)
{
int res;
mc_Socket *ptr;
	ASSERT(pThis);
	ptr = (mc_Socket *) pThis;
	res = sendto(ptr->s, buff, len, 0, (SOA *) &ptr->sa, sizeof(SOA));
	if(res == -1)
		return nat_Error(NAT_E_SYSTEM,NULL);
	return res;
}

int MCSocket_Read(MCSocket *pThis, char *buff, size_t len)
{
	return MCSocket_ReadAI(pThis, buff, len, NULL);
}

int MCSocket_ReadAI(MCSocket *pThis, char *buff, size_t len, char **ai)
{
int res;
mc_Socket *ptr;
SOAIN sain;
size_t s;
static char aib[100], *aip;

	ASSERT(pThis);
	ptr = (mc_Socket *) pThis;
	s = sizeof(sain);
	res = recvfrom(ptr->s, buff, len, 0, (SOA *) &sain, &s);
	if(res == -1)
		return nat_Error(NAT_E_SYSTEM,NULL);
	aip = inet_ntoa(sain.sin_addr);
	ASSERT(aip && strlen(aip) < sizeof(aib));
	strcpy(aib, aip);
	if(ai)
		*ai = aib;
	return res;
}

int MCSocket_GetSock(MCSocket *pThis)
{
mc_Socket *ptr;
	ASSERT(pThis);
	ptr = (mc_Socket *) pThis;
	return ptr->s;
}

int MCSocket_SetIf(MCSocket *pThis, char *ifname)
{
mc_Socket *ptr;
INAD inad;
struct ifreq ifrq;

	ASSERT(pThis);
	ptr = (mc_Socket *) pThis;

	if (ifname != NULL)
	{
		strncpy(ifrq.ifr_name, ifname, IFNAMSIZ);
		if (ioctl(ptr->s, SIOCGIFADDR, &ifrq) < 0)
			return nat_Error(NAT_E_SYSTEM,"IOCTL SIOCGIFADDR");
		memcpy(&inad, &((SOAIN *) &ifrq.ifr_addr)->sin_addr, sizeof(INAD));
	}
	else
	{
		inad.s_addr = htonl(INADDR_ANY);
	}
	if(setsockopt(ptr->s, IPPROTO_IP, IP_MULTICAST_IF, &inad, sizeof(INAD))==-1)
			return nat_Error(NAT_E_SYSTEM,"SETSOCKOPT IP_MULTICAST_IF");
	return 0;
}

/*
 *=== Block FIFO (tbi)
 */

BlockFIFO *BlockFIFO_Create()
{
BlockFIFO *pThis;
	pThis = malloc(sizeof(BlockFIFO));
	ASSERT(pThis);
	pThis->count = 0;
	return pThis;
}

void BlockFIFO_Destroy(BlockFIFO *pThis)
{
void *p;
size_t s;
	if(pThis == NULL)
		return;
	while(BlockFIFO_Count(pThis))
	{
		p = BlockFIFO_Get(pThis, &s);
		ASSERT(p);
		free(p);
	}
	free(pThis);
	return;
}

int BlockFIFO_Put(BlockFIFO *pThis, void *p, size_t s)
{
void *pd;
BlockFIFOEntry *pe;

	ASSERT(pThis);
	if(p == NULL)
		return nat_Error(NAT_E_BADVAL, NULL);
/*
 * alloc data area: maybe large block, may fail.
 * Compute length for assumably (size equals to 0 ) null terminated strings.
 */
	if (s == 0)
		s = strlen(p) + 1;
	
	pd = malloc(s);
	if(pd == NULL)
		return nat_Error(NAT_E_SYSTEM,"malloc");
	memcpy(pd,p,s);
/*
 * alloc entry -- small block, should not fail
 */
	pe = (BlockFIFOEntry *) malloc(sizeof(BlockFIFOEntry));
	ASSERT(pe);
	pe->data = pd;
	pe->size = s;
	pe->link = NULL;
/*
 * count == 0 means the fifo is empty
 */
	if(pThis->count == 0)
	{
		pThis->pget = pe;
	}
	else
	{
		pThis->pput->link = pe;
	}
	pThis->pput = pe;
	++pThis->count;
	return 0;
}

void *BlockFIFO_Get(BlockFIFO *pThis, size_t *ps)
{
BlockFIFOEntry *pe;
void *pd;
	ASSERT(pThis);
	if(pThis->count == 0)
		return NULL;
	pe = pThis->pget;
	ASSERT(pe);
	pThis->pget = pe->link;
	--pThis->count;

	*ps = pe->size;
	pd = pe->data;
	free(pe);
	return pd;
}

unsigned int BlockFIFO_Count(BlockFIFO *pThis)
{
	ASSERT(pThis);
	return pThis->count;
}

/*
 * Queue Bridge
 */

/* macros, consts etc */

#define qb_MAGIC	"1QB99"

#define qb_VERIFY_XTR(d,x,s)	if(d==NULL||x==NULL||*x+s>d->size) return -1;
#define qb_VALIDATE_INS(d,x,s) if(qb_ValidateIns(d,x,s)) return -1;

#define qb_BLKSIZE 	64
#define qb_DATASIZE(s) (sizeof(QBData)-1+(s))

#define qb_INITIAL	1
#define qb_OPEN			2
#define qb_CLOSED		3
#define qb_LOST			4
#define qb_KILLED		5

#define qb_TMREAD		1
#define qb_TMWRITE	2
#define qb_TMQRECV	3
#define qb_TMQSEND	4

static int qb_ValidateIns(char **ptr, int pos, int nbytes);

static EHInputId qb_iid_pipe = -1;
static EHTimerId qb_tid_hb = -1;
static int qb_pe_state = qb_INITIAL;
static QBCommCtl qb_commctl;
static int qb_qid_in, qb_qid_ack;
static unsigned long qb_hbid;

static int qb_InputCB(int, void *);
static int qb_TimerCB(void *);
static int qb_PEOpen(int, QBCallback);
static void qb_PEClose(void);
static int qb_TimedIO(int, void *, size_t, long, int, int);
static int qb_NBIO(int);

#define qb_MAXQBRIDGE	16

typedef struct
{
	int pid;
	int fd;
	int state;
	int qid;
} QBEntry;

static QBEntry qb_qent[qb_MAXQBRIDGE];
static int qb_qcount = 0;
static struct sigaction qb_qcldsa, qb_qpipesa;
static int qb_qinit;
static void qb_SigCld(int);
static void qb_Kill(QBEntry *pe, int);
static void qb_Close(QBEntry *pe);

/*=== Queue Bridge Pipe End API === */

int QBSetup(int pfd, QBCallback cb)
{
/*
 * pipe fd -1 means reaset (close)
 */
	if(pfd < 0)
	{
		if(qb_pe_state == qb_OPEN)
		{
			qb_PEClose();
			return 0;
		}
		else 
		{
			return nat_Error(NAT_E_BADVAL,NULL);
		}
	}
	else if(qb_pe_state == qb_INITIAL)
	{
		return qb_PEOpen(pfd, cb);
	}
	return nat_Error(NAT_E_BADSTAT,NULL);
}

/*
 * set the message to the queue end
 */

int QBSendMessage(short ty, char *data, size_t len, long to)
{
size_t sz;
ITEM *pi, ack;
EVENT *pe;
BC_HEAD *pbc;
CMDBLK *pcm;
unsigned long la[2];
int rc;

/*
 * check state
 */
	if(qb_pe_state != qb_OPEN)
		return nat_Error(NAT_E_BADSTAT,NULL);
/*
 * now only "small" items
 */
	if(len > I_SIZE)
		return nat_Error(NAT_E_TOOBIG,NULL);
/*
 * build the item struct
 */

	sz = sizeof(ITEM) + sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + len;
	pi = malloc(sz);
	if(pi == NULL)
		return nat_Error(NAT_E_SYSTEM, NULL);
	pe = (EVENT *) pi->text;
	pbc = (BC_HEAD *) ((char *) pe + sizeof(EVENT));
	pcm = (CMDBLK *)((char *) pbc->data);
/*
 * user data in CMDBLK
 */
	memset(pcm, 0, sizeof(CMDBLK));
	strcpy(pcm->obj_name, qb_MAGIC);
	la[0] = htonl(sz);
	la[1] = htonl(ty);
	memcpy(pcm->tw_start, la, sizeof(la));
	memcpy(pcm->data, data, len);
/*
 * broadcast header 
 */
	memcpy(pbc->dest_name, qb_commctl.dest_name, sizeof(pbc->dest_name));
	memcpy(pbc->recv_name, qb_commctl.recv_name, sizeof(pbc->recv_name));
	pbc->rc = RC_SUCCESS;
	pbc->data_size = len + sizeof(CMDBLK); /* (not used by ceda) */
/*
 * event header 
 */
	pe->type = SYS_EVENT;
	pe->command = EVENT_DATA;
	pe->originator = qb_commctl.sndmod_id;
	pe->retry_count = 0;
	pe->data_offset = sizeof(EVENT);
	pe->data_length = pbc->data_size + sizeof(BC_HEAD);
/*
 * item header 
 */
	pi->msg_header.mtype = 3;	/* prio is tbi */
	pi->function = QUE_PUT;
	pi->route = qb_commctl.rcvmod_id;
	pi->priority = 3; /* prio is tbi */
	pi->msg_length = pe->data_length + sizeof(EVENT);
	pi->originator = qb_commctl.sndmod_id;
	pi->block_nr = 0;
/*
 * send the item
 */
	sz -= sizeof(long); /* see man msgsnd */
	rc = qb_TimedIO(qb_qid_in, pi, sz, to, qb_TMQSEND, 0);
	free(pi);
	if(rc <= 0)
		return rc == 0 ? nat_Error(NAT_E_TIMEOUT,"QUEUE SEND") : rc;
/*
 * get ack (max to 1 sec)
 */
	sz = sizeof(ITEM) - sizeof(long); 
	rc = qb_TimedIO(qb_qid_ack, &ack, sz, 1000, qb_TMQRECV, qb_commctl.sndmod_id);
	if(rc <= 0)
		return rc == 0 ? nat_Error(NAT_E_TIMEOUT,"QUEUE RECV") : rc;
	
	return rc;
}

/*=== Queue Bridge Queue End API === */

QBHandle QBOpen(char *pn, char **av, QBCommCtl *ctl, int sigp)
{
int fds[2], hd, ix, na, rc;
char **av2;
char buff[20];
struct sigaction sa;
	if(qb_qinit == 0)
	{
		for(ix=0; ix<qb_MAXQBRIDGE; ++ix)
			qb_qent[ix].state = qb_CLOSED;
		qb_qinit = 1;
	}
/*
 * tmp restrictions: only one open queue bridge and signal policy QB_SIGNONE
 * Note: the algorithms partially support more bridges and other sig policies
 */
	if(qb_qcount > 0)
		return nat_Error(NAT_E_BADVAL, NULL);
	if(sigp != QB_SIGNONE)
		return nat_Error(NAT_E_BADVAL, NULL);
	if(av == NULL || av[0] == NULL) /* at least argv[0] must be present */
		return nat_Error(NAT_E_BADVAL, NULL);
/*
 * get entry in queue bridge table and set signal handler if it's the
 * first child to run
 */
	if(qb_qcount == 0)
	{
		hd = 0;
		sa.sa_handler = qb_SigCld;
		sigemptyset(&sa.sa_mask); 
		sa.sa_flags = 0;
		if(sigaction(SIGCLD, &sa, &qb_qcldsa) == -1)
			return nat_Error(NAT_E_SYSTEM, "sigaction cld");
		sa.sa_handler = SIG_IGN;
		sigemptyset(&sa.sa_mask); 
		sa.sa_flags = 0;
		if(sigaction(SIGPIPE, &sa, &qb_qpipesa) == -1)
			return nat_Error(NAT_E_SYSTEM, "sigaction pipe");
	}
	else
	{
		for(ix=0; ix<qb_MAXQBRIDGE; ++ix)
			if(qb_qent[ix].state == qb_CLOSED)
				break;
		if(ix == qb_MAXQBRIDGE)
			return nat_Error(NAT_E_NOSPACE, NULL);
		hd = ix;
	}

	if(pipe(fds) != 0)
		return nat_Error(NAT_E_SYSTEM, "pipe");
/*
 * save pid (child copy will be discarded on execv)
 */
	++qb_qcount;
	qb_qent[hd].state = qb_OPEN;
	qb_qent[hd].fd = fds[1];
	qb_qent[hd].pid = fork();
	qb_qent[hd].qid = ctl->sndmod_id;

	switch(qb_qent[hd].pid)
	{
		case -1:	/* fork failed */
			qb_Close(qb_qent + hd);
			return nat_Error(NAT_E_SYSTEM, "fork");
			/* break; 	 not reached */
		case 0:		/* child proc running */
			close(fds[1]); 	/* don't need pipe write end */
			for(na=1; av[na]; ++na)	/* insert read pipe fd into  arg list */
				;
			av2 = malloc( (na+2) * sizeof(char *)); /* add pipe and NULL */
			ASSERT(av2);
			sprintf(buff, "%d", fds[0]);
			for(ix=0; ix<=na; ++ix)
				av2[ix] = ix == 1 ? buff : ix == 0 ? av[0] : av[ix-1];
			av2[ix] = NULL;
			execv(pn, av2);
			nat_Error(NAT_E_SYSTEM, "execv");
			exit(1);
		default: /* parent running */
			close(fds[0]);	/* don't need pipe read end */
			if(qb_NBIO(fds[1]) == -1) /* non-blocking mode */
			{
				qb_Kill(qb_qent+hd, 0);
				return -1;
			}
			rc = qb_TimedIO(fds[1], ctl, sizeof(*ctl), 5000, qb_TMWRITE, 0);
			if(rc <= 0)
			{
				qb_Kill(qb_qent+hd, 0); 
				return rc == 0 ? nat_Error(NAT_E_TIMEOUT, "WRITE COMMCTL") : rc;
			}
	}
	return 0;
}

QBHandle QBOpenCL(char *cl, QBCommCtl *ctl, int sigp)
{
char **va, **sl;
int na, ix;
QBHandle res;

	na = TokenZ_GetStrings(&cl," \t", &sl);
	va = malloc( (na+1) * sizeof(char *));
	ASSERT(va);
	for(ix=0; ix<na; ++ix)
		va[ix] = sl[ix];
	va[ix] = NULL;
	free(sl);

	res = QBOpen(va[0], va, ctl, sigp);
	
	for(ix=0; ix<na; ++ix)
		free(va[ix]);

	return res;
} 

int QBWrite(QBHandle hd, short ty, char *data, size_t sz, long to)
{
size_t szb, s;
char *pb;
int rc; 
QBEntry *pe;

	if(hd < 0 || hd >= qb_MAXQBRIDGE || qb_qent[hd].state != qb_OPEN)
		return nat_Error(NAT_E_BADVAL, NULL);

	pe = qb_qent + hd;

	szb = sizeof(size_t) + sizeof(short) + sz;
	pb = malloc(szb);
	if(pb == NULL)
		return nat_Error(NAT_E_SYSTEM, NULL);

	memcpy(pb, &sz, sizeof(sz) );
	s  = sizeof(sz);
	memcpy(pb + s,  &ty, sizeof(ty) );
	s += sizeof(ty);
	memcpy(pb + s,  data, sz );

	for(s = szb; s > 0; s -= rc)
	{
		rc = qb_TimedIO(pe->fd, pb + szb - s, s, to, qb_TMWRITE, 0);
		if(rc <= 0)
			break;
	}

	if (rc < 0 && NatErrNo() == EPIPE)
		qb_Kill(pe, 0); 

	free(pb);
	return rc;
}
 
QBHandle QBSelect(void *p)
{
ITEM *pi;
CMDBLK *pcm;
int ix;

	pi = (ITEM *) p;
	pcm = (CMDBLK *) ((BC_HEAD *) ( pi->text + sizeof(EVENT)))->data;
	if(strcmp(pcm->obj_name, qb_MAGIC))
		return -1;

	for(ix = 0; ix < qb_MAXQBRIDGE; ++ix)
		if(qb_qent[ix].state == qb_OPEN && qb_qent[ix].qid == pi->originator)
			break;

	return ix; /* qe_MAXQBRIDGE is legal */
}

QBData *QBGetMessage(QBHandle hd, void *p)
{
CMDBLK *pcm;
size_t sz;
short t;
unsigned long l;

	if(hd < 0 || hd >= qb_MAXQBRIDGE)
		return NULL;
	pcm = (CMDBLK *) ((BC_HEAD *) ( ((ITEM*)p)->text + sizeof(EVENT)))->data;
	if(strcmp(pcm->obj_name, qb_MAGIC))
		return 0;
	memcpy(&l, pcm->tw_start, sizeof(l));
	sz = ntohl(l);
	memcpy(&l, pcm->tw_start + sizeof(l), sizeof(l));
	t = (short) ntohl(l);
	return QBMakeData(t, pcm->data, sz);
}

/*
 * QBClose: terminate child proc by sending SIGTERM to it and wait for
 * it to exit (wait() called by SIGCLD handling). 
 *
 * return value:    0 if the child process terminated and the queue bridge
 *                  closed.
 *
 *                  -1 if an error has occured. The error number (returned
 *                  by NatErrNo()) is NAT_E_BADVAL if the handle parameter
 *                  is invalid or the bridge is closed and NAT_E_BADSTAT
 *                  if the child process apperently won't die.
 */

int QBClose(QBHandle hd)
{
unsigned int s;
int try_hard;
	if(hd < 0 || hd >= qb_MAXQBRIDGE)
		return nat_Error(NAT_E_BADVAL, NULL);
/*
 * child may have exited already
 */
	if(qb_qent[hd].state == qb_CLOSED)
		return 0;
/*
 * First, a TERM signal is sent to the child process. If it won't die
 * (although it should catch the TERM signal and terminate) then a KILL
 * signal is sent.
 * In both runs sleep() is performed to give the child some time to terminate.
 * The sleep is interrupted anyway when the SIGCLD comes (which also causes
 * the handle to be freed).
 *
 * Note that sleep() can be interrupted by another signal or also there
 * can be other situations where the child process still exists after
 * the sleep() call returns
 */
 	for(try_hard = 0; try_hard < 2 && qb_qent[hd].state != qb_CLOSED; ++try_hard)
	{
		LOG(DBGLIB,"QBClose: %s kill %d", try_hard?"HARD":"soft",qb_qent[hd].pid);
		qb_Kill(qb_qent+hd, try_hard);
		for(s = 3; qb_qent[hd].state != qb_CLOSED && s > 0; s = sleep(s))
			;
	}
	if(qb_qent[hd].state != qb_CLOSED)
		return nat_Error(NAT_E_BADSTAT, "QBClose");
	return 0;
}

/* queue bridge private functions */

void qb_SigCld(int n)
{
int ix, pid, s;
/*
 * lookup for child(ren) that terminated. Note that CLD sigs are not
 * necessarily queued so we must check all PIDs
 */
/*	LOG(DBGLIB,">>> SIGCLD"); */
	for(ix=0; ix<qb_MAXQBRIDGE; ++ix)
	{
		pid = qb_qent[ix].pid;
		if(qb_qent[ix].state != qb_CLOSED && waitpid(pid, &s, WNOHANG) == pid)
		{
/*
 * found child
 */
			LOG(DBGLIB,"caught CLD for %d (count=%d)",qb_qent[ix].pid, qb_qcount);
			qb_Close(qb_qent + ix);
		}
	}

#if 0 /* this must be implemented yet */
/*
 * if not a queue bridge child -> call old handler if any
 */
	if(qb_qcldsa.sa_handler != SIG_IGN && qb_qcldsa.sa_handler != SIG_DFL)
		qb_qcldsa.sa_handler(n);
#endif /* tbi */
	return;
}

void qb_Close(QBEntry *pe)
{
	ASSERT(qb_qcount);
	pe->state = qb_CLOSED;
	close(pe->fd);
	--qb_qcount;
	if(qb_qcount == 0) /* reset sighd if there are no other children */
	{
		sigaction(SIGCLD, NULL, &qb_qcldsa);
		sigaction(SIGPIPE, NULL, &qb_qpipesa);
	}
}

void qb_Kill(QBEntry *pe, int try_hard)
{
	ASSERT(pe->pid > 0);
	kill(pe->pid, try_hard ? SIGKILL : SIGTERM);
}

int qb_PEOpen(int fd, QBCallback cb)
{
int rc;

/*
 * set nonblocking IO for pipe
 */
	if(qb_NBIO(fd))
		return -1;
/*
 * read ctl struct with a timeout of 1000 msec
 */
	rc = qb_TimedIO(fd, &qb_commctl, sizeof(qb_commctl), 5000, qb_TMREAD, 0);

	if(rc <= 0)  
		return rc == 0 ? nat_Error(NAT_E_TIMEOUT, "READ COMMCTL") : rc;

	if(rc < sizeof(qb_commctl))
		return nat_Error(NAT_E_BADVAL, NULL);

	if((qb_qid_in = msgget(qb_commctl.inque_key, 0)) == -1)
		return nat_Error(NAT_E_SYSTEM,NULL);

	if((qb_qid_ack = msgget(qb_commctl.ackque_key, 0)) == -1)
		return nat_Error(NAT_E_SYSTEM,NULL);

	qb_iid_pipe = EHAddInput(fd, qb_InputCB, (void *)cb);
	ASSERT(qb_iid_pipe != -1);
	if(qb_commctl.heartbeat > 0)
	{
		qb_tid_hb = EHAddTimer((unsigned long) qb_commctl.heartbeat, qb_TimerCB, 0);
		ASSERT(qb_tid_hb != -1);
	}
	qb_pe_state = qb_OPEN;
	return 0;
}

void qb_PEClose()
{
	if(qb_iid_pipe != -1)
		EHRemoveInput(qb_iid_pipe);
	if(qb_tid_hb != -1)
		EHRemoveTimer(qb_tid_hb);
	qb_pe_state = qb_CLOSED;
}

int qb_InputCB(int fd, void *p)
{
size_t len, n;
short ty;
void *pr;
int ix, rc;
QBData *pd;

	for(ix=0; ix<3; ++ix)
	{
		if(ix == 0)
		{
			n = sizeof(len);
			pr = &len;
		}
		else if(ix == 1)
		{
			n = sizeof(ty);
			pr = &ty;
		}
		else 
		{
			n = len;
			pr = malloc(len);
			if(pr == NULL)
			{
				nat_Error(NAT_E_SYSTEM, NULL);
				(* (QBCallback) p) (NULL);
				return -1;
			}
		}
		rc = qb_TimedIO(fd, pr, n, 1000, qb_TMREAD, 0);
		if(rc < n)
			break;
	}

	if(ix < 3)
	{
		if(rc == 0)
 			nat_Error(NAT_E_TIMEOUT,"PIPE READ");
		else if(rc > 0)
 			nat_Error(NAT_E_DATA,"PIPE READ");
		(* (QBCallback) p) (NULL);
		return -1;
	}

	pd = QBMakeData(ty, pr, len);
	free(pr);
	if(pd == NULL)
	{
		nat_Error(NAT_E_SYSTEM, NULL);
		(* (QBCallback) p) (NULL);
		return(-1);
	}

/*dbg printf("=== calling cb\n"); dbg*/
	(* (QBCallback) p) (pd);
/*dbg printf("=== cb done \n"); dbg*/
	return 0;
}

/*
 * heartbeat timer
 */

int qb_TimerCB(void *p)
{
char *pb;
int ix;
	ix = 0;
	ASSERT(QB_PUT_ULong(qb_hbid, &pb, &ix) == 0);
	++qb_hbid;
	QBSendMessage(0, pb, ix, 5000);
	free(pb); /* 010928 jhe */
	return 0;
}

/*
 * set non blocking mode for pipe
 */

int qb_NBIO(int fd)
{
int arg;
	arg = fcntl(fd, F_GETFL, O_NONBLOCK);
	if(arg == -1)
		return nat_Error(NAT_E_SYSTEM, "fnctl-get");
	if(fcntl(fd, F_SETFL, arg|O_NONBLOCK) == -1)
		return nat_Error(NAT_E_SYSTEM, "fnctl-set");
	return 0;
}

/*
 * read/write with timeout: requires non blocking fd.
 * should not be used with long time intervals since it polls.
 * return:  0 timeout 
 *         -1 error other than EINTR or EAGAIN (nat_Error set)
 *         > 0 number of transferred bytes
 */

int qb_TimedIO(int id, void *buff, size_t len, long to, int op, int arg)
{
EHTimerCtl tmr;
int rc, nodata;
	tmr.val_ms = to > 0 ? to : 0;

	LOG(DBGLIB,"TimedIO: id=%d len=%d to=%d op=%d arg=%d", id, len, to, op, arg);
	eh_Reset(&tmr, NULL);
	nodata = EAGAIN;
	do
	{
		switch(op)
		{
			case qb_TMREAD:
				rc = read(id, buff, len);
				break;
			case qb_TMWRITE:
				rc = write(id, buff, len);
				break;
			case qb_TMQRECV:
				nodata = ENOMSG;
				rc = msgrcv(id, buff, len, arg, IPC_NOWAIT|MSG_NOERROR); /* ceda bug */
				break;
			case qb_TMQSEND:
				rc = msgsnd(id, buff, len, IPC_NOWAIT);
				if(rc == 0)
					rc = (int) len;
				break;
			default:
				ASSERT(0);
		}
		if(rc == -1 && errno == EINTR)
			continue;
	} while(rc == -1 && errno == nodata && eh_StepTimer(&tmr, NULL) > 0);
	
	if(rc == -1)
	{
		if (errno == EAGAIN)
			return 0;
		else
			return nat_Error(NAT_E_SYSTEM, "TimedIO");
	}

	return rc;
}

/*=== Data Representation API === */

QBData *QBMakeData(short t, char *buff, size_t sz)
{
QBData *res;
	res = (QBData *) malloc(qb_DATASIZE(sz));
	if (res)
	{
		res->size = sz;
		res->type = t;
		memcpy(res->data,buff,sz);
	}
	return res;
}

int QB_GET_Short(QBShort *vptr, QBData *dptr, int *xptr)
{
unsigned short usn, ush;
	qb_VERIFY_XTR(dptr, xptr, 2);
	memcpy(&usn, dptr->data + *xptr, 2);
	ush = ntohs(usn);
	*vptr = * (short *) &ush;
	*xptr += 2;
	return 0;
}

int QB_GET_UShort(QBUShort *vptr, QBData *dptr, int *xptr)
{
unsigned short usn;
	qb_VERIFY_XTR(dptr, xptr, 2);
	memcpy(&usn, dptr->data + *xptr, 2);
	*vptr = ntohs(usn);
	*xptr += 2;
	return 0;
}

int QB_GET_Long(QBLong *vptr, QBData *dptr, int *xptr)
{
unsigned long uln, ulh;
	qb_VERIFY_XTR(dptr, xptr, 4);
	memcpy(&uln, dptr->data + *xptr, 4);
	ulh = ntohl(uln);
	*vptr = *(long *) &ulh;
	*xptr += 4;
	return 0;
}

int QB_GET_ULong(QBULong *vptr, QBData *dptr, int *xptr)
{
unsigned uln;
	qb_VERIFY_XTR(dptr, xptr, 4);
	memcpy(&uln, dptr->data + *xptr, 4);
	*vptr = ntohl(uln);
	*xptr += 4;
	return 0;
}

int QB_GET_StringZ(QBStringZ *vptr, QBData *dptr, int *xptr)
{
int len, ix;
	qb_VERIFY_XTR(dptr, xptr, 1); /* at least \0 expected */

	*vptr = NULL;
	for(len = -1, ix = *xptr; ix < dptr->size; ++ix)
	{
		if(dptr->data[ix] == '\0')
		{
			len = ix - *xptr;
			break;
		}
	}

	if(len == -1)
		return -1;

	*vptr = dptr->data + *xptr;
	*xptr += (len + 1);

	return 0;
}

int QB_PUT_Short(QBShort val, char **pptr, int *xptr)
{
	return QB_PUT_UShort(* (QBUShort *) &val, pptr, xptr);
}

int QB_PUT_UShort(QBUShort val, char **pptr, int *xptr)
{
unsigned short usn;
	qb_VALIDATE_INS(pptr, *xptr, 2);
	usn = htons(val);
	memcpy(*pptr + *xptr, &usn, 2);
	*xptr += 2;
	return 0;
}

int QB_PUT_Long(QBLong val, char **pptr, int *xptr)
{
	return QB_PUT_ULong(*(QBULong *)&val, pptr, xptr);
}

int QB_PUT_ULong(QBULong val, char **pptr, int *xptr)
{
unsigned long uln;
	qb_VALIDATE_INS(pptr, *xptr, 4);
	uln = htonl(val);
	memcpy(*pptr + *xptr, &uln, 4);
	*xptr += 4;
	return 0;
}

int QB_PUT_StringZ(QBStringZ val, char **pptr, int *xptr)
{
int len;
	if(val == NULL)
		return -1;
	len = strlen(val) + 1;
	qb_VALIDATE_INS(pptr, *xptr, len);
	memcpy( *pptr + *xptr, val, len);	/* incl \0 */
	*xptr += len;
	return 0;
}

/*=== qb private part ===*/

int qb_ValidateIns(char **ptr, int pos, int nbytes)
{
int blk1, blk2;

	ASSERT(nbytes > 0);

	blk1 = pos / qb_BLKSIZE + 1;
	blk2 = (pos + nbytes) / qb_BLKSIZE +1;

	if (pos == 0 || blk2 > blk1)
	{
			if(pos == 0)
				*ptr = malloc(blk2 * qb_BLKSIZE);
			else
				*ptr = realloc(*ptr, blk2 * qb_BLKSIZE);
		if(*ptr == NULL)
			return -1;
	}
	return 0;
}

