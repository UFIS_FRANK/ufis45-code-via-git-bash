#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Lis/LIS_Server/Base/Server/Library/Cedalib/twrdat.c 1.3 2004/10/07 21:42:06SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/
/*                                                                           */
/* ABB AAT/I twrdat.c                                                        */
/* UFIS/TWR-AOS                                                              */ 
/*                                                                           */
/* Author         : Janos Heilig                                             */
/* Creation Date  : Aug 10    2001                                           */
/* Description    : tower data data manipulation functions (see twrdatX.doc) */
/*                                                                           */
/* Update history : JHE Sep 14 2001 START_UP removed, ETD replaced by EOBT   */
/*                  JHE Sep 18 2001 chgd attr matrix to incl ETA for DIV_ETA */
/*                  JHE Sep 18 2001 added default sequence number to initing */
/*                  JHE Sep 23 2001 added CheckMsg() and availability table  */
/*                                  manipulation twrd_AlterTable()           */
/*                  JHE Sep 26 2001 changes to avoid gcc warnings            */
/*                  JHE Sep 27 2001 Q&D case insensitive strcmp (bad twr dat)*/ 
/*                  JHE Sep 27 2001 TWRDErrorCount()                         */
/*****************************************************************************/
                                                                                
/*
 * $Log: Ufis/Lis/LIS_Server/Base/Server/Library/Cedalib/twrdat.c  $
 * Revision 1.3 2004/10/07 21:42:06SGT jwe 
 * added seconds to ceda-timestamp
 * Revision 1.2 2004/04/02 14:12:33CEST jwe 
 * added mks-version string to sources/headers
 * Revision 1.3 2002/04/29 10:46:38CEST jwe 
 * history move from cedalib to cedalib/src due to change in SCM-tree structure:
 * -changed CTOT field not be mandatory 
 * -bugfixed the two time & date conversion function UttT() and UdtT();
 * Revision 1.2 2002/04/29 10:41:47GMT+02:00 jwe 
 * removed two "" from a resolving table for the reson messages because whenever this table was used and a 
 * not defined reason was given by the tower this lead into a core dump of twrcom.
 * Revision 1.1 2002/04/29 09:03:28GMT+02:00 jwe 
 * Initial revision
 * Revision 1.3 2002/04/26 13:44:33GMT+02:00 jwe 
 * -changed CTOT field not be mandatory 
 * Revision 1.2 2001/11/30 15:28:34GMT+01:00 jwe 
 * removed two "" from a resolving table for the reson messages because whenever this table was used and a 
 * Revision 1.1  2001/09/04 22:16:09  project
 * Initial revision
 *
 * -----
 * $Id: Ufis/Lis/LIS_Server/Base/Server/Library/Cedalib/twrdat.c 1.3 2004/10/07 21:42:06SGT jwe Exp  $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

#include "twrdat.h"
#include "nat.h"

/*
 * An automatic procedure is defined to handle mandatory and optional fields.
 * The message type table from the documentation is used here to define 
 * the table containing availability information for each field and message
 * type  combination.
 */

#define TWRD_NMTYPES	(AOSARR-ETA+1)

#define TWRD_ISFSET(p,f) ((p)->flags&(TWRD_B_##f))
#define TWRD_ISFSET_(p,f) ((p)->flags&f)
#define TWRD_SETF_(p,f) ((p)->flags |= f)
#define TWRD_UNSETF_(p,f) ((p)->flags &= ~f)


#define TWRD_NAME_FIELD(f)	TWRD_B_##f, #f

typedef struct
{
	unsigned long fid;
	char *fname, *key;
	char avail[TWRD_NMTYPES]; /* 0=never 1=always 2=opt */
} twrd_FieldAvailRec;

/*
 * Because of possible trigraph sequence problems (??-form) we are using
 * the 'o' character for '?' (optional field)
 */

#define TWRD_AVLNONE	-1
#define TWRD_AVLOPT		0
#define TWRD_AVLMAND	1

static twrd_FieldAvailRec twrd_FieldAvailTab[] =
{
	{ TWRD_NAME_FIELD(msg_seq),	"",						"!!!!!!!!!!!" },
	{ TWRD_NAME_FIELD(mType),		"",						"!!!!!!!!!!!" },
	{ TWRD_NAME_FIELD(datim),		"",						"!!!!!!!!!!!" },
	{ TWRD_NAME_FIELD(fKey), 		"KEY",				"!!!!!!!!!!!" },
	{ TWRD_NAME_FIELD(cs),			"CS",					"!!!!!!!!!--" },
	{ TWRD_NAME_FIELD(dof),			"DOF",				"!!!!!!!!!--" },
	{ TWRD_NAME_FIELD(adep),		"ADEP",				"!!!!!!!!!--" },
	{ TWRD_NAME_FIELD(ades),		"ADES",				"!!!!!!!!!--" },
	{ TWRD_NAME_FIELD(airc),		"AIRCRAFT",		"ooooooooo--" },
	{ TWRD_NAME_FIELD(wtc),			"WTC",				"ooooooooo--" },
	{ TWRD_NAME_FIELD(rwy),			"RWY",				"ooooooooo--" },
	{ TWRD_NAME_FIELD(flag),		"FLAG",				"ooooooooo--" },
	{ TWRD_NAME_FIELD(stand),		"STAND" ,			"ooooooooo!!" },
	{ TWRD_NAME_FIELD(regMark),	"REGMARK",		"ooooooooooo" },
	{ TWRD_NAME_FIELD(aobd),		"AOBD",				"ooooooooooo" },
	{ TWRD_NAME_FIELD(aobt),		"AOBT",				"ooooooooooo" },
	{ TWRD_NAME_FIELD(firstC),	"FIRSTC",			"ooooooooo--" },
	{ TWRD_NAME_FIELD(lastC),		"LASTC", 			"ooooooooo--" },
	{ TWRD_NAME_FIELD(eta),			"ETA",				"!-o-!------" },
	{ TWRD_NAME_FIELD(ata),			"ATA",				"-!---------" },
	{ TWRD_NAME_FIELD(new_ades),"NEW_ADES",		"---!-------" },
	{ TWRD_NAME_FIELD(divTime),	"DIV_TIME",		"---!!------" },
	{ TWRD_NAME_FIELD(reason),	"REASON",		 	"---oo------" },
	{ TWRD_NAME_FIELD(prv_ades),"PREV_ADES", 	"----!------" },
	{ TWRD_NAME_FIELD(eobt),		"EOBT",				"-----!-----" },
	{ TWRD_NAME_FIELD(ctot),		"CTOT",  			"-----oo----" },
	{ TWRD_NAME_FIELD(slotMsg),	"SLOTMSG",  	"-----ooo---" },
	{ TWRD_NAME_FIELD(atd),			"ATD",				"------!----" },
	{ TWRD_NAME_FIELD(tgCnt),		"TG_COUNT",		"--------!--" },
	{ TWRD_NAME_FIELD(tgTime),	"TG_TIME",		"--------o--" },
	{ TWRD_NAME_FIELD(end),     "",					""            }
};
/*
 * Lookup table for msg types and names
 */

typedef struct
{
	char *name;
	TWRD_MTYPE type;
} twrd_NamedType;

static twrd_NamedType twrd_TypeTable[TWRD_NMTYPES+1] = 
{
	{ "ETA",       ETA },
	{ "ARR",       ARR },
	{ "GO_AROUND", GO_AROUND },
	{ "DIV",       DIV },
	{ "DIV_ETA",   DIV_ETA },
	{ "EOBT",       EOBT },
	{ "DEP",       DEP },
	{ "CANCEL",    CANCEL },
	{ "TG",        TG },
	{ "AOSDEP",    AOSDEP },
	{ "AOSARR",    AOSARR  },
	{ NULL,        M_NONE }
};

static char *twrd_MonthTable[12] =
{
 "JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"
};

typedef struct
{
	char *name;
	TWRD_REASON reason;
} twrd_NamedReason;

static twrd_NamedReason twrd_ReasonTable[] =
{
 	{ "FUEL", FUEL },
	{ "WIND", WIND },
	{ "VISIBILITY", VISIBILITY },
	{ "TECHNICAL", TECHNICAL },
	{ "MEDICAL", MEDICAL },
	{ "OTHER", OTHER },
	/*{ "NULL", R_NONE }*/
	{ NULL, R_NONE } /* by JWE 29.11.2001 */
};


static unsigned long twrd_magic = 0x25746425; /* %td% */

static void twrd_StrNZCpy(void *, void *, size_t);
static int twrd_GetAvailability(TWRD_MESSAGE *msg, unsigned long fid);
static TWRD_MTYPE twrd_TypeByName(char *n);
static char *twrd_NameByType(TWRD_MTYPE t);
static int twrd_ScanDatim(TWRD_MSGITEM its[], TWRD_DATIM *p);
static int twrd_ScanDate(TWRD_MSGITEM its[], TWRD_DATE *p);
static int twrd_ScanCTime(TWRD_MSGITEM its[], TWRD_CTIME *p);
static int twrd_ScanFDate(TWRD_MSGITEM its[], TWRD_FDATE *p);
static int twrd_ScanTime(TWRD_MSGITEM its[], TWRD_TIME *p);
static unsigned long twrd_FIdByKey(char *);

#if 0 /* need it? */
static char *twrd_NameByFId(unsigned long);
#endif

static TWRD_REASON twrd_ReasonByName(char *n);
static char *twrd_NameByReason(TWRD_REASON r);

static char *twrd_MessageToStr(TWRD_MESSAGE *, int);
static char *twrd_DatimToStr(TWRD_DATIM *p);
static char *twrd_TimeToStr(TWRD_TIME *p);
static char *twrd_CTimeToStr(TWRD_CTIME *p);
static char *twrd_DateToStr(TWRD_DATE *p);
static char *twrd_FDateToStr(TWRD_FDATE *p);

static void twrd_AlterTable(TWRD_MESSAGE *msg, unsigned long fid, int v);

/*
 *===== API
 */

int TWRDInitialize(TWRD_MESSAGE *msg, TWRD_MTYPE t)
{
time_t tm;
struct tm *pt;
TWRD_DATIM dt;
	switch(t)
	{
		case ETA:
		case ARR:
		case GO_AROUND:
		case DIV:
		case DIV_ETA:
		case EOBT:
		case DEP:
		case CANCEL:
		case TG:
		case AOSDEP:
		case AOSARR:
			break;
		default:
			return -1;
	}
/*
 * set basic values
 */
	msg->mType = t;
	msg->magic = twrd_magic;
	msg->flags = TWRD_B_mType;
	msg->errcnt = 0;
	msg->tgCnt = 0;
/*
 * set datim field (convenience: the user may also set it)
 */
	time(&tm);
	pt = localtime(&tm);
	dt.dat.year = pt->tm_year + 1900;
	dt.dat.month = pt->tm_mon + 1;
	dt.dat.day = pt->tm_mday;

	dt.tim.hour = pt->tm_hour;
	dt.tim.min = pt->tm_min;

	ASSERT(TWRDSetField(msg, TWRD_B_datim, (unsigned long) &dt)==1);
/*
 * add default seq no for apps not interested in it to be able
 * to encode the message
 */
	ASSERT(TWRDSetField(msg, TWRD_B_msg_seq, 0) == 1);
	return 0;
}

/*
 * Create a message from a \0 terminated string.
 * It uses the fixed sized TWRD_MSGITEM type for white spaced separated
 * message elements.
 */

TWRD_MESSAGE *TWRDDecode(char *str)
{
int ix, iv, err;
unsigned short hseq;
unsigned long seq, fid, ul, setv;
TWRD_MTYPE mt;
TWRD_MSGITEM items[10];
TWRD_MESSAGE *msg;
TWRD_DATIM datim;
TWRD_FDATE fdate;
TWRD_TIME tm, tma[TWRD_MAXTGTIME];
TWRD_REASON reason;

/*
 * sequence, type and datim are required at any case
 */

	for(ix = 0; ix < 3; ++ix)
	{
		if(ix == 0)
		{
			if(TWRDGetMsgItems(&str,items,1)<1 || sscanf(items[0],"0x%hx",&hseq)!=1)
				break;
			seq = (unsigned long) hseq;
/*dbg printf("msg_seq = 0x%04lX\n", seq); dbg*/
		}
		else if (ix == 1)
		{
			if(TWRDGetMsgItems(&str, items, 1) < 1 || 
			   (mt=twrd_TypeByName(items[0])) == M_NONE)
				break;
/*dbg printf("mType = %s\n", twrd_NameByType(mt)); dbg*/
		}
		else /*  ix == 2 */
		{
			if(TWRDGetMsgItems(&str, items, 4) < 4 || twrd_ScanDatim(items, &datim))
				break;
/*dbg printf("datim = %s %s %s %s\n",items[0],items[1],items[2],items[3]); dbg*/
		}
	}

	if(ix != 3)
		return NULL;

/*
 * create message with type and set sequence number and datim
 */

	msg = malloc(sizeof(TWRD_MESSAGE));
	ASSERT(msg);
	TWRDInitialize(msg, mt);
	TWRD_SET(msg, msg_seq, seq);
	TWRD_SET(msg, datim, &datim);

/*
 * get the key,value pairs
 */

	while(TWRDGetMsgItems(&str,items,1) == 1)
	{
		if(items[0][0] != '/')
		{
			++msg->errcnt;
			break;
		}
		fid = twrd_FIdByKey(items[0]+1);
		err = 0;
		switch(fid)
		{
			case TWRD_B_fKey:
				if(TWRDGetMsgItems(&str,items,1)!=1 || sscanf(items[0],"%lx",&ul)!=1)
					err = 1;
				else
					setv = ul;
				break;
			case TWRD_B_cs:
			case TWRD_B_adep:
			case TWRD_B_ades:
			case TWRD_B_airc:
			case TWRD_B_stand:
			case TWRD_B_regMark:
			case TWRD_B_new_ades:
			case TWRD_B_prv_ades:
			case TWRD_B_slotMsg:
				if(TWRDGetMsgItems(&str,items,1)!=1)
					err = 1;
				else
					setv = (unsigned long) items[0];
				break;
			case TWRD_B_dof:
			case TWRD_B_aobd:
				if(TWRDGetMsgItems(&str,items,1)!=1 || twrd_ScanFDate(items, &fdate))
					err = 1;
				else
					setv =  (unsigned long) &fdate;
				break;
			case TWRD_B_wtc:
			case TWRD_B_flag:
				if(TWRDGetMsgItems(&str,items,1)!=1)
					err = 1;
				else
					setv = (unsigned long) items[0][0];
				break;
			case TWRD_B_rwy:
			case TWRD_B_tgCnt:
				if(TWRDGetMsgItems(&str,items,1)!=1 || sscanf(items[0],"%d",&iv)!=1)
					err = 1;
				else
					setv = (unsigned long) iv;
				break;
			case TWRD_B_aobt:
			case TWRD_B_firstC:
			case TWRD_B_lastC:
			case TWRD_B_eta:
			case TWRD_B_ata:
			case TWRD_B_divTime:
			/*case TWRD_B_etd:*/
			case TWRD_B_eobt:
			case TWRD_B_ctot:
			case TWRD_B_atd:
				if(TWRDGetMsgItems(&str,items,1)!=1 || twrd_ScanTime(items, &tm))
					err = 1;
				else
					setv = (unsigned long) &tm;
				break;
			case TWRD_B_reason:
				if(TWRDGetMsgItems(&str,items,1)!=1 || 
           (reason = twrd_ReasonByName(items[0])) ==R_NONE)
					err = 1;
				else
					setv = (unsigned long) reason;
				break;
			case TWRD_B_tgTime:
				setv = (unsigned long) &tma;
				for(ix=0; ix< msg->tgCnt && !err ; ++ix)
					if(TWRDGetMsgItems(&str, items, 1)!=1||twrd_ScanTime(items, tma+ix))
						err = 1;
				break;
			default:
					err = 1;
		}
		if(err || TWRDSetField(msg, fid, setv) == -1)
		{
			++msg->errcnt;
		}
	}

	return msg;
}

char *TWRDEncode(TWRD_MESSAGE *msg)
{
char *res;
	res = twrd_MessageToStr(msg, 0);
	if(res)
		return(strdup(res));
	return NULL;
}

int TWRDGetField(TWRD_MESSAGE *msg, unsigned long fid, void *p)
{
int rc;
/*
 * check availability
 */
	rc = twrd_GetAvailability(msg, fid);
	if(rc == TWRD_AVLNONE)
		return -1;
	if(rc == TWRD_AVLOPT && !TWRD_ISFSET_(msg, fid))
		return 0;
	if(rc == TWRD_AVLMAND && !TWRD_ISFSET_(msg, fid))
		return -1;
/*
 * copy the data.
 */
	switch(fid)
	{
 		case TWRD_B_msg_seq:
			*(unsigned long *) p = msg->msg_seq;
			break;
		case TWRD_B_mType:
			*(TWRD_MTYPE *) p = msg->mType;
			break;
		case TWRD_B_datim:
			*(TWRD_DATIM *) p = msg->datim;
			break;
		case TWRD_B_fKey:
			*(unsigned long *) p = msg->fKey;
			break;
		case TWRD_B_cs:
			twrd_StrNZCpy(p, &(msg->cs), sizeof(msg->cs));
			break;
		case TWRD_B_dof:
			*(TWRD_FDATE *) p = msg->dof;
			break;
		case TWRD_B_adep:
			twrd_StrNZCpy(p, &(msg->adep), sizeof(msg->adep));
			break;
		case TWRD_B_ades:
			twrd_StrNZCpy(p, &(msg->ades), sizeof(msg->ades));
			break;
		case TWRD_B_airc:
			twrd_StrNZCpy(p, &(msg->airc), sizeof(msg->airc));
			break;
		case TWRD_B_wtc:
			*(char *) p = msg->wtc;
			break;
		case TWRD_B_rwy:
			*(int *) p = msg->rwy;
			break;
		case TWRD_B_flag:
			*(char *) p = msg->flag;
			break;
		case TWRD_B_stand:
			twrd_StrNZCpy(p, &(msg->stand), sizeof(msg->stand));
			break;
		case TWRD_B_regMark:
			twrd_StrNZCpy(p, &(msg->regMark), sizeof(msg->regMark));
			break;
/*
 * the resctriction "[6] : both aobd and aobt must be present or omitted"
 * could be enforced here
 */
		case TWRD_B_aobd:
			*(TWRD_FDATE *) p = msg->aobd;
			break;
		case TWRD_B_aobt:
			*(TWRD_TIME *) p = msg->aobt;
			break;
/*
 * the restriction "[7] : allowed only if firstC is supplied" could be
 * enforced here 
 */
		case TWRD_B_firstC:
			*(TWRD_TIME *) p = msg->firstC;
			break;
		case TWRD_B_lastC:
			*(TWRD_TIME *) p = msg->lastC;
			break;
		case TWRD_B_eta:
			*(TWRD_TIME *) p = msg->eta;
			break;
		case TWRD_B_ata:
			*(TWRD_TIME *) p = msg->ata;
			break;
		case TWRD_B_new_ades:
			twrd_StrNZCpy(p, &(msg->new_ades), sizeof(msg->new_ades));
			break;
		case TWRD_B_divTime:
			*(TWRD_TIME *) p = msg->divTime;
			break;
		case TWRD_B_reason:
			*(TWRD_REASON *) p = msg->reason;
			break;
		case TWRD_B_prv_ades:
			twrd_StrNZCpy(p, &(msg->prv_ades), sizeof(msg->prv_ades));
			break;
		/*case TWRD_B_etd:
			*(TWRD_TIME *) p = msg->etd;*/
		case TWRD_B_eobt:
			*(TWRD_TIME *) p = msg->eobt;
			break;
		case TWRD_B_ctot:
			*(TWRD_TIME *) p = msg->ctot;
			break;
		case TWRD_B_slotMsg:
			twrd_StrNZCpy(p, msg->slotMsg, sizeof(msg->slotMsg));
			break;
		case TWRD_B_atd:
			*(TWRD_TIME *) p = msg->atd;
			break;
		case TWRD_B_tgCnt:
			*(int *) p = msg->tgCnt;
			break;
		case TWRD_B_tgTime:
			*(TWRD_TIME **) p = msg->tgTime;
			break;
		default:
			return -1;
	}
	return 1;
}

int TWRDSetField(TWRD_MESSAGE *msg, unsigned long fid, unsigned long val)
{
int rc, err, ix;
TWRD_TIME *pt;
	rc = twrd_GetAvailability(msg, fid);
	if(rc == TWRD_AVLNONE)
		return -1;
/*
 * set the data.
 */
	err = 0;
	switch(fid)
	{
 		case TWRD_B_msg_seq:
			msg->msg_seq = val;
			break;
		case TWRD_B_mType:
/*
 * mType is read only
 */
			err = 1;
			break;
		case TWRD_B_datim:
			msg->datim = *(TWRD_DATIM *) val;
			break;
		case TWRD_B_fKey:
			msg->fKey = val;
			break;
		case TWRD_B_cs:
			twrd_StrNZCpy(&(msg->cs), (void *) val, sizeof(msg->cs));
			break;
		case TWRD_B_dof:
			msg->dof = *(TWRD_FDATE *) val;
			break;
		case TWRD_B_adep:
			twrd_StrNZCpy(&(msg->adep), (void *) val, sizeof(msg->adep));
			break;
		case TWRD_B_ades:
			twrd_StrNZCpy(&(msg->ades), (void *) val, sizeof(msg->ades));
			break;
		case TWRD_B_airc:
			twrd_StrNZCpy(&(msg->airc), (void *) val, sizeof(msg->airc));
			break;
		case TWRD_B_wtc:
			msg->wtc =  (char) val;
			break;
		case TWRD_B_rwy:
			msg->rwy = (int) val;
			break;
		case TWRD_B_flag:
			msg->flag = (char) val;
			break;
		case TWRD_B_stand:
			twrd_StrNZCpy(&(msg->stand), (void *) val, sizeof(msg->stand));
			break;
		case TWRD_B_regMark:
			twrd_StrNZCpy(&(msg->regMark), (void *) val, sizeof(msg->regMark));
			break;
		case TWRD_B_aobd:
			msg->aobd = *(TWRD_FDATE *) val;
			break;
		case TWRD_B_aobt:
			msg->aobt = *(TWRD_TIME *) val;
			break;
		case TWRD_B_firstC:
			msg->firstC = *(TWRD_TIME *) val;
			break;
		case TWRD_B_lastC:
/*
 * enforcing restriction "[7] : allowed only if firstC is supplied"
 */
			if(!TWRD_ISFSET_(msg, TWRD_B_firstC))
				err = 2;
			else
				msg->lastC = *(TWRD_TIME *) val;
			break;
		case TWRD_B_eta:
			msg->eta =  *(TWRD_TIME *) val;
			break;
		case TWRD_B_ata:
			msg->ata = *(TWRD_TIME *) val;
			break;
		case TWRD_B_new_ades:
			twrd_StrNZCpy(&(msg->new_ades), (void *) val, sizeof(msg->new_ades));
			break;
		case TWRD_B_divTime:
			msg->divTime = *(TWRD_TIME *) val;
			break;
		case TWRD_B_reason:
			msg->reason = (TWRD_REASON) val;
			break;
		case TWRD_B_prv_ades:
			twrd_StrNZCpy(&(msg->prv_ades), (void *) val, sizeof(msg->prv_ades));
			break;
		case TWRD_B_eobt:
			msg->eobt = *(TWRD_TIME *) val;
/*
 * enforcing restriction "[4] : ETD and START_UP have EITHER etd OR ctot"
 * enforcing restriction "[4] : EOBT has EITHER etd OR ctot"
 */
			TWRD_UNSETF_(msg, TWRD_B_ctot);
			twrd_AlterTable(msg, TWRD_B_eobt, TWRD_AVLMAND);
			twrd_AlterTable(msg, TWRD_B_ctot, TWRD_AVLOPT);
			break;
		case TWRD_B_ctot:
			msg->ctot = *(TWRD_TIME *) val;
			TWRD_UNSETF_(msg, TWRD_B_eobt);
			/*twrd_AlterTable(msg, TWRD_B_ctot, TWRD_AVLMAND); JWE 03.01.2002 */
			twrd_AlterTable(msg, TWRD_B_ctot, TWRD_AVLOPT);
			twrd_AlterTable(msg, TWRD_B_eobt, TWRD_AVLOPT);
			break;
		case TWRD_B_slotMsg:
			twrd_StrNZCpy(msg->slotMsg, (void *) val, sizeof(msg->slotMsg));
			break;
		case TWRD_B_atd:
			msg->atd =  *(TWRD_TIME *) val;
			break;
		case TWRD_B_tgCnt:
/*
 * enforcing implementation limitation
 */
			if((int) val > TWRD_MAXTGTIME || (int) val < 0)
				err = 3;
			else
				msg->tgCnt = (int) val;
			break;
		case TWRD_B_tgTime:
			pt = (TWRD_TIME *) val;
			for(ix=0; ix<msg->tgCnt; ++ix)
				msg->tgTime[ix] = *pt++;
			break;
		default:
			err = 4;
	}
	if(err)
		return -1;

	TWRD_SETF_(msg, fid);
	return 1;

}

/*
 * print message into char buffer and return the buffer. The buffer
 * is static and should be copied if the result should be preserved
 * across calls to this function
 */

char *TWRDDumpMessage(TWRD_MESSAGE *msg)
{
	return twrd_MessageToStr(msg,1);
}

/*
 * Function to check message integrity: returns nonzero if the parameter
 * does not look like a message or has at least one mandatory field unset.
 *
 * Note: the implementation is a bit redundant and not optimized (mainly
 * because of calling GetAvailablity() in a loop)
 */

int TWRDCheckMsg(TWRD_MESSAGE *msg)
{
int fx, avl;
twrd_FieldAvailRec *far;
	if(!msg || msg->magic != twrd_magic)
		return -1;
	for(fx=0; twrd_FieldAvailTab[fx].fid != TWRD_B_end; ++fx)
	{
		far = &twrd_FieldAvailTab[fx];
		avl = twrd_GetAvailability(msg, far->fid);
		if(avl == TWRD_AVLMAND && !TWRD_ISFSET_(msg, far->fid))
				return -1;
	}
	return 0;
}

/*
 * Return number of non-fatal errors detected during decoding
 * > 0 number of errors (typically ignored fields)
 * = 0 no errors (always the case with messages inited with TWRDInitialize()
 * < 0 bad msg 
 */

int TWRDErrorCount(TWRD_MESSAGE *msg)
{
	if(!msg || msg->magic != twrd_magic)
		return -1;
	return msg->errcnt;
}

/*
 * return msg items from string moving the string ptr
 */

int TWRDGetMsgItems(char **ps, TWRD_MSGITEM *pits, int mi)
{
int ix, dx;
#define item_delim(c) ((c)==' ' || (c)=='\t')

	for(ix=0; ps && *ps && **ps && ix<mi;)
	{
		while(**ps && item_delim(**ps))
			++*ps;
		for(dx=0; dx<sizeof(TWRD_MSGITEM)-1&&**ps&&!item_delim(**ps); ++dx,++*ps)
			pits[ix][dx] = **ps;
		pits[ix][dx] = '\0';
		if(dx)
			++ix;
	}
	return ix;
}


/*
 * private part
 */

static int twrd_GetAvailability(TWRD_MESSAGE *msg, unsigned long fid)
{
int ix;
char avl;
/*
 * msg must be inited properly
 */
	if(!msg || msg->magic != twrd_magic || !TWRD_ISFSET_(msg, TWRD_B_mType))
		return TWRD_AVLNONE;
	ASSERT(msg->mType >= 0 && msg->mType < TWRD_NMTYPES);
/*
 * lookup for field id
 */
	for(ix=0; twrd_FieldAvailTab[ix].avail[0]; ++ix)
		if(twrd_FieldAvailTab[ix].fid == fid)
			break;
/*
 * field id not found
 */
	if(!twrd_FieldAvailTab[ix].avail[0])
		return TWRD_AVLNONE;
/*
 * check availability
 */
	avl = twrd_FieldAvailTab[ix].avail[msg->mType];
	if(avl == '-')
		return TWRD_AVLNONE;
	if(avl == 'o')
		return TWRD_AVLOPT;
	ASSERT(avl == '!');
	return TWRD_AVLMAND;
}

void twrd_StrNZCpy(void *dst, void *src, size_t len)
{
	strncpy((char *)dst, (char *)src, len);
	((char *) dst)[len-1] = '\0';
}

TWRD_MTYPE twrd_TypeByName(char *n)
{
int ix;
	for(ix=0; twrd_TypeTable[ix].name; ++ix)
		if(strcasecmp(n, twrd_TypeTable[ix].name) == 0)
			break;
	return  twrd_TypeTable[ix].type;
}

char *twrd_NameByType(TWRD_MTYPE t)
{
int ix;
	for(ix=0; twrd_TypeTable[ix].name; ++ix)
		if(twrd_TypeTable[ix].type == t)
			break;
	return  twrd_TypeTable[ix].name;
}

static unsigned long twrd_FIdByKey(char *key)
{
int ix;
	for(ix=0; twrd_FieldAvailTab[ix].avail[0]; ++ix)
		if(strcasecmp(twrd_FieldAvailTab[ix].key, key) == 0)
			break;
	return  twrd_FieldAvailTab[ix].fid;
}

#if 0 /* need it? */
static char *twrd_NameByFId(unsigned long fid)
{
int ix;
	for(ix=0; twrd_FieldAvailTab[ix].avail[0]; ++ix)
		if(twrd_FieldAvailTab[ix].fid == fid)
			break;
	return  twrd_FieldAvailTab[ix].fname;
}
#endif /* need it? */

static TWRD_REASON twrd_ReasonByName(char *n)
{
int ix;
	for(ix=0; twrd_ReasonTable[ix].name; ++ix)
		if(strcasecmp(twrd_ReasonTable[ix].name,n) == 0)
			break;
	return twrd_ReasonTable[ix].reason;
}

static char *twrd_NameByReason(TWRD_REASON r)
{
int ix;
	for(ix=0; twrd_ReasonTable[ix].name; ++ix)
		if(twrd_ReasonTable[ix].reason == r)
			break;
	return twrd_ReasonTable[ix].name;
}

/*
 * scan functions for item values
 */

int twrd_ScanDatim(TWRD_MSGITEM its[], TWRD_DATIM *p)
{
	if(twrd_ScanDate(its, &(p->dat)))
		return -1;
	return twrd_ScanCTime(its+3, &(p->tim));
}

int twrd_ScanDate(TWRD_MSGITEM its[], TWRD_DATE *p)
{
int ix;

	if(sscanf(its[0],"%hd",&(p->day)) != 1)
		return -1;

	for(ix=0; ix<12; ++ix)
		if(strcasecmp(its[1], twrd_MonthTable[ix]) == 0)
			break;
	if(ix >= 12)
		return -1;
	p->month = ix+1;

	if(sscanf(its[2],"%hd",&(p->year)) != 1)
		return -1;
	
	return 0;
}

int twrd_ScanFDate(TWRD_MSGITEM its[], TWRD_DATE *p)
{
int ix, c;
int num_ydigits;

/*
 * the specs says the year has 4 digits but some examples have only 2
 * (contradictory). we try to figure it out here.
 */
	num_ydigits= strlen(its[0]) == 6 ? 2 : 4;

	p->year = p->month = p->day = 0;

	for(ix=0; ix< num_ydigits+4 && (c=its[0][ix]) != 0; ++ix)
	{
		if(c < '0' || c > '9')
			break;
		c -= '0';
		if(ix == 0)
			p->year = c;
		else if (ix < num_ydigits)
			p->year = p->year * 10 + c;
		else if(ix == num_ydigits)
			p->month = c;
		else if (ix == num_ydigits+1)
			p->month = p->month * 10 + c;
		else if(ix == num_ydigits+2)
			p->day = c;
		else
			p->day = p->day * 10 + c;
	}
	return ix == num_ydigits+4 ? 0 : -1;
}

int twrd_ScanTime(TWRD_MSGITEM its[], TWRD_TIME *p)
{
int ix, c;

	p->hour = p->min = 0;

	for(ix=0; ix<4 && (c=its[0][ix]) != 0; ++ix)
	{
		if(c < '0' || c > '9')
			break;
		c -= '0';
		if(ix == 0)
			p->hour = c;
		else if (ix == 1)
			p->hour = p->hour * 10 + c;
		else if(ix == 2)
			p->min = c;
		else
			p->min = p->min * 10 + c;
	}
	return ix == 4 ? 0 : -1;
}

		
int twrd_ScanCTime(TWRD_MSGITEM its[], TWRD_CTIME *p)
{
	if(sscanf(its[0],"%hd:%hd",&(p->hour),&(p->min)) != 2)
		return -1;
	return 0;
}

/*
 * the following functions return a pointer to a \0 terminated 
 * buffer holding the ASCII representation of the passed parameter.
 * The buffer is static and should be copied if its contents should 
 * be preserved across calls to the same function.
 */

static char *twrd_MessageToStr(TWRD_MESSAGE *msg, int diag)
{
static DynStr *str = NULL;
char lineb[2000], *pb, c;
twrd_FieldAvailRec *far;
int avl, rc, iv, fx, tx;
unsigned long ul;
TWRD_MTYPE mt;
TWRD_DATIM datim;
TWRD_FDATE fdate;
TWRD_TIME tm, *tmp;
TWRD_REASON rsn;

	if(str == NULL)
		str = DynStr_CreateQ(500);
	else
		DynStr_Reset(str);
	if(!msg || msg->magic != twrd_magic)
	{
		if(diag)
			DynStr_AppendStr(str,"Invalid message ptr or magic number");
		return str->chars;
	}
	
	for(fx=0; twrd_FieldAvailTab[fx].fid != TWRD_B_end; ++fx)
	{
		far = &twrd_FieldAvailTab[fx];
		if(diag)
		{
			DynStr_AppendStr(str,"  ");
			DynStr_AppendStr(str,far->fname);
			DynStr_AppendStr(str,": ");
		}
		avl = twrd_GetAvailability(msg, far->fid);
		if(avl == TWRD_AVLNONE)
		{
			if(diag)
				DynStr_AppendStr(str, "-- N/A\n");
			continue;
		}
		pb = lineb;
		switch(far->fid)
		{
			case TWRD_B_msg_seq:
				if((rc = TWRDGetField(msg, far->fid, &ul)) == 1)
					sprintf(lineb,"0x%04hX",(unsigned short) ul);
				break;
			case TWRD_B_mType:
				if((rc = TWRDGetField(msg, far->fid, (void *) &mt)) == 1)
					pb = STR0(twrd_NameByType(mt));
				break;
			case TWRD_B_datim:
				if( (rc = TWRDGetField(msg, far->fid, (void *) &datim)) == 1)
					pb = twrd_DatimToStr(&datim);
				break;
			case TWRD_B_fKey:
				if((rc = TWRDGetField(msg, far->fid, &ul)) == 1)
					sprintf(lineb,"0x%08lX",ul);
				break;
			case TWRD_B_cs:
			case TWRD_B_adep:
			case TWRD_B_ades:
			case TWRD_B_airc:
			case TWRD_B_stand:
			case TWRD_B_regMark:
			case TWRD_B_new_ades:
			case TWRD_B_prv_ades:
			case TWRD_B_slotMsg:
				rc = TWRDGetField(msg, far->fid, (void *) lineb);
				break;
			case TWRD_B_dof:
			case TWRD_B_aobd:
				if((rc = TWRDGetField(msg, far->fid, (void *) &fdate)) == 1)
					pb = twrd_FDateToStr(&fdate);
				break;
			case TWRD_B_wtc:
			case TWRD_B_flag:
				if((rc = TWRDGetField(msg, far->fid, (void *) &c)) == 1)
				{
					lineb[0] = c;
					lineb[1] = '\0';
				}
				break;
			case TWRD_B_rwy:
			case TWRD_B_tgCnt:
				if((rc = TWRDGetField(msg, far->fid, (void *) &iv)) == 1)
					sprintf(lineb,"%d", iv);
				break;
			case TWRD_B_aobt:
			case TWRD_B_firstC:
			case TWRD_B_lastC:
			case TWRD_B_eta:
			case TWRD_B_ata:
			case TWRD_B_divTime:
			/*case TWRD_B_etd:*/
			case TWRD_B_eobt:
			case TWRD_B_ctot:
			case TWRD_B_atd:
				if((rc = TWRDGetField(msg, far->fid, (void *) &tm)) == 1)
					pb = twrd_TimeToStr(&tm);
				break;
			case TWRD_B_reason:
				if((rc = TWRDGetField(msg, far->fid, (void *) &rsn)) == 1)
					pb = twrd_NameByReason(rsn);
				break;
			case TWRD_B_tgTime:
				if((rc = TWRDGetField(msg, far->fid, (void *) &tmp)) == 1)
				{
					for(tx=0; tx< msg->tgCnt; ++tx)
					{
						if(tx == 0)
							lineb[0] = '\0';
						else
							strcat(lineb," ");
						strcat(lineb,twrd_TimeToStr(tmp+tx));
					}
				}
				break;
			default:
				ASSERT(0);
		}

		if(rc == -1)
		{
			if(diag)
				DynStr_AppendStr(str, "-- MISSING (error)");
		}
		else if(rc == 0)
		{
			if(diag)
			DynStr_AppendStr(str, "-- NOT SET");
		}
		else
		{
			if(!diag)
			{
				if(fx > 0)
					DynStr_AppendC(str,' ');
				if(far->key[0])
				{
					DynStr_AppendC(str,'/');
					DynStr_AppendStr(str,far->key);
					DynStr_AppendC(str,' ');
				}
			}
			DynStr_AppendStr(str, pb);
		}
		if(diag)
			DynStr_AppendC(str,'\n');
	}

	return str->chars;
}

char *twrd_DatimToStr(TWRD_DATIM *p)
{
static char buff[100];
	strcpy(buff,twrd_DateToStr(&(p->dat)));
	strcat(buff," ");
	strcat(buff,twrd_CTimeToStr(&(p->tim)));
	return buff;
}

char *twrd_TimeToStr(TWRD_TIME *p)
{
static char buff[100];
	sprintf(buff,"%02d%02d",p->hour,p->min);
	return buff;
}

char *twrd_CTimeToStr(TWRD_CTIME *p)
{
static char buff[100];
	sprintf(buff,"%02d:%02d",p->hour,p->min);
	return buff;
}

char *twrd_DateToStr(TWRD_DATE *p)
{
static char buff[100];
	sprintf(buff,"%02d %s %04d",p->day,twrd_MonthTable[(p->month-1)%12],p->year);
	return buff;
}

char *twrd_FDateToStr(TWRD_FDATE *p)
{
static char buff[100];
	sprintf(buff,"%04d%02d%02d", p->year, p->month, p->day);
	return buff;
}

/*
 * Set the FDATE and TIME structures from the of the 14 digit YYYYMMDDhhmmss
 * date and time representation. The seconds are ignored.
 * rc = 0 if Ok and -1 upon error
 */

int TWRD_F14CToDT(char *s, TWRD_FDATE *pd, TWRD_TIME *pt)
{
TWRD_MSGITEM mid[1], mit[1];
	if(strlen(s) < 12)
		return -1;
	strncpy(mid[0] , s, 8);
	mid[0][8] = '\0';
	strncpy(mit[0] , s + 8, 4);
	mit[0][4] = '\0';
	if(twrd_ScanFDate(mid, pd) == 0 && twrd_ScanTime(mit, pt) == 0)
		return 0;
	return -1;
}

/*
 * return ptr to static char holding the 14 digit YYYYMMDDhhmmss
 * representation of the data and time in FDATE and TIME format.
 * The seconds are always null.
 *
 * JWE's rule: it is allowed to omit the time (t equals to NULL) in which case
 * 0000 is substituted. If the year is less than 100 then 2000 is added making
 * for example 1 to 2001 in the output string (Note: it's not really
 * POSIX conform).
 *
 * It is also allowed to mit the date (d equals to NULL) in which case the 
 * 20000101 is substituted.
 */

#if 0
char *TWRD_DTToF14C(TWRD_FDATE *d, TWRD_TIME *t)
{
static char b[100];
short h, m;
short year, month, day;
	if(t)
	{
			h = t->hour;
			m = t->min;
	}
	else
	{
		h = 0;
		m = 0;
	}
	if(d)
	{
		year = d->year;
		month = d->month;
		day = d->day;
		if(year < 100)
			year += 2000;
	}
	else
	{
		year = 2000;
		month = 1;
		day = 1;
	}

	sprintf(b,"%04d%02d%02d%02d%02d00", year, month, day, h, m);
}
#endif
  
char *TtUd(TWRD_FDATE *d)
{
static char b[100];
short year, month, day;
	if(d)
	{
		if (d->year==-1 && d->month==-1 && d->day==-1)
		{
			b[0] = '\0'; /* jhe 010926 sprintf(b,""); */
		}
		else
		{
			year = d->year;
			month = d->month;
			day = d->day;
			if(year < 100)
				year += 2000;
			sprintf(b,"%04d%02d%02d", year, month, day);
		}
	}
	else
	{
		b[0] = '\0'; /* jhe 010926 sprintf(b,""); */
		/*year = 2000;
		month = 1;
		day = 1;*/
	}
	return b;
}

char *TtUt(TWRD_TIME *t)
{
static char b[100];
short h,m;
	if(t)
	{
		if (t->hour==-1 && t->min==-1)
		{
			b[0] = '\0'; /* jhe 010926 sprintf(b,""); */
		}
		else
		{
			h = t->hour;
			m = t->min;
			sprintf(b,"%02d%02d",h,m);
		}
	}
	else
	{
		b[0] = '\0'; /* jhe 010926 sprintf(b,""); */
	}
	return b;
}

char *TtCdt(TWRD_FDATE *d, TWRD_TIME *t)
{
static char b[100],date[10],time[10];

	if(t)
	{
		sprintf(time,"%s",TtUt(t));
	}
	else
	{
		time[0] = '\0'; /* jhe 010926 sprintf(time,""); */
	}

	if(d)
	{
		sprintf(date,"%s",TtUd(d));
	}
	else
	{
		date[0] = '\0'; /* jhe 010926 sprintf(date,""); */
	}

	if (strlen(date)>0 && strlen(time)>0)
	{
		/* JWE 20041007: adding 00 seconds to get a 14 digit timestamp */
		if (strlen(time)<=4)
		{
			strncat(time,"00",2);
		}
		sprintf(b,"%s%s",date,time);
	}
	else
		b[0] = '\0'; /* jhe 010926 sprintf(b,""); */

	return b;
}
void UdtT(char *pcpCedaDate,TWRD_FDATE *pcpDate)
{
  char  year[8],month[8],day[8];
	memset(year,0x00,8);
	memset(month,0x00,8);
	memset(day,0x00,8);

  if(pcpCedaDate)
  {
     strncpy(year,pcpCedaDate,4);
     strncpy(month,(char*)&pcpCedaDate[4],2);
     strncpy(day,(char*)&pcpCedaDate[6],2);
     pcpDate->year = atoi(year);
     pcpDate->month = atoi(month);
     pcpDate->day = atoi(day);
  }
}
void UttT(char *pcpCedaDate, TWRD_TIME *pcpTime)
{
 	char  hour[8],min[8];
	memset(hour,0x00,8);
	memset(min,0x00,8);

 if(pcpCedaDate)
 {
   strncpy(hour,(char*)&pcpCedaDate[8],2);
   strncpy(min,(char*)&pcpCedaDate[10],2);
   pcpTime->hour = atoi(hour);
   pcpTime->min = atoi(min);
 }
}

/*
 * Change availability information in the table. This is actually used
 * to manipulate correlated availability of different fields (eobt and
 * ctot for EOBT).
 *
 * Use this function carefully.
 */

void twrd_AlterTable(TWRD_MESSAGE *msg, unsigned long fid, int avl)
{
int ix, mx;
/*
 * offset for msg type (avail table column)
 */
	mx = msg->mType - ETA;
	ASSERT(mx >= 0 && mx < TWRD_NMTYPES);
/*
 * get field id (avail table row)
 */
	for(ix=0; twrd_FieldAvailTab[ix].avail[0]; ++ix)
		if(twrd_FieldAvailTab[ix].fid == fid)
			break;
	ASSERT(twrd_FieldAvailTab[ix].avail[0]);
/*
 * set the new value for (field id , msg type)
 */
	switch(avl)
	{
		case TWRD_AVLOPT:
			twrd_FieldAvailTab[ix].avail[mx] = 'o';
			break;
		case TWRD_AVLMAND:
			twrd_FieldAvailTab[ix].avail[mx] = '!';
			break;
		case TWRD_AVLNONE:
			twrd_FieldAvailTab[ix].avail[mx] = '-';
			break;
		default:
			ASSERT(0);
	}
}
