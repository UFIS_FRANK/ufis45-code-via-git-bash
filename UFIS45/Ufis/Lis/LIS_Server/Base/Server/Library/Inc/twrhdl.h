#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Lis/LIS_Server/Base/Server/Library/Inc/twrhdl.h 1.3 2005/02/07 20:25:11SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/*********************************************************/
/* TWRHDL inetrface to ATC-tower lisbon        */
/* header file                                           */
/*********************************************************/
static char sccs_twrhdl_inc[]="@(#) UFIS 4.4 (c) ABB AAT/I twrhdl.h 44.1 / 01/08/28 00:00:00 / JWE"; 
#ifndef BOOL
#define BOOL int
#endif

/***************/
/* ALL DEFINES */
/***************/
#define XXS_BUFF  32
#define XS_BUFF  128
#define S_BUFF  512
#define M_BUFF  1024
#define L_BUFF  2048
#define XL_BUFF 4096

#ifndef MAX_BUFFER_LEN
	#define MAX_BUFFER_LEN 2048
#endif
#define DATABLK_SIZE  524288 /* Maximum space for sql_if data_area&sql-buffer*/
#define CONNECT_TIMEOUT 2
#define READ_TIMEOUT 2
#define WRITE_TIMEOUT 2

#define RIGHT 1100
#define LEFT 1101

#define CFG_ALPHA 1200
#define CFG_NUM		1201
#define CFG_ALPHANUM	1202
#define CFG_IGNORE	1203
#define CFG_PRINT	1204

#define HEARTBEAT	0
#define QB_INT_MSG	1
#define TOWER_DATA	2

typedef struct
{
	char section_name[XXS_BUFF];
	char *startup;						/* shall I be started up ? */
	char *binary_name;				/* name of the bianry to startup */
	char *cmd_line;						/* cmd-line for starting the child */
	char *timeout;						/* value for restarting child after terminate */
	char *homeap;							/* value for home-airport*/
	unsigned long  llHBCount;	/* heartbeat counter */
	int ilNr;									/* group nr */
	QBHandle handle;					/* int pipe or file descr. to write to */
	char *Send_modid;					/* int pipe or file descr. to write to */
	int sndmod_id;						/* int pipe or file descr. to write to */
	char *natlib_log;					/* shall I write a error log from library functions ?*/
	char *write_to_db;				/* shall I sent data via flight to DB ?*/
	char *ufis_rcv_modid;			/* modid to write events to ufis */
	char *send_info_telex;		/* shall info telexes being sent or not */
	char *timeaccuracy;
	int iSit;									/* int flag for telex info */
	int ilRcvModId;						/* modid to write events to ufis */
	LPLISTHEADER prlDataList;	/* information on flights concerning tower information */
}CHILD_INFO;

typedef struct
{
	int ilNewMessage;
	/* JWE 20050127: PRF 6772 - b) */
	int ilKeyChange;
	/* JWE 20050127: PRF 6772 - a) */
	int ilInUfisDB;
	long llurno;
	char pcladid[2];
	char pclregn[13];
	char pclpsta[6];
	char pclonbl[15];
	char pclpstd[6];
	char pclofbl[15];
	char pclrem1[260]; /*rem1 = 256 in database */
	char pclstoa[15];
	char pclstod[15];

 	/* message header */
  unsigned long msg_seq;
  TWRD_MTYPE mType;
  TWRD_DATIM datim;

 	/*'generic' data fields */
  unsigned long fKey;
  TWRD_CALLSIGN cs;
  TWRD_FDATE dof;
  TWRD_ICAO adep;
  TWRD_ICAO ades;
  TWRD_AIRCTYP airc;
  char wtc;
  int rwy;
  char flag;
  TWRD_STDID stand;
  TWRD_RMARK regMark;
  TWRD_FDATE aobd;
  TWRD_TIME aobt;
  TWRD_TIME firstC;
  TWRD_TIME lastC;

	/* specific message data (aosarr and aosdep have no specific data) */
  TWRD_TIME eta;      /* ETA, GO_AROUND */
  TWRD_TIME ata;      /* ARR */
  TWRD_ICAO new_ades; /* DIV */
  TWRD_TIME divTime;  /* DIV, DIV_ETA */
  TWRD_REASON reason; /* DIV, DIV_ETA */
  TWRD_ICAO prv_ades; /* DIV_ETA */
  /*TWRD_TIME etd;      ETD, STARTUP */
  TWRD_TIME eobt;      /* ETD, STARTUP */
  TWRD_TIME ctot;     /* ETD, STARTUP, DEP */
  TWRD_MSG  slotMsg;  /* ETD, DEP, CANCEL */
  TWRD_TIME atd;      /* DEP */
  int tgCnt;          /* TG */
  /*TWRD_TIME tgTime[TWRD_MAXTGTIME];  TG */
	char tg_times[S_BUFF];			/* string of all times for touch and go message */
} TWR_DATA;
