#ifndef _DEF_mks_version_fileif_h
  #define _DEF_mks_version_fileif_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fileif_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/Lis/LIS_Server/Base/Server/Library/Inc/fileif.h 1.1 2006/04/12 22:40:49SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :							  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */
#ifndef _FILEIF_H
#define _FILEIF_H

#ifndef BOOL
#define BOOL int
#endif


#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <memory.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include "ugccsma.h"
#include "quedef.h"
#include "glbdef.h"
#include "new_catch.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "syslib.h"
#include "l_ccstime.h"
#include "msgno.h"
#include "helpful.h"
#include "send.h"
/********
#include "uevent.h"
#include "tools.h"
#include "send_tools.h"
*******/
/*changes by MOS 28 June 2000 for FTPHDL implementation*/
#include "tools.h"
/* EOChanges */

/*changes by MOS 6 Apr for VIA field handling */
#include "db_if.h"
/* EOChanges */

extern char *strdup(const char*);

#define	iFILEIF_ERROR_MESSAGE1		513

#define	iOPEN_FAIL						-1
#define	iOPEN_CTL_FAIL					-2
#define 	iWRITE_FILE_FAIL				-3

#define	cLEFT								'L'
#define	cRIGHT							'R'
#define	iBLANK							32
#define	cCOMMA							','
#define 	cSAME								'='
#define	cSEPARATOR						','
#define	sDEFAULT_FILEPATH				"./"
#define	sDEFAULT_FILE					"data"
#define	sDEFAULT_FILEEXTENSION		".dat"
#define	sDEFAULT_SOF					"{FILESTART}"
#define	sDEFAULT_EOF					"{FILEEND}"
#define	sDEFAULT_SOBNUMBER			"NO"
#define	sDEFAULT_SOB					"{BLOCK}"
#define	sDEFAULT_EOBNUMBER			"NO"
#define	sDEFAULT_EOB					"{ENDE}"
#define	sDEFAULT_CHAR_SET				"ASCII"
#define	sDEFAULT_EOL					"CR,LF"
#define	sDEFAULT_COMMENT				".;"
#define	sDEFAULT_WRITE_DATA			"DYNAMIC"
#define	sDEFAULT_FIELD_NAME			"NO"
#define	sDEFAULT_FILETIMESTAMP		"NO"
#define	sDEFAULT_CTRLFILE				"./ctrl.ftp"
#define	sDEFAULT_INFOFILE				"./fileif.inf"
#define	s14_BLANKS						"              "

#define	iASCII							0
#define	iANSI								1
#define	iWRITE_DYNAMIC					0
#define	iWRITE_FIX						1

#define	iORG_DATA						0
#define	iNULL_DATA						1
#define	iFOUND							0
#define	iNOT_FOUND						1

/* defines for status word (16 Bit) */
#define 	iINITIALIZE						0x0000 
/* #define	iHANDLE_CMD_MEMORY			0x0001 */
/* #define	iHANDLE_TABLE_MEMORY			0x0002 */
/* #define	iHANDLE_POINTER_MEMORY		0x0004 */
/* #define	iDATABASE_CONNECTION			0x0008 */
/* #define	iDATAFILE_OPEN					0x0010 */
/* #define	iHANDLE_FORMAT_MEMORY		0x0020 */
/* #define				0x0040 */
/* #define				0x0080 */
/* #define				0x0100 */
/* #define				0x0200 */
/* #define				0x0400 */
/* #define				0x0800 */
/* #define				0x1000 */
/* #define				0x2000 */
/* #define				0x4000 */
/* #define				0x8000 */

#define	IsAscii(a) 		((a) == iASCII ? 1 : 0)
#define	IsAnsi(a)  		((a) == iANSI  ? 1 : 0)
#define	IsCR(a)			(strcmp((a),"CR") ? 0 : 1)	
#define	IsLF(a)			(strcmp((a),"LF") ? 0 : 1)	
#define	IsDynamic(a)	((a) == iWRITE_DYNAMIC ? 1 : 0)
#define	IsFix(a)			((a) == iWRITE_FIX ? 1 : 0)
#define	FMIN(a,b)		((a) < (b) ? (a) : (b))
#define	FMAX(a,b)		((a) > (b) ? (a) : (b))

typedef struct _substr
{
	int			iUseSubStr;	/* should i use substrings... */
	int			iStartPos;	/* position we start (like ORACLE-SUBSTR) */ 
	int			iBytes;	/* number of bytes to copy */
} SubStr;

typedef struct _sndecmd
{
	int			iModID;	/* mod_id we send to */
	char			pcCmd[iMIN];	/* command we send to */
	int			iSendFileToQue; /* should i write file to que? */
	int 		  iAfcKeepOrgTwStart;
	int			iAfcSendFilename;
	char 		  pcOrgTwStart[iMIN_BUF_SIZE];
} SndECmd;

typedef struct _datamap
{
	UINT			iOtherData; /* flag */
	char			pcOrgData[iMIN_BUF_SIZE]; /* original data */
	char			pcNewData[iMIN_BUF_SIZE]; /* the new data */
} DataMap;

typedef struct _syslib
{
	char			pcTabName[iMIN];	/* Name of table */
	char			pcTabField[iMIN];	/* Field in table */
	char			pcIField[iMIN];	/* internal field */
	char			pcResultField[iMIN]; /* result field... */
} SysLib;

typedef struct _assign
{
	UINT			iDBFieldSize;	/* length of database field */
	UINT			iIFFieldSize;	/* length of interface field */
	char			pcDBField[iMIN];	/* name of database field (for mapping) */
	char			pcIFField[iMIN];	/* name of interface field (for mapping) */
} Assign;

typedef struct _conditions
{
	char			pcCondition[iMIN]; /* condition for this field */
	char			pcFieldName[iMIN]; /* field-name */	
	UINT			iFieldPosition;	/* position of this field in data list */
} Conditions;

typedef struct _tabdef
{
	char			**pcFieldTimestampFormat; /* the timestamp format */
	UINT			iNoOfUtcToLocal; /* counter for mapped fields */
	char			**pcMapUtcToLocal; /* fields to map... */
	SubStr		*prSubStr;	/* the sub-strings... */
	UINT			iVia34;	/* looking for 3- or 4-lettercode in VIAL */
	char			pcConst[10][iMIN]; /* for constant data max 10... */
	char			pcSectionName[iMIN];	/* name of this section */
	UINT			*piNoMapData;	/* how many data for mapping */
	DataMap		**prDataMap; /* the mapping */	
	UINT			iViaInFieldList; /* is there a VIAx in field list? */
	int 			iAddVialVian; /* old behaviour if VIAN/VIAL in list */
	int			iNoSyslib;	/* how many syslib data */
	SysLib		*prSysLib;	/* for all syslibs... */	
	UINT			iLines; /* write only the first n lines... */
	char			pcSndCmd[iMIN];	/* send command to ... */
	int			iDataLengthCounter;	/* counter field length */
	UINT			*piDataLength; /* the field length */
	UINT			iUseComment;	/* should we set comment? */
	char			pcComment[iMIN]; /* the comment */
	char			pcTabName[iMIN];	/* name of table */
	UINT			iUseSelection; /* should we use selection? */
	char			pcSelection[iMAX_BUF_SIZE]; /* the CFG selection */
	/* ADDED BY BERNI */
	char			pcUsedSelection[iMAX_BUF_SIZE]; /* the used selection */
	int			iNoFields; /* number of all fields */
	char			**pcFields; /* the fields */
	char			*pcDataAlignment; /* Alignment for all data fields */
	int			iNoAssign; /* how many assignments? */
	Assign		*prAssign; /* the assignments */
	int			iNoCondition; /* how many conditions */
	Conditions	*prCondition;	/* the conditions */
	int			iNoOfIgnore; /* how many fields must we ignore */
	char			**pcIgnoreFields; /* the ignore fields */
} TabDef;

typedef struct	_handleCmd 
{
	char			pcHomeAirport[iMIN]; /* the HomeAirport */
	UINT			iUseReceivedEvent; /* using the received fields and data for conversion. Do NOT select any data */
	char			pcTableExtension[iMIN]; /* the table extension */
	char			pcShell[iMIN]; /* the shell for FTP... */
	int			iModID; /* to mod_id */
	UINT			iSendSqlRc; /* send answer to client */
	int			iSqlRCOriginator; /* this is the originator we send to */
	UINT			iUseReceivedSelection; /* should i use received (via QUE) sele */
	UINT			iUseSEC;	/* should i send command after file creating? */
	SndECmd		rSendEventCmds;	/* sending commands after creating files */
	UINT			iCheckFileDiff;	/* check diff between old and new file */
	char			pcOldFileNameWithPath[iMIN_BUF_SIZE]; /* the last filename... */
	long			lOldFileLength; /* the old file length */
	long			lFileLength; /* the new file length */
	char			*pcOldFileContent; /* content of old data file... */
	char			*pcFileContent; /* content of new data file... */
	UINT			iUseAirport; /* kennung schreiben... */
	char			pcAirport[iMIN_BUF_SIZE]; /* for airport full name */
	int			iNoOfSQLKeywords; /* how many SQL-Keywords */
	char			**pcSQLKeywords; /* the keywords */
	FILE			*pFh;	/* for this data file */
	UINT			iOpenDataFile;	/* is data file for this command open? */
	UINT			iUseFilenameWithTimestamp; /* filename with timestamp */
	UINT			iUseFileTimestampBase; /* UTC=0 / Local=1  */
	UINT			iUseFilenameWithSeqNr;
	UINT			iStdLineNrLen;
	char			pcTimeStampFormat[iMIN]; /* format of timestamp */
	char			pcDynDateFormat[iMIN]; /* format of timestamp */
	char			pcDynTimeFormat[iMIN]; /* format of timestamp */
	char			pcFile[iMIN_BUF_SIZE]; /* the final filename */
	char			pcFileExtension[iMIN]; /* the fileextension */
	char			pcFileName[iMIN_BUF_SIZE]; /* the final & complete filename */
	char			pcFileNameWithPath[iMIN_BUF_SIZE]; /* final & complete filename and path */
	char			pcTmpFile[iMIN_BUF_SIZE]; /* the tmp filename during creation */
	char			pcTmpFileName[iMIN_BUF_SIZE]; /* the tmp-complete filename */
	char			pcTmpFileNameWithPath[iMIN_BUF_SIZE]; /* the tmp complete & final filename and path */
	UINT			iUseFTP;	/* should i use FTP? */
	char			pcFtpUser[iMIN]; /* the ftp-user */
	char			pcFtpPass[iMIN]; /* password of ftp-user */
	char			pcMachine[iMIN]; /* name of machine we send file to */
	char			pcftp_timeout[iMIN]; /* timeout for data transfer */
	char			pcftp_mode[iMIN]; /* IF set to SYSTEM then use system call, else if available FTPHDL */
	char			pcMachinePath[iMIN_BUF_SIZE]; /* path at machine */
	char			pcFilePath[iMIN_BUF_SIZE]; /* path for creating file */
	char			pcFtp_Client_OS[iMIN_BUF_SIZE]; /* CHANGES BY MOS 03.JULY.2000 OS of remote machine*/
	char			pcFtpDestFileName[iMIN_BUF_SIZE]; /* name of remote file*/
	char			pcFtpRenameDestFile[iMIN_BUF_SIZE]; /* rename value of remote file*/
	char			pcRemoteFile_AppendExtension[iMIN_BUF_SIZE]; /*extension of the remote file*/
	char			pcNumtab_Key[iMIN_BUF_SIZE]; /* key of datarecord in numtab*/
	char			pcNumtab_Startvalue[iMIN_BUF_SIZE]; /* start value in numtab */
	char			pcSeqResetDay[iMIN_BUF_SIZE]; /* reseting the value to the start at this specific day of the month*/
	char			pcSeqNrLen[iMIN_BUF_SIZE]; /* len of a sequence-number*/
	char			pcNumtab_Stopvalue[iMIN_BUF_SIZE]; /* Stop value in numtab*/
	char			pcNumtab_Automaticinsert[iMIN_BUF_SIZE]; /* EO CHANGES  ** Inserts record into Numtab, if not existent, Values YES or NO **/
	UINT			iUseErrorFile; /* should i use error file option? */
	char			pcErrorTimeStampFormat[iMIN]; /* the timestamp format */
	char			pcErrorFileName[iMIN_BUF_SIZE]; /* the error filename */
	char			pcErrorFilePath[iMIN_BUF_SIZE]; /* the error path */
	UINT			iUseSOF; /* should we StartOfFile-String */
	char			pcStartOfFile[iMAX_BUF_SIZE]; /* the SOF-String */
	char			pcStartOfFileOrg[iMAX_BUF_SIZE]; /* the SOF-String */
	UINT			iUseEOF; /* should we use EndOfFile-String? */
	char			pcEndOfFile[iMAX_BUF_SIZE]; /* the EOF-String to use also with STD.-Tokens */
	char			pcEndOfFileOrg[iMAX_BUF_SIZE]; /* the EOF-String */
	UINT			iUseSOB; /* should we use StartOfBlock-String? */
	UINT			iStartOfBlockNumber; /* SOB-String with number ? */
	char			pcStartOfBlock[iMIN]; /* the string */
	UINT			iUseEOB; /* same for EndOfBlock */
	UINT			iEndOfBlockNumber /* same for EndOfBlock */;
	char			pcEndOfBlock[iMIN] /* same for EndOfBlock */;
	UINT			iCharacterSet; /* ANSI or ASCII */
	UINT			iWriteData; /* dynamic or at fix positions */
	char			pcDataSeparator[iMIN]; /* used for dynamic files */
	UINT			iFieldName; /* use field-name? */
	UINT			iFieldNameAsXml; /* use XML field-name tags around field-data */
	UINT			iNoEOLCharacters; /* how many EndOfLine characters? */
	char			pcDataLinePrefix[iMAX_BUF_SIZE]; /* the line prefix */
	char			pcDataLinePostfix[iMAX_BUF_SIZE]; /* the line postfix before eol-sign */
	char			pcEndOfLine[iMIN]; /* the EOL-Characters */
	char			pcCommentSign[iMIN]; /* sign for comment */
	char			pcCommand[iMIN]; /* the command */
	UINT			iDataFromTable; /* internal counter */
	UINT			iDataCounter; /* internal counter */
	int			iNoOfTables; /* count number of tables for this command */
	int			iLineNumberSav; /* buffer for having ongoing line-number if required*/
	TabDef		*prTabDef; /* all table definition */
} HandleCmd;

typedef struct _tfmain
{
	int			iNoCommands; /* number of all commands */
	HandleCmd	*prHandleCmd; /* the command definitions */
} TFMain;

#endif /* _FILEIF_H */

