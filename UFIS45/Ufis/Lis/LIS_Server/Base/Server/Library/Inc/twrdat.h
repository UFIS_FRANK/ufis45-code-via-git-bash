#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Lis/LIS_Server/Base/Server/Library/Inc/twrdat.h 1.2 2004/04/02 20:12:36SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/
/*                                                                           */
/* ABB AAT/I twrdat.h                                                        */
/* UFIS/TWR-AOS                                                              */
/*
 * Implementation of the message format for the TOWER AOS Data Exchange
 * interface specified in the following specification:
 *
 *       ==========================================================
 *                A N A e.p. L I S B O N - P O R T U G A L
 *
 *           Volume :   9007  TOWER System
 *           Chapter :    22  SOFTWARE INTERFACE SPECIFICATION
 *           Section :     2  Tower AOS Data Exchange
 *
 *                               NAV e.p. LISBON [I031xx.SAF]
 *         M.H.G. den Blanken
 *                             DOC_REV 1  2001-06-27  22.2.x/yyy
 *       ==========================================================
 *
 * For other related documents see the actual version of
 *
 *          ABB/AAT ATC Lissabon Tower Handler --Document list
 *          ==================================================
 *
 *                 Last updated: *********** J.Heilig
 *                             Version: ***
 *
 */
/*                                                                           */
/* Author         : Janos Heilig                                             */
/* Creation Date  : Aug-Sep 2001                                             */
/* Description    : data types and function declarations for tower data      */
/*                                                                           */
/* Update history : JHE added JWE's rule on Sep 10 2001                      */
/*                  JWE removed START_UP, replaced ETD by EOBT Sep 14 2001   */
/*                  26 Sep 2001 JHE added declarations for JWE's functions   */
/*                  27 Sep 2001 JHE added TWRDErrorCount()                   */
/*                                                                           */
/*****************************************************************************/
                                                                                
/*
 * $Log: Ufis/Lis/LIS_Server/Base/Server/Library/Inc/twrdat.h  $
 * Revision 1.2 2004/04/02 20:12:36SGT jwe 
 * added mks-version string to sources/headers
 * Revision 1.2 2001/12/14 16:50:12CET jwe 
 * nothing major changed but checking in to keep the archiv up to date with test server state
 * Revision 1.1  2001/09/04 22:15:51  project
 * Initial revision
 *
 * -----
 * $Id: Ufis/Lis/LIS_Server/Base/Server/Library/Inc/twrdat.h 1.2 2004/04/02 20:12:36SGT jwe Exp  $
 */

#ifndef twrdat_included
#define twrdat_included

/*
 * legal types must be valid contiguous indeces start with zero (automation)
 * and the indices must match the columns of the availability table in twrdat.c
 */
  /*M_NONE=-1, ETA, ARR, GO_AROUND, DIV, DIV_ETA, ETD, START_UP, DEP, CANCEL,*/

typedef enum
{
  M_NONE=-1, ETA, ARR, GO_AROUND, DIV, DIV_ETA, EOBT, DEP, CANCEL,
  TG, AOSDEP, AOSARR 
} TWRD_MTYPE;

/*
 * We use fixed size types to avoid the need for creating and destroying
 * objects containing dynamically allocated members. 
 */

typedef struct
{
  short year, month, day;
} TWRD_FDATE, TWRD_DATE;

typedef struct
{
  short hour, min;
} TWRD_CTIME, TWRD_TIME;

typedef struct
{
  TWRD_DATE dat;
  TWRD_CTIME tim;
} TWRD_DATIM;
  
typedef char TWRD_ICAO[5];
typedef char TWRD_CALLSIGN[8];
typedef char TWRD_STDID[5];
typedef char TWRD_AIRCTYP[5];
typedef char TWRD_RMARK[13];
typedef char TWRD_MSG[4];

typedef enum 
{
  R_NONE, FUEL, WIND, VISIBILITY, TECHNICAL, MEDICAL, OTHER
} TWRD_REASON;


#define TWRD_B_msg_seq  0x10000000
#define TWRD_B_mType    0x20000000
#define TWRD_B_datim    0x40000000
#define TWRD_B_fKey     0x00000001
#define TWRD_B_cs       0x00000002
#define TWRD_B_dof      0x00000004
#define TWRD_B_adep     0x00000008
#define TWRD_B_ades     0x00000010
#define TWRD_B_airc     0x00000020
#define TWRD_B_wtc      0x00000040
#define TWRD_B_rwy      0x00000080
#define TWRD_B_flag     0x00000100
#define TWRD_B_stand    0x00000200
#define TWRD_B_regMark  0x00000400
#define TWRD_B_aobd     0x00000800
#define TWRD_B_aobt     0x00001000
#define TWRD_B_firstC   0x00002000
#define TWRD_B_lastC    0x00004000

#define TWRD_B_eta      0x00008000

#define TWRD_B_ata      0x00010000

#define TWRD_B_new_ades 0x00020000
#define TWRD_B_divTime  0x00040000
#define TWRD_B_reason   0x00080000

#define TWRD_B_prv_ades 0x00100000

/*#define TWRD_B_etd      0x00200000*/
#define TWRD_B_eobt      0x00200000
#define TWRD_B_ctot     0x00400000
#define TWRD_B_slotMsg  0x00800000
#define TWRD_B_atd      0x01000000
#define TWRD_B_tgCnt    0x02000000
#define TWRD_B_tgTime   0x04000000
#define TWRD_B_end	    0x00000000

#define TWRD_MAXTGTIME	128

typedef struct
{
/*
 * internal part: magic number and bit field to indicate set data
 */
  unsigned long magic;
  unsigned long flags;
	unsigned long errcnt;
/*
 * message header
 */
  unsigned long msg_seq;
  TWRD_MTYPE mType;
  TWRD_DATIM datim;
/*
 * 'generic' data fields
 */
  unsigned long fKey;
  TWRD_CALLSIGN cs;
  TWRD_FDATE dof;
  TWRD_ICAO adep;
  TWRD_ICAO ades;
  TWRD_AIRCTYP airc;
  char wtc;
  int rwy;
  char flag;
  TWRD_STDID stand;
  TWRD_RMARK regMark;
  TWRD_FDATE aobd;
  TWRD_TIME aobt;
  TWRD_TIME firstC;
  TWRD_TIME lastC;
/*
 * specific message data (aosarr and aosdep have no specific data)
 */
  TWRD_TIME eta;      /* ETA, GO_AROUND */
  TWRD_TIME ata;      /* ARR */
  TWRD_ICAO new_ades; /* DIV */
  TWRD_TIME divTime;  /* DIV, DIV_ETA */
  TWRD_REASON reason; /* DIV, DIV_ETA */
  TWRD_ICAO prv_ades; /* DIV_ETA */
	#if 0
		TWRD_TIME etd;      /* ETD, STARTUP */
		TWRD_TIME ctot;     /* ETD, STARTUP, DEP */
		TWRD_MSG  slotMsg;  /* ETD, START_UP, DEP, CANCEL */
	#endif
  TWRD_TIME eobt;      /* EOBT */
  TWRD_TIME ctot;     /* EOBT, DEP */
  TWRD_MSG  slotMsg;  /* EOBT, DEP, CANCEL */
  TWRD_TIME atd;      /* DEP */
  int tgCnt;          /* TG */
  TWRD_TIME tgTime[TWRD_MAXTGTIME];  /* TG */
} TWRD_MESSAGE;

/*
 * Function declarations
 */

int TWRDInitialize(TWRD_MESSAGE *, TWRD_MTYPE t);
TWRD_MESSAGE *TWRDDecode(char *);
char *TWRDEncode(TWRD_MESSAGE *);
int TWRDGetField(TWRD_MESSAGE *m, unsigned long, void *);
int TWRDSetField(TWRD_MESSAGE *m, unsigned long, unsigned long);
char *TWRDDumpMessage(TWRD_MESSAGE *m);

/*
 * GET and SET macros 
 */

#define TWRD_GET(m, f, d) TWRDGetField((m), TWRD_B_##f, (void *)(d))
#define TWRD_SET(m, f, s) TWRDSetField((m), TWRD_B_##f, (unsigned long)(s))

/*
 * Conversion between 16 char format (F14C) YYYYMMDDhhmmss and FDATE, TIME
 */

int TWRD_F14CToDT(char *, TWRD_FDATE *, TWRD_TIME *);
char *TWRD_DTToF14C(TWRD_FDATE *, TWRD_TIME *);

/*
 * JWE's functions
 */

char *TtUd(TWRD_FDATE *d);
char *TtUt(TWRD_TIME *t);
char *TtCdt(TWRD_FDATE *d, TWRD_TIME *t);
void UdtT(char *pcpCedaDate,TWRD_FDATE *pcpDate);
void UttT(char *pcpCedaDate, TWRD_TIME *pcpTime);

/*
 * Originally, it was private functionality to the TWRDAT module. 
 * But it proved useful for applications scanning messages other than
 * data frames.
 *
 * We use this fixed size var knowing that the max msg size never exceeds it.
 * so we can avoid time consuming dynamic string operations when parsing
 * message strings
 */

#define TWRD_MAXMSG 1401
typedef char TWRD_MSGITEM[TWRD_MAXMSG];
int TWRDGetMsgItems(char **, TWRD_MSGITEM *, int);

/*
 * Function to check message integrity
 */

int TWRDCheckMsg(TWRD_MESSAGE *);
int TWRDErrorCount(TWRD_MESSAGE *);


#endif /* twrdat_included */
