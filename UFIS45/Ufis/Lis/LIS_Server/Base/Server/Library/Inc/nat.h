#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Lis/LIS_Server/Base/Server/Library/Inc/nat.h 1.2 2004/04/02 20:12:35SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/*
 * ABB/AAT New AAT Tools 
 *
 * Author         : Janos Heilig
 * Creation Date  : Aug 17 01
 * Description    : new tools
 *
 * Update history : 
 *
 * $Log: Ufis/Lis/LIS_Server/Base/Server/Library/Inc/nat.h  $
 * Revision 1.2 2004/04/02 20:12:35SGT jwe 
 * added mks-version string to sources/headers
 * Revision 1.2 2001/12/14 16:48:12CET jwe 
 * nothing major changed but anyway checking for having the actual state inside the archiv for lisbon
 * Revision 1.3  2001/08/31 20:45:48  project
 * que sys running at abb, pe no ready
 *
 * Revision 1.2  2001/08/29 20:40:09  project
 * before changing to sigaction
 *
 * Revision 1.1  2001/08/29 19:35:44  project
 * Initial revision
 *
 * -----
 * $Id: Ufis/Lis/LIS_Server/Base/Server/Library/Inc/nat.h 1.2 2004/04/02 20:12:35SGT jwe Exp  $
 */

#ifndef nat_included
#define nat_included

#include <assert.h>
#include <sys/types.h>

/*
 *=== NAT: logging and error handling support
 */

#define NAT_LLIBERR	0x0001
#define NAT_LLIBDBG	0x0002
#define NAT_LERR	 	0x0004
#define NAT_LTRC  	0x0008
#define NAT_LDBG  	0x0010
#define NAT_LAPP  	0x0020

#define NAT_LLMASK	0x00FF

#define NAT_LPID		0x0100
#define	NAT_LTIME		0x0200
#define	NAT_LFLUSH	0x0400
#define	NAT_LSTDERR	0x0800

#define NAT_LOMASK	0x0FFF

#define STR0(x)	(x?x:"(null)")
#define ASSERT(x)	(void)((x)||(nat_Assert(#x, __FILE__, __LINE__)))
#define LOG				NatLog
#define ERRNO			NatErrNo()
#define ERRTEXT		NatErrText()
#define ERRLOG		NAT_LERR
#define TRCLOG		NAT_LTRC
#define DBGLOG		NAT_LDBG
#define DBGLIB		NAT_LLIBDBG

int NatSetLog(int, const char *, const char *);
void NatLog(int, const char*,...);
int nat_Assert(const char*, const char *, int);

int nat_Error(int, char *);
int NatErrNo(void);
char *NatErrText(void);

#define NAT_E_NONE			 0
#define NAT_E_SYSTEM		-1
#define NAT_E_INIT			-2
#define NAT_E_BADFILE		-3
#define NAT_E_BADVAL		-4
#define NAT_E_NOSPACE		-5
#define NAT_E_BADSTAT		-6
#define NAT_E_TIMEOUT		-7
#define NAT_E_TOOBIG		-8
#define NAT_E_DATA			-9

/*
 *=== DynStr: dynamic string utility to handle growing strings 
 */

typedef struct
{
	unsigned int blksz, nblk, nfree, length;
	char *chars;
} DynStr;

DynStr *DynStr_Create(void);
DynStr *DynStr_CreateQ(unsigned int);
void DynStr_Destroy(DynStr *);
void DynStr_AppendStr(DynStr *, char *);
void DynStr_AppendC(DynStr *, char c);
void DynStr_Reset(DynStr *);
#define DynStr_Cpy(d,s) {DynStr_Reset((d));DynStr_AppendStr((d),(s));}
#define DynStr_Cat(d,s) DynStr_AppendStr((d),(s));

/*
 *=== TokenZ: token class to parse string
 */

typedef DynStr TokenZ;

TokenZ *TokenZ_CreateFromString(char **, char *, char *);
void TokenZ_Destroy(TokenZ *);
int TokenZ_GetStrings(char **, char *, char ***);

/*
 *=== QB: QueueBridge
 */

#define QB_SIGNONE	 0
#define QB_SIGSTATIC 1

typedef int QBHandle;

typedef struct
{
	short type;
	long heartbeat;
	short ack_policy;
	key_t inque_key;
	key_t ackque_key;
	short rcvmod_id;
	short sndmod_id;
	char dest_name[10];
	char recv_name[10];
} QBCommCtl;

typedef struct
{
	size_t size;
	short type;
	char data[1];
} QBData;

/* queue end */

QBHandle QBOpen(char *, char **, QBCommCtl *, int);
QBHandle QBOpenCL(char *, QBCommCtl *, int);
int QBWrite(QBHandle, short, char *, size_t, long);
QBHandle QBSelect(void *);
QBData *QBGetMessage(QBHandle, void *);
QBData *QBMakeData(short , char *, size_t);
int QBClose(QBHandle);

/* pipe end */

typedef int (* QBCallback)(QBData *);
int QBSetup(int p, QBCallback);
int QBSendMessage(short t, char *d, size_t l, long to);

typedef short QBShort;
typedef unsigned short QBUShort;
typedef long QBLong;
typedef unsigned long QBULong;
typedef char *QBStringZ;

int QB_PUT_Short(QBShort, char **, int *);
int QB_PUT_UShort(QBUShort, char **, int *);
int QB_PUT_Long(QBLong, char **, int *);
int QB_PUT_ULong(QBULong, char **, int *);
int QB_PUT_StringZ(QBStringZ, char **, int *);

int QB_GET_Short(QBShort *, QBData *, int *);
int QB_GET_UShort(QBUShort *, QBData *, int *);
int QB_GET_Long(QBLong *, QBData *, int *);
int QB_GET_ULong(QBULong *, QBData *, int *);
int QB_GET_StringZ(QBStringZ *, QBData *, int *);

/*
 *=== Event Handling
 */

typedef int EHInputId;
typedef int EHOutputId;
typedef int EHTimerId;
typedef int EHSignalId;

typedef int (* EHInputCB)(int, void *);
typedef int (* EHOutputCB)(int, void *);
typedef int (* EHTimerCB)(void *);
typedef int (* EHSignalCB)(int, void *);

EHInputId EHAddInput(int, EHInputCB, void *);
int EHRemoveInput(EHInputId);

EHInputId EHAddOutput(int, EHInputCB, void *);
int EHRemoveOutput(EHInputId);

EHTimerId EHAddTimer(unsigned long, EHTimerCB, void *);
int EHRemoveTimer(EHTimerId);
int EHResetTimer(EHTimerId);

EHSignalId EHAddSignal(int, EHSignalCB, void *);
int EHRemoveSignal(EHSignalId);

int EHInitialize(void);
int EHHandleEvents(int);

/*
 *=== MultiCasting
 */

#define MC_LOOPBACK	1
#define MC_CONNECT	2
#define MC_BIND			4

typedef void MCSocket;
	
MCSocket *MCSocket_Create(char *, char *, int, int);
int MCSocket_Destroy(MCSocket *);
int MCSocket_Write(MCSocket *, char *, size_t);
int MCSocket_Read(MCSocket *, char *, size_t);
int MCSocket_Read(MCSocket *, char *, size_t);
int MCSocket_ReadAI(MCSocket *, char *, size_t, char **);
int MCSocket_GetSock(MCSocket *);
int MCSocket_SetIf(MCSocket *, char *);

/*
 *=== Block FIFO
 */

typedef struct BlockFIFOEntry_
{
	size_t size;
	void *data;
	struct BlockFIFOEntry_ *link;
} BlockFIFOEntry;

typedef struct
{
	unsigned int count;
	BlockFIFOEntry *pput, *pget;
} BlockFIFO;

BlockFIFO *BlockFIFO_Create(void);
void BlockFIFO_Destroy(BlockFIFO *);
int BlockFIFO_Put(BlockFIFO *, void *, size_t);
void *BlockFIFO_Get(BlockFIFO *, size_t *);
unsigned int BlockFIFO_Count(BlockFIFO *);

/*
 *=== CEDA Internals
 */

#define QB_QCP_INPUT	44
#define QB_QCP_ATTACH	55
	
#endif /* nat_included */

