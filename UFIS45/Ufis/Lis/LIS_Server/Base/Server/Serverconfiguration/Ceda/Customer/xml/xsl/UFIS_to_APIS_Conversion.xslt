<?xml version="1.0" encoding="UTF-8"?>
<!-- edited with XMLSPY v2004 rel. 3 U (http://www.xmlspy.com) by Weerts (ABB Airport Technologies GmbH) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" version="1.0" encoding="UTF-8"/>
	<!-- ######################################################## -->
	<!-- Conversion of UFIS-table column names to agreed node-names           -->
	<!-- ######################################################## -->
	<xsl:template name="UFIS_TO_APIS_CONVERSION">
		<xsl:param name="BO_NAME"/>
		<xsl:param name="NODE_NAME"/>
		<xsl:param name="NODE_DATA"/>
		
		<xsl:choose>
			<xsl:when test="$BO_NAME='XOI'">
				<xsl:choose>
					<xsl:when test="$NODE_NAME='URNO'">&lt;UniqueKey&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/UniqueKey&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='RKEY'">&lt;RotationKey&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/RotationKey&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='FLNO'">&lt;FlightKey&gt;
						&lt;FlightNumber&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/FlightNumber&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='CSGN'">
						&lt;CallSign&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/CallSign&gt;
						&lt;/FlightKey&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='FTYP'">&lt;FlightStatus&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/FlightStatus&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ORG3'">&lt;Origin-IATA&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/Origin-IATA&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ORG4'">&lt;Origin-ICAO&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/Origin-ICAO&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='STOA'">
					&lt;SchArrivalDateTime&gt;
						&lt;SchArrivalDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/SchArrivalDate&gt;
						&lt;SchArrivalTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/SchArrivalTime&gt;
					&lt;/SchArrivalDateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='PSTA'">&lt;StandArr&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/StandArr&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ACT3'">&lt;AircraftType-IATA&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/AircraftType-IATA&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ACT5'">&lt;AircraftType-ICAO&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/AircraftType-ICAO&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ETOA'">
					&lt;EstArrivalDateTime&gt;
						&lt;EstArrivalDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/EstArrivalDate&gt;
						&lt;EstArrivalTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/EstArrivalTime&gt;
					&lt;/EstArrivalDateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='TMOA'">
					&lt;TMODateTime&gt;
						&lt;TMODate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/TMODate&gt;
						&lt;TMOTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/TMOTime&gt;
					&lt;/TMODateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='LAND'">
					&lt;ActArrivalDateTime&gt;
						&lt;ActArrivalDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/ActArrivalDate&gt;
						&lt;ActArrivalTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/ActArrivalTime&gt;
					&lt;/ActArrivalDateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ONBL'">
					&lt;OnBlockDateTime&gt;
						&lt;OnBlockDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/OnBlockDate&gt;
						&lt;OnBlockTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/OnBlockTime&gt;
					&lt;/OnBlockDateTime&gt;
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$BO_NAME='XOO'">
			<!--
			Correctly interpreted events & fields (in that order) from UFIS:
			1. 	URNO,RKEY,FLNO,CSGN,FTYP,ORG3,ORG4,STOA,PSTA,ACT3,ACT5,ETOA,LAND,ONBL 
			 -->
				<xsl:choose>
					<xsl:when test="$NODE_NAME='URNO'">&lt;UniqueKey&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/UniqueKey&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='RKEY'">&lt;RotationKey&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/RotationKey&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='FLNO'">&lt;FlightKey&gt;
						&lt;FlightNumber&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/FlightNumber&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='CSGN'">
						&lt;CallSign&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/CallSign&gt;
						&lt;/FlightKey&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='FTYP'">&lt;FlightStatus&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/FlightStatus&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='DES3'">&lt;Destination-IATA&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/Destination-IATA&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='DES4'">&lt;Destination-ICAO&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/Destination-ICAO&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='STOD'">
					&lt;SchDepartDateTime&gt;
						&lt;SchDepartDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/SchDepartDate&gt;
						&lt;SchDepartTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/SchDepartTime&gt;
					&lt;/SchDepartDateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='PSTD'">&lt;StandDep&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/StandDep&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ACT3'">&lt;AircraftType-IATA&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/AircraftType-IATA&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ACT5'">&lt;AircraftType-ICAO&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/AircraftType-ICAO&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ETDI'">
					&lt;EstDepartDateTime&gt;
						&lt;EstDepartDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/EstDepartDate&gt;
						&lt;EstDepartTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/EstDepartTime&gt;
					&lt;/EstDepartDateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='AIRB'">
					&lt;ActDepartDateTime&gt;
						&lt;ActDepartDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/ActDepartDate&gt;
						&lt;ActDepartTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/ActDepartTime&gt;
					&lt;/ActDepartDateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='OFBL'">
					&lt;OffBlockDateTime&gt;
						&lt;OffBlockDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/OffBlockDate&gt;
						&lt;OffBlockTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/OffBlockTime&gt;
					&lt;/OffBlockDateTime&gt;
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<!-- TOWING -->
			<xsl:when test="$BO_NAME='XOT'">
				<xsl:choose>
					<xsl:when test="$NODE_NAME='URNO'">&lt;UniqueKey&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/UniqueKey&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='RKEY'">&lt;RotationKey&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/RotationKey&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='FLNO'">&lt;FlightKey&gt;
						&lt;FlightNumber&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/FlightNumber&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='CSGN'">
						&lt;CallSign&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/CallSign&gt;
						&lt;/FlightKey&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='FTYP'">&lt;FlightStatus&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/FlightStatus&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='STOA'">
					&lt;SchArrivalDateTime&gt;
						&lt;SchArrivalDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/SchArrivalDate&gt;
						&lt;SchArrivalTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/SchArrivalTime&gt;
					&lt;/SchArrivalDateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='STOD'">
					&lt;SchDepartDateTime&gt;
						&lt;SchDepartDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/SchDepartDate&gt;
						&lt;SchDepartTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/SchDepartTime&gt;
					&lt;/SchDepartDateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='PSTA'">&lt;StandArr&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/StandArr&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='PSTD'">&lt;StandDep&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/StandDep&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ACT3'">&lt;AircraftType-IATA&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/AircraftType-IATA&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ACT5'">&lt;AircraftType-ICAO&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/AircraftType-ICAO&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ETOA'">
					&lt;EstArrivalDateTime&gt;
						&lt;EstArrivalDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/EstArrivalDate&gt;
						&lt;EstArrivalTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/EstArrivalTime&gt;
					&lt;/EstArrivalDateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ETDI'">
					&lt;EstDepartDateTime&gt;
						&lt;EstDepartDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/EstDepartDate&gt;
						&lt;EstDepartTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/EstDepartTime&gt;
					&lt;/EstDepartDateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='LAND'">
					&lt;ActArrivalDateTime&gt;
						&lt;ActArrivalDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/ActArrivalDate&gt;
						&lt;ActArrivalTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/ActArrivalTime&gt;
					&lt;/ActArrivalDateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='AIRB'">
					&lt;ActDepartDateTime&gt;
						&lt;ActDepartDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/ActDepartDate&gt;
						&lt;ActDepartTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/ActDepartTime&gt;
					&lt;/ActDepartDateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ONBL'">
					&lt;OnBlockDateTime&gt;
						&lt;OnBlockDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/OnBlockDate&gt;
						&lt;OnBlockTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/OnBlockTime&gt;
					&lt;/OnBlockDateTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='OFBL'">
					&lt;OffBlockDateTime&gt;
						&lt;OffBlockDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/OffBlockDate&gt;
						&lt;OffBlockTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/OffBlockTime&gt;
					&lt;/OffBlockDateTime&gt;
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>
</xsl:stylesheet>
