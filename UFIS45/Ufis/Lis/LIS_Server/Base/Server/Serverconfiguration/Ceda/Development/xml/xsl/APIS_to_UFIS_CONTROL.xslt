<?xml version="1.0" encoding="UTF-8"?>
<!-- edited with XMLSPY v2004 rel. 3 U (http://www.xmlspy.com) by Weerts (ABB Airport Technologies GmbH) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" media-type="text" version="1.0"/>
	<!-- ################################## -->
	<!-- Embedded Templates for business objects  -->
	<!-- ################################## -->
	<!-- Template for INB objects -->
	<xsl:template name="INC_CONTROL">
			<xsl:variable name="TYPE">
				<xsl:value-of select="./TYPE"/>
			</xsl:variable>
			<xsl:if test="$TYPE='SYNC'">
			<xsl:text>&lt;CMD&gt;SYN&lt;/CMD&gt;&lt;ROUTE&gt;7800&lt;/ROUTE&gt;</xsl:text>
			</xsl:if>
			<xsl:if test="$TYPE='KEEPALIVE'">
				<xsl:text>&lt;CMD&gt;ALV&lt;/CMD&gt;&lt;ROUTE&gt;0815&lt;/ROUTE&gt;</xsl:text>
			</xsl:if>
	</xsl:template>
</xsl:stylesheet>
