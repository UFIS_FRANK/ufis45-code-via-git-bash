update pagtab set cotb = '1' where pcfn = 'AR_64_48.CFG';
update pagtab set cotb = '1' where pcfn = 'AR_76_12.CFG';
update pagtab set cotb = '1' where pcfn = 'DC_76_12.CFG';
update pagtab set cotb = '1' where pcfn = 'DG_12_76.CFG';
update pagtab set cotb = '1' where pcfn = 'DG_64_48.CFG';
update pagtab set cotb = '1' where pcfn = 'DG_76_12.CFG';
update pagtab set cotb = '1' where pcfn = 'E_76_12.CFG';
update pagtab set cotb = '1' where pcfn = 'E_12_76.CFG';
update pagtab set cotb = '1' where pcfn = 'DEP_T2.CFG';
update pagtab set cotb = '1' where pcfn = 'GAT_LS.CFG';
update pagtab set cotb = '1' where pcfn = 'GAT_DEF.CFG';

update pagtab set cotb = '2' where pcfn = 'AR_BT_L.CFG';
update pagtab set cotb = '2' where pcfn = 'AR_ET.CFG';
update pagtab set cotb = '2' where pcfn = 'AR_ET_L.CFG';
update pagtab set cotb = '2' where pcfn = 'DP_C.CFG';
update pagtab set cotb = '2' where pcfn = 'DP_C_L.CFG';
update pagtab set cotb = '2' where pcfn = 'DP_G.CFG';
update pagtab set cotb = '2' where pcfn = 'DP_G_L.CFG';
update pagtab set cotb = '2' where pcfn = 'BAG_SUM.CFG';
update pagtab set cotb = '2' where pcfn = 'BAGSUMTF.CFG';
update pagtab set cotb = '2' where pcfn = 'DC169TFT.CFG';
update pagtab set cotb = '2' where pcfn = 'DG169TFT.CFG';
update pagtab set cotb = '2' where pcfn = 'ARR_C_LS.CFG';
update pagtab set cotb = '2' where pcfn = 'ARR_C_PT.CFG';
update pagtab set cotb = '2' where pcfn = 'DEP_C_LS.CFG';
update pagtab set cotb = '2' where pcfn = 'DEP_C_PT.CFG';
update pagtab set cotb = '2' where pcfn = 'ST_ARR.CFG';
update pagtab set cotb = '2' where pcfn = 'ST_DEP.CFG';
update pagtab set cotb = '2' where pcfn = 'ARR_LS.CFG';
update pagtab set cotb = '2' where pcfn = 'ARRBLTLS.CFG';
update pagtab set cotb = '2' where pcfn = 'ARR_PT.CFG';
update pagtab set cotb = '2' where pcfn = 'DEP_LS.CFG';
update pagtab set cotb = '2' where pcfn = 'DG_LS.CFG';
update pagtab set cotb = '2' where pcfn = 'DC_LS.CFG';


update pagtab set cotb = '3' where pcfn = 'BLT1_L.CFG';
update pagtab set cotb = '3' where pcfn = 'BLT2_L.CFG';
update pagtab set cotb = '3' where pcfn = 'DEF_BLT2.CFG';

update pagtab set cotb = '4' where pcfn = 'CKI_C1.CFG';
update pagtab set cotb = '4' where pcfn = 'CKI_C2.CFG';
update pagtab set cotb = '4' where pcfn = 'CKI_D1.CFG';
update pagtab set cotb = '4' where pcfn = 'CKI_D2.CFG';
update pagtab set cotb = '4' where pcfn = 'DEF_TP1.CFG';
update pagtab set cotb = '4' where pcfn = 'DEF_TP2.CFG';
update pagtab set cotb = '4' where pcfn = 'DEF_TP3.CFG';
update pagtab set cotb = '4' where pcfn = 'DEF_TP4.CFG';
update pagtab set cotb = '4' where pcfn = 'DEF_TP5.CFG';
update pagtab set cotb = '4' where pcfn = 'DEF_TP6.CFG';
update pagtab set cotb = '4' where pcfn = 'DEF_PAG.CFG';
update pagtab set cotb = '4' where pcfn = 'DEF_LK1.CFG';
update pagtab set cotb = '4' where pcfn = 'DEF_LK2.CFG';

update pagtab set cotb = '5' where pcfn = 'GAT_L.CFG';
update pagtab set cotb = '5' where pcfn = 'GAT_R.CFG';

commit;

