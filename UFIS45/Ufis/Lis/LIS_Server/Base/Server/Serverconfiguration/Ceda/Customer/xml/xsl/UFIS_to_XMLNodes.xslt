<?xml version="1.0" encoding="UTF-8"?>
<!-- edited with XMLSPY v2004 rel. 3 U (http://www.xmlspy.com) by Weerts (ABB Airport Technologies GmbH) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output  method="text" version="1.0" encoding="UTF-8" indent="no"/>
	<xsl:include href="UFIS_to_APIS_Conversion.xslt"/>
	<xsl:template match="UFISXML">&lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;MSG&gt;
		<xsl:variable name="BO_NAME" select="CMD"/>
			<xsl:text>
				&lt;HEAD&gt;
					&lt;MSGID&gt;&lt;/MSGID&gt;
					&lt;MSGORIG&gt;UFIS&lt;/MSGORIG&gt;
					&lt;MSGACTION&gt;</xsl:text>
						<xsl:value-of select="CMD"/>
					<xsl:text>&lt;/MSGACTION&gt;</xsl:text>
					&lt;MSGGENDATETIME&gt;
						&lt;MSGGENDATE&gt;<xsl:value-of select="MSGGENDATETIME/MSGGENDATE"/>&lt;/MSGGENDATE&gt;
						&lt;MSGGENTIME&gt;<xsl:value-of select="MSGGENDATETIME/MSGGENTIME"/>&lt;/MSGGENTIME&gt;
					&lt;/MSGGENDATETIME&gt;
				&lt;/HEAD&gt;
				&lt;BODY&gt;
				<!-- process single event from UFIS-ACTION, etc. -->
				<xsl:if test="contains('XOI XOO XOT ALV SYNR', $BO_NAME)">
					<xsl:if test="$BO_NAME='XOI'">&lt;INB&gt;</xsl:if>
					<xsl:if test="$BO_NAME='XOO'">&lt;OUTB&gt;</xsl:if>
					<xsl:if test="$BO_NAME='XOT'">&lt;TOW&gt;</xsl:if>
					<xsl:if test="$BO_NAME='ALV'">&lt;CONTROL&gt;&lt;TYPE&gt;KEEPALIVE</xsl:if>
					<xsl:if test="$BO_NAME='SYNR'">&lt;CONTROL&gt;&lt;TYPE&gt;SYNC</xsl:if>
					<xsl:for-each select="*">
						<xsl:if test="name(.)='DATA'">
							<xsl:variable name="FLDLINE"><xsl:value-of select="../FIELDS"/></xsl:variable>
							<xsl:variable name="DATLINE"><xsl:value-of select="."/></xsl:variable>
							<xsl:call-template name="UFIS_FLD_DAT_REC_TO_XML">
								<xsl:with-param name="BO_NAME">
									<xsl:value-of select="$BO_NAME"/>
								</xsl:with-param>
								<xsl:with-param name="FLDLIST" select="$FLDLINE"/>
								<xsl:with-param name="DATLIST" select="$DATLINE"/>
							</xsl:call-template>
						</xsl:if>
					</xsl:for-each>
					<xsl:if test="$BO_NAME='XOI'">&lt;/INB&gt;</xsl:if>
					<xsl:if test="$BO_NAME='XOO'">&lt;/OUTB&gt;</xsl:if>
					<xsl:if test="$BO_NAME='XOT'">&lt;/TOW&gt;</xsl:if>
					<xsl:if test="$BO_NAME='ALV'">&lt;/TYPE&gt;&lt;/CONTROL&gt;</xsl:if>
					<xsl:if test="$BO_NAME='SYNR'">&lt;/TYPE&gt;&lt;/CONTROL&gt;</xsl:if>
				</xsl:if>
				<!-- process sync-answer (SYNA) from UFIS-FILEIF input  -->
				<xsl:if test="$BO_NAME='SYNA'">
					<xsl:for-each select="*">
						<xsl:if test="name(.)='DATA'">
							<xsl:variable name="FIRSTFIELD"><xsl:value-of select="substring-before(.,',')"/></xsl:variable>
							<xsl:variable name="RESTOFSTRING"><xsl:value-of select="substring-after(.,',')"/></xsl:variable>
							<xsl:variable name="TYPE"><xsl:value-of select="substring-before($RESTOFSTRING,',')"/></xsl:variable>
							<xsl:variable name="DATLINE"><xsl:value-of select="substring-after($RESTOFSTRING,',')"/></xsl:variable>
							<xsl:if test="$TYPE='A'">&lt;INB&gt;
							<xsl:call-template name="UFIS_DAT_REC_TO_XML">
								<xsl:with-param name="BO_NAME">XOI</xsl:with-param>
								<xsl:with-param name="DATLIST" select="$DATLINE"/>
							</xsl:call-template>
							</xsl:if>
							<xsl:if test="$TYPE='D'">&lt;OUTB&gt;
							<xsl:call-template name="UFIS_DAT_REC_TO_XML">
								<xsl:with-param name="BO_NAME">XOO</xsl:with-param>
								<xsl:with-param name="DATLIST" select="$DATLINE"/>
							</xsl:call-template>
							</xsl:if>
							<xsl:if test="$TYPE='B'">&lt;TOW&gt;
							<xsl:call-template name="UFIS_DAT_REC_TO_XML">
								<xsl:with-param name="BO_NAME">XOT</xsl:with-param>
								<xsl:with-param name="DATLIST" select="$DATLINE"/>
							</xsl:call-template>
							</xsl:if>
							<xsl:if test="$TYPE='A'">&lt;/INB&gt;</xsl:if>
							<xsl:if test="$TYPE='D'">&lt;/OUTB&gt;</xsl:if>
							<xsl:if test="$TYPE='B'">&lt;/TOW&gt;</xsl:if>
						</xsl:if>
					</xsl:for-each>
				</xsl:if>
	&lt;/BODY&gt;&lt;/MSG&gt;</xsl:template>	
	<!-- process a single line of data which is between <DATA>....</DATA> -->
	<!-- template for transforming an UFISXML event with fieldnames and data inside the data list (<DATA>ADID,A,URNO,4711....</DATA>) -->
	<xsl:template name="UFIS_DAT_REC_TO_XML">
		<xsl:param name="BO_NAME"/>
		<xsl:param name="DATLIST"/>
		<xsl:if test="string-length($DATLIST) &gt; 0">
			<xsl:variable name="FLD"><xsl:value-of select="substring-before($DATLIST,',')"/></xsl:variable>
			<xsl:variable name="RESTOFSTRING"><xsl:value-of select="substring-after($DATLIST,',')"/></xsl:variable>
			<xsl:variable name="DAT"><xsl:value-of select="substring-before($RESTOFSTRING,',')"/></xsl:variable>
			<xsl:variable name="DATLINE"><xsl:value-of select="substring-after($RESTOFSTRING,',')"/></xsl:variable>
			<xsl:choose>
				<!-- first too last field -1 -->
					<xsl:when test="string-length($FLD) &gt; 0">
					<xsl:call-template name="UFIS_TO_APIS_CONVERSION">
						<xsl:with-param name="BO_NAME">
							<xsl:value-of select="$BO_NAME"/>
						</xsl:with-param>
						<xsl:with-param name="NODE_NAME" select="$FLD"/>
						<xsl:with-param name="NODE_DATA" select="$DAT"/>
					</xsl:call-template>
				</xsl:when>
				<!-- last field in list -->
				<xsl:when test="string-length($FLD) = 0">
					<!-- replace comma's at the end of a line to avoid comm's being used  as data content -->
					<xsl:variable name="DATLIST2" select="translate($DATLIST,',','')"/>
					<xsl:call-template name="UFIS_TO_APIS_CONVERSION">
						<xsl:with-param name="BO_NAME" select="$BO_NAME"/>
						<xsl:with-param name="NODE_NAME" select="$FLD"/>
						<xsl:with-param name="NODE_DATA" select="$DATLIST2"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					ERROR
				</xsl:otherwise>
			</xsl:choose>
			<!-- call myself with the rest of the list until the end of the list -->
			<xsl:call-template name="UFIS_DAT_REC_TO_XML">
				<xsl:with-param name="BO_NAME" select="$BO_NAME"/>
				<xsl:with-param name="DATLIST" select="$DATLINE"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<!-- template for transforming an UFISXML event with single FIELD-list and according DATA-List -->
	<xsl:template name="UFIS_FLD_DAT_REC_TO_XML">
		<xsl:param name="BO_NAME"/>
		<xsl:param name="FLDLIST"/>
		<xsl:param name="DATLIST"/>
		<xsl:if test="string-length($FLDLIST) &gt; 0">
			<xsl:variable name="FLD" select="substring-before($FLDLIST,',')"/>
			<xsl:variable name="DAT" select="substring-before($DATLIST,',')"/>
			<xsl:choose>
				<!-- first too last field -1 -->
					<xsl:when test="string-length($FLD) &gt; 0">
					<xsl:call-template name="UFIS_TO_APIS_CONVERSION">
						<xsl:with-param name="BO_NAME">
							<xsl:value-of select="$BO_NAME"/>
						</xsl:with-param>
						<xsl:with-param name="NODE_NAME" select="$FLD"/>
						<xsl:with-param name="NODE_DATA" select="$DAT"/>
					</xsl:call-template>
				</xsl:when>
				<!-- last field in list -->
				<xsl:when test="string-length($FLD) = 0">
					<!-- replace comma's at the end of a line to avoid comm's being used  as data content -->
					<xsl:variable name="DATLIST2" select="translate($DATLIST,',','')"/>
					<xsl:call-template name="UFIS_TO_APIS_CONVERSION">
						<xsl:with-param name="BO_NAME" select="$BO_NAME"/>
						<xsl:with-param name="NODE_NAME" select="$FLDLIST"/>
						<xsl:with-param name="NODE_DATA" select="$DATLIST2"/>
					</xsl:call-template>
					<!-- TAG now generated inside the substylesheet 
					&lt;/<xsl:value-of select="$BO_NAME"/>&gt;
					-->
				</xsl:when>
				<xsl:otherwise>
					ERROR
				</xsl:otherwise>
			</xsl:choose>
			<!-- call myself with the rest of the list until the end of the list -->
			<xsl:call-template name="UFIS_FLD_DAT_REC_TO_XML">
				<xsl:with-param name="BO_NAME" select="$BO_NAME"/>
				<xsl:with-param name="FLDLIST" select="substring-after($FLDLIST,',')"/>
				<xsl:with-param name="DATLIST" select="substring-after($DATLIST,',')"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template match="*"></xsl:template>
</xsl:stylesheet>
