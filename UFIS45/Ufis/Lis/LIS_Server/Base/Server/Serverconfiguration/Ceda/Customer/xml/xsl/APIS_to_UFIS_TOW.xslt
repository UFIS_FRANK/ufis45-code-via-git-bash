<?xml version="1.0" encoding="UTF-8"?>
<!-- edited with XMLSPY v2004 rel. 3 U (http://www.xmlspy.com) by Weerts (ABB Airport Technologies GmbH) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" media-type="text" version="1.0"/>
	<!-- ################################## -->
	<!-- Embedded Templates for business objects  -->
	<!-- ################################## -->
	<!-- Template for INB objects -->
	<xsl:template name="INC_TOW">
			<xsl:text>
			&lt;CMD&gt;UFR&lt;/CMD&gt;&lt;ROUTE&gt;7805&lt;/ROUTE&gt;
			&lt;SELECTION&gt;WHERE URNO=</xsl:text><xsl:value-of select="normalize-space(./UniqueKey)"/>
			<xsl:text>&lt;/SELECTION&gt;	</xsl:text>
			<xsl:variable name="ONBS"><xsl:value-of select="name(./OnBlockDateTime)"/></xsl:variable><xsl:variable name="OFBS"><xsl:value-of select="name(./OffBlockDateTime)"/></xsl:variable><!-- ON and OFF-BLOCK are set --><xsl:if test="$ONBS='OnBlockDateTime' and $OFBS='OffBlockDateTime'">&lt;FIELDS&gt;ONBS,OFBS&lt;/FIELDS&gt;&lt;DATA&gt;<xsl:value-of select="./OnBlockDateTime/OnBlockDate"/><xsl:value-of select="./OnBlockDateTime/OnBlockTime"/>,<xsl:value-of select="./OffBlockDateTime/OffBlockDate"/><xsl:value-of select="./OffBlockDateTime/OffBlockTime"/>&lt;/DATA&gt;</xsl:if><!-- ON-BLOCK only is set --><xsl:if test="$ONBS='OnBlockDateTime' and $OFBS=''">&lt;FIELDS&gt;ONBS&lt;/FIELDS&gt;&lt;DATA&gt;<xsl:value-of select="./OnBlockDateTime/OnBlockDate"/><xsl:value-of select="./OnBlockDateTime/OnBlockTime"/>&lt;/DATA&gt;	</xsl:if><!-- OFF-BLOCK only is set --><xsl:if test="$OFBS='OffBlockDateTime' and $ONBS=''">&lt;FIELDS&gt;OFBS&lt;/FIELDS&gt;&lt;DATA&gt;<xsl:value-of select="./OffBlockDateTime/OffBlockDate"/><xsl:value-of select="./OffBlockDateTime/OffBlockTime"/>&lt;/DATA&gt;</xsl:if></xsl:template></xsl:stylesheet>
