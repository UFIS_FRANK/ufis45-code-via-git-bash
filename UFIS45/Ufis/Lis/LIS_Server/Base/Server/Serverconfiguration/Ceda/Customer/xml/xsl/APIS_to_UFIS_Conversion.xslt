<?xml version="1.0"?>
<!-- edited with XMLSPY v2004 rel. 3 U (http://www.xmlspy.com) by Weerts (ABB Airport Technologies GmbH) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" version="1.0" indent="no"/>
	<!-- ############# -->
	<!-- Global variables -->
	<!-- ############# -->
	<xsl:variable name="HOPO">LIS</xsl:variable>

	<!-- #################################### -->
	<!-- Calling routines for business object templates -->
	<!-- #################################### -->
	<xsl:include href="APIS_to_UFIS_INB.xslt"/>
	<xsl:include href="APIS_to_UFIS_OUTB.xslt"/>
	<xsl:include href="APIS_to_UFIS_TOW.xslt"/>
	<xsl:include href="APIS_to_UFIS_STANDINFO.xslt"/>
	<xsl:include href="APIS_to_UFIS_CONTROL.xslt"/>	
	

	<!-- ################################## -->
	<!-- Embedded Templates for business objects  -->
	<!-- ################################## -->
	<xsl:template match="MSG">
		<xsl:text>&lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;UFISXML&gt;</xsl:text>
		<xsl:for-each select="*">
			<xsl:choose>
				<xsl:when test="name(.)='BODY' and element-available('HEAD')=TRUE">
				<xsl:text>
				&lt;!-- START OF NOT USED UFISXML-TAGS --&gt;
				&lt;DEST&gt;&lt;/DEST&gt;
				&lt;BCORIGINATOR&gt;&lt;/BCORIGINATOR&gt;
				&lt;WKS&gt;&lt;/WKS&gt;
				&lt;BCNUM&gt;&lt;/BCNUM&gt;
				&lt;SEQID&gt;&lt;/SEQID&gt;
				&lt;TOTALBUF&gt;&lt;/TOTALBUF&gt;
				&lt;ACTBUF&gt;&lt;/ACTBUF&gt;
				&lt;RSEQID&gt;&lt;/RSEQID&gt;
				&lt;RC&gt;&lt;/RC&gt;
				&lt;TOTALSIZE&gt;&lt;/TOTALSIZE&gt;
				&lt;CMDSIZE&gt;&lt;/CMDSIZE&gt;
				&lt;DATASIZE&gt;&lt;/DATASIZE&gt;
				&lt;OBJECT&gt;&lt;/OBJECT&gt;
				&lt;ORDER&gt;&lt;/ORDER&gt;
				&lt;TWSTART&gt;&lt;/TWSTART&gt;
				&lt;TWEND&gt;&lt;/TWEND&gt;
				&lt;FUNCTION&gt;&lt;/FUNCTION&gt;
				&lt;ITORIGINATOR&gt;&lt;/ITORIGINATOR&gt;
				&lt;ITPRIORITY&gt;3&lt;/ITPRIORITY&gt;
				&lt;MSGLENGTH&gt;&lt;/MSGLENGTH&gt;
				&lt;ORIGINATOR&gt;&lt;/ORIGINATOR&gt;
				&lt;BLOCKNR&gt;&lt;/BLOCKNR&gt;
				&lt;EVENT_COMMAND&gt;&lt;/EVENT_COMMAND&gt;
				&lt;EVENT_TYPE&gt;&lt;/EVENT_TYPE&gt;
				&lt;!-- START OF RELEVANT UFISXML-TAGS --&gt;</xsl:text>
				<xsl:for-each select="*">
						<xsl:if test="name(.)='INB'">
							<xsl:call-template name="INC_INB">
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="name(.)='OUTB'">
							<xsl:call-template name="INC_OUTB">
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="name(.)='TOW'">
							<xsl:call-template name="INC_TOW">
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="name(.)='STANDINFO'">
							<xsl:call-template name="INC_STANDINFO">
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="name(.)='CONTROL'">
							<xsl:call-template name="INC_CONTROL">
							</xsl:call-template>
						</xsl:if>
					</xsl:for-each>
				</xsl:when>
			</xsl:choose>
		</xsl:for-each>
		<xsl:text>&lt;/UFISXML&gt;</xsl:text>
	</xsl:template>
</xsl:stylesheet>
