<?xml version="1.0" encoding="UTF-8"?>
<!-- edited with XMLSPY v2004 rel. 3 U (http://www.xmlspy.com) by Weerts (ABB Airport Technologies GmbH) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" version="1.0" encoding="UTF-8"/>
	<!-- ######################################################## -->
	<!-- Conversion of UFIS-table column names to agreed node-names           -->
	<!-- ######################################################## -->
	<xsl:template name="UFIS_TO_DFW_CONVERSION">
		<xsl:param name="BO_NAME"/>
		<xsl:param name="NODE_NAME"/>
		<xsl:param name="NODE_DATA"/>
		<xsl:param name="DO_RAO"/>
		
		<xsl:choose>
			<xsl:when test="$BO_NAME='FMO'">
			<!--
			Correctly interpreted events & fields (in that order) from UFIS:
			1. 	URNO, ALC3, FLNO, CSGN, JFNO, ACT3, REGN, TTYP, ORG3, DES3, VIAL, ADID, FTYP, STOA, ETOA, LAND, STOD, ETOD, AIRB, FLDA,REMP,REM1, BLT1, B1BS, B1ES, B1BA, B1EA, RKEY
 			 -->
				<xsl:choose>
					<xsl:when test="$NODE_NAME='ALC3'">	
						&lt;AirlineOperator&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/AirlineOperator&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='FLNO'">	
						&lt;FlightNumber&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/FlightNumber&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='CSGN'">	
						&lt;CallSign&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/CallSign&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='JFNO'">	
						&lt;CodeShareNumber&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/CodeShareNumber&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='ACT3'">	
						&lt;AircraftType&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/AircraftType&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='REGN'">	
						&lt;AircraftRegistrationNum&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/AircraftRegistrationNum&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='TTYP'">	
						&lt;FlightType&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/FlightType&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='FLTI'">	
						&lt;FlightIntDom&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/FlightIntDom&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='ORG3'">	
						&lt;Origin&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/Origin&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='DES3'">	
						&lt;Destination&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/Destination&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='VIAL'">	
						&lt;Vias&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/Vias&gt;</xsl:when>
					
					<xsl:when test="$NODE_NAME='ADID'">	
						&lt;Arrive-Depart&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/Arrive-Depart&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='FTYP'">	
						&lt;Status&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/Status&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='STOA'">	
						&lt;SchArrivalDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/SchArrivalDate&gt;
						&lt;SchArrivalTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/SchArrivalTime&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='ETOA'">	
						&lt;EstArrivalDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/EstArrivalDate&gt;
						&lt;EstArrivalTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/EstArrivalTime&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='LAND'">	
						&lt;ActArrivalDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/ActArrivalDate&gt;
						&lt;ActArrivalTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/ActArrivalTime&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='STOD'">	
						&lt;SchDepartDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/SchDepartDate&gt;
						&lt;SchDepartTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/SchDepartTime&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='ETOD'">	
						&lt;EstDepartDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/EstDepartDate&gt;
						&lt;EstDepartTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/EstDepartTime&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='AIRB'">	
						&lt;ActDepartDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/ActDepartDate&gt;
						&lt;ActDepartTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/ActDepartTime&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='FLDA'">	
						&lt;DateOfOperation&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/DateOfOperation&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='REMP'">	
						&lt;PublicRemarks&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/PublicRemarks&gt;</xsl:when>
					<xsl:when test="$NODE_NAME='REM1'">	
						&lt;PrivateRemarks&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/PrivateRemarks&gt;
					</xsl:when>
					
					<!-- Location relevant data (BELT,GATE,STAND) -->
					<xsl:when test="$DO_RAO='TRUE'">
					<xsl:choose>
					<!-- setting of according resource type -->
					<xsl:when test="$NODE_NAME='BLT1' or $NODE_NAME='GTA1' or $NODE_NAME='PSTA' or  $NODE_NAME='GTD1' or  $NODE_NAME='PSTD'">
						<xsl:choose>
								<xsl:when test="$NODE_NAME='BLT1'">&lt;ResourceType&gt;BELT&lt;/ResourceType&gt;</xsl:when>
								<xsl:when test="$NODE_NAME='GTA1' or $NODE_NAME='GTD1'">																&lt;ResourceType&gt;GATE&lt;/ResourceType&gt;</xsl:when>
								<xsl:when test="$NODE_NAME='PSTA' or $NODE_NAME='PSTD'">																&lt;ResourceType&gt;STAND&lt;/ResourceType&gt;</xsl:when>
							</xsl:choose>
							<!-- setting of according resource-id's according to UFIS values -->
							&lt;SchResourceID&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/SchResourceID&gt;
							&lt;ActResourceID&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/ActResourceID&gt;
						</xsl:when>
						<!-- setting of according times according to UFIS values -->
						<xsl:when test="$NODE_NAME='B1BS' or $NODE_NAME='GA1B' or $NODE_NAME='GD1B' or $NODE_NAME='PABS' or $NODE_NAME='PDBS'">
							&lt;SchStartDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/SchStartDate&gt;
							&lt;SchStartTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/SchStartTime&gt;
						</xsl:when>
						<xsl:when test="$NODE_NAME='B1ES' or $NODE_NAME='GA1E' or $NODE_NAME='GD1E' or $NODE_NAME='PAES' or $NODE_NAME='PDES'">
							&lt;SchEndDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/SchEndDate&gt;
							&lt;SchEndTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/SchEndTime&gt;
						</xsl:when>
						<xsl:when test="$NODE_NAME='B1BA' or $NODE_NAME='GA1X' or $NODE_NAME='GD1X' or $NODE_NAME='PABA' or $NODE_NAME='PDBA'">
							&lt;ActStartDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/ActStartDate&gt;
							&lt;ActStartTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/ActStartTime&gt;
						</xsl:when>
						<xsl:when test="$NODE_NAME='B1EA' or $NODE_NAME='GA1Y' or $NODE_NAME='GD1Y' or $NODE_NAME='PAEA' or $NODE_NAME='PDEA'">
							&lt;ActEndDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/ActEndDate&gt;
							&lt;ActEndTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/ActEndTime&gt;
						</xsl:when>
						</xsl:choose>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<!--
				<xsl:when test="$BO_NAME='TSO'">
				<xsl:choose>
					<xsl:when test="$NODE_NAME='USER'">
						&lt;User&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/User&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='ORGN'">
						&lt;Organization&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/Organization&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='TERM'">
						&lt;TerminalID&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/TerminalID&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='LITS'">
						&lt;LoginDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/LoginDate&gt;
						&lt;LoginTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/LoginTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='LOTS'">
						&lt;LogoutDate&gt;<xsl:value-of select="substring($NODE_DATA,1,8)"/>&lt;/LogoutDate&gt;
						&lt;LogoutTime&gt;<xsl:value-of select="substring($NODE_DATA,9,14)"/>&lt;/LogoutTime&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='UDUR'">
						&lt;UsageDuration&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/UsageDuration&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='PAXP'">
						&lt;PassengersProcessed&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/PassengersProcessed&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='RETR'">
						&lt;RemoteTransactions&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/RemoteTransactions&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='LOTR'">
						&lt;LocalTransactions&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/LocalTransactions&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='BPPR'">
						&lt;BoardingPassesPrinted&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/BoardingPassesPrinted&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='BTPR'">
						&lt;BagTagsPrinted&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/BagTagsPrinted&gt;
					</xsl:when>
					<xsl:when test="$NODE_NAME='REPR'">
						&lt;ReportsPrinted&gt;<xsl:value-of select="$NODE_DATA"/>&lt;/ReportsPrinted&gt;
					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			-->
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>