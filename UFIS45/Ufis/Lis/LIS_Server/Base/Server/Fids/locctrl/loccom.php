<?php
// cedaphpif.php = main communication channel for PHP to communicate
//                 with CEDA using the cput-program for synchronus processing
//                 of request/commands.
include("../generic/cedaphpif.php");
$RES="";
$RC=0;
// if debug is set to true then the Load() function should be disabled if the results should be seen inside the browser
//$debug="TRUE";

if (!empty($REC) && !empty($ALC3) && !empty($LOCTYPE))
{
	switch ($LOCTYPE)
	{
		case "COUNTER":
			$Dest="sqlhdl4";
			$Prio="3";
			$Command="URT";
			$Table="CCATAB";
			$Selection="WHERE URNO='".$REC."'";
			$Timeout="30";
			$SubCmd="";
			$FREEREMARK = strtoupper(trim($FREEREMARK));

			trim($PREREMARK);
			if (!empty($FREEREMARK))
			{
				$REMARK=$FREEREMARK;
			}
			else
			{
				$REMARK=$PREREMARK;
			}
			switch ($COUNTERSTAT)
			{
				case "OPEN":
					$CCA_CKEA_UTC_NEW=" ";
					if (strlen(trim($OPENTIME)) <= 0)
					{
						$CCA_CKBA_UTC_NEW=gmdate("YmdHis");
						$Fields="CKBA,CKEA,REMA,DISP";
						$Data="".$CCA_CKBA_UTC_NEW.",".$CCA_CKEA_UTC_NEW.",".$LOGO.",".$REMARK;
					}
					else
					{
						$Fields="CKEA,REMA,DISP";
						$Data=$CCA_CKEA_UTC_NEW.",".$LOGO.",".$REMARK;
					}
					$result = CallCput(TRACE,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$SubCmd);
					$RES=$RES."<font color=green>Counter open request successfully processed.<br></font>";
					break;
				case "CLOSED":
					$CCA_CKEA_UTC_NEW=gmdate("YmdHis");
					$Fields="CKEA,REMA,DISP";
					$Data="".$CCA_CKEA_UTC_NEW.",".$LOGO.",".$REMARK;
					$result = CallCput(TRACE,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$SubCmd);
					$RES=$RES."<font color=green>Counter close request successfully processed.<br></font>";
					break;
				default: 
					$RES=$RES."<font color=red>ERROR: Invalid counter status received!<br></font>";
					break;
			}

			if (empty($DAYVAL))
			{
				$DAYVAL='+0';
			}
			if (empty($AFT_BOAO_HH) && empty($AFT_BOAO_MI)) 
			{
						$RES=$RES."<font color=green>Cleaned boarding time.<br></font>";
						$RC=-1;
			}
			else
			{
					if (empty($AFT_BOAO_HH)) 
					{
						$AFT_BOAO_HH="00";
					}
					if (empty($AFT_BOAO_MI)) 
					{
						$AFT_BOAO_MI="00";
					}
					/* check input values for boarding time */
					if (!ereg("[0-9]{2}",$AFT_BOAO_HH) || $AFT_BOAO_HH<0 || $AFT_BOAO_HH>23)
					{
								$RES=$RES."<font color=red>ERROR: Invalid value for Boarding hours received! Cleaned boarding time!<br></font>";
								$RC=-1;
					}
					if (!ereg("[0-9]{2}",$AFT_BOAO_MI) || $AFT_BOAO_MI<0 || $AFT_BOAO_MI>59)
					{
								$RES=$RES."<font color=red>ERROR: Invalid value for Boarding minutes received! Cleaned boarding time!<br></font>";
								$RC=-1;
					}

					if (!empty($DAYVAL) && ereg("[0-9]{1,2}",substr($DAYVAL,1,1)) && $RC==0)
					{
						/* this value is local NOT UTC because the user puts in in local time */
						/* (AFT_STOD is the scheduled time of departure at origin as a base for flight information connected to times (according to IATA-rules)) */
						$AFT_BOAO_LOC=substr($AFT_STOD,0,8).$AFT_BOAO_HH.$AFT_BOAO_MI."00";
						$AFT_BOAO=mktime(substr($AFT_BOAO_LOC,8,2), 
														 substr($AFT_BOAO_LOC,10,2), 
														 substr($AFT_BOAO_LOC,12,2), 
														 substr($AFT_BOAO_LOC,4,2),
														 substr($AFT_BOAO_LOC,6,2),
														 substr($AFT_BOAO_LOC,0,4)
																);
						$VAL=substr($DAYVAL,1,1);
						if (substr($DAYVAL,0,1) =='+')
						{
							$AFT_BOAO=$AFT_BOAO+$VAL*86400;
						}
						else if (substr($DAYVAL,0,1)=='-')
						{
							$AFT_BOAO=$AFT_BOAO-$VAL*86400;
						}
						else
						{
							$AFT_BOAO=$AFT_BOAO;
						}
						/* now back to UTC for the UFIS database */
						$AFT_BOAO=gmdate("YmdHis",$AFT_BOAO);
					}
				}
				if ($RC!=0)
				{
					$AFT_BOAO=" ";
				}
				$Dest="flight";
				$Prio="3";
				$Command="UFR";
				$Table="AFTTAB";
				$Selection="WHERE URNO='".$AFT_URNO."'";
				$Timeout="30";
				$SubCmd="";
				$Fields="URNO,BOAO";
				$Data="".$AFT_URNO.",".$AFT_BOAO;
				$result = CallCput(TRACE,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$SubCmd);
			break;
		case "GATE":
			$Dest="7800";
			$Prio="4";
			$Command="UFR";
			$Table="AFTTAB";
			$Selection="WHERE URNO='".$REC."'";
			$Timeout="30";
			$SubCmd="";

			// Check which Gate is 
			if (!empty($GateNumer)) {
				$thisGateNumer = $GateNumer;
			} else {
				$thisGateNumer = 1;
			}
			
			
			switch ($GATESTAT)
			{
				case "GTO":
						$AFT_GD1X=gmdate("YmdHis");
						$AFT_GD1Y=" ";
						$Fields="URNO,GD$thisGateNumerX,GD$thisGateNumerY,RGD$thisGateNumer";
						//$Data=$REC.",".$AFT_GD1X.",".$AFT_GD1Y.",".$REMARK;
						$Data=$REC.",".$AFT_GD1X.",".$AFT_GD1Y.",".$GATESTAT;
						$result = CallCput(TRACE,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$SubCmd);
						$RES=$RES."<font color=green>Gate open request successfully processed.<br></font>";
					break;
				case "BRD":
						$Fields="URNO,RGD$thisGateNumer";
						//$Data=$REC.",".$REMARK;
						$Data=$REC.",".$GATESTAT;
						$result = CallCput(TRACE,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$SubCmd);
						$RES=$RES."<font color=green>Gate boarding request successfully processed.<br></font>";
					break;
				case "LCL":
						$Fields="URNO,RGD$thisGateNumer";
						//$Data=$REC.",".$REMARK;
						$Data=$REC.",".$GATESTAT;
						$result = CallCput(TRACE,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$SubCmd);
						$RES=$RES."<font color=green>Gate last call request successfully processed.<br></font>";
					break;
				case "GCL":
						$AFT_GD1Y=gmdate("YmdHis");
						$Fields="URNO,GD$thisGateNumerY,RGD$thisGateNumer";
						//$Data=$REC.",".$AFT_GD1Y.",".$REMARK;
						$Data=$REC.",".$AFT_GD1Y.",".$GATESTAT;
						$result = CallCput(TRACE,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$SubCmd);
						$RES=$RES."<font color=green>Gate close request successfully processed.<br></font>";
					break;
				default: 
					$RES=$RES."<font color=red>ERROR: Invalid gate status received!<br></font>";
					break;
		}
	}
}
else
{
		$RES=$RES."<font color=red>ERROR: Incomplete request received!<br>Type=(".$LOCTYPE."), Airline=(".$ALC3."), Record-ID=(".$REC.")<br></font>";
}
?>
<script type="text/javascript">
var RESULT = "<?php echo $RES; ?>";
var ALC3 = "<?php echo $ALC3; ?>";
var REC = "<?php echo $REC; ?>";

//Browser-negotiation 
var Moz = (navigator.userAgent.indexOf('4.04') != -1); 
var Moz2 = (navigator.userAgent.indexOf('4.61') != -1); 
var MSIE = (navigator.userAgent.indexOf('MSIE') != -1); 
var NS = (navigator.userAgent.indexOf('Netscape') != -1 && navigator.userAgent.indexOf('Gecko') != -1); 

<!--
	function Load (){
	if (MSIE==true)
			location.reload('flocctrl.php?ALC3='+ALC3+'&RES='+RESULT+'&REC='+REC);
	if (NS==true || Moz==true || Moz2==true)
		window.open('flocctrl.php?ALC3='+ALC3+'&REC='+REC,"_self","align=center width=640,height=480,left=30,top=50");
		//window.open('flocctrl.php?ALC3='+ALC3+'&RES='+RESULT+'&REC='+REC,"_self","align=center width=640,height=480,left=30,top=50");
	}
	Load();
//-->
</script>
<?php
if ($debug=="TRUE")
{
	echo "<HTML>";
	echo "<HEAD>";
	echo "</HEAD>";
	echo "<BODY>";
	print "<br>AIRLINE    :[".$ALC3."]";
	print "<br>LOCTYPE    :[".$LOCTYPE."]";
	print "<br>LOCNAME    :[".$LOCNAME."]";
	print "<br>-------------------------";
	print "<br>CCA-VALUES";
	print "<br>URNO       :[".$REC."]";
	print "<br>CKBA       :[".$OPENTIME."]";
	print "<br>CKEA       :[".$CLOSETIME."]";
	print "<br>LOGO       :[".$LOGO."]";
	print "<br>FREEREMARK :[".$FREEREMARK."]";
	print "<br>PREREMARK  :[".$PREREMARK."]";
	print "<br>-------------------------";
	print "<br>AFT-VALUES";
	print "<br>URNO       :[".$AFT_URNO."]";
	print "<br>STOD       :[".$AFT_STOD."]";
	print "<br>DAY        :[".$DAYVAL."]";
	print "<br>BOAO-HH    :[".$AFT_BOAO_HH."]";
	print "<br>BOAO-MI    :[".$AFT_BOAO_MI."]";
	print "<br>BOAO-LOC   :[".$AFT_BOAO_LOC."]";
	print "<br>BOAO-UTC   :[".$AFT_BOAO."]";
	print "<br>-------------------------";
	print "<br>GATESTAT   :[".$GATESTAT."]";
	print "<br>COUNTERSTAT:[".$COUNTERSTAT."]";
	print "<br>-------------------------";
	print "<br>-------------------------";
	print "<br>RES        :[".$RES."]";
	echo "<br>";
	echo "<input type=\"submit\" Value=\"Proceed\" onClick=\"Load()\"/>";
	echo "</BODY>";
	echo "</HTML>";
}
?>

