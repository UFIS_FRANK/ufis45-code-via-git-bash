<?php
//********************************************************************************


function Administration_show()
{
  
  global $tpl;
  // Set URLs
  $fldUSR = "USERS.php";
  $fldMNU = "S_TMPL_MENUSRecord.php";
  $fldTTF = "S_FLDSRecord.php";
  $fldTEMP = "S_TMPLRecord.php";
  $fldUMT = "S_USERSMT.php";
  $fldUMF = "S_USERSMF.php";
  $fldLOGOUT = "Index.php";
  // Show fields
  $tpl->set_var("USR", $fldUSR);
  $tpl->set_var("MNU", $fldMNU);
  $tpl->set_var("TTF", $fldTTF);
  $tpl->set_var("TEMP", $fldTEMP);
  $tpl->set_var("UMT", $fldUMT);
  $tpl->set_var("UMF", $fldUMF);
  $tpl->set_var("LOGOUT", $fldLOGOUT);
  $tpl->parse("FormAdministration", false);
}

?>