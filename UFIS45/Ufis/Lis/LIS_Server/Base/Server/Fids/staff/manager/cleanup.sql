REM /* deleting entries from s_tmpl_fld which do not have a valid field-relation */
delete from s_tmpl_fld where fld_id not in (select fldid from s_flds);
REM /* deleting entries from s_tmpl_fld which do not have a valid menu-relation */
delete from s_tmpl_fld where tmpl_menuid not in (select tmpl_menuid from s_tmpl_menus);
REM /* deleting entries from s_tmpl_fld which do not have a valid user-relation */
delete from s_tmpl_fld where userid not in (select userid from s_users);
REM /* deleting entries from s_flds which do not have a valid relation to s_tmp_fld-table (no more users use those fields) */
delete from s_tmpl_fld where fld_id not in (select fldid from s_flds);

REM /* deleting entries from s_tmpl_menus_users which do not have a valid menu-relation */
delete from s_tmpl_menus_users where tmpl_menuid not in (select tmpl_menuid from s_tmpl_menus);
REM /* deleting entries from s_tmpl_menu_users which do not have a valid user-relation */
delete from s_tmpl_menus_users where userid not in (select userid from s_users);

