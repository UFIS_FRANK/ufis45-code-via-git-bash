<?php 
	include("include/manager.php");
	include("$ADODB_RootPath/list.php"); 
	include("include/template.php"); 
	include("class.overlib/class.overlib.php3"); 
	include("include/class.LoginInit.php"); 

	/* Login Check */
	/* Set Login Instance*/
	$login = new  LoginInit;
	$tpl = new Template($tmpl_path);

	// Over lib 
	$ol = new Overlib;
	$ol->ol_backcolor = $maskbgcolor;
	$overlib=$ol->init();
				
	if (isset($Session["userid"])) { 
		// The user has already been Logged
		$Query=$login->Tmpl($Session["userid"]);
	} else {
		$Query=	$login->Login($UserName,$Password);
	}

	//$debug = false;
	/* Start Debug  if requested */
	if ($debug=="true")
	{
		$tmpdebug="?debug=true";
		$db->debug=$debug;
	}
	else
	{
		$debug="false";
	}			 
	//$debug=true;
	//$db->debug=$debug;
			 
	$tdi=FindTDI();
	$tdiminus=$tdi-($tdi*2);

	// Expires the Page
	$Cexpires=DateAdd("s" ,$Refresh,$startdate);
	$Cexpires=date("D, d M Y H:i:s ",DateAdd("n" ,$tdiminus,$Cexpires))." GMT";

	$SearchFlno="";
	if (len($airline)>0) {
		$airline=strtoupper($airline);
		$SearchFlno=$airline;
	}
	if (len($city)>0) {
		$city=strtoupper($city);
	}
	if (len($flightnumber)>0) {
			if (len($airline)>2) {
				$SearchFlno=$airline.$flightnumber;
			}
			if (len($airline)<=2) {
				$SearchFlno=$airline." ".$flightnumber;
			}
			if (len($airline)==0) {
				$SearchFlno=$flightnumber;
			}
	}
	// ##################################################################
	// ### starting to check if times have been set using the search mask
	// ##################################################################
	/*
	if ($SEARCHUSED==TRUE)
	{
		$Searchstartdate = mktime($SearchstartdateHour,$SearchstartdateMin, 0,$SearchstartdateMonth ,$SearchstartdateDay, $SearchstartdateYear);
		$Searchenddate = ($todateplus*3600) + mktime($SearchstartdateHour,$SearchstartdateMin, 0,$SearchstartdateMonth ,$SearchstartdateDay, $SearchstartdateYear);
		//$Searchenddate = mktime($SearchenddateHour,$SearchenddateMin, 0,$SearchenddateMonth ,$SearchenddateDay, $SearchenddateYear);

		$TfVal1=$Searchstartdate;
		$TfVal1Month=date("m",$TfVal1);
		$TfVal1Year=date("Y",$TfVal1);
		$TfVal1Day=date("d",$TfVal1);
		$TfVal1Hour=date("H",$TfVal1);
		$TfVal1Min=date("i",$TfVal1);
		$TfVal1=date("YmdHis",$TfVal1);

		$TfVal2=$Searchenddate;
		$TfVal2Month=date("m",$TfVal2);
		$TfVal2Year=date("Y",$TfVal2);
		$TfVal2Day=date("d",$TfVal2);
		$TfVal2Hour=date("H",$TfVal2);
		$TfVal2Min=date("i",$TfVal2);
		$TfVal2=date("YmdHis",$TfVal2);
	}
	else
	{
	*/
		$TfVal1=time(0) + ($TfVal1*60);
		$TfVal1Month=date("m",$TfVal1);
		$TfVal1Year=date("Y",$TfVal1);
		$TfVal1Day=date("d",$TfVal1);
		$TfVal1Hour=date("H",$TfVal1);
		$TfVal1Min=date("i",$TfVal1);

		$TfVal1=date("YmdHis",$TfVal1);

		$TfVal2=time(0) + ($TfVal2*60);
		$TfVal2Month=date("m",$TfVal2);
		$TfVal2Year=date("Y",$TfVal2);
		$TfVal2Day=date("d",$TfVal2);
		$TfVal2Hour=date("H",$TfVal2);
		$TfVal2Min=date("i",$TfVal2);

		$TfVal2=date("YmdHis",$TfVal2);

		// Setting search mask defaults on todays date at 00:00
		$SearchstartdateMonth=date("m",time(0));
		$SearchstartdateYear=date("Y",time(0));
		$SearchstartdateDay=date("d",time(0));
		$SearchstartdateHour="00";
		$SearchstartdateMin="00";
	//}	

	//echo "Search:(".$SEARCHUSED.")<br>";
	//echo "TfVal1:(".$TfVal1.")<br>";
	//echo "TfVal2:(".$TfVal2.")<br>";
	// Start Header
	// $start = $tpl->utime();

	/* We Must reverse the values of the array in order to pass  the results  
	to the parse functions in the correct order */

	$tmpl_array=array_reverse ($tmpl_array);

	// values for HTTP header
	$tpl->set_var("PageTitle", $PageTitle );
	$tpl->set_var("STYLESHEET", $StyleSheet );
	$tpl->set_var("Refresh",$RefreshRate);
	$tpl->set_var("Cexpires",$Cexpires );

	// values for div search mask
	$tpl->set_var("FromSearch",$FromSearch);
	$tpl->set_var("SearchstartdateMonth",$SearchstartdateMonth);
	$tpl->set_var("SearchstartdateYear",$SearchstartdateYear);
	$tpl->set_var("SearchstartdateDay",$SearchstartdateDay);
	$tpl->set_var("SearchstartdateHour",$SearchstartdateHour);
	$tpl->set_var("SearchstartdateMin",$SearchstartdateMin);
	$tpl->set_var("SearchenddateMonth",$SearchenddateMonth);
	$tpl->set_var("SearchenddateYear",$SearchenddateYear);
	$tpl->set_var("SearchenddateDay",$SearchenddateDay);
	$tpl->set_var("SearchenddateHour",$SearchenddateHour);
	$tpl->set_var("SearchenddateMin",$SearchenddateMin);
	// values for div search mask
	$tpl->set_var("overlib",$overlib);
	$tpl->set_var("alfn",$alfn );	
	$tpl->set_var("alfnspaces",$alfnspaces );			
	$tpl->set_var("apfn",$apfn );	
	$tpl->set_var("apfnspaces",$apfnspaces );				
	//$tpl->set_var("FromDateHeader",$FromDateHeader );			
	//$tpl->set_var("ToDateHeader",$ToDateHeader );			
	$tpl->set_var("dateerror",$dateerror );			
	$tpl->set_var("type",$type );					
	$tpl->set_var("TMPL_MENUID",$TMPL_MENUID );					
	$tpl->set_var("TfVal1",$TfVal1);
	$tpl->set_var("TfVal1Month",$TfVal1Month );
	$tpl->set_var("TfVal1Year",$TfVal1Year );
	$tpl->set_var("TfVal1Day",$TfVal1Day );
	$tpl->set_var("TfVal1Hour",$TfVal1Hour );
	$tpl->set_var("TfVal1Min",$TfVal1Min );
	$tpl->set_var("TfVal2",$TfVal2);
	$tpl->set_var("TfVal2Month",$TfVal2Month );
	$tpl->set_var("TfVal2Year",$TfVal2Year );
	$tpl->set_var("TfVal2Day",$TfVal2Day );
	$tpl->set_var("TfVal2Hour",$TfVal2Hour );
	$tpl->set_var("TfVal2Min",$TfVal2Min );	
	$tpl->set_var("OrgDes",$OrgDes );	
	$tpl->set_var("airline",$airline );	
	//$tpl->set_var("flno",$flno );		
	$tpl->set_var("flightnumber",$flightnumber);		
	$tpl->set_var("SearchFlno",$SearchFlno);		
	$tpl->set_var("city",$city );		
	$tpl->set_var("PHP_SELF",$PHP_SELF );		
	$tpl->set_var("debug",$debug);		
	// End Header


	// We assign the values to the TDplus Select 
	$i = -36;
	while ($i <=36) {
		$tmps="";
		// if ($i == $todateplus) {
		// default selected entry of the std_search-window
		if ($i == 24) {
			$tmps= " selected";
		}
		if ($i<0)
			$tpl->set_var( "todateplus","<option value=\"$i\" $tmps>-$i" );
		else
			$tpl->set_var( "todateplus","<option value=\"$i\" $tmps>+$i" );
		$tpl->parse("TDplus", true);
		$i++;
	}
	
	// Start Results
	$login->FldDSrows ($FldArray);	
	if ($debug == "true" )
	{
		echo "inf1.php: QUERY:$Query<br>";
		echo "<br>";
		echo "inf1.php: EQUERY:$ExtraQuery<br>";
	}

	$Query=ereg_replace("\"","'",$Query);
	$pos=strpos ($Query," ORDER BY" );
	$Wpos=strpos ($Query," from " );

	if  ( $pos>0)  {
		$Query=substr ($Query,0,$pos).$ExtraQuery.substr ($Query,$pos, len($Query));
	} else {
		$Query.=$ExtraQuery;
	}				

	if (len($Query)>0) {
		eval("\$Query = \"$Query\";");
		//echo "Query:(".$Query.")<br>";
		if  ( $Wpos>0)  {
			$WhereP=substr ($Query,$Wpos,len($Query));
		}				

		// Show the Results
		$login->QFld($Query,$WhereP);
	}
	// End  Results

	// Start Footer
	$tpl->pparse($menuname,false);			
	// End Footer
	exit;
?>
