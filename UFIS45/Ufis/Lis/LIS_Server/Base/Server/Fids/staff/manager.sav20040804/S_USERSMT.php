<?php
/*********************************************************************************
 *       Filename: S_USERSMT.php
 *       Generated with CodeCharge 1.1.16
 *       PHP & Templates build 03/28/2001
 *********************************************************************************/

include ("./common.php");
include ("./Admin.php");

session_start();

$filename = "S_USERSMT.php";



check_security(3);

$tpl = new Template($app_path);
$tpl->load_file("S_USERSMT.html", "main");
$tpl->load_file($header_filename, "Header");

$tpl->set_var("FileName", $filename);


$sS_TMPL_MENUS_USERSRecordErr = "";

$sAction = get_param("FormAction");
$sForm = get_param("FormName");
switch ($sForm) {
  case "S_TMPL_MENUS_USERSRecord":
    S_TMPL_MENUS_USERSRecord_action($sAction);
  break;
}Administration_show();
Search_show();
S_USERS_show();
S_TMPL_MENUS_USERSGrid_show();
S_TMPL_MENUS_USERSRecord_show();

$tpl->parse("Header", false);

$tpl->pparse("main", false);

//********************************************************************************


function Search_show()
{
  global $db;
  global $tpl;
  
  $tpl->set_var("ActionPage", "S_USERSMT.php");
	
  // Set variables with search parameters
  $flds_USERNAME = strip(get_param("s_USERNAME"));
    // Show fields
    $tpl->set_var("s_USERNAME", tohtml($flds_USERNAME));
  $tpl->parse("FormSearch", false);
}



function S_USERS_show()
{

  
  global $tpl;
  global $db;
  global $sS_USERSErr;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $HasParam = false;

  
  $tpl->set_var("TransitParams", "s_USERNAME=" . tourl(strip(get_param("s_USERNAME"))) . "&");
  $tpl->set_var("FormParams", "s_USERNAME=" . tourl(strip(get_param("s_USERNAME"))) . "&");
  // Build WHERE statement
  
  $ps_USERNAME = get_param("s_USERNAME");
  if(strlen($ps_USERNAME)) 
  {
    $HasParam = true;
    $sWhere .= "S.USERNAME like " . tosql("%".$ps_USERNAME ."%", "Text");
  }
  if($HasParam)
    $sWhere = " WHERE (" . $sWhere . ")";

  $sDirection = "";
  $sSortParams = "";
  
  // Build ORDER statement
  $sOrder = " order by S.USERNAME Asc";
  $iSort = get_param("FormS_USERS_Sorting");
  $iSorted = get_param("FormS_USERS_Sorted");
  if(!$iSort)
  {
    $tpl->set_var("Form_Sorting", "");
  }
  else
  {
    if($iSort == $iSorted)
    {
      $tpl->set_var("Form_Sorting", "");
      $sDirection = " DESC";
      $sSortParams = "FormS_USERS_Sorting=" . $iSort . "&FormS_USERS_Sorted=" . $iSort . "&";
    }
    else
    {
      $tpl->set_var("Form_Sorting", $iSort);
      $sDirection = " ASC";
      $sSortParams = "FormS_USERS_Sorting=" . $iSort . "&FormS_USERS_Sorted=" . "&";
    }
    
    if ($iSort == 1) $sOrder = " order by S.USERNAME" . $sDirection;
  }

  // Build full SQL statement
  
  $sSQL = "select S.USERID as S_USERID, " . 
    "S.USERNAME as S_USERNAME " . 
    " from S_USERS S ";
  
  $sSQL .= $sWhere . $sOrder;
  $tpl->set_var("SortParams", $sSortParams);

  // Execute SQL statement
  $db->query($sSQL);
  
  // Select current page
  $iPage = get_param("FormS_USERS_Page");
  if(!strlen($iPage)) $iPage = 1;
  $RecordsPerPage = 10;
  if(($iPage - 1) * $RecordsPerPage != 0)
    $db->seek(($iPage - 1) * $RecordsPerPage);
  $iCounter = 0;

  if($db->next_record())
  {  
    // Show main table based on SQL query
    do
    {
      $fldUSERID = $db->f("S_USERID");
      $fldUSERNAME = $db->f("S_USERNAME");
    $tpl->set_var("USERID", tohtml($fldUSERID));
      $tpl->set_var("USERNAME", tohtml($fldUSERNAME));
      $tpl->set_var("USERNAME_URLLink", "S_USERSMT.php");
      $tpl->set_var("Prm_USERID", tourl($db->f("S_USERID"))); 
      $tpl->set_var("FormS_USERS_Page", tourl($iPage)); 
      $tpl->parse("DListS_USERS", true);
      $iCounter++;
    } while($iCounter < $RecordsPerPage &&$db->next_record());
  }
  else
  {
    // No Records in DB
    $tpl->set_var("DListS_USERS", "");
    $tpl->parse("S_USERSNoRecords", false);
    $tpl->set_var("S_USERSScroller", "");
    $tpl->parse("FormS_USERS", false);
    return;
  }
  
  // Parse scroller
  if(@$db->next_record())
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_USERSScrollerPrevSwitch", "_");
    }
    else
    {
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_USERSScrollerPrevSwitch", "");
    }
    $tpl->set_var("NextPage", ($iPage + 1));
    $tpl->set_var("S_USERSScrollerNextSwitch", "");
    $tpl->set_var("S_USERSCurrentPage", $iPage);
    $tpl->parse("S_USERSScroller", false);
  }
  else
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_USERSScroller", "");
    }
    else
    {
      $tpl->set_var("S_USERSScrollerNextSwitch", "_");
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_USERSScrollerPrevSwitch", "");
      $tpl->set_var("S_USERSCurrentPage", $iPage);
      $tpl->parse("S_USERSScroller", false);
    }
  }
  $tpl->set_var("S_USERSNoRecords", "");
  $tpl->parse("FormS_USERS", false);
  
}



function S_TMPL_MENUS_USERSGrid_show()
{

  
  global $tpl;
  global $db;
  global $sS_TMPL_MENUS_USERSGridErr;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $HasParam = false;

  
  $tpl->set_var("TransitParams", "USERID=" . tourl(strip(get_param("USERID"))) . "&");
  $tpl->set_var("FormParams", "USERID=" . tourl(strip(get_param("USERID"))) . "&");
  $bReq = true;
  // Build WHERE statement
  
  $pUSERID = get_param("USERID");
  if(is_number($pUSERID) && strlen($pUSERID))
    $pUSERID = round($pUSERID);
  else 
    $pUSERID = "";
  if(strlen($pUSERID)) 
  {
    $HasParam = true;
    $sWhere .= "S.USERID=" . $pUSERID;
  }
  else
    $bReq = false;
  if($HasParam)
    $sWhere = " AND (" . $sWhere . ")";

  $sDirection = "";
  $sSortParams = "";
  
  // Build ORDER statement
  $iSort = get_param("FormS_TMPL_MENUS_USERSGrid_Sorting");
  $iSorted = get_param("FormS_TMPL_MENUS_USERSGrid_Sorted");
  if(!$iSort)
  {
    $tpl->set_var("Form_Sorting", "");
  }
  else
  {
    if($iSort == $iSorted)
    {
      $tpl->set_var("Form_Sorting", "");
      $sDirection = " DESC";
      $sSortParams = "FormS_TMPL_MENUS_USERSGrid_Sorting=" . $iSort . "&FormS_TMPL_MENUS_USERSGrid_Sorted=" . $iSort . "&";
    }
    else
    {
      $tpl->set_var("Form_Sorting", $iSort);
      $sDirection = " ASC";
      $sSortParams = "FormS_TMPL_MENUS_USERSGrid_Sorting=" . $iSort . "&FormS_TMPL_MENUS_USERSGrid_Sorted=" . "&";
    }
    
    if ($iSort == 1) $sOrder = " order by S.ID" . $sDirection;
    if ($iSort == 2) $sOrder = " order by S1.MENUNAME" . $sDirection;
    if ($iSort == 3) $sOrder = " order by S2.TMPLNAME" . $sDirection;
  }

  // Build full SQL statement
  
  $sSQL = "select S.ID as S_ID, " . 
    "S.TMPLID as S_TMPLID, " . 
    "S.TMPL_MENUID as S_TMPL_MENUID, " . 
    "S.USERID as S_USERID, " . 
    "S1.TMPL_MENUID as S1_TMPL_MENUID, " . 
    "S1.MENUNAME as S1_MENUNAME, " . 
    "S2.TMPLID as S2_TMPLID, " . 
    "S2.TMPLNAME as S2_TMPLNAME " . 
    " from S_TMPL_MENUS_USERS S, S_TMPL_MENUS S1, S_TMPL S2" . 
    " where S1.TMPL_MENUID=S.TMPL_MENUID and S2.TMPLID=S.TMPLID  ";
  
  $sSQL .= $sWhere . $sOrder;
  $tpl->set_var("FormAction", "S_USERSMT.php");
  $tpl->set_var("SortParams", $sSortParams);
  if(!$bReq)
  {
    $tpl->set_var("DListS_TMPL_MENUS_USERSGrid", "");
    $tpl->parse("S_TMPL_MENUS_USERSGridNoRecords", false);
    $tpl->set_var("S_TMPL_MENUS_USERSGridScroller", "");
    $tpl->parse("FormS_TMPL_MENUS_USERSGrid", false);
    return;
  }

  // Execute SQL statement
  $db->query($sSQL);
  
  // Select current page
  $iPage = get_param("FormS_TMPL_MENUS_USERSGrid_Page");
  if(!strlen($iPage)) $iPage = 1;
  $RecordsPerPage = 10;
  if(($iPage - 1) * $RecordsPerPage != 0)
    $db->seek(($iPage - 1) * $RecordsPerPage);
  $iCounter = 0;

  if($db->next_record())
  {  
    // Show main table based on SQL query
    do
    {
      $fldID = $db->f("S_ID");
      $fldTMPL_MENUID = $db->f("S1_MENUNAME");
      $fldTMPLID = $db->f("S2_TMPLNAME");
      $tpl->set_var("ID", tohtml($fldID));
      $tpl->set_var("ID_URLLink", "S_USERSMT.php");
      $tpl->set_var("Prm_ID", tourl($db->f("S_ID"))); 
      $tpl->set_var("TMPL_MENUID", tohtml($fldTMPL_MENUID));
      $tpl->set_var("TMPLID", tohtml($fldTMPLID));
      $tpl->parse("DListS_TMPL_MENUS_USERSGrid", true);
      $iCounter++;
    } while($iCounter < $RecordsPerPage &&$db->next_record());
  }
  else
  {
    // No Records in DB
    $tpl->set_var("DListS_TMPL_MENUS_USERSGrid", "");
    $tpl->parse("S_TMPL_MENUS_USERSGridNoRecords", false);
    $tpl->set_var("S_TMPL_MENUS_USERSGridScroller", "");
    $tpl->parse("FormS_TMPL_MENUS_USERSGrid", false);
    return;
  }
  
  // Parse scroller
  if(@$db->next_record())
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_TMPL_MENUS_USERSGridScrollerPrevSwitch", "_");
    }
    else
    {
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_TMPL_MENUS_USERSGridScrollerPrevSwitch", "");
    }
    $tpl->set_var("NextPage", ($iPage + 1));
    $tpl->set_var("S_TMPL_MENUS_USERSGridScrollerNextSwitch", "");
    $tpl->set_var("S_TMPL_MENUS_USERSGridCurrentPage", $iPage);
    $tpl->parse("S_TMPL_MENUS_USERSGridScroller", false);
  }
  else
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_TMPL_MENUS_USERSGridScroller", "");
    }
    else
    {
      $tpl->set_var("S_TMPL_MENUS_USERSGridScrollerNextSwitch", "_");
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_TMPL_MENUS_USERSGridScrollerPrevSwitch", "");
      $tpl->set_var("S_TMPL_MENUS_USERSGridCurrentPage", $iPage);
      $tpl->parse("S_TMPL_MENUS_USERSGridScroller", false);
    }
  }
  $tpl->set_var("S_TMPL_MENUS_USERSGridNoRecords", "");
  $tpl->parse("FormS_TMPL_MENUS_USERSGrid", false);
  
}



function S_TMPL_MENUS_USERSRecord_action($sAction)
{
  global $db;
  global $tpl;
  global $sForm;
  global $sS_TMPL_MENUS_USERSRecordErr;
  
  $sParams = "";
  $sActionFileName = "S_USERSMT.php";

  
  $sParams = "?";
  $sParams .= "ID=" . tourl(get_param("Trn_ID"));

  $sWhere = "";
  $bErr = false;

  if($sAction == "cancel")
    header("Location: " . $sActionFileName . $sParams); 

  
  // Create WHERE statement
  if($sAction == "update" || $sAction == "delete") 
  {
    $pPKID = get_param("PK_ID");
    if( !strlen($pPKID)) return;
    $sWhere = "ID=" . tosql($pPKID, "Text");
  }

  // Load all form fields into variables
  
  $fldID = get_param("Rqd_ID");
  $fldUSERID = get_param("USERID");
  $fldTMPL_MENUID = get_param("TMPL_MENUID");
  $fldTMPLID = get_param("TMPLID");
  // Validate fields
  if($sAction == "insert" || $sAction == "update") 
  {

    if(strlen($sS_TMPL_MENUS_USERSRecordErr)) return;
  }
  

  $sSQL = "";
  // Create SQL statement
  
  switch(strtolower($sAction)) 
  {
    case "insert":
      
        $sSQL = "insert into S_TMPL_MENUS_USERS (" . 
          "ID," . 
          "USERID," . 
          "TMPL_MENUID," . 
          "TMPLID)" . 
          " values (" . 
          tosql($fldID, "Number") . "," .
          tosql($fldUSERID, "Text") . "," .
          tosql($fldTMPL_MENUID, "Text") . "," .
          tosql($fldTMPLID, "Text") . ")";    
    break;
    case "update":
      
        $sSQL = "update S_TMPL_MENUS_USERS set " .
          "USERID=" . tosql($fldUSERID, "Text") .
          ",TMPL_MENUID=" . tosql($fldTMPL_MENUID, "Text") .
          ",TMPLID=" . tosql($fldTMPLID, "Text");
        $sSQL .= " where " . $sWhere;
    break;
    case "delete":
      
        $sSQL = "delete from S_TMPL_MENUS_USERS where " . $sWhere;
    break;
  }
  // Execute SQL statement
  if(strlen($sS_TMPL_MENUS_USERSRecordErr)) return;
  $db->query($sSQL);
  
  header("Location: " . $sActionFileName . $sParams);
  
}

function S_TMPL_MENUS_USERSRecord_show()
{
  global $db;
  global $tpl;
  global $sAction;
  global $sForm;
  global $sS_TMPL_MENUS_USERSRecordErr;

  $sWhere = "";
  
  $bPK = true;
  $fldID = "";
  $fldUSERID = "";
  $fldTMPL_MENUID = "";
  $fldTMPLID = "";
  

  if($sS_TMPL_MENUS_USERSRecordErr == "")
  {
    // Load primary key and form parameters
    $fldID = get_param("ID");
    $tpl->set_var("Trn_ID", get_param("ID"));
    $tpl->set_var("Rqd_ID", get_param("ID"));
    $pID = get_param("ID");
    $tpl->set_var("S_TMPL_MENUS_USERSRecordError", "");
  }
  else
  {
    // Load primary key, form parameters and form fields
    $fldID = strip(get_param("ID"));
    $fldUSERID = strip(get_param("USERID"));
    $fldTMPL_MENUID = strip(get_param("TMPL_MENUID"));
    $fldTMPLID = strip(get_param("TMPLID"));
    $tpl->set_var("Trn_ID", get_param("Trn_ID"));
    $pID = get_param("PK_ID");
    $tpl->set_var("sS_TMPL_MENUS_USERSRecordErr", $sS_TMPL_MENUS_USERSRecordErr);
    $tpl->parse("S_TMPL_MENUS_USERSRecordError", false);
  }

  
  if( !strlen($pID)) $bPK = false;
  
  $sWhere .= "ID=" . tosql($pID, "Text");
  $tpl->set_var("PK_ID", $pID);

  $sSQL = "select * from S_TMPL_MENUS_USERS where " . $sWhere;

  

  if($bPK && !($sAction == "insert" && $sForm == "S_TMPL_MENUS_USERSRecord"))
  {
    // Execute SQL statement
    $db->query($sSQL);
    $db->next_record();
    
    $fldID = $db->f("ID");
    if($sS_TMPL_MENUS_USERSRecordErr == "") 
    {
      // Load data from recordset when form displayed first time
      $fldUSERID = $db->f("USERID");
      $fldTMPL_MENUID = $db->f("TMPL_MENUID");
      $fldTMPLID = $db->f("TMPLID");
    }
    $tpl->set_var("S_TMPL_MENUS_USERSRecordInsert", "");
    $tpl->parse("S_TMPL_MENUS_USERSRecordEdit", false);
  }
  else
  {
    if($sS_TMPL_MENUS_USERSRecordErr == "")
    {
      $fldID = tohtml(get_param("ID"));
    }
    $tpl->set_var("S_TMPL_MENUS_USERSRecordEdit", "");
    $tpl->parse("S_TMPL_MENUS_USERSRecordInsert", false);
  }
  $tpl->parse("S_TMPL_MENUS_USERSRecordCancel", false);

  // Show form field
  
    $tpl->set_var("ID", tohtml($fldID));
    $tpl->set_var("LBUSERID", "");
    $tpl->set_var("ID", "");
    $tpl->set_var("Value", "");
    $tpl->parse("LBUSERID", true);
    $dbUSERID = new DB_Sql();
    $dbUSERID->Database = DATABASE_NAME;
    $dbUSERID->User     = DATABASE_USER;
    $dbUSERID->Password = DATABASE_PASSWORD;
    $dbUSERID->Host     = DATABASE_HOST;
  
    
    $dbUSERID->query("select USERID, USERNAME from S_USERS order by 2");
    while($dbUSERID->next_record())
    {
      $tpl->set_var("ID", $dbUSERID->f(0));
      $tpl->set_var("Value", $dbUSERID->f(1));
      if($dbUSERID->f(0) == $fldUSERID)
        $tpl->set_var("Selected", "SELECTED" );
      else 
        $tpl->set_var("Selected", "");
      $tpl->parse("LBUSERID", true);
    }
    
    $tpl->set_var("LBTMPL_MENUID", "");
    $tpl->set_var("ID", "");
    $tpl->set_var("Value", "");
    $tpl->parse("LBTMPL_MENUID", true);
    $dbTMPL_MENUID = new DB_Sql();
    $dbTMPL_MENUID->Database = DATABASE_NAME;
    $dbTMPL_MENUID->User     = DATABASE_USER;
    $dbTMPL_MENUID->Password = DATABASE_PASSWORD;
    $dbTMPL_MENUID->Host     = DATABASE_HOST;
  
    
    $dbTMPL_MENUID->query("select TMPL_MENUID, MENUNAME from S_TMPL_MENUS order by 2");
    while($dbTMPL_MENUID->next_record())
    {
      $tpl->set_var("ID", $dbTMPL_MENUID->f(0));
      $tpl->set_var("Value", $dbTMPL_MENUID->f(1));
      if($dbTMPL_MENUID->f(0) == $fldTMPL_MENUID)
        $tpl->set_var("Selected", "SELECTED" );
      else 
        $tpl->set_var("Selected", "");
      $tpl->parse("LBTMPL_MENUID", true);
    }
    
    $tpl->set_var("LBTMPLID", "");
    $tpl->set_var("ID", "");
    $tpl->set_var("Value", "");
    $tpl->parse("LBTMPLID", true);
    $dbTMPLID = new DB_Sql();
    $dbTMPLID->Database = DATABASE_NAME;
    $dbTMPLID->User     = DATABASE_USER;
    $dbTMPLID->Password = DATABASE_PASSWORD;
    $dbTMPLID->Host     = DATABASE_HOST;
  
    
    $dbTMPLID->query("select TMPLID, TMPLNAME from S_TMPL order by 2");
    while($dbTMPLID->next_record())
    {
      $tpl->set_var("ID", $dbTMPLID->f(0));
      $tpl->set_var("Value", $dbTMPLID->f(1));
      if($dbTMPLID->f(0) == $fldTMPLID)
        $tpl->set_var("Selected", "SELECTED" );
      else 
        $tpl->set_var("Selected", "");
      $tpl->parse("LBTMPLID", true);
    }
    
  $tpl->parse("FormS_TMPL_MENUS_USERSRecord", false);
  

}

?>
