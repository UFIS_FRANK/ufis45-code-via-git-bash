<?php
/*********************************************************************************
 *       Filename: S_TMPLRecord.php
 *       Generated with CodeCharge 1.1.16
 *       PHP & Templates build 03/28/2001
 *********************************************************************************/

include ("./common.php");
include ("./Admin.php");

session_start();

$filename = "S_TMPLRecord.php";



check_security(3);

$tpl = new Template($app_path);
$tpl->load_file("S_TMPLRecord.html", "main");
$tpl->load_file($header_filename, "Header");

$tpl->set_var("FileName", $filename);


$sS_TMPL1Err = "";

$sAction = get_param("FormAction");
$sForm = get_param("FormName");
switch ($sForm) {
  case "S_TMPL1":
    S_TMPL1_action($sAction);
  break;
}Administration_show();
S_TMPL_show();
S_TMPL1_show();

$tpl->parse("Header", false);

$tpl->pparse("main", false);

//********************************************************************************



function S_TMPL_show()
{

  
  global $tpl;
  global $db;
  global $sS_TMPLErr;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $HasParam = false;

  
  $tpl->set_var("TransitParams", "");
  $tpl->set_var("FormParams", "");
  // Build WHERE statement
  

  $sDirection = "";
  $sSortParams = "";
  
  // Build ORDER statement
  $sOrder = " order by S.TMPLNAME Asc";
  $iSort = get_param("FormS_TMPL_Sorting");
  $iSorted = get_param("FormS_TMPL_Sorted");
  if(!$iSort)
  {
    $tpl->set_var("Form_Sorting", "");
  }
  else
  {
    if($iSort == $iSorted)
    {
      $tpl->set_var("Form_Sorting", "");
      $sDirection = " DESC";
      $sSortParams = "FormS_TMPL_Sorting=" . $iSort . "&FormS_TMPL_Sorted=" . $iSort . "&";
    }
    else
    {
      $tpl->set_var("Form_Sorting", $iSort);
      $sDirection = " ASC";
      $sSortParams = "FormS_TMPL_Sorting=" . $iSort . "&FormS_TMPL_Sorted=" . "&";
    }
    
    if ($iSort == 1) $sOrder = " order by S.TMPLNAME" . $sDirection;
  }

  // Build full SQL statement
  
  $sSQL = "select S.TMPLID as S_TMPLID, " . 
    "S.TMPLNAME as S_TMPLNAME " . 
    " from S_TMPL S ";
  
  $sSQL .= $sWhere . $sOrder;
  $tpl->set_var("FormAction", "S_TMPLRecord.php");
  $tpl->set_var("SortParams", $sSortParams);

  // Execute SQL statement
  $db->query($sSQL);
  
  // Select current page
  $iPage = get_param("FormS_TMPL_Page");
  if(!strlen($iPage)) $iPage = 1;
  $RecordsPerPage = 10;
  if(($iPage - 1) * $RecordsPerPage != 0)
    $db->seek(($iPage - 1) * $RecordsPerPage);
  $iCounter = 0;

  if($db->next_record())
  {  
    // Show main table based on SQL query
    do
    {
      $fldTMPLNAME = $db->f("S_TMPLNAME");
      $tpl->set_var("TMPLNAME", tohtml($fldTMPLNAME));
      $tpl->set_var("TMPLNAME_URLLink", "S_TMPLRecord.php");
      $tpl->set_var("Prm_TMPLID", tourl($db->f("S_TMPLID"))); 
      $tpl->parse("DListS_TMPL", true);
      $iCounter++;
    } while($iCounter < $RecordsPerPage &&$db->next_record());
  }
  else
  {
    // No Records in DB
    $tpl->set_var("DListS_TMPL", "");
    $tpl->parse("S_TMPLNoRecords", false);
    $tpl->set_var("S_TMPLScroller", "");
    $tpl->parse("FormS_TMPL", false);
    return;
  }
  
  // Parse scroller
  if(@$db->next_record())
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_TMPLScrollerPrevSwitch", "_");
    }
    else
    {
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_TMPLScrollerPrevSwitch", "");
    }
    $tpl->set_var("NextPage", ($iPage + 1));
    $tpl->set_var("S_TMPLScrollerNextSwitch", "");
    $tpl->set_var("S_TMPLCurrentPage", $iPage);
    $tpl->parse("S_TMPLScroller", false);
  }
  else
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_TMPLScroller", "");
    }
    else
    {
      $tpl->set_var("S_TMPLScrollerNextSwitch", "_");
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_TMPLScrollerPrevSwitch", "");
      $tpl->set_var("S_TMPLCurrentPage", $iPage);
      $tpl->parse("S_TMPLScroller", false);
    }
  }
  $tpl->set_var("S_TMPLNoRecords", "");
  $tpl->parse("FormS_TMPL", false);
  
}



function S_TMPL1_action($sAction)
{
  global $db;
  global $tpl;
  global $sForm;
  global $sS_TMPL1Err;
  
  $sParams = "";
  $sActionFileName = "S_TMPLRecord.php";

  

  $sWhere = "";
  $bErr = false;

  if($sAction == "cancel")
    header("Location: " . $sActionFileName); 

  
  // Create WHERE statement
  if($sAction == "update" || $sAction == "delete") 
  {
    $pPKTMPLID = get_param("PK_TMPLID");
    if( !strlen($pPKTMPLID)) return;
    $sWhere = "TMPLID=" . tosql($pPKTMPLID, "Text");
  }

  // Load all form fields into variables
  
  $fldTMPLNAME = get_param("TMPLNAME");
  // Validate fields
  if($sAction == "insert" || $sAction == "update") 
  {

    if(!strlen($fldTMPLNAME))
	$sS_TMPL1Err .= "No Template Name set. Please correct.<br>";
    if(strlen($sS_TMPL1Err)) return;
  }
  

  $sSQL = "";
  // Create SQL statement
  
  switch(strtolower($sAction)) 
  {
    case "insert":
      
        $sSQL = "insert into S_TMPL (" . 
          "TMPLNAME)" . 
          " values (" . 
          tosql($fldTMPLNAME, "Text") . ")";    
    break;
    case "update":
      
        $sSQL = "update S_TMPL set " .
          "TMPLNAME=" . tosql($fldTMPLNAME, "Text");
        $sSQL .= " where " . $sWhere;
    break;
    case "delete":
      
        $sSQL = "delete from S_TMPL where " . $sWhere;
    break;
  }
  // Execute SQL statement
  if(strlen($sS_TMPL1Err)) return;
  $db->query($sSQL);
  
  header("Location: " . $sActionFileName);
  
}

function S_TMPL1_show()
{
  global $db;
  global $tpl;
  global $sAction;
  global $sForm;
  global $sS_TMPL1Err;

  $sWhere = "";
  
  $bPK = true;
  $fldTMPLID = "";
  $fldTMPLNAME = "";
  

  if($sS_TMPL1Err == "")
  {
    // Load primary key and form parameters
    $fldTMPLID = get_param("TMPLID");
    $pTMPLID = get_param("TMPLID");
    $tpl->set_var("S_TMPL1Error", "");
  }
  else
  {
    // Load primary key, form parameters and form fields
    $fldTMPLID = strip(get_param("TMPLID"));
    $fldTMPLNAME = strip(get_param("TMPLNAME"));
    $pTMPLID = get_param("PK_TMPLID");
    $tpl->set_var("sS_TMPL1Err", $sS_TMPL1Err);
    $tpl->parse("S_TMPL1Error", false);
  }

  
  if( !strlen($pTMPLID)) $bPK = false;
  
  $sWhere .= "TMPLID=" . tosql($pTMPLID, "Text");
  $tpl->set_var("PK_TMPLID", $pTMPLID);

  $sSQL = "select * from S_TMPL where " . $sWhere;

  

  if($bPK && !($sAction == "insert" && $sForm == "S_TMPL1"))
  {
    // Execute SQL statement
    $db->query($sSQL);
    $db->next_record();
    
    $fldTMPLID = $db->f("TMPLID");
    if($sS_TMPL1Err == "") 
    {
      // Load data from recordset when form displayed first time
      $fldTMPLNAME = $db->f("TMPLNAME");
    }
    $tpl->set_var("S_TMPL1Insert", "");
    $tpl->parse("S_TMPL1Edit", false);
  }
  else
  {
    if($sS_TMPL1Err == "")
    {
      $fldTMPLID = tohtml(get_param("TMPLID"));
    }
    $tpl->set_var("S_TMPL1Edit", "");
    $tpl->parse("S_TMPL1Insert", false);
  }
  $tpl->parse("S_TMPL1Cancel", false);

  // Show form field
  
    $tpl->set_var("TMPLID", tohtml($fldTMPLID));
    $tpl->set_var("TMPLNAME", tohtml($fldTMPLNAME));
  $tpl->parse("FormS_TMPL1", false);
  

}

?>
