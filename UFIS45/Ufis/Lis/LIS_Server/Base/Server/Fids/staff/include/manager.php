<?php
set_time_limit(240); // increase timeout
$ADODB_RootPath = 'include';

// db-connection for CEDA-flight information data
$DatabaseType="oci8";
$dbusername="CEDA";
$dbpassword="CEDA";
$databaseName="UFIS-4.5";
// in any case you should copy the total description of the db-link from tnsnames.ora into $database
$database="(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = UFISORA)(PORT = 1521))) (CONNECT_DATA = (SERVICE_NAME = UFISORA.WORLD)))";

// old, depreceated parameters
//$database="UFISORA.WORLD";
//$servername="UFISORA.WORLD";
//$servername="(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = UFISORA)(PORT = 1521))) (CONNECT_DATA = (SERVICE_NAME = UFISORA.WORLD)))";

// db-connection for staff user admin tables
$dbusernameSystem="LIS_SP";
$dbpasswordSystem="SP";
$databaseNameSystem="UFIS-4.5";
// in any case you should copy the total description of the db-link from tnsnames.ora into $databaseSystem
$databaseSystem="(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = UFISORA)(PORT = 1521))) (CONNECT_DATA = (SERVICE_NAME = UFISORA.WORLD)))";

// old, depreceated parameters
//$databaseSystem="UFISORA.WORLD";
//$servernameSystem="UFISORA.WORLD";
//$servernameSystem="(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = UFISORA)(PORT = 1521))) (CONNECT_DATA = (SERVICE_NAME = UFISORA.WORLD)))";

$HostName = $HTTP_SERVER_VARS["HTTP_HOST"];
$Url = $HTTP_SERVER_VARS["HTTP_URL"];

$devserver="";
if ($HostName==$devserver)
{
  $mailto="";
  $WebApplicationName="/";
  if ($devserver==$Hostname)
  {
       $HostName=$HostName."";
  } 
}
else
{
  $mailto="";
  $WebApplicationName="/";
} 

$mailfrom="webmaster";

if (!$nofunction) {
	include("$ADODB_RootPath/adodb.inc.php");	
	ADOLoadCode($DatabaseType);
	$db = ADONewConnection();
   	$db->debug = false;
	//$db->debug = true;

	if ($db->Connect(false,$dbusername,$dbpassword,$database))
		$cna="true"; //connect to oracle
	else $cna="false";
		$dbS = ADONewConnection();
    	$dbS->debug = false;
	//$dbS->debug = true;

	if ($dbS->Connect(false,$dbusernameSystem,$dbpasswordSystem,$databaseSystem))
		$cna="true"; //connect to oracle
	else
		 $cna="false";

	$masktitle="Explanation";
	$maskbgcolor="#111166";

$gSQLMaxRows = 1000; // max no of rows to download
$gSQLBlockRows=25; // default max no of rows per table block

$currentpagetitle=" Page : ";
$pagecounttitle=" from "	;

//TemplatePath 
$tmpl_path="/ceda/www/UfisInfo/staff/templates/";

// Template Array
$tmpl_array=array();

// Main Template name
$maintmpl="";

// Fields Array
$FldArray=array();

// Page Title
$PageTitle="";
$OrgDes="";
$RowsPerPage=0;
$RefreshRate=60; // default for the page refresh

// ExtraQuery
$ExtraQuery="";

include("include/functions.php"); 
}
?>
