#!/bin/ksh
# This script shuts down all normalceda-processes for restarting with the correct time after summer/winter time change

# shutdown
/ceda/etc/putque 1100 28674 ; sleep 5 # nmghdl
/ceda/etc/putque 1200 28674 ; sleep 5 # router
/ceda/etc/putque 2600 28674 ; sleep 5 # authdl
/ceda/etc/putque 5400 28674 ; sleep 5 # relhdl
/ceda/etc/putque 7150 28674 ; sleep 5 # sqlhdl1
/ceda/etc/putque 7160 28674 ; sleep 5 # sqlhdl2
/ceda/etc/putque 7170 28674 ; sleep 5 # sqlhdl3
/ceda/etc/putque 7180 28674 ; sleep 5 # sqlhdl4
/ceda/etc/putque 7190 28674 ; sleep 5 # sqlsita
/ceda/etc/putque 7490 28674 ; sleep 5 # twrhdl
/ceda/etc/putque 7500 28674 ; sleep 5 # sui_srv
/ceda/etc/putque 7510 28674 ; sleep 5 # suib_srv
/ceda/etc/putque 7540 28674 ; sleep 5 # oui_prd
/ceda/etc/putque 7550 28674 ; sleep 5 # oui_tst
/ceda/etc/putque 7760 28674 ; sleep 5 # ssihdl
/ceda/etc/putque 7765 28674 ; sleep 5 # ssihdl
/ceda/etc/putque 7770 28674 ; sleep 5 # fogdhd
/ceda/etc/putque 7775 28674 ; sleep 5 # fogshd
/ceda/etc/putque 7780 28674 ; sleep 5 # roghdl
/ceda/etc/putque 7800 28674 ; sleep 5 # flight
/ceda/etc/putque 7805 28674 ; sleep 5 # flisea
/ceda/etc/putque 7810 28674 ; sleep 5 # fligro1
/ceda/etc/putque 7815 28674 ; sleep 5 # fligro2
/ceda/etc/putque 7820 28674 ; sleep 5 # fligro3
/ceda/etc/putque 7825 28674 ; sleep 5 # fligro4
/ceda/etc/putque 7850 28674 ; sleep 5 # ntisch
/ceda/etc/putque 7900 28674 ; sleep 5 # stxhdl
/ceda/etc/putque 7950 28674 ; sleep 5 # nflhdl
/ceda/etc/putque 8310 28674 ; sleep 5 # nflhdl
/ceda/etc/putque 8450 28674 ; sleep 5 # fdihdl
/ceda/etc/putque 8750 28674 ; sleep 5 # ftphdl
/ceda/etc/putque 9200 28674 ; sleep 5 # tlxgen
/ceda/etc/putque 9350 28674 ; sleep 60 # lcdgat1
/ceda/etc/putque 9360 28674 ; sleep 60 # lcdgat2
/ceda/etc/putque 9370 28674 ; sleep 60 # lcdpub
/ceda/etc/putque 9550 28674 ; sleep 60 # impmgr

# use 28673 for bchdl-shutdown. That way there will be no Close BC-message! 
# reset:
/ceda/etc/putque 1900 28673 ; sleep 2 # bchdl
exit 0
