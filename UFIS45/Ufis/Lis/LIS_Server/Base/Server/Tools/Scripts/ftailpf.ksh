# @(#) ftailpf 2.0 20.10.2000 MTE
# tailpf with switching logs

function logtail 
{
# the following '(' creates a subshell with a new pid
(
   echo $$
   # the pid of this subshell can be evaluated in AWK
   tail -f $1 | while [ "1" = "1" ] ; do 
      read line
      echo $line 
      case $line in
         "Renaming Old Debug File to "*)   
            # get the pid belonging to the piping tail:
            tailppid=`ps -ef | grep " $LOGNAME [0-9]* $$ " | awk '{print $2}'`
            tailpid=`ps -ef | grep " $LOGNAME [0-9]* ${tailppid} .* tail -f" | awk '{print $2}'`
            echo $tailppid $tailpid
            kill -9 $tailpid
            break
	      ;;
      esac
   done
)
}

if [ ! -z "$1" ] ; then
   # get pid of CEDA process by listing of processes and grep for a line
   # beginning with 'ceda', somewhere in the line the process name surrounded
   # by blanks and followed by the queue id number and status (21 or 20)  
   # The process name, the queue id and the status are the last three items
   # of the line
   PRID=`ps -ef | grep "^[ \t]*ceda .* $1 [0-9]* 2[01][ \t]*$" | awk '{printf "%05d",$2}'`
   if [ "$1" = "sysmon" ] ; then
      # sysmon daoes not append pid to file name
      PRID=""
   fi
   LOGN=${DBG_PATH}/$1$LOGC$PRID.log
   if [ -f "$LOGN" ] ; then
     echo $LOGN
     while [ "1" = "1" ] ; do
        logtail $LOGN
        # perhaps we miss some dbg lines now, but anyway....
        cat <<EOF 
 ==============================================================

 #    #  ######  #    #          ######     #    #       ######
 ##   #  #       #    #          #          #    #       #
 # #  #  #####   #    #          #####      #    #       #####
 #  # #  #       # ## #          #          #    #       #
 #   ##  #       ##  ##          #          #    #       #
 #    #  ######  #    #          #          #    ######  ######

 ==============================================================

EOF
     done
   else
      echo "file $LOGN not found"
      shift
   fi
fi
if [ -z "$1" ] ; then
   echo "Usage: ftailpf.ksh <processname>"
   echo "<processname> must be complete name like in sgs.tab"
   echo "i.e.  'ftailpf.ksh sysmo'  will fail, "
   echo "while 'ftailpf.ksh sysmon' will succeed"
   
fi
