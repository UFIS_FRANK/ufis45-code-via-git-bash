. /ceda/etc/UfisEnv
TARGETDIR=/ceda/exco/Internet
THIS=`basename $0`
TEST_EXTENSION="_TEST.DAT"
WORK_EXTENSION="_P.DAT"
EXTENSION=${WORK_EXTENSION}
REMOTE=192.168.1.30

# get arrival records, where the format '20040520235959' has to be converted to '2359xx'
# with 'xx' equal to '-1' for the day before,
#      'xx' equal to '+1' for the day after,
# and  'xx' equal to '  ' for today
# Also the remark code has to be replaced by according text and VIA has to be expanded to full name
SqlGetArrRecords()
{
(
   sqlplus -s ${CEDADBUSER}/${CEDADBPW} @<<EOF
    set lines 120
    set pagesize 0
    set tab off
    select substr(stoa,9,4)||decode(substr(stoa,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                  ),substr(FLNO,1,8),ORG3,
           substr(decode(via3,NULL,' ','   ',' ',(select c.apsn from apttab c where c.apc3 = a.via3 and rownum=1)),1,22),
           substr(ETOA,9,4)||decode(substr(etoa,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                 ),
           decode(remp,NULL,' ',(select substr(b.bemd,1,20) from fidtab b where b.code = a.remp)),
           substr(NXTI,9,4)||decode(substr(NXTI,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                 )
               from afttab a
    where stoa between to_char(sysdate-1,'YYYYMMDD')||'000000' and to_char(sysdate+1,'YYYYMMDD')||'235959'
--    where stoa between to_char(sysdate-1,'YYYYMMDDHH24MI') and to_char(sysdate+1,'YYYYMMDDHH24MI')
    and des3='LIS' 
    and styp in ('C','G','J','O','S') AND FTYP IN ('O','X','D','R','S') AND FLNO <> ' '
    order by stoa;
    quit;
EOF
) | grep -v selected | grep -v "^$"
}

# get departure records, where the format '20040520235959' has to be converted to '2359xx'
# with 'xx' equal to '-1' for the day before,
#      'xx' equal to '+1' for the day after,
# and  'xx' equal to '  ' for today
# Also the remark code has to be replaced by according text and VIA has to be expanded to full name
SqlGetDepRecords()
{
(
   sqlplus -s ${CEDADBUSER}/${CEDADBPW} @<<EOF
    set lines 120
    set pagesize 0
    set tab off
    select substr(stod,9,4)||decode(substr(stod,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                  ),substr(FLNO,1,8),DES3,
           substr(decode(via3,NULL,' ','   ',' ',(select c.apsn from apttab c where c.apc3 = a.via3 and rownum=1)),1,22),
           substr(ETOD,9,4)||decode(substr(etod,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                 ),
           decode(remp,NULL,' ',(select substr(b.bemd,1,20) from fidtab b where b.code = a.remp)),
           substr(NXTI,9,4)||decode(substr(NXTI,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                 )
               from afttab a
    where stod between to_char(sysdate-1,'YYYYMMDD')||'000000' and to_char(sysdate+1,'YYYYMMDD')||'235959'
--    where stod between to_char(sysdate-1,'YYYYMMDDHH24MI') and to_char(sysdate+1,'YYYYMMDDHH24MI')
    and ORG3='LIS' 
    and styp in ('C','G','J','O','S') AND FTYP IN ('O','X','D','R','S') AND FLNO <> ' '
    order by stod;
    quit;
EOF
) | grep -v selected | grep -v "^$"
}

# get arrival records, where the format '20040520235959' has to be converted to '2359xx'
# with 'xx' equal to '-1' for the day before,
#      'xx' equal to '+1' for the day after,
# and  'xx' equal to '  ' for today
# Also the remark code has to be replaced by according text and VIA has to be expanded to full name
SqlGetArrRecordsNew()
{
(
   sqlplus -s ${CEDADBUSER}/${CEDADBPW} @<<EOF
    set lines 120
    set pagesize 0
    set tab off
    select substr(stoa,9,4)||decode(substr(stoa,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                  ),substr(FLNO,1,8),ORG3,
           substr(decode(via3,NULL,' ','   ',' ',(select c.apsn from apttab c where c.apc3 = a.via3 and rownum=1)),1,22),
           substr(ETOA,9,4)||decode(substr(etoa,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                 ),
           decode(remp,NULL,' ',(select substr(b.bemd,1,20) from fidtab b where b.code = a.remp)),
           substr(NXTI,9,4)||decode(substr(NXTI,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                 ),
           substr(ONBL,9,4)||decode(substr(ONBL,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                 ),
           substr(LAND,9,4)||decode(substr(LAND,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                 )
               from afttab a
    where stoa between to_char(sysdate-1,'YYYYMMDD')||'000000' and to_char(sysdate+1,'YYYYMMDD')||'235959'
--    where stoa between to_char(sysdate-1,'YYYYMMDDHH24MI') and to_char(sysdate+1,'YYYYMMDDHH24MI')
    and des3='LIS' 
    and styp in ('C','G','J','O','S') AND FTYP IN ('O','X','D','R','S') AND FLNO <> ' '
    order by stoa;
    quit;
EOF
) | grep -v selected | grep -v "^$"
}

# get departure records, where the format '20040520235959' has to be converted to '2359xx'
# with 'xx' equal to '-1' for the day before,
#      'xx' equal to '+1' for the day after,
# and  'xx' equal to '  ' for today
# Also the remark code has to be replaced by according text and VIA has to be expanded to full name
SqlGetDepRecordsNew()
{
(
   sqlplus -s ${CEDADBUSER}/${CEDADBPW} @<<EOF
    set lines 120
    set pagesize 0
    set tab off
    select substr(stod,9,4)||decode(substr(stod,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                  ),substr(FLNO,1,8),DES3,
           substr(decode(via3,NULL,' ','   ',' ',(select c.apsn from apttab c where c.apc3 = a.via3 and rownum=1)),1,22),
           substr(ETOD,9,4)||decode(substr(etod,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                 ),
           decode(remp,NULL,' ',(select substr(b.bemd,1,20) from fidtab b where b.code = a.remp)),
           substr(NXTI,9,4)||decode(substr(NXTI,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                 ),
           substr(AIRB,9,4)||decode(substr(AIRB,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                 ),
           substr(OFBL,9,4)||decode(substr(OFBL,7,2),to_char(sysdate-1,'DD'),'-1',
                                                     to_char(sysdate+1,'DD'),'+1',
                                                     '  '
                 )
               from afttab a
    where stod between to_char(sysdate-1,'YYYYMMDD')||'000000' and to_char(sysdate+1,'YYYYMMDD')||'235959'
--    where stod between to_char(sysdate-1,'YYYYMMDDHH24MI') and to_char(sysdate+1,'YYYYMMDDHH24MI')
    and ORG3='LIS' 
    and styp in ('C','G','J','O','S') AND FTYP IN ('O','X','D','R','S') AND FLNO <> ' '
    order by stod;
    quit;
EOF
) | grep -v selected | grep -v "^$"
}

# all text 'HH:MM' in remarks have to be deleted in normal case, but for NEXT INFO the
# value of NXTI has to be appended to the line:
Replace_HHMM_and_NXTINew()
{
awk '
   {
     time_pos=70
     best_pos=42
     if ((length($0)>77) && (substr($0,77,1) != " "))
     {
        best_pos=77
     } else if ((length($0)>84) && (substr($0,84,1) != " "))
     {
        best_pos=84
     } 
     BESTTIME=substr($0"      ",best_pos,6)
     if (index($0,"Info HH:MM")>0) 
     {
        BEME="Info "substr($0,time_pos,2)":"substr($0,time_pos+2,2)
     }
     else
     {
        BEME=substr($0,49,20)
     }
     p=index(BEME,"HH:MM")
     if (p==0)
     {
       p=21
     }
     line=substr($0,1,40)""BESTTIME" "substr(BEME,1,p-1)
     printf "%-90s \n",line
   }'
} 

# all text 'HH:MM' in remarks have to be deleted in normal case, but for NEXT INFO the
# value of NXTI has to be appended to the line:
Replace_HHMM_and_NXTI()
{
awk '
   {
     time_pos=70
     if (index($0,"Info HH:MM")>0) 
     {
        NXTI=substr($0,time_pos,2)":"substr($0,time_pos+2,2)
     }
     else
     {
        NXTI=""
     }
     p=index($0,"HH:MM")
     if (p==0)
     {
       p=time_pos
     }
     line=substr($0,1,p-1)""NXTI
     printf "%-90s \n",line
   }'
} 

#+-  cat DEP_TEST.DAT.wrk | Replace_HHMM_and_NXTINew >DEP_TEST.DAT.wrk.new
#+-  cat DEP_TEST.DAT.wrk | Replace_HHMM_and_NXTI >DEP_TEST.DAT.wrk.old

cnt=`ps -ef | grep "ftp[ ]*-n[ ]*${REMOTE}" | wc -l`
if [ "${cnt}" = "0" ] ; then

  # if not exist ${TARGETDIR}/tmp create it:
  test -d ${TARGETDIR}/tmp || mkdir -p ${TARGETDIR}/tmp

  # write data to temp direcotry (avoiding temporary incomplete file):
#+-  SqlGetDepRecords | Replace_HHMM_and_NXTI  >${TARGETDIR}/tmp/DEP${EXTENSION}
#+-  SqlGetArrRecords | Replace_HHMM_and_NXTI  >${TARGETDIR}/tmp/ARR${EXTENSION}
  SqlGetDepRecords | Replace_HHMM_and_NXTI >${TARGETDIR}/tmp/DEP${EXTENSION}.old
  SqlGetArrRecords | Replace_HHMM_and_NXTI >${TARGETDIR}/tmp/ARR${EXTENSION}.old
  SqlGetDepRecordsNew | Replace_HHMM_and_NXTINew >${TARGETDIR}/tmp/DEP${EXTENSION}
  SqlGetArrRecordsNew | Replace_HHMM_and_NXTINew >${TARGETDIR}/tmp/ARR${EXTENSION}
  SqlGetDepRecordsNew >${TARGETDIR}/tmp/DEP${EXTENSION}.raw
  SqlGetArrRecordsNew >${TARGETDIR}/tmp/ARR${EXTENSION}.raw

  # replace old files by new files:
  mv ${TARGETDIR}/tmp/*${EXTENSION} ${TARGETDIR}/.

  cd ${TARGETDIR}
  ftp -n ${REMOTE} <<EOF
    user anonymous UFIS@UFIS.com
    put DEP${EXTENSION}
    put ARR${EXTENSION}
    quit
EOF

#else
#  echo ${THIS} seems to run already $cnt times! stopped....
fi
