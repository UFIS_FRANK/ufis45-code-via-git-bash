THIS=`basename $0`
DisplayExample()
{
   cat <<EOF
START ":CMD <UFR>"
NEXT "UFR TABLE"
NEXT "UFR FIELDS"
NEXT "UFR DATA"
NEXT "=========== START"
END "UFR SELECT"

EOF
}
DisplayUsage()
{
   cat <<EOF
          Enter a START key word and the number of the argument to log
          Enter a NEXT  key word and the number of the argument to log
          Enter a NEXT  key word and the number of the argument to log
          Enter a ::::  key word and the number of the argument to log
          Enter a NEXT  key word and the number of the argument to log
          Enter a END   key word and the number of the argument to log
          Enter an empty line
          Example:
EOF
DisplayExample
}
DisplayHelp()
{
   cat <<EOF
   ${THIS}: script to show only lines of interest from logfiles
   usage: ${THIS} <processname>
          after <ENTER> the following short help appears and a prompt is waiting
          to get the values:
          `DisplayUsage`
EOF
}
GetParms()
{
KEY=" "
CNT=0
while [ ! "${KEY}" = "" ] ; do
   read KEY pattern
   case "${KEY}" in
      "START") cat <<EOL
                 START=${pattern} ;
EOL
               ;;
      "NEXT")  ((CNT++))
               cat <<EOL
                 NEXT[${CNT}]=${pattern} ;
EOL
               ;;
      "END")   cat <<EOL
                  CNT=${CNT} ;
                  LAST=${pattern} ;
EOL
               ;;
   esac
#   echo "key now: <$KEY>"
done 
}
checkit()
{
cat <<EOF
 awk '
  BEGIN {
`DisplayExample | GetParms` 
  }
  {
     if (index(\$0,START)>0) {print \$0 }
     if (index(\$0,LAST)>0) {print \$0}
     for (ii=1; ii<=CNT; ii++) {
       if (index(\$0,NEXT[ii])>0) {print \$0}
     }
  }
 '
EOF
}
checkit_ok()
{
cat <<EOF
 awk '
  BEGIN {
  START=":CMD <UFR>" ;
  NEXT[1]="UFR TABLE" ;
  NEXT[2]="UFR FIELDS" ;
  NEXT[3]="UFR DATA" ; 
  NEXT[4]="=========== START" ;
  CNT=4 ;
  LAST="UFR SELECT" ;
  }
  {
     if (index(\$0,START)>0) {print \$0 }
     if (index(\$0,LAST)>0) {print \$0}
     for (ii=1; ii<=CNT; ii++) {
       if (index(\$0,NEXT[ii])>0) {print \$0}
     }
  }
 '
EOF
}
AWK=`checkit`
#echo $AWK
DisplayUsage
cat /ceda/debug/flight10598.log | eval $AWK
