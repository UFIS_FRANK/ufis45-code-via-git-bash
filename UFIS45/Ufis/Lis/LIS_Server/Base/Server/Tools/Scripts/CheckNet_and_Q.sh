#!/usr/bin/bash
while true
do
	LISTENCNT=`netstat -a|grep LISTEN|wc -l`
	ESTCNT=`netstat -a|grep ESTABLISHED|wc -l`
	WAITCNT=`netstat -a|grep WAIT|wc -l`
	INQUE=`ipcs -qa|grep ceda|grep "002c"|awk '{printf "Max.BYTES(%s)\tCurr.BYTES(%s)\tNr.of PACK.(%s)\n",$11,$9,$10}';`
	ATTQUE=`ipcs -qa|grep ceda|grep "0037"|awk '{printf "Max.BYTES(%s)\tCurr.BYTES(%s)\tNr.of PACK.(%s)\n",$11,$9,$10}';`
	GENQUE=`ipcs -qa|grep ceda|grep "0042"|awk '{printf "Max.BYTES(%s)\tCurr.BYTES(%s)\tNr.of PACK.(%s)\n",$11,$9,$10}';`
	echo "--------------------------------------------------"
	echo "NETCON.: LISTEN=<$LISTENCNT>; EST.=<$ESTCNT>; WAIT<$WAITCNT>"
	echo "UNIX-QUEUE: INCOMING: $INQUE"
	echo "UNIX-QUEUE: ATTACH  : $ATTQUE"
	echo "UNIX-QUEUE: GENERAL : $GENQUE"
	sleep 5
done
