# $Id: Ufis/Lis/LIS_Server/Base/Server/Tools/Scripts/ArchivFlightData.ksh 1.4 2006/03/09 01:29:15SGT jim Exp  $
#
# Script to archive AFTTAB, CCATAB LOATAB, TLXTAB, H00TAB and H01TAB 
# see 'PrintUsage' below
#
# 20060223 JIM: split archive and delete into two runs, better logs
# 20060224 JIM: allow delete without archive (undocumented)
#               fixed wrong display at end of day
# 20060308 JIM: added access to remote source tables
#
# (C) UFIS AS

ACTION=$1
SEASON=$2
FROM=$3
TO=$4
LOG="${DBG_PATH}/`basename $0`_`date +%Y%m%d_%H%M`.log"

export DBLINK="@UFISARCHTST"

# in case using a DB link, give an hint:
DBLNK_MSG=""
if [ ! -z "${DBLINK}" ] ; then
   DBLNK_MSG="Source tables are accessed by DB link ${DBLINK}!"
fi
PrintUsage()
{
   cat <<EOF
   $0: Script to archive data from AFTTAB, CCATAB LOATAB, 
       TLXTAB, H00TAB and H01TAB for a named Season.
   usage:
       $0 <archive|delete> <abbrivated season name>
   where
        archive: archives the data.
                 The archive tables will have names like
                 AFTTAB_W2004_from_to, wherby 'from' and 'to'
                 are in the notation YYYYmmdd.
        delete:  removes the archived data from the online tables.
                 To removes the archived data from the online tables
                 the time range must be exact as during archiving.
   ${DBLNK_MSG}

   example:
       $0 archive W04
   Result of example: The data of the mentioned tables of the winter 
       season 2004 will be moved to archive table day by day, 

   To restrict time range following calls are allowed:
       $0 archive W04 20041101
       $0 archive W04 20041101 20041130
   The first restriction will archive one day only, the second will
   archive the Nov 2004. The date(s) must be in range of the season
   defined in SEATAB, in the example of season W04.

   The abrivated season name may be on of following:

`sql_call "select seas||': '||beme from seatab${DBLINK} order by vpfr;"`

EOF
   exit 1
}

sql_call()
{
(
  sqlplus -s ${CEDADBUSER}/${CEDADBPW} <<EOSQL
    set head off
    set pagesize 0
    set linesize 1024
    $*
EOSQL
) | grep -v selected | grep -v "^$"
}

. /ceda/etc/UfisEnv

SqlWithComment()
{
    cat <<EOF
    WHENEVER SQLERROR EXIT
    set head off
    set termout off
    set feedback off
    select to_char(sysdate,'YYYYmmdd HH24:mi:SS: ')||' ${COMMENT} ' from dual;
--  prompt ${SQLLINE}
    set feedback on
    ${SQLLINE}
    set feedback off
    COMMIT;
EOF
}

SqlComment()
{
    cat <<EOF
    set head off
    select to_char(sysdate,'YYYYmmdd HH24:mi:SS: ')||' $1 ' from dual;
EOF
}


HOME=`grep "^EXTAB SYS,HOMEAP" /ceda/conf/sgs.tab | cut -d\" -f2`
test -z "${DBG_PATH}" && DBG_PATH="."

#
# CheckDateRange: check if  date 1 is less date 2
#
CheckDateRange()
{
   expr $1 \> $2 >/dev/null &&  echo "Date $1 is greater than date $2 " && exit 1
   return 0
}

#
# CheckDateSeason: check if date 7 Months ago 
#
CheckDateSeason()
{
   DIFFM1=`expr \( $YYC - $1 \) \* 12  + $MMC - $2`
   #echo "-- Month diff: $DIFFM1"
   expr $DIFFM1 \< 7 >/dev/null &&  echo "Date $1$2 is less than 7 months ago " && exit 1
   return 0
}

#
# CheckDateForm: check year between 1998 and now, month between 1 and 12 and
#                day between 1 and max day of month
#
CheckDateForm()
{
   if [ -z "$1" ] ; then
      echo "No date given" 
      exit 1
   fi
   YY=`echo $1 | cut -c-4`
   MM=`echo $1 | cut -c5-6`
   DD=`echo $1 | cut -c7-8`
   expr $YY \< 1998       >/dev/null && echo "Year $YY to low" && exit 1
   expr $YY \> `date +%Y` >/dev/null && echo "Year $YY greater current year" && exit 1
   expr $MM \< 1          >/dev/null && echo "Month $MM must be between 1 and 12" && exit 1
   expr $MM \> 12         >/dev/null && echo "Month $MM must be between 1 and 12" && exit 1
   expr $DD \< 1          >/dev/null && echo "Day $DD must be greater 1" && exit 1
   case $MM in
    01|03|05|07|08|10|12) DMAX=31
                      ;;
    02)               DMAX=28
                      expr \( $YY / 4 \) \* 4 = $YY >/dev/null && DMAX=29
                      expr \( $YY / 100 \) \* 100 = $YY >/dev/null && DMAX=28
                      ;;
    *)                DMAX=30
                      ;;
   esac
   expr $DD \> ${DMAX}     >/dev/null && echo "For month $MM day $DD may not be greater $DMAX" && exit 1
   
   return 0
}

#
# AddOneDay: calculate next day
#
AddOneDay()
{
  YY=`echo $1 | cut -c-4`
  MM=`echo $1 | cut -c5-6`
  DD=`echo $1 | cut -c7-8`
  case $MM in
   01|03|05|07|08|10|12) DMAX=31
                    ;;
   02)               DMAX=28
                    expr \( $YY / 4 \) \* 4 = $YY >/dev/null && DMAX=29
                    expr \( $YY / 100 \) \* 100 = $YY >/dev/null && DMAX=28
                    ;;
   *)               DMAX=30
                    ;;
  esac
  #echo $YY $MM $DD: DMAX: $DMAX
  DD=`expr $DD + 1`
  expr ${DD} \> ${DMAX} >/dev/null && DD=1 && MM=`expr $MM + 1`
  expr ${MM} \> 12 >/dev/null && MM=1 && YY=`expr $YY + 1`
  test "`echo $DD | cut -c2-`" = "" && DD="0"$DD
  test "`echo $MM | cut -c2-`" = "" && MM="0"$MM
  echo $YY$MM$DD
}

CreateXXXUrno()
{
   COMMENT="Creating temporary table $1"
   SQLLINE="create table $1 as select urno from $2 where urno=NULL;"
   SqlWithComment
}

DropXXXUrno()
{
   COMMENT="drop temporary table $1"
   SQLLINE="drop table $1;"
   SqlWithComment
}

CreateXXX()
{
   STORAGE=""
   TSPACE=""
   if [ ! -z "$4" ] ; then
      NEXT=`expr ${4} / 5`
      STORAGE=" STORAGE(INITIAL $4 NEXT ${NEXT} MINEXTENTS 1 MAXEXTENTS 2147483645) "
   fi
   if [ ! -z "$3" ] ; then
      TSPACE=" TABLESPACE \"$3\" "
   fi
   COMMENT="Creating table $1"
   SQLLINE="create table $1 ${STORAGE}${TSPACE}as select * from $2 where urno=NULL;"
   SqlWithComment
}

PrepareCopyDay()
{
  CreateXXXUrno ${AFTURNO} ${AFTTAB}
  CreateXXXUrno ${CCAURNO} ${CCATAB}
  CreateXXXUrno ${TLXURNO} ${TLXTAB}

  DATE=$1
  COMMENT="AFT: Collecting arrivals"
  WHERE="WHERE STOA LIKE '${DATE}%' AND DES3='${HOME}' AND ADID='A'"
  SQLLINE="INSERT INTO ${AFTURNO} SELECT URNO FROM ${AFTTAB} ${WHERE};"
  SqlWithComment

  COMMENT="AFT: Collecting departures"
  WHERE="WHERE STOD LIKE '${DATE}%' AND ORG3='${HOME}' AND ADID IN('D','B')"
  SQLLINE="INSERT INTO ${AFTURNO} SELECT URNO FROM ${AFTTAB} ${WHERE};"
  SqlWithComment

  COMMENT="CCA: Collecting dedicated CC"
  WHERE="WHERE FLNU IN (SELECT URNO from ${AFTURNO})"
  SQLLINE="INSERT INTO ${CCAURNO} SELECT URNO FROM ${CCATAB} ${WHERE};"
  SqlWithComment

  COMMENT="CCA: Collecting common CC"
  WHERE="WHERE ( (CKES LIKE '${DATE}%' AND CKEA = ' ') OR (CKEA LIKE '${DATE}%' ) ) AND CTYP='C' "
  SQLLINE="INSERT INTO ${CCAURNO} SELECT URNO FROM ${CCATAB} ${WHERE};"
  SqlWithComment

  COMMENT="TLX: Collecting dedicated Telexes"
  WHERE="WHERE FLNU IN (SELECT URNO from ${AFTURNO})"
  SQLLINE="INSERT INTO ${TLXURNO} SELECT URNO FROM ${TLXTAB} ${WHERE};"
  SqlWithComment

  COMMENT="TLX: Collecting undedicated Telexes"
  WHERE="WHERE TIME LIKE '${DATE}%' AND FLNU=0"
  SQLLINE="INSERT INTO ${TLXURNO} SELECT URNO FROM ${TLXTAB} ${WHERE};"
  SqlWithComment

}

CopyDay()
{
  COMMENT="AFTTAB: copy records to archive"
  WHERE="WHERE URNO IN (SELECT URNO FROM ${AFTURNO})"
  SQLLINE="INSERT INTO ${AFTARC} SELECT * FROM ${AFTTAB} ${WHERE};"
  SqlWithComment

  COMMENT="CCATAB: copy records to archive"
  WHERE="WHERE URNO IN (SELECT URNO FROM ${CCAURNO})"
  SQLLINE="INSERT INTO ${CCAARC} SELECT * FROM ${CCATAB} ${WHERE};"
  SqlWithComment

  COMMENT="TLXTAB: copying records"
  WHERE="WHERE URNO IN (SELECT URNO FROM ${TLXURNO})"
  SQLLINE="INSERT INTO ${TLXARC} SELECT * FROM ${TLXTAB} ${WHERE};"
  SqlWithComment

  COMMENT="LOATAB: copying dedicated load data"
  WHERE="WHERE FLNU IN (SELECT URNO FROM ${AFTURNO})"
  SQLLINE="INSERT INTO ${LOAARC} SELECT * FROM ${LOATAB} ${WHERE};"
  SqlWithComment

  COMMENT="LOATAB: copying undedicated load data"
  WHERE="WHERE RURN IN (SELECT URNO FROM ${TLXURNO})"
  SQLLINE="INSERT INTO ${LOAARC} SELECT * FROM ${LOATAB} ${WHERE};"
  SqlWithComment

  COMMENT="H00TAB: copying AFT log data"
  WHERE="WHERE UREF IN (SELECT URNO FROM ${AFTURNO})"
  SQLLINE="INSERT INTO ${H00ARC} SELECT * FROM ${H00TAB} ${WHERE};"
  SqlWithComment

  COMMENT="H01TAB: copying CCA log data"
  WHERE="WHERE UREF IN (SELECT URNO FROM ${CCAURNO})"
  SQLLINE="INSERT INTO ${H01ARC} SELECT * FROM ${H01TAB} ${WHERE};"
  SqlWithComment

  DropXXXUrno ${AFTURNO} 
  DropXXXUrno ${CCAURNO} 
  DropXXXUrno ${TLXURNO} 
}

#
# CopyLoop: Loop days from first day to last day, output SQL lines to stdout.
#              SQL lines may be processed in calling shell function or may output
#              to a file. If output to a file, be sure for hard disk space
#                
CopyLoop()
{
  DAY1=$DATE1
  DAY2=$DAY1
  SqlComment "====== START/END Copy data of day ${DAY1} ======"
  while expr $DAY1 \<= $DATE2 >/dev/null ; do
    PrepareCopyDay $DAY1
    CopyDay $DAY1
    DAY2=$DAY1
    DAY1=`AddOneDay $DAY1`
  done
  SqlComment "====== START/END Copy data of day ${DAY2} ======"
}

PrepareDeleteDay()
{
  CreateXXXUrno ${AFTURNO} ${AFTARC}
  CreateXXXUrno ${CCAURNO} ${CCAARC}
  CreateXXXUrno ${TLXURNO} ${TLXARC}

  DATE=$1
  COMMENT="AFTARC: Collecting archived arrivals"
  WHERE="WHERE STOA LIKE '${DATE}%' AND DES3='${HOME}' AND ADID='A'"
  SQLLINE="INSERT INTO ${AFTURNO} SELECT URNO FROM ${AFTARC} ${WHERE};"
  SqlWithComment

  COMMENT="AFTARC: Collecting archived departures"
  WHERE="WHERE STOD LIKE '${DATE}%' AND ORG3='${HOME}' AND ADID IN('D','B')"
  SQLLINE="INSERT INTO ${AFTURNO} SELECT URNO FROM ${AFTARC} ${WHERE};"
  SqlWithComment

  COMMENT="CCAARC: Collecting archived dedicated CC"
  WHERE="WHERE FLNU IN (SELECT URNO from ${AFTURNO})"
  SQLLINE="INSERT INTO ${CCAURNO} SELECT URNO FROM ${CCAARC} ${WHERE};"
  SqlWithComment

  COMMENT="CCAARC: Collecting archived common CC"
  WHERE="WHERE ( (CKES LIKE '${DATE}%' AND CKEA = ' ') OR (CKEA LIKE '${DATE}%' ) ) AND CTYP='C' "
  SQLLINE="INSERT INTO ${CCAURNO} SELECT URNO FROM ${CCAARC} ${WHERE};"
  SqlWithComment

  COMMENT="TLXARC: Collecting archived dedicated Telexes"
  WHERE="WHERE FLNU IN (SELECT URNO from ${AFTURNO})"
  SQLLINE="INSERT INTO ${TLXURNO} SELECT URNO FROM ${TLXARC} ${WHERE};"
  SqlWithComment

  COMMENT="TLXARC: Collecting archived undedicated Telexes"
  WHERE="WHERE TIME LIKE '${DATE}%' AND FLNU=0"
  SQLLINE="INSERT INTO ${TLXURNO} SELECT URNO FROM ${TLXARC} ${WHERE};"
  SqlWithComment

}

DeleteDay()
{
  WHERE="WHERE URNO IN (SELECT URNO FROM ${AFTURNO})"
  COMMENT="AFTTAB: deleting records "
  SQLLINE="DELETE FROM ${AFTTAB} ${WHERE};"
  SqlWithComment

  WHERE="WHERE URNO IN (SELECT URNO FROM ${CCAURNO})"
  COMMENT="CCATAB: deleting records "
  SQLLINE="DELETE FROM ${CCATAB} ${WHERE};"
  SqlWithComment

  WHERE="WHERE URNO IN (SELECT URNO FROM ${TLXURNO})"
  COMMENT="TLXTAB: deleting records "
  SQLLINE="DELETE FROM ${TLXTAB} ${WHERE};"
  SqlWithComment

  WHERE="WHERE FLNU IN (SELECT URNO FROM ${AFTURNO})"
  COMMENT="LOATAB: deleting dedicated load data"
  SQLLINE="DELETE FROM ${LOATAB} ${WHERE};"
  SqlWithComment

  WHERE="WHERE RURN IN (SELECT URNO FROM ${TLXURNO})"
  COMMENT="LOATAB: deleting undedicated load data"
  SQLLINE="DELETE FROM ${LOATAB} ${WHERE};"
  SqlWithComment

  WHERE="WHERE UREF IN (SELECT URNO FROM ${AFTURNO})"
  COMMENT="H00TAB: deleting AFT log data"
  SQLLINE="DELETE FROM ${H00TAB} ${WHERE};"
  SqlWithComment

  WHERE="WHERE UREF IN (SELECT URNO FROM ${CCAURNO})"
  COMMENT="H01TAB: deleting CCA log data"
  SQLLINE="DELETE FROM ${H01TAB} ${WHERE};"
  SqlWithComment

  DropXXXUrno ${AFTURNO} 
  DropXXXUrno ${CCAURNO} 
  DropXXXUrno ${TLXURNO} 
}

#
# DeleteLoop: Loop days from first day to last day, output SQL lines to stdout.
#              SQL lines may be processed in calling shell function or may output
#              to a file. If output to a file, be sure for hard disk space
#                
DeleteLoop()
{
  DAY1=$DATE1
  DAY2=$DAY1
  while expr $DAY1 \<= $DATE2 >/dev/null ; do
    SqlComment "====== START/END Delete data of day ${DAY1} ======"
    PrepareDeleteDay $DAY1
    DeleteDay $DAY1
    DAY2=$DAY1
    DAY1=`AddOneDay $DAY1`
  done
  SqlComment "====== START/END Delete data of day ${DAY2} ======"
}

# DropArchives drops all archives. This example only shows, how to do it.
DropArchives()
{
  # remember: '_' is any char in SQL select! But grep can filter more exact:
  DROPLIST=`sql_call "select table_name from user_tables 
                      where table_name like '___TAB__20%';" | \
            grep "^...TAB_[SW]20[0-3][0-9]_"`
  # cat <<EODROP
  sqlplus -s ${CEDADBUSER}/${CEDADBPW} <<EODROP
  `for t in ${DROPLIST} ; do
     echo "drop table $t;"
  done`
EODROP
}

test -z "${ACTION}" && PrintUsage
test -z "${SEASON}" && PrintUsage
case "${ACTION}" in
   archive) ;;
   delete)  ;;
   delete_by_online) ;;
   *) PrintUsage
esac

FROM_TO=`sql_call "select VPFR,VPTO from SEATAB${DBLINK} where SEAS='${SEASON}';"`
DATE1=`echo ${FROM_TO} | cut -d" " -f1 | cut -c1-8`
DATE2=`echo ${FROM_TO} | cut -d" " -f2 | cut -c1-8`
echo "`date +\"%Y%d%m %H:%M:%S:\"` Season ${SEASON}: from ${DATE1} to ${DATE2}"

YYC=`date +%Y`
MMC=`date +%m`
DDC=`date +%d`
YY1=`echo ${DATE1} | cut -c-4`
MM1=`echo ${DATE1} | cut -c5-6`
DD1=`echo ${DATE1} | cut -c7-8`

YY2=`echo ${DATE2} | cut -c-4`
MM2=`echo ${DATE2} | cut -c5-6`
DD2=`echo ${DATE2} | cut -c7-8`

CheckDateForm $DATE1
CheckDateForm $DATE2
CheckDateSeason $YY1 $MM1
CheckDateSeason $YY2 $MM2
CheckDateRange $DATE1 $DATE2

if [ ! -z "${FROM}" ] ; then
   unset MSG
   if [ ${FROM} -ge $DATE1 ] ; then
      if [ ${FROM} -le $DATE2 ] ; then
         DATE1=${FROM}
         if [ ! -z "${TO}" ] ; then
            if [ ${TO} -ge $DATE1 ] ; then
               if [ ${TO} -le $DATE2 ] ; then
                  DATE2=${TO}
               else
                  MSG="value ${TO} greater than to $DATE2"
               fi
            else
               MSG="value ${TO} less than to $DATE1"
            fi
         else # only one day:
            DATE2=${FROM}
         fi
      else
         MSG="value ${FROM} greater than to $DATE2"
      fi
   else
      MSG="value ${FROM} less than to $DATE1"
   fi
   if [ ! -z "${MSG}" ] ; then
     echo "-- $MSG"
     exit
   fi
fi

echo "`date +\"%Y%d%m %H:%M:%S:\"` Archiving requested for ${DATE1} to ${DATE2}"
echo "`date +\"%Y%d%m %H:%M:%S:\"` ${DBLNK_MSG}"

# summer or winter:
SEASON=S${YY1}
test $MM1 -gt 6 && SEASON=W${YY1}

# source tables:
AFTTAB=AFTTAB${DBLINK}
CCATAB=CCATAB${DBLINK}
TLXTAB=TLXTAB${DBLINK}
LOATAB=LOATAB${DBLINK}
H00TAB=H00TAB${DBLINK}
H01TAB=H01TAB${DBLINK}

# tmp tables
AFTURNO=AFT_ARC_URNOS
TLXURNO=TLX_ARC_URNOS
CCAURNO=CCA_ARC_URNOS

# target tables
AFTARC=AFTTAB_${SEASON}_${DATE1}_${DATE2}
CCAARC=CCATAB_${SEASON}_${DATE1}_${DATE2}
TLXARC=TLXTAB_${SEASON}_${DATE1}_${DATE2}
LOAARC=LOATAB_${SEASON}_${DATE1}_${DATE2}
H00ARC=H00TAB_${SEASON}_${DATE1}_${DATE2}
H01ARC=H01TAB_${SEASON}_${DATE1}_${DATE2}

# abort, when archive tables exist:
CHECK_TABLES=`sql_call "select table_name from user_tables where table_name in
          ('${AFTARC}','${CCAARC}','${TLXARC}','${LOAARC}','${H00ARC}','${H01ARC}');"`
case "${ACTION}" in
   archive) 
     if [ ! -z "${CHECK_TABLES}" ] ; then
       echo "Try to create following tables for Archiving, but they exist already: "
       echo "${CHECK_TABLES}"
       echo "Please delete or rename this table(s)...."
       return 2>/dev/null
       exit
     fi
     ;;
   delete)
     MISSING=
     for ttt in ${AFTARC} ${CCAARC} ${TLXARC} ${LOAARC} ${H00ARC} ${H01ARC} ; do
        case ${CHECK_TABLES} in
           *$ttt*) ;;
                *) MISSING=${MISSING}" $ttt"
                   echo "Missing archive table $ttt!"
        esac
     done
     if [ ! -z "${MISSING}" ] ; then
       echo "Try to delete archived data from online tables failes! "
       echo "The archive tables listed above do not exist! "
       echo "Please check for season and/or time range...."
       return 2>/dev/null
       exit
     fi
     ;;
   delete_by_online)
     AFTARC=${AFTTAB}
     CCAARC=${CCATAB}
     TLXARC=${TLXTAB}
     ;;
esac

# delete temporary tables, when they still exist:
CHECK_TABLES=`sql_call "select table_name from user_tables where table_name in
          ('${AFTURNO}','${CCAURNO}','${TLXURNO}');"`
if [ ! -z "${CHECK_TABLES}" ] ; then
   for t in ${CHECK_TABLES} ; do
     sql_call "`DropXXXUrno $t`"
   done
fi

Process()
{
   case "${ACTION}" in

      archive)
               # remove " # " before CEDA01 to use storage clause, adjust tablespace
               CreateXXX ${AFTARC} ${AFTTAB} # CEDA01  250000000
               CreateXXX ${CCAARC} ${CCATAB} # CEDA01   30000000
               CreateXXX ${TLXARC} ${TLXTAB} # CEDA01  200000000
               CreateXXX ${LOAARC} ${LOATAB} # CEDA01  180000000
               CreateXXX ${H00ARC} ${H00TAB} # CEDA01 2700000000
               CreateXXX ${H01ARC} ${H01TAB} # CEDA01  135000000
               CopyLoop
               ;;

      delete)
               DeleteLoop
               ;;

      delete_by_online) 
               # use following to delete online data without having archived:
               # WARNING: The script does not check, if you have archived data
               #          Only a date check (older than 7 month) is performed
               DeleteLoop
               ;;
      *)
   esac
}


# ProcessLoopSQL 2>&1 | tee ${DBG_PATH}/`basename $0`.log
(
# uncomment cat to get a list of SQL statements processed:
# cat <<EOSQL
sqlplus -s ${CEDADBUSER}/${CEDADBPW} <<EOSQL
    `Process`
EOSQL
) 2>&1 | tee "${LOG}"
echo
echo "Log file written to ${LOG}"