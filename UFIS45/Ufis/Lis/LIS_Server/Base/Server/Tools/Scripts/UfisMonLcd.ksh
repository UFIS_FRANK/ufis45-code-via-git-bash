LAST="UNKNOWN"
THIS=`basename $0`
PROCESS="lcdgat1,9350 lcdgat2,9360 lcdpub,9370 midhdl,8320"
PROCESS="lcdpub,9370"
syscnt=`ps -ef | grep "\./sysmon[ ]* /ceda" | grep -v grep | wc -l`
if [ "${syscnt}" = "1" ] ; then
 while [ "${syscnt}" = "1" ] ; do
  for PROC in ${PROCESS} ; do
    p=`echo ${PROC} | cut -d "," -f1`
    m=`echo ${PROC} | cut -d "," -f2`
    cnt=`ps -ef | grep -c "$p [0-9][0-9][0-9][0-9] 21"`
    if [ "${cnt}" = "0" ] ; then
         LAST=NOTFOUND
         echo "${THIS} `date  +%Y%m%d%H%M%S`: process $p not found, restarting <$p $m 21> ...."
       (cd ${RUN_PATH} ; cp ${BIN_PATH}/$p . ; $p $m 21 &) 
    else
         LAST=FOUND
         echo "${THIS} `date  +%Y%m%d%H%M%S`: process $p found, nothing to do for <$p $m 21> ...."
    fi
  done
  sleep 60
  syscnt=`ps -ef | grep "\./sysmon[ ]* /ceda" | grep -v grep | wc -l`
  if [ "${syscnt}" = "0" ] ; then
    echo No sysmon running! Aborting ....
  fi
 done
else
   echo No sysmon running!
fi