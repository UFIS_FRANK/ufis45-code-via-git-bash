#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<memory.h>
#include<malloc.h>
#include<signal.h>
#include<ctype.h>
#include<signal.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <time.h>

#define RC_SUCCESS 0
#define RC_NOTFOUND 1
#define XS_BUFF  128
static int igUTCDIFF = 0;
main(int argc,char *argv[],char *envp[])
{
	char pclTimeOut[100];
	char pclTimeIn[100];
	memset(pclTimeOut,0x00,100);
	strncpy(pclTimeIn,argv[1],100);

	/* LCDHDL calls */
	printf("---------------- LCDHDL (OK) -----------------\n");
	LcdUtcToLocal(pclTimeIn,pclTimeOut,-1);
	printf("DST:-1 TIMEZONE:<%d> UTC <%s> => LOCAL <%s> \n",timezone,pclTimeIn,pclTimeOut);
	LcdUtcToLocal(pclTimeIn,pclTimeOut,0);
	printf("DST: 0 TIMEZONE:<%d> UTC <%s> => LOCAL <%s> \n",timezone,pclTimeIn,pclTimeOut);
	LcdUtcToLocal(pclTimeIn,pclTimeOut,1);
	printf("DST: 1 TIMEZONE:<%d> UTC <%s> => LOCAL <%s> \n",timezone,pclTimeIn,pclTimeOut);
	printf("----------------------------------------------\n");
	/* DSPHDL calls */
	printf("---------------- DSPHDL ( unknown ) ----------\n");
	DspUtcToLocal(pclTimeIn,-1,"");
	DspUtcToLocal(pclTimeIn,0,"");
	DspUtcToLocal(pclTimeIn,1,"");
	printf("----------------------------------------------\n");

	return(0);
}

int LcdUtcToLocal(char *oldstr, char *newstr, int ipDst) 
{
  int c;
  char year[5], month[3], day[3], hour[3], minute[3],second[3];
  struct tm TimeBuffer, *final_result;
  time_t time_result;
	/*dbg(DEBUG,"UtcToLocal: IN <%s>",oldstr);*/

	/********** Extract the Year off CEDA timestamp **********/
  for(c=0; c<= 3; ++c)
    {
      year[c] = oldstr[c];
    }
  year[4] = '\0';
	/********** Extract month, day, hour and minute off CEDA timestamp **********/
  for(c=0; c <= 1; ++c)
    {
      month[c]  = oldstr[c + 4];
      day[c]    = oldstr[c + 6];
      hour[c]   = oldstr[c + 8];
      minute[c] = oldstr[c + 10];
      second[c] = oldstr[c + 12];
    }
	/********** Terminate the Buffer strings **********/
  month[2]  = '\0';
  day[2]    = '\0';
  hour[2]   = '\0';
  minute[2] = '\0';
  second[2] = '\0';


	/***** Fill a broken-down time structure incl. string to integer *****/
  TimeBuffer.tm_year  = atoi(year) - 1900;
  TimeBuffer.tm_mon   = atoi(month) - 1;
  TimeBuffer.tm_mday  = atoi(day);
  TimeBuffer.tm_hour  = atoi(hour);
  TimeBuffer.tm_min   = atoi(minute);
  TimeBuffer.tm_sec   = atoi(second);
  TimeBuffer.tm_isdst = ipDst;
	/***** Make secondbased timeformat and correct mktime *****/
  time_result = mktime(&TimeBuffer) - timezone;
	/***** Reconvert into broken-down time structure *****/
  final_result = localtime(&time_result);

  sprintf(newstr,"%d%.2d%.2d%.2d%.2d%.2d"
		,final_result->tm_year+1900
		,final_result->tm_mon+1
		,final_result->tm_mday
		,final_result->tm_hour
		,final_result->tm_min
		,final_result->tm_sec);

/*dbg(DEBUG,"UtcToLocal: OUT<%s>",newstr);*/
  return(0); /**** DONE WELL ****/
}	

/* DSPHDL UtcToLocal */
int DspUtcToLocal(char* pcpTime,int ipDst,char* pcpFormat)
{
  struct tm *_tm;
  time_t    now;
  char      _tmpc[6]; 
  int hour_gm,hour_local;
  int UtcDifference;
  char *pclTmp;
  char pclBuff[XS_BUFF];
  char pclTmpBuff[XS_BUFF];
  float fladdtime;
  now = time(NULL);
  _tm = (struct tm *)gmtime(&now);
  hour_gm = _tm->tm_hour;
  _tm = (struct tm *)localtime(&now);
  hour_local = _tm->tm_hour;

  memset(pclTmpBuff,0x00,XS_BUFF);
	strcpy(pclTmpBuff,pcpTime);

  if(hour_gm > hour_local)
    {
      UtcDifference = (hour_local+24-hour_gm);
    }else{
      UtcDifference = (hour_local-hour_gm);
    }
  igUTCDIFF=UtcDifference*60;
  if(pcpTime==NULL )
    {
      printf("pcpTime = NULL");
      return (time_t) 1;
    } /* end if */
  if((int)strlen(pcpTime) == 0 )
    {
      return RC_NOTFOUND;
    } /* end if */
  /*DeleteCharacterInString(pcpTime,cBLANK);*/

  fladdtime=0;
  pclTmp=NULL;
  strcpy(pclBuff,pcpFormat);
  if(strstr(pcpFormat,"HH:mm+")!=NULL||strstr(pcpFormat,"HH:mm-")!=NULL)
    {
      pclTmp=pclBuff;
      pclTmp+=5;
      fladdtime=atof(pclTmp);
      fladdtime*=(float)3600;
    }
  if(strlen(pcpTime) < 12 )
    {
      strcpy(pcpTime," ");
      return RC_SUCCESS;
    } /* end if */
  now = time(0L); 
  _tm = (struct tm *)localtime(&now); 
  _tmpc[2] = '\0';
  strncpy(_tmpc,pcpTime+12,2);
  _tm -> tm_sec = atoi(_tmpc);
  strncpy(_tmpc,pcpTime+10,2);
  _tm -> tm_min = atoi(_tmpc);
  strncpy(_tmpc,pcpTime+8,2);
  _tm -> tm_hour = atoi(_tmpc);
  strncpy(_tmpc,pcpTime+6,2);
  _tm -> tm_mday = atoi(_tmpc);
  strncpy(_tmpc,pcpTime+4,2);
  _tm -> tm_mon = atoi(_tmpc)-1;
  strncpy(_tmpc,pcpTime,4);
  _tmpc[4] = '\0';
  _tm -> tm_year = atoi(_tmpc)-1900;
  _tm -> tm_wday = 0;
  _tm -> tm_yday = 0;
  /*daylight saving time flag must be 0 for SCO and Solaris*/
  _tm -> tm_isdst=ipDst;
  now = mktime(_tm)-timezone+(time_t)fladdtime;
  _tm = (struct tm *)localtime(&now);

  if(strcmp(pcpFormat,"NH:mm")==0)
    {
      *pcpTime='\0';
      sprintf(pcpTime,"{NL}%02d:%02d",_tm->tm_hour,_tm->tm_min);
      return RC_SUCCESS;
    }
  if(strstr(pcpFormat,"HH:mm")!=NULL)
    {
      sprintf(pcpTime,"%02d:%02d",_tm->tm_hour,_tm->tm_min);
      return RC_SUCCESS;
    }
  if(strcmp(pcpFormat,"HHmm")==0)
    {
      *pcpTime='\0';
      sprintf(pcpTime,"%02d%02d",_tm->tm_hour,_tm->tm_min);
      return RC_SUCCESS;
    }
  if(strcmp(pcpFormat,"HH:MM")==0)
    {
      *pcpTime='\0';
      sprintf(pcpTime,"%02d:%02d",_tm->tm_hour,_tm->tm_min);
      return RC_SUCCESS;
    }
     
  memset(pcpTime,0x00,strlen(pcpTime));
  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d", _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
	  _tm->tm_min,_tm->tm_sec);
	printf("DST: %d TIMEZONE:<%d> UTC <%s> => LOCAL <%s> \n",ipDst,timezone,pclTmpBuff,pcpTime);
  memset(pcpTime,0x00,strlen(pcpTime));
	strcpy(pcpTime,pclTmpBuff);
  return RC_SUCCESS;
}
