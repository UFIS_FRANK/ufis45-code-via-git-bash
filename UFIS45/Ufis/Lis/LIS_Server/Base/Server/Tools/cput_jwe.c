#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Lis/LIS_Server/Base/Server/Tools/cput_jwe.c 1.1 2005/08/03 17:46:57SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************/
/* UGCCS Program Skeleton					      */
/*								      */
/* Author		: Jochen Hiller				      */
/* Date			: 30.04.2001				      */
/* Description		:					      */
/*								      */
/* Update history	:					      */
/* 20020924 JIM: added global variables mod_name, __CedaLogFile and outp    */
/*               to avoid core dump by null pointer in dbg                  */
/* 20030219 JIM: additional arguments WKS, USER, TW_START and TW_END        */
/* 20040706 JIM: -102: delete all queues of CPUT                            */
/* 20040706 JIM: -103: delete all queues of *UTIL*                          */
/* 20040706 JIM: -104: delete one dedicated queue of *UTIL*  and *CPUT*     */
/* 20040706 JIM: QUE_GETBIG instead of QUE_GET, allocates memory in que()   */
/* 20040707 JIM: SYSQCP only uses I_SIZE Bytes (Unix-Queue-Size) for        */
/*               que_overview, so printf has to stop before this limit      */
/* 20040715 JIM: toggle the debug_level of SYSQCP with 'cput 0 7 -105'      */
/*               Switch off debug_level of SYSQCP with 'cput 0 8 -105'      */
/* 20040728 JIM: for DEBUG only: use SendNextForQueue to set one NEXTNW flag*/
/*               in the the SYSQCP, i.e. 'cput 0 7560 -105' for rulfndp     */
/* 20040804 JIM: for cc: declare SendNextForQueue before usage              */
/* 20040804 JIM: handled nearly all gcc warnings                            */
/* 20040813 MCU: alarm replaced by wait in loop (for MQ)                    */
/* 20040813 JIM: prototype que() removed                                    */
/* 20040813 JIM: display hint on SYSQCP_stat.log                            */
/* 20040813 JHE: using exit codes usage/error/timeout/ok (see XC_ consts)   */
/* ********************************************************************/
/* This program is a UGCCS main program */

#define U_MAIN
#define CEDA_PRG
#define QUE_INTERNAL 
#include <stdio.h>	
#include <unistd.h>
#include "glbdef.h"
#include "ugccsma.h"
#include "quedef.h"
#include "uevent.h"
#include "tools.h"
#include "sthdef.h"
#include "quedef.h"
#include <stdarg.h>
#include <time.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <sys/stat.h>


#define QO_INFO(data)   "\n%5u\t%s%5u\t%5u%5u%5u%5u%5u\t%u\t%8lu",data.id,data.name,data.total,data.nbr_items[0],data.nbr_items[1],data.nbr_items[2],data.nbr_items[3],data.nbr_items[4],data.status,data.msg_count
#define HTML_QO_INFO(data)   "\n%5u\t%s%5u\t%5u%5u%5u%5u%5u\t%u\t%8lu",data.id,data.name,data.total,data.nbr_items[0],data.nbr_items[1],data.nbr_items[2],data.nbr_items[3],data.nbr_items[4],data.status,data.msg_count
/*  #define HTML_QO_INFO(data)   "\n%05u  %10s %05u  %05u  %05u  %05u  %05u  %05u  %u  %8lu",data.id,data.name,data.total,data.nbr_items[0],data.nbr_items[1],data.nbr_items[2],data.nbr_items[3],data.nbr_items[4],data.status,data.msg_count */

#ifndef max
   #define max(a,b)    (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
   #define min(a,b)    (((a) < (b)) ? (a) : (b))
#endif

extern int debug_level = TRACE;

/* ******************************************************************** */
/* External variables							*/
/* ******************************************************************** */

/* ******************************************************************** */
/* Global variables							*/
/* ******************************************************************** */
char	*mod_name;
static char	pcgCedaLogFile[128];
static FILE *pfgDbgLog = NULL;
static char pcgMyName[32];
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp = NULL;*/
static	EVENT	event;			/* The event        		*/
static int mode;
/* ******************************************************************** */
/* External functions							*/
/* ******************************************************************** */
extern	int	init_que();		/* Attach to UGCCS queues	*/
/* extern	int	que(int,int,int,int,int,ITEM **); /* UGCCS queuing routine*/
extern	void	catch_all();		/* Catches all signals		*/
extern  int	send_message(int,int,int,int,char*);
extern	int	atoi(const char *);
    
/* ******************************************************************** */
/* Function prototypes							*/
/* ******************************************************************** */
static int 	init_cput();
static void	handle_sig();		/* Handles signals		*/
/* static void	handle_err(int);*/	/* Handles general errors	*/
static void	handle_qerr(int);	/* Handles queuing errors	*/
static int get_q_id(char*);
static void	one_queue_overview(int pmod_id);
static void	queue_overview(int ipWhat,int ipDest);
static void TrimRight(char *pcpBuffer);
static int delete_queue(int que_ID,int mod_id);
static int handle_data_input(int ipDestination,int ipPriority,char* pcpCommand,
                      char *pcpTable,char *pcpFields,char* pcpData,
                      char* pcpSelection,int ipTimeWait,
                      char* pcpDst,char* pcpRcv,char* pcpTws,char* pcpTwe);
static void get_time(char *s);
static void dbglog(int level,char *fmt, ...);
static void cput45_terminate(int ipExVal);

static void set_sysqcp_dbg(int level);
static void set_sysqcp_nextnw(int module);

/*
 * 04 oct 04 jhe added exit codes consts
 * original nonzero exit codes are left unchanged
 */
#define XC_OK 0
#define XC_USAGE 1
#define XC_ERROR 2
#define XC_TIMEOUT 3

/* ******************************************************************** */
/*									*/
/* The MAIN program							*/
/*									*/
/* ******************************************************************** */
static int data;
MAIN
{
  int rc=0;			/* Return code			*/
  int dest,i;
  int loop;
  int delay;
  short	cmd;
  char pid[200];			/* command			*/
  char cmd1[100];
  char shellcommand[200];
  char filename[100];
	char pclTimeStamp[32];
  char dummyfile[100]="/ceda/bin/dummy";
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;
  FILE *fp;
	int ilMaxFileSize = (1024 * 1024 * 2);
	struct stat rlLfBuff;
  int   iLen     = 0;
  mod_name = "cput";

  dbglog(TRACE,"============ NEW CPUT SESSION ===========");
	if (pfgDbgLog != NULL)
	{
    if (fstat(fileno(pfgDbgLog), &rlLfBuff) != RC_FAIL)
    {
      if (rlLfBuff.st_size >= ilMaxFileSize)
      {
        dbglog(TRACE,"SWITCHING LOGFILE (SIZE NOW = %d)", rlLfBuff.st_size);
				get_time(pclTimeStamp);
				pclTimeStamp[14] = 0x00;
        sprintf(filename,"%s/%s_till_%s.bak.log","/ceda/debug",mod_name,pclTimeStamp);
				dbglog(TRACE,"RENAMING TO <%s>", filename);
				fclose(pfgDbgLog);
				pfgDbgLog = NULL;
				rename(pcgCedaLogFile, filename);
        dbglog(TRACE,"============ NEW CPUT SESSION ===========");
      }
    }
  }

  init();  
  
	for (i=1;i<argc;i++)
	{
		if (argv[i] != NULL)
		{
			dbglog(TRACE,"ARG(%d): <%s>", i, argv[i]);
		}
		else
		{
			dbglog(TRACE,"ARG(%d): <NULL>", i);
		}
	}
  
  if ( argc < 4 ) {
    dbglog(TRACE,"WRONG NUMBER OF ARGUMENTS (%d)", argc);
    printf("\twrong number of parameters!  usage:\n");fflush(stdout);
    printf("\tcput mode destination command opt( [loop]  opt[delay])\n");fflush(stdout);
    printf("\tmode 0=Commandline,1=HTML full mode,2=HTML only data\n");fflush(stdout);
    printf("\tdestination as mod_id or mod_name\n");fflush(stdout);
    printf("\tcommand as string (EVENT_DATA) or\n");fflush(stdout);
    printf("\tcommand as number (SYS_EVENT)  or\n");fflush(stdout);
    printf("\tSHUTDOWN = -1 RESET = -2 (SYS_EVENT)\n");fflush(stdout);
    printf("\tSTOP = -3 starts dummy (sig kill -9)\n");fflush(stdout);
    printf("\tother kills: -9 ,-15 \n");fflush(stdout);
    printf("\t====Optional=======================\n");fflush(stdout);
    printf("\tnumber of loops, delay in seconds\n");fflush(stdout);
    printf("\t====Usage as Interface=============\n");fflush(stdout);
    printf("\tcput mode destination priority command table(full) fields data "
           "selection timeout wks user TW_START TW_END\n"),fflush(stdout);
    printf("\tcput mode destination priority command table(full) fields data selection timeout\n"),fflush(stdout);
    printf("\tcput mode destination priority command table(full) fields data selection \n "),fflush(stdout);
    printf("\tcput mode destination command table(full) fields data selection\n"),fflush(stdout);
    printf("\tcput mode destination command fields data selection timeout \n"),fflush(stdout);
    printf("\tcput mode destination command fields data selection \n"),fflush(stdout);
    printf("\t====Discription of parameters======\n");fflush(stdout);
    printf("\tpriority value 1-5\n");fflush(stdout);
    printf("\ttable full name like AFTTAB\n");fflush(stdout);
    printf("\tfields and data as comma sep string\n");fflush(stdout);
    printf("\tselection -> where clause\n");fflush(stdout);
    printf("\ttimeout for queue get big in sec,0 for ever,-1 ignore answer\n");fflush(stdout);
    printf("\t====Answer format is===============\n\tCommand\n\tSelection\n\tFields\n\tData\n"),fflush(stdout);
    printf("\tto stdout like answer from sqlhdl\n"),fflush(stdout);
    cput45_terminate(XC_USAGE);
  } /* end if */
  rc=init_cput();
  if(rc!=RC_SUCCESS){
    cput45_terminate(XC_USAGE);
  }/* end of if */
  mode=atoi(argv[1]);
  if(mode<0||mode>2)
    {
      dbglog(TRACE,"Invalid mode %s!",argv[1]);
      dbglog(TRACE,"Valid modes: 0=Commandline, 1=HTML full mode, 2=HTML only data");
      printf("\nInvalid mode %s! use:",argv[1]);
      printf("\tmode 0=Commandline,1=HTML full mode,2=HTML only data\n");fflush(stdout);
      cput45_terminate(XC_USAGE);
    }
  if(argc>4)
    loop = atoi(argv[4]);
  else
    loop=1;
  if(argc>5)
    delay= atoi(argv[5]);
  else
    delay=0;
  catch_all(handle_sig);		/* handles signal		*/
  if (argc>6)
    {
      data=1;
      /*this cput is used as real interface*/
      if(strlen(argv[2])>5&&atoi(argv[2])==0)
	{
	  /*modname*/
	
	  dest=get_q_id(argv[2]);
	}else{
	  /*modid*/
	  dest=atoi(argv[2]);
	}
		dbglog(TRACE,"GOT %d ARGUMENTS. DEST=%d", argc, dest);
      switch(argc)
	{
	case 14:
	  /*with all entries*/
	  handle_data_input(dest,atoi(argv[3]),argv[4],argv[5],argv[6],argv[7],argv[8],
    atoi(argv[9]),argv[10],argv[11],argv[12],argv[13]);
	  break;
	case 13:
	  /*with all entries*/
	  handle_data_input(dest,atoi(argv[3]),argv[4],argv[5],argv[6],argv[7],argv[8],
    atoi(argv[9]),argv[10],argv[11],argv[12]," ");
	  break;
	case 12:
	  /*with all entries*/
	  handle_data_input(dest,atoi(argv[3]),argv[4],argv[5],argv[6],argv[7],argv[8],
    atoi(argv[9]),argv[10],argv[11],"   ,CPUT"," ");
	  break;
	case 11:
	  /*with all entries*/
	  handle_data_input(dest,atoi(argv[3]),argv[4],argv[5],argv[6],argv[7],argv[8],
    atoi(argv[9]),argv[10],"EXCO","   ,CPUT"," ");
	  break;
	case 10:
	  /*with all entries*/
	  handle_data_input(dest,atoi(argv[3]),argv[4],argv[5],argv[6],argv[7],argv[8],
    atoi(argv[9]),"CPUT","EXCO","   ,CPUT"," ");
	  break;
	case 9 :
	  /*timeout is missing -> std value is 1 sec*/
	  handle_data_input(dest,atoi(argv[3]),argv[4],argv[5],argv[6],argv[7],argv[8],
    1,"CPUT","EXCO","   ,CPUT"," ");
	  break;
	case 8 :
	  /*priority is missing -> std value is 1 or priority and table is missing but timeout is there*/
	  if(strstr(argv[4],"TAB")==NULL&&strlen(argv[4])!=6)
	    {
	      handle_data_input(dest,1,argv[3],"",argv[4],argv[5],argv[6],atoi(argv[7]),
        "CPUT","EXCO","   ,CPUT"," ");
	    }else{
	      handle_data_input(dest,1,argv[3],argv[4],argv[5],argv[6],argv[7],1,
        "CPUT","EXCO","   ,CPUT"," ");
	    }
	  break; 
	case 7 :
	  /*table is missing -> Null*/
	  handle_data_input(dest,1,argv[3],"",argv[4],argv[5],argv[6],1,
    "CPUT","EXCO","   ,CPUT"," ");
	  break;
	default:
	  printf("\nToo many Parameters %d\n",argc);
	  cput45_terminate(XC_USAGE);
	}
    }else{
      data=0;
      /*this is only for shorttype commands*/
      if(strlen(argv[2])>5)
	{
	  /*modname*/
	  dest=get_q_id(argv[2]);
	}else{
	  /*modid*/
	  dest=atoi(argv[2]);
	}
      if(!strcmp(argv[3],"-1"))
	{
	  rc=-1;
	  cmd=SHUTDOWN;
	}
      if(!strcmp(argv[3],"-2"))
	{
	  rc=-2;
	  cmd=RESET;
	}
      if(!strcmp(argv[3],"-99"))
	one_queue_overview(dest);
      if(!strcmp(argv[3],"-100"))
	queue_overview(0,0);
      if(!strcmp(argv[3],"-101"))
	queue_overview(1,0);
      if(!strcmp(argv[3],"-102"))
	queue_overview(2,0);
      if(!strcmp(argv[3],"-103"))
	queue_overview(3,0);
      if(!strcmp(argv[3],"-104"))
	queue_overview(4,atoi(argv[2]));
      if(!strcmp(argv[3],"-105"))
	set_sysqcp_dbg(atoi(argv[2]));
      if(!strcmp(argv[3],"-106"))
	set_sysqcp_nextnw(atoi(argv[2]));
      if(!strcmp(argv[3],"-110044"))
	queue_overview(5,atoi(argv[2]));
      if(!strcmp(argv[3],"-3"))
	{
	  if(atoi(argv[2])!= 0)
	    {
	      printf("\n\tUSE ASSOCIATED NAME");fflush(stdout);
	      cput45_terminate(XC_USAGE);
	    }
	  fp = fopen ("/ceda/bin/dummy", "r");
	  if(fp==NULL)
	    {
	      fclose(fp);fflush(fp);
	      printf("\nFIRST CREATE \"dummy\" FILE !!!\n");fflush(stdout);
	      cput45_terminate(XC_USAGE);
	    }
	  fclose(fp);fflush(fp);
	  sprintf(filename,"/ceda/bin/%s.cput",argv[1]);
	  fp = fopen (filename, "r");
	  if(fp!=NULL)
	    {
	      fclose(fp);fflush(fp);
	      printf("\norg file %s.cput exists save first !!! and remove it\n",argv[1]);fflush(stdout);
	      cput45_terminate(XC_USAGE);
	    }
	  sprintf(filename,"/ceda/bin/%s",argv[2]); 
	  rename(filename,"/ceda/bin/test.cput.save"); 
	  rename(dummyfile,filename);
	  sprintf(shellcommand,"ps -e | grep %s | awk ' { printf $1\",\" > \"/ceda/tmp/cput.tmp\"}'",argv[1]);  
	  fp=popen(shellcommand,"w");
	  pclose(fp);
	  fp=fopen("/ceda/tmp/cput.tmp","r");
	  if(fp!=NULL)
	    {
	      fscanf(fp,"%s",pid);
	      fclose(fp);fflush(fp);
	    }
	  pid[strlen(pid)-1]='\0'; 
	  if(strchr(pid,',')==NULL)
	    {
	      kill(atoi(pid),9);
	      printf("\n\tkill pid= %s\n",pid);fflush(stdout);
	      printf("\n\t%s runs as dummy\n\n",argv[2]);fflush(stdout);
	      sleep(2);
	    }else{
	      printf("\n\tFOUND MORE THAN ONE PID = %s MODNAME IS NOT UNIQUE\n\n",pid);fflush(stdout);
	    }
	  rename(filename,dummyfile); 
	  rename("/ceda/bin/test.cput.save",filename); 
	  remove("/ceda/tmp/cput.tmp"); 
	  cput45_terminate(XC_USAGE);
	} 
      if(!strcmp(argv[3],"-9")||!strcmp(argv[3],"-15"))
	{
	  if(atoi(argv[2])!= 0)
	    {
	      printf("\n\tUSE ASSOCIATED NAME");fflush(stdout);
	      cput45_terminate(XC_USAGE);
	    }
	  sprintf(shellcommand,"ps -e | grep %s | awk ' { printf $1\",\" > \"/ceda/tmp/cput.tmp\"}'",argv[2]);  
	  fp=popen(shellcommand,"w");
	  pclose(fp);
	  fp=fopen("/ceda/tmp/cput.tmp","r");
	  if(fp!=NULL)
	    {
	      fscanf(fp,"%s",pid);
	      fclose(fp);fflush(fp);
	    }
	  pid[strlen(pid)-1]='\0'; 
	  if(strchr(pid,',')==NULL)
	    {
	      kill(atoi(pid),atoi(argv[3])*-1);
	      printf("\n\tkill %d  %s \n\n",atoi(argv[3]),pid);fflush(stdout);
	    }else{
	      printf("\n\tFOUND MORE THAN ONE PID = %s MODNAME IS NOT UNIQUE\n\n",pid);fflush(stdout);
	    }
	  remove("/ceda/tmp/cput.tmp"); 
	  cput45_terminate(XC_USAGE);
	}
      if(rc>=0&&atoi(argv[3])==0)
	{
	  strcpy(cmd1,argv[3]);
	  iLen= sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK);
	  prlOutEvent = (EVENT*)malloc((size_t)iLen);
	  memset((void*)prlOutEvent, 0x00, iLen);
	  /* set event structure... */
	  prlOutEvent->type         = SYS_EVENT;
	  prlOutEvent->command      = EVENT_DATA;
	  prlOutEvent->originator   = 9;
	  prlOutEvent->retry_count  = 0;
	  prlOutEvent->data_offset  = 0;
	  prlOutEvent->data_length  = 0; 
	  prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
	  prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
	  strcpy(prlOutCmdblk->command,cmd1);
	  for(i=0;i<loop;i++)
	    {
	      if((rc=que(QUE_PUT,dest,9,1,iLen,(char *)prlOutEvent))!=RC_SUCCESS)
		{
		  handle_qerr(rc);
		  cput45_terminate(XC_ERROR);
		}else{
		  if(mode==0)
		    printf("\n\tsend %d string command ok\n",i+1);fflush(stdout);
		} /* end if */
	      sleep(delay);
	    }
	}else if(rc>=0)
	  {
	    cmd=atoi(argv[3]);
	    event.type = SYS_EVENT;
	    event.command = cmd;
	    event.originator = 9;
	    event.retry_count = 0;
	    event.data_offset = 0;
	    event.data_length = 0;
	    for(i=0;i<loop;i++)
	      {
		if((rc=que(QUE_PUT,dest,9,1,sizeof(EVENT),(char *)&event))!=RC_SUCCESS)
		  {
		    handle_qerr(rc);
		    cput45_terminate(XC_ERROR);
		  }else{
		    if(mode==0)
		      printf("\n\tsend %d number command ok\n",i+1);fflush(stdout);
		  } /* end if */
		sleep(delay);
	      }
	  }
      if(rc==-1||rc==-2)
	{
	  event.type = SYS_EVENT;
	  event.command = cmd;
	  event.originator = 9;
	  event.retry_count = 0;
	  event.data_offset = 0;
	  event.data_length = 0;
	  if((rc=que(QUE_PUT,dest,9,1,sizeof(EVENT),(char *)&event))!=RC_SUCCESS)
	    {
	      handle_qerr(rc);
	      cput45_terminate(XC_ERROR);
	    }else{
	      if(mode==0)
		printf("\n\tsend reset or shutdown ok\n");fflush(stdout);
	    } /* end if */
	}
    }
  cput45_terminate(XC_OK);
  return 0;
} /* end of MAIN */

/* ******************************************************************** */
/* The initialization routine						*/
/* ******************************************************************** */
static int init_cput()
{
	int	cnt = 0;
	
	/* Attach to the SYMAP queues */
	while( (init_que() !=0) && (cnt<10) ){
		cnt++;
		sleep(6);		/* Wait for QCP to create queues */
	} /* end while */
	
	if(cnt>=10){
    dbglog(TRACE,"ERROR: UNABLE TO INIT QUEUES");
		printf("CPUT: ERROR: unable to init que!\n");fflush(stdout);
		return(FATAL);
	}/* end of if */

  dbglog(TRACE,"ATTACHED TO CEDA MSG QUEUES");
	return(RC_SUCCESS);
	
} /* end of initialize */

/* ******************************************************************** */
/* The handle signals routine						*/
/* ******************************************************************** */
static void handle_sig()
{
	
	return;
	
} /* end of handle_sig */
/* ******************************************************************** */
/* The handle general error routine					*/
/* ******************************************************************** */
/* static void handle_err(int err)
{
	return;

}*/ /* end of handle_err */

/* ******************************************************************** */
/* The handle queuing error routine					*/
/* ******************************************************************** */
static void handle_qerr(int err)
{
	switch(err) {
	case	  QUE_E_FUNC	:		/* Unknown function */
	  printf("\nCPUT ERROR:%d Unknown function\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_MEMORY	:		/* Malloc reports no memory */
	  printf("\nCPUT ERROR:%d Malloc reports no memory\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_SEND	:		/* Error using msgsnd */
	  printf("\nCPUT ERROR:%d Error using msgsnd\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_GET	:		/* Error using msgrcv */
	  printf("\nCPUT ERROR:%d Error using msgrcv\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_EXISTS	:		/* Route/Queue exists */
	  printf("\nCPUT ERROR:%d Route/Queue exists\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_NOFIND	:		/* Not found ( ex. route ) */
	  printf("\nCPUT ERROR:%d Not found ( ex. route )\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_ACKUNEX	:		/* Unexpected ACK received */
	  printf("\nCPUT ERROR:%d Unexpected ACK received\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_STATUS	:		/* Unknown queue status */
	  printf("\nCPUT ERROR:%d Unknown queue status\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_INACTIVE:      		/* Queue is inactive */
	  printf("\nCPUT ERROR:%d Queue is inactive\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_MISACK	:		/* Missing ACK */
	  printf("\nCPUT ERROR:%d Missing ACK\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_NOQUEUES:		/* The queues don't exist */
	  printf("\nCPUT ERROR:%d The queues don't exist\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_RESP	:		/* No response on CREATE */
	  printf("\nCPUT ERROR:%d No response on CREATE\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_FULL	:		/* Table full */
	  printf("\nCPUT ERROR:%d Table full\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_NOMSG	:		/* No message on queue */
	  printf("\nCPUT ERROR:%d No message on queue\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_INVORG	:		/* Mod id by que call is 0 */
	  printf("\nCPUT ERROR:%d Mod id by que call is 0\n",err);fflush(stdout); 
	  break;
	case	  QUE_E_NOINIT	:		/* Queues is not initialized*/
	  printf("\nCPUT ERROR:%d Queues is not initialized\n",err);fflush(stdout); 
	  break;
	default			:		/* Unknown queue error */
	  printf("\nCPUT ERROR:%d\n",err);fflush(stdout); 
			break;
	} /* end switch */
	return;

} /* end of handle_qerr */
static int get_q_id (char *Pnam)
{
  int   rc = RC_SUCCESS;        /* Return code                  */
  int recno=0;
  char buf[30];
  int cnt=0;
  int ready = FALSE;
 
  rc = sgs_get_no_of_record(PNTAB, buf);
 
  if (rc > 0)
  {
    cnt = rc;
    rc = RC_SUCCESS;
  }
  else
  {
    rc = RC_FAIL;
  } /* fi */
 
  for (recno=0; ! ready && rc == RC_SUCCESS && recno < cnt; recno++)
  {
    rc = sgs_get_record (PNTAB, recno, buf);
    if (rc == RC_SUCCESS)
    {
      if (strncmp (buf, Pnam, strlen(Pnam)) == 0)
      {
        ready = TRUE;
#if defined(_SNI) || defined(_HPUX_SOURCE) || defined(_SOLARIS)|| defined(_LINUX)
	rc = *((short *) (buf+8));
#else
	rc = *((int *) (buf+8));
#endif
      } /* fi */
    } /* fi */
  } /* for */
  if(data==0&&mode==0)
    printf ("\n\tsend to %s -> %d \n", Pnam,rc);fflush(stdout);
  return rc;
} /* tool_get_q_id */



static void one_queue_overview(int pmod_id)
{
  ITEM          item1;
  ITEM		*item;
  QUE_INFO	*que_info;
  int tag;
  int		len;
  int		return_value;
  tag = (getpid()%1000) + 30000;
  if ( que(QUE_CREATE,0,tag,0,5,"CPUT") != RC_SUCCESS ) 
    {
      cput45_terminate(XC_ERROR);
    }  
  alarm(2);
  if ( que(QUE_RESP,0,tag,0,sizeof(ITEM),(char *)&item1) != RC_SUCCESS ) 
    {
      cput45_terminate(XC_ERROR);
    } /* end if */
  alarm(0);
	
  mod_id = item1.originator;
 
  /* Request the queue overview */
   
  return_value = que(QUE_O_QOVER,pmod_id ,mod_id,5,0,0);
  if ( return_value != RC_SUCCESS ) 
    {
      dbglog(TRACE,"DELETING QUEUE %d", mod_id);
      que(QUE_DELETE,mod_id,mod_id,0,0,0);
      cput45_terminate(XC_ERROR);
    } /* end if */
  len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
  item = (ITEM *) malloc(len);
  if ( item == (ITEM *) NUL ) 
    {
      dbglog(TRACE,"DELETING QUEUE %d", mod_id);
      que(QUE_DELETE,mod_id,mod_id,0,0,0);
      cput45_terminate(XC_ERROR);
    } /* end if */
	
  if (que(QUE_GET,0,mod_id,5,len,(char *)item) != RC_SUCCESS ) 
    {
      dbglog(TRACE,"DELETING QUEUE %d", mod_id);
      que(QUE_DELETE,mod_id,mod_id,0,0,0);
      free((char *) item);
      
      cput45_terminate(XC_ERROR);
    } /* end if */
	
  if ( item->msg_length == FATAL ) 
    {
      dbglog(TRACE,"DELETING QUEUE %d", mod_id);
      que(QUE_DELETE,mod_id,mod_id,0,0,0);
      free((char *) item);
      cput45_terminate(XC_ERROR);
    } /* end if */
      dbglog(TRACE,"DELETING QUEUE %d", mod_id);
  que(QUE_DELETE,mod_id,mod_id,0,0,0);
  que_info = (QUE_INFO *) &(item->text[0]);
  if(mode==0)
    {
      printf(QO_INFO(que_info->data[0]));fflush(stdout);
      printf("\n");
    }
  if(mode==1)
    {
      printf(HTML_QO_INFO(que_info->data[0]));fflush(stdout);
    }
  if(mode==2)
    {
      printf(HTML_QO_INFO(que_info->data[0]));fflush(stdout);
    }
  cput45_terminate(XC_OK);
}
static int handle_data_input(int ipDestination,int ipPriority,char* pcpCommand,
                             char* pcpTable,char *pcpFields,char* pcpData,
                             char* pcpSelection,int ipTimeWait,
                             char* pcpRcv,char* pcpDst,char* pcpTws,char* pcpTwe)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  ITEM          item1;
  ITEM          *item2=NULL;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;
  EVENT   *prlEvent  = NULL;
  BC_HEAD *prlBCHead = NULL;
  CMDBLK  *prlCmdblk = NULL;
  int tag;


	dbglog(TRACE,"NOW HANDLE DATA INPUT (CREATE QUEUE)");
	mod_id = 9999;

	if (ipTimeWait >= 0)
	{
  tag = (getpid()%1000) + 30000;
  if ( que(QUE_CREATE,0,tag,0,5,"CPUT") != RC_SUCCESS ) 
    {
			dbglog(TRACE,"ERROR ON QUE_CREATE");
      cput45_terminate(XC_ERROR);
    }  
    alarm(2);
  if ( que(QUE_RESP,0,tag,0,sizeof(ITEM),(char *)&item1) != RC_SUCCESS ) 
    {
			dbglog(TRACE,"ERROR ON QUE_RESP");
      cput45_terminate(XC_ERROR);
    } /* end if */
    alarm(0);
  mod_id = item1.originator;
	  dbglog(TRACE,"ATTACHED TO ANSWER QUEUE (%d)", mod_id);
  }
	else
	{
	  dbglog(TRACE,"NO ANSWER EXPECTED. USING MOD_ID (%d)", mod_id);
	}

	dbglog(TRACE,"DST: (%d)", ipDestination);
	dbglog(TRACE,"PRI: (%d)", ipPriority);
	dbglog(TRACE,"TIM: (%d)", ipTimeWait);
	dbglog(TRACE,"WKS: <%s>", pcpRcv);
	dbglog(TRACE,"USR: <%s>", pcpDst);
	dbglog(TRACE,"TWS: <%s>", pcpTws);
	dbglog(TRACE,"TWE: <%s>", pcpTwe);
	dbglog(TRACE,"CMD: <%s>", pcpCommand);
	dbglog(TRACE,"TBL: <%s>", pcpTable);
	dbglog(TRACE,"FLD: <%s>", pcpFields);
	dbglog(TRACE,"DAT: <%s>", pcpData);
	dbglog(TRACE,"SEL: <%s>", pcpSelection);

  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 128;
  /* memory for prlOutEvent */
  if((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);
      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command    = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT); 
      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      if(ipTimeWait>=0)
			{
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)0;/*spaeter nur bei disconnect*/
      }
			else
			{
        prlOutBCHead->rc = NETOUT_NO_ACK;
			}
      strncpy(prlOutBCHead->dest_name,pcpDst,10);
      strncpy(prlOutBCHead->recv_name,pcpRcv,10);
      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCommand);
      if (pcpTable != NULL)
	{
	  strcpy(prlOutCmdblk->obj_name,pcpTable);
	}
      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTws,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwe,32);
      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      
      /* setting selection inside event */
      strcpy(prlOutCmdblk->data,pcpSelection);
      /* setting field-list inside event */
      strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
      /* setting data-list inside event */
      strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
	
			dbglog(TRACE,"SENDING EVENT TO %d", ipDestination);
      if((ilRc = que(QUE_PUT,ipDestination,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
	 != RC_SUCCESS)
	{
			dbglog(TRACE,"GOT QUEUE ERROR %d", ilRc);
      dbglog(TRACE,"DELETING QUEUE %d", mod_id);
	  que(QUE_DELETE,mod_id,mod_id,0,0,0);
	  printf("SendEvent: QUE_PUT returns: <%d>", ilRc);
	  cput45_terminate(XC_ERROR);
	}
      /* free memory */
      free((void*)prlOutEvent); 
    }

		/*printf("TimeWait=%d",ipTimeWait);*/

  if(ipTimeWait>=0)
    {
		int ilRc;
		time_t tlStart = time(NULL);
    dbglog(TRACE,"WAITING FOR ANSWER (WAKEUP=%d)",ipTimeWait);

      /*alarm(ipTimeWait);*/

		do
		 {
      ilRc = que(QUE_GETBIGNW,0,mod_id,4,0,(char *)&item2);
      if(ilRc == QUE_E_NOMSG ) 
			{
				 time_t tlNow = time(NULL);
							
				 if (tlNow - tlStart >= ipTimeWait)
				 {
	  				printf("\nNo Answer after %d  Start %d Now %d Now-Start %d sec.\n",
							ipTimeWait,tlStart,tlNow,tlNow-tlStart);
	  				que(QUE_DELETE,mod_id,mod_id,0,0,0);
	  				exit(XC_TIMEOUT);
				 }
			}
			else if(ilRc != RC_SUCCESS ) 
	{
dbglog(TRACE,"TIMEOUT: NO RESPONSE AFTER %d SECONDS",ipTimeWait);	  
printf("\nQCP failure %d\n",ipTimeWait);
		handle_qerr(ilRc);
dbglog(TRACE,"DELETING QUEUE %d", mod_id);
	  que(QUE_DELETE,mod_id,mod_id,0,0,0);
	  cput45_terminate(XC_ERROR);
	} /* end if */
	} while (ilRc == QUE_E_NOMSG );

      if( item2->msg_length == FATAL ) 
	{
      dbglog(TRACE,"DELETING QUEUE %d", mod_id);
	  que(QUE_DELETE,mod_id,mod_id,0,0,0);
	  printf("\nNo Answer after %d sec.",ipTimeWait);
	  cput45_terminate(XC_TIMEOUT);
	} /* end if */
	dbglog(TRACE,"GOT THE ANSWER");
  dbglog(TRACE,"DELETING QUEUE %d", mod_id);
      que(QUE_DELETE,mod_id,mod_id,0,0,0);
      if(item2!=NULL)
	{
	  prlEvent = (EVENT *) item2->text;
	  prlBCHead  = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT));
	  prlCmdblk= (CMDBLK  *) ((char *)prlBCHead->data);
	  pclSelection = prlCmdblk->data;
	  pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
	  pclData = (char*)pclFields + strlen(pclFields) + 1;
	  dbglog(TRACE,"ANSW SEL: <%s>", pclSelection);
	  dbglog(TRACE,"ANSW FLD: <%s>", pclFields);
	  dbglog(TRACE,"ANSW DAT: <%s>", pclData);
	  if(mode==0)
	    printf("\n%s\n",pclData);
	  if(mode==1)
	    printf("\n%s\n%s\n%s\n%s\n",prlCmdblk->command,pclSelection,pclFields,pclData);
	  if(mode==2)
	    printf("\n%s\n",pclData);
	}else{
	  printf("\nNo Answer after %d sec.",ipTimeWait);
	}
    }
		
		/*
		else{

      dbglog(TRACE,"DELETING QUEUE %d", mod_id);
      que(QUE_DELETE,mod_id,mod_id,0,0,0);
    }
		*/

	fflush(stdout);

  return ilRc;
}
/* ******************************************************************** */
/*									*/
/* The following routine displays the queue overview.			*/
/*									*/
/* ******************************************************************** */

static void	queue_overview(int ipWhat, int ipDest)
{
  ITEM		*item;
  ITEM          item1;
  QUE_INFO	*que_info;
  char pclTmp[128];
  int		info_count;
  int           rc;
  int		return_value;
  int tag;
  int ilMaxValid;
  int	ilSomeFound;

  /* 20040707 JIM: SYSQCP legt nur I_SIZE Bytes auf die Unix-Queue, daher
                   kann es sein, da� die angebliche Anzahl der Queue-Infos 
                   nicht mit der wirklich gelieferten Datenmenge �bereinstimmt
  */ 
  ilMaxValid = ( I_SIZE - sizeof(ITEM) - sizeof(QUE_INFO) ) / sizeof(QINFO);
  
  tag = (getpid()%1000) + 30000;
  if ( que(QUE_CREATE,0,tag,0,5,"CPUT") != RC_SUCCESS ) 
    {
      cput45_terminate(XC_ERROR);
    }  
  alarm(2);
  if ( que(QUE_RESP,0,tag,0,sizeof(ITEM),(char *)&item1) != RC_SUCCESS ) 
    {
      cput45_terminate(XC_ERROR);
    } /* end if */
  alarm(0);
  mod_id = item1.originator;
  /* Request the queue overview */

  errno = 10;
  return_value = que(QUE_QOVER,0,mod_id,5,0,0);
  if ( return_value != RC_SUCCESS ) 
    {
      dbglog(TRACE,"DELETING QUEUE %d", mod_id);
      que(QUE_DELETE,mod_id,mod_id,0,0,0);
      cput45_terminate(XC_ERROR);
     
    } /* end if */
/* 20040706 JIM: Allocation will be done in que(QUE_GETBIG,....  :
  len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
  item = (ITEM *) malloc(len);
  if ( item == (ITEM *) NUL ) 
    {
      dbglog(TRACE,"DELETING QUEUE %d", mod_id);
      que(QUE_DELETE,mod_id,mod_id,0,0,0);
      cput45_terminate(XC_OK);
    } * end if *
	
  if ( que(QUE_GET,0,mod_id,5,len,(char *)item) != RC_SUCCESS )
*/
  item = NULL; /* 20040706 JIM: flag to allocate item in que(QUE_GETBIG,.... */
  if ( que(QUE_GETBIG,0,mod_id,5,0,(char *)&item)!= RC_SUCCESS ) 
    {
      free((char *) item); 
      dbglog(TRACE,"DELETING QUEUE %d", mod_id);
      que(QUE_DELETE,mod_id,mod_id,0,0,0);
      cput45_terminate(XC_ERROR);
    } /* end if */
	
  if ( item->msg_length == FATAL ) 
    {
      free((char *) item);
      dbglog(TRACE,"DELETING QUEUE %d", mod_id);
      que(QUE_DELETE,mod_id,mod_id,0,0,0);
      cput45_terminate(XC_ERROR);
    }/* end if */
	
  que_info = (QUE_INFO *) &(item->text[0]);
  info_count = 0;
/*    line_count = 3; */
  switch(ipWhat)
    {
    case 0:
      while ( info_count <= (min (ilMaxValid,que_info->entries)) ) 
	{
	  if(mode==0)
	    printf(QO_INFO(que_info->data[info_count]));
	  if(mode==1)
	    printf(HTML_QO_INFO(que_info->data[info_count]));
	  if(mode==2)
	    printf(HTML_QO_INFO(que_info->data[info_count]));
	  info_count++;
	  /*        line_count++; */
	} /* end while */
      printf("\n");
      if (que_info->entries > ilMaxValid)
      {
         printf("Only %d of %d entries could be displayed!\n",
                ilMaxValid,que_info->entries);
      }
	    printf("_NTIF#, BCQ#, CDR#: TOTAL shows num of queues, details in SYSQCP_Stat.log\n");

      free((char *) item);
      break;
    case 1: 
 		  printf("\nDelete all CPUT queues with STAT==5 ....\n");
      ilSomeFound= 0;
      while ( info_count <= (min (ilMaxValid,que_info->entries)) ) 
	{
	  strcpy(pclTmp,que_info->data[info_count].name);
	  TrimRight(pclTmp);
	  if(!strcmp(pclTmp,"CPUT")&&que_info->data[info_count].status==5)
	    {
	     rc=delete_queue(que_info->data[info_count].id,mod_id);
	     if(rc!=RC_SUCCESS) 
	       {
	     printf("\nDelete queue %d of %s: Error %d \n",
             que_info->data[info_count].id,que_info->data[info_count].name,rc);
	       }
         else
         {
    	     printf("\nDeleted queue %d of %s \n",
             que_info->data[info_count].id,que_info->data[info_count].name);
            ilSomeFound= 1;
         }
	    }
	  info_count++;
  /*        line_count++; */
	} /* end while */
      printf("\n");
      if (ilSomeFound == 0)
      {
         printf("No appliable queues of CPUT found!\n");
      }
      if (que_info->entries > ilMaxValid)
      {
         printf("Only %d of %d entries could be processed!\n",
                ilMaxValid,que_info->entries);
      }
      free((char *) item);
      break;
    case 2: 
 		  printf("\nDelete all CPUT queues....\n");
      ilSomeFound= 0;
      while ( info_count <= (min (ilMaxValid,que_info->entries)) ) 
	{
	  strcpy(pclTmp,que_info->data[info_count].name);
	  TrimRight(pclTmp);
	  if(!strcmp(pclTmp,"CPUT")&&que_info->data[info_count].id!=mod_id)
	    {
	     rc=delete_queue(que_info->data[info_count].id,mod_id);
	     if(rc!=RC_SUCCESS) 
	       {
            printf("\nQueue %d of %s: Delete Error %d\n",
              que_info->data[info_count].id,que_info->data[info_count].name,rc);
	       }
         else
         {
	          printf("\nQueue %d of %s deleted\n",
                que_info->data[info_count].id,que_info->data[info_count].name);
            ilSomeFound= 1;
         }
	    }
      else if (que_info->data[info_count].id==mod_id)
      {
	     printf("\nDelete not valid for %s, my queue %d\n",
                que_info->data[info_count].name,mod_id);
      }
	  info_count++;
  /*        line_count++; */
	} /* end while */
      printf("\n");
      if (ilSomeFound == 0)
      {
         printf("No appliable queues of CPUT found!\n");
      }
      if (que_info->entries > ilMaxValid)
      {
         printf("Only %d of %d entries could be processed!\n",
                ilMaxValid,que_info->entries);
      }
      free((char *) item);
      break;
    case 3: 
 		  printf("\nDelete all UTIL queues....\n");
      ilSomeFound= 0;
      while ( info_count <= (min (ilMaxValid,que_info->entries)) ) 
	{
	  strcpy(pclTmp,que_info->data[info_count].name);
	  TrimRight(pclTmp);
	  if (strstr(pclTmp,"UTIL") != NULL) 
	    {
	     rc=delete_queue(que_info->data[info_count].id,mod_id);
	     if(rc!=RC_SUCCESS) 
	       {
		        printf("\nQueue_delete Error %d\n",rc);
	       }
         else
         {
	          printf("\nQueue %d of %s deleted\n",
                que_info->data[info_count].id,que_info->data[info_count].name);
            ilSomeFound= 1;
         }
	    }
	  info_count++;
  /*        line_count++; */
	} /* end while */
      printf("\n");
      if (ilSomeFound == 0)
      {
         printf("No queues of UTIL found!\n");
      }
      if (que_info->entries > ilMaxValid)
      {
         printf("Only %d of %d entries could be processed!\n",
                ilMaxValid,que_info->entries);
      }
      free((char *) item);
      break;

    case 4: 
      if ((ipDest>=20000) && (ipDest<30000) )
      {
 		  printf("\nDelete one CUTIL queue: %d\n",ipDest);

      while ( info_count <= (min (ilMaxValid,que_info->entries)) ) 
	{
	  strcpy(pclTmp,que_info->data[info_count].name);
	  TrimRight(pclTmp);
	  if( ((strstr(pclTmp,"UTIL") != NULL) || (strstr(pclTmp,"CPUT") != NULL))
       &&(que_info->data[info_count].id==ipDest))
	    {
       ilMaxValid= que_info->entries; /* queue found, avoid errmsg*/
	     rc=delete_queue(que_info->data[info_count].id,mod_id);
	     if(rc!=RC_SUCCESS) 
	       {
		 printf("\nQueue_delete Error %d\n",rc);
	       }
         else
         {
	     printf("\n%s queue %d deleted\n",que_info->data[info_count].name,que_info->data[info_count].id);
         }
	    }
	  info_count++;
  /*        line_count++; */
	} /* end while */
  }
  else
  {
    printf("\ndelete queue: id %d out of range (20000-30000)\n",ipDest);
    ilMaxValid= que_info->entries; /* avoid second errmsg */
  }
      printf("\n");
      if (que_info->entries > ilMaxValid)
      {
         printf("Only %d of %d entries could be displayed!\n",
                ilMaxValid,que_info->entries);
      }
      free((char *) item);
      break;

    case 5:
    /* print one line and core without deleting QUEUE (for debugging) */
	  if(mode==0)
	    printf(QO_INFO(que_info->data[info_count]));
	  if(mode==1)
	    printf(HTML_QO_INFO(que_info->data[info_count]));
	  if(mode==2)
	    printf(HTML_QO_INFO(que_info->data[info_count]));
      printf("\n");
      free((char *) item);
      info_count= info_count / (ipWhat - 5);
      break;
    default:
      break;
    }
  que(QUE_DELETE,mod_id,mod_id,0,0,0);
  cput45_terminate(XC_OK);
} /* end of queue_overview */

/* ******************************************************************** */
/*									*/
/* The following routine deletes a queue.				*/
/*									*/
/* ******************************************************************** */

static int delete_queue(int que_ID,int mod_id)
{
	ITEM	*item;
	int	len;

	/* Prompt for queue ID */
	if ( ( que_ID <= 0 ) || ( que_ID >= 30000 ) ) {
		return RC_FAIL;
	} /* end if */
	
      dbglog(TRACE,"DELETING QUEUE (QUE: %d) (MOD: %d)", que_ID, mod_id);
	if ( que(QUE_DELETE,que_ID,mod_id,5,0,0) != RC_SUCCESS ) {
		return RC_FAIL;
	} /* end if */
	 
	len = sizeof(ITEM);
	item = (ITEM *) malloc(len);
	if ( que(QUE_GETNW,0,mod_id,5,len,(char *)item) != RC_SUCCESS ) {
		free((char *) item);
		return RC_SUCCESS;
	} /* end if */
		
	free((char *) item);
	return RC_SUCCESS;
	


} /* end of delete_queue */
/* ******************************************************************** */
/* The TrimRight() routine*/
/* ******************************************************************** */
static void TrimRight(char *pcpBuffer)
{
    int i = 0;
    for (i = strlen(pcpBuffer); i > 0 && isspace((int) pcpBuffer[i-1]); i--)
        ;
    pcpBuffer[i] = '\0';
}

static void dbglog(int level,char *fmt, ...)
{
	va_list args;
	char *s; 
	char Errbuf[1000];
	int i;
	char *pclPath = "/ceda/debug";
	char *pclDbgPath = NULL;

	if (debug_level > 0 ) 
	{
		if (pfgDbgLog == NULL)
		{
	    sprintf(pcgMyName, "%s_%05d", mod_name,getpid());
			pclDbgPath = getenv("DBG_PATH");
			if (pclDbgPath == NULL)
			{
				pclDbgPath = pclPath;
			}
      sprintf(pcgCedaLogFile,"%s/%s_all.log", pclDbgPath, mod_name);
      pfgDbgLog = fopen(pcgCedaLogFile,"a");
    }

		if (pfgDbgLog != NULL)
		{

		  va_start(args, fmt);

		  if ((level == TRACE) && debug_level>0 ) 
		  {
			  (void) sprintf(Errbuf, "%s ",pcgMyName);
			  s = Errbuf + strlen(Errbuf);
			  get_time(s);
			  s = Errbuf + strlen(Errbuf);
			  strcat (s, fmt);
			  strcat (s, "\n");
			  fseek(pfgDbgLog, 0, SEEK_END);
			  vfprintf(pfgDbgLog,Errbuf, args); fflush(pfgDbgLog);
		  } /* end if */

		  if (level == DEBUG && debug_level>TRACE ) 
		  {
			  (void) sprintf(Errbuf, "%s ",pcgMyName);
			  s = Errbuf + strlen(Errbuf);
			  get_time(s);
			  s = Errbuf + strlen(Errbuf);
			  strcat (s, fmt);
			  strcat (s, "\n");
			  fseek(pfgDbgLog, 0, SEEK_END);
			  vfprintf(pfgDbgLog,Errbuf, args); fflush(pfgDbgLog);
		  } /* end if */

		  va_end(args);                        

    }

	} /* end if */
}

/* ******************************************************************** */
/* ******************************************************************** */
static void get_time(char *s)
{
   struct timeb tp;
   time_t	_CurTime;
   struct tm	*CurTime;
   struct tm	rlTm;
   long    	secVal,minVal,hourVal;
   char		tmp[16];
  
   if(s != NULL)
     {      
       _CurTime = time(0L);
       CurTime = (struct tm *)localtime(&_CurTime);
       rlTm = *CurTime;
#if defined(_WINNT) 
       strftime(s,20,"%" "H:%" "M:%" "S",&rlTm);
#else
       strftime(s,20,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
       /*   strftime(s,20,"%" "b%" "d %" "T",&rlTm);*/
#endif

       ftime(&tp);
       minVal = tp.time / 60;
       secVal = tp.time % 60;
       hourVal= minVal / 60;
       minVal = minVal % 60;
       sprintf(tmp,":%3.3d>> ",tp.millitm);
       strcat(s,tmp);
     }

}
   
/* ******************************************************************** */
/* ******************************************************************** */
static void cput45_terminate(int ipExVal)
{
	dbglog(TRACE,"============ TERMINATE (RC %d) ===========", ipExVal);
	exit(ipExVal);
}

/* ******************************************************************** */
/* added 20040715 JIM:                                                  */
/* The set_sysqcp_dbg	uses special commands to set the debug_level of   */
/* the SYSQCP process                                                   */
/* ******************************************************************** */
static void set_sysqcp_dbg(int level)
{

  ITEM          item1;
  ITEM		*item;
  int tag;
  int		len;
  int		return_value;

   if ((level==TRACE_ON) || (level==TRACE_OFF))
   {
     tag = (getpid()%1000) + 30000;
     if ( que(QUE_CREATE,0,tag,0,5,"CPUT") != RC_SUCCESS ) 
     {
       exit(XC_ERROR);
     }  
     alarm(2);
     if ( que(QUE_RESP,0,tag,0,sizeof(ITEM),(char *)&item1) != RC_SUCCESS ) 
     {
       exit(XC_ERROR);
     } /* end if */
     alarm(0);
	
     mod_id = item1.originator;
 
     /* send DBG flag for SYSQCP */

     switch (level)
     {
       case TRACE_ON:
          return_value = que(QUE_SYSDBG,9 ,mod_id,5,0,0);
          break;
       case TRACE_OFF:
          return_value = que(QUE_SYSTRCOFF,9 ,mod_id,5,0,0);
          break;
     }
     if ( return_value != RC_SUCCESS ) 
     {
       que(QUE_DELETE,mod_id,mod_id,0,0,0);
       exit(XC_ERROR);
     } /* end if */
     len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
     item = (ITEM *) malloc(len);
     if ( item == (ITEM *) NUL ) 
     {
       que(QUE_DELETE,mod_id,mod_id,0,0,0);
       exit(XC_ERROR);
     } /* end if */
	
     if (que(QUE_GET,0,mod_id,5,len,(char *)item) != RC_SUCCESS ) 
     {
       que(QUE_DELETE,mod_id,mod_id,0,0,0);
       free((char *) item);
       
       exit(XC_ERROR);
     } /* end if */
	
     if ( item->msg_length == FATAL ) 
     {
       que(QUE_DELETE,mod_id,mod_id,0,0,0);
       free((char *) item);
       exit(XC_ERROR);
     } /* end if */
     que(QUE_DELETE,mod_id,mod_id,0,0,0);
     printf("%s\n",item->text);fflush(stdout);
  }
  exit(XC_ERROR);
   
}

extern void SendNextForQueue(int module);
static void set_sysqcp_nextnw(int module)
{
  SendNextForQueue(module);
  exit(XC_OK);
}

/* ******************************************************************** */
/* Space for Appendings (Source-Code Control)				*/
/* ******************************************************************** */
/* ******************************************************************** */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
