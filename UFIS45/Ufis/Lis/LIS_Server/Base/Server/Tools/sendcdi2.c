
/* *************************************************** */
/* Author		: JWE      												        */
/* Date			: 20.11.1998                               */
/* Description		: test-proc for bagagge handling     */
/*               		interface between UFIS & Mannesmann*/
/*               		for athens int. airport            */
/* *************************************************** */
/*static char sccs_bhsif_tcp[]="@(#)UFIS 4.4 (c) ABB AAT/I bhsif_tcp.c 44.3 / 00/07/03 10:35:18 / JWE";*/
/* *******  System Header Files (unix includes) ****** */
#include <sys/types.h> 
#include <values.h> 
#include <sys/socket.h> 
#include <time.h>
#include <stdio.h>
#include "netin.h"
#include "errno.h"

#ifndef RC_SUCCESS
#define RC_SUCCESS 0
#endif
#ifndef RC_FAIL
#define RC_FAIL         -1
#endif                             
#define TRACE       0x1000
#define DEBUG       0x1001
#define TRUE        1
#define FALSE       0         

/* *******  CCS Header Files (CCS includes) ****** */
int debug_level = DEBUG;

static int bgReadSock = FALSE;
static int igLine = 0;
static int igReadSock = 0;
static char pcgReadPort[256];
static char pcgFile[256];
static char pcgHost[256];
static char	pcgAllData[1000][2048];
static int	igLength[2048];
static char pcgSendBuffer[2096];
#ifndef _SOLARIS
static  struct	sockaddr_in	my_client;	/* IP addr of client */
#else
static  struct	sockaddr	my_client;	/* IP addr of client */
#endif

/* prototypes */
static int Send_data();
static int PrepareData();
static int OpenConn (char *pcpPort,int *ipSock);
static int RunInterface();
static void terminate();
static void usage(void); 

/*short mod_id = 4711;*/
char *mod_name;
char *mod_version=NULL;
char logfile[128];
FILE *outp;

#define INIT  mod_name = argv[0];\
 sprintf(logfile,"%s/%s%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());\
 outp = fopen(logfile,"w");

/* ****************************************************************** */
/* The MAIN program                                                   */
/* ****************************************************************** */
main(int argc, char *argv[], char *envp[])
{
	int ilRc = RC_SUCCESS;
	int ilCnt1 = 0;
	
	*pcgReadPort = '\0';
	*pcgHost = '\0';
	strcpy(pcgReadPort,argv[2]);
	strcpy(pcgFile,argv[3]);
	strcpy(pcgHost,argv[4]);

	INIT;							/* General initialization	*/
	if(argc != 5)
	{
		usage();
		exit(1);
	}             

	dbg(DEBUG,"Period  (%s)sec.",argv[1]);
	dbg(DEBUG,"Service  (%s)",argv[2]);
	dbg(DEBUG,"File    (%s)",argv[3]);
	dbg(DEBUG,"to host (%s)",argv[4]);
	if ((ilRc = RunInterface()) == RC_SUCCESS)
	{
		while(TRUE)
		{
			PrepareData();
			Send_data();
			sleep(atoi(argv[1]));
		}
	}
	else
	{
		dbg(DEBUG,"Error running service (%s)",pcgReadPort);
	}
	terminate();
} 
/* ****************************************************************** */
/* The RunInterface() routine                                         */
/* ****************************************************************** */
static int RunInterface()
{
		int ilRc = RC_SUCCESS;

		do
		{
			if ((bgReadSock==FALSE) && (ilRc = OpenConn(pcgReadPort,&igReadSock))==RC_FAIL)
			{
					dbg(DEBUG,"OpenConn failed for Port (%s) !",pcgReadPort);
					bgReadSock = TRUE;
			}
		}while(bgReadSock);
		return ilRc;
}
/* ****************************************************************** */
/* The OpenConn routine							*/
/* ****************************************************************** */
static int OpenConn (char *pcpPort,int *ipSock)
{
  int	rc = RC_FAIL;	/*Return code*/
  int i=0;
  int ilReady=FALSE;
  int ilLen=0;

  dbg(DEBUG,"OpenConn: open connection for (%s)",pcpPort);

  *ipSock = my_tcp_create_socket (SOCK_STREAM, pcpPort); /* create the socket*/
  if (*ipSock < 0)
  {
     dbg(DEBUG,"OpenConn: Create socket failed: %s ",strerror(errno));
      rc = RC_FAIL;
      ilReady = TRUE;
    }
    else
    {
      dbg(DEBUG,"OpenConn: Create socket successfull.");
			#if 0
			/* doing accept */
			listen(*ipSock,20);
			ilLen = sizeof(my_client);
			/*alarm(2);*/
			/* *ipSock = accept(*ipSock,(struct sockaddr*)&my_client,&ilLen);*/
			/*alarm(0);*/
			if ( *ipSock < 0 ) 
			{
				if (errno == EINTR)
				{	/* Timeout */
		 		 /*dbg(DEBUG,"OpenConn: connect Timeout: %s:",strerror(errno));*/
				}
				else
				{
					dbg(TRACE,"OpenConn: connect failed because of : %s:",strerror(errno));
				} 
			}

    	dbg(DEBUG,"OpenConn: connected on socket %d ipaddr=%x",*ipSock,
				ntohl(my_client.sin_addr.s_addr) );
			rc = RC_SUCCESS;
			#endif

			/* doing connect */
			dbg(DEBUG,"OpenConn: try to connect ...");
      /*alarm(4);*/
      rc = tcp_open_connection(*ipSock,pcpPort,pcgHost);
      /*alarm(0);*/
      if (rc != RC_SUCCESS)
      {
				dbg(DEBUG,"OpenConn: Open connection to (%s) failed: (%s) ",
						pcgHost,strerror(errno)); 
				close (*ipSock);
				*ipSock = 0;
      }
      else
      {
				dbg(DEBUG,"OpenConn: Open connection to (%s) successfull.",
						pcgHost); 
				ilReady = TRUE;
      } 
    } 
  	dbg(DEBUG,"OpenConn: return(%d)\n",rc);
  	return rc;
} 
/* ****************************************************************** */
/* The PrepareData routine                                       */
/* prepares the data after the command for sending                    */
/* ****************************************************************** */
static int PrepareData()
{
	int ilRc = RC_SUCCESS;
	char pclMsgNr[8];
	char	pclTmpFile[128];
	char	*pclCfgPath;
	FILE	*fh;

	dbg(DEBUG,"<PrepareData> port is: <%s>", pcgReadPort);
	if ((pclCfgPath = getenv("CFG_PATH")) == NULL)
	{
		dbg(DEBUG,"<PrepareData> missing CFG_PATH...");
		exit(1);
	}
	memset((void*)pclTmpFile, 0x00, 128);
	strcpy(pclTmpFile,pcgFile);
	dbg(DEBUG,"<PrepareData> filename: <%s>", pclTmpFile);
	if ((fh = fopen(pclTmpFile, "r")) == NULL)
	{
		dbg(DEBUG,"<PrepareData> cannot open file <%s>", pclTmpFile);
		exit(1);
	}
	else
	{
		igLine = 0;
		while (!feof(fh))
		{
			fscanf(fh, "%[^\n]%*c", pcgAllData[igLine]);
			igLength[igLine] = strlen(pcgAllData[igLine]);
			if (igLength[igLine] > 0)
			{
				dbg(DEBUG,"<found: <%s> lenght<%d>"
					,pcgAllData[igLine],igLength[igLine]);
				igLine++;
			}
		}
		fclose(fh);
		return RC_SUCCESS;
	}
	return ilRc;
}
/* **************************************************************** */
/* The Send_data routine                                             */
/* **************************************************************** */
static int Send_data()
{
  int ilCnt = 0;
  int ilLen = 0;
  int rc=RC_SUCCESS;
	
	dbg(DEBUG,"Send_data: sending data to (%s).",pcgHost);
	 
  /* igReadSock = opened socket */
	/* ipLen = number of bytes */
	/* be transmitted. */

	while(ilCnt < igLine)
	{
		ilLen = strlen(pcgAllData[ilCnt]);

		/*pcgAllData[ilCnt][0] = */
		/*pcgAllData[ilCnt][0] = */
		/*pcgAllData[ilCnt][ilLen-1] = 0xD; */
		/*pcgAllData[ilCnt][0] = 0x2; 
		pcgAllData[ilCnt][ilLen-2] = 0xD; 
		pcgAllData[ilCnt][ilLen-1] = 0x3; */

		rc = write(igReadSock,(char *)pcgAllData[ilCnt],ilLen);/*unix*/
		if (rc == -1)
		{
			dbg(DEBUG,"Send_data: Write failed: %d %s",errno,strerror( errno)); 
			rc = RC_FAIL;
		}
		else
		{
			dbg(DEBUG,"Send_data: wrote (%d) bytes to (%d)-socket.",rc,igReadSock); 
			rc = RC_SUCCESS;
			snap ((char *)pcgAllData[ilCnt],ilLen,outp);
			fflush(outp);
		}
		ilCnt++;
	}
  return rc;
}
static void terminate()
{
	close(igReadSock);
	fflush(outp);
	fclose(outp);
}

static void usage(void)
{
  printf("\nUsage: sendcdi2 PERIOD SERVICE FILE HOST\n");
  printf("1 PERIOD  = frequency (sec.) for sending \"file\" \n");
  printf("2 SERVICE = name of the \"/etc/service\" to use\n");
  printf("3 FILE    = file which should be sent\n");
	printf("4 HOST    = hostname (or IP) where the data should be sent to \n");
	printf("\n");
}

int my_tcp_create_socket (int type, char *Pservice)
{
  int	ilRC;
  int	rc = RC_SUCCESS;	/* Return code */
  int	sock = 0;
  int	ilKeepalive = 0;
	#ifdef _HPUX_SOURCE
  	int	ilRecLen = 0;
	#else
  	size_t	ilRecLen = 0;
	#endif

		
  struct 	servent 	*sp;
  struct	sockaddr_in	name;

#if defined(_WINNT)
  int bool_value=1;	/* Arg for setsockopt */
  int *opt_bool=&bool_value;
  struct linger lingstruct;	/* Arg for setsockopt */
  struct linger *ling=&lingstruct;	/* Arg for setsockopt */
#else
  int opt_bool=1;	/* Arg for setsockopt */
  struct linger ling;	/* Arg for setsockopt */
#endif

  sock = socket(AF_INET, type, 0); /* create socket */

  if (sock < 0 )
  {
    dbg(TRACE,"tcp_create_socket: Error can't open socket: %s", strerror( errno));
    rc = RC_FAIL;
  }
  else
  {
    dbg(DEBUG,"tcp_create_socket: Socket %d opened", sock);
    if (Pservice != NULL)
    {
      if((sp = getservbyname(Pservice, NULL) ) == NULL )
      {
	dbg(TRACE,"tcp_create_socket: unknown service %s", Pservice); 
	rc = RC_FAIL;
      }
    }
  }


  if(rc == RC_SUCCESS)
  {
    ilKeepalive = 0;

#if defined(_UNIXWARE) || defined(_SOLARIS) || defined(_HPUX_SOURCE)
    ilRecLen = sizeof(int);
#endif

  /* int getsockopt(int s, int level, int optname, void *optval, size_t *optlen); */

    rc = getsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, (char *) &ilKeepalive, &ilRecLen);
    if (rc == -1 )
    {
      rc = RC_FAIL;
      dbg(TRACE,"tcp_create_socket: getsockopt <%s>", strerror(errno));
    }
    else
    {
      if(ilKeepalive == 1)
      {
        dbg(DEBUG,"tcp_create_socket: SO_KEEPALIVE already set");
      }
      else
      {
        ilKeepalive = 1;
        rc = setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, (char *) &ilKeepalive, sizeof(ilKeepalive));
        if (rc == -1 )
        {
          rc = RC_FAIL;
          dbg(TRACE,"tcp_create_socket: setsockopt <%s>", strerror(errno));
        }
        else
        {
          dbg(DEBUG,"tcp_create_socket: SO_KEEPALIVE set");
        }/* end of if */
      }/* end of if */
    }/* end of if */
  }/* end of if */


  if (rc == RC_SUCCESS)
  {
    rc = setsockopt (sock, SOL_SOCKET, SO_REUSEADDR, (char *) &opt_bool, sizeof (opt_bool));
    if (rc == -1 )
    {	
      dbg(TRACE,"tcp_create_socket: Error Setsockopt REUSEADDR: %s", strerror(errno));
      rc = RC_FAIL;
    }
  }


  if (rc == RC_SUCCESS)
  {
#if defined(_WINNT)
    lingstruct.l_onoff = 0;
    lingstruct.l_linger = 0;
#else
    ling.l_onoff = 0;
    ling.l_linger = 0;
#endif
    rc = setsockopt (sock, SOL_SOCKET, SO_LINGER, (char *) &ling, sizeof (ling));
    if (rc == -1 )
    {	
      dbg(TRACE,"tcp_create_socket: Error Setsockopt LINGER: %s", strerror(errno));
      rc = RC_FAIL;
    }
  }


#if defined(_WINNT)
      rc = RC_SUCCESS;
#endif

  if (Pservice != NULL && rc == RC_SUCCESS)
  {
    dbg(DEBUG,"tcp_create_socket: got services %s", Pservice); 
  
    name.sin_addr.s_addr = INADDR_ANY;
    name.sin_family = AF_INET;
    name.sin_port   = (u_short) sp->s_port;
  
#if 0
#ifdef _WINNT
    while ((rc = bind(sock, &name, sizeof(struct sockaddr_in))) < 0)
#else
	 errno = 0;
    while ((rc = bind(sock, (struct sockaddr *) &name, sizeof(struct sockaddr_in))) < 0)
#endif
    {
      dbg(DEBUG,"tcp_create_socket: bind returns: %d - sleep 10 seconds now -> <%s>", rc, strerror(errno));
      sleep(10);
    }
#endif
  }

  if (rc == RC_SUCCESS)
  {
    dbg(DEBUG,"tcp_create_socket: return sock %d",sock);
    return sock;
  }
  else
  {
    dbg(DEBUG,"tcp_create_socket: return RC_FAIL");
    return RC_FAIL;
  }

}
