#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <arpa/inet.h>
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <dirent.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <time.h>
#include <locale.h>

/* jhe 1 sep 04 includes for interface ioctl */

#ifndef _HPUX_SOURCE
#include <sys/sockio.h>
#endif
#include <sys/ioctl.h>
#include <net/if.h>

#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h" 
#include "AATArray.h"
#include "gt2hdl.h"

#include "dsphdl.h" 
/*  #include "dsplib.h" */
#include "lexdsp.h"
#include "tools.h"
#include "action.h"
#include "helpful.h"
#include "timdef.h"
#include "cedatime.h"

#define RC_SUCCESS 0
#define RC_NOTFOUND 1
#define XS_BUFF  128
static int igUTCDIFF = 0;
int debug_level = 0;
int mod_version = 01;
int tcp_send_datagram (int sock, char *Phostadr, char *Phostname, 
		       char *Pservice, char *Pdata, int len)
{
  int rc = RC_SUCCESS;
  struct servent *sp;
  struct sockaddr_in target;
  struct hostent *remote_adr;

  memset ((char *) &target, 0, sizeof (struct sockaddr_in));

  if (Phostadr == NULL)
  {	/* A host name is given */
    printf("tcp_send_datagram: addr=<%s> name=<%s> serv=<%s> len=<%d>\n",Phostadr, 
			 Phostname,Pservice, len);

		errno = 0;
#if !(defined(_HPUX_SOURCE) || defined(_WINNT))
		h_errno = 0;
#endif
    remote_adr = gethostbyname (Phostname);
    if (remote_adr == NULL)
    {
#if defined(_HPUX_SOURCE) || defined(_WINNT)
      printf("tcp_send_datagram: gethostbyname(%s) errno<%d>=<%s>\n", 
	  		Phostname, errno,strerror(errno));
#else
      printf("tcp_send_datagram: gethostbyname(%s) h_errno<%d>=<%s>\n", 
	  		Phostname,h_errno,strerror(h_errno));
#endif
      rc = RC_NOT_FOUND;
    }
    else
    {
      dbg (DEBUG,"tcp_send_datagram: Host info: name=<%s> len=<%d> adr1=<%x>"
	   		,remote_adr->h_name, remote_adr->h_length,remote_adr->h_addr);
      dbg (DEBUG,"tcp_send_datagram: Send answer to name=<%s>",
	   		remote_adr->h_name);
      memcpy((char *) &target.sin_addr, *remote_adr->h_addr_list, 
	     remote_adr->h_length);
    } /* fi */
  } 
  else
  {	/* The address is given */
    dbg (DEBUG,"tcp_send_datagram: name=<%s> addr=<%s> serv=<%s> len=<%d>",Phostname, 
			 Phostadr,Pservice, len);
    sscanf(Phostadr,"%8x",(char *)&(target.sin_addr));
  } /* fi */ 

  if (rc == RC_SUCCESS)
  {
    if ((sp=getservbyname(Pservice,NULL) ) == NULL )
		{
      dbg (TRACE,"tcp_send_datagram: unknown service <%s>",Pservice);
      rc = RC_FAIL;
    } /* end if */
  } /* end if */

  if (rc == RC_SUCCESS)
  {
    target.sin_addr.s_addr = htonl(target.sin_addr.s_addr);
    target.sin_family = AF_INET;
    target.sin_port   = (u_short) sp->s_port;
    printf("tcp_send_datagram: target address<%8x> port<%x>\n",(int) (target.sin_addr.s_addr),
					target.sin_port);

    errno=0;
    rc = sendto(sock, Pdata, len, 0, (struct sockaddr *) &target, 
		 						sizeof (struct sockaddr_in));

    if (rc < 0)
    {
      rc = RC_FAIL;
      printf("tcp_send_datagram: sendto() errno<%d>=<%s>\n",errno,strerror(errno));
    }
    else
    {
      printf("tcp_send_datagram: sendto() wrote <%d> bytes to socket!\n",rc);
      rc = RC_SUCCESS;
    } /* fi */
  } /* fi */

	return rc;
}

main(int argc,char *argv[],char *envp[])
{
	int ilI = 0;
	int ilRC = 0;
	int ilRc = 0;
	int igDgramSock = 0;
	int ilTimeOffset = 10;
	int ilRetryCnt = 5;
	int ilYear;
  time_t             tlNow = 0;
  struct tm         *_tm;
  struct tm         rlTm;
  char pclBcAddr[32];
  char                 pclIp[XS_BUFF];
  char                 pclTime[XS_BUFF];
  char                 pclSec[6];
  char                 pclMin[6];
  char                 pclHour[6];
  char                 pclDay[6];
  char                 pclMonth[6];
  char                 pclYear[6];
  unsigned char        pclNewTime[7];
  IDS_TIME_SYNC     rlTime;

  if (igDgramSock != 0)
    {
      /*dsplib_close(igDgramSock); */
      close(igDgramSock);
      igDgramSock =0;
    }
  do{
    if ((ilRc = dsplib_socket(&igDgramSock,SOCK_DGRAM,NULL,0,FALSE,"*")) != RC_SUCCESS)
      {
        /*dsplib_close(igDgramSock);*/
        close(igDgramSock);
        printf("<ConnectDevice> dsplib_socket returns: <%d>.\n",ilRc);
      }else{
        printf("<ConnectDevice> UDP-Socket = <%d>\n",igDgramSock);
      }
    ilRetryCnt++;
    nap(ilTimeOffset*ilRetryCnt);
  }while(ilRc!=RC_SUCCESS && ilRetryCnt < 10);

  tlNow = time(NULL);
  if (igDgramSock <= 0)
  {
		exit(0);
  }
  memset(pclTime,0x00,XS_BUFF);
  memset((char*)&rlTime,0x00,sizeof(IDS_TIME_SYNC));
  rlTime.id = htonl(HEX_TIME_SYNC_ID);
  _tm = (struct tm*)localtime(&tlNow);
  rlTm = *_tm;  
  /* Unusal strftime format because of sccs !! */
  strftime(pclSec,6,"%" "S",&rlTm);
  strftime(pclMin,6,"%" "M",&rlTm);
  strftime(pclHour,6,"%" "H",&rlTm);
  strftime(pclDay,6,"%" "d",&rlTm);
  strftime(pclMonth,6,"%" "m",&rlTm);
  strftime(pclYear,6,"%" "Y",&rlTm);
  ilYear = (int)atoi(pclYear) - 1970;
  printf("<SendTimeSync> time <%s%s%s%s%s%s>\n",pclYear,pclMonth,pclDay,pclHour,pclMin,pclSec);
  rlTime.currenttime.sec=(unsigned char)(atoi(pclSec));
  rlTime.currenttime.min=(unsigned char)(atoi(pclMin));
  rlTime.currenttime.hour=(unsigned char)(atoi(pclHour));
  rlTime.currenttime.day=(unsigned char)(atoi(pclDay));
  rlTime.currenttime.month=(unsigned char)(atoi(pclMonth));
  rlTime.currenttime.year=(unsigned char)ilYear;
  /*sprintf(pclIp,"%x",ntohl(uigBcAddr));*/

  if (strcmp(argv[1],"FFFFFFFF") == 0)
  {
     if ((ilRc = tcp_send_datagram(igDgramSock,"FFFFFFFF",NULL,"UFIS_FIDS_SEND_UDP", (char*)&rlTime,IDS_TIME_SYNC_SIZE)) != RC_SUCCESS)
     {
        printf("<SendTimeSync> tcp_send_datagram(%s) returns <%d>\n",argv[1],ilRc);
     }
  }
  else
  {
     for (ilI = 1; ilI < 255; ilI++)
     {
        sprintf(pclBcAddr,"%s%2.2x",argv[1],ilI);
        if ((ilRc = tcp_send_datagram(igDgramSock,pclBcAddr,NULL,"UFIS_FIDS_SEND_UDP", (char*)&rlTime,IDS_TIME_SYNC_SIZE)) != RC_SUCCESS)
        {
           printf("<SendTimeSync> tcp_send_datagram(%s) returns <%d>\n",pclBcAddr,ilRc);
        }
     }
  }
	exit(0);
}

