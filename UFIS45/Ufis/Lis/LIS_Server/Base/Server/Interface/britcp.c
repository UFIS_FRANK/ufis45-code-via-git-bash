#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Lis/LIS_Server/Base/Server/Interface/britcp.c 1.7 2007/05/03 20:02:44SGT akl Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I skeleton.c 44.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>

#ifdef _HPUX_SOURCE
extern int daylight;
extern long timezone;
extern char *tzname[2];
#else
extern int _daylight;
extern long _timezone;
extern char *_tzname[2];
#endif

#define BRI_OUT 1
#define BRI_IN  2

int debug_level = DEBUG;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;
static int igStartUpMode = TRACE;
static int igRuntimeMode = 0;
static int igSendQueue = 0;
static int igStartSocket = -1;
static int igWorkSocket = -1;

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */
static int bgDirection;
static char cgSocketType[24] = "";
static char cgServiceIn[124] = "BRIDGE_IN";
static char cgServiceOut[124] = "BRIDGE_OUT";
static char cgHost[124] = "localhost";
static char cgCommands[248];
static int  igPriority = 3;
static char cgStartScript[248] = "";
static char cgQueues[248];
static char cgJavaLogFileName[1024];
static char cgJRE[1024] = "\0";
static char cgJREPara[1024] = "\0";
static char cgJavaProgram[1024] = "\0";
static char cgJavaScript[1024] = "\0";
static char cgJavaShell[1024] = "\0";
static int igTcpTimeout = 10;
static int igTerminateTimeout = 60;
static int igTcpDelay = 10;
static int igConnectTimeout = 20;
static int bgConnected = FALSE;
static int igCheckTime = 20;
static time_t tgLastCheck = 0;
static int igMaxBytes = 8192;
static int CheckQueue();

static long lgEvtCnt = 0;

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int  Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
			   char *pcpCfgBuffer);
static int ReadConfigRow(char *pcpSection,char *pcpKeyword,
			 char *pcpCfgBuffer);
static char *GetKeyItem(char *pcpResultBuff, long *plpResultSize, 
			char *pcpTextBuff, char *pcpKeyWord, char *pcpItemEnd, int bpCopyData);
static int GetDebugLevel(char *pcpMode, int *pipMode);
static void ForkChild();
static int GetCommandFromSocket();
static int OpenConnectionOut();
static int OpenConnectionIn();
static int PutCommandOnSocket(char *pcpCommand,char *pcpData, char *pcpFields, char *pcpSelection);

static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void   HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static int	HandleData(ITEM *prpItem);       /* Handles event data     */
static int  GetCommand(char *pcpCommand, int *pipCmd);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int	ilRc = RC_SUCCESS;			/* Return code			*/
    int	ilCnt = 0;
    int ilOldDebugLevel = 0;
    char clAlert[200] = "\0";

    INITIALIZE;			/* General initialization	*/


    debug_level = TRACE;

    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",sccs_version);

    /* Attach to the MIKE queues */
    do
    {
	ilRc = init_que();
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	    sleep(6);
	    ilCnt++;
	}/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
	sleep(60);
	exit(1);
    }else{
	dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
	ilRc = init_db();
	if (ilRc != RC_SUCCESS)
	{
	    check_ret(ilRc);
	    dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	    sleep(6);
	    ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
	sleep(60);
	exit(2);
    }else{
	dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
	dbg(DEBUG,"MAIN: waiting for status switch ...");
	HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
	dbg(TRACE,"MAIN: initializing ...");
	ilRc = Init_Process();
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"Init_Process: init failed!");
	} /* end of if */
    } 
    else 
    {
				Terminate(30);
    }/* end of if */



    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");


		if (strcmp(cgSocketType,"CLIENT") == 0)
		{
			if (OpenConnectionOut() == RC_SUCCESS)
			{
				dbg(TRACE,"%05d:Connection established Socket %d",__LINE__,igWorkSocket);
				if (*cgStartScript != '\0')
				{
					dbg(TRACE,"Startscript <%s> will be called",cgStartScript);
					system(cgStartScript);
				}
			}
		}
		else if (strcmp(cgSocketType,"SERVER") == 0)
		{
			OpenConnectionIn();
		}
		else
		{
			dbg(TRACE,"Invalid Sockettype <%s>",cgSocketType);
		}

		if (bgDirection == BRI_IN)
		{
			 for(;;)
			 {
				if (GetCommandFromSocket() == RC_FAIL)
				{
					Terminate(igTerminateTimeout);
				}
				dbg(DEBUG,"Got Message from Socket, now CheckQueue");
				CheckQueue();
			}
		}
		else if (bgDirection == BRI_OUT)
		{
    	for(;;)
			{
				CheckQueue();
			}
		}
		else
		{
			dbg(TRACE,"Invalid Direction <%d>",bgDirection);
		}

} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int CheckQueue()
{
    int ilRc = RC_SUCCESS;

    ilRc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		
    if (ilRc == RC_SUCCESS) 
    {		
	/* Acknowledge the item */
	ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	if( ilRc != RC_SUCCESS ) 
	{
	    HandleQueErr(ilRc);
	} 

    	prgEvent = (EVENT *) prgItem->text;

	switch( prgEvent->command )
	{
	    case	HSB_STANDBY	:
		ctrl_sta = prgEvent->command;
		HandleQueues();
		break;	
	    case	HSB_COMING_UP	:
		ctrl_sta = prgEvent->command;
		HandleQueues();
		break;	
	    case	HSB_ACTIVE	:
		ctrl_sta = prgEvent->command;
		break;	
	    case	HSB_ACT_TO_SBY	:
		ctrl_sta = prgEvent->command;
		/* CloseConnection(); */
		HandleQueues();
		break;	
	    case	HSB_DOWN	:
		/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		ctrl_sta = prgEvent->command;
		Terminate(1);
		break;	
	    case	HSB_STANDALONE	:
		ctrl_sta = prgEvent->command;
		ResetDBCounter();
		break;	
	    case	REMOTE_DB :
		/* ctrl_sta is checked inside */
		HandleRemoteDB(prgEvent);
		break;
	    case	SHUTDOWN	:
		/* process shutdown - maybe from uutil */
	  ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
		Terminate(1);
		break;
					
	    case	RESET		:
		ilRc = Reset();
		break;
					
	    case	EVENT_DATA	:
		    ilRc = HandleData(prgItem);
		break;
				

	    case	TRACE_ON :
		dbg_handle_debug(prgEvent->command);
		break;
	    case	TRACE_OFF :
		dbg_handle_debug(prgEvent->command);
		break;
	    default			:
		dbg(TRACE,"MAIN: unknown event");
		DebugPrintItem(TRACE,prgItem);
		DebugPrintEvent(TRACE,prgEvent);
		break;
	} /* end switch */
	}
	else
	{
	nap(1000);
	}

	/* Acknowledge the item 
	ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	if( ilRc != RC_SUCCESS ) 
	{
	    HandleQueErr(ilRc);
	}
************/
	return ilRc;
}

static int OpenConnectionOut()
{
    int   ilRc = RC_FAIL;   /* Return code                  */
    int i=0;
    int ilReady=FALSE;
		time_t tlNow,tlEnd;

		if (bgConnected == TRUE)
		{
			dbg(DEBUG,"OpenConnectionOut: already connected");
			return RC_SUCCESS;
		}

		dbg(DEBUG,"OpenConnectionOut: Host <%s>, Port <%s>",
			cgHost, cgServiceOut);
	
		do
		{
	  if (igWorkSocket == -1 ) 
		{
  		igWorkSocket = tcp_create_socket (SOCK_STREAM, NULL);
	  	if (igWorkSocket == -1 ) 
			{
		    	dbg(TRACE,"%s: Error open socket",mod_name);
					Terminate(igTerminateTimeout);
			} /* end if */
		}
		tlNow = time(NULL);
		tlEnd = tlNow + igConnectTimeout;
			alarm(igConnectTimeout);
			errno = 0;
			ilRc = tcp_open_connection (igWorkSocket, cgServiceOut, cgHost);
			alarm(0);
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"OpenConn: on %s failed: %s ", cgHost,strerror( errno));
				close(igWorkSocket);
				igWorkSocket = -1;
				while(CheckQueue() == RC_SUCCESS);
				sleep(2);
			}
			else
			{
				bgConnected = TRUE;
			} 
		  tlNow = time(NULL);
		  if (tlNow > tlEnd)
			{
				close(igWorkSocket);
				igWorkSocket = -1;
				break;
			}
		} while (bgConnected == FALSE);
    dbg(TRACE,"OpenConnectionOut: returns %d ",ilRc);
    return ilRc;
}
/* ******************************************************************** */
/* The OpenConn routine                                                 */
/* ******************************************************************** */
static int OpenConnectionIn()
{
    int   ilRc = RC_FAIL;   /* Return code                  */

		if (bgConnected == TRUE)
		{
			dbg(DEBUG,"OpenConnectionIn: already connected");
			return RC_SUCCESS;
		}
		dbg(DEBUG,"OpenConnectionIn: Listen on Port <%s>",cgServiceIn);
	
	  if (igStartSocket == -1 ) 
		{
  		igStartSocket = tcp_create_socket (SOCK_STREAM, cgServiceIn);
	  	if (igStartSocket == -1 ) 
			{
		    	dbg(TRACE,"%s: Error create socket",mod_name);
					Terminate(igTerminateTimeout);
			} /* end if */
	
			dbg(TRACE,"OpenConnectionIn: socket created listen on port <%s> follows",
					cgServiceIn);
			if ((ilRc = listen(igStartSocket,1)) == -1)
			{
				dbg(TRACE,"listen returns %d (%s)", igStartSocket, strerror(errno));
				while(CheckQueue() == RC_SUCCESS);
				Terminate(igTerminateTimeout);
			}
		}
    
	 alarm(igConnectTimeout);
	 igWorkSocket = accept(igStartSocket, NULL, NULL);
	 alarm(0);
	 if (igWorkSocket < 0)
	 {
			dbg(TRACE,"%05d",__LINE__);
			dbg(TRACE,"OpenConnectionIn: accept: %d,<%s>",errno,strerror(errno));
	 }
	 else
	 {
	 	dbg(TRACE,"Connection established");
	 	bgConnected = TRUE;
	 }
   return ilRc;
}


static int GetCommandFromSocket()
{

    int ilRc = RC_SUCCESS;
    long llLocalLen;
    long llBytesToRead;
    long llBytesAlreadyRead;
    long llResultLen = 0L;
    int ilCnt = 0;
    char clBytes[120];
    static char *pclTcpBuffer = NULL;
    static long llTcpBufferLen = 0L;
    char *pclTmpBuffer;
    char clAlert[200] = "\0";
		time_t tlNow,tlEnd;
  
    llLocalLen = 15;
    llBytesToRead = 15;
    llBytesAlreadyRead = 0;

    /*dbg(DEBUG,"GetCommandFromSocket starts here");*/
    if (llTcpBufferLen < llLocalLen)
    {
			pclTcpBuffer = realloc(pclTcpBuffer,llLocalLen + 12);
			if (pclTcpBuffer == NULL)
			{
	    		dbg(TRACE,"GetCommandFromSocket: could not allocate TcpBuffer, reason %d <%s>",
						errno,strerror(errno));
		
	    			Terminate(60);
			}
			llTcpBufferLen = llLocalLen + 10;
    }

    pclTmpBuffer = pclTcpBuffer;

    if (igWorkSocket < 0)
    {
			if(OpenConnectionOut() != RC_SUCCESS)
			{
				Terminate(igTerminateTimeout);
			}
			else
			{
				dbg(TRACE,"%05d:Connection established",__LINE__);
			}
		}
	
		memset(pclTcpBuffer,0,llTcpBufferLen);
		dbg(DEBUG,"%05d:reading %d bytes from socket",__LINE__,llBytesToRead);
		tlNow = time(NULL);
		tlEnd = tlNow + igTcpTimeout;
		do
		{
			alarm(igTcpTimeout);
			errno = 0;
			ilRc = read(igWorkSocket,pclTmpBuffer,llBytesToRead);
			alarm(0);
			if(ilRc <= 0)
			{
				if(ilRc < 0)
				{
					dbg(TRACE,"read: socket %d error %d %s",
						igWorkSocket,errno,strerror(errno));
					Terminate(igTerminateTimeout);
				}
		  	tlNow = time(NULL);
		  	if (tlNow > tlEnd)
				{
				  dbg(DEBUG,"TCP-Timeout reached End %ld now %ld Timeout %ld",tlEnd,tlNow,igTcpTimeout);
					close(igWorkSocket);
					igWorkSocket = -1;
					bgConnected = FALSE;
					return(RC_FAIL);
				}
			}
			else
			{
				dbg(DEBUG,"RC from read is %d <%s>",ilRc,pclTmpBuffer);
				llBytesToRead -= ilRc;
				llBytesAlreadyRead += ilRc;
				pclTmpBuffer += ilRc;
				ilRc = RC_SUCCESS;
			}
	} while (ilRc == RC_SUCCESS && llBytesToRead > 0);

	dbg(TRACE,"pclTcpBuffer:<%s> total/read Bytes %d/%d",
		pclTcpBuffer,llLocalLen,llBytesAlreadyRead);

	if (strncmp("MSGLEN:",pclTcpBuffer,7) != NULL)
	{
		dbg(TRACE,"GetCommandFromSocket: invalid size parameter <%s>",pclTcpBuffer);
		Terminate(igTerminateTimeout);
	}
	llLocalLen = atol(&pclTcpBuffer[7]);
	llBytesAlreadyRead  = 0;
	dbg(TRACE,"%05d:GetCommandFromSocket: Bytes to read <%ld> <%s>",
	 __LINE__,llLocalLen,&pclTcpBuffer[7]);

	if (llTcpBufferLen < llLocalLen)
	{
		pclTcpBuffer = realloc(pclTcpBuffer,llLocalLen+12);
		if (pclTcpBuffer == NULL)
		{
			dbg(TRACE,"GetCommandFromSocket: could not allocate %d Bytes for "
			"TcpBuffer, reason %d <%s>",llLocalLen,errno,strerror(errno));
			Terminate(igTerminateTimeout);
		}
		llTcpBufferLen = llLocalLen+10;
	}

	pclTmpBuffer = pclTcpBuffer;
	llBytesToRead = llLocalLen; 
	memset(pclTcpBuffer,0,llTcpBufferLen);
	do
	{
		dbg(TRACE,"reading %d bytes from socket",llBytesToRead);
		alarm(igTcpTimeout);
		errno = 0;
		ilRc = read(igWorkSocket,pclTmpBuffer,llBytesToRead);
		alarm(0);
		dbg(DEBUG,"RC from read is %d",ilRc);
		if(ilRc <= 0)
		{
			if (++ilCnt > 10)
			{
					ilRc = RC_FAIL;
					Terminate(igTerminateTimeout);
			}
			dbg(TRACE,"read: error %d %s",errno,strerror(errno));
		}
		else
		{
			ilCnt = 0;
			llBytesToRead -= ilRc;
			llBytesAlreadyRead += ilRc;
			pclTmpBuffer += ilRc;
			ilRc = RC_SUCCESS;
		}
	} while (ilRc == RC_SUCCESS && llBytesToRead > 0);

    dbg(DEBUG,"pclTcpBuffer:<%s> total/read Bytes %d/%d",
				pclTcpBuffer,llLocalLen,llBytesAlreadyRead);
	/** create BC_HEAD(CMDBLK and put on queue **/
	{
	  ITEM  *prlItem = NULL;
  	BC_HEAD *prlBchead = NULL;
    CMDBLK  *prlCmdblk = NULL;
    EVENT *prlEvent = NULL;
		char *pclQueue = NULL;
		int ilQueue = 0;

    MqToCeda(&prlItem,pclTcpBuffer);

    prlEvent = (EVENT *) prlItem->text;
	  prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT));
  	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
                strcpy(prlBchead->dest_name,"Atlantis");
		prlEvent->originator = mod_id;
		prlEvent->command = EVENT_DATA;
		pclQueue = mysGet_data(prlCmdblk->command,cgCommands,cgQueues);
		ilQueue = atoi(pclQueue);
		prlItem->priority = igPriority;
		if (ilQueue > 0)
		{
			char *pclSelection;
			char *pclFields;
			char *pclData;

    	pclSelection = prlCmdblk->data;
    	pclFields    = pclSelection + strlen(pclSelection) + 1;
    	pclData      = pclFields + strlen(pclFields) + 1;
		  DebugPrintItem(DEBUG,prlItem);
			DebugPrintBchead(DEBUG,prlBchead);
			DebugPrintCmdblk(DEBUG,prlCmdblk);
			dbg(DEBUG,"Command <%s> will be sent to queue %d",prlCmdblk->command,ilQueue);
			dbg(DEBUG,"Selection: <%s>",pclSelection);
			dbg(DEBUG,"Fields   : <%s>",pclFields);
			dbg(DEBUG,"Data     : <%s>",pclData);
		  que(QUE_PUT,ilQueue,mod_id,prlItem->priority,prlItem->msg_length,(char *)prlEvent);
		}
		else
		{
			dbg(TRACE,"No valid Command in Message <%s> ",pclTcpBuffer);
		}
	}
	return ilRc;
}



static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int ReadConfigRow(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,
			 CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Process()
{
    int  ilRc = RC_SUCCESS;			/* Return code */
    char clTmpStr[64] = "\0";
    char clSection[64] = "\0";
    char clKeyword[64] = "\0";
    char clTmpString[164] = "\0";
    int ilOldDebugLevel = 0;
    long pclAddFieldLens[12];
    char pclAddFields[256] = "\0";
    char pclSqlBuf[2560] = "\0";
    char pclSelection[1024] = "\0";
    short slCursor = 0;
    short slSqlFunc = 0;
    char  clBreak[24] = "\0";
    char clAlert[200] = "\0";

    if(ilRc == RC_SUCCESS)
    {
		/* read HomeAirPort from SGS.TAB */
			ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
			if (ilRc != RC_SUCCESS)
			{
	    	dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
	    	Terminate(30);
			} else {
	    	dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
				}
    	}

	
    	if(ilRc == RC_SUCCESS)
    	{
				GetDebugLevel("STARTUP_MODE", &igStartUpMode);
				GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
		 
				debug_level = igStartUpMode;
			}

			ilRc = ReadConfigEntry("MAIN","DIRECTION",clTmpStr);
			if(ilRc == RC_SUCCESS)
			{
					if (strcmp(clTmpStr,"OUT") == 0)
					{
						bgDirection = BRI_OUT;
						dbg(TRACE,"Direction is OUT");
					} 
					else if (strcmp(clTmpStr,"IN") == 0)
					{
						bgDirection = BRI_IN;
						dbg(TRACE,"Direction is IN");
				} else
					{
						dbg(TRACE,"Invalid direction <%s>",clTmpStr);
					}
			}
			else
			{
				dbg(TRACE,"Direction Parameter not found!");
			}

			ilRc = ReadConfigEntry("MAIN","SOCKETTYPE",cgSocketType);
			if(ilRc == RC_SUCCESS)
			{
				dbg(TRACE,"Bridge is <%s>",cgSocketType);
			} 
			else
			{
				dbg(TRACE,"Sockettype Parameter not found!");
			}

			ilRc = ReadConfigEntry("MAIN","STARTSCRIPT",cgStartScript);
			if(ilRc == RC_SUCCESS)
			{
	    		dbg(TRACE,"Startscript: <%s>",cgStartScript);
			}
			else
			{
				dbg(TRACE,"Startscript not found!");
				ilRc = RC_SUCCESS;
			}
			

			ilRc = ReadConfigEntry("MAIN","COMMANDS",cgCommands);
			if(ilRc == RC_SUCCESS)
			{
	    		dbg(TRACE,"Commandlist: <%s>",cgCommands);
			}
			else
			{
				dbg(TRACE,"Commandlist not found!");
			}
			if ((ilRc == RC_SUCCESS) && (bgDirection == BRI_IN))
			{
				ilRc = ReadConfigEntry("MAIN","QUEUES",cgQueues);
				if(ilRc == RC_SUCCESS)
				{
					dbg(TRACE,"Queuelist: <%s>",cgQueues);
				}
				else
				{
					dbg(TRACE,"Queuelist not found!");
				}
			}	
			ilRc = ReadConfigEntry("MAIN","PRIORITY",clTmpString);
			if(ilRc == RC_SUCCESS)
			{
					igPriority = atoi(clTmpString);
	    		dbg(TRACE,"PRIORITY: <%d>",igPriority);
			}
			else
			{
				dbg(TRACE,"PRIORITY not found!");
				ilRc = RC_SUCCESS;
			}
			
		
		 if ((ilRc == RC_SUCCESS)) /* && (bgDirection == BRI_IN))*/
		{
			ilRc = ReadConfigEntry("NETWORK","SERVICE",cgServiceIn);
			if(ilRc == RC_SUCCESS)
			{
	    	dbg(TRACE,"Port to be listened to %c",cgServiceIn);
			}
		}
		if ((ilRc == RC_SUCCESS)) /* && (bgDirection == BRI_OUT))*/
		{
			ilRc = ReadConfigEntry("NETWORK","HOST",cgHost);
			if(ilRc == RC_SUCCESS)
			{
	    	dbg(TRACE,"Host to connect to <%s>",cgHost);
			}
			ilRc = ReadConfigEntry("NETWORK","SERVICE",cgServiceOut);
			if(ilRc == RC_SUCCESS)
			{
	    	dbg(TRACE,"Port to connect to <%s>",cgServiceOut);
			}
		}

		ilRc = ReadConfigEntry("NETWORK","TCP_TIMEOUT",clTmpString);
		if(ilRc == RC_SUCCESS)
		{
	    igTcpTimeout = atoi(clTmpString);
	    dbg(TRACE,"TCP_TIMEOUT %d",igTcpTimeout);
		}
		else
		{
			ilRc = RC_SUCCESS;
		}
		
		ilRc = ReadConfigEntry("NETWORK","TERMINATE_TIMEOUT",clTmpString);
		if(ilRc == RC_SUCCESS)
		{
	    igTerminateTimeout = atoi(clTmpString);
	    dbg(TRACE,"TERMINATE_TIMEOUT %d",igTerminateTimeout);
		}
		else
		{
			ilRc = RC_SUCCESS;
		}
		
		ilRc = ReadConfigEntry("NETWORK","TCP_DELAY",clTmpString);
		if(ilRc == RC_SUCCESS)
		{
	    igTcpDelay = atoi(clTmpString);
	    dbg(TRACE,"TCP_DELAY %d",igTcpDelay);
		}
		else
		{
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("NETWORK","CONNECT_TIMEOUT",clTmpString);
		if(ilRc == RC_SUCCESS)
		{
	    igConnectTimeout = atoi(clTmpString);
	    dbg(TRACE,"CONNECT_TIMEOUT %d",igConnectTimeout);
		}
		else
		{
			ilRc = RC_SUCCESS;
		}
		
	
	if (ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"Init_Process failed");
			Terminate(igTerminateTimeout);
	}
	
   debug_level = igRuntimeMode;
   dbg(TRACE,"debug_level set to %d",igRuntimeMode);

   return(ilRc);
} 


static int GetDebugLevel(char *pcpMode, int *pipMode)
{
    int		ilRc = RC_SUCCESS;
    char	clCfgValue[64];

    ilRc = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
			 clCfgValue);

    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
    }
    else
    {
	dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
	if (!strcmp(clCfgValue, "DEBUG"))
	    *pipMode = DEBUG;
	else if (!strcmp(clCfgValue, "TRACE"))
	    *pipMode = TRACE;
	else
	    *pipMode = 0;
    }
    return ilRc;
}

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int	ilRc = RC_SUCCESS;				/* Return code */
	
    dbg(TRACE,"Reset: now resetting");
	
    return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    if (igWorkSocket > -1)
    {
				PutCommandOnSocket("<EVENT_COMMAND>28674</EVENT_COMMAND>","", " ", " ");
    }
    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);
    sleep(ipSleep < 1 ? 1 : ipSleep);

    signal(SIGCHLD,SIG_IGN);

    logoff();
		if (igWorkSocket > -1)
		{
			dbg(TRACE,"WorkSocket %d will be closed",igWorkSocket);
			close(igWorkSocket);
		}
		if (igStartSocket > -1)
		{
			dbg(TRACE,"StartSocket %d will be closed",igStartSocket);
			close (igStartSocket);
		}
		/********
    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);
    sleep(ipSleep < 1 ? 1 : ipSleep);
		***********/
	
    dbg(TRACE,"Terminate: now leaving ...");
    fclose(outp);
    exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
	default	:
	    Terminate(1);
	    break;
    } /* end of switch */

    exit(1);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
	    dbg(TRACE,"<%d> : unknown function",pipErr);
	    break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
	    dbg(TRACE,"<%d> : malloc failed",pipErr);
	    break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
	    dbg(TRACE,"<%d> : msgsnd failed",pipErr);
	    break;
	case	QUE_E_GET	:	/* Error using msgrcv */
	    dbg(TRACE,"<%d> : msgrcv failed",pipErr);
	    break;
	case	QUE_E_EXISTS	:
	    dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
	    break;
	case	QUE_E_NOFIND	:
	    dbg(TRACE,"<%d> : route not found ",pipErr);
	    break;
	case	QUE_E_ACKUNEX	:
	    dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
	    break;
	case	QUE_E_STATUS	:
	    dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
	    break;
	case	QUE_E_INACTIVE	:
	    dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
	    break;
	case	QUE_E_MISACK	:
	    dbg(TRACE,"<%d> : missing ack ",pipErr);
	    break;
	case	QUE_E_NOQUEUES	:
	    dbg(TRACE,"<%d> : queue does not exist",pipErr);
	    break;
	case	QUE_E_RESP	:	/* No response on CREATE */
	    dbg(TRACE,"<%d> : no response on create",pipErr);
	    break;
	case	QUE_E_FULL	:
	    dbg(TRACE,"<%d> : too many route destinations",pipErr);
	    break;
	case	QUE_E_NOMSG	:	/* No message on queue */
	    /*dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
	    break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
	    dbg(TRACE,"<%d> : invalid originator=0",pipErr);
	    break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
	    dbg(TRACE,"<%d> : queues are not initialized",pipErr);
	    break;
	case	QUE_E_ITOBIG	:
	    dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
	    break;
	case	QUE_E_BUFSIZ	:
	    dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
	    break;
	default			:	/* Unknown queue error */
	    dbg(TRACE,"<%d> : unknown error",pipErr);
	    break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int	ilRc = RC_SUCCESS;			/* Return code */
    int	ilBreakOut = FALSE;
	
    do
    {
	ilRc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	/* depending on the size of the received item  */
	/* a realloc could be made by the que function */
	/* so do never forget to set event pointer !!! */
	prgEvent = (EVENT *) prgItem->text;	

	if( ilRc == RC_SUCCESS )
	{
	    /* Acknowledge the item */
	    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	    if( ilRc != RC_SUCCESS ) 
	    {
		/* handle que_ack error */
		HandleQueErr(ilRc);
	    } /* fi */
		
	    switch( prgEvent->command )
	    {
		case	HSB_STANDBY	:
		    ctrl_sta = prgEvent->command;
		    break;	
	
		case	HSB_COMING_UP	:
		    ctrl_sta = prgEvent->command;
		    break;	
	
		case	HSB_ACTIVE	:
		    ctrl_sta = prgEvent->command;
		    ilBreakOut = TRUE;
		    break;	

		case	HSB_ACT_TO_SBY	:
		    ctrl_sta = prgEvent->command;
		    break;	
	
		case	HSB_DOWN	:
		    /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		    ctrl_sta = prgEvent->command;
		    Terminate(1);
		    break;	
	
		case	HSB_STANDALONE	:
		    ctrl_sta = prgEvent->command;
		    ResetDBCounter();
		    ilBreakOut = TRUE;
		    break;	

		case	REMOTE_DB :
		    /* ctrl_sta is checked inside */
		    HandleRemoteDB(prgEvent);
		    break;

		case	SHUTDOWN	:
		    Terminate(1);
		    break;
						
		case	RESET		:
		    ilRc = Reset();
		    break;
						
		case	EVENT_DATA	:
		    dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
		    DebugPrintItem(TRACE,prgItem);
		    DebugPrintEvent(TRACE,prgEvent);
		    break;
					
		case	TRACE_ON :
		    dbg_handle_debug(prgEvent->command);
		    break;

		case	TRACE_OFF :
		    dbg_handle_debug(prgEvent->command);
		    break;

		default			:
		    dbg(TRACE,"HandleQueues: unknown event");
		    DebugPrintItem(TRACE,prgItem);
		    DebugPrintEvent(TRACE,prgEvent);
		    break;
	    } /* end switch */
	} else {
	    /* Handle queuing errors */
	    HandleQueErr(ilRc);
	} /* end else */
    } while (ilBreakOut == FALSE);

    ilRc = Init_Process();
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Init_Process: init failed!");
    } 
     

} /* end of HandleQueues */




/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(ITEM *prpItem)
{
    int	   ilRc           = RC_SUCCESS;			/* Return code */
    int      ilCmd          = 0;

    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
		EVENT   *prlEvent        = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclBuf         = NULL;
    char 		clCCO[24];
    char 		clTable[34];
		char    clMsgLen[24];
    int			ilUpdPoolJob = TRUE;
    char clAlert[200] = "\0";
		int     ilBufLen;
		int     ilHeadLen;
		int 	ilCnt = 0;

		int ilBytesAlreadyWritten = 0;

		if (bgDirection != BRI_OUT)
		{
			dbg(TRACE,"Data command for incoming bridge");
			return ilRc;
		}

    prlEvent = (EVENT *) prpItem->text;
    prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;


    strcpy(clTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
/*	DebugPrintBchead(TRACE,prlBchead);*/
/*	DebugPrintCmdblk(TRACE,prlCmdblk);*/
    dbg(TRACE,"Command: <%s>",prlCmdblk->command);
    dbg(TRACE,"originator follows event = %p ",prlEvent);

    dbg(TRACE,"originator<%d>",prlEvent->originator);
    dbg(TRACE,"selection follows Selection = %p ",pclSelection);
    dbg(TRACE,"selection <%s>",pclSelection);
    dbg(TRACE,"fields    <%s>",pclFields);
    dbg(TRACE,"data      <%s>",pclData);
    /****************************************/

		if (bgConnected == FALSE)
		{
			dbg(TRACE,"Message not sent because socket ist not connected");
			return ilRc;
		}
		if (strcmp("CCO",prlCmdblk->command) == 0)
		{
			dbg(DEBUG,"Send Keepalive Message");
			strcpy(clCCO,"MSGLEN:00000003CCO");
	    alarm(igTcpTimeout);
	    errno = 0;
	    ilRc = write(igWorkSocket,clCCO,18);
	    alarm(0);
	    if(ilRc <= 0)
	    {
				dbg(TRACE,"write: error %d %s",errno,strerror(errno));
				Terminate(60);
	    }
		}
		else if (strstr(cgCommands,prlCmdblk->command) != NULL)
		{
			dbg(DEBUG,"Command <%s> will be sent to %s",prlCmdblk->command,cgHost);
			CedaToMq(prpItem,&pclBuf,NULL,NULL);
			ilBufLen = strlen(pclBuf);
			sprintf(clMsgLen,"MSGLEN:%08d",ilBufLen);
			dbg(DEBUG,"%s%s",clMsgLen,pclBuf);
			nap(igTcpDelay);

			ilHeadLen = 15;
			do
			{
	    	dbg(TRACE,"writing %d bytes to socket %d",ilHeadLen,igWorkSocket);
	    	alarm(igTcpTimeout);
	    	errno = 0;
	    	ilRc = write(igWorkSocket,&clMsgLen[ilBytesAlreadyWritten],ilHeadLen);
	    	alarm(0);
	    	if(ilRc <= 0)
	    	{
					if (++ilCnt > 10)
					{
		    			ilRc = RC_FAIL;
							Terminate(60);
					}
					dbg(TRACE,"write: error %d %s",errno,strerror(errno));
	    	}
	     else
	    {
				ilCnt = 0;
				ilHeadLen -= ilRc;
				ilBytesAlreadyWritten += ilRc;
				ilRc = RC_SUCCESS;
	   	}
		} while(ilHeadLen > 0);

		ilBytesAlreadyWritten = 0;
		do
    {
	    	dbg(TRACE,"writing %d bytes to socket",ilBufLen);
	    	alarm(igTcpTimeout);
	    	errno = 0;
	    	ilRc = write(igWorkSocket,&pclBuf[ilBytesAlreadyWritten],ilBufLen);
	    	alarm(0);
	    	if(ilRc <= 0)
	    	{
					if (++ilCnt > 10)
					{
		    			ilRc = RC_FAIL;
							Terminate(60);
					}
					dbg(TRACE,"write: error %d %s",errno,strerror(errno));
	    	}
	     else
	    {
				ilCnt = 0;
				ilBufLen -= ilRc;
				ilBytesAlreadyWritten += ilRc;
				ilRc = RC_SUCCESS;
	   	}
		} while(ilBufLen > 0);
		
		dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    return(RC_SUCCESS);
	}	
	else
	{
		dbg(TRACE,"Invalid Command: <%s>",prlCmdblk->command);
	}
} /* end of HandleData */

static int PutCommandOnSocket(char *pcpCommand,char *pcpData, char *pcpFields, char *pcpSelection)
{

    int ilRc = RC_SUCCESS;
    long llLocalLen;
    long llBytesToRead;
    long llBytesAlreadyWritten;
    int ilCnt = 0;
    static char *pclTcpBuffer = NULL;
    char *pclTmpBuffer = NULL;
    static long llTcbBufferSize = 0L;
    char clAlert[200] = "\0";

    llBytesAlreadyWritten = 0;
    ilCnt = 0;

    dbg(TRACE,"PutCommandOnSocket: Cmd <%s> Data <%s>",pcpCommand,pcpData);

    if (igWorkSocket > 0)
    {
	long llActualCommandLen;

	llActualCommandLen = 31;  /* 15+8+8 */
	llActualCommandLen += strlen(pcpCommand);
	llActualCommandLen += strlen(pcpData);
	llActualCommandLen += 8;
	llActualCommandLen += strlen(pcpFields);
	llActualCommandLen += 8;
	llActualCommandLen += strlen(pcpSelection);


	if (llActualCommandLen > llTcbBufferSize)
	{
	    pclTcpBuffer = realloc(pclTcpBuffer,llActualCommandLen);
	    llTcbBufferSize = llActualCommandLen;
	    if (pclTcpBuffer == NULL)
	    {
		dbg(TRACE,"could not allocate Tcp buffer, reason: %d <%s>",
		    errno,strerror(errno));
		sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
		Terminate(30);
	    }
	}

	pclTmpBuffer = pclTcpBuffer;
	sprintf(pclTcpBuffer,"MSGLEN:<%08ld>;#CMD#<%s>;#DAT#<%s>;#FLD#<%s>;#SEL#<%s>",
		llActualCommandLen-15,pcpCommand,pcpData,pcpFields,pcpSelection);
	llLocalLen = llActualCommandLen;
	do
	{
	    dbg(TRACE,"writing %d bytes to socket",llLocalLen);
	    dbg(TRACE,"writing  %d <%s> to socket",strlen(pclTmpBuffer),pclTmpBuffer);
	    alarm(igTcpTimeout);
	    errno = 0;
	    ilRc = write(igWorkSocket,pclTmpBuffer,llLocalLen);
	    alarm(0);
	    if(ilRc <= 0)
	    {
		if (++ilCnt > 10)
		{
		    ilRc = RC_FAIL;
		}
		dbg(TRACE,"write: error %d %s",errno,strerror(errno));
	    }
	    else
	    {
		ilCnt = 0;
		llLocalLen -= ilRc;
		llBytesAlreadyWritten += ilRc;
		pclTmpBuffer += ilRc;
		ilRc = RC_SUCCESS;
	    }
	} while (ilRc == RC_SUCCESS && llLocalLen > 0);
    }
    return ilRc;
}

