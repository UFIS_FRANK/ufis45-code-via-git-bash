#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Lis/LIS_Server/Base/Server/Interface/twrcom.c 1.2 2004/04/02 20:12:20SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*
 * ABB/AAT UFIS-ATC Handler
 *
 * J. Heilig September  2001
 *
 * This is the tower communications module of the UFIS Tower Handler (twrcom).
 * It handles the communication with the tower and the tower handler.
 *
 * twrcom implements the TOWER AOS Data Exchange interface specified in the
 * following specification:
 *
 *       ==========================================================
 *                A N A e.p. L I S B O N - P O R T U G A L
 *
 *           Volume :   9007  TOWER System
 *           Chapter :    22  SOFTWARE INTERFACE SPECIFICATION
 *           Section :     2  Tower AOS Data Exchange
 *
 *                               NAV e.p. LISBON [I031xx.SAF]
 *         M.H.G. den Blanken
 *                             DOC_REV 1  2001-06-27  22.2.x/yyy
 *       ==========================================================
 *
 * This documentation is referred to in the following as the "Specification".
 * For other related documents see the actual version of
 *
 *          ABB/AAT ATC Lissabon Tower Handler --Document list
 *          ==================================================
 *
 *                 Last updated: *********** J.Heilig
 *                             Version: ***
 *
 *
 *
 * Usage: twrcom fd [options] mcast_address mcast_port
 *
 * fd is the queue bridge pipe file descriptor, normally inserted by the
 *    parent process (queue bridge queue end)
 *
 * mcast_address is the class D multicasting address, e.g. 224.0.0.1
 * 
 * mcast_port is the port twrcom binds to
 *
 * Options are:
 * 
 *   -lo logflags (bit mask of nat.h:NAT_L* values, defaults to NAT_LERR)
 *   -lf filename (logfile, defaults to stderr)
 *   -ji interface_name (mcast join interface, e.g. "eth1", defaults to NULL)
 *   -si interface_name (mcast send interface, e.g. "eth1", defaults to NULL)
 *   -so socket_options (mcast socket opts, defaults to MC_BIND) (see nat.h)
 *   -sp send port (for internal tests, defaults to mcport)
 *   -xa exclude address : to filter out a sending address (SCO bug?)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include "nat.h"
#include "twrdat.h"

/*
 * config vars
 */

static int logopts = NAT_LERR|NAT_LTRC;
static char *logfile = NULL;
static char final_logfile[512];
static char *mcaddr;
static int mcport, mcsport = -1;
static int mcsopts = MC_BIND;
static char *jifname = NULL;
static char *sifname = NULL;
static char *xaddress = NULL;

/*
 * states enum and names
 */

typedef enum { ONLINE=0, OFFLINE, HALTED, TERMINATING, EXIT } State;
#define StateName(s) s==ONLINE?"ONLINE":s==OFFLINE?"OFFLINE":\
                     s==HALTED?"HALTED":s==TERMINATING?"TERMINATING":\
                     s==EXIT?"EXIT":"INVALID"
/*
 * exit codes
 */

#define EXC_NORMAL 0
#define EXC_QBLOST 1

/*
 * event handlers. These callbacks are called by the event handler 
 * lib. In EXIT state they must immediately return (events are queued 
 * up in the lib). We define a prolog with debug info for it.
 */

static int aos_input_cb(QBData *);
static int twr_input_cb(int, void *);
static int twr_output_cb(int, void *);
static int twrhb_timer_cb(void *);
static int ownhb_timer_cb(void *);
static int term_timer_cb(void *);
static int sigterm_cb(int, void *);
static int sighup_cb(int, void *);

#define CB_PROLOG(x) LOG(DBGLOG,x" called in state %s",StateName(state));\
                     if(state == EXIT) return 0;

/*
 * tower com vars and some constans from the Specification
 */

#define MAX_TWRMSG	TWRD_MAXMSG
#define MAX_TOCFRAMES	9
#define MAX_TOCKEYS 75
#define MAX_KMSG (MAX_TOCFRAMES*MAX_TOCKEYS)

typedef struct
{
	int mx;	
	int mcnt;
	TWRD_MESSAGE *msg[MAX_KMSG];
} KMsgTab;

static KMsgTab kmsg_intab = {0,0};
static KMsgTab kmsg_outtab = {0,0};

static MCSocket *mcsock, *mcsock2;
static int tid_twrhb = -1;
static int tid_ownhb = -1;
static int tid_term = -1;
static int sid_term = -1;
static int sid_hup = -1;
static int iid_twr = -1;
static int oid_twr = -1;
static State state = ONLINE;
static int excode = EXC_NORMAL;
static BlockFIFO *twrout_fifo = NULL;
static unsigned long msg_inseq = 0;
static unsigned long msg_outseq = 0;
static int sigex_hit = 0;

/*
 * utility functions
 */

static void usage(void);
static int get_opts(int, char **, int);
static void ignore_event(char *);
static void set_state(State);
static void process_aos_data(char *);
static void clear_twrout_fifo(void);
static void reg_kmsg(TWRD_MESSAGE *, KMsgTab *);
static void post_twrdata(char *p, int);
static void clear_to_term(void);
static void aos_send(short, char *, int );
static void process_req_toc(void);
static void process_toc(char *);
static void process_req(char *);
static void process_hb(unsigned long);
static void process_twr_data(TWRD_MESSAGE *, unsigned long, char *);
static void check_msgseq(unsigned long, unsigned long);
static int cmp_keys(const void *, const void *);
static void kmsg_dump(KMsgTab *);
static void issue_dataframe(TWRD_MESSAGE *);
static void process_esc(char *);

int main(int argc, char **argv)
{
int fd, ax;

/*
 * get parms and options
 */

	if (argc < 2 || sscanf(argv[1], "%d", &fd) != 1)
		usage();

	ax = get_opts(argc, argv, 2);

/*
 * Q&D solution to grab stderr into logfile - remove close(2) if in
 * doubt
 */
	if(logfile)
		close(2);

	if(NatSetLog(logopts, logfile, "TWRCOM") != 0)
	{
		printf("ERROR in NatSetLog!!\n");
	};

	fprintf(stderr, "TWRCOM: stderr redirected (you should see this in %s)\n",
					logfile);

	if(ax + 1 >= argc)
	{
		LOG(ERRLOG,"Missing mcast address and/or mcast port parameter");
		exit(1);
	}

	mcaddr = argv[ax];

	if(sscanf(argv[ax+1],"%d",&mcport) != 1)
	{
		LOG(ERRLOG,"Invalid mcast port parameter");
		exit(1);
	}

	LOG(DBGLOG,"Starting up. mcaddr=%s mcport=%d jif=%s sif=%s xaddr=%s",
              mcaddr, mcport, STR0(jifname), STR0(sifname), STR0(xaddress));
/*
 * Init event handling lib and the queue bridge pipe end
 */

	EHInitialize();

	if(QBSetup(fd, aos_input_cb) == -1)
	{
		LOG(ERRLOG,"QBSetup(%d...) failed, <%d> <%s>", fd, ERRNO, ERRTEXT);
		exit(1);
	}

	LOG(DBGLOG, "QBSetup Ok");

/*
 * Create mcast socket and set sending interface if required
 */

	if( (mcsock = MCSocket_Create(mcaddr, jifname, mcport, mcsopts)) == NULL)
	{
		LOG(ERRLOG,"MCSocket_Create failed, <%d> <%s>", ERRNO, ERRTEXT);
		exit(1);
	}

/*
 * Create second sock with another port number if required
 */
	if(mcsport > 0)
	{
		mcsopts = MC_LOOPBACK;
		if( (mcsock2 = MCSocket_Create(mcaddr, jifname, mcsport, mcsopts)) == NULL)
		{
			LOG(ERRLOG,"MCSocket_Create 2 failed, <%d> <%s>", ERRNO, ERRTEXT);
			exit(1);
		}
	}
	else
	{
		mcsock2 = mcsock;
	}

	if(sifname && MCSocket_SetIf(mcsock2, sifname) == -1)
	{
		LOG(ERRLOG,"MCSocket_SetIf failed, <%d> <%s>", ERRNO, ERRTEXT);
		exit(1);
	}

	LOG(DBGLOG, "MCSocket Ok");

/*
 * setup timers, signal and tower input callback.
 * Note that the term timer and the tower output callbacks
 * will be set upon need.
 */

	tid_twrhb = EHAddTimer(6500, twrhb_timer_cb, NULL);
	ASSERT(tid_twrhb != -1);
	tid_ownhb = EHAddTimer(5000, ownhb_timer_cb, NULL);
	ASSERT(tid_ownhb != -1);
	sid_term = EHAddSignal(SIGTERM, sigterm_cb, NULL);
	ASSERT(sid_term != -1);
	sid_hup = EHAddSignal(SIGHUP, sighup_cb, NULL);
	ASSERT(sid_hup != -1);
	iid_twr = EHAddInput(MCSocket_GetSock(mcsock),twr_input_cb, NULL);
	ASSERT(iid_twr != -1);

	ASSERT((twrout_fifo = BlockFIFO_Create()));

/*
 * The main loop: get the work done. To see that we do it right,
 * state changes are verified.
 */

	while(state != EXIT)
	{
		EHHandleEvents(0);
	}
/*
 * Free resources. Except for the mcast socket, this could be omitted but
 * it's a good occasion to verify program integrity. In a future version
 * allowing a restart this step is always required.
 */

	ASSERT(QBSetup(-1, NULL) != -1);
	ASSERT(EHRemoveTimer(tid_twrhb) != -1);
	ASSERT(EHRemoveTimer(tid_ownhb) != -1);
	ASSERT(EHRemoveSignal(sid_term) != -1);
	ASSERT(EHRemoveSignal(sid_hup) != -1);
	ASSERT(EHRemoveInput(iid_twr) != -1);
	if(tid_term != -1)
		ASSERT(EHRemoveTimer(tid_term) != -1);
	MCSocket_Destroy(mcsock);
	if(mcsock2 != mcsock)
		MCSocket_Destroy(mcsock2);

/*
 * Done: give the parent some time to close the queue bridge. If it does
 * so then the qb sends a SIGTERM that interrupts the sleep() call. If it
 * doesn't, then the program exits after 3 seconds.
 *
 * sigex_hit inidicates that a signal causing exit (sigterm oder sighup)
 * has already been received.
 */

	if(excode != EXC_QBLOST && !sigex_hit)
	{
		LOG(DBGLOG, "EXIT: waiting for SIGTERM");
		sleep(3);
	}

	LOG(TRCLOG, "Exiting (%d)", excode);
	exit(excode);
}

/*
 * handle data from twrhdl
 */

int aos_input_cb(QBData *p)
{
short cmd;
int ix;
long val;

	CB_PROLOG("aos_input_cb");

	if(p == NULL)
	{
		LOG(ERRLOG, "received NULL from twrhdl -- exiting");
		excode = EXC_QBLOST;
		set_state(EXIT);
		return 0;
	}

	LOG(TRCLOG, "AOS RECV: size=%d type=%d", p->size, p->type);

	if(p->type == 1)
	{
		ix = 0;
		if(QB_GET_Short(&cmd, p, &ix) || QB_GET_Long(&val, p, &ix) || cmd != 1)
		{
			ignore_event("aos_input: bad type 1 data");
		}
		else if(state == OFFLINE || state == HALTED)
		{
			set_state(EXIT);
		}
		else if(state == TERMINATING)
		{
			ignore_event("aos_input: TERMINATE cmd");
		}
		else
		{
			if(val <= 0)
				set_state(EXIT);
			LOG(DBGLOG,"setting termination timer to %ld", val);
			ASSERT((tid_term=EHAddTimer(val, term_timer_cb, NULL)) != -1);
			set_state(TERMINATING);
		}
	}
	else if(p->type == 2)
	{
		if(state == TERMINATING)
		{
			ignore_event("aos_input: AOS data");
		}
		else
		{
			process_aos_data(p->data);
		}
	}
	free(p);
	return 0;
}

int twrhb_timer_cb(void *p)
{
	CB_PROLOG("twrhb_timer_cb");

	if(state == ONLINE || state == OFFLINE)
		set_state(HALTED);
	else if(state == TERMINATING)
		set_state(EXIT);

	return 0;
}

int ownhb_timer_cb(void *p)
{
char buff[100];
	CB_PROLOG("ownhb_timer_cb");

	if(state == ONLINE || state == TERMINATING)
	{
		sprintf(buff,"HB 0x%04hX",(unsigned short) msg_outseq);
		post_twrdata(buff, 1);
	}

	return 0;
}

/*
 * termination timer expired
 */

int term_timer_cb(void *p)
{
	CB_PROLOG("term_timer_cb");
	clear_to_term();
	return 0;
}

int sigterm_cb(int n, void *p)
{
	sigex_hit = 1;
	CB_PROLOG("sigterm_cb");
	set_state(EXIT);
	return 0;
}

int sighup_cb(int n, void *p)
{
	sigex_hit = 1;
	CB_PROLOG("sighup_cb");
	set_state(EXIT);
	return 0;
}

int twr_input_cb(int fd, void *p)
{
int len;
char buff[MAX_TWRMSG], *pai;
unsigned long seq;
unsigned short hseq;
TWRD_MESSAGE *msg;
	CB_PROLOG("twr_input_cb");

/*
	len = MCSocket_Read(mcsock, buff, MAX_TWRMSG);
 */
	len = MCSocket_ReadAI(mcsock, buff, MAX_TWRMSG, &pai);

	ASSERT(len < MAX_TWRMSG);
	if(len == -1)
	{
		LOG(ERRLOG,"MCSocket_ReadAI failed, <%d> <%s>", ERRNO, ERRTEXT);
		if(state == TERMINATING)
			set_state(EXIT);
		else
			set_state(HALTED);
		return 0;
	}
	ASSERT(len > 0); /* JWE auf Rat von JHE am 28.11.2001*/

	if(xaddress && strcmp(xaddress, pai) == 0)
	{
		LOG(DBGLOG,"ignoring datagram from <%s>", xaddress);
		return 0;
	}

	buff[len] = '\0';
	LOG(TRCLOG,"TWR RECV <%s>", buff);
/*
 * restart tower heartbeat timer
 */
	ASSERT(EHResetTimer(tid_twrhb) != -1);
/*
 * process frame
 */
  if(strncmp(buff,"/!",2) == 0) /* escape for special(test) commands */
  {
		process_esc(buff);
  }
	else if(strncmp(buff,"START LINK",10) == 0)
	{
		if(state == ONLINE || state == TERMINATING)
			ignore_event("unexpected START LINK");
		else
			set_state(ONLINE);
	}
	else if(strncmp(buff,"STOP LINK",9) == 0)
	{
		if(state == OFFLINE)
			ignore_event("unexpected STOP LINK");
		else if(state == TERMINATING)
			clear_to_term();
		else
			set_state(OFFLINE);
	}
	else if(strncmp(buff,"REQ_TOC",7) == 0)
	{
		if(state == ONLINE || state == TERMINATING)
			process_req_toc();
		else
			ignore_event("unexpected REQ_TOC");
	}
	else if(strncmp(buff,"TOC",3) == 0)
	{
		if(state == ONLINE || state == TERMINATING)
			process_toc(buff);
		else
			ignore_event("unexpected TOC");
	}
	else if(strncmp(buff,"REQ",3) == 0)
	{
		if(state == ONLINE || state == TERMINATING)
			process_req(buff);
		else
			ignore_event("unexpected REQ");
	}
	else if(sscanf(buff,"HB 0x%hx",&hseq) == 1)
	{
		seq = (unsigned long) hseq;
		if(state == HALTED)
			set_state(ONLINE);
		if(state == ONLINE || state == TERMINATING)
			process_hb(seq);
		/* silently ignore OFFLINE */
	}
	else if(sscanf(buff,"0x%hx",&hseq) == 1 && (msg = TWRDDecode(buff)) != NULL)
	{
		seq = (unsigned long) hseq;
		if(state == ONLINE || state == TERMINATING)
			process_twr_data(msg, seq, buff);
		/* silently ignore HALTED and OFFLINE */
		/* 0928-diabled JWE */
		/* free(msg);*/
	}
	else
	{
		ignore_event("bad twr data");
	}
	return 0;
}

static int twr_output_cb(int fd, void *v)
{
size_t len;
char *p;
int rc;
	p = BlockFIFO_Get(twrout_fifo, &len);
	ASSERT(p);
	ASSERT(len);
	LOG(TRCLOG,"TWR SEND <%s>", p);
	rc = MCSocket_Write(mcsock2, p, len);
	free(p);
	if (rc == -1)
	{
		LOG(ERRLOG,"MCSocket_Write failed, <%d> <%s>", fd, ERRNO, ERRTEXT);
		set_state(HALTED); /* empties fifo! */
	}

 	if(BlockFIFO_Count(twrout_fifo) == 0)
	{
		ASSERT(EHRemoveOutput(oid_twr) != -1);
		oid_twr = -1;
	}
	return 0;
}

/*
 * Type 2 messages (AOS to TWR) messages are processed here
 */

void process_aos_data(char *data)
{
TWRD_MESSAGE *msg;

	LOG(DBGLOG,"processing aos data <%s>", data);
	if( (msg = TWRDDecode(data)) == NULL)
	{
		ignore_event("process_aos_data: bad AOS message");
		return;
	}
	issue_dataframe(msg);
}

/*
 * this function assigns a new sequence number to the message and stores
 * it in the table of issued messages. When online, the message is
 * also sent.
 */

void issue_dataframe(TWRD_MESSAGE *msg)
{
char *p;
/*
 * Set the message number and register the msg in the key table for
 * retransmission. Don't delete the message here: it will be discarded
 * when its slot is reused.
 */
	++msg_outseq;
	ASSERT(TWRD_SET(msg, msg_seq, msg_outseq) == 1);
	reg_kmsg(msg, &kmsg_outtab);
/*
 * post the message if online: the output fifo owns the buffer
 */
	if(state == ONLINE || state == TERMINATING)
	{
		p = TWRDEncode(msg);
		ASSERT(p);
		post_twrdata(p, 0);
	}
}

/*
 * options. return index of first not used arg or exits on error
 */

int get_opts(int argc, char **argv, int ax)
{
	for(; ax < argc && *(argv[ax]) == '-'; ++ax)
	{
		if(strcmp(argv[ax],"-lo") == 0)
		{
			++ax;
			if(ax == argc || (sscanf(argv[ax],"0x%x", &logopts) != 1 &&
         sscanf(argv[ax],"%d", &logopts) != 1 &&
			   sscanf(argv[ax],"x%x", &logopts) != 1 ))
			{
				fprintf(stderr,"bad or missing logopts spec");
				usage();
			}
		}
		else if(strcmp(argv[ax],"-lf") == 0)
		{
			++ax;
			if(ax == argc)
			{
				fprintf(stderr,"logfile name expected");
				usage();
			}
			else
			{
				logfile = argv[ax];
				memset(final_logfile,0x00,512);
				sprintf(final_logfile,"%s%.5ld.log",logfile,getpid());
				logfile = final_logfile;
			}
		}
		else if(strcmp(argv[ax],"-ji") == 0)
		{
			++ax;
			if(ax == argc)
			{
				fprintf(stderr,"interface name expected");
				usage();
			}
			else
				jifname = argv[ax];
		}
		else if(strcmp(argv[ax],"-si") == 0)
		{
			++ax;
			if(ax == argc)
			{
				fprintf(stderr,"interface name expected");
				usage();
			}
			else
				sifname = argv[ax];
		}
		else if(strcmp(argv[ax],"-xa") == 0)
		{
			++ax;
			if(ax == argc)
			{
				fprintf(stderr,"address specification expected");
				usage();
			}
			else
				xaddress = argv[ax];
		}
		else if(strcmp(argv[ax],"-so") == 0)
		{
			++ax;
			if(ax == argc || sscanf(argv[ax],"%d", &mcsopts) != 1 || mcsopts < 0)
			{
				fprintf(stderr,"bad or missing socket opts");
				usage();
			}
		}
		else if(strcmp(argv[ax],"-sp") == 0)
		{
			++ax;
			if(ax == argc || sscanf(argv[ax],"%d", &mcsport) != 1)
			{
				fprintf(stderr,"bad or missing send port");
				usage();
			}
		}
		else
		{
			usage();
		}
	}
	return ax;
}

void usage()
{
	fprintf(stderr,"twrcom pipe-fd options mc-addr mc-port\n");
	exit(1);
}

void set_state(State new_state)
{
#define NUM_STATES EXIT-ONLINE+1
int legal_trans[NUM_STATES][NUM_STATES] =
{
/*                 ONLINE    OFFLINE   HALTED  TERMINATING  EXIT */
/* ONLINE      */ {   0,        1,       1,        1,        1 },
/* OFFLINE     */ {   1,        0,       1,        0,        1 },
/* HALTED      */ {   1,        1,       0,        1,        1 },
/* TERMINATING */ {   0,        0,       0,        0,        1 },
/* EXIT        */ {   0,        0,       0,        0,        0 }
};
	LOG(TRCLOG,"State change %s -> %s", StateName(state), StateName(new_state));
	ASSERT(state >= ONLINE && state <= EXIT);
	ASSERT(legal_trans[state][new_state]);
	if(state == ONLINE && (new_state == OFFLINE || new_state == HALTED) )
		clear_twrout_fifo();
	state = new_state;
}

void ignore_event(char *msg)
{
	LOG(ERRLOG,"Ignoring <%s> in state %s", msg, StateName(state));
}

void clear_twrout_fifo()
{
void *p;
size_t s;
	while(BlockFIFO_Count(twrout_fifo))
	{
		p = BlockFIFO_Get(twrout_fifo, &s);
		ASSERT(s);
		free(p);
	}
}

/*
 * register message with sequence number
 */

void reg_kmsg(TWRD_MESSAGE *msg, KMsgTab *mtab)
{
int ix;
unsigned long key, mkey;
/*
 * get the message key
 */
	ASSERT(TWRD_GET(msg,fKey,&key)==1);
/*
 * find key in table
 */
	for(ix=0; ix < mtab->mcnt; ++ix)
	{
		ASSERT(TWRD_GET(mtab->msg[ix], fKey, &mkey)==1);
		if(mkey == key)
			break;
	}
/*
 * not found -> insert or replace if table full
 */
	if(ix >= mtab->mcnt)
	{
		ix = mtab->mx;
		if(mtab->mcnt < MAX_KMSG) /* mtab->mx points to empty slot */
		{
			++mtab->mcnt;
		}
		else /* mtab->mx points to next reusable slot */
		{
			ASSERT(mtab->msg[mtab->mx]);
			free(mtab->msg[mtab->mx]);
		}
/*
 * move index to next slot
 */
		++mtab->mx;
		if(mtab->mx >= MAX_KMSG)
			mtab->mx = 0;
	}
/*
 * set ptr to data 
 */
	mtab->msg[ix] = msg;
/*
 * debugging
 */
	kmsg_dump(mtab);
}

/*
 * place zero terminated message into output fifo and request output if
 * necessary.  If cp is nonzero then the messages is copied. Otherwise
 * it must be dynamically allocated by the user and must not be freed.
 */

static void post_twrdata(char *p, int cp)
{
char *pc;
	LOG(DBGLOG,"post_twrdata: posting <%s>",p);
	if(cp)
		pc = strdup(p);
	else
		pc = p;
	ASSERT(pc);
	ASSERT(BlockFIFO_Put(twrout_fifo, pc, 0) == 0);
	if(oid_twr == -1)
	{
		oid_twr = EHAddOutput(MCSocket_GetSock(mcsock2),twr_output_cb, NULL);
		ASSERT(oid_twr != -1);
	}
}

/*
 * send "clear to terminate" message to tower handle
 */

void clear_to_term()
{
char *pb;
int ix;
	ix = 0;
	ASSERT(QB_PUT_Short(1, &pb, &ix) == 0);
	ASSERT(QB_PUT_Long(0, &pb, &ix) == 0);
	aos_send(1, pb, ix);
	free(pb);
	set_state(EXIT);
}

/*
 * send message to tower handler. buff is not freed -- is must be 
 * freed by the caller if it was dynamically allocated.
 */

void aos_send(short t, char *buff, int len)
{
int rc;
	LOG(TRCLOG,"AOS SEND: type=%d len=%d", t, len); 
	if( (rc = QBSendMessage(t, buff, len, 1000)) == -1)
	{
		LOG(ERRLOG,"QBSendMessage failed, <%d> <%s>", ERRNO, ERRTEXT);
		excode = EXC_QBLOST;
		set_state(EXIT);
	}
}

/*
 * this message has the format <toc_seq> <toc_frames> {<key> <seq>}
 * toc_seq and toc_frame are not really used except for checking that
 * toc_frames >= 1 && toc_frames <= 9 && toc_seq >= 1 && toc_seq <= toc_frames
 * In addition, the number of <key> <seq> pairs must not exceed MAX_TOCKEYS
 * per frame.
 *
 * We could also check here if the table of contents has been requested but
 * we do not.
 *
 * We use the TWRD data type TWRD_MSGITEM to simply get each data item into
 * a buffer.
 */

void process_toc(char *data)
{
#define MAX_TOCITEMS (MAX_TOCKEYS+3) /* TOC seq frm ...*/
/*TWRD_MSGITEM items[MAX_TOCITEMS];*/
TWRD_MSGITEM items[(MAX_TOCKEYS*2)+3];
unsigned long keys[MAX_TOCKEYS], key;
unsigned long seqs[MAX_TOCKEYS], seq;
int toc_seq, tot_frm;
int nit,nit_cnt, kx, mx;
char buff[100], *pk, *ps;
	LOG(DBGLOG,"process_toc");
	toc_seq = -1;
	tot_frm = -1;
/*
 * get total number of fields and check consistency
 */
	/*nit = TWRDGetMsgItems(&data, items, MAX_TOCITEMS);*/
	nit = TWRDGetMsgItems(&data, items, (2*MAX_TOCKEYS)+3);
	if(nit < 4 || sscanf(items[2],"%d",&tot_frm) != 1 || tot_frm < 1 ||
     tot_frm > MAX_TOCFRAMES || sscanf(items[1],"%d",&toc_seq) != 1 ||
     toc_seq < 1 || toc_seq > MAX_TOCKEYS)
	{
		sprintf(buff,"bad TOC data toc_seq=%d tot_frm=%d", toc_seq, tot_frm);
		ignore_event(buff);
		return;
	}
	LOG(DBGLOG,"process_toc: nit=<%d>",nit);
/*
 * get corresponding number of key,seq pairs
 */
	/*for(kx=0; kx<toc_seq; ++kx)*/ /* JWE: 01.10.2002 wrong counter toc_seq! */
																	/* needs to be (nit-3)/2 to process all messages */
																	/* Thanks to Mr. Heilig, identifying the bug. */
																	/* waiting for my 2. bottle of wine */

	/* number of items must not give a rest on modulo operation */
	/* because otherwise we have a incomplete key/seq pair which can not pe processed */
	ASSERT(((nit-3)%2)==0);

	nit_cnt = (nit-3)/2; /* -substracting 3 from nit for eliminating "<TOC X Y" from TOC-answer */ 
											/*  -divide by 2 for getting the number of key/seq-pairs inside TOC-anser */

	LOG(DBGLOG,"process_toc: nit_cnt=<%d>",nit_cnt);
	for(kx=0; kx<nit_cnt; ++kx)
	{
		pk = items[2*kx+3];
		ps = items[2*kx+4];
		if(sscanf(pk,"0x%lx",&(keys[kx]))!=1 || sscanf(ps,"0x%lx",&(seqs[kx]))!=1)
		{

			LOG(DBGLOG,"process_toc: keys=<%lx> seq=<%lx>",keys[kx],seqs[kx]);
			ignore_event("bad (key,seq) pair");
			return;
		}
		LOG(DBGLOG,"process_toc: <%d> keys=<%lx> seq=<%lx>",kx,keys[kx],seqs[kx]);
	}
/*
 * check for each key if we have the sequence
 */
	/*for(kx=0; kx<toc_seq; ++kx)*/ /* JWE: 01.10.2002 wrong counter toc_seq! */
																	/* needs to be (nit-3)/2 to process all messages */
																	/* Thanks to Mr. Heilig, identifying the bug. */
																	/* waiting for my 2. bottle of wine */
	for(kx=0; kx<nit_cnt; ++kx)
	{
		for(mx=0; mx < kmsg_intab.mcnt; ++mx)
		{
			ASSERT(TWRD_GET(kmsg_intab.msg[mx], fKey, &key) == 1);
			ASSERT(TWRD_GET(kmsg_intab.msg[mx], msg_seq, &seq) == 1);
			if(key == keys[kx] && seq == seqs[kx])
				break;
		}
/*
 * request for retransmission if the key,seq pair is not found
 */
		if(mx >= kmsg_intab.mcnt)
		{
			LOG(DBGLOG,"requesting 0x%08X", keys[kx]);
			sprintf(buff,"REQ 0x%08lX",keys[kx]);
			post_twrdata(buff, 1);
		}
	}
}

/*
 * retransmit data frames specified by the request. Although the 
 * Specification does not explicitly state it, we assume a maximum
 * of MAX_TOCKEYS to be requested.
 */

void process_req(char *data)
{
#define MAX_REQITEMS (MAX_TOCKEYS+1)
TWRD_MSGITEM items[MAX_REQITEMS]; /* REC keys... */
unsigned long keys[MAX_TOCKEYS], key;
int nit, kx, mx;
	LOG(DBGLOG,"process_req");
/*
 * get the key fields
 */
	nit = TWRDGetMsgItems(&data,items,MAX_REQITEMS);
	if(nit < 2)
	{
		ignore_event("bad REQ data");
		return;
	}
/*
 * scan the key values
 */
	for(kx=0; kx < nit-1; ++kx)
	{
		if(sscanf(items[kx+1],"0x%lx",&(keys[kx])) != 1)
		{
			ignore_event("bad key data");
			return;
		}
	}
/*
 * send the ones we have
 */
	for(kx=0; kx < nit-1; ++kx)
	{
		for(mx=0; mx<kmsg_outtab.mcnt; ++mx)
		{
			ASSERT(TWRD_GET(kmsg_outtab.msg[mx], fKey, &key) == 1);
			if(key == keys[kx])
				break;
		}
		if(mx >= kmsg_outtab.mcnt)
		{
			LOG(DBGLOG,"don't have msg with key 0x%lx", keys[kx]);
			continue;
		}
		LOG(DBGLOG,"retransmitting msg with key 0x%lx", keys[kx]);
		issue_dataframe(kmsg_outtab.msg[mx]);
	}
}

/*
 * build and send table of contents. The output is sorted on the key value.
 */

void process_req_toc()
{
int nframes, fx, mx, kx;
KMsgTab sorted_tab;
char b1[MAX_TWRMSG], b2[MAX_TWRMSG];
int l1, l2;
unsigned long key, seq;

	LOG(DBGLOG,"process_req_toc");
	nframes = kmsg_outtab.mcnt/MAX_TOCKEYS;
	if(kmsg_outtab.mcnt%MAX_TOCKEYS)
		++nframes;
	ASSERT(nframes <= MAX_TOCFRAMES);
/*
 * copy the output msg table (safe to use bitwise struct copy)
 */
	sorted_tab = kmsg_outtab;
/*
 * sort the table
 */
	qsort(sorted_tab.msg, sorted_tab.mcnt, sizeof(TWRD_MESSAGE *), cmp_keys);
/*
 * dump for debug
 */
	LOG(DBGLOG,"sorted TOC:");
	kmsg_dump(&sorted_tab);
/*
 * send the toc frames
 */
	for(mx=fx=0; fx < nframes && mx < sorted_tab.mcnt; ++fx)
	{
		sprintf(b1,"TOC %d %d", fx+1, nframes);
		l1 = strlen(b1);
		for(kx=0; kx<MAX_TOCKEYS && mx < sorted_tab.mcnt; ++kx, ++mx)
		{
			ASSERT(TWRD_GET(sorted_tab.msg[mx], fKey, &key) == 1);
			ASSERT(TWRD_GET(sorted_tab.msg[mx], msg_seq, &seq) == 1);
			sprintf(b2," 0x%08lX 0x%04hX", key, (unsigned short) seq);
			l2 = strlen(b2);
			ASSERT(l1+l2 < MAX_TWRMSG-1);
			memcpy(b1+l1, b2, l2);
			l1 += l2;
		}
		b1[l1] = '\0';
		post_twrdata(b1, 1);
	}
}

void process_hb(unsigned long seq)
{
	LOG(DBGLOG,"process_hb");
/*
 * the sequence number of the hb frame should equal to that
 * of the most recent data frame
 */
	check_msgseq(seq, msg_inseq);
}

void process_twr_data(TWRD_MESSAGE *msg, unsigned long seq, char *msgb)
{
	LOG(DBGLOG,"process_twr_data");
/*
 * issue a warning if suspicious message
 */
 if(TWRDErrorCount(msg) != 0)
	LOG(ERRLOG,"*** WARNING : nonzero twr msg error count");
/*
 * the sequence number of the data frame should equal to that
 * of the most recent data frame plus one
 */
	check_msgseq(seq, ++msg_inseq);
	reg_kmsg(msg, &kmsg_intab);
	aos_send(2, msgb, strlen(msgb)+1);
}

/*
 * check for gaps in the message stream. If the received sequence 
 * number is not equal to the expected one then the table of contents
 * is requested and the sequence number is set to the received one.
 */

void check_msgseq(unsigned long seq, unsigned long exp_seq)
{
	if(seq == exp_seq)
		return;
	msg_inseq = seq;
	LOG(TRCLOG, "check_msgseq: detected msg seq gap -> REQ_TOC");
	post_twrdata("REQ_TOC", 1);
}
	
/*
 * used by qsort to sort messages by key
 */
static int cmp_keys(const void *p1, const void *p2)
{
unsigned long k1, k2;
	ASSERT(TWRD_GET(*(TWRD_MESSAGE **)p1, fKey, &k1) == 1);
	ASSERT(TWRD_GET(*(TWRD_MESSAGE **)p2, fKey, &k2) == 1);
	return k1 < k2 ? -1 : k1 > k2 ? 1 : 0;
}

/*
 * output key table for diagnostics purposes
 */

void kmsg_dump(KMsgTab *mtab)
{
int ix;
unsigned long mkey, mseq;
	LOG(DBGLOG, "===== reg_kmsg: KMsgTab dump follows =====");
	for(ix=0; ix<mtab->mcnt; ++ix)
	{
		ASSERT(TWRD_GET(mtab->msg[ix], fKey, &mkey)==1);
		ASSERT(TWRD_GET(mtab->msg[ix], msg_seq, &mseq)==1);
		LOG(DBGLOG, "  %3d : key:%08lX seq=%08lX", ix, mkey, mseq);
	}
	LOG(DBGLOG, "===== reg_kmsg: KMsgTab dump done =====");
}

/*
 * Special tower escape commands
 */

void process_esc(char *buff)
{
TWRD_MSGITEM items[3]; /* for sleep */
int nit;
unsigned int s;
char *p;

/*
 * currently only sleep implemented
 *
 * format = "/! sleep|SLEEP seconds" where seconds is an usigned int
 *
 * All program activities are suspended until the sleep interval elapses
 * or a signal is received by the program.
 */

	p = buff; /* gets moved */
	nit = TWRDGetMsgItems(&p, items, 3);
	if(nit < 3 || strcmp(items[0],"/!") || (strcmp(items[1],"sleep") && 
		 strcmp(items[1],"SLEEP")) || sscanf(items[2],"%u",&s) != 1)
	{
		ignore_event(buff);
		return	;
	}

	LOG(DBGLOG,"process_esc: sleep(%u)", s);
	sleep(s);
	LOG(DBGLOG,"process_esc: sleep done.");
	
}
