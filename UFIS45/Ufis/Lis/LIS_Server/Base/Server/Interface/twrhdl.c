#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Lis/LIS_Server/Base/Server/Interface/twrhdl.c 1.13 2007/12/14 22:37:29SGT akl Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB AAT/I Program twrhdl.c    	                                            */
/*                                                                            */
/* Author         : Joern Weerts                                              */
/* Date           : 27.08.2001                                                */
/* Description    : interface process for ATC-tower communication lisbon	*/
/* Update history :                                           								*/
/*                  jwe 27.08.2001                                            */
/*                  started with programming                                  */
/*                  date/who                                                  */
/*                  action                                                    */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
/* be carefule with strftime or similar functions !!!                         */
/* static char sccs_twrhdl[]="@(#) UFIS 4.4 (c) ABB AAT/I twrhdl.c SCM 44.1 / 00/08/27 00:00:00 / JWE"; */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <dirent.h>
#include <arpa/inet.h>
#include "ugccsma.h"
#include "quedef.h"
#include "msgno.h"
#include "glbdef.h"
#include "uevent.h"
#include "db_if.h"
#include "sthdef.h"
#include "helpful.h"
#include "hsbsub.h"
#include "debugrec.h"
#include "netin.h"
#include "tcputil.h"
#include "list.h"
#include "twrdat.h"
#include "nat.h"
#include "twrhdl.h"

#define RC_CANCEL -99
/******************************************************************************/
/* External variables and functions                                           */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
int  igDebugLevel = 0;

extern int nap(unsigned long time);
extern int snap(char*,long,FILE*);
extern void BuildItemBuffer(char *, char *, int, char *);
extern int GetDataItem(char*,char*,int,char,char*,char*);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static int igCnt = 0;
static int igSigCnt = 0;
static ITEM  *prgItem      = NULL;    		/* The queue item pointer  */
static int igItemLen     	 = 0;       		/* length of incoming item */
static EVENT *prgEvent     = NULL;    		/* The event pointer       */
static char pcgProcessName[20];      			/* buffer for my own process name */ 
static char pcgHomeAp[20];      					/* buffer for name of the home-airport */
static char pcgTabEnd[20];      					/* buffer for name of the table extension */
static char pcgAftTable[20];							/* buffer for AFT-table to use */
static int igInitOK   		 = FALSE;				/* init flag */
static char pcgConfFile[S_BUFF];					/* buffer for filename */
static char *pcgSections = NULL;
static char *pcgSectionPointer = NULL;
static char *pcgDebugLevel = NULL;
static char *pcgTmpToken = NULL;
static char pcgTlxToken[XS_BUFF];
static char *pcgTryReconnect = NULL;

static LPLISTHEADER prgChildList = NULL;	/* pointer to internal data list */

static BOOL bgAlarm 					= FALSE;		/* global flag for timed-out socket-read */
static BOOL bgUseHopo					= FALSE;		/* global flag for use of HOPO database field */

static time_t tgLastTowerHB = 0; /* give the process some time to come up */
static time_t tgTowerTimeOut = 0;

static short sgCallsignCursor = 0;
static short sgArrCsCursor = 0;
static short sgDepCsCursor = 0;
static int igTimeAccuracy=0;
/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
/*----------------*/
/* MAIN functions */
/*----------------*/
static int Init_twrhdl(void);		/* main initializing */	
static int Reset(void);         /* Reset program          */
static int HandleIntData(void);        /* Handles data from CEDA */
static void	Terminate(BOOL bpSleep);        /* Terminate program      */
static void	HandleSignal(int);              /* Handles signals        */
static void	HandleErr(int);                 /* Handles general errors */
static void	HandleQueErr(int);              /* Handles queuing errors */
static void HandleQueues(void);             /* Waiting for Sts.-switch*/
static int ReadSections();
static int GetSection(char *pcpSection);
static int ReadCfg(char *pcpSection, int *ipGroup);
static int GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,
												char **pcpDest,int ipValueType,char *pcpDefVal);
static int CheckValue(char *pcpEntry,char *pcpValue,int ipType);
static void FreeCfgMem(void);
static int poll_q ();
/*static int Send_data(COMM_INFO *prpCommGroup,char *pcpData,int ipLen);*/
/*---------------------*/
/* formating functions */
/*---------------------*/
static void TrimRight(char *pcpBuffer);
static void TrimAll(char *pcpBuffer);
static int StrToTime(char *pcpTime,time_t *pclTime);
static void TimeToStr(char *pcpTime,time_t lpTime,int ipType,int ipLocal);
static void CleanMessage(char *pcpMsg,int ipMsgLen,char cpChar, char cpRepl);
static void TrimNewLine(char *pcpString);

static int FormatDate(char *pcpOldDate,char *pcpFormat,char *pcpNewDate);
static void StoreData(CHILD_INFO *prpGroup,char *pclUrnoBuf,char *pcpEngData,char *pcpLocalData);
static int UtcToLocal(char *oldstr,char *newstr);
/*-------------------*/
/* logging functions */
/*-------------------*/
static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile);
/*-----------------------------------------*/
/* functions for internal created messages */
/*-----------------------------------------*/
static int PrepareData(CHILD_INFO *prpGroup);
/*---------------------------*/
/* tower specific functions  */
/*---------------------------*/
static int FindFlightByTowerData(CHILD_INFO *prpChildGroup, TWR_DATA *prpNewMsg);
static int RunChild(char *pcpChild);
static int FindTowerInfoByCedaData(char *pcpFields,char *pcpData, TWR_DATA **prpTwrData,int *ipQBHandle);
static void KillChild(CHILD_INFO *prpChild);
static void GetChildByHandle(int ipHandle, CHILD_INFO **prpChild);
static int ProcessTwrData(char *pcpDataString,TWR_DATA *prpMsgCopy);
static int CheckTowerDataMsg(TWR_DATA *prpNewMsg,CHILD_INFO *prpChildGroup);
static void AppendToString(char *pcpFieldString,char *pcpFieldName,char *pcpDataString,char *pcpData);
static void InitMsg(TWR_DATA *prpMsg,int ipBytes);
static int TowerMsgToUfis(TWR_DATA *prpMsg,CHILD_INFO *prpChildGroup,char* pcpDataString);
static int SendEvent(char *pcpCmd,char *pcpSelection,char *pcpFields, char *pcpData,
											char *pcpAddStruct, int ipAddLen,int ipModIdSend,int ipModIdRecv,
											int ipPriority,char *pcpTable);
static int PrepareTowerMsg(TWR_DATA *prpTwrData, TWRD_MESSAGE *prpMsgForTower);
static int SendTowerData(int ipQBHandle,TWRD_MESSAGE *prpMsgForTower);
static int SendInfoTelex(TWR_DATA *prpMsg,char *pcpMsgString,char* pcpAddString,char* cpStatus,int ipDoSend);
static void SetCorrectDate(char *pcpScheduledDateTime, char *pcpCedaDateTwrTime, char *pcpResultDateTime);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;

	INITIALIZE;			/* General initialization	*/

	/* handles signal		*/
	(void)SetSignals(HandleSignal);
	(void)UnsetSignals();

  /* copy process-name to global buffer */
  memset(pcgProcessName,0x00,sizeof(pcgProcessName));
	strcpy(pcgProcessName,mod_name); 

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));
	
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}
	else
	{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}
	/* Attach to the DB */
	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
		dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
		sleep(6);
		ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}
	else
	{
		dbg(TRACE,"MAIN: init_db()  OK!");
	} /* end of if */
	/**********************************************************/
	/* following lines for identical binaries on HSB-machines */
	/**********************************************************/
	*pcgConfFile = '\0'; 
	sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),pcgProcessName);
	ilRc = TransferFile(pcgConfFile); 
	if(ilRc != RC_SUCCESS) 
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
	}
	
	/********************************************************************/
	/*For identical cfg-files on HSB-machines uncomment following lines */
	/********************************************************************/
	*pcgConfFile = '\0';
	sprintf(pcgConfFile,"%s/%s.cfg",getenv("CFG_PATH"),pcgProcessName);
	ilRc = TransferFile(pcgConfFile); 
	if(ilRc != RC_SUCCESS) 
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
	} 
	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	}
	
	/* now waiting if I'm STANDBY */
	if ((ctrl_sta != HSB_STANDALONE) &&
			(ctrl_sta != HSB_ACTIVE) &&
			(ctrl_sta != HSB_ACT_TO_SBY))
	{
		HandleQueues();
	}

	dbg(TRACE,"--------------------------------");
	if ((ctrl_sta == HSB_STANDALONE) ||
			(ctrl_sta == HSB_ACTIVE) ||
			(ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		dbg(TRACE,"--------------------------------");
		if(igInitOK == FALSE)
		{
			ilRc = Init_twrhdl();
			debug_level = TRACE;
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"--------------------------------");
				dbg(TRACE,"MAIN: Init_twrhdl() failed! Terminating!");
				Terminate(TRUE);
			}
		}
	} 
	else
	{
		dbg(TRACE,"MAIN: wrong HSB-state! Terminating!");
		Terminate(FALSE);
	}
	
	if (ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: initializing OK");
		dbg(TRACE,"--------------------------------");
		debug_level = igDebugLevel;

		for(;;)
		{
			if ((ilRc = poll_q()) == RC_SUCCESS)
			{
				dbg(TRACE,"MAIN: poll_q() failed!");
			}
		}
	}
	else
	{
		dbg(TRACE,"MAIN: Init_twrhdl() invalid! Terminating!");
	}
	Terminate(TRUE);
	return(0);
} /* end of MAIN */
/*********************************************************************
Function	 :Init_twrhdl
Paramter	 :IN:                                                      
Returnvalue:RC_SUCCES,RC_FAIL
Description:initializes the interface with all necessary values
*********************************************************************/
static int Init_twrhdl()
{
	int	ilRc = RC_FAIL;			/* Return code */
	int	ilGroup = 0;	
	char pclSection[XS_BUFF];
	
	/* reading default home-airport from sgs.tab */
	memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
	ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Init_twrhdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
		return RC_FAIL;
	}
	else
	{
		dbg(TRACE,"Init_twrhdl : HOMEAP = <%s>",pcgHomeAp);
	}
	
	/* reading default table-extension from sgs.tab */
	memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
	ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Init_twrhdl : No TABEND entry in sgs.tab: EXTAB! Please add!");
		return RC_FAIL;
	}
	else
	{
		*pcgAftTable = 0x00;
		strcpy(pcgAftTable,"AFT");
		dbg(TRACE,"Init_twrhdl : TABEND = <%s>",pcgTabEnd);
		strcat(pcgAftTable,pcgTabEnd);
		dbg(TRACE,"Init_twrhdl : AFT-table = <%s>",pcgAftTable);
		if (strcmp(pcgTabEnd,"TAB") == 0)
		{
			bgUseHopo = TRUE;
			dbg(TRACE,"Init_twrhdl : use HOPO-field!");
		}
	}

	if ((prgChildList=ListInit(prgChildList,sizeof(CHILD_INFO))) == NULL)
	{
		dbg(TRACE,"Init_twrhdl : couldn't create internal list of Child-elements!");
		return RC_FAIL;
	}

	/* now reading from configfile */
	if (*pcgConfFile != 0x00)
	{
		if ((ilRc = ReadSections()) == RC_SUCCESS)
		{
			while(GetSection(pclSection) == RC_SUCCESS)
			{
				if ((ilRc = ReadCfg(pclSection, &ilGroup)) == RC_SUCCESS)
				{
					igInitOK = TRUE;
				}
				else
				{
					dbg(TRACE,"Init_twrhdl : ReadCfg(%s) failed!",pclSection);
				}
			}
		}
	}
	/* starting child twrcom for communication with ATC-tower lisbon */
	if ((ilRc = RunChild("TWRCOM")) == RC_SUCCESS)
	{
		dbg(TRACE,"Init_twrhdl: Start of TWRCOM-child successfull or disabled!");
	}
	else
	{
		dbg(TRACE,"Init_twrhdl: Start of TWRCOM-child failed!");
	}
	tgLastTowerHB = time(NULL);
	return(ilRc);
} /* end of initialize */
/*********************************************************************
Function	 :Reset()
Paramter	 :IN:                                                   
Returnvalue:RC_SUCCESS,RC_FAIL
Description:rereads the config-file,reinitializes the recv. and send
						log-files and clears the receive and the send buffers
*********************************************************************/
static int Reset(void)
{
	int	ilRc = RC_SUCCESS;
	int ilCommGroup = 0;
	char pclSection[XS_BUFF];
	
	dbg(TRACE,"Reset: now resetting ...");
	dbg(TRACE,"Reset: terminating COMM-child(s)");


	dbg(TRACE,"Reset: freeing CFG-Memory.",ilRc);

	FreeCfgMem();
	ListDestroy(prgChildList);

	if ((prgChildList=ListInit(prgChildList,sizeof(CHILD_INFO))) == NULL)
	{
		dbg(TRACE,"Reset : couldn't create internal Child-element(s) list!");
		return RC_FAIL;
	}

	if (*pcgConfFile != '\0')
	{
		if ((ilRc = ReadSections()) == RC_SUCCESS)
		{
			while(GetSection(pclSection) == RC_SUCCESS)
			{
				if ((ilRc = ReadCfg(pclSection,&ilCommGroup)) != RC_SUCCESS)
				{
					dbg(TRACE,"Reset: ReadCfg(%s) failed!",pclSection);
					ilRc = RC_FAIL;
				}
			}
		}
	}
	dbg(TRACE,"Reset: ... finished!");
	return ilRc;
} 
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(BOOL bpSleep)
{
	int ilRc = 0;
	CHILD_INFO *prlChildGroup = NULL;
	LPLISTELEMENT prlChildEle = NULL;

	if (bpSleep == TRUE)
	{
		dbg(TRACE,"Terminate: sleeping 30 sec. before terminating.");
		sleep(30);
	}

	if (igInitOK == TRUE)
	{
		dbg(TRACE,"Terminate: terminating COMM-child(s)");

		prlChildEle = ListFindFirst(prgChildList);
		while(prlChildEle!=NULL)
		{
			prlChildGroup = (CHILD_INFO*)prlChildEle->Data;
			dbg(DEBUG,"Terminate: Stopping section   <%s> ...",prlChildGroup->section_name);
			KillChild(prlChildGroup);
			dbg(DEBUG,"Terminate: terminated section <%s>!",prlChildGroup->section_name);
			prlChildEle = ListFindNext(prgChildList);
		}

		dbg(TRACE,"Terminate: freeing CFG-Memory.",ilRc);
		FreeCfgMem();

		ListDestroy(prgChildList);
		dbg(TRACE,"Terminate: internal data deleted.");

		close_my_cursor(&sgCallsignCursor);
		dbg(TRACE,"Terminate: DB-cursors closed.");
	}
	dbg(TRACE,"Terminate: now leaving ......");
	exit(0);
} 
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
	switch (ipSig)
	{
		case SIGPIPE:
			/*dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGPIPE).",ipSig);*/
			break;
		case SIGTERM:
			dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGTERM).",ipSig);
			Terminate(FALSE);
			break;
		case SIGALRM:
			dbg(DEBUG,"HandleSignal: Received Signal <%d>(SIGALRM)",ipSig);
			bgAlarm = TRUE;
			break;
		case SIGCHLD:
			dbg(TRACE,"HandleSignal: Received Signal <%d>(SIGCHLD)",ipSig);
			Terminate(FALSE);
			break;
		default:
			dbg(TRACE,"HandleSignal: Received Signal <%d>. Terminating !",ipSig);
			Terminate(FALSE);
			break;
	} 
	if (igSigCnt > 100)
	{
		dbg(TRACE,"HandleSignal: Terminating. Received SIGPIPE more than 100 times!");
		Terminate(TRUE);
	}
} 
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr)
	{
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	case	QUE_E_PRIORITY:
		dbg(TRACE,"<%d> : invalid priority was send ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
	return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	dbg(TRACE,"HandleQueues: now entering ...");
	do
	{
		/*memset(prgItem,0x00,igItemLen);*/
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *)prgItem->text;	
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_STANDBY event!");
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_COMING_UP event!");
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_ACTIVE event!");
				dbg(TRACE,"HandleQueues: Terminating for new init ...");
				Terminate(FALSE);
				ilBreakOut = TRUE;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_ACT_TO_SBY event!");
				break;	
			case	HSB_DOWN	:
				/* 	whole system shutdown - do not further use que(), */
				/*	send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_DOWN event!");
				Terminate(FALSE);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				dbg(TRACE,"HandleQueues: received HSB_STANDALONE event!");
				dbg(TRACE,"HandleQueues: Terminating for new init ...");
				Terminate(FALSE);
				ilBreakOut = TRUE;
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				/*HandleRemoteDB(prgEvent);*/
				break;
			case	SHUTDOWN	:
				dbg(TRACE,"HandleQueues: received SHUTDOWN event!");
				Terminate(FALSE);
				break;
			case	RESET		:
				dbg(TRACE,"HandleQueues: received RESET event!");
				ilRc = Reset();
				if (ilRc == RC_FAIL)
				{
					Terminate(FALSE);
				}
				break;
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong HSB-status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		}
		else
		{
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	}while (ilBreakOut == FALSE);

	if(igInitOK == FALSE)
	{
			ilRc = Init_twrhdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"HandleQueues: Init_twrhdl() failed!");
			} /* end of if */
	}/* end of if */
	dbg(TRACE,"HandleQueues: ... now leaving");
} /* end of HandleQueues */
	

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleIntData()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilQBHandle = -1;			/* Return code */
	char *pclSelection = NULL;
	char *pclFields = NULL;
	char *pclData = NULL;
  BC_HEAD *bchd = NULL;		/* Broadcast header		*/
  CMDBLK  *cmdblk = NULL;	/* Command Block 		*/
	TWR_DATA *prlTwrData = NULL;
	TWRD_MESSAGE rlMsgForTower;

	memset(&rlMsgForTower,0x00,sizeof(TWRD_MESSAGE));

  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);

	if (strstr(cmdblk->tw_end,mod_name)==NULL)/* only if I'm not the sender */
	{
		dbg(TRACE,"");
		dbg(TRACE,"=========================================== START === INTERNAL DATA ===");

		dbg(TRACE,"HID: From<%d> Cmd<%s> Table<%s> Twstart<%s> Twend<%s>",
				prgEvent->originator,cmdblk->command,cmdblk->obj_name,cmdblk->tw_start,cmdblk->tw_end);

		/*DebugPrintItem(DEBUG,prgItem);*/
		/*DebugPrintEvent(DEBUG,prgEvent);*/

		pclSelection = cmdblk->data;
		pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
		pclData = (char*)pclFields + strlen(pclFields) + 1;

		dbg(DEBUG,"HID: Selection: <%s>",pclSelection); 
		dbg(DEBUG,"HID: Fields   : <%s>",pclFields); 
		dbg(DEBUG,"HID: Data     : <%s>",pclData);

		/*************************************************/
		/* receiving event from ntisch to check my child */
		/*************************************************/
		if (strcmp(cmdblk->command,"TWR") == 0)
		{
			if ((time(NULL)-tgLastTowerHB) > tgTowerTimeOut)
			{
				dbg(TRACE,"HID: Last heartbeat from tower is longer than <%ld> sec. ago! Restart TWRCOM!",tgTowerTimeOut);	
				if ((ilRc = RunChild("TWRCOM")) == RC_SUCCESS)
				{
					dbg(TRACE,"HID: Start of TWRCOM-child successfull or disabled!");
				}
				else
				{
					dbg(TRACE,"HID: Start of TWRCOM-child failed!");
				}
			}
			else
			{
				dbg(TRACE,"HID: checking heartbeat timer! TWRCOM heartbeat OK!");
			}
		}

		/***********************************************************************/
		/* receiving update/insert from ACTION for sending a new info to tower */
		/***********************************************************************/
		if (strstr(cmdblk->tw_end,mod_name)==NULL)/* only if I'm not the sender */
		{
			TrimNewLine(pclFields);
			if (strncmp(cmdblk->command,"TARR",4)==0 || strncmp(cmdblk->command,"TDEP",4)==0)
			{
				dbg(TRACE,"HID: Received update info from ACTION => informing ATC-Tower!");
				if ((ilRc = FindTowerInfoByCedaData(pclFields,pclData,&prlTwrData,&ilQBHandle)) == RC_SUCCESS)
				{
					dbg(DEBUG,"HID: FindTowerInfoByCedaData() successful.");

					/* JWE 15.01.2002 */
					/*snapit((char*)prlTwrData,sizeof(TWR_DATA)+1,outp);*/

					if ((ilRc = PrepareTowerMsg(prlTwrData,&rlMsgForTower)) == RC_SUCCESS)
					{
						if ((ilRc = SendTowerData(ilQBHandle,&rlMsgForTower)) != RC_SUCCESS)
						{
							dbg(TRACE,"HID: SendTowerData() failed => Did not send info to tower !");
						}
						else
						{
							dbg(DEBUG,"HID: SendTowerData() successfull!");
						}
					}
					else
					{
						dbg(TRACE,"HID: PrepareTowerMsg() failed => Can not send info to tower !");
					}
				}
				else
				{
					dbg(TRACE,"HID: FindTowerInfoByCedaData() failed => No action to perform!");
				}
			}
		}
		dbg(TRACE,"=========================================== END   === INTERNAL DATA ===");
		dbg(TRACE,"");
	}
  return ilRc;
} 
/******************************************************************************/
/* The handle external data routine                                                    */
/******************************************************************************/
static void HandleExtData(QBHandle ipHandle, ITEM *prpItem)
{
	int ilRc = RC_FAIL;
	QBData *prlCommData = NULL;
	CHILD_INFO *prlChildGroup = NULL;
	unsigned long llRecvHBCount = 0;
	int ilSomeInt = 0;
	TWR_DATA rlMsg;
	TWR_DATA rlMsgInList;
	TWRD_MESSAGE rlMsgForTower;

	dbg(TRACE,"");
	dbg(TRACE,"=========================================== START === EXTERNAL DATA ===");
	if ((prlCommData = QBGetMessage(ipHandle,prpItem)) != NULL)
	{
		GetChildByHandle(ipHandle,&prlChildGroup);
		if (prlChildGroup != NULL && prlCommData != NULL)
		{
			switch (prlCommData->type)
			{
				case HEARTBEAT: /* Heartbeat */
					dbg(TRACE,"HED:---- HEARTBEAT-message ----");
					if (QB_GET_ULong(&llRecvHBCount,prlCommData,&ilSomeInt) != -1)
					{
						dbg(DEBUG,"HED: CURR:<%d> RCV:<%d>",prlChildGroup->llHBCount,llRecvHBCount);
						if (prlChildGroup->llHBCount != llRecvHBCount)
						{
							dbg(TRACE,"HED: HB-counter NOT equal! What shall I Do now ?");
							dbg(TRACE,"HED: For now I will sync the two counters!");
							prlChildGroup->llHBCount = llRecvHBCount;
							/* because next HB has increased for one */
							prlChildGroup->llHBCount++;
						}
						else
						{
							prlChildGroup->llHBCount++;
						}
						tgLastTowerHB = time(NULL);
					}
					else
					{
						dbg(TRACE,"HED: Heartbeatcounter: Error on QB_GET_ULong()!");
					}
					break;
				case QB_INT_MSG:  /* internal communication between child and parent */
					dbg(TRACE,"HED:---- INTERNAL-UFIS-message ----");
					break;
				case TOWER_DATA: /* tower communication */
					dbg(TRACE,"HED:---- TOWER-message ----");
					InitMsg(&rlMsg,sizeof(TWR_DATA));

					if ((ilRc=ProcessTwrData(prlCommData->data,&rlMsg)) == RC_SUCCESS)
					{
						dbg(DEBUG,"HED: ProcessTwrData() succesfull!");

						if ((ilRc = CheckTowerDataMsg(&rlMsg,prlChildGroup)) == RC_SUCCESS)
						{
							dbg(DEBUG,"HED: CheckTowerDataMsg() successfull!");
							if (rlMsg.ilInUfisDB == TRUE)
							{
								if ((ilRc = TowerMsgToUfis(&rlMsg,prlChildGroup,prlCommData->data)) == RC_SUCCESS)
								{
									dbg(DEBUG,"HED: TowerMsgToUfis() successfull!");
								}
								/*meaning that the message has been received by twrhdl but is not used */
								else /*inside UFIS AODB */
								{
									dbg(TRACE,"HED: TowerMsgToUfis() failed/not needed!");
								}
								/**************************************************************/
								/* now sending info to tower if it was a new flight for tower */
								/* and the flight information is available inside UFIS DB or  */
								/* if the FPL-key value has been changed and tower needs a new*/
								/* transmission of the information.                           */
								/**************************************************************/
								if (rlMsg.mType != CANCEL) 
								{
									if ((rlMsg.ilNewMessage == 1 && rlMsg.ilInUfisDB==TRUE) || rlMsg.ilKeyChange==TRUE) 
									{
										/* JWE 15.01.2002 */
										/*snapit((char*)&rlMsg,sizeof(TWR_DATA)+1,outp);*/

										if ((ilRc = PrepareTowerMsg(&rlMsg,&rlMsgForTower)) == RC_SUCCESS)
										{
											if ((ilRc = SendTowerData(prlChildGroup->handle,&rlMsgForTower)) != RC_SUCCESS)
											{
												dbg(TRACE,"HED: SendTowerData() failed => Did not send info to tower !");
											}
											else
											{
												dbg(DEBUG,"HED: SendTowerData() successfull!");
											}
										}
										else
										{
											dbg(TRACE,"HED: PrepareTowerMsg() failed => Can not send info to tower !");
										}
									}
									else
									{
										dbg(DEBUG,"HED: Flight already known by tower! No need to send right now!");
									}
								}
								else
								{	
									dbg(TRACE,"HED: NO PrepareTowerMsg() performed due to CANCEL-Message!");
								}
							}
							else /* Send Info telex every time tower sends a message for a flight which is now in */
									 /* the interface communication BUT NOT in the UFIS-DB */
							{
								SendInfoTelex(&rlMsg,prlCommData->data,"FLIGHT NOT FOUND IN UFIS-SYSTEM!","E",prlChildGroup->iSit);
							}
						}
						else
						{
							SendInfoTelex(&rlMsg,prlCommData->data,"FLIGHT NOT FOUND IN UFIS-SYSTEM!","E",prlChildGroup->iSit);
						}
					}
					else
					{
						dbg(TRACE,"HED: ProcessTwrData() failed!");
					}
					break;
				default:
					dbg(TRACE,"HED: unknown type <%d> received ! Ignoring!");
					break;
			}
		}
		/*****************/
		/* don't forget !*/
		/*****************/
		free(prlCommData);
	}
	else
	{
		dbg(TRACE,"HED: QBGetMessage() returns NULL! Invalid data pointer!");
	}
	dbg(TRACE,"=========================================== END   === EXTERNAL DATA ===");
	dbg(TRACE,"");
}
/* ********************************************************************/
/* The InitMsg() routine																					 */
/* ********************************************************************/
static void InitMsg(TWR_DATA *prpMsg,int ipBytes)
{
	memset(prpMsg,0x00,ipBytes);
	prpMsg->dof.year = -1;
	prpMsg->dof.month = -1;
	prpMsg->dof.day = -1;

	prpMsg->aobd.year = -1;
	prpMsg->aobd.month = -1;
	prpMsg->aobd.day = -1;
	prpMsg->aobt.hour = -1;
	prpMsg->aobt.min = -1;

	prpMsg->firstC.hour = -1;
	prpMsg->firstC.min = -1;

	prpMsg->lastC.hour = -1;
	prpMsg->lastC.min = -1;

	prpMsg->eta.hour = -1;
	prpMsg->eta.min = -1;

	prpMsg->ata.hour = -1;
	prpMsg->ata.min = -1;

	prpMsg->divTime.hour = -1;
	prpMsg->divTime.min = -1;

	/*prpMsg->edt.hour = -1;
	prpMsg->edt.min = -1;*/
	prpMsg->eobt.hour = -1;
	prpMsg->eobt.min = -1;

	prpMsg->ctot.hour = -1;
	prpMsg->ctot.min = -1;

	prpMsg->atd.hour = -1;
	prpMsg->atd.min = -1;

	prpMsg->ilNewMessage = -1;
	prpMsg->ilKeyChange = -1;
	prpMsg->ilInUfisDB = -1;
}
/* ********************************************************************/
/* The TrimNewLine() routine																					 */
/* ********************************************************************/
static void TrimNewLine(char *pcpString)
{
	char *pclPointer = NULL;
	if ((pclPointer=strchr(pcpString,'\n')) != NULL)
	{
		*pclPointer = '\0';
	}
}
/* ********************************************************************/
/* The ReadCfg() routine																					 */
/* ********************************************************************/
static int ReadCfg(char *pcpSection, int *ipGroup)
{
	int ilRc = RC_SUCCESS;
	CHILD_INFO rlChildGroup;
	char pclTmpBuf[L_BUFF];

	memset(&rlChildGroup,0x00,sizeof(CHILD_INFO));

	dbg(TRACE,"--------------------------------");
	dbg(TRACE,"ReadCfg: file <%s> Section <%s>",pcgConfFile,pcpSection);
	dbg(TRACE,"--------------------------------");

	/* need to init handle because QBClose can not handle 0 value because 0 is already a valid handle */
	/* but if the handle of 0 is not initialised we get a core file */
	/* QBClose is called inside RunChild() */
	rlChildGroup.handle = -1;

	strncpy(rlChildGroup.section_name,pcpSection,XXS_BUFF);
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"STARTUP",CFG_STRING,&rlChildGroup.startup,CFG_ALPHA,"NO"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"BINARY_NAME",CFG_STRING,&rlChildGroup.binary_name,CFG_ALPHA,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"CMD_LINE",CFG_STRING,&rlChildGroup.cmd_line,CFG_PRINT,""))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"NATLIB_LOG",CFG_STRING,&rlChildGroup.natlib_log,CFG_NUM,"0"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
		else
		{
			if (strncmp(rlChildGroup.natlib_log,"1",1) == 0)
			{
				memset(pclTmpBuf,0x00,L_BUFF);
				sprintf(pclTmpBuf,"%s/natlibrary%d.log",getenv("DBG_PATH"),getpid());
				NatSetLog(NAT_LLIBERR|NAT_LERR|NAT_LTRC|NAT_LFLUSH,pclTmpBuf,NULL);
			}
		}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"HOMEAP",CFG_STRING,&rlChildGroup.homeap,CFG_ALPHA," "))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"TIME_ACCURACY",CFG_STRING,&rlChildGroup.timeaccuracy,CFG_NUM,"43600"))
			!= RC_SUCCESS)
		{igTimeAccuracy=43600;}
		else
		{igTimeAccuracy=atoi(rlChildGroup.timeaccuracy);}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"TIMEOUT",CFG_STRING,&rlChildGroup.timeout,CFG_NUM,"30000"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
		else
		{
			tgTowerTimeOut = ((atol(rlChildGroup.timeout))/1000);
		} 

	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"WRITE_TO_DB",CFG_STRING,&rlChildGroup.write_to_db,CFG_ALPHA,"NO"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"UFIS_RCV_MODID",CFG_STRING,&rlChildGroup.ufis_rcv_modid,CFG_NUM,"0"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
			else
			{ rlChildGroup.ilRcvModId = atoi(rlChildGroup.ufis_rcv_modid);	}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"SND_MODID",CFG_STRING,&rlChildGroup.Send_modid,CFG_NUM,"7495"))
			!= RC_SUCCESS)
		{return RC_FAIL;}
			else
			{ rlChildGroup.sndmod_id = atoi(rlChildGroup.Send_modid);	}
	if ((ilRc=GetCfgEntry(pcgConfFile,pcpSection,"SEND_INFO_TELEX",CFG_STRING,&rlChildGroup.send_info_telex,CFG_NUM,"0"))
			== RC_SUCCESS)
			{ rlChildGroup.iSit = atoi(rlChildGroup.send_info_telex);	}
			else
			{ rlChildGroup.iSit = 0;}
	dbg(TRACE,"--------------------------------");

	if (strcmp(pcpSection,"TWRCOM") == 0)
	{
		dbg(TRACE,"ReadCfg: initalise internal data list with TWR_DATA-type structure!");
		/* initializing flightinfo list of group */
		if ((rlChildGroup.prlDataList=ListInit(rlChildGroup.prlDataList,sizeof(TWR_DATA))) == NULL)
		{
			dbg(TRACE,"ReadCfg: : couldn't create internal data-list!");
			return RC_FAIL;
		}
	}
	/* appending group to global list of groups */
	if (ListAppend(prgChildList,&rlChildGroup) == NULL)
	{
		dbg(TRACE,"ReadCfg: ListAppend failed!");
		Terminate(FALSE);
	}
	return ilRc;
}
/* *********************************************************************/
/* The GetCfgEntry() routine   																				 */
/* *********************************************************************/
static int GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,
												char **pcpDest,int ipValueType,char *pcpDefVal)
{
	int ilRc = RC_SUCCESS;
	int ilLen = 0;
	char pclCfgLineBuffer[L_BUFF];
	
	memset(pclCfgLineBuffer,0x00,L_BUFF);
	if ((ilRc=iGetConfigRow(pcpFile,pcpSection,pcpEntry,spType,
													pclCfgLineBuffer)) != RC_SUCCESS)
	{
		dbg(TRACE,"GetCfgEntry: reading entry <%s> failed.",pcpEntry);
	}
	else
	{
		if (strlen(pclCfgLineBuffer) < 1)
		{
			dbg(TRACE,"GetCfgEntry: EMPTY %s! Use default <%s>.",pcpEntry,pcpDefVal);
			strcpy(pclCfgLineBuffer,pcpDefVal);
		}
		ilLen = strlen(pclCfgLineBuffer)+4; 
		*pcpDest = malloc(ilLen);
		/*memset(pcpDest,0x00,ilLen);*/
		strcpy(*pcpDest,pclCfgLineBuffer);
		dbg(TRACE,"GetCfgEntry: %s = <%s>",pcpEntry,*pcpDest);
		if ((ilRc = CheckValue(pcpEntry,*pcpDest,ipValueType)) != RC_SUCCESS)
		{
			dbg(TRACE,"GetCfgEntry: please correct value <%s>!",pcpEntry); 	
		}
	}
	return ilRc;
}
/* ******************************************************************** */
/* The CheckValue() routine																						*/
/* ******************************************************************** */
static int CheckValue(char *pcpEntry,char *pcpValue,int ipType)
{
	int ilRc = RC_SUCCESS;

	switch(ipType)
	{
		case CFG_NUM:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isdigit(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : NOT A NUMBER! <%s>!"
						,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			}
			break;
		case CFG_ALPHA:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isalpha(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
						,*pcpValue,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			} 
			break;
		case CFG_ALPHANUM:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isalnum(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
						,*pcpValue,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			}
			break;
		case CFG_PRINT:
			while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
			{
				if (isprint(*pcpValue)==0)
				{
					dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!"
						,*pcpValue,pcpEntry); 
					ilRc = RC_FAIL;
				}
				pcpValue++;
			}
			break;
		default:
			break;
	}
	return ilRc;
}
/* ******************************************************************** */
/* The FreeCfgMem() routine	  	                                    */
/* ******************************************************************** */
static void FreeCfgMem(void)
{
	LPLISTELEMENT prlChildEle = NULL;
	CHILD_INFO *prlChildGroup = NULL;

	prlChildEle = ListFindFirst(prgChildList);
	while(prlChildEle!=NULL)
	{
		prlChildGroup = (CHILD_INFO*)prlChildEle->Data;
		free(prlChildGroup->timeout);  /* time for restart */
		prlChildEle = ListFindNext(prgChildList);
	}
}
/* ******************************************************************** */
/* Following the poll_q function				*/
/* Waits for input on the socket and polls the QCP for messages*/
/* ******************************************************************** */
static int poll_q() 
{
  int ilRc;
  QBHandle ilRc_QB = 0;
	char pclUrno[11];

  do
	{
		/*---------------------------*/
		/* now looking on ceda-queue */
		/*---------------------------*/
		ilRc = RC_NOT_FOUND;
		if (ilRc == RC_NOT_FOUND)
    {
      while ((ilRc=que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen
											,(char *) &prgItem)) == RC_SUCCESS)
			{
				/* depending on the size of the received item  */
				/* a realloc could be made by the que function */
				/* so do never forget to set event pointer !!! */
				prgEvent = (EVENT *) prgItem->text;

				/* Acknowledge the item */
				ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
				if( ilRc != RC_SUCCESS ) 
				{
					/* handle que_ack error */
					HandleQueErr(ilRc);
				}

				/* This means the item is from one of my COMM-childs */
				if ((ilRc_QB = QBSelect(prgItem)) >= 0)
				{
					/*dbg(DEBUG,"MAIN: received event from COMM-child for handle=<%d>!",ilRc_QB);*/
					HandleExtData(ilRc_QB,prgItem);
				}
				else
				{
					/*dbg(DEBUG,"MAIN: QBSelect returned handle=<%d>",ilRc_QB);*/
					switch(prgEvent->command)
					{
					case	HSB_STANDBY	:
						ctrl_sta = prgEvent->command;
						dbg(TRACE,"PQ: received HSB_STANDBY event!");
						HandleQueues();
						break;	
					case	HSB_COMING_UP	:
						ctrl_sta = prgEvent->command;
						dbg(TRACE,"PQ: received HSB_COMING_UP event!");
						HandleQueues();
						break;	
					case	HSB_ACTIVE	:
						ctrl_sta = prgEvent->command;
						dbg(TRACE,"PQ: received HSB_ACTIVE event!");
						break;	
					case	HSB_ACT_TO_SBY	:
						ctrl_sta = prgEvent->command;
						dbg(TRACE,"PQ: received HSB_ACT_TO_SBY event!");
						HandleQueues();
						break;	
					case	HSB_DOWN	:
						/* 	whole system shutdown - do not further use que(), */
						/*	send_message() or timsch() ! */
						ctrl_sta = prgEvent->command;
						dbg(TRACE,"PQ: received HSB_DOWN event!");
						Terminate(FALSE);
						break;	
					case	HSB_STANDALONE	:
						ctrl_sta = prgEvent->command;
						dbg(TRACE,"PQ: received HSB_STANDALONE event!");
						/*ResetDBCounter();*/
						break;	
					case	REMOTE_DB :
						/* ctrl_sta is checked inside */
						/*HandleRemoteDB(prgEvent);*/
						break;
					case	SHUTDOWN	:
						/* process shutdown - maybe from uutil */
						dbg(TRACE,"PQ: received SHUTDOWN event!");
						Terminate(FALSE);
						break;
					case	RESET		:
						dbg(TRACE,"PQ: received RESET event!");
						ilRc = Reset();
						if (ilRc == RC_FAIL)
						{
							Terminate(FALSE);
						}
						break;
					case	EVENT_DATA	:
						if((ctrl_sta == HSB_STANDALONE) ||
							(ctrl_sta == HSB_ACTIVE) ||
							(ctrl_sta == HSB_ACT_TO_SBY))
						{
							ilRc = HandleIntData();
							if(ilRc!=RC_SUCCESS)
							{
								dbg(TRACE,"PQ: HandleIntData failed <%d>",ilRc);
								HandleErr(ilRc);
							}
						}
						else
						{
							dbg(TRACE,"poll_q: wrong HSB-status <%d>",ctrl_sta);
							DebugPrintItem(TRACE,prgItem);
							DebugPrintEvent(TRACE,prgEvent);
						}/* end of if */
						break;
					case	TRACE_ON :
						dbg_handle_debug(prgEvent->command);
						break;
					case	TRACE_OFF :
						dbg_handle_debug(prgEvent->command);
						break;
					case 999:
						break;
					default			:
						dbg(TRACE,"MAIN: unknown event");
						DebugPrintItem(TRACE,prgItem);
						DebugPrintEvent(TRACE,prgEvent);
						break;
					} /* end switch */

				}
      } /* end while */ 
    }

		/* stay in loop */
		ilRc = RC_FAIL;

  }while (ilRc!=RC_SUCCESS);
	return ilRc;
} 
/* **************************************************************** */
/* The snapit routine                                           */
/* snaps data if the debug-level is set to DEBUG                */
/* **************************************************************** */
static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile)
{
	if (debug_level==DEBUG)
	{
		snap((char*)pcpBuffer,(long)lpDataLen,(FILE*)pcpDbgFile);
	}
}
/*********************************************************************
Function	 :CleanMessage(char *pcpMsg,int ipMsgLen)
Paramter	 :IN:pcpMsg = Message received directly after the socket read.
						IN:ipMsgLen=length of the message
Returnvalue:none
Description:Replaces unallowed bytes from the original telegram
						(especially '\0' and '_') with "allowed" blanks
*********************************************************************/
static void CleanMessage(char *pcpMsg,int ipMsgLen,char cpChar, char cpRepl)
{
	int ilCnt = 0;
	while(ilCnt < ipMsgLen)
	{
		if (pcpMsg[ilCnt]==cpChar)
		{
			pcpMsg[ilCnt]=cpRepl;
		}
		ilCnt++;
	}
}
/* ******************************************************************** */
/* The FormatDate() routine                                         */
/* ******************************************************************** */
static int FormatDate(char *pcpOldDate,char *pcpFormat,char *pcpNewDate)
{
	int ilRc = RC_SUCCESS;
	int ilCfgChar = 0;
	int ilRightChar = 0;
	int ilDateChar = 0;
	char pclOldDate[20];
	char pclValidLetters[] = "YMDHIS";
	char pclRightFormat[20];
	char *pclTmpDate = NULL;
	
	TrimRight(pcpOldDate);
	memset(pclOldDate,0x00,sizeof(pclOldDate));
	memset(pclRightFormat,0x00,sizeof(pclRightFormat));
	*pcpNewDate = 0x00;
	
	if (strlen(pcpOldDate) > 14)
	{
		return RC_FAIL;	
	}else
	{
		strcpy(pclOldDate,pcpOldDate);
		TrimRight(pclOldDate);
	}

	/* adding '0' until the length of the date is 14byte */ 
	if (strlen(pclOldDate) < 14)
	{
		while(strlen(pclOldDate) < 14)
		{
			pclOldDate[strlen(pclOldDate)] = '0';
			pclOldDate[strlen(pclOldDate)+1] = 0x00;
		}
	}
	
	/* removing all unallowed letters from pcpFormat-string */
	ilCfgChar=0;
	while(ilCfgChar < (int)strlen(pcpFormat))
	{
		if (strchr(pclValidLetters,pcpFormat[ilCfgChar]) != NULL)
		{
			pclRightFormat[ilRightChar] = pcpFormat[ilCfgChar];
			ilRightChar++;
		}
		ilCfgChar++;
	}

	/* now formatting CEDA-time format from pclOldDate to right format */	
	if ((pclTmpDate =
			GetPartOfTimeStamp(pclOldDate,pclRightFormat)) != NULL)
	{
		/* now changing the layout like it is in the cfg-file */	
		ilCfgChar = 0;
		ilRightChar = 0;
		ilDateChar = 0;
		while(ilCfgChar < (int)strlen(pcpFormat))
		{
			if (strchr(pclValidLetters,pcpFormat[ilCfgChar]) != NULL)
			{
				(pcpNewDate)[ilDateChar] = pclTmpDate[ilRightChar];
				ilRightChar++;
				ilDateChar++;
			}
			else
			{
				(pcpNewDate)[ilDateChar] = pcpFormat[ilCfgChar];
				ilDateChar++;
			}
			ilCfgChar++;
		}
		(pcpNewDate)[strlen(pcpFormat)] = 0x00;
	}
	else
	{
		ilRc = RC_FAIL;
	}
	/*dbg(DEBUG,"FormatDate: Old=<%s> New=<%s>",pclOldDate,pcpNewDate);*/
	return ilRc;	
}
/* ******************************************************************** */
/* The TrimRight() routine																						*/
/* ******************************************************************** */
static void TrimRight(char *pcpBuffer)
{
    int i = 0;
    for (i = strlen(pcpBuffer); i > 0 && isspace(pcpBuffer[i-1]); i--)  
        ;
    pcpBuffer[i] = '\0';    
}
/* ******************************************************************** */
/* The StrToTime() routine																	*/
/* Format of Date has to be CEDA-Format - 14 Byte													*/
/* ******************************************************************** */
static int StrToTime(char *pcpTime,time_t *plpTime)
{
	struct tm *_tm;
	time_t now;
	char  _tmpc[6];

	/*dbg(DEBUG,"StrToTime, <%s>",pcpTime);*/
	if (strlen(pcpTime) < 12 )
	{
		*plpTime = time(0L);
		return RC_FAIL;
	} 

	now = time(0L);
	_tm = (struct tm *)localtime(&now);
	_tmpc[2] = '\0';
	_tm -> tm_sec = 0;
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm -> tm_year = atoi(_tmpc)-1900;
	_tm -> tm_wday = 0;
	_tm -> tm_yday = 0;
	now = mktime(_tm);
	/*dbg(DEBUG,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",
	_tm->tm_mday,_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);*/
	if (now != (time_t) -1)
	{
		*plpTime = now;
		return RC_SUCCESS;
	}
	*plpTime = time(NULL);
	return RC_FAIL;
}
/* *********************************************************************/
/* The ReadSections() routine                                          */
/* *********************************************************************/
static int ReadSections()
{
	int   ilRc = RC_SUCCESS;

	/* reading [MAIN] section from cfg-file */
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","SECTIONS",CFG_STRING,&pcgSections,CFG_PRINT,""))
			== RC_SUCCESS)
	{
		pcgSectionPointer = pcgSections;
		/*igSections = GetNoOfElements(pcgSections,',');*/
  }

	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","DEBUG_LEVEL",CFG_STRING,&pcgDebugLevel,CFG_NUM,"0"))
		== RC_SUCCESS)
	{
		if (atoi(pcgDebugLevel)==0)
		{
			dbg(TRACE,"GetCfgEntry: debug info = OFF after init-phase!");
			igDebugLevel = 0;
		}
		else
		{
			if (atoi(pcgDebugLevel)==1)
			{
				dbg(TRACE,"GetCfgEntry: debug info = TRACE after init-phase!");
				igDebugLevel = TRACE;
			}
			else
			{
				if (atoi(pcgDebugLevel)==2)
				{
					dbg(TRACE,"GetCfgEntry: debug info = DEBUG after init-phase!");
					igDebugLevel = DEBUG;
				}
				else
				{
					dbg(TRACE,"GetCfgEntry: debug info = OFF after init-phase!");
					igDebugLevel = 0;
				}
			}
		}
	}
	else
	{
		dbg(TRACE,"GetCfgEntry: debug info = OFF after init-phase!");
		igDebugLevel = 0;
	}
	if ((ilRc=GetCfgEntry(pcgConfFile,"MAIN","TLX_TOKEN",CFG_STRING,&pcgTmpToken,CFG_PRINT,""))
			== RC_SUCCESS)
	{
		memset(pcgTlxToken,0x00,XS_BUFF);
		strncpy(pcgTlxToken,pcgTmpToken,64);
		dbg(DEBUG,"GetCfgEntry: <%s>",pcgTlxToken);
		free(pcgTmpToken);
	}
	return ilRc;
}
/* ******************************************************************** */
/* The GetSection() routine                                             */
/* ******************************************************************** */
static int GetSection(char *pcpSection)
{
	int ilRc = RC_SUCCESS;

	memset((char*)pcpSection,0x00,XS_BUFF);
	GetNextDataItem(pcpSection, &pcgSectionPointer, ",", "\0", "  ");
	if (pcpSection[0] == 0x00)
	{
		ilRc = RC_FAIL;
	}
	else
	{
		dbg(DEBUG,"GetSection : Section Nr.<%d>=<%s>",pcpSection);
	}
	return ilRc;
}
/* ******************************************************************** */
/* The TrimAll() routine                                            */
/* ******************************************************************** */
static void TrimAll(char *pcpBuffer)
{
	const char *p = pcpBuffer;
	int i = 0;

	for (i = strlen(pcpBuffer); i > 0 && isspace(pcpBuffer[i-1]); i--);
			pcpBuffer[i] = '\0';
	for ( *p ; isspace(*p); p++);
	while ((*pcpBuffer++ = *p++) != '\0');
}
/* ******************************************************************** */
/* The TimeToStr() routine																	*/
/* ******************************************************************** */
static void TimeToStr(char *pcpTime,time_t lpTime,int ipType,int ipLocal)
{
	char pclTime[20];
	struct tm *_tm;
	struct tm rlTm;

	*pclTime = 0x00;
	*pcpTime = 0x00;
	if (lpTime == 0)
	{
		lpTime = time(NULL);
	}
	lpTime = lpTime;
	switch (ipLocal)
	{
		case 0:
			_tm = (struct tm *) localtime(&lpTime);
			break;
		case 1:
			_tm = (struct tm *) gmtime(&lpTime);
			break;
	}
	rlTm = *_tm;
	switch(ipType)
	{
		case 0: /* Returns "now" in CEDA-format YYYYMMDDHHMMSS */
			/* Unusal format because of sccs !! */
			strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
			/*dbg (DEBUG,"NOW-TimeToStr : return <%s>",pcpTime);*/
			break;
		case 1:/* Returns lpTime in CEDA-format YYYYMMDDHHMMSS */
			/* Unusal format because of sccs !! */
			strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
			/*dbg(DEBUG,"CEDA-TimeToStr: return <%s>",pcpTime);*/
			break;
		default:
			dbg(TRACE,"TimeToStr : CEDA-ERROR: unknown time-type received!");
			break;
	}
}
/* ******************************************************************** */
/* The UtcToLocal(char *pcpTime) routine 				*/
/* ******************************************************************** */
static int UtcToLocal(char *oldstr, char *newstr) 
{
  int c;
  char year[5], month[3], day[3], hour[3], minute[3],second[3];
  struct tm TimeBuffer, *final_result;
  time_t time_result;
	/*dbg(DEBUG,"UtcToLocal: IN <%s>",oldstr);*/

	/********** Extract the Year off CEDA timestamp **********/
  for(c=0; c<= 3; ++c)
    {
      year[c] = oldstr[c];
    }
  year[4] = '\0';
	/********** Extract month, day, hour and minute off CEDA timestamp **********/
  for(c=0; c <= 1; ++c)
    {
      month[c]  = oldstr[c + 4];
      day[c]    = oldstr[c + 6];
      hour[c]   = oldstr[c + 8];
      minute[c] = oldstr[c + 10];
      second[c] = oldstr[c + 12];
    }
	/********** Terminate the Buffer strings **********/
  month[2]  = '\0';
  day[2]    = '\0';
  hour[2]   = '\0';
  minute[2] = '\0';
  second[2] = '\0';


	/***** Fill a broken-down time structure incl. string to integer *****/
  TimeBuffer.tm_year  = atoi(year) - 1900;
  TimeBuffer.tm_mon   = atoi(month) - 1;
  TimeBuffer.tm_mday  = atoi(day);
  TimeBuffer.tm_hour  = atoi(hour);
  TimeBuffer.tm_min   = atoi(minute);
  TimeBuffer.tm_sec   = atoi(second);
  TimeBuffer.tm_isdst = 0;
	/***** Make secondbased timeformat and correct mktime *****/
  time_result = mktime(&TimeBuffer) - timezone;
	/***** Reconvert into broken-down time structure *****/
  final_result = localtime(&time_result);

  sprintf(newstr,"%d%.2d%.2d%.2d%.2d%.2d"
		,final_result->tm_year+1900
		,final_result->tm_mon+1
		,final_result->tm_mday
		,final_result->tm_hour
		,final_result->tm_min
		,final_result->tm_sec);

/*dbg(DEBUG,"UtcToLocal: OUT<%s>",newstr);*/
  return(0); /**** DONE WELL ****/
}
/* ******************************************************************** */
/* The FindFlightByTowerData-function 				*/
/* ******************************************************************** */
static int FindFlightByTowerData(CHILD_INFO *prpChildGroup,TWR_DATA *prpNewMsg)
{
int ilRc = RC_FAIL;
int ilRcDb = RC_SUCCESS;
char pclErrMsg[L_BUFF];
char pclSqlBuf[L_BUFF];
char pclTmpSqlAnswer[XL_BUFF];
char pclUrno[XXS_BUFF];
char pclRegn[XXS_BUFF];
char pclPsta[XXS_BUFF];
char pclOnbl[XXS_BUFF];
char pclPstd[XXS_BUFF];
char pclOfbl[XXS_BUFF];
char pclRem1[S_BUFF];
char pclStoa[XXS_BUFF];
char pclStod[XXS_BUFF];
char pclDate[XXS_BUFF];
char pclAdid[XXS_BUFF];
char pclAirb[XXS_BUFF];
char pclTimeField[XXS_BUFF];
short slFkt = START;
short slCursor = 0;
char *pclFieldList="URNO,REGN,PSTA,ONBL,PSTD,OFBL,REM1,STOA,STOD,AIRB";
int ilHit;

  memset(pclUrno,0x00,XXS_BUFF);
  memset(pclRegn,0x00,XXS_BUFF);
  memset(pclPsta,0x00,XXS_BUFF);
  memset(pclOnbl,0x00,XXS_BUFF);
  memset(pclPstd,0x00,XXS_BUFF);
  memset(pclOfbl,0x00,XXS_BUFF);
  memset(pclRem1,0x00,S_BUFF);
  memset(pclStoa,0x00,XXS_BUFF);
  memset(pclStod,0x00,XXS_BUFF);
  memset(pclTimeField,0x00,XXS_BUFF);
  memset(pclAdid,0x00,XXS_BUFF);
  memset(pclAirb,0x00,XXS_BUFF);
  memset(pclDate,0x00,XXS_BUFF);
  memset(pclSqlBuf,0x00,L_BUFF);
  memset(pclTmpSqlAnswer,0x00,XL_BUFF);
	
  if (strcmp(prpNewMsg->ades,"LPPT") == 0)
  {
     dbg(DEBUG,"FFBTD: Look for arrival flight");
     /* JWE 12.04.2006: Looks wrong at first sight, but since the DOF value (date of flight) */ 
     /*                 from ATC nowadays means the date of departure at origin, we as well use STOD */
     /*                 now also for searching the proper arrival flight instead of using STOA as before. */
     /*                 This of course means, that the standard flight times for all airports should be */
     /*                 filled in, since otherwise the STOD for the arrival could not be filled */
     /*                 automatically by the UFIS system. */
     strcpy(pclTimeField,"STOD");
     strcpy(prpNewMsg->pcladid,"A");
  }
  else
  {
     if (strcmp(prpNewMsg->adep,"LPPT") == 0)
     {
        dbg(DEBUG,"FFBTD: Look for departure flight");
        strcpy(pclTimeField,"STOD");
        strcpy(prpNewMsg->pcladid,"D");
     }
  }

  if (strlen(pclTimeField)>0)
  {
     strcpy(pclDate,(char*)(TtUd(&prpNewMsg->dof)));
     pclDate[8] = 0x00;
     sprintf(pclSqlBuf,
        "SELECT %s FROM AFT%s WHERE %s LIKE '%s%s' AND ADID='%s' AND CSGN='%s' AND FTYP NOT IN ('N','X')",
        pclFieldList,pcgTabEnd,pclTimeField,pclDate,"%",prpNewMsg->pcladid,prpNewMsg->cs);
     dbg(DEBUG,"FFBTD: SQL<%s>",pclSqlBuf);
     ilHit = 0;
     slCursor = 0;
     slFkt = START;
     ilRcDb = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer);
     while (ilRcDb == RC_SUCCESS)
     {
        ilHit ++;
        dbg(DEBUG,"FFBTD: Found FLIGHT <%s> in DB!",prpNewMsg->cs);
        BuildItemBuffer(pclTmpSqlAnswer,pclFieldList,0,",");
        GetDataItem(pclUrno,pclTmpSqlAnswer,1,',',"","");
        dbg(DEBUG,"FFBTD: URNO = <%s>",pclUrno);
        prpNewMsg->llurno = atol(pclUrno);
        GetDataItem(pclRegn,pclTmpSqlAnswer,2,',',"","");
        dbg(DEBUG,"FFBTD: REGN = <%s>",pclRegn);
        strncpy(prpNewMsg->pclregn,pclRegn,12);
        GetDataItem(pclPsta,pclTmpSqlAnswer,3,',',"","");
        dbg(DEBUG,"FFBTD: PSTA = <%s>",pclPsta);
        strncpy(prpNewMsg->pclpsta,pclPsta,6);
        GetDataItem(pclOnbl,pclTmpSqlAnswer,4,',',"","");
        dbg(DEBUG,"FFBTD: ONBL = <%s>",pclOnbl);
        strncpy(prpNewMsg->pclonbl,pclOnbl,14);
        GetDataItem(pclPstd,pclTmpSqlAnswer,5,',',"","");
        dbg(DEBUG,"FFBTD: PSTD = <%s>",pclPstd);
        strncpy(prpNewMsg->pclpstd,pclPstd,6);
        GetDataItem(pclOfbl,pclTmpSqlAnswer,6,',',"","");
        dbg(DEBUG,"FFBTD: OFBL = <%s>",pclOfbl);
        strncpy(prpNewMsg->pclofbl,pclOfbl,14);
        /* current comment on flight! Needed for TG-message */
        GetDataItem(pclRem1,pclTmpSqlAnswer,7,',',"","");
        dbg(DEBUG,"FFBTD: REM1 = <%s>",pclRem1);
        TrimAll(pclRem1);
        strcpy(prpNewMsg->pclrem1,pclRem1);
        GetDataItem(pclStoa,pclTmpSqlAnswer,8,',',"","");
        dbg(DEBUG,"FFBTD: STOA = <%s>",pclStoa);
        TrimAll(pclStoa);
        strncpy(prpNewMsg->pclstoa,pclStoa,14);
        GetDataItem(pclStod,pclTmpSqlAnswer,9,',',"","");
        dbg(DEBUG,"FFBTD: STOD = <%s>",pclStod);
        TrimAll(pclStod);
        strncpy(prpNewMsg->pclstod,pclStod,14);
        GetDataItem(pclAirb,pclTmpSqlAnswer,10,',',"","");
        dbg(DEBUG,"FFBTD: AIRB = <%s>",pclAirb);
        TrimAll(pclAirb);
        if (strcmp(prpNewMsg->pcladid,"D") == 0 &&
            (*pclAirb != ' ' && *pclAirb != '\0'))
        {
           slFkt = NEXT;
           ilRcDb = sql_if(slFkt,&slCursor,pclSqlBuf,pclTmpSqlAnswer);
        }
        else
           ilRcDb = SQL_NOTFOUND;
     }
     close_my_cursor(&slCursor);
     if (ilHit > 0)
        ilRc = RC_SUCCESS;
     else
     {
        ilRc = ilRcDb;
        if (ilRc!=SQL_NOTFOUND)
        {
           get_ora_err(ilRc,pclErrMsg);
           dbg(TRACE,"FFBTD: ORACLE returns <%s>",pclErrMsg);
        }
     }
  }
  return ilRc;
}
/* ******************************************************************** */
/* The RunChild()-function 				*/
/* ******************************************************************** */
static int RunChild(char *pcpChild)
{
	int ilRc = RC_FAIL;
	int ilRc_system = RC_FAIL;
	QBHandle ilPipe = -1;
	QBCommCtl rlCtl;
	CHILD_INFO *prlChildGroup = NULL;
	LPLISTELEMENT prlChildEle = NULL;
	char pclCopy[S_BUFF];

	dbg(DEBUG,"RunChild: Request to run <%s>-child!",pcpChild);
	prlChildEle = ListFindFirst(prgChildList);
	while(prlChildEle!=NULL)
	{
		prlChildGroup = (CHILD_INFO*)prlChildEle->Data;
		if (strcmp(prlChildGroup->section_name,pcpChild) == 0)
		{
			if (strncmp(prlChildGroup->startup,"YES",3)==0)
			{
				dbg(DEBUG,"RunChild: Found child <%s> in list of sections.",prlChildGroup->section_name);
				prlChildGroup->llHBCount = 0;
				if (prlChildGroup->handle >= 0)
				{
					if (QBClose(prlChildGroup->handle)==-1 && NatErrNo()==NAT_E_BADSTAT)
					{
						dbg(TRACE,"RunChild: QBClose() failed! Terminating due to AMOK-child!");
						Terminate(TRUE);
					}
				}

				dbg(TRACE,"RunChild: Now copy child <%s> from <%s> to <%s>"
					,prlChildGroup->binary_name,getenv("BIN_PATH"),getenv("RUN_PATH"));

				sprintf(pclCopy,"cp %s/%s %s",getenv("BIN_PATH"),prlChildGroup->binary_name,getenv("RUN_PATH"));
				dbg(TRACE,"RunChild: Executing: system(%s)",pclCopy);
				ilRc_system = system(pclCopy);
				if (ilRc_system != 0)
				{
					dbg(TRACE,"RunChild: system()      returns <%d>!",ilRc_system);
					dbg(TRACE,"RunChild: WEXITSTATUS() returns <%d>!",WEXITSTATUS(ilRc_system));
					dbg(TRACE,"RunChild: possible failure at copy of child <%s>!",prlChildGroup->binary_name);
				}
				else
				{
					dbg(TRACE,"RunChild: system()-shell exit state=<%d>. Copy successfull!",ilRc_system);
				}

				memset(&rlCtl,0x00,sizeof(QBCommCtl));
				rlCtl.type = 0;
				rlCtl.heartbeat = atol(prlChildGroup->timeout); /* milli-seconds */
				rlCtl.ack_policy = QB_SIGNONE;
				rlCtl.inque_key = QB_QCP_INPUT; 
				rlCtl.ackque_key = QB_QCP_ATTACH;
				rlCtl.rcvmod_id = mod_id;
				rlCtl.sndmod_id = prlChildGroup->sndmod_id;
				strncpy(rlCtl.dest_name,prlChildGroup->section_name,9);
				strncpy(rlCtl.recv_name,mod_name,9);

				dbg(DEBUG,"RunChild: Section <%s> CMD_LINE <%s>",prlChildGroup->section_name,prlChildGroup->cmd_line);
				if ((ilPipe = QBOpenCL(prlChildGroup->cmd_line,&rlCtl,QB_SIGNONE)) < 0)
				{
					dbg(TRACE,"RunChild: QBOpenCL() failed! Handle<%d> -> NatErrno=<%d>=><%s>",ilPipe,NatErrNo(),NatErrText());	
					ilRc = RC_FAIL;
				}
				else
				{
					dbg(DEBUG,"RunChild: QBOpenCL() successfull! Pipe,QBHandle=<%d>",ilPipe);	
					prlChildGroup->handle = ilPipe;
					ilRc = RC_SUCCESS;
				}
				prlChildEle = NULL;
			}
			else
			{
				dbg(TRACE,"RunChild: startup of <%s> is DISABLED! Set STARTUP = YES in %s.cfg to enable!"
					,prlChildGroup->section_name,mod_name);
				prlChildEle = NULL;
				ilRc = RC_SUCCESS;
			}
		}
		else
		{
			prlChildEle = ListFindNext(prgChildList);
		}
	}
	return ilRc;
}
/* ******************************************************************** */
/* The GetChildByHandle()function */
/* ******************************************************************** */
static void GetChildByHandle(int ipHandle, CHILD_INFO **prpChild)
{
	CHILD_INFO *prlChildGroup = NULL;
	LPLISTELEMENT prlChildEle = NULL;

	prlChildEle = ListFindFirst(prgChildList);
	while(prlChildEle!=NULL)
	{
		prlChildGroup = (CHILD_INFO*)prlChildEle->Data;
		if (ipHandle == prlChildGroup->handle)
		{
			dbg(DEBUG,"GCBH: Found child <%s> with handle=<%d>!",prlChildGroup->section_name,prlChildGroup->handle);
			*prpChild = prlChildGroup;
			prlChildEle = NULL;
		}
		else
		{
			prlChildEle = ListFindNext(prgChildList);
		}
	}
}
/* ******************************************************************** */
/* The KillChild()-function 				*/
/* ******************************************************************** */
static void KillChild(CHILD_INFO *prpChild)
{
	int ilRc = RC_SUCCESS;

	dbg(TRACE,"KillChild: Section <%s>",prpChild->section_name);
	if (QBClose(prpChild->handle)==-1 && NatErrNo()==NAT_E_BADSTAT)
	{
		dbg(TRACE,"KillChild: QBClose() failed! Terminating due to AMOK-child!");

		/* following would be a recursive call */
		/* meaning that i will end in an endless-loop.*/
		/* I can never terminate smoothly */
		/* Terminate(TRUE);*/
	}
}
/* ******************************************************************** */
/* The FindTowerInfoByCedaData - function*/
/* ******************************************************************** */
static int FindTowerInfoByCedaData(char *pcpFields,char *pcpData,TWR_DATA **prpTwrData,int *ipQBHandle)
{
	int ilRc = RC_FAIL;
	int ilRc_urno = RC_FAIL;
	int ilPosInFields = 0;
	int ilCol = 0;
	int ilFromStart = 0;
	BOOL blTwrInfoChange = FALSE;
	char pclUrno[XXS_BUFF];
	char pclPsta[XXS_BUFF];
	char pclOnbl[XXS_BUFF];
	char pclPstd[XXS_BUFF];
	char pclOfbl[XXS_BUFF];
	char pclAdid[XXS_BUFF];
	char pclCsgn[XXS_BUFF];
	char pclStoa[XXS_BUFF];
	char pclStod[XXS_BUFF];
	char pclTmpBuff[XXS_BUFF];
	CHILD_INFO *prlChildGroup = NULL;
	LPLISTELEMENT prlChildEle = NULL;
	TWR_DATA	*prlTwrData = NULL;
	LPLISTELEMENT prlTwrDataEle = NULL;
	
	memset(pclCsgn,0x00,XXS_BUFF);
	memset(pclAdid,0x00,XXS_BUFF);
	memset(pclStoa,0x00,XXS_BUFF);
	memset(pclStod,0x00,XXS_BUFF);

	ilPosInFields = 0;
	if ((ilRc = FindItemInList(pcpFields,"CSGN",',',&ilPosInFields,&ilCol,&ilFromStart)) == RC_SUCCESS)
	{
		GetDataItem(pclCsgn,pcpData,ilPosInFields,',',"","");
		dbg(DEBUG,"FTIBCD: CSGN <%s>",pclCsgn);
	}
	ilPosInFields = 0;
	if ((ilRc = FindItemInList(pcpFields,"ADID",',',&ilPosInFields,&ilCol,&ilFromStart)) == RC_SUCCESS)
	{
		GetDataItem(pclAdid,pcpData,ilPosInFields,',',"","");
		dbg(DEBUG,"FTIBCD: ADID <%s>",pclAdid);
	}
	ilPosInFields = 0;
	if ((ilRc = FindItemInList(pcpFields,"STOA",',',&ilPosInFields,&ilCol,&ilFromStart)) == RC_SUCCESS)
	{
		GetDataItem(pclStoa,pcpData,ilPosInFields,',',"","");
		dbg(DEBUG,"FTIBCD: STOA <%s>",pclStoa);
		pclStoa[8]=0x00;
	}
	ilPosInFields = 0;
	if ((ilRc = FindItemInList(pcpFields,"STOD",',',&ilPosInFields,&ilCol,&ilFromStart)) == RC_SUCCESS)
	{
		GetDataItem(pclStod,pcpData,ilPosInFields,',',"","");
		dbg(DEBUG,"FTIBCD: STOD <%s>",pclStod);
		pclStod[8]=0x00;
	}

	prlChildEle = ListFindFirst(prgChildList);
	while(prlChildEle!=NULL)
	{
		prlChildGroup = (CHILD_INFO*)prlChildEle->Data;
		if (strncmp(prlChildGroup->section_name,"TWRCOM",6) == 0)
		{
			*ipQBHandle = prlChildGroup->handle;
			prlChildEle = NULL;
			dbg(DEBUG,"FTIBCD: Found data-list for <%s>! QB_Handle=<%d>!",prlChildGroup->section_name,*ipQBHandle);
			/*dbg(DEBUG,"FTIBCD: strip info from event!");*/
			/*Get urno from event */
			if ((ilRc_urno = FindItemInList(pcpFields,"URNO",',',&ilPosInFields,&ilCol,&ilFromStart)) == RC_SUCCESS)
			{
				GetDataItem(pclUrno,pcpData,ilPosInFields,',',"","");
				if (strlen(pclUrno) <= 0)
				{
					dbg(DEBUG,"FTIBCD: can't find URNO inside event! Can't continue!");
					return RC_FAIL;
				}
			}

			/* start comparing the urno of the flight if it is in my ringbuffer */
			prlTwrDataEle = ListFindFirst(prlChildGroup->prlDataList);
			while(prlTwrDataEle!=NULL)
			{
				prlTwrData = prlTwrDataEle->Data;
				sprintf(pclTmpBuff,"%s",TtUd(&prlTwrData->dof));
				#if 0
				dbg(DEBUG,"FTIBCD: already stored is:");
				dbg(DEBUG,"FTIBCD: CSGN <%s>",prlTwrData->cs);
				dbg(DEBUG,"FTIBCD: DOF  <%s>",pclTmpBuff);
				dbg(DEBUG,"FTIBCD: ADES <%s>",prlTwrData->ades);
				dbg(DEBUG,"FTIBCD: ADEP <%s>",prlTwrData->adep);
				#endif

				if(
					(prlTwrData->llurno == atol(pclUrno)) ||
					(strncmp(prlTwrData->ades,"LPPT",4)==0 && strncmp(prlTwrData->cs,pclCsgn,8)==0 && strncmp(pclTmpBuff,pclStoa,8)==0) ||
					(strncmp(prlTwrData->adep,"LPPT",4)==0 && strncmp(prlTwrData->cs,pclCsgn,8)==0 && strncmp(pclTmpBuff,pclStod,8)==0)
					)
				{
					dbg(DEBUG,"FTIBCD: Found <%s> in tower data list!",prlTwrData->cs);
					dbg(DEBUG,"FTIBCD: Checking flight for new/updated values!");
					*prpTwrData = prlTwrData;

					ilPosInFields = 0;
					if ((ilRc = FindItemInList(pcpFields,"REGN",',',&ilPosInFields,&ilCol,&ilFromStart)) == RC_SUCCESS)
					{
						strcpy(pclTmpBuff,prlTwrData->pclregn);
						TrimAll(pclTmpBuff);
						dbg(DEBUG,"FTIBCD: Old-REGN <%s>",pclTmpBuff);
						GetDataItem(prlTwrData->pclregn,pcpData,ilPosInFields,',',"","");
						dbg(DEBUG,"FTIBCD: New-REGN <%s>",prlTwrData->pclregn);
						if (strcmp(pclTmpBuff,prlTwrData->pclregn) != 0)
							blTwrInfoChange = TRUE;
					}
					ilPosInFields = 0;
					if ((ilRc = FindItemInList(pcpFields,"PSTA",',',&ilPosInFields,&ilCol,&ilFromStart)) == RC_SUCCESS)
					{
						strcpy(pclTmpBuff,prlTwrData->pclpsta);
						TrimAll(pclTmpBuff);
						dbg(DEBUG,"FTIBCD: Old-PSTA <%s>",pclTmpBuff);
						GetDataItem(prlTwrData->pclpsta,pcpData,ilPosInFields,',',"","");
						dbg(DEBUG,"FTIBCD: New-PSTA <%s>",prlTwrData->pclpsta);
						if (strcmp(pclTmpBuff,prlTwrData->pclpsta) != 0)
							blTwrInfoChange = TRUE;
					}
					ilPosInFields = 0;
					if ((ilRc = FindItemInList(pcpFields,"ONBL",',',&ilPosInFields,&ilCol,&ilFromStart)) == RC_SUCCESS)
					{
						strcpy(pclTmpBuff,prlTwrData->pclonbl);
						TrimAll(pclTmpBuff);
						dbg(DEBUG,"FTIBCD: Old-ONBL <%s>",pclTmpBuff);
						GetDataItem(prlTwrData->pclonbl,pcpData,ilPosInFields,',',"","");
						dbg(DEBUG,"FTIBCD: New-ONBL <%s>",prlTwrData->pclonbl);
						if (strcmp(pclTmpBuff,prlTwrData->pclonbl) != 0)
							blTwrInfoChange = TRUE;
					}
					ilPosInFields = 0;
					if ((ilRc = FindItemInList(pcpFields,"PSTD",',',&ilPosInFields,&ilCol,&ilFromStart)) == RC_SUCCESS)
					{
						strcpy(pclTmpBuff,prlTwrData->pclpstd);
						TrimAll(pclTmpBuff);
						dbg(DEBUG,"FTIBCD: Old-PSTD <%s>",pclTmpBuff);
						GetDataItem(prlTwrData->pclpstd,pcpData,ilPosInFields,',',"","");
						dbg(DEBUG,"FTIBCD: New-PSTD <%s>",prlTwrData->pclpstd);
						if (strcmp(pclTmpBuff,prlTwrData->pclpstd) != 0)
							blTwrInfoChange = TRUE;
					}
					ilPosInFields = 0;
					if ((ilRc = FindItemInList(pcpFields,"OFBL",',',&ilPosInFields,&ilCol,&ilFromStart)) == RC_SUCCESS)
					{
						strcpy(pclTmpBuff,prlTwrData->pclofbl);
						TrimAll(pclTmpBuff);
						dbg(DEBUG,"FTIBCD: Old-OFBL <%s>",pclTmpBuff);
						GetDataItem(prlTwrData->pclofbl,pcpData,ilPosInFields,',',"","");
						dbg(DEBUG,"FTIBCD: New-OFBL <%s>",prlTwrData->pclofbl);
						if (strcmp(pclTmpBuff,prlTwrData->pclofbl) != 0)
							blTwrInfoChange = TRUE;
					}
					ilPosInFields = 0;
					if ((ilRc = FindItemInList(pcpFields,"REM1",',',&ilPosInFields,&ilCol,&ilFromStart)) == RC_SUCCESS)
					{
						dbg(DEBUG,"FTIBCD: Old-REM1 <%s>",prlTwrData->pclrem1);
						GetDataItem(prlTwrData->pclrem1,pcpData,ilPosInFields,',',"","");
						dbg(DEBUG,"FTIBCD: New-REM1 <%s>",prlTwrData->pclrem1);
						/*blTwrInfoChange = FALSE;*/
					}
					prlTwrDataEle = NULL;
				}
				else
				{
					prlTwrDataEle = ListFindNext(prlChildGroup->prlDataList);
				}
			}
		}
		else
		{
			prlChildEle = ListFindNext(prlChildGroup->prlDataList);
		}
	}
	/* only if a tower relevant information has been changed we confirm sending here */
	if (blTwrInfoChange == TRUE)
	{
		return RC_SUCCESS;
	}
	else
	{
		dbg(TRACE,"FTIBCD: no relevant info has changed OR flight not in interface comm.-list!");
		dbg(TRACE,"FTIBCD: Discard sending anything to ATC-tower!");
		return RC_FAIL;
	}
}
/* ******************************************************************** */
/* The ProcessTwrData - function*/
/* ******************************************************************** */
static int ProcessTwrData(char *pcpDataString,TWR_DATA *prpMsgCopy)
{
	int ilRc = RC_SUCCESS;
	int ilRc_times = RC_SUCCESS;
	int ilCnt = 0;
	TWRD_TIME *prlTwrTime = NULL;

	TWRD_MESSAGE	*prlMsg = NULL;

	prlMsg = TWRDDecode(pcpDataString);
	dbg(DEBUG,"PTD:---- MESSAGE START -----\n%s",TWRDDumpMessage(prlMsg));
	dbg(DEBUG,"PTD:---- MESSAGE END   -----");
	if (prlMsg != NULL)
	{
		if (TWRDCheckMsg(prlMsg) != RC_SUCCESS)
		{
			dbg(TRACE,"PTD: Invalid or incomplete message received! Ignoring message!");
			dbg(TRACE,"PTD: Invalid message was:\n<%s>",TWRDDumpMessage(prlMsg));
			return RC_FAIL;
		}
			
		/*****************************************/
		/* mandatory fields inside tower message */
		/*****************************************/
		dbg(DEBUG,"PTD: ===== MESSAGE DECODING START =====");
		dbg(DEBUG,"PTD: mandatory info:---");
		TWRD_GET(prlMsg,msg_seq,&prpMsgCopy->msg_seq);
		dbg(DEBUG,"MSG: SEQ <%ld>",prpMsgCopy->msg_seq);	
		TWRD_GET(prlMsg,datim,&prpMsgCopy->datim);
		TWRD_GET(prlMsg,fKey,&prpMsgCopy->fKey);
		dbg(DEBUG,"MSG: KEY <%ld>",prpMsgCopy->fKey);
		TWRD_GET(prlMsg,cs,&prpMsgCopy->cs);
		dbg(DEBUG,"MSG: CS  <%s>",prpMsgCopy->cs);	
		TWRD_GET(prlMsg,dof,&prpMsgCopy->dof);
		dbg(DEBUG,"MSG: DOF <%s>",TtUd(&prpMsgCopy->dof));	
		TWRD_GET(prlMsg,adep,&prpMsgCopy->adep);
		dbg(DEBUG,"MSG: ADEP<%s>",prpMsgCopy->adep);	
		TWRD_GET(prlMsg,ades,&prpMsgCopy->ades);
		dbg(DEBUG,"MSG: ADES<%s>",prpMsgCopy->ades);
		/*****************************************/
		/* optional fields inside tower message */
		/*****************************************/
		dbg(DEBUG,"PTD: optional info:---");
		TWRD_GET(prlMsg,airc,&prpMsgCopy->airc);
		dbg(DEBUG,"MSG: AIRC <%s>",prpMsgCopy->airc);	
		TWRD_GET(prlMsg,wtc,&prpMsgCopy->wtc);
		dbg(DEBUG,"MSG: WTC <%c>",prpMsgCopy->wtc);	
		TWRD_GET(prlMsg,rwy,&prpMsgCopy->rwy);
		dbg(DEBUG,"MSG: RWY <%02d>",prpMsgCopy->rwy);
		TWRD_GET(prlMsg,flag,&prpMsgCopy->flag);
		dbg(DEBUG,"MSG: FLAG <%c>",prpMsgCopy->flag);	
		TWRD_GET(prlMsg,stand,&prpMsgCopy->stand);
		dbg(DEBUG,"MSG: STAND <%s>",prpMsgCopy->stand);	
		TWRD_GET(prlMsg,regMark,&prpMsgCopy->regMark);
		dbg(DEBUG,"MSG: REGN <%s>",prpMsgCopy->regMark);	
		TWRD_GET(prlMsg,aobd,&prpMsgCopy->aobd);
		dbg(DEBUG,"MSG: ONBL-date <%s>",TtUd(&prpMsgCopy->aobd));	
		TWRD_GET(prlMsg,aobt,&prpMsgCopy->aobt);
		dbg(DEBUG,"MSG: ONBL-time <%s>",TtUt(&prpMsgCopy->aobt));	
		TWRD_GET(prlMsg,firstC,&prpMsgCopy->firstC);
		dbg(DEBUG,"MSG: First-contact-time <%s>",TtUt(&prpMsgCopy->firstC));	
		TWRD_GET(prlMsg,lastC,&prpMsgCopy->lastC);
		dbg(DEBUG,"MSG: Last-contact-time <%s>",TtUt(&prpMsgCopy->lastC));	

		/* mandatory field inside tower message */
		TWRD_GET(prlMsg,mType,&prpMsgCopy->mType);
		dbg(DEBUG,"PTD: msg-specific info:---");
		switch (prpMsgCopy->mType)
		{
			case ETA:
				dbg(DEBUG,"PTD: Type: ETA-MESSAGE");	
				TWRD_GET(prlMsg,eta,&prpMsgCopy->eta);
				dbg(DEBUG,"MSG: ETA-time <%s>",TtUt(&prpMsgCopy->eta));	
			break;
			case ARR:
				dbg(DEBUG,"PTD: Type: ARR-MESSAGE");	
				TWRD_GET(prlMsg,ata,&prpMsgCopy->ata);
				dbg(DEBUG,"MSG: ATA-time <%s>",TtUt(&prpMsgCopy->ata));	
			break;
			case GO_AROUND:
				dbg(DEBUG,"PTD: Type: GO_AROUND-MESSAGE");	
				TWRD_GET(prlMsg,eta,&prpMsgCopy->eta);
				dbg(DEBUG,"MSG: ETA-time <%s>",TtUt(&prpMsgCopy->eta));	
			break;
			case DIV:
				dbg(DEBUG,"PTD: Type: DIV-MESSAGE");	
				TWRD_GET(prlMsg,new_ades,&prpMsgCopy->new_ades);
				dbg(DEBUG,"MSG: New dest. <%s>",prpMsgCopy->new_ades);	
				TWRD_GET(prlMsg,divTime,&prpMsgCopy->divTime);
				dbg(DEBUG,"MSG: Div.-time <%s>",TtUt(&prpMsgCopy->divTime));	
				TWRD_GET(prlMsg,reason,&prpMsgCopy->reason);
				dbg(DEBUG,"MSG: Reason <%d>",prpMsgCopy->reason);	
			break;
			case DIV_ETA:
				dbg(DEBUG,"PTD: Type: DIV_ETA-MESSAGE");	
				TWRD_GET(prlMsg,divTime,&prpMsgCopy->divTime);
				dbg(DEBUG,"MSG: Div.-time <%s>",TtUt(&prpMsgCopy->divTime));	
				TWRD_GET(prlMsg,prv_ades,&prpMsgCopy->prv_ades);
				dbg(DEBUG,"MSG: Prv. dest. <%s>",prpMsgCopy->prv_ades);	
				TWRD_GET(prlMsg,reason,&prpMsgCopy->reason);
				dbg(DEBUG,"MSG: Reason <%d>",prpMsgCopy->reason);	
			break;
			/*case ETD:*/
			case EOBT:
			/*case START_UP:*/
				/*dbg(DEBUG,"PTD: Type: ETD or START_UP-MESSAGE");	*/
				dbg(DEBUG,"PTD: Type: EOBT-MESSAGE");	
				/*TWRD_GET(prlMsg,etd,&prpMsgCopy->etd);
				dbg(DEBUG,"MSG: ETD-time <%s>",TtUt(&prpMsgCopy->etd));	*/
				TWRD_GET(prlMsg,eobt,&prpMsgCopy->eobt);
				dbg(DEBUG,"MSG: EOBT-time <%s>",TtUt(&prpMsgCopy->eobt));	
				TWRD_GET(prlMsg,ctot,&prpMsgCopy->ctot);
				dbg(DEBUG,"MSG: CTOT-time <%s>",TtUt(&prpMsgCopy->ctot));	
				TWRD_GET(prlMsg,slotMsg,&prpMsgCopy->slotMsg);
				dbg(DEBUG,"MSG: Slot Msg. <%s>",prpMsgCopy->slotMsg);	
			break;
			case DEP:
				dbg(DEBUG,"PTD: Type: DEP-MESSAGE");	
				TWRD_GET(prlMsg,ctot,&prpMsgCopy->ctot);
				dbg(DEBUG,"MSG: CTOT-time <%s>",TtUt(&prpMsgCopy->ctot));	
				TWRD_GET(prlMsg,slotMsg,&prpMsgCopy->slotMsg);
				dbg(DEBUG,"MSG: Slot Msg. <%s>",prpMsgCopy->slotMsg);	
				TWRD_GET(prlMsg,atd,&prpMsgCopy->atd);
				dbg(DEBUG,"MSG: ATD-time <%s>",TtUt(&prpMsgCopy->atd));	
			break;
			case CANCEL:
				dbg(DEBUG,"PTD: Type: CANCEL-MESSAGE");	
				TWRD_GET(prlMsg,slotMsg,&prpMsgCopy->slotMsg);
				dbg(DEBUG,"MSG: Slot Msg. <%s>",prpMsgCopy->slotMsg);	
			break;
			case TG:
				dbg(DEBUG,"PTD: Type: TG-MESSAGE");	
				TWRD_GET(prlMsg,tgCnt,&prpMsgCopy->tgCnt);
				dbg(DEBUG,"MSG: TG-count <%d>",prpMsgCopy->tgCnt);	

				/* we use another logic here because we only need the string of TG-times */
				if ((ilRc_times = TWRD_GET(prlMsg,tgTime,&prlTwrTime)) == 1)
				{	
					/* only about 100 (4 x numbers + 1 x blank per time) TG-times can be stored here */
					memset(prpMsgCopy->tg_times,0x00,S_BUFF);
					for(ilCnt=0; ilCnt< prlMsg->tgCnt; ++ilCnt)
					{
						strcat(prpMsgCopy->tg_times,TtUt(prlTwrTime+ilCnt));
						strcat(prpMsgCopy->tg_times,";");
					}
					prpMsgCopy->tg_times[strlen(prpMsgCopy->tg_times)-1] = 0x00;
					dbg(DEBUG,"MSG: TG-times <%s>",prpMsgCopy->tg_times);
				}
			break;
			case M_NONE:
				ilRc = RC_FAIL;
			break;
			default:
				dbg(TRACE,"PTD: Invalid message type <%d> received! Ignoring!",prpMsgCopy->mType);	
				ilRc = RC_FAIL;
			break;
		}
		dbg(DEBUG,"PTD: ===== MESSAGE DECODING END   =====");
	}
	else
	{
		dbg(TRACE,"PTD: Msg-pointer = NULL ! TWRDecode() failed!");	
		ilRc = RC_FAIL;
	}
	free(prlMsg);
	return ilRc;
}
/* ******************************************************************** */
/* The CheckTowerDataMsg - function                                     */
/* ******************************************************************** */
static int CheckTowerDataMsg(TWR_DATA *prpNewMsg,CHILD_INFO *prpChildGroup)
{
	int ilRc = RC_FAIL;
	int ilRc_DB;
	TWR_DATA	*prlTwrData = NULL;
	LPLISTELEMENT prlTwrDataEle = NULL;
	
	if (strncmp(prpChildGroup->section_name,"TWRCOM",6) == 0)
	{
		/* 1. STEP */
		/* ********************************************************************************************************************* */
		/* start comparing if the "fkey-value" or "callsign & date of flight" of the tower message can be found in my ringbuffer */
		/* ********************************************************************************************************************* */
		prlTwrDataEle = ListFindFirst(prpChildGroup->prlDataList);
		while(prlTwrDataEle!=NULL)
		{
			prlTwrData = prlTwrDataEle->Data;
			switch (prpNewMsg->mType)
			{
				/* JWE 20050127: CANCEL Messages shall only use the FPL-key-value for identification */
				/*               (mail from M. den Blanken from 26.11.2004  */
				case CANCEL:
					if (prlTwrData->fKey == prpNewMsg->fKey)
					{
						prpNewMsg->ilNewMessage = 0;
					}
				break;
				/* JWE 20050127: all other Messages shall use the key-value and/or */
			  /*	    				 the "callsign & date of flight" for identificatioon */
				default:
					/* JWE 20041007: added adep,ades to check to avoid failure in detection of arrival/departure flights */
					/*               if the same flight number , callsign is used for arrival/departure*/
					if ((prlTwrData->fKey == prpNewMsg->fKey) ||
							(strcmp(prlTwrData->cs,prpNewMsg->cs)==0 &&
							 strcmp(prlTwrData->adep,prpNewMsg->adep)==0 &&
							 strcmp(prlTwrData->ades,prpNewMsg->ades)==0 &&
							 prlTwrData->dof.year == prpNewMsg->dof.year &&
							 prlTwrData->dof.month == prpNewMsg->dof.month &&
							 prlTwrData->dof.day == prpNewMsg->dof.day) )
					{
						/* this is an already known flight/telegram */
						prpNewMsg->ilNewMessage = 0;

						/* JWE 20050127: PRF 6772 - b) */
						if (prlTwrData->fKey != prpNewMsg->fKey)
						{
							/* setting the new value for the fKey*/ 
							prlTwrData->fKey = prpNewMsg->fKey;
							/* setting the Key - change flag for the message to be used for inidication */ 
							/* that this message information needs to be send to the tower asap */ 
							prlTwrData->ilKeyChange = TRUE;
							dbg(DEBUG,"CTDM: FPL-key of flight was changed! Resending information to ATC!");
						}
						else
						{
							prlTwrData->ilKeyChange = FALSE;
						}
					}
				break;
			};

			if (prpNewMsg->ilNewMessage==0 && prpNewMsg->mType==CANCEL) 
			{
				dbg(DEBUG,"CTDM: CANCEL-MSG.[FPL-key=<%d>, callsign=<%s>, dof=<%s>]! DELETING from IF-comm.-list!"
					,prlTwrData->fKey,prlTwrData->cs,TtUd(&prlTwrData->dof));
				ListDelete(prpChildGroup->prlDataList);
				/* we have only one element per fpl-key, so we can quit the loop here */
				prlTwrDataEle = NULL;
			}
			else if (prpNewMsg->ilNewMessage==0 && prpNewMsg->mType!=CANCEL) 
			{
				dbg(DEBUG,"CTDM: Found [FPL-key=<%d>, info-flag=<%c>, callsign=<%s>, dof=<%s>]."
					,prlTwrData->fKey,prlTwrData->flag,prlTwrData->cs,TtUd(&prlTwrData->dof));

				if (prlTwrData->ilInUfisDB == FALSE)
				{
					/* 1.5. STEP */
					/* ************************************************************************************************ */
					/* if flight could be found inside the interface comm.list but is still not in DB we try to find it */
					/* ************************************************************************************************ */
					dbg(DEBUG,"CTDM: Search flight in DB (follow up search)!");
					if ((ilRc_DB = FindFlightByTowerData(prpChildGroup,prlTwrData)) != RC_SUCCESS)
					{
						if (ilRc_DB==SQL_NOTFOUND)
						{
							dbg(DEBUG,"CTDM: Flight still NOT found in DB!");
							prlTwrData->ilInUfisDB = FALSE;
						}
						else
						{
							dbg(TRACE,"CTDM: ERROR inside FindFlightByTowerData()-function!");
							prlTwrData->ilInUfisDB = FALSE;
						}
					}
					else
					{
						prlTwrData->ilInUfisDB = TRUE;
					}
				}

				dbg(DEBUG,"CTDM: Copy UFIS-values from old to new message!");
				prpNewMsg->llurno = prlTwrData->llurno;
				strcpy(prpNewMsg->pcladid,prlTwrData->pcladid);
				strcpy(prpNewMsg->pclregn,prlTwrData->pclregn);
				strcpy(prpNewMsg->pclpsta,prlTwrData->pclpsta);
				strcpy(prpNewMsg->pclonbl,prlTwrData->pclonbl);
				strcpy(prpNewMsg->pclpstd,prlTwrData->pclpstd);
				strcpy(prpNewMsg->pclofbl,prlTwrData->pclofbl);
				strcpy(prpNewMsg->pclrem1,prlTwrData->pclrem1);
				strcpy(prpNewMsg->pclstoa,prlTwrData->pclstoa);
				strcpy(prpNewMsg->pclstod,prlTwrData->pclstod);
				prpNewMsg->ilInUfisDB = prlTwrData->ilInUfisDB;
				prpNewMsg->ilKeyChange = prlTwrData->ilKeyChange;
				dbg(DEBUG,"CTDM: Replace old message values with new message values!");
				memcpy(prlTwrData,prpNewMsg,sizeof(TWR_DATA));
				ilRc = RC_SUCCESS;
				prlTwrDataEle = NULL;
			}
			else
			{
				/*dbg(DEBUG,"CTDM: checking next message in data list!");*/
				prlTwrDataEle = ListFindNext(prpChildGroup->prlDataList);
			}
		}
	}

	/* 2. STEP */
	/* ************************************************************************************************ */
	/* if flight could not be found inside the interface comm.list then we try to find it inside the DB */
	/* ************************************************************************************************ */
	if (ilRc != RC_SUCCESS)
	{
		dbg(DEBUG,"CTDM: Search flight in DB (first search)!");
		if ((ilRc = FindFlightByTowerData(prpChildGroup,prpNewMsg)) == RC_SUCCESS)
		{
			dbg(DEBUG,"CTDM: FindFlightByTowerData() successful.");
			/* JWE 20050127: PRF 6772 - a) */
			prpNewMsg->ilInUfisDB = TRUE;
			prpNewMsg->ilNewMessage = 1;
		}
		else
		{
			if (ilRc==SQL_NOTFOUND)
			{
				/*dbg(DEBUG,"CTDM: Flight not found in DB! Generating info-telex!");*/
				dbg(DEBUG,"CTDM: Flight not found in DB! Storeing info about unknown flight!");
				/* JWE 20050127: PRF 6772 - a) */
				prpNewMsg->ilInUfisDB = FALSE;
				prpNewMsg->ilNewMessage = 1;
			}
			else
			{
				dbg(TRACE,"CTDM: ERROR inside FindFlightByTowerData()-function!");
				ilRc = RC_FAIL;
			}
		}
	}

	/* 3. STEP */
	/* ************************************************************************************************ */
	/* Now the telegram is being stored if it really is a new one with a UFIS-DB relation or if it is   */
	/* a new telegram without a DB relation yet.                                                        */
	/* ************************************************************************************************ */

	/* JWE 20050127: PRF 6772 - a) */
	/* if (ilRc==RC_SUCCESS && prpNewMsg->ilNewMessage==1)*/

	if ((ilRc==RC_SUCCESS && prpNewMsg->ilNewMessage==1) ||
			(prpNewMsg->ilInUfisDB==FALSE && prpNewMsg->ilNewMessage==1))
	{
		/******************************/
		/* adding data to ring buffer */
		/******************************/
		if (prpNewMsg->mType!=CANCEL)
		{
			dbg(DEBUG,"CTDM: Store new message nr. <%d> in ring buffer!",prpChildGroup->prlDataList->Count+1);
			if (prpChildGroup->prlDataList->Count >= 675)
			{
				ListDeleteFirst(prpChildGroup->prlDataList);
			}
			if (ListAppend(prpChildGroup->prlDataList,prpNewMsg) == NULL)
			{
				dbg(TRACE,"CTDM: ListAppend failed!");
				Terminate(FALSE);
			}
		}
		else
		{	
			dbg(DEBUG,"CTDM: DO NOT Store CANCEL message in ring buffer! Send info telex only! ");
			ilRc = RC_SUCCESS;
		}
	}
	return ilRc;
}
/* ******************************************************************** */
/* The TowerMsgToUfis() - function*/
/* ******************************************************************** */
static int TowerMsgToUfis(TWR_DATA *prpMsg,CHILD_INFO *prpChildGroup,char *pcpDataString)
{
	int ilRc = RC_SUCCESS;
	int ilRemLen = 0;
	int ilCopyLen = 0;
	char pclSelection[L_BUFF];
	char pclFields[L_BUFF];
	char pclData[L_BUFF];
	char pclTmpBuf[L_BUFF];
	char pclResultDateTime[XXS_BUFF];
	char pclTimeBuf[XXS_BUFF];
	char pclFlag[XXS_BUFF];
	char *pclATCString = NULL;

	memset(pclSelection,0x00,L_BUFF);
	memset(pclFields,0x00,L_BUFF);
	memset(pclData,0x00,L_BUFF);
	memset(pclTmpBuf,0x00,L_BUFF);
	memset(pclResultDateTime,0x00,XXS_BUFF);

	/*sprintf(pclSelection,"WHERE URNO = '%ld'",prpMsg->llurno);*/
	sprintf(pclSelection,"WHERE URNO = %ld",prpMsg->llurno);
	dbg(TRACE,"TMTU: Flight <%s> Type <%s>! Message-type <%d>!",prpMsg->cs,prpMsg->pcladid,prpMsg->mType);

	if (strncmp(prpMsg->pcladid,"A",1) == 0)
	{
		AppendToString(pclFields,"ACT5",pclData,prpMsg->airc);
		if (prpMsg->rwy!=0)
		{
			/*sprintf(pclTmpBuf,"%d",prpMsg->rwy);*/
			sprintf(pclTmpBuf,"%02d",prpMsg->rwy);
			AppendToString(pclFields,"RWYA",pclData,pclTmpBuf);
		}
		/*AppendToString(pclFields,"PSTA",pclData,prpMsg->stand);
		AppendToString(pclFields,"REGN",pclData,prpMsg->regMark);
		sprintf(pclTmpBuf,"%s",TtCdt(&prpMsg->aobd,&prpMsg->aobt));
		AppendToString(pclFields,"ONBS",pclData,pclTmpBuf);*/
	}
	else
	{
		if (strncmp(prpMsg->pcladid,"D",1) == 0)
		{
			AppendToString(pclFields,"ACT5",pclData,prpMsg->airc);
			if (prpMsg->rwy!=0)
			{
				/*sprintf(pclTmpBuf,"%d",prpMsg->rwy);*/
				sprintf(pclTmpBuf,"%02d",prpMsg->rwy);
				AppendToString(pclFields,"RWYD",pclData,pclTmpBuf);
			}
			/*AppendToString(pclFields,"PSTD",pclData,prpMsg->stand);
			AppendToString(pclFields,"REGN",pclData,prpMsg->regMark);*/
		}
		else
		{
			ilRc = RC_FAIL;	
		}
	}

	if (ilRc == RC_SUCCESS)
	{
		switch (prpMsg->mType)
		{
			case ETA:	
				/* JWE 20050128: PRF 6772: below line is wrong since ETA-time can be next day / prev day */
				/* for flights around midnight. Instead assupmtions have to be made what it could be */
				/*sprintf(pclTmpBuf,"%s",TtCdt(&prpMsg->dof,&prpMsg->eta));*/
				SetCorrectDate(prpMsg->pclstoa,TtCdt(&prpMsg->dof,&prpMsg->eta),pclResultDateTime);
				if (strlen(pclResultDateTime) > 0)
				{
					/* if the time has been confirmed by the tower we write it to ETA-FIDS also */
					if (prpMsg->flag == 'C')
					{
						/*AppendToString(pclFields,"ETAC",pclData,pclTmpBuf);*/
						AppendToString(pclFields,"ETAC",pclData,pclResultDateTime);
						dbg(DEBUG,"TMTU: Now setting ETOA <%s> due to Confirmed-flag of tower!",pclResultDateTime);
						/* automatically set by flight configuration according to etac */
						/*AppendToString(pclFields,"ETOA",pclData,pclTmpBuf);*/
					}
					else
					{
						/* only on departure we accept that the ETD is set by ATC */
						/* therefore on arrival we only get the message from SITA (flight config) */
						if (strncmp(prpMsg->pcladid,"D",1)==0)
						{
							dbg(DEBUG,"TMTU: Setting ETDA for departure flight only!");
							AppendToString(pclFields,"ETDA",pclData,pclTmpBuf);
						}
						/* automatically set by flight configuration according to etac */
						/*AppendToString(pclFields,"ETOA",pclData," ");*/
					}
				}
			break;
			case ARR:
				/*sprintf(pclTmpBuf,"%s",TtCdt(&prpMsg->dof,&prpMsg->ata));*/
				SetCorrectDate(prpMsg->pclstoa,TtCdt(&prpMsg->dof,&prpMsg->ata),pclResultDateTime);
				if (strlen(pclResultDateTime) > 0)
				{
					AppendToString(pclFields,"LNDA",pclData,pclResultDateTime);
				}
			break;
			case GO_AROUND:
				/*sprintf(pclTmpBuf,"%s",TtCdt(&prpMsg->dof,&prpMsg->eta));*/
				SetCorrectDate(prpMsg->pclstoa,TtCdt(&prpMsg->dof,&prpMsg->eta),pclResultDateTime);
				AppendToString(pclFields,"ETAA",pclData,pclResultDateTime);
			break;
			case DIV:
				if (strncmp(prpMsg->pcladid,"A",1)==0)
				{
					if (strlen(prpMsg->new_ades) > 0)
					{
						AppendToString(pclFields,"DIVR",pclData,prpMsg->new_ades);
						AppendToString(pclFields,"FTYP",pclData,"D");
					}
					/* no idea where to put the follwing two fields inside UFIS-AFT */
					/*sprintf(pclTmpBuf,"%s",TtCdt(&prpMsg->dof,&prpMsg->divTime));
					AppendToString(pclFields,"",pclData,pclTmpBuf);
					AppendToString(pclFields,"",pclData,prpMsg->reason);*/
				}
				else
				{
					dbg(TRACE,"TMTU: Received DIV-message for departure-flight! Ignoring!");
				}
			break;
			case DIV_ETA:
				/*AppendToString(pclFields,"DIVR",pclData,prpMsg->prv_ades);*/
				/*AppendToString(pclFields,"FTYP",pclData,"D");*/
				/* no idea where to put the follwing two fields inside UFIS-AFT */
				/*sprintf(pclTmpBuf,"%s",TtCdt(&prpMsg->dof,&prpMsg->divTime));
				AppendToString(pclFields,"",pclData,pclTmpBuf)
				AppendToString(pclFields,"",pclData,prpMsg->reason);*/
			break;
			case EOBT:
				/*sprintf(pclTmpBuf,"%s",TtCdt(&prpMsg->dof,&prpMsg->eobt));*/
				SetCorrectDate(prpMsg->pclstod,TtCdt(&prpMsg->dof,&prpMsg->eobt),pclResultDateTime);
				if (strlen(pclResultDateTime)>0)
				{
					AppendToString(pclFields,"ETDA",pclData,pclResultDateTime);
				}
				/*sprintf(pclTmpBuf,"%s",TtCdt(&prpMsg->dof,&prpMsg->ctot));*/
				SetCorrectDate(prpMsg->pclstod,TtCdt(&prpMsg->dof,&prpMsg->ctot),pclResultDateTime);
				if (strlen(pclResultDateTime)>0)
				{
					AppendToString(pclFields,"CTOT",pclData,pclResultDateTime);
				}
				/* no idea where to put the follwing field inside UFIS-AFT */
				/*AppendToString(pclFields,"",pclData,prpMsg->slotMsg));*/
			break;
			case DEP:
				/*sprintf(pclTmpBuf,"%s",TtCdt(&prpMsg->dof,&prpMsg->atd));*/
				SetCorrectDate(prpMsg->pclstod,TtCdt(&prpMsg->dof,&prpMsg->atd),pclResultDateTime);
				if (strlen(pclResultDateTime) > 0)
				{
					AppendToString(pclFields,"AIRA",pclData,pclResultDateTime);
					/*sprintf(pclTmpBuf,"%s",TtCdt(&prpMsg->dof,&prpMsg->ctot));*/
					SetCorrectDate(prpMsg->pclstod,TtCdt(&prpMsg->dof,&prpMsg->ctot),pclResultDateTime);
					if (strlen(pclResultDateTime)>0)
					{
						AppendToString(pclFields,"CTOT",pclData,pclResultDateTime);
					}
				}
				/* no idea where to put the follwing field inside UFIS-AFT */
				/*AppendToString(pclFields,"",pclData,prpMsg->slotMsg));*/
			break;
			case CANCEL:
				/*dbg(DEBUG,"TMTU: Flight <%s> was CANCELED by tower!",prpMsg->cs);*/
				dbg(DEBUG,"TMTU: Send info telex here !");
				SendInfoTelex(prpMsg,pcpDataString,"FLIGHT CANCELED !","X",prpChildGroup->iSit);
				ilRc = RC_FAIL;

				/* AppendToString(pclFields,"FTYP",pclData,"X"); */
				/* no idea where to put the following field inside UFIS-AFT */
				/*AppendToString(pclFields,"",pclData,prpMsg->slotMsg));*/
			break;
			case TG:
				sprintf(pclTmpBuf,"TOUCH+GO - FLIGHT! TIMES: <%s> !",prpMsg->tg_times);
				SendInfoTelex(prpMsg,pcpDataString,pclTmpBuf,"X",prpChildGroup->iSit);

				#if 0 /* here the remark field rem1 of a flight is manipulated to add some atc-info about*/
				      /* touch and go times of the flight. Instead we now send a info telex to telexpool */
				dbg(TRACE,"TMTU: REM1 <%s>",prpMsg->pclrem1);
				if (strlen(prpMsg->tg_times) > 0)
				{
					if ((pclATCString = strstr(prpMsg->pclrem1,"-ATC")) != NULL)
					{
						*pclATCString = 0x00;
					}
					ilRemLen = strlen(prpMsg->pclrem1) + strlen(pcgAtcToken) + strlen(prpMsg->tg_times) + 1 + 4;

					/* DB-length - existing rem1 - length of  ATC-token - length of " ..." */
					ilCopyLen = 256 - strlen(prpMsg->pclrem1) - strlen(pcgAtcToken) - 4 ; 

					strcat(prpMsg->pclrem1,pcgAtcToken);
					strncat(prpMsg->pclrem1,prpMsg->tg_times,ilCopyLen);
					if (ilRemLen >= 256)
					{
						dbg(TRACE,"TMTU: couldn't copy all TG-times into remark due to exceeding 256 bytes for total remark!");
						dbg(TRACE,"TMTU: total TG-times are <%s>",prpMsg->tg_times);
						strcat(prpMsg->pclrem1," ...");
					}
					AppendToString(pclFields,"REM1",pclData,prpMsg->pclrem1);
					AppendToString(pclFields,"ISRE",pclData,"+");
				}
				#endif
			break;
			case M_NONE:
				ilRc = RC_FAIL;
			break;
			default:
				dbg(TRACE,"TMTU: Invalid message type <%d> received! Ignoring!",prpMsg->mType);	
				ilRc = RC_FAIL;
			break;
		}
	}

	if (ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"----------------------------------------------------------------");
		dbg(DEBUG,"TMTU: SELECT <%s>",pclSelection);
		strcat(pclFields,"LSTU,USEU");
		dbg(DEBUG,"TMTU: FIELDS <%s>",pclFields);

		TimeToStr(pclTimeBuf,0,0,1);
		sprintf(pclTmpBuf,"%s,EXCO-ATC-interface",pclTimeBuf);
		strcat(pclData,pclTmpBuf);
		dbg(DEBUG,"TMTU: DATA   <%s>",pclData);
		dbg(DEBUG,"----------------------------------------------------------------");
		if (strcmp(prpChildGroup->write_to_db,"YES")==0)
		{
			if ((ilRc = SendEvent("UFR",pclSelection,pclFields,pclData,NULL,
													0,prpChildGroup->ilRcvModId,mod_id,PRIORITY_3,pcgAftTable))
									!= RC_SUCCESS)
			{
				dbg(TRACE,"TMTU: SendEvent() to <%d> returns <%d>!",prpChildGroup->ilRcvModId,ilRc);
			}
		}
		else
		{
			dbg(TRACE,"TMTU: DB-write is DISABLED! Set WRITE_TO_DB = YES in %s.cfg to enable!",mod_name);	
			ilRc = RC_FAIL;
		}
	}
	return ilRc;
}
/* ******************************************************************** */
/* The AppendToString() - function*/
/* ******************************************************************** */
static void AppendToString(char *pcpFieldString,char *pcpFieldName,char *pcpDataString,char *pcpData)
{
	TrimAll(pcpData);
	if (strlen(pcpData)>0)
	{
		strcat(pcpFieldString,pcpFieldName);
		strncat(pcpFieldString,",",1);
		strcat(pcpDataString,pcpData);
		strncat(pcpDataString,",",1);
	}
}
/* **************************************************************** */
/* The SendEvent routine                                         */
/* prepares an internal CEDA-event                                  */
/* **************************************************************** */
static int SendEvent(char *pcpCmd,char *pcpSelection,char *pcpFields, char *pcpData,
											char *pcpAddStruct, int ipAddLen,int ipModIdSend,int ipModIdRecv,
											int ipPriority,char *pcpTable)
{
	int     ilRc        		= RC_FAIL;
	int     ilLen        		= 0;
  EVENT   *prlOutEvent    = NULL;
  BC_HEAD *prlOutBCHead   = NULL;
  CMDBLK  *prlOutCmdblk   = NULL;

	if (ipModIdSend == 0)
	{
		dbg(TRACE,"SendEvent: Can't send event to ModID=<0> !");
	}
	else
	{
		if (pcpFields[strlen(pcpFields)-1]==',')
				pcpFields[strlen(pcpFields)-1]=0x00;

		if (pcpData[strlen(pcpData)-1]==',')
				pcpData[strlen(pcpData)-1]=0x00;

		/* size-calculation for prlOutEvent */
		ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) +
						strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + ipAddLen + 10;
		
		/*dbg(DEBUG,"Total-Len = %d+%d+%d+%d+%d+%d+%d+%d = %d",sizeof(EVENT),sizeof(BC_HEAD),sizeof(CMDBLK),strlen(pcpSelection),strlen(pcpFields),strlen(pcpData),ipAddLen,10,ilLen);*/

		/* memory for prlOutEvent */
		if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
		{
			dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
			prlOutEvent = NULL;
		}
		else
		{
			/* clear whole outgoing event */
			memset((void*)prlOutEvent, 0x00, ilLen);

			/* set event structure... */
			prlOutEvent->type	     		= SYS_EVENT;
			prlOutEvent->command   		= EVENT_DATA;

			prlOutEvent->originator  = (short)mod_id;
			prlOutEvent->retry_count  = 0;
			prlOutEvent->data_offset  = sizeof(EVENT);
			prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

			/* BC_HEAD-Structure... */
			prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
			/*prlOutBCHead->rc = (short)RC_SUCCESS;*/
			prlOutBCHead->rc = NETOUT_NO_ACK;
			/*strncpy(prlOutBCHead->dest_name,mod_name,10);*/
			strncpy(prlOutBCHead->dest_name,"EXCO-ATC",8);
			strncpy(prlOutBCHead->recv_name, "EXCO",10);

			/* Cmdblk-Structure... */
			prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
			strcpy(prlOutCmdblk->command,pcpCmd);
			strcpy(prlOutCmdblk->obj_name,pcpTable);
			strcat(prlOutCmdblk->obj_name,pcgTabEnd);
			
			/* setting tw_start entries */
			sprintf(prlOutCmdblk->tw_start,"");
			
			/* setting tw_end entries */
			sprintf(prlOutCmdblk->tw_end,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
			
			/* setting selection inside event */
			strcpy(prlOutCmdblk->data,pcpSelection);

			/* setting field-list inside event */
			strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);

			/* setting data-list inside event */
			strcpy((prlOutCmdblk->data +
						(strlen(pcpSelection)+1) +
						(strlen(pcpFields)+1)),pcpData);

			if (pcpAddStruct != NULL)
			{
				memcpy((prlOutCmdblk->data +
							(strlen(pcpSelection)+1) +
							(strlen(pcpFields)+1)) + 
							(strlen(pcpData)+1),pcpAddStruct,ipAddLen);
			}

			/*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
			/*snapit((char*)prlOutEvent,ilLen,outp);*/

			dbg(DEBUG,"SendEvent: <%d> --<%s>--> <%d>",prlOutEvent->originator,pcpCmd,ipModIdSend);

			if ((ilRc = que(QUE_PUT,ipModIdSend,ipModIdRecv,ipPriority,ilLen,(char*)prlOutEvent))
				!= RC_SUCCESS)
			{
				dbg(TRACE,"SendEvent: QUE_PUT to <%d> returns: <%d>",ipModIdSend,ilRc);
				HandleQueErr(ilRc);
			}
			/* free memory */
			free((void*)prlOutEvent); 
		}
	}
	return ilRc;
}
/* **************************************************************** */
/* The PrepareTowerData function                                 */
/* processes flight changes and prepares them for tower messages    */
/* **************************************************************** */
static int PrepareTowerMsg(TWR_DATA *prpTwrData,TWRD_MESSAGE *prpMsgForTower)
{
	int ilRc = RC_SUCCESS;
	TWRD_MESSAGE rlMsg;
	TWRD_FDATE pclDate;
	TWRD_TIME pclTime;
	char *pclBufferToSend;

	if (strncmp(prpTwrData->pcladid,"A",1) == 0)
	{
		dbg(DEBUG,"PTM: Initialise AOSARR-message type!");	
		ASSERT(TWRDInitialize(prpMsgForTower, AOSARR)==0); /* inits already datim */	
	}
	else
	{
		if (strncmp(prpTwrData->pcladid,"D",1) == 0)
		{
			dbg(DEBUG,"PTM: Initialise AOSDEP-message type!");	
			ASSERT(TWRDInitialize(prpMsgForTower, AOSDEP)==0); /* inits already datim */	
		}
		else
		{
			dbg(TRACE,"PTM: ADID is neither ARR nor DEP! Ignoring this message!");	
			ilRc = RC_FAIL;
		}
	}

	if (ilRc == RC_SUCCESS)
	{
		TrimAll(prpTwrData->pclregn);
		TrimAll(prpTwrData->pclpsta);
		TrimAll(prpTwrData->pclonbl);
		TrimAll(prpTwrData->pclpstd);
		TrimAll(prpTwrData->pclofbl);
		TrimAll(prpTwrData->pclrem1);

		/* info is only send if FPL-key and position exist for this flight */
		if (prpTwrData->fKey != 0 &&
				(strlen(prpTwrData->pclpsta) ||
				strlen(prpTwrData->pclpstd)))
		{
			/* same for both msg-types */
			if (prpTwrData->fKey != 0)
				ASSERT(TWRD_SET(prpMsgForTower,fKey,prpTwrData->fKey)==1);
			if (strlen(prpTwrData->pclregn))
				ASSERT(TWRD_SET(prpMsgForTower,regMark,prpTwrData->pclregn)==1);

			switch (prpMsgForTower->mType)
			{
				case AOSARR:
					if (strlen(prpTwrData->pclpsta))
						ASSERT(TWRD_SET(prpMsgForTower,stand,prpTwrData->pclpsta)==1);

					if (strlen(prpTwrData->pclonbl)>0)
					{
					/*dbg(DEBUG,"1.pcldate: %04d%02d%02d",pclDate.year,pclDate.month,pclDate.day);*/
						memset(&pclDate,0x00,sizeof(TWRD_FDATE));
					/*dbg(DEBUG,"2.pcldate: %04d%02d%02d",pclDate.year,pclDate.month,pclDate.day);*/
					/*dbg(DEBUG,"3.pclonbl: %s",prpTwrData->pclonbl);*/
						UdtT(prpTwrData->pclonbl,&pclDate);
					/*dbg(DEBUG,"4.pclonbl: %s",prpTwrData->pclonbl);*/
					/*dbg(DEBUG,"5.pcldate: %04d%02d%02d",pclDate.year,pclDate.month,pclDate.day);*/
						ASSERT(TWRD_SET(prpMsgForTower,aobd,&pclDate)==1);
					dbg(DEBUG,"converted-aobd: %04d%02d%02d",prpMsgForTower->aobd.year,prpMsgForTower->aobd.month,prpMsgForTower->aobd.day);

					/*dbg(DEBUG,"1.pcltime: %02d%02d",pclTime.hour,pclTime.min);*/
						memset(&pclTime,0x00,sizeof(TWRD_TIME));
					/*dbg(DEBUG,"2.pcltime: %02d%02d",pclTime.hour,pclTime.min);*/
					/*dbg(DEBUG,"3.pclonbl: %s",prpTwrData->pclonbl);*/
						UttT(prpTwrData->pclonbl,&pclTime);
					/*dbg(DEBUG,"4.pclonbl: %s",prpTwrData->pclonbl);*/
					/*dbg(DEBUG,"5.pcltime: %02d%02d",pclTime.hour,pclTime.min);*/
						ASSERT(TWRD_SET(prpMsgForTower,aobt,&pclTime)==1);
					dbg(DEBUG,"converted-aobt: %02d%02d",prpMsgForTower->aobt.hour,prpMsgForTower->aobt.min);
					}
				break;

				case AOSDEP:
					if (strlen(prpTwrData->pclpstd))
						ASSERT(TWRD_SET(prpMsgForTower,stand,prpTwrData->pclpstd)==1);

					if (strlen(prpTwrData->pclofbl)>0)
					{
					/*dbg(DEBUG,"1.pcldate: %04d%02d%02d",pclDate.year,pclDate.month,pclDate.day);*/
						memset(&pclDate,0x00,sizeof(TWRD_FDATE));
					/*dbg(DEBUG,"2.pcldate: %04d%02d%02d",pclDate.year,pclDate.month,pclDate.day);*/
					/*dbg(DEBUG,"3.pclofbl: %s",prpTwrData->pclofbl);*/
						UdtT(prpTwrData->pclofbl,&pclDate);
					/*dbg(DEBUG,"4.pclofbl: %s",prpTwrData->pclofbl);*/
					/*dbg(DEBUG,"5.pcldate: %04d%02d%02d",pclDate.year,pclDate.month,pclDate.day);*/
						ASSERT(TWRD_SET(prpMsgForTower,aobd,&pclDate)==1);
					dbg(DEBUG,"converted-aobd: %04d%02d%02d",prpMsgForTower->aobd.year,prpMsgForTower->aobd.month,prpMsgForTower->aobd.day);

					/*dbg(DEBUG,"1.pcltime: %02d%02d",pclTime.hour,pclTime.min);*/
						memset(&pclTime,0x00,sizeof(TWRD_TIME));
					/*dbg(DEBUG,"2.pcltime: %02d%02d",pclTime.hour,pclTime.min);*/
					/*dbg(DEBUG,"3.pclofbl: %s",prpTwrData->pclofbl);*/
						UttT(prpTwrData->pclofbl,&pclTime);
					/*dbg(DEBUG,"4.pclofbl: %s",prpTwrData->pclofbl);*/
					/*dbg(DEBUG,"5.pcltime: %02d%02d",pclTime.hour,pclTime.min);*/
						ASSERT(TWRD_SET(prpMsgForTower,aobt,&pclTime)==1);
					dbg(DEBUG,"converted-aobt: %02d%02d",prpMsgForTower->aobt.hour,prpMsgForTower->aobt.min);
					}
				break;
			}
		}
		else
		{
			dbg(TRACE,"PTM: Mandatory info missing (either tower-FKEY,PSTA or PSTD)! Stop sending data!");	
			ilRc = RC_FAIL;
		}
	}
	return ilRc;
}
/* *************************************************************/
/* The SendTowerData()-function                                */
/* sends a message to twrcom and this one sends it to tower    */
/* *************************************************************/
static int SendTowerData(int ipQBHandle,TWRD_MESSAGE *prpMsgForTower)
{
	int ilRc = RC_SUCCESS;
	int ilRc_Restart;
	BOOL blRestart = FALSE;
	char *pclMessage = NULL;

	if (TWRDCheckMsg(prpMsgForTower) != RC_SUCCESS)
	{
		dbg(TRACE,"STD: invalid or incomplete message created! Can't send!");
		return RC_FAIL;
	}
	pclMessage = TWRDEncode(prpMsgForTower);
	ASSERT(pclMessage);
	dbg(DEBUG,"STD: Send following message to tower: \n<%s>",TWRDDumpMessage(prpMsgForTower));
	ilRc = QBWrite(ipQBHandle, TOWER_DATA, pclMessage, strlen(pclMessage)+1,1000);
	if(ilRc < 0)
	{
		dbg(TRACE,"STD: QBWrite() failed!"); 
		ilRc = RC_FAIL;
		blRestart = TRUE;
	}
	else if (ilRc == 0)
	{
		dbg(TRACE,"STD: QBWrite() returns timeout!"); 
		ilRc = RC_FAIL;
	}
	else if (ilRc > 0)
	{
		dbg(DEBUG,"STD: QBWrite() successfull!"); 
		ilRc = RC_SUCCESS;
	}
	if (blRestart == TRUE)
	{
		dbg(TRACE,"STD: restarting child TWRCOM!");
		if ((ilRc_Restart = RunChild("TWRCOM")) == RC_SUCCESS)
		{
			dbg(TRACE,"STD: Restart of TWRCOM-child successfull or disabled!");
		}
		else
		{
			dbg(TRACE,"STD: Restart of TWRCOM-child failed!");
		}
	}
	free(pclMessage);
	return ilRc;
}
/* *************************************************** */
/*                                                     */
/* *************************************************** */
static int SendInfoTelex(TWR_DATA *prpMsg,char* pcpDataString,char* pcpAddString,char* cpStatus,int ipDoSend)
{
	int ilRc = RC_FAIL;
	char pclSelection[L_BUFF];
	char pclFields[L_BUFF];
	char pclData[L_BUFF];
	char pclTmpBuf[L_BUFF];
	char pclTmpBuf2[L_BUFF];
	char pclInfoString[L_BUFF];

	memset(pclSelection,0x00,L_BUFF);
	memset(pclFields,0x00,L_BUFF);
	memset(pclData,0x00,L_BUFF);
	memset(pclTmpBuf,0x00,L_BUFF);
	memset(pclInfoString,0x00,L_BUFF);

	sprintf(pclInfoString,"-------------- \263"\
											"CALLSIGN      : %s \263"\
											"DATE OF FLIGHT: %s \263"\
											"REGISTRATION  : %s \263"\
											"AIRCRAFT      : %s \263"\
											"-------------- \263"\
											"MESSAGE CONT. : %s \263"\
											,prpMsg->cs,TtUd(&prpMsg->dof),prpMsg->regMark,prpMsg->airc,pcpDataString);

	/* airline 3 letter code from callsign */
	strncpy(pclTmpBuf,prpMsg->cs,3);
	AppendToString(pclFields,"ALC3",pclData,pclTmpBuf);

	/* flight number from callsign */
	strncpy(pclTmpBuf,&prpMsg->cs[3],4);
	AppendToString(pclFields,"FLTN",pclData,pclTmpBuf);

	/* suffix empty by tower */
	AppendToString(pclFields,"FLNS",pclData," ");

	/* date of event-generation */
	TimeToStr(pclTmpBuf,0,0,1);
	AppendToString(pclFields,"CDAT",pclData,pclTmpBuf);

	/* flight; urno for flight */
	sprintf(pclTmpBuf,"%ld",prpMsg->llurno);
	AppendToString(pclFields,"FLNU",pclData,pclTmpBuf);

	/* is this a send=S or received=R telegram */
	AppendToString(pclFields,"SERE",pclData,"R");

	/* time of receipt of message */
	sprintf(pclTmpBuf,"%s",TtCdt(&prpMsg->datim.dat,&prpMsg->datim.tim));
	AppendToString(pclFields,"TIME",pclData,pclTmpBuf);

	/* telex status ; */
	AppendToString(pclFields,"STAT",pclData,cpStatus);

	/* telex type ; here always ATC */
	AppendToString(pclFields,"TTYP",pclData,"ATC");

	/* message text */
	memset(pclTmpBuf,0x00,L_BUFF);
	memset(pclTmpBuf2,0x00,L_BUFF);
	sprintf(pclTmpBuf,"%s %s\263%s\263%s",pcgTlxToken,prpMsg->cs,pcpAddString,pclInfoString);
	/* max. 2000 chars fit in the database */ 
	strncpy(pclTmpBuf2,pclTmpBuf,1999);
	AppendToString(pclFields,"TXT1",pclData,pclTmpBuf2);

	dbg(DEBUG,"SIT: Tlx-info <%s>",pclTmpBuf);

	if (ipDoSend == 1)
	{
		if ((ilRc = SendEvent("IBT"," ",pclFields,pclData,NULL,
									0,1200,mod_id,PRIORITY_3,"TLX")) != RC_SUCCESS)
		{
			dbg(TRACE,"SIT: SendEvent() to <1200=router> failed with <%d>!",ilRc);
		}
		else
		{
			dbg(DEBUG,"SIT: SendEvent() to <1200=router> successfull!");
		}
	}
	else
	{
		dbg(TRACE,"SIT: Sending of info telexes disabled! Set SEND_INFO_TELEX = 1 to enable!");
	}
	return ilRc;
}
/* *****************************************************************************************/
/* SetCorrectDate()-function                                                               */
/* IN: pcpScheduledDateTime = UTC date/time of UFIS for this flight                        */
/* IN: pcpCedaDateTwrTime   = UTC UFIS-date+ UTC towertime combined (may be the wrong day) */
/* IN: pcpResultDateTime    = UTC corrected date/time according to implemenet rules)       */
/* *****************************************************************************************/
static void SetCorrectDate(char *pcpScheduledDateTime, char *pcpCedaDateTwrTime, char *pcpResultDateTime)
{
	time_t tlSched = 0;
	time_t tlSchedDate = 0;
	time_t tlCedaTwr = 0;
	time_t tlResult = 0;
	char pclSchedDate[XXS_BUFF];
	char pclTwrTime[XXS_BUFF];

	pcpResultDateTime[0] = 0x00;

	dbg(DEBUG,"SCD: SCHED:<%s>, 1.ASSUMPTION:<%s>, RESULT:<%s>"
		,pcpScheduledDateTime,pcpCedaDateTwrTime,pcpResultDateTime);

	if (strlen(pcpCedaDateTwrTime) > 0 && strlen(pcpScheduledDateTime) > 0)
	{
		/* convert strings to times for calculation */
		StrToTime(pcpScheduledDateTime,&tlSched);
		StrToTime(pcpCedaDateTwrTime,&tlCedaTwr);
		/* Init result with first assumption in case none of below if's applies */
		StrToTime(pcpCedaDateTwrTime,&tlResult);

		/* use date part only to put whole day more/less on it */
		memset(pclSchedDate,0x00,XXS_BUFF);
		strcpy(pclSchedDate,pcpScheduledDateTime);
		pclSchedDate[8]=0x00;
		strcat(pclSchedDate,"000001");
		StrToTime(pclSchedDate,&tlSchedDate);

		memset(pclTwrTime,0x00,XXS_BUFF);
		strcpy(pclTwrTime,(char*)&pcpCedaDateTwrTime[8]);

		/* Example:                                     */
		/* STOA: 20050127233000
		/* ETOA by ATC: 00:30                           */
		/* ETOA first assumed by UFIS: 20050127003000 (combination of "DOF" and "ETA" by ATC) */
		/* Now tlCedaTwr is 23 hours smaller than 23:30 (UFIS-sched) - the time-accuraccy(12:00 hours) */
		/* ==> 20050127003000 is smaller than 20050127113000 ==> means that the time sent from ATC most likely */
		/* was meant for next day . Therefore a full day (86400 sec.) are added to the first assumed ETOA of UFIS*/
		/* to correct this value accordingly */

		if (tlCedaTwr < (tlSched - igTimeAccuracy))
		{
			/*tlResult = tlSchedDate + 86400;*/
			tlResult = tlCedaTwr + 86400;
		}
		else
		{
			if (tlCedaTwr > (tlSched + igTimeAccuracy))
			{
				/*tlResult = tlSchedDate - 86400;*/
				tlResult = tlCedaTwr - 86400;
			}
			else
			{
				tlResult = tlCedaTwr;
			}
		}

		/* here the same applies vice versa for flights comming early at the prev day */
		TimeToStr(pcpResultDateTime,tlResult,0,0);

		/* Now copy given time by tower to the corrected date value */
		strncpy((char*)&pcpResultDateTime[8],pclTwrTime,6);

		dbg(DEBUG,"SCD: SCHED:<%s>, 1.CORR.-RESULT:<%s>"
			,pcpScheduledDateTime,pcpResultDateTime);
	}
	else
	{
		dbg(DEBUG,"SCD: Missing ATC-time OR scheduled time. Can't assume correct date!");
	}
}
