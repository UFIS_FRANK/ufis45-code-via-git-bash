#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Lis/LIS_Server/Base/Server/Interface/twrhds.c 1.2 2004/04/02 20:12:22SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*
 * ABB/AAT UFIS-ATC Handler (tower handler)
 *
 * twrhds - Tower Handler Simulator Program
 *
 * j.heilig Sep 03 2001
 *
 * Sep 9 01 : added AOSARR simulation
 *
 * This program is part of the test environment. It can be used to test
 * the tower comm module without the tower handler part which is simulated
 * by the program. The CEDA (MIKE) messaging que is emulated by the program
 * to allow the twrcom to communicate via it. Note that this uses other
 * queue keys than the original system. 
 *
 * Like the real tower handler, twrhds starts twrcom (or the program
 * specified  by  the -pg option) and waits for messages from it.
 * Messages may be heartbeats, termination notifications and
 * tower data. Heartbeats and tower data are simply displayed. In addition,
 * if AOS simulation is enabled (see the --as option below), then
 * the program sends AOSARR messages to twrcom upon the receipt of
 * ARR tower messages. Receiving a termination notification or detecting
 * the termination of twrcom indirectly (see message loop) causes twrhds
 * to terminate.
 *
 * The following twrcom messages are processed (also shown are the parameters
 * contained in the message buffer):
 *
 * type = 0 : twrcom heartbeat : ULong count
 * type = 1 : twrcom/twrhdl notification : short command, long value
 *            command = 1 : terminate : value is the remaining time for
 *            twrcom to send messages (twrhdl->twrcom) or the number of
 *            unsent messages immediately before termination (twrcom->twrhdl)
 * type = 2 : tower data : StringZ data string.
 *
 * The program is also intended as a programming example for implementing
 * the real tower handler. It shows the typical actions taken when working
 * with twrcom.
 *
 * Usage: twrhds [options] [twrcom command line options].
 *
 * Options must begin with "--". Starting with the first argument not
 * beginning with the "--" string, the command line arguments are 
 * considered as twrcom parameters and will be passed to it.
 *
 * --pg : name of the program to start (defaults to twrcom)
 * --lo : log options (defaults to errors and trace reports, see nat.h)
 *
 * --lf : log file (defaults to stderr)
 *
 * --lt : lifetime in seconds (default is infinite). After this time has
 *        elapsed, the program send a shutdown command to twrcom to initiate
 *        its termination. After receiving the confirmation, thrhds exits.
 *        Note that an alarm signal is used to implement the lifetime in
 *        order to be independent from the heartbeat and generally the 
 *        queue system. This is exactly how thhe real tower handlers is
 *        likely to implement internal timers.
 *
 * --as   AOS message simulation (no parameter required)
 *
 * Example: twrhds --lo 12 --lt 60 -lo 255 -sp 9999 224.0.1.1 9998 
 *
 * This will start twrhds with logging flags set for errors and trace reports.
 * After 60 seconds, twrhds will send a terminate command to twrcom.
 *
 * twrcom is started with the command line "-lo 255 -sp 9999 224.0.1.1 9998".
 *
 * $Log: Ufis/Lis/LIS_Server/Base/Server/Interface/twrhds.c  $
 * Revision 1.2 2004/04/02 20:12:22SGT jwe 
 * added mks-version string to sources/headers
 * Revision 1.1 2001/09/28 16:34:33CEST jwe 
 * Initial revision
 * Member added to project /usr1/projects/LISBON/Serverprograms.pj
 * ----
 * $Id: Ufis/Lis/LIS_Server/Base/Server/Interface/twrhds.c 1.2 2004/04/02 20:12:22SGT jwe Exp  $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>


#include "nat.h"
#include "twrdat.h"

/*--- ceda simulation ---*/

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#ifndef LINUX
#include "ugccsma.h"
#endif
#include "glbdef.h"
#include "uevent.h"
#include "quedef.h"

#define QUEID_IN	32
#define QUEID_ACK	33

/*--- program vars ---*/

static int logopts = NAT_LERR | NAT_LTRC;
static char *logfile = NULL;
static char *program = "twrcom";
static int qid_in, qid_ack;
static int lifetime = -1;
static int alarm_hit = 0;
static int terminate = 0;
static int arrsim = 0;

/*--- program functions ---*/

static void usage(void);
static int get_opts(int, char **);
static int que_init(void);
static int que_get(ITEM **);
static int get_data(ITEM *, QBData **);
static int process_data(int, QBData *);
static void lifetime_alarm(int);
static int shut_twrcom(int);
static void twrcom_send(int , short , char *, int );
static void aos_sim(int, char *);

int main(int argc, char **argv)
{
int ix, rc, ax;
QBHandle qbh, qbs;
char cl[1000];
QBCommCtl ctl;
QBData *pd;
ITEM *pitem;

/*
 * get opts
 */
	if( (ax = get_opts(argc, argv)) < 1)
	{
		usage();
	}

/*
 * init logging
 */

	NatSetLog(logopts, logfile, "TWRHDS");
	LOG(TRCLOG,"Starting up");

/*
 * build twrcom command line
 */

	for(ix=ax; ix<=argc; ++ix)
	{
		if(ix == ax)
				strcpy(cl,program);
		else
		{
				strcat(cl," ");
				strcat(cl,argv[ix-1]);
		}
	}
	LOG(DBGLOG,"%s command line: <%s>", program, cl);

/*
 * init que system
 */

	if(que_init() == -1)
	{
		LOG(ERRLOG,"que_init failed: <%d> <%s>", errno, strerror(errno));
		exit(1);
	}

/*
 * init queue bridge
 */
 
	ctl.type = 0;
	ctl.heartbeat = 5000;
	ctl.ack_policy = 0;
	ctl.inque_key = QUEID_IN;
	ctl.ackque_key = QUEID_ACK;
	ctl.rcvmod_id = 9011;
	ctl.sndmod_id = 9012;
	strcpy(ctl.dest_name,"DEST");
	strcpy(ctl.recv_name,"RECV");

	qbh = QBOpenCL(cl, &ctl, QB_SIGNONE);

	if(qbh == -1)
	{
		LOG(ERRLOG,"QBOpen error, errno=%d text=%s", ERRNO, ERRTEXT);
		exit(1);
	}

	LOG(DBGLOG,"QBOpen Ok -> que()");

/*
 * set kill alarm if twrcom lifetime is limited
 */

	if(lifetime > 0)
	{
		signal(SIGALRM, lifetime_alarm);
		LOG(TRCLOG,"Setting TWRCOM lifetime alarm to %d sec", lifetime);
		alarm(lifetime);
	}

/*
 * get and process queue messages. This is where a MIKE program
 * normally performs a que(GET...) call, which we simulate with "que_get",
 * in a loop.
 *
 * The loop is finished if either an error occurs or a termination message
 * is received from twrcom (which is normally the response to the termination
 * notification sent by twrhds).
 */

	pitem = NULL;
	do
	{
/*
 * get next queue message
 */
		rc = que_get(&pitem);
/*
 * if Ok, see if it's from twrcom (the returned handle must equal to ours).
 * Note that with the current version of this program, the ITEM buffer is
 * always expected to contain twrcom message (this is asserted).
 *
 * The message extracted from the ITEM buffer can however be empty (NULL
 * ptr). This indicates that twrcom has terminated before its last 
 * message has been received (race condition).
 *
 */
		if(rc == 0)
		{
			qbs = get_data(pitem, &pd);
			ASSERT(qbs == qbh || qbs > 0);
			if(pd)
 			{
				rc = process_data(qbh, pd); /* may set terminate flag */
				free(pd);
			}
			else
			{
				terminate = 1;				/* no more messages expected from twrcom */
			}
		}
/*
 * check if lifetime flag has been hit by alarm. If so, tell twrcom to
 * terminate
 */
		if(alarm_hit)
		{
			alarm_hit = 0;
			LOG(TRCLOG,"lifetime alarm expired -> shutting down TWRCOM");
			rc = shut_twrcom(qbh);
		}
	} while (rc >= 0 && !terminate);


	QBClose(qbh);

	LOG(DBGLOG,"Exiting");

	exit(0);
}

/*
 * initialize CEDA queue system emulation
 */

int que_init()
{
	qid_in = msgget(QUEID_IN, IPC_CREAT|0666);
	qid_ack = msgget(QUEID_ACK, IPC_CREAT|0666);

	if(qid_in == -1 || qid_ack == -1)
		return -1;
	return 0;
}

/*
 * this function works similarly to MIKE que() with GET. If the ITEM parameter
 * is not NULL than a buffer is allocated to hold the result. The address of
 * this buffer is returned. It may be used in succesive calls. que_get() reads
 * the next message in blocking mode.
 *
 * que_get sends the acknowledgment to the originator of the received 
 * message (this is normally done by sysqcp). 
 */

int que_get(ITEM **pi)
{
int rc;
ITEM ackit;

/*
 * get item buffer
 */
	if(*pi == NULL)
	{
		*pi = (ITEM *) malloc(I_SIZE);
		ASSERT(*pi);
	}
/*
 * wait for the next message
 */
	LOG(DBGLOG,"starting msgrcv");
	do
	{
		rc = msgrcv(qid_in, *pi, I_SIZE, 0, 0);
	} while (0); /* rc == -1 && errno == EINTR); */

	if(rc == -1)
	{
			LOG(ERRLOG,"msgrcv() failed, errno=%d text=%s", errno, strerror(errno));
			return -1;
	}
	LOG(DBGLOG,"msgrcv done");

/*
 * send acknowledgement
 */
	ackit.msg_header.mtype = (*pi)->originator;
	do
	{
		rc = msgsnd(qid_ack, &ackit, sizeof(ackit), IPC_NOWAIT);
	} while (rc == -1 && errno == EINTR);
	if(rc == -1)
	{
			LOG(ERRLOG,"msgsnd() failed, errno=%d text=%s", errno, strerror(errno));
			return -1;
	}
	LOG(DBGLOG,"sending ack Ok");
/*
 * Ok
 */
	return 0;
}

/*
 * This function checks if the ITEM buffer pointed to by the pointer parameter 
 * contains a queue bridge message. If not then -1 is returned. Otherwise
 * the queue bridge message is retrieved and the pd parameter is set to point
 * to it and the handle of the involved queue bridge is returned. It is legal
 * that another positive value is returned and  pd is set to NULL. This
 * indicates that the queue bridge having sent the message is already closed.
 *
 * Unlike with que_get() (the behaviour of which is inherited from MIKE), the
 * ownership of the returned queue bridge message is passed to the caller so
 * that it must free() the message when it is not required anymore.
 */

int get_data(ITEM *pi, QBData **pd)
{
int rc;
	rc = QBSelect(pi);
	if(rc == -1)
	{
			LOG(ERRLOG,"Unxpected queue message ignored.");
			return -1;
	}
	*pd = QBGetMessage(rc, pi);
	return rc;
}

/*
 * Verify the type of a queue bridge message an retrieve parameters
 */

int process_data(int hd, QBData *pd)
{
unsigned long ulv;
int ix;
char *pc;
short cmd;
	ix = 0;
	LOG(DBGLOG,"QBData.type=%d", pd->type);
/*
 * heartbeat 
 */
	if(pd->type == 0)
	{
		ASSERT(QB_GET_ULong(&ulv, pd, &ix)==0);
		LOG(TRCLOG,"RECV heartbeat, id=%lu", ulv);
	}
/*
 * internal notification (only termination is supported)
 */
	else if(pd->type == 1)
	{
		ASSERT(QB_GET_Short(&cmd, pd, &ix)==0 && cmd==1);
		LOG(TRCLOG,"RECV type 1 message cmd=1 -> terminate");
		terminate = 1;
	}
/*
 * tower data 
 */
	else if(pd->type == 2)
	{
		ASSERT(QB_GET_StringZ(&pc, pd, &ix) == 0);
		LOG(TRCLOG,"RECV tower data=<%s>", pc);
		if(arrsim)
			aos_sim(hd, pc);
	}
/*
 * unknown
 */
	else
	{
		LOG(ERRLOG,"RECV unknown type %d", pd->type);
		return -1;
	}
		
	return 0;
}

/*
 * get options
 * returns the index of the first not used arg (> 0) 
 * or a negative value on error
 */

int get_opts(int argc, char **argv)
{
int ax;
	for(ax=1; ax < argc && !strncmp(argv[ax],"--",2); ++ax)
	{
		if(strcmp(argv[ax],"--lo") == 0)
		{
			++ax;
			if(ax == argc || (sscanf(argv[ax],"0x%x", &logopts) != 1 &&
         sscanf(argv[ax],"%d", &logopts) != 1 &&
			   sscanf(argv[ax],"x%x", &logopts) != 1 ))
			{
				fprintf(stderr,"bad or missing logopts spec");
				return -1;
			}
		}
		else if(strcmp(argv[ax],"--lt") == 0)
		{
			++ax;
			if(ax == argc || sscanf(argv[ax],"%d", &lifetime) != 1)
			{
				fprintf(stderr,"bad or missing lifetime spec");
				return -1;
			}
		}
		else if(strcmp(argv[ax],"--lf") == 0)
		{
			++ax;
			if(ax == argc)
			{
				fprintf(stderr,"logfile name expected");
				return -1;
			}
			else
				logfile = argv[ax];
		}
		else if(strcmp(argv[ax],"--pg") == 0)
		{
			++ax;
			if(ax == argc)
			{
				fprintf(stderr,"program name expected");
				return -1;
			}
			else
				program = argv[ax];
		}
		else if(strcmp(argv[ax],"--as") == 0)
		{
			arrsim = 1;
		}
		else
		{
			return -1;
		}
	}
	return ax;
}

/*
 * print usage and exit with error code 1
 */

static void usage(void)
{
	fprintf(stderr,"usage: twrhds [options] [program-options]\n");
	exit(1);
}

/*
 * alarm handler to kill twrcom
 */

void lifetime_alarm(int n)
{
	alarm_hit = 1;
}


/*
 * shutdown twrcom - shutdown in 5 seconds
 */

int shut_twrcom(int hd)
{
char *p;
int ix;
/*
 * build termination notification allowing 5000 msec to close
 * gracefully. 
 */
	ix = 0;
	ASSERT(QB_PUT_Short(1, &p, &ix) == 0);
	ASSERT(QB_PUT_Long(5000, &p, &ix) == 0);
	twrcom_send(hd, 1, p, ix);
	free(p);
	return 0;
}

void twrcom_send(int hd, short ty, char *data, int len)
{
int rc;
/*
 * send the message with 1000 msec timeout
 */
	rc = QBWrite(hd, ty, data, len, 1000);
	if(rc == -1)
	{
		LOG(ERRLOG,"QBWrite() failed, errno=%d text=%s", ERRNO, ERRTEXT);
		return;
	}
	else if (rc == 0)
	{
		LOG(ERRLOG,"QBWrite() timeout");
		return;
	}
	LOG(DBGLOG,"shutdown(5) sent to TWRCOM");
}

void aos_sim(int hd, char *data)
{
TWRD_MESSAGE *msg, aosarr;
TWRD_MTYPE mType;
unsigned long fKey;
char buff[20], *p;
static int stdid = 1;
static int regid = 104;
	if( (msg = TWRDDecode(data)) == NULL)
	{
		LOG(ERRLOG,"aos_sim: bad data");
		return;
	}
	ASSERT(TWRD_GET(msg,mType,&mType) == 1);
	if(mType != ARR)
		return;
/*
 * init AOSARR msg
 */
	TWRDInitialize(&aosarr, AOSARR); /* implicitly sets datim field */
/*
 * copy the fkey
 */
	ASSERT(TWRD_GET(msg, fKey, &fKey) == 1);
	ASSERT(TWRD_SET(&aosarr, fKey, fKey) == 1);
/*
 * set stand (normally retrieved from AOS DB)
 */
	sprintf(buff,"B%d", stdid++ % 20);
	ASSERT(TWRD_SET(&aosarr, stand, buff) == 1);
/*
 * set regmark (normally retrieved from AOS DB)
 */
	sprintf(buff,"RM-%d", regid++ % 1000);
	ASSERT(TWRD_SET(&aosarr, regMark, buff) == 1);
/*
 * set seq no to zero to allow encoding
 */
	ASSERT(TWRD_SET(&aosarr, msg_seq, 0) == 1);
/*
 * encode the message
 */
	ASSERT((p = TWRDEncode(&aosarr)) != NULL);
/*
 * send the message
 */
	twrcom_send(hd, 2, p, strlen(p)+1);
/*
 * throw away the message buffer
 */
	free(p);
}

