import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Level;
import jcedalib.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.text.*;

public class jxmlif extends DefaultHandler
{
    private static String mks_version = "@(#) UFIS_VERSION $Id: Ufis/Lis/LIS_Server/Base/Server/Interface/jxmlif.java 1.6 2006/05/26 15:55:09SGT mcu Exp  $";
    // Global value so it can be ref'd by the tree-adapter
    static Document document;
    static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
    static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";
    static String sgXsdFile = null;	
    static String sgXslFile1 = null;
    static String sgXslFile2 = null;
    static String sgDirection = null;
    static String sgXmlReceived = null;
    static String sgXmlResult = null;
    static CedaXML cedaXML = null;
    static Bchead bch = null;
    static int iBegin = 0;
    static int iEnd = 0;
    static StringBuffer XmlStringBuffer = null;
    static String sgOutgoingHeader = null;
    static String sgOutgoingFooter = null;
    static String sgConfigFile = null;
    static String sgInputQueue = null;
    static String sgInputQmanager = null;
    static String sgInputChannel = null;
    static String sgInputServer = null;
    static String sgInputSockType = null;
    static String sgInputPort = null;
    static String sgOutput1Queue = null;
    static String sgOutput1Qmanager = null;
    static String sgOutput1Channel = null;
    static String sgOutput1Server = null;
    static String sgOutput1Port = null;
    static String sgOutput1SockType = null;
    static String sgOutput2Queue = null;
    static String sgOutput2Qmanager = null;
    static String sgOutput2Channel = null;
    static String sgOutput2Server = null;
    static String sgOutput2Port = null;
 static String sgInputType = null;
 static String sgOutput1Type = null;
 static String sgOutput2Type = null;
 static String sgOutput1AllowedHost = null;
 static String sgInputAllowedHost = null;
 static String sgTimeID = null;
 static String sgFormatPatternDate = null;
 static String sgFormatPatternTime = null;
 static String sgSearchFor = null;
 static String sgReplaceWith = null;
 static String sgXsdResultCheck1 = null;
  static String sgXsdResultCheck2 = null;
	static String sgWaitForConnection = null;
	static int  igWaitForConnection = 120000;
  static int igInputPort = 1414;
	static String sgInputTimeout = null;
	static int igInputTimeout = 120000;
  static int igOutput1Port = 1414;
  static int igOutput2Port = 1414;
  static int igDataCount = 0;
     static String sgInputCheckInterval;
  static int igInputCheckInterval =  0;
     static String sgOutput1CheckInterval;
  static int igOutput1CheckInterval =  0;
     static String sgOutput2CheckInterval;
  static int igOutput2CheckInterval =  0;
	static String sgOutput1Timeout = null;
	static int igOutput1Timeout = 120000;
  static String sgKeepAliveText;
  static int ilTmpInsertIndex = 0;
 static boolean blContinue = true;
    static long WaitNext = 0;
    static String sgWaitNext = null;
    static Logger dbg,msglog,failed_msglog;
 static WebsphereQueue WQInputQueue  = null;
 static WebsphereQueue WQOutput1Queue = null;
 static WebsphereQueue WQOutput2Queue = null;
 static File schemafile = null;
 static File stylesheet1 = null;
 static File stylesheet2 = null;
 static File OutputFile1 = null;
 static File OutputFile2 = null;
 static File ResultCheckFile1 = null;
 static File ResultCheckFile2 = null;
 static FileOutputStream OutputStream1 = null;
 static PrintWriter pw = null;
 static boolean bUseXslt = true;
 static boolean bValidate = true;

 static StringBuffer sbDataBlock = null;
 
    static public void main (String argv [])
	{

	    dbg = Logger.getLogger("jxmlif");
	    msglog = Logger.getLogger("messagelog");
     failed_msglog = Logger.getLogger("failed_messagelog");
     
	    sgConfigFile = argv[0];
	    PropertyConfigurator.configure(sgConfigFile);

	    dbg.fatal(mks_version);
	    dbg.debug("CONFIG-FILE: <" + sgConfigFile + ">");
	    getConfigEntrys();
		
	    if(sgDirection.equals("INCOMING"))
	    {
				while(true)
				{
					RunAsIncoming();
				}
	    }
	    if(sgDirection.equals("OUTGOING"))
	    {
				while(true)
				{
					RunAsOutgoing();
				}
	  	}

	} // main

    private static void RunAsOutgoing()
	{
           
	    boolean bRun = true;
     boolean bTmp = false;
     int ilStart = 0;
     int ilEnd = 0;
     int ilInsertIndex = 0;
     int ilCount = 0;
     int ilTmp = 0;
     StringBuffer sbXmlReceivedPart = null;
    StringBuffer sbXmlReceived = null;
    String myTmpString = null;
    
    
    
     SimpleDateFormat mySimpleDateFormat = new SimpleDateFormat(sgFormatPatternDate);
     SimpleDateFormat mySimpleTimeFormat = new SimpleDateFormat(sgFormatPatternTime);
     FieldPosition myFieldPosition = new FieldPosition(0);
	    dbg.debug("OUT: ### STARTED IN OUTGOING MODE");

			 try
			 {
  	      WQInputQueue.DisconnectQueue();
    	    WQOutput1Queue.DisconnectQueue();
 			 }
      catch (Exception e)
    	{
        dbg.debug("Exception in RunAsOutgoing while Disconnecting Sockets " + e);
    	}

	    try
	    {
	// connect to output (external) server first if it is outgoin 
     MakeOutputConnection();
     MakeInputConnection();
		 }
      catch (Exception e)
   {
        dbg.debug("Exception in RunAsOutgoing while Connecting Sockets " + e);
   }

	    try
	    {
      	if(bValidate) schemafile = new File(sgXsdFile);
			}
 			catch (Exception e)
 			{
     		dbg.error("Exception in RunAsOutgoing while opening stylesheet: " + sgXsdFile +  "    " + e);
 			}
  
	    try
	    {
				if(bUseXslt) stylesheet1 = new File(sgXslFile1);
			}
 			catch (Exception e2)
 			{
     		dbg.error("Exception in RunAsOutgoing while opening stylesheet: " +sgXslFile1+  "    " + e2);
				return;
 			}
  
		while(bRun == true)
		{
   try
   {
     dbg.debug("receive Msg follows");
		    sgXmlReceived = WQInputQueue.receiveMsg();
   }
     catch (Exception e)
    {
      //dbg.debug("Exception in RunAsOutgoing: " + e);
      dbg.debug("OUT: ### EXCEPTION IN RunAsOutgoing(): [" + e.toString() + "]");
      String slExceptionText =  e.toString();
		 return;
    }
    try
    {
		if (sgXmlReceived.length() < 3)
		{
			continue;
		}
     if ((sgXmlReceived == null) || sgXmlReceived.equals(""))
     {
    WQInputQueue.DisconnectQueue();
    if (WQInputQueue.isConnected() == false)
    {
        dbg.debug("No Message received. Socket is not connected");
    }
    MakeInputConnection();
    continue;
     }
    
      if (sgXmlReceived.indexOf("CCO") == 0)
      {
     //  dbg.debug("Keepalive Message received");
       continue;
      }
     
      if (sgXmlReceived.indexOf("<EVENT_COMMAND>28674</EVENT_COMMAND>") != -1)
      {
       dbg.fatal("======================= Now Terminate =====================");
       WQInputQueue.DisconnectQueue();
       WQOutput1Queue.DisconnectQueue();
       Thread.sleep(1000);
       System.exit(0);
      }
      msglog.debug(sgXmlReceived);
		    cedaXML = new CedaXML();
		    bch = cedaXML.getXML(sgXmlReceived);
      sgXmlReceived = cedaXML.setXML(bch,true);
      sgXmlReceived = sgOutgoingHeader + sgXmlReceived + sgOutgoingFooter;
		  dbg.debug("OUT: Message with header and footer: <" + sgXmlReceived +">");
      if (bValidate) 
      {
    		bTmp = doValidation(sgXmlReceived, schemafile);
      }
      else
      {
       bTmp = true;
      }
      
      if (bTmp)
      {
    
   sbXmlReceived = new StringBuffer(sgXmlReceived);
       Date ActualDate = new Date(System.currentTimeMillis());
       if (sgTimeID.equals("REPLACE"))
       {
     StringBuffer DateID = new StringBuffer("");
     mySimpleDateFormat.format(ActualDate,DateID,myFieldPosition);
     StringBuffer sbReplaceWith = new StringBuffer(sgReplaceWith);
     ilStart = sbReplaceWith.indexOf("#DATE_ID#");
     if (ilStart >= 0)
     {
         ilEnd = ilStart + 9;
         sbReplaceWith.replace(ilStart,ilEnd,DateID.toString());
     }
     StringBuffer TimeID = new StringBuffer("");
     mySimpleTimeFormat.format(ActualDate,TimeID,myFieldPosition);
     ilStart = sbReplaceWith.indexOf("#TIME_ID#");
     if (ilStart >= 0)
     {
         ilEnd = ilStart + 9;
         sbReplaceWith.replace(ilStart,ilEnd,TimeID.toString());
     }
     while((ilStart = sbXmlReceived.indexOf(sgSearchFor,ilStart)) != -1)
     {
      ilEnd = ilStart + sgSearchFor.length();
      sbXmlReceived.replace(ilStart,ilEnd,sbReplaceWith.toString());
      ilStart = ilStart + sbReplaceWith.length();
     }
         
     dbg.debug("OUT: ### MESSAGE WITH REPLACEMNETS:\n-->" + sbXmlReceived.toString() + "<--");
       }

       ilStart = 0;
       ilEnd = 0;
       ilStart = sbXmlReceived.indexOf("<DATA>");
       ilInsertIndex = ilStart;
       ilEnd = sbXmlReceived.lastIndexOf("</DATA>")+7;
       myTmpString =sbXmlReceived.substring(ilStart,ilEnd);
       sbDataBlock = new StringBuffer(sbXmlReceived.substring(ilStart,ilEnd)); 
       sbXmlReceived.delete(ilStart,ilEnd);
       ilStart = 0;
       ilEnd = 0;
        ilTmp = 0; 
        ilTmpInsertIndex = ilInsertIndex;
    blContinue = true;

    sbXmlReceivedPart = new StringBuffer(sbXmlReceived.toString());
    
    while (blContinue == true)
    {
     ilTmp = sbDataBlock.indexOf("</DATA>",ilEnd);
     if(ilTmp != -1)
     {
      ilEnd = ilTmp + 7;
      ilCount++;
      
      myTmpString = sbDataBlock.substring(ilStart,ilEnd);
      sbXmlReceivedPart.insert(ilTmpInsertIndex,myTmpString);
      ilTmpInsertIndex = ilTmpInsertIndex + myTmpString.length();      
     }

     if((ilCount == igDataCount) || ((ilTmp == -1) && (ilCount > 0)))
     {
      ilCount = 0;
      if (sgOutput1Type.equals("QUEUE") || sgOutput1Type.equals("SOCKET"))
      {
       if(bUseXslt) 
       {
        sgXmlResult = doTransformation(sbXmlReceivedPart.toString(), stylesheet1);
       }
       else
       {
        sgXmlResult = doSpecialTransformation(sbXmlReceivedPart); // Quickhack for PVG45
       }
       
       //sgXmlResult = doTransformation(sbXmlReceivedPart.toString(), stylesheet1);
       //dbg.debug("Transformation-Result1: " + sgXmlResult);
       dbg.debug("OUT: ### TRANSFORMATION RESULT1:\n-->" + sgXmlResult + "<--");
       if(!sgXsdResultCheck1.equals("DEFAULT"))
       {
        dbg.debug("OUT: ### VALIDATION OF RESULT1");
        if(doValidation(sgXmlResult, ResultCheckFile1))
        {
         //dbg.debug("now send data to Qmgr: " + sgOutput1Qmanager + " Queue: " + sgOutput1Queue);
           dbg.debug("OUT: ### NOW SEND TO QMGR:(" + sgOutput1Qmanager + ") QUEUE:(" + sgOutput1Queue + ")");
         WQOutput1Queue.sendMsg(sgXmlResult);
        }
        else
        {
         failed_msglog.debug(sgXmlResult);
        }
       }
       else
       {
        //dbg.debug("now send data to Qmgr: " + sgOutput1Qmanager + " Queue: " + sgOutput1Queue);
        dbg.debug("OUT: ### NOW SEND TO QMGR:(" + sgOutput1Qmanager + ") QUEUE:(" + sgOutput1Queue + ")");
        WQOutput1Queue.sendMsg(sgXmlResult);
       }

      }

      if (sgOutput2Type.equals("QUEUE"))
      {
       if(bUseXslt) 
       {
        sgXmlResult = doTransformation(sbXmlReceivedPart.toString(), stylesheet2);
       }
       else
       {
        sgXmlResult = doSpecialTransformation(sbXmlReceivedPart); // Quickhack for PVG45
       }
       //dbg.debug("Transformation-Result2: " + sgXmlResult);
       dbg.debug("OUT: ### TRANSFORMATION RESULT2:\n-->" + sgXmlResult + "<--");
       if(!sgXsdResultCheck2.equals("DEFAULT"))
       {
        //dbg.debug("Now validate the Result:");
        dbg.debug("OUT: ### VALIDATION OF RESULT2");
        if(doValidation(sgXmlResult, ResultCheckFile2))
        {
         //dbg.debug("now send data to Qmgr: " + sgOutput2Qmanager + " Queue: " + sgOutput2Queue);
           dbg.debug("OUT: ### NOW SEND TO QMGR:(" + sgOutput2Qmanager + ") QUEUE:(" + sgOutput2Queue + ")");
         WQOutput2Queue.sendMsg(sgXmlResult);
        }
        else
        {
         failed_msglog.debug(sgXmlResult);
        }

       }
       else
       {
        dbg.debug("OUT: ### NOW SEND TO QMGR:(" + sgOutput2Qmanager + ") QUEUE:(" + sgOutput2Queue + ")");
            WQOutput2Queue.sendMsg(sgXmlResult); 
       }

      }

      sbXmlReceivedPart = new StringBuffer(sbXmlReceived.toString());
      ilTmpInsertIndex = ilInsertIndex;
     }
     if (ilTmp == -1)
     {
      blContinue = false;
     }
     ilStart = ilEnd;
    } //end of While
             
      }//if(doValidation)
		   
    }
 	  catch (Exception e)
    {
      //dbg.debug("Exception in RunAsOutgoing: " + e);
      dbg.error("OUT: #### EXCEPTION IN RunAsOutgoing(): [" + e.toString() + "]");
			 try
			 {
  	      WQInputQueue.DisconnectQueue();
    	    WQOutput1Queue.DisconnectQueue();
        	Thread.sleep(igWaitForConnection);
 			 }
      catch (Exception e1)
    	{
        dbg.debug("OUT: ####Exception in RunAsOutgoing while Disconnecting Sockets " + e1);
    	}
     		return;
    }

		}//while
	

	}// end RunAsOutgoing


  private static void RunAsIncoming()
	{

	    PrintWriter pw;
	    boolean bRun = true;
     boolean bTmp = false;
     int ilStart = 0;
     int ilEnd = 0;
     SimpleDateFormat mySimpleDateFormat = new SimpleDateFormat(sgFormatPatternDate);
     SimpleDateFormat mySimpleTimeFormat = new SimpleDateFormat(sgFormatPatternTime);
     FieldPosition myFieldPosition = new FieldPosition(0);
	    dbg.debug("Started in Incoming mode");

    // connect to output (bridge) server first if it is incoming 
			 try
			 {
       WQInputQueue.DisconnectQueue();
       WQOutput1Queue.DisconnectQueue();
		 }
      catch (Exception e)
   {
        dbg.debug("Exception in RunAsIncoming while Disconnecting Queues " + e);
   }
     	MakeOutputConnection();
 		 MakeInputConnection();

		while(bRun == true)
		{
 	    try
      {
      	sgXmlReceived = WQInputQueue.receiveMsg();
 	    }
      catch (Exception e)
   		{
        dbg.debug("Exception in RunAsIncoming: " + e);
				return;
   		}
    
		    dbg.debug(" ************************************************* START ***************************************");

      dbg.debug("Received Message: \n-->" + sgXmlReceived + "<--");
      if (sgXmlReceived.indexOf("<EVENT_COMMAND>28674</EVENT_COMMAND>") != -1)
      {
       dbg.fatal("======================= Now Terminate =====================");
			 try
			 {
		    Thread.sleep(WaitNext);
			 }
      	catch (Exception e)
      	{
					; //do nothing, will exit anyway
				}	 
       System.exit(0);
      }
      msglog.debug(sgXmlReceived);  
      Date ActualDate = new Date(System.currentTimeMillis());
      if(bValidate)
      {
				try
				{
     FileOutputStream rlOutFile = new FileOutputStream("/ceda/debug/incoming.msg");
     rlOutFile.write(sgXmlReceived.getBytes());
       bTmp = doValidation(sgXmlReceived, schemafile);
       rlOutFile.close();
			 }
      	catch (Exception e)
      	{
					dbg.fatal("Problems with Validation-File: " + e );
				}
      }
      else
      {
       bTmp = true;
      }
      if(bTmp)
      {
       StringBuffer sbXmlReceived = new StringBuffer(sgXmlReceived);
       if (sgTimeID.equals("REPLACE"))
       {
     StringBuffer DateID = new StringBuffer("");
     mySimpleDateFormat.format(ActualDate,DateID,myFieldPosition);
     StringBuffer sbReplaceWith = new StringBuffer(sgReplaceWith);
     ilStart = sbReplaceWith.indexOf("#DATE_ID#");
     if (ilStart >= 0)
     {
         ilEnd = ilStart + 9;
         sbReplaceWith.replace(ilStart,ilEnd,DateID.toString());
     }
     StringBuffer TimeID = new StringBuffer("");
     mySimpleTimeFormat.format(ActualDate,TimeID,myFieldPosition);
     ilStart = sbReplaceWith.indexOf("#TIME_ID#");
     if (ilStart >= 0)
     {
         ilEnd = ilStart + 9;
         sbReplaceWith.replace(ilStart,ilEnd,TimeID.toString());
     }
     while((ilStart = sbXmlReceived.indexOf(sgSearchFor,ilStart)) != -1)
     {
      ilEnd = ilStart + sgSearchFor.length();
      sbXmlReceived.replace(ilStart,ilEnd,sbReplaceWith.toString());
      ilStart = ilStart + sbReplaceWith.length();
     }
         
     dbg.debug("IN: ### MESSAGE WITH REPLACEMNETS:\n-->" + sbXmlReceived.toString() + "<--");
       }
      
       
    if ((sgOutput1Type.equals("QUEUE")) || sgOutput1Type.equals("SOCKET"))
    {
     if (bUseXslt)
     {
      sgXmlResult = doTransformation(sgXmlReceived, stylesheet1);
     }
     else
     {
          //do some hard-coded stuff 
     }
     
     dbg.debug("Transformation-Result1: " + sgXmlResult);
     if(!sgXsdResultCheck1.equals("DEFAULT"))
     {
      dbg.debug("Now validate the Result:");
      if(doValidation(sgXmlResult, ResultCheckFile1))
      {
       dbg.debug("now send data to Qmgr: " + sgOutput1Qmanager + " Queue: " + sgOutput1Queue);
       if (sendToInternalQueue(WQOutput1Queue, sgXmlResult) == false)
			 {
			 	return;  
			}
			 dbg.debug("after sendToInternalQueue with validation");
      }
     }
     else
     {
      dbg.debug("now send data to Qmgr: " + sgOutput1Qmanager + " Queue: " + sgOutput1Queue);
       if (sendToInternalQueue(WQOutput1Queue, sgXmlResult) == false)
			 {
			 	return;  
			}
			 dbg.debug("after sendToInternalQueue without validation");
     }
     
    }
      }//if dovalidation
      else
      {
       failed_msglog.debug(sgXmlReceived);
      }

		}//while

	}//RunAsIncoming

 private static boolean sendToInternalQueue(WebsphereQueue destQueue, String resultString)
 {
   cedaXML = new CedaXML();

		    Bchead bch = new Bchead();
				dbg.debug("cedaXML.getXML follows with: <" + resultString + ">");
      try
      {
      	bch = cedaXML.getXML(resultString);
			}
      catch (Exception e)
      {
       dbg.fatal("Exception in method :cedaXML.getXML "+e);
			 dbg.fatal("Message not send to UFIS");
			 return true;
			}

	    dbg.debug("\n"+bch.getContentInfo());
      try
      {
   destQueue.sendMsg(cedaXML.setXML(bch,false));
      }
      catch (Exception e)
      {
       dbg.fatal("Exception in method sendToInternalQueue: "+e);
			 try
			 {
        	Thread.sleep(igWaitForConnection);
					return false;
 			 }
      catch (Exception e1)
    	{
        dbg.debug("OUT: ####Exception in RunAsIncoming while Disconnecting Sockets " + e1);
     		return false;
    	}
    }
     		return true;
 }

    private static boolean doValidation(String xmlString, File theschemafile)
	{
	    try
	    {
	    	dbg.debug("GEN: ### DATAFILE VALIDATION WITH SCHEMA: (" + theschemafile.getName() +")");
		DefaultHandler SAXhandler = new jxmlif();
		SAXParserFactory SAXfactory = SAXParserFactory.newInstance();
		SAXfactory.setValidating(true);
		SAXfactory.setNamespaceAware(true);
		SAXParser saxParser= SAXfactory.newSAXParser();
		saxParser.setProperty(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
		saxParser.setProperty(JAXP_SCHEMA_SOURCE,theschemafile);
		// Get the encapsulated SAX XMLReader
		XMLReader xmlReader = saxParser.getXMLReader();

		// Set the ContentHandler of the XMLReader
		// xmlReader.setContentHandler(new SAXLocalNameCount());

		// Set an ErrorHandler before parsing
		//xmlReader.setErrorHandler(new MyErrorHandler());

		StringReader XmlStringReader = new StringReader(xmlString);
		InputSource XmlInputSource = new InputSource(XmlStringReader);	
		// Tell the XMLReader to parse the XML document

		xmlReader.parse(XmlInputSource);	
	    }
	    catch(Exception e)
	    {
					dbg.warn("GEN: ### EXCEPTION DURING VALIDATION: ["+e+"]");
  return false;
	    }
	    dbg.debug("GEN: ### DATAFILE SUCCESSFULL VALIDATED WITH SCHEMA: (" + theschemafile.getName() +")");
  return true;
 }




    private static void MakeInputConnection()
    {
   boolean blConnected = false;
   
    while(blConnected == false)
	 {
  if (sgInputType.equals("QUEUE"))
  {
       try
   {
     /*   WQInputQueue = new WebsphereQueue(sgInputServer, sgInputChannel, sgInputQmanager, sgInputQueue, igInputPort, "GET");*/
    blConnected = true;
   }
   catch (Exception e)
   {
    dbg.fatal(e);
    blConnected = false;
   }
      }
      else if (sgInputType.equals("SOCKET"))
      {
       try
   {
   		dbg.debug("new input connection follows: " + sgInputServer + " " + igInputPort + " " + sgInputSockType);
    WQInputQueue = new WebsphereQueue(sgInputServer,igInputPort,sgInputSockType,"GET",igInputCheckInterval,sgInputAllowedHost,igInputTimeout);
    blConnected = true;
   }
   catch (Exception e)
   {
    dbg.fatal(e);
    blConnected = false;
   }
      }
 
  }
    }

    private static void MakeOutputConnection()
    {
   boolean blConnected = false;
    
   while(blConnected == false)
	{
 
   if (sgOutput1Type.equals("QUEUE"))
   {
    try
    {
        //  WQOutput1Queue = new WebsphereQueue(sgOutput1Server, sgOutput1Channel, sgOutput1Qmanager, sgOutput1Queue, igOutput1Port, "PUT");
     blConnected = true;
    }
    catch (Exception e)
    {
     dbg.debug("Exception while connecting to WQOutput1Queue:" + e);
     blConnected = false;
    }
   } 
   else if (sgOutput1Type.equals("SOCKET"))
   {
    try
    {
   		dbg.debug("new output connection follows: " + sgOutput1Server + " " + igOutput1Port + " " + sgOutput1SockType);
     WQOutput1Queue = new WebsphereQueue(sgOutput1Server, igOutput1Port, sgOutput1SockType, "PUT", igOutput1CheckInterval,sgOutput1AllowedHost,igOutput1Timeout);
     blConnected = true;
    }
    catch (Exception e)
    {
     dbg.debug("Exception while connecting to WQOutput1Queue:" + e);
     blConnected = false;
    }
   }
   if (blConnected == false)
   {
    try
    {
     dbg.fatal("Connect failed, now sleep 10 seconds");
     Thread.sleep(10000);
    }
    catch (Exception e)
    {
     dbg.fatal("Sleep failed");
    }
        
   }
  }
		  }
    

   private static void getConfigEntrys()
 {
  Integer myint;
  String tmpString="";
  
  tmpString = readConfigEntry("DATA_COUNT","1");
  myint = new Integer(tmpString);
  igDataCount = myint.intValue();
  tmpString = readConfigEntry("USE_XSLT","YES");
  if (tmpString.equals("YES"))
  {
   bUseXslt = true;
  }
  else
  {
   bUseXslt = false;
  }

  tmpString = readConfigEntry("VALIDATE","YES");
  if (tmpString.equals("YES"))
  {
   bValidate = true;
  }
  else
  {
   bValidate = false;
  }
  sgXsdFile = readConfigEntry("XSD-FILE", "DEFAULT");
  sgXslFile1 = readConfigEntry("XSL-FILE1", "DEFAULT");
  sgXslFile2 = readConfigEntry("XSL-FILE2", "DEFAULT");
		schemafile = new File(sgXsdFile);
		dbg.debug("Schemafile opend");
		stylesheet1 = new File(sgXslFile1);
		dbg.debug("Stylesheetfile1 opend");
  if(!sgXslFile2.equals("DEFAULT"))
  {
 		stylesheet2 = new File(sgXslFile2);
  	dbg.debug("Stylesheetfile2 opened");     
  }
  sgXsdResultCheck1 = readConfigEntry("XSD-RESULT-CHECK1", "DEFAULT");
  if(!sgXsdResultCheck1.equals("DEFAULT"))
  {
 		ResultCheckFile1 = new File(sgXsdResultCheck1);
  	dbg.debug("ResultCheckFile1 opened");     
  }
  sgXsdResultCheck2 = readConfigEntry("XSD-RESULT-CHECK2", "DEFAULT");
  if(!sgXsdResultCheck2.equals("DEFAULT"))
  {
 		ResultCheckFile2 = new File(sgXsdResultCheck2);
  	dbg.debug("ResultCheckFile2 opened");     
  }
  sgDirection = readConfigEntry("DIRECTION", "DEFAULT");
  sgOutgoingHeader = readConfigEntry("OUTGOING-HEADER", "");
  sgOutgoingFooter = readConfigEntry("OUTGOING-FOOTER", "");
  sgInputQueue = readConfigEntry("INPUT-QUEUE", "DEFAULT");
  sgInputQmanager = readConfigEntry("INPUT-QMANAGER", "DEFAULT");
  sgInputChannel = readConfigEntry("INPUT-CHANNEL", "DEFAULT");
  sgInputServer = readConfigEntry("INPUT-SERVER", "DEFAULT");
  sgInputAllowedHost = readConfigEntry("INPUT-ALLOWED-HOST", null);
  sgInputPort = readConfigEntry("INPUT-PORT","1414");
  igInputPort = myint.parseInt(sgInputPort);
  sgInputType = readConfigEntry("INPUT-TYPE","QUEUE");
  sgInputSockType = readConfigEntry("INPUT-SOCKTYPE","SERVER");
	sgInputCheckInterval =  readConfigEntry("INPUT-CHECKINTERVAL","0");
	igInputCheckInterval =  myint.parseInt(sgInputCheckInterval) * 1000;
  sgInputTimeout = readConfigEntry("INPUT-TIMEOUT","120");
  igInputTimeout = myint.parseInt(sgInputTimeout) * 1000;
	dbg.debug("Timeout is " + igInputTimeout);
      
  sgKeepAliveText =  readConfigEntry("KEEPALIVE-TEXT","");
  sgOutput1Queue = readConfigEntry("OUTPUT1-QUEUE", "DEFAULT");
  sgOutput1Qmanager = readConfigEntry("OUTPUT1-QMANAGER", "DEFAULT");
  sgOutput1Channel = readConfigEntry("OUTPUT1-CHANNEL", "DEFAULT");
  sgOutput1Server = readConfigEntry("OUTPUT1-SERVER", "DEFAULT");
  sgOutput1AllowedHost = readConfigEntry("OUTPUT1-ALLOWED-HOST", null);
  sgOutput1Port = readConfigEntry("OUTPUT1-PORT","1414");
  sgOutput1SockType = readConfigEntry("OUTPUT1-SOCKTYPE","SERVER");
  igOutput1Port = myint.parseInt(sgOutput1Port);
	sgOutput1CheckInterval =  readConfigEntry("OUTPUT1-CHECKINTERVAL","0");
	igOutput1CheckInterval =  myint.parseInt(sgOutput1CheckInterval) * 1000;
  sgOutput2Queue = readConfigEntry("OUTPUT2-QUEUE", "DEFAULT");
  sgOutput2Qmanager = readConfigEntry("OUTPUT2-QMANAGER", "DEFAULT");
  sgOutput2Channel = readConfigEntry("OUTPUT2-CHANNEL", "DEFAULT");
  sgOutput2Server = readConfigEntry("OUTPUT2-SERVER", "DEFAULT");
  sgOutput2Port = readConfigEntry("OUTPUT2-PORT","1414");
  igOutput2Port = myint.parseInt(sgOutput2Port);      
	sgOutput2CheckInterval = readConfigEntry("OUTPUT2-CHECKINTERVAL","0");
	igOutput2CheckInterval = myint.parseInt(sgOutput2CheckInterval) * 1000;
  sgWaitNext = readConfigEntry("WAIT-BEFORE-NEXT-MESSAGE", "0");
  sgOutput1Type = readConfigEntry("OUTPUT1-TYPE","DEFAULT");
  sgOutput2Type = readConfigEntry("OUTPUT2-TYPE","DEFAULT");
	sgWaitForConnection = readConfigEntry("WAITFORCONNECTION","120");
  igWaitForConnection = myint.parseInt(sgWaitForConnection) * 1000;      
  sgTimeID = readConfigEntry("TIME_ID","NO");
  sgFormatPatternDate = readConfigEntry("FORMAT_PATTERN_DATE","yyyyMMdd");
  sgFormatPatternTime = readConfigEntry("FORMAT_PATTERN_TIME","ddHHmmss:SSS");
  sgSearchFor = readConfigEntry("SEARCH_FOR","");
  sgReplaceWith = readConfigEntry("REPLACE_WITH","");
	dbg.debug("Input-Type: " + sgInputType);

 }/*end getConfigEntrys*/



    private static String doTransformation(String XMLString, File mystylesheet)
	{
 dbg.debug("GEN: ### NOW TRANSFORMATION WITH STYLESHEET:(" + mystylesheet.getName() + ")");
			dbg.debug("GEN: Message to transform: <" + XMLString + "<");
	    String doTransformResult = "";
	    try
	    {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		//factory.setValidating(true);
		factory.setValidating(false);
		DocumentBuilder builder = factory.newDocumentBuilder();
		//StringReader XmlStringReader = new StringReader(sgXmlReceived);
		StringReader XmlStringReader = new StringReader(XMLString);
		InputSource XmlInputSource = new InputSource(XmlStringReader);
		document = builder.parse(XmlInputSource);
 
		// Use a Transformer for output
		TransformerFactory tFactory = TransformerFactory.newInstance();
		StreamSource stylesource = new StreamSource(mystylesheet);
		Transformer transformer = tFactory.newTransformer(stylesource);
 
		DOMSource source = new DOMSource(document);
		ByteArrayOutputStream XmlByteArrayOutputStream = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(XmlByteArrayOutputStream);
   
		transformer.transform(source, result);
		doTransformResult = XmlByteArrayOutputStream.toString();
   
	    }
	    catch (Exception e)
	    {
		dbg.debug("Exception in doTransformation: " + e);
	    }
	    return doTransformResult;
	}

    private static String readConfigEntry(String Key, String DefaultValue)
	{

	    boolean eof = false;
	    boolean found = false;
	    int ilIndex = 0;
	    String Result = "";
 
	    try
	    {
		FileReader ConfigFile = new FileReader(sgConfigFile);
		BufferedReader ConfigBuffer = new BufferedReader(ConfigFile);

		while (!eof)
		{
		    String ConfigLine = ConfigBuffer.readLine();
   
		    if (ConfigLine == null)
		    {
			eof = true;
		    }
		    else
		    {
			if (ConfigLine.startsWith(Key+"="))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    Result = ConfigLine;
			    found = true;
			}
		    }
		}
	    }
	    catch (Exception e)
	    {
		dbg.warn("ConfigEntry: Exception occured: "+e);
	    }
	    if (found == true)
	    {
		dbg.debug("ConfigEntry: <"+Key+"=> found. Value: <" + Result + ">");
	    }
	    else
	    {
		Result = DefaultValue;
		dbg.debug("ConfigEntry: <"+Key+"=> NOT found. Use defaultvalue: <" + DefaultValue + ">");
	    }
	    return Result;
	}

 static private String doSpecialTransformation(StringBuffer xmlBuffer)
 {
  StringBuffer resultBuffer = new StringBuffer();
  String Cmd = "";
  String Fields = "";
  String Datablock = "";
  String Data="";
  String Adid = "";
  String [] splitFields;
  String [] splitData;
  String [] mysplitData;
  int ilStart = 0;
  int ilEnd = 0;
  int ilTmp = 0;
  boolean blContinue = true;
  
  dbg.debug("++++++++++++++++++++++++++++++ doSpecialTransformation +++++++++++++++++++\n" + xmlBuffer.toString());

  ilStart = xmlBuffer.indexOf("<CMD>") + 5;
  ilEnd = xmlBuffer.indexOf("</CMD>");
  Cmd = xmlBuffer.substring(ilStart,ilEnd);
     //  dbg.debug("CMD: " + Cmd);     

  ilStart = xmlBuffer.indexOf("<FIELDS>") + 8;
  ilEnd = xmlBuffer.indexOf("</FIELDS>");
  Fields = xmlBuffer.substring(ilStart,ilEnd);
 // dbg.debug("FIELDS: " + Fields);
  splitFields = Fields.split(",");



  ilStart = xmlBuffer.indexOf("<DATA>") ;
  ilEnd = xmlBuffer.lastIndexOf("</DATA>")+7;
  Datablock = xmlBuffer.substring(ilStart, ilEnd);
   //    dbg.debug("Datablock: " + Datablock);

  resultBuffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<MSG xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" >\n");
  
  
  ilStart = 0;

  while (blContinue == true && ilStart != -1)
  {
   ilEnd = Datablock.indexOf("</DATA>",ilStart);
   if(ilEnd != -1)
   {
       
    Data = Datablock.substring(ilStart + 6, ilEnd);
    dbg.debug("Data-Line: " + Data);
    mysplitData = Data.split(",");
    splitData = new String[splitFields.length];
    for(int i = 0; i < splitFields.length; i++)
    {
     if(i >= mysplitData.length)
     {
      splitData[i] = "";
     }
     else
     {
      splitData[i] = mysplitData[i]; 
     }     
     if(splitFields[i].equals("ADID")) Adid = splitData[i];
    }

    resultBuffer.append("<FMO>\n");
    if(Cmd.equals("IFR")) resultBuffer.append("<Action>"+Cmd+"</Action>\n");
    if(Cmd.equals("UFR")) resultBuffer.append("<Action>"+Cmd+"</Action>\n");
    if(Cmd.equals("DFR")) resultBuffer.append("<Action>"+Cmd+"</Action>\n");
    for (int x=0; x<splitFields.length; x++)
    {

     if(splitFields[x].equals("URNO")) 
     {
      resultBuffer.append("<URNO>"+splitData[x]+"</URNO>\n");
     }else
     if(splitFields[x].equals("ALC3")) 
     {
      resultBuffer.append("<AirlineOperator>"+splitData[x]+"</AirlineOperator>\n");
     }else
     if(splitFields[x].equals("FLTN")) 
     {
      resultBuffer.append("<FlightNumber>"+splitData[x]+"</FlightNumber>\n");
     }else
     if(splitFields[x].equals("FLNS")) 
     {
      resultBuffer.append("<FlightSuffix>"+splitData[x]+"</FlightSuffix>\n");
     }else
     if(splitFields[x].equals("CSGN")) 
     {
      resultBuffer.append("<CallSign>"+splitData[x]+"</CallSign>\n");
     }else
     if(splitFields[x].equals("ACT5")) 
     {
      resultBuffer.append("<AircraftType>"+splitData[x]+"</AircraftType>\n");
     }else
     if(splitFields[x].equals("TTYP")) 
     {
      resultBuffer.append("<FlightType>"+splitData[x]+"</FlightType>\n");
     }else
     if(splitFields[x].equals("REGN")) 
     {
      resultBuffer.append("<AircraftRegistrationNum>"+splitData[x]+"</AircraftRegistrationNum>\n");
     }else
     if(splitFields[x].equals("ORG4") && (Adid.equals("A") || Adid.equals("B"))) 
     {
      resultBuffer.append("<Origin>"+splitData[x]+"</Origin>\n");
     }else
     if(splitFields[x].equals("DES4") && (Adid.equals("D") || Adid.equals("B"))) 
     {
      resultBuffer.append("<Destination>"+splitData[x]+"</Destination>\n");
     }else     
     if(splitFields[x].equals("ADID")) 
     {
      resultBuffer.append("<Arrive-Depart>"+splitData[x]+"</Arrive-Depart>\n");
     }else
     if(splitFields[x].equals("FTYP")) 
     {
      resultBuffer.append("<Status>"+splitData[x]+"</Status>\n");
     }else
         // if(splitFields[x].equals("STOA") && (Adid.equals("A") || Adid.equals("B"))) 
         // send STOA always 
     if(splitFields[x].equals("STOA")) 
     {
      if(splitData[x].length() == 0)
      {
       resultBuffer.append("<SchArrivalDateTime>\n<SchArrivalDate></SchArrivalDate>\n");
       resultBuffer.append("<SchArrivalTime></SchArrivalTime>\n</SchArrivalDateTime>\n");
      }
      else
      {
       resultBuffer.append("<SchArrivalDateTime>\n<SchArrivalDate>"+splitData[x].substring(0,8)+"</SchArrivalDate>\n");
       resultBuffer.append("<SchArrivalTime>"+splitData[x].substring(8,14)+"</SchArrivalTime>\n</SchArrivalDateTime>\n");
      }
     }else
      if(splitFields[x].equals("ETOA")) // && (Adid.equals("A") || Adid.equals("B"))) 
     {
      if(splitData[x].length() == 0)
      {
       resultBuffer.append("<EstArrivalDateTime>\n<EstArrivalDate></EstArrivalDate>\n");
       resultBuffer.append("<EstArrivalTime></EstArrivalTime>\n</EstArrivalDateTime>\n");
      }
      else
      {
       resultBuffer.append("<EstArrivalDateTime>\n<EstArrivalDate>"+splitData[x].substring(0,8)+"</EstArrivalDate>\n");
       resultBuffer.append("<EstArrivalTime>"+splitData[x].substring(8,14)+"</EstArrivalTime>\n</EstArrivalDateTime>\n");          
      }
     }else
     
      if(splitFields[x].equals("LAND")) // && (Adid.equals("A") || Adid.equals("B"))) 
     {
      if(splitData[x].length() == 0)
      {
       resultBuffer.append("<ActArrivalDateTime>\n<ActArrivalDate></ActArrivalDate>\n");
       resultBuffer.append("<ActArrivalTime></ActArrivalTime>\n</ActArrivalDateTime>\n");
      }
      else
      {
       resultBuffer.append("<ActArrivalDateTime>\n<ActArrivalDate>"+splitData[x].substring(0,8)+"</ActArrivalDate>\n");
       resultBuffer.append("<ActArrivalTime>"+splitData[x].substring(8,14)+"</ActArrivalTime>\n</ActArrivalDateTime>\n");  
      }
     }else
         // if(splitFields[x].equals("STOD") && (Adid.equals("D") || Adid.equals("B"))) 
         // send STOD always 
     if(splitFields[x].equals("STOD")) 
     {
      if(splitData[x].length() == 0)
      {
       resultBuffer.append("<SchDepartDateTime>\n<SchDepartDate></SchDepartDate>\n");
       resultBuffer.append("<SchDepartTime></SchDepartTime>\n</SchDepartDateTime>\n");
      }
      else
      {
       resultBuffer.append("<SchDepartDateTime>\n<SchDepartDate>"+splitData[x].substring(0,8)+"</SchDepartDate>\n");
       resultBuffer.append("<SchDepartTime>"+splitData[x].substring(8,14)+"</SchDepartTime>\n</SchDepartDateTime>\n");
      }
     }else
      if(splitFields[x].equals("ETOD") && (Adid.equals("D") || Adid.equals("B"))) 
     {
      if(splitData[x].length() == 0)
      {
       resultBuffer.append("<EstDepartDateTime>\n<EstDepartDate></EstDepartDate>\n");
       resultBuffer.append("<EstDepartTime></EstDepartTime>\n</EstDepartDateTime>\n");
      }
      else
      {
       resultBuffer.append("<EstDepartDateTime>\n<EstDepartDate>"+splitData[x].substring(0,8)+"</EstDepartDate>\n");
       resultBuffer.append("<EstDepartTime>"+splitData[x].substring(8,14)+"</EstDepartTime>\n</EstDepartDateTime>\n");       
      }
     }else
     if(splitFields[x].equals("ETDI") && (Adid.equals("A")))
     {
      if(splitData[x].length() == 0)
      {
       resultBuffer.append("<EstDepartDateTime>\n<EstDepartDate></EstDepartDate>\n");
       resultBuffer.append("<EstDepartTime></EstDepartTime>\n</EstDepartDateTime>\n");
      }
      else
      {
       resultBuffer.append("<EstDepartDateTime>\n<EstDepartDate>"+splitData[x].substring(0,8)+"</EstDepartDate>\n");
       resultBuffer.append("<EstDepartTime>"+splitData[x].substring(8,14)+"</EstDepartTime>\n</EstDepartDateTime>\n");       
      }
     }else
 
     
      if(splitFields[x].equals("AIRB")) // && (Adid.equals("D") || Adid.equals("B"))) 
     {
      if(splitData[x].length() == 0)
      {
       resultBuffer.append("<ActDepartDateTime>\n<ActDepartDate></ActDepartDate>\n");
       resultBuffer.append("<ActDepartTime></ActDepartTime>\n</ActDepartDateTime>\n");
      }
      else
      {
       resultBuffer.append("<ActDepartDateTime>\n<ActDepartDate>"+splitData[x].substring(0,8)+"</ActDepartDate>\n");
       resultBuffer.append("<ActDepartTime>"+splitData[x].substring(8,14)+"</ActDepartTime>\n</ActDepartDateTime>\n");
      }
     }
     
     
    }
    resultBuffer.append("</FMO>\n");
   }

   if (ilEnd == -1)
   {
    blContinue = false;
   }
   ilStart = Datablock.indexOf("<DATA>",ilEnd);
   
  } //end of While
         
  
  resultBuffer.append("</MSG>\n");
  return resultBuffer.toString();
 }

    // Error handler to report errors and warnings
    private static class MyErrorHandler implements ErrorHandler 
    {
	/** Error handler output goes here */
	private PrintStream out;

	MyErrorHandler() 
	    {
	
	    }

	/**
	 * Returns a string describing parse exception details
	 */
	private String getParseExceptionInfo(SAXParseException spe) 
	    {
		String systemId = spe.getSystemId();
		if (systemId == null) 
		{
		    systemId = "null";
		}
		String info = "URI=" + systemId +
		    " Line=" + spe.getLineNumber() +
		    ": " + spe.getMessage();
		return info;
	    }

	// The following methods are standard SAX ErrorHandler methods.
	// See SAX documentation for more info.

	public void warning(SAXParseException spe) throws SAXException 
	    {
		dbg.warn("Warning: " + getParseExceptionInfo(spe));
	    }
	    
	public void error(SAXParseException spe) throws SAXException 
	    {
		String message = "Error: " + getParseExceptionInfo(spe);
		dbg.error(message);
	    }

	public void fatalError(SAXParseException spe) throws SAXException 
	    {
		String message = "Fatal Error: " + getParseExceptionInfo(spe);
		dbg.fatal(message);
	    }
    } // end of class MyErrorHandler



} // end of class jxmlif



