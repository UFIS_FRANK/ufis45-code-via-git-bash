#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Lis/LIS_Server/Base/Server/Interface/twrsim.c 1.2 2004/04/02 20:12:22SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*
 * ABB/AAT Project ATC Lisbon (Tower Handler)
 *
 * tower simulator program  -- j.heilig Aug 22 01
 *
 * Sept 06 01: added mcact join and send interface name options -ji and -si
 * Sept 09 01: added SIGUSR2 support to enter a twrcom message from stdin
 *
 *
 * Usage: twrsim [options] addres port [file]
 *
 * Options: -lo logopts -lf logfile -sp sendport -ji ifname -si ifname
 *
 * This program can be used to simulate the tower part of the TOWER-AOS system,
 * in particular to test the UFIS tower handler as follows:
 *
 * A multicasting address (group), a port and an input file are specified
 * in the command line. twrsim creates a datagram socket, binds it to that
 * socket address and starts to read messages from the specified file and
 * to send them via the datagram socket. The read and send rate is given
 * by the time interval of the tid_msg timer (currently 1500 msec). At the
 * same time, the heartbeat timer (tid_hb, currently 5000 msec) causes
 * heartbeat messages to be generated and sent periodically. A timer on
 * send datagram timeouts is also installed but not yet fully implemented.
 *
 * The interface names for receiving (ji) and sending (si) interfaces
 * can be given. Both default to NULL which means a system default.
 *
 * Additional actions can be controlled by signals: sending SIGUSR1 to the
 * program toggles the sending of START LINK and STOP LINK frames,
 * respectively. SIGTERM causes the program to free all resources and
 * terminate. SIGUSR2 causes a message to be read from stdin and sent to
 * twrcom. For the duration of entering the message all other program
 * activities are suspended so that this feature can be used to simulate
 * tower communication failures (offline states). Or otherwise the message
 * should be entered pretty quickly.
 *
 * Reaching end of file in the input file the program terminates. 
 * 
 * It is allowed to omit the input file name. In this case the program
 * only sends the heartbeat and the link control frames.
 * 
 * In both cases, the specified socket is monitored for incoming messages.
 * Received messages and those being sent are displayed if the "-lo" option
 * has bit 3 set (-lo 8). More debugging messages can be obtained by specifying
 * "-lo 13" (see nat.h loh options for logging features). The log file defaults
 * to stderr.
 *
 * The option "-so" can be used to cause the creation of a separate datagram
 * socket to send messages. In this case, the option MC_LOOPBACK is set to
 * make the system to loop back messages to the sending host. In contrast to
 * the receiving socket, MC_BIND is not set for this socket to allow running
 * two instances of the twrsim on the same host and communicating with each
 * other. This can be considered a rudimentary self test feature but it also
 * shows how the twrcom program (the tower handler) should be built to allow
 * running it on the same host for test purposes.
 *
 * BACKGROUND: multicast sockets must be bound to allow them receiving of
 * datagrams (which would otherwise be discarded in the IP layer). But you
 * can't have two programs on the same host binding sockets to the same
 * socket address (internet address:port number).
 * 
 * Using the -sp option and omitting the input file allows combining two 
 * twrsim programs to a single host test environment as follows:
 *
 * twrsim -lo 34 -sp 9998 224.0.1.1 9999 messages.txt (1)
 *
 * twrsim -lo 34 -sp 9999 224.0.1.1 9998              (2)
 *
 * In the above configuration (1) sends heartbeat and data frames while
 * (2) sends heartbeat frames only (both can be made to send link control
 * frames by sending SIGUSR1 to them) (the log option 34 causes the display
 * of trace logs including PID). (1) receives messages from (2) and vica
 * versa. Any other programs on any other hosts would be participated in
 * this communication by joining the same multicast group and binding to
 * the send port number of the desired twrsim.
 *
 * A note on ASSERT statements: this is a test program. It is not only meant
 * to test the tower handler but also to test the NAT library and the
 * application logic. This is mainly achieved by applying lots of ASSERT
 * macros (see nat.h and man assert). These macros cause the program to
 * terminate with an "assertion failed" message if an illegal program
 * state occurs.
 *
 * The program control is fully asynchronous. It is based on the philosophy
 * of the NAT library defining (adding) event sources like the expiration of
 * timers, receiving signals and detecting file descriptors to be ready for
 * read/write. The NAT event handler calls back the functions specified by
 * the application to process the events.
 *
 * To be implemented yet:
 *
 * 1) interpreting received link control messages
 * 2) processing missing heartbeat frames
 * 3) timeout on sending messages (not expexted with datagrams)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "nat.h"
#include "twrdat.h"

/*
 * event handler callbacks
 */

static int timer_cb(void *);
static int input_cb(int, void *);
static int output_cb(int, void *);
static int signal_cb(int, void *);

/*
 * utilities
 */

static void usage(void);
static void post_message(char *, int );
static void post_dataframe(char *);
static int get_msgline(char *, int );

/*
 * program variables
 */

static int status;
static EHTimerId tid_msg, tid_hb, tid_to;

#define RUN 		1
#define STOP	 	2
#define PAUSE		3

static MCSocket *mcsock, *mcsendsock;
static FILE *msgfp;
static EHOutputId oid_com = -1;
static BlockFIFO *output_fifo;
static unsigned int msg_seq = 0;

int main(int argc, char **argv)
{
int ax, mcport, logopts, sendport;
EHInputId iid;
EHSignalId sid_term, sid_usr1, sid_usr2;
char *mcaddr, *msgfile, *logfile;
char *jifname, *sifname;

/*
 * Get parms and options
 */
	logopts = 0;
	logfile = NULL;
	sendport = -1;
	jifname = sifname = NULL;
/*
 * options
 */
	for(ax=1; ax < argc && *(argv[ax]) == '-'; ++ax)
	{
		if(strcmp(argv[ax],"-lo") == 0)
		{
			++ax;
			if(ax == argc || (sscanf(argv[ax],"0x%x", &logopts) != 1 &&
         sscanf(argv[ax],"%d", &logopts) != 1 &&
			   sscanf(argv[ax],"x%x", &logopts) != 1 ))
			{
				fprintf(stderr,"bad or missing logopts spec\n");
				exit(1);
			}
		}
		else if(strcmp(argv[ax],"-lf") == 0)
		{
			++ax;
			if(ax == argc)
			{
				fprintf(stderr,"logfile name expected\n");
				exit(1);
			}
			logfile = argv[ax];
		}
		else if(strcmp(argv[ax],"-ji") == 0)
		{
			++ax;
			if(ax == argc)
			{
				fprintf(stderr,"interface name expected\n");
				exit(1);
			}
			jifname = argv[ax];
		}
		else if(strcmp(argv[ax],"-si") == 0)
		{
			++ax;
			if(ax == argc)
			{
				fprintf(stderr,"interface name expected\n");
				exit(1);
			}
			sifname = argv[ax];
		}
		else if(strcmp(argv[ax],"-sp") == 0)
		{
			++ax;
			if(ax == argc || sscanf(argv[ax],"%d", &sendport) != 1 || sendport <= 0)
			{
				fprintf(stderr,"bad or missing sendport spec\n");
				exit(1);
			}
		}
		else
		{
			usage();
		}
	}
/*
 * mandatory parms
 */
	if(ax + 1 >= argc) /* 2 mandatory and one optional parms expexted */
	{
		usage();
	}

	mcaddr = argv[ax];

 	if(sscanf(argv[ax+1],"%d",&mcport) != 1 || mcport <= 0)
	{
		fprintf(stderr,"bad port spec\n");
		exit(1);
	}

	if(ax+2 < argc)
	{
		msgfile = argv[ax+2];
		if((msgfp = fopen(msgfile,"r")) == NULL)
		{
			perror(msgfile);
			exit(2);
		}
	}
	else
	{
		msgfile = NULL;
		msgfp = NULL;
	}

/*
 * set up logging and create multicating socket(s)
 */

	NatSetLog(logopts, logfile, "twrsim:");
	LOG(TRCLOG,"mc addr=%s\nmc port=%d\nsendport=%d\nmsg file=%s",
              mcaddr, mcport, sendport, STR0(msgfile));

	if((mcsock = MCSocket_Create(mcaddr, jifname, mcport, MC_BIND)) == NULL)
	{
		LOG(ERRLOG,"MCSocket error, errno=%d text=%s\n",ERRNO, ERRTEXT);
		exit(3);
	}

/*
 * need a separate send socket?
 */
	if(sendport > 0)
	{
		if((mcsendsock = MCSocket_Create(mcaddr,NULL,sendport,MC_LOOPBACK)) == NULL)
		{
			LOG(ERRLOG,"MCSocket error, errno=%d text=%s\n",ERRNO, ERRTEXT);
			exit(3);
		}
	}
	else
	{
		mcsendsock = mcsock;
	}
/*
 * another interface for the send socket?
 */
	if(sifname && MCSocket_SetIf(mcsendsock,sifname) == -1)
	{
		LOG(ERRLOG,"MCSocket_SetIf error, errno=%d text=%s\n",ERRNO, ERRTEXT);
		exit(4);
	}
/*
 * Initialize event handling and add event sources
 */
	ASSERT(EHInitialize() != -1);
	if(msgfp)
	{
		tid_msg = EHAddTimer(1500, timer_cb, (void *) 1);
		ASSERT(tid_msg != -1);
	}
	tid_hb = EHAddTimer(5000, timer_cb, (void *) 2);
	ASSERT(tid_hb != -1);
	tid_to = EHAddTimer(10000, timer_cb, (void *) 3);
	ASSERT(tid_to != -1);
	iid = EHAddInput(MCSocket_GetSock(mcsock), input_cb, NULL);
	ASSERT(iid != -1);
	sid_term = EHAddSignal(SIGTERM, signal_cb, NULL);
	ASSERT(sid_term != -1);
	sid_usr1 = EHAddSignal(SIGUSR1, signal_cb, NULL);
	ASSERT(sid_usr1 != -1);
	sid_usr2 = EHAddSignal(SIGUSR2, signal_cb, NULL);
	ASSERT(sid_usr2 != -1);

	output_fifo = BlockFIFO_Create();
	
/*
 * handle events until state changes to STOP
 */
	status = RUN;
	do
	{
		EHHandleEvents(0);
	} while(status != STOP);
/*
 * release resources and exit
 */
	BlockFIFO_Destroy(output_fifo);

	ASSERT(EHRemoveInput(iid) != -1);
	ASSERT(EHRemoveSignal(sid_term) != -1);
	ASSERT(EHRemoveSignal(sid_usr1) != -1);
	ASSERT(EHRemoveSignal(sid_usr2) != -1);
	ASSERT(EHRemoveTimer(tid_hb) != -1);
	ASSERT(EHRemoveTimer(tid_to) != -1);
	if(oid_com != -1)
		ASSERT(EHRemoveOutput(oid_com) != -1);

	if(msgfp)
	{
		ASSERT(EHRemoveTimer(tid_msg) != -1);
		fclose(msgfp);
	}
	ASSERT(MCSocket_Destroy(mcsock) != -1);
	if(mcsendsock != mcsock)
		ASSERT(MCSocket_Destroy(mcsendsock) != -1);
	return 0;
}

void usage()
{
	fprintf(stderr,"twrsim [options] mc-address mc-port [msg-file]\n");
	fprintf(stderr,"options: -lo logopts -lf logfile -sp sendport");
	exit(1);
}

/*
 * called by the event handler if any of the timers has expired
 */

int timer_cb(void *p)
{
int fct, len;
char buff[1500];
	fct = (int) p;
	switch(fct)
	{
		case 1:
			LOG(DBGLOG,"msg timer");
			if(status != RUN)
				break;
			if((len=get_msgline(buff, sizeof(buff))) == -1)
			{
#ifdef STOP_ON_EOF
				LOG(TRCLOG,"EOF in message file -- terminating");
				status = STOP;
#else
				LOG(TRCLOG,"EOF in message file -- continuing with HB");
				fclose(msgfp);
				msgfp = NULL;
				ASSERT(EHRemoveTimer(tid_msg) != -1);
#endif
			}
			else if(len>= 2 && buff[0] != '#' && (buff[0] != '/' || buff[1] != '/'))
			{
				post_dataframe(buff);
			}
			break;
		case 2:
			LOG(DBGLOG,"heartbeat timer");
			if(status != RUN)
				break;
			sprintf(buff, "HB 0x%u", msg_seq);
			post_message(buff, -1);
			break;
		case 3:
			LOG(DBGLOG,"write timeout timer");
			break;
		default:
			ASSERT(0);
	}
	return 0;
}

/*
 * called if a datagram can be read without blocking
 */

int input_cb(int s, void *p)
{
char buff[1501];
int n;
	LOG(DBGLOG,"input_cb");
	n = MCSocket_Read(mcsock, buff, 1500);
	ASSERT(n > 0);
	buff[n] = '\0';
	LOG(TRCLOG,"RECEIVED %d byte(s) '%s'", n, buff);
	return 0;
}

/*
 * called if a datagram can be written without blocking
 */

int output_cb(int s, void *p)
{
void *data;
size_t len;
	LOG(DBGLOG,"output_cb");
	ASSERT(BlockFIFO_Count(output_fifo));
	data = BlockFIFO_Get(output_fifo, &len);
	ASSERT(data);
	ASSERT(len);
	LOG(TRCLOG,"SENDING '%s'", data);
	ASSERT(MCSocket_Write(mcsendsock, data, len) == len);
	LOG(DBGLOG,"SENT.");
	ASSERT(EHResetTimer(tid_to)!=-1);
	free(data);
	if(BlockFIFO_Count(output_fifo) == 0)
	{
		ASSERT(EHRemoveOutput(oid_com)!=-1);
		oid_com = -1;
	}
	return 0;
}
	
/*
 * called if the event handler detects a signal sent to the program
 */

int signal_cb(int s, void *p)
{
static int start_stop = 0;
char *m;
char buff[1000];
	if(s == SIGTERM)
	{
		LOG(TRCLOG,"CAUGHT SIGTERM -- terminating");
		status = STOP;
	}
	else if(s == SIGUSR1)
	{
		if(start_stop)
		{
			m = "STOP LINK";
			start_stop = 0;
		}	
		else
		{
			m = "START LINK";
			start_stop = 1;
		}
		LOG(TRCLOG,"CAUGHT SIGUSR1 -- send %s", m);
		post_message(m,0);
	}
	else if(s == SIGUSR2)
	{
		printf("ENTER MESSAGE:");
		fflush(stdout);
		if(gets(buff) != NULL && buff[0] != '\0')
		{
			post_dataframe(buff);
		}
	}
	else
	{
		ASSERT(0);
	}
	return 0;
}

/*
 * save message and wait for being able to send it
 */

void post_message(char *buff, int len)
{
	if(oid_com == -1)
		oid_com=EHAddOutput(MCSocket_GetSock(mcsendsock),output_cb,NULL);
	ASSERT(oid_com !=-1 );
	if(len <= 0)
		len = strlen(buff) + 1;
	ASSERT(BlockFIFO_Put(output_fifo, buff, len)!=-1);
}

/*
 * read one line from the message file
 */

int get_msgline(char *buff, int len)
{
	ASSERT(len>0);
	if(fgets(buff, len, msgfp) == NULL)
		return -1;
	buff[len-1] = '\0';
	len = strlen(buff);
	if(len >= 2 && buff[len-2] == '\r' && buff[len-1] == '\n')
	{
		buff[len-2] = '\0';
		len -= 2;
	}
	else if(len >= 1 && buff[len-1] == '\n')
	{
		buff[len-1] = '\0';
		len -= 1;
	}
	return len + 1; /* incl \0 */
}

/*
 * if it's a data frame, save the msg_sequence number for the heartbeat
 */
void post_dataframe(char *buff)
{
TWRD_MESSAGE *msg;
unsigned long seq;
	LOG(TRCLOG,"Buff for TWRDDecode <%s>",buff);
		if((msg = TWRDDecode(buff)) != NULL)
		{
			ASSERT(TWRD_GET(msg, msg_seq, &seq) == 1);
			msg_seq = seq;
			free(msg);
		}
		post_message(buff,0);
}
