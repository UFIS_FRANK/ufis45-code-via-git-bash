rem Copies all generated files from the SCM-Structue into the Install-Shield directory.


rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:

rem Copy ini- and config-files
copy c:\ufis_bin\ceda.ini %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\bdps_sec.cfg %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\kiv_build\ufis_client_appl\system\*.*

rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\kiv_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\kiv_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\kiv_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\kiv_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\kiv_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\kiv_build\ufis_client_appl\applications\*.*

REM copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\kiv_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\kiv_build\ufis_client_appl\system\*.*

rem copy interop dll's
rem copy c:\ufis_bin\release\*lib.dll %drive%\kiv_build\ufis_client_appl\applications\interopdll\*.*
rem copy c:\ufis_bin\release\ufis.utils.dll %drive%\kiv_build\ufis_client_appl\applications\interopdll\*.*

rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\kiv_build\ufis_client_appl\help\*.*