#if !defined(AFX_PSTRULEDLG_H__BDF1B1D2_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_)
#define AFX_PSTRULEDLG_H__BDF1B1D2_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PstRuleDlg.h : header file
//

#include <CedaPstData.h>
#include <CCSButtonCtrl.h>
/////////////////////////////////////////////////////////////////////////////
// PstRuleDlg dialog

class PstRuleDlg : public CDialog
{
// Construction
public:
	PstRuleDlg(CWnd* pParent, PSTDATA *popPst);   // standard constructor
	PstRuleDlg();

	PSTDATA *pomPst;
// Dialog Data
	//{{AFX_DATA(PstRuleDlg)
	enum { IDD = IDD_PST_RULES };
	CComboBox	m_Gt_Unit2;
	CComboBox	m_Gt_Unit1;
	CEdit	m_Gt_Value1Ctrl;
	CEdit	m_Gt_Value2Ctrl;
	CEdit	m_MaxPaxCtrl;
	CEdit	m_MinPaxCtrl;
	CButton	m_Gt_AndCtrl;
	CEdit	m_SequenceCtrl;
	CButton	m_Matrix1;
	CString	m_AL_List;
	CString	m_Exce;
	CString	m_PDest;
	CString m_sPst_Regn;		// 050228 MVy: Aircraft Registrations IDC_PST_RULES_ACR_REGN
	CString m_sPst_Flno;		// 050228 MVy: Aircraft Registrations IDC_PST_RULES_ACR_REGN
	CString	m_Heig_Max;
	CString	m_Heig_Min;
	CString	m_Leng_Max;
	CString	m_Leng_Min;
	CString	m_Max_Ac;
	CString	m_Nat_Restr;
	CString	m_Npos1;
	CString	m_Npos2;
	CString	m_Npos3;
	CString	m_Npos4;
	CString	m_Npos5;
	CString	m_Npos6;
	CString	m_Npos7;
	CString	m_Npos8;
	CString	m_Npos9;
	CString	m_Npos10;
	CString	m_Prio;
	CString	m_Restr_To1;
	CString	m_Restr_To2;
	CString	m_Restr_To3;
	CString	m_Restr_To4;
	CString	m_Restr_To5;
	CString	m_Restr_To6;
	CString	m_Restr_To7;
	CString	m_Restr_To8;
	CString	m_Restr_To9;
	CString	m_Restr_To10;
	CString	m_Span_Max;
	CString	m_Span_Min;
	int	m_Sequence;
	int		m_Gt_Value1;
	int		m_Gt_Value2;
	CString	m_Gt_String;
	CString	m_STYP_List;
	CString	m_FltiArr;
	CString	m_FltiDep;
	CString m_ACGroup;
	CString m_MinPax;
	CString m_MaxPax;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PstRuleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	BOOL CheckRegistrations( CString *psMessage = 0 );		// 050228 MVy: check the values in the registrations edit field

	// Generated message map functions
	//{{AFX_MSG(PstRuleDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnMatrix1();
	afx_msg void OnMatrix2();
	afx_msg void OnMatrix3();
	afx_msg void OnMatrix4();
	afx_msg void OnMatrix5();
	afx_msg void OnMatrix6();
	afx_msg void OnMatrix7();
	afx_msg void OnMatrix8();
	afx_msg void OnMatrix9();
	afx_msg void OnMatrix10();
	afx_msg void OnSelchangeComboGtUnit1();
	afx_msg void OnCheckGtAnd();
	afx_msg void OnSelchangeComboGtUnit2();
	afx_msg void OnALMatrix1();
	afx_msg void OnALMatrix2();
	afx_msg void OnALMatrix3();
	afx_msg void OnALMatrix4();
	afx_msg void OnALMatrix5();
	afx_msg void OnALMatrix6();
	afx_msg void OnALMatrix7();
	afx_msg void OnALMatrix8();
	afx_msg void OnALMatrix9();
	afx_msg void OnALMatrix10();
	afx_msg void OnAPMatrix1();
	afx_msg void OnAPMatrix2();
	afx_msg void OnAPMatrix3();
	afx_msg void OnAPMatrix4();
	afx_msg void OnAPMatrix5();
	afx_msg void OnAPMatrix6();
	afx_msg void OnAPMatrix7();
	afx_msg void OnAPMatrix8();
	afx_msg void OnAPMatrix9();
	afx_msg void OnAPMatrix10();
	afx_msg void OnACGroupSel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	DECLARE_SERIAL(PstRuleDlg)
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSTRULEDLG_H__BDF1B1D2_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_)
