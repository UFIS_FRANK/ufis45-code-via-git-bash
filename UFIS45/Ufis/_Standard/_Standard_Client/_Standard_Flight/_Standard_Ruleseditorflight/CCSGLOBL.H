// CCSGlobl.h: interface for the CCSGlobl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
#define AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_

#ifndef __CCSGLOBL_H__
#define __CCSGLOBL_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <CCSDefines.h>
#include <CCSTime.h>

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section

CString GetString(UINT ID);

class CCSBcHandle;
class CCSCedaCom;
class CCSDdx;
class CCSLog;
class CedaCfgData;
class CCSBcHandle;
class CBasicData;
class CedaBasicData;
class PrivList;

extern CCSBcHandle ogBcHandle;
extern CCSCedaCom ogCommHandler;  // The one and only CedaCom object
extern CCSDdx ogDdx;
extern CCSLog ogLog;
extern CBasicData ogBasicData;
extern CedaCfgData ogCfgData;
extern char pcgTableExt[10];

extern const char *pcgAppName;
extern CedaBasicData ogBCD;
extern CString ogFltiPrio;

extern PrivList ogPrivList;

enum enumColorIndexes
{
	GRAY_IDX=2,GREEN_IDX,RED_IDX,BLUE_IDX,SILVER_IDX,MAROON_IDX,
	OLIVE_IDX,NAVY_IDX,PURPLE_IDX,TEAL_IDX,LIME_IDX,
	YELLOW_IDX,FUCHSIA_IDX,AQUA_IDX, WHITE_IDX,BLACK_IDX,ORANGE_IDX
};



enum DLG_ACTION 
{ 
    DLG_INSERT,DLG_UPDATE
   
};  

//enum enumBarType{BAR_FLIGHT,BAR_SPECIAL,BKBAR,GEDBAR,BAR_BREAK,BAR_ABSENT,BAR_SHADOW};





#define MAXCOLORS 64
#define FIRSTCONFLICTCOLOR 31

// Symbolic colors (helper constants for CGateDiagramDialog -- testing purpose only)
#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
#define SILVER  RGB(192, 192, 192)          // light gray
#define RED     RGB(255,   0,   0)
#define LTYELLOW  RGB(255, 255,   160)
#define ORANGE  RGB(255, 132,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255,   0)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(  0, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];
extern CString ogAppName;

extern COLORREF lgBkColor;
extern COLORREF lgTextColor;
extern COLORREF lgHilightColor;

extern enum enumRecordState egRecordState;

extern BOOL bgIsInitialized;

// CFont : translate from logical point to physical pixel
#define PT_TO_PIXELS(dc, pt)    (- MulDiv( pt, (dc).GetDeviceCaps( LOGPIXELSY ), 72 ))


/////////////////////////////////////////////////////////////////////////////
// IDs


#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_INPLACEEDIT		0x400c


/////////////////////////////////////////////////////////////////////////////
// Messages


// Message and constants which are used to handshake viewer and the attached diagram
#define WM_POSITIONCHILD				(WM_USER + 201)
#define WM_UPDATEDIAGRAM				(WM_USER + 202)
#define UD_UPDATEGROUP                  (WM_USER + 203)
#define UD_DELETELINE                   (WM_USER + 204)
#define UD_DELETEGROUP                  (WM_USER + 205)
#define UD_INSERTLINE                   (WM_USER + 206)
#define UD_INSERTGROUP                  (WM_USER + 207)
#define UD_UPDATELINEHEIGHT             (WM_USER + 208)
#define UD_UPDATELINE					(WM_USER + 209)
#define UD_UPDATEDLG					(WM_USER + 210)
#define WM_REASSIGNFINISHED				(WM_USER + 211)
#define WM_ATTENTIONSETCOLOR			(WM_USER + 212)
#define UD_REASSIGNFINISHED				(WM_USER + 212)
#define WM_SETRELEASE				(WM_USER + 216)
#define WM_RESETRELEASE				(WM_USER + 217)
#define UD_RESETCONTENT				(WM_USER + 220)



/////////////////////////////////////////////////////////////////////////////
// Font variable
extern CFont ogSmallFonts_Regular_6;
extern CFont ogSmallFonts_Regular_7;
extern CFont ogSmallFonts_Regular_8;
extern CFont ogSmallFonts_Bold_7;
extern CFont ogMSSansSerif_Regular_8;
extern CFont ogMSSansSerif_Bold_8;
extern CFont ogCourier_Bold_10;
extern CFont ogCourier_Regular_10;
extern CFont ogCourier_Regular_8;
extern CFont ogCourier_Bold_8;
extern CFont ogCourier_Regular_9;

extern CFont ogTimesNewRoman_9;
extern CFont ogTimesNewRoman_12;
extern CFont ogTimesNewRoman_16;
extern CFont ogTimesNewRoman_30;

extern CFont ogScalingFonts[30];
extern int igFontIndex1;
extern int igFontIndex2;

extern bool bgDoesFidsRemaExist;
extern bool IsRemaExist;
extern bool IsAdjacentGatRestriction;
extern bool bgUseMultiAirport;

void InitFont();
void DeleteBrushes();
void CreateBrushes();


enum{MS_SANS6, MS_SANS8, MS_SANS12, MS_SANS16, 
     MS_SANS6BOLD, MS_SANS8BOLD, MS_SANS12BOLD, MS_SANS16BOLD};

struct FONT_INDEXES
{
	int VerticalScale;
	int Chart;
	FONT_INDEXES(void)
	{VerticalScale=0;Chart=0;}
};


/////////////////////////////////////////////////////////////////////////////
// Color and brush variables

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];

/////////////////////////////////////////////////////////////////////////////



struct TIMEFRAMEDATA
{
	CTime StartTime;
	CTime EndTime;
	TIMEFRAMEDATA(void)
	{StartTime=TIMENULL;EndTime=TIMENULL;}
};


/////////////////////////////////////////////////////////////////////////////
// application globals 
extern char pcgHome[4];
extern bool bgNoScroll;
extern bool bgOnline;
extern bool bgIsButtonListMovable;

class CInitialLoadDlg;
extern CInitialLoadDlg *pogInitialLoad;

/////////////////////////////////////////////////////////////////////////////
// IDs

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "tscall.cpp" by Pichate May 08,96 18:15
#define IDC_TIMESCALE       0x4001

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "vscale.cpp" by Pichate May 08,96 18:15
#define IDC_VERTICALSCALE   0x4002

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "gbar.cpp" by Pichate May 08,96 18:15
#define IDC_GANTTBAR        0x4003

//#define IDD_DIAGRAM         0x4004
#define IDD_CHART           0x4005
#define IDC_CHARTBUTTON     0x4006
#define IDD_GANTT           0x4007

#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_ARRIVAL         0x400a
#define IDC_DEPARTURE       0x400b
#define IDC_INPLACEEDIT		0x400c

#define IDD_GANTTCHART      0x4101


/////////////////////////////////////////////////////////////////////////////
// Messages

// Message sent from PrePlanTable to the parent window when closed
// Id 24-Sep-96
// Fix some bug here, since CCI diagram and Gate diagram shares the same message number.
// This surely will crash the machine or destroy our memory blocks if the user terminate
// the diagram with Alt-F4.
#define WM_STAFFTABLE_EXIT          (WM_APP + 20)	/* tables */
#define WM_GEQTABLE_EXIT            (WM_APP + 21)	/* tables */
#define WM_INFTABLE_EXIT            (WM_APP + 22)	/* tables */
#define WM_HWDIA_EXIT				(WM_APP + 23)	/* tables */
#define WM_FLIGHTDIA_EXIT           (WM_APP + 24)	/* tables */
#define WM_STAFFDIA_EXIT            (WM_APP + 25)	/* tables */
#define WM_CONFTABLE_EXIT           (WM_APP + 26)	/* tables */
#define WM_FLIGHTTABLE_EXIT         (WM_APP + 27)	/* tables */
#define WM_MAGNETICTABLE_EXIT       (WM_APP + 28)	/* tables */
#define WM_ATTENTIONTABLE_EXIT      (WM_APP + 29)	/* tables */
#define WM_CONFLICTTABLE_EXIT       (WM_APP + 30)	/* tables */
#define WM_LOCKWINDOWUPDATE         (WM_APP + 31)	/* tables */
#define WM_ZOLLTABLE_EXIT			(WM_APP + 32)	/* tables */


#define GRID_MESSAGE_ENDEDITING			(WM_USER + 222)
#define GRID_MESSAGE_BUTTONCLICK		(WM_USER + 223)
#define GRID_MESSAGE_CELLCLICK			(WM_USER + 224)
#define GRID_MESSAGE_DOUBLECLICK		(WM_USER + 225)
#define GRID_ACTCELLMOVED		        (WM_USER + 226)



/////////////////////////////////////////////////////////////////////////////
// Drag Information Type

enum 
{
	DIT_AFLIGHT,	// source: Flugdatenansicht
	DIT_DFLIGHT,		// source: Flugdatenansicht
	DIT_GHSLIST,				// source: GhsList Target PremisPageGantt
	DIT_ANSICHT_GRP,			// source: Geometrie Gruppen Table targt dito
	DIT_DUTYFROMGANTT,			// Drop from this gantt to another point
	DIT_DUTYFROMTABLE,			// Drop from NoResourceList
	DIT_DUTYFROMDETAIL,			// Drop from Detail-Gantt of Rotatio-Diagram
	DIT_EXCHANGE_JOBS,			// Source: Gantt Destination: Gantt
	DIT_BASIC_SHIFT_FOR_DEMAND, // Drag from Coverage Basic-Shift-Gantt
	DIT_POS_GANTT,
	DIT_SHIFTROSTER_BSD_TABLE,	// Drag from Shiftroster BSD Table
	DIT_DUTYROSTER_BSD_TABLE	// Drag from Dutyroster BSD Table
};

// DIT 0 - 49



enum
{
	DLG_NEW,
	DLG_COPY,
	DLG_CHANGE
};


enum enumDDXTypes
{
    UNDO_CHANGE, 
    BC_CFG_INSERT, BC_CFG_CHANGE, CFG_CHANGE, CFG_INSERT,CFG_DELETE,
	BC_GHS_NEW, BC_GHS_DELETE, BC_GHS_CHANGE,
	GHS_NEW, GHS_DELETE, GHS_CHANGE,
	BC_GPM_NEW, BC_GPM_DELETE, BC_GPM_CHANGE,
	GPM_NEW, GPM_DELETE, GPM_CHANGE,
	BC_GHP_NEW, BC_GHP_DELETE, BC_GHP_CHANGE,
	BC_GRM_NEW, BC_GRM_DELETE, BC_GRM_CHANGE,
	GRM_NEW, GRM_DELETE, GRM_CHANGE,
	BC_GRN_NEW, BC_GRN_DELETE, BC_GRN_CHANGE,
	GRN_NEW, GRN_DELETE, GRN_CHANGE,
	GHP_NEW, GHP_CHANGE, GHP_DELETE,
	BC_TEA_CHANGE,BC_TEA_NEW,BC_TEA_DELETE,
	TEA_CHANGE,TEA_NEW,TEA_DELETE,
	BC_BSD_CHANGE,BC_BSD_NEW,BC_BSD_DELETE,
	BSD_CHANGE,BSD_NEW,BSD_DELETE,
	BC_EQU_NEW, BC_EQU_DELETE, BC_EQU_CHANGE,
	EQU_NEW, EQU_DELETE, EQU_CHANGE,
	BC_STF_CHANGE,BC_STF_DELETE,BC_STF_NEW,STF_CHANGE,STF_DELETE,STF_NEW,
	GPM_PREMIS_DELETE,
//----BDPS-SECTION
	BC_ALT_CHANGE,BC_ALT_DELETE,ALT_CHANGE,ALT_DELETE,    
	BC_ACT_CHANGE,BC_ACT_DELETE,ACT_CHANGE,ACT_DELETE,    
	BC_ACR_CHANGE,BC_ACR_DELETE,ACR_CHANGE,ACR_DELETE,
	BC_APT_CHANGE,BC_APT_DELETE,APT_CHANGE,APT_DELETE,    
	BC_RWY_CHANGE,BC_RWY_DELETE,RWY_CHANGE,RWY_DELETE,    
	BC_TWY_CHANGE,BC_TWY_DELETE,TWY_CHANGE,TWY_DELETE,   
	BC_PST_CHANGE,BC_PST_DELETE,PST_CHANGE,PST_DELETE, 
	BC_GAT_CHANGE,BC_GAT_DELETE,GAT_CHANGE,GAT_DELETE,
	BC_CIC_CHANGE,BC_CIC_DELETE,CIC_CHANGE,CIC_DELETE,   
	BC_BLT_CHANGE,BC_BLT_DELETE,BLT_CHANGE,BLT_DELETE,   
	BC_BLA_CHANGE,BC_BLA_DELETE,BLA_CHANGE,BLA_DELETE,   
	BC_EXT_CHANGE,BC_EXT_DELETE,EXT_CHANGE,EXT_DELETE,   
	BC_DEN_CHANGE,BC_DEN_DELETE,DEN_CHANGE,DEN_DELETE,   
	BC_MVT_CHANGE,BC_MVT_DELETE,MVT_CHANGE,MVT_DELETE,   
	BC_HAG_CHANGE,BC_HAG_DELETE,HAG_CHANGE,HAG_DELETE,    
	BC_NAT_CHANGE,BC_NAT_DELETE,NAT_CHANGE,NAT_DELETE,   
	BC_WRO_CHANGE,BC_WRO_DELETE,WRO_CHANGE,WRO_DELETE,   
	BC_HTY_CHANGE,BC_HTY_DELETE,HTY_CHANGE,HTY_DELETE,   
	BC_STY_CHANGE,BC_STY_DELETE,STY_CHANGE,STY_DELETE,   
	BC_FID_CHANGE,BC_FID_DELETE,FID_CHANGE,FID_DELETE,
	BC_PER_CHANGE,BC_PER_NEW,BC_PER_DELETE,
	PER_NEW,PER_DELETE,PER_CHANGE,
	BC_GEG_CHANGE,BC_GEG_NEW,BC_GEG_DELETE,
	GEG_CHANGE,GEG_NEW,GEG_DELETE,
	BC_PFC_CHANGE,BC_PFC_DELETE,BC_PFC_NEW,PFC_CHANGE,PFC_DELETE,PFC_NEW,
	BC_CCA_CHANGE,BC_CCA_DELETE,BC_CCA_NEW,CCA_CHANGE,CCA_DELETE,CCA_NEW,
	BC_PRC_CHANGE,BC_PRC_DELETE,BC_PRC_NEW,PRC_CHANGE,PRC_DELETE,PRC_NEW,
	BC_SDT_CHANGE,BC_SDT_DELETE,BC_SDT_NEW,SDT_CHANGE,SDT_DELETE,SDT_NEW,
	BC_VAL_CHANGE,BC_VAL_NEW,BC_VAL_DELETE,VAL_NEW,VAL_CHANGE,VAL_DELETE,
	BC_PAR_CHANGE,BC_PAR_NEW,BC_PAR_DELETE,PAR_NEW,PAR_CHANGE,PAR_DELETE,
	BC_SGR_CHANGE,BC_SGR_NEW,BC_SGR_DELETE,SGR_NEW,SGR_CHANGE,SGR_DELETE,
	BC_SGM_CHANGE,BC_SGM_NEW,BC_SGM_DELETE,SGM_NEW,SGM_CHANGE,SGM_DELETE,

    // from here, there are defines for your project
	FLIGHT_CHANGE, FLIGHT_DELETE, FLIGHT_UPDATE,
	BC_FLIGHT_CHANGE, BC_FLIGHT_DELETE
};

// end globals
/////////////////////////////////////////////////////////////////////////////


#endif //__CCSGLOBL_H__
#endif // !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
