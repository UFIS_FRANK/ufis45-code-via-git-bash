#if !defined(AFX_GHSLIST_H__C2FA1333_49D6_11D1_B3DF_0000B45A33F5__INCLUDED_)
#define AFX_GHSLIST_H__C2FA1333_49D6_11D1_B3DF_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GhsList.h : header file
//
#include "resource.h"
#include "CedaGhsData.h"
#include "Table.h"
/////////////////////////////////////////////////////////////////////////////
// GhsList dialog

class GhsList : public CDialog
{
// Construction
public:
	GhsList(CWnd* pParent = NULL, 
			int ipSelectMode = (LBS_MULTIPLESEL | LBS_EXTENDEDSEL), 
			CString opCalledFrom = CString(""),
			long *lpUrno = NULL);   // standard constructor

	~GhsList();
// Dialog Data
	//{{AFX_DATA(GhsList)
	enum { IDD = IDD_AW_GHS };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	long m_nDialogBarHeight;
	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;

	CCSPtrArray<GHSDATA> omLines;
	CTable *pomTable;

	long *plmGhsUrno;
	int imSelectMode;
	CString omCalledFrom;
	GHSDATA *prmSelectedGhs;
	void UpdateDisplay();
	CString Format(GHSDATA *prpBreak);
	void CreateLine(GHSDATA *prpBreak);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GhsList)
	public:
	virtual BOOL DestroyWindow();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GhsList)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
    afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GHSLIST_H__C2FA1333_49D6_11D1_B3DF_0000B45A33F5__INCLUDED_)
