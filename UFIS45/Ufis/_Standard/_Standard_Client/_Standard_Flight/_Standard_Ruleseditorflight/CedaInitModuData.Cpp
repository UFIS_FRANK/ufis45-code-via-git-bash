// CedaInitModuData.cpp
 
#include "stdafx.h"
#include "CedaInitModuData.h"

CedaInitModuData ogInitModuData;


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaInitModuData::CedaInitModuData()
{
	strcpy(pcmInitModuFieldList,"APPL,SUBD,FUNC,FUAL,TYPE,STAT");
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaInitModuData::~CedaInitModuData(void)
{
	TRACE("CedaInitModuData::~CedaInitModuData called\n");
}

//--SEND INIT MODULS-------------------------------------------------------------------------------------------

bool CedaInitModuData::SendInitModu()
{
	bool ilRc = true;

//INITMODU//////////////////////////////////////////////////////////////
	GetInitModuTxt();

//INITMODU END//////////////////////////////////////////////////////////


	int ilCount = omInitModuData.GetLength();

	char *pclInitModuData = new char[omInitModuData.GetLength()+3];
	strcpy(pclInitModuData,omInitModuData);

	int ilCount2 = strlen(pclInitModuData);

	char pclTableExt[256];

	strcpy(pclTableExt, pcgTableExt);
	strcat(pclTableExt, " ");

	ilRc = CedaAction("SMI",pclTableExt,pcmInitModuFieldList,"","",pclInitModuData);
	TRACE("SendInitModu: CedaReturn %d \n",ilRc);
	if(ilRc==false)
	{
		AfxMessageBox(CString(GetString(IDS_STRING327)) + omLastErrorMessage,MB_ICONEXCLAMATION);
	}
	delete pclInitModuData;
	omInitModuData.Empty();
	return ilRc;
}

//--------------------------------------------------------------------------------------------------------------



CString CedaInitModuData::GetInitModuTxt()
{
	omInitModuData.Empty();

	GetInitModuData1();
	GetInitModuData2();

	return omInitModuData;
}

void CedaInitModuData::GetInitModuData1()
{
		omInitModuData =  ogAppName + ",InitModu,InitModu,Initialisieren (InitModu),B,-";
}


void CedaInitModuData::GetInitModuData2()
{	

}