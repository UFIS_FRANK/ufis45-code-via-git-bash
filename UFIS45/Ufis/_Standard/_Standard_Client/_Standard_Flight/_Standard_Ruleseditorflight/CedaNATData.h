// CedaNATData.h

#ifndef __CEDANATDATA__
#define __CEDANATDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

//@See: CedaNATData
//@Doc:	A structure for reading Duty data. We read all data from database and put them
//		into this struct and store the data in omData.  

//---------------------------------------------------------------------------------------------------------
struct NATDATA 
{
	CTime	 Cdat; 		// Erstellungsdatum
	CTime	 Lstu; 		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Tnam[32]; 	// Verkehrsart Name
	char 	 Ttyp[6]; 	// Verkehrsart
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr; 		// G�ltig von
	CTime	 Vato; 		// G�ltig bis
	char	 Alga[2];		//Allocate Position
	char	 Alpo[2];		//Allocate Gate
	char	 Albb[2];		//Allocate Belt
	char	 Alwr[2];		//Allocate Lounge

	//DataCreated by this class
	int		 IsChanged;

	NATDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
		strcpy(Alga, "");		//Allocate Position
		strcpy(Alpo, "");		//Allocate Gate
		strcpy(Albb, "");		//Allocate Belt
		strcpy(Alwr, "");		//Allocate Lounge
	}

}; // end NATDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

//@Memo: Read and write NAT(duty) data
//@See: CedaData, CedaDutyData
/*@Doc:
  Reads and writes NAT(duty) data from and to database. Stores NAT data in memory and
  provides methods for searching and retrieving NATs from its internal list. Does
  some data preparations.

  Data of this class has to be updated by CEDA through broadcasts. This procedure and the
  necessary methods will be described later with the description of broadcast handling.

  {\bf CedaNATData} handles data from the server process. 
  See the specification for table descriptions.*/

class CedaNATData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Urno field of all loaded NATs.
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omTtypMap;

    //@ManMemo: NATDATA records read by ReadAllNAT().
    CCSPtrArray<NATDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf NATDATA},
      the {\bf pcmTableName} with table name {\bf NATLSG}, the data members {\bf pcmFieldList}
      contains a list of used fields of {\bf NATDATA}.
    */
    CedaNATData();
	~CedaNATData();

	void Register(void);

	void ClearAll(void);

    //@ManMemo: Read all NATs.
    /*@Doc:
      Read all NATs. 

      This method will clear {\bf omData} and {\bf omKeyMap}. Convert each record
      to a NATDATA and store it in {\bf omData} and the Urno field in {\bf omKeyMap}.
    */
    bool ReadAllNATs();
    //@AreMemo: Read all NAT-Data corresponding to Where-condition
	bool ReadSpecialData(CCSPtrArray<NATDATA> *popNat,char *pspWhere,char *pspFieldList,bool ipSYS = true);
    //@ManMemo: Add a new NAT and store the new data to the Database.
	bool InsertNAT(NATDATA *prpNAT,BOOL bpSendDdx = TRUE);
    //@ManMemo: Add data for a new NAT to the internal data.
	bool InsertNATInternal(NATDATA *prpNAT);
	//@ManMemo: Change the data of a special NAT
	bool UpdateNAT(NATDATA *prpNAT,BOOL bpSendDdx = TRUE);
    //@AreMemo: Change the data of a special NAT in the internal data.
	bool UpdateNATInternal(NATDATA *prpNAT);
    //@ManMemo: Delete a special NAT, also in the Database.
	bool DeleteNAT(long lpUrno);
    //@ManMemo: Delete a special NAT only in the internal data.
	bool DeleteNATInternal(NATDATA *prpNAT);
    //@AreMemo: Selects all NATs with a special Urno.
	NATDATA  *GetNATByUrno(long lpUrno);
    //@AreMemo: return Urno with Ttyp
	long GetUrnoByTtyp(char *pspTtyp);
    //@ManMemo: Search a NAT with a special Key.
	bool NATExists(NATDATA *prpNAT);
    //@AreMemo: Search a Ttyp
	bool ExistTtyp(char* popTtyp);
	//@ManMemo: Writes a special NAT to the Database.
	bool SaveNAT(NATDATA *prpNAT);
    //@ManMemo: Contains the valid data for all existing NATs.
	char pcmNATFieldList[2048];
    //@ManMemo: Handle Broadcasts for NATs.
	void ProcessNATBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Private methods
private:
    void PrepareNATData(NATDATA *prpNATData);
};

extern CedaNATData ogNatData;
//---------------------------------------------------------------------------------------------------------

#endif //__CEDANATDATA__
