// CedaACRData.h

#ifndef __CEDAACRDATA__
#define __CEDAACRDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

//@See: CedaACRData
//@Doc:	A structure for reading Duty data. We read all data from database and put them
//		into this struct and store the data in omData.  

//---------------------------------------------------------------------------------------------------------
struct ACRDATA 
{
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Regn[14]; 	// Flugzeug-Kennung
	char 	 Regi[14]; 	// Flugzeug-Kennung mit Bindestrich
	char 	 Act3[5]; 	// Flugzeug-Typ 3-Letter Code (IATA)
	char 	 Act5[7]; 	// Flugzeug-Typ 5-Letter Code (ICAO)
	char 	 Owne[5]; 	// Flugzeug-Besitzer
	char 	 Selc[7]; 	// SELCAL
	char 	 Mtow[8]; 	// Max. Startgewicht
	char 	 Mowe[8]; 	// Max. Eigengewicht
	char 	 Apui[3]; 	// APU INOP
	char 	 Main[3]; 	// Maindeck-Flugzeug
	char 	 Enna[12]; 	// Triebwerksbezeichnung
	char 	 Annx[4]; 	// L�rm-Annex
	char 	 Nose[5]; 	// Anzahl Sitze
	char 	 Noga[4]; 	// Anzahl K�chen
	char 	 Noto[4]; 	// Anzahl Toiletten
	char 	 Debi[12]; 	// Debitoren Nummer
	CTime	 Ladp;	 	// Letzte Abflugzeit
	char 	 Rema[42]; 	// Bemerkung
	CTime	 Vafr;	 	// G�ltig von
	CTime	 Vato;	 	// G�ltig bis

	//DataCreated by this class
	int      IsChanged;

	ACRDATA(void)
	{ memset(this,'\0',sizeof(*this));
	 Cdat=-1;
	 Lstu=-1;
	 Ladp=-1;
	 Vafr=-1;
	 Vato=-1;
	}


}; // end ACRDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

//@Memo: Read and write ACR(duty) data
//@See: CedaData, CedaDutyData
/*@Doc:
  Reads and writes ACR(duty) data from and to database. Stores ACR data in memory and
  provides methods for searching and retrieving ACRs from its internal list. Does
  some data preparations.

  Data of this class has to be updated by CEDA through broadcasts. This procedure and the
  necessary methods will be described later with the description of broadcast handling.

  {\bf CedaACRData} handles data from the server process. 
  See the specification for table descriptions.*/

class CedaACRData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Urno field of all loaded ACRs.
    CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omRegnMap;

    //@ManMemo: ACRDATA records read by ReadAllACR().
    CCSPtrArray<ACRDATA> omData;
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf ACRDATA},
      the {\bf pcmTableName} with table name {\bf ACRLSG}, the data members {\bf pcmFieldList}
      contains a list of used fields of {\bf ACRDATA}.
    */
    CedaACRData();
	~CedaACRData();
	void Register(void);
	void ClearAll(void);

    //@ManMemo: Read all ACRs.
    /*@Doc:
      Read all ACRs. 

      This method will clear {\bf omData} and {\bf omKeyMap}. Convert each record
      to a ACRDATA and store it in {\bf omData} and the Urno field in {\bf omKeyMap}.
    */
    bool ReadAllACRs(char *pspWhere = "");
    //@AreMemo: Read all ACR-Data corresponding to Where-condition
	bool ReadSpecialAcrData(CCSPtrArray<ACRDATA> &ropAcr,char *pspWhere);
    //@ManMemo: Add a new ACR and store the new data to the Database.
	bool InsertACR(ACRDATA *prpACR,BOOL bpSendDdx = TRUE);
    //@ManMemo: Add data for a new ACR to the internal data.
	bool InsertACRInternal(ACRDATA *prpACR);
	//@ManMemo: Change the data of a special ACR
	bool UpdateACR(ACRDATA *prpACR,BOOL bpSendDdx = TRUE);
    //@AreMemo: Change the data of a special ACR in the internal data.
	bool UpdateACRInternal(ACRDATA *prpACR);
    //@ManMemo: Delete a special ACR, also in the Database.
	bool DeleteACR(long lpUrno);
    //@ManMemo: Delete a special ACR only in the internal data.
	bool DeleteACRInternal(ACRDATA *prpACR);
    //@AreMemo: Selects all ACRs with a special Urno.
	ACRDATA  *GetACRByUrno(long lpUrno);
    //@ManMemo: Search a ACR with a special Key.
	bool ACRExists(ACRDATA *prpACR);
	//@ManMemo: Writes a special ACR to the Database.
	bool SaveACR(ACRDATA *prpACR);
    //@ManMemo: Contains the valid data for all existing ACRs.
	char pcmACRFieldList[2048];
    //@ManMemo: Handle Broadcasts for ACRs.
	void ProcessACRBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Private methods
private:
    void PrepareACRData(ACRDATA *prpACRData);
};

//---------------------------------------------------------------------------------------------------------

extern CedaACRData ogAcrData;

#endif //__CEDAACRDATA__
