// GatPosBeltAlloc.cpp : implementation file
//

#include "stdafx.h"
#include "Rules.h"
#include "CCSGlobl.h"
#include "CedaBlaData.h"
#include "GatPosBeltAlloc.h"
#include "BasicData.h"
#include "BlaRuleDlg.h"
#include "DutyPreferences.h"
#include <iomanip.h>
#include <fstream.h>
#include <iostream.h>
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern DutyPreferences *pogDutyPreferences;

static int CompareFtyp(const BLADATA **e1, const BLADATA **e2)
{
	return strcmp((**e1).Ftyp, (**e2).Ftyp);
}




/////////////////////////////////////////////////////////////////////////////
// GatPosBeltAlloc property page

IMPLEMENT_DYNCREATE(GatPosBeltAlloc, CPropertyPage)

static int CompareFlti(const BLADATA **e1, const BLADATA **e2)
{
	return strcmp((**e1).Ftyp, (**e2).Ftyp);
}


GatPosBeltAlloc::GatPosBeltAlloc() : CPropertyPage(GatPosBeltAlloc::IDD)
{
	//{{AFX_DATA_INIT(GatPosBeltAlloc)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomBlaAllocList = new CCSTable;
}

GatPosBeltAlloc::~GatPosBeltAlloc()
{
	delete pomBlaAllocList;
}

void GatPosBeltAlloc::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatPosRules)
	DDX_Control(pDX, IDC_BELTALLOC_LIST, m_BlaAllocRuleListTable);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatPosBeltAlloc, CPropertyPage)
	//{{AFX_MSG_MAP(GatPosBeltAlloc)
	  ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnGridDblClk)
	  ON_MESSAGE(CLK_BUTTON_PRINT,OnPrintBeltAlloc)
//	ON_MESSAGE(CLK_BUTTON_SAVE,			OnSaveButton)
	ON_MESSAGE(CLK_BUTTON_NEW,			OnNewButton)
//	ON_MESSAGE(CLK_BUTTON_UPDATE,		OnUpdateButton)
	ON_MESSAGE(CLK_BUTTON_DELETE,		OnDeleteButton)
	ON_MESSAGE(CLK_BUTTON_COPY,			OnCopyButton)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,  OnUpdateButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatPosBeltAlloc message handlers

BOOL GatPosBeltAlloc::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	int i=0;
	CRect olRect;
	CStringArray olBlaHeader;

	/*

	ogBlaData.omData.Sort(CompareFtyp);
	for(i = 0; i < ogBlaData.omData.GetSize(); i++)
	{
		omBlaUrnos.Add(ogBlaData.omData[i].Urno);
	}
*/

	m_BlaAllocRuleListTable.GetClientRect(olRect);
	pomBlaAllocList->SetTableData(this, olRect.left + 13, olRect.right + 13, olRect.top +13, olRect.bottom + 13);


	CString olViewName = "<Default>";
	omBlaContextItem = -1;
	omBlaRulesViewer.Attach(pomBlaAllocList);
	omBlaRulesViewer.ChangeViewTo(olViewName);

	UpdateView();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GatPosBeltAlloc::UpdateView()
{

	CString olViewName = "<Default>";
	int ilBlaLine = pomBlaAllocList->pomListBox->GetTopIndex();
	
	AfxGetApp()->DoWaitCursor(1);
    omBlaRulesViewer.ChangeViewTo(olViewName);

	pomBlaAllocList->pomListBox->SetTopIndex(ilBlaLine);
	

	AfxGetApp()->DoWaitCursor(-1);

}


void GatPosBeltAlloc::MakeBlaLineData(BLADATA *popBla, CStringArray &ropArray)
{
		ropArray.Add(popBla->Ftyp);
		ropArray.Add(popBla->Agrp);
		ropArray.Add(popBla->Ogrp);
}

LONG GatPosBeltAlloc::OnGridDblClk(UINT wParam, LONG lParam)
{

	int ilCol = HIWORD(wParam);
	int ilRow = LOWORD(wParam);
		
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lParam;



	if(polNotify->SourceTable == pomBlaAllocList)
	{
//		if(ilRow >= 0 && ilRow < omBlaUrnos.GetSize())
		if(ilRow >= 0 && ilRow < omBlaRulesViewer.omLines.GetSize())
		{
			long llUrno = (long)	omBlaRulesViewer.omLines[ilRow].Urno;
			BLADATA *polBla = ogBlaData.GetBlaByUrno(llUrno);
			if(polBla != NULL)
			{
				BlaRuleDlg olDlg(this, polBla);
				if(olDlg.DoModal() == IDOK)
				{
					olDlg.GetBlaData(polBla);
					ogBlaData.UpdateBla(polBla,FALSE);
					omBlaRulesViewer.ChangeViewTo(0);
				} 
			}
		}
	}

	return 0L;
}

BOOL GatPosBeltAlloc::OnKillActive() 
{
	// TODO: Add your specialized code here and/or call the base class

	pogDutyPreferences->pomSaveButton->EnableWindow(TRUE);	

	pogDutyPreferences->pomCopyButton->EnableWindow(FALSE);
	pogDutyPreferences->pomStandardButton->EnableWindow(FALSE);
	pogDutyPreferences->pomAdditionalButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomNewButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomUpdateButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomDeleteButton->EnableWindow(FALSE);
	

	return CPropertyPage::OnKillActive();
}

BOOL GatPosBeltAlloc::OnSetActive() 
{

	pogDutyPreferences->pomSaveButton->EnableWindow(FALSE);	
	
	pogDutyPreferences->pomCopyButton->EnableWindow(TRUE);
//	pogDutyPreferences->pomStandardButton->EnableWindow(FALSE);
//	pogDutyPreferences->pomAdditionalButton->EnableWindow(FALSE);	
//	pogDutyPreferences->pomSaveButton->EnableWindow(TRUE);	
	pogDutyPreferences->pomNewButton->EnableWindow(TRUE);	
//	pogDutyPreferences->pomUpdateButton->EnableWindow(TRUE);	
	pogDutyPreferences->pomDeleteButton->EnableWindow(TRUE);

	return CPropertyPage::OnSetActive();
}

void GatPosBeltAlloc::OnPrintBeltAlloc()
{
	char pclExcelPath[256];
	char pclConfigPath[256];

	if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString("FIPS", "Excel", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	if(!strcmp(pclExcelPath, "DEFAULT"))
		AfxMessageBox("Unable to Find MSExcel");
	
	ofstream of;
	CString omTableName = "BLATAB";
	CString olFileName = omTableName;
	CString opTrenner = ",";

	CString HeadColu1 = "Flight-ID";
	CString HeadColu2 = "A/C-Group";
	CString HeadColu3 = "Origin-Group";
	CString HeadColu4 = "Time";

	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	CString omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;
	
	of  << setw(ilwidth) << HeadColu1
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu2
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu3
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu4
		<< endl;

	int ilCount = omBlaRulesViewer.omLines.GetSize();
	
	for(int i = 0; i < ilCount; i++)
	{
		BLATABLE_LINEDATA *polBlaLine = &omBlaRulesViewer.omLines.GetAt(i);
		if(polBlaLine != NULL)
		{
			CString BlaTime ;
			BlaTime.Format(_T("%d"),polBlaLine->Time);
			of.setf(ios::left, ios::adjustfield);
			of  << setw(1) << polBlaLine->Ftyp
				<< setw(1) << opTrenner 
				<< setw(1) << polBlaLine->Agrp
				<< setw(1) << opTrenner 
				<< setw(1) << polBlaLine->Ogrp
				<< setw(1) << opTrenner 
				<< setw(1) << BlaTime;

			of << endl;
				 
		}
	
	}

	of.close();

	char pclTmp[256] ; 
	omFileName.TrimLeft();
	omFileName.TrimRight();
	strcpy(pclTmp ,omFileName);
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	int li_Path = _spawnv( _P_NOWAIT , pclExcelPath, args );

	if(li_Path == -1)
		AfxMessageBox("The EXCEL path in Ceda.ini is not proper.Please correct it");
}


LONG GatPosBeltAlloc::OnNewButton(WPARAM wParam, LPARAM lParam)
{

	BLADATA olBla;
	BlaRuleDlg *polDlg = new BlaRuleDlg(this,&olBla);

	if (polDlg->DoModal() == IDOK)
	{
		polDlg->GetBlaData(&olBla);
		ogBlaData.InsertBla(&olBla,FALSE);
		omBlaRulesViewer.ChangeViewTo(0);
	}

//	imMode = NEW_MODE;
	return 0L;
}



LONG GatPosBeltAlloc::OnUpdateButton(WPARAM wParam, LPARAM lParam)
{
		int ilLineNo = pomBlaAllocList->pomListBox->GetCurSel();
		long llUrno = omBlaRulesViewer.omLines[ilLineNo].Urno;

			BLADATA olBla;
			BLATABLE_LINEDATA line;
			line = omBlaRulesViewer.omLines[ilLineNo];
			strcpy(olBla.Ftyp,line.Ftyp);
			olBla.Urno = line.Urno;
			strcpy(olBla.Agrp,line.Agrp);
			strcpy(olBla.Ogrp,line.Ogrp);
			olBla.Time = line.Time;

	BlaRuleDlg *polDlg = new BlaRuleDlg(this,&olBla);

	if (polDlg->DoModal() == IDOK)
	{
		polDlg->GetBlaData(&olBla);
		ogBlaData.UpdateBla(&olBla,FALSE);
		omBlaRulesViewer.ChangeViewTo(0);
	}

	return 0L;
}

LONG GatPosBeltAlloc::OnDeleteButton(WPARAM wParam, LPARAM lParam)
{

		int ilLineNo = pomBlaAllocList->pomListBox->GetCurSel();
		if (ilLineNo > -1)
		{
			long llUrno = omBlaRulesViewer.omLines[ilLineNo].Urno;

			ogBlaData.DeleteBla(llUrno);
			omBlaRulesViewer.ChangeViewTo(0);
		}
	return 0L;
}

LONG GatPosBeltAlloc::OnCopyButton(WPARAM wParam, LPARAM lParam)
{
		int ilLineNo = pomBlaAllocList->pomListBox->GetCurSel();
		long llUrno = omBlaRulesViewer.omLines[ilLineNo].Urno;

			BLADATA olBla;
			BLATABLE_LINEDATA line;
			line = omBlaRulesViewer.omLines[ilLineNo];
			strcpy(olBla.Ftyp,line.Ftyp);
			olBla.Urno = 0L;
			strcpy(olBla.Agrp,line.Agrp);
			strcpy(olBla.Ogrp,line.Ogrp);
			olBla.Time = line.Time;

	BlaRuleDlg *polDlg = new BlaRuleDlg(this,&olBla);
	if (polDlg->DoModal() == IDOK)
	{
		polDlg->GetBlaData(&olBla);
		ogBlaData.InsertBla(&olBla,FALSE);
		omBlaRulesViewer.ChangeViewTo(0);
	}

	return 0L;
}