// basicdat.h CCSBasicData class for providing general used methods

#ifndef _BASICDATA
#define _BASICDATA

#include <InitialLoadDlg.h>
#include <ccsglobl.h>
#include <ccsptrarray.h>
#include <ccscedaData.h>
#include <CcsLog.h>
#include <CedaCfgData.h>
#include <CCSTime.h>
#include <CCSDdx.h>
#include <CCSCedaCom.h>
#include <CCSBcHandle.h>
#include <CCSMessageBox.h>


int GetItemCount(CString olList, char cpTrenner  = ',' );
int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner = ',');
CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner  = ',');
CString DeleteListItem(CString &opList, CString olItem);
int SplitItemList(CString opString, CStringArray *popStrArray, int ipMaxItem, char cpTrenner = ',');

bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CString &opData);
bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CTime &opData);

CString LoadStg(UINT nID);
CString SortItemList(CString opSubString, char cpTrenner);


/////////////////////////////////////////////////////////////////////////////
// class BasicData

class CBasicData : public CCSCedaData
{

public:

	CBasicData(void);
	~CBasicData(void);
	
	int imScreenResolutionX;
	int imScreenResolutionY;
	int imMonitorCount;
	long ConfirmTime;
	CString omUserID;
	CString omLoginUser;

	long GetNextUrno(void);
	int imNextOrder;


	int GetNextOrderNo();
	bool GetNurnos(int ipNrOfUrnos);

	char *GetCedaCommand(CString opCmdType);
	void GetDiagramStartTime(CTime &opStart, CTime &opEnd);
	void SetDiagramStartTime(CTime opDiagramStartTime, CTime opEndTime);
	void SetWorkstationName(CString opWsName);
	CString GetWorkstationName();
	bool GetWindowPosition(CRect& rlPos,CString olMonitor);
	int GetUtcDifference(void);

	void SetLocalDiff();

	CTimeSpan GetLocalDiff(CTime opDate)
	{
		if(opDate < omTich)
		{
			return omLocalDiff1;
		}
		else
		{
			return omLocalDiff2;
		}

		//return omLocalDiff;
	};
	CTimeSpan GetLocalDiff1() { return omLocalDiff1; };
	CTimeSpan GetLocalDiff2() { return omLocalDiff2; };
	CTime     GetTich() { return omTich; };

	void LocalToUtc(CTime &opTime);
	void UtcToLocal(CTime &opTime);

	bool IsGatPosEnabled();

	int		GetDependingPositions(long lpPstUrno,CStringArray& ropPositions);
	bool	SetDependingPositions(long lpPstUrno,const CStringArray& ropPositions);
	CString	GetDependingAircraftGroups(long lpPstFromUrno,long lpPstToUrno,const CString& ropACFrom,const CString& ropACTo);
	bool	SetDependingAircraftGroups(long lpFromPosUrno,long lpToPosUrno,const CString& ropACFrom,const CString& ropACTo,const CString& ropValue);
	CString	GetDependingXXXGroups(long lpPstFromUrno,long lpPstToUrno,const CString& ropACFrom,const CString& ropACTo, const CString& ropAlo, const CString& ropGrp);
	bool	SetDependingXXXGroups(long lpFromPosUrno,long lpToPosUrno,const CString& ropACFrom,const CString& ropACTo,const CString& ropValue, const CString& ropAlo, const CString& ropGrp);
	
public:
	static void TrimLeft(char *s);
	static void TrimRight(char *s);

private:
	CTimeSpan omLocalDiff1;
	CTimeSpan omLocalDiff2;
	CTime	  omTich;

	char pcmCedaCmd[12];
	CTime omDiaStartTime;
	CTime omDiaEndTime;

	CString omDefaultComands;
	CString omActualComands;
	char pcmCedaComand[24];
	CString omWorkstationName; 
private:
	CDWordArray omUrnos;
};


#endif
