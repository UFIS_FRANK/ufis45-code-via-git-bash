// CedaAPTData.cpp
 
#include <stdafx.h>
#include <CCSGlobl.h>
#include <CedaAPTData.h>

// Local function prototype
static void ProcessAPTCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------


CedaAPTData::CedaAPTData()
{
    // Create an array of CEDARECINFO for APTDATA
	BEGIN_CEDARECINFO(APTDATA,APTDataRecInfo)
		FIELD_LONG     (Urno,"URNO")
		FIELD_DATE	   (Cdat,"CDAT")
		FIELD_CHAR_TRIM(Usec,"USEC")
		FIELD_DATE	   (Lstu,"LSTU")
		FIELD_CHAR_TRIM(Prfl,"PRFL")
		FIELD_CHAR_TRIM(Useu,"USEU")
		FIELD_CHAR_TRIM(Apc3,"APC3")
		FIELD_CHAR_TRIM(Apc4,"APC4")
		FIELD_CHAR_TRIM(Apfn,"APFN")
		FIELD_CHAR_TRIM(Land,"LAND")
		FIELD_CHAR_TRIM(Etof,"ETOF")
		FIELD_DATE	   (Tich,"TICH")
		FIELD_CHAR_TRIM(Tdi1,"TDI1")
		FIELD_CHAR_TRIM(Tdi2,"TDI2")
		FIELD_CHAR_TRIM(Apsn,"APSN")
		FIELD_DATE	   (Vafr,"VAFR")
		FIELD_DATE	   (Vato,"VATO")
	END_CEDARECINFO //(APTDataAStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(APTDataRecInfo)/sizeof(APTDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&APTDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"APT");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmAPTFieldList,"URNO,CDAT,USEC,LSTU,PRFL,USEU,APC3,APC4,APFN,LAND,ETOF,TICH,TDI1,TDI2,APSN,VAFR,VATO");
	pcmFieldList = pcmAPTFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}


void CedaAPTData::Register(void)
{
	ogDdx.Register((void *)this,BC_APT_CHANGE,CString("APTDATA"), CString("APT-changed"),ProcessAPTCf);
	ogDdx.Register((void *)this,BC_APT_DELETE,CString("APTDATA"), CString("APT-deleted"),ProcessAPTCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaAPTData::~CedaAPTData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaAPTData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
	omApc3Map.RemoveAll();
	omApc4Map.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaAPTData::ReadAllAPTs()
{
	char pclTable[64] = "AFT";
	char pclFieldList[1024] = "";
	char pclData[3000] = "";
	char pclSelection[256] = "[CONFIG]";

	bool blRet = CedaAction("GFR", pclTable, pclFieldList, pclSelection, "", pclData, "BUF1");

	bool blFLTI_PRIO = false;
	CStringArray* olDataBuf = GetDataBuff();
	for (int k = olDataBuf->GetSize() - 1; k >= 0; k--)
//	for (int k = GetBufferSize() - 1; k >= 0; k--)
	{
		CString olTmp = olDataBuf->GetAt(k);

//		olTmp="FLTI_PRIO,ESD";

		if (olTmp.Find("FLTI_PRIO") != -1)
		{
			CStringArray olStrArray;
			ExtractItemList(CString(olTmp), &olStrArray);
			if(olStrArray.GetSize() == 2)
			{
				ogFltiPrio = olStrArray[1];
				blFLTI_PRIO = true;
				break;
			}
		}
	}
	if (blFLTI_PRIO && ogFltiPrio.IsEmpty())
	{
		ogFltiPrio = "?";
		::MessageBox(NULL,"FlightConfig: FLTI_PRIO hasn't Values" ,"ERROR FlightConfig",MB_OK);
	}




    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
	omApc3Map.RemoveAll();
	omApc4Map.RemoveAll();
    omData.DeleteAll();
	ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		APTDATA *prpAPT = new APTDATA;
		if ((ilRc = GetBufferRecord(ilLc,prpAPT)) == true)
		{
			prpAPT->IsChanged = DATA_UNCHANGED;
			omData.Add(prpAPT);//Update omData
			omUrnoMap.SetAt((void *)prpAPT->Urno,prpAPT);
			omApc3Map.SetAt(prpAPT->Apc3,prpAPT);
			omApc4Map.SetAt(prpAPT->Apc4,prpAPT);
		}
		else
		{
			delete prpAPT;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaAPTData::InsertAPT(APTDATA *prpAPT,BOOL bpSendDdx)
{
	APTDATA *prlAPT = new APTDATA;
	memcpy(prlAPT,prpAPT,sizeof(APTDATA));
	prlAPT->IsChanged = DATA_NEW;
	SaveAPT(prlAPT); //Update Database
	InsertAPTInternal(prlAPT);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaAPTData::InsertAPTInternal(APTDATA *prpAPT)
{
	//PrepareAPTData(prpAPT);
	ogDdx.DataChanged((void *)this, APT_CHANGE,(void *)prpAPT ); //Update Viewer
	omData.Add(prpAPT);//Update omData
	omUrnoMap.SetAt((void *)prpAPT->Urno,prpAPT);
	omApc3Map.SetAt(prpAPT->Apc3,prpAPT);
	omApc4Map.SetAt(prpAPT->Apc4,prpAPT);
	return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaAPTData::DeleteAPT(long lpUrno)
{
	bool olRc = true;
	APTDATA *prlAPT = GetAPTByUrno(lpUrno);
	if (prlAPT != NULL)
	{
		prlAPT->IsChanged = DATA_DELETED;
		olRc = SaveAPT(prlAPT); //Update Database
		DeleteAPTInternal(prlAPT);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaAPTData::DeleteAPTInternal(APTDATA *prpAPT)
{
	ogDdx.DataChanged((void *)this,APT_DELETE,(void *)prpAPT); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpAPT->Urno);
	omApc3Map.RemoveKey(prpAPT->Apc3);
	omApc4Map.RemoveKey(prpAPT->Apc4);

	int ilAPTCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilAPTCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpAPT->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaAPTData::PrepareAPTData(APTDATA *prpAPT)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaAPTData::UpdateAPT(APTDATA *prpAPT,BOOL bpSendDdx)
{
	if (GetAPTByUrno(prpAPT->Urno) != NULL)
	{
		if (prpAPT->IsChanged == DATA_UNCHANGED)
		{
			prpAPT->IsChanged = DATA_CHANGED;
		}
		SaveAPT(prpAPT); //Update Database
		UpdateAPTInternal(prpAPT);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaAPTData::UpdateAPTInternal(APTDATA *prpAPT)
{
	APTDATA *prlAPT = GetAPTByUrno(prpAPT->Urno);
	if (prlAPT != NULL)
	{
		omApc3Map.RemoveKey(prlAPT->Apc3);
		omApc4Map.RemoveKey(prlAPT->Apc4);
		*prlAPT = *prpAPT; //Update omData
		omApc3Map.SetAt(prlAPT->Apc3,prlAPT);
		omApc4Map.SetAt(prlAPT->Apc4,prlAPT);
		ogDdx.DataChanged((void *)this,APT_CHANGE,(void *)prlAPT); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

APTDATA *CedaAPTData::GetAPTByUrno(long lpUrno)
{
	APTDATA  *prlAPT;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlAPT) == TRUE)
	{
		return prlAPT;
	}
	return NULL;
}

//--READSPECIALAPTDATA-------------------------------------------------------------------------------------

bool CedaAPTData::ReadSpecial(CCSPtrArray<APTDATA> &ropApt,char *pspWhere)
{
	bool ilRc = true;
	if (pspWhere == NULL)
	{
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT",pspWhere);
	}
	if (ilRc != true)
	{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
	}

	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		APTDATA *prpApt = new APTDATA;
		if ((ilRc = GetBufferRecord(ilLc,prpApt)) == true)
		{
			ropApt.Add(prpApt);
		}
		else
		{
			delete prpApt;
		}
	}
    return true;
}

//--EXIST-I-----------------------------------------------------------------------------------------------
long CedaAPTData::GetUrnoByApc(char *pspApc)
{
	APTDATA  *prlAPT;
	if(strcmp(pspApc, "") == 0)
	{
		return 0;
	}

	if (omApc3Map.Lookup((LPCSTR)pspApc,(void *&)prlAPT) == TRUE)
	{
		return prlAPT->Urno;
	}

	if (omApc4Map.Lookup((LPCSTR)pspApc,(void *&)prlAPT) == TRUE)
	{
		return prlAPT->Urno;
	}
	return 0;
}

CString CedaAPTData::APTExists(APTDATA *prpAPT)
{
	CString olField = "";
	APTDATA  *prlAPT;

	if (omApc3Map.Lookup((LPCSTR)prpAPT->Apc3,(void *&)prlAPT) == TRUE)
	{
		if(prlAPT->Urno != prpAPT->Urno)
		{
			olField += GetString(IDS_STRING193);
		}
	}

	if (omApc4Map.Lookup((LPCSTR)prpAPT->Apc4,(void *&)prlAPT) == TRUE)
	{
		if(prlAPT->Urno != prpAPT->Urno)
		{
			olField += GetString(IDS_STRING194);
		}
	}

	return olField;
}

//--EXIST-II----------------------------------------------------------------------------------------------

bool CedaAPTData::ExistApc3(char* popApc3)
{
	bool ilRc = true;
	char pclSelection[512];
	sprintf(pclSelection,"WHERE APC3='%s'",popApc3);
	ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"),pcmTableName,"APC3",pclSelection,"","");
	return ilRc;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaAPTData::SaveAPT(APTDATA *prpAPT)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpAPT->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpAPT->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpAPT);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpAPT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpAPT->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpAPT);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpAPT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpAPT->Urno);
		olRc = CedaAction("DRT",pclSelection);
		//prpAPT->IsChanged = DATA_UNCHANGED;
		break;
	}

	if(!omLastErrorMessage.IsEmpty())
	{
		if(omLastErrorMessage.Find("ORA") != -1)
		{
			char pclMsg[2048]="";
			sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
			::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
			return false;
		}
	}
    return true;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessAPTCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_APT_CHANGE :
	case BC_APT_DELETE :
		((CedaAPTData *)popInstance)->ProcessAPTBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaAPTData::ProcessAPTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlAPTData;
	long llUrno;
	prlAPTData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlAPTData->Selection;
	if (olSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(prlAPTData->Selection);
	}
	else
	{
		int ilFirst = olSelection.Find("=")+2;
		int ilLast  = olSelection.GetLength();
		llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
	}

	APTDATA *prlAPT;
	if(ipDDXType == BC_APT_CHANGE)
	{
		if((prlAPT = GetAPTByUrno(llUrno)) != NULL)
		{
			GetRecordFromItemList(prlAPT,prlAPTData->Fields,prlAPTData->Data);
			UpdateAPTInternal(prlAPT);
		}
		else
		{
			prlAPT = new APTDATA;
			GetRecordFromItemList(prlAPT,prlAPTData->Fields,prlAPTData->Data);
			InsertAPTInternal(prlAPT);
		}
	}
	if(ipDDXType == BC_APT_DELETE)
	{
		prlAPT = GetAPTByUrno(llUrno);
		if (prlAPT != NULL)
		{
			DeleteAPTInternal(prlAPT);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
