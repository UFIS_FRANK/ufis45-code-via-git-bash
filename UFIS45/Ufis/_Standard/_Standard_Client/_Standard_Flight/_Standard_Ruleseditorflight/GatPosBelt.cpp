// GatPosBelt.cpp : implementation file
//

#include "stdafx.h"
#include "Rules.h"
#include "CCSGlobl.h"
#include "CedaBltData.h"
#include "GatPosBelt.h"
#include "BasicData.h"
#include "BltRuleDlg.h"
#include "DutyPreferences.h"
#include <iomanip.h>
#include <fstream.h>
#include <iostream.h>
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern DutyPreferences *pogDutyPreferences;


/////////////////////////////////////////////////////////////////////////////
// GatPosBelt property page

IMPLEMENT_DYNCREATE(GatPosBelt, CPropertyPage)

static int CompareBnam(const BLTDATA **e1, const BLTDATA **e2)
{
	return strcmp((**e1).Bnam, (**e2).Bnam);
}


GatPosBelt::GatPosBelt() : CPropertyPage(GatPosBelt::IDD)
{
	//{{AFX_DATA_INIT(GatPosBelt)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomBltList = new CCSTable;
}

GatPosBelt::~GatPosBelt()
{
	delete pomBltList;
}

void GatPosBelt::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatPosRules)
	DDX_Control(pDX, IDC_BELT_LIST, m_BeltListTable);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatPosBelt, CPropertyPage)
	//{{AFX_MSG_MAP(GatPosBelt)
	  ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnGridDblClk)
	  ON_MESSAGE(CLK_BUTTON_PRINT,OnPrintBelt)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatPosBelt message handlers

BOOL GatPosBelt::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	int i=0;
	CRect olRect;
	CStringArray olBltHeader;

	ogBltData.omData.Sort(CompareBnam);
	for(i = 0; i < ogBltData.omData.GetSize(); i++)
	{
		omBltUrnos.Add(ogBltData.omData[i].Urno);
	}


	m_BeltListTable.GetClientRect(olRect);
	pomBltList->SetTableData(this, olRect.left + 13, olRect.right + 13, olRect.top +13, olRect.bottom + 13);


	CString olViewName = "<Default>";
	omBltContextItem = -1;
	omBltRulesViewer.Attach(pomBltList);
	omBltRulesViewer.ChangeViewTo(olViewName);

	UpdateView();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GatPosBelt::UpdateView()
{

	CString olViewName = "<Default>";
	int ilBltLine = pomBltList->pomListBox->GetTopIndex();
	
	AfxGetApp()->DoWaitCursor(1);
    omBltRulesViewer.ChangeViewTo(olViewName);

	pomBltList->pomListBox->SetTopIndex(ilBltLine);
	

	AfxGetApp()->DoWaitCursor(-1);

}


void GatPosBelt::MakeBltLineData(BLTDATA *popBlt, CStringArray &ropArray)
{
	CStringArray olResbList;
	CString olTmpBltr;
	olTmpBltr = popBlt->Bltr;
	if(olTmpBltr.Replace(";","#") < 3)
	{
		olTmpBltr =popBlt->Bltr;
		olTmpBltr.Replace("/",";");
		strcpy(popBlt->Bltr, olTmpBltr.GetBuffer(0));
	}
	

	ExtractItemList(popBlt->Bltr, &olResbList, ';');
	for(int i = 0; i < olResbList.GetSize(); i++)
	{
		ropArray.Add(olResbList[i]);
	}
}
LONG GatPosBelt::OnGridDblClk(UINT wParam, LONG lParam)
{

	int ilCol = HIWORD(wParam);
	int ilRow = LOWORD(wParam);
		
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lParam;


	if(polNotify->SourceTable == pomBltList)
	{
		if(ilRow >= 0 && ilRow < omBltUrnos.GetSize())
		{
			long llUrno = (long)omBltUrnos[ilRow];
			BLTDATA *polBlt = ogBltData.GetBLTByUrno(llUrno);
			if(polBlt != NULL)
			{
				BltRuleDlg olDlg(this, polBlt);
				if(olDlg.DoModal() == IDOK)
				{
					UpdateView();
				} 
			}
		}
	}

	return 0L;
}

BOOL GatPosBelt::OnKillActive() 
{
	// TODO: Add your specialized code here and/or call the base class

	pogDutyPreferences->pomSaveButton->EnableWindow(TRUE);	
	

	return CPropertyPage::OnKillActive();
}

BOOL GatPosBelt::OnSetActive() 
{

	pogDutyPreferences->pomSaveButton->EnableWindow(FALSE);	
	

	return CPropertyPage::OnSetActive();
}

void GatPosBelt::OnPrintBelt()
{
	char pclExcelPath[256];
	char pclConfigPath[256];

	if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString("FIPS", "Excel", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	if(!strcmp(pclExcelPath, "DEFAULT"))
		AfxMessageBox("Unable to Find MSExcel");
	
	ofstream of;
	CString omTableName = "BeltRules";
	CString olFileName = omTableName;
	CString opTrenner = ",";

	CString HeadColu1 = "No.";
	CString HeadColu2 = "Belt";
	CString HeadColu3 = "Term";
	CString HeadColu4 = "Prio";
	CString HeadColu5 = "Sequ";
	CString HeadColu6 = "Pax";
	CString HeadColu7 = "A/L Parameter";
	CString HeadColu8 = "Origin Parameter";
	CString HeadColu9 = "Nature Parameter";
	CString HeadColu10 = "Service Types";
	CString HeadColu11 = "A/C Type Parameters";
	CString HeadColu12 = "AirCraft Groups";
	CString HeadColu13 = "Flight ID";

	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	CString omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;
	
	of  << setw(ilwidth) << HeadColu1
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu2
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu3
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu4
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu5
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu6
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu7
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu8
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu9
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu10
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu11
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu12
		<< setw(1) << opTrenner;

		if( ( (CRulesApp *)AfxGetApp() )->CanHandleBltFlti() == TRUE)
		{
			of << setw(ilwidth) << HeadColu13;
		}

		of << endl;

	int ilCount = omBltRulesViewer.omLines.GetSize();
	
	for(int i = 0; i < ilCount; i++)
	{
		BLTTABLE_LINEDATA *polPst = &omBltRulesViewer.omLines.GetAt(i);
		if(polPst != NULL)
		{
			CString BltNo ;
			BltNo.Format(_T("%d"),polPst->Rtnr);
			of.setf(ios::left, ios::adjustfield);
			of  << setw(1) << BltNo
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Belt
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->T
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Prio
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Sequ
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Pax
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Nat
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Alc
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Org
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Service
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->ACParam
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->ACGroup;

			if( ( (CRulesApp *)AfxGetApp() )->CanHandleBltFlti() == TRUE)
			{
				of << setw(1) << opTrenner 
					<< setw(1) << polPst->Flti;
			}
				
							
			of << endl;
				 
		}
	
	}

	of.close();

	char pclTmp[256] ; 
	omFileName.TrimLeft();
	omFileName.TrimRight();
	strcpy(pclTmp ,omFileName);
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	int li_Path = _spawnv( _P_NOWAIT , pclExcelPath, args );

	if(li_Path == -1)
		AfxMessageBox("The EXCEL path in Ceda.ini is not proper.Please correct it");
}
