#if !defined(AFX_PREMISAWDLG_H__57BD7BD1_45F4_11D1_B3DB_0000B45A33F5__INCLUDED_)
#define AFX_PREMISAWDLG_H__57BD7BD1_45F4_11D1_B3DB_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// PremisAwDlg.h : header file
//
#include "Table.h"
#include "CedaGhpData.h"
#include "resource.h"
/////////////////////////////////////////////////////////////////////////////
// PremisAwDlg dialog

class PremisAwDlg : public CDialog
{
// Construction
public:
	PremisAwDlg(CWnd* pParent, CRect opParentRect);   // standard constructor

// Dialog Data
	//{{AFX_DATA(PremisAwDlg)
	enum { IDD = IDD_AW_PREMIS_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	CCSPtrArray<GHPDATA> omLines;
	CRect omParentRect;
	int m_nDialogBarHeight;
	CTable *pomTable;
	void UpdateDisplay();
	CString Format(GHPDATA *prpGhp);
	void CreateLine(GHPDATA *prpGhp);

    LONG OnTableLButtonDblClk(UINT wParam, LONG lParam);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PremisAwDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PremisAwDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PREMISAWDLG_H__57BD7BD1_45F4_11D1_B3DB_0000B45A33F5__INCLUDED_)
