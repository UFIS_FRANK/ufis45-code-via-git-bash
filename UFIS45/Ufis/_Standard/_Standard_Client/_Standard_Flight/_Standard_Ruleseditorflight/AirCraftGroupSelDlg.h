#if !defined(AFX_AIRCRAFTGROUPSELDLG_H__27FC8383_C5A9_4BE3_BBD1_19A4A04EF50D__INCLUDED_)
#define AFX_AIRCRAFTGROUPSELDLG_H__27FC8383_C5A9_4BE3_BBD1_19A4A04EF50D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AirCraftGroupSelDlg.h : header file
//
#include <resrc1.h>
/////////////////////////////////////////////////////////////////////////////
// CAirCraftGroupSelDlg dialog

class CAirCraftGroupSelDlg : public CDialog
{
// Construction
public:
	CAirCraftGroupSelDlg(CWnd* pParent, CString& opGrpStr);   // standard constructor

	CString omGrpStr;
// Dialog Data
	//{{AFX_DATA(CAirCraftGroupSelDlg)
	enum { IDD = IDD_GROUP_SEL };
		CListBox	m_GroupNameList;
		CListBox    m_SelGroupNameList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAirCraftGroupSelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAirCraftGroupSelDlg)
	virtual BOOL OnInitDialog();
	void OnIncludeGroup();
	void OnExcludeGroup();
	void OnRemomeGroup();
	void OnOk();
	void OnWithoutPrefix();
	void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AIRCRAFTGROUPSELDLG_H__27FC8383_C5A9_4BE3_BBD1_19A4A04EF50D__INCLUDED_)
