#if !defined(AFX_BLTRULEDLG_H__1AB6B0C3_22FE_11D7_8089_0001022205EE__INCLUDED_)
#define AFX_BLTRULEDLG_H__1AB6B0C3_22FE_11D7_8089_0001022205EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BltRuleDlg.h : header file
//
#include "resrc1.h"
#include "resource.h"
#include "CedaBltData.h"
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// BltRuleDlg dialog

class BltRuleDlg : public CDialog
{
// Construction
public:
	BltRuleDlg(CWnd* pParent, BLTDATA *popBlt);   // standard constructor
	BltRuleDlg();
// Dialog Data
	//{{AFX_DATA(BltRuleDlg)
	enum { IDD = IDD_BLT_RULE_DLG };
	CCSEdit	m_MaxPCtrl;
	CCSEdit	m_MaxBCtrl;
	CCSEdit	m_SequenceCtrl;
	CCSEdit	m_PrioCtrl;
	int		m_Prio;
	int		m_sequence;
	int		m_Maxp;
	int		m_Maxb;
	CString	m_Pral;
	CString	m_Natr;
	CString	m_STYP_List;
	CString	m_PDest;
	CString	m_Flti;
	CString m_ACParam;
	CString m_ACGroup;
	//}}AFX_DATA

	BLTDATA *pomBlt;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BltRuleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(BltRuleDlg)
		// NOTE: the ClassWizard will add member functions here
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	void OnACGroup();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
		DECLARE_SERIAL(BltRuleDlg)
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BLTRULEDLG_H__1AB6B0C3_22FE_11D7_8089_0001022205EE__INCLUDED_)
