// BltRulesViewer.h: Schnittstelle f�r die Klasse BltRulesViewer.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BltRulesViewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_)
#define AFX_BltRulesViewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

struct BLTTABLE_LINEDATA
{
	int Rtnr;
	CString Belt;
	CString T;
	CString Id;
	CString	Prio;
	CString	Sequ;
	CString	Pax;
	CString	Bag;
	CString	Nat;
	CString	Alc;
	CString	Org;
	CString	Service;
	CString	Flti;
	CString ACParam;
	CString ACGroup;
};



#include "CVIEWER.H"
#include "CCSTable.H"
#include "CedaBltData.h"


class BltRulesViewer : public CViewer  
{
public:
	BltRulesViewer();
	virtual ~BltRulesViewer();

	void Attach(CCSTable *popAttachWnd);
    //@ManMemo: ChangeViewTo
    void ChangeViewTo(const char *pcpViewName);
    //@ManMemo: omLines
    CCSPtrArray<BLTTABLE_LINEDATA> omLines;
	bool bmNoUpdatesNow;
private:
	
	
	int CompareBlt(BLTTABLE_LINEDATA *prpBlt1, BLTTABLE_LINEDATA *prpBlt2);

    void MakeLines();

	void MakeBltLineData(BLTDATA *popBlt, CStringArray &ropArray);
	int MakeLine(BLTDATA *prpBlt);
	int MakeLine();
	void MakeHeaderData();
	void MakeColumnData(CCSPtrArray<TABLE_COLUMN> &ropColList,BLTTABLE_LINEDATA *prpLine);

	BOOL FindLine(char *pcpKeya, char *pcpKeyd, int &rilLineno);
	BOOL FindLine(long lpRtnr, int &rilLineno);
public:
    //@ManMemo: DeleteAll
	void DeleteAll();
    //@ManMemo: CreateLine
	//@ManMemo: SetStartEndTime
	int CreateLine(BLTTABLE_LINEDATA *prpBlt);
    //@ManMemo: DeleteLine
	void DeleteLine(int ipLineno);
	void ClearAll();


// Window refreshing routines
public:


    //@ManMemo: UpdateDisplay
	void UpdateDisplay();
private:
    CString omDate;
	// Attributes
private:
	CCSTable *pomTable;
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	int imTotalLines;

// Methods which handle changes (from Data Distributor)
public:
    //@ManMemo: omStartTime
    
    //@ManMemo: omEndTime
	
    //@ManMemo: omDay
	

};

#endif // !defined(AFX_BltRulesViewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_)
