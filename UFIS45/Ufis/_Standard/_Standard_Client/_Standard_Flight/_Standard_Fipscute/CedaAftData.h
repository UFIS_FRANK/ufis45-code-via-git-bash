// CedaAftData.h

#ifndef __CEDAAFTDATA__
#define __CEDAAFTDATA__
 
#include "stdafx.h"
#include "CCSGlobl.h"
#include "CCSPtrArray.h"
#include "CCSCedadata.h"
#include "CCSddx.h"



#define DBFIELDLEN_AFT_ALC2 2
#define DBFIELDLEN_AFT_ALC3 3
#define DBFIELDLEN_AFT_FLNO 9
#define DBFIELDLEN_AFT_FLNS 1
#define DBFIELDLEN_AFT_FLTN 5
#define DBFIELDLEN_AFT_FTYP 1
#define DBFIELDLEN_AFT_GTD1 5
#define DBFIELDLEN_AFT_GTD2 5
#define DBFIELDLEN_AFT_GTA1 5
#define DBFIELDLEN_AFT_GTA2 5
#define DBFIELDLEN_AFT_REMP 4



//---------------------------------------------------------------------------------------------------------

struct AFTDATA 
{
	long	Urno;

	char 	 Alc2[DBFIELDLEN_AFT_ALC2 + 1]; 	// Fluggesellschaft (Airline 2-Letter Code)
	char 	 Alc3[DBFIELDLEN_AFT_ALC3 + 1]; 	// Fluggesellschaft (Airline 3-Letter Code)

	char 	 Flno[DBFIELDLEN_AFT_FLNO + 1]; 	// komplette Flugnummer (Airline, Nummer, Suffix)
	char 	 Flns[DBFIELDLEN_AFT_FLNS + 1]; 	// Suffix
	char 	 Fltn[DBFIELDLEN_AFT_FLTN + 1]; 	// Flugnummer

	char	 Ftyp[DBFIELDLEN_AFT_FTYP + 1];		// Flight typ ('O', ...)

	CTime	Tifd;		// Beste Abflugzeit

	char 	 Gtd1[DBFIELDLEN_AFT_GTD1 + 1]; 	// Gate 1 Abflug
	char 	 Gtd2[DBFIELDLEN_AFT_GTD2 + 1]; 	// Doppelgate Abflug
	char 	 Gta1[DBFIELDLEN_AFT_GTA1 + 1]; 	// Gate 1 Ankunft
	char 	 Gta2[DBFIELDLEN_AFT_GTA2 + 1]; 	// Gate 2 Ankunft

	CTime 	 Gd1b; 	// Belegung Gate 1 Abflug geplanter Beginn
	CTime 	 Gd1e; 	// Belegung Gate 1 Abflug geplantes Ende
	CTime 	 Gd2b; 	// Belegung Gate 2 Abflug geplanter Beginn
	CTime 	 Gd2e; 	// Belegung Gate 2 Abflug geplantes Ende
	CTime 	 Ga1b; 	// Belegung Gate 1 Ankunft geplanter Beginn
	CTime 	 Ga1e; 	// Belegung Gate 1 Ankunft geplantes Ende
	CTime 	 Ga2b; 	// Belegung Gate 2 Ankunft geplanter Beginn
	CTime 	 Ga2e; 	// Belegung Gate 2 Ankunft geplantes Ende


	CTime 	 Gd1x; 	// Belegung Gate 1 Abflug aktueller Beginn
	CTime 	 Gd1y; 	// Belegung Gate 1 Abflug aktuelles Ende
	CTime 	 Gd2x; 	// Belegung Gate 2 Abflug aktueller Beginn
	CTime 	 Gd2y; 	// Belegung Gate 2 Abflug aktuelles Ende
	CTime 	 Ga1x; 	// Belegung Gate 1 Ankunft aktueller Beginn
	CTime 	 Ga1y; 	// Belegung Gate 1 Ankunft aktuelles Ende
	CTime 	 Ga2x; 	// Belegung Gate 2 Ankunft aktueller Beginn
	CTime 	 Ga2y; 	// Belegung Gate 2 Ankunft aktuelles Ende

	char	 Remp[DBFIELDLEN_AFT_REMP + 1];	// FIDS Remark Code

	AFTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Urno		=   0;

		Tifd		=	TIMENULL;

		Gd1b		=	TIMENULL;
		Gd1e		=	TIMENULL;
		Gd2b		=	TIMENULL;
		Gd2e		=	TIMENULL;
		Ga1b		=	TIMENULL;
		Ga1e		=	TIMENULL;
		Ga2b		=	TIMENULL;
		Ga2e		=	TIMENULL;

		Gd1x		=	TIMENULL;
		Gd1y		=	TIMENULL;
		Gd2x		=	TIMENULL;
		Gd2y		=	TIMENULL;
		Ga1x		=	TIMENULL;
		Ga1y		=	TIMENULL;
		Ga2x		=	TIMENULL;
		Ga2y		=	TIMENULL;
	}

	const AFTDATA& operator= ( const AFTDATA& s)
	{
		Urno = s.Urno;

		strncpy(Alc2, s.Alc2, DBFIELDLEN_AFT_ALC2);
		strncpy(Alc3, s.Alc3, DBFIELDLEN_AFT_ALC3);

		strncpy(Flno, s.Flno, DBFIELDLEN_AFT_FLNO);
		strncpy(Flns, s.Flns, DBFIELDLEN_AFT_FLNS);
		strncpy(Fltn, s.Fltn, DBFIELDLEN_AFT_FLTN);

		strncpy(Ftyp, s.Ftyp, DBFIELDLEN_AFT_FTYP);

		Tifd = s.Tifd;

		strncpy(Gtd1, s.Gtd1, DBFIELDLEN_AFT_GTD1);
		strncpy(Gtd2, s.Gtd2, DBFIELDLEN_AFT_GTD2);
		strncpy(Gta1, s.Gta1, DBFIELDLEN_AFT_GTA1);
		strncpy(Gta2, s.Gta2, DBFIELDLEN_AFT_GTA2);

		Gd1e = s.Gd1e;
		Gd1b = s.Gd1b;
		Gd2e = s.Gd2e;
		Gd2b = s.Gd2b;
		Ga1e = s.Ga1e;
		Ga1b = s.Ga1b;
		Ga2e = s.Ga2e;
		Ga2b = s.Ga2b;

		Gd1x = s.Gd1x;
		Gd1y = s.Gd1y;
		Gd2x = s.Gd2x;
		Gd2y = s.Gd2y;
		Ga1x = s.Ga1x;
		Ga1y = s.Ga1y;
		Ga2x = s.Ga2x;
		Ga2y = s.Ga2y;

		strncpy(Remp, s.Remp, DBFIELDLEN_AFT_REMP);

		return *this;

	}


}; // end AftDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaAftData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CPtrArray omData;
	char pcmListOfFields[2048];

// Operations
public:
    CedaAftData();
	~CedaAftData();
	void Register(void);
	void UnRegister(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(CString opWhere, bool bpDdx = true);
	bool ReadUrno(long lpUrno, bool bpDdx /* = true */ );
	bool ReadCcaFlights(const CStringArray &ropFlnus, bool bpDdx = true); 
	bool ReadGateRecs(const CString &ropGate, bool bpDdx = true);

	bool ReadOneFlight(long lpUrno, AFTDATA &ropFlight); // const;

	//bool ReadFlights(const CStringArray &ropUrnos);
	//bool ReadFlights(const CString &ropGate);

	void ProcessAftBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	AFTDATA *GetRecByUrno(long lpUrno) const;

	//bool GetOpenGateRecs(CPtrArray *popOpenGateRecs) const;

	bool GetMostRecentScheduled(CPtrArray *popAfts);
	bool Update(AFTDATA *prpAft);
	//bool IsOpen(const AFTDATA &ropAft) const;


	// Private methods
private:
	//AFTDATA rmCuteAftData;
	CString omCurrGate;
	CString omCurrSel;

	//bool IsOpen(const AFTDATA &ropCca) const;

	bool InsertInternal(AFTDATA *prpCca, bool bpDdx = true);
	bool UpdateInternal(AFTDATA *prpCca, bool bpDdx = true);
	bool DeleteInternal(AFTDATA *prpCca, bool bpDdx = true);

	bool IsChangeRelevant(const char *prpBcFields, const char *prpBcData);
	bool ReloadData();

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAAFTDATA__
