// CuteIFDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CuteIF.h"
#include "CuteIFDlg.h"
#include "CCSBcHandle.h"
#include <afxinet.h>
#include <strstrea.h>
#include "CedaBasicData.h"
#include "cdib.h"
#include "BasicData.h"
#include "ClntTypes.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CString omAddWhereIfClosing("DSEQ = '1'");
CString omAddWhereIfUpdating("DSEQ = '0'");


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//--PROCESS-CF---------------------------------------------------------------------------------------------

static void  ProcessBC(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CCuteIFDlg *)vpInstance)->ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}




/////////////////////////////////////////////////////////////////////////////
// CCuteIFDlg dialog

CCuteIFDlg::CCuteIFDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCuteIFDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCuteIFDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_UFISAmSink.SetLauncher(this);


	if (bgLogoSourceHttp)
	{
		pomInternetSession = new CInternetSession(_T("CuteIF"), 1, 0);
		pomHttpServer = pomInternetSession->GetHttpConnection(ogHttpServer, igHttpPort);
	}
	else
	{
		pomInternetSession = NULL;
		pomHttpServer = NULL;
	}

	bmBCEnabled = true;

	bmClosed = true;

	prmFlzRecord = NULL;

	pomBltFreeTextDlg = NULL;

}


CCuteIFDlg::~CCuteIFDlg()
{
	if (pomBltFreeTextDlg != NULL)
	{
		delete pomBltFreeTextDlg;
	}
	
	if (pomHttpServer != NULL)
	{
		pomHttpServer->Close();
		//delete pomHttpServer;
	}
	if (pomInternetSession != NULL)
	{
		pomInternetSession->Close();
		delete pomInternetSession;
	}

	if (bgComOk)
		pConnect->DetachApp(static_cast<long>(FipsCUTE));

	if (prmFlzRecord)
		delete prmFlzRecord;
	
}


void CCuteIFDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCuteIFDlg)
	DDX_Control(pDX, IDC_CKICGAT_STATE, m_CS_State);
	DDX_Control(pDX, IDC_FREE1, m_CE_Free1);	
	DDX_Control(pDX, IDC_FREE2, m_CE_Free2);	
	DDX_Control(pDX, IDC_LOGO_SEL, m_CCB_LogoSelect);
	DDX_Control(pDX, IDC_CHECKIN_SEL, m_CCB_CheckinSel);
	DDX_Control(pDX, IDC_GATE_SEL, m_CCB_GateSel);
	DDX_Control(pDX, IDC_FIDS_REMARKS, m_CCB_FidsRemarks);
	DDX_Control(pDX, IDC_CKICGATE_CLOSE, m_CB_Close);
	DDX_Control(pDX, IDC_TRANSMIT, m_CB_Transmit);
	DDX_Control(pDX, IDC_FLT_BOR, m_CS_FlightTBorder);
	DDX_Control(pDX, IDC_SIGN, m_Text_Sign);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCuteIFDlg, CDialog)
	//{{AFX_MSG_MAP(CCuteIFDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_WM_TIMER()
	ON_MESSAGE(WM_BCADD,OnBcAdd)
	ON_CBN_SELCHANGE(IDC_LOGO_SEL, OnLogoSelChange)
	ON_CBN_SELCHANGE(IDC_CHECKIN_SEL, OnCheckinSelChange)
	ON_CBN_SELCHANGE(IDC_GATE_SEL, OnGateSelChange)
	ON_BN_CLICKED(IDC_CKICGATE_CLOSE, OnCkicGateClose)
	ON_BN_CLICKED(IDC_TRANSMIT, OnTransmit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCuteIFDlg message handlers

BOOL CCuteIFDlg::OnInitDialog()
{
 
	CCuteIFApp::TriggerWaitCursor(true);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	CDialog::OnInitDialog();


	// setup text sign
	m_Text_Sign.SetTypeToString();
	m_Text_Sign.SetInitText(GetString(IDS_STRING114));
	//m_Text_Sign.SetReadOnly();
	m_Text_Sign.SetEditFont(&ogCourier_Bold_10);
	m_Text_Sign.SetTextColor(RED);
	m_Text_Sign.SetBKColor(SILVER);
	m_Text_Sign.ShowWindow(SW_HIDE);


	ogCommHandler.RegisterBcWindow(this);
	ogBcHandle.GetBc();

	omCkicName="";
	omGateName="";

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// hide help button
	CWnd *polHelpButton = GetDlgItem(ID_HELP);
	if (polHelpButton != NULL)
		polHelpButton->ShowWindow(SW_HIDE);

	
	// hide resource selection
	// checkin
	CWnd *polWnd;
	polWnd = GetDlgItem(IDC_CKIC_STATIC);
	if (polWnd != NULL)
		polWnd->ShowWindow(SW_HIDE);
	polWnd = GetDlgItem(IDC_CHECKIN_SEL);
	if (polWnd != NULL)
		polWnd->ShowWindow(SW_HIDE);
	// gate
	polWnd = GetDlgItem(IDC_GATE_STATIC);
	if (polWnd != NULL)
		polWnd->ShowWindow(SW_HIDE);
	polWnd = GetDlgItem(IDC_GATE_SEL);
	if (polWnd != NULL)
		polWnd->ShowWindow(SW_HIDE);


	// get position of logo
	WINDOWPLACEMENT olWndPl; 
	GetDlgItem(IDC_DUMMYLOGO)->GetWindowPlacement(&olWndPl);

	omLogoRect.left = olWndPl.rcNormalPosition.left;
	omLogoRect.right = omLogoRect.left + MAX_LOGO_SIZE_X;
	omLogoRect.top = olWndPl.rcNormalPosition.top;
	omLogoRect.bottom = omLogoRect.top + MAX_LOGO_SIZE_Y;
	

	// init flight table
	CRect olRectBorder;
	m_CS_FlightTBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	omFlightTable.SetSelectMode(LBS_MULTIPLESEL);
	omFlightTable.SetHeaderSpacing(0);
	//omFlightTable.SetMiniTable();
	//omFlightTable.SetTableEditable(false);
    omFlightTable.SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);



    SetTimer(0, (UINT) RELOAD_SEC * 1000, NULL); // update every minute


	LPVOID lpvResource = (LPVOID) ::LoadResource(NULL,
		::FindResource(NULL, MAKEINTRESOURCE(IDB_NOLOGO),
		RT_BITMAP));
	omLogo.AttachMemory(lpvResource); // no need for


	//omLogo.LoadBitmap(IDB_NOLOGO);

	if (bgLogoSourceHttp)
	{
		// Check Internet connection
		if (!CheckInternetCon(ogHttpLogoPath))
		{
			CCuteIFApp::DisableLogos();
		}
	}

	// Get list of logos
	GetListOfLogos();

	GetListOfCheckins();
	GetListOfGates();

	prmFldData = omFldData.GetCuteRecord();

	LoadData();

	ogDdx.Register((void *)this, FLDCUTE_CHANGE, CString("FLDCUTE_CHANGE"), CString("JobDataChange"), ProcessBC);
	ogDdx.Register((void *)this, CCA_NEW, CString("CCA_NEW"), CString("JobDataChange"), ProcessBC);
	ogDdx.Register((void *)this, CCA_CHANGE, CString("CCA_CHANGE"), CString("JobDataChange"), ProcessBC);
	ogDdx.Register((void *)this, CCA_DELETE, CString("CCA_DELETE"), CString("JobDataChange"), ProcessBC);
	ogDdx.Register((void *)this, FLIGHT_NEW, CString("FLIGHT_NEW"), CString("JobDataChange"), ProcessBC);
	ogDdx.Register((void *)this, FLIGHT_CHANGE, CString("FLIGHT_CHANGE"), CString("JobDataChange"), ProcessBC);
	ogDdx.Register((void *)this, FLIGHT_DELETE, CString("FLIGHT_DELETE"), CString("JobDataChange"), ProcessBC);
	ogDdx.Register((void *)this, FLZ_CHANGE, CString("LOGOMAP_CHANGE"), CString("JobDataChange"), ProcessBC);
	ogDdx.Register((void *)this, FXT_CHANGE, CString("FXT_CHANGE"), CString("JobDataChange"), ProcessBC);

	InitCOMInterface();

	/*
	CWnd *polWnd;
	polWnd = GetDlgItem(IDC_FREE1);
	if (polWnd != NULL)
		polWnd->SetFocus();
	*/

	CCuteIFApp::TriggerWaitCursor(false);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}



void CCuteIFDlg::DrawFlightTHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	omFlightTable.SetShowSelection(true);
	omFlightTable.ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[3];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 2 * 9; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = GetString(IDS_STRING109);

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 10 * 9; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = GetString(IDS_STRING107);

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 5 * 9; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = GetString(IDS_STRING108);
	
	for (int i = 0; i < 3; i++)
	{
		omHeaderDataArray.Add(prlHeader[i]);
	}

	omFlightTable.SetHeaderFields(omHeaderDataArray);
	omFlightTable.SetDefaultSeparator();
	//omFlightTable.SetTableEditable(false);
	omHeaderDataArray.DeleteAll();
	omFlightTable.DisplayTable();

}



bool CCuteIFDlg::CheckInternetCon(const CString &ropHttpLogoPath)
{
	CHttpFile *polHttpFile = pomHttpServer->OpenRequest(CHttpConnection::HTTP_VERB_GET,
			ropHttpLogoPath, "*/*", 1, NULL, NULL, INTERNET_FLAG_EXISTING_CONNECT | INTERNET_FLAG_NO_AUTO_REDIRECT);
	try
	{
		if (!polHttpFile->SendRequest())
		{
			delete polHttpFile;
			return false;
		}

		polHttpFile->Close();
		delete polHttpFile;
	}
	catch (CInternetException olIntExc)
	{		
		delete polHttpFile;
		return false;
	}

	return true;
}



void CCuteIFDlg::GetListOfLogos()
{

	m_CCB_LogoSelect.ResetContent();
	m_CCB_LogoSelect.AddString("<NONE>");
	
	int ilCount = ogBCD.GetDataCount("FLG");

	for (int i = 0; i < ilCount; i++)
	{
		m_CCB_LogoSelect.AddString(ogBCD.GetField("FLG", i, "LGFN"));
	}

	m_CCB_LogoSelect.SetCurSel(0);	
}


void CCuteIFDlg::SetFidsRemarks()
{
	m_CCB_FidsRemarks.ResetContent();

	char clTyp = ' ';
	if (!omCkicName.IsEmpty())
		clTyp = 'C';
	else if (!omGateName.IsEmpty())
		clTyp = 'G';
	else
		return;
	
	int ilCount = ogBCD.GetDataCount("FID");

	for (int i = 0; i < ilCount; i++)
	{
		if (ogBCD.GetField("FID", i, "REMT")[0] == clTyp)
			m_CCB_FidsRemarks.AddString(ogBCD.GetField("FID", i, "BEME"));
	}
	
}


void CCuteIFDlg::GetListOfCheckins()
{
	// only for testing
	m_CCB_CheckinSel.ResetContent();
	m_CCB_CheckinSel.AddString("001");
	m_CCB_CheckinSel.AddString("002");
	m_CCB_CheckinSel.AddString("003");
	m_CCB_CheckinSel.AddString("004");
	m_CCB_CheckinSel.AddString("005");
	m_CCB_CheckinSel.AddString("006");
	m_CCB_CheckinSel.AddString("044");
	m_CCB_CheckinSel.AddString("045");

}



void CCuteIFDlg::GetListOfGates()
{
	// only for testing
	m_CCB_GateSel.ResetContent();
	m_CCB_GateSel.AddString("B28");
	m_CCB_GateSel.AddString("A01");
	m_CCB_GateSel.AddString("A02");
	m_CCB_GateSel.AddString("A03");
	m_CCB_GateSel.AddString("A04");
	m_CCB_GateSel.AddString("A05");
	m_CCB_GateSel.AddString("A06");

}




void CCuteIFDlg::OnLogoSelChange()
{
	CCuteIFApp::TriggerWaitCursor(true);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	int ilItemID;
	if ((ilItemID = m_CCB_LogoSelect.GetCurSel()) != CB_ERR)
	{
		CString olSelLogoName;
		CString olLogoName;
		m_CCB_LogoSelect.GetLBText(ilItemID, olSelLogoName);
		if (ogBCD.GetField("FLG", "LGFN", olSelLogoName, "LGSN", olLogoName))
		{
			SetLogo(olLogoName);
		}
	}

	CCuteIFApp::TriggerWaitCursor(false);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}



void CCuteIFDlg::OnCheckinSelChange()
{
	int ilItemID;
	if ((ilItemID = m_CCB_CheckinSel.GetCurSel()) != CB_ERR)
	{
		CString olCkic;
		m_CCB_CheckinSel.GetLBText(ilItemID, olCkic);
		NewCkic(olCkic);
	}
}


void CCuteIFDlg::NewCkic(const CString &ropCkic, long lpFlightUrno /* = 0 */)
{
	omCkicName = ropCkic;
	omGateName.Empty();

	m_CCB_CheckinSel.SetWindowText(omCkicName);
	m_CCB_GateSel.SetWindowText("");

	SetFidsRemarks();
	LoadData(lpFlightUrno);
}


void CCuteIFDlg::OnGateSelChange()
{
	int ilItemID;
	if ((ilItemID = m_CCB_GateSel.GetCurSel()) != CB_ERR)
	{
		CString olGate;
		m_CCB_GateSel.GetLBText(ilItemID, olGate);
		NewGate(olGate);
	}
}


void CCuteIFDlg::NewGate(const CString &ropGate, long lpFlightUrno /* = 0 */)
{
	omGateName = ropGate;
	omCkicName.Empty();

	m_CCB_GateSel.SetWindowText(omGateName);
	m_CCB_CheckinSel.SetWindowText("");

	SetFidsRemarks();
	LoadData(lpFlightUrno);
}



void CCuteIFDlg::OnCkicGateClose()
{
	CCuteIFApp::TriggerWaitCursor(true);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	// close checkin/gate for all open flights


	// get selected remark
	CString olFIDSRemCode;
	GetSelFIDSRemCode(olFIDSRemCode);

	if (!omCkicName.IsEmpty())
	{

		// get current utc time
		CTime olCurrUtc = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olCurrUtc);

		// set the actual closing time to current utc time 
		//CCADATA *prlCca;
		CCADATA rlNewCca;
		for (int i = 0; i < omCcaList.GetSize(); i++)
		{
			rlNewCca = *((CCADATA *) omCcaList[i]);
			rlNewCca.Ckea = olCurrUtc;
			strncpy(rlNewCca.Disp, olFIDSRemCode, DBFIELDLEN_CCA_DISP);
			omCcaData.SaveDiff(&rlNewCca);
		}
		prmFldData->Dseq = -1;
		omFldData.SaveCuteRecord(&omAddWhereIfClosing);

		LoadData(rmSelFlight.Urno);
	}

	if (!omGateName.IsEmpty())
	{

		// get current utc time
		CTime olCurrUtc = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olCurrUtc);

		// set the actual closing time to current utc time 
		AFTDATA *prlAft;
		AFTDATA rlNewAft;
		for (int i = 0; i < omGateList.GetSize(); i++)
		{
			prlAft = (AFTDATA *) omGateList[i];
			rlNewAft = *prlAft;
			strncpy(rlNewAft.Remp, olFIDSRemCode, DBFIELDLEN_AFT_REMP);
			if (strcmp(prlAft->Gtd1, omGateName) == 0)
			{
				rlNewAft.Gd1y = olCurrUtc;
			}
			if (strcmp(prlAft->Gtd2, omGateName) == 0)
			{
				rlNewAft.Gd2y = olCurrUtc;
			}
			omAftData.Update(&rlNewAft);
		}
		prmFldData->Dseq = -1;
		omFldData.SaveCuteRecord(&omAddWhereIfClosing);

		LoadData(rmSelFlight.Urno);
	}

	CCuteIFApp::TriggerWaitCursor(false);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}




void CCuteIFDlg::OnTransmit()
{
	if (bmClosed)
	{
		// only open resource if flights are selected
		if (!AreFlightsSelected())
			return;
	}


	CCuteIFApp::TriggerWaitCursor(true);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	bmBCEnabled = false;

	if (!omCkicName.IsEmpty())
	{
		CCADATA *prlCca = GetCurrentCcaData();		
		if (prlCca)
		{
			// Cca-Data found!
			if (strcmp(prlCca->Ctyp, "C") == 0)
			{
				// common checkin
				SaveCuteRecord(0, prlCca->Urno);
			}
			else
			{
				// dedicated checkin
				SaveCuteRecord(prlCca->Flnu, prlCca->Urno);
			}
			UpdateCkicData();
		}
	}
	else if (!omGateName.IsEmpty())
	{
		AFTDATA *prlFlight = GetCurrentAftData();
		if (prlFlight)
		{
			// Flight-Data found
			SaveCuteRecord(prlFlight->Urno, 0);
			UpdateGateData();
		}
	}

	bmBCEnabled = true;
	
	RefreshData();

	CCuteIFApp::TriggerWaitCursor(false);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}


bool CCuteIFDlg::GetSelFIDSRemCode(CString &ropFIDSRemCode)
{
	CString olFidRem;
	m_CCB_FidsRemarks.GetWindowText(olFidRem);
	return ogBCD.GetField("FID", "BEME", olFidRem, "CODE", ropFIDSRemCode);
}




bool CCuteIFDlg::SaveCuteRecord(long lpAftUrno, long lpCcaUrno)
{

	CTime olCurrUtc = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrUtc);


	// FIDS remark
	CString olFidCode;
	GetSelFIDSRemCode(olFidCode);
	strncpy(prmFldData->Crec, olFidCode, DBFIELDLEN_CREC);
	
	if (!omCkicName.IsEmpty())
	{
		// checkin
		if (lpAftUrno == 0)
		{
			// common checkin
			strncpy(prmFldData->Ctyp, " ", DBFIELDLEN_CTYP);
			prmFldData->Aurn = 0;
			strncpy(prmFldData->Rtab, "CCA", DBFIELDLEN_RTAB);
			prmFldData->Rurn = lpCcaUrno;
		}
		else
		{
			// dedicated checkin
			strncpy(prmFldData->Ctyp, "M", DBFIELDLEN_CTYP);
			prmFldData->Aurn = lpAftUrno;
			strncpy(prmFldData->Rtab, "CCA", DBFIELDLEN_RTAB);
			prmFldData->Rurn = lpCcaUrno;
		}


		/*
		CCADATA *prlCca = GetCurrentCcaData();		
		if (prlCca)
		{
			// Cca-Data found!
			if (strcmp(prlCca->Ctyp, "C") == 0)
			{
				// common checkin
				strncpy(prmFldData->Ctyp, " ", DBFIELDLEN_CTYP);
				prmFldData->Aurn = 0;
				strncpy(prmFldData->Rtab, "CCA", DBFIELDLEN_RTAB);
				prmFldData->Rurn = prlCca->Urno;
			}
			else
			{
				// dedicated checkin
				strncpy(prmFldData->Ctyp, "M", DBFIELDLEN_CTYP);
				prmFldData->Aurn = prlCca->Flnu;
				strncpy(prmFldData->Rtab, "CCA", DBFIELDLEN_RTAB);
				prmFldData->Rurn = prlCca->Urno;
			}
		}
		*/


	}
	else
	{
		// gate
		if (lpAftUrno != 0)
		{
			strncpy(prmFldData->Ctyp, "M", DBFIELDLEN_CTYP);
			prmFldData->Aurn = lpAftUrno;
			prmFldData->Rtab[0] = '\0';
			prmFldData->Rurn = 0;
		}
		/*
		AFTDATA *prlFlight = GetCurrentAftData();
		if (prlFlight)
		{
			// Flight-Data found
			strncpy(prmFldData->Ctyp, "M", DBFIELDLEN_CTYP);
			prmFldData->Aurn = prlFlight->Urno;
			prmFldData->Rtab[0] = '\0';
			prmFldData->Rurn = 0;
		}
		*/

		if (!bmClosed)
		{
			// set 'boarding' and 'final call' time if necessary
			if (olFidCode == REMCODE_BOARDING)
			{
				prmFldData->Abti = olCurrUtc;
			}
			if (olFidCode == REMCODE_FINALCALL)
			{
				prmFldData->Afti = olCurrUtc;
			}
 
		}
	}

	// set dseq != 1, since all other data records are updated!
	prmFldData->Dseq = 0;
	if (omFldData.SaveCuteRecord())
	{
		// update logo
		CString olLogoName;
		m_CCB_LogoSelect.GetWindowText(olLogoName);
		DBUpdateLogo(prmFldData->Urno, olLogoName);

		// update freetext
		CString olText;
		m_CE_Free1.GetWindowText(olText);
		omFxtData.DBUpdateFreetext(prmFldData->Urno, 1, olText);
		m_CE_Free2.GetWindowText(olText);
		omFxtData.DBUpdateFreetext(prmFldData->Urno, 2, olText);

		prmFldData->Dseq = 1;
		omFldData.SaveCuteRecord(&omAddWhereIfUpdating);

		return true;
	}

	return false;
}


AFTDATA *CCuteIFDlg::GetCurrentAftData()
{
	if (omGateName.IsEmpty())
		return NULL;

	CListBox *polLB = omFlightTable.GetCTableListBox();
	if (polLB == NULL)
		return NULL;

	if (bmClosed)
	{
		// Get Urno of the first selected flight
		for (int i = 0; i < polLB->GetCount(); i++)
		{
			if (polLB->GetSel(i) > 0)
			{
				return (AFTDATA *) omGateList[i];
			}
		}
	}
	else
	{
		// Get Urno of the actual flight
		if (omGateList.GetSize() > 0)
		{
			return (AFTDATA *) omGateList[0];
		}
	}

	return NULL;
}



CCADATA *CCuteIFDlg::GetCurrentCcaData()
{
	if (omCkicName.IsEmpty())
		return NULL;

	CListBox *polLB = omFlightTable.GetCTableListBox();
	if (polLB == NULL)
		return NULL;

	if (bmClosed)
	{
		// Get Flnu of the first selected checkin-Counter
		for (int i = 0; i < polLB->GetCount(); i++)
		{
			if (polLB->GetSel(i) > 0)
			{
				return (CCADATA *)omCcaList[i];
			}
		}
	}
	else
	{
		// Get Flnu of the actual checkin-Counter
		if (omCcaList.GetSize() > 0)
		{
			return (CCADATA *)omCcaList[0];
		}
	}

	return NULL;
}



bool CCuteIFDlg::UpdateCkicData()
{

	// get current utc time
	CTime olCurrUtc = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrUtc);
	

	// Scan all Items
	CListBox *polLB = omFlightTable.GetCTableListBox();
	if (polLB == NULL)
		return false;
	
	// get selected remark
	CString olFIDSRemCode;
	GetSelFIDSRemCode(olFIDSRemCode);

	if (bmClosed)
	{
		// open counter for all selected flights
		//CCADATA *prlCca;
		CCADATA rlNewCca;
		for (int ilCount = 0; ilCount < polLB->GetCount(); ilCount++)
		{
			if (polLB->GetSel(ilCount) > 0)
			{
				rlNewCca = *((CCADATA *) omCcaList[ilCount]);
				strncpy(rlNewCca.Disp, olFIDSRemCode, DBFIELDLEN_CCA_DISP);
				rlNewCca.Ckba = olCurrUtc;
				rlNewCca.Ckea = TIMENULL;
				omCcaData.SaveDiff(&rlNewCca);
			}
		}
	}
	else
	{
		// update FIDS remark for all counter records
		//CCADATA *prlCca;
		CCADATA rlNewCca;
		for (int ilCount = 0; ilCount < polLB->GetCount(); ilCount++)
		{
			rlNewCca = *((CCADATA *) omCcaList[ilCount]);
			strncpy(rlNewCca.Disp, olFIDSRemCode, DBFIELDLEN_CCA_DISP);
			omCcaData.SaveDiff(&rlNewCca);
		}


	}
	return true;
}


bool CCuteIFDlg::UpdateGateData()
{

	// get current utc time
	CTime olCurrUtc = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrUtc);
	

	// Scan all Items
	AFTDATA *prlAft;
	AFTDATA rlNewAft;

	// Scan all Items
	CListBox *polLB = omFlightTable.GetCTableListBox();
	if (polLB == NULL)
		return false;
	
	// get selected remark
	CString olFIDSRemCode;
	GetSelFIDSRemCode(olFIDSRemCode);

	if (bmClosed)
	{
		// open gate for all selected flights
		for (int ilCount = 0; ilCount < polLB->GetCount(); ilCount++)
		{
			if (polLB->GetSel(ilCount) > 0)
			{
				prlAft = (AFTDATA *) omGateList[ilCount];
				rlNewAft = *prlAft;
				// set FIDS remark
				strncpy(rlNewAft.Remp, olFIDSRemCode, DBFIELDLEN_AFT_REMP);
				// set opening time
				if (strcmp(prlAft->Gtd1, omGateName) == 0)
				{
					rlNewAft.Gd1x = olCurrUtc;
					rlNewAft.Gd1y = TIMENULL;
				}
				if (strcmp(prlAft->Gtd2, omGateName) == 0)
				{
					rlNewAft.Gd2x = olCurrUtc;
					rlNewAft.Gd2y = TIMENULL;
				}
				omAftData.Update(&rlNewAft);
			}
		}
	}
	else
	{
		// update FIDS remark for all gates
		for (int ilCount = 0; ilCount < polLB->GetCount(); ilCount++)
		{
			prlAft = (AFTDATA *) omGateList[ilCount];
			rlNewAft = *prlAft;
			strncpy(rlNewAft.Remp, olFIDSRemCode, DBFIELDLEN_AFT_REMP);
			omAftData.Update(&rlNewAft);
		}
	}

	return true;
}



/*
bool CCuteIFDlg::GetAllOpenFlights(CString &ropFlightList) const
{

	ropFlightList.Empty();

	if (!omCkicName.IsEmpty())
	{
		CCADATA *prlCca;
		for (int i = 0; i < omCcaList.GetSize(); i++)
		{		
			prlCca = (CCADATA *) omCcaList[i];
			if (strcmp("C", prlCca->Ctyp) == 0)
			{
				CString olAlc;
				CString olFlnu;
				olFlnu.Format("%ld", prlCca->Flnu);
				ogBCD.GetField("ALT", "URNO", olFlnu, "ALC3", olAlc);
				ropFlightList += olAlc;
			}
			else
			{
				ropFlightList += prlCca->Flno;
			}
			ropFlightList += '|';
		}
	}


	if (!omGateName.IsEmpty())
	{
		AFTDATA *prlAft;
		for (int i = 0; i < omGateList.GetSize(); i++)
		{		
			prlAft = (AFTDATA *) omGateList[i];
			ropFlightList += prlAft->Flno;
			ropFlightList += '|';
		}
	}


	// remove last '|'
	if (!ropFlightList.IsEmpty())
		ropFlightList = ropFlightList.Left(ropFlightList.GetLength() - 1);

	return true;
}
*/



bool CCuteIFDlg::AreFlightsSelected()
{
	CListBox *polLB = omFlightTable.GetCTableListBox();
	if (polLB == NULL)
		return false;

	for (int i = 0; i < polLB->GetCount(); i++)
	{
		if (polLB->GetSel(i) > 0)
		{
			return true;
		}
	}

	return false;
}


/*
bool CCuteIFDlg::GetSelListOfFlights(CString &ropFlightList)
{

	ropFlightList.Empty();

	CListBox *polLB = omFlightTable.GetCTableListBox();
	if (polLB == NULL)
		return false;

	if (!omCkicName.IsEmpty())
	{
		// Scan all Items
		CCADATA *prlCca;
		for (int i = 0; i < polLB->GetCount(); i++)
		{
			if (polLB->GetSel(i) > 0)
			{
				prlCca = (CCADATA *) omCcaList[i];
				if (strcmp("C", prlCca->Ctyp) == 0)
				{
					CString olAlc;
					CString olFlnu;
					olFlnu.Format("%ld", prlCca->Flnu);
					ogBCD.GetField("ALT", "URNO", olFlnu, "ALC3", olAlc);
					ropFlightList += olAlc;
				}
				else
				{
					ropFlightList += prlCca->Flno;
				}
				ropFlightList += '|';

			}
		}
	}

	if (!omGateName.IsEmpty())
	{
		AFTDATA *prlAft;
		for (int i = 0; i < polLB->GetCount(); i++)
		{
			if (polLB->GetSel(i) > 0)
			{
				prlAft = (AFTDATA *) omGateList[i];
				ropFlightList += prlAft->Flno;
				ropFlightList += '|';

			}
		}
	}
	

	// remove last '|'
	if (!ropFlightList.IsEmpty())
		ropFlightList = ropFlightList.Left(ropFlightList.GetLength() - 1);
	
	
	return true;
}
*/



bool CCuteIFDlg::GetAlcFromSelFlight(CString &ropAlc)
{
	CListBox *polLB = omFlightTable.GetCTableListBox();
	if (polLB == NULL)
		return false;


	if (!omCkicName.IsEmpty())
	{	
		for (int i = 0; i < polLB->GetCount(); i++)
		{
			if (polLB->GetSel(i) > 0)
			{
				CCADATA *prlCca = (CCADATA *) omCcaList[i];
				if (strcmp(prlCca->Ctyp, "C") == 0)
				{
					CString olFlnu;
					olFlnu.Format("%ld", prlCca->Flnu);
					ogBCD.GetField("ALT", "URNO", olFlnu, "URNO", ropAlc);
					return true;
				}
				else
				{
					if (omCcaData.GetAlc3FromRelFlight(prlCca, ropAlc))
						return true;
				}
			}
		}
		return false;
		
	}

	if (!omGateName.IsEmpty())
	{
		for (int i = 0; i < polLB->GetCount(); i++)
		{
			if (polLB->GetSel(i) > 0)
			{
				AFTDATA *prlAft = (AFTDATA *) omGateList[i];
				ropAlc = prlAft->Alc3;
				return true;
			}
		}
		return false;
	}
	

	return false;
}


bool CCuteIFDlg::GetMonitorIP(CString &ropMonitorIP)
{
	CString olWhere;
	if (!omCkicName.IsEmpty())
	{
		olWhere.Format("WHERE DFFD = 'CKIF' AND DFCO = '%s'", omCkicName);
	}
	else
	{
		olWhere.Format("WHERE DFFD = 'GTD1' AND DFCO = '%s'", omGateName);
	}

	ogBCD.Read("DEV", olWhere);


	if (ogBCD.GetDataCount("DEV") < 1)
		return false;

	ropMonitorIP = ogBCD.GetField("DEV", 0, "DADR");
	
	return true;
}




void CCuteIFDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CCuteIFDlg::OnDestroy()
{
	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCuteIFDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	if (IsIconic())
	{ 
 
		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();

	     //CBitmap *poldbmp;
		 //CDC memdc;

	     // Create a compatible memory DC
		 //memdc.CreateCompatibleDC( &dc );
		 
		 omLogo.UsePalette(&dc); 


	    // Select the bitmap into the DC
		 //poldbmp = memdc.SelectObject( &omLogo );

		 CSize olLogoDim(MAX_LOGO_SIZE_X, MAX_LOGO_SIZE_Y);
		 //CSize olLogoDim = omLogo.GetDimensions();
		 //sizeResourceDib.cx *= 30;
		 //sizeResourceDib.cy *= -30;
		 omLogo.Draw(&dc, CPoint(omLogoRect.left, omLogoRect.top), olLogoDim);


		//BITMAP olBITMAP;
		//omLogo.GetBitmap(&olBITMAP);

		// Copy (BitBlt) bitmap from memory DC to screen DC and stretch it if necessary
		//dc.StretchBlt(omLogoRect.left, omLogoRect.top, MAX_LOGO_SIZE_X, MAX_LOGO_SIZE_Y, &memdc, 0, 0, olBITMAP.bmWidth, olBITMAP.bmHeight, SRCCOPY);

	    //memdc.SelectObject( poldbmp );
		//memdc.DeleteDC();
	}
}


HCURSOR CCuteIFDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


LONG CCuteIFDlg::OnBcAdd(UINT wParam, LONG /*lParam*/)
{

	ogBcHandle.GetBc(wParam);
	//ogBcHandle.GetBc();
	return TRUE;
}




void CCuteIFDlg::LoadData(long lpFlightUrno /* = 0 */)
{
	if (bmClosed)
	{
		KillTimer(0);
		SetTimer(0, (UINT) RELOAD_SEC * 1000, NULL); // update every minute
	}

	CCuteIFApp::TriggerWaitCursor(true);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));


	// Read the selected flight!
	rmSelFlight.Urno = 0;
	if (lpFlightUrno > 0)
	{
		// Flight related mode! Read the selected flight!
		omAftData.ReadOneFlight(lpFlightUrno, rmSelFlight);
	}

	m_CB_Transmit.EnableWindow(false);
	m_CB_Close.EnableWindow(false);

	if (!omCkicName.IsEmpty())
	{
		omFldData.ReadCuteRecord(omCkicName, ResCKI, 1, false);

		if (prmFldData->Dseq == 1)
		{
			// counter open! only read open ccas
			omCcaData.ReadUrno(prmFldData->Rurn, omCkicName, false);
		}
		else
		{
			// counter closed! read next scheduled ccas
			if (rmSelFlight.Urno == 0)
				omCcaData.ReadCkic(omCkicName, false);
			else
				omCcaData.ReadCkic(omCkicName, rmSelFlight.Urno, false);
		}
	}
	else if (!omGateName.IsEmpty())
	{
		omFldData.ReadCuteRecord(omGateName, ResGAT, 1, false);
		if (prmFldData->Dseq == 1)
		{
			// gate open! only read open gate
			omAftData.ReadUrno(prmFldData->Aurn, false);
		}
		else
		{
			// gate closed! read next scheduled
			if (rmSelFlight.Urno == 0)
			{
				omAftData.ReadGateRecs(omGateName, false);		
			}
			else
			{
				omAftData.ReadUrno(rmSelFlight.Urno, false);
			}
		}
	}

	RefreshData();

	CCuteIFApp::TriggerWaitCursor(false);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}



void CCuteIFDlg::RefreshData()
{
	CCuteIFApp::TriggerWaitCursor(true);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	ResetDlg();

	if (!omCkicName.IsEmpty())
	{
		if (prmFldData->Dseq == 1)
		{
			bmClosed = false;
			KillTimer(0);

			GetOpenCcaRecs(&omCcaList);
		}
		else
		{
			bmClosed = true;
			SetTimer(0, (UINT) RELOAD_SEC * 1000, NULL); // update every minute

			if (rmSelFlight.Urno == 0)
			{
				omCcaData.GetMostRecentScheduled(&omCcaList);
			}
			else
			{
				omCcaList.RemoveAll();
				CCADATA *prlCca = omCcaData.GetRecByFlnuCkic(rmSelFlight.Urno, omCkicName);
				if (prlCca != NULL)
				{
					omCcaList.Add(prlCca);
				}
			}

		}

	}
	else if (!omGateName.IsEmpty())
	{
		if (prmFldData->Dseq == 1)
		{
			bmClosed = false;
			KillTimer(0);

			GetOpenGateRecs(&omGateList);
		}
		else
		{
			bmClosed = true;
			SetTimer(0, (UINT) RELOAD_SEC * 1000, NULL); // update every minute

			if (rmSelFlight.Urno == 0)
			{
				omAftData.GetMostRecentScheduled(&omGateList);
			}
			else
			{
				omGateList.RemoveAll();
				omGateList.Add(&rmSelFlight);
			}
		}
	}

	DisplayData();

	CCuteIFApp::TriggerWaitCursor(false);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}



bool CCuteIFDlg::GetOpenCcaRecs(CPtrArray *popCcas)
{
	if (popCcas == NULL) return false;
	popCcas->RemoveAll();
	
	CCADATA *prlCca = omCcaData.GetRecByUrno(prmFldData->Rurn);
	if (prlCca)
		popCcas->Add(prlCca);
	
	return true;
}


bool CCuteIFDlg::GetOpenGateRecs(CPtrArray *popAfts)
{
	if (popAfts == NULL) return false;
	popAfts->RemoveAll();
	
	AFTDATA *prlAft = omAftData.GetRecByUrno(prmFldData->Aurn);
	if (prlAft)
		popAfts->Add(prlAft);
	
	return true;
}




void CCuteIFDlg::DisplayData()
{
	DisplayRecordData();
}



void CCuteIFDlg::DisplayRecordData()
{

	if (!omCkicName.IsEmpty())
	{
		m_CB_Transmit.EnableWindow(true);

		////////// Display FIDS Data
		if (!bmClosed && prmFldData->Urno > 0)
		{
			// set logoname
			CString olLogoName;
			GetDBLogo(prmFldData->Urno, olLogoName);
			SetLogo(olLogoName);

			// set freetexts
			CString olFreetext;
			omFxtData.DBGetFreetext(prmFldData->Urno, 1, olFreetext);
			m_CE_Free1.SetWindowText(olFreetext);
			omFxtData.DBGetFreetext(prmFldData->Urno, 2, olFreetext);
			m_CE_Free2.SetWindowText(olFreetext);

			// set fids remark code			
			m_CCB_FidsRemarks.SetWindowText(ogBCD.GetField("FID", "CODE", prmFldData->Crec, "BEME"));
		}
		else
		{
			// initialize display data with the data of the first flight
			if (omCcaList.GetSize() > 0)
			{
				CCADATA *prlCca = (CCADATA *) omCcaList[0];
				// set remark
				m_CCB_FidsRemarks.SetWindowText(ogBCD.GetField("FID", "CODE", prlCca->Disp, "BEME"));			
				
				if (bmClosed)
				{
					// get airline 3-Letter-Code
					CString olAlc3;
					CString olAlc;
					if (strcmp(prlCca->Ctyp, "C") == 0)
					{
						CString olFlnu;
						olFlnu.Format("%ld", prlCca->Flnu);
						ogBCD.GetField("ALT", "URNO", olFlnu, "ALC3", olAlc3);
						ogBCD.GetField("ALT", "URNO", olFlnu, "ALC2", olAlc);
					}
					else
					{
						olAlc = CString(prlCca->Flno).Left(3);
						olAlc.TrimRight();

						if (!ogBCD.GetField("ALT", "ALC3", olAlc, "ALC3", olAlc3))
							ogBCD.GetField("ALT", "ALC2", olAlc, "ALC3", olAlc3);
					}
					// set logo
					CString olLogoName;
					olLogoName.Format("%s_L", olAlc3);
					if (!SetLogo(olLogoName))
					{
						olLogoName.Format("%s_L", olAlc);
						SetLogo(olLogoName);
					}
				}
			}
		}

		/////// Update flight list
		SetFlightListCca(omCcaList);

		/////// Activate/Deactivate FIDS Data Dialog Items
		ActivateFIDSData();
		if (bmClosed)
		{
			// Checkin not open!!
			// set caption
			CString olCaption;
			olCaption.Format(GetString(IDS_STRING103), omCkicName);
			m_CS_State.SetWindowText(olCaption);

			omFlightTable.SetShowSelection(true);
			omFlightTable.SetSelectMode(LBS_MULTIPLESEL);

			// disable close button
			m_CB_Close.EnableWindow(false);

			if (omCcaList.GetSize()  < 1)
			{
				DeactivateFIDSData();
			}

			// hide sign
			m_Text_Sign.ShowWindow(SW_HIDE);
		}
		else
		{
			// checkin open!
			// set caption
			CString olCaption;
			olCaption.Format(GetString(IDS_STRING104), omCkicName);
			m_CS_State.SetWindowText(olCaption);

			omFlightTable.SetShowSelection(false);
			omFlightTable.SetSelectMode(LBS_NOSEL);

			// show sign only if the another than the selected flight is open!
			if (rmSelFlight.Urno > 0 && ((CCADATA *)omCcaList[0])->Flnu != rmSelFlight.Urno)
				m_Text_Sign.ShowWindow(SW_SHOW);
			else
				m_Text_Sign.ShowWindow(SW_HIDE);				

			// enable close button
			m_CB_Close.EnableWindow(true);
		}
	}


	if (!omGateName.IsEmpty())
	{
		m_CB_Transmit.EnableWindow(true);
		////////// Display FIDS Data
		if (!bmClosed && prmFldData->Urno > 0)
		{
			// set logoname
			CString olLogoName;
			GetDBLogo(prmFldData->Urno, olLogoName);
			SetLogo(olLogoName);

			// set freetexts
			CString olFreetext;
			omFxtData.DBGetFreetext(prmFldData->Urno, 1, olFreetext);
			m_CE_Free1.SetWindowText(olFreetext);
			omFxtData.DBGetFreetext(prmFldData->Urno, 2, olFreetext);
			m_CE_Free2.SetWindowText(olFreetext);

			// set fids remark code			
			m_CCB_FidsRemarks.SetWindowText(ogBCD.GetField("FID", "CODE", prmFldData->Crec, "BEME"));
		}
		else
		{
			// initialize display data with the data of the first flight
			if (omGateList.GetSize() > 0)
			{
				AFTDATA *prlAft = (AFTDATA *) omGateList[0];
				// set remark
				m_CCB_FidsRemarks.SetWindowText(ogBCD.GetField("FID", "CODE", prlAft->Remp, "BEME"));
			
				if (bmClosed)
				{
					// set logo
					CString olLogoName;
					olLogoName.Format("%s_L", prlAft->Alc3);
					if (!SetLogo(olLogoName))
					{
						olLogoName.Format("%s_L", prlAft->Alc2);
						SetLogo(olLogoName);
					}
				}
			}
		}

		//////// Update flight list
		SetFlightListGate(omGateList);

		/////// Activate/Deactivate FIDS Data Dialog Items
		ActivateFIDSData();
		if (bmClosed)
		{
			// gate not open!!
			// set caption
			CString olCaption;
			olCaption.Format(GetString(IDS_STRING105), omGateName);
			m_CS_State.SetWindowText(olCaption);

			omFlightTable.SetShowSelection(true);
			omFlightTable.SetSelectMode(LBS_MULTIPLESEL);

			// disable close button
			m_CB_Close.EnableWindow(false);

			if (omGateList.GetSize()  < 1)
			{
				DeactivateFIDSData();
			}

			// hide sign
			m_Text_Sign.ShowWindow(SW_HIDE);
		}
		else
		{
			// checkin open!
			// set caption
			CString olCaption;
			olCaption.Format(GetString(IDS_STRING106), omGateName);
			m_CS_State.SetWindowText(olCaption);

			omFlightTable.SetShowSelection(false);
			omFlightTable.SetSelectMode(LBS_NOSEL);

			// show sign only if the another than the selected flight is open!
			if (omGateList.GetSize() > 0)
			{
				if (rmSelFlight.Urno > 0 && ((AFTDATA *)omGateList[0])->Urno != rmSelFlight.Urno)
				{
					m_Text_Sign.ShowWindow(SW_SHOW);
				}
				else
				{
					m_Text_Sign.ShowWindow(SW_HIDE);				
				}
			}
			else
			{
				m_Text_Sign.ShowWindow(SW_HIDE);				
			}

			// enable close button
			m_CB_Close.EnableWindow(true);
		}


	}

}


void CCuteIFDlg::ResetDlg()
{
	SetLogo("<NONE>");
	m_CE_Free1.SetWindowText("");	
	m_CE_Free2.SetWindowText("");
	DrawFlightTHeader();
	omCcaList.RemoveAll();
	omGateList.RemoveAll();
}


void CCuteIFDlg::ActivateFIDSData()
{
	m_CE_Free1.EnableWindow(true);
	m_CE_Free2.EnableWindow(true);
	m_CCB_FidsRemarks.EnableWindow(true);
	m_CCB_LogoSelect.EnableWindow(true);
}



void CCuteIFDlg::DeactivateFIDSData()
{
	m_CE_Free1.EnableWindow(false);
	m_CE_Free2.EnableWindow(false);
	m_CCB_FidsRemarks.EnableWindow(false);
	m_CCB_LogoSelect.EnableWindow(false);
}



void CCuteIFDlg::SetFlightListCca(const CPtrArray &ropCcaList)
{

	DrawFlightTHeader();
	CCADATA *prlCca;
	CCSPtrArray<TABLE_COLUMN> olColList;

	CString olJoint;
	CString olFlno;
	CString olAlc3;

	for (int i = 0; i < ropCcaList.GetSize(); i++)
	{
		olJoint.Empty();
		olFlno.Empty();
		olAlc3.Empty();

		prlCca = (CCADATA *) (ropCcaList[i]);
		if (strcmp(prlCca->Ctyp, "C") == 0)
		{
			// common checkin
			olFlno = "<COMMON>";
			CString olFlnu;
			olFlnu.Format("%ld", prlCca->Flnu);
			ogBCD.GetField("ALT", "URNO", olFlnu, "ALC3", olAlc3);
		}
		else
		{
			olFlno = prlCca->Flno;
		}

	
		MakeFlightTColList(olJoint, olFlno, olAlc3, olColList);
		omFlightTable.AddTextLine(olColList, NULL);
		olColList.DeleteAll();
	}


    omFlightTable.DisplayTable();
}



void CCuteIFDlg::SetFlightListGate(const CPtrArray &ropAftList)
{

	DrawFlightTHeader();
	AFTDATA *prlAft;
	CCSPtrArray<TABLE_COLUMN> olColList;

	CString olJoint;
	CString olFlno;
	CString olAlc3;

	for (int i = 0; i < ropAftList.GetSize(); i++)
	{
		olJoint.Empty();
		olFlno.Empty();
		olAlc3.Empty();

		prlAft = (AFTDATA *) (ropAftList[i]);
		olFlno = prlAft->Flno;

	
		MakeFlightTColList(olJoint, olFlno, olAlc3, olColList);
		omFlightTable.AddTextLine(olColList, (void *)prlAft);
		olColList.DeleteAll();
	}


    omFlightTable.DisplayTable();

}



void CCuteIFDlg::MakeFlightTColList(const CString &ropJoint, const CString &ropFlno, const CString &ropAlc3, CCSPtrArray<TABLE_COLUMN> &olColList)
{
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_10;


	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = ropJoint;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

 	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = ropFlno;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = ropAlc3;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

}



bool CCuteIFDlg::SetLogo(const CString &ropLogoSName)
{

	// set combo box
	CString olLogoFull;
	if (ogBCD.GetField("FLG", "LGSN", ropLogoSName, "LGFN", olLogoFull))
	{
		m_CCB_LogoSelect.SetWindowText(olLogoFull);
	}


	// set logo bitmap


	if (!CCuteIFApp::bgLogosAvail) return false;

	//char rlLogoBytes[MAX_LOGO_SIZE_X*MAX_LOGO_SIZE_Y*2];
	//int ilBytesRead = 0;

	//BITMAP olBITMAP;
	//omLogo.GetBitmap(&olBITMAP);

	bool blOK = false;

	if (!(ropLogoSName == "<NONE>" || ropLogoSName.IsEmpty()))
	{
		if (bgLogoSourceHttp)
		{
			// get bitmap data over internet
			CHttpFile *polHttpFile;
			polHttpFile = pomHttpServer->OpenRequest(CHttpConnection::HTTP_VERB_GET,
					ogHttpLogoPath+ropLogoSName+".bmp", "image/*", 1, NULL, NULL, INTERNET_FLAG_EXISTING_CONNECT | INTERNET_FLAG_NO_AUTO_REDIRECT);
				
			try
			{
				if (polHttpFile->SendRequest())
				{
					DWORD wlRet;
					polHttpFile->QueryInfoStatusCode(wlRet);
					if (wlRet <= 299 && wlRet >= 200)
					{
						if (ogBCD.GetField("FLG", "LGSN", ropLogoSName, "LGFN", olLogoFull))
						{
							SetLogoData(polHttpFile);
							blOK = true;
						}
					}
					polHttpFile->Close();
				}
				else
				{
					//CCuteIFApp::DisableLogos();
					//TRACE("CCuteIFDlg::SetLogo: Error in 'CHttpFile::SendRequest'!\n");
				}
			}
			catch (CInternetException olExc)
			{
				CCuteIFApp::DisableLogos();
			}
			delete polHttpFile;
		}
		else
		{
			// get bitmap data from file
			CString olFilePath = ogFileLogoPath+ropLogoSName+".bmp";
			try
			{
				CFile olLogoFile(olFilePath, CFile::modeRead);
				if (ogBCD.GetField("FLG", "LGSN", ropLogoSName, "LGFN", olLogoFull))
				{
					SetLogoData(&olLogoFile);
					blOK = true;
				}
			}
			catch(CFileException olExc)
			{
				blOK = false;
			}
		}
	}

	if (!blOK)
	{
		//omLogo.Detach();
		LPVOID lpvResource = (LPVOID) ::LoadResource(NULL,
			::FindResource(NULL, MAKEINTRESOURCE(IDB_NOLOGO),
			RT_BITMAP));
		omLogo.AttachMemory(lpvResource); // no need for
		//omLogo.LoadBitmap(IDB_NOLOGO);
		CString olLogoFull;
		if (!ogBCD.GetField("FLG", "LGSN", ropLogoSName, "LGFN", olLogoFull))
			olLogoFull = "<NONE>";
		m_CCB_LogoSelect.SetWindowText(olLogoFull);
	}

	InvalidateRect(omLogoRect);

	return blOK;

	//SetLogoData(rlLogoBytes, ilBytesRead);

	//int ilSet = omLogo.SetBitmapBits(ilBytesRead, rlLogoBytes);

	//m_CBB_Logo.LoadBitmaps(IDB_LOGO);
	//m_CBB_Logo.Invalidate();
	
}

struct BITMAPINFO256 : public BITMAPINFO
{
	RGBQUAD bmiOtherColors[255];
};



bool CCuteIFDlg::SetLogoData(CFile *popFileData) //char *prpLogoData, long lpDataSize)
{
	if (popFileData == NULL)
		return false;

	/*
	// Try to read the bitmapfileheader.
	BITMAPFILEHEADER bmfh;
	if (popFileData->Read((char *) &bmfh, sizeof(bmfh)) < 1)
		return false;

	// Try to read the bitmapinfoheader.
	BITMAPINFO256 rlBmInfo;
	if (popFileData->Read((char *) &rlBmInfo.bmiHeader, sizeof(rlBmInfo.bmiHeader)) < 1)
		return false;

	// Now we know the image size.
	CSize olBmSize(rlBmInfo.bmiHeader.biWidth, rlBmInfo.bmiHeader.biHeight);
	

	// Read the color table
	if (popFileData->Read((char *) &rlBmInfo.bmiColors, 
		sizeof(RGBQUAD) * (1 << rlBmInfo.bmiHeader.biBitCount)) < 1)
		return false;

	// Read some pels
	long llDataBytesNum = (rlBmInfo.bmiHeader.biBitCount * olBmSize.cx * olBmSize.cy) / 8;
	BYTE *prlBmData = new BYTE[llDataBytesNum];

	if (popFileData->Read(prlBmData, llDataBytesNum) < 1)
	{
		delete[] prlBmData;
		return false;
	}

	
	CPaintDC olPaintDc(this); // device context for painting
	CDC dc;
	dc.CreateCompatibleDC(&olPaintDc);

	omLogo.Detach();
	//omLogo.CreateCompatibleBitmap(&dc, olBmSize.cx, olBmSize.cy);
	omLogo.CreateBitmap(olBmSize.cx, olBmSize.cy, 1, 16, NULL);
	SetDIBits(dc.m_hDC, (HBITMAP)omLogo.m_hObject, 0, olBmSize.cy,
				 prlBmData, &rlBmInfo, DIB_RGB_COLORS);
	*/

	omLogo.Read(popFileData);

	CClientDC dc(this);
	omLogo.SetSystemPalette(&dc);


	/*
	dc.StretchBlt(0, 0, 300, 84, &dc, 0, 0, olBmSize.cx, olBmSize.cy, SRCCOPY);


	CDC dc2;
	dc2.CreateCompatibleDC(&dc);
	StretchBlt(dc.m_hDC, 0, 0, 300, 84, dc.m_hDC, 0, 0, olBmSize.cx, olBmSize.cy, SRCCOPY); 
	*/

	//delete[] prlBmData;


	return true;
}




void CCuteIFDlg::OnTimer(UINT nIDEvent)
{
	if (nIDEvent == 0)
	{
		LoadData(rmSelFlight.Urno);
	}
	else if (nIDEvent == 1)
	{
		ProcessExtMesBuffer();
	}
}


void CCuteIFDlg::OnOK()
{
	OnCancel();
	//Exit();
}


void CCuteIFDlg::OnClose()
{
	OnCancel();
	//Exit();
}


void CCuteIFDlg::OnCancel()
{
	ShowWindow(SW_HIDE);
}


void CCuteIFDlg::Exit()
{
	ogDdx.UnRegister(this,NOTUSED);
	ogCommHandler.CleanUpCom();	

	CDialog::OnCancel();
}


bool CCuteIFDlg::DBUpdateLogo(long lpFldu, const CString &ropName)
{
	// get urno of flg-record of the given logo
	CString olNewFlgu;
	if (!ogBCD.GetField("FLG", "LGFN", ropName, "URNO", olNewFlgu))
		return false;
	
	CString olFldu;
	olFldu.Format("%ld", lpFldu);
	
	if (prmFlzRecord != NULL && (*prmFlzRecord)[ogBCD.GetFieldIndex("FLZ", "FLDU")] == olFldu)
	{
		// flz-record exists!! Update it!
		ogBCD.SetField("FLZ", "URNO", (*prmFlzRecord)[ogBCD.GetFieldIndex("FLZ", "URNO")], "FLGU", olNewFlgu, false);

		ogBCD.Save("FLZ");
		return true;
	}
	else
	{
		CString olNextUrno;
		olNextUrno.Format("%ld", ogBasicData.GetNextUrno());
		// create new flz record
		RecordSet olNewFlz(ogBCD.GetFieldCount("FLZ"));
		olNewFlz[ogBCD.GetFieldIndex("FLZ", "URNO")] = olNextUrno;
		olNewFlz[ogBCD.GetFieldIndex("FLZ", "FLDU")] = olFldu;
		olNewFlz[ogBCD.GetFieldIndex("FLZ", "FLGU")] = olNewFlgu;
		olNewFlz[ogBCD.GetFieldIndex("FLZ", "SORT")] = "1";

		return ogBCD.InsertRecord("FLZ", olNewFlz, true);
	}
}



bool CCuteIFDlg::GetDBLogo(long lpFldu, CString &ropName)
{
	ropName.Empty();

	CString olFldu;
	olFldu.Format("%ld", lpFldu);

	CString olSel;
	olSel.Format("WHERE FLDU = %ld", lpFldu);
	ogBCD.Read("FLZ", olSel);
	if (!prmFlzRecord)
		prmFlzRecord = new RecordSet;
	if (!ogBCD.GetRecord("FLZ", "FLDU", olFldu, *prmFlzRecord))
		return false;


	return ogBCD.GetField("FLG", "URNO", (*prmFlzRecord)[ogBCD.GetFieldIndex("FLZ", "FLGU")], "LGSN", ropName); 
}


bool CCuteIFDlg::IsCkinOpen(long lpAftUrno, long lpCcaUrno) const
{
	if (bmClosed)
		return false;

	// scan all open ckin
	CCADATA *prlCca;
	for (int i = 0; i < omCcaList.GetSize(); i++)
	{
		prlCca = (CCADATA *) omCcaList[i];
		if (prlCca->Ctyp[0] == 'C')
		{
			if (prlCca->Urno == lpCcaUrno)
				return true;
		}
		else
		{
			if (prlCca->Flnu == lpAftUrno && prlCca->Urno == lpCcaUrno)
				return true;
		}
	}
	return false;
}


bool CCuteIFDlg::IsGateOpen(long lpAftUrno) const
{
	if (bmClosed)
		return false;

	// scan all open gates
	AFTDATA *prlAft;
	for (int i = 0; i < omGateList.GetSize(); i++)
	{
		prlAft = (AFTDATA *) omGateList[i];
		if (prlAft->Urno == lpAftUrno)
			return true;
	}
	return false;
}



bool CCuteIFDlg::ExtractText(const CString &ropText, const CString &ropKey, CString &ropValue) const
{
	int ilKPos = ropText.Find(ropKey);
	if (ilKPos == -1)
		return false;

	int ilVPos = ilKPos + ropKey.GetLength(); // beginning of the value
	int ilSpacePos = ropText.Find(' ', ilVPos);
	if (ilSpacePos == -1)
	{
		// Value is at the end of the text
		ropValue = ropText.Mid(ilVPos);
		return true;
	}
	else
	{
		// Value ends at the found space char
		ropValue = ropText.Mid(ilVPos, ilSpacePos - ilVPos);
		return true;
	}


}




bool CCuteIFDlg::ExtractUrno(const CString &ropText, const CString &ropKey, long &rlpUrno) const
{

	CString olUrno;
	if (!ExtractText(ropText, ropKey, olUrno))
		return false;

	rlpUrno = atol(olUrno);
	return true;
	
}




void  CCuteIFDlg::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	if (!bmBCEnabled)
		return;


	CString olCurrFldu;
	olCurrFldu.Format("%ld", prmFldData->Urno);

	if (ipDDXType == FLZ_CHANGE)
	{
		//TRACE("CCuteIFDlg::ProcessBc: FLZ_CHANGE\n");

		RecordSet *prlBcRecord = (RecordSet *) vpDataPointer;
		if ((*prlBcRecord)[ogBCD.GetFieldIndex("FLZ", "FLDU")] != olCurrFldu)
			return;
		delete prmFlzRecord;
		prmFlzRecord = NULL;
	}
	if (ipDDXType == FXT_CHANGE)
	{
		//TRACE("CCuteIFDlg::ProcessBc: FXT_CHANGE\n");

		RecordSet *prlBcRecord = (RecordSet *) vpDataPointer;
		if ((*prlBcRecord)[ogBCD.GetFieldIndex("FXT", "FLDU")] != olCurrFldu)
			return;
	}
	//struct BcStruct *prlBcData = (struct BcStruct *) vpDataPointer;
	RefreshData();
}




bool CCuteIFDlg::InitCOMInterface()
{

	HRESULT olRes = CoInitialize(NULL);
	if (olRes == E_INVALIDARG)
		int asdf = 0;
	if (olRes == E_OUTOFMEMORY)
		int asdf = 0;
	if (olRes == E_UNEXPECTED)
		int asdf = 0;
	if (olRes == S_FALSE)
		int asdf = 0;
	if (olRes == RPC_E_CHANGED_MODE)
		int asdf = 0;

 	if(olRes != S_OK && olRes != S_FALSE)
	{
		//AfxMessageBox(_T("CoInitializeEx failed!"));
		return false;
	}
	

	try
	{
		pConnect = IUFISAmPtr(CLSID_UFISAm);
	}
	catch(_com_error error)
	{
		//AfxMessageBox(error.ErrorMessage());
		return false;
	}


	try
	{
		BOOL Res = m_UFISAmSink.Advise(pConnect,IID_IUFISAmEventSink);
		ASSERT(Res == TRUE);
	}
	catch(_com_error error)
	{
		//AfxMessageBox(error.ErrorMessage());
		return false;
	}
	/*
	 * Assign AppTag immediately after advising the sink!
	 */
	pConnect->AssignAppTag(static_cast<long>(FipsCUTE));

	bgComOk = true;
	return true;
}



void CCuteIFDlg::ExternCall(LPCSTR popMessage)
{
	TRACE("CCuteIFDlg::ExternCall: %s\n", popMessage);

	// Buffer Message
	CString olMessage(popMessage);
	omExtMesBuffer.Add(olMessage);
	// Set Timer
    SetTimer(1, (UINT) 1 * 1000, NULL); // process new message in one sec
}



void CCuteIFDlg::ProcessExtMesBuffer()
{
	KillTimer(1);
	while (omExtMesBuffer.GetSize() > 0)
	{
		ProcessExternMessage(omExtMesBuffer[0]);
		omExtMesBuffer.RemoveAt(0);
	}
}



void CCuteIFDlg::ProcessExternMessage(const CString &ropMessage)
{
	// There are only 4 defined formats of Message:
	// 1) "SHOW CKIN: %s AFTU: %s"
	// 2) "SHOW GATE: %s AFTU: %s"
	// 3) "SHOW BELT: %s"
	// 4) "OPEN CKIN: %s AFTU: %s CCAU: %s"
	// 5) "CLOSE CKIN: %s AFTU: %s CCAU: %s"
	// 6) "OPEN GATE: %s AFTU: %s"
	// 7) "CLOSE GATE: %s AFTU: %s"
	// 8) "EXIT"

	// EXIT
	if (ropMessage == "EXIT")
	{
		Exit();
		return;
	}
	///// Checkin
	// Show Checkin	
	int ilCNum;
	ilCNum = ropMessage.Find("SHOW CKIN");
	if (ilCNum != -1)
	{
		// extract Checkin-counter name
		CString olCkin;
		ExtractText(ropMessage, "CKIN: ", olCkin);
		// extract Aft-Urno
		long llAftU = 0;
		ExtractUrno(ropMessage, "AFTU: ", llAftU);

		SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
		NewCkic(olCkin, llAftU);
		SetWindowPos(&wndNoTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
		return;
	}
	// Open Checkin	
	ilCNum = ropMessage.Find("OPEN CKIN");
	if (ilCNum != -1)
	{
		// extract Checkin-counter name
		CString olCkin;
		ExtractText(ropMessage, "CKIN: ", olCkin);
		// extract Aft-Urno
		long llAftU;
		ExtractUrno(ropMessage, "AFTU: ", llAftU);
		// extract CCA-Urno
		long llCcaU;
		ExtractUrno(ropMessage, "CCAU: ", llCcaU);

		// Open resource
		NewCkic(olCkin);
		if (bmClosed)
		{
			// open resource (only FLD!, because cca was updated by caller!)
			SaveCuteRecord(llAftU, llCcaU);
		}
			
		return;
	}
	// Close Checkin	
	ilCNum = ropMessage.Find("CLOSE CKIN");
	if (ilCNum != -1)
	{
		// extract Checkin-counter name
		CString olCkin;
		ExtractText(ropMessage, "CKIN: ", olCkin);
		// extract Aft-Urno
		long llAftU;
		ExtractUrno(ropMessage, "AFTU: ", llAftU);
		// extract CCA-Urno
		long llCcaU;
		ExtractUrno(ropMessage, "CCAU: ", llCcaU);

		// Close resource
		NewCkic(olCkin);
		if (IsCkinOpen(llAftU, llCcaU))
		{
			// close resource (only FLD!, because cca was updated by caller!)
			prmFldData->Dseq = -1;
			omFldData.SaveCuteRecord(&omAddWhereIfClosing);
		}
		return;
	}
	//// Gate
	// Show Gate	
	ilCNum = ropMessage.Find("SHOW GATE");
	if (ilCNum != -1)
	{
		// extract gate name
		CString olGate;
		ExtractText(ropMessage, "GATE: ", olGate);
		// extract Aft-Urno
		long llAftU;
		ExtractUrno(ropMessage, "AFTU: ", llAftU);

		SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
		NewGate(olGate, llAftU);
		SetWindowPos(&wndNoTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);

		return;
	}
	// Open Gate	
	ilCNum = ropMessage.Find("OPEN GATE");
	if (ilCNum != -1)
	{
		// extract gate name
		CString olGate;
		ExtractText(ropMessage, "GATE: ", olGate);
		// extract Aft-Urno
		long llAftU;
		ExtractUrno(ropMessage, "AFTU: ", llAftU);

		NewGate(olGate);
		if (bmClosed)
		{
			// open resource (only FLD!, because cca was updated by caller!)
			SaveCuteRecord(llAftU, 0);
		}
			
		return;
	}
	// Close Gate	
	ilCNum = ropMessage.Find("CLOSE GATE");
	if (ilCNum != -1)
	{
		// extract gate name
		CString olGate;
		ExtractText(ropMessage, "GATE: ", olGate);
		// extract Aft-Urno
		long llAftU;
		ExtractUrno(ropMessage, "AFTU: ", llAftU);

		NewGate(olGate);
		if (IsGateOpen(llAftU))
		{
			// close resource (only FLD!, because cca was updated by caller!)
			prmFldData->Dseq = -1;
			omFldData.SaveCuteRecord(&omAddWhereIfClosing);
		}
		return;
	}

	
	//// Belt
	// Show Belt Free Texts	
	ilCNum = ropMessage.Find("SHOW BELT");
	if (ilCNum != -1)
	{
		if (pomBltFreeTextDlg != NULL)
		{
			delete pomBltFreeTextDlg;
		}
		pomBltFreeTextDlg = new BltFreeTextDlg(ropMessage.Right(ropMessage.GetLength() - 11));

		return;
	}


}






