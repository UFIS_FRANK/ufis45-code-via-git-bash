// CCSGlobl.h: interface for the CCSGlobl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
#define AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_

#ifndef __CCSGLOBL_H__
#define __CCSGLOBL_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include "CCSDefines.h"
#include "CCSTime.h"
#include "CCSPtrArray.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <afxinet.h>


/////////////////////////////////////////////////////////////////////////////
// application globals 
extern char pcgHome[4];
extern char pcgHome4[5];
extern char pcgTableExt[10];
extern char pcgVersion[12];
extern char pcgInternalBuild[12];



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section


class CCSBcHandle;
class CCSCedaCom;
class CCSDdx;
class CCSLog;
class CedaBasicData;
class CCSBasic;
class CBasicData;

extern CCSBasic ogCCSBasic;
extern CCSBcHandle ogBcHandle;
extern CCSCedaCom ogCommHandler;  // The one and only CedaCom object
extern CCSDdx ogDdx;
extern CCSLog ogLog;
extern CedaBasicData ogBCD;
extern CBasicData ogBasicData;


extern CTimeSpan ogLocalDiff;
extern CTime ogUtcStartTime;


extern char pcgUser[33];
extern char pcgPasswd[33];
extern CTimeSpan ogUtcDiff;

extern bool bgLogoSourceHttp;
extern CString ogHttpServer;
extern CString ogHttpLogoPath;
extern CString ogFileLogoPath;
extern INTERNET_PORT igHttpPort;

extern bool bgDebug;
extern ofstream of_debug;

#include "UFISAmSink.h"
extern IUFISAmPtr pConnect;  // for com-interface
extern bool bgComOk;

enum enumColorIndexes
{
	IDX_GRAY=2,IDX_GREEN,IDX_RED,IDX_BLUE,IDX_SILVER,IDX_MAROON,
	IDX_OLIVE,IDX_NAVY,IDX_PURPLE,IDX_TEAL,IDX_LIME,
	IDX_YELLOW,IDX_FUCHSIA,IDX_AQUA, IDX_WHITE,IDX_BLACK,IDX_ORANGE
};



#define MAXCOLORS 64
#define FIRSTCONFLICTCOLOR 31

// Symbolic colors (helper constants for CGateDiagramDialog -- testing purpose only)
#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
#define SILVER  RGB(192, 192, 192)          // light gray
#define RED     RGB(255,   0,   0)
#define ORANGE  RGB(255, 132,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255,   0)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(  0, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)
#define LGREEN  RGB(128, 255, 128)


extern COLORREF ogColors[];
extern CBrush *ogBrushs[];
extern CString ogAppName;
extern CString ogCustomer;

extern COLORREF lgBkColor;
extern COLORREF lgTextColor;
extern COLORREF lgHilightColor;

extern BOOL bgIsInitialized;


// CFont : translate from logical point to physical pixel
#define PT_TO_PIXELS(dc, pt)    (- MulDiv( pt, (dc).GetDeviceCaps( LOGPIXELSY ), 72 ))


/////////////////////////////////////////////////////////////////////////////
// IDs


#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_INPLACEEDIT		0x400c



/////////////////////////////////////////////////////////////////////////////
// Messages





/////////////////////////////////////////////////////////////////////////////
// Font variable

extern CFont ogSmallFonts_Regular_6;
extern CFont ogSmallFonts_Regular_7;
extern CFont ogMSSansSerif_Regular_8;
extern CFont ogMSSansSerif_Bold_8;
extern CFont ogMSSansSerif_Bold_7;
extern CFont ogCourier_Bold_10;
extern CFont ogCourier_Bold_8;
extern CFont ogCourier_Regular_10;
extern CFont ogCourier_Regular_8;
extern CFont ogCourier_Regular_9;
extern CFont ogMS_Sans_Serif_8;

extern CFont ogTimesNewRoman_9;
extern CFont ogTimesNewRoman_12;
extern CFont ogTimesNewRoman_16;
extern CFont ogTimesNewRoman_30;

extern CFont ogSetupFont;


extern CFont ogScalingFonts[30];
extern int igFontIndex1;
extern int igFontIndex2;
extern int igDaysToRead;


void InitFont();
void DeleteBrushes();
void CreateBrushes();


enum{MS_SANS6, MS_SANS8, MS_SANS12, MS_SANS16, 
     MS_SANS6BOLD, MS_SANS8BOLD, MS_SANS12BOLD, MS_SANS16BOLD};

struct FONT_INDEXES
{
	int VerticalScale;
	int Chart;
	FONT_INDEXES(void)
	{VerticalScale=0;Chart=0;}
};


/////////////////////////////////////////////////////////////////////////////
// Color and brush variables

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];

/////////////////////////////////////////////////////////////////////////////



struct TIMEFRAMEDATA
{
	CTime StartTime;
	CTime EndTime;
	TIMEFRAMEDATA(void)
	{StartTime=TIMENULL;EndTime=TIMENULL;}
};



class CInitialLoadDlg;
extern CInitialLoadDlg *pogInitialLoad;

/////////////////////////////////////////////////////////////////////////////
// IDs

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "tscall.cpp" by Pichate May 08,96 18:15
#define IDC_TIMESCALE       0x4001

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "vscale.cpp" by Pichate May 08,96 18:15
#define IDC_VERTICALSCALE   0x4002

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "gbar.cpp" by Pichate May 08,96 18:15
#define IDC_GANTTBAR        0x4003

//#define IDD_DIAGRAM         0x4004
#define IDD_CHART           0x4004
#define IDC_CHARTBUTTON     0x4005
#define IDC_CHARTBUTTON2    0x4006
#define IDD_GANTT           0x4007

#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_ARRIVAL         0x400a
#define IDC_DEPARTURE       0x400b
#define IDC_INPLACEEDIT		0x400c
#define IDC_CHARTBUTTON3    0x400d
#define IDC_CHARTBUTTON4    0x400e

#define IDD_GANTTCHART      0x4101


#define IDM_ROTATION		11L
#define IDM_SEASON			14L
#define IDM_IMPORT			17L
#define IDM_COMMONCCA		19L
#define IDM_DIACCA			21L
#define IDM_SEASON_COL		23L


/////////////////////////////////////////////////////////////////////////////
// Messages

// Message sent from PrePlanTable to the parent window when closed
// Id 24-Sep-96
// Fix some bug here, since CCI diagram and Gate diagram shares the same message number.
// This surely will crash the machine or destroy our memory blocks if the user terminate
// the diagram with Alt-F4.
#define WM_STAFFTABLE_EXIT          (WM_APP + 20)	/* tables */
#define WM_GEQTABLE_EXIT            (WM_APP + 21)	/* tables */
#define WM_INFTABLE_EXIT            (WM_APP + 22)	/* tables */
#define WM_HWDIA_EXIT				(WM_APP + 23)	/* tables */
#define WM_FLIGHTDIA_EXIT           (WM_APP + 24)	/* tables */
#define WM_STAFFDIA_EXIT            (WM_APP + 25)	/* tables */
#define WM_CONFTABLE_EXIT           (WM_APP + 26)	/* tables */
#define WM_FLIGHTTABLE_EXIT         (WM_APP + 27)	/* tables */
#define WM_MAGNETICTABLE_EXIT       (WM_APP + 28)	/* tables */
#define WM_ATTENTIONTABLE_EXIT      (WM_APP + 29)	/* tables */
#define WM_CONFLICTTABLE_EXIT       (WM_APP + 30)	/* tables */
#define WM_LOCKWINDOWUPDATE         (WM_APP + 31)	/* tables */
#define WM_ZOLLTABLE_EXIT			(WM_APP + 32)	/* tables */

#define WM_PLACEROUND				(WM_APP + 33)	/* tables */
#define WM_TOUCHANDGO				(WM_APP + 34)	/* tables */
#define WM_MAXIMIZE_CHILD			(WM_APP + 35)	
#define WM_RETURNFLIGHT				(WM_APP + 36)	





enum enumDDXTypes
{

	APP_EXIT,
	DATA_RELOAD,
	TABLE_FONT_CHANGED,
    UNDO_CHANGE, APP_LOCKED, APP_UNLOCK,
    BC_CFG_INSERT, BC_CFG_CHANGE, CFG_CHANGE, CFG_INSERT,CFG_DELETE,
	KONF_CHANGE,KONF_DELETE,KONF_INSERT,

    // from here, there are defines for your project
	BC_FLD_CHANGE, BC_FLD_DELETE, BC_FLD_NEW, FLDCUTE_CHANGE,
	BC_CCA_CHANGE, BC_CCA_DELETE, BC_CCA_NEW,
	CCA_CHANGE, CCA_DELETE, CCA_NEW, CCACUTE_CHANGE,
	BC_FLIGHT_NEW, BC_FLIGHT_CHANGE, BC_FLIGHT_DELETE, FLIGHT_NEW, FLIGHT_DELETE, FLIGHT_CHANGE,
	FLZ_CHANGE,
	FXT_CHANGE
};





CString GetString(UINT ID);
void DeleteBrushes();






extern ofstream of_catch;
extern ofstream of_debug;

extern ofstream *pogBCLog;

#define  CCS_TRY try{
#define  CCS_CATCH_ALL }\
						catch(...)\
						{\
						char pclExcText[512]="";\
						sprintf(pclExcText, "File: %s  ==> Source-Line: %d", __FILE__, __LINE__);\
						::MessageBox(NULL, pclExcText, "Error", MB_YESNO);\
						of_catch << pclExcText << endl;\
						}









/////////////////////////////////////////////////////////////////////////////
// BDPS-SEC 

#define SetWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat=='-') plWnd.ShowWindow(SW_HIDE);

#define SetWndStatPrio_1(clStat1,clStat2, plWnd)\
	if(clStat1=='-'||clStat2=='-') plWnd.ShowWindow(SW_HIDE);\
	else if(clStat1=='0'||clStat2=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat1=='1'||clStat2=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}

#define SetpWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd->ShowWindow(SW_SHOW);plWnd->EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd->ShowWindow(SW_SHOW);plWnd->EnableWindow(FALSE);}\
	else if(clStat=='-') plWnd->ShowWindow(SW_HIDE);


// end globals
/////////////////////////////////////////////////////////////////////////////


#endif //__CCSGLOBL_H__
#endif // !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
