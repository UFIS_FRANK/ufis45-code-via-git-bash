#if !defined(AFX_BLTFREETEXTDLG_H__9F7FA605_F046_11D4_8008_0050DA1CACF1__INCLUDED_)
#define AFX_BLTFREETEXTDLG_H__9F7FA605_F046_11D4_8008_0050DA1CACF1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BltFreeTextDlg.h : header file
//

#include "CedaFldData.h"
#include "FxtData.h"
#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// BltFreeTextDlg dialog

class BltFreeTextDlg : public CDialog
{
// Construction
public:
	BltFreeTextDlg(const CString &ropBltName, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(BltFreeTextDlg)
	enum { IDD = IDD_BLT_FREETEXT };
	CStatic	m_BltName;
	CEdit	m_FreeText2;
	CEdit	m_FreeText1;
	//}}AFX_DATA

	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


// Implementation
protected:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BltFreeTextDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


	// Generated message map functions
	//{{AFX_MSG(BltFreeTextDlg)
	virtual void OnCancel();
	afx_msg void OnSave();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CString omBltName;
	FLDDATA *pomFldRecord;

	CedaFldData omFldData;
	FxtData omFxtData;

	void ReloadData();

	bool SaveData();
	bool SaveFreeText(const CString &ropFreeT1, const CString &ropFreeT2) const;


};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BLTFREETEXTDLG_H__9F7FA605_F046_11D4_8008_0050DA1CACF1__INCLUDED_)
