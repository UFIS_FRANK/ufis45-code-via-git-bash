whenever sqlerror continue
host clear
host echo "----------------------------------------------------------------"
host echo "Building Table SRLTAB."
host echo "----------------------------------------------------------------"
Drop Table SRLTAB;
CREATE TABLE SRLTAB (
       URNO      NUMBER  (10) DEFAULT 0,
       FLNU      NUMBER  (10) DEFAULT 0,
       HOPO      CHAR    (3)  DEFAULT ' ',
       USEC      CHAR    (32) DEFAULT ' ',
       CDAT      CHAR    (14) DEFAULT ' ',
       USEU      CHAR    (32) DEFAULT ' ',
       LSTU      CHAR    (14) DEFAULT ' ',
       PRFL      CHAR    (1)  DEFAULT ' ',
       AREA      CHAR    (1)  DEFAULT ' ',
       READ      CHAR    (1)  DEFAULT ' ',
       ADID      CHAR    (1)  DEFAULT ' ',
       FLNO      CHAR    (9)  DEFAULT ' ',
       STIM      CHAR    (14) DEFAULT ' ',
       ETIM      CHAR    (14) DEFAULT ' ',
       TYPE      CHAR    (2)  DEFAULT ' ',
       TIME      CHAR    (14) DEFAULT ' ',
       TEXT      VARCHAR2    (1024) DEFAULT ' '
)
;
COMMIT;
host echo "Completed."
