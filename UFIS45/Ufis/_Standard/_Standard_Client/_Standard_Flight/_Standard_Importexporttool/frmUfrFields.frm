VERSION 5.00
Begin VB.Form frmUfrFields 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Selected Import Fields"
   ClientHeight    =   3435
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6990
   Icon            =   "frmUfrFields.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3435
   ScaleWidth      =   6990
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame4 
      Caption         =   "Specials"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1785
      Left            =   4380
      TabIndex        =   12
      Top             =   1500
      Width           =   2475
      Begin VB.CheckBox chkDatesOnly 
         Caption         =   "Dates Only"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   180
         TabIndex        =   13
         ToolTipText     =   "ACT3/ACT5"
         Top             =   270
         Width           =   2055
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Common Flight Data"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1785
      Left            =   120
      TabIndex        =   6
      Top             =   1500
      Width           =   4155
      Begin VB.CheckBox chkVia3 
         Caption         =   "List of VIA Stations"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   9
         ToolTipText     =   "VIA3/VIA4/VIAL"
         Top             =   870
         Width           =   2055
      End
      Begin VB.CheckBox chkFtyp 
         Caption         =   "Flight Status (S / O / X / N)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   8
         ToolTipText     =   "FTYP"
         Top             =   300
         Width           =   3375
      End
      Begin VB.CheckBox chkAct3 
         Caption         =   "Aircraft Types"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   7
         ToolTipText     =   "ACT3/ACT5"
         Top             =   570
         Width           =   2055
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Departure Flights"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   3540
      TabIndex        =   3
      Top             =   150
      Width           =   3315
      Begin VB.CheckBox chkDes3 
         Caption         =   "Final Destination Airport"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   11
         ToolTipText     =   "DES3/DES4"
         Top             =   840
         Width           =   2775
      End
      Begin VB.CheckBox DepStod 
         Caption         =   "Scheduled Departure Time"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   5
         ToolTipText     =   "STOD"
         Top             =   300
         Width           =   2655
      End
      Begin VB.CheckBox DepStoa 
         Caption         =   "Arrival at Final Destination"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   4
         ToolTipText     =   "STOA"
         Top             =   570
         Width           =   2865
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Arrival Flights"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   120
      TabIndex        =   0
      Top             =   150
      Width           =   3315
      Begin VB.CheckBox chkOrg3 
         Caption         =   "Origin Airport"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   10
         ToolTipText     =   "ORG3/ORG4"
         Top             =   300
         Width           =   2055
      End
      Begin VB.CheckBox ArrStoa 
         Caption         =   "Scheduled Arrival Time"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   2
         ToolTipText     =   "STOA"
         Top             =   840
         Width           =   2655
      End
      Begin VB.CheckBox ArrStod 
         Caption         =   "Departure from Origin"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   1
         ToolTipText     =   "STOD"
         Top             =   570
         Width           =   2295
      End
   End
End
Attribute VB_Name = "frmUfrFields"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim UfrRebuildMode As Boolean

Private Sub chkDes3_Click()
    If chkDes3.Value = 1 Then
        chkVia3.Value = 1
    Else
        chkVia3.Value = chkOrg3.Value
    End If
End Sub

Private Sub chkOrg3_Click()
    If chkOrg3.Value = 1 Then
        chkVia3.Value = 1
    Else
        chkVia3.Value = chkDes3.Value
    End If
End Sub

Private Sub Form_Load()
    Dim tmpData As String
    tmpData = GetIniEntry(myIniFullName, "MAIN", "", "UFR_REBUILD_MODE", "N")
    If tmpData = "YES" Then
        UfrRebuildMode = True
        InitCheckButtons 0
        NoopFlights.chkWork(22).Enabled = True
    Else
        NoopFlights.chkWork(22).Enabled = False
        UfrRebuildMode = False
        InitCheckButtons 1
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Me.Hide
        Cancel = True
    End If
End Sub

Private Sub InitCheckButtons(SetVal As Integer)
    ArrStod.Value = SetVal
    ArrStoa.Value = SetVal
    chkOrg3.Value = SetVal
    chkVia3.Value = SetVal
    chkDes3.Value = SetVal
    DepStod.Value = SetVal
    DepStoa.Value = SetVal
    chkFtyp.Value = SetVal
    chkAct3.Value = SetVal
End Sub
