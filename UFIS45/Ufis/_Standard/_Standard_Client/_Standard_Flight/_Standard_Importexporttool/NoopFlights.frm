VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form NoopFlights 
   Caption         =   "Flight Schedule Analyzer"
   ClientHeight    =   7815
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   15885
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   11.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "NoopFlights.frx":0000
   KeyPreview      =   -1  'True
   LinkMode        =   1  'Source
   LinkTopic       =   "CompareInfo"
   LockControls    =   -1  'True
   OLEDropMode     =   1  'Manual
   ScaleHeight     =   7815
   ScaleWidth      =   15885
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   9060
      TabIndex        =   176
      Text            =   "Text1"
      Top             =   5220
      Visible         =   0   'False
      Width           =   6045
   End
   Begin TABLib.TAB DataTab 
      Height          =   2745
      Left            =   30
      TabIndex        =   174
      Top             =   4230
      Visible         =   0   'False
      Width           =   1935
      _Version        =   65536
      _ExtentX        =   3413
      _ExtentY        =   4842
      _StockProps     =   64
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Fields"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   22
      Left            =   8580
      Style           =   1  'Graphical
      TabIndex        =   172
      ToolTipText     =   "Activate DoubleClick for Deletes"
      Top             =   30
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   21
      Left            =   7350
      Style           =   1  'Graphical
      TabIndex        =   171
      ToolTipText     =   "Perform Deletions"
      Top             =   30
      Width           =   285
   End
   Begin VB.CheckBox chkWork 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   20
      Left            =   5910
      Style           =   1  'Graphical
      TabIndex        =   170
      ToolTipText     =   "Perform Deletions"
      Top             =   30
      Width           =   285
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "NOP"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   19
      Left            =   6780
      Style           =   1  'Graphical
      TabIndex        =   169
      ToolTipText     =   "Set Flights to NOP (No Deletion)"
      Top             =   30
      Width           =   555
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "DEL"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   18
      Left            =   6210
      Style           =   1  'Graphical
      TabIndex        =   168
      ToolTipText     =   "Perform Deletions on AODB"
      Top             =   30
      Width           =   555
   End
   Begin VB.CheckBox chkFullMode 
      Caption         =   "FM"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   167
      ToolTipText     =   "Switch to Full Mode"
      Top             =   30
      Width           =   420
   End
   Begin VB.CheckBox chkUpdMode 
      Caption         =   "UP"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   465
      Style           =   1  'Graphical
      TabIndex        =   166
      ToolTipText     =   "Switch to Update Mode"
      Top             =   30
      Width           =   420
   End
   Begin VB.Timer SpoolTimer 
      Enabled         =   0   'False
      Interval        =   5
      Left            =   12870
      Top             =   2880
   End
   Begin VB.TextBox MsgFromServer 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   11250
      Locked          =   -1  'True
      TabIndex        =   164
      Text            =   "MsgFromServer"
      Top             =   2460
      Visible         =   0   'False
      Width           =   2355
   End
   Begin VB.TextBox MsgToServer 
      BackColor       =   &H0000FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   11250
      TabIndex        =   163
      Text            =   "MsgToServer"
      Top             =   2100
      Visible         =   0   'False
      Width           =   2355
   End
   Begin VB.PictureBox Splitter 
      Height          =   135
      Index           =   3
      Left            =   750
      MousePointer    =   7  'Size N S
      ScaleHeight     =   75
      ScaleWidth      =   3945
      TabIndex        =   158
      Tag             =   "-1,-1"
      Top             =   330
      Width           =   4005
   End
   Begin VB.PictureBox Splitter 
      Height          =   135
      Index           =   2
      Left            =   2310
      MousePointer    =   7  'Size N S
      ScaleHeight     =   75
      ScaleWidth      =   3945
      TabIndex        =   141
      Tag             =   "-1,-1"
      Top             =   1200
      Width           =   4005
   End
   Begin VB.PictureBox Splitter 
      Height          =   2025
      Index           =   1
      Left            =   6180
      MousePointer    =   9  'Size W E
      ScaleHeight     =   1965
      ScaleWidth      =   75
      TabIndex        =   140
      Tag             =   "-1,-1"
      Top             =   1350
      Width           =   135
   End
   Begin VB.PictureBox Splitter 
      Height          =   2025
      Index           =   0
      Left            =   5940
      MousePointer    =   9  'Size W E
      ScaleHeight     =   1965
      ScaleWidth      =   75
      TabIndex        =   139
      Tag             =   "-1,-1"
      Top             =   1350
      Width           =   135
   End
   Begin TABLib.TAB TlxImpData 
      Height          =   825
      Index           =   1
      Left            =   5970
      TabIndex        =   146
      Top             =   4050
      Width           =   5475
      _Version        =   65536
      _ExtentX        =   9657
      _ExtentY        =   1455
      _StockProps     =   64
   End
   Begin VB.Frame SpoolerPanel 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   5880
      TabIndex        =   147
      Top             =   3480
      Visible         =   0   'False
      Width           =   8775
      Begin VB.CheckBox chkSpool 
         Caption         =   "Import"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   9
         Left            =   5220
         Style           =   1  'Graphical
         TabIndex        =   175
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkSpool 
         Caption         =   "File"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   870
         Style           =   1  'Graphical
         TabIndex        =   173
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkSpool 
         Caption         =   "Compare"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   4350
         Style           =   1  'Graphical
         TabIndex        =   160
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkSpool 
         Caption         =   "Lookup"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   6960
         Style           =   1  'Graphical
         TabIndex        =   159
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkSpool 
         Caption         =   "Check"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   3480
         Style           =   1  'Graphical
         TabIndex        =   153
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkSpool 
         Caption         =   "Reset"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   6090
         Style           =   1  'Graphical
         TabIndex        =   152
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkSpool 
         Caption         =   "AI"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   435
         Style           =   1  'Graphical
         TabIndex        =   151
         ToolTipText     =   "Auto Import"
         Top             =   0
         Width           =   420
      End
      Begin VB.CheckBox chkSpool 
         Caption         =   "AT"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   150
         ToolTipText     =   "Auto Transmit"
         Top             =   0
         Width           =   420
      End
      Begin VB.CheckBox chkSpool 
         Caption         =   "Start"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   2610
         Style           =   1  'Graphical
         TabIndex        =   149
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkSpool 
         Caption         =   "Spooler"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   148
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Load"
      Height          =   255
      Left            =   12030
      TabIndex        =   145
      Top             =   360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Save"
      Enabled         =   0   'False
      Height          =   255
      Left            =   11190
      TabIndex        =   144
      Top             =   360
      Visible         =   0   'False
      Width           =   825
   End
   Begin VB.TextBox InfoToServer 
      BackColor       =   &H0000FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8850
      TabIndex        =   143
      Text            =   "InfoToServer"
      Top             =   2460
      Visible         =   0   'False
      Width           =   2355
   End
   Begin VB.TextBox InfoFromServer 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8850
      Locked          =   -1  'True
      TabIndex        =   142
      Text            =   "InfoFromServer"
      Top             =   2100
      Visible         =   0   'False
      Width           =   2355
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   4
      Top             =   7500
      Width           =   15885
      _ExtentX        =   28019
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   7056
            MinWidth        =   7056
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   12577
            MinWidth        =   6174
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7814
            MinWidth        =   1411
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TABLib.TAB TlxImpData 
      Height          =   825
      Index           =   0
      Left            =   6270
      TabIndex        =   138
      Top             =   4350
      Visible         =   0   'False
      Width           =   5475
      _Version        =   65536
      _ExtentX        =   9657
      _ExtentY        =   1455
      _StockProps     =   64
   End
   Begin VB.CheckBox chkWork2 
      BackColor       =   &H0080FF80&
      Caption         =   "NxtDay"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   13050
      Style           =   1  'Graphical
      TabIndex        =   137
      Top             =   1230
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkWork2 
      BackColor       =   &H0080FF80&
      Caption         =   "Merge"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   12180
      Style           =   1  'Graphical
      TabIndex        =   136
      Top             =   1230
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Split"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   17
      Left            =   10920
      Style           =   1  'Graphical
      TabIndex        =   135
      ToolTipText     =   "Full Mode Import of All Records"
      Top             =   1230
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Join"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   16
      Left            =   10050
      Style           =   1  'Graphical
      TabIndex        =   134
      ToolTipText     =   "Full Mode Import of All Records"
      Top             =   1230
      Width           =   855
   End
   Begin VB.Frame ScrollCover 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   825
      Index           =   5
      Left            =   3060
      TabIndex        =   133
      Top             =   3900
      Width           =   255
   End
   Begin VB.Frame ScrollCover 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   825
      Index           =   4
      Left            =   3060
      TabIndex        =   132
      Top             =   4650
      Width           =   255
   End
   Begin VB.Frame ScrollCover 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   825
      Index           =   3
      Left            =   5640
      TabIndex        =   131
      Top             =   3900
      Width           =   255
   End
   Begin VB.Frame ScrollCover 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   825
      Index           =   2
      Left            =   2130
      TabIndex        =   130
      Top             =   3900
      Width           =   255
   End
   Begin VB.Frame ScrollCover 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   825
      Index           =   1
      Left            =   5640
      TabIndex        =   129
      Top             =   4650
      Width           =   255
   End
   Begin VB.Frame ScrollCover 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   825
      Index           =   0
      Left            =   2130
      TabIndex        =   128
      Top             =   4650
      Width           =   255
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Load"
      Enabled         =   0   'False
      Height          =   315
      Left            =   10830
      TabIndex        =   109
      Top             =   1590
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Save"
      Enabled         =   0   'False
      Height          =   315
      Left            =   9960
      TabIndex        =   108
      Top             =   1590
      Visible         =   0   'False
      Width           =   825
   End
   Begin TABLib.TAB HelperTab 
      Height          =   1455
      Index           =   0
      Left            =   6390
      TabIndex        =   103
      Top             =   1200
      Visible         =   0   'False
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   2566
      _StockProps     =   64
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Full"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   15
      Left            =   9180
      Style           =   1  'Graphical
      TabIndex        =   93
      ToolTipText     =   "Full Mode Import of All Records"
      Top             =   1230
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Single"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   14
      Left            =   8310
      Style           =   1  'Graphical
      TabIndex        =   92
      ToolTipText     =   "Import Records of the Selected Aircraft(s)"
      Top             =   1230
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   13
      Left            =   2430
      Style           =   1  'Graphical
      TabIndex        =   87
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB RotaLink 
      Height          =   705
      Index           =   4
      Left            =   2400
      TabIndex        =   77
      Top             =   4770
      Width           =   885
      _Version        =   65536
      _ExtentX        =   1561
      _ExtentY        =   1244
      _StockProps     =   64
      FontName        =   "Courier New"
   End
   Begin TABLib.TAB RotaLink 
      Height          =   705
      Index           =   1
      Left            =   3420
      TabIndex        =   78
      Top             =   4770
      Width           =   2445
      _Version        =   65536
      _ExtentX        =   4313
      _ExtentY        =   1244
      _StockProps     =   64
      FontName        =   "Courier New"
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Keep"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   12
      Left            =   9390
      Style           =   1  'Graphical
      TabIndex        =   53
      ToolTipText     =   "Don't clear selected lines"
      Top             =   270
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Delete"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   11
      Left            =   8520
      Style           =   1  'Graphical
      TabIndex        =   52
      ToolTipText     =   "Activate DoubleClick for Deletes"
      Top             =   270
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Update"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   10
      Left            =   7650
      Style           =   1  'Graphical
      TabIndex        =   51
      ToolTipText     =   "Activate DoubleClick for Updates"
      Top             =   270
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Insert"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   9
      Left            =   6780
      Style           =   1  'Graphical
      TabIndex        =   50
      ToolTipText     =   "Activate DoubleClick for Inserts"
      Top             =   270
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Jump"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   10260
      Style           =   1  'Graphical
      TabIndex        =   49
      ToolTipText     =   "AutoJump after each Transaction"
      Top             =   270
      Value           =   1  'Checked
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Ask"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   5910
      Style           =   1  'Graphical
      TabIndex        =   48
      Top             =   270
      Value           =   1  'Checked
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Activate"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   5040
      Style           =   1  'Graphical
      TabIndex        =   47
      ToolTipText     =   "Activate the Transaction Functions"
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Doubles"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   690
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Rotation"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   3300
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "AODB"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   1560
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Shrink"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   4170
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Show Differences Only"
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   7650
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   30
      Width           =   855
   End
   Begin VB.Frame BottomLabels 
      Height          =   855
      Index           =   1
      Left            =   30
      TabIndex        =   9
      Top             =   6540
      Width           =   6015
      Begin VB.CheckBox chkSrvImp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   162
         Top             =   0
         Width           =   585
      End
      Begin VB.CheckBox chkSearch 
         Caption         =   "Find"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   705
         Style           =   1  'Graphical
         TabIndex        =   111
         Top             =   495
         Width           =   585
      End
      Begin VB.CheckBox chkSearch 
         Caption         =   "Look"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   98
         Top             =   495
         Width           =   600
      End
      Begin VB.TextBox txtSearch 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   90
         MaxLength       =   9
         TabIndex        =   97
         Top             =   195
         Width           =   1185
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   1980
         TabIndex        =   102
         Tag             =   "JOIN"
         ToolTipText     =   "Rotation ready to join"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1980
         TabIndex        =   101
         Tag             =   "JOIN"
         ToolTipText     =   "Rotation ready to join"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   1320
         TabIndex        =   10
         Tag             =   "NONE"
         ToolTipText     =   "Loaded from AODB"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H008080FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   9
         Left            =   2640
         TabIndex        =   91
         Tag             =   "MIXED"
         ToolTipText     =   "Join with update or insert"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H008080FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   2640
         TabIndex        =   90
         Tag             =   "MIXED"
         ToolTipText     =   "Join with update or insert"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FF00FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   7
         Left            =   1320
         TabIndex        =   42
         Tag             =   "NONE"
         ToolTipText     =   "Loaded from AODB"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   10
         Left            =   3300
         TabIndex        =   41
         Tag             =   "UPD"
         ToolTipText     =   "Different to File (Update)"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   11
         Left            =   3960
         TabIndex        =   40
         Tag             =   "DEL"
         ToolTipText     =   "Delete Flight (In AODB but not in File)"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   12
         Left            =   4620
         TabIndex        =   39
         Tag             =   "REM"
         ToolTipText     =   "Doubles in AODB"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   13
         Left            =   5280
         TabIndex        =   38
         Tag             =   "DONE"
         ToolTipText     =   "Identical in File and AODB"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   6
         Left            =   5280
         TabIndex        =   32
         Tag             =   "DONE"
         ToolTipText     =   "Identical in File and AODB"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   5
         Left            =   4620
         TabIndex        =   26
         Tag             =   "REM"
         ToolTipText     =   "Doubles in AODB"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   4
         Left            =   3960
         TabIndex        =   17
         Tag             =   "DEL"
         ToolTipText     =   "Delete Flight (In AODB but not in File)"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   3
         Left            =   3300
         TabIndex        =   16
         Tag             =   "UPD"
         ToolTipText     =   "Different to File (Update)"
         Top             =   195
         Width           =   645
      End
   End
   Begin VB.Frame BottomLabels 
      Height          =   855
      Index           =   3
      Left            =   6120
      TabIndex        =   79
      Top             =   6540
      Visible         =   0   'False
      Width           =   3045
      Begin VB.CheckBox chkRota 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   2010
         Style           =   1  'Graphical
         TabIndex        =   86
         Top             =   210
         Width           =   945
      End
      Begin VB.CheckBox chkRota 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   85
         Top             =   210
         Width           =   945
      End
      Begin VB.CheckBox chkRota 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   84
         Top             =   210
         Width           =   945
      End
      Begin VB.Label RotaKeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   3
         Left            =   2250
         TabIndex        =   83
         Top             =   510
         Width           =   705
      End
      Begin VB.Label RotaKeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   2
         Left            =   1530
         TabIndex        =   82
         Top             =   510
         Width           =   705
      End
      Begin VB.Label RotaKeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   1
         Left            =   810
         TabIndex        =   81
         Top             =   510
         Width           =   705
      End
      Begin VB.Label RotaKeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   0
         Left            =   90
         TabIndex        =   80
         Top             =   510
         Width           =   705
      End
   End
   Begin VB.Frame BottomLabels 
      Height          =   855
      Index           =   2
      Left            =   6120
      TabIndex        =   24
      Top             =   5610
      Width           =   3045
      Begin VB.Frame RotaCover 
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   90
         TabIndex        =   124
         Top             =   510
         Visible         =   0   'False
         Width           =   2865
         Begin VB.CheckBox chkRota 
            Caption         =   "Flights"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   5
            Left            =   2130
            Style           =   1  'Graphical
            TabIndex        =   127
            Top             =   0
            Width           =   735
         End
         Begin VB.CheckBox chkRota 
            Caption         =   "Transmit"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   990
            Style           =   1  'Graphical
            TabIndex        =   126
            Top             =   0
            Width           =   1125
         End
         Begin VB.CheckBox chkRota 
            BackColor       =   &H0080FF80&
            Caption         =   "Rotations"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   125
            Top             =   0
            Value           =   1  'Checked
            Width           =   975
         End
      End
      Begin MSComctlLib.ProgressBar MyProgressBar 
         Height          =   120
         Index           =   1
         Left            =   120
         TabIndex        =   55
         Top             =   630
         Visible         =   0   'False
         Width           =   2805
         _ExtentX        =   4948
         _ExtentY        =   212
         _Version        =   393216
         Appearance      =   0
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar MyProgressBar 
         Height          =   240
         Index           =   0
         Left            =   120
         TabIndex        =   43
         Top             =   510
         Visible         =   0   'False
         Width           =   2805
         _ExtentX        =   4948
         _ExtentY        =   423
         _Version        =   393216
         Appearance      =   0
         Scrolling       =   1
      End
      Begin VB.Shape CedaLed 
         BackColor       =   &H00C0C0C0&
         BorderColor     =   &H00000000&
         FillStyle       =   0  'Solid
         Height          =   165
         Index           =   1
         Left            =   1665
         Top             =   255
         Visible         =   0   'False
         Width           =   465
      End
      Begin VB.Shape CedaLed 
         BackColor       =   &H00C0C0C0&
         BorderColor     =   &H00000000&
         FillStyle       =   0  'Solid
         Height          =   165
         Index           =   0
         Left            =   1155
         Top             =   255
         Visible         =   0   'False
         Width           =   465
      End
      Begin VB.Label CurPos 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   4
         Left            =   2205
         TabIndex        =   54
         Top             =   195
         Visible         =   0   'False
         Width           =   750
      End
      Begin VB.Label CurPos 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   90
         TabIndex        =   44
         Top             =   495
         Width           =   2865
      End
      Begin VB.Label CurPos 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   2205
         TabIndex        =   30
         Top             =   195
         Width           =   750
      End
      Begin VB.Label CurPos 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1080
         TabIndex        =   28
         Top             =   195
         Width           =   1110
      End
      Begin VB.Label CurPos 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         TabIndex        =   27
         Top             =   195
         Width           =   975
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Flights"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   30
      Visible         =   0   'False
      Width           =   855
   End
   Begin TABLib.TAB HelperTab 
      Height          =   1365
      Index           =   1
      Left            =   7260
      TabIndex        =   104
      Top             =   1530
      Visible         =   0   'False
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB RotaLink 
      Height          =   705
      Index           =   5
      Left            =   2400
      TabIndex        =   106
      Top             =   4020
      Width           =   885
      _Version        =   65536
      _ExtentX        =   1561
      _ExtentY        =   1244
      _StockProps     =   64
      FontName        =   "Courier New"
   End
   Begin TABLib.TAB RotaLink 
      Height          =   705
      Index           =   3
      Left            =   3420
      TabIndex        =   107
      Top             =   4020
      Width           =   2445
      _Version        =   65536
      _ExtentX        =   4313
      _ExtentY        =   1244
      _StockProps     =   64
      FontName        =   "Courier New"
   End
   Begin VB.Frame TopLabels 
      Caption         =   "Airline Filter / Flight Filter"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   30
      TabIndex        =   6
      Top             =   510
      Width           =   14445
      Begin TABLib.TAB FlnoTabCombo 
         Height          =   375
         Index           =   0
         Left            =   4650
         TabIndex        =   96
         Top             =   270
         Width           =   1365
         _Version        =   65536
         _ExtentX        =   2408
         _ExtentY        =   661
         _StockProps     =   64
      End
      Begin TABLib.TAB RegnTabCombo 
         Height          =   375
         Index           =   0
         Left            =   4650
         TabIndex        =   95
         Top             =   270
         Width           =   1365
         _Version        =   65536
         _ExtentX        =   2408
         _ExtentY        =   661
         _StockProps     =   64
      End
      Begin TABLib.TAB AlcTabCombo 
         Height          =   375
         Index           =   0
         Left            =   90
         TabIndex        =   94
         Top             =   270
         Width           =   4515
         _Version        =   65536
         _ExtentX        =   7964
         _ExtentY        =   661
         _StockProps     =   64
      End
      Begin VB.Frame DspShrink 
         Height          =   675
         Left            =   6060
         TabIndex        =   61
         Top             =   -30
         Visible         =   0   'False
         Width           =   2775
         Begin VB.CheckBox chkShrink 
            Caption         =   "OK"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   2325
            Style           =   1  'Graphical
            TabIndex        =   62
            Top             =   270
            Width           =   360
         End
         Begin VB.Label lblShrinkCover 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   420
            Index           =   6
            Left            =   1890
            TabIndex        =   74
            Tag             =   "C2"
            Top             =   30
            Width           =   330
         End
         Begin VB.Label lblShrinkCover 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   420
            Index           =   3
            Left            =   810
            TabIndex        =   71
            Tag             =   "C2"
            Top             =   30
            Width           =   330
         End
         Begin VB.Label lblShrinkCover 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   420
            Index           =   2
            Left            =   450
            TabIndex        =   70
            Tag             =   "C2"
            Top             =   30
            Width           =   330
         End
         Begin VB.Label lblShrinkCover 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   420
            Index           =   1
            Left            =   90
            TabIndex        =   69
            Tag             =   "C2"
            Top             =   30
            Width           =   330
         End
         Begin VB.Label lblShrinkCover 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   195
            Index           =   12
            Left            =   1530
            TabIndex        =   122
            Tag             =   "C2"
            Top             =   0
            Width           =   330
         End
         Begin VB.Label lblShrinkCover 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   195
            Index           =   11
            Left            =   1170
            TabIndex        =   121
            Tag             =   "C2"
            Top             =   0
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   1530
            TabIndex        =   116
            Tag             =   "C2"
            Top             =   420
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   1170
            TabIndex        =   115
            Tag             =   "C2"
            Top             =   420
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Index           =   6
            Left            =   1890
            TabIndex        =   68
            Tag             =   "C4"
            Top             =   195
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   1530
            TabIndex        =   67
            Tag             =   "S3"
            Top             =   195
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   1170
            TabIndex        =   66
            Tag             =   "C3"
            Top             =   195
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Index           =   3
            Left            =   810
            TabIndex        =   65
            Tag             =   "S2"
            Top             =   195
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Index           =   2
            Left            =   450
            TabIndex        =   64
            Tag             =   "C1"
            Top             =   195
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Index           =   1
            Left            =   90
            TabIndex        =   63
            Tag             =   "C2"
            Top             =   195
            Width           =   330
         End
         Begin VB.Label lblShrinkCover 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   195
            Index           =   4
            Left            =   1170
            TabIndex        =   72
            Tag             =   "C2"
            Top             =   30
            Width           =   330
         End
         Begin VB.Label lblShrinkCover 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   195
            Index           =   5
            Left            =   1530
            TabIndex        =   73
            Tag             =   "C2"
            Top             =   30
            Width           =   330
         End
         Begin VB.Label lblShrinkCover 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   195
            Index           =   8
            Left            =   90
            TabIndex        =   118
            Tag             =   "C2"
            Top             =   0
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   90
            TabIndex        =   112
            Tag             =   "C2"
            Top             =   405
            Width           =   330
         End
         Begin VB.Label lblShrinkCover 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   195
            Index           =   9
            Left            =   450
            TabIndex        =   119
            Tag             =   "C2"
            Top             =   0
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   450
            TabIndex        =   113
            Tag             =   "C2"
            Top             =   405
            Width           =   330
         End
         Begin VB.Label lblShrinkCover 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   195
            Index           =   10
            Left            =   810
            TabIndex        =   120
            Tag             =   "C2"
            Top             =   0
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   810
            TabIndex        =   114
            Tag             =   "C2"
            Top             =   405
            Width           =   330
         End
         Begin VB.Label lblShrinkCover 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   195
            Index           =   13
            Left            =   1890
            TabIndex        =   123
            Tag             =   "C2"
            Top             =   0
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   1890
            TabIndex        =   117
            Tag             =   "C2"
            Top             =   405
            Width           =   330
         End
      End
      Begin VB.ComboBox FlnoList 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4410
         Sorted          =   -1  'True
         TabIndex        =   29
         Top             =   90
         Visible         =   0   'False
         Width           =   1635
      End
      Begin VB.Frame DspLoading 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   6030
         TabIndex        =   18
         Top             =   270
         Visible         =   0   'False
         Width           =   2775
         Begin VB.Label Label6 
            Caption         =   "Loading"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   90
            TabIndex        =   21
            Top             =   30
            Width           =   735
         End
         Begin VB.Label lblVpto 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1830
            TabIndex        =   20
            Top             =   0
            Width           =   945
         End
         Begin VB.Label lblVpfr 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   840
            TabIndex        =   19
            Top             =   0
            Width           =   945
         End
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   30
         TabIndex        =   154
         Top             =   240
         Width           =   6045
      End
      Begin VB.Frame DspLoaded 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   9270
         TabIndex        =   56
         Top             =   240
         Width           =   4995
         Begin VB.Label TotalSecs 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3975
            TabIndex        =   75
            Top             =   30
            Width           =   1005
         End
         Begin VB.Label Label1 
            Caption         =   "Loaded"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   210
            TabIndex        =   60
            Top             =   60
            Width           =   675
         End
         Begin VB.Label CurEnd 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1980
            TabIndex        =   59
            Top             =   30
            Width           =   945
         End
         Begin VB.Label CurBegin 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   990
            TabIndex        =   58
            Top             =   30
            Width           =   945
         End
         Begin VB.Label UsedSec 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2940
            TabIndex        =   57
            Top             =   30
            Width           =   1005
         End
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000014&
         Index           =   2
         X1              =   60
         X2              =   60
         Y1              =   0
         Y2              =   690
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000010&
         Index           =   0
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   90
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000015&
         Index           =   1
         X1              =   15
         X2              =   15
         Y1              =   0
         Y2              =   90
      End
   End
   Begin VB.Frame DspLimit 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   12330
      TabIndex        =   155
      Top             =   30
      Width           =   2805
      Begin VB.TextBox ServerTime 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   156
         Tag             =   "200205240800"
         Text            =   "24.05.2002 / 08:00"
         Top             =   0
         Width           =   1800
      End
      Begin VB.Label Label2 
         Caption         =   "Time Limit"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   0
         TabIndex        =   157
         Top             =   45
         Width           =   915
      End
   End
   Begin TABLib.TAB NoopList 
      Height          =   2475
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Top             =   1350
      Width           =   2295
      _Version        =   65536
      _ExtentX        =   4048
      _ExtentY        =   4366
      _StockProps     =   64
      FontName        =   "Courier New"
   End
   Begin TABLib.TAB NoopList 
      Height          =   2475
      Index           =   2
      Left            =   2460
      TabIndex        =   1
      Top             =   1350
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   4366
      _StockProps     =   64
      FontName        =   "Courier New"
   End
   Begin TABLib.TAB NoopList 
      Height          =   2475
      Index           =   1
      Left            =   3420
      TabIndex        =   2
      Top             =   1350
      Width           =   2295
      _Version        =   65536
      _ExtentX        =   4048
      _ExtentY        =   4366
      _StockProps     =   64
      FontName        =   "Courier New"
   End
   Begin TABLib.TAB RotaLink 
      Height          =   705
      Index           =   2
      Left            =   0
      TabIndex        =   105
      Top             =   4020
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   1244
      _StockProps     =   64
      FontName        =   "Courier New"
   End
   Begin TABLib.TAB RotaLink 
      Height          =   705
      Index           =   0
      Left            =   0
      TabIndex        =   76
      Top             =   4770
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   1244
      _StockProps     =   64
      FontName        =   "Courier New"
   End
   Begin VB.Frame BottomLabels 
      Height          =   855
      Index           =   0
      Left            =   30
      TabIndex        =   7
      Top             =   5610
      Width           =   6015
      Begin VB.CheckBox chkCliImp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   161
         Top             =   0
         Width           =   585
      End
      Begin VB.CheckBox chkSearch 
         Caption         =   "Find"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   705
         Style           =   1  'Graphical
         TabIndex        =   110
         Top             =   495
         Width           =   585
      End
      Begin VB.TextBox txtSearch 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         MaxLength       =   9
         TabIndex        =   45
         Top             =   195
         Width           =   1185
      End
      Begin VB.CheckBox chkSearch 
         Caption         =   "Look"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   46
         Top             =   495
         Width           =   600
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1980
         TabIndex        =   100
         Tag             =   "JOIN"
         ToolTipText     =   "Rotation ready to join"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   1980
         TabIndex        =   99
         Tag             =   "JOIN"
         ToolTipText     =   "Rotation ready to join"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H008080FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   9
         Left            =   2640
         TabIndex        =   89
         Tag             =   "MIXED"
         ToolTipText     =   "Join with update or insert"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H008080FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   2640
         TabIndex        =   88
         Tag             =   "MIXED"
         ToolTipText     =   "Join with update or insert"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00000000&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   285
         Index           =   7
         Left            =   1320
         TabIndex        =   37
         Tag             =   "NONE"
         ToolTipText     =   "Loaded from File"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   10
         Left            =   3300
         TabIndex        =   36
         Tag             =   "UPD"
         ToolTipText     =   "Different to AODB (Update)"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FF00&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   11
         Left            =   3960
         TabIndex        =   35
         Tag             =   "INS"
         ToolTipText     =   "Must be inserted."
         Top             =   495
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   12
         Left            =   4620
         TabIndex        =   34
         Tag             =   "ERR"
         ToolTipText     =   "Doubles in File"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   13
         Left            =   5280
         TabIndex        =   33
         Tag             =   "DONE"
         ToolTipText     =   "Identical in File and AODB"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   6
         Left            =   5280
         TabIndex        =   31
         Tag             =   "DONE"
         ToolTipText     =   "Identical in File and AODB"
         Top             =   180
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   5
         Left            =   4620
         TabIndex        =   25
         Tag             =   "ERR"
         ToolTipText     =   "Doubles in File"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FF00&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   4
         Left            =   3960
         TabIndex        =   15
         Tag             =   "INS"
         ToolTipText     =   "Must be inserted."
         Top             =   195
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   3
         Left            =   3300
         TabIndex        =   14
         Tag             =   "UPD"
         ToolTipText     =   "Different to AODB (Update)"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   1320
         TabIndex        =   8
         Tag             =   "NONE"
         ToolTipText     =   "Loaded from File"
         Top             =   195
         Width           =   645
      End
   End
   Begin TABLib.TAB InfoSpooler 
      Height          =   555
      Left            =   8850
      TabIndex        =   165
      Top             =   2820
      Visible         =   0   'False
      Width           =   3945
      _Version        =   65536
      _ExtentX        =   6959
      _ExtentY        =   979
      _StockProps     =   64
   End
   Begin VB.Label Label3 
      Caption         =   "<< A little bit oversized, isn't it ? >>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   16110
      TabIndex        =   13
      Top             =   1710
      Width           =   2595
   End
End
Attribute VB_Name = "NoopFlights"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim AddArrFldCnt As Long
Dim AddDepFldCnt As Long

'Dim DefaultFileHeader As String
Dim ApdxIsMerged As Boolean
Dim TelexPoolFile As String
Dim ScoreFileHeader As String
Dim IsMultiSel As Long
Dim CmdIfrDone As Long
Dim CmdUfrDone As Long
Dim CmdDfrDone As Long
Dim CmdSprDone As Long
Dim CmdJofDone As Long
Dim CmdSplDone As Long
Public IsImported As Long
Dim TotalStartZeit
Dim AlreadyActive As Boolean
Dim MyFullMode As Boolean
Dim MyUpdateMode As Boolean
Dim MyMainFullMode As Boolean
Dim MyMainUpdateMode As Boolean
Dim FormIsBusy As Boolean
Dim ImportIsBusy As Boolean
Dim SeriousError As Boolean
Dim EmptyLine As String
Dim ArrFlightLineNo(8) As Long
Dim DepFlightLineNo(8) As Long
Dim ArrFlightData(8) As String
Dim DepFlightData(8) As String
Dim IsArrShiftLeft(8) As Boolean
Dim IsDepShiftLeft(8) As Boolean
Dim IsRotation(8) As Boolean
Dim IsFlightPair(8) As Boolean
Dim RotaJoinCnt(8) As Long ''Update Integer to long data type by Phyoe 20-mar-2012
Dim CmdLineIdx As String
Public RotaSelFields As String
Dim SelRegnList As String
Dim MyCompFields As String
Dim ImpEmptyData As String
Dim ArrUpdColList As String
Dim DepUpdColList As String
Dim EmptyLineColor As Long
Dim LimitColor As Long
Dim PrflCol As Long
Dim FtypCol As Long
Dim FlnoCol As Long
Public AdidCol As Long
Dim TimeCol As Long
Dim DoopCol As Long
Dim Act3Col As Long
Dim Apc3Col As Long
Dim Via3Col As Long
Dim StypCol As Long
Dim FltnCol As Long
Dim TifdCol As Long
Dim RtypCol As Long
Dim RegnCol As Long
Dim FldaCol As Long
Dim SkedCol As Long
Dim RoutCol As Long
Public FkeyCol As Long
Public RkeyCol As Long
Public UrnoCol As Long
Dim RotaCol As Long
Public ChckCol As Long
Public DoneCol As Long
Dim Act5Col As Long
Dim Apc4Col As Long
Dim Via4Col As Long
Dim RemaCol As Long
Dim ApdxCol As Long
Dim LastMsgNum As Long
Dim SpoolImpData As Boolean
Dim TelexIsConnected As Boolean
Dim MsgIsPending As Boolean
Dim MainHeadCols As String
Dim NoEventEcho As Boolean
Dim StopCedaAction As Boolean
Dim DeleteAllSelected As Boolean

Private Sub AlcTabCombo_ComboSelChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal OldValue As String, ByVal NewValues As String)
    Dim NewAlc As String
    NewAlc = Trim(GetItem(NewValues, 1, ","))
    If NewAlc = "" Then NewAlc = Trim(GetItem(NewValues, 2, ","))
    If NewAlc = "***" Then NewAlc = "-ALL-"
    If (Not FormIsBusy) And (NewAlc <> CurPos(0).Caption) Then
        CurPos(0).Caption = NewAlc
        If (NewAlc <> "-ALL-") And (AlcTabCombo(1).GetLineCount > 1) Then
            AdjustScrollMaster FlnoCol, 3, NewAlc, False
        Else
            AdjustScrollMaster -1, -1, "", False
        End If
        CreateDistinctFlno NewAlc
    End If
End Sub

Private Sub chkCliImp_Click(Index As Integer)
    Dim UseSelList As String
    Dim ActionType As String
    If chkCliImp(Index).Value = 1 Then
        If DataSystemType <> "OAFOS" Then
            ActionType = DefineColorAction(CliFkeyCnt(Index).BackColor)
            If (RotationMode) And (ActionType = "MIXED") Then ActionType = "JOIN"
            If (RotationMode) And (ActionType <> "JOIN") Then ActionType = ""
            If ActionType <> "" Then
                UseSelList = NoopList(2).GetLinesByBackColor(CliFkeyCnt(Index).BackColor)
                CallServerAction ActionType, Val(CliFkeyCnt(Index).Caption), True, UseSelList
            End If
        End If
        chkCliImp(Index).Value = 0
    End If
End Sub

Private Sub chkFullMode_Click()
    If chkFullMode.Value = 1 Then
        chkFullMode.BackColor = LightGreen
        chkUpdMode.Value = 0
        MyFullMode = True
        MyUpdateMode = False
        chkWork(2).Value = 0
    Else
        chkFullMode.BackColor = vbButtonFace
        chkUpdMode.Value = 1
    End If
End Sub

Private Sub chkRota_Click(Index As Integer)
    If chkRota(Index).Value = 1 Then chkRota(Index).BackColor = LightGreen Else chkRota(Index).BackColor = vbButtonFace
    Select Case Index
        Case 3
            If chkRota(Index).Value = 1 Then chkRota(5).Value = 0 Else chkRota(5).Value = 1
        Case 4
            If chkRota(Index).Value = 1 Then
                'TransmitSelectedFlights
                chkRota(Index).Value = 0
            End If
        Case 5
            If chkRota(Index).Value = 1 Then chkRota(3).Value = 0 Else chkRota(3).Value = 1
        Case Else
    End Select
    If chkRota(Index).Value = 1 Then
        ToggleDetailCaption
        NoopList(2).SetCurrentSelection NoopList(2).GetCurrentSelected
    End If
End Sub

Private Sub chkSearch_Click(Index As Integer)
    If chkSearch(Index).Value = 1 Then
        chkSearch(Index).BackColor = LightGreen
        Me.Refresh
        SearchInList Index, True, False
        chkSearch(Index).Value = 0
        NoopList(2).SetFocus
    Else
        chkSearch(Index).BackColor = vbButtonFace
        Me.Refresh
    End If
End Sub

Private Sub chkShrink_Click(Index As Integer)
    If chkShrink(Index).Value = 1 Then
        FormIsBusy = True
        AdjustScrollMaster -1, -2, "", False
        FormIsBusy = False
        AdjustShrinkButton False
        chkShrink(Index).Value = 0
    End If
End Sub

Private Sub chkSpool_Click(Index As Integer)
    Dim LastValue As Integer
    If chkSpool(Index).Value = 1 Then
        chkSpool(Index).BackColor = LightGreen
        chkSpool(Index).Refresh
        Select Case Index
            Case 0  'Spool
                chkWork(2).Value = 0
                chkSpool(4).Value = 1
                TlxImpData(0).Visible = True
                TlxImpData(1).Visible = False
                Me.Refresh
            Case 1  'Start
                SetFormOnTop Me, True
                Me.SetFocus
                chkSpool(0).Value = 0
                CreateTlxImpRecords
                SetFormOnTop Me, False
                chkSpool(5).Value = 1
                HandleAutoImport
                chkSpool(Index).Value = 0
            Case 2  'AT (Auto Transmit)
                MsgToTelexPool "0,ATSET"
            Case 3  'AI (Auto Import)
                MsgToTelexPool "0,AISET"
            Case 4  'Reset
                TlxImpData(0).ResetContent
                TlxImpData(0).Refresh
                TlxImpData(0).SetUniqueFields "2"
                TlxImpData(1).ResetContent
                TlxImpData(1).Refresh
                TlxImpData(1).SetUniqueFields "0"
                DataTab.ResetContent
                'TelexPoolFile = ""
                chkSpool(Index).Value = 0
            Case 5  'Check
                chkSpool(1).Value = 1
                CheckImpBasicData -1, -1
                chkSpool(Index).Value = 0
            Case 6  'Basic Data
                Validation.Show
            Case 7
                chkWork(2).Value = 0
                chkWork(2).Value = 1
            Case 8  'FILE ReadyToGo
                chkSpool(0).Value = 1
                chkSpool(1).Value = 0
                chkSpool(5).Value = 0
                chkSpool(7).Value = 0
                chkSpool(9).Value = 0
                HandleFileImport
                chkSpool(8).Value = 0
            Case 9
                LastValue = chkSpool(3).Value
                chkSpool(3).Value = 1
                HandleAutoImport
                chkSpool(1).Value = 0
                chkSpool(5).Value = 0
                chkSpool(7).Value = 0
                chkSpool(9).Value = 0
                chkSpool(3).Value = LastValue
            Case Else
        End Select
    Else
        chkSpool(Index).BackColor = vbButtonFace
        chkSpool(Index).Refresh
        Select Case Index
            Case 0  'Spool
                TlxImpData(1).Visible = True
                TlxImpData(0).Visible = False
                Me.Refresh
            Case 2  'AT (Auto Transmit)
                MsgToTelexPool "0,ATOFF"
            Case 3  'AI (Auto Import)
                MsgToTelexPool "0,AIOFF"
            Case 6  'Basic Data
                Validation.Hide
            Case Else
        End Select
    End If
End Sub
Private Sub HandleFileImport()
    Dim ScoreLine As String
    Dim tmpSqid As String
    Dim tmpPkid As String
    Dim tmpTrid As String
    Dim tmpData As String
    Dim tmpUrno As String
    Dim FldSep As String
    Dim MyApc3 As String
    'Dim ChkApc As String
    Dim SpoolLine As String
    Dim ApcPos As Integer
    'Dim OffSet As Integer
    'Dim PosDif As Integer
    'Dim PosChk As Boolean
    Dim CurLine As Long
    Dim MaxLine As Long
    On Error GoTo ErrHdl
    MyApc3 = HomeAirport
    FldSep = Chr(15)
    'TlxImpData(0).ResetContent
    'TlxImpData(1).ResetContent
    chkSpool(4).Value = 1
    If TelexPoolFile = "" Then
        'Test cases
'        TelexPoolFile = "D:\Ufis\Data\ImportTool\DXB\UDXBW09201002011012.txt"
'        TelexPoolFile = "D:\Ufis\Data\ImportTool\DXB\EP-SCORE-half.txt"
'        TelexPoolFile = "D:\Ufis\Data\ImportTool\DXB\SIN-SCORE-half.txt"
'        MyApc3 = "DXB"
'        TelexPoolFile = "D:\Ufis\Data\ImportTool\DEL\ScoreFile_updDEL.txt"
'        MyApc3 = "DEL"
'        TelexPoolFile = "D:\Ufis\Data\ImportTool\DXB\LisData.txt"
'        MyApc3 = "LIS"
    End If
    If TelexPoolFile <> "" Then
        If chkSpool(0).Value = 0 Then TlxImpData(0).ResetContent
        ReadAnyFile DataTab, TelexPoolFile
        If (AddOnDataFound) And (Not ApdxIsMerged) And (Not AddOnDataMerge) Then
            ModifyTabLayout 2
            InitTlxImpLayout "MODIFY"
        End If
        ApcPos = 0
        MaxLine = DataTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            ScoreLine = DataTab.GetLineValues(CurLine)
            If InStr(ScoreLine, "[APPEND]") > 0 Then
            Else
                tmpSqid = CStr(CurLine)
                ApcPos = CheckScoreLineFormat(ScoreLine, MyApc3, tmpSqid)
                tmpPkid = tmpSqid
                tmpData = ScoreLine
                tmpUrno = "0000000000"
                tmpTrid = Left(ScoreLine, 8)
                SpoolLine = ""
                SpoolLine = SpoolLine & tmpSqid & FldSep
                SpoolLine = SpoolLine & tmpPkid & FldSep
                SpoolLine = SpoolLine & tmpTrid & FldSep
                SpoolLine = SpoolLine & tmpData & FldSep
                SpoolLine = SpoolLine & tmpUrno & FldSep
                TlxImpData(0).InsertTextLine SpoolLine, False
            End If
        Next
        TlxImpData(0).AutoSizeColumns
        TlxImpData(0).Refresh
    End If
    Exit Sub
ErrHdl:
    'MsgBox Err.Description
End Sub
Private Function CheckScoreLineFormat(ScoreLine As String, MyApc3 As String, SeqId As String) As Integer
    Dim ChkApc As String
    Dim tmpData As String
    Dim ApcPos As Integer
    Dim OffSet As Integer
    Dim PosDif As Integer
    Dim PosChk As Boolean
    PosChk = False
    If PosChk = False Then
        OffSet = InStr(ScoreLine, MyApc3)
        If OffSet = 1 Then
            tmpData = ""
            tmpData = tmpData & Right("        " & SeqId, 8)
            tmpData = tmpData & "K0SEA"
            tmpData = tmpData & ScoreLine
            ScoreLine = tmpData
            ApcPos = 14
        ElseIf OffSet > 0 Then
            PosDif = 14 - OffSet
            If PosDif > 0 Then
                ScoreLine = Space(PosDif) & ScoreLine
                ApcPos = 14
            ElseIf PosDif < 0 Then
            End If
        Else
            'Wrong File Format
        End If
    End If
    CheckScoreLineFormat = ApcPos
End Function
Private Sub HandleAutoImport()
    If chkSpool(3).Value = 1 Then
        chkWork(2).Value = 1
        If chkWork(2).Value = 1 Then
            chkWork(6).Value = 1
            If chkCliImp(4).Visible Then chkCliImp(4).Value = 1 'Insert
            If chkCliImp(3).Visible Then chkCliImp(3).Value = 1 'Update
            If chkCliImp(2).Visible Then chkCliImp(2).Value = 1 'Update and Join
            If chkCliImp(1).Visible Then chkCliImp(1).Value = 1 'Join unchanged
            If chkSrvImp(4).Visible Then chkSrvImp(4).Value = 1 'Delete
            If FormIsVisible("CedaSpooler") = True Then
                CedaSpooler.chkWork(1).Value = 1
                CheckFullImport "IMPORTED"
            End If
            chkWork(6).Value = 0
        End If
    End If
End Sub

Public Sub CheckFullImport(ForWhat As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpUrno As String
    Dim tmpUrnoList As String
    tmpUrnoList = ""
    MaxLine = TlxImpData(1).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpUrno = TlxImpData(1).GetFieldValue(CurLine, "URNO")
        If (tmpUrno <> "") And (InStr(tmpUrnoList, tmpUrno) = 0) Then
            tmpUrnoList = tmpUrnoList & tmpUrno & ";"
        End If
    Next
    If tmpUrnoList <> "" Then
        tmpUrnoList = "0," & ForWhat & "," & tmpUrnoList
        MsgToTelexPool tmpUrnoList
    End If
End Sub

Private Sub CheckImpBasicData(FromLine As Long, ToLine As Long)
    Dim IsOk As Boolean
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FirstLine As Long
    Dim LastLine As Long
    Dim tmpErrCols As String
    Dim tmpUrno As String
    Dim tmpUrnoList As String
    Load Validation
    tmpUrnoList = ""
    MaxLine = TlxImpData(1).GetLineCount - 1
    FirstLine = FromLine
    If FirstLine < 0 Then FirstLine = 0
    LastLine = ToLine
    If LastLine < 0 Then LastLine = MaxLine
    For CurLine = FirstLine To LastLine
        tmpErrCols = ""
        'Check if the line belongs to this airport
        If Trim(TlxImpData(1).GetFieldValue(CurLine, "HOPO")) <> HomeAirport Then tmpErrCols = tmpErrCols & "HOPO,"
        'Check Airline Codes
        CheckBasicValues 0, CurLine, "FLCA", "ALC2", "FLCA", "ALC3", tmpErrCols
        CheckBasicValues 0, CurLine, "FLCD", "ALC2", "FLCD", "ALC3", tmpErrCols
        'Check Airport Codes (or translate APC4)
        CheckBasicValues 1, CurLine, "ORG3", "APC3", "ORG4", "APC4", tmpErrCols
        CheckBasicValues 1, CurLine, "VSA3", "APC3", "VSA4", "APC4", tmpErrCols
        CheckBasicValues 1, CurLine, "DES3", "APC3", "DES4", "APC4", tmpErrCols
        CheckBasicValues 1, CurLine, "VSD3", "APC3", "VSD4", "APC4", tmpErrCols
        'Check Aircraft Type (or translate ACT5)
        CheckBasicValues 2, CurLine, "ACT3", "ACT3", "ACT5", "ACT5", tmpErrCols
        CheckFlightPeriod CurLine, tmpErrCols
        If tmpErrCols <> "" Then
            SetErrorFlags CurLine, tmpErrCols
            tmpUrno = TlxImpData(1).GetFieldValue(CurLine, "URNO")
            If (tmpUrno <> "") And (InStr(tmpUrnoList, tmpUrno) = 0) Then
                tmpUrnoList = tmpUrnoList & tmpUrno & ";"
            End If
        End If
    Next
    TlxImpData(1).RedrawTab
    If tmpUrnoList <> "" Then
        tmpUrnoList = "0,ERROR," & tmpUrnoList
        MsgToTelexPool tmpUrnoList
    End If
End Sub
Private Sub CheckFlightPeriod(LineNo As Long, ErrBuff As String)
    Dim RetVal As Integer
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpFrqd As String
    Dim tmpFrqw As String
    Dim tmpFday As String
    Dim tmpStoa As String
    Dim tmpStod As String
    Dim tmpDaof As String
    '"TRID,LTID,FLAG,SEAS,HOPO,FLCA,FLTA,FLCD,FLTD,VPFR,VPTO,FREQ,"
    '"NOSE,ACT3,ACT5,ORG3,VSA3,ORG4,VSA4,STOA,STOD,DAOF,VSD3,DES3,VSD4,DES4,"
    '"NATA,NATD,FREW"
    tmpVpfr = Trim(TlxImpData(1).GetFieldValue(LineNo, "VPFR"))
    tmpVpto = Trim(TlxImpData(1).GetFieldValue(LineNo, "VPTO"))
    tmpFrqd = Trim(TlxImpData(1).GetFieldValue(LineNo, "FREQ"))
    tmpFrqw = Trim(TlxImpData(1).GetFieldValue(LineNo, "FREW"))
    RetVal = Validation.CheckPeriodData(tmpVpfr, tmpVpto, tmpFrqd, tmpFrqw, tmpFday, False)
    If RetVal <> 0 Then
        Select Case RetVal
            Case -1
                ErrBuff = ErrBuff & "FREQ" & ","
            Case -2
                ErrBuff = ErrBuff & "VPFR" & ","
                ErrBuff = ErrBuff & "VPTO" & ","
            Case -3
                ErrBuff = ErrBuff & "FREQ" & ","
            Case -4
                ErrBuff = ErrBuff & "FREQ" & ","
            Case -5
                ErrBuff = ErrBuff & "VPFR" & ","
            Case -6
                ErrBuff = ErrBuff & "VPTO" & ","
            Case Else
                ErrBuff = ErrBuff & "VPFR" & ","
                ErrBuff = ErrBuff & "VPTO" & ","
                ErrBuff = ErrBuff & "FREQ" & ","
        End Select
    End If
    tmpStoa = Trim(TlxImpData(1).GetFieldValue(LineNo, "STOA"))
    'PHYOE
    If (tmpStoa <> "") And (CheckValidTime(tmpStoa, "2400") = False) Then ErrBuff = ErrBuff & "STOA" & ","
    tmpStod = Trim(TlxImpData(1).GetFieldValue(LineNo, "STOD"))
    If (tmpStod <> "") And (CheckValidTime(tmpStod, "2400") = False) Then ErrBuff = ErrBuff & "STOD" & ","
    
End Sub
Private Sub CheckBasicValues(Index As Integer, LineNo As Long, ImpFld1 As String, BasFld1 As String, ImpFld2 As String, BasFld2 As String, ErrBuff As String)
    Dim IsOk As Boolean
    Dim tmpLook1 As String
    Dim tmpLook2 As String
    Dim tmpData As String
    tmpLook1 = Trim(TlxImpData(1).GetFieldValue(LineNo, ImpFld1))
    tmpLook2 = Trim(TlxImpData(1).GetFieldValue(LineNo, ImpFld2))
    If Index = 1 Then
        If AutoPatchHomeApc <> "" Then
            If tmpLook1 = HomeAirport Then
                tmpLook1 = GetItem(AutoPatchHomeApc, 1, ",")
                tmpLook2 = GetItem(AutoPatchHomeApc, 2, ",")
            ElseIf tmpLook2 = HomeAirport Then
                tmpLook2 = GetItem(AutoPatchHomeApc, 1, ",")
                tmpLook1 = GetItem(AutoPatchHomeApc, 2, ",")
            End If
        End If
    End If
    If ImpFld1 <> ImpFld2 Then
        If (tmpLook1 = "") Or (tmpLook2 = "") Then
            IsOk = Validation.BasicCheck(Index, BasFld1, tmpLook1, BasFld2, tmpLook2)
        Else
            tmpData = ""
            IsOk = Validation.BasicCheck(Index, BasFld1, tmpLook1, BasFld2, tmpData)
            If Not IsOk Then ErrBuff = ErrBuff & ImpFld1 & ","
            tmpData = ""
            IsOk = Validation.BasicCheck(Index, BasFld1, tmpData, BasFld2, tmpLook2)
            If Not IsOk Then ErrBuff = ErrBuff & ImpFld2 & ","
            IsOk = True
        End If
    Else
        tmpLook2 = ""
        IsOk = Validation.BasicCheck(Index, BasFld1, tmpLook1, BasFld2, tmpLook2)
        If Not IsOk Then IsOk = Validation.BasicCheck(Index, BasFld2, tmpLook1, BasFld1, tmpLook2)
    End If
    
    If IsOk Then
        If ImpFld1 <> ImpFld2 Then
            If ImpFld1 <> "" Then TlxImpData(1).SetFieldValues LineNo, ImpFld1, tmpLook1
            If ImpFld2 <> "" Then TlxImpData(1).SetFieldValues LineNo, ImpFld2, tmpLook2
        End If
    Else
        If (ImpFld1 <> "") And (tmpLook1 <> "") Then ErrBuff = ErrBuff & ImpFld1 & ","
        If (ImpFld2 <> "") And (tmpLook2 <> "") Then ErrBuff = ErrBuff & ImpFld2 & ","
    End If
End Sub
Private Sub SetErrorFlags(LineNo As Long, FieldList As String)
    Dim tmpFldNam As String
    Dim TabFields As String
    Dim CurFld As Long
    Dim ColNo As Long
    TabFields = TlxImpData(1).LogicalFieldList
    CurFld = 0
    tmpFldNam = GetRealItem(FieldList, CurFld, ",")
    While tmpFldNam <> ""
        ColNo = CLng(GetRealItemNo(TabFields, tmpFldNam))
        TlxImpData(1).SetDecorationObject LineNo, ColNo, "CellError"
        CurFld = CurFld + 1
        tmpFldNam = GetRealItem(FieldList, CurFld, ",")
    Wend
    TlxImpData(1).SetLineColor LineNo, vbWhite, vbRed
    TlxImpData(1).RedrawTab
End Sub

Private Sub chkSrvImp_Click(Index As Integer)
    Dim UseSelList As String
    Dim ActionType As String
    If chkSrvImp(Index).Value = 1 Then
        If DataSystemType <> "OAFOS" Then
            ActionType = DefineColorAction(SrvFkeyCnt(Index).BackColor)
            If (RotationMode) And (ActionType = "MIXED") Then ActionType = "JOIN"
            If (RotationMode) And (ActionType <> "JOIN") Then ActionType = ""
            If ActionType <> "" Then
                UseSelList = NoopList(2).GetLinesByBackColor(SrvFkeyCnt(Index).BackColor)
                CallServerAction ActionType, Val(SrvFkeyCnt(Index).Caption), True, UseSelList
            End If
        End If
        chkSrvImp(Index).Value = 0
    End If
End Sub

Private Sub chkUpdMode_Click()
    If chkUpdMode.Value = 1 Then
        chkUpdMode.BackColor = LightGreen
        chkFullMode.Value = 0
        MyFullMode = False
        MyUpdateMode = True
        chkWork(2).Value = 0
    Else
        chkUpdMode.BackColor = vbButtonFace
        chkFullMode.Value = 1
    End If
End Sub

Private Sub Command1_Click()
    Dim CurName As String
    'CurName = "C:\Ufis\" & DataSystemType & "NoopClient.txt"
    CurName = UFIS_UFIS & DataSystemType & "NoopClient.txt"
    NoopList(0).WriteToFile CurName, False
    'CurName = "C:\Ufis\" & DataSystemType & "NoopServer.txt"
    CurName = UFIS_UFIS & DataSystemType & "NoopServer.txt"
    NoopList(1).WriteToFile CurName, False
    NoopList(0).ResetContent
    NoopList(1).ResetContent
End Sub

Private Sub Command2_Click()
    Dim CurName As String
    'CurName = "C:\Ufis\" & DataSystemType & "NoopClient.txt"
    CurName = UFIS_UFIS & DataSystemType & "NoopClient.txt"
    NoopList(0).ResetContent
    NoopList(0).ReadFromFile CurName
    NoopList(1).ResetContent
    'CurName = "C:\Ufis\" & DataSystemType & "NoopServer.txt"
    CurName = UFIS_UFIS & DataSystemType & "NoopServer.txt"
    NoopList(1).ReadFromFile CurName
    ClearOutEmptyLines 0, FlnoCol, True
    ClearOutEmptyLines 1, FlnoCol, True
    CompareFlightLists FkeyCol, FkeyCol
End Sub

Private Sub Command4_Click()
    'TlxImpData(0).WriteToFile "c:\tmp\TabData.txt", False
    TlxImpData(0).WriteToFile UFIS_TMP & "\TabData.txt", False
End Sub

Private Sub Command5_Click()
    'TlxImpData(0).ReadFromFile "c:\tmp\TabData.txt"
    TlxImpData(0).ReadFromFile UFIS_TMP & "\TabData.txt"
    TlxImpData(0).AutoSizeColumns
    TlxImpData(0).Refresh
    If chkSpool(3).Value = 1 Then
        chkSpool(0).Value = 1
        chkWork(2).Value = 0
        chkSpool(1).Value = 1
    End If
End Sub

Private Sub CurPos_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim ActionType As String
    Dim MsgTxt As String
    If Index = 4 Then
        If Button = 2 Then
            ActionType = DefineColorAction(CurPos(Index).BackColor)
            If (RotationMode) And (ActionType = "MIXED") Then ActionType = "JOIN"
            If (RotationMode) And (ActionType <> "JOIN") Then ActionType = ""
            If ActionType <> "" Then CallServerAction ActionType, Val(CurPos(Index).Caption), True, ""
        Else
            If Val(CurPos(Index).Caption) > 0 Then JumpToColoredLine CurPos(Index).BackColor, 1, -1
        End If
    ElseIf Index = 2 Then
        If Button = 2 Then
            ActionType = NoopList(2).GetLinesByBackColor(vbBlack)
            ActionType = Str(ItemCount(ActionType, ","))
            MsgTxt = ""
            MsgTxt = MsgTxt & "The left list (import flights) contains" & ActionType & " flights" & vbNewLine
            MsgTxt = MsgTxt & "prior to the import time limit " & ServerTime.Text & " !"
            If MyMsgBox.CallAskUser(0, 0, 0, "Import Time Limit Control", MsgTxt, "infomsg", "", UserAnswer) > 0 Then DoNothing
        Else
            JumpToColoredLine vbBlack, -1, -1
        End If
    End If
End Sub

Private Sub FlnoTabCombo_ComboSelChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal OldValue As String, ByVal NewValues As String)
    Dim NewFlno As String
    Dim CurAlc As String
    NewFlno = GetItem(NewValues, 1, ",")
    If (Not FormIsBusy) And (NewFlno <> CurPos(1).Caption) Then
        CurPos(1).Caption = NewFlno
        If NewFlno <> "-ALL-" Then
            AdjustScrollMaster FlnoCol, 9, NewFlno, False
        Else
            CurAlc = CurPos(0).Caption
            CurPos(0).Caption = ""
            AlcTabCombo_ComboSelChanged 0, 0, 0, "***", CurAlc
        End If
    End If

End Sub

Private Sub Form_Activate()
    If (Not NoopCheckDoubles) And (Not AlreadyActive) Then
        If InStr(Command, "NOLINK") = 0 Then
            CheckInfoServer
        End If
        If UpdateFtypOnDelete Then
            SrvFkeyCnt(4).ToolTipText = "Set Flight Status NOOP='" & FtypOnDelete & "' (In AODB but not in File)"
            SrvFkeyCnt(11).ToolTipText = "Set Flight Status NOOP='" & FtypOnDelete & "' (In AODB but not in File)"
        End If
        If MyMainFullMode = True Then chkFullMode.Value = 1
        If (MyMainUpdateMode = True) Or (MyMainFullMode = False) Then chkUpdMode.Value = 1
        If AutoUpdateMode Then chkUpdMode.Value = 1
        If StartedAsImportTool Then chkWork(2).Value = 1
        AlreadyActive = True
        If StartedAsCompareTool Then
            'Note:
            'SCORE is the only import type, which is using this feature (called from TelexPool)
            DateFieldFormat = GetIniEntry(myIniFullName, "SCORE", "", "DATE_FORMAT", "")
            DefaultFileHeader = GetIniEntry(myIniFullName, "SCORE", "", "ADD_FILE_HEADER", "")
        End If
    End If
End Sub

Private Sub Form_LinkOpen(Cancel As Integer)
    InfoSpooler.ResetContent
    InfoSpooler.HeaderString = "Message"
    InfoSpooler.HeaderLengthString = "1000"
    InfoSpooler.SetFieldSeparator Chr(16)
    'InfoSpooler.Visible = True
    'InfoSpooler.ZOrder
    If MsgFromServer.LinkMode = 0 Then
        MsgFromServer.LinkTopic = "TelexPool|InfoServer"
        MsgFromServer.LinkItem = "MsgToClient"
        MsgFromServer.LinkMode = vbLinkAutomatic
        TelexIsConnected = True
    End If
End Sub

Private Sub Form_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim tmpFile As String
    Dim RetVal As Integer
    Dim nbr As Integer
    Dim tmpLine As String
    Dim tmpRec As String
    Dim LineNo As Long
    
    On Error GoTo ErrHdl
    If StartedAsCompareTool Then
        If (Effect And vbDropEffectCopy) = vbDropEffectCopy Then
            If Data.Files.Count > 0 Then
                tmpFile = Data.Files(1)
                RetVal = 1
                'If chkExpand.Value = 1 Then
                    tmpRec = "Do you want to import the current file?"
                    tmpRec = tmpRec & vbNewLine & tmpFile
                    Me.SetFocus
                    RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Import File Control", tmpRec, "export", "Yes,No", UserAnswer)
                'End If
                If RetVal = 1 Then
                    TelexPoolFile = tmpFile
                    chkSpool(8).Value = 1
'                    chkSpool(0).Value = 1
'                    TlxImpData(0).ResetContent
'                    TlxImpData(0).SetUniqueFields "2"
'                    TlxImpData(1).ResetContent
'                    TlxImpData(1).SetUniqueFields "0"
'                    nbr = FreeFile
'                    Open tmpFile For Input As #nbr
'                    While Not EOF(nbr)
'                        LineNo = LineNo + 1
'                        tmpRec = ""
'                        Line Input #nbr, tmpLine
'                        tmpRec = tmpRec & CStr(LineNo) & ","
'                        tmpRec = tmpRec & CStr(LineNo) & ","
'                        tmpRec = tmpRec & Left(tmpLine, 8) & ","
'                        tmpRec = tmpRec & tmpLine
'                        tmpRec = Replace(tmpRec, ",", Chr(15), 1, -1, vbBinaryCompare)
'                        TlxImpData(0).InsertTextLine tmpRec, False
'                    Wend
'                    Close nbr
'                    TlxImpData(0).AutoSizeColumns
'                    TlxImpData(0).Refresh
'                    chkSpool(1).Value = 1
                End If
            End If
        End If
    End If
ErrHdl:
End Sub

Private Sub Form_Load()
    LastMsgNum = -1
    Load frmUfrFields
    Splitter(2).Left = 30
    Splitter(3).Left = 30
    If StartedAsCompareTool Then
        DataSystemType = "ONLINE"
        Splitter(3).Visible = True
        Splitter(3).MousePointer = 7
        Splitter(2).MousePointer = 7
        Splitter(3).Height = 180
        SpoolerPanel.Top = chkWork(0).Top + chkWork(0).Height + 15
        SpoolerPanel.Left = 30
        SpoolerPanel.Visible = True
        TlxImpData(0).ResetContent
        TlxImpData(0).Left = 30
        TlxImpData(0).Top = SpoolerPanel.Top + SpoolerPanel.Height + 30
        TlxImpData(0).Height = 3000
        TlxImpData(0).Visible = True
        TlxImpData(1).Left = TlxImpData(0).Left
        TlxImpData(1).Top = TlxImpData(0).Top
        TlxImpData(1).Height = TlxImpData(0).Height
        Splitter(3).Top = TlxImpData(0).Top + TlxImpData(0).Height + 30
        TopLabels.Top = Splitter(3).Top + 90
        TopLabels.Left = Splitter(3).Left
        Splitter(2).Top = TopLabels.Top + TopLabels.Height - 15
        DspShrink.Top = -30
        chkSpool(0).Value = 1
        InitImportTabs
        ImportAct3Act5 = True
    Else
        Line1(0).Visible = False
        Line1(1).Visible = False
        Line1(2).Visible = False
        SpoolerPanel.Visible = False
        Splitter(2).MousePointer = 0
        TlxImpData(0).Visible = False
        TlxImpData(1).Visible = False
        Splitter(3).Visible = False
        Splitter(3).Top = chkWork(0).Top + chkWork(0).Height + 60
        TopLabels.Top = Splitter(3).Top
        Splitter(2).Top = TopLabels.Top + TopLabels.Height + 30
        DspShrink.Top = -30
    End If
    Splitter(0).Top = Splitter(2).Top
    Splitter(1).Top = Splitter(2).Top
    NoopList(0).Top = Splitter(2).Top + Splitter(2).Height + 15
    NoopList(1).Top = NoopList(0).Top
    NoopList(2).Top = NoopList(0).Top
    CliFkeyCnt(7).BackColor = vbButtonFace
    CliFkeyCnt(7).ForeColor = vbButtonText
    If RotationMode Then
        CliFkeyCnt(2).ToolTipText = "Join despite differences"
        CliFkeyCnt(9).ToolTipText = "Join despite differences"
    Else
        CliFkeyCnt(2).ToolTipText = "Join with update or insert"
        CliFkeyCnt(9).ToolTipText = "Join with update or insert"
    End If
    MyFullMode = NoopFullMode
    MyUpdateMode = Not NoopFullMode
    MyMainFullMode = NoopFullMode
    MyMainUpdateMode = Not NoopFullMode
    
    If MyFullMode Then
        If NoopCheckDoubles Then
            Me.Caption = Me.Caption & " (For Check Doubles) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
        Else
            Me.Caption = Me.Caption & " (For Full Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
        End If
    Else
        If RotationMode Then
            Me.Caption = Me.Caption & " (For Link Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
        Else
            Me.Caption = Me.Caption & " (For Update Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
        End If
    End If
    InitImpButtons
    InitLists
    InitShrinkDsp
    If MyUpdateMode Then
        chkWork(5).Enabled = False
    End If
    If DataSystemType <> "OAFOS" Then
        FlnoTabCombo(0).Visible = True
        RegnTabCombo(0).Visible = False
    Else
        FlnoTabCombo(0).Visible = False
        RegnTabCombo(0).Visible = True
    End If
    If Not NoopCheckDoubles Then
        ShowSecondList True, "2,0"
    Else
        ShowSecondList False, "0"
    End If
    MySetUp.NoopListIsOpen = True
    MySetUp.SynchronizeTimeDisplays
    SelRegnList = ""
    LimitColor = NormalYellow
    SearchInList -1, False, False
    TopLabels.ZOrder
    StatusBar.ZOrder
    
    If StartedAsCompareTool Then
        Me.Height = 600 * 15
        Me.Width = 800 * 15
    End If
End Sub
Private Sub InitImpButtons()
    Dim i As Integer
    For i = 0 To CliFkeyCnt.UBound
        If i > 0 Then Load chkCliImp(i)
        Set chkCliImp(i).Container = BottomLabels(0)
        chkCliImp(i).BackColor = CliFkeyCnt(i).BackColor
        chkCliImp(i).ForeColor = CliFkeyCnt(i).ForeColor
        chkCliImp(i).Caption = CliFkeyCnt(i).Caption
        chkCliImp(i).ToolTipText = CliFkeyCnt(i).ToolTipText
        chkCliImp(i).Visible = False
    Next
    For i = 0 To SrvFkeyCnt.UBound
        If i > 0 Then Load chkSrvImp(i)
        Set chkSrvImp(i).Container = BottomLabels(1)
        chkSrvImp(i).BackColor = SrvFkeyCnt(i).BackColor
        chkSrvImp(i).ForeColor = SrvFkeyCnt(i).ForeColor
        chkSrvImp(i).Caption = SrvFkeyCnt(i).Caption
        chkSrvImp(i).ToolTipText = SrvFkeyCnt(i).ToolTipText
        chkSrvImp(i).Visible = False
    Next
End Sub
Private Sub AdjustImpButtons()
    Dim i As Integer
    For i = 0 To CliFkeyCnt.UBound
        chkCliImp(i).Top = CliFkeyCnt(i).Top
        chkCliImp(i).Left = CliFkeyCnt(i).Left
        chkCliImp(i).Height = CliFkeyCnt(i).Height
        chkCliImp(i).Width = CliFkeyCnt(i).Width
        chkCliImp(i).Caption = Trim(CliFkeyCnt(i).Caption)
        If (Val(chkCliImp(i).Caption) = 0) Or (chkWork(6).Value = 0) Then
            CliFkeyCnt(i).Visible = True
            chkCliImp(i).Visible = False
        ElseIf chkWork(6).Value = 1 Then
            If ((i > 0) And (i < 6)) Or ((i > 7) And (i < 13)) Then
                chkCliImp(i).Visible = True
                CliFkeyCnt(i).Visible = False
            End If
        End If
    Next
    For i = 0 To SrvFkeyCnt.UBound
        chkSrvImp(i).Top = SrvFkeyCnt(i).Top
        chkSrvImp(i).Left = SrvFkeyCnt(i).Left
        chkSrvImp(i).Height = SrvFkeyCnt(i).Height
        chkSrvImp(i).Width = SrvFkeyCnt(i).Width
        chkSrvImp(i).Caption = Trim(SrvFkeyCnt(i).Caption)
        If (Val(chkSrvImp(i).Caption) = 0) Or (chkWork(6).Value = 0) Then
            SrvFkeyCnt(i).Visible = True
            chkSrvImp(i).Visible = False
        ElseIf chkWork(6).Value = 1 Then
            If ((i > 0) And (i < 6)) Or ((i > 7) And (i < 13)) Then
                chkSrvImp(i).Visible = True
                SrvFkeyCnt(i).Visible = False
            End If
        End If
    Next
End Sub
Private Sub InitShrinkDsp()
    Dim i As Integer
    For i = 1 To 6
        lblShrink(i).BackColor = CliFkeyCnt(i).BackColor
        lblShrinkCover(i).Top = lblShrink(i).Top
    Next
    For i = 8 To 13
        lblShrink(i).BackColor = SrvFkeyCnt(i).BackColor
        lblShrinkCover(i).Top = lblShrink(i).Top
    Next
End Sub
Private Sub AdjustShrinkLabels()
    Dim i As Integer
    Dim ColorCount As Long
    For i = 1 To 6
        ColorCount = Val(CliFkeyCnt(i).Caption)
        If ColorCount > 0 Then
            lblShrink(i).BorderStyle = 1
            lblShrink(i).ZOrder
        Else
            lblShrink(i).BorderStyle = 0
            lblShrinkCover(i).ZOrder
        End If
    Next
    For i = 11 To 12
        ColorCount = Val(SrvFkeyCnt(i - 7).Caption)
        If ColorCount > 0 Then
            lblShrink(i).BorderStyle = 1
            lblShrink(i).ZOrder
        Else
            lblShrink(i).BorderStyle = 0
            lblShrinkCover(i).ZOrder
        End If
    Next
    AdjustShrinkButton False
End Sub
Private Sub AdjustShrinkButton(ShowDiff As Boolean)
    Dim i As Integer
    Dim Result As String
    Result = ""
    For i = 1 To 6
        Result = Result & Trim(Str(lblShrink(i).BorderStyle)) & ","
    Next
    For i = 11 To 12
        Result = Result & Trim(Str(lblShrink(i).BorderStyle)) & ","
    Next
    If (ShowDiff) And (Result <> chkShrink(0).Tag) Then
        chkShrink(0).BackColor = vbRed
        chkShrink(0).ForeColor = vbWhite
    Else
        If Not ShowDiff Then chkShrink(0).Tag = Result
        chkShrink(0).BackColor = vbButtonFace
        chkShrink(0).ForeColor = vbBlack
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    MySetUp.NoopListIsOpen = False
    NoopCheckDoubles = False
    AlreadyActive = False
    If StartedAsCompareTool Then End
End Sub

Private Sub InfoFromServer_Change()
    Dim FileName As String
    Dim tmpData As String
    Dim ImpMsgNum As Long
    Dim ImpPakNum As Long
    Dim tmpVal As Integer
    Dim ImpCmd As String
    Dim FldSep As String
    Dim LineNo As Long
    ShowStopButton = False
    ShrinkNewDel = False
    UseFkeyTime = False
    tmpData = InfoFromServer.Text
    If tmpData <> "" Then
        FldSep = Chr(15)
        ImpMsgNum = Val(GetItem(tmpData, 1, FldSep))
        ImpPakNum = Val(GetItem(tmpData, 2, FldSep))
        If ImpPakNum > 0 Then
            TlxImpData(0).InsertTextLine tmpData, False
            TlxImpData(0).AutoSizeColumns
            LineNo = TlxImpData(0).GetLineCount
            TlxImpData(0).OnVScrollTo LineNo
            TlxImpData(0).Refresh
            InfoFromServer.Text = ""
            InfoToServer.Text = "GOT:" & GetItem(tmpData, 1, FldSep)
        Else
            ImpCmd = GetItem(tmpData, 3, FldSep)
            If Left(ImpCmd, 4) = "NAME" Then
                GetKeyItem FileName, ImpCmd, "NAME[", "]"
                ImpCmd = "NAME"
            End If
            Select Case ImpCmd
                Case "FILE"
                    Text1.Text = "GOT MESSAGE [FILE] FROM TELEXPOOL"
                    'Text1.ZOrder
                    chkSpool(8).Value = 1
                Case "NAME"
                    Text1.Text = "[" & FileName & "]"
                    TelexPoolFile = FileName
                Case "INIT"
                    Me.Left = Val(GetItem(tmpData, 4, FldSep))
                    Me.Top = Val(GetItem(tmpData, 5, FldSep))
                    tmpVal = Val(GetItem(tmpData, 6, FldSep))
                    If tmpVal = 1 Then SetFormOnTop Me, True
                Case "PCK.BGN"
                    chkSpool(0).Value = 1
                Case "SPOOL"
                    chkSpool(0).Value = 1
                    SpoolImpData = True
                Case "ACTIV"
                    chkSpool(0).Value = 0
                    SpoolImpData = False
                    If TlxImpData(0).GetLineCount > 0 Then
                        chkSpool(1).Value = 1
                    End If
                Case "CLOSE"
                    chkWork(0).Value = 1
                Case "ATSET"
                    chkSpool(2).Value = 1
                Case "ATOFF"
                    chkSpool(2).Value = 0
                Case "AISET"
                    chkSpool(3).Value = 1
                Case "AIOFF"
                    chkSpool(3).Value = 0
                Case "PCK.END"
                    If Not SpoolImpData Then
                        'chkSpool(1).Value = 1
                    End If
                Case Else
            End Select
            InfoToServer.Text = "GOT:" & GetItem(tmpData, 1, FldSep)
        End If
        LastMsgNum = ImpMsgNum + 1
    End If
End Sub

Private Sub lblShrink_Click(Index As Integer)
    Dim CurIdx As Integer
    CurIdx = Index
    If lblShrink(CurIdx).BorderStyle = 0 Then
        lblShrink(CurIdx).BorderStyle = 1
        lblShrink(CurIdx).ZOrder
    Else
        lblShrink(CurIdx).BorderStyle = 0
        lblShrinkCover(CurIdx).ZOrder
    End If
    AdjustShrinkButton True
End Sub
Private Sub InitLists()
    Dim tmpTest As String
    Dim myHeaderList As String
    Dim HdrLenLst As String
    Dim ActColLst As String
    Dim ColAlign As String
    Dim i As Integer
    Dim j As Integer
    Dim tmpHeader As String
    Dim tmpHedLen As String
    Dim tmpLen As Integer
    Dim tmpDat As String
    Dim TotalSize As Long
    Dim NewTop As Long
    Dim NewHeight As Long
    Dim tmpSize As Long
    Dim tmpVal As Long
    Dim tmpFkeyCol As Integer
    Dim tmpColor As String
    Dim tmpAddon As String
    
    PrflCol = 0
    FtypCol = 1
    FlnoCol = 2
    AdidCol = 3
    TimeCol = 4
    DoopCol = 5
    Act3Col = 6
    Apc3Col = 7
    Via3Col = 8
    StypCol = 9
    RegnCol = 10
    FldaCol = 11
    SkedCol = 12
    RoutCol = 13
    FltnCol = 14
    TifdCol = 15
    RtypCol = 16
    FkeyCol = 17
    RkeyCol = 18
    UrnoCol = 19
    RotaCol = 20
    ChckCol = 21
    DoneCol = 22
    Act5Col = 23
    Apc4Col = 24
    Via4Col = 25
    RemaCol = 26
    ApdxCol = 27
    
    MyCompFields = "1,4,5,6,7,8,9,10,11,12,13,27"
    ImpEmptyData = "N,N,N,N,N,Y,N,N,N,N,Y,Y"
    EmptyLineColor = NormalGray
    
    IsMultiSel = 1
    CmdIfrDone = 2
    CmdUfrDone = 4
    CmdDfrDone = 8
    CmdSprDone = 16
    CmdJofDone = 32
    CmdSplDone = 64
    IsImported = 128
    
    RotaSelFields = ""
    RotaSelFields = RotaSelFields & Str(FlnoCol) & ","
    RotaSelFields = RotaSelFields & Str(TimeCol) & ","
    RotaSelFields = RotaSelFields & Str(AdidCol) & ","
    RotaSelFields = RotaSelFields & Str(DoopCol)
    
    tmpFkeyCol = CInt(RegnCol + 2)
    
    tmpColor = Str(vbBlack) & "," & Str(LightGray)
    tmpHeader = "A,S, Flights,I,Time,D,ACT,AIRP,VIAS,NAT,REGN,FS.Date,STOA or STOD,Itinerary (Route),Linked, Date   Time,I,  FKEY,RKEY,URNO,ROTAKEY,C,D,ACT5,APC4,VIA4,RegnKey,"
    If UseStypField = "STYP" Then tmpHeader = Replace(tmpHeader, ",NAT,", ",SRV,", 1, -1, vbBinaryCompare)
    ActColLst = "2,1,9,1,5,1,3,3,3,3,6,8,12,100,9,12,1,18,12,10,22,1,1,5,4,4,8"
    If ImportAct3Act5 Then
        tmpHeader = Replace(tmpHeader, ",ACT,", ",ACT/ICAO,", 1, -1, vbBinaryCompare)
        ActColLst = "2,1,9,1,5,1,9,4,4,3,6,8,12,100,9,12,1,18,12,10,22,1,1,5,4,4,8"
    End If
    MainHeadCols = "11,3,6,6,20"
    ColAlign = ""
    HdrLenLst = ""
    TotalSize = 0
    i = 0
    Do
        i = i + 1
        tmpDat = GetItem(ActColLst, i, ",")
        If tmpDat <> "" Then
            tmpLen = Val(tmpDat)
            If tmpLen > 0 Then
                tmpSize = (TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX) + 7
            Else
                tmpSize = 0
            End If
            HdrLenLst = HdrLenLst & Str(tmpSize) & ","
            If i < tmpFkeyCol Then TotalSize = TotalSize + tmpSize
            ColAlign = ColAlign & "L,"
        End If
    Loop While tmpDat <> ""
    
    tmpHeader = tmpHeader & "ApdxData,"
    HdrLenLst = HdrLenLst & "100,"
    ColAlign = ColAlign & "L,"
    ActColLst = ActColLst & "10,"
    
    tmpHeader = tmpHeader & "Remark"
    HdrLenLst = HdrLenLst & "1000"
    ColAlign = ColAlign & "L"
    ActColLst = ActColLst & "40"
    
    AddArrFldCnt = ItemCount(AddOnArrAftFld, ",")
    AddDepFldCnt = ItemCount(AddOnDepAftFld, ",")
    For i = 1 To AddArrFldCnt
        tmpAddon = "X" & Right("000" & CStr(i), 3)
        tmpHeader = tmpHeader & "," & tmpAddon
        HdrLenLst = HdrLenLst & ",50"
        ColAlign = ColAlign & ",L"
        ActColLst = ActColLst & ",10"
    Next
    
    
    
    
    For i = 0 To 2
        NoopList(i).ResetContent
        NoopList(i).ShowVertScroller True
        NoopList(i).ShowHorzScroller True
        NoopList(i).VScrollMaster = False
        NoopList(i).FontName = "Courier New"
        NoopList(i).SetMainHeaderFont 17, False, False, True, 0, "Courier New"
        NoopList(i).SetTabFontBold True
        NoopList(i).HeaderFontSize = 17
        NoopList(i).FontSize = 17
        NoopList(i).LineHeight = 17
        NoopList(i).LeftTextOffset = 2
        NoopList(i).GridLineColor = DarkGray
        'If i = 0 Then
        '    NoopList(i).HeaderLengthString = HdrLenLst & ",100,"
        '    NoopList(i).HeaderString = tmpHeader & ",ApdxData,"
        '    NoopList(i).ColumnAlignmentString = ColAlign & ",L,"
        '    NoopList(i).ColumnWidthString = ActColLst & ",20,"
        'Else
            NoopList(i).HeaderLengthString = HdrLenLst
            NoopList(i).HeaderString = tmpHeader
            NoopList(i).ColumnAlignmentString = ColAlign
            NoopList(i).ColumnWidthString = ActColLst
        'End If
        NoopList(i).Height = 24 * NoopList(i).LineHeight * Screen.TwipsPerPixelY
        NoopList(i).CreateDecorationObject "CellDiff", "L,R,T,B,L", "-1,2,2,2,2", CStr(LightBlue) & "," & tmpColor & "," & tmpColor
        NoopList(i).CreateDecorationObject "SrvRota", "BTR", "8", Trim(Str(LightRed))
        NoopList(i).CreateDecorationObject "CliRota", "BTL", "8", Trim(Str(LightRed))
        NoopList(i).CreateDecorationObject "RotaDiff", "L,R,T,B,L", "-1,2,2,2,2", Trim(Str(LightRed)) & "," & tmpColor & "," & tmpColor
        NoopList(i).CreateDecorationObject "RotaJoin", "L", "-1", Trim(Str(LightBlue))
        NoopList(i).CreateDecorationObject "RotaDeny", "L", "-1", Trim(Str(LightRed))
        NoopList(i).CreateDecorationObject "SplitArr", "TL", "16", Trim(Str(LightRed))
        NoopList(i).CreateDecorationObject "SplitDep", "BR", "16", Trim(Str(LightRed))
        NoopList(i).CreateDecorationObject "IsSplitArr", "TL", "16", Trim(Str(LightGreen))
        NoopList(i).CreateDecorationObject "IsSplitDep", "BR", "16", Trim(Str(LightGreen))
        NoopList(i).CreateCellObj "MultiSel", vbBlue, vbWhite, 17, False, False, True, 0, "Courier New"
        BottomLabels(i).Top = NoopList(i).Top + NoopList(i).Height - 7 * Screen.TwipsPerPixelY
        If i < 2 Then
            NoopList(i).DateTimeSetColumn TimeCol
            NoopList(i).DateTimeSetInputFormatString TimeCol, "YYYYMMDDhhmm"
            NoopList(i).DateTimeSetOutputFormatString TimeCol, "hh':'mm"
            NoopList(i).DateTimeSetColumn TifdCol
            NoopList(i).DateTimeSetInputFormatString TifdCol, "YYYYMMDDhhmm"
            NoopList(i).DateTimeSetOutputFormatString TifdCol, "DDMMMYY'/'hhmm"
            NoopList(i).DateTimeSetColumn SkedCol
            NoopList(i).DateTimeSetInputFormatString SkedCol, "YYYYMMDDhhmm"
            NoopList(i).DateTimeSetOutputFormatString SkedCol, "DDMMMYY'/'hhmm"
        End If
        NoopList(i).LifeStyle = True
    Next
    For i = 0 To 3
        RotaLink(i).ResetContent
        RotaLink(i).ShowVertScroller False
        RotaLink(i).ShowHorzScroller False
        RotaLink(i).FontName = "Courier New"
        RotaLink(i).SetMainHeaderFont 17, False, False, True, 0, "Courier New"
        RotaLink(i).SetTabFontBold True
        RotaLink(i).HeaderFontSize = 17
        RotaLink(i).FontSize = 17
        RotaLink(i).LineHeight = 17
        RotaLink(i).LeftTextOffset = 2
        RotaLink(i).GridLineColor = DarkGray
        RotaLink(i).HeaderLengthString = HdrLenLst
        RotaLink(i).HeaderString = tmpHeader
        RotaLink(i).ColumnAlignmentString = ColAlign
        RotaLink(i).ColumnWidthString = ActColLst
        RotaLink(i).Height = 3 * RotaLink(i).LineHeight * Screen.TwipsPerPixelY
        RotaLink(i).CreateDecorationObject "SrvRota", "BTR", "8", Trim(Str(LightRed))
        RotaLink(i).CreateDecorationObject "CliRota", "BTL", "8", Trim(Str(LightRed))
        RotaLink(i).CreateDecorationObject "CellDiff", "L,R,T,B,L", "-1,2,2,2,2", CStr(LightBlue) & "," & tmpColor & "," & tmpColor
        RotaLink(i).CreateDecorationObject "RotaDiff", "L,R,T,B,L", "-1,2,2,2,2", Trim(Str(LightRed)) & "," & tmpColor & "," & tmpColor
        RotaLink(i).CreateDecorationObject "RotaDeny", "L", "-1", Trim(Str(LightRed))
        RotaLink(i).CreateDecorationObject "RotaJoin", "L", "-1", Trim(Str(LightBlue))
        RotaLink(i).CreateDecorationObject "SplitArr", "TL", "16", Trim(Str(LightRed))
        RotaLink(i).CreateDecorationObject "SplitDep", "BR", "16", Trim(Str(LightRed))
        RotaLink(i).CreateDecorationObject "IsSplitArr", "TL", "16", Trim(Str(LightGreen))
        RotaLink(i).CreateDecorationObject "IsSplitDep", "BR", "16", Trim(Str(LightGreen))
        RotaLink(i).MainHeaderOnly = True
        RotaLink(i).DateTimeSetColumn TimeCol
        RotaLink(i).DateTimeSetInputFormatString TimeCol, "YYYYMMDDhhmm"
        RotaLink(i).DateTimeSetOutputFormatString TimeCol, "hh':'mm"
        RotaLink(i).DateTimeSetColumn TifdCol
        RotaLink(i).DateTimeSetInputFormatString TifdCol, "YYYYMMDDhhmm"
        RotaLink(i).DateTimeSetOutputFormatString TifdCol, "DDMMMYY'/'hhmm"
        RotaLink(i).DateTimeSetColumn SkedCol
        RotaLink(i).DateTimeSetInputFormatString SkedCol, "YYYYMMDDhhmm"
        RotaLink(i).DateTimeSetOutputFormatString SkedCol, "DDMMMYY'/'hhmm"
        RotaLink(i).ShowRowSelection = False
        RotaLink(i).LifeStyle = True
    Next
    
    RotaLink(0).SetMainHeaderValues MainHeadCols, "Import File Rotation Data,Route Data,Linked Flight,System Data,Appendix", "12632256,12632256,12632256,12632256"
    RotaLink(0).MainHeader = True
    RotaLink(1).SetMainHeaderValues MainHeadCols, "UFIS AODB Rotation Data,Route Data,Linked Flight,System Data,Appendix", "12632256,12632256,12632256,12632256"
    RotaLink(1).MainHeader = True
    
    RotaLink(2).SetMainHeaderValues MainHeadCols, "Import File Flight Data,Route Data,Linked Flight,System Data,Appendix", "12632256,12632256,12632256,12632256"
    RotaLink(2).MainHeader = True
    RotaLink(3).SetMainHeaderValues MainHeadCols, "UFIS AODB Flight Data,Route Data,Linked Flight,System Data,Appendix", "12632256,12632256,12632256,12632256"
    RotaLink(3).MainHeader = True
    
    TotalSize = TotalSize + 16  'Plus VertScrollBar
    NoopList(0).Left = 30
    NoopList(0).SetMainHeaderValues MainHeadCols, "Import Flight Data,Route Data,Rotation Data,System Data,Appendix", "12632256,12632256,12632256,12632256"
    NoopList(0).MainHeader = True
    NoopList(2).Left = NoopList(0).Left + NoopList(0).Width + 15
    
    tmpSize = TextWidth("ABC1234 S") / Screen.TwipsPerPixelX + 6
    tmpHedLen = Trim(Str(tmpSize))
    tmpVal = TextWidth("31DEC02") / Screen.TwipsPerPixelX + 6
    tmpHedLen = tmpHedLen & "," & Trim(Str(tmpVal))
    tmpSize = tmpSize + tmpVal
    tmpVal = TextWidth("A") / Screen.TwipsPerPixelX + 6
    tmpHedLen = tmpHedLen & "," & Trim(Str(tmpVal))
    tmpSize = tmpSize + tmpVal
    tmpVal = TextWidth("1") / Screen.TwipsPerPixelX + 6
    tmpHedLen = tmpHedLen & "," & Trim(Str(tmpVal))
    tmpVal = TextWidth("12345678") / Screen.TwipsPerPixelX + 6
    tmpHedLen = tmpHedLen & "," & Trim(Str(tmpVal))
    NoopList(2).HeaderLengthString = tmpHedLen
    NoopList(2).HeaderString = " Flights,  Day,I,D,  LineNo"
    NoopList(2).ColumnAlignmentString = "L,L,L,L,R"
    NoopList(2).ColumnWidthString = "9,7,1,1,8"
    NoopList(2).SetMainHeaderValues "4,1", "MAIN SCROLL AREA,  ", "12632256,12632256"
    NoopList(2).MainHeader = True
    NoopList(2).Width = (tmpSize + 32) * Screen.TwipsPerPixelX
    NoopList(2).VScrollMaster = True
    NoopList(2).DateTimeSetColumn 1
    NoopList(2).DateTimeSetInputFormatString 1, "YYYYMMDD"
    NoopList(2).DateTimeSetOutputFormatString 1, "DDMMMYY"
    NoopList(2).ShowVertScroller True
    
    For i = 4 To 5
        RotaLink(i).ResetContent
        RotaLink(i).HeaderLengthString = tmpHedLen
        RotaLink(i).HeaderString = " Flights,  Day,I,D,  LineNo"
        RotaLink(i).ColumnAlignmentString = "L,L,L,L,R"
        RotaLink(i).ColumnWidthString = "9,7,1,1,8"
        RotaLink(i).ShowVertScroller False
        RotaLink(i).ShowHorzScroller False
        RotaLink(i).SetMainHeaderFont 17, False, False, True, 0, "Courier New"
        RotaLink(i).FontName = "Courier New"
        RotaLink(i).SetTabFontBold True
        RotaLink(i).HeaderFontSize = 17
        RotaLink(i).FontSize = 17
        RotaLink(i).LineHeight = 17
        RotaLink(i).Height = 3 * RotaLink(i).LineHeight * Screen.TwipsPerPixelY
        RotaLink(i).LeftTextOffset = 2
        RotaLink(i).GridLineColor = DarkGray
        RotaLink(i).CreateDecorationObject "SrvRota", "BTR", "8", Trim(Str(LightRed))
        RotaLink(i).CreateDecorationObject "CliRota", "BTL", "8", Trim(Str(LightRed))
        RotaLink(i).DateTimeSetColumn 1
        RotaLink(i).DateTimeSetInputFormatString 1, "YYYYMMDD"
        RotaLink(i).DateTimeSetOutputFormatString 1, "DDMMMYY"
        RotaLink(i).MainHeader = True
        RotaLink(i).MainHeaderOnly = True
        RotaLink(i).ShowRowSelection = False
        RotaLink(i).LifeStyle = True
    Next
    
    RotaLink(4).SetMainHeaderValues "4,1", "Current Selected Flight,  ", "12632256,12632256"
    RotaLink(5).SetMainHeaderValues "4,1", "Operational Days,  ", "12632256,12632256"
    
    BottomLabels(2).Width = NoopList(2).Width + 30
    NoopList(1).Left = NoopList(2).Left + NoopList(2).Width
    NoopList(1).SetMainHeaderValues MainHeadCols, "UFIS AODB Flight Data,Route Data,Rotation Data,System Data,Appendix", "12632256,12632256,12632256,12632256"
    NoopList(1).MainHeader = True
    
    
    For i = 1 To 4
        tmpDat = Trim(Str(CliFkeyCnt(i).BackColor))
        NoopList(2).CreateDecorationObject tmpDat & "1", "L,L,T,B", "-1,2,2,2", tmpDat & "," & Str(NormalGray) & "," & Str(NormalGray) & "," & Str(DarkGray)
        NoopList(2).CreateDecorationObject tmpDat & "2", "L,B,T", "-1,2,2", tmpDat & "," & Str(DarkGray) & "," & Str(NormalGray)
        NoopList(2).CreateDecorationObject tmpDat & "3", "L,R,T,B", "-1,2,2,2", tmpDat & "," & Str(DarkGray) & "," & Str(NormalGray) & "," & Str(DarkGray)
    Next
    For i = 4 To 5
        tmpDat = Trim(Str(SrvFkeyCnt(i).BackColor))
        NoopList(2).CreateDecorationObject tmpDat & "1", "L,L,T,B", "-1,2,2,2", tmpDat & "," & Str(NormalGray) & "," & Str(NormalGray) & "," & Str(DarkGray)
        NoopList(2).CreateDecorationObject tmpDat & "2", "L,B,T", "-1,2,2", tmpDat & "," & Str(DarkGray) & "," & Str(NormalGray)
        NoopList(2).CreateDecorationObject tmpDat & "3", "L,R,T,B", "-1,2,2,2", tmpDat & "," & Str(DarkGray) & "," & Str(NormalGray) & "," & Str(DarkGray)
    Next
    NoopList(2).CreateDecorationObject "SelMarker1", "L,L,T,B", "-1,2,2,2", Str(LightBlue) & "," & Str(LightestBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
    NoopList(2).CreateDecorationObject "SelMarker2", "L,B,T", "-1,2,2", Str(LightBlue) & "," & Str(DarkBlue) & "," & Str(LightestBlue)
    NoopList(2).CreateDecorationObject "SelMarker3", "L,R,T,B", "-1,2,2,2", Str(LightBlue) & "," & Str(DarkBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
    
    ToggleDetailCaption
    
    AlcTabCombo(0).ResetContent
    AlcTabCombo(0).ShowVertScroller False
    AlcTabCombo(0).ShowHorzScroller False
    AlcTabCombo(0).VScrollMaster = False
    AlcTabCombo(0).FontName = "Courier New"
    AlcTabCombo(0).SetTabFontBold True
    AlcTabCombo(0).HeaderFontSize = 17
    AlcTabCombo(0).FontSize = 17
    AlcTabCombo(0).LineHeight = 19
    AlcTabCombo(0).GridLineColor = vbBlack
    AlcTabCombo(0).HeaderLengthString = Str(AlcTabCombo(0).Width / Screen.TwipsPerPixelX)
    AlcTabCombo(0).HeaderString = "ALC"
    AlcTabCombo(0).ColumnAlignmentString = "L"
    AlcTabCombo(0).ColumnWidthString = "500"
    AlcTabCombo(0).Height = 1 * AlcTabCombo(0).LineHeight * Screen.TwipsPerPixelY
    AlcTabCombo(0).MainHeaderOnly = True
    AlcTabCombo(0).InsertTextLine ",,", False
    AlcTabCombo(0).SetCurrentSelection 0
    AlcTabCombo(0).CreateComboObject "AirlCodes", 0, 3, "EDIT", 400
    AlcTabCombo(0).SetComboColumn "AirlCodes", 0
    AlcTabCombo(0).ComboMinWidth = 30
    AlcTabCombo(0).EnableInlineEdit True
    AlcTabCombo(0).NoFocusColumns = "0"
    AlcTabCombo(0).ComboSetColumnLengthString "AirlCodes", "32,32,350"
    tmpTest = ",,,"
    AlcTabCombo(0).ComboAddTextLines "AirlCodes", tmpTest, vbLf
    AlcTabCombo(0).RedrawTab
    Load AlcTabCombo(1)
    AlcTabCombo(1).Visible = False
    
    RegnTabCombo(0).ResetContent
    RegnTabCombo(0).ShowVertScroller False
    RegnTabCombo(0).ShowHorzScroller False
    RegnTabCombo(0).VScrollMaster = False
    RegnTabCombo(0).FontName = "Courier New"
    RegnTabCombo(0).SetTabFontBold True
    RegnTabCombo(0).HeaderFontSize = 17
    RegnTabCombo(0).FontSize = 17
    RegnTabCombo(0).LineHeight = 19
    RegnTabCombo(0).GridLineColor = vbBlack
    RegnTabCombo(0).HeaderLengthString = Str(RegnTabCombo(0).Width / Screen.TwipsPerPixelX)
    RegnTabCombo(0).HeaderString = "ALC"
    RegnTabCombo(0).ColumnAlignmentString = "L"
    RegnTabCombo(0).ColumnWidthString = "12"
    RegnTabCombo(0).Height = 1 * RegnTabCombo(0).LineHeight * Screen.TwipsPerPixelY
    RegnTabCombo(0).MainHeaderOnly = True
    RegnTabCombo(0).InsertTextLine ",,", False
    RegnTabCombo(0).SetCurrentSelection 0
    RegnTabCombo(0).CreateComboObject "RegnCodes", 0, 1, "EDIT", 105
    RegnTabCombo(0).SetComboColumn "RegnCodes", 0
    RegnTabCombo(0).ComboMinWidth = 30
    RegnTabCombo(0).EnableInlineEdit True
    RegnTabCombo(0).NoFocusColumns = "0"
    RegnTabCombo(0).ComboSetColumnLengthString "RegnCodes", "105"
    tmpTest = ","
    RegnTabCombo(0).ComboAddTextLines "RegnCodes", tmpTest, vbLf
    RegnTabCombo(0).RedrawTab
    Load RegnTabCombo(1)
    RegnTabCombo(1).Visible = False
    
    FlnoTabCombo(0).ResetContent
    FlnoTabCombo(0).ShowVertScroller False
    FlnoTabCombo(0).ShowHorzScroller False
    FlnoTabCombo(0).VScrollMaster = False
    FlnoTabCombo(0).FontName = "Courier New"
    FlnoTabCombo(0).SetTabFontBold True
    FlnoTabCombo(0).HeaderFontSize = 17
    FlnoTabCombo(0).FontSize = 17
    FlnoTabCombo(0).LineHeight = 19
    FlnoTabCombo(0).GridLineColor = vbBlack
    FlnoTabCombo(0).HeaderLengthString = Str(FlnoTabCombo(0).Width / Screen.TwipsPerPixelX)
    FlnoTabCombo(0).HeaderString = "ALC"
    FlnoTabCombo(0).ColumnAlignmentString = "L"
    FlnoTabCombo(0).ColumnWidthString = "12"
    FlnoTabCombo(0).Height = 1 * FlnoTabCombo(0).LineHeight * Screen.TwipsPerPixelY
    FlnoTabCombo(0).MainHeaderOnly = True
    FlnoTabCombo(0).InsertTextLine ",,", False
    FlnoTabCombo(0).SetCurrentSelection 0
    FlnoTabCombo(0).CreateComboObject "FlnoCodes", 0, 1, "EDIT", 105
    FlnoTabCombo(0).SetComboColumn "FlnoCodes", 0
    FlnoTabCombo(0).ComboMinWidth = 30
    FlnoTabCombo(0).EnableInlineEdit True
    FlnoTabCombo(0).NoFocusColumns = "0"
    FlnoTabCombo(0).ComboSetColumnLengthString "FlnoCodes", "105"
    tmpTest = ","
    FlnoTabCombo(0).ComboAddTextLines "FlnoCodes", tmpTest, vbLf
    FlnoTabCombo(0).RedrawTab
    Load FlnoTabCombo(1)
    FlnoTabCombo(1).Visible = False
    
    For i = 0 To 5
        ScrollCover(i).Height = RotaLink(i).Height + 120
    Next
    For i = 0 To 2
        NoopList(i).ZOrder
    Next
    Me.Width = TopLabels.Width + 2 * TopLabels.Left + 6 * Screen.TwipsPerPixelX
    Me.Left = 300
    If StartedAsImportTool Then
        If Not NoopCheckDoubles Then
            Me.Top = FlightExtract.Top
            Me.Height = FlightExtract.Height
        End If
    Else
        Me.Top = 300
    End If
End Sub
Private Sub ToggleRotationView(ShowRotation As Boolean)
    Dim HdrLenLst As String
    Dim ActColLst As String
    Dim ColAlign As String
    Dim tmpHeader As String
    Dim tmpHedLen As String
    Dim tmpLen As Integer
    Dim tmpDat As String
    Dim TotalSize As Long
    Dim tmpSize As Long
    Dim tmpFkeyCol As Integer
    Dim i As Integer
    If ShowRotation Then
        tmpFkeyCol = CInt(RegnCol + 12)
        tmpHeader = "A,S, Flights,I,Time,D,ACT,APC,VIA,S,REGN,Linked, Date   Time,I,  FKEY,RKEY,URNO,ROTAKEY,C,D,Remark"
        ActColLst = "1,1,0,0,5,0,3,3,3,1,6,9,12,1,18,10,10,10,1,1,"
    Else
        tmpFkeyCol = CInt(RegnCol + 12)
        tmpHeader = "A,S, Flights,I,Time,D,ACT,APC,VIA,S,REGN,Linked, Date   Time,I,  FKEY,RKEY,URNO,ROTAKEY,C,D,Remark"
        ActColLst = "1,1,9,1,5,1,3,3,3,1,6,9,12,1,18,10,10,10,1,1,"
    End If
    ColAlign = ""
    HdrLenLst = ""
    TotalSize = 0
    i = 0
    Do
        i = i + 1
        tmpDat = GetItem(ActColLst, i, ",")
        If tmpDat <> "" Then
            tmpLen = Val(tmpDat)
            If tmpLen > 0 Then
                tmpSize = (TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX) + 7
            Else
                tmpSize = 0
            End If
            HdrLenLst = HdrLenLst & Str(tmpSize) & ","
            If i < tmpFkeyCol Then TotalSize = TotalSize + tmpSize
            ColAlign = ColAlign & "L,"
        End If
    Loop While tmpDat <> ""
    HdrLenLst = HdrLenLst & "1000"
    ColAlign = ColAlign & "L"
    ActColLst = ActColLst & "40"
    For i = 0 To 1
        NoopList(i).HeaderLengthString = HdrLenLst
        NoopList(i).HeaderString = tmpHeader
        NoopList(i).ColumnAlignmentString = ColAlign
        NoopList(i).ColumnWidthString = ActColLst
        NoopList(i).ShowVertScroller True
        NoopList(i).RedrawTab
    Next
    For i = 0 To 3
        RotaLink(i).HeaderLengthString = HdrLenLst
        RotaLink(i).HeaderString = tmpHeader
        RotaLink(i).ColumnAlignmentString = ColAlign
        RotaLink(i).ColumnWidthString = ActColLst
        RotaLink(i).ShowVertScroller False
        RotaLink(i).RedrawTab
    Next
    ToggleDetailCaption
    If ShowRotation Then
        NoopList(0).OnHScrollTo 1
        NoopList(1).OnHScrollTo 1
    Else
        NoopList(0).OnHScrollTo 0
        NoopList(1).OnHScrollTo 0
    End If
    
End Sub

Private Sub chkWork_Click(Index As Integer)
    Dim i As Integer
    Dim tmpResult As String
    On Error Resume Next
    If chkWork(Index).Value = 0 Then chkWork(Index).BackColor = vbButtonFace
    Select Case Index
        Case 0            'Close
            If chkWork(Index).Value = 1 Then
                If chkWork(Index).Caption = "Close" Then
                    If (Not NoopCheckDoubles) And (Not StartedAsCompareTool) Then RecFilter.chkTool(4).Value = 0
                    Unload Me
                    Exit Sub
                Else
                    StopAllLoops = True
                    chkWork(Index).Value = 0
                End If
            End If
        Case 1            'Shrink
            If chkWork(Index).Value = 1 Then
                AdjustShrinkLabels
                DspShrink.Visible = True
            Else
                DspShrink.Visible = False
            End If
            FormIsBusy = True
            AdjustScrollMaster -2, -1, "", False
            FormIsBusy = False
        Case 2
            'AODB
            chkWork(6).Value = 0
            If chkWork(Index).Value = 1 Then
                CheckStopButton = True
                TotalStartZeit = Timer
                FormIsBusy = True
                chkWork(5).Value = 0
                chkWork(1).Value = 0
                chkWork(3).Value = 0
                chkWork(4).Value = 0
                If ShowStopButton Then
                    chkWork(0).Caption = "Stop"
                    chkFullMode.Enabled = False
                    chkUpdMode.Enabled = False
                    chkWork(0).MousePointer = 1
                End If
                ShowSecondList True, "2,0"
                AdjustCounterCaption 0, 13, 0, 0, False
                Me.Refresh
                If CreateFlightLists = True Then
                    tmpResult = Trim(Str(Timer - TotalStartZeit))
                    If Left(tmpResult, 1) = "." Then tmpResult = "0" & tmpResult
                    tmpResult = GetItem(tmpResult, 1, ".") & "." & Left(GetItem(tmpResult, 2, "."), 3) & "s"
                    TotalSecs.Caption = tmpResult
                    chkWork(1).Enabled = False
                    chkWork(3).Enabled = True
                    chkWork(6).Enabled = False
                    chkWork(3).Value = 1
                    chkWork(6).Enabled = True
                    chkWork(13).Enabled = True
                    FormIsBusy = False
                Else
                    FormIsBusy = False
                    chkWork(Index).Value = 0
                End If
                If ShowStopButton Then
                    chkWork(0).Caption = "Close"
                    chkFullMode.Enabled = True
                    chkUpdMode.Enabled = True
                    chkWork(0).MousePointer = 0
                End If
                CheckStopButton = False
                Me.Refresh
                If StopAllLoops Then chkWork(Index).Value = 0
            Else
                chkWork(1).Enabled = False
                chkWork(3).Enabled = False
                chkWork(4).Value = 0
                chkWork(4).Enabled = False
                chkWork(6).Enabled = False
                chkWork(13).Value = 0
                chkWork(13).Enabled = False
                AdjustCounterCaption 0, 13, 0, 0, False
                'If StartedAsCompareTool Then
                    'chkSpool(4).Value = 1
                'End If
                NoopList(0).ResetContent
                NoopList(1).ResetContent
                NoopList(2).ResetContent
                NoopList(0).RedrawTab
                NoopList(1).RedrawTab
                NoopList(2).RedrawTab
                Me.Refresh
                StopAllLoops = False
            End If
        Case 3
            'Flights
            If chkWork(Index).Value = 1 Then
                FormIsBusy = True
                chkWork(4).Value = 0
                chkWork(Index).Enabled = False
                If chkWork(5).Value = 1 Then
                    'Doubles
                    CompareFlightLists FkeyCol, FkeyCol
                Else
                    'AODB
                    CompareFlightLists RotaCol, RotaCol
                    If DataSystemType <> "OAFOS" Then
                        ShowSecondList True, "2,22,13,1,6,0"
                    Else
                        ShowSecondList True, "2,13,6,0"
                    End If
                End If
                tmpResult = Trim(Str(Timer - TotalStartZeit))
                If Left(tmpResult, 1) = "." Then tmpResult = "0" & tmpResult
                tmpResult = GetItem(tmpResult, 1, ".") & "." & Left(GetItem(tmpResult, 2, "."), 3) & "s"
                TotalSecs.Caption = tmpResult
                chkWork(1).Enabled = True
                chkWork(4).Enabled = True
                FormIsBusy = False
            End If
        Case 4
            'Rotation
            If chkWork(Index).Value = 1 Then
                ToggleRotationView True
            Else
                ToggleRotationView False
            End If
        Case 5            'Doubles
            If chkWork(Index).Value = 1 Then
                If Not NoopCheckDoubles Then
                    TotalStartZeit = Timer
                    FormIsBusy = True
                    chkWork(2).Value = 0
                    chkWork(1).Value = 0
                    chkWork(3).Value = 0
                    ShowSecondList False, "5,13,1,0"
                    CreateFlightLists
                    'chkWork(1).Enabled = True
                    chkWork(3).Enabled = False
                    chkWork(4).Enabled = False
                    FormIsBusy = False
                    tmpResult = Trim(Str(Timer - TotalStartZeit))
                    If Left(tmpResult, 1) = "." Then tmpResult = "0" & tmpResult
                    tmpResult = GetItem(tmpResult, 1, ".") & "." & Left(GetItem(tmpResult, 2, "."), 3) & "s"
                    TotalSecs.Caption = tmpResult
                Else
                    CreateFlightLists
                End If
            Else
                chkWork(1).Enabled = False
                chkWork(3).Enabled = False
                chkWork(4).Enabled = False
            End If
        Case 6  'Activate
            If chkWork(Index).Value = 1 Then
                If chkWork(18).Value = chkWork(19).Value Then
                    If UpdateFtypOnDelete = True Then
                        chkWork(19).Value = 1
                    Else
                        chkWork(18).Value = 1
                    End If
                End If
                If DataSystemType <> "OAFOS" Then
                    If RotationMode Then
                        ShowSecondList True, "2,22,13,1,6,7,14,15,16,17,8,0"
                    Else
                        If (Val(SrvFkeyCnt(4).Caption) + Val(SrvFkeyCnt(5).Caption)) > 0 Then
                            ShowSecondList True, "2,22,13,1,6,20,18,19,21,0"
                        Else
                            ShowSecondList True, "2,22,13,1,6,0"
                        End If
                    End If
                Else
                    ShowSecondList True, "2,13,6,7,14,15,12,0"
                End If
                For i = 7 To 12
                    chkWork(i).Enabled = True
                    If chkWork(i).Value = 1 Then chkWork(i).BackColor = LightGreen
                Next
                NoopList(2).SetCurrentSelection NoopList(2).GetCurrentSelected
            Else
                If DataSystemType <> "OAFOS" Then
                    ShowSecondList True, "2,22,13,1,6,0"
                Else
                    ShowSecondList True, "2,13,6,0"
                End If
                For i = 7 To 12
                    chkWork(i).Enabled = False
                    chkWork(i).BackColor = vbButtonFace
                Next
                ClearLineSelection
            End If
            AdjustImpButtons
            NoopList(2).SetFocus
        Case 9      'Insert
            HandleActionButton "INS"
            NoopList(2).SetFocus
        Case 10     'Update
            HandleActionButton "UPD"
            NoopList(2).SetFocus
        Case 11     'Delete
            HandleActionButton "DEL"
            NoopList(2).SetFocus
        Case 12
            If chkWork(Index).Value = 0 Then ClearLineSelection
            NoopList(2).SetCurrentSelection NoopList(2).GetCurrentSelected
            NoopList(2).SetFocus
        Case 13
            Form_Resize
            If chkWork(Index).Value = 1 Then
                NoopList(2).SetCurrentSelection NoopList(2).GetCurrentSelected
            End If
            NoopList(2).SetFocus
        Case 14 'Single
            If chkWork(Index).Value = 1 Then
                If DataSystemType = "OAFOS" Then
                    'OAFOS single Import
                    chkWork(Index).BackColor = LightGreen
                    OaFosRegnImport True, DataSystemAlc3
                    chkWork(Index).Value = 0
                Else
                    CheckSingleImport False
                End If
            End If
        Case 15 'OAFOS or Full Import
            If chkWork(Index).Value = 1 Then
                If DataSystemType = "OAFOS" Then
                    chkWork(Index).BackColor = LightGreen
                    OaFosRegnImport False, DataSystemAlc3
                    chkWork(Index).Value = 0
                Else
                    chkWork(Index).Value = 0
                End If
            End If
        Case 16 'Join
            chkWork(Index).Value = 0
        Case 17 'Split
            chkWork(Index).Value = 0
        Case 18 'DEL
            If chkWork(Index).Value = 1 Then
                chkWork(19).Value = 0
            Else
                chkWork(19).Value = 1
            End If
        Case 19 'NOP
            If chkWork(Index).Value = 1 Then
                chkWork(18).Value = 0
            Else
                chkWork(18).Value = 1
            End If
        Case 22 'Fields
            If chkWork(Index).Value = 1 Then
                frmUfrFields.Show , Me
            Else
                frmUfrFields.Hide
            End If

        Case Else
    End Select
    If chkWork(Index).Value = 1 Then chkWork(Index).BackColor = LightGreen
    Me.MousePointer = 0
End Sub

Private Sub CheckSingleImport(DoIt As Boolean)
    Dim SelLineNo As Long
    Dim CurLineNo As Long
    Dim LineTag As String
    If (chkWork(14).Value = 1) Or (DoIt) Then
        SelLineNo = NoopList(2).GetCurrentSelected
        If SelLineNo >= 0 Then
            CurLineNo = Val(NoopList(2).GetColumnValue(SelLineNo, 4))
            If NoopList(0).GetColumnValue(CurLineNo, DoneCol) <> "1" Then
                If RotationMode Then
                    LineTag = NoopList(0).GetLineTag(CurLineNo)
                    If InStr(LineTag, "RotaJoin") = 0 Then SelLineNo = -1
                End If
                If SelLineNo >= 0 Then CallServerAction "UPD", 1, False, Str(SelLineNo)
            End If
        End If
    End If
End Sub
Private Sub OaFosRegnImport(SingleMode As Boolean, Alc3List As String)
    Dim MsgTxt As String
    Dim RetVal As Integer
    Dim LedIdx As Integer
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FirstRegnLine As Long
    Dim SelList As String
    Dim LinNbr As String
    Dim ItmNbr As Long
    Dim CurRegn As String
    Dim chkTime As String
    Dim CurPrfl As String
    Dim CurFtyp As String
    Dim tmpTime As String
    Dim tmpRota As String
    Dim RefRegn As String
    Dim CurAdid As String
    Dim tmpLine As String
    Dim ArrLine As String
    Dim DepLine As String
    Dim MinTime As String
    Dim MaxTime As String
    Dim ArrFldLst As String
    Dim DepFldLst As String
    Dim tmpFldLst As String
    Dim tmpCmd As String
    Dim tmpKey As String
    Dim OutResult As String
    Dim RefLine As Long
    Dim MidLine As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim CurLineTag As String
    Dim SomeThingChanged As Boolean
    Dim SomeThingSent As Boolean
    Dim LineCode As String
    Dim StartZeit
    SomeThingSent = False
    RetVal = 1
    If (SingleMode = False) Or (chkWork(7).Value = 1) Then
        If SingleMode Then
            If SelRegnList <> "" Then
                If InStr(SelRegnList, ",") = 0 Then
                    MsgTxt = "Do you want to import the turnarounds of " & SelRegnList & "?"
                Else
                    MsgTxt = "Do you want to import the turnarounds of the selected aircrafts" & vbNewLine & SelRegnList & "?"
                End If
            Else
                OaFosCancel -1, True
                RetVal = -1
            End If
        Else
            MsgTxt = "Do you want to import the full list ?"
        End If
        If RetVal = 1 Then RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", MsgTxt, "ask", "Yes;F,No", UserAnswer)
        Me.Refresh
    End If
    If RetVal = 1 Then
        Me.MousePointer = 11
        StartZeit = Timer
        UsedSec.Caption = ""
        CedaProgress.Show , Me
        CedaProgress.Top = Me.Top + NoopList(1).Top + 15 * Screen.TwipsPerPixelY
        CedaProgress.Left = Me.Left + NoopList(1).Left + 4 * Screen.TwipsPerPixelX
        CedaProgress.Caption = "OA FOS Import Progress"
        If SingleMode = False Then CedaProgress.MsgList(0).ResetContent
        CedaProgress.Refresh
        'UfisServer.BcSpool True
        CedaLed(0).FillColor = NormalGreen
        CedaLed(0).Visible = True
        CedaLed(0).Refresh
        CedaLed(1).FillColor = NormalGreen
        CedaLed(1).Visible = True
        CedaLed(1).Refresh
        LedIdx = 0
        ImportIsBusy = True
        MidLine = (NoopList(2).Height / (NoopList(2).LineHeight * Screen.TwipsPerPixelY) / 2) - 2
        'ArrFldLst = "REGN,ADID,FKEY,FTYP,STOA,ORG3,VIAL," & UseStypField
        'DepFldLst = "REGN,ADID,FKEY,FTYP,STOD,DES3,VIAL," & UseStypField
        ArrFldLst = ""
        DepFldLst = ""
        If SingleMode Then SelList = NoopList(2).GetLinesByStatusValue(1, 0) Else SelList = ""
        chkWork(12).Value = 0
        ClearLineSelection
        RefRegn = ""
        ArrLine = ""
        DepLine = ""
        OutResult = ""
        ItmNbr = -1
        CurLine = -1
        SomeThingChanged = False
        MaxLine = NoopList(2).GetLineCount - 1
        While CurLine < MaxLine
            If SingleMode Then
                ItmNbr = ItmNbr + 1
                LinNbr = GetRealItem(SelList, ItmNbr, ",")
                If LinNbr <> "" Then CurLine = Val(LinNbr) Else MaxLine = -1
            Else
                CurLine = CurLine + 1
                If CurLine > MaxLine Then MaxLine = -1
            End If
            If CurLine <= MaxLine Then
                If CurLine > MidLine Then
                    NoopList(2).OnVScrollTo CurLine - MidLine
                ElseIf CurLine < NoopList(2).GetVScrollPos Then
                    NoopList(2).OnVScrollTo CurLine
                End If
                NoopList(2).SetCurrentSelection CurLine
                Me.MousePointer = 11
                RefLine = Val(NoopList(2).GetColumnValue(CurLine, 4))
                CurRegn = NoopList(0).GetColumnValue(RefLine, RegnCol)
                chkTime = GetImportCheckTime
                tmpTime = NoopList(0).GetColumnValue(RefLine, TimeCol)
                CurFtyp = NoopList(0).GetColumnValue(RefLine, FtypCol)
                If (CurFtyp = "X") Or (CurFtyp = "N") Then CurRegn = ""
                If CurRegn <> "" Then
                    CurPrfl = NoopList(0).GetColumnValue(RefLine, PrflCol)
                    If Len(CurPrfl) > 1 Then CurPrfl = Mid(CurPrfl, 2, 1)
                    If CurPrfl <> "!" Then
                        If tmpTime >= chkTime Then
                            If RefRegn = "" Then
                                RefRegn = CurRegn
                                MinTime = tmpTime
                                MaxTime = tmpTime
                                CurPos(3).Caption = "Turnarounds of " & RefRegn
                                CurPos(3).Refresh
                                FirstRegnLine = CurLine
                            End If
                            CurAdid = NoopList(0).GetColumnValue(RefLine, AdidCol)
                            CurLineTag = Trim(NoopList(2).GetLineTag(CurLine))
                            If CurLineTag = "/" Then CurLineTag = ""
                            PrepareAftInsert RefLine, tmpCmd, tmpKey, tmpFldLst, tmpLine, "REGN"
                            tmpLine = CurRegn & "," & CurAdid & "," & TurnImpFkeyToAftFkey(NoopList(0).GetColumnValue(RefLine, FkeyCol)) & "," & tmpLine
                            NoopList(0).ResetLineDecorations RefLine
                            NoopList(1).ResetLineDecorations RefLine
                            If NoopList(0).GetLineTag(RefLine) <> "" Then
                                CliFkeyCnt(2).Caption = Trim(Str(Val(CliFkeyCnt(2).Caption) - 1))
                                NoopList(0).SetLineTag RefLine, ""
                            End If
                            If NoopList(1).GetLineTag(RefLine) <> "" Then
                                SrvFkeyCnt(2).Caption = Trim(Str(Val(SrvFkeyCnt(2).Caption) - 1))
                                NoopList(1).SetLineTag RefLine, ""
                            End If
                            NoopList(2).SetLineTag CurLine, ""
                            NoopList(0).GetLineColor RefLine, CurForeColor, CurBackColor
                            If CurBackColor = CliFkeyCnt(3).BackColor Then
                                'phyoe
                                CliFkeyCnt(3).Caption = Trim(Str(Val(CliFkeyCnt(3).Caption) - 1))
                                SrvFkeyCnt(3).Caption = Trim(Str(Val(SrvFkeyCnt(3).Caption) - 1))
                            ElseIf CurBackColor = CliFkeyCnt(4).BackColor Then
                                CliFkeyCnt(4).Caption = Trim(Str(Val(CliFkeyCnt(4).Caption) - 1))
                            End If
                            tmpRota = NoopList(1).GetColumnValue(RefLine, RotaCol)
                            NoopList(1).UpdateTextLine RefLine, NoopList(0).GetLineValues(RefLine), False
                            NoopList(0).SetColumnValue RefLine, 0, "I"
                            NoopList(1).SetColumnValue RefLine, 0, ">"
                            NoopList(1).SetColumnValue RefLine, RotaCol, tmpRota
                            'NoopList(1).SetLineColor RefLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                            NoopList(0).SetLineColor RefLine, CliFkeyCnt(4).ForeColor, CliFkeyCnt(4).BackColor
                            NoopList(2).SetLineColor RefLine, CliFkeyCnt(4).ForeColor, CliFkeyCnt(4).BackColor
                            CliFkeyCnt(4).Caption = Trim(Str(Val(CliFkeyCnt(4).Caption) + 1))
                            'SrvFkeyCnt(4).Caption = Trim(Str(Val(SrvFkeyCnt(4).Caption) + 1))
                            If CurRegn = RefRegn Then
                                Select Case CurAdid
                                    Case "A"
                                        If ArrLine <> "" Then
                                            If DepLine = "" Then DepLine = RefRegn & ",D,-"
                                            OutResult = OutResult & ArrLine & vbLf & DepLine & vbLf
                                        End If
                                        ArrLine = tmpLine
                                        If ArrFldLst = "" Then ArrFldLst = "REGN,ADID,FKEY," & tmpFldLst
                                        DepLine = ""
                                    Case "D"
                                        DepLine = tmpLine
                                        If DepFldLst = "" Then DepFldLst = "REGN,ADID,FKEY," & tmpFldLst
                                        If ArrLine = "" Then ArrLine = RefRegn & ",A,-"
                                        OutResult = OutResult & ArrLine & vbLf & DepLine & vbLf
                                        ArrLine = ""
                                        DepLine = ""
                                    Case Else
                                End Select
                                If tmpTime > MaxTime Then MaxTime = tmpTime
                                If (CurBackColor = CliFkeyCnt(3).BackColor) Or (CurBackColor = CliFkeyCnt(4).BackColor) Or (CurLineTag <> "") Then
                                    SomeThingChanged = True
                                End If
                            Else
                                If ArrLine <> "" Then
                                    DepLine = RefRegn & ",D,-"
                                    OutResult = OutResult & ArrLine & vbLf & DepLine & vbLf
                                    ArrLine = ""
                                    DepLine = ""
                                End If
'SomeThingChanged = True
                                CedaProgress.HandleProgress 1, CurLine, MaxLine
                                If (SomeThingChanged) And (OutResult <> "") Then
                                    NoopList(0).RedrawTab
                                    NoopList(1).RedrawTab
                                    NoopList(2).RedrawTab
                                    OutResult = "*CMD*,ACC," & RefRegn & "," & MinTime & "00," & MaxTime & "59," & Alc3List & vbLf & OutResult
                                    OutResult = Left(OutResult, Len(OutResult) - 1)
                                    CedaLed(LedIdx).FillColor = NormalYellow
                                    CedaLed(LedIdx).Refresh
                                    Me.Refresh
                                    'MsgBox ArrFldLst & vbLf & DepFldLst
                                    LineCode = CedaProgress.AppendToMyList(RefRegn & "," & MinTime & "," & MaxTime & ",,,,")
                                    UfisServer.TwsCode.Text = "F.NBC." & LineCode & "." & Trim(Str(FirstRegnLine)) & "." & Trim(Str(MaxLine)) & "." & UfisServer.GetMyWorkStationName
                                    If Not SomeThingSent Then
                                        UfisServer.CallCeda UserAnswer, "SEAS", "AFTTAB", "", "", "ACCBGN,OAFOS", "", 0, False, False
                                        SomeThingSent = True
                                    End If
                                    UfisServer.CallCeda UserAnswer, "SEAS", "AFTTAB", (ArrFldLst & vbLf & DepFldLst), OutResult, "ACC,OAFOS", "", 0, False, False
                                    CedaProgress.UpdateMyList LineCode, RefRegn & "," & MinTime & "," & MaxTime & ",X,,,"
                                    CedaLed(LedIdx).FillColor = NormalGreen
                                    CedaLed(LedIdx).Refresh
                                    If LedIdx = 0 Then LedIdx = 1 Else LedIdx = 0
                                    Me.Refresh
                                End If
                                SomeThingChanged = False
                                RefRegn = CurRegn
                                MinTime = tmpTime
                                MaxTime = tmpTime
                                FirstRegnLine = CurLine
                                OutResult = ""
                                Select Case CurAdid
                                    Case "A"
                                        ArrLine = tmpLine
                                    Case "D"
                                        DepLine = tmpLine
                                        ArrLine = RefRegn & ",A,-"
                                        OutResult = OutResult & ArrLine & vbLf & DepLine & vbLf
                                        ArrLine = ""
                                        DepLine = ""
                                    Case Else
                                End Select
                                If (CurBackColor = CliFkeyCnt(3).BackColor) Or (CurBackColor = CliFkeyCnt(4).BackColor) Or (CurLineTag <> "") Then
                                    SomeThingChanged = True
                                End If
                                CurPos(3).Caption = "Turnarounds of " & RefRegn
                                CurPos(3).Refresh
                            End If
                        End If
                    End If
                Else
                    If (CurFtyp = "X") Or (CurFtyp = "N") Then
                        OaFosCancel CurLine, False
                    End If
                End If
            End If
            MsgTxt = Trim(Str(Timer - StartZeit))
            If Left(MsgTxt, 1) = "." Then MsgTxt = "0" & MsgTxt
            MsgTxt = GetItem(MsgTxt, 1, ".") & "s"
            UsedSec.Caption = MsgTxt
            UsedSec.Refresh
            'UfisServer.GetNextBcFromSpool True
        Wend
        If ArrLine <> "" Then
            DepLine = RefRegn & ",D,-"
            OutResult = OutResult & ArrLine & vbLf & DepLine & vbLf
            ArrLine = ""
            DepLine = ""
        End If
'SomeThingChanged = True
        CedaProgress.HandleProgress 1, CurLine, MaxLine
        If (SomeThingChanged) And (OutResult <> "") Then
            OutResult = "*CMD*,ACC," & RefRegn & "," & MinTime & "00," & MaxTime & "59," & Alc3List & vbLf & OutResult
            OutResult = Left(OutResult, Len(OutResult) - 1)
            CedaLed(LedIdx).FillColor = NormalYellow
            CedaLed(LedIdx).Refresh
            LineCode = CedaProgress.AppendToMyList(RefRegn & "," & MinTime & "," & MaxTime & ",,,,")
            UfisServer.TwsCode.Text = "F.NBC." & LineCode & "." & Trim(Str(FirstRegnLine)) & "." & Trim(Str(MaxLine)) & "." & UfisServer.GetMyWorkStationName
            If Not SomeThingSent Then
                UfisServer.CallCeda UserAnswer, "SEAS", "AFTTAB", "", "", "ACCBGN,OAFOS", "", 0, False, False
                SomeThingSent = True
            End If
            UfisServer.CallCeda UserAnswer, "SEAS", "AFTTAB", (ArrFldLst & vbLf & DepFldLst), OutResult, "ACC,OAFOS", "", 0, False, False
            CedaProgress.UpdateMyList LineCode, RefRegn & "," & MinTime & "," & MaxTime & ",X,,,"
            CedaLed(LedIdx).FillColor = NormalGreen
            CedaLed(LedIdx).Refresh
        End If
        CedaProgress.HandleProgress 1, -1, -1
        NoopList(2).RedrawTab
        NoopList(1).RedrawTab
        NoopList(0).RedrawTab
        CedaLed(0).Visible = False
        CedaLed(1).Visible = False
        CurPos(3).Caption = ""
        CurPos(3).Refresh
        'UfisServer.BcSpool False
        If SomeThingSent Then
            UfisServer.TwsCode.Text = "F.NBC.-1.-1.-1." & UfisServer.GetMyWorkStationName
            UfisServer.CallCeda UserAnswer, "SEAS", "AFTTAB", "", "", "ACCEND,OAFOS", "", 0, False, False
        End If
        UfisServer.TwsCode.Text = ".NBC."
        MsgTxt = Trim(Str(Timer - StartZeit))
        If Left(MsgTxt, 1) = "." Then MsgTxt = "0" & MsgTxt
        MsgTxt = GetItem(MsgTxt, 1, ".") & "s"
        UsedSec.Caption = MsgTxt
        ImportIsBusy = False
        Me.MousePointer = 0
    End If
End Sub
Private Sub OaFosCancel(SelLine As Long, AskBack As Boolean)
    Dim RetVal As Integer
    Dim SqlRet As Integer
    Dim MsgTxt As String
    Dim CurLine As Long
    Dim RefLine As Long
    Dim CurRegn As String
    Dim chkTime As String
    Dim tmpTime As String
    Dim CurFtyp As String
    Dim tmpRota As String
    Dim CurCmd As String
    Dim SqlKey As String
    Dim FldLst As String
    Dim DatLst As String
    Dim TblNam As String
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    If SelLine >= 0 Then CurLine = SelLine Else CurLine = NoopList(2).GetCurrentSelected
    RefLine = Val(NoopList(2).GetColumnValue(CurLine, 4))
    chkTime = GetImportCheckTime
    tmpTime = NoopList(0).GetColumnValue(RefLine, TimeCol)
    If tmpTime >= chkTime Then
        CurFtyp = NoopList(0).GetColumnValue(RefLine, FtypCol)
        If (CurFtyp = "X") Or (CurFtyp = "N") Then
            RetVal = 0
            NoopList(0).GetLineColor RefLine, CurForeColor, CurBackColor
            If CurBackColor = CliFkeyCnt(3).BackColor Then
                MsgTxt = "Do you want to cancel this flight?"
                CurCmd = "UFR"
                RetVal = 1
            End If
            If CurBackColor = CliFkeyCnt(4).BackColor Then
                MsgTxt = "Do you want to insert this cancelled flight?"
                CurCmd = "ISF"
                RetVal = 1
            End If
            If (AskBack) And (RetVal = 1) Then RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", MsgTxt, "ask", "Yes;F,No", UserAnswer)
            If RetVal = 1 Then
                Select Case CurCmd
                    Case "UFR"
                        PrepareAftUpdate RefLine, CurCmd, SqlKey, FldLst, DatLst, "", "REGN"
                    Case "ISF"
                        PrepareAftInsert RefLine, CurCmd, SqlKey, FldLst, DatLst, "REGN"
                    Case Else
                End Select
                If CurCmd <> "" Then
                    TblNam = "AFTTAB"
                    SqlRet = UfisServer.CallCeda(UserAnswer, CurCmd, TblNam, FldLst, DatLst, SqlKey, "", 0, False, False)
                    NoopList(0).ResetLineDecorations RefLine
                    NoopList(1).ResetLineDecorations RefLine
                    If NoopList(0).GetLineTag(RefLine) <> "" Then
                        CliFkeyCnt(2).Caption = Trim(Str(Val(CliFkeyCnt(2).Caption) - 1))
                        NoopList(0).SetLineTag RefLine, ""
                    End If
                    If NoopList(1).GetLineTag(RefLine) <> "" Then
                        SrvFkeyCnt(2).Caption = Trim(Str(Val(SrvFkeyCnt(2).Caption) - 1))
                        NoopList(1).SetLineTag RefLine, ""
                    End If
                    NoopList(2).SetLineTag CurLine, ""
                    If CurBackColor = CliFkeyCnt(3).BackColor Then
                        'phyoe
                        CliFkeyCnt(3).Caption = Trim(Str(Val(CliFkeyCnt(3).Caption) - 1))
                        SrvFkeyCnt(3).Caption = Trim(Str(Val(SrvFkeyCnt(3).Caption) - 1))
                    ElseIf CurBackColor = CliFkeyCnt(4).BackColor Then
                        CliFkeyCnt(4).Caption = Trim(Str(Val(CliFkeyCnt(4).Caption) - 1))
                    End If
                    tmpRota = NoopList(1).GetColumnValue(RefLine, RotaCol)
                    NoopList(1).UpdateTextLine RefLine, NoopList(0).GetLineValues(RefLine), False
                    NoopList(0).SetColumnValue RefLine, 0, "I"
                    NoopList(1).SetColumnValue RefLine, 0, ">"
                    NoopList(1).SetColumnValue RefLine, RotaCol, tmpRota
                    'NoopList(1).SetLineColor RefLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                    NoopList(0).SetLineColor RefLine, CliFkeyCnt(4).ForeColor, CliFkeyCnt(4).BackColor
                    NoopList(2).SetLineColor RefLine, CliFkeyCnt(4).ForeColor, CliFkeyCnt(4).BackColor
                    CliFkeyCnt(4).Caption = Trim(Str(Val(CliFkeyCnt(4).Caption) + 1))
                    'SrvFkeyCnt(4).Caption = Trim(Str(Val(SrvFkeyCnt(4).Caption) + 1))
                End If
            End If
        End If
    End If
End Sub
Private Sub HandleActionButton(ActionCode As String)
    Dim CurLine As Long
    Dim TextColor As Long
    Dim BackColor As Long
    Dim ActionType As String
    Dim DoIt As Boolean
    If ActionCode = "SINGLE" Then
        'CheckSingleImport
    Else
        CurLine = NoopList(2).GetCurrentSelected
        NoopList(2).GetLineColor CurLine, TextColor, BackColor
        ActionType = DefineColorAction(BackColor)
        DoIt = False
        'If Not RotationMode Then
            Select Case ActionType
                Case "INS"
                    If chkWork(9).Value = 1 Then DoIt = True
                Case "UPD"
                    If chkWork(10).Value = 1 Then DoIt = True
                Case "DEL", "REM"
                    If chkWork(11).Value = 1 Then DoIt = True
                Case Else
            End Select
            If ActionCode <> "ALL" Then
                If ActionType <> ActionCode Then DoIt = False
            End If
        'End If
        If DoIt Then CallServerAction ActionType, 1, False, ""
    End If
End Sub

Private Sub ShowSecondList(SetVisible As Boolean, Buttons As String)
    Dim i As Integer
    Dim CurBtn As Integer
    Dim CurPos As Long
    Dim tmpItm As String
    NoopList(1).Visible = SetVisible
    BottomLabels(1).Visible = SetVisible
    For i = 0 To chkWork.UBound
        chkWork(i).Visible = False
    Next
    If SetVisible = True Then
        TopLabels.Width = NoopList(1).Left + NoopList(1).Width - 30
        If Me.WindowState = vbNormal Then Me.Width = TopLabels.Width + 2 * TopLabels.Left + 8 * Screen.TwipsPerPixelX + 30
    Else
        TopLabels.Width = NoopList(0).Width + NoopList(2).Width - 30
        If Me.WindowState = vbNormal Then Me.Width = TopLabels.Width + 2 * TopLabels.Left + 8 * Screen.TwipsPerPixelX + 30
    End If
    tmpItm = "START"
    i = 0
    If UseFkeyTime = False Then
        chkFullMode.Visible = True
        chkUpdMode.Visible = True
        CurPos = chkUpdMode.Left + chkUpdMode.Width + 15
    Else
        chkFullMode.Visible = False
        chkUpdMode.Visible = False
        CurPos = 30
    End If
    While tmpItm <> ""
        i = i + 1
        tmpItm = GetItem(Buttons, i, ",")
        If tmpItm <> "" Then
            CurBtn = Val(tmpItm)
            chkWork(CurBtn).Left = CurPos
            CurPos = CurPos + chkWork(CurBtn).Width + Screen.TwipsPerPixelX
            chkWork(CurBtn).Top = chkFullMode.Top
            chkWork(CurBtn).Visible = True
        End If
    Wend
    Me.Refresh
    'DoEvents
End Sub

Private Sub CreateServerRotations()
    Dim AppendFlights As Boolean
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim CurRkey As String
    Dim NxtRkey As String
    Dim tmpFkey As String
    Dim tmpTime As String
    Dim tmpSked As String
    Dim tmpAdid As String
    Dim tmpStoa As String
    Dim tmpStod As String
    Dim ArrRkeys As String
    Dim DepRkeys As String
    Dim SrvRtyp As String
    Dim NxtLine As Long
    Dim LoopCount As Integer
    Dim RightLine As Long
    Dim RightMax As Long
    Dim CurRightUrno As String
    Dim OldRightUrno As String
    
    AppendFlights = False
    LoopCount = 0
    Do
        Me.MousePointer = 11
        LoopCount = LoopCount + 1
        ArrRkeys = ""
        DepRkeys = ""
        NoopList(1).Sort Str(RkeyCol), True, True
        MaxLine = NoopList(1).GetLineCount - 1
        SetProgressBar 0, 1, MaxLine, "Building Flight Keys and Merging AODB Rotations ..."
        CurLine = 0
        NxtLine = 0
        AppendFlights = False
        While (CurLine <= MaxLine)
            If (CurLine Mod 100 = 0) Or (NxtLine Mod 100 = 0) Then SetProgressBar 0, 2, CurLine, ""
            If CurLine Mod 100 = 0 Then
                NoopList(1).OnVScrollTo CurLine
                NoopList(1).Refresh
            End If
            CurRkey = NoopList(1).GetColumnValue(CurLine, RkeyCol)
            If (CurRkey <> "") And (Left(CurRkey, 1) <> ".") Then
                NxtLine = CurLine + 1
                NxtRkey = NoopList(1).GetColumnValue(NxtLine, RkeyCol)
                If CurRkey = NxtRkey Then
                    If Trim(NoopList(1).GetColumnValue(CurLine, FltnCol)) <> "-" Then
                        tmpFkey = TurnAftFkey(NoopList(1).GetColumnValue(CurLine, FkeyCol))
                        NoopList(1).SetColumnValue CurLine, FkeyCol, tmpFkey
                        tmpTime = Left(NoopList(1).GetColumnValue(CurLine, TimeCol), 12)
                        NoopList(1).SetColumnValue CurLine, TimeCol, tmpTime
                        tmpSked = Left(NoopList(1).GetColumnValue(CurLine, SkedCol), 12)
                        If Len(tmpSked) <> 12 Then tmpSked = ""
                        NoopList(1).SetColumnValue CurLine, SkedCol, tmpSked
                        If UseFkeyTime Then tmpFkey = tmpFkey & Mid(tmpTime, 9, 4)
                        NoopList(1).SetColumnValue CurLine, RotaCol, tmpFkey
                        If NoopList(1).GetColumnValue(CurLine, PrflCol) <> "L" Then NoopList(1).SetColumnValue CurLine, PrflCol, ""
                    End If
                    If Trim(NoopList(1).GetColumnValue(NxtLine, FltnCol)) <> "-" Then
                        tmpFkey = TurnAftFkey(NoopList(1).GetColumnValue(NxtLine, FkeyCol))
                        NoopList(1).SetColumnValue NxtLine, FkeyCol, tmpFkey
                        tmpTime = Left(NoopList(1).GetColumnValue(NxtLine, TimeCol), 12)
                        NoopList(1).SetColumnValue NxtLine, TimeCol, tmpTime
                        tmpSked = Left(NoopList(1).GetColumnValue(NxtLine, SkedCol), 12)
                        If Len(tmpSked) <> 12 Then tmpSked = ""
                        NoopList(1).SetColumnValue NxtLine, SkedCol, tmpSked
                        If UseFkeyTime Then tmpFkey = tmpFkey & Mid(tmpTime, 9, 4)
                        NoopList(1).SetColumnValue NxtLine, RotaCol, tmpFkey
                        If NoopList(1).GetColumnValue(NxtLine, PrflCol) <> "L" Then NoopList(1).SetColumnValue NxtLine, PrflCol, ""
                    End If
                    NoopList(1).SetColumnValue CurLine, ChckCol, "0"
                    NoopList(1).SetColumnValue CurLine, DoneCol, "0"
                    NoopList(1).SetColumnValue NxtLine, ChckCol, "0"
                    NoopList(1).SetColumnValue NxtLine, DoneCol, "0"
                    NoopList(1).SetColumnValue CurLine, FltnCol, NoopList(1).GetColumnValue(NxtLine, FlnoCol)
                    NoopList(1).SetColumnValue CurLine, TifdCol, NoopList(1).GetColumnValue(NxtLine, TimeCol)
                    NoopList(1).SetColumnValue CurLine, RtypCol, NoopList(1).GetColumnValue(NxtLine, AdidCol)
                    NoopList(1).SetColumnValue CurLine, RkeyCol, ("." & CurRkey)
                    NoopList(1).SetColumnValue NxtLine, FltnCol, NoopList(1).GetColumnValue(CurLine, FlnoCol)
                    NoopList(1).SetColumnValue NxtLine, TifdCol, NoopList(1).GetColumnValue(CurLine, TimeCol)
                    NoopList(1).SetColumnValue NxtLine, RtypCol, NoopList(1).GetColumnValue(CurLine, AdidCol)
                    NoopList(1).SetColumnValue NxtLine, RkeyCol, ("." & CurRkey)
                    CurLine = CurLine + 1
                Else
                    If Trim(NoopList(1).GetColumnValue(CurLine, FltnCol)) <> "-" Then
                        tmpFkey = TurnAftFkey(NoopList(1).GetColumnValue(CurLine, FkeyCol))
                        NoopList(1).SetColumnValue CurLine, FkeyCol, tmpFkey
                        tmpTime = Left(NoopList(1).GetColumnValue(CurLine, TimeCol), 12)
                        NoopList(1).SetColumnValue CurLine, TimeCol, tmpTime
                        tmpSked = Left(NoopList(1).GetColumnValue(CurLine, SkedCol), 12)
                        If Len(tmpSked) <> 12 Then tmpSked = ""
                        NoopList(1).SetColumnValue CurLine, SkedCol, tmpSked
                        If UseFkeyTime Then tmpFkey = tmpFkey & Mid(tmpTime, 9, 4)
                        NoopList(1).SetColumnValue CurLine, RotaCol, tmpFkey
                        If NoopList(1).GetColumnValue(CurLine, PrflCol) <> "L" Then NoopList(1).SetColumnValue CurLine, PrflCol, ""
                    End If
                    NoopList(1).SetColumnValue CurLine, FltnCol, "    -"
                    NoopList(1).SetColumnValue CurLine, TifdCol, ""
                    SrvRtyp = NoopList(1).GetColumnValue(CurLine, RtypCol)
                    If (MyUpdateMode) Or ((MyFullMode) And (SrvRtyp = "J")) Then
                        tmpAdid = NoopList(1).GetColumnValue(CurLine, AdidCol)
                        Select Case tmpAdid
                            Case "D"
                                ArrRkeys = ArrRkeys & CurRkey & ","
                            Case "A"
                                DepRkeys = DepRkeys & CurRkey & ","
                            Case Else
                        End Select
                        If MyFullMode Then AppendFlights = True
                    End If
                    NoopList(1).SetColumnValue CurLine, RtypCol, ""
                    If NoopList(1).GetColumnValue(CurLine, PrflCol) <> "L" Then NoopList(1).SetColumnValue CurLine, PrflCol, ""
                    NoopList(1).SetColumnValue CurLine, ChckCol, "0"
                    NoopList(1).SetColumnValue CurLine, DoneCol, "0"
                End If
    
            Else
                If CurRkey <> "" Then
                    NxtLine = CurLine + 1
                    NoopList(1).SetColumnValue CurLine, ChckCol, "0"
                    NoopList(1).SetColumnValue CurLine, DoneCol, "0"
                    NxtRkey = NoopList(1).GetColumnValue(NxtLine, RkeyCol)
                    If CurRkey = NxtRkey Then
                        NoopList(1).SetColumnValue NxtLine, ChckCol, "0"
                        NoopList(1).SetColumnValue NxtLine, DoneCol, "0"
                        CurLine = CurLine + 1
                    End If
                End If
            End If
            CurLine = CurLine + 1
            If StopAllLoops Then MaxLine = -1
        Wend
        NoopList(1).RedrawTab
        SetProgressBar 0, 3, -1, ""
        If Not StopAllLoops Then
            If ArrRkeys <> "" Then
                ArrRkeys = Left(ArrRkeys, Len(ArrRkeys) - 1)
                AppendFlights = True
            End If
            If DepRkeys <> "" Then
                DepRkeys = Left(DepRkeys, Len(DepRkeys) - 1)
                AppendFlights = True
            End If
            If (LoopCount = 1) And (AppendFlights) Then
                LoadFlightsByFkey ArrRkeys, DepRkeys
            End If
        End If
        Me.MousePointer = 0
        If StopAllLoops Then LoopCount = 3
    Loop While (LoopCount < 2) And (AppendFlights = True)
    NoopList(1).OnVScrollTo 0
    NoopList(1).Refresh
    If Not StopAllLoops Then
        NoopList(1).Sort Trim(Str(UrnoCol)), True, True
        RightMax = NoopList(1).GetLineCount - 1
        RightLine = 0
        SetProgressBar 0, 1, RightMax, "Filtering AODB Unique Keys ..."
        While RightLine <= RightMax
            If RightLine Mod 100 = 0 Then SetProgressBar 0, 2, RightLine, ""
            CurRightUrno = Trim(NoopList(1).GetColumnValue(RightLine, UrnoCol))
            If CurRightUrno = OldRightUrno Then
                NoopList(1).DeleteLine RightLine
                RightLine = RightLine - 1
                RightMax = RightMax - 1
            End If
            RightLine = RightLine + 1
            OldRightUrno = CurRightUrno
            If StopAllLoops Then RightMax = -1
        Wend
    End If
    SetProgressBar 0, 3, -1, ""
    If Not StopAllLoops Then CreateServerRouting
End Sub

Private Sub CreateServerRouting()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CheckIt As Boolean
    Dim tmpVial As String
    Dim tmpAdid As String
    Dim tmpOrig As String
    Dim tmpDest As String
    Dim tmpStod As String
    Dim tmpStoa As String
    Dim tmpApc3 As String
    Dim tmpApc4 As String
    Dim tmpVia3 As String
    Dim tmpVia4 As String
    Dim tmpVsta As String
    Dim tmpVstd As String
    Dim tmpRout As String
    Dim tmpAct3 As String
    Dim Apc3Pos As Integer
    MaxLine = NoopList(1).GetLineCount - 1
    SetProgressBar 0, 1, MaxLine, "Building AODB Routing Info ..."
    For CurLine = 0 To MaxLine
        If StopAllLoops Then Exit For
        If CurLine Mod 100 = 0 Then
            SetProgressBar 0, 2, CurLine, ""
            NoopList(1).OnVScrollTo CurLine
            NoopList(1).Refresh
        End If
        tmpApc3 = NoopList(1).GetColumnValue(CurLine, Apc3Col)
        If tmpApc3 = "" Then
            tmpApc4 = NoopList(1).GetColumnValue(CurLine, Apc4Col)
            NoopList(1).SetColumnValue CurLine, Apc3Col, tmpApc4
        End If
        tmpVia3 = NoopList(1).GetColumnValue(CurLine, Via3Col)
        If tmpVia3 = "" Then
            tmpVia4 = NoopList(1).GetColumnValue(CurLine, Via4Col)
            NoopList(1).SetColumnValue CurLine, Via3Col, tmpVia4
        End If
        If UseRoutField Then
            tmpAdid = NoopList(1).GetColumnValue(CurLine, AdidCol)
            CheckIt = True
            Select Case tmpAdid
                Case "A"
                    tmpOrig = NoopList(1).GetColumnValue(CurLine, Apc3Col)
                    tmpDest = HomeAirport
                    tmpStod = NoopList(1).GetColumnValue(CurLine, SkedCol)
                    tmpStoa = NoopList(1).GetColumnValue(CurLine, TimeCol)
                    tmpVial = NoopList(1).GetColumnValue(CurLine, RoutCol)
                Case "D"
                    tmpOrig = HomeAirport
                    tmpDest = NoopList(1).GetColumnValue(CurLine, Apc3Col)
                    tmpStod = NoopList(1).GetColumnValue(CurLine, TimeCol)
                    tmpStoa = NoopList(1).GetColumnValue(CurLine, SkedCol)
                    tmpVial = NoopList(1).GetColumnValue(CurLine, RoutCol)
                Case Else
                    CheckIt = False
            End Select
            tmpRout = ""
            tmpVsta = ""
            tmpVstd = ""
            Apc3Pos = 2
            tmpApc3 = Trim(Mid(tmpVial, Apc3Pos, 3))
            While tmpApc3 <> ""
                If RoutWithTimes Then
                    tmpVsta = Trim(Mid(tmpVial, Apc3Pos + 7, 12))
                    tmpVstd = Trim(Mid(tmpVial, Apc3Pos + 63, 12))
                End If
                tmpRout = tmpRout & tmpApc3 & ";" & tmpVsta & ";" & tmpVstd & "|"
                Apc3Pos = Apc3Pos + 120
                tmpApc3 = Trim(Mid(tmpVial, Apc3Pos, 3))
            Wend
            If tmpRout <> "" Then tmpRout = Left(tmpRout, Len(tmpRout) - 1)
        Else
            tmpRout = ""
        End If
        If ImportAct3Act5 Then
            tmpAct3 = NoopList(1).GetColumnValue(CurLine, Act3Col)
            tmpAct3 = tmpAct3 & "/" & NoopList(1).GetColumnValue(CurLine, Act5Col)
            If tmpAct3 <> "/" Then NoopList(1).SetColumnValue CurLine, Act3Col, tmpAct3
        End If
        NoopList(1).SetColumnValue CurLine, RoutCol, tmpRout
    Next
    NoopList(1).OnVScrollTo 0
    NoopList(1).Refresh
    SetProgressBar 0, 3, -1, ""
End Sub
Private Sub CreateClientRouting()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpAdid As String
    If UseRoutField Then
        MaxLine = NoopList(0).GetLineCount - 1
        SetProgressBar 0, 1, MaxLine, "Building Import Routing Info ..."
        For CurLine = 0 To MaxLine
            If StopAllLoops Then Exit For
            If CurLine Mod 100 = 0 Then SetProgressBar 0, 2, CurLine, ""
            tmpAdid = NoopList(0).GetColumnValue(CurLine, AdidCol)
            Select Case tmpAdid
                Case "A"
                    CreateRoutArr CurLine
                Case "D"
                    CreateRoutDep CurLine
                Case Else
            End Select
        Next
        SetProgressBar 0, 3, -1, ""
    End If
End Sub

Private Sub CreateRoutDep(CurLine As Long)
    Dim tmpVial As String
    Dim tmpAdid As String
    Dim tmpOrig As String
    Dim tmpDest As String
    Dim tmpStod As String
    Dim tmpStoa As String
    Dim tmpFlda As String
    Dim tmpApc3 As String
    Dim tmpVsta As String
    Dim tmpVstd As String
    Dim tmpSect As String
    Dim tmpSked As String
    Dim tmpRout As String
    Dim LegItem As Long
    Dim BreakOut As Boolean
    If UseRoutField Then
        tmpOrig = HomeAirport
        tmpDest = NoopList(0).GetColumnValue(CurLine, Apc3Col)
        tmpStod = NoopList(0).GetColumnValue(CurLine, TimeCol)
        tmpStoa = NoopList(0).GetColumnValue(CurLine, SkedCol)
        tmpVial = NoopList(0).GetColumnValue(CurLine, RoutCol)
        tmpFlda = Left(tmpStod, 8)
        tmpRout = ""
        LegItem = 0
        BreakOut = False
        tmpSect = GetRealItem(tmpVial, LegItem, "|")
        While (tmpSect <> "") And (Not BreakOut)
            tmpApc3 = GetRealItem(tmpSect, 0, ":")
            If tmpApc3 = tmpOrig Then BreakOut = True
            LegItem = LegItem + 1
            tmpSect = GetRealItem(tmpVial, LegItem, "|")
        Wend
        If BreakOut Then
            BreakOut = False
            tmpVsta = ""
            tmpVstd = ""
            tmpStoa = ""
            If RoutWithTimes Then tmpVstd = tmpStod
            tmpSect = GetRealItem(tmpVial, LegItem, "|")
            While (tmpSect <> "") And (Not BreakOut)
                tmpApc3 = GetRealItem(tmpSect, 0, ":")
                If tmpApc3 <> tmpDest Then
                    If RoutWithTimes Then
                        tmpSked = GetRealItem(tmpSect, 1, ":")
                        tmpVsta = GetRealItem(tmpSked, 0, "-")
                        If Len(tmpVsta) >= 4 Then
                            If Len(tmpVsta) < 12 Then tmpVsta = Left(tmpVstd, 8) & tmpVsta
                            If tmpVsta < tmpVstd Then tmpVsta = CedaDateAdd(Left(tmpVsta, 8), 1) & Mid(tmpVsta, 9)
                        Else
                            tmpVsta = ""
                        End If
                        tmpVstd = GetRealItem(tmpSked, 1, "-")
                        If Len(tmpVstd) >= 4 Then
                            If Len(tmpVstd) < 12 Then tmpVstd = Left(tmpVsta, 8) & tmpVstd
                            If tmpVstd < tmpVsta Then tmpVstd = CedaDateAdd(Left(tmpVstd, 8), 1) & Mid(tmpVstd, 9)
                        Else
                            tmpVstd = ""
                        End If
                    End If
                    tmpRout = tmpRout & tmpApc3 & ";" & tmpVsta & ";" & tmpVstd & "|"
                Else
                    If RoutWithTimes Then
                        tmpSked = GetRealItem(tmpSect, 1, ":")
                        tmpStoa = GetRealItem(tmpSked, 0, "-")
                        If Len(tmpStoa) >= 4 Then
                            If Len(tmpStoa) < 12 Then tmpStoa = Left(tmpVstd, 8) & tmpStoa
                            If tmpStoa < tmpVstd Then tmpStoa = CedaDateAdd(Left(tmpStoa, 8), 1) & Mid(tmpStoa, 9)
                        Else
                            tmpStoa = ""
                        End If
                    End If
                    BreakOut = True
                End If
                LegItem = LegItem + 1
                tmpSect = GetRealItem(tmpVial, LegItem, "|")
            Wend
        End If
        If tmpRout <> "" Then tmpRout = Left(tmpRout, Len(tmpRout) - 1)
        If UseFldaField Then
            tmpFlda = Trim(tmpFlda)
            If Len(tmpFlda) = 8 Then NoopList(0).SetColumnValue CurLine, FldaCol, tmpFlda
        End If
        tmpStoa = Trim(tmpStoa)
        If Len(tmpStoa) = 12 Then NoopList(0).SetColumnValue CurLine, SkedCol, tmpStoa
        NoopList(0).SetColumnValue CurLine, RoutCol, tmpRout
    End If
End Sub

Private Sub CreateRoutArr(CurLine As Long)
    Dim tmpVial As String
    Dim tmpAdid As String
    Dim tmpOrig As String
    Dim tmpDest As String
    Dim tmpStod As String
    Dim tmpStoa As String
    Dim tmpFlda As String
    Dim tmpApc3 As String
    Dim tmpVsta As String
    Dim tmpVstd As String
    Dim tmpSect As String
    Dim tmpSked As String
    Dim tmpRout As String
    Dim LegItem As Long
    Dim BreakOut As Boolean
    If UseRoutField Then
        tmpDest = HomeAirport
        tmpOrig = NoopList(0).GetColumnValue(CurLine, Apc3Col)
        tmpDest = HomeAirport
        tmpStoa = NoopList(0).GetColumnValue(CurLine, TimeCol)
        tmpStod = NoopList(0).GetColumnValue(CurLine, SkedCol)
        tmpVial = NoopList(0).GetColumnValue(CurLine, RoutCol)
        tmpRout = ""
        LegItem = ItemCount(tmpVial, "|") - 1
        BreakOut = False
        tmpSect = GetRealItem(tmpVial, LegItem, "|")
        While (tmpSect <> "") And (Not BreakOut)
            tmpApc3 = GetRealItem(tmpSect, 0, ":")
            If tmpApc3 = tmpDest Then BreakOut = True
            LegItem = LegItem - 1
            tmpSect = GetRealItem(tmpVial, LegItem, "|")
        Wend
        If BreakOut Then
            BreakOut = False
            tmpVsta = ""
            tmpVstd = ""
            tmpStod = ""
            If RoutWithTimes Then tmpVsta = tmpStoa
            tmpSect = GetRealItem(tmpVial, LegItem, "|")
            While (tmpSect <> "") And (Not BreakOut)
                tmpApc3 = GetRealItem(tmpSect, 0, ":")
                If tmpApc3 <> tmpOrig Then
                    If RoutWithTimes Then
                        tmpSked = GetRealItem(tmpSect, 1, ":")
                        tmpVstd = GetRealItem(tmpSked, 1, "-")
                        If Len(tmpVstd) >= 4 Then
                            If Len(tmpVstd) < 12 Then tmpVstd = Left(tmpVsta, 8) & tmpVstd
                            If tmpVstd > tmpVsta Then tmpVstd = CedaDateAdd(Left(tmpVstd, 8), -1) & Mid(tmpVstd, 9)
                        Else
                            tmpVstd = ""
                        End If
                        tmpVsta = GetRealItem(tmpSked, 0, "-")
                        If Len(tmpVsta) >= 4 Then
                            If Len(tmpVsta) < 12 Then tmpVsta = Left(tmpVstd, 8) & tmpVsta
                            If tmpVsta > tmpVstd Then tmpVsta = CedaDateAdd(Left(tmpVsta, 8), -1) & Mid(tmpVsta, 9)
                        Else
                            tmpVsta = ""
                        End If
                    End If
                    tmpRout = tmpApc3 & ";" & tmpVsta & ";" & tmpVstd & "|" & tmpRout
                Else
                    If RoutWithTimes Then
                        tmpSked = GetRealItem(tmpSect, 1, ":")
                        tmpStod = GetRealItem(tmpSked, 1, "-")
                        If Len(tmpStod) >= 4 Then
                            If Len(tmpStod) < 12 Then tmpStod = Left(tmpVsta, 8) & tmpStod
                            If tmpStod > tmpVsta Then tmpStod = CedaDateAdd(Left(tmpStod, 8), -1) & Mid(tmpStod, 9)
                        Else
                            tmpStod = ""
                        End If
                    End If
                    BreakOut = True
                End If
                LegItem = LegItem - 1
                tmpSect = GetRealItem(tmpVial, LegItem, "|")
            Wend
        End If
        If tmpRout <> "" Then tmpRout = Left(tmpRout, Len(tmpRout) - 1)
        tmpStod = Trim(tmpStod)
        If Len(tmpStod) = 12 Then
            NoopList(0).SetColumnValue CurLine, SkedCol, tmpStod
            If UseFldaField Then
                tmpFlda = Left(tmpStod, 8)
                NoopList(0).SetColumnValue CurLine, FldaCol, tmpFlda
            End If
        End If
        NoopList(0).SetColumnValue CurLine, RoutCol, tmpRout
    End If
End Sub

Private Sub ToggleUtcToLocal()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurSked As String
    Dim NewSked As String
    Dim CurDate As String
    Dim NewDate As String
    Dim CurFkey As String
'    MaxLine = NoopList(1).GetLineCount - 1
'    For CurLine = 0 To MaxLine
'        CurSked = NoopList(1).GetColumnValue(CurLine, TimeCol)
'        NewSked = ApcUtcToLocal(HomeAirport, CurSked)
'        NoopList(1).SetColumnValue CurLine, TimeCol, NewSked
'        CurDate = Left(CurSked, 8)
'        NewDate = Left(NewSked, 8)
'        If CurDate <> NewDate Then
'            CurFkey = NoopList(1).GetColumnValue(CurLine, FkeyCol)
'            Mid(CurFkey, 10, 8) = NewDate
'            NoopList(1).SetColumnValue CurLine, FkeyCol, CurFkey
'        End If
'    Next
    
End Sub
Private Sub ClearOutEmptyLines(Index As Integer, ColNo As Long, ClearLabels As Boolean)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CntLine As Long
    Dim tmpData As String
    Dim i As Integer
    StatusBar.Panels(2).Text = "Clear empty lines. Loop" & Str(Index)
    MaxLine = NoopList(Index).GetLineCount - 1
    CurLine = 0
    CntLine = 0
    While CurLine <= MaxLine
        CntLine = CntLine + 1
        If CntLine Mod 100 = 0 Then
            StatusBar.Panels(2).Text = "Clear empty lines. Loop" & Str(Index) & " Line" & Str(CntLine)
        End If
        'Adid must be filled
        tmpData = Trim(NoopList(Index).GetColumnValue(CurLine, ColNo))
        If tmpData = "" Then
            NoopList(Index).DeleteLine CurLine
            CurLine = CurLine - 1
            MaxLine = MaxLine - 1
        Else
            'NoopList(Index).SetLineColor CurLine, vbBlack, vbWhite
        End If
        CurLine = CurLine + 1
    Wend
    NoopList(Index).RedrawTab
    If ClearLabels Then
        For i = 1 To 4
            If Index = 0 Then
                CliFkeyCnt(i).Caption = "0"
            Else
                SrvFkeyCnt(i).Caption = "0"
            End If
        Next
    End If
    StatusBar.Panels(2).Text = "Ready."
End Sub

Private Sub CompareFlightLists(SortCol As Long, CheckCol As Long)
    Dim LeftLine As Long
    Dim RightLine As Long
    Dim RotaLine As Long
    Dim FtypVal As String
    Dim RotaItem As String
    Dim tmpFtyp As String
    Dim tmpData As String
    Dim tmpPrfl As String
    Dim OldPrfl As String
    Dim tmpFlda As String
    Dim LeftAct3 As String
    Dim LeftAct5 As String
    Dim RightAct3 As String
    Dim RightAct5 As String
    Dim LoadBeginDate As String
    Dim LoadEndDate As String
    Dim LeftMax As Long
    Dim RightMax As Long
    Dim useMax As Long
    Dim FullLeftVal As String
    Dim FullRightVal As String
    Dim LeftVal As String
    Dim RightVal As String
    Dim LeftValues As String
    Dim RightValues As String
    Dim OldLeftVal(8) As String
    Dim OldLeftLin(8) As Long
    Dim OldRightVal(8) As String
    Dim OldRightLin(8) As Long
    Dim OldLine As Long
    Dim OldLeftIdx As Integer
    Dim OldRightIdx As Integer
    Dim OldRightUrno As String
    Dim CurRightUrno As String
    Dim CompValues As Boolean
    Dim GreenForeColor As Long
    Dim GreenBackColor As Long
    Dim RedForeColor As Long
    Dim RedBackColor As Long
    Dim LeftYelForeColor As Long
    Dim LeftYelBackColor As Long
    Dim RightYelForeColor As Long
    Dim RightYelBackColor As Long
    Dim WhiteForeColor As Long
    Dim WhiteBackColor As Long
    Dim GrayForeColor As Long
    Dim GrayBackColor As Long
    Dim CurLeftAdid As String
    Dim CurRightAdid As String
    Dim DoubledLeft As Boolean
    Dim DoubledRight As Boolean
    Dim IsInList As Boolean
    Dim NotInList As String
    Dim MsgTxt As String
    Dim tmpLook1 As String
    Dim tmpLook2 As String
    Dim IsOk As Boolean
    

    Me.MousePointer = 11
    EmptyLine = ",,,,,,,,,,,,,,,,,,,,,0,0,"
    NotInList = ""
    ClearOutEmptyLines 0, AdidCol, True
    ClearOutEmptyLines 1, AdidCol, True
    StatusBar.Panels(2).Text = "Sorting the Flight Lists."
    
    NoopList(0).Sort Trim(Str(SortCol)), True, True
    NoopList(1).Sort Trim(Str(SortCol)), True, True
    
    NoopList(0).Refresh
    NoopList(1).Refresh
    
    LoadBeginDate = CurBegin.Tag
    LoadEndDate = CurEnd.Tag
    GreenBackColor = CliFkeyCnt(4).BackColor
    GreenForeColor = CliFkeyCnt(4).ForeColor
    RedBackColor = SrvFkeyCnt(4).BackColor
    RedForeColor = SrvFkeyCnt(4).ForeColor
    LeftYelBackColor = CliFkeyCnt(5).BackColor
    LeftYelForeColor = CliFkeyCnt(5).ForeColor
    RightYelBackColor = SrvFkeyCnt(5).BackColor
    RightYelForeColor = SrvFkeyCnt(5).ForeColor
    WhiteForeColor = CliFkeyCnt(3).ForeColor
    WhiteBackColor = CliFkeyCnt(3).BackColor
    GrayForeColor = CliFkeyCnt(6).ForeColor
    GrayBackColor = CliFkeyCnt(6).BackColor
    For OldLeftIdx = 0 To 8
        OldLeftVal(OldLeftIdx) = ""
        OldRightVal(OldLeftIdx) = ""
        OldLeftLin(OldLeftIdx) = -1
        OldRightLin(OldLeftIdx) = -1
    Next
    StatusBar.Panels(2).Text = "Comparing the Flight Lists ..."
    NoopList(0).OnVScrollTo 0
    NoopList(0).Refresh
    NoopList(1).OnVScrollTo 0
    NoopList(1).Refresh
    LeftLine = 0
    RightLine = 0
    LeftMax = NoopList(0).GetLineCount - 1
    RightMax = NoopList(1).GetLineCount - 1
    If RightMax > LeftMax Then useMax = RightMax Else useMax = LeftMax
    While LeftLine <= useMax
        If LeftLine Mod 250 = 0 Then SetProgressBar 0, 0, useMax, Str(LeftLine)
        If (LeftLine Mod 100) = 0 Then
            NoopList(0).OnVScrollTo LeftLine
            NoopList(0).Refresh
            If RightLine > 0 Then
                NoopList(1).OnVScrollTo RightLine
                NoopList(1).Refresh
            End If
        End If
        
        FullLeftVal = ""
        FullRightVal = ""
        OldLeftIdx = -1
        OldRightIdx = -1
        
        FullLeftVal = Trim(NoopList(0).GetColumnValue(LeftLine, CheckCol))
        LeftVal = Left(FullLeftVal, 18)
        CurLeftAdid = Trim(NoopList(0).GetColumnValue(LeftLine, AdidCol))
        Select Case CurLeftAdid
            Case "A"
                OldLeftIdx = 0
            Case "D"
                OldLeftIdx = 1
            Case Else
                OldLeftIdx = -1
        End Select
        DoubledLeft = False
        If LeftVal <> "" Then
            If OldLeftIdx >= 0 Then
                If OldLeftVal(OldLeftIdx) = FullLeftVal Then
                    NoopList(0).SetLineColor LeftLine - 1, LeftYelForeColor, LeftYelBackColor
                    OldPrfl = NoopList(0).GetColumnValue(LeftLine - 1, PrflCol)
                    NoopList(0).SetColumnValue LeftLine - 1, 0, OldPrfl & "!"
                    OldPrfl = NoopList(0).GetColumnValue(LeftLine - 1, RkeyCol)
                    NoopList(0).SetColumnValue LeftLine - 1, RkeyCol, "!" & OldPrfl & "!"
                    NoopList(1).InsertTextLineAt LeftLine - 1, EmptyLine, False
                    NoopList(1).SetLineColor LeftLine - 1, LeftYelForeColor, LeftYelBackColor
                    RightMax = RightMax + 1
                    FullRightVal = ""
                    DoubledLeft = True
                End If
            End If
        End If
        If Not DoubledLeft Then
            FullRightVal = Trim(NoopList(1).GetColumnValue(RightLine, CheckCol))
            RightVal = Left(FullRightVal, 18)
            CurRightAdid = Trim(NoopList(1).GetColumnValue(RightLine, AdidCol))
            Select Case CurRightAdid
                Case "A"
                    OldRightIdx = 0
                Case "D"
                    OldRightIdx = 1
                Case Else
                    OldRightIdx = -1
            End Select
            DoubledRight = False
            If RightVal <> "" Then
                If OldRightIdx >= 0 Then
                    If OldRightVal(OldRightIdx) = FullRightVal Then
                        NoopList(0).InsertTextLineAt LeftLine, EmptyLine, False
                        NoopList(0).SetLineColor LeftLine, RightYelForeColor, RightYelBackColor
                        NoopList(1).SetLineColor LeftLine, RightYelForeColor, RightYelBackColor
                        LeftMax = LeftMax + 1
                        FullLeftVal = ""
                        DoubledRight = True
                    End If
                End If
            End If
        End If
            
        If (Not DoubledLeft) And (Not DoubledRight) Then
            If (FullLeftVal <> "") And (FullRightVal <> "") Then
                If FullLeftVal <> FullRightVal Then
                    If FullRightVal > FullLeftVal Then
                        'Flight in File but not on Server (Green Color)
                        tmpPrfl = NoopList(0).GetColumnValue(LeftLine, PrflCol)
                        If tmpPrfl = "D" Then
                            NoopList(0).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                            NoopList(1).InsertTextLineAt LeftLine, EmptyLine, False
                            NoopList(1).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                        Else
                            NoopList(0).SetLineColor LeftLine, GreenForeColor, GreenBackColor
                            NoopList(1).InsertTextLineAt LeftLine, EmptyLine, False
                            NoopList(1).SetLineColor LeftLine, GreenForeColor, GreenBackColor
                        End If
                        RightMax = RightMax + 1
                        FullRightVal = ""
                    ElseIf FullLeftVal > FullRightVal Then
                        'Flight on Server but not in File (Red Color)
                        FtypVal = NoopList(1).GetColumnValue(LeftLine, FtypCol)
                        tmpPrfl = NoopList(1).GetColumnValue(LeftLine, PrflCol)
                        If UseFldaField Then tmpFlda = Mid(RightVal, 10, 8)
                        IsInList = CheckAlc3InList(RightVal)
                        NoopList(0).InsertTextLineAt LeftLine, EmptyLine, False
                        If (tmpPrfl = "L") And ((tmpFlda < LoadBeginDate) Or (tmpFlda > LoadEndDate)) Then
                            NoopList(0).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                            NoopList(1).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                        Else
                            If (IsInList) And (FtypVal <> "N") And (FtypVal <> "X") Then
                                NoopList(0).SetLineColor LeftLine, RedForeColor, RedBackColor
                                NoopList(1).SetLineColor LeftLine, RedForeColor, RedBackColor
                            Else
                                If IsInList Then
                                    NoopList(0).SetLineColor LeftLine, RedForeColor, RedBackColor
                                Else
                                    NoopList(0).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                                End If
                                NoopList(1).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                            End If
                            If (Not IsInList) And (InStr(NotInList, Left(RightVal, 3)) = 0) Then
                                NotInList = NotInList & Left(RightVal, 3) & vbNewLine
                            End If
                        End If
                        LeftMax = LeftMax + 1
                        FullLeftVal = ""
                    End If
                    If RightMax > LeftMax Then useMax = RightMax Else useMax = LeftMax
                Else
                    If ImportAct3Act5 Then
                        LeftAct3 = NoopList(0).GetColumnValue(LeftLine, Act3Col)
                        LeftAct5 = GetItem(LeftAct3, 2, "/")
                        LeftAct3 = GetItem(LeftAct3, 1, "/")
                        RightAct3 = NoopList(1).GetColumnValue(LeftLine, Act3Col)
                        RightAct5 = GetItem(RightAct3, 2, "/")
                        RightAct3 = GetItem(RightAct3, 1, "/")
                        If (LeftAct3 = RightAct3) And (LeftAct5 <> RightAct5) Then
                            tmpLook1 = RightAct5
                            tmpLook2 = ""
                            IsOk = Validation.BasicCheck(2, "ACT5", tmpLook1, "ACT3", tmpLook2)
                            If Not IsOk Then RightAct5 = ""
                            If RightAct5 <> "" Then
                                LeftAct3 = LeftAct3 & "/" & RightAct5
                                NoopList(0).SetColumnValue LeftLine, Act3Col, LeftAct3
                            End If
                        ElseIf (LeftAct5 = RightAct5) And (LeftAct3 <> RightAct3) Then
                            tmpLook1 = RightAct3
                            tmpLook2 = ""
                            IsOk = Validation.BasicCheck(2, "ACT3", tmpLook1, "ACT5", tmpLook2)
                            If Not IsOk Then RightAct3 = ""
                            If RightAct3 <> "" Then
                                LeftAct3 = RightAct3 & "/" & RightAct5
                                NoopList(0).SetColumnValue LeftLine, Act3Col, LeftAct3
                            End If
                        End If
                    End If
                End If
            Else
                If LeftVal <> "" Then
                    NoopList(0).SetLineColor LeftLine, GreenForeColor, GreenBackColor
                    NoopList(1).InsertTextLineAt LeftLine, EmptyLine, False
                    NoopList(1).SetLineColor LeftLine, GreenForeColor, GreenBackColor
                    FullRightVal = ""
                    RightMax = RightMax + 1
                End If
                If RightVal <> "" Then
                    IsInList = CheckAlc3InList(RightVal)
                    FtypVal = NoopList(1).GetColumnValue(LeftLine, FtypCol)
                    tmpPrfl = NoopList(1).GetColumnValue(LeftLine, PrflCol)
                    If UseFldaField Then tmpFlda = Mid(RightVal, 10, 8)
                    NoopList(0).InsertTextLineAt LeftLine, EmptyLine, False
                    If (tmpPrfl = "L") And ((tmpFlda < LoadBeginDate) Or (tmpFlda > LoadEndDate)) Then
                        NoopList(0).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                        NoopList(1).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                    Else
                        If (IsInList) And (FtypVal <> "N") And (FtypVal <> "X") Then
                            NoopList(0).SetLineColor LeftLine, RedForeColor, RedBackColor
                            NoopList(1).SetLineColor LeftLine, RedForeColor, RedBackColor
                        Else
                            If IsInList Then
                                NoopList(0).SetLineColor LeftLine, RedForeColor, RedBackColor
                            Else
                                NoopList(0).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                            End If
                            NoopList(1).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                        End If
                    End If
                    If (Not IsInList) And (InStr(NotInList, Left(RightVal, 3)) = 0) Then
                        NotInList = NotInList & Left(RightVal, 3) & vbNewLine
                    End If
                    LeftMax = LeftMax + 1
                    FullLeftVal = ""
                End If
            End If
        End If
        If OldLeftIdx >= 0 Then
            OldLeftVal(OldLeftIdx) = FullLeftVal
            OldLeftLin(OldLeftIdx) = LeftLine
        End If
        If OldRightIdx >= 0 Then
            OldRightVal(OldRightIdx) = FullRightVal
            OldRightLin(OldRightIdx) = LeftLine
        End If
        RightLine = RightLine + 1
        LeftLine = LeftLine + 1
        If RightMax > LeftMax Then useMax = RightMax Else useMax = LeftMax
        If StopAllLoops Then useMax = -1
    Wend
    NoopList(0).RedrawTab
    NoopList(1).RedrawTab
    Me.Refresh
    SetProgressBar 0, 3, 0, ""
    
    If Not StopAllLoops Then
        If (ShrinkNewDel) And (chkWork2(0).Value = 1) Then ShrinkInsertDeleteToUpdate
        
        If NoopList(0).GetLineCount <> NoopList(1).GetLineCount Then
            'Happened sometimes with UfisCom.ocx
            'Didn't occure since we use cdrhdl
            MsgTxt = "The lists cannot be synchronized !!" & vbNewLine
            MsgTxt = MsgTxt & "Please stop the application," & vbNewLine & "start up and try it again."
            If MyMsgBox.CallAskUser(0, 0, 0, "Application Ooops Control", MsgTxt, "stop", "", UserAnswer) > 0 Then DoNothing
            SeriousError = True
            AdjustScrollMaster -1, -1, "", False
        Else
            'If (MyUpdateMode) And (StartedAsCompareTool) Then AdjustUpdateColors
            AdjustUpdateColors
            SeriousError = False
        End If
        If Not SeriousError Then
            AdjustCounterCaption 1, 6, 0, 1, True
            If DataSystemType = "OAFOS" Then
                CreateRegnView
            End If
            CheckRotationDetails
            If Not StopAllLoops Then
                Me.MousePointer = 0
                AdjustScrollMaster -1, -1, "", False
                If DataSystemType <> "OAFOS" Then
                    CreateDistinctFlno "-ALL-"
                Else
                    CreateDistinctRegn "-ALL-"
                End If
                LeftMax = NoopList(0).GetLineCount
                For useMax = 1 To 32
                    NoopList(0).InsertTextLine "-,,,,,,,,,,,,,,,,,,", False
                    NoopList(1).InsertTextLine "-,,,,,,,,,,,,,,,,,,", False
                    NoopList(0).SetLineColor LeftMax, vbBlack, EmptyLineColor
                    NoopList(1).SetLineColor LeftMax, vbBlack, EmptyLineColor
                    LeftMax = LeftMax + 1
                Next
                NoopList(2).SetCurrentSelection NoopList(2).GetCurrentSelected
                Me.Refresh
                If NotInList <> "" Then
                    MsgTxt = "Attention:" & vbNewLine & "Some of the co-operating airlines" & vbNewLine
                    MsgTxt = MsgTxt & "could not be found in the left side." & vbNewLine
                    MsgTxt = MsgTxt & "This concerns:" & vbNewLine & NotInList
                    If FullMode Then
                        MsgTxt = MsgTxt & "It's recommended to load these" & vbNewLine
                        MsgTxt = MsgTxt & "airlines into your filter and run" & vbNewLine
                        MsgTxt = MsgTxt & "the full validation again." & vbNewLine
                    End If
                    If MyMsgBox.CallAskUser(0, 0, 0, "Combined Airlines", MsgTxt, "infomsg", "", UserAnswer) > 0 Then DoNothing
                    Me.Refresh
                End If
            End If
        End If
    End If
End Sub

Private Sub ShrinkInsertDeleteToUpdate()
    Dim CurLine As Long
    Dim NxtLine As Long
    Dim MaxLine As Long
    Dim DelLeft As Long
    Dim DelRight As Long
    Dim CurRedLine As Long
    Dim CurGreenLine As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim NxtForeColor As Long
    Dim NxtBackColor As Long
    Dim CliFullFkey As String
    Dim SrvFullFkey As String
    Dim CliAdid As String
    Dim SrvAdid As String
    
    CurRedLine = SrvFkeyCnt(4).BackColor
    CurGreenLine = CliFkeyCnt(4).BackColor
    
    MaxLine = NoopList(0).GetLineCount - 1
    CurLine = 0
    DelLeft = -1
    While CurLine < MaxLine
        NoopList(0).GetLineColor CurLine, CurForeColor, CurBackColor
        Select Case CurBackColor
            Case CurRedLine
                NxtLine = CurLine + 1
                NoopList(0).GetLineColor NxtLine, NxtForeColor, NxtBackColor
                If NxtBackColor = CurGreenLine Then
                    SrvFullFkey = NoopList(1).GetColumnValue(CurLine, RotaCol)
                    CliFullFkey = NoopList(0).GetColumnValue(NxtLine, RotaCol)
                    If Left(CliFullFkey, 18) = Left(SrvFullFkey, 18) Then
                        DelLeft = CurLine
                        DelRight = NxtLine
                    ElseIf chkWork2(1).Value = 1 Then
                        CliAdid = Mid(CliFullFkey, 18, 1)
                        SrvAdid = Mid(SrvFullFkey, 18, 1)
                        If CliAdid = SrvAdid Then
                            If Left(CliFullFkey, 9) = Left(SrvFullFkey, 9) Then
                                DelLeft = CurLine
                                DelRight = NxtLine
                            End If
                        End If
                    End If
                End If
            Case CurGreenLine
                NxtLine = CurLine + 1
                NoopList(0).GetLineColor NxtLine, NxtForeColor, NxtBackColor
                If NxtBackColor = CurRedLine Then
                    CliFullFkey = NoopList(0).GetColumnValue(CurLine, RotaCol)
                    SrvFullFkey = NoopList(1).GetColumnValue(NxtLine, RotaCol)
                    If Left(CliFullFkey, 18) = Left(SrvFullFkey, 18) Then
                        DelLeft = NxtLine
                        DelRight = CurLine
                    ElseIf chkWork2(1).Value = 1 Then
                        CliAdid = Mid(CliFullFkey, 18, 1)
                        SrvAdid = Mid(SrvFullFkey, 18, 1)
                        If CliAdid = SrvAdid Then
                            If Left(CliFullFkey, 9) = Left(SrvFullFkey, 9) Then
                                DelLeft = NxtLine
                                DelRight = CurLine
                            End If
                        End If
                    End If
                End If
            Case Else
        End Select
        If DelLeft >= 0 Then
            NoopList(0).DeleteLine DelLeft
            NoopList(1).DeleteLine DelRight
            NoopList(0).SetLineColor CurLine, vbBlack, vbWhite
            NoopList(1).SetLineColor CurLine, vbBlack, vbWhite
            CurLine = CurLine - 1
            MaxLine = MaxLine - 1
            DelLeft = -1
        End If
        CurLine = CurLine + 1
    Wend
End Sub
Private Function CheckAlc3InList(UsedFkey As String) As Boolean
    Dim Max As Long
    Dim i As Long
    Dim tmpAlc3 As String
    Dim IsInList As Boolean
    IsInList = True
    tmpAlc3 = Trim(Left(UsedFkey, 3))
    If Len(tmpAlc3) = 3 Then
        IsInList = False
        Max = AlcTabCombo(1).GetLineCount - 1
        For i = 0 To Max
            If (Trim(AlcTabCombo(1).GetColumnValue(i, 0)) = tmpAlc3) Or (Trim(AlcTabCombo(1).GetColumnValue(i, 1)) = tmpAlc3) Then
                IsInList = True
                Exit For
            End If
        Next
    End If
    CheckAlc3InList = IsInList
End Function
Private Sub CreateRegnView()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim RegnKey As String
    Dim CurFtyp As String
    MaxLine = NoopList(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        CurFtyp = NoopList(0).GetColumnValue(CurLine, FtypCol)
        If (CurFtyp <> "X") And (CurFtyp <> "N") Then
            RegnKey = NoopList(0).GetColumnValue(CurLine, RegnCol)
            RegnKey = Left(RegnKey & "ZZZZZZZZZZZZ", 12)
            RegnKey = RegnKey & NoopList(0).GetColumnValue(CurLine, TimeCol)
            RegnKey = RegnKey & "," & Right("000000" & Trim(Str(CurLine)), 6)
        Else
            RegnKey = ""
            'NoopList(0).SetColumnValue CurLine, RegnCol, ""
        End If
        NoopList(0).SetColumnValue CurLine, RemaCol, RegnKey
        NoopList(1).SetColumnValue CurLine, RemaCol, RegnKey
    Next
    NoopList(0).Sort RemaCol, True, True
    NoopList(1).Sort RemaCol, True, True
End Sub

Private Sub AdjustUpdateColors()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim i As Integer
    Dim tmpCode As String
    Dim tmpFtyp As String
    Dim tmpFkey As String
    Dim CliTime As String
    Dim chkTime As String
    Dim SrvTime As String
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    chkTime = GetImportCheckTime
    MaxLine = NoopList(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpCode = Trim(NoopList(0).GetColumnValue(CurLine, PrflCol))
        If tmpCode = "" Then
            tmpCode = NoopList(1).GetColumnValue(CurLine, PrflCol)
        End If
        tmpFtyp = NoopList(1).GetColumnValue(CurLine, FtypCol)
        tmpFkey = NoopList(1).GetColumnValue(CurLine, FkeyCol)
        CliTime = NoopList(0).GetColumnValue(CurLine, TimeCol)
        'SrvTime = NoopList(1).GetColumnValue(CurLine, TimeCol)
        NoopList(0).GetLineColor CurLine, CurForeColor, CurBackColor
        If (CurBackColor <> SrvFkeyCnt(5).BackColor) And (CurBackColor <> CliFkeyCnt(5).BackColor) Then
            Select Case tmpCode
                Case "D"
                    If (tmpFkey <> "") And (InStr("XN", tmpFtyp) = 0) Then
                        NoopList(0).SetLineColor CurLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                        NoopList(1).SetLineColor CurLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                        NoopList(1).SetColumnValue CurLine, PrflCol, "D"
                    Else
                        NoopList(0).SetLineColor CurLine, SrvFkeyCnt(6).ForeColor, SrvFkeyCnt(6).BackColor
                        NoopList(1).SetLineColor CurLine, SrvFkeyCnt(6).ForeColor, SrvFkeyCnt(6).BackColor
                        NoopList(1).SetColumnValue CurLine, PrflCol, "-"
                    End If
                Case "N"
                    If tmpFkey <> "" Then
                        If CurBackColor = CliFkeyCnt(3).BackColor Then
                            NoopList(0).SetLineColor CurLine, CliFkeyCnt(3).ForeColor, CliFkeyCnt(3).BackColor
                            NoopList(1).SetLineColor CurLine, SrvFkeyCnt(3).ForeColor, SrvFkeyCnt(3).BackColor
                            NoopList(1).SetColumnValue CurLine, PrflCol, "U"
                        Else
                            NoopList(0).SetLineColor CurLine, CliFkeyCnt(6).ForeColor, CliFkeyCnt(6).BackColor
                            NoopList(1).SetLineColor CurLine, SrvFkeyCnt(6).ForeColor, SrvFkeyCnt(6).BackColor
                            NoopList(1).SetColumnValue CurLine, PrflCol, "-"
                        End If
                    Else
                        NoopList(0).SetLineColor CurLine, CliFkeyCnt(4).ForeColor, CliFkeyCnt(4).BackColor
                        NoopList(1).SetLineColor CurLine, CliFkeyCnt(4).ForeColor, CliFkeyCnt(4).BackColor
                        NoopList(1).SetColumnValue CurLine, PrflCol, "I"
                    End If
                Case "L"    'Loaded Rotation Link
                    NoopList(0).SetLineColor CurLine, SrvFkeyCnt(6).ForeColor, SrvFkeyCnt(6).BackColor
                    NoopList(1).SetLineColor CurLine, SrvFkeyCnt(6).ForeColor, SrvFkeyCnt(6).BackColor
                    NoopList(0).SetColumnValue CurLine, PrflCol, "-"
                Case Else
            End Select
        End If
    Next
    NoopList(0).Refresh
    NoopList(1).Refresh
End Sub
Private Function CompareValues(LineNo As Long, ColNo As Long, ShowDiff As Boolean, RetVal As Boolean, IgnoreData As String, UpdateEmpty As Boolean) As Boolean
    Dim ShowCellObj As Boolean
    Dim LeftItem As String
    Dim RightItem As String
    If NoopList(0).GetColumnValue(LineNo, ColNo) <> NoopList(1).GetColumnValue(LineNo, ColNo) Then
        CompareValues = False
        LeftItem = NoopList(0).GetColumnValue(LineNo, ColNo)
        If (LeftItem <> "") Or (UpdateEmpty) Then
            ShowCellObj = ShowDiff
            If IgnoreData <> "" Then
                RightItem = NoopList(1).GetColumnValue(LineNo, ColNo)
                If LeftItem = GetItem(IgnoreData, 1, ",") And RightItem = GetItem(IgnoreData, 2, ",") Then
                    ShowCellObj = False
                    CompareValues = RetVal
                End If
            End If
        Else
            CompareValues = RetVal
        End If
        If ShowCellObj Then
            NoopList(0).SetDecorationObject LineNo, ColNo, "CellDiff"
            NoopList(1).SetDecorationObject LineNo, ColNo, "CellDiff"
        End If
    Else
        CompareValues = RetVal
    End If
End Function

Private Function CreateFlightLists() As Boolean
    Dim FilterCnt As Long
    Dim MsgTxt As String
    Dim tmpDat As String
    Dim tmpAlc As String
    Dim tmpAlc3 As String
    Dim ChkAlc As String
    Dim tmpBeginDay As String
    Dim tmpEndDay As String
    Dim iLen As Integer
    Dim ReqVal As Integer
    Dim ErrCnt As Integer
    Dim LineNo As Long
    Dim UsedAlc3Filter As String
    Dim Alc3Fields As String
    Dim ItemNbr As Integer
    Dim i As Integer
    AlcTabCombo(0).ComboResetContent "AirlCodes"
    AlcTabCombo(1).ResetContent
    RegnTabCombo(0).ComboResetContent "RegnCodes"
    RegnTabCombo(1).ResetContent
    FlnoTabCombo(0).ComboResetContent "RegnCodes"
    FlnoTabCombo(1).ResetContent
    UfisTools.BlackBox(0).ResetContent
    NoopList(0).ResetContent
    NoopList(1).ResetContent
    NoopList(2).ResetContent
    NoopList(0).RedrawTab
    NoopList(1).RedrawTab
    NoopList(2).RedrawTab
    For i = 0 To 8
        RotaJoinCnt(i) = 0
    Next
    Me.Refresh
    StatusBar.Panels(2).Text = "Preparing the Airline Filters"
    ErrCnt = 0
    FilterCnt = 0
    UsedAlc3Filter = ""
    If NewLayoutType Then
        'Determin Airline Fields
        Alc3Fields = ""
        If Not NoopCheckDoubles Then
            ItemNbr = GetItemNo(myViewFieldList, "FLCA") + 2
            If ItemNbr > 2 Then Alc3Fields = Alc3Fields & Trim(Str(ItemNbr)) & ","
            ItemNbr = GetItemNo(myViewFieldList, "FLCD") + 2
            If ItemNbr > 2 Then Alc3Fields = Alc3Fields & Trim(Str(ItemNbr)) & ","
            If Alc3Fields = "" Then
                ItemNbr = GetItemNo(MainFieldList, "FLCB") + 2
                If ItemNbr > 2 Then Alc3Fields = Alc3Fields & Trim(Str(ItemNbr)) & ","
            End If
        Else
            ItemNbr = GetItemNo(MainFieldList, "FLCA") + 2
            If ItemNbr > 2 Then Alc3Fields = Alc3Fields & Trim(Str(ItemNbr)) & ","
            ItemNbr = GetItemNo(MainFieldList, "FLCD") + 2
            If ItemNbr > 2 Then Alc3Fields = Alc3Fields & Trim(Str(ItemNbr)) & ","
            If Alc3Fields = "" Then
                ItemNbr = GetItemNo(MainFieldList, "FLCB") + 2
                If ItemNbr > 2 Then Alc3Fields = Alc3Fields & Trim(Str(ItemNbr)) & ","
            End If
        End If
        If Alc3Fields <> "" Then
            Alc3Fields = Left(Alc3Fields, Len(Alc3Fields) - 1)
            FilterCnt = Val(DistinctBasicData("INIT", Alc3Fields))
        End If
    Else
        If Not StartedAsCompareTool Then
            FilterCnt = Val(DistinctBasicData("INIT", "3,5"))
        Else
            FilterCnt = Val(DistinctBasicData("INIT", "5,7"))
        End If
    End If
    If (FilterCnt > 0) Or (Not CedaIsConnected) Then
        Me.MousePointer = 11
        For LineNo = 1 To FilterCnt
            tmpAlc3 = DistinctBasicData("NEXT", tmpAlc)
            iLen = Len(tmpAlc3)
            If iLen = 3 Then
                AlcTabCombo(1).InsertTextLine tmpAlc, False
                UsedAlc3Filter = UsedAlc3Filter + "'" + tmpAlc3 + "',"
            Else
                MsgTxt = "Can't determine the airline's 2LC/3LC of: " & tmpAlc
                If MyMsgBox.CallAskUser(0, 0, 0, "Record Filter", MsgTxt, "infomsg", "", UserAnswer) > 0 Then DoNothing
                Me.Refresh
                ErrCnt = ErrCnt + 1
            End If
        Next
        FilterCnt = Val(DistinctBasicData("RESET", "3,5"))
        AlcTabCombo(1).Sort 1, True, True
        AlcTabCombo(1).Sort 0, True, True
        FilterCnt = AlcTabCombo(1).GetLineCount
        If FilterCnt > 1 Then AlcTabCombo(1).InsertTextLineAt 0, "***,,All Airlines,", False
        FilterCnt = AlcTabCombo(1).GetLineCount
        AlcTabCombo(0).SetColumnValue 0, 0, AlcTabCombo(1).GetColumnValue(0, 0)
        CurPos(0).Caption = AlcTabCombo(1).GetColumnValue(0, 0)
        If CurPos(0).Caption = "***" Then CurPos(0).Caption = "-ALL-"
        For LineNo = 0 To FilterCnt
            tmpAlc = AlcTabCombo(1).GetLineValues(LineNo)
            AlcTabCombo(0).ComboAddTextLines "AirlCodes", tmpAlc, vbLf
            ChkAlc = "," + GetItem(tmpAlc, 1, ",") + ","
            If InStr(ChkErrorList, ChkAlc) > 0 Then
                AlcTabCombo(0).ComboSetLineColors "AirlCodes", LineNo, vbWhite, vbRed
            End If
        Next
        AlcTabCombo(0).SetCurrentSelection 0
        AlcTabCombo(0).RedrawTab
        Me.MousePointer = 0
        If UsedAlc3Filter <> "" Then UsedAlc3Filter = Left(UsedAlc3Filter, Len(UsedAlc3Filter) - 1)
        If Len(UsedAlc3Filter) = 0 Then ErrCnt = 1
        ReqVal = 1
        If (ErrCnt > 0) And (CedaIsConnected) Then
            MsgTxt = ErrCnt & " Error(s) detected." & vbNewLine & "Do you want to proceed?"
            ReqVal = MyMsgBox.CallAskUser(0, 0, 0, "Record Filter", MsgTxt, "infomsg", "Yes,No", UserAnswer)
            Me.Refresh
            CreateFlightLists = False
        End If
        If ReqVal = 1 Then
            If Not NoopCheckDoubles Then
                If Not StartedAsCompareTool Then
                    tmpBeginDay = FlightExtract.txtVpfr(0).Tag
                    tmpEndDay = FlightExtract.txtVpto(0).Tag
                Else
                    tmpBeginDay = MinImpVpfr
                    tmpEndDay = MaxImpVpto
                End If
                CurBegin.Caption = DecodeSsimDayFormat(tmpBeginDay, "CEDA", "SSIM2")
                CurBegin.Tag = tmpBeginDay
                CurEnd.Caption = DecodeSsimDayFormat(tmpEndDay, "CEDA", "SSIM2")
                CurEnd.Tag = tmpEndDay
                LoadClientFlights
                If Not StopAllLoops Then
                    If chkWork(2).Value = 1 Then
                        If MyFullMode = True Then LoadFlightsByAlc3 UsedAlc3Filter, tmpBeginDay, tmpEndDay
                        If MyUpdateMode = True Then LoadFlightsByFkey "", ""
                        CreateApdxData
                    Else
                        CreateDistinctList
                    End If
                End If
            Else
                LoadClientFlights
                If Not StopAllLoops Then
                    CreateDistinctList
                    CreateDistinctFlno "-ALL-"
                End If
            End If
            CreateFlightLists = True
        End If
    Else
        If chkSpool(3).Value = 0 Then
            MsgTxt = "Please load flight data first."
            If MyMsgBox.CallAskUser(0, 0, 0, "Record Filter", MsgTxt, "infomsg", "", UserAnswer) > 0 Then DoNothing
        End If
        CreateFlightLists = False
    End If
    StatusBar.Panels(2).Text = ""
    If StopAllLoops Then CreateFlightLists = False
End Function

Private Sub CreateDistinctList()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim BgnLine As Long
    Dim CntLine As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim tmpBuffer As String
    Dim tmpRec As String
    Dim OldRec As String
    Dim NxtRec As String
    Me.MousePointer = 11
    StatusBar.Panels(2).Text = "Sorting buffer."
    NoopList(0).Sort Trim(Str(RotaCol)), True, True
    NoopList(0).RedrawTab
    MaxLine = NoopList(0).GetLineCount - 1
    OldRec = ""
    SetProgressBar 0, 1, MaxLine, "Comparing Flights ..."
    If Not NoopCheckDoubles Then
        For CurLine = 0 To MaxLine
            If CurLine Mod 200 = 0 Then SetProgressBar 0, 2, CurLine, ""
            tmpRec = NoopList(0).GetColumnValue(CurLine, RotaCol)
            If OldRec = tmpRec Then
                NoopList(0).SetLineColor CurLine, vbBlack, CliFkeyCnt(5).BackColor
            Else
                NoopList(0).SetLineColor CurLine, vbBlack, LightGray
            End If
            OldRec = tmpRec
        Next
    Else
        CurLine = 0
        BgnLine = -1
        CntLine = 0
        While CurLine <= MaxLine
            CntLine = CntLine + 1
            If CntLine Mod 1000 = 0 Then SetProgressBar 0, 2, CntLine, ""
            tmpRec = NoopList(0).GetColumnValue(CurLine, RotaCol)
            If tmpRec = OldRec Then
                NoopList(0).SetLineColor CurLine, vbBlack, CliFkeyCnt(5).BackColor
                BgnLine = -1
            Else
                If BgnLine >= 0 Then
                    NoopList(0).DeleteLine BgnLine
                    CurLine = CurLine - 1
                    MaxLine = MaxLine - 1
                End If
                BgnLine = CurLine
                OldRec = tmpRec
            End If
            CurLine = CurLine + 1
        Wend
        If BgnLine >= 0 Then NoopList(0).DeleteLine BgnLine
    End If
    NoopList(0).RedrawTab
    Me.MousePointer = 0
    SetProgressBar 0, 3, 0, ""
    AdjustScrollMaster -1, -1, "", False
    Me.Refresh
End Sub

Private Function Alc3LookUp(AirlCode As String) As String
    Dim i As Long
    Dim Max As Long
    Dim Result As String
    Dim tmpAlc As String
    tmpAlc = Trim(AirlCode)
    Result = tmpAlc
    If Len(tmpAlc) = 2 Then
        Max = AlcTabCombo(1).GetLineCount - 1
        For i = 0 To Max
            If Trim(AlcTabCombo(1).GetColumnValue(i, 0)) = tmpAlc Then
                Result = Trim(AlcTabCombo(1).GetColumnValue(i, 1))
                Exit For
            End If
        Next
    End If
    Alc3LookUp = Result
End Function

Private Sub LoadFlightsByAlc3(UseAlc3 As String, BeginDay As String, EndDay As String)
    Dim FlightsFound As Boolean
    Dim CedaError As Boolean
    Dim RetCode As Integer
    Dim CdrAnsw As Boolean
    Dim tmpResult As String
    Dim tmpArrSqlKey As String
    Dim tmpArrFldLst As String
    Dim tmpDepSqlKey As String
    Dim tmpDepFldLst As String
    Dim tmpKeyItems As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpFkey As String
    Dim tmpTime As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim FromLine As Long
    Dim ToLine As Long
    Dim BufferSteps As Long
    Dim ReadDays As Long
    Dim FirstDay
    Dim LastDay
    Dim ReadFrom
    Dim ReadTo
    Dim ReadSteps As Long
    Dim LoopCount As Long
    Dim LastLineCount As Long
    Dim i As Long
    
    Dim StartZeit
    If CedaIsConnected Then
    
        Me.MousePointer = 11
        NoopList(1).ResetContent
    
        DspLoading.Visible = True
        DspLoaded.Visible = False
        Me.Refresh
        
        CedaLed(0).Visible = True
        CedaLed(1).Visible = True
        FirstDay = CedaDateToVb(BeginDay)
        LastDay = CedaDateToVb(EndDay)
        'ReadDays = 30
        ReadDays = 10
        ReadSteps = ((DateDiff("d", FirstDay, LastDay) - 1) / ReadDays) + 1
        If ReadSteps < 1 Then ReadSteps = 1
        SetProgressBar 0, 1, ReadSteps, "Reading from the database ..."
    
        NoopList(1).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
        NoopList(1).CedaHopo = UfisServer.HOPO
        NoopList(1).CedaIdentifier = ""
        NoopList(1).CedaPort = "3357"
        NoopList(1).CedaReceiveTimeout = "250"
        NoopList(1).CedaRecordSeparator = vbLf
        NoopList(1).CedaSendTimeout = "250"
        NoopList(1).CedaServerName = UfisServer.HostName
        NoopList(1).CedaTabext = UfisServer.TblExt
        NoopList(1).CedaUser = gsUserName
        NoopList(1).CedaWorkstation = UfisServer.GetMyWorkStationName
        
        'tmpArrFldLst = "PRFL,FTYP,FLNO,ADID,STOA,DOOA,ACT3,ORG3,VIA3,STYP,REGN,FLDA,STOD,VIAL,FLTN,TIFA,RTYP,FKEY,RKEY,URNO,PRFL,PRFL,PRFL,ACT5,ORG4,VIA4,'  ',"
        'tmpDepFldLst = "PRFL,FTYP,FLNO,ADID,STOD,DOOD,ACT3,DES3,VIA3,STYP,REGN,FLDA,STOA,VIAL,FLTN,TIFD,RTYP,FKEY,RKEY,URNO,PRFL,PRFL,PRFL,ACT5,DES4,VIA4,'  ',"
        tmpArrFldLst = "PRFL,FTYP,FLNO,ADID,STOA,DOOA,ACT3,ORG3,VIA3,STYP,REGN,FLDA,STOD,VIAL,FLTN,TIFA,RTYP,FKEY,RKEY,URNO,PRFL,PRFL,PRFL,ACT5,ORG4,VIA4,REGN,"
        tmpDepFldLst = "PRFL,FTYP,FLNO,ADID,STOD,DOOD,ACT3,DES3,VIA3,STYP,REGN,FLDA,STOA,VIAL,FLTN,TIFD,RTYP,FKEY,RKEY,URNO,PRFL,PRFL,PRFL,ACT5,DES4,VIA4,REGN,"
'        tmpArrFldLst = tmpArrFldLst & "'  ',"
'        tmpDepFldLst = tmpDepFldLst & "'  ',"
'        tmpArrFldLst = tmpArrFldLst & "'  '"
'        tmpDepFldLst = tmpDepFldLst & "'  '"
        tmpArrFldLst = tmpArrFldLst & "STEV,"
        tmpDepFldLst = tmpDepFldLst & "STEV,"
        tmpArrFldLst = tmpArrFldLst & "REMP"
        tmpDepFldLst = tmpDepFldLst & "REMP"
        
        If (AddOnArrFields <> "") And (AddOnArrAftFld <> "") Then
            tmpArrFldLst = tmpArrFldLst & "," & AddOnArrAftFld
        End If
        If (AddOnDepFields <> "") And (AddOnDepAftFld <> "") Then
            tmpDepFldLst = tmpDepFldLst & "," & AddOnDepAftFld
        End If
        
        tmpArrFldLst = Replace(tmpArrFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
        tmpDepFldLst = Replace(tmpDepFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
        
        StartZeit = Timer
        LoopCount = 0
        CedaError = False
        ReadFrom = FirstDay
        Do
            FlightsFound = False
            ReadTo = DateAdd("d", (ReadDays - 1), ReadFrom)
            If ReadTo > LastDay Then ReadTo = LastDay
            tmpVpfr = Format(ReadFrom, "yyyymmdd")
            tmpVpto = Format(ReadTo, "yyyymmdd")
            lblVpfr.Caption = DecodeSsimDayFormat(tmpVpfr, "CEDA", "SSIM2")
            lblVpto.Caption = DecodeSsimDayFormat(tmpVpto, "CEDA", "SSIM2")
            DspLoading.Refresh
            LoopCount = LoopCount + 1
            SetProgressBar 0, 2, LoopCount, ""
            'tmpArrSqlKey = "WHERE STOA BETWEEN '" & tmpVpfr & "000000' AND '" & tmpVpto & "235959' AND ALC3 IN (" & UseAlc3 & ") AND ADID='A' AND FLNO<>' '"
            'tmpDepSqlKey = "WHERE STOD BETWEEN '" & tmpVpfr & "000000' AND '" & tmpVpto & "235959' AND ALC3 IN (" & UseAlc3 & ") AND ADID='D' AND FLNO<>' '"
            tmpArrSqlKey = "WHERE STOA BETWEEN '" & tmpVpfr & "000000' AND '" & tmpVpto & "235959' AND ALC3 IN (" & UseAlc3 & ") AND ADID='A'"
            tmpDepSqlKey = "WHERE STOD BETWEEN '" & tmpVpfr & "000000' AND '" & tmpVpto & "235959' AND ALC3 IN (" & UseAlc3 & ") AND ADID='D'"
            If MySetUp.chkServer(0).Value = 1 Then
                tmpKeyItems = "{=CMD=}" & UseGfrCmd & "{=TBL=}AFTTAB{=EVT=}2"
                tmpKeyItems = tmpKeyItems & "{=EVT_1=}{=FLD=}" & tmpArrFldLst & "{=WHE=}" & tmpArrSqlKey
                tmpKeyItems = tmpKeyItems & "{=EVT_2=}{=FLD=}" & tmpDepFldLst & "{=WHE=}" & tmpDepSqlKey
                LastLineCount = NoopList(1).GetLineCount
                CedaLed(0).FillColor = NormalYellow
                CedaLed(0).Refresh
                CedaLed(1).FillColor = NormalYellow
                CedaLed(1).Refresh
                CdrAnsw = NoopList(1).CedaFreeCommand(tmpKeyItems)
                If CdrAnsw = False Then
                    CedaLed(0).FillColor = vbRed
                    CedaLed(0).Refresh
                    CedaLed(1).FillColor = vbRed
                    CedaLed(1).Refresh
                    MsgBox NoopList(1).GetLastCedaError
                    CedaError = True
                Else
                    CedaLed(0).FillColor = NormalGreen
                    CedaLed(0).Refresh
                    CedaLed(1).FillColor = NormalGreen
                    CedaLed(1).Refresh
                    If NoopList(1).GetLineCount > LastLineCount Then FlightsFound = True
                End If
                If FlightsFound Then
                    MaxLine = NoopList(1).GetLineCount
                    SrvFkeyCnt(0).Caption = CStr(MaxLine)
                    SrvFkeyCnt(0).Refresh
                    NoopList(1).OnVScrollTo MaxLine
                    NoopList(1).Refresh
                End If
            Else
                CedaLed(0).FillColor = NormalYellow
                CedaLed(0).Refresh
                RetCode = UfisServer.CallCeda(tmpResult, UseGfrCmd, "AFTTAB", tmpArrFldLst, "", tmpArrSqlKey, "", 0, False, False)
                CedaLed(0).FillColor = NormalGreen
                CedaLed(0).Refresh
                MaxLine = UfisServer.DataBuffer(0).GetLineCount - 1
                If MaxLine >= 0 Then
                    FlightsFound = True
                    If MaxLine = 0 Then
                        tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(0))
                        If Left(tmpResult, 3) <> ",,," Then
                            NoopList(1).InsertTextLine tmpResult, False
                        End If
                        MaxLine = -1
                    End If
                    For CurLine = 0 To MaxLine
                        tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(CurLine))
                        NoopList(1).InsertTextLine tmpResult, False
                    Next
                    MaxLine = NoopList(1).GetLineCount
                    SrvFkeyCnt(0).Caption = CStr(MaxLine)
                    SrvFkeyCnt(0).Refresh
                    NoopList(1).OnVScrollTo MaxLine
                    NoopList(1).Refresh
                End If
                CedaLed(1).FillColor = NormalYellow
                CedaLed(1).Refresh
                RetCode = UfisServer.CallCeda(tmpResult, UseGfrCmd, "AFTTAB", tmpDepFldLst, "", tmpDepSqlKey, "", 0, False, False)
                CedaLed(1).FillColor = NormalGreen
                CedaLed(1).Refresh
                MaxLine = UfisServer.DataBuffer(0).GetLineCount - 1
                If MaxLine >= 0 Then
                    FlightsFound = True
                    If MaxLine = 0 Then
                        tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(0))
                        If Left(tmpResult, 3) <> ",,," Then
                            NoopList(1).InsertTextLine tmpResult, False
                        End If
                        MaxLine = -1
                    End If
                    For CurLine = 0 To MaxLine
                        tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(CurLine))
                        NoopList(1).InsertTextLine tmpResult, False
                    Next
                    MaxLine = NoopList(1).GetLineCount
                    SrvFkeyCnt(0).Caption = CStr(MaxLine)
                    SrvFkeyCnt(0).Refresh
                    NoopList(1).OnVScrollTo MaxLine
                    NoopList(1).Refresh
                End If
            End If
            If (FlightsFound) And ((MaxLine < 32) Or (LoopCount = 1)) Then
                NoopList(1).RedrawTab
                Me.Refresh
            End If
            ReadFrom = DateAdd("d", 1, ReadTo)
            If StopAllLoops Then ReadFrom = DateAdd("d", 1, LastDay)
        Loop While (ReadFrom <= LastDay) And (Not CedaError)
        If CedaError Then
            NoopList(0).ResetContent
            NoopList(0).RedrawTab
            NoopList(1).ResetContent
        End If
        NoopList(1).OnVScrollTo 0
        NoopList(1).Refresh
        DspLoading.Visible = False
        tmpResult = Trim(Str(Timer - StartZeit))
        If Left(tmpResult, 1) = "." Then tmpResult = "0" & tmpResult
        tmpResult = GetItem(tmpResult, 1, ".") & "." & Left(GetItem(tmpResult, 2, "."), 3) & "s"
        UsedSec.Caption = tmpResult
        DspLoaded.Visible = True
        SetProgressBar 0, 3, 0, ""
        Me.Refresh
        CedaLed(0).Visible = False
        CedaLed(1).Visible = False
        
        Me.MousePointer = 0
        SrvFkeyCnt(0).Caption = CStr(NoopList(1).GetLineCount)
        Me.Refresh
        If Not StopAllLoops Then CreateServerRotations
    End If
End Sub
'LOAD AODB FLIGHT
'PHYOE
Private Sub LoadFlightsByFkey(UseArrRkeys As String, UseDepRkeys As String)
    Dim AppendFlights As Boolean
    Dim FlightsFound As Boolean
    Dim RetCode As Integer
    Dim CdrAnsw As Boolean
    Dim CedaError As Boolean
    Dim tmpResult As String
    Dim ArrFkeys As String
    Dim DepFkeys As String
    Dim tmpArrSqlKey As String
    Dim tmpArrFldLst As String
    Dim tmpDepSqlKey As String
    Dim tmpDepFldLst As String
    Dim tmpKeyItems As String
    Dim tmpFkey As String
    Dim tmpTime As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim ReadMaxLine As Long
    Dim FromLine As Long
    Dim ToLine As Long
    Dim LinCnt As Long
    Dim LastLineCount As Long
    Dim AppLine As Long
    Dim MaxAppLine As Long
    Dim BufferSteps As Long
    Dim i As Long
    
    Dim StartZeit
    
    If CedaIsConnected Then
        
        Me.MousePointer = 11
        DspLoaded.Visible = False
        CedaLed(0).Visible = True
        CedaLed(1).Visible = True
        If (UseArrRkeys <> "") Or (UseDepRkeys <> "") Then AppendFlights = True Else AppendFlights = False
        If Not AppendFlights Then NoopList(1).ResetContent
    
        If MySetUp.chkServer(0).Value = 1 Then
            NoopList(1).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
            NoopList(1).CedaHopo = UfisServer.HOPO
            NoopList(1).CedaIdentifier = "ID"
            NoopList(1).CedaPort = "3357"
            NoopList(1).CedaReceiveTimeout = "250"
            NoopList(1).CedaRecordSeparator = vbLf
            NoopList(1).CedaSendTimeout = "250"
            NoopList(1).CedaServerName = UfisServer.HostName
            NoopList(1).CedaTabext = UfisServer.TblExt
            NoopList(1).CedaUser = gsUserName
            NoopList(1).CedaWorkstation = UfisServer.GetMyWorkStationName
        End If
        
        'tmpArrFldLst = "PRFL,FTYP,FLNO,ADID,STOA,DOOA,ACT3,ORG3,VIA3,STYP,REGN,FLDA,STOD,VIAL,FLTN,TIFA,RTYP,FKEY,RKEY,URNO,PRFL,PRFL,PRFL,ACT5,ORG4,VIA4,'  ',"
        'tmpDepFldLst = "PRFL,FTYP,FLNO,ADID,STOD,DOOD,ACT3,DES3,VIA3,STYP,REGN,FLDA,STOA,VIAL,FLTN,TIFD,RTYP,FKEY,RKEY,URNO,PRFL,PRFL,PRFL,ACT5,DES4,VIA4,'  ',"
        tmpArrFldLst = "PRFL,FTYP,FLNO,ADID,STOA,DOOA,ACT3,ORG3,VIA3,STYP,REGN,FLDA,STOD,VIAL,FLTN,TIFA,RTYP,FKEY,RKEY,URNO,PRFL,PRFL,PRFL,ACT5,ORG4,VIA4,REGN,"
        tmpDepFldLst = "PRFL,FTYP,FLNO,ADID,STOD,DOOD,ACT3,DES3,VIA3,STYP,REGN,FLDA,STOA,VIAL,FLTN,TIFD,RTYP,FKEY,RKEY,URNO,PRFL,PRFL,PRFL,ACT5,DES4,VIA4,REGN,"
'        tmpArrFldLst = tmpArrFldLst & "'  ',"
'        tmpDepFldLst = tmpDepFldLst & "'  ',"
'        tmpArrFldLst = tmpArrFldLst & "'  '"
'        tmpDepFldLst = tmpDepFldLst & "'  '"
        
        tmpArrFldLst = tmpArrFldLst & "STEV,"
        tmpDepFldLst = tmpDepFldLst & "STEV,"
        tmpArrFldLst = tmpArrFldLst & "REMP"
        tmpDepFldLst = tmpDepFldLst & "REMP"
        
        If (AddOnArrFields <> "") And (AddOnArrAftFld <> "") Then
            tmpArrFldLst = tmpArrFldLst & "," & AddOnArrAftFld
        End If
        If (AddOnDepFields <> "") And (AddOnDepAftFld <> "") Then
            tmpDepFldLst = tmpDepFldLst & "," & AddOnDepAftFld
        End If
        
        tmpArrFldLst = Replace(tmpArrFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
        tmpDepFldLst = Replace(tmpDepFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
        
        StartZeit = Timer
        BufferSteps = 500
        ReadMaxLine = NoopList(0).GetLineCount - 1
        FromLine = 0
        If AppendFlights Then
            HelperTab(0).ResetContent
            HelperTab(1).ResetContent
            HelperTab(0).InsertBuffer UseArrRkeys, ","
            HelperTab(1).InsertBuffer UseDepRkeys, ","
            'HelperTab(0).RedrawTab
            'HelperTab(1).RedrawTab
            FromLine = HelperTab(0).GetLineCount - 1
            ReadMaxLine = HelperTab(1).GetLineCount - 1
            If FromLine > ReadMaxLine Then ReadMaxLine = FromLine
            FromLine = 0
        End If
        SetProgressBar 0, 1, ReadMaxLine, "Reading from the database ..."
        While FromLine <= ReadMaxLine
            ToLine = FromLine + BufferSteps
            If ToLine > ReadMaxLine Then ToLine = ReadMaxLine
            SetProgressBar 0, 2, ToLine, ""
            FlightsFound = False
            If Not AppendFlights Then
                CreateFkeyFilters FromLine, ToLine, ArrFkeys, DepFkeys
                'tmpArrSqlKey = "WHERE FKEY IN (" & ArrFkeys & ") AND ADID='A' AND FTYP NOT IN ('T','G')"
                'tmpDepSqlKey = "WHERE FKEY IN (" & DepFkeys & ") AND ADID='D' AND FTYP NOT IN ('T','G')"
                tmpArrSqlKey = "WHERE FKEY IN (" & ArrFkeys & ") AND ADID='A'"
                tmpDepSqlKey = "WHERE FKEY IN (" & DepFkeys & ") AND ADID='D'"
            Else
                CreateRkeyFilters FromLine, ToLine, ArrFkeys, DepFkeys
                'tmpArrSqlKey = "WHERE RKEY IN (" & ArrFkeys & ") AND ADID='A' AND FTYP NOT IN ('T','G')"
                tmpArrSqlKey = "WHERE RKEY IN (" & ArrFkeys & ") AND ADID='A'"
                'tmpDepSqlKey = "WHERE RKEY IN (" & DepFkeys & ") AND ADID='D' AND FTYP NOT IN ('T','G')"
                tmpDepSqlKey = "WHERE RKEY IN (" & DepFkeys & ") AND ADID='D'"
            End If
            If ((ArrFkeys <> "") And (DepFkeys <> "")) And (MySetUp.chkServer(0).Value = 1) Then
                tmpKeyItems = "{=CMD=}" & UseGfrCmd & "{=TBL=}AFTTAB{=EVT=}2"
                tmpKeyItems = tmpKeyItems & "{=EVT_1=}{=FLD=}" & tmpArrFldLst & "{=WHE=}" & tmpArrSqlKey
                tmpKeyItems = tmpKeyItems & "{=EVT_2=}{=FLD=}" & tmpDepFldLst & "{=WHE=}" & tmpDepSqlKey
                LastLineCount = NoopList(1).GetLineCount
                CedaLed(0).FillColor = NormalYellow
                CedaLed(0).Refresh
                CedaLed(1).FillColor = NormalYellow
                CedaLed(1).Refresh
                CdrAnsw = NoopList(1).CedaFreeCommand(tmpKeyItems)
                If CdrAnsw = False Then
                    CedaLed(0).FillColor = vbRed
                    CedaLed(0).Refresh
                    CedaLed(1).FillColor = vbRed
                    CedaLed(1).Refresh
                    MsgBox NoopList(1).GetLastCedaError
                    CedaError = True
                Else
                    CedaLed(0).FillColor = NormalGreen
                    CedaLed(0).Refresh
                    CedaLed(1).FillColor = NormalGreen
                    CedaLed(1).Refresh
                    If NoopList(1).GetLineCount > LastLineCount Then
                        If AppendFlights Then
                            MaxAppLine = NoopList(1).GetLineCount - 1
                            For AppLine = LastLineCount To MaxAppLine
                                NoopList(1).SetColumnValue AppLine, 0, "L"
                            Next
                        End If
                        FlightsFound = True
                    End If
                End If
                ArrFkeys = ""
                DepFkeys = ""
            End If
            If ArrFkeys <> "" Then
                CedaLed(0).FillColor = NormalYellow
                CedaLed(0).Refresh
                If MySetUp.chkServer(0).Value = 1 Then
                    LastLineCount = NoopList(1).GetLineCount
                    CdrAnsw = NoopList(1).CedaAction(UseGfrCmd, "AFTTAB", tmpArrFldLst, "", tmpArrSqlKey)
                    If CdrAnsw = False Then
                        CedaLed(0).FillColor = vbRed
                        CedaLed(0).Refresh
                        MsgBox NoopList(1).GetLastCedaError
                        CedaError = True
                    Else
                        CedaLed(0).FillColor = NormalGreen
                        CedaLed(0).Refresh
                        If NoopList(1).GetLineCount > LastLineCount Then
                            If AppendFlights Then
                                MaxAppLine = NoopList(1).GetLineCount - 1
                                For AppLine = LastLineCount To MaxAppLine
                                    NoopList(1).SetColumnValue AppLine, 0, "L"
                                Next
                            End If
                            FlightsFound = True
                        End If
                    End If
                Else
                    RetCode = UfisServer.CallCeda(tmpResult, UseGfrCmd, "AFTTAB", tmpArrFldLst, "", tmpArrSqlKey, "", 0, False, False)
                    CedaLed(0).FillColor = NormalGreen
                    CedaLed(0).Refresh
                    MaxLine = UfisServer.DataBuffer(0).GetLineCount - 1
                    If MaxLine >= 0 Then
                        FlightsFound = True
                        AppLine = NoopList(1).GetLineCount
                        If MaxLine = 0 Then
                            tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(0))
                            If Left(tmpResult, 3) <> ",,," Then
                                NoopList(1).InsertTextLine tmpResult, False
                                If AppendFlights Then
                                    NoopList(1).SetColumnValue AppLine, 0, "L"
                                End If
                            End If
                            MaxLine = -1
                        End If
                        For CurLine = 0 To MaxLine
                            tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(CurLine))
                            NoopList(1).InsertTextLine tmpResult, False
                            If AppendFlights Then
                                NoopList(1).SetColumnValue AppLine, 0, "L"
                                AppLine = AppLine + 1
                            End If
                        Next
                    End If
                End If
                LinCnt = NoopList(1).GetLineCount
                SrvFkeyCnt(0).Caption = CStr(LinCnt)
                SrvFkeyCnt(0).Refresh
            End If
            
            If DepFkeys <> "" Then
                CedaLed(1).FillColor = NormalYellow
                CedaLed(1).Refresh
                If MySetUp.chkServer(0).Value = 1 Then
                    LastLineCount = NoopList(1).GetLineCount
                    CdrAnsw = NoopList(1).CedaAction(UseGfrCmd, "AFTTAB", tmpDepFldLst, "", tmpDepSqlKey)
                    If CdrAnsw = False Then
                        CedaLed(1).FillColor = vbRed
                        CedaLed(1).Refresh
                        MsgBox NoopList(1).GetLastCedaError
                        CedaError = True
                    Else
                        CedaLed(1).FillColor = NormalGreen
                        CedaLed(1).Refresh
                        If NoopList(1).GetLineCount > LastLineCount Then
                            If AppendFlights Then
                                MaxAppLine = NoopList(1).GetLineCount - 1
                                For AppLine = LastLineCount To MaxAppLine
                                    NoopList(1).SetColumnValue AppLine, 0, "L"
                                Next
                            End If
                            FlightsFound = True
                        End If
                    End If
                Else
                    RetCode = UfisServer.CallCeda(tmpResult, UseGfrCmd, "AFTTAB", tmpDepFldLst, "", tmpDepSqlKey, "", 0, False, False)
                    CedaLed(1).FillColor = NormalGreen
                    CedaLed(1).Refresh
                    MaxLine = UfisServer.DataBuffer(0).GetLineCount - 1
                    If MaxLine >= 0 Then
                        FlightsFound = True
                        AppLine = NoopList(1).GetLineCount
                        If MaxLine = 0 Then
                            tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(0))
                            If Left(tmpResult, 3) <> ",,," Then
                                NoopList(1).InsertTextLine tmpResult, False
                                If AppendFlights Then
                                    NoopList(1).SetColumnValue AppLine, 0, "L"
                                End If
                            End If
                            MaxLine = -1
                        End If
                        For CurLine = 0 To MaxLine
                            tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(CurLine))
                            NoopList(1).InsertTextLine tmpResult, False
                            If AppendFlights Then
                                NoopList(1).SetColumnValue AppLine, 0, "L"
                                AppLine = AppLine + 1
                            End If
                        Next
                    End If
                End If
                LinCnt = NoopList(1).GetLineCount
                SrvFkeyCnt(0).Caption = CStr(LinCnt)
                SrvFkeyCnt(0).Refresh
            End If
            If FlightsFound Then
                MaxLine = NoopList(1).GetLineCount
                SrvFkeyCnt(0).Caption = CStr(MaxLine)
                SrvFkeyCnt(0).Refresh
                NoopList(1).OnVScrollTo MaxLine
                NoopList(1).Refresh
            End If
            FromLine = ToLine + 1
            If StopAllLoops Then FromLine = ReadMaxLine + 1
        Wend
        NoopList(1).OnVScrollTo 0
        NoopList(1).Refresh
        
        If Not AppendFlights Then
            tmpResult = Trim(Str(Timer - StartZeit))
            If Left(tmpResult, 1) = "." Then tmpResult = "0" & tmpResult
            tmpResult = GetItem(tmpResult, 1, ".") & "." & Left(GetItem(tmpResult, 2, "."), 3) & "s"
            UsedSec.Caption = tmpResult
        End If
        DspLoaded.Visible = True
        SetProgressBar 0, 3, 0, ""
        CedaLed(0).Visible = False
        CedaLed(1).Visible = False
        HelperTab(0).ResetContent
        HelperTab(1).ResetContent
        Me.MousePointer = 0
        SrvFkeyCnt(0).Caption = CStr(NoopList(1).GetLineCount)
        Me.Refresh
        If Not StopAllLoops Then
            If Not AppendFlights Then CreateServerRotations
        End If
    End If
    HelperTab(0).ResetContent
    HelperTab(1).ResetContent
End Sub

Private Sub CreateApdxData()
    Dim tmpApdxData As String
    Dim tmpAddOnVal As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurCol As Long
    Dim i As Integer
    If AddArrFldCnt > 0 Then
        MaxLine = NoopList(1).GetLineCount - 1
        For CurLine = 0 To MaxLine
            tmpApdxData = ""
            CurCol = ApdxCol + 1
            For i = 1 To AddArrFldCnt
                CurCol = CurCol + 1
                tmpAddOnVal = NoopList(1).GetColumnValue(CurLine, CurCol)
                tmpApdxData = tmpApdxData & ";" & tmpAddOnVal
            Next
            tmpApdxData = Mid(tmpApdxData, 2)
            NoopList(1).SetColumnValue CurLine, ApdxCol, tmpApdxData
        Next
    End If
End Sub
Private Sub CreateFkeyFilters(FromLine As Long, ToLine As Long, ArrFkeys As String, DepFkeys As String)
    Dim tmpAdid As String
    Dim CurFkey As String
    Dim NewFkey As String
    Dim CurSked As String
    Dim NewSked As String
    Dim CurDate As String
    Dim NewDate As String
    Dim CurLine As Long
    ArrFkeys = ""
    DepFkeys = ""
    For CurLine = FromLine To ToLine
        CurFkey = NoopList(0).GetColumnValue(CurLine, FkeyCol)
        CurFkey = Mid(CurFkey, 4, 5) & Left(CurFkey, 3) & Mid(CurFkey, 9)
        'CurSked = NoopList(0).GetColumnValue(CurLine, TimeCol)
        'NewSked = ApcLocalToUtc(HomeAirport, CurSked)
        'CurDate = Left(CurSked, 8)
        'NewDate = Left(NewSked, 8)
        NewFkey = ""
        'If CurDate <> NewDate Then
        '    NewFkey = CurFkey
        '    Mid(NewFkey, 10, 8) = NewDate
        '    tmpAdid = Mid(CurFkey, 18, 1)
        'End If
        tmpAdid = Mid(CurFkey, 18, 1)
        Select Case tmpAdid
            Case "A"
                ArrFkeys = ArrFkeys & "'" & CurFkey & "',"
                If NewFkey <> "" Then ArrFkeys = ArrFkeys & "'" & NewFkey & "',"
            Case "D"
                DepFkeys = DepFkeys & "'" & CurFkey & "',"
                If NewFkey <> "" Then ArrFkeys = ArrFkeys & "'" & NewFkey & "',"
            Case Else
        End Select
    Next
    If ArrFkeys <> "" Then ArrFkeys = Left(ArrFkeys, Len(ArrFkeys) - 1)
    If DepFkeys <> "" Then DepFkeys = Left(DepFkeys, Len(DepFkeys) - 1)
End Sub
Private Sub CreateRkeyFilters(FromLine As Long, ToLine As Long, ArrRkeys As String, DepRkeys As String)
    Dim CurRkey As String
    Dim CurLine As Long
    ArrRkeys = ""
    DepRkeys = ""
    For CurLine = FromLine To ToLine
        CurRkey = HelperTab(0).GetColumnValue(CurLine, 0)
        If CurRkey <> "" Then ArrRkeys = ArrRkeys & CurRkey & ","
        CurRkey = HelperTab(1).GetColumnValue(CurLine, 0)
        If CurRkey <> "" Then DepRkeys = DepRkeys & CurRkey & ","
    Next
    If ArrRkeys <> "" Then ArrRkeys = Left(ArrRkeys, Len(ArrRkeys) - 1)
    If DepRkeys <> "" Then DepRkeys = Left(DepRkeys, Len(DepRkeys) - 1)
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        ArrangeLayoutVert
        ArrangeLayoutHoriz
        DataTab.Top = NoopList(0).Top
        DataTab.Width = Me.ScaleWidth - 60
        DataTab.ZOrder
    End If
End Sub
'LOAD CLIENT FLIGHTS
'PHYOE
Private Sub LoadClientFlights()
    Dim TlxImpFields As String
    Dim PreFixFields As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineCount As Long
    Dim BufferCount As Long
    Dim CurDateDiff As Long
    Dim CurDateCount As Long
    Dim DataLine As String
    Dim ExtractLine As String
    Dim LineBuff As String
    Dim strFrqd As String
    Dim WeekFreq As Integer
    Dim DayOffset As Integer
    
    Dim CurCode As String
    
    Dim ArrDate As String
    Dim ArrStoa As String
    Dim ArrFlno As String
    Dim ArrAlc3 As String
    Dim ArrFlca As String
    Dim ArrFltn As String
    Dim ArrFlns As String
    Dim ArrFkey As String
    Dim ArrVia3 As String
    Dim ArrTtyp As String
    
    Dim DepDate As String
    Dim DepStod As String
    Dim DepFlno As String
    Dim DepAlc3 As String
    Dim DepFlcd As String
    Dim DepFltn As String
    Dim DepFlns As String
    Dim DepFkey As String
    Dim DepVia3 As String
    Dim DepTtyp As String
    
    Dim CurFtyp As String
    Dim CurRegn As String
    Dim CurAct3 As String
    Dim CurOrg3 As String
    Dim CurDes3 As String
    Dim CurRkey As String
    Dim CurAcmd As String
    
    Dim varVpfr As Variant
    Dim varVpto As Variant
    Dim varFday As Variant
    Dim strWkdy As String
    Dim ArrWkdy As String
    Dim DepWkdy As String
    Dim WkDayCnt As Integer
    Dim tmpData As String
    Dim tmpRout As String
    
    Dim varStoa As Variant
    Dim varStod As Variant
    Dim varCurStoa As Variant
    Dim varCurStod As Variant
    
    Dim ArrGrti As Long
    Dim DepGrti As Long
    Dim AcmdCol As Long
    
    Dim IsValidWeek As Boolean
    Dim InvalidDays As Integer
    Dim TurnFlag As Integer
    Dim ArrFlt As Boolean
    Dim DepFlt As Boolean
    
    Dim SeasVpfr As String
    Dim SeasVpto As String
    Dim UseAlc3 As String
    Dim ArrIdx As Integer
    Dim DepIdx As Integer
    Dim OutRange As Long
    
    Dim FirstDay
    Dim LastDay
    Dim PeriodRange As Long
    Dim i As Integer
    Dim j As Integer
    
    Dim OrigLine As Long
    Dim tmpLinDat As String
    Dim tmpArrFlight As String
    Dim tmpDepFlight As String
    Dim ArrRota As String
    Dim DepRota As String
    Dim LineType As String
    Dim UseAdidCall As Boolean
    Dim LineTextColor As Long
    Dim LineBackColor As Long
    Dim IsOverNight As Boolean
    Dim ApdxArr As String
    Dim ApdxDep As String
    
    If Not StartedAsCompareTool Then
        PreFixFields = "....,....,....,"
    Else
        PreFixFields = ""
    End If
    
    Me.MousePointer = 11
    DspLoading.Visible = False
    DspLoaded.Visible = False
    Me.Refresh
    If Not NoopCheckDoubles Then
        If Not StartedAsCompareTool Then
            MaxLine = FlightExtract.ExtractData.GetLineCount - 1
            SeasVpfr = CurBegin.Tag
            SeasVpto = CurEnd.Tag
        Else
            TlxImpFields = TlxImpData(1).LogicalFieldList
            UseStypField = "STYP"
            MaxLine = TlxImpData(1).GetLineCount - 1
            SeasVpfr = MinImpVpfr
            SeasVpto = MaxImpVpto
        End If
    Else
        SeasVpfr = MainDialog.ActSeason(3).Tag
        SeasVpto = MainDialog.ActSeason(2).Tag
        MaxLine = MainDialog.FileData(CurFdIdx).GetLineCount - 1
    End If
    FirstDay = CedaDateToVb(SeasVpfr)
    LastDay = CedaDateToVb(SeasVpto)
    PeriodRange = DateDiff("d", FirstDay, LastDay)
    If PeriodRange >= 0 Then
        NoopList(0).ResetContent
        NoopList(1).ResetContent
        Me.MousePointer = 11
        Refresh
        SetProgressBar 0, 1, MaxLine, "Creating Flights from File ..."
        LineBuff = ""
        LineCount = 0
        BufferCount = 0
        AcmdCol = CLng(GetItemNo(myViewFieldList, "ACMD")) + 2
        If Not StartedAsCompareTool Then
            If InStr(MainFieldList, "ADID") > 0 Then UseAdidCall = True Else UseAdidCall = False
        End If
        For CurLine = 0 To MaxLine
            If CurLine Mod 100 = 0 Then SetProgressBar 0, 2, CurLine, ""
            If Not NoopCheckDoubles Then
                If Not StartedAsCompareTool Then
                    DataLine = FlightExtract.ExtractData.GetLineValues(CurLine)
                    If NewLayoutType Then
                        CurAcmd = FlightExtract.ExtractData.GetColumnValue(CurLine, AcmdCol)
                        ExtractLine = DataLine
                        If Not UseAdidCall Then
                            DataLine = TranslateRecordData(ExtractLine, myViewFieldList, mySlotFieldList)
                        Else
                            DataLine = TranslateRecordFromFlight(ExtractLine, myViewFieldList, mySlotFieldList)
                        End If
                    End If
                Else
                    TlxImpData(1).GetLineColor CurLine, LineTextColor, LineBackColor
                    If LineBackColor <> vbRed Then
                        ExtractLine = TlxImpData(1).GetLineValues(CurLine)
                        DataLine = TranslateRecordData(ExtractLine, TlxImpFields, mySlotFieldList)
                    Else
                        DataLine = "E,E,"
                    End If
                End If
            Else
                DataLine = MainDialog.FileData(CurFdIdx).GetLineValues(CurLine)
                If NewLayoutType Then
                    ExtractLine = DataLine
                    If Not UseAdidCall Then
                        DataLine = TranslateRecordData(ExtractLine, MainFieldList, mySlotFieldList)
                    Else
                        DataLine = TranslateRecordFromFlight(ExtractLine, MainFieldList, mySlotFieldList)
                    End If
                End If
            End If
            
            'Attention: Known Bug for Weekly Frequencies > 1
            'The Extract Datalist contains a Period Begin (VPFR) cut to the first day of the filter.
            'In order to create all flights at the beginning of the filtered timeframe correctly,
            'we need the original period begin that is still kept in the lines of the main datalist.
            'Unfortunately it still works only with SLOT file formats
            'If DataSystemType = "SLOT" Then
            '    tmpLinDat = GetItem(DataLine, 28, ",")
            '    OrigLine = Val(tmpLinDat)
            '    DataLine = MainDialog.FileData(CurFdIdx).GetLineValues(OrigLine)
            'End If
            
            LineType = Trim(GetItem(DataLine, 2, ","))
            If (LineType <> "-") And (LineType <> "E") Then
                'No Ignore and no Error Line
                CurCode = GetItem(DataLine, 3, ",")
                If CurCode = "" Then CurCode = CurAcmd
                If CurCode = "K" Then CurCode = "N"
                If CurCode = "I" Then CurCode = "N"
                If CurCode = "X" Then CurCode = "D"
                
                varVpfr = CedaDateToVb(Left(GetItem(DataLine, 8, ","), 8))
                varVpto = CedaDateToVb(Left(GetItem(DataLine, 9, ","), 8))
                CurDateDiff = DateDiff("d", varVpfr, varVpto)
                strFrqd = GetItem(DataLine, 10, ",")
                If strFrqd = "" Then strFrqd = "1234567"
                WeekFreq = Val(GetItem(DataLine, 22, ","))
                If WeekFreq = 0 Then WeekFreq = 1
                DayOffset = Val(GetItem(DataLine, 17, ",")) 'Defines the departure
                IsOverNight = False
                If DayOffset > 0 Then IsOverNight = True
                
                'Determine what type of rotation we got
                ArrFltn = GetItem(DataLine, 5, ",")
                If ArrFltn <> "" Then
                    ArrAlc3 = Alc3LookUp(GetItem(DataLine, 4, ","))
                    ArrFlt = True
                Else
                    ArrAlc3 = ""
                    ArrFlt = False
                End If
                DepFltn = GetItem(DataLine, 7, ",")
                If DepFltn <> "" Then
                    DepAlc3 = Alc3LookUp(GetItem(DataLine, 6, ","))
                    DepFlt = True
                Else
                    DepAlc3 = ""
                    DepFlt = False
                End If
                TurnFlag = 0
                If ArrFlt Then TurnFlag = TurnFlag + 1
                If DepFlt Then TurnFlag = TurnFlag + 2
                
                CurRegn = GetItem(DataLine, 23, ",")
                CurFtyp = GetItem(DataLine, 24, ",")
                If CurFtyp = "" Then CurFtyp = DefaultFtyp
                If CurFtyp = "C" Then CurFtyp = "X"
                If CurFtyp = "" Then CurFtyp = "S"
                If CurCode = "D" Then CurFtyp = FtypOnDelete
                ArrStoa = GetItem(DataLine, 15, ",")
                If (Len(ArrStoa) < 12) And (ArrFlt) Then
                    varStoa = CedaFullDateToVb(GetItem(DataLine, 8, ",") & ArrStoa)
                Else
                    varStoa = CedaFullDateToVb(GetItem(DataLine, 15, ","))
                    ArrStoa = Mid(GetItem(DataLine, 15, ","), 9)
                End If
                DepStod = GetItem(DataLine, 16, ",")
                If (Len(DepStod) < 12) And (DepFlt) Then
                    varStod = CedaFullDateToVb(GetItem(DataLine, 8, ",") & DepStod)
                    If TurnFlag = 3 Then
                        varStod = DateAdd("d", DayOffset, varStod)
                        DepDate = Format(varStod, "yyyymmdd")
                        'If DepDate > SeasVpto Then SeasVpto = DepDate
                    End If
                Else
                    varStod = CedaFullDateToVb(GetItem(DataLine, 16, ","))
                    DepStod = Mid(GetItem(DataLine, 16, ","), 9)
                End If
                ArrFlca = GetItem(DataLine, 4, ",")
                DepFlcd = GetItem(DataLine, 6, ",")
                CurAct3 = GetItem(DataLine, 11, ",")
                If ImportAct3Act5 Then CurAct3 = CurAct3 & "/" & GetItem(DataLine, 25, ",")
                CurOrg3 = GetItem(DataLine, 13, ",")
                If (CurOrg3 = "") And (InStr("OAFOS,SSIM,SLOT", DataSystemType) = 0) Then CurOrg3 = GetItem(DataLine, 26, ",")
                ArrVia3 = GetItem(DataLine, 14, ",")
                If (ArrVia3 = "") And (InStr("OAFOS,SSIM,SLOT", DataSystemType) = 0) Then ArrVia3 = GetItem(DataLine, 27, ",")
                DepVia3 = GetItem(DataLine, 18, ",")
                If (DepVia3 = "") And (InStr("OAFOS,SSIM,SLOT", DataSystemType) = 0) Then DepVia3 = GetItem(DataLine, 29, ",")
                CurDes3 = GetItem(DataLine, 19, ",")
                If (CurDes3 = "") And (InStr("OAFOS,SSIM,SLOT", DataSystemType) = 0) Then CurDes3 = GetItem(DataLine, 28, ",")
                ArrTtyp = GetItem(DataLine, 20, ",")
                DepTtyp = GetItem(DataLine, 21, ",")
                ArrFlns = ""
                DepFlns = ""
                ArrFlno = BuildProperAftFlno(ArrFlca, ArrFltn, ArrFlns)
                DepFlno = BuildProperAftFlno(DepFlcd, DepFltn, DepFlns)
                If NewLayoutType Then
                    If CurOrg3 = "" Then CurOrg3 = ResolveDataLists("ORG3", "ROUT", ExtractLine, "....,....,....," & myViewFieldList)
                    If ArrVia3 = "" Then ArrVia3 = ResolveDataLists("VSA3", "ROUT", ExtractLine, "....,....,....," & myViewFieldList)
                    If DepVia3 = "" Then DepVia3 = ResolveDataLists("VSD3", "ROUT", ExtractLine, "....,....,....," & myViewFieldList)
                    If CurDes3 = "" Then CurDes3 = ResolveDataLists("DES3", "ROUT", ExtractLine, "....,....,....," & myViewFieldList)
                End If
                tmpRout = GetFieldValue("ROUT", ExtractLine, "....,....,....," & myViewFieldList)
                WkDayCnt = 0
                IsValidWeek = True
                InvalidDays = 0
                OutRange = 0
                
                ArrIdx = -1
                If TurnFlag = 3 Then
                    DepIdx = DateDiff("d", varStoa, varStod)
                Else
                    DepIdx = 0
                End If
                If CurDateDiff > 250 Then SetProgressBar 1, 1, CurDateDiff, ""
                CurDateCount = 0
                'If NoopCheckDoubles Then
                '    SeasVpfr = varVpfr
                '    SeasVpto = varVpto
                'End If
                
                ApdxArr = ""
                If (AddOnArrFields <> "") And (AddOnArrAftFld <> "") Then
                    ApdxArr = GetAppendixFields(AddOnArrFields, ExtractLine, PreFixFields & myViewFieldList)
                End If
                ApdxDep = ""
                If (AddOnDepFields <> "") And (AddOnDepAftFld <> "") Then
                    ApdxDep = GetAppendixFields(AddOnDepFields, ExtractLine, PreFixFields & myViewFieldList)
                End If
                
                
                varFday = varVpfr
                Do
                    If CurDateDiff > 250 Then CurDateCount = CurDateCount + 1
                    ArrIdx = ArrIdx + 1
                    strWkdy = Trim(Str(Weekday(varFday, vbMonday)))
                    If IsValidWeek And (InStr(strFrqd, strWkdy) > 0) Then
                        'Hit. Always valid for Arrivals or single Departures
                        If CurDateDiff > 250 Then SetProgressBar 1, 2, CurDateCount, ""
                        tmpData = Trim(Str(CurLine))
                        CurRkey = Right("00000" & tmpData, 5)
                        tmpData = Trim(Str(ArrIdx))
                        CurRkey = CurRkey & Right("00000" & tmpData, 5)
                        
                        Select Case TurnFlag
                            Case 1  'Single arrival
                                varCurStoa = varFday
                                ArrDate = Format(varCurStoa, "yyyymmdd")
                                If (ArrDate >= SeasVpfr) And (ArrDate <= SeasVpto) Then
                                    ArrFkey = TurnAftFkey(BuildAftFkey(ArrAlc3, ArrFltn, ArrFlns, ArrDate, "A"))
                                    ArrRota = ArrFkey
                                    If UseFkeyTime Then ArrRota = ArrFkey & ArrStoa
                                    tmpData = CurCode + "," + CurFtyp + "," + ArrFlno + ",A," + ArrDate + ArrStoa + "," + strWkdy + "," + CurAct3 + "," + CurOrg3 + "," + ArrVia3 + "," + ArrTtyp + "," + CurRegn + ",,," + tmpRout + ",    -,,," + ArrFkey + "," + CurRkey + "," + Str(CurLine) + "," + ArrRota + ",0,0,"
                                    tmpData = tmpData & ",,,," & ApdxArr & ",,"
                                    LineBuff = LineBuff + tmpData + vbLf
                                    BufferCount = BufferCount + 1
                                    LineCount = LineCount + 1
                                End If
                            Case 2  'Single departure
                                varCurStod = DateAdd("d", DepIdx, varFday)
                                DepDate = Format(varCurStod, "yyyymmdd")
                                If ((DepDate >= SeasVpfr) And (DepDate <= SeasVpto)) Or ((DepDate >= SeasVpfr) And (IsOverNight)) Then
                                    strWkdy = Trim(Str(Weekday(varCurStod, vbMonday)))
                                    DepFkey = TurnAftFkey(BuildAftFkey(DepAlc3, DepFltn, DepFlns, DepDate, "D"))
                                    DepRota = DepFkey
                                    If UseFkeyTime Then DepRota = DepFkey & DepStod
                                    tmpData = CurCode + "," + CurFtyp + "," + DepFlno + ",D," + DepDate + DepStod + "," + strWkdy + "," + CurAct3 + "," + CurDes3 + "," + DepVia3 + "," + DepTtyp + "," + CurRegn + ",,," + tmpRout + ",    -,,," + DepFkey + "," + CurRkey + "," + Str(CurLine) + "," + DepRota + ",0,0,"
                                    tmpData = tmpData & ",,,," & ApdxDep & ",,"
                                    LineBuff = LineBuff + tmpData + vbLf
                                    BufferCount = BufferCount + 1
                                    LineCount = LineCount + 1
                                End If
                            Case 3  'Rotation
                                ArrFkey = ""
                                DepFkey = ""
                                varCurStoa = varFday
                                ArrDate = Format(varCurStoa, "yyyymmdd")
                                If (ArrDate >= SeasVpfr) And (ArrDate <= SeasVpto) Then
                                    ArrWkdy = strWkdy
                                    ArrFkey = TurnAftFkey(BuildAftFkey(ArrAlc3, ArrFltn, ArrFlns, ArrDate, "A"))
                                    ArrRota = ArrFkey
                                    If UseFkeyTime Then ArrRota = ArrFkey & ArrStoa
                                    BufferCount = BufferCount + 1
                                    LineCount = LineCount + 1
                                    varCurStod = DateAdd("d", DepIdx, varFday)
                                    DepDate = Format(varCurStod, "yyyymmdd")
                                    If ((DepDate >= SeasVpfr) And (DepDate <= SeasVpto)) Or (RotationMode And Not NoopCheckDoubles) Or (((DepDate >= SeasVpfr) And (IsOverNight))) Then
                                        DepWkdy = Trim(Str(Weekday(varCurStod, vbMonday)))
                                        DepFkey = TurnAftFkey(BuildAftFkey(DepAlc3, DepFltn, DepFlns, DepDate, "D"))
                                        DepRota = DepFkey
                                        If UseFkeyTime Then DepRota = DepFkey & DepStod
                                        BufferCount = BufferCount + 1
                                        LineCount = LineCount + 1
                                    End If
                                End If
                                If (ArrFkey <> "") And (DepFkey <> "") Then
                                    tmpArrFlight = CurCode + "," + CurFtyp + "," + ArrFlno + ",A," + ArrDate + ArrStoa + "," + ArrWkdy + "," + CurAct3 + "," + CurOrg3 + "," + ArrVia3 + "," + ArrTtyp + "," + CurRegn + ",,," + tmpRout + "," + DepFlno + "," + DepDate + DepStod + ",D," + ArrFkey + "," + CurRkey + "," + Str(CurLine) + "," + ArrRota + ",0,0,"
                                    tmpArrFlight = tmpArrFlight & ",,,," & ApdxArr & ",,"
                                    LineBuff = LineBuff + tmpArrFlight + vbLf
                                    tmpDepFlight = CurCode + "," + CurFtyp + "," + DepFlno + ",D," + DepDate + DepStod + "," + DepWkdy + "," + CurAct3 + "," + CurDes3 + "," + DepVia3 + "," + DepTtyp + "," + CurRegn + ",,," + tmpRout + "," + ArrFlno + "," + ArrDate + ArrStoa + ",A," + DepFkey + "," + CurRkey + "," + Str(CurLine) + "," + DepRota + ",0,0,"
                                    tmpDepFlight = tmpDepFlight & ",,,," & ApdxDep & ",,"
                                    LineBuff = LineBuff + tmpDepFlight + vbLf
                                ElseIf ArrFkey <> "" Then
                                    tmpArrFlight = CurCode + "," + CurFtyp + "," + ArrFlno + ",A," + ArrDate + ArrStoa + "," + ArrWkdy + "," + CurAct3 + "," + CurOrg3 + "," + ArrVia3 + "," + ArrTtyp + "," + CurRegn + ",,," + tmpRout + ",,,," + ArrFkey + "," + CurRkey + "," + Str(CurLine) + "," + ArrRota + ",0,0,"
                                    tmpArrFlight = tmpArrFlight & ",,,," & ApdxArr & ",,"
                                    LineBuff = LineBuff + tmpArrFlight + vbLf
                                ElseIf DepFkey <> "" Then
                                    tmpDepFlight = CurCode + "," + CurFtyp + "," + DepFlno + ",D," + DepDate + DepStod + "," + DepWkdy + "," + CurAct3 + "," + CurDes3 + "," + DepVia3 + "," + DepTtyp + "," + CurRegn + ",,," + tmpRout + ",,,," + DepFkey + "," + CurRkey + "," + Str(CurLine) + "," + DepRota + ",0,0,"
                                    tmpDepFlight = tmpDepFlight & ",,,," & ApdxDep & ",,"
                                    LineBuff = LineBuff + tmpDepFlight + vbLf
                                End If
                            Case Else
                        End Select
                    End If
                    varFday = DateAdd("d", 1, varFday)
                    If Not IsValidWeek Then InvalidDays = InvalidDays - 1
                    WkDayCnt = WkDayCnt + 1
                    If WkDayCnt = 7 Then
                        If IsValidWeek Then
                            InvalidDays = (WeekFreq - 1) * 7
                            If InvalidDays > 0 Then IsValidWeek = False
                        Else
                            If InvalidDays = 0 Then
                                IsValidWeek = True
                            Else
                                'here we have a problem
                                'MsgBox "Please tell Berni that here is a problem with the weekly frequency."
                            End If
                        End If
                        WkDayCnt = 0
                    End If
                    If BufferCount >= 150 Then
                        NoopList(0).InsertBuffer LineBuff, vbLf
                        LineBuff = ""
                        BufferCount = NoopList(0).GetLineCount
                        NoopList(0).OnVScrollTo BufferCount
                        NoopList(0).Refresh
                        BufferCount = 0
                    End If
                Loop While varFday <= varVpto
                If CurDateDiff > 250 Then SetProgressBar 1, 3, 0, ""
            End If
            If StopAllLoops Then Exit For
        Next
        If LineBuff <> "" Then
            NoopList(0).InsertBuffer LineBuff, vbLf
            LineBuff = ""
        End If
        NoopList(0).OnVScrollTo 0
        NoopList(0).RedrawTab
        Me.MousePointer = 0
        If OutRange > 0 Then
            MsgBox OutRange & " flights out of period"
        End If
    Else
        MsgBox "Period range out of limits"
    End If
    CliFkeyCnt(0).Caption = CStr(NoopList(0).GetLineCount)
    SrvFkeyCnt(0).Caption = CStr(NoopList(1).GetLineCount)
    SetProgressBar 0, 3, 0, ""
    Me.Refresh
    If Not StopAllLoops Then CreateClientRouting
    Me.MousePointer = 0

End Sub
Private Function GetAppendixFields(ApdxFields As String, DataLine As String, DataFields As String) As String
    Dim itm As Integer
    Dim Fld As String
    Dim Dat As String
    Dim Result As String
    Result = ""
    itm = 0
    Fld = "START"
    While Fld <> ""
        itm = itm + 1
        Fld = GetItem(ApdxFields, itm, ",")
        If Fld <> "" Then
            Dat = GetFieldValue(Fld, DataLine, DataFields)
            Result = Result & Dat & ";"
        End If
    Wend
    If Result <> "" Then Result = Left(Result, Len(Result) - 1)
    GetAppendixFields = Result
End Function
Private Sub SearchInList(UseIndex As Integer, FindNextHit As Boolean, LookFullText As Boolean)
    Static LastIndex As Integer
    Dim Index As Integer
    Dim HitLst As String
    Dim HitRow As Long
    Dim NxtRow As Long
    Dim NewScroll As Long
    Dim LoopCount As Integer
    Dim tmpText As String
    Dim ColList As String
    Dim ColNbr As String
    Dim UseIdx As Integer
    Dim UseCol As Long
    
    Index = UseIndex
    If (FindNextHit) And (Index < 0) Then Index = LastIndex
    If LastIndex <> Index Then
        If Index >= 0 Then txtSearch(Index).BackColor = vbWhite
        If LastIndex >= 0 Then txtSearch(LastIndex).BackColor = LightGray
    End If
    LastIndex = Index
    
    If FindNextHit Then
        tmpText = txtSearch(Index).Text
        Select Case Index
            Case 0
                UseIdx = 0
                ColList = Trim(Str(FlnoCol))
                'UseCol = FlnoCol
            Case 1
                UseIdx = 1
                ColList = Trim(Str(FlnoCol))
                'UseCol = FlnoCol
            Case Else
                tmpText = ""
        End Select
        If tmpText <> "" Then
            LoopCount = 0
            Do
                ColNbr = GetItem(ColList, 1, ",")
                If ColNbr <> "" Then
                    UseCol = Val(ColNbr)
                    HitLst = NoopList(UseIdx).GetNextLineByColumnValue(UseCol, tmpText, 1)
                    HitRow = Val(HitLst)
                    If HitLst <> "" Then
                        If HitRow > 5 Then NewScroll = HitRow - 5 Else NewScroll = 0
                        NoopList(2).OnVScrollTo NewScroll
                    Else
                        NoopList(UseIdx).SetCurrentSelection -1
                        NoopList(2).OnVScrollTo 0
                    End If
                End If
                LoopCount = LoopCount + 1
            Loop While LoopCount < 2 And HitLst = ""
            NoopList(2).SetCurrentSelection HitRow
            NoopList(UseIdx).SetCurrentSelection HitRow
        End If
    End If
End Sub

Private Sub lblShrinkCover_Click(Index As Integer)
    Dim CurIdx As Integer
    CurIdx = Index
    If lblShrink(CurIdx).BorderStyle = 0 Then
        lblShrink(CurIdx).BorderStyle = 1
        lblShrink(CurIdx).ZOrder
    Else
        lblShrink(CurIdx).BorderStyle = 0
        lblShrinkCover(CurIdx).ZOrder
    End If
    AdjustShrinkButton True
End Sub

Private Sub NoopList_OnHScroll(Index As Integer, ByVal ColNo As Long)
    Select Case Index
        Case 0
            If Not NoEventEcho Then
                NoEventEcho = True
                NoopList(1).OnHScrollTo ColNo
                NoEventEcho = False
            End If
            RotaLink(0).OnHScrollTo ColNo
            RotaLink(2).OnHScrollTo ColNo
        Case 1
            If Not NoEventEcho Then
                NoEventEcho = True
                NoopList(0).OnHScrollTo ColNo
                NoEventEcho = False
            End If
            RotaLink(1).OnHScrollTo ColNo
            RotaLink(3).OnHScrollTo ColNo
        Case 2
            RotaLink(4).OnHScrollTo ColNo
            RotaLink(5).OnHScrollTo ColNo
        Case Else
    End Select
    NoopList(Index).SetFocus
End Sub

Private Sub NoopList_OnVScroll(Index As Integer, ByVal LineNo As Long)
    Dim ExtLineNo As Long
    Dim SyncLineOffs As Long
    Dim SlaveLineOffs As Long
    Dim VisLine As Long
    Dim SelLine As Long
    If Index = 2 Then
        ExtLineNo = Val(NoopList(2).GetColumnValue(LineNo, 4))
        If ExtLineNo >= 0 Then
            NoopList(0).OnVScrollTo ExtLineNo
            NoopList(1).OnVScrollTo ExtLineNo
            'SelLine = NoopList(2).GetCurrentSelected
            'VisLine = LineNo + ((NoopList(2).Height / Screen.TwipsPerPixelY) / NoopList(2).LineHeight) - 3
            'StopNestedCalls = True
            'If (SelLine >= LineNo) And (SelLine <= VisLine) Then NoopList(2).SetCurrentSelection SelLine
            'StopNestedCalls = False
            NoopList(2).Refresh
            NoopList(0).Refresh
            NoopList(1).Refresh
        Else
            NoopList(0).SetCurrentSelection -1
            NoopList(1).SetCurrentSelection -1
        End If
    End If
End Sub

Private Sub NoopList_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim ExtLineNo As Long
    Dim SyncLineOffs As Long
    Dim SlaveLineOffs As Long
    Dim tmpDat As String
    Dim tmpArrLineNo As Long
    Dim tmpDepLineNo As Long
    Dim LineStatus As Long
    Dim LineForeColor As Long
    Dim LineBackColor As Long
    Dim CurMarker As String
    Dim IsValidRotation As Boolean
    Select Case Index
        Case 0
            If (Not ImportIsBusy) And (Selected) And (LineNo >= 0) Then
                tmpDat = NoopList(0).GetColumnValue(LineNo, UrnoCol)
                If tmpDat = "" Then tmpDat = "-1"
                ExtLineNo = Val(tmpDat)
                If (Not NoopCheckDoubles) And (Not StartedAsCompareTool) Then
                    If ExtLineNo <> FlightExtract.ExtractData.GetCurrentSelected Then
                        FlightExtract.ExtractData.SetCurrentSelection ExtLineNo
                    End If
                ElseIf StartedAsCompareTool Then
                    If ExtLineNo <> TlxImpData(1).GetCurrentSelected Then
                        TlxImpData(1).OnVScrollTo ExtLineNo - 1
                        TlxImpData(1).SetCurrentSelection ExtLineNo
                    End If
                End If
            End If
        Case 1
            'If Selected And LineNo >= 0 Then SrvFkeyCnt(4).Caption = Trim(Str(LineNo + 1))
        Case 2
            If LineNo >= 0 Then
                'LineStatus = NoopList(2).GetLineStatusValue(LineNo)
                If Selected = True Then
                    'If LineStatus = 1 Then SetLineMarkers "SelMarker", Str(LineNo)
                    ExtLineNo = Val(NoopList(2).GetColumnValue(LineNo, 4))
                    If ExtLineNo >= 0 Then
                        SyncLineOffs = LineNo - NoopList(2).GetVScrollPos
                        SlaveLineOffs = ExtLineNo - NoopList(0).GetVScrollPos
                        If SyncLineOffs <> SlaveLineOffs Then NoopList(0).OnVScrollTo (ExtLineNo - SyncLineOffs)
                        SlaveLineOffs = ExtLineNo - NoopList(1).GetVScrollPos
                        If SyncLineOffs <> SlaveLineOffs Then NoopList(1).OnVScrollTo (ExtLineNo - SyncLineOffs)
                        NoopList(0).SetCurrentSelection ExtLineNo
                        NoopList(1).SetCurrentSelection ExtLineNo
                        CurPos(2).Caption = Trim(Str(LineNo + 1))
                        If Not ImportIsBusy Then
                            'If NewLayoutType Then
                                If chkWork(13).Value = 1 Then AnalyseRotaLinks ExtLineNo, False, True
                            'Else
                                'If chkWork(13).Value = 1 Then ShowRotationDetails LineNo, ExtLineNo, True, tmpArrLineNo, tmpDepLineNo, IsValidRotation
                            'End If
                            NoopList(2).Refresh
                            NoopList(0).Refresh
                            NoopList(1).Refresh
                            NoopList(2).SetFocus
                            HandleMultiSelect LineNo
                        End If
                    Else
                        NoopList(0).SetCurrentSelection -1
                        NoopList(1).SetCurrentSelection -1
                    End If
                Else
'                    If LineStatus = 1 Then
'                        NoopList(2).GetLineColor LineNo, LineForeColor, LineBackColor
'                        CurMarker = Trim(Str(LineBackColor))
'                        SetLineMarkers CurMarker, Str(LineNo)
'                    Else
'                        NoopList(2).ResetLineDecorations LineNo
'                    End If
                End If
            End If
        Case Else
    End Select
End Sub

Private Sub CheckRotationDetails()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim NewLine As Long
    Dim RotaLine(2) As Long
    Dim RotaItem(2) As String
    Dim RotaRegn(2) As String
    Dim CurCol As Long
    Dim tmpDat As String
    Dim Result As String
    Dim i As Integer
    Dim ChgFlag As Integer
    Dim CheckIt As Boolean
    Dim ArrFlight As Boolean
    Dim ArrLineNo As Long
    Dim DepLineNo As Long
    Dim IsValidRotation As Boolean
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim CurRkey(2) As String
    Dim RotaLineList(2) As String
    Dim RkeyCount(2) As Integer
    Dim CliTime As String
    Dim chkTime As String
    Dim tmpCode As String
    Dim tmpFtyp As String
    Dim tmpPrfl As String
    Dim FoundHistory As Boolean
    chkTime = GetImportCheckTime
    
    If (NewLayoutType) Or (StartedAsCompareTool) Or (InStr("OAFOS,SSIM,SLOT", DataSystemType) > 0) Then
        SetProgressBar 0, 1, 10, "Create Rotation Index List ..."
        For i = 0 To 1
            NoopList(i).IndexCreate "ROTATION", RkeyCol
        Next
        SetProgressBar 0, 3, -1, ""
        MaxLine = NoopList(0).GetLineCount - 1
        SetProgressBar 0, 1, MaxLine, "CrossCheck Arrival Rotation Links ..."
        
        For CurLine = 0 To MaxLine
            If StopAllLoops Then Exit For
            If CurLine Mod 200 = 0 Then SetProgressBar 0, 2, CurLine, ""
            If CurLine Mod 100 = 0 Then
                NoopList(0).OnVScrollTo CurLine
                NoopList(0).Refresh
                NoopList(1).OnVScrollTo CurLine
                NoopList(1).Refresh
            End If
            tmpCode = NoopList(0).GetColumnValue(CurLine, 0)
            CliTime = Trim(NoopList(0).GetColumnValue(CurLine, TimeCol))
            If (CliTime <> "") And (CliTime < chkTime) Then
                NoopList(0).SetLineColor CurLine, LimitColor, vbBlack
                tmpPrfl = NoopList(0).GetColumnValue(CurLine, PrflCol)
                NoopList(0).SetColumnValue CurLine, PrflCol, tmpPrfl & "#"
                FoundHistory = True
            End If
            tmpDat = Trim(NoopList(0).GetColumnValue(CurLine, AdidCol))
            If tmpDat = "" Then tmpDat = Trim(NoopList(1).GetColumnValue(CurLine, AdidCol))
            If tmpDat = "A" Then AnalyseRotaLinks CurLine, True, False
            tmpFtyp = Trim(NoopList(1).GetColumnValue(CurLine, FtypCol))
            If (tmpCode = "D") Then
                tmpDat = Trim(NoopList(1).GetColumnValue(CurLine, UrnoCol))
                If (tmpDat <> "") And (InStr("XN", tmpFtyp) = 0) Then
                    NoopList(0).SetLineColor CurLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                    NoopList(1).SetLineColor CurLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                Else
                    NoopList(0).SetLineColor CurLine, SrvFkeyCnt(6).ForeColor, SrvFkeyCnt(6).BackColor
                    NoopList(1).SetLineColor CurLine, SrvFkeyCnt(6).ForeColor, SrvFkeyCnt(6).BackColor
                End If
            End If
        Next
        SetProgressBar 0, 3, -1, ""
        MaxLine = NoopList(0).GetLineCount - 1
        SetProgressBar 0, 1, MaxLine, "CrossCheck Departure Rotation Links ..."
        For CurLine = 0 To MaxLine
            If StopAllLoops Then Exit For
            If CurLine Mod 200 = 0 Then SetProgressBar 0, 2, CurLine, ""
            If CurLine Mod 100 = 0 Then
                NoopList(0).OnVScrollTo CurLine
                NoopList(0).Refresh
                NoopList(1).OnVScrollTo CurLine
                NoopList(1).Refresh
            End If
            tmpCode = NoopList(0).GetColumnValue(CurLine, 0)
            tmpDat = Trim(NoopList(0).GetColumnValue(CurLine, ChckCol))
            If tmpDat <> "1" Then
                CliTime = Trim(NoopList(0).GetColumnValue(CurLine, TimeCol))
                If (CliTime <> "") And (CliTime < chkTime) Then
                    NoopList(0).SetLineColor CurLine, LimitColor, vbBlack
                    tmpPrfl = NoopList(0).GetColumnValue(CurLine, PrflCol)
                    NoopList(0).SetColumnValue CurLine, PrflCol, tmpPrfl & "#"
                    FoundHistory = True
                End If
                AnalyseRotaLinks CurLine, True, False
            End If
            If tmpCode = "D" Then
                tmpDat = Trim(NoopList(1).GetColumnValue(CurLine, UrnoCol))
                tmpFtyp = Trim(NoopList(1).GetColumnValue(CurLine, FtypCol))
                If (tmpDat <> "") And (InStr("XN", tmpFtyp) = 0) Then
                    NoopList(0).SetLineColor CurLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                    NoopList(1).SetLineColor CurLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                Else
                    NoopList(0).SetLineColor CurLine, SrvFkeyCnt(6).ForeColor, SrvFkeyCnt(6).BackColor
                    NoopList(1).SetLineColor CurLine, SrvFkeyCnt(6).ForeColor, SrvFkeyCnt(6).BackColor
                End If
            End If
        Next
        SetProgressBar 0, 3, -1, ""
        If FoundHistory Then
            CliFkeyCnt(7).BackColor = vbBlack
            CliFkeyCnt(7).ForeColor = LimitColor
        Else
            CliFkeyCnt(7).BackColor = vbButtonFace
            CliFkeyCnt(7).ForeColor = vbButtonText
        End If
        NoopList(0).OnVScrollTo 0
        NoopList(0).Refresh
        NoopList(1).OnVScrollTo 0
        NoopList(1).Refresh
    End If
End Sub

Private Function AnalyseRotaLinks(CurLineNo As Long, PreCheck As Boolean, ShowDetails As Boolean) As String
    Dim IsValidImport As Boolean
    Dim LineForeColor As Long
    Dim LineBackColor As Long
    Dim tmpRotaRec As String
    Dim CurRkey As String
    Dim CurAdid As String
    Dim RotaLines As String
    Dim ImpLines(2) As Long
    Dim SrvLines(2) As Long
    Dim tmpLineNo As Long
    Dim OutLineNo As Long
    Dim OutMaxLine As Long
    Dim OutLineCount As Long
    Dim OutRecord As String
    'Dim GrpLineIdx As String
    Dim i As Integer
    Dim j As Integer
    
    If ShowDetails Then
        For i = 0 To 5
            RotaLink(i).ResetContent
        Next
    End If
    For i = 0 To 8
        ArrFlightData(i) = EmptyLine
        DepFlightData(i) = EmptyLine
        ArrFlightLineNo(i) = -1
        DepFlightLineNo(i) = -1
        IsArrShiftLeft(i) = False
        IsDepShiftLeft(i) = False
        IsRotation(i) = False
        IsFlightPair(i) = False
    Next
    RotaLines = ""
    CurRkey = Trim(NoopList(0).GetColumnValue(CurLineNo, RkeyCol))
    If CurRkey <> "" Then RotaLines = NoopList(0).GetLinesByIndexValue("ROTATION", CurRkey, 0)
    If RotaLines <> "" Then RotaLines = RotaLines & ",-1" Else RotaLines = "-1,-1"
    tmpLineNo = Val(GetItem(RotaLines, 1, ","))
    If tmpLineNo = CurLineNo Then
        ImpLines(0) = tmpLineNo
        ImpLines(1) = Val(GetItem(RotaLines, 2, ","))
    Else
        ImpLines(0) = Val(GetItem(RotaLines, 2, ","))
        ImpLines(1) = tmpLineNo
    End If
    'If ItemCount(RotaLines, ",") > 2 Then
    '    'MsgBox "Hallo"
    'End If
    RotaLines = ""
    CurRkey = Trim(NoopList(1).GetColumnValue(CurLineNo, RkeyCol))
    If CurRkey <> "" Then RotaLines = NoopList(1).GetLinesByIndexValue("ROTATION", CurRkey, 0)
    If RotaLines <> "" Then RotaLines = RotaLines & ",-1" Else RotaLines = "-1,-1"
    tmpLineNo = Val(GetItem(RotaLines, 1, ","))
    If tmpLineNo = CurLineNo Then
        SrvLines(0) = tmpLineNo
        SrvLines(1) = Val(GetItem(RotaLines, 2, ","))
    Else
        SrvLines(0) = Val(GetItem(RotaLines, 2, ","))
        SrvLines(1) = tmpLineNo
    End If
    'If ItemCount(RotaLines, ",") > 2 Then
    '    'MsgBox "Hallo"
    'End If
    CurAdid = Trim(NoopList(0).GetColumnValue(CurLineNo, AdidCol))
    If CurAdid = "" Then CurAdid = Trim(NoopList(1).GetColumnValue(CurLineNo, AdidCol))
    If (Not PreCheck) And (Not ShowDetails) Then CmdLineIdx = CurAdid & Right("000000" & Trim(Str(CurLineNo)), 6)
    
    Select Case CurAdid
        Case "A"
            ArrFlightLineNo(0) = ImpLines(0)
            ArrFlightLineNo(1) = SrvLines(0)
            ArrFlightLineNo(2) = ImpLines(0)
            ArrFlightLineNo(3) = ImpLines(0)
            DepFlightLineNo(0) = ImpLines(1)
            DepFlightLineNo(1) = SrvLines(1)
            DepFlightLineNo(2) = ImpLines(1)
            DepFlightLineNo(3) = ImpLines(1)
        Case "D"
            ArrFlightLineNo(0) = ImpLines(1)
            ArrFlightLineNo(1) = SrvLines(1)
            ArrFlightLineNo(2) = ImpLines(1)
            ArrFlightLineNo(3) = ImpLines(1)
            DepFlightLineNo(0) = ImpLines(0)
            DepFlightLineNo(1) = SrvLines(0)
            DepFlightLineNo(2) = ImpLines(0)
            DepFlightLineNo(3) = ImpLines(0)
        Case Else
    End Select
    If ArrFlightLineNo(3) < 0 Then
        ArrFlightLineNo(3) = ArrFlightLineNo(1)
    End If
    If DepFlightLineNo(3) < 0 Then
        DepFlightLineNo(3) = DepFlightLineNo(1)
    End If
    If ArrFlightLineNo(2) < 0 Then
        ArrFlightLineNo(2) = ArrFlightLineNo(3)
        If ArrFlightLineNo(2) >= 0 Then IsArrShiftLeft(2) = True
    End If
    If DepFlightLineNo(2) < 0 Then
        DepFlightLineNo(2) = DepFlightLineNo(3)
        If DepFlightLineNo(2) >= 0 Then IsDepShiftLeft(2) = True
    End If
    If ArrFlightLineNo(0) < 0 Then
        ArrFlightLineNo(0) = ArrFlightLineNo(1)
        If ArrFlightLineNo(0) >= 0 Then IsArrShiftLeft(0) = True
    End If
    If DepFlightLineNo(0) < 0 Then
        DepFlightLineNo(0) = DepFlightLineNo(1)
        If DepFlightLineNo(0) >= 0 Then IsDepShiftLeft(0) = True
    End If
    For i = 0 To 1
        j = i + 2
        If ArrFlightLineNo(i) >= 0 Then ArrFlightData(i) = NoopList(i).GetLineValues(ArrFlightLineNo(i))
        If DepFlightLineNo(i) >= 0 Then DepFlightData(i) = NoopList(i).GetLineValues(DepFlightLineNo(i))
        If ArrFlightLineNo(j) >= 0 Then ArrFlightData(j) = NoopList(i).GetLineValues(ArrFlightLineNo(j))
        If DepFlightLineNo(j) >= 0 Then DepFlightData(j) = NoopList(i).GetLineValues(DepFlightLineNo(j))
    Next
    PrepareRotationView
    If PreCheck Then
        If ArrFlightLineNo(0) >= 0 Then NoopList(0).SetColumnValue ArrFlightLineNo(0), ChckCol, "1"
        If DepFlightLineNo(0) >= 0 Then NoopList(0).SetColumnValue DepFlightLineNo(0), ChckCol, "1"
    End If
    
    'If ShowDetails Then
    '    If chkRota(5).Value = 1 Then
    '        ShowFlightDetails CurLineNo, CurAdid
    '    Else
    '        ShowRotaDetails CurLineNo, CurAdid
    '    End If
    'End If
    For i = 0 To 3
        If Trim(GetRealItem(ArrFlightData(i), FkeyCol, ",")) = "" Then
            ArrFlightData(i) = ""
            ArrFlightLineNo(i) = -1
        End If
        If Trim(GetRealItem(DepFlightData(i), FkeyCol, ",")) = "" Then
            DepFlightData(i) = ""
            DepFlightLineNo(i) = -1
        End If
    Next
    IsValidImport = CrossCheckFlights(PreCheck, ShowDetails)
    If ShowDetails Then
        If chkRota(5).Value = 1 Then
            ShowFlightDetails CurLineNo, CurAdid
        Else
            ShowRotaDetails CurLineNo, CurAdid
        End If
        For i = 0 To 5
            RotaLink(i).RedrawTab
        Next
    End If
    
    'GrpLineIdx = Trim(Str(CurLineNo))
    'If (ArrFlightLineNo(0) >= 0) And (CurLineNo <> ArrFlightLineNo(0)) And (Not IsArrShiftLeft(0)) Then GrpLineIdx = GrpLineIdx & "," & Trim(Str(ArrFlightLineNo(0)))
    'If (DepFlightLineNo(0) >= 0) And (CurLineNo <> DepFlightLineNo(0)) And (Not IsDepShiftLeft(0)) Then GrpLineIdx = GrpLineIdx & "," & Trim(Str(DepFlightLineNo(0)))
    
    If (Not PreCheck) And (Not ShowDetails) Then
        'If ArrFlightLineNo(0) >= 0 Then NoopList(0).SetColumnValue ArrFlightLineNo(0), DoneCol, "1"
        'If DepFlightLineNo(0) >= 0 Then NoopList(0).SetColumnValue DepFlightLineNo(0), DoneCol, "1"
    End If
    AnalyseRotaLinks = CmdLineIdx
End Function
Private Sub ShowFlightDetails(CurLineNo As Long, CurAdid As String)
    Dim LineForeColor As Long
    Dim LineBackColor As Long
    Dim tmpRotaRec As String
    Dim RotaLines As String
    Dim tmpLineNo As Long
    Dim i As Integer
    Dim j As Integer
    For i = 0 To 1
        j = i + 2
        RotaLink(i).InsertTextLine ArrFlightData(i), False
        RotaLink(i).SetLineTag 0, Trim(Str(ArrFlightLineNo(i)))
        RotaLink(i).InsertTextLine DepFlightData(i), False
        RotaLink(i).SetLineTag 1, Trim(Str(DepFlightLineNo(i)))
        RotaLink(j).InsertTextLine ArrFlightData(j), False
        RotaLink(j).SetLineTag 0, Trim(Str(ArrFlightLineNo(j)))
        RotaLink(j).InsertTextLine DepFlightData(j), False
        RotaLink(j).SetLineTag 1, Trim(Str(DepFlightLineNo(j)))
        If ArrFlightLineNo(i) >= 0 Then
            NoopList(i).GetLineColor ArrFlightLineNo(i), LineForeColor, LineBackColor
            RotaLink(i).SetLineColor 0, LineForeColor, LineBackColor
            'SetFieldMarkers RotaLink(i), 0, "CellDiff", NoopList(i).GetLineTag(ArrFlightLineNo(i))
        Else
            RotaLink(i).SetLineColor 0, vbBlack, EmptyLineColor
        End If
        If ArrFlightLineNo(j) >= 0 Then
            NoopList(i).GetLineColor ArrFlightLineNo(j), LineForeColor, LineBackColor
            If (LineBackColor = CliFkeyCnt(1).BackColor) Or (LineBackColor = CliFkeyCnt(2).BackColor) Then
                LineBackColor = CliFkeyCnt(6).BackColor
            ElseIf LineBackColor = SrvFkeyCnt(5).BackColor Then
                LineForeColor = SrvFkeyCnt(4).ForeColor
                LineBackColor = SrvFkeyCnt(4).BackColor
            End If
            RotaLink(j).SetLineColor 0, LineForeColor, LineBackColor
            'SetFieldMarkers RotaLink(j), 0, "CellDiff", NoopList(i).GetLineTag(ArrFlightLineNo(j))
        Else
            RotaLink(j).SetLineColor 0, vbBlack, EmptyLineColor
        End If
        If DepFlightLineNo(i) >= 0 Then
            NoopList(i).GetLineColor DepFlightLineNo(i), LineForeColor, LineBackColor
            RotaLink(i).SetLineColor 1, LineForeColor, LineBackColor
            'SetFieldMarkers RotaLink(i), 1, "CellDiff", NoopList(i).GetLineTag(DepFlightLineNo(i))
        Else
            RotaLink(i).SetLineColor 1, vbBlack, EmptyLineColor
        End If
        If DepFlightLineNo(j) >= 0 Then
            NoopList(i).GetLineColor DepFlightLineNo(j), LineForeColor, LineBackColor
            If (LineBackColor = CliFkeyCnt(1).BackColor) Or (LineBackColor = CliFkeyCnt(2).BackColor) Then
                LineBackColor = CliFkeyCnt(6).BackColor
            ElseIf LineBackColor = SrvFkeyCnt(5).BackColor Then
                LineForeColor = SrvFkeyCnt(4).ForeColor
                LineBackColor = SrvFkeyCnt(4).BackColor
            End If
            RotaLink(j).SetLineColor 1, LineForeColor, LineBackColor
            'SetFieldMarkers RotaLink(j), 1, "CellDiff", NoopList(i).GetLineTag(DepFlightLineNo(j))
        Else
            RotaLink(j).SetLineColor 1, vbBlack, EmptyLineColor
        End If
    Next
    tmpRotaRec = Trim(Str(CurLineNo))
    RotaLines = NoopList(2).GetLinesByIndexValue("LineIdx", tmpRotaRec, 0)
    If RotaLines <> "" Then
        tmpLineNo = Val(RotaLines)
        tmpRotaRec = NoopList(2).GetLineValues(tmpLineNo)
        NoopList(2).GetLineColor tmpLineNo, LineForeColor, LineBackColor
        Select Case CurAdid
            Case "A"
                RotaLink(4).InsertTextLine tmpRotaRec, False
                RotaLink(4).InsertTextLine ",,,,-1", False
                RotaLink(4).SetLineColor 0, LineForeColor, LineBackColor
                RotaLink(4).SetLineColor 1, vbBlack, EmptyLineColor
            Case "D"
                RotaLink(4).InsertTextLine ",,,,-1", False
                RotaLink(4).InsertTextLine tmpRotaRec, False
                RotaLink(4).SetLineColor 0, vbBlack, EmptyLineColor
                RotaLink(4).SetLineColor 1, LineForeColor, LineBackColor
            Case Else
        End Select
    End If
    tmpRotaRec = RotaLink(2).GetColumnValues(0, RotaSelFields) & "," & Str(ArrFlightLineNo(2))
    If GetItem(tmpRotaRec, 1, ",") = "" Then
        tmpRotaRec = RotaLink(3).GetColumnValues(0, RotaSelFields) & "," & Str(ArrFlightLineNo(3))
        If GetItem(tmpRotaRec, 1, ",") = "" Then tmpRotaRec = ",,,,-1"
    End If
    RotaLink(5).InsertTextLine tmpRotaRec, False
    If GetItem(tmpRotaRec, 1, ",") <> "" Then
        RotaLink(2).GetLineColor 0, LineForeColor, LineBackColor
        RotaLink(5).SetLineColor 0, LineForeColor, LineBackColor
    Else
        RotaLink(5).SetLineColor 0, vbBlack, EmptyLineColor
    End If
    
    tmpRotaRec = RotaLink(2).GetColumnValues(1, RotaSelFields) & "," & Str(DepFlightLineNo(2))
    If GetItem(tmpRotaRec, 1, ",") = "" Then
        tmpRotaRec = RotaLink(3).GetColumnValues(1, RotaSelFields) & "," & Str(DepFlightLineNo(3))
        If GetItem(tmpRotaRec, 1, ",") = "" Then tmpRotaRec = ",,,,-1"
    End If
    RotaLink(5).InsertTextLine tmpRotaRec, False
    If GetItem(tmpRotaRec, 1, ",") <> "" Then
        RotaLink(2).GetLineColor 1, LineForeColor, LineBackColor
        RotaLink(5).SetLineColor 1, LineForeColor, LineBackColor
    Else
        RotaLink(5).SetLineColor 1, vbBlack, EmptyLineColor
    End If
    
    ShowMarkers
    
End Sub
Private Sub ShowRotaDetails(CurLineNo As Long, CurAdid As String)
    Dim LineForeColor As Long
    Dim LineBackColor As Long
    Dim tmpRotaRec As String
    Dim RotaLines As String
    Dim MarkerList As String
    Dim tmpLineNo As Long
    Dim i As Integer
    Dim j As Integer
    
    RotaLink(2).InsertTextLine ArrFlightData(2), False
    RotaLink(2).InsertTextLine DepFlightData(2), False
    RotaLink(3).InsertTextLine ArrFlightData(4), False
    RotaLink(3).InsertTextLine DepFlightData(4), False
    RotaLink(0).InsertTextLine ArrFlightData(0), False
    RotaLink(0).InsertTextLine DepFlightData(0), False
    RotaLink(1).InsertTextLine ArrFlightData(5), False
    RotaLink(1).InsertTextLine DepFlightData(5), False
    
    If ArrFlightLineNo(0) >= 0 Then
        NoopList(0).GetLineColor ArrFlightLineNo(0), LineForeColor, LineBackColor
        RotaLink(0).SetLineColor 0, LineForeColor, LineBackColor
        'GetKeyItem MarkerList, NoopList(0).GetLineTag(ArrFlightLineNo(0)), "{=UPD=}", "{="
        'SetFieldMarkers RotaLink(0), 0, "CellDiff", MarkerList
    Else
        RotaLink(0).SetLineColor 0, vbBlack, EmptyLineColor
    End If
    If DepFlightLineNo(0) >= 0 Then
        NoopList(0).GetLineColor DepFlightLineNo(0), LineForeColor, LineBackColor
        RotaLink(0).SetLineColor 1, LineForeColor, LineBackColor
        'GetKeyItem MarkerList, NoopList(0).GetLineTag(DepFlightLineNo(0)), "{=UPD=}", "{="
        'SetFieldMarkers RotaLink(0), 1, "CellDiff", MarkerList
    Else
        RotaLink(0).SetLineColor 1, vbBlack, EmptyLineColor
    End If
    If ArrFlightLineNo(2) >= 0 Then
        NoopList(0).GetLineColor ArrFlightLineNo(2), LineForeColor, LineBackColor
        RotaLink(2).SetLineColor 0, LineForeColor, LineBackColor
        'GetKeyItem MarkerList, NoopList(0).GetLineTag(ArrFlightLineNo(2)), "{=UPD=}", "{="
        'SetFieldMarkers RotaLink(2), 0, "CellDiff", MarkerList
    Else
        RotaLink(2).SetLineColor 0, vbBlack, EmptyLineColor
    End If
    If DepFlightLineNo(2) >= 0 Then
        NoopList(0).GetLineColor DepFlightLineNo(2), LineForeColor, LineBackColor
        RotaLink(2).SetLineColor 1, LineForeColor, LineBackColor
        'GetKeyItem MarkerList, NoopList(0).GetLineTag(DepFlightLineNo(2)), "{=UPD=}", "{="
        'SetFieldMarkers RotaLink(2), 1, "CellDiff", MarkerList
    Else
        RotaLink(2).SetLineColor 1, vbBlack, EmptyLineColor
    End If
    
    If ArrFlightLineNo(4) >= 0 Then
        NoopList(1).GetLineColor ArrFlightLineNo(4), LineForeColor, LineBackColor
        RotaLink(3).SetLineColor 0, LineForeColor, LineBackColor
        'GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(4)), "{=UPD=}", "{="
        'SetFieldMarkers RotaLink(3), 0, "CellDiff", MarkerList
    Else
        RotaLink(3).SetLineColor 0, vbBlack, EmptyLineColor
    End If
    If DepFlightLineNo(4) >= 0 Then
        NoopList(1).GetLineColor DepFlightLineNo(4), LineForeColor, LineBackColor
        RotaLink(3).SetLineColor 1, LineForeColor, LineBackColor
        'GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(4)), "{=UPD=}", "{="
        'SetFieldMarkers RotaLink(3), 1, "CellDiff", MarkerList
    Else
        RotaLink(3).SetLineColor 1, vbBlack, EmptyLineColor
    End If
    If ArrFlightLineNo(5) >= 0 Then
        NoopList(1).GetLineColor ArrFlightLineNo(5), LineForeColor, LineBackColor
        RotaLink(1).SetLineColor 0, LineForeColor, LineBackColor
        'GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(5)), "{=UPD=}", "{="
        'SetFieldMarkers RotaLink(1), 0, "CellDiff", MarkerList
    Else
        RotaLink(1).SetLineColor 0, vbBlack, EmptyLineColor
    End If
    If DepFlightLineNo(5) >= 0 Then
        NoopList(1).GetLineColor DepFlightLineNo(5), LineForeColor, LineBackColor
        RotaLink(1).SetLineColor 1, LineForeColor, LineBackColor
        'GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(5)), "{=UPD=}", "{="
        'SetFieldMarkers RotaLink(1), 1, "CellDiff", MarkerList
    Else
        RotaLink(1).SetLineColor 1, vbBlack, EmptyLineColor
    End If
    
    tmpRotaRec = RotaLink(2).GetColumnValues(0, RotaSelFields) & "," & Str(ArrFlightLineNo(2))
    If GetItem(tmpRotaRec, 1, ",") = "" Then
        tmpRotaRec = RotaLink(3).GetColumnValues(0, RotaSelFields) & "," & Str(ArrFlightLineNo(3))
        If GetItem(tmpRotaRec, 1, ",") = "" Then tmpRotaRec = ",,,,-1"
    End If
    RotaLink(5).InsertTextLine tmpRotaRec, False
    RotaLink(5).InsertTextLine ",,,,-1", False
    RotaLink(2).GetLineColor 0, LineForeColor, LineBackColor
    RotaLink(5).SetLineColor 0, LineForeColor, LineBackColor
    RotaLink(5).SetLineColor 1, vbBlack, EmptyLineColor
    
    tmpRotaRec = RotaLink(0).GetColumnValues(1, RotaSelFields) & "," & Str(DepFlightLineNo(0))
    If GetItem(tmpRotaRec, 1, ",") = "" Then
        tmpRotaRec = RotaLink(1).GetColumnValues(1, RotaSelFields) & "," & Str(DepFlightLineNo(1))
        If GetItem(tmpRotaRec, 1, ",") = "" Then tmpRotaRec = ",,,,-1"
    End If
    RotaLink(4).InsertTextLine ",,,,-1", False
    RotaLink(4).InsertTextLine tmpRotaRec, False
    RotaLink(0).GetLineColor 1, LineForeColor, LineBackColor
    RotaLink(4).SetLineColor 0, vbBlack, EmptyLineColor
    RotaLink(4).SetLineColor 1, LineForeColor, LineBackColor
    
    ShowMarkers
    
    RotaLink(0).SetLineTag 0, Trim(Str(ArrFlightLineNo(0)))
    RotaLink(0).SetLineTag 1, Trim(Str(DepFlightLineNo(0)))
    RotaLink(1).SetLineTag 0, Trim(Str(ArrFlightLineNo(5)))
    RotaLink(1).SetLineTag 1, Trim(Str(DepFlightLineNo(5)))
    RotaLink(2).SetLineTag 0, Trim(Str(ArrFlightLineNo(2)))
    RotaLink(2).SetLineTag 1, Trim(Str(DepFlightLineNo(2)))
    RotaLink(3).SetLineTag 0, Trim(Str(ArrFlightLineNo(4)))
    RotaLink(3).SetLineTag 1, Trim(Str(DepFlightLineNo(4)))
    
End Sub

Private Sub ShowMarkers()
    Dim MarkerList As String
    
    'Top Left: Client Rotation
    GetKeyItem MarkerList, NoopList(0).GetLineTag(ArrFlightLineNo(2)), "{=UPD=}", "{="
    If MarkerList <> "" Then SetFieldMarkers RotaLink(2), 0, "CellDiff", MarkerList
    GetKeyItem MarkerList, NoopList(0).GetLineTag(DepFlightLineNo(2)), "{=UPD=}", "{="
    If MarkerList <> "" Then SetFieldMarkers RotaLink(2), 1, "CellDiff", MarkerList
    If Not IsArrShiftLeft(2) Then
        GetKeyItem MarkerList, NoopList(0).GetLineTag(ArrFlightLineNo(2)), "{=DENY=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(2), 0, "RotaDiff", MarkerList
        GetKeyItem MarkerList, NoopList(0).GetLineTag(DepFlightLineNo(2)), "{=DENY=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(2), 1, "RotaDiff", MarkerList
    End If
    GetKeyItem MarkerList, NoopList(0).GetLineTag(ArrFlightLineNo(2)), "{=SPR=}", "{="
    If MarkerList <> "" Then SetFieldMarkers RotaLink(2), 0, MarkerList, "0"
    GetKeyItem MarkerList, NoopList(0).GetLineTag(DepFlightLineNo(2)), "{=SPR=}", "{="
    If MarkerList <> "" Then SetFieldMarkers RotaLink(2), 1, MarkerList, "0"
    If IsArrShiftLeft(2) Then
        SetFieldMarkers RotaLink(2), 0, "SrvRota", "10"
        SetFieldMarkers RotaLink(3), 0, "SrvRota", "2"
    End If
    If IsDepShiftLeft(2) Then
        SetFieldMarkers RotaLink(2), 1, "SrvRota", "10"
        SetFieldMarkers RotaLink(3), 1, "SrvRota", "2"
    End If
    
    'Bottom Left: Client Rotation
    GetKeyItem MarkerList, NoopList(0).GetLineTag(ArrFlightLineNo(0)), "{=UPD=}", "{="
    If MarkerList <> "" Then SetFieldMarkers RotaLink(0), 0, "CellDiff", MarkerList
    GetKeyItem MarkerList, NoopList(0).GetLineTag(DepFlightLineNo(0)), "{=UPD=}", "{="
    If MarkerList <> "" Then SetFieldMarkers RotaLink(0), 1, "CellDiff", MarkerList
    If Not IsArrShiftLeft(2) Then
        GetKeyItem MarkerList, NoopList(0).GetLineTag(ArrFlightLineNo(0)), "{=DENY=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(0), 0, "RotaDiff", MarkerList
        GetKeyItem MarkerList, NoopList(0).GetLineTag(DepFlightLineNo(0)), "{=DENY=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(0), 1, "RotaDiff", MarkerList
    End If
    GetKeyItem MarkerList, NoopList(0).GetLineTag(ArrFlightLineNo(0)), "{=SPR=}", "{="
    If MarkerList <> "" Then SetFieldMarkers RotaLink(0), 0, MarkerList, "0"
    GetKeyItem MarkerList, NoopList(0).GetLineTag(DepFlightLineNo(0)), "{=SPR=}", "{="
    If MarkerList <> "" Then SetFieldMarkers RotaLink(0), 1, MarkerList, "0"
    If IsArrShiftLeft(0) Then
        SetFieldMarkers RotaLink(0), 0, "SrvRota", "10"
        SetFieldMarkers RotaLink(1), 0, "SrvRota", "2"
    End If
    If IsDepShiftLeft(0) Then
        SetFieldMarkers RotaLink(0), 1, "SrvRota", "10"
        SetFieldMarkers RotaLink(1), 1, "SrvRota", "2"
    End If

'nooplist(0).GetLineTagKeyItem( ) might also be used !
    
    If chkRota(5).Value = 1 Then        'Flights
        'Top Right: Server Flights
        GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(3)), "{=UPD=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(3), 0, "CellDiff", MarkerList
        GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(3)), "{=UPD=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(3), 1, "CellDiff", MarkerList
        If Not IsArrShiftLeft(2) Then
            GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(3)), "{=DENY=}", "{="
            If MarkerList <> "" Then SetFieldMarkers RotaLink(3), 0, "RotaDiff", MarkerList
            GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(3)), "{=DENY=}", "{="
            If MarkerList <> "" Then SetFieldMarkers RotaLink(3), 1, "RotaDiff", MarkerList
        End If
        GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(3)), "{=SPR=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(3), 0, MarkerList, "0"
        GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(3)), "{=SPR=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(3), 1, MarkerList, "0"
        
        'Bottom Right: Selected Server Rotation
        GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(1)), "{=UPD=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(1), 0, "CellDiff", MarkerList
        GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(1)), "{=UPD=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(1), 1, "CellDiff", MarkerList
        If Not IsArrShiftLeft(2) Then
            GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(1)), "{=DENY=}", "{="
            If MarkerList <> "" Then SetFieldMarkers RotaLink(1), 0, "RotaDiff", MarkerList
            GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(1)), "{=DENY=}", "{="
            If MarkerList <> "" Then SetFieldMarkers RotaLink(1), 1, "RotaDiff", MarkerList
        End If
        GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(1)), "{=SPR=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(1), 0, MarkerList, "0"
        GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(1)), "{=SPR=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(1), 1, MarkerList, "0"
    Else        'Rotations
        'Top Right: Server Arrival Rotation
        GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(4)), "{=UPD=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(3), 0, "CellDiff", MarkerList
        GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(4)), "{=UPD=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(3), 1, "CellDiff", MarkerList
        If Not IsArrShiftLeft(2) Then
            GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(4)), "{=DENY=}", "{="
            If MarkerList <> "" Then SetFieldMarkers RotaLink(3), 0, "RotaDiff", MarkerList
            GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(4)), "{=DENY=}", "{="
            If MarkerList <> "" Then SetFieldMarkers RotaLink(3), 1, "RotaDiff", MarkerList
        End If
        GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(4)), "{=SPR=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(3), 0, MarkerList, "0"
        GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(4)), "{=SPR=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(3), 1, MarkerList, "0"
        
        'Bottom Right: Server Departure Rotation
        GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(5)), "{=UPD=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(1), 0, "CellDiff", MarkerList
        GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(5)), "{=UPD=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(1), 1, "CellDiff", MarkerList
        If Not IsArrShiftLeft(2) Then
            GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(5)), "{=DENY=}", "{="
            If MarkerList <> "" Then SetFieldMarkers RotaLink(1), 0, "RotaDiff", MarkerList
            GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(5)), "{=DENY=}", "{="
            If MarkerList <> "" Then SetFieldMarkers RotaLink(1), 1, "RotaDiff", MarkerList
        End If
        GetKeyItem MarkerList, NoopList(1).GetLineTag(ArrFlightLineNo(5)), "{=SPR=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(1), 0, MarkerList, "0"
        GetKeyItem MarkerList, NoopList(1).GetLineTag(DepFlightLineNo(5)), "{=SPR=}", "{="
        If MarkerList <> "" Then SetFieldMarkers RotaLink(1), 1, MarkerList, "0"
    End If
    ShowCoverColors
End Sub
Private Sub ShowCoverColors()
    Dim IdxList As String
    Dim i As Integer
    Dim j As Integer
    If chkRota(5).Value = 1 Then IdxList = "0,1,2,3" Else IdxList = "0,5,2,4"
    For i = 0 To 3
        j = Val(GetRealItem(IdxList, CLng(i), ","))
        If (Not IsRotation(j)) And (Not IsFlightPair(j)) Then ScrollCover(i).BackColor = LightGray
        If IsRotation(j) Then ScrollCover(i).BackColor = LightBlue
        If IsFlightPair(j) Then ScrollCover(i).BackColor = LightGreen
    Next
End Sub
Private Sub PrepareRotationView()
    Dim tmpRotaRec As String
    Dim CurRkey As String
    Dim CurAdid As String
    Dim ArrRkey As String
    Dim DepRkey As String
    Dim RotaLines As String
    Dim ImpLines(2) As Long
    Dim SrvLines(2) As Long
    Dim tmpLineNo As Long
    Dim i As Integer
    If ArrFlightLineNo(3) >= 0 Then
        RotaLines = ""
        CurRkey = Trim(GetRealItem(ArrFlightData(3), RkeyCol, ","))
        If CurRkey <> "" Then RotaLines = NoopList(1).GetLinesByIndexValue("ROTATION", CurRkey, 0)
        If RotaLines <> "" Then RotaLines = RotaLines & ",-1" Else RotaLines = "-1,-1"
        tmpLineNo = Val(GetItem(RotaLines, 1, ","))
        If tmpLineNo = ArrFlightLineNo(3) Then
            ArrFlightLineNo(4) = tmpLineNo
            DepFlightLineNo(4) = Val(GetItem(RotaLines, 2, ","))
        Else
            ArrFlightLineNo(4) = Val(GetItem(RotaLines, 2, ","))
            DepFlightLineNo(4) = tmpLineNo
        End If
        If ArrFlightLineNo(4) >= 0 Then ArrFlightData(4) = NoopList(1).GetLineValues(ArrFlightLineNo(4))
        If DepFlightLineNo(4) >= 0 Then DepFlightData(4) = NoopList(1).GetLineValues(DepFlightLineNo(4))
        If (ArrFlightLineNo(4) >= 0) And (DepFlightLineNo(4) >= 0) Then IsRotation(4) = True
    End If
    If DepFlightLineNo(3) >= 0 Then
        RotaLines = ""
        CurRkey = Trim(GetRealItem(DepFlightData(3), RkeyCol, ","))
        If CurRkey <> "" Then RotaLines = NoopList(1).GetLinesByIndexValue("ROTATION", CurRkey, 0)
        If RotaLines <> "" Then RotaLines = RotaLines & ",-1" Else RotaLines = "-1,-1"
        tmpLineNo = Val(GetItem(RotaLines, 1, ","))
        If tmpLineNo = DepFlightLineNo(3) Then
            ArrFlightLineNo(5) = Val(GetItem(RotaLines, 2, ","))
            DepFlightLineNo(5) = tmpLineNo
        Else
            ArrFlightLineNo(5) = tmpLineNo
            DepFlightLineNo(5) = Val(GetItem(RotaLines, 2, ","))
        End If
        If ArrFlightLineNo(5) >= 0 Then ArrFlightData(5) = NoopList(1).GetLineValues(ArrFlightLineNo(5))
        If DepFlightLineNo(5) >= 0 Then DepFlightData(5) = NoopList(1).GetLineValues(DepFlightLineNo(5))
        If (ArrFlightLineNo(5) >= 0) And (DepFlightLineNo(5) >= 0) Then IsRotation(5) = True
    End If
    For i = 0 To 5
        If (ArrFlightLineNo(i) >= 0) And (DepFlightLineNo(i) >= 0) Then
            ArrRkey = Trim(GetRealItem(ArrFlightData(i), RkeyCol, ","))
            DepRkey = Trim(GetRealItem(DepFlightData(i), RkeyCol, ","))
            If (ArrRkey <> "") And (DepRkey <> "") Then
                If ArrRkey = DepRkey Then IsRotation(i) = True Else IsFlightPair(i) = True
            End If
        End If
    Next
End Sub
Private Function CrossCheckFlights(PreCheck As Boolean, ShowDetails As Boolean) As Boolean
    Dim IsValidImport As Boolean
    IsValidImport = True
    CheckFieldUpdates PreCheck, ShowDetails
    CheckSplitAndJoin PreCheck, ShowDetails
    CrossCheckFlights = IsValidImport
End Function
Private Sub CheckSplitAndJoin(PreCheck As Boolean, ShowDetails As Boolean)
    Dim ImpJoin As Boolean
    Dim ImpSplit As Boolean
    Dim SrvSplit As Boolean
    Dim SrvIsSplit As Boolean
    Dim Act3Split As Boolean
    Dim ArrForeColor As Long
    Dim ArrBackColor As Long
    Dim DepForeColor As Long
    Dim DepBackColor As Long
    Dim DataIdx As Integer
    Dim ArrData As String
    Dim DepData As String
    Dim ArrTime As String
    Dim DepTime As String
    Dim SrvArrRkey As String
    Dim SrvDepRkey As String
    If (ArrFlightLineNo(0) >= 0) And (DepFlightLineNo(0) >= 0) Then
        ArrData = GetRealItem(ArrFlightData(0), PrflCol, ",")
        If Len(ArrData) > 1 Then ArrData = Mid(ArrData, 2, 1)
        DepData = GetRealItem(DepFlightData(0), PrflCol, ",")
        If Len(DepData) > 1 Then DepData = Mid(DepData, 2, 1)
        If (ArrData = "#") Or (DepData = "#") Then ImpSplit = True
        If (Not ImpSplit) And ((ArrFlightLineNo(3) < 0) Or (DepFlightLineNo(3) < 0)) And (RotationMode) Then ImpSplit = True
        If (Not ImpSplit) And (IsArrShiftLeft(2) Or IsDepShiftLeft(2)) And (RotationMode) Then ImpSplit = True
        If (ArrData = "!") Or (DepData = "!") Then ImpSplit = True
        If Not ImpSplit Then
            If RotationMode Then DataIdx = 3 Else DataIdx = 2
            ArrData = GetRealItem(ArrFlightData(DataIdx), Act3Col, ",")
            DepData = GetRealItem(DepFlightData(DataIdx), Act3Col, ",")
            If ArrData <> DepData Then ImpSplit = True
            ArrData = GetRealItem(ArrFlightData(DataIdx), TimeCol, ",")
            DepData = GetRealItem(DepFlightData(DataIdx), TimeCol, ",")
            If ArrData >= DepData Then ImpSplit = True
        End If
        If Not ImpSplit Then
            If (Not IsArrShiftLeft(2)) And (Not IsDepShiftLeft(2)) Then
                ArrData = GetRealItem(ArrFlightData(0), RkeyCol, ",")
                DepData = GetRealItem(DepFlightData(0), RkeyCol, ",")
                If ArrData = DepData Then ImpJoin = True
            End If
        End If
        If Not ImpSplit Then
            ArrData = GetRealItem(ArrFlightData(3), RkeyCol, ",")
            DepData = GetRealItem(DepFlightData(3), RkeyCol, ",")
            If ArrData <> DepData Then
                NoopList(0).GetLineColor ArrFlightLineNo(0), ArrForeColor, ArrBackColor
                NoopList(0).GetLineColor DepFlightLineNo(0), DepForeColor, DepBackColor
                If (ArrBackColor = CliFkeyCnt(5).BackColor) Or (DepBackColor = CliFkeyCnt(5).BackColor) Then
                    ImpJoin = False
                    ImpSplit = True
                End If
                If (ArrBackColor = SrvFkeyCnt(4).BackColor) Or (DepBackColor = SrvFkeyCnt(4).BackColor) Then
                    ImpJoin = False
                    ImpSplit = True
                End If
                If (ArrBackColor = SrvFkeyCnt(5).BackColor) Or (DepBackColor = SrvFkeyCnt(5).BackColor) Then
                    ImpJoin = False
                    ImpSplit = True
                End If
                If Not ImpSplit Then
                    If ArrBackColor = DepBackColor Then
                        If ArrBackColor = CliFkeyCnt(6).BackColor Then
                            ArrForeColor = CliFkeyCnt(1).ForeColor
                            ArrBackColor = CliFkeyCnt(1).BackColor
                            DepForeColor = CliFkeyCnt(1).ForeColor
                            DepBackColor = CliFkeyCnt(1).BackColor
                        Else
                            If RotationMode Then
                                If ArrBackColor <> CliFkeyCnt(6).BackColor Then
                                    ArrForeColor = CliFkeyCnt(2).ForeColor
                                    ArrBackColor = CliFkeyCnt(2).BackColor
                                End If
                                If DepBackColor <> CliFkeyCnt(6).BackColor Then
                                    DepForeColor = CliFkeyCnt(2).ForeColor
                                    DepBackColor = CliFkeyCnt(2).BackColor
                                End If
                            End If
                        End If
                    Else
                        If RotationMode Then
                            If ArrBackColor <> CliFkeyCnt(6).BackColor Then
                                ArrForeColor = CliFkeyCnt(2).ForeColor
                                ArrBackColor = CliFkeyCnt(2).BackColor
                            End If
                            If DepBackColor <> CliFkeyCnt(6).BackColor Then
                                DepForeColor = CliFkeyCnt(2).ForeColor
                                DepBackColor = CliFkeyCnt(2).BackColor
                            End If
                        Else
                            If ArrBackColor = CliFkeyCnt(6).BackColor Then
                                ArrForeColor = CliFkeyCnt(2).ForeColor
                                ArrBackColor = CliFkeyCnt(2).BackColor
                            End If
                            If DepBackColor = CliFkeyCnt(6).BackColor Then
                                DepForeColor = CliFkeyCnt(2).ForeColor
                                DepBackColor = CliFkeyCnt(2).BackColor
                            End If
                        End If
                    End If
                    NoopList(0).SetLineColor ArrFlightLineNo(0), ArrForeColor, ArrBackColor
                    NoopList(0).SetLineColor DepFlightLineNo(0), DepForeColor, DepBackColor
                    ImpJoin = True
                End If
            Else
                If (DataSystemType <> "SATS") Then
                    If (ArrData <> "") And (DepData <> "") Then
                        ImpJoin = False
                    Else
                        ArrData = GetRealItem(ArrFlightData(0), PrflCol, ",")
                        DepData = GetRealItem(DepFlightData(0), PrflCol, ",")
                        If (ArrData = "D") Or (DepData = "D") Then
                            ImpJoin = False
                        End If
                    End If
                Else
                    If RotationMode Then ImpJoin = False
                End If
            End If
            If (ArrFlightLineNo(1) >= 0) And (DepFlightLineNo(1) >= 0) Then
                If ArrFlightLineNo(0) <> ArrFlightLineNo(1) Then SrvSplit = True
                If DepFlightLineNo(0) <> DepFlightLineNo(1) Then SrvSplit = True
            End If
        End If
    End If
    
    If Not RotationMode Then
        If (ArrFlightLineNo(2) >= 0) And (Not IsArrShiftLeft(2)) Then
            'Check Updates performed by Client Arrival
            If (ArrFlightLineNo(4) >= 0) And (DepFlightLineNo(4) >= 0) Then
                'We got a Server Rotation related to the Client Arrival
                'Thus we got a Client Arrival and a Server Departure
                If (DepFlightLineNo(2) <> DepFlightLineNo(4)) Or (IsDepShiftLeft(2)) Then
                    'It is not the same rotation
                    'Check Necessary Implicit Split by A/C Type
                    'that should not be updated in the server rotation
                    'from the Client Arrival to the Server Departure
                    If (DepFlightLineNo(2) >= 0) And (IsDepShiftLeft(2)) Then
                        ArrData = GetRealItem(ArrFlightData(2), Act3Col, ",")
                        DepData = GetRealItem(DepFlightData(2), Act3Col, ",")
                    Else
                        ArrData = GetRealItem(ArrFlightData(2), Act3Col, ",")
                        DepData = GetRealItem(DepFlightData(4), Act3Col, ",")
                    End If
                    If ArrData <> DepData Then
                        If PreCheck Then
                            SetFieldMarkers NoopList(1), DepFlightLineNo(4), "SplitDep", "0"
                            AppendToLineTag NoopList(1), DepFlightLineNo(4), "{=SPR=}SplitDep"
                            SetFieldMarkers NoopList(1), DepFlightLineNo(4), "RotaDiff", Trim(Str(Act3Col))
                            AppendToLineTag NoopList(1), DepFlightLineNo(4), "{=DENY=}" & Trim(Str(Act3Col))
                            SetFieldMarkers NoopList(0), ArrFlightLineNo(2), "RotaDiff", Trim(Str(Act3Col))
                            AppendToLineTag NoopList(0), ArrFlightLineNo(2), "{=DENY=}" & Trim(Str(Act3Col))
                        End If
                        If (Not PreCheck) And (Not ShowDetails) Then
                            CreateCedaTransaction CmdLineIdx, "SPLIT", ArrFlightLineNo(4), DepFlightLineNo(4), ""
                        End If
                    End If
                End If
            End If
        End If
    End If
    
    If PreCheck Then
        If Not SrvSplit Then
            If ImpJoin Then
                If (ArrFlightLineNo(3) >= 0) And (DepFlightLineNo(3) >= 0) Then
                    If DepFlightLineNo(4) < 0 Then
                        SetFieldMarkers NoopList(1), ArrFlightLineNo(3), "IsSplitArr", "0"
                        AppendToLineTag NoopList(1), ArrFlightLineNo(3), "{=SPR=}IsSplitArr"
                    End If
                    If ArrFlightLineNo(5) < 0 Then
                        SetFieldMarkers NoopList(1), DepFlightLineNo(3), "IsSplitDep", "0"
                        AppendToLineTag NoopList(1), DepFlightLineNo(3), "{=SPR=}IsSplitDep"
                    End If
                End If
            End If
        End If
        If SrvSplit Then
            SetFieldMarkers NoopList(1), ArrFlightLineNo(1), "SplitArr", "0"
            SetFieldMarkers NoopList(1), DepFlightLineNo(1), "SplitDep", "0"
            AppendToLineTag NoopList(1), ArrFlightLineNo(1), "{=SPR=}SplitArr"
            AppendToLineTag NoopList(1), DepFlightLineNo(1), "{=SPR=}SplitDep"
        End If
        If ImpSplit Then
            SetFieldMarkers NoopList(0), ArrFlightLineNo(0), "SplitArr", "0"
            SetFieldMarkers NoopList(0), DepFlightLineNo(0), "SplitDep", "0"
            AppendToLineTag NoopList(0), ArrFlightLineNo(0), "{=SPR=}SplitArr"
            AppendToLineTag NoopList(0), DepFlightLineNo(0), "{=SPR=}SplitDep"
        End If
        If ImpJoin Then
            SetFieldMarkers NoopList(0), ArrFlightLineNo(0), "RotaJoin", "0"
            SetFieldMarkers NoopList(0), DepFlightLineNo(0), "RotaJoin", "0"
            AppendToLineTag NoopList(0), ArrFlightLineNo(0), "{=SPR=}RotaJoin"
            AppendToLineTag NoopList(0), DepFlightLineNo(0), "{=SPR=}RotaJoin"
            'PHYOE
            'If RotaJoinCnt(0) count is 0, no need to count
            'UFIS-3548
            If RotaJoinCnt(0) <> 0 Then
                RotaJoinCnt(0) = RotaJoinCnt(0) + 2
            End If
        End If
    End If
    If (Not PreCheck) And (Not ShowDetails) Then
        If ImpJoin Then CreateCedaTransaction CmdLineIdx, "JOIN", ArrFlightLineNo(0), DepFlightLineNo(0), ""
        'Here we must check for internal splits
        'Just for data display purposes
        'Check Arrival Rotation and as well Departure Rotation
        'If SrvSplit Then
            'CreateCedaTransaction "SPRI", ArrFlightLineNo(1), DepFlightLineNo(1), ""
        'End If
    End If
End Sub
Private Sub AppendToLineTag(CurTab As TABLib.Tab, LineNo As Long, TagText As String)
    Dim CurTag As String
    CurTag = CurTab.GetLineTag(LineNo)
    CurTag = CurTag & TagText
    CurTab.SetLineTag LineNo, CurTag
End Sub
Private Sub CheckFieldUpdates(PreCheck As Boolean, ShowDetails As Boolean)
    Dim ImpLineNo As Long
    Dim CurImpLineNo As Long
    Dim SrvLineNo As Long
    Dim CmdLineNo As Long
    Dim LineForeColor As Long
    Dim LineBackColor As Long
    Dim ActionType As String
    Dim CurFtypCol As String
    ArrUpdColList = ""
    DepUpdColList = ""
    CurFtypCol = CStr(FtypCol)
    
    'Check Arrival Records
    ImpLineNo = ArrFlightLineNo(2)
    SrvLineNo = ArrFlightLineNo(3)
    CurImpLineNo = ImpLineNo
    If (ImpLineNo >= 0) And (SrvLineNo >= 0) Then
        If NoopList(0).GetColumnValue(ImpLineNo, 0) = "D" Then ImpLineNo = -1
    End If
    
    If ((ImpLineNo >= 0) And (SrvLineNo >= 0)) Then
        If ImpLineNo = SrvLineNo Then
            ArrUpdColList = CompareRecords(MyCompFields, ImpEmptyData, ArrFlightData(2), ArrFlightData(3))
            If PreCheck Then
                AppendToLineTag NoopList(0), ImpLineNo, "{=UPD=}" & ArrUpdColList
                AppendToLineTag NoopList(1), SrvLineNo, "{=UPD=}" & ArrUpdColList
                If ArrUpdColList <> "" Then
                    LineForeColor = CliFkeyCnt(3).ForeColor
                    LineBackColor = CliFkeyCnt(3).BackColor
                Else
                    LineForeColor = CliFkeyCnt(6).ForeColor
                    LineBackColor = CliFkeyCnt(6).BackColor
                    NoopList(1).SetColumnValue SrvLineNo, PrflCol, "-"
                End If
                NoopList(0).SetLineColor ImpLineNo, LineForeColor, LineBackColor
                NoopList(1).SetLineColor SrvLineNo, LineForeColor, LineBackColor
                SetFieldMarkers NoopList(0), ImpLineNo, "CellDiff", ArrUpdColList
                SetFieldMarkers NoopList(1), SrvLineNo, "CellDiff", ArrUpdColList
            End If
            If (Not PreCheck) And (Not ShowDetails) Then
                If (Not IsArrShiftLeft(2)) And (ArrUpdColList <> "") Then CreateCedaTransaction CmdLineIdx, "UFR", ImpLineNo, 0, ArrUpdColList
            End If
        End If
    Else
        'Inserts or Deletes
        If CurImpLineNo <> ImpLineNo Then
            'Check FTYP on Delete
            If UpdateFtypOnDelete Then
                ArrUpdColList = CompareRecords(CurFtypCol, "N", ArrFlightData(2), ArrFlightData(3))
                SetFieldMarkers NoopList(0), CurImpLineNo, "CellDiff", ArrUpdColList
                SetFieldMarkers NoopList(1), SrvLineNo, "CellDiff", ArrUpdColList
            End If
        End If
        If (Not PreCheck) And (Not ShowDetails) Then
            CmdLineNo = -1
            If ImpLineNo >= 0 Then
                NoopList(0).GetLineColor ImpLineNo, LineForeColor, LineBackColor
                CmdLineNo = ImpLineNo
            ElseIf SrvLineNo >= 0 Then
                NoopList(1).GetLineColor SrvLineNo, LineForeColor, LineBackColor
                CmdLineNo = SrvLineNo
            End If
            If CmdLineNo >= 0 Then
                ActionType = DefineColorAction(LineBackColor)
                CreateCedaTransaction CmdLineIdx, ActionType, CmdLineNo, 0, ""
            End If
        End If
    End If
    
    'Check Departure Records
    ImpLineNo = DepFlightLineNo(2)
    SrvLineNo = DepFlightLineNo(3)
    CurImpLineNo = ImpLineNo
    If (ImpLineNo >= 0) And (SrvLineNo >= 0) Then
        If NoopList(0).GetColumnValue(ImpLineNo, 0) = "D" Then ImpLineNo = -1
    End If
    If (ImpLineNo >= 0) And (SrvLineNo >= 0) Then
        If ImpLineNo = SrvLineNo Then
            DepUpdColList = CompareRecords(MyCompFields, ImpEmptyData, DepFlightData(2), DepFlightData(3))
            If PreCheck Then
                AppendToLineTag NoopList(0), ImpLineNo, "{=UPD=}" & DepUpdColList
                AppendToLineTag NoopList(1), SrvLineNo, "{=UPD=}" & DepUpdColList
                If DepUpdColList <> "" Then
                    LineForeColor = CliFkeyCnt(3).ForeColor
                    LineBackColor = CliFkeyCnt(3).BackColor
                Else
                    LineForeColor = CliFkeyCnt(6).ForeColor
                    LineBackColor = CliFkeyCnt(6).BackColor
                    NoopList(1).SetColumnValue SrvLineNo, PrflCol, "-"
                End If
                NoopList(0).SetLineColor ImpLineNo, LineForeColor, LineBackColor
                NoopList(1).SetLineColor SrvLineNo, LineForeColor, LineBackColor
                SetFieldMarkers NoopList(0), ImpLineNo, "CellDiff", DepUpdColList
                SetFieldMarkers NoopList(1), SrvLineNo, "CellDiff", DepUpdColList
            End If
            If (Not PreCheck) And (Not ShowDetails) Then
                If (Not IsDepShiftLeft(2)) And (DepUpdColList <> "") Then CreateCedaTransaction CmdLineIdx, "UFR", ImpLineNo, 0, DepUpdColList
            End If
        End If
    Else
        'Inserts or Deletes
        If CurImpLineNo <> ImpLineNo Then
            'Check FTYP on Delete
            If UpdateFtypOnDelete Then
                DepUpdColList = CompareRecords(CurFtypCol, "N", DepFlightData(2), DepFlightData(3))
                SetFieldMarkers NoopList(0), CurImpLineNo, "CellDiff", DepUpdColList
                SetFieldMarkers NoopList(1), SrvLineNo, "CellDiff", DepUpdColList
            End If
        End If
        If (Not PreCheck) And (Not ShowDetails) Then
            CmdLineNo = -1
            If ImpLineNo >= 0 Then
                NoopList(0).GetLineColor ImpLineNo, LineForeColor, LineBackColor
                CmdLineNo = ImpLineNo
            ElseIf SrvLineNo >= 0 Then
                NoopList(1).GetLineColor SrvLineNo, LineForeColor, LineBackColor
                CmdLineNo = SrvLineNo
            End If
            If CmdLineNo >= 0 Then
                ActionType = DefineColorAction(LineBackColor)
                CreateCedaTransaction CmdLineIdx, ActionType, CmdLineNo, 0, ""
            End If
        End If
    End If
End Sub
Private Sub CreateCedaTransaction(GrpLineIdx As String, CedaCmd As String, CurLineNo As Long, DepLineNo As Long, ColItemList As String)
    Dim CurCliStatus As Long
    Dim SqlCmd As String
    Dim SqlKey As String
    Dim SqlFld As String
    Dim SqlDat As String
    Dim NewLine As String
    Dim CmdPrio As String
    Dim TwsIdx As String
    Dim CurFkey As String
    Dim CurDate As String
    If CedaCmd <> "" Then
        If CurLineNo >= 0 Then
            SqlCmd = ""
            CurCliStatus = NoopList(0).GetLineStatusValue(CurLineNo)
            Select Case CedaCmd
                Case "UFR", "UPD"
                    If (CurCliStatus And CmdUfrDone) <> CmdUfrDone Then
                        If Not RotationMode Then PrepareAftUpdate CurLineNo, SqlCmd, SqlKey, SqlFld, SqlDat, ColItemList, ""
                        TwsIdx = Trim(Str(CurLineNo))
                        CmdPrio = "3" & NoopList(0).GetColumnValue(CurLineNo, AdidCol)
                        CurFkey = TurnImpFkeyToAftFkey(NoopList(0).GetColumnValue(CurLineNo, FkeyCol))
                        NoopList(0).SetLineStatusValue CurLineNo, CurCliStatus Or CmdUfrDone
                    End If
                Case "IFR", "INS"
                    If (CurCliStatus And CmdIfrDone) <> CmdIfrDone Then
                        If Not RotationMode Then PrepareAftInsert CurLineNo, SqlCmd, SqlKey, SqlFld, SqlDat, ""
                        TwsIdx = Trim(Str(CurLineNo))
                        CmdPrio = "2" & NoopList(0).GetColumnValue(CurLineNo, AdidCol)
                        CurFkey = TurnImpFkeyToAftFkey(NoopList(0).GetColumnValue(CurLineNo, FkeyCol))
                        NoopList(0).SetLineStatusValue CurLineNo, CurCliStatus Or CmdIfrDone
                    End If
                Case "DFR", "DEL", "REM"
                    If ((CurCliStatus And CmdDfrDone) <> CmdDfrDone) Or (CedaCmd = "REM") Then
                        If Not RotationMode Then PrepareAftDelete CurLineNo, SqlCmd, SqlKey, SqlFld, SqlDat, ""
                        TwsIdx = Trim(Str(CurLineNo))
                        CmdPrio = "4"
                        CurFkey = TurnImpFkeyToAftFkey(NoopList(1).GetColumnValue(CurLineNo, FkeyCol))
                        NoopList(0).SetLineStatusValue CurLineNo, CurCliStatus Or CmdDfrDone
                    End If
                Case "SPR", "SPLIT"
                    If (CurCliStatus And CmdSprDone) <> CmdSprDone Then
                        PrepareAftSplit CurLineNo, DepLineNo, SqlCmd, SqlKey, SqlFld, SqlDat, ""
                        TwsIdx = Trim(Str(CurLineNo)) & "," & Trim(Str(DepLineNo))
                        CmdPrio = "1"
                        CurFkey = "{S}" & TurnImpFkeyToAftFkey(NoopList(1).GetColumnValue(CurLineNo, FkeyCol))
                        CurFkey = CurFkey & "," & TurnImpFkeyToAftFkey(NoopList(1).GetColumnValue(DepLineNo, FkeyCol))
                        NoopList(0).SetLineStatusValue CurLineNo, CurCliStatus Or CmdSprDone
                        NoopList(0).SetLineStatusValue DepLineNo, CurCliStatus Or CmdSprDone
                    End If
                Case "JOF", "JOIN"
                    If (CurCliStatus And CmdJofDone) <> CmdJofDone Then
                        PrepareAftJoin CurLineNo, DepLineNo, SqlCmd, SqlKey, SqlFld, SqlDat, ""
                        TwsIdx = Trim(Str(CurLineNo)) & "," & Trim(Str(DepLineNo))
                        CmdPrio = "5"
                        CurFkey = "{J}" & TurnImpFkeyToAftFkey(NoopList(1).GetColumnValue(CurLineNo, FkeyCol))
                        CurFkey = CurFkey & "," & TurnImpFkeyToAftFkey(NoopList(1).GetColumnValue(DepLineNo, FkeyCol))
                        NoopList(0).SetLineStatusValue CurLineNo, CurCliStatus Or CmdJofDone
                        NoopList(0).SetLineStatusValue DepLineNo, CurCliStatus Or CmdJofDone
                    End If
                Case "SPRI"
                    'PrepareAftSplit CurLineNo, DepLineNo, SqlCmd, SqlKey, SqlFld, SqlDat, ""
                    'If SqlCmd <> "" Then SqlCmd = "SPRI"
                    'TwsIdx = Trim(Str(CurLineNo)) & "," & Trim(Str(DepLineNo))
                    'CmdPrio = "0"
                    'NoopList(0).SetLineStatusValue CurLineNo, 3
                    'NoopList(0).SetLineStatusValue DepLineNo, 3
                Case Else
                    'Error
            End Select
            If SqlCmd <> "" Then
                NewLine = GrpLineIdx & "," & CmdPrio & Chr(30)
                NewLine = NewLine & TwsIdx & Chr(30)
                NewLine = NewLine & CurFkey & Chr(30)
                NewLine = NewLine & SqlCmd & Chr(30)
                NewLine = NewLine & SqlKey & Chr(30)
                NewLine = NewLine & SqlFld & Chr(30)
                NewLine = NewLine & SqlDat & Chr(30)
                CedaSpooler.CedaOut(0).InsertTextLine NewLine, False
                If Left(CurFkey, 1) = "{" Then
                    CurFkey = Mid(CurFkey, 4)
                    CurDate = Mid(GetItem(CurFkey, 1, ","), 10, 8)
                    If CurDate < MainPacketMinDate Then MainPacketMinDate = CurDate
                    If CurDate > MainPacketMaxDate Then MainPacketMaxDate = CurDate
                    CurDate = Mid(GetItem(CurFkey, 2, ","), 10, 8)
                Else
                    CurDate = Mid(CurFkey, 10, 8)
                End If
                If CurDate < MainPacketMinDate Then MainPacketMinDate = CurDate
                If CurDate > MainPacketMaxDate Then MainPacketMaxDate = CurDate
            End If
        End If
    End If
End Sub
Private Sub SetFieldMarkers(CurTab As TABLib.Tab, LineNo As Long, CellObj As String, ColList As String)
    Dim ColNbr As String
    Dim ColNo As Long
    Dim CurItm As Long
    CurItm = 0
    ColNbr = GetRealItem(ColList, CurItm, ",")
    While ColNbr <> ""
        ColNo = Val(ColNbr)
        CurTab.SetDecorationObject LineNo, ColNo, CellObj
        CurItm = CurItm + 1
        ColNbr = GetRealItem(ColList, CurItm, ",")
    Wend
End Sub
Private Function CompareRecords(ItemList As String, AllowEmpty As String, ImpRecord As String, SrvRecord As String) As String
    Dim Result As String
    Dim ColNbr As String
    Dim ImpData As String
    Dim SrvData As String
    Dim EmptyFlag As String
    Dim CurCol As Long
    Dim CurItm As Long
    Result = ""
    CurItm = 0
    ColNbr = GetRealItem(ItemList, CurItm, ",")
    'PHYOE
    'DON'T COMPARE RegnKey, ApdxData, Remark
    While ColNbr <> ""
        EmptyFlag = GetRealItem(AllowEmpty, CurItm, ",")
        CurCol = Val(ColNbr)
        ImpData = GetRealItem(ImpRecord, CurCol, ",")
        SrvData = GetRealItem(SrvRecord, CurCol, ",")
        If (ImpData <> "") Or (EmptyFlag = "Y") Then
            If Left(ImpData, 1) <> "?" Then
                If ImpData <> SrvData Then
                    If CurCol = FtypCol Then
                        If SrvData = "" Then SrvData = " "
                        If InStr("XNDR ", SrvData) > 0 Then
                            Result = Result & ColNbr & ","
                        ElseIf InStr(("XN" & FtypOnDelete), ImpData) > 0 Then
                            Result = Result & ColNbr & ","
                        End If
                    Else
                        Result = Result & ColNbr & ","
                    End If
                End If
            End If
        End If
        CurItm = CurItm + 1
        ColNbr = GetRealItem(ItemList, CurItm, ",")
    Wend
    If Result <> "" Then Result = Left(Result, Len(Result) - 1)
    CompareRecords = Result
End Function

Private Sub HandleMultiSelect(LineNo As Long)
    Static FirstLine As Long
    Static LastLine As Long
    Static FirstTextColor As Long
    Static FirstBackColor As Long
    Static MultiSelection As Boolean
    Static SelCount As Long
    Dim FromLine As Long
    Dim ToLine As Long
    Dim CurLine As Long
    Dim DmmColor As Long
    Dim CurBackColor As Long
    Dim SelList As String
    Dim LinNbr As String
    Dim ItmNbr As Long
    If DataSystemType = "OAFOS" Then
        SelectRegnLines LineNo
    Else
'        If (chkWork(6).Value = 1) And (Not StopNestedCalls) And (LineNo >= 0) Then
'            FromLine = -1
'            ToLine = -1
'            Select Case KeybdFlags
'                Case 1  'Shift
'                    If LineNo <= FirstLine Then
'                        FromLine = LineNo
'                        ToLine = FirstLine
'                        FirstLine = LineNo
'                    End If
'                    If LineNo >= LastLine Then
'                        FromLine = LastLine
'                        ToLine = LineNo
'                        LastLine = LineNo
'                    End If
'                Case 2  'Ctrl
'                    NoopList(2).GetLineColor LineNo, DmmColor, CurBackColor
'                    If MultiSelection Then
'                        If CurBackColor = FirstBackColor Then
'                            If NoopList(2).GetLineStatusValue(LineNo) <> 1 Then
'                                NoopList(2).SetCellProperty LineNo, 0, "MultiSel"
'                                NoopList(2).SetCellProperty LineNo, 1, "MultiSel"
'                                NoopList(2).SetCellProperty LineNo, 2, "MultiSel"
'                                NoopList(2).SetLineStatusValue LineNo, 1
'                                SelCount = SelCount + 1
'                            Else
'                                NoopList(2).ResetCellProperties LineNo
'                                NoopList(2).SetLineStatusValue LineNo, -1
'                                SelCount = SelCount - 1
'                            End If
'                            If LineNo < FirstLine Then FirstLine = LineNo
'                            If LineNo > LastLine Then LastLine = LineNo
'                        End If
'                    Else
'                        If (CurBackColor <> CliFkeyCnt(4).BackColor) And (CurBackColor <> CliFkeyCnt(3).BackColor) Then
'                            NoopList(2).SetCellProperty LineNo, 0, "MultiSel"
'                            NoopList(2).SetCellProperty LineNo, 1, "MultiSel"
'                            NoopList(2).SetCellProperty LineNo, 2, "MultiSel"
'                            NoopList(2).SetLineStatusValue LineNo, 1
'                            FirstLine = LineNo
'                            LastLine = LineNo
'                            FirstBackColor = CurBackColor
'                            FirstTextColor = DmmColor
'                            MultiSelection = True
'                            SelCount = 1
'                        End If
'                    End If
'                    NoopList(2).RedrawTab
'                Case Else
'                If chkWork(12).Value = 0 Then
'                    If MultiSelection Then
'                        SelList = NoopList(2).GetLinesByStatusValue(1, 0)
'                        If SelList <> "" Then
'                            me.MousePointer = 11
'                            LinNbr = "START"
'                            ItmNbr = -1
'                            While LinNbr <> ""
'                                ItmNbr = ItmNbr + 1
'                                LinNbr = GetRealItem(SelList, ItmNbr, ",")
'                                If LinNbr <> "" Then
'                                    CurLine = Val(LinNbr)
'                                    NoopList(2).ResetCellProperties CurLine
'                                    NoopList(2).SetLineStatusValue CurLine, -1
'                                End If
'                            Wend
'                            me.MousePointer = 0
'                            NoopList(2).RedrawTab
'                        End If
'                    End If
'                    NoopList(2).GetLineColor LineNo, FirstTextColor, FirstBackColor
'                    FirstLine = LineNo
'                    LastLine = LineNo
'                    SelCount = 0
'                    MultiSelection = False
'                Else
'                    If Not MultiSelection Then
'                        NoopList(2).GetLineColor LineNo, FirstTextColor, FirstBackColor
'                        FirstLine = LineNo
'                        LastLine = LineNo
'                        SelCount = 0
'                    End If
'                End If
'            End Select
'            If (FirstBackColor = CliFkeyCnt(4).BackColor) Or (FirstBackColor = CliFkeyCnt(3).BackColor) Then FromLine = -1
'            If FromLine >= 0 Then
'                For CurLine = FromLine To ToLine
'                    If NoopList(2).GetLineStatusValue(CurLine) <> 1 Then
'                        NoopList(2).GetLineColor CurLine, DmmColor, CurBackColor
'                        If CurBackColor = FirstBackColor Then
'                            NoopList(2).SetCellProperty CurLine, 0, "MultiSel"
'                            NoopList(2).SetCellProperty CurLine, 1, "MultiSel"
'                            NoopList(2).SetCellProperty CurLine, 2, "MultiSel"
'                            NoopList(2).SetLineStatusValue CurLine, 1
'                            SelCount = SelCount + 1
'                        End If
'                    End If
'                Next
'                NoopList(2).RedrawTab
'                MultiSelection = True
'            End If
'            If SelCount > 0 Then
'                CurPos(4).Caption = Str(SelCount)
'                CurPos(4).ForeColor = FirstTextColor
'                CurPos(4).BackColor = FirstBackColor
'                CurPos(4).Visible = True
'            Else
'                CurPos(4).Visible = False
'            End If
'        End If
    End If
End Sub

Private Sub SelectRegnLines(LineNo As Long)
    Dim RefFtyp As String
    Dim RefRegn As String
    Dim CurRegn As String
    Dim CurPrfl As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim RefLine As Long
    If chkWork(6).Value = 1 Then
        If NoopList(2).GetLineStatusValue(LineNo) <> 1 Then
            If chkWork(12).Value = 0 Then ClearLineSelection
            RefLine = Val(NoopList(2).GetColumnValue(LineNo, 4))
            RefRegn = NoopList(0).GetColumnValue(RefLine, RegnCol)
            RefFtyp = NoopList(0).GetColumnValue(RefLine, FtypCol)
            If (RefFtyp = "X") Or (RefFtyp = "N") Then RefRegn = ""
            If RefRegn <> "" Then
                If InStr(SelRegnList, RefRegn) = 0 Then
                    If SelRegnList <> "" Then SelRegnList = SelRegnList & ", "
                    SelRegnList = SelRegnList & RefRegn
                End If
                CurPrfl = NoopList(0).GetColumnValue(RefLine, PrflCol)
                If Len(CurPrfl) > 1 Then CurPrfl = Mid(CurPrfl, 2, 1)
                If CurPrfl <> "!" Then
                    NoopList(2).SetCellProperty LineNo, 0, "MultiSel"
                    NoopList(2).SetCellProperty LineNo, 1, "MultiSel"
                    NoopList(2).SetCellProperty LineNo, 2, "MultiSel"
                    NoopList(2).SetLineStatusValue LineNo, 1
                End If
                CurLine = LineNo
                CurRegn = RefRegn
                While (CurLine > 0) And (CurRegn = RefRegn)
                    CurLine = CurLine - 1
                    RefLine = Val(NoopList(2).GetColumnValue(CurLine, 4))
                    CurRegn = NoopList(0).GetColumnValue(RefLine, RegnCol)
                    If CurRegn = RefRegn Then
                        CurPrfl = NoopList(0).GetColumnValue(RefLine, PrflCol)
                        If Len(CurPrfl) > 1 Then CurPrfl = Mid(CurPrfl, 2, 1)
                        If CurPrfl <> "!" Then
                            NoopList(2).SetCellProperty CurLine, 0, "MultiSel"
                            NoopList(2).SetCellProperty CurLine, 1, "MultiSel"
                            NoopList(2).SetCellProperty CurLine, 2, "MultiSel"
                            NoopList(2).SetLineStatusValue CurLine, 1
                        End If
                    End If
                Wend
                CurLine = LineNo
                CurRegn = RefRegn
                MaxLine = NoopList(2).GetLineCount - 1
                While (CurLine < MaxLine) And (CurRegn = RefRegn)
                    CurLine = CurLine + 1
                    RefLine = Val(NoopList(2).GetColumnValue(CurLine, 4))
                    CurRegn = NoopList(0).GetColumnValue(RefLine, RegnCol)
                    If CurRegn = RefRegn Then
                        CurPrfl = NoopList(0).GetColumnValue(RefLine, PrflCol)
                        If Len(CurPrfl) > 1 Then CurPrfl = Mid(CurPrfl, 2, 1)
                        If CurPrfl <> "!" Then
                            NoopList(2).SetCellProperty CurLine, 0, "MultiSel"
                            NoopList(2).SetCellProperty CurLine, 1, "MultiSel"
                            NoopList(2).SetCellProperty CurLine, 2, "MultiSel"
                            NoopList(2).SetLineStatusValue CurLine, 1
                        End If
                    End If
                Wend
            End If
            NoopList(2).RedrawTab
        End If
    End If
End Sub

Private Sub ClearLineSelection()
    Dim CurLine As Long
    Dim SelList As String
    Dim LinNbr As String
    Dim ItmNbr As Long
    SelList = NoopList(2).GetLinesByStatusValue(1, 0)
    If SelList <> "" Then
        LinNbr = "START"
        ItmNbr = -1
        While LinNbr <> ""
            ItmNbr = ItmNbr + 1
            LinNbr = GetRealItem(SelList, ItmNbr, ",")
            If LinNbr <> "" Then
                CurLine = Val(LinNbr)
                NoopList(2).ResetCellProperties CurLine
                NoopList(2).SetLineStatusValue CurLine, -1
            End If
        Wend
        NoopList(2).Refresh
    End If
    SelRegnList = ""
End Sub

Private Sub NoopList_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If (Index = 2) And (LineNo >= 0) Then
        'HandleActionButton "ALL"
        'HandleActionButton "SINGLE"
        CheckSingleImport False
    End If
End Sub

Private Sub NoopList_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpText As String
    Dim tmpType As String
    Dim ActionType As String
    Dim CallIndex As Integer
    Dim ForWhat As Integer
    Dim CurAction As Integer
    Dim TextColor As Long
    Dim BackColor As Long
    'MsgBox NoopList(Index).GetLineTag(LineNo)
    'Exit Sub
    CallIndex = -1
    tmpText = Trim(NoopList(Index).GetColumnValue(LineNo, ColNo))
    If Index <> 2 Then
        Select Case ColNo
            Case FlnoCol  'Flights
                CallIndex = 2
                tmpType = "FLNO"
            'Case 1
            'Case 2
            'Case 3
            Case Act3Col  'AirCraft Type
                CallIndex = 6
                tmpType = "ACT"
            Case Apc3Col  'AirportCode
                CallIndex = 4
                tmpType = "APT"
            Case Via3Col  'AirportCode
                CallIndex = 4
                tmpType = "APT"
            'Case 7
            'Case 8
            Case Else
        End Select
        BasicDetails tmpType, tmpText
    ElseIf chkWork(6).Value = 0 Then
        Select Case ColNo
            Case 0
                tmpText = Trim(Left(tmpText, 3))
                CallIndex = 0   'AirlineCode
                tmpType = "ALC"
            'Case 1
            'Case 2
            Case Else
        End Select
        BasicDetails tmpType, tmpText
    Else
        If DataSystemType = "OAFOS" Then
            OaFosRegnImport True, DataSystemAlc3
        Else
            'If Not RotationMode Then
                CheckSingleImport True
                'NoopList(2).GetLineColor LineNo, TextColor, BackColor
                'ActionType = DefineColorAction(BackColor)
                'CallServerAction ActionType, 1, False, ""
            'End If
        End If
    End If
End Sub
Private Sub NoopList_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    Dim tmpText As String
    'Dim CallIndex As Integer
    'Dim ForWhat As Integer
    'Dim CurAction As Integer
    Dim ActionType As String
    Dim CurKey As Integer
    Dim TextColor As Long
    Dim BackColor As Long
    Dim DoIt As Boolean
    If chkWork(6).Value = 1 Then
        If Index = 2 Then
            CurKey = Key
            Select Case KeybdFlags
                Case 0  'Nothing
                    Select Case CurKey
                        Case 13, 32, 45, 46, 73, 68, 85
                            If Not RotationMode Then
                                NoopList(2).GetLineColor LineNo, TextColor, BackColor
                                ActionType = DefineColorAction(BackColor)
                                DoIt = False
                                Select Case Key
                                    Case 46, 68
                                        If ActionType = "DEL" Then DoIt = True
                                    Case 32, 85
                                        If ActionType = "UPD" Then DoIt = True
                                    Case 45, 73
                                        If ActionType = "INS" Then DoIt = True
                                    Case 13
                                        DoIt = True
                                    Case Else
                                End Select
                                If DoIt Then CallServerAction ActionType, 1, False, ""
                            End If
                        Case Else
                    End Select
                Case 1, 2 'Shift or Ctrl
                    Select Case CurKey
                        Case 13, 32
                            HandleMultiSelect LineNo
                        Case Else
                    End Select
                Case Else
            End Select
        End If
    End If
End Sub

Private Function DefineColorAction(BackColor As Long) As String
    Dim Result As String
    Dim CurIdx As Integer
    Result = ""
    For CurIdx = 1 To 6
        If CliFkeyCnt(CurIdx).BackColor = BackColor Then
            Result = CliFkeyCnt(CurIdx).Tag
            Exit For
        End If
    Next
    If Result = "" Then
        For CurIdx = 1 To 6
            If SrvFkeyCnt(CurIdx).BackColor = BackColor Then
                Result = SrvFkeyCnt(CurIdx).Tag
                Exit For
            End If
        Next
    End If
    DefineColorAction = Result
End Function

Private Sub JumpToColoredLine(ErrColor As Long, LineStatus As Long, JumpToLine As Long)
    Dim CurScroll As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim UseLine As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim LineFound As Boolean
    Dim LoopCount As Integer
    Dim VisLines As Long
    If JumpToLine < 0 Then
        CurLine = NoopList(2).GetCurrentSelected
        If CurLine < 0 Then CurLine = -1
        MaxLine = NoopList(2).GetLineCount - 1
        LineFound = False
        LoopCount = 0
        Do
            While CurLine <= MaxLine
                CurLine = CurLine + 1
                NoopList(2).GetLineColor CurLine, ForeColor, BackColor
                If (LineStatus > 0) And (LineStatus <> NoopList(2).GetLineStatusValue(CurLine)) Then BackColor = -1
                If BackColor = ErrColor Then
                    UseLine = CurLine
                    LineFound = True
                    CurLine = MaxLine + 1
                End If
            Wend
            LoopCount = LoopCount + 1
            CurLine = -1
        Loop While (LineFound = False) And (LoopCount < 2)
    Else
        UseLine = JumpToLine
        LineFound = True
    End If
    If LineFound Then
        VisLines = (NoopList(2).Height / NoopList(2).LineHeight / 15) - 5
        If UseLine > VisLines Then CurScroll = UseLine - VisLines Else CurScroll = 0
        NoopList(2).OnVScrollTo CurScroll
        If LineStatus > 0 Then StopNestedCalls = True
        NoopList(2).SetCurrentSelection UseLine
        StopNestedCalls = False
    End If
End Sub

Public Sub AdjustScrollMaster(UseCheckCol As Long, UseCheckLen As Integer, UseCheckData As String, SetColorOnly As Boolean)
    Static LastCheckCol As Long
    Static LastCheckLen As Integer
    Static LastCheckData As String
    Dim CheckCol As Long
    Dim CheckLen As Integer
    Dim CheckData As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim RecLine As Long
    Dim LeftTextColor As Long
    Dim LeftBackColor As Long
    Dim RightTextColor As Long
    Dim RightBackColor As Long
    Dim UseTextColor As Long
    Dim UseBackColor As Long
    Dim tmpFkey As String
    Dim tmpFlno As String
    Dim tmpDoop As String
    Dim tmpLine As String
    Dim tmpNoopLine As String
    Dim LeftTag As String
    Dim RightTag As String
    Dim CurLeftDat As String
    Dim CurRightDat As String
    Dim LeftColorIdx As Integer
    Dim RightColorIdx As Integer
    Dim UseColorIdx As Integer
    Dim tmpBuff As String
    Dim i As Integer
    Dim StartZeit
    Me.MousePointer = 11
    If Not SetColorOnly Then
        If UseCheckCol < -1 Then
            CheckCol = LastCheckCol
            CheckLen = LastCheckLen
            CheckData = LastCheckData
        Else
            CheckCol = UseCheckCol
            CheckLen = UseCheckLen
            CheckData = UseCheckData
        End If
        NoopList(2).ResetContent
        MaxLine = NoopList(0).GetLineCount - 1
        SetProgressBar 0, 1, MaxLine, "Adjusting Scroll Master Lines ..."
        tmpLine = ""
        StartZeit = Timer
        For CurLine = 0 To MaxLine
            If CheckCol >= 0 Then
                CurLeftDat = Trim(Left(NoopList(0).GetColumnValue(CurLine, CheckCol), CheckLen))
                CurRightDat = Trim(Left(NoopList(1).GetColumnValue(CurLine, CheckCol), CheckLen))
            Else
                CurLeftDat = CheckData
            End If
            If (CurLeftDat = CheckData) Or (CurRightDat = CheckData) Then
                'tmpNoopLine = NoopList(0).GetColumnValues(CurLine, RotaSelFields) & "," & Trim(Str(CurLine)) & ","
                tmpNoopLine = NoopList(0).GetColumnValues(CurLine, RotaSelFields) & "," & CStr(CurLine)
                If GetItem(tmpNoopLine, 1, ",") = "" Then
                    'tmpNoopLine = NoopList(1).GetColumnValues(CurLine, RotaSelFields) & "," & Trim(Str(CurLine)) & ","
                    tmpNoopLine = NoopList(1).GetColumnValues(CurLine, RotaSelFields) & "," & CStr(CurLine)
                    If GetItem(tmpNoopLine, 1, ",") = "" Then tmpNoopLine = ",,,,-1"
                End If
                If chkWork(1).Value = 1 Then
                    'Shrink
                    NoopList(0).GetLineColor CurLine, RightTextColor, RightBackColor
                    If Not FindShrinkColor(RightBackColor) Then tmpNoopLine = ""
                End If
                If tmpNoopLine <> "" Then
                    tmpLine = tmpLine & tmpNoopLine & vbLf
                End If
            End If
            If CurLine Mod 200 = 0 Then
                If tmpLine <> "" Then
                    NoopList(2).InsertBuffer tmpLine, vbLf
                    tmpLine = ""
                End If
                SetProgressBar 0, 2, CurLine, ""
            End If
        Next
        If tmpLine <> "" Then
            NoopList(2).InsertBuffer tmpLine, vbLf
            tmpLine = ""
        End If
        SetProgressBar 0, 3, 0, ""
    End If
    MaxLine = NoopList(2).GetLineCount - 1
    If MaxLine = 0 Then NoopList(2).InsertTextLine "-,,,,-1", False
    SetProgressBar 0, 1, MaxLine, "Adjusting Scroll Master Colors ..."
    For CurLine = 0 To MaxLine
        If CurLine Mod 150 = 0 Then SetProgressBar 0, 2, CurLine, ""
        RecLine = Val(NoopList(2).GetColumnValue(CurLine, 4))
        NoopList(0).GetLineColor RecLine, LeftTextColor, LeftBackColor
        NoopList(2).SetLineColor CurLine, LeftTextColor, LeftBackColor
    Next
    AdjustCounterCaption 7, 13, 0, 0, False
    If (CheckCol >= 0) Or (UseCheckLen < -1) Then
        'Filter and/or Shrink
        If UseCheckCol > -2 Then AdjustCounterCaption 8, 13, 2, 2, True
    Else
        If chkWork(1).Value = 0 Then AdjustCounterCaption 1, 6, 0, 1, True
    End If
    
    tmpLine = Str(CountColorLines(NoopList(2), vbBlack))
    If Val(tmpLine) > 0 Then
        CliFkeyCnt(7).Caption = tmpLine
        CliFkeyCnt(7).BackColor = vbBlack
        CliFkeyCnt(7).ForeColor = LimitColor
    Else
        CliFkeyCnt(7).BackColor = vbButtonFace
        CliFkeyCnt(7).ForeColor = vbButtonText
    End If
    
    NoopList(2).IndexCreate "LineIdx", 4
    SetProgressBar 0, 3, 0, ""
    LastCheckCol = CheckCol
    LastCheckLen = CheckLen
    LastCheckData = CheckData
    NoopList(2).RedrawTab
    
    NoopList(2).SetCurrentSelection 0
    NoopList(2).SetFocus
    Me.MousePointer = 0
End Sub
'SET COUNTER
'PHYOE
Public Sub AdjustCounterCaption(I1 As Integer, I2 As Integer, TB1 As Integer, TB2 As Integer, SetValue As Boolean)
    Dim i As Integer
    For i = I1 To I2
        If SetValue Then
            CliFkeyCnt(i).Caption = CStr(CountColorLines(NoopList(TB1), CliFkeyCnt(i).BackColor))
            SrvFkeyCnt(i).Caption = CStr(CountColorLines(NoopList(TB2), SrvFkeyCnt(i).BackColor))
        Else
            CliFkeyCnt(i).Caption = ""
            SrvFkeyCnt(i).Caption = ""
        End If
        CliFkeyCnt(i).Refresh
        SrvFkeyCnt(i).Refresh
    Next
    CliFkeyCnt(1).Caption = CStr(RotaJoinCnt(0))
    AdjustImpButtons
End Sub
Private Function CountColorLines(CurTab As TABLib.Tab, LookColor As Long) As Long
    Dim LineList As String
    CurTab.SetInternalLineBuffer True
    CountColorLines = Val(CurTab.GetLinesByBackColor(LookColor))
    CurTab.SetInternalLineBuffer False
End Function
Private Function FindShrinkColor(LookUpColor As Long) As Boolean
    Dim i As Integer
    Dim Result As Boolean
    Result = False
    For i = 1 To 6
        If lblShrink(i).BorderStyle = 1 Then
            If LookUpColor = lblShrink(i).BackColor Then
                Result = True
                Exit For
            End If
        End If
    Next
    If Not Result Then
        For i = 11 To 12
            If lblShrink(i).BorderStyle = 1 Then
                If LookUpColor = lblShrink(i).BackColor Then
                    Result = True
                    Exit For
                End If
            End If
        Next
    End If
    FindShrinkColor = Result
End Function

Private Function DistinctBasicData(ForWhat As String, ItemList As String) As String
    Static CurLine As Long
    Static MaxLine As Long
    Dim ItmNbr As Integer
    Dim CurItm As String
    Dim ColTxt As String
    Select Case ForWhat
        Case "INIT"
            UfisTools.BlackBox(0).ResetContent
            ItmNbr = 1
            Do
                CurItm = GetItem(ItemList, ItmNbr, ",")
                If CurItm <> "" Then
                    If Not StartedAsCompareTool Then
                        If Not NoopCheckDoubles Then
                            ColTxt = FlightExtract.ExtractData.SelectDistinct(CurItm, "", "", vbLf, True)
                        Else
                            ColTxt = MainDialog.FileData(CurFdIdx).SelectDistinct(CurItm, "", "", vbLf, True)
                        End If
                    Else
                        ColTxt = TlxImpData(1).SelectDistinct(CurItm, "", "", vbLf, True)
                    End If
                    UfisTools.BlackBox(0).InsertBuffer ColTxt, vbLf
                End If
                ItmNbr = ItmNbr + 1
            Loop While CurItm <> ""
            ColTxt = UfisTools.BlackBox(0).SelectDistinct("0", "", "", vbLf, True)
            UfisTools.BlackBox(0).ResetContent
            ColTxt = Replace(ColTxt, (vbLf & vbLf), vbLf, 1, -1, vbBinaryCompare)
            If Left(ColTxt, 1) = vbLf Then ColTxt = Mid(ColTxt, 2)
            If Right(ColTxt, 1) = vbLf Then
                ColTxt = Left(ColTxt, Len(ColTxt) - 1)
            End If
            UfisTools.BlackBox(0).InsertBuffer ColTxt, vbLf
            InitAlcBasicData
            CurLine = -1
            MaxLine = UfisTools.BlackBox(0).GetLineCount
            DistinctBasicData = Str(MaxLine)
        Case "NEXT"
            CurLine = CurLine + 1
            If CurLine < MaxLine Then
                'itemlist = Replace(UfisTools.BlackBox(0).GetLineValues(CurLine), ",", "", 1, -1, vbBinaryCompare)
                ItemList = UfisTools.BlackBox(0).GetLineValues(CurLine)
                ColTxt = Trim(GetItem(ItemList, 2, ","))
                If Len(ColTxt) <> 3 Then ColTxt = Trim(GetItem(ItemList, 1, ","))
                DistinctBasicData = ColTxt
            Else
                DistinctBasicData = "{=END=}"
            End If
        Case "RESET"
            UfisTools.BlackBox(0).ResetContent
            CurLine = -1
            MaxLine = 0
            DistinctBasicData = "0"
        Case Else
    End Select
End Function

Private Sub InitAlcBasicData()
    Dim LineNo As Long
    Dim LineCnt As Long
    Dim iLen As Integer
    Dim tmpDat As String
    Dim MyAlc3Filter As String
    Dim MyAlc2Filter As String
    LineCnt = UfisTools.BlackBox(0).GetLineCount - 1
    For LineNo = 0 To LineCnt
        tmpDat = UfisTools.BlackBox(0).GetColumnValue(LineNo, 0)
        iLen = Len(tmpDat)
        If iLen = 3 Then MyAlc3Filter = MyAlc3Filter + "'" + tmpDat + "',"
        If iLen = 2 Then MyAlc2Filter = MyAlc2Filter + "'" + tmpDat + "',"
    Next
    UfisTools.BlackBox(0).ResetContent
    If MyAlc3Filter <> "" Then
        MyAlc3Filter = Left(MyAlc3Filter, Len(MyAlc3Filter) - 1)
        ReadAlcData "ALC3", MyAlc3Filter
    End If
    If MyAlc2Filter <> "" Then
        MyAlc2Filter = Left(MyAlc2Filter, Len(MyAlc2Filter) - 1)
        ReadAlcData "ALC2", MyAlc2Filter
    End If
End Sub
Private Sub ReadAlcData(FilterField As String, FilterData As String)
    Dim RetCode As Integer
    Dim tmpSqlKey As String
    Dim tmpAlc2 As String
    Dim tmpAlc3 As String
    Dim tmpAlfn As String
    Dim RecData As String
    Dim tmpLine As String
    Dim CurLine As Long
    Dim MaxLine As Long
    If CedaIsConnected Then
        tmpSqlKey = "WHERE " & FilterField & " IN (" & FilterData & ")"
        RetCode = UfisServer.CallCeda(RecData, "RT", "ALTTAB", "ALC2,ALC3,ALFN", "", tmpSqlKey, "", 0, False, False)
        If RetCode >= 0 Then
            MaxLine = UfisServer.DataBuffer(0).GetLineCount - 1
            For CurLine = 0 To MaxLine
                RecData = UfisServer.DataBuffer(0).GetLineValues(CurLine)
                tmpAlc2 = GetItem(RecData, 1, ",")
                tmpAlc3 = GetItem(RecData, 2, ",")
                tmpAlfn = GetItem(RecData, 3, ",")
                If tmpAlc2 = "" Then tmpAlc2 = tmpAlc3
                If tmpAlc3 = "" Then tmpAlc3 = tmpAlc2
                If tmpAlc3 <> "" Then
                    If FilterField = "ALC2" Then
                        tmpLine = tmpAlc2 & "," & tmpAlc3 & "," & tmpAlfn
                    Else
                        tmpLine = tmpAlc3 & "," & tmpAlc2 & "," & tmpAlfn
                    End If
                    UfisTools.BlackBox(0).InsertTextLine tmpLine, False
                End If
            Next
        End If
        MainDialog.Refresh
    End If
End Sub

Private Function FormatFkeyToFlno(AftFkey As String, AftFlno As String, TurnedKey As Boolean) As String
    Dim Result As String
    Dim tmpDat As String
    Dim tmpVal As Integer
    If AftFlno = "" Then
        If TurnedKey Then
            Result = Left(AftFkey, 3) & Mid(AftFkey, 5, 5) & ","
        Else
            Result = Mid(AftFkey, 6, 3) & Mid(AftFkey, 2, 4) & Mid(AftFkey, 9, 1) & ","
        End If
    Else
        Result = AftFlno & ","
    End If
    Result = Result & Mid(AftFkey, 10, 8) & ","
    Result = Result & Right(AftFkey, 1)
    If Mid(Result, 8, 1) = "#" Then Mid(Result, 8, 1) = " "
    FormatFkeyToFlno = Result
End Function

Private Sub CreateDistinctFlno(AirlCode As String)
    Dim tmpBuff As String
    Dim CurLine As Long
    Dim MaxLine As Long
    FormIsBusy = True
    Me.MousePointer = 11
    UfisTools.BlackBox(0).ResetContent
    FlnoTabCombo(0).ComboResetContent "FlnoCodes"
    FlnoTabCombo(1).ResetContent
    'MaxLine = UfisTools.BlackBox(0).GetLineCount - 1
    'If MaxLine < 0 Then
        StatusBar.Panels(2).Text = "Creating Flight Filter Lines ..."
        UfisTools.BlackBox(0).ResetContent
        tmpBuff = NoopList(0).SelectDistinct(Trim(Str(FlnoCol)), "", "", vbLf, True)
        UfisTools.BlackBox(0).InsertBuffer tmpBuff, vbLf
        tmpBuff = NoopList(1).SelectDistinct(Trim(Str(FlnoCol)), "", "", vbLf, True)
        UfisTools.BlackBox(0).InsertBuffer tmpBuff, vbLf
        tmpBuff = UfisTools.BlackBox(0).SelectDistinct("0", "", "", vbLf, True)
        UfisTools.BlackBox(0).ResetContent
        tmpBuff = Replace(tmpBuff, (vbLf & vbLf), vbLf, 1, -1, vbBinaryCompare)
        If Left(tmpBuff, 1) = vbLf Then tmpBuff = Mid(tmpBuff, 2)
        If Right(tmpBuff, 1) = vbLf Then tmpBuff = Left(tmpBuff, Len(tmpBuff) - 1)
        UfisTools.BlackBox(0).InsertBuffer tmpBuff, vbLf
        UfisTools.BlackBox(0).Sort 0, True, True
    'End If
    MaxLine = UfisTools.BlackBox(0).GetLineCount - 1
    SetProgressBar 0, 1, MaxLine, "Adjusting Flight Filters ..."
    For CurLine = 0 To MaxLine
        If CurLine Mod 50 = 0 Then
            SetProgressBar 0, 2, CurLine, ""
        End If
        tmpBuff = UfisTools.BlackBox(0).GetLineValues(CurLine)
        If (AirlCode = "-ALL-") Or (RTrim(Left(tmpBuff, 3)) = AirlCode) Then FlnoTabCombo(1).InsertTextLine tmpBuff, False
    Next
    If FlnoTabCombo(1).GetLineCount > 1 Then FlnoTabCombo(1).InsertTextLineAt 0, "-ALL-,", False
    FlnoTabCombo(0).SetColumnValue 0, 0, FlnoTabCombo(1).GetColumnValue(0, 0)
    MaxLine = FlnoTabCombo(1).GetLineCount - 1
    For CurLine = 0 To MaxLine
        FlnoTabCombo(0).ComboAddTextLines "FlnoCodes", FlnoTabCombo(1).GetLineValues(CurLine), vbLf
    Next
    FlnoTabCombo(0).SetCurrentSelection 0
    CurPos(1).Caption = FlnoTabCombo(1).GetColumnValue(0, 0)
    SetProgressBar 0, 3, 0, ""
    Me.MousePointer = 0
    FormIsBusy = False
End Sub

Private Sub CreateDistinctRegn(AirlCode As String)
    Dim tmpBuff As String
    Dim CurLine As Long
    Dim MaxLine As Long
    FormIsBusy = True
    On Error Resume Next
    Me.MousePointer = 11
    RegnTabCombo(0).ComboResetContent "RegnCodes"
    RegnTabCombo(1).ResetContent
    tmpBuff = NoopList(0).SelectDistinct(Trim(Str(RegnCol)), "", "", vbLf, True)
    RegnTabCombo(1).InsertBuffer tmpBuff, vbLf
    tmpBuff = NoopList(1).SelectDistinct(Trim(Str(RegnCol)), "", "", vbLf, True)
    RegnTabCombo(1).InsertBuffer tmpBuff, vbLf
    tmpBuff = RegnTabCombo(1).SelectDistinct("0", "", "", vbLf, True)
    RegnTabCombo(1).ResetContent
    tmpBuff = Replace(tmpBuff, (vbLf & vbLf), vbLf, 1, -1, vbBinaryCompare)
    If Left(tmpBuff, 1) = vbLf Then tmpBuff = Mid(tmpBuff, 2)
    If Right(tmpBuff, 1) = vbLf Then tmpBuff = Left(tmpBuff, Len(tmpBuff) - 1)
    RegnTabCombo(1).InsertBuffer tmpBuff, vbLf
    RegnTabCombo(1).Sort 0, True, True
    MaxLine = RegnTabCombo(1).GetLineCount
    If MaxLine > 1 Then RegnTabCombo(1).InsertTextLineAt 0, "-ALL-,", False
    MaxLine = RegnTabCombo(1).GetLineCount - 1
    RegnTabCombo(0).SetColumnValue 0, 0, RegnTabCombo(1).GetColumnValue(0, 0)
    For CurLine = 0 To MaxLine
        tmpBuff = RegnTabCombo(1).GetLineValues(CurLine)
        RegnTabCombo(0).ComboAddTextLines "RegnCodes", tmpBuff, vbLf
    Next
    RegnTabCombo(0).SetCurrentSelection 0
    CurPos(1).Caption = RegnTabCombo(1).GetColumnValue(0, 0)
    Me.MousePointer = 0
    FormIsBusy = False
End Sub

Private Sub FlnoList_Click()
    Dim NewFlno As String
    Dim CurLeftFlno As String
    Dim CurRightFlno As String
    Dim tmpLine As String
    Dim tmpFkey As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim NewLine As Long
    Dim Dummy As Long
    Dim LeftBackColor As Long
    Dim RightBackColor As Long
    NewFlno = GetItem(FlnoList.Text, 1, "/")
    If NewFlno = "" Then NewFlno = GetItem(FlnoList.Text, 2, "/")
    If (Not FormIsBusy) And (FlnoList.ListCount > 2) And (NewFlno <> CurPos(1).Caption) Then
        CurPos(1).Caption = NewFlno
        If NewFlno <> "-ALL-" Then
            AdjustScrollMaster FlnoCol, 9, NewFlno, False
        Else
            CurPos(0).Caption = ""
            'Alc3List_Click
        End If
    End If
End Sub

Public Sub SetProgressBar(Index As Integer, ForWhat As Integer, CurValue As Long, Context As String)
    Static ProgFact(0 To 2) As Variant
    Dim tmpVal As Long
    Dim NewVal As Integer
    On Error Resume Next
    Select Case ForWhat
        Case 0
            ProgFact(Index) = -1
            If CurValue > 0 Then
                MyProgressBar(Index).Max = 30000
                MyProgressBar(Index).Value = 0
                ProgFact(Index) = MyProgressBar(Index).Max / CurValue
            End If
            If ProgFact(Index) > 0 Then
                NewVal = CInt(Val(Context) * ProgFact(Index))
                If NewVal <= MyProgressBar(Index).Max Then MyProgressBar(Index).Value = NewVal
                MyProgressBar(Index).Visible = True
                If Index = 0 Then
                    tmpVal = CLng(MyProgressBar(Index).Value) * 100 / CLng(MyProgressBar(Index).Max)
                    If tmpVal > 100 Then tmpVal = 100
                    CurPos(2).Caption = Str(tmpVal) & "%"
                    CurPos(2).Refresh
                End If
            End If
        Case 1
            ProgFact(Index) = -1
            If CurValue > 0 Then
                MyProgressBar(Index).Max = 30000
                ProgFact(Index) = MyProgressBar(Index).Max / CurValue
                MyProgressBar(Index).Value = 0
                MyProgressBar(Index).Visible = True
            End If
            If Index = 0 Then StatusBar.Panels(2).Text = Context
        Case 2
            If ProgFact(Index) > 0 Then
                NewVal = CInt(CurValue * ProgFact(Index))
                If NewVal <= MyProgressBar(Index).Max Then MyProgressBar(Index).Value = NewVal
                If Index = 0 Then
                    tmpVal = CLng(MyProgressBar(Index).Value) * 100 / CLng(MyProgressBar(Index).Max)
                    If tmpVal > 100 Then tmpVal = 100
                    CurPos(2).Caption = Str(tmpVal) & "%"
                    CurPos(2).Refresh
                End If
            End If
            If Context <> "" Then StatusBar.Panels(2).Text = Context
        Case 3
            ProgFact(Index) = -1
            MyProgressBar(Index).Visible = False
            If Index = 0 Then
                StatusBar.Panels(2).Text = Context
                CurPos(2).Caption = ""
            End If
    End Select
    If CheckStopButton And ShowStopButton Then DoEvents
End Sub

Private Sub RegnTabCombo_ComboSelChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal OldValue As String, ByVal NewValues As String)
    Dim NewRegn As String
    Dim CurAlc As String
    NewRegn = GetItem(NewValues, 1, ",")
    If (Not FormIsBusy) And (NewRegn <> CurPos(1).Caption) Then
        CurPos(1).Caption = NewRegn
        If NewRegn <> "-ALL-" Then
            AdjustScrollMaster RegnCol, 9, NewRegn, False
        Else
            CurAlc = CurPos(0).Caption
            CurPos(0).Caption = ""
            AlcTabCombo_ComboSelChanged 0, 0, 0, "***", CurAlc
        End If
    End If
End Sub

Private Sub RotaLink_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim ShowLineNo As Long
    Dim LookLine As String
    Dim SetLine As String
    If (Index < 4) And (LineNo >= 0) Then
        ShowLineNo = Val(RotaLink(Index).GetLineTag(LineNo))
        If ShowLineNo >= 0 Then
            LookLine = CStr(ShowLineNo)
            SetLine = NoopList(2).GetLinesByIndexValue("LineIdx", LookLine, 0)
            If SetLine <> "" Then
                ShowLineNo = Val(SetLine)
                NoopList(2).OnVScrollTo ShowLineNo
                NoopList(2).SetCurrentSelection ShowLineNo
            End If
        End If
    End If
End Sub
Private Function GetMainLineNo(CurLineNo As Long) As Long
    Dim MainLine As Long
    Dim LookLine As String
    Dim SetLine As String
    MainLine = -1
    If CurLineNo >= 0 Then
        LookLine = Trim(Str(CurLineNo))
        'SetLine = NoopList(2).GetLinesByColumnValue(4, LookLine, 0)
        SetLine = NoopList(2).GetLinesByIndexValue("LineIdx", LookLine, 0)
        If SetLine <> "" Then MainLine = Val(SetLine)
    End If
    GetMainLineNo = MainLine
End Function

Private Sub RotaLink_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    'If Index = 1 Then
    '    MsgBox "Hallo"
    'End If
End Sub

Private Sub Splitter_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If (Index <> 2) Or (TlxImpData(0).Visible) Or (TlxImpData(1).Visible) Then
        Splitter(Index).Tag = CStr(X) & "," & CStr(Y)
    End If
End Sub

Private Sub Splitter_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim tmpTag As String
    Dim tmpDiffX As Long
    Dim tmpDiffY As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewSize As Long
    Dim LeftSize As Long
    Dim RightSize As Long
    tmpTag = Splitter(Index).Tag
    If tmpTag <> "-1,-1" Then
        tmpDiffX = CLng(X) - Val(GetItem(tmpTag, 1, ","))
        tmpDiffY = CLng(Y) - Val(GetItem(tmpTag, 2, ","))
        Select Case Index
            Case 0, 1 'Horizontal
                LeftSize = NoopList(0).Width + tmpDiffX
                RightSize = NoopList(1).Width - tmpDiffX
                If (LeftSize > 600) And (RightSize > 600) Then
                    Splitter(0).Left = Splitter(0).Left + tmpDiffX
                    Splitter(1).Left = Splitter(1).Left + tmpDiffX
                    NoopList(2).Left = NoopList(2).Left + tmpDiffX
                    NoopList(0).Width = LeftSize
                    NoopList(1).Left = NoopList(1).Left + tmpDiffX
                    NoopList(1).Width = RightSize
                    ArrangeLayoutRota
                    ArrangeLayoutBottom
                    Me.Refresh
                End If
            Case 2, 3 'Vertical
                If TlxImpData(0).Visible Or TlxImpData(1).Visible Then
                    NewTop = Splitter(3).Top + tmpDiffY
                    NewSize = NoopList(0).Height - tmpDiffY
                    If NewTop < 1500 Then NewTop = 0
                    If NewSize < 900 Then NewTop = 0
                    If NewTop > 0 Then
                        Splitter(3).Top = NewTop
                        Splitter(2).Top = Splitter(2).Top + tmpDiffY
                        ArrangeLayoutVert
                        Me.Refresh
                    End If
                End If
        End Select
    End If
End Sub

Private Sub Splitter_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Splitter(Index).Tag = "-1,-1"
End Sub

Private Sub InfoFromServer_LinkClose()
    'MsgBox "LinkClose"
    InfoServerIsConnected = False
    Unload Me
    'ShutDownApplication
End Sub

Private Sub InfoFromServer_LinkError(LinkErr As Integer)
    'MsgBox "LinkError"
End Sub

Private Sub InfoFromServer_LinkNotify()
    'MsgBox "LinkNotify"
End Sub

Private Sub InfoFromServer_LinkOpen(Cancel As Integer)
    'MsgBox "LinkOpen"
    InfoServerIsConnected = True
End Sub

Private Sub txtSearch_Change(Index As Integer)
    SearchInList Index, True, False
    txtSearch(Index).SetFocus
End Sub

Private Sub txtSearch_GotFocus(Index As Integer)
    SearchInList Index, False, False
End Sub

Private Sub txtSearch_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    'KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    CheckKeyboard Shift
    If KeyCode = 114 Then
        If Shift = 0 Then
            SearchInList -1, True, False
        ElseIf Shift = 1 Then
            'Shift
        End If
    End If
    'If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
    If KeyCode = vbKeyF1 Then ShowHtmlHelp Me, "", ""
End Sub
Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    CheckKeyboard Shift
End Sub

Private Sub CliFkeyCnt_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim UseSelList As String
    Dim ActionType As String
    If Button = 2 Then
        If DataSystemType <> "OAFOS" Then
            If Val(CliFkeyCnt(Index).Caption) > 0 Then
                ActionType = DefineColorAction(CliFkeyCnt(Index).BackColor)
                If ActionType <> "DONE" Then
                    If (RotationMode) And (ActionType = "MIXED") Then ActionType = "JOIN"
                    If (RotationMode) And (ActionType <> "JOIN") Then ActionType = ""
                    If ActionType <> "" Then
                        UseSelList = NoopList(2).GetLinesByBackColor(CliFkeyCnt(Index).BackColor)
                        CallServerAction ActionType, Val(CliFkeyCnt(Index).Caption), True, UseSelList
                    End If
                End If
            End If
        End If
    Else
        If Index > 0 Then
            If Val(CliFkeyCnt(Index).Caption) > 0 Then JumpToColoredLine CliFkeyCnt(Index).BackColor, -1, -1
        Else
            JumpToColoredLine vbBlack, -1, -1
        End If
    End If
End Sub

Private Sub SrvFkeyCnt_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim UseSelList As String
    Dim ActionType As String
    If Button = 2 Then
        If DataSystemType <> "OAFOS" Then
            If Val(SrvFkeyCnt(Index).Caption) > 0 Then
                ActionType = DefineColorAction(SrvFkeyCnt(Index).BackColor)
                If ActionType <> "DONE" Then
                    If (RotationMode) And (ActionType = "MIXED") Then ActionType = "JOIN"
                    If (RotationMode) And (ActionType <> "JOIN") Then ActionType = ""
                    If ActionType <> "" Then
                        UseSelList = NoopList(2).GetLinesByBackColor(SrvFkeyCnt(Index).BackColor)
                        CallServerAction ActionType, Val(SrvFkeyCnt(Index).Caption), True, UseSelList
                    End If
                End If
            End If
        End If
    Else
        If Val(SrvFkeyCnt(Index).Caption) > 0 Then JumpToColoredLine SrvFkeyCnt(Index).BackColor, -1, -1
    End If
End Sub

Private Sub CallServerAction(ActionType As String, UseCount As Long, MyFullMode As Boolean, UseSelList As String)
    Dim GrpLineIdx As String
    Dim MsgTxt As String
    Dim ActionCode As Integer
    Dim RetVal As Integer
    Dim SqlRet As Integer
    Dim LinCnt As Long
    Dim SelLine As Long
    Dim LineNo As Long
    Dim TblNam As String
    Dim SqlCmd As String
    Dim SqlKey As String
    Dim FldLst As String
    Dim DatLst As String
    Dim SelLst As String
    Dim SelItm As Long
    Dim SelDat As String
    Dim tmpAdid As String
    Dim chkAdid As String
    Dim JumpValue As Integer
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim DiffList As String
    Dim ArrLineNo As Long
    Dim DepLineNo As Long
    Dim TotalLineCount As Long
    Dim TotalInserts As Long
    Dim IsValidRotation As Boolean
    Dim AlreadyAsked As Boolean
    If (chkWork(6).Value = 1) Then
        Load CedaSpooler
        MainPacketMinDate = "99999999"
        MainPacketMaxDate = "00000000"
        If (DataSystemType <> "OAFOS") Then
            If Not RotationMode Then
                TotalInserts = Val(CliFkeyCnt(4).Caption)
                If TotalInserts > 0 Then UfisServer.UrnoPoolPrepare TotalInserts
            End If
            Me.MousePointer = 11
            JumpValue = chkWork(8).Value
            RetVal = 1
            Me.Refresh
            If RetVal = 1 Then
                If LinCnt > 1 Then chkWork(8).Value = 0
                Me.MousePointer = 11
                RotaCover.Visible = False
                Me.Refresh
                SelLst = UseSelList
                If UseSelList <> "" Then UseSelList = ""
                If SelLst = "" Then
                    SelLst = NoopList(2).GetLinesByStatusValue(1, 0)
                    SelLine = NoopList(2).GetCurrentSelected
                    If SelLst = "" Then SelLst = Trim(Str(SelLine))
                End If
                ImportIsBusy = True
                HelperTab(0).ResetContent
                HelperTab(0).InsertBuffer SelLst, ","
                TotalLineCount = HelperTab(0).GetLineCount - 1
                SetProgressBar 0, 1, TotalLineCount, "Creating AODB Transactions ..."
                DeleteAllSelected = False
                StopCedaAction = False
                For SelItm = 0 To TotalLineCount
                    SelDat = HelperTab(0).GetColumnValue(SelItm, 0)
                    If SelDat <> "" Then
                        SelLine = Val(SelDat)
                        If SelItm Mod 150 = 0 Then
                            If SelItm > 0 Then
                                JumpToColoredLine -1, 1, SelLine
                                NoopList(2).Refresh
                                NoopList(0).Refresh
                                NoopList(1).Refresh
                                SetProgressBar 0, 2, SelItm, ""
                            End If
                        End If
                        LineNo = Val(NoopList(2).GetColumnValue(SelLine, 4))
                        If LineNo >= 0 Then
                            GrpLineIdx = AnalyseRotaLinks(LineNo, False, False)
                            If StopCedaAction = True Then Exit For
                        End If
                    End If
                Next
                SetProgressBar 0, 3, -1, ""
                HelperTab(0).ResetContent
                
                StatusBar.Panels(2).Text = ""
                ImportIsBusy = False
                If chkWork(13).Value = 1 Then RotaCover.Visible = True
                Me.MousePointer = 0
                If CedaSpooler.CedaOut(0).GetLineCount > 0 Then
                    CedaSpooler.PrepareUserInfo
                    CedaSpooler.Show , Me
                    CedaSpooler.Refresh
                End If
            End If
            chkWork(8).Value = JumpValue
            Me.MousePointer = 0
        End If
    End If
End Sub

Private Sub CheckSplitBeforeUpdate(IsValidRotation As Boolean, ArrLineNo As Long, DepLineNo As Long)
    Dim CliArrFkey As String
    Dim CliDepFkey As String
    Dim SvrArrFkey As String
    Dim SvrDepFkey As String
    Dim CliArrAct3 As String
    Dim CliDepAct3 As String
    Dim SvrArrAct3 As String
    Dim SvrDepAct3 As String
    Dim SvrArrRkey As String
    Dim SvrDepRkey As String
    Dim SqlRet As Integer
    Dim SqlKey As String
    If (ArrLineNo >= 0) And (DepLineNo >= 0) Then
        'We going to change data of a rotation and must avoid
        'an implicit change of the Departure's Aircraft Type
        'that will be copied from Arr to Dep by the Flight Handler !
        SvrDepAct3 = Trim(NoopList(1).GetColumnValue(DepLineNo, Act3Col))
        CliArrAct3 = Trim(NoopList(0).GetColumnValue(ArrLineNo, Act3Col))
        If (CliArrAct3 <> "") And (SvrDepAct3 <> "") Then
            'A server's departure record exists
            'Now check the client's arrival Act3
            If (CliArrAct3 <> SvrDepAct3) Or (Not IsValidRotation) Then
                SvrArrRkey = Trim(NoopList(1).GetColumnValue(ArrLineNo, RkeyCol))
                SvrDepRkey = Trim(NoopList(1).GetColumnValue(DepLineNo, RkeyCol))
                If (SvrArrRkey = SvrDepRkey) Or (Not IsValidRotation) Then
                    SqlKey = Trim(NoopList(1).GetColumnValue(ArrLineNo, UrnoCol))
                    SqlKey = SqlKey & "," & SqlKey
                    SqlRet = UfisServer.CallCeda(UserAnswer, "SPR", "AFTTAB", "", "", SqlKey, "", 0, False, False)
                    IsValidRotation = False
                End If
            End If
        End If
    End If
End Sub
Private Sub PrepareAftJoin(ArrLineNo As Long, DepLineNo As Long, SqlCmd As String, SqlKey As String, Fields As String, Data As String, IgnoreFields As String)
    Dim ArrUrno As String
    Dim DepUrno As String
    Dim chkTime As String
    Dim tmpTime As String
    SqlCmd = ""
    If (ArrLineNo >= 0) And (DepLineNo >= 0) Then
        SqlCmd = "JOF"
        ArrUrno = NoopList(1).GetColumnValue(ArrLineNo, UrnoCol)
        DepUrno = NoopList(1).GetColumnValue(DepLineNo, UrnoCol)
        SqlKey = ArrUrno & ",0," & DepUrno & ",0"
        Fields = "RTYP"
        Data = "I"
        chkTime = GetImportCheckTime
        tmpTime = NoopList(0).GetColumnValue(ArrLineNo, TimeCol)
        If tmpTime < chkTime Then SqlCmd = ""
        tmpTime = NoopList(0).GetColumnValue(DepLineNo, TimeCol)
        If tmpTime < chkTime Then SqlCmd = ""
    End If
End Sub

Private Sub PrepareAftSplit(ArrLineNo As Long, DepLineNo As Long, SqlCmd As String, SqlKey As String, Fields As String, Data As String, IgnoreFields As String)
    Dim ArrUrno As String
    Dim DepUrno As String
    Dim ArrRkey As String
    Dim DepRkey As String
    Dim chkTime As String
    Dim tmpTime As String
    SqlCmd = ""
    If (ArrLineNo >= 0) And (DepLineNo >= 0) Then
        SqlCmd = "SPR"
        ArrRkey = NoopList(1).GetColumnValue(ArrLineNo, RkeyCol)
        DepRkey = NoopList(1).GetColumnValue(DepLineNo, RkeyCol)
        If ArrRkey = DepRkey Then
            ArrUrno = NoopList(1).GetColumnValue(ArrLineNo, UrnoCol)
            DepUrno = NoopList(1).GetColumnValue(DepLineNo, UrnoCol)
            SqlKey = ArrUrno & "," & ArrUrno
            Fields = ""
            Data = ""
            chkTime = GetImportCheckTime
            tmpTime = NoopList(0).GetColumnValue(ArrLineNo, TimeCol)
            If tmpTime < chkTime Then SqlCmd = ""
            tmpTime = NoopList(0).GetColumnValue(DepLineNo, TimeCol)
            If tmpTime < chkTime Then SqlCmd = ""
        End If
    End If
End Sub

Private Sub PrepareAftInsert(LineNo As Long, SqlCmd As String, SqlKey As String, Fields As String, Data As String, IgnoreFields As String)
    Dim CliRec As String
    Dim tmpFtyp As String
    Dim tmpAdid As String
    Dim tmpDate As String
    Dim tmpFlno As String
    Dim tmpAirl As String
    Dim tmpFltn As String
    Dim tmpFlns As String
    Dim tmpTime As String
    Dim chkTime As String
    Dim tmpApc3 As String
    Dim tmpValu As String
    Dim tmpApdx As String
    
    CliRec = NoopList(0).GetLineValues(LineNo)
    
    If DataSystemType <> "OAFOS" Then
        SqlCmd = "IFR"
        SqlKey = ""
        Fields = "URNO"
        Data = UfisServer.UrnoPoolGetNext
        NoopList(1).SetColumnValue LineNo, UrnoCol, Data
    Else
        SqlCmd = "ISF"
        'Extract of aftdata.doc ('flight' documentation)
        'Command ISF: Insert Seasonal Flight
        'Selection bei einem Flug
        '1. Date (YYYYMMDD) Begin of the period
        '2. Date (YYYYMMDD) End of the period
        '3. Operatioanl days (1-7) of the flight
        '4. Weekly frequency (1..2..) of the flight
        tmpDate = Left(GetRealItem(CliRec, TimeCol, ","), 8)
        SqlKey = tmpDate & "," & tmpDate & ",1234567,1"
        Fields = ""
        Data = ""
    End If
    tmpAdid = GetRealItem(CliRec, AdidCol, ",")
    Fields = Fields & ",FTYP"
    tmpFtyp = GetRealItem(CliRec, FtypCol, ",")
    Data = Data & "," & tmpFtyp
    tmpFlno = GetRealItem(CliRec, FlnoCol, ",")
    StripAftFlno tmpFlno, tmpAirl, tmpFltn, tmpFlns
    Fields = Fields & ",FLNO"
    Data = Data & "," & tmpFlno
    Fields = Fields & ",ALC3"
    Data = Data & "," & Alc3LookUp(Trim(Left(GetRealItem(CliRec, FlnoCol, ","), 3)))
    Fields = Fields & ",FLTN"
    Data = Data & "," & tmpFltn
    Fields = Fields & ",FLNS"
    Data = Data & "," & tmpFlns
    Select Case tmpAdid
        Case "A"
            tmpValu = GetRealItem(CliRec, SkedCol, ",")
            If tmpValu <> "" Then
                Fields = Fields & ",STOD"
                Data = Data & "," & tmpValu & "00"
            End If
            Fields = Fields & ",STOA"
            Data = Data & "," & GetRealItem(CliRec, TimeCol, ",") & "00"
            tmpApc3 = GetRealItem(CliRec, Apc3Col, ",")
            If Len(tmpApc3) = 3 Then Fields = Fields & ",ORG3" Else Fields = Fields & ",ORG4"
            Data = Data & "," & tmpApc3
            Fields = Fields & ",DES3"
            Data = Data & "," & HomeAirport
            If (AddOnArrFields <> "") And (AddOnArrAftFld <> "") Then
                tmpApdx = GetRealItem(CliRec, ApdxCol, ",")
                tmpApdx = Replace(tmpApdx, ";", ",", 1, -1, vbBinaryCompare)
                Fields = Fields & "," & AddOnArrAftFld
                Data = Data & "," & tmpApdx
            End If
                
        Case "D"
            Fields = Fields & ",STOD"
            Data = Data & "," & GetRealItem(CliRec, TimeCol, ",") & "00"
            tmpValu = GetRealItem(CliRec, SkedCol, ",")
            If tmpValu <> "" Then
                Fields = Fields & ",STOA"
                Data = Data & "," & tmpValu & "00"
            End If
            Fields = Fields & ",ORG3"
            Data = Data & "," & HomeAirport
            tmpApc3 = GetRealItem(CliRec, Apc3Col, ",")
            If Len(tmpApc3) = 3 Then Fields = Fields & ",DES3" Else Fields = Fields & ",DES4"
            Data = Data & "," & tmpApc3
            If (AddOnDepFields <> "") And (AddOnDepAftFld <> "") Then
                tmpApdx = GetRealItem(CliRec, ApdxCol, ",")
                tmpApdx = Replace(tmpApdx, ";", ",", 1, -1, vbBinaryCompare)
                Fields = Fields & "," & AddOnDepAftFld
                Data = Data & "," & tmpApdx
            End If
        Case Else
    End Select
    If InStr(IgnoreFields, "REGN") = 0 Then
        If GetRealItem(CliRec, RegnCol, ",") <> "" Then
            Fields = Fields & ",REGN"
            Data = Data & "," & GetRealItem(CliRec, RegnCol, ",")
        End If
    End If
    If ImportAct3Act5 Then
        tmpValu = GetRealItem(CliRec, Act3Col, ",")
        Fields = Fields & ",ACT3"
        Data = Data & "," & GetRealItem(tmpValu, 0, "/")
        Fields = Fields & ",ACT5"
        Data = Data & "," & GetRealItem(tmpValu, 1, "/")
    Else
        Fields = Fields & ",ACT3"
        Data = Data & "," & GetRealItem(CliRec, Act3Col, ",")
    End If
    Fields = Fields & ",STYP"
    Data = Data & "," & GetRealItem(CliRec, StypCol, ",")
    If UseFldaField Then
        Fields = Fields & ",FLDA"
        Data = Data & "," & GetRealItem(CliRec, FldaCol, ",")
    End If
    If UseRoutField Then
        Fields = Fields & ",ROUT"
        Data = Data & "," & GetRealItem(CliRec, RoutCol, ",")
    Else
        Fields = Fields & ",VIAL"
        tmpApc3 = GetRealItem(CliRec, Via3Col, ",")
        If Len(tmpApc3) = 3 Then
            Data = Data & ", " & tmpApc3
        Else
            Data = Data & ",    " & tmpApc3
        End If
    End If
    Fields = Replace(Fields, "STYP", UseStypField, 1, -1, vbBinaryCompare)
    If DataSystemType <> "OAFOS" Then
        If (tmpFtyp <> "S") And (tmpFtyp <> "O") Then
            SqlKey = ""
            SqlCmd = ""
        End If
    Else
        If Left(Fields, 1) = "," Then
            Fields = Mid(Fields, 2)
            Data = Mid(Data, 2)
        End If
    End If
    chkTime = GetImportCheckTime
    tmpTime = GetRealItem(CliRec, TimeCol, ",")
    If tmpTime < chkTime Then SqlCmd = ""
End Sub

Private Sub FinishAftInsert(SelLine As Long, DoJump As Boolean)
    Dim LineNo As Long
    Dim i As Integer
    NoopList(2).SetLineColor SelLine, vbBlack, LightGray
    NoopList(2).ResetCellProperties SelLine
    NoopList(2).SetLineStatusValue SelLine, -1
    NoopList(2).RedrawTab
    LineNo = Val(NoopList(2).GetColumnValue(SelLine, 4))
    For i = 0 To 1
        NoopList(i).SetLineColor LineNo, vbBlack, LightGray
        NoopList(i).RedrawTab
    Next
    CliFkeyCnt(4).Caption = Str(Val(CliFkeyCnt(4).Caption) - 1)
    CliFkeyCnt(6).Caption = Str(Val(CliFkeyCnt(6).Caption) + 1)
    SrvFkeyCnt(6).Caption = Str(Val(SrvFkeyCnt(6).Caption) + 1)
    If Val(CliFkeyCnt(11).Caption) > 0 Then
        CliFkeyCnt(11).Caption = Str(Val(CliFkeyCnt(11).Caption) - 1)
        CliFkeyCnt(13).Caption = Str(Val(CliFkeyCnt(13).Caption) + 1)
        SrvFkeyCnt(13).Caption = Str(Val(SrvFkeyCnt(13).Caption) + 1)
    End If
    If (DoJump = True) And (chkWork(8).Value = 1) And (Val(CliFkeyCnt(4).Caption) > 0) Then JumpToColoredLine CliFkeyCnt(4).BackColor, -1, -1
End Sub

Private Sub PrepareAftUpdate(LineNo As Long, SqlCmd As String, SqlKey As String, Fields As String, Data As String, UpdColList As String, IgnoreFields As String)
    Dim CliRec As String
    Dim SrvRec As String
    Dim tmpAdid As String
    Dim chkTime As String
    Dim tmpTime As String
    Dim tmpApc3 As String
    Dim tmpValu As String
    Dim tmpApdx As String
    Dim tmpSrvValu As String
    Dim tmpCliDate As String
    Dim tmpSrvDate As String
    CliRec = NoopList(0).GetLineValues(LineNo)
    SrvRec = NoopList(1).GetLineValues(LineNo)
    SqlCmd = "UFR"
    SqlKey = "WHERE URNO=" & GetRealItem(SrvRec, UrnoCol, ",")
    Fields = ""
    Data = ""
    tmpAdid = GetRealItem(SrvRec, AdidCol, ",")
    If Not CompareValues(LineNo, FtypCol, False, True, "S,O", False) Then
        If frmUfrFields.chkFtyp.Value = 1 Then
            Fields = Fields & ",FTYP"
            Data = Data & "," & GetRealItem(CliRec, FtypCol, ",")
        End If
    End If
    If InStr(IgnoreFields, "REGN") = 0 Then
        'update by phyoe
        If GetRealItem(CliRec, RegnCol, ",") <> "" Then
        'If Len(GetRealItem(CliRec, RegnCol, ",")) > -1 Then
            If Not CompareValues(LineNo, RegnCol, False, True, "", True) Then
                Fields = Fields & ",REGN"
                Data = Data & "," & GetRealItem(CliRec, RegnCol, ",")
            End If
        End If
    End If
    If InStr(IgnoreFields, "REMP") = 0 Then
        'update by phyoe
        If GetRealItem(CliRec, RemaCol, ",") <> "" Then
        'If Len(GetRealItem(CliRec, RemaCol, ",")) > -1 Then
            If Not CompareValues(LineNo, RemaCol, False, True, "", True) Then
                Fields = Fields & ",REMP"
                Data = Data & "," & GetRealItem(CliRec, RemaCol, ",")
            End If
        End If
    End If
    If Not CompareValues(LineNo, Act3Col, False, True, "", False) Then
        If frmUfrFields.chkAct3.Value = 1 Then
            If ImportAct3Act5 Then
                tmpValu = GetRealItem(CliRec, Act3Col, ",")
                Fields = Fields & ",ACT3"
                Data = Data & "," & GetRealItem(tmpValu, 0, "/")
                Fields = Fields & ",ACT5"
                Data = Data & "," & GetRealItem(tmpValu, 1, "/")
            Else
                Fields = Fields & ",ACT3"
                Data = Data & "," & GetRealItem(CliRec, Act3Col, ",")
            End If
        End If
    End If
    tmpValu = GetRealItem(CliRec, StypCol, ",")
    If (tmpValu <> "") And (Left(tmpValu, 1) <> "?") Then
        If Not CompareValues(LineNo, StypCol, False, True, "", False) Then
            Fields = Fields & ",STYP"
            Data = Data & "," & GetRealItem(CliRec, StypCol, ",")
        End If
    End If
    Select Case tmpAdid
        Case "A"
            If Not CompareValues(LineNo, SkedCol, False, True, "", False) Then
                If frmUfrFields.ArrStod.Value = 1 Then
                    tmpValu = GetRealItem(CliRec, SkedCol, ",")
                    If tmpValu <> "" Then
                        If frmUfrFields.chkDatesOnly.Value = 1 Then
                            tmpSrvValu = GetRealItem(SrvRec, SkedCol, ",")
                            If tmpSrvValu <> "" Then
                                tmpCliDate = Left(tmpValu, 8)
                                tmpSrvDate = Left(tmpSrvValu, 8)
                                If tmpCliDate <> tmpSrvDate Then
                                    Mid(tmpValu, 9, 4) = Mid(tmpSrvValu, 9, 4)
                                Else
                                    tmpValu = ""
                                End If
                            End If
                        End If
                        If tmpValu <> "" Then
                            Fields = Fields & ",STOD"
                            Data = Data & "," & tmpValu & "00"
                        End If
                    End If
                End If
            End If
            If Not CompareValues(LineNo, TimeCol, False, True, "", False) Then
                If frmUfrFields.ArrStoa.Value = 1 Then
                    Fields = Fields & ",STOA"
                    Data = Data & "," & GetRealItem(CliRec, TimeCol, ",") & "00"
                End If
            End If
            If Not CompareValues(LineNo, Apc3Col, False, True, "", False) Then
                If frmUfrFields.chkOrg3.Value = 1 Then
                    tmpApc3 = GetRealItem(CliRec, Apc3Col, ",")
                    If Len(tmpApc3) = 3 Then Fields = Fields & ",ORG3" Else Fields = Fields & ",ORG4"
                    Data = Data & "," & tmpApc3
                End If
            End If
            If (AddOnArrFields <> "") And (AddOnArrAftFld <> "") Then
                tmpApdx = GetRealItem(CliRec, ApdxCol, ",")
                tmpApdx = Replace(tmpApdx, ";", ",", 1, -1, vbBinaryCompare)
                Fields = Fields & "," & AddOnArrAftFld
                Data = Data & "," & tmpApdx
            End If
        Case "D"
            If Not CompareValues(LineNo, TimeCol, False, True, "", False) Then
                If frmUfrFields.DepStod.Value = 1 Then
                    Fields = Fields & ",STOD"
                    Data = Data & "," & GetRealItem(CliRec, TimeCol, ",") & "00"
                End If
            End If
            If Not CompareValues(LineNo, SkedCol, False, True, "", False) Then
                If frmUfrFields.DepStoa.Value = 1 Then
                    tmpValu = GetRealItem(CliRec, SkedCol, ",")
                    If tmpValu <> "" Then
                        If frmUfrFields.chkDatesOnly.Value = 1 Then
                            tmpSrvValu = GetRealItem(SrvRec, SkedCol, ",")
                            If tmpSrvValu <> "" Then
                                tmpCliDate = Left(tmpValu, 8)
                                tmpSrvDate = Left(tmpSrvValu, 8)
                                If tmpCliDate <> tmpSrvDate Then
                                    Mid(tmpValu, 9, 4) = Mid(tmpSrvValu, 9, 4)
                                Else
                                    tmpValu = ""
                                End If
                            End If
                        End If
                        If tmpValu <> "" Then
                            Fields = Fields & ",STOA"
                            Data = Data & "," & tmpValu & "00"
                        End If
                    End If
                End If
            End If
            If Not CompareValues(LineNo, Apc3Col, False, True, "", False) Then
                If frmUfrFields.chkDes3.Value = 1 Then
                    tmpApc3 = GetRealItem(CliRec, Apc3Col, ",")
                    If Len(tmpApc3) = 3 Then Fields = Fields & ",DES33" Else Fields = Fields & ",DES44"
                    Data = Data & "," & tmpApc3
                End If
            End If
            If (AddOnDepFields <> "") And (AddOnDepAftFld <> "") Then
                tmpApdx = GetRealItem(CliRec, ApdxCol, ",")
                tmpApdx = Replace(tmpApdx, ";", ",", 1, -1, vbBinaryCompare)
                Fields = Fields & "," & AddOnDepAftFld
                Data = Data & "," & tmpApdx
            End If
        Case Else
    End Select
    If UseFldaField Then
        If Not CompareValues(LineNo, FldaCol, False, True, "", False) Then
            Fields = Fields & ",FLDA"
            Data = Data & "," & GetRealItem(CliRec, FldaCol, ",")
        End If
    End If
    If UseRoutField Then
        If Not CompareValues(LineNo, RoutCol, False, True, "", True) Then
            If frmUfrFields.chkVia3.Value = 1 Then
                Fields = Fields & ",ROUT"
                Data = Data & "," & GetRealItem(CliRec, RoutCol, ",")
            End If
        End If
    Else
        If Not CompareValues(LineNo, Via3Col, False, True, "", True) Then
            If frmUfrFields.chkVia3.Value = 1 Then
                Fields = Fields & ",VIAL"
                tmpApc3 = GetRealItem(CliRec, Via3Col, ",")
                If Len(tmpApc3) = 3 Then
                    Data = Data & ", " & tmpApc3
                Else
                    Data = Data & ",    " & tmpApc3
                End If
            End If
        End If
    End If
    Fields = Mid(Fields, 2)
    Data = Mid(Data, 2)
    Fields = Replace(Fields, "STYP", UseStypField, 1, -1, vbBinaryCompare)
    If Fields = "" Then SqlCmd = ""
    chkTime = GetImportCheckTime
    tmpTime = GetRealItem(CliRec, TimeCol, ",")
    If tmpTime < chkTime Then SqlCmd = ""
    tmpTime = GetRealItem(SrvRec, TimeCol, ",")
    If tmpTime < chkTime Then SqlCmd = ""
End Sub

Private Sub FinishAftUpdate(SelLine As Long, DoJump As Boolean)
    Dim LineNo As Long
    Dim i As Integer
    NoopList(2).SetLineColor SelLine, vbBlack, LightGray
    NoopList(2).ResetCellProperties SelLine
    NoopList(2).SetLineStatusValue SelLine, -1
    NoopList(2).RedrawTab
    LineNo = Val(NoopList(2).GetColumnValue(SelLine, 4))
    If NoopList(0).GetColumnValue(LineNo, FlnoCol) <> "" Then
        NoopList(1).SetColumnValue LineNo, TimeCol, NoopList(0).GetColumnValue(LineNo, TimeCol)
        NoopList(1).SetColumnValue LineNo, Apc3Col, NoopList(0).GetColumnValue(LineNo, Apc3Col)
        NoopList(1).SetColumnValue LineNo, Via3Col, NoopList(0).GetColumnValue(LineNo, Via3Col)
        NoopList(1).SetColumnValue LineNo, StypCol, NoopList(0).GetColumnValue(LineNo, StypCol)
        NoopList(1).SetColumnValue LineNo, Act3Col, NoopList(0).GetColumnValue(LineNo, Act3Col)
    End If
    For i = 0 To 1
        NoopList(i).SetLineColor LineNo, vbBlack, LightGray
        NoopList(i).ResetLineDecorations LineNo
        NoopList(i).RedrawTab
    Next
    SrvFkeyCnt(1).Caption = Str(Val(SrvFkeyCnt(1).Caption) - 1)
    SrvFkeyCnt(4).Caption = Str(Val(SrvFkeyCnt(4).Caption) + 1)
    If Val(SrvFkeyCnt(8).Caption) > 0 Then
        SrvFkeyCnt(8).Caption = Str(Val(SrvFkeyCnt(8).Caption) - 1)
        SrvFkeyCnt(5).Caption = Str(Val(SrvFkeyCnt(5).Caption) + 1)
    End If
    CliFkeyCnt(1).Caption = Str(Val(CliFkeyCnt(1).Caption) - 1)
    CliFkeyCnt(4).Caption = Str(Val(CliFkeyCnt(4).Caption) + 1)
    If Val(CliFkeyCnt(8).Caption) > 0 Then
        CliFkeyCnt(8).Caption = Str(Val(CliFkeyCnt(8).Caption) - 1)
        CliFkeyCnt(5).Caption = Str(Val(CliFkeyCnt(5).Caption) + 1)
    End If
    If (DoJump = True) And (chkWork(8).Value = 1) And (Val(SrvFkeyCnt(1).Caption) > 0) Then JumpToColoredLine SrvFkeyCnt(3).BackColor, -1, -1
End Sub

Private Sub PrepareAftDelete(LineNo As Long, SqlCmd As String, SqlKey As String, Fields As String, Data As String, UpdColList As String)
    Dim SrvRec As String
    Dim CurFlno As String
    Dim CurFtyp As String
    Dim CurTime As String
    Dim RetVal As Integer
    Dim MsgTxt As String
    Dim tmpTime As String
    Dim chkTime As String
    SrvRec = NoopList(1).GetLineValues(LineNo)
    RetVal = 1
    If Not DeleteAllSelected Then
        CurFlno = GetRealItem(SrvRec, FlnoCol, ",")
        If CheckAlcDelError(CurFlno) Then
            MsgTxt = ""
            MsgTxt = MsgTxt & "You ignored errors of the import file" & vbNewLine
            MsgTxt = MsgTxt & "in the main data list. Thus the compared" & vbNewLine
            MsgTxt = MsgTxt & "flight lists are not complete regarding" & vbNewLine
            MsgTxt = MsgTxt & "the selected flight carrier." & vbNewLine & vbNewLine
            CurTime = GetRealItem(SrvRec, TimeCol, ",")
            CurTime = DecodeSsimDayFormat(CurTime, "CEDA", "SSIM2")
            
            If chkWork(19).Value = 1 Then
                MsgTxt = MsgTxt & "Do you want to set flight '" & CurFlno & "' of " & CurTime & " to NOOP?"
            Else
                MsgTxt = MsgTxt & "Do you want to delete flight '" & CurFlno & "' of " & CurTime & "?"
            End If
            
            RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Deletion Control", MsgTxt, "stop2", "Yes;F,All,No,Stop", UserAnswer)
            If RetVal = 2 Then
                DeleteAllSelected = True
                RetVal = 1
            End If
        End If
    End If
    If RetVal = 1 Then
        If chkWork(19).Value = 1 Then
            CurFtyp = GetRealItem(SrvRec, FtypCol, ",")
            If CurFtyp <> FtypOnDelete Then
                SqlCmd = "UFR"
                Fields = "FTYP"
                Data = FtypOnDelete
                SqlKey = "WHERE URNO=" & GetRealItem(SrvRec, UrnoCol, ",")
            End If
        Else
            SqlCmd = "DFR"
            SqlKey = "WHERE URNO=" & GetRealItem(SrvRec, UrnoCol, ",")
        End If
    Else
        SqlCmd = ""
        If RetVal = 4 Then StopCedaAction = True
    End If
    chkTime = GetImportCheckTime
    tmpTime = GetRealItem(SrvRec, TimeCol, ",")
    If tmpTime < chkTime Then SqlCmd = ""
End Sub

Private Sub FinishAftDelete(SelLine As Long)
    Dim LineNo As Long
    Dim i As Integer
    NoopList(2).SetLineColor SelLine, vbBlack, LightGray
    NoopList(2).ResetCellProperties SelLine
    NoopList(2).SetLineStatusValue SelLine, -1
    NoopList(2).RedrawTab
    LineNo = Val(NoopList(2).GetColumnValue(SelLine, 4))
    For i = 0 To 1
        NoopList(i).SetLineColor LineNo, vbBlack, LightGray
        NoopList(i).ResetLineDecorations LineNo
        NoopList(i).RedrawTab
    Next
    SrvFkeyCnt(2).Caption = Str(Val(SrvFkeyCnt(2).Caption) - 1)
    SrvFkeyCnt(4).Caption = Str(Val(SrvFkeyCnt(4).Caption) + 1)
    CliFkeyCnt(4).Caption = Str(Val(CliFkeyCnt(4).Caption) + 1)
    If Val(SrvFkeyCnt(7).Caption) > 0 Then
        SrvFkeyCnt(7).Caption = Str(Val(SrvFkeyCnt(7).Caption) - 1)
        SrvFkeyCnt(5).Caption = Str(Val(SrvFkeyCnt(5).Caption) + 1)
        CliFkeyCnt(5).Caption = Str(Val(CliFkeyCnt(5).Caption) + 1)
    End If
    If (chkWork(8).Value = 1) And (Val(SrvFkeyCnt(2).Caption) > 0) Then JumpToColoredLine SrvFkeyCnt(2).BackColor, -1, -1
End Sub

Private Function TrimRecordData(RecData As String) As String
    Dim OldResult As String
    Dim NewResult As String
    Dim LastNonBlank As Long
    Dim CurPos As Long
    Dim MaxPos As Long
    OldResult = RecData
    NewResult = Replace(OldResult, " ,", ",", 1, -1, vbBinaryCompare)
    While NewResult <> OldResult
        OldResult = NewResult
        NewResult = Replace(OldResult, " ,", ",", 1, -1, vbBinaryCompare)
    Wend
    TrimRecordData = RTrim(NewResult)
End Function

Private Function CheckAlcDelError(ChkFlno As String) As Boolean
    Dim ChkAlc As String
    ChkAlc = "," + Trim(Left(ChkFlno, 3)) + ","
    If (MyFullMode) And (InStr(ChkErrorList, ChkAlc) > 0) Then
        CheckAlcDelError = True
    Else
        CheckAlcDelError = False
    End If
End Function

Private Function GetImportCheckTime() As String
    Dim chkTime As String
    chkTime = ServerTime.Tag
    GetImportCheckTime = chkTime
End Function
Public Sub EvaluateBc(BcCmd As String, BcTws As String, BcSelect As String, BcFields As String, BcData As String)
    Dim tmpResult As String
    Dim LineCode As String
    Dim CurLine As Long
    Select Case BcCmd
        Case "ACCBGN"
            TotalStartZeit = Timer
            TotalSecs.Caption = ""
            TotalSecs.Refresh
        Case "RAC", "ACCEND"
            tmpResult = Trim(Str(Timer - TotalStartZeit))
            If Left(tmpResult, 1) = "." Then tmpResult = "0" & tmpResult
            tmpResult = GetItem(tmpResult, 1, ".") & "s"
            TotalSecs.Caption = tmpResult
            TotalSecs.Refresh
        Case "RAX"
            LineCode = GetItem(BcTws, 4, ".")
            CurLine = Val(LineCode)
            If CurLine >= 0 Then
                tmpResult = Trim(NoopList(0).GetColumnValue(CurLine, RegnCol))
                If tmpResult <> "" Then
                    CedaProgress.AppendRaxData BcSelect, tmpResult
                End If
            End If
        Case Else
    End Select
End Sub

Private Sub SendDataToImpMgr(SelLine As Long, ArrLineNo As Long, DepLineNo As Long)
    Dim ArrRec As String
    Dim DepRec As String
    Dim ArrFldLst As String
    Dim DepFldLst As String
    Dim ImpArrRec As String
    Dim ImpDepRec As String
    Dim CedaFields As String
    Dim CedaData As String
    Dim SrvAnsw As String
    Dim tmpVia3 As String
    Dim ArrAct3 As String
    Dim DepAct3 As String
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim MainLineNo As Long
    'Function of Version 4.4 unused in 4.5
    ArrRec = ""
    DepRec = ""
    If ArrLineNo >= 0 Then ArrRec = NoopList(0).GetLineValues(ArrLineNo)
    If DepLineNo >= 0 Then DepRec = NoopList(0).GetLineValues(DepLineNo)
    ArrFldLst = "VPFR,VPTO,FRQD,FRQW,ORG3,VIAL,STOA,ACT3,STYP,FLNO,AIRL,FLTN,FLNS,DES3,FTYP"
    DepFldLst = "VPFR,VPTO,FRQD,FRQW,DES3,VIAL,STOD,ACT3,STYP,FLNO,AIRL,FLTN,FLNS,ORG3,FTYP"
    ArrFldLst = Replace(ArrFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
    DepFldLst = Replace(DepFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
    ImpArrRec = ""
    ImpArrRec = ImpArrRec & Left(GetRealItem(ArrRec, TimeCol, ","), 8) & ","
    ImpArrRec = ImpArrRec & Left(GetRealItem(ArrRec, TimeCol, ","), 8) & ","
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, DoopCol, ",") & ",1,"
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, Apc3Col, ",") & ","
    tmpVia3 = GetRealItem(ArrRec, Via3Col, ",")
    If (tmpVia3 <> "") And (MainDialog.UseVialFlag = "YES") Then
        ImpArrRec = ImpArrRec & "1" & tmpVia3 & ","
    Else
        ImpArrRec = ImpArrRec & " " & tmpVia3 & ","
    End If
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, TimeCol, ",") & "00,"
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, Act3Col, ",") & ","
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, StypCol, ",") & ","
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, FlnoCol, ",") & ","
    ImpArrRec = ImpArrRec & Left(GetRealItem(ArrRec, FkeyCol, ","), 3) & ","
    ImpArrRec = ImpArrRec & Trim(Mid(GetRealItem(ArrRec, FlnoCol, ","), 4, 5)) & ","
    ImpArrRec = ImpArrRec & Trim(Mid(GetRealItem(ArrRec, FlnoCol, ","), 9, 1)) & ","
    ImpArrRec = ImpArrRec & HomeAirport & ","
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, FtypCol, ",")
    ImpDepRec = ""
    ImpDepRec = ImpDepRec & Left(GetRealItem(DepRec, TimeCol, ","), 8) & ","
    ImpDepRec = ImpDepRec & Left(GetRealItem(DepRec, TimeCol, ","), 8) & ","
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, DoopCol, ",") & ",1,"
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, Apc3Col, ",") & ","
    tmpVia3 = GetRealItem(DepRec, Via3Col, ",")
    If (tmpVia3 <> "") And (MainDialog.UseVialFlag = "YES") Then
        ImpDepRec = ImpDepRec & "1" & tmpVia3 & ","
    Else
        ImpDepRec = ImpDepRec & " " & tmpVia3 & ","
    End If
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, TimeCol, ",") & "00,"
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, Act3Col, ",") & ","
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, StypCol, ",") & ","
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, FlnoCol, ",") & ","
    ImpDepRec = ImpDepRec & Left(GetRealItem(DepRec, FkeyCol, ","), 3) & ","
    ImpDepRec = ImpDepRec & Trim(Mid(GetRealItem(DepRec, FlnoCol, ","), 4, 5)) & ","
    ImpDepRec = ImpDepRec & Trim(Mid(GetRealItem(DepRec, FlnoCol, ","), 9, 1)) & ","
    ImpDepRec = ImpDepRec & HomeAirport & ","
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, FtypCol, ",")
    
    ArrAct3 = GetRealItem(ArrRec, Act3Col, ",")
    DepAct3 = GetRealItem(DepRec, Act3Col, ",")
    If ArrAct3 = DepAct3 Then
        CedaFields = ArrFldLst & vbLf & DepFldLst & vbLf
        CedaData = ImpArrRec & vbLf & ImpDepRec & vbLf
        UfisServer.CallCeda SrvAnsw, "EXCO", "AFTTAB", CedaFields, CedaData, "IMP,IMPORT", "", 0, False, False
    Else
        CedaFields = ArrFldLst & vbLf & DepFldLst & vbLf
        CedaData = ImpArrRec & vbLf & ",,,,,,,,,,,,,,," & vbLf
        UfisServer.CallCeda SrvAnsw, "EXCO", "AFTTAB", CedaFields, CedaData, "IMP,IMPORT", "", 0, False, False
        CedaFields = ArrFldLst & vbLf & DepFldLst & vbLf
        CedaData = ",,,,,,,,,,,,,,," & vbLf & ImpDepRec & vbLf
        UfisServer.CallCeda SrvAnsw, "EXCO", "AFTTAB", CedaFields, CedaData, "IMP,IMPORT", "", 0, False, False
    End If
    
    If SelLine <> ArrLineNo Then MainLineNo = GetMainLineNo(ArrLineNo) Else MainLineNo = SelLine
    NoopList(0).GetLineColor ArrLineNo, CurForeColor, CurBackColor
    If CurBackColor = CliFkeyCnt(3).BackColor Then FinishAftUpdate MainLineNo, False
    If CurBackColor = CliFkeyCnt(4).BackColor Then FinishAftInsert MainLineNo, False
    If SelLine <> DepLineNo Then MainLineNo = GetMainLineNo(DepLineNo) Else MainLineNo = SelLine
    NoopList(0).GetLineColor DepLineNo, CurForeColor, CurBackColor
    If CurBackColor = CliFkeyCnt(3).BackColor Then FinishAftUpdate MainLineNo, False
    If CurBackColor = CliFkeyCnt(4).BackColor Then FinishAftInsert MainLineNo, False
    
    If Val(CliFkeyCnt(2).Caption) > 1 Then CliFkeyCnt(2).Caption = Trim(Str(Val(CliFkeyCnt(2).Caption) - 1))
    If Val(CliFkeyCnt(2).Caption) > 1 Then CliFkeyCnt(2).Caption = Trim(Str(Val(CliFkeyCnt(2).Caption) - 1))
    If Val(SrvFkeyCnt(2).Caption) > 1 Then SrvFkeyCnt(2).Caption = Trim(Str(Val(SrvFkeyCnt(2).Caption) - 1))
    If Val(SrvFkeyCnt(2).Caption) > 1 Then SrvFkeyCnt(2).Caption = Trim(Str(Val(SrvFkeyCnt(2).Caption) - 1))
    NoopList(0).SetLineTag ArrLineNo, "DONE"
    NoopList(0).SetLineTag DepLineNo, "DONE"
    NoopList(0).RedrawTab
End Sub

Private Sub ToggleDetailCaption()
    NoopList(0).SetMainHeaderValues MainHeadCols, "Import Data,Route Data,Rotation Data,System Data,Appendix", "12632256,12632256,12632256,12632256"
    NoopList(0).MainHeader = True
    NoopList(1).SetMainHeaderValues MainHeadCols, "AODB Data,Route Data,Rotation Data,System Data,Appendix", "12632256,12632256,12632256,12632256"
    NoopList(1).MainHeader = True
    If chkRota(5).Value = 1 Then
        RotaLink(0).SetMainHeaderValues MainHeadCols, "Import Rotation,Route Data,Linked Flight,System Data,Appendix", "12632256,12632256,12632256,12632256"
        RotaLink(0).MainHeader = True
        RotaLink(1).SetMainHeaderValues MainHeadCols, "AODB Rotation,Route Data,Linked Flight,System Data,Appendix", "12632256,12632256,12632256,12632256"
        RotaLink(1).MainHeader = True
        RotaLink(2).SetMainHeaderValues MainHeadCols, "Import Flight Pair,Route Data,Linked Flight,System Data,Appendix", "12632256,12632256,12632256,12632256"
        RotaLink(2).MainHeader = True
        RotaLink(3).SetMainHeaderValues MainHeadCols, "AODB Flight Data,Route Data,Linked Flight,System Data,Appendix", "12632256,12632256,12632256,12632256"
        RotaLink(3).MainHeader = True
        RotaLink(4).SetMainHeaderValues "4,1", "Selected Flight,  ", "12632256,12632256"
        RotaLink(4).MainHeader = True
        RotaLink(5).SetMainHeaderValues "4,1", "Operational Days,  ", "12632256,12632256"
        RotaLink(5).MainHeader = True
    Else
        RotaLink(0).SetMainHeaderValues MainHeadCols, "Import File Departure Rotation,Route Data,Linked Flight,System Data,Appendix", "12632256,12632256,12632256,12632256"
        RotaLink(0).MainHeader = True
        RotaLink(1).SetMainHeaderValues MainHeadCols, "AODB Departure Rotation,Route Data,Linked Flight,System Data,Appendix", "12632256,12632256,12632256,12632256"
        RotaLink(1).MainHeader = True
        RotaLink(2).SetMainHeaderValues MainHeadCols, "Import File Arrival Rotation,Route Data,Linked Flight,System Data,Appendix", "12632256,12632256,12632256,12632256"
        RotaLink(2).MainHeader = True
        RotaLink(3).SetMainHeaderValues MainHeadCols, "AODB Arrival Rotation,Route Data,Linked Flight,System Data,Appendix", "12632256,12632256,12632256,12632256"
        RotaLink(3).MainHeader = True
        RotaLink(4).SetMainHeaderValues "4,1", "Departure Flight,  ", "12632256,12632256"
        RotaLink(4).MainHeader = True
        RotaLink(5).SetMainHeaderValues "4,1", "Arrival Flight,  ", "12632256,12632256"
        RotaLink(5).MainHeader = True
    End If
End Sub

Private Sub SetLineMarkers(UseMarker As String, LineList As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim CurBackColor As Long
    Dim CurForeColor As Long
    HelperTab(1).ResetContent
    HelperTab(1).InsertBuffer LineList, ","
    MaxLine = HelperTab(1).GetLineCount - 1
    For CurLine = 0 To MaxLine
        LineNo = Val(HelperTab(1).GetColumnValue(CurLine, 0))
        NoopList(2).SetDecorationObject LineNo, 0, UseMarker & "1"
        NoopList(2).SetDecorationObject LineNo, 1, UseMarker & "2"
        NoopList(2).SetDecorationObject LineNo, 2, UseMarker & "3"
    Next
    HelperTab(1).ResetContent
End Sub

Private Sub SetLineStatus(Index As Integer, LineList As String, LineStatus As Long)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim CurBackColor As Long
    Dim CurForeColor As Long
    HelperTab(1).ResetContent
    HelperTab(1).InsertBuffer LineList, ","
    MaxLine = HelperTab(1).GetLineCount - 1
    For CurLine = 0 To MaxLine
        LineNo = Val(HelperTab(1).GetColumnValue(CurLine, 0))
        NoopList(Index).SetLineStatusValue LineNo, LineStatus
    Next
    HelperTab(1).ResetContent
End Sub
Private Sub ArrangeLayoutVert()
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim CoverTop As Long
    Dim NewTop2 As Long
    Dim CoverTop2 As Long
    Dim i As Integer
    If StartedAsCompareTool Then
        TopLabels.Top = Splitter(3).Top + 90
        TlxImpData(0).Height = Splitter(3).Top - TlxImpData(0).Top - 30
        TlxImpData(1).Height = TlxImpData(0).Height
    Else
        TopLabels.Top = Splitter(3).Top
    End If
    DspShrink.Top = -30
    NoopList(0).Top = Splitter(2).Top + Splitter(2).Height + 15
    NoopList(1).Top = NoopList(0).Top
    NoopList(2).Top = NoopList(0).Top
    Splitter(0).Top = Splitter(2).Top
    Splitter(1).Top = Splitter(2).Top
    
    If chkWork(13).Value = 1 Then
        For i = 0 To 5
            RotaLink(i).Visible = True
            ScrollCover(i).Visible = True
        Next
        RotaCover.Visible = True
    Else
        For i = 0 To 5
            RotaLink(i).Visible = False
            ScrollCover(i).Visible = False
        Next
        RotaCover.Visible = False
    End If
    NewTop = Me.ScaleHeight - StatusBar.Height - BottomLabels(0).Height - 15
    If NewTop > 150 Then
        BottomLabels(0).Top = NewTop
        BottomLabels(1).Top = NewTop
        BottomLabels(2).Top = NewTop
        NewHeight = NewTop + BottomLabels(0).Height + 30
    End If
    If chkWork(13).Value = 1 Then
        NewTop = NewTop - (RotaLink(0).Height * 2) + 8 * Screen.TwipsPerPixelY
        If NewTop > 150 Then
            RotaLink(2).Top = NewTop
            RotaLink(3).Top = NewTop
            RotaLink(5).Top = NewTop
            CoverTop = NewTop - 120
            ScrollCover(2).Top = CoverTop
            ScrollCover(3).Top = CoverTop
            ScrollCover(5).Top = CoverTop
            NewTop2 = NewTop + RotaLink(0).Height
            RotaLink(0).Top = NewTop2
            RotaLink(1).Top = NewTop2
            RotaLink(4).Top = NewTop2
            CoverTop2 = NewTop2 - 120
            ScrollCover(0).Top = CoverTop2
            ScrollCover(1).Top = CoverTop2
            ScrollCover(4).Top = CoverTop2
        End If
    Else
        NewTop = NewTop + 8 * Screen.TwipsPerPixelY
    End If
    NewHeight = NewTop - NoopList(0).Top - 1 * Screen.TwipsPerPixelY
    If NewHeight > 600 Then
        NoopList(0).Height = NewHeight
        NoopList(2).Height = NewHeight
        NoopList(1).Height = NewHeight
    End If
    NewHeight = Me.ScaleHeight - StatusBar.Height - Splitter(2).Top - 15
    If NewHeight > 100 Then
        Splitter(0).Height = NewHeight
        Splitter(1).Height = NewHeight
    End If
End Sub
Private Sub ArrangeLayoutHoriz()
    Dim i As Integer
    Dim NewSize As Long
    Dim ObjSize As Long
    Dim LeftSize As Long
    Dim RightSize As Long
    Dim NewLeft As Long
    Dim PerSize As Long
    NewSize = Me.ScaleWidth - 60
    NewLeft = NewSize - DspLimit.Width + 60
    If NewLeft < 90 Then NewLeft = 90
    DspLimit.Left = NewLeft
    TopLabels.Width = NewSize - 30
    Line1(2).X1 = NewSize - 45
    Line1(2).X2 = NewSize - 45
    NewLeft = NewSize - DspLoaded.Width - 90
    If NewLeft < 90 Then NewLeft = 90
    DspLoaded.Left = NewLeft
    TlxImpData(0).Width = NewSize - 30
    TlxImpData(1).Width = TlxImpData(0).Width
    Splitter(2).Width = NewSize - 30
    Splitter(3).Width = NewSize - 30
    NewSize = NewSize - Splitter(0).Width - Splitter(1).Width - 30
    ObjSize = NewSize - NoopList(2).Width
    PerSize = (NoopList(0).Width * 100) / (NoopList(0).Width + NoopList(1).Width)
    LeftSize = ObjSize * PerSize / 100
    RightSize = ObjSize - LeftSize
    NewLeft = NoopList(0).Left
    If LeftSize > 300 Then NoopList(0).Width = LeftSize
    NewLeft = NewLeft + LeftSize
    Splitter(0).Left = NewLeft
    NewLeft = NewLeft + Splitter(0).Width
    NoopList(2).Left = NewLeft
    NewLeft = NewLeft + NoopList(2).Width
    Splitter(1).Left = NewLeft
    NewLeft = NewLeft + Splitter(1).Width
    NoopList(1).Left = NewLeft
    If RightSize > 300 Then NoopList(1).Width = RightSize
    
    ArrangeLayoutBottom
    ArrangeLayoutRota
End Sub
Private Sub ArrangeLayoutRota()
    Dim i As Integer
    If chkWork(13).Value = 1 Then
        RotaLink(0).Left = NoopList(0).Left
        RotaLink(0).Width = NoopList(0).Width
        RotaLink(2).Left = NoopList(0).Left
        RotaLink(2).Width = NoopList(0).Width
        RotaLink(1).Left = NoopList(1).Left
        RotaLink(1).Width = NoopList(1).Width
        RotaLink(3).Left = NoopList(1).Left
        RotaLink(3).Width = NoopList(1).Width
        RotaLink(4).Left = NoopList(2).Left
        RotaLink(4).Width = NoopList(2).Width
        RotaLink(5).Left = NoopList(2).Left
        RotaLink(5).Width = NoopList(2).Width
        For i = 0 To 5
            ScrollCover(i).Left = RotaLink(i).Left + RotaLink(i).Width - ScrollCover(i).Width + 15
        Next
    End If
End Sub
Private Sub ArrangeLayoutBottom()
    Dim i As Integer
    Dim j As Integer
    Dim NewSize As Long
    Dim ObjSize As Long
    Dim ObjLeft As Long
    BottomLabels(0).Left = NoopList(0).Left
    BottomLabels(0).Width = NoopList(0).Width + 30
    NewSize = BottomLabels(0).Width - txtSearch(0).Width - 300
    ObjSize = NewSize / 7
    ObjSize = (ObjSize \ 15) * 15
    ObjLeft = CliFkeyCnt(0).Left
    If ObjSize > 180 Then
        For i = 0 To 5
            j = i + 7
            CliFkeyCnt(i).Left = ObjLeft
            CliFkeyCnt(i).Width = ObjSize
            CliFkeyCnt(j).Left = ObjLeft
            CliFkeyCnt(j).Width = ObjSize
            ObjLeft = ObjLeft + ObjSize + 15
        Next
        ObjSize = BottomLabels(0).Width - ObjLeft - 75
        CliFkeyCnt(6).Left = ObjLeft
        CliFkeyCnt(6).Width = ObjSize
        CliFkeyCnt(13).Left = ObjLeft
        CliFkeyCnt(13).Width = ObjSize
    End If
    BottomLabels(1).Left = NoopList(1).Left
    BottomLabels(1).Width = NoopList(1).Width + 30
    NewSize = BottomLabels(1).Width - txtSearch(1).Width - 300
    ObjSize = NewSize / 7
    ObjSize = (ObjSize \ 15) * 15
    ObjLeft = SrvFkeyCnt(0).Left
    If ObjSize > 180 Then
        For i = 0 To 5
            j = i + 7
            SrvFkeyCnt(i).Left = ObjLeft
            SrvFkeyCnt(i).Width = ObjSize
            SrvFkeyCnt(j).Left = ObjLeft
            SrvFkeyCnt(j).Width = ObjSize
            ObjLeft = ObjLeft + ObjSize + 15
        Next
        ObjSize = BottomLabels(1).Width - ObjLeft - 75
        SrvFkeyCnt(6).Left = ObjLeft
        SrvFkeyCnt(6).Width = ObjSize
        SrvFkeyCnt(13).Left = ObjLeft
        SrvFkeyCnt(13).Width = ObjSize
    End If
    BottomLabels(2).Left = NoopList(2).Left
    AdjustImpButtons
End Sub

Private Sub CheckInfoServer()
    On Error GoTo ErrHdl
    If (StartedAsCompareTool) And (InfoServerIsConnected = False) Then
        If InfoFromServer.LinkMode = 0 Then
            InfoFromServer.LinkTopic = "TelexPool|InfoServer"
            InfoFromServer.LinkItem = "InfoToClient"
            InfoFromServer.LinkMode = vbLinkAutomatic
        End If
        chkUpdMode.Value = 1
        chkFullMode.Enabled = False
        chkUpdMode.Enabled = False
    End If
    Exit Sub
ErrHdl:
End Sub
Private Sub InitImportTabs()
    TlxImpData(0).ResetContent
    TlxImpData(0).HeaderString = "SID,PID,TR.ID,DATA LINE,TLX.URNO"
    TlxImpData(0).LogicalFieldList = "SQID,PKID,TRID,DATA,URNO"
    TlxImpData(0).HeaderLengthString = "10,10,10,10,10"
    TlxImpData(0).ColumnWidthString = "6,6,8,1000,10"
    TlxImpData(0).ColumnAlignmentString = "R,R,L,L,L"
    TlxImpData(0).FontName = "Courier New"
    TlxImpData(0).SetMainHeaderValues "5", "Telex Import Spooler", "12632256"
    TlxImpData(0).SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    TlxImpData(0).MainHeader = True
    TlxImpData(0).FontSize = 17
    TlxImpData(0).LineHeight = 17
    TlxImpData(0).HeaderFontSize = 17
    TlxImpData(0).SetTabFontBold True
    TlxImpData(0).ShowHorzScroller True
    TlxImpData(0).ShowVertScroller True
    TlxImpData(0).LifeStyle = True
    TlxImpData(0).SetFieldSeparator Chr(15)
    TlxImpData(0).SetUniqueFields "2"
    TlxImpData(0).AutoSizeByHeader = True
    TlxImpData(0).AutoSizeColumns
    InitTlxImpLayout "INIT"
End Sub
Private Sub InitTlxImpLayout(ForWhat As String)
    'Set Global Layout Variables
    Select Case ForWhat
        Case "INIT"
            TlxImpData(1).ResetContent
            MainFieldList = "TRID,LTID,FLAG,SEAS,HOPO,FLCA,FLTA,FLCD,FLTD,VPFR,VPTO,FREQ,NOSE,ACT3,ACT5,ORG3,VSA3,ORG4,VSA4,STOA,STOD,DAOF,VSD3,DES3,VSD4,DES4,NATA,NATD,FREW,URNO"
            MainExpandHeader = "TR.ID,LT,S,SSC,APC,FCA,FLTA,FCD,FLTD,P.BEGIN,P.END,OP.DAYS,CAP,ACT,ICAO,ORG,VIA,ORG4,VIA4,STOA,STOD,O,VIA,DST,VIA4,DST4,A,D,W,TLX.URNO"
            MainFldWidLst = "10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10"
            MainColWidLst = "8,1,1,3,3,3,5,3,5,8,8,7,3,3,4,3,3,4,4,4,4,1,3,3,4,4,1,1,1,10"
            TlxImpData(1).HeaderString = MainExpandHeader
            TlxImpData(1).LogicalFieldList = MainFieldList
            TlxImpData(1).HeaderLengthString = MainFldWidLst
            TlxImpData(1).ColumnWidthString = MainColWidLst
            TlxImpData(1).ColumnAlignmentString = "R,R,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L"
            TlxImpData(1).FontName = "Courier New"
            MainHeaderWidth = "5,4,3,3,5,6,2,1,1"
            MainHeaderTitle = "System,Flights,Period,Aircraft,Arrival,Departure,SV,F,TLXTAB"
            MainHeaderColor = "12632256,12632256,12632256,12632256,12632256,12632256,12632256,12632256,12632256"
            TlxImpData(1).SetMainHeaderValues MainHeaderWidth, MainHeaderTitle, MainHeaderColor
            
            'TlxImpData(1).SetMainHeaderValues "5,4,3,3,5,6,2,1,1", "System,Flights,Period,Aircraft,Arrival,Departure,SV,F,TLXTAB", "12632256,12632256,12632256,12632256,12632256,12632256,12632256,12632256,12632256"
            TlxImpData(1).SetMainHeaderFont 17, False, False, True, 0, "Courier New"
            TlxImpData(1).MainHeader = True
            TlxImpData(1).FontSize = 17
            TlxImpData(1).LineHeight = 17
            TlxImpData(1).HeaderFontSize = 17
            TlxImpData(1).SetTabFontBold True
            TlxImpData(1).ShowHorzScroller True
            TlxImpData(1).ShowVertScroller True
            TlxImpData(1).DateTimeSetColumn 9
            TlxImpData(1).DateTimeSetInputFormatString 9, "YYYYMMDD"
            TlxImpData(1).DateTimeSetOutputFormatString 9, "DDMMMYY"
            TlxImpData(1).DateTimeSetColumn 10
            TlxImpData(1).DateTimeSetInputFormatString 10, "YYYYMMDD"
            TlxImpData(1).DateTimeSetOutputFormatString 10, "DDMMMYY"
            TlxImpData(1).CreateDecorationObject "CellError", "R,T,B,L", "2,2,2,2", Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray)
            TlxImpData(1).LifeStyle = True
            TlxImpData(1).SetUniqueFields "0"
            TlxImpData(1).AutoSizeByHeader = True
            TlxImpData(1).AutoSizeColumns
        Case "MODIFY"
            TlxImpData(1).ResetContent
            TlxImpData(1).HeaderString = MainExpandHeader
            TlxImpData(1).LogicalFieldList = MainFieldList
            TlxImpData(1).HeaderLengthString = MainFldWidLst
            TlxImpData(1).ColumnWidthString = MainColWidLst
            TlxImpData(1).SetMainHeaderValues MainHeaderWidth, MainHeaderTitle, MainHeaderColor
            ApdxIsMerged = True
        Case Else
    End Select
    myViewFieldList = MainFieldList
End Sub
Private Sub CreateTlxImpRecords()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ImpData As String
    Dim NewRec As String
    Dim tmpData As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpApc As String
    Dim tmpVia As String
    Dim tmpUrno As String
    Dim tmpPos As String
    Dim tmpLen As String
    Dim AddOnLength As Integer
    
    Dim DatPos As Integer
    Dim DatLen As Integer
    Dim itm As Integer
    Dim IsValid As Boolean
    MaxLine = TlxImpData(0).GetLineCount - 1
    If MaxLine >= 0 Then
        MinImpVpfr = "99999999"
        MaxImpVpto = "00000000"
        NewRec = TlxImpData(0).GetLineValues(0)
        If InStr(NewRec, "[APPEND]") = 0 Then
            If DefaultFileHeader <> "" Then
                NewRec = Replace(DefaultFileHeader, "[APPEND]", "", 1, -1, vbBinaryCompare)
                'Case "[APPEND]"
                AddOnLength = GetAddOnLength("[APPEND]", NewRec)
                If (AddOnDataFound) And (Not ApdxIsMerged) And (Not AddOnDataMerge) Then
                    ModifyTabLayout 2
                    InitTlxImpLayout "MODIFY"
                End If
                'TlxImpData(0).InsertTextLineAt 0, DefaultFileHeader, False
            End If
        End If
    End If
    MaxLine = TlxImpData(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpUrno = TlxImpData(0).GetColumnValue(CurLine, 4)
        ImpData = TlxImpData(0).GetColumnValue(CurLine, 3)
        NewRec = ""
        NewRec = NewRec & Trim(Mid(ImpData, 1, 8)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 9, 1)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 10, 1)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 11, 3)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 14, 3)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 17, 3)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 20, 5)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 25, 3)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 28, 5)) & ","
        tmpVpfr = Mid(ImpData, 33, 8)
        tmpVpto = Mid(ImpData, 41, 8)
        tmpVpfr = CheckDateFieldFormat(-1, tmpVpfr, "", "", False)
        tmpVpto = CheckDateFieldFormat(-1, tmpVpto, "", "", False)
        If (tmpVpfr < MinImpVpfr) And (Trim(tmpVpfr) <> "") Then MinImpVpfr = tmpVpfr
        NewRec = NewRec & Trim(tmpVpfr) & ","
        If tmpVpto > MaxImpVpto Then MaxImpVpto = tmpVpto
        NewRec = NewRec & Trim(tmpVpto) & ","
        
        tmpData = Trim(Mid(ImpData, 49, 7))
        tmpData = FormatFrequency(tmpData, "123", IsValid)
        NewRec = NewRec & tmpData & ","
        NewRec = NewRec & Trim(Mid(ImpData, 56, 3)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 59, 3)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 62, 4)) & ","
        tmpApc = Trim(Mid(ImpData, 66, 3))
        tmpVia = Trim(Mid(ImpData, 69, 3))
        If tmpVia = tmpApc Then tmpVia = ""
        NewRec = NewRec & tmpApc & ","
        NewRec = NewRec & tmpVia & ","
        tmpApc = Trim(Mid(ImpData, 72, 4))
        tmpVia = Trim(Mid(ImpData, 76, 4))
        If tmpVia = tmpApc Then tmpVia = ""
        NewRec = NewRec & tmpApc & ","
        NewRec = NewRec & tmpVia & ","
        NewRec = NewRec & Trim(Mid(ImpData, 80, 4)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 84, 4)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 88, 1)) & ","
        tmpVia = Trim(Mid(ImpData, 89, 3))
        tmpApc = Trim(Mid(ImpData, 92, 3))
        If tmpVia = tmpApc Then tmpVia = ""
        NewRec = NewRec & tmpVia & ","
        NewRec = NewRec & tmpApc & ","
        tmpVia = Trim(Mid(ImpData, 95, 4))
        tmpApc = Trim(Mid(ImpData, 99, 4))
        If tmpVia = tmpApc Then tmpVia = ""
        NewRec = NewRec & tmpVia & ","
        NewRec = NewRec & tmpApc & ","
        NewRec = NewRec & Trim(Mid(ImpData, 103, 1)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 104, 1)) & ","
        NewRec = NewRec & Trim(Mid(ImpData, 105, 1))
        If (ApdxIsMerged) Or (AddOnDataMerge) Then
            'AddOnMainTitle = ""  'MAIN_HEADER_TITLE = Title of MainHeader
            'AddOnMainWidth = ""  'MAIN_HEADER_WIDTH = MainHeader Groups
            'AddOnMainFields = "" 'MAIN_FIELDS = Logical Fields of the Grid
            'AddOnFinalHead = ""  'FINAL_HEAD = Grid Column Titles
            'AddOnFinalLen = ""   'FINAL_LEN = List of Grid Column Field Length
            'AddOnFinalPos = ""   'FINAL_POS = List of Grid Column Field Pos in TextLine
            'AddOnGridSize = ""   'Internal HeaderLengthString
            'AddOnFinalLen = AddOnFinalLen
            'AddOnFinalPos = AddOnFinalPos
            itm = 0
            tmpPos = "START"
            While tmpPos <> ""
                itm = itm + 1
                tmpPos = GetItem(AddOnFinalPos, itm, ",")
                tmpLen = GetItem(AddOnFinalLen, itm, ",")
                If tmpPos <> "" Then
                    DatPos = Val(tmpPos)
                    DatLen = Val(tmpLen)
                    tmpData = Mid(ImpData, DatPos, DatLen)
                    NewRec = NewRec & "," & tmpData
                End If
            Wend
            
            
        End If
        NewRec = NewRec & "," & tmpUrno
        TlxImpData(1).InsertTextLine NewRec, False
    Next
    TlxImpData(0).ResetContent
    TlxImpData(0).SetUniqueFields "2"
    TlxImpData(0).Refresh
    TlxImpData(1).AutoSizeColumns
    TlxImpData(1).Refresh
End Sub
Public Sub MsgToTelexPool(MyMsg As String)
    Static CurMsgNum As Long
    Dim tmpMsg As String
    Dim tmpData As String
    On Error GoTo ErrHdl
    If TelexIsConnected Then
        CurMsgNum = CurMsgNum + 1
        tmpMsg = CStr(CurMsgNum) & "," & MyMsg
        tmpMsg = Replace(tmpMsg, ",", Chr(15), 1, -1, vbBinaryCompare)
        InfoSpooler.InsertTextLine tmpMsg, False
        InfoSpooler.AutoSizeColumns
        InfoSpooler.Refresh
        SpoolTimer.Enabled = True
    End If
    Exit Sub
ErrHdl:
End Sub

Private Sub SpoolTimer_Timer()
    Dim tmpMsg As String
    If (TelexIsConnected) And (MsgFromServer.Text <> "WAIT") Then
        SpoolTimer.Enabled = False
        tmpMsg = InfoSpooler.GetColumnValue(0, 0)
        InfoSpooler.DeleteLine 0
        InfoSpooler.Refresh
        MsgFromServer = "WAIT"
        MsgToServer.Text = tmpMsg
        If InfoSpooler.GetLineCount > 0 Then SpoolTimer.Enabled = True
    End If
End Sub


