VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form RecFilter 
   Caption         =   "Record Filter Preset"
   ClientHeight    =   4740
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9270
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   11.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "RecFilter.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4740
   ScaleWidth      =   9270
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraDispoButtonPanel 
      BorderStyle     =   0  'None
      Height          =   405
      Left            =   2610
      TabIndex        =   59
      Top             =   150
      Visible         =   0   'False
      Width           =   7005
      Begin VB.CheckBox Check1 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3870
         Style           =   1  'Graphical
         TabIndex        =   63
         Top             =   0
         Width           =   855
      End
      Begin TABLib.TAB NatTabCombo 
         Height          =   315
         Index           =   0
         Left            =   0
         TabIndex        =   62
         Top             =   0
         Visible         =   0   'False
         Width           =   3855
         _Version        =   65536
         _ExtentX        =   6800
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin VB.CheckBox chkSetDispo 
         Caption         =   "Perform"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4740
         Style           =   1  'Graphical
         TabIndex        =   61
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5610
         Style           =   1  'Graphical
         TabIndex        =   60
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame fraFilterButtonPanel 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   30
      TabIndex        =   53
      Top             =   30
      Visible         =   0   'False
      Width           =   4365
      Begin VB.CheckBox chkTool 
         Caption         =   "Adjust"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   2610
         Style           =   1  'Graphical
         TabIndex        =   58
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   3480
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Refresh"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   56
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Load"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   55
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Compare"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   870
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame fraFredButtonPanel 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   30
      TabIndex        =   36
      Top             =   360
      Visible         =   0   'False
      Width           =   3585
      Begin VB.CheckBox chkWkDay 
         Caption         =   "All"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   45
         ToolTipText     =   "Select all frequency days"
         Top             =   0
         Width           =   765
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   2400
         Style           =   1  'Graphical
         TabIndex        =   44
         ToolTipText     =   "Frequency filter for Sunday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   2130
         Style           =   1  'Graphical
         TabIndex        =   43
         ToolTipText     =   "Frequency filter for Saturday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   42
         ToolTipText     =   "Frequency filter for Friday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   1590
         Style           =   1  'Graphical
         TabIndex        =   41
         ToolTipText     =   "Frequency filter for Thursday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   40
         ToolTipText     =   "Frequency filter for Wednesday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   39
         ToolTipText     =   "Frequency filter for Thuesday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   38
         ToolTipText     =   "Frequency filter for Monday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "None"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   2670
         Style           =   1  'Graphical
         TabIndex        =   37
         Tag             =   "......."
         ToolTipText     =   "Unselect all frequency days"
         Top             =   0
         Width           =   795
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   18
      Top             =   4455
      Width           =   9270
      _ExtentX        =   16351
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   5
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1632
            MinWidth        =   1632
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1623
            MinWidth        =   1623
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   8387
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2311
            MinWidth        =   2311
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1764
            MinWidth        =   1764
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Registration"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   9450
      Style           =   1  'Graphical
      TabIndex        =   17
      ToolTipText     =   "Activates/De-Activates the filter "
      Top             =   750
      Width           =   1185
   End
   Begin VB.Frame FilterPanel 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3795
      Index           =   4
      Left            =   9390
      TabIndex        =   16
      Top             =   600
      Width           =   2505
      Begin VB.TextBox txtFilterVal 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   1260
         TabIndex        =   50
         Top             =   150
         Width           =   1185
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   8
         Left            =   60
         TabIndex        =   8
         Top             =   480
         Width           =   1185
         _Version        =   65536
         _ExtentX        =   2090
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   9
         Left            =   1260
         TabIndex        =   9
         Top             =   480
         Width           =   1185
         _Version        =   65536
         _ExtentX        =   2090
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin VB.Frame ButtonPanel 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   60
         TabIndex        =   23
         Top             =   3450
         Width           =   2385
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   9
            Left            =   1200
            Style           =   1  'Graphical
            TabIndex        =   33
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   8
            Left            =   330
            Style           =   1  'Graphical
            TabIndex        =   32
            Top             =   0
            Width           =   855
         End
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Aircraft"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   7440
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Activates/De-Activates the filter "
      Top             =   750
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Airport"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   5430
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Activates/De-Activates the filter "
      Top             =   750
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Flight"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   2100
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Activates/De-Activates the filter "
      Top             =   750
      Width           =   1515
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Airline"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   90
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Activates/De-Activates the filter "
      Top             =   750
      Width           =   855
   End
   Begin VB.Frame FilterPanel 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3795
      Index           =   3
      Left            =   7380
      TabIndex        =   14
      Top             =   600
      Width           =   1845
      Begin VB.TextBox txtFilterVal 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   930
         TabIndex        =   49
         Top             =   150
         Width           =   855
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   7
         Left            =   930
         TabIndex        =   15
         Top             =   480
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin VB.Frame ButtonPanel 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   60
         TabIndex        =   22
         Top             =   3450
         Width           =   1725
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   7
            Left            =   870
            Style           =   1  'Graphical
            TabIndex        =   31
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   6
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   30
            Top             =   0
            Width           =   855
         End
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   6
         Left            =   60
         TabIndex        =   35
         Top             =   480
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
   End
   Begin VB.Frame FilterPanel 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3795
      Index           =   2
      Left            =   5370
      TabIndex        =   12
      Top             =   600
      Width           =   1845
      Begin VB.TextBox txtFilterVal 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   930
         TabIndex        =   48
         Top             =   150
         Width           =   855
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   4
         Left            =   60
         TabIndex        =   7
         Top             =   480
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   5
         Left            =   930
         TabIndex        =   13
         Top             =   480
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin VB.Frame ButtonPanel 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   60
         TabIndex        =   21
         Top             =   3450
         Width           =   1725
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   5
            Left            =   870
            Style           =   1  'Graphical
            TabIndex        =   29
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   28
            Top             =   0
            Width           =   855
         End
      End
   End
   Begin VB.Frame FilterPanel 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3795
      Index           =   1
      Left            =   2040
      TabIndex        =   11
      Top             =   600
      Width           =   3165
      Begin VB.TextBox txtFilterVal 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1590
         TabIndex        =   47
         Top             =   150
         Width           =   1515
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   2
         Left            =   60
         TabIndex        =   5
         Top             =   480
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   3
         Left            =   1590
         TabIndex        =   6
         Top             =   480
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin VB.Frame ButtonPanel 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   60
         TabIndex        =   20
         Top             =   3450
         Width           =   2385
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   1320
            Style           =   1  'Graphical
            TabIndex        =   27
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   450
            Style           =   1  'Graphical
            TabIndex        =   26
            Top             =   0
            Width           =   855
         End
      End
   End
   Begin VB.Frame FilterPanel 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3795
      Index           =   0
      Left            =   30
      TabIndex        =   10
      Top             =   600
      Width           =   1845
      Begin VB.TextBox txtFilterVal 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   930
         TabIndex        =   46
         Top             =   150
         Width           =   855
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   0
         Left            =   60
         TabIndex        =   4
         Top             =   480
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin VB.Frame ButtonPanel 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   60
         TabIndex        =   19
         Top             =   3450
         Width           =   1725
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   870
            Style           =   1  'Graphical
            TabIndex        =   25
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   24
            Top             =   0
            Width           =   855
         End
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   1
         Left            =   930
         TabIndex        =   34
         Top             =   480
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
   End
   Begin TABLib.TAB CrossFilter 
      Height          =   3345
      Index           =   0
      Left            =   6750
      TabIndex        =   51
      Top             =   30
      Visible         =   0   'False
      Width           =   1185
      _Version        =   65536
      _ExtentX        =   2090
      _ExtentY        =   5900
      _StockProps     =   64
   End
   Begin TABLib.TAB CrossFilter 
      Height          =   3345
      Index           =   1
      Left            =   7470
      TabIndex        =   52
      Top             =   120
      Visible         =   0   'False
      Width           =   1185
      _Version        =   65536
      _ExtentX        =   2090
      _ExtentY        =   5900
      _StockProps     =   64
   End
End
Attribute VB_Name = "RecFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim FilterItems(4) As String
Dim CheckItems(4) As String
Dim LastLoadFilter As String
Dim MyFilterCaption As String

Public Sub ArrangeFormLayout()
    If FilterCalledAsDispo Then
        fraFilterButtonPanel.Visible = False
        fraDispoButtonPanel.Left = fraFilterButtonPanel.Left
        fraDispoButtonPanel.Visible = True
        fraFredButtonPanel.Visible = False
        InitNatureCombo
        Me.Caption = "Flight Nature Disposition"
        Me.Left = MainDialog.Left + MainDialog.Width / 2 - Me.Width / 2
        Me.Top = MainDialog.Top + MainDialog.Height / 2 - Me.Height / 2
    Else
        fraFilterButtonPanel.Visible = True
        fraDispoButtonPanel.Visible = False
        fraFredButtonPanel.Visible = True
        Me.Caption = MyFilterCaption
        Me.Top = FlightExtract.Top + FlightExtract.Height - FlightExtract.StatusBar.Height - Me.Height - 22 * Screen.TwipsPerPixelY
        Me.Left = FlightExtract.Left + FlightExtract.Width / 2 - Me.Width / 2
    End If
End Sub

Private Sub InitNatureCombo()
    Static ComboFilled As Boolean
    Dim LineNo As Long
    Dim MaxLine As Long
    Dim tmpTest As String
    If Not ComboFilled Then
        NatTabCombo(0).ResetContent
        NatTabCombo(0).ShowVertScroller False
        NatTabCombo(0).ShowHorzScroller False
        NatTabCombo(0).VScrollMaster = False
        NatTabCombo(0).FontName = "Courier New"
        NatTabCombo(0).SetTabFontBold True
        NatTabCombo(0).HeaderFontSize = 17
        NatTabCombo(0).FontSize = 17
        NatTabCombo(0).LineHeight = 19
        NatTabCombo(0).GridLineColor = vbBlack
        NatTabCombo(0).HeaderLengthString = Str(NatTabCombo(0).Width / Screen.TwipsPerPixelX)
        NatTabCombo(0).HeaderString = "TYP"
        NatTabCombo(0).ColumnAlignmentString = "L"
        NatTabCombo(0).ColumnWidthString = "500"
        NatTabCombo(0).Height = 1 * NatTabCombo(0).LineHeight * Screen.TwipsPerPixelY
        NatTabCombo(0).MainHeaderOnly = True
        NatTabCombo(0).InsertTextLine ",,", False
        NatTabCombo(0).SetCurrentSelection 0
        NatTabCombo(0).CreateComboObject "NatureCodes", 0, 2, "EDIT", 400
        NatTabCombo(0).SetComboColumn "NatureCodes", 0
        NatTabCombo(0).ComboMinWidth = 30
        NatTabCombo(0).EnableInlineEdit True
        NatTabCombo(0).NoFocusColumns = "0"
        NatTabCombo(0).ComboSetColumnLengthString "NatureCodes", "32,350"
        tmpTest = ",,"
        NatTabCombo(0).ComboAddTextLines "NatureCodes", tmpTest, vbLf
        NatTabCombo(0).Visible = True
        NatTabCombo(0).RedrawTab
        NatTabCombo(0).ComboResetContent "NatureCodes"
        MaxLine = UfisServer.BasicData(1).GetLineCount - 1
        For LineNo = 0 To MaxLine
            tmpTest = UfisServer.BasicData(1).GetLineValues(LineNo)
            NatTabCombo(0).ComboAddTextLines "NatureCodes", tmpTest, vbLf
        Next
        NatTabCombo(0).SetCurrentSelection 0
        NatTabCombo(0).RedrawTab
        ComboFilled = True
    End If
End Sub
Private Sub InitFilterConfig()
    Dim i As Integer
    Dim KeyCode As String
    Dim FilterCfg As String
    Dim CurTab As Integer
    Dim ItemList As String
    For i = 0 To 4
        If DataSystemType <> "FREE" Then
            KeyCode = "FILTER" & Trim(Str(i + 1))
            FilterCfg = GetIniEntry(myIniFullName, myIniSection, "", KeyCode, "")
            If NewLayoutType Then FilterCfg = TranslateFilter(FilterCfg)
        Else
            FilterCfg = ""
            Select Case i
                Case 0  'ALC
                    FilterCfg = MainDialog.BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(19), 2, 25, ",")
                    FilterCfg = Replace(FilterCfg, ":,", ":", 1, -1, vbBinaryCompare)
                    FilterCfg = Replace(FilterCfg, ",", ":", 1, -1, vbBinaryCompare)
                Case 1  'FLNO
                    FilterCfg = MainDialog.BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(20), 2, 25, ",")
                    FilterCfg = Replace(FilterCfg, ":,", ":", 1, -1, vbBinaryCompare)
                    FilterCfg = Replace(FilterCfg, ",:", ":", 1, -1, vbBinaryCompare)
                    FilterCfg = Replace(FilterCfg, "+,", "+", 1, -1, vbBinaryCompare)
                Case 2  'APC
                    FilterCfg = MainDialog.BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(21), 2, 25, ",")
                    FilterCfg = Replace(FilterCfg, ":,", ":", 1, -1, vbBinaryCompare)
                    FilterCfg = Replace(FilterCfg, ",:", ":", 1, -1, vbBinaryCompare)
                Case 3 'ACT
                    FilterCfg = MainDialog.BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(22), 2, 25, ",")
                    FilterCfg = Replace(FilterCfg, ":,", ":", 1, -1, vbBinaryCompare)
                    FilterCfg = Replace(FilterCfg, ",", ":", 1, -1, vbBinaryCompare)
                Case 4
                Case Else
            End Select
        End If
        chkWork(i).Caption = GetItem(FilterCfg, 1, ":")
        CurTab = i * 2
        FilterTab(CurTab).SetHeaderText GetItem(FilterCfg, 2, ":")
        FilterTab(CurTab).Tag = GetItem(FilterCfg, 2, ":")
        FilterTab(CurTab).ResetContent
        FilterTab(CurTab + 1).SetHeaderText "Filter"
        FilterTab(CurTab + 1).ResetContent
        FilterTab(CurTab + 1).Tag = GetItem(FilterCfg, 2, ":")
        'we must increase all coloumn numbers by 2
        ItemList = GetItem(FilterCfg, 3, ":")
        FilterItems(i) = ShiftItemValues(ItemList, 2)
        ItemList = GetItem(FilterCfg, 4, ":")
        If ItemList <> "" Then
            CheckItems(i) = ShiftItemValues(ItemList, 2)
        Else
            CheckItems(i) = FilterItems(i)
        End If
    Next
End Sub
Private Function TranslateFilter(CurFilterCfg As String) As String
    Dim Result As String
    Dim CurItemList As String
    Dim MainItems As String
    Dim ViewItems As String
    Result = ""
    If CurFilterCfg <> "" Then
        Result = Result & GetItem(CurFilterCfg, 1, ":") & ":"
        Result = Result & GetItem(CurFilterCfg, 2, ":") & ":"
        CurItemList = GetItem(CurFilterCfg, 3, ":")
        MainItems = TranslateItemList(CurItemList, MainFieldList, False)
        CurItemList = GetItem(CurFilterCfg, 4, ":")
        If CurItemList = "" Then
            ViewItems = MainItems
        Else
            ViewItems = TranslateItemList(CurItemList, myViewFieldList, False)
        End If
        Result = Result & MainItems & ":" & ViewItems
    End If
    TranslateFilter = Result
End Function
Private Function ShiftItemValues(ItemList As String, ShiftVal As Integer) As String
    Dim Result As String
    Dim CurItm As Integer
    Dim ColItm As Integer
    Dim ColNbr As Integer
    Dim tmpItm As String
    Dim tmpCol As String
    Dim NewItm As String
    Dim tmpLen As Integer
    Result = ""
    CurItm = 1
    Do
        'separate items
        tmpItm = GetItem(ItemList, CurItm, ",")
        If tmpItm <> "" Then
            NewItm = ""
            ColItm = 1
            Do
                'separate coloumns
                tmpCol = GetItem(tmpItm, ColItm, "+")
                If tmpCol <> "" Then
                    ColNbr = Val(tmpCol) + ShiftVal
                    NewItm = NewItm & Trim(Str(ColNbr)) & "+"
                End If
                ColItm = ColItm + 1
            Loop While tmpCol <> ""
            If ColItm > 2 Then NewItm = Left(NewItm, Len(NewItm) - 1)
            Result = Result & NewItm & ","
        End If
        CurItm = CurItm + 1
    Loop While tmpItm <> ""
    If CurItm > 2 Then Result = Left(Result, Len(Result) - 1)
    ShiftItemValues = Result
End Function

Private Sub chkClose_Click()
    If chkClose.Value = 1 Then
        Me.Hide
        chkClose.Value = 0
    End If
End Sub

Private Sub chkFilterSet_Click(Index As Integer)
Dim FromCol As Integer
Dim ToCol As Integer
    If chkFilterSet(Index).Value = 1 Then
        FromCol = Index
        If FromCol Mod 2 = 0 Then
            ToCol = FromCol + 1
        Else
            ToCol = FromCol - 1
        End If
        ShiftAllData FromCol, ToCol
        chkFilterSet(Index).Value = 0
    End If
End Sub

Private Sub ShiftAllData(FromCol As Integer, ToCol As Integer)
    Dim ColTxt As String
    MousePointer = 11
    ColTxt = FilterTab(FromCol).SelectDistinct("0", "", "", vbLf, False)
    FilterTab(ToCol).InsertBuffer ColTxt, vbLf
    FilterTab(ToCol).Sort "0", True, True
    FilterTab(ToCol).RedrawTab
    FilterTab(FromCol).ResetContent
    FilterTab(FromCol).RedrawTab
    SetFilterCount FromCol
    SetFilterCount ToCol
    MousePointer = 0
    CheckRecLoadFilter False
End Sub
Private Sub SetFilterCount(ColIdx As Integer)
    Dim tmpCount As Long
    Dim MaxLin As Long
    tmpCount = FilterTab(ColIdx).GetLineCount
    chkFilterSet(ColIdx).Caption = Trim(Str(tmpCount))
    If FilterTab(ColIdx).GetVScrollPos = 0 Then
        MaxLin = FilterTab(ColIdx).Height / (FilterTab(ColIdx).LineHeight * Screen.TwipsPerPixelY) - 1
        If tmpCount > MaxLin Then
            FilterTab(ColIdx).ShowVertScroller True
        Else
            FilterTab(ColIdx).ShowVertScroller False
        End If
    End If
End Sub
Private Sub ShiftData(FromCol As Integer, ToCol As Integer, ActSelLine As Long)
Dim SelLine As Long
Dim TgtLine As Long
Dim LineData As String
Dim NewData As String
Dim ItmDat As String
    SelLine = ActSelLine
    If SelLine < 0 Then SelLine = FilterTab(FromCol).GetCurrentSelected
    If SelLine >= 0 Then
        LineData = FilterTab(FromCol).GetLineValues(SelLine)
        ItmDat = GetItem(LineData, 1, ",")
        If ItmDat <> "" Then
            InsertFilterValue LineData, ToCol
            FilterTab(FromCol).DeleteLine SelLine
        End If
        SetFilterCount FromCol
        SetFilterCount ToCol
        FilterTab(FromCol).RedrawTab
        FilterTab(ToCol).RedrawTab
    Else
        MyMsgBox.InfoApi 0, "Click on the desired (valid) line and try it again.", "More info"
        If MyMsgBox.CallAskUser(0, 0, 0, "Action Control", "No data selected", "hand", "", UserAnswer) >= 0 Then DoNothing
    End If
End Sub

Private Sub chkSetDispo_Click()
    If chkSetDispo.Value = 1 Then
        chkSetDispo.BackColor = LightGreen
        Screen.MousePointer = 11
        PerformNatureDispo
        chkSetDispo.Value = 0
        Screen.MousePointer = 0
    Else
        chkSetDispo.BackColor = vbButtonFace
    End If
End Sub
Private Sub PerformNatureDispo()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim TtypColFound As Boolean
    Dim TtypColNo As Long
    Dim NataColNo As Long
    Dim NatdColNo As Long
    Dim LookTwice As Boolean
    Dim IsValidLine As Boolean
    Dim tmpTtyp As String
    Dim FredFilter As String
    Dim tmpFldList As String
    Dim tmpRec As String
    Dim tmpFltRec As String
    tmpTtyp = NatTabCombo(0).GetColumnValue(0, 0)
    TtypColFound = False
    TtypColNo = CLng(GetRealItemNo(MainFieldList, "TTYP") + 3)
    If TtypColNo >= 3 Then
        LookTwice = False
        TtypColFound = True
    Else
        NataColNo = CLng(GetRealItemNo(MainFieldList, "NATA") + 3)
        NatdColNo = CLng(GetRealItemNo(MainFieldList, "NATD") + 3)
        If (NataColNo >= 3) And (NatdColNo >= 3) Then
            LookTwice = True
            TtypColFound = True
        End If
    End If
    If (TtypColFound) And (tmpTtyp <> "") Then
        FredFilter = chkWkDay(0).Tag
        If FredFilter = "......." Then FredFilter = ""
        MaxLine = MainDialog.FileData(CurFdIdx).GetLineCount - 1
        For CurLine = 0 To MaxLine
            tmpRec = MainDialog.FileData(CurFdIdx).GetLineValues(CurLine)
            If Not LookTwice Then
                IsValidLine = True
                IsValidLine = CheckFilterKeys(tmpRec, IsValidLine)
                'IsValidLine = MainDialog.CheckFlightPeriod(Spfr, Spto, RecData, IsValidLine, True, True, FredFilter)
                If IsValidLine Then MainDialog.FileData(CurFdIdx).SetColumnValue CurLine, TtypColNo, tmpTtyp
            Else
                tmpFldList = MainFieldList
                tmpFldList = Replace(tmpFldList, "FLCD", "....", 1, -1, vbBinaryCompare)
                tmpFldList = Replace(tmpFldList, "FLTD", "....", 1, -1, vbBinaryCompare)
                tmpFltRec = TranslateRecordData(tmpRec, MainFieldList, tmpFldList)
                IsValidLine = True
                IsValidLine = CheckFilterKeys(tmpFltRec, IsValidLine)
                If IsValidLine Then MainDialog.FileData(CurFdIdx).SetColumnValue CurLine, NataColNo, tmpTtyp
                tmpFldList = MainFieldList
                tmpFldList = Replace(tmpFldList, "FLCA", "....", 1, -1, vbBinaryCompare)
                tmpFldList = Replace(tmpFldList, "FLTA", "....", 1, -1, vbBinaryCompare)
                tmpFltRec = TranslateRecordData(tmpRec, MainFieldList, tmpFldList)
                IsValidLine = True
                IsValidLine = CheckFilterKeys(tmpFltRec, IsValidLine)
                If IsValidLine Then MainDialog.FileData(CurFdIdx).SetColumnValue CurLine, NatdColNo, tmpTtyp
            End If
        Next
        MainDialog.FileData(CurFdIdx).AutoSizeColumns
        MainDialog.FileData(CurFdIdx).Refresh
    Else
    End If
End Sub
Private Sub chkTool_Click(Index As Integer)
    If chkTool(Index).Value = 0 Then chkTool(Index).BackColor = vbButtonFace
    Select Case Index
        Case 0
            'Close
            If chkTool(Index).Value = 1 Then
                Me.Tag = ""
                Me.Hide
                If Not FilterCalledAsDispo Then FlightExtract.Show
                chkTool(Index).Value = 0
            End If
        Case 1
            'Refresh
            If chkTool(Index).Value = 1 Then
                InitLists
                chkTool(Index).Value = 0
            End If
        Case 2
            'load
            If chkTool(Index).Value = 1 Then
                If Not FilterCalledAsDispo Then
                    CheckRecLoadFilter True
                    FlightExtract.chkWork(4).Value = 1
                    If Me.Visible = True Then Me.Refresh
                End If
            End If
        Case 3
            'Adjust
        Case 4
            'Compare NOOP
            If Not FilterCalledAsDispo Then
                If chkTool(Index).Value = 1 Then
                    FlightExtract.chkWork(13).Value = 1
                Else
                    FlightExtract.chkWork(13).Value = 0
                End If
            End If
        Case Else
    End Select
    If chkTool(Index).Value = 1 Then chkTool(Index).BackColor = LightestGreen
End Sub

Private Sub BuildFilters()
    Dim tmpType As String
    Dim ColTxt As String
    Dim TgtCol As Integer
    Dim ItmNbr As Integer
    Dim CurCol As String
    Dim i As Integer
    MousePointer = 11
    For i = 0 To 4
        UfisTools.BlackBox(0).ResetContent
        UfisTools.BlackBox(0).ColumnWidthString = "100"
        TgtCol = i * 2
        FilterTab(TgtCol).ResetContent
        FilterTab(TgtCol + 1).ResetContent
        ItmNbr = 1
        Do
            CurCol = GetItem(FilterItems(i), ItmNbr, ",")
            If CurCol <> "" Then
                CurCol = Replace(CurCol, "+", ",", 1, -1, vbBinaryCompare)
                ColTxt = MainDialog.FileData(CurFdIdx).SelectDistinct(CurCol, "", "", vbLf, True)
                UfisTools.BlackBox(0).InsertBuffer ColTxt, vbLf
            End If
            ItmNbr = ItmNbr + 1
        Loop While CurCol <> ""
        ColTxt = UfisTools.BlackBox(0).SelectDistinct("0", "", "", vbLf, True)
        UfisTools.BlackBox(0).ResetContent
        ColTxt = Replace(ColTxt, (vbLf & vbLf), vbLf, 1, -1, vbBinaryCompare)
        If Left(ColTxt, 1) = vbLf Then ColTxt = Mid(ColTxt, 2)
        If Right(ColTxt, 1) = vbLf Then
            ColTxt = Left(ColTxt, Len(ColTxt) - 1)
        End If
        tmpType = FilterTab(TgtCol).Tag
        Select Case tmpType
            Case "ROUTE"
                ExtractMultiItems ColTxt
            Case Else
        End Select
        FilterTab(TgtCol).InsertBuffer ColTxt, vbLf
        FilterTab(TgtCol).Sort "0", True, True
        FilterTab(TgtCol).RedrawTab
        SetFilterCount TgtCol
    Next
    MousePointer = 0
End Sub
Private Sub ExtractMultiItems(ItemList As String)
    Dim ItemText As String
    Dim CurLine As Long
    Dim MaxLine As Long
    CrossFilter(0).ResetContent
    CrossFilter(1).ResetContent
    CrossFilter(0).ColumnWidthString = "100"
    CrossFilter(1).ColumnWidthString = "100"
    CrossFilter(1).InsertBuffer ItemList, vbLf
    MaxLine = CrossFilter(1).GetLineCount - 1
    For CurLine = 0 To MaxLine
        ItemText = CrossFilter(1).GetColumnValue(CurLine, 0)
        CrossFilter(0).InsertBuffer ItemText, "|"
    Next
    If InStr(ItemList, ":") > 0 Then
        MaxLine = CrossFilter(0).GetLineCount - 1
        For CurLine = 0 To MaxLine
            ItemText = CrossFilter(0).GetColumnValue(CurLine, 0)
            ItemText = GetItem(ItemText, 1, ":")
            CrossFilter(0).SetColumnValue CurLine, 0, ItemText
        Next
    End If
    ItemList = CrossFilter(0).SelectDistinct("0", "", "", vbLf, True)
    ItemList = Replace(ItemList, (vbLf & vbLf), vbLf, 1, -1, vbBinaryCompare)
    If Left(ItemList, 1) = vbLf Then ItemList = Mid(ItemList, 2)
    If Right(ItemList, 1) = vbLf Then ItemList = Left(ItemList, Len(ItemList) - 1)
End Sub

Private Sub chkWkDay_Click(Index As Integer)
    Dim i As Integer
    Dim tmpTag As String
    If chkWkDay(Index).Value = 1 Then chkWkDay(Index).BackColor = LightGreen Else chkWkDay(Index).BackColor = vbButtonFace
    Select Case Index
        Case 8
            If chkWkDay(Index).Value = 1 Then
                For i = 1 To 7
                    chkWkDay(i).Value = 1
                Next
                chkWkDay(8).Value = 0
            End If
        Case 0
            If chkWkDay(Index).Value = 1 Then
                For i = 1 To 7
                    chkWkDay(i).Value = 0
                Next
                chkWkDay(Index).Tag = "......."
                chkWkDay(0).Value = 0
            End If
        Case Else
            tmpTag = chkWkDay(0).Tag
            If chkWkDay(Index).Value = 1 Then
                Mid(tmpTag, Index, 1) = Trim(Str(Index))
                chkWkDay(0).Tag = tmpTag
            Else
                Mid(tmpTag, Index, 1) = "."
                chkWkDay(0).Tag = tmpTag
                If tmpTag = "......." Then chkWkDay(0).Value = 1
            End If
    End Select
End Sub

Private Sub chkWork_Click(Index As Integer)
Dim FirstButton As Integer
Dim LastButton As Integer
Dim CurButton As Integer
Dim ColIdx As Integer
    If chkWork(Index).Value = 0 Then chkWork(Index).BackColor = vbButtonFace
    If chkWork(Index).Value = 1 Then
        FilterPanel(Index).Enabled = True
    Else
        FilterPanel(Index).Enabled = False
    End If
    FirstButton = Index * 2
    LastButton = FirstButton + 1
    For CurButton = FirstButton To LastButton
        chkFilterSet(CurButton).Enabled = FilterPanel(Index).Enabled
        If FilterPanel(Index).Enabled Then
            If CurButton Mod 2 = 0 Then
                FilterTab(CurButton).DisplayBackColor = LightYellow
                FilterTab(CurButton).DisplayTextColor = vbBlack
                FilterTab(CurButton).SelectBackColor = DarkestYellow
                FilterTab(CurButton).SelectTextColor = vbWhite
                txtFilterVal(CurButton).BackColor = vbWhite
            Else
                FilterTab(CurButton).DisplayBackColor = NormalBlue
                FilterTab(CurButton).DisplayTextColor = vbWhite
                FilterTab(CurButton).SelectBackColor = DarkBlue
                FilterTab(CurButton).SelectTextColor = vbWhite
            End If
        Else
            FilterTab(CurButton).DisplayBackColor = LightGray
            FilterTab(CurButton).DisplayTextColor = vbBlack
            FilterTab(CurButton).SelectBackColor = LightGray
            FilterTab(CurButton).SelectTextColor = vbBlack
            If CurButton Mod 2 = 0 Then txtFilterVal(CurButton).BackColor = LightGray
        End If
        FilterTab(CurButton).RedrawTab
    Next
    CheckRecLoadFilter False
    If chkWork(Index).Value = 1 Then chkWork(Index).BackColor = LightestGreen
End Sub

Private Sub FilterTab_GotFocus(Index As Integer)
    FilterTab(Index).SelectTextColor = vbWhite
    FilterTab(Index).RedrawTab
    StatusBar.Panels(5).Text = FilterTab(Index).Tag
End Sub
Private Sub FilterTab_LostFocus(Index As Integer)
    FilterTab(Index).SelectTextColor = NormalGray
    FilterTab(Index).RedrawTab
    StatusBar.Panels(5).Text = ""
End Sub

Private Sub FilterTab_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    If Trim(FilterTab(Index).GetColumnValue(LineNo, 0)) <> "" Then
        SetStatusInfo Index, FilterTab(Index).GetColumnValue(LineNo, 0), "HighLighted"
    End If
End Sub

Private Sub FilterTab_SendLButtonClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    If Line < 0 Then
        FilterTab(Index).SetFocus
    Else
        'ToggleFilterValues Index, Line, Col
    End If
End Sub

Private Sub FilterTab_SendLButtonDblClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    If Line < 0 Then
        FilterTab(Index).SetFocus
    Else
        ToggleFilterValues Index, Line, Col
    End If
End Sub

Private Sub ToggleFilterValues(Index As Integer, Line As Long, Col As Long)
    Dim FromCol As Integer
    Dim ToCol As Integer
    Dim SelCol As Integer
    If Line >= 0 Then
        If Trim(FilterTab(Index).GetColumnValue(Line, 0)) <> "" Then
            FromCol = Index
            If Index Mod 2 = 0 Then
                ToCol = FromCol + 1
                SelCol = ToCol
            Else
                ToCol = FromCol - 1
                SelCol = FromCol
            End If
            SetStatusInfo Index, FilterTab(Index).GetColumnValue(Line, 0), "Moved"
            ShiftData FromCol, ToCol, Line
            FilterTab(Index).SetCurrentSelection Line
            FilterTab(ToCol).RedrawTab
            Me.Refresh
            If FromCol = 0 Then
                If FilterTab(ToCol).GetLineCount = 1 Then
                    'chkTool(2).Value = 1
                End If
            ElseIf FromCol = 1 Then
                If FilterTab(FromCol).GetLineCount = 0 Then
                    If Not FilterCalledAsDispo Then FlightExtract.ResetList
                End If
                If FilterTab(FromCol).GetLineCount = 1 Then
                    'chkTool(2).Value = 1
                End If
            End If
            CheckRecLoadFilter False
        End If
    End If
End Sub

Private Sub SetStatusInfo(ColIdx As Integer, ColTxt As String, ActTxt As String)
    Dim tmpTxt As String
    Dim tmpWork As Integer
    tmpWork = ((ColIdx + 2) \ 2) - 1
    StatusBar.Panels(1).Text = chkWork(tmpWork).Caption
    StatusBar.Panels(2).Text = FilterTab(ColIdx).GetHeaderText
    StatusBar.Panels(3).Text = Replace(ColTxt, ",", "", 1, -1, vbBinaryCompare) & ": "
    StatusBar.Panels(4).Text = ActTxt
End Sub

Private Sub FilterTab_SendRButtonClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    Dim tmpText As String
    Dim tmpType As String
    tmpText = Trim(FilterTab(Index).GetColumnValue(Line, Col))
    tmpType = FilterTab(Index).Tag
    BasicDetails tmpType, tmpText
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    'If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
    If KeyCode = vbKeyF1 Then ShowHtmlHelp Me, "", ""
End Sub

Private Sub Form_Load()
    If UpdateMode Then
        Me.Width = FilterPanel(1).Left + FilterPanel(1).Width + 10 * Screen.TwipsPerPixelX
    End If
    Me.Caption = Me.Caption & " (" & MySetUp.CurFileInfo(5).Caption & " Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
    MyFilterCaption = Me.Caption
    Me.Visible = True
    Me.Refresh
    InitLists
End Sub

Private Sub InitLists()
Dim myHeaderList As String
Dim HdrLenLst As String
Dim i As Integer
Dim j As Integer
Dim tmpLen As Integer
    For i = 0 To 9
        FilterTab(i).ShowHorzScroller False
        FilterTab(i).ResetContent
        FilterTab(i).FontName = "Courier New"
        FilterTab(i).SetTabFontBold True
        FilterTab(i).HeaderFontSize = 17
        FilterTab(i).FontSize = 17
        FilterTab(i).LineHeight = 18
        FilterTab(i).HeaderLengthString = "200"
        FilterTab(i).HeaderString = " "
        FilterTab(i).MainHeaderOnly = True
        FilterTab(i).Tag = ""
        If i Mod 2 = 0 Then
            FilterTab(i).DisplayBackColor = LightYellow
            FilterTab(i).DisplayTextColor = vbBlack
            FilterTab(i).SelectBackColor = DarkestYellow
            FilterTab(i).SelectTextColor = vbBlack
        Else
            FilterTab(i).DisplayBackColor = NormalBlue
            FilterTab(i).DisplayTextColor = vbWhite
            FilterTab(i).SelectBackColor = DarkestBlue
            FilterTab(i).SelectTextColor = vbWhite
        End If
        FilterTab(i).EmptyAreaBackColor = LightGray
        FilterTab(i).GridLineColor = DarkGray
    Next
    Me.Refresh
    InitFilterConfig
    BuildFilters
    For i = 0 To 4
        chkWork(i).Value = 1
        If Val(chkFilterSet(i * 2).Caption) = 0 Then chkWork(i).Value = 0
    Next
    If UpdateMode Then
        For i = 2 To 4
            chkWork(i).Value = 0
            chkWork(i).Enabled = False
        Next
    End If
End Sub

Public Function CheckFilterKeys(RecData As String, IsValidLine As Boolean) As Boolean
    Dim RetVal As Boolean
    Dim i As Integer
    Dim CurTab As Integer
    Dim CurColLst As String
    Dim tmpType As String
    Dim tmpWidthList As String
    Dim ItmNbr As Integer
    Dim ColNbr As Integer
    Dim tmpCol As String
    Dim ColDat As String
    Dim ChkVal As String
    Dim HitLst As String
    Dim ColLen As Integer
    Dim CurCol As Integer
    Dim SelCnt As Integer
    Dim HitCnt As Integer
    RetVal = IsValidLine
    If RetVal = True Then
        If FilterCalledAsDispo Then
            tmpWidthList = MainDialog.FileData(CurFdIdx).ColumnWidthString
        Else
            tmpWidthList = FlightExtract.ExtractData.ColumnWidthString
        End If
        SelCnt = 0
        HitCnt = 0
        i = 0
        Do
            If FilterPanel(i).Enabled = True Then
                If chkWork(i).Value = 1 Then
                    CurTab = i * 2 + 1
                    If FilterTab(CurTab).GetLineCount > 0 Then
                        tmpType = FilterTab(CurTab).Tag
                        SelCnt = SelCnt + 1
                        'Run through Activated FilterValues
                        ItmNbr = 1
                        Do
                            CurColLst = GetItem(CheckItems(i), ItmNbr, ",")
                            If CurColLst <> "" Then
                                CurColLst = Replace(CurColLst, "+", ",", 1, -1, vbBinaryCompare)
                                ChkVal = ""
                                ColNbr = 1
                                Do
                                    tmpCol = GetItem(CurColLst, ColNbr, ",")
                                    If tmpCol <> "" Then
                                        CurCol = Val(tmpCol) + 1
                                        ColLen = Val(GetItem(tmpWidthList, CurCol, ","))
                                        ColDat = GetItem(RecData, CurCol, ",")
                                        ChkVal = ChkVal & Left(ColDat & Space(ColLen), ColLen)
                                    End If
                                    ColNbr = ColNbr + 1
                                Loop While tmpCol <> ""
                                ChkVal = RTrim(ChkVal)
                                If ChkVal <> "" Then
                                    Select Case tmpType
                                        Case "ROUTE"
                                            HitLst = CheckCrossFilter(CurTab, ChkVal)
                                        Case Else
                                            HitLst = FilterTab(CurTab).GetLinesByColumnValue(0, ChkVal, 0)
                                    End Select
                                    If HitLst <> "" Then HitCnt = HitCnt + 1
                                End If
                            End If
                            ItmNbr = ItmNbr + 1
                        Loop While (CurColLst <> "") And (HitCnt < SelCnt)
                    End If
                End If
            End If
            i = i + 1
        Loop While (i <= 4) And (SelCnt = HitCnt)
        If SelCnt <> HitCnt Then RetVal = False
    End If
    CheckFilterKeys = RetVal
End Function
Private Function CheckCrossFilter(CurTab As Integer, ItemList As String) As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ChkVal As String
    Dim HitList As String
    CrossFilter(0).ResetContent
    CrossFilter(0).InsertBuffer ItemList, "|"
    HitList = ""
    CurLine = 0
    MaxLine = CrossFilter(0).GetLineCount - 1
    While (HitList = "") And (CurLine <= MaxLine)
        ChkVal = CrossFilter(0).GetColumnValue(CurLine, 0)
        ChkVal = GetItem(ChkVal, 1, ":")
        HitList = FilterTab(CurTab).GetLinesByColumnValue(0, ChkVal, 0)
        CurLine = CurLine + 1
    Wend
    CheckCrossFilter = HitList
End Function
Public Sub InsertFilterValue(FilterData As String, ColIdx As Integer)
Dim MaxLin As Long
Dim CurLin As Long
Dim FndLin As Long
Dim tmpData As String
    MaxLin = FilterTab(ColIdx).GetLineCount - 1
    FndLin = -1
    CurLin = 0
    While (CurLin <= MaxLin) And (FndLin < 0)
        tmpData = FilterTab(ColIdx).GetLineValues(CurLin)
        If FilterData <= tmpData Then FndLin = CurLin
        CurLin = CurLin + 1
    Wend
    If FndLin < 0 Then
        If CurLin > MaxLin Then FndLin = CurLin Else FndLin = 0
    End If
    FilterTab(ColIdx).InsertTextLineAt FndLin, FilterData, True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Me.Tag = ""
        Me.Hide
        'FlightExtract.Show
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    Dim i As Integer
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim MaxLin As Long
    NewHeight = Me.ScaleHeight - StatusBar.Height - FilterPanel(0).Top - Screen.TwipsPerPixelY
    If NewHeight > 1500 Then
        NewTop = NewHeight - ButtonPanel(0).Height - 2 * Screen.TwipsPerPixelY
        For i = 0 To 4
            FilterPanel(i).Height = NewHeight
            ButtonPanel(i).Top = NewTop
        Next
        NewHeight = ButtonPanel(0).Top - FilterTab(0).Top - 2 * Screen.TwipsPerPixelY
        MaxLin = (NewHeight - (FilterTab(0).LineHeight * Screen.TwipsPerPixelY) + (10 * Screen.TwipsPerPixelY)) / (FilterTab(0).LineHeight * Screen.TwipsPerPixelY)
        For i = 0 To 9
            FilterTab(i).Height = NewHeight
            If FilterTab(i).GetVScrollPos = 0 Then
                If MaxLin > FilterTab(i).GetLineCount Then
                    FilterTab(i).ShowVertScroller False
                Else
                    FilterTab(i).ShowVertScroller True
                End If
            End If
        Next
        Me.Refresh
    End If
End Sub
Private Sub CheckRecLoadFilter(ForLoad As Boolean)
    Dim Result As String
    Dim i As Integer
    Dim MaxLine As Long
    Result = ""
    For i = 1 To 9 Step 2
        If chkFilterSet(i).Enabled = True Then
            MaxLine = FilterTab(i).GetLineCount - 1
            If MaxLine >= 0 Then
                Result = Result & FilterTab(i).GetBuffer(0, MaxLine, ",")
            End If
        End If
    Next
    If ForLoad = True Then LastLoadFilter = Result
    If LastLoadFilter = Result Then
        chkTool(2).ForeColor = vbBlack
        chkTool(2).BackColor = vbButtonFace
        chkTool(4).Enabled = True
        chkTool(2).Tag = "OK"
    Else
        chkTool(2).ForeColor = vbWhite
        chkTool(2).BackColor = vbRed
        chkTool(4).Enabled = False
        chkTool(2).Tag = "NOTOK"
    End If
    If Not FilterCalledAsDispo Then
        If Not ForLoad Then FlightExtract.CheckLoadFilter False
    End If
End Sub

Public Function CheckFullAirlineFilter() As Boolean
    Dim Result As Boolean
    Dim i As Integer
    Dim FilterCnt As Integer
    Result = True
    FilterCnt = 0
    For i = 3 To 9 Step 2
        If chkFilterSet(i).Enabled = True Then FilterCnt = FilterCnt + Val(chkFilterSet(i).Caption)
    Next
    If FilterCnt > 0 Then Result = False
    CheckFullAirlineFilter = Result
End Function

Public Sub PrepareNoopFlights(SetEnable As Boolean)
    Dim i As Integer
    For i = 1 To 4
        If SetEnable = True Then chkWork(i) = 1 Else chkWork(i) = 0
        chkWork(i).Enabled = SetEnable
    Next
    If UpdateMode Then
        For i = 2 To 4
            chkWork(i).Value = 0
            chkWork(i).Enabled = False
        Next
    End If
    If SetEnable = True Then
        Caption = "Record Filter Preset" & " (" & MySetUp.CurFileInfo(5).Caption & " Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
    Else
        Caption = "Airline Compare Flights Filter" & " (" & MySetUp.CurFileInfo(5).Caption & " Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
    End If
End Sub

Private Sub txtFilterVal_Change(Index As Integer)
    If Index = 2 Then
        SearchInList Index, 1
    Else
        SearchInList Index, 2
    End If
End Sub

Private Sub txtFilterVal_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim LineNo As Long
    Dim HitText As String
    If KeyAscii = 13 Then
        LineNo = FilterTab(Index).GetCurrentSelected
        If LineNo >= 0 Then
            HitText = FilterTab(Index).GetColumnValue(LineNo, 0)
            If InStr(HitText, txtFilterVal(Index).Text) > 0 Then
                FilterTab_SendLButtonDblClick Index, LineNo, 0
                chkTool(2).Value = 1
            End If
        End If
        KeyAscii = 0
    Else
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End If
End Sub

Private Sub SearchInList(UseIndex As Integer, Method As Integer)
    Static LastIndex As Integer
    Dim HitLst As String
    Dim HitRow As Long
    Dim NxtRow As Long
    Dim NewScroll As Long
    Dim LoopCount As Integer
    Dim tmpText As String
    Dim ColNbr As String
    Dim Index As Integer
    Dim UseCol As Long
    Index = UseIndex
    If Index < 0 Then Index = LastIndex
    LastIndex = Index
    tmpText = Trim(txtFilterVal(Index).Text)
    UseCol = 0
    If tmpText <> "" Then
        LoopCount = 0
        Do
            HitLst = FilterTab(Index).GetNextLineByColumnValue(UseCol, tmpText, Method)
            HitRow = Val(HitLst)
            If HitLst <> "" Then
                If HitRow > 5 Then NewScroll = HitRow - 5 Else NewScroll = 0
                FilterTab(Index).OnVScrollTo NewScroll
            Else
                FilterTab(Index).SetCurrentSelection -1
                FilterTab(Index).OnVScrollTo 0
            End If
            LoopCount = LoopCount + 1
        Loop While LoopCount < 2 And HitLst = ""
        FilterTab(Index).SetCurrentSelection HitRow
    End If
    txtFilterVal(Index).SetFocus
End Sub


