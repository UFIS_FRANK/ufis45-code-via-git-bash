VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form ImportProgress 
   Caption         =   "Import Progress"
   ClientHeight    =   975
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8040
   Icon            =   "ImportProgress.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   975
   ScaleWidth      =   8040
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   5000
      Left            =   7350
      Top             =   30
   End
   Begin VB.Timer Timer1 
      Interval        =   5000
      Left            =   6900
      Top             =   30
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   690
      Width           =   8040
      _ExtentX        =   14182
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11113
         EndProperty
      EndProperty
   End
   Begin VB.Frame ButtonPanel 
      Height          =   2715
      Left            =   5790
      TabIndex        =   2
      Top             =   -120
      Width           =   1065
      Begin VB.CheckBox chkWork 
         Caption         =   ".END."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   1470
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   ".POP."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   1140
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   ".BGN."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   810
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Reset"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   480
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Hide"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   150
         Width           =   855
      End
   End
   Begin TABLib.TAB ProgTab 
      Height          =   2595
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5355
      _Version        =   65536
      _ExtentX        =   9446
      _ExtentY        =   4577
      _StockProps     =   64
   End
End
Attribute VB_Name = "ImportProgress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkWork_Click(Index As Integer)
    On Error Resume Next
    Select Case Index
        Case 0
            If chkWork(Index).Value = 1 Then
                Me.Hide
                chkWork(Index).Value = 0
            End If
        Case 1
            If chkWork(Index).Value = 1 Then
                ProgTab.ResetContent
                ProgTab.Refresh
                chkWork(Index).Value = 0
            End If
        Case 2
            If chkWork(Index).Value = 1 Then
                TestBcBgn
                chkWork(Index).Value = 0
            End If
        Case 3
            If chkWork(Index).Value = 1 Then
                TestBcPop
                chkWork(Index).Value = 0
            End If
        Case 4
            If chkWork(Index).Value = 1 Then
                TestBcEnd
                chkWork(Index).Value = 0
            End If
        Case Else
    End Select
End Sub
Private Sub TestBcBgn()
    Dim RecvName As String
    Dim Tws As String
    Dim CedaSqlKey As String
    Dim Fields As String
    Dim Data As String
    RecvName = "trevisans1"
    Tws = ".BGN.120910121,1,2"
    CedaSqlKey = "CMDS,120910122,500,501,1000"
    Fields = "RMSIMP,20030302,20030325,20030302,20030325"
    Data = "DATA"
    EvaluateBc RecvName, Tws, CedaSqlKey, Fields, Data
End Sub
Private Sub TestBcPop()
    Dim RecvName As String
    Dim Tws As String
    Dim CedaSqlKey As String
    Dim Fields As String
    Dim Data As String
    RecvName = "trevisans1"
    Tws = ".POP.120910121,1,2"
    CedaSqlKey = "CMDS,120910122,500,501,1000"
    Fields = "RMSIMP,20030302,20030325,20030302,20030325"
    Data = "5,392,500,7805"
    EvaluateBc RecvName, Tws, CedaSqlKey, Fields, Data
End Sub
Private Sub TestBcEnd()
    Dim RecvName As String
    Dim Tws As String
    Dim CedaSqlKey As String
    Dim Fields As String
    Dim Data As String
    RecvName = "trevisans1"
    Tws = ".END.120910121,1,2"
    CedaSqlKey = "CMDS,120910122,500,501,1000"
    Fields = "RMSIMP,20030302,20030325,20030302,20030325"
    Data = "251,500,7805"
    EvaluateBc RecvName, Tws, CedaSqlKey, Fields, Data
End Sub
Private Sub Form_Load()
    Dim i As Integer
    'If StartedAsCompareTool Then Me.WindowState = vbMinimized
    ProgTab.ResetContent
    ProgTab.HeaderString = "ID,P,Type,Begin,End,Progress,Total,WKS"
    ProgTab.HeaderLengthString = "0,0,52,54,54,200,46,500"
    ProgTab.ColumnWidthString = "20,1,10,8,8,10,10,32"
    ProgTab.HeaderAlignmentString = "L,L,C,C,C,C,C,L"
    ProgTab.ColumnAlignmentString = "L,L,C,C,C,C,R,L"
    ProgTab.FontName = "Courier New"
    ProgTab.DateTimeSetColumnFormat 3, "YYYYMMDD", "DDMMMYY"
    ProgTab.DateTimeSetColumnFormat 4, "YYYYMMDD", "DDMMMYY"
    For i = 1 To 100
        ProgTab.CreateDecorationObject "Prog" & CStr(i), "L,L,T,B,R", CStr(i * 2) & ",2,2,2,2", Str(vbGreen) & "," & Str(LightGray) & "," & Str(LightGray) & "," & Str(vbBlack) & "," & Str(vbBlack)
    Next
    ProgTab.Refresh
End Sub

Private Sub Form_Resize()
    Dim NewVal As Long
    ButtonPanel.Left = Me.ScaleWidth - ButtonPanel.Width + 60
    NewVal = ButtonPanel.Left - ProgTab.Left
    If NewVal > 300 Then ProgTab.Width = NewVal
    NewVal = Me.ScaleHeight - ProgTab.Top - StatusBar1.Height
    If NewVal > 300 Then ProgTab.Height = NewVal
End Sub

Public Sub EvaluateBc(BcWks As String, BcTws As String, BcSelect As String, BcFields As String, BcData As String)
    Dim NewMainEntry As Boolean
    Dim NewSubEntry As Boolean
    Dim NewEntry As Boolean
    Dim PacketMsgCode As String
    Dim PacketImportType As String
    Dim PacketBeginDate As String
    Dim PacketEndDate As String
    Dim MainPacketKey As String
    Dim SubPacketKey As String
    Dim tmpData As String
    Dim LookKey As String
    Dim TotalPackets As Long
    Dim CurPacketNo As Long
    Dim CurTotal As Long
    Dim StepValue As Long
    Dim MaxTotal As Long
    Dim CurProgress As Long
    Dim NewProgEntry As String
    Dim CurBar As String
    Dim LineNo As Long
    
    On Error Resume Next
    
    ProgTab.SetInternalLineBuffer True
    PacketMsgCode = GetItem(BcTws, 2, ".")
    tmpData = GetItem(BcTws, 3, ".")
    MainPacketKey = GetItem(tmpData, 1, ",")
    CurPacketNo = Val(GetItem(tmpData, 2, ","))
    TotalPackets = Val(GetItem(tmpData, 3, ","))
    SubPacketKey = GetItem(BcSelect, 2, ",")
    
    StepValue = Val(GetItem(BcData, 1, ","))
    
    MainPacketKey = Right("0000000000" & MainPacketKey, 10)
    SubPacketKey = Right("0000000000" & SubPacketKey, 10)
    LookKey = MainPacketKey & "0000000000"
    NewMainEntry = True
    MaxTotal = Val(GetItem(BcSelect, 5, ","))
    If Val(ProgTab.GetLinesByIndexValue("LOOK", LookKey, 0)) > 0 Then
        LineNo = ProgTab.GetNextResultLine
        If PacketMsgCode = "END" Then
            CurTotal = Val(GetItem(BcSelect, 4, ","))
        Else
            CurTotal = Val(ProgTab.GetColumnValue(LineNo, 6)) + StepValue
        End If
        CurProgress = Int(CurTotal * 100 / MaxTotal)
        If CurTotal > MaxTotal Then CurTotal = MaxTotal
        If CurProgress > 100 Then CurProgress = 100
        CurBar = "Prog" & CStr(CurProgress)
        ProgTab.SetDecorationObject LineNo, 5, CurBar
        ProgTab.SetColumnValue LineNo, 5, CStr(CurProgress) & " %"
        ProgTab.SetColumnValue LineNo, 6, CStr(CurTotal)
        ProgTab.TimerSetValue LineNo, 12
        NewMainEntry = False
    End If
    If NewMainEntry Then
        If PacketMsgCode = "END" Then
            CurTotal = Val(GetItem(BcSelect, 4, ","))
        Else
            CurTotal = Val(ProgTab.GetColumnValue(LineNo, 6)) + StepValue
        End If
        CurProgress = Int(CurTotal * 100 / MaxTotal)
        If CurTotal > MaxTotal Then CurTotal = MaxTotal
        If CurProgress > 100 Then CurProgress = 100
        CurBar = "Prog" & CStr(CurProgress)
        PacketImportType = GetItem(BcFields, 1, ",")
        PacketBeginDate = GetItem(BcFields, 4, ",")
        PacketEndDate = GetItem(BcFields, 5, ",")
        NewProgEntry = ""
        NewProgEntry = NewProgEntry & LookKey & ","
        NewProgEntry = NewProgEntry & "M" & ","
        NewProgEntry = NewProgEntry & PacketImportType & ","
        NewProgEntry = NewProgEntry & PacketBeginDate & ","
        NewProgEntry = NewProgEntry & PacketEndDate & ","
        NewProgEntry = NewProgEntry & CStr(CurProgress) & " %,"
        NewProgEntry = NewProgEntry & CStr(CurTotal) & ","
        NewProgEntry = NewProgEntry & BcWks
        ProgTab.InsertTextLine NewProgEntry, False
        LineNo = ProgTab.GetLineCount - 1
        CurBar = "Prog" & CStr(CurProgress)
        ProgTab.SetDecorationObject LineNo, 5, CurBar
        ProgTab.TimerSetValue LineNo, 12
        NewEntry = True
    End If
    If TotalPackets > 1 Then
        LookKey = MainPacketKey & SubPacketKey
        NewSubEntry = True
        MaxTotal = Val(GetItem(BcSelect, 3, ","))
        If Val(ProgTab.GetLinesByIndexValue("LOOK", LookKey, 0)) > 0 Then
            LineNo = ProgTab.GetNextResultLine
            If PacketMsgCode = "END" Then
                CurTotal = MaxTotal
                CurProgress = 100
            Else
                CurTotal = Val(ProgTab.GetColumnValue(LineNo, 6)) + StepValue
                CurProgress = Int(CurTotal * 100 / MaxTotal)
            End If
            If CurTotal > MaxTotal Then CurTotal = MaxTotal
            If CurProgress > 100 Then CurProgress = 100
            CurBar = "Prog" & CStr(CurProgress)
            ProgTab.SetDecorationObject LineNo, 5, CurBar
            ProgTab.SetColumnValue LineNo, 5, CStr(CurProgress) & " %"
            ProgTab.SetColumnValue LineNo, 6, CStr(CurTotal)
            ProgTab.TimerSetValue LineNo, 12
            NewSubEntry = False
        End If
        If NewSubEntry Then
            If PacketMsgCode = "END" Then
                CurTotal = MaxTotal
                CurProgress = 100
            Else
                CurTotal = Val(ProgTab.GetColumnValue(LineNo, 6)) + StepValue
                CurProgress = Int(CurTotal * 100 / MaxTotal)
            End If
            If CurTotal > MaxTotal Then CurTotal = MaxTotal
            If CurProgress > 100 Then CurProgress = 100
            CurBar = "Prog" & CStr(CurProgress)
            PacketImportType = GetItem(BcFields, 1, ",")
            PacketBeginDate = GetItem(BcFields, 4, ",")
            PacketEndDate = GetItem(BcFields, 5, ",")
            NewProgEntry = ""
            NewProgEntry = NewProgEntry & LookKey & ","
            NewProgEntry = NewProgEntry & "S" & ","
            NewProgEntry = NewProgEntry & CStr(CurPacketNo) & "/"
            NewProgEntry = NewProgEntry & CStr(TotalPackets) & ","
            NewProgEntry = NewProgEntry & PacketBeginDate & ","
            NewProgEntry = NewProgEntry & PacketEndDate & ","
            NewProgEntry = NewProgEntry & CStr(CurProgress) & " %,"
            NewProgEntry = NewProgEntry & CStr(CurTotal) & ","
            NewProgEntry = NewProgEntry & BcWks
            ProgTab.InsertTextLine NewProgEntry, False
            LineNo = ProgTab.GetLineCount - 1
            CurBar = "Prog" & CStr(CurProgress)
            ProgTab.SetDecorationObject LineNo, 5, CurBar
            ProgTab.TimerSetValue LineNo, 12
            NewEntry = True
        End If
    End If
    ProgTab.SetInternalLineBuffer False
    If NewEntry Then
        ProgTab.IndexDestroy "LOOK"
        ProgTab.Sort 0, True, True
        ProgTab.IndexCreate "LOOK", 0
        If Not Me.Visible Then
            Me.Show
            SetFormOnTop Me, True
        End If
    End If
    ProgTab.Refresh
End Sub

Private Sub ProgTab_TimerExpired(ByVal LineNo As Long, ByVal LineStatus As Long)
    Dim tmpVal As String
    tmpVal = Trim(ProgTab.GetColumnValue(LineNo, 5))
    tmpVal = Replace(tmpVal, "%", "", 1, -1, vbBinaryCompare)
    If Val(tmpVal) >= 100 Then
        ProgTab.SetLineStatusValue LineNo, 1
    Else
        ProgTab.TimerSetValue LineNo, 12
    End If
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    ProgTab.TimerCheck
    Timer2.Enabled = True
End Sub

Private Sub Timer2_Timer()
    Dim MaxLine As Long
    Dim CurLine As Long
    Timer2.Enabled = False
    ProgTab.IndexDestroy "LOOK"
    MaxLine = ProgTab.GetLineCount - 1
    CurLine = 0
    While CurLine <= MaxLine
        If ProgTab.GetLineStatusValue(CurLine) = 1 Then
            ProgTab.DeleteLine CurLine
            CurLine = CurLine - 1
            MaxLine = MaxLine - 1
        End If
        CurLine = CurLine + 1
    Wend
    ProgTab.IndexCreate "LOOK", 0
    ProgTab.RedrawTab
    If ProgTab.GetLineCount = 0 Then chkWork(0).Value = 1
    Timer1.Enabled = True
End Sub
