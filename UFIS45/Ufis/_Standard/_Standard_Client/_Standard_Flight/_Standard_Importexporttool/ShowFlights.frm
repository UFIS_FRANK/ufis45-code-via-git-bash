VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form ShowFlights 
   Caption         =   "Existing Flights"
   ClientHeight    =   5295
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7950
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "ShowFlights.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   5295
   ScaleWidth      =   7950
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkWork 
      Caption         =   "Save As"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Compare"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   1770
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB FlightList 
      Height          =   2775
      Left            =   60
      TabIndex        =   0
      Top             =   330
      Width           =   4605
      _Version        =   65536
      _ExtentX        =   8123
      _ExtentY        =   4895
      _StockProps     =   64
   End
   Begin VB.Label lblCount 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2640
      TabIndex        =   2
      Top             =   30
      Width           =   945
   End
End
Attribute VB_Name = "ShowFlights"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private CurHeadLength As String
Private IsInitialized As Boolean

Public Sub InitFlightList(ipLines As Integer, ipFirstCall As Boolean)
Dim i As Integer
Dim tmpLen
Dim HdrLenLst As String
Dim tmpHeader As String
Dim ActColLst As String
Dim FldTypLst As String

    Me.FontSize = MySetUp.FontSlider.Value
    '"FTYP,ADID,FLNO,ORG3,STOD,DOOD,VIA3,DES3,STOA,DOOA,FLTI,STYP,ACT3,NOSE,REGN,RTYP,RKEY,URNO"
    tmpHeader = "F,I,FLNO,ORG,STOD,W,VIA,DES,STOA,W,T,NT,ACT,CAP,REGN,L,FKEY,RKEY,URNO,REMARK"
    ActColLst = "1,1,9   ,3  ,14  ,1,3  ,3  ,14  ,1,1,2 ,3  ,3  ,8   ,1,18  ,10  ,10  "
    HdrLenLst = ""
    FldTypLst = ""
    i = 0
    Do
        i = i + 1
        tmpLen = GetItem(ActColLst, i, ",")
        If tmpLen <> "" Then
            If tmpLen > 0 Then
                HdrLenLst = HdrLenLst & TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX + 6 & ","
            Else
                HdrLenLst = HdrLenLst & "1,"
            End If
            FldTypLst = FldTypLst & "L,"
        End If
    Loop While tmpLen <> ""
    HdrLenLst = HdrLenLst & "1000"
    FldTypLst = FldTypLst & "L"
    If ipFirstCall Then FlightList.ResetContent
    FlightList.FontName = "Courier New"
    FlightList.SetTabFontBold True
    FlightList.ShowHorzScroller False
    FlightList.HeaderFontSize = MySetUp.FontSlider.Value + 6
    FlightList.FontSize = MySetUp.FontSlider.Value + 6
    FlightList.LineHeight = MySetUp.FontSlider.Value + 7
    FlightList.HeaderLengthString = HdrLenLst
    FlightList.HeaderString = tmpHeader
    
    If ipFirstCall And Not IsInitialized Then
        FlightList.Height = ipLines * FlightList.LineHeight * Screen.TwipsPerPixelY
        Me.Height = FlightList.Top + FlightList.Height + 30 * Screen.TwipsPerPixelY
        IsInitialized = True
    End If
    CurHeadLength = ActColLst
    Me.Refresh
End Sub

Public Sub LoadFlightData(AftVpfr As String, AftVpto As String, AftFlca As String, AftFltn As String, AftFlns As String)
    Dim RetVal As Integer
    Dim tmpFields As String
    Dim tmpSqlKey As String
    FlightList.ResetContent
    tmpFields = "FTYP,ADID,FLNO,ORG3,STOD,DOOD,VIA3,DES3,STOA,DOOA,FLTI,STYP,ACT3,NOSE,REGN,RTYP,FKEY,RKEY,URNO"
    tmpFields = Replace(tmpFields, "STYP", UseStypField, 1, -1, vbBinaryCompare)
    tmpSqlKey = "WHERE "
    If AftFltn <> "" Then tmpSqlKey = tmpSqlKey & "FLTN='" & AftFltn & "' AND "
    tmpSqlKey = tmpSqlKey & "((STOD BETWEEN '" & AftVpfr & "' AND '" & AftVpto & "') OR "
    tmpSqlKey = tmpSqlKey & "(STOA BETWEEN '" & AftVpfr & "' AND '" & AftVpto & "')) "
    If Len(AftFlca) = 2 Then
        tmpSqlKey = tmpSqlKey & "AND ALC2='" & AftFlca & "' "
    Else
        tmpSqlKey = tmpSqlKey & "AND ALC3='" & AftFlca & "' "
    End If
    If Trim(AftFlns) <> "" Then
        tmpSqlKey = tmpSqlKey & "AND FLNS='" & AftFlns & "' "
    End If
    RetVal = UfisServer.CallCeda(UserAnswer, "GFR", "AFTTAB", tmpFields, "", tmpSqlKey, "FLNO,STOD,STOA", 0, True, False)
    FlightList.InsertBuffer UserAnswer, vbLf
    FlightList.RedrawTab
    lblCount.Caption = Str(FlightList.GetLineCount)
    Me.Refresh
End Sub

Private Sub chkWork_Click(Index As Integer)
    Select Case Index
        Case 0
            'Close
            If chkWork(Index) = 1 Then Unload Me
        Case 1
            If chkWork(Index) = 1 Then
                CompareFlights
                chkWork(Index) = 0
            End If
        Case 2
            'Save As
            If chkWork(Index).Value = 1 Then
                MainDialog.SaveLoadedFile Index, ShowFlights.FlightList
                chkWork(Index).Value = 0
            End If
    End Select
End Sub

Private Sub CompareFlights()
    FlightExtract.ExtractData.SetCurrentSelection 0
            FlightDetails.GetFlightCalendar
            If MySetUp.MonitorSetting(3).Value = 1 Then
                FlightDetails.Show , Me
            Else
                FlightDetails.Show
            End If
End Sub
Private Sub FlightList_SendRButtonClick(ByVal Line As Long, ByVal Col As Long)
    If Col >= 0 Then
        'FlightList.Sort str(Col), True, True
        'FlightList.RedrawTab
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    'If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
    If KeyCode = vbKeyF1 Then ShowHtmlHelp Me, "", ""
End Sub

Private Sub Form_Load()
    Me.Left = 0
    Me.Top = 1000
    Me.Width = MainDialog.Width
    InitFlightList 16, True
End Sub
Private Sub Form_Resize()
Dim NewVal As Long
    NewVal = Me.ScaleHeight - FlightList.Top '- StatusBar.Height - 4 * Screen.TwipsPerPixelY
    If NewVal > 50 Then
        FlightList.Height = NewVal
    End If
    NewVal = Me.ScaleWidth - 2 * FlightList.Left '- 5 * Screen.TwipsPerPixelX
    If NewVal > 50 Then FlightList.Width = NewVal
End Sub

