﻿namespace EFPS
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.panelFlight = new System.Windows.Forms.Panel();
            this.txtFlightAltitude = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTCode = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cBoxTraining = new System.Windows.Forms.CheckBox();
            this.cBoxCircuit = new System.Windows.Forms.CheckBox();
            this.LoginControl = new AxAATLOGINLib.AxAatLogin();
            this.panel2 = new System.Windows.Forms.Panel();
            this.VISCArr = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.EXITArr = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.LIDRArr = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.EETTArr = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.ASSTArr = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.AETTArr = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtMissedTime = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ArrtxtAircraftType = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.ArrtxtCallSign = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.ArrtxtFreqChange = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ArrtxtCJS = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ArrtxtRoute = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ArrtxtFlno2 = new System.Windows.Forms.TextBox();
            this.ArrtxtFlno1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtArrivalUpdatedOn = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtArrivalCreatedOn = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtArrivalGroudRoute = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtIntersection = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtInboundGate = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtLandingClearance = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtActualArrivalTime = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtEstimatedArrivalTime = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtScheduedArrivalTime = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDepartAirport = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.OFTXDep = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.VISCDep = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.LIDRDep = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.EXOTDep = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.EETTDep = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.ASTTDep = new System.Windows.Forms.TextBox();
            this.AETTDep = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.DeptxtAircraftType = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.DeptxtCallSign = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cBoxParking = new System.Windows.Forms.CheckBox();
            this.cBoxAerodrome = new System.Windows.Forms.CheckBox();
            this.DeptxtFreqChange = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.DeptxtCJS = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.DeptxtRoute = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.DeptxtFlno2 = new System.Windows.Forms.TextBox();
            this.DeptxtFlno1 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtTsat = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtWake = new System.Windows.Forms.TextBox();
            this.txtClearanceIssued = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtClearanceRequest = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtTakeoffIssued = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtTaxiIssued = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtTaxiRequest = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtOutboundGate = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtPushIssued = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtPuchRequest = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtDepartureUpdatedOn = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtDepartureCreatedOn = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtDepartureGroudRoute = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtActualDepartureTime = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtEstimatedDepartureTime = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtScheduedDepartureTime = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtDesignationAirport = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.panelFlight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelFlight
            // 
            this.panelFlight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelFlight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFlight.Controls.Add(this.txtFlightAltitude);
            this.panelFlight.Controls.Add(this.label9);
            this.panelFlight.Controls.Add(this.txtSID);
            this.panelFlight.Controls.Add(this.label8);
            this.panelFlight.Controls.Add(this.txtTCode);
            this.panelFlight.Controls.Add(this.label6);
            this.panelFlight.Controls.Add(this.cBoxTraining);
            this.panelFlight.Controls.Add(this.cBoxCircuit);
            this.panelFlight.Controls.Add(this.LoginControl);
            this.panelFlight.Location = new System.Drawing.Point(14, 12);
            this.panelFlight.Name = "panelFlight";
            this.panelFlight.Size = new System.Drawing.Size(937, 32);
            this.panelFlight.TabIndex = 0;
            // 
            // txtFlightAltitude
            // 
            this.txtFlightAltitude.Location = new System.Drawing.Point(523, 4);
            this.txtFlightAltitude.Name = "txtFlightAltitude";
            this.txtFlightAltitude.ReadOnly = true;
            this.txtFlightAltitude.Size = new System.Drawing.Size(60, 20);
            this.txtFlightAltitude.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(370, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(147, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Flight Planned Altitude : ";
            // 
            // txtSID
            // 
            this.txtSID.BackColor = System.Drawing.SystemColors.Control;
            this.txtSID.Location = new System.Drawing.Point(47, 3);
            this.txtSID.Name = "txtSID";
            this.txtSID.ReadOnly = true;
            this.txtSID.Size = new System.Drawing.Size(75, 20);
            this.txtSID.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "SID : ";
            // 
            // txtTCode
            // 
            this.txtTCode.Location = new System.Drawing.Point(276, 3);
            this.txtTCode.Name = "txtTCode";
            this.txtTCode.ReadOnly = true;
            this.txtTCode.Size = new System.Drawing.Size(73, 20);
            this.txtTCode.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(148, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Transponder Code : ";
            // 
            // cBoxTraining
            // 
            this.cBoxTraining.AutoSize = true;
            this.cBoxTraining.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxTraining.Location = new System.Drawing.Point(822, 6);
            this.cBoxTraining.Name = "cBoxTraining";
            this.cBoxTraining.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cBoxTraining.Size = new System.Drawing.Size(107, 17);
            this.cBoxTraining.TabIndex = 6;
            this.cBoxTraining.Text = "Training Flight";
            this.cBoxTraining.UseVisualStyleBackColor = true;
            // 
            // cBoxCircuit
            // 
            this.cBoxCircuit.AutoSize = true;
            this.cBoxCircuit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxCircuit.Location = new System.Drawing.Point(663, 5);
            this.cBoxCircuit.Name = "cBoxCircuit";
            this.cBoxCircuit.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cBoxCircuit.Size = new System.Drawing.Size(112, 17);
            this.cBoxCircuit.TabIndex = 5;
            this.cBoxCircuit.Text = "Circuit Training";
            this.cBoxCircuit.UseVisualStyleBackColor = true;
            // 
            // LoginControl
            // 
            this.LoginControl.Enabled = true;
            this.LoginControl.Location = new System.Drawing.Point(3, 3);
            this.LoginControl.Name = "LoginControl";
            this.LoginControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("LoginControl.OcxState")));
            this.LoginControl.RightToLeft = false;
            this.LoginControl.Size = new System.Drawing.Size(100, 50);
            this.LoginControl.TabIndex = 18;
            this.LoginControl.Text = null;
            this.LoginControl.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.VISCArr);
            this.panel2.Controls.Add(this.label53);
            this.panel2.Controls.Add(this.EXITArr);
            this.panel2.Controls.Add(this.label51);
            this.panel2.Controls.Add(this.LIDRArr);
            this.panel2.Controls.Add(this.label50);
            this.panel2.Controls.Add(this.EETTArr);
            this.panel2.Controls.Add(this.label48);
            this.panel2.Controls.Add(this.ASSTArr);
            this.panel2.Controls.Add(this.label46);
            this.panel2.Controls.Add(this.AETTArr);
            this.panel2.Controls.Add(this.label47);
            this.panel2.Controls.Add(this.txtMissedTime);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.ArrtxtAircraftType);
            this.panel2.Controls.Add(this.label43);
            this.panel2.Controls.Add(this.ArrtxtCallSign);
            this.panel2.Controls.Add(this.label42);
            this.panel2.Controls.Add(this.ArrtxtFreqChange);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.ArrtxtCJS);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.ArrtxtRoute);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.ArrtxtFlno2);
            this.panel2.Controls.Add(this.ArrtxtFlno1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtArrivalUpdatedOn);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.txtArrivalCreatedOn);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.txtArrivalGroudRoute);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.txtIntersection);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.txtInboundGate);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.txtLandingClearance);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.txtActualArrivalTime);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.txtEstimatedArrivalTime);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.txtScheduedArrivalTime);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.txtDepartAirport);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Location = new System.Drawing.Point(13, 50);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(460, 660);
            this.panel2.TabIndex = 1;
            // 
            // VISCArr
            // 
            this.VISCArr.Location = new System.Drawing.Point(171, 565);
            this.VISCArr.Name = "VISCArr";
            this.VISCArr.ReadOnly = true;
            this.VISCArr.Size = new System.Drawing.Size(282, 20);
            this.VISCArr.TabIndex = 59;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(47, 568);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(118, 13);
            this.label53.TabIndex = 58;
            this.label53.Text = "Visibility Condition :";
            // 
            // EXITArr
            // 
            this.EXITArr.Location = new System.Drawing.Point(282, 495);
            this.EXITArr.Name = "EXITArr";
            this.EXITArr.ReadOnly = true;
            this.EXITArr.Size = new System.Drawing.Size(171, 20);
            this.EXITArr.TabIndex = 57;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(242, 498);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(39, 13);
            this.label51.TabIndex = 56;
            this.label51.Text = "EXIT:";
            // 
            // LIDRArr
            // 
            this.LIDRArr.Location = new System.Drawing.Point(171, 532);
            this.LIDRArr.Name = "LIDRArr";
            this.LIDRArr.ReadOnly = true;
            this.LIDRArr.Size = new System.Drawing.Size(282, 20);
            this.LIDRArr.TabIndex = 55;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(2, 534);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(164, 13);
            this.label50.TabIndex = 54;
            this.label50.Text = "Runway Landing Direction :";
            // 
            // EETTArr
            // 
            this.EETTArr.Location = new System.Drawing.Point(50, 496);
            this.EETTArr.Name = "EETTArr";
            this.EETTArr.ReadOnly = true;
            this.EETTArr.Size = new System.Drawing.Size(180, 20);
            this.EETTArr.TabIndex = 53;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(1, 498);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(43, 13);
            this.label48.TabIndex = 52;
            this.label48.Text = "EETT:";
            // 
            // ASSTArr
            // 
            this.ASSTArr.Location = new System.Drawing.Point(282, 464);
            this.ASSTArr.Name = "ASSTArr";
            this.ASSTArr.ReadOnly = true;
            this.ASSTArr.Size = new System.Drawing.Size(171, 20);
            this.ASSTArr.TabIndex = 51;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(238, 466);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(43, 13);
            this.label46.TabIndex = 50;
            this.label46.Text = "ASTT:";
            // 
            // AETTArr
            // 
            this.AETTArr.Location = new System.Drawing.Point(51, 463);
            this.AETTArr.Name = "AETTArr";
            this.AETTArr.ReadOnly = true;
            this.AETTArr.Size = new System.Drawing.Size(180, 20);
            this.AETTArr.TabIndex = 49;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(2, 466);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(43, 13);
            this.label47.TabIndex = 48;
            this.label47.Text = "AETT:";
            // 
            // txtMissedTime
            // 
            this.txtMissedTime.Location = new System.Drawing.Point(173, 148);
            this.txtMissedTime.Name = "txtMissedTime";
            this.txtMissedTime.ReadOnly = true;
            this.txtMissedTime.Size = new System.Drawing.Size(50, 20);
            this.txtMissedTime.TabIndex = 47;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(19, 152);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(147, 13);
            this.label10.TabIndex = 46;
            this.label10.Text = "Missed Approach Time : ";
            // 
            // ArrtxtAircraftType
            // 
            this.ArrtxtAircraftType.Location = new System.Drawing.Point(150, 56);
            this.ArrtxtAircraftType.Name = "ArrtxtAircraftType";
            this.ArrtxtAircraftType.ReadOnly = true;
            this.ArrtxtAircraftType.Size = new System.Drawing.Size(73, 20);
            this.ArrtxtAircraftType.TabIndex = 45;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(37, 59);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(92, 13);
            this.label43.TabIndex = 44;
            this.label43.Text = "Aircraft Type : ";
            // 
            // ArrtxtCallSign
            // 
            this.ArrtxtCallSign.Location = new System.Drawing.Point(378, 13);
            this.ArrtxtCallSign.Name = "ArrtxtCallSign";
            this.ArrtxtCallSign.ReadOnly = true;
            this.ArrtxtCallSign.Size = new System.Drawing.Size(72, 20);
            this.ArrtxtCallSign.TabIndex = 43;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(303, 16);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(69, 13);
            this.label42.TabIndex = 42;
            this.label42.Text = "Call Sign : ";
            // 
            // ArrtxtFreqChange
            // 
            this.ArrtxtFreqChange.Location = new System.Drawing.Point(400, 258);
            this.ArrtxtFreqChange.Name = "ArrtxtFreqChange";
            this.ArrtxtFreqChange.ReadOnly = true;
            this.ArrtxtFreqChange.Size = new System.Drawing.Size(50, 20);
            this.ArrtxtFreqChange.TabIndex = 41;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(264, 261);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 13);
            this.label7.TabIndex = 40;
            this.label7.Text = "Frequency Change : ";
            // 
            // ArrtxtCJS
            // 
            this.ArrtxtCJS.Location = new System.Drawing.Point(173, 258);
            this.ArrtxtCJS.Name = "ArrtxtCJS";
            this.ArrtxtCJS.ReadOnly = true;
            this.ArrtxtCJS.Size = new System.Drawing.Size(60, 20);
            this.ArrtxtCJS.TabIndex = 39;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(128, 261);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "CJS : ";
            // 
            // ArrtxtRoute
            // 
            this.ArrtxtRoute.Location = new System.Drawing.Point(116, 320);
            this.ArrtxtRoute.Multiline = true;
            this.ArrtxtRoute.Name = "ArrtxtRoute";
            this.ArrtxtRoute.ReadOnly = true;
            this.ArrtxtRoute.Size = new System.Drawing.Size(334, 60);
            this.ArrtxtRoute.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(60, 344);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 36;
            this.label2.Text = "Route : ";
            // 
            // ArrtxtFlno2
            // 
            this.ArrtxtFlno2.BackColor = System.Drawing.SystemColors.Control;
            this.ArrtxtFlno2.Location = new System.Drawing.Point(178, 13);
            this.ArrtxtFlno2.Name = "ArrtxtFlno2";
            this.ArrtxtFlno2.ReadOnly = true;
            this.ArrtxtFlno2.Size = new System.Drawing.Size(45, 20);
            this.ArrtxtFlno2.TabIndex = 35;
            // 
            // ArrtxtFlno1
            // 
            this.ArrtxtFlno1.BackColor = System.Drawing.SystemColors.Control;
            this.ArrtxtFlno1.Location = new System.Drawing.Point(132, 13);
            this.ArrtxtFlno1.Name = "ArrtxtFlno1";
            this.ArrtxtFlno1.ReadOnly = true;
            this.ArrtxtFlno1.Size = new System.Drawing.Size(40, 20);
            this.ArrtxtFlno1.TabIndex = 34;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(63, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Flight No : ";
            // 
            // txtArrivalUpdatedOn
            // 
            this.txtArrivalUpdatedOn.Location = new System.Drawing.Point(321, 630);
            this.txtArrivalUpdatedOn.Name = "txtArrivalUpdatedOn";
            this.txtArrivalUpdatedOn.ReadOnly = true;
            this.txtArrivalUpdatedOn.Size = new System.Drawing.Size(100, 20);
            this.txtArrivalUpdatedOn.TabIndex = 32;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(232, 633);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(87, 13);
            this.label21.TabIndex = 31;
            this.label21.Text = "Updated On : ";
            // 
            // txtArrivalCreatedOn
            // 
            this.txtArrivalCreatedOn.Location = new System.Drawing.Point(100, 630);
            this.txtArrivalCreatedOn.Name = "txtArrivalCreatedOn";
            this.txtArrivalCreatedOn.ReadOnly = true;
            this.txtArrivalCreatedOn.Size = new System.Drawing.Size(100, 20);
            this.txtArrivalCreatedOn.TabIndex = 30;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(15, 633);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 13);
            this.label20.TabIndex = 29;
            this.label20.Text = "Created On : ";
            // 
            // txtArrivalGroudRoute
            // 
            this.txtArrivalGroudRoute.BackColor = System.Drawing.SystemColors.Control;
            this.txtArrivalGroudRoute.Location = new System.Drawing.Point(116, 386);
            this.txtArrivalGroudRoute.Multiline = true;
            this.txtArrivalGroudRoute.Name = "txtArrivalGroudRoute";
            this.txtArrivalGroudRoute.ReadOnly = true;
            this.txtArrivalGroudRoute.Size = new System.Drawing.Size(334, 60);
            this.txtArrivalGroudRoute.TabIndex = 28;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(15, 409);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(98, 13);
            this.label19.TabIndex = 27;
            this.label19.Text = "Ground Route : ";
            // 
            // txtIntersection
            // 
            this.txtIntersection.Location = new System.Drawing.Point(173, 225);
            this.txtIntersection.Name = "txtIntersection";
            this.txtIntersection.ReadOnly = true;
            this.txtIntersection.Size = new System.Drawing.Size(50, 20);
            this.txtIntersection.TabIndex = 24;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(84, 228);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(86, 13);
            this.label18.TabIndex = 23;
            this.label18.Text = "Intersection : ";
            // 
            // txtInboundGate
            // 
            this.txtInboundGate.Location = new System.Drawing.Point(400, 186);
            this.txtInboundGate.Name = "txtInboundGate";
            this.txtInboundGate.ReadOnly = true;
            this.txtInboundGate.Size = new System.Drawing.Size(50, 20);
            this.txtInboundGate.TabIndex = 22;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(264, 189);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(133, 13);
            this.label17.TabIndex = 21;
            this.label17.Text = "Inbound Gate Stand : ";
            // 
            // txtLandingClearance
            // 
            this.txtLandingClearance.Location = new System.Drawing.Point(173, 186);
            this.txtLandingClearance.Name = "txtLandingClearance";
            this.txtLandingClearance.ReadOnly = true;
            this.txtLandingClearance.Size = new System.Drawing.Size(50, 20);
            this.txtLandingClearance.TabIndex = 20;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 189);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(166, 13);
            this.label16.TabIndex = 19;
            this.label16.Text = "Landing Clearance Issued : ";
            // 
            // txtActualArrivalTime
            // 
            this.txtActualArrivalTime.Location = new System.Drawing.Point(400, 148);
            this.txtActualArrivalTime.Name = "txtActualArrivalTime";
            this.txtActualArrivalTime.ReadOnly = true;
            this.txtActualArrivalTime.Size = new System.Drawing.Size(50, 20);
            this.txtActualArrivalTime.TabIndex = 18;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(256, 151);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(141, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Actual Time of Arrival : ";
            // 
            // txtEstimatedArrivalTime
            // 
            this.txtEstimatedArrivalTime.Location = new System.Drawing.Point(400, 114);
            this.txtEstimatedArrivalTime.Name = "txtEstimatedArrivalTime";
            this.txtEstimatedArrivalTime.ReadOnly = true;
            this.txtEstimatedArrivalTime.Size = new System.Drawing.Size(50, 20);
            this.txtEstimatedArrivalTime.TabIndex = 16;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(238, 117);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(160, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "Estimated Time of Arrival : ";
            // 
            // txtScheduedArrivalTime
            // 
            this.txtScheduedArrivalTime.Location = new System.Drawing.Point(173, 114);
            this.txtScheduedArrivalTime.Name = "txtScheduedArrivalTime";
            this.txtScheduedArrivalTime.ReadOnly = true;
            this.txtScheduedArrivalTime.Size = new System.Drawing.Size(50, 20);
            this.txtScheduedArrivalTime.TabIndex = 14;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 117);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(165, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Scheduled Time of Arrival : ";
            // 
            // txtDepartAirport
            // 
            this.txtDepartAirport.Location = new System.Drawing.Point(378, 60);
            this.txtDepartAirport.Name = "txtDepartAirport";
            this.txtDepartAirport.ReadOnly = true;
            this.txtDepartAirport.Size = new System.Drawing.Size(72, 20);
            this.txtDepartAirport.TabIndex = 12;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(256, 63);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(116, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Departure Airport : ";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.OFTXDep);
            this.panel3.Controls.Add(this.label56);
            this.panel3.Controls.Add(this.VISCDep);
            this.panel3.Controls.Add(this.label55);
            this.panel3.Controls.Add(this.LIDRDep);
            this.panel3.Controls.Add(this.label54);
            this.panel3.Controls.Add(this.EXOTDep);
            this.panel3.Controls.Add(this.label52);
            this.panel3.Controls.Add(this.EETTDep);
            this.panel3.Controls.Add(this.label49);
            this.panel3.Controls.Add(this.label45);
            this.panel3.Controls.Add(this.ASTTDep);
            this.panel3.Controls.Add(this.AETTDep);
            this.panel3.Controls.Add(this.label44);
            this.panel3.Controls.Add(this.DeptxtAircraftType);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.DeptxtCallSign);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.cBoxParking);
            this.panel3.Controls.Add(this.cBoxAerodrome);
            this.panel3.Controls.Add(this.DeptxtFreqChange);
            this.panel3.Controls.Add(this.label40);
            this.panel3.Controls.Add(this.DeptxtCJS);
            this.panel3.Controls.Add(this.label41);
            this.panel3.Controls.Add(this.DeptxtRoute);
            this.panel3.Controls.Add(this.label39);
            this.panel3.Controls.Add(this.DeptxtFlno2);
            this.panel3.Controls.Add(this.DeptxtFlno1);
            this.panel3.Controls.Add(this.label38);
            this.panel3.Controls.Add(this.label37);
            this.panel3.Controls.Add(this.txtTsat);
            this.panel3.Controls.Add(this.label36);
            this.panel3.Controls.Add(this.txtWake);
            this.panel3.Controls.Add(this.txtClearanceIssued);
            this.panel3.Controls.Add(this.label34);
            this.panel3.Controls.Add(this.txtClearanceRequest);
            this.panel3.Controls.Add(this.label35);
            this.panel3.Controls.Add(this.txtTakeoffIssued);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.txtTaxiIssued);
            this.panel3.Controls.Add(this.label32);
            this.panel3.Controls.Add(this.txtTaxiRequest);
            this.panel3.Controls.Add(this.label33);
            this.panel3.Controls.Add(this.txtOutboundGate);
            this.panel3.Controls.Add(this.label31);
            this.panel3.Controls.Add(this.txtPushIssued);
            this.panel3.Controls.Add(this.label29);
            this.panel3.Controls.Add(this.txtPuchRequest);
            this.panel3.Controls.Add(this.label30);
            this.panel3.Controls.Add(this.txtDepartureUpdatedOn);
            this.panel3.Controls.Add(this.label25);
            this.panel3.Controls.Add(this.txtDepartureCreatedOn);
            this.panel3.Controls.Add(this.label26);
            this.panel3.Controls.Add(this.txtDepartureGroudRoute);
            this.panel3.Controls.Add(this.label27);
            this.panel3.Controls.Add(this.txtActualDepartureTime);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.txtEstimatedDepartureTime);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.txtScheduedDepartureTime);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.txtDesignationAirport);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Location = new System.Drawing.Point(473, 50);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(480, 660);
            this.panel3.TabIndex = 2;
            // 
            // OFTXDep
            // 
            this.OFTXDep.Location = new System.Drawing.Point(197, 592);
            this.OFTXDep.Name = "OFTXDep";
            this.OFTXDep.ReadOnly = true;
            this.OFTXDep.Size = new System.Drawing.Size(273, 20);
            this.OFTXDep.TabIndex = 89;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(5, 596);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(188, 13);
            this.label56.TabIndex = 88;
            this.label56.Text = "Off-Block to Start of Taxi Time :";
            // 
            // VISCDep
            // 
            this.VISCDep.Location = new System.Drawing.Point(181, 561);
            this.VISCDep.Name = "VISCDep";
            this.VISCDep.ReadOnly = true;
            this.VISCDep.Size = new System.Drawing.Size(289, 20);
            this.VISCDep.TabIndex = 87;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(55, 563);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(118, 13);
            this.label55.TabIndex = 86;
            this.label55.Text = "Visibility Condition :";
            // 
            // LIDRDep
            // 
            this.LIDRDep.Location = new System.Drawing.Point(181, 531);
            this.LIDRDep.Name = "LIDRDep";
            this.LIDRDep.ReadOnly = true;
            this.LIDRDep.Size = new System.Drawing.Size(289, 20);
            this.LIDRDep.TabIndex = 85;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(6, 533);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(168, 13);
            this.label54.TabIndex = 84;
            this.label54.Text = "Runway Landing Direction  :";
            // 
            // EXOTDep
            // 
            this.EXOTDep.Location = new System.Drawing.Point(286, 495);
            this.EXOTDep.Name = "EXOTDep";
            this.EXOTDep.ReadOnly = true;
            this.EXOTDep.Size = new System.Drawing.Size(185, 20);
            this.EXOTDep.TabIndex = 83;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(242, 498);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(44, 13);
            this.label52.TabIndex = 82;
            this.label52.Text = "EXOT:";
            // 
            // EETTDep
            // 
            this.EETTDep.Location = new System.Drawing.Point(48, 495);
            this.EETTDep.Name = "EETTDep";
            this.EETTDep.ReadOnly = true;
            this.EETTDep.Size = new System.Drawing.Size(185, 20);
            this.EETTDep.TabIndex = 81;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(2, 498);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(43, 13);
            this.label49.TabIndex = 80;
            this.label49.Text = "EETT:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(243, 467);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(43, 13);
            this.label45.TabIndex = 79;
            this.label45.Text = "ASTT:";
            // 
            // ASTTDep
            // 
            this.ASTTDep.Location = new System.Drawing.Point(286, 464);
            this.ASTTDep.Name = "ASTTDep";
            this.ASTTDep.ReadOnly = true;
            this.ASTTDep.Size = new System.Drawing.Size(185, 20);
            this.ASTTDep.TabIndex = 78;
            // 
            // AETTDep
            // 
            this.AETTDep.Location = new System.Drawing.Point(48, 464);
            this.AETTDep.Name = "AETTDep";
            this.AETTDep.ReadOnly = true;
            this.AETTDep.Size = new System.Drawing.Size(185, 20);
            this.AETTDep.TabIndex = 77;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(2, 467);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(43, 13);
            this.label44.TabIndex = 76;
            this.label44.Text = "AETT:";
            // 
            // DeptxtAircraftType
            // 
            this.DeptxtAircraftType.Location = new System.Drawing.Point(176, 35);
            this.DeptxtAircraftType.Name = "DeptxtAircraftType";
            this.DeptxtAircraftType.ReadOnly = true;
            this.DeptxtAircraftType.Size = new System.Drawing.Size(73, 20);
            this.DeptxtAircraftType.TabIndex = 75;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(70, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 74;
            this.label5.Text = "Aircraft Type : ";
            // 
            // DeptxtCallSign
            // 
            this.DeptxtCallSign.Location = new System.Drawing.Point(395, 9);
            this.DeptxtCallSign.Name = "DeptxtCallSign";
            this.DeptxtCallSign.ReadOnly = true;
            this.DeptxtCallSign.Size = new System.Drawing.Size(75, 20);
            this.DeptxtCallSign.TabIndex = 73;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(315, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 72;
            this.label3.Text = "Call Sign : ";
            // 
            // cBoxParking
            // 
            this.cBoxParking.AutoSize = true;
            this.cBoxParking.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxParking.Location = new System.Drawing.Point(318, 300);
            this.cBoxParking.Name = "cBoxParking";
            this.cBoxParking.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cBoxParking.Size = new System.Drawing.Size(154, 17);
            this.cBoxParking.TabIndex = 71;
            this.cBoxParking.Text = "Return Taxi to Parking";
            this.cBoxParking.UseVisualStyleBackColor = true;
            // 
            // cBoxAerodrome
            // 
            this.cBoxAerodrome.AutoSize = true;
            this.cBoxAerodrome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxAerodrome.Location = new System.Drawing.Point(106, 300);
            this.cBoxAerodrome.Name = "cBoxAerodrome";
            this.cBoxAerodrome.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cBoxAerodrome.Size = new System.Drawing.Size(143, 17);
            this.cBoxAerodrome.TabIndex = 70;
            this.cBoxAerodrome.Text = "Return to Aerodrome";
            this.cBoxAerodrome.UseVisualStyleBackColor = true;
            // 
            // DeptxtFreqChange
            // 
            this.DeptxtFreqChange.Location = new System.Drawing.Point(420, 270);
            this.DeptxtFreqChange.Name = "DeptxtFreqChange";
            this.DeptxtFreqChange.ReadOnly = true;
            this.DeptxtFreqChange.Size = new System.Drawing.Size(50, 20);
            this.DeptxtFreqChange.TabIndex = 69;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(290, 273);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(125, 13);
            this.label40.TabIndex = 68;
            this.label40.Text = "Frequency Change : ";
            // 
            // DeptxtCJS
            // 
            this.DeptxtCJS.Location = new System.Drawing.Point(176, 271);
            this.DeptxtCJS.Name = "DeptxtCJS";
            this.DeptxtCJS.ReadOnly = true;
            this.DeptxtCJS.Size = new System.Drawing.Size(73, 20);
            this.DeptxtCJS.TabIndex = 67;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(132, 274);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(41, 13);
            this.label41.TabIndex = 66;
            this.label41.Text = "CJS : ";
            // 
            // DeptxtRoute
            // 
            this.DeptxtRoute.Location = new System.Drawing.Point(124, 323);
            this.DeptxtRoute.Multiline = true;
            this.DeptxtRoute.Name = "DeptxtRoute";
            this.DeptxtRoute.ReadOnly = true;
            this.DeptxtRoute.Size = new System.Drawing.Size(334, 60);
            this.DeptxtRoute.TabIndex = 65;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(68, 345);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(53, 13);
            this.label39.TabIndex = 64;
            this.label39.Text = "Route : ";
            // 
            // DeptxtFlno2
            // 
            this.DeptxtFlno2.BackColor = System.Drawing.SystemColors.Control;
            this.DeptxtFlno2.Location = new System.Drawing.Point(204, 9);
            this.DeptxtFlno2.Name = "DeptxtFlno2";
            this.DeptxtFlno2.ReadOnly = true;
            this.DeptxtFlno2.Size = new System.Drawing.Size(45, 20);
            this.DeptxtFlno2.TabIndex = 63;
            // 
            // DeptxtFlno1
            // 
            this.DeptxtFlno1.BackColor = System.Drawing.SystemColors.Control;
            this.DeptxtFlno1.Location = new System.Drawing.Point(158, 10);
            this.DeptxtFlno1.Name = "DeptxtFlno1";
            this.DeptxtFlno1.ReadOnly = true;
            this.DeptxtFlno1.Size = new System.Drawing.Size(40, 20);
            this.DeptxtFlno1.TabIndex = 62;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(82, 13);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(70, 13);
            this.label38.TabIndex = 61;
            this.label38.Text = "Flight No : ";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(367, 214);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(48, 13);
            this.label37.TabIndex = 60;
            this.label37.Text = "Wake: ";
            // 
            // txtTsat
            // 
            this.txtTsat.Location = new System.Drawing.Point(420, 237);
            this.txtTsat.Name = "txtTsat";
            this.txtTsat.ReadOnly = true;
            this.txtTsat.Size = new System.Drawing.Size(50, 20);
            this.txtTsat.TabIndex = 59;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(368, 240);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(47, 13);
            this.label36.TabIndex = 58;
            this.label36.Text = "TSAT: ";
            // 
            // txtWake
            // 
            this.txtWake.Location = new System.Drawing.Point(420, 211);
            this.txtWake.Name = "txtWake";
            this.txtWake.ReadOnly = true;
            this.txtWake.Size = new System.Drawing.Size(50, 20);
            this.txtWake.TabIndex = 57;
            // 
            // txtClearanceIssued
            // 
            this.txtClearanceIssued.Location = new System.Drawing.Point(420, 178);
            this.txtClearanceIssued.Name = "txtClearanceIssued";
            this.txtClearanceIssued.ReadOnly = true;
            this.txtClearanceIssued.Size = new System.Drawing.Size(50, 20);
            this.txtClearanceIssued.TabIndex = 56;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(297, 181);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(117, 13);
            this.label34.TabIndex = 55;
            this.label34.Text = "Clearance Issued : ";
            // 
            // txtClearanceRequest
            // 
            this.txtClearanceRequest.Location = new System.Drawing.Point(420, 149);
            this.txtClearanceRequest.Name = "txtClearanceRequest";
            this.txtClearanceRequest.ReadOnly = true;
            this.txtClearanceRequest.Size = new System.Drawing.Size(50, 20);
            this.txtClearanceRequest.TabIndex = 54;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(288, 153);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(127, 13);
            this.label35.TabIndex = 53;
            this.label35.Text = "Clearance Request : ";
            // 
            // txtTakeoffIssued
            // 
            this.txtTakeoffIssued.Location = new System.Drawing.Point(176, 245);
            this.txtTakeoffIssued.Name = "txtTakeoffIssued";
            this.txtTakeoffIssued.ReadOnly = true;
            this.txtTakeoffIssued.Size = new System.Drawing.Size(50, 20);
            this.txtTakeoffIssued.TabIndex = 52;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(1, 248);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(173, 13);
            this.label28.TabIndex = 51;
            this.label28.Text = "Take-off  Clearance Issued : ";
            // 
            // txtTaxiIssued
            // 
            this.txtTaxiIssued.Location = new System.Drawing.Point(176, 212);
            this.txtTaxiIssued.Name = "txtTaxiIssued";
            this.txtTaxiIssued.ReadOnly = true;
            this.txtTaxiIssued.Size = new System.Drawing.Size(50, 20);
            this.txtTaxiIssued.TabIndex = 50;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(29, 215);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(145, 13);
            this.label32.TabIndex = 49;
            this.label32.Text = "Taxi Clearance Issued : ";
            // 
            // txtTaxiRequest
            // 
            this.txtTaxiRequest.Location = new System.Drawing.Point(176, 181);
            this.txtTaxiRequest.Name = "txtTaxiRequest";
            this.txtTaxiRequest.ReadOnly = true;
            this.txtTaxiRequest.Size = new System.Drawing.Size(50, 20);
            this.txtTaxiRequest.TabIndex = 48;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(19, 184);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(155, 13);
            this.label33.TabIndex = 47;
            this.label33.Text = "Taxi Clearance Request : ";
            // 
            // txtOutboundGate
            // 
            this.txtOutboundGate.Location = new System.Drawing.Point(420, 121);
            this.txtOutboundGate.Name = "txtOutboundGate";
            this.txtOutboundGate.ReadOnly = true;
            this.txtOutboundGate.Size = new System.Drawing.Size(50, 20);
            this.txtOutboundGate.TabIndex = 46;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(279, 124);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(142, 13);
            this.label31.TabIndex = 45;
            this.label31.Text = "Outbound Gate Stand : ";
            // 
            // txtPushIssued
            // 
            this.txtPushIssued.Location = new System.Drawing.Point(176, 150);
            this.txtPushIssued.Name = "txtPushIssued";
            this.txtPushIssued.ReadOnly = true;
            this.txtPushIssued.Size = new System.Drawing.Size(50, 20);
            this.txtPushIssued.TabIndex = 42;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(58, 153);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(116, 13);
            this.label29.TabIndex = 41;
            this.label29.Text = "Pushback Issued : ";
            // 
            // txtPuchRequest
            // 
            this.txtPuchRequest.Location = new System.Drawing.Point(176, 121);
            this.txtPuchRequest.Name = "txtPuchRequest";
            this.txtPuchRequest.ReadOnly = true;
            this.txtPuchRequest.Size = new System.Drawing.Size(50, 20);
            this.txtPuchRequest.TabIndex = 40;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(48, 124);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(126, 13);
            this.label30.TabIndex = 39;
            this.label30.Text = "Pushback Request : ";
            // 
            // txtDepartureUpdatedOn
            // 
            this.txtDepartureUpdatedOn.Location = new System.Drawing.Point(329, 629);
            this.txtDepartureUpdatedOn.Name = "txtDepartureUpdatedOn";
            this.txtDepartureUpdatedOn.ReadOnly = true;
            this.txtDepartureUpdatedOn.Size = new System.Drawing.Size(100, 20);
            this.txtDepartureUpdatedOn.TabIndex = 38;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(240, 632);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 13);
            this.label25.TabIndex = 37;
            this.label25.Text = "Updated On : ";
            // 
            // txtDepartureCreatedOn
            // 
            this.txtDepartureCreatedOn.Location = new System.Drawing.Point(108, 629);
            this.txtDepartureCreatedOn.Name = "txtDepartureCreatedOn";
            this.txtDepartureCreatedOn.ReadOnly = true;
            this.txtDepartureCreatedOn.Size = new System.Drawing.Size(100, 20);
            this.txtDepartureCreatedOn.TabIndex = 36;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(23, 632);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(83, 13);
            this.label26.TabIndex = 35;
            this.label26.Text = "Created On : ";
            // 
            // txtDepartureGroudRoute
            // 
            this.txtDepartureGroudRoute.BackColor = System.Drawing.SystemColors.Control;
            this.txtDepartureGroudRoute.Location = new System.Drawing.Point(124, 389);
            this.txtDepartureGroudRoute.Multiline = true;
            this.txtDepartureGroudRoute.Name = "txtDepartureGroudRoute";
            this.txtDepartureGroudRoute.ReadOnly = true;
            this.txtDepartureGroudRoute.Size = new System.Drawing.Size(334, 60);
            this.txtDepartureGroudRoute.TabIndex = 34;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(23, 412);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(98, 13);
            this.label27.TabIndex = 33;
            this.label27.Text = "Ground Route : ";
            // 
            // txtActualDepartureTime
            // 
            this.txtActualDepartureTime.Location = new System.Drawing.Point(420, 95);
            this.txtActualDepartureTime.Name = "txtActualDepartureTime";
            this.txtActualDepartureTime.ReadOnly = true;
            this.txtActualDepartureTime.Size = new System.Drawing.Size(50, 20);
            this.txtActualDepartureTime.TabIndex = 22;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(261, 99);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(161, 13);
            this.label24.TabIndex = 21;
            this.label24.Text = "Actual Time of Departure : ";
            // 
            // txtEstimatedDepartureTime
            // 
            this.txtEstimatedDepartureTime.Location = new System.Drawing.Point(420, 68);
            this.txtEstimatedDepartureTime.Name = "txtEstimatedDepartureTime";
            this.txtEstimatedDepartureTime.ReadOnly = true;
            this.txtEstimatedDepartureTime.Size = new System.Drawing.Size(50, 20);
            this.txtEstimatedDepartureTime.TabIndex = 20;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(241, 71);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(180, 13);
            this.label22.TabIndex = 19;
            this.label22.Text = "Estimated Time of Departure : ";
            // 
            // txtScheduedDepartureTime
            // 
            this.txtScheduedDepartureTime.Location = new System.Drawing.Point(177, 71);
            this.txtScheduedDepartureTime.Name = "txtScheduedDepartureTime";
            this.txtScheduedDepartureTime.ReadOnly = true;
            this.txtScheduedDepartureTime.Size = new System.Drawing.Size(50, 20);
            this.txtScheduedDepartureTime.TabIndex = 18;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(0, 73);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(185, 13);
            this.label23.TabIndex = 17;
            this.label23.Text = "Scheduled Time of Deaprture : ";
            // 
            // txtDesignationAirport
            // 
            this.txtDesignationAirport.Location = new System.Drawing.Point(395, 39);
            this.txtDesignationAirport.Name = "txtDesignationAirport";
            this.txtDesignationAirport.ReadOnly = true;
            this.txtDesignationAirport.Size = new System.Drawing.Size(75, 20);
            this.txtDesignationAirport.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(269, 41);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(124, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Destination Airport : ";
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(871, 717);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(61, 23);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 746);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panelFlight);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "EFPS - Data Viewer";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.panelFlight.ResumeLayout(false);
            this.panelFlight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelFlight;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox cBoxCircuit;
        private System.Windows.Forms.CheckBox cBoxTraining;
        private System.Windows.Forms.TextBox txtTCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtFlightAltitude;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox txtDepartAirport;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtDesignationAirport;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtScheduedArrivalTime;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtEstimatedArrivalTime;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtActualArrivalTime;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtLandingClearance;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtInboundGate;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtIntersection;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtArrivalGroudRoute;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtArrivalUpdatedOn;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtArrivalCreatedOn;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtEstimatedDepartureTime;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtScheduedDepartureTime;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtActualDepartureTime;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtDepartureUpdatedOn;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtDepartureCreatedOn;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtDepartureGroudRoute;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtPushIssued;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtPuchRequest;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtOutboundGate;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtTakeoffIssued;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtTaxiIssued;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtTaxiRequest;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtClearanceIssued;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtClearanceRequest;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtTsat;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtWake;
        private AxAATLOGINLib.AxAatLogin LoginControl;
        private System.Windows.Forms.TextBox ArrtxtFlno2;
        private System.Windows.Forms.TextBox ArrtxtFlno1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox DeptxtFlno2;
        private System.Windows.Forms.TextBox DeptxtFlno1;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox ArrtxtRoute;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox DeptxtRoute;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox ArrtxtFreqChange;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox ArrtxtCJS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox DeptxtFreqChange;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox DeptxtCJS;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox ArrtxtCallSign;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox ArrtxtAircraftType;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtMissedTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox cBoxParking;
        private System.Windows.Forms.CheckBox cBoxAerodrome;
        private System.Windows.Forms.TextBox DeptxtAircraftType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox DeptxtCallSign;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox AETTDep;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox ASSTArr;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox AETTArr;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox ASTTDep;
        private System.Windows.Forms.TextBox EETTArr;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox EETTDep;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox LIDRArr;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox EXITArr;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox VISCArr;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox EXOTDep;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox OFTXDep;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox VISCDep;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox LIDRDep;
        private System.Windows.Forms.Label label54;

    }
}