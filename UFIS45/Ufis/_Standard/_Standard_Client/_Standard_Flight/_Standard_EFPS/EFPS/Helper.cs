﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace EFPS
{
    public class Helper
    {
        public static DateTime StandardDateTime
        {
            get;
            set;
        }

        public static DateTime DateTimetoUFISDateTime(string sDateTime, string sDateFormat, int iUTCOffset)
        {
            return DateTime.ParseExact(sDateTime, sDateFormat, CultureInfo.InvariantCulture);
        }

        public static string DateTimetoUFISDateTime(DateTime dtDateTime, int iUTCOffset)
        {
            dtDateTime = dtDateTime.AddHours(iUTCOffset);
            return dtDateTime.ToString("dd.MM.yyyy HH':'mm");
        }

        public static string DateTimetoUFISTime(DateTime dtDateTime, int iUTCOffset)
        {
            dtDateTime = dtDateTime.AddHours(iUTCOffset);
            return dtDateTime.ToString("HH':'mm");
        }

        public static string DateTimetoUFISTime(DateTime dtDateTime)
        {
            TimeSpan timeDiff = dtDateTime.Date - StandardDateTime.Date;
            string time = string.Empty;

            if (timeDiff.Days >-1)
                if (timeDiff.Days == 0)
                    time = dtDateTime.Hour.ToString().PadLeft(2, '0') + ":" + dtDateTime.Minute.ToString().PadLeft(2, '0');
                else
                    time = dtDateTime.Hour.ToString().PadLeft(2, '0') + ":" + dtDateTime.Minute.ToString().PadLeft(2, '0') + "+" + timeDiff.Days.ToString();
            else
                time = dtDateTime.Hour.ToString().PadLeft(2, '0') + ":" + dtDateTime.Minute.ToString().PadLeft(2, '0') + timeDiff.Days.ToString(); 

            return time;
        }

        public static string DateTimetoUFISDateTime(DateTime dtDateTime)
        {
            return dtDateTime.ToString("dd.MM.yyyy HH':'mm");
        }
    }
}
