VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTelex 
   Caption         =   "Telex of Selected Entry"
   ClientHeight    =   4995
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6375
   Icon            =   "frmTelex.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4995
   ScaleWidth      =   6375
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin TABLib.TAB TlxTab 
      Height          =   855
      Index           =   0
      Left            =   3630
      TabIndex        =   2
      Top             =   510
      Visible         =   0   'False
      Width           =   1725
      _Version        =   65536
      _ExtentX        =   3043
      _ExtentY        =   1508
      _StockProps     =   64
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   1
      Top             =   4680
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10716
         EndProperty
      EndProperty
   End
   Begin VB.TextBox txtTelex 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   30
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   30
      Width           =   3165
   End
   Begin TABLib.TAB TlxTab 
      Height          =   855
      Index           =   1
      Left            =   3630
      TabIndex        =   3
      Top             =   1530
      Visible         =   0   'False
      Width           =   1725
      _Version        =   65536
      _ExtentX        =   3043
      _ExtentY        =   1508
      _StockProps     =   64
   End
End
Attribute VB_Name = "frmTelex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    txtTelex.FontSize = MyFontSize - 6
    txtTelex.FontBold = MyFontBold
    TlxTab(0).ResetContent
    TlxTab(1).ResetContent
End Sub
Public Sub TabReadTelex(TlxTabUrno As String)
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim tmpText As String
    Dim tmpSqlKey As String
    Dim LineNo As Long
    If TlxTabUrno <> strTLX_URNO Then
        strTLX_URNO = TlxTabUrno
        tmpText = ""
        tmpData = TlxTab(1).GetLinesByColumnValue(0, TlxTabUrno, 0)
        If tmpData <> "" Then
            LineNo = Val(tmpData)
            tmpText = TlxTab(1).GetColumnValue(LineNo, 1)
        Else
            TlxTab(0).ResetContent
            TlxTab(0).CedaServerName = strServer
            TlxTab(0).CedaPort = "3357"
            TlxTab(0).CedaHopo = strHOPO
            TlxTab(0).CedaCurrentApplication = "LoaTabViewer" & "," & UfisTools.GetApplVersion(True)
            TlxTab(0).CedaTabext = "TAB"
            TlxTab(0).CedaUser = GetWindowsUserName()
            TlxTab(0).CedaWorkstation = GetWorkStationName()
            TlxTab(0).CedaSendTimeout = "3"
            TlxTab(0).CedaReceiveTimeout = "240"
            TlxTab(0).CedaRecordSeparator = Chr(10)
            TlxTab(0).CedaIdentifier = "LoaTab"
            tmpCmd = "RT"
            tmpTable = "TLXTAB"
            tmpFields = "TXT1,TXT2"
            tmpData = ""
            tmpSqlKey = "WHERE URNO=" & strTLX_URNO
            If strServer <> "LOCAL" Then
                Screen.MousePointer = 11
                TlxTab(0).CedaAction tmpCmd, tmpTable, tmpFields, tmpData, tmpSqlKey
                Screen.MousePointer = 0
                tmpText = TlxTab(0).GetColumnValue(0, 0) & TlxTab(0).GetColumnValue(0, 1)
            End If
            If tmpText = "" Then tmpText = "Telex not found. (TLXTAB.URNO=" & TlxTabUrno & ")"
            TlxTab(1).InsertTextLine TlxTabUrno & "," & tmpText, False
        End If
        tmpText = CleanString(tmpText, FOR_CLIENT, True)
        txtTelex.Text = tmpText
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = True
        frmMain.chkTelex.value = 0
        Me.Hide
    End If
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - txtTelex.Left * 2
    If NewSize > 15 Then txtTelex.Width = NewSize
    NewSize = Me.ScaleHeight - StatusBar1.Height - 60
    If NewSize > 15 Then txtTelex.Height = NewSize
End Sub

