VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{6FBA474E-43AC-11CE-9A0E-00AA0062BB4C}#1.0#0"; "SYSINFO.ocx"
Begin VB.Form frmMain 
   Caption         =   "PAX And Loading Information"
   ClientHeight    =   8640
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   12405
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8640
   ScaleWidth      =   12405
   StartUpPosition =   3  'Windows Default
   Begin SysInfoLib.SysInfo SysInfo 
      Left            =   5820
      Top             =   2040
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   3450
      Top             =   1230
   End
   Begin VB.Frame fraTopPanel 
      Caption         =   "Flight Information"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Index           =   0
      Left            =   30
      TabIndex        =   38
      Tag             =   "VISIBLE"
      Top             =   60
      Width           =   11985
      Begin VB.TextBox txtFLNO 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   630
         Locked          =   -1  'True
         TabIndex        =   46
         TabStop         =   0   'False
         Top             =   270
         Width           =   1230
      End
      Begin VB.TextBox txtACT 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2850
         Locked          =   -1  'True
         TabIndex        =   45
         TabStop         =   0   'False
         Top             =   270
         Width           =   675
      End
      Begin VB.TextBox txtREGN 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   4560
         Locked          =   -1  'True
         TabIndex        =   44
         TabStop         =   0   'False
         Top             =   270
         Width           =   1620
      End
      Begin VB.TextBox txtADID 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   630
         Locked          =   -1  'True
         TabIndex        =   43
         TabStop         =   0   'False
         Top             =   600
         Width           =   1230
      End
      Begin VB.TextBox txtORIGDEST 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2850
         Locked          =   -1  'True
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   600
         Width           =   675
      End
      Begin VB.TextBox txtSched 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   4560
         Locked          =   -1  'True
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   600
         Width           =   1620
      End
      Begin VB.TextBox txtEstimated 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   7125
         Locked          =   -1  'True
         TabIndex        =   40
         TabStop         =   0   'False
         Top             =   600
         Width           =   1620
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Show Empty Values"
         Height          =   240
         Left            =   9750
         TabIndex        =   39
         Top             =   630
         Visible         =   0   'False
         Width           =   2130
      End
      Begin VB.Label Label2 
         Caption         =   "Flight:"
         Height          =   225
         Left            =   120
         TabIndex        =   53
         Top             =   315
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "A/C:"
         Height          =   225
         Left            =   2460
         TabIndex        =   52
         Top             =   315
         Width           =   435
      End
      Begin VB.Label Label4 
         Caption         =   "Reg.No.:"
         Height          =   225
         Left            =   3750
         TabIndex        =   51
         Top             =   315
         Width           =   675
      End
      Begin VB.Label Label5 
         Caption         =   "Type:"
         Height          =   225
         Left            =   120
         TabIndex        =   50
         Top             =   645
         Width           =   735
      End
      Begin VB.Label Label6 
         Caption         =   "Orig/Dest:"
         Height          =   225
         Left            =   2070
         TabIndex        =   49
         Top             =   645
         Width           =   795
      End
      Begin VB.Label Label7 
         Caption         =   "Scheduled:"
         Height          =   225
         Left            =   3720
         TabIndex        =   48
         Top             =   645
         Width           =   855
      End
      Begin VB.Label Label8 
         Caption         =   "Estimated:"
         Height          =   225
         Left            =   6345
         TabIndex        =   47
         Top             =   645
         Width           =   855
      End
   End
   Begin VB.Frame fraButtonPanel 
      Height          =   555
      Left            =   60
      TabIndex        =   29
      Top             =   3600
      Width           =   10695
      Begin VB.Frame fraRightButtons 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   3240
         TabIndex        =   32
         Top             =   180
         Width           =   2505
         Begin VB.CheckBox chkType 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   33
            Top             =   0
            Visible         =   0   'False
            Width           =   645
         End
      End
      Begin VB.Frame fraLeftButtons 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   90
         TabIndex        =   30
         Top             =   180
         Width           =   2505
         Begin VB.CheckBox chkDssn 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   31
            Top             =   0
            Visible         =   0   'False
            Width           =   645
         End
      End
      Begin VB.Line panelLine 
         BorderColor     =   &H00FFFFFF&
         Index           =   1
         X1              =   0
         X2              =   1125
         Y1              =   15
         Y2              =   15
      End
      Begin VB.Line panelLine 
         BorderColor     =   &H80000010&
         Index           =   0
         X1              =   0
         X2              =   1125
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Frame fraDetails 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   2850
      TabIndex        =   21
      Top             =   2670
      Width           =   1500
      Begin VB.CheckBox chkDetail 
         Caption         =   "P4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   1125
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   0
         Value           =   2  'Grayed
         Width           =   375
      End
      Begin VB.CheckBox chkDetail 
         Caption         =   "P3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   750
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   0
         Value           =   2  'Grayed
         Width           =   375
      End
      Begin VB.CheckBox chkDetail 
         Caption         =   "P2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   375
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   0
         Value           =   2  'Grayed
         Width           =   375
      End
      Begin VB.CheckBox chkDetail 
         Caption         =   "P1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   0
         Value           =   2  'Grayed
         Width           =   375
      End
   End
   Begin VB.Frame fraBottomPanel 
      BorderStyle     =   0  'None
      Height          =   400
      Left            =   0
      TabIndex        =   20
      Top             =   6720
      Width           =   7785
      Begin VB.CheckBox chkInitialLoad 
         Caption         =   "InitialLoad"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   360
         Style           =   1  'Graphical
         TabIndex        =   59
         Top             =   0
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CheckBox chkSave 
         Caption         =   "Save"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1750
         Style           =   1  'Graphical
         TabIndex        =   58
         Top             =   30
         Width           =   795
      End
      Begin VB.CheckBox chkAbout 
         Caption         =   "About"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5790
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   30
         Width           =   795
      End
      Begin VB.CheckBox chkPrint 
         Caption         =   "Print"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4995
         Style           =   1  'Graphical
         TabIndex        =   56
         Top             =   30
         Width           =   795
      End
      Begin VB.CheckBox chkTelex 
         Caption         =   "Telex"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3405
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   30
         Width           =   795
      End
      Begin VB.CheckBox chkSetup 
         Caption         =   "Setup"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2550
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   30
         Width           =   855
      End
      Begin VB.CheckBox chkClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6585
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   30
         Width           =   795
      End
      Begin VB.CheckBox chkLoad 
         Caption         =   "Refresh"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4200
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   30
         Width           =   795
      End
      Begin VB.CheckBox OnTop 
         Caption         =   "On Top"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2550
         Style           =   1  'Graphical
         TabIndex        =   55
         Top             =   30
         Width           =   795
      End
   End
   Begin MSComctlLib.StatusBar MyStatusBar 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   19
      Top             =   8325
      Width           =   12405
      _ExtentX        =   21881
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   21352
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraListPanel 
      Height          =   1665
      Left            =   30
      TabIndex        =   18
      Top             =   4890
      Width           =   4845
      Begin TABLib.TAB DataTab 
         Height          =   1365
         Index           =   0
         Left            =   90
         TabIndex        =   25
         Top             =   180
         Width           =   4665
         _Version        =   65536
         _ExtentX        =   8229
         _ExtentY        =   2408
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraFolderToggles 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   2070
      TabIndex        =   13
      Top             =   1200
      Width           =   1170
      Begin VB.CheckBox chkToggle 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   915
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   195
         Width           =   180
      End
      Begin VB.CheckBox chkToggle 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   915
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   555
         Value           =   2  'Grayed
         Width           =   180
      End
      Begin VB.HScrollBar FolderScroll 
         Height          =   285
         Left            =   90
         TabIndex        =   15
         Top             =   195
         Width           =   795
      End
      Begin VB.CheckBox chkScroll 
         Caption         =   "Hidden"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   555
         Width           =   795
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H00FFFFFF&
         Index           =   22
         X1              =   30
         X2              =   1155
         Y1              =   510
         Y2              =   510
      End
   End
   Begin VB.Frame UfisFolder 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1995
      Left            =   30
      TabIndex        =   0
      Top             =   1200
      Width           =   1965
      Begin VB.Frame fraFolder 
         BackColor       =   &H00404040&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Index           =   0
         Left            =   360
         TabIndex        =   8
         Top             =   240
         Visible         =   0   'False
         Width           =   795
         Begin VB.Frame fraBlend 
            BackColor       =   &H000000FF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   60
            Index           =   0
            Left            =   0
            TabIndex        =   12
            Top             =   480
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.CheckBox chkFolder 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   11
            Top             =   60
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.CheckBox chkTabs 
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   10
            Top             =   600
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.Frame fraLine 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   15
            Index           =   0
            Left            =   0
            TabIndex        =   9
            Top             =   420
            Visible         =   0   'False
            Width           =   645
         End
      End
      Begin VB.Frame fraFolderLine 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   15
         Index           =   0
         Left            =   0
         TabIndex        =   7
         Top             =   210
         Visible         =   0   'False
         Width           =   1965
      End
      Begin VB.Frame fraRightLine 
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   0
         Left            =   1590
         TabIndex        =   6
         Top             =   1320
         Visible         =   0   'False
         Width           =   45
      End
      Begin VB.Frame fraLeftLine 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   1230
         TabIndex        =   5
         Top             =   1320
         Visible         =   0   'False
         Width           =   45
      End
      Begin VB.Frame fraLeftLine2 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   1290
         TabIndex        =   4
         Top             =   1320
         Visible         =   0   'False
         Width           =   45
      End
      Begin VB.Frame fraRightLine2 
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   0
         Left            =   1530
         TabIndex        =   3
         Top             =   1320
         Visible         =   0   'False
         Width           =   45
      End
      Begin VB.Frame fraFolderLine 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   15
         Index           =   1
         Left            =   0
         TabIndex        =   2
         Top             =   870
         Width           =   1965
      End
      Begin VB.Frame fraFolderLine 
         BackColor       =   &H80000010&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   15
         Index           =   2
         Left            =   0
         TabIndex        =   1
         Top             =   810
         Width           =   1965
      End
   End
   Begin VB.Frame fraUrnoPanel 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   630
      TabIndex        =   35
      Top             =   7230
      Width           =   2115
      Begin VB.TextBox txtAFTURNO 
         Height          =   285
         Left            =   1020
         TabIndex        =   36
         Text            =   "11"
         ToolTipText     =   "URNO of AFTTAB"
         Top             =   30
         Width           =   1035
      End
      Begin VB.Label Label1 
         Caption         =   "Flight Urno:"
         Height          =   255
         Left            =   0
         TabIndex        =   37
         Top             =   60
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim TabsCount As Integer
Dim CurrentTab As Integer
Dim AllTypesCol As Long
Dim OtherTypesCol As Long
Dim FolderCaptions As String
Dim TelexTypeList As String
Dim TypeActionList As String
Dim ShowTabButtons As Boolean
Dim ChkTabsIsBusy As Boolean
Dim PrepareTabIsBusy As Boolean

Private UserName As String 'igu on 24/05/2011
Private UpdateRemarkWithUserName As Boolean 'igu on 24/05/2011

Private Sub InitDataTab(Index As Integer)
    Dim EntryFound As Boolean
    Dim TabCfg As String
    Dim CurKey As String
    Dim CurHead As String
    Dim CurDssn As String
    Dim CurType As String
    Dim CurAlign As String
    Dim CurWidth As String
    Dim CurLength As String
    Dim CurFields As String
    Dim CurData As String
    Dim CurDssnList As String
    Dim CurTypeList As String
    Dim FoundDssn As String
    Dim FoundType As String
    Dim tmpData As String
    Dim LineNo As Long
    Dim ColNo As Long
    Dim ReadOnce As Boolean
    Dim i As Integer
    Dim j As Integer
    
    If Index >= 0 Then TabCfg = fraFolder(Index).Tag Else TabCfg = "Data"
    CurFields = GetRealItem(TabCfg, 3, "|")
    CurFields = Replace(CurFields, ";", ",", 1, -1, vbBinaryCompare)
    
    CurHead = GetRealItem(TabCfg, 0, "|")
    If CurHead = "" Then
        CurHead = BuildHeaderFromFields(CurFields)
    Else
        CurHead = Replace(CurHead, ";", ",", 1, -1, vbBinaryCompare)
    End If
    CurHead = CurHead & "," & Space(1000)
    CurHead = CurHead & ",RURN"
    CurAlign = BuildAlignListFromFields(CurFields) & ",L,L"
    CurLength = BuildLengthListFromFields(CurFields) & ",10,10"
    CurWidth = BuildWidthListFromFields(CurFields) & ",100,100"
    DataTab(0).ResetContent
    DataTab(0).HeaderFontSize = MyFontSize
    DataTab(0).fontSize = MyFontSize
    DataTab(0).LineHeight = MyFontSize
    DataTab(0).SetTabFontBold MyFontBold
    DataTab(0).HeaderString = CurHead
    DataTab(0).HeaderAlignmentString = CurAlign
    DataTab(0).ColumnAlignmentString = CurAlign
    DataTab(0).HeaderLengthString = CurWidth
    DataTab(0).ColumnWidthString = CurLength
    DataTab(0).LogicalFieldList = CurFields & ",NONE,RURN"
    SetDateColumns DataTab(0), CurFields 'igu on 29/11/2011
    DataTab(0).AutoSizeByHeader = True
    DataTab(0).AutoSizeColumns
    If Index >= 0 Then
        CurDssnList = fraLeftButtons.Tag
        CurTypeList = fraRightButtons.Tag
        
        ColNo = CLng(GetRealItemNo(frmHiddenData.tabData(idxLOATAB).LogicalFieldList, "RFLD"))
        frmHiddenData.tabData(idxLOATAB).IndexDestroy "FolderData"
        frmHiddenData.tabData(idxLOATAB).IndexCreate "FolderData", ColNo
        
        frmHiddenData.tabData(idxLOATAB).SetInternalLineBuffer True
        FoundDssn = ","
        FoundType = ","
        ReadOnce = False
        j = chkDetail.UBound
        For i = 0 To j
            If chkDetail(i).Value = 1 Then
                EntryFound = False
                If Not ReadOnce Then
                    CurKey = CStr(i + 1) & fraBlend(Index).Tag
                    If Index = OtherTypesCol Then CurKey = "??"
                    If Index = AllTypesCol Then
                        CurKey = CStr(i + 1)
                        LineNo = Val(frmHiddenData.tabData(idxLOATAB).GetLinesByIndexValue("FolderData", CurKey, 2))
                    Else
                        LineNo = Val(frmHiddenData.tabData(idxLOATAB).GetLinesByIndexValue("FolderData", CurKey, 0))
                    End If
                    While LineNo >= 0
                        LineNo = frmHiddenData.tabData(idxLOATAB).GetNextResultLine
                        If LineNo >= 0 Then
                            CurData = frmHiddenData.tabData(idxLOATAB).GetFieldValues(LineNo, CurFields)
                            CurDssn = frmHiddenData.tabData(idxLOATAB).GetFieldValue(LineNo, "DSSN")
                            CurType = frmHiddenData.tabData(idxLOATAB).GetFieldValue(LineNo, "TYPE")
                            If (InStr(CurDssnList, CurDssn) > 0) And (InStr(CurTypeList, CurType) > 0) Then
                                CurData = CurData & ",," & frmHiddenData.tabData(idxLOATAB).GetFieldValue(LineNo, "RURN")
                                CurData = Replace(CurData, "#", " ", 1, -1, vbBinaryCompare)
                                DataTab(0).InsertTextLine CurData, False
                                If InStr(FoundDssn, CurDssn) = 0 Then FoundDssn = FoundDssn & CurDssn & ","
                                If InStr(FoundType, CurDssn) = 0 Then FoundType = FoundType & CurType & ","
                                EntryFound = True
                            End If
                        End If
                    Wend
                End If
                If Index = OtherTypesCol Then ReadOnce = True
                If (EntryFound) And (Not ReadOnce) Then chkDetail(i).BackColor = LightGreen Else chkDetail(i).BackColor = vbButtonFace
            End If
        Next
        For i = 0 To fraFolder.UBound
            CurKey = fraBlend(i).Tag
            LineNo = Val(frmHiddenData.tabData(idxLOATAB).GetLinesByColumnValue(ColNo, CurKey, 1))
            If LineNo > 0 Then chkTabs(i).Caption = CStr(LineNo) Else chkTabs(i).Caption = ""
        Next
'        If Index = OtherTypesCol Then
'            LineNo = Val(frmHiddenData.tabData(idxLOATAB).GetLinesByIndexValue("FolderData", "??", 0))
'            If LineNo > 0 Then chkTabs(OtherTypesCol).Caption = CStr(LineNo) Else chkTabs(OtherTypesCol).Caption = ""
'        End If
        frmHiddenData.tabData(idxLOATAB).SetInternalLineBuffer False
        For i = 0 To chkDssn.UBound
            If InStr(FoundDssn, chkDssn(i).Caption) > 0 Then chkDssn(i).BackColor = LightGreen Else chkDssn(i).BackColor = vbButtonFace
        Next
        For i = 0 To chkType.UBound
            If InStr(FoundType, chkType(i).Caption) > 0 Then chkType(i).BackColor = LightGreen Else chkType(i).BackColor = vbButtonFace
        Next
        If AllTypesCol >= 0 Then
            LineNo = frmHiddenData.tabData(idxLOATAB).GetLineCount
            If OtherTypesCol >= 0 Then LineNo = LineNo - Val(chkTabs(OtherTypesCol).Caption)
            If LineNo > 0 Then chkTabs(AllTypesCol).Caption = CStr(LineNo) Else chkTabs(AllTypesCol).Caption = ""
        End If
    End If
    DataTab(0).ShowHorzScroller True
    DataTab(0).AutoSizeColumns
    DataTab(0).Refresh
End Sub

Private Function BuildHeaderFromFields(FieldList As String) As String
    Dim Result As String
    Dim FldName As String
    Dim CurItem As Long
    Result = ""
    CurItem = 0
    FldName = GetRealItem(FieldList, CurItem, ",")
    While FldName <> ""
        Result = Result & GetFieldValue(FldName, MainFieldsMean, MainFieldsFina) & ","
        CurItem = CurItem + 1
        FldName = GetRealItem(FieldList, CurItem, ",")
    Wend
    If Result <> "" Then Result = Left(Result, Len(Result) - 1)
    If Result = "" Then Result = " "
    BuildHeaderFromFields = Result
End Function
Private Function BuildAlignListFromFields(FieldList As String) As String
    Dim Result As String
    Dim FldName As String
    Dim CurItem As Long
    Dim CurValue As String
    Result = ""
    CurItem = 0
    FldName = GetRealItem(FieldList, CurItem, ",")
    While FldName <> ""
        CurValue = GetFieldValue(FldName, MainFieldsAlig, MainFieldsFina)
        If CurValue = "" Then CurValue = "L"
        Result = Result & CurValue & ","
        CurItem = CurItem + 1
        FldName = GetRealItem(FieldList, CurItem, ",")
    Wend
    If Result <> "" Then Result = Left(Result, Len(Result) - 1)
    If Result = "" Then Result = "L"
    BuildAlignListFromFields = Result
End Function

Public Function BuildLengthListFromFields(FieldList As String) As String
    Dim Result As String
    Dim FldName As String
    Dim CurValue As String
    Dim CurItem As Long
    Result = ""
    CurItem = 0
    FldName = GetRealItem(FieldList, CurItem, ",")
    While FldName <> ""
        CurValue = GetFieldValue(FldName, MainFieldsFele, MainFieldsFina)
        If CurValue = "" Then CurValue = "10"
        Result = Result & CurValue & ","
        CurItem = CurItem + 1
        FldName = GetRealItem(FieldList, CurItem, ",")
    Wend
    If Result <> "" Then Result = Left(Result, Len(Result) - 1)
    If Result = "" Then Result = "10"
    BuildLengthListFromFields = Result
End Function
Private Function BuildWidthListFromFields(FieldList As String) As String
    Dim Result As String
    Dim FldName As String
    Dim CurItem As Long
    Dim CurWidth As Long
    Result = ""
    CurItem = 0
    FldName = GetRealItem(FieldList, CurItem, ",")
    While FldName <> ""
        CurWidth = Val(GetFieldValue(FldName, MainFieldsSize, MainFieldsFina))
        If CurWidth < 10 Then CurWidth = 10
        Result = Result & CStr(CurWidth) & ","
        CurItem = CurItem + 1
        FldName = GetRealItem(FieldList, CurItem, ",")
    Wend
    If Result <> "" Then Result = Left(Result, Len(Result) - 1)
    If Result = "" Then Result = "100"
    BuildWidthListFromFields = Result
End Function

'igu on 29/11/2011
Private Sub SetDateColumns(TabObject As TABLib.TAB, FieldList As String)
    Dim FldName As String
    Dim CurItem As Long
    Dim CurType As String
    
    Const CEDA_DATE_TIME_FORMAT = "YYYYMMDDhhmmss"
    Const SSIM2_DATE_TIME_FORMAT = "DDMMMYY'/'hhmm"
    
    CurItem = 0
    FldName = GetRealItem(FieldList, CurItem, ",")
    While FldName <> ""
        CurType = UCase(GetFieldValue(FldName, MainFieldsType, MainFieldsFina))
        Select Case CurType
            Case "DATE", "DATETTIME"
                'apply date time setting for this column
                TabObject.DateTimeSetColumn CurItem
                TabObject.DateTimeSetInputFormatString CurItem, CEDA_DATE_TIME_FORMAT
                TabObject.DateTimeSetOutputFormatString CurItem, SSIM2_DATE_TIME_FORMAT
                TabObject.DateTimeSetUTCOffsetMinutes CurItem, LoatabTimeZoneOffset
            Case Else
                'Do nothing
        End Select
        CurItem = CurItem + 1
        FldName = GetRealItem(FieldList, CurItem, ",")
    Wend
End Sub

Private Sub chkAbout_Click()
    If chkAbout.Value = 1 Then
        ShowAbout
        chkAbout.Value = 0
    End If
End Sub
Private Sub ShowAbout()
    Dim myControls() As Control
    Dim CompLine As String
    Dim CompList As String
    CompLine = "DDActiveReport," & PrintData.Version & ",,,"
    CompList = CompList & CompLine & vbLf
    MyAboutBox.SetComponents myControls(), CompList
    MyAboutBox.SetLifeStyle = True
    MyAboutBox.Show , Me
    'MyAboutBox.cmdHelp.Enabled = False
    If OnTop.Value = 1 Then SetFormOnTop MyAboutBox, True
End Sub
Private Sub chkClose_Click()
    If chkClose.Value = 1 Then
        If LoatabEditMode Then
            If IsCloseSafe Then
                ShutDownApplication False
            Else
                chkClose.Value = 0
            End If
        Else
            ShutDownApplication False
        End If
    End If
End Sub

Private Sub chkDetail_Click(Index As Integer)
    If chkDetail(Index).Value = 1 Then chkDetail(Index).BackColor = LightGreen Else chkDetail(Index).BackColor = vbButtonFace
    If Not ChkTabsIsBusy Then InitDataTab Val(UfisFolder.Tag)
End Sub

Private Sub chkDssn_Click(Index As Integer)
    If chkDssn(Index).Value = 1 Then chkDssn(Index).BackColor = LightGreen Else chkDssn(Index).BackColor = vbButtonFace
    If (Not ChkTabsIsBusy) And (Not PrepareTabIsBusy) Then
        BuildDssnList
        InitDataTab Val(UfisFolder.Tag)
    End If
End Sub
Private Sub BuildDssnList()
    Dim i As Integer
    Dim tmpList As String
    tmpList = ""
    For i = 0 To chkDssn.UBound
        If chkDssn(i).Value = 1 Then tmpList = tmpList & chkDssn(i).Caption & ","
    Next
    If tmpList <> "" Then tmpList = Left(tmpList, Len(tmpList) - 1)
    fraLeftButtons.Tag = tmpList
End Sub
'kkh to insert / update the lastest figures for Total on board for Departure flight
Private Sub chkInitialLoad_Click()
    Dim changes As Collection
    Dim change As Variant
    Dim Fields() As String
    Dim ufisCmd As String
    Dim strData() As String
    Dim action As String
    Dim sqlFields As String
    Dim sbcKey As String
    Dim insertCount As Long
    Dim updateCount As Long
    
    sqlFields = "URNO,FLNU,DSSN,TYPE,STYP,SSTP,SSST,VALU"
    If LoatabEditMode Then
        Set changes = LoatabEditGrids.GetInitialChanges
        If txtADID.Text = "Departure" Then
            For Each change In changes
                Fields = Split(change, ";")
                If Fields(0) = "Create" Then
                    strData = Split(Fields(1), ",")
                    If strData(0) = "" Then
                        insertCount = insertCount + 1
                    Else
                        updateCount = updateCount + 1
                    End If
                End If
            Next
        
            UfisServer.UrnoPoolInit 50
            UfisServer.UrnoPoolPrepare insertCount
            
            For Each change In changes
                Fields = Split(change, ";")
                If Fields(0) = "Create" Then
                    strData = Split(Fields(1), ",")
                    If strData(0) = "" Then
                        action = "IRT"
                    Else
                        action = "URT"
                    End If
                    If action = "IRT" Then
                        ufisCmd = ufisCmd & "*CMD*," & MainDataTable & ",IRT," & CStr(UBound(Split(sqlFields, ",")) + 1) & "," & sqlFields & vbLf
                        ufisCmd = ufisCmd & UfisServer.UrnoPoolGetNext & Replace(Fields(1), "#", " ") & vbLf
                    Else
                        ufisCmd = ufisCmd & "*CMD*," & MainDataTable & ",URT," & CStr(UBound(Split(sqlFields, ",")) + 1) & "," & sqlFields & ",[URNO=:VURNO]" & vbLf
                        ufisCmd = ufisCmd & Replace(Fields(1), "#", " ")
                        Fields = Split(Fields(1), ",")
                        ufisCmd = ufisCmd & "," & Fields(0) & vbLf
                    End If
                    'ufisCmd = ufisCmd & UfisServer.UrnoPoolGetNext & Replace(Fields(1), "#", " ") & vbLf
                End If
            Next
            If ufisCmd <> "" Then 'igu on 05 Apr 2010
                UfisServer.CallCeda CedaDataAnswer, "REL", MainDataTable, sqlFields, ufisCmd, "LATE,NOBC,NOACTION", "", 0, False, False
                    
                sqlFields = "URNO"
                ufisCmd = txtAFTURNO.Text
                sbcKey = UfisServer.CdrhdlSock.LocalHostName & "," & CStr(App.ThreadID)
                UfisServer.CallCeda CedaDataAnswer, "SBC", "LOAEDT/REFR", sqlFields, ufisCmd, sbcKey, "", 0, False, False
            End If 'igu on 05 Apr 2010
            
            chkInitialLoad.Value = 1
        End If
    End If
End Sub

Private Sub chkLoad_Click()
    Dim CurFolder As String
    Dim i As Integer
    Dim idx As Integer
    If chkLoad.Value = 1 Then
        chkLoad.BackColor = LightGreen
        strAFT_URNO = txtAFTURNO.Text
        Screen.MousePointer = vbHourglass
        frmHiddenData.tabData(1).Tag = MainDataTable & ";" & UsedLoaTabFields
        frmHiddenData.LoadAll
        Screen.MousePointer = 0
        SetFlightFields
        chkLoad.Value = 0
        If Not LoatabEditMode Then
            If Val(UfisFolder.Tag) >= 0 Then
                InitDataTab Val(UfisFolder.Tag)
            Else
                CurFolder = GetItem(Command, 2, ",")
                idx = -1
                i = 0
                While (i <= fraBlend.UBound) And (idx < 0)
                    If CurFolder = fraBlend(i).Tag Then idx = i
                    i = i + 1
                Wend
                If idx < 0 Then idx = 0
                chkFolder(idx).Value = 1
            End If
        Else
            If IsCloseSafe Then
                LoatabEditGrids.LoadData frmHiddenData.tabData(1)
            End If
        End If
    Else
        chkLoad.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkPrint_Click()
    If chkPrint.Value = 1 Then
        chkPrint.BackColor = LightGreen
        PreparePrintLayout
        chkPrint.Value = 0
    Else
        chkPrint.BackColor = vbButtonFace
    End If
End Sub
Private Sub PreparePrintLayout()
    Dim Index As Integer
    Dim CurFields As String
    Dim CurHead As String
    Dim CurAlign As String
    Dim CurWidth As String
    Dim FieldCnt As Long
    Index = Val(UfisFolder.Tag)
    
    CurFields = DataTab(0).LogicalFieldList
    CurHead = DataTab(0).GetHeaderText
    CurWidth = DataTab(0).HeaderLengthString
    CurAlign = DataTab(0).ColumnAlignmentString
    FieldCnt = ItemCount(CurFields, ",") - 2
    
    Dim rpt As New PrintData
    SetFormOnTop Me, False
    Load rpt
    rpt.Caption = "Print Report: " & chkFolder(Index).Caption
    rpt.lblLoaCaption.Caption = Me.Caption
    rpt.lblFolderCaption.Caption = chkFolder(Index).Caption
    rpt.aftFLNO.Text = txtFLNO.Text
    rpt.aftADID.Text = txtADID.Text
    rpt.aftACT3.Text = txtACT.Text
    rpt.aftORG3.Text = txtORIGDEST.Text
    rpt.aftREGN.Text = txtREGN.Text
    rpt.aftSKED.Text = txtSched.Text
    rpt.aftETOA.Text = txtEstimated.Text
    rpt.InitPrintLayout FieldCnt, CurHead, CurFields, CurAlign, CurWidth
    rpt.Show , Me
    rpt.Refresh
    SetFormOnTop Me, True
End Sub

Private Sub chkSave_Click()
    Dim OnTop As Boolean
    Dim changes As Collection
    Dim change As Variant
    Dim Fields() As String
    Dim insertCount As Long
    Dim updateCount As Long
    Dim deleteCount As Long
    Dim ufisCmd As String
    Dim sbcKey As String
    Dim sqlFields As String
    Dim i As Long
    
    'Zaw Min Tun
    '2012-Nov-27
    'V 4.5.25 : TIME column added in the following list to be filled with current timestamp
    sqlFields = "URNO,FLNU,DSSN,TYPE,STYP,SSTP,SSST,VALU,TIME"
    
    If UpdateRemarkWithUserName Then sqlFields = sqlFields & ",RMRK" 'igu on 24/05/2011
    If chkSave.Value = 1 Then
        chkSave.BackColor = LightGreen
'        OnTop = Me.OnTop
'        If OnTop Then SetFormOnTop Me, False
        Set changes = LoatabEditGrids.GetChanges
'        frmDebugOutput.DumpChanges changes
'        frmDebugOutput.Show 1, Me
        
        For Each change In changes
            Fields = Split(change, ";")
            If Fields(0) = "Inserted" Then insertCount = insertCount + 1
            If Fields(0) = "Modified" Then updateCount = updateCount + 1
            If Fields(0) = "Deleted" Then deleteCount = deleteCount + 1
        Next
        
'        UfisServer.ConnectToCeda
        
        If insertCount > 0 Then
            UfisServer.UrnoPoolInit 50
            UfisServer.UrnoPoolPrepare insertCount
        
            ufisCmd = ufisCmd & "*CMD*," & MainDataTable & ",IRT," & CStr(UBound(Split(sqlFields, ",")) + 1) & "," & sqlFields & vbLf
            For Each change In changes
                Fields = Split(change, ";")
                If Fields(0) = "Inserted" Then
                    ufisCmd = ufisCmd & UfisServer.UrnoPoolGetNext & Replace(Fields(1), "#", " ")
                    ufisCmd = ufisCmd & "," & GetTimeStamp(LoatabTimeZoneOffset) 'Zaw Min Tun / 2012-Nov-27 > V 4.5.25 > TIME column is filled with current timestamp
                    If UpdateRemarkWithUserName Then ufisCmd = ufisCmd & "," & UserName 'igu on 24/05/2011
                    ufisCmd = ufisCmd & vbLf
                End If
            Next
        End If
        
        If updateCount > 0 Then
            ufisCmd = ufisCmd & "*CMD*," & MainDataTable & ",URT," & CStr(UBound(Split(sqlFields, ",")) + 1) & "," & sqlFields & ",[URNO=:VURNO]" & vbLf
            For Each change In changes
                Fields = Split(change, ";")
                If Fields(0) = "Modified" Then
                    ufisCmd = ufisCmd & Replace(Fields(1), "#", " ")
                    ufisCmd = ufisCmd & "," & GetTimeStamp(LoatabTimeZoneOffset) 'Zaw Min Tun / 2012-Nov-27 > V 4.5.25 > TIME column is filled with current timestamp
                    If UpdateRemarkWithUserName Then ufisCmd = ufisCmd & "," & UserName 'igu on 24/05/2011
                    Fields = Split(Fields(1), ",")
                    ufisCmd = ufisCmd & "," & Fields(0) & vbLf
                End If
            Next
        End If
        
        If deleteCount > 0 Then
            ufisCmd = ufisCmd & "*CMD*," & MainDataTable & ",DRT,-1,[URNO=:VURNO]" & vbLf
            For Each change In changes
                Fields = Split(change, ";")
                If Fields(0) = "Deleted" Then
                    Fields = Split(Fields(1), ",")
                    ufisCmd = ufisCmd & Fields(0) & vbLf
                End If
            Next
        End If
        
'        frmDebugOutput.DumpText ufisCmd
'        frmDebugOutput.Show 1, Me
'        If OnTop Then SetFormOnTop Me, True
        
        'igu on 30/01/2012
        'UFIS-1384: send extra command "LOA" to server
        If UfisServer.CallCeda(CedaDataAnswer, "REL", MainDataTable, sqlFields, ufisCmd, "LATE,NOBC,NOACTION", "", 0, False, False) = 0 Then
            sqlFields = "DSSN,FLNU"
            ufisCmd = "USR," & txtAFTURNO.Text
            UfisServer.CallCeda CedaDataAnswer, "LOA", "LOATAB", sqlFields, ufisCmd, "", "", 0, False, False
            
            sqlFields = "URNO"
            ufisCmd = txtAFTURNO.Text
            sbcKey = UfisServer.CdrhdlSock.LocalHostName & "," & CStr(App.ThreadID)
            UfisServer.CallCeda CedaDataAnswer, "SBC", "LOAEDT/REFR", sqlFields, ufisCmd, sbcKey, "", 0, False, False
        End If
        
        Call SaveToExtTable 'igu on 05 Apr 2010
        
        
'        UfisServer.DisconnectFromCeda
'        kkh on 16/3/2007 Uncomment
        LoatabEditGrids.ResetAllState
        chkLoad.Value = 1 'igu on 30 Jul 2010 Uncomment, Update loadtabviewer immediately, don't wait for broadcast
        chkSave.Value = 0
    Else
        chkSave.BackColor = vbButtonFace
    End If
End Sub

'igu on 05 Apr 2010
'save to ext table
Private Sub SaveToExtTable()
    Dim grid As EditGrid
    Dim Cells As EditGridCells
    Dim cell As EditGridCell
    Dim i As Integer, j As Integer, k As Integer
    Dim vntConfigs As Variant
    Dim vntConfig As Variant
    Dim vntConfigItems As Variant
    Dim strTable As String
    Dim strField As String
    Dim strKey As String
    Dim strValue As String
    Dim strHelpString As String
    Dim colConfig As Collection
    Dim colConfigs As New Collection
    Dim strCommand As String
    Dim strWhere As String
    Dim strResult As String
    Dim colUpdCommand As New Collection
    
    For i = 1 To LoatabEditGrids.Count
        Set grid = LoatabEditGrids.item(i)
        
        Set Cells = grid.Cells
        
        For j = 1 To Cells.Count
            Set cell = Cells.item(j)
            
            If cell.SaveToExtTable <> "" Then
                vntConfigs = Split(cell.SaveToExtTable, "|")
                For Each vntConfig In vntConfigs
                    vntConfigItems = Split(vntConfig, ",")
                    strTable = vntConfigItems(0)
                    strField = vntConfigItems(2)
                    strKey = strTable & "." & strField
                    vntConfig = vntConfig & "," & cell.Value
                    
                    k = GetRealItemNo(strHelpString, strKey) + 1
                    If k > 0 Then
                        Set colConfig = colConfigs.item(k)
                        colConfig.Add vntConfig
                    Else
                        strHelpString = strHelpString & strKey & ","
                        
                        Set colConfig = New Collection
                        colConfig.Add vntConfig
                        
                        colConfigs.Add colConfig
                    End If
                Next vntConfig
            End If
        Next j
    Next i
    
    For i = 1 To colConfigs.Count
        Set colConfig = colConfigs(i)
        
        strField = ""
        strValue = ""
        For j = 1 To colConfig.Count
            vntConfigItems = Split(colConfig.item(j), ",")
            
            If j = 1 Then
                strTable = vntConfigItems(0)
                strKey = vntConfigItems(2)
                
                If UCase(strTable) = "AFTTAB" Then
                    strCommand = "UFR"
                Else
                    strCommand = "URT"
                End If
                
                strWhere = "WHERE " & strKey & " = " & txtAFTURNO.Text
            End If
            
            strField = strField & "," & vntConfigItems(1)
            strValue = strValue & "," & vntConfigItems(3)
        Next j
        strField = Mid(strField, 2)
        strValue = Mid(strValue, 2)
        
        UfisServer.CallCeda strResult, strCommand, strTable, strField, strValue, strWhere, "", 0, False, False
    Next i
End Sub

Private Sub chkSetup_Click()
    If chkSetup.Value = 1 Then
        chkSetup.BackColor = LightGreen
        SetFormOnTop Me, False
        frmHiddenData.Show , Me
        frmHiddenData.ArrangeConfigMode True
    Else
        frmHiddenData.ArrangeConfigMode False
        frmHiddenData.Hide
        chkSetup.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkTabs_Click(Index As Integer)
    If chkTabs(Index).Value = 1 Then
        chkFolder(Index).Value = 1
        chkTabs(Index).Value = 0
    End If
End Sub

Private Sub chkTelex_Click()
    Dim LineNo As Long
    Dim tmpRurn As String
    If chkTelex.Value = 1 Then
        chkTelex.BackColor = LightGreen
        SetFormOnTop Me, False
        frmTelex.Show , Me
        If OnTop.Value = 1 Then SetFormOnTop Me, True
        LineNo = DataTab(0).GetCurrentSelected
        If LineNo >= 0 Then
            tmpRurn = DataTab(0).GetFieldValue(LineNo, "RURN")
            If tmpRurn <> "" Then
                frmTelex.TabReadTelex tmpRurn
            End If
        End If
    Else
        frmTelex.Hide
        chkTelex.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkType_Click(Index As Integer)
    If chkType(Index).Value = 1 Then chkType(Index).BackColor = LightGreen Else chkType(Index).BackColor = vbButtonFace
    If (Not ChkTabsIsBusy) And (Not PrepareTabIsBusy) Then
        BuildTypeList
        InitDataTab Val(UfisFolder.Tag)
    End If
End Sub
Private Sub BuildTypeList()
    Dim i As Integer
    Dim tmpList As String
    tmpList = ""
    For i = 0 To chkType.UBound
        If chkType(i).Value = 1 Then tmpList = tmpList & chkType(i).Caption & ","
    Next
    If tmpList <> "" Then tmpList = Left(tmpList, Len(tmpList) - 1)
    fraRightButtons.Tag = tmpList
End Sub

Private Sub DataTab_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim tmpRurn As String
    If (Selected) And (LineNo >= 0) Then
        If chkTelex.Value = 1 Then
            tmpRurn = DataTab(0).GetFieldValue(LineNo, "RURN")
            frmTelex.TabReadTelex tmpRurn
        End If
    End If
End Sub

Private Sub DataTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo < 0 Then
        DataTab(Index).Sort CStr(ColNo), True, True
        DataTab(Index).Refresh
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = True
        chkClose.Value = 1
    End If
End Sub

Private Sub Form_Resize()
    Dim NewHeight As Long
    Dim NewSize As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    
    Dim EditGridLeft As Long
    Dim EditGridTop As Long
    Dim EditGridWidth As Long
    Dim EditGridHeight As Long
    
    If Me.WindowState <> vbMinimized Then
        fraBottomPanel.Left = Me.ScaleWidth - fraBottomPanel.Width
        NewSize = Me.ScaleWidth - UfisFolder.Left * 2
        UfisFolder.Width = NewSize
        EditGridLeft = UfisFolder.Left
        EditGridWidth = NewSize
        fraButtonPanel.Width = NewSize
        fraRightButtons.Left = NewSize - fraRightButtons.Width - 30
        fraListPanel.Width = NewSize
        panelLine(0).X2 = fraListPanel.Width - 15
        panelLine(1).X2 = fraListPanel.Width
        NewSize = NewSize - (DataTab(0).Left * 2)
        DataTab(0).Width = NewSize
        NewTop = ArrangeFolderTabs(fraButtonPanel.Top)
        fraFolderLine(0).Width = UfisFolder.Width
        fraFolderLine(1).Width = UfisFolder.Width
        fraFolderLine(2).Width = UfisFolder.Width
        fraTopPanel(0).Width = UfisFolder.Width
        NewLeft = UfisFolder.Width + UfisFolder.Left - fraFolderToggles.Width
        fraFolderToggles.Left = NewLeft
        fraButtonPanel.Top = NewTop
        NewHeight = fraLeftButtons.Top + fraLeftButtons.height + 60
        NewTop = fraLeftButtons.Top
        If (fraLeftButtons.Left + fraLeftButtons.Width) > fraRightButtons.Left Then
            NewHeight = NewHeight + fraLeftButtons.height + 30
            NewTop = NewTop + fraLeftButtons.height + 30
        End If
        fraButtonPanel.height = NewHeight
        fraRightButtons.Top = NewTop
        If Not LoatabEditMode Then
            NewTop = fraButtonPanel.Top + fraButtonPanel.height - 30
            fraListPanel.Top = NewTop
        End If
        NewTop = Me.ScaleHeight - MyStatusBar.height - fraBottomPanel.height
        fraBottomPanel.Top = NewTop
        fraUrnoPanel.Top = NewTop
        NewHeight = NewTop - UfisFolder.Top - 30
        If NewHeight > 150 Then UfisFolder.height = NewHeight
        EditGridHeight = NewHeight + 30
        NewHeight = NewHeight - fraListPanel.Top + UfisFolder.Top
        If NewHeight > 600 Then
            fraListPanel.height = NewHeight
            NewHeight = NewHeight - DataTab(0).Top - 90
            DataTab(0).height = NewHeight
        End If
        EditGridTop = fraTopPanel(0).Top + fraTopPanel(0).height
        
        If LoatabEditMode Then
            LoatabEditGrids.Resize EditGridLeft, EditGridTop, EditGridWidth, EditGridHeight
        End If
    End If
End Sub

Public Sub SetFlightFields()
    Dim idx As Integer
    Dim Value As String
    
    If frmHiddenData.tabData(0).GetLineCount = 0 Then
        Exit Sub
    End If
    
    idx = GetRealItemNo(strAFTFIELDS, "FLNO")
    If idx <> -1 Then
        txtFLNO = frmHiddenData.tabData(0).GetColumnValue(0, idx)
    End If
    idx = GetRealItemNo(strAFTFIELDS, "ACT3")
    If idx <> -1 Then
        Value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
        If Value = "" Then
            idx = GetRealItemNo(strAFTFIELDS, "ACT5")
            If idx <> -1 Then
                Value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
            End If
        End If
        txtACT = Value
    End If
    idx = GetRealItemNo(strAFTFIELDS, "REGN")
    If idx <> -1 Then
        txtREGN = frmHiddenData.tabData(0).GetColumnValue(0, idx)
    End If
    idx = GetRealItemNo(strAFTFIELDS, "ADID")
    If idx <> -1 Then
        Value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
        If Value = "A" Then
            txtADID = "Arrival"
        End If
        If Value = "D" Then
            txtADID = "Departure"
        End If
        '----############Added by Sai for Local flights where ADID = "B"########-----
        If (Value = "B" And strADID = "A") Then
            txtADID = "Arrival"
        End If
        If (Value = "B" And strADID = "D") Then
            txtADID = "Departure"
        End If
        '----#######Ends Here###---------------------------
    End If
    If txtADID = "Arrival" Then
        idx = GetRealItemNo(strAFTFIELDS, "ORG3")
        If idx <> -1 Then
            Value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
            txtORIGDEST = Value
        End If
    End If
    If txtADID = "Departure" Then
        idx = GetRealItemNo(strAFTFIELDS, "DES3")
        If idx <> -1 Then
            Value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
            txtORIGDEST = Value
        End If
    End If
    If txtADID = "Arrival" Then
        idx = GetRealItemNo(strAFTFIELDS, "STOA")
        If idx <> -1 Then
            Value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
            txtSched = DecodeSsimDateTime(Value, "CEDA", "SSIM2")
        End If
    End If
    If txtADID = "Departure" Then
        idx = GetRealItemNo(strAFTFIELDS, "STOD")
        If idx <> -1 Then
            Value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
            txtSched = DecodeSsimDateTime(Value, "CEDA", "SSIM2")
        End If
    End If
    If txtADID = "Arrival" Then
        idx = GetRealItemNo(strAFTFIELDS, "ETAI")
        If idx <> -1 Then
            Value = Trim(frmHiddenData.tabData(0).GetColumnValue(0, idx))
            txtEstimated = ""
            If Value <> "" Then txtEstimated = DecodeSsimDateTime(Value, "CEDA", "SSIM2")
        End If
    End If
    If txtADID = "Departure" Then
        idx = GetRealItemNo(strAFTFIELDS, "ETDI")
        If idx <> -1 Then
            Value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
            txtEstimated = ""
            If Value <> "" Then txtEstimated = DecodeSsimDateTime(Value, "CEDA", "SSIM2")
        End If
    End If

End Sub

Private Sub Form_Load()
On Error GoTo ErrorHandler

    Dim ilLeft As Integer
    Dim ilTop As Integer
    Dim tmpData As String
    Dim ilValue As Long
    Dim blStandaloneOrDebug As Boolean
    Dim strCommand As String
    
    Dim strPassword As String 'igu on 30/05/2011
    
    '----------########Added by Sai####For Disembarkment of Arrival flights##(14Jul,2005)----------------------
    Dim ResultData As String
    strADID = ""
    openedConnection = True
    '------------########Ends here------------------------------------------------------------------------------
       
    strSep = ","
    UsedLoaTabFields = "URNO,FLNU,RURN,DSSN,TYPE,STYP,SSTP,SSST,VALU,IDNT,RFLD,APC3"  '----By Sai added APC3
    AllTypesCol = -1
    OtherTypesCol = -1
    strCommand = Command()
    If Len(strCommand) > 0 Then
    
        '------------
        'igu on 30/05/2011
        'check if username and password are passed
        UserName = GetItem(strCommand, 9, ",")
        strPassword = GetItem(strCommand, 10, ",")
        UfisServer.aCeda.UserName = UserName
        '------------
        
        txtAFTURNO.Text = GetItem(strCommand, 1, ",")
        '---------#####Added by Sai####For Disembarkment of Arrival flights##(14Jul,2005)----------------------
        strADID = UCase(GetItem(strCommand, 8, ",")) '---#### Check if the ADID is passed as parameter ###-----
        
        If (strADID = "" Or strADID = Null) Then
            UfisServer.ConnectToCeda
            If CedaIsConnected Then
                UfisServer.CallCeda ResultData, "RTA", "AFTTAB", "ADID", "", ("WHERE URNO=" & txtAFTURNO.Text), "", 0, True, False
                strADID = ResultData
                openedConnection = True
            Else
                openedConnection = False
            End If
            If strADID = "" Or strADID = Null Or strADID = strADID_B Then
                strADID = strADID_D
            End If
        End If
        '---------------------####Ends Here -------------------------------------------------------------------
        ilValue = Val(GetItem(strCommand, 3, ","))
        If ilValue <= 0 Then ilValue = 14
        If ilValue < 8 Then ilValue = 8
        If ilValue > 32 Then ilValue = 32
        MyFontSize = CInt(ilValue)
        ilValue = Val(GetItem(strCommand, 4, ","))
        If ilValue = 0 Then MyFontBold = False Else MyFontBold = True
        blStandaloneOrDebug = False
        LoatabTimeZoneOffset = Val(GetItem(strCommand, 5, ","))
        If UCase(GetItem(strCommand, 6, ",")) = "EDIT" Then LoatabEditMode = True
        If UCase(GetItem(strCommand, 7, ",")) = "RO" Then
            LoatabEditReadOnly = True
        Else
            LoatabEditReadOnly = False
        End If
    Else
        MyFontSize = 14
        MyFontBold = False
        blStandaloneOrDebug = True
    End If

    ReadConfig
    InitMyForm
    
    idxAFTTAB = 0
    idxLOATAB = 1
    
    strServer = GetIniEntry("", "LOATABVIEWER", "GLOBAL", "HOSTNAME", "")
    strHOPO = GetIniEntry("", "LOATABVIEWER", "GLOBAL", "HOMEAIRPORT", "")
    ilLeft = GetSetting(appName:="LoaTabViewer", _
                        section:="Startup", _
                        Key:="frmMainLeft", _
                        Default:=-1)
    ilTop = GetSetting(appName:="LoaTabViewer", _
                        section:="Startup", _
                        Key:="frmMainTop", _
                        Default:=-1)
    If ilTop = -1 Or ilLeft = -1 Then
        Me.Move 0, 0
    Else
        Me.Move ilLeft, ilTop
    End If

    ' if we are in "runtime", start loading the data immediately
    If blStandaloneOrDebug = False Then
        fraUrnoPanel.Visible = False
        Timer1.Enabled = True
    Else
        'frmHiddenData.Show
        fraUrnoPanel.Visible = True
    End If
    
    Exit Sub
ErrorHandler:
    MsgBox (Err.Description)
    Exit Sub

End Sub

Private Sub InitMyForm()
On Error GoTo ErrorHandler
    Dim newWidth As Long

    fraListPanel.ZOrder
    fraButtonPanel.ZOrder
    fraFolderToggles.ZOrder
    fraTopPanel(0).ZOrder
    fraBottomPanel.ZOrder
    MyStatusBar.ZOrder
    
    If Not LoatabEditMode Then
        chkSave.Visible = False
        UfisFolder.Left = 90
        UfisFolder.Top = fraTopPanel(0).Top
        If fraTopPanel(0).Tag = "VISIBLE" Then UfisFolder.Top = fraTopPanel(0).Top + fraTopPanel(0).height - 30
        fraFolderToggles.Top = UfisFolder.Top
        DefineFolderTab 0, "", "", ""
        UfisFolder.Width = 3000
        fraTopPanel(0).Left = UfisFolder.Left
        fraListPanel.Left = UfisFolder.Left
        fraButtonPanel.Left = UfisFolder.Left
        fraListPanel.Width = UfisFolder.Width
        fraListPanel.height = UfisFolder.height
        fraButtonPanel.Top = UfisFolder.Top + 1020
        fraListPanel.Top = fraButtonPanel.Top + fraButtonPanel.height
        panelLine(1).Y1 = 15
        panelLine(1).Y2 = 15
        fraFolderLine(0).Top = fraLine(0).Top
        fraFolderLine(0).Width = UfisFolder.Width
        fraBottomPanel.Left = UfisFolder.Left
        InitFolderList
        UfisFolder.Tag = "-1"
    Else
        chkSave.Visible = True
        UfisFolder.Visible = False
        fraFolderToggles.Visible = False
        fraButtonPanel.Visible = False
        fraListPanel.Visible = False
        fraDetails.Visible = False
        chkPrint.Enabled = False
        chkTelex.Enabled = False
        
        If LoatabEditReadOnly Then chkSave.Enabled = False
        
        LoatabEditGrids.CreateAll Me
        newWidth = LoatabEditGrids.MinWidth / 96 * 1440 + 380
        If newWidth < txtEstimated.Left + txtEstimated.Width + 310 Then newWidth = txtEstimated.Left + txtEstimated.Width + 310
        Me.Width = newWidth
        Me.height = LoatabEditGrids.MinHeight + 2350
        '-----------------Added by Sai------------------------
        If (Not CedaIsConnected And openedConnection) Then
            UfisServer.ConnectToCeda '---------#####commented connection to ceda as it is already connected above in form load########-----
            If (CedaIsConnected) Then
                openedConnection = True
            Else
                openedConnection = False
            End If
        End If
        If (Not CedaIsConnected And Not openedConnection) Then
            MsgBox "Ceda not connected.Check ceda.ini or contact Admin"
        End If
        '----------------------------Ends here ------------------
        UfisServer.ConnectToBcProxy
    End If
    InitDataTab -1
    
    Exit Sub
ErrorHandler:
    MsgBox (Err.Description)
    Exit Sub
End Sub
Private Sub Form_Unload(Cancel As Integer)
    Dim ilLeft As Integer
    Dim ilTop As Integer

    If LoatabEditMode Then UfisServer.DisconnectFromCeda

    ilLeft = Me.Left
    ilTop = Me.Top
    SaveSetting "LoaTabViewer", _
                "Startup", _
                "frmMainLeft", _
                ilLeft
    SaveSetting "LoaTabViewer", _
                "Startup", _
                "frmMainTop", ilTop
End Sub

Private Sub InitFolderList()
    Dim i As Long
    Dim j As Long
    Dim FolderWidth As Long
    Dim TabList As String
    Dim TabTags As String
    Dim TabActs As String
    Dim SetCaption As String
    Dim SetTag As String
    Dim SetAction As String
    
    ShowTabButtons = True
    FolderWidth = 1500
    
    fraFolder(0).height = chkFolder(0).height
    chkFolder(0).height = 345
    chkFolder(0).Width = FolderWidth
    chkTabs(0).Width = FolderWidth
    fraLine(0).Width = FolderWidth
    fraBlend(0).Width = FolderWidth
    
    ChkTabsIsBusy = True
    fraDetails.Visible = False
    fraDetails.Width = FolderWidth
    chkDetail(0).Value = 1
    chkDetail(1).Value = 1
    chkDetail(2).Value = 1
    chkDetail(3).Value = 1
    ChkTabsIsBusy = False
    
    fraBlend(0).height = 60
    fraFolder(0).height = chkFolder(0).height + fraBlend(0).height + chkTabs(0).height + 60
    
    TabList = FolderCaptions
    TabTags = TelexTypeList
    TabActs = TypeActionList
    j = ItemCount(TabList, ",") - 1
    For i = 0 To j
        SetCaption = GetRealItem(TabList, i, ",")
        SetTag = GetRealItem(TabTags, i, ",")
        SetAction = GetRealItem(TabActs, i, ",")
        If i = OtherTypesCol Then
            SetAction = "??"
            SetTag = CreateMixedCfg(i, TabTags)
        End If
        If i = AllTypesCol Then
            SetAction = "*"
            SetTag = CreateMixedCfg(i, TabTags)
        End If
        DefineFolderTab CInt(i), SetCaption, SetTag, SetAction
    Next
    fraFolderLine(0).Left = UfisFolder.Left
    fraFolderLine(1).Left = UfisFolder.Left
    fraFolderLine(2).Left = UfisFolder.Left
    fraFolderLine(0).Top = 510
    fraFolderLine(1).Top = 885
    fraFolderLine(2).Top = 870
    fraFolderLine(0).Width = UfisFolder.Width
    fraFolderLine(1).Width = UfisFolder.Width
    fraFolderLine(2).Width = UfisFolder.Width
    fraFolderLine(0).Visible = True
    
    FolderScroll.Max = j
    FolderScroll.Min = 0
    chkScroll.Tag = "-1"
    If Not ShowTabButtons Then chkToggle(1).Visible = False
End Sub
Private Function CreateMixedCfg(TabNo As Long, CfgData As String) As String
    Dim j As Long
    Dim i As Long
    Dim TabNoCfg As String
    Dim TabNoHead As String
    Dim TabNoDssn As String
    Dim TabNoType As String
    Dim TabNoFina As String
    Dim CurCfg As String
    Dim NewHead As String
    Dim NewDssn As String
    Dim NewType As String
    Dim NewFina As String
    Dim Result As String
    TabNoCfg = GetRealItem(CfgData, TabNo, ",")
    TabNoHead = GetRealItem(TabNoCfg, 0, "|")
    TabNoDssn = GetRealItem(TabNoCfg, 1, "|")
    If TabNoDssn = "???" Then TabNoDssn = ""
    TabNoType = GetRealItem(TabNoCfg, 2, "|")
    If TabNoType = "???" Then TabNoType = ""
    TabNoFina = GetRealItem(TabNoCfg, 3, "|")
    If TabNoFina = "????" Then
        TabNoFina = ""
        TabNoHead = ""
    End If
    NewDssn = ""
    NewType = ""
    NewFina = ""
    j = ItemCount(CfgData, ",") - 1
    For i = 0 To j
        If i <> TabNo Then
            CurCfg = GetRealItem(CfgData, i, ",")
            If TabNoDssn = "" Then NewDssn = MixItemList(NewDssn, GetRealItem(CurCfg, 1, "|"), ";")
            If TabNoType = "" Then NewType = MixItemList(NewType, GetRealItem(CurCfg, 2, "|"), ";")
            If TabNoFina = "" Then NewFina = MixItemList(NewFina, GetRealItem(CurCfg, 3, "|"), ";")
        End If
    Next
    If TabNoDssn = "" Then TabNoDssn = NewDssn
    If TabNoType = "" Then TabNoType = NewType
    If TabNoFina = "" Then TabNoFina = NewFina
    Result = ""
    Result = Result & TabNoHead & "|"
    Result = Result & TabNoDssn & "|"
    Result = Result & TabNoType & "|"
    Result = Result & TabNoFina
    CreateMixedCfg = Result
End Function
Private Function MixItemList(ItemList1 As String, ItemList2 As String, ItmSep As String) As String
    Dim NewList As String
    Dim ItmData As String
    Dim i As Long
    Dim j As Long
    NewList = ItemList1
    If ItemList2 <> "" Then
        If Left(ItemList2, 1) <> "?" Then
            If NewList = "" Then
                NewList = ItemList2
            Else
                j = ItemCount(ItemList2, ItmSep) - 1
                For i = 0 To j
                    ItmData = GetRealItem(ItemList2, i, ItmSep)
                    If InStr(NewList, ItmData) = 0 Then NewList = NewList & ItmSep & ItmData
                Next
            End If
        End If
    End If
    MixItemList = NewList
End Function
Private Sub DefineFolderTab(Index As Integer, SetCaption As String, SetTag As String, SetAction As String)
    Dim NewLeft As Long
    Dim ButtonTop As Long
    Dim FolderTop As Long
    Dim BlendTop As Long
    Dim SelTop As Long
    Dim LineTop As Long
    Dim RightSide As Long
    
    FolderTop = 135
    ButtonTop = FolderTop + 60
    LineTop = ButtonTop + chkFolder(0).height - 30
    BlendTop = LineTop - 30
    SelTop = ButtonTop + chkFolder(0).height + 30
    'NewLeft = (chkToggle(0).Left + chkToggle(0).Width + 30) + (fraFolder(0).Width * Index)
    NewLeft = fraFolder(0).Width * Index + 90
    RightSide = NewLeft + fraFolder(0).Width - 15
    If Index > fraFolder.UBound Then
        Load fraFolder(Index)
        Load chkFolder(Index)
        Load chkTabs(Index)
        Load fraLine(Index)
        Load fraBlend(Index)
        Load fraRightLine(Index)
        'Load fraLeftLine(Index)
        'Load fraRightLine2(Index)
        'Load fraLeftLine2(Index)
        Set chkFolder(Index).Container = fraFolder(Index)
        Set chkTabs(Index).Container = fraFolder(Index)
        Set fraLine(Index).Container = fraFolder(Index)
        Set fraBlend(Index).Container = fraFolder(Index)
        'Set fraRightLine(Index).Container = fraFolder(Index)
        'Set fraLeftLine(Index).Container = fraFolder(Index)
        'Set fraRightLine2(Index).Container = fraFolder(Index)
        'Set fraLeftLine2(Index).Container = fraFolder(Index)
    End If
    fraFolder(Index).Left = NewLeft
    fraFolder(Index).Width = chkFolder(Index).Width
    chkFolder(Index).Left = 0
    chkTabs(Index).Left = 0
    fraLine(Index).Left = 0
    fraBlend(Index).Left = 0
    fraFolder(Index).Top = FolderTop
    chkFolder(Index).Top = 60
    fraLine(Index).Top = 375
    fraBlend(Index).Top = 345
    chkTabs(Index).Top = 420
    'fraLeftLine(Index).Left = newLeft
    'fraLeftLine(Index).Top = BlendTop - 15
    'fraRightLine(Index).Left = RightSide
    'fraRightLine(Index).Top = BlendTop - 15
    'fraLeftLine2(Index).Left = newLeft + 15
    'fraLeftLine2(Index).Top = BlendTop - 15
    'fraRightLine2(Index).Left = RightSide - 15
    'fraRightLine2(Index).Top = BlendTop - 15
    fraFolder(Index).BackColor = vbButtonFace
    fraBlend(Index).BackColor = vbButtonFace
    chkFolder(Index).Caption = SetCaption
    fraFolder(Index).Tag = SetTag
    fraBlend(Index).Tag = SetAction
    chkFolder(Index).Tag = "0"
    chkTabs(Index).Tag = "0"
    fraFolder(Index).Visible = True
    chkFolder(Index).Visible = True
    fraLine(Index).Visible = True
    fraBlend(Index).Visible = True
    fraFolder(Index).ZOrder
    'chkFolder(Index).ZOrder
    fraBlend(Index).ZOrder
    fraLine(Index).ZOrder
    If ShowTabButtons Then
        chkTabs(Index).Visible = True
        'chkTabs(Index).ZOrder
    End If
End Sub

Private Sub ScrollFolderList()
    Dim NewTop As Long
    Dim CurTop As Long
    CurTop = fraListPanel.Top
    NewTop = ArrangeFolderTabs(CurTop)
    If NewTop <> CurTop Then Form_Resize
End Sub

Private Function ArrangeFolderTabs(LastPanelTop As Long) As Long
    Dim VisibleTabs As Integer
    Dim CurTabList As String
    Dim NewTabList As String
    Dim NewSize As Long
    Dim NewTop As Long
    Dim NewPanelTop As Long
    Dim NewLeft As Long
    Dim TabsCount As Integer
    Dim rowCount As Integer
    Dim MaxRows As Integer
    Dim LastVisible As Integer
    Dim TabIsVisible As Boolean
    Dim i As Integer
    NewTabList = ""
    CurTabList = chkScroll.Tag
    NewPanelTop = LastPanelTop
    MaxRows = 3
    NewSize = UfisFolder.Width - fraFolderToggles.Width - 90
    VisibleTabs = Int(NewSize / fraFolder(0).Width)
    If (VisibleTabs > 2) Or (Not fraFolder(0).Visible) Then
        LastVisible = -1
        TabsCount = 0
        rowCount = 0
        NewTop = fraFolder(0).Top
        NewLeft = 90
        NewTabList = ""
        For i = 0 To fraFolder.UBound
            TabIsVisible = False
            If chkScroll.Value = 0 Then
                If (i >= FolderScroll.Value) And (rowCount < MaxRows) Then TabIsVisible = True
            Else
                If GetRealItem(CurTabList, CLng(i), ",") = "H" Then TabIsVisible = True
            End If
            If TabIsVisible Then
                fraFolder(i).Top = NewTop
                fraFolder(i).Left = NewLeft
                fraFolder(i).Visible = True
                If chkScroll.Value = 0 Then NewTabList = NewTabList & "V,"
                LastVisible = i
                NewLeft = NewLeft + fraFolder(i).Width
                TabsCount = TabsCount + 1
                If TabsCount >= VisibleTabs Then
                    rowCount = rowCount + 1
                    NewSize = UfisFolder.Width - 120
                    VisibleTabs = Int(NewSize / fraFolder(0).Width)
                    NewLeft = 90
                    NewTop = fraFolder(i).Top + fraFolder(i).height + 15
                    TabsCount = 0
                End If
            Else
                fraFolder(i).Visible = False
                If chkScroll.Value = 0 Then NewTabList = NewTabList & "H,"
            End If
        Next
        If LastVisible >= 0 Then NewPanelTop = UfisFolder.Top + fraFolder(LastVisible).Top + fraFolder(LastVisible).height - 15
    End If
    If NewTabList <> "" Then
        chkScroll.Tag = NewTabList
        If InStr(NewTabList, "H") > 0 Then chkScroll.Enabled = True Else chkScroll.Enabled = False
    End If
    If (rowCount > 1) Or ((rowCount = 1) And (NewLeft > 90)) Or (chkScroll.Enabled) Then
        FolderScroll.Enabled = True
    Else
        FolderScroll.Enabled = False
    End If
    fraFolderLine(1).ZOrder
    fraFolderLine(2).ZOrder
    ArrangeFolderTabs = NewPanelTop
End Function


Private Sub chkFolder_Click(Index As Integer)
    Dim TabCfg As String
    Dim CurDssn As String
    If chkFolder(Index).Value = 1 Then
        If chkFolder(Index).Tag <> "1" Then
            If Not ChkTabsIsBusy Then CheckToggledTabs Index, chkFolder, 0
            chkFolder(Index).Top = 0
            chkFolder(Index).height = 375
            chkFolder(Index).BackColor = DarkestBlue
            chkFolder(Index).ForeColor = vbWhite
            fraBlend(Index).ZOrder
            Set fraDetails.Container = fraFolder(Index)
            fraDetails.Top = chkTabs(Index).Top
            fraDetails.Left = chkTabs(Index).Left
            fraDetails.Visible = True
            If Not ChkTabsIsBusy Then
                PrepareTabIsBusy = True
                TabCfg = fraFolder(Index).Tag
                CurDssn = GetRealItem(TabCfg, 1, "|")
                InitDssnButtons CurDssn
                CurDssn = GetRealItem(TabCfg, 2, "|")
                InitTypeButtons CurDssn
                PrepareTabIsBusy = False
                InitDataTab Index
                UfisFolder.Tag = CStr(Index)
            End If
        Else
            If Not ChkTabsIsBusy Then CheckToggledTabs Index, chkFolder, 0
            chkFolder(Index).Tag = "0"
            chkFolder(Index).Top = 60
            chkFolder(Index).height = 345
            chkFolder(Index).BackColor = vbButtonFace
            chkFolder(Index).ForeColor = vbBlack
            fraDetails.Visible = False
            fraLine(Index).ZOrder
        End If
        chkFolder(Index).Value = 0
    End If
End Sub

Private Sub CheckToggledTabs(Index As Integer, CheckObj, ToggleIdx As Integer)
    Dim MaxIdx As Integer
    Dim LastIdx As Integer
    Dim DownValue As Integer
    Dim UpValue As Integer
    Dim i As Integer
    Dim j As Integer
    If Not ChkTabsIsBusy Then
        StopNestedCalls = True
        ChkTabsIsBusy = True
        If ToggleIdx = 0 Then UpValue = 1 Else UpValue = 0
        DownValue = 1
        If chkToggle(ToggleIdx).Value = 0 Then
            For i = 0 To CheckObj.UBound
                If (i <> Index) And (CheckObj(i).Tag <> "0") Then
                    CheckObj(i).Value = UpValue
                    CheckObj(i).Tag = "0"
                End If
            Next
            If CheckObj(Index).Tag = "1" Then CheckObj(Index).Tag = "0" Else CheckObj(Index).Tag = "1"
        Else
            LastIdx = CheckObj.UBound
            MaxIdx = CheckObj.UBound - 2
            If Index <= MaxIdx Then
                If (CheckObj(LastIdx).Tag = "1") Then
                    CheckObj(LastIdx).Value = UpValue
                    CheckObj(LastIdx).Tag = "0"
                End If
                For i = 0 To MaxIdx
                    If CheckObj(i).Tag = "2" Then CheckObj(i).Tag = "0"
                Next
            ElseIf Index = LastIdx Then
                If CheckObj(LastIdx).Tag = "0" Then
                    For i = 0 To MaxIdx
                        If CheckObj(i).Tag <> "0" Then
                            CheckObj(i).Value = UpValue
                            CheckObj(i).Tag = "2"
                        End If
                    Next
                Else
                    For i = 0 To MaxIdx
                        If CheckObj(i).Tag = "2" Then
                            CheckObj(i).Value = DownValue
                            CheckObj(i).Tag = "1"
                        End If
                    Next
                End If
            End If
            If CheckObj(Index).Tag = "1" Then CheckObj(Index).Tag = "0" Else CheckObj(Index).Tag = "1"
        End If
        ChkTabsIsBusy = False
        StopNestedCalls = False
    End If
End Sub


Private Sub ReadConfig()
    Dim LoaIniFile As String
    Dim ActiveFolders As String
    Dim FolderType As String
    Dim CurKeyWord As String
    Dim cfgLine As String
    Dim CfgData As String
    Dim CurItem As Long
    Dim tmpData As String
    
    Dim strLabelWidth As String 'igu on 26/03/2012
    
    If DEFAULT_CEDA_INI = "" Then GetUfisDir
    'LoaIniFile = "c:\ufis\system\LoaTabViewer.ini"
    LoaIniFile = UFIS_SYSTEM & "\LoaTabViewer.ini"
    'MyPrintLogo = GetIniEntry(LoaIniFile, "MAIN", "", "PRINT_LOGO", "c:\ufis\system\CustLogo.bmp")
    MyPrintLogo = GetIniEntry(LoaIniFile, "MAIN", "", "PRINT_LOGO", UFIS_SYSTEM & "\CustLogo.bmp")
    
    Me.Caption = GetIniEntry(LoaIniFile, "MAIN", "", "MAIN_CAPTION", "Pax and Load Information")
    tmpData = GetIniEntry(LoaIniFile, "MAIN", "", "MAIN_SETUP", "NO")
    If tmpData = "YES" Then
        chkSetup.Visible = True
        chkSetup.Enabled = True
        chkSetup.ZOrder
        LoatabEditSetup = True
    Else
        chkSetup.Visible = False
        chkSetup.Enabled = False
        LoatabEditSetup = False
    End If
    MainDataTable = GetIniEntry(LoaIniFile, "MAIN", "", "MAIN_TABLE", "LOATAB")
    tmpData = "URNO,HOPO,FLNU,RURN,IDNT,DSSN,TYPE,STYP,SSTP,SSST,FLNO,APC3,ULDP,ULDT,ADDI,VALU,RMRK,UNIT,TIME,RFLD,RTAB"
    MainFieldsFina = GetIniEntry(LoaIniFile, "MAIN", "", "MAIN_FIELDS", tmpData)
    MainFieldsFele = ""
    MainFieldsAlig = ""
    MainFieldsMean = ""
    MainFieldsType = ""
    MainFieldsSize = ""
    CurItem = 0
    tmpData = GetRealItem(MainFieldsFina, CurItem, ",")
    While tmpData <> ""
        CurKeyWord = tmpData & "_PROP"
        CfgData = tmpData & ",14,L,TRIM"
        cfgLine = GetIniEntry(LoaIniFile, "MAIN", "", CurKeyWord, CfgData)
        MainFieldsMean = MainFieldsMean & GetRealItem(cfgLine, 0, ",") & ","
        MainFieldsFele = MainFieldsFele & GetRealItem(cfgLine, 1, ",") & ","
        MainFieldsAlig = MainFieldsAlig & GetRealItem(cfgLine, 2, ",") & ","
        MainFieldsType = MainFieldsType & GetRealItem(cfgLine, 3, ",") & ","
        MainFieldsSize = MainFieldsSize & GetRealItem(cfgLine, 4, ",") & ","
        CurItem = CurItem + 1
        tmpData = GetRealItem(MainFieldsFina, CurItem, ",")
    Wend
    
    If Not LoatabEditMode Then
        ActiveFolders = GetIniEntry(LoaIniFile, "FOLDERS", "", "ACTIVE_FOLDERS", "NONE")
        FolderCaptions = ""
        TypeActionList = ""
        TelexTypeList = ""
        CurItem = 0
        FolderType = GetRealItem(ActiveFolders, CurItem, ",")
        While FolderType <> ""
            If FolderType = "ELSE" Then OtherTypesCol = CurItem
            If FolderType = "ALL" Then AllTypesCol = CurItem
            TypeActionList = TypeActionList & FolderType & ","
            CurKeyWord = FolderType & "_FOLDER_NAME"
            CfgData = GetIniEntry(LoaIniFile, "FOLDERS", "", CurKeyWord, FolderType & "?")
            FolderCaptions = FolderCaptions & CfgData & ","
            cfgLine = ""
            CurKeyWord = FolderType & "_FOLDER_HEAD"
            CfgData = GetIniEntry(LoaIniFile, "FOLDERS", "", CurKeyWord, "")
            CfgData = Replace(CfgData, ",", ";", 1, -1, vbBinaryCompare)
            cfgLine = cfgLine & CfgData & "|"
            CurKeyWord = FolderType & "_FOLDER_DSSN"
            CfgData = GetIniEntry(LoaIniFile, "FOLDERS", "", CurKeyWord, "???")
            CfgData = Replace(CfgData, ",", ";", 1, -1, vbBinaryCompare)
            cfgLine = cfgLine & CfgData & "|"
            CurKeyWord = FolderType & "_FOLDER_TYPE"
            CfgData = GetIniEntry(LoaIniFile, "FOLDERS", "", CurKeyWord, "???")
            CfgData = Replace(CfgData, ",", ";", 1, -1, vbBinaryCompare)
            cfgLine = cfgLine & CfgData & "|"
            CurKeyWord = FolderType & "_FOLDER_FINA"
            CfgData = GetIniEntry(LoaIniFile, "FOLDERS", "", CurKeyWord, "????")
            CfgData = Replace(CfgData, ",", ";", 1, -1, vbBinaryCompare)
            CheckUsedFields CfgData
            cfgLine = cfgLine & CfgData & "|"
            TelexTypeList = TelexTypeList & cfgLine & ","
            CurItem = CurItem + 1
            FolderType = GetRealItem(ActiveFolders, CurItem, ",")
        Wend
        FolderCaptions = Left(FolderCaptions, Len(FolderCaptions) - 1)
        TypeActionList = Left(TypeActionList, Len(TypeActionList) - 1)
        TelexTypeList = Left(TelexTypeList, Len(TelexTypeList) - 1)
    Else
        Me.Caption = GetIniEntry(LoaIniFile, "EDIT", "", "MAIN_CAPTION", "Pax and Load Information Editor")
        Set LoatabEditGrids = New EditGrids
        LoatabEditGrids.InitFromConfig LoaIniFile
    End If
    
    'igu on 24/05/2011
    Dim strUpdateRemarkWithUserName As String
    strUpdateRemarkWithUserName = GetIniEntry(LoaIniFile, "MAIN", "", "UPDATE_REMARK_WITH_USERNAME", "NO")
    Select Case strUpdateRemarkWithUserName
        Case "TRUE", "YES", "Y"
            UpdateRemarkWithUserName = True
        Case Else
            UpdateRemarkWithUserName = False
    End Select
    'isUsername passed?
    UpdateRemarkWithUserName = UpdateRemarkWithUserName And (UserName <> "")
    
    'igu on 26/03/2012
    'get the row label width
    strLabelWidth = GetIniEntry(LoaIniFile, "MAIN", "", "LABEL_WIDTH", "0")
    intLabelWidth = Val(strLabelWidth)
End Sub


Private Sub CheckUsedFields(FolderFields As String)
    Dim CurField As String
    Dim CurItem As Long
    CurItem = 0
    CurField = GetRealItem(FolderFields, CurItem, ";")
    While CurField <> ""
        If CurField <> "????" Then
            If InStr(UsedLoaTabFields, CurField) = 0 Then UsedLoaTabFields = UsedLoaTabFields & "," & CurField
        End If
        CurItem = CurItem + 1
        CurField = GetRealItem(FolderFields, CurItem, ";")
    Wend
End Sub
Private Sub InitDssnButtons(TypeList As String)
    Dim CurType As String
    Dim CurLeft As Long
    Dim i As Long
    For i = 0 To chkDssn.UBound
        chkDssn(i).Visible = False
    Next
    CurLeft = chkDssn(0).Left
    i = 0
    CurType = GetRealItem(TypeList, i, ";")
    While CurType <> ""
        If i > chkDssn.UBound Then
            Load chkDssn(i)
        End If
        chkDssn(i).Top = chkDssn(0).Top
        chkDssn(i).Left = CurLeft
        chkDssn(i).Caption = CurType
        chkDssn(i).Value = 1
        chkDssn(i).Visible = True
        CurLeft = CurLeft + chkDssn(0).Width
        i = i + 1
        CurType = GetRealItem(TypeList, i, ";")
    Wend
    fraLeftButtons.Width = CurLeft + 30
    BuildDssnList
End Sub
Private Sub InitTypeButtons(TypeList As String)
    Dim CurType As String
    Dim CurLeft As Long
    Dim i As Long
    For i = 0 To chkType.UBound
        chkType(i).Visible = False
    Next
    CurLeft = chkType(0).Left
    i = 0
    CurType = GetRealItem(TypeList, i, ";")
    While CurType <> ""
        If i > chkType.UBound Then
            Load chkType(i)
        End If
        chkType(i).Top = chkType(0).Top
        chkType(i).Left = CurLeft
        chkType(i).Caption = CurType
        chkType(i).Value = 1
        chkType(i).Visible = True
        CurLeft = CurLeft + chkType(0).Width
        i = i + 1
        CurType = GetRealItem(TypeList, i, ";")
    Wend
    fraRightButtons.Width = CurLeft + 60
    fraRightButtons.Left = fraButtonPanel.Width - fraRightButtons.Width - 60
    BuildTypeList
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then
        OnTop.BackColor = LightGreen
        SetFormOnTop Me, True
    Else
        SetFormOnTop Me, False
        OnTop.BackColor = vbButtonFace
    End If
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    OnTop.Value = 1
    chkLoad.Value = 1
    chkInitialLoad.Value = 1
End Sub
Private Sub chkScroll_Click()
    If chkScroll.Value = 1 Then chkScroll.BackColor = LightGreen Else chkScroll.BackColor = vbButtonFace
    ScrollFolderList
End Sub
Private Sub FolderScroll_Change()
    ScrollFolderList
End Sub

Private Sub FolderScroll_Scroll()
    ScrollFolderList
End Sub

Private Sub chkToggle_Click(Index As Integer)
    If chkToggle(Index).Value = 1 Then chkToggle(Index).BackColor = LightGreen Else chkToggle(Index).BackColor = vbButtonFace
End Sub

Private Function IsCloseSafe() As Boolean
    IsCloseSafe = False
    If LoatabEditGrids.GetChanges().Count > 0 Then
        If MsgBox("Do you want to discard all changes?", vbOKCancel, Me.Caption) = vbOK Then
            IsCloseSafe = True
        End If
    Else
        IsCloseSafe = True
    End If
End Function

