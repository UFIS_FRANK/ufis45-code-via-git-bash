VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form FilterDialog 
   Caption         =   "Record Filter Preset"
   ClientHeight    =   5100
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11070
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   11.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FilterDialog.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5100
   ScaleWidth      =   11070
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox FtypPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   3945
      Left            =   8070
      ScaleHeight     =   3885
      ScaleWidth      =   1365
      TabIndex        =   59
      Top             =   0
      Visible         =   0   'False
      Width           =   1425
      Begin VB.CheckBox chkFtypAll 
         BackColor       =   &H008080FF&
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   45
         MaskColor       =   &H8000000F&
         TabIndex        =   64
         Tag             =   "-1"
         Top             =   1680
         Width           =   420
      End
      Begin VB.TextBox txtFtypFilter 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   30
         TabIndex        =   63
         Text            =   "FTYP FILTER"
         Top             =   2280
         Visible         =   0   'False
         Width           =   1245
      End
      Begin VB.CheckBox chkFtypFilter 
         BackColor       =   &H008080FF&
         Caption         =   "X"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   45
         MaskColor       =   &H8000000F&
         TabIndex        =   60
         Tag             =   "-1"
         Top             =   1980
         Width           =   420
      End
      Begin VB.Label lblFtypAll 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "All"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Index           =   0
         Left            =   540
         TabIndex        =   65
         ToolTipText     =   "Checks or Unchecks All"
         Top             =   1680
         Width           =   210
      End
      Begin VB.Label lblFtypName 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "CXX"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Index           =   0
         Left            =   570
         TabIndex        =   62
         Top             =   1980
         Width           =   330
      End
      Begin VB.Label lblFtypShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "CXX"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   0
         Left            =   585
         TabIndex        =   61
         Top             =   1995
         Width           =   330
      End
      Begin VB.Label lblFtypAllShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "All"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   0
         Left            =   810
         TabIndex        =   66
         Top             =   1680
         Width           =   210
      End
   End
   Begin VB.PictureBox RightPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   3945
      Left            =   9555
      ScaleHeight     =   3885
      ScaleWidth      =   1095
      TabIndex        =   35
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   1155
      Begin VB.HScrollBar FilterScroll 
         Height          =   315
         Left            =   30
         TabIndex        =   46
         Top             =   30
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Reset"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   56
         Tag             =   "RESETFILTER"
         Top             =   1050
         Width           =   1035
      End
      Begin VB.CheckBox chkFilterIsVisible 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   45
         MaskColor       =   &H8000000F&
         TabIndex        =   51
         Tag             =   "-1"
         Top             =   2520
         Visible         =   0   'False
         Width           =   210
      End
      Begin VB.CheckBox chkFullSize 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   60
         TabIndex        =   48
         Top             =   2010
         Width           =   210
      End
      Begin VB.CheckBox chkToggle 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   60
         TabIndex        =   47
         Top             =   2250
         Width           =   210
      End
      Begin VB.CheckBox chkOnTop 
         Caption         =   "Top"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   30
         Width           =   720
      End
      Begin VB.CheckBox Check2 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   750
         Style           =   1  'Graphical
         TabIndex        =   44
         ToolTipText     =   "Bypass the Clear Function"
         Top             =   30
         Width           =   315
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Refresh"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   43
         Tag             =   "AODBREFRESH"
         Top             =   1380
         Width           =   1035
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Load"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   42
         Tag             =   "AODB_FILTER"
         Top             =   390
         Width           =   1035
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   36
         Tag             =   "CLOSE"
         Top             =   720
         Width           =   1035
      End
      Begin VB.Label lblToggle 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Toggle"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   54
         Top             =   2220
         Width           =   600
      End
      Begin VB.Label lblToggle 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Toggle"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   1
         Left            =   315
         TabIndex        =   55
         Top             =   2295
         Width           =   600
      End
      Begin VB.Label lblFilterShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Shadow"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   0
         Left            =   90
         TabIndex        =   53
         Top             =   2760
         Visible         =   0   'False
         Width           =   690
      End
      Begin VB.Label lblFilterName 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   52
         Top             =   2550
         Visible         =   0   'False
         Width           =   585
      End
      Begin VB.Label lblFullSize 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Full"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   0
         Left            =   360
         TabIndex        =   49
         Top             =   1980
         Width           =   315
      End
      Begin VB.Label lblFullSize 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Full"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   1
         Left            =   465
         TabIndex        =   50
         Top             =   2025
         Width           =   315
      End
   End
   Begin VB.PictureBox TopPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   1125
      Left            =   60
      ScaleHeight     =   1065
      ScaleWidth      =   7005
      TabIndex        =   34
      TabStop         =   0   'False
      Top             =   60
      Visible         =   0   'False
      Width           =   7065
      Begin VB.OptionButton optUTC 
         Caption         =   "UTC"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4770
         Style           =   1  'Graphical
         TabIndex        =   58
         ToolTipText     =   "Timeframe in UTC"
         Top             =   420
         Width           =   525
      End
      Begin VB.OptionButton optLocal 
         Caption         =   "LT"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4080
         Style           =   1  'Graphical
         TabIndex        =   57
         Tag             =   "TEST"
         ToolTipText     =   "Timeframe in Local Time"
         Top             =   420
         Width           =   525
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   690
         MaxLength       =   4
         TabIndex        =   0
         Top             =   420
         Width           =   585
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1290
         MaxLength       =   2
         TabIndex        =   1
         Top             =   420
         Width           =   375
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   1680
         MaxLength       =   2
         TabIndex        =   2
         Top             =   420
         Width           =   375
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   3540
         MaxLength       =   2
         TabIndex        =   5
         Top             =   420
         Width           =   375
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   3150
         MaxLength       =   2
         TabIndex        =   4
         Top             =   420
         Width           =   375
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   2550
         MaxLength       =   4
         TabIndex        =   3
         Top             =   420
         Width           =   585
      End
      Begin VB.Label TopLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   0
         Left            =   720
         TabIndex        =   41
         Top             =   0
         Width           =   600
      End
      Begin VB.Label TopShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Shadow"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   0
         Left            =   1500
         TabIndex        =   40
         Top             =   60
         Width           =   840
      End
   End
   Begin VB.PictureBox WorkArea 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   2235
      Left            =   60
      ScaleHeight     =   2175
      ScaleWidth      =   2685
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   1260
      Visible         =   0   'False
      Width           =   2745
      Begin VB.PictureBox FilterPanel 
         Height          =   1845
         Index           =   0
         Left            =   90
         ScaleHeight     =   1785
         ScaleWidth      =   2295
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   60
         Visible         =   0   'False
         Width           =   2355
         Begin VB.PictureBox ButtonPanel 
            AutoRedraw      =   -1  'True
            Height          =   405
            Index           =   0
            Left            =   0
            ScaleHeight     =   345
            ScaleWidth      =   2115
            TabIndex        =   37
            TabStop         =   0   'False
            Top             =   1350
            Width           =   2175
            Begin VB.CheckBox chkRightFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   1215
               Style           =   1  'Graphical
               TabIndex        =   39
               Top             =   30
               Width           =   765
            End
            Begin VB.CheckBox chkLeftFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   180
               Style           =   1  'Graphical
               TabIndex        =   38
               Top             =   30
               Width           =   765
            End
         End
         Begin VB.TextBox txtFilterVal 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   870
            TabIndex        =   31
            Top             =   15
            Width           =   855
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "Filter"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   15
            Style           =   1  'Graphical
            TabIndex        =   30
            ToolTipText     =   "Activates/De-Activates the filter "
            Top             =   15
            Width           =   855
         End
         Begin TABLib.TAB LeftFilterTab 
            Height          =   915
            Index           =   0
            Left            =   15
            TabIndex        =   32
            Top             =   345
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1614
            _StockProps     =   64
            Columns         =   10
         End
         Begin TABLib.TAB RightFilterTab 
            Height          =   915
            Index           =   0
            Left            =   870
            TabIndex        =   33
            Top             =   345
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1614
            _StockProps     =   64
            Columns         =   10
         End
      End
   End
   Begin VB.Frame fraDispoButtonPanel 
      BorderStyle     =   0  'None
      Height          =   405
      Left            =   60
      TabIndex        =   23
      Top             =   3570
      Visible         =   0   'False
      Width           =   7005
      Begin VB.CheckBox Check1 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3870
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   0
         Width           =   855
      End
      Begin TABLib.TAB NatTabCombo 
         Height          =   315
         Index           =   0
         Left            =   0
         TabIndex        =   26
         Top             =   0
         Visible         =   0   'False
         Width           =   3855
         _Version        =   65536
         _ExtentX        =   6800
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin VB.CheckBox chkSetDispo 
         Caption         =   "Perform"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4740
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5610
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame fraFilterButtonPanel 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   60
      TabIndex        =   17
      Top             =   4080
      Visible         =   0   'False
      Width           =   5175
      Begin VB.CheckBox chkTool 
         Caption         =   "Adjust"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   2610
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   3480
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Refresh"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Load"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Compare"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   870
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame fraFredButtonPanel 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   60
      TabIndex        =   7
      Top             =   4410
      Visible         =   0   'False
      Width           =   3585
      Begin VB.CheckBox chkWkDay 
         Caption         =   "All"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "Select all frequency days"
         Top             =   0
         Width           =   765
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   2400
         Style           =   1  'Graphical
         TabIndex        =   15
         ToolTipText     =   "Frequency filter for Sunday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   2130
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "Frequency filter for Saturday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "Frequency filter for Friday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   1590
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Frequency filter for Thursday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Frequency filter for Wednesday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Frequency filter for Thuesday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   9
         ToolTipText     =   "Frequency filter for Monday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "None"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   2670
         Style           =   1  'Graphical
         TabIndex        =   8
         Tag             =   "......."
         ToolTipText     =   "Unselect all frequency days"
         Top             =   0
         Width           =   795
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   4815
      Width           =   11070
      _ExtentX        =   19526
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   18988
            MinWidth        =   1764
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "FilterDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim InternalResize As Boolean
Dim LayoutChanged As Boolean
Dim MyGlobalResult As String
Dim MyFilterIniSection As String
Dim MyCurParent As Form
Dim ButtonPushedByUser As Boolean
Dim LoadingConfig As Boolean

Public Function InitMyLayout(MyParent As Form, ShowModal As Boolean, LayoutStyle As String) As String
    Dim MyTop As Long
    Dim MyLeft As Long
    Set MyCurParent = MyParent
    If LayoutStyle <> Me.Tag Then LayoutChanged = True Else LayoutChanged = False
    MyFilterIniSection = GetIniEntry(myIniFullName, myIniSection, "", "AODB_FILTER_SECTION", myIniSection)
    
    Me.Tag = ""
    InitTopPanel LayoutStyle
    InitWorkArea LayoutStyle
    InitFtypFilterPanel LayoutStyle
    Me.Tag = LayoutStyle
    MyTop = MyCurParent.Top + ((MyCurParent.Height - Me.Height) / 2)
    MyLeft = MyCurParent.Left + ((MyCurParent.Width - Me.Width) / 2)
    If MyTop > Screen.Height - Me.Height Then MyTop = Screen.Height - Me.Height
    If MyTop < 0 Then MyTop = 0
    If MyLeft > Screen.Width - Me.Width Then MyLeft = Screen.Width - Me.Width
    If MyLeft < 0 Then MyLeft = 0
    Me.Top = MyTop
    Me.Left = MyLeft
    Me.Caption = GetFilterConfig(LayoutStyle, "FILTER_CAPTION", -1)
    If ShowModal Then
        Me.Show vbModal, MyParent
    Else
        Me.Show , MyParent
    End If
    InitMyLayout = MyGlobalResult
End Function

Private Sub InitTopPanel(LayoutType As String)
    Dim i As Integer
    Dim NewLeft As Long
    Dim tmpData As String
    TopPanel.Top = 0
    TopPanel.Left = 0
    If TopLabel.UBound = 0 Then
        For i = 1 To 2
            Load TopLabel(i)
            Load TopShadow(i)
        Next
    End If
    TopLabel(0).Caption = "Timeframe Filter && Flight Search Engine"
    TopLabel(0).Top = 120
    TopLabel(0).Left = 120
    NewLeft = 120
    For i = 0 To 2
        txtVpfr(i).Top = 420
        txtVpfr(i).Left = NewLeft
        NewLeft = NewLeft + txtVpfr(i).Width
    Next
    NewLeft = NewLeft + 90
    TopLabel(1).Caption = "-"
    TopLabel(1).Top = 450
    TopLabel(1).Left = NewLeft
    NewLeft = NewLeft + TopLabel(1).Width + 60
    For i = 0 To 2
        txtVpto(i).Top = 420
        txtVpto(i).Left = NewLeft
        NewLeft = NewLeft + txtVpto(i).Width
    Next
    For i = 0 To 1
        TopLabel(i).ZOrder
        TopShadow(i).Caption = TopLabel(i).Caption
        TopShadow(i).Top = TopLabel(i).Top + 15
        TopShadow(i).Left = TopLabel(i).Left + 15
        TopLabel(i).Visible = True
        TopShadow(i).Visible = True
        TopLabel(i).ZOrder
    Next
    tmpData = Format(Now, "yyyymmdd")
    If txtVpfr(0).Tag = "" Then
        txtVpfr(0).Text = Mid(tmpData, 1, 4)
        txtVpfr(1).Text = Mid(tmpData, 5, 2)
        txtVpfr(2).Text = Mid(tmpData, 7, 2)
    End If
    If txtVpto(0).Tag = "" Then
        txtVpto(0).Text = Mid(tmpData, 1, 4)
        txtVpto(1).Text = Mid(tmpData, 5, 2)
        txtVpto(2).Text = Mid(tmpData, 7, 2)
    End If
    optLocal.Top = 420
    optUTC.Top = 420
    optLocal.Left = txtVpto(2).Left + txtVpto(2).Width + 120
    optUTC.Left = optLocal.Left + optLocal.Width + 15
    TopPanel.Height = 900
    TopPanel.Width = Screen.Width + 300
    DrawBackGround TopPanel, 7, True, False
    If LayoutChanged Then optLocal.Value = True
End Sub
Private Sub InitWorkArea(LayoutStyle As String)
    Dim UsedPanels As Integer
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim TabLeft As Long
    Dim TabWidth As Long
    Dim WidthList As String
    Dim TabCfgLine As String
    Dim ItemNo As Long
    Dim NewSection As Boolean
    Dim FilterCount As Integer
    Dim CurFilter As Integer
    Dim PanelLayout As String
    Dim tmpHeader As String
    Dim tmpActive As String
    Dim tmpToggle As String
    
    UsedPanels = FilterPanel.UBound
    For UsedPanels = 0 To FilterPanel.UBound
        FilterPanel(UsedPanels).Visible = False
        chkFilterIsVisible(UsedPanels).Visible = False
        lblFilterName(UsedPanels).Visible = False
        lblFilterShadow(UsedPanels).Visible = False
        'RightFilterTab(UsedPanels).myTag = ""
        HiddenData.UpdateTabConfig LeftFilterTab(UsedPanels).myName, "STAT", "H"
    Next
    WorkArea.Top = TopPanel.Height
    FilterCount = Val(GetFilterConfig(LayoutStyle, "FILTER_PANELS", -1))
    
    NewLeft = 120
    NewTop = chkToggle.Top + chkToggle.Height + 90
    UsedPanels = -1
    ItemNo = 0
    For CurFilter = 1 To FilterCount
        PanelLayout = GetFilterConfig(LayoutStyle, "PANEL_LAYOUT", CurFilter)
        UsedPanels = UsedPanels + 1
        NewSection = False
        If UsedPanels > FilterPanel.UBound Then
            Load FilterPanel(UsedPanels)
            Load txtFilterVal(UsedPanels)
            Load chkWork(UsedPanels)
            Load LeftFilterTab(UsedPanels)
            Load RightFilterTab(UsedPanels)
            Load ButtonPanel(UsedPanels)
            Load chkFilterIsVisible(UsedPanels)
            Load lblFilterName(UsedPanels)
            Load lblFilterShadow(UsedPanels)
            Set txtFilterVal(UsedPanels).Container = FilterPanel(UsedPanels)
            Set chkWork(UsedPanels).Container = FilterPanel(UsedPanels)
            Set LeftFilterTab(UsedPanels).Container = FilterPanel(UsedPanels)
            Set RightFilterTab(UsedPanels).Container = FilterPanel(UsedPanels)
            Set ButtonPanel(UsedPanels).Container = FilterPanel(UsedPanels)
            Load chkLeftFilterSet(UsedPanels)
            Load chkRightFilterSet(UsedPanels)
            Set chkLeftFilterSet(UsedPanels).Container = ButtonPanel(UsedPanels)
            Set chkRightFilterSet(UsedPanels).Container = ButtonPanel(UsedPanels)
            NewSection = True
        End If
        
        FilterPanel(UsedPanels).Top = 120
        FilterPanel(UsedPanels).Left = NewLeft
        FilterPanel(UsedPanels).Visible = True
        txtFilterVal(UsedPanels).Visible = True
        If (LayoutChanged) Or (NewSection) Then
            LeftFilterTab(UsedPanels).ResetContent
            tmpHeader = GetRealItem(PanelLayout, 4, "|")
            tmpHeader = BuildDummyHeader(tmpHeader) & ",-----"
            LeftFilterTab(UsedPanels).FontName = "Courier New"
            LeftFilterTab(UsedPanels).SetMainHeaderFont MainDialog.FontSlider.Value + 6, False, False, False, 0, "Courier New"
            LeftFilterTab(UsedPanels).HeaderString = tmpHeader
            LeftFilterTab(UsedPanels).FontSize = MainDialog.FontSlider.Value + 6
            LeftFilterTab(UsedPanels).HeaderFontSize = MainDialog.FontSlider.Value + 6
            LeftFilterTab(UsedPanels).LineHeight = MainDialog.FontSlider.Value + 6
            LeftFilterTab(UsedPanels).HeaderLengthString = "10,10,10"
            If (LayoutChanged) Or (NewSection) Then LeftFilterTab(UsedPanels).LogicalFieldList = GetRealItem(PanelLayout, 3, "|")
            LeftFilterTab(UsedPanels).SelectBackColor = DarkestYellow
            LeftFilterTab(UsedPanels).SelectTextColor = vbWhite
            LeftFilterTab(UsedPanels).EmptyAreaBackColor = LightGray
            LeftFilterTab(UsedPanels).EmptyAreaRightColor = LightGray
            LeftFilterTab(UsedPanels).MainHeaderOnly = True
            LeftFilterTab(UsedPanels).LeftTextOffset = 1
            LeftFilterTab(UsedPanels).SetTabFontBold True
            LeftFilterTab(UsedPanels).AutoSizeByHeader = True
            LeftFilterTab(UsedPanels).AutoSizeColumns
            WidthList = LeftFilterTab(UsedPanels).HeaderLengthString
            TabWidth = (Val(GetRealItem(WidthList, 0, ",")) * 15) + (Val(GetRealItem(WidthList, 1, ",")) * 15)
            'If GetRealItem(PanelLayout, 2, "|") = "Y" Then TabWidth = TabWidth + Val(GetRealItem(WidthList, 1, ",")) * 15
            TabWidth = TabWidth + 16 * 15 'ScrollBar
            If TabWidth < 855 Then TabWidth = 855
            LeftFilterTab(UsedPanels).Width = TabWidth
        End If
        LeftFilterTab(UsedPanels).Visible = True
        
        If (LayoutChanged) Or (NewSection) Then
            RightFilterTab(UsedPanels).ResetContent
            RightFilterTab(UsedPanels).Width = TabWidth
            RightFilterTab(UsedPanels).HeaderString = tmpHeader
            RightFilterTab(UsedPanels).HeaderLengthString = "10,10,10"
            RightFilterTab(UsedPanels).FontName = "Courier New"
            RightFilterTab(UsedPanels).SetMainHeaderFont MainDialog.FontSlider.Value + 6, False, False, False, 0, "Courier New"
            RightFilterTab(UsedPanels).FontSize = MainDialog.FontSlider.Value + 6
            RightFilterTab(UsedPanels).HeaderFontSize = MainDialog.FontSlider.Value + 6
            RightFilterTab(UsedPanels).LineHeight = MainDialog.FontSlider.Value + 6
            If (LayoutChanged) Or (NewSection) Then RightFilterTab(UsedPanels).LogicalFieldList = GetRealItem(PanelLayout, 3, "|")
            RightFilterTab(UsedPanels).Left = LeftFilterTab(UsedPanels).Left + TabWidth + 15
            RightFilterTab(UsedPanels).SelectBackColor = DarkBlue
            RightFilterTab(UsedPanels).SelectTextColor = vbWhite
            RightFilterTab(UsedPanels).EmptyAreaBackColor = LightGray
            RightFilterTab(UsedPanels).EmptyAreaRightColor = LightGray
            RightFilterTab(UsedPanels).MainHeaderOnly = True
            RightFilterTab(UsedPanels).LeftTextOffset = 1
            RightFilterTab(UsedPanels).SetTabFontBold True
            RightFilterTab(UsedPanels).AutoSizeByHeader = True
            RightFilterTab(UsedPanels).AutoSizeColumns
        End If
        RightFilterTab(UsedPanels).Visible = True
        
        If (LayoutChanged) Or (NewSection) Then
            chkWork(UsedPanels).Width = TabWidth
            txtFilterVal(UsedPanels).Width = TabWidth
            txtFilterVal(UsedPanels).Left = RightFilterTab(UsedPanels).Left
            FilterPanel(UsedPanels).Width = RightFilterTab(UsedPanels).Left + RightFilterTab(UsedPanels).Width + 60
            
            chkLeftFilterSet(UsedPanels).Width = TabWidth - 90
            chkRightFilterSet(UsedPanels).Width = TabWidth - 90
            chkLeftFilterSet(UsedPanels).Left = LeftFilterTab(UsedPanels).Left + ((TabWidth - chkLeftFilterSet(UsedPanels).Width) / 2)
            chkRightFilterSet(UsedPanels).Left = RightFilterTab(UsedPanels).Left + ((TabWidth - chkRightFilterSet(UsedPanels).Width) / 2) - 15
            chkLeftFilterSet(UsedPanels).Visible = True
            chkRightFilterSet(UsedPanels).Visible = True
            ButtonPanel(UsedPanels).Left = -30
            ButtonPanel(UsedPanels).Width = FilterPanel(UsedPanels).Width + 30
            DrawBackGround ButtonPanel(UsedPanels), 7, True, True
        End If
        ButtonPanel(UsedPanels).Visible = True
        
        tmpActive = GetRealItem(PanelLayout, 1, "|")
        chkWork(UsedPanels).Caption = GetRealItem(PanelLayout, 0, "|")
        If (LayoutChanged) Or (NewSection) Then
            chkWork(UsedPanels).Value = 0
            chkWork(UsedPanels).Value = 1
            If GetRealItem(tmpActive, 1, ",") <> "Y" Then chkWork(UsedPanels).Value = 0
            chkWork(UsedPanels).Tag = ""
        End If
        chkWork(UsedPanels).Visible = True
        
        LeftFilterTab(UsedPanels).myName = "FILTER_LEFT_" & CStr(UsedPanels)
        DefineDataConfig LeftFilterTab(UsedPanels), LayoutStyle, PanelLayout
        If (LayoutChanged) Or (NewSection) Then RightFilterTab(UsedPanels).myTag = GetRealItem(RightFilterTab(UsedPanels).LogicalFieldList, 0, ",")
        
        chkFilterIsVisible(UsedPanels).Top = NewTop
        lblFilterName(UsedPanels).Top = NewTop + 15
        lblFilterShadow(UsedPanels).Top = NewTop + 30
        lblFilterName(UsedPanels).Left = chkFilterIsVisible(UsedPanels).Left + chkFilterIsVisible(UsedPanels).Width + 15
        lblFilterShadow(UsedPanels).Left = lblFilterName(UsedPanels).Left + 15
        lblFilterName(UsedPanels).Caption = GetRealItem(PanelLayout, 0, "|")
        lblFilterShadow(UsedPanels).Caption = GetRealItem(PanelLayout, 0, "|")
        lblFilterName(UsedPanels).ZOrder
        chkFilterIsVisible(UsedPanels).Value = 1
        If GetRealItem(tmpActive, 0, ",") <> "Y" Then chkFilterIsVisible(UsedPanels).Value = 0
        If RightFilterTab(UsedPanels).GetLineCount > 0 Then chkFilterIsVisible(UsedPanels).Value = 1
        chkFilterIsVisible(UsedPanels).Visible = True
        lblFilterName(UsedPanels).Visible = True
        lblFilterShadow(UsedPanels).Visible = True
        If (LayoutChanged) Or (NewSection) Then
            If GetRealItem(tmpActive, 2, ",") = "T" Then
                HandleToggleFilter chkFilterIsVisible(UsedPanels), UsedPanels
            End If
        End If
        
        NewTop = NewTop + chkFilterIsVisible(UsedPanels).Height + 15
        If chkFilterIsVisible(UsedPanels).Value = 1 Then
            NewLeft = FilterPanel(UsedPanels).Left + FilterPanel(UsedPanels).Width + 120
        End If
        ItemNo = ItemNo + 1
    Next
    FtypPanel.Align = 0
    RightPanel.Align = 0
    RightPanel.Height = Screen.Height
    DrawBackGround RightPanel, 7, True, True
    RightPanel.Align = 4
    RightPanel.Visible = True
    TopPanel.Visible = True
    Me.Width = NewLeft + RightPanel.Width + 180
End Sub
Private Sub InitFtypFilterPanel(LayoutStyle As String)
    Dim Index As Integer
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim NewSection As Boolean
    Dim FilterCount As Integer
    Dim CurFilter As Integer
    Dim FtypLayout As String
    Dim tmpItem As String
    
    FtypPanel.Visible = False
    LoadingConfig = True
    For Index = 0 To chkFtypFilter.UBound
        chkFtypFilter(Index).Visible = False
        lblFtypName(Index).Visible = False
        lblFtypShadow(Index).Visible = False
        If LayoutChanged Then chkFtypFilter(Index).Value = 0
    Next
    FilterCount = Val(GetFilterConfig(LayoutStyle, "FILTER_FTYPES", -1))
    If FilterCount > 0 Then
        NewTop = chkAppl(2).Top + chkAppl(2).Height + 15
        NewLeft = 45
        chkFtypAll(0).Top = NewTop
        chkFtypAll(0).BackColor = LightGray
        lblFtypAll(0).Top = NewTop + 15
        lblFtypAll(0).Left = chkFtypAll(0).Left + chkFtypAll(0).Width + 60
        lblFtypAllShadow(0).Top = NewTop + 30
        lblFtypAllShadow(0).Left = lblFtypAll(0).Left + 15
        
        NewTop = chkFullSize.Top
        Index = -1
        For CurFilter = 1 To FilterCount
            FtypLayout = GetFilterConfig(LayoutStyle, "FTYP_LAYOUT", CurFilter)
            Index = Index + 1
            NewSection = False
            If Index > chkFtypFilter.UBound Then
                Load chkFtypFilter(Index)
                Load lblFtypName(Index)
                Load lblFtypShadow(Index)
                Set chkFtypFilter(Index).Container = FtypPanel
                Set lblFtypName(Index).Container = FtypPanel
                Set lblFtypShadow(Index).Container = FtypPanel
                NewSection = True
            End If
            chkFtypFilter(Index).Top = NewTop
            chkFtypFilter(Index).Left = NewLeft
            lblFtypName(Index).Top = NewTop + 15
            lblFtypName(Index).Left = chkFtypFilter(Index).Left + chkFtypFilter(Index).Width + 60
            lblFtypShadow(Index).Top = NewTop + 30
            lblFtypShadow(Index).Left = lblFtypName(Index).Left + 15
            chkFtypFilter(Index).Caption = GetItem(FtypLayout, 1, ",")
            lblFtypName(Index).Caption = GetItem(FtypLayout, 2, ",")
            lblFtypShadow(Index).Caption = lblFtypName(Index).Caption
            lblFtypName(Index).ZOrder
            If (LayoutChanged) Or (NewSection) Then
                tmpItem = GetItem(FtypLayout, 3, ",")
                If tmpItem = "Y" Then chkFtypFilter(Index).Value = 1 Else chkFtypFilter(Index).Value = 0
            End If
            tmpItem = GetItem(FtypLayout, 4, ",")
            If tmpItem = "Y" Then chkFtypFilter(Index).Enabled = True Else chkFtypFilter(Index).Enabled = False
            lblFtypName(Index).ToolTipText = GetItem(FtypLayout, 5, ",")
            chkFtypFilter(Index).Visible = True
            lblFtypName(Index).Visible = True
            lblFtypShadow(Index).Visible = True
            NewTop = NewTop + chkFtypFilter(Index).Height + 15
        Next
        FtypPanel.Align = 0
        FtypPanel.Height = Screen.Height
        DrawBackGround FtypPanel, 7, False, True
        FtypPanel.Align = 4
        FtypPanel.Visible = True
        Me.Width = Me.Width + FtypPanel.Width
    End If
    LoadingConfig = False
End Sub

Private Function BuildDummyHeader(SizeList As String) As String
    Dim Result As String
    Dim ItemNo As Long
    Dim ItemVal As String
    Dim ColWidth As Long
    ItemNo = 0
    ItemVal = GetRealItem(SizeList, ItemNo, ",")
    While ItemVal <> ""
        ColWidth = Val(ItemVal)
        'QuickHack
        If ColWidth < 1 Then ColWidth = 1
        Result = Result & String(ColWidth, "-") & ","
        ItemNo = ItemNo + 1
        ItemVal = GetRealItem(SizeList, ItemNo, ",")
    Wend
    If Result <> "" Then Result = Left(Result, Len(Result) - 1)
    BuildDummyHeader = Result
End Function
Private Sub DefineDataConfig(CurTab As TABLib.Tab, LayoutStyle As String, PanelLayout As String)
    Dim TabCfgLine As String
    Dim TabCfgFields As String
    Dim TabCfgData As String
    Dim HiddenTab As String
    Dim tmpFldLst As String
    Dim tmpSqlKey As String
    Dim ItemNo As Long
    Dim ItemVal As String
    TabCfgLine = CurTab.myName & Chr(15)
    TabCfgLine = TabCfgLine & "0" & Chr(15)
    TabCfgLine = TabCfgLine & "V" & Chr(15)
    TabCfgLine = TabCfgLine & "SEL" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & CurTab.LogicalFieldList & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    HiddenData.CheckTabConfig TabCfgLine
    HiddenTab = HiddenData.CreateFilterTab(CurTab.Index)
    HiddenData.UpdateTabConfig CurTab.myName, "DSRC", HiddenTab
    
    TabCfgLine = GetFilterConfig(LayoutStyle, "READAODB," & CStr(CurTab.Index), -1)
    TabCfgFields = "DTAB,LFLD,DFLD,DSQL"
    TabCfgData = ""
    TabCfgData = TabCfgData & GetRealItem(PanelLayout, 5, "|") & Chr(15)
    TabCfgData = TabCfgData & GetRealItem(PanelLayout, 3, "|") & Chr(15)
    ItemNo = 6
    ItemVal = GetRealItem(PanelLayout, ItemNo, "|")
    While ItemVal <> ""
        tmpFldLst = tmpFldLst & ItemVal & Chr(14)
        ItemNo = ItemNo + 1
        ItemVal = GetRealItem(PanelLayout, ItemNo, "|")
        tmpSqlKey = tmpSqlKey & ItemVal & Chr(14)
        ItemNo = ItemNo + 1
        ItemVal = GetRealItem(PanelLayout, ItemNo, "|")
    Wend
    If tmpFldLst <> "" Then tmpFldLst = Left(tmpFldLst, Len(tmpFldLst) - 1)
    If tmpSqlKey <> "" Then tmpSqlKey = Left(tmpSqlKey, Len(tmpSqlKey) - 1)
    TabCfgData = TabCfgData & tmpFldLst & Chr(15) & tmpSqlKey
    
    HiddenData.UpdateTabConfig HiddenTab, TabCfgFields, TabCfgData
End Sub
Private Function GetFilterConfig(LayoutType As String, ForWhat As String, Index As Integer) As String
    Dim Result As String
    Dim TypeCode As String
    Dim WhatCode As String
    Dim UseIniKey As String
    Result = ""
    TypeCode = GetRealItem(LayoutType, 0, ",")
    WhatCode = GetRealItem(ForWhat, 0, ",")
    Select Case TypeCode
        Case "AODB_FILTER"
            Select Case WhatCode
                Case "FILTER_PANELS"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", "AODB_FILTER_PANELS", "0")
                Case "FILTER_CAPTION"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", "AODB_FILTER_CAPTION", "AODB Flight Data Filter")
                Case "PANEL_LAYOUT"
                    UseIniKey = "AODB_FILTER_PANEL_" & CStr(Index)
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", UseIniKey, "??|??,??")
                Case "FILTER_FTYPES"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", "AODB_FILTER_FTYPES", "0")
                Case "FTYP_LAYOUT"
                    UseIniKey = "AODB_FILTER_FTYP_" & CStr(Index)
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", UseIniKey, "?,???,???")
                Case Else
            End Select
        Case Else
    End Select
    GetFilterConfig = Result
End Function
Private Sub chkAppl_Click(Index As Integer)
    If chkAppl(Index).Value = 1 Then
        chkAppl(Index).BackColor = LightGreen
        chkAppl(Index).Refresh
        Select Case chkAppl(Index).Tag
            Case "CLOSE"
                HandleClose chkAppl(Index)
            Case "AODB_FILTER"
                HandleAodbFilter chkAppl(Index), True
            Case "AODBREFRESH"
                HandleAodbRefresh chkAppl(Index)
            Case "RESETFILTER"
                HandleResetFilter chkAppl(Index)
            Case Else
                chkAppl(Index).Value = 0
        End Select
    Else
        chkAppl(Index).BackColor = MyOwnButtonFace
    End If
End Sub

Private Sub HandleResetFilter(CurButton As CheckBox)
    Dim LCnt As Long
    Dim i As Integer
    LCnt = 0
    For i = 0 To chkFilterIsVisible.UBound
        If chkFilterIsVisible(i).Visible Then LCnt = LCnt + RightFilterTab(i).GetLineCount
    Next
    If LCnt > 0 Then
        For i = 0 To chkFilterIsVisible.UBound
            If chkFilterIsVisible(i).Visible Then
                chkRightFilterSet(i).Value = 1
                txtFilterVal(i).Text = "0"
                'chkWork(i).Value = 0
            End If
        Next
    Else
        For i = 0 To chkFilterIsVisible.UBound
            If chkFilterIsVisible(i).Visible Then
                LeftFilterTab(i).ResetContent
                chkLeftFilterSet(i).Caption = "0"
                chkRightFilterSet(i).Caption = "0"
                txtFilterVal(i).Text = ""
                LeftFilterTab(i).Refresh
                chkWork(i).Value = 0
                chkWork(i).Tag = ""
            End If
        Next
    End If
    CurButton.Value = 0
End Sub
Private Sub HandleToggleFilter(CurButton As CheckBox, Index As Integer)
    Dim i As Integer
    For i = 0 To FilterPanel.UBound
        If (Index < 0) Or (Index = i) Then
            'If chkFilterIsVisible(i).Visible Then
                ToggleTabColumns LeftFilterTab(i)
                ToggleTabColumns RightFilterTab(i)
            'End If
        End If
    Next
End Sub
Private Sub ToggleTabColumns(CurTab As TABLib.Tab)
    Dim tmpLogFields As String
    Dim tmpNewFields As String
    Dim tmpBuff As String
    Dim LineNo As Long
    tmpLogFields = CurTab.LogicalFieldList
    tmpNewFields = GetRealItem(tmpLogFields, 1, ",") & "," & GetRealItem(tmpLogFields, 0, ",")
    LineNo = CurTab.GetLineCount - 1
    If LineNo >= 0 Then
        tmpBuff = CurTab.GetBufferByFieldList(0, LineNo, tmpNewFields, vbLf)
        CurTab.ResetContent
        CurTab.InsertBuffer tmpBuff, vbLf
    End If
    CurTab.LogicalFieldList = tmpNewFields
    tmpLogFields = CurTab.HeaderString
    tmpNewFields = GetRealItem(tmpLogFields, 1, ",") & "," & GetRealItem(tmpLogFields, 0, ",") & "," & GetRealItem(tmpLogFields, 2, ",")
    CurTab.HeaderString = tmpNewFields
    CurTab.Sort "0", True, True
    CurTab.Sort "2", True, True
    CurTab.AutoSizeColumns
    CurTab.Refresh
End Sub
Private Sub HandleAodbRefresh(CurButton As CheckBox)
    Dim i As Integer
    For i = 0 To FilterPanel.UBound
        If FilterPanel(i).Visible Then
            If chkWork(i).Value = 1 Then
                chkWork(i).Value = 0
                chkWork(i).Value = 1
            End If
        End If
    Next
    CurButton.Value = 0
End Sub
Private Sub HandleAodbFilter(CurButton As CheckBox, CloseWin As Boolean)
    Dim tmpMsg As String
    Dim tmpFields As String
    Dim tmpTest As String
    Dim tmpData As String
    Dim tmpBuff As String
    Dim tmpFld As String
    Dim i As Integer
    Dim j As Long
    Dim LineNo As Long
    tmpMsg = ""
    If CloseWin Then
        If txtVpfr(0).Tag = "" Then tmpMsg = tmpMsg & "Period Begin" & vbNewLine
        If txtVpto(0).Tag = "" Then tmpMsg = tmpMsg & "Period End" & vbNewLine
    End If
    If tmpMsg = "" Then
        DataFilterVpfr = txtVpfr(1).Tag & txtVpfr(2).Tag
        DataFilterVpto = txtVpto(1).Tag & txtVpto(2).Tag
        MyGlobalResult = ""
        tmpFields = "ZONE" & Chr(15) & "VPFR" & Chr(15) & "VPTO" & Chr(15) & "HOPO"
        tmpFields = tmpFields & Chr(15) & "TIFR" & Chr(15) & "TITO"
        tmpData = optLocal.Tag & Chr(15) & txtVpfr(1).Tag & Chr(15) & txtVpto(1).Tag & Chr(15) & UfisServer.HOPO
        tmpData = tmpData & Chr(15) & txtVpfr(2).Tag & Chr(15) & txtVpto(2).Tag
        If (FtypPanel.Visible) And (txtFtypFilter.Text <> "") Then
            tmpFields = tmpFields & Chr(15) & "FTYP"
            tmpData = tmpData & Chr(15) & txtFtypFilter.Text
        End If
        For i = 0 To FilterPanel.UBound
            If FilterPanel(i).Visible Then
                If chkWork(i).Value = 1 Then
                    If LeftFilterTab(i).GetLineCount > 0 Then
                        LineNo = RightFilterTab(i).GetLineCount - 1
                        If LineNo >= 0 Then
                            tmpTest = RightFilterTab(i).LogicalFieldList
                            tmpFld = RightFilterTab(i).myTag
                            tmpFields = tmpFields & Chr(15) & tmpFld
                            tmpBuff = RightFilterTab(i).GetBufferByFieldList(0, LineNo, tmpFld, ",")
                            tmpBuff = "'" & Replace(tmpBuff, ",", "','", 1, -1, vbBinaryCompare) & "'"
                            tmpBuff = Replace(tmpBuff, "''", "' '", 1, -1, vbBinaryCompare)
                            tmpData = tmpData & Chr(15) & tmpBuff
                        End If
                    End If
                End If
            End If
        Next
        MyGlobalResult = tmpFields & Chr(16) & tmpData
        If CloseWin Then Me.Hide
    Else
        tmpMsg = "Missing or wrong input for:" & vbNewLine & tmpMsg
        If MyMsgBox.CallAskUser(0, 0, 0, "Action Control", tmpMsg, "stop2", "", UserAnswer) >= 0 Then DoNothing
    End If
    If CloseWin Then CurButton.Value = 0
End Sub
Private Sub HandleClose(CurButton As CheckBox)
    MyGlobalResult = ""
    Me.Hide
    CurButton.Value = 0
End Sub

Private Sub chkFilterIsVisible_Click(Index As Integer)
    Dim MyOldWidth As Long
    Dim MyNewWidth As Long
    Dim NewLeft As Long
    Dim TabWidth As Long
    Dim WidthList As String
    Dim i As Integer
    MyOldWidth = Me.Width
    NewLeft = FilterPanel(0).Left
    For i = 0 To LeftFilterTab.UBound
        If RightFilterTab(i).myTag <> "" Then
            If chkFilterIsVisible(i).Value = 1 Then
                FilterPanel(i).Left = NewLeft
                FilterPanel(i).Visible = True
                NewLeft = FilterPanel(i).Left + FilterPanel(i).Width + 120
            Else
                FilterPanel(i).Visible = False
            End If
        End If
    Next
    InternalResize = True
    MyNewWidth = NewLeft + RightPanel.Width + 180
    If FtypPanel.Visible Then MyNewWidth = MyNewWidth + FtypPanel.Width
    NewLeft = Me.Left + MyOldWidth - MyNewWidth
    Me.Move NewLeft, Me.Top, MyNewWidth, Me.Height
    InternalResize = False
    Form_Resize
End Sub

Private Sub chkFtypAll_Click(Index As Integer)
    Dim i As Integer
    If (Not ButtonPushedByUser) And (Not LoadingConfig) Then
        StopNestedCalls = True
        For i = 0 To chkFtypFilter.UBound
            If chkFtypFilter(i).Visible Then
                If chkFtypFilter(i).Enabled Then
                    chkFtypFilter(i).Value = (1 - chkFtypAll(Index).Value)
                End If
            End If
        Next
        StopNestedCalls = False
    End If
End Sub

Private Sub chkFtypFilter_Click(Index As Integer)
    If (Not StopNestedCalls) Then ButtonPushedByUser = True
    CheckFtypFilter
    ButtonPushedByUser = False
End Sub
Private Sub CheckFtypFilter()
    Dim tmpData As String
    Dim i As Integer
    tmpData = ""
    For i = 0 To chkFtypFilter.UBound
        If chkFtypFilter(i).Visible Then
            If chkFtypFilter(i).Value = 1 Then
                tmpData = tmpData & "'" & chkFtypFilter(i).Caption & "',"
            End If
        End If
    Next
    If tmpData <> "" Then
        tmpData = Left(tmpData, Len(tmpData) - 1)
        If (Not StopNestedCalls) And (Not LoadingConfig) Then chkFtypAll(0).Value = 0
    Else
        If (Not StopNestedCalls) And (Not LoadingConfig) Then chkFtypAll(0).Value = 1
    End If
    txtFtypFilter.Text = tmpData
End Sub
Private Sub chkFullSize_Click()
    Dim MyOldWidth As Long
    Dim MyNewWidth As Long
    Dim NewLeft As Long
    Dim TabWidth As Long
    Dim WidthList As String
    Dim i As Integer
    MyOldWidth = Me.Width
    NewLeft = FilterPanel(0).Left
    For i = 0 To LeftFilterTab.UBound
        If RightFilterTab(i).myTag <> "" Then
            WidthList = LeftFilterTab(i).HeaderLengthString
            TabWidth = Val(GetRealItem(WidthList, 0, ",")) * 15
            If chkFullSize.Value = 1 Then TabWidth = TabWidth + Val(GetRealItem(WidthList, 1, ",")) * 15
            TabWidth = TabWidth + 16 * 15 'ScrollBar
            If TabWidth < 855 Then TabWidth = 855
            LeftFilterTab(i).Width = TabWidth
            RightFilterTab(i).Width = TabWidth
            RightFilterTab(i).Left = LeftFilterTab(i).Left + TabWidth + 15
            FilterPanel(i).Left = NewLeft
            FilterPanel(i).Width = RightFilterTab(i).Left + RightFilterTab(i).Width + 60
            chkWork(i).Width = TabWidth
            txtFilterVal(i).Width = TabWidth
            txtFilterVal(i).Left = RightFilterTab(i).Left
            FilterPanel(i).Width = RightFilterTab(i).Left + RightFilterTab(i).Width + 60
            
            chkLeftFilterSet(i).Width = TabWidth - 90
            chkRightFilterSet(i).Width = TabWidth - 90
            chkLeftFilterSet(i).Left = LeftFilterTab(i).Left + ((TabWidth - chkLeftFilterSet(i).Width) / 2)
            chkRightFilterSet(i).Left = RightFilterTab(i).Left + ((TabWidth - chkRightFilterSet(i).Width) / 2) - 15
            
            ButtonPanel(i).Left = -30
            ButtonPanel(i).Width = FilterPanel(i).Width + 30
            DrawBackGround ButtonPanel(i), 7, True, True
            
            NewLeft = FilterPanel(i).Left + FilterPanel(i).Width + 120
        End If
    Next
    InternalResize = True
    MyNewWidth = NewLeft + RightPanel.Width + 180
    NewLeft = Me.Left + MyOldWidth - MyNewWidth
    Me.Move NewLeft, Me.Top, MyNewWidth, Me.Height
    InternalResize = False
    Form_Resize
End Sub

Private Sub chkLeftFilterSet_Click(Index As Integer)
    Dim tmpBuff As String
    Dim LineNo As Long
    If chkLeftFilterSet(Index).Value = 1 Then
        LineNo = LeftFilterTab(Index).GetLineCount - 1
        If LineNo >= 0 Then
            tmpBuff = LeftFilterTab(Index).GetBuffer(0, LineNo, vbLf)
            RightFilterTab(Index).InsertBuffer tmpBuff, vbLf
            LeftFilterTab(Index).ResetContent
            LeftFilterTab(Index).Refresh
            RightFilterTab(Index).Sort "0", True, True
            RightFilterTab(Index).Refresh
            chkLeftFilterSet(Index).Caption = "0"
            chkRightFilterSet(Index).Caption = CStr(RightFilterTab(Index).GetLineCount)
        End If
        chkLeftFilterSet(Index).Value = 0
    Else
    End If
End Sub

Private Sub chkRightFilterSet_Click(Index As Integer)
    Dim tmpBuff As String
    Dim LineNo As Long
    If chkRightFilterSet(Index).Value = 1 Then
        LineNo = RightFilterTab(Index).GetLineCount - 1
        If LineNo >= 0 Then
            tmpBuff = RightFilterTab(Index).GetBuffer(0, LineNo, vbLf)
            LeftFilterTab(Index).InsertBuffer tmpBuff, vbLf
            RightFilterTab(Index).ResetContent
            RightFilterTab(Index).Refresh
            LeftFilterTab(Index).Sort "0", True, True
            LeftFilterTab(Index).Refresh
            chkRightFilterSet(Index).Caption = "0"
            chkLeftFilterSet(Index).Caption = CStr(LeftFilterTab(Index).GetLineCount)
        End If
        chkRightFilterSet(Index).Value = 0
    Else
    End If
End Sub

Private Sub chkToggle_Click()
    HandleToggleFilter chkToggle, -1
End Sub

Private Sub chkWork_Click(Index As Integer)
    On Error Resume Next
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        CheckFilterTask Index, chkWork(Index), LeftFilterTab(Index)
        txtFilterVal(Index).Enabled = True
        txtFilterVal(Index).BackColor = vbWhite
        LeftFilterTab(Index).DisplayBackColor = LightYellow
        LeftFilterTab(Index).DisplayTextColor = vbBlack
        LeftFilterTab(Index).Enabled = True
        LeftFilterTab(Index).AutoSizeColumns
        LeftFilterTab(Index).Refresh
        RightFilterTab(Index).DisplayBackColor = NormalBlue
        RightFilterTab(Index).DisplayTextColor = vbWhite
        RightFilterTab(Index).Enabled = True
        RightFilterTab(Index).AutoSizeColumns
        RightFilterTab(Index).Refresh
        chkLeftFilterSet(Index).Enabled = True
        chkRightFilterSet(Index).Enabled = True
        txtFilterVal(Index).SetFocus
    Else
        chkWork(Index).BackColor = MyOwnButtonFace
        txtFilterVal(Index).BackColor = LightGray
        txtFilterVal(Index).Enabled = False
        LeftFilterTab(Index).DisplayBackColor = LightGray
        LeftFilterTab(Index).DisplayTextColor = vbBlack
        LeftFilterTab(Index).Refresh
        LeftFilterTab(Index).Enabled = False
        RightFilterTab(Index).DisplayBackColor = LightGray
        RightFilterTab(Index).DisplayTextColor = vbBlack
        RightFilterTab(Index).Refresh
        RightFilterTab(Index).Enabled = False
        chkLeftFilterSet(Index).Enabled = False
        chkRightFilterSet(Index).Enabled = False
    End If
End Sub

Private Function CheckFilterTask(Index As Integer, CurButton As CheckBox, CurTab As TABLib.Tab) As Boolean
    Dim NewData As Boolean
    Dim tmpTag As String
    Dim tmpLayout As String
    Dim tmpTimeFrame As String
    Dim AodbResult As String
    Dim PatchCodes As String
    Dim PatchValues As String
    Dim FilterList As String
    NewData = False
    If WorkArea.Visible Then
        tmpTag = Me.Tag
        tmpLayout = GetRealItem(tmpTag, 0, ",")
        Select Case tmpLayout
            Case "AODB_FILTER"
                HandleAodbFilter CurButton, False
                FilterList = MyGlobalResult
                tmpTimeFrame = txtVpfr(0).Tag & "/" & txtVpto(0).Tag
                If Len(tmpTimeFrame) = 17 Then
                    tmpTag = CurButton.Tag
                    If tmpTag <> FilterList Then
                        If RightFilterTab(Index).GetLineCount = 0 Then
                            CurTab.ResetContent
                            CurTab.Refresh
                            RightFilterTab(Index).ResetContent
                            RightFilterTab(Index).Refresh
                            chkRightFilterSet(Index).Caption = "0"
                            PatchCodes = GetRealItem(FilterList, 0, Chr(16))
                            PatchValues = GetRealItem(FilterList, 1, Chr(16))
                            AodbResult = HiddenData.GetFilterData(CurTab.myName, CurTab.LogicalFieldList, PatchCodes, PatchValues)
                            CurTab.InsertBuffer AodbResult, vbLf
                            CurButton.Tag = FilterList
                            CurTab.Sort "0", True, True
                            CurTab.Sort "2", True, True
                            chkLeftFilterSet(Index).Caption = CStr(CurTab.GetLineCount)
                            NewData = True
                        End If
                    End If
                End If
            Case Else
        End Select
    End If
    CheckFilterTask = NewData
End Function

Private Sub chkWork_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then HandleToggleFilter chkFilterIsVisible(Index), Index
End Sub

Private Sub Form_Activate()
    FtypPanel.Align = 0
    RightPanel.Align = 0
    RightPanel.Align = 4
    FtypPanel.Align = 4
    CheckFtypFilter
End Sub

Private Sub Form_Load()
    Dim i As Integer
    SetButtonFaceStyle Me
    For i = 0 To chkFtypFilter.UBound
        chkFtypFilter(i).BackColor = LightGray
    Next
    WorkArea.Top = 0
    WorkArea.Left = 0
    lblFullSize(0).Top = chkFullSize.Top + 15
    lblFullSize(0).Left = chkFullSize.Left + chkFullSize.Width + 15
    lblFullSize(1).Left = chkFullSize.Left + chkFullSize.Width + 30
    lblFullSize(1).Top = lblFullSize(0).Top + 15
    lblToggle(0).Top = chkToggle.Top + 15
    lblToggle(0).Left = chkToggle.Left + chkToggle.Width + 15
    lblToggle(1).Left = chkToggle.Left + chkToggle.Width + 30
    lblToggle(1).Top = lblToggle(0).Top + 15
    StatusBar.ZOrder
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        MyGlobalResult = ""
        Me.Tag = ""
        Me.Hide
        Cancel = True
    End If
End Sub
'
Private Sub Form_Resize()
    Dim i As Integer
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim MaxLin As Long
    If Not InternalResize Then
        WorkArea.Visible = False
        If RightPanel.Visible Then
            WorkArea.Width = Me.ScaleWidth - RightPanel.Width
        Else
            WorkArea.Width = Me.ScaleWidth
        End If
        If FtypPanel.Visible Then
            If WorkArea.Width > (FtypPanel.Width + 300) Then
                WorkArea.Width = WorkArea.Width - FtypPanel.Width
            End If
        End If
        TopPanel.Width = WorkArea.Width
        NewHeight = Me.ScaleHeight - StatusBar.Height - WorkArea.Top
        If NewHeight > 1200 Then
            WorkArea.Height = NewHeight
            NewHeight = NewHeight - 300
            NewTop = NewHeight - ButtonPanel(0).Height - 30
            For i = 0 To FilterPanel.UBound
                FilterPanel(i).Height = NewHeight
                ButtonPanel(i).Top = NewTop
            Next
            NewHeight = ButtonPanel(0).Top - LeftFilterTab(0).Top - 15
            For i = 0 To LeftFilterTab.UBound
                LeftFilterTab(i).Height = NewHeight
                RightFilterTab(i).Height = NewHeight
            Next
        End If
        DrawBackGround WorkArea, 7, True, True
        WorkArea.Visible = True
        NewLeft = 0
        For i = FilterPanel.UBound To 0 Step -1
            If FilterPanel(i).Visible Then
                NewLeft = FilterPanel(i).Left + (FilterPanel(i).Width / 2)
                Exit For
            End If
        Next
        If WorkArea.Width >= NewLeft Then
            FilterScroll.Visible = False
        Else
            FilterScroll.Visible = True
            FilterScroll.ZOrder
        End If
    End If
End Sub

Private Sub lblFilterName_Click(Index As Integer)
    If chkFilterIsVisible(Index).Value = 0 Then chkFilterIsVisible(Index).Value = 1 Else chkFilterIsVisible(Index).Value = 0
End Sub

Private Sub lblFtypAll_Click(Index As Integer)
    If chkFtypAll(Index).Value = 0 Then chkFtypAll(Index).Value = 1 Else chkFtypAll(Index).Value = 0
End Sub

Private Sub lblFtypName_Click(Index As Integer)
    If chkFtypFilter(Index).Enabled = True Then
        If chkFtypFilter(Index).Value = 0 Then chkFtypFilter(Index).Value = 1 Else chkFtypFilter(Index).Value = 0
    End If
End Sub

Private Sub lblFullSize_Click(Index As Integer)
    If Index = 0 Then
        If chkFullSize.Value = 0 Then chkFullSize.Value = 1 Else chkFullSize.Value = 0
    End If
End Sub

Private Sub lblToggle_Click(Index As Integer)
    If chkToggle.Value = 0 Then chkToggle.Value = 1 Else chkToggle.Value = 0
End Sub

Private Sub LeftFilterTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpBuff As String
    If LineNo >= 0 Then
        tmpBuff = LeftFilterTab(Index).GetLineValues(LineNo)
        RightFilterTab(Index).InsertTextLine tmpBuff, False
        LeftFilterTab(Index).DeleteLine LineNo
        RightFilterTab(Index).Sort "0", True, True
        LeftFilterTab(Index).Refresh
        RightFilterTab(Index).Refresh
        chkLeftFilterSet(Index).Caption = CStr(LeftFilterTab(Index).GetLineCount)
        chkRightFilterSet(Index).Caption = CStr(RightFilterTab(Index).GetLineCount)
        CheckRefreshSettings
    End If
End Sub

Private Sub optLocal_Click()
    optLocal.BackColor = LightGreen
    optUTC.BackColor = vbButtonFace
    optLocal.Tag = "LOC"
    txtVpfr(1).Tag = CedaDateAdd(txtVpfr(0).Tag, -1)
    txtVpfr(2).Tag = "160000"
    txtVpto(1).Tag = txtVpto(0).Tag
    txtVpto(2).Tag = "155959"
End Sub

Private Sub optUTC_Click()
    optUTC.BackColor = LightGreen
    optLocal.BackColor = vbButtonFace
    optLocal.Tag = "UTC"
    txtVpfr(1).Tag = txtVpfr(0).Tag
    txtVpfr(2).Tag = "000000"
    txtVpto(1).Tag = txtVpto(0).Tag
    txtVpto(2).Tag = "235959"
End Sub

Private Sub RightFilterTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpBuff As String
    If LineNo >= 0 Then
        tmpBuff = RightFilterTab(Index).GetLineValues(LineNo)
        LeftFilterTab(Index).InsertTextLine tmpBuff, False
        RightFilterTab(Index).DeleteLine LineNo
        LeftFilterTab(Index).Sort "0", True, True
        LeftFilterTab(Index).Refresh
        RightFilterTab(Index).Refresh
        chkLeftFilterSet(Index).Caption = CStr(LeftFilterTab(Index).GetLineCount)
        chkRightFilterSet(Index).Caption = CStr(RightFilterTab(Index).GetLineCount)
        CheckRefreshSettings
    End If
End Sub

Private Sub txtFilterVal_Change(Index As Integer)
    SearchInList Index, 1
End Sub

Private Sub txtFilterVal_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtFilterVal_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim LineNo As Long
    Dim HitText As String
    If KeyAscii = 13 Then
        LineNo = LeftFilterTab(Index).GetCurrentSelected
        If LineNo >= 0 Then
            HitText = LeftFilterTab(Index).GetColumnValue(LineNo, 0)
            If InStr(HitText, txtFilterVal(Index).Text) > 0 Then
                LeftFilterTab_SendLButtonDblClick Index, LineNo, 0
                SetTextSelected
            End If
        End If
        KeyAscii = 0
    Else
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End If
End Sub

Private Sub txtVpfr_Change(Index As Integer)
    Dim tmpCedaDate As String
    Dim i As Integer
    On Error Resume Next
    tmpCedaDate = ""
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(0).Text)
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(1).Text)
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(2).Text)
    If CheckValidDate(tmpCedaDate) = True Then
        txtVpfr(0).Tag = tmpCedaDate
        If optUTC.Value = True Then
            txtVpfr(1).Tag = tmpCedaDate
        Else
            txtVpfr(1).Tag = CedaDateAdd(tmpCedaDate, -1)
        End If
        For i = 0 To 2
            txtVpfr(i).BackColor = vbYellow
            txtVpfr(i).ForeColor = vbBlack
        Next
        AdjustFilterButtons True
        If Len(txtVpfr(Index).Text) = txtVpfr(Index).MaxLength Then
            If Index < 2 Then
                txtVpfr(Index + 1).SetFocus
            Else
                txtVpto(0).SetFocus
            End If
        End If
    Else
        txtVpfr(0).Tag = ""
        txtVpfr(Index).BackColor = vbRed
        txtVpfr(Index).ForeColor = vbWhite
        AdjustFilterButtons False
    End If
End Sub
Private Sub txtVpfr_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtVpto_Change(Index As Integer)
    Dim tmpCedaDate As String
    Dim i As Integer
    On Error Resume Next
    tmpCedaDate = ""
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(0).Text)
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(1).Text)
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(2).Text)
    If CheckValidDate(tmpCedaDate) = True Then
        txtVpto(0).Tag = tmpCedaDate
        If optUTC.Value = True Then
            txtVpto(1).Tag = tmpCedaDate
        Else
            'txtVpto(1).Tag = CedaDateAdd(tmpCedaDate, -1)
            txtVpto(1).Tag = tmpCedaDate
        End If
        For i = 0 To 2
            txtVpto(i).BackColor = vbYellow
            txtVpto(i).ForeColor = vbBlack
        Next
        AdjustFilterButtons True
        If Len(txtVpto(Index).Text) = txtVpto(Index).MaxLength Then
            If Index < 2 Then
                txtVpto(Index + 1).SetFocus
            Else
                'txtVpto(0).SetFocus
            End If
        End If
    Else
        txtVpto(0).Tag = ""
        txtVpto(Index).BackColor = vbRed
        txtVpto(Index).ForeColor = vbWhite
        AdjustFilterButtons False
    End If
End Sub

Private Sub txtVpto_GotFocus(Index As Integer)
    SetTextSelected
End Sub
Private Sub AdjustFilterButtons(SetEnable As Boolean)
    Dim i As Integer
    For i = 0 To FilterPanel.UBound
        If FilterPanel(i).Visible Then
            chkWork(i).Enabled = SetEnable
        End If
    Next
    AdjustApplButton "AODB_FILTER", SetEnable
    AdjustApplButton "FILE_FILTER", SetEnable
    AdjustApplButton "TOGGLE", SetEnable
    If SetEnable = True Then
        CheckRefreshSettings
    Else
        AdjustApplButton "AODBREFRESH", SetEnable
        AdjustApplButton "FILEREFRESH", SetEnable
    End If
End Sub
Private Sub AdjustApplButton(KeyCode As String, SetEnable As Boolean)
    Dim i As Integer
    For i = 0 To chkAppl.UBound
        If chkAppl(i).Tag = KeyCode Then
            chkAppl(i).Enabled = SetEnable
            Exit For
        End If
    Next
End Sub
Private Sub CheckRefreshSettings()
    'Dim tmpTimeFrame As String
    Dim tmpTag As String
    Dim FilterList As String
    Dim ChkValue As Boolean
    Dim i As Integer
    ChkValue = False
    'tmpTimeFrame = txtVpfr(0).Tag & "/" & txtVpto(0).Tag
    HandleAodbFilter chkWork(0), False
    FilterList = MyGlobalResult
    For i = 0 To FilterPanel.UBound
        If FilterPanel(i).Visible Then
            If chkWork(i).Value = 1 Then
                tmpTag = chkWork(i).Tag
                If tmpTag <> FilterList Then
                    ChkValue = True
                    Exit For
                End If
            End If
        End If
    Next
    'QuickHack
    ChkValue = True
    AdjustApplButton "AODBREFRESH", ChkValue
    AdjustApplButton "FILEREFRESH", ChkValue
End Sub

Private Sub SearchInList(UseIndex As Integer, Method As Integer)
    Static LastIndex As Integer
    Dim HitLst As String
    Dim HitRow As Long
    Dim NxtRow As Long
    Dim NewScroll As Long
    Dim LoopCount As Integer
    Dim tmpText As String
    Dim ColNbr As String
    Dim Index As Integer
    Dim UseCol As Long
    Index = UseIndex
    If Index < 0 Then Index = LastIndex
    LastIndex = Index
    tmpText = Trim(txtFilterVal(Index).Text)
    UseCol = 0
    If tmpText <> "" Then
        LoopCount = 0
        Do
            HitLst = LeftFilterTab(Index).GetNextLineByColumnValue(UseCol, tmpText, Method)
            HitRow = Val(HitLst)
            If HitLst <> "" Then
                If HitRow > 5 Then NewScroll = HitRow - 5 Else NewScroll = 0
                LeftFilterTab(Index).OnVScrollTo NewScroll
            Else
                LeftFilterTab(Index).SetCurrentSelection -1
                LeftFilterTab(Index).OnVScrollTo 0
            End If
            LoopCount = LoopCount + 1
        Loop While LoopCount < 2 And HitLst = ""
        LeftFilterTab(Index).SetCurrentSelection HitRow
    End If
    txtFilterVal(Index).SetFocus
End Sub


