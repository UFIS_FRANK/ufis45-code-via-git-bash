VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{6782E133-3223-11D4-996A-0000863DE95C}#1.1#0"; "UGantt.ocx"
Begin VB.Form GanttChartPos 
   Caption         =   "Position Gantt Chart"
   ClientHeight    =   5145
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11340
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "GanttChartPos.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5145
   ScaleWidth      =   11340
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   3
      Top             =   4860
      Width           =   11340
      _ExtentX        =   20003
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16907
         EndProperty
      EndProperty
   End
   Begin UGANTTLib.UGantt GanttChart 
      Height          =   3645
      Index           =   0
      Left            =   4680
      TabIndex        =   2
      Top             =   1020
      Width           =   6435
      _Version        =   65536
      _ExtentX        =   11351
      _ExtentY        =   6429
      _StockProps     =   64
   End
   Begin VB.CheckBox chkTask 
      Caption         =   "Close"
      Height          =   315
      Index           =   0
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   0
      Width           =   1035
   End
   Begin TABLib.TAB LineKeys 
      Height          =   3675
      Index           =   0
      Left            =   150
      TabIndex        =   0
      Top             =   1020
      Visible         =   0   'False
      Width           =   3585
      _Version        =   65536
      _ExtentX        =   6324
      _ExtentY        =   6482
      _StockProps     =   64
   End
   Begin MSComctlLib.Slider GanttSlider 
      Height          =   285
      Index           =   0
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Visible         =   0   'False
      Width           =   3465
      _ExtentX        =   6112
      _ExtentY        =   503
      _Version        =   393216
      Min             =   1
      Max             =   24
      SelStart        =   4
      Value           =   4
   End
End
Attribute VB_Name = "GanttChartPos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub InitMyForm()
    InitMyTabs
    InitThisChart 0, GanttChart(0)
End Sub

Private Sub InitMyTabs()
    Dim FldList As String
    Dim Index As Integer
    Index = 0
    FldList = "PNAM,URNO,LINE"
    LineKeys(Index).ResetContent
    LineKeys(Index).LogicalFieldList = FldList
    LineKeys(Index).HeaderString = FldList
    LineKeys(Index).HeaderLengthString = "10,10,10"
    LineKeys(Index).ColumnWidthString = "10,10,10"
    LineKeys(Index).AutoSizeByHeader = True
    LineKeys(Index).AutoSizeColumns
End Sub

Public Sub CreateMyChartView()
    InitThisChart 0, GanttChart(0)
    If TabBasPstTabIdx >= 0 Then
        CreateChartLeftScale TabBasPstTabIdx, GanttChart(0), TabBasPstTabTab, ""
        CreateChartSeparators TabBasPstTabIdx, GanttChart(0), TabBasPstTabTab, -1, False
        'CreateChartBackground TabBasPstTabIdx, GanttChart(0), TabBasPstTabTab, ""
        
        CreateChartCandyBars TabBlkPosTabIdx, GanttChart(0), TabBlkPosTabTab, "BLK"
        
        CreateChartFlightBars TabArrFlightsIdx, GanttChart(0), TabArrFlightsTab, "ARR"
        CreateChartFlightBars TabDepFlightsIdx, GanttChart(0), TabDepFlightsTab, "DEP"
        CreateChartFlightBars TabArrTowingIdx, GanttChart(0), TabArrTowingTab, "TOW"
        
    End If
End Sub
Public Sub InitThisChart(Index As Integer, CurGantt As UGANTTLib.UGantt)
    Dim GanttRange As String
    Dim strTimeFrameFrom As String
    Dim strTimeFrameTo As String
    Dim datTF As Date
    Dim datTT As Date
    Dim TFFrom As Date
    Dim TFTo As Date
    'Setting default layout and time frame
    CurGantt.ResetContent
    CurGantt.TabHeaderLengthString = "100"
    CurGantt.TabSetHeaderText ("Left Scale Grid")
    CurGantt.SplitterPosition = 100
    CurGantt.TabBodyFontName = "Arial"
    CurGantt.TabFontSize = 14
    CurGantt.TabHeaderFontName = "Arial"
    CurGantt.TabHeaderFontSize = 16
    CurGantt.TimeScaleHeaderHeight = 40
    CurGantt.BarOverlapOffset = 18
    CurGantt.BarNumberExpandLine = 0
    CurGantt.TabBodyFontBold = True
    CurGantt.TabHeaderFontBold = True
    CurGantt.LifeStyle = True
    'GanttRange = MainDialog.GetTabProperty(Index, "GANTT_RANGE", 1, "")
    
    'DataFilterVpfr , DataFilterVpto
    If DataFilterVpfr <> "" Then
        strTimeFrameFrom = DataFilterVpfr
        strTimeFrameTo = DataFilterVpto
        TFFrom = CedaFullDateToVb(strTimeFrameFrom)
        TFFrom = DateAdd("d", -1, TFFrom)
        TFTo = CedaFullDateToVb(strTimeFrameTo)
        TFTo = DateAdd("d", 1, TFTo)
    Else
        strTimeFrameFrom = GetTimeStamp(0)
        strTimeFrameTo = GetTimeStamp(0)
        TFFrom = CedaFullDateToVb(strTimeFrameFrom)
        TFFrom = DateAdd("d", -1, TFFrom)
        TFTo = CedaFullDateToVb(strTimeFrameTo)
        TFTo = DateAdd("d", 1, TFTo)
    End If
    If CurrentTimeCode = "LOC" Then
        TFFrom = DateAdd("n", 0, TFFrom)
        TFTo = DateAdd("n", 0, TFTo)
    Else
        TFFrom = DateAdd("n", UtcTimeDisp, TFFrom)
        TFTo = DateAdd("n", UtcTimeDisp, TFTo)
    End If
    CurGantt.TimeFrameFrom = TFFrom
    CurGantt.TimeFrameTo = TFTo
    CurGantt.TabLineHeight = 20
    'MainDialog.GanttSlider(Index).Value = Val(GanttRange)
    'CurGantt.TimeScaleDuration = MainDialog.GanttSlider(Index).Value
    CreateTimeSeparators Index, CurGantt, ""
End Sub

Public Sub CreateChartLeftScale(Index As Integer, CurGantt As UGANTTLib.UGantt, DataTab As TABLib.Tab, UseTabFields As String)
    Dim TabFieldLst As String
    Dim TabHeadStrg As String
    Dim TabHeadSize As String
    Dim GttHeadStrg As String
    Dim GttHeadSize As String
    Dim GanttScale As String
    Dim TabFields As String
    Dim TabScaleFields As String
    Dim TabFldName As String
    Dim TabFldData As String
    Dim TabUrno As String
    Dim GttLeftRec As String
    Dim KeyDataRec As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LeftWidth As Long
    Dim FldColNo As Long
    Dim Itm As Integer
    GanttScale = UseTabFields
    If GanttScale = "" Then
        'GanttScale = MainDialog.GetTabProperty(Index, "GANTT_SCALE", 1, "")
    End If
    If GanttScale = "" Then
        GanttScale = DataTab.LogicalFieldList
    End If
    TabFields = GanttScale
    TabFieldLst = DataTab.LogicalFieldList
    TabHeadStrg = DataTab.HeaderString
    TabHeadSize = DataTab.HeaderLengthString
    GttHeadStrg = ""
    GttHeadSize = ""
    TabScaleFields = ""
    LeftWidth = 0
    Itm = 0
    TabFldName = "START"
    While TabFldName <> ""
        Itm = Itm + 1
        TabFldName = GetItem(TabFields, Itm, ",")
        If TabFldName <> "" Then
            If (Left(TabFldName, 1) <> "'") And (TabFldName <> "URNO") Then
                FldColNo = GetRealItemNo(TabFieldLst, TabFldName)
                If FldColNo >= 0 Then
                    TabFldData = GetRealItem(TabHeadStrg, FldColNo, ",")
                    GttHeadStrg = GttHeadStrg & TabFldData & ","
                    TabFldData = GetRealItem(TabHeadSize, FldColNo, ",")
                    If InStr(MainDialog.ListOfCedaTimeFields, TabFldName) > 0 Then
                        TabFldData = "55"
                    End If
                    GttHeadSize = GttHeadSize & TabFldData & ","
                    LeftWidth = LeftWidth + Val(TabFldData)
                    TabScaleFields = TabScaleFields & TabFldName & ","
                End If
            End If
        End If
    Wend
    If GttHeadStrg <> "" Then
        TabFields = TabScaleFields
        If Right(TabFields, 1) = "," Then
            TabFields = Left(TabFields, Len(TabFields) - 1)
        End If
        
        GttHeadStrg = GttHeadStrg & "Remark"
        GttHeadSize = GttHeadSize & "100"
        CurGantt.ResetContent
        CurGantt.TabHeaderLengthString = GttHeadSize
        CurGantt.TabSetHeaderText GttHeadStrg
        CurGantt.SplitterPosition = LeftWidth
        'MainDialog.GanttSlider(Index).Width = (LeftWidth + 8) * 15
        'MainDialog.GanttIcon(Index).Left = (LeftWidth + 6) * 15
        CurGantt.Refresh
        LineKeys(0).ResetContent
        MaxLine = DataTab.GetLineCount
        CurGantt.TabLines = 0
        MaxLine = MaxLine - 1
        For CurLine = 0 To MaxLine
            TabUrno = DataTab.GetFieldValue(CurLine, "URNO")
            GttLeftRec = DataTab.GetFieldValues(CurLine, TabFields)
            GttLeftRec = AutoFormatFieldValues(TabFields, GttLeftRec, "hh:mm / dd.mm.yy", "hh:mm")
            GttLeftRec = GttLeftRec & ", "
            CurGantt.AddLineAt CurLine, TabUrno, GttLeftRec
            KeyDataRec = ""
            KeyDataRec = KeyDataRec & DataTab.GetFieldValues(CurLine, "PNAM") & ","
            KeyDataRec = KeyDataRec & TabUrno & ","
            KeyDataRec = KeyDataRec & CStr(CurLine)
            LineKeys(0).InsertTextLine KeyDataRec, False
        Next
        CurGantt.Refresh
        LineKeys(0).IndexCreate "PNAM", 0
        LineKeys(0).IndexCreate "URNO", 1
        LineKeys(0).AutoSizeColumns
    End If
End Sub
Public Sub CreateChartSeparators(Index As Integer, CurGantt As UGANTTLib.UGantt, DataTab As TABLib.Tab, LineNo As Long, SetSelected As Boolean)
    Dim GanttScale As String
    Dim FldName As String
    Dim CurData As String
    Dim NxtData As String
    Dim CurLine As Long
    Dim BgnLine As Long
    Dim MaxLine As Long
    Dim SepColor As Long
    Dim SepStyle As Integer
    Dim SepLineHeight As Integer
    Dim SepBarHeight As Integer
    Dim SepBegin As Integer
    Dim BckBarInfo As String
    FldName = "PNAM"
    If FldName = "" Then
        'GanttScale = MainDialog.GetTabProperty(Index, "GANTT_SCALE", 1, "")
        'FldName = GetItem(GanttScale, 1, ",")
    End If
    SepStyle = 0
    SepLineHeight = 1
    SepBarHeight = 3
    SepBegin = 1
    If LineNo >= 0 Then
        MaxLine = LineNo
        BgnLine = LineNo
        If SetSelected Then SepColor = vbCyan Else SepColor = vbBlack
    Else
        MaxLine = DataTab.GetLineCount - 1
        BgnLine = 0
        SepColor = vbBlack
    End If
    For CurLine = BgnLine To MaxLine
        If LineNo < 0 Then
            CurData = DataTab.GetFieldValue(CurLine, FldName)
            NxtData = DataTab.GetFieldValue(CurLine + 1, FldName)
            If NxtData <> CurData Then
                'If SetSelected Then
                    CurGantt.SetLineSeparator CurLine, SepLineHeight, SepColor, SepStyle, SepBarHeight, SepBegin
                    If LineNo > 0 Then
                        CurGantt.SetLineSeparator CurLine - 1, SepLineHeight, SepColor, SepStyle, SepBarHeight, SepBegin
                    End If
                'Else
                'End If
            Else
                'If Not SetSelected Then
                '    CurGantt.DeleteLineSeparator CurLine
                '    If LineNo > 0 Then
                '        CurGantt.DeleteLineSeparator CurLine - 1
                '    End If
                'Else
                'End If
            End If
        End If
        If LineNo >= 0 Then
            BckBarInfo = CurGantt.GetBkBarsByLine(CurLine)
            BckBarInfo = GetItem(BckBarInfo, 1, ";")
            If SetSelected Then
                CurGantt.SetBkBarColor BckBarInfo, LightestBlue
            Else
                CurGantt.SetBkBarColor BckBarInfo, vbBlue
            End If
        End If
    Next
    CurGantt.Refresh
    'CurGantt.RefreshArea "GANTT,TAB"
End Sub
Public Sub CreateChartBackground(Index As Integer, CurGantt As UGANTTLib.UGantt, DataTab As TABLib.Tab, TabFields As String)
    Dim BckBarTimes As String
    Dim BarBgnFldName As String
    Dim BarEndFldName As String
    Dim tmpFltBarBgn As String
    Dim tmpFltBarEnd As String
    Dim BarKey As String
    Dim BarTxt As String
    Dim BarBgn As Date
    Dim BarEnd As Date
    Dim BarColor As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    BarTxt = ""
    BarColor = vbBlue
    'BckBarTimes = MainDialog.GetTabProperty(Index, "GANTT_TIMES", 1, "")
    'If BckBarTimes <> "" Then
        MaxLine = DataTab.GetLineCount
        MaxLine = MaxLine - 1
        For CurLine = 0 To MaxLine
            BarKey = ""
            BarBgnFldName = GetItem(BckBarTimes, 1, ",")
            BarEndFldName = GetItem(BckBarTimes, 2, ",")
            tmpFltBarBgn = DataTab.GetFieldValue(CurLine, BarBgnFldName)
            tmpFltBarBgn = "20090101000000"
            If tmpFltBarBgn <> "" Then
                BarBgn = CedaFullDateToVb(tmpFltBarBgn)
                'BarBgn = DateAdd("d", -1, BarBgn)
                BarKey = DataTab.GetFieldValue(CurLine, "URNO")
            End If
            tmpFltBarEnd = DataTab.GetFieldValue(CurLine, BarEndFldName)
            tmpFltBarEnd = "20110101000000"
            If tmpFltBarEnd <> "" Then
                BarEnd = CedaFullDateToVb(tmpFltBarEnd)
                'BarEnd = DateAdd("d", 1, BarEnd)
                BarKey = DataTab.GetFieldValue(CurLine, "URNO")
            End If
            If BarKey <> "" Then
                CurGantt.AddBkBarToLine CurLine, BarKey, BarBgn, BarEnd, BarColor, BarTxt
            End If
        Next
        CurGantt.Refresh
    'End If
End Sub

Public Sub CreateChartCandyBars(Index As Integer, CurGantt As UGANTTLib.UGantt, DataTab As TABLib.Tab, TabFields As String)
    Dim BlkTabn As String
    Dim BlkType As String
    Dim BlkUrno As String
    Dim BlkBurn As String
    Dim BlkNafr As String
    Dim BlkNato As String
    Dim BlkTifr As String
    Dim BlkTito As String
    Dim BlkDays As String
    Dim BarWkdy As String
    Dim BarDate As String
    Dim BarVpfr As String
    Dim BarVpto As String
    Dim BarStnd As String
    Dim BarKey As String
    Dim BarTxt As String
    Dim BarNafr As Date
    Dim BarNato As Date
    Dim BarDay As Date
    Dim BarBgn As Date
    Dim BarEnd As Date
    Dim BarColor As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim BarLine As Long
    Dim DayCnt As Integer
    Dim strTimeFrameFrom As String
    Dim strTimeFrameTo As String
    Dim datTF As Date
    Dim datTT As Date
    Dim TFFrom As Date
    Dim TFTo As Date
    strTimeFrameFrom = DataFilterVpfr
    strTimeFrameTo = DataFilterVpto
    TFFrom = CedaFullDateToVb(strTimeFrameFrom)
    TFFrom = DateAdd("d", -1, TFFrom)
    TFTo = CedaFullDateToVb(strTimeFrameTo)
    TFTo = DateAdd("d", 1, TFTo)
    If CurrentTimeCode = "LOC" Then
        TFFrom = DateAdd("n", 0, TFFrom)
        TFTo = DateAdd("n", 0, TFTo)
    Else
        TFFrom = DateAdd("n", UtcTimeDisp, TFFrom)
        TFTo = DateAdd("n", UtcTimeDisp, TFTo)
    End If
    
    BarTxt = ""
    BarColor = vbMagenta
    If Index >= 0 Then
        MaxLine = DataTab.GetLineCount
        MaxLine = MaxLine - 1
        For CurLine = 0 To MaxLine
            BlkTabn = DataTab.GetFieldValue(CurLine, "TABN")
            If BlkTabn = "PST" Then
                DayCnt = 0
                BlkType = DataTab.GetFieldValue(CurLine, "TYPE")
                BlkUrno = DataTab.GetFieldValue(CurLine, "URNO")
                BlkBurn = DataTab.GetFieldValue(CurLine, "BURN")
                BarLine = GetBarScaleLineNo(CurGantt, TabBasPstTabTab, "URNO", BlkBurn)
                If BarLine < 0 Then BlkUrno = ""
                If BlkUrno <> "" Then
                    BlkNafr = DataTab.GetFieldValue(CurLine, "NAFR")
                    BlkNato = DataTab.GetFieldValue(CurLine, "NATO")
                    BlkDays = DataTab.GetFieldValue(CurLine, "DAYS")
                    If (BlkNafr <> "") And (BlkNato <> "") Then
                        BarKey = ""
                        BarTxt = DataTab.GetFieldValue(CurLine, "RESN")
                        BlkTifr = DataTab.GetFieldValue(CurLine, "TIFR")
                        BlkTito = DataTab.GetFieldValue(CurLine, "TITO")
                        If (BlkTifr <> "") And (BlkTito <> "") Then
                            BarNafr = CedaFullDateToVb(BlkNafr)
                            BarNato = CedaFullDateToVb(BlkNato)
                            If CurrentTimeCode = "LOC" Then
                                BarNafr = DateAdd("n", 0, BarNafr)
                                BarNato = DateAdd("n", 0, BarNato)
                            Else
                                BarNafr = DateAdd("n", UtcTimeDisp, BarNafr)
                                BarNato = DateAdd("n", UtcTimeDisp, BarNato)
                            End If
                            BarDay = BarNafr
                            While BarDay <= BarNato
                                If BarDay >= TFFrom Then
                                    BarWkdy = Trim(Str(Weekday(BarDay, vbMonday)))
                                    If InStr(BlkDays, BarWkdy) > 0 Then
                                        DayCnt = DayCnt + 1
                                        BarKey = BlkUrno & "#" & CStr(DayCnt) & "#"
                                        BarDate = Format(BarDay, "yyyymmddhhmm00")
                                        BarBgn = CedaFullDateToVb(BarDate)
                                        BarBgn = DateAdd("n", -UtcTimeDisp, BarBgn)
                                        BarDate = Format(BarBgn, "yyyymmdd")
                                        BarVpfr = BlkNafr
                                        Mid(BarVpfr, 1) = BarDate
                                        If BlkTifr <> "" Then Mid(BarVpfr, 9) = BlkTifr
                                        BarVpto = BlkNato
                                        Mid(BarVpto, 1) = BarDate
                                        If BlkTito <> "" Then Mid(BarVpto, 9) = BlkTito
                                        BarBgn = CedaFullDateToVb(BarVpfr)
                                        BarEnd = CedaFullDateToVb(BarVpto)
                                        If BarEnd < BarBgn Then
                                            BarEnd = DateAdd("d", 1, BarEnd)
                                        End If
                                        If CurrentTimeCode = "LOC" Then
                                            BarBgn = DateAdd("n", 0, BarBgn)
                                            BarEnd = DateAdd("n", 0, BarEnd)
                                        Else
                                            BarBgn = DateAdd("n", UtcTimeDisp, BarBgn)
                                            BarEnd = DateAdd("n", UtcTimeDisp, BarEnd)
                                        End If
                                        If BarBgn < TFFrom Then BarBgn = TFFrom
                                        If BarEnd > TFTo Then BarEnd = TFTo
                                        If BarKey <> "" Then
                                            CurGantt.AddBkBarToLine BarLine, BarKey, BarBgn, BarEnd, BarColor, BarTxt
                                        End If
                                    End If
                                End If
                                If BarDay > TFTo Then BarDay = BarNato
                                BarDay = DateAdd("d", 1, BarDay)
                            Wend
                        Else
                            DayCnt = DayCnt + 1
                            BarKey = BlkUrno & "#" & CStr(DayCnt) & "#"
                            BarNafr = CedaFullDateToVb(BlkNafr)
                            BarNato = CedaFullDateToVb(BlkNato)
                            If CurrentTimeCode = "LOC" Then
                                BarNafr = DateAdd("n", 0, BarNafr)
                                BarNato = DateAdd("n", 0, BarNato)
                            Else
                                BarNafr = DateAdd("n", UtcTimeDisp, BarNafr)
                                BarNato = DateAdd("n", UtcTimeDisp, BarNato)
                            End If
                            BarBgn = BarNafr
                            BarEnd = BarNato
                            If BarBgn < TFFrom Then BarBgn = TFFrom
                            If BarEnd > TFTo Then BarEnd = TFTo
                            If BarKey <> "" Then
                                CurGantt.AddBkBarToLine BarLine, BarKey, BarBgn, BarEnd, BarColor, BarTxt
                            End If
                        End If
                    End If
                End If
            End If
        Next
        CurGantt.Refresh
    End If
End Sub


Public Sub CreateChartFlightBars(Index As Integer, CurGantt As UGANTTLib.UGantt, DataTab As TABLib.Tab, ForWhat As String)
    Dim TabFields As String
    Dim TxtGroups As String
    Dim BarLTextCfg As String
    Dim BarRTextCfg As String
    Dim BarBgnText As String
    Dim BarEndText As String
    Dim BarGroups As String
    Dim BarFields As String
    Dim BarUrno As String
    Dim BarAdid As String
    Dim BarFtyp As String
    Dim BarRkey As String
    Dim BarAurn As String
    Dim BarStat As String
    Dim FldProp As String
    Dim FldName As String
    Dim FldOffs As String
    Dim tmpFltBarBgn As String
    Dim tmpFltBarEnd As String
    Dim OutFldName As String
    Dim OutFldData As String
    Dim BarRefField As String
    Dim BarRefValue As String
    Dim BarKey As String
    Dim BarTxt As String
    Dim CurLine As Long
    Dim BarLine As Long
    Dim BarColor As Long
    Dim BarStyle As Integer
    Dim MaxLine As Long
    Dim BarBgn As Date
    Dim BarEnd As Date
    Dim BgnIsValid As Boolean
    Dim EndIsValid As Boolean
    Dim TimeOff As Integer
    Dim iGrp As Integer
    Dim iFld As Integer
    Dim ShowBar As Boolean
    GanttDataPool.TabFlightBars(0).ResetContent
    TxtGroups = MainDialog.GetTabProperty(Index, "GANTT_FLBAR", 1, "")
    BarGroups = MainDialog.GetTabProperty(Index, "GANTT_TIMES", 1, "")
    BarLTextCfg = MainDialog.GetTabProperty(Index, "GANTT_LTEXT", 1, "")
    BarRTextCfg = MainDialog.GetTabProperty(Index, "GANTT_RTEXT", 1, "")
    Select Case ForWhat
        Case "ARR"
            BarRefField = "PSTA"
        Case "DEP"
            BarRefField = "PSTD"
        Case "TOW"
            BarRefField = "PSTD"
            'TxtGroups = "FLNO,REGN,ACT3,PSTD,PSTA|FLNO,REGN,ACT3,PSTD,PSTA"
        Case Else
    End Select
    MaxLine = DataTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        ShowBar = True
        BarLine = -1
        BarRefValue = ""
        BarUrno = DataTab.GetFieldValue(CurLine, "URNO")
        BarRkey = DataTab.GetFieldValue(CurLine, "RKEY")
        BarAurn = DataTab.GetFieldValue(CurLine, "AURN")
        BarFtyp = DataTab.GetFieldValue(CurLine, "FTYP")
        BarAdid = DataTab.GetFieldValue(CurLine, "ADID")
        Select Case Index
            Case TabArrFlightsIdx
                GanttDataPool.InitFlightBarList BarRkey, BarAurn, BarUrno
            Case TabArrTowingIdx
                GanttDataPool.InitFlightBarList BarRkey, BarAurn, BarUrno
            Case TabDepFlightsIdx
                GanttDataPool.InitFlightBarList BarRkey, BarAurn, BarUrno
            Case Else
        End Select
        iGrp = 0
        BarFields = "START"
        While BarFields <> ""
            BgnIsValid = False
            EndIsValid = False
            iGrp = iGrp + 1
            BarFields = GetItem(BarGroups, iGrp, "|")
            TabFields = GetItem(TxtGroups, iGrp, "|")
            If BarFields <> "" Then
                tmpFltBarBgn = ""
                tmpFltBarEnd = ""
                FldProp = GetItem(BarFields, 1, ",")
                iFld = 0
                FldName = "START"
                While (FldName <> "") And (BgnIsValid = False)
                    iFld = iFld + 1
                    FldName = GetItem(FldProp, iFld, ";")
                    TimeOff = 0
                    If InStr(FldName, "+") > 0 Then
                        FldOffs = GetItem(FldName, 2, "+")
                        FldName = GetItem(FldName, 1, "+")
                        TimeOff = Val(FldOffs)
                    End If
                    If InStr(FldName, "-") > 0 Then
                        FldOffs = GetItem(FldName, 2, "-")
                        FldName = GetItem(FldName, 1, "-")
                        TimeOff = Val(FldOffs) * -1
                    End If
                    If FldName <> "" Then
                        If tmpFltBarBgn = "" Then
                            Select Case Index
                                Case TabArrFlightsIdx
                                    BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "A", "A", tmpFltBarBgn, tmpFltBarEnd)
                                Case TabArrTowingIdx
                                    Select Case FldName
                                        Case "PDBS"
                                            BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "B", "D", tmpFltBarBgn, tmpFltBarEnd)
                                        Case "PABS"
                                            BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "B", "A", tmpFltBarBgn, tmpFltBarEnd)
                                        Case Else
                                    End Select
                                Case TabDepFlightsIdx
                                    BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "D", "D", tmpFltBarBgn, tmpFltBarEnd)
                                Case Else
                                    tmpFltBarBgn = DataTab.GetFieldValue(CurLine, FldName)
                                    tmpFltBarBgn = Trim(tmpFltBarBgn) 'igu on 18 Aug 2010
                            End Select
                        End If
                        If tmpFltBarBgn = "" Then
                            tmpFltBarBgn = DataTab.GetFieldValue(CurLine, FldName)
                            tmpFltBarBgn = Trim(tmpFltBarBgn) 'igu on 18 Aug 2010
                        End If
                        If tmpFltBarBgn <> "" Then
                            BarBgn = CedaFullDateToVb(tmpFltBarBgn)
                            BarBgn = DateAdd("n", TimeOff, BarBgn)
                            BgnIsValid = True
                        End If
                    End If
                Wend
                FldProp = GetItem(BarFields, 2, ",")
                iFld = 0
                FldName = "START"
                While (FldName <> "") And (EndIsValid = False)
                    iFld = iFld + 1
                    FldName = GetItem(FldProp, iFld, ";")
                    TimeOff = 0
                    If InStr(FldName, "+") > 0 Then
                        FldOffs = GetItem(FldName, 2, "+")
                        FldName = GetItem(FldName, 1, "+")
                        TimeOff = Val(FldOffs)
                    End If
                    If InStr(FldName, "-") > 0 Then
                        FldOffs = GetItem(FldName, 2, "-")
                        FldName = GetItem(FldName, 1, "-")
                        TimeOff = Val(FldOffs) * -1
                    End If
                    If FldName <> "" Then
                        If tmpFltBarEnd = "" Then
                            Select Case Index
                                Case TabArrFlightsIdx
                                    BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "A", "A", tmpFltBarBgn, tmpFltBarEnd)
                                Case TabArrTowingIdx
                                    Select Case FldName
                                        Case "PDES"
                                            BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "B", "D", tmpFltBarBgn, tmpFltBarEnd)
                                        Case "PAES"
                                            BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "B", "A", tmpFltBarBgn, tmpFltBarEnd)
                                        Case Else
                                    End Select
                                Case TabDepFlightsIdx
                                    BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "D", "D", tmpFltBarBgn, tmpFltBarEnd)
                                Case Else
                                tmpFltBarEnd = DataTab.GetFieldValue(CurLine, FldName)
                                tmpFltBarEnd = Trim(tmpFltBarEnd) 'igu on 18 Aug 2010
                            End Select
                        End If
                        If tmpFltBarEnd = "" Then
                            tmpFltBarEnd = DataTab.GetFieldValue(CurLine, FldName)
                            tmpFltBarEnd = Trim(tmpFltBarEnd) 'igu on 18 Aug 2010
                        End If
                        If tmpFltBarEnd <> "" Then
                            BarEnd = CedaFullDateToVb(tmpFltBarEnd)
                            BarEnd = DateAdd("n", TimeOff, BarEnd)
                            EndIsValid = True
                        End If
                    End If
                Wend
                BarTxt = DataTab.GetFieldValues(CurLine, TabFields)
                If (BgnIsValid = True) And (EndIsValid = True) Then
                    BarTxt = Replace(BarTxt, ",", " / ", 1, -1, vbBinaryCompare)
                    BarBgn = DateAdd("n", UtcTimeDisp, BarBgn)
                    BarEnd = DateAdd("n", UtcTimeDisp, BarEnd)
                    BarKey = "#" & CStr(iGrp) & "#" & BarUrno
                    Select Case BarAdid
                        Case "A"
                            BarStyle = 1
                            BarColor = vbYellow
                        Case "B"
                            If (BarFtyp = "T") Or (BarFtyp = "G") Then
                                BarStyle = 0
                                BarColor = vbCyan
                            Else
                                BarStyle = 1
                                BarColor = DarkGreen
                            End If
                        Case "D"
                            BarStyle = 1
                            BarColor = vbGreen
                        Case Else
                            BarStyle = 1
                            BarColor = vbMagenta
                    End Select
                    Select Case BarFtyp
                        Case "O"
                        Case "S"
                        Case "X"
                            BarColor = vbRed
                            ShowBar = False
                        Case "N"
                            BarColor = vbRed
                            ShowBar = False
                        Case "D"
                            BarColor = vbRed
                            ShowBar = False
                        Case Else
                    End Select
                    BarBgnText = ""
                    BarEndText = ""
                    If BarLTextCfg <> "" Then
                        OutFldName = GetItem(BarLTextCfg, 2, "]")
                        OutFldData = DataTab.GetFieldValue(CurLine, OutFldName)
                        If OutFldData <> "" Then
                            GetKeyItem BarBgnText, BarLTextCfg, "[", "]"
                            BarBgnText = BarBgnText & " "
                            If InStr(MainDialog.ListOfCedaTimeFields, FldName) > 0 Then
                                BarBgnText = BarBgnText & GetItem(AutoFormatFieldValues(OutFldName, OutFldData, "hh:mm", "hh:mm"), 1, ",")
                            Else
                                BarBgnText = BarBgnText & OutFldData
                            End If
                            BarBgnText = BarBgnText & " "
                        End If
                    End If
                    If BarRTextCfg <> "" Then
                        OutFldName = GetItem(BarRTextCfg, 2, "]")
                        OutFldData = DataTab.GetFieldValue(CurLine, OutFldName)
                        If OutFldData <> "" Then
                            GetKeyItem BarEndText, BarRTextCfg, "[", "]"
                            BarEndText = BarEndText & " "
                            If InStr(MainDialog.ListOfCedaTimeFields, FldName) > 0 Then
                                BarEndText = BarEndText & GetItem(AutoFormatFieldValues(OutFldName, OutFldData, "hh:mm", "hh:mm"), 1, ",")
                            Else
                                BarEndText = BarEndText & OutFldData
                            End If
                            BarEndText = " " & BarEndText
                        End If
                    End If
                    If BarRefValue = "" Then
                        BarLine = -1
                        BarRefValue = Trim(DataTab.GetFieldValue(CurLine, BarRefField))
                        If BarRefValue <> "" Then
                            BarLine = GetBarScaleLineNo(CurGantt, TabBasPstTabTab, "PNAM", BarRefValue)
                        End If
                    End If
                    If (ShowBar) And (BarLine >= 0) Then
                        If (BarBgn < CurGantt.TimeFrameTo) And (BarEnd > CurGantt.TimeFrameFrom) Then
                            CurGantt.AddBarToLine BarLine, BarKey, BarBgn, BarEnd, BarTxt, BarStyle, BarColor, vbBlack, 0, 0, 0, BarBgnText, BarEndText, vbBlack, vbBlack
                        End If
                    End If
                End If
                If ForWhat = "TOW" Then
                    If BarRefField = "PSTD" Then BarRefField = "PSTA" Else BarRefField = "PSTD"
                    BarRefValue = ""
                End If
            End If
        Wend
        If ForWhat = "TOW" Then
            BarRefField = "PSTD"
            BarRefValue = ""
        End If
    Next
    CurGantt.Refresh
End Sub
Private Function GetBarScaleLineNo(CurGantt As UGANTTLib.UGantt, DataTab As TABLib.Tab, RefField As String, RefValue As String) As Long
    Dim HitLines As String
    Dim HitLineNo As Long
    Dim KeyUrno As String
    Dim LineNo As Long
    Dim ColNo As Long
    LineNo = -1
    'ColNo = CLng(GetRealItemNo(DataTab.LogicalFieldList, RefField))
    'If ColNo >= 0 Then
        'If RefValue <> "ZZZ" Then
            HitLines = LineKeys(0).GetLinesByIndexValue(RefField, RefValue, 0)
            'HitLines = DataTab.GetLinesByIndexValue(RefField, RefValue, 0)
            If HitLines <> "" Then
                LineNo = Val(HitLines)
                'KeyUrno = DataTab.GetFieldValue(HitLineNo, "URNO")
            End If
        'Else
        '    KeyUrno = "0000000000"
        'End If
        'If KeyUrno <> "" Then
        '    'LineNo = CurGantt.GetLineNoByKey(KeyUrno)
        '    HitLines = LineKeys(0).GetLinesByIndexValue("PNAM", KeyUrno, 0)
        '    If HitLines <> "" Then
        '        LineNo = Val(HitLines)
        '    End If
        'End If
    'End If
    GetBarScaleLineNo = LineNo
End Function

Private Sub chkTask_Click(Index As Integer)
    If chkTask(Index).Value = 1 Then
        Select Case Index
            Case 0
                Me.Hide
                chkTask(Index).Value = 0
            Case Else
                chkTask(Index).Value = 0
        End Select
    Else
    End If
End Sub

Private Sub Form_Load()
    GanttChart(0).Top = chkTask(0).Top + chkTask(0).Height + 30
    GanttChart(0).Left = 0
    InitMyForm
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Me.Hide
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - GanttChart(0).Left
    If NewSize > 600 Then
        GanttChart(0).Width = NewSize
    End If
    NewSize = Me.ScaleHeight - StatusBar1.Height - GanttChart(0).Top - 30
    If NewSize > 600 Then
        GanttChart(0).Height = NewSize
    End If
End Sub

