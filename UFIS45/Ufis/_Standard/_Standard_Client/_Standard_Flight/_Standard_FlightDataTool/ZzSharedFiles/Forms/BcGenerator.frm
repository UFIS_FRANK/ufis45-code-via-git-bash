VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form BcGenerator 
   Caption         =   "UFIS IMBIS Event Preview Generator"
   ClientHeight    =   7455
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8970
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "BcGenerator.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7455
   ScaleWidth      =   8970
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkWork 
      Caption         =   "Stop"
      Height          =   315
      Index           =   3
      Left            =   3150
      Style           =   1  'Graphical
      TabIndex        =   36
      Top             =   0
      Width           =   1035
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Pause"
      Height          =   315
      Index           =   2
      Left            =   2100
      Style           =   1  'Graphical
      TabIndex        =   35
      Top             =   0
      Width           =   1035
   End
   Begin VB.Frame fraPanel 
      Caption         =   "Event History"
      Height          =   2745
      Index           =   1
      Left            =   30
      TabIndex        =   5
      Top             =   4170
      Width           =   2895
      Begin TABLib.TAB BcPreviewTab 
         Height          =   2295
         Index           =   1
         Left            =   60
         TabIndex        =   7
         Top             =   270
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   4048
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraPanel 
      Caption         =   "Event Preview"
      Height          =   3735
      Index           =   0
      Left            =   30
      TabIndex        =   4
      Top             =   360
      Width           =   2895
      Begin TABLib.TAB BcPreviewTab 
         Height          =   2895
         Index           =   0
         Left            =   60
         TabIndex        =   6
         Top             =   270
         Width           =   1395
         _Version        =   65536
         _ExtentX        =   2461
         _ExtentY        =   5106
         _StockProps     =   64
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Start"
      Height          =   315
      Index           =   1
      Left            =   1050
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   0
      Width           =   1035
   End
   Begin VB.Frame fraOpt 
      Caption         =   "Options"
      Height          =   6555
      Left            =   3600
      TabIndex        =   2
      Top             =   360
      Width           =   3795
      Begin VB.Frame fraDetails 
         Caption         =   "Future Use"
         Enabled         =   0   'False
         Height          =   1065
         Index           =   3
         Left            =   1920
         TabIndex        =   29
         Top             =   1950
         Width           =   1755
         Begin VB.OptionButton optBcDetail 
            Caption         =   "BcProxy Event"
            Height          =   195
            Index           =   11
            Left            =   120
            TabIndex        =   32
            Top             =   270
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.OptionButton optBcDetail 
            Caption         =   "BC Spooler"
            Height          =   195
            Index           =   10
            Left            =   120
            TabIndex        =   31
            Top             =   510
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.OptionButton optBcDetail 
            Caption         =   "BC Processor"
            Height          =   195
            Index           =   9
            Left            =   120
            TabIndex        =   30
            Top             =   750
            Visible         =   0   'False
            Width           =   1455
         End
      End
      Begin VB.Frame fraScenario 
         Caption         =   "Future Use"
         Enabled         =   0   'False
         Height          =   1065
         Index           =   1
         Left            =   120
         TabIndex        =   25
         Top             =   1950
         Width           =   1755
         Begin VB.OptionButton optBcType 
            Caption         =   "WKS (Local)"
            Height          =   195
            Index           =   5
            Left            =   120
            TabIndex        =   28
            Top             =   270
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.OptionButton optBcType 
            Caption         =   "NET (Remote)"
            Height          =   195
            Index           =   4
            Left            =   120
            TabIndex        =   27
            Top             =   510
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.OptionButton optBcType 
            Caption         =   "SRV (Server)"
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   26
            Top             =   750
            Visible         =   0   'False
            Width           =   1455
         End
      End
      Begin VB.Frame fraDetails 
         Caption         =   "Server Updates"
         Height          =   1065
         Index           =   2
         Left            =   3030
         TabIndex        =   12
         Top             =   570
         Width           =   1755
         Begin VB.OptionButton optBcDetail 
            Caption         =   "Others"
            Enabled         =   0   'False
            Height          =   195
            Index           =   8
            Left            =   120
            TabIndex        =   23
            Top             =   750
            Width           =   1455
         End
         Begin VB.OptionButton optBcDetail 
            Caption         =   "Real Updates"
            Height          =   195
            Index           =   7
            Left            =   120
            TabIndex        =   22
            Top             =   510
            Width           =   1455
         End
         Begin VB.OptionButton optBcDetail 
            Caption         =   "BroadCasts"
            Height          =   195
            Index           =   6
            Left            =   120
            TabIndex        =   21
            Top             =   270
            Width           =   1455
         End
      End
      Begin VB.Frame fraDetails 
         Caption         =   "Remote Updates"
         Height          =   1065
         Index           =   1
         Left            =   2460
         TabIndex        =   11
         Top             =   570
         Width           =   1755
         Begin VB.OptionButton optBcDetail 
            Caption         =   "Local Hub"
            Height          =   195
            Index           =   5
            Left            =   120
            TabIndex        =   20
            Top             =   750
            Width           =   1455
         End
         Begin VB.OptionButton optBcDetail 
            Caption         =   "Local Client"
            Height          =   195
            Index           =   4
            Left            =   120
            TabIndex        =   19
            Top             =   510
            Width           =   1455
         End
         Begin VB.OptionButton optBcDetail 
            Caption         =   "Local Server"
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   18
            Top             =   270
            Width           =   1455
         End
      End
      Begin VB.Frame fraDetails 
         Caption         =   "Local Updates"
         Height          =   1065
         Index           =   0
         Left            =   1920
         TabIndex        =   10
         Top             =   570
         Width           =   1755
         Begin VB.OptionButton optBcDetail 
            Caption         =   "BC Processor"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   17
            Top             =   750
            Width           =   1455
         End
         Begin VB.OptionButton optBcDetail 
            Caption         =   "BC Spooler"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   16
            Top             =   510
            Width           =   1455
         End
         Begin VB.OptionButton optBcDetail 
            Caption         =   "BcProxy Event"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   15
            Top             =   270
            Width           =   1455
         End
      End
      Begin VB.Frame fraScenario 
         Caption         =   "Environment"
         Height          =   1065
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   570
         Width           =   1755
         Begin VB.OptionButton optBcType 
            Caption         =   "SRV (Server)"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   14
            Top             =   750
            Width           =   1455
         End
         Begin VB.OptionButton optBcType 
            Caption         =   "NET (Remote)"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   13
            Top             =   510
            Width           =   1455
         End
         Begin VB.OptionButton optBcType 
            Caption         =   "WKS (Local)"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   9
            Top             =   270
            Width           =   1455
         End
      End
      Begin VB.Label lblScenario 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   2
         Left            =   -30
         TabIndex        =   34
         Top             =   3030
         Width           =   3885
      End
      Begin VB.Label lblScenario 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   1
         Left            =   -30
         TabIndex        =   33
         Top             =   1650
         Width           =   3885
      End
      Begin VB.Label lblScenario 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   0
         Left            =   -60
         TabIndex        =   24
         Top             =   270
         Width           =   3915
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Create"
      Height          =   315
      Index           =   0
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   0
      Width           =   1035
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   7140
      Width           =   8970
      _ExtentX        =   15822
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12726
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "BcGenerator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub CreateBcPreview()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim EvtRec As String
    Dim TrgRec As String
    BcPreviewTab(0).ResetContent
    BcPreviewTab(0).Refresh
    If TabArrFlightsIdx >= 0 Then
        MaxLine = TabArrFlightsTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            EvtRec = GetNextFlightEvent(TabArrFlightsTab, CurLine, TrgRec)
            If EvtRec <> "" Then BcPreviewTab(0).InsertTextLine EvtRec, False
            If TrgRec <> "" Then BcPreviewTab(1).InsertTextLine TrgRec, False
        Next
    End If
    If TabDepFlightsIdx >= 0 Then
        MaxLine = TabDepFlightsTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            EvtRec = GetNextFlightEvent(TabDepFlightsTab, CurLine, TrgRec)
            If EvtRec <> "" Then BcPreviewTab(0).InsertTextLine EvtRec, False
            If TrgRec <> "" Then BcPreviewTab(1).InsertTextLine TrgRec, False
        Next
    End If
    BcPreviewTab(0).Sort "8", True, True
    BcPreviewTab(0).AutoSizeColumns
    BcPreviewTab(0).Refresh
    BcPreviewTab(1).Sort "8", True, True
    BcPreviewTab(1).AutoSizeColumns
    BcPreviewTab(1).Refresh
End Sub
Public Function GetNextFlightEvent(AftTab As TABLib.Tab, LineNo As Long, TrgEvent As String) As String
    Dim NewEvent As String
    Dim OldEvent As String
    Dim tmpFtyp As String
    Dim tmpAdid As String
    Dim tmpFlno As String
    Dim tmpData As String
    Dim EvtField As String
    Dim EvtValue As String
    Dim EvtTCode As String
    Dim tmpField As String
    Dim tmpValue As String
    Dim ArrCheck As String
    Dim ArrTCode As String
    Dim DepCheck As String
    Dim DepTCode As String
    Dim FltCheck As String
    Dim FltTCode As String
    Dim CurCheck As String
    Dim CurTCode As String
    Dim CurValue As String
    Dim StopLoop As Boolean
    Dim FltTime As Date
    Dim EvtTime As Date
    Dim TrgTime As Date
    Dim Itm As Integer
    ArrCheck = "ONBL,LAND,TMOA,ETAI,STOA"
    ArrTCode = "ONB,LND,TMO,ETA,ATD"
    DepCheck = "AIRB,OFBL,ETDI,STOD"
    DepTCode = "AIR,OFB,ETD,ATA"
    NewEvent = ""
    OldEvent = ""
    EvtField = ""
    EvtValue = ""
    FltCheck = ""
    tmpFtyp = AftTab.GetFieldValue(LineNo, "FTYP")
    tmpAdid = AftTab.GetFieldValue(LineNo, "ADID")
    tmpFlno = AftTab.GetFieldValue(LineNo, "FLNO")
    If tmpFlno <> "" Then
        Select Case tmpFtyp
            Case "O", "S"
                Select Case tmpAdid
                    Case "A"
                        FltCheck = ArrCheck
                        FltTCode = ArrTCode
                    Case "D"
                        FltCheck = DepCheck
                        FltTCode = DepTCode
                    Case "B"
                    Case Else
                End Select
            Case Else
        End Select
    End If
    If FltCheck <> "" Then
        Itm = 0
        CurCheck = "START"
        StopLoop = False
        While (Not StopLoop)
            EvtField = CurCheck
            EvtTCode = CurTCode
            Itm = Itm + 1
            CurCheck = GetItem(FltCheck, Itm, ",")
            CurTCode = GetItem(FltTCode, Itm, ",")
            StopLoop = True
            If CurCheck <> "" Then
                StopLoop = False
                CurValue = AftTab.GetFieldValue(LineNo, CurCheck)
                If CurValue <> "" Then
                    'Found the first filled field
                    'Get the value and stop loop
                    EvtValue = CurValue
                    StopLoop = True
                End If
            End If
        Wend
        'FldLst = "ADID,FLNO,SKED,TYPE,TIME,WHAT,REMA,TRIG,FINA,VALU,URNO"
        Select Case tmpFtyp
            Case "O", "S"
                Select Case tmpAdid
                    Case "A"
                        If EvtField <> "START" Then
                            NewEvent = NewEvent & "ARR" & ","
                            NewEvent = NewEvent & tmpFlno & ","
                            NewEvent = NewEvent & AftTab.GetFieldValue(LineNo, "STOA") & ","
                            NewEvent = NewEvent & "" & ","
                            NewEvent = NewEvent & EvtTCode & ","
                            NewEvent = NewEvent & EvtValue & ","
                            NewEvent = NewEvent & "" & ","
                            NewEvent = NewEvent & "" & ","
                            NewEvent = NewEvent & EvtValue & ","
                            NewEvent = NewEvent & "" & ","
                            NewEvent = NewEvent & "" & ","
                            NewEvent = NewEvent & EvtField & ","
                            NewEvent = NewEvent & EvtValue & ","
                            NewEvent = NewEvent & AftTab.GetFieldValue(LineNo, "URNO")
                        End If
                        OldEvent = OldEvent & "ARR" & ","
                        OldEvent = OldEvent & tmpFlno & ","
                        OldEvent = OldEvent & AftTab.GetFieldValue(LineNo, "STOA") & ","
                        OldEvent = OldEvent & "" & ","
                        OldEvent = OldEvent & CurTCode & ","
                        OldEvent = OldEvent & CurValue & ","
                        OldEvent = OldEvent & "" & ","
                        OldEvent = OldEvent & "" & ","
                        OldEvent = OldEvent & CurValue & ","
                        OldEvent = OldEvent & "" & ","
                        OldEvent = OldEvent & "" & ","
                        OldEvent = OldEvent & CurCheck & ","
                        OldEvent = OldEvent & CurValue & ","
                        OldEvent = OldEvent & AftTab.GetFieldValue(LineNo, "URNO")
                    Case "D"
                        If EvtField <> "START" Then
                            NewEvent = NewEvent & "DEP" & ","
                            NewEvent = NewEvent & tmpFlno & ","
                            NewEvent = NewEvent & AftTab.GetFieldValue(LineNo, "STOD") & ","
                            NewEvent = NewEvent & "" & ","
                            NewEvent = NewEvent & EvtTCode & ","
                            NewEvent = NewEvent & EvtValue & ","
                            NewEvent = NewEvent & "" & ","
                            NewEvent = NewEvent & "" & ","
                            NewEvent = NewEvent & EvtValue & ","
                            NewEvent = NewEvent & "" & ","
                            NewEvent = NewEvent & "" & ","
                            NewEvent = NewEvent & EvtField & ","
                            NewEvent = NewEvent & EvtValue & ","
                            NewEvent = NewEvent & AftTab.GetFieldValue(LineNo, "URNO")
                        End If
                        OldEvent = OldEvent & "DEP" & ","
                        OldEvent = OldEvent & tmpFlno & ","
                        OldEvent = OldEvent & AftTab.GetFieldValue(LineNo, "STOD") & ","
                        OldEvent = OldEvent & "" & ","
                        OldEvent = OldEvent & CurTCode & ","
                        OldEvent = OldEvent & CurValue & ","
                        OldEvent = OldEvent & "" & ","
                        OldEvent = OldEvent & "" & ","
                        OldEvent = OldEvent & CurValue & ","
                        OldEvent = OldEvent & "" & ","
                        OldEvent = OldEvent & "" & ","
                        OldEvent = OldEvent & CurCheck & ","
                        OldEvent = OldEvent & CurValue & ","
                        OldEvent = OldEvent & AftTab.GetFieldValue(LineNo, "URNO")
                    Case "B"
                    Case Else
                End Select
            Case Else
        End Select
    End If
    TrgEvent = OldEvent
    GetNextFlightEvent = NewEvent
End Function

Private Sub BcPreviewTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If Index = 0 Then
        If LineNo >= 0 Then
            CreateInternalUpdate LineNo, 1
        End If
    Else
    End If
End Sub

Private Sub CreateInternalUpdate(LineNo As Long, ForWhat As Integer)
    Dim ReqId As String
    Dim DestName As String
    Dim RecvName As String
    Dim CedaCmd As String
    Dim ObjName As String
    Dim Seq As String
    Dim tws As String
    Dim twe As String
    Dim CedaSqlKey As String
    Dim Fields As String
    Dim Data As String
    Dim BcNum As String
    Dim tmpUrno As String
    Dim tmpFidx As String
    Dim tmpFina As String
    Dim tmpValu As String
    tmpUrno = BcPreviewTab(0).GetFieldValue(LineNo, "URNO")
    tmpFina = BcPreviewTab(0).GetFieldValue(LineNo, "FINA")
    tmpValu = BcPreviewTab(0).GetFieldValue(LineNo, "VALU")
    tmpFidx = BcPreviewTab(0).GetFieldValue(LineNo, "FIDX")
    
End Sub
Private Sub chkWork_Click(Index As Integer)
    If chkWork(Index).Value = 1 Then
        Select Case Index
            Case 0
                CreateBcPreview
                chkWork(Index).Value = 0
            Case Else
        End Select
    Else
    End If
End Sub

Private Sub Form_Load()
    Dim FldLst As String
    Dim LenLst As String
    Dim HdrLst As String
    Dim ColAli As String
    Dim HdrMai As String
    Dim i As Integer
    FldLst = "ADID,FLNO,SKED,STAT,TYPE,TIME,WHAT,REMA,TRIG,SPC1,SPC2,FINA,VALU,URNO,FIDX"
    LenLst = "10,10,10,10,10,10,10,10,10,10,10,10,10,10,10"
    ColAli = "C,L,C,C,C,C,L,L,C,L,L,L,L,R,L"
    HdrMai = "Flight,Next Expected Event,Trigger Setting,System"
    HdrLst = "A/D ,Flight,Schedule,STAT,TYPE,TIME,Context,Remark,Trigger,-----,                                         ,FINA,VALU,URNO,FIDX"
    For i = 0 To 1
        BcPreviewTab(i).ResetContent
        BcPreviewTab(i).LogicalFieldList = FldLst
        BcPreviewTab(i).HeaderString = HdrLst
        BcPreviewTab(i).HeaderLengthString = LenLst
        BcPreviewTab(i).ColumnAlignmentString = ColAli
        BcPreviewTab(i).HeaderAlignmentString = ColAli
        BcPreviewTab(i).LineHeight = 18
        BcPreviewTab(i).FontName = "Arial"
        BcPreviewTab(i).FontSize = 16
        BcPreviewTab(i).HeaderFontSize = 16
        BcPreviewTab(i).SetTabFontBold True
        BcPreviewTab(i).MainHeader = True
        BcPreviewTab(i).SetMainHeaderValues "3,5,3,4", HdrMai, ""
        BcPreviewTab(i).SetMainHeaderFont 16, False, False, True, 0, "Arial"
        BcPreviewTab(i).DateTimeSetColumn 2
        BcPreviewTab(i).DateTimeSetInputFormatString 2, "YYYYMMDDhhmm"
        BcPreviewTab(i).DateTimeSetOutputFormatString 2, "hh':'mm'/'DD"
        BcPreviewTab(i).DateTimeSetColumn 5
        BcPreviewTab(i).DateTimeSetInputFormatString 5, "YYYYMMDDhhmm"
        BcPreviewTab(i).DateTimeSetOutputFormatString 5, "hh':'mm'/'DD"
        BcPreviewTab(i).DateTimeSetColumn 8
        BcPreviewTab(i).DateTimeSetInputFormatString 8, "YYYYMMDDhhmm"
        BcPreviewTab(i).DateTimeSetOutputFormatString 8, "hh':'mm'/'DD"
        BcPreviewTab(i).LifeStyle = True
        BcPreviewTab(i).CursorLifeStyle = True
        BcPreviewTab(i).AutoSizeByHeader = True
        BcPreviewTab(i).AutoSizeColumns
        HdrLst = "A/D ,Flight,Schedule,STAT,TYPE,TIME,Context,Remark,Trigger,-----,                                         ,FINA,VALU,URNO,FIDX"
        HdrMai = "Flight,Last Recent Event,Trigger Setting,System"
    Next
    For i = 1 To 2
        fraDetails(i).Top = fraDetails(0).Top
        fraDetails(i).Left = fraDetails(0).Left
    Next
    optBcType(0).Value = True
    optBcDetail(0).Value = True
    optBcDetail(3).Value = True
    optBcDetail(6).Value = True
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    Dim i As Integer
    NewSize = Me.ScaleWidth - fraOpt.Width - fraPanel(0).Left
    If NewSize > 600 Then
        fraOpt.Left = Me.ScaleWidth - fraOpt.Width - 30
        NewSize = fraOpt.Left - fraPanel(0).Left - 30
        fraPanel(0).Width = NewSize
        fraPanel(1).Width = NewSize
    End If
    NewSize = Me.ScaleHeight - StatusBar1.Height - fraOpt.Top
    If NewSize > 600 Then fraOpt.Height = NewSize
    NewSize = Me.ScaleHeight - StatusBar1.Height - fraPanel(1).Height - fraPanel(0).Top - 60
    If NewSize > 600 Then
        fraPanel(0).Height = NewSize
        fraPanel(1).Top = fraPanel(0).Top + NewSize + 60
    End If
    For i = 0 To 1
        NewSize = fraPanel(i).Width - (BcPreviewTab(i).Left * 2) - 15
        If NewSize > 600 Then BcPreviewTab(i).Width = NewSize
        NewSize = fraPanel(i).Height - BcPreviewTab(i).Top - 75
        If NewSize > 600 Then BcPreviewTab(i).Height = NewSize
    Next
    
End Sub

Private Sub optBcType_Click(Index As Integer)
    Dim i As Integer
    If optBcType(Index).Value = True Then
        'For i = 0 To fraDetails.UBound
        '    fraDetails(Index).Visible = False
        'Next
        fraDetails(Index).Visible = True
        fraDetails(Index).ZOrder
    End If
End Sub
