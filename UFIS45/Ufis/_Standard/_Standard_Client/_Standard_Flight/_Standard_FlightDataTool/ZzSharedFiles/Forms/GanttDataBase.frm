VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form GanttDataBase 
   AutoRedraw      =   -1  'True
   Caption         =   "HiddenDataTab"
   ClientHeight    =   7320
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10620
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   ScaleHeight     =   7320
   ScaleWidth      =   10620
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB TabAFT 
      Height          =   2175
      Left            =   120
      TabIndex        =   2
      Top             =   4920
      Width           =   10335
      _Version        =   65536
      _ExtentX        =   18230
      _ExtentY        =   3836
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDSR 
      Height          =   2175
      Left            =   120
      TabIndex        =   1
      Top             =   2520
      Width           =   10335
      _Version        =   65536
      _ExtentX        =   18230
      _ExtentY        =   3836
      _StockProps     =   64
   End
   Begin TABLib.TAB TabGHD 
      Height          =   2175
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   10335
      _Version        =   65536
      _ExtentX        =   18230
      _ExtentY        =   3836
      _StockProps     =   64
   End
End
Attribute VB_Name = "GanttDataBase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub LoadGHD()

    Dim rec As New Recordset
    Dim conn As New Connection
    Dim strSQL, strTable, strFieldlist, strWhere, strHeaderLenStr, strInsert As String
    Dim strArr() As String
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim i As Integer

    strTable = "GHDTAB"
    strFieldlist = "URNO,FKEY,EURN,DUBE,DUEN,DRTI"
    strWhere = "DUBE < '19990309060000' AND DUEN > '19990308000000' AND FKEY <> '0' AND EURN <> '0'"

    conn.Open "DSN=gantt;DBQ=" + App.Path + "\gantt.mdb;DefaultDir=" + App.Path + ";DriverId=25;FIL=MS Access;MaxBufferSize=2048;PageTimeout=5;UID=admin;"
    strSQL = "SELECT " + strFieldlist + " FROM " + strTable + " WHERE " + strWhere
    rec.Open strSQL, conn

'--- init ghd
    TabGHD.ResetContent
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabGHD.HeaderLengthString = strHeaderLenStr
    TabGHD.HeaderString = strFieldlist

'--- fill ghd
    While Not rec.EOF
        strInsert = ""
        strInsert = rec!urno & "," & rec!fkey & "," & rec!eurn & "," & _
            rec!dube & "," & rec!duen & "," & rec!drti
        TabGHD.InsertTextLine strInsert, False
        rec.MoveNext
    Wend

    rec.Close
    conn.Close
    Set rec = Nothing
    Set conn = Nothing

End Sub

Public Sub LoadDSR()

    Dim rec As New Recordset
    Dim conn As New Connection
    Dim strSQL, strTable, strFieldlist, strWhere, strHeaderLenStr, strInsert As String
    Dim strArr() As String
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim i As Integer

    strTable = "DSRTAB"
    strFieldlist = "URNO,ENAM,ENM2,AVFA,AVTA"
    strWhere = "AVFA < '19990309060000' AND AVTA > '19990308000000' ORDER BY AVFA"
    
    conn.Open "DSN=gantt;DBQ=" + App.Path + "\gantt.mdb;DefaultDir=" + App.Path + ";DriverId=25;FIL=MS Access;MaxBufferSize=2048;PageTimeout=5;UID=admin;"
    strSQL = "SELECT " + strFieldlist + " FROM " + strTable + " WHERE " + strWhere
    rec.Open strSQL, conn

'--- init dsr
    TabDSR.ResetContent
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabDSR.HeaderLengthString = strHeaderLenStr
    TabDSR.HeaderString = strFieldlist

'--- fill dsr
    While Not rec.EOF
        strInsert = ""
        strInsert = rec!urno & "," & rec!enam & "," & rec!enm2 & "," & _
            rec!avfa & "," & rec!avta
        TabDSR.InsertTextLine strInsert, False
        rec.MoveNext
    Wend

    rec.Close
    conn.Close
    Set rec = Nothing
    Set conn = Nothing
End Sub

Public Sub LoadAFT()
    Dim rec As New Recordset
    Dim conn As New Connection
    Dim strSQL, strTable, strFieldlist, strWhere, strHeaderLenStr, strInsert As String
    Dim strArr() As String
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim i As Integer

    strTable = "AFTTAB"
    strFieldlist = "URNO,FLNO,TIFA,TIFD"
    strWhere = "TIFA < '19990309060000' AND TIFD > '19990308000000'"
    
    conn.Open "DSN=gantt;DBQ=" + App.Path + "\gantt.mdb;DefaultDir=" + App.Path + ";DriverId=25;FIL=MS Access;MaxBufferSize=2048;PageTimeout=5;UID=admin;"
    strSQL = "SELECT " + strFieldlist + " FROM " + strTable + " WHERE " + strWhere
    rec.Open strSQL, conn

'--- init aft
    TabAFT.ResetContent
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabAFT.HeaderLengthString = strHeaderLenStr
    TabAFT.HeaderString = strFieldlist

'--- fill dsr
    While Not rec.EOF
        strInsert = ""
        strInsert = rec!urno & "," & rec!flno & "," & _
            rec!tifa & "," & rec!tifd
        TabAFT.InsertTextLine strInsert, False
        rec.MoveNext
    Wend

    rec.Close
    conn.Close
    Set rec = Nothing
    Set conn = Nothing

End Sub

