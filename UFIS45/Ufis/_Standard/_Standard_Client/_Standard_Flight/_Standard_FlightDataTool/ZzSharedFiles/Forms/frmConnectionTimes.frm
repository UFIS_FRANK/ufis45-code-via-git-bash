VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmConnectionTimes 
   Caption         =   "Connection Times ..."
   ClientHeight    =   7530
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11235
   Icon            =   "frmConnectionTimes.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7530
   ScaleWidth      =   11235
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picFrame 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H8000000A&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7755
      Left            =   0
      ScaleHeight     =   7755
      ScaleWidth      =   12855
      TabIndex        =   0
      Top             =   0
      Width           =   12855
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Delete"
         Height          =   375
         Left            =   8880
         TabIndex        =   6
         Top             =   6840
         Width           =   1095
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "&Add"
         Height          =   375
         Left            =   7560
         TabIndex        =   5
         Top             =   6840
         Width           =   1095
      End
      Begin VB.CommandButton btnTmpSave 
         Caption         =   "&Tmp Save"
         Height          =   375
         Left            =   6300
         TabIndex        =   4
         Top             =   6840
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "&Close"
         Height          =   375
         Left            =   4350
         TabIndex        =   2
         Top             =   6840
         Width           =   1095
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Height          =   375
         Left            =   3150
         TabIndex        =   1
         Top             =   6840
         Width           =   1095
      End
      Begin VB.Timer Timer1 
         Interval        =   10
         Left            =   5580
         Top             =   6780
      End
      Begin TABLib.TAB myTab 
         Height          =   6375
         Left            =   105
         TabIndex        =   3
         Top             =   240
         Width           =   11025
         _Version        =   65536
         _ExtentX        =   19447
         _ExtentY        =   11245
         _StockProps     =   64
      End
   End
End
Attribute VB_Name = "frmConnectionTimes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public frmConnTimesDirty As Boolean
Public lmCurrCol As Long
Dim LastFocusIndex As Integer
Dim CurLineAddNew As Long
Dim addCnt As Integer

Private Sub btnTmpSave_Click()
    Dim i As Long
    Dim cnt As Long
    Dim tCol As Long
    Dim bCol As Long
    Dim strRet As String
    Dim llLine As Long
    Dim strUrno As String
    Dim strVal As String
    Dim strFieldList As String
    Dim d As Date
    Dim strLstu As String
    Dim strUseu As String
    Dim strData As String
    
    d = Now
    strLstu = Format(d, "YYYYMMDDhhmmss")
    strUseu = strCurrentUser
    cnt = myTab.GetLineCount - 1
    strFieldList = "URNO,ALC2,ALC3,PCON,MCON,CCON,PSAM,MSAM,CSAM,CDAT,LSTU,USEC,USEU"
    For i = 0 To cnt
        myTab.GetLineColor i, tCol, bCol
        strUrno = myTab.GetFieldValue(i, "URNO")
        strRet = frmData.tabData(5).GetLinesByIndexValue("URNO", strUrno, 0)
        llLine = frmData.tabData(5).GetNextResultLine
        Select Case (bCol)
            Case vbGreen
                myTab.OnVScrollTo i
                myTab.SetFieldValues i, "LSTU,USEU", strLstu + "," + strUseu
                strData = strVal
                strVal = myTab.GetFieldValues(i, "URNO,ALC2,ALC3,PCON,MCON,CCON,PSAM,MSAM,CSAM,CDAT,LSTU,USEC,USEU")
                strData = strVal
                If llLine > -1 Then
                    strVal = CleanNullValues(strVal)
                    frmData.tabData(5).SetFieldValues llLine, strFieldList, strVal
                    frmData.tabData(5).Sort "0", True, True
                    frmData.SetServerParameters
'                    If frmData.aUfis.CallServer("URT", "CONTAB", strFieldList, strVal, "WHERE URNO=" & strUrno, "230") <> 0 Then
'                        MsgBox "Update Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
'                    End If
                Else
                    myTab.SetFieldValues i, "CDAT,LSTU,USEC,USEU", strLstu + "," + strLstu + "," + strUseu + "," + strUseu
                    frmData.tabData(5).InsertTextLine strVal, False
                    frmData.tabData(5).Sort "0", True, True
                    strVal = CleanNullValues(strVal)
                    frmData.SetServerParameters
'                    If frmData.aUfis.CallServer("IRT", "CONTAB", strFieldList, strVal, "", "230") <> 0 Then
'                        MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
'                    End If
                End If
                myTab.SetLineColor i, vbBlack, vbWhite
                myTab.SetLineTag i, strData + "," + myTab.GetFieldValue(i, "ALCOMBI")
            Case vbYellow
                strUrno = frmData.GetNextUrno
                myTab.SetFieldValues i, "URNO", strUrno
                myTab.OnVScrollTo i
                strVal = myTab.GetFieldValues(i, "URNO,ALC2,ALC3,PCON,MCON,CCON,PSAM,MSAM,CSAM,CDAT,LSTU,USEC,USEU")
                strData = strVal
                frmData.tabData(5).InsertTextLine strVal, False
                frmData.tabData(5).Sort "0", True, True
                strVal = CleanNullValues(strVal)
                frmData.SetServerParameters
'                If frmData.aUfis.CallServer("IRT", "CONTAB", strFieldList, strVal, "", "230") <> 0 Then
'                        MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
'                End If
                myTab.SetLineColor i, vbBlack, vbWhite
                myTab.SetLineTag i, strData + "," + myTab.GetFieldValue(i, "ALCOMBI")
        End Select
    Next i
    frmData.tabData(5).Sort "1,2", True, True
    myTab.Refresh
End Sub
Private Sub cmdAdd_Click()
    
    Dim strAlc2 As String
    Dim strAlc3 As String
    Dim cnt, i As Integer
            
    Dim strValues As String
    Dim strUrno As String
    Dim d As Date
    Dim strCdat As String
    Dim strUsec As String
    Dim strCombi As String
         
    d = Now
    strCombi = strAlc2 & "|" & strAlc3
    strUsec = strCurrentUser
    strCdat = Format(d, "YYYYMMDDhhmmss")
    'GetNextUrno only for the case that user saves the changes
    strUrno = ""
    strValues = strUrno + ",,,,," + strPaxDefault + ","
    strValues = strValues + strMailDefault + "," + strCargoDefault + "," + strPaxDefault + "," + strMailDefault + "," + strCargoDefault + ","
    strValues = strValues + strCdat + "," + strCdat + "," + strUsec + "," + strUsec + ","
    strCombi = ""
    strValues = strValues + strCombi
    
    myTab.InsertTextLine strValues, False
    myTab.SetLineTag myTab.GetLineCount - 1, strValues
    myTab.SetLineColor myTab.GetLineCount - 1, vbBlack, vbYellow
    myTab.SetCurrentSelection myTab.GetLineCount - 1
            
    CurLineAddNew = myTab.GetCurrentSelected
    myTab.OnVScrollTo (CurLineAddNew)
    cmdAdd.Enabled = False
End Sub

Private Sub cmdClose_Click()
    'frmMain.bmConnTimesDirty = frmConnTimesDirty
    Unload Me
End Sub

Private Sub cmdDelete_Click()

    Dim strWhere As String
    Dim llLine As Long
    Dim strUrno As String
    
    llLine = myTab.GetCurrentSelected
    strUrno = myTab.GetFieldValue(llLine, "URNO")
    strWhere = "where URNO = " & strUrno
    
    If strUrno = "" Then
        myTab.Refresh
    Else
        If frmData.aUfis.CallServer("DRT", "CONTAB", "", "", strWhere, "230") <> 0 Then
            MsgBox "Delete Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
        End If
    End If
    
    frmData.LoadDataCONTAB
    frmConnectionTimes.Refresh
    myTab.Refresh
    
    Form_Load
End Sub

Private Sub cmdSave_Click()
    Dim i As Long
    Dim cnt As Long
    Dim tCol As Long
    Dim bCol As Long
    Dim strRet As String
    Dim llLine As Long
    Dim strUrno As String
    Dim strVal As String
    Dim strFieldList As String
    Dim d As Date
    Dim strLstu As String
    Dim strUseu As String
    Dim strData As String
    
    checkAlcSelection CurLineAddNew
    
    d = Now
    strLstu = Format(d, "YYYYMMDDhhmmss")
    strUseu = strCurrentUser
    cnt = myTab.GetLineCount - 1
    strFieldList = "URNO,ALC2,ALC3,OAL2,OAL3,PCON,MCON,CCON,PSAM,MSAM,CSAM,CDAT,LSTU,USEC,USEU"
    For i = 0 To cnt
        myTab.GetLineColor i, tCol, bCol
        strUrno = myTab.GetFieldValue(i, "URNO")
        strRet = frmData.tabData(5).GetLinesByIndexValue("URNO", strUrno, 0)
        llLine = frmData.tabData(5).GetNextResultLine
        Select Case (bCol)
            Case vbGreen
                myTab.OnVScrollTo i
                myTab.SetFieldValues i, "LSTU,USEU", strLstu + "," + strUseu
                strData = strVal
                strVal = myTab.GetFieldValues(i, "URNO,ALC2,ALC3,OAL2,OAL3,PCON,MCON,CCON,PSAM,MSAM,CSAM,CDAT,LSTU,USEC,USEU")
                strData = strVal
                If llLine > -1 Then
                    strVal = CleanNullValues(strVal)
                    frmData.tabData(5).SetFieldValues llLine, strFieldList, strVal
                    frmData.tabData(5).Sort "0", True, True
                    frmData.SetServerParameters
                    If frmData.aUfis.CallServer("URT", "CONTAB", strFieldList, strVal, "WHERE URNO=" & strUrno, "230") <> 0 Then
                        MsgBox "Update Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
                    End If
                Else
                    myTab.SetFieldValues i, "CDAT,LSTU,USEC,USEU", strLstu + "," + strLstu + "," + strUseu + "," + strUseu
                    frmData.tabData(5).InsertTextLine strVal, False
                    frmData.tabData(5).Sort "0", True, True
                    strVal = CleanNullValues(strVal)
                    frmData.SetServerParameters
                    If frmData.aUfis.CallServer("IRT", "CONTAB", strFieldList, strVal, "", "230") <> 0 Then
                        MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
                    End If
                End If
                myTab.SetLineColor i, vbBlack, vbWhite
                myTab.SetLineTag i, strData + "," + myTab.GetFieldValue(i, "ALCOMBI")
            Case vbYellow
                strUrno = frmData.GetNextUrno
                myTab.SetFieldValues i, "URNO", strUrno
                myTab.OnVScrollTo i
                strVal = myTab.GetFieldValues(i, "URNO,ALC2,ALC3,OAL2,OAL3,PCON,MCON,CCON,PSAM,MSAM,CSAM,CDAT,LSTU,USEC,USEU")
                strData = strVal
                frmData.tabData(5).InsertTextLine strVal, False
                frmData.tabData(5).Sort "0", True, True
                strVal = CleanNullValues(strVal)
                frmData.SetServerParameters
                If frmData.aUfis.CallServer("IRT", "CONTAB", strFieldList, strVal, "", "230") <> 0 Then
                        MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
                End If
                myTab.SetLineColor i, vbBlack, vbWhite
                myTab.SetLineTag i, strData + "," + myTab.GetFieldValue(i, "ALCOMBI")
        End Select
    Next i
    frmData.tabData(5).Sort "1,2", True, True
    myTab.Refresh
    bRefresh = True
    Form_Load

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If lmCurrCol = 3 Or lmCurrCol = 4 Or lmCurrCol = 5 Or _
       lmCurrCol = 6 Or lmCurrCol = 7 Or lmCurrCol = 8 Then
        If (KeyAscii >= 48 And KeyAscii <= 57) Or KeyAscii = 8 Then
        Else
            KeyAscii = 0
        End If
    End If
End Sub
Private Sub Form_Load()
    Dim cnt As Long
    Dim i As Long
    Dim strALCOMBI As String
    Dim strAlc2 As String, strAlc3 As String
    frmConnTimesDirty = False
    myTab.ResetContent
    myTab.HeaderString = "URNO,ALC2,ALC3,OAL2,OAL3,PAX,Mail,Cargo,PAX,Mail,Cargo,CDAT,LSTU,USEC,USEU,ALCOMBI"
    myTab.LogicalFieldList = "URNO,ALC2,ALC3,OAL2,OAL3,PCON,MCON,CCON,PSAM,MSAM,CSAM,CDAT,LSTU,USEC,USEU,ALCOMBI"
    myTab.EnableInlineEdit True
    myTab.SetInternalLineBuffer True
    myTab.InplaceEditUpperCase = False
    myTab.HeaderLengthString = "0,60,60,60,60,80,80,80,80,80,80,80,120,120,80,80,80"
    myTab.InsertBuffer frmData.tabData(5).GetBufferByFieldList(0, frmData.tabData(5).GetLineCount - 1, "URNO,ALC2,ALC3,OAL2,OAL3,PCON,MCON,CCON,PSAM,MSAM,CSAM,CDAT,LSTU,USEC,USEU", vbLf), vbLf
    myTab.ColumnWidthString = "10,2,3,2,3,3,3,3,3,3,3,14,14,32,32,10"
    myTab.ShowHorzScroller True
    myTab.LifeStyle = True
    myTab.NoFocusColumns = "0,11,12,13,14,15,16"
    myTab.SetMainHeaderValues "3,2,3,3,5", "Airlines,Other Airline,General,To Same Airline,Info", ""
    myTab.SetMainHeaderFont 14, False, False, True, 0, "Arial"
    myTab.MainHeader = True
    myTab.DateTimeSetColumnFormat 11, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm"
    myTab.DateTimeSetColumnFormat 12, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm"
    myTab.ShowRowSelection = False
    cnt = myTab.GetLineCount - 1
    For i = 0 To cnt
        strAlc2 = Trim(myTab.GetFieldValue(i, "ALC2"))
        strAlc3 = Trim(myTab.GetFieldValue(i, "ALC3"))
        strALCOMBI = strAlc2 + "|" + strAlc3
        myTab.SetFieldValues i, "ALCOMBI", strALCOMBI
        myTab.SetLineTag i, myTab.GetFieldValues(i, "URNO,ALC2,ALC3,OAL2,OAL3,PCON,MCON,CCON,PSAM,MSAM,CSAM,CDAT,LSTU,USEC,USEU,ALCOMBI")
    Next i
    myTab.IndexCreate "ALC2", 1
    myTab.IndexCreate "ALC3", 2
    myTab.IndexCreate "ALCOMBI", 15
    DrawBackGround picFrame, 7, True, True
    lmCurrCol = -1
    
    Dim strComboValues As String
    Dim strComboValues2 As String
    Dim strComboValues3 As String
    Dim strComboValues4 As String
    
    Dim optionALC2 As String
    Dim optionALC3 As String
    Dim optionOAL2 As String
    Dim optionOAL3 As String
        
    addCnt = addCnt + 1
    cnt = frmData.tabData(4).GetLineCount - 1
    For i = 0 To cnt
        strAlc2 = Trim(frmData.tabData(4).GetFieldValue(i, "ALC2"))
        strAlc3 = Trim(frmData.tabData(4).GetFieldValue(i, "ALC3"))
        strComboValues = strComboValues & strAlc2 & vbLf
        strComboValues2 = strComboValues2 & strAlc3 & vbLf
    Next i
    
    If addCnt = 1 Then
        myTab.CreateComboObject "optionALC2", 1, 1, "", 130
        myTab.CreateComboObject "optionALC3", 2, 2, "", 130
        myTab.CreateComboObject "optionOAL2", 3, 3, "", 130
        myTab.CreateComboObject "optionOAL3", 4, 4, "", 130
    End If
    
    myTab.ComboSetColumnLengthString "optionALC2", "120"
    myTab.SetComboColumn "optionALC2", 1
    myTab.ComboAddTextLines "optionALC2", strComboValues, vbLf
    myTab.ComboMinWidth = 19

    myTab.ComboSetColumnLengthString "optionALC3", "120"
    myTab.SetComboColumn "optionALC3", 2
    myTab.ComboAddTextLines "optionALC3", strComboValues2, vbLf
    myTab.ComboMinWidth = 19

    myTab.ComboSetColumnLengthString "optionOAL2", "120"
    myTab.SetComboColumn "optionOAL2", 3
    myTab.ComboAddTextLines "optionOAL2", strComboValues, vbLf
    myTab.ComboMinWidth = 19

    myTab.ComboSetColumnLengthString "optionOAL3", "120"
    myTab.SetComboColumn "optionOAL3", 4
    myTab.ComboAddTextLines "optionOAL3", strComboValues2, vbLf
    myTab.ComboMinWidth = 19
    
    cmdAdd.Enabled = True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If CheckDirty = True Then
        If MsgBox("There are changes! Do you want to save ?", vbYesNo, "Note") = vbYes Then
            cmdSave_Click
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    addCnt = 0
    'frmMain.RefreshAllControls
End Sub
'-----------------------------------------------------------
' Iterates throug all airlines and checks if the ALC2/ALC3
' combination exists in the CONTAB. For the case it does not
' exist a new line will be created with the default connection
' times for pax, mail and cargo
'-----------------------------------------------------------
Public Sub CheckForNewAirlines()
    Dim cnt As Long
    Dim i As Long
    Dim strAlc2 As String
    Dim strAlc3 As String
    Dim strVal As String
    Dim strOldCaption As String
    
    strOldCaption = Me.Caption
    cnt = frmData.tabData(4).GetLineCount - 1
    For i = 0 To cnt
        If i Mod 100 = 0 Then
            Me.Caption = "Check for new AL: " & i & " of " & cnt
        End If
        strAlc2 = Trim(frmData.tabData(4).GetFieldValue(i, "ALC2"))
        strAlc3 = Trim(frmData.tabData(4).GetFieldValue(i, "ALC3"))
        strVal = myTab.GetFieldValues(i, "URNO,ALC2,ALC3,OAL2,OAL3,PCON,MCON,CCON,PSAM,MSAM,CSAM,CDAT,LSTU,USEC,USEU,ALCOMBI")
        If FindAirline(strAlc2, strAlc3) = False Then
            frmData.tabData(4).SetFieldValues i, "PRFL", "N"
        Else
            myTab.SetLineTag i, strVal
        End If
    Next i
    For i = 0 To cnt
        If frmData.tabData(4).GetFieldValue(i, "PRFL") = "N" Then
            strAlc2 = frmData.tabData(4).GetFieldValue(i, "ALC2")
            strAlc3 = frmData.tabData(4).GetFieldValue(i, "ALC3")
            CreateNewLine strAlc2, strAlc3
            frmData.tabData(4).SetFieldValues i, "PRFL", "0"
        End If
    Next i
    frmData.tabData(4).Refresh
    Me.Caption = strOldCaption
End Sub

Public Function FindAirline(pAlc2 As String, pAlc3 As String) As Boolean
    Dim blRet As Boolean
    Dim i As Long
    Dim cnt As Long
    Dim strAlc2 As String
    Dim strAlc3 As String
    Dim strRet As String
    Dim llLine As Long
    Dim strCombi As String
    
    strCombi = pAlc2 & "|" & pAlc3
    blRet = False
    strRet = myTab.GetLinesByIndexValue("ALCOMBI", strCombi, 0)
    llLine = myTab.GetNextResultLine
    If llLine > -1 Then
        blRet = True
    Else
        blRet = False
    End If
    
    FindAirline = blRet
End Function

Public Sub CreateNewLine(strAlc2 As String, strAlc3 As String)
    Dim strValues As String
    Dim strUrno As String
    Dim d As Date
    Dim strCdat As String
    Dim strUsec As String
    Dim strCombi As String
    
    d = Now
    strCombi = strAlc2 & "|" & strAlc3
    strUsec = strCurrentUser
    strCdat = Format(d, "YYYYMMDDhhmmss")
'CONTAB;URNO,ALC2,ALC3,PCON,MCON,CCON,CDAT,LSTU,USEC,USEU;ORDER BY ALC2,ALC3
    'GetNextUrno only for the case that user saves the changes
    strUrno = ""
    Dim strOal2 As String
    Dim strOal3 As String
    strValues = strUrno + "," + strAlc2 + "," + strAlc3 + "," + strOal2 + "," + strOal3 + "," + strPaxDefault + ","
    strValues = strValues + strMailDefault + "," + strCargoDefault + "," + strPaxDefault + "," + strMailDefault + "," + strCargoDefault + ","
    strValues = strValues + strCdat + "," + strCdat + "," + strUsec + "," + strUsec + ","
    strValues = strValues + strCombi
    
    myTab.InsertTextLine strValues, False
    myTab.SetLineTag myTab.GetLineCount - 1, strValues
    myTab.SetLineColor myTab.GetLineCount - 1, vbBlack, vbYellow
End Sub

Public Function CheckDirty() As Boolean
    Dim i As Long
    Dim cnt As Long
    Dim tCol As Long
    Dim bCol As Long
    
    If frmConnTimesDirty = True Then
        CheckDirty = True
        'frmMain.bmConnTimesDirty = True
    Else
        cnt = myTab.GetLineCount - 1
        For i = 0 To cnt
            myTab.GetLineColor i, tCol, bCol
            If bCol <> vbWhite Then
                CheckDirty = True
                frmConnTimesDirty = True
                i = cnt + 1
            End If
        Next i
    End If
End Function

Private Sub myTab_CloseInplaceEdit(ByVal LineNo As Long, ByVal ColNo As Long)
    CheckValuesChanged LineNo
    lmCurrCol = -1
End Sub

Private Sub myTab_EditPositionChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    CheckValuesChanged LineNo
    lmCurrCol = ColNo
End Sub

Private Sub myTab_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim i As Long
    Dim strColor As String
    
    CheckValuesChanged LineNo
    checkAlcSelection LineNo
    
    If Selected = False Then
        myTab.ResetLineDecorations LineNo
    Else
        strColor = vbBlue & "," & vbBlue
        myTab.CreateDecorationObject "CURSOR", "T,B", "2,2", strColor
        For i = 1 To 10
            myTab.SetDecorationObject LineNo, i, "CURSOR"
        Next i
    End If
    myTab.Refresh
End Sub
Public Sub checkAlcSelection(LineNo As Long)
    Dim tCol As Long
    Dim bCol As Long
    Dim strAlc2Selected As String
    Dim strAlc3Auto As String
    Dim strOal2Selected As String
    Dim strOal3Auto As String
    
    Dim strAlc3Selected As String
    Dim strAlc2Auto As String
    Dim strOal3Selected As String
    Dim strOal2Auto As String
    
    Dim strRet As Integer
    Dim llLine As Long
    Dim cnt As Integer
    Dim colour As String
    
    myTab.GetLineColor LineNo, tCol, bCol
    
    If LineNo >= CurLineAddNew And CurLineAddNew <> 0 Then
        strAlc2Selected = myTab.GetFieldValues(LineNo, "ALC2")
        
        If strAlc2Selected <> "" Then
            strRet = frmData.tabData(4).GetLinesByIndexValue("ALC2", strAlc2Selected, 0)
            llLine = frmData.tabData(4).GetNextResultLine
            strAlc3Auto = Trim(frmData.tabData(4).GetFieldValues(llLine, "ALC3"))
            myTab.SetFieldValues LineNo, "ALC3", strAlc3Auto
            
            strOal2Selected = myTab.GetFieldValues(LineNo, "OAL2")
            
            strRet = frmData.tabData(4).GetLinesByIndexValue("ALC2", strOal2Selected, 0)
            llLine = frmData.tabData(4).GetNextResultLine
            strOal3Auto = Trim(frmData.tabData(4).GetFieldValues(llLine, "ALC3"))
            myTab.SetFieldValues LineNo, "OAL3", strOal3Auto
        Else
            strAlc3Selected = myTab.GetFieldValues(LineNo, "ALC3")
            strRet = frmData.tabData(4).GetLinesByIndexValue("ALC3", strAlc3Selected, 0)
            llLine = frmData.tabData(4).GetNextResultLine
            strAlc2Auto = Trim(frmData.tabData(4).GetFieldValues(llLine, "ALC2"))
            myTab.SetFieldValues LineNo, "ALC2", strAlc2Auto
            
            strOal3Selected = myTab.GetFieldValues(LineNo, "OAL3")
            
            strRet = frmData.tabData(4).GetLinesByIndexValue("ALC3", strOal3Selected, 0)
            llLine = frmData.tabData(4).GetNextResultLine
            strOal2Auto = Trim(frmData.tabData(4).GetFieldValues(llLine, "ALC2"))
            myTab.SetFieldValues LineNo, "OAL2", strOal2Auto
        End If
        myTab.Refresh
    End If

End Sub
Public Sub CheckValuesChanged(LineNo As Long)
    Dim strColor As String
    Dim i As Long
    Dim strValues As String
    Dim strPax As String
    Dim strMail As String
    Dim strCargo As String
    Dim strOPax As String
    Dim strOMail As String
    Dim strOCargo As String
    
    Dim tCol As Long
    Dim bCol As Long
    
    myTab.GetLineColor LineNo, tCol, bCol
    If LineNo >= 0 Then
        strPax = myTab.GetFieldValues(LineNo, "PCON")
        strMail = myTab.GetFieldValues(LineNo, "MCON")
        strCargo = myTab.GetFieldValues(LineNo, "CCON")
        strOPax = myTab.GetFieldValues(LineNo, "PSAM")
        strOMail = myTab.GetFieldValues(LineNo, "MSAM")
        strOCargo = myTab.GetFieldValues(LineNo, "CSAM")
        If Len(strPax) > 3 Then
            strPax = Mid(strPax, 1, 3)
            myTab.SetFieldValues LineNo, "PCON", strPax
        End If
        If Len(strMail) > 3 Then
            strMail = Mid(strMail, 1, 3)
            myTab.SetFieldValues LineNo, "MCON", strMail
        End If
        If Len(strCargo) > 3 Then
            strCargo = Mid(strCargo, 1, 3)
            myTab.SetFieldValues LineNo, "CCON", strCargo
        End If
        If Len(strOPax) > 3 Then
            strOPax = Mid(strOPax, 1, 3)
            myTab.SetFieldValues LineNo, "PSAM", strPax
        End If
        If Len(strOMail) > 3 Then
            strOMail = Mid(strOMail, 1, 3)
            myTab.SetFieldValues LineNo, "MSAM", strMail
        End If
        If Len(strOCargo) > 3 Then
            strOCargo = Mid(strOCargo, 1, 3)
            myTab.SetFieldValues LineNo, "CSAM", strCargo
        End If
        strValues = myTab.GetFieldValues(LineNo, "URNO,ALC2,ALC3,OAL2,OAL3,PCON,MCON,CCON,PSAM,MSAM,CSAM,CDAT,LSTU,USEC,USEU,ALCOMBI")
        If strValues <> myTab.GetLineTag(LineNo) Then
            If bCol = vbWhite Then
                myTab.SetLineColor LineNo, vbBlack, vbGreen
            End If
        Else
            If bCol = vbGreen Then
                myTab.SetLineColor LineNo, vbBlack, vbWhite
            End If
        End If
        myTab.Refresh
    End If
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    
    Me.MousePointer = vbHourglass
    CheckForNewAirlines
    Me.MousePointer = 0
    myTab.Sort "1,2", True, True
    myTab.Refresh
End Sub
