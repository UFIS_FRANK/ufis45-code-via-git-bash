VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form BatchDialog 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Flight Data Batch Processing"
   ClientHeight    =   8325
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14730
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "BatchDialog.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   8325
   ScaleWidth      =   14730
   Begin VB.CheckBox ChkTurn 
      Caption         =   "Turn"
      Height          =   300
      Index           =   0
      Left            =   9390
      Style           =   1  'Graphical
      TabIndex        =   41
      Top             =   390
      Visible         =   0   'False
      Width           =   600
   End
   Begin VB.CheckBox ChkDep 
      Caption         =   "Dep"
      Height          =   300
      Index           =   0
      Left            =   9990
      Style           =   1  'Graphical
      TabIndex        =   40
      Top             =   390
      Visible         =   0   'False
      Width           =   600
   End
   Begin VB.CheckBox ChkArr 
      Caption         =   "Arr"
      Height          =   300
      Index           =   0
      Left            =   8790
      Style           =   1  'Graphical
      TabIndex        =   39
      Top             =   390
      Visible         =   0   'False
      Width           =   600
   End
   Begin VB.PictureBox DatePanel 
      BackColor       =   &H0000C000&
      BorderStyle     =   0  'None
      Height          =   510
      Index           =   0
      Left            =   6750
      ScaleHeight     =   510
      ScaleWidth      =   1800
      TabIndex        =   29
      Top             =   60
      Visible         =   0   'False
      Width           =   1800
      Begin VB.CheckBox DepData 
         Caption         =   "DEP"
         Height          =   240
         Index           =   0
         Left            =   900
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   0
         Width           =   900
      End
      Begin VB.CheckBox ArrData 
         Caption         =   "ARR"
         Height          =   240
         Index           =   0
         Left            =   300
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   0
         Width           =   900
      End
      Begin VB.Image DepBall 
         Height          =   195
         Index           =   0
         Left            =   30
         Picture         =   "BatchDialog.frx":014A
         Top             =   300
         Width           =   195
      End
      Begin VB.Image ArrBall 
         Height          =   195
         Index           =   0
         Left            =   30
         Picture         =   "BatchDialog.frx":0394
         Top             =   30
         Width           =   195
      End
      Begin VB.Label DepLabel 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Caption         =   "Dep"
         Height          =   240
         Index           =   0
         Left            =   900
         TabIndex        =   64
         Top             =   270
         Width           =   900
      End
      Begin VB.Label ArrLabel 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Caption         =   "Arr"
         Height          =   240
         Index           =   0
         Left            =   180
         TabIndex        =   63
         Top             =   270
         Width           =   900
      End
   End
   Begin VB.PictureBox MondayPanel 
      BackColor       =   &H0000C000&
      BorderStyle     =   0  'None
      Height          =   240
      Index           =   0
      Left            =   5160
      ScaleHeight     =   240
      ScaleWidth      =   1380
      TabIndex        =   26
      Top             =   90
      Visible         =   0   'False
      Width           =   1380
      Begin VB.CheckBox ChkWkNo 
         Caption         =   "47"
         Height          =   240
         Index           =   0
         Left            =   1020
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   0
         Width           =   360
      End
      Begin VB.CheckBox ChkWeek 
         Caption         =   "26.04.2010"
         Height          =   240
         Index           =   0
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   0
         Width           =   1020
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   25
      Top             =   8025
      Width           =   14730
      _ExtentX        =   25982
      _ExtentY        =   529
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   23363
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox WeekDayPanel 
      Height          =   360
      Index           =   0
      Left            =   8730
      ScaleHeight     =   300
      ScaleWidth      =   1800
      TabIndex        =   9
      Top             =   0
      Visible         =   0   'False
      Width           =   1860
      Begin VB.CheckBox ChkDay 
         Caption         =   "Monday"
         Height          =   300
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   0
         Width           =   1800
      End
   End
   Begin VB.PictureBox BatchPanel 
      Height          =   7065
      Left            =   30
      ScaleHeight     =   7005
      ScaleWidth      =   14250
      TabIndex        =   4
      Top             =   690
      Width           =   14310
      Begin VB.PictureBox CornerPanel 
         Height          =   660
         Index           =   0
         Left            =   -30
         ScaleHeight     =   600
         ScaleWidth      =   1380
         TabIndex        =   8
         Top             =   -30
         Width           =   1440
         Begin VB.CheckBox ChkCorner 
            Caption         =   "SU10"
            Height          =   300
            Index           =   4
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   65
            Top             =   0
            Width           =   1020
         End
         Begin VB.CheckBox ChkCorner 
            Caption         =   "All"
            Height          =   300
            Index           =   3
            Left            =   1020
            Style           =   1  'Graphical
            TabIndex        =   37
            Top             =   0
            Width           =   360
         End
         Begin VB.CheckBox ChkCorner 
            Caption         =   "Clr"
            Height          =   300
            Index           =   2
            Left            =   1020
            Style           =   1  'Graphical
            TabIndex        =   36
            Top             =   300
            Width           =   360
         End
         Begin VB.CheckBox ChkCorner 
            Caption         =   "Lbl"
            Height          =   300
            Index           =   1
            Left            =   525
            Style           =   1  'Graphical
            TabIndex        =   35
            Top             =   300
            Width           =   495
         End
         Begin VB.CheckBox ChkCorner 
            Caption         =   "Btn"
            Height          =   300
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   32
            Top             =   300
            Width           =   525
         End
      End
      Begin VB.PictureBox DayDatePanel 
         Height          =   6525
         Left            =   1440
         ScaleHeight     =   6465
         ScaleWidth      =   12780
         TabIndex        =   7
         Top             =   660
         Width           =   12840
      End
      Begin VB.PictureBox DayPanel 
         Height          =   660
         Index           =   1
         Left            =   1440
         ScaleHeight     =   600
         ScaleWidth      =   12780
         TabIndex        =   6
         Top             =   -30
         Width           =   12840
         Begin VB.CheckBox ChkTurn 
            Caption         =   "Turn"
            Height          =   300
            Index           =   7
            Left            =   11580
            Style           =   1  'Graphical
            TabIndex        =   62
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkDep 
            Caption         =   "Dep"
            Height          =   300
            Index           =   7
            Left            =   12180
            Style           =   1  'Graphical
            TabIndex        =   61
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkArr 
            Caption         =   "Arr"
            Height          =   300
            Index           =   7
            Left            =   10980
            Style           =   1  'Graphical
            TabIndex        =   60
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkTurn 
            Caption         =   "Turn"
            Height          =   300
            Index           =   6
            Left            =   9750
            Style           =   1  'Graphical
            TabIndex        =   59
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkDep 
            Caption         =   "Dep"
            Height          =   300
            Index           =   6
            Left            =   10350
            Style           =   1  'Graphical
            TabIndex        =   58
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkArr 
            Caption         =   "Arr"
            Height          =   300
            Index           =   6
            Left            =   9150
            Style           =   1  'Graphical
            TabIndex        =   57
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkTurn 
            Caption         =   "Turn"
            Height          =   300
            Index           =   5
            Left            =   7920
            Style           =   1  'Graphical
            TabIndex        =   56
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkDep 
            Caption         =   "Dep"
            Height          =   300
            Index           =   5
            Left            =   8520
            Style           =   1  'Graphical
            TabIndex        =   55
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkArr 
            Caption         =   "Arr"
            Height          =   300
            Index           =   5
            Left            =   7320
            Style           =   1  'Graphical
            TabIndex        =   54
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkTurn 
            Caption         =   "Turn"
            Height          =   300
            Index           =   4
            Left            =   6090
            Style           =   1  'Graphical
            TabIndex        =   53
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkDep 
            Caption         =   "Dep"
            Height          =   300
            Index           =   4
            Left            =   6690
            Style           =   1  'Graphical
            TabIndex        =   52
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkArr 
            Caption         =   "Arr"
            Height          =   300
            Index           =   4
            Left            =   5490
            Style           =   1  'Graphical
            TabIndex        =   51
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkTurn 
            Caption         =   "Turn"
            Height          =   300
            Index           =   3
            Left            =   4260
            Style           =   1  'Graphical
            TabIndex        =   50
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkDep 
            Caption         =   "Dep"
            Height          =   300
            Index           =   3
            Left            =   4860
            Style           =   1  'Graphical
            TabIndex        =   49
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkArr 
            Caption         =   "Arr"
            Height          =   300
            Index           =   3
            Left            =   3660
            Style           =   1  'Graphical
            TabIndex        =   48
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkTurn 
            Caption         =   "Turn"
            Height          =   300
            Index           =   2
            Left            =   2430
            Style           =   1  'Graphical
            TabIndex        =   47
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkDep 
            Caption         =   "Dep"
            Height          =   300
            Index           =   2
            Left            =   3030
            Style           =   1  'Graphical
            TabIndex        =   46
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkArr 
            Caption         =   "Arr"
            Height          =   300
            Index           =   2
            Left            =   1830
            Style           =   1  'Graphical
            TabIndex        =   45
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkTurn 
            Caption         =   "Turn"
            Height          =   300
            Index           =   1
            Left            =   600
            Style           =   1  'Graphical
            TabIndex        =   44
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkDep 
            Caption         =   "Dep"
            Height          =   300
            Index           =   1
            Left            =   1200
            Style           =   1  'Graphical
            TabIndex        =   43
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox ChkArr 
            Caption         =   "Arr"
            Height          =   300
            Index           =   1
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   42
            Top             =   300
            Width           =   600
         End
         Begin VB.PictureBox WeekDayPanel 
            BorderStyle     =   0  'None
            Height          =   300
            Index           =   7
            Left            =   10980
            ScaleHeight     =   300
            ScaleWidth      =   1800
            TabIndex        =   23
            Top             =   0
            Width           =   1800
            Begin VB.CheckBox ChkDay 
               Caption         =   "Sunday"
               Height          =   300
               Index           =   7
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   24
               Top             =   0
               Width           =   1800
            End
         End
         Begin VB.PictureBox WeekDayPanel 
            BorderStyle     =   0  'None
            Height          =   300
            Index           =   6
            Left            =   9150
            ScaleHeight     =   300
            ScaleWidth      =   1800
            TabIndex        =   21
            Top             =   0
            Width           =   1800
            Begin VB.CheckBox ChkDay 
               Caption         =   "Saturday"
               Height          =   300
               Index           =   6
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   22
               Top             =   0
               Width           =   1800
            End
         End
         Begin VB.PictureBox WeekDayPanel 
            BorderStyle     =   0  'None
            Height          =   300
            Index           =   5
            Left            =   7320
            ScaleHeight     =   300
            ScaleWidth      =   1800
            TabIndex        =   19
            Top             =   0
            Width           =   1800
            Begin VB.CheckBox ChkDay 
               Caption         =   "Friday"
               Height          =   300
               Index           =   5
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   20
               Top             =   0
               Width           =   1800
            End
         End
         Begin VB.PictureBox WeekDayPanel 
            BorderStyle     =   0  'None
            Height          =   300
            Index           =   4
            Left            =   5490
            ScaleHeight     =   300
            ScaleWidth      =   1800
            TabIndex        =   17
            Top             =   0
            Width           =   1800
            Begin VB.CheckBox ChkDay 
               Caption         =   "Thursday"
               Height          =   300
               Index           =   4
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   18
               Top             =   0
               Width           =   1800
            End
         End
         Begin VB.PictureBox WeekDayPanel 
            BorderStyle     =   0  'None
            Height          =   300
            Index           =   3
            Left            =   3660
            ScaleHeight     =   300
            ScaleWidth      =   1800
            TabIndex        =   15
            Top             =   0
            Width           =   1800
            Begin VB.CheckBox ChkDay 
               Caption         =   "Wednesday"
               Height          =   300
               Index           =   3
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   16
               Top             =   0
               Width           =   1800
            End
         End
         Begin VB.PictureBox WeekDayPanel 
            BorderStyle     =   0  'None
            Height          =   300
            Index           =   2
            Left            =   1830
            ScaleHeight     =   300
            ScaleWidth      =   1800
            TabIndex        =   13
            Top             =   0
            Width           =   1800
            Begin VB.CheckBox ChkDay 
               Caption         =   "Tuesday"
               Height          =   300
               Index           =   2
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   14
               Top             =   0
               Width           =   1800
            End
         End
         Begin VB.PictureBox WeekDayPanel 
            BorderStyle     =   0  'None
            Height          =   300
            Index           =   1
            Left            =   0
            ScaleHeight     =   300
            ScaleWidth      =   1800
            TabIndex        =   11
            Top             =   0
            Width           =   1800
            Begin VB.CheckBox ChkDay 
               Caption         =   "Monday"
               Height          =   300
               Index           =   1
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   12
               Top             =   0
               Width           =   1800
            End
         End
      End
      Begin VB.PictureBox WeekPanel 
         Height          =   6525
         Left            =   -30
         ScaleHeight     =   6465
         ScaleWidth      =   1380
         TabIndex        =   5
         Top             =   660
         Width           =   1440
      End
   End
   Begin VB.PictureBox TaskButtonPanel 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   30
      ScaleHeight     =   315
      ScaleWidth      =   4785
      TabIndex        =   2
      Top             =   360
      Width           =   4785
      Begin VB.CheckBox chkTask 
         Caption         =   "Task"
         Height          =   315
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   3
         Tag             =   "ABS.HRVIEW"
         ToolTipText     =   "Filter: Show Confirmed Updates"
         Top             =   0
         Width           =   1035
      End
   End
   Begin VB.PictureBox MainButtonPanel 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   30
      ScaleHeight     =   315
      ScaleWidth      =   4785
      TabIndex        =   0
      Top             =   30
      Width           =   4785
      Begin VB.CheckBox chkMain 
         Caption         =   "Close"
         Height          =   315
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   1
         Tag             =   "ABS.HRVIEW"
         ToolTipText     =   "Filter: Show Confirmed Updates"
         Top             =   0
         Width           =   1035
      End
   End
   Begin VB.Label MasterFlight 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Master Turnaround"
      Height          =   270
      Index           =   2
      Left            =   5880
      TabIndex        =   38
      Top             =   7770
      Width           =   2730
   End
   Begin VB.Label MasterFlight 
      Alignment       =   1  'Right Justify
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Master Flight Departure"
      Height          =   270
      Index           =   1
      Left            =   8640
      TabIndex        =   34
      Top             =   7770
      Width           =   5700
   End
   Begin VB.Label MasterFlight 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Master Flight Arrival"
      Height          =   270
      Index           =   0
      Left            =   30
      TabIndex        =   33
      Top             =   7770
      Width           =   5820
   End
End
Attribute VB_Name = "BatchDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim PanelBusy As Boolean
Dim LastIndex As Integer

Private Sub ArrData_Click(Index As Integer)
    If Not PanelBusy Then CreatePanels "USER", Index, True
End Sub

Private Sub ArrLabel_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Shift = 0 Then
        LastIndex = Index
        CreatePanels "USER", Index, True
    Else
        If LastIndex = 0 Then LastIndex = Index
        CreatePanels "RNGBGN", LastIndex, True
        Screen.MousePointer = 5
        If Index <> LastIndex Then
            CreatePanels "RNGEND", Index, True
            Screen.MousePointer = 0
        End If
    End If
End Sub

Private Sub ArrLabel_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Shift = 0 Then
        Screen.MousePointer = 0
        LastIndex = 0
    End If
End Sub

Private Sub ChkCorner_Click(Index As Integer)
    Dim i As Integer
    If ChkCorner(Index).Value = 1 Then
        ChkCorner(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                CreatePanels "BUTTONS", -1, False
                ChkCorner(Index).Value = 0
            Case 1
                CreatePanels "LABELS", -1, False
                ChkCorner(Index).Value = 0
            Case 2
                ChkCorner(3).Value = 0
                For i = 1 To ChkWeek.UBound
                    ChkWeek(i).Value = 0
                Next
                For i = 1 To ChkDay.UBound
                    ChkDay(i).Value = 0
                Next
                CreatePanels "CLEAR", -1, False
                ChkCorner(Index).Value = 0
            Case 3
                ChkCorner(4).Value = 1
                For i = 1 To ChkWeek.UBound
                    ChkWeek(i).Value = 1
                Next
                For i = 1 To ChkDay.UBound
                    ChkDay(i).Value = 1
                Next
            Case 4
                ChkCorner(3).Value = 1
            Case Else
                ChkCorner(Index).Value = 0
        End Select
    Else
        ChkCorner(Index).BackColor = vbButtonFace
        Select Case Index
            Case 3
                ChkCorner(4).Value = 0
                For i = 1 To ChkWeek.UBound
                    ChkWeek(i).Value = 0
                Next
                For i = 1 To ChkDay.UBound
                    ChkDay(i).Value = 0
                Next
            Case 4
                ChkCorner(3).Value = 0
            Case Else
        End Select
    End If
End Sub

Private Sub ChkDay_Click(Index As Integer)
    If ChkDay(Index).Value = 1 Then
        ChkDay(Index).BackColor = LightGreen
        CreatePanels "WKDAY", Index, True
    Else
        ChkDay(Index).BackColor = vbButtonFace
        CreatePanels "WKDAY", Index, False
    End If
End Sub

Private Sub chkMain_Click(Index As Integer)
    If chkMain(Index).Value = 1 Then
        chkMain(Index).BackColor = LightGreen
        chkMain(Index).Refresh
        Select Case Index
            Case 0  'Close
                chkMain(Index).Value = 0
                Me.Hide
            Case Else
                chkMain(Index).Value = 0
        End Select
    Else
        chkMain(Index).BackColor = vbButtonFace
        chkMain(Index).Refresh
    End If
End Sub

Private Sub chkTask_Click(Index As Integer)
    If chkTask(Index).Value = 1 Then
        chkTask(Index).BackColor = LightGreen
    Else
        chkTask(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub ChkWeek_Click(Index As Integer)
    ChkWkNo(Index).Value = ChkWeek(Index).Value
    If ChkWeek(Index).Value = 1 Then
        ChkWeek(Index).BackColor = LightGreen
        CreatePanels "WEEK", Index, True
    Else
        ChkWeek(Index).BackColor = vbButtonFace
        CreatePanels "WEEK", Index, False
    End If
End Sub

Private Sub ChkWkNo_Click(Index As Integer)
    ChkWeek(Index).Value = ChkWkNo(Index).Value
    If ChkWkNo(Index).Value = 1 Then
        ChkWkNo(Index).BackColor = LightGreen
    Else
        ChkWkNo(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub DepData_Click(Index As Integer)
    If Not PanelBusy Then CreatePanels "USER", Index, True
End Sub

Private Sub DepLabel_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Shift = 0 Then
        CreatePanels "USER", Index, True
    Else
    End If
End Sub

Private Sub Form_Activate()
    Me.Refresh
End Sub

Private Sub Form_Load()
    Dim NewHeight As Long
    Dim NewLeft As Long
    Dim i As Integer
    DayDatePanel.BackColor = vbBlack
    DatePanel(0).BackColor = vbBlack
    DayPanel(1).BackColor = vbBlack
    DatePanel(0).Height = 240
    ArrLabel(0).Top = 0
    ArrLabel(0).Left = 0
    ArrLabel(0).Width = 885
    DepLabel(0).Top = 0
    ArrData(0).Left = 0
    ArrBall(0).Top = 15
    ArrBall(0).Left = ArrLabel(0).Width - ArrBall(0).Width - 15
    DepBall(0).Top = 15
    DepBall(0).Left = DepLabel(0).Left + 15
    WeekPanel.Width = MondayPanel(0).Width + 60
    For i = 1 To WeekDayPanel.UBound
        WeekDayPanel(i).Height = 600
        Set ChkArr(i).Container = WeekDayPanel(i)
        Set ChkTurn(i).Container = WeekDayPanel(i)
        Set ChkDep(i).Container = WeekDayPanel(i)
        ChkArr(i).Top = 300
        ChkTurn(i).Top = 300
        ChkDep(i).Top = 300
        ChkArr(i).Left = 0
        ChkTurn(i).Left = 600
        ChkDep(i).Left = 1200
        ChkArr(i).Visible = True
        ChkTurn(i).Visible = True
        ChkDep(i).Visible = True
    Next
    CreatePanels "INIT", -1, False
    NewHeight = BatchPanel.Top + BatchPanel.Height + 30
    NewHeight = NewHeight + MasterFlight(0).Height
    NewHeight = NewHeight + StatusBar.Height
    NewHeight = NewHeight + (Me.Height - Me.ScaleHeight)
    Me.Height = NewHeight
    Me.Width = BatchPanel.Width + (BatchPanel.Left * 2) + (Me.Width - Me.ScaleWidth) + 30
    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = MainDialog.Left + ((MainDialog.Width - Me.Width) / 2)
End Sub
Private Sub CreatePanels(ForWhat As String, Index As Integer, SetSelected As Boolean)
    Dim tmpTag As String
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim IdxBgn As Integer
    Dim IdxEnd As Integer
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    PanelBusy = True
    Select Case ForWhat
        Case "INIT"
            k = 0
            NewTop = 0
            For i = 1 To 32
                Load MondayPanel(i)
                Set MondayPanel(i).Container = WeekPanel
                MondayPanel(i).Top = NewTop
                MondayPanel(i).Left = 0
                Load ChkWeek(i)
                Set ChkWeek(i).Container = MondayPanel(i)
                ChkWeek(i).Left = 0
                ChkWeek(i).Caption = ""
                ChkWeek(i).Visible = True
                Load ChkWkNo(i)
                Set ChkWkNo(i).Container = MondayPanel(i)
                ChkWkNo(i).Caption = CStr(i)
                ChkWkNo(i).Visible = True
                MondayPanel(i).Visible = True
                NewLeft = 0
                For j = 1 To 7
                    k = k + 1
                    Load DatePanel(k)
                    Set DatePanel(k).Container = DayDatePanel
                    Load ArrData(k)
                    Set ArrData(k).Container = DatePanel(k)
                    ArrData(k).Visible = False
                    Load DepData(k)
                    Set DepData(k).Container = DatePanel(k)
                    DepData(k).Visible = False
                    Load ArrLabel(k)
                    Set ArrLabel(k).Container = DatePanel(k)
                    ArrLabel(k).Visible = True
                    Load DepLabel(k)
                    Set DepLabel(k).Container = DatePanel(k)
                    DepLabel(k).Visible = True
                    Load ArrBall(k)
                    Set ArrBall(k).Container = DatePanel(k)
                    ArrBall(k).Visible = False
                    ArrBall(k).ZOrder
                    Load DepBall(k)
                    Set DepBall(k).Container = DatePanel(k)
                    DepBall(k).Visible = False
                    DepBall(k).ZOrder
                    DatePanel(k).Top = NewTop
                    DatePanel(k).Left = NewLeft
                    DatePanel(k).Visible = True
                    NewLeft = NewLeft + DatePanel(k).Width + 30
                Next
                NewTop = NewTop + MondayPanel(i).Height + 15
            Next
            NewTop = NewTop + 60
            DayDatePanel.Height = NewTop
            WeekPanel.Height = NewTop
            NewTop = WeekPanel.Top + WeekPanel.Height + 30
            BatchPanel.Height = NewTop
            NewTop = BatchPanel.Top + NewTop + 30
            For i = 0 To MasterFlight.UBound
                MasterFlight(i).Top = NewTop
            Next
        Case "BUTTONS"
            For i = 1 To DatePanel.UBound
                ArrData(i).Visible = True
                DepData(i).Visible = True
                ArrLabel(i).Visible = False
                DepLabel(i).Visible = False
            Next
        Case "LABELS"
            For i = 1 To DatePanel.UBound
                ArrData(i).Visible = False
                DepData(i).Visible = False
                ArrLabel(i).Visible = True
                DepLabel(i).Visible = True
            Next
        Case "WKDAY"
            For i = Index To DatePanel.UBound Step 7
                tmpTag = DatePanel(i).Tag
                If SetSelected Then
                    ArrLabel(i).BackColor = RGB(235, 235, 255)
                    ArrLabel(i).ForeColor = vbBlack
                    DepLabel(i).BackColor = RGB(235, 235, 255)
                    DepLabel(i).ForeColor = vbBlack
                    ArrBall(i).Visible = True
                    DepBall(i).Visible = True
                    If InStr(tmpTag, ForWhat) = 0 Then tmpTag = tmpTag & ForWhat
                Else
                    tmpTag = Replace(tmpTag, ForWhat, "", 1, -1, vbBinaryCompare)
                    If tmpTag = "" Then
                        ArrLabel(i).BackColor = vbWhite
                        ArrLabel(i).ForeColor = vbBlack
                        DepLabel(i).BackColor = vbWhite
                        DepLabel(i).ForeColor = vbBlack
                        ArrBall(i).Visible = False
                        DepBall(i).Visible = False
                    End If
                End If
                DatePanel(i).Tag = tmpTag
            Next
        Case "WEEK"
            IdxBgn = ((Index - 1) * 7) + 1
            IdxEnd = IdxBgn + 7 - 1
            For i = IdxBgn To IdxEnd
                tmpTag = DatePanel(i).Tag
                If SetSelected Then
                    ArrLabel(i).BackColor = RGB(235, 235, 255)
                    ArrLabel(i).ForeColor = vbBlack
                    DepLabel(i).BackColor = RGB(235, 235, 255)
                    DepLabel(i).ForeColor = vbBlack
                    ArrBall(i).Visible = True
                    DepBall(i).Visible = True
                    If InStr(tmpTag, ForWhat) = 0 Then tmpTag = tmpTag & ForWhat
                Else
                    tmpTag = Replace(tmpTag, ForWhat, "", 1, -1, vbBinaryCompare)
                    If tmpTag = "" Then
                        ArrLabel(i).BackColor = vbWhite
                        ArrLabel(i).ForeColor = vbBlack
                        DepLabel(i).BackColor = vbWhite
                        DepLabel(i).ForeColor = vbBlack
                        ArrBall(i).Visible = False
                        DepBall(i).Visible = False
                    End If
                End If
                DatePanel(i).Tag = tmpTag
            Next
        Case "USER"
            i = Index
            tmpTag = DatePanel(i).Tag
            If ArrLabel(i).BackColor = vbWhite Then
                ArrLabel(i).BackColor = RGB(235, 235, 255)
                ArrLabel(i).ForeColor = vbBlack
                DepLabel(i).BackColor = RGB(235, 235, 255)
                DepLabel(i).ForeColor = vbBlack
                ArrBall(i).Visible = True
                DepBall(i).Visible = True
                If InStr(tmpTag, ForWhat) = 0 Then tmpTag = tmpTag & ForWhat
            Else
                tmpTag = Replace(tmpTag, ForWhat, "", 1, -1, vbBinaryCompare)
                ArrLabel(i).BackColor = vbWhite
                ArrLabel(i).ForeColor = vbBlack
                DepLabel(i).BackColor = vbWhite
                DepLabel(i).ForeColor = vbBlack
                ArrBall(i).Visible = False
                DepBall(i).Visible = False
            End If
            DatePanel(i).Tag = tmpTag
        Case "CLEAR"
            For i = 1 To DatePanel.UBound
                DatePanel(i).Tag = ""
                ArrLabel(i).BackColor = vbWhite
                ArrLabel(i).ForeColor = vbBlack
                DepLabel(i).BackColor = vbWhite
                DepLabel(i).ForeColor = vbBlack
                ArrBall(i).Visible = False
                DepBall(i).Visible = False
            Next
        Case "RNGBGN"
            i = Index
            ArrLabel(i).BackColor = LightYellow
            ArrLabel(i).ForeColor = vbBlack
            DepLabel(i).BackColor = LightYellow
            DepLabel(i).ForeColor = vbBlack
        Case Else
    End Select
    PanelBusy = False
End Sub
Private Sub Form_Resize()
    Dim NewHeight As Long
    MainButtonPanel.Width = Me.ScaleWidth
    TaskButtonPanel.Width = Me.ScaleWidth
    StatusBar.Panels(1).Text = CStr(Me.Width / 15) & " / " & CStr(Me.Height / 15)
End Sub

Private Sub Image1_Click()

End Sub
