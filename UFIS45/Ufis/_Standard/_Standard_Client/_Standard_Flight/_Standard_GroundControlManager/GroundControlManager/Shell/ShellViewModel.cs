﻿using GroundControlManager.Helpers;
using GroundControlManager.MainWindow;
using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.LoginWindow;
using Ufis.MVVM.ViewModel;
using Ufis.NotificationWindow;
using System.ComponentModel;
using Ufis.Utilities;
using System.Windows.Input;

namespace GroundControlManager.Shell
{
    /// <summary>
    /// The ViewModel for the application's shell window.
    /// </summary>
    public class ShellViewModel : WorkspaceViewModel
    {
        #region Fields

        WorkspaceViewModel _workspace;

        #endregion // Fields

        #region Constructor

        public ShellViewModel()
        {
            DisplayName = HpAppInfo.Current.ProductTitle;
        }

        #endregion // Constructor

        #region Workspace

        public WorkspaceViewModel Workspace
        {
            get
            {
                return _workspace;
            }
            set
            {
                if (_workspace == value)
                    return;

                //Detach the old workspace event handler
                if (_workspace != null)
                    _workspace.RequestClose -= OnWorkspaceRequestClose;

                //Attach the new workspace event handler
                _workspace = value;
                if (_workspace != null)
                    _workspace.RequestClose += OnWorkspaceRequestClose;

                OnPropertyChanged("Workspace");
            }
        }

        void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            WorkspaceViewModel workspace = sender as WorkspaceViewModel;

            if (workspace is NotificationViewModel) //check if the workspace if notification view            
            {
                NotificationViewModel notificationViewModel = workspace as NotificationViewModel;
                if (notificationViewModel.IsQuitCommandExecuted)
                {
                    CloseCommand.Execute(null);
                    workspace.Dispose();
                }
            }
            else if (workspace is LoginViewModel) //check if the workspace if login view
            {
                LoginViewModel loginViewModel = workspace as LoginViewModel;
                if (loginViewModel.LoginSucceeded)
                {
                    loginViewModel.ShowLoadingBar = true;

                    //Set the current user
                    HpUser.ActiveUser = loginViewModel.User;
                    HpUser.Privileges = loginViewModel.UserPrivileges;
                    DataAccess.DlUfisData.Current.DataContext.Connection.UserName = HpUser.ActiveUser.UserId;

                    workspace.Dispose();

                    MainWindowViewModel mainWindowViewModel = new MainWindowViewModel();
                    //load the default flight data
                    using (BackgroundWorker worker = new BackgroundWorker())
                    {
                        worker.DoWork += (o, d) =>
                        {
                            string strErrorMsg = mainWindowViewModel.LoadFlightData();
                            if (strErrorMsg != null)
                                throw new Exception(strErrorMsg);
                        };

                        worker.RunWorkerCompleted += (o, c) =>
                        {
                            if (c.Error == null)
                            {
                                mainWindowViewModel.LoadFlightData();
                                Workspace = mainWindowViewModel;
                            }
                            else
                            {
                                mainWindowViewModel.Dispose();

                                WorkspaceViewModel errorWorkspace = new NotificationViewModel()
                                {
                                    NotificationInfo = new MessageInfo()
                                    {
                                        InfoText = c.Error.Message,
                                        Severity = MessageInfo.MessageInfoSeverity.Error
                                    },
                                    CanContinue = false
                                };
                                Workspace = errorWorkspace;
                            }
                        };

                        Mouse.OverrideCursor = Cursors.Wait;                            
                        worker.RunWorkerAsync();
                    }
                }
                else
                {
                    CloseCommand.Execute(null);
                    workspace.Dispose();
                }
            }
            else
            {
                workspace.Dispose();
            }
        }

        #endregion // Workspace
    }
}