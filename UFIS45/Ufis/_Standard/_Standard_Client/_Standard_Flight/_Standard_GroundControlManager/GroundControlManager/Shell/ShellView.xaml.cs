﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows;
using DevExpress.Xpf.Ribbon;
using Ufis.MVVM.ViewModel;
using Ufis.LoginWindow;
using DevExpress.Xpf.Core;
using Ufis.NotificationWindow;

namespace GroundControlManager.Shell
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : DXRibbonWindow
    {
        public ShellView()
        {
            InitializeComponent();
            Loaded += OnLoaded;
            Closing += OnClosing;
        }

        public event EventHandler BeforeClosed;
        void OnClosing(object sender, CancelEventArgs e)
        {
            ShellViewModel viewModel = (ShellViewModel)DataContext;
            WorkspaceViewModel workspace = viewModel.Workspace;
            if (workspace != null && !(workspace is LoginViewModel || workspace is NotificationViewModel))
            {
                if (DXMessageBox.Show("Do you really want to exit the application?", "Exit",
                    MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
                {
                    workspace.Dispose();

                    if (BeforeClosed != null)
                        BeforeClosed(this, EventArgs.Empty);
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        void OnLoaded(object sender, RoutedEventArgs e)
        {
            Activate();
        }
    }
}
