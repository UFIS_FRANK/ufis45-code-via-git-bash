﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Ufis.Entities;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using GroundControlManager.Entities;

namespace GroundControlManager.MainWindow
{
    /// <summary>
    /// Interaction logic for MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : UserControl
    {
        public MainWindowView()
        {
            InitializeComponent();
        }

        private void UserControl_LayoutUpdated(object sender, EventArgs e)
        {
            Mouse.OverrideCursor = null;
        }

        private void TableView_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            TableView view = sender as TableView;
            if (view != null)
            {
                view.PostEditor();

                MainWindowViewModel viewModel = DataContext as MainWindowViewModel;
                if (viewModel != null)
                {
                    viewModel.UpdateDb(e.Row as EntFlight);
                }
            }
        }
    }
}
