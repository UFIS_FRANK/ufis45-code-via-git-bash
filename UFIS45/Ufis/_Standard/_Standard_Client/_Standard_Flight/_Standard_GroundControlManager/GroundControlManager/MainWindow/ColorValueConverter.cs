﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Media;

namespace GroundControlManager.MainWindow
{
    public class ColorValueConverter : IValueConverter 
    {
        private readonly Brush backgroundColor = 
            new LinearGradientBrush(
                Colors.LimeGreen, Colors.PaleGreen, 90
            );

        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) 
        {
            if (value == null)
                return value;

            bool isReady = (bool)value;

            if (isReady)
                return backgroundColor;
            else
                return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) 
        {
            Brush background = (Brush)value;
            return (background == backgroundColor);
        }
        #endregion

    }
}
