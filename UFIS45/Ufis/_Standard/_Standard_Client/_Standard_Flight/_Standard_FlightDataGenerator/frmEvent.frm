VERSION 5.00
Begin VB.Form frmEvent 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Flight Data Event [UTC]"
   ClientHeight    =   7140
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8310
   Icon            =   "frmEvent.frx":0000
   LinkTopic       =   "frmMain"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7140
   ScaleWidth      =   8310
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame4 
      Caption         =   "Resource Times"
      Height          =   1095
      Left            =   120
      TabIndex        =   48
      Top             =   5280
      Width           =   8055
      Begin VB.TextBox txtRes2To 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   6480
         TabIndex        =   24
         Tag             =   "FLNO"
         Text            =   "20051231120000"
         Top             =   600
         Width           =   1455
      End
      Begin VB.TextBox txtRes2From 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   4920
         TabIndex        =   23
         Tag             =   "FLNO"
         Text            =   "20051231120000"
         Top             =   600
         Width           =   1455
      End
      Begin VB.TextBox txtRes1To 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1680
         TabIndex        =   22
         Tag             =   "FLNO"
         Text            =   "20051231120000"
         Top             =   600
         Width           =   1455
      End
      Begin VB.TextBox txtRes1From 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   120
         TabIndex        =   21
         Tag             =   "FLNO"
         Text            =   "20051231120000"
         Top             =   600
         Width           =   1455
      End
      Begin VB.Label Label21 
         Caption         =   "Resource 2 From - To"
         Height          =   255
         Left            =   4920
         TabIndex        =   50
         Top             =   360
         Width           =   2295
      End
      Begin VB.Label Label20 
         Caption         =   "Resource 1 From - To"
         Height          =   255
         Left            =   120
         TabIndex        =   49
         Top             =   360
         Width           =   2295
      End
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      Height          =   375
      Left            =   2828
      TabIndex        =   25
      Top             =   6570
      Width           =   1215
   End
   Begin VB.CommandButton CancelButton 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4268
      TabIndex        =   26
      Top             =   6570
      Width           =   1215
   End
   Begin VB.Frame Frame3 
      Caption         =   "Ground Resources"
      Height          =   1095
      Left            =   120
      TabIndex        =   28
      Top             =   4080
      Width           =   8055
      Begin VB.TextBox txtGate2 
         Height          =   285
         Left            =   5895
         TabIndex        =   20
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox txtGate1 
         Height          =   285
         Left            =   4935
         TabIndex        =   19
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox txtPosition 
         Height          =   285
         Left            =   3615
         TabIndex        =   18
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox txtRES2 
         Height          =   285
         Left            =   2295
         TabIndex        =   17
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox txtRES1 
         Height          =   285
         Left            =   1335
         TabIndex        =   16
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label18 
         Caption         =   "Gate 2:"
         Height          =   255
         Left            =   5895
         TabIndex        =   46
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label17 
         Caption         =   "Gate 1:"
         Height          =   255
         Left            =   4935
         TabIndex        =   45
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label16 
         Caption         =   "Position:"
         Height          =   255
         Left            =   3615
         TabIndex        =   44
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label15 
         Caption         =   "CI from/to or BLT1/BLT2:"
         Height          =   255
         Left            =   1335
         TabIndex        =   43
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Flight Times"
      Height          =   1215
      Left            =   120
      TabIndex        =   27
      Top             =   2640
      Width           =   8055
      Begin VB.TextBox txtLANDAIRB 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   5970
         TabIndex        =   15
         Tag             =   "FLNO"
         Text            =   "20051231120000"
         Top             =   720
         Width           =   1455
      End
      Begin VB.TextBox txtOXBL 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   4170
         TabIndex        =   14
         Tag             =   "FLNO"
         Text            =   "20051231120000"
         Top             =   720
         Width           =   1455
      End
      Begin VB.TextBox txtETOX 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2370
         TabIndex        =   13
         Tag             =   "FLNO"
         Text            =   "20051231120000"
         Top             =   720
         Width           =   1455
      End
      Begin VB.TextBox txtSTOX 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   570
         TabIndex        =   12
         Tag             =   "FLNO"
         Text            =   "20051231120000"
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Label12 
         Caption         =   "LAND/AIRB:"
         Height          =   255
         Left            =   5970
         TabIndex        =   40
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label11 
         Caption         =   "ONBL/OFBL:"
         Height          =   255
         Left            =   4170
         TabIndex        =   39
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label10 
         Caption         =   "ETA/ETD:"
         Height          =   255
         Left            =   2370
         TabIndex        =   38
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label9 
         Caption         =   "STA/STD:"
         Height          =   255
         Left            =   570
         TabIndex        =   37
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Flight Event"
      Height          =   2295
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8055
      Begin VB.ComboBox cmbTTYP 
         Height          =   315
         Left            =   6300
         TabIndex        =   11
         Top             =   1800
         Width           =   1095
      End
      Begin VB.ComboBox cmbREGN 
         Height          =   315
         ItemData        =   "frmEvent.frx":030A
         Left            =   3435
         List            =   "frmEvent.frx":0320
         TabIndex        =   10
         Tag             =   "ADID"
         Top             =   1800
         Width           =   2655
      End
      Begin VB.TextBox txtDIVR 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2595
         TabIndex        =   9
         Tag             =   "DES3"
         Text            =   "MUC"
         Top             =   1800
         Width           =   495
      End
      Begin VB.TextBox txtTimestampMs 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3930
         TabIndex        =   3
         Tag             =   "DES3"
         Text            =   "0"
         Top             =   720
         Width           =   495
      End
      Begin VB.TextBox txtTimestamp 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2370
         TabIndex        =   2
         Tag             =   "FLNO"
         Text            =   "20051231120000"
         Top             =   720
         Width           =   1455
      End
      Begin VB.ComboBox cmbEventType 
         Height          =   315
         Left            =   90
         TabIndex        =   1
         Text            =   "STOA event"
         Top             =   720
         Width           =   2175
      End
      Begin VB.ComboBox cmbStatus 
         Height          =   315
         ItemData        =   "frmEvent.frx":0383
         Left            =   6450
         List            =   "frmEvent.frx":0399
         TabIndex        =   6
         Tag             =   "ADID"
         Text            =   "O - Operative"
         Top             =   720
         Width           =   1455
      End
      Begin VB.ComboBox cmbADID 
         Height          =   315
         ItemData        =   "frmEvent.frx":03FC
         Left            =   5730
         List            =   "frmEvent.frx":0406
         TabIndex        =   5
         Tag             =   "ADID"
         Text            =   "D"
         Top             =   720
         Width           =   615
      End
      Begin VB.TextBox txtDES3 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1755
         TabIndex        =   8
         Tag             =   "DES3"
         Text            =   "FRA"
         Top             =   1800
         Width           =   495
      End
      Begin VB.TextBox txtORG3 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   915
         TabIndex        =   7
         Tag             =   "ORG3"
         Text            =   "BKK"
         Top             =   1800
         Width           =   495
      End
      Begin VB.TextBox txtFLNO 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   4530
         TabIndex        =   4
         Tag             =   "FLNO"
         Text            =   "TG 960"
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label19 
         Caption         =   "Nature:"
         Height          =   255
         Left            =   6300
         TabIndex        =   47
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label Label14 
         Caption         =   "Registration:"
         Height          =   255
         Left            =   3435
         TabIndex        =   42
         Top             =   1440
         Width           =   1095
      End
      Begin VB.Label Label13 
         Caption         =   "Div. (3L):"
         Height          =   255
         Left            =   2595
         TabIndex        =   41
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label8 
         Caption         =   "ms:"
         Height          =   255
         Left            =   3930
         TabIndex        =   36
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label7 
         Caption         =   "Timestamp:"
         Height          =   255
         Left            =   2370
         TabIndex        =   35
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label6 
         Caption         =   "Event type:"
         Height          =   255
         Left            =   90
         TabIndex        =   34
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label5 
         Caption         =   "Status:"
         Height          =   255
         Left            =   6450
         TabIndex        =   33
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label4 
         Caption         =   "A/D:"
         Height          =   255
         Left            =   5730
         TabIndex        =   32
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label3 
         Caption         =   "Dest. (3L):"
         Height          =   255
         Left            =   1755
         TabIndex        =   31
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Origin (3L):"
         Height          =   255
         Left            =   915
         TabIndex        =   30
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Flight number:"
         Height          =   255
         Left            =   4530
         TabIndex        =   29
         Top             =   360
         Width           =   1095
      End
   End
End
Attribute VB_Name = "frmEvent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private strMode As String
Private strKeyItems As String
Public bCancel As Boolean

Private Sub Form_Activate()
    bCancel = True
    strKeyItems = ""
End Sub

Private Sub CancelButton_Click()
    bCancel = True
    strKeyItems = ""
    Me.Hide
End Sub

Private Sub cmbEventType_Change()
    If UCase(strMode) = "INSERT" And Len(cmbEventType.Text) > 0 Then
        ResetFields
    End If
    SetMode strMode, cmbEventType.Text
End Sub

Private Sub cmbEventType_Click()
    cmbEventType_Change
End Sub


Private Sub Form_Load()
    Dim l As Long
    Dim idxREGN As Integer
    Dim idxACT5 As Integer
    Dim strTmp As String

    cmbEventType.Clear
    cmbEventType.AddItem "ETOA event"
    cmbEventType.AddItem "ETOD event"
    cmbEventType.AddItem "LAND event"
    cmbEventType.AddItem "AIRB event"
    cmbEventType.AddItem "ONBL event"
    cmbEventType.AddItem "OFBL event"
    cmbEventType.AddItem "CANCEL"
    cmbEventType.AddItem "RETURN TAXI"
    cmbEventType.AddItem "RETURN FLIGHT"
    cmbEventType.AddItem "DIVERTED FLIGHT"
    cmbEventType.AddItem "AC CHANGE"
    cmbEventType.AddItem "TOUCH AND GO"
    cmbEventType.AddItem "GATE BLOCKED"
    cmbEventType.AddItem "CIC BLOCKED"
    cmbEventType.AddItem "POSITION BLOCKED"
    cmbEventType.AddItem "BELT BLOCKED"
    cmbEventType.AddItem "Insert Flight"
    cmbEventType.AddItem "Delete flight"
    cmbEventType.AddItem "CIC TIME"
    cmbEventType.AddItem "GAT TIME"

    cmbStatus.Clear
    cmbStatus.AddItem "O - Operative"
    cmbStatus.AddItem "X - Cancelled"
    cmbStatus.AddItem "D - Diverted"
    cmbStatus.AddItem "B - Return taxi"
    cmbStatus.AddItem "N - Delete flight"
    cmbStatus.AddItem "S - Scheduled"
    cmbStatus.AddItem "Z - Return flight"

    cmbADID.Clear
    cmbADID.AddItem "A"
    cmbADID.AddItem "D"
    
    cmbTTYP.Clear
    cmbTTYP.AddItem "01"
    cmbTTYP.AddItem "02"
    cmbTTYP.AddItem "03"
    cmbTTYP.AddItem "04"
    cmbTTYP.AddItem "05"
    cmbTTYP.AddItem "06"
    cmbTTYP.AddItem "07"
    cmbTTYP.AddItem "08"
    cmbTTYP.AddItem "91"
    cmbTTYP.AddItem "99"

'01 PAX/CGO SCHED FLT-INTL
'02 BUSINESS/PRIVATE FLIGHT INTL
'03 PAX/CGO NON SCHED T/L INTL
'04 PAX/CGO NON SCHED CHRT INTL
'05 PAX/CGO SCHED FLT-DOMESTIC
'06 NON-SCHED FLT-DOMESTIC
'07 DOMESTIC COMMUTER SCHED FLT
'08 CARGO SCHED FLT-INTL
'91 STATE AIRCRAFT,DOA,ETC.-INTL
'99 STATE AIRCRAFT,DOA,ETC.-DOM

    cmbREGN.Clear
    idxREGN = GetRealItemNo(frmData.TabACR.LogicalFieldList, "REGN")
    idxACT5 = GetRealItemNo(frmData.TabACR.LogicalFieldList, "ACT5")
    For l = 0 To frmData.TabACR.GetLineCount - 1
        strTmp = frmData.TabACR.GetColumnValue(l, idxACT5)
        If Len(strTmp) > 0 Then
            cmbREGN.AddItem frmData.TabACR.GetColumnValue(l, idxREGN) & " - " & strTmp
        Else
            cmbREGN.AddItem frmData.TabACR.GetColumnValue(l, idxREGN)
        End If
    Next l

    Dim olTimestamp As Date
    olTimestamp = DateAdd("h", sglUTCOffsetHours * -1, Now)
    txtTimestamp.Text = Format(olTimestamp, "YYYYMMDDhhmmss")
    bCancel = True
End Sub

Public Sub SetMode(sMode As String, sEventType As String)
    strMode = sMode
    cmbEventType.Text = sEventType

    cmbEventType.Enabled = False
    cmbEventType.BackColor = vbWhite
    txtTimestamp.Enabled = False
    txtTimestamp.BackColor = vbWhite
    txtTimestampMs.Enabled = False
    txtTimestampMs.BackColor = vbWhite
    txtFLNO.Enabled = False
    txtFLNO.BackColor = vbWhite
    cmbADID.Enabled = False
    cmbADID.BackColor = vbWhite
    txtORG3.Enabled = False
    txtORG3.BackColor = vbWhite
    txtDES3.Enabled = False
    txtDES3.BackColor = vbWhite
    cmbStatus.Enabled = False
    cmbStatus.BackColor = vbWhite
    txtSTOX.Enabled = False
    txtSTOX.BackColor = vbWhite
    txtETOX.Enabled = False
    txtETOX.BackColor = vbWhite
    txtOXBL.Enabled = False
    txtOXBL.BackColor = vbWhite
    txtLANDAIRB.Enabled = False
    txtLANDAIRB.BackColor = vbWhite
    txtDIVR.Enabled = False
    txtDIVR.BackColor = vbWhite
    cmbREGN.Enabled = False
    cmbREGN.BackColor = vbWhite
    txtRES1.Enabled = False
    txtRES1.BackColor = vbWhite
    txtRES2.Enabled = False
    txtRES2.BackColor = vbWhite
    txtGate1.Enabled = False
    txtGate1.BackColor = vbWhite
    txtGate2.Enabled = False
    txtGate2.BackColor = vbWhite
    txtPosition.Enabled = False
    txtPosition.BackColor = vbWhite
    cmbTTYP.Enabled = False
    cmbTTYP.BackColor = vbWhite
    txtRes1From.Enabled = False
    txtRes1To.Enabled = False
    txtRes1From.BackColor = vbWhite
    txtRes1To.BackColor = vbWhite
    txtRes2From.Enabled = False
    txtRes2To.Enabled = False
    txtRes2From.BackColor = vbWhite
    txtRes2To.BackColor = vbWhite

    If UCase(sMode) = "EDIT" Or UCase(sMode) = "INSERT" Then
        'in the insert mode you can choose the event type
        If UCase(sMode) = "INSERT" Or UCase(sEventType) = "INSERT FLIGHT" Then
            cmbEventType.Enabled = True
            cmbEventType.BackColor = vbYellow
            cmbStatus.Text = "O - Operative"
            txtFLNO.Enabled = True
            txtFLNO.BackColor = vbYellow
            cmbADID.Enabled = True
            cmbADID.BackColor = vbYellow
            txtSTOX.Enabled = True
            txtSTOX.BackColor = vbYellow
        End If

        ' enable the timestamp fields
        txtTimestamp.Enabled = True
        txtTimestamp.BackColor = vbYellow
        txtTimestampMs.Enabled = True
        txtTimestampMs.BackColor = vbYellow

        ' enable controls in dependancy of the event type
        Select Case UCase(sEventType)
            Case "ETOA EVENT"
                txtETOX.Enabled = True
                txtETOX.BackColor = vbYellow
            Case "ETOD EVENT"
                txtETOX.Enabled = True
                txtETOX.BackColor = vbYellow
            Case "LAND EVENT"
                txtLANDAIRB.Enabled = True
                txtLANDAIRB.BackColor = vbYellow
            Case "AIRB EVENT"
                txtLANDAIRB.Enabled = True
                txtLANDAIRB.BackColor = vbYellow
            Case "ONBL EVENT"
                txtOXBL.Enabled = True
                txtOXBL.BackColor = vbYellow
            Case "OFBL EVENT"
                txtOXBL.Enabled = True
                txtOXBL.BackColor = vbYellow
            Case "CANCEL"
                cmbStatus.Text = "X - Cancelled"
            Case "DELETE FLIGHT"
                cmbStatus.Text = "N - Delete Flight"
            Case "RETURN TAXI"
                cmbStatus.Text = "B - Return taxi"
            Case "RETURN FLIGHT"
                cmbStatus.Text = "Z - Return flight"
            Case "DIVERTED FLIGHT"
                cmbStatus.Text = "D - Diverted"
                txtDIVR.Enabled = True
                txtDIVR.BackColor = vbYellow
            Case "AC CHANGE"
                cmbREGN.Enabled = True
                cmbREGN.BackColor = vbYellow
            Case "TOUCH AND GO"
                txtLANDAIRB.Enabled = True
                txtLANDAIRB.BackColor = vbYellow
                txtOXBL.Enabled = True
                txtOXBL.BackColor = vbYellow
            Case "GATE BLOCKED"
                txtGate1.Enabled = True
                txtGate1.BackColor = vbYellow
                txtGate2.Enabled = True
                txtGate2.BackColor = vbGreen
            Case "CIC BLOCKED"
                txtRES1.Enabled = True
                txtRES1.BackColor = vbYellow
                txtRES2.Enabled = True
                txtRES2.BackColor = vbYellow
            Case "POSITION BLOCKED"
                txtPosition.Enabled = True
                txtPosition.BackColor = vbYellow
            Case "BELT BLOCKED"
                txtRES1.Enabled = True
                txtRES1.BackColor = vbYellow
                txtRES2.Enabled = True
                txtRES2.BackColor = vbGreen
            Case "INSERT FLIGHT"
                'FLNO,ADID,STOA,ORG3,DES3,REGN,BLT1,BLT2,PSTA,GTA1,GTA2
                txtORG3.Enabled = True
                txtORG3.BackColor = vbYellow
                txtDES3.Enabled = True
                txtDES3.BackColor = vbYellow
                cmbREGN.Enabled = True
                cmbREGN.BackColor = vbYellow
                txtRES1.Enabled = True
                txtRES1.BackColor = vbGreen
                txtRES2.Enabled = True
                txtRES2.BackColor = vbGreen
                txtPosition.Enabled = True
                txtPosition.BackColor = vbGreen
                txtGate1.Enabled = True
                txtGate1.BackColor = vbGreen
                txtGate2.Enabled = True
                txtGate2.BackColor = vbGreen
                cmbTTYP.Enabled = True
                cmbTTYP.BackColor = vbYellow
            Case "CIC TIME"
                txtRes1From.Enabled = True
                txtRes1To.Enabled = True
                txtRes1From.BackColor = vbYellow
                txtRes1To.BackColor = vbYellow
                txtRES1.Enabled = True
                txtRES1.BackColor = vbYellow
                txtRES2.Enabled = True
                txtRES2.BackColor = vbYellow
                If UCase(sMode) = "INSERT" Then SetADID "D"
            Case "GAT TIME"
                txtRes1From.Enabled = True
                txtRes1To.Enabled = True
                txtRes1From.BackColor = vbYellow
                txtRes1To.BackColor = vbYellow
                txtRes2From.Enabled = True
                txtRes2To.Enabled = True
                txtRes2From.BackColor = vbGreen
                txtRes2To.BackColor = vbGreen
                txtGate1.Enabled = True
                txtGate1.BackColor = vbYellow
                txtGate2.Enabled = True
                txtGate2.BackColor = vbGreen
            Case Else
                If Len(cmbEventType.Text) > 0 Then
                    MsgBox "Please choose a valid event of the list. The event type is not valid.", vbCritical, "Invalid Event"
                End If
        End Select
    End If
End Sub

Public Sub SetFlightTimes(sSTOX As String, sETOX As String, sOXBL As String, sLANDAIRB As String)
    txtSTOX.Text = sSTOX
    txtETOX.Text = sETOX
    txtOXBL.Text = sOXBL
    txtLANDAIRB.Text = sLANDAIRB
End Sub

Public Sub SetADID(sADID As String)
    cmbADID.Text = sADID
End Sub

Public Sub SetStatus(sTYPE As String)
    Dim i As Integer
    cmbStatus.Text = "O - Operative"
    For i = 0 To cmbStatus.ListCount - 1
        If Left(cmbStatus.List(i), 1) = sTYPE Then
            cmbStatus.ListIndex = i
        End If
    Next
End Sub

Public Sub SetRegistration(sREGN As String)
    Dim i As Integer
    cmbREGN.Text = ""
    For i = 0 To cmbREGN.ListCount - 1
        If (Split(cmbREGN.List(i), " - ", -1, vbTextCompare)(0)) = sREGN Then
            cmbREGN.ListIndex = i
        End If
    Next
End Sub

Public Sub SetResources(sRes1 As String, sRes2 As String, sPosition As String, sGate1 As String, sGate2 As String)
    txtRES1.Text = sRes1
    txtRES2.Text = sRes2
    txtPosition.Text = sPosition
    txtGate1.Text = sGate1
    txtGate2.Text = sGate2
End Sub

Public Sub SetResourcesTimes(sRes1From As String, sRes1To As String, sRes2From As String, sRes2To As String)
    txtRes1From.Text = sRes1From
    txtRes1To.Text = sRes1To
    txtRes2From.Text = sRes2From
    txtRes2To.Text = sRes2To
End Sub

Public Sub SetTimestamp(sTime As String, sMiliseconds As String)
    txtTimestamp.Text = sTime
    txtTimestampMs.Text = sMiliseconds
End Sub

Private Sub OKButton_Click()
    Dim strItems As String
    Dim strData As String
    Dim strFields As String
    Dim strTmp As String

    strKeyItems = ""

    If (UCase(strMode) = "EDIT" Or UCase(strMode) = "INSERT") And Len(cmbEventType.Text) > 0 Then
        Select Case UCase(cmbEventType.Text)
            Case "ETOA EVENT"
                strFields = "ETAA,ETOA"
                strData = txtETOX.Text & sExcelSeparator & txtETOX.Text
            Case "ETOD EVENT"
                strFields = "ETDA,ETOD"
                strData = txtETOX.Text & sExcelSeparator & txtETOX.Text
            Case "LAND EVENT"
                strFields = "LNDA"
                strData = txtLANDAIRB.Text
            Case "AIRB EVENT"
                strFields = "AIRA"
                strData = txtLANDAIRB.Text
            Case "ONBL EVENT"
                strFields = "ONBD"
                strData = txtOXBL.Text
            Case "OFBL EVENT"
                strFields = "OFBD"
                strData = txtOXBL.Text
            Case "DIVERTED FLIGHT"
                strFields = "DIVR,FTYP"
                strData = txtDIVR.Text & sExcelSeparator & Left(cmbStatus.Text, 1)
            Case "AC CHANGE"
                strFields = "REGN"
                strData = (Split(cmbREGN.Text, " - ", -1, vbTextCompare)(0))
           Case "GATE BLOCKED"
                If cmbADID.Text = "A" Then
                    strFields = "GTA1,GTA2"
                Else
                    strFields = "GTD1,GTD2"
                End If
                strData = txtGate1.Text & sExcelSeparator & txtGate2.Text
            Case "CIC BLOCKED"
                strFields = "CKIF,CKIT"
                strData = txtRES1.Text & sExcelSeparator & txtRES2.Text
            Case "POSITION BLOCKED"
                If cmbADID.Text = "A" Then
                    strFields = "PSTA"
                Else
                    strFields = "PSTD"
                End If
                strData = txtPosition.Text
            Case "BELT BLOCKED"
                strFields = "BLT1,BLT2"
                strData = txtRES1.Text & sExcelSeparator & txtRES2.Text
            Case "TOUCH AND GO"
                strFields = "LNDA,ONBD"
                strData = txtTimestamp.Text & sExcelSeparator & txtTimestamp.Text
            Case "INSERT FLIGHT"
                If cmbADID.Text = "A" Then
                    'strFields = "FLNO,ADID,STOA,ORG3,DES3,REGN,BLT1,BLT2,PSTA,GTA1,GTA2,FTYP,TTYP"
                    strFields = "ADID,ORG3,DES3,REGN,BLT1,BLT2,PSTA,GTA1,GTA2,FTYP,TTYP"
                Else
                    'strFields = "FLNO,ADID,STOD,DES3,DES3,REGN,CKIF,CKIT,PSTD,GTD1,GTD2,FTYP,TTYP"
                    strFields = "ADID,ORG3,DES3,REGN,CKIF,CKIT,PSTD,GTD1,GTD2,FTYP,TTYP"
                End If
                strData = cmbADID.Text & sExcelSeparator & _
                     txtORG3 & sExcelSeparator & txtDES3 & sExcelSeparator & (Split(cmbREGN.Text, " - ", -1, vbTextCompare)(0)) & _
                     sExcelSeparator & txtRES1.Text & sExcelSeparator & txtRES2.Text & sExcelSeparator & _
                     txtPosition.Text & sExcelSeparator & txtGate1.Text & sExcelSeparator & txtGate2.Text & _
                     sExcelSeparator & Left(cmbStatus.Text, 1) & sExcelSeparator & cmbTTYP.Text
            Case "CIC TIME"
                strFields = "CKIF,CKIT,ETAA,ETDA"
                strData = txtRES1.Text & sExcelSeparator & txtRES2.Text & sExcelSeparator & _
                    txtRes1From.Text & sExcelSeparator & txtRes1To.Text
            Case "GAT TIME"
                If cmbADID.Text = "A" Then
                    strFields = "GTA1,GTA2"
                Else
                    strFields = "GTD1,GTD2"
                End If
                strData = txtGate1.Text & sExcelSeparator & txtGate2.Text

                strFields = strFields & ",ETAA,ETDA,AIRA,LNDA"
                strData = strData & sExcelSeparator & txtRes1From.Text & sExcelSeparator & txtRes1To.Text & _
                    sExcelSeparator & txtRes2From.Text & sExcelSeparator & txtRes2To.Text
            Case Else
                strFields = "FTYP"
                strData = Left(cmbStatus.Text, 1)
                'MsgBox "Please choose an event of the list. The event type is not valid.", vbCritical, "Invalid Event"
        End Select

        ' insert the timestamp
        strFields = "Timestamp,ms," & strFields
        strData = txtTimestamp.Text & sExcelSeparator & txtTimestampMs.Text & sExcelSeparator & strData

        If Len(strFields) > 0 Then
            If cmbADID.Text = "A" Then
                strFields = strFields & ",FLNO,STOA,Remark,ADID"
            Else
                strFields = strFields & ",FLNO,STOD,Remark,ADID"
            End If
            strData = strData & sExcelSeparator & txtFLNO.Text & sExcelSeparator & txtSTOX.Text & sExcelSeparator & cmbEventType.Text & sExcelSeparator & cmbADID.Text
            strKeyItems = strKeyItems & "{=FIELDS=}" & strFields
            strKeyItems = strKeyItems & "{=DATA=}" & strData
        End If
    End If
    Me.Hide
End Sub

Public Function GetValues() As String
    GetValues = strKeyItems
End Function

Public Sub SetFlightData(sFLNO As String, sORG3 As String, sDES3 As String, sDIVR As String)
    txtFLNO.Text = sFLNO
    txtORG3.Text = sORG3
    txtDES3.Text = sDES3
    txtDIVR.Text = sDIVR
End Sub

Public Sub ResetFields()
    Dim olTimestamp As Date

    SetFlightTimes "", "", "", ""
    SetStatus ""
    SetRegistration ""
    SetResources "", "", "", "", ""
    SetResourcesTimes "", "", "", ""
    ' Timestamp in UTC
    olTimestamp = DateAdd("h", sglUTCOffsetHours * -1, Now)
    SetTimestamp Format(olTimestamp, "YYYYMMDDhhmmss"), "0"
    SetFlightData "", "", "", ""
    SetTTYP ""
End Sub
Public Sub SetTTYP(sTTYP As String)
    Dim i As Integer
    cmbTTYP.Text = ""
    For i = 0 To cmbTTYP.ListCount - 1
        If Left(cmbTTYP.List(i), 2) = sTTYP Then
            cmbTTYP.ListIndex = i
        End If
    Next
End Sub
