Attribute VB_Name = "mdlEvents"
Option Explicit

Public Sub GenerateRandomEvents()
    Dim l As Long
    Dim llLineCount As Long
    Dim strPercentValue As String
    Dim strOPERATIONS_TIMEFRAME_EVENT As String
    Dim strMINUTES_BEFORE_ETOX As String

    ' init
    frmMain.TAB_UpcomingEvents.ResetContent
    frmMain.TAB_SentEvents.ResetContent
    llLineCount = frmData.TabAFT.GetLineCount - 1
    For l = 0 To llLineCount
        frmData.TabAFT.SetLineStatusValue l, 0
    Next

    ' random time events
    If GetIniEntry(sConfigFile, gsAppName, "", "GENERATE_EVENTS_ETOA", "YES") = "YES" Then
        RandomTimes "ETOA event", "ETOA_TIMEFRAME_EVENT", "ETOA_VALUE", "A", "ETAA", 0, 0
    End If

    If GetIniEntry(sConfigFile, gsAppName, "", "GENERATE_EVENTS_ETOD", "YES") = "YES" Then
        RandomTimes "ETOD event", "ETOD_TIMEFRAME_EVENT", "ETOD_VALUE", "D", "ETDA", 0, 0
    End If

    If GetIniEntry(sConfigFile, gsAppName, "", "GENERATE_EVENTS_LAND", "YES") = "YES" Then
        FixedTimes "LAND event", "LAND_TIME", "A", "LNDA"
    End If

    If GetIniEntry(sConfigFile, gsAppName, "", "GENERATE_EVENTS_AIRB", "YES") = "YES" Then
        FixedTimes "AIRB event", "AIRB_TIME", "D", "AIRA"
    End If

    If GetIniEntry(sConfigFile, gsAppName, "", "GENERATE_EVENTS_ONBL", "YES") = "YES" Then
        FixedTimes "ONBL event", "", "A", "ONBD"
    End If

    If GetIniEntry(sConfigFile, gsAppName, "", "GENERATE_EVENTS_OFBL", "YES") = "YES" Then
        FixedTimes "OFBL event", "", "D", "OFBD"
    End If

    ' operations
    strOPERATIONS_TIMEFRAME_EVENT = GetIniEntry(sConfigFile, gsAppName, "", "OPERATIONS_TIMEFRAME_EVENT", "30")

    strPercentValue = GetIniEntry(sConfigFile, gsAppName, "", "PERCENT_CANCEL", "5")
    If CInt(strPercentValue) > 0 Then
        RandomOperations "CANCEL", strOPERATIONS_TIMEFRAME_EVENT, "X", strPercentValue / 2, "A", ""
        RandomOperations "CANCEL", strOPERATIONS_TIMEFRAME_EVENT, "X", strPercentValue / 2, "D", ""
    End If

    strPercentValue = GetIniEntry(sConfigFile, gsAppName, "", "PERCENT_RETURN_TAXI", "1")
    If CInt(strPercentValue) > 0 Then
        RandomOperations "RETURN TAXI", strOPERATIONS_TIMEFRAME_EVENT, "B", strPercentValue, "D", ""
    End If

    strPercentValue = GetIniEntry(sConfigFile, gsAppName, "", "PERCENT_RETURN_FLIGHT", "1")
    If CInt(strPercentValue) > 0 Then
        RandomOperations "RETURN FLIGHT", strOPERATIONS_TIMEFRAME_EVENT, "Z", strPercentValue, "D", ""
    End If

    strPercentValue = GetIniEntry(sConfigFile, gsAppName, "", "PERCENT_DIVERTED", "1")
    If CInt(strPercentValue) > 0 Then
        RandomOperations "DIVERTED FLIGHT", strOPERATIONS_TIMEFRAME_EVENT, "D", strPercentValue, "A", "DIVR"
    End If

    strPercentValue = GetIniEntry(sConfigFile, gsAppName, "", "PERCENT_REGN_CHANGE", "5")
    If CInt(strPercentValue) > 0 Then
        RandomOperations "AC CHANGE", strOPERATIONS_TIMEFRAME_EVENT, "", strPercentValue / 2, "A", "REGN"
        RandomOperations "AC CHANGE", strOPERATIONS_TIMEFRAME_EVENT, "", strPercentValue / 2, "D", "REGN"
    End If

    strPercentValue = GetIniEntry(sConfigFile, gsAppName, "", "PERCENT_TOUCH_GO", "1")
    If CInt(strPercentValue) > 0 Then
        RandomOperations "TOUCH AND GO", strOPERATIONS_TIMEFRAME_EVENT, "", strPercentValue, "A", "LAND,ONBL"
    End If

    ' random resource blocking
    strMINUTES_BEFORE_ETOX = GetIniEntry(sConfigFile, gsAppName, "", "MINUTES_BEFORE_ETOX", "60")
    strPercentValue = GetIniEntry(sConfigFile, gsAppName, "", "PERCENT_BLOCKING_GAT", "2")
    If CInt(strPercentValue) > 0 Then
        RandomResourceBlocking "GATE BLOCKED", strMINUTES_BEFORE_ETOX, "GAT", strPercentValue / 2, "A", frmData.TabGAT
        RandomResourceBlocking "GATE BLOCKED", strMINUTES_BEFORE_ETOX, "GAT", strPercentValue / 2, "D", frmData.TabGAT
    End If

    strPercentValue = GetIniEntry(sConfigFile, gsAppName, "", "PERCENT_BLOCKING_CIC", "2")
    If CInt(strPercentValue) > 0 Then
        RandomResourceBlocking "CIC BLOCKED", strMINUTES_BEFORE_ETOX, "CIC", strPercentValue, "D", frmData.TabCIC
    End If

    strPercentValue = GetIniEntry(sConfigFile, gsAppName, "", "PERCENT_BLOCKING_PST", "2")
    If CInt(strPercentValue) > 0 Then
        RandomResourceBlocking "POSITION BLOCKED", strMINUTES_BEFORE_ETOX, "POS", strPercentValue / 2, "A", frmData.TabPST
        RandomResourceBlocking "POSITION BLOCKED", strMINUTES_BEFORE_ETOX, "POS", strPercentValue / 2, "D", frmData.TabPST
    End If

    strPercentValue = GetIniEntry(sConfigFile, gsAppName, "", "PERCENT_BLOCKING_BLT", "2")
    If CInt(strPercentValue) > 0 Then
        RandomResourceBlocking "BELT BLOCKED", strMINUTES_BEFORE_ETOX, "BLT", strPercentValue, "A", frmData.TabBLT
    End If

    strPercentValue = GetIniEntry(sConfigFile, gsAppName, "", "PERCENT_MULTI_ETA_ETD", "5")
    If CInt(strPercentValue) > 0 Then
        RandomTimes "ETOA event", "ETOA_TIMEFRAME_EVENT", "ETOA_VALUE", "A", "ETAA", strPercentValue / 2, 2
        RandomTimes "ETOD event", "ETOD_TIMEFRAME_EVENT", "ETOD_VALUE", "D", "ETDA", strPercentValue / 2, 2
    End If

    If UCase(GetIniEntry(sConfigFile, gsAppName, "", "LOGICAL_CHECK", "NO")) = "YES" Then
        LogicalCheck_CANCEL
        LogicalCheck_DIVERTED
        LogicalCheck_ReturnTaxi
    End If
    frmMain.GenerateEventTags -1
End Sub

Private Sub RandomTimes(sRemark As String, sTimeframe As String, sTimevalue As String, sADID As String, sTargetField As String, dMultiPercent As Double, iMultiNumber As Integer)
    Dim l As Long
    Dim i As Integer
    Dim llFlights As Long
    Dim idxSTOX As Integer
    Dim idxADID As Integer
    Dim idxFLNO As Integer
    Dim strRandomValue As String
    Dim strTmp As String
    Dim strTimeStamp As String
    Dim olTimeValue As Date
    Dim olTimestamp As Date
    Dim strKeyItems As String
    Dim strFLNO As String
    Dim strFields As String
    Dim strData As String
    Dim strSTOX_Field As String
    Dim strSTOX_Value As String
    Dim ilCountMultiFlights As Long

    Dim strTIMEFRAME_EVENT As String
    Dim strVALUE_EVENT As String

    Randomize

    ' Get Config Entries
    strTIMEFRAME_EVENT = GetIniEntry(sConfigFile, gsAppName, "", sTimeframe, 30)
    strVALUE_EVENT = GetIniEntry(sConfigFile, gsAppName, "", sTimevalue, 15)

    llFlights = frmData.TabAFT.GetLineCount
    ilCountMultiFlights = 0

    With frmData.TabAFT
        If sADID = "A" Then
            strSTOX_Field = "STOA"
        Else
            strSTOX_Field = "STOD"
        End If
        idxADID = GetItemNo(.LogicalFieldList, "ADID") - 1
        idxFLNO = GetItemNo(.LogicalFieldList, "FLNO") - 1
        idxSTOX = GetItemNo(.LogicalFieldList, strSTOX_Field) - 1

        For l = 0 To llFlights
            If .GetColumnValue(l, idxADID) = sADID Then
                For i = 0 To iMultiNumber
                    strSTOX_Value = .GetColumnValue(l, idxSTOX)
                    olTimestamp = CedaFullDateToVb(strSTOX_Value)

                    strTimeStamp = GetRandomTimestamp(strTIMEFRAME_EVENT, olTimestamp)

                    ' random value of timefield
                    strTmp = (Rnd * strVALUE_EVENT - (strVALUE_EVENT * 0.5))

                    ' add the random minute and make the estimated time not later than the timestamp
                    olTimeValue = DateAdd("n", CDbl(Fix(strTmp)), olTimestamp)
'                    If olTimeValue > olTimeStamp Then
'                        olTimeValue = olTimeStamp
'                    End If

                    ' round (down) to full 5 minutes
                    While Minute(olTimeValue) Mod 5 <> 0
                        olTimeValue = DateAdd("n", -1, olTimeValue)
                    Wend
                    strRandomValue = Format(olTimeValue, "YYYYMMDDhhmm00")

                    ' update flight schedule internal
                    .SetFieldValues l, sTargetField, strRandomValue

                    ' insert event into TAB_UpcomingEvents
                    frmMain.TAB_UpcomingEvents.InsertTextLine strTimeStamp & sExcelSeparator & sRemark & _
                    sExcelSeparator & .GetLineValues(l), False
                Next i

                ilCountMultiFlights = ilCountMultiFlights + 1
                If dMultiPercent > 0 And ilCountMultiFlights > (llFlights * dMultiPercent / 100) Then
                    l = llFlights
                End If
            End If
        Next l
    End With
End Sub

Private Sub FixedTimes(sRemark As String, sTimeRelative As String, sADID As String, sTargetField As String)
    Dim l As Long
    Dim llFlights As Long
    Dim idxETOX As Integer
    Dim idxADID As Integer
    Dim idxFLNO As Integer
    Dim idxSTOX As Integer
    Dim strETOX  As String
    Dim strValue As String
    Dim strKeyItems As String
    Dim strTimeStamp As String
    Dim olDate As Date
    Dim strSTOX_Value As String
    Dim strSTOX_Field As String

    Dim strTIME_RELATIVE  As String

    ' Get Config Entries
    If sTimeRelative = "" Then
        strTIME_RELATIVE = "0"
    Else
        strTIME_RELATIVE = GetIniEntry(sConfigFile, gsAppName, "", sTimeRelative, 0)
    End If

    llFlights = frmData.TabAFT.GetLineCount

    With frmData.TabAFT
        If sADID = "A" Then
            idxETOX = GetItemNo(.LogicalFieldList, "ETAA") - 1
            strSTOX_Field = "STOA"
        Else
            idxETOX = GetItemNo(.LogicalFieldList, "ETDA") - 1
            strSTOX_Field = "STOD"
        End If
        idxADID = GetItemNo(.LogicalFieldList, "ADID") - 1
        idxFLNO = GetItemNo(.LogicalFieldList, "FLNO") - 1
        idxSTOX = GetItemNo(.LogicalFieldList, strSTOX_Field) - 1

        For l = 0 To llFlights
            If .GetColumnValue(l, idxADID) = sADID Then
                strSTOX_Value = .GetColumnValue(l, idxSTOX)
                strETOX = .GetColumnValue(l, idxETOX)
                If Len(strETOX) > 0 Then
                    olDate = CedaFullDateToVb(strETOX)
                Else
                    olDate = CedaFullDateToVb(strSTOX_Value)
                End If

                ' add minute, second and millisecond
                olDate = DateAdd("n", CDbl(strTIME_RELATIVE), olDate)
                strValue = Format(olDate, "YYYYMMDDhhmmss")
                'olDate=DateAdd(
                strTimeStamp = Format(olDate, "YYYYMMDDhhmmss") & sExcelSeparator & "0"
                
                

                ' update flight schedule internal
                .SetFieldValues l, sTargetField, strValue

                ' insert event into TAB_UpcomingEvents
                frmMain.TAB_UpcomingEvents.InsertTextLine strTimeStamp & sExcelSeparator & sRemark & _
                sExcelSeparator & .GetLineValues(l), False
            End If
        Next l
    End With
End Sub

Private Sub RandomOperations(sRemark As String, sTimeRelative As String, sFTYP As String, sPercentValue As String, sADID As String, sChange As String)
    Dim llLineCount As Long
    Dim ilFlights As Integer
    Dim llStartFlight As Long
    Dim llRandomFlight As Long
    Dim blWrap As Boolean
    Dim blFound As Boolean
    Dim strTimeStamp As String
    Dim strFields As String
    Dim strData As String
    Dim strKeyItems As String
    Dim strRandomValues As String
    Dim olETOX As Date
    Dim idxETOX As Integer
    Dim idxADID As Integer
    Dim idxFLNO As Integer
    Dim idxSTOX As Integer
    Dim strSTOX_Value As String
    Dim strSTOX_Field As String

    Randomize
    llLineCount = frmData.TabAFT.GetLineCount
    ilFlights = llLineCount * sPercentValue / 100
    If ilFlights = 0 And llLineCount > 0 Then ilFlights = 1

    With frmData.TabAFT
        If sADID = "A" Then
            idxETOX = GetItemNo(.LogicalFieldList, "ETAA") - 1
            strSTOX_Field = "STOA"
        Else
            idxETOX = GetItemNo(.LogicalFieldList, "ETDA") - 1
            strSTOX_Field = "STOD"
        End If
        idxADID = GetItemNo(.LogicalFieldList, "ADID") - 1
        idxFLNO = GetItemNo(.LogicalFieldList, "FLNO") - 1
        idxSTOX = GetItemNo(.LogicalFieldList, strSTOX_Field) - 1

        While ilFlights > 0
            blWrap = False
            blFound = False
            ilFlights = ilFlights - 1

            llStartFlight = Rnd() * llLineCount
            llRandomFlight = llStartFlight

            ' find a flight which was not used for another random operation
            While blFound = False And (blWrap = False Or llStartFlight < llRandomFlight)
                If .GetLineStatusValue(llRandomFlight) = 0 And .GetColumnValue(llRandomFlight, idxADID) = sADID Then
                    blFound = True
                Else
                    llRandomFlight = llRandomFlight + 1
                    If llRandomFlight > llLineCount Then
                        blWrap = True
                        llRandomFlight = 0
                    End If
                End If
            Wend

            ' set Field Values
            If blFound = True Then
                ' get a random timestamp
                olETOX = CedaFullDateToVb(.GetColumnValue(llRandomFlight, idxETOX))
                
                If Year(olETOX) > 2000 Then
                    strTimeStamp = GetRandomTimestamp(sTimeRelative, olETOX)

                    ' update flight schedule internal
                    If Len(sFTYP) > 0 Then
                        .SetFieldValues llRandomFlight, "FTYP", sFTYP
                    End If
                    .SetLineStatusValue llRandomFlight, 1
                    Select Case sChange
                        Case "DIVR"
                            strRandomValues = frmData.TabAPT.GetFieldValues(CLng(Rnd() * frmData.TabAPT.GetLineCount), "APC3")
                            .SetFieldValues llRandomFlight, "DIVR", strRandomValues
                        Case "REGN"
                            strRandomValues = frmData.TabACR.GetFieldValues(CLng(Rnd() * frmData.TabACR.GetLineCount), "REGN")
                            .SetFieldValues llRandomFlight, "REGN", strRandomValues
                        Case ("LAND,ONBL")
                            strRandomValues = Left(strTimeStamp, 14) & sExcelSeparator & Left(strTimeStamp, 14)
                            .SetFieldValues llRandomFlight, "LNDA,ONBD", strRandomValues
                    End Select

                    ' insert event into TAB_UpcomingEvents
                    frmMain.TAB_UpcomingEvents.InsertTextLine strTimeStamp & sExcelSeparator & sRemark & _
                    sExcelSeparator & .GetLineValues(llRandomFlight), False
                End If
            End If
        Wend
    End With
End Sub

Private Function GetRandomTimestamp(sTimeframe As String, oDate As Date) As String
    Dim strTmp As String
    Dim olDate As Date

    Randomize
    olDate = oDate

    ' random occurance/timestamp of event
    strTmp = (Rnd * sTimeframe - (sTimeframe / 2))

    ' add minute, second and millisecond
    olDate = DateAdd("n", CDbl(Fix(strTmp)), olDate)
    olDate = DateAdd("s", CDbl((strTmp - Int(strTmp)) * 60), olDate)
    GetRandomTimestamp = Format(olDate, "YYYYMMDDhhmmss") & sExcelSeparator & Mid((strTmp - Int(strTmp)), 5, 1)
End Function

Private Sub RandomResourceBlocking(sRemark As String, sTimeRelative As String, sResType As String, sPercentValue As String, sADID As String, ByRef rTab As TABLib.Tab)
    Dim llLineCount As Long
    Dim ilFlights As Integer
    Dim llStartFlight As Long
    Dim llRandomFlight As Long
    Dim blWrap As Boolean
    Dim blFound As Boolean
    Dim strTimeStamp As String
    Dim olETOX As Date
    Dim idxETOX As Integer
    Dim idxADID As Integer
    Dim idxFLNO As Integer
    Dim idxRes1 As Integer
    Dim idxRes2 As Integer
    Dim llTmp As Long
    Dim ilBlockBefore As Integer
    Dim ilBlockAfter As Integer

    idxRes1 = -1
    idxRes2 = -1

    Select Case sResType
        Case "GAT"
            If sADID = "D" Then
                idxRes1 = GetRealItemNo(frmData.TabAFT.LogicalFieldList, "GTD1")
                idxRes2 = GetRealItemNo(frmData.TabAFT.LogicalFieldList, "GTD2")
            Else
                idxRes1 = GetRealItemNo(frmData.TabAFT.LogicalFieldList, "GTA1")
                idxRes2 = GetRealItemNo(frmData.TabAFT.LogicalFieldList, "GTA2")
            End If
            ilBlockBefore = 90
            ilBlockAfter = 90
        Case "CIC"
            idxRes1 = GetRealItemNo(frmData.TabAFT.LogicalFieldList, "CKIF")
            idxRes2 = GetRealItemNo(frmData.TabAFT.LogicalFieldList, "CKIT")
            ilBlockBefore = 120
            ilBlockAfter = 30
        Case "POS"
            If sADID = "D" Then
                idxRes1 = GetRealItemNo(frmData.TabAFT.LogicalFieldList, "PSTD")
            Else
                idxRes1 = GetRealItemNo(frmData.TabAFT.LogicalFieldList, "PSTA")
            End If
            ilBlockBefore = 60
            ilBlockAfter = 60
        Case "BLT"
            idxRes1 = GetRealItemNo(frmData.TabAFT.LogicalFieldList, "BLT1")
            idxRes2 = GetRealItemNo(frmData.TabAFT.LogicalFieldList, "BLT2")
            ilBlockBefore = 30
            ilBlockAfter = 120
    End Select

    Randomize
    llLineCount = frmData.TabAFT.GetLineCount
    ilFlights = llLineCount * sPercentValue / 100
    If ilFlights = 0 And llLineCount > 0 Then ilFlights = 1

    With frmData.TabAFT
        If sADID = "A" Then
            idxETOX = GetItemNo(.LogicalFieldList, "ETAA") - 1
        Else
            idxETOX = GetItemNo(.LogicalFieldList, "ETDA") - 1
        End If
        idxADID = GetItemNo(.LogicalFieldList, "ADID") - 1
        idxFLNO = GetItemNo(.LogicalFieldList, "FLNO") - 1

        While ilFlights > 0
            blWrap = False
            blFound = False
            ilFlights = ilFlights - 1

            llStartFlight = Rnd() * llLineCount
            llRandomFlight = llStartFlight

            ' find a flight which was not used for another random operation
            While blFound = False And (blWrap = False Or llStartFlight < llRandomFlight)
                If .GetLineStatusValue(llRandomFlight) < 10 And _
                .GetColumnValue(llRandomFlight, idxADID) = sADID And _
                Trim(.GetColumnValue(llRandomFlight, idxRes1)) <> "" Then
                    blFound = True
                Else
                    llRandomFlight = llRandomFlight + 1
                    If llRandomFlight > llLineCount Then
                        blWrap = True
                        llRandomFlight = 0
                    End If
                End If
            Wend

            ' set Field Values
            If blFound = True Then

                ' update flight schedule internal
                .SetLineStatusValue llRandomFlight, .GetLineStatusValue(llRandomFlight) + 10
                
                ' get a random timestamp
                olETOX = CedaFullDateToVb(.GetColumnValue(llRandomFlight, idxETOX))
                olETOX = olETOX - (sTimeRelative / (24 * 60)) 'make the event 1 hour prior to estimated time

                If Year(olETOX) > 2000 Then
                    strTimeStamp = Format(olETOX, "YYYYMMDDhhmmss") & sExcelSeparator & "0"

                    ' update flight schedule internal
                    .SetLineStatusValue llRandomFlight, .GetLineStatusValue(llRandomFlight) + 10

                    ' insert event into TAB_UpcomingEvents
                    frmMain.TAB_UpcomingEvents.InsertTextLine strTimeStamp & sExcelSeparator & sRemark & _
                    sExcelSeparator & .GetLineValues(llRandomFlight), False
                End If
            End If
        Wend
    End With
End Sub


Public Function GetBlockingEventTag(lLineNo As Long, sResType As String, iETOX As Integer, iSTOX As Integer, sTimeRelative As String, iRes1 As Integer, iRes2 As Integer, iFLNO As Integer, ByRef rTab As TABLib.Tab, iBlockBefore As Integer, iBlockAfter As Integer, sRemark As String) As String
    Dim strKeyItems As String
    Dim strTimeStamp As String
    Dim strLineNo As String
    Dim strFields As String
    Dim strData As String
    Dim olETOX As Date
    Dim strTmp As String
    Dim llTmp As Long

    With frmMain.TAB_UpcomingEvents
        ' get the estimated time (or scheduled time if estimated time is not available)
        strTmp = .GetColumnValue(lLineNo, iETOX)
        If IsNumeric(strTmp) = False Then strTmp = .GetColumnValue(lLineNo, iSTOX)
        If IsNumeric(strTmp) = True Then
            olETOX = CedaFullDateToVb(strTmp)
        Else
            GetBlockingEventTag = ""
            MsgBox "Blocking event not possible without estimated or scheduled time!", vbCritical, "Logical error"
            Exit Function
        End If

        If Year(olETOX) > 2000 Then
            strTimeStamp = Format((olETOX - sTimeRelative / 24), "YYYYMMDDhhmmss") & sExcelSeparator & "0"

            ' build the LineTag for fireing the event
            ' PART 1: "main event"
            strTmp = .GetColumnValue(lLineNo, iRes1) '
            strLineNo = rTab.GetLinesByColumnValue(1, strTmp, 0)
            If IsNumeric(strLineNo) = True Then
                llTmp = strLineNo
            Else
                Exit Function
            End If
            strKeyItems = "{-EVENT_1-}{=CMD=}IBT{=TABLE=}BLKTAB"
            strKeyItems = strKeyItems & "{=WHERE=}"
            strFields = "{=FIELDS=}BURN,NAFR,NATO,DAYS,TYPE,TABN,RESN,TIFR,TITO,IBIT"
            strData = "{=DATA=}" & rTab.GetColumnValue(llTmp, 0) & "," & Format((olETOX - iBlockBefore / 60 / 24), "YYYYMMDDhhmm00,") 'BURN,NAFR
            strData = strData & Format((olETOX + iBlockAfter / 60 / 24), "YYYYMMDDhhmm") & "00,1234567, ," 'NATO,DAYS,TYPE
            strData = strData & sResType & "," & sRemark & " " & .GetColumnValue(lLineNo, iFLNO) 'TABN,RESN
            strData = strData & ", , ,0" 'TIFR,TITO,IBIT
            strKeyItems = strKeyItems & strFields & strData

            ' PART 2: if there are more than 1 event (CKIF-CKIT, 2 belts or 2 gates)
            If iRes2 > -1 Then
                strTmp = .GetColumnValue(lLineNo, iRes2)
                If Len(strTmp) > 0 Then
                    strKeyItems = strKeyItems & "{=ADDITIONAL_RES=}" & sResType & "," & _
                    .GetColumnValue(lLineNo, iRes1) & "," & .GetColumnValue(lLineNo, iRes2)
                End If
            End If
        End If
    End With
    GetBlockingEventTag = strKeyItems
End Function

Private Function LogicalCheck_CANCEL()
    Dim strTmp As String
    Dim strFLNOs As String
    Dim strTimestamps As String
    Dim strADIDs As String
    Dim strLineNo As String
    Dim i As Integer
    Dim j As Integer
    Dim llLineNo As Long
    Dim strTime As String

    Dim idxRemark As Integer
    Dim idxFLNO As Integer
    Dim idxADID As Integer
    Dim idxTimestamp As Integer
    Dim idxMs As Integer

    ' check cancel events
    With frmMain.TAB_UpcomingEvents
        idxRemark = GetRealItemNo(.HeaderString, "Remark")
        idxFLNO = GetRealItemNo(.HeaderString, "FLNO")
        idxADID = GetRealItemNo(.HeaderString, "ADID")
        idxTimestamp = GetRealItemNo(.HeaderString, "Timestamp")
        idxMs = GetRealItemNo(.HeaderString, "ms")

        'find out the cancel flights and remember flight information
        strLineNo = .GetLinesByColumnValue(idxRemark, "CANCEL", 0)
        For i = 1 To ItemCount(strLineNo, ",")
            strFLNOs = strFLNOs & .GetColumnValue(CLng(GetItem(strLineNo, i, ",")), idxFLNO) & ","
            strADIDs = strADIDs & .GetColumnValue(CLng(GetItem(strLineNo, i, ",")), idxADID) & ","
            strTimestamps = strTimestamps & .GetColumnValue(CLng(GetItem(strLineNo, i, ",")), idxTimestamp) & _
                .GetColumnValue(CLng(GetItem(strLineNo, i, ",")), idxMs) & ","
        Next

        For i = 1 To ItemCount(strFLNOs, ",") - 1
            strLineNo = .GetLinesByColumnValue(idxFLNO, GetItem(strFLNOs, i, ","), 0)
            If Len(strLineNo) > 0 Then
                For j = ItemCount(strLineNo, ",") To 1 Step -1
                    llLineNo = CLng(GetItem(strLineNo, j, ","))
                    strTime = .GetColumnValue(llLineNo, idxTimestamp) & .GetColumnValue(llLineNo, idxMs)
                    If strTime > GetItem(strTimestamps, i, ",") Then
                        ' events later than the cancel event shall be deleted
                        frmMain.TAB_UpcomingEvents.DeleteLine llLineNo
                    Else
                        If UCase(.GetColumnValue(llLineNo, idxRemark)) <> "CANCEL" Then
                            ' make sure that only allowed events are sent before the cancel
                            If GetItem(strADIDs, i, ",") = "A" Then
                                If UCase(.GetColumnValue(llLineNo, idxRemark)) <> "ETOA EVENT" Then
                                    .DeleteLine llLineNo
                                End If
                            Else
                                If UCase(.GetColumnValue(llLineNo, idxRemark)) <> "ETOD EVENT" Then
                                    .DeleteLine llLineNo
                                End If
                            End If
                        End If
                    End If
                Next
            End If
        Next
    End With
End Function

Private Function LogicalCheck_DIVERTED()
    Dim strTmp As String
    Dim strFLNOs As String
    Dim strTimestamps As String
    Dim strLineNo As String
    Dim i As Integer
    Dim j As Integer
    Dim llLineNo As Long
    Dim strTime As String

    Dim idxRemark As Integer
    Dim idxFLNO As Integer
    Dim idxADID As Integer
    Dim idxTimestamp As Integer
    Dim idxMs As Integer

    ' check diverted flight events
    With frmMain.TAB_UpcomingEvents
        idxRemark = GetRealItemNo(.HeaderString, "Remark")
        idxFLNO = GetRealItemNo(.HeaderString, "FLNO")
        idxTimestamp = GetRealItemNo(.HeaderString, "Timestamp")
        idxMs = GetRealItemNo(.HeaderString, "ms")

        'find out the cancel flights and remember flight information
        strLineNo = .GetLinesByColumnValue(idxRemark, "DIVERTED FLIGHT", 0)
        For i = 1 To ItemCount(strLineNo, ",")
            strFLNOs = strFLNOs & .GetColumnValue(CLng(GetItem(strLineNo, i, ",")), idxFLNO) & ","
            strTimestamps = strTimestamps & .GetColumnValue(CLng(GetItem(strLineNo, i, ",")), idxTimestamp) & _
                .GetColumnValue(CLng(GetItem(strLineNo, i, ",")), idxMs) & ","
        Next

        For i = 1 To ItemCount(strFLNOs, ",") - 1
            strLineNo = .GetLinesByColumnValue(idxFLNO, GetItem(strFLNOs, i, ","), 0)
            If Len(strLineNo) > 0 Then
                For j = ItemCount(strLineNo, ",") To 1 Step -1
                    llLineNo = CLng(GetItem(strLineNo, j, ","))
                    strTime = .GetColumnValue(llLineNo, idxTimestamp) & .GetColumnValue(llLineNo, idxMs)
                    If strTime > GetItem(strTimestamps, i, ",") Then
                        ' events later than the diverted flight event shall be deleted
                        frmMain.TAB_UpcomingEvents.DeleteLine llLineNo
                    Else
                        If UCase(.GetColumnValue(llLineNo, idxRemark)) <> "DIVERTED FLIGHT" Then
                            ' make sure that only allowed events are sent before the divertion
                            If UCase(.GetColumnValue(llLineNo, idxRemark)) <> "ETOA EVENT" Then
                                .DeleteLine llLineNo
                            End If
                        End If
                    End If
                Next
            End If
        Next
    End With
End Function


Private Function LogicalCheck_ReturnTaxi()
    Dim strTmp As String
    Dim strFLNOs As String
    Dim strTimestamps As String
    Dim strLineNo As String
    Dim i As Integer
    Dim j As Integer
    Dim llLineNo As Long
    Dim strTime As String

    Dim idxRemark As Integer
    Dim idxFLNO As Integer
    Dim idxADID As Integer
    Dim idxTimestamp As Integer
    Dim idxMs As Integer

    ' check diverted flight events
    With frmMain.TAB_UpcomingEvents
        idxRemark = GetRealItemNo(.HeaderString, "Remark")
        idxFLNO = GetRealItemNo(.HeaderString, "FLNO")
        idxTimestamp = GetRealItemNo(.HeaderString, "Timestamp")
        idxMs = GetRealItemNo(.HeaderString, "ms")

        'find out the cancel flights and remember flight information
        strLineNo = .GetLinesByColumnValue(idxRemark, "RETURN TAXI", 0)
        For i = 1 To ItemCount(strLineNo, ",")
            strFLNOs = strFLNOs & .GetColumnValue(CLng(GetItem(strLineNo, i, ",")), idxFLNO) & ","
            strTimestamps = strTimestamps & .GetColumnValue(CLng(GetItem(strLineNo, i, ",")), idxTimestamp) & _
                .GetColumnValue(CLng(GetItem(strLineNo, i, ",")), idxMs) & ","
        Next

        For i = 1 To ItemCount(strFLNOs, ",") - 1
            strLineNo = .GetLinesByColumnValue(idxFLNO, GetItem(strFLNOs, i, ","), 0)
            If Len(strLineNo) > 0 Then
                For j = ItemCount(strLineNo, ",") To 1 Step -1
                    llLineNo = CLng(GetItem(strLineNo, j, ","))
                    
                    ' make sure that the OFBL event is before return taxi
                    If UCase(.GetColumnValue(llLineNo, idxRemark)) = "OFBL EVENT" Then
                        Dim olTmpDate As Date
                        olTmpDate = CedaFullDateToVb(Left(GetItem(strTimestamps, i, ","), 14))
                        olTmpDate = DateAdd("m", -5, olTmpDate)
                    End If
                    
                    
                    strTime = .GetColumnValue(llLineNo, idxTimestamp) & .GetColumnValue(llLineNo, idxMs)
                    If strTime > GetItem(strTimestamps, i, ",") Then
                        ' events later than the diverted flight event shall be deleted
                        frmMain.TAB_UpcomingEvents.DeleteLine llLineNo
                    Else
                        If UCase(.GetColumnValue(llLineNo, idxRemark)) <> "RETURN TAXI" Then
                            ' make sure that only allowed events are sent before the divertion
                            If UCase(.GetColumnValue(llLineNo, idxRemark)) <> "EOFBL EVENT" Then
                                .DeleteLine llLineNo
                            End If
                        End If
                    End If
                Next
            End If
        Next
    End With
End Function

