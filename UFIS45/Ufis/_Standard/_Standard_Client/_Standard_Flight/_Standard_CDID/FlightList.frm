VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form FlightList 
   Caption         =   "Flight List"
   ClientHeight    =   5865
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6285
   LinkTopic       =   "Form1"
   ScaleHeight     =   5865
   ScaleWidth      =   6285
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picMainButtons 
      Height          =   435
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   6015
      TabIndex        =   0
      Top             =   0
      Width           =   6075
   End
   Begin TABLib.TAB tabFlightList 
      Height          =   5295
      Left            =   0
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   480
      Width           =   4455
      _Version        =   65536
      _ExtentX        =   7858
      _ExtentY        =   9340
      _StockProps     =   64
   End
End
Attribute VB_Name = "FlightList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private rsAft As ADODB.Recordset
Private rsView As ADODB.Recordset

Private colViewParams As CollectionExtended
Private colSearchParams As CollectionExtended

Private IsFormActivated As Boolean

Public Sub LoadData(ByVal LoadParam As Variant)
    Dim vntDataSources As Variant
    Dim vntDataSource As Variant
    Dim strWhere As String
    Dim colLocalParams As New CollectionExtended
    
    If IsObject(LoadParam) Then
        If TypeOf LoadParam Is CollectionExtended Then
            Set colViewParams = LoadParam
        Else
            Set colViewParams = GetDefaultParamCollection()
        End If
    Else
        Set colViewParams = GetDefaultParamCollection()
    End If
    
    If pstrDataSource = "" Then
        pstrDataSource = GetConfigEntry(colConfigs, "MAIN", "DATA_SOURCE", "AFTTAB")
    End If
    If InStr(pstrDataSource & ",", "AFTTAB,") > 0 Then
        strWhere = BuildWhereClause(colViewParams)
                
        colLocalParams.Add "URNO", "OrderByClause"
        colLocalParams.Add strWhere, "WhereClause"
        
        pstrAftUrnoList = AddDataSource(MyMainForm.AppType, colDataSources, "AFTTAB", colLocalParams, "URNO")
        
        Set rsAft = colDataSources.Item("AFTTAB")
        If Not (rsAft Is Nothing) Then
            If Not (rsAft.BOF And rsAft.EOF) Then
                Call ReadDataSource(pstrAftUrnoList)
            End If
        End If
    End If
    
    Call PopulateGridData(ftypArrival + ftypDeparture, "", rsAft)
    If tabFlightList.GetLineCount > 1 Then
        ChangeRowSelection 0
    End If
End Sub

Private Function BuildWhereClause(ViewParamCollection As CollectionExtended) As String
    Dim dtmNow As Date
    Dim dtmStartDate As Date
    Dim dtmEndDate As Date
    Dim strStartDate As String
    Dim strEndDate As String
    Dim strStartTime As String
    Dim strEndTime As String
    Dim strRelStart As String
    Dim strRelEnd As String
    Dim vntDates As Variant
    Dim strParamVal As String
    Dim strWhere As String
    
    dtmNow = Now
    strRelStart = ViewParamCollection.Item("RELPRDSTART")
    strRelEnd = ViewParamCollection.Item("RELPRDEND")
        
    strStartDate = ViewParamCollection.Item("STARTDATE")
    strStartTime = ViewParamCollection.Item("STARTTIME")
    strEndDate = ViewParamCollection.Item("ENDDATE")
    strEndTime = ViewParamCollection.Item("ENDTIME")
    
    vntDates = GetParamDateArray(strStartDate, strStartTime, strEndDate, strEndTime, strRelStart, strRelEnd, dtmNow)
    dtmStartDate = vntDates(0)
    dtmEndDate = vntDates(1)
    
    'check ALC2
    strParamVal = ViewParamCollection.Item("ALC2")
    If Trim(strParamVal) <> "" Then
        strWhere = strWhere & "AND ALC2 = '" & strParamVal & "'"
    End If
    
    'check FLTN
    strParamVal = ViewParamCollection.Item("FLTN")
    If Trim(strParamVal) <> "" Then
        strWhere = strWhere & "AND FLTN = '" & strParamVal & "'"
    End If
    
    'check FLNS
    strParamVal = ViewParamCollection.Item("FLNS")
    If Trim(strParamVal) <> "" Then
        strWhere = strWhere & "AND FLNS = '" & strParamVal & "'"
    End If
    
    'check AIRLINE
    strParamVal = ViewParamCollection.Item("AIRLINE")
    If Trim(strParamVal) <> "" Then
        strParamVal = "'" & Replace(strParamVal, ",", "','") & "'"
        strWhere = strWhere & " AND (ALC3 IN (" & strParamVal & "))"
    End If
    
    'check NATURE
    strParamVal = ViewParamCollection.Item("NATURE")
    If Trim(strParamVal) <> "" Then
        strParamVal = "'" & Replace(strParamVal, ",", "','") & "'"
        strWhere = strWhere & " AND (TTYP IN (" & strParamVal & "))"
    End If

    dtmStartDate = LocalDateToUTC(dtmStartDate, UtcTimeDiff)
    dtmEndDate = LocalDateToUTC(dtmEndDate, UtcTimeDiff)
    strStartDate = Format(dtmStartDate, "YYYYMMDDhhmmss")
    strEndDate = Format(dtmEndDate, "YYYYMMDDhhmmss")
    
    'add to paramcollection -> UTC START/END Date
    ViewParamCollection.Add dtmStartDate, "UTCSTARTDATE"
    ViewParamCollection.Add dtmEndDate, "UTCENDDATE"
            
    BuildWhereClause = "((TIFA BETWEEN '" & strStartDate & "' AND '" & strEndDate & "') OR " & _
        "(STOA BETWEEN '" & strStartDate & "' AND '" & strEndDate & "')) AND " & _
        "((TIFD BETWEEN '" & strStartDate & "' AND '" & strEndDate & "') OR " & _
        "(STOD BETWEEN '" & strStartDate & "' AND '" & strEndDate & "')) " & _
        strWhere
End Function

Private Function GetDefaultParamCollection() As CollectionExtended
    Dim strRelBefore As String
    Dim strRelAfter As String
    
    rsView.Find "URNO = -1", , , 1
    If rsView.EOF Then
        strRelBefore = GetConfigEntry(colConfigs, MyMainForm.AppType, "LOAD_REL_BEFORE", "2")
        strRelAfter = GetConfigEntry(colConfigs, MyMainForm.AppType, "LOAD_REL_AFTER", "2")
            
        Set GetDefaultParamCollection = New CollectionExtended
        GetDefaultParamCollection.Add "<Default>", "VIEW"
        GetDefaultParamCollection.Add strRelBefore, "RELPRDSTART"
        GetDefaultParamCollection.Add strRelAfter, "RELPRDEND"
    Else
        Set GetDefaultParamCollection = GetViewParamCollection(rsView.Fields("TEXT").Value)
    End If
End Function

Private Sub AddDefaultView()
    Dim AppName As String
    Dim strRelBefore As String
    Dim strRelAfter As String
    
    AppName = Mid(colParameters.Item("AppName"), 2)
    AppName = Left(AppName, Len(AppName) - 1)
        
    strRelBefore = GetConfigEntry(colConfigs, "MAIN", "LOAD_REL_BEFORE", "2")
    strRelAfter = GetConfigEntry(colConfigs, "MAIN", "LOAD_REL_AFTER", "2")
    
    rsView.AddNew
    rsView.Fields("URNO").Value = -1
    rsView.Fields("APPN").Value = AppName
    rsView.Fields("TEXT").Value = "VIEW=<Default>;RELPRDSTART=" & strRelBefore & ";" & _
        "RELPRDEND=" & strRelAfter
    rsView.Fields("PKNO").Value = LoginUserName
    rsView.Update
End Sub

Private Sub ShowFlightViewForm(ByVal Search As Boolean)
    Dim oViewForm As FlightView
    Dim strView As String
    Dim chk As CheckBox
    
    If Search Then
        If colDataSources.Exists("AFTTAB") Then
            If SearchViewForm Is Nothing Then
                Set SearchViewForm = New FlightView
            End If
            Set oViewForm = SearchViewForm
        End If
    Else
        Set oViewForm = FlightView
    End If
    
    If Not (oViewForm Is Nothing) Then
        oViewForm.IsSearchForm = Search
        If Not Search Then
            If FormIsLoaded("MainButtons") Then
                oViewForm.cboView.ListIndex = MainButtons.cboView.ListIndex
            End If
        End If
        oViewForm.Show vbModal
        strView = oViewForm.ViewString
        
        If strView <> "" Then
            If Search Then
                Set colSearchParams = GetViewParamCollection(strView, Search)
                Call ShowSearchList(colSearchParams)
            Else
                Set colViewParams = GetViewParamCollection(strView, Search)
                Call LoadData(colViewParams)
            End If
        End If
    End If
End Sub

Private Function ShowSearchList(colParams As CollectionExtended)
    Static oSearchListForm As Form
    Dim i As Integer
    Dim intColCount As Long
    Dim strColIndex As String
    Dim rsView As ADODB.Recordset
    
    strColIndex = ""
    intColCount = Split(tabFlightList.GetMainHeaderRanges, ",")(0)
    For i = 0 To intColCount - 1
        strColIndex = strColIndex & "," & CStr(i)
    Next i
    If strColIndex <> "" Then strColIndex = Mid(strColIndex, 2)
        
    If (oSearchListForm Is Nothing) Then
        Set oSearchListForm = SearchList
        oSearchListForm.SetParentForm Me
        'oSearchListForm.SetMasterTab tabFlightList, strColIndex, lngAftUrnoColNo
    End If
    
    Set rsView = GetSearchViewRecordset(strColIndex, colParams)
    
    oSearchListForm.PopulateList rsView
    oSearchListForm.Show vbModal
End Function

Private Function GetSearchViewRecordset(ByVal ColIndexList As String, colParams As CollectionExtended) As ADODB.Recordset
    Dim i As Integer
    
    Set GetSearchViewRecordset = New ADODB.Recordset
    GetSearchViewRecordset.Fields.Append "DATA", adVarChar, 400, adFldIsNullable
    GetSearchViewRecordset.Open
    
    For i = 0 To tabFlightList.GetLineCount - 1
        'rsAft.Find "URNO = " & tabFlightList.GetColumnValue(i, lngAftUrnoColNo), , , 1
        If Not rsAft.EOF Then
            'If ValidateDataWithCurrentView(rsAft, colParams) Then
                GetSearchViewRecordset.AddNew
                GetSearchViewRecordset.Fields("DATA").Value = tabFlightList.GetColumnValues(i, ColIndexList)
                GetSearchViewRecordset.Update
            'End If
        End If
    Next i
End Function

Private Function GetViewParamCollection(ByVal ViewParams As String, Optional ByVal Search As Boolean = False) As CollectionExtended
    Dim vntParams As Variant
    Dim vntParam As Variant
    Dim vntParamItems As Variant
    Dim strParamKey As String
    Dim strParamValue As String
    Dim dtmDefStartDate As Date
    Dim dtmDefEndDate As Date
    
    Set GetViewParamCollection = New CollectionExtended
    
    vntParams = Split(ViewParams, ";")
    For Each vntParam In vntParams
        vntParamItems = Split(vntParam, "=")
        strParamKey = vntParamItems(0)
        strParamValue = vntParamItems(1)
        
        GetViewParamCollection.Add strParamValue, strParamKey
    Next vntParam
    
    If Search Then
        dtmDefStartDate = UTCDateToLocal(colViewParams.Item("UTCSTARTDATE"), UtcTimeDiff)
        dtmDefEndDate = UTCDateToLocal(colViewParams.Item("UTCENDDATE"), UtcTimeDiff)
    
        vntParams = GetParamDateArray( _
            GetViewParamCollection.Item("STARTDATE"), GetViewParamCollection.Item("STARTTIME"), _
            GetViewParamCollection.Item("ENDDATE"), GetViewParamCollection.Item("ENDTTIME"), _
            GetViewParamCollection.Item("RELPRDSTART"), GetViewParamCollection.Item("RELPRDEND"), _
            Now, dtmDefStartDate, dtmDefEndDate)
    
        GetViewParamCollection.Add LocalDateToUTC(vntParams(0), UtcTimeDiff), "UTCSTARTDATE"
        GetViewParamCollection.Add LocalDateToUTC(vntParams(1), UtcTimeDiff), "UTCENDDATE"
    End If
End Function

Public Sub RefreshTabByUrno(ByVal FlightUrno As String, Optional ByVal ChangedFields As String)
    Dim colUrnoCols As Collection
    Dim i As Integer
    Dim blnFound As Boolean
    Dim strUrnoFields(1) As String
    Dim intUrnoColIndex As Integer
    Dim strRowIndex As String
    Dim strFields As String
    Dim strValues As String
    Dim blnTwoSided As Boolean
    Dim rsAft As ADODB.Recordset

    Set rsAft = colDataSources.Item("AFTTAB")
    
    If ChangedFields <> "" Then
        If Not IsRefreshNeeded(ChangedFields) Then Exit Sub
    End If
    
    Set colUrnoCols = New Collection

    strUrnoFields(0) = "URNA"
    strUrnoFields(1) = "URND"
    
    blnTwoSided = True
    For i = 0 To UBound(strUrnoFields)
        If blnTwoSided Then
            intUrnoColIndex = GetRealItemNo(tabFlightList.LogicalFieldList, strUrnoFields(i))
            If (intUrnoColIndex >= 0) Then
                colUrnoCols.Add strUrnoFields(i)
            Else
                blnTwoSided = False
            End If
        End If
    Next i

    blnFound = False
    For i = 1 To colUrnoCols.Count
        If Not blnFound Then
            intUrnoColIndex = GetRealItemNo(tabFlightList.LogicalFieldList, colUrnoCols.Item(i))
            If intUrnoColIndex >= 0 Then
                strRowIndex = tabFlightList.GetLinesByColumnValue(intUrnoColIndex, FlightUrno, 0)
                If IsNumeric(strRowIndex) Then
                    blnFound = True
    
                    Call AddToGrid(tabFlightList, rsAft, "1234567", (colUrnoCols.Item(i) = "URNA"), _
                        blnTwoSided, Val(strRowIndex))
                    
                    If rsAft.Fields("FTYP").Value = "X" Then
                        If tabFlightList.Tag = "" Then
                            tabFlightList.CreateCellObj "Cancelled", vbWhite, vbRed, 0, False, False, False, 0, ""
                            tabFlightList.Tag = "DecorationCreated"
                        End If
                        tabFlightList.SetCellProperty Val(strRowIndex), 1, "Cancelled"
                    End If
                End If
            End If
        End If
    Next i
End Sub

Public Sub FindRowByUrno(ByVal Urno As String, ByVal NotFoundRow As Long)
    Dim colUrnoCols As Collection
    Dim strUrnoFields(1) As String
    Dim blnTwoSided As Boolean
    Dim intUrnoColIndex As Integer
    Dim blnFound As Boolean
    Dim i As Integer
    Dim strRowIndex As String
    
    Set colUrnoCols = New Collection

    strUrnoFields(0) = "URNA"
    strUrnoFields(1) = "URND"
    
    blnTwoSided = True
    For i = 0 To UBound(strUrnoFields)
        If blnTwoSided Then
            intUrnoColIndex = GetRealItemNo(tabFlightList.LogicalFieldList, strUrnoFields(i))
            If (intUrnoColIndex >= 0) Then
                colUrnoCols.Add strUrnoFields(i)
            Else
                blnTwoSided = False
            End If
        End If
    Next i

    blnFound = False
    If Urno <> "" Then
        For i = 1 To colUrnoCols.Count
            If Not blnFound Then
                intUrnoColIndex = GetRealItemNo(tabFlightList.LogicalFieldList, colUrnoCols.Item(i))
                If intUrnoColIndex >= 0 Then
                    strRowIndex = tabFlightList.GetLinesByColumnValue(intUrnoColIndex, Urno, 0)
                    If IsNumeric(strRowIndex) Then
                        blnFound = True
                        Exit Sub
                    End If
                End If
            End If
        Next i
    End If
    
    If blnFound Then
        Call ChangeRowSelection(Val(strRowIndex))
    Else
        Call ChangeRowSelection(NotFoundRow)
    End If
End Sub

Public Sub ChangeRowSelection(ByVal NewRow As Long)
    If NewRow < tabFlightList.GetLineCount Then
        tabFlightList.SetCurrentSelection NewRow
    Else
        FlightDetails.ShowFlightDetails "", ""
    End If
End Sub

Public Sub MainControlsExecuted(ByVal Key As String, Optional ByVal Value As Variant)
    Select Case Key
        Case "view"
            Call ShowFlightViewForm(False)
        Case "viewselect"
            'Call ChangeView(Value(0), Value(1))
        Case "search"
            Call ShowFlightViewForm(True)
    End Select
End Sub

Private Sub ReadDataSource(ByVal FlightUrno As String)
    Dim intUrnoCountInWhereClause As Integer
    Dim vntFlightUrno As Variant
    Dim intUrnoCount As Integer
    Dim intArrayCount As Integer
    Dim i As Integer, j As Integer
    Dim strUrno As String
    Dim intStart As Integer, intEnd As Integer
    Dim strWhere As String
    Dim strDataSource As String
    Dim vntDataSources As Variant
    Dim vntDataSourceItem As Variant
    
    intUrnoCountInWhereClause = Val(GetConfigEntry(colConfigs, "MAIN", "URNO_COUNT_IN_WHERE_CLAUSE", "300"))
    
    vntFlightUrno = Split(FlightUrno, ",")
    intUrnoCount = UBound(vntFlightUrno) + 1
    If intUrnoCount > intUrnoCountInWhereClause Then
        FlightUrno = ""
        intArrayCount = Int(intUrnoCount / intUrnoCountInWhereClause) + 1
        For i = 1 To intArrayCount
            intStart = intUrnoCountInWhereClause * (i - 1)
            intEnd = intStart + (intUrnoCountInWhereClause - 1)
            If intEnd > intUrnoCount - 1 Then
                intEnd = intUrnoCount - 1
            End If
        
            strUrno = ""
            For j = intStart To intEnd
                strUrno = strUrno & "," & vntFlightUrno(j)
            Next j
            strUrno = Mid(strUrno, 2)
            
            FlightUrno = FlightUrno & "|" & strUrno
        Next i
        If FlightUrno <> "" Then FlightUrno = Mid(FlightUrno, 2)
    End If
    
   If colParameters.Exists("AftUrnoList") Then
        colParameters.Remove "AftUrnoList"
    End If
    colParameters.Add FlightUrno, "AftUrnoList"
    
    If pstrDataSource = "" Then
        pstrDataSource = GetConfigEntry(colConfigs, MyMainForm.AppType, "DATA_SOURCE", "")
    End If
    vntDataSources = Split(pstrDataSource, ",")
    For Each vntDataSourceItem In vntDataSources
        If vntDataSourceItem <> "AFTTAB" Then
            Call AddDataSource(MyMainForm.AppType, colDataSources, vntDataSourceItem, colParameters, , True, True, "|")
        End If
    Next vntDataSourceItem
End Sub

Private Function IsRefreshNeeded(ByVal ChangedFields As String) As Boolean
    Const FIELD_LIST = "STOA,STOD,FLNO,ACT3,ACT5,FTYP"
    
    IsRefreshNeeded = IsInList(ChangedFields, FIELD_LIST)
End Function

Public Sub PopulateGridData(ByVal eFlightType As FlightTypeEnum, ByVal strDays As String, rsAft As Recordset)
    Dim strFieldList As String
    Dim strHeaderString As String
    Dim strHeaderLengthString As String
    Dim strRanges As String
    Dim strValues As String
    Dim strADID As String
    Dim strArrUrnoList As String
    Dim strDepUrnoList As String
    Dim i As Long
    
    If eFlightType >= ftypArrival + ftypDeparture Then
        strFieldList = "STD,URNA,FLNA,STOA_D,STOA_T,ACTA,RKEY,URND,FLND,STOD_D,STOD_T,ACTD"
        strHeaderString = "STD,URNO,FLNR,DT,STA,A/C,RKEY,URNO,FLNR,DT,STD,A/C"
        strHeaderLengthString = "0,0,70,30,50,80,0,0,70,30,50,80"
        strRanges = "1,5,6"
        strValues = ",ARRIVAL,DEPARTURE"
    Else
        Select Case eFlightType
            Case ftypArrival
                strFieldList = "STD,URNA,FLNA,STOA_D,STOA_T,ACTA"
                strHeaderString = "STD,URNO,FLNR,DT,STA,A/C"
                strHeaderLengthString = "0,0,70,30,50,80"
                strRanges = "1,5"
                strValues = ",ARRIVAL"
            Case ftypDeparture
                strFieldList = "STD,URND,FLND,STOD_D,STOD_T,ACTD"
                strHeaderString = "STD,URNO,FLNR,DT,STD,A/C"
                strHeaderLengthString = "0,0,70,30,50,80"
                strRanges = "1,5"
                strValues = ",DEPARTURE"
        End Select
    End If
    
    If strDays = "" Then strDays = "1234567"
    
    With tabFlightList
        .ResetContent
        .LogicalFieldList = strFieldList
        .HeaderString = strHeaderString
        .HeaderLengthString = strHeaderLengthString
        .SetMainHeaderValues strRanges, strValues, ""
        .MainHeader = True
        .AutoSizeByHeader = True
        .ShowHorzScroller True
        .ShowVertScroller True
        .Tag = ""
        
        'Start filling in the grid
        If Not (rsAft Is Nothing) Then
            strArrUrnoList = ""
            strDepUrnoList = ""
        
            If Not (rsAft.BOF And rsAft.EOF) Then
                rsAft.MoveFirst
            End If
            Do While Not rsAft.EOF
                strADID = Trim(rsAft.Fields("ADID").Value)
                
                Select Case eFlightType
                    Case ftypArrival
                        Call AddToGrid(tabFlightList, rsAft, strDays, True)
                    Case ftypDeparture
                        Call AddToGrid(tabFlightList, rsAft, strDays, False)
                    Case Else
                        Select Case strADID
                            Case "A"
                                Call AddToGrid(tabFlightList, rsAft, strDays, True)
                            Case "D", "B"
                                Call AddToGrid(tabFlightList, rsAft, strDays, False, True)
                        End Select
                End Select
            
                Select Case rsAft.Fields("FTYP").Value
                    Case "X" 'Cancelled
                        If .Tag = "" Then
                            .CreateCellObj "Cancelled", vbWhite, vbRed, 0, False, False, False, 0, ""
                            .Tag = "DecorationCreated"
                        End If
                        .SetCellProperty .GetLineCount - 1, 1, "Cancelled"
                End Select
                
                rsAft.MoveNext
            Loop
            
            .Sort 0, True, True
            .Refresh
        End If
    End With
End Sub

Private Sub AddToGrid(TabObject As TABLib.Tab, rsData As ADODB.Recordset, _
ByVal strDays As String, ByVal IsArrival As Boolean, Optional ByVal IsTwoSide As Boolean = False, _
Optional ByVal RowNumber As Long = -1)
    Dim strField As String
    Dim dtmDate As Date
    Dim strDay As String
    Dim strHourMin As String
    Dim strFullDate As String
    Dim intDayOfWeek As Integer
    Dim strLine As String
    Dim strLineNo As String
    Dim intUrnoColNo As Integer
    Dim strSTD As String
    
    If IsArrival Then
        strField = "STOA"
    Else
        strField = "STOD"
    End If
    
    If IsNull(rsData.Fields(strField).Value) Then
        dtmDate = CDate(0)
        strDay = ""
        strHourMin = ""
        strFullDate = ""
    Else
        dtmDate = UTCDateToLocal(rsData.Fields(strField).Value, UtcTimeDiff)
        strDay = Format(dtmDate, "dd")
        strHourMin = Format(dtmDate, "hh:mm")
        strFullDate = Format(dtmDate, "yyyymmddhhmmss")
    End If
    
    'check the day
    intDayOfWeek = Weekday(dtmDate, vbMonday)
    If dtmDate = CDate(0) Or InStr(strDays, CStr(intDayOfWeek)) > 0 Then
        strLine = CStr(rsData.Fields("URNO").Value) & "," & _
            Trim(rsData.Fields("FLNO").Value) & "," & _
            strDay & "," & _
            strHourMin & "," & _
            Trim(rsData.Fields("ACT3").Value) & "/" & _
            Trim(rsData.Fields("ACT5").Value)
           
        If Not IsTwoSide Then
            strLine = strFullDate & "," & strLine
            If RowNumber = -1 Then
                TabObject.InsertTextLine strLine, False
            Else
                TabObject.SetFieldValues RowNumber, TabObject.LogicalFieldList, strLine
            End If
        Else
            If IsArrival Then
                strLine = strFullDate & "," & strLine
            
                If RowNumber = -1 Then
                    intUrnoColNo = GetRealItemNo(TabObject.LogicalFieldList, "RKEY")
                    strLineNo = TabObject.GetLinesByColumnValue(intUrnoColNo, rsData.Fields("URNO").Value, 0)
                    If IsNumeric(strLineNo) Then
                        RowNumber = Val(strLineNo)
                    End If
                End If
                If RowNumber <> -1 Then
                    TabObject.SetFieldValues RowNumber, "STD,URNA,FLNA,STOA_D,STOA_T,ACTA", strLine
                Else
                    strLine = strLine & String(7, ",")
                    TabObject.InsertTextLine strLine, False
                End If
            Else
                If RowNumber = -1 Then
                    intUrnoColNo = GetRealItemNo(TabObject.LogicalFieldList, "URNA")
                    strLineNo = TabObject.GetLinesByColumnValue(intUrnoColNo, rsData.Fields("RKEY").Value, 0)
                    If IsNumeric(strLineNo) Then
                        RowNumber = Val(strLineNo)
                    End If
                End If
                If RowNumber <> -1 Then
                    strSTD = TabObject.GetFieldValue(RowNumber, "STD")
                    If strSTD = "" Then
                        strLine = strFullDate & "," & strLine
                    Else
                        strLine = strSTD & "," & strLine
                    End If
                    strLine = CStr(rsData.Fields("RKEY").Value) & "," & strLine
                    TabObject.SetFieldValues RowNumber, "RKEY,STD,URND,FLND,STOD_D,STOD_T,ACTD", strLine
                Else
                    strLine = strFullDate & String(6, ",") & CStr(rsData.Fields("RKEY").Value) & "," & strLine
                    TabObject.InsertTextLine strLine, False
                End If
            End If
        End If
    End If
End Sub

Private Sub Form_Activate()
    If Not IsFormActivated Then
        Call Form_Resize
        
        IsFormActivated = True
    End If
End Sub

Private Sub Form_Load()
    Dim FormToShow As Form
    
    Set rsView = colDataSources.Item("VCDTAB")
    Call AddDefaultView
    
    If pblnFlightListMainButtons Then
        Load MainButtons
    
        Set FormToShow = MainButtons
        Set MainButtons.ContainerForm = Me
        MainButtons.cmdMainButtons(2).Visible = False
        MainButtons.cmdMainButtons(3).Visible = False
        Call ChangeFormBorderStyle(FormToShow, vbBSNone)
        FormWithinForm picMainButtons, FormToShow
        'FormToShow.Move 0, 0, picMainButtons.Width, picMainButtons.Height
        FormToShow.Show
        
        picMainButtons.Visible = True
    Else
        picMainButtons.Visible = False
    End If

    Call PopulateGridData(ftypArrival + ftypDeparture, "", Nothing)
    
    If pblnFlightListWindow Then
        Call SetWindowSizePos(Me, colConfigs, "FLIGHT_LIST")
    End If
    
    IsFormActivated = False
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next

    If UnloadMode = 0 Then
        If pblnFlightListMainButtons Then
            Unload MainButtons
        End If
    End If
End Sub

Private Sub Form_Resize()
    Dim sngTop As Single
    Dim intAdjustment As Integer

    On Error Resume Next
    
    If Me.WindowState <> vbMinimized Then
        sngTop = 0
        If picMainButtons.Visible Then sngTop = sngTop + picMainButtons.Height
        
        intAdjustment = 0
        If pblnIsStandAlone And Not pblnFlightListWindow Then
            intAdjustment = 50
        End If
        
        picMainButtons.Width = Me.ScaleWidth
        tabFlightList.Move 0, sngTop, Me.ScaleWidth - intAdjustment, Me.ScaleHeight - sngTop - intAdjustment
    End If
End Sub

Private Sub picMainButtons_Resize()
    If pblnFlightListMainButtons Then
        MainButtons.Move 0, 0, picMainButtons.Width, picMainButtons.Height
    End If
End Sub

Private Sub tabFlightList_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim strURNO_A As String
    Dim strURNO_D As String

    If Selected Then
        With tabFlightList
            strURNO_A = .GetFieldValues(LineNo, "URNA")
            strURNO_D = .GetFieldValues(LineNo, "URND")
            
            FlightDetails.ShowFlightDetails strURNO_A, strURNO_D
        End With
    End If
End Sub
