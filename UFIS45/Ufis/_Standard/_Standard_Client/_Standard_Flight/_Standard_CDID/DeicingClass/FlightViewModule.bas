Attribute VB_Name = "FlightViewModule"
Option Explicit

Public MainButtons As Form

Public pblnShowSplashScreen As Boolean
Public LoginUserName As String
Public p_AppName As String
Public p_AppVersion As String

Public Function StartApp(ByVal LoadBasicDataOnly As Boolean) As Boolean
    Dim sAppType As String
    Dim sFlightUrno As String
    Dim bStandAlone As Boolean
    Dim strAppName As String
    
    StartApp = True
    
    If Not DetermineIniFile(myIniFullName) Then
        
        MsgBox "Sorry. Couldn't find the INI file!" & vbNewLine & myIniFullName
        
        StartApp = False
        
    Else
        
        If LoginUserName = "" Then
        
            MsgBox "Sorry. Couldn't find the user name!" & vbNewLine & myIniFullName
            
            StartApp = False
        
        Else
            If LoadBasicDataOnly Then
                sAppType = "DE-ICING"
                bStandAlone = True
            
                Set oUfisServer = New UfisServer
                oUfisServer.StayConnected = True
                oUfisServer.TwsCode = ".."
                oUfisServer.ConnectToCeda
                oUfisServer.UserName = LoginUserName
                oUfisServer.ModName = p_AppName
                oUfisServer.AppVersion = p_AppVersion
            
                colParameters.Add "'" & LoginUserName & "'", "LoginUserName"
                colParameters.Add LoginUserName, "UserName"
                
                'get the default time diff
                Call GetUTCLocalTimeDiff(myIniFullName)
                
                Call PrepareEnv(sAppType, bStandAlone)
            Else
                sFlightUrno = ""
                
                
                Set MyMainForm = Deicing
                Set MainButtons = MyMainForm
                MyMainForm.Show vbModal
            End If
        End If
    End If
End Function

Private Function DetermineIniFile(ByVal IniFile As String) As Boolean
    Dim tmpStr As String
    Dim IniFound As Boolean
    
    If IniFile = "" Then
        If DEFAULT_CEDA_INI = "" Then GetUfisDir
        myIniPath = UFIS_SYSTEM
        
        myIniFile = "De-icing.ini"
        myIniFullName = myIniPath & "\" & myIniFile
            
        tmpStr = Dir(myIniFullName)
        IniFound = (UCase(tmpStr) = UCase(myIniFile))
        
        DetermineIniFile = IniFound
    Else
        DetermineIniFile = (Dir(IniFile) <> "")
    End If
End Function

Private Function GetChangedFields(ByVal Fields As String) As String
    Dim intLoc As Integer
    
    intLoc = InStr(Fields, "#")
    If intLoc > 0 Then
        GetChangedFields = Replace(Mid(Fields, intLoc), "#", "")
    End If
End Function

Private Function CreateVirtualTable(ByVal TableOperation As TableOperationType, ByVal Fields As String, _
ByVal Data As String, Optional ByVal ChangedFields As String, Optional ByVal CedaSqlKey As String) As ADODB.Recordset
    Const MAX_VARCHAR_LENGTH = 8184

    Dim rsData As ADODB.Recordset
    Dim aFields As Variant
    Dim aData As Variant
    Dim i As Integer
    Dim colUpdateFieldsIndex As New Collection
    Dim strChangedFields As String
    Dim strField As String
    
    If TableOperation = toEdit Then
        If InStr(ChangedFields, "URNO") <= 0 Then
            If InStr(Fields, "URNO") > 0 Then
                ChangedFields = "URNO," & ChangedFields
            Else
                CedaSqlKey = Replace(CedaSqlKey, " ", "")
                i = InStr(CedaSqlKey, "URNO=")
                If i > 0 Then
                    CedaSqlKey = Mid(CedaSqlKey, i + Len("URNO="))
                    
                    i = InStr(CedaSqlKey, vbLf)
                    If i > 0 Then
                        CedaSqlKey = Left(CedaSqlKey, i - 1)
                    End If
                    
                    i = InStr(CedaSqlKey, "AND")
                    If i > 0 Then
                        CedaSqlKey = Left(CedaSqlKey, i - 1)
                    End If
                    
                    If Left(CedaSqlKey, 1) = "'" And Right(CedaSqlKey, 1) = "'" Then
                        CedaSqlKey = Mid(CedaSqlKey, 2)
                        CedaSqlKey = Left(CedaSqlKey, Len(CedaSqlKey) - 1)
                    End If
                    
                    Fields = "URNO," & Fields
                    Data = CedaSqlKey & "," & Data
                    
                    ChangedFields = "URNO," & ChangedFields
                End If
            End If
        End If
        strChangedFields = ChangedFields
    End If
    
    aFields = Split(Fields, ",")
    aData = Split(Data, ",")
    
    For i = 0 To UBound(aFields)
        strField = aFields(i)
        If TableOperation = toInsert Then
            colUpdateFieldsIndex.Add i
        ElseIf TableOperation = toEdit Then
            If InStr(strChangedFields, strField) > 0 Then
                colUpdateFieldsIndex.Add i
            End If
        End If
    Next i
    
    Set rsData = New ADODB.Recordset
    For i = 1 To colUpdateFieldsIndex.Count
        strField = aFields(colUpdateFieldsIndex.Item(i))
        rsData.Fields.Append strField, adVarChar, MAX_VARCHAR_LENGTH, adFldIsNullable
    Next i
    rsData.Open
    
    rsData.AddNew
    For i = 1 To colUpdateFieldsIndex.Count
        rsData.Fields(i - 1).Value = aData(colUpdateFieldsIndex.Item(i))
    Next i
    rsData.Update
    
    Set CreateVirtualTable = rsData
End Function

Public Sub UpdateMemoryTable(rsData As ADODB.Recordset, rsBc As ADODB.Recordset, ByVal TableOperation As TableOperationType)
    Dim fld As ADODB.Field
    Dim strFields As String
    Dim strValue As String

    If Not (rsBc.BOF And rsBc.EOF) Then
        Select Case TableOperation
            Case toInsert, toEdit
                strFields = ""
                For Each fld In rsBc.Fields
                    strFields = strFields & fld.Name & ","
                Next fld
                
                If TableOperation = toInsert Then
                    rsData.Find "URNO = " & rsBc.Fields("URNO").Value, , , 1
                    If rsData.EOF Then
                        rsData.AddNew
                    End If
                End If
                For Each fld In rsData.Fields
                    If InStr(strFields, fld.Name) > 0 Then
                        strValue = rsBc.Fields(fld.Name).Value
                        
                        Select Case fld.Type
                            Case adVarChar
                                fld.Value = UfisLib.CleanString(strValue, _
                                                SERVER_TO_CLIENT, _
                                                True)
                            Case adInteger, adBigInt, adSingle, adDouble
                                If Trim(strValue) = "" Then
                                    fld.Value = Null
                                Else
                                    fld.Value = Val(strValue)
                                End If
                            Case adDate
                                If Trim(strValue) = "" Then
                                    fld.Value = Null
                                Else
                                    fld.Value = UfisLib.CedaFullDateToVb(strValue)
                                End If
                        End Select
                    Else
                        If TableOperation = toInsert Then
                            Select Case fld.Type
                                Case adVarChar
                                    fld.Value = ""
                                Case adInteger, adBigInt, adSingle, adDouble
                                    fld.Value = 0
                                Case adDate
                                    fld.Value = Null
                            End Select
                        End If
                    End If
                Next fld
                rsData.Update
        End Select
    End If
End Sub

Private Sub EvaluateBc(ByVal CedaCmd As String, ByVal ObjName As String, ByVal CedaSqlKey As String, ByVal Fields As String, ByVal Data As String)
    Dim rsData As Recordset
    Dim rsBc As ADODB.Recordset
    Dim strChangedFields As String
    Dim strField As String
    
    Set rsData = colDataSources.Item(ObjName)
    
    strField = "URNO"
    Select Case ObjName
        Case "CCATAB"
            strField = "FLNU"
        Case "CSDTAB", "CSCTAB"
            strField = "RURN,JFNO"
    End Select
    
    If Not (rsData Is Nothing) Then
        Select Case CedaCmd
            Case "IRT", "IFR"
                Set rsBc = CreateVirtualTable(toInsert, Fields, Data)
                Call UpdateMemoryTable(rsData, rsBc, toInsert)
            Case "URT", "UFR"
                If CedaCmd = "URT" Then
                    strChangedFields = Fields
                Else
                    strChangedFields = GetChangedFields(Fields)
                End If
                                
                Set rsBc = CreateVirtualTable(toEdit, Fields, Data, strChangedFields, CedaSqlKey)
                If Not (rsBc.BOF And rsBc.EOF) Then
                    CedaSqlKey = rsBc.Fields("URNO").Value
                    
                    rsData.Find "URNO = " & CedaSqlKey, , , 1
                    If Not rsData.EOF Then
                        Call UpdateMemoryTable(rsData, rsBc, toEdit)
                    End If
                End If
            Case "DRT", "DFR"
                CedaSqlKey = HandleRecordDeletion(rsData, ObjName, CedaSqlKey, strField)
            Case "SBC"
                strChangedFields = Data
        End Select
        
        If InStr(pstrDataSource, ObjName) > 0 Then
            MyMainForm.EvaluateBc CedaCmd, ObjName, CedaSqlKey, strChangedFields
        End If
    End If
End Sub

Private Function HandleRecordDeletion(rsData As ADODB.Recordset, ByVal ObjName As String, ByVal CedaSqlKey As String, ByVal ReturnedFields As String) As String
    Dim strFilter As String
    Dim strReturned As String
    Dim aFields As Variant
    Dim i As Integer
    Dim strValue As String
    
    On Error GoTo ErrHandle
    
    aFields = Split(ReturnedFields, ",")
    strFilter = Replace(CedaSqlKey, "WHERE ", "")
    strReturned = ""
    
    rsData.Filter = strFilter
    Do While Not rsData.EOF
        strValue = ""
        For i = 0 To UBound(aFields)
            strValue = strValue & "," & Trim(rsData.Fields(aFields(i)).Value)
        Next i
        If strValue <> "" Then strValue = Mid(strValue, 2)
        
        strReturned = strReturned & ";" & strValue
        
        rsData.Delete
        rsData.MoveNext
    Loop
    If strReturned <> "" Then strReturned = Mid(strReturned, 2)
    rsData.Filter = adFilterNone
    
ErrHandle:
    HandleRecordDeletion = strReturned
End Function

Public Sub HandleBroadCast(ReqId As String, DestName As String, RecvName As String, CedaCmd As String, ObjName As String, Seq As String, Tws As String, Twe As String, CedaSqlKey As String, Fields As String, Data As String, BcNum As String)
    Dim TableNameList As String
    
    If pstrBasicData = "NONE" Then
        TableNameList = pstrDataSource
    Else
        TableNameList = pstrDataSource & "," & pstrBasicData
    End If
    Select Case CedaCmd
        Case "CLO"
            MsgBox "The server had switched." & vbCrLf & "You can restart the application in one minute."
            Exit Sub
        Case "CFL"
        Case "IFR", "UFR", "DFR", "IRT", "URT", "DRT"
            If pblnHandleBc And InStr(TableNameList, ObjName) > 0 Then
                Call EvaluateBc(CedaCmd, ObjName, CedaSqlKey, Fields, Data)
            End If
        Case "SBC"
            If pblnHandleBc And InStr(TableNameList, ObjName) > 0 And ObjName = "DCFTAB" Then
                Call EvaluateBc(CedaCmd, ObjName, CedaSqlKey, Fields, Data)
            End If
    End Select
End Sub

Private Sub LoadBasicData(ByVal AppType As String)
    Dim vntDataTables As Variant
    Dim vntDataTable As Variant
    Dim strDataDesc As String
    
    pstrBasicData = GetConfigEntry(colConfigs, AppType, "BASIC_DATA", "NONE")
    
    If pstrBasicData <> "NONE" Then
    
        vntDataTables = Split(pstrBasicData, ",")
        
        For Each vntDataTable In vntDataTables
            strDataDesc = GetConfigEntry(colConfigs, "DATA_SOURCES", vntDataTable & "_DESC", "")
            
            Call AddDataSource(AppType, colDataSources, vntDataTable, colParameters)
            
        Next vntDataTable
    End If
    
    Dim blnShowSplashScreen As Boolean
    
    blnShowSplashScreen = (GetConfigEntry(colConfigs, AppType, "SHOW_SPLASH_SCREEN", "NO") = "YES")
    pstrBasicData = GetConfigEntry(colConfigs, AppType, "BASIC_DATA", "NONE")
End Sub

Private Sub PrepareEnv(ByVal AppType As String, ByVal StandAlone As Boolean)
    Dim strApplCommands As String
    Dim strAppName As String
    Dim strADID As String
    Dim FormToShow As Form
    Dim rsAft As ADODB.Recordset
    
    Set colConfigs = ReadConfigurations(myIniFullName)
                
    If InStr(GetConfigEntry(colConfigs, "MAIN", "VALID_COMMAND", "") & ",", AppType & ",") <= 0 Then
        AppType = "MAIN"
    End If
    
        
    If Not (rsAft Is Nothing) Then
        If Not (rsAft.BOF And rsAft.EOF) Then
            If rsAft.RecordCount = 1 Then
                strADID = RTrim(rsAft.Fields("ADID").Value)
            End If
        End If
    End If
    
    If AppType = "MAIN" Then
        strApplCommands = GetConfigEntry(colConfigs, "MAIN", "DEFAULT_COMMAND", "")
        If InStr(strApplCommands, ",") = 0 Then
            AppType = strApplCommands
        End If
    Else
        strApplCommands = AppType
    End If
    If strApplCommands = "" Then
        MsgBoxEx "Unknown command!", vbInformation, AppType, , , eCentreDialog
        Exit Sub
    End If
        
    'Call EvaluateCommands(strApplCommands, strADID)
    
    strAppName = GetConfigEntry(colConfigs, AppType, "APP_NAME", "CDID")
    colParameters.Add "'" & strAppName & "'", "AppName"
               
    Call LoadBasicData(AppType)
        
    'get the time diff in winter and summer
    If colDataSources.Exists("APTTAB") Then
        Set colUTCTimeDiff = AptUTCTimeDiff(colDataSources.Item("APTTAB"), oUfisServer.HOPO, UtcTimeDiff)
    Else
        Set colUTCTimeDiff = AptUTCTimeDiff(Nothing, oUfisServer.HOPO, UtcTimeDiff)
    End If
        
    'read settings
    pblnIsStandAlone = StandAlone
    
    '*****IsUTCTimeZone = (GetConfigEntry(colConfigs, AppType, "DEFAULT_TIME_ZONE", "LOCAL") = "UTC")
    
    '*****pblnHandleBc = (GetConfigEntry(colConfigs, AppType, "HANDLE_BC", "YES") = "YES")
    pblnShowFlightList = (GetConfigEntry(colConfigs, AppType, "SHOW_FLIGHT_LIST", "YES") = "YES")
    pblnShowFlightFilterOnStartup = (GetConfigEntry(colConfigs, AppType, "SHOW_FLIGHT_FILTER_ON_STARTUP", "YES") = "YES")
    pblnShowFlightDetails = (GetConfigEntry(colConfigs, AppType, "SHOW_FLIGHT_DETAILS", "YES") = "YES")
    pblnShowRightButtons = (GetConfigEntry(colConfigs, AppType, "SHOW_RIGHT_BUTTONS", "YES") = "YES")
    
    pblnFlightListMainButtons = (GetConfigEntry(colConfigs, "FLIGHT_LIST", "SHOW_MAIN_BUTTONS", "YES") = "YES")
    pblnFlightListWindow = (GetConfigEntry(colConfigs, "FLIGHT_LIST", "FLOATING_WINDOW", "YES") = "YES")
    
    'end of settings
    If pblnHandleBc Then
        oUfisServer.ConnectToBcProxy
    End If
End Sub
