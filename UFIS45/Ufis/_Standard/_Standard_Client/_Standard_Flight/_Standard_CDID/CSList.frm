VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form CSList 
   Caption         =   "Code Share Flights List"
   ClientHeight    =   9285
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12990
   LinkTopic       =   "Form1"
   ScaleHeight     =   9285
   ScaleWidth      =   12990
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picSizer 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000C&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   20
      Left            =   10080
      ScaleHeight     =   15
      ScaleWidth      =   1635
      TabIndex        =   4
      Top             =   1800
      Visible         =   0   'False
      Width           =   1635
   End
   Begin TABLib.TAB CSTAB 
      Height          =   4935
      Left            =   600
      TabIndex        =   0
      Top             =   2640
      Width           =   8895
      _Version        =   65536
      _ExtentX        =   15690
      _ExtentY        =   8705
      _StockProps     =   64
      FontSize        =   12
      FontName        =   "Arial"
   End
   Begin TABLib.TAB MFTAB 
      Height          =   2055
      Left            =   600
      TabIndex        =   1
      Top             =   480
      Width           =   8895
      _Version        =   65536
      _ExtentX        =   15690
      _ExtentY        =   3625
      _StockProps     =   64
      FontSize        =   12
      FontName        =   "Arial"
   End
   Begin VB.PictureBox picSep 
      BorderStyle     =   0  'None
      FillStyle       =   0  'Solid
      Height          =   20
      Left            =   600
      MousePointer    =   7  'Size N S
      ScaleHeight     =   15
      ScaleWidth      =   8895
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   2575
      Width           =   8895
   End
   Begin VB.Label lblMsg 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Message"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   600
      TabIndex        =   2
      Top             =   240
      Width           =   3375
   End
End
Attribute VB_Name = "CSList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public FormType As enmFormType

Private FlightUrno As String

Private rsAft As ADODB.Recordset
Private rsCSD As ADODB.Recordset
Private rsCCA As ADODB.Recordset
Private rsCSC As ADODB.Recordset

Public Sub RefreshDisplay(ByVal CedaCmd As String, ByVal TableName As String, ByVal Urno As String, Optional ByVal ChangedFields As String)
    Dim strADID As String
    Dim blnArr As Boolean
    Dim rsData As ADODB.Recordset
    Dim blnRefreshNeeded As Boolean

    rsAft.Find "URNO = " & FlightUrno, , , 1
    If rsAft.EOF Then Exit Sub
    
    strADID = rsAft.Fields("ADID").Value
    blnArr = (strADID = "A")
    blnRefreshNeeded = False
            
    Select Case TableName
        Case "AFTTAB"
            If Urno = FlightUrno Then
                If ChangedFields <> "" Then
                    If Not IsRefreshNeeded(TableName, blnArr, ChangedFields) Then Exit Sub
                End If
                
                Call FillInData(rsAft, strADID, True, False)
                MFTAB.Refresh
            End If
        Case "CCATAB"
            If CedaCmd = "DRT" Then
                blnRefreshNeeded = (InStr(Urno, FlightUrno) > 0)
            Else
                Urno = rsCCA.Fields("FLNU").Value
                If Urno = FlightUrno Then
                    blnRefreshNeeded = True
                    If CedaCmd = "URT" And ChangedFields <> "" Then
                        blnRefreshNeeded = IsRefreshNeeded(TableName, blnArr, ChangedFields)
                    End If
                End If
            End If
            If blnRefreshNeeded Then
                Call ClearTabLines(MFTAB)
                Call FillInData(rsAft, strADID, True, True)
                MFTAB.Refresh
            End If
        Case "CSDTAB", "CSCTAB"
            If CedaCmd = "DRT" Then
                blnRefreshNeeded = (InStr(Urno, FlightUrno & ",") > 0)
            Else
                If TableName = "CSDTAB" Then
                    Set rsData = rsCSD
                Else
                    Set rsData = rsCSC
                End If
                Urno = rsData.Fields("RURN").Value
                If Urno = FlightUrno Then
                    blnRefreshNeeded = True
                    If CedaCmd = "URT" And ChangedFields <> "" Then
                        blnRefreshNeeded = IsRefreshNeeded(TableName, blnArr, ChangedFields)
                    End If
                End If
            End If
            If blnRefreshNeeded Then
                Call ClearTabLines(CSTAB)
                Call FillInData(rsAft, strADID, False, True)
                CSTAB.Refresh
            End If
    End Select
End Sub

Private Function IsRefreshNeeded(ByVal TableName As String, ByVal IsArrival As Boolean, ByVal ChangedFields As String) As Boolean
    Const AFT_ARR_FIELD_LIST = "FLNO,VIAL,VIAN,ORG3,GTA1,GTA2," & _
        "BLT1,BLT2,B1BS,B1ES,B1BA,B1EA,B2BS,B2ES,B2BA,B2EA"
    Const AFT_DEP_FIELD_LIST = "FLNO,VIAL,VIAN,DES3,GTD1,GTD2"
    Const CCA_FIELD_LIST = "CKIC,CKIT,CKBS,CKES,CKBA,CKEA"
    Const CSD_ARR_FIELD_LIST = "JFNO,VIAL,ORG3,GTA1,GTA2," & _
        "BLT1,BLT2,B1BS,B1ES,B1BA,B1EA,B2BS,B2ES,B2BA,B2EA"
    Const CSD_DEP_FIELD_LIST = "VIAL,DES3,GTD1,GTD2"
    Const CSC_FIELD_LIST = "CKIC,CKTR,CKBS,CKES,CKBA,CKEA"
    
    Dim strFields As String
    
    Select Case TableName
        Case "AFTTAB"
            If IsArrival Then
                strFields = AFT_ARR_FIELD_LIST
            Else
                strFields = AFT_DEP_FIELD_LIST
            End If
        Case "CCATAB"
            strFields = CCA_FIELD_LIST
        Case "CSDTAB"
            If IsArrival Then
                strFields = CSD_ARR_FIELD_LIST
            Else
                strFields = CSD_DEP_FIELD_LIST
            End If
        Case "CSCTAB"
            strFields = CSC_FIELD_LIST
    End Select
    
    IsRefreshNeeded = IsInList(ChangedFields, strFields)
End Function

Public Sub LoadData(ByVal strUrno As String)
    Dim blnFLightFound As Boolean
    
    blnFLightFound = (strUrno <> "")
    If blnFLightFound Then
        rsAft.Find "URNO = " & strUrno, , , 1
        blnFLightFound = Not (rsAft.EOF)
    End If
    
    If blnFLightFound Then
        FlightUrno = strUrno
        
        Call InitCodeShareAllTab(rsAft)
        lblMsg.Caption = ""
    Else
        FlightUrno = ""
        
        Call InitCodeShareAllTab(Nothing)
        lblMsg.Caption = "Flight not found!"
    End If
End Sub

Private Sub FillInData(rsData As ADODB.Recordset, ByVal sAdid As String, ByVal MainFlight As Boolean, Optional ByVal NewFlight As Boolean = True)
    Dim dtmBaseDate As Date
    Dim strLine As String
    Dim strFlno As String
    Dim strLeg As String
    Dim strBelt As String
    Dim strGate1 As String
    Dim strGate2 As String
    Dim dtmDate As Date
    Dim strDate As String
    Dim blnNotEmptyLine As Boolean
    Dim strBeltInfo(3) As Variant
    Dim strCICInfo(3) As Variant
    Dim strCKICs(5) As Variant
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    
    If sAdid = "A" Then
        dtmBaseDate = DateTimeToDate(UTCDateToLocal(rsData.Fields("STOA").Value, UtcTimeDiff))
    
        strBeltInfo(0) = "B{n}BS"
        strBeltInfo(1) = "B{n}ES"
        strBeltInfo(2) = "B{n}BA"
        strBeltInfo(3) = "B{n}EA"
    
        'Main Flight
        If MainFlight Then
            For j = 1 To 2
                If j = 1 Then
                    blnNotEmptyLine = True
                    strFlno = RTrim(rsData.Fields("FLNO").Value)
                    strLeg = GetViaListString(rsData.Fields("VIAL").Value, _
                        NVL(rsData.Fields("VIAN").Value, 0))
                    strLeg = GetCSLeg(strLeg, rsData.Fields("ORG3").Value, sAdid)
                    strGate1 = RTrim(rsData.Fields("GTA1").Value)
                    strGate2 = RTrim(rsData.Fields("GTA2").Value)
                Else
                    blnNotEmptyLine = False
                    strFlno = ""
                    strLeg = ""
                    strGate1 = ""
                    strGate2 = ""
                End If
                strLine = strFlno & "," & strLeg & "," & strGate1 & "," & strGate2
                
                strBelt = RTrim(rsData.Fields("BLT" & CStr(j)).Value)
                strLine = strLine & "," & strBelt
                blnNotEmptyLine = blnNotEmptyLine Or (strBelt <> "")
                
                For k = 0 To UBound(strBeltInfo)
                    If IsNull(rsData.Fields(Replace(strBeltInfo(k), "{n}", CStr(j))).Value) Then
                        strDate = ""
                    Else
                        dtmDate = rsData.Fields(Replace(strBeltInfo(k), "{n}", CStr(j))).Value
                        dtmDate = UTCDateToLocal(dtmDate, UtcTimeDiff)
                        strDate = FullDateTimeToShortTime(dtmBaseDate, dtmDate)
                    End If
                    strLine = strLine & "," & strDate
                    
                    blnNotEmptyLine = blnNotEmptyLine Or (strDate <> "")
                Next k
                If NewFlight Then
                    If blnNotEmptyLine Then
                        MFTAB.InsertTextLine strLine, False
                    End If
                Else
                    If blnNotEmptyLine Then
                        If MFTAB.GetLineCount < j Then
                            MFTAB.InsertTextLine strLine, False
                        Else
                            MFTAB.SetFieldValues j - 1, MFTAB.LogicalFieldList, strLine
                        End If
                    Else
                        If MFTAB.GetLineCount >= j Then
                            MFTAB.DeleteLine j - 1
                        End If
                    End If
                End If
            Next j
        Else
            'Code Share Flight
            CSTAB.CreateDecorationObject "BottomBorder", "B", "1", CStr(vbBlack)
            
            rsCSD.Find "RURN = " & FlightUrno, , , 1
            Do While Not rsCSD.EOF
                If rsCSD.Fields("RURN").Value <> FlightUrno Then Exit Do
                
                For j = 1 To 2
                    If j = 1 Then
                        blnNotEmptyLine = True
                        strFlno = RTrim(rsCSD.Fields("JFNO").Value)
                        strLeg = GetViaListString(rsCSD.Fields("VIAL").Value)
                        strLeg = GetCSLeg(strLeg, rsCSD.Fields("ORG3").Value, sAdid)
                        strGate1 = RTrim(rsCSD.Fields("GTA1").Value)
                        strGate2 = RTrim(rsCSD.Fields("GTA2").Value)
                    Else
                        blnNotEmptyLine = False
                        strFlno = ""
                        strLeg = ""
                        strGate1 = ""
                        strGate2 = ""
                    End If
                    strLine = strFlno & "," & strLeg & "," & strGate1 & "," & strGate2
                    
                    strBelt = RTrim(rsCSD.Fields("BLT" & CStr(j)).Value)
                    strLine = strLine & "," & strBelt
                    blnNotEmptyLine = blnNotEmptyLine Or (strBelt <> "")
                
                    For k = 0 To UBound(strBeltInfo)
                        If IsNull(rsCSD.Fields(Replace(strBeltInfo(k), "{n}", CStr(j))).Value) Then
                            strDate = ""
                        Else
                            dtmDate = rsCSD.Fields(Replace(strBeltInfo(k), "{n}", CStr(j))).Value
                            dtmDate = UTCDateToLocal(dtmDate, UtcTimeDiff)
                            strDate = FullDateTimeToShortTime(dtmBaseDate, dtmDate)
                        End If
                        strLine = strLine & "," & strDate
                        
                        blnNotEmptyLine = blnNotEmptyLine Or (strDate <> "")
                    Next k
                    If blnNotEmptyLine Then
                        CSTAB.InsertTextLine strLine, False
                    End If
                Next j
                
                For i = 0 To CSTAB.GetColumnCount - 1
                    CSTAB.SetDecorationObject CSTAB.GetLineCount - 1, i, "BottomBorder"
                Next i
                
                rsCSD.MoveNext
            Loop
        End If
    Else
        dtmBaseDate = DateTimeToDate(UTCDateToLocal(rsData.Fields("STOD").Value, UtcTimeDiff))
        
        strCICInfo(0) = "CKBS"
        strCICInfo(1) = "CKES"
        strCICInfo(2) = "CKBA"
        strCICInfo(3) = "CKEA"
    
        If MainFlight Then
            'Main Flight
            strFlno = RTrim(rsData.Fields("FLNO").Value)
            strLeg = GetViaListString(rsData.Fields("VIAL").Value, _
                NVL(rsData.Fields("VIAN").Value, 0))
            strLeg = GetCSLeg(strLeg, rsData.Fields("DES3").Value, sAdid)
            strGate1 = RTrim(rsData.Fields("GTD1").Value)
            strGate2 = RTrim(rsData.Fields("GTD2").Value)
            
            If NewFlight Then
                rsCCA.Find "FLNU = " & FlightUrno, , , 1
                If rsCCA.EOF Then
                    strLine = strFlno & "," & strLeg & "," & strGate1 & "," & strGate2
                    For j = 0 To UBound(strCKICs)
                        strLine = strLine & ","
                    Next j
                    MFTAB.InsertTextLine strLine, False
                Else
                    Do While Not rsCCA.EOF
                        If rsCCA.Fields("FLNU").Value <> FlightUrno Then Exit Do
                        
                        strCKICs(0) = RTrim(rsCCA.Fields("CKIC").Value)
                        strCKICs(1) = RTrim(rsCCA.Fields("CKIT").Value)
                        For j = 0 To UBound(strCICInfo)
                            If IsNull(rsCCA.Fields(strCICInfo(j)).Value) Then
                                strDate = ""
                            Else
                                dtmDate = rsCCA.Fields(strCICInfo(j)).Value
                                dtmDate = UTCDateToLocal(dtmDate, UtcTimeDiff)
                                strDate = FullDateTimeToShortTime(dtmBaseDate, dtmDate)
                            End If
                            strCKICs(j + 2) = strDate
                        Next j
                        
                        strLine = strFlno & "," & strLeg & "," & strGate1 & "," & strGate2
                        For j = 0 To UBound(strCKICs)
                            strLine = strLine & "," & strCKICs(j)
                        Next j
                        MFTAB.InsertTextLine strLine, False
                        
                        strFlno = ""
                        strLeg = ""
                        strGate1 = ""
                        strGate2 = ""
                        
                        rsCCA.MoveNext
                    Loop
                End If
            Else
                strLine = strFlno & "," & strLeg & "," & strGate1 & "," & strGate2
                MFTAB.SetFieldValues 0, "JFNO,VIAL,GTD1,GTD2", strLine
            End If
        Else
            'Code Share Flight
            CSTAB.CreateDecorationObject "BottomBorder", "B", "1", CStr(vbBlack)
            
            rsCSD.Find "RURN = " & FlightUrno, , , 1
            Do While Not rsCSD.EOF
                If rsCSD.Fields("RURN").Value <> FlightUrno Then Exit Do
                
                strFlno = RTrim(rsCSD.Fields("JFNO").Value)
                strLeg = GetViaListString(rsCSD.Fields("VIAL").Value)
                strLeg = GetCSLeg(strLeg, rsCSD.Fields("DES3").Value, sAdid)
                strGate1 = RTrim(rsCSD.Fields("GTD1").Value)
                strGate2 = RTrim(rsCSD.Fields("GTD2").Value)
                
                rsCSC.Filter = "RURN = " & FlightUrno & " AND JFNO = '" & strFlno & "'"
                If Not rsCSC.EOF Then
                    Do While Not rsCSC.EOF
                        strCKICs(0) = RTrim(rsCSC.Fields("CKIC").Value)
                        strCKICs(1) = RTrim(rsCSC.Fields("CKTR").Value)
                        For j = 0 To UBound(strCICInfo)
                            If IsNull(rsCSC.Fields(strCICInfo(j)).Value) Then
                                strDate = ""
                            Else
                                dtmDate = rsCSC.Fields(strCICInfo(j)).Value
                                dtmDate = UTCDateToLocal(dtmDate, UtcTimeDiff)
                                strDate = FullDateTimeToShortTime(dtmBaseDate, dtmDate)
                            End If
                            strCKICs(j + 2) = strDate
                        Next j
                        
                        strLine = strFlno & "," & strLeg & "," & strGate1 & "," & strGate2
                        For j = 0 To UBound(strCKICs)
                            strLine = strLine & "," & strCKICs(j)
                        Next j
                        CSTAB.InsertTextLine strLine, False
                                                
                        strFlno = ""
                        strLeg = ""
                        strGate1 = ""
                        strGate2 = ""
                    
                        rsCSC.MoveNext
                    Loop
                Else
                    strCKICs(0) = ""
                    strCKICs(1) = ""
                    strCKICs(2) = ""
                    strCKICs(3) = ""
                    strCKICs(4) = ""
                    strCKICs(5) = ""
                    strLine = strFlno & "," & strLeg & "," & strGate1 & "," & strGate2
                    For j = 0 To UBound(strCKICs)
                        strLine = strLine & "," & strCKICs(j)
                    Next j
                    CSTAB.InsertTextLine strLine, False
                End If
                rsCSC.Filter = adFilterNone
                
                For i = 0 To CSTAB.GetColumnCount - 1
                    CSTAB.SetDecorationObject CSTAB.GetLineCount - 1, i, "BottomBorder"
                Next i
                
                rsCSD.MoveNext
            Loop
        End If
    End If
    
    MFTAB.ShowVertScroller (MFTAB.GetLineCount > 3)
End Sub

Private Sub InitCodeShareAllTab(rsData As ADODB.Recordset)
    Const FLD_LST = "JFNO,VIAL"
    Const HDR_STR = "Flight No,Routing"
    Const HDR_LNG = "250,400"
    Const HDR_ALG = "C,C"
    
    Dim DisplayTab(1) As TABLib.Tab
    Dim strFlightTitles(1) As String
    Dim i As Integer
    Dim strADID As String
    
    Set DisplayTab(0) = MFTAB
    Set DisplayTab(1) = CSTAB
    
    strFlightTitles(0) = "Main Flight"
    strFlightTitles(1) = "CS Flight"
    
    For i = 0 To UBound(DisplayTab)
        With DisplayTab(i)
            .ResetContent
            If rsData Is Nothing Then
                .LogicalFieldList = FLD_LST
                .HeaderString = HDR_STR
                .HeaderLengthString = HDR_LNG
                .HeaderAlignmentString = HDR_ALG
                .SetMainHeaderValues "2", strFlightTitles(i), ""
            Else
                strADID = RTrim(rsData.Fields("ADID").Value)
                If strADID = "A" Then
                    .LogicalFieldList = FLD_LST & ",GTA1,GTA2,BLT1,B1BS,B1ES,B1BA,B1EA,BLT2,B2BS,B2ES,B2BA,B2EA"
                    .HeaderString = HDR_STR & ",Gt1,Gt2," & _
                        "No,Pln Opn,Pln Cls,Act Opn,Act Cls"
                    .HeaderLengthString = HDR_LNG & Repeat(7, ",200")
                    .HeaderAlignmentString = HDR_ALG & Repeat(7, ",C")
                    .SetMainHeaderValues "2,2,5", strFlightTitles(i) & ",Gate,Belt", ""
                Else
                    .LogicalFieldList = FLD_LST & ",GTD1,GTD2,CKIC,CKIT,CKBS,CKES,CKBA,CKEA"
                    .HeaderString = HDR_STR & ",Gt1,Gt2," & _
                        "No,T,Pln Opn,Pln Cls,Act Opn,Act Cls"
                    .HeaderLengthString = HDR_LNG & Repeat(8, ",200")
                    .HeaderAlignmentString = HDR_ALG & Repeat(8, ",C")
                    .SetMainHeaderValues "2,2,6", strFlightTitles(i) & ",Gates,Check-In Counters", ""
                End If
                .ShowHorzScroller True
                .ShowVertScroller True
                
                Call FillInData(rsData, strADID, (i = 0))
            End If
            .MainHeader = True
            .AutoSizeByHeader = True
            .AutoSizeColumns
            
            .Refresh
        End With
    Next i
End Sub

Private Sub Form_Load()
    If colDataSources.Exists("AFTTAB") Then
        Set rsAft = colDataSources.Item("AFTTAB")
    End If
    If colDataSources.Exists("CSDTAB") Then
        Set rsCSD = colDataSources.Item("CSDTAB")
    End If
    If colDataSources.Exists("CCATAB") Then
        Set rsCCA = colDataSources.Item("CCATAB")
    End If
    If colDataSources.Exists("CSCTAB") Then
        Set rsCSC = colDataSources.Item("CSCTAB")
    End If
End Sub

Private Sub picSep_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbLeftButton Then
        picSizer.Move picSep.Left, picSep.Top, picSep.Width, picSep.Height
        picSizer.Visible = True
    End If
End Sub

Private Sub picSep_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Const MIN_HEIGHT = 1000
    Dim MF_Min_Height As Single
    Dim CS_Min_Height As Single
    Dim nTop As Single
    
    If Button = vbLeftButton Then
        MF_Min_Height = MFTAB.Top + MIN_HEIGHT
        CS_Min_Height = CSTAB.Top + CSTAB.Height - MIN_HEIGHT
    
        nTop = picSep.Top + y
        If nTop < MF_Min_Height Then nTop = MF_Min_Height
        If nTop > CS_Min_Height Then nTop = CS_Min_Height
        picSizer.Top = nTop
    End If
End Sub

Private Sub picSep_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim sngDiff As Single
    
    If Button = vbLeftButton Then
        picSizer.Visible = False
        
        picSep.Top = picSizer.Top
         
        sngDiff = MFTAB.Top + MFTAB.Height - (picSizer.Top - 40)
        MFTAB.Height = MFTAB.Height - sngDiff
        CSTAB.Top = CSTAB.Top - sngDiff
        CSTAB.Height = CSTAB.Height + sngDiff
    End If
End Sub
