Attribute VB_Name = "StartModule"
Option Explicit

Public LoginUserName As String
Public LoginUserPwd As String
Public IsTimeInLocal As Boolean

Sub Main()
    Dim strCommandLine As String
    
    Dim vntCommand As Variant
    Dim vntCommand1 As Variant
    Dim vntCommand2 As Variant
    Dim vntCommand3 As Variant
    
    Dim sAppType As String
    Dim sFlightUrno As String
    Dim bStandAlone As Boolean
    
    Dim blnLoginSucceeded As Boolean
    
    Dim strBcLogFilename As String
    Dim intDotLoc As Integer
    
    'Dim ll_WindowHandle As Long
    
    If Not DetermineIniFile Then
        
        MsgBox "Sorry. Couldn't find the INI file!" & vbNewLine & myIniFullName
        
    Else
        
        'Read config
        Set colConfigs = ReadConfigurations(myIniFullName)
        
        strCommandLine = Command
        If Trim(strCommandLine) = "" Then 'need a login
            blnLoginSucceeded = DoLogin(LoginUserName, LoginUserPwd)
            
            If blnLoginSucceeded Then
                sAppType = GetConfigEntry(colConfigs, "MAIN", "DEFAULT_COMMAND", "FUPD,FCSL")
                sFlightUrno = ""
                bStandAlone = True
            End If
        Else
            vntCommand = Split(strCommandLine, "|")
            vntCommand1 = Split(vntCommand(0), ",")
            vntCommand2 = Split(vntCommand(1), ",")
            vntCommand3 = Split(vntCommand(2), ",")
            
            LoginUserName = vntCommand2(0)
            
            'Check if password was passed
            If UBound(vntCommand) > 2 Then
                LoginUserPwd = vntCommand(3)
            Else
                LoginUserPwd = ""
            End If
            
            blnLoginSucceeded = DoLogin(LoginUserName, LoginUserPwd)
            
            If blnLoginSucceeded Then
                sAppType = vntCommand1(0)
                sFlightUrno = vntCommand1(1) & "," & vntCommand1(2)
                Select Case sAppType
                    Case "DE-ICING"
                        bStandAlone = True
                    Case Else
                        bStandAlone = False
                End Select
                
                App.Title = sAppType
                
                If App.PrevInstance Then
    '                ll_WindowHandle = CheckAppPrevInstance()
    '                If ll_WindowHandle <> 0 Then
    '                    'Find the window we need to restore
    '                    ll_WindowHandle = GetWindow(ll_WindowHandle, GW_HWNDPREV)
    '
    '                    'Now restore it
    '                    Call OpenIcon(ll_WindowHandle)
    '                    'And Bring it to the foreground
    '                    Call SetForegroundWindow(ll_WindowHandle)
    '
    '                    Exit Sub
    '                End If
                    If CheckAppPrevInstance() Then Exit Sub
                End If
            End If
        End If
        
        If blnLoginSucceeded Then
            Set oUfisServer = New UfisServer
            oUfisServer.StayConnected = True
            oUfisServer.TwsCode = ".."
            oUfisServer.ConnectToCeda
            oUfisServer.UserName = LoginUserName
            
            colParameters.Add "'" & LoginUserName & "'", "LoginUserName"
            colParameters.Add LoginUserName, "UserName"
            colParameters.Add LoginUserPwd, "UserPassword"
            
            'get the default time diff
            'igu on 12/01/2012
            'check whether should display time in local or UTC
            
            'Zaw Min Tun
            '2012-11-26
            'V 4.6.17
            'JIRA UFIS-2496
            'To check for crash safety in case vntCommand2 is null or empty
            '<
            'IsTimeInLocal = (vntCommand2(2) = "L")
            '>
            '<
            If IsNull(vntCommand2) Or IsEmpty(vntCommand2) Then
               IsTimeInLocal = False
            Else
                IsTimeInLocal = (vntCommand2(2) = "L")
            End If
            '>
            
            If IsTimeInLocal Then
                Call GetUTCLocalTimeDiff(myIniFullName)
            End If
            
            Set MyMainForm = MainDialog
            MyMainForm.AppType = sAppType
            MyMainForm.FlightUrno = sFlightUrno
            MyMainForm.StandAlone = bStandAlone
            MyMainForm.Show
            
            If GetConfigEntry(colConfigs, sAppType, "SHOW_BC_LOG", "NO") = "YES" Then
                Set BcLogForm = New TextDisplay
                BcLogForm.Caption = "Broadcast Log"
                BcLogForm.Show
            End If
            
            strBcLogFilename = GetConfigEntry(colConfigs, sAppType, "BC_LOG_FILENAME", "")
            If strBcLogFilename <> "" Then
                intDotLoc = InStrRev(strBcLogFilename, ".")
                If intDotLoc > 0 Then
                    strBcLogFilename = Left(strBcLogFilename, intDotLoc - 1) & _
                        "_" & Format(Now, "yyyyMMddHHmmss") & _
                        Mid(strBcLogFilename, intDotLoc)
                Else
                    strBcLogFilename = strBcLogFilename & "_" & Format(Now, "yyyyMMddHHmmss")
                End If
                Call OpenLogFile(strBcLogFilename)
            End If
        End If
    End If
End Sub

Private Sub OpenLogFile(ByVal LogFileName As String)
    On Error GoTo ErrOpen

    pintBcLogFileHandle = FreeFile
    
    Open LogFileName For Append As #pintBcLogFileHandle
    
    Exit Sub
    
ErrOpen:
    pintBcLogFileHandle = 0
    
    MsgBox "Failed to open BC Log File!" & vbNewLine & _
        "BC won't be logged into file.", vbInformation, "Error Open File"
End Sub

Private Function DetermineIniFile() As Boolean
    Dim tmpStr As String
    Dim IniFound As Boolean
    
    If DEFAULT_CEDA_INI = "" Then GetUfisDir
    myIniPath = UFIS_SYSTEM
    
    myIniFile = App.EXEName & ".ini"
    myIniFullName = myIniPath & "\" & myIniFile
        
    tmpStr = Dir(myIniFullName)
    IniFound = (UCase(tmpStr) = UCase(myIniFile))
    
    DetermineIniFile = IniFound
End Function

Private Function DoLogin(UserName As String, UserPwd As String) As Boolean
    Dim oLogin As Object
    Dim oUfisCom As Object
    
    Dim AppFullVersion As String
    Dim AppMajorVersion As String
    Dim strRegister As String
    Dim strRetLogin As String
    
    AppMajorVersion = App.Major & "." & App.Minor
    AppFullVersion = AppMajorVersion & ".0." & App.Revision
    
    ' build the register string for BDPS-SEC
    strRegister = App.EXEName & "," & _
        "InitModu,InitModu,Initialize (InitModu),B,-"
    
    Set oLogin = CreateObject("AATLOGIN.AatLoginCtrl.1")
    Set oUfisCom = CreateObject("UFISCOM.UfisComCtrl.1")
    
    With oLogin
        Set oLogin.UfisComCtrl = oUfisCom
        
        .ApplicationName = App.EXEName
        .VersionString = AppFullVersion
        .InfoCaption = "Info about this application"
        .InfoButtonVisible = True
        .InfoUfisVersion = "UFIS Version " & AppMajorVersion
        .InfoAppVersion = App.Title & " " & AppFullVersion
        .InfoCopyright = "� 2001-2005 UFIS Airport Solutions GmbH"
        .InfoAAT = "UFIS Airport Solutions GmbH"
        .UserNameLCase = True 'automatic upper case letters
        .LoginAttempts = 3
        .RegisterApplicationString = strRegister
        
        If UserName = "" Or UserPwd = "" Then
            strRetLogin = .ShowLoginDialog
        Else
            strRetLogin = .DoLoginSilentMode(UserName, UserPwd)
            'look after the login result
            If strRetLogin = "CANCEL" Or strRetLogin = "ERROR" Then
                strRetLogin = .ShowLoginDialog
            End If
        End If
        
        ' close connection to server
        oUfisCom.CleanupCom
        
        'look after the login result
        If strRetLogin = "CANCEL" Or strRetLogin = "ERROR" Then
            UserName = ""
            UserPwd = ""
        Else
            UserName = .GetUserName
            UserPwd = .GetUserPassword
        End If
    End With
    
    Set oUfisCom = Nothing
    Set oLogin = Nothing
    
    DoLogin = (UserName <> "")
End Function

Private Function GetChangedFields(ByVal Fields As String) As String
    Dim intLoc As Integer
    
    intLoc = InStr(Fields, "#")
    If intLoc > 0 Then
        GetChangedFields = Replace(Mid(Fields, intLoc), "#", "")
    End If
End Function

Private Function CreateVirtualTable(ByVal TableOperation As TableOperationType, ByVal Fields As String, _
ByVal Data As String, Optional ByVal ChangedFields As String, Optional ByVal CedaSqlKey As String) As ADODB.Recordset
    Const MAX_VARCHAR_LENGTH = 8184

    Dim rsData As ADODB.Recordset
    Dim aFields As Variant
    Dim aData As Variant
    Dim i As Integer
    Dim colUpdateFieldsIndex As New Collection
    Dim strChangedFields As String
    Dim strField As String
    
    If TableOperation = toEdit Then
        If InStr(ChangedFields, "URNO") <= 0 Then
            If InStr(Fields, "URNO") > 0 Then
                ChangedFields = "URNO," & ChangedFields
            Else
                CedaSqlKey = Replace(CedaSqlKey, " ", "")
                i = InStr(CedaSqlKey, "URNO=")
                If i > 0 Then
                    CedaSqlKey = Mid(CedaSqlKey, i + Len("URNO="))
                    
                    i = InStr(CedaSqlKey, vbLf)
                    If i > 0 Then
                        CedaSqlKey = Left(CedaSqlKey, i - 1)
                    End If
                    
                    i = InStr(CedaSqlKey, "AND")
                    If i > 0 Then
                        CedaSqlKey = Left(CedaSqlKey, i - 1)
                    End If
                    
                    If Left(CedaSqlKey, 1) = "'" And Right(CedaSqlKey, 1) = "'" Then
                        CedaSqlKey = Mid(CedaSqlKey, 2)
                        CedaSqlKey = Left(CedaSqlKey, Len(CedaSqlKey) - 1)
                    End If
                    
                    Fields = "URNO," & Fields
                    Data = CedaSqlKey & "," & Data
                    
                    ChangedFields = "URNO," & ChangedFields
                End If
            End If
        End If
        strChangedFields = ChangedFields
    End If
    
    aFields = Split(Fields, ",")
    aData = Split(Data, ",")
    
    For i = 0 To UBound(aFields)
        strField = aFields(i)
        If TableOperation = toInsert Then
            colUpdateFieldsIndex.Add i
        ElseIf TableOperation = toEdit Then
            If InStr(strChangedFields, strField) > 0 Then
                colUpdateFieldsIndex.Add i
            End If
        End If
    Next i
    
    Set rsData = New ADODB.Recordset
    For i = 1 To colUpdateFieldsIndex.Count
        strField = aFields(colUpdateFieldsIndex.Item(i))
        rsData.Fields.Append strField, adVarChar, MAX_VARCHAR_LENGTH, adFldIsNullable
    Next i
    rsData.Open
    
    rsData.AddNew
    For i = 1 To colUpdateFieldsIndex.Count
        rsData.Fields(i - 1).Value = aData(colUpdateFieldsIndex.Item(i))
    Next i
    rsData.Update
    
    Set CreateVirtualTable = rsData
End Function

Public Sub UpdateMemoryTable(rsData As ADODB.Recordset, rsBc As ADODB.Recordset, ByVal TableOperation As TableOperationType)
    Dim fld As ADODB.Field
    Dim strFields As String
    Dim strValue As String

    If Not (rsBc.BOF And rsBc.EOF) Then
        Select Case TableOperation
            Case toInsert, toEdit
                strFields = ""
                For Each fld In rsBc.Fields
                    strFields = strFields & fld.Name & ","
                Next fld
                
                If TableOperation = toInsert Then
                    rsData.Find "URNO = " & rsBc.Fields("URNO").Value, , , 1
                    If rsData.EOF Then
                        rsData.AddNew
                    End If
                End If
                For Each fld In rsData.Fields
                    If InStr(strFields, fld.Name) > 0 Then
                        strValue = rsBc.Fields(fld.Name).Value
                        
                        Select Case fld.Type
                            Case adVarChar
                                fld.Value = UfisLib.CleanString(strValue, _
                                                SERVER_TO_CLIENT, _
                                                True)
                            Case adInteger, adBigInt, adSingle, adDouble
                                If Trim(strValue) = "" Then
                                    fld.Value = Null
                                Else
                                    fld.Value = Val(strValue)
                                End If
                            Case adDate
                                If Trim(strValue) = "" Then
                                    fld.Value = Null
                                Else
                                    fld.Value = UfisLib.CedaFullDateToVb(strValue)
                                End If
                        End Select
                    Else
                        If TableOperation = toInsert Then
                            Select Case fld.Type
                                Case adVarChar
                                    fld.Value = ""
                                Case adInteger, adBigInt, adSingle, adDouble
                                    fld.Value = 0
                                Case adDate
                                    fld.Value = Null
                            End Select
                        End If
                    End If
                Next fld
                rsData.Update
        End Select
    End If
End Sub

Private Sub EvaluateFlightCommand(ByVal CedaCmd As String, ByVal ObjName As String, ByVal CedaSqlKey As String, ByVal Fields As String, ByVal Data As String)
    Dim vntUrnos As Variant
    Dim vntUrno As Variant
    Dim strWhere As String
    Dim colLocalParams As New CollectionExtended
    
    If pstrDataSource = "" Then
        pstrDataSource = GetConfigEntry(colConfigs, MyMainForm.AppType, "DATA_SOURCE", "AFTTAB")
    End If
    If InStr(pstrDataSource & ",", "AFTTAB,") > 0 Then
        strWhere = BuildWhereClause(CedaCmd, Fields, Data)
        
        If strWhere <> "" Then
            colLocalParams.Add "URNO", "OrderByClause"
            colLocalParams.Add strWhere, "WhereClause"
            
            pstrAftUrnoList = AddDataSource(MyMainForm.AppType, colDataSources, "AFTTAB", colLocalParams, "URNO", , , , False)
            
            vntUrnos = Split(pstrAftUrnoList, ",")
            For Each vntUrno In vntUrnos
                MyMainForm.EvaluateBc "IFR", ObjName, vntUrno, ""
            Next vntUrno
        End If
    End If
End Sub

Private Function BuildWhereClause(ByVal CedaCmd As String, ByVal Fields As String, ByVal Data As String) As String
    Dim strWhere As String
    Dim colFields As New CollectionExtended
    Dim vntFields As Variant
    Dim vntData As Variant
    Dim i As Integer
    Dim vntRow As Variant
    Dim strADID As String
    Dim strFlno As String
    
    vntFields = Split(Fields, ",")
    vntData = Split(Data, ",")
    For i = 0 To UBound(vntFields)
        colFields.Add vntData(i), vntFields(i)
    Next i
    
    Select Case CedaCmd
        Case "ISF"
            '----
            'in some servers, like WAW, FLNOA/FLNOD is not included in broadcast
            If (Not colFields.Exists("FLNOA")) Or (Not colFields.Exists("FLNOD")) Then
                Call oUfisServer.CallCedaDirect("GFR", "AFTTAB", "FLNO,ADID", "", "URNO = " & colFields.Item("SKEY"), "")
                
                For i = 0 To oUfisServer.RowCount - 1
                    If oUfisServer.Row(i) <> "" Then
                        vntRow = Split(oUfisServer.Row(i), ",")
                        strFlno = RTrim(vntRow(0))
                        strADID = RTrim(vntRow(1))
                        
                        If strADID = "A" Or strADID = "D" Then
                            If Not colFields.Exists("FLNO" & strADID) Then
                                colFields.Add strFlno, "FLNO" & strADID
                            End If
                        End If
                    End If
                Next i
            End If
            '----
            
            If colFields.Item("FLNOA") <> "" Then
                strWhere = "((TIFA BETWEEN '" & colFields.Item("TIFA") & "' AND '" & _
                    colFields.Item("TIFD") & "') AND FLNO = '" & colFields.Item("FLNOA") & "')"
            End If
            If colFields.Item("FLNOD") <> "" Then
                If colFields.Item("FLNOA") <> "" Then
                    strWhere = "(" & strWhere & " OR "
                End If
                strWhere = "((TIFD BETWEEN '" & colFields.Item("TIFA") & "' AND '" & _
                    colFields.Item("TIFD") & "') AND FLNO = '" & colFields.Item("FLNOD") & "')"
                If colFields.Item("FLNOA") <> "" Then
                    strWhere = strWhere & ")"
                End If
            End If
            If strWhere <> "" Then
                strWhere = strWhere & " AND (FTYP = '" & colFields.Item("FTYP") & "')"
            End If
    End Select
    
    BuildWhereClause = strWhere
End Function

Private Sub EvaluateBc(ByVal CedaCmd As String, ByVal ObjName As String, ByVal CedaSqlKey As String, ByVal Fields As String, ByVal Data As String)
    Dim rsData As Recordset
    Dim rsBc As ADODB.Recordset
    Dim strChangedFields As String
    Dim strField As String
    
    Set rsData = colDataSources.Item(ObjName)
    
    strField = "URNO"
    Select Case ObjName
        Case "CCATAB"
            strField = "FLNU"
        Case "CSDTAB", "CSCTAB"
            strField = "RURN,JFNO"
    End Select
    
    If Not (rsData Is Nothing) Then
        Select Case CedaCmd
            Case "IRT", "IFR"
                'if delay-code, ignore insertion to AFTTAB
                If MyMainForm.AppType = "FDLY" And ObjName = "AFTTAB" Then
                    'Do nothing
                Else
                    Set rsBc = CreateVirtualTable(toInsert, Fields, Data)
                    Call UpdateMemoryTable(rsData, rsBc, toInsert)
                End If
            Case "ISF"
                'if delay-code, ignore insertion to AFTTAB
                If MyMainForm.AppType = "FDLY" And ObjName = "AFTTAB" Then
                    'Do nothing
                Else
                    Call EvaluateFlightCommand(CedaCmd, ObjName, CedaSqlKey, Fields, Data)
                End If
            Case "URT", "UFR"
                If CedaCmd = "URT" Then
                    strChangedFields = Fields
                Else
                    strChangedFields = GetChangedFields(Fields)
                End If
                
                Set rsBc = CreateVirtualTable(toEdit, Fields, Data, strChangedFields, CedaSqlKey)
                If Not (rsBc.BOF And rsBc.EOF) Then
                    CedaSqlKey = rsBc.Fields("URNO").Value
                    
                    rsData.Find "URNO = " & CedaSqlKey, , , 1
                    If rsData.EOF Then
                        'if delay-code, don not add to rsAft
                        If MyMainForm.AppType = "FDLY" Then
                            'Do nothing
                        Else
                            If CedaCmd = "UFR" Then
                                Call EvaluateBc("IFR", ObjName, CedaSqlKey, Fields, Data)
                                Call LoadDependantData(CedaSqlKey)
                                Exit Sub
                            End If
                        End If
                    Else
                        Call UpdateMemoryTable(rsData, rsBc, toEdit)
                    End If
                End If
            Case "DRT", "DFR"
                CedaSqlKey = HandleRecordDeletion(rsData, ObjName, CedaSqlKey, strField)
            Case "SBC"
                strChangedFields = Data
        End Select
        
        If InStr(pstrDataSource, ObjName) > 0 Then
            If CedaCmd <> "ISF" Then
                MyMainForm.EvaluateBc CedaCmd, ObjName, CedaSqlKey, strChangedFields
            End If
        End If
    End If
End Sub

Private Sub LoadDependantData(ByVal Urno As String)
    Dim vntDataSources As Variant
    Dim vntDataSource As Variant
    Dim colLocalParams As CollectionExtended
    Dim strUrnoList As String
    Dim vntUrnos As Variant
    Dim vntUrno As Variant
    
    Set colLocalParams = New CollectionExtended
    colLocalParams.Add Urno, "AftUrnoList"
    
    vntDataSources = Split(pstrDataSource, ",")
    For Each vntDataSource In vntDataSources
        If vntDataSource <> "AFTTAB" Then
            strUrnoList = AddDataSource(MyMainForm.AppType, colDataSources, vntDataSource, colLocalParams, "URNO", , , , False)
            
            vntUrnos = Split(strUrnoList, ",")
            For Each vntUrno In vntUrnos
                MyMainForm.EvaluateBc "IRT", vntDataSource, vntUrno, ""
            Next vntUrno
        End If
    Next vntDataSource
End Sub

Private Function HandleRecordDeletion(rsData As ADODB.Recordset, ByVal ObjName As String, ByVal CedaSqlKey As String, ByVal ReturnedFields As String) As String
    Dim strFilter As String
    Dim strReturned As String
    Dim aFields As Variant
    Dim i As Integer
    Dim strValue As String
    
    On Error GoTo ErrHandle
    
    aFields = Split(ReturnedFields, ",")
    strFilter = Replace(CedaSqlKey, "WHERE ", "")
    strReturned = ""
    
    rsData.Filter = strFilter
    Do While Not rsData.EOF
        strValue = ""
        For i = 0 To UBound(aFields)
            strValue = strValue & "," & Trim(rsData.Fields(aFields(i)).Value)
        Next i
        If strValue <> "" Then strValue = Mid(strValue, 2)
        
        strReturned = strReturned & ";" & strValue
        
        rsData.Delete
        rsData.MoveNext
    Loop
    If strReturned <> "" Then strReturned = Mid(strReturned, 2)
    rsData.Filter = adFilterNone
    
ErrHandle:
    HandleRecordDeletion = strReturned
End Function

Public Sub HandleBroadCast(ReqId As String, DestName As String, RecvName As String, CedaCmd As String, ObjName As String, Seq As String, Tws As String, Twe As String, CedaSqlKey As String, Fields As String, Data As String, BcNum As String)
    Dim TableNameList As String
    
    Call AddTextToLogForm(BcLogForm, _
        "ReqId : " & ReqId & vbNewLine & _
        "DestName : " & DestName & vbNewLine & _
        "RecvName : " & RecvName & vbNewLine & _
        "CedaCmd : " & CedaCmd & vbNewLine & _
        "ObjName : " & ObjName & vbNewLine & _
        "Seq : " & Seq & vbNewLine & _
        "Tws : " & Tws & vbNewLine & _
        "Twe : " & Twe & vbNewLine & _
        "CedaSqlKey : " & CedaSqlKey & vbNewLine & _
        "Fields : " & Fields & vbNewLine & _
        "Data : " & Data & vbNewLine & _
        "BcNum : " & BcNum & vbNewLine & _
        vbNewLine)
    
    If pstrBasicData = "NONE" Then
        TableNameList = pstrDataSource
    Else
        TableNameList = pstrDataSource & "," & pstrBasicData
    End If
    Select Case CedaCmd
        Case "CLO"
            MsgBox "The server had switched." & vbCrLf & "You can restart the application in one minute."
            End
        Case "CFL"
        Case "ISF", "IFR", "UFR", "DFR", "IRT", "URT", "DRT"
            If pblnHandleBc And InStr(TableNameList, ObjName) > 0 Then
                Call EvaluateBc(CedaCmd, ObjName, CedaSqlKey, Fields, Data)
            End If
        Case "SBC"
            If pblnHandleBc And InStr(TableNameList, ObjName) > 0 And ObjName = "DCFTAB" Then
                Call EvaluateBc(CedaCmd, ObjName, CedaSqlKey, Fields, Data)
            End If
    End Select
End Sub

'Public Function CheckAppPrevInstance() As Long
'    Dim OldTitle As String
'    Dim ll_WindowHandle As Long
'
'    'saving the current title in OldTitle variable
'    'and changing the application title so it will not look for itself
'    OldTitle = App.Title
'
'    App.Title = "Dup instance"
'
'    'finding the previous instance.
'    ll_WindowHandle = FindWindow(vbNullString, OldTitle)
'    'if there is no old instances of your application - exit.
'
'    App.Title = OldTitle
'
'    CheckAppPrevInstance = ll_WindowHandle
'End Function

Public Function CheckAppPrevInstance() As Boolean
    Dim OldTitle As String
    Dim ll_WindowHandle  As Long
    
    'saving the current title in OldTitle variable
    'and changing the application title so it will not look for itself
    OldTitle = App.Title
    
    App.Title = "New instance of " & OldTitle
    
    On Error GoTo ErrHandle
    
    'finding the previous instance.
    ll_WindowHandle = FindWindow(vbNullString, OldTitle)
    'if there is no old instances of your application - exit.
    If ll_WindowHandle = 0 Then
        App.Title = OldTitle
        CheckAppPrevInstance = False
    Else
        AppActivate OldTitle
        CheckAppPrevInstance = True
    End If
    
    Exit Function
    
ErrHandle:
    App.Title = OldTitle
    CheckAppPrevInstance = True
End Function

Public Sub AddTextToLogForm(FormObject As Form, ByVal TextToAdd As String)
    If Not (FormObject Is Nothing) Then
        FormObject.txtText.Text = FormObject.txtText.Text & TextToAdd
    End If
    
    If pintBcLogFileHandle > 0 Then
        Print #pintBcLogFileHandle, TextToAdd
    End If
End Sub
