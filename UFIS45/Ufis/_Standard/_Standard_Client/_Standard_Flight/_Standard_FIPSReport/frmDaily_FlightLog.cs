using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;
using System.Text.RegularExpressions;
using System.Collections.Generic; //igu on 19/01/2011
using FIPS_Reports.Ctrl; //igu on 22/03/2011

namespace FIPS_Reports
{
    /// <summary>
    /// Summary description for frmDaily_FlightLog.
    /// </summary>
    public class frmDaily_FlightLog : System.Windows.Forms.Form
    {
        #region _My Members

        /// <summary>
        /// The where statements: all "@@xx" fields will be replaced
        /// with the true values according to the user's entry.
        /// </summary>
        //private string strWhereRaw = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO')) AND (FTYP<>'S' AND FTYP<>'G' AND FTYP<>'T')" ;
        private string strWhereRaw = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO') OR " +
                                     "(STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO')) AND (FTYP='O' OR FTYP='S' OR FTYP='N' OR FTYP='B' OR FTYP='X')";

        private string strWhereONBLOFBLRaw = "WHERE ((TIFA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO' AND ONBL BETWEEN '@@FROM' AND '@@TO') OR  (TIFD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO' AND OFBL BETWEEN '@@FROM' AND '@@TO'))";

        private string strWhereLANDAIRBRaw = "WHERE ((TIFA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO' AND LAND BETWEEN '@@FROM' AND '@@TO') OR  (TIFD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO' AND AIRB BETWEEN '@@FROM' AND '@@TO'))";
        /// <summary>
        /// The where statement for the dedicated check counters.
        /// </summary>
        private string strDedicatedCCAWhere = "WHERE FLNU IN (@@FLNU) AND CKIC<>' '";
        /// <summary>
        /// The where statement for the delays.
        /// </summary>
        private string strDCFWhere = "WHERE FURN IN (@@FLNU)"; //igu on 18/01/2011
        /// <summary>
        /// The where statement for the dedicated chutes.
        /// </summary>
        private string strDedicatedCHAWhere = "WHERE RURN IN (@@RURN) AND LNAM<>' ' AND DES3 <> '@@@'";
        /// <summary>
        /// Where statement for the LOATAB. @@FLNU will be replaced by the
        /// AFT.URNO list.
        /// </summary>
        private string strWhereLoa = "WHERE FLNU IN (@@FLNU) AND TYPE='PAX' AND STYP='T' AND SSTP=' ' AND SSST=' '";
        /// <summary>
        /// The logical field names of the tab control for internal 
        /// development purposes ==> for Rotation
        /// </summary>
        private string strLogicalFieldsRotationRaw = "Sort_key,Arrival_A,Org_A,Via_A,Na_A,Sta_A,Eta_A,Tmo_A,Land_A,Onbl_A,{0}Pax_T_A,Pos_A,Term_A,Gate_A,Belt_A,Ftp_A,A_C_A,Reg_A,Depart__D,Des_D,Via_D,Na_D,Std_D,Etd_D,Slot_D,Ofbl_D,Airb_D,{1}Pax_T_D,Pos_D,Term_D,Gate_D,CknF_T,ChuteFr_To_D,Rem__D"; //igu on 22/03/2011
        private string strLogicalFieldsRotation; //igu on 18/01/2011
        //private string strLogicalFieldsRotation = "Sort_key,Arrival_A,Org_A,Via_A,Na_A,Sta_A,Eta_A,Tmo_A,Land_A,Onbl_A,Pax_T_A,Pos_A,Term_A,Gate_A,Belt_A,Ftp_A,A_C_A,Reg_A,Depart__D,Des_D,Via_D,Na_D,Std_D,Etd_D,Slot_D,Ofbl_D,Airb_D,Del1_D,Min1_D,Del2_D,Min2_D,Pax_T_D,Pos_D,Term_D,Gate_D,CknF_T,ChuteFr_To_D,Rem__D"; //igu on 18/01/2011
        /// The header columns, which must be used for the report output
        /// for rotation
        /// </summary>
        private string strTabHeaderRotationRaw = "sort,Arrival,Org,Via,Na,STA,ETA,TMO,Land,Onbl,{0}Pax T,Pos,Term,Gate,Belt,Ftp,A/C,Reg,Depart.,Des,Via,Na,STD,ETD,Slot,Ofbl,Airb,{1}Pax T,Pos,Term,Gate,Cki F-T,Cht F-T,Rem."; //igu on 22/03/2011
        private string strTabHeaderRotation; //igu on 18/01/2011
        //private string strTabHeaderRotation = "sort,Arrival,Org,Via,Na,STA,ETA,TMO,Land,Onbl,Pax T,Pos,Term,Gate,Belt,Ftp,A/C,Reg,Depart.,Des,Via,Na,STD,ETD,Slot,Ofbl,Airb,Del1,Min,Del2,Min,Pax T,Pos,Term,Gate,Cki F-T,Cht F-T,Rem."; //igu on 18/01/2011
        /// <summary>
        /// The lengths, which must be used for the report's column widths
        /// for rotation
        /// </summary>
        //private string strTabHeaderLensRotationRaw = "0,45,30,30,28,45,47,47,47,47,0,33,18,33,33,27,25,62,45,30,29,27,45,45,45,45,45,@@Del_Min_Len,0,33,18,33,70,60,55"; //igu on 18/01/2011
        private string strTabHeaderLensRotationRaw = "0,45,30,30,28,45,47,47,47,47,{0}@@PaxT_Len,33,18,33,33,27,25,62,45,30,29,27,45,45,45,45,45,{1}@@PaxT_Len,33,18,33,70,60,55"; //igu on 24/02/2011
        private string strTabHeaderLensRotation;  //igu on 18/01/2011
        //private string strTabHeaderLensRotation = "0,45,30,30,28,45,47,47,47,47,34,33,18,33,33,27,25,62,45,30,29,27,45,45,45,45,45,38,30,34,30,38,33,18,33,70,60,55"; //igu on 18/01/2011
        //private string strTabHeaderLensRotation = "0,54,36,30,28,50,58,58,58,47,34,33,33,33,27,33,62,58,36,29,27,50,55,52,55,56,38,30,34,30,38,33,33,40,40,60";
        /// <summary>
        /// The lengths, which must be used for the report's excel export column widths
        /// for rotation
        /// </summary>
        //private string strExcelTabHeaderLensRotationRaw = "0,54,36,40,28,105,105,105,105,105,0,33,20,33,33,27,33,62,58,36,40,27,105,105,105,105,105,@@Del_Min_Len,0,33,20,33,90,90,350"; //igu on 18/01/2011
        private string strExcelTabHeaderLensRotationRaw = "0,54,36,40,28,105,105,105,105,105,{0}@@PaxT_Len,33,20,33,33,27,33,62,58,36,40,27,105,105,105,105,105,{1}@@PaxT_Len,33,20,33,90,90,350"; //igu on 24/02/2011
        private string strExcelTabHeaderLensRotation; //igu on 18/01/2011
        //private string strExcelTabHeaderLensRotation = "0,54,36,40,28,105,105,105,105,105,34,33,20,33,33,27,33,62,58,36,40,27,105,105,105,105,105,38,30,34,30,38,33,20,33,90,90,350"; //igu on 18/01/2011
        /// <summary>
        /// use Delay Subcode?
        /// </summary>
        private bool bmUseDecs = Ctrl.CtrlDelayedFlight.GetInstance().UseDelayCodeDecs; //igu on 18/01/2011
        /// <summary>
        /// max number of Delays
        /// </summary>
        //private int imMaxOfDelays = 2; //igu on 18/01/2011
        private DelayCount mMaxOfDelays = new DelayCount(); //igu on 23/03/2011
        /// <summary>
        /// FlightDelaysCollection
        /// </summary>
        private FlightDelaysCollection flightDelaysCollection; //igu on 23/03/2011
        /// <summary>
        /// The one and only IDatabase obejct.
        /// </summary>
        private IDatabase myDB;
        /// <summary>
        /// ITable object for the AFTTAB
        /// </summary>
        private ITable myAFT;
        /// <summary>
        /// ITable object for the chutes table CHATABF
        /// </summary>
        private ITable myCHA;
        /// <summary>
        /// ITable object for the checkin counter allocation table CCATAB
        /// </summary>
        private ITable myCCA;
        /// <summary>
        /// ITable object for DCFTAB
        /// </summary>
        private ITable myDCF; //igu on 18/01/2011
        /// <summary>
        /// ITable object for the nature codes of DENTAB
        /// </summary>
        private ITable myDEN; //igu on 03/03/2011
        /// ITable object for the nature codes of NATTAB
        /// </summary>
        private ITable myLOA;
        /// <summary>
        /// Total of arrival flights in the report
        /// </summary>
        private int imArrivals = 0;
        /// <summary>
        /// Total of departure flights in the report
        /// </summary>
        private int imDepartures = 0;
        /// <summary>
        /// Total of rotationein the report
        /// </summary>
        private int imRotations = 0;
        /// <summary>
        /// Total of all flights in the report
        /// </summary>
        private int imTotalFlights = 0;
        /// <summary>
        /// Datetime format for excel export
        /// </summary>
        private string strExcelDateTimeFormat = "";
        /// <summary>
        /// Datetime format for report
        /// </summary>
        private string strReportDateTimeFormat = "dd'/'HH:mm";
        /// <summary>
        /// Datetime format for table
        /// </summary>
        private string strDateDisplayFormat = "";
        /// <summary>
        /// Boolean for mouse status with respect to tabResult control
        /// </summary>
        private bool bmMouseInsideTabControl = false;
        /// <summary>
        /// Report Header
        /// </summary>
        private string strReportHeader = "";
        /// <summary>
        /// Report Sub Header
        /// </summary>
        private string strReportSubHeader = "";
        /// <summary>
        /// Report Object
        /// </summary>
        private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;
        /// <summary>
        /// Boolean to suggest if "Chutes from/to" column should be visible or not
        /// </summary>
        private bool bmChuteDisplay = false;
        /// <summary>
        /// Boolean for showing stev
        /// </summary>
        private bool bmShowStev = false;
        /// <summary>
        /// Boolean for showing PaxT
        /// </summary>
        private bool bmShowPaxT = true; //igu on 24/02/2011

        #endregion _My Members

        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.Panel panelTab;
        private AxTABLib.AxTAB tabResult;
        private AxTABLib.AxTAB tabExcelResult;
        private System.Windows.Forms.Label lblResults;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnLoadPrint;
        private System.Windows.Forms.Button btnPrintPreview;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtLens;
        private System.Windows.Forms.Button btnLens;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButtonLANDAIRB;
        private System.Windows.Forms.RadioButton radioButtonONBLOFBL;
        private System.Windows.Forms.RadioButton radioButtonSTASTD;
        private System.Windows.Forms.RadioButton radioButtonICAO;
        private System.Windows.Forms.RadioButton radioButtonIATA;
        private System.Windows.Forms.Button buttonHelp;
        private CheckBox chkShowLegWithinTimeframe;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public frmDaily_FlightLog()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeMouseEvents();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDaily_FlightLog));
            this.panelBody = new System.Windows.Forms.Panel();
            this.panelTab = new System.Windows.Forms.Panel();
            this.tabResult = new AxTABLib.AxTAB();
            this.tabExcelResult = new AxTABLib.AxTAB();
            this.lblResults = new System.Windows.Forms.Label();
            this.panelTop = new System.Windows.Forms.Panel();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonICAO = new System.Windows.Forms.RadioButton();
            this.radioButtonIATA = new System.Windows.Forms.RadioButton();
            this.radioButtonLANDAIRB = new System.Windows.Forms.RadioButton();
            this.radioButtonONBLOFBL = new System.Windows.Forms.RadioButton();
            this.radioButtonSTASTD = new System.Windows.Forms.RadioButton();
            this.btnLens = new System.Windows.Forms.Button();
            this.txtLens = new System.Windows.Forms.TextBox();
            this.lblProgress = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnLoadPrint = new System.Windows.Forms.Button();
            this.btnPrintPreview = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkShowLegWithinTimeframe = new System.Windows.Forms.CheckBox();
            this.panelBody.SuspendLayout();
            this.panelTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).BeginInit();
            this.panelTop.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBody
            // 
            this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelBody.Controls.Add(this.panelTab);
            this.panelBody.Controls.Add(this.lblResults);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 176);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(796, 358);
            this.panelBody.TabIndex = 5;
            // 
            // panelTab
            // 
            this.panelTab.Controls.Add(this.tabResult);
            this.panelTab.Controls.Add(this.tabExcelResult);
            this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTab.Location = new System.Drawing.Point(0, 16);
            this.panelTab.Name = "panelTab";
            this.panelTab.Size = new System.Drawing.Size(796, 342);
            this.panelTab.TabIndex = 2;
            // 
            // tabResult
            // 
            this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabResult.Location = new System.Drawing.Point(0, 0);
            this.tabResult.Name = "tabResult";
            this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
            this.tabResult.Size = new System.Drawing.Size(796, 342);
            this.tabResult.TabIndex = 0;
            this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
            // 
            // tabExcelResult
            // 
            this.tabExcelResult.Location = new System.Drawing.Point(0, 0);
            this.tabExcelResult.Name = "tabExcelResult";
            this.tabExcelResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabExcelResult.OcxState")));
            this.tabExcelResult.Size = new System.Drawing.Size(100, 50);
            this.tabExcelResult.TabIndex = 1;
            // 
            // lblResults
            // 
            this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblResults.Location = new System.Drawing.Point(0, 0);
            this.lblResults.Name = "lblResults";
            this.lblResults.Size = new System.Drawing.Size(796, 16);
            this.lblResults.TabIndex = 1;
            this.lblResults.Text = "Report Results";
            this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelTop.Controls.Add(this.chkShowLegWithinTimeframe);
            this.panelTop.Controls.Add(this.buttonHelp);
            this.panelTop.Controls.Add(this.groupBox2);
            this.panelTop.Controls.Add(this.radioButtonLANDAIRB);
            this.panelTop.Controls.Add(this.radioButtonONBLOFBL);
            this.panelTop.Controls.Add(this.radioButtonSTASTD);
            this.panelTop.Controls.Add(this.btnLens);
            this.panelTop.Controls.Add(this.txtLens);
            this.panelTop.Controls.Add(this.lblProgress);
            this.panelTop.Controls.Add(this.progressBar1);
            this.panelTop.Controls.Add(this.btnLoadPrint);
            this.panelTop.Controls.Add(this.btnPrintPreview);
            this.panelTop.Controls.Add(this.btnCancel);
            this.panelTop.Controls.Add(this.btnOK);
            this.panelTop.Controls.Add(this.dtTo);
            this.panelTop.Controls.Add(this.label2);
            this.panelTop.Controls.Add(this.dtFrom);
            this.panelTop.Controls.Add(this.label1);
            this.panelTop.Controls.Add(this.groupBox1);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(796, 176);
            this.panelTop.TabIndex = 4;
            // 
            // buttonHelp
            // 
            this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonHelp.Location = new System.Drawing.Point(396, 140);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(75, 23);
            this.buttonHelp.TabIndex = 8;
            this.buttonHelp.Text = "Help";
            this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonICAO);
            this.groupBox2.Controls.Add(this.radioButtonIATA);
            this.groupBox2.Location = new System.Drawing.Point(228, 32);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(120, 76);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            // 
            // radioButtonICAO
            // 
            this.radioButtonICAO.Location = new System.Drawing.Point(12, 44);
            this.radioButtonICAO.Name = "radioButtonICAO";
            this.radioButtonICAO.Size = new System.Drawing.Size(104, 24);
            this.radioButtonICAO.TabIndex = 1;
            this.radioButtonICAO.Text = "ICAO";
            this.radioButtonICAO.CheckedChanged += new System.EventHandler(this.radioButtonICAO_CheckedChanged);
            // 
            // radioButtonIATA
            // 
            this.radioButtonIATA.Location = new System.Drawing.Point(12, 16);
            this.radioButtonIATA.Name = "radioButtonIATA";
            this.radioButtonIATA.Size = new System.Drawing.Size(104, 24);
            this.radioButtonIATA.TabIndex = 0;
            this.radioButtonIATA.Text = "IATA";
            this.radioButtonIATA.CheckedChanged += new System.EventHandler(this.radioButtonIATA_CheckedChanged);
            // 
            // radioButtonLANDAIRB
            // 
            this.radioButtonLANDAIRB.Location = new System.Drawing.Point(24, 100);
            this.radioButtonLANDAIRB.Name = "radioButtonLANDAIRB";
            this.radioButtonLANDAIRB.Size = new System.Drawing.Size(180, 24);
            this.radioButtonLANDAIRB.TabIndex = 11;
            this.radioButtonLANDAIRB.Text = "&LAND / AIRB";
            this.radioButtonLANDAIRB.CheckedChanged += new System.EventHandler(this.radioButtonLANDAIRB_CheckedChanged);
            // 
            // radioButtonONBLOFBL
            // 
            this.radioButtonONBLOFBL.Location = new System.Drawing.Point(24, 76);
            this.radioButtonONBLOFBL.Name = "radioButtonONBLOFBL";
            this.radioButtonONBLOFBL.Size = new System.Drawing.Size(184, 20);
            this.radioButtonONBLOFBL.TabIndex = 10;
            this.radioButtonONBLOFBL.Text = "&ONBL / OFBL";
            this.radioButtonONBLOFBL.CheckedChanged += new System.EventHandler(this.radioButtonONBLOFBL_CheckedChanged);
            // 
            // radioButtonSTASTD
            // 
            this.radioButtonSTASTD.Location = new System.Drawing.Point(24, 52);
            this.radioButtonSTASTD.Name = "radioButtonSTASTD";
            this.radioButtonSTASTD.Size = new System.Drawing.Size(180, 20);
            this.radioButtonSTASTD.TabIndex = 9;
            this.radioButtonSTASTD.Text = "&STA / STD";
            this.radioButtonSTASTD.CheckedChanged += new System.EventHandler(this.radioButtonSTASTD_CheckedChanged);
            // 
            // btnLens
            // 
            this.btnLens.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnLens.Location = new System.Drawing.Point(680, 28);
            this.btnLens.Name = "btnLens";
            this.btnLens.Size = new System.Drawing.Size(75, 23);
            this.btnLens.TabIndex = 24;
            this.btnLens.Text = "Lens";
            this.btnLens.UseVisualStyleBackColor = false;
            this.btnLens.Visible = false;
            this.btnLens.Click += new System.EventHandler(this.btnLens_Click);
            // 
            // txtLens
            // 
            this.txtLens.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.txtLens.Location = new System.Drawing.Point(404, 4);
            this.txtLens.Name = "txtLens";
            this.txtLens.Size = new System.Drawing.Size(376, 20);
            this.txtLens.TabIndex = 23;
            this.txtLens.Text = "textBox1";
            this.txtLens.Visible = false;
            // 
            // lblProgress
            // 
            this.lblProgress.Location = new System.Drawing.Point(496, 116);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(212, 16);
            this.lblProgress.TabIndex = 22;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(492, 140);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(216, 23);
            this.progressBar1.TabIndex = 21;
            this.progressBar1.Visible = false;
            // 
            // btnLoadPrint
            // 
            this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLoadPrint.Location = new System.Drawing.Point(244, 140);
            this.btnLoadPrint.Name = "btnLoadPrint";
            this.btnLoadPrint.Size = new System.Drawing.Size(75, 23);
            this.btnLoadPrint.TabIndex = 6;
            this.btnLoadPrint.Text = "Loa&d + Print";
            this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
            // 
            // btnPrintPreview
            // 
            this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPrintPreview.Location = new System.Drawing.Point(168, 140);
            this.btnPrintPreview.Name = "btnPrintPreview";
            this.btnPrintPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPrintPreview.TabIndex = 5;
            this.btnPrintPreview.Text = "&Print Preview";
            this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Location = new System.Drawing.Point(320, 140);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "&Close";
            // 
            // btnOK
            // 
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOK.Location = new System.Drawing.Point(92, 140);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "&Load Data";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTo.Location = new System.Drawing.Point(268, 4);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(128, 20);
            this.dtTo.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(232, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "to:";
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFrom.Location = new System.Drawing.Point(92, 4);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(128, 20);
            this.dtFrom.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date from:";
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(12, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Time to be considered";
            // 
            // chkShowLegWithinTimeframe
            // 
            this.chkShowLegWithinTimeframe.AutoSize = true;
            this.chkShowLegWithinTimeframe.Location = new System.Drawing.Point(368, 83);
            this.chkShowLegWithinTimeframe.Name = "chkShowLegWithinTimeframe";
            this.chkShowLegWithinTimeframe.Size = new System.Drawing.Size(226, 17);
            this.chkShowLegWithinTimeframe.TabIndex = 30;
            this.chkShowLegWithinTimeframe.Text = "Show only rotation leg within the timeframe";
            this.chkShowLegWithinTimeframe.UseVisualStyleBackColor = true;
            // 
            // frmDaily_FlightLog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(796, 534);
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmDaily_FlightLog";
            this.Text = "Daily Flight Log";
            this.Load += new System.EventHandler(this.frmDaily_FlightLog_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmDaily_FlightLog_Closing);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmDaily_FlightLog_HelpRequested);
            this.panelBody.ResumeLayout(false);
            this.panelTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion


        /// <summary>
        /// Loads the from and inits the necessary things.
        /// </summary>
        /// <param name="sender">Originator of the method call</param>
        /// <param name="e">Event args</param>
        private void frmDaily_FlightLog_Load(object sender, System.EventArgs e)
        {
            myDB = UT.GetMemDB();
            InitTimePickers();
            SetupReportTabHeader();
            InitTab();
            PrepareFilterData();
            //tabResult.AutoSizeColumns();

            if (bmUseDecs) //igu on 03/03/2011
            {
                myDEN = myDB.Bind("DEN", "DEN", "DECA,DECN,DECS,DENA,ALC3,URNO", "2,2,2,75,4,10", "DECA,DECN,DECS,DENA,ALC3,URNO");
            }
            else
            {
                myDEN = myDB.Bind("DEN", "DEN", "DECA,DECN,DENA,URNO", "2,2,75,10", "DECA,DECN,DENA,URNO");
            }
            myDEN.Load("");
            myDEN.CreateIndex("URNO", "URNO"); //igu on 03/03/2011
        }
        /// <summary>
        /// initializes the From/to time control with the current day in
        /// the custom format "dd.MM.yyyy - HH:mm".
        /// </summary>
        private void InitTimePickers()
        {
            DateTime olFrom;
            DateTime olTo;

            olFrom = DateTime.Now;
            olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0, 0, 0);
            olTo = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23, 59, 0);
            dtFrom.Value = olFrom;
            dtTo.Value = olTo;
            dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
            dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
        }

        //--change InitTab()
        //igu on 19//01/2011
        /// <summary>
        /// Initializes the tab control.
        /// </summary>
        private void InitTab()
        {
            InitTab(true, true, true);
        }

        private void InitTab(bool _bInitTabReport, bool _bInitTabExcel, bool _bFirstInit)
        {
            //igu on 24/02/2011
            if (_bFirstInit)
            {
                IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
                strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
                strExcelDateTimeFormat = myIni.IniReadValue("FIPSREPORT", "DATEEXCELFORMAT");
                string strShowPaxT = myIni.IniReadValue("FIPSREPORT", "SHOW_PAXT_IN_DAILY_FLIGHT_LOG"); //igu on 24/02/2011                
                bmShowPaxT = !(strShowPaxT.Equals("N") || strShowPaxT.Equals("NO"));//igu on 24/02/2011                
                string strPaxTLen = "0";
                if (bmShowPaxT)
                {
                    strPaxTLen = "34";
                }

                strTabHeaderLensRotationRaw = strTabHeaderLensRotationRaw.Replace("@@PaxT_Len", strPaxTLen);
                strExcelTabHeaderLensRotationRaw = strExcelTabHeaderLensRotationRaw.Replace("@@PaxT_Len", strPaxTLen);

                strTabHeaderLensRotation = strTabHeaderLensRotation.Replace("@@PaxT_Len", strPaxTLen);
                strExcelTabHeaderLensRotation = strExcelTabHeaderLensRotation.Replace("@@PaxT_Len", strPaxTLen);
            }
            //

            if (_bInitTabReport)
            {
                tabResult.ResetContent();
                tabResult.LifeStyle = true;
                tabResult.LineHeight = 16;
                tabResult.FontName = "Arial";
                tabResult.FontSize = 14;
                tabResult.HeaderFontSize = 14;
                tabResult.AutoSizeByHeader = true;
                tabResult.HeaderString = strTabHeaderRotation;
                tabResult.LogicalFieldList = strLogicalFieldsRotation;
                tabResult.HeaderLengthString = strTabHeaderLensRotation;
                tabResult.ShowHorzScroller(true); //igu on 03/03/2011
            }

            if (_bInitTabExcel)
            {
                tabExcelResult.ResetContent();
                tabExcelResult.HeaderString = strTabHeaderRotation;
                tabExcelResult.LogicalFieldList = strLogicalFieldsRotation;
                tabExcelResult.HeaderLengthString = strTabHeaderLensRotation;
            }

            if (_bFirstInit)
            {
                if (strDateDisplayFormat.Length == 0)
                {
                    strDateDisplayFormat = strReportDateTimeFormat;
                }
                else
                {
                    strReportDateTimeFormat = strDateDisplayFormat;
                }
                if (strExcelDateTimeFormat.Length == 0)
                {
                    strExcelDateTimeFormat = strReportDateTimeFormat;
                }

                PrepareReportDataRotations("Report");
                PrepareReportDataRotations("Excel");
            }
        }
        //--end of InitTab()
        //igu on 19//01/2011

        //--change SetupReportTabHeader()
        //igu on 18//01/2011
        /// <summary>
        /// Initializes the tab control header data.
        /// </summary>
        private void SetupReportTabHeader()
        {
            SetupReportTabHeader(new DelayCount(), true);
        }

        private void SetupReportTabHeader(DelayCount _maxNumberOfDelays, bool _checkChuteDisplay)
        {
            //igu on 22/03/2011
            string sArrDelHdr = "";
            string sArrDelFld = "";
            string sArrDelLen = "";
            string sDepDelHdr = "";
            string sDepDelFld = "";
            string sDepDelLen = "";

            for (int i = 1; i <= _maxNumberOfDelays.ArrDelayCodeCount; i++)
            {
                sArrDelHdr += "Del" + i.ToString() + ",Min,";
                sArrDelFld += "Del" + i.ToString() + "_A,Min" + i.ToString() + "_A,";
                sArrDelLen += "34,30,";
            }
            for (int i = 1; i <= _maxNumberOfDelays.ArrDelayCodeCount; i++)
            {
                sArrDelHdr += "SC" + i.ToString() + ",";
                sArrDelFld += "SC" + i.ToString() + "_A,";
                sArrDelLen += "34,";
            }
            for (int i = 1; i <= _maxNumberOfDelays.DepDelayCodeCount; i++)
            {
                sDepDelHdr += "Del" + i.ToString() + ",Min,";
                sDepDelFld += "Del" + i.ToString() + "_D,Min" + i.ToString() + "_D,";
                sDepDelLen += "34,30,";
            }
            for (int i = 1; i <= _maxNumberOfDelays.DepDelayCodeCount; i++)
            {
                sDepDelHdr += "SC" + i.ToString() + ",";
                sDepDelFld += "SC" + i.ToString() + "_D,";
                sDepDelLen += "34,";
            }

            strTabHeaderRotation = string.Format(strTabHeaderRotationRaw, sArrDelHdr, sDepDelHdr);
            strLogicalFieldsRotation = string.Format(strLogicalFieldsRotationRaw, sArrDelFld, sDepDelFld);
            strTabHeaderLensRotation = string.Format(strTabHeaderLensRotationRaw, sArrDelLen, sDepDelLen);
            strExcelTabHeaderLensRotation = string.Format(strExcelTabHeaderLensRotationRaw, sArrDelLen, sDepDelLen);
            //

            string[] strLogicalFieldsRotationArray = strLogicalFieldsRotation.Split(',');

            if (_checkChuteDisplay)
            {
                IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
                if (myIni.IniReadValue("FIPS", "CHUTE_DISPLAY").CompareTo("TRUE") == 0)
                {
                    bmChuteDisplay = true;
                }
            }

            string[] strLabelHeaderEntry = new string[2];
            string strTerminalHeader = "";
            int arrTermPos = Array.IndexOf(strLogicalFieldsRotationArray, "Term_A");
            int depTermPos = Array.IndexOf(strLogicalFieldsRotationArray, "Term_D");
            if (Helper.IsSTEVConfigured(this, ref strLabelHeaderEntry, true, true) == true)
            {
                bmShowStev = true;
                strTerminalHeader = strLabelHeaderEntry[1];

                Helper.RemoveOrReplaceString(arrTermPos, ref strTabHeaderRotation, ',', strTerminalHeader, false);
                Helper.RemoveOrReplaceString(depTermPos, ref strTabHeaderRotation, ',', strTerminalHeader, false);
                //Helper.RemoveOrReplaceString(CalcItemPos(12, intTabHeaderRotationStartPos, intAdd), ref strTabHeaderRotation, ',', strTerminalHeader, false);
                //Helper.RemoveOrReplaceString(CalcItemPos(33, intTabHeaderRotationStartPos, intAdd), ref strTabHeaderRotation, ',', strTerminalHeader, false);
            }

            if (bmChuteDisplay == false)
            {
                int chutePos = Array.IndexOf(strLogicalFieldsRotationArray, "ChuteFr_To_D");

                Helper.RemoveOrReplaceString(Array.IndexOf(strLogicalFieldsRotationArray, "Arrival_A"), ref strTabHeaderLensRotation, ',', "54", false);
                Helper.RemoveOrReplaceString(Array.IndexOf(strLogicalFieldsRotationArray, "Org_A"), ref strTabHeaderLensRotation, ',', "36", false);
                Helper.RemoveOrReplaceString(Array.IndexOf(strLogicalFieldsRotationArray, "Sta_A"), ref strTabHeaderLensRotation, ',', "50", false);
                Helper.RemoveOrReplaceString(Array.IndexOf(strLogicalFieldsRotationArray, "A_C_A"), ref strTabHeaderLensRotation, ',', "33", false);
                Helper.RemoveOrReplaceString(Array.IndexOf(strLogicalFieldsRotationArray, "Depart_D"), ref strTabHeaderLensRotation, ',', "58", false);
                Helper.RemoveOrReplaceString(Array.IndexOf(strLogicalFieldsRotationArray, "Des_D"), ref strTabHeaderLensRotation, ',', "36", false);
                Helper.RemoveOrReplaceString(Array.IndexOf(strLogicalFieldsRotationArray, "Std_D"), ref strTabHeaderLensRotation, ',', "50", false);
                Helper.RemoveOrReplaceString(Array.IndexOf(strLogicalFieldsRotationArray, "Etd_D"), ref strTabHeaderLensRotation, ',', "55", false);
                //Helper.RemoveOrReplaceString(CalcItemPos(36, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "60", false);

                Helper.RemoveOrReplaceString(chutePos, ref strTabHeaderRotation, ',', "", true);
                Helper.RemoveOrReplaceString(chutePos, ref strLogicalFieldsRotation, ',', "", true);
                Helper.RemoveOrReplaceString(chutePos, ref strTabHeaderLensRotation, ',', "", true);
                Helper.RemoveOrReplaceString(chutePos, ref strExcelTabHeaderLensRotation, ',', "", true);

                //Helper.RemoveOrReplaceString(CalcItemPos(1, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "54", false);
                //Helper.RemoveOrReplaceString(CalcItemPos(2, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "36", false);
                //Helper.RemoveOrReplaceString(CalcItemPos(5, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "50", false);
                //Helper.RemoveOrReplaceString(CalcItemPos(16, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "33", false);
                //Helper.RemoveOrReplaceString(CalcItemPos(18, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "58", false);
                //Helper.RemoveOrReplaceString(CalcItemPos(19, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "36", false);
                //Helper.RemoveOrReplaceString(CalcItemPos(22, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "50", false);
                //Helper.RemoveOrReplaceString(CalcItemPos(23, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "55", false);
                //Helper.RemoveOrReplaceString(CalcItemPos(36, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "60", false);

                //Helper.RemoveOrReplaceString(CalcItemPos(36, intTabHeaderRotationStartPos, intAdd), ref strTabHeaderRotation, ',', "", true);
                //Helper.RemoveOrReplaceString(CalcItemPos(36, intLogicalFieldsRotationStartPos, intAdd), ref strLogicalFieldsRotation, ',', "", true);
                //Helper.RemoveOrReplaceString(CalcItemPos(36, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "", true);
                //Helper.RemoveOrReplaceString(CalcItemPos(36, intTabHeaderLensRotationStartPos, intAdd), ref strExcelTabHeaderLensRotation, ',', "", true);
            }
            if (bmShowStev == false)
            {
                Helper.RemoveOrReplaceString(Array.IndexOf(strLogicalFieldsRotationArray, "Eta_A"), ref strTabHeaderLensRotation, ',', "58", false);
                Helper.RemoveOrReplaceString(Array.IndexOf(strLogicalFieldsRotationArray, "Tmo_A"), ref strTabHeaderLensRotation, ',', "58", false);
                Helper.RemoveOrReplaceString(Array.IndexOf(strLogicalFieldsRotationArray, "Land_A"), ref strTabHeaderLensRotation, ',', "58", false);

                Helper.RemoveOrReplaceString(arrTermPos, ref strTabHeaderRotation, ',', "", true);
                Helper.RemoveOrReplaceString(depTermPos - 1, ref strTabHeaderRotation, ',', "", true);
                Helper.RemoveOrReplaceString(arrTermPos, ref strTabHeaderLensRotation, ',', "", true);
                Helper.RemoveOrReplaceString(depTermPos - 1, ref strTabHeaderLensRotation, ',', "", true);
                Helper.RemoveOrReplaceString(arrTermPos, ref strExcelTabHeaderLensRotation, ',', "", true);
                Helper.RemoveOrReplaceString(depTermPos - 1, ref strExcelTabHeaderLensRotation, ',', "", true);
                Helper.RemoveOrReplaceString(arrTermPos, ref strLogicalFieldsRotation, ',', "", true);
                Helper.RemoveOrReplaceString(depTermPos - 1, ref strLogicalFieldsRotation, ',', "", true);

                //Helper.RemoveOrReplaceString(CalcItemPos(6, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "58", false);
                //Helper.RemoveOrReplaceString(CalcItemPos(7, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "58", false);
                //Helper.RemoveOrReplaceString(CalcItemPos(8, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "58", false);

                //Helper.RemoveOrReplaceString(CalcItemPos(12, intTabHeaderRotationStartPos, intAdd), ref strTabHeaderRotation, ',', "", true);
                //Helper.RemoveOrReplaceString(CalcItemPos(32, intTabHeaderRotationStartPos, intAdd), ref strTabHeaderRotation, ',', "", true);
                //Helper.RemoveOrReplaceString(CalcItemPos(12, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "", true);
                //Helper.RemoveOrReplaceString(CalcItemPos(32, intTabHeaderLensRotationStartPos, intAdd), ref strTabHeaderLensRotation, ',', "", true);
                //Helper.RemoveOrReplaceString(CalcItemPos(12, intExcelTabHeaderLensRotationStartPos, intAdd), ref strExcelTabHeaderLensRotation, ',', "", true);
                //Helper.RemoveOrReplaceString(CalcItemPos(32, intExcelTabHeaderLensRotationStartPos, intAdd), ref strExcelTabHeaderLensRotation, ',', "", true);
                //Helper.RemoveOrReplaceString(CalcItemPos(12, intLogicalFieldsRotationStartPos, intAdd), ref strLogicalFieldsRotation, ',', "", true);
                //Helper.RemoveOrReplaceString(CalcItemPos(32, intLogicalFieldsRotationStartPos, intAdd), ref strLogicalFieldsRotation, ',', "", true);
            }
        }
        //--end of SetupReportTabHeader()
        //igu on 18//01/2011

        ////igu on 18//01/2011
        ///// <summary>
        ///// Calculate the correct position of Logical Field, Header String or Length String
        ///// </summary>
        //private int CalcItemPos(int _OriginalPos, int _StartPosToChange, int _Additional)
        //{
        //    return (_OriginalPos + (_OriginalPos >= _StartPosToChange ? _Additional : 0));
        //}


        /// <summary>
        /// Loads and prepare the data necessary for the filter. 
        /// In this case loads the nature codes and puts the
        /// data into the combobox.
        /// </summary>
        private void PrepareFilterData()
        {
            this.radioButtonIATA.Checked = true;
            this.radioButtonSTASTD.Checked = true;
        }
        /// <summary>
        /// Runs the data queries according to the reports needs and puts
        ///	Calls the data prepare method and puts the prepared data into
        ///	the tab control. After this is finished it calls the reports
        ///	by sending the tab and the headers to the generic report.
        /// </summary>
        /// <param name="sender">Originator object of the call</param>
        /// <param name="e">Event arguments</param>
        private void btnOK_Click(object sender, System.EventArgs e)
        {
            string strError = ValidateUserEntry();
            if (strError != "")
            {
                MessageBox.Show(this, strError);
                return; // Do nothing due to errors
            }
            flightDelaysCollection = new FlightDelaysCollection();
            this.Cursor = Cursors.WaitCursor;
            LoadReportData();
            PrepareReportDataRotations("Report");
            PrepareReportDataRotations("Excel");
            //RunReport();
            this.Cursor = Cursors.Arrow;
        }
        /// <summary>
        /// Validates the user entry for nonsense.
        /// </summary>
        /// <returns></returns>
        private string ValidateUserEntry()
        {
            string strRet = "";
            int ilErrorCount = 1;

            if (dtFrom.Value >= dtTo.Value)
            {
                strRet += ilErrorCount.ToString() + ". Date From is later than Date To!\n";
                ilErrorCount++;
            }
            return strRet;
        }
        /// <summary>
        /// Loads all necessary tables with filter criteria from the 
        /// database ==> to memoryDB.
        /// </summary>
        private void LoadReportData()
        {
            string strTmpWhere = strWhereRaw;
            //Clear the tab
            tabResult.ResetContent();
            tabExcelResult.ResetContent();

            // In the first step change the times to UTC if this is
            // necessary.
            DateTime datFrom;
            DateTime datTo;
            string strDateFrom = "";
            string strDateTo = "";
            datFrom = dtFrom.Value;
            datTo = dtTo.Value;
            if (UT.IsTimeInUtc == false)
            {
                datFrom = UT.LocalToUtc(dtFrom.Value);
                datTo = UT.LocalToUtc(dtTo.Value);
                strDateFrom = UT.DateTimeToCeda(datFrom);
                strDateTo = UT.DateTimeToCeda(datTo);
            }
            else
            {
                strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
                strDateTo = UT.DateTimeToCeda(dtTo.Value);
            }

            //Now reading day by day the flight records in order to 
            //not stress the database too much
            myDB.Unbind("AFT");
            myAFT = myDB.Bind("AFT", "AFT",
                "RKEY,URNO,FTYP,ADID,FLNO,CSGN,FLTI,JCNT,STOA,ETAI,LAND,ONBL,TMOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,ETDI,OFBL,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT,REGN,SLOT,AIRB,DCD1,DCD2,DTD1,DTD2,WRO1,WRO2,REM1,PAXT,ALC2,ALC3,ACT5",
                "10,10,1,1,9,8,1,1,14,14,14,14,14,14,1,5,1,2,3,4,3,4,2,5,5,5,5,5,1,3,14,14,14,14,1,3,4,5,5,5,5,5,12,14,14,2,2,4,4,5,5,256,7,2,3,5",
                "RKEY,URNO,FTYP,ADID,FLNO,CSGN,FLTI,JCNT,STOA,ETAI,LAND,ONBL,TMOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,ETDI,OFBL,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT,REGN,SLOT,AIRB,DCD1,DCD2,DTD1,DTD2,WRO1,WRO2,REM1,PAXT,ALC2,ALC3,ACT5");
            myAFT.Clear();
            myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL,TMOA,ETAI,LAND,ETDI,SLOT,AIRB";
            myAFT.Command("read", ",GFR,");
            ArrayList myUniques = new ArrayList();
            myUniques.Add("URNO");
            myAFT.UniqueFields = myUniques;
            myAFT.TimeFieldsInitiallyInUtc = true;

            //Prepare loop reading day by day
            DateTime datReadFrom = datFrom;
            DateTime datReadTo;
            TimeSpan tsDays = (datTo - datFrom);
            progressBar1.Value = 0;
            int ilTotal = Convert.ToInt32(tsDays.TotalDays);
            if (ilTotal == 0) ilTotal = 1;
            lblProgress.Text = "Loading Data";
            lblProgress.Refresh();
            progressBar1.Show();
            int loopCnt = 1;

            do
            {
                int percent = Convert.ToInt32((loopCnt * 100) / ilTotal);
                if (percent > 100)
                    percent = 100;
                progressBar1.Value = percent;
                datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
                if (datReadTo > datTo) datReadTo = datTo;
                strDateFrom = UT.DateTimeToCeda(datReadFrom);
                strDateTo = UT.DateTimeToCeda(datReadTo);
                //patch the where statement according to the user's entry
                //				strTmpWhere = strWhereRaw;
                if (this.radioButtonSTASTD.Checked == true)
                {
                    strTmpWhere = strWhereRaw;
                }
                else if (this.radioButtonONBLOFBL.Checked == true)
                {
                    strTmpWhere = strWhereONBLOFBLRaw;
                }
                else if (this.radioButtonLANDAIRB.Checked == true)
                {
                    strTmpWhere = strWhereLANDAIRBRaw;
                }

                strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
                strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo);
                strTmpWhere = strTmpWhere.Replace("@@HOPO", UT.Hopo);
                strTmpWhere += "[ROTATIONS]";
                //Load the data from AFTTAB
                myAFT.Load(strTmpWhere);
                datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
                loopCnt++;
            } while (datReadFrom <= datReadTo);
            //progressBar1.Hide();
            //----------------------  LOATAB Data
            myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

            //Filter out the FTYPs which are selected due to rotations
            for (int i = myAFT.Count - 1; i >= 0; i--)
            {
                if (myAFT[i]["FTYP"] == "T" || myAFT[i]["FTYP"] == "G" ||
                    /*myAFT[i]["FTYP"] == "N" || myAFT[i]["FTYP"] == "X" ||
                    myAFT[i]["FTYP"] == "B" ||*/
                                                 myAFT[i]["FTYP"] == "Z")
                {
                    myAFT.Remove(i);
                }
            }

            myDB.Unbind("LOA");
            myLOA = myDB.Bind("LOA", "LOA", "URNO,FLNU,DSSN,VALU", "10,10,3,6", "URNO,FLNU,DSSN,VALU");

            lblProgress.Text = "Loading Pax Data";
            lblProgress.Refresh();
            int loops = 0;
            progressBar1.Value = 0;
            progressBar1.Refresh();
            ilTotal = (int)(myAFT.Count / 300);
            if (ilTotal == 0) ilTotal = 1;
            string strFlnus = "";
            strTmpWhere = "";
            for (int i = 0; i < myAFT.Count; i++)
            {
                strFlnus += myAFT[i]["URNO"] + ",";
                if ((i % 300) == 0 && i > 0)
                {
                    loops++;
                    int percent = Convert.ToInt32((loops * 100) / ilTotal);
                    if (percent > 100)
                        percent = 100;
                    progressBar1.Value = percent;
                    strFlnus = strFlnus.Remove(strFlnus.Length - 1, 1);
                    strTmpWhere = strWhereLoa;
                    strTmpWhere = strTmpWhere.Replace("@@FLNU", strFlnus);
                    myLOA.Load(strTmpWhere);
                    strFlnus = "";
                }
            }

            //ilTotal = (int)(myAFT.Count / 50);
            //if (ilTotal == 0) ilTotal = 1;
            //string strFlnus = "";
            //strTmpWhere = "";
            //for (int i = 0; i < myAFT.Count; i++)
            //{
            //    strFlnus += myAFT[i]["URNO"] + ",";
            //    if ((i % 50) == 0 && i > 0)
            //    {
            //        loops++;
            //        int percent = Convert.ToInt32((loops * 100) / ilTotal);
            //        if (percent > 100)
            //            percent = 100;
            //        progressBar1.Value = percent;
            //        strFlnus = strFlnus.Remove(strFlnus.Length - 1, 1);
            //        strTmpWhere = strWhereLoa;
            //        strTmpWhere = strTmpWhere.Replace("@@FLNU", strFlnus);
            //        myLOA.Load(strTmpWhere);
            //        strFlnus = "";
            //    }
            //}

            //Load the rest of the GPAs.
            if (strFlnus.Length > 0)
            {
                strFlnus = strFlnus.Remove(strFlnus.Length - 1, 1);
                strTmpWhere = strWhereLoa;
                strTmpWhere = strTmpWhere.Replace("@@FLNU", strFlnus);
                myLOA.Load(strTmpWhere);
                strFlnus = "";
            }

            myLOA.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
            myLOA.CreateIndex("FLNU", "FLNU");

            //-------------------------------------------------------------------
            //Load dedicated check-in counter according to the AFT.URNO->CCA.FLNU
            myDB.Unbind("DED_CCA");
            myCCA = myDB.Bind("DED_CCA", "CCA", "FLNU,CKIC,CKBS,CKES,CKBA,CKEA,CTYP,CICF,CICT,ANZ", "10,5,14,14,14,14,3,5,5,3", "FLNU,CKIC,CKBS,CKES,CKBA,CKEA,CTYP");
            myCCA.Clear();
            lblProgress.Text = "Loading Counter Data";
            lblProgress.Refresh();
            loops = 0;
            progressBar1.Value = 0;
            progressBar1.Refresh();
            ilTotal = (int)(myAFT.Count / 300);
            if (ilTotal == 0) ilTotal = 1;
            strFlnus = "";
            string strTmpCCAWhere = strDedicatedCCAWhere;
            for (int i = 0; i < myAFT.Count; i++)
            {
                strFlnus += myAFT[i]["URNO"] + ",";
                if ((i % 300) == 0 && i > 0)
                {
                    loops++;
                    int percent = Convert.ToInt32((loops * 100) / ilTotal);
                    if (percent > 100)
                        percent = 100;
                    progressBar1.Value = percent;
                    strFlnus = strFlnus.Remove(strFlnus.Length - 1, 1);
                    strTmpCCAWhere = strDedicatedCCAWhere;
                    strTmpCCAWhere = strTmpCCAWhere.Replace("@@FLNU", strFlnus);
                    myCCA.Load(strTmpCCAWhere);
                    strFlnus = "";
                }
            }
            //Load the rest of the dedicated Counters.
            if (strFlnus.Length > 0)
            {
                strFlnus = strFlnus.Remove(strFlnus.Length - 1, 1);
                strTmpCCAWhere = strDedicatedCCAWhere;
                strTmpCCAWhere = strTmpCCAWhere.Replace("@@FLNU", strFlnus);
                myCCA.Load(strTmpCCAWhere);
                strFlnus = "";
            }
            myCCA.Sort("CKBS,CKBA,CKES,CKEA", true);
            myCCA.CreateIndex("FLNU", "FLNU");
            lblProgress.Text = "";

            //igu on 18/01/2011
            //-------------------------------------------------------------------
            //Load delays

            //mna on 18/01/2012
            //-------------------------------------------------------------------
            myDB.Unbind("DCF");
            myDCF = myDB.Bind("DCF", "DCF", "URNO,DURN,DURA,FURN,DECN,DECA,DECS,DSEQ,USEQ,USEC",
                              "10,10,4,10,2,2,2,2,2,32", "URNO,DURN,DURA,FURN,DECN,DECA,DECS,DSEQ,USEQ,USEC");
            myDCF.Clear();

            if (bmUseDecs)
            {
                lblProgress.Text = "Loading Delays Data";
                lblProgress.Refresh();
                loops = 0;
                progressBar1.Value = 0;
                progressBar1.Refresh();
                ilTotal = (int)(myAFT.Count / 300);
                if (ilTotal == 0) ilTotal = 1;
                strFlnus = "";
                string strTmpDCFWhere = strDCFWhere;
                for (int i = 0; i < myAFT.Count; i++)
                {
                    strFlnus += myAFT[i]["URNO"] + ",";
                    if ((i % 300) == 0 && i > 0)
                    {
                        loops++;
                        int percent = Convert.ToInt32((loops * 100) / ilTotal);
                        if (percent > 100)
                            percent = 100;
                        progressBar1.Value = percent;
                        strFlnus = strFlnus.Remove(strFlnus.Length - 1, 1);
                        strTmpDCFWhere = strDCFWhere;
                        strTmpDCFWhere = strTmpDCFWhere.Replace("@@FLNU", strFlnus);
                        myDCF.Load(strTmpDCFWhere);
                        strFlnus = "";
                    }
                }
                //Load the rest of the DCF.
                if (strFlnus.Length > 0)
                {
                    strFlnus = strFlnus.Remove(strFlnus.Length - 1, 1);
                    strTmpDCFWhere = strDCFWhere;
                    strTmpDCFWhere = strTmpDCFWhere.Replace("@@FLNU", strFlnus);
                    myDCF.Load(strTmpDCFWhere);
                    strFlnus = "";
                }
                //myDCF.Sort("CKBS,CKBA,CKES,CKEA", true);
                myDCF.CreateIndex("FURN", "FURN");
                lblProgress.Text = "";

                ////count maxNumberOfDelays
                //mMaxOfDelays = CtrlDelayedFlight.ComputeMaxDelayCnt(myAFT, myDCF, myDEN, bmUseDecs, out flightDelaysCollection);
            }

            //count maxNumberOfDelays

            mMaxOfDelays = CtrlDelayedFlight.ComputeMaxDelayCnt(myAFT, myDCF, myDEN, bmUseDecs, out flightDelaysCollection);

            //-end-
            //igu on 18/01/2011
            //-------------    

            if (bmChuteDisplay == true)
            {
                //-------------------------------------------------------------------
                //Load chutes according to the AFT.URNO->CHA.RURN
                myDB.Unbind("DED_CHA");
                myCHA = myDB.Bind("DED_CHA", "CHA", "RURN,LNAM,STOB,STOE,TIFB,TIFE,LTYP,CHUF,CHUT", "10,5,14,14,14,14,3,5,5,3", "RURN,LNAM,STOB,STOE,TIFB,TIFE,LTYP");
                myCHA.Clear();
                lblProgress.Text = "Loading Chute Data";
                lblProgress.Refresh();
                loops = 0;
                progressBar1.Value = 0;
                progressBar1.Refresh();
                ilTotal = (int)(myAFT.Count / 300);
                if (ilTotal == 0) ilTotal = 1;
                string strRurns = "";
                string strTmpCHAWhere = strDedicatedCHAWhere;
                for (int i = 0; i < myAFT.Count; i++)
                {
                    strRurns += myAFT[i]["URNO"] + ",";
                    if ((i % 300) == 0 && i > 0)
                    {
                        loops++;
                        int percent = Convert.ToInt32((loops * 100) / ilTotal);
                        if (percent > 100)
                            percent = 100;
                        progressBar1.Value = percent;
                        strRurns = strRurns.Remove(strRurns.Length - 1, 1);
                        strTmpCHAWhere = strDedicatedCHAWhere;
                        strTmpCHAWhere = strTmpCHAWhere.Replace("@@RURN", strRurns);
                        myCHA.Load(strTmpCHAWhere);
                        strRurns = "";
                    }
                }
                //Load the rest of the dedicated Chutes.
                if (strRurns.Length > 0)
                {
                    strRurns = strRurns.Remove(strRurns.Length - 1, 1);
                    strTmpCHAWhere = strDedicatedCHAWhere;
                    strTmpCHAWhere = strTmpCHAWhere.Replace("@@RURN", strRurns);
                    myCHA.Load(strTmpCHAWhere);
                    strRurns = "";
                }
                myCHA.Sort("STOB,TIFB,STOE,TIFE", true);
                myCHA.CreateIndex("RURN", "RURN");
            }

            progressBar1.Hide();
            lblProgress.Text = "";
        }

        /// <summary>
        /// Prepare the loaded data according to the report's needs.
        /// First organize the rotations and then put the result
        /// into the tabResult
        /// </summary>
        private void PrepareReportDataRotations(string strReportOrExcel)
        {
            if (myAFT == null)
                return;

            bool blButtonICAOChecked = this.radioButtonICAO.Checked;
            imArrivals = 0;
            imDepartures = 0;
            imTotalFlights = 0;
            myAFT.Sort("RKEY", true);
            int llCurr = 0;
            int ilStep = 1;
            string strCurrAdid = "";
            string strNextAdid = "";
            string strCurrRkey = "";
            string strNextRkey = "";
            string strPax = "";
            StringBuilder sb = new StringBuilder(10000);
            string sBlankArrDelays = new String(',', mMaxOfDelays.ArrDelayCodeCount * 3); //igu  on 23/03/2011                        
            string sBlankDepDelays = new String(',', mMaxOfDelays.DepDelayCodeCount * 3); //igu  on 23/03/2011                        

            SetupReportTabHeader(mMaxOfDelays, false); //igu on 19/01/2011
            InitTab(strReportOrExcel.Equals("Report"), strReportOrExcel.Equals("Excel"), false); //igu on 19/01/2011

            string strDateTimeFormat = strReportDateTimeFormat;
            if (strReportOrExcel.Equals("Excel") == true)
            {
                strDateTimeFormat = strExcelDateTimeFormat;
            }

            while (llCurr < myAFT.Count)
            {
                string strValues = "";
                strCurrRkey = myAFT[llCurr]["RKEY"];
                strCurrAdid = myAFT[llCurr]["ADID"];
                if ((llCurr + 1) < myAFT.Count)
                {
                    strNextRkey = myAFT[llCurr + 1]["RKEY"];
                    strNextAdid = myAFT[llCurr + 1]["ADID"];
                }
                else
                {
                    strNextRkey = "";
                    strNextAdid = "";
                }
                string strGate = "";
                string strBlt = "";

                //igu on 24/02/2011
                string strArrField = "";
                string strDepField = "";
                DateTime datFrom, datTo;
                string strDateFrom, strDateTo;
                bool bWithinTimeframe = false;
                if (radioButtonSTASTD.Checked)
                {
                    strArrField = "STOA";
                    strDepField = "STOD";
                }
                else
                {
                    if (radioButtonONBLOFBL.Checked)
                    {
                        strArrField = "ONBL";
                        strDepField = "OFBL";
                    }
                    else
                    {
                        if (radioButtonLANDAIRB.Checked)
                        {
                            strArrField = "LAND";
                            strDepField = "AIRB";
                        }
                    }
                }
                if (UT.IsTimeInUtc == false)
                {
                    datFrom = UT.LocalToUtc(dtFrom.Value);
                    datTo = UT.LocalToUtc(dtTo.Value);
                    strDateFrom = UT.DateTimeToCeda(datFrom);
                    strDateTo = UT.DateTimeToCeda(datTo);
                }
                else
                {
                    strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
                    strDateTo = UT.DateTimeToCeda(dtTo.Value);
                }
                //

                bool bArrWithinTimeFrame = false; //igu on 14/04/2011

                if (strCurrRkey == strNextRkey)
                {//Is a rotation
                    ilStep = 2;
                    if (strCurrAdid == "A" && strNextAdid == "D")
                    {
                        //Arrival,Org,Via,Na,Sta,Eta,Land,Onbl,Pax T,Pos,Gate,Belt,Ftp,A/C,Reg
                        //ARR:,,,,,,,,,,,,,,,
                        //DEP:,,,,,,,,,,,,,,,,,
                        bWithinTimeframe = ((myAFT[llCurr][strArrField].CompareTo(strDateFrom) >= 0) &&
                                            (myAFT[llCurr][strArrField].CompareTo(strDateTo) <= 0));
                        if (!chkShowLegWithinTimeframe.Checked || bWithinTimeframe)
                        {
                            bArrWithinTimeFrame = true; //igu on 14/04/2011

                            imArrivals++;

                            string strFLNO = myAFT[llCurr]["FLNO"];
                            if (blButtonICAOChecked == true && strFLNO.Length > 0)
                            {
                                strFLNO = myAFT[llCurr]["ALC3"] + myAFT[llCurr]["FLNO"].Substring(3);
                            }
                            if (strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
                            strValues += myAFT[llCurr]["STOA"] + myAFT[llCurr + 1]["STOD"] + ",";
                            strValues += strFLNO + ",";
                            strValues += blButtonICAOChecked == true ? myAFT[llCurr]["ORG4"] + "," : myAFT[llCurr]["ORG3"] + ",";
                            strValues += blButtonICAOChecked == true ? myAFT[llCurr]["VIA4"] + "," : myAFT[llCurr]["VIA3"] + ",";
                            strValues += myAFT[llCurr]["TTYP"] + ",";
                            strValues += Helper.DateString(myAFT[llCurr]["STOA"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr]["ETAI"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr]["TMOA"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr]["LAND"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr]["ONBL"], strDateTimeFormat) + ",";

                            //igu on 22/03/2011
                            strValues += PrepareDelayData(mMaxOfDelays, myAFT[llCurr]);

                            //changed for the PRF 8523
                            if (UT.Hopo == "DXB")
                            {
                                strValues += myAFT[llCurr]["PAXT"] + ",";
                            }
                            else
                            {
                                strPax = GetPaxData(myAFT[llCurr]["URNO"]);
                                if (strPax == "")
                                    strPax = myAFT[llCurr]["PAXT"];
                                strValues += strPax + ",";
                            }
                            strValues += myAFT[llCurr]["PSTA"] + ",";
                            if (bmShowStev == true)
                            {
                                strValues += myAFT[llCurr]["STEV"] + ",";
                            }
                            strGate = myAFT[llCurr]["GTA1"];
                            if (strGate == "") strGate = myAFT[llCurr]["GTA2"];
                            strValues += strGate + ",";
                            strBlt = myAFT[llCurr]["BLT1"];
                            if (strBlt == "") strBlt = myAFT[llCurr]["BLT2"];
                            strValues += strBlt + ",";
                            strValues += myAFT[llCurr]["FTYP"] + "/" + myAFT[llCurr + 1]["FTYP"] + ",";
                            strValues += blButtonICAOChecked == true ? myAFT[llCurr]["ACT5"] + "," : myAFT[llCurr]["ACT3"] + ",";
                            strValues += myAFT[llCurr]["REGN"] + ",";
                        }
                        else
                        {
                            //strValues += ",,,,,,,,,,,,,,,,,"; //igu on 14/04/2011
                            bArrWithinTimeFrame = false; //igu on 14/04/2011
                            strValues += ",,,,,,,,,,,,,," + sBlankArrDelays;  //igu on 14/04/2011
                            if (bmShowStev)
                            {
                                strValues += ",";
                            }
                        }
                        //-----------------------------------The departure part of rotation
                        //Depart.,Des,Via,Na,Std,Etd,Slot,Ofbl,Airb,Del1,Min,Del2,Min,Pax T,Pos,Gate,Wro,Rem.
                        //ARR:,,,,,,,,,,,,,,,
                        bWithinTimeframe = ((myAFT[llCurr + 1][strDepField].CompareTo(strDateFrom) >= 0) &&
                                            (myAFT[llCurr + 1][strDepField].CompareTo(strDateTo) <= 0));
                        if (!chkShowLegWithinTimeframe.Checked || bWithinTimeframe)
                        {
                            imDepartures++;

                            //igu on 14/04/2011
                            if (!bArrWithinTimeFrame)
                            {
                                strValues += myAFT[llCurr + 1]["FTYP"] + ",";
                                strValues += blButtonICAOChecked == true ? myAFT[llCurr + 1]["ACT5"] + "," : myAFT[llCurr + 1]["ACT3"] + ",";
                                strValues += myAFT[llCurr + 1]["REGN"] + ",";
                            }
                            //
                            string strFLNO = myAFT[llCurr + 1]["FLNO"];
                            if (blButtonICAOChecked == true && strFLNO.Length > 0)
                            {
                                strFLNO = myAFT[llCurr + 1]["ALC3"] + myAFT[llCurr + 1]["FLNO"].Substring(3);
                            }
                            if (strFLNO == "") strFLNO = myAFT[llCurr + 1]["CSGN"];
                            strValues += strFLNO + ",";
                            strValues += blButtonICAOChecked == true ? myAFT[llCurr + 1]["DES4"] + "," : myAFT[llCurr + 1]["DES3"] + ",";
                            strValues += blButtonICAOChecked == true ? myAFT[llCurr + 1]["VIA4"] + "," : myAFT[llCurr + 1]["VIA3"] + ",";
                            strValues += myAFT[llCurr + 1]["TTYP"] + ",";
                            strValues += Helper.DateString(myAFT[llCurr + 1]["STOD"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr + 1]["ETDI"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr + 1]["SLOT"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr + 1]["OFBL"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr + 1]["AIRB"], strDateTimeFormat) + ",";
                            //strValues += myAFT[llCurr+1]["DCD1"] + ","; //igu on 19/01/2011
                            //strValues += myAFT[llCurr+1]["DTD1"] + ",";
                            //strValues += myAFT[llCurr+1]["DCD2"] + ",";
                            //strValues += myAFT[llCurr+1]["DTD2"] + ",";
                            strValues += PrepareDelayData(mMaxOfDelays, myAFT[llCurr + 1]); //igu on 19/01/2011
                            //changed for the PRF 8523
                            if (UT.Hopo == "DXB")
                            {
                                strValues += myAFT[llCurr + 1]["PAXT"] + ",";
                            }
                            else
                            {
                                strPax = GetPaxData(myAFT[llCurr + 1]["URNO"]);
                                if (strPax == "")
                                    strPax = myAFT[llCurr + 1]["PAXT"];
                                strValues += strPax + ",";
                            }
                            strValues += myAFT[llCurr + 1]["PSTD"] + ",";
                            if (bmShowStev == true)
                            {
                                strValues += myAFT[llCurr + 1]["STEV"] + ",";
                            }
                            strGate = myAFT[llCurr + 1]["GTD1"];
                            if (strGate == "") strGate = myAFT[llCurr + 1]["GTD2"];
                            strValues += strGate + ",";

                            /*strBlt = myAFT[llCurr+1]["WRO1"];
                            if(strBlt == "") strBlt = myAFT[llCurr+1]["WRO2"];
                            strValues += strBlt + ",";*/

                            IRow[] rows = myCCA.RowsByIndexValue("FLNU", myAFT[llCurr + 1]["URNO"]);
                            string[] strCheckinData = GetCheckinCounterAllocationData(rows);
                            int ilCheckinSize = strCheckinData.Length;

                            string strRem = myAFT[llCurr + 1]["REM1"] + "-" + myAFT[llCurr]["REM1"];
                            //						strRem = strRem.Replace("\n", " ");
                            //						strRem = strRem.Replace("\r", " ");
                            bool blDataAppended = false;
                            if (bmChuteDisplay == true)
                            {
                                rows = myCHA.RowsByIndexValue("RURN", myAFT[llCurr + 1]["URNO"]);
                                string[] chuteData = GetChuteData(rows);
                                int ilChuteSize = chuteData.Length;
                                if (ilCheckinSize == 0 && ilChuteSize == 0)
                                {
                                    if (myAFT[llCurr + 1]["CKIF"] != "" || myAFT[llCurr + 1]["CKIT"] != "")
                                    {
                                        strValues += myAFT[llCurr + 1]["CKIF"] + "-" + myAFT[llCurr + 1]["CKIT"];
                                    }
                                    strValues += ",," + strRem + "\n";
                                    sb.Append(strValues);
                                    blDataAppended = true;
                                }
                                else
                                {
                                    //igu on 19/01/2011
                                    //----
                                    //for(int i = 0 ; (i < ilChuteSize || i < ilCheckinSize) ; i++)
                                    //{
                                    //    string strRowValue = strValues;
                                    //    strRowValue += i < ilCheckinSize ? strCheckinData[i] + "," : (i == 0 ? (myAFT[llCurr+1]["CKIF"] != "" || myAFT[llCurr+1]["CKIT"] != "" ? myAFT[llCurr+1]["CKIF"] + "-" + myAFT[llCurr+1]["CKIT"]+"," : ",") : ",");
                                    //    strRowValue += i < ilChuteSize ? chuteData[i] + "," : ",";
                                    //    strRowValue += strRem + "\n";
                                    //    sb.Append(strRowValue);
                                    //}
                                    string sCheckinData = "";
                                    string sChuteData = "";

                                    if (ilCheckinSize == 0)
                                    {
                                        if (myAFT[llCurr + 1]["CKIF"] != "" || myAFT[llCurr + 1]["CKIT"] != "")
                                        {
                                            sCheckinData = myAFT[llCurr + 1]["CKIF"] + "-" + myAFT[llCurr + 1]["CKIT"] + ",";
                                        }
                                    }

                                    if (ilChuteSize == 0)
                                    {
                                        sChuteData = ",";
                                    }

                                    for (int i = 0; (i < ilChuteSize || i < ilCheckinSize); i++)
                                    {
                                        if (ilCheckinSize > 0)
                                        {
                                            if (i < ilCheckinSize)
                                            {
                                                sCheckinData += strCheckinData[i] + (i == ilCheckinSize - 1 ? "," : "\r");
                                            }
                                        }

                                        if (ilChuteSize > 0)
                                        {
                                            if (i < ilChuteSize)
                                            {
                                                sChuteData += chuteData[i] + (i == ilChuteSize - 1 ? "," : "\r");
                                            }
                                        }
                                    }
                                    string strRowValue = strValues + sCheckinData + sChuteData + strRem + "\n";
                                    sb.Append(strRowValue);
                                    //---
                                    blDataAppended = true;
                                    //this.AppendChuteFromTo(ref strValues,rows,sb);
                                }
                            }
                            else
                            {
                                if (ilCheckinSize > 0)
                                {
                                    //igu on 19/01/2011
                                    //----
                                    //for(int i = 0 ; i < strCheckinData.Length; i++)
                                    //{
                                    //    string strRowValue = strValues;
                                    //    strRowValue += strCheckinData[i] + ",";
                                    //    strRowValue += strRem + "\n";
                                    //    sb.Append(strRowValue);
                                    //    blDataAppended = true;
                                    //}
                                    string sCheckinData = "";
                                    for (int i = 0; i < ilCheckinSize; i++)
                                    {
                                        sCheckinData += strCheckinData[i] + (i == ilCheckinSize - 1 ? "," : "\r");
                                    }
                                    string strRowValue = strValues + sCheckinData + strRem + "\n";
                                    sb.Append(strRowValue);
                                    blDataAppended = true;
                                    //--igu on 19/01/2011
                                }
                                else
                                {
                                    if (myAFT[llCurr + 1]["CKIF"] != "" || myAFT[llCurr + 1]["CKIT"] != "")
                                    {
                                        strValues += myAFT[llCurr + 1]["CKIF"] + "-" + myAFT[llCurr + 1]["CKIT"];
                                    }
                                    strValues += "," + strRem + "\n";
                                    sb.Append(strValues);
                                    blDataAppended = true;
                                }
                            }
                            if (blDataAppended == false)
                            {
                                strValues += strRem + "\n";
                                sb.Append(strValues);
                            }
                        }
                        else
                        {
                            if (bArrWithinTimeFrame) //igu on 14/04/2011
                            {
                                //igu on 21/04/2011
                                strValues += ",,,,,,,,,,,,,";
                                strValues += sBlankDepDelays;
                                if (bmShowStev == true)
                                {
                                    strValues += ",";
                                }
                                if (bmChuteDisplay == true)
                                {
                                    strValues += ",";
                                }

                                strValues += "\n";
                                sb.Append(strValues);
                            }
                        }
                    }
                    if (strCurrAdid == "D" && strNextAdid == "A")
                    {
                        //The arrival part of rotation
                        bWithinTimeframe = ((myAFT[llCurr + 1][strArrField].CompareTo(strDateFrom) >= 0) &&
                                            (myAFT[llCurr + 1][strArrField].CompareTo(strDateTo) <= 0));
                        if (!chkShowLegWithinTimeframe.Checked || bWithinTimeframe)
                        {
                            bArrWithinTimeFrame = true; //igu on 14/04/2011

                            imArrivals++;

                            string strFLNO = myAFT[llCurr + 1]["FLNO"];
                            if (blButtonICAOChecked == true && strFLNO.Length > 0)
                            {
                                strFLNO = myAFT[llCurr + 1]["ALC3"] + myAFT[llCurr + 1]["FLNO"].Substring(3);
                            }
                            if (strFLNO == "") strFLNO = myAFT[llCurr + 1]["CSGN"];
                            strValues += myAFT[llCurr + 1]["STOA"] + myAFT[llCurr]["STOD"] + ",";
                            strValues += strFLNO + ",";
                            strValues += blButtonICAOChecked == true ? myAFT[llCurr + 1]["ORG4"] + "," : myAFT[llCurr + 1]["ORG3"] + ",";
                            strValues += blButtonICAOChecked == true ? myAFT[llCurr + 1]["VIA4"] + "," : myAFT[llCurr + 1]["VIA3"] + ",";
                            strValues += myAFT[llCurr + 1]["TTYP"] + ",";
                            strValues += Helper.DateString(myAFT[llCurr + 1]["STOA"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr + 1]["ETAI"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr + 1]["TMOA"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr + 1]["LAND"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr + 1]["ONBL"], strDateTimeFormat) + ",";

                            //igu on 22/03/2011
                            strValues += PrepareDelayData(mMaxOfDelays, myAFT[llCurr + 1]);

                            //changes for the PRF 8523
                            if (UT.Hopo == "DXB")
                            {
                                strValues += myAFT[llCurr + 1]["PAXT"] + ",";
                            }
                            else
                            {
                                strPax = GetPaxData(myAFT[llCurr + 1]["URNO"]);
                                if (strPax == "")
                                    strPax = myAFT[llCurr + 1]["PAXT"];
                                strValues += strPax + ",";
                            }
                            strValues += myAFT[llCurr + 1]["PSTA"] + ",";
                            if (bmShowStev == true)
                            {
                                strValues += myAFT[llCurr + 1]["STEV"] + ",";
                            }
                            strGate = myAFT[llCurr + 1]["GTA1"];
                            if (strGate == "") strGate = myAFT[llCurr + 1]["GTA2"];
                            strValues += strGate + ",";
                            strBlt = myAFT[llCurr + 1]["BLT1"];
                            if (strBlt == "") strBlt = myAFT[llCurr + 1]["BLT2"];
                            strValues += strBlt + ",";
                            strValues += myAFT[llCurr + 1]["FTYP"] + "/" + myAFT[llCurr]["FTYP"] + ",";
                            strValues += blButtonICAOChecked == true ? myAFT[llCurr + 1]["ACT5"] + "," : myAFT[llCurr + 1]["ACT3"] + ",";
                            strValues += myAFT[llCurr + 1]["REGN"] + ",";
                        }
                        else
                        {
                            //strValues += ",,,,,,,,,,,,,,,,,"; //igu on 14/04/2011
                            bArrWithinTimeFrame = false; //igu on 14/04/2011
                            strValues += ",,,,,,,,,,,,,," + sBlankArrDelays; //igu on 14/04/2011
                            if (bmShowStev)
                            {
                                strValues += ",";
                            }
                        }
                        //-----------------------------------The departure part of rotation
                        bWithinTimeframe = ((myAFT[llCurr][strDepField].CompareTo(strDateFrom) >= 0) &&
                                            (myAFT[llCurr][strDepField].CompareTo(strDateTo) <= 0));
                        if (!chkShowLegWithinTimeframe.Checked || bWithinTimeframe)
                        {
                            imDepartures++;

                            //igu on 14/04/2011
                            if (!bArrWithinTimeFrame)
                            {
                                strValues += myAFT[llCurr]["FTYP"] + ",";
                                strValues += blButtonICAOChecked == true ? myAFT[llCurr]["ACT5"] + "," : myAFT[llCurr]["ACT3"] + ",";
                                strValues += myAFT[llCurr]["REGN"] + ",";
                            }
                            //
                            string strFLNO = myAFT[llCurr]["FLNO"];
                            if (blButtonICAOChecked == true && strFLNO.Length > 0)
                            {
                                strFLNO = myAFT[llCurr]["ALC3"] + myAFT[llCurr]["FLNO"].Substring(3);
                            }
                            if (strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
                            strValues += strFLNO + ",";
                            strValues += blButtonICAOChecked == true ? myAFT[llCurr]["DES4"] + "," : myAFT[llCurr]["DES3"] + ",";
                            strValues += blButtonICAOChecked == true ? myAFT[llCurr]["VIA4"] + "," : myAFT[llCurr]["VIA3"] + ",";
                            strValues += myAFT[llCurr]["TTYP"] + ",";

                            strValues += Helper.DateString(myAFT[llCurr]["STOD"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr]["ETDI"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr]["SLOT"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr]["OFBL"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFT[llCurr]["AIRB"], strDateTimeFormat) + ",";
                            //strValues += myAFT[llCurr]["DCD1"] + ","; //iguigu
                            //strValues += myAFT[llCurr]["DTD1"] + ",";
                            //strValues += myAFT[llCurr]["DCD2"] + ",";
                            //strValues += myAFT[llCurr]["DTD2"] + ",";
                            strValues += PrepareDelayData(mMaxOfDelays, myAFT[llCurr]); //igu on 19/01/2011
                            //changes for the PRF 8523
                            if (UT.Hopo == "DXB")
                            {
                                strValues += myAFT[llCurr]["PAXT"] + ",";
                            }
                            else
                            {
                                strPax = GetPaxData(myAFT[llCurr]["URNO"]);
                                if (strPax == "")
                                    strPax = myAFT[llCurr]["PAXT"];
                                strValues += strPax + ",";
                            }
                            strValues += myAFT[llCurr]["PSTD"] + ",";
                            if (bmShowStev == true)
                            {
                                strValues += myAFT[llCurr]["STEV"] + ",";
                            }
                            strGate = myAFT[llCurr]["GTD1"];
                            if (strGate == "") strGate = myAFT[llCurr]["GTD2"];
                            strValues += strGate + ",";

                            /*strBlt = myAFT[llCurr]["WRO1"];
                            if(strBlt == "") strBlt = myAFT[llCurr]["WRO2"];
                            strValues += strBlt + ",";*/
                            IRow[] rows = myCCA.RowsByIndexValue("FLNU", myAFT[llCurr]["URNO"]);
                            string[] strCheckinData = GetCheckinCounterAllocationData(rows);
                            int ilCheckinSize = strCheckinData.Length;

                            string strRem = myAFT[llCurr + 1]["REM1"] + "-" + myAFT[llCurr]["REM1"];
                            //						strRem = strRem.Replace("\n", " ");
                            //						strRem = strRem.Replace("\r", " ");
                            bool blDataAppended = false;
                            if (bmChuteDisplay == true)
                            {
                                rows = myCHA.RowsByIndexValue("RURN", myAFT[llCurr]["URNO"]);
                                string[] chuteData = GetChuteData(rows);
                                int ilChuteSize = chuteData.Length;
                                if (ilCheckinSize == 0 && ilChuteSize == 0)
                                {
                                    if (myAFT[llCurr]["CKIF"] != "" || myAFT[llCurr]["CKIT"] != "")
                                    {
                                        strValues += myAFT[llCurr]["CKIF"] + "-" + myAFT[llCurr]["CKIT"];
                                    }
                                    strValues += ",," + strRem + "\n";
                                    sb.Append(strValues);
                                    blDataAppended = true;
                                }
                                else
                                {
                                    //igu on 19/01/2011
                                    //----
                                    //for(int i = 0 ; (i < ilChuteSize || i < ilCheckinSize) ; i++)
                                    //{
                                    //    string strRowValue = strValues;
                                    //    strRowValue += i < ilCheckinSize ? strCheckinData[i] + "," : (i == 0 ? (myAFT[llCurr]["CKIF"] != "" || myAFT[llCurr]["CKIT"] != "" ? myAFT[llCurr]["CKIF"] + "-" + myAFT[llCurr]["CKIT"]+"," : ",") : ",");
                                    //    strRowValue += i < ilChuteSize ? chuteData[i] + "," : ",";
                                    //    strRowValue += strRem + "\n";
                                    //    sb.Append(strRowValue);
                                    //}
                                    string sCheckinData = "";
                                    string sChuteData = "";

                                    if (ilCheckinSize == 0)
                                    {
                                        if (myAFT[llCurr + 1]["CKIF"] != "" || myAFT[llCurr + 1]["CKIT"] != "")
                                        {
                                            sCheckinData = myAFT[llCurr + 1]["CKIF"] + "-" + myAFT[llCurr + 1]["CKIT"] + ",";
                                        }
                                    }

                                    if (ilChuteSize == 0)
                                    {
                                        sChuteData = ",";
                                    }

                                    for (int i = 0; (i < ilChuteSize || i < ilCheckinSize); i++)
                                    {
                                        if (ilCheckinSize > 0)
                                        {
                                            if (i < ilCheckinSize)
                                            {
                                                sCheckinData += strCheckinData[i] + (i == ilCheckinSize - 1 ? "," : "\r");
                                            }
                                        }

                                        if (ilChuteSize > 0)
                                        {
                                            if (i < ilChuteSize)
                                            {
                                                sChuteData += chuteData[i] + (i == ilChuteSize - 1 ? "," : "\r");
                                            }
                                        }
                                    }
                                    string strRowValue = strValues + sCheckinData + sChuteData + strRem + "\n";
                                    sb.Append(strRowValue);
                                    //---
                                    blDataAppended = true;
                                    //this.AppendChuteFromTo(ref strValues,rows,sb);
                                }
                            }
                            else
                            {
                                if (ilCheckinSize > 0)
                                {
                                    //igu on 19/01/2011
                                    //----
                                    //for(int i = 0 ; i < strCheckinData.Length; i++)
                                    //{
                                    //    string strRowValue = strValues;
                                    //    strRowValue += strCheckinData[i] + ",";
                                    //    strRowValue += strRem + "\n";
                                    //    sb.Append(strRowValue);
                                    //    blDataAppended = true;
                                    //}
                                    string sCheckinData = "";
                                    for (int i = 0; i < ilCheckinSize; i++)
                                    {
                                        sCheckinData += strCheckinData[i] + (i == ilCheckinSize - 1 ? "," : "\r");
                                    }
                                    string strRowValue = strValues + sCheckinData + strRem + "\n";
                                    sb.Append(strRowValue);
                                    blDataAppended = true;
                                    //--igu on 19/01/2011
                                }
                                else
                                {
                                    if (myAFT[llCurr]["CKIF"] != "" || myAFT[llCurr]["CKIT"] != "")
                                    {
                                        strValues += myAFT[llCurr]["CKIF"] + "-" + myAFT[llCurr]["CKIT"];
                                    }
                                    strValues += ",," + strRem + "\n";
                                    sb.Append(strValues);
                                    blDataAppended = true;
                                }
                            }
                            if (blDataAppended == false)
                            {
                                strValues += strRem + "\n";
                                sb.Append(strValues);
                            }
                        }
                        else
                        {
                            if (bArrWithinTimeFrame) //igu on 14/04/2011
                            {
                                //igu on 21/04/2011
                                strValues += ",,,,,,,,,,,,,";
                                strValues += sBlankDepDelays;
                                if (bmShowStev == true)
                                {
                                    strValues += ",";
                                }
                                if (bmChuteDisplay == true)
                                {
                                    strValues += ",";
                                }

                                strValues += "\n";
                                sb.Append(strValues);
                            }
                        }
                    }
                }//if(strCurrRkey == strNextRkey)
                else
                {
                    ilStep = 1;
                    if (strCurrAdid == "A")
                    {
                        imArrivals++;
                        string strFLNO = myAFT[llCurr]["FLNO"];
                        if (blButtonICAOChecked == true && strFLNO.Length > 0)
                        {
                            strFLNO = myAFT[llCurr]["ALC3"] + myAFT[llCurr]["FLNO"].Substring(3);
                        }
                        if (strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
                        strValues += myAFT[llCurr]["STOA"] + ",";
                        strValues += strFLNO + ",";
                        strValues += blButtonICAOChecked == true ? myAFT[llCurr]["ORG4"] + "," : myAFT[llCurr]["ORG3"] + ",";
                        strValues += blButtonICAOChecked == true ? myAFT[llCurr]["VIA4"] + "," : myAFT[llCurr]["VIA3"] + ",";
                        strValues += myAFT[llCurr]["TTYP"] + ",";
                        strValues += Helper.DateString(myAFT[llCurr]["STOA"], strDateTimeFormat) + ",";
                        strValues += Helper.DateString(myAFT[llCurr]["ETAI"], strDateTimeFormat) + ",";
                        strValues += Helper.DateString(myAFT[llCurr]["TMOA"], strDateTimeFormat) + ",";
                        strValues += Helper.DateString(myAFT[llCurr]["LAND"], strDateTimeFormat) + ",";
                        strValues += Helper.DateString(myAFT[llCurr]["ONBL"], strDateTimeFormat) + ",";

                        //igu on 22/03/2011
                        strValues += PrepareDelayData(mMaxOfDelays, myAFT[llCurr]);

                        //changes for the PRF 8523
                        if (UT.Hopo == "DXB")
                        {
                            strValues += myAFT[llCurr]["PAXT"] + ",";
                        }
                        else
                        {
                            strPax = GetPaxData(myAFT[llCurr]["URNO"]);
                            if (strPax == "")
                                strPax = myAFT[llCurr]["PAXT"];
                            strValues += strPax + ",";
                        }
                        strValues += myAFT[llCurr]["PSTA"] + ",";
                        strValues += bmShowStev == true ? myAFT[llCurr]["STEV"] + "," : "";
                        strGate = myAFT[llCurr]["GTA1"];
                        if (strGate == "") strGate = myAFT[llCurr]["GTA2"];
                        strValues += strGate + ",";
                        strBlt = myAFT[llCurr]["BLT1"];
                        if (strBlt == "") strBlt = myAFT[llCurr]["BLT2"];
                        strValues += strBlt + ",";
                        strValues += myAFT[llCurr]["FTYP"] + ",";
                        strValues += blButtonICAOChecked == true ? myAFT[llCurr]["ACT5"] + "," : myAFT[llCurr]["ACT3"] + ",";
                        strValues += myAFT[llCurr]["REGN"] + ",";
                        strValues += bmShowStev == true ? "," : "";
                        strValues += bmChuteDisplay == true ? "," : "";
                        //strValues += ",,,,,,,,,,,,,,,,," + myAFT[llCurr]["REM1"] + "\n"; //igu on 07/03/2011
                        strValues += ",,,,,,,,,,,,," + sBlankDepDelays + myAFT[llCurr]["REM1"] + "\n"; //igu on 16/03/2011
                        sb.Append(strValues);
                    }
                    if (strCurrAdid == "D")
                    {
                        imDepartures++;
                        strValues += myAFT[llCurr]["STOD"] + ",,,,,,,,,,,,,,";
                        strValues += sBlankArrDelays; //igu on 22/03/2011                        
                        strValues += bmShowStev == true ? "," : "";
                        strValues += myAFT[llCurr]["FTYP"] + ",";//,,";
                        strValues += blButtonICAOChecked == true ? myAFT[llCurr]["ACT5"] + "," : myAFT[llCurr]["ACT3"] + ",";
                        strValues += myAFT[llCurr]["REGN"] + ",";
                        string strFLNO = myAFT[llCurr]["FLNO"];
                        if (blButtonICAOChecked == true && strFLNO.Length > 0)
                        {
                            strFLNO = myAFT[llCurr]["ALC3"] + myAFT[llCurr]["FLNO"].Substring(3);
                        }
                        if (strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
                        //strValues += myAFT[llCurr]["STOD"] + ",";
                        strValues += strFLNO + ",";
                        strValues += blButtonICAOChecked == true ? myAFT[llCurr]["DES4"] + "," : myAFT[llCurr]["DES3"] + ",";
                        strValues += blButtonICAOChecked == true ? myAFT[llCurr]["VIA4"] + "," : myAFT[llCurr]["VIA3"] + ",";
                        strValues += myAFT[llCurr]["TTYP"] + ",";

                        strValues += Helper.DateString(myAFT[llCurr]["STOD"], strDateTimeFormat) + ",";
                        strValues += Helper.DateString(myAFT[llCurr]["ETDI"], strDateTimeFormat) + ",";
                        strValues += Helper.DateString(myAFT[llCurr]["SLOT"], strDateTimeFormat) + ",";
                        strValues += Helper.DateString(myAFT[llCurr]["OFBL"], strDateTimeFormat) + ",";
                        strValues += Helper.DateString(myAFT[llCurr]["AIRB"], strDateTimeFormat) + ",";

                        //strValues += myAFT[llCurr]["DCD1"] + ","; //iguigu
                        //strValues += myAFT[llCurr]["DTD1"] + ",";
                        //strValues += myAFT[llCurr]["DCD2"] + ",";
                        //strValues += myAFT[llCurr]["DTD2"] + ",";
                        strValues += PrepareDelayData(mMaxOfDelays, myAFT[llCurr]); //igu on 19/01/2011
                        //changes for the PRF 8523
                        if (UT.Hopo == "DXB")
                        {
                            strValues += myAFT[llCurr]["PAXT"] + ",";
                        }
                        else
                        {
                            strPax = GetPaxData(myAFT[llCurr]["URNO"]);
                            if (strPax == "")
                                strPax = myAFT[llCurr]["PAXT"];
                            strValues += strPax + ",";
                        }
                        strValues += myAFT[llCurr]["PSTD"] + ",";
                        strValues += bmShowStev == true ? myAFT[llCurr]["STEV"] + "," : "";
                        strGate = myAFT[llCurr]["GTD1"];
                        if (strGate == "") strGate = myAFT[llCurr]["GTD2"];
                        strValues += strGate + ",";

                        /*strBlt = myAFT[llCurr]["WRO1"];
                        if(strBlt == "") strBlt = myAFT[llCurr]["WRO2"];
                        strValues += strBlt + ",";*/

                        IRow[] rows = myCCA.RowsByIndexValue("FLNU", myAFT[llCurr]["URNO"]);
                        string[] strCheckinData = GetCheckinCounterAllocationData(rows);
                        int ilCheckinSize = strCheckinData.Length;

                        string strRem = myAFT[llCurr]["REM1"];
                        bool blDataAppended = false;
                        if (bmChuteDisplay == true)
                        {
                            rows = myCHA.RowsByIndexValue("RURN", myAFT[llCurr]["URNO"]);
                            string[] chuteData = GetChuteData(rows);
                            int ilChuteSize = chuteData.Length;
                            if (ilCheckinSize == 0 && ilChuteSize == 0)
                            {
                                if (myAFT[llCurr]["CKIF"] != "" || myAFT[llCurr]["CKIT"] != "")
                                {
                                    strValues += myAFT[llCurr]["CKIF"] + "-" + myAFT[llCurr]["CKIT"];
                                }
                                strValues += ",," + strRem + "\n";
                                sb.Append(strValues);
                                blDataAppended = true;
                            }
                            else
                            {
                                //igu on 19/01/2011
                                //----
                                //for(int i = 0 ; (i < ilChuteSize || i < ilCheckinSize) ; i++)
                                //{
                                //    string strRowValue = strValues;
                                //    strRowValue += i < ilCheckinSize  ? strCheckinData[i] + "," : (i == 0 ? (myAFT[llCurr]["CKIF"] != "" || myAFT[llCurr]["CKIT"] != "" ? myAFT[llCurr]["CKIF"] + "-" + myAFT[llCurr]["CKIT"]+"," : ",") : ",");
                                //    strRowValue += i < ilChuteSize ? chuteData[i] + "," : ",";
                                //    strRowValue += strRem + "\n";
                                //    sb.Append(strRowValue);
                                //}
                                string sCheckinData = "";
                                string sChuteData = "";

                                if (ilCheckinSize == 0)
                                {
                                    if (myAFT[llCurr + 1]["CKIF"] != "" || myAFT[llCurr + 1]["CKIT"] != "")
                                    {
                                        sCheckinData = myAFT[llCurr + 1]["CKIF"] + "-" + myAFT[llCurr + 1]["CKIT"] + ",";
                                    }
                                }

                                if (ilChuteSize == 0)
                                {
                                    sChuteData = ",";
                                }

                                for (int i = 0; (i < ilChuteSize || i < ilCheckinSize); i++)
                                {
                                    if (ilCheckinSize > 0)
                                    {
                                        if (i < ilCheckinSize)
                                        {
                                            sCheckinData += strCheckinData[i] + (i == ilCheckinSize - 1 ? "," : "\r");
                                        }
                                    }

                                    if (ilChuteSize > 0)
                                    {
                                        if (i < ilChuteSize)
                                        {
                                            sChuteData += chuteData[i] + (i == ilChuteSize - 1 ? "," : "\r");
                                        }
                                    }
                                }
                                string strRowValue = strValues + sCheckinData + sChuteData + strRem + "\n";
                                sb.Append(strRowValue);
                                //---
                                blDataAppended = true;
                                //this.AppendChuteFromTo(ref strValues,rows,sb);
                            }
                        }
                        else
                        {
                            if (ilCheckinSize > 0)
                            {
                                //igu on 19/01/2011
                                //----
                                //for(int i = 0 ; i < strCheckinData.Length; i++)
                                //{
                                //    string strRowValue = strValues;
                                //    strRowValue += strCheckinData[i] + ",";
                                //    strRowValue += strRem + "\n";
                                //    sb.Append(strRowValue);
                                //    blDataAppended = true;
                                //}
                                string sCheckinData = "";
                                for (int i = 0; i < ilCheckinSize; i++)
                                {
                                    sCheckinData += strCheckinData[i] + (i == ilCheckinSize - 1 ? "," : "\r");
                                }
                                string strRowValue = strValues + sCheckinData + strRem + "\n";
                                sb.Append(strRowValue);
                                blDataAppended = true;
                                //--igu on 19/01/2011
                            }
                            else
                            {
                                if (myAFT[llCurr]["CKIF"] != "" || myAFT[llCurr]["CKIT"] != "")
                                {
                                    strValues += myAFT[llCurr]["CKIF"] + "-" + myAFT[llCurr]["CKIT"];
                                }
                                strValues += ",," + strRem + "\n";
                                sb.Append(strValues);
                                blDataAppended = true;
                            }
                        }
                        if (blDataAppended == false)
                        {
                            strValues += strRem + "\n";
                            sb.Append(strValues);
                        }
                    }
                }
                llCurr = llCurr + ilStep;
            }//while(llCurr < myAFT.Count)
            if (strReportOrExcel.Equals("Excel") == true)
            {
                tabExcelResult.InsertBuffer(sb.ToString(), "\n");
                tabExcelResult.Sort("0", true, true);
            }
            else if (strReportOrExcel.Equals("Report") == true)
            {
                tabResult.InsertBuffer(sb.ToString(), "\n");
                tabResult.Sort("0", true, true);
                tabResult.Refresh();
                lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
            }
            lblProgress.Text = "";
            progressBar1.Visible = false;
            //tabResult.Sort("4,19", true, true);
        }

        private string PrepareDelayData(DelayCount maxDelayCnt, IRow flightRow)
        {
            string strValues = "";
            int cnt = 0;

            //igu on 22/03/2011
            int delayCodeCount = 0;

            if (flightRow["ADID"] == "A")
            {
                delayCodeCount = maxDelayCnt.ArrDelayCodeCount;
            }
            else
            {
                delayCodeCount = maxDelayCnt.DepDelayCodeCount;
            }
            FlightDelays flightDelays = flightDelaysCollection.GetFlightDelays(flightRow["URNO"]);
            if (flightDelays == null)
            {
                strValues = new String(',', delayCodeCount * 3);
            }
            else
            {
                List<DelayData> lsDelays = flightDelays.DelayDataCollection.DelayDataList;
                for (int i = 0; i < delayCodeCount * 2; i++)
                {
                    DelayData delay = null;
                    if (cnt < lsDelays.Count)
                    {
                        delay = lsDelays[cnt];
                    }

                    if (i < delayCodeCount) //MainDelay
                    {
                        if (delay == null)
                        {
                            strValues += string.Format("{0},{1},", "", DelayData.DEFAULT_DURA);
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(delay.Subcode))
                            {
                                strValues += string.Format("{0},{1},", delay.FullCode, delay.Duration);
                                cnt++;
                            }
                            else
                            {
                                strValues += string.Format("{0},{1},", "", DelayData.DEFAULT_DURA);
                            }
                        }
                    }
                    else //SubDelay
                    {
                        if (delay == null)
                        {
                            strValues += string.Format("{0},", "");
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(delay.Subcode))
                            {
                                strValues += string.Format("{0},", "");
                            }
                            else
                            {
                                strValues += string.Format("{0},", delay.FullCode);
                                cnt++;
                            }
                        }
                    }
                }
            }

            return strValues;
        }

        private string[] GetCheckinCounterAllocationData(IRow[] rows)
        {
            ArrayList myRows = new ArrayList(rows);

            for (int i = 0; i < myRows.Count; i++)
            {
                ((IRow)myRows[i])["CICF"] = ((IRow)myRows[i])["CKIC"];
                ((IRow)myRows[i])["CICT"] = ((IRow)myRows[i])["CKIC"];
            }

            for (int i = 0; i < myRows.Count; i++)
            {
                int ilCicf = CalculateIntValueOfString(((IRow)myRows[i])["CICF"]);
                int ilCict = CalculateIntValueOfString(((IRow)myRows[i])["CICT"]);
                for (int j = 0; j < myRows.Count; j++)
                {
                    if (((IRow)myRows[i])["CTYP"] == ((IRow)myRows[j])["CTYP"] &&
                        ((IRow)myRows[i])["CKBS"] == ((IRow)myRows[j])["CKBS"] &&
                        ((IRow)myRows[i])["CKES"] == ((IRow)myRows[j])["CKES"])
                    {
                        if (CalculateIntValueOfString(((IRow)myRows[j])["CICT"]) + 1 == ilCicf ||
                            CalculateIntValueOfString(((IRow)myRows[j])["CICF"]) - 1 == ilCict)
                        {
                            if (CalculateIntValueOfString(((IRow)myRows[j])["CICT"]) + 1 == ilCicf)
                            {
                                ((IRow)myRows[i])["CICF"] = ((IRow)myRows[j])["CICF"];
                            }
                            else
                            {
                                ((IRow)myRows[i])["CICT"] = ((IRow)myRows[j])["CICT"];
                            }

                            myRows.RemoveAt(j);
                            if (j < i)
                                i--;

                            i--;
                            break;
                        }
                    }
                }
            }

            string[] checkinData = new string[myRows.Count];
            for (int i = 0; i < myRows.Count; i++)
            {
                checkinData[i] = ((IRow)myRows[i])["CICF"] + "-" + ((IRow)myRows[i])["CICT"];
            }
            return checkinData;
        }

        private string[] GetChuteData(IRow[] rows)
        {
            // copy members to array list
            ArrayList myRows = new ArrayList(rows);
            // initialize members
            for (int i = 0; i < myRows.Count; i++)
            {
                ((IRow)myRows[i])["CHUF"] = ((IRow)myRows[i])["LNAM"];
                ((IRow)myRows[i])["CHUT"] = ((IRow)myRows[i])["LNAM"];
            }

            for (int i = 0; i < myRows.Count; i++)
            {
                int ilCHUF = CalculateIntValueOfString(((IRow)myRows[i])["CHUF"]);
                int ilCHUT = CalculateIntValueOfString(((IRow)myRows[i])["CHUT"]);
                for (int j = 0; j < myRows.Count; j++)
                {
                    if (((IRow)myRows[i])["LTYP"] == ((IRow)myRows[j])["LTYP"] &&
                        ((IRow)myRows[i])["STOB"] == ((IRow)myRows[j])["STOB"] &&
                        ((IRow)myRows[i])["STOE"] == ((IRow)myRows[j])["STOE"])
                    {
                        if (CalculateIntValueOfString(((IRow)myRows[j])["CHUT"]) + 1 == ilCHUF ||
                            CalculateIntValueOfString(((IRow)myRows[j])["CHUF"]) - 1 == ilCHUT)
                        {
                            if (CalculateIntValueOfString(((IRow)myRows[j])["CHUT"]) + 1 == ilCHUF)
                            {
                                ((IRow)myRows[i])["CHUF"] = ((IRow)myRows[j])["CHUF"];
                            }
                            else
                            {
                                ((IRow)myRows[i])["CHUT"] = ((IRow)myRows[j])["CHUT"];
                            }

                            myRows.RemoveAt(j);
                            if (j < i)
                                i--;
                            i--;
                            break;
                        }
                    }
                }
            }

            string[] chuteData = new string[myRows.Count];
            for (int i = 0; i < myRows.Count; i++)
            {
                chuteData[i] = ((IRow)myRows[i])["CHUF"] + "-" + ((IRow)myRows[i])["CHUT"];
            }
            return chuteData;
        }

        private string GetPaxData(string strAftUrno)
        {
            string strLDM = "";
            string strMVT = "";
            string strKRI = "";

            IRow[] rows = myLOA.RowsByIndexValue("FLNU", strAftUrno);
            for (int i = 0; i < rows.Length; i++)
            {
                if (rows[i]["DSSN"] == "USR")
                    return rows[i]["VALU"];
                if (rows[i]["DSSN"] == "LDM")
                    strLDM = rows[i]["VALU"];
                if (rows[i]["DSSN"] == "MVT")
                    strMVT = rows[i]["VALU"];
                if (UT.Hopo == "WAW")
                {
                    //Changed for the PRF 8523
                    if (rows[i]["DSSN"] == "KRI")
                        strKRI = rows[i]["VALU"];
                }
            }
            if (strLDM != "") return strLDM;
            if (strMVT != "") return strMVT;
            if (strKRI != "") return strKRI;
            return "";
        }
        /// <summary>
        /// Calls the generic report. The contructor must have the Tab 
        /// control and the reports headers as well as the types for
        /// each column to enable the reports to transform data, e.g.
        /// the data/time formatting. The lengths of the columns are
        /// dynamically read out of the tab control's header.
        /// </summary>
        private void RunReport()
        {
            string strSubheader = "";
            imTotalFlights = imArrivals + imDepartures;
            string strSubHeader = "";
            strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " +
                dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
            strSubHeader += "       (Flights: " + imTotalFlights.ToString();
            strSubHeader += " ARR: " + imArrivals.ToString();
            strSubHeader += " DEP: " + imDepartures.ToString() + ")";

            strReportHeader = frmMain.strPrefixVal + "Daily Flight Log";
            strReportSubHeader = strSubHeader;
            rptA3_Landscape rpt = new rptA3_Landscape(tabResult, strReportHeader, strReportSubHeader, "", 8);
            rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLensRotation);
            rpt.SetExcelExportTabResult(tabExcelResult);
            //rpt.TextBoxCanGrow = false;

            frmPrintPreview frm = new frmPrintPreview(rpt);
            if (Helper.UseSpreadBuilder() == true)
            {
                activeReport = rpt;
                frm.UseSpreadBuilder(true);
                frm.GetBtnExcelExport().Click += new EventHandler(frmDaily_FlightLog_Click);
            }
            else
            {
                frm.UseSpreadBuilder(false);
            }
            frm.Show();
        }

        /// <summary>
        /// Start the print preview without data selection.
        /// This can be used to adjust the column widths and
        /// the sorting of the tab before printing.
        /// </summary>
        /// <param name="sender">Originator is the form</param>
        /// <param name="e">Event arguments.</param>
        private void btnPrintPreview_Click(object sender, System.EventArgs e)
        {
            RunReport();
        }


        /// <summary>
        /// Loads data and dirctly calls the print preview.
        /// </summary>
        /// <param name="sender">Originator is the form</param>
        /// <param name="e">Event arguments.</param>
        private void btnLoadPrint_Click(object sender, System.EventArgs e)
        {
            string strError = ValidateUserEntry();
            if (strError != "")
            {
                MessageBox.Show(this, strError);
                return; // Do nothing due to errors
            }
            this.Cursor = Cursors.WaitCursor;
            LoadReportData();
            PrepareReportDataRotations("Report");
            PrepareReportDataRotations("Excel");
            RunReport();
            this.Cursor = Cursors.Arrow;
        }
        /// <summary>
        /// Sent by the tab control when the user double click.
        /// This is necessary for sorting.
        /// </summary>
        /// <param name="sender">The Tab control</param>
        /// <param name="e">Double click event parameters.</param>
        private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
        {
            if (e.lineNo == -1)
            {
                if (e.colNo == tabResult.CurrentSortColumn)
                {
                    if (tabResult.SortOrderASC == true)
                    {
                        tabResult.Sort(e.colNo.ToString(), false, true);
                        tabExcelResult.Sort(e.colNo.ToString(), false, true);
                    }
                    else
                    {
                        tabResult.Sort(e.colNo.ToString(), true, true);
                        tabExcelResult.Sort(e.colNo.ToString(), true, true);
                    }
                }
                else
                {
                    tabResult.Sort(e.colNo.ToString(), true, true);
                    tabExcelResult.Sort(e.colNo.ToString(), true, true);
                }
                tabResult.Refresh();
            }
        }

        private void frmDaily_FlightLog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            myDB.Unbind("AFT");
            myDB.Unbind("LOA");
            myDB.Unbind("CHA");
        }

        private void btnLens_Click(object sender, System.EventArgs e)
        {
            txtLens.Text = tabResult.HeaderLengthString;
        }

        /// <summary>
        /// This function returns the int value of a string.
        /// </summary>
        /// <param name="strFlightValues"></param>
        /// <returns></returns>
        private int CalculateIntValueOfString(string strFlightValues)
        {
            string strNew = "";
            int total = 0;
            if (IsAlpha(strFlightValues))
            {
                char ab;
                IEnumerator strFltValEnum = strFlightValues.GetEnumerator();
                int CharCount = 0;
                while (strFltValEnum.MoveNext())
                {
                    CharCount++;
                    ab = (char)strFltValEnum.Current;
                    if (Char.IsNumber(ab))
                        strNew += ab;
                }
                if (string.IsNullOrEmpty(strNew))//mzi on 10 May 2013
                    total = 0;
                else
                    total = int.Parse(strNew);
            }
            else
            {
                total = int.Parse(strFlightValues);
            }
            return total;
        }
        /// <summary>
        /// Checks if a character is Alphabet or Numeric.
        /// </summary>
        /// <param name="strToCheck"></param>
        /// <returns></returns>
        public bool IsAlpha(string strToCheck)
        {
            Regex objAlphaPattern = new Regex("[A-Z]");

            return objAlphaPattern.IsMatch(strToCheck);
        }

        private void radioButtonSTASTD_CheckedChanged(object sender, System.EventArgs e)
        {
            if (tabResult.GetLineCount() > 0 && this.radioButtonSTASTD.Checked == true)
            {
                LoadAndPrepareData();
            }
        }

        private void radioButtonONBLOFBL_CheckedChanged(object sender, System.EventArgs e)
        {
            if (tabResult.GetLineCount() > 0 && this.radioButtonONBLOFBL.Checked == true)
            {
                LoadAndPrepareData();
            }
        }

        private void radioButtonLANDAIRB_CheckedChanged(object sender, System.EventArgs e)
        {
            if (tabResult.GetLineCount() > 0 && this.radioButtonLANDAIRB.Checked == true)
            {
                LoadAndPrepareData();
            }
        }

        private void radioButtonIATA_CheckedChanged(object sender, System.EventArgs e)
        {
            if (tabResult.GetLineCount() > 0 && this.radioButtonIATA.Checked == true)
            {
                LoadAndPrepareData();
            }
        }

        private void radioButtonICAO_CheckedChanged(object sender, System.EventArgs e)
        {
            if (tabResult.GetLineCount() > 0 && this.radioButtonICAO.Checked == true)
            {
                LoadAndPrepareData();
            }
        }
        private void LoadAndPrepareData()
        {
            LoadReportData();
            PrepareReportDataRotations("Report");
            PrepareReportDataRotations("Excel");
        }

        private void InitializeMouseEvents()
        {
            ((System.Windows.Forms.Control)tabResult).MouseWheel += new System.Windows.Forms.MouseEventHandler(this.frmDaily_FlightLog_MouseWheel);
            ((System.Windows.Forms.Control)tabResult).MouseEnter += new System.EventHandler(this.frmDaily_FlightLog_MouseEnter);
            ((System.Windows.Forms.Control)tabResult).MouseLeave += new System.EventHandler(this.frmDaily_FlightLog_MouseLeave);

        }

        private void frmDaily_FlightLog_MouseWheel(object sender, MouseEventArgs e)
        {
            if (bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
            {
                return;
            }
            System.Windows.Forms.Control ba = (System.Windows.Forms.Control)sender;

            if (e.Delta > 0 && tabResult.GetVScrollPos() > 0)
            {
                tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
            }
            else if (e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
            {
                tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
            }
        }

        private void frmDaily_FlightLog_MouseEnter(object sender, EventArgs e)
        {
            bmMouseInsideTabControl = true;
        }

        private void frmDaily_FlightLog_MouseLeave(object sender, EventArgs e)
        {
            bmMouseInsideTabControl = false;
        }

        private void frmDaily_FlightLog_Click(object sender, EventArgs e)
        {
            Helper.ExportToExcel(tabExcelResult, strReportHeader, strReportSubHeader, strExcelTabHeaderLensRotation, 8, 1, activeReport);
        }

        private void buttonHelp_Click(object sender, System.EventArgs e)
        {
            Helper.ShowHelpFile(this);
        }

        private void frmDaily_FlightLog_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            Helper.ShowHelpFile(this);
        }
    }
}
