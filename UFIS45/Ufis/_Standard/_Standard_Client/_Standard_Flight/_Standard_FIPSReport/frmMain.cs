#region ###### Source Code Information ########
/********************
  Please take note that if the Developer want to add new Report button, Please follow this 
 * configuration steps also:
 * 1- Add the report no at the Ceda.ini > FIPSReport Section> REPORT_LIST Tag 
 * 2- Add the report checking case at the ConstructReportList function  
 * 3- Update the Report list at the FipsReport_AccessRight excel file of Ticket No 2496
 * 4- Set the latest Tab Index No for that button.
 
*/
#endregion

using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Ufis.Utils;
using Ufis.Data;
using System.Diagnostics;

namespace FIPS_Reports
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class frmMain : System.Windows.Forms.Form
    {
        #region ### Initialist Variables ###
         #region My Members
         /// <summary>
        /// The one and only IDatabase member reference within 
        /// the application. This is reused in all other modules.
        /// </summary>
        private IDatabase myDB;
        private ITable myCfg;
        private int isCustomerPVG = -1;
        public string[] args;
        private int iXLocation = 0;
        private int iYLocation = 0;
         int iONewX = 0,iONewY = 0,iOGap = 0,iOCYCnt = 0, iOYCnt = 18;
        #endregion My Members
        public static string strPrefixVal = "";
        public static string strPositionUsage = string.Empty; //Position Usage Report checking to use old or new
        public static string strReportList = string.Empty; //Report String List
        public static string iniFilePath = string.Empty;
        Boolean bShowReport = false;
        bool bMAirport = false;     //check for Multi Airport flag at Ceda Ini
        string sCurrentScenario = string.Empty; //For showing current scenario at Main UI
        public static Dictionary<string,string> _ReportList;
      
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Button btnFlightsByNatureCode;
        private System.Windows.Forms.CheckBox cbTimeInLocal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.Button btnCounterSchedule;
        private System.Windows.Forms.Button btnStatisticCancelledFlights;
        private System.Windows.Forms.Button btnBeltAllocations;
        private System.Windows.Forms.Button btnOvernightFlights;
        private System.Windows.Forms.Button btnInventory;
        private System.Windows.Forms.Button btnGPUUsage;
        private System.Windows.Forms.Button btnDailyFlightLog;
        private System.Windows.Forms.Button btnSpotUsingRate;
        private System.Windows.Forms.Button btnStatistAccToArrDep;
        private System.Windows.Forms.Button btnStatistCancelledFlightSummary;
        private System.Windows.Forms.Button btnStatistOvernightArrSummary;
        private System.Windows.Forms.Button btnStatistAccArrDep;
        private System.Windows.Forms.Button btnAircraftMovementsPerHour;
        private System.Windows.Forms.Button btnSeatStatisDomIntComb;
        private System.Windows.Forms.Button btnStatistDomesticIntComb;
        private System.Windows.Forms.Button btnStatistRushHour;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button7;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Button button_ConfigLoadPax;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button buttonHelp;
        private System.Windows.Forms.Button buttonChuteAllocation;
        private Button btnNightOperations;
        private Button btnNightOperationsSummary;
        private Button btnDeIcing;
        private Button btnFlightAllocation;
        private Button btnGateUsage;
        private Button btnFlightViewer;
        private Button butBeltAllocation;
        private Button btnpiechart;
        private Button btnAircraftLanding;
        private Button btnAircraftOnGround;
        private Button btnTowingReport;
        private Label lblScenario;
        private ComboBox cboScenario;
        private Label lblCurrentScenario;
        private AxAATLOGINLib.AxAatLogin LoginControl;

        /// <summary>
        /// Constructor of the main form.
        /// </summary>
        public frmMain()
        {
             //
             // Required for Windows Form Designer support
             //
             InitializeComponent();

             //
             // TODO: Add any constructor code after InitializeComponent call
             //
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
             if (disposing)
             {
                  if (components != null)
                  {
                       components.Dispose();
                  }
             }
             base.Dispose(disposing);
        }
       #endregion        

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
             this.components = new System.ComponentModel.Container();
             System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
             this.panel1 = new System.Windows.Forms.Panel();
             this.lblCurrentScenario = new System.Windows.Forms.Label();
             this.lblServer = new System.Windows.Forms.Label();
             this.label3 = new System.Windows.Forms.Label();
             this.lblVersion = new System.Windows.Forms.Label();
             this.label2 = new System.Windows.Forms.Label();
             this.pictureBox1 = new System.Windows.Forms.PictureBox();
             this.LoginControl = new AxAATLOGINLib.AxAatLogin();
             this.cbTimeInLocal = new System.Windows.Forms.CheckBox();
             this.imageList1 = new System.Windows.Forms.ImageList(this.components);
             this.panel2 = new System.Windows.Forms.Panel();
             this.cboScenario = new System.Windows.Forms.ComboBox();
             this.lblScenario = new System.Windows.Forms.Label();
             this.btnTowingReport = new System.Windows.Forms.Button();
             this.btnAircraftLanding = new System.Windows.Forms.Button();
             this.btnAircraftOnGround = new System.Windows.Forms.Button();
             this.btnpiechart = new System.Windows.Forms.Button();
             this.butBeltAllocation = new System.Windows.Forms.Button();
             this.btnFlightViewer = new System.Windows.Forms.Button();
             this.btnGateUsage = new System.Windows.Forms.Button();
             this.btnFlightAllocation = new System.Windows.Forms.Button();
             this.btnDeIcing = new System.Windows.Forms.Button();
             this.btnNightOperationsSummary = new System.Windows.Forms.Button();
             this.btnNightOperations = new System.Windows.Forms.Button();
             this.buttonChuteAllocation = new System.Windows.Forms.Button();
             this.buttonHelp = new System.Windows.Forms.Button();
             this.button_ConfigLoadPax = new System.Windows.Forms.Button();
             this.button8 = new System.Windows.Forms.Button();
             this.button7 = new System.Windows.Forms.Button();
             this.button2 = new System.Windows.Forms.Button();
             this.button1 = new System.Windows.Forms.Button();
             this.btnSeatStatisDomIntComb = new System.Windows.Forms.Button();
             this.btnStatistDomesticIntComb = new System.Windows.Forms.Button();
             this.btnStatistRushHour = new System.Windows.Forms.Button();
             this.btnAircraftMovementsPerHour = new System.Windows.Forms.Button();
             this.btnStatistAccArrDep = new System.Windows.Forms.Button();
             this.btnStatistOvernightArrSummary = new System.Windows.Forms.Button();
             this.btnStatistCancelledFlightSummary = new System.Windows.Forms.Button();
             this.btnStatistAccToArrDep = new System.Windows.Forms.Button();
             this.btnSpotUsingRate = new System.Windows.Forms.Button();
             this.btnDailyFlightLog = new System.Windows.Forms.Button();
             this.btnGPUUsage = new System.Windows.Forms.Button();
             this.btnInventory = new System.Windows.Forms.Button();
             this.btnOvernightFlights = new System.Windows.Forms.Button();
             this.btnBeltAllocations = new System.Windows.Forms.Button();
             this.button4 = new System.Windows.Forms.Button();
             this.btnStatisticCancelledFlights = new System.Windows.Forms.Button();
             this.btnCounterSchedule = new System.Windows.Forms.Button();
             this.label1 = new System.Windows.Forms.Label();
             this.btnFlightsByNatureCode = new System.Windows.Forms.Button();
             this.panel1.SuspendLayout();
             ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
             ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).BeginInit();
             this.panel2.SuspendLayout();
             this.SuspendLayout();
             // 
             // panel1
             // 
             this.panel1.BackColor = System.Drawing.Color.White;
             this.panel1.Controls.Add(this.lblCurrentScenario);
             this.panel1.Controls.Add(this.lblServer);
             this.panel1.Controls.Add(this.label3);
             this.panel1.Controls.Add(this.lblVersion);
             this.panel1.Controls.Add(this.label2);
             this.panel1.Controls.Add(this.pictureBox1);
             this.panel1.Controls.Add(this.LoginControl);
             this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
             this.panel1.Location = new System.Drawing.Point(0, 0);
             this.panel1.Name = "panel1";
             this.panel1.Size = new System.Drawing.Size(722, 64);
             this.panel1.TabIndex = 0;
             // 
             // lblCurrentScenario
             // 
             this.lblCurrentScenario.AutoSize = true;
             this.lblCurrentScenario.Location = new System.Drawing.Point(80, 45);
             this.lblCurrentScenario.Name = "lblCurrentScenario";
             this.lblCurrentScenario.Size = new System.Drawing.Size(0, 13);
             this.lblCurrentScenario.TabIndex = 6;
             // 
             // lblServer
             // 
             this.lblServer.Location = new System.Drawing.Point(128, 4);
             this.lblServer.Name = "lblServer";
             this.lblServer.Size = new System.Drawing.Size(136, 16);
             this.lblServer.TabIndex = 5;
             // 
             // label3
             // 
             this.label3.Location = new System.Drawing.Point(80, 4);
             this.label3.Name = "label3";
             this.label3.Size = new System.Drawing.Size(48, 16);
             this.label3.TabIndex = 4;
             this.label3.Text = "Server:";
             // 
             // lblVersion
             // 
             this.lblVersion.Location = new System.Drawing.Point(608, 0);
             this.lblVersion.Name = "lblVersion";
             this.lblVersion.Size = new System.Drawing.Size(96, 23);
             this.lblVersion.TabIndex = 2;
             this.lblVersion.Text = "Version: 4.5.1.2";
             this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             // 
             // label2
             // 
             this.label2.Location = new System.Drawing.Point(480, 48);
             this.label2.Name = "label2";
             this.label2.Size = new System.Drawing.Size(248, 16);
             this.label2.TabIndex = 1;
             this.label2.Text = "©2005, 2008 UFIS Airport Solutions GmbH";
             this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             // 
             // pictureBox1
             // 
             this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
             this.pictureBox1.Location = new System.Drawing.Point(8, 0);
             this.pictureBox1.Name = "pictureBox1";
             this.pictureBox1.Size = new System.Drawing.Size(64, 64);
             this.pictureBox1.TabIndex = 0;
             this.pictureBox1.TabStop = false;
             // 
             // LoginControl
             // 
             this.LoginControl.Enabled = true;
             this.LoginControl.Location = new System.Drawing.Point(39, 0);
             this.LoginControl.Name = "LoginControl";
             this.LoginControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("LoginControl.OcxState")));
             this.LoginControl.RightToLeft = false;
             this.LoginControl.Size = new System.Drawing.Size(19, 50);
             this.LoginControl.TabIndex = 0;
             this.LoginControl.Text = null;
             this.LoginControl.Visible = false;
             // 
             // cbTimeInLocal
             // 
             this.cbTimeInLocal.Checked = true;
             this.cbTimeInLocal.CheckState = System.Windows.Forms.CheckState.Checked;
             this.cbTimeInLocal.Location = new System.Drawing.Point(19, 42);
             this.cbTimeInLocal.Name = "cbTimeInLocal";
             this.cbTimeInLocal.Size = new System.Drawing.Size(124, 16);
             this.cbTimeInLocal.TabIndex = 2;
             this.cbTimeInLocal.Text = "Times in Local Time";
             this.cbTimeInLocal.CheckedChanged += new System.EventHandler(this.cbTimeInLocal_CheckedChanged);
             // 
             // imageList1
             // 
             this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
             this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
             this.imageList1.Images.SetKeyName(0, "");
             // 
             // panel2
             // 
             this.panel2.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
             this.panel2.Controls.Add(this.cboScenario);
             this.panel2.Controls.Add(this.lblScenario);
             this.panel2.Controls.Add(this.btnTowingReport);
             this.panel2.Controls.Add(this.btnAircraftLanding);
             this.panel2.Controls.Add(this.btnAircraftOnGround);
             this.panel2.Controls.Add(this.btnpiechart);
             this.panel2.Controls.Add(this.butBeltAllocation);
             this.panel2.Controls.Add(this.btnFlightViewer);
             this.panel2.Controls.Add(this.btnGateUsage);
             this.panel2.Controls.Add(this.btnFlightAllocation);
             this.panel2.Controls.Add(this.btnDeIcing);
             this.panel2.Controls.Add(this.btnNightOperationsSummary);
             this.panel2.Controls.Add(this.btnNightOperations);
             this.panel2.Controls.Add(this.buttonChuteAllocation);
             this.panel2.Controls.Add(this.buttonHelp);
             this.panel2.Controls.Add(this.button_ConfigLoadPax);
             this.panel2.Controls.Add(this.button8);
             this.panel2.Controls.Add(this.button7);
             this.panel2.Controls.Add(this.button2);
             this.panel2.Controls.Add(this.button1);
             this.panel2.Controls.Add(this.btnSeatStatisDomIntComb);
             this.panel2.Controls.Add(this.btnStatistDomesticIntComb);
             this.panel2.Controls.Add(this.btnStatistRushHour);
             this.panel2.Controls.Add(this.btnAircraftMovementsPerHour);
             this.panel2.Controls.Add(this.btnStatistAccArrDep);
             this.panel2.Controls.Add(this.btnStatistOvernightArrSummary);
             this.panel2.Controls.Add(this.btnStatistCancelledFlightSummary);
             this.panel2.Controls.Add(this.btnStatistAccToArrDep);
             this.panel2.Controls.Add(this.btnSpotUsingRate);
             this.panel2.Controls.Add(this.btnDailyFlightLog);
             this.panel2.Controls.Add(this.btnGPUUsage);
             this.panel2.Controls.Add(this.btnInventory);
             this.panel2.Controls.Add(this.btnOvernightFlights);
             this.panel2.Controls.Add(this.btnBeltAllocations);
             this.panel2.Controls.Add(this.button4);
             this.panel2.Controls.Add(this.btnStatisticCancelledFlights);
             this.panel2.Controls.Add(this.btnCounterSchedule);
             this.panel2.Controls.Add(this.label1);
             this.panel2.Controls.Add(this.btnFlightsByNatureCode);
             this.panel2.Controls.Add(this.cbTimeInLocal);
             this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
             this.panel2.Location = new System.Drawing.Point(0, 64);
             this.panel2.Name = "panel2";
             this.panel2.Size = new System.Drawing.Size(722, 561);
             this.panel2.TabIndex = 1;
             this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
             // 
             // cboScenario
             // 
             this.cboScenario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
             this.cboScenario.FormattingEnabled = true;
             this.cboScenario.Location = new System.Drawing.Point(426, 6);
             this.cboScenario.Name = "cboScenario";
             this.cboScenario.Size = new System.Drawing.Size(190, 21);
             this.cboScenario.TabIndex = 0;
             // 
             // lblScenario
             // 
             this.lblScenario.Location = new System.Drawing.Point(372, 5);
             this.lblScenario.Name = "lblScenario";
             this.lblScenario.Size = new System.Drawing.Size(62, 22);
             this.lblScenario.TabIndex = 45;
             this.lblScenario.Text = "Scenario :";
             this.lblScenario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             // 
             // btnTowingReport
             // 
             this.btnTowingReport.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnTowingReport.Location = new System.Drawing.Point(19, 523);
             this.btnTowingReport.Name = "btnTowingReport";
             this.btnTowingReport.Size = new System.Drawing.Size(324, 23);
             this.btnTowingReport.TabIndex = 20;
             this.btnTowingReport.Text = "Towing Report";
             this.btnTowingReport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnTowingReport.Click += new System.EventHandler(this.btnTowingReport_Click);
             // 
             // btnAircraftLanding
             // 
             this.btnAircraftLanding.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnAircraftLanding.Location = new System.Drawing.Point(19, 496);
             this.btnAircraftLanding.Name = "btnAircraftLanding";
             this.btnAircraftLanding.Size = new System.Drawing.Size(324, 23);
             this.btnAircraftLanding.TabIndex = 19;
             this.btnAircraftLanding.Text = "Flight Permit - Non-Scheduled Aircraft Landing Permission";
             this.btnAircraftLanding.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnAircraftLanding.Click += new System.EventHandler(this.btnAircraftLanding_Click);
             // 
             // btnAircraftOnGround
             // 
             this.btnAircraftOnGround.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnAircraftOnGround.Location = new System.Drawing.Point(19, 469);
             this.btnAircraftOnGround.Name = "btnAircraftOnGround";
             this.btnAircraftOnGround.Size = new System.Drawing.Size(324, 23);
             this.btnAircraftOnGround.TabIndex = 18;
             this.btnAircraftOnGround.Text = "Flight Permit - Non-Scheduled Aircraft On Ground Report";
             this.btnAircraftOnGround.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnAircraftOnGround.Click += new System.EventHandler(this.btnAircraftOnGround_Click);
             // 
             // btnpiechart
             // 
             this.btnpiechart.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnpiechart.Location = new System.Drawing.Point(375, 415);
             this.btnpiechart.Name = "btnpiechart";
             this.btnpiechart.Size = new System.Drawing.Size(324, 23);
             this.btnpiechart.TabIndex = 34;
             this.btnpiechart.Text = "Standard Report Pie Chart";
             this.btnpiechart.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnpiechart.Click += new System.EventHandler(this.btnpiechart_Click);
             // 
             // butBeltAllocation
             // 
             this.butBeltAllocation.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.butBeltAllocation.Location = new System.Drawing.Point(375, 172);
             this.butBeltAllocation.Name = "butBeltAllocation";
             this.butBeltAllocation.Size = new System.Drawing.Size(324, 23);
             this.butBeltAllocation.TabIndex = 25;
             this.butBeltAllocation.Text = "Belt Allocation Plan";
             this.butBeltAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.butBeltAllocation.Click += new System.EventHandler(this.butBeltAllocation_Click);
             // 
             // btnFlightViewer
             // 
             this.btnFlightViewer.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnFlightViewer.Location = new System.Drawing.Point(19, 442);
             this.btnFlightViewer.Name = "btnFlightViewer";
             this.btnFlightViewer.Size = new System.Drawing.Size(324, 23);
             this.btnFlightViewer.TabIndex = 17;
             this.btnFlightViewer.Text = "Seasonal Flight Viewer";
             this.btnFlightViewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnFlightViewer.Click += new System.EventHandler(this.btnFlightViewer_Click);
             // 
             // btnGateUsage
             // 
             this.btnGateUsage.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnGateUsage.Location = new System.Drawing.Point(375, 199);
             this.btnGateUsage.Name = "btnGateUsage";
             this.btnGateUsage.Size = new System.Drawing.Size(324, 23);
             this.btnGateUsage.TabIndex = 26;
             this.btnGateUsage.Text = "Gate Usage";
             this.btnGateUsage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnGateUsage.Click += new System.EventHandler(this.btnGateUsage_Click);
             // 
             // btnFlightAllocation
             // 
             this.btnFlightAllocation.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnFlightAllocation.Location = new System.Drawing.Point(375, 469);
             this.btnFlightAllocation.Name = "btnFlightAllocation";
             this.btnFlightAllocation.Size = new System.Drawing.Size(324, 23);
             this.btnFlightAllocation.TabIndex = 36;
             this.btnFlightAllocation.Text = "Flight Allocation Plan";
             this.btnFlightAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnFlightAllocation.Click += new System.EventHandler(this.btnFlightAllocation_Click);
             // 
             // btnDeIcing
             // 
             this.btnDeIcing.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
             this.btnDeIcing.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnDeIcing.Location = new System.Drawing.Point(18, 361);
             this.btnDeIcing.Name = "btnDeIcing";
             this.btnDeIcing.Size = new System.Drawing.Size(324, 23);
             this.btnDeIcing.TabIndex = 14;
             this.btnDeIcing.Text = "De-icing";
             this.btnDeIcing.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnDeIcing.UseVisualStyleBackColor = false;
             this.btnDeIcing.Click += new System.EventHandler(this.btnDeIcing_Click);
             // 
             // btnNightOperationsSummary
             // 
             this.btnNightOperationsSummary.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnNightOperationsSummary.Location = new System.Drawing.Point(375, 442);
             this.btnNightOperationsSummary.Name = "btnNightOperationsSummary";
             this.btnNightOperationsSummary.Size = new System.Drawing.Size(324, 23);
             this.btnNightOperationsSummary.TabIndex = 35;
             this.btnNightOperationsSummary.Text = "Night Operations (Summary)";
             this.btnNightOperationsSummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnNightOperationsSummary.Click += new System.EventHandler(this.btnNightOperationsSummary_Click);
             // 
             // btnNightOperations
             // 
             this.btnNightOperations.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnNightOperations.Location = new System.Drawing.Point(375, 388);
             this.btnNightOperations.Name = "btnNightOperations";
             this.btnNightOperations.Size = new System.Drawing.Size(324, 23);
             this.btnNightOperations.TabIndex = 33;
             this.btnNightOperations.Text = "Night Operations";
             this.btnNightOperations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnNightOperations.Click += new System.EventHandler(this.btnNightOperations_Click);
             // 
             // buttonChuteAllocation
             // 
             this.buttonChuteAllocation.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
             this.buttonChuteAllocation.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.buttonChuteAllocation.Location = new System.Drawing.Point(19, 388);
             this.buttonChuteAllocation.Name = "buttonChuteAllocation";
             this.buttonChuteAllocation.Size = new System.Drawing.Size(324, 23);
             this.buttonChuteAllocation.TabIndex = 15;
             this.buttonChuteAllocation.Text = "Chute Allocation";
             this.buttonChuteAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.buttonChuteAllocation.UseVisualStyleBackColor = false;
             this.buttonChuteAllocation.Click += new System.EventHandler(this.buttonChuteAllocation_Click);
             // 
             // buttonHelp
             // 
             this.buttonHelp.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
             this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.buttonHelp.Location = new System.Drawing.Point(631, 5);
             this.buttonHelp.Name = "buttonHelp";
             this.buttonHelp.Size = new System.Drawing.Size(68, 23);
             this.buttonHelp.TabIndex = 1;
             this.buttonHelp.Text = "Help";
             this.buttonHelp.UseVisualStyleBackColor = false;
             this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
             // 
             // button_ConfigLoadPax
             // 
             this.button_ConfigLoadPax.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.button_ConfigLoadPax.Location = new System.Drawing.Point(375, 334);
             this.button_ConfigLoadPax.Name = "button_ConfigLoadPax";
             this.button_ConfigLoadPax.Size = new System.Drawing.Size(324, 23);
             this.button_ConfigLoadPax.TabIndex = 31;
             this.button_ConfigLoadPax.Text = "Load and Pax";
             this.button_ConfigLoadPax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.button_ConfigLoadPax.Click += new System.EventHandler(this.button_ConfigLoadPax_Click);
             // 
             // button8
             // 
             this.button8.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
             this.button8.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.button8.Location = new System.Drawing.Point(19, 415);
             this.button8.Name = "button8";
             this.button8.Size = new System.Drawing.Size(324, 23);
             this.button8.TabIndex = 16;
             this.button8.Text = "Delay Reasons (Summary)";
             this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.button8.UseVisualStyleBackColor = false;
             this.button8.Visible = false;
             this.button8.Click += new System.EventHandler(this.button8_Click);
             // 
             // button7
             // 
             this.button7.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.button7.Location = new System.Drawing.Point(375, 361);
             this.button7.Name = "button7";
             this.button7.Size = new System.Drawing.Size(324, 23);
             this.button7.TabIndex = 32;
             this.button7.Text = "Reason for Changes";
             this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.button7.Click += new System.EventHandler(this.button7_Click);
             // 
             // button2
             // 
             this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.button2.Location = new System.Drawing.Point(375, 496);
             this.button2.Name = "button2";
             this.button2.Size = new System.Drawing.Size(324, 23);
             this.button2.TabIndex = 37;
             this.button2.Text = "Abnormal Flights at Pudong Airport";
             this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.button2.Click += new System.EventHandler(this.button2_Click);
             // 
             // button1
             // 
             this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.button1.Location = new System.Drawing.Point(375, 253);
             this.button1.Name = "button1";
             this.button1.Size = new System.Drawing.Size(324, 23);
             this.button1.TabIndex = 28;
             this.button1.Text = "Unavailable Resources ";
             this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.button1.Click += new System.EventHandler(this.button1_Click);
             // 
             // btnSeatStatisDomIntComb
             // 
             this.btnSeatStatisDomIntComb.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnSeatStatisDomIntComb.Location = new System.Drawing.Point(375, 307);
             this.btnSeatStatisDomIntComb.Name = "btnSeatStatisDomIntComb";
             this.btnSeatStatisDomIntComb.Size = new System.Drawing.Size(324, 23);
             this.btnSeatStatisDomIntComb.TabIndex = 30;
             this.btnSeatStatisDomIntComb.Text = "Seats According to Airline/Destination/Combined";
             this.btnSeatStatisDomIntComb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnSeatStatisDomIntComb.Click += new System.EventHandler(this.btnSeatStatisDomIntComb_Click);
             // 
             // btnStatistDomesticIntComb
             // 
             this.btnStatistDomesticIntComb.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnStatistDomesticIntComb.Location = new System.Drawing.Point(19, 172);
             this.btnStatistDomesticIntComb.Name = "btnStatistDomesticIntComb";
             this.btnStatistDomesticIntComb.Size = new System.Drawing.Size(324, 23);
             this.btnStatistDomesticIntComb.TabIndex = 7;
             this.btnStatistDomesticIntComb.Text = "Flights According to Domestic/International/Combined";
             this.btnStatistDomesticIntComb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnStatistDomesticIntComb.Click += new System.EventHandler(this.btnStatistDomesticIntComb_Click);
             // 
             // btnStatistRushHour
             // 
             this.btnStatistRushHour.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnStatistRushHour.Location = new System.Drawing.Point(375, 91);
             this.btnStatistRushHour.Name = "btnStatistRushHour";
             this.btnStatistRushHour.Size = new System.Drawing.Size(324, 23);
             this.btnStatistRushHour.TabIndex = 22;
             this.btnStatistRushHour.Text = "Check-in Counter Usage";
             this.btnStatistRushHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnStatistRushHour.Click += new System.EventHandler(this.btnStatistRushHour_Click);
             // 
             // btnAircraftMovementsPerHour
             // 
             this.btnAircraftMovementsPerHour.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnAircraftMovementsPerHour.Location = new System.Drawing.Point(19, 199);
             this.btnAircraftMovementsPerHour.Name = "btnAircraftMovementsPerHour";
             this.btnAircraftMovementsPerHour.Size = new System.Drawing.Size(324, 23);
             this.btnAircraftMovementsPerHour.TabIndex = 8;
             this.btnAircraftMovementsPerHour.Text = "Aircraft Movements per Hour";
             this.btnAircraftMovementsPerHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnAircraftMovementsPerHour.Click += new System.EventHandler(this.btnAircraftMovementsPerHour_Click);
             // 
             // btnStatistAccArrDep
             // 
             this.btnStatistAccArrDep.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnStatistAccArrDep.Location = new System.Drawing.Point(19, 91);
             this.btnStatistAccArrDep.Name = "btnStatistAccArrDep";
             this.btnStatistAccArrDep.Size = new System.Drawing.Size(324, 23);
             this.btnStatistAccArrDep.TabIndex = 4;
             this.btnStatistAccArrDep.Text = "Flights According to Arrival / Departure ";
             this.btnStatistAccArrDep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnStatistAccArrDep.Click += new System.EventHandler(this.btnStatistAccArrDep_Click);
             // 
             // btnStatistOvernightArrSummary
             // 
             this.btnStatistOvernightArrSummary.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnStatistOvernightArrSummary.Location = new System.Drawing.Point(19, 253);
             this.btnStatistOvernightArrSummary.Name = "btnStatistOvernightArrSummary";
             this.btnStatistOvernightArrSummary.Size = new System.Drawing.Size(324, 23);
             this.btnStatistOvernightArrSummary.TabIndex = 10;
             this.btnStatistOvernightArrSummary.Text = "Overnight Flights (Summary)";
             this.btnStatistOvernightArrSummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnStatistOvernightArrSummary.Click += new System.EventHandler(this.btnStatistOvernightArrSummary_Click);
             // 
             // btnStatistCancelledFlightSummary
             // 
             this.btnStatistCancelledFlightSummary.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnStatistCancelledFlightSummary.Location = new System.Drawing.Point(19, 307);
             this.btnStatistCancelledFlightSummary.Name = "btnStatistCancelledFlightSummary";
             this.btnStatistCancelledFlightSummary.Size = new System.Drawing.Size(324, 23);
             this.btnStatistCancelledFlightSummary.TabIndex = 12;
             this.btnStatistCancelledFlightSummary.Text = "Cancelled Flights (Summary)";
             this.btnStatistCancelledFlightSummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnStatistCancelledFlightSummary.Click += new System.EventHandler(this.btnStatistCancelledFlightSummary_Click);
             // 
             // btnStatistAccToArrDep
             // 
             this.btnStatistAccToArrDep.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnStatistAccToArrDep.Location = new System.Drawing.Point(19, 118);
             this.btnStatistAccToArrDep.Name = "btnStatistAccToArrDep";
             this.btnStatistAccToArrDep.Size = new System.Drawing.Size(324, 23);
             this.btnStatistAccToArrDep.TabIndex = 5;
             this.btnStatistAccToArrDep.Text = "Flights According to Arrival / Departure  (Summary)";
             this.btnStatistAccToArrDep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnStatistAccToArrDep.Click += new System.EventHandler(this.btnStatistAccToArrDep_Click);
             // 
             // btnSpotUsingRate
             // 
             this.btnSpotUsingRate.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnSpotUsingRate.Location = new System.Drawing.Point(375, 118);
             this.btnSpotUsingRate.Name = "btnSpotUsingRate";
             this.btnSpotUsingRate.Size = new System.Drawing.Size(324, 23);
             this.btnSpotUsingRate.TabIndex = 23;
             this.btnSpotUsingRate.Text = "Position Usage";
             this.btnSpotUsingRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnSpotUsingRate.Click += new System.EventHandler(this.btnSpotUsingRate_Click);
             // 
             // btnDailyFlightLog
             // 
             this.btnDailyFlightLog.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnDailyFlightLog.Location = new System.Drawing.Point(19, 145);
             this.btnDailyFlightLog.Name = "btnDailyFlightLog";
             this.btnDailyFlightLog.Size = new System.Drawing.Size(324, 23);
             this.btnDailyFlightLog.TabIndex = 6;
             this.btnDailyFlightLog.Text = "Daily Flight Log";
             this.btnDailyFlightLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnDailyFlightLog.Click += new System.EventHandler(this.btnDailyFlightLog_Click);
             // 
             // btnGPUUsage
             // 
             this.btnGPUUsage.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnGPUUsage.Location = new System.Drawing.Point(375, 226);
             this.btnGPUUsage.Name = "btnGPUUsage";
             this.btnGPUUsage.Size = new System.Drawing.Size(324, 23);
             this.btnGPUUsage.TabIndex = 27;
             this.btnGPUUsage.Text = "GPU Usage";
             this.btnGPUUsage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnGPUUsage.Click += new System.EventHandler(this.btnGPUUsage_Click);
             // 
             // btnInventory
             // 
             this.btnInventory.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnInventory.Location = new System.Drawing.Point(375, 280);
             this.btnInventory.Name = "btnInventory";
             this.btnInventory.Size = new System.Drawing.Size(324, 23);
             this.btnInventory.TabIndex = 29;
             this.btnInventory.Text = "Inventory (A/C on Ground)";
             this.btnInventory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnInventory.Click += new System.EventHandler(this.btnInventory_Click);
             // 
             // btnOvernightFlights
             // 
             this.btnOvernightFlights.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnOvernightFlights.Location = new System.Drawing.Point(19, 226);
             this.btnOvernightFlights.Name = "btnOvernightFlights";
             this.btnOvernightFlights.Size = new System.Drawing.Size(324, 23);
             this.btnOvernightFlights.TabIndex = 9;
             this.btnOvernightFlights.Text = "Overnight Flights";
             this.btnOvernightFlights.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnOvernightFlights.Click += new System.EventHandler(this.btnOvernightFlights_Click);
             // 
             // btnBeltAllocations
             // 
             this.btnBeltAllocations.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnBeltAllocations.Location = new System.Drawing.Point(375, 145);
             this.btnBeltAllocations.Name = "btnBeltAllocations";
             this.btnBeltAllocations.Size = new System.Drawing.Size(324, 23);
             this.btnBeltAllocations.TabIndex = 24;
             this.btnBeltAllocations.Text = "Baggage Belt Usage ";
             this.btnBeltAllocations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnBeltAllocations.Click += new System.EventHandler(this.btnBeltAllocations_Click);
             // 
             // button4
             // 
             this.button4.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.button4.Location = new System.Drawing.Point(19, 334);
             this.button4.Name = "button4";
             this.button4.Size = new System.Drawing.Size(324, 23);
             this.button4.TabIndex = 13;
             this.button4.Text = "Delayed Flights";
             this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.button4.Click += new System.EventHandler(this.button4_Click);
             // 
             // btnStatisticCancelledFlights
             // 
             this.btnStatisticCancelledFlights.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnStatisticCancelledFlights.Location = new System.Drawing.Point(19, 280);
             this.btnStatisticCancelledFlights.Name = "btnStatisticCancelledFlights";
             this.btnStatisticCancelledFlights.Size = new System.Drawing.Size(324, 23);
             this.btnStatisticCancelledFlights.TabIndex = 11;
             this.btnStatisticCancelledFlights.Text = "Cancelled Flights";
             this.btnStatisticCancelledFlights.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnStatisticCancelledFlights.Click += new System.EventHandler(this.btnStatisticCancelledFlights_Click);
             // 
             // btnCounterSchedule
             // 
             this.btnCounterSchedule.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnCounterSchedule.Location = new System.Drawing.Point(375, 64);
             this.btnCounterSchedule.Name = "btnCounterSchedule";
             this.btnCounterSchedule.Size = new System.Drawing.Size(324, 23);
             this.btnCounterSchedule.TabIndex = 21;
             this.btnCounterSchedule.Text = "Check-in Counter Allocation";
             this.btnCounterSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnCounterSchedule.Click += new System.EventHandler(this.btnCounterSchedule_Click);
             // 
             // label1
             // 
             this.label1.Location = new System.Drawing.Point(16, -2);
             this.label1.Name = "label1";
             this.label1.Size = new System.Drawing.Size(350, 36);
             this.label1.TabIndex = 1;
             this.label1.Text = "Please click one of the following buttons to activate the respective report:";
             this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             // 
             // btnFlightsByNatureCode
             // 
             this.btnFlightsByNatureCode.FlatStyle = System.Windows.Forms.FlatStyle.System;
             this.btnFlightsByNatureCode.Location = new System.Drawing.Point(19, 64);
             this.btnFlightsByNatureCode.Name = "btnFlightsByNatureCode";
             this.btnFlightsByNatureCode.Size = new System.Drawing.Size(324, 23);
             this.btnFlightsByNatureCode.TabIndex = 3;
             this.btnFlightsByNatureCode.Text = "Flights By Natures/Aircraft/Airlines/Origin/Destinations/Reg.";
             this.btnFlightsByNatureCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
             this.btnFlightsByNatureCode.Click += new System.EventHandler(this.btnFlightsByNatureCode_Click);
             // 
             // frmMain
             // 
             this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
             this.ClientSize = new System.Drawing.Size(722, 625);
             this.Controls.Add(this.panel2);
             this.Controls.Add(this.panel1);
             this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
             this.MaximizeBox = false;
             this.Name = "frmMain";
             this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
             this.Text = "FIPS Reports";
             this.Load += new System.EventHandler(this.frmMain_Load);
             this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmMain_HelpRequested);
             this.panel1.ResumeLayout(false);
             this.panel1.PerformLayout();
             ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
             ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).EndInit();
             this.panel2.ResumeLayout(false);
             this.ResumeLayout(false);

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// Instantiates the frmMain, which is the
        /// main form of the application and starts it.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            frmMain olDlg = new frmMain();
            olDlg.args = args;
            Application.Run(olDlg);
            //Application.Run(new frmMain());
        }

       #region ### Windows Form Events ###
        /// <summary>
        /// Opens the Flights by Nature Code Filter dialog
        /// </summary>
        /// <param name="sender">Originator of the call</param>
        /// <param name="e">The event arguments</param>
        private void btnFlightsByNatureCode_Click(object sender, System.EventArgs e)
        {
            frmFlightByNatureCode frm = new frmFlightByNatureCode();
            frm.ShowDialog(this);
        }
        /// <summary>
        /// Initialize the main form of the application.
        /// Connect to CEDA, init the version and so on.
        /// </summary>
        /// <param name="sender">Originator of the call</param>
        /// <param name="e">Event arguments</param>
        private void frmMain_Load(object sender, System.EventArgs e)
        {
            try
            {
                 
                
                string strUTCConvertion = "";

                lblScenario.Visible = false;
                cboScenario.Visible = false;

                Ufis.Utils.IniFile myIni = new IniFile(Environment.GetEnvironmentVariable("CEDA").ToString());
                UT.ServerName = myIni.IniReadValue("GLOBAL", "HOSTNAME");
                UT.Hopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
                strPrefixVal = myIni.IniReadValue("FIPS", "PREFIX_REPORTS");
                strPositionUsage = myIni.IniReadValue("FIPSREPORT", "POSITIONUSAGE");
                strReportList = myIni.IniReadValue("FIPSREPORT", "REPORT_LIST");
                //Prepare report list according to no. This will use if the customer don't want to control by BDPS_SEC
                ConstructReportList(strReportList);                
                
                strUTCConvertion = myIni.IniReadValue("GLOBAL", "UTCCONVERTIONS");
                sCurrentScenario = string.Empty;
                if (strUTCConvertion == "NO")
                {
                    UT.UTCOffset = 0;
                }
                else
                {
                    DateTime datLocal = DateTime.Now;
                    DateTime datUTC = DateTime.UtcNow;//CFC.GetUTC();
                    TimeSpan span = datLocal - datUTC;

                    UT.UTCOffset = (short)span.Hours;
                }

                //Connect Database.
                myDB = UT.GetMemDB();
                bool result = myDB.Connect(UT.ServerName, UT.UserName, UT.Hopo, "TAB", "FIPS_RPT");               

                UT.IsTimeInUtc = false;
                lblVersion.Text = "Version: ";
                lblVersion.Text += Application.ProductVersion;
                lblServer.Text = UT.ServerName;
                //check whether Reason for change is enabled or not
                string strReasonForChange = myIni.IniReadValue("FIPS", "REASONFORCHANGES");

                if (strReasonForChange.Equals("TRUE") || strReasonForChange.Equals("ENABLE"))
                {
                    this.button7.Show();
                }
                else
                {
                    this.button7.Hide();
                }
                if (myIni.IniReadValue("FIPS", "CHUTE_DISPLAY").CompareTo("TRUE") != 0)
                {
                    buttonChuteAllocation.Hide();
                }
                if (myIni.IniReadValue("FIPS", "NIGHT_FLIGHT_SLOT").CompareTo("TRUE") != 0)
                {
                    btnNightOperations.Hide();
                    btnNightOperationsSummary.Hide();
                }
                // check, if column for Abnormal flight report is available in delay code table
                this.button2.Enabled = false;
                this.button2.Visible = false;

                ///Changed for the Login dialog to pop while the application is started.
                ///This is added while solving the PRF 8523
                //string iltet;
                //if (args.Length == 0)
                //    iltet = "";

                if (!LoginProcedure())
                {
                    Application.Exit();
                }
                //Initialise De icing report
                InitDeIce();
                
                if (myIni.IniReadValue("FIPS", "ALLOCATION_PLAN").CompareTo("TRUE") != 0)
                {
                    btnFlightAllocation.Hide();
                }
                #region Multi Airport Control Handler
                     // Handle BDPS_SEC Access Right
                     HandleToolbar_BDPS_SEC();
                    //Get List of button controls
                     List<Control> lstBtnCtrls = Helper.GetButtonCtrlsAndAdjust("Button", this);
                    //Bind to SortedDictionary in order to sort by key (TabIndex of Control)
                     SortedDictionary<int, Button> srtDict = new SortedDictionary<int, Button>();
                     foreach (Control ctrl in lstBtnCtrls)
                     {
                          Control tmpCtrl = ctrl;
                          Button btn = (Button)ctrl;
                          srtDict.Add(ctrl.TabIndex, btn);
                     }
                    //Set default location of report buttons
                     iONewX = btnFlightsByNatureCode.Location.X;
                     iONewY = btnFlightsByNatureCode.Location.Y;
                     iOGap = btnStatistAccArrDep.Height / 5;

                     foreach (Button ctrl in srtDict.Values)
                     {
                          CalculateButtonLocation(ctrl, iONewX, iONewY, iOGap, iOCYCnt, iOYCnt);
                     }
                     try
                     {
                          bMAirport = Convert.ToBoolean(myIni.IniReadValue("GLOBAL", "MULTI_AIRPORT"));
                     }
                     catch (Exception)
                     {
 
                     }
                     if (bMAirport)
                     {
                         lblScenario.Visible = true;
                         cboScenario.Visible = true;
                         loadScenario();
                     }
                     else
                     {
                         cbTimeInLocal.Location = new Point(372, 6);
                     }
                     lblCurrentScenario.Text = sCurrentScenario;
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show("Connecting to Server Failed! " + ex.Message , "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

        }

        /// <summary>
        /// Sets the UT namespace internal indicator to local times or UTC times,
        /// depending on the user's click onto the cbTimeInLocal check box.
        /// </summary>
        /// <param name="sender">Originator of the call</param>
        /// <param name="e">Event arguments</param>
        private void cbTimeInLocal_CheckedChanged(object sender, System.EventArgs e)
        {
             UT.IsTimeInUtc = !cbTimeInLocal.Checked;
        }
        /// <summary>
        /// Opens the Counter Schedule Filter dialog
        /// </summary>
        /// <param name="sender">Originator of the call</param>
        /// <param name="e">The event arguments</param>
        private void btnCounterSchedule_Click(object sender, System.EventArgs e)
        {
             frmCounterSchedule frm = new frmCounterSchedule();
             frm.ShowDialog();
        }
        /// <summary>
        /// Open the cancelled flights
        /// </summary>
        /// <param name="sender">Originator of the call</param>
        /// <param name="e">The event arguments</param>
        private void btnStatisticCancelledFlights_Click(object sender, System.EventArgs e)
        {
             frmCancelled_Flights frm = new frmCancelled_Flights();
             frm.ShowDialog();
        }
        /// <summary>
        /// Opens delayed flights
        /// </summary>
        /// <param name="sender">Originator of the call</param>
        /// <param name="e">The event arguments</param>
        private void button4_Click(object sender, System.EventArgs e)
        {
             frmDelayed_Flights frm = new frmDelayed_Flights();
             frm.ShowDialog();
        }
        /// <summary>
        /// Opens the belt allocations
        /// </summary>
        /// <param name="sender">Originator of the call</param>
        /// <param name="e">The event arguments</param>
        private void btnBeltAllocations_Click(object sender, System.EventArgs e)
        {
             frmBeltAllocations frm = new frmBeltAllocations();
             frm.ShowDialog();
        }
        /// <summary>
        /// Opens the overnight flights.
        /// </summary>
        /// <param name="sender">Originator of the call</param>
        /// <param name="e">The event arguments</param>
        private void btnOvernightFlights_Click(object sender, System.EventArgs e)
        {
             frmOvernightFlights frm = new frmOvernightFlights();
             frm.ShowDialog();
        }
        /// <summary>
        /// Opens the Inventory list (flights on ground)
        /// </summary>
        /// <param name="sender">Originator of the call</param>
        /// <param name="e">The event arguments</param>
        private void btnInventory_Click(object sender, System.EventArgs e)
        {
             ITable mySYS = myDB.Bind("SYS", "SYS", "TANA", "10", "TANA");
             mySYS.Load("WHERE TANA='FOG'");
             if (mySYS.Count > 0)
             {
                  frmInventory frm = new frmInventory();
                  frm.ShowDialog();
             }
             else
             {
                  MessageBox.Show(this, "Flights on Ground is not supported (FOGTAB missing)!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
             }
             myDB.Unbind("SYS");
        }
        /// <summary>
        /// Opens the form for GPU usage
        /// </summary>
        /// <param name="sender">Originator of the call</param>
        /// <param name="e">The event arguments</param>
        private void btnGPUUsage_Click(object sender, System.EventArgs e)
        {
             frmGPUUsage frm = new frmGPUUsage();
             frm.ShowDialog();
        }
        /// <summary>
        /// Opens the form for Daily flight log.
        /// </summary>
        /// <param name="sender">Originator of the call</param>
        /// <param name="e">The event arguments</param>
        private void btnDailyFlightLog_Click(object sender, System.EventArgs e)
        {
             try
             {
                  frmDaily_FlightLog frm = new frmDaily_FlightLog();
                  frm.ShowDialog();
             }
             catch (Exception ex)
             {
                  MessageBox.Show(this, ex.Message);
             }
        }


        private void panel2_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {

        }

        private void btnAircraftsByHour_Click(object sender, System.EventArgs e)
        {
        }

        private void btnSpotUsingRate_Click(object sender, System.EventArgs e)
        {
             IniFile myIni = new IniFile(Environment.GetEnvironmentVariable("CEDA").ToString());
             string[] strExclude = new string[] { };
             string strExcludePos = myIni.IniReadValue("FIPSREPORT", "EXCLUDE_FROM_POS_USAGE");

             if (strPositionUsage != string.Empty)
             {
                  frmSpot_UsingRate frm = new frmSpot_UsingRate(strExclude);
                  frm.ShowDialog();
             }
             else
             {
                  if (strExcludePos.Length > 0)
                  {
                       strExclude = strExcludePos.Split(',');
                  }

                  string AppPath = Application.StartupPath.ToString();
                  AppPath = AppPath + @"Report\Ufis.Reports.exe";

                  try
                  {
                       string sLocalUTC = UT.IsTimeInUtc == true ? "U" : "L";
                       string sSco = cboScenario.Text == "<None>" ? "" : cboScenario.Text;
                       Process myProcess = new Process();
                       if (bMAirport)
                       {
                           myProcess.StartInfo.Arguments = "POUR" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC + "||" + sSco;
                       }
                       else
                       {
                           myProcess.StartInfo.Arguments = "POUR" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC;                           
                       }
                       myProcess.StartInfo.UseShellExecute = false;
                       myProcess.StartInfo.FileName = AppPath;
                       myProcess.StartInfo.CreateNoWindow = true;
                       myProcess.Start();
                       myProcess.WaitForExit();
                  }
                  catch (Exception ex)
                  {
                       MessageBox.Show(ex.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                  }
             }
        }

        private void btnStatistAccToArrDep_Click(object sender, System.EventArgs e)
        {
             frmStatistics_Arr_Dep frm = new frmStatistics_Arr_Dep();
             frm.ShowDialog();
        }

        private void btnStatistCancelledFlightSummary_Click(object sender, System.EventArgs e)
        {
             frmCancelled_FlightDetail frm = new frmCancelled_FlightDetail();
             frm.ShowDialog();
        }

        private void btnStatistOvernightArrSummary_Click(object sender, System.EventArgs e)
        {
             frmOverNight_FlightSummary frm = new frmOverNight_FlightSummary();
             frm.ShowDialog();
        }

        private void btnStatistAccArrDep_Click(object sender, System.EventArgs e)
        {
             frmStatisticsAccordingToArrivalDeparture frm = new frmStatisticsAccordingToArrivalDeparture();
             frm.ShowDialog();
        }


        private void btnAircraftMovementsPerHour_Click(object sender, System.EventArgs e)
        {
             frmAircraftMovementsperHour frm = new frmAircraftMovementsperHour();
             frm.ShowDialog();
        }

        private void btnStatistRushHour_Click(object sender, System.EventArgs e)
        {
             frmCheckin_statisticGraph frm = new frmCheckin_statisticGraph();
             frm.ShowDialog();
        }

        private void btnStatistDomesticIntComb_Click(object sender, System.EventArgs e)
        {
             IniFile myIni = new IniFile(Environment.GetEnvironmentVariable("CEDA").ToString());
             string[] strFltiAndDesc;
             string strFltiDesc = myIni.IniReadValue("FIPSREPORT", "FLIGHTS_ACCORDING_FLTI");
             if (strFltiDesc.Length > 0)
             {
                  strFltiAndDesc = strFltiDesc.Split(',');
                  if (strFltiAndDesc.Length < 4 || strFltiAndDesc.Length % 2 != 0 || strFltiAndDesc.Length > 8)
                  {
                       MessageBox.Show(this, "FLIGHTS_ACCORDING_FLTI entry is corrupted", "Ceda.ini Entry", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                       return;
                  }
             }
             frmFlightStatisticsDomesticInternationalCombined frm = new frmFlightStatisticsDomesticInternationalCombined();
             frm.ShowDialog();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
             frmBlockedTimes frm = new frmBlockedTimes();
             frm.ShowDialog();
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
             frmAbnormal_Flights frm = new frmAbnormal_Flights();
             frm.ShowDialog();
        }

        private void button8_Click(object sender, System.EventArgs e)
        {
             frmStatDelayReasons frmStatDelayRes = new frmStatDelayReasons();
             frmStatDelayRes.ShowDialog();
        }

        private void button7_Click(object sender, System.EventArgs e)
        {
             frmReasonForChanges frmReasonForChanges = new frmReasonForChanges();
             frmReasonForChanges.ShowDialog();
        }

        private void btnSeatStatisDomIntComb_Click(object sender, System.EventArgs e)
        {
             frmSeatStatisticsAccDomInt frmSeatStatDomInt = new frmSeatStatisticsAccDomInt();
             frmSeatStatDomInt.ShowDialog();
        }

        private void button_ConfigLoadPax_Click(object sender, System.EventArgs e)
        {
             frmCongfigLoadPax frm = new frmCongfigLoadPax();
             frm.ShowDialog();
        }

        private void buttonChuteAllocation_Click(object sender, System.EventArgs e)
        {
             frmChuteSchedule frm = new frmChuteSchedule();
             frm.ShowDialog();
        }

        private void buttonHelp_Click(object sender, System.EventArgs e)
        {
             Helper.ShowHelpFile(this);
        }

        private void frmMain_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
             Helper.ShowHelpFile(this);
        }

        private void btnNightOperations_Click(object sender, EventArgs e)
        {
             frmNightOperations frmNightOperationsRes = new frmNightOperations();
             frmNightOperationsRes.ShowDialog();

        }

        private void btnNightOperationsSummary_Click(object sender, EventArgs e)
        {
             frmNightFlightSummary frmNightFlightSummaryRes = new frmNightFlightSummary();
             frmNightFlightSummaryRes.ShowDialog();

        }

        private void btnDeIcing_Click(object sender, EventArgs e)
        {
             try
             {
                  //AxDeicingClass.DeicingDialogClass deIcing = new AxDeicingClass.DeicingDialogClass();

                  //deIcing.AppName = LoginControl.ApplicationName;// "FIPS-Report";
                  //deIcing.AppVersion = LoginControl.InfoAppVersion;// "4.6.1.9";
                  ////deIcing.IniFilePath = @"d:\Ufis\System\De-icing.ini";
                  //deIcing.HandleBc = true;
                  //AxDeicingClass.TimeZoneEnum timeZone = AxDeicingClass.TimeZoneEnum.UTC ; 
                  //deIcing.set_TimeZone(ref timeZone);
                  //deIcing.UserName = LoginControl.GetUserName();// "UFIS$ADMIN";
                  //deIcing.ShowSplashScreen = false;
                  //deIcing.LoadBasicData();


                  //deIcing.ShowDialog(true);

                  Ctrl.CtrlDeIce.GetInstance().Show();
             }
             catch (Exception ex)
             {
                  MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
             }
        }
        //Phyoe Khaing Min
        //17-Feb-2012
        //Allocation Planning
        private void btnFlightAllocation_Click(object sender, EventArgs e)
        {
            string AppPath = Application.StartupPath.ToString();
            AppPath = AppPath + @"\Ufis.Reports.exe";

            try
            {
                string sLocalUTC = UT.IsTimeInUtc == true ? "U" : "L";
                string sSco = cboScenario.Text == "<None>" ? "" : cboScenario.Text;
                Process myProcess = new Process();
                if (bMAirport)
                {
                    
                    myProcess.StartInfo.Arguments = "AUH-FLAP" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC + "||" + sSco;
                }
                else
                {
                    myProcess.StartInfo.Arguments = "AUH-FLAP" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC;
                }
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.FileName = AppPath;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.Start();
                myProcess.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
             //try
             //{
             //     string AppPath = Application.StartupPath.ToString();
             //     string sLocalUTC = "";
             //     if (cbTimeInLocal.Checked)
             //     {
             //          sLocalUTC = "L";
             //     }
             //     else
             //     {
             //          sLocalUTC = "U";
             //     }
             //     AppPath = AppPath + @"\Ufis.Reports.exe";
             //     UT.UserName = LoginControl.GetUserName();
             //     string Arguments = "ALPR|" + UT.UserName + "|" + sLocalUTC;
             //     Process.Start(AppPath, Arguments);
             //}
             //catch (Exception ex)
             //{
             //     MessageBox.Show(ex.Message);
             //}
        }


        /// <summary>
        /// Call Gate Usage Report
        /// Siva
        /// 8.May.2012
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGateUsage_Click(object sender, EventArgs e)
        {
             string AppPath = Application.StartupPath.ToString();
             AppPath = AppPath + @"\Ufis.Reports.exe";

             try
             {
                  string sLocalUTC = UT.IsTimeInUtc == true ? "U" : "L";
                  string sSco = cboScenario.Text == "<None>" ? "" : cboScenario.Text;
                  Process myProcess = new Process();
                  if (bMAirport)
                  {                      
                      myProcess.StartInfo.Arguments = "GTUR" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC + "||" + sSco.ToString();
                   }
                  else
                  {
                      myProcess.StartInfo.Arguments = "GTUR" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() +"|" + sLocalUTC;
                  }
                  myProcess.StartInfo.UseShellExecute = false;
                  myProcess.StartInfo.FileName = AppPath;
                  myProcess.StartInfo.CreateNoWindow = true;
                  myProcess.Start();
                  myProcess.WaitForExit();
             }
             catch (Exception ex)
             {
                  MessageBox.Show(ex.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
             }
        }

        /// <summary>
        /// Call Baggage Belt Allocation Report
        /// Siva
        /// 8.May.2012
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butBeltAllocation_Click(object sender, EventArgs e)
        {
             string AppPath = Application.StartupPath.ToString();
             AppPath = AppPath + @"\Ufis.Reports.exe";

             try
             {
                  string sLocalUTC = UT.IsTimeInUtc == true ? "U" : "L";
                  string sSco = cboScenario.Text == "<None>" ? "" : cboScenario.Text;
                  Process myProcess = new Process();
                  if (bMAirport)
                  {
                      
                      myProcess.StartInfo.Arguments = "AUH-BLAP" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC + "||" + sSco;
                  }
                  else
                  {
                      myProcess.StartInfo.Arguments = "AUH-BLAP" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC;
                  }
                  myProcess.StartInfo.UseShellExecute = false;
                  myProcess.StartInfo.FileName = AppPath;
                  myProcess.StartInfo.CreateNoWindow = true;
                  myProcess.Start();
                  myProcess.WaitForExit();
             }
             catch (Exception ex)
             {
                  MessageBox.Show(ex.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
             }
        }

        /// <summary>
        /// Call Seasonal Flight Viewer Report
        /// Phyoe Khaing Min
        /// 26.Apr.2012
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFlightViewer_Click(object sender, EventArgs e)
        {
            string AppPath = Application.StartupPath.ToString();
            AppPath = AppPath + @"\Ufis.Reports.exe";

            try
            {
                string sLocalUTC = UT.IsTimeInUtc == true ? "U" : "L";
                string sSco = cboScenario.Text == "<None>" ? "" : cboScenario.Text;
                Process myProcess = new Process();
                if (bMAirport)
                {
                    
                    myProcess.StartInfo.Arguments = "SEFL" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC + "||" + sSco;
                }
                else
                {
                    myProcess.StartInfo.Arguments = "SEFL" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC;
                }
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.FileName = AppPath;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.Start();
                myProcess.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnpiechart_Click(object sender, EventArgs e)
        {

             string AppPath = Application.StartupPath.ToString();
             AppPath = AppPath + @"\Ufis.Reports.exe";

             try
             {
                  string sLocalUTC = UT.IsTimeInUtc == true ? "U" : "L";
                  Process myProcess = new Process();
                  myProcess.StartInfo.Arguments = "DYTR" + "|" + UT.UserName + "|" + sLocalUTC + "||";
                  myProcess.StartInfo.UseShellExecute = false;
                  myProcess.StartInfo.FileName = AppPath;
                  myProcess.StartInfo.CreateNoWindow = true;
                  myProcess.Start();
                  myProcess.WaitForExit();
             }
             catch (Exception ex)
             {
                  MessageBox.Show(ex.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
             }
        }

        private void btnAircraftOnGround_Click(object sender, EventArgs e)
        {
            string AppPath = Application.StartupPath.ToString();
            AppPath = AppPath + @"\Ufis.Reports.exe";

            try
            {
                string sLocalUTC = UT.IsTimeInUtc == true ? "U" : "L";
                string sSco = cboScenario.Text == "<None>" ? "" : cboScenario.Text;
                Process myProcess = new Process();
                if (bMAirport)
                {
                    
                    myProcess.StartInfo.Arguments = "AUH-FPOG" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC + "||" + sSco;
                }
                else
                {
                    myProcess.StartInfo.Arguments = "AUH-FPOG" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC;
                }
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.FileName = AppPath;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.Start();
                myProcess.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAircraftLanding_Click(object sender, EventArgs e)
        {
            string AppPath = Application.StartupPath.ToString();
            AppPath = AppPath + @"\Ufis.Reports.exe";

            try
            {
                string sLocalUTC = UT.IsTimeInUtc == true ? "U" : "L";
                string sSco = cboScenario.Text == "<None>" ? "" : cboScenario.Text;
                Process myProcess = new Process();
                if (bMAirport)
                {
                    
                    myProcess.StartInfo.Arguments = "AUH-FPLP" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC + "||" + sSco;
                }
                else
                {
                    myProcess.StartInfo.Arguments = "AUH-FPLP" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC;
                }
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.FileName = AppPath;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.Start();
                myProcess.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnTowingReport_Click(object sender, EventArgs e)
        {
            string AppPath = Application.StartupPath.ToString();
            AppPath = AppPath + @"\Ufis.Reports.exe";

            try
            {
                string sLocalUTC = UT.IsTimeInUtc == true ? "U" : "L";
                string sSco = cboScenario.Text == "<None>" ? "" : cboScenario.Text;
                Process myProcess = new Process();
                if (bMAirport)
                {
                    myProcess.StartInfo.Arguments = "AUH-TOWI" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC + "||" + sSco;
                }
                else
                {
                    myProcess.StartInfo.Arguments = "AUH-TOWI" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC;
                }
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.FileName = AppPath;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.Start();
                myProcess.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region ### Private & Public Access Functions ###
        /// <summary>
        /// Relocation Report button location 
        /// </summary>
        /// <param name="ReportButton">System.Windows.Form.Button containing button control</param>
        /// <param name="iNewX">System.int containing button location X</param>
        /// <param name="iNewY">System.int containing button location Y</param>
        /// <param name="iGap">System.int containing button gap</param>
        /// <param name="iCYCnt">System.int containing current button count</param>
        /// <param name="iYCnt">System.int containing button Total Count</param>
        private void CalculateButtonLocation(Button ReportButton, int iNewX, int iNewY, int iGap, int iCYCnt, int iYCnt)
        {
             if (ReportButton.Text == "Help")
                  return;

             if (ReportButton.Visible == true)
             {
                  ReportButton.Location = new Point(iNewX, iNewY);
                  iNewY = iNewY + ReportButton.Height + iGap;
                  iCYCnt++;
                  iONewY = iNewY;                  
                  iOCYCnt = iCYCnt;
                  if (iCYCnt == iYCnt)
                  {
                       iNewX = iNewX + btnFlightsByNatureCode.Width + btnFlightsByNatureCode.Height;
                       iNewY = btnFlightsByNatureCode.Location.Y;
                       iONewX = iNewX;
                       iONewY = iNewY;
                  }
             }
         
        }       
        /// <summary>
        /// Move the button according to show hide
        /// </summary>
        /// 
        private void HandleToolbar_BDPS_SEC()
        {
            bShowReport = _ReportList.ContainsKey("FlightsByNature");
            string prv = string.Empty;

            if (bShowReport)
            {
                btnFlightsByNatureCode.Enabled = true;
            }
            else
            {
                prv = LoginControl.GetPrivileges("FlightsByNature");
                if (prv == "0")
                {
                    btnFlightsByNatureCode.Enabled = false;
                }
                else if (prv == "-")
                {
                    btnFlightsByNatureCode.Visible = false;
                }
                else if (prv == "1")
                {
                    btnFlightsByNatureCode.Visible = true;
                }                
                //Zaw Min Tun / MZAW
                //The case is no INI control and also no BDPS-SEC setting
                //By-chance patch with 3084
                //Similar checking is added in all places
                else 
                {
                    btnFlightsByNatureCode.Visible = false;
                }
            }
            
            bShowReport = false;
            //FlightsArrDep
            bShowReport = _ReportList.ContainsKey("STDPieChart");
            if (bShowReport)
            {
                btnpiechart.Enabled = true;
            }
            else
            {
                prv = LoginControl.GetPrivileges("STDPieChart");
                if (prv == "0")
                {
                    btnpiechart.Enabled = false;
                }
                else if (prv == "-")
                {
                    btnpiechart.Visible = false;
                }
                else if (prv == "1")
                {
                    btnpiechart.Visible = true;
                }
                else
                {
                    btnpiechart.Visible = false;
                }
            }
            bShowReport = false;
             //FlightsArrDep
            bShowReport =_ReportList.ContainsKey("FlightsArrDep");
            if (bShowReport)
            {
                btnStatistAccArrDep.Enabled = true;
            }
            else 
            {
                prv = LoginControl.GetPrivileges("FlightsArrDep");
                if (prv == "0")
                {
                    btnStatistAccArrDep.Enabled = false;
                }
                else if (prv == "-")
                {
                    btnStatistAccArrDep.Visible = false;
                }
                else if (prv == "1")
                {
                    btnStatistAccArrDep.Visible = true;
                }
                else 
                {
                    btnStatistAccArrDep.Visible = false;
                }
            }
            bShowReport = false;
            bShowReport =_ReportList.ContainsKey("FlightsArrDepSum");
            if (bShowReport)
            {
                btnStatistAccToArrDep.Enabled = true;
            }
            else
            {
                //FlightsArrDepSum
                prv = LoginControl.GetPrivileges("FlightsArrDepSum");
                if (prv == "0")
                {
                    btnStatistAccToArrDep.Enabled = false;
                }
                else if (prv == "-")
                {
                    btnStatistAccToArrDep.Visible = false;
                }
                else if (prv == "1")
                {
                    btnStatistAccToArrDep.Visible = true;
                }
                else 
                {
                    btnStatistAccToArrDep.Visible = false;
                }
            }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("DailyFlightLog");
             if (bShowReport)
             {
                 btnDailyFlightLog.Enabled = true;
             }
             else
             {
                 //DailyFlightLog
                 prv = LoginControl.GetPrivileges("DailyFlightLog");
                 if (prv == "0")
                 {
                     btnDailyFlightLog.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnDailyFlightLog.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnDailyFlightLog.Visible = true;
                 }
                 else 
                 {
                     btnDailyFlightLog.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("FlightsAccDIC");
             if (bShowReport)
             {
                 btnStatistDomesticIntComb.Enabled = true;
             }
             else
             {
                 //FlightsAccDIC,Button,B,0";
                 prv = LoginControl.GetPrivileges("FlightsAccDIC");
                 if (prv == "0")
                 {
                     btnStatistDomesticIntComb.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnStatistDomesticIntComb.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnStatistDomesticIntComb.Visible = true;
                 }
                 else 
                 {
                     btnStatistDomesticIntComb.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("Aircraft");
             if (bShowReport)
             {
                 btnAircraftMovementsPerHour.Enabled = true;
             }
             else
             {
                 //Aircraft,Button,B,0";
                 prv = LoginControl.GetPrivileges("Aircraft");
                 if (prv == "0")
                 {
                     btnAircraftMovementsPerHour.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnAircraftMovementsPerHour.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnAircraftMovementsPerHour.Visible = true;
                 }
                 else 
                 {
                     btnAircraftMovementsPerHour.Visible = false;
                 }
             }
            bShowReport = false;
            bShowReport =_ReportList.ContainsKey("OvernightFlights");
             if (bShowReport)
             {
                 btnOvernightFlights.Enabled = true;
             }
             else
             {
                 //OvernightFlights,Button,B,0";
                 prv = LoginControl.GetPrivileges("OvernightFlights");
                 if (prv == "0")
                 {
                     btnOvernightFlights.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnOvernightFlights.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnOvernightFlights.Visible = true;
                 }
                 else 
                 {
                     btnOvernightFlights.Visible = false;
                 }
             }
            bShowReport = false;
            bShowReport =_ReportList.ContainsKey("OnightFSummary");
            if (bShowReport)
            {
                btnStatistOvernightArrSummary.Enabled = true;
            }
            else
            {
                //OnightFSummary,Button,B,0";
                prv = LoginControl.GetPrivileges("OnightFSummary");
                if (prv == "0")
                {
                    btnStatistOvernightArrSummary.Enabled = false;
                }
                else if (prv == "-")
                {
                    btnStatistOvernightArrSummary.Visible = false;
                }
                else if (prv == "1")
                {
                    btnStatistOvernightArrSummary.Visible = true;
                }
                else 
                {
                    btnStatistOvernightArrSummary.Visible = false;
                }
            }
            bShowReport = false;
            bShowReport =_ReportList.ContainsKey("CFlights");
            if (bShowReport)
            {
                btnStatisticCancelledFlights.Enabled = true;
            }
            else
            {
                //CFlights,Button,B,0";
                prv = LoginControl.GetPrivileges("CFlights");
                if (prv == "0")
                {
                    btnStatisticCancelledFlights.Enabled = false;
                }
                else if (prv == "-")
                {
                    btnStatisticCancelledFlights.Visible = false;
                }
                else if (prv == "1")
                {
                    btnStatisticCancelledFlights.Visible = true;
                }
                else 
                {
                    btnStatisticCancelledFlights.Visible = false;
                }
            }
            bShowReport = false;
            bShowReport =_ReportList.ContainsKey("CFlightsSummary");
            if (bShowReport)
            {
                btnStatistCancelledFlightSummary.Enabled = true;
            }
            else
            {
                //CFlightsSummary,B,0";
                prv = LoginControl.GetPrivileges("CFlightsSummary");
                if (prv == "0")
                {
                    btnStatistCancelledFlightSummary.Enabled = false;
                }
                else if (prv == "-")
                {
                    btnStatistCancelledFlightSummary.Visible = false;
                }
                else if (prv == "1")
                {
                    btnStatistCancelledFlightSummary.Visible = true;
                }
                else 
                {
                    btnStatistCancelledFlightSummary.Visible = false;
                }
            }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("DelayFlights");
             if (bShowReport)
             {
                 button4.Enabled = true;
             }
             else
             {
                 //DelayFlights,Button,B,0";
                 prv = LoginControl.GetPrivileges("DelayFlights");
                 if (prv == "0")
                 {
                     button4.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     button4.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     button4.Visible = true;
                 }
                 else 
                 {
                     button4.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("Deicing");
             if (bShowReport)
             {
                 btnDeIcing.Enabled = true;
             }
             else
             {
                 //Deicing,Button,B,0";
                 prv = LoginControl.GetPrivileges("Deicing");
                 if (prv == "0")
                 {
                     btnDeIcing.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnDeIcing.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnDeIcing.Visible = true;
                 }
                 else 
                 {
                     btnDeIcing.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("ChuteAllocation");
             if (bShowReport)
             {
                 buttonChuteAllocation.Enabled = true;
             }
             else
             {
                 //ChuteAllocation,Button,B,0";
                 prv = LoginControl.GetPrivileges("ChuteAllocation");
                 if (prv == "0")
                 {
                     buttonChuteAllocation.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     buttonChuteAllocation.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     buttonChuteAllocation.Visible = true;
                 }
                 else 
                 {
                     buttonChuteAllocation.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("DelayReasons");
             if (bShowReport)
             {
                 button8.Enabled = true;
             }
             else
             {
                 //DelayReasons,Button,B,0";
                 prv = LoginControl.GetPrivileges("DelayReasons");
                 if (prv == "0")
                 {
                     button8.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     button8.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     button8.Visible = true;
                 }
                 else 
                 {
                     button8.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("Seasonal");
             if (bShowReport)
             {
                 btnFlightViewer.Enabled = true;
             }
             else
             {
                 //Seasonal
                 prv = LoginControl.GetPrivileges("Seasonal");
                 if (prv == "0")
                 {
                     btnFlightViewer.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnFlightViewer.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnFlightViewer.Visible = true;
                 }
                 else 
                 {
                     btnFlightViewer.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("AirCraftOnGround");
             if (bShowReport)
             {
                 btnAircraftOnGround.Enabled = true;
             }
             else
             {
                 //OnGround
                 prv = LoginControl.GetPrivileges("AirCraftOnGround");
                 if (prv == "0")
                 {
                     btnAircraftOnGround.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnAircraftOnGround.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnAircraftOnGround.Visible = true;
                 }
                 else 
                 {
                     btnAircraftOnGround.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("AirCraftLanding");
             if (bShowReport)
             {
                 btnAircraftLanding.Enabled = true;
             }
             else
             {
                 //Landing
                 prv = LoginControl.GetPrivileges("AirCraftLanding");
                 if (prv == "0")
                 {
                     btnAircraftLanding.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnAircraftLanding.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnAircraftLanding.Visible = true;
                 }
                 else 
                 {
                     btnAircraftLanding.Visible = false;
                 }
             }
              
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("ChkCAllocation");
             if (bShowReport)
             {
                 btnCounterSchedule.Enabled = true;
             }
             else
             {
                 //ChkCAllocation,Button,B,0";
                 prv = LoginControl.GetPrivileges("ChkCAllocation");
                 if (prv == "0")
                 {
                     btnCounterSchedule.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnCounterSchedule.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnCounterSchedule.Visible = true;
                 }
                 else 
                 {
                     btnCounterSchedule.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("ChkCUsage");
             if (bShowReport)
             {
                 btnStatistRushHour.Enabled = true;
             }
             else
             {
                 //ChkCUsage,Button,B,0";
                 prv = LoginControl.GetPrivileges("ChkCUsage");
                 if (prv == "0")
                 {
                     btnStatistRushHour.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnStatistRushHour.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnStatistRushHour.Visible = true;
                 }
                 else
                 {
                     btnStatistRushHour.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("PositionUsage");
             if (bShowReport)
             {
                 btnSpotUsingRate.Enabled = true;
             }
             else
             {
                 //PositionUsage,Button,B,0";             
                 prv = LoginControl.GetPrivileges("PositionUsage");
                 if (prv == "0")
                 {
                     btnSpotUsingRate.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnSpotUsingRate.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnSpotUsingRate.Visible = true;
                 }
                 else 
                 {
                     btnSpotUsingRate.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("BgBeltUsage");
             if (bShowReport)
             {
                 btnBeltAllocations.Enabled = true;
             }
             else
             {
                 //BgBeltUsage,Button,B,0";
                 prv = LoginControl.GetPrivileges("BgBeltUsage");
                 if (prv == "0")
                 {
                     btnBeltAllocations.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnBeltAllocations.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnBeltAllocations.Visible = true;
                 }
                 else 
                 {
                     btnBeltAllocations.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("BeltAllocation");
             if (bShowReport)
             {
                 butBeltAllocation.Enabled = true;
             }
             else
             {
                 //BeltAllocation,Button,B,0";
                 prv = LoginControl.GetPrivileges("BeltAllocation");
                 if (prv == "0")
                 {
                     butBeltAllocation.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     butBeltAllocation.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     butBeltAllocation.Visible = true;
                 }
                 else 
                 {
                     butBeltAllocation.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("GateUsage");
             if (bShowReport)
             {
                 btnGateUsage.Enabled = true;
             }
             else
             {
                 //GateUsage,Button,B,0";
                 prv = LoginControl.GetPrivileges("GateUsage");
                 if (prv == "0")
                 {
                     btnGateUsage.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnGateUsage.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnGateUsage.Visible = true;
                 }
                 else 
                 {
                     btnGateUsage.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("GPUUsage");
             if (bShowReport)
             {
                 btnGPUUsage.Enabled = true;
             }
             else
             {
                 //GPUUsage,Button,B,0";
                 prv = LoginControl.GetPrivileges("GPUUsage");
                 if (prv == "0")
                 {
                     btnGPUUsage.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnGPUUsage.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnGPUUsage.Visible = true;
                 }
                 else
                 {
                     btnGPUUsage.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("UnavlResource");
             if (bShowReport)
             {
                 button1.Enabled = true;
             }
             else
             {
                 //UnavlResource,Button,B,0";
                 prv = LoginControl.GetPrivileges("UnavlResource");
                 if (prv == "0")
                 {
                     button1.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     button1.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     button1.Visible = true;
                 }
                 else 
                 {
                     button1.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("Inventory");
             if (bShowReport)
             {
                 btnInventory.Enabled = true;
             }
             else
             {
                 //Inventory,Button,B,0";
                 prv = LoginControl.GetPrivileges("Inventory");
                 if (prv == "0")
                 {
                     btnInventory.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnInventory.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnInventory.Visible = true;
                 }
                 else 
                 {
                     btnInventory.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("LoadPax");
             if (bShowReport)
             {
                 button_ConfigLoadPax.Enabled = true;
             }
             else
             {
                 //LoadPax,Button,B,0";
                 prv = LoginControl.GetPrivileges("LoadPax");
                 if (prv == "0")
                 {
                     button_ConfigLoadPax.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     button_ConfigLoadPax.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     button_ConfigLoadPax.Visible = true;
                 }
                 else 
                 {
                     button_ConfigLoadPax.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("Reason");
             if (bShowReport)
             {
                 button7.Enabled = true;
             }
             else
             {
                 //Reason,Button,B,0";
                 prv = LoginControl.GetPrivileges("Reason");
                 if (prv == "0")
                 {
                     button7.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     button7.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     button7.Visible = true;
                 }
                 else 
                 {
                     button7.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("NightOpr");
             if (bShowReport)
             {
                 btnNightOperations.Enabled = true;
             }
             else
             {
                 //NightOpr,Button,B,0";
                 prv = LoginControl.GetPrivileges("NightOpr");
                 if (prv == "0")
                 {
                     btnNightOperations.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnNightOperations.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnNightOperations.Visible = true;
                 }
                 else 
                 {
                     btnNightOperations.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("NightOprSumary");
             if (bShowReport)
             {
                 btnNightOperationsSummary.Enabled = true;
             }
             else
             {
                 //NightOprSumary,Button,B,0";
                 prv = LoginControl.GetPrivileges("NightOprSumary");
                 if (prv == "0")
                 {
                     btnNightOperationsSummary.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnNightOperationsSummary.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnNightOperationsSummary.Visible = true;
                 }
                 else 
                 {
                     btnNightOperationsSummary.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("AbnorFlights");
             if (bShowReport)
             {
                 button2.Enabled = true;
             }
             else
             {
                 //AbnorFlights,Button,B,0";
                 prv = LoginControl.GetPrivileges("AbnorFlights");
                 if (prv == "0")
                 {
                     button2.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     button2.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     button2.Visible = true;
                 }
                 else 
                 {
                     button2.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport =_ReportList.ContainsKey("TowingReport");
             if (bShowReport)
             {
                 btnTowingReport.Enabled = true;
             }
             else
             {
                 //Towing,Button,B,0";
                 prv = LoginControl.GetPrivileges("TowingReport");
                 if (prv == "0")
                 {
                     btnTowingReport.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnTowingReport.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnTowingReport.Visible = true;
                 }
                 else
                 {
                     btnTowingReport.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport = _ReportList.ContainsKey("FlightAllocationPlan");
             if (bShowReport)
             {
                 btnFlightAllocation .Enabled = true;
             }
             else
             {
                 //FlightAllocationPlan,Button,B,0";
                 prv = LoginControl.GetPrivileges("FlightAllocationPlan");
                 if (prv == "0")
                 {
                     btnFlightAllocation.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                     btnFlightAllocation.Visible = false;
                 }
                 else if (prv == "1")
                 {
                     btnFlightAllocation.Visible = true;
                 }
                 else
                 {
                     btnFlightAllocation.Visible = false;
                 }
             }
             bShowReport = false;
             bShowReport = _ReportList.ContainsKey("SeatStatisDomIntComb");
             if (bShowReport)
             {
                  btnSeatStatisDomIntComb.Enabled = true;
             }
             else
             {
                 //FlightAllocationPlan,Button,B,0";
                  prv = LoginControl.GetPrivileges("SeatStatisDomIntComb");
                 if (prv == "0")
                 {
                      btnSeatStatisDomIntComb.Enabled = false;
                 }
                 else if (prv == "-")
                 {
                      btnSeatStatisDomIntComb.Visible = false;
                 }
                 else if (prv == "1")
                 {
                      btnSeatStatisDomIntComb.Visible = true;
                 }
                 else
                 {
                      btnSeatStatisDomIntComb.Visible = false;
                 }
             }
             
            
        }        
        /// <summary>
        /// De Icing Report
        /// </summary>
        private void InitDeIce()
        {
            string st = Ctrl.CtrlConfig.GetConfigInfo(Environment.GetEnvironmentVariable("CEDA").ToString(), "FIPSREPORT", Ctrl.CtrlDeIce.CEDA_INI_CONFIG, "N");
            if (Ctrl.CtrlConfig.GetConfigInfo(Environment.GetEnvironmentVariable("CEDA").ToString(), "FIPSREPORT", Ctrl.CtrlDeIce.CEDA_INI_CONFIG, "N") == "Y")
            {
                Ctrl.CtrlDeIce.GetInstance().Init(this.LoginControl);
                this.btnDeIcing.Visible = true;
            }
            else
            {
                this.btnDeIcing.Visible = false;
            }
            
        }        
        /// <summary>
        /// Load Scenario
        /// </summary>
        private void loadScenario()
        {
            ITable Sco = myDB.Bind("SDP", "SDP", "STXN", "10", "STXN");
            Sco.Load("ORDER BY STXN");
            cboScenario.Items.Add("<None>");            
            for (int i = 0; i < Sco.Count; i++)
            {
                string strValue = Sco[i]["STXN"];
                cboScenario.Items.Add(strValue); 
            }
            if (cboScenario.Items.Count > 0)
            {
                cboScenario.SelectedIndex = 0;
            }
        }        
        private bool IsColumnForAbnormalFlightsReportAvailable()
        {
            if (myDB == null)
                return false;
            return myDB.Reader.CedaAction("RT", "SYSTAB", "TANA,FINA", "", "WHERE Tana = 'DEN' AND Fina = 'RCOL'") && myDB.Reader.GetBufferCount() == 1;
        }
        /// <summary>
        /// This function pops up the login dialog and opens the application or closes the application
        /// depending on the user rights.This is done for the PRF 8523
        /// </summary>
        /// <returns></returns>
        private bool LoginProcedure()
        {
            string LoginAnsw = "";
            string tmpRegStrg = "";
            string strRet = "";

            LoginControl.ApplicationName = "FIPSReport";
            LoginControl.VersionString = Application.ProductVersion;
            LoginControl.InfoCaption = "Info about FIPSReport";
            LoginControl.InfoButtonVisible = true;
            LoginControl.InfoUfisVersion = "Ufis Version 4.5";
            LoginControl.InfoAppVersion = "FIPSReport " + Application.ProductVersion.ToString();
            LoginControl.InfoAAT = "UFIS Airport Solutions GmbH";
            LoginControl.UserNameLCase = true;

            tmpRegStrg = "FIPSReport" + ",";
            tmpRegStrg += "InitModu,InitModu,Initialisieren (InitModu),B,1";
            tmpRegStrg += ",Main Menu,FlightsByNature,Flights By Natures/Aircraft/Airlines/Origin/Destinations/Reg,B,1";
            tmpRegStrg += ",Main Menu,FlightsArrDep,Flights According to Arrival/Departure,B,1";
            tmpRegStrg += ",Main Menu,FlightsArrDepSum,Flights According to Arrival/Departure Summary,B,1";
            tmpRegStrg += ",Main Menu,DailyFlightLog,Daily Flight Log,B,1";
            tmpRegStrg += ",Main Menu,FlightsAccDIC,Flights According to Domestic/International/Combined,B,1";
            tmpRegStrg += ",Main Menu,Aircraft,Aaircraft Movements per Hour,B,1";
            tmpRegStrg += ",Main Menu,OvernightFlights,Overnight Flights,B,1";
            tmpRegStrg += ",Main Menu,OnightFSummary,Overnight Flights Summary,B,1";
            tmpRegStrg += ",Main Menu,CFlights,Cancelled Flights,B,1";
            tmpRegStrg += ",Main Menu,CFlightsSummary,Cancelled Flights Summary,B,1";
            tmpRegStrg += ",Main Menu,DelayFlights,Delayed Flights,B,1";
            tmpRegStrg += ",Main Menu,Deicing,De icing,B,1";
            tmpRegStrg += ",Main Menu,ChuteAllocation,Chute Allocation,B,1";
            tmpRegStrg += ",Main Menu,DelayReasons,Delay Reasons Summary,B,1";
            tmpRegStrg += ",Main Menu,Seasonal,Seasonal Flight Viewer,B,1";
            tmpRegStrg += ",Main Menu,ChkCAllocation,Check in Counter Allocation,B,1";
            tmpRegStrg += ",Main Menu,ChkCUsage,Check in Counter Usage,B,1";
            tmpRegStrg += ",Main Menu,PositionUsage,Position Usage,B,1";
            tmpRegStrg += ",Main Menu,BgBeltUsage,Baggage Belt Usage,B,1";
            tmpRegStrg += ",Main Menu,BeltAllocation,Belt Allocation Plan,B,1";
            tmpRegStrg += ",Main Menu,GateUsage,Gate Usage,B,1";
            tmpRegStrg += ",Main Menu,GPUUsage,GPU Usage,B,1";
            tmpRegStrg += ",Main Menu,UnavlResource,Unavailable Resources,B,1";
            tmpRegStrg += ",Main Menu,Inventory,Inventory A/C on Ground,B,1";
            tmpRegStrg += ",Main Menu,LoadPax,Load and Pax,B,1";
            tmpRegStrg += ",Main Menu,Reason,Reason for Changes,B,1";
            tmpRegStrg += ",Main Menu,NightOpr,Night Operation,B,1";
            tmpRegStrg += ",Main Menu,AirCraftOnGround,Aircraft On Ground Report,B,1";
            tmpRegStrg += ",Main Menu,AirCraftLanding,Aircraft Landing Permission,B,1";
            tmpRegStrg += ",Main Menu,NightOprSumary,Night Operation Summary,B,1";                        
            tmpRegStrg += ",Main Menu,FlightAllocationPlan,Flight Allocation Plan,B,1";
            tmpRegStrg += ",Main Menu,TowingReport,Towing Report,B,1";
            tmpRegStrg += ",Main Menu,STDPieChart,Standard Report Pie Chart,B,1";
            tmpRegStrg += ",Main Menu,SeatStatisDomIntComb,Seats According to Airline/Destination/Combined,B,1";
             
            
            
            LoginControl.RegisterApplicationString = tmpRegStrg;
            /*			
            args = new string[3];

            args[0] =  "UFIS$ADMIN";
            args[1] =  "Passwort";
            */
           
            if (args.Length == 0)
            {
                strRet = LoginControl.ShowLoginDialog();

            }
            else
            {
                string userId = args[0];
                string password = "";
                bool hasUserIdNPwd = false;
                string[] stParams = args[0].Split('|');
                if (stParams.Length >= 4)
                {
                    string[] stTemps = stParams[1].Split(',');
                    userId = stTemps[0];
                    password = stParams[3];
                    hasUserIdNPwd = true;
                }
                else if (args.Length == 2)
                {
                    password = args[1];
                    hasUserIdNPwd = true;
                }
                if (hasUserIdNPwd)
                {
                    strRet = LoginControl.DoLoginSilentMode(userId, password);
                }
                if (strRet != "OK")
                    strRet = LoginControl.ShowLoginDialog();

            }

            LoginAnsw = LoginControl.GetPrivileges("InitModu");
            UT.UserName = LoginControl.GetUserName();
            
             //LoginControl.GetTableExt();
            if (LoginAnsw != "" && strRet != "CANCEL")
            {
                try
                {
                    if (LoginControl.GetTableExt().ToString().Trim() != "" && LoginControl.GetTableExt().ToString().Trim() != "TAB")
                    {
                        sCurrentScenario = "Current Scenario : " + LoginControl.GetTableExt().ToString().Trim() + " - " + GetScenarioName(LoginControl.GetTableExt().ToString());
                    }
                }
                catch { }
                return true;
            }
            else
            {
                return false;
            }
        }
        private string GetScenarioName(string sScenarioNo)
        {
            string sReturnString = string.Empty;

            ITable SSD = myDB.Bind("SDP", "SDP", "STXN,SSDT", "10", "STXN,SSDT");
            SSD.Load("WHERE STXN = " + sScenarioNo);                     
            sReturnString = SSD[0]["SSDT"];               
            return sReturnString;
        }        
        /// <summary>
     /// Get List of report by Ceda.ini (REPORT_LIST)
     /// </summary>
     /// <param name="sRptList">System.string containing sRptList</param>
        private void ConstructReportList(string sRptList)
        {
            //Zaw Min Tun / MZA
            //2013-03-04
            //Refer to Comment:20130304112201
            //By-chance patch while working on UFIS-3084
            _ReportList = new Dictionary<string, string>();

            if (!String.IsNullOrEmpty(sRptList))
            {
                //Commented by Zaw Min Tun / MZA
                //2013-03-04
                //Because this can raise the error : Object reference not set to an instance of an object.
                //Comment:20130304112201
                //_ReportList = new Dictionary<string, string>();

                string[] ArrRptList = sRptList.Split(',');
                foreach (string s in ArrRptList)
                {
                    switch (s)
                    {
                        case "1":
                            _ReportList.Add("FlightsByNature", s);
                            break;
                        case "2":
                            _ReportList.Add("FlightsArrDep", s);
                            break;
                        case "3":
                            _ReportList.Add("FlightsArrDepSum", s);
                            break;
                        case "4":
                            _ReportList.Add("DailyFlightLog", s);
                            break;
                        case "5":
                            _ReportList.Add("FlightsAccDIC", s);
                            break;
                        case "6":
                            _ReportList.Add("Aircraft", s);
                            break;
                        case "7":
                            _ReportList.Add("OvernightFlights", s);
                            break;
                        case "8":
                            _ReportList.Add("OnightFSummary", s);
                            break;
                        case "9":
                            _ReportList.Add("CFlights", s);
                            break;
                        case "10":
                            _ReportList.Add("CFlightsSummary", s);
                            break;
                        case "11":
                            _ReportList.Add("DelayFlights", s);
                            break;
                        case "12":
                            _ReportList.Add("Deicing", s);
                            break;
                        case "13":
                            _ReportList.Add("ChuteAllocation", s);
                            break;
                        case "14":
                            _ReportList.Add("DelayReasons", s);
                            break;
                        case "15":
                            _ReportList.Add("Seasonal", s);
                            break;
                        case "16":
                            _ReportList.Add("AirCraftOnGround", s);
                            break;
                        case "17":
                            _ReportList.Add("AirCraftLanding", s);
                            break;
                        case "18":
                            _ReportList.Add("ChkCAllocation", s);
                            break;
                        case "19":
                            _ReportList.Add("ChkCUsage", s);
                            break;
                        case "20":
                            _ReportList.Add("PositionUsage", s);
                            break;
                        case "21":
                            _ReportList.Add("BgBeltUsage", s);
                            break;
                        case "22":
                            _ReportList.Add("BeltAllocation", s);
                            break;
                        case "23":
                            _ReportList.Add("GateUsage", s);
                            break;
                        case "24":
                            _ReportList.Add("GPUUsage", s);
                            break;
                        case "25":
                            _ReportList.Add("UnavlResource", s);
                            break;
                        case "26":
                            _ReportList.Add("Inventory", s);
                            break;
                        case "27":
                            _ReportList.Add("LoadPax", s);
                            break;
                        case "28":
                            _ReportList.Add("Reason", s);
                            break;
                        case "29":
                            _ReportList.Add("NightOpr", s);
                            break;
                        case "30":
                            _ReportList.Add("NightOprSumary", s);
                            break;
                        case "31":
                            _ReportList.Add("AbnorFlights", s);
                            break;
                        case "32":
                            _ReportList.Add("Towing", s);
                            break; 
                         case "33":
                              _ReportList.Add("BaggageBelt",s);
                              break;
                         case "34":
                              _ReportList.Add("OnGround",s);
                              break;
                         case "35":
                              _ReportList.Add("LandingPermission",s);
                              break;
                         case "36":
                              _ReportList.Add("SeatStatisDomIntComb", s);
                              break;
                        case "37":
                              _ReportList.Add("FlightAllocationPlan",s);
                              break;
                    }       
                }
            }
        }
        #endregion

        #region #### No Use Code ####
        /// <summary>
        /// Checking Button Location from left to right
        /// </summary>
        private void CheckingButtonLocation()
        {
             int y;
             if (button8.Visible == false)
             {
                  y = button8.Location.Y;
                  btnFlightViewer.Location = button8.Location;
                  btnAircraftOnGround.Location = new Point(btnFlightViewer.Location.X, y + 26);
                  btnAircraftLanding.Location = new Point(btnFlightViewer.Location.X, y + 52);
                  btnTowingReport.Location = new Point(btnTowingReport.Location.X, y + 78);
             }
             if (buttonChuteAllocation.Visible == false)
             {
                  y = buttonChuteAllocation.Location.Y;
                  btnFlightViewer.Location = buttonChuteAllocation.Location;
                  btnAircraftOnGround.Location = new Point(btnFlightViewer.Location.X, y + 26);
                  btnAircraftLanding.Location = new Point(btnFlightViewer.Location.X, y + 52);
                  btnTowingReport.Location = new Point(btnTowingReport.Location.X, y + 78);
             }
             if (btnDeIcing.Visible == false)
             {
                  y = btnDeIcing.Location.Y;
                  btnFlightViewer.Location = btnDeIcing.Location;
                  btnAircraftOnGround.Location = new Point(btnFlightViewer.Location.X, y + 26);
                  btnAircraftLanding.Location = new Point(btnFlightViewer.Location.X, y + 52);
                  btnTowingReport.Location = new Point(btnTowingReport.Location.X, y + 78);
             }
             if (btnNightOperationsSummary.Visible == false)
             {
                  y = btnNightOperations.Location.Y;
                  btnFlightAllocation.Location = btnNightOperationsSummary.Location;
                  btnAircraftOnGround.Location = new Point(btnFlightViewer.Location.X, y + 26);
                  btnAircraftLanding.Location = new Point(btnFlightViewer.Location.X, y + 52);
                  btnTowingReport.Location = new Point(btnTowingReport.Location.X, y + 78);
             }
             if (btnNightOperations.Visible == false)
             {
                  y = btnNightOperations.Location.Y;
                  btnFlightAllocation.Location = btnNightOperations.Location;
                  btnAircraftOnGround.Location = new Point(btnFlightViewer.Location.X, y + 26);
                  btnAircraftLanding.Location = new Point(btnFlightViewer.Location.X, y + 52);
                  btnTowingReport.Location = new Point(btnTowingReport.Location.X, y + 78);
             }
        }
        #endregion

    }
}