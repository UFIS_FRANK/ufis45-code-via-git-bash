using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Data;
using Ufis.Utils;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmCancelled_Flights.
	/// </summary>
	public class frmCancelled_Flights : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statement: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhere = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('A','B'))) OR (STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B')))) OR " +
			"((TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('A','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))))) AND FTYP='X'" ;
		/// <summary>
		/// The where clause for airline filtration
		/// </summary>
		private string strWhereAirlinePart = " AND ((ALC2 = '@@ALC2') OR (ALC3 = '@@ALC3'))";
		/// <summary>
		/// The where clause for cancellation reason filtration
		/// </summary>
		private string strWhereReasonPart = " AND (CXXR = '@@CXXR')";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "SCHEDULE,FLNO,ADID,ORGDES,ACTYPE,REMARK1,REMARK2,REASON";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		private string strTabHeader =     "Schedule,Flight  ,A/D  ,Org/Des  ,A/C-Type,Remark1,Remark2,Reason ";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "70,70,30,80,70,100,100,100";
		/// <summary>
		/// The lengths, which must be used for the report's excel export column widths
		/// </summary>
		private string strExcelTabHeaderLens = "135,70,30,80,70,300,300,300";
		/// <summary>
		/// String for empty line generation in the tab control.
		/// </summary>
		private string strEmptyLine = ",,,,,,,";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the airline code
		/// </summary>
		private ITable myALT;
		/// <summary>
		/// ITable object for the cancellation reason code
		/// </summary>
		private ITable myCRC;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		/// <summary>
		/// Datetime format for excel export
		/// </summary>
		private string strExcelDateTimeFormat = "";
		/// <summary>
		/// Datetime format for report
		/// </summary>
		private string strReportDateTimeFormat = "dd'/'HH:mm";
		/// <summary>
		/// Datetime format for table
		/// </summary>
		private string strDateDisplayFormat = "";
		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;
		/// <summary>
		/// List for airline 2L data
		/// </summary>
		private System.Collections.ArrayList listAlc2 = null;
		/// <summary>
		/// List for airline 3L data
		/// </summary>
		private System.Collections.ArrayList listAlc3 = null;
		/// <summary>
		/// Boolean to suggest if "Reason" column should be visible or not
		/// </summary>
		private bool bmReasonForCXX = true;

		#endregion _My Members

		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private AxTABLib.AxTAB tabExcelResult;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button buttonHelp;
		private System.Windows.Forms.RadioButton radioButton2L;
		private System.Windows.Forms.RadioButton radioButton3L;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox cbAirline;
		private System.Windows.Forms.ComboBox cbReason;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmCancelled_Flights()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmCancelled_Flights));
			this.panelTop = new System.Windows.Forms.Panel();
			this.buttonHelp = new System.Windows.Forms.Button();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.tabExcelResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.radioButton2L = new System.Windows.Forms.RadioButton();
			this.radioButton3L = new System.Windows.Forms.RadioButton();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.cbAirline = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.cbReason = new System.Windows.Forms.ComboBox();
			this.panelTop.SuspendLayout();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.cbReason);
			this.panelTop.Controls.Add(this.label5);
			this.panelTop.Controls.Add(this.cbAirline);
			this.panelTop.Controls.Add(this.label4);
			this.panelTop.Controls.Add(this.label3);
			this.panelTop.Controls.Add(this.radioButton3L);
			this.panelTop.Controls.Add(this.radioButton2L);
			this.panelTop.Controls.Add(this.buttonHelp);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(720, 152);
			this.panelTop.TabIndex = 0;
			// 
			// buttonHelp
			// 
			this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonHelp.Location = new System.Drawing.Point(396, 112);
			this.buttonHelp.Name = "buttonHelp";
			this.buttonHelp.TabIndex = 30;
			this.buttonHelp.Text = "Help";
			this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(488, 96);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 22;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(488, 112);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 6;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 112);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 4;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 112);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 3;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 112);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "&Close";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 112);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 2;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(268, 4);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(92, 4);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			this.label1.Click += new System.EventHandler(this.label1_Click);
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 152);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(720, 310);
			this.panelBody.TabIndex = 1;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Controls.Add(this.tabExcelResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(720, 294);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(720, 294);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// tabExcelResult
			// 
			this.tabExcelResult.ContainingControl = this;
			this.tabExcelResult.Location = new System.Drawing.Point(0, 0);
			this.tabExcelResult.Name = "tabExcelResult";
			this.tabExcelResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabExcelResult.OcxState")));
			this.tabExcelResult.Size = new System.Drawing.Size(100, 50);
			this.tabExcelResult.TabIndex = 1;
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(720, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// radioButton2L
			// 
			this.radioButton2L.Location = new System.Drawing.Point(92, 38);
			this.radioButton2L.Name = "radioButton2L";
			this.radioButton2L.Size = new System.Drawing.Size(36, 24);
			this.radioButton2L.TabIndex = 31;
			this.radioButton2L.Text = "2L";
			this.radioButton2L.CheckedChanged += new System.EventHandler(this.radioButton2L_CheckedChanged);
			// 
			// radioButton3L
			// 
			this.radioButton3L.Location = new System.Drawing.Point(128, 38);
			this.radioButton3L.Name = "radioButton3L";
			this.radioButton3L.Size = new System.Drawing.Size(36, 24);
			this.radioButton3L.TabIndex = 32;
			this.radioButton3L.Text = "3L";
			this.radioButton3L.CheckedChanged += new System.EventHandler(this.radioButton3L_CheckedChanged);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 41);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(60, 16);
			this.label3.TabIndex = 33;
			this.label3.Text = "Airline:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(120, 42);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(8, 16);
			this.label4.TabIndex = 34;
			this.label4.Text = "/";
			// 
			// cbAirline
			// 
			this.cbAirline.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbAirline.Location = new System.Drawing.Point(168, 38);
			this.cbAirline.Name = "cbAirline";
			this.cbAirline.Size = new System.Drawing.Size(228, 21);
			this.cbAirline.TabIndex = 35;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 80);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(60, 16);
			this.label5.TabIndex = 36;
			this.label5.Text = "Reason:";
			// 
			// cbReason
			// 
			this.cbReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbReason.Location = new System.Drawing.Point(92, 76);
			this.cbReason.Name = "cbReason";
			this.cbReason.Size = new System.Drawing.Size(304, 21);
			this.cbReason.TabIndex = 37;
			// 
			// frmCancelled_Flights
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(720, 462);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmCancelled_Flights";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Cancelled Flights";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmFlightByNatureCode_Closing);
			this.Load += new System.EventHandler(this.frmFlightByNatureCode_Load);
			this.HelpRequested += new HelpEventHandler(frmCancelled_Flights_HelpRequested);
			this.panelTop.ResumeLayout(false);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
		
		/// <summary>
		/// Load the form. Prepare and init all controls.
		/// </summary>
		/// <param name="sender">Form.</param>
		/// <param name="e">Event arguments.</param>
		private void frmFlightByNatureCode_Load(object sender, System.EventArgs e)
		{			 
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
			PrepareFilterData();
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}
		/// <summary>
		/// Initializes the tab controll.
		/// </summary>
		private void InitTab()
		{
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");			
			if(myIni.IniReadValue("FIPS","REASON_FOR_CXX").CompareTo("TRUE") != 0)
			{
				bmReasonForCXX = false;
				this.cbReason.Visible = false;
				this.label5.Visible = false;
                Helper.RemoveOrReplaceString(7,ref strTabHeader,',',"",true);
				Helper.RemoveOrReplaceString(7,ref strLogicalFields,',',"",true);
				Helper.RemoveOrReplaceString(7,ref strTabHeaderLens,',',"",true);
				Helper.RemoveOrReplaceString(7,ref strExcelTabHeaderLens,',',"",true);
				Helper.RemoveOrReplaceString(7,ref strEmptyLine,',',"",true);
			}

			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;			

			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;

			tabExcelResult.ResetContent();
			tabExcelResult.HeaderString = strTabHeader;
			tabExcelResult.LogicalFieldList = strLogicalFields;
			tabExcelResult.HeaderLengthString = strTabHeaderLens;
			
			strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			strExcelDateTimeFormat = myIni.IniReadValue("FIPSREPORT", "DATEEXCELFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = strReportDateTimeFormat;
			}
			else
			{
				strReportDateTimeFormat = strDateDisplayFormat;
			}
			if(strExcelDateTimeFormat.Length == 0)
			{
				strExcelDateTimeFormat = strReportDateTimeFormat;
			}			
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
			myALT = myDB["ALT"];	
		    if(myALT == null)
			{
				myALT = myDB.Bind("ALT", "ALT", "ALC2,ALC3,ALFN", "2,3,60", "ALC2,ALC3,ALFN");
                myALT.Load("");
			}
			
			if(listAlc2 == null)
			{
				listAlc2 = new ArrayList();
			}
			if(listAlc3 == null)
			{
				listAlc3 = new ArrayList();
			}
			for(int i = 0; i < myALT.Count; i++)
			{
				string strValue = "";
				if(myALT[i]["ALC2"].Length > 0)
				{
					strValue = myALT[i]["ALC2"] + " | " + myALT[i]["ALFN"];
					listAlc2.Add(strValue);
				}
				if(myALT[i]["ALC3"].Length > 0)
				{
					strValue = myALT[i]["ALC3"] + " | " + myALT[i]["ALFN"];
					listAlc3.Add(strValue);
				}				
			}
			listAlc2.Sort();
			listAlc3.Sort();

			cbAirline.Items.Add("<None>");
			if(bmReasonForCXX == true)
			{
				myCRC = myDB["CRC"];
				if(myCRC == null)
				{
					myCRC = myDB.Bind("CRC", "CRC", "CODE,REMA", "5,128", "CODE,REMA");
					myCRC.Load("ORDER BY CODE");
				}
				
				cbReason.Items.Add("<None>");
				for(int i = 0; i < myCRC.Count; i++)
				{
					string strValue = "";
					if(myCRC[i]["CODE"].Length > 0)
					{					
						strValue += myCRC[i]["CODE"].Trim() + " | " + myCRC[i]["REMA"].Trim();
						cbReason.Items.Add(strValue);
					}
				}
				cbReason.SelectedIndex = 0;
			}
			cbAirline.SelectedIndex = 0;			
		}
		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData();
			//RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			//			TimeSpan span = dtTo.Value - dtFrom.Value;
			//			if(span.TotalDays > 10)
			//			{
			//				strRet += ilErrorCount.ToString() +  ". Please do not load more the 10 days!\n";
			//				ilErrorCount++;
			//			}
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpAFTWhere = strWhere;
			//Clear the tab
			tabResult.ResetContent();
			tabExcelResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}

			string strAirline = "";
			if(cbAirline.SelectedIndex != 0 && cbAirline.SelectedIndex != -1)
			{
				strAirline = cbAirline.Text.Split('|')[0].Trim();
			}
			string strReasonCode = "";
			if(cbReason.SelectedIndex != 0 && cbReason.SelectedIndex != -1)
			{
				strReasonCode = cbReason.Text.Split('|')[0].Trim();
			}

			//Load the data from AFTTAB
			myDB.Unbind("AFT");
			if(bmReasonForCXX == true)
			{
				myAFT = myDB.Bind("AFT", "AFT", "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,REM1,REM2,CXXR", "10,12,2,3,3,14,14,14,14,3,256,256,5", "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,REM1,REM2,CXXR");
			}
			else
			{
				myAFT = myDB.Bind("AFT", "AFT", "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,REM1,REM2", "10,12,2,3,3,14,14,14,14,3,256,256", "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,REM1,REM2");
			}
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.Clear();
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpAFTWhere = strWhere;
				if(strAirline.Length > 0)
				{
					strTmpAFTWhere += strWhereAirlinePart;
				}
				if(strReasonCode.Length > 0)
				{
					strTmpAFTWhere += strWhereReasonPart;
				}
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@TO", strDateTo );
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@ALC2",strAirline);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@ALC3",strAirline);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@CXXR",strReasonCode);

				//Load the data from AFTTAB
				myAFT.Load(strTmpAFTWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

		}
		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportData()
		{
			//"FLNO,ADID,ORGDES,SCHEDULE,ACTYPE,REMARK"
			int ilLine = 0;
			imArrivals = 0;
			imDepartures =0 ;
			imRotations = 0;
			imTotalFlights = 0;

			for(int i = 0; i < myAFT.Count; i++)
			{
				string strAdid = myAFT[i]["ADID"];
				if(strAdid == "A")
				{
					imArrivals++;
					imTotalFlights++;
					tabResult.InsertTextLine(strEmptyLine, false);
					ilLine = tabResult.GetLineCount() - 1;
					tabResult.SetFieldValues(ilLine, "FLNO", myAFT[i]["FLNO"]);
					tabResult.SetFieldValues(ilLine, "ADID", strAdid);
					tabResult.SetFieldValues(ilLine, "ORGDES", myAFT[i]["ORG3"]);
					tabResult.SetFieldValues(ilLine, "SCHEDULE", Helper.DateString(myAFT[i]["STOA"], strDateDisplayFormat));					
					tabResult.SetFieldValues(ilLine, "ACTYPE", myAFT[i]["ACT3"]);
					tabResult.SetFieldValues(ilLine, "REMARK1", myAFT[i]["REM1"]);
					tabResult.SetFieldValues(ilLine, "REMARK2", myAFT[i]["REM2"]);
					if(bmReasonForCXX == true)
					{
						tabResult.SetFieldValues(ilLine, "REASON", myAFT[i]["CXXR"]);
					}
					
					tabExcelResult.InsertTextLine(strEmptyLine, false);
					ilLine = tabExcelResult.GetLineCount() - 1;
					tabExcelResult.SetFieldValues(ilLine, "FLNO", myAFT[i]["FLNO"]);
					tabExcelResult.SetFieldValues(ilLine, "ADID", strAdid);
					tabExcelResult.SetFieldValues(ilLine, "ORGDES", myAFT[i]["ORG3"]);
					tabExcelResult.SetFieldValues(ilLine, "SCHEDULE", Helper.DateString(myAFT[i]["STOA"],strExcelDateTimeFormat));					
					tabExcelResult.SetFieldValues(ilLine, "ACTYPE", myAFT[i]["ACT3"]);
					tabExcelResult.SetFieldValues(ilLine, "REMARK1", myAFT[i]["REM1"]);
					tabExcelResult.SetFieldValues(ilLine, "REMARK2", myAFT[i]["REM2"]);
					if(bmReasonForCXX == true)
					{
						tabExcelResult.SetFieldValues(ilLine, "REASON", myAFT[i]["CXXR"]);
					}
				}
				if(strAdid == "D")
				{
					imDepartures++;
					imTotalFlights++;
					tabResult.InsertTextLine(strEmptyLine, false);
					ilLine = tabResult.GetLineCount() - 1;
					tabResult.SetFieldValues(ilLine, "FLNO", myAFT[i]["FLNO"]);
					tabResult.SetFieldValues(ilLine, "ADID", strAdid);
					tabResult.SetFieldValues(ilLine, "ORGDES", myAFT[i]["DES3"]);
					tabResult.SetFieldValues(ilLine, "SCHEDULE", Helper.DateString(myAFT[i]["STOD"], strDateDisplayFormat));
					tabResult.SetFieldValues(ilLine, "ACTYPE", myAFT[i]["ACT3"]);
					tabResult.SetFieldValues(ilLine, "REMARK1",myAFT[i]["REM1"]);
					tabResult.SetFieldValues(ilLine, "REMARK2",myAFT[i]["REM2"]);
					if(bmReasonForCXX == true)
					{
						tabResult.SetFieldValues(ilLine, "REASON",myAFT[i]["CXXR"]);
					}
					
					tabExcelResult.InsertTextLine(strEmptyLine, false);
					ilLine = tabExcelResult.GetLineCount() - 1;
					tabExcelResult.SetFieldValues(ilLine, "FLNO", myAFT[i]["FLNO"]);
					tabExcelResult.SetFieldValues(ilLine, "ADID", strAdid);
					tabExcelResult.SetFieldValues(ilLine, "ORGDES", myAFT[i]["DES3"]);
					tabExcelResult.SetFieldValues(ilLine, "SCHEDULE", Helper.DateString(myAFT[i]["STOD"],strExcelDateTimeFormat));
					tabExcelResult.SetFieldValues(ilLine, "ACTYPE", myAFT[i]["ACT3"]);
					tabExcelResult.SetFieldValues(ilLine, "REMARK1",myAFT[i]["REM1"]);
					tabExcelResult.SetFieldValues(ilLine, "REMARK2",myAFT[i]["REM2"]);
					if(bmReasonForCXX == true)
					{
						tabExcelResult.SetFieldValues(ilLine, "REASON",myAFT[i]["CXXR"]);
					}
				}
			}
			tabResult.Sort("0", true, true);
			tabExcelResult.Sort("0",true,true);
			//tabResult.AutoSizeColumns();
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " ARR: " + imArrivals.ToString();
			strSubHeader += " DEP: " + imDepartures.ToString() + ")";
			strReportHeader = "Cancelled Flights";
			strReportSubHeader = strSubHeader;
			rptFIPS rpt = new rptFIPS(tabResult,strReportHeader, strSubHeader, "", 10);
			rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLens);
			rpt.SetExcelExportTabResult(tabExcelResult);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			if(Helper.UseSpreadBuilder() == true)
			{
				activeReport = rpt;
				frm.UseSpreadBuilder(true);
				frm.GetBtnExcelExport().Click += new EventHandler(frmCancelled_Flights_Click);
			}
			else
			{
				frm.UseSpreadBuilder(false);
			}
			frm.Show();
		}
		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
						tabExcelResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
						tabExcelResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
					tabExcelResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmFlightByNatureCode_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("AFT");
		}

		private void label1_Click(object sender, System.EventArgs e)
		{
		
		}

		private void InitializeMouseEvents()
		{
			((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmCancelled_Flights_MouseWheel);
			((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmCancelled_Flights_MouseEnter);
			((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmCancelled_Flights_MouseLeave);
		}

		private void frmCancelled_Flights_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && tabResult.GetVScrollPos() > 0)
			{				
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
			{
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
			}
		}

		private void frmCancelled_Flights_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmCancelled_Flights_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmCancelled_Flights_Click(object sender, EventArgs e)
		{
			Helper.ExportToExcel(tabExcelResult,strReportHeader,strReportSubHeader,strExcelTabHeaderLens,10,0,activeReport);
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void radioButton2L_CheckedChanged(object sender, System.EventArgs e)
		{
			cbAirline.Items.Clear();
			cbAirline.ResetText();
			cbAirline.Items.Add("<None>");
			for(int i = 0 ; i < listAlc2.Count ; i++)
			{
				cbAirline.Items.Add(listAlc2[i]);
			}
			cbAirline.SelectedIndex = 0;
		}

		private void radioButton3L_CheckedChanged(object sender, System.EventArgs e)
		{
			cbAirline.Items.Clear();
			cbAirline.ResetText();
			cbAirline.Items.Add("<None>");
			for(int i = 0 ; i < listAlc3.Count ; i++)
			{
				cbAirline.Items.Add(listAlc3[i]);
			}
			cbAirline.SelectedIndex = 0;
		}

		private void frmCancelled_Flights_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}		
	}
}