using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmNightFlightSummary.
	/// </summary>
	public class frmNightFlightSummary : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		
		//		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROMDATE@@FROMTIME' AND '@@TODATE@@TOTIME' AND DES3='@@HOPO' ) OR ( STOD BETWEEN '@@FROMDATE@@FROMTIME' AND '@@TODATE@@TOTIME' AND ORG3='@@HOPO')) AND (FTYP='O' OR FTYP='S' OR FTYP='X')) AND STAT<>'DEL'";
		//		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROMDATE@@FROMTIME' AND '@@TODATE@@TOTIME') AND (STOD BETWEEN '@@FROMDATE@@FROMTIME' AND '@@TODATE@@TOTIME'))) AND (FTYP='O' OR FTYP='S' OR FTYP='X') AND STAT<>'DEL' " ;
        private string strWhereRaw = "WHERE ((STOA BETWEEN '@@FROMDATE@@FROMTIME' AND '@@TODATE@@TOTIME' AND DES3='@@HOPO') OR " +
                                     "(STOD BETWEEN '@@FROMDATE@@FROMTIME' AND '@@TODATE@@TOTIME' AND ORG3='@@HOPO')) AND " +
                                     "(FTYP IN (@@FTYP))";

        private string strWhereONBLOFBLRaw = "WHERE ((ONBL BETWEEN '@@FROMDATE@@FROMTIME' AND '@@TODATE@@TOTIME' AND DES3='@@HOPO') " +
                                                "OR  (OFBL BETWEEN '@@FROMDATE@@FROMTIME' AND '@@TODATE@@TOTIME' AND ORG3='@@HOPO')) AND " +
                                                 "(FTYP IN (@@FTYP))";

        private string strWhereLANDAIRBRaw = "WHERE ((LAND BETWEEN '@@FROMDATE@@FROMTIME' AND '@@TODATE@@TOTIME' AND DES3='@@HOPO') " +
                                                "OR  (AIRB BETWEEN '@@FROMDATE@@FROMTIME' AND '@@TODATE@@TOTIME' AND ORG3='@@HOPO')) AND " +
                                                 "(FTYP IN (@@FTYP))";

        
        private string strNatWhere			= " AND (TTYP='@@NAT')";
		private string strORGDESWhere	= " AND (ORG3='@@APT' OR DES3='@@APT')";
		
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string stractLogicalFields = "AIRLINE,DATE,ARRIVAL,DEPARTURE,COUNT_TOT,COUNT_NFES";

		/// The header columns, which must be used for the report output
		/// </summary>
		/// <summary>
		private string stractTabHeader = "Airlines,Date,Arrivals,Departures,Total,Total NFES";
		
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string stractTabHeaderLens = "100,100,100,100,100,100";

	
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the nature codes of NATTAB
		/// </summary>
		private ITable myNAT;
		/// <summary>
		/// The nature code as global valid member.
		/// </summary>
		private string strNatureCode = "";
		/// <summary>
		/// Airline 2-3 letter code
		/// </summary>
		private string strAirportCode = "";
		/// <summary>
		/// Different variables to be used for counts
		/// </summary>
		private int [] NumberofFlights;
		StringBuilder sb = new StringBuilder(10000);
		private string strEmptyLine = ",,";


        private bool TimeFrameIsWithinOneDay = false;

		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;

		private class NightOperationSummaryRowData
		{
			public string strAirline = "";
			public string strDate = "";
			public int iTotalNoOfDepartureFlights = 0;
            public int iTotalNoOfArrivalFlights = 0;
            public int iTotalNoOfNfes = 0;
        }
		//End here
		
		#endregion _My Members

		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.TextBox txtAirport;
        private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ProgressBar lblProgress;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cbNatures;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker timeFrom;
        private System.Windows.Forms.Button buttonHelp;
        private CheckBox checkBoxFtypR;
        private CheckBox checkBoxFtypD;
        private GroupBox groupBox3;
        private RadioButton radioButton1;
        private RadioButton radioButton3;
        private RadioButton radioButton2;
        private CheckBox checkBoxFtypZ;
        private GroupBox groupBox4;
        private CheckBox checkBoxFtypO;
        private TextBox txtLens;
        private Button button1;
        private DateTimePicker timeTo;
        private Label label6;		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmNightFlightSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNightFlightSummary));
            this.panelBody = new System.Windows.Forms.Panel();
            this.panelTab = new System.Windows.Forms.Panel();
            this.tabResult = new AxTABLib.AxTAB();
            this.lblResults = new System.Windows.Forms.Label();
            this.panelTop = new System.Windows.Forms.Panel();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.timeFrom = new System.Windows.Forms.DateTimePicker();
            this.txtAirport = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblProgress = new System.Windows.Forms.ProgressBar();
            this.btnLoadPrint = new System.Windows.Forms.Button();
            this.btnPrintPreview = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.cbNatures = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtLens = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBoxFtypR = new System.Windows.Forms.CheckBox();
            this.checkBoxFtypD = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.checkBoxFtypZ = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkBoxFtypO = new System.Windows.Forms.CheckBox();
            this.timeTo = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.panelBody.SuspendLayout();
            this.panelTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
            this.panelTop.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBody
            // 
            this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelBody.Controls.Add(this.panelTab);
            this.panelBody.Controls.Add(this.lblResults);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 199);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(780, 323);
            this.panelBody.TabIndex = 3;
            // 
            // panelTab
            // 
            this.panelTab.Controls.Add(this.tabResult);
            this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTab.Location = new System.Drawing.Point(0, 16);
            this.panelTab.Name = "panelTab";
            this.panelTab.Size = new System.Drawing.Size(780, 307);
            this.panelTab.TabIndex = 2;
            // 
            // tabResult
            // 
            this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabResult.Location = new System.Drawing.Point(0, 0);
            this.tabResult.Name = "tabResult";
            this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
            this.tabResult.Size = new System.Drawing.Size(780, 307);
            this.tabResult.TabIndex = 0;
            this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
            // 
            // lblResults
            // 
            this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblResults.Location = new System.Drawing.Point(0, 0);
            this.lblResults.Name = "lblResults";
            this.lblResults.Size = new System.Drawing.Size(780, 16);
            this.lblResults.TabIndex = 1;
            this.lblResults.Text = "Report Results";
            this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelTop.Controls.Add(this.buttonHelp);
            this.panelTop.Controls.Add(this.txtAirport);
            this.panelTop.Controls.Add(this.label4);
            this.panelTop.Controls.Add(this.lblProgress);
            this.panelTop.Controls.Add(this.btnLoadPrint);
            this.panelTop.Controls.Add(this.btnPrintPreview);
            this.panelTop.Controls.Add(this.btnCancel);
            this.panelTop.Controls.Add(this.btnOK);
            this.panelTop.Controls.Add(this.cbNatures);
            this.panelTop.Controls.Add(this.label3);
            this.panelTop.Controls.Add(this.dtTo);
            this.panelTop.Controls.Add(this.label2);
            this.panelTop.Controls.Add(this.dtFrom);
            this.panelTop.Controls.Add(this.label1);
            this.panelTop.Controls.Add(this.groupBox1);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(780, 199);
            this.panelTop.TabIndex = 2;
            // 
            // buttonHelp
            // 
            this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonHelp.Location = new System.Drawing.Point(404, 166);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(75, 23);
            this.buttonHelp.TabIndex = 36;
            this.buttonHelp.Text = "Help";
            this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(12, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 16);
            this.label7.TabIndex = 32;
            this.label7.Text = "Time from:";
            // 
            // timeFrom
            // 
            this.timeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeFrom.Location = new System.Drawing.Point(84, 48);
            this.timeFrom.Name = "timeFrom";
            this.timeFrom.ShowUpDown = true;
            this.timeFrom.Size = new System.Drawing.Size(100, 20);
            this.timeFrom.TabIndex = 29;
            // 
            // txtAirport
            // 
            this.txtAirport.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAirport.Location = new System.Drawing.Point(100, 123);
            this.txtAirport.Name = "txtAirport";
            this.txtAirport.Size = new System.Drawing.Size(100, 20);
            this.txtAirport.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(28, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 16);
            this.label4.TabIndex = 17;
            this.label4.Text = "Airport:";
            // 
            // lblProgress
            // 
            this.lblProgress.Location = new System.Drawing.Point(500, 166);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(216, 23);
            this.lblProgress.TabIndex = 10;
            this.lblProgress.Visible = false;
            // 
            // btnLoadPrint
            // 
            this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLoadPrint.Location = new System.Drawing.Point(252, 166);
            this.btnLoadPrint.Name = "btnLoadPrint";
            this.btnLoadPrint.Size = new System.Drawing.Size(75, 23);
            this.btnLoadPrint.TabIndex = 8;
            this.btnLoadPrint.Text = "Loa&d + Print";
            this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
            // 
            // btnPrintPreview
            // 
            this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPrintPreview.Location = new System.Drawing.Point(176, 166);
            this.btnPrintPreview.Name = "btnPrintPreview";
            this.btnPrintPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPrintPreview.TabIndex = 7;
            this.btnPrintPreview.Text = "&Print Preview";
            this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Location = new System.Drawing.Point(328, 166);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "&Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOK.Location = new System.Drawing.Point(100, 166);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "&Load Data";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // cbNatures
            // 
            this.cbNatures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNatures.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNatures.ItemHeight = 16;
            this.cbNatures.Location = new System.Drawing.Point(100, 90);
            this.cbNatures.MaxDropDownItems = 16;
            this.cbNatures.Name = "cbNatures";
            this.cbNatures.Size = new System.Drawing.Size(235, 24);
            this.cbNatures.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label3.Location = new System.Drawing.Point(28, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Nature:";
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTo.Location = new System.Drawing.Point(235, 32);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(100, 20);
            this.dtTo.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(209, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "to:";
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFrom.Location = new System.Drawing.Point(100, 32);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(100, 20);
            this.dtFrom.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(28, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date from:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.timeTo);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.timeFrom);
            this.groupBox1.Controls.Add(this.txtLens);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.checkBoxFtypR);
            this.groupBox1.Controls.Add(this.checkBoxFtypD);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.checkBoxFtypZ);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Location = new System.Drawing.Point(16, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(756, 143);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Flights";
            // 
            // txtLens
            // 
            this.txtLens.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.txtLens.Location = new System.Drawing.Point(681, 38);
            this.txtLens.Name = "txtLens";
            this.txtLens.Size = new System.Drawing.Size(106, 20);
            this.txtLens.TabIndex = 19;
            this.txtLens.Text = "textBox1";
            this.txtLens.Visible = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.button1.Location = new System.Drawing.Point(681, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBoxFtypR
            // 
            this.checkBoxFtypR.Location = new System.Drawing.Point(494, 104);
            this.checkBoxFtypR.Name = "checkBoxFtypR";
            this.checkBoxFtypR.Size = new System.Drawing.Size(87, 24);
            this.checkBoxFtypR.TabIndex = 47;
            this.checkBoxFtypR.Text = "Rerouted";
            // 
            // checkBoxFtypD
            // 
            this.checkBoxFtypD.Location = new System.Drawing.Point(494, 79);
            this.checkBoxFtypD.Name = "checkBoxFtypD";
            this.checkBoxFtypD.Size = new System.Drawing.Size(87, 24);
            this.checkBoxFtypD.TabIndex = 46;
            this.checkBoxFtypD.Text = "Diverted";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButton1);
            this.groupBox3.Controls.Add(this.radioButton3);
            this.groupBox3.Controls.Add(this.radioButton2);
            this.groupBox3.Location = new System.Drawing.Point(336, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(136, 118);
            this.groupBox3.TabIndex = 43;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Time to be considered";
            // 
            // radioButton1
            // 
            this.radioButton1.Location = new System.Drawing.Point(16, 68);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(103, 28);
            this.radioButton1.TabIndex = 40;
            this.radioButton1.Text = "&LAND / AIRB";
            // 
            // radioButton3
            // 
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(16, 20);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(103, 24);
            this.radioButton3.TabIndex = 38;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "&STA / STD";
            // 
            // radioButton2
            // 
            this.radioButton2.Location = new System.Drawing.Point(16, 44);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(103, 24);
            this.radioButton2.TabIndex = 39;
            this.radioButton2.Text = "&ONBL / OFBL";
            // 
            // checkBoxFtypZ
            // 
            this.checkBoxFtypZ.Location = new System.Drawing.Point(494, 54);
            this.checkBoxFtypZ.Name = "checkBoxFtypZ";
            this.checkBoxFtypZ.Size = new System.Drawing.Size(98, 24);
            this.checkBoxFtypZ.TabIndex = 45;
            this.checkBoxFtypZ.Text = "Return Flight";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkBoxFtypO);
            this.groupBox4.Location = new System.Drawing.Point(484, 11);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(116, 119);
            this.groupBox4.TabIndex = 42;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Flight Status";
            // 
            // checkBoxFtypO
            // 
            this.checkBoxFtypO.Checked = true;
            this.checkBoxFtypO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFtypO.Location = new System.Drawing.Point(10, 19);
            this.checkBoxFtypO.Name = "checkBoxFtypO";
            this.checkBoxFtypO.Size = new System.Drawing.Size(87, 24);
            this.checkBoxFtypO.TabIndex = 44;
            this.checkBoxFtypO.Text = "Operation";
            // 
            // timeTo
            // 
            this.timeTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeTo.Location = new System.Drawing.Point(219, 48);
            this.timeTo.Name = "timeTo";
            this.timeTo.ShowUpDown = true;
            this.timeTo.Size = new System.Drawing.Size(100, 20);
            this.timeTo.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(193, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 16);
            this.label6.TabIndex = 31;
            this.label6.Text = "to:";
            // 
            // frmNightFlightSummary
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(780, 522);
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelTop);
            this.Name = "frmNightFlightSummary";
            this.Text = "Night Flights (Summary)";
            this.Load += new System.EventHandler(this.frmNightFlightSummary_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmNightFlightSummary_Closing);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmNightFlightSummary_HelpRequested);
            this.panelBody.ResumeLayout(false);
            this.panelTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion
		
		/// <summary>
		/// Loads the from and inits the necessary things.
		/// </summary>
		/// <param name="sender">Originator of the method call</param>
		/// <param name="e">Event args</param>
		private void frmNightFlightSummary_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
			PrepareFilterData();
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			DateTime olDura;
			
			olFrom = DateTime.Now;			
			olTo = DateTime.Now;
			olFrom = olFrom.AddDays(-1);

			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olTo.Year, olTo.Month, olTo.Day, 23,59,0);
			olDura = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 3,0,0);
			
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy";
			dtTo.CustomFormat = "dd.MM.yyyy";
			
			timeFrom.CustomFormat = "HH:mm";
			timeTo.CustomFormat = "HH:mm";

			olTo   = new DateTime(olTo.Year, olTo.Month, olTo.Day, 06,00,0);
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 22,0,0);

			timeFrom.Value = olFrom;
			timeTo.Value = olTo;
		}
		/// <summary>
		/// Initializes the tab control.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString = stractTabHeader;
			tabResult.LogicalFieldList = stractLogicalFields;
			tabResult.HeaderLengthString = stractTabHeaderLens;

			tabResult.HeaderString = stractTabHeader;
			tabResult.LogicalFieldList = stractLogicalFields;
			tabResult.HeaderLengthString = stractTabHeaderLens;
			PrepareReportData();
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
			myNAT = myDB["NAT"];
			cbNatures.Items.Add("<None>");
			if(myNAT == null)
			{
				myNAT = myDB.Bind("NAT", "NAT", "TTYP,TNAM", "5,30", "TTYP,TNAM");
				myNAT.Load("ORDER BY TTYP");
			}
			else if(myNAT.Count == 0)
			{
				myNAT = myDB.Bind("NAT", "NAT", "TTYP,TNAM", "5,30", "TTYP,TNAM");
				myNAT.Load("ORDER BY TTYP");
			}
			for(int i = 0; i < myNAT.Count; i++)
			{
				string strValue = myNAT[i]["TTYP"] + " / " + myNAT[i]["TNAM"];
				cbNatures.Items.Add(strValue);
			}
			if(cbNatures.Items.Count > 0)
				cbNatures.SelectedIndex = 0;
		}

		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent();			
			LoadReportData();
			PrepareReportData();
			this.Cursor = Cursors.Arrow;
			txtLens.Text = tabResult.HeaderLengthString;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			if(cbNatures.Text == "")
			{
				strRet += ilErrorCount.ToString() +  ". Please select a nature code!\n";
				ilErrorCount++;
			}
			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
            string strTmpWhere = strWhereRaw;
            string strTmpWhereSave = strWhereRaw;

            if (this.radioButton3.Checked == true)
            {
                strTmpWhere = strWhereRaw;
            }
            else if (this.radioButton2.Checked == true)
            {
                strTmpWhere = strWhereONBLOFBLRaw;
            }
            else if (this.radioButton1.Checked == true)
            {
                strTmpWhere = strWhereLANDAIRBRaw;
            }

            strTmpWhereSave = strTmpWhere;


            string strFtyp = "";

            if (checkBoxFtypO.Checked)
            {
                if (strFtyp == "")
                    strFtyp = "'O'";
            }
            if (checkBoxFtypD.Checked)
            {
                if (strFtyp == "")
                    strFtyp = "'D'";
                else
                    strFtyp += ",'D'";
            }
            if (checkBoxFtypZ.Checked)
            {
                if (strFtyp == "")
                    strFtyp = "'Z'";
                else
                    strFtyp += ",'Z'";
            }
            if (checkBoxFtypR.Checked)
            {
                if (strFtyp == "")
                    strFtyp = "'R'";
                else
                    strFtyp += ",'R'";
            }
			tabResult.ResetContent();
			// In the first step change the times to UTC if this is necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;


            datFrom = new DateTime(datFrom.Year, datFrom.Month, datFrom.Day, timeFrom.Value.Hour, timeFrom.Value.Minute, 0);
            datTo = new DateTime(datTo.Year, datTo.Month, datTo.Day, timeFrom.Value.Hour, timeFrom.Value.Minute, 0);

            TimeSpan NightDuration = datTo - datFrom;

            int Days = NightDuration.Days;


            if (timeFrom.Value.Hour > timeTo.Value.Hour)
            {
                TimeSpan olOneDay = new TimeSpan(1, 0, 0, 0);
                DateTime datTmp2 = datFrom + olOneDay;

                DateTime datToTmp = new DateTime(datTmp2.Year, datTmp2.Month, datTmp2.Day , timeTo.Value.Hour, timeTo.Value.Minute, 0);
                NightDuration = datToTmp - datFrom;
                TimeFrameIsWithinOneDay = false;           
            }
            else
            {
                Days += 1;
                DateTime datToTmp = new DateTime(datFrom.Year, datFrom.Month, datFrom.Day , timeTo.Value.Hour, timeTo.Value.Minute, 0);
                NightDuration = datToTmp - datFrom;
                TimeFrameIsWithinOneDay = true;
            }



            if(UT.IsTimeInUtc == false)
			{
                datFrom = UT.LocalToUtc(datFrom);
                datTo = UT.LocalToUtc(datTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
                strDateFrom = UT.DateTimeToCeda(datFrom);
                strDateTo = UT.DateTimeToCeda(datTo);
			}
			// Extract the true Nature code from the selected entry of the combobox
			strNatureCode = "";
			string [] strArr = cbNatures.Text.Split('/');
			strNatureCode = strArr[0].Trim();
			strAirportCode = txtAirport.Text;

			//Now reading day by day the flight records in order to 
			//not stress the database too much
			myDB.Unbind("AFT");
			// 10 , 10 , 9  ,  8 , 1  , 1  , 14 , 1  ,  5 , 1  , 2  , 3  , 4  , 3  , 4  , 2  , 5  , 5  , 5  , 5  , 5  , 1  , 3  , 14 , 1  , 3  , 4  , 5  ,  5 , 5  , 5  , 5
			//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT
			myAFT = myDB.Bind("AFT", "AFT", 
				"ALC2,ALC3,RKEY,URNO,ADID,FLNO,CSGN,FLTI,JCNT,STOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT,RTYP,ONBL,OFBL,NFES", 
				"2,3,10,10,1,9,8,1,1,14,14,1,5,1,2,3,4,3,4,2,5,5,5,5,5,1,3,14,14,1,3,4,5,5,5,5,5,1,14,14,14",
				"ALC2,ALC3,RKEY,URNO,ADID,FLNO,CSGN,FLTI,JCNT,STOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT,RTYP,ONBL,OFBL,NFES");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL,NFES";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			lblProgress.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			lblProgress.Show();
			
			for(int loopCnt = 0; loopCnt < Days; loopCnt++)
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				lblProgress.Value = percent;
                datReadTo = datReadFrom + NightDuration;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				
				//patch the where statement according to the user's entry

                strTmpWhere = strTmpWhereSave;
  
                if(strNatureCode != "<None>")
					strTmpWhere += strNatWhere;
				if(strAirportCode != "" )
					strTmpWhere += strORGDESWhere;
				strTmpWhere = strTmpWhere.Replace("@@FROMDATE@@FROMTIME", strDateFrom);
                strTmpWhere = strTmpWhere.Replace("@@FTYP", strFtyp);
                strTmpWhere = strTmpWhere.Replace("@@TODATE@@TOTIME", strDateTo);
				strTmpWhere = strTmpWhere.Replace("@@NAT", strNatureCode );
				strTmpWhere = strTmpWhere.Replace("@@APT", strAirportCode);
                strTmpWhere = strTmpWhere.Replace("@@HOPO", UT.Hopo);
                strTmpWhere = strTmpWhere.Replace("@@ORIG", UT.Hopo);
                strTmpWhere = strTmpWhere.Replace("@@DEST", UT.Hopo);

				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);				
				myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;				
				//New function added
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
			}


			lblProgress.Text = "";
			lblProgress.Hide();

		}

		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// First organize the rotations and then put the result
		/// into the tabResult
		/// </summary>
		private void PrepareReportData()
		{
			if(myAFT == null)
				return;

			myAFT.Sort("RKEY", true);
			int llCurr = 0;
			int ilStep = 1;
			string strCurrAdid = "";
            DateTime DateCurrSto;
            string strCurrSto;
            DateTime TmpDate;
            string strTime = "";
            string strDate = "";
            DateTime tmpDate;
			StringBuilder sb = new StringBuilder(10000);
			System.Collections.Hashtable hashOvernightFlightData = new Hashtable();
			            
            while(llCurr < myAFT.Count)
			{
				string strValues = "";
				strCurrAdid = myAFT[llCurr]["ADID"];


                if (strCurrAdid == "A")
                {
                    DateCurrSto = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOA"]);
                    strCurrSto = myAFT[llCurr]["STOA"];
                }
                else
                {
                    DateCurrSto = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOD"]);
                    strCurrSto = myAFT[llCurr]["STOD"];
                }

                string strKey = myAFT[llCurr]["ALC2"] + "/" + myAFT[llCurr]["ALC3"];

                if (!TimeFrameIsWithinOneDay)
                {
                    if (DateCurrSto.Hour > 12)
                    {
                        strDate = Helper.DateString(strCurrSto, "yyyyMMdd");
                        strDate += "/";
                        TmpDate = DateCurrSto.AddDays(1);
                        strDate += TmpDate.ToString("dd");
                        strKey += strDate;
                    }
                    else
                    {
                        TmpDate = DateCurrSto.AddDays(-1);
                        strDate = TmpDate.ToString("yyyyMMdd");
                        strDate += "/";
                        strDate += Helper.DateString(strCurrSto, "dd");
                        strKey += strDate;
                    }
                }
                else
                {
                    strDate = Helper.DateString(strCurrSto, "yyyyMMdd");
                    strKey += strDate;
                }

                NightOperationSummaryRowData NightOperationSummaryRowData = (NightOperationSummaryRowData)hashOvernightFlightData[strKey];
                if (NightOperationSummaryRowData == null)
                {
                    NightOperationSummaryRowData = new NightOperationSummaryRowData();
                    NightOperationSummaryRowData.strAirline = myAFT[llCurr]["ALC2"] + " / " + myAFT[llCurr]["ALC3"];
                    NightOperationSummaryRowData.strDate = strDate;
                    hashOvernightFlightData.Add(strKey, NightOperationSummaryRowData);
                }

                if (strCurrAdid == "A")
                {
                    NightOperationSummaryRowData.iTotalNoOfArrivalFlights++;
                }
                else
                {
                    NightOperationSummaryRowData.iTotalNoOfDepartureFlights++;
                }
                if (myAFT[llCurr]["NFES"] != "")
                {
                    NightOperationSummaryRowData.iTotalNoOfNfes++;
                }

            
				llCurr = llCurr + ilStep;
			}//while(llCurr < myAFT.Count)

			IDictionaryEnumerator iDictEnumerator = hashOvernightFlightData.GetEnumerator();
            int ilTotal;

			while(iDictEnumerator.MoveNext() == true)
			{
				string strValues = "";
				strValues += ((NightOperationSummaryRowData)iDictEnumerator.Value).strAirline + ",";
				strValues += ((NightOperationSummaryRowData)iDictEnumerator.Value).strDate + ",";
                strValues += ((NightOperationSummaryRowData)iDictEnumerator.Value).iTotalNoOfArrivalFlights.ToString() + ",";
                strValues += ((NightOperationSummaryRowData)iDictEnumerator.Value).iTotalNoOfDepartureFlights.ToString() + ",";
                ilTotal = ((NightOperationSummaryRowData)iDictEnumerator.Value).iTotalNoOfDepartureFlights + ((NightOperationSummaryRowData)iDictEnumerator.Value).iTotalNoOfArrivalFlights;
                strValues += ilTotal.ToString() + ",";
                strValues += ((NightOperationSummaryRowData)iDictEnumerator.Value).iTotalNoOfNfes.ToString();
                strValues += "\n";
                sb.Append(strValues);
			}

			tabResult.InsertBuffer(sb.ToString(), "\n");
			lblProgress.Text = "";
			lblProgress.Visible = false;
			tabResult.Sort("0", true, true);			
			tabResult.Refresh();			
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
	
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
            int ilTotalNoOfFlights = myAFT.Count;
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + ilTotalNoOfFlights.ToString() + " )";

            strReportHeader = frmMain.strPrefixVal + "Night Operation (Summary): NAT: " + strNatureCode +  " "+" APT: " + strAirportCode + " "+ " Timeframe: " + timeFrom.Value.ToString("HH:mm") + " to " + timeTo.Value.ToString("HH:mm");
			strReportSubHeader = strSubHeader;
			rptFIPS rpt = new rptFIPS(tabResult,strReportHeader, strSubHeader, "", 10);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			if(Helper.UseSpreadBuilder() == true)
			{
				activeReport = rpt;
				frm.UseSpreadBuilder(true);
				frm.GetBtnExcelExport().Click += new EventHandler(frmNightFlightSummary_Click);
			}
			else
			{
				frm.UseSpreadBuilder(false);
			}
			frm.Show();
		}

		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();		
		}

		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent();			
			LoadReportData();
			PrepareReportData();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}

		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmNightFlightSummary_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("NAT");
			myDB.Unbind("AFT");
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			txtLens.Text = tabResult.HeaderLengthString;
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			myDB.Unbind("NAT");
			myDB.Unbind("AFT");
			this.Close ();
		}

		private void checkBoxSingleArrivals_CheckedChanged(object sender, System.EventArgs e)
		{
			if(tabResult.GetLineCount() > 0)
			{
				InitTab();
				LoadReportData();
				PrepareReportData();
			}
			else
			{
				InitTab();
			}
		}

		private void InitializeMouseEvents()
		{
			((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmNightFlightSummary_MouseWheel);
			((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmNightFlightSummary_MouseEnter);
			((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmNightFlightSummary_MouseLeave);
		}

		private void frmNightFlightSummary_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && tabResult.GetVScrollPos() > 0)
			{				
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
			{
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
			}
		}

		private void frmNightFlightSummary_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmNightFlightSummary_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmNightFlightSummary_Click(object sender, EventArgs e)
		{
			Helper.ExportToExcel(tabResult,strReportHeader,strReportSubHeader,stractTabHeaderLens,10,0,activeReport);
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmNightFlightSummary_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}
	}
}