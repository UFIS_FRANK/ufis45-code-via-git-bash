using System;
using Ufis.Utils;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for Helper.
	/// </summary>
	public class Helper
	{
		public Helper()
		{
			//
			// TODO: Add constructor logic here
			//
		}
        
		/// <summary>
		/// Converts a ceda date in a readable string according to the format.
		/// </summary>
		/// <param name="pDateString">The ceda string</param>
		/// <param name="strFormat">The format string</param>
		/// <returns>A readable string or ""</returns>
		public static string DateString(string CedaString, string strFormat)
		{
			string strRet = "";
			if (CedaString == null)
				return strRet;

			strRet = CedaString;

			// add seconds in case of using ETAI or ETDI
			if (strRet.Length == 12)
				strRet += "00";

			if (strRet != null && strRet != "")
			{
				DateTime tmpDate;
				tmpDate = UT.CedaFullDateToDateTime(strRet);
				strRet = tmpDate.ToString(strFormat);
			}
			else
			{
				strRet = "";
			}
			return strRet;
		}

		/// <summary>
		/// Converts a ceda date in a readable string according to the format.
		/// </summary>
		/// <param name="pDateString">The ceda string</param>
		/// <param name="strFormat">The format string</param>
		/// <returns>A readable string or ""</returns>
		public static string DateString(string CedaScheduledString, string CedaActualString,string strFormat)
		{
			string strScheduled = "";
			strScheduled = CedaScheduledString;

			// add seconds in case of using ETAI or ETDI
			if (strScheduled.Length == 12)
				strScheduled += "00";

			string strActual = "";
			strActual = CedaActualString;

			// add seconds in case of using ETAI or ETDI
			if (strActual.Length == 12)
				strActual += "00";

			string strRet = "";

			if (strActual != "")
			{
				DateTime tmpActual;
				tmpActual = UT.CedaFullDateToDateTime(strActual);
				if (strScheduled != "")
				{
					DateTime tmpScheduled = UT.CedaFullDateToDateTime(strScheduled);
					if (tmpScheduled.Date != tmpActual.Date)
					{
						TimeSpan diff = tmpActual.Date - tmpScheduled.Date;
						int days = (int)diff.Days;
						string strExtFormat = strFormat;
						if (days > 0)
							strExtFormat += "+" + days.ToString();
						else
							strExtFormat += "-" + days.ToString();
						strRet = tmpActual.ToString(strExtFormat);
					}
					else
					{
						strRet = tmpActual.ToString(strFormat);
					}
				}
				else
				{
					strRet = tmpActual.ToString(strFormat);
				}
			}
			return strRet;
		}

		/// <summary>
		/// Checks the printer support for paper size of report
		/// </summary>
		/// <param name="ropPaperKind">The Paperkind enum</param>
		/// <param name="ropPrinter">The printer object</param>
		/// <param name="bpShowErrorMessage">The boolean to decide whether to show error message or not</param>
		/// <returns>A boolean value</returns>
		public static bool IsPageSupportedByPrinter(System.Drawing.Printing.PaperKind ropPaperKind,
			                                        DataDynamics.ActiveReports.Document.Printer ropPrinter,
			                                        bool bpShowErrorMessage)
		{			
			bool blValidatePageSupport = true;			
			string strPaperSizeNames = "";
			System.Drawing.Printing.PaperSize paperSize;
			for(int i = 0; i < ropPrinter.PaperSizes.Count; i++)
			{
				paperSize = ropPrinter.PaperSizes[i];
				if(strPaperSizeNames.IndexOf(paperSize.PaperName.ToString()) == -1)
				{
					strPaperSizeNames += paperSize.PaperName.ToString() + ",";
				}
			}				
	       		   
			if(strPaperSizeNames.IndexOf(ropPaperKind.ToString()) == -1)
			{				
				blValidatePageSupport = false;				
				if(bpShowErrorMessage == true)
				{					
					System.Windows.Forms.MessageBox.Show("Select printer does not support the paper size " +  ropPaperKind.ToString() + ". \nPlease choose another printer.","Print Error",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error,System.Windows.Forms.MessageBoxDefaultButton.Button1,System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly);
				}
			}
			return blValidatePageSupport;
		}

		/// <summary>
		/// Checks the scrolling eligibility of AxTABLib.AxTAB control
		/// </summary>
		/// <param name="tabResult">The table control</param>		
		/// <returns>A boolean value</returns>
		public static bool CheckScrollingStatus(AxTABLib.AxTAB tabResult)
		{
			bool blCheckScrollingStatus = false;
			int ilOffset = 0;
			
			if(tabResult.HScrollMaster == true)
			{
				ilOffset = 16;
			}

			int ilHeaderLines = 0;

			if(tabResult.MainHeaderOnly == false && tabResult.MainHeader == true)
			{
				ilHeaderLines = 2;
			}
			else if(tabResult.MainHeaderOnly == true && tabResult.MainHeader == true)
			{
				ilHeaderLines = 1;
			}
			else if(tabResult.MainHeaderOnly == true && tabResult.MainHeader == false)
			{
				ilHeaderLines = 0;
			}
			else
			{
				ilHeaderLines = 1;
			}
			
			ilOffset += (ilHeaderLines * tabResult.LineHeight);
			int ilRemainder = (int)(tabResult.Size.Height-ilOffset) % tabResult.LineHeight;
			int ilMaxVisibleLines = (int)((tabResult.Size.Height - ilOffset) / tabResult.LineHeight);
			
			if(ilRemainder != 0)
			{
				ilMaxVisibleLines -= 1;
			}
			if(tabResult.GetLineCount() <= ilMaxVisibleLines)
			{
				blCheckScrollingStatus = false;
			}
			else
			{
				blCheckScrollingStatus = true;
			}
			return blCheckScrollingStatus;
		}

		/// <summary>
		/// Exports to excel using SpreadBuilder APIs
		/// </summary>
		/// <param name="tabResult">The table control</param>
		/// <param name="strHeader">Report Header</param>
		/// <param name="strSubHeader">Report Subheader</param>
		/// <param name="strExcelHeaderLength">Excel Header length </param>
		/// <param name="iFontSize">Report Font Size</param>
		/// <param name="iHeaderStartIndex">Header start index with respect to tab control</param>
		/// <param name="activeReport">Active Report Object</param>
		/// <returns>A boolean value</returns>
		public static void ExportToExcel(AxTABLib.AxTAB tabResult, string strHeader,
			                             string strSubHeader, string strExcelHeaderLength,
			                             int iFontSize, int iHeaderStartIndex,
			                             DataDynamics.ActiveReports.ActiveReport3 activeReport)
		{
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.Filter = "Excel-files (*.xls)|*.xls|All filed (*.*)|*.*";
			if(dlg.ShowDialog() == DialogResult.OK)
			{
				string[] arrExcelHeaderLength = strExcelHeaderLength.Split(',');
				DataDynamics.SpreadBuilder.Workbook sb = new DataDynamics.SpreadBuilder.Workbook();
				sb.Sheets.AddNew();				
				sb.Sheets[0].PageSetup.PaperSize = activeReport.PageSettings.PaperKind;

				string[] arrayHeaderNames = tabResult.HeaderString.Split(',');
				sb.Sheets[0].Cell(3,0).FontBold = true;			
				sb.Sheets[0].Cell(3,0).SetValue(strSubHeader);

				//The below "for loop" is to intialize the widht of few columns, so that our logic for image width works fine
				for(short k = 0; k < 100; k++)
				{
					sb.Sheets[0].Columns(k).Width = 1000;
				}

				int ilHeaderLength = 0;
                short invalidColumnLengthCount = 0; //igu on 24/02/2011
				for(short j = 0; j < tabResult.GetColumnCount()-iHeaderStartIndex; j++)
				{			
					if((ilHeaderLength = Convert.ToInt32(arrExcelHeaderLength[j+iHeaderStartIndex])) <= 0)
                    {
                        invalidColumnLengthCount++;
                        continue;
					}
                    sb.Sheets[0].Columns((short)(j - invalidColumnLengthCount)).Width = Math.Max(arrayHeaderNames[j + iHeaderStartIndex].Length * 135, ilHeaderLength * 16);


                    sb.Sheets[0].Cell(5, j - invalidColumnLengthCount).FillColor = System.Drawing.Color.LightGray;
                    sb.Sheets[0].Cell(5, j - invalidColumnLengthCount).SetValue(arrayHeaderNames[j + iHeaderStartIndex]);
                    sb.Sheets[0].Cell(5, j - invalidColumnLengthCount).FontBold = true;
                    sb.Sheets[0].Cell(5, j - invalidColumnLengthCount).Alignment = DataDynamics.SpreadBuilder.Style.HorzAlignments.Left;
                    sb.Sheets[0].Cell(5, j - invalidColumnLengthCount).BorderTopStyle = DataDynamics.SpreadBuilder.Style.BorderLineStyle.Double;//.Thick;
                    if (j - invalidColumnLengthCount == 0)
					{
                        sb.Sheets[0].Cell(5, j - invalidColumnLengthCount).BorderLeftStyle = DataDynamics.SpreadBuilder.Style.BorderLineStyle.Double;
					}
                    sb.Sheets[0].Cell(5, j - invalidColumnLengthCount).BorderRightStyle = DataDynamics.SpreadBuilder.Style.BorderLineStyle.Double;
                    sb.Sheets[0].Cell(5, j - invalidColumnLengthCount).BorderBottomStyle = DataDynamics.SpreadBuilder.Style.BorderLineStyle.Double;
					for(int i = 0; i < tabResult.GetLineCount(); i++)
					{
                        sb.Sheets[0].Cell(i + 6, j - invalidColumnLengthCount).FontSize = iFontSize;
                        sb.Sheets[0].Cell(i + 6, j - invalidColumnLengthCount).BorderLeftStyle = DataDynamics.SpreadBuilder.Style.BorderLineStyle.Double;
                        sb.Sheets[0].Cell(i + 6, j - invalidColumnLengthCount).BorderRightStyle = DataDynamics.SpreadBuilder.Style.BorderLineStyle.Double;
                        if (tabResult.GetColumnValue(i, j + iHeaderStartIndex).Length > 0)
						{
                            sb.Sheets[0].Cell(i + 6, j - invalidColumnLengthCount).Alignment = DataDynamics.SpreadBuilder.Style.HorzAlignments.Left;
                            sb.Sheets[0].Cell(i + 6, j - invalidColumnLengthCount).VertAlignment = DataDynamics.SpreadBuilder.Style.VertAlignments.Top;
                            sb.Sheets[0].Cell(i + 6, j - invalidColumnLengthCount).WrapText = true;

                            string olTest = tabResult.GetColumnValue(i, j + iHeaderStartIndex);
                            if (olTest.Length == 5 && olTest.Contains("."))
                            {
                                try
                                {
                                    sb.Sheets[0].Cell(i + 6, j - invalidColumnLengthCount).NumberFormat = "00.00";
                                    sb.Sheets[0].Cell(i + 6, j - invalidColumnLengthCount).SetValue(Convert.ToDouble(tabResult.GetColumnValue(i, j + iHeaderStartIndex)));
                                }
                                catch
                                {
                                    sb.Sheets[0].Cell(i + 6, j - invalidColumnLengthCount).SetValue(tabResult.GetColumnValue(i, j + iHeaderStartIndex));
                                }
                            }
                            else
                            {
                                try
                                {
                                    sb.Sheets[0].Cell(i + 6, j - invalidColumnLengthCount).SetValue(Convert.ToInt32(tabResult.GetColumnValue(i, j + iHeaderStartIndex)));
                                }
                                catch
                                {
                                    sb.Sheets[0].Cell(i + 6, j - invalidColumnLengthCount).SetValue(tabResult.GetColumnValue(i, j + iHeaderStartIndex));
                                }
                            }
						}
					}
                    sb.Sheets[0].Cell(tabResult.GetLineCount() + 5, j - invalidColumnLengthCount).BorderBottomStyle = DataDynamics.SpreadBuilder.Style.BorderLineStyle.Double;			
				}

				IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
				string strPrintLogoPath = myIni.IniReadValue("GLOBAL", "PRINTLOGO");
				DataDynamics.ActiveReports.Picture pictureHeader = null;
				Image imageHeader = null;
				float fHeaderWidth = 0;
				try
				{
					if(activeReport != null)
					{
						pictureHeader = (DataDynamics.ActiveReports.Picture)activeReport.Sections["ReportHeader"].Controls["Picture1"];
						if(pictureHeader == null)
						{
							pictureHeader = (DataDynamics.ActiveReports.Picture)activeReport.Sections["PageHeader"].Controls["Picture2"];
						}
						if(pictureHeader != null)
						{
							imageHeader = pictureHeader.Image;
						}
						
						fHeaderWidth  = activeReport.Sections["ReportHeader"].Controls["lblReportHeader1"].Width;
						if(fHeaderWidth  == 0)
						{
							fHeaderWidth  = activeReport.Sections["PageHeader"].Controls["lblPageHeader1"].Width;
						}
						fHeaderWidth *= 1500;
					}
				}
				catch(Exception)
				{
				}
				if(fHeaderWidth == 0)
				{
					fHeaderWidth = strHeader.Length*135;
				}
				float fTotalWidth = 0;
				short iStartColumn = 0;
				while(true)
				{					
					fTotalWidth += sb.Sheets[0].Columns(iStartColumn++).Width;
					if(fTotalWidth > fHeaderWidth)
					{
						break;
					}
				}
				
				if(iStartColumn > 0)
				{
					sb.Sheets[0].Cell(1,0).FontSize = 12;
					sb.Sheets[0].Cell(1,0).FontBold = true;			
					sb.Sheets[0].Cell(1,0).Merge(0,(short)(iStartColumn - 1));
					sb.Sheets[0].Cell(1,0).FillColor = System.Drawing.Color.LightGray;
					sb.Sheets[0].Cell(1,0).SetValue(strHeader);
				}

				Image myImage = imageHeader;
				try
				{
					if(strPrintLogoPath != "")
					{
						myImage = new Bitmap(strPrintLogoPath);
					}
					if(myImage != null)
					{						
						short iEndColumn = iStartColumn;						
						int iImageWidth = (int)(pictureHeader.Width*1350);//myImage.Width*16;
						int totalWidth = 0;
						float fDxAddImage = 0;
						while(true)
						{
							totalWidth += sb.Sheets[0].Columns(iEndColumn).Width;
							if(totalWidth >= iImageWidth)
							{	
								float fExtraWidth = totalWidth - iImageWidth;
								fDxAddImage = 1024 - (fExtraWidth / (float)sb.Sheets[0].Columns(iEndColumn).Width) * 1024;
								break;
							}
							else
							{
								iEndColumn++;
							}
						}						
						DataDynamics.SpreadBuilder.Imaging.ImageInfo imageInfo = new DataDynamics.SpreadBuilder.Imaging.ImageInfo();
						imageInfo.AutoFill = true;						
						sb.Sheets[0].AddImage(myImage,imageInfo,System.Drawing.Color.Black,System.Drawing.Color.White,iStartColumn,0,0,0,iEndColumn,(short)fDxAddImage,4,1,"");
					}
				}
				catch(Exception)
				{
				}
				
				sb.Sheets[0].Cell(tabResult.GetLineCount()+6,0).FontSize = iFontSize;
				sb.Sheets[0].Cell(tabResult.GetLineCount() + 6,1).FontSize = iFontSize;				
				sb.Sheets[0].Cell(tabResult.GetLineCount()+7,0).FontSize = iFontSize;

				sb.Sheets[0].Cell(tabResult.GetLineCount() + 6,0).SetValue("Printed On: ");			
				sb.Sheets[0].Cell(tabResult.GetLineCount() + 6,1).SetValue(System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToLongTimeString());
				int ilNoOfPages = (tabResult.GetLineCount()/52) + 1;

				int ilNoOfColumns = tabResult.GetColumnCount() - iHeaderStartIndex;
				sb.Sheets[0].Cell(tabResult.GetLineCount()+6,ilNoOfColumns - 1).FontSize = iFontSize;
				sb.Sheets[0].Cell(tabResult.GetLineCount() + 6,ilNoOfColumns -1).SetValue("Page " + ilNoOfPages.ToString() + " / " + ilNoOfPages.ToString());			
				sb.Save(dlg.FileName);
			}
		}

		/// <summary>
		/// Shows help file
		/// </summary>
		/// <param name="parent">The parent control</param>
		/// <returns>void</returns>
		public static void ShowHelpFile(System.Windows.Forms.Control parent)
		{		
			try
			{
				IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
				String strHelpFileName = myIni.IniReadValue("GLOBAL", "HelpDirectory") + "\\" + myIni.IniReadValue("FIPSREPORT", "HelpFile");
				if(strHelpFileName.Length > 0)
				{
					System.Windows.Forms.Help.ShowHelp(parent,strHelpFileName);
				}
			}
			catch(Exception e)
			{				
				MessageBox.Show(parent,e.Message, AppDomain.CurrentDomain.FriendlyName, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);				
			}
		}

		/// <summary>
		/// RemoveOrReplaceString-Edits the string
		/// </summary>
		/// <param name="position">The position of the string to be searched</param>
		/// <param name="strString">The original string to be modified</param>
		/// <param name="chSeparator">The separator in the original string </param>
		/// <param name="strNewReplaceValue">The new string value to be placed</param>
		/// <param name="bpRemove">Boolean to decide if to replace or remove the string specified by position</param>
		/// <returns>void</returns>
		public static void RemoveOrReplaceString(int position, ref string strString, char chSeparator,
			                                      string strNewReplaceValue, bool bpRemove)
		{
			string[] strStrings = strString.Split(chSeparator);
			for(int i = 0; i < strStrings.Length; i++)
			{
				if(i == position)
				{
					strStrings[i] = strNewReplaceValue;
				}
			}
			strString = "";
			for(int i = 0; i < strStrings.Length; i++)
			{
				if(bpRemove == true)
				{
					if(i == position)
					{
						continue;
					}
					else
					{
						if((i == strStrings.Length - 1) || ((position == strStrings.Length - 1) && (i == (position - 1))))
						{
							strString += strStrings[i];
						}
						else
						{
							strString += strStrings[i] + chSeparator;
						}
					}
				}
				else
				{
					if(i == strStrings.Length - 1)
					{
						strString += strStrings[i];
					}
					else
					{
						strString += strStrings[i] + chSeparator;
					}					
				}
			}
		}
		
		/// <summary>
		/// InsertString-Edits the string
		/// </summary>
		/// <param name="position">The position of the string to be inserted</param>
		/// <param name="strString">The original string to be modified</param>
		/// <param name="chSeparator">The separator in the original string </param>
		/// <param name="strNewInsertValue">The new string value to be inserted</param>		
		/// <returns>void</returns>
		public static void InsertString(int position, ref string strString,
			                            char chSeparator, string strInsertValue)
		{
			string[] strStrings = strString.Split(chSeparator);
			if(position == strStrings.Length)
			{
				strString += chSeparator + strInsertValue;
			}
			else
			{
				strString = "";
				for(int i = 0; i < strStrings.Length; i++)
				{	
					if(i == position)
					{
						strString += strInsertValue + chSeparator;	
						i--;
						position = -9;
					}				
					else
					{
						strString += (i < (strStrings.Length -1)) ? strStrings[i] + chSeparator : strStrings[i];
					}
				}
			}
		}

		/// <summary>
		/// IsSTEVConfigured-Checks for SHOW_STEV and STEV_LABEL configuration in ceda.ini file
		/// </summary>
		/// <param name="parentControl">The caller of this function</param>
		/// <param name="strStrings[]">The string array to hold the values of label and header</param>
		/// <param name="bpFillStringArray">The boolean to decide if the array passed should be filled </param>
		/// <param name="bpShowErrorMessage">The boolean to decide if to show error message or not</param>		
		/// <returns>bool</returns>
		public static bool IsSTEVConfigured(System.Windows.Forms.IWin32Window parentControl,
			                                ref string[] strLabelHeaderEntry, bool bpFillStringArray, bool bpShowErrorMessage)
		{
			bool blIsSTEVConfigured = false;
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			if(myIni.IniReadValue("FIPSREPORT","SHOW_STEV").CompareTo("TRUE") == 0)
			{	
				blIsSTEVConfigured = true;
				if(bpFillStringArray == true)
				{
					string strSTEV_LABEL = myIni.IniReadValue("FIPSREPORT","STEV_LABEL");
					strLabelHeaderEntry = strSTEV_LABEL.Split(';');
					if(strSTEV_LABEL.Length == 0 || strLabelHeaderEntry.Length != 2)
					{
						if(bpShowErrorMessage == true)
						{
							MessageBox.Show(parentControl,"'STEV_LABEL' entry in ceda.ini file missing or corrupted","Ceda.ini entry",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Information);
						}
						blIsSTEVConfigured = false;
					}
					else
					{
						blIsSTEVConfigured = true;
					}
				}
			}
			return blIsSTEVConfigured;
		}

		/// <summary>
		/// UseSpreadBuilder-Checks if USESPREADBUILDER configured TRUE or not configured in ceda.ini file
		/// If set to TRUE or not configured the excel export is done by Active Reports's spread builder APIs.
		/// </summary>
		/// <returns>bool</returns>
		public static bool UseSpreadBuilder()
		{				
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strUseSpreadBuilder = myIni.IniReadValue("FIPSREPORT","USESPREADBUILDER");			
			return strUseSpreadBuilder.CompareTo("FALSE") != 0 ? true : false;
		}
          /// <summary>
          /// Get a list of specific type of control list from the form
          /// </summary>
          /// <param name="CtrlTypeName">System.String containing control name (e.g; Button or TextBox, etc)</param>
          /// <param name="FormName">System.Windows.Form.Control containing control name (e.g; This)</param>
          /// <returns>Return System.Collection.Generic list of controls.</returns>
          public static List<Control> GetButtonCtrlsAndAdjust(string CtrlTypeName, Control FormName)
          {
               List<Control> controles = new List<Control>();              
               
               foreach (Control c in FormName.Controls)
               {
                    if (c.GetType().Name == CtrlTypeName)
                    {
                         controles.Add(c);
                    }
                    else if (c.Controls.Count > 0)
                    {
                         controles.AddRange(GetButtonCtrlsAndAdjust(CtrlTypeName, c));
                    }
               }
               
               return controles;
          }
	}
}

