using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Data;
using Ufis.Utils;
using DataDynamics.ActiveReports;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmFlightStatisticsDomesticInternationalCombined : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statement: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		//private string strWhere = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO') AND (STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO'))";// AND (ALC2='@@ALC' OR ALC3='@@ALC')" ;
//		private string strWhere = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO'))";// AND (ALC2='@@ALC' OR ALC3='@@ALC')" ;

		//Where condition with a Airline
		private string strWhereAftAirline= "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO')) AND FTYP IN ('O','S')) AND (ALC2 IN ( '@@AIRLINE') OR ALC3 IN ('@@AIRLINE'))"; 
		
		//Where condition without a Airline
		private string strWhereAftWithoutAirline= "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO')) AND FTYP IN ('O','S'))"; 
		
		//Where condition with a Handling agent
		private string strWhereHaiAgent = "WHERE  (((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO'))AND FTYP IN ('O','S')) AND (URNO IN (SELECT FLNU FROM HAITAB WHERE HSNA IN ('@@HSNA')) OR ALC2 IN (SELECT ALC2  FROM ALTTAB WHERE URNO IN ( SELECT ALTU FROM HAITAB WHERE  HSNA IN ('@@HSNA')) AND ALC2 <>' ') "
			                + " OR ALC3 IN (SELECT ALC3  FROM ALTTAB WHERE URNO IN ( SELECT ALTU FROM HAITAB WHERE  HSNA IN ('@@HSNA')) AND ALC3 <>' ')) ";

		//Where condition without a Handling agent
		private string strWhereWithoutHaiAgent = "WHERE  (((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO'))AND FTYP IN ('O','S')) AND (URNO IN (SELECT FLNU FROM HAITAB) OR ALC2 IN (SELECT ALC2  FROM ALTTAB WHERE URNO IN ( SELECT ALTU FROM HAITAB ) AND ALC2 <>' ') "
			+ " OR ALC3 IN (SELECT ALC3  FROM ALTTAB WHERE URNO IN ( SELECT ALTU FROM HAITAB) AND ALC3 <>' ')) ";

		//Where condition for STEV field
		private string strTerminalWhere     = " AND (STEV = '@@STEV')";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		//private string strLogicalFields = "Date,DomArrival,DomDeparture,DomTotal,IntArrival,IntDeparture,IntTotal,RegArrival,RegDeparture,RegTotal,MixArrival,MixDeparture,MixTotal,Arrival,Departure,Total";
		private string strLogicalFields = "Date,Arrival,Departure,Total";
		
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		//private string strTabHeader = "Date,Arr.,Dep.,Tot. Dom.,Arr.,Dep.,Tot. Internl. ,Arr.,Dep.,Tot. Regnl.,Arr.,Dep.,Tot. Combnd.,Tot. Arr.,Tot. Dep.,Total Flights";
		private string strTabHeader = "Date,Tot. Arr.,Tot. Dep.,Total Flights";
		/// <summary>
		/// The default flight type and desc configuration incase Ceda.ini entry is missing
		/// </summary>		
		private string strDefFltiAndDesc = "S,Schengen,I,Int,D,Dom,M,Mixed";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		//private string strTabHeaderLens = "70,45,45,90,45,45,50,45,45,76,45,45,85,55,57,85";
		private string strTabHeaderLens = "65,55,60,90";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITAble object for the HAITAB
		/// </summary>
		private ITable myHAI;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Variables to capture number of flights
		/// </summary>
		private double [] NumberofFlights;
		///<summary>
		///Variable to capture number of flights in vertical
		///</summary>
		private double [] NumberofFlightsinVertical;
		
		/// <summary>
		/// String builder
		/// </summary>
		private StringBuilder sb = new StringBuilder(10000);
		/// <summary>
		/// variable to modify the Airline entry
		/// </summary>
		private string strTxtAirline;
		/// <summary>
		/// variable to modify the Handling Agent entry
		/// </summary>
		private string strTxtAgent;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;
		/// <summary>
		/// Boolean for showing stev
		/// </summary>
		private bool bmShowStev = false;
		/// <summary>
		/// String array for storing flight types
		/// </summary>		
		private string[] strFlti;
		/// <summary>
		/// String array for storing flight types description
		/// </summary>		
		private string[] strFltiDesc;

		#endregion _My Members

		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.TextBox txtAirline;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RadioButton rdbAirlines;
		private System.Windows.Forms.RadioButton rdbHandlingAgents;
		private System.Windows.Forms.TextBox txtHandlingAgents;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button buttonHelp;
		private System.Windows.Forms.Label lblTerminal;
		private System.Windows.Forms.TextBox txtTerminal;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmFlightStatisticsDomesticInternationalCombined()
		{
			//
			// Required for Windows Form Designer support
			//
					
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFlightStatisticsDomesticInternationalCombined));
            this.panelBody = new System.Windows.Forms.Panel();
            this.panelTab = new System.Windows.Forms.Panel();
            this.tabResult = new AxTABLib.AxTAB();
            this.lblResults = new System.Windows.Forms.Label();
            this.panelTop = new System.Windows.Forms.Panel();
            this.txtTerminal = new System.Windows.Forms.TextBox();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtHandlingAgents = new System.Windows.Forms.TextBox();
            this.rdbHandlingAgents = new System.Windows.Forms.RadioButton();
            this.rdbAirlines = new System.Windows.Forms.RadioButton();
            this.txtAirline = new System.Windows.Forms.TextBox();
            this.lblProgress = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnLoadPrint = new System.Windows.Forms.Button();
            this.btnPrintPreview = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panelBody.SuspendLayout();
            this.panelTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
            this.panelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBody
            // 
            this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelBody.Controls.Add(this.panelTab);
            this.panelBody.Controls.Add(this.lblResults);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 160);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(1192, 493);
            this.panelBody.TabIndex = 5;
            this.panelBody.Paint += new System.Windows.Forms.PaintEventHandler(this.panelBody_Paint);
            // 
            // panelTab
            // 
            this.panelTab.Controls.Add(this.tabResult);
            this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTab.Location = new System.Drawing.Point(0, 16);
            this.panelTab.Name = "panelTab";
            this.panelTab.Size = new System.Drawing.Size(1192, 477);
            this.panelTab.TabIndex = 2;
            this.panelTab.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTab_Paint);
            // 
            // tabResult
            // 
            this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabResult.Location = new System.Drawing.Point(0, 0);
            this.tabResult.Name = "tabResult";
            this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
            this.tabResult.Size = new System.Drawing.Size(1192, 477);
            this.tabResult.TabIndex = 0;
            this.tabResult.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.tabResult_SendLButtonClick);
            // 
            // lblResults
            // 
            this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblResults.Location = new System.Drawing.Point(0, 0);
            this.lblResults.Name = "lblResults";
            this.lblResults.Size = new System.Drawing.Size(1192, 16);
            this.lblResults.TabIndex = 1;
            this.lblResults.Text = "Report Results";
            this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblResults.Click += new System.EventHandler(this.lblResults_Click);
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelTop.Controls.Add(this.txtTerminal);
            this.panelTop.Controls.Add(this.lblTerminal);
            this.panelTop.Controls.Add(this.buttonHelp);
            this.panelTop.Controls.Add(this.label3);
            this.panelTop.Controls.Add(this.txtHandlingAgents);
            this.panelTop.Controls.Add(this.rdbHandlingAgents);
            this.panelTop.Controls.Add(this.rdbAirlines);
            this.panelTop.Controls.Add(this.txtAirline);
            this.panelTop.Controls.Add(this.lblProgress);
            this.panelTop.Controls.Add(this.progressBar1);
            this.panelTop.Controls.Add(this.btnLoadPrint);
            this.panelTop.Controls.Add(this.btnPrintPreview);
            this.panelTop.Controls.Add(this.btnCancel);
            this.panelTop.Controls.Add(this.btnOK);
            this.panelTop.Controls.Add(this.dtTo);
            this.panelTop.Controls.Add(this.label2);
            this.panelTop.Controls.Add(this.dtFrom);
            this.panelTop.Controls.Add(this.label1);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1192, 160);
            this.panelTop.TabIndex = 4;
            this.panelTop.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTop_Paint);
            // 
            // txtTerminal
            // 
            this.txtTerminal.Location = new System.Drawing.Point(332, 53);
            this.txtTerminal.Name = "txtTerminal";
            this.txtTerminal.Size = new System.Drawing.Size(68, 20);
            this.txtTerminal.TabIndex = 3;
            this.txtTerminal.TextChanged += new System.EventHandler(this.txtTerminal_TextChanged);
            // 
            // lblTerminal
            // 
            this.lblTerminal.Location = new System.Drawing.Point(268, 57);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(56, 18);
            this.lblTerminal.TabIndex = 32;
            this.lblTerminal.Text = "Terminal:";
            this.lblTerminal.Click += new System.EventHandler(this.lblTerminal_Click);
            // 
            // buttonHelp
            // 
            this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonHelp.Location = new System.Drawing.Point(392, 116);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(75, 23);
            this.buttonHelp.TabIndex = 9;
            this.buttonHelp.Text = "Help";
            this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(320, 16);
            this.label3.TabIndex = 20;
            this.label3.Text = "Note: Use comma separation for multiple values";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // txtHandlingAgents
            // 
            this.txtHandlingAgents.BackColor = System.Drawing.SystemColors.Window;
            this.txtHandlingAgents.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHandlingAgents.Location = new System.Drawing.Point(136, 87);
            this.txtHandlingAgents.Name = "txtHandlingAgents";
            this.txtHandlingAgents.Size = new System.Drawing.Size(100, 20);
            this.txtHandlingAgents.TabIndex = 4;
            this.txtHandlingAgents.TextChanged += new System.EventHandler(this.txtHandlingAgents_TextChanged);
            // 
            // rdbHandlingAgents
            // 
            this.rdbHandlingAgents.Location = new System.Drawing.Point(16, 87);
            this.rdbHandlingAgents.Name = "rdbHandlingAgents";
            this.rdbHandlingAgents.Size = new System.Drawing.Size(108, 24);
            this.rdbHandlingAgents.TabIndex = 19;
            this.rdbHandlingAgents.Text = "Handling Agents:";
            this.rdbHandlingAgents.CheckedChanged += new System.EventHandler(this.rdbHandlingAgents_CheckedChanged);
            // 
            // rdbAirlines
            // 
            this.rdbAirlines.Location = new System.Drawing.Point(16, 55);
            this.rdbAirlines.Name = "rdbAirlines";
            this.rdbAirlines.Size = new System.Drawing.Size(104, 24);
            this.rdbAirlines.TabIndex = 18;
            this.rdbAirlines.Text = "Airlines:";
            this.rdbAirlines.CheckedChanged += new System.EventHandler(this.rdbAirlines_CheckedChanged);
            // 
            // txtAirline
            // 
            this.txtAirline.BackColor = System.Drawing.SystemColors.Window;
            this.txtAirline.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAirline.Location = new System.Drawing.Point(136, 55);
            this.txtAirline.Name = "txtAirline";
            this.txtAirline.Size = new System.Drawing.Size(100, 20);
            this.txtAirline.TabIndex = 2;
            this.txtAirline.TextChanged += new System.EventHandler(this.txtAirline_TextChanged);
            // 
            // lblProgress
            // 
            this.lblProgress.Location = new System.Drawing.Point(485, 94);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(212, 16);
            this.lblProgress.TabIndex = 12;
            this.lblProgress.Click += new System.EventHandler(this.lblProgress_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(484, 116);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(216, 23);
            this.progressBar1.TabIndex = 13;
            this.progressBar1.Visible = false;
            this.progressBar1.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // btnLoadPrint
            // 
            this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLoadPrint.Location = new System.Drawing.Point(232, 116);
            this.btnLoadPrint.Name = "btnLoadPrint";
            this.btnLoadPrint.Size = new System.Drawing.Size(75, 23);
            this.btnLoadPrint.TabIndex = 7;
            this.btnLoadPrint.Text = "Loa&d + Print";
            this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
            // 
            // btnPrintPreview
            // 
            this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPrintPreview.Location = new System.Drawing.Point(152, 116);
            this.btnPrintPreview.Name = "btnPrintPreview";
            this.btnPrintPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPrintPreview.TabIndex = 6;
            this.btnPrintPreview.Text = "&Print Preview";
            this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Location = new System.Drawing.Point(312, 116);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "&Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOK.Location = new System.Drawing.Point(72, 116);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "&Load Data";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTo.Location = new System.Drawing.Point(268, 4);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(136, 20);
            this.dtTo.TabIndex = 1;
            this.dtTo.ValueChanged += new System.EventHandler(this.dtTo_ValueChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(232, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "to:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFrom.Location = new System.Drawing.Point(92, 4);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(132, 20);
            this.dtFrom.TabIndex = 0;
            this.dtFrom.ValueChanged += new System.EventHandler(this.dtFrom_ValueChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date from:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmFlightStatisticsDomesticInternationalCombined
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1192, 653);
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelTop);
            this.Name = "frmFlightStatisticsDomesticInternationalCombined";
            this.Text = "Flights According to Domestic/International/Combined";
            this.Load += new System.EventHandler(this.frmFlightStatisticsDomesticInternationalCombined_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmFlightStatisticsDomesticInternationalCombined_HelpRequested);
            this.panelBody.ResumeLayout(false);
            this.panelTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private void frmFlightStatisticsDomesticInternationalCombined_Load(object sender, System.EventArgs e)
		{
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strFltiAndDescEntry = myIni.IniReadValue("FIPSREPORT","FLIGHTS_ACCORDING_FLTI");
			string[] strFltiAndDesc;			
			if(strFltiAndDescEntry.Length == 0)
			{
				strFltiAndDesc = strDefFltiAndDesc.Split(',');
			}
			else
			{
				strFltiAndDesc = strFltiAndDescEntry.Split(',');
			}
			strFlti = new string[strFltiAndDesc.Length/2];
			strFltiDesc = new string[strFltiAndDesc.Length/2];

			for(int i = 0 ; i < strFltiAndDesc.Length; i+=2)
			{
				strFlti[i/2] = strFltiAndDesc[i];
				strFltiDesc[i/2] = strFltiAndDesc[i+1];

				Helper.InsertString(3*i/2 + 1,ref strTabHeader,',',"Arr " + strFlti[i/2]);
				Helper.InsertString(3*i/2 + 2,ref strTabHeader,',',"Dep " + strFlti[i/2]);
				Helper.InsertString(3*i/2 + 3,ref strTabHeader,',',"Tot " + strFltiDesc[i/2]);

				Helper.InsertString(3*i/2 + 1,ref strLogicalFields,',',"ARR_" + strFlti[i/2]);
				Helper.InsertString(3*i/2 + 2,ref strLogicalFields,',',"DEP_" + strFlti[i/2]);
				Helper.InsertString(3*i/2 + 3,ref strLogicalFields,',',"TOT_" + strFltiDesc[i/2]);

				Helper.InsertString(3*i/2 + 1,ref strTabHeaderLens,',',"45");
				Helper.InsertString(3*i/2 + 2,ref strTabHeaderLens,',',"45");
				Helper.InsertString(3*i/2 + 3,ref strTabHeaderLens,',',(("Tot " + strFltiDesc[i/2]).Length*8).ToString());
			}

			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
			PrepareFilterData();
			NumberofFlightsinVertical = new double[15];
			NumberofFlightsinVertical.Initialize();			
		}

		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}


		/// <summary>
		/// Initializes the tab controll.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
            tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
            tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
          	tabResult.HeaderFontSize = 14;
            
            tabResult.AutoSizeByHeader = true;
			
			string[] strLabelHeaderEntry = new string[2];
			if((bmShowStev = Helper.IsSTEVConfigured(this,ref strLabelHeaderEntry,true,false)) == true)
			{
				lblTerminal.Text = strLabelHeaderEntry[0] + ":";
			}
			else
			{
				lblTerminal.Hide();
				txtTerminal.Hide();
			}
						
			/*Helper.RemoveOrReplaceString(1,ref strTabHeader,',',"Arr  " + strFlti[0],false);
			Helper.RemoveOrReplaceString(2,ref strTabHeader,',',"Dep  " + strFlti[0],false);
			Helper.RemoveOrReplaceString(3,ref strTabHeader,',',"Tot  " + strFltiDesc[0],false);

			Helper.RemoveOrReplaceString(4,ref strTabHeader,',',"Arr  " + strFlti[1],false);
			Helper.RemoveOrReplaceString(5,ref strTabHeader,',',"Dep  " + strFlti[1],false);
			Helper.RemoveOrReplaceString(6,ref strTabHeader,',',"Tot  " + strFltiDesc[1],false);

			Helper.RemoveOrReplaceString(7,ref strTabHeader,',',"Arr  " + strFlti[2],false);
			Helper.RemoveOrReplaceString(8,ref strTabHeader,',',"Dep  " + strFlti[2],false);
			Helper.RemoveOrReplaceString(9,ref strTabHeader,',',"Tot  " + strFltiDesc[2],false);

			Helper.RemoveOrReplaceString(10,ref strTabHeader,',',"Arr  " + strFlti[3],false);
			Helper.RemoveOrReplaceString(11,ref strTabHeader,',',"Dep  " + strFlti[3],false);
			Helper.RemoveOrReplaceString(12,ref strTabHeader,',',"Tot  " + strFltiDesc[3],false);*/
            tabResult.HeaderString = strTabHeader;
            tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
			rdbAirlines.Checked =true;
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			strTxtAirline ="";
			strTxtAgent ="";
			//NumberofFlightsinVertical = new double[15];
			string strError = ValidateUserEntry();
			
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent();
			if (txtAirline.Enabled )
			{

				strTxtAirline =  txtAirline.Text ;
				strTxtAirline = strTxtAirline.Replace(",","','").ToUpper();

			}
			if(txtHandlingAgents.Enabled)
			{
				strTxtAgent = txtHandlingAgents.Text;
				strTxtAgent = strTxtAgent.Replace(",","','").ToUpper();
			}
			if (txtAirline.Enabled && txtAirline.Text.Length !=0)	LoadReportData(strWhereAftAirline);
			if (txtAirline.Enabled && txtAirline.Text.Length ==0)	LoadReportData(strWhereAftWithoutAirline);
			if (txtHandlingAgents.Enabled && txtHandlingAgents.Text.Length !=0) LoadHaiReportData(strWhereHaiAgent);
			if (txtHandlingAgents.Enabled && txtHandlingAgents.Text.Length ==0) LoadHaiReportData(strWhereWithoutHaiAgent);
			PrepareFillReportData();
			this.Cursor = Cursors.Arrow;
		}

		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			return strRet;
		}

		private void LoadReportData(string strWhere)
		{
			for(int j = 0; j < NumberofFlightsinVertical.Length; j++)
			{
				NumberofFlightsinVertical[j] = 0;
			}
			string strTmpAFTWhere = strWhere;
		//	StringBuilder sb = new StringBuilder(10000);
			//Clear the tab
			tabResult.ResetContent();
			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			//Load the data from AFTTAB
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,ADID,ALC2,ALC3,STOA,STOD,FLTI,STEV", 
				"10,1,2,3,14,14,1,1", 
				"URNO,ADID,ALC2,ALC3,STOA,STOD,FLTI,STEV");
			myAFT.TimeFields = "STOA,STOD";
			myAFT.Clear();
			myAFT.TimeFieldsInitiallyInUtc = true;
			string strTerminal = "";
			if(bmShowStev == true)
			{
				strTerminal = txtTerminal.Text.Trim();
			}
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Show();
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpAFTWhere = strWhere;
				if(strTerminal != "")
				{
					strTmpAFTWhere += strTerminalWhere;
				}
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@TO", strDateTo );
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@AIRLINE", strTxtAirline);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@HOPO", UT.Hopo);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@STEV",strTerminal);
				//Load the data from AFTTAB
				myAFT.Load(strTmpAFTWhere);
				myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
				NumberofFlights= new double[15];
				for(int i = 0; i < myAFT.Count; i++)
				{
					PrepareReportCount(i,datReadFrom);
				}
				myAFT.Clear();
				//New function added
				FillGrid (datReadFrom);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
		}
		///<summary>
		///This method loads data for the Handling agents
		///</summary>
		
		private void LoadHaiReportData(string strWhere)
		{
			for(int j = 0; j < NumberofFlightsinVertical.Length; j++)
			{
				NumberofFlightsinVertical[j] = 0;
			}
			string strTmpAFTWhere = strWhere;
			 StringBuilder sb = new StringBuilder(10000);
			//Clear the tab
			tabResult.ResetContent();
			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			//Load the data from AFTTAB
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,ADID,ALC2,ALC3,STOA,STOD,FLTI,STEV", 
				"10,1,2,3,14,14,1,1", 
				"URNO,ADID,ALC2,ALC3,STOA,STOD,FLTI,STEV");
			myAFT.TimeFields = "STOA,STOD";
			myAFT.Clear();
			myAFT.TimeFieldsInitiallyInUtc = true;
			string strTerminal = "";
			if(bmShowStev == true)
			{
				strTerminal = txtTerminal.Text.Trim();
			}
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Show();
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpAFTWhere = strWhere;				
				if(strTerminal != "")
				{
					strTmpAFTWhere += strTerminalWhere;
				}
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@TO", strDateTo );
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@AIRLINE", txtAirline.Text);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@HSNA", strTxtAgent);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@HOPO", UT.Hopo);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@STEV",strTerminal);
				//Load the data from AFTTAB
				myAFT.Load(strTmpAFTWhere);
				myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
				NumberofFlights= new double[15];
				for(int i = 0; i < myAFT.Count; i++)
				{
					//	datTMP = UT.CedaDateToDateTime( myAFT[i][strDepartureTimeKey]);
					PrepareReportCount(i,datReadFrom );
				}
				myAFT.Clear();
				//New function added
				FillGrid (datReadFrom);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
	//		myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
		}

		/// <summary>
		/// This method counts the  number of flights
		/// </summary>
		private void PrepareReportCount(int i,DateTime From)
		{			
			//Start with the count of Arrival flights
			if (myAFT[i]["ADID"] == "A")
			{
				//Check for Domestic flights
                if (strFlti.Length >= 1 && strFlti[0].Contains(myAFT[i]["FLTI"]))//"D")
				{
					NumberofFlights[0]++;
					NumberofFlights[2]++;
					NumberofFlights[12]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[0]++;
					NumberofFlightsinVertical[2]++;
					NumberofFlightsinVertical[12]++;
					NumberofFlightsinVertical[14]++; 

				}
				//Check for International Flights
                if (strFlti.Length >= 2 && strFlti[1].Contains(myAFT[i]["FLTI"]))// "I")
				{
					NumberofFlights[3]++;
					NumberofFlights[5]++;
					NumberofFlights[12]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[3]++;
					NumberofFlightsinVertical[5]++;
					NumberofFlightsinVertical[12]++;
					NumberofFlightsinVertical[14]++;
				}
				//Check for Regional Flights
                if (strFlti.Length >= 3 && strFlti[2].Contains(myAFT[i]["FLTI"]))//"R")
				{
					NumberofFlights[6]++;
					NumberofFlights[8]++;
					NumberofFlights[12]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[6]++;
					NumberofFlightsinVertical[8]++;
					NumberofFlightsinVertical[12]++;
					NumberofFlightsinVertical[14]++;
				}
				//Check for combined Flights
                if (strFlti.Length >= 4 && strFlti[3].Contains(myAFT[i]["FLTI"]))//"M")
				{
					NumberofFlights[9]++;
					NumberofFlights[11]++;
					NumberofFlights[12]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[9]++;
					NumberofFlightsinVertical[11]++;
					NumberofFlightsinVertical[12]++;
					NumberofFlightsinVertical[14]++;
				}
			}
			//Check for Departure flights
			if (myAFT[i]["ADID"] == "D")
			{
				//Check for Domestic flights
                if (strFlti.Length >= 1 && strFlti[0].Contains(myAFT[i]["FLTI"]))//"D")
				{
					NumberofFlights[1]++;
					NumberofFlights[2]++;
					NumberofFlights[13]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[1]++;
					NumberofFlightsinVertical[2]++;
					NumberofFlightsinVertical[13]++;
					NumberofFlightsinVertical[14]++;
				}
				//Check for International Flights
                if (strFlti.Length >= 2 && strFlti[1].Contains(myAFT[i]["FLTI"]))//"I")
				{
					NumberofFlights[4]++;
					NumberofFlights[5]++;
					NumberofFlights[13]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[4]++;
					NumberofFlightsinVertical[5]++;
					NumberofFlightsinVertical[13]++;
					NumberofFlightsinVertical[14]++;
				}
				//Check for Regional Flights
                if (strFlti.Length >= 3 && strFlti[2].Contains(myAFT[i]["FLTI"]))//"R")
				{
					NumberofFlights[7]++;
					NumberofFlights[8]++;
					NumberofFlights[13]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[7]++;
					NumberofFlightsinVertical[8]++;
					NumberofFlightsinVertical[13]++;
					NumberofFlightsinVertical[14]++;
				}
				//Check for combined Flights
                if (strFlti.Length >= 4 && strFlti[3].Contains(myAFT[i]["FLTI"]))//"M")
				{
					NumberofFlights[10]++;
					NumberofFlights[11]++;
					NumberofFlights[13]++;
					NumberofFlights[14]++; 

					NumberofFlightsinVertical[10]++;
					NumberofFlightsinVertical[11]++;
					NumberofFlightsinVertical[13]++;
					NumberofFlightsinVertical[14]++;
				}
			}
			//Check for Rotation flights
			if (myAFT[i]["ADID"] == "B")
			{
				DateTime dtStoa = UT.CedaFullDateToDateTime(myAFT[i]["STOA"]);
				
				DateTime dtStod = UT.CedaFullDateToDateTime(myAFT[i]["STOD"]);

				if (Helper.DateString(" dtStoa","ddMMyyyy") ==Helper.DateString("From","ddMMyyyy")) // Arrival Flights
				{
					//Check for Domestic flights
                    if (strFlti.Length >= 1 && strFlti[0].Contains(myAFT[i]["FLTI"]))//"D")
					{
						NumberofFlights[0]++;
						NumberofFlights[2]++;
						NumberofFlights[12]++;
						NumberofFlights[14]++; 

						NumberofFlightsinVertical[0]++;
						NumberofFlightsinVertical[2]++;
						NumberofFlightsinVertical[12]++;
						NumberofFlightsinVertical[14]++; 

					}
					//Check for International Flights
                    if (strFlti.Length >= 2 && strFlti[1].Contains(myAFT[i]["FLTI"]))//"I")
					{
						NumberofFlights[3]++;
						NumberofFlights[5]++;
						NumberofFlights[12]++;
						NumberofFlights[14]++; 

						NumberofFlightsinVertical[3]++;
						NumberofFlightsinVertical[5]++;
						NumberofFlightsinVertical[12]++;
						NumberofFlightsinVertical[14]++;
					}
					//Check for Regional Flights
                    if (strFlti.Length >= 3 && strFlti[2].Contains(myAFT[i]["FLTI"]))//"R")
					{
						NumberofFlights[6]++;
						NumberofFlights[8]++;
						NumberofFlights[12]++;
						NumberofFlights[14]++; 

						NumberofFlightsinVertical[6]++;
						NumberofFlightsinVertical[8]++;
						NumberofFlightsinVertical[12]++;
						NumberofFlightsinVertical[14]++;
					}
					//Check for combined Flights
                    if (strFlti.Length >= 4 && strFlti[3].Contains(myAFT[i]["FLTI"]))//"M")
					{
						NumberofFlights[9]++;
						NumberofFlights[11]++;
						NumberofFlights[12]++;
						NumberofFlights[14]++; 

						NumberofFlightsinVertical[9]++;
						NumberofFlightsinVertical[11]++;
						NumberofFlightsinVertical[12]++;
						NumberofFlightsinVertical[14]++;
					}
				}
		
				if (Helper.DateString( "dtStod","ddMMyyyy") ==Helper.DateString("From","ddMMyyyy"))
				{
					//Check for Domestic flights
                    if (strFlti.Length >= 1 && strFlti[0].Contains(myAFT[i]["FLTI"]))//"D")
					{
						NumberofFlights[1]++;
						NumberofFlights[2]++;
						NumberofFlights[13]++;
						NumberofFlights[14]++; 
					}
					//Check for International Flights
                    if (strFlti.Length >= 2 && strFlti[1].Contains(myAFT[i]["FLTI"]))//"I")
					{
						NumberofFlights[4]++;
						NumberofFlights[5]++;
						NumberofFlights[13]++;
						NumberofFlights[14]++; 
					}
					//Check for Regional Flights
                    if (strFlti.Length >= 3 && strFlti[2].Contains(myAFT[i]["FLTI"]))//"R")
					{
						NumberofFlights[7]++;
						NumberofFlights[8]++;
						NumberofFlights[13]++;
						NumberofFlights[14]++; 
					}
					//Check for combined Flights
                    if (strFlti.Length >= 4 && strFlti[3].Contains(myAFT[i]["FLTI"]))//"M")
					{
						NumberofFlights[10]++;
						NumberofFlights[11]++;
						NumberofFlights[13]++;
						NumberofFlights[14]++; 
					}
				}
			}
		}
		private void FillGrid(DateTime From)
		{
			StringBuilder sb = new StringBuilder(10000);
			string strValues = "";
			strValues += From.ToLocalTime().Date.ToString("dd/MM/yyyy")/*.ToShortDateString()*/ + ",";
			if(strFlti.Length >= 1)
			{
				strValues += NumberofFlights[0].ToString()+"," + NumberofFlights[1].ToString() +"," + NumberofFlights[2].ToString() +","; 
			}
			if(strFlti.Length >= 2)
			{
				strValues += NumberofFlights[3].ToString()+","+ NumberofFlights[4].ToString()+ "," + NumberofFlights[5].ToString()+ ",";
			}
			if(strFlti.Length >= 3)
			{
				strValues += NumberofFlights[6].ToString()+","+ NumberofFlights[7].ToString()+ "," + NumberofFlights[8].ToString()+ ",";
			}
			
			if(strFlti.Length >= 4)
			{
				strValues += NumberofFlights[9].ToString()+","+ NumberofFlights[10].ToString()+ "," + NumberofFlights[11].ToString()+ "," ;
			}
			strValues += NumberofFlights[12].ToString()+"," + NumberofFlights[13].ToString()+ "," + NumberofFlights[14].ToString()+ "\n";;
			sb.Append(strValues);
			tabResult.InsertBuffer(sb.ToString(), "\n");
		}
		private void PrepareFillReportData()
		{
			StringBuilder sb1 = new StringBuilder(10000);
			string strValuesTotal = "";
		//	tabResult.Sort("0", true, true);
			strValuesTotal += "-- Total --" + ",";
			if(strFlti.Length >= 1)
			{
				strValuesTotal += NumberofFlightsinVertical[0].ToString()+ "," + NumberofFlightsinVertical[1].ToString()+ "," + NumberofFlightsinVertical[2].ToString() + ","; 
			}
			if(strFlti.Length >= 2)
			{
				strValuesTotal += NumberofFlightsinVertical[3].ToString()+ "," + NumberofFlightsinVertical[4].ToString()+ "," + NumberofFlightsinVertical[5].ToString()+ "," ;
			}
			if(strFlti.Length >= 3)
			{
				strValuesTotal += NumberofFlightsinVertical[6].ToString()+ "," + NumberofFlightsinVertical[7].ToString()+ "," + NumberofFlightsinVertical[8].ToString()+ "," ;
			}
			if(strFlti.Length >= 4)
			{
				strValuesTotal += NumberofFlightsinVertical[9].ToString()+ "," + NumberofFlightsinVertical[10].ToString()+ "," + NumberofFlightsinVertical[11].ToString()+ "," ;
			}
			strValuesTotal += NumberofFlightsinVertical[12].ToString()+ "," + NumberofFlightsinVertical[13].ToString()+ "," + NumberofFlightsinVertical[14].ToString()+ "\n";			
			sb1.Append(strValuesTotal);
            tabResult.InsertBuffer(sb1.ToString(), "\n");
			lblProgress.Text = "";
			lblProgress.Refresh ();
			progressBar1.Visible = false;
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		private void rdbAirlines_CheckedChanged(object sender, System.EventArgs e)
		{
			txtHandlingAgents.Enabled=false;
			txtAirline.Enabled=true;
		}
		private void rdbHandlingAgents_CheckedChanged(object sender, System.EventArgs e)
		{
			txtAirline.Enabled  =false;
			txtHandlingAgents.Enabled=true;
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + NumberofFlightsinVertical[14].ToString();
			strSubHeader += " " + " Tot. ARR: " + NumberofFlightsinVertical[12].ToString();
			strSubHeader += " " + " Tot. DEP: " + NumberofFlightsinVertical[13].ToString() + ")";
			strReportHeader = "Flights According to Domestic/International/Combined: ";
			strReportSubHeader = strSubHeader;
            
			if(rdbAirlines.Checked && txtAirline.Text.Length > 0)
			{
				strReportHeader += "(" + rdbAirlines.Text +  "-" + txtAirline.Text + ")";                
			}
			else if(txtHandlingAgents.Text.Length > 0)
			{
				strReportHeader += "(" + rdbHandlingAgents.Text + "-" + txtHandlingAgents.Text + ")";
			}

            tabResult.ColumnAlignmentString = "C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C";            
            prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult, strReportHeader, strReportSubHeader, "", 9);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			if(Helper.UseSpreadBuilder() == true)
			{
				activeReport = rpt;
                
				frm.UseSpreadBuilder(true);
				frm.GetBtnExcelExport().Click += new EventHandler(frmFlightStatisticsDomesticInternationalCombined_Click);
			}
			else
			{
				frm.UseSpreadBuilder(false);
			}
			frm.Show();
		}
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			strTxtAirline ="";
			strTxtAgent ="";
			//NumberofFlightsinVertical = new double[15];
			string strError = ValidateUserEntry();
			
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent();
			if (txtAirline.Enabled )
			{

				strTxtAirline =  txtAirline.Text ;
				strTxtAirline = strTxtAirline.Replace(",","','").ToUpper();

			}
			if(txtHandlingAgents.Enabled)
			{
				strTxtAgent = txtHandlingAgents.Text;
				strTxtAgent = strTxtAgent.Replace(",","','").ToUpper();
			}
			if (txtAirline.Enabled && txtAirline.Text.Length !=0)	LoadReportData(strWhereAftAirline);
			if (txtAirline.Enabled && txtAirline.Text.Length ==0)	LoadReportData(strWhereAftWithoutAirline);
			if (txtHandlingAgents.Enabled && txtHandlingAgents.Text.Length !=0) LoadHaiReportData(strWhereHaiAgent);
			if (txtHandlingAgents.Enabled && txtHandlingAgents.Text.Length ==0) LoadHaiReportData(strWhereWithoutHaiAgent);
			PrepareFillReportData();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}

		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			myDB.Unbind("AFT");
			this.Close ();
		}

		private void tabResult_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if(tabResult.SortOrderASC == true)
					{
						tabResult.DeleteLine(tabResult.GetLineCount()-1);
						tabResult.Sort(e.colNo.ToString(), false, true);
						PrepareFillReportData();
					}
					else
					{
						tabResult.DeleteLine(tabResult.GetLineCount()-1);
						tabResult.Sort(e.colNo.ToString(), true, true);
						PrepareFillReportData();
					}
				}
				else
				{
					tabResult.DeleteLine(tabResult.GetLineCount()-1);
					tabResult.Sort( e.colNo.ToString(), true, true);
					PrepareFillReportData();
				}
				tabResult.Refresh();
			}
		}

		private void InitializeMouseEvents()
		{			
			((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmFlightStatisticsDomesticInternationalCombined_MouseWheel);
			((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmFlightStatisticsDomesticInternationalCombined_MouseEnter);
			((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmFlightStatisticsDomesticInternationalCombined_MouseLeave);
		}

		private void frmFlightStatisticsDomesticInternationalCombined_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && tabResult.GetVScrollPos() > 0)
			{				
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
			{
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
			}
		}

		private void frmFlightStatisticsDomesticInternationalCombined_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmFlightStatisticsDomesticInternationalCombined_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmFlightStatisticsDomesticInternationalCombined_Click(object sender, EventArgs e)
		{
			Helper.ExportToExcel(tabResult,strReportHeader,strReportSubHeader,strTabHeaderLens,8,0,activeReport);
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmFlightStatisticsDomesticInternationalCombined_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}

        private void panelBody_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelTab_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblResults_Click(object sender, EventArgs e)
        {

        }

        private void panelTop_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtTerminal_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblTerminal_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtHandlingAgents_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAirline_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblProgress_Click(object sender, EventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void dtTo_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dtFrom_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
	}
}
