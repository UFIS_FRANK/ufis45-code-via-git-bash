using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
    /// <summary>
    /// Summary description for frmInventory.
    /// </summary>
    public class frmInventory : System.Windows.Forms.Form
    {
        #region _My Members
        /// <summary>
        /// The where statement: all "@@xx" fields will be replaced
        /// with the true values according to the user's entry.
        /// </summary>
        private string strWhereFOG = "WHERE STAT='O'";
        private string strWhereAFT = "WHERE URNO IN ('@@URNOLIST')";
        /// <summary>
        /// The logical field names of the tab control for internal 
        /// development purposes
        /// </summary>
        private string strLogicalFields = "NO,REGN,TYPE,POS,TIFA,LAND";
        /// <summary>
        /// The header columns, which must be used for the report output
        /// </summary>
        private string strTabHeader = "No.,Registration,A/C,Position,Date of Arrival,Touch down";
        /// <summary>
        /// The lengths, which must be used for the report's column widths
        /// </summary>
        private string strTabHeaderLens = "30,100,60,80,120,120";
        /// <summary>
        /// The one and only IDatabase obejct.
        /// </summary>
        private IDatabase myDB;
        /// <summary>
        /// ITable object for the AFTTAB
        /// </summary>
        private ITable myAFT;
        ///// <summary>
        ///// ITable object for the AFTTAB
        ///// </summary>
        private ITable myAFTRet;
        /// <summary>
        /// ITable object for the FOGTAB
        /// </summary>
        private ITable myFOG;
        /// <summary>
        /// Boolean for mouse status with respect to tabResult control
        /// </summary>
        private bool bmMouseInsideTabControl = false;
        /// <summary>
        /// Report Header
        /// </summary>
        private string strReportHeader = "";
        /// <summary>
        /// Report Sub Header
        /// </summary>
        private string strReportSubHeader = "";
        /// <summary>
        /// Report Object
        /// </summary>
        private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;

        #endregion _My Members

        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.Panel panelTab;
        private AxTABLib.AxTAB tabResult;
        private System.Windows.Forms.Label lblResults;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnPrintPreview;
        private System.Windows.Forms.Button btnCancel;
        private System.Timers.Timer timer1;
        private System.Windows.Forms.Button buttonHelp;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public frmInventory()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeMouseEvents();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmInventory));
            this.panelBody = new System.Windows.Forms.Panel();
            this.panelTab = new System.Windows.Forms.Panel();
            this.tabResult = new AxTABLib.AxTAB();
            this.lblResults = new System.Windows.Forms.Label();
            this.panelTop = new System.Windows.Forms.Panel();
            this.lblProgress = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnPrintPreview = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.timer1 = new System.Timers.Timer();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.panelBody.SuspendLayout();
            this.panelTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timer1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelBody
            // 
            this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelBody.Controls.Add(this.panelTab);
            this.panelBody.Controls.Add(this.lblResults);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 56);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(664, 398);
            this.panelBody.TabIndex = 5;
            // 
            // panelTab
            // 
            this.panelTab.Controls.Add(this.tabResult);
            this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTab.Location = new System.Drawing.Point(0, 16);
            this.panelTab.Name = "panelTab";
            this.panelTab.Size = new System.Drawing.Size(664, 382);
            this.panelTab.TabIndex = 2;
            // 
            // tabResult
            // 
            this.tabResult.ContainingControl = this;
            this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabResult.Location = new System.Drawing.Point(0, 0);
            this.tabResult.Name = "tabResult";
            this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
            this.tabResult.Size = new System.Drawing.Size(664, 382);
            this.tabResult.TabIndex = 0;
            this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
            // 
            // lblResults
            // 
            this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblResults.Location = new System.Drawing.Point(0, 0);
            this.lblResults.Name = "lblResults";
            this.lblResults.Size = new System.Drawing.Size(664, 16);
            this.lblResults.TabIndex = 1;
            this.lblResults.Text = "Report Results";
            this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelTop.Controls.Add(this.buttonHelp);
            this.panelTop.Controls.Add(this.lblProgress);
            this.panelTop.Controls.Add(this.progressBar1);
            this.panelTop.Controls.Add(this.btnPrintPreview);
            this.panelTop.Controls.Add(this.btnCancel);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(664, 56);
            this.panelTop.TabIndex = 4;
            // 
            // lblProgress
            // 
            this.lblProgress.Location = new System.Drawing.Point(304, 8);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(212, 16);
            this.lblProgress.TabIndex = 22;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(304, 24);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(216, 23);
            this.progressBar1.TabIndex = 21;
            this.progressBar1.Visible = false;
            // 
            // btnPrintPreview
            // 
            this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPrintPreview.Location = new System.Drawing.Point(8, 24);
            this.btnPrintPreview.Name = "btnPrintPreview";
            this.btnPrintPreview.TabIndex = 9;
            this.btnPrintPreview.Text = "&Print Preview";
            this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Location = new System.Drawing.Point(104, 24);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "&Close";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.SynchronizingObject = this;
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Elapsed);
            // 
            // buttonHelp
            // 
            this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonHelp.Location = new System.Drawing.Point(200, 24);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.TabIndex = 30;
            this.buttonHelp.Text = "Help";
            this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
            // 
            // frmInventory
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(664, 454);
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmInventory";
            this.Text = "Inventory (A/C on Ground)";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmInventory_Closing);
            this.Load += new System.EventHandler(this.frmInventory_Load);
            this.HelpRequested += new HelpEventHandler(frmInventory_HelpRequested);
            this.panelBody.ResumeLayout(false);
            this.panelTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.timer1)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        /// <summary>
        /// Load of the form
        /// </summary>
        /// <param name="sender">Originator of the method call</param>
        /// <param name="e">Event Arguments</param>
        private void frmInventory_Load(object sender, System.EventArgs e)
        {
            myDB = UT.GetMemDB();
            InitTab();
            //LoadData();
        }
        /// <summary>
        /// Initializes the tab controll.
        /// </summary>
        private void InitTab()
        {
            tabResult.ResetContent();
            tabResult.ShowHorzScroller(true);
            tabResult.EnableHeaderSizing(true);
            tabResult.SetTabFontBold(true);
            tabResult.LifeStyle = true;
            tabResult.LineHeight = 16;
            tabResult.FontName = "Arial";
            tabResult.FontSize = 14;
            tabResult.HeaderFontSize = 14;
            tabResult.AutoSizeByHeader = true;

            tabResult.HeaderString = strTabHeader;
            tabResult.LogicalFieldList = strLogicalFields;
            tabResult.HeaderLengthString = strTabHeaderLens;
            tabResult.DateTimeSetColumn(4);
            tabResult.DateTimeSetColumnFormat(4, "YYYYMMDD", "DD'.'MM'.'YYYY");
        }
        /// <summary>
        /// Load the data from the fogtab and then load the corresponding
        /// flights by WHERE URNO IN (FOG.FLNU-List) and display the
        /// data in the tab control.
        /// </summary>
        private void LoadData()
        {
            myDB.Unbind("FOG");
            myFOG = myDB.Bind("FOG", "FOG", "FLNU", "10", "FLNU");
            myFOG.Clear();
            myFOG.Load(strWhereFOG);

            myDB.Unbind("AFTRT");
            myAFTRet = myDB.Bind("AFTRT", "AFT", "RKEY,REGN,ACT3,PSTA,TIFA,LAND,ONBL,ADID,FTYP", "10,12,3,10,14,14,14,14,1,1", "RKEY,REGN,ACT3,PSTA,TIFA,LAND,ONBL,ADID,FTYP");
            myAFTRet.TimeFields = "TIFA,ONBL,LAND";
            myAFTRet.Clear();
            myAFTRet.Load("WHERE ADID = 'B' AND FTYP NOT IN ('T','G') AND RKEY  IN (SELECT FLNU FROM FOGTAB WHERE STAT = 'O')");
            myAFTRet.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

            myDB.Unbind("AFT");
            myAFT = myDB.Bind("AFT", "AFT", "RKEY,REGN,ACT3,PSTA,TIFA,LAND,ONBL,ADID,FTYP", "10,12,3,10,14,14,14,14,1,1", "RKEY,REGN,ACT3,PSTA,TIFA,LAND,ONBL,ADID,FTYP");
            myAFT.TimeFields = "TIFA,ONBL,LAND";
            myAFT.Clear();
            myAFT.Load("WHERE  ADID IN ('A', 'B') AND FTYP <>'Z' AND URNO IN (SELECT FLNU  FROM FOGTAB WHERE STAT='O')");
            lblProgress.Text = "Loading Counter Data";
            lblProgress.Refresh();
            myAFT.Sort("REGN", true);
            myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
            lblProgress.Text = "";
            progressBar1.Hide();
        }

        /// <summary>
        /// Loops through the data and prepares the tab control.
        /// </summary>
        private void PrepareReportData()
        { 

            //StringBuilder sb = new StringBuilder(10000);
            //for (int i = 0; i < myAFT.Count; i++)
            //{
            //    int lfd = i + 1;
            //    string strValues = "";
            //    strValues += lfd.ToString() + ",";
            //    strValues += myAFT[i]["REGN"] + ",";
            //    strValues += myAFT[i]["ACT3"] + ",";
            //    strValues += myAFT[i]["PSTA"] + ",";
            //    strValues += Helper.DateString(myAFT[i]["TIFA"], "yyyyMMdd") + ",";
            //    strValues += Helper.DateString(myAFT[i]["LAND"], "HH:mm") + "\n";
            //    sb.Append(strValues);
            //}
            //tabResult.InsertBuffer(sb.ToString(), "\n");
            ////tabResult.Refresh();

            //for (int i = 0; i < myAFTRet.Count; i++)
            //{
            //    string strValues = "";
            //    strValues += lfd.ToString() + ",";
            //    strValues += myAFTRet[i]["REGN"] + ",";
            //    strValues += myAFTRet[i]["ACT3"] + ",";
            //    strValues += myAFTRet[i]["PSTA"] + ",";
            //    strValues += Helper.DateString(myAFTRet[i]["TIFA"], "yyyyMMdd") + ",";
            //    strValues += Helper.DateString(myAFTRet[i]["ONBL"], "HH:mm") + "\n";


            //    sb.Append(strValues);
            //}



            StringBuilder sb = new StringBuilder(10000);
            for (int i = 0; i < myAFT.Count; i++)
            {
                int lfd = i + 1;
                string strValues = "";
                strValues += lfd.ToString() + ",";
                strValues += myAFT[i]["REGN"] + ",";
                strValues += myAFT[i]["ACT3"] + ",";
                strValues += myAFT[i]["PSTA"] + ",";
                strValues += Helper.DateString(myAFT[i]["TIFA"], "yyyyMMdd") + ",";
                strValues += Helper.DateString(myAFT[i]["LAND"], "HH:mm") + "\n";

                for (int j = 0; j < myAFTRet.Count; j++)
                {
                    if (myAFT[i]["RKEY"] == myAFTRet[j]["RKEY"])
                    {
                        strValues = "";
                        strValues += lfd.ToString() + ",";
                        strValues += myAFTRet[j]["REGN"] + ",";
                        strValues += myAFTRet[j]["ACT3"] + ",";
                        strValues += myAFTRet[j]["PSTA"] + ",";
                        strValues += Helper.DateString(myAFTRet[j]["TIFA"], "yyyyMMdd") + ",";
                        strValues += Helper.DateString(myAFTRet[j]["ONBL"], "HH:mm") + "\n";
                        break;
                    }
                }

                sb.Append(strValues);
            }
            tabResult.InsertBuffer(sb.ToString(), "\n");
            tabResult.Refresh();


        }
        /// <summary>
        /// Timer so that the form is still visible before the data load
        /// starts => to animate the progress bar in visible form.
        /// </summary>
        /// <param name="sender">Originator of the call</param>
        /// <param name="e">Event argument</param>
        private void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer1.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
            LoadData();
            PrepareReportData();
            this.Cursor = Cursors.Arrow;
        }
        /// <summary>
        /// Unbinds the ITable instances.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args.</param>
        private void frmInventory_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            myDB.Unbind("AFTRT");
            myDB.Unbind("AFT");
            myDB.Unbind("FOG");
        }
        /// <summary>
        /// Calls the generic report. The contructor must have the Tab 
        /// control and the reports headers as well as the types for
        /// each column to enable the reports to transform data, e.g.
        /// the data/time formatting. The lengths of the columns are
        /// dynamically read out of the tab control's header.
        /// </summary>
        private void RunReport()
        {
            string strSubHeader = "";
            strSubHeader = "Number of Registrations: " + tabResult.GetLineCount().ToString();
            strReportHeader = frmMain.strPrefixVal + "Inventory (A/C on Ground)";
            strReportSubHeader = strSubHeader;
            prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult, strReportHeader, strSubHeader, "", 10);
            frmPrintPreview frm = new frmPrintPreview(rpt);
            if (Helper.UseSpreadBuilder() == true)
            {
                activeReport = rpt;
                frm.UseSpreadBuilder(true);
                frm.GetBtnExcelExport().Click += new EventHandler(frmInventory_Click);
            }
            else
            {
                frm.UseSpreadBuilder(false);
            }
            frm.Show();
        }

        private void btnPrintPreview_Click(object sender, System.EventArgs e)
        {
            RunReport();
        }

        private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
        {
            if (e.lineNo == -1)
            {
                if (e.colNo == tabResult.CurrentSortColumn)
                {
                    if (tabResult.SortOrderASC == true)
                    {
                        tabResult.Sort(e.colNo.ToString(), false, true);
                    }
                    else
                    {
                        tabResult.Sort(e.colNo.ToString(), true, true);
                    }
                }
                else
                {
                    tabResult.Sort(e.colNo.ToString(), true, true);
                }
                tabResult.Refresh();
            }
        }

        private void InitializeMouseEvents()
        {
            ((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmInventory_MouseWheel);
            ((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmInventory_MouseEnter);
            ((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmInventory_MouseLeave);
        }

        private void frmInventory_MouseWheel(object sender, MouseEventArgs e)
        {
            if (bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
            {
                return;
            }
            if (e.Delta > 0 && tabResult.GetVScrollPos() > 0)
            {
                tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
            }
            else if (e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
            {
                tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
            }
        }

        private void frmInventory_MouseEnter(object sender, EventArgs e)
        {
            bmMouseInsideTabControl = true;
        }

        private void frmInventory_MouseLeave(object sender, EventArgs e)
        {
            bmMouseInsideTabControl = false;
        }

        private void frmInventory_Click(object sender, EventArgs e)
        {
            Helper.ExportToExcel(tabResult, strReportHeader, strReportSubHeader, strTabHeaderLens, 10, 0, activeReport);
        }

        private void buttonHelp_Click(object sender, System.EventArgs e)
        {
            Helper.ShowHelpFile(this);
        }

        private void frmInventory_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            Helper.ShowHelpFile(this);
        }
    }
}
