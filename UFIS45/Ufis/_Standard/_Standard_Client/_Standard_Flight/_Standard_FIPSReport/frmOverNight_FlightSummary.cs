using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmOverNight_FlightSummary.
	/// </summary>
	public class frmOverNight_FlightSummary : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		
		//		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO' ) OR ( STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO')) AND (FTYP='O' OR FTYP='S' OR FTYP='X')) AND STAT<>'DEL'";
		//		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO') AND (STOD BETWEEN '@@FROM' AND '@@TO'))) AND (FTYP='O' OR FTYP='S' OR FTYP='X') AND STAT<>'DEL' " ;
		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B')))) OR " +
			"((TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))))) AND (FTYP='O' OR FTYP='S') AND STAT<>'DEL' " ;
        private string strWhereSingleArrivals = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TIMEFROM') OR (TIFA BETWEEN '@@FROM' AND '@@TIMEFROM')) AND (ADID='A' AND RTYP='S' AND ONBL != ' ')";
		private string strNatWhere			= " AND (TTYP='@@NAT')";
		private string strORGDESWhere	= " AND (ORG3='@@APT' OR DES3='@@APT')";
		
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string stractLogicalFields = "ARRIVAL,STA,COUNT,COUNT_A";

		/// The header columns, which must be used for the report output
		/// </summary>
		/// <summary>
		private string stractTabHeader = "Airlines,Date,Overnight flights,Arrival Flights";
		
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string stractTabHeaderLens = "60,80,100,100";

		/// <summary>
		/// The logical field names of the tab control for internal development purposes for without arrival
		/// </summary>
		private string strLogicalFieldsWithoutArrival = "ARRIVAL,STA,COUNT";
		
		/// <summary>
		/// The header columns, which must be used for the report output for without arrival
		/// </summary>
		private string strTabHeaderWithoutArrival = "Airlines,Date,Overnight flights";
	
		/// <summary>
		/// The lengths, which must be used for the report's column widths for without arrival
		/// </summary>
		private string strTabHeaderLensWithoutArrival = "60,80,100";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the nature codes of NATTAB
		/// </summary>
		private ITable myNAT;
		/// <summary>
		/// The nature code as global valid member.
		/// </summary>
		private string strNatureCode = "";
		/// <summary>
		/// Airline 2-3 letter code
		/// </summary>
		private string strAirportCode = "";
		/// <summary>
		/// Different variables to be used for counts
		/// </summary>
		private int [] NumberofFlights;
		StringBuilder sb = new StringBuilder(10000);
		private string strEmptyLine = ",,";

		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;

		private class OverNightFlightSummaryRowData
		{
			public string strAirline = "";
			public string strDate = "";
			public int iTotalNoOfRotationFlights = 0;
			public int iTotalNoOfArrivalFlights = 0;
		}
		//End here
		
		#endregion _My Members

		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.TextBox txtAirport;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cbNatures;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.DateTimePicker dtDuration;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtLens;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox checkBoxTIFATIFD;
		private System.Windows.Forms.CheckBox checkBoxSTASTD;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.DateTimePicker timeTo;
		private System.Windows.Forms.DateTimePicker timeFrom;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.CheckBox checkBoxSingleArrivals;
		private System.Windows.Forms.Button buttonHelp;		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmOverNight_FlightSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmOverNight_FlightSummary));
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop = new System.Windows.Forms.Panel();
			this.checkBoxTIFATIFD = new System.Windows.Forms.CheckBox();
			this.checkBoxSTASTD = new System.Windows.Forms.CheckBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.timeTo = new System.Windows.Forms.DateTimePicker();
			this.timeFrom = new System.Windows.Forms.DateTimePicker();
			this.button1 = new System.Windows.Forms.Button();
			this.txtLens = new System.Windows.Forms.TextBox();
			this.dtDuration = new System.Windows.Forms.DateTimePicker();
			this.label5 = new System.Windows.Forms.Label();
			this.txtAirport = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.cbNatures = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.checkBoxSingleArrivals = new System.Windows.Forms.CheckBox();
			this.buttonHelp = new System.Windows.Forms.Button();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.panelTop.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 240);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(780, 282);
			this.panelBody.TabIndex = 3;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(780, 266);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(780, 266);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(780, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.buttonHelp);
			this.panelTop.Controls.Add(this.checkBoxTIFATIFD);
			this.panelTop.Controls.Add(this.checkBoxSTASTD);
			this.panelTop.Controls.Add(this.label7);
			this.panelTop.Controls.Add(this.label6);
			this.panelTop.Controls.Add(this.timeTo);
			this.panelTop.Controls.Add(this.timeFrom);
			this.panelTop.Controls.Add(this.button1);
			this.panelTop.Controls.Add(this.txtLens);
			this.panelTop.Controls.Add(this.dtDuration);
			this.panelTop.Controls.Add(this.label5);
			this.panelTop.Controls.Add(this.txtAirport);
			this.panelTop.Controls.Add(this.label4);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.cbNatures);
			this.panelTop.Controls.Add(this.label3);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Controls.Add(this.groupBox1);
			this.panelTop.Controls.Add(this.groupBox2);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(780, 240);
			this.panelTop.TabIndex = 2;
			// 
			// checkBoxTIFATIFD
			// 
			this.checkBoxTIFATIFD.Location = new System.Drawing.Point(480, 156);
			this.checkBoxTIFATIFD.Name = "checkBoxTIFATIFD";
			this.checkBoxTIFATIFD.Size = new System.Drawing.Size(72, 24);
			this.checkBoxTIFATIFD.TabIndex = 34;
			this.checkBoxTIFATIFD.Text = "ATA/ATD";
			// 
			// checkBoxSTASTD
			// 
			this.checkBoxSTASTD.Checked = true;
			this.checkBoxSTASTD.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxSTASTD.Location = new System.Drawing.Point(400, 156);
			this.checkBoxSTASTD.Name = "checkBoxSTASTD";
			this.checkBoxSTASTD.Size = new System.Drawing.Size(72, 24);
			this.checkBoxSTASTD.TabIndex = 33;
			this.checkBoxSTASTD.Text = "STA/STD";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(28, 160);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(64, 16);
			this.label7.TabIndex = 32;
			this.label7.Text = "Time from:";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(232, 160);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(24, 16);
			this.label6.TabIndex = 31;
			this.label6.Text = "to:";
			// 
			// timeTo
			// 
			this.timeTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.timeTo.Location = new System.Drawing.Point(260, 156);
			this.timeTo.Name = "timeTo";
			this.timeTo.ShowUpDown = true;
			this.timeTo.Size = new System.Drawing.Size(128, 20);
			this.timeTo.TabIndex = 30;
			// 
			// timeFrom
			// 
			this.timeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.timeFrom.Location = new System.Drawing.Point(100, 156);
			this.timeFrom.Name = "timeFrom";
			this.timeFrom.ShowUpDown = true;
			this.timeFrom.Size = new System.Drawing.Size(128, 20);
			this.timeFrom.TabIndex = 29;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(224)), ((System.Byte)(192)));
			this.button1.Location = new System.Drawing.Point(456, 12);
			this.button1.Name = "button1";
			this.button1.TabIndex = 20;
			this.button1.Text = "button1";
			this.button1.Visible = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtLens
			// 
			this.txtLens.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(224)), ((System.Byte)(192)));
			this.txtLens.Location = new System.Drawing.Point(276, 36);
			this.txtLens.Name = "txtLens";
			this.txtLens.Size = new System.Drawing.Size(488, 20);
			this.txtLens.TabIndex = 19;
			this.txtLens.Text = "textBox1";
			this.txtLens.Visible = false;
			// 
			// dtDuration
			// 
			this.dtDuration.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtDuration.Location = new System.Drawing.Point(708, 64);
			this.dtDuration.Name = "dtDuration";
			this.dtDuration.ShowUpDown = true;
			this.dtDuration.Size = new System.Drawing.Size(128, 20);
			this.dtDuration.TabIndex = 2;
			this.dtDuration.Visible = false;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(640, 68);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 16);
			this.label5.TabIndex = 18;
			this.label5.Text = "Duration >=";
			this.label5.Visible = false;
			// 
			// txtAirport
			// 
			this.txtAirport.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtAirport.Location = new System.Drawing.Point(100, 96);
			this.txtAirport.Name = "txtAirport";
			this.txtAirport.TabIndex = 5;
			this.txtAirport.Text = "";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(28, 100);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(64, 16);
			this.label4.TabIndex = 17;
			this.label4.Text = "Airport:";
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(412, 28);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 12;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(500, 200);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 10;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(252, 200);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 8;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(176, 200);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 7;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(328, 200);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 9;
			this.btnCancel.Text = "&Close";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(100, 200);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 6;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// cbNatures
			// 
			this.cbNatures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbNatures.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbNatures.ItemHeight = 16;
			this.cbNatures.Location = new System.Drawing.Point(100, 64);
			this.cbNatures.MaxDropDownItems = 16;
			this.cbNatures.Name = "cbNatures";
			this.cbNatures.Size = new System.Drawing.Size(288, 24);
			this.cbNatures.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.label3.Location = new System.Drawing.Point(28, 68);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 16);
			this.label3.TabIndex = 4;
			this.label3.Text = "Nature:";
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(260, 32);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 36);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(100, 32);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(28, 36);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			// 
			// groupBox1
			// 
			this.groupBox1.Location = new System.Drawing.Point(16, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(756, 116);
			this.groupBox1.TabIndex = 21;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Search Flights";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.checkBoxSingleArrivals);
			this.groupBox2.Location = new System.Drawing.Point(16, 132);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(756, 56);
			this.groupBox2.TabIndex = 35;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Timeframe that define an overnight flight:";
			// 
			// checkBoxSingleArrivals
			// 
			this.checkBoxSingleArrivals.Location = new System.Drawing.Point(548, 24);
			this.checkBoxSingleArrivals.Name = "checkBoxSingleArrivals";
			this.checkBoxSingleArrivals.Size = new System.Drawing.Size(124, 24);
			this.checkBoxSingleArrivals.TabIndex = 1;
			this.checkBoxSingleArrivals.Text = "With Single Arrivals";
			this.checkBoxSingleArrivals.CheckedChanged += new System.EventHandler(this.checkBoxSingleArrivals_CheckedChanged);
			// 
			// buttonHelp
			// 
			this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonHelp.Location = new System.Drawing.Point(404, 200);
			this.buttonHelp.Name = "buttonHelp";
			this.buttonHelp.TabIndex = 36;
			this.buttonHelp.Text = "Help";
			this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
			// 
			// frmOverNight_FlightSummary
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(780, 522);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Name = "frmOverNight_FlightSummary";
			this.Text = "Overnight Flights (Summary)";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmOverNight_FlightSummary_Closing);
			this.Load += new System.EventHandler(this.frmOverNight_FlightSummary_Load);
			this.HelpRequested += new HelpEventHandler(frmOverNight_FlightSummary_HelpRequested);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.panelTop.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
		
		/// <summary>
		/// Loads the from and inits the necessary things.
		/// </summary>
		/// <param name="sender">Originator of the method call</param>
		/// <param name="e">Event args</param>
		private void frmOverNight_FlightSummary_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
			PrepareFilterData();
			checkBoxSingleArrivals.Checked = false;
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			DateTime olDura;
			
			olFrom = DateTime.Now;			
			olTo = DateTime.Now;
			olFrom = olFrom.AddDays(-1);

			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olTo.Year, olTo.Month, olTo.Day, 23,59,0);
			olDura = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 3,0,0);
			
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtDuration.Value = olDura;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtDuration.CustomFormat = "HH:mm";
			
			timeFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			timeTo.CustomFormat = "dd.MM.yyyy - HH:mm";

			olTo   = new DateTime(olTo.Year, olTo.Month, olTo.Day, 02,00,0);
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,0,0);

			timeFrom.Value = olFrom;
			timeTo.Value = olTo;
		}
		/// <summary>
		/// Initializes the tab control.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString = stractTabHeader;
			tabResult.LogicalFieldList = stractLogicalFields;
			tabResult.HeaderLengthString = stractTabHeaderLens;

			if(this.checkBoxSingleArrivals.Checked == false)
			{				
				tabResult.HeaderString = strTabHeaderWithoutArrival;
				tabResult.LogicalFieldList = strLogicalFieldsWithoutArrival;
				tabResult.HeaderLengthString = strTabHeaderLensWithoutArrival;
			}
			else
			{
				tabResult.HeaderString = stractTabHeader;
				tabResult.LogicalFieldList = stractLogicalFields;
				tabResult.HeaderLengthString = stractTabHeaderLens;
			}
			PrepareReportDataRotations();
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
			myNAT = myDB["NAT"];
			cbNatures.Items.Add("<None>");
			if(myNAT == null)
			{
				myNAT = myDB.Bind("NAT", "NAT", "TTYP,TNAM", "5,30", "TTYP,TNAM");
				myNAT.Load("ORDER BY TTYP");
			}
			else if(myNAT.Count == 0)
			{
				myNAT = myDB.Bind("NAT", "NAT", "TTYP,TNAM", "5,30", "TTYP,TNAM");
				myNAT.Load("ORDER BY TTYP");
			}
			for(int i = 0; i < myNAT.Count; i++)
			{
				string strValue = myNAT[i]["TTYP"] + " / " + myNAT[i]["TNAM"];
				cbNatures.Items.Add(strValue);
			}
			if(cbNatures.Items.Count > 0)
				cbNatures.SelectedIndex = 0;
		}

		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent();			
			LoadReportData();
			PrepareReportDataRotations();
			this.Cursor = Cursors.Arrow;
			txtLens.Text = tabResult.HeaderLengthString;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			if(cbNatures.Text == "")
			{
				strRet += ilErrorCount.ToString() +  ". Please select a nature code!\n";
				ilErrorCount++;
			}
			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere = strWhereRaw;			
			tabResult.ResetContent();
			// In the first step change the times to UTC if this is necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			// Extract the true Nature code from the selected entry of the combobox
			strNatureCode = "";
			string [] strArr = cbNatures.Text.Split('/');
			strNatureCode = strArr[0].Trim();
			strAirportCode = txtAirport.Text;

			//Now reading day by day the flight records in order to 
			//not stress the database too much
			myDB.Unbind("AFT");
			// 10 , 10 , 9  ,  8 , 1  , 1  , 14 , 1  ,  5 , 1  , 2  , 3  , 4  , 3  , 4  , 2  , 5  , 5  , 5  , 5  , 5  , 1  , 3  , 14 , 1  , 3  , 4  , 5  ,  5 , 5  , 5  , 5
			//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT
			myAFT = myDB.Bind("AFT", "AFT", 
				"ALC2,RKEY,URNO,ADID,FLNO,CSGN,FLTI,JCNT,STOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT,RTYP,ONBL,OFBL", 
				"2,10,10,1,9,8,1,1,14,14,1,5,1,2,3,4,3,4,2,5,5,5,5,5,1,3,14,14,1,3,4,5,5,5,5,5,1,14,14",
				"ALC2,RKEY,URNO,ADID,FLNO,CSGN,FLTI,JCNT,STOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT,RTYP,ONBL,OFBL");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				
				//patch the where statement according to the user's entry
				strTmpWhere = strWhereRaw;
				if(strNatureCode != "<None>")
					strTmpWhere += strNatWhere;
				if(strAirportCode != "" )
					strTmpWhere += strORGDESWhere;
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				strTmpWhere = strTmpWhere.Replace("@@NAT", strNatureCode );
				strTmpWhere = strTmpWhere.Replace("@@APT", strAirportCode);
				strTmpWhere += "[ROTATIONS]";
				
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);				
				myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;				
				//New function added
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);

			if(this.checkBoxSingleArrivals.Checked == true)
			{				
				string strtimeFromDate = "";
				if(UT.IsTimeInUtc == false)
				{					
					strtimeFromDate = UT.DateTimeToCeda(UT.LocalToUtc(timeFrom.Value));
					strDateFrom = UT.DateTimeToCeda(UT.LocalToUtc(dtFrom.Value));
				}
				else
				{
					strtimeFromDate = UT.DateTimeToCeda(timeFrom.Value);
					strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				}				
				strTmpWhere = strWhereSingleArrivals;
				if(strNatureCode != "<None>")
					strTmpWhere += strNatWhere;
				if(strAirportCode != "" )
					strTmpWhere += strORGDESWhere;
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TIMEFROM", strtimeFromDate );
				strTmpWhere = strTmpWhere.Replace("@@NAT", strNatureCode );
				strTmpWhere = strTmpWhere.Replace("@@APT", strAirportCode);				
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
			}

			lblProgress.Text = "";
			progressBar1.Hide();

			//Filter out the FTYPs which are selected due to rotations
			for(int i = myAFT.Count-1; i >= 0; i--)
			{
				switch(myAFT[i]["FTYP"])
				{
					case "T":
					case "G":
					case "N":
					case "X":
						myAFT.Remove(i);
						break;
				}
			}
		}

		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// First organize the rotations and then put the result
		/// into the tabResult
		/// </summary>
		private void PrepareReportDataRotations()
		{
			NumberofFlights = new int[5];
			if(myAFT == null)
				return;

			myAFT.Sort("RKEY", true);
			int llCurr = 0;
			int ilStep = 1;
			string strCurrAdid = "";
			string strNextAdid = "";
			string strCurrRkey = "";
			string strNextRkey = "";
			string strTime = "";
			DateTime tmpDate;
			StringBuilder sb = new StringBuilder(10000);
			System.Collections.Hashtable hashOvernightFlightData = new Hashtable();
			while(llCurr < myAFT.Count)
			{
				string strValues = "";
				strCurrRkey = myAFT[llCurr]["RKEY"];
				strCurrAdid = myAFT[llCurr]["ADID"];
				if ((llCurr + 1) < myAFT.Count)
				{
					strNextRkey = myAFT[llCurr+1]["RKEY"];
					strNextAdid = myAFT[llCurr+1]["ADID"];
				}
				else
				{
					strNextRkey = "";
					strNextAdid = "";
				}

				if(strCurrRkey == strNextRkey)
				{//Is a rotation
					ilStep = 2;
					if( strCurrAdid == "A" && strNextAdid == "D")
					{
						DateTime timeSTOA = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOA"]);
						DateTime timeSTOD = UT.CedaFullDateToDateTime(myAFT[llCurr+1]["STOD"]);

						DateTime timeARR = new DateTime(1980,1,1,0,0,0) ;
						DateTime timeDEP = new DateTime(1980,1,1,0,0,0) ;
						
						strTime = myAFT[llCurr]["TIFA"];
						if(strTime != "")
							timeARR = UT.CedaFullDateToDateTime(strTime);
						strTime = myAFT[llCurr+1]["TIFD"];
						if(strTime != "")
							timeDEP = UT.CedaFullDateToDateTime(strTime);

						DateTime timeFromDate = new DateTime(timeFrom.Value.Year, timeFrom.Value.Month, timeFrom.Value.Day, timeFrom.Value.Hour, timeFrom.Value.Minute, 0 );
						DateTime timeToDate = new DateTime(timeTo.Value.Year, timeTo.Value.Month, timeTo.Value.Day, timeTo.Value.Hour, timeTo.Value.Minute, 0 );

						bool show = false;
						
						if(checkBoxSTASTD.Checked)
						{
							if(timeSTOA < timeFromDate && timeSTOD > timeToDate)
								show= true;				
						}
						
						if(checkBoxTIFATIFD.Checked)
						{
							if(timeARR < timeFromDate && timeDEP > timeToDate)
								show= true;				
						}

						if(show == true)
						{
							NumberofFlights[0]++;
							NumberofFlights[1]++;

							string strKey = myAFT[llCurr]["ALC2"] + (myAFT[llCurr]["STOA"].Trim().Length == 0 ? Helper.DateString(myAFT[llCurr]["TIFA"], "yyyyMMdd"):Helper.DateString(myAFT[llCurr]["STOA"], "yyyyMMdd"));
							OverNightFlightSummaryRowData overNightFlightSummaryRowData = (OverNightFlightSummaryRowData)hashOvernightFlightData[strKey];
							if(overNightFlightSummaryRowData == null)
							{
								overNightFlightSummaryRowData = new OverNightFlightSummaryRowData();								
								overNightFlightSummaryRowData.strAirline = myAFT[llCurr]["ALC2"];
								overNightFlightSummaryRowData.strDate = myAFT[llCurr]["STOA"].Trim().Length == 0 ? Helper.DateString(myAFT[llCurr]["TIFA"], "yyyyMMdd") : Helper.DateString(myAFT[llCurr]["STOA"], "yyyyMMdd");
								hashOvernightFlightData.Add(strKey,overNightFlightSummaryRowData);
							}
							overNightFlightSummaryRowData.iTotalNoOfRotationFlights++;							
						}						
					}
					if( strCurrAdid == "D" && strNextAdid == "A")
					{
						DateTime timeSTOA = UT.CedaFullDateToDateTime(myAFT[llCurr+1]["STOA"]);
						DateTime timeSTOD = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOD"]);

						DateTime timeARR = new DateTime(1980,1,1,0,0,0) ;
						DateTime timeDEP = new DateTime(1980,1,1,0,0,0) ;

						strTime = myAFT[llCurr+1]["TIFA"];
						if(strTime != "")
							timeARR = UT.CedaFullDateToDateTime(strTime);
						strTime = myAFT[llCurr]["TIFD"];
						if(strTime != "")
							timeDEP = UT.CedaFullDateToDateTime(strTime);

						DateTime timeFromDate = new DateTime(timeFrom.Value.Year, timeFrom.Value.Month, timeFrom.Value.Day, timeFrom.Value.Hour, timeFrom.Value.Minute, 0 );
						DateTime timeToDate = new DateTime(timeTo.Value.Year, timeTo.Value.Month, timeTo.Value.Day, timeTo.Value.Hour, timeTo.Value.Minute, 0 );
						
						bool show = false;
						
						if(checkBoxSTASTD.Checked)
						{
							if(timeSTOA < timeFromDate && timeSTOD > timeToDate)
								show= true;				
						}
						
						if(checkBoxTIFATIFD.Checked)
						{
							if(timeARR < timeFromDate && timeDEP > timeToDate)
								show= true;				
						}

						if(show == true)
						{
							NumberofFlights[2]++;
							NumberofFlights[3]++;

							string strKey = myAFT[llCurr]["ALC2"] + (myAFT[llCurr+1]["STOA"].Trim().Length == 0 ? Helper.DateString(myAFT[llCurr+1]["TIFA"], "yyyyMMdd"):Helper.DateString(myAFT[llCurr+1]["STOA"], "yyyyMMdd"));
							OverNightFlightSummaryRowData overNightFlightSummaryRowData = (OverNightFlightSummaryRowData)hashOvernightFlightData[strKey];
							if(overNightFlightSummaryRowData == null)
							{
								overNightFlightSummaryRowData = new OverNightFlightSummaryRowData();
								overNightFlightSummaryRowData.strAirline = myAFT[llCurr]["ALC2"];
								overNightFlightSummaryRowData.strDate = myAFT[llCurr+1]["STOA"].Trim().Length == 0 ? Helper.DateString(myAFT[llCurr+1]["TIFA"], "yyyyMMdd") : Helper.DateString(myAFT[llCurr+1]["STOA"], "yyyyMMdd");
								hashOvernightFlightData.Add(strKey,overNightFlightSummaryRowData);
							}
							overNightFlightSummaryRowData.iTotalNoOfRotationFlights++;	
						}
					}
				}//if(strCurrRkey == strNextRkey)
				else
				{	
					if(this.checkBoxSingleArrivals.Checked == true)
					{
						DateTime timeSTOA = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOA"]);
						DateTime timeARR = new DateTime(1980,1,1,0,0,0) ;
						
						strTime = myAFT[llCurr]["TIFA"];
						if(strTime != "")
							timeARR = UT.CedaFullDateToDateTime(strTime);

						DateTime timeFromDate = new DateTime(timeFrom.Value.Year, timeFrom.Value.Month, timeFrom.Value.Day, timeFrom.Value.Hour, timeFrom.Value.Minute, 0 );
				
						bool show = false;
						if(checkBoxSTASTD.Checked)
						{
							if(timeSTOA < timeFromDate)
								show= true;				
						}
						
						if(checkBoxTIFATIFD.Checked)
						{
							if(timeARR < timeFromDate)
								show= true;				
						}

						if(myAFT[llCurr]["ADID"] != "A" || myAFT[llCurr]["RTYP"] != "S" || myAFT[llCurr]["ONBL"].Trim().Length == 0)
						{
							show = false;
						}

						if(show)
						{
							NumberofFlights[4]++;
							string strKey = myAFT[llCurr]["ALC2"] + (myAFT[llCurr]["STOA"].Trim().Length == 0 ? Helper.DateString(myAFT[llCurr]["TIFA"], "yyyyMMdd"):Helper.DateString(myAFT[llCurr]["STOA"], "yyyyMMdd"));
							OverNightFlightSummaryRowData overNightFlightSummaryRowData = (OverNightFlightSummaryRowData)hashOvernightFlightData[strKey];
							if(overNightFlightSummaryRowData == null)
							{
								overNightFlightSummaryRowData = new OverNightFlightSummaryRowData();
								overNightFlightSummaryRowData.strAirline = myAFT[llCurr]["ALC2"];
								overNightFlightSummaryRowData.strDate = myAFT[llCurr]["STOA"].Trim().Length == 0 ? Helper.DateString(myAFT[llCurr]["TIFA"], "yyyyMMdd") : Helper.DateString(myAFT[llCurr]["STOA"], "yyyyMMdd");								
								hashOvernightFlightData.Add(strKey,overNightFlightSummaryRowData);
							}
							overNightFlightSummaryRowData.iTotalNoOfArrivalFlights++;
						}
					}
					ilStep = 1;
				}
				llCurr = llCurr + ilStep;
			}//while(llCurr < myAFT.Count)

			IDictionaryEnumerator iDictEnumerator = hashOvernightFlightData.GetEnumerator();
			
			while(iDictEnumerator.MoveNext() == true)
			{
				string strValues = "";
				strValues += ((OverNightFlightSummaryRowData)iDictEnumerator.Value).strAirline + ",";
				strValues += ((OverNightFlightSummaryRowData)iDictEnumerator.Value).strDate + ",";
				strValues += ((OverNightFlightSummaryRowData)iDictEnumerator.Value).iTotalNoOfRotationFlights.ToString() + ",";
				if(this.checkBoxSingleArrivals.Checked == true)
				{
					strValues += ((OverNightFlightSummaryRowData)iDictEnumerator.Value).iTotalNoOfArrivalFlights.ToString() + "\n";
				}
				else
				{
					strValues += "\n";
				}
				sb.Append(strValues);
			}

			tabResult.InsertBuffer(sb.ToString(), "\n");
			lblProgress.Text = "";
			progressBar1.Visible = false;
			tabResult.Sort("0", true, true);			
			tabResult.Refresh();			
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
	
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			int ilTotalNoOfFlights = NumberofFlights[0] +NumberofFlights[2] + NumberofFlights[1] +NumberofFlights[3];
			if(this.checkBoxSingleArrivals.Checked == true)
			{
				ilTotalNoOfFlights += (int)NumberofFlights[4];
			}
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + ilTotalNoOfFlights.ToString();
			strSubHeader += " "+" ARR: " + (NumberofFlights[0] +NumberofFlights[2] +NumberofFlights[4]).ToString();
			strSubHeader += " "+" DEP: " + (NumberofFlights[1] +NumberofFlights[3]).ToString() + ")";			
			strReportHeader = frmMain.strPrefixVal + "Overnight Flights (Summary): NAT: " + strNatureCode +  " "+" APT: " + strAirportCode + " "+ " Min G/T: " + timeFrom.Value.ToString("HH:mm") + " to " + timeTo.Value.ToString("HH:mm");
			strReportSubHeader = strSubHeader;
			rptFIPS rpt = new rptFIPS(tabResult,strReportHeader, strSubHeader, "", 10);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			if(Helper.UseSpreadBuilder() == true)
			{
				activeReport = rpt;
				frm.UseSpreadBuilder(true);
				frm.GetBtnExcelExport().Click += new EventHandler(frmOverNight_FlightSummary_Click);
			}
			else
			{
				frm.UseSpreadBuilder(false);
			}
			frm.Show();
		}

		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();		
		}

		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent();			
			LoadReportData();
			PrepareReportDataRotations();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}

		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmOverNight_FlightSummary_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("NAT");
			myDB.Unbind("AFT");
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			txtLens.Text = tabResult.HeaderLengthString;
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			myDB.Unbind("NAT");
			myDB.Unbind("AFT");
			this.Close ();
		}

		private void checkBoxSingleArrivals_CheckedChanged(object sender, System.EventArgs e)
		{
			if(tabResult.GetLineCount() > 0)
			{
				InitTab();
				LoadReportData();
				PrepareReportDataRotations();
			}
			else
			{
				InitTab();
			}
		}

		private void InitializeMouseEvents()
		{
			((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmOverNight_FlightSummary_MouseWheel);
			((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmOverNight_FlightSummary_MouseEnter);
			((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmOverNight_FlightSummary_MouseLeave);
		}

		private void frmOverNight_FlightSummary_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && tabResult.GetVScrollPos() > 0)
			{				
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
			{
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
			}
		}

		private void frmOverNight_FlightSummary_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmOverNight_FlightSummary_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmOverNight_FlightSummary_Click(object sender, EventArgs e)
		{
			if(this.checkBoxSingleArrivals.Checked == false)
			{
				Helper.ExportToExcel(tabResult,strReportHeader,strReportSubHeader,strTabHeaderLensWithoutArrival,10,0,activeReport);
			}
			else
			{
				Helper.ExportToExcel(tabResult,strReportHeader,strReportSubHeader,stractTabHeaderLens,10,0,activeReport);
			}
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmOverNight_FlightSummary_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}
	}
}