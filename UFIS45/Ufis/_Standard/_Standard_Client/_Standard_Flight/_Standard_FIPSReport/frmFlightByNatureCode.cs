using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmFlightByNatureCode.
	/// </summary>
	public class frmFlightByNatureCode : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		
//		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO' ) OR ( STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO')) AND (FTYP='O' OR FTYP='S' OR FTYP='X')) AND STAT<>'DEL'";
//		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO') AND (STOD BETWEEN '@@FROM' AND '@@TO'))) AND (FTYP='O' OR FTYP='S' OR FTYP='X') AND STAT<>'DEL' " ;
		//private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B')))) OR " +
		//								 "((TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO'AND (ADID IN ('D','B'))))) AND (FTYP='O' OR FTYP='S' OR FTYP='X') AND STAT<>'DEL' " ;
		private string strWhereRaw = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO') OR (STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO')) AND (FTYP='O' OR FTYP='S')";
		private string strNatWhere			= " AND (TTYP='@@NAT')";
		private string strAirlineWhere		= " AND (ALC2='@@ALC' OR ALC3='@@ALC')";
		private string strACWhere			= " AND (ACT3='@@ACT' OR ACT5='@@ACT')";
		private string strOriginWhere		= " AND (ORG3='@@ORIG')";
		private string strDestinationWhere	= " AND (DES3='@@DEST')";
		private string strORGDESWhere	= " AND (ORG3='@@ORIG' OR DES3='@@DEST')";
		private string strRegistrationWhere = " AND (REGN='@@REGN')";
		private string strRegOwnerWhere		= " AND REGN IN (@@OWNE)";	
		private string strTerminalWhere     = " AND (STEV = '@@STEV')";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "FLNO,ADID,ORGDES,VIAA,SCHEDULE,ACTUAL,ACTYPE,ONOFBL,TTYP,REGN,RWY,TERM";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes ==> for Rotation
		/// </summary>
		private string strLogicalFieldsRotation = "SORT,FLNO_A,ORG,VIA_A,SCHED_A,ACTUAL_A,ACT_A,ONBL,TTYP_A,REGN_A,RWY_A,TERM_A,FLNO_D,DES,VIA_D,SCHED_D,ACT_D,ACTUAL_D,OFBL,TTYP_D,REGN_D,RWY_D,TERM_D";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		/// <summary>
		private string strTabHeader =     "Flight  ,A/D  ,Org/Des ,Via  ,Schedule  ,Actual  ,A/C  , On/Ofbl  ,Na  ,Reg,RWY, Term";
		/// The header columns, which must be used for the report output
		/// for rotation
		/// </summary>
		private string strTabHeaderRotation = "Sort,Arrival  ,Org  ,Via ,Schedule  ,Actual  ,A/C  , Onbl  ,Na  ,Reg,Rwy,Term,Depart.  ,Des  ,Via ,Schedule  ,Actual  ,A/C  , Ofbl  ,Na  ,Reg,Rwy, Term";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "80,30,50,50,70,70,40,60,40,70,30,20";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// for rotation
		/// </summary>
		private string strTabHeaderLensRotation = "0,55,35,35,60,60,30,55,30,55,30,15,55,35,35,60,60,30,55,30,55,30,15";
		/// <summary>
		/// The lengths, which must be used for the report's excel export column widths
		/// </summary>
		private string strExcelTabHeaderLens = "80,30,50,50,130,130,40,130,40,70,30,20";
		/// <summary>
		/// The lengths, which must be used for the report's excel export column widths
		/// for rotation
		/// </summary>
		private string strExcelTabHeaderLensRotation = "0,65,50,50,130,130,40,130,40,65,30,20,65,50,50,130,130,40,130,40,65,30,20";
		
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the nature codes of NATTAB
		/// </summary>
		private ITable myNAT;
		/// <summary>
		/// ITable object for the owner codes of ACRTAB
		/// </summary>
		private ITable myACR;
		/// <summary>
		/// The nature code as global valid member.
		/// </summary>
		private string strNatureCode = "";
		/// <summary>
		/// Airline 2-3 letter code
		/// </summary>
		private string strAirlineCode = "";
		/// <summary>
		/// ACT3 or ACT5
		/// </summary>
		private string strACTypeCode = "";
		/// <summary>
		/// Airport code for origin
		/// </summary>
		private string strOriginCode = "";
		/// <summary>
		/// Airport code for destination
		/// </summary>
		private string strDestinationCode = "";
		/// <summary>
		/// Registration
		/// </summary>
		private string strRegistrationCode = "";
		/// <summary>
		/// Registration owner code
		/// </summary>
		private string strRegOwnerCode = "";
		/// <summary>
		/// Terminal value
		/// </summary>
		private string strTerminal = "";
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		/// <summary>
		/// Datetime format for excel export
		/// </summary>
		private string strExcelDateTimeFormat = "";
		/// <summary>
		/// Datetime format for report
		/// </summary>
		private string strReportDateTimeFormat = "dd'/'HH:mm";
		/// <summary>
		/// Datetime format for table
		/// </summary>
		private string strDateDisplayFormat = "";
		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeaderRotation = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;
		/// <summary>
		/// Boolean for showing stev
		/// </summary>
		private bool bmShowStev = false;

		#endregion _My Members

		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private AxTABLib.AxTAB tabExcelResult;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ComboBox cbNatures;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.TextBox txtAirline;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtACT;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtORIG;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtDEST;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.CheckBox cbRotations;
		private System.Windows.Forms.TextBox txtREGN;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox txtRegOwner;
		private System.Windows.Forms.Button buttonHelp;
		private System.Windows.Forms.TextBox txtTerminal;
		private System.Windows.Forms.Label lblTerminal;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmFlightByNatureCode()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmFlightByNatureCode));
			this.panelTop = new System.Windows.Forms.Panel();
			this.txtTerminal = new System.Windows.Forms.TextBox();
			this.lblTerminal = new System.Windows.Forms.Label();
			this.buttonHelp = new System.Windows.Forms.Button();
			this.txtRegOwner = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.txtREGN = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.cbRotations = new System.Windows.Forms.CheckBox();
			this.txtDEST = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.txtORIG = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.txtACT = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.txtAirline = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.cbNatures = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.tabExcelResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop.SuspendLayout();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.txtTerminal);
			this.panelTop.Controls.Add(this.lblTerminal);
			this.panelTop.Controls.Add(this.buttonHelp);
			this.panelTop.Controls.Add(this.txtRegOwner);
			this.panelTop.Controls.Add(this.label9);
			this.panelTop.Controls.Add(this.txtREGN);
			this.panelTop.Controls.Add(this.label8);
			this.panelTop.Controls.Add(this.cbRotations);
			this.panelTop.Controls.Add(this.txtDEST);
			this.panelTop.Controls.Add(this.label7);
			this.panelTop.Controls.Add(this.txtORIG);
			this.panelTop.Controls.Add(this.label6);
			this.panelTop.Controls.Add(this.txtACT);
			this.panelTop.Controls.Add(this.label5);
			this.panelTop.Controls.Add(this.txtAirline);
			this.panelTop.Controls.Add(this.label4);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.cbNatures);
			this.panelTop.Controls.Add(this.label3);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(752, 252);
			this.panelTop.TabIndex = 0;
			// 
			// txtTerminal
			// 
			this.txtTerminal.Location = new System.Drawing.Point(292, 144);
			this.txtTerminal.Name = "txtTerminal";
			this.txtTerminal.TabIndex = 9;
			this.txtTerminal.Text = "";
			// 
			// lblTerminal
			// 
			this.lblTerminal.Location = new System.Drawing.Point(236, 148);
			this.lblTerminal.Name = "lblTerminal";
			this.lblTerminal.Size = new System.Drawing.Size(52, 16);
			this.lblTerminal.TabIndex = 31;
			this.lblTerminal.Text = "Term:";
			// 
			// buttonHelp
			// 
			this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonHelp.Location = new System.Drawing.Point(396, 216);
			this.buttonHelp.Name = "buttonHelp";
			this.buttonHelp.TabIndex = 15;
			this.buttonHelp.Text = "Help";
			this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
			// 
			// txtRegOwner
			// 
			this.txtRegOwner.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtRegOwner.Location = new System.Drawing.Point(292, 172);
			this.txtRegOwner.Name = "txtRegOwner";
			this.txtRegOwner.TabIndex = 10;
			this.txtRegOwner.Text = "";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(236, 176);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(48, 16);
			this.label9.TabIndex = 27;
			this.label9.Text = "Owner:";
			this.label9.Click += new System.EventHandler(this.label9_Click);
			// 
			// txtREGN
			// 
			this.txtREGN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtREGN.Location = new System.Drawing.Point(92, 172);
			this.txtREGN.Name = "txtREGN";
			this.txtREGN.TabIndex = 8;
			this.txtREGN.Text = "";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(12, 176);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(72, 16);
			this.label8.TabIndex = 26;
			this.label8.Text = "Registration:";
			// 
			// cbRotations
			// 
			this.cbRotations.Location = new System.Drawing.Point(292, 60);
			this.cbRotations.Name = "cbRotations";
			this.cbRotations.TabIndex = 4;
			this.cbRotations.Text = "Rotation View";
			this.cbRotations.CheckedChanged += new System.EventHandler(this.cbRotations_CheckedChanged);
			// 
			// txtDEST
			// 
			this.txtDEST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtDEST.Location = new System.Drawing.Point(92, 144);
			this.txtDEST.Name = "txtDEST";
			this.txtDEST.TabIndex = 7;
			this.txtDEST.Text = "";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(12, 148);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(72, 16);
			this.label7.TabIndex = 23;
			this.label7.Text = "Destination:";
			// 
			// txtORIG
			// 
			this.txtORIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtORIG.Location = new System.Drawing.Point(92, 116);
			this.txtORIG.Name = "txtORIG";
			this.txtORIG.TabIndex = 6;
			this.txtORIG.Text = "";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(12, 120);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(72, 16);
			this.label6.TabIndex = 21;
			this.label6.Text = "Origin:";
			// 
			// txtACT
			// 
			this.txtACT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtACT.Location = new System.Drawing.Point(92, 88);
			this.txtACT.Name = "txtACT";
			this.txtACT.TabIndex = 5;
			this.txtACT.Text = "";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(12, 92);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 16);
			this.label5.TabIndex = 19;
			this.label5.Text = "Aircraft Type:";
			// 
			// txtAirline
			// 
			this.txtAirline.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtAirline.Location = new System.Drawing.Point(92, 60);
			this.txtAirline.Name = "txtAirline";
			this.txtAirline.TabIndex = 3;
			this.txtAirline.Text = "";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(12, 64);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(72, 16);
			this.label4.TabIndex = 17;
			this.label4.Text = "Airline:";
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(480, 200);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 12;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(480, 216);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 13;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 216);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 13;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 216);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 12;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 216);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 14;
			this.btnCancel.Text = "&Close";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 216);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 11;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// cbNatures
			// 
			this.cbNatures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbNatures.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbNatures.Location = new System.Drawing.Point(92, 32);
			this.cbNatures.MaxDropDownItems = 16;
			this.cbNatures.Name = "cbNatures";
			this.cbNatures.Size = new System.Drawing.Size(304, 24);
			this.cbNatures.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(12, 36);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 16);
			this.label3.TabIndex = 4;
			this.label3.Text = "Nature:";
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(268, 4);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(92, 4);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 252);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(752, 182);
			this.panelBody.TabIndex = 1;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Controls.Add(this.tabExcelResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(752, 166);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(752, 166);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// tabExcelResult
			// 
			this.tabExcelResult.ContainingControl = this;
			this.tabExcelResult.Location = new System.Drawing.Point(0, 0);
			this.tabExcelResult.Name = "tabExcelResult";
			this.tabExcelResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabExcelResult.OcxState")));
			this.tabExcelResult.Size = new System.Drawing.Size(100, 50);
			this.tabExcelResult.TabIndex = 1;
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(752, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// frmFlightByNatureCode
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(752, 434);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmFlightByNatureCode";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Flights By Natures/Aircraft/Airlines/Origin/Destinations/Reg.";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmFlightByNatureCode_Closing);
			this.Load += new System.EventHandler(this.frmFlightByNatureCode_Load);
			this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmFlightByNatureCode_HelpRequested);
			this.panelTop.ResumeLayout(false);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
		
		/// <summary>
		/// Load the form. Prepare and init all controls.
		/// </summary>
		/// <param name="sender">Form.</param>
		/// <param name="e">Event arguments.</param>
		private void frmFlightByNatureCode_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			SetupReportTabHeader();
			InitTab();
			PrepareFilterData();			
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}
		/// <summary>
		/// Initializes the tab controll.
		/// </summary>
		private void InitTab()
		{
			tabExcelResult.ResetContent();
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;

			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			strExcelDateTimeFormat = myIni.IniReadValue("FIPSREPORT", "DATEEXCELFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = strReportDateTimeFormat;
			}
			else
			{
				strReportDateTimeFormat = strDateDisplayFormat;
			}
			if(strExcelDateTimeFormat.Length == 0)
			{
				strExcelDateTimeFormat = strReportDateTimeFormat;
			}			

			if(cbRotations.Checked == false)
			{
				tabResult.HeaderString = strTabHeader;
				tabResult.LogicalFieldList = strLogicalFields;
				tabResult.HeaderLengthString = strTabHeaderLens;

				tabExcelResult.HeaderString = strTabHeader;
				tabExcelResult.LogicalFieldList = strLogicalFields;
				tabExcelResult.HeaderLengthString = strTabHeaderLens;
				PrepareReportData("Report");
				PrepareReportData("Excel");
			}
			else
			{
				tabResult.HeaderString = strTabHeaderRotation;
				tabResult.LogicalFieldList = strLogicalFieldsRotation;
				tabResult.HeaderLengthString = strTabHeaderLensRotation;

				tabExcelResult.HeaderString = strTabHeaderRotation;
				tabExcelResult.LogicalFieldList = strLogicalFieldsRotation;
				tabExcelResult.HeaderLengthString = strTabHeaderLensRotation;
				PrepareReportDataRotations("Report");
				PrepareReportDataRotations("Excel");
			}			
		}

		
		/// <summary>
		/// Initializes the tab control header data.
		/// </summary>
		private void SetupReportTabHeader()
		{			
			string[] strLabelHeaderEntry = new string[2];
			string strTerminalLabel = "";
			string strTerminalHeader = "";
			if(Helper.IsSTEVConfigured(this,ref strLabelHeaderEntry,true,true) == true)
			{
				bmShowStev = true;
				strTerminalLabel  = strLabelHeaderEntry[0];
				strTerminalHeader = strLabelHeaderEntry[1];				
				lblTerminal.Text = strTerminalLabel + ":";
				Helper.RemoveOrReplaceString(11,ref strTabHeader,',',strTerminalHeader,false);
				Helper.RemoveOrReplaceString(11,ref strTabHeaderRotation,',',strTerminalHeader,false);
				Helper.RemoveOrReplaceString(22,ref strTabHeaderRotation,',',strTerminalHeader,false);				
			}
			else
			{
				Helper.RemoveOrReplaceString(11,ref strLogicalFields,',',"",true);
				Helper.RemoveOrReplaceString(11,ref strLogicalFieldsRotation,',',"",true);
				Helper.RemoveOrReplaceString(21,ref strLogicalFieldsRotation,',',"",true);
				Helper.RemoveOrReplaceString(11,ref strTabHeader,',',"",true);
				Helper.RemoveOrReplaceString(11,ref strTabHeaderRotation,',',"",true);
				Helper.RemoveOrReplaceString(21,ref strTabHeaderRotation,',',"",true);
				Helper.RemoveOrReplaceString(11,ref strTabHeaderLens,',',"",true);
				Helper.RemoveOrReplaceString(11,ref strTabHeaderLensRotation,',',"",true);
				Helper.RemoveOrReplaceString(21,ref strTabHeaderLensRotation,',',"",true);
				Helper.RemoveOrReplaceString(11,ref strExcelTabHeaderLens,',',"",true);
				Helper.RemoveOrReplaceString(11,ref strExcelTabHeaderLensRotation,',',"",true);
				Helper.RemoveOrReplaceString(21,ref strExcelTabHeaderLensRotation,',',"",true);
				lblTerminal.Hide();
				txtTerminal.Hide();
			}
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
			myNAT = myDB["NAT"];
			cbNatures.Items.Add("<None>");
			if(myNAT == null)
			{
				myNAT = myDB.Bind("NAT", "NAT", "TTYP,TNAM", "5,30", "TTYP,TNAM");
				myNAT.Load("ORDER BY TTYP");
			}
			else if(myNAT.Count == 0)
			{
				myNAT = myDB.Bind("NAT", "NAT", "TTYP,TNAM", "5,30", "TTYP,TNAM");
				myNAT.Load("ORDER BY TTYP");
			}
			for(int i = 0; i < myNAT.Count; i++)
			{
				string strValue = myNAT[i]["TTYP"] + " | " + myNAT[i]["TNAM"];
				cbNatures.Items.Add(strValue);
			}
			if(cbNatures.Items.Count > 0)
				cbNatures.SelectedIndex = 0;

		}
		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			if(cbRotations.Checked == false)
			{
				PrepareReportData("Report");
				PrepareReportData("Excel");
			}
			else
			{
				PrepareReportDataRotations("Report");
				PrepareReportDataRotations("Excel");
			}
			//RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			if(cbNatures.Text == "")
			{
				strRet += ilErrorCount.ToString() +  ". Please select a nature code!\n";
				ilErrorCount++;
			}
			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere = strWhereRaw;
			//Clear the tab
			tabResult.ResetContent();
			tabExcelResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			// Extract the true Nature code from the selected entry of the combobox
			strNatureCode = "";
			string [] strArr = cbNatures.Text.Split('|');
			strNatureCode = strArr[0].Trim();
			strAirlineCode = txtAirline.Text;
			strRegistrationCode = txtREGN.Text;
			strACTypeCode = txtACT.Text;
			strOriginCode = txtORIG.Text;
			strDestinationCode = txtDEST.Text;
			strRegOwnerCode = txtRegOwner.Text;
			if(bmShowStev == true)
			{
				strTerminal = txtTerminal.Text.Trim();
			}

			string strOwnerRegCodes = "";
			//	read registration of specified owner if necessary
			if (strRegOwnerCode.Length > 0)
			{
				myDB.Unbind("ACR");
				myACR = myDB.Bind("ACR","ACR","REGN,OWNE","12,10","REGN,OWNE");
				string strWhere = string.Format("WHERE OWNE='{0}'",strRegOwnerCode);
				myACR.Load(strWhere);
				myACR.CreateIndex("REGN","REGN");
				StringBuilder sb = new StringBuilder(1024);
				for (int i = 0; i < myACR.Count; i++)
				{
					if (i > 0)
						sb.Append(',');
					sb.Append('\'');
					sb.Append(myACR[i]["REGN"]);
					sb.Append('\'');
				}

				if (myACR.Count > 0)
				{
					strOwnerRegCodes = sb.ToString();
				}
				else
				{
					strOwnerRegCodes = "";
				}
			}

			//Now reading day by day the flight records in order to 
			//not stress the database too much
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,RKEY,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,TTYP,ONBL,OFBL,REGN,VIA3,STEV,RWYA,RWYD", 
							  "10,10,12,1,3,3,14,14,14,14,3,5,14,14,12,3,1,4,4",
                              "URNO,RKEY,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,TTYP,ONBL,OFBL,REGN,VIA3,STEV,RWYA,RWYD");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpWhere = strWhereRaw;
				if(strNatureCode != "<None>")
					strTmpWhere += strNatWhere;
				if(strAirlineCode != "")
					strTmpWhere += strAirlineWhere;
				if(strRegistrationCode != "")
					strTmpWhere += strRegistrationWhere;
				if(strACTypeCode != "")
					strTmpWhere += strACWhere;
				if(strOriginCode != "" && strDestinationCode != "")
				{
					strTmpWhere += strORGDESWhere;
				}
				else
				{
					if(strOriginCode != "")
						strTmpWhere += strOriginWhere;
					if(strDestinationCode != "")
						strTmpWhere += strDestinationWhere;
				}

				if (strOwnerRegCodes != "")
				{
					strTmpWhere += strRegOwnerWhere;
				}

				if(strTerminal.Length != 0)
				{
					strTmpWhere += strTerminalWhere;
				}

				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				strTmpWhere = strTmpWhere.Replace("@@NAT", strNatureCode );
				strTmpWhere = strTmpWhere.Replace("@@ALC", strAirlineCode);
				strTmpWhere = strTmpWhere.Replace("@@ACT", strACTypeCode);
				strTmpWhere = strTmpWhere.Replace("@@ORIG", strOriginCode);
				strTmpWhere = strTmpWhere.Replace("@@DEST", strDestinationCode);
				strTmpWhere = strTmpWhere.Replace("@@REGN", strRegistrationCode);
				strTmpWhere = strTmpWhere.Replace("@@OWNE", strOwnerRegCodes);
				strTmpWhere = strTmpWhere.Replace("@@HOPO", UT.Hopo);
				strTmpWhere = strTmpWhere.Replace("@@STEV", strTerminal);
				//strTmpWhere += "[ROTATIONS]";
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom = datReadFrom.AddDays(1);//+=  new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			//Filter out the FTYPs which are selected due to rotations
			for(int i = myAFT.Count-1; i >= 0; i--)
			{
				if(myAFT[i]["FTYP"] == "T" || myAFT[i]["FTYP"] == "G" || 
					myAFT[i]["FTYP"] == "N" || myAFT[i]["FTYP"] == "B" ||  myAFT[i]["FTYP"] == "Z" )
				{
					myAFT.Remove(i);
				}
			}
			lblProgress.Text = "";
			progressBar1.Hide();
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

		}

		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// First organize the rotations and then put the result
		/// into the tabResult
		/// </summary>
		private void PrepareReportDataRotations(string strReportOrExcel)
		{
			imArrivals = 0;
			imDepartures =0 ;
			imRotations = 0;
			imTotalFlights = 0;

			if(myAFT == null)
				return;

			myAFT.Sort("RKEY", true);
			int llCurr = 0;
			int ilStep = 1;
			string strCurrAdid = "";
			string strNextAdid = "";
			string strCurrRkey = "";
			string strNextRkey = "";
			StringBuilder sb = new StringBuilder(10000);

			string strDateTimeFormat = strReportDateTimeFormat;
			if(strReportOrExcel.Equals("Excel") == true)
			{
				strDateTimeFormat = strExcelDateTimeFormat;
			}

			while(llCurr < myAFT.Count)
			{
				string strValues = "";
				strCurrRkey = myAFT[llCurr]["RKEY"];
				strCurrAdid = myAFT[llCurr]["ADID"];
				if ((llCurr + 1) < myAFT.Count)
				{
					strNextRkey = myAFT[llCurr+1]["RKEY"];
					strNextAdid = myAFT[llCurr+1]["ADID"];
				}
				else
				{
					strNextRkey = "";
					strNextAdid = "";
				}

				if(strCurrRkey == strNextRkey)
				{//Is a rotation
					++this.imRotations;
					++this.imArrivals;
					++this.imTotalFlights;
					++this.imDepartures;
					++this.imTotalFlights;

					ilStep = 2;
					if( strCurrAdid == "A" && strNextAdid == "D")
					{
						//The arrival part of rotation
						strValues += myAFT[llCurr]["STOA"] + myAFT[llCurr+1]["STOD"] + ",";
						strValues += myAFT[llCurr]["FLNO"] + ",";
						strValues += myAFT[llCurr]["ORG3"] + ",";
						//Changed for the PRF 8523
						strValues += myAFT[llCurr]["VIA3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["STOA"], strDateTimeFormat) + ",";
						strValues += Helper.DateString(myAFT[llCurr]["TIFA"], strDateTimeFormat) + ",";
						strValues += myAFT[llCurr]["ACT3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["ONBL"], strDateTimeFormat) + ",";
						strValues += myAFT[llCurr]["TTYP"] + ",";
						strValues += myAFT[llCurr]["REGN"] + ",";
                        strValues += myAFT[llCurr]["RWYA"] + ",";

						if(bmShowStev == true)
						{
							strValues += myAFT[llCurr]["STEV"] + ",";
						}
						//-----------------------------------The departure part of rotation
						strValues += myAFT[llCurr+1]["FLNO"] + ",";
						strValues += myAFT[llCurr+1]["DES3"] + ",";
						//Changed for the PRF 8523
						strValues += myAFT[llCurr+1]["VIA3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["STOD"], strDateTimeFormat) + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["TIFD"], strDateTimeFormat) + ",";
						strValues += myAFT[llCurr+1]["ACT3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["OFBL"], strDateTimeFormat) + ",";
						strValues += myAFT[llCurr+1]["TTYP"] + ",";
                        strValues += myAFT[llCurr + 1]["REGN"] + ",";
                        strValues += myAFT[llCurr + 1]["RWYD"];

                        if (bmShowStev == true)
						{
							strValues += "," + myAFT[llCurr+1]["STEV"];
						}						
						strValues += "\n";
						sb.Append(strValues);
					}
					if( strCurrAdid == "D" && strNextAdid == "A")
					{
						//The arrival part of rotation
						strValues += myAFT[llCurr+1]["STOA"] + myAFT[llCurr]["STOD"]  + ",";
						strValues += myAFT[llCurr+1]["FLNO"] + ",";
						strValues += myAFT[llCurr+1]["ORG3"] + ",";
						//Changed for the PRF 8523
						strValues += myAFT[llCurr+1]["VIA3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["STOA"], strDateTimeFormat) + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["TIFA"], strDateTimeFormat) + ",";
						strValues += myAFT[llCurr+1]["ACT3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["ONBL"], strDateTimeFormat) + ",";
						strValues += myAFT[llCurr+1]["TTYP"] + ",";
						strValues += myAFT[llCurr+1]["REGN"] + ",";
                        strValues += myAFT[llCurr+1]["RWYA"] + ",";

						if(bmShowStev == true)
						{
							strValues += myAFT[llCurr+1]["STEV"] + ",";
						}
						//-----------------------------------The departure part of rotation
						strValues += myAFT[llCurr]["FLNO"] + ",";
						strValues += myAFT[llCurr]["DES3"] + ",";
						//Changed for the PRF 8523
						strValues += myAFT[llCurr]["VIA3"] + ",";
                        strValues += Helper.DateString(myAFT[llCurr]["STOD"], strDateTimeFormat) + ",";
						strValues += Helper.DateString(myAFT[llCurr]["TIFD"], strDateTimeFormat) + ",";
						strValues += myAFT[llCurr]["ACT3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["OFBL"], strDateTimeFormat) + ",";
						strValues += myAFT[llCurr]["TTYP"] + ",";
                        strValues += myAFT[llCurr]["REGN"] + ",";
                        strValues += myAFT[llCurr]["RWYD"];

						if(bmShowStev == true)
						{
							strValues += "," + myAFT[llCurr]["STEV"];
						}
						strValues += "\n";
						sb.Append(strValues);
					}

				}//if(strCurrRkey == strNextRkey)
				else
				{
					++this.imTotalFlights;
					ilStep = 1;
					if( strCurrAdid == "A")
					{
						++this.imArrivals;
						strValues += myAFT[llCurr]["STOA"] + ",";
						strValues += myAFT[llCurr]["FLNO"] + ",";
						strValues += myAFT[llCurr]["ORG3"] + ",";
						strValues += myAFT[llCurr]["VIA3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["STOA"], strDateTimeFormat) + ",";
						strValues += Helper.DateString(myAFT[llCurr]["TIFA"], strDateTimeFormat) + ",";
						strValues += myAFT[llCurr]["ACT3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["ONBL"], strDateTimeFormat) + ",";
						strValues += myAFT[llCurr]["TTYP"] + ",";
						strValues += myAFT[llCurr]["REGN"] + ",";
                        strValues += myAFT[llCurr]["RWYA"] + ",";

						if(bmShowStev == true)
						{
							strValues += myAFT[llCurr]["STEV"] + ",";
						}
						strValues += bmShowStev == true ? "," + ",,,,,,,,\n" : ",,,,,,,,\n";
						sb.Append(strValues);
					}
					else if( strCurrAdid == "D")
					{
						++this.imDepartures;
						//strValues += myAFT[llCurr]["STOD"] + ",,,,,,,,,";
						strValues += bmShowStev == true ? ",,,,,,,,," + "," + "," : ",,,,,,,,," + ",";
						strValues += myAFT[llCurr]["FLNO"] + ",";
						strValues += myAFT[llCurr]["DES3"] + ",";
						strValues += myAFT[llCurr]["VIA3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["STOD"], strDateTimeFormat) + ",";
						strValues += Helper.DateString(myAFT[llCurr]["TIFD"], strDateTimeFormat) + ",";
						strValues += myAFT[llCurr]["ACT3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["OFBL"], strDateTimeFormat) + ",";						
						strValues += myAFT[llCurr]["TTYP"] + ",";
                        strValues += myAFT[llCurr]["REGN"] + ",";
                        strValues += myAFT[llCurr]["RWYD"];

						if(bmShowStev == true)
						{
							strValues += "," + myAFT[llCurr]["STEV"];
						}
						strValues += "\n";
						sb.Append(strValues);
					}
				}
				llCurr = llCurr + ilStep;
			}//while(llCurr < myAFT.Count)
			if(strReportOrExcel.Equals("Report") == true)
			{
				tabResult.InsertBuffer(sb.ToString(), "\n");
				tabResult.Sort("0", true, true);
				tabResult.Refresh();
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
			else if(strReportOrExcel.Equals("Excel") == true)
			{
				tabExcelResult.InsertBuffer(sb.ToString(),"\n");
				tabExcelResult.Sort("0",true,true);
			}
			lblProgress.Text = "";
			progressBar1.Visible = false;
			
		}
		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportData(string strReportOrExcel)
		{
			//"FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ACTYPE"
			if(myAFT == null) 
				return;
			string strSchedule = "";
			DateTime tmpDate;
			if(strReportOrExcel.Equals("Report") == true)
			{
				lblProgress.Text = "Preparing Data";
				lblProgress.Refresh();
				progressBar1.Visible = true;
				progressBar1.Value = 0;
			}
			int ilTotal = myAFT.Count;
			if(ilTotal == 0) ilTotal = 1;
			imArrivals = 0;
			imDepartures =0 ;
			imRotations = 0;
			imTotalFlights = 0;
			StringBuilder sb = new StringBuilder(10000);

			string strDateTimeFormat = strReportDateTimeFormat;
			if(strReportOrExcel.Equals("Excel") == true)
			{
				strDateTimeFormat = strExcelDateTimeFormat;
			}

			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = "dd'/'HH:mm";
			}
			
			for(int i = 0; i < myAFT.Count; i++)
			{
				string strValues = "";
				string strAdid = myAFT[i]["ADID"];
				if( i % 100 == 0)
				{
					int percent = Convert.ToInt32((i * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;
				}
				if(strAdid == "A")
				{
					imArrivals++;
					imTotalFlights++;
					strValues += myAFT[i]["FLNO"] + ",";
					strValues += strAdid + ",";
					strValues += myAFT[i]["ORG3"] + ",";
					//Changed for the PRF 8523
					strValues += myAFT[i]["VIA3"] + ",";
					strValues += Helper.DateString(myAFT[i]["STOA"], strDateTimeFormat) + ",";
					strValues += Helper.DateString(myAFT[i]["TIFA"], strDateTimeFormat) + ",";					
					strValues += myAFT[i]["ACT3"] + ",";
					strValues += Helper.DateString(myAFT[i]["ONBL"], strDateTimeFormat) + ",";
					strValues += myAFT[i]["TTYP"] + ",";
                    strValues += myAFT[i]["REGN"] + ",";
                    strValues += myAFT[i]["RWYA"];

					if(bmShowStev == true)
					{
						strValues += "," + myAFT[i]["STEV"];
					}
					strValues += "\n";
					sb.Append(strValues);
				}
				if(strAdid == "D")
				{
					imDepartures++;
					imTotalFlights++;
					strValues += myAFT[i]["FLNO"] + ",";
					strValues += strAdid + ",";
					strValues += myAFT[i]["DES3"] + ",";
					//Changed for the PRF 8523
					strValues += myAFT[i]["VIA3"] + ",";
					strValues += Helper.DateString(myAFT[i]["STOD"], strDateTimeFormat) + ",";
					strValues += Helper.DateString(myAFT[i]["TIFD"], strDateTimeFormat) + ",";
					tmpDate = UT.CedaFullDateToDateTime(strSchedule);
					strValues += myAFT[i]["ACT3"] + ",";
					strValues += Helper.DateString(myAFT[i]["OFBL"], strDateTimeFormat) + ",";
					strValues += myAFT[i]["TTYP"] + ",";
                    strValues += myAFT[i]["REGN"] + ",";
                    strValues += myAFT[i]["RWYD"];

					if(bmShowStev == true)
					{
						strValues += "," + myAFT[i]["STEV"];
				}
					strValues += "\n";
					sb.Append(strValues);
				}
			}
			if(strReportOrExcel.Equals("Report") == true)
			{
				tabResult.InsertBuffer(sb.ToString(), "\n");
				tabResult.Sort("4", true, true);
				tabResult.Refresh();
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
			else if(strReportOrExcel.Equals("Excel") == true)
			{
				tabExcelResult.InsertBuffer(sb.ToString(), "\n");
				tabExcelResult.Sort("4", true, true);				
			}
			lblProgress.Text = "";
			progressBar1.Visible = false;
			
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{	
			string strSubHeader = "";
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " ARR: " + imArrivals.ToString();
			strSubHeader += " DEP: " + imDepartures.ToString() + ")";
			strReportSubHeader = strSubHeader;

			string strTmpNC = strNatureCode;
			if (cbRotations.Checked == false)
			{
				if(strNatureCode == "<None>")
					strTmpNC = "";
				strReportHeader = frmMain.strPrefixVal +
					"Flights By Natures/Aircraft/Airlines/Origin/Destinations/Reg.: [N:"  + strTmpNC + " AC: " + strACTypeCode + 
					" AL: " + strAirlineCode + " O: " +  strOriginCode + " D: " + strDestinationCode + "R:" + strRegistrationCode + "OW:" + strRegOwnerCode + "]";
				rptFIPS rpt = new rptFIPS(tabResult, strReportHeader, strSubHeader, "", 10);				
				rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLens);
				rpt.SetExcelExportTabResult(tabExcelResult);
				//rpt.TextBoxCanGrow = false;
				frmPrintPreview frm = new frmPrintPreview(rpt);				
				if(Helper.UseSpreadBuilder() == true)
				{
					activeReport = rpt;
					frm.UseSpreadBuilder(true);
					frm.GetBtnExcelExport().Click += new EventHandler(frmFlightByNatureCode_Click);
				}
				else
				{
					frm.UseSpreadBuilder(false);
				}
				frm.Show();
			}
			else
			{
				strReportHeaderRotation =  frmMain.strPrefixVal +
					"Flights by Nat/A-C/Airline/Org/Des/Reg: [N:"  + strTmpNC + " AC: " + strACTypeCode + 
					" AL: " + strAirlineCode + " O: " +  strOriginCode + " D: " + strDestinationCode + "R:" + strRegistrationCode + "OW:" + strRegOwnerCode + "]";
				prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult,strReportHeader,strSubHeader, "", 10);
				rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLensRotation);
				rpt.SetExcelExportTabResult(tabExcelResult);
				//rpt.TextBoxCanGrow = false;
				frmPrintPreview frm = new frmPrintPreview(rpt);
				if(Helper.UseSpreadBuilder() == true)
				{
					activeReport = rpt;
					frm.UseSpreadBuilder(true);
					frm.GetBtnExcelExport().Click += new EventHandler(frmFlightByNatureCode_Click);
				}
				else
				{
					frm.UseSpreadBuilder(false);
				}
				frm.Show();
			}
		}
		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			if(cbRotations.Checked == false)
			{
				PrepareReportData("Report");
				PrepareReportData("Excel");
			}
			else
			{
				PrepareReportDataRotations("Report");
				PrepareReportDataRotations("Excel");
			}
			RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
						tabExcelResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
						tabExcelResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
					tabExcelResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void InitializeMouseEvents()
		{
			((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmFlightByNatureCode_MouseWheel);
			((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmFlightByNatureCode_MouseEnter);
			((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmFlightByNatureCode_MouseLeave);
		}

		private void frmFlightByNatureCode_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("NAT");
			myDB.Unbind("AFT");
			myDB.Unbind("ACR");
		}

		private void cbRotations_CheckedChanged(object sender, System.EventArgs e)
		{
			InitTab();
		}

		private void label9_Click(object sender, System.EventArgs e)
		{
		
		}

		private void frmFlightByNatureCode_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && tabResult.GetVScrollPos() > 0)
			{				
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
			{
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
			}
		}

		private void frmFlightByNatureCode_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmFlightByNatureCode_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmFlightByNatureCode_Click(object sender, EventArgs e)
		{			
			if (cbRotations.Checked == false)
			{
				Helper.ExportToExcel(tabExcelResult,strReportHeader,strReportSubHeader,strExcelTabHeaderLens,10,0,activeReport);
			}
			else
			{
				Helper.ExportToExcel(tabExcelResult,strReportHeaderRotation,strReportSubHeader,strExcelTabHeaderLensRotation,10,1,activeReport);
			}
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmFlightByNatureCode_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}
	}
}
