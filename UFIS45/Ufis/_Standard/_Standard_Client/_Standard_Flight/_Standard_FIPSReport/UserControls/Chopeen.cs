#region Copyright (c) 2004 Marek Grzenkowicz <chopeen@gmail.com>
/*
 * Copyright (c) 2004 Marek Grzenkowicz <chopeen@gmail.com>
 * 
 * This software is provided 'as-is', without any warranty.
 * 
 * Permission is granted to anyone to use this software for any purpose.
 * 
 * This notice may not be removed from any source distibution; if you are
 * using this software in a product, this notice should be included in
 * materials distributed with your product.
 */
#endregion

#if DEBUG
namespace Chopeen
{
    /// <summary>
    /// This is <b>Chopeen</b> namespace.<br/>
    /// I (<a href="mailto:chopeen@gmail.com">chopeen@gmail.com</a>) put everything I code here.
    /// </summary>
    internal class NamespaceDoc
    {
    }
}
#endif