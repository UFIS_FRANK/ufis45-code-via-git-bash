using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Drawing;
using Ufis.Utils;
using System.Text.RegularExpressions;

using System.Collections.Generic;

namespace FIPS_Reports
{
	public class prtFIPS_Landscape : ActiveReport3
	{
		#region _My Members
		/// <summary>
		/// Stores the main header.
		/// </summary>
		private string _header1 = "";
		/// <summary>
		/// Stores the sub header string
		/// </summary>
		private string _header2 = "";
		/// <summary>
		/// Stores the data types for each column.
		/// This might be necessary for type convertions.
		/// </summary>
		private string [] arrTypes;
		/// <summary>
		/// The reference to the tab control, which is
		/// a parameter of the constructor.
		/// </summary>
		private AxTABLib.AxTAB tabResult;
		/// <summary>
		/// The tableResult for report output
		/// </summary>
		private AxTABLib.AxTAB tabReportResult;
		/// <summary>
		/// The tableResult for excel export
		/// </summary>
		private AxTABLib.AxTAB tabExcelResult;
		/// <summary>
		/// Stores the current line number. This is necessary
		/// to identif when EOF in the xxx_fetchdata method must
		/// return end of report. The current value is checked 
		/// against the Total line count of the tab control.
		/// </summary>
		private int currLine = 0;
		/// <summary>
		/// Stores the header text in an array. This is filled
		/// in the constructor.
		/// </summary>
		string [] arrHeader;
		/// <summary>
		/// The length list from the tab control. This is generated
		/// in the constructor.
		/// </summary>
		string [] arrLens;  
		/// <summary>
		/// The length list from the tab control used for report.
		/// </summary>
		string [] arrReportLens;
		/// <summary>
		/// The length list from the tab control used for excel export. This is generated
		/// in the SetExcelExportHeaderLenghtString() function.
		/// </summary>
		string [] arrExcelLens;
        /// <summary>
		/// Stores the logical field list of the tab control. This 
		/// names are used for the dynamically generated report controls.
		/// </summary>
		string [] arrNames; 
		/// <summary>
		/// The font size for the detail section.
		/// </summary>
        int imFontSize = 10;
        /// <summary>
        /// The font size for the header section.
        /// </summary>
        int imHdrFontSize = 10; //igu on 19/05/2011
		/// <summary>
		/// Defines whether a txtbox can grow or not
		/// </summary>
		bool bmFieldCanGrow = true;		
		/// <summary>
		/// Property, to allow/forbid gorwing of a textbox according to the content.
		/// </summary>
		bool blFirst = true;
		/// <summary>
		/// In excel export report is run twice, hence to return back to normal print width this variable is used.
		/// </summary>
		float fmReportPrintWidth;
		/// <summary>
		/// In excel export length of few fields increased, hence report print width also needs to be increased.
		/// </summary>
		float fmExcelPrintWidth = 0;
		/// <summary>
		/// In excel export length of header columns are more than normal report output, hence this is set
		/// in SetExcelExportHeaderLenghtString(...) function.
		/// </summary>
		string strExcelHeaderLengthString = "";
		
		public bool TextBoxCanGrow
		{
			get{return bmFieldCanGrow;}
			set{bmFieldCanGrow = value;}
		}

		float maxHeight = 0F;
		
		private int imTotalNoOfHeaderControls;
		
		/// <summary>
		/// PageSupport of printer
		/// </summary>
		private System.Drawing.Printing.PaperKind reportPaperKind;

		#endregion _My Members

        //----
        //igu on 19/05/2011

        /// <summary>
        /// Constructor:
        /// The tab control is a parameter. This tab is used to generate
        /// the report dynamically.
        /// </summary>
        /// <param name="pTab">The tab control with settings and data</param>
        /// <param name="logicalFields">Logical fields of tab control to be displayed</param>
        /// <param name="headerCaptions">Header text of tab control to be displayed</param>
        /// <param name="headerLengths">Fields length of tab control to be displayed</param>
        /// <param name="header1">The main header string</param>
        /// <param name="header2">Additional header string</param>
        /// <param name="fieldtypes">The types for the fields. This might be
        /// necessary to convert data to the respective format.
        /// </param>
        /// <param name="detailFontSize">Font size for detail section</param>
        /// <param name="headerFontSize">Font size for header section</param>
        public prtFIPS_Landscape(AxTABLib.AxTAB pTab, string logicalFields, string headerCaptions, string headerLengths, 
            string header1, string header2, string fieldtypes, int detailFontSize, int headerFontSize)
        {
            imFontSize = detailFontSize;
            _header1 = header1;
            _header2 = header2;
            arrTypes = fieldtypes.Split(',');
            tabExcelResult = tabResult = tabReportResult = pTab;
            if (string.IsNullOrEmpty(logicalFields))
            {
                arrNames = tabResult.LogicalFieldList.Split(',');
            }
            else
            {
                arrNames = logicalFields.Split(',');
            }
            if (string.IsNullOrEmpty(headerCaptions))
            {
                arrHeader = tabResult.HeaderString.Split(',');
            }
            else
            {
                arrHeader = headerCaptions.Split(',');
            }
            if (string.IsNullOrEmpty(headerLengths))
            {
                arrExcelLens = arrLens = arrReportLens = tabResult.HeaderLengthString.Split(',');
            }
            else
            {
                arrExcelLens = arrLens = arrReportLens = headerLengths.Split(',');
            }
            if (headerFontSize != 0)
            {
                imHdrFontSize = headerFontSize;
            }
            InitializeReport();
            InitializeEventHandler();
            ReportHeader.Visible = false;
            ReportFooter.Visible = false;
            CalculateNoOfHeaderControls();
            fmExcelPrintWidth = fmReportPrintWidth = this.PrintWidth;
            reportPaperKind = this.PageSettings.PaperKind;
        }

		/// <summary>
		/// Constructor:
		/// The tab control is a parameter. This tab is used to generate
		/// the report dynamically.
		/// </summary>
		/// <param name="pTab">The tab control with settings and data</param>
		/// <param name="header1">The main header string</param>
		/// <param name="header2">Additional header string</param>
		/// <param name="fieldtypes">The types for the fields. This might be
		/// necessary to convert data to the respective format.
		/// </param>
		public prtFIPS_Landscape(AxTABLib.AxTAB pTab, string header1, string header2, string fieldtypes, int fontSize)
            : this(pTab, null, null, null, header1, header2, fieldtypes, fontSize, 0)
		{
            //imFontSize = fontSize;
            //_header1 = header1;
            //_header2 = header2;
            //arrTypes = fieldtypes.Split(',');
            //tabExcelResult = tabResult = tabReportResult = pTab;
            //arrHeader = tabResult.HeaderString.Split(',');
            //arrExcelLens = arrLens = arrReportLens = tabResult.HeaderLengthString.Split(',');
            //arrNames = tabResult.LogicalFieldList.Split(',');
            //InitializeReport();
            //InitializeEventHandler();
            //ReportHeader.Visible = false;
            //ReportFooter.Visible = false;
            //CalculateNoOfHeaderControls();
            //fmExcelPrintWidth = fmReportPrintWidth = this.PrintWidth;
            //reportPaperKind = this.PageSettings.PaperKind;	
		}

        //---

		/// <summary>
		/// Called when the report starts. This function must call the 
		/// methods, which generate the header-, detail- and footer sections.
		/// </summary>
		/// <param name="sender">Originator of the call. This is the report itself.</param>
		/// <param name="eArgs">Event arguments.</param>
		private void prtFIPS_Landscape_ReportStart(object sender, System.EventArgs eArgs)
		{
			if(Helper.IsPageSupportedByPrinter(reportPaperKind,this.Document.Printer,false) == false)
			{
				this.Document.Printer.PrinterName = "";
				
			}

			maxHeight = 0.0f;
			currLine = 0;
			DateTime d		= DateTime.Now;
			txtPageDate.Text	= d.ToString();
			lblPageHeader1.Text = _header1;
			lblPageHeader2.Text = _header2;

			txtReportDate.Text = d.ToString();
			lblReportHeader1.Text = _header1;
			lblReportHeader2.Text = _header2;

			//*** Read the logo if exists and set it
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strPrintLogoPath = myIni.IniReadValue("GLOBAL", "PRINTLOGO");
			Image myImage = null;
			try
			{
				if(strPrintLogoPath != "")
				{
					myImage = new Bitmap(strPrintLogoPath);
					Picture1.Image = myImage;
					Picture2.Image = myImage;
				}
			}
			catch(Exception)
			{
			}
			
			if(ReportHeader.Visible == true)
			{				
				tabResult = tabExcelResult;
				this.PrintWidth = (fmExcelPrintWidth > fmReportPrintWidth ? fmExcelPrintWidth : fmReportPrintWidth);
				arrLens = arrExcelLens;				
			}
			else
			{	
				tabResult = tabReportResult;
				this.PrintWidth = fmReportPrintWidth;
				arrLens = arrReportLens;
			}

			MakeReportHeader();
			MakeReportDetail();
		}
		/// <summary>
		/// Generates the Detail section of the report according
		/// to the tab control.
		/// </summary>
		private void MakeReportDetail()
		{
            if (this.multiLineData != null)
            {
                MakeReportDetailMultiLine();
                return;
            }

			Detail.Controls.Clear();
			float ilX = 0;
			float ilCurrW = 0f;
            List<float> lsIlX = new List<float>();
            List<float> lsIlCurrW = new List<float>();

            int lineNo = 0;
            int colNo = 0;

			DataDynamics.ActiveReports.Border bd = new Border();
			System.Drawing.Font fnt = new System.Drawing.Font("Arial", imFontSize, System.Drawing.FontStyle.Regular);
            bool firstCol = true; //igu on 31/05/2011
			for(int i = 0; i < arrHeader.Length; i++)
			{
				if(Convert.ToInt32(arrLens[i]) > 0)
				{
					ilCurrW = PageHeader.Visible == true ? PixToInch (Convert.ToInt32(arrLens[i])) : PixToInch (Convert.ToInt32(arrExcelLens[i]));
					DataDynamics.ActiveReports.TextBox  txtBox = new TextBox();
					DataDynamics.ActiveReports.Line olLine = new Line();
					txtBox.Font = fnt;
					txtBox.Top = 0;
					txtBox.Height = 0.2f;
					txtBox.Left = ilX;
                    txtBox.Alignment = TextAlignment.Center;
                    txtBox.Width = ilCurrW;
					txtBox.CanGrow = bmFieldCanGrow;
					txtBox.WordWrap = false;
					olLine.Y1 = 0;
					olLine.Y2 = 0.2f;
					olLine.X1 = ilX;
					olLine.X2 = ilX;
					olLine.Name = "lin" + arrNames[i];
					ilX += ilCurrW;
                    //txtBox.Border.BottomStyle = BorderLineStyle.Solid;
                    if (firstCol) //igu on 31/05/2011
                    {
                        txtBox.Border.LeftStyle = BorderLineStyle.Solid;
                        firstCol = false; //igu on 31/05/2011
                    }
                    //txtBox.Border.TopStyle = BorderLineStyle.Solid;
                    txtBox.Border.RightStyle = BorderLineStyle.Solid;
					txtBox.Name = "txt" + arrNames[i];
					txtBox.Text = "txt" + arrNames[i];
					Detail.Controls.Add(txtBox);
					Detail.Controls.Add(olLine);
				}
			}
			DataDynamics.ActiveReports.Line olLineBottom = new Line();
			olLineBottom.X1 = 0;
			olLineBottom.X2 = ilX;//+ilCurrW;
			olLineBottom.Y1 = 0.2f;
			olLineBottom.Y2 = 0.2f;
			olLineBottom.Name = "linBottom";
			Detail.Controls.Add(olLineBottom);
			//Last vertical separator on the right end to finish the line
			DataDynamics.ActiveReports.Line olLastVertical = new Line();
			olLastVertical.Y1 = 0;
			olLastVertical.Y2 = 0.2f;//+ilCurrW;
			olLastVertical.X1 = ilX;
			olLastVertical.X2 = ilX;
			olLastVertical.Name = "linLast";
			Detail.Controls.Add(olLastVertical);
		}
		/// <summary>
		/// Generates the report Header from the tab
		/// </summary>
		private void MakeReportHeader()
		{
            if (this.multiLineData != null)
            {
                MakeReportHeaderMultiLine();
                return;
            }

			float ilX = 0;
			//DataDynamics.ActiveReports.Border bd = new Border();
            //System.Drawing.Font fnt = new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Bold); //igu on 18/05/2011
            System.Drawing.Font fnt = new System.Drawing.Font("Arial", imHdrFontSize, System.Drawing.FontStyle.Bold); //igu on 18/05/2011
			for(int i = 0; i < arrHeader.Length; i++)
			{
				float ilCurrW = PageHeader.Visible == true ? PixToInch((Convert.ToInt32(arrLens[i]))) : PixToInch((Convert.ToInt32(arrExcelLens[i])));
				if(ilCurrW > 0)
				{
					Label lbl = new Label();
					lbl.Font = fnt;
					lbl.Top = 1;
					lbl.Height = 0.2f;
					lbl.Left = ilX;
					lbl.Width = ilCurrW;
					lbl.Alignment = TextAlignment.Center;
					ilX += ilCurrW;
					lbl.Border.BottomStyle = BorderLineStyle.Solid;
					lbl.Border.LeftStyle = BorderLineStyle.Solid;
					lbl.Border.TopStyle  = BorderLineStyle.Solid;
					lbl.Border.RightStyle = BorderLineStyle.Solid;
					lbl.Name = "lbl" + arrNames[i];
					lbl.Text = arrHeader[i];
					lbl.BackColor = Color.LightGray;
					if(ReportHeader.Visible == true && ReportHeader.Controls.Count < imTotalNoOfHeaderControls)
					{
						ReportHeader.Controls.Add(lbl);
					}
					if(PageHeader.Visible == true && PageHeader.Controls.Count < imTotalNoOfHeaderControls)
					{
						PageHeader.Controls.Add(lbl);
					}
				}
			}
		}
		/// <summary>
		/// Converts a pixel integer into inches
		/// </summary>
		/// <param name="pix">Input parameter. The number of pixels.</param>
		/// <returns>The converted pixel in inches.</returns>
		private float PixToInch(int pix)
		{
			float fRet = 0f;
			float twips = 0f;
			//First we need twips
			twips = pix * 16f; //Normally it's 15 but 16 meets better!
			//1440 twips = 1 inch
			fRet = (float)(twips / 1440f);
			return fRet;
		}

		private void prtFIPS_Landscape_FetchData(object sender, DataDynamics.ActiveReports.ActiveReport3.FetchEventArgs eArgs)
		{
            if (this.multiLineData != null)
            {
                prtFIPS_Landscape_FetchDataMultiLine(sender, eArgs);
                return;
            }

			Detail.Height = 0.2f;
			if(tabResult.GetLineCount() == 0)
			{
				eArgs.EOF = true;
				return;
			}

			if(currLine < tabResult.GetLineCount())
			{
				TextBox txt;				
				//string [] arrValues = tabResult.GetLineValues(currLine).Split(','); //igu on 18/05/2011
                string[] arrValues = tabResult.GetFieldValues(currLine, string.Join(",", arrNames)).Split(','); //igu on 18/05/2011
				for(int i = 0; i < arrNames.Length; i++)
				{
					if(Convert.ToInt32(arrLens[i]) > 0)
					{	
						string strName = "txt" + arrNames[i];						
						txt = (TextBox)Detail.Controls[strName];                        
						txt.Text = arrValues[i].Length > 1 ? arrValues[i] : " " + arrValues[i];
						
						if(txt.Height > maxHeight)
							maxHeight = txt.Height;
					}
				}

				currLine++;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		/// <summary>
		/// Calculate the no of header controls
		/// </summary>
		private void Detail_BeforePrint(object sender, System.EventArgs eArgs)
		{
            if (this.multiLineData != null)
            {
                Detail_BeforePrintMultiLine(sender, eArgs);
                return;
            }

			maxHeight = Detail.Height;
			TextBox textBox;
			string strName;			
			Line lin;
			for(int i = 0; i < arrNames.Length; i++)
			{
				if(Convert.ToInt32(arrLens[i]) > 0)
				{
					strName = "lin" + arrNames[i];
					lin = (Line)Detail.Controls[strName];
					lin.Y2 = maxHeight;
                    lin.Visible = false;
				}
				if(Convert.ToInt32(arrLens[i]) > 0 && Detail.Height > 0.2f)
				{					
					strName = "txt" + arrNames[i];					
					textBox = (TextBox)Detail.Controls[strName];					
					textBox.WordWrap = true;
				}
			}
			lin = (Line)Detail.Controls["linLast"];
			lin.Y2 = maxHeight;
		}

		/// <summary>
		/// Prints the bottom line in detail section
		/// </summary>
		private void Detail_AfterPrint(object sender, System.EventArgs eArgs)
		{
            return;
			Line lin;
			lin = (Line)Detail.Controls["linBottom"];	
			lin.Y1 = maxHeight;
			lin.Y2 = maxHeight;
		}

		private void CalculateNoOfHeaderControls()
		{
			int ilCount = 0;
			for(int i = 0; i < arrHeader.Length; i++)
			{
				float ilCurrW = PixToInch (Convert.ToInt32(arrLens[i]));
				if(ilCurrW > 0)
				{
					ilCount++;
				}
			}
			imTotalNoOfHeaderControls = PageHeader.Controls.Count + ilCount;
		}

		/// <summary>
		/// Sets the tabResult for excel export
		/// </summary>
		public void SetExcelExportTabResult(AxTABLib.AxTAB tabResult)
		{
			tabExcelResult = tabResult;
		}
		
		/// <summary>
		/// Sets the length of the headers to be exported to excel
		/// </summary>
		public void SetExcelExportHeaderLenghtString(string strHeaderLengthString)
		{
			fmExcelPrintWidth = 0;
			strExcelHeaderLengthString = strHeaderLengthString;
			arrExcelLens = strExcelHeaderLengthString.Split(',');
			for(int i = 0; i < arrExcelLens.Length; i++)
			{
				fmExcelPrintWidth += PixToInch((Convert.ToInt32(arrExcelLens[i])));
			}
		}

		/// <summary>
		/// Initializes the event handler.
		/// </summary>
		private void InitializeEventHandler()
		{
			this.Document.Printer.BeginPrint += new System.Drawing.Printing.PrintEventHandler(Printer_BeginPrint);
		}

		/// <summary>
		/// Starts the print operation to the printer
		/// </summary>
		private void Printer_BeginPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			DataDynamics.ActiveReports.Document.Printer printer = (DataDynamics.ActiveReports.Document.Printer)sender;
			if(Helper.IsPageSupportedByPrinter(reportPaperKind,printer,true) == false)
			{				
				e.Cancel = true;
			}
			else
			{
				printer.PaperKind = reportPaperKind;
			}
		}

		#region ActiveReports Designer generated code
		private ReportHeader ReportHeader = null;
		private Picture Picture1 = null;
		private Label lblReportHeader1 = null;
		private Label lblReportHeader2 = null;
		private PageHeader PageHeader = null;
		private Picture Picture2 = null;
		private Label lblPageHeader1 = null;
		private Label lblPageHeader2 = null;
		private Detail Detail = null;
		private PageFooter PageFooter = null;
		private TextBox txtPageDate = null;
		private Label Label34 = null;
		private Label Label33 = null;
		private TextBox TextBox2 = null;
		private TextBox TextBox3 = null;
		private Label Label32 = null;
		private ReportFooter ReportFooter = null;
		private TextBox txtReportDate = null;
		private Label Label35 = null;
		private Label Label36 = null;
		private TextBox TextBox5 = null;
		private TextBox TextBox6 = null;
		private Label Label37 = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "FIPS_Reports.prtFIPS_Landscape.rpx");
			this.ReportHeader = ((DataDynamics.ActiveReports.ReportHeader)(this.Sections["ReportHeader"]));
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.ReportFooter = ((DataDynamics.ActiveReports.ReportFooter)(this.Sections["ReportFooter"]));
			this.Picture1 = ((DataDynamics.ActiveReports.Picture)(this.ReportHeader.Controls[0]));
			this.lblReportHeader1 = ((DataDynamics.ActiveReports.Label)(this.ReportHeader.Controls[1]));
			this.lblReportHeader2 = ((DataDynamics.ActiveReports.Label)(this.ReportHeader.Controls[2]));
			this.Picture2 = ((DataDynamics.ActiveReports.Picture)(this.PageHeader.Controls[0]));
			this.lblPageHeader1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
			this.lblPageHeader2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
			this.txtPageDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[0]));
			this.Label34 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
			this.Label33 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[2]));
			this.TextBox2 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[3]));
			this.TextBox3 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[4]));
			this.Label32 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[5]));
			this.txtReportDate = ((DataDynamics.ActiveReports.TextBox)(this.ReportFooter.Controls[0]));
			this.Label35 = ((DataDynamics.ActiveReports.Label)(this.ReportFooter.Controls[1]));
			this.Label36 = ((DataDynamics.ActiveReports.Label)(this.ReportFooter.Controls[2]));
			this.TextBox5 = ((DataDynamics.ActiveReports.TextBox)(this.ReportFooter.Controls[3]));
			this.TextBox6 = ((DataDynamics.ActiveReports.TextBox)(this.ReportFooter.Controls[4]));
			this.Label37 = ((DataDynamics.ActiveReports.Label)(this.ReportFooter.Controls[5]));
			// Attach Report Events
			this.ReportStart += new System.EventHandler(this.prtFIPS_Landscape_ReportStart);
			this.FetchData += new DataDynamics.ActiveReports.ActiveReport3.FetchEventHandler(this.prtFIPS_Landscape_FetchData);
			this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
			this.Detail.AfterPrint += new System.EventHandler(this.Detail_AfterPrint);
		}

		#endregion

        List<float> lsMaxHeight = new List<float>();
        PrtMultiLineData multiLineData = null;

        /// <summary>
        /// Constructor:
        /// The tab control is a parameter. This tab is used to generate
        /// the report dynamically.
        /// </summary>
        /// <param name="pTab">The tab control with settings and data</param>
        /// <param name="header1">The main header string</param>
        /// <param name="header2">Additional header string</param>
        /// <param name="fieldtypes">The types for the fields. This might be
        /// necessary to convert data to the respective format.
        /// </param>
        public prtFIPS_Landscape(AxTABLib.AxTAB pTab, string header1, string header2, string fieldtypes, int fontSize,
            PrtMultiLineData iniMultilineData)
        {
            imFontSize = fontSize;
            _header1 = header1;
            _header2 = header2;
            arrTypes = fieldtypes.Split(',');
            tabExcelResult = tabResult = tabReportResult = pTab;
            arrHeader = tabResult.HeaderString.Split(',');
            arrExcelLens = arrLens = arrReportLens = tabResult.HeaderLengthString.Split(',');
            arrNames = tabResult.LogicalFieldList.Split(',');
            InitializeReport();
            InitializeEventHandler();
            ReportHeader.Visible = false;
            ReportFooter.Visible = false;
            CalculateNoOfHeaderControls();
            fmExcelPrintWidth = fmReportPrintWidth = this.PrintWidth;
            reportPaperKind = this.PageSettings.PaperKind;
            this.multiLineData = iniMultilineData;
            if (this.multiLineData == null)
            {
                this.multiLineData = new PrtMultiLineData(1, 2);
            }

            for (int i = 0; i < arrHeader.Length; i++)
            {
                try
                {
                    this.multiLineData.AddFor1stLine(i);
                }
                catch (Exception)
                {
                }
            }

            //igu on 28/01/2011
            //compare the size of actual column in report with the multi line column
            //and adjust the size of actual column to fit multi line column
            foreach (PrtMultiLineData.PrtData prtData in this.multiLineData.ListPrtData)
            {
                int reportColumnWidth;
                int tabColumnWidth;
                
                int colInReport = prtData._colInReport;
                int idxInTab = prtData._idxInTab;

                if (!int.TryParse(arrReportLens[colInReport], out reportColumnWidth))
                    reportColumnWidth = 0;
                if (!int.TryParse(arrReportLens[idxInTab], out tabColumnWidth))
                    tabColumnWidth = 0;

                if (tabColumnWidth > reportColumnWidth)
                {
                    arrReportLens[colInReport] = tabColumnWidth.ToString();
                    arrLens[colInReport] = tabColumnWidth.ToString();
                    arrExcelLens[colInReport] = tabColumnWidth.ToString();
                }
            }
            //
        }

        private int GetLineNoInMultiLine(int idxInTab)
        {
            int lineNo = 0;
            if (this.multiLineData != null)
            {
                PrtMultiLineData.PrtData prtData = null;
                if (this.multiLineData.IsIdxInTabExisted(idxInTab, out prtData))
                {
                    lineNo = prtData._lineInReport;
                }
            }
            return lineNo;
        }

        List<List<ARControl>> lsDetControlsByLine = new List<List<ARControl>>();
        Dictionary<int,int> mapDetDataIdxLine = new Dictionary<int,int>();
        List<Line> lsDetVerticalLine = new List<Line>();
        List<Line> lsDetFullBottomLine = new List<Line>();

        private const float DEFAULT_HEIGHT = 0.2f; 
        /// <summary>
        /// Generates the Detail section of the report according
        /// to the tab control.
        /// </summary>
        private void MakeReportDetailMultiLine()
        {
            Detail.Controls.Clear();
            float maxX = 0;
            float ilCurrW = 0f;
            int maxLine = this.multiLineData.MaxLineNo;
            if (maxLine < 1) maxLine = 1;
            List<float> lsIlX = new List<float>();
            lsDetControlsByLine.Clear();
            mapDetDataIdxLine.Clear();
            lsDetVerticalLine.Clear();
            lsDetFullBottomLine.Clear();

            for (int i = 0; i < maxLine; i++)
            {
                lsIlX.Add ( 0f );
                lsDetControlsByLine.Add(new List<ARControl>());
            }

            int lineNo = 0;
            int colNo = 0;
          

            DataDynamics.ActiveReports.Border bd = new Border();
            System.Drawing.Font fnt = new System.Drawing.Font("Arial", imFontSize, System.Drawing.FontStyle.Regular);
            this.multiLineData.ListPrtData.Sort(new PrtMultiLineData.PrtDataComparerByPositionInReport());

            foreach( PrtMultiLineData.PrtData prtData in this.multiLineData.ListPrtData )
            {
                int i = prtData._idxInTab;
                lineNo = prtData._lineInReport;
                colNo = prtData._colInReport;
                if (lineNo < 0) lineNo = 0;
 
                if (Convert.ToInt32(arrLens[i]) > 0)
                {
                    ilCurrW = PageHeader.Visible == true ? PixToInch(Convert.ToInt32(arrLens[i])) : PixToInch(Convert.ToInt32(arrExcelLens[i]));
                    DataDynamics.ActiveReports.TextBox txtBox = new TextBox();
                    DataDynamics.ActiveReports.Line olLine = new Line();

                    if (lineNo < 1)
                    {                  
                        prtData._width = ilCurrW;
                        prtData._left = lsIlX[lineNo];

                        olLine.Y1 = 0 + DEFAULT_HEIGHT * lineNo;
                        olLine.Y2 = 0.2f + olLine.Y1;
                        olLine.X1 = lsIlX[lineNo];
                        olLine.X2 = lsIlX[lineNo];
                        olLine.Name = "lin" + arrNames[i];

                        Detail.Controls.Add(olLine);
                        lsDetVerticalLine.Add(olLine);
                        lsDetControlsByLine[lineNo].Add(olLine);
                    }
                    else
                    {
                        PrtMultiLineData.PrtData line1Data = null;
                        if (this.multiLineData.GetPrtData(0, colNo, out line1Data))
                        {
                            ilCurrW = line1Data._width;
                            lsIlX[lineNo] = line1Data._left;
                        }
                    }
                   
                    txtBox.Font = fnt;
                    txtBox.Top = DEFAULT_HEIGHT * lineNo;
                    txtBox.Height = DEFAULT_HEIGHT;
                    txtBox.Left = lsIlX[lineNo];
                    txtBox.Width = ilCurrW;
                    txtBox.CanGrow = bmFieldCanGrow;
                    txtBox.WordWrap = false;
                    txtBox.BackColor = prtData._bagroundColor;

                    
                    
                    lsIlX[lineNo] += ilCurrW;
                    //					txtBox.Border.BottomStyle = BorderLineStyle.Solid;
                    //					txtBox.Border.LeftStyle = BorderLineStyle.Solid;
                    //					txtBox.Border.TopStyle  = BorderLineStyle.Solid;
                    //					txtBox.Border.RightStyle = BorderLineStyle.Solid;
                    txtBox.Name = "txt" + arrNames[i];
                    txtBox.Text = "txt" + arrNames[i];
                    //txtBox.Visible = false;
                    //olLine.Visible = false;
                    Detail.Controls.Add(txtBox);
                    
                    lsDetControlsByLine[lineNo].Add(txtBox);
                    
                    mapDetDataIdxLine.Add(i,lineNo);
                    if (lsIlX[lineNo] > maxX) maxX = lsIlX[lineNo];
                }
            }

            for (int i = 0; i < maxLine; i++)
            {
                DataDynamics.ActiveReports.Line olLineBottom = new Line();
                float y = DEFAULT_HEIGHT * (i +1);
                olLineBottom.X1 = 0;
                olLineBottom.X2 = maxX;
                olLineBottom.Y1 = y - 0.001f;
                olLineBottom.Y2 = y - 0.001f;
                olLineBottom.Name = "linBottom" + i;
                Detail.Controls.Add(olLineBottom);
                //Last vertical separator on the right end to finish the line
                DataDynamics.ActiveReports.Line olLastVertical = new Line();
                olLastVertical.Y1 = 0;
                olLastVertical.Y2 = y;
                olLastVertical.X1 = maxX;
                olLastVertical.X2 = maxX;
                olLastVertical.Name = "linLast" +i ;
                Detail.Controls.Add(olLastVertical);
                lsDetControlsByLine[i].Add(olLastVertical);
            }

            int vCnt = 0;
            foreach (Line ln in lsDetVerticalLine)
            {
                float x = ln.X1;
                for (int i = 1; i < maxLine; i++)
                {
                    float y = DEFAULT_HEIGHT * i;
                    DataDynamics.ActiveReports.Line olV = new Line();

                    olV.Y1 = y;
                    olV.Y2 = olV.Y1 + DEFAULT_HEIGHT;//+ilCurrW;
                    olV.X1 = x;
                    olV.X2 = x;
                    olV.Name = string.Format("linV{0}_{1}", i, vCnt++);
                    Detail.Controls.Add(olV);
                    lsDetControlsByLine[i].Add(olV);
                }
            }
        }

        /// <summary>
        /// Generates the report Header from the tab
        /// </summary>
        private void MakeReportHeaderMultiLine()
        {
            float ilX = 0;
            //DataDynamics.ActiveReports.Border bd = new Border();
            //System.Drawing.Font fnt = new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Bold); //igu on 18/05/2011
            System.Drawing.Font fnt = new System.Drawing.Font("Arial", imHdrFontSize, System.Drawing.FontStyle.Bold); //igu on 18/05/2011	
            for (int i = 0; i < arrHeader.Length; i++)
            {
                PrtMultiLineData.PrtData prtData = null;
                if (this.multiLineData.IsIdxInTabExisted(i, out prtData))
                {
                    if (prtData._lineInReport != 0) continue;
                }

                float ilCurrW = PageHeader.Visible == true ? PixToInch((Convert.ToInt32(arrLens[i]))) : PixToInch((Convert.ToInt32(arrExcelLens[i])));
                if (ilCurrW > 0)
                {
                    Label lbl = new Label();
                    lbl.Font = fnt;
                    lbl.Top = 1;
                    lbl.Height = DEFAULT_HEIGHT;
                    lbl.Left = ilX;
                    lbl.Width = ilCurrW;
                    lbl.Alignment = TextAlignment.Center;
                    ilX += ilCurrW;
                    lbl.Border.BottomStyle = BorderLineStyle.Solid;
                    lbl.Border.LeftStyle = BorderLineStyle.Solid;
                    lbl.Border.TopStyle = BorderLineStyle.Solid;
                    lbl.Border.RightStyle = BorderLineStyle.Solid;
                    lbl.Name = "lbl" + arrNames[i];
                    lbl.Text = arrHeader[i];
                    lbl.BackColor = Color.LightGray;
                    if (ReportHeader.Visible == true && ReportHeader.Controls.Count < imTotalNoOfHeaderControls)
                    {
                        ReportHeader.Controls.Add(lbl);
                    }
                    if (PageHeader.Visible == true && PageHeader.Controls.Count < imTotalNoOfHeaderControls)
                    {
                        PageHeader.Controls.Add(lbl);
                    }
                }
            }
        }

        float CurLineHeight = DEFAULT_HEIGHT;
        private void prtFIPS_Landscape_FetchDataMultiLine(object sender, DataDynamics.ActiveReports.ActiveReport3.FetchEventArgs eArgs)
        {
            Detail.Height = 0.2f;
            if (tabResult.GetLineCount() == 0)
            {
                eArgs.EOF = true;
                return;
            }

            if (currLine < tabResult.GetLineCount())
            {
                TextBox txt;
                string[] arrValues = tabResult.GetLineValues(currLine).Split(',');
                List<bool> lsHasData = new List<bool>();
                lsHasData.Add(true);
                int maxLineNo = this.multiLineData.MaxLineNo;
                for (int i = 1; i < maxLineNo; i++)
                {
                    lsHasData.Add(false);
                }

                for (int i = 0; i < arrNames.Length; i++)
                {
                    if (Convert.ToInt32(arrLens[i]) > 0)
                    {
                        string strName = "txt" + arrNames[i];
                        txt = (TextBox)Detail.Controls[strName];

                        txt.Text = arrValues[i].Length > 1 ? arrValues[i] : " " + arrValues[i];

                        int lineNo = mapDetDataIdxLine[i];

                        if (lineNo > 0 && !lsHasData[lineNo] && arrValues[i].Length > 0) lsHasData[lineNo] = true;

                        if (txt.Height > maxHeight)
                            maxHeight = txt.Height;
                    }
                }

                int visibleLineCnt = 1;
                for (int i = 1; i < maxLineNo; i++)
                {
                    bool visibleLine = lsHasData[i];
                    if (visibleLine) visibleLineCnt++;
                    List<ARControl> ls = this.lsDetControlsByLine[i];
                    for (int j = 0; j < ls.Count; j++)
                    {
                        ls[j].Visible = visibleLine;
                    }
                }


                //float lineHeight = DEFAULT_HEIGHT * visibleLineCnt;

                //foreach (Line vLine in this.lsDetVerticalLine)
                //{
                //    vLine.Y2 = lineHeight;
                //}

                //Line lin = (Line)Detail.Controls["linLast"];
                //lin.Y2 = lineHeight;
                //lin.Visible = false;
                //lin = (Line)Detail.Controls["linBottom"];
                //lin.Y2 = lineHeight + 0.01f;
                //lin.Y1 = lineHeight;
                //lin.Visible = true;

                currLine++;
                eArgs.EOF = false;
            }
            else
            {
                eArgs.EOF = true;
            }
        }

        /// <summary>
        /// Calculate the no of header controls
        /// </summary>
        private void Detail_BeforePrintMultiLine(object sender, System.EventArgs eArgs)
        {
            return;
            maxHeight = Detail.Height;
            TextBox textBox;
            string strName;
            Line lin;
            string[] arrValues = tabResult.GetLineValues(currLine).Split(',');

            for (int i = 0; i < arrNames.Length; i++)
            {
                if (Convert.ToInt32(arrLens[i]) > 0)
                {
                    if (arrValues.Length>i && arrValues[i].Length > 0)
                    {
                        strName = "lin" + arrNames[i];
                        lin = (Line)Detail.Controls[strName];
                        lin.Y2 = maxHeight;
                        lin.Visible = false;

                        if (Detail.Height > 0.2f)
                        {
                            strName = "txt" + arrNames[i];
                            textBox = (TextBox)Detail.Controls[strName];
                            textBox.WordWrap = true;
                        }
                    }
                }

            }

            //lin = (Line)Detail.Controls["linLast"];
            //lin.Y2 = maxHeight;
        }

	}




    /// <summary>
    /// Helper class to print one row of data in multiple line in the report
    /// </summary>
    public class PrtMultiLineData
    {
        private static readonly Color DEFAULT_ODD_LINE_BK_COLOR = Color.AliceBlue;
        private static readonly Color DEFAULT_EVEN_LINE_BK_COLOR = Color.AntiqueWhite;
        int _startPos = 10;//10 character away from left;
        int _startColNo = 1;//start from 2nd Column

        public int StartColNo
        {
            get { return _startColNo; }
            set
            {
                if (value < 0) _startColNo = 0;
                else _startColNo = value;
            }
        }
        int _maxLineNo = -1;

        public int MaxLineNo
        {
            get { return _maxLineNo; }
            //set { _maxLineNo = value; }
        }
        int _maxColNo = -1;

        public int MaxColNo
        {
            get { return _maxColNo; }
            set {
                if (value < _startColNo || value<1) throw new ApplicationException("Invalid Column No.");
                else _maxColNo = value; 
            }
        }

        int _lastLineNo = -1;
        int _lastColNo = -1;

        List<PrtData> _lsData = new List<PrtData>();//Index from Original Table to print in multi line

        internal List<PrtData> ListPrtData
        {
            get { return _lsData; }
            //set { _lsData = value; }
        }

        Color _oddLineBkColor = DEFAULT_ODD_LINE_BK_COLOR;
        Color _evenLineBkColor = DEFAULT_EVEN_LINE_BK_COLOR;

        internal class PrtData
        {
            internal int _idxInTab;
            internal int _lineInReport;//Line No. after 1st data line. Start from 1.
            internal int _colInReport;//Column No.
            internal Color _bagroundColor = DEFAULT_EVEN_LINE_BK_COLOR;
            internal float _width = 0.2f;
            internal float _left = 0f;

            internal PrtData(int idxInTab, int lineInReport, int colInReport,
                Color bagroundColor)
            {
                this._colInReport = colInReport;
                this._bagroundColor = bagroundColor;
                this._idxInTab = idxInTab;
                this._lineInReport = lineInReport;
            }
        }

        internal bool GetPrtData(int lineNo, int colNo, out PrtData data)
        {
            bool hasData = false;
            data = null;
            foreach (PrtData data1 in this.ListPrtData)
            {
                if (data1._lineInReport == lineNo && data1._colInReport == colNo)
                {
                    data = data1;
                    hasData = true;
                    break;
                }
            }
            return hasData;
        }

        internal class PrtDataComparerByPositionInReport:IComparer<PrtData>
        {
            #region IComparer<PrtData> Members

            public int Compare(PrtData x, PrtData y)
            {
                int result = x._lineInReport - y._lineInReport;
 	            if ( result == 0 ) result = x._colInReport - y._colInReport;
                if (result == 0) result = x._idxInTab - y._idxInTab;
                return result;
            }

            #endregion
        }

        public PrtMultiLineData(int frColNo, int toColNo)
        {
            if (frColNo < 0) throw new ApplicationException("Invalid Start Position.");
            if (toColNo < frColNo) throw new ApplicationException("'End Position' must be after 'Start Position'");
            this.StartColNo = frColNo;
            this.MaxColNo = toColNo;
        }

        int firstLineCol = 0;
        public void AddFor1stLine(int idxInTab)
        {
            if (IsIdxInTabExisted(idxInTab)) throw new ApplicationException("Index was already existed.");
            this._lsData.Add(new PrtData(idxInTab, 0, firstLineCol++, Color.Empty));
            if (MaxLineNo < 1) _maxLineNo = 1;
        }

        public void AddForMultiLine(int idxInTab, Color bagroundColor)
        {
            
            if (_lsData.Count < 1)
            {
                _lastLineNo = 1;
                _lastColNo = _startColNo;
            }
            else
            {
                if (IsIdxInTabExisted(idxInTab)) throw new ApplicationException("Index was already existed.");
                _lastColNo++;
                if (_lastColNo > this.MaxColNo)
                {
                    _lastLineNo++;
                    _lastColNo = _startColNo;
                }
            }
            _maxLineNo = _lastLineNo + 1;

            this._lsData.Add(new PrtData(idxInTab, _lastLineNo, _lastColNo, bagroundColor));
        }

        internal bool IsIdxInTabExisted(int idxInTab, out PrtData prtData)
        {
            bool existed = false;
            prtData = null;
            foreach( PrtData data in this._lsData)
            {
                if (idxInTab == data._idxInTab)
                {
                    existed = true;
                    prtData = data;
                    break;
                }
            }

            return existed;
        }

        public bool IsIdxInTabExisted(int idxInTab)
        {
            bool existed = false;
            foreach (PrtData data in this._lsData)
            {
                if (idxInTab == data._idxInTab)
                {
                    existed = true;
                    break;
                }
            }

            return existed;
        }
    }
}
