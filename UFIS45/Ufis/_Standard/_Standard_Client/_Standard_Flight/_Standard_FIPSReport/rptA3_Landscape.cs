using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Drawing;
using Ufis.Utils;

namespace FIPS_Reports
{
	public class rptA3_Landscape : ActiveReport3
	{
		#region _My Members
		/// <summary>
		/// Stores the main header.
		/// </summary>
		private string _header1 = "";
		/// <summary>
		/// Stores the sub header string
		/// </summary>
		private string _header2 = "";
		/// <summary>
		/// Stores the data types for each column.
		/// This might be necessary for type convertions.
		/// </summary>
		private string [] arrTypes;
		/// <summary>
		/// The reference to the tab control, which is
		/// a parameter of the constructor.
		/// </summary>
		private AxTABLib.AxTAB tabResult;
		/// <summary>
		/// The tableResult for report output
		/// </summary>
		private AxTABLib.AxTAB tabReportResult;
		/// <summary>
		/// The tableResult for excel export
		/// </summary>
		private AxTABLib.AxTAB tabExcelResult;
		/// <summary>
		/// Stores the current line number. This is necessary
		/// to identif when EOF in the xxx_fetchdata method must
		/// return end of report. The current value is checked 
		/// against the Total line count of the tab control.
		/// </summary>
		private int currLine = 0;
		/// <summary>
		/// Stores the header text in an array. This is filled
		/// in the constructor.
		/// </summary>
		string [] arrHeader;
		/// <summary>
		/// The length list from the tab control. This is generated
		/// in the constructor.
		/// </summary>
		string [] arrLens;
		/// <summary>
		/// The length list from the tab control used for report.
		/// </summary>
		string [] arrReportLens;
		/// <summary>
		/// The length list from the tab control used for excel export. This is generated
		/// in the SetExcelExportHeaderLenghtString() function.
		/// </summary>
		string [] arrExcelLens;
		/// <summary>
		/// Stores the logical field list of the tab control. This 
		/// names are used for the dynamically generated report controls.
		/// </summary>
		string [] arrNames; 
		/// <summary>
		/// The font size for the detail section.
		/// </summary>
		int imFontSize = 10;
		/// <summary>
		/// Defines whether a txtbox can grow or not
		/// </summary>
		bool bmFieldCanGrow = true;
		/// <summary>
		/// Property, to allow/forbid gorwing of a textbox according to the content.
		/// </summary>
		bool blFirst = true;
		/// <summary>
		/// In excel export report is run twice, hence to return back to normal print width this variable is used.
		/// </summary>
		float fmReportPrintWidth;		
		/// <summary>
		/// In excel export length of few fields increased, hence report print width also needs to be increased.
		/// </summary>
		float fmExcelPrintWidth = 0;
		/// <summary>
		/// In excel export length of header columns are more than normal report output, hence this is set
		/// in SetExcelExportHeaderLenghtString(...) function.
		/// </summary>
		string strExcelHeaderLengthString = "";
		
		public bool TextBoxCanGrow
		{
			get{return bmFieldCanGrow;}
			set{bmFieldCanGrow = value;}
		}

		/// <summary>
		/// PageSupport of printer
		/// </summary>
		private System.Drawing.Printing.PaperKind reportPaperKind;

		float maxHeight = 0F;
		
		private int imTotalNoOfHeaderControls;

		#endregion _My Members

		/// <summary>
		/// Constructor:
		/// The tab control is a parameter. This tab is used to generate
		/// the report dynamically.
		/// </summary>
		/// <param name="pTab">The tab control with settings and data</param>
		/// <param name="header1">The main header string</param>
		/// <param name="header2">Additional header string</param>
		/// <param name="fieldtypes">The types for the fields. This might be
		/// necessary to convert data to the respective format.
		/// </param>
		public rptA3_Landscape(AxTABLib.AxTAB pTab, string header1, string header2, string fieldtypes, int fontSize)
		{
			imFontSize = fontSize;
			_header1 = header1;
			_header2 = header2;
			arrTypes = fieldtypes.Split(',');
			tabExcelResult = tabResult = tabReportResult = pTab;
			arrHeader = tabResult.HeaderString.Split(',');						
			arrExcelLens = arrLens = arrReportLens = tabResult.HeaderLengthString.Split(',');
			arrNames = tabResult.LogicalFieldList.Split(',');
			InitializeReport();
			InitializeEventHandler();
			ReportHeader.Visible = false;
			ReportFooter.Visible = false;
			CalculateNoOfHeaderControls();

			if(imTotalNoOfHeaderControls > 42)
				imFontSize = 6;

			fmReportPrintWidth = this.PrintWidth;
           	reportPaperKind = this.PageSettings.PaperKind;
      	}

		/// <summary>
		/// Called when the report starts. This function must call the 
		/// methods, which generate the header-, detail- and footer sections.
		/// </summary>
		/// <param name="sender">Originator of the call. This is the report itself.</param>
		/// <param name="eArgs">Event arguments.</param>
		private void rptA3_Landscape_ReportStart(object sender, System.EventArgs eArgs)
		{				
			if(Helper.IsPageSupportedByPrinter(reportPaperKind,this.Document.Printer,false) == false)
			{
				this.Document.Printer.PrinterName = "";
				
			}

			currLine = 0;
			DateTime d		= DateTime.Now;
			txtPageDate.Text	= d.ToString();
			lblPageHeader1.Text = _header1;
			lblPageHeader2.Text = _header2;

			txtReportDate.Text = d.ToString();
			lblReportHeader1.Text = _header1;
			lblReportHeader2.Text = _header2;

			//*** Read the logo if exists and set it
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strPrintLogoPath = myIni.IniReadValue("GLOBAL", "PRINTLOGO");
			Image myImage = null;
			try
			{
				if(strPrintLogoPath != "")
				{
					myImage = new Bitmap(strPrintLogoPath);
					Picture1.Image = myImage;
					Picture2.Image = myImage;
				}
			}
			catch(Exception)
			{
			}
			
			if(ReportHeader.Visible == true)
			{
				tabResult = tabExcelResult;				
				this.PrintWidth = fmExcelPrintWidth;
				arrLens = arrExcelLens;
			}
			else
			{
				tabResult = tabReportResult;
				this.PrintWidth = fmReportPrintWidth;
				arrLens = arrReportLens;
			}
			MakeReportHeader();			
			MakeReportDetail();
		}

		/// <summary>
		/// Generates the Detail section of the report according
		/// to the tab control.
		/// </summary>
		private void MakeReportDetail()
		{
			Detail.Controls.Clear();
			float ilX = 0;
			float ilCurrW = 0f;
			DataDynamics.ActiveReports.Border bd = new Border();
			System.Drawing.Font fnt = new System.Drawing.Font("Arial", imFontSize, System.Drawing.FontStyle.Regular);

			float ilHeight = 0.2f;



			for(int i = 0; i < arrHeader.Length; i++)
			{
				if(Convert.ToInt32(arrLens[i]) > 0)
				{
					ilCurrW = PageHeader.Visible == true ? PixToInch (Convert.ToInt32(arrLens[i])) : PixToInch (Convert.ToInt32(arrExcelLens[i]));
					float ilCurrWSave =  ilCurrW;

					ilHeight = 0.2f;	

					ilCurrW = (ilCurrW / 8) * imFontSize;

					if(ilCurrW > ilCurrWSave)
					{
						ilCurrW = ilCurrWSave;
					}
					else
					{
						ilHeight = (ilHeight / 8) * imFontSize;
					}
											
					DataDynamics.ActiveReports.TextBox  txtBox = new TextBox();
					DataDynamics.ActiveReports.Line olLine = new Line();

					Detail.Height = ilHeight;
					txtBox.Font = fnt;
					txtBox.Top = 0;
					txtBox.Height = ilHeight;
					txtBox.Left = ilX;
					txtBox.Width = ilCurrW;
					txtBox.CanGrow = bmFieldCanGrow;					
					txtBox.WordWrap = false;
					olLine.Y1 = 0;
					olLine.Y2 = ilHeight;
					olLine.X1 = ilX;
					olLine.X2 = ilX;
					olLine.Name = "lin" + arrNames[i];
					ilX += ilCurrW;
//					txtBox.Border.BottomStyle = BorderLineStyle.Solid;
//					txtBox.Border.LeftStyle = BorderLineStyle.Solid;
//					txtBox.Border.TopStyle  = BorderLineStyle.Solid;
//					txtBox.Border.RightStyle = BorderLineStyle.Solid;
					txtBox.Name = "txt" + arrNames[i];
					txtBox.Text = "txt" + arrNames[i];
					Detail.Controls.Add(txtBox);
					Detail.Controls.Add(olLine);
				}
			}
			DataDynamics.ActiveReports.Line olLineBottom = new Line();
			olLineBottom.X1 = 0;
			olLineBottom.X2 = ilX;//+ilCurrW;
			olLineBottom.Y1 = ilHeight;
			olLineBottom.Y2 = ilHeight;
			olLineBottom.Name = "linBottom";
			Detail.Controls.Add(olLineBottom);
			//Last vertical separator on the right end to finish the line
			DataDynamics.ActiveReports.Line olLastVertical = new Line();
			olLastVertical.Y1 = 0;
			olLastVertical.Y2 = ilHeight;//+ilCurrW;
			olLastVertical.X1 = ilX;
			olLastVertical.X2 = ilX;
			olLastVertical.Name = "linLast";
			Detail.Controls.Add(olLastVertical);
		}
		/// <summary>
		/// Generates the report Header from the tab
		/// </summary>
		private void MakeReportHeader()
		{
			float ilX = 0;
			//DataDynamics.ActiveReports.Border bd = new Border();
			System.Drawing.Font fnt = new System.Drawing.Font("Arial", imFontSize, System.Drawing.FontStyle.Regular);
			for(int i = 0; i < arrHeader.Length; i++)
			{
				float ilCurrW = PageHeader.Visible == true ? PixToInch((Convert.ToInt32(arrLens[i]))) : PixToInch((Convert.ToInt32(arrExcelLens[i])));
				float ilCurrWSave = ilCurrW;
				
				float ilHeight = 0.2f;

				ilCurrW = (ilCurrW / 8) * imFontSize;

				ilHeight = 0.2f;
				
				if(ilCurrW > ilCurrWSave)
				{
					ilCurrW = ilCurrWSave;
				}
				else
				{
					ilHeight = (ilHeight / 8) * imFontSize;
				}

				if(ilCurrW > 0)
				{
					Label lbl = new Label();
					lbl.Font = fnt;
					lbl.Top = 1;
					lbl.Height = 0.2f;
					lbl.Left = ilX;
					lbl.Width = ilCurrW;
					lbl.Alignment = TextAlignment.Center;
					ilX += ilCurrW;
					lbl.Border.BottomStyle = BorderLineStyle.Solid;
					lbl.Border.LeftStyle = BorderLineStyle.Solid;
					lbl.Border.TopStyle  = BorderLineStyle.Solid;
					lbl.Border.RightStyle = BorderLineStyle.Solid;
					lbl.Name = "lbl" + arrNames[i];
					lbl.Text = arrHeader[i];
					lbl.BackColor = Color.LightGray;
					if(ReportHeader.Visible == true && ReportHeader.Controls.Count < imTotalNoOfHeaderControls)
					{
						ReportHeader.Controls.Add(lbl);
					}
					if(PageHeader.Visible == true && PageHeader.Controls.Count < imTotalNoOfHeaderControls)
					{
						PageHeader.Controls.Add(lbl);
					}
				}
			}
		}
		/// <summary>
		/// Converts a pixel integer into inches
		/// </summary>
		/// <param name="pix">Input parameter. The number of pixels.</param>
		/// <returns>The converted pixel in inches.</returns>
		private float PixToInch(int pix)
		{
			float fRet = 0f;
			float twips = 0f;
			//First we need twips
			twips = pix * 16f; //Normally it's 15 but 16 meets better!
			//1440 twips = 1 inch
			fRet = (float)(twips / 1440f);
			return fRet;
		}
		private void rptA3_Landscape_FetchData(object sender, DataDynamics.ActiveReports.ActiveReport3.FetchEventArgs eArgs)
		{
			Detail.Height = 0.2f;
			if(tabResult.GetLineCount() == 0)
			{
				eArgs.EOF = true;
				return;
			}

			if(currLine < tabResult.GetLineCount())
			{
				TextBox txt;				
				string [] arrValues = tabResult.GetLineValues(currLine).Split(',');
				for(int i = 0; i < arrNames.Length; i++)
				{
                    try
                    {
                        //GFO added 02/02/2011 
                        //ATH-50
                        //Index out of bounds 
                        //Sometimes the Array with the Values does not have 
                        //the same items as the Array with the names
                        if (i < arrValues.Length)
                        {
                            if (Convert.ToInt32(arrLens[i]) > 0)
                            {
                                string strName = "txt" + arrNames[i];
                                txt = (TextBox)Detail.Controls[strName];
                                txt.Text = arrValues[i].Length > 1 ? arrValues[i] : " " + arrValues[i];

                                if (txt.Height > maxHeight)
                                    maxHeight = txt.Height;
                            }
                        }
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                       String message =ex.Message;
                    }

				}

				currLine++;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void Detail_BeforePrint(object sender, System.EventArgs eArgs)
		{			
			maxHeight = Detail.Height;
			TextBox textBox;
			string strName;			
			Line lin;
			for(int i = 0; i < arrNames.Length; i++)
			{
				if(Convert.ToInt32(arrLens[i]) > 0)
				{
					strName = "lin" + arrNames[i];
					lin = (Line)Detail.Controls[strName];
					lin.Y2 = maxHeight;
				}
				if(Convert.ToInt32(arrLens[i]) > 0 && Detail.Height > 0.2f)
				{					
					strName = "txt" + arrNames[i];					
					textBox = (TextBox)Detail.Controls[strName];					
					textBox.WordWrap = true;
				}
			}
			lin = (Line)Detail.Controls["linLast"];
			lin.Y2 = maxHeight;
		}

		/// <summary>
		/// Prints the bottom line in detail section
		/// </summary>
		private void Detail_AfterPrint(object sender, System.EventArgs eArgs)
		{				
			Line lin;
			lin = (Line)Detail.Controls["linBottom"];	
			lin.Y1 = maxHeight;
			lin.Y2 = maxHeight;
		}

		/// <summary>
		/// Calculates the no of header controls
		/// </summary>
		private void CalculateNoOfHeaderControls()
		{
			int ilCount = 0;
			for(int i = 0; i < arrHeader.Length; i++)
			{
				float ilCurrW = PixToInch (Convert.ToInt32(arrLens[i]));
				if(ilCurrW > 0)
				{
					ilCount++;
				}
			}
			imTotalNoOfHeaderControls = PageHeader.Controls.Count + ilCount;
		}

		/// <summary>
		/// Sets the tabResult for excel export
		/// </summary>
		public void SetExcelExportTabResult(AxTABLib.AxTAB tabResult)
		{
			tabExcelResult = tabResult;
		}

		/// <summary>
		/// Sets the length of the headers to be exported to excel
		/// </summary>
		public void SetExcelExportHeaderLenghtString(string strHeaderLengthString)
		{
			strExcelHeaderLengthString = strHeaderLengthString;
			arrExcelLens = strExcelHeaderLengthString.Split(',');
			for(int i = 0; i < arrExcelLens.Length; i++)
			{
				fmExcelPrintWidth += PixToInch((Convert.ToInt32(arrExcelLens[i])));
			}
		}
		
		/// <summary>
		/// Initializes the event handler.
		/// </summary>
		private void InitializeEventHandler()
		{
			this.Document.Printer.BeginPrint += new System.Drawing.Printing.PrintEventHandler(Printer_BeginPrint);
		}

		/// <summary>
		/// Starts the print operation to the printer
		/// </summary>
		private void Printer_BeginPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			DataDynamics.ActiveReports.Document.Printer printer = (DataDynamics.ActiveReports.Document.Printer)sender;
			if(Helper.IsPageSupportedByPrinter(reportPaperKind,printer,true) == false)
			{				
				e.Cancel = true;
			}
			else
			{				
				printer.PaperKind = reportPaperKind;
			}
		}
		#region ActiveReports Designer generated code
		private ReportHeader ReportHeader = null;
		private Picture Picture1 = null;
		private Label lblReportHeader1 = null;
		private Label lblReportHeader2 = null;
		private PageHeader PageHeader = null;
		private Picture Picture2 = null;
		private Label lblPageHeader1 = null;
		private Label lblPageHeader2 = null;
		public Detail Detail = null;
		private PageFooter PageFooter = null;
		private TextBox txtPageDate = null;
		private Label Label34 = null;
		private Label Label35 = null;
		private TextBox TextBox4 = null;
		private TextBox TextBox5 = null;
		private Label Label36 = null;
		private ReportFooter ReportFooter = null;
		private TextBox txtReportDate = null;
		private Label Label41 = null;
		private Label Label42 = null;
		private TextBox TextBox9 = null;
		private TextBox TextBox10 = null;
		private Label Label43 = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "FIPS_Reports.rptA3_Landscape.rpx");
			this.ReportHeader = ((DataDynamics.ActiveReports.ReportHeader)(this.Sections["ReportHeader"]));
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.ReportFooter = ((DataDynamics.ActiveReports.ReportFooter)(this.Sections["ReportFooter"]));
			this.Picture1 = ((DataDynamics.ActiveReports.Picture)(this.ReportHeader.Controls[0]));
			this.lblReportHeader1 = ((DataDynamics.ActiveReports.Label)(this.ReportHeader.Controls[1]));
			this.lblReportHeader2 = ((DataDynamics.ActiveReports.Label)(this.ReportHeader.Controls[2]));
			this.Picture2 = ((DataDynamics.ActiveReports.Picture)(this.PageHeader.Controls[0]));
			this.lblPageHeader1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
			this.lblPageHeader2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
			this.txtPageDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[0]));
			this.Label34 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
			this.Label35 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[2]));
			this.TextBox4 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[3]));
			this.TextBox5 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[4]));
			this.Label36 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[5]));
			this.txtReportDate = ((DataDynamics.ActiveReports.TextBox)(this.ReportFooter.Controls[0]));
			this.Label41 = ((DataDynamics.ActiveReports.Label)(this.ReportFooter.Controls[1]));
			this.Label42 = ((DataDynamics.ActiveReports.Label)(this.ReportFooter.Controls[2]));
			this.TextBox9 = ((DataDynamics.ActiveReports.TextBox)(this.ReportFooter.Controls[3]));
			this.TextBox10 = ((DataDynamics.ActiveReports.TextBox)(this.ReportFooter.Controls[4]));
			this.Label43 = ((DataDynamics.ActiveReports.Label)(this.ReportFooter.Controls[5]));
			// Attach Report Events
			this.ReportStart += new System.EventHandler(this.rptA3_Landscape_ReportStart);
			this.FetchData += new DataDynamics.ActiveReports.ActiveReport3.FetchEventHandler(this.rptA3_Landscape_FetchData);
			this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
			this.Detail.AfterPrint += new System.EventHandler(this.Detail_AfterPrint);
		}

		#endregion
	}
}
