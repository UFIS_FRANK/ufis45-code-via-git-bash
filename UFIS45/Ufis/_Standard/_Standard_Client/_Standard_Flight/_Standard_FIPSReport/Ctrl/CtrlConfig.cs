﻿using System;
using System.Collections.Generic;
using System.Text;

using Ufis.Utils;

namespace FIPS_Reports.Ctrl
{
    public class CtrlConfig
    {

        private static Dictionary<string, string> _mapConfigInfo = null;

        public static Dictionary<string, string> MapConfigInfo
        {
            get
            {
                if (CtrlConfig._mapConfigInfo == null) CtrlConfig._mapConfigInfo = new Dictionary<string, string>();
                return CtrlConfig._mapConfigInfo;
            }
        }

        public static string GetConfigInfo(string configFileName, 
            string stSection, string stKey, string stDefaultValue)
        {
            string info = stDefaultValue;

            string stMapKey = string.Format(@"{0}///{1}///{2}",
                configFileName, stSection, stKey);

            if (MapConfigInfo.ContainsKey(stMapKey))
            {
                info = MapConfigInfo[stMapKey];
            }
            else
            {
                
                IniFile myIni = new IniFile(configFileName);
                
                try
                {
                    info = myIni.IniReadValue(stSection, stKey);
                    if (info==null) info = "";
                    MapConfigInfo.Add(stMapKey, info);
                }
                catch { }
            }

            return info;
        }


 
    }
}
