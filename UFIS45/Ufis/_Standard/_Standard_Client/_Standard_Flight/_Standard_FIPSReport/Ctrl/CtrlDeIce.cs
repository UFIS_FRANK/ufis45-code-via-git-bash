﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Threading;
using System.ComponentModel;

using Ufis.Data;
using Ufis.Utils;
//using Ufis.Lib;

namespace FIPS_Reports.Ctrl
{
    public class CtrlDeIce
    {
        string _stDeIceConfigFileName = "";
        string _stDeIceConfigSection = "DE-ICING";
        static CtrlDeIce _this = null;
        static AxDeicingClass.DeicingDialogClass _deIcing = null;
        static bool _deIcingShow = false;
        static readonly object _locker = new object();
        static BackgroundWorker _bw;
        static AxAATLOGINLib.AxAatLogin _loginControl = null;
        public const string CEDA_INI_CONFIG = "DE_ICE";

        enum EnumDeIcingIntiStatus { NotInit, InProgress, Initialized };
        EnumDeIcingIntiStatus _initStat = EnumDeIcingIntiStatus.NotInit;

        public void Init(AxAATLOGINLib.AxAatLogin loginControl)
        {
            if (_initStat == EnumDeIcingIntiStatus.NotInit)
            {
                lock (_locker)
                {
                    if (_initStat == EnumDeIcingIntiStatus.NotInit)
                    {
                        _loginControl = loginControl;
                        InitInternal2();                       
                    }
                }
            }

            
        }


        private void InitInternal2()
        {
            if (_deIcing == null)
            {
                _deIcing = new AxDeicingClass.DeicingDialogClass();
                _deIcing.AppName = _loginControl.ApplicationName;// "FIPS-Report";
                _deIcing.AppVersion = _loginControl.InfoAppVersion;// "4.6.1.9";
                //deIcing.IniFilePath = @"d:\Ufis\System\De-icing.ini";
                _deIcing.HandleBc = true;
                AxDeicingClass.TimeZoneEnum timeZone = AxDeicingClass.TimeZoneEnum.UTC;
                _deIcing.set_TimeZone(ref timeZone);
                _deIcing.UserName = _loginControl.GetUserName();// "UFIS$ADMIN";
                //_deIcing.ShowSplashScreen = false;

                _deIcing.LoadBasicData();
                _initStat = EnumDeIcingIntiStatus.Initialized;
            }
        }

        void _bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _initStat = EnumDeIcingIntiStatus.Initialized;
        }

        void _bw_DoWork(object sender, DoWorkEventArgs e)
        {
            if (_deIcing == null)
            {
                _deIcing = new AxDeicingClass.DeicingDialogClass();
                _deIcing.AppName = _loginControl.ApplicationName;// "FIPS-Report";
                _deIcing.AppVersion = _loginControl.InfoAppVersion;// "4.6.1.9";
                //deIcing.IniFilePath = @"d:\Ufis\System\De-icing.ini";
                _deIcing.HandleBc = true;
                AxDeicingClass.TimeZoneEnum timeZone = AxDeicingClass.TimeZoneEnum.UTC;
                _deIcing.set_TimeZone(ref timeZone);
                _deIcing.UserName = _loginControl.GetUserName();// "UFIS$ADMIN";
                //_deIcing.ShowSplashScreen = false;
                //_deIcing.
                _deIcing.LoadBasicData();
            }
        }

        private void InitInternal()
        {
            if (_deIcing == null && (_initStat!=EnumDeIcingIntiStatus.Initialized))
            {
                _bw = new BackgroundWorker();
                _bw.WorkerReportsProgress = true;
                _bw.DoWork += new DoWorkEventHandler(_bw_DoWork);
                _bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_bw_RunWorkerCompleted);
                _bw.ProgressChanged += new ProgressChangedEventHandler(_bw_ProgressChanged);
                _bw.RunWorkerAsync();
                _initStat = EnumDeIcingIntiStatus.InProgress;
            }
        }

        void _bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Console.WriteLine("Progress");
        }

        public static CtrlDeIce GetInstance()
        {
            if (_this == null)
            {
                _this = new CtrlDeIce();
            }

            return _this;
 
        }

        public void Show()
        {
            while (_initStat != EnumDeIcingIntiStatus.Initialized)
            {
                Thread.Sleep(10000);
            }
            
            _deIcing.ShowDialog(false);
            
        }

        public bool IsAlwaysSort()
        {
            return (GetDeIceConfigInfo("ALWAYS_SORT", "NO") == "YES");
        }

        private string GetDeIceConfigInfo(string stKey, string stDefaultValue)
        {
            return CtrlConfig.GetConfigInfo(_stDeIceConfigFileName,
              _stDeIceConfigSection, stKey, stDefaultValue);
        }


        public void LoadData()
        {
            string stDataSource = GetDeIceConfigInfo("DATA_SOURCE", "AFTTAB");
            string stWhere = "";
            Dictionary<string, string> mapParams = GetDefaultParamCollection();

        //    if(stDataSource.Contains("AFTTAB,"))
        //    {
        //        strWhere = Util.BuildWhereClause(colViewParams);
                
        //mapParams.Add( "URNO", "OrderByClause");
        //mapParams.Add (strWhere, "WhereClause");
        
        //pstrAftUrnoList = AddDataSource(colDataSources, "AFTTAB", colLocalParams, "URNO");
        
        //Set rsAft = colDataSources.Item("AFTTAB")
        //If Not (rsAft Is Nothing) Then
        //    If Not (rsAft.BOF And rsAft.EOF) Then
        //        Call ReadDataSource(pstrAftUrnoList)
        //    End If
        //End If
        
        //If colDataSources.Exists("ICETAB") Then
        //    Set rsDeicing = colDataSources.Item("ICETAB")
        //End If
        //    }
        }

        

//        public string AddDataSource(string DataSourceName , 
//Dictionary<string,string> colParameters , string KeyColumn, bool UseTabObject ,//As Boolean = False, 
//bool SplitWhereClause,// As Boolean = False, 
//            string Separator,// As String = "|",
//        bool OverwriteDataSource //As Boolean = True
//            )
//        {

//    String strDataTableName;
//    String strDataFields;
//    String strDataTypes;
//    String strDataSizes;
//    String strDataOrder;
//    String[] vntDataWhere ;
//    String strKeyList ;
    
//    strKeyList = "";
    
//    strDataTableName = CtrlConfig.GetConfigInfo( _stDeIceConfigFileName, "DATA_SOURCES", DataSourceName & "_TABLE", DataSourceName);
//    strDataFields = CtrlConfig.GetConfigInfo( _stDeIceConfigFileName,  "DATA_SOURCES", DataSourceName & "_FIELDS", "");
//    strDataTypes =CtrlConfig.GetConfigInfo( _stDeIceConfigFileName,  "DATA_SOURCES", DataSourceName & "_TYPES", "");
//    strDataSizes = CtrlConfig.GetConfigInfo( _stDeIceConfigFileName,  "DATA_SOURCES", DataSourceName & "_SIZES", "");
//    strDataOrder = EvaluateParameters(colParameters, 
//        GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_ORDER", ""));
//    vntDataWhere = EvaluateParameters(colParameters, 
//        GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_WHERE", ""), SplitWhereClause, Separator);
//    IDatabase myDb = UT.GetMemDB();
//            ITable myTab = myDb[DataSourceName];
//            myDb.Unbind( DataSourceName );
//            myTab = myDb.Bind( DataSourceName, strDataTableName, strDataFields, strDataSizes, strDataFields );
//            if (strDataOrder.Trim()!="")
//            {
//            myTab.Sort( strDataOrder );
//            }
//            myTab.Load(strw

//        If IsArray(vntDataWhere) Then
//            .WhereClause = vntDataWhere
//        Else
//            If vntDataWhere <> "" Then
//                .WhereClause = vntDataWhere
//            End If
//        End If
//        If strDataTypes <> "" Then
//            .ColumnTypeList = strDataTypes
//        End If
//        If strDataSizes <> "" Then
//            .ColumnSizeList = strDataSizes
//        End If
        
//        If Not IsMissing(KeyColumn) Then
//            .KeyColumn = KeyColumn
//            .PopulateKeyStringList = True
//        End If
        
//        Set rsResult = .CreateRecordset(UseTabObject)
//        If strDataOrder <> "" Then
//            rsResult.Sort = strDataOrder
//        End If
//        If Not IsMissing(KeyColumn) Then
//            strKeyList = .KeyStringList
//        End If
        
//        If colDataSources.Exists(DataSourceName) Then
//            If OverwriteDataSource Then
//                colDataSources.Remove DataSourceName
                
//                colDataSources.Add rsResult, DataSourceName
//            Else
//                Call CombineDataSource(colDataSources.Item(DataSourceName), rsResult)
//            End If
//        Else
//            colDataSources.Add rsResult, DataSourceName
//        End If
//    End With
    
//    AddDataSource = strKeyList
//    }

        private DateTime GetDatePart(string ufisDateTime)
        {
            return new DateTime(Convert.ToInt32(ufisDateTime.Substring(0, 4)),
                                          Convert.ToInt32(ufisDateTime.Substring(4, 2)),
                                          Convert.ToInt32(ufisDateTime.Substring(6, 2)));
        }

        private TimeSpan GetTimeSpan(string hhmm )
        {
            int hh = Convert.ToInt32( hhmm.Substring(0,2));
            int mm = 0;
            int len = hhmm.Length - 2;
            string stMM = hhmm.Substring(2, len);
            if (!Int32.TryParse(stMM, out mm))
            {
                len--;
                if (len>0)
                {
                mm = Convert.ToInt32(hhmm.Substring(3,len));
                }else{ mm = 0;}
            }
            return new TimeSpan( hh, mm, 0); 
        }

        /*
        private string BuildWhereClause(Dictionary<string, string> mapParams)
        {
            string strWhere = "";
            DateTime dtNow = DateTime.Now;

            bool isUtcTimeZone = true;

            if (isUtcTimeZone) dtNow = UT.LocalToUtc(dtNow);

            string strRelStart = mapParams["RELPRDSTART"];

            string strRelEnd = mapParams["RELPRDEND"];

            string strStartDate = mapParams["STARTDATE"];
            string strStartTime = mapParams["STARTTIME"];
            string strEndDate = mapParams["ENDDATE"];
            string strEndTime = mapParams["ENDTIME"];

            List<DateTime> vntDates = GetParamDateArray(strStartDate, strStartTime, strEndDate, strEndTime, strRelStart, strRelEnd, dtNow);
            DateTime dtmStartDate = vntDates[0];
            DateTime dtmEndDate = vntDates[1];
            string strParamVal = "";
            string strDays = "";

            //Check DAYS
            if (isUtcTimeZone)
            {
                strParamVal = mapParams["DAYS"];
                if (strParamVal != "" && (strParamVal.Length < 7))
                {
                    strDays = "";
                    for (int i = 0; i < (strParamVal.Length); i++)
                    {
                        strDays = strDays + ",'" + strParamVal.Substring(i, 1) + "'";
                    }
                    if (strDays != "") strDays = strDays.Substring(2);
                    strWhere = strWhere + "AND DOOD IN (" + strDays + ")";

                }
            }

            //check ALC2
            string strTemp =Util.BuildSimpleWhereClause(mapParams, "ALC2", "ALC2");
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }

            //check FLTN
            strTemp = Util.BuildSimpleWhereClause(mapParams, "FLTN", "FLTN");
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }

            //check FLNS
            strTemp = Util.BuildSimpleWhereClause(mapParams, "FLNS", "FLNS");
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }

            //check AIRLINE
            strTemp = Util.BuildExtendedWhereClause(mapParams, "AIRLINE",
                new List<string>{"ALC2", "ALC3"}, new List<int>{2, 3}, true);
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }

            //check CSGN
            strTemp = Util.BuildSimpleWhereClause(mapParams, "CSGN", "CSGN");
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }

            //check FLTI
            strTemp = Util.BuildSimpleWhereClause(mapParams, "FLTI", "FLTI");
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }

            //check REGN
            strTemp = Util.BuildSimpleWhereClause(mapParams, "REGN", "REGN", true);
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }

            //check ACTY
            strTemp = Util.BuildExtendedWhereClause(mapParams, "ACTY",
                new List<string>{"ACT3", "ACT5"}, new List<int>{3, 5}, true);
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }

            //check AIRP
            strTemp = Util.BuildExtendedWhereClause(mapParams, "AIRP",
                new List<string>{"DES3", "DES4"}, new List<int>{3, 4}, true);
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }

            //check VIAP
            strTemp = Util.BuildExtendedWhereClause(mapParams, "VIAP",
                new List<string>{"VIA3", "VIA4"}, new List<int>{3, 4}, true);
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }

            //check NATURE
            strTemp = Util.BuildSimpleWhereClause(mapParams, "NATURE", "TTYP", true);
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }

            //check STEV
            strTemp = Util.BuildSimpleWhereClause(mapParams, "STEV", "STEV", true);
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }

            //check HDLAGENT
            strParamVal = mapParams["HDLAGENT"];
            if (strParamVal.Trim() != "")
            {
                string[] vntParamVal = strParamVal.Split(',');
                strParamVal = "";
                for (int i = 0; i< vntParamVal.Length; i++)
                {
                    strParamVal += CtrlHandlingAgent.GetInstance().GetAirLineCodesForHandlingAgentInString(vntParamVal[i], ",", "'");
                }
                if (strParamVal != "")
                {
                    strParamVal = strParamVal.Substring(1);
                    strWhere = strWhere + " AND ALC2 IN (" + strParamVal + ")";
                }
            }

            //check HTYP
            strTemp = Util.BuildSimpleWhereClause(mapParams, "HTYP", "HTYP", true);
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }

            //check PSTN/PSGR
            //check PSTN
            strParamVal = mapParams["PSTN"];
            if (strParamVal != "")
            {
                strParamVal = ",'" + strParamVal.Replace(",", "','") + "'";
            }
            //check PSGR
            strTemp = mapParams["PSGR"];
            if (strTemp != "")
            {
                string[] vntParamVal = strTemp.Split(',');
                for (int i = 0; i < vntParamVal.Length; i++)
                {
                    strParamVal += CtrlAircraftPosition.GetInstance().GetPositionsForAircraftGroupNameInString(vntParamVal[i], ",", "'");
                }
                if (strParamVal != "")
                {
                    strParamVal = strParamVal.Substring(1);
                    strWhere = strWhere + " AND PSTD IN (" + strParamVal + ")";
                }
            }

            strTemp = Util.BuildSimpleWhereClause(mapParams, "PSTN", "PSTD", true);
            if (strTemp != "") { strWhere = strWhere + " " + strTemp; }



            //check FTYP
            strParamVal = Util.GetParamValList(mapParams, new List<string> { "O", "S", "X", "N", "D", "R", "P", "G", "T" },
               new List<string> { "O,Z,B", "S", "X", "N", "D", "R", "G", " ", "T" });
            if (strParamVal != ""){
                strParamVal = "'" + strParamVal.Replace(",", "','") + "'";
                strWhere = strWhere + " AND FTYP IN (" + strParamVal + ")";
            }

            dtmStartDate = UT.LocalToUtc(dtmStartDate);
            dtmEndDate = UT.LocalToUtc(dtmEndDate);
            strStartDate = UT.DateTimeToCeda(dtmStartDate);
            strEndDate = UT.DateTimeToCeda(dtmEndDate);

            ////add to paramcollection -> UTC START/END Date
            //mapParams.Add dtmStartDate, "UTCSTARTDATE"
            //mapParams.Add dtmEndDate, "UTCENDDATE"

            return "((TIFD BETWEEN '" + strStartDate + "' AND '" + strEndDate + "') OR " +
                "(STOD BETWEEN '" + strStartDate + "' AND '" + strEndDate + "')) AND " +
                "AIRB = ' ' AND ADID = 'D' " + strWhere;
            

        }
        */

  
        private List<DateTime> GetParamDateArray(
            string startDate, string startTime,
            string endDate, string endTime,
            string relStart, string relEnd,
            DateTime relDateBase)
        {
            List<DateTime> dtmDate = new List<DateTime>();
            DateTime defaultStartDate, defaultEndDate;
            DateTime dtNow = DateTime.Now;

            //if(IsMissing(defaultStartDate) )
            {
                if (startDate != "")
                {
                    defaultStartDate = GetDatePart(startDate);

                }
                else
                {
                    defaultStartDate = dtNow.Date;
                }
            }

            defaultEndDate = defaultStartDate.Add(new TimeSpan(23, 59, 0));
            dtmDate.Add(defaultStartDate);
            dtmDate.Add(defaultEndDate);


            if (startDate == "" && startTime == "")
            { //'user didn't put start date/time
                //'check for relative date
                if (relStart == "")
                { //no rel start
                    dtmDate[0] = defaultStartDate; //default: today 00:00
                }
                else
                {
                    dtmDate[0] = relDateBase.AddHours(Convert.ToDouble(relStart)); //DateAdd("h", -Val(RelStart), RelDateBase) ;//point to rel date
                }
            }
            else if (startDate != "" && startTime == "")
            { //user put start date only
                dtmDate[0] = GetDatePart(startDate);
            }
            else if (startDate == "" && startTime != "")
            { //user put start time only
                dtmDate[0] = defaultStartDate + GetTimeSpan(startTime); //default: today + time
            }
            else
            {//user put both start date/time
                dtmDate[0] = GetDatePart(startDate) + GetTimeSpan(startTime);
            }

            if (endDate == "" && endTime == "")
            { //user didn't put end date/time
                //check for relative date
                if (relEnd == "")
                { //no rel end
                    dtmDate[1] = defaultEndDate;//default: startdate + 23:59
                }
                else
                {
                    dtmDate[1] = relDateBase.AddHours(Convert.ToDouble(relEnd)); //point to rel date
                }
            }
            else if (endDate != "" && endTime == "")
            { //user put end date only
                dtmDate[1] = GetDatePart(endDate) +
                             new TimeSpan(23, 59, 0);
            }
            else if (endDate == "" && endTime != "")
            { //user put end time only
                dtmDate[1] = defaultStartDate +
                             GetTimeSpan(endTime);//default: startdate + time
            }
            else
            {//'user put both end date/time
                dtmDate[1] = GetDatePart(endDate) +
                             GetTimeSpan(endTime);
            }

            return dtmDate;
        }
    

        private Dictionary<string, string> GetDefaultParamCollection()
        {
            Dictionary<string, string> map = new Dictionary<string, string>();

            string strRelBefore = GetDeIceConfigInfo("LOAD_REL_BEFORE", "2");
            string strRelAfter = GetDeIceConfigInfo("LOAD_REL_AFTER", "2");

            map.Add("<Default>", "VIEW");
            map.Add(strRelBefore, "RELPRDSTART");
            map.Add(strRelAfter, "RELPRDEND");
            map.Add("1", "DEP");
            map.Add("1", "O");
            map.Add("1", "X");

            return map;
        }

        private Dictionary<string, string> GetViewParamCollection(string stViewParams, bool search)
        {
            Dictionary<string, string> map = new Dictionary<string, string>();

            string[] stArr = stViewParams.Split(';');
            foreach (string stParam in stArr)
            {
                string[] paraArr = stParam.Split('=');
                if(paraArr.Length > 1)
                {
                    map.Add(paraArr[0], paraArr[1]);
                }
            }

            if(search)
            {
#warning TO DO to implement //search'
                //to do
            }

            return map;
        }
    }
}
