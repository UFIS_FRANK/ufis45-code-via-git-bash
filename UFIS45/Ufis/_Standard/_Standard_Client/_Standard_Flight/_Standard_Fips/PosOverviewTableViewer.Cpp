// PosOverviewTableViewer.cpp : implementation file
// 
// Modification History: 


#include <stdafx.h>
#include <PosOverviewTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <math.h>
#include <BasicData.h>
#include <CcaCedaFlightData.h>
#include <DiaCedaFlightData.h>

#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif




// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// PosOverviewTableViewer
//


int PosOverviewTableViewer::imTableColCharWidths[POSOVERVIEWTABLE_COLCOUNT]={4,15,15,10,2,1,3,8,14,12,8,10,10,2,1,5,8,14,15};


PosOverviewTableViewer::PosOverviewTableViewer():
	imOrientation(PRINT_LANDSCAPE),
 	romTableHeaderFont(ogCourier_Bold_10), romTableLinesFont(ogCourier_Regular_9),
	fmTableHeaderFontWidth(10), fmTableLinesFontWidth(7.5)
{
	dgCCSPrintFactor = 2.5;

	CString olHeader = GetString(IDS_STRING2744);
 
	CStringArray olList;
	int ilCount = SplitItemList(olHeader, &olList, 1);
	for(int i=0;  i<ilCount; i++)
	{
		omTableHeadlines[i] = olList.GetAt(i);
	}

 
	// calculate table column widths
	for (i=0; i < POSOVERVIEWTABLE_COLCOUNT; i++)
	{
 		imTableColWidths[i] = (int) max(imTableColCharWidths[i]*fmTableLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmTableHeaderFontWidth);
	}

    pomTable = NULL;
}


void PosOverviewTableViewer::UnRegister()
{
	ogDdx.UnRegister(this, NOTUSED);
 
}

PosOverviewTableViewer::~PosOverviewTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void PosOverviewTableViewer::SetResource(CString opResource)
{
	myResource = opResource;

	if (myResource == "BLT")
		myCaption = GetString(IDS_STRING1934);
	else if (myResource == "GAT")
		myCaption = GetString(IDS_STRING2731);
	else if (myResource == "WRO")
		myCaption = GetString(IDS_STRING2732);
	else if (myResource == "POS")
		myCaption = GetString(IDS_STRING2734);
	else if (myResource == "CCA")
		myCaption = GetString(IDS_STRING2733);
}


void PosOverviewTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}



void PosOverviewTableViewer::ChangeViewTo(CString opView)
{
	// Register broadcasts
	ogDdx.UnRegister(this, NOTUSED);
	ogDdx.Register(this, D_FLIGHT_CHANGE, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	ogDdx.Register(this, D_FLIGHT_DELETE, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	ogDdx.Register(this, D_FLIGHT_INSERT, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);

	if (pomTable)
		pomTable->ResetContent();

    DeleteAll();  
    MakeLines();
	UpdateDisplay();
	
	if (pomParentDlg)
	{
		CString olCaption = GetCaption();
		pomParentDlg->SetWindowText(olCaption);
	}
}



/////////////////////////////////////////////////////////////////////////////
// PosOverviewTableViewer -- code specific to this class
void PosOverviewTableViewer::MakeLines()
{
	CString olTmp;
	DiaCedaFlightData::RKEYLIST *prlRkey;
	POSITION pos;
	void *pVoid;
	for( pos = ogPosDiaFlightData.omRkeyMap.GetStartPosition(); pos != NULL; )
	{
		ogPosDiaFlightData.omRkeyMap.GetNextAssoc( pos, pVoid , (void *&)prlRkey );
		MakeRkey (prlRkey, false);
	}
}


void PosOverviewTableViewer::MakeRkey(DiaCedaFlightData::RKEYLIST *prlRkey, bool bpInsertDisplay /*flase*/)
{
	if (!prlRkey)
		return;

	int ilFlightCount = prlRkey->Rotation.GetSize();
	for(int i=0; i<ilFlightCount; i++)
	{
		DIAFLIGHTDATA *prlFlightA = &prlRkey->Rotation[i];
		DIAFLIGHTDATA *prlFlightD = NULL;
		if(prlRkey->Rotation.GetSize() == 1 && prlFlightA->Adid[0] == 'D')
		{
			prlFlightD = prlFlightA;
			prlFlightA = NULL;
		}
		else
		{
			if(i + 1 <  prlRkey->Rotation.GetSize())
			{
				prlFlightD = &prlRkey->Rotation[i+1];
				if(prlFlightA->Adid[0] == 'D')
				{
					prlFlightD = prlFlightA;
					prlFlightA = NULL;
				}
				else
				{
				
					if(prlFlightD->Tifd <= prlFlightA->Tifa)
					{
						prlFlightD = NULL;
					}
					else
					{
						if(prlFlightD->Adid[0] != 'B')
							i++;
					}				
				}
			}
			else
			{
				if(prlFlightA->Adid[0] == 'D')
				{
					prlFlightD = prlFlightA;
					prlFlightA = NULL;
				}
				else
				{
					prlFlightD = NULL;
				}
			}
		}

		Make (prlFlightA, prlFlightD, bpInsertDisplay);
	}
}



int PosOverviewTableViewer::Make(const DIAFLIGHTDATA *prlFlightA, const DIAFLIGHTDATA *prlFlightD, bool bpInsertDisplay /*flase*/)
{		
//		if (!prlFlightA)// || CString(";T;G").Find(prlFlight->Ftyp) > 0)
//			return -1;

		int ilLineNo = MakeLine(prlFlightA, prlFlightD);
		if(bpInsertDisplay && ilLineNo >= 0)
			InsertDisplayLine(ilLineNo);

		return 0;
}


int PosOverviewTableViewer::MakeLine(const DIAFLIGHTDATA *rrpFlightA, const DIAFLIGHTDATA *rrpFlightD)
{
	CString olPsta = "";
	CString olPstd = "";

	if(rrpFlightA != NULL)
	{
		olPsta = rrpFlightA->Psta;
	}
	if(rrpFlightD != NULL)
	{
		olPstd = rrpFlightD->Pstd;
	}

	if (!olPsta.IsEmpty() || !olPstd.IsEmpty())
	{
		POSOVERVIEWTABLE_LINEDATA rlLine;
 
 		MakeLineData(rrpFlightA, rrpFlightD, rlLine);
		return CreateLine(rlLine);
	}

	return -1;
}



// fill the intern line data
void PosOverviewTableViewer::MakeLineData(const DIAFLIGHTDATA *prlFlightA, const DIAFLIGHTDATA *prlFlightD, POSOVERVIEWTABLE_LINEDATA &rrpLine)
{
	CString olPsta = "";
	CString olPstd = "";

	if(prlFlightA != NULL)
		olPsta = prlFlightA->Psta;

	if(prlFlightD != NULL)
		olPstd = prlFlightD->Pstd;

	if (prlFlightD && !olPstd.IsEmpty())
	{
		rrpLine.DUrno =  prlFlightD->Urno;
		rrpLine.Rkey  =  prlFlightD->Rkey;
		rrpLine.DFlno =  CString(prlFlightD->Flno);
		rrpLine.DFlti =  CString(prlFlightD->Flti);
		rrpLine.DFtyp =  CString(prlFlightD->Ftyp);
		rrpLine.DNat  =  CString(prlFlightD->Ttyp);
		rrpLine.Des34=  CString(prlFlightD->Des3) + CString("/") + CString(prlFlightD->Des4);
		rrpLine.Stod  =  prlFlightD->Stod;
		rrpLine.Ofbl  =  prlFlightD->Ofbl;
		rrpLine.DAkt  =  prlFlightD->Tifd;

		rrpLine.Act35 =  CString(prlFlightD->Act3) + CString("/") + CString(prlFlightD->Act5);
		rrpLine.Regn  =  CString(prlFlightD->Regn);

		rrpLine.DAktStr = CString("STD: ");
		rrpLine.DAkt = prlFlightD->Stod;
		if(prlFlightD->Ofbl != TIMENULL)
		{
			rrpLine.DAktStr = CString("OFBL: ");
			rrpLine.DAkt = prlFlightD->Ofbl;

			if(prlFlightD->Airb != TIMENULL)
			{
				rrpLine.DAktStr = CString("ATD: ");
				rrpLine.DAkt = prlFlightD->Airb;
			}
		}
		else
		{
			if(prlFlightD->Etdi != TIMENULL)
			{
				rrpLine.DAktStr = CString("ETD: ");
				rrpLine.DAkt = prlFlightD->Etdi;
			}
		}


		DIAFLIGHTDATA* temp;
		if(omUrnoMap.Lookup((void *)rrpLine.DUrno,(void *& ) temp) == FALSE)
			omUrnoMap.SetAt((void *)rrpLine.DUrno, temp);
	}


	if (prlFlightA && !olPsta.IsEmpty())
	{
		rrpLine.AUrno =  prlFlightA->Urno;
		rrpLine.Rkey  =  prlFlightA->Rkey;
		rrpLine.AFlno =  CString(prlFlightA->Flno);
		rrpLine.AFlti =  CString(prlFlightA->Flti);
		rrpLine.AFtyp =  CString(prlFlightA->Ftyp);
		rrpLine.ANat  =  CString(prlFlightA->Ttyp);
		rrpLine.Org34=  CString(prlFlightA->Org3) + CString("/") + CString(prlFlightA->Org4);
		rrpLine.Stoa  =  prlFlightA->Stoa;
		rrpLine.Onbl  =  prlFlightA->Onbl;
		rrpLine.AAkt  =  prlFlightA->Tifa;

		rrpLine.Act35 =  CString(prlFlightA->Act3) + CString("/") + CString(prlFlightA->Act5);
		rrpLine.Regn  =  CString(prlFlightA->Regn);


		rrpLine.AAktStr = CString("STA: ");
		rrpLine.AAkt = prlFlightA->Stoa;
		if(prlFlightA->Onbl != TIMENULL)
		{
			rrpLine.AAktStr = CString("ONBL: ");
			rrpLine.AAkt = prlFlightA->Onbl;
		}
		else
		{
			if(prlFlightA->Land != TIMENULL)
			{
				rrpLine.AAktStr = CString("ATA: ");
				rrpLine.AAkt = prlFlightA->Land;
			}
			else
			{
				if(prlFlightA->Tmoa != TIMENULL)
				{
					rrpLine.AAktStr = CString("TMO: ");
					rrpLine.AAkt = prlFlightA->Tmoa;
				}
				else
				{
					if(prlFlightA->Etai != TIMENULL)
					{
						rrpLine.AAktStr = CString("ETA: ");
						rrpLine.AAkt = prlFlightA->Etai;
					}
				}
			}
		}


		DIAFLIGHTDATA* temp;
		if(omUrnoMap.Lookup((void *)rrpLine.AUrno,(void *& ) temp) == FALSE)
			omUrnoMap.SetAt((void *)rrpLine.AUrno, temp);
	}



	if(olPsta != olPstd)
	{
		if(!olPsta.IsEmpty())
		{
			if(prlFlightA != NULL)
			{
				rrpLine.Name  = olPsta;
				rrpLine.Begin = prlFlightA->StartPosArr;
				rrpLine.End   = prlFlightA->EndPosArr;
			}
		}
		if(!olPstd.IsEmpty())
		{
			if(prlFlightD != NULL)
			{
				rrpLine.Name  = olPstd;
				rrpLine.Begin = prlFlightD->StartPosDep;
				rrpLine.End   = prlFlightD->EndPosDep;
			}
		}
	}
	else if((olPstd == olPsta) && (!olPstd.IsEmpty() && !olPsta.IsEmpty()))
	{
		if(prlFlightA != NULL)
		{
			rrpLine.Name  = olPsta;
			rrpLine.Begin = prlFlightA->StartPosArr;
			rrpLine.End   = prlFlightD->EndPosDep;
		}
	}

	// local times requested?
	if(bgGatPosLocal) UtcToLocal(rrpLine);

    return;
}


// convert all the times in the given line in local time
bool PosOverviewTableViewer::UtcToLocal(POSOVERVIEWTABLE_LINEDATA &rrpLine)
{

	ogBasicData.UtcToLocal(rrpLine.Stoa);
	ogBasicData.UtcToLocal(rrpLine.Stod);
	ogBasicData.UtcToLocal(rrpLine.AAkt);
	ogBasicData.UtcToLocal(rrpLine.DAkt);
	ogBasicData.UtcToLocal(rrpLine.Onbl);
	ogBasicData.UtcToLocal(rrpLine.Ofbl);
	ogBasicData.UtcToLocal(rrpLine.Begin);
	ogBasicData.UtcToLocal(rrpLine.End);

	return true;
}


// create one internal line
int PosOverviewTableViewer::CreateLine(POSOVERVIEWTABLE_LINEDATA &rrpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareLines(rrpLine, omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rrpLine);
	return ilLineno;
}


void PosOverviewTableViewer::DeleteLine(int ipLineno)
{
	// delete internal line
	omLines.DeleteAt(ipLineno);
	// delete table line
	pomTable->DeleteTextLine(ipLineno);

}



bool PosOverviewTableViewer::FindLine(long lpUrno, int &ripLineno) const
{
	ripLineno = -1;
	// scan all intern lines
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if(omLines[i].Rkey == lpUrno)
	  {
		ripLineno = i;
		return true;
	  }
	}
	return false;
}






/////////////////////////////////////////////////////////////////////////////
// PosOverviewTableViewer - POSOVERVIEWTABLE_LINEDATA array maintenance

void PosOverviewTableViewer::DeleteAll()
{
    omLines.DeleteAll();
	omUrnoMap.RemoveAll();
}


/////////////////////////////////////////////////////////////////////////////
// PosOverviewTableViewer - display drawing routine


void PosOverviewTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	POSOVERVIEWTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(*prlLine, olColList);
		// add table line
		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}

// Insert new lines for the given flight
void PosOverviewTableViewer::InsertFlight(const DIAFLIGHTDATA &rrpFlight)
{
	if (rrpFlight.Rkey > 0)
	{
		DiaCedaFlightData::RKEYLIST *prlRkey = ogPosDiaFlightData.GetRotationByRkey(rrpFlight.Rkey);
		MakeRkey(prlRkey, true);
	}
	return;
}



void PosOverviewTableViewer::InsertDisplayLine( int ipLineNo)
{
	if(!((ipLineNo >= 0) && (ipLineNo < omLines.GetSize())))
		return;
	CCSPtrArray<TABLE_COLUMN> olColList;
	MakeColList(omLines[ipLineNo], olColList);
	pomTable->InsertTextLine(ipLineNo, olColList, &omLines[ipLineNo]);
	olColList.DeleteAll();
}






void PosOverviewTableViewer::DrawHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &romTableHeaderFont;

	// for all rows
	for (int i=0; i < POSOVERVIEWTABLE_COLCOUNT; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		rlHeader.Length = imTableColWidths[i]; 
		rlHeader.Text = omTableHeadlines[i]; 
		omHeaderDataArray.New(rlHeader);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}


// fill the table line with the data of the given intern line
void PosOverviewTableViewer::MakeColList(const POSOVERVIEWTABLE_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList)
{

	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &romTableLinesFont;
	
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Name;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Begin.Format("%d.%m.%y %H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.End.Format("%d.%m.%y %H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
 
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.AFlno;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.AFlti;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.AFtyp;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.ANat;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Org34;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Stoa.Format("%d.%m %H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
/*
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Onbl.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
*/
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.AAktStr + rrpLine.AAkt.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Act35;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Regn;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DFlno;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DFlti;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DFtyp;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DNat;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Des34;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Stod.Format("%d.%m %H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
/*
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Ofbl.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
*/
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DAktStr + rrpLine.DAkt.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
}

int PosOverviewTableViewer::GetFlightCount()
{
	return omUrnoMap.GetCount();
}

void PosOverviewTableViewer::SetParentDlg(CDialog* ppParentDlg)
{
	pomParentDlg = ppParentDlg;
}

CString PosOverviewTableViewer::GetCaption()
{
	CString olCaption;
	olCaption.Format(myCaption, omLines.GetSize(), GetFlightCount());
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	olCaption += " ("+olTimes+")";

	return olCaption;
}

//////////////////////////////////////////////////////////////////
//// Printing routines
 
// Print table to paper
void PosOverviewTableViewer::PrintTableView(void) {

	CCSPrint olPrint(NULL, imOrientation, 45);

	// Set printing fonts
	pomPrintHeaderFont = &olPrint.ogCourierNew_Bold_7;
	pomPrintLinesFont = &olPrint.ogCourierNew_Regular_7;
	fmPrintHeaderFontWidth = 6;
	fmPrintLinesFontWidth = 6;

	// calculate table column widths for printing
	for (int i=0; i < POSOVERVIEWTABLE_COLCOUNT; i++)
	{
  		imPrintColWidths[i] = (int) max(imTableColCharWidths[i]*fmPrintLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmPrintHeaderFontWidth);
	}

	CString olFooter1,olFooter2;
	// Set left footer to: "printed at: <date>"
	CString olCaption = GetCaption();
 	olFooter1.Format("%s %s     %s", GetString(IDS_STRING1481),
		(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")), olCaption);

  	if (olPrint.InitializePrinter(imOrientation) == TRUE)
	{ 
		olPrint.imMaxLines = 38;	// Def. imMaxLines: 57 Portrait / 38 Landscape
		// Calculate number of pages
		const double dlPages = ceil((double)pomTable->GetLinesCount() / (double)(olPrint.imMaxLines - 1));
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		// At first a pagebreak
		olPrint.imLineNo = olPrint.imMaxLines + 1;	
		DOCINFO	rlDocInfo;
		memset(&rlDocInfo, 0, sizeof(DOCINFO));
		rlDocInfo.cbSize = sizeof( DOCINFO );
		rlDocInfo.lpszDocName = GetString(IDS_STRING1934);	
		olPrint.omCdc.StartDoc( &rlDocInfo );
		olPrint.imPageNo = 0;
		// Print all table lines
		for (int ilLc = 0; ilLc < pomTable->GetLinesCount(); ilLc++)
		{
			// Page break
 			if(olPrint.imLineNo >= olPrint.imMaxLines)
			{
				if(olPrint.imPageNo > 0)
				{
					// Set right footer to: "Page: %d"
					olFooter2.Format(GetString(IDS_STRING1199),olPrint.imPageNo, dlPages);
					// print footer
					olPrint.PrintUIFFooter(olFooter1,"",olFooter2);
					olPrint.omCdc.EndPage();
				}
				// print header
				PrintTableHeader(olPrint);
			}				
			// print line
			PrintTableLine(olPrint, ilLc);
		}
		// print footer
		olFooter2.Format(GetString(IDS_STRING1199),olPrint.imPageNo, dlPages);
		olPrint.PrintUIFFooter(olFooter1,"",olFooter2);
		olPrint.omCdc.EndPage();
		olPrint.omCdc.EndDoc();
	}  // if (olPrint.InitializePrin...
 	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}




bool PosOverviewTableViewer::PrintTableHeader(CCSPrint &ropPrint)
{
	ropPrint.omCdc.StartPage();
	ropPrint.imPageNo++;
	ropPrint.imLineNo = 0;
	//double dgCCSPrintFactor = 2.7 ;
	// Headline
// 	olHeadline.Format(GetString(IDS_STRING1934), pomTable->GetLinesCount());
// 	olHeadline.Format(GetString(IDS_STRING1934), omUrnoMap.GetCount());
	CString olHeadline = GetCaption();
	// print page headline
	ropPrint.imLeftOffset = 100;
	ropPrint.PrintUIFHeader("", olHeadline, ropPrint.imFirstLine-10);
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_NOFRAME;
	rlElement.pFont       = pomPrintHeaderFont;

	// Create table headline
	for (int ilCc = 0; ilCc < POSOVERVIEWTABLE_COLCOUNT; ilCc++)
	{
		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 

		rlElement.Text = omTableHeadlines[ilCc]; 
		
		rlPrintLine.NewAt(rlPrintLine.GetSize(), rlElement);
 	}
	// Print table headline
 	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	return true;
}


 

bool PosOverviewTableViewer::PrintTableLine(CCSPrint &ropPrint, int ipLineNo) {
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	
	//double dgCCSPrintFactor = 2.7 ;

	PRINTELEDATA rlElement;
	rlElement.pFont = pomPrintLinesFont;
	rlElement.FrameTop = PRINT_FRAMETHIN;
 	rlElement.FrameBottom = PRINT_FRAMETHIN;
	rlElement.FrameLeft  = PRINT_FRAMETHIN;
	rlElement.FrameRight = PRINT_FRAMETHIN;

	CString olCellValue;
	// create table line
	for (int ilCc = 0; ilCc < POSOVERVIEWTABLE_COLCOUNT; ilCc++)
	{
		rlElement.Alignment  = PRINT_LEFT;
 		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 
		// Get printing text from table	
		pomTable->GetTextFieldValue(ipLineNo, ilCc, olCellValue);
		rlElement.Text = olCellValue; 
		
 		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	// print table line
	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll(); 
	
	return true;
}



// Print table to file
bool PosOverviewTableViewer::PrintPlanToFile(CString opFilePath) const
{

	if(opFilePath.GetLength() != 0)
	{
		// Get Seperator for Excel-File
		char pclConfigPath[256];
		char pclTrenner[2];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
		    strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
		    pclTrenner, sizeof pclTrenner, pclConfigPath);

		CString olCaption;
		olCaption.Format(myCaption, omLines.GetSize(), omUrnoMap.GetCount());
		CString olTimes;
		if (bgGatPosLocal)
			olTimes = GetString(IDS_STRING1921);
		else
			olTimes = GetString(IDS_STRING1920);
		olCaption += " ("+olTimes+")";

		ofstream of;
		of.open(opFilePath.GetBuffer(0), ios::out);

		of << olCaption;
		of << endl;

		// Header
		for (int ilCc = 0; ilCc < POSOVERVIEWTABLE_COLCOUNT; ilCc++)
		{
			of  << omTableHeadlines[ilCc];
			if (ilCc < POSOVERVIEWTABLE_COLCOUNT-1)
			{
				of << pclTrenner;
			}
		}
		of << endl;

		CString olCellValue;
		 // Lines
		for (int ilLc = 0; ilLc < pomTable->GetLinesCount(); ilLc++)
		{
			for (int ilCc = 0; ilCc < POSOVERVIEWTABLE_COLCOUNT; ilCc++)
			{
				// get text from table
				pomTable->GetTextFieldValue(ilLc, ilCc, olCellValue);
				of  << olCellValue;
				if (ilCc < POSOVERVIEWTABLE_COLCOUNT-1)
				{
					of << pclTrenner;
				}
			}
			of << endl;
		}

		of.close();
 
		return true;
	} // 	if(GetSaveFileName(polOfn) != 0)

	return false;
}





// broadcast-function
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	if (vpDataPointer == NULL) 
		return;

    PosOverviewTableViewer *polViewer = (PosOverviewTableViewer *)popInstance;

    if (ipDDXType == D_FLIGHT_CHANGE)
        polViewer->ProcessFlightChange(*(DIAFLIGHTDATA *)vpDataPointer);

	if (ipDDXType == D_FLIGHT_DELETE)
        polViewer->ProcessFlightDelete(*(DIAFLIGHTDATA *)vpDataPointer);

	if (ipDDXType == CCA_FLIGHT_CHANGE) //CCAFLIGHTDATA
	{
        polViewer->ProcessCcaFlightChange((CCAFLIGHTDATA *)vpDataPointer);
	}

	if (ipDDXType == CCA_FLIGHT_DELETE) //CCAFLIGHTDATA
	{
        polViewer->ProcessCcaFlightDelete((CCAFLIGHTDATA *)vpDataPointer);
	}

	if (ipDDXType == DIACCA_CHANGE)
	{
        polViewer->ProcessCcaChange(*(long *)vpDataPointer);
	}
}

void PosOverviewTableViewer::ProcessCcaFlightChange(CCAFLIGHTDATA* rrpFlight)
{
	DIAFLIGHTDATA prlDiaFlight;;
	CCAConvertDIA(rrpFlight, prlDiaFlight);
	if (rrpFlight)
	{
		ProcessFlightChange(prlDiaFlight);
	}
}

void PosOverviewTableViewer::ProcessCcaFlightDelete(CCAFLIGHTDATA* rrpFlight)
{
	DIAFLIGHTDATA prlDiaFlight;;
	CCAConvertDIA(rrpFlight, prlDiaFlight);
	if (rrpFlight)
	{
		ProcessFlightDelete(prlDiaFlight);
	}
}

void PosOverviewTableViewer::ProcessCcaChange(long plpCcaUrno)
{
	DIACCADATA *prlCca =ogCcaDiaFlightData.omCcaData.GetCcaByUrno(plpCcaUrno);
	if(prlCca)
	{
		CCAFLIGHTDATA* prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlCca->Flnu);
		if (prlFlight)
		{
			DIAFLIGHTDATA prlDiaFlight;;
			CCAConvertDIA(prlFlight, prlDiaFlight);
			ProcessFlightChange(prlDiaFlight);
		}
	}
}


void PosOverviewTableViewer::CCAConvertDIA(CCAFLIGHTDATA* prlFlight, DIAFLIGHTDATA& rlDiaFlight)
{
	if (prlFlight)
	{
		//DIAFLIGHTDATA* rlDiaFlight = new DIAFLIGHTDATA();;
		rlDiaFlight.Urno = prlFlight->Urno;
		strncpy(rlDiaFlight.Flno,prlFlight->Flno,10);
		strncpy(rlDiaFlight.Flti,prlFlight->Flti,2);
		strncpy(rlDiaFlight.Des3,prlFlight->Des3,3);
		rlDiaFlight.Stod = prlFlight->Stod;
		rlDiaFlight.Etdi = prlFlight->Etdi;
		rlDiaFlight.Ofbl = prlFlight->Ofbl;
		strncpy(rlDiaFlight.Adid,prlFlight->Adid,2);
		strncpy(rlDiaFlight.Ftyp,prlFlight->Ftyp,2);
		strncpy(rlDiaFlight.Ttyp,prlFlight->Ttyp,6);

	}
}



void PosOverviewTableViewer::ProcessFlightChange(const DIAFLIGHTDATA &rrpFlight)
{
	int ilTopIndex = pomTable->pomListBox->GetTopIndex();
 
	// Delete all lines with the urno of the changed flight
	ProcessFlightDelete(rrpFlight);

	// Insert the changed flight
	InsertFlight(rrpFlight);

	if (pomParentDlg)
	{
		CString olCaption = GetCaption();
		pomParentDlg->SetWindowText(olCaption);
	}

	if (ilTopIndex < omLines.GetSize())
		pomTable->pomListBox->SetTopIndex(ilTopIndex);

	return;

}



void PosOverviewTableViewer::ProcessFlightDelete(const DIAFLIGHTDATA &rrpFlight)
{
 
	// Delete all lines with the urno of the changed flight
    for (int ilLineNo = omLines.GetSize() - 1; ilLineNo >= 0; ilLineNo--)
	{
	  if(omLines[ilLineNo].Rkey == rrpFlight.Rkey)
	  {
		DeleteLine(ilLineNo);
	  }
	}

 	omUrnoMap.RemoveKey((void *)rrpFlight.Urno);

	if (pomParentDlg)
	{
		CString olCaption = GetCaption();
		pomParentDlg->SetWindowText(olCaption);
	}

}




// compare function for sorting
int PosOverviewTableViewer::CompareLines(const POSOVERVIEWTABLE_LINEDATA &rrpLine1, const POSOVERVIEWTABLE_LINEDATA &rrpLine2) const
{
	if (rrpLine1.Name > rrpLine2.Name) return 1;
	if (rrpLine1.Name < rrpLine2.Name) return -1;

	if (rrpLine1.Begin > rrpLine2.Begin) return 1;
	if (rrpLine1.Begin < rrpLine2.Begin) return -1;

	return 0;
}








