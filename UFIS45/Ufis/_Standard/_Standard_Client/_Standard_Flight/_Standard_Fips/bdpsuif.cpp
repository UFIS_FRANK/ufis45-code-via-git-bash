// Machine generated IDispatch wrapper class(es) created with ClassWizard

#include "stdafx.h"
#include "bdpsuif.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

 

/////////////////////////////////////////////////////////////////////////////
// IApplication properties

CString IApplication::GetUsername()
{
	CString result;
	GetProperty(0x1, VT_BSTR, (void*)&result); 
	return result;
}

void IApplication::SetUsername(LPCTSTR propVal)
{
	SetProperty(0x1, VT_BSTR, propVal);
}

CString IApplication::GetPassword()
{
	CString result;
	GetProperty(0x2, VT_BSTR, (void*)&result);
	return result;
}

void IApplication::SetPassword(LPCTSTR propVal)
{
	SetProperty(0x2, VT_BSTR, propVal);
}

CString IApplication::GetResourceTable()
{
	CString result;
	GetProperty(0x3, VT_BSTR, (void*)&result);
	return result;
}

void IApplication::SetResourceTable(LPCTSTR propVal)
{
	SetProperty(0x3, VT_BSTR, propVal);
}

long IApplication::GetResourceUrno()
{
	long result;
	GetProperty(0x4, VT_I4, (void*)&result);
	return result;
}

void IApplication::SetResourceUrno(long propVal)
{
	SetProperty(0x4, VT_I4, propVal);
}

CString IApplication::GetCaller()
{
	CString result;
	GetProperty(0x5, VT_BSTR, (void*)&result);
	return result;
}

void IApplication::SetCaller(LPCTSTR propVal)
{
	SetProperty(0x5, VT_BSTR, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IApplication operations

BOOL IApplication::SetCmdLine(LPCTSTR pszCmdLine)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		pszCmdLine);
	return result;
}

BOOL IApplication::DisplayResource()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}
