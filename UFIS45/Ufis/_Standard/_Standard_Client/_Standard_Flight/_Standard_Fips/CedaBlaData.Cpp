// CedaBlaData.cpp
 
#include "stdafx.h"
#include "CedaBlaData.h"

// Local function prototype
static void ProcessBlaCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaBlaData ogBlaData;

CedaBlaData::CedaBlaData()
{
    // Create an array of CEDARECINFO for BLADATA
	BEGIN_CEDARECINFO(BLADATA,BlaDataRecInfo)
		FIELD_CHAR_TRIM	(Ftyp,"FTYP")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG      (Time,"TIME")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Ogrp, "OGRP")
		FIELD_CHAR_TRIM	(Agrp, "AGRP")
	END_CEDARECINFO //(BlaDataAStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(BlaDataRecInfo)/sizeof(BlaDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&BlaDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"BLA");
//	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmBlaFieldList,"FTYP,CDAT,LSTU,URNO,TIME,USEC,USEU,OGRP,AGRP");
	pcmFieldList = pcmBlaFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

void CedaBlaData::Register(void)
{
	ogDdx.Register((void *)this,BC_BLA_CHANGE,CString("BLADATA"), CString("Bla-changed"),ProcessBlaCf);
	ogDdx.Register((void *)this,BC_BLA_DELETE,CString("BLADATA"), CString("Bla-deleted"),ProcessBlaCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaBlaData::~CedaBlaData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaBlaData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
   omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaBlaData::ReadAllBlas()
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(ST_FEHLER),MB_OK);
					return false;
				}
			}
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		BLADATA *prpBla = new BLADATA;
		if ((ilRc = GetBufferRecord(ilLc,prpBla)) == true)
		{
			prpBla->IsChanged = DATA_UNCHANGED;
			omData.Add(prpBla);//Update omData
			omUrnoMap.SetAt((void *)prpBla->Urno,prpBla);
//			omBnamMap.SetAt(prpBla->Bnam,prpBla);
		}
		else
		{
			delete prpBla;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaBlaData::InsertBla(BLADATA *prpBla,BOOL bpSendDdx)
{
	BLADATA *prlBla = new BLADATA;
	prpBla->Urno = ogBasicData.GetNextUrno();
	memcpy(prlBla,prpBla,sizeof(BLADATA));
	strcpy(prlBla->Usec,ogBasicData.omUserID);
	prlBla->Cdat = CTime::GetCurrentTime();
	prlBla->IsChanged = DATA_NEW;
	SaveBla(prlBla); //Update Database
	InsertBlaInternal(prlBla);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaBlaData::InsertBlaInternal(BLADATA *prpBla)
{
	//PrepareBlaData(prpBla);
	ogDdx.DataChanged((void *)this, BLA_CHANGE,(void *)prpBla ); //Update Viewer
	omData.Add(prpBla);//Update omData
	omUrnoMap.SetAt((void *)prpBla->Urno,prpBla);
//	omBnamMap.SetAt(prpBla->Bnam,prpBla);
	return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaBlaData::DeleteBla(long lpUrno)
{
	bool olRc = true;
	BLADATA *prlBla = GetBlaByUrno(lpUrno);
	if (prlBla != NULL)
	{
		prlBla->IsChanged = DATA_DELETED;
		olRc = SaveBla(prlBla); //Update Database
		DeleteBlaInternal(prlBla);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaBlaData::DeleteBlaInternal(BLADATA *prpBla)
{
	ogDdx.DataChanged((void *)this,BLA_DELETE,(void *)prpBla); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpBla->Urno);
//	omBnamMap.RemoveKey(prpBla->Bnam);
	int ilBlaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilBlaCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpBla->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaBlaData::PrepareBlaData(BLADATA *prpBla)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaBlaData::UpdateBla(BLADATA *prpBla,BOOL bpSendDdx)
{
	if (GetBlaByUrno(prpBla->Urno) != NULL)
	{
		if (prpBla->IsChanged == DATA_UNCHANGED)
		{
			prpBla->IsChanged = DATA_CHANGED;
		}
		strcpy(prpBla->Useu,ogBasicData.omUserID);
		prpBla->Lstu = CTime::GetCurrentTime();
		SaveBla(prpBla); //Update Database
		UpdateBlaInternal(prpBla);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaBlaData::UpdateBlaInternal(BLADATA *prpBla)
{
	BLADATA *prlBla = GetBlaByUrno(prpBla->Urno);
	if (prlBla != NULL)
	{
//		omBnamMap.RemoveKey(prlBla->Bnam);
		*prlBla = *prpBla; //Update omData
//		omBnamMap.SetAt(prlBla->Bnam,prlBla);
		ogDdx.DataChanged((void *)this,BLA_CHANGE,(void *)prlBla); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

BLADATA *CedaBlaData::GetBlaByUrno(long lpUrno)
{
	BLADATA  *prlBla;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlBla) == TRUE)
	{
		return prlBla;
	}
	return NULL;
}

//--READSPECIALBlaDATA-------------------------------------------------------------------------------------

bool CedaBlaData::ReadSpecial(CCSPtrArray<BLADATA> &ropBlt,char *pspWhere)
{
	bool ilRc = true;
	if (pspWhere == NULL)
	{
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT",pspWhere);
	}
	if (ilRc != true)
	{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(ST_FEHLER),MB_OK);
					return false;
				}
			}
	}
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		BLADATA *prpBlt = new BLADATA;
		if ((ilRc = GetBufferRecord(ilLc,prpBlt)) == true)
		{
			ropBlt.Add(prpBlt);
		}
		else
		{
			delete prpBlt;
		}
	}
    return true;
}




//--EXIST-I------------------------------------------------------------------------------------------------
/*
CString CedaBlaData::BlaExists(BLADATA *prpBla)
{
	CString olField = "";
	BLADATA  *prlBla;

	if (omBnamMap.Lookup((LPCSTR)prpBla->Bnam,(void *&)prlBla) == TRUE)
	{
		if(prlBla->Urno != prpBla->Urno)
		{
			olField += GetString(IDS_STRING195);
		}
	}

	return olField;
}
*/
//--EXIST-II----------------------------------------------------------------------------------------------

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaBlaData::SaveBla(BLADATA *prpBla)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpBla->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpBla->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpBla);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpBla->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpBla->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpBla);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpBla->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpBla->Urno);
		olRc = CedaAction("DRT",pclSelection);
		//prpBla->IsChanged = DATA_UNCHANGED;
		break;
	}
	if(!omLastErrorMessage.IsEmpty())
	{
		if(omLastErrorMessage.Find("ORA") != -1)
		{
			char pclMsg[2048]="";
			sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
			::MessageBox(NULL,pclMsg,GetString(ST_FEHLER),MB_OK);
			return false;
		}
	}

    return true;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessBlaCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_BLA_CHANGE :
	case BC_BLA_DELETE :
		((CedaBlaData *)popInstance)->ProcessBlaBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaBlaData::ProcessBlaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlBlaData;
	long llUrno;
	prlBlaData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlBlaData->Selection;
	if (olSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(prlBlaData->Selection);
	}
	else
	{
		int ilFirst = olSelection.Find("=")+2;
		int ilLast  = olSelection.GetLength();
		llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
	}

	BLADATA *prlBla;
	if(ipDDXType == BC_BLA_CHANGE)
	{
		if((prlBla = GetBlaByUrno(llUrno)) != NULL)
		{
			GetRecordFromItemList(prlBla,prlBlaData->Fields,prlBlaData->Data);
			UpdateBlaInternal(prlBla);
		}
		else
		{
			prlBla = new BLADATA;
			GetRecordFromItemList(prlBla,prlBlaData->Fields,prlBlaData->Data);
			InsertBlaInternal(prlBla);
		}
	}
	if(ipDDXType == BC_BLA_DELETE)
	{
		prlBla = GetBlaByUrno(llUrno);
		if (prlBla != NULL)
		{
			DeleteBlaInternal(prlBla);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
