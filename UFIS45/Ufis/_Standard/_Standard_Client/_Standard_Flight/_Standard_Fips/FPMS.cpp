// FPMS.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <FPMS.h>
#include <ufis.h>

#include <process.h>

#include <CCSTime.h>
#include <ButtonListDlg.h>
#include <LoginDlg.h>
#include <BasicData.h>
#include <BackGround.h>
#include <SeasonCedaFlightData.h>
#include <DailyCedaFlightData.h>
#include <RotationCedaFlightData.h>
#include <SeasonDlgCedaFlightData.h>
#include <SeasonCollectCedaFlightData.h>
#include <CCSClientWnd.h>
#include <PrivList.h> 
#include <RegisterDlg.h>
#include <CedaCfgData.h>
#include <CViewer.h>
#include <CedaBasicData.h>
#include <CedaInfData.h>
#include <CCSGlobl.h>

#include <CedaGrmData.h>
#include <DataSet.h>
#include <Konflikte.h>
#include <SpotAllocation.h>
#include <CedaResGroupData.h> 
#include <RotationDlg.h>
#include <SeasonDlg.h>
#include <RotGroundDlg.h>
#include <WoResTableDlg.h>
#include <CedaAptLocalUtc.h>
#include <CedaFpeData.h>
#include <CedaBlaData.h>
#include <CedaBwaData.h>

#include <UFISAmSink.h>
#include <ClntTypes.h>

#include <resrc1.h>
#include <AatHelp.h>
#include <AatLogin.h>
#include <CedaInitModuData.h>
#include <CedaAloData.h>
#include <CedaCraData.h>  
#include <CedaBwaData.h>  
#include <ListStringArrayDlg.h>  

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



COLORREF CCSClientWnd::lmBkColor = lgBkColor;
COLORREF CCSClientWnd::lmTextColor = lgTextColor;
COLORREF CCSClientWnd::lmHilightColor = lgHilightColor;

/////////////////////////////////////////////////////////////////////////////
// CFPMSApp

BEGIN_MESSAGE_MAP(CFPMSApp, CWinApp)
	//{{AFX_MSG_MAP(CFPMSApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()


// 050502 MVy: unified all local path setting
// 050502 MVy: initialize the variable
void CFPMSApp::UpdateConfigPath()
{
	char* pcPathFromEnv = getenv( "CEDA" );
	if(!pcPathFromEnv)
		strcpy( m_pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI" );
	else
	{
		ASSERT( pcPathFromEnv && ( strlen( pcPathFromEnv ) < sizeof( m_pclConfigPath ) ) );
		strcpy( m_pclConfigPath, pcPathFromEnv );
	};
};	// UpdateConfigPath

// 050502 MVy: unified all local path setting
char* CFPMSApp::GetConfigPath()
{
	if( !m_pclConfigPath || !*m_pclConfigPath )
		UpdateConfigPath();

	if( !m_pclConfigPath || !*m_pclConfigPath )
	{
		::MessageBox( 0, "configuration path not available", "config error", MB_ICONWARNING );
		strcpy( m_pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI" );
	};

	return m_pclConfigPath ;
};	// GetConfigPath


/////////////////////////////////////////////////////////////////////////////
// CFPMSApp construction

CFPMSApp::CFPMSApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	pcmLKey[0] = '\0';	
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	*m_pclConfigPath = 0 ;
}

CFPMSApp::~CFPMSApp()
{
	m_ulDebugPrintOnlyFirstLines = 0 ;		// 050309 MVy: for debugging purposes, waste less paper on printing, for testing print only one page, 0=ignore

	TRACE("CFPMSApp::~CFPMSApp\n");

	if (pogBCLog)
	{
		(*pogBCLog) << "\n\nFIPS: Broadcast-Log end " << CTime::GetCurrentTime().Format("%d.%m.%y %H:%M:%S") << "\n";

		pogBCLog->close();
		delete pogBCLog;
	}

	if (pogBackGround)
	{
		pogBackGround->DestroyWindow();
		delete pogBackGround;
	}

	DeleteBrushes();
	ogMapLblUrnoToKonftype.RemoveAll();

	if (bgDebug)
	{
		CTime olCurrTime = CTime::GetCurrentTime();
		of_debug << endl << "FIPS TERMINATED! (" << olCurrTime.Format("%d.%m.%Y %H:%M:%S") << ")" << endl;
		of_debug.close();
	}

#ifndef SATS_PERFORMANCE		// ExitApp is not called in SATSPerformance version
	AatHelp::ExitApp();
	//ExitProcess(0);
#endif	// SATS_PERFORMANCE
}

 
int CFPMSApp::ExitInstance() 
{

	//if (pogButtonList != NULL)
	//	pogButtonList->SendMessage(WM_CLOSE, 0, 0);

#ifdef SATS_PERFORMANCE
	AatHelp::ExitApp();
	ogBcHandle.ExitApp();
#endif	// SATS_PERFORMANCE

   return CWinApp::ExitInstance();
}



/////////////////////////////////////////////////////////////////////////////
// The one and only CFPMSApp object

CFPMSApp theApp;



static void InitSeasonFlightTableDefaultView()
{
   	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olGeoFilter;

	// Read the default value from the database in the server
	//TRACE("InitPrePlanTableDefaultView\n");

	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("SPECFILTER");
	olPossibleFilters.Add("UNIFILTER");
	//olPossibleFilters.Add("FLIGHTSEARCH");
	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("SAISONFLIGHT");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	//if (olLastView != "")
	//	olViewer.SelectView(olLastView);	// restore the previously selected view




	olViewer.SetViewerKey("ROTATIONFLIGHT");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	//if (olLastView != "")
	//	olViewer.SelectView(olLastView);	// restore the previously selected view

	olViewer.SetViewerKey("DAILYFLIGHT");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");

	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("FLIGHTDIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	//if (olLastView != "")
	//	olViewer.SelectView(olLastView);	// restore the previously selected view

	/*
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("GATPOSDIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view

	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("BLTDIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view

	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("GATDIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view


	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("WRODIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view


  */
	//PRF 8382
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("GEOMETRIE");
	olPossibleFilters.Add("UNIFILTER");
	olViewer.SetViewerKey("POSDIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view


	olPossibleFilters.RemoveAll();
	 //PRF 8382
	olPossibleFilters.Add("GEOMETRIE");
	olPossibleFilters.Add("UNIFILTER");
	olViewer.SetViewerKey("CCADIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view

/*
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("GEOMETRIE");
	olPossibleFilters.Add("UNIFILTER");
	olViewer.SetViewerKey("CHADIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view

*/


}






/////////////////////////////////////////////////////////////////////////////
// CFPMSApp initialization

void CFPMSApp::WinHelp(DWORD dwData, UINT nCmd) 
{
	// TODO: Add your specialized code here and/or call the base class
	
//---------------------------------------------------
//Neu MWO: Da AatHelp das Hilfe Fenster unter den ButtonListDlg schiebt und man nicht dran kommt
	CString omFileName;
	char pclTmpText[512];

	GetPrivateProfileString("GLOBAL", "HELPDIRECTORY", "c:\\Ufis\\Help",pclTmpText,sizeof pclTmpText, GetConfigPath() );
	omFileName = pclTmpText;												
	if(!omFileName.IsEmpty())
	{
		GetPrivateProfileString(ogAppName, "HELPFILE", ogAppName + ".chm",pclTmpText,sizeof pclTmpText, GetConfigPath() );
		if (!omFileName.IsEmpty())
		{
			if (omFileName[omFileName.GetLength() - 1] != '\\')
				omFileName += '\\';
			omFileName += pclTmpText;
			char *args[4];
			args[0] = "child";
			args[1] = omFileName.GetBuffer(0);
			args[2] = NULL;
			args[3] = NULL;
			int ilReturn = _spawnv(_P_NOWAIT,"C:\\Winnt\\HH.exe",args);
		}
	}
//	AatHelp::WinHelp(dwData, nCmd);

//End MWO
}


BOOL CFPMSApp::InitInstance()
{

	CCSCedaData::bmVersCheck = true;

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

#ifdef SATS_PERFORMANCE
	//TCP/IP-Broadcast

	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, GetConfigPath() );

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0) 
	{
		ogBcHandle.UseBcProxy();
		ogBcHandle.StopBc();
	}
	else if (stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		ogBcHandle.UseBcCom();
		ogBcHandle.StopBc();
	}
	//TCP/IP-Broadcast
#endif	// SATS_PERFORMANCE

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	#ifdef _AFXDLL
		Enable3dControls();			// Call this when using MFC in a shared DLL
	#else
		Enable3dControlsStatic();	// Call this when linking to MFC statically
	#endif

	// Parse the command line to see if launched as OLE server
	if (RunEmbedded() || RunAutomated())
	{
		// Register all OLE server (factories) as running.  This enables the
		//  OLE libraries to create objects from other applications.
		COleTemplateServer::RegisterAll();
	}
	else
	{
		// When a server application is launched stand-alone, it is a good idea
		//  to update the system registry in case it has been damaged.
		COleObjectFactory::UpdateRegistryAll();
	}


	HICON pHicon = AfxGetApp()->LoadIcon(IDI_FPMS);


  

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	// CCS-Initialization
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	Customize();
/*
	CTime oltmptime = CTime::GetCurrentTime();
	CString oltmpstr = oltmptime.Format("Datum: %d.%m.%Y");
	of_catch.open("CritErr.txt", ios::app);
	of_catch << oltmpstr.GetBuffer(0) << endl;
	of_catch << "=================" << endl;
*/


	/////////////////////////////////////////////////////////////////////////////
	// Standard CCS-Initialization
	/////////////////////////////////////////////////////////////////////////////

	
	ogLog.SetAppName(ogAppName);
	ogCommHandler.SetAppName(ogAppName);


	// Standard CCS-Fonts and Brushes initialization (see ccsglobl.h)
    InitFont();
	CreateBrushes();
	// CCS-Desktop background with an bitmap (logo or copyright etc.. 
	// loaded in the methode CBackGround::OnPaint)
	//pogBackGround = new CBackGround;
	//pogBackGround->Create(NULL, IDD_BACKGROUND);


	ogBcHandle.SetCloMessage(GetString(IDS_STRING1391));

    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
   		ogCommHandler.CleanUpCom();	
		ExitProcess(0);
   		return FALSE;
	}




	::UfisDllAdmin("TRACE", "ON", "FATAL");

/*	//support htmlhelp
	if (!AatHelp::Initialize(ogAppName))
	{
		CString olErrorMsg;
		AatHelp::GetLastError(olErrorMsg);
		MessageBox(NULL,olErrorMsg,"Error",MB_OK);
	}
*/
	// INIT Tablenames and Homeairport
	char pclUser[256];
	char pclPassword[256];
	char pclDebug[256];

	char pclSeasonLocal[64];
	char pclGatPosLocal[64];
	char pclDailyLocal[64];
	char pclBCLogFile[200];
	char pclDailyWithAlt[10];
	char pclCreateDaily_OrgAtdBlue[10];
	char pclFidsRemark[3];
	char pclOffRelease[10];
	char pclAirport4LC[64];
	char pclPosBuffer[11];
	char pclViewEditFilter[64];
	char pclViewEditSort[64];
	char pclViewBigBobs[64];
	char pclShowOnGround[64];
	char pclViewOnGroundTimelimit[64];
	char pclCheckAllConflicts[64];
	char pclTowingSpan[64];
	char pclResetAllocParameter[64];
	char pclDelaycode[64];
	char pclRealLocal[64];
	char pclSeatsFrom[64];
	char pclAllowCcaOverlap[64];
	char pclPositionTerminalConflict[64];
	char pclBltHeight[64];
	char pclGatHeight[64];
	char pclPosHeight[64];
	char pclWroHeight[64];
	char pclCcaHeight[64];
	char pclOverlapHeight[64];

	char pclFwrPos[64];
	char pclFwrGat[64];
	char pclFwrBlt[64];
	char pclFwrWro[64];
	char pclFwrCic[64];

	char pclPrintAutoAlloc[64];
	char pclCreateAutoAllocFiles[64];
	char pclAutoAllocFirstRule[64];

	char pclKeepExisting[64];
	char pclKeepExistingCki[64];
	char pclShowMXAC[64];
	char pclShowOrgDes[64];
	char pclShowRegn[64];
	char pclBeltAlocWithoutGates[64];
	char pclBeltAlocEqualDistribution[64];
	char pclBeltAllocReferExactParameters[64];
	char pclFWRSortPOSByType[64];
	char pclGateFIDSRemark[64];

	//TSC 0909
	char pclScenario_AutoAllocate[64];
   	char pclScenario_CheckInCounter[64]; 

   	char NoCheckInConflictAC[64]; 
	char pclUseCcaReload[64];
	char pclAutoAllocRemovePosIfDifferent[64];

	char pclIncomApPos[64];
	char pclIncomAlPos[64];
	char pclIncomApGat[64];
	char pclIncomAlGat[64];

	char pclCheckPRFL[64];
	char pclConflictNewFlight[64];

	char pclRuleforAcrRegn[64];
	char pclConflictBeltFlti[64];

	char pclDiffColoursID[64];
	char pclDisplayCcaGroups[64];
	char pclDisplayPaid[64];

	char pclNewGoodCount[64];
	char pclFactorPrio[64];
	char pclFactorSequ[64];
	char pclFactorGoodCount[64];

	char pclConflictsForAirb[64];
	char pclAllocationOverview[64];
	char pclUTCCONVERTIONS[64];

	char pclFactor_CCA[64];
	char pclConfFromCfltab[64];

	char pclAutoAllocVacancy[64];

	char pclStevFilterForBatchandExpand[64];
	char pclNewFwRGates[64];
	char pclAutoAllocGateFetchRotation[64];
	char pclAutoAllocGateWithoutGlobalNature[64];
	char pclReinstadedConflicts[64];
//	char pclCheckinAirline[64];
	char pclNatureAutoCalc[64];
	char pclNatureServiceMand[64];
	char pclRotationmask[64];
	char pclDailyRotChainDlg[64];
	char pclReports[64];
	char pclPrefixReports[64];
	char pclCheckinRemarkEdit[64];
	char pclTowingPopup[64];
	char pclCicConflictCheck[64];
	char pclSplitJoinLocal[64];
    char pclConflictUtctoLocal[64];
    char pclPreviousDayCheckIn[64];
	char pclReasonFlag[64];	
	char pclRelatedGatePosition[64];
	char pclSecondGateDemandFlag[64];
	char pclShowFlightDataToolButton[64];//AM 20070924
	char pclShowPopupImgMenu[64];//AM 20070924
	char pclGanttChartHLColor[64];//AM 20071107
	char pclPositionText[128];//AM 20071109
	char pclUNICODERemarks[64];
	char pclExtConflicts[64];
	char pclKeyCodeFilterDefault[64];
	char pclAdjacentGatRestriction[64];
	char pclAdjacentGatBuffer[64];
	char pclPositionConfirmation[64];
	char pclPosCutPaste[64];
	char pclFIDSLogo[64];
	char pclEnhancedHandlingAgentAssignment[64];
	char pclCxxReason[64];
	char pclNightFlightSlot[64];
	char pclChuteDisplay[64];	
	char pclExcelFLBag[64];
	char pclBltShowMaxPax[64];
	char pclColorDisabledFields[64];
	char pclMergeBlk[64];
	char pclAdditionalRemark[64];	
	char pclCDMPhase1[64];	
	char pclPrintArrivalGate[64];
	char pclEnableDepBelts[64];
	char pclEnableEmptyBatch[64];
	char pclTmpString[124];
	char pclShowClassInBatchDlg[64];
	char pclShowDelayArrival[64];
	char pclbgShowCashButtonInBothFlight[64];
	char pclCopyCashToOther[64];
	char pclDailyShowActivities[64];


    GetPrivateProfileString(ogAppName, "POS_AP_INCOM_FLIGHT", "FALSE", pclIncomApPos, sizeof pclIncomApPos, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "POS_AL_INCOM_FLIGHT", "FALSE", pclIncomAlPos, sizeof pclIncomAlPos, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "GAT_AP_INCOM_FLIGHT", "FALSE", pclIncomApGat, sizeof pclIncomApGat, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "GAT_AL_INCOM_FLIGHT", "FALSE", pclIncomAlGat, sizeof pclIncomAlGat, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "ADJACENT_GAT_RESTRICTION", "FALSE", pclAdjacentGatRestriction, sizeof pclAdjacentGatRestriction, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "ADJACENT_GAT_BUFFER", "30", pclAdjacentGatBuffer, sizeof pclAdjacentGatBuffer, GetConfigPath() );



    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "HAJ", pcgHome, sizeof pcgHome, GetConfigPath() );
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "HAJ", pcgTableExt, sizeof pcgTableExt, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "SEASONLOCAL", "FALSE", pclSeasonLocal, sizeof pclSeasonLocal, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "DAILYLOCAL", "FALSE", pclDailyLocal, sizeof pclDailyLocal, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "GATPOSLOCAL", "FALSE", pclGatPosLocal, sizeof pclGatPosLocal, GetConfigPath());
	GetPrivateProfileString(ogAppName, "BC-LOGFILE", "", pclBCLogFile, sizeof pclBCLogFile, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "DAILY_WITHALTKEY", "FALSE", pclDailyWithAlt, sizeof pclDailyWithAlt, GetConfigPath() );
	GetPrivateProfileString(ogAppName, "DAILY_ORGATDBLUE", "FALSE", pclCreateDaily_OrgAtdBlue, sizeof pclCreateDaily_OrgAtdBlue, GetConfigPath() );
	GetPrivateProfileString(ogAppName, "FIDS_REMARK", "2", pclFidsRemark, sizeof pclFidsRemark, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "DEBUG", "DEFAULT", pclDebug, sizeof pclDebug, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "OFFLINE_RELEASE", "TRUE", pclOffRelease, sizeof pclOffRelease, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "LKEY", "", pcmLKey, sizeof pcmLKey, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "AIRPORT4LC", "TRUE", pclAirport4LC, sizeof pclAirport4LC, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "POSBUFFER", "0", pclPosBuffer, sizeof pclPosBuffer, GetConfigPath() );

    GetPrivateProfileString(ogAppName, "EDIT_VIEW_FILTER", "0", pclViewEditFilter, sizeof pclViewEditFilter, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "EDIT_VIEW_SORT"  , "0", pclViewEditSort, sizeof pclViewEditSort, GetConfigPath() );

    GetPrivateProfileString(ogAppName, "VIEW_POS_35"  , "FALSE", pclViewBigBobs, sizeof pclViewBigBobs, GetConfigPath() );

    GetPrivateProfileString("GLOBAL", "USER", "DEFAULT", pclUser, sizeof pclUser, GetConfigPath() );
    GetPrivateProfileString("GLOBAL", "PASSWORD", "DEFAULT", pclPassword, sizeof pclPassword, GetConfigPath() );

    GetPrivateProfileString(ogAppName, "SHOW_ON_GROUND"  , "FALSE", pclShowOnGround, sizeof pclShowOnGround, GetConfigPath() );
	GetPrivateProfileString(ogAppName, "BATCH_SHOW_CLASS"  , "FALSE", pclShowClassInBatchDlg, sizeof pclShowClassInBatchDlg, GetConfigPath() );//Added by Christine
    GetPrivateProfileString(ogAppName, "ROT_GROUNDTIME_LIMIT"  , "720", pclViewOnGroundTimelimit, sizeof pclViewOnGroundTimelimit, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "CHECK_ALL_CONFLICTS"  , "FALSE", pclCheckAllConflicts, sizeof pclCheckAllConflicts, GetConfigPath() );
	//ogBcHandle.SetHomeAirport(pcgHome);

    GetPrivateProfileString(ogAppName, "DEFAULT_TOWING_TIME"  , "-10", pclTowingSpan, sizeof pclTowingSpan, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "RESET_ALLOC_PAST_SCHED"  , "0", pclResetAllocParameter, sizeof pclResetAllocParameter, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "SHOW_DELAYCODE"  , "", pclDelaycode, sizeof pclDelaycode, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "SHOW_REAL_LOCAL_TIMES", "FALSE", pclRealLocal, sizeof pclRealLocal, GetConfigPath() );

    GetPrivateProfileString(ogAppName, "LOAD_SEATCONFIGURATION_FROM"  , "ACT", pclSeatsFrom, sizeof pclSeatsFrom, GetConfigPath() );

    GetPrivateProfileString(ogAppName, "SHOW_FULL_BELT_HEIGHT ", "TRUE", pclBltHeight, sizeof pclBltHeight, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "SHOW_FULL_GATE_HEIGHT ", "TRUE", pclGatHeight, sizeof pclGatHeight, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "SHOW_FULL_POSITION_HEIGHT ", "TRUE", pclPosHeight, sizeof pclPosHeight, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "SHOW_FULL_LOUNGE_HEIGHT ", "TRUE", pclWroHeight, sizeof pclWroHeight, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "SHOW_FULL_CKI_HEIGHT ", "TRUE", pclCcaHeight, sizeof pclCcaHeight, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "DISTANCE_OF_OVERLAPHEIGHT "  , "50", pclOverlapHeight, sizeof pclOverlapHeight, GetConfigPath() );

    GetPrivateProfileString(ogAppName, "POS_FWR_BUTTON_COLOURED ", "FALSE", pclFwrPos, sizeof pclFwrPos, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "GAT_FWR_BUTTON_COLOURED ", "FALSE", pclFwrGat, sizeof pclFwrGat, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "BLT_FWR_BUTTON_COLOURED ", "FALSE", pclFwrBlt, sizeof pclFwrBlt, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "LGE_FWR_BUTTON_COLOURED ", "FALSE", pclFwrWro, sizeof pclFwrWro, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "CKI_FWR_BUTTON_COLOURED ", "FALSE", pclFwrCic, sizeof pclFwrCic, GetConfigPath() );

    GetPrivateProfileString(ogAppName, "PRINT_AUTO_ALLOCATION", "FALSE", pclPrintAutoAlloc, sizeof pclPrintAutoAlloc, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "CREATE_AUTO_ALLOCATION_FILES", "FALSE", pclCreateAutoAllocFiles, sizeof pclCreateAutoAllocFiles, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "AUTO_ALLOCATION_WITH_FIRST_RULE", "FALSE", pclAutoAllocFirstRule, sizeof pclAutoAllocFirstRule, GetConfigPath() );

    GetPrivateProfileString(ogAppName, "KEEP_EXISTING_RESOURCECHAIN", "FALSE", pclKeepExisting, sizeof pclKeepExisting, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "KEEP_EXISTING_RESOURCECHAIN_CKI_ONLY", "FALSE", pclKeepExistingCki, sizeof pclKeepExistingCki, GetConfigPath() );

    GetPrivateProfileString(ogAppName, "PST_SHOW_MXAC", "FALSE", pclShowMXAC, sizeof pclShowMXAC, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "PST_SHOW_ORGDES", "FALSE", pclShowOrgDes, sizeof pclShowOrgDes, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "PST_SHOW_REGN", "FALSE", pclShowRegn, sizeof pclShowRegn, GetConfigPath() );

    GetPrivateProfileString(ogAppName, "BELT_ALLOC_WITHOUT_GATES ", "FALSE", pclBeltAlocWithoutGates, sizeof pclBeltAlocWithoutGates, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "BELT_ALLOC_EVENLY_DISTRIBUTION ", "FALSE", pclBeltAlocEqualDistribution, sizeof pclBeltAlocEqualDistribution, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "BELT_ALLOC_PREFER_EXACT_PARAMETERS ", "TRUE", pclBeltAllocReferExactParameters, sizeof pclBeltAllocReferExactParameters, GetConfigPath() );
    GetPrivateProfileString(ogAppName, "FWR_SORT_POS_BY_TYPE ", "FALSE", pclFWRSortPOSByType, sizeof pclFWRSortPOSByType, GetConfigPath() );


	// TSC 0909
	GetPrivateProfileString(ogAppName,  "SCENARIO_AUTOALLOCATE",   "FALSE", pclScenario_AutoAllocate, sizeof pclScenario_AutoAllocate, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "SCENARIO_CHECKINCOUNTER", "FALSE", pclScenario_CheckInCounter, sizeof pclScenario_CheckInCounter, GetConfigPath() );

	GetPrivateProfileString(ogAppName,  "NO_CHECKIN_CONFLICT_ACT", "FALSE", NoCheckInConflictAC, sizeof NoCheckInConflictAC, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "USE_CCA_RELOAD", "FALSE", pclUseCcaReload, sizeof pclUseCcaReload, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "AUTOALLOCATION_REMOVEPOSITION_IF_DIFFERENT", "TRUE", pclAutoAllocRemovePosIfDifferent, sizeof pclAutoAllocRemovePosIfDifferent, GetConfigPath() );

	GetPrivateProfileString(ogAppName,  "CHECK_PRFL", "FALSE", pclCheckPRFL, sizeof pclCheckPRFL, GetConfigPath() );

	GetPrivateProfileString(ogAppName,  "CONFLICT_NEW_FLIGHT", "FALSE", pclConflictNewFlight, sizeof pclConflictNewFlight, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "POSRULE_WITH_REGN", "FALSE", pclRuleforAcrRegn, sizeof pclRuleforAcrRegn, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "BELTRULE_WITH_FLTI", "FALSE", pclConflictBeltFlti, sizeof pclConflictBeltFlti, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "DIFFERENT_COLOURS_FLIGHTID", "FALSE", pclDiffColoursID, sizeof pclDiffColoursID, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "DISPLAY_CCA_GROUPS_IN_CHART", "FALSE", pclDisplayCcaGroups, sizeof pclDisplayCcaGroups, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "DAILY_SCHEDULE_DISP_PAID", "TRUE", pclDisplayPaid, sizeof pclDisplayPaid, GetConfigPath() );

	GetPrivateProfileString(ogAppName,  "NEW_GOOD_COUNT", "FALSE", pclNewGoodCount, sizeof pclNewGoodCount, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "FACTOR_PRIO", "1.0", pclFactorPrio, sizeof pclFactorPrio, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "FACTOR_SEQU", "0.1", pclFactorSequ, sizeof pclFactorSequ, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "FACTOR_GOOD_COUNT", "1.8", pclFactorGoodCount, sizeof pclFactorGoodCount, GetConfigPath() );

	GetPrivateProfileString(ogAppName,  "CONFLICTS_FOR_AIRB", "TRUE", pclConflictsForAirb, sizeof pclConflictsForAirb, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "ALLOCATION_OVERVIEW", "FALSE", pclAllocationOverview, sizeof pclAllocationOverview, GetConfigPath() );

	GetPrivateProfileString("GLOBAL",  "UTCCONVERTIONS", "YES", pclUTCCONVERTIONS, sizeof pclUTCCONVERTIONS, GetConfigPath() );

	GetPrivateProfileString(ogAppName,  "FACTOR_CHECKIN", "", pclFactor_CCA, sizeof pclFactor_CCA, GetConfigPath() );
	ogFactor_CCA = pclFactor_CCA;

	GetPrivateProfileString(ogAppName,  "CONF_FROM_CFLTAB", "FALSE", pclConfFromCfltab, sizeof pclConfFromCfltab, GetConfigPath() );

	GetPrivateProfileString(ogAppName,  "AUTO_CCA_ALLOCATE_WITH_VACANCY", "FALSE", pclAutoAllocVacancy, sizeof pclAutoAllocVacancy, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "FWR_GATE_WITH_ARRIVAL", "FALSE", pclNewFwRGates, sizeof pclNewFwRGates, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "AUTO_ALLOCATE_GATE_ROTATION", "FALSE", pclAutoAllocGateFetchRotation, sizeof pclAutoAllocGateFetchRotation, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "FWR_WITHOUT_GLOBAL_NATURE", "FALSE", pclAutoAllocGateWithoutGlobalNature, sizeof pclAutoAllocGateWithoutGlobalNature, GetConfigPath() );

	GetPrivateProfileString(ogAppName,  "REINSTADED_CONFLICTS", "FALSE", pclReinstadedConflicts, sizeof pclReinstadedConflicts, GetConfigPath() );

/*	not in use
	GetPrivateProfileString(ogAppName,  "COMMON_CHECKIN_AIRLINE_GROUP", "FALSE", pclCheckinAirline, sizeof pclCheckinAirline, GetConfigPath() );
*/
	GetPrivateProfileString(ogAppName,  "NATURE_AUTO_CALC", "FALSE", pclNatureAutoCalc, sizeof pclNatureAutoCalc, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "NATURE_SERVICE_MANDATORY", "FALSE", pclNatureServiceMand, sizeof pclNatureServiceMand, GetConfigPath() );
    
	GetPrivateProfileString(ogAppName,  "ADDITIONAL_EVAL_ROTMASK", "FALSE", pclRotationmask, sizeof pclRotationmask, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "DAILYROTATION_RESOURCE_CHAINDLG", "FALSE", pclDailyRotChainDlg, sizeof pclDailyRotChainDlg, GetConfigPath() );
	//if no errors will be dedected then set this to true in global and remove here
	GetPrivateProfileString(ogAppName,  "REPORTS_MASK", "FALSE", pclReports, sizeof pclReports, GetConfigPath() );
	GetPrivateProfileString(ogAppName, "PREFIX_REPORTS", "",pclPrefixReports, sizeof pclPrefixReports, GetConfigPath());

	GetPrivateProfileString(ogAppName,  "CHECKIN_REMARK_EDIT", "FALSE", pclCheckinRemarkEdit, sizeof pclCheckinRemarkEdit, GetConfigPath() );
	GetPrivateProfileString(ogAppName,  "TOWING_POPUP", "FALSE", pclTowingPopup, sizeof pclTowingPopup, GetConfigPath() );
	//PRF 8084
	GetPrivateProfileString(ogAppName,  "PREVIOUSDAY_CHECKIN", "FALSE", pclPreviousDayCheckIn, sizeof pclPreviousDayCheckIn, GetConfigPath() );
	ogPrefixReports = pclPrefixReports;	

	//For PRF 8183
	GetPrivateProfileString(ogAppName,  "SPLITJOIN_MESSAGE_UTCTOLOCAL","FALSE", pclSplitJoinLocal,sizeof pclSplitJoinLocal,GetConfigPath());
	GetPrivateProfileString(ogAppName,  "CONFLICTUTCTOLOCAL", "FALSE", pclConflictUtctoLocal, sizeof pclConflictUtctoLocal, GetConfigPath() );
	//Added a ceda.ini entry for the updation of the connected gates for the Position ,PRF 8494 by sisl
	GetPrivateProfileString(ogAppName,  "RELATEDGATETOPOSITION","FALSE", pclRelatedGatePosition,sizeof pclRelatedGatePosition,GetConfigPath());

	// For Lisbon Terminal 2 
	GetPrivateProfileString(ogAppName,  "SECONDGATEDEMAND","FALSE", pclSecondGateDemandFlag,sizeof pclSecondGateDemandFlag,GetConfigPath());

	// For Lisbon Terminal 2 
	GetPrivateProfileString(ogAppName,  "GATE_FIDS_REMARK","FALSE", pclGateFIDSRemark,sizeof pclGateFIDSRemark,GetConfigPath());

	// For Lisbon Terminal 2 
	GetPrivateProfileString(ogAppName,  "ALLOW_CKI_OVERLAP","NO", pclAllowCcaOverlap,sizeof pclAllowCcaOverlap,GetConfigPath());

	//Get the setting from ceda.ini
	// For GOCC - To show the button to launch the "Flight Data Tool Application" 
	GetPrivateProfileString(ogAppName,  "SHOW_FLIGHT_DATA_TOOL_BUTTON","FALSE", pclShowFlightDataToolButton,sizeof pclShowFlightDataToolButton, GetConfigPath());
	GetPrivateProfileString(ogAppName,  "SHOW_POPUP_IMG_MENU","FALSE", pclShowPopupImgMenu,sizeof pclShowFlightDataToolButton, GetConfigPath());

	GetPrivateProfileString("BDPS-UIF",  "UNICODE","FALSE", pclUNICODERemarks,sizeof pclUNICODERemarks, GetConfigPath());

	// RFC8784 DXB
	GetPrivateProfileString(ogAppName,  "POSITION_TERMINAL_CONFLICT","FALSE", pclPositionTerminalConflict,sizeof pclPositionTerminalConflict, GetConfigPath());

	
	

	


    if (strcmp(pclPositionTerminalConflict,"TRUE")==0)
		bgPositionTerminalConflict = true;
	else
		bgPositionTerminalConflict = false;


	// RFC 8768 ADR
	GetPrivateProfileString(ogAppName,  "KEYCODE_FILTER_BATCH_EXPAND","FALSE", pclStevFilterForBatchandExpand,sizeof pclPositionTerminalConflict, GetConfigPath());

    if (strcmp(pclStevFilterForBatchandExpand,"TRUE")==0)
		bgStevFilterForBatchandExpand = true;
	else
		bgStevFilterForBatchandExpand = false;


	// RFC 8925 ADR
	GetPrivateProfileString(ogAppName,  "KEYCODE_FILTER_DEFAULT","", pclKeyCodeFilterDefault,sizeof pclKeyCodeFilterDefault, GetConfigPath());

	ogKeyCodeFilterDefault = CString(pclKeyCodeFilterDefault);

	// RFC 8963 ADR
	GetPrivateProfileString(ogAppName,  "POSITION_CONFIRMATION","FALSE", pclPositionConfirmation,sizeof pclPositionConfirmation, GetConfigPath());

	if( strcmp(pclPositionConfirmation,"TRUE")==0)
		bgPositionConfirmation = true;
	else
		bgPositionConfirmation = false;



	// RFC 9004 ADR
	GetPrivateProfileString(ogAppName,  "POS_CUT_PASTE","FALSE", pclPosCutPaste,sizeof pclPosCutPaste, GetConfigPath());

	if( strcmp(pclPosCutPaste,"TRUE")==0)
		bgPosCutPaste = true;
	else
		bgPosCutPaste = false;



	// RFC 9128 WAW Logos on Gate and CKI Displays 
	GetPrivateProfileString(ogAppName,  "FIDSLOGOS","FALSE", pclFIDSLogo,sizeof pclFIDSLogo, GetConfigPath());

	if( strcmp(pclFIDSLogo,"TRUE")==0)
		bgFIDSLogo = true;
	else
		bgFIDSLogo = false;


	// RFC 9030 WAW new function connecting Flight with Service and Handling Agent  

	GetPrivateProfileString(ogAppName,  "HANDLING_AGENT_ASSIGNMENT","FALSE", pclEnhancedHandlingAgentAssignment,sizeof pclEnhancedHandlingAgentAssignment, GetConfigPath());

	if( strcmp(pclEnhancedHandlingAgentAssignment,"TRUE")==0)
		bgEnhancedHandlingAgentAssignment = true;
	else
		bgEnhancedHandlingAgentAssignment = false;


	// RFC WAW 9029 Scroll-down list with flight cancellation reasons

	GetPrivateProfileString(ogAppName,  "REASON_FOR_CXX","FALSE", pclCxxReason,sizeof pclCxxReason, GetConfigPath());

	if( strcmp(pclCxxReason,"TRUE")==0)
		bgCxxReason = true;
	else
		bgCxxReason = false;


	// RFC WAW 9032 new checkbox/timefield for night curfew exception 

	GetPrivateProfileString(ogAppName,  "NIGHT_FLIGHT_SLOT","FALSE", pclNightFlightSlot,sizeof pclNightFlightSlot, GetConfigPath());

	if( strcmp(pclNightFlightSlot,"TRUE")==0)
		bgNightFlightSlot = true;
	else
		bgNightFlightSlot = false;



	GetPrivateProfileString(ogAppName,  "CHUTE_DISPLAY","FALSE", pclChuteDisplay,sizeof pclChuteDisplay, GetConfigPath());

	if( strcmp(pclChuteDisplay,"TRUE")==0)
		bgChuteDisplay = true;
	else
		bgChuteDisplay = false;


	GetPrivateProfileString(ogAppName,  "MERGE_BLK","FALSE", pclMergeBlk,sizeof pclMergeBlk, GetConfigPath());

	if( strcmp(pclMergeBlk,"TRUE")==0)
		bgMergeBlk = true;
	else
		bgMergeBlk = false;



	GetPrivateProfileString(ogAppName,  "MERGE_BLK","FALSE", pclMergeBlk,sizeof pclMergeBlk, GetConfigPath());

	if( strcmp(pclMergeBlk,"TRUE")==0)
		bgMergeBlk = true;
	else
		bgMergeBlk = false;
	

	GetPrivateProfileString(ogAppName,  "ADDITIONAL_REMARK_FIELD","FALSE", pclAdditionalRemark,sizeof pclAdditionalRemark, GetConfigPath());

	if( strcmp(pclAdditionalRemark,"TRUE")==0)
		bgAdditionalRemark = true;
	else
		bgAdditionalRemark = false;


	GetPrivateProfileString(ogAppName,  "PRINT_ARRIVAL_GATE","FALSE", pclPrintArrivalGate,sizeof pclPrintArrivalGate, GetConfigPath());

	if( strcmp(pclPrintArrivalGate,"TRUE")==0)
		bgPrintArrivalGate = true;
	else
		bgPrintArrivalGate = false;
	



	GetPrivateProfileString(ogAppName,  "EXCEL_FL_BAG","FALSE", pclExcelFLBag,sizeof pclExcelFLBag, GetConfigPath());

	if( strcmp(pclExcelFLBag,"TRUE")==0)
		bgExcelFLBag = true;
	else
		bgExcelFLBag = false;


	// LIS CDM
	GetPrivateProfileString(ogAppName,  "EXTERNALCONFLICTS","", pclExtConflicts,sizeof pclExtConflicts, GetConfigPath());

	ogExtConflicts = CString( pclExtConflicts);



	GetPrivateProfileString(ogAppName,  "CDM_PHASE1","FALSE", pclCDMPhase1, sizeof pclCDMPhase1, GetConfigPath());

	if( strcmp(pclCDMPhase1,"TRUE")==0)
		bgCDMPhase1 = true;
	else
		bgCDMPhase1 = false;




	GetPrivateProfileString(ogAppName,  "BELT_SHOW_MAX_PAX","FALSE", pclBltShowMaxPax,sizeof pclBltShowMaxPax, GetConfigPath());

	if( strcmp(pclBltShowMaxPax,"TRUE")==0)
		bgBltShowMaxPax = true;
	else
		bgBltShowMaxPax = false;



	GetPrivateProfileString(ogAppName,  "COLOR_DISABLED_FIELDS_ROTATIONDLG","FALSE", pclColorDisabledFields,sizeof pclColorDisabledFields, GetConfigPath());

	if( strcmp(pclColorDisabledFields,"TRUE")==0)
		bgColorDisabledFields = true;
	else
		bgColorDisabledFields = false;


 	// enable Departure Belts
	GetPrivateProfileString(ogAppName,  "ENABLE_DEPARTUREBELTS","NO", pclEnableDepBelts,sizeof pclEnableDepBelts, GetConfigPath());

    if (strcmp(pclEnableDepBelts,"YES")==0)
		bgUseDepBelts = true;
	else
		bgUseDepBelts = false;

	// enable Batch processing without selected flight
	GetPrivateProfileString(ogAppName,  "ENABLE_EMPTYBATCH","NO", pclEnableEmptyBatch,sizeof pclEnableEmptyBatch, GetConfigPath());

    if (strcmp(pclEnableEmptyBatch,"YES")==0)
		bgUseEmptyBatch = true;
	else
		bgUseEmptyBatch = false;

	// enable Flno select list in Batch processing load other flight number
	GetPrivateProfileString(ogAppName,  "ENABLE_FLNOSELECT","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());

    if (strcmp(pclTmpString,"YES")==0)
		bgEnableFlnoSelect = true;
	else
		bgEnableFlnoSelect = false;

	
	// enable Registration if Seasonal General Filter View
	GetPrivateProfileString(ogAppName,  "ENABLEREGSEARCH","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());

    if (strcmp(pclTmpString,"YES")==0)
		bgUseRegInSeasonSearch = true;
	else
		bgUseRegInSeasonSearch = false;

		// enable Registration if Seasonal General Filter View
	GetPrivateProfileString(ogAppName,  "ENABLEREGINSERT","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());

    if (strcmp(pclTmpString,"YES")==0)
		bgEnableRegInsert = true;
	else
		bgEnableRegInsert = false;

	// enable 3 additional keyfields
	GetPrivateProfileString(ogAppName,  "USEADDKEYCODES","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());

    if (strcmp(pclTmpString,"YES")==0)
		bgUseAddKeyfields = true;
	else
		bgUseAddKeyfields = false;
	// enable 3 additional keyfields
	GetPrivateProfileString(ogAppName,  "ADDKEYCODES_DR","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());

    if (strcmp(pclTmpString,"YES")==0)
		bgAddKeyCodesDR = true;
	else
		bgAddKeyCodesDR = false;

	// enable 3 additional keyfields
	GetPrivateProfileString(ogAppName,  "ADDKEYCODES_ST","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());

    if (strcmp(pclTmpString,"YES")==0)
		bgAddKeyCodesST = true;
	else
		bgAddKeyCodesST = false;
	// enable 3 additional keyfields
	GetPrivateProfileString(ogAppName,  "ADDKEYCODES_DS","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());

    if (strcmp(pclTmpString,"YES")==0)
		bgAddKeyCodesDS = true;
	else
		bgAddKeyCodesDS = false;
	
	// enable conflict priority
	GetPrivateProfileString(ogAppName,  "CONFLICTS_PRIORITY","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());

    if (strcmp(pclTmpString,"YES")==0)
		bgConflictPriority = true;
	else
		bgConflictPriority = false;
	
	// enable Display of Wingspan and length in Rotation Dlg.
	GetPrivateProfileString(ogAppName,  "SHOWWINGSPAN","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());

    if (strcmp(pclTmpString,"YES")==0)
		bgShowWingspan = true;
	else
		bgShowWingspan = false;



	// use select lists for APT, ACT,TTYP and ALC in Flight Search Dialog.
	GetPrivateProfileString(ogAppName,  "USEAPTACTNATLIST","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());

    if (strcmp(pclTmpString,"YES")==0)
		bgUseAptActNatAlcList = true;
	else
		bgUseAptActNatAlcList = false;


	GetPrivateProfileString(ogAppName,  "SHOW_FLBAG","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());

    if (strcmp(pclTmpString,"YES")==0)
		bgFLBag = true;
	else
		bgFLBag = false;

	GetPrivateProfileString(ogAppName,  "SHOW_CSTAD","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());
    if (strcmp(pclTmpString,"YES")==0)
		bgShowCSTAD = true;
	else
		bgShowCSTAD = false;

	
	GetPrivateProfileString(ogAppName,  "WINGOVERLAPBUFFER","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());
    if (strcmp(pclTmpString,"YES")==0)
		bgWingoverBuffer = true;
	else
		bgWingoverBuffer = false;


	//AM:20101015 - To launch the multi delay tool from FIPS					
	GetPrivateProfileString(ogAppName,  "USE_MULTI_DELAY_TOOL","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"YES")==0)					
		bgUseMultiDelayTool = true;				
	else					
		bgUseMultiDelayTool = false;				

	// UFIS-361 Indication in the Aircraft Position gantt chart of VIP flights (for BKK AOT).
	GetPrivateProfileString(ogAppName,  "VIPFLAG","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"YES")==0)					
		bgVIPFlag = true;				
	else					
		bgVIPFlag = false;				


	// UFIS-362 Indication of positions confirmed by airlines in the Aircraft Position gantt chart (for BKK AOT).
	GetPrivateProfileString(ogAppName,  "FIXED_RES","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"YES")==0)					
		bgFixedRes = true;				
	else					
		bgFixedRes = false;				

	// UFIS-334 Modify or New VIP mask (for BKK AOT).
	GetPrivateProfileString(ogAppName,  "NEW_VIP","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"YES")==0)					
		bgNewVIPDlg = true;				
	else					
		bgNewVIPDlg = false;				


	// UFIS-361 Show VIP checkbox near the Boarding Bridge (for AUH).
	GetPrivateProfileString(ogAppName,  "SHOW_VIP_NEAR_BRIDGE","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgShowVIPNearBridge = true;				
	else					
		bgShowVIPNearBridge = false;

	
	GetPrivateProfileString(ogAppName,  "USE_REQUESTED_FOR_PROGNOSIS","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgUseRequestedForPrognosis = true;				
	else					
		bgUseRequestedForPrognosis = false;

	// UFIS-961 Show Payment Details in Daily schedule and rotation masks
	GetPrivateProfileString(ogAppName,  "SHOW_PAY_DETAILS","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgShowPayDetail = true;				
	else					
		bgShowPayDetail = false;


	GetPrivateProfileString(ogAppName,  "DAILY_FIXED_RES","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgDailyFixedRes = true;				
	else					
		bgDailyFixedRes = false;

	GetPrivateProfileString(ogAppName,  "SEASON_FIXED_RES","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgSeasonFixedRes = true;				
	else					
		bgSeasonFixedRes = false;


	GetPrivateProfileString(ogAppName,  "DAILY_SHOW_CHOCKS","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgShowChock = true;				
	else					
		bgShowChock = false;

	GetPrivateProfileString(ogAppName,  "DAILY_ROT_AUTO_HIGHLIGHT","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgDailyHightligh = true;				
	else					
		bgDailyHightligh = false;

	GetPrivateProfileString(ogAppName,  "DAILY_HIDE_INSERT_BUTTON","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgDailyHideInsert = true;				
	else					
		bgDailyHideInsert = false;

	GetPrivateProfileString(ogAppName,  "DAILY_ROT_SHOW_VIP","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgDailyShowVIP = true;				
	else					
		bgDailyShowVIP = false;

	// UFIS-989 earch/Filter with flight number in Conflict list
	GetPrivateProfileString(ogAppName,  "CONFLICT_SEARCH","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgCanSearchConflict = true;				
	else					
		bgCanSearchConflict = false;

	// Show AOG flag
	GetPrivateProfileString(ogAppName,  "DAILY_SHOW_AOG_FLAG","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgShowAOG = true;				
	else					
		bgShowAOG = false;

	// Show ADHOC flag
	GetPrivateProfileString(ogAppName,  "DAILY_SHOW_ADHOC_FLAG","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgShowADHOC = true;				
	else					
		bgShowADHOC = false;

	// Show ADHOC flag
	GetPrivateProfileString(ogAppName,  "SEASON_SHOW_ADHOC_FLAG","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgSeasonShowADHOC = true;				
	else					
		bgSeasonShowADHOC = false;

	GetPrivateProfileString(ogAppName,  "DAILY_SHOW_EFPS","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgShowEFPS = true;				
	else					
		bgShowEFPS = false;

	GetPrivateProfileString(ogAppName,  "SEASON_SHOW_PERMIT","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgShowSeasonPermit = true;				
	else					
		bgShowSeasonPermit = false;


	GetPrivateProfileString(ogAppName, DE_ICING_CFG_NAME, "", pclTmpString, sizeof pclTmpString, __GetConfigPath());	
	ogDeicingApplName = CString(pclTmpString);

	char clTempBltAlloc[10];
	GetPrivateProfileString( ogAppName, "BELTRULE_ALLOC", "FALSE", clTempBltAlloc, sizeof clTempBltAlloc, __GetConfigPath() );
	bgUseBeltAllocTime = !strncmp( clTempBltAlloc, "TRUE", 5 );

	char clTempGroupString[20];
	GetPrivateProfileString( ogAppName, "DIA_GROUPING", "FALSE", clTempGroupString, sizeof clTempGroupString, __GetConfigPath() );
	if (strcmp(clTempGroupString,"TRUE")==0)					
		bgDiaGrouping = true;				
	else					
		bgDiaGrouping = false;				

	// UFIS-957 Blacklist / watchlist for aircraft registrations, aircraft types and airlines.
	GetPrivateProfileString(ogAppName,  "BLACKLIST","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgBlackList = true;				
	else					
		bgBlackList = false;


	// UFIS-1143  Use of Centerline Distance between stands instead of "restricted to" 
	GetPrivateProfileString(ogAppName,  "CENTERLINE_DIST","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgUseCenterline = true;				
	else					
		bgUseCenterline = false;


	// UFIS-1033 Include PAX Load in Rules of Parking Stands. 
	GetPrivateProfileString(ogAppName,  "POS_MINMAXPAX","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgPosRulePax = true;				
	else					
		bgPosRulePax = false;


	// UFIS-984 Baggage rule shall use either No. of PAX or No. of Bags 
	GetPrivateProfileString(ogAppName,  "BELT_MAXBAG","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgBltRuleBag = true;				
	else					
		bgBltRuleBag = false;


	// UFIS-1226 Adjustment of auto allocation  
	GetPrivateProfileString(ogAppName,  "AUTO_ALLOC_WITH_SCORE","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgAutoAllocWithScore = true;				
	else					
		bgAutoAllocWithScore = false;


	// UFIS-1229 auto towings  
	GetPrivateProfileString(ogAppName,  "TOWING_DEMAND","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgTowingDemand = true;				
	else					
		bgTowingDemand = false;

	// UFIS-987 To indicate "Ready for Towing"
	GetPrivateProfileString(ogAppName,  "READY_FOR_TOWING","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"YES")==0)					
		bgReadyForTowing = true;				
	else					
		bgReadyForTowing = false;

	// UFIS-1253 Upon searching a flight in the Gantt Chart, highlight the flight if found instead of providing the "Detailed Mask"
	GetPrivateProfileString(ogAppName,  "SHOW_DETAIL_AFTER_SEARCH","TRUE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgShowDetailAfterSearch = true;				
	else					
		bgShowDetailAfterSearch = false;


	// UFIS-991 Auto allocate: add time frame for auto allocation
	GetPrivateProfileString(ogAppName,  "AUTOALLOC_TIMEFRAME","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgAutoAllocTimeFrame = true;				
	else					
		bgAutoAllocTimeFrame = false;
	 
	// UFIS-985:Right mouse menu entry on position bar for creating a towing
	GetPrivateProfileString(ogAppName,  "CREATE_TOWING","NO", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"YES")==0)					
		bgCreateTowing = true;				
	else					
		bgCreateTowing = false;


	// UFIS-1035 common gate
	GetPrivateProfileString(ogAppName,  "COMMON_GATE","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgCommonGate = true;				
	else					
		bgCommonGate = false;

	// UFIS-1035 common gate
	GetPrivateProfileString(ogAppName,  "COMMON_GATE_BUFFER","0", pclTmpString,sizeof pclTmpString, GetConfigPath());					

	int ilCommonGatBuffer = atoi(pclTmpString);
	ogCommonGateBeginBuffer = CTimeSpan(0,0,ilCommonGatBuffer,0);



	// UFIS-982 Check-in Counter allocation: compare the "local passenger load" against a service value (e.g. 50 passengers per counter) and generate a conflict
	GetPrivateProfileString(ogAppName,  "PAX_PER_COUNTER","0", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	igPaxPerCounter = atoi(pclTmpString);


	// FIS-1265 position allocation for single arrival flights
	GetPrivateProfileString(ogAppName,  "SHOW_ONGROUNDFLIGHTS_IN_FUTURE","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgOngroundInFuture = true;				
	else					
		bgOngroundInFuture = false;


	// FIS-1265 position allocation for single arrival flights
	GetPrivateProfileString("Global",  "USE_NUMTAB_KEYS","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"YES")==0)					
		bgNumTabKeys = true;				
	else					
		bgNumTabKeys = false;

	
	// UFIS-978 The operator shall be able to roll-back unsaved allocations
	GetPrivateProfileString(ogAppName,  "UNDO","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgUndo = true;				
	else					
		bgUndo = false;


	// UFIS-1892 Change in Gates Gantt Chart
	GetPrivateProfileString(ogAppName,  "GATE_BUFFER","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgGateOverlapConflictWithBuffer = true;				
	else					
		bgGateOverlapConflictWithBuffer = false;


	// UFIS-1536 Include an indicator that the towing arrival stand is available to receive the towed aircraft
	GetPrivateProfileString(ogAppName,  "TOW_ARRIVAL_POS_FREE_INDICATOR","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgTowArrPosFree = true;				
	else					
		bgTowArrPosFree = false;				



	// enable 3 additional keyfields
	int ilFidsNum = atoi(pclFidsRemark);
	switch(ilFidsNum)
	{
		case 1: ogFIDRemarkField = "BEME"; break;
		case 2: ogFIDRemarkField = "BEMD"; break;
		case 3: ogFIDRemarkField = "BET3"; break;
		case 4: ogFIDRemarkField = "BET4"; break;
		default: ogFIDRemarkField = "BEMD"; break;
	}


	if( strcmp(pclUNICODERemarks,"YES")==0)
	{
		bgUNICODERemarks = true;
		ogFIDRemarkField = "BEME";
	}
	else
	{
		bgUNICODERemarks = false;
	}


	GetPrivateProfileString(ogAppName,  "GANTT_CHART_HIGHLIGHT_COLOR","-1", pclGanttChartHLColor,sizeof pclGanttChartHLColor, GetConfigPath());
	GetPrivateProfileString(ogAppName,  "POSITION_TEXT","", pclPositionText ,sizeof pclPositionText, GetConfigPath());
	if (strcmp(pclPositionText,"")!=0)
		ogPositionText = pclPositionText;
	ogPositionText.TrimRight();
	ogPositionText.TrimLeft();
	//Assign the setting to global variable, to show or hide the button
    if (strcmp(pclShowFlightDataToolButton,"TRUE")==0)
		bgShowFlightDataToolButton = true;
	else
		bgShowFlightDataToolButton = false;

	//Assign the setting to global variable, to activate the popup image menu
    if (strcmp(pclShowPopupImgMenu,"TRUE")==0)
		bgShowPopupImgMenu  = true;
	else
		bgShowPopupImgMenu  = false;

	//Assign the setting to change the highlight color in GanttChart
	bgHasGanttChartHLColor = false;
	if (strcmp(pclGanttChartHLColor,"-1")!=0)
	{
		try{
			ulgGanttChartHLColor = strtoul( pclGanttChartHLColor, NULL, 0 );
			pogHLBrush = new CBrush(ulgGanttChartHLColor);
			bgHasGanttChartHLColor = true;
		}catch(...)
		{
		}
	}


	if(strcmp(pclGateFIDSRemark,"YES")==0)
		bgGateFIDSRemark=true;
	else
        bgGateFIDSRemark=false;



	if(strcmp(pclAllowCcaOverlap,"YES")==0)
		bgAllowCcaOverlap=true;
	else
        bgAllowCcaOverlap=false;



	if(strcmp(pclSecondGateDemandFlag,"YES")==0)
		bgSecondGateDemandFlag=true;
	else
        bgSecondGateDemandFlag=false;

	//Delay Button for Arrival section
	GetPrivateProfileString(ogAppName,  "SHOW_DELAY_ARRIVAL","FALSE", pclShowDelayArrival,sizeof pclShowDelayArrival, GetConfigPath());
	if(strcmp(pclShowDelayArrival,"TRUE")==0)
		bgShowDelayArrival=true;
	else
        bgShowDelayArrival=false;

	//UFIS-1694 Edit Load Dialog should be accessible in offline gantt charts 
	GetPrivateProfileString(ogAppName,  "EDITLOAD_BAR_MENU","FALSE", pclShowDelayArrival,sizeof pclShowDelayArrival, GetConfigPath());
	if(strcmp(pclShowDelayArrival,"TRUE")==0)
		bgEditloadBarMenu = true;
	else
        bgEditloadBarMenu = false;




	ogDailyCedaFlightData.SetFieldList();
	ogDiaFlightData.SetFieldList();
	ogPosDiaFlightData.SetFieldList();
	ogRotationFlights.SetFieldList();
	ogRotationDlgFlights.SetFieldList();
	ogSeasonFlights.SetFieldList();
	ogSeasonCollectFlights.SetFieldList();
	ogSeasonDlgFlights.SetFieldList();

	ogPosDiaFlightData.bmGantt = true;


	//For Bangkok
	GetPrivateProfileString(ogAppName,  "CIC_CONFLICTCHECK", "YES", pclCicConflictCheck, sizeof pclCicConflictCheck, GetConfigPath() );
//	if (strcmp(pcgHome, "DXB") == 0)
	{
		bgNewGantDefault = true;
	}

//	if (strcmp(pcgHome, "BKK") == 0)
	{
		bgNewRecalculateResChain = true;
	}

	//excel enviroment
	if (getenv("CEDA") == NULL)
		strcpy(ogConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(ogConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
		ogExcelPath, sizeof ogExcelPath, ogConfigPath);

	GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
		ogExcelTrenner, sizeof ogExcelTrenner, ogConfigPath);
	
	// For Getting the Reason for change flag - PRF 8379 
	GetPrivateProfileString(ogAppName, "REASONFORCHANGES", "FALSE",	pclReasonFlag, sizeof pclReasonFlag, ogConfigPath);
	if(!strcmp(pclReasonFlag, "TRUE")) bgReasonFlag = true;
	else bgReasonFlag = false;
	

//    GetPrivateProfileString(ogAppName, "DATTAB_FOR_POS_DURATION", "FALSE", pclDatPosDura, sizeof pclDatPosDura, GetConfigPath() );
//    GetPrivateProfileString(ogAppName, "DATTAB_FOR_GAT_DURATION", "FALSE", pclDatGatDura, sizeof pclDatGatDura, GetConfigPath() );

	if (strlen(pclBCLogFile) != 0)
	{
		pogBCLog = new ofstream;
		pogBCLog->open(pclBCLogFile, ios::out | ios::trunc);
		pogBCLog->setf(ios::unitbuf);   

		(*pogBCLog) << "FIPS: Broadcast-Log begin " << CTime::GetCurrentTime().Format("%d.%m.%y %H:%M:%S") << "\n\n";
	}
	else
	{
		pogBCLog = NULL;
	}


	// logging for debugging
	bgDebug = false;
	if(strcmp(pclDebug, "DEFAULT") != 0)
	{
		bgDebug = true;
		of_debug.open( pclDebug, ios::out);

		CTime olCurrTime = CTime::GetCurrentTime();

		of_debug << "FIPS STARTED! (" << olCurrTime.Format("%d.%m.%Y %H:%M:%S") << ")" << endl;
		of_debug << "==========================================" << endl << endl;
	}

/*	not in use
	if(strcmp(pclCheckinAirline ,"TRUE")==0)
		bgCheckInAlGroup= true;
	else
	   bgCheckInAlGroup = false;
*/	
	
	if(strcmp(pclCicConflictCheck,"YES")==0)
		bgCicConflictCheck=true;
	else
        bgCicConflictCheck=false;
	
	//changed for the PRF 8494 
	if(strcmp(pclRelatedGatePosition,"TRUE")==0)
		bgRelatedGatePosition = true;
	else
        bgRelatedGatePosition = false;

	if(strcmp(pclReports,"TRUE")==0)
		bgReports=true;
	else
        bgReports=false;
	
	
	if(strcmp(pclRotationmask,"TRUE")==0)
		bgRotationmask=true;
	else
        bgRotationmask=false;
	
	if(strcmp(pclNatureAutoCalc, "TRUE") == 0)
		bgNatureAutoCalc = true;
	else
		bgNatureAutoCalc = false;


	if(strcmp(pclNatureServiceMand, "TRUE") == 0)
		bgNatureServiceMand = true;
	else
		bgNatureServiceMand = false;


	//For PRF 8183
	if(strcmp(pclSplitJoinLocal, "TRUE") == 0)
		bgSplitJoinLocal = true;
	else
		bgSplitJoinLocal = false;

	if(strcmp(pclConflictUtctoLocal,"TRUE")==0)
		bgConflictUtctoLocal=true;
	else
        bgConflictUtctoLocal=false;


	if(strcmp(pclCheckinRemarkEdit, "TRUE") == 0)
		bgcheckingRemarkEdit = true;
	else
		bgcheckingRemarkEdit = false;

	if(strcmp(pclTowingPopup, "TRUE") == 0)
		bgTowingPopup = true;
	else
		bgTowingPopup = false;
	 
    //PRF 8084
	if(strcmp(pclPreviousDayCheckIn, "TRUE") == 0)
		bgPreviousDaycheckIn = true;
	else
		bgPreviousDaycheckIn = false;

	if(strcmp(pclDailyRotChainDlg, "TRUE") == 0)
		bgDailyRotChainDlg = true;
	else
		bgDailyRotChainDlg = false;
	
	if(strcmp(pclReinstadedConflicts, "TRUE") == 0)
		bgReinstadedConflicts = true;
	else
		bgReinstadedConflicts = false;

	if(strcmp(pclAutoAllocGateWithoutGlobalNature, "TRUE") == 0)
		bgAutoAllocGateWithoutGlobalNature = true;
	else
		bgAutoAllocGateWithoutGlobalNature = false;

	if(strcmp(pclAutoAllocGateFetchRotation, "TRUE") == 0)
		bgAutoAllocGateFetchRotation = true;
	else
		bgAutoAllocGateFetchRotation = false;

	if(strcmp(pclNewFwRGates, "TRUE") == 0)
		bgNewFwRGates = true;
	else
		bgNewFwRGates = false;

	if(strcmp(pclAutoAllocVacancy, "TRUE") == 0)
		bgAutoAllocVacancy = true;
	else
		bgAutoAllocVacancy = false;

	if(strcmp(pclConfFromCfltab, "TRUE") == 0)
		bgConfFromCfltab = true;
	else
		bgConfFromCfltab = false;

	if(strcmp(pclNewGoodCount, "TRUE") == 0)
		bgNewGoodCount = true;
	else
		bgNewGoodCount = false;
	
	dgFactorPrio = atof(pclFactorPrio);
	dgFactorSequ = atof(pclFactorSequ);
	dgFactorGoodCount = atof(pclFactorGoodCount);

	if(strcmp(pclUTCCONVERTIONS, "NO") == 0)
		bgUTCCONVERTIONS = false;
	else
		bgUTCCONVERTIONS = true;

	if(strcmp(pclAllocationOverview, "TRUE") == 0)
		bgAllocationOverview = true;
	else
		bgAllocationOverview = false;

	if(strcmp(pclConflictsForAirb, "TRUE") == 0)
		bgConflictsForAirb = true;
	else
		bgConflictsForAirb = false;

	if(strcmp(pclDisplayPaid, "TRUE") == 0)
		bgDisplayPaid = true;
	else
		bgDisplayPaid = false;

	if(strcmp(pclDisplayCcaGroups, "TRUE") == 0)
		bgDisplayCcaGroups = true;
	else
		bgDisplayCcaGroups = false;

	if(strcmp(pclDiffColoursID, "TRUE") == 0)
		bgDiffColoursID = true;
	else
		bgDiffColoursID = false;

	if(strcmp(pclConflictBeltFlti, "TRUE") == 0)
		bgConflictBeltFlti = true;
	else
		bgConflictBeltFlti = false;

	if(strcmp(pclRuleforAcrRegn, "TRUE") == 0)
		bgRuleforAcrRegn = true;
	else
		bgRuleforAcrRegn = false;

	if(strcmp(pclConflictNewFlight, "TRUE") == 0)
		bgConflictNewFlight = true;
	else
		bgConflictNewFlight = false;


	if(strcmp(pclCheckPRFL, "TRUE") == 0)
		bgCheckPRFL = true;
	else
		bgCheckPRFL = false;

	if(strcmp(pclIncomApPos, "TRUE") == 0)
		blIncomApPos = true;
	else
		blIncomApPos = false;

	if(strcmp(pclIncomAlPos, "TRUE") == 0)
		blIncomAlPos = true;
	else
		blIncomApPos = false;

	if(strcmp(pclIncomApGat, "TRUE") == 0)
		blIncomApGat = true;
	else
		blIncomApGat = false;

	if(strcmp(pclIncomAlGat, "TRUE") == 0)
		blIncomAlGat = true;
	else
		blIncomAlGat = false;


	if(strcmp(pclAdjacentGatRestriction, "TRUE") == 0)
		bgAdjacentGatRestriction = true;
	else
		bgAdjacentGatRestriction = false;


	int ilGatBuffer = atoi(pclAdjacentGatBuffer);
	ogAdjacentGatBufferTime = CTimeSpan(0,0,ilGatBuffer,0);


	if(strcmp(pclAutoAllocRemovePosIfDifferent, "TRUE") == 0)
		bgAutoAllocRemovePosIfDifferent = true;
	else
		bgAutoAllocRemovePosIfDifferent = false;

	if(strcmp(pclUseCcaReload, "TRUE") == 0)
		bgUseCcaReload = true;
	else
		bgUseCcaReload = false;

	if(strcmp(NoCheckInConflictAC, "TRUE") == 0)
		blNoCheckInConflictAC = true;
	else
		blNoCheckInConflictAC = false;

	if(strcmp(pclFWRSortPOSByType, "TRUE") == 0)
		bgFWRSortPOSByType = true;
	else
		bgFWRSortPOSByType = false;

	if(strcmp(pclBeltAllocReferExactParameters, "TRUE") == 0)
		bgBeltAllocReferExactParameters = true;
	else
		bgBeltAllocReferExactParameters = false;

	if(strcmp(pclBeltAlocEqualDistribution, "TRUE") == 0)
		bgBeltAlocEqualDistribution = true;
	else
		bgBeltAlocEqualDistribution = false;

	if(strcmp(pclBeltAlocWithoutGates, "TRUE") == 0)
		bgBeltAlocWithoutGates = true;
	else
		bgBeltAlocWithoutGates = false;

	if(strcmp(pclShowOrgDes, "TRUE") == 0)
		bgShowOrgDes = true;
	else
		bgShowOrgDes = false;

	if(strcmp(pclShowRegn, "TRUE") == 0)
		bgShowRegn = true;
	else
		bgShowRegn = false;

	if(strcmp(pclShowMXAC, "TRUE") == 0)
		bgShowMXAC = true;
	else
		bgShowMXAC = false;

	if(strcmp(pclKeepExisting, "TRUE") == 0)
		bgKeepExisting = true;
	else
		bgKeepExisting = false;

	if(strcmp(pclKeepExistingCki, "TRUE") == 0)
		bgKeepExistingCki = true;
	else
		bgKeepExistingCki = false;

	if(strcmp(pclCreateAutoAllocFiles, "TRUE") == 0)
		bgCreateAutoAllocFiles = true;
	else
		bgCreateAutoAllocFiles = false;

	if(strcmp(pclPrintAutoAlloc, "TRUE") == 0)
		bgPrintAutoAlloc = true;
	else
		bgPrintAutoAlloc = false;

	if(strcmp(pclAutoAllocFirstRule, "TRUE") == 0)
		bgAutoAllocFirstRule = true;
	else
		bgAutoAllocFirstRule = false;

	int ilTowingSpan = atoi(pclTowingSpan);
	ogTowingSpan = CTimeSpan(0,0,ilTowingSpan,0);
	if (ogTowingSpan.GetTotalMinutes() >= 0)
		bgFastTowing = true;
	else
		bgFastTowing = false;

	ogResetAllocParameter = atoi(pclResetAllocParameter);

	if(strcmp(pclSeatsFrom, "ACR") == 0)
		ogSeatsFrom = "ACR";
	else
		ogSeatsFrom = "ACT";


	if(strcmp(pclDelaycode, "ALPHABETICAL") == 0)
		bgDelaycodeNumeric = false;
	else
		bgDelaycodeNumeric = true;

	if(strcmp(pclViewBigBobs, "TRUE") == 0)
		bgViewBigBobs = true;
	else
		bgViewBigBobs = false;

	if(strcmp(pclSeasonLocal, "TRUE") == 0)
		bgSeasonLocal = true;
	else
		bgSeasonLocal = false;


	if(strcmp(pclGatPosLocal, "TRUE") == 0)
		bgGatPosLocal = true;
	else
		bgGatPosLocal = false;



	//TSC 0909

	if(strcmp(pclScenario_AutoAllocate, "TRUE") == 0)
		bgscenario_autoallocate = true;
	else
		bgscenario_autoallocate = false;

	if(strcmp(pclScenario_CheckInCounter, "TRUE") == 0)
		bgscenario_checkincounter = true;
	else
		bgscenario_checkincounter = false;

	//END TSC


	if(strcmp(pclDailyLocal, "TRUE") == 0)
		bgDailyLocal = true;
	else
		bgDailyLocal = false;


	if(strcmp(pclDailyWithAlt, "TRUE") == 0)
		bgDailyWithAlt = true;
	else
		bgDailyWithAlt = false;

	if(strcmp(pclCreateDaily_OrgAtdBlue, "TRUE") == 0)
		bgCreateDaily_OrgAtdBlue = true;
	else
		bgCreateDaily_OrgAtdBlue = false;


	if(strcmp(pclViewEditFilter, "TRUE") == 0)
		bgViewEditFilter = true;
	else
		bgViewEditFilter = false;

	if(strcmp(pclViewEditSort, "TRUE") == 0)
		bgViewEditSort = true;
	else
		bgViewEditSort = false;



	if(strcmp(pclShowOnGround, "TRUE") == 0)
		bgShowOnGround = true;
	else
		bgShowOnGround = false;

	if(strcmp(pclCheckAllConflicts, "TRUE") == 0)
		bgCheckAllConflicts = true;
	else
		bgCheckAllConflicts = false;

	int ilGroundTimelimit = atoi(pclViewOnGroundTimelimit);
	ogOnGroundTimelimit = CTimeSpan(0,0,ilGroundTimelimit,0);

	if(strcmp(pclBltHeight, "TRUE") == 0)
		bgBltHeight = true;
	else
		bgBltHeight = false;

	if(strcmp(pclCcaHeight, "TRUE") == 0)
		bgCcaHeight = true;
	else
		bgCcaHeight = false;

	if(strcmp(pclGatHeight, "TRUE") == 0)
		bgGatHeight = true;
	else
		bgGatHeight = false;

	if(strcmp(pclPosHeight, "TRUE") == 0)
		bgPosHeight = true;
	else
		bgPosHeight = false;

	if(strcmp(pclWroHeight, "TRUE") == 0)
		bgWroHeight = true;
	else
		bgWroHeight = false;

	double dlOverlapHeight = atof(pclOverlapHeight) / 100;
	if (dlOverlapHeight > 0.05)
		dgOverlapHeight = dlOverlapHeight;
	else
		dgOverlapHeight = 0.10;

//FWR
	if(strcmp(pclFwrPos, "TRUE") == 0)
		bgFwrPos = true;
	else
		bgFwrPos = false;

	if(strcmp(pclFwrGat, "TRUE") == 0)
		bgFwrGat = true;
	else
		bgFwrGat = false;

	if(strcmp(pclFwrBlt, "TRUE") == 0)
		bgFwrBlt = true;
	else
		bgFwrBlt = false;

	if(strcmp(pclFwrWro, "TRUE") == 0)
		bgFwrWro = true;
	else
		bgFwrWro = false;

	if(strcmp(pclFwrCic, "TRUE") == 0)
		bgFwrCic = true;
	else
		bgFwrCic = false;
//FWR


	if(strcmp(pclOffRelease, "TRUE") == 0)
	{
		bgOffRelFuncGatpos = true;
		bgOffRelFuncCheckin = true;
	}
	else
	{
		bgOffRelFuncGatpos = false;
		bgOffRelFuncCheckin = false;
	}

	if(strcmp(pclAirport4LC, "TRUE") == 0)
		bgAirport4LC = true;
	else
		bgAirport4LC = false;

	if(strcmp(pclRealLocal, "TRUE") == 0)
		bgRealLocal = true;
	else
		bgRealLocal = false;


	//rkr20042001
	// this part is only to perform the value to the next version
	// the posbuffer now is stored in an own record of cedacfgdata.
	// you can remove this part and delete the entry in ceda.ini by time
	int ilPosBuffer = atoi(pclPosBuffer);
	ogPosAllocBufferTime = CTimeSpan(0,0,ilPosBuffer,0);
	// you can remove this part and delete the entry in ceda.ini by time
	//rkr20042001





/*	// set postflight days
	int ilPostFlightDays = atoi(pclPostFlightDays);
	if (ilPostFlightDays > 0)
	{
		ogTimeSpanPostFlight = CTimeSpan(ilPostFlightDays, 0, 0, 0);
	}
*/
	//Added by Christine
	//To show class for CCA in Batch Processing batch
	if(strcmp(pclShowClassInBatchDlg, "TRUE") == 0)
		bgShowClassInBatchDlg = true;
	else
		bgShowClassInBatchDlg = false;

	GetPrivateProfileString(ogAppName, "DAILY_SHOW_TWO_CASH_BUTTONS"  , "FALSE", pclbgShowCashButtonInBothFlight, sizeof pclbgShowCashButtonInBothFlight, GetConfigPath() );//Added by Christine
	if(strcmp(pclbgShowCashButtonInBothFlight, "TRUE") == 0)
		bgShowCashButtonInBothFlight = true;
	else
		bgShowCashButtonInBothFlight = false;

	GetPrivateProfileString(ogAppName, "DAILY_COPY_CASH_PAY_TOOTHER"  , "FALSE", pclCopyCashToOther, sizeof pclCopyCashToOther, GetConfigPath() );//Added by Christine
	if(strcmp(pclCopyCashToOther, "TRUE") == 0)
		bgCopyCashToOther = true;
	else
		bgCopyCashToOther = false;

	GetPrivateProfileString(ogAppName, "ACTIVITIES"  , "FALSE", pclDailyShowActivities, sizeof pclDailyShowActivities, GetConfigPath() );//Added by Christine
	if(strcmp(pclDailyShowActivities, "FALSE") == 0)
		bgDailyShowActivities = false;
	else
		bgDailyShowActivities = true;


	// UFIS-	FLNO in Pos rule
	GetPrivateProfileString(ogAppName,  "PST_RULE_FLNO","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());					
	if (strcmp(pclTmpString,"TRUE")==0)					
		bgPstRuleFlno = true;				
	else					
		bgPstRuleFlno = false;


	if(CString(pcgHome) != "BKK")
		ogRotationDlgAutoLockFields = "";

	GetPrivateProfileString(ogAppName,  "SERVICE_MANDATORY","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());
    if (strcmp(pclTmpString,"TRUE")==0)
		bgServiceMandatory = true;
	else
		bgServiceMandatory = false;

	GetPrivateProfileString(ogAppName,  "NATURE_MANDATORY","TRUE", pclTmpString,sizeof pclTmpString, GetConfigPath());
    if (strcmp(pclTmpString,"TRUE")==0)
		bgNatureMandatory = true;
	else
		bgNatureMandatory = false;

	//UFIS-1070
	GetPrivateProfileString(ogAppName,  "HANDLING_AGENT_ALLOW_CHG_AGENT","TRUE", pclTmpString,sizeof pclTmpString, GetConfigPath());
    if (strcmp(pclTmpString,"TRUE")==0)
		bgHAAllowChangeAgent = true;
	else
		bgHAAllowChangeAgent = false;

	GetPrivateProfileString(ogAppName,  "BAR_COLOR","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());
    if (strcmp(pclTmpString,"NATURE")==0)
		bgBarColorNature = true;
	else
		bgBarColorNature = false;

	// UFIS-988 Position Gantt chart: show shadow/ blocked bar on adjacent stands or sub-stands in case main stand is used
	GetPrivateProfileString(ogAppName,  "POS_SHADOWBAR","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());
    if (strcmp(pclTmpString,"TRUE")==0)
		bgPosShadowbars = true;
	else
		bgPosShadowbars = false;

	// UFIS-988 Position Gantt chart: show shadow/ blocked bar on adjacent stands or sub-stands in case main stand is used
	GetPrivateProfileString(ogAppName,  "POS_BAR_SLOT_MINUS","0", pclTmpString,sizeof pclTmpString, GetConfigPath());

	//setting for multi airports
	GetPrivateProfileString("GLOBAL", "MULTI_AIRPORT", "FALSE",pclTmpString,sizeof pclTmpString, GetConfigPath() );
	if (strcmp(pclTmpString,"TRUE")==0)
		bgUseMultiAirport = true;
	else
		bgUseMultiAirport = false;

	int ilTmp3 = atoi(pclTmpString);
	ogPosBarSlotMinus  = CTimeSpan(0,0,ilTmp3,0);

    
	// UFIS-1036 Provide split window for FWR and represent flights as bars in Gantt Chart View
	GetPrivateProfileString(ogAppName,  "FWR_GANTT","FALSE", pclTmpString,sizeof pclTmpString, GetConfigPath());
    if (strcmp(pclTmpString,"TRUE")==0)
		bgFWRGantt = true;
	else
		bgFWRGantt = false;

	// UFIS-1036 Provide split window for FWR and represent flights as bars in Gantt Chart View
	GetPrivateProfileString(ogAppName,  "FWR_LIST","TRUE", pclTmpString,sizeof pclTmpString, GetConfigPath());
    if (strcmp(pclTmpString,"TRUE")==0)
		bgFWRList = true;
	else
		bgFWRList = false;





	SetCustomerConfig();

	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pcgHome);
	strcpy(CCSCedaData::pcmApplName, ogAppName);
	strcpy(CCSCedaData::pcmVersion, pcgVersion);
	strcpy(CCSCedaData::pcmInternalBuild, pcgInternalBuild);


	CString theKey = "UFIS";
	SetRegistryKey (theKey);

	

#if	0
	CLoginDialog olLoginDlg(pcgTableExt,ogAppName,pogBackGround);

	if((strcmp(pclUser, "DEFAULT") != 0) && (strcmp(pclPassword, "DEFAULT") != 0))
	{
		strcpy(pcgUser,pclUser);
		strcpy(pcgPasswd,pclPassword);
		ogBasicData.omUserID = pclUser;
		ogCommHandler.SetUser(pclUser);

		strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);
		strcpy(CCSCedaData::pcmUser, pclUser);

		if(bgUseMultiAirport)
		{
			CString strSelectedExt= olLoginCtrl.GetTableExt();
			strSelectedExt.TrimLeft();
			strSelectedExt.TrimRight();
			if(strSelectedExt.GetLength()>0)
			{
				strcpy(pcgTableExt,olLoginCtrl.GetTableExt());
			}
		}

		if(!ogPrivList.Login(pcgTableExt,pclUser,pclPassword,ogAppName))
			return FALSE;
	}
	else
	{
		if( olLoginDlg.DoModal() != IDCANCEL )
		{
			int ilStartApp = IDOK;
			if(ogPrivList.GetStat("InitModu") == '1')
			{
				RegisterDlg olRegisterDlg;
				ilStartApp = olRegisterDlg.DoModal();
			}
			if(ilStartApp != IDOK)
			{
				return FALSE;
			}

		}
		else
		{
			return FALSE;
		}
	}
#else
	//m_lpCmdLine = "_CONNECTED";
	if(strcmp(m_lpCmdLine, "_CONNECTED") != 0)
	{
		strcpy(pclUser, "TEST");
		CDialog olDummyDlg;	
		olDummyDlg.Create(IDD_LISTBOXDLG,NULL);
		CAatLogin olLoginCtrl;
		olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
		olLoginCtrl.SetInfoButtonVisible(TRUE);
		olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());

		GetPrivateProfileString(ogAppName, "TEXT1"  , "DEFAULT", pclUser, sizeof pclUser, "C:\\tmp\\debug.txt" );
		GetPrivateProfileString(ogAppName, "TEXT2"  , "DEFAULT", pclPassword, sizeof pclPassword, "C:\\tmp\\debug.txt" );

		if((strcmp(pclUser, "DEFAULT") != 0) && (strcmp(pclPassword, "DEFAULT") != 0))
		{
			strcpy(pcgUser,pclUser);
			strcpy(pcgPasswd,pclPassword);
			ogBasicData.omUserID = pclUser;
			ogCommHandler.SetUser(pclUser);

			strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);
			strcpy(CCSCedaData::pcmUser, pclUser);


			if (olLoginCtrl.DoLoginSilentMode(pclUser,pclPassword) != "OK")
				return FALSE;
		}
		else
		{
			if (olLoginCtrl.ShowLoginDialog() != "OK")
			{
				olDummyDlg.DestroyWindow();
				return FALSE;
			}

			strcpy(pcgUser,olLoginCtrl.GetUserName_());
			strcpy(pcgPasswd,olLoginCtrl.GetUserPassword());

			if(bgUseMultiAirport)
			{
				CString strSelectedExt= olLoginCtrl.GetTableExt();
				strSelectedExt.TrimLeft();
				strSelectedExt.TrimRight();
				if(strSelectedExt.GetLength()>0)
				{
					strcpy(pcgTableExt,olLoginCtrl.GetTableExt());
				}
			}

			ogBasicData.omUserID = olLoginCtrl.GetUserName_();
			ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
			strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
			strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

		}


		ogPrivList.Add(olLoginCtrl.GetPrivilegList());
		
		//MessageBox(0,olLoginCtrl.GetProfileList(),"Profile List",MB_OK);
		
		olDummyDlg.DestroyWindow();
	}
	else
	{
		bgTroyanHorse = true;
		strcpy(pcgUser,"TEST");
		strcpy(pcgPasswd,"TEST");
		ogBasicData.omUserID = "TEST";
		ogCommHandler.SetUser("TEST");

		strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);
		strcpy(CCSCedaData::pcmUser, "TEST");
	}
#endif

	
	if(bgUseMultiAirport)
	{
		
			strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
			ogBCD.SetTableExtension(CString(pcgTableExt));
			ogBCD.SetHomeAirport(pcgHome);
		
	}


	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("RAC"), BC_RAC_RELOAD, true);
	ogBcHandle.AddTableCommand(CString("RELAFT"), CString("SBC"), AFT_RELOAD, true);

	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);

	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);

	ogBcHandle.AddTableCommand(CString(""), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("IFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("UPJ"), BC_FLIGHT_CHANGE, true);

	ogBcHandle.AddTableCommand(CString("CCA") + CString(pcgTableExt), CString("IRT"), BC_CCA_NEW, true);
	ogBcHandle.AddTableCommand(CString("CCA") + CString(pcgTableExt), CString("DRT"), BC_CCA_DELETE, true);
	ogBcHandle.AddTableCommand(CString("CCA") + CString(pcgTableExt), CString("URT"), BC_CCA_CHANGE, true);


	ogBcHandle.AddTableCommand(CString("FLZ") + CString(pcgTableExt), CString("IRT"), BC_FLZ_NEW, true);
	ogBcHandle.AddTableCommand(CString("FLZ") + CString(pcgTableExt), CString("DRT"), BC_FLZ_DELETE, true);
	ogBcHandle.AddTableCommand(CString("FLZ") + CString(pcgTableExt), CString("URT"), BC_FLZ_CHANGE, true);


	ogBcHandle.AddTableCommand(CString("CHA") + CString(pcgTableExt), CString("IRT"), BC_CHA_NEW, true);
	ogBcHandle.AddTableCommand(CString("CHA") + CString(pcgTableExt), CString("DRT"), BC_CHA_DELETE, true);
	ogBcHandle.AddTableCommand(CString("CHA") + CString(pcgTableExt), CString("URT"), BC_CHA_CHANGE, true);
	

	ogBcHandle.AddTableCommand(CString("INF") + CString(pcgTableExt), CString("IRT"), BC_INF_NEW, false,ogAppName);
	ogBcHandle.AddTableCommand(CString("INF") + CString(pcgTableExt), CString("URT"), BC_INF_CHANGE, false,ogAppName);
	ogBcHandle.AddTableCommand(CString("INF") + CString(pcgTableExt), CString("DRT"), BC_INF_DELETE, true);
/*
	ogBcHandle.AddTableCommand(CString("INF") + CString(pcgTableExt), CString("IRT"), BC_INF_NEW, true);
	ogBcHandle.AddTableCommand(CString("INF") + CString(pcgTableExt), CString("URT"), BC_INF_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("INF") + CString(pcgTableExt), CString("DRT"), BC_INF_DELETE, true);
*/


	ogBcHandle.AddTableCommand(CString("CFL") + CString(pcgTableExt), CString("IRT"), BC_CFL_NEW, true);
	ogBcHandle.AddTableCommand(CString("CFL") + CString(pcgTableExt), CString("URT"), BC_CFL_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("CFL") + CString(pcgTableExt), CString("DRT"), BC_CFL_CHANGE, true);
	//ogBcHandle.AddTableCommand(CString("CFL") + CString(pcgTableExt), CString("IBT"), BC_CFL_NEW, true);
	//ogBcHandle.AddTableCommand(CString("CFL") + CString(pcgTableExt), CString("UBT"), BC_CFL_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("RELCFL"), CString("SBC"), CFL_RELOAD, true);
	ogBcHandle.AddTableCommand(CString("RELCCA"), CString("SBC"), CCA_RELOAD, true);

	ogBcHandle.AddTableCommand(CString("PAR") + CString(pcgTableExt), CString("IRT"), BC_PAR_NEW, false,ogAppName);
	ogBcHandle.AddTableCommand(CString("PAR") + CString(pcgTableExt), CString("URT"), BC_PAR_CHANGE, false,ogAppName);
	ogBcHandle.AddTableCommand(CString("PAR") + CString(pcgTableExt), CString("DRT"), BC_PAR_DELETE, false,ogAppName);

	ogBcHandle.AddTableCommand(CString("VAL") + CString(pcgTableExt), CString("IRT"), BC_VAL_NEW, false,ogAppName);
	ogBcHandle.AddTableCommand(CString("VAL") + CString(pcgTableExt), CString("URT"), BC_VAL_CHANGE, false,ogAppName);
	ogBcHandle.AddTableCommand(CString("VAL") + CString(pcgTableExt), CString("DRT"), BC_VAL_DELETE, false,ogAppName);

	ogBcHandle.AddTableCommand(CString("DCF") + CString(pcgTableExt), CString("IRT"), BC_DCF_INSERT, true,ogAppName);
	ogBcHandle.AddTableCommand(CString("DCF") + CString(pcgTableExt), CString("URT"), BC_DCF_UPDATE, true,ogAppName);
	ogBcHandle.AddTableCommand(CString("DCF") + CString(pcgTableExt), CString("DRT"), BC_DCF_DELETE, true,ogAppName);

	ogBcHandle.AddTableCommand(CString("NAT") + CString(pcgTableExt), CString("IRT"), BCD_NAT_CHANGE, true,ogAppName);
	ogBcHandle.AddTableCommand(CString("NAT") + CString(pcgTableExt), CString("URT"), BCD_NAT_CHANGE, true,ogAppName);
	ogBcHandle.AddTableCommand(CString("NAT") + CString(pcgTableExt), CString("DRT"), BCD_NAT_CHANGE, true,ogAppName);

	ogBcHandle.AddTableCommand(CString("BWA") + CString(pcgTableExt), CString("IRT"), BC_BWA_NEW, false,ogAppName);
	ogBcHandle.AddTableCommand(CString("BWA") + CString(pcgTableExt), CString("URT"), BC_BWA_CHANGE, false,ogAppName);
	ogBcHandle.AddTableCommand(CString("BWA") + CString(pcgTableExt), CString("DRT"), BC_BWA_DELETE, false,ogAppName);



	if(ogFpeData.bmFlightPermitsEnabled)
	{
		ogBcHandle.AddTableCommand(CString("FPE") + CString(pcgTableExt), CString("IRT"), BC_FPE_INSERT, true, ogAppName);
		ogBcHandle.AddTableCommand(CString("FPE") + CString(pcgTableExt), CString("URT"), BC_FPE_UPDATE, true, ogAppName);
		ogBcHandle.AddTableCommand(CString("FPE") + CString(pcgTableExt), CString("DRT"), BC_FPE_DELETE, true, ogAppName);
	}
//	ogBcHandle.AddTableCommand(CString("BCTEST"), CString("SBC"), BC_TEST, true);
#ifdef SATS_PERFORMANCE
	ogBcHandle.AddTableCommand(CString("TIMERS"), CString("TIME"), BROADCAST_CHECK,true);
#else
	ogBcHandle.AddTableCommand(CString("TIMERS_FIPS"), CString("TIME"), BROADCAST_CHECK,true);
#endif	// SATS_PERFORMANCE
	ogBcHandle.AddTableCommand(CString("BCLOCC"), CString("SBC"), BC_LOCCHNG, true);

	// initialize configurated parameters to be used with wrapper functions any time later; don�t forget to include header file; TODO: remove to own (config)class
	UpdateDebugPrintOnlyFirstLines();		// 050309 MVy: for debugging purposes, waste less paper on printing, for testing print only one page
	UpdateShowOnlyOwnConfirmations();		// 050307 MVy: the current user sees only confirmations of conflicts he confirmed by himself, otherwise he can see all confirmations
	UpdateShowDailyScheduleColumnsDisplay();		// 050308 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
	UpdateShowDailyRotationsColumnBelts();		// 050308 MVy: daily rotations list should contain additional column for baggage belts BLT1/BLT2
	UpdateShowDailyRotationsColumnVIPs();		// 050309 MVy: daily rotations list column for VIPs
	UpdateShowDailyRotationsColumnRemarks();		// 050309 MVy: daily rotations list column for Remarks
	UpdateHandlePositionFlightTodayOnlyCheck();		// 050310 MVy: today only or period for position flight check
	//UpdateHandleCheckinCounterRestrictionsForHandlingAgents();		// 050310 MVy: today only or period for position flight check
	UpdateShowConflictPriorityColor();		// 050323 MVy: PRF6981: conflict priority, make configurable, addendum

	UpdateShowFIDSDisplay();//User ETA(FIDS),ETD(FIDS) or ETA(D),ETD(D)
	UpdateSSIM();//UFIS-1078 AM:20111028-SSIM/TimeTable

	//gatpos init
	ogBasicData.LoadParameters();
	bgGATPOS = ogBasicData.IsGatPosEnabled();

	blDatPosDura = ogBasicData.IsExtLmEnabled();
	blDatGatDura = ogBasicData.IsExtLmEnabled();

	if (blDatPosDura || blDatGatDura)
	{
		SeasonDlgCedaFlightData olFlightData;
		CStringArray opReqData,opReqFields;
		olFlightData.GetDefAllocForeCast(CString("GDA"), CString(""), CString("PSTA,PSTD,GTA1,GTD1"), opReqData, opReqFields);
	}


	ogCfgData.ReadUfisCedaConfig();



/*
	// ++++++++++++++++++++++++++++++ login SUS	
	Beep( 400,100 ) ;
	strcpy( pcgUser, "hbe" ) ;
	strcpy( pcgPasswd, "hbe" ) ;
	ogBasicData.omUserID = "hbe" ;
	ogCommHandler.SetUser("hbe");
*/
	
	if (pogBackGround)
	{
		pogBackGround->InvalidateRect(NULL);
		pogBackGround->UpdateWindow();
	}

	//CWinThread* pThread = AfxBeginThread(Test, NULL, THREAD_PRIORITY_NORMAL); 



	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		


	//InitialLoadDialog
//    InitialLoad(pogBackGround);
    InitialLoad(NULL);
	ogDataSet.Init();


	//Set Homeairport 4 letter code
	CString olApc4;
	ogBCD.GetField("APT","APC3", CString(pcgHome), "APC4", olApc4);
	strcpy(pcgHome4, olApc4);
	CedaAptLocalUtc::InitCedaAptLocalUtc(olApc4);


	ogBasicData.SetLocalDiff();

	CCSCedaData::omLocalDiff1 = ogBasicData.GetLocalDiff1();
	CCSCedaData::omLocalDiff2 = ogBasicData.GetLocalDiff2();
	CCSCedaData::omTich = ogBasicData.GetTich() ;


	InitCOMInterface();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));		


/*
//	CButtonListDlg dlg(pogBackGround);
	CButtonListDlg dlg(NULL);
	m_pMainWnd = &dlg;

	pogButtonList = &dlg; 	
	
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	pogButtonList = NULL;
 
	of_catch.close();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;

*/

	CFrameWnd* pMainFrame = new CFrameWnd;

	pMainFrame->Create( NULL, NULL, WS_OVERLAPPEDWINDOW );

	pMainFrame->SetIcon(pHicon, FALSE);


	m_pMainWnd = pMainFrame;


	pogButtonList = new CButtonListDlg(m_pMainWnd);
	if (pogButtonList->Create(IDD_BUTTONLIST))
	{
		return TRUE;
	}

	return FALSE;

}

// -------------------------------------------------------------------
//	CEDA.INI, configuration handling, debugging help, too
// -------------------------------------------------------------------

// 050309 MVy: for debugging purposes, waste less paper on printing, for testing print only one page
// get the current configuration
void CFPMSApp::UpdateDebugPrintOnlyFirstLines()
{
	CCS_TRY

	m_ulDebugPrintOnlyFirstLines = 0 ;
	char pc[1000];
	int iLen = 0 ;
	// hint: only the length of the entered string will be compared
	if( iLen = GetPrivateProfileString( ogAppName, "DEBUG_PRINT_ONLY_FIRST_LINES", "", pc, sizeof( pc ) -1, GetConfigPath() ) )
	{
		ASSERT( pc );
		m_ulDebugPrintOnlyFirstLines = atol( pc );
	};

	CCS_CATCH_ALL
};	// UpdateDebugPrintOnlyFirstLines

	// 050309 MVy: for debugging purposes, waste less paper on printing, for testing print only one page
// return amount of lines to print
unsigned long CFPMSApp::GetDebugPrintOnlyFirstLines()
{
	return m_ulDebugPrintOnlyFirstLines ;
};	// GetDebugPrintOnlyFirstLines


// 050307 MVy: the current user sees only confirmations of conflicts he confirmed by himself, otherwise he can see all confirmations
// 050307 MVy: CONFLICTS_CONFIRMATION_FOR_USER = ALL | CURRENT
//	get the current configuration
void CFPMSApp::UpdateShowOnlyOwnConfirmations()
{
	CCS_TRY

	m_bShowOnlyOwnConfirmations = false ;
	char pc[1000];
	int iLen = 0 ;
	// hint: only the length of the entered string will be compared
	if( iLen = GetPrivateProfileString( ogAppName, "CONFLICTS_CONFIRMATION_FOR_USER", "ALL", pc, sizeof( pc ) -1, GetConfigPath() ) )
	{
		ASSERT( pc );
		if( !strncmp( pc, "CURRENT", iLen ) ) m_bShowOnlyOwnConfirmations = true ; else
		if( !strncmp( pc, pcgUser, iLen ) ) m_bShowOnlyOwnConfirmations = true ;
	};

	CCS_CATCH_ALL
};	// UpdateShowOnlyOwnConfirmations

// 050307 MVy: the current user sees only confirmations of conflicts he confirmed by himself, otherwise he can see all confirmations
//	return true if user can only see his own confirmations
bool CFPMSApp::CanSeeOnlyOwnConfirmations()
{
	return m_bShowOnlyOwnConfirmations ;
};	// CanSeeOnlyOwnConfirmations


// 050308 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
void CFPMSApp::UpdateShowDailyScheduleColumnsDisplay()
{
	CCS_TRY

	m_bShowDailyScheduleColumnsDisplay = false ;
	char pc[1000];
	int iLen = 0 ;
	// hint: only the length of the entered string will be compared
	if( iLen = GetPrivateProfileString( ogAppName, "DAILY_SCHEDULE_COLUMNS_DISPLAY", "FALSE", pc, sizeof( pc ) -1, GetConfigPath() ) )
	{
		ASSERT( pc );
		if( !strncmp( pc, "TRUE", iLen ) ) m_bShowDailyScheduleColumnsDisplay = true ; else
		if( !strncmp( pc, "SHOW", iLen ) ) m_bShowDailyScheduleColumnsDisplay = true ; else
		if( !strncmp( pc, "ENABLE", iLen ) ) m_bShowDailyScheduleColumnsDisplay = true ; else
		if( !strncmp( pc, "ON", iLen ) ) m_bShowDailyScheduleColumnsDisplay = true ; else
		if( !strncmp( pc, "1", iLen ) ) m_bShowDailyScheduleColumnsDisplay = true ;
	};

	CCS_CATCH_ALL
};	// UpdateShowDailyScheduleColumnsDisplay


void CFPMSApp::UpdateShowFIDSDisplay()
{
	CCS_TRY

	m_bShowFIDS = false ;
	char pc[1000];
	int iLen = 0 ;
	// hint: only the length of the entered string will be compared
	if( iLen = GetPrivateProfileString( ogAppName, "DAILY_SCHEDULE_USE_FIDS", "FALSE", pc, sizeof( pc ) -1, GetConfigPath() ) )
	{
		ASSERT( pc );
		if( !strncmp( pc, "TRUE", iLen ) ) m_bShowFIDS = true ; else
		if( !strncmp( pc, "SHOW", iLen ) ) m_bShowFIDS = true ; else
		if( !strncmp( pc, "ENABLE", iLen ) ) m_bShowFIDS = true ; else
		if( !strncmp( pc, "ON", iLen ) ) m_bShowFIDS = true ; else
		if( !strncmp( pc, "1", iLen ) ) m_bShowFIDS = true ;
	};

	CCS_CATCH_ALL
};	
// 050308 MVy: daily schedule lists should contain additional column for display data: ETA(D) and ETD(D)
//	return true if user can only see his own confirmations
bool CFPMSApp::CanSeeDailyScheduleColumnsDisplay()
{
	return m_bShowDailyScheduleColumnsDisplay ;
};	// CanSeeDailyScheduleColumnsDisplay

bool CFPMSApp::CanUseFIDSHeader()
{
	return m_bShowFIDS;
};


// 050308 MVy: daily rotations list should contain additional column for baggage belts BLT1/BLT2
void CFPMSApp::UpdateShowDailyRotationsColumnBelts()
{
	CCS_TRY

	m_ulShowDailyRotationsColumnBelts = 0x00000000 ;
	char pc[1000];
	int iLen = 0 ;
	// hint: only the length of the entered string will be compared
	if( iLen = GetPrivateProfileString( ogAppName, "DAILY_ROTATIONS_COLUMN_BELTS", "FALSE", pc, sizeof( pc ) -1, GetConfigPath() ) )
	{
		ASSERT( pc );
		if( !strncmp( pc, "TRUE", iLen ) ) m_ulShowDailyRotationsColumnBelts = 0x00000003 ; else
		if( !strncmp( pc, "1", iLen ) ) m_ulShowDailyRotationsColumnBelts = 0x00000001 ; else
		if( !strncmp( pc, "2", iLen ) ) m_ulShowDailyRotationsColumnBelts = 0x00000002 ; else
		if( !strncmp( pc, "ON", iLen ) ) m_ulShowDailyRotationsColumnBelts = 0x00000003 ; else
		if( !strncmp( pc, "ALL", iLen ) ) m_ulShowDailyRotationsColumnBelts = 0x00000003 ;
	};

	CCS_CATCH_ALL
};	// UpdateShowDailyRotationsColumnBelts

// 050308 MVy: daily rotations list should contain additional column for baggage belts BLT1/BLT2
//	return true if user can only see his own confirmations
bool CFPMSApp::CanSeeDailyRotationsColumnBelts( unsigned long ulMask )
{
	return m_ulShowDailyRotationsColumnBelts & ulMask ;
};	// CanSeeDailyRotationsColumnBelts


// 050309 MVy: daily rotations list column for VIPs
void CFPMSApp::UpdateShowDailyRotationsColumnVIPs()
{
	CCS_TRY

	m_ucShowDailyRotationsColumnVIPs = 0x00 ;
	char pc[1000];
	int iLen = 0 ;
	// hint: only the length of the entered string will be compared
	if( iLen = GetPrivateProfileString( ogAppName, "DAILY_ROTATIONS_COLUMN_VIPS", "FALSE", pc, sizeof( pc ) -1, GetConfigPath() ) )
	{
		ASSERT( pc );
		if( !strncmp( pc, "TRUE", iLen ) ) m_ucShowDailyRotationsColumnVIPs = 0x03 ; else
		if( !strncmp( pc, "SHOW", iLen ) ) m_ucShowDailyRotationsColumnVIPs = 0x03 ; else
		if( !strncmp( pc, "A", iLen ) ) m_ucShowDailyRotationsColumnVIPs = 0x01 ; else
		if( !strncmp( pc, "D", iLen ) ) m_ucShowDailyRotationsColumnVIPs = 0x02 ; else
		if( !strncmp( pc, "ON", iLen ) ) m_ucShowDailyRotationsColumnVIPs = 0x03 ; else
		if( !strncmp( pc, "ENABLE", iLen ) ) m_ucShowDailyRotationsColumnVIPs = 0x03 ; else
		if( !strncmp( pc, "ALL", iLen ) ) m_ucShowDailyRotationsColumnVIPs = 0x03 ;
	};

	CCS_CATCH_ALL
};	// UpdateShowDailyRotationsColumnVIPs

// 050309 MVy: daily rotations list column for VIPs
//	return true if user can only see his own confirmations
bool CFPMSApp::CanSeeDailyRotationsColumnVIPs( unsigned char ucMask )
{
	return m_ucShowDailyRotationsColumnVIPs & ucMask ;
};	// CanSeeDailyRotationsColumnVIPs


// 050309 MVy: daily rotations list column for Remarks
void CFPMSApp::UpdateShowDailyRotationsColumnRemarks()
{
	CCS_TRY

	m_ucShowDailyRotationsColumnRemarks = 0x00 ;
	char pc[1000];
	int iLen = 0 ;
	// hint: only the length of the entered string will be compared
	if( iLen = GetPrivateProfileString( ogAppName, "DAILY_ROTATIONS_COLUMN_REMARKS", "FALSE", pc, sizeof( pc ) -1, GetConfigPath() ) )
	{
		ASSERT( pc );
		if( !strncmp( pc, "TRUE", iLen ) ) m_ucShowDailyRotationsColumnRemarks = 0x03 ; else
		if( !strncmp( pc, "SHOW", iLen ) ) m_ucShowDailyRotationsColumnRemarks = 0x03 ; else
		if( !strncmp( pc, "A", iLen ) ) m_ucShowDailyRotationsColumnRemarks = 0x01 ; else
		if( !strncmp( pc, "D", iLen ) ) m_ucShowDailyRotationsColumnRemarks = 0x02 ; else
		if( !strncmp( pc, "ON", iLen ) ) m_ucShowDailyRotationsColumnRemarks = 0x03 ; else
		if( !strncmp( pc, "ENABLE", iLen ) ) m_ucShowDailyRotationsColumnRemarks = 0x03 ; else
		if( !strncmp( pc, "ALL", iLen ) ) m_ucShowDailyRotationsColumnRemarks = 0x03 ;
	};

	CCS_CATCH_ALL
};	// UpdateShowDailyRotationsColumnRemarks

// 050309 MVy: daily rotations list column for Remarks
//	return true if user can only see his own confirmations
bool CFPMSApp::CanSeeDailyRotationsColumnRemarks( unsigned char ucMask )
{
	return m_ucShowDailyRotationsColumnRemarks & ucMask ;
};	// CanSeeDailyRotationsColumnRemarks


// 050310 MVy: today only or period for position flight check
//	get the current configuration
void CFPMSApp::UpdateHandlePositionFlightTodayOnlyCheck()
{
	CCS_TRY

	m_bAllowPositionFlightTodayOnlyCheck = false ;
	char pc[1000];
	int iLen = 0 ;
	// hint: only the length of the entered string will be compared
	if( iLen = GetPrivateProfileString( ogAppName, "POSITION_FLIGHT_TODAY_ONLY_CHECK", "FALSE", pc, sizeof( pc ) -1, GetConfigPath() ) )
	{
		ASSERT( pc );
		if( !strncmp( pc, "TRUE", iLen ) ) m_bAllowPositionFlightTodayOnlyCheck = true ; else
		if( !strncmp( pc, "ENABLE", iLen ) ) m_bAllowPositionFlightTodayOnlyCheck = true ; else
		if( !strncmp( pc, "ALLOW", iLen ) ) m_bAllowPositionFlightTodayOnlyCheck = true ; else
		if( !strncmp( pc, "1", iLen ) ) m_bAllowPositionFlightTodayOnlyCheck = true ; else
		if( !strncmp( pc, "ON", iLen ) ) m_bAllowPositionFlightTodayOnlyCheck = true ;
	};

	CCS_CATCH_ALL
};	// UpdateHandlePositionFlightTodayOnlyCheck

// 050310 MVy: today only or period for position flight check
//	return true if user can only see his own confirmations
bool CFPMSApp::CanHandlePositionFlightTodayOnlyCheck()
{
	return m_bAllowPositionFlightTodayOnlyCheck ;
};	// CanHandlePositionFlightTodayOnlyCheck


// 050311 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents
//	get the current configuration
void CFPMSApp::UpdateHandleCheckinCounterRestrictionsForHandlingAgents()
{
	ASSERT(0);	// 050316 MVy: do not use, other handling method preferred see CanHandle... function
	/*
	CCS_TRY

	m_bAllowCheckinCounterRestrictionsForHandlingAgents = false ;
	char pc[1000];
	int iLen = 0 ;
	// hint: only the length of the entered string will be compared
	if( iLen = GetPrivateProfileString( ogAppName, "CHECKIN_COUNTER_RESTRICTIONS_FOR_HANDLING_AGENTS", "FALSE", pc, sizeof( pc ) -1, GetConfigPath() ) )
	{
		ASSERT( pc );
		if( !strncmp( pc, "TRUE", iLen ) ) m_bAllowCheckinCounterRestrictionsForHandlingAgents = true ; else
		if( !strncmp( pc, "ENABLE", iLen ) ) m_bAllowCheckinCounterRestrictionsForHandlingAgents = true ; else
		if( !strncmp( pc, "ALLOW", iLen ) ) m_bAllowCheckinCounterRestrictionsForHandlingAgents = true ; else
		if( !strncmp( pc, "ALL", iLen ) ) m_bAllowCheckinCounterRestrictionsForHandlingAgents = true ; else
		if( !strncmp( pc, "ON", iLen ) ) m_bAllowCheckinCounterRestrictionsForHandlingAgents = true ; else
		if( !strncmp( pc, "1", iLen ) ) m_bAllowCheckinCounterRestrictionsForHandlingAgents = true ; else
		if( !strncmp( pc, pcgUser, iLen ) ) m_bAllowCheckinCounterRestrictionsForHandlingAgents = true ;
	};

	CCS_CATCH_ALL
	*/
};	// UpdateHandleCheckinCounterRestrictionsForHandlingAgents

// 050311 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents
//	return true if handling agent restriction are used with checkin counters
bool CFPMSApp::CanHandleCheckinCounterRestrictionsForHandlingAgents()
{
	return ogBasicData.IsExtLmEnabled();	// 050316 MVy: other handling method preferred
	//return m_bAllowCheckinCounterRestrictionsForHandlingAgents ;
};	// CanHandleCheckinCounterRestrictionsForHandlingAgents


// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
// get the current configuration
// 050506 MVy: name changed from CONFLICT_PRIORITY_COLOR to ATTENTION_LIST_COLOR
void CFPMSApp::UpdateShowConflictPriorityColor()
{
	CCS_TRY

	m_bShowConflictPriorityColor = false ;
	char pc[1000];
	int iLen = 0 ;
	// hint: only the length of the entered string will be compared
	if( iLen = GetPrivateProfileString( ogAppName, "ATTENTION_LIST_COLOR", "FALSE", pc, sizeof( pc ) -1, GetConfigPath() ) )
	{
		ASSERT( pc );
		if( !strncmp( pc, "TRUE", iLen ) ) m_bShowConflictPriorityColor = true ; else
		if( !strncmp( pc, "SHOW", iLen ) ) m_bShowConflictPriorityColor = true ; else
		if( !strncmp( pc, "ON", iLen ) ) m_bShowConflictPriorityColor = true ; else
		if( !strncmp( pc, "ENABLE", iLen ) ) m_bShowConflictPriorityColor = true ; else
		if( !strncmp( pc, "ALL", iLen ) ) m_bShowConflictPriorityColor = true ;
	};

	CCS_CATCH_ALL
};	// UpdateShowConflictPriorityColor

// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
// return true if user can modify the conflict colors, -> conflict list dialog, setup  conflict column
bool CFPMSApp::CanSeeConflictPriorityColor()
{
	return m_bShowConflictPriorityColor ;
};	// CanSeeConflictPriorityColor

// -------------------------------------------------------------------


bool CFPMSApp::InitCOMInterface()
{

	HRESULT olRes = CoInitialize(NULL);
	if (olRes == E_INVALIDARG)
		int asdf = 0;
	if (olRes == E_OUTOFMEMORY)
		int asdf = 0;
	if (olRes == E_UNEXPECTED)
		int asdf = 0;
	if (olRes == S_FALSE)
		int asdf = 0;
	if (olRes == RPC_E_CHANGED_MODE)
		int asdf = 0;

 	if(olRes != S_OK && olRes != S_FALSE)
	{
		//AfxMessageBox(_T("CoInitializeEx failed!"));
		return false;
	}
	

	try
	{
		pConnect = IUFISAmPtr(CLSID_UFISAm);
	}
	catch(_com_error error)
	{
		//AfxMessageBox(error.ErrorMessage());
		return false;
	}


	try
	{
		BOOL Res = m_UFISAmSink.Advise(pConnect,IID_IUFISAmEventSink);
		ASSERT(Res == TRUE);
	}
	catch(_com_error error)
	{
		//AfxMessageBox(error.ErrorMessage());
		return false;
	}
	/*
	 * Assign AppTag immediately after advising the sink!
	 */
	pConnect->AssignAppTag(static_cast<long>(Fips));

	bgComOk = true;

	CFPMSApp::DumpDebugLog("COM-Interface succesfully initialized!");

	return true;
}

void CFPMSApp::CheckAloTab()
{
	ogAloData.ReadAloData();

	ALODATA *polAloc = ogAloData.GetAloByName("POSPOS");
	if (!polAloc)
	{
		ogAloData.Insert(CString("POSPOS"));
	}

	polAloc = ogAloData.GetAloByName("POSACT");
	if (!polAloc)
	{
		ogAloData.Insert(CString("POSACT"));
	}

	polAloc = ogAloData.GetAloByName("ACTACT");
	if (!polAloc)
	{
		ogAloData.Insert(CString("ACTACT"));
	}

	if(blIncomApPos)
	{
		ALODATA *polAloc = ogAloData.GetAloByName("POSPOS");
		if (!polAloc)
		{
			ogAloData.Insert(CString("POSPOS"));
		}

		polAloc = ogAloData.GetAloByName("POSAPT");
		if (!polAloc)
		{
			ogAloData.Insert(CString("POSAPT"));
		}

		polAloc = ogAloData.GetAloByName("APTAPT");
		if (!polAloc)
		{
			ogAloData.Insert(CString("APTAPT"));
		}
	}
	if(blIncomAlPos)
	{
		ALODATA *polAloc = ogAloData.GetAloByName("POSPOS");
		if (!polAloc)
		{
			ogAloData.Insert(CString("POSPOS"));
		}

		polAloc = ogAloData.GetAloByName("POSALT");
		if (!polAloc)
		{
			ogAloData.Insert(CString("POSALT"));
		}

		polAloc = ogAloData.GetAloByName("ALTALT");
		if (!polAloc)
		{
			ogAloData.Insert(CString("ALTALT"));
		}
	}
	if(blIncomApGat)
	{
		ALODATA *polAloc = ogAloData.GetAloByName("GATGAT");
		if (!polAloc)
		{
			ogAloData.Insert(CString("GATGAT"));
		}

		polAloc = ogAloData.GetAloByName("GATAPT");
		if (!polAloc)
		{
			ogAloData.Insert(CString("GATAPT"));
		}

		polAloc = ogAloData.GetAloByName("APTAPT");
		if (!polAloc)
		{
			ogAloData.Insert(CString("APTAPT"));
		}
	}
	if(blIncomAlPos)
	{
		ALODATA *polAloc = ogAloData.GetAloByName("GATGAT");
		if (!polAloc)
		{
			ogAloData.Insert(CString("GATGAT"));
		}

		polAloc = ogAloData.GetAloByName("GATALT");
		if (!polAloc)
		{
			ogAloData.Insert(CString("GATALT"));
		}

		polAloc = ogAloData.GetAloByName("ALTALT");
		if (!polAloc)
		{
			ogAloData.Insert(CString("ALTALT"));
		}
	}
}

void CFPMSApp::InitialLoad(CWnd *pParent)
{
	CCS_TRY

	//CTime olTime = CTime::GetCurrentTime();
	//TRACE("\n\nbeginn InitialLoad  %s", olTime.Format("%H:%M:%S")); 

	pogInitialLoad = new CInitialLoadDlg(pParent); 

	int ilGant = 1;

	ogBCD.SetTableExtension(CString(pcgTableExt));
	ogCcaDiaFlightData.omCcaData.SetTableName(CString("CCA") + CString(pcgTableExt));


	ogBCD.SetSystabErrorMsg( GetString(IDS_STRING1656), GetString(ST_FEHLER ) );

	
	//MessageBox( 0, CString(pcgTableExt), "pcgTableExt in InitLoad", MB_ICONWARNING );
//////////////////////////////////////////////////////////////////////////////////////

	pogInitialLoad->SetRange(30); 

	// toms tables
	pogInitialLoad->SetMessage(GetString(IDS_STRING2347));
	ogBCD.SetObject("ALO","ALOC,URNO"); 
	ogBCD.AddKeyMap("ALO","ALOC");
	ogBCD.Read("ALO");
	pogInitialLoad->SetProgress(ilGant);



	CheckAloTab();

	//check fields avail ?
	ogBCD.SetObject("SYS","FINA"); 
	CCSPtrArray<RecordSet> pomBuffer;
	int ilCount = ogBCD.Read(CString("SYS"), "WHERE FINA = 'BAA4' and TANA = 'AFT'",&pomBuffer);
	if (ilCount < 1)
	{
		if (blDatPosDura || blDatGatDura)
			::MessageBox(NULL,"Ceda.ini: DATTAB_FOR_POS[GAT]_DURATION=TRUE but AFTTAB:BAA4 not availible" ,"Datenbankfehler",MB_OK);

		blDatPosDura = false;
		blDatGatDura = false;
	}
	ilCount = ogBCD.Read(CString("SYS"), "WHERE FINA = 'BAA5' and TANA = 'AFT'",&pomBuffer);
	if (ilCount < 1)
	{
		if (blDatPosDura || blDatGatDura)
			::MessageBox(NULL,"Ceda.ini: DATTAB_FOR_POS[GAT]_DURATION=TRUE but AFTTAB:BAA5 not availible" ,"Datenbankfehler",MB_OK);

		blDatPosDura = false;
		blDatGatDura = false;
	}

	//Added by Christine
	ilCount = ogBCD.Read(CString("SYS"), "WHERE FINA = 'PCOM' and TANA = 'AFT'",&pomBuffer);
	if (ilCount < 1)
	{
		bgShowCashComment = false;
	}
	else
	{
		bgShowCashComment = true;
	}

	//Delay button Arrival

	CCSPtrArray<RecordSet> olBuffer;
	RecordSet olRec;
	CString olTmp;
	int ilRet = 4;

	ogBCD.SetObject("SYS","FELE");
	
	int ilCount2 = ogBCD.ReadSpecial("SYS", CString("FELE"), CString("WHERE TANA = 'AFT' AND FINA = 'STEV'"), olBuffer);

	if(olBuffer.GetSize() == 1)
	{
		olRec = olBuffer[0];
		olTmp = olRec.Values[0];	
		igLengthStev = atoi(olTmp);
	}
	olBuffer.DeleteAll();


	ilCount = ogBCD.Read(CString("SYS"), "WHERE FINA = 'CATR' and TANA = 'CIC'",&pomBuffer);
	if (ilCount < 1)
		bgCnamAtr = false;
	else
		bgCnamAtr = true;

	ilCount = ogBCD.Read(CString("SYS"), "WHERE FINA = 'GURN' and TANA = 'CCA'",&pomBuffer);
	if (ilCount < 1)
		bgCheckInAirline = false;
	else
		bgCheckInAirline = true;


	pogInitialLoad->SetMessage(GetString(IDS_STRING2348));
	ogBCD.SetObject("SGR","UGTY,STYP,URNO");
	ogBCD.AddKeyMap("SGR","UGTY");
	ogBCD.AddKeyMap("SGR","STYP");

	//ogBCD.Read("SGR", "where appl in ('BAGGAT','EXTBAG','GATPOS','WROGAT','RULE_AFT')");
	
	//ogBCD.Read("SGR", "where UGTY in (select URNO from ALOTAB where ALOC in ('GATPOS','BAGGAT','WROGAT','EXTBAG','POSPOS','POSACT','ACTACT','POSAPT','APTAPT','POSALT','ALTALT','GATGAT','GATAPT','GATALT') )");
	ogBCD.Read("SGR", "where UGTY in (select URNO from ALO" + CString(pcgTableExt) + " where ALOC in ('GATPOS','BAGGAT','WROGAT','EXTBAG','POSPOS','POSACT','ACTACT','POSAPT','APTAPT','POSALT','ALTALT','GATGAT','GATAPT','GATALT') )");
	
	pogInitialLoad->SetProgress(ilGant);

	pogInitialLoad->SetMessage(GetString(IDS_STRING2349));
	ogBCD.SetObject("SGM","UVAL,VALU,USGR,URNO,UGTY,PRFL,SORT"); 
	ogBCD.AddKeyMap("SGM","USGR");
	ogBCD.AddKeyMap("SGM","UGTY");
	ogBCD.AddKeyMap("SGM","UVAL");
	ogBCD.AddKeyMap("SGM","SORT");
	//ogBCD.Read("SGM", "where usgr in ( select urno from sgrtab where UGTY in (select URNO from ALOTAB where ALOC in ('GATPOS','BAGGAT','WROGAT','EXTBAG','POSPOS','POSACT','ACTACT','POSAPT','APTAPT','POSALT','ALTALT','GATGAT','GATAPT','GATALT') ))");

	ogBCD.Read("SGM", "where usgr in ( select urno from sgr" + CString(pcgTableExt) + " where UGTY in (select URNO from ALO" + CString(pcgTableExt) + " where ALOC in ('GATPOS','BAGGAT','WROGAT','EXTBAG','POSPOS','POSACT','ACTACT','POSAPT','APTAPT','POSALT','ALTALT','GATGAT','GATAPT','GATALT') ))");
	//ogBCD.Read("SGM");
	pogInitialLoad->SetProgress(ilGant);
	// toms tables

	// Fluggesellschaften
	pogInitialLoad->SetMessage(GetString(IDS_STRING954));
	if (ogBasicData.IsTerminalRestrictionForAirlinesAvailable())
	{
		bgTermRestrForAl = true;
		ogBCD.SetObject("ALT", "URNO,ALC2,ALC3,ALFN,TERM,CASH,TERB,TERG,TERP,TERW,VAFR,VATO"); 
	}
	else
	{
		bgTermRestrForAl = false;
		ogBCD.SetObject("ALT", "URNO,ALC2,ALC3,ALFN,TERM,CASH,VAFR,VATO"); 
	}
		
	ogBCD.SetObjectDesc("ALT", GetString(IDS_STRING955));
	ogBCD.SetTableHeader("ALT", "ALFN", GetString(IDS_STRING985));
	ogBCD.SetTableHeader("ALT", "ALC2", GetString(IDS_STRING987));
	ogBCD.SetTableHeader("ALT", "ALC3", GetString(IDS_STRING988));
	ogBCD.AddKeyMap("ALT", "ALC2");
	ogBCD.AddKeyMap("ALT", "ALC3");
	ogBCD.Read(CString("ALT"), CString("WHERE ALC3 <> ' '"));
	pogInitialLoad->SetProgress(ilGant);
	//ogBCD.SetDdxType(CString("ALT"), "IRT", S_FLIGHT_INSERT);
	//ogBCD.SetDdxType(CString("ALT"), "DRT", S_FLIGHT_DELETE);
	//ogBCD.SetDdxType(CString("ALT"), "URT", S_FLIGHT_UPDATE);
	

	// Delaycodes
	pogInitialLoad->SetMessage(GetString(IDS_STRING992));
	ogBCD.SetObject("DEN", "URNO,DECA,DECN,DENA");
	ogBCD.SetObjectDesc("DEN", "Delaycodes");
	ogBCD.SetTableHeader("DEN", "DENA", GetString(IDS_STRING985));
	ogBCD.SetTableHeader("DEN", "DECN", GetString(IDS_STRING993));
	ogBCD.SetTableHeader("DEN", "DECA", GetString(IDS_STRING994));
	ogBCD.AddKeyMap("DEN", "DECA");
	ogBCD.AddKeyMap("DEN", "DECN");
	ogBCD.Read(CString("DEN"));
	pogInitialLoad->SetProgress(ilGant);

	//Checkin-Counter Blockierungen
	pogInitialLoad->SetMessage(GetString(IDS_STRING2350));
	ogBCD.SetObject("BLK", "BURN,DAYS,NAFR,NATO,RESN,TABN,TYPE,TIFR,TITO,URNO,IBIT");
//	ogBCD.SetObject("BLK", "BURN,DAYS,NAFR,NATO,RESN,TABN,TYPE,TIFR,TITO,URNO"); 
	ogBCD.SetObjectDesc("BLK", "Blockade");
	ogBCD.Read("BLK"); //Wird beim laden von CCA-Daten mit filter geladen!

	ogBCD.SetDdxType("BLK", "IRT", BLK_CHANGE);
	ogBCD.SetDdxType("BLK", "DRT", BLK_CHANGE);
	ogBCD.SetDdxType("BLK", "URT", BLK_CHANGE);


	//Checkin-Counter Blockierungen f�r REPORT19
	ogBCD.SetObject("AAA", "BLK", CString("BURN,DAYS,NAFR,NATO,RESN,TABN,TYPE,TIFR,TITO,URNO"));

	// Registrierungen
	pogInitialLoad->SetMessage(GetString(IDS_STRING995));
	ogBCD.SetObject("ACR", "URNO,REGN,ACT3,ACT5,UFIS,APUI,CNAM,SEAB,SEAE,SEAF,MTOW");
	ogBCD.SetObjectDesc("ACR", GetString(IDS_STRING996));
	//ogBCD.AddKeyMap("ACR", "REGN");
	//ogBCD.Read(CString("ACR"), CString("WHERE UFIS='1'"));
	pogInitialLoad->SetProgress(ilGant * 2);

	ogBCD.SetObject("GHS", "URNO,LKNM,LKCC");
	ogBCD.Read(CString("GHS"));

	ogBCD.SetObject("GHD", "URNO,DUST,FKEY,EURN,GHSU");
	ogBCD.SetBcMode("GHD", -1);
	//ogBCD.Read(CString("ACR"));

	// Flugzeugtypen
	pogInitialLoad->SetMessage(GetString(IDS_STRING997));
	ogBCD.SetObject("ACT", "URNO,ACT3,ACT5,ACFN,SEAT,ACWS,ACHE,ACLE,SEAB,SEAE,SEAF,HOPO,AFMC");
	ogBCD.SetObjectDesc("ACT", GetString(IDS_STRING998));
	ogBCD.SetTableHeader("ACT", "ACFN", GetString(IDS_STRING985));
	ogBCD.SetTableHeader("ACT", "ACT3", GetString(IDS_STRING988));
	ogBCD.SetTableHeader("ACT", "ACT5", GetString(IDS_STRING991));
	ogBCD.AddKeyMap("ACT", "ACT5");
	ogBCD.AddKeyMap("ACT", "ACT3");
	ogBCD.Read(CString("ACT"));
	pogInitialLoad->SetProgress(ilGant);


	// Flugh�fen
	pogInitialLoad->SetMessage(GetString(IDS_STRING999));
//	ogBCD.SetObject("APT", "URNO,APC3,APC4,APFN,TDI1,TDI2,TICH,APTT");
	ogBCD.SetObject("APT", "URNO,APC3,APC4,APFN,TDI1,TDI2,TICH,APTT,TDIS,TDIW,TZON");
	ogBCD.SetObjectDesc("APT", GetString(IDS_STRING990));
	ogBCD.SetTableHeader("APT", "APFN", GetString(IDS_STRING985));
	ogBCD.SetTableHeader("APT", "APC3", GetString(IDS_STRING988));
	ogBCD.SetTableHeader("APT", "APC4", GetString(IDS_STRING989));
	ogBCD.AddKeyMap("APT", "APC3");
	ogBCD.AddKeyMap("APT", "APC4");
	ogBCD.Read(CString("APT"));
	pogInitialLoad->SetProgress(ilGant);

	// Gep�ckb�nder
	pogInitialLoad->SetMessage(GetString(IDS_STRING1000));
//	ogBCD.SetObject("BLT", "URNO,TERM,BNAM,NAFR,NATO,MAXF,DEFD,BLTT");
	ogBCD.SetObject("BLT", "URNO,TERM,BNAM,NAFR,NATO,MAXF,DEFD,BLTT,BLTR");
	ogBCD.SetTableHeader("BLT", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("BLT", "BNAM", GetString(IDS_STRING986));
	ogBCD.SetObjectDesc("BLT", GetString(IDS_STRING1010));
	ogBCD.AddKeyMap("BLT", "BNAM");

	ogBCD.SetDdxType("BLT", "IRT", BCD_BLT_CHANGE);
	ogBCD.SetDdxType("BLT", "DRT", BCD_BLT_CHANGE);
	ogBCD.SetDdxType("BLT", "URT", BCD_BLT_CHANGE);
	
	
	ogBCD.Read(CString("BLT"));
	pogInitialLoad->SetProgress(ilGant);

	// Checkin Schalter
	pogInitialLoad->SetMessage(GetString(IDS_STRING1011));
	ogBCD.SetObject("CIC");

	if (bgCnamAtr)
	{		
		if(bgShowClassInBatchDlg)
		{
			ogBCD.SetObject("CIC", "URNO,TERM,CNAM,NAFR,NATO,HALL,CICR,VATO,VAFR,RGBL,CBAZ,CATR,DISP");
		}
		else
		{
			ogBCD.SetObject("CIC", "URNO,TERM,CNAM,NAFR,NATO,HALL,CICR,VATO,VAFR,RGBL,CBAZ,CATR");
		}
		
	}
	else
	{
		
		if(bgShowClassInBatchDlg)
		{
			ogBCD.SetObject("CIC", "URNO,TERM,CNAM,NAFR,NATO,HALL,CICR,VATO,VAFR,RGBL,CBAZ,DISP");
		}
		else
		{
			ogBCD.SetObject("CIC", "URNO,TERM,CNAM,NAFR,NATO,HALL,CICR,VATO,VAFR,RGBL,CBAZ");
		}
		
	}

	ogBCD.SetObjectDesc("CIC", GetString(IDS_STRING1012));
	ogBCD.SetTableHeader("CIC", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("CIC", "CNAM", GetString(IDS_STRING986));
	if (bgCnamAtr)
		ogBCD.SetTableHeader("CIC", "CATR", GetString(IDS_STRING2687));
	
	ogBCD.AddKeyMap("CIC", "CNAM");
	if(bgShowClassInBatchDlg)
		ogBCD.SetTableHeader("CIC", "DISP", GetString(IDS_STRING2916));
	ogBCD.AddKeyMap("CIC", "DISP");
	ogBCD.Read(CString("CIC"));

	ogBCD.SetDdxType("CIC", "IRT", BCD_CIC_CHANGE);
	ogBCD.SetDdxType("CIC", "DRT", BCD_CIC_CHANGE);
	ogBCD.SetDdxType("CIC", "URT", BCD_CIC_CHANGE);

	
	if(bgChuteDisplay)
	{
		ogBCD.SetObject("CHU", "URNO,TERM,CNAM,VATO,VAFR");
		ogBCD.AddKeyMap("CHU", "CNAM");
		ogBCD.Read(CString("CHU"));

		ogBCD.SetDdxType("CHU", "IRT", BCD_CHU_CHANGE);
		ogBCD.SetDdxType("CHU", "DRT", BCD_CHU_CHANGE);
		ogBCD.SetDdxType("CHU", "URT", BCD_CHU_CHANGE);
	}


	pogInitialLoad->SetProgress(ilGant);

	if( __CanHandleCheckinCounterRestrictionsForHandlingAgents() )		// 050311 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents
	{
		// 050322 MVy: Check-In Counter Occupations for Handling Agents
		CString s = "Check-In Counter Occupations for Handling Agents" ;
		pogInitialLoad->SetMessage( "Loading Data for '" + s + "' ..." );
		ogBCD.SetObject( "OCC" );//, "HOPO,URNO,OCFR,OCTO,TIFR,TITO,OCBY,ALID,DAYS,REMA" );
		//ogBCD.AddKeyMap( "OCC", "URNO" );
		ogBCD.Read( "OCC" );
		ogBCD.SetDdxType( "OCC", "IRT", BCD_CIC_CHANGE );
		ogBCD.SetDdxType( "OCC", "DRT", BCD_CIC_CHANGE );
		//ogBCD.SetDdxType( "OCC", "URT", BCD_CIC_CHANGE );
		pogInitialLoad->SetProgress(ilGant);
	};

	// Ausg�nge
	pogInitialLoad->SetMessage(GetString(IDS_STRING1013));
	ogBCD.SetObject("EXT", "URNO,ENAM,TERM");
	ogBCD.SetObjectDesc("EXT", GetString(IDS_STRING1014));
	ogBCD.SetTableHeader("EXT", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("EXT", "ENAM", GetString(IDS_STRING986));
	ogBCD.AddKeyMap("EXT", "ENAM");
	ogBCD.Read(CString("EXT"));
	pogInitialLoad->SetProgress(ilGant);

	// Fids Remarks
/*
	pogInitialLoad->SetMessage(GetString(IDS_STRING1015));
	CString olFIDFields;
	olFIDFields.Format("URNO,%s,CODE", ogFIDRemarkField);
	ogBCD.SetObject("FID", olFIDFields);
	ogBCD.SetObjectDesc("FID", GetString(IDS_STRING1016));
	ogBCD.SetTableHeader("FID", ogFIDRemarkField, GetString(IDS_STRING985));
	ogBCD.SetTableHeader("FID", "CODE", GetString(IDS_STRING1017));
	ogBCD.AddKeyMap("FID", "CODE");
	ogBCD.Read(CString("FID"));
*/
	// Fids Remarks / Arrival
	pogInitialLoad->SetMessage(GetString(IDS_STRING1015));
	CString olFIDFields;
	olFIDFields.Format("URNO,%s,CODE", ogFIDRemarkField);
	ogBCD.SetObject("FID_ARR", "FID", olFIDFields);
	ogBCD.SetObjectDesc("FID_ARR", GetString(IDS_STRING1016));
	ogBCD.SetTableHeader("FID_ARR", ogFIDRemarkField, GetString(IDS_STRING985));
	ogBCD.SetTableHeader("FID_ARR", "CODE", GetString(IDS_STRING1017));
	ogBCD.AddKeyMap("FID_ARR", "CODE");
	ogBCD.Read(CString("FID_ARR"), "WHERE REMT IN ('A','B','X')");

	// Fids Remarks / Departure
	ogBCD.SetObject("FID_DEP", "FID", olFIDFields);
	ogBCD.SetObjectDesc("FID_DEP", GetString(IDS_STRING1016));
	ogBCD.SetTableHeader("FID_DEP", ogFIDRemarkField, GetString(IDS_STRING985));
	ogBCD.SetTableHeader("FID_DEP", "CODE", GetString(IDS_STRING1017));
	ogBCD.AddKeyMap("FID_DEP", "CODE");

	/*
	if(bgSecondGateDemandFlag)
		ogBCD.Read(CString("FID_DEP"), "WHERE REMT IN ('D','B')");
	else
	*/
		ogBCD.Read(CString("FID_DEP"), "WHERE REMT IN ('D','B','G','P')");


	ogBCD.SetObject("FID_GAT", "FID", olFIDFields);
	ogBCD.SetObjectDesc("FID_GAT", GetString(IDS_STRING1016));
	ogBCD.SetTableHeader("FID_GAT", ogFIDRemarkField, GetString(IDS_STRING985));
	ogBCD.SetTableHeader("FID_GAT", "CODE", GetString(IDS_STRING1017));
	ogBCD.AddKeyMap("FID_GAT", "CODE");
	ogBCD.Read(CString("FID_GAT"), "WHERE REMT IN ('G')");


	
	ogBCD.SetObject("FID_CIC", "FID", olFIDFields);
	ogBCD.SetObjectDesc("FID_CIC", GetString(IDS_STRING1016));
	ogBCD.SetTableHeader("FID_CIC", ogFIDRemarkField, GetString(IDS_STRING985));
	ogBCD.SetTableHeader("FID_CIC", "CODE", GetString(IDS_STRING1017));
	ogBCD.AddKeyMap("FID_CIC", "CODE");
	ogBCD.Read(CString("FID_CIC"), "WHERE REMT = 'C'");
	
	if(bgUNICODERemarks)
	{
		CString olOctItem;
		CString olAscii;
		CString olUrno;

		ilCount = ogBCD.GetDataCount("FID_ARR");
		for( int i = 0; i < ilCount; i++)
		{
			olOctItem = ogBCD.GetField("FID_ARR",i , ogFIDRemarkField);
			olUrno = ogBCD.GetField("FID_ARR",i , "URNO");
			ConvOctStToByteArr(olOctItem, olAscii);
			ogBCD.SetField("FID_ARR", "URNO" , olUrno, ogFIDRemarkField, olAscii);
		}

		ilCount = ogBCD.GetDataCount("FID_DEP");
		for(  i = 0; i < ilCount; i++)
		{
			olOctItem = ogBCD.GetField("FID_DEP",i , ogFIDRemarkField);
			olUrno = ogBCD.GetField("FID_DEP",i , "URNO");
			ConvOctStToByteArr(olOctItem, olAscii);
			ogBCD.SetField("FID_DEP", "URNO" , olUrno, ogFIDRemarkField, olAscii);
		}

		ilCount = ogBCD.GetDataCount("FID_GAT");
		for(  i = 0; i < ilCount; i++)
		{
			olOctItem = ogBCD.GetField("FID_GAT",i , ogFIDRemarkField);
			olUrno = ogBCD.GetField("FID_GAT",i , "URNO");
			ConvOctStToByteArr(olOctItem, olAscii);
			ogBCD.SetField("FID_GAT", "URNO" , olUrno, ogFIDRemarkField, olAscii);
		}
		ilCount = ogBCD.GetDataCount("FID_CIC");
		for(  i = 0; i < ilCount; i++)
		{
			olOctItem = ogBCD.GetField("FID_CIC",i , ogFIDRemarkField);
			olUrno = ogBCD.GetField("FID_CIC",i , "URNO");
			ConvOctStToByteArr(olOctItem, olAscii);
			ogBCD.SetField("FID_CIC", "URNO" , olUrno, ogFIDRemarkField, olAscii);
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////

	pogInitialLoad->SetProgress(ilGant);

	// Gates
	pogInitialLoad->SetMessage(GetString(IDS_STRING1018));
	ogBCD.SetObject("GAT", "URNO,GNAM,TERM,NAFR,NATO,RGA1,RGA2,RBAB,RESB,BUSG,GATR,DEFD");
	ogBCD.SetObjectDesc("GAT", GetString(IDS_STRING1019));
	ogBCD.SetTableHeader("GAT", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("GAT", "GNAM", GetString(IDS_STRING986));
	ogBCD.AddKeyMap("GAT", "GNAM");
	ogBCD.AddKeyMap("GAT", "RGA1");

	ogBCD.SetDdxType("GAT", "IRT", BCD_GAT_CHANGE);
	ogBCD.SetDdxType("GAT", "DRT", BCD_GAT_CHANGE);
	ogBCD.SetDdxType("GAT", "URT", BCD_GAT_CHANGE);


	ogBCD.Read(CString("GAT"));
	pogInitialLoad->SetProgress(ilGant);

	// init port and gates
	LoadPortAndGate();

	// Abfertigungsarten
	pogInitialLoad->SetMessage(GetString(IDS_STRING1020));
	ogBCD.SetObject("HTY", "URNO,HNAM,HTYP");
	ogBCD.SetTableHeader("HTY", "HTYP", GetString(IDS_STRING1009));
	ogBCD.SetTableHeader("HTY", "HNAM", GetString(IDS_STRING985));
	ogBCD.SetObjectDesc("HTY", GetString(IDS_STRING1021));
	ogBCD.AddKeyMap("HTY", "HTYP");
	ogBCD.Read(CString("HTY"));
	pogInitialLoad->SetProgress(ilGant);

	// Verkehrsarten
	pogInitialLoad->SetMessage(GetString(IDS_STRING1022));
	ogBCD.SetObject("NAT", "URNO,TNAM,TTYP,ALGA,ALPO,ALBB,ALWR");
	ogBCD.SetTableHeader("NAT", "TTYP", GetString(IDS_STRING1009));
	ogBCD.SetTableHeader("NAT", "TNAM", GetString(IDS_STRING985));
	ogBCD.SetObjectDesc("NAT", GetString(IDS_STRING1023));
	ogBCD.AddKeyMap("NAT", "TTYP");
	ogBCD.Read(CString("NAT"));

	ogBCD.SetDdxType("NAT", "IRT", BCD_NAT_CHANGE);
	ogBCD.SetDdxType("NAT", "DRT", BCD_NAT_CHANGE);
	ogBCD.SetDdxType("NAT", "URT", BCD_NAT_CHANGE);
	
	
	pogInitialLoad->SetProgress(ilGant);

	// Positionen
	pogInitialLoad->SetMessage(GetString(IDS_STRING1024));
	ogBCD.SetObject("PST", "URNO,PNAM,RESN,NAFR,NATO,CDAT,POSR,VATO,VAFR,TAXI,GPUS,DEFD,BRGS,MXAC");
	if (!ogPositionText.IsEmpty())
		ogBCD.SetObjectDesc("PST", ogPositionText);
	else
		ogBCD.SetObjectDesc("PST", GetString(IDS_STRING1025));

	ogBCD.SetTableHeader("PST", "PNAM", GetString(IDS_STRING986));
	ogBCD.AddKeyMap("PST", "PNAM");

	ogBCD.SetDdxType("PST", "IRT", BCD_PST_CHANGE);
	ogBCD.SetDdxType("PST", "DRT", BCD_PST_CHANGE);
	ogBCD.SetDdxType("PST", "URT", BCD_PST_CHANGE);


	ogBCD.Read(CString("PST"));
	pogInitialLoad->SetProgress(ilGant);

	// Start und Landebahnen
	pogInitialLoad->SetMessage(GetString(IDS_STRING1026));
	ogBCD.SetObject("RWY", "URNO,RNAM,RTYP,RNUM");
	ogBCD.SetObjectDesc("RWY", GetString(IDS_STRING1027));
	ogBCD.AddKeyMap("RWY", "RNAM");
	ogBCD.AddKeyMap("RWY", "RNUM");
	ogBCD.Read(CString("RWY"));
	pogInitialLoad->SetProgress(ilGant);


	ogBCD.SetObject("TWY");
	ogBCD.AddKeyMap("TWY", "TNAM");
	ogBCD.Read(CString("TWY"));


	// Season
	pogInitialLoad->SetMessage(GetString(IDS_STRING1028));
	ogBCD.SetObject("SEA", "URNO,SEAS,VPFR,VPTO,BEME");
	ogBCD.SetObjectDesc("SEA", GetString(IDS_STRING1007));
	ogBCD.SetTableHeader("SEA", "SEAS", "Name");
	ogBCD.SetTableHeader("SEA", "BEME", "Description");
	ogBCD.SetTableHeader("SEA", "VPFR", "From");
	ogBCD.SetTableHeader("SEA", "VPTO", "To");
	ogBCD.AddKeyMap("SEA", "SEAS");
	ogBCD.Read(CString("SEA"));
	pogInitialLoad->SetProgress(ilGant);

	ogBCD.SetObject("AWI");



	CString olLkey;
	if (strlen(pcmLKey) == 0)
		olLkey = ogLkeyFlight;//ogCfgData.GetLkey();
	else
		olLkey = pcmLKey;

	if (olLkey.IsEmpty())
		olLkey = "ENG";

	CString olSel = CString("WHERE LKEY='") + olLkey + CString("'");


	ogBCD.SetObject("LBL");
	ogBCD.AddKeyMap("LBL", "URNO");
	ogBCD.AddKeyMap("LBL", "TKEY");
	ogBCD.SetSort("LBL","URNO+",true);
	ogBCD.Read(CString("LBL"), olSel);

	ilCount = ogBCD.GetDataCount("LBL");


	RecordSet rlRec;
	char bufferTmp[128];

	for(int  i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("LBL", i, rlRec);
		CString llUrno = rlRec[ogBCD.GetFieldIndex("LBL", "URNO")];

		if(ogExtConflicts.Find(rlRec[ogBCD.GetFieldIndex("LBL", "TKEY")]) >= 0)
		{
			itoa(i+1000, bufferTmp, 10);
			ogMapLblUrnoToKonftype.SetAt(llUrno,bufferTmp);
		}

	}

	/*
	// VIPs
	// MBR Anf
	pogInitialLoad->SetMessage(GetString(IDS_STRING2351));
	ogBCD.SetObject("VIP");
	ogBCD.SetObjectDesc("VIP", GetString(IDS_STRING1749));
	ogBCD.Read(CString("VIP"));
	pogInitialLoad->SetProgress(ilGant);

	ogBCD.SetDdxType("VIP", "IRT", BCD_VIP_INSERT);
	ogBCD.SetDdxType("VIP", "DRT", BCD_VIP_DELETE);
	ogBCD.SetDdxType("VIP", "URT", BCD_VIP_UPDATE);
*/
	// HAI
	pogInitialLoad->SetMessage(GetString(IDS_STRING2352));
	ogBCD.SetObject("HAI");
	ogBCD.SetObjectDesc("HAI", GetString(IDS_STRING1769));
	ogBCD.AddKeyMap("HAI", "TASK");
	ogBCD.AddKeyMap("HAI", "REMA");
	ogBCD.AddKeyMap("HAI", "HSNA");
	ogBCD.Read(CString("HAI"));

	ogBCD.SetDdxType("HAI", "IRT", BCD_HAI_INSERT);
	ogBCD.SetDdxType("HAI", "DRT", BCD_HAI_DELETE);
	ogBCD.SetDdxType("HAI", "URT", BCD_HAI_UPDATE);

	ogBCD.SetObject("HAG", "URNO,HSNA,HNAM,TELE,FAXN");
	ogBCD.SetObjectDesc("HAG", GetString(IDS_STRING1769));
	ogBCD.SetTableHeader("HAG", "HSNA", GetString(IDS_STRING1768));
	ogBCD.SetTableHeader("HAG", "HNAM", GetString(IDS_STRING1769));
	ogBCD.AddKeyMap("HAG", "HNAM");
	ogBCD.AddKeyMap("HAG", "TELE");
	ogBCD.AddKeyMap("HAG", "FAXN");
	ogBCD.Read(CString("HAG"));

	ogBCD.SetDdxType("HAG", "IRT", BCD_HAI_INSERT);
	ogBCD.SetDdxType("HAG", "DRT", BCD_HAI_DELETE);
	ogBCD.SetDdxType("HAG", "URT", BCD_HAI_UPDATE);
	pogInitialLoad->SetProgress(ilGant);
	// MBR Ende


	// Servicetypen
	pogInitialLoad->SetMessage(GetString(IDS_STRING1006));
	ogBCD.SetObject("STY", "URNO,SNAM,STYP");
	ogBCD.SetObjectDesc("STY", GetString(IDS_STRING1008));
	ogBCD.SetTableHeader("STY", "SNAM", GetString(IDS_STRING986));
	ogBCD.SetTableHeader("STY", "STYP", GetString(IDS_STRING1009));
	ogBCD.AddKeyMap("STY", "STYP");
	ogBCD.Read(CString("STY"));
	pogInitialLoad->SetProgress(ilGant);


	// Warter�ume
	pogInitialLoad->SetMessage(GetString(IDS_STRING1003));
	ogBCD.SetObject("WRO", "URNO,WNAM,TERM,NAFR,NATO,GTE1,GTE2,DEFD,MAXF,SHGN,BRCA");
	ogBCD.SetObjectDesc("WRO", GetString(IDS_STRING1002));
	ogBCD.SetTableHeader("WRO", "WNAM", GetString(IDS_STRING986));
	ogBCD.SetTableHeader("WRO", "TERM", GetString(IDS_STRING1001));
	ogBCD.AddKeyMap("WRO", "WNAM");
	ogBCD.AddKeyMap("WRO", "GTE1");

	ogBCD.SetDdxType("WRO", "IRT", BCD_WRO_CHANGE);
	ogBCD.SetDdxType("WRO", "DRT", BCD_WRO_CHANGE);
	ogBCD.SetDdxType("WRO", "URT", BCD_WRO_CHANGE);
	
	
	ogBCD.Read(CString("WRO"));
	pogInitialLoad->SetProgress(ilGant);


	/*
	ogBCD.SetObject("FID_GAT", "FID", olFIDFields);
	ogBCD.SetObjectDesc("FID_GAT", GetString(IDS_STRING1016));
	ogBCD.SetTableHeader("FID_GAT", ogFIDRemarkField, GetString(IDS_STRING985));
	ogBCD.SetTableHeader("FID_GAT", "CODE", GetString(IDS_STRING1017));
	ogBCD.AddKeyMap("FID_GAT", "CODE");
	ogBCD.Read(CString("FID_GAT"), "WHERE REMT IN ('G')");

*/
	if(bgFIDSLogo)
	{
		// Logos
		CString olFields = "URNO,LGSN,LGFN,TYPE,ALC2,ALC3";

		ogBCD.SetObject("FLG_GAT", "FLG",olFields );
		ogBCD.SetObjectDesc("FLG_GAT", GetString(IDS_STRING2859));
		ogBCD.SetTableHeader("FLG_GAT", "LGSN", GetString(IDS_STRING2860));
		ogBCD.SetTableHeader("FLG_GAT", "LGFN", GetString(IDS_STRING985));
		ogBCD.SetTableHeader("FLG_GAT", "ALC2", GetString(IDS_STRING2791));
		ogBCD.SetTableHeader("FLG_GAT", "ALC3", GetString(IDS_STRING2791));
		ogBCD.AddKeyMap("FLG_GAT", "LGSN");
		ogBCD.Read(CString("FLG_GAT"), "WHERE TYPE='G'");


		ogBCD.SetObject("FLG_CKI", "FLG", olFields);
		ogBCD.SetObjectDesc("FLG_CKI", GetString(IDS_STRING2859));
		ogBCD.SetTableHeader("FLG_CKI", "LGSN", GetString(IDS_STRING2860));
		ogBCD.SetTableHeader("FLG_CKI", "LGFN", GetString(IDS_STRING985));
		ogBCD.SetTableHeader("FLG_CKI", "ALC2", GetString(IDS_STRING2791));
		ogBCD.SetTableHeader("FLG_CKI", "ALC3", GetString(IDS_STRING2791));
		ogBCD.AddKeyMap("FLG_CKI", "LGSN");

		
		ogBCD.Read(CString("FLG_CKI"), "WHERE TYPE='C'");

	}

	//useres
	ogBCD.SetObject("SEC", "URNO,USID,NAME,TYPE");
	olSel.Format("WHERE TYPE = 'U'");
	ogBCD.Read(CString("SEC"), olSel);
	int ilCountUser = ogBCD.GetDataCount("SEC");
	/////////////////////////////////////////////////////////////////////


	CString olTable;


	pogInitialLoad->SetProgress(ilGant);

	
	// Gruppenname
	pogInitialLoad->SetMessage(GetString(IDS_STRING1444));
	ogBCD.SetObject("GRN");
	ogBCD.AddKeyMap("GRN","GRPN");
	ogBCD.AddKeyMap("GRN","URNO");


	olSel.Format("WHERE APPL='%s' OR APPL='%s' OR APPL='%s'", "GATPOS","POPS", ogAppName);
	ogBCD.Read(CString("GRN"), olSel);
	pogInitialLoad->SetProgress(ilGant);

	olTable = CString("GRN") + CString(pcgTableExt);
	// for airline groups
	ogBCD.SetObject("GRN_ALT","GRN",CString("URNO,GRPN,TABN,APPL"));
	ogBCD.SetObjectDesc("GRN_ALT",GetString(IDS_STRING2774));
	ogBCD.SetTableHeader("GRN_ALT","GRPN",GetString(IDS_STRING2775));
	CString olSelection;
	olSelection="WHERE TABN='ALTTAB'";
	ogBCD.Read(CString("GRN_ALT"),olSelection);

	int ilCountGrn = ogBCD.GetDataCount("GRN_ALT");

	pogInitialLoad->SetProgress(ilGant);

	// Gruppenname
	ogGrmData.SetTableName(CString("GRM") + CString(pcgTableExt));
	ogGrmData.Read();

	pogInitialLoad->SetMessage(GetString(IDS_STRING2718)); // "Loading Flight Permits..."
	ogFpeData.ReadFpeData();

	ogBCD.SetObject("GRM");
	ogBCD.AddKeyMap("GRM", "VALU");

	olSel.Format("WHERE GURN IN (SELECT URNO FROM %s WHERE APPL='%s' OR APPL='%s' OR APPL='%s')", olTable, "GATPOS","POPS", ogAppName);
	ogBCD.Read(CString("GRM"), olSel);
	pogInitialLoad->SetProgress(ilGant);

	//DurationAllocationTime
	if (blDatPosDura || blDatGatDura)
	{
		ogBCD.SetObject("DAT");
		ogBCD.AddKeyMap("DAT", "ACGR");
		ogBCD.AddKeyMap("DAT", "ALGR");
		ogBCD.AddKeyMap("DAT", "NAGR");
		ogBCD.AddKeyMap("DAT", "FLTY");
		ogBCD.AddKeyMap("DAT", "DURA");
		ogBCD.AddKeyMap("DAT", "ALID");
		ogBCD.AddKeyMap("DAT", "URNO");
		ogBCD.SetSort("DAT","URNO+",true);
		ogBCD.Read(CString("DAT"));
		pogInitialLoad->SetProgress(ilGant);
	}


	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////

	ogBCD.SetObject("GHP");
	ogBCD.SetObject("GPM");
	ogBCD.AddKeyMap("GPM", "RKEY");



	olSel.Format("WHERE APPL='%s' OR APPL='%s' OR APPL='%s'", "GATPOS","POPS", ogAppName);
	ogBCD.Read(CString("GHP"), olSel);

	int h = ogBCD.GetDataCount("GHP");

	olTable = CString("GHP") + CString(pcgTableExt);


	olSel.Format("WHERE RKEY IN (SELECT URNO FROM %s WHERE APPL='%s' OR APPL='%s' OR APPL='%s')", olTable, "GATPOS","POPS", ogAppName);
	ogBCD.Read(CString("GPM"), olSel);

	int j = ogBCD.GetDataCount("GPM");
	
	pogInitialLoad->SetProgress(ilGant);

	//Reading CRATAB - PRF 8379			
	ogBCD.SetObject("CRC");
	ogBCD.SetObjectDesc("CRC", GetString(IDS_STRING2804));
	ogBCD.AddKeyMap("CRC", "CODE");
	ogBCD.SetSort("CRC","CODE+",true);

	if(bgReasonFlag)	
		ogBCD.Read("CRC");	


	if(bgBlackList)
	{
		ogBwaData.ReadAll();
	}

	//Reading CRATAB - PRF 9029			
	CString olFields("UNRO,CODE,CXXF,GATF,POSF,REMA");
	ogBCD.SetObject("CRC_CXX", "CRC", olFields);
	ogBCD.SetObjectDesc("CRC_CXX", GetString(IDS_STRING2804));
	ogBCD.AddKeyMap("CRC_CXX", "CODE");
	ogBCD.SetSort("CRC_CXX","CODE+",true);

	if(bgCxxReason)	
		ogBCD.Read("CRC_CXX", "WHERE CXXF='X'");	



	ogBCD.SetDdxType("CRC", "IRT", BCD_CRC_CHANGE);
	ogBCD.SetDdxType("CRC", "DRT", BCD_CRC_CHANGE);
	ogBCD.SetDdxType("CRC", "URT", BCD_CRC_CHANGE);
	pogInitialLoad->SetProgress(ilGant); 
	

	


	//RST??? ogCraData.ReadCraData();  //PRF 8379 
	pogInitialLoad->SetProgress(ilGant);  

	// Konfigurationsdaten
	pogInitialLoad->SetMessage(GetString(IDS_STRING1004));
	ogCfgData.GetFlightConfig();
	ogCfgData.ReadCfgData();
	ogCfgData.SetCfgData();
	ogCfgData.ReadMonitorSetup();
	ogCfgData.ReadFontSetup();
	ogCfgData.ReadPopsReportDailyAlcSetup();
	ogCfgData.ReadConflictSetup();	
	ogCfgData.ReadAllConfigData();

	if(bgBarColorNature)
	{
		ogCfgData.ReadNatureBarColorSetup();// Added by Christine
	}

	ogSpotAllocation.Init();
	

	if(bgWingoverBuffer)
		ogCfgData.ReadWingoverlapBufferSetup();	


	ogKonflikte.SetConfiguration();
	ogCfgData.ReadGateBufferTimeSetup();	
	ogCfgData.ReadPosBufferTimeSetup();	
	ogCfgData.ReadXminutesBufferTimeSetup(); //PRF 8379  
	ogCfgData.ReadArchivePastBufferTimeSetup();	
	ogCfgData.ReadArchiveFutureBufferTimeSetup();	
	if( __CanHandlePositionFlightTodayOnlyCheck() )		// 050310 MVy: today only or period for position flight check
		ogCfgData.ReadArchivePostFlightTodayOnly();		// 050309 MVy: today only or period, read data from db
	if( __CanHandleCheckinCounterRestrictionsForHandlingAgents() )		// 050311 MVy: Handling Agents
		ogCfgData.ReadHandlingAgent();		// 050311 MVy: Handling Agents, initialize global data like urno
	ogCfgData.ReadResChainDlgSetup();	
	ogCfgData.ReadXDaysSetup();	
	ogCfgData.ReadTimelineBufferSetup();

	pogInitialLoad->SetProgress(ilGant);

	// Ansichten
	pogInitialLoad->SetMessage(GetString(IDS_STRING1005));
	InitSeasonFlightTableDefaultView();
	pogInitialLoad->SetProgress(ilGant);


	// resource groups
	pogInitialLoad->SetMessage(GetString(IDS_STRING1976));
	ogResGroupData.ReadGroups();
	pogInitialLoad->SetProgress(ilGant);

	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->DestroyWindow();
		pogInitialLoad = NULL;
	}
	if( bgUseBeltAllocTime) {
		ogBlaData.ReadAllBlas();
	}
	CCS_CATCH_ALL


	//olTime = CTime::GetCurrentTime();
	//TRACE("\n\nend    InitialLoad  %s", olTime.Format("%H:%M:%S"));

}


void CFPMSApp::LoadPortAndGate()
{
	RecordSet rlRec;
	int ilCount = ogBCD.GetDataCount("GAT");

	CString olUrnoPort("");
	CString olUrnoGate("");

	for(int i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("GAT", i, rlRec);
		
		CString llUrno = rlRec[ogBCD.GetFieldIndex("GAT", "URNO")];

		CString olResb;
		ogBCD.GetField("GAT", "URNO", llUrno, "GATR", olResb); 

//		char buffer[32];
//		ltoa(llUrno, buffer, 10);

		CStringArray olArray;
		ExtractItemList(olResb, &olArray, ';');
		if(olArray.GetSize() >= 5)
		{
			if(olArray[3] == "X") //Inbo
				olUrnoPort += CString(llUrno) + ',';

			if(olArray[4] == "X") //Outbo
				olUrnoGate += CString(llUrno) + ',';
		}
	}

	char pclShowPortWoInInd[256];
	char pclShowGateWoOutInd[256];
	GetPrivateProfileString("FIPS", "SHOW_PORT_WO_INBOUND_IND", "N",pclShowPortWoInInd, sizeof pclShowPortWoInInd, GetConfigPath() );
	GetPrivateProfileString("FIPS", "SHOW_GATE_WO_OUTBOUND_IND", "N",pclShowGateWoOutInd, sizeof pclShowGateWoOutInd, GetConfigPath() );
	

	if (stricmp(pclShowPortWoInInd,"Y")==0)
	{//Show All Ports Without Inbound Indicator.
		LoadPort("");
	}
	else if(!olUrnoPort.IsEmpty())
	{
		olUrnoPort = olUrnoPort.Left(olUrnoPort.GetLength() - 1);

		CString olSelection;
  		olSelection.Format("WHERE URNO IN (%s)", olUrnoPort);
		LoadPort( olSelection );
	}

	if (stricmp(pclShowGateWoOutInd,"Y")==0)
	{//Show All Gates Without Outbound Indicator.
		LoadGate("");
	}
	else if(!olUrnoGate.IsEmpty())
	{
		olUrnoGate = olUrnoGate.Left(olUrnoGate.GetLength() - 1);

		CString olSelection;
  		olSelection.Format("WHERE URNO IN (%s)", olUrnoGate);
		LoadGate( olSelection);
	}
}

void CFPMSApp::LoadPort(const CString polWhereClause)
{
	ogBCD.SetObject("GAT_PORT", "GAT", CString("URNO,GNAM,TERM,NAFR,NATO,RGA1,RGA2,RBAB,RESB,BUSG,GATR,DEFD"));
	ogBCD.SetObjectDesc("GAT_PORT", GetString(IDS_STRING2302));
	ogBCD.SetTableHeader("GAT_PORT", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("GAT_PORT", "GNAM", GetString(IDS_STRING986));
	ogBCD.AddKeyMap("GAT_PORT", "GNAM");
	ogBCD.AddKeyMap("GAT_PORT", "RGA1");

	ogBCD.Read(CString("GAT_PORT"), polWhereClause);

	ogBCD.SetDdxType("GAT_PORT", "IRT", BCD_GAT_CHANGE);
	ogBCD.SetDdxType("GAT_PORT", "DRT", BCD_GAT_CHANGE);
	ogBCD.SetDdxType("GAT_PORT", "URT", BCD_GAT_CHANGE);
}

void CFPMSApp::LoadGate(const CString polWhereClause)
{
	ogBCD.SetObject("GAT_GATE", "GAT", CString("URNO,GNAM,TERM,NAFR,NATO,RGA1,RGA2,RBAB,RESB,BUSG,GATR,DEFD"));
	ogBCD.SetObjectDesc("GAT_GATE", GetString(IDS_STRING1019));
	ogBCD.SetTableHeader("GAT_GATE", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("GAT_GATE", "GNAM", GetString(IDS_STRING986));
	ogBCD.AddKeyMap("GAT_GATE", "GNAM");
	ogBCD.AddKeyMap("GAT_GATE", "RGA1");

	ogBCD.Read(CString("GAT_GATE"), polWhereClause);

	ogBCD.SetDdxType("GAT_GATE", "IRT", BCD_GAT_CHANGE);
	ogBCD.SetDdxType("GAT_GATE", "DRT", BCD_GAT_CHANGE);
	ogBCD.SetDdxType("GAT_GATE", "URT", BCD_GAT_CHANGE);
}


void CFPMSApp::Customize()
{
	
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//
	// in den Projektsettings die Sprache ggf. umstellen  ( Englisch(UK) und _ENGLISH )
	//
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!


	// Deutsche Versionen !!!

		// Hannover Langenhagen

		//ogAppName	= "FPMS";
		//ogCustomer	= "HAJ";

		// POPS f�r FAG   --> hier sind Anpassungen vorhanden !!! 

		//ogAppName	= "POPS";
		//ogCustomer	= "BVD";


	// Englische Versionen !!!


		// FIPS-MASTERVERSION  

		//ogAppName	= "FIPS";
		//ogCustomer	= "";

	
		// FIPS-MASTERVERSION  

		ogAppName	= "FIPS";
		ogCustomer	= "SHA";


		// FIPS f�r Lissabon 

//		ogAppName	= "FIPS";
//		ogCustomer	= "LIS";


	//---------------------------------------------------------------------------
}





bool CFPMSApp::ShowFlightRecordData(CWnd *opParent, char cpFTyp, long lpUrno, long lpRkey, char cpAdid, const CTime &ropTifad, bool bpLocalTimes, char cpMask)
{

	if (cpFTyp == 'T' || cpFTyp == 'G')
	{
		// ACTIVATE TOWING-DIALOG
		if(pogRotGroundDlg == NULL)
		{
			pogRotGroundDlg = new RotGroundDlg(opParent);
			pogRotGroundDlg->Create(IDD_ROTGROUND);
		}

		pogRotGroundDlg->SetWindowPos(&opParent->wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
		pogRotGroundDlg->NewData(opParent, lpRkey, lpUrno, cpAdid, bpLocalTimes);
	}
	else
	{
		// ACTIVATE ROTATION-MASK
		bool blSeasonMask = true;
		switch (cpMask)
		{
		case 'S':
			blSeasonMask = true;
			break;
		case 'D':
			blSeasonMask = false;
			break;
		default:
			{
				// decide with the given time if daily- or season-rotation-mask shall be activated		
				CTime olCurrTime = CTime::GetCurrentTime();
				// SeasonMask if the given time is at least 2 days in the future
				if (ropTifad - CTimeSpan(2,0,0,0) < CTime::GetCurrentTime())
				{	
					blSeasonMask = false;
				}
				else
				{
					blSeasonMask = true;
				}
			}
			break;
		}

		if (blSeasonMask)
		{
			pogSeasonDlg->NewData(opParent, lpRkey, lpUrno, DLG_CHANGE, bpLocalTimes, cpAdid);
		}
		else
		{
 			pogRotationDlg->NewData(opParent, lpRkey, lpUrno, cpAdid, bpLocalTimes);
		}
	}

	return true;
}


bool CFPMSApp::SetCustomerConfig()
{
	// enable all functions for release !!

	// Lines in the 'create daily schedule' will be blue if the arrival flight has an atd of origin
//	bgCreateDaily_OrgAtdBlue = true;
	// enable additional baggage belt check
	bgExtBltCheck = true;
	// enable yellow bars when flight is international
	bgFlightIntYelBar = true;
	// enable editing of flight ID
	bgFlightIDEditable = true;

	return true;
}
 

int CFPMSApp::MyTopmostMessageBox(CWnd *polWnd, const CString &ropText, const CString &ropCaption, UINT ipType /* = 0 */)
{
	HWND hWnd = NULL;
	if (polWnd != NULL)
		hWnd = polWnd->m_hWnd;

	UnsetTopmostWnds();
	int ilRet = ::MessageBox(hWnd, ropText, ropCaption, ipType | MB_APPLMODAL | MB_TOPMOST);
	SetTopmostWnds();

	return ilRet;
}


void CFPMSApp::UnsetTopmostWnds()
{
	if(pogWoResTableDlg)
		pogWoResTableDlg->SetWindowPos( &pogWoResTableDlg->wndNoTopMost, 0,0,0,0, SWP_NOSIZE  | SWP_NOMOVE);
}


void CFPMSApp::SetTopmostWnds()
{
	if(pogWoResTableDlg)
		pogWoResTableDlg->SetWindowPos( &pogWoResTableDlg->wndTopMost  , 0,0,0,0, SWP_NOSIZE  | SWP_NOMOVE);
}


bool CFPMSApp::DumpDebugLog(const CString &ropStr)
{

	if (!bgDebug) return false;

	CTime olCurrTime = CTime::GetCurrentTime();
	of_debug << olCurrTime.Format("%d.%m.%Y %H:%M:%S:  ") << ropStr << endl;

	return true;
}


bool CFPMSApp::CheckCedaError(const CCSCedaData &ropCedaData)
{
	if (ropCedaData.imLastReturnCode != ropCedaData.RC_SUCCESS)
	{
		CString olMsg;
		// Dump into logfile
		olMsg.Format("ERROR: Ceda reports: '%s'", ropCedaData.omLastErrorMessage);
		DumpDebugLog(olMsg);

		if(ropCedaData.omLastErrorMessage.Find("ORA") != -1)
		{
			// display message
			olMsg = CString("CEDA reports:\n\n") + ropCedaData.omLastErrorMessage;
			CFPMSApp::MyTopmostMessageBox(NULL, olMsg, GetString(ST_FEHLER), MB_ICONERROR);
		}

		return false;
	}
	return true;
}


CString CFPMSApp::GetSelectedReason(CWnd *poWnd, bool blHideCancel)
{
	// Get all the reasons of olField //PRF 8379 
	CCSPtrArray<RecordSet> olCrcRecords;
	ogBCD.GetRecords("CRC", "POSF", "X", &olCrcRecords);
	CMapStringToString olResMapCRC;
	CString olRet;
	CStringArray olRemaArr;
	for (int i = 0; i < olCrcRecords.GetSize(); i++)
	{
		CString olUrno = olCrcRecords[i].Values[ogBCD.GetFieldIndex("CRC", "URNO")];
		CString olRema = olCrcRecords[i].Values[ogBCD.GetFieldIndex("CRC", "REMA")];
		if(olUrno.GetLength() >0)
		{
			olRemaArr.Add(olRema);
			olResMapCRC.SetAt(olRema, olUrno);
		}
	}

	//Display all the reasons
	CListStringArrayDlg *polDlg = new CListStringArrayDlg(poWnd,"Reasons For Position Change",&olRemaArr,"",blHideCancel);
	if(polDlg->DoModal() == IDOK)
	{
		olRet = polDlg->GetField();
	}
	olResMapCRC.Lookup(olRet,olRet);
	olCrcRecords.RemoveAll();
	delete polDlg;

	return olRet;
}

CString CFPMSApp::GetSelectedReason(CWnd* rppWnd, const CString& ropField,const CString& ropSelStr, CString opCaption)
{
	// Get all the reasons of olField //PRF 8379 
	CCSPtrArray<RecordSet> olCrcRecords;
    ogBCD.GetRecords("CRC", ropField, "X", &olCrcRecords);
	CMapStringToString olResMapCRC;
	CString olCaption;

	CString olSelStr = "";
	CString olRet;
	CStringArray olRemaArr;
	for (int i = 0; i < olCrcRecords.GetSize(); i++)
	{
		CString olUrno = olCrcRecords[i].Values[ogBCD.GetFieldIndex("CRC", "URNO")];
		CString olRema = olCrcRecords[i].Values[ogBCD.GetFieldIndex("CRC", "REMA")];
		if(olUrno.GetLength() >0)
		{
			olRemaArr.Add(olRema);
			olResMapCRC.SetAt(olRema, olUrno);

			if(olUrno == ropSelStr)
				olSelStr = olRema;
		}
	}

	if(opCaption.IsEmpty())
	{
		if(ropField =="POSF")
			olCaption = "Reasons For Position Change";
		else if(ropField =="BELT")
			olCaption = "Reasons For Belt Change";
		else
			olCaption = "Reasons For Gate Change";
	}
	else
		olCaption = opCaption;

	//Display all the reasons
	CListStringArrayDlg *polDlg = new CListStringArrayDlg(rppWnd,olCaption,&olRemaArr,olSelStr);
	//AwBasicDataDlg *polDlg = new AwBasicDataDlg(rppWnd,"Reasons For Change",olRemaArr,ropSelStr);
	if(polDlg->DoModal() == IDOK)
	{
		olRet = polDlg->GetField();	
	}
	
	delete polDlg;
	olCrcRecords.RemoveAll();
	olResMapCRC.Lookup(olRet,olRet);
	return olRet;
}

//UFIS-1078:SSIM, AM:20111028  - Start

void CFPMSApp::UpdateSSIM()
{
	CCS_TRY

	m_bShowSSIM = false ;
	char pc[1000];
	int iLen = 0 ;
	// hint: only the length of the entered string will be compared
    if( iLen = GetPrivateProfileString( ogAppName, "SSIM", "N", pc, sizeof( pc ) -1, GetConfigPath() ) )
	{
		ASSERT( pc );
		if( !strncmp( pc, "Y", iLen ) ) m_bShowSSIM = true ; 
	};

	CCS_CATCH_ALL
};

bool CFPMSApp::CanSeeSSIM()
{
	return m_bShowSSIM;
};

//UFIS-1078 AM:20111028 - End
