// stviewer.h : header file
// 17-jan-01	rkr			PRF58-ATH: color_coding for "I" and "D"(struct POSDIA_BARDATA changed)
//

#ifndef _POSDIAVIEWER_H_
#define _POSDIAVIEWER_H_

#include <cviewer.h>
#include <CCSPrint.h>
#include <DiaCedaFlightData.h>
#include <CedaBasicData.h>
#include <Utils.h>

struct POSDIA_INDICATORDATA
{
	// standard data for indicators in general GanttBar
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
};
struct POSDIA_BARDATA 
{
	long Urno;
	char FPart;
	long Rkey;
	long AUrno;
	char AFPart;
	long DUrno;
	char DFPart;
	long ARkey;
	long DRkey;
	
	CString AFtyp;
	CString DFtyp;

    CString Text;
    CTime StartTime;
    CTime EndTime;
	CTime GatBegin;
	CTime GatEnd;
	CTime WroBegin;
	CTime WroEnd;
	CTime BltBegin;
	CTime BltEnd;
	CTime CcaBegin;
	CTime CcaEnd;
	CTime Tmo;
	CTime Land;
	CTime Onbl;
	CTime Ofbl;
	CTime Slot;
	CTime Airb;

	CString Belt;
	CString Gate;
	CString Wro;
	CString Cca;
	CString Paxt;
	CString Act3;

	CString Pafp;
	CString Pdfp;

	CString StatusText;
    int FrameType;
    int MarkerType;

//	the left side of the bar
    CBrush *MarkerBrush;
//	the left right of the bar
    CBrush *MarkerBrushRightSide;

	CBrush *TriangleLeft;
	CBrush *TriangleRight;
	CPen *FramePen;
	COLORREF TextColor;
	COLORREF TriangelColorRight;
	COLORREF TriangelColorLeft;

	CString PrintText;

    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	CCSPtrArray<POSDIA_INDICATORDATA> Indicators;

	CString SpecialREQ;
	int SpecialREQStaus;

	int StartDefDura;
	int EndDefDura;

	CString AAlert;//Arrival Flight Alert string to use to show the popup icon menu//AM 20070919
	CString DAlert;//Arrival Flight Alert string to use to show the popup icon menu//AM 20070919

	bool VipA;
	bool FixA;
	bool VipD;
	bool FixD;

	bool TowArrPosFree;
	bool IsDepTowing;

	POSDIA_BARDATA(void)
	{
		AUrno = 0;
		DUrno = 0;
		ARkey = 0;
		DRkey = 0;
		StartTime = TIMENULL;
		EndTime = TIMENULL;
		GatBegin = TIMENULL;
		GatEnd = TIMENULL;
		WroBegin = TIMENULL;
		WroEnd = TIMENULL;
		BltBegin = TIMENULL;
		BltEnd = TIMENULL;
		CcaBegin = TIMENULL;
		CcaEnd = TIMENULL;
		Tmo = TIMENULL;
		Land = TIMENULL;
		Onbl = TIMENULL;
		Ofbl = TIMENULL; 
		Slot = TIMENULL;
		Airb = TIMENULL;
		FramePen = NULL;
		TextColor = COLORREF(BLACK);
		SpecialREQ = "";
		SpecialREQStaus = NO_REQ;
		StartDefDura = 0;
		EndDefDura = 0;
		AAlert = "";
		DAlert = "";
		VipA = false;
		FixA = false;
		VipD = false;
		FixD = false;
		TowArrPosFree = false;
		IsDepTowing = false;
	}
};

struct POSDIA_BKBARDATA
{
	long AUrno;
	long DUrno;
	long ARkey;
	long DRkey;
    CString Text;
    CString Type;
	CTime StartTime;
    CTime EndTime;
    CBrush *MarkerBrush;	// background bar will be only displayed in FRAMENONE, MARKFULL
	CBrush *SepBrush;
	POSDIA_BKBARDATA(void)
	{
		AUrno = 0;
		DUrno = 0;
		ARkey = 0;
		DRkey = 0;
		Text = "";
		Type = "";
		StartTime = TIMENULL;
		EndTime  = TIMENULL;
		MarkerBrush = NULL;
		SepBrush = NULL;
	}
};

struct POSDIA_LINEDATA 
{
	long Urno;
	CString Pnam;
	CString Term;

	long AUrno;
	long DUrno;
	long ARkey;
	long DRkey;
	CTime StartTime;
	CTime EndTime;
    CString Text;
	bool IsExpanded;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
    CCSPtrArray<POSDIA_BARDATA> Bars;		// in order of painting (leftmost is the bottommost)
    CCSPtrArray<POSDIA_BKBARDATA> BkBars;	// background bar
    CCSPtrArray<int> TimeOrder;				// maintain overlapped bars, see below
	COLORREF TextColor;
	COLORREF TextColor2;
	COLORREF TextColor3;
	bool bmFullHeight;
	CBrush *BkBrush;
	CBrush *SepBrush;
	POSDIA_LINEDATA(void)
	{
		IsExpanded = false;
		bmFullHeight = false;
	}
	/*
	~POSDIA_LINEDATA()
	{
		Bars.DeleteAll();
		BkBars.DeleteAll();
		TimeOrder.DeleteAll();
	}
	*/
};
struct POSDIA_GROUPDATA 
{
	int GroupNo;
	CString GroupName;			// Gruppen ID
	CBrush *BkBrush;
	CBrush *SepBrush;
	//CString GroupType;			// 
	//CCSPtrArray<CString> Codes; // For future Airlinecodes
	// standard data for groups in general Diagram
    //CString Text;
    CCSPtrArray<POSDIA_LINEDATA> Lines;
	bool bmFullHeight;
	POSDIA_GROUPDATA(void)
	{
		bmFullHeight = false;
	}
	/*
	~POSDIA_GROUPDATA()
	{
		Lines.DeleteAll();
	}
	*/
};

// Attention:
// Element "TimeOrder" in the struct "POSDIA_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array POSDIA_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the POSDIA_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array POSDIA_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// PosDiagramViewer window
 
class PosGantt;
class PosChart;

class PosDiagramViewer: public CViewer
{
	friend PosGantt;

// Constructions
public:
    PosDiagramViewer();
    ~PosDiagramViewer();

	int  AdjustBar(DIAFLIGHTDATA *prpAFlight, DIAFLIGHTDATA *prpDFlight,int &ripChartNo);
	int  FindFirstBar(long lpUrno,int &ripChartNo); 

	void Attach(CWnd *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName, CTime opStartTime, CTime opEndTime);
	int GetItemID(int ipGroupNo, int ipLineNo) const;

	void MakeMasstab();
	int GetGeometryFontIndex(){return omGeometryFontIndex;}
	CTime GetGeometrieStartTime(){return omStartShowTime;}
	CTimeSpan GetGeometryTimeSpan(){return omGeometryTimeSpan;}
	int GetAllLineCount();
    CString GetGroupTopScaleText(int ipGroupno);

	void AllowUpdates(BOOL bpNoUpdatesNow);
	bool CalculateTimes(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CTime &opStart, CTime &opEnd, bool bpPreCheck = false, CString opPrePos = CString(""));

	void PrintGantt(PosChart *popChart,bool bpFirstChart);
	void PrintGanttEnd();
	void PrintSingleLine(int ipGroupNo, int ipLineNo);

	void UpdatePositionLines(CString opPos);
	int GetGroupCount();
	CString GetGroupText(int ipGroupno);
	bool bmGeometryTimeLines;
	BOOL bmNoUpdatesNow;

	void UpdateLineHeights();
	bool GetLoadedFlights(CCSPtrArray<DIAFLIGHTDATA> &opFlights);
	int GetLineOfFirstVisibleBar(int ipGroup, CTime opTSStartTime);
	void ShowTowArrPosFree();

	CTime omMarkTimeStart;
	CTime omMarkTimeEnd;


// Internal data processing routines
private:
	void PrepareGrouping();
	void PrepareFilter();
	void PrepareSorter();
	bool IsPassFilter(DIAFLIGHTDATA *prpFlight);
	bool IsPassFilter(RecordSet &ropRec);
	bool IsPassFilter(DiaCedaFlightData::RKEYLIST *popRot);
	int CompareGroup(POSDIA_GROUPDATA *prpGroup1, POSDIA_GROUPDATA *prpGroup2);
	int CompareLine(POSDIA_LINEDATA *prpLine1, POSDIA_LINEDATA *prpLine2);

	bool CompressOverlapping();


	bool CheckBkBarParameters(int ipGroupno, int ipLineno, int ipBkBarno) const;
	bool CheckBarParameters(int ipGroupno, int ipLineno, int ipBarno) const;
	bool CheckBarParameters(int ipGroupno, int ipLineno) const;


	DIAFLIGHTDATA * GetFlightAInRotation(DiaCedaFlightData::RKEYLIST *popRotation);
	DIAFLIGHTDATA * GetFlightDInRotation(DiaCedaFlightData::RKEYLIST *popRotation);
	bool MakeGroupsOld();
	bool MakeGroups();
	bool MakeGroupAll();

	void MakeBars();

	void MakeBars( DiaCedaFlightData::RKEYLIST *prlRkey );
	
//	void MakeLine(POSDIA_GROUPDATA &rrpGroup, RecordSet &ropRec);
//	void MakeLineData(POSDIA_LINEDATA *prlLine, RecordSet &ropRec);
	void MakeLine(POSDIA_GROUPDATA &rrpGroup, RecordSet &ropRec, bool bpRECfromBLK = false);
	void MakeLineData(POSDIA_GROUPDATA &rrpGroup, POSDIA_LINEDATA *prlLine, RecordSet &ropRec, bool bpRECfromBLK = false);
	void MakeTriangleColors(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, POSDIA_BARDATA *prpBar);

	void MakeBarData(POSDIA_BARDATA *prlBar, DIAFLIGHTDATA *popFlight);

	void MakeBarData(POSDIA_BARDATA *prlBar, DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);

	void MakeBar(int ipGroupno, int ipLineno, DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);
	void MakeFwrBar(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);

	long GetRkeyFromRotation(DiaCedaFlightData::RKEYLIST *popRotation);
	
	bool GetFlightsInRotation(DiaCedaFlightData::RKEYLIST *popRotation, DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);

	CString GroupText(int ipGroupNo);
	CString LineText(DiaCedaFlightData::RKEYLIST *popRot);
	CString BarText(DiaCedaFlightData::RKEYLIST *popRot);
	CString BarTextAndValues(POSDIA_BARDATA *prlBar);
	CString StatusText(DiaCedaFlightData::RKEYLIST *popRot);
	int FindGroup(CString opGroupName);
	bool FindLine(CString opPnam, CUIntArray &ropGroups, CUIntArray &ropLines);
	bool FindGroupsForType(CUIntArray &ropGroupNos, CString opType, CString opPfc);
	BOOL FindDutyBar(long lpUrno, int &ripGroupno, int &ripLineno);
	bool FindDutyBars(long lpUrno, CUIntArray &ropGroups, CUIntArray &ropLines);
	BOOL FindBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno);
	bool FindBarsGlobal(long lpUrno, CUIntArray &ropGroups, CUIntArray &ropLines, CUIntArray &ropBars);

	bool GetGroupAndLineNoOfItem(int ipItemID, int &ripGroupNo, int &ripLineNo) const;

	CBrush *GetBarColor(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);
	CBrush *GetBarColor(DIAFLIGHTDATA *prpFlight, char cpFPart);


//	bool CheckAvailability(DIAFLIGHTDATA *prpFlight, CString opPnam);
	bool CheckAvailability(DIAFLIGHTDATA *prpFlight, POSDIA_LINEDATA *prlLine, bool bpPreCheck = false);

	void UtcToLocal(POSDIA_BKBARDATA *prlBar);
	void UtcToLocal(POSDIA_BARDATA *prlBar);

	void SetKonfliktColor(int ipGroupno, int ipLineno);
	bool CalculateBarColors(POSDIA_BARDATA *prpBar, DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);

	bool MakeBlkData(/*DIAFLIGHTDATA *prpFlight, POSDIA_LINEDATA *prlLine*/CTime &opStartTime, CTime &opEndTime, CString &opTabn, CString &opName, CStringArray &opNafr, CStringArray &opNato, CStringArray &opResn);

// Operations
	CImageList* omImageListNotValid;
 	int omGeometryFontIndex;
	CTimeSpan omGeometryTimeSpan;
	bool bmGeometryMinimize;
	CTime omStartShowTime;
	int   imGeometrieHours;

    POSDIA_GROUPDATA *GetGroup(int ipGroupno);
    
	int GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd);

    POSDIA_LINEDATA *GetLine(int ipGroupno, int ipLineno);
    CString GetLineText(int ipGroupno, int ipLineno);
    int GetMaxOverlapLevel(int ipGroupNo, int ipLineno);
 	bool IsExpandedLine(int ipGroupNo, int ipLineno) const;
	void SetExpanded(int ipGroupno, int ipLineno);

 	bool GetFullHeight(int ipGroupNo, int ipLineno) const;

     int GetBkBarCount(int ipGroupno, int ipLineno);
    POSDIA_BKBARDATA *GetBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    CString GetBkBarText(int ipGroupno, int ipLineno, int ipBkBarno);
    int GetBarCount(int ipGroupno, int ipLineno);
    POSDIA_BARDATA *GetBar(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBarText(int ipGroupno, int ipLineno, int ipBarno);
    CString GetStatusBarText(int ipGroupno, int ipLineno, int ipBarno);
	int GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno);
	POSDIA_INDICATORDATA *GetIndicator(int ipGroupno, int ipLineno, int ipBarno,
		int ipIndicatorno);

    void DeleteAll();
    int CreateGroup(POSDIA_GROUPDATA *prpGroup);
    void DeleteGroup(int ipGroupno);
    int CreateLine(POSDIA_GROUPDATA &rrpGroup, POSDIA_LINEDATA *prpLine);
    void DeleteLine(int ipGroupno, int ipLineno);
    int CreateBkBar(int ipGroupno, int ipLineno, POSDIA_BKBARDATA *prpBkBar);
    void DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    int CreateBar(int ipGroupno, int ipLineno, POSDIA_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipGroupno, int ipLineno, int ipBarno, bool bpSetOverlapColor = true);
    int CreateIndicator(int ipGroupno, int ipLineno, int ipBarno,
    	POSDIA_INDICATORDATA *prpIndicator);
	void DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);

    int GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
        int ipOverlapLevel1, int ipOverlapLevel2);
    int GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2);
	bool FindBKBarsGlobal(long lpUrno, CUIntArray &ropGroups, CUIntArray &ropLines, CUIntArray &ropBKBars);


	bool GetGeometryTimeLines(){return bmGeometryTimeLines;}
	bool GetGeometryMinimize(){return bmGeometryMinimize;}



	bool GetDispoZeitraumFromTo(CTime &ropFrom, CTime &ropTo);

	// Private helper routines
 	void GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
		CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);
	bool GetGroupsFromViewer(CStringArray &ropGroups);
	void PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine);
	void PrintDiaArea(PosChart *popChart);
	void PrintDiagramHeader();

	void PrintPrepareBKLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintBKLine);

	bool CalculateWro(POSDIA_BARDATA *prpBar, DIAFLIGHTDATA *prpFlight);
	bool CalculateBlt(POSDIA_BARDATA *prpBar, DIAFLIGHTDATA *prpFlight);
	bool CalculateCca(POSDIA_BARDATA *prpBar, DIAFLIGHTDATA *prpFlight);
	bool CalculateGateAD(POSDIA_BARDATA *prpBar, DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlight);
	bool CalculateGateA(POSDIA_BARDATA *prpBar, DIAFLIGHTDATA *prpFlight);
	bool CalculateGateD(POSDIA_BARDATA *prpBar, DIAFLIGHTDATA *prpFlight);

	bool FlightIsOfbl(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);

// Attributes used for filtering condition
	int omGroupBy;			// enumerated value -- define in "stviewer.cpp"
	CStringArray omSortOrder;
	CTime omStartTime;
	CTime omEndTime;
// Attributes
	CWnd *pomAttachWnd;
	CBrush omBkBrush;
	CBrush omBkBrush2;
	CBrush omBreakBrush;
	CBrush omBkBreakBrush;
	CBrush omWIFBkBrush;
	int	igFirstGroupOnPage;

	BOOL bmIsFirstGroupOnPage;
 
 	CCSPrint *pomPrint;


// Methods which handle changes (from Data Distributor)
public:
	COLORREF GetTextColor(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);
	void SelectBar(const POSDIA_BARDATA *prpBar, bool bpHighLight = false);
	void DeSelectAll();
    CCSPtrArray<POSDIA_GROUPDATA> omGroups;
	void ProcessFlightInsert(CUIntArray *popRkeys);
	void ProcessFlightChange(DIAFLIGHTDATA *prpFlight);
	void ProcessFlightDelete(DIAFLIGHTDATA *prpFlight);
	void ProcessPstChange(RecordSet *prpRecord);
	void ProcessBlkChange( RecordSet * popBlkRecord );
	void ProcessPosBarUpdate(long *prpUrno,int ipDDXType);
	void ProcessKonfliktChange(DIAFLIGHTDATA *prpFlight);
    int GetLineCount(int ipGroupNo);
	int PosDiagramViewer::GetAbsoluteLine(int ipGroupNo, int ipLineNo);



};

/////////////////////////////////////////////////////////////////////////////

#endif
