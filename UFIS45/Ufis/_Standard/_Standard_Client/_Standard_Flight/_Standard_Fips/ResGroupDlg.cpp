// ResGroupDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <Utils.h>
#include <PrivList.h>

#include <resrc1.h>
#include <ResGroupDlg.h>
 
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ResGroupDlg dialog


ResGroupDlg::ResGroupDlg(CedaResGroupData *popResGroupData, CWnd* pParent /*=NULL*/)
	: CDialog(ResGroupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ResGroupDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
 
	pomParent = pParent;
	pomResGroupData = popResGroupData;
	ASSERT(pomResGroupData != NULL);

	bmGroupChanged = false;

}


ResGroupDlg::~ResGroupDlg()
{
 
}


void ResGroupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ResGroupDlg)
	DDX_Control(pDX, IDC_SAVEGROUP, m_CB_Save);
	DDX_Control(pDX, IDC_DELETEGROUP, m_CB_Delete);
	DDX_Control(pDX, IDC_RESTYPES, m_CB_ResTypes);
	DDX_Control(pDX, IDC_RESLIST, m_LB_ResList);
	DDX_Control(pDX, IDC_GROUPNAMES, m_CB_GroupNames);
	DDX_Control(pDX, IDC_GROUPRESLIST, m_LB_GroupResList);
	DDX_Control(pDX, IDC_CHECKHEIGHT, m_Checkheight);
 		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ResGroupDlg, CDialog)
	//{{AFX_MSG_MAP(ResGroupDlg)
	ON_WM_SIZE()
	ON_WM_CLOSE()
    ON_CBN_SELCHANGE(IDC_RESTYPES, OnResTypesChanged)
    ON_CBN_KILLFOCUS(IDC_GROUPNAMES, OnGroupChanged)
    ON_CBN_SELCHANGE(IDC_GROUPNAMES, OnGroupChanged)
    ON_BN_CLICKED(IDC_ADDITEM, OnAddItems)
    ON_BN_CLICKED(IDC_REMOVEITEM, OnRemoveItems)
    ON_BN_CLICKED(IDC_DELETEGROUP, OnDeleteGroup)
    ON_BN_CLICKED(IDC_ITEMUP, OnItemUp)
    ON_BN_CLICKED(IDC_ITEMDOWN, OnItemDown)
    ON_BN_CLICKED(IDC_SAVEGROUP, OnSaveGroup)
	ON_BN_CLICKED(IDC_CHECKHEIGHT, OnChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ResGroupDlg message handlers



bool ResGroupDlg::ResetDlgItems(void) 
{
	m_CB_ResTypes.SetCurSel(-1);
	m_LB_ResList.ResetContent();
	m_LB_GroupResList.ResetContent();


	m_CB_GroupNames.ResetContent();
	m_CB_GroupNames.SetCurSel(-1);

	return true;
}





BOOL ResGroupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowPos(pomParent, 40, 80,0,0, SWP_NOSIZE);

	m_CB_Save.SetSecState(ogPrivList.GetStat("RESGROUPDLG_CB_Save"));			
	m_CB_Delete.SetSecState(ogPrivList.GetStat("RESGROUPDLG_CB_Delete"));			
	m_Checkheight.SetSecState(ogPrivList.GetStat("RESGROUPDLG_CB_CheckHeight"));			


	// Build list of resource types
	m_CB_ResTypes.ResetContent();
	m_CB_ResTypes.AddString(GetString(IDS_STRING1220));
	m_CB_ResTypes.AddString(GetString(IDS_STRING1218));
	m_CB_ResTypes.AddString(GetString(IDS_STRING1219));
	m_CB_ResTypes.AddString(GetString(IDS_STRING1222));
	m_CB_ResTypes.AddString(GetString(IDS_STRING1012));
	if(bgChuteDisplay)
		m_CB_ResTypes.AddString(GetString(IDS_STRING2871));



	m_CB_GroupNames.LimitText(27);
 
	ResetDlgItems();

	m_Checkheight.SetCheck(0);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



afx_msg void ResGroupDlg::OnResTypesChanged() 
{
 
	// get resource name
 	CString olResType;
	CString olResFieldName;
	if (!GetSelectedRes(olResType, olResFieldName))
		return;

	// Fill the resource listbox with all names of the selected resource
	m_LB_ResList.ResetContent();

	if (olResType == "PST")
		ogBCD.ShowAllFields(olResType, olResFieldName, &m_LB_ResList, "", false);
	else
		ogBCD.ShowAllFields(olResType, CString("TERM,") + olResFieldName, &m_LB_ResList, "", true);

	// Reset group combobox
	m_CB_GroupNames.ResetContent();
	m_CB_GroupNames.SetCurSel(-1); 
 
 
	// Get all groups for the selected resource
	CStringArray olGroupList;
	if (!pomResGroupData->GetGroupList(olResType, olGroupList)) 
		return;
	// Fill the combobox with the group names
	for (int i = 0; i < olGroupList.GetSize(); i++)
	{
		m_CB_GroupNames.AddString(olGroupList[i]);
	}

	m_LB_GroupResList.ResetContent();

	m_Checkheight.SetCheck(0);

	bmGroupChanged = false;
}


afx_msg void ResGroupDlg::OnGroupChanged()
{
 	
	// get resource name
 	CString olResType;
	CString olResFieldName;
	if (!GetSelectedRes(olResType, olResFieldName))
		return;
	
	// get group name
	CString olGroupName;
	if (!GetSelectedGroupName(olGroupName))
		return;

	// get list of resources in group
	CPtrArray olUrnoList;
	if (!pomResGroupData->GetGroupResList(olResType, olGroupName, olUrnoList))
		return;

	// fills the resource group list box
	m_LB_GroupResList.ResetContent();
	CString olResName;
	CString olUrno;
	for (int i = 0; i < olUrnoList.GetSize(); i++)
	{
		olUrno.Format("%ld", (long) olUrnoList[i]);
		if (ogBCD.GetField(olResType, "URNO", olUrno, olResFieldName, olResName) == true)
		{
			CString olTerm ("");
			if (olResType != "PST")
			{
				ogBCD.GetField(olResType, "URNO", olUrno, "TERM", olTerm);
				if (olTerm.IsEmpty())
					olTerm += " ";

				olTerm += "  ";
				olResName = olTerm + olResName;
			}

			m_LB_GroupResList.AddString(olResName);
		}
	}

	// Fill the resource listbox with all names of the selected resource
	m_LB_ResList.ResetContent();
	if (olResType == "PST")
		ogBCD.ShowAllFields(olResType, olResFieldName, &m_LB_ResList, "", false);
	else
		ogBCD.ShowAllFields(olResType, CString("TERM,") + olResFieldName, &m_LB_ResList, "", true);
//	ogBCD.ShowAllFields(olResType, olResFieldName, &m_LB_ResList);

	// removes the resource item in the group from the resource list box
	RemoveDoubleItems();

	bool blFullHeight = false;
	if (!pomResGroupData->GetGroupResList(olResType, olGroupName, blFullHeight))
		return;

	m_Checkheight.SetCheck(!blFullHeight);

	bmGroupChanged = false;
}




bool ResGroupDlg::GetSelectedRes(CString &ropResType, CString &ropResFieldName) const
{
	int ilIndex = m_CB_ResTypes.GetCurSel();
 	switch(ilIndex)
	{
	case 0: 
		ropResType = "PST"; 
		ropResFieldName = "PNAM";
 		break;
	case 1: 
		ropResType = "GAT"; 
		ropResFieldName = "GNAM";
 		break;
	case 2: 
		ropResType = "BLT"; 
		ropResFieldName = "BNAM";
 		break;
	case 3: 
		ropResType = "WRO"; 
		ropResFieldName = "WNAM";
 		break;
	case 4: 
		ropResType = "CIC"; 
		ropResFieldName = "CNAM";
 		break;
	case 5: 
		ropResType = "CHU"; 
		ropResFieldName = "CNAM";
 		break;
	default:
		return false;
	}

	return true;

}



bool ResGroupDlg::GetSelectedGroupName(CString &ropGroupName) const
{
	// get group name
	int ilIndex = m_CB_GroupNames.GetCurSel();
	if (ilIndex != CB_ERR)
		// if no one is selected: get entered text
 		m_CB_GroupNames.GetLBText(ilIndex, ropGroupName);
	else	
		m_CB_GroupNames.GetWindowText(ropGroupName);

	return true;
}


bool ResGroupDlg::RemoveDoubleItems(void)
{
	CString olResName;
	int ilResListIndex;
	// scan all resource group list items
	for (int i = 0; i < m_LB_GroupResList.GetCount(); i++)
	{
		m_LB_GroupResList.GetText(i, olResName);
		// search item in resource list
		ilResListIndex = FindItem(m_LB_ResList, olResName);
		if (ilResListIndex > -1)
		{	
			// if found delete item in resource list
			m_LB_ResList.DeleteString(ilResListIndex);
		}
	}

	return true;
}



int ResGroupDlg::FindItem(const CListBox &ropListBox, const CString &ropItem) const
{
	CString olItem;
	// scan all list box items
	for (int i = 0; i < ropListBox.GetCount(); i++)
	{
		ropListBox.GetText(i, olItem);
		olItem.TrimRight();
		if (olItem == ropItem)  // item found
			return i;
	}
	// item not found
	return -1;
}



int ResGroupDlg::FindItem(const CComboBox &ropComboBox, const CString &ropItem) const
{
	CString olItem;
	// scan all combo box items
	for (int i = 0; i < ropComboBox.GetCount(); i++)
	{
		ropComboBox.GetLBText(i, olItem);
		if (olItem == ropItem)  // item found
			return i;
	}
	// item not found
	return -1;
}



afx_msg void ResGroupDlg::OnAddItems()
{
	// move the selected items from resource list to group resource list
	int i = 0;
	CString olItem;

	while (i < m_LB_ResList.GetCount())
	{
		if (m_LB_ResList.GetSel(i) != 0)
		{
			m_LB_ResList.GetText(i, olItem);
			m_LB_GroupResList.AddString(olItem);
			m_LB_ResList.DeleteString(i);

			bmGroupChanged = true;
		}
		else
			i++;
	}

}
	


afx_msg void ResGroupDlg::OnRemoveItems()
{
	// move the selected item from group resource list to resource list
	int i = 0;
	CString olItem;

	while (i < m_LB_GroupResList.GetCount())
	{
		if (m_LB_GroupResList.GetSel(i) != 0)
		{
			m_LB_GroupResList.GetText(i, olItem);
			m_LB_ResList.AddString(olItem);
			m_LB_GroupResList.DeleteString(i);

			bmGroupChanged = true;
		}
		else
			i++;
	}
 
}

afx_msg void ResGroupDlg::OnChange()
{
	bmGroupChanged = true;
}



afx_msg void ResGroupDlg::OnDeleteGroup()
{
	
	// get resource name
 	CString olResType;
	CString olResFieldName;
	if (!GetSelectedRes(olResType, olResFieldName))
		return;

	// get group name
	CString olGroupName;
	if (!GetSelectedGroupName(olGroupName))
		return;
 
	// delete group from database
	if (!pomResGroupData->DeleteGroup(olResType, olGroupName))
		return;

	bmGroupChanged = false;

	// delete group from combobox
	m_CB_GroupNames.SetCurSel(-1);
	int ilIndex = FindItem(m_CB_GroupNames, olGroupName);
	if (ilIndex > -1)
	{
		m_CB_GroupNames.DeleteString(ilIndex);
	}

	// reset resource list
	OnResTypesChanged();

	// display success message
	CString olCaption;
	GetWindowText(olCaption);
	MessageBox(GetString(IDS_STRING1979), olCaption, MB_OK | MB_ICONEXCLAMATION);
	

}



afx_msg void ResGroupDlg::OnItemUp()
{
	int ilIndex;
	if (MyGetSelItems(m_LB_GroupResList, 1, &ilIndex) == 0) 
		return;
	if (ilIndex == 0)
		return; // item is at the top

	CString olItem;
	m_LB_GroupResList.GetText(ilIndex, olItem);
	m_LB_GroupResList.DeleteString(ilIndex);
	m_LB_GroupResList.InsertString(ilIndex-1, olItem);
	m_LB_GroupResList.SetSel(ilIndex-1, TRUE);

	bmGroupChanged = true;
}



afx_msg void ResGroupDlg::OnItemDown()
{
	int ilIndex;
	if (MyGetSelItems(m_LB_GroupResList, 1, &ilIndex) == 0) 
		return;
	if (ilIndex == m_LB_GroupResList.GetCount()-1)
		return; // item is at the bottom

	CString olItem;
	m_LB_GroupResList.GetText(ilIndex, olItem);
	m_LB_GroupResList.DeleteString(ilIndex);
	m_LB_GroupResList.InsertString(ilIndex+1, olItem);
	m_LB_GroupResList.SetSel(ilIndex+1, TRUE);

	bmGroupChanged = true;

}



afx_msg void ResGroupDlg::OnSaveGroup(void)
{
	// get group name
	CString olGroupName;
	m_CB_GroupNames.GetWindowText(olGroupName);
 
	if (FindItem(m_CB_GroupNames, olGroupName) == -1)
	{
		bmGroupChanged = true;
		m_CB_GroupNames.AddString(olGroupName);
	}
	
	// only if any changes where made
	if (!bmGroupChanged)
	{
		CString olCaption;
		GetWindowText(olCaption);
		MessageBox(GetString(IDS_STRING1977), olCaption, MB_OK | MB_ICONEXCLAMATION);
		return;
	}
	
	// get resource name
 	CString olResType;
	CString olResFieldName;
	if (!GetSelectedRes(olResType, olResFieldName))
		return;
		
	// create urno array
	CPtrArray olUrnoArray;
	CString olItem;
	CString olResUrno;
	for (int i = 0; i < m_LB_GroupResList.GetCount(); i++)
	{
		m_LB_GroupResList.GetText(i, olItem);
		olItem.TrimRight();

		CString olName = olItem;
		int ilIndex = olItem.ReverseFind(' ');
		if (ilIndex > -1)
		{
			olName = olItem.Right(olItem.GetLength()-1-ilIndex);
		}

//		if (ogBCD.GetField(olResType, olResFieldName, olItem, "URNO", olResUrno) == true)
		if (ogBCD.GetField(olResType, olResFieldName, olName, "URNO", olResUrno) == true)
		{
			olUrnoArray.Add((void *) atol(olResUrno));
		}
	}
	
	//overlap for group
	bool blFullheight = false;
	if (m_Checkheight.GetCheck() == 1)
		blFullheight = true;
	
	if (olUrnoArray.GetSize() <= 0)
		return;

	// save group in database
	if (pomResGroupData->SaveGroup(olResType, olGroupName, olUrnoArray, blFullheight) == true)
	{
		bmGroupChanged = false;
		// display success message
		CString olCaption;
		GetWindowText(olCaption);
		MessageBox(GetString(IDS_STRING1978), olCaption, MB_OK | MB_ICONEXCLAMATION);
	}
 
}



void ResGroupDlg::OnOK() 
{	
	CDialog::OnOK();
}



void ResGroupDlg::OnClose() 
{
 	CDialog::OnClose();
}



 