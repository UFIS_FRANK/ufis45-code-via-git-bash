// CicAgentPeriod.cpp: implementation of the CCicAgentPeriod class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "fpms.h"
#include "CicAgentPeriod.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// 050315 MVy: containts an occupation period of a checkin counter regarding to a handling agent
CCicAgentPeriod::CCicAgentPeriod()
{
	m_lAgentUrno = 0 ;
	*Days = 0 ;
	m_lCounterUrno = 0 ;
};	// constructor

CCicAgentPeriod::CCicAgentPeriod( CTimeRange &range )
{
	m_lAgentUrno = 0 ;
	*Days = 0 ;
	m_lCounterUrno = 0 ;
	StartTime = range.StartTime ;
	EndTime = range.EndTime ;
};	// constructor

CCicAgentPeriod::~CCicAgentPeriod()
{
};	// destructor

void CCicAgentPeriod::SetPeriod( CTime start, CTime end )
{
	StartTime = start ;
	EndTime = end ;
};	// SetPeriod

void CCicAgentPeriod::SetPeriod( CTimeRange &range )
{
	StartTime = range.StartTime ;
	EndTime = range.EndTime ;
};	// SetPeriod

void CCicAgentPeriod::SetAgentUrno( long lUrno )
{
	m_lAgentUrno = lUrno ;
};	// SetAgentUrno

void CCicAgentPeriod::SetCounterUrno( long lUrno )
{
	m_lCounterUrno = lUrno ;
};	// SetCounterUrno

long CCicAgentPeriod::GetAgentUrno()
{
	return m_lAgentUrno ;
};	// GetAgentUrno

bool CCicAgentPeriod::IsAgentUrno( long lUrno )
{
	return m_lAgentUrno == lUrno ;
};	// IsAgentUrno

long CCicAgentPeriod::GetCounterUrno()
{
	return m_lCounterUrno ;
};	// GetCounterUrno

bool CCicAgentPeriod::IsCounterUrno( long lUrno )
{
	return m_lCounterUrno == lUrno ;
};	// IsCounterUrno

// set the text you see in the bar
void CCicAgentPeriod::SetTitle( CString sTitle )
{
	m_sBarTitle = sTitle ;
};	// SetTitle

CString* CCicAgentPeriod::GetTitle()
{
	return &m_sBarTitle ;
};

// give some infos for debugging
CString CCicAgentPeriod::Dump()
{
	CString s ;
	CTimeSpan span = EndTime - StartTime ;
	s.Format( "HAG#=%d, CIC#=%d, days=%s, from/to=%s <%d:%d> %s, Title=%s", 
		m_lAgentUrno, m_lCounterUrno, Days, 
		StartTime.Format( "%x,%X" ), 
		span.GetTotalHours(), span.GetMinutes(),
		EndTime.Format( "%x,%X" ), 
		m_sBarTitle
	);
	return s ;
};	// Dump

// store days 1..7 in sorted order as a string
void CCicAgentPeriod::SetDays( CString days )
{
	int len = days.GetLength();
	char c[2] ;		// atoi need a string but we have only a char
	c[1]=0 ;		// terminator for atoi
	for( int i = 0 ; i < __min( len, 8 ) ; i++ )
	{
		c[0]=days[i];
		unsigned char day = atoi( c ) -1 ;		// assuming 1..7, needing 0..6
		ASSERT( day > 0 && day <= 7 );		// check
		day &= 0x07 ;		// removing bad things
		Days[ day ] = days[i] ;		// now sorted
	};
	Days[i] = 0 ;		// need terminating zero if used as string
};	// SetDays
