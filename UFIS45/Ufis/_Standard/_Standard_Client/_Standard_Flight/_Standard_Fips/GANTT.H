// gantt.h : header file
//

#ifndef GANTT_H
#define GANTT_H

// This file just providing the symbols definition for customizing
// the behavior of all the GanttChart in the PEPPER project.
// This may be useful in case if some new features is not working any more,
// we still have a chance to disable those features (such as overlapping bars)
// without having to compile the whole project.

// Define this symbol to enable the overlapping bar features
#ifndef USE_OVERLAPPING_BAR
#define USE_OVERLAPPING_BAR
#endif

// Define this symbol to improve some efficiency of bar creation
//	Generally, raw data will be sorted from left to right.
//	So, scanning backward for the place of insertion is a little bit faster
//	(2.89 seconds vs. 4.21 seconds) for 3000 bars creation.
#ifndef SCANBACKWARD
#define SCANBACKWARD
#endif

#endif // GANTT_H

