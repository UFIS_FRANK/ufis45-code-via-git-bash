// ReasonForChange.cpp: implementation of the ReasonForChange class.

//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ReportReasonForChange.h"
#include <fpms.h>
#include <ReportSeqField3DateTimeDlg.h>
#include <CCSTime.h>
#include <CedaBasicData.h>
#include <CcsGlobl.h>
#include "CedaCraData.h"
#include "RotationDlgCedaFlightData.h"
#include <CedaBasicData.h>
#include <ReportSelectDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CReportReasonForChange::CReportReasonForChange(CWnd* popParent,const CString& ropHeadline, CTime* popMinDate, CTime *popMaxDate)
 	: CDialog(CReportReasonForChange::IDD, popParent)
{
	//{{AFX_DATA_INIT(ReportSeqField3DateTimeDlg)
	m_DateFrom = _T("");
	m_DateTo = _T("");
	m_TimeFrom = _T("");
	m_TimeTo = _T("");

	m_Positions = 0;
	m_Gates     = 0;
	m_Sort1     = 0;
	m_Sort2     = 0;
	//}}AFX_DATA_INIT

	omHeadline = ropHeadline;
	pomMinDate = popMinDate;
	pomMaxDate = popMaxDate;
}

CReportReasonForChange::~CReportReasonForChange()
{

}

void CReportReasonForChange::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportReasonForChange)
	DDX_Control(pDX, IDC_CHECK_POSITIONS, m_CB_Positions);
	DDX_Control(pDX, IDC_CHECK_GATES, m_CB_Gates);
	DDX_Control(pDX, IDC_RADIO_SORT1, m_CB_Sort1);
	DDX_Control(pDX, IDC_RADIO_SORT2, m_CB_Sort2);
	DDX_Control(pDX, IDC_EDIT_DATEFROM, m_CE_DateFrom);
	DDX_Control(pDX, IDC_EDIT_TIMEFROM, m_CE_TimeFrom);
	DDX_Control(pDX, IDC_EDIT_DATETO, m_CE_DateTo);
	DDX_Control(pDX, IDC_EDIT_TIMETO, m_CE_TimeTo);
 	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportReasonForChange, CDialog)
	//{{AFX_MSG_MAP(ReportSeqField3DateTimeDlg)	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CReportReasonForChange::OnInitDialog() 
{

	CDialog::OnInitDialog();
	CDialog::SetWindowText(omHeadline);	
	
	((CButton*)GetDlgItem(IDC_CHECK_POSITIONS))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_CHECK_GATES))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_RADIO_SORT1))->SetCheck(1);
	
	m_CE_DateFrom.SetTypeToDate();
	m_CE_DateTo.SetTypeToDate();
	m_CE_TimeFrom.SetTypeToTime();
	m_CE_TimeTo.SetTypeToTime();

	m_CE_DateFrom.SetBKColor(YELLOW);
	m_CE_DateTo.SetBKColor(YELLOW);
	m_CE_TimeFrom.SetBKColor(YELLOW);
	m_CE_TimeTo.SetBKColor(YELLOW);

	m_DateFrom = pomMinDate->Format("%d.%m.%Y");
	m_CE_DateFrom.SetWindowText( m_DateFrom );

	m_TimeFrom = CString("00:00");// pomMinDate->Format("%H:%M");
	m_CE_TimeFrom.SetWindowText( m_TimeFrom );

	m_DateTo = pomMaxDate->Format("%d.%m.%Y" );
	m_CE_DateTo.SetWindowText( m_DateTo );

	m_TimeTo = CString("23:59");;
	m_CE_TimeTo.SetWindowText( m_TimeTo );
 
	return TRUE;
}

void CReportReasonForChange::OnOK() 
{
	// check input fields
	m_Positions = ((CButton*)GetDlgItem(IDC_CHECK_POSITIONS))->GetCheck();
	m_Gates     = ((CButton*)GetDlgItem(IDC_CHECK_GATES))->GetCheck();
	m_Sort1     = ((CButton*)GetDlgItem(IDC_RADIO_SORT1))->GetCheck();
	m_Sort2     = ((CButton*)GetDlgItem(IDC_RADIO_SORT2))->GetCheck();

	if((m_Positions!= 1) && (m_Gates != 1))
	{
		MessageBox(GetString(IDS_STRING918), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		return;
	}
 	if((m_Sort1 != 1) && (m_Sort2 != 1))
	{
		MessageBox(GetString(IDS_STRING918), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		return;
	}	
	
	// Check date fields
	m_CE_DateFrom.GetWindowText(m_DateFrom);
	m_CE_DateTo.GetWindowText(m_DateTo);

	// if only one date given, it is copied to the other
 	if(m_DateFrom.IsEmpty())
		m_CE_DateTo.GetWindowText(m_DateFrom);
	if(m_DateTo.IsEmpty())
		m_CE_DateFrom.GetWindowText(m_DateTo);
	// set time fields
	m_CE_TimeFrom.GetWindowText(m_TimeFrom);
	if (m_TimeFrom.IsEmpty())
		m_TimeFrom="00:00";
	m_CE_TimeTo.GetWindowText(m_TimeTo);
	if (m_TimeTo.IsEmpty())
		m_TimeTo="23:59";

	
	if( (m_DateTo.IsEmpty()) || (m_DateFrom.IsEmpty()) )
	{
		// no date specified
		MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
		return;
	}
 
	if(!m_CE_DateFrom.GetStatus() || !m_CE_DateTo.GetStatus() ||
		!m_CE_TimeFrom.GetStatus() || !m_CE_TimeTo.GetStatus())
	{
		// wrong date / time format
		MessageBox(GetString(IDS_STRING934), GetString(IMFK_REPORT), MB_OK);
		return;
	}

	*pomMinDate = DateHourStringToDate(m_DateFrom, m_TimeFrom);
	*pomMaxDate = DateHourStringToDate(m_DateTo, m_TimeTo); 

	if (*pomMinDate > *pomMaxDate)
	{
		// To-Time before From-Time
		MessageBox(GetString(IDS_TIMERANGE), GetString(IMFK_REPORT), MB_OK);
		return;
	}
 
	CDialog::OnOK(); 
}

void CReportReasonForChange::OnCancel() 
{	
	CDialog::OnCancel();
}

void CReportReasonForChange::SetFieldType() 
{
	//ropField.SetTypeToString("X|#X|#X|#",3,2);
}

bool CReportReasonForChange::ReadReasonForChange(CProgressCtrl* pop_PR_Read,
												 const CString& ropMinStr,
												 const CString& ropMaxStr)
{	
	bool blReasonSorChange = true;
	
	char pspWhere[1024];
	sprintf(pspWhere,"WHERE CDAT >= '%s' AND CDAT <= '%s'",ropMinStr,ropMaxStr);

	CCSPtrArray<CRADATA> olCraDataArray;
	ogCraData.ReadSpecial(olCraDataArray,pspWhere);
	CRADATA olCraData;
	ROTATIONDLGFLIGHTDATA* polRotationData = NULL;
	REASON_FOR_CHANGE olReasonForChange;
	
	CString olCode;
	CString olRema;

	for(int i = 0 ; i < olCraDataArray.GetSize() ; i++)
	{
		olCraData = olCraDataArray.GetAt(i);	
		
		if((m_Positions == 1 && m_Gates == 0)
			&& ((strstr(olCraData.Aloc,"GTA1") != NULL) || (strstr(olCraData.Aloc,"GTA2") != NULL)
			|| (strstr(olCraData.Aloc,"GTD1") != NULL) ||  (strstr(olCraData.Aloc,"GTD2") != NULL)))
		{
			continue;
		}		
		else if((m_Positions == 0 && m_Gates == 1)
			&& ((strstr(olCraData.Aloc,"PSTA") != NULL) || (strstr(olCraData.Aloc,"PSTD") != NULL)))	
		{
			continue;
		}


		char pclSelectionArr[256];
		sprintf(pclSelectionArr, " URNO = %ld", olCraData.Furn);
		polRotationData = NULL;

		ogRotationDlgFlights.ReadFlights(pclSelectionArr);
		polRotationData = ogRotationDlgFlights.GetFlightByUrno(olCraData.Furn);
		if(polRotationData == NULL)
		{
			continue;
		}

		char pclSelection[20];
		sprintf(pclSelection, "%ld", olCraData.Curn);

		ogBCD.GetField("CRC", "URNO", CString(pclSelection), "CODE", olCode);
		ogBCD.GetField("CRC", "URNO", CString(pclSelection), "REMA", olRema);
		
		if(olCode.IsEmpty())
		{
			continue;
		}
		olReasonForChange.Urno = olCraData.Urno;
		olReasonForChange.Furn = olCraData.Furn;
		strcpy(olReasonForChange.Flno,polRotationData->Flno);
		strcpy(olReasonForChange.Ftyp,polRotationData->Adid);
		if(polRotationData->Adid[0] == 'A')
		{
			olReasonForChange.Scdt = polRotationData->Stoa;
		}
		else if(polRotationData->Adid[0] == 'D')
		{
			olReasonForChange.Scdt = polRotationData->Stod;
		}
				
		strcpy(olReasonForChange.Aloc,olCraData.Aloc);
		olReasonForChange.Cdat = olCraData.Cdat;
		strcpy(olReasonForChange.OVal,olCraData.Oval);
		strcpy(olReasonForChange.NVal,olCraData.Nval);
		strcpy(olReasonForChange.Code,olCode);
		strcpy(olReasonForChange.Rema,olRema);
		strcpy(olReasonForChange.Usec,olCraData.Usec);

		omReasonForChangeData.NewAt(omReasonForChangeData.GetSize(),olReasonForChange);
		pop_PR_Read->StepIt();

	}

	if(omReasonForChangeData.GetSize() == 0)
	{
		blReasonSorChange = false;
	}
	return blReasonSorChange;
}
