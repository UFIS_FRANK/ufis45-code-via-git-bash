#if !defined(AFX_HAIDLG_H__89A8CE34_48BB_11D3_AB92_00001C019D0B__INCLUDED_)
#define AFX_HAIDLG_H__89A8CE34_48BB_11D3_AB92_00001C019D0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HAIDlg.h : header file
//

#include <CCSEdit.h>
#include <resrc1.h>

/////////////////////////////////////////////////////////////////////////////
// HAIDlg dialog

class HAIDlg : public CDialog
{
// Construction
public:
	HAIDlg(CWnd* pParent = NULL, char opType = ' ', char opMode = ' ', CString opUrno = "-1");
	~HAIDlg();

	void SetAftTabUrnoA(CString opUrno);
	void SetAftTabUrnoD(CString opUrno);

	void InitDialog();

// Dialog Data
	//{{AFX_DATA(HAIDlg)
	enum { IDD = IDD_HAI };
	CButton	m_OK;
	CCSEdit	m_CdatD;
	CCSEdit	m_CdatT;
	CCSEdit	m_LstuD;
	CCSEdit	m_LstuT;
	CCSEdit	m_Usec;
	CCSEdit	m_Useu;
	CCSEdit	m_HSNA;
	CCSEdit	m_TASK;
	CCSEdit	m_REMA;
	BOOL    m_CH_Arrival;
	BOOL    m_CH_Departure;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(HAIDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(HAIDlg)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnChArrival();
	afx_msg void OnChDeparture();
	afx_msg void OnHsnalist();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CString omUrno; 
	CString omUrnoA;
	CString omUrnoD;
	char    omMode;
	char    omType;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HAIDLG_H__89A8CE34_48BB_11D3_AB92_00001C019D0B__INCLUDED_)
