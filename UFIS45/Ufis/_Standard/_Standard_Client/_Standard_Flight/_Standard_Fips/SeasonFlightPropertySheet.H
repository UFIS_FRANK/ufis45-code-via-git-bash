// flplanps.h : header file
//



#ifndef _SEASONFLIGHTTABLEPROPERTYSHEET_H_
#define _SEASONFLIGHTTABLEPROPERTYSHEET_H_


#include <BasePropertySheet.h>
#include <PSUniFilterPage.h>
#include <PSSearchFlightPage.h>
#include <PSunisortpage.h>
#include <PSSortFlightPage.h>
#include <PSspecfilterpage.h>

//#include "TestPage.h"

/////////////////////////////////////////////////////////////////////////////
// SeasonFlightTablePropertySheet

class SeasonFlightTablePropertySheet : public BasePropertySheet
{
// Construction
public:
	SeasonFlightTablePropertySheet(CString opCalledFrom, CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0,  LPCSTR pszCaption = NULL);
	~SeasonFlightTablePropertySheet();

// Attributes
public:
	//TestPage m_TestPage;
	CPsUniFilter m_PSUniFilter;
	CSpecFilterPage m_SpecialFilterPage;
	CSearchFlightPage m_SearchFlightPage;
	CPSUniSortPage m_PSUniSortPage;
	CSortFlightPage m_SpecialSortPage;
/*	FilterPage m_pageAirline;
	FilterPage m_pageArrival;
	FilterPage m_pageDeparture;
	FlightTableBoundFilterPage m_pageBound;
	FlightTableSortPage m_pageSort;
	ZeitPage m_pageZeit;
*/
// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();


};

/////////////////////////////////////////////////////////////////////////////

#endif // _SEASONFLIGHTTABLEPROPERTYSHEET_H_
