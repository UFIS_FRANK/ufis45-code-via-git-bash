// CedaChaData.h

#ifndef __CEDACHADATA__
#define __CEDACHADATA__
 
#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <CCSCedadata.h>
#include <CCSddx.h>

#include "TimeRange.h"		// 050317 MVy: needed for CHADATA

//---------------------------------------------------------------------------------------------------------

struct CCAFLIGHTDATA;
struct COLLECTFLIGHTDATA;


struct CHADATA 
{
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	long 	 Rurn; 	// Eindeutige Datensatz-Nr. des zugeteilten Fluges
	CTime 	 Cdat; 		// Erstellungsdatum
	CTime 	 Lstu; 		// Datum letzte �nderung
	char 	 Usec[33]; 	// Anwender (Ersteller)
	char 	 Useu[33]; 	// Anwender (letzte �nderung)
	char 	 Lnam[6]; 	// Chute name
	char	Des3[4];		// 3-Char Destination Code of Chute, can also be @@@ for 'All Destinations'
	char	Des4[5];		// 4-Char Destination Code of Chute, can also be @@@ for 'All Destinations', curently not filled
	char	Fcla[2];		// Baggage Class Code Information, Class Code is defined in FIDTAB (REMT = H)
	char	Ftyp[2];		// Flight Type (N,O,X)
	char	Indi[2];		// F (First), N (Next) or L (Last Telegram for a Flight)
	char	Lafx[2];		// New: Baggage Transit Code, Transit Code is defined in FIDTAB (REMT = T)
	CTime	Stob;			// Scheduled Time Begin
	CTime	Stoe;			// Scheduled Time End
	CTime	Tifb;			// Best Time Begin
	CTime	Tife;			// Best Time End
	char	Tisb[2];		// Status of Best Time Begin
	char	Tise[2];		// Status of Best Time End
	char	Ctyp[2];			// Blank = Chute; N = Blocked
	char	Rema[32];			// Blank = Chute; N = Blocked

	
	long	 KKey;	
	bool	 Valid;
	int 	 NotValidBrush;


	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	bool	 IsSelected;


	int NextC;
	int PrevC;
	int Freq;


	CHADATA()
	{ 
		memset(this,'\0',sizeof(*this));
		IsSelected  =   false;
		Valid		=	false;
		Urno		=   0;
		Rurn		=   0;
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
		Stob		=	TIMENULL;
		Stoe		=	TIMENULL;
		Tifb		=	TIMENULL;
		Tife		=	TIMENULL;
		
		
		NextC = 0;
		PrevC = 0;

		NotValidBrush = -1;
	}

public:
	bool IsOpen();
	bool UpdateFIDSMonitor();
	
	const CTime GetBestStartAlloc( bool bUtcToLocal = false );
	const CTime GetBestEndAlloc( bool bUtcToLocal = false );
	CTimeRange GetAllocation( bool bUtcToLocal = false );		// 050317 MVy: return actual times if exist, otherwise return planned time
	bool IsFlightOfAirline( CStringArray& strarrAirlineCodes );		// 050323 MVy: return whether this flight belongs to one of the specified airlines
	bool IsFlight();		// 050324 MVy: helper function for check
	bool IsCommon();		// 050324 MVy: helper function for check
	bool IsPreCheck();		// 050324 MVy: helper function for check
	bool IsBlocking();		// 050324 MVy: helper function for check

}; // end CHADATAStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaChaData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omLnamMap;

    CCSPtrArray<CHADATA> omData;
	CCSPtrArray<CHADATA>  omDelData;

 	char pcmListOfFields[2048];
	CString omSelection;
	bool bmSelection;

	CTime omFrom;
	CTime omTo;


	CMapPtrToPtr omRurnMap;
 
	bool bmNew ;

	bool bmOffLine;
	CString omFlnuList; 


	const void *pomParent;

// Operations
public:
    CedaChaData();
	~CedaChaData();

	bool SetParent(const void *popParent);

	
	void AddToKeyMap(CHADATA *prpCha);
	void DelFromKeyMap(CHADATA *prpCha); 
	
//	bool CkeckOverlapping(CString opCnam, CTime opStart, CTime opEnd);
	bool CkeckOverlapping(CString opCnam, CTime opStart, CTime opEnd, long lpRurn=0);

	void Register(void);
	void UnRegister(void);
	void ClearAll(bool bpWithRegistration = true);
	bool ReadAll(CString &opUrnos, CTime opFrom, CTime opTo, CMapStringToString& opFilterMap, bool bpClearAll = true, bool bpOnlyFlight = false);

	bool GetChasByRurn(CCSPtrArray<CHADATA> &ropChaList, long lpRurn);
	bool HasChasByRurn(long lpRurn);
	bool HasDuplicate(CHADATA *prpChaDup, bool bpDdx = false);


	bool GetChasByLnam(CCSPtrArray<CHADATA> &ropChaList, CString opLnam);
	void GetChaArray(CCSPtrArray<CHADATA> &opCha, CString opTyp, CString opCnam);

	bool GetChaData(long lpRurn, CString &opCha, CTime &opStart, CTime &opEnd);    

	bool GetChaArray(long lpRurn, CCSPtrArray<CHADATA> &opData);

	bool GetMinMaxLnam(long lpRurn, CString &opMin, CString &opMax);
	bool GetAllLnam(long lpRurn, CString &opMin, CString &opMax, CString &opCompleteList);

	bool GetMinMaxLnamAndTime(long lpRurn, CString &opMin, CString &opMax, CTime &opStart, CTime &opEnd);

	bool IsPassFilter(CHADATA *prpCha);

	void SetFilter(const CString &ropFlnuList);


	bool GetSel(CUIntArray &opUrnos);


	bool Read(char *pspWhere = NULL, bool bpClearAll = true, bool bpDdx = false);
    bool ReadInsert(char *pspWhere = NULL);
	bool InsertInternal(CHADATA *prpCha, bool bpDdx = true);
	bool Update(CHADATA *prpCha);
	bool UpdateInternal(CHADATA *prpCha, bool bpDdx = true, bool bpSaveStat = false);
	bool DeleteInternal(CHADATA *prpCha, bool bpDdx = true);
	bool ReadSpecial(CCSPtrArray<CHADATA> &ropAct,char *pspWhere);
	bool ExistLnam(CHADATA *prpCha);
	bool ExistSpecial(char *pspWhere);

	//Save Single Row
	bool Save(CHADATA *prpCha);
	void SaveInternal(CCSPtrArray<CHADATA> *popChaList, bool bpDdx =true);
	//Save multi
	void Release(CCSPtrArray<CHADATA> *popChaList = NULL, bool bpDdx = true);

	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void ReloadChaSBC(void *vpDataPointer);


	CHADATA  *GetChaByUrno(long lpUrno);
	bool UpdateSpecial(CCSPtrArray<CHADATA> &ropCha, CCSPtrArray<CHADATA> &ropChaSave, long lpFlightUrno);
	bool DeleteSpecial(CString olFlightUrnoList);
	void SetPreSelection(CTime &opFrom, CTime &opTo, CString &opFtyps);
	bool ReadChas(char *pcpSelection = NULL, bool bpIgnoreViewSel = false);
	bool AddChaInternal(CHADATA *prpCCA);
	CString omFtyps;
	CString omRurns;

	void SetTableName(CString opTab)
	{
		strcpy(pcmTableName, opTab.GetBuffer(0));
	}
	
	// hbe 11/12/98: um gew�nschten Verkehrstag erweitert; def.: => heute
    // hbe 5/99: um Object erweitert BLK / REPBLK 
	CString MakeBlkData( CTime opTrafficDay = TIMENULL , int ipDOO = -1, CString opObject="BLK");
	CString MakeBlkData(RecordSet *popBlkRecord , int ipDOO = -1 );

	CString  GetDataList(CHADATA *prpCha);


	bool AllHaveActOpenTime(const CUIntArray &ropUrnos);


	// Private methods
private:
 	CMapStringToString omFilterMap;

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDACHADATA__
