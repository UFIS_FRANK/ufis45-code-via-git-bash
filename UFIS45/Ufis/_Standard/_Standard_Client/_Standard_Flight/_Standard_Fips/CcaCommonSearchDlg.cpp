// CcaCommonSearchDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
//#include "fpms.h"
#include <CcaCommonSearchDlg.h>
#include <CedaBasicData.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#define ST_NOTFORMAT	GetString(IDS_STRING1039)

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CCcaCommonSearchDlg 


CCcaCommonSearchDlg::CCcaCommonSearchDlg(CWnd* pParent, char *opSelect, CTime *popFrom, CTime *popTo)
	: CDialog(CCcaCommonSearchDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCcaCommonSearchDlg)
	m_Beginn_d = _T("");
	m_Beginn_t = _T("");
	m_Ende_d = _T("");
	m_Ende_t = _T("");
	m_Fluggs = _T("");
	m_Schalter = _T("");
	m_Terminal = _T("");
	//}}AFX_DATA_INIT
	pcmSelect = opSelect;
	pomTo = popTo;
	pomFrom = popFrom;
}



void CCcaCommonSearchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCcaCommonSearchDlg)
	DDX_Control(pDX, IDC_TERMINAL, m_CE_Terminal);
	DDX_Control(pDX, IDC_SCHALTER, m_CE_Schalter);
	DDX_Control(pDX, IDC_FLUGGS, m_CE_Fluggs);
	DDX_Control(pDX, IDC_ENDE_T, m_CE_Ende_t);
	DDX_Control(pDX, IDC_ENDE_D, m_CE_Ende_d);
	DDX_Control(pDX, IDC_BEGINN_T, m_CE_Beginn_t);
	DDX_Control(pDX, IDC_BEGINN_D, m_CE_Beginn_d);
	DDX_Text(pDX, IDC_BEGINN_D, m_Beginn_d);
	DDX_Text(pDX, IDC_BEGINN_T, m_Beginn_t);
	DDX_Text(pDX, IDC_ENDE_D, m_Ende_d);
	DDX_Text(pDX, IDC_ENDE_T, m_Ende_t);
	DDX_Text(pDX, IDC_FLUGGS, m_Fluggs);
	DDX_Text(pDX, IDC_SCHALTER, m_Schalter);
	DDX_Text(pDX, IDC_TERMINAL, m_Terminal);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCcaCommonSearchDlg, CDialog)
	//{{AFX_MSG_MAP(CCcaCommonSearchDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CCcaCommonSearchDlg 

BOOL CCcaCommonSearchDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_CE_Schalter.SetTypeToString("X(5)",5,0);

	m_CE_Terminal.SetTypeToString("X",1,0);;
	
	m_CE_Fluggs.SetTypeToString("X(3)",3,0);

	m_CE_Beginn_d.SetTypeToDate();
	
	m_CE_Beginn_t.SetTypeToTime();
	
	m_CE_Ende_d.SetTypeToDate();
	
	m_CE_Ende_t.SetTypeToTime();


	if(pomFrom != NULL)
	{
		if(*pomFrom != TIMENULL)
		{
			m_CE_Beginn_d.SetInitText(pomFrom->Format("%d.%m.%Y"));
			m_CE_Beginn_t.SetInitText(pomFrom->Format("%H:%M"));
		}
	}

	if(pomTo != NULL)
	{
		if(*pomTo != TIMENULL)
		{
			m_CE_Ende_d.SetInitText(pomTo->Format("%d.%m.%Y"));
			m_CE_Ende_t.SetInitText(pomTo->Format("%H:%M"));
		}
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CCcaCommonSearchDlg::OnOK() 
{
	CString olErrorText;
	CString olNichtFormat(ST_NOTFORMAT);
	CString ropWhere;
	CTime   olMinDate;
	CTime   olMaxDate;

	if(m_CE_Schalter.GetStatus() == false)
	{
		olErrorText += GetString(IDS_STRING1364) + CString(" \n");
	}
	else
	{
		m_CE_Schalter.GetWindowText(m_Schalter);
		if(!m_Schalter.IsEmpty())
			ropWhere += " AND CKIC='" + m_Schalter + CString("'");
	}

	if(m_CE_Terminal.GetStatus() == false)
	{
		olErrorText += GetString(IDS_STRING1521) + CString(" \n") ;
	}
	else
	{
		m_CE_Terminal.GetWindowText(m_Terminal);
		if(!m_Terminal.IsEmpty())
			ropWhere += " AND CKIT='" + m_Terminal + CString("'");
	}

	if(m_CE_Fluggs.GetStatus() == false)
	{
		olErrorText += GetString(IDS_STRING1040) + CString(" \n") ;
	}
	else
	{
		CString olFlugges;
		m_CE_Fluggs.GetWindowText(m_Fluggs);

		if(!m_Fluggs.IsEmpty())
		{
			bool blRet = ogBCD.GetField("ALT", "ALC2", m_Fluggs, "URNO", olFlugges);
			if(blRet == false)
				blRet = ogBCD.GetField("ALT", "ALC3", m_Fluggs, "URNO", olFlugges);
			ropWhere += " AND FLNU=" + olFlugges;	// + CString(" ");
		}
	}

	if(!m_CE_Beginn_d.GetStatus())
		olErrorText += GetString(IDS_STRING1522) + CString(" \n");
	
	if(!m_CE_Beginn_t.GetStatus())
		olErrorText += GetString(IDS_STRING1523) + CString(" \n");
	
	if(!m_CE_Ende_d.GetStatus())
		olErrorText += GetString(IDS_STRING1524) + CString(" \n");

	if(!m_CE_Ende_t.GetStatus())
		olErrorText += GetString(IDS_STRING1525) + CString(" \n");

	
	if(!olErrorText.IsEmpty())	
	{

		olErrorText= GetString(IDS_STRING1526)  + olErrorText;

		MessageBox(olErrorText,GetString(ST_FEHLER),MB_ICONEXCLAMATION);
		return;
	}
	

	

	m_CE_Beginn_d.GetWindowText(m_Beginn_d);
	m_CE_Beginn_t.GetWindowText(m_Beginn_t);
	m_CE_Ende_d.GetWindowText(m_Ende_d);
	m_CE_Ende_t.GetWindowText(m_Ende_t);
	

	if(!m_Beginn_d.IsEmpty())
	{
		if(m_Ende_d.IsEmpty())
		{
			m_Ende_d = m_Beginn_d;
		}

		if(m_Beginn_t.IsEmpty())
		{
			m_Beginn_t = CString("00:00");
		}

		if(m_Ende_t.IsEmpty())
		{
			m_Ende_t = CString("23:59");
		}
	}



	olMinDate = DateHourStringToDate(m_Beginn_d,m_Beginn_t);
	ogBasicData.LocalToUtc(olMinDate);

	olMaxDate = DateHourStringToDate(m_Ende_d,m_Ende_t);
	ogBasicData.LocalToUtc(olMaxDate);



	if((olMaxDate != TIMENULL) && (olMinDate != TIMENULL) )
	{
		ropWhere += " AND CKBS <= '" + olMaxDate.Format( "%Y%m%d%H%M%S" ) + CString("'") + CString(" AND ") + "CKES >= '" + olMinDate.Format( "%Y%m%d%H%M%S" ) + CString("'");
	
		if(olMinDate > olMaxDate)
		{
			olErrorText += GetString(IDS_STRING1518);
		}
	}

	if(!olErrorText.IsEmpty())	
	{
		MessageBox(olErrorText,GetString(ST_FEHLER),MB_ICONEXCLAMATION);
		return;
	}

	if(ropWhere.IsEmpty())
	{
		if(MessageBox(GetString(IDS_STRING1527), GetString(IDS_STRING908),MB_YESNO) == IDCANCEL)
			return;
	}

	*pomFrom = olMinDate;
	*pomTo = olMaxDate;

	ogBasicData.UtcToLocal(*pomFrom);
	ogBasicData.UtcToLocal(*pomTo);


	ropWhere = CString("(CTYP='C' OR CTYP='P') ") + ropWhere;

	strcpy(pcmSelect, ropWhere);
	
	CDialog::OnOK();

}
