#ifndef AFX_CHECKINDLG_H__183C77C1_B4D6_11D1_8154_0000B43C4B01__INCLUDED_
#define AFX_CHECKINDLG_H__183C77C1_B4D6_11D1_8154_0000B43C4B01__INCLUDED_

// CheckInDlg.h : Header-Datei
//
#include <RotationDlgCedaFlightData.h>
#include <CcaCommonTableViewer.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CCcaCommonTableDlg 

class CCcaCommonTableDlg : public CDialog
{
// Konstruktion
public:
	CCcaCommonTableDlg(CWnd* pParent = NULL, CTime opFrom  = TIMENULL, CTime opTo = TIMENULL);   // Standardkonstruktor
	~CCcaCommonTableDlg();

	void SetTimeFrame(CTime opFrom  = TIMENULL, CTime opTo = TIMENULL);



	void Reload();
	bool isCreated;
	void SaveToReg();

// Dialogfelddaten
	//{{AFX_DATA(CCcaCommonTableDlg)
	enum { IDD = IDD_CCA_COMMON_TABLEDLG };
	CButton	m_CB_Kopieren;
	CButton	m_CB_Entf;
	CButton	m_CB_Einfuegen;
	CComboBox	m_CC_Ansicht;
	CButton	m_CB_Ansicht;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CCcaCommonTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CCcaCommonTableDlg)
	afx_msg void OnAendern();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnEinfuegen();
	afx_msg void OnEntf();
	afx_msg void OnKopieren();
	afx_msg void OnButtonAnsicht();
	afx_msg void OnSelchangeComboAnsicht();
	afx_msg void OnClose();
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void ShowAnsicht();
	void UpdateComboAnsicht();
	CCSTable *pomCheckInTable;
	CCSPtrArray<CCADATA> *pomData;
	CcaCommonTableViewer	*pomCheckInViewer;
	//CedaCcaData omCcaData;

	CString omSelection;

	CTime omFrom;
	CTime omTo;

	bool bgIsDialogOpen;
	CString m_key; 
	int imDialogHeight;

public:
    CMapPtrToPtr omUrnoMap;
	void UpdateView();

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_CHECKINDLG_H__183C77C1_B4D6_11D1_8154_0000B43C4B01__INCLUDED_
