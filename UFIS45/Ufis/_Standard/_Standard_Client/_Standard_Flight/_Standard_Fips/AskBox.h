#if !defined(AFX_ASKBOX_H__130FC741_8F41_11D2_860F_0000C04D916B__INCLUDED_)
#define AFX_ASKBOX_H__130FC741_8F41_11D2_860F_0000C04D916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// AskBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// AskBox dialog

class AskBox : public CDialog
{
// Construction
public:
	AskBox(CWnd* pParent, const CString &ropCaption, const CString &ropMessage, const CString &ropButton1Text, CString opButton2Text = "", CString opButton3Text = "", CString opCancelText = "Cancel");

	void EnableButtons(bool bpButton1 = true, bool bpButton2 = true, bool bpButton3 = true);
protected:
	CString omButtonText1;
	CString omButtonText2;
	CString omButtonText3;
	CString omCancelText;

	bool bmButton1Enable;
	bool bmButton2Enable;
	bool bmButton3Enable;

	CString omCaption;
	CString omMessage;

public:
	bool bmJoinFlag;

// Dialog Data
	//{{AFX_DATA(AskBox)
	enum { IDD = IDD_ASKBOX };
	CButton	m_CB_Cancel;
	CButton	m_CB_Button1;
	CButton	m_CB_Button2;
	CButton	m_CB_Button3;
	CStatic	m_CS_Text;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AskBox)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	int GetMessageWidth(CDC *popDC) const;

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AskBox)
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ASKBOX_H__130FC741_8F41_11D2_860F_0000C04D916B__INCLUDED_)
