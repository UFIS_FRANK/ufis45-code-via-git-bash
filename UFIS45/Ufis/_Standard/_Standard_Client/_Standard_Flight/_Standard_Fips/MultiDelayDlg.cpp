// MultiDelayDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <RotationDlgCedaFlightData.h>
#include <MultiDelayDlg.h>
#include <resrc1.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <AwBasicDataDlg.h>
#include <Utils.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MultiDelayDlg dialog

//**************************************************
//MWO: Color definitions to be used in TABOCX
#define colBlack COLORREF(RGB(0,0,0))
#define colYellow COLORREF(RGB(255,255,0))
#define colRed COLORREF(RGB(255,0,0))
#define colGreen COLORREF(RGB(128,255,128))
#define colWhite COLORREF(RGB(255,255,255))
#define colBlue COLORREF(RGB(0,0,255))
//END MWO: Color definitions to be used in TABOCX
//**************************************************
CString myDeleyCodeField;

MultiDelayDlg::MultiDelayDlg(CWnd* pParent, ROTATIONDLGFLIGHTDATA *prpFlight, CTAB *prpSourceTab)
	: CDialog(MultiDelayDlg::IDD, pParent)
{
	prmFlight = prpFlight;
	prmTabSrc = prpSourceTab;
	isInit = false;
    CDialog::Create(MultiDelayDlg::IDD, pParent);

	myDeleyCodeField = CString("");
	//Read ceda.ini to identify which delay code field has to be used
	//the alphabetical or the numerical one.
	char pclConfigPath[256];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));
	char pclDelayField[256];
	GetPrivateProfileString("FIPS", "SHOW_DELAYCODE", "FIPS",pclDelayField, sizeof pclDelayField, pclConfigPath);
	if(strcmp(pclDelayField,"ALPHABETICAL") == 0)
	{
		myDeleyCodeField = "DECA";
	}
	else
	{
		myDeleyCodeField = "DECN";
	}

	bmOpen = true;

}


void MultiDelayDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(MultiDelayDlg)
	DDX_Control(pDX, IDC_HELPTEXT, m_HelpText);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDCANCEL, m_Cancel);
	DDX_Control(pDX, IDC_NEW, m_New);
	DDX_Control(pDX, IDC_DELETE, m_Delete);
	DDX_Control(pDX, IDC_TAB, myTab);
	DDX_Control(pDX, IDC_UFISCOM, myUfisCom);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(MultiDelayDlg, CDialog)
	//{{AFX_MSG_MAP(MultiDelayDlg)
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(ID_SAVE, OnSave)
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MultiDelayDlg message handlers

void MultiDelayDlg::OnNew() 
{
	myTab.InsertTextLine(GetEmptyLine(), true);
	myTab.SetLineColor(myTab.GetLineCount()-1, colBlack, colYellow);
	myTab.OnVScrollTo(myTab.GetLineCount());
}

void MultiDelayDlg::OnDelete() 
{
	HandleDelete();
}
//****************************************************************
//MWO: Iterate through the tab and collect the new, the
//     udpated and deleted rows. Reset the line colors
//     to white backcolor and renew the linetag to be
//     prepared for the next updates. The rows will
//     be stored based on singel events. Perhaps for 
//     the future it will be necessary to store them
//     by calling the RELHDL and send a sing SBC broadcast
//     after finishing the database actions.
//****************************************************************
void MultiDelayDlg::OnSave() 
{
	CTime olTime = CTime::GetCurrentTime();
	CString newLstu = olTime.Format("%Y%m%d%H%M%S");
	CString newCdat = olTime.Format("%Y%m%d%H%M%S");
	CString newUsec = ogCommHandler.pcmUser;
	CString newUseu = ogCommHandler.pcmUser;
	CString strWhere = "";
	CString strError = "";
	CString strFielddList = "URNO,FURN,DURN,READ,REMA,DURA,CDAT,LSTU,USEC,USEU";

	long i = 0;
	for( i = 0; i < myTab.GetLineCount(); i++)
	{
		CString strData = myTab.GetFieldValues(i, "URNO,FURN,DURN,READ,REMA,DURA") + CString(",");
		CString oldCdat = myTab.GetFieldValues(i, "CDAT");
		CString strCode = myTab.GetFieldValues(i, "DURN");
		CString strDura = myTab.GetFieldValues(i, "DURA");
		if(strDura != "" || strCode != "") //Otherwise don't save this record because it will be emtpy
		{
			//if(oldLstu != "") //Then we must set the lstu/useu and not the cdat/usec
			{
				strData += myTab.GetFieldValues(i, "CDAT") + CString(",");
				strData += newLstu + CString(",");
				strData += myTab.GetFieldValues(i, "USEC") + CString(",");
				strData += newUseu;
				myTab.SetFieldValues(i, "USEU,LSTU", newUseu + CString(",") + newLstu);
			}
			if(oldCdat == "") //Then we only have to append the cdat/usec field for a new record
			{
				strData += myTab.GetFieldValues(i, "CDAT") + CString(",");
				strData += CString(",");
				strData += myTab.GetFieldValues(i, "USEC") + CString(",");
			}
			//In Tab: URNO,FURN,DURN,CODE,READ,REMA,DURA,CDAT,LSTU,USEC,USEU
			//Fieldlist URNO,FURN,DURN,CODE,READ,REMA,DURA,CDAT,LSTU,USEC,USEU
			long bCol = -1;
			long tCol = -1;
			myTab.GetLineColor(i, &tCol, &bCol);
			if(bCol == colYellow)
			{
				if(myUfisCom.CallServer ( "IRT", "DCFTAB", strFielddList, strData, "", "230") != 0)
					strError += myUfisCom.GetLastErrorMessage() + CString("\n");
				myTab.SetLineColor(i, colBlack, colWhite);
				myTab.SetLineTag(i, myTab.GetFieldValues(i, "URNO,FURN,DURN,CODE,READ,REMA,DURA,CDAT,LSTU,USEC,USEU"));
			}
			else if(bCol == colGreen)
			{
				strWhere = "WHERE URNO='" + myTab.GetFieldValue(i, "URNO") + "'";
				if(myUfisCom.CallServer ( "URT", "DCFTAB", strFielddList, strData, strWhere, "230") != 0)
					strError += myUfisCom.GetLastErrorMessage() + CString("\n");
				myTab.SetLineColor(i, colBlack, colWhite);
				myTab.SetLineTag(i, myTab.GetFieldValues(i, "URNO,FURN,DURN,CODE,READ,REMA,DURA,CDAT,LSTU,USEC,USEU"));
			}
			else if(bCol == colRed)
			{
				strWhere = "WHERE URNO='" + myTab.GetFieldValue(i, "URNO") + "'";
				if(myUfisCom.CallServer ( "DRT", "DCFTAB", strFielddList, strData, strWhere, "230") != 0)
					strError += myUfisCom.GetLastErrorMessage() + CString("\n");
			}
		}
	}
	if(strError != "")
	{
		MessageBox(strError);
	}

	bmOpen = false;
}

BEGIN_EVENTSINK_MAP(MultiDelayDlg, CDialog)
    //{{AFX_EVENTSINK_MAP(MultiDelayDlg)
	ON_EVENT(MultiDelayDlg, IDC_TAB, 15 /* CloseInplaceEdit */, OnCloseInplaceEditTab, VTS_I4 VTS_I4)
	ON_EVENT(MultiDelayDlg, IDC_TAB, 14 /* EditPositionChanged */, OnEditPositionChangedTab, VTS_I4 VTS_I4 VTS_BSTR)
	ON_EVENT(MultiDelayDlg, IDC_TAB, 10 /* HitKeyOnLine */, OnHitKeyOnLineTab, VTS_I2 VTS_I4)
	ON_EVENT(MultiDelayDlg, IDC_TAB, 9 /* RowSelectionChanged */, OnRowSelectionChangedTab, VTS_I4 VTS_BOOL)
	ON_EVENT(MultiDelayDlg, IDC_TAB, 16 /* BoolPropertyChanged */, OnBoolPropertyChangedTab, VTS_I4 VTS_I4 VTS_BOOL)
	ON_EVENT(MultiDelayDlg, IDC_TAB, 1 /* SendLButtonClick */, OnSendLButtonClickTab, VTS_I4 VTS_I4)
	ON_EVENT(MultiDelayDlg, IDC_TAB, 2 /* SendLButtonDblClick */, OnSendLButtonDblClickTab, VTS_I4 VTS_I4)
	ON_EVENT(MultiDelayDlg, IDC_TAB, 7 /* InplaceEditCell */, OnInplaceEditCellTab, VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR)
	ON_EVENT(MultiDelayDlg, IDC_TAB, 3 /* SendRButtonClick */, OnSendRButtonClickTab, VTS_I4 VTS_I4)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

//****************************************************************
//TAB EVENT FUNCTIONS
void MultiDelayDlg::OnCloseInplaceEditTab(long LineNo, long ColNo) 
{
	CheckValuesChanged(LineNo);
}

void MultiDelayDlg::OnEditPositionChangedTab(long LineNo, long ColNo, LPCTSTR Value) 
{
	CheckValuesChanged(LineNo);
}

void MultiDelayDlg::OnHitKeyOnLineTab(short Key, long LineNo) 
{
}

void MultiDelayDlg::OnRowSelectionChangedTab(long LineNo, BOOL Selected) 
{
	CheckValuesChanged(LineNo);
}

void MultiDelayDlg::OnBoolPropertyChangedTab(long LineNo, long ColNo, BOOL NewValue) 
{
	CheckValuesChanged(LineNo);
}

void MultiDelayDlg::OnSendLButtonClickTab(long LineNo, long ColNo) 
{
	
}
//****************************************************************
// MWO: When the user double clicks on the CODE column the
//      choice list for delay code must appear to select an
//      existing code from the basic data. 
//      The urno will be written into the DURN field and the 
//      code will temp. be written to the code column.
//****************************************************************
void MultiDelayDlg::OnSendLButtonDblClickTab(long LineNo, long ColNo) 
{
}
//END TAB EVENT FUNCTIONS
//****************************************************************

void MultiDelayDlg::OnOK() 
{
	OnSave();
	CDialog::OnOK();
	bmOpen = false;

}

BOOL MultiDelayDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	

	isInit = true;
	CString strText = "Delays for Flight <" + CString(prmFlight->Flno) + ">";
	this->SetWindowText(strText);	

	CRect olRect;
	GetWindowRect(olRect);
	CString	m_key = "DialogPosition\\MultiDelayDlg";
	GetDialogFromReg(olRect, m_key);

	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW/* | SWP_NOSIZE*/);

	CRect olR2;
	myTab.GetClientRect(olR2);
	myTab.SetWindowPos(&wndTop, olR2.left, olR2.top, olRect.Width()-10, olR2.Height(), SWP_SHOWWINDOW);
	InitTab();
	InitUfisCom();

	FillTab();
	HandleSizeAndPosition();

	if(ogPrivList.GetStat("DelayCodeDlg.Save") != '1')
		m_OK.EnableWindow(false);
	if(ogPrivList.GetStat("RotationDlg.Delete") != '1')
		m_Delete.EnableWindow(false);
	if(ogPrivList.GetStat("RotationDlg.Insert") != '1')
		m_New.EnableWindow(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//**************************************************
//MWO: Initializes the main structure
//     look & feel of the tab
//**************************************************
void MultiDelayDlg::InitTab()
{
	myTab.ResetContent();
	myTab.SetHeaderString("URNO,FURN,DURN,Code,Read,Duration,Remark,Created on,Updated on,Created by,Updated by");
	myTab.SetLogicalFieldList("URNO,FURN,DURN,CODE,READ,DURA,REMA,CDAT,LSTU,USEC,USEU");
	myTab.SetHeaderLengthString("-1,-1,-1,100,40,70,720,100,100,100,100");
	myTab.SetColumnWidthString("10,10,10,12,1,4,2000,14,14,32,32");
	myTab.EnableHeaderSizing(TRUE);
	myTab.EnableInlineEdit(TRUE);
	myTab.SetAutoSizeByHeader(TRUE);
	myTab.SetInplaceEditUpperCase(FALSE);
	myTab.ShowHorzScroller(TRUE);
	myTab.SetFontName("Arial");
	myTab.SetFontSize(14);
	myTab.SetLineHeight(20);
	myTab.SetNoFocusColumns("0,1,2,4,7,8,9,10");
	myTab.SetColumnBoolProperty(4, "Y", "N");
	myTab.SetDefaultCursor(FALSE);
	myTab.DateTimeSetColumnFormat(7, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm");
	myTab.DateTimeSetColumnFormat(8, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm");
	myTab.SetLeftTextOffset(2);
	CString strColor;
	strColor.Format("%ld,%ld", colBlue, colBlue);
	myTab.CursorDecoration("URNO,FURN,DURN,CODE,READ,REMA,DURA,CDAT,LSTU,USEC,USEU", "T,B", "2,2", strColor);

}

//**************************************************
//MWO: Initialize the UfisCom control for the correct
//     connection to the server.
//**************************************************
void MultiDelayDlg::InitUfisCom()
{
    myUfisCom.SetHomeAirport(pcgHome);
    myUfisCom.InitCom(ogCommHandler.pcmRealHostName, "CEDA");
    myUfisCom.SetTwe( CString(pcgHome) + CString(",") + CString("TAB") + "," + CString("FIPS"));
    myUfisCom.SetUserName(ogCommHandler.pcmUser);
    myUfisCom.SetWorkStation(myUfisCom.GetWorkstationName());
    myUfisCom.SetCedaPerameters(ogCommHandler.pcmUser, pcgHome, "TAB");

    myUfisCom.SetHomeAirport(pcgHome);
    myUfisCom.SetTableExt("TAB");
    myUfisCom.SetModule("FIPS");
    myUfisCom.SetTwe("");
    myUfisCom.SetTws("0");
    myUfisCom.SetUserName(ogCommHandler.pcmUser);
}

//**************************************************
// MWO: Fills the tab with already existing
//      delay rows.
//**************************************************
void MultiDelayDlg::FillTab()
{
	//For PRF 8286
	char pclConfigPath[256];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));
	char pclDelayField[256];
	GetPrivateProfileString("FIPS", "SHOW_DELAYCODE", "FIPS",pclDelayField, sizeof pclDelayField, pclConfigPath);
	if(strcmp(pclDelayField,"ALPHABETICAL") == 0)
	{
		myDeleyCodeField = "DECA";
	}
	else
	{
		myDeleyCodeField = "DECN";
	}
	//"URNO,FURN,DURN,READ,REMA,DURA,CDAT,LSTU,USEC,USEU" from rotationDlg
	//"URNO,FURN,DURN,CODE,READ,REMA,DURA,CDAT,LSTU,USEC,USEU" local myTab
	for(int i = 0; i < prmTabSrc->GetLineCount(); i++)
	{
		CString strValues = prmTabSrc->GetFieldValues(i, "URNO,FURN,DURN");
		CString strDurn = prmTabSrc->GetFieldValue(i, "DURN");
		CString strCode = ogBCD.GetField("DEN", "URNO", strDurn, myDeleyCodeField);
		strValues += CString(",") + strCode + CString(",");
		strValues += prmTabSrc->GetFieldValues(i, "READ,DURA,REMA,CDAT,LSTU,USEC,USEU");
		myTab.InsertTextLine(strValues, true);
		myTab.SetLineTag(myTab.GetLineCount()-1, strValues);
	}
}
//**************************************************
// Creates an empty line to be inserted 
// into the tab control including a new
// URNO
// returns: A string of a valid empty line
//**************************************************
CString MultiDelayDlg::GetEmptyLine()
{
	CString olEmptyLine = "";

	char pclUrno[12] = "";
		
	CString olCDAT = "";
	CTime olTime = CTime::GetCurrentTime();
	CString olFlightUrno = "";

	olFlightUrno.Format("%ld", prmFlight->Urno);

	olCDAT = olTime.Format("%Y%m%d%H%M%S");

	sprintf(pclUrno, "%ld", ogBasicData.GetNextUrno());
	olEmptyLine = CString(pclUrno) +  CString(",") + olFlightUrno + CString(",,,,,,") + olCDAT + CString(",,") + CString(ogCommHandler.pcmUser) + CString(",");

	return olEmptyLine;
}
//**************************************************
//MWO: Check if some values are changed. This
//     is necessary to identif modified lines
//     or reset lines to unmodified when the old
//     value is written back.
//**************************************************
void MultiDelayDlg::CheckValuesChanged(int LineNo)
{
	long tCol = -1;
	long bCol = -1;
	myTab.GetLineColor(LineNo, &tCol, &bCol);
	CString strCurrValues = myTab.GetFieldValues(LineNo, myTab.GetLogicalFieldList());//tabData.GetLineValues(LineNo);
	if(strCurrValues != myTab.GetLineTag(LineNo))
	{
		if(bCol == colWhite)
		{
			CString strCode = myTab.GetFieldValues(LineNo, "CODE");
			CString strUrno = ogBCD.GetField("DEN", myDeleyCodeField, strCode, "URNO");
			myTab.SetFieldValues(LineNo, "DURN",strUrno);

			myTab.SetLineColor(LineNo, colBlack, colGreen);
			myTab.Refresh();
		}
	}
	else
	{
		if(bCol == colGreen)
		{
			CString strCode = myTab.GetFieldValues(LineNo, "CODE");
			CString strUrno = ogBCD.GetField("DEN", myDeleyCodeField, strCode, "URNO");
			myTab.SetFieldValues(LineNo, "DURN",strUrno);

			myTab.SetLineColor(LineNo, colBlack, colWhite);
			myTab.Refresh();
		}
	}
}
//**************************************************
//MWO: When the user has clicked the delete
//     button. Get the current selected line
//     and depending on the state the line 
//     will be directly deleted (if new /yellow)
//     or marked to be deleted if it was an
//     existing row.
//**************************************************
void MultiDelayDlg::HandleDelete()
{
	long currSel = myTab.GetCurrentSelected();
	long bCol = -1;
	long tCol = -1;
	if(currSel > -1)
	{
		myTab.GetLineColor(currSel, &tCol, &bCol);
		if(bCol != colRed)
		{
			myTab.SetLineColor(currSel, colBlack, colRed);
		}
		if(bCol == colYellow)
		{
			myTab.DeleteLine(currSel);
		}
		if(bCol == colRed)
		{
			myTab.SetLineColor(currSel, colBlack, colWhite);
			CheckValuesChanged(currSel);
		}
		myTab.Refresh();
	}
}



void MultiDelayDlg::OnClose() 
{
	myUfisCom.CleanupCom();	
	CString	m_key = "DialogPosition\\MultiDelayDlg";
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);

	CDialog::OnClose();
	bmOpen = false;
}

void MultiDelayDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	HandleSizeAndPosition();
	
}

//**************************************************
//MOW: Handle the sizeing and layout of the embedded
//	   controls.
//**************************************************
void MultiDelayDlg::HandleSizeAndPosition()
{
	CRect okRect;
	CRect cancelRect;
	CRect newRect;
	CRect deleteRect;
	CRect clientRect;
	CRect helpRect;
	CRect tabRect;

	if(isInit == false) return;
	m_OK.GetWindowRect(okRect);
	m_Cancel.GetWindowRect(cancelRect);
	m_Delete.GetWindowRect(deleteRect);
	m_New.GetWindowRect(newRect);
	m_HelpText.GetWindowRect(helpRect);
	this->GetWindowRect(clientRect);

	int buttonWidth = 100;//;okRect.Width();
	int buttonHeight = 22;//okRect.Height();
	int tabHeight = clientRect.Height() - (5*buttonHeight);
	int tabWidth = clientRect.Width();
	tabRect.left = 5;
	tabRect.right = tabWidth-14;
	tabRect.top = 5;
	tabRect.bottom = tabRect.top + tabHeight;

	int currY = clientRect.Height() - 40;
	okRect.left = 20;
	newRect.left = okRect.left;
	okRect.top = currY - buttonHeight;
	okRect.right = okRect.left + buttonWidth;
	newRect.right = okRect.right;
	okRect.bottom = okRect.top + buttonHeight;
	newRect.top = okRect.top - 10 - buttonHeight;
	newRect.bottom = newRect.top + buttonHeight;

	deleteRect.top = newRect.top;
	deleteRect.bottom = newRect.bottom;
	cancelRect.top = okRect.top;
	cancelRect.bottom = okRect.bottom;
	deleteRect.left = newRect.right + 10;
	cancelRect.left = okRect.right + 10;
	deleteRect.right = deleteRect.left + buttonWidth;
	cancelRect.right = cancelRect.left + buttonWidth;
	helpRect.top = deleteRect.top;
	helpRect.left = deleteRect.right + 10;
	helpRect.right = helpRect.left + helpRect.Width();
	helpRect.bottom = helpRect.top + helpRect.Height();


	myTab.SetWindowPos(&wndTop, tabRect.left, tabRect.top, tabRect.Width(), tabRect.Height(), SWP_SHOWWINDOW);
	m_OK.SetWindowPos(&wndTop, okRect.left, okRect.top, okRect.Width(), okRect.Height(), SWP_SHOWWINDOW);
	m_Cancel.SetWindowPos(&wndTop, cancelRect.left, cancelRect.top, cancelRect.Width(), cancelRect.Height(), SWP_SHOWWINDOW);
	m_Delete.SetWindowPos(&wndTop, deleteRect.left, deleteRect.top, deleteRect.Width(), deleteRect.Height(), SWP_SHOWWINDOW);
	m_New.SetWindowPos(&wndTop, newRect.left, newRect.top, newRect.Width(), newRect.Height(), SWP_SHOWWINDOW);
	m_HelpText.SetWindowPos(&wndTop, helpRect.left, helpRect.top, helpRect.Width(), helpRect.Height(), SWP_SHOWWINDOW);

	m_OK.UpdateWindow();
	m_Cancel.UpdateWindow();
	m_Delete.UpdateWindow();
	m_New.UpdateWindow();
}


void MultiDelayDlg::OnDestroy() 
{
	CString	m_key = "DialogPosition\\MultiDelayDlg";
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
	CDialog::OnDestroy();
	
	delete this;
	//this = NULL;
}

//**************************************************
//MWO: Check if the Duration insertion/change 
//     was digit character, otherwise set back the
//	   old value
//**************************************************
void MultiDelayDlg::OnInplaceEditCellTab(long LineNo, long ColNo, LPCTSTR NewValue, LPCTSTR OldValue) 
{
	if(ColNo == 5)
	{
		int len = 0;
		int i = 0;
		bool badString = false;
		len = strlen(NewValue);
		for( i = 0; i < len; i++)
		{
			if(!isdigit(NewValue[i]))
			{
				badString = true;
			}
		}
		if(badString == true)
		{
			myTab.SetColumnValue(LineNo, ColNo, OldValue);
		}
	}
	if(ColNo == 3)
	{
		CString strUrno = ogBCD.GetField("DEN", myDeleyCodeField, CString(NewValue), "URNO");
		if(strUrno == "")
		{
			myTab.SetColumnValue(LineNo, ColNo, OldValue);
		}
		else
		{
			myTab.SetColumnValue(LineNo, ColNo-1, strUrno);
		}
	}
	CheckValuesChanged(LineNo);	
}

void MultiDelayDlg::OnSendRButtonClickTab(long LineNo, long ColNo) 
{
	if(ColNo == 3)
	{
		CString olSelStr = myTab.GetFieldValue(LineNo, "DURN");
		CString strDeca = ogBCD.GetField("DEN", "URNO", olSelStr, "DECA");
		CString strDecn = ogBCD.GetField("DEN", "URNO", olSelStr, "DECN");
		CString strAlc3 = ogBCD.GetField("DEN", "URNO", olSelStr, "ALC3");
		CString strDena = ogBCD.GetField("DEN", "URNO", olSelStr, "DENA");
		olSelStr = strDeca + "," + strDecn + "," + strAlc3 + "," + strDena + "," + olSelStr;

		AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "DEN", "DECA,DECN,ALC3,DENA,URNO", "DENA+", olSelStr);
		if(polDlg->DoModal() == IDOK)
		{
			CString olUrno = polDlg->GetField("URNO");
			CString olCode = polDlg->GetField(myDeleyCodeField);
			myTab.SetFieldValues(LineNo, "CODE,DURN", olCode+","+olUrno);
			CheckValuesChanged(LineNo);
			myTab.Refresh();
		}
		delete polDlg;
	}
}
