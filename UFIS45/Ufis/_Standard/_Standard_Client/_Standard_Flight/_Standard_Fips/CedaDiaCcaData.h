// CedaDiaCcaData.h

#ifndef __CEDADIACCADATA__
#define __CEDADIACCADATA__
 
#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <CCSCedadata.h>
#include <CedaFlzData.h>
#include <CCSddx.h>
//#include <SeasonCollectCedaFlightData.h>

#include "TimeRange.h"		// 050317 MVy: needed for DIACCADATA


//---------------------------------------------------------------------------------------------------------

struct CCAFLIGHTDATA;
struct COLLECTFLIGHTDATA;

enum
{
	CCA_CFL_NOCONFLICT=-1,
	CCA_CFL_BADALLOC=0,
	CCA_CFL_OVERLAPPED=2
};


struct DIA_CKI_UNDO 
{
	CString Ckic;
	CString Ckit;
	long	Furn;
	long	Curn;
	long	Id;

	DIA_CKI_UNDO(void)
	{ 
		Ckic = "-1";
		Ckit = "-1";
		Furn = 0;
		Curn = 0;
		Id = 0;
	}

};



struct DIACCADATA 
{
	CTime 	 Cdat; 		// Erstellungsdatum
	CTime 	 Ckba; 		// Belegung Checkin-Schalter aktueller Beginn
	CTime 	 Ckbs; 		// Belegung Checkin-Schalter geplanter Beginn
	CTime 	 Ckea; 		// Belegung Checkin-Schalter aktuelles Ende
	CTime 	 Ckes; 		// Belegung Checkin-Schalter geplantes Ende
	char 	 Ckic[6]; 	// Checkin-Schalter
	char 	 Ckif[2]; 	// fester Checkin-Schalter
	char 	 Ckit[2]; 	// Terminal Checkin-Schalter
	char 	 Ctyp[2]; 	// Check-In Typ (Common oder Flight)
	long 	 Flnu; 	// Eindeutige Datensatz-Nr. des zugeteilten Fluges
	CTime 	 Lstu; 		// Datum letzte Änderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[33]; 	// Anwender (Ersteller)
	char 	 Useu[33]; 	// Anwender (letzte Änderung)
	char	 Rema[61];	// Bemerkung
	char	 Disp[61];	// Bemerkung
	char	 Act3[6];	// Act3 of Flight
	char     Flno[11];	// Flno of Flight
	char	 Stat[11];	// Status of Cca for Conflicts
	long	 Ghpu;		// Urno von Ghp
	long     Gpmu;		// Urno von Gpm
	long     Ghsu;		// Urno von Ghs
	char	 Cgru[258];	// Urnos aus GRN
	CTime	 Stod;
	int 	 Copn	; 	// Schalteröffnungszeit (in Min. vor Abflug)
	int		 Coix;		// Code Share Index
    long     Gurn ;     //For Airlinegroup
	
	long	 KKey;	
	bool	 Valid;
	int 	 NotValidBrush;
	char	Coverage;

	char 	 CkicPrev[6]; 	// Checkin-Schalter
	char 	 CkitPrev[2]; 	// fester Checkin-Schalter

/*STAT
	Index 0: WRONG Halle/Terminal-Konflik ALLOC,	==> 'J'	or ' '
	Index 1: CONFIRMED,		==> ' ' or 'J'
	Index 2: OVERLAPPED,	==> 'J' or ' '
	Index 3: CONFIRMED,		==> ' ' or 'J'
	Index 4: FLNO changed
	Index 5: Cki whitout demand
	Index 6: Rule changed
	Index 7: Act changed 
	Index 8: Std changed
	Index 9: Error: 0 = No error;   N=No Rules  ; X=More Rules;   W = Rules without leistung
*/

	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed für Relaese

	bool	 IsSelected;
	bool	AlcKonf;

	//bool	Freq;	
	int		FlightFreq;

	int NextC;
	int PrevC;
	int Freq;

	bool bmInDB;

	/// MCU 22.09.98 Brush wird bei Konfliktcheck gesetzt
	//CBrush *Brush;

	DIACCADATA()
	{ 
		memset(this,'\0',sizeof(*this));
		AlcKonf		=	false;
		IsSelected  =   false;
		Valid		=	false;
		Urno		=   0;
		Flnu		=   0;
		Ghpu		=	0;
		Gpmu		=	0;
		Ghsu		=	0;
		KKey		=	0;
		Coix		=	0;
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Ckba		=	TIMENULL;
		Ckbs		=	TIMENULL;
		Ckea		=	TIMENULL;
		Ckes		=	TIMENULL;
		Lstu		=	TIMENULL;
		Stod		=	TIMENULL;
		strcpy(Stat, "0000000000");
		Freq = 0;
		FlightFreq = 0;
		NextC = 0;
		PrevC = 0;

		bmInDB = false;
		NotValidBrush = -1;
		Coverage = ' ';
	}

public:
	bool IsOpen();
	bool UpdateFIDSMonitor();
	
	const CTime GetBestStartAlloc( bool bUtcToLocal = false );
	const CTime GetBestEndAlloc( bool bUtcToLocal = false );
	CTimeRange GetAllocation( bool bUtcToLocal = false );		// 050317 MVy: return actual times if exist, otherwise return planned time
	bool IsFlightOfAirline( CStringArray& strarrAirlineCodes );		// 050323 MVy: return whether this flight belongs to one of the specified airlines
	bool IsFlight();		// 050324 MVy: helper function for check
	bool IsCommon();		// 050324 MVy: helper function for check
	bool IsPreCheck();		// 050324 MVy: helper function for check
	bool IsBlocking();		// 050324 MVy: helper function for check

}; // end DIACCADATAStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaDiaCcaData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omCkicMap;

    CCSPtrArray<DIACCADATA> omData;
	CCSPtrArray<DIACCADATA>  omDelData;

 	char pcmListOfFields[2048];
	CString omSelection;
	bool bmSelection;


	CTime omFrom;
	CTime omTo;

	CUIntArray omKKeyArray;
	CMapPtrToPtr omKKeyMap;

	CMapPtrToPtr omFlnuMap;
	CMapPtrToPtr omFlnuMapC;
 
	bool bmNew ;

	bool bmOffLine;

	CedaFlzData omFlzData;

	const void *pomParent;

    CCSPtrArray<DIA_CKI_UNDO> omUndo;
	long lmUndoId;


// Operations
public:
    CedaDiaCcaData();
	~CedaDiaCcaData();

	bool SetParent(const void *popParent);

	void SetOffLine();
	void ReleaseCca();
	
	
	void AddToKeyMap(DIACCADATA *prpCca);
	void DelFromKeyMap(DIACCADATA *prpCca); 
	
//	bool CkeckOverlapping(CString opCnam, CTime opStart, CTime opEnd);
	bool CkeckOverlapping(CString opCnam, CTime opStart, CTime opEnd, long lpFlnu=0);
	void Undo(long lpId);

	void Register(void);
	void UnRegister(void);
	void ClearAll(bool bpWithRegistration = true);
//	bool ReadAll(CString &opUrnos, CTime opFrom, CTime opTo, bool bpClearAll = true, bool bpOnlyFlight = true); 
	bool ReadAll(CString &opUrnos, CTime opFrom, CTime opTo, CMapStringToString& opFilterMap, bool bpClearAll = true, bool bpOnlyFlight = false);

	bool GetCcasByFlnu(CCSPtrArray<DIACCADATA> &ropCcaList, long lpFlnu);
	int	GetCkiCountByFlnu(long lpFlnu);



	bool HasCcasByFlnu(long lpFlnu);
	bool GetCcasByCkic(CCSPtrArray<DIACCADATA> &ropCcaList, CString opCkic);
	void GetCcaArray(CCSPtrArray<DIACCADATA> &opCca, CString opTyp, CString opCnam);

	bool GetCcaData(long lpFlnu, CString &opCca, CTime &opStart, CTime &opEnd);    

	bool GetCcaArray(long lpFlnu, CCSPtrArray<DIACCADATA> &opData);
	void GetCcaError(CCSPtrArray<DIACCADATA> &ropAct, long lpFlnu);

	bool GetCca(CCSPtrArray<DIACCADATA> &opCca, CMapStringToString& opTypMap, bool bpOnlySel, bool bpMustBeInOneDay = false);


	bool GetMinMaxCkic(long lpFlnu, CString &opMin, CString &opMax);
	bool GetAllCkic(long lpFlnu, CString &opMin, CString &opMax, CString &opCompleteList);

	bool GetMinMaxCkicAndTime(long lpFlnu, CString &opMin, CString &opMax, CTime &opStart, CTime &opEnd);

	bool IsPassFilter(DIACCADATA *prpCca);

	void GetCommonUrnos(CUIntArray &opUrnos, bool bpOnlySel);

	bool GetSel(CUIntArray &opUrnos);

	bool Expand(long lpUrno, CTime opFrom , CTime opTo, CTime opRefDay, CUIntArray& opDayArray, CUIntArray& opMustHasArray, ofstream& of, ofstream& ofRes, char* opTrenner, CMapStringToString& opFlightsToDelete, bool bpAllDays = false, bool bpOverwrite = false, CString opStev = CString(" "));
//	bool Expand(CCSPtrArray<DIACCADATA> &ropSelCca, CTime opFrom , CTime opTo, CTime opRefDay, CUIntArray& opDayArray, CUIntArray& opMustHasArray, ofstream& of, ofstream& ofRes, char* opTrenner, CMapStringToString& opFlightsToDelete, bool bpAllDays = false, bool bpOverwrite = false);
	bool Expand(CProgressCtrl&	opProgess, CCSPtrArray<DIACCADATA> &ropSelCca, CTime opFrom , CTime opTo, CUIntArray& opDayArray, CUIntArray& opMustHasArray, CStringArray& opCopied, CStringArray& opNotCopied, bool bpOverwrite = false, CString opStev = CString(" "), CTime opRefDate = TIMENULL);

	bool WriteExpandFile(CString& olValue, CCAFLIGHTDATA* prlSrcFlight, COLLECTFLIGHTDATA *prlFlight,
			CMapStringToString& opHasMap, bool bpCopy, CTime opFrom, CStringArray& opCopied, CStringArray& opNotCopied);

	bool DeleteOrUpdateTimeFrame(CTime opFrom, CTime opTo, CString opVerkTag);

	void ChangeTimesByUrnos(CUIntArray &opUrnos, CTime opCkbs , CTime opCkes, CString &opMessage, bool bpActual = false);

	bool Read(char *pspWhere = NULL, bool bpClearAll = true, bool bpDdx = false);
    bool ReadInsert(char *pspWhere = NULL);
	bool InsertInternal(DIACCADATA *prpCca, bool bpDdx = true);
	bool Update(DIACCADATA *prpCca);
	bool UpdateInternal(DIACCADATA *prpCca, bool bpDdx = true, bool bpSaveStat = false, bool bpUndo = true);
	bool DeleteInternal(DIACCADATA *prpCca, bool bpDdx = true);
	bool ReadSpecial(CCSPtrArray<DIACCADATA> &ropAct,char *pspWhere);
	bool ExistCkic(DIACCADATA *prpCca);
	bool ExistSpecial(char *pspWhere);

	bool IsOpenClose(long lpFlnu, bool &bpOpen, bool &bpClose);


	//Save Single Row
	bool Save(DIACCADATA *prpCca);
	void SaveInternal(CCSPtrArray<DIACCADATA> *popCcaList, bool bpDdx =true);
	//Save multi
	void Release(CCSPtrArray<DIACCADATA> *popCcaList = NULL, bool bpDdx = true);

	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void ReloadCcaSBC(void *vpDataPointer);


	DIACCADATA  *GetCcaByUrno(long lpUrno);
	bool UpdateSpecial(CCSPtrArray<DIACCADATA> &ropCca, CCSPtrArray<DIACCADATA> &ropCcaSave, long lpFlightUrno);
	void GetSpecialList(CCSPtrArray<DIACCADATA> &opCca, CString &opFieldDataList);
	bool DeleteSpecial(CString olFlightUrnoList);
	void SetPreSelection(CTime &opFrom, CTime &opTo, CString &opFtyps);
	bool ReadCcas(char *pcpSelection = NULL, bool bpIgnoreViewSel = false);
	bool AddCcaInternal(DIACCADATA *prpCCA);
	CString omFtyps;
	CString omFlnus;

	void SetTableName(CString opTab)
	{
		strcpy(pcmTableName, opTab.GetBuffer(0));
	}
	
	// hbe 11/12/98: um gewünschten Verkehrstag erweitert; def.: => heute
    // hbe 5/99: um Object erweitert BLK / REPBLK 
	CString MakeBlkData( CTime opTrafficDay = TIMENULL , int ipDOO = -1, CString opObject="BLK");
	CString MakeBlkData(RecordSet *popBlkRecord , int ipDOO = -1 );

	CString  GetDataList(DIACCADATA *prpCca);

	void GetBazValues(long lpFlnu, CString opFlti, CString &opBaz1,CString &opBaz4, CTime &opStart, CTime &opEnd);

	bool AllHaveActOpenTime(const CUIntArray &ropUrnos);


	bool CheckDemands( const CCAFLIGHTDATA *prpFlightD, bool bpFltiChanged = false);
	bool DelAllDemByFlnuGhpu(long lpFlnu, long lpGhpu);

	bool UpdateExpandOverwrite(CMapStringToString& olFlightsToDelete);

//-----------------
	bool ChangeTime(long lpUrno,CTime opNewStart,CTime opNewEnd, bool bpWithSave);
	void CheckCommonForFlight(long lpAlcUrno, bool bpDDX = false);
	void UpdateCommonForFlight(bool bpOnlyOpen = false);


	// Private methods
private:
    void PrepareDiaCcaData(DIACCADATA *prpDIACCADATA);
	CMapStringToString omFilterMap;
	bool DeleteUnion(DIACCADATA& rlCca);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDADIACCADATA__
