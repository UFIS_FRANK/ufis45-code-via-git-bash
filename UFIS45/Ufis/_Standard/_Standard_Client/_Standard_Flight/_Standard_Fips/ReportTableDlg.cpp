// ReportTableDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <ReportTableDlg.h>
#include <ReportSelectDlg.h>
#include <RotationDlgCedaFlightData.h>
#include <resrc1.h>
#include <process.h>
#include <Utils.h>
#include <ReportReasonForChange.h>

#ifdef _DEBUG 
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportTableDlg 

CReportTableDlg::CReportTableDlg(CWnd* pParent, int iTable, 
								 CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo, 
								 char *pcpSelect, char *pcpInfo2, int ipArrDep,RotationDlgCedaFlightData* popRotDlgData, char *pcpInfoPlus)
	: CDialog(CReportTableDlg::IDD, pParent)
{
	pomData = popData;
	pomRotDlgData = popRotDlgData;
	pcmInfo = pcpInfo;
	pcmInfo2 = pcpInfo2;	
	pcmInfoPlus = pcpInfoPlus;	
	pcmSelect = pcpSelect;
	miTable = iTable;
	bmDailyRep =  false;
	imArrDep = ipArrDep;
	bmCommonDlg = false;

    pomReportTable = NULL;
	pomReportCTable = NULL;

	pomGefundenefluegeViewer = NULL;			//0-L1-Flights by airline-Report1TableViewer
	pomReportSameORGTableViewer  = NULL;		//1-L2-flights by origin
	pomReportSameREGNTableViewer = NULL;		//2-L3-flights by a/c registration
	pomReportSameDESTableViewer  = NULL;		//3-L4-flights by destination
	pomReportSameACTTableViewer  = NULL;		//4-L5-flights by a/c type
	pomReportSameTTYPTableViewer = NULL;		//5-L6-flights by nature code
	pomAdditionalReport11TableViewer = NULL;	//12-L13-aircraft movements per hour
	pomGanttChartReportWnd  = NULL;
	pomReport16Viewer = NULL;					//15-L14-inventory (a/c on ground)
	pomReportDailyFlightLogViewer = NULL;		//26-L20-daily flight log
	pomReportOvernightTableViewer = NULL;		//20-L21-overnight flight report
	pomReportArrDepLoadPaxTableViewer   = NULL;	//23-L24-load and pax report
	pomReportFlightGPUTableViewer   = NULL;	    //24-L25-gpu usage report
	pomReportFlightDelayViewer = NULL;			//27-L27-delay report
	pomReport19Viewer = NULL;					//18-L19-checkin
	pomReportReasonForChangeViewer = NULL;      //28-Reason For Change Report 

	m_key = "DialogPosition\\Reports";
}

CReportTableDlg::~CReportTableDlg()
{
	if (pomReportTable != NULL)
	{
		delete pomReportTable;
	}
	if (pomReportCTable != NULL)
	{
		delete pomReportCTable;
	}

	//0-L1-flights by airline (Report1TableViewer)
	if (pomGefundenefluegeViewer != NULL)
		delete pomGefundenefluegeViewer;

	//1-L2-flights by origin
	if (pomReportSameORGTableViewer != NULL)
		delete pomReportSameORGTableViewer;

	//2-L3-flights by a/c registration
	if (pomReportSameREGNTableViewer != NULL)
		delete pomReportSameREGNTableViewer;

	//3-L4-flights by destination
	if (pomReportSameDESTableViewer != NULL)
		delete pomReportSameDESTableViewer;

	//4-L5-flights by a/c type
	if (pomReportSameACTTableViewer != NULL)
		delete pomReportSameACTTableViewer;

	//5-L6-flights by nature code
	if (pomReportSameTTYPTableViewer != NULL)
		delete pomReportSameTTYPTableViewer;

	//12-L13-aircraft movements per hour
	if (pomAdditionalReport11TableViewer != NULL)
		delete pomAdditionalReport11TableViewer;
	if (pomGanttChartReportWnd != NULL)
		delete pomGanttChartReportWnd;

	//15-L14-inventory (a/c on ground)
	if (pomReport16Viewer != NULL)
		delete pomReport16Viewer;

	//18-L19-checkin
	if (pomReport19Viewer != NULL)
		delete pomReport19Viewer;

	//26-L20-daily flight log
	if (pomReportDailyFlightLogViewer)
		delete pomReportDailyFlightLogViewer;

	//20-L21-overnight flight report
	if (pomReportOvernightTableViewer)
		delete pomReportOvernightTableViewer;

	//23-L24-load and pax report
	if (pomReportArrDepLoadPaxTableViewer)
		delete pomReportArrDepLoadPaxTableViewer;

	//24-L25-gpu usage report
	if (pomReportFlightGPUTableViewer)
		delete pomReportFlightGPUTableViewer;

	//27-L27-delay report
	if (pomReportFlightDelayViewer)
		delete pomReportFlightDelayViewer;

	//28-Reason For Change Report - PRF 8379  	
	if(pomReportReasonForChangeViewer != NULL)
	{
		delete pomReportReasonForChangeViewer;
		pomReportReasonForChangeViewer = NULL;
	}
}

void CReportTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportTableDlg)
	DDX_Control(pDX, IDC_BEENDEN, m_CB_Beenden);
	DDX_Control(pDX, IDC_PAPIER, m_CB_Papier);
	DDX_Control(pDX, IDC_DRUCKEN, m_CB_Drucken);
	DDX_Control(pDX, IDC_DATEI, m_CB_Datei);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportTableDlg, CDialog)
	//{{AFX_MSG_MAP(CReportTableDlg)
	ON_BN_CLICKED(IDC_DATEI, OnDatei)
	ON_BN_CLICKED(IDC_DRUCKEN, OnDrucken)
	ON_BN_CLICKED(IDC_PAPIER, OnPapier)
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	ON_WM_SIZE()
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportTableDlg 

void CReportTableDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void CReportTableDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}

void CReportTableDlg::OnSize(UINT nType, int cx, int cy)  
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();

	if(pomReportTable != NULL)
	{
		CRect rect;	
		GetClientRect(&rect);
		rect.InflateRect(1, 1);     // hiding the CTable window border
		pomReportTable->SetPosition(rect.left, rect.right, rect.top+imDialogBarHeight, rect.bottom);
	}
	if(pomReportCTable != NULL)
	{
		CRect rect;	
		GetClientRect(&rect);
		rect.InflateRect(1, 1);     // hiding the CTable window border
		pomReportCTable->SetPosition(rect.left, rect.right, rect.top+imDialogBarHeight, rect.bottom);
	}

	this->Invalidate();
}


void CReportTableDlg::SetReasonData(CCSPtrArray<REASON_FOR_CHANGE> *popData, char *pcpInfo)
{
	pomReasonData = popData;
	pcmInfo = pcpInfo;
}

BOOL CReportTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\Reports";
	
	// Table Initialisierung 
	CRect olrectTable;

	GetClientRect(&olrectTable);
    imDialogBarHeight = olrectTable.bottom - olrectTable.top;
    
	// extend dialog window to current screen width
    olrectTable.left = 0;
//    olrectTable.top = 65;
    olrectTable.top = 95;
    olrectTable.right = 1024;
    olrectTable.bottom = 768;
    MoveWindow(&olrectTable);
	
	int ilFlightCount;
	bool blValidTable = true ;

	// Default Ausgabe: Drucker
	m_CB_Papier.SetCheck( TRUE ) ;

	if( miTable < 14 || (miTable > 19 && miTable <= 28))  // Reports verwenden CCSTable, au�er ...
	{
		pomReportTable = new CCSTable;
		pomReportTable->imSelectMode = 0;

		GetClientRect(&olrectTable);              // !!!!!T E S T !!!!!!!!!!!!!!!!!
		olrectTable.top = olrectTable.top + imDialogBarHeight + 25;

		olrectTable.InflateRect(1,1);     // hiding the CTable window border
		//pomReportTable->SetTableData(this, olrectTable.left+2, olrectTable.right-2, olrectTable.top+28, olrectTable.bottom);
		pomReportTable->SetTableData(this, olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);
		if(miTable != 28)
			ilFlightCount = pomData->GetSize();
	}
	else if( miTable <= 19 ) // Wochenplan, Daily-Pl�ne und Inventar verwenden CTable
	{
		pomReportCTable = new CTable;
		pomReportCTable->imSelectMode = 0;

		GetClientRect(&olrectTable);                    
		olrectTable.top = olrectTable.top + imDialogBarHeight;

		olrectTable.InflateRect(1,1);     // hiding the CTable window border
		pomReportCTable->SetTableData(this, olrectTable.left, olrectTable.right, olrectTable.top, (olrectTable.bottom)+28);
	}
	else
	{
		blValidTable = false ;
		Beep( 500, 500 ) ;
		CString olMessage ;
		olMessage.Format( GetString( IDS_STRING1460) , miTable ) ;
		MessageBox( olMessage, "ReportTableDlg", MB_ICONWARNING ) ;
	}


	if( blValidTable )
	{
		switch( miTable )
		{
		//Flights by Airline
		case 0:
		{
			pomGefundenefluegeViewer = new GefundenefluegeTableViewer(pomData, pcmInfo, pcmSelect);
			pomGefundenefluegeViewer->Attach(pomReportTable);

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomGefundenefluegeViewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomGefundenefluegeViewer->omTableName;
		}
		break;

		//Flights by Origin
		case 1:
		{
			pomReportSameORGTableViewer = new ReportSameORGTableViewer(pomData, pcmInfo, pcmSelect);
			pomReportSameORGTableViewer->SetParentDlg(this);
			pomReportSameORGTableViewer->Attach(pomReportTable);
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReportSameORGTableViewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomReportSameORGTableViewer->omTableName;
		}
		break;

		//	Flights by A/C Registration
		case 2:
		{
			pomReportSameREGNTableViewer = new ReportSameREGNTableViewer(pomData, pcmInfo, pcmSelect);
			pomReportSameREGNTableViewer->SetParentDlg(this);
			pomReportSameREGNTableViewer->Attach(pomReportTable);
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReportSameREGNTableViewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomReportSameREGNTableViewer->omTableName;
		}
		break;

		// Flights by Destination
		case 3:
		{
			pomReportSameDESTableViewer = new ReportSameDESTableViewer(pomData, pcmInfo, pcmSelect);
			pomReportSameDESTableViewer->SetParentDlg(this);
			pomReportSameDESTableViewer->Attach(pomReportTable);
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReportSameDESTableViewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomReportSameDESTableViewer->omTableName;
		}
		break;

		// Flights by A/C Type
		case 4:
		{
			pomReportSameACTTableViewer = new ReportSameACTTableViewer(pomData, pcmInfo, pcmSelect);
			pomReportSameACTTableViewer->SetParentDlg(this);
			pomReportSameACTTableViewer->Attach(pomReportTable);
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReportSameACTTableViewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomReportSameACTTableViewer->omTableName;
		}
		break;

		// Flights By Nature Code
		case 5:
		{
			pomReportSameTTYPTableViewer = new ReportSameTTYPTableViewer(pomData, pcmInfo, pcmSelect);
			pomReportSameTTYPTableViewer->SetParentDlg(this);
			pomReportSameTTYPTableViewer->Attach(pomReportTable);
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReportSameTTYPTableViewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomReportSameTTYPTableViewer->omTableName;
		}
		break;

		//Aircraft Movements per Hour
		case 12:
		{
			CStringArray olColumnsToShow;
			olColumnsToShow.Add("Time");
			olColumnsToShow.Add("AFlights");
			olColumnsToShow.Add("DFlights");

			pomAdditionalReport11TableViewer = 
				new AdditionalReport11TableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
			pomAdditionalReport11TableViewer->SetParentDlg(this);
			pomAdditionalReport11TableViewer->Attach(pomReportTable);

			olColumnsToShow.RemoveAll();

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomAdditionalReport11TableViewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomAdditionalReport11TableViewer->omTableName;
			
			CRect olRect;
			GetClientRect(&olRect);

			// Anzhal Balken
			int ilBarCount = pomAdditionalReport11TableViewer->GetBarCount();
			CUIntArray olData;
			//added for the PRF 8494
			CUIntArray olArrivalData;
			CUIntArray olDepartureData;
			
			CStringArray olXAxeText;
			m_CB_Datei.EnableWindow(false);

			for (int i = 0; i < ilBarCount; i++)
			{
				olData.Add(pomAdditionalReport11TableViewer->GetData(i));
				//Changed while fixing the PRF 8494 
				olArrivalData.Add(pomAdditionalReport11TableViewer->GetArrivalData(i));
				olDepartureData.Add(pomAdditionalReport11TableViewer->GetDepartureData(i));
				olXAxeText.Add(pomAdditionalReport11TableViewer->GetTime(i));

			}
			CColor olColor;
			if (pomGanttChartReportWnd)
				delete pomGanttChartReportWnd;
			pomGanttChartReportWnd = new GanttChartReportWnd(this, "By Hour", 
					                                         "Number of Flights", olColor);

			// Daten (=Balkenhoehe) �bergeben
			pomGanttChartReportWnd->SetData(olData);
			//changed while fixing the PRF 8494 by sisl
			pomGanttChartReportWnd->SetArrivalData(olArrivalData);
			pomGanttChartReportWnd->SetDepartureData(olDepartureData);

			// Text der XAxe �bergeben
			pomGanttChartReportWnd->SetXAxeText(olXAxeText);

			pomGanttChartReportWnd->Create(NULL, "Chart", WS_CHILD | WS_VISIBLE, olRect, this, 0);
			pomGanttChartReportWnd->MoveWindow(olRect, true);


		}
		break;


		// Inventory (A/C On Ground)
		case 15:
		{
			pomReport16Viewer = new Report16TableViewer(pomData, pcmInfo);
			pomReport16Viewer->Attach(pomReportCTable);

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReport16Viewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomReport16Viewer->omTableName;
		}
		break;

		case 19 :	//  Daily Counter-Plan
		{
			CString olDay(pcmInfo2) ;
			olDay.TrimLeft() ;
			CTime olTimeFrom, olTimeTo;
			if (olDay.GetLength() < 14)
			{
				olTimeFrom = CTime::GetCurrentTime();
				olTimeTo = CTime(olTimeFrom.GetYear(), olTimeFrom.GetMonth(), olTimeFrom.GetDay(), 23, 59, 59);
			}
			else if (olDay.GetLength() < 28)
			{
				olTimeFrom = DBStringToDateTime(olDay.Left(14));
				olTimeTo = CTime(olTimeFrom.GetYear(), olTimeFrom.GetMonth(), olTimeFrom.GetDay(), 23, 59, 59);
			}
			else
			{
				olTimeFrom = DBStringToDateTime(olDay.Left(14));
				olTimeTo = DBStringToDateTime(olDay.Mid(15,14));
			}

			//CTime olTrafficDay = olDay.GetLength() >= 14 ? DBStringToDateTime(olDay.Left(14)) : CTime::GetCurrentTime() ;
			
			pomReport19Viewer = new Report19DailyCcaTableViewer(pcmInfo, pcmSelect, pcmInfo2);
			pomReport19Viewer->Attach(pomReportCTable);

			//m_CB_Datei.EnableWindow( false ) ;
			//m_CB_Papier.SetCheck( TRUE ) ;

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReport19Viewer->ChangeViewTo(olTimeFrom, olTimeTo);	// Daily Counter
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			char pclHeadlineSelect[120];
			sprintf(pclHeadlineSelect, GetString(IDS_STRING1462), pcmSelect,pcmInfo);
			omHeadline = pclHeadlineSelect;
		}

		break;
		// Overnight Flight Report
		case 20:
		{
			pomReportOvernightTableViewer = new ReportOvernightTableViewer(pomRotDlgData, pcmInfo, pcmSelect);
			pomReportOvernightTableViewer->SetParentDlg(this);
			pomReportOvernightTableViewer->Attach(pomReportTable);
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReportOvernightTableViewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomReportOvernightTableViewer->omTableName;
		}
		break;

		// Load & Pax Report
		case 23:
		{
			pomReportArrDepLoadPaxTableViewer = new ReportArrDepLoadPaxTableViewer(pomData, imArrDep, pcmInfo);
			pomReportArrDepLoadPaxTableViewer->SetParentDlg(this);
			pomReportArrDepLoadPaxTableViewer->Attach(pomReportTable);

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReportArrDepLoadPaxTableViewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomReportArrDepLoadPaxTableViewer->omTableName;
		}
		break;

		// Load & Pax Report User
		case 25:
		{
			pomReportArrDepLoadPaxTableViewer = new ReportArrDepLoadPaxTableViewer(pomData, imArrDep, pcmInfo, NULL, true);
			pomReportArrDepLoadPaxTableViewer->SetParentDlg(this);
			pomReportArrDepLoadPaxTableViewer->Attach(pomReportTable);

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReportArrDepLoadPaxTableViewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomReportArrDepLoadPaxTableViewer->omTableName;
		}
		break;

		//GPU Usage Report
		case 24:
		{
			pomReportFlightGPUTableViewer = new ReportFlightGPUTableViewer(pomData, pcmInfo, pcmSelect);
			pomReportFlightGPUTableViewer->SetParentDlg(this);
			pomReportFlightGPUTableViewer->Attach(pomReportTable);

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReportFlightGPUTableViewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomReportFlightGPUTableViewer->omTableName;
		}
		break;

		// Daily flight log
		case 26:
		{
			// Create viewer
			pomReportDailyFlightLogViewer = new ReportDailyFlightLogViewer();
			pomReportDailyFlightLogViewer->Attach(pomReportTable);

			// Build and show table
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReportDailyFlightLogViewer->ChangeViewTo(*pomData, pcmInfo, imArrDep);
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
 			
			omHeadline = pomReportDailyFlightLogViewer->omTableName;
		}
		break;	

		// Delay Report
		case 27:
		{
			// Create viewer
			pomReportFlightDelayViewer = new ReportFlightDelayViewer();
			pomReportFlightDelayViewer->Attach(pomReportTable);
			// Build and show table
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReportFlightDelayViewer->ChangeViewTo(*pomData, pcmInfo, pcmSelect, pcmInfo2, imArrDep);
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomReportFlightDelayViewer->omTableName;
		}
		break;	

		//Reason For Change - PRF 8379  
		case 28:
		{
			pomReportReasonForChangeViewer = new ReportReasonForChangeViewer();
			pomReportReasonForChangeViewer->Attach(pomReportTable);
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReportReasonForChangeViewer->ChangeViewTo(*pomReasonData, pcmInfo);
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
			omHeadline = pomReportReasonForChangeViewer->omTableName;
		}
		break;

		default:
			Beep( 500, 500 ) ;
			MessageBox( GetString(ST_FEHLER)+" serious data damage !" ,"ReportTableDlg", MB_ICONERROR ) ;

		}	// end switch( miTable ...
	}	// end if( ValidTable ...

	CDialog::SetWindowText(omHeadline);	//Dialogbox-�berschrift

	m_resizeHelper.Init(this->m_hWnd);
	m_resizeHelper.Fix(IDC_BEENDEN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_DRUCKEN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_PAPIER,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_DATEI,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_STATIC,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}



void CReportTableDlg::OnDatei() 
{
	// Daily-Pl�ne nur auf Papier 
	if( bmDailyRep && m_CB_Datei.GetCheck() )
	{
		MessageBox( GetString(IDS_STRING1403), MB_OK ) ;
	}
}

void CReportTableDlg::OnDrucken() 
{
//im FPMS und BDPS-SEC/UIF mu� ein neuer Men�eintrag zum Einrichten von Druckern aufgenommen
//werden. F�r diese Zwecke gibt es einen Common-Dialog, mit dessen Hilfe der/die Drucker eingerichtet
//werden k�nnen. Die Benutzung ist in der C++ Online-Hilfe dokumentiert

	//  Wer geht nur auf Papier raus ? : Daily-Pl�ne 
	bool blAlwaysPaper = bmDailyRep ? true : false ;

	if( ! blAlwaysPaper )
	{
		if((m_CB_Papier.GetCheck() != 1) && (m_CB_Datei.GetCheck() != 1))
		{
			if( MessageBox(GetString(IDS_STRING1434), GetString(IMFK_PRINT), MB_YESNO|MB_ICONQUESTION ) != IDYES ) return ;
			else blAlwaysPaper = true ;
		}
	}

	// ----------------------------------------------------------- Druckerausgabe 
	if (m_CB_Papier.GetCheck() == 1 || blAlwaysPaper )
	{
		switch(miTable)
		{
			//0-L1-Flights by airline-Report1TableViewer
			case 0:
			{
				pomGefundenefluegeViewer->PrintTableView();
			}
			break;

			//1-L2-flights by origin
			case 1:
			{
				pomReportSameORGTableViewer->PrintTableView();
			}
			break;

			//2-L3-flights by a/c registration
			case 2:
			{
				pomReportSameREGNTableViewer->PrintTableView();
			}
			break;

			//3-L4-flights by destination
			case 3:
			{
				pomReportSameDESTableViewer->PrintTableView();
			}
			break;

			//4-L5-flights by a/c type
			case 4:
			{
				pomReportSameACTTableViewer->PrintTableView();
			}
			break;

			//5-L6-flights by nature code
			case 5:
			{
				pomReportSameTTYPTableViewer->PrintTableView();
			}
			break;

			//12-L13-aircraft movements per hour
			case 12:
			{
				pomAdditionalReport11TableViewer->PrintTableView(pomGanttChartReportWnd);
			}
			break;

			//15-L14-inventory (a/c on ground)
			case 15:
			{
				pomReport16Viewer->PrintTableView();
			}
			break;

			case 19:	//	Daily Counter-Plan
			{
				pomReport19Viewer->PrintTableView();
			}
			break;

			//20-L21-overnight flight report
			case 20:
			{
				pomReportOvernightTableViewer->PrintTableView();
			}
			break;

			//23-L24-load and pax report
			case 23:
			{
				pomReportArrDepLoadPaxTableViewer->PrintTableView();
			}
			break;

			//25-L26-load and pax report
			case 25:
			{
				pomReportArrDepLoadPaxTableViewer->PrintTableView();
			}
			break;

			//24-L25-gpu usage report
			case 24:
			{
				pomReportFlightGPUTableViewer->PrintTableView();
			}
			break;

			//26-L20-daily flight log
			case 26:
			{
				pomReportDailyFlightLogViewer->PrintTableView();
			}
			break;

			//27-L27-delay report
			case 27:
			{
				pomReportFlightDelayViewer->PrintTableView();
			}
			break;

			//28-Reason For Change Report PRF 8379
			case 28:
			{
				pomReportReasonForChangeViewer->PrintTableView();
			}
			break;
		}
	}


	// -------------------------------------- Drucken in Datei: alle au�er OPS-Plan
	if (m_CB_Datei.GetCheck() == 1 && ! blAlwaysPaper )
	{

		char pclConfigPath[256];
		char pclExcelPath[256];
		char pclTrenner[64];
		

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, pclConfigPath);


		GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
			pclTrenner, sizeof pclTrenner, pclConfigPath);


		if(!strcmp(pclExcelPath, "DEFAULT"))
			CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING977), GetString(ST_FEHLER), MB_ICONERROR);

		CString olFileName;

		switch(miTable)
		{
			//0-L1-Flights by airline-Report1TableViewer
			case 0:
			{
				if (pomGefundenefluegeViewer->PrintPlanToFile(pclTrenner))
					olFileName = pomGefundenefluegeViewer->omFileName;
			}
			break;

			//1-L2-flights by origin
			case 1:
			{	
				if (pomReportSameORGTableViewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReportSameORGTableViewer->omFileName;
			}
			break;

			//2-L3-flights by a/c registration
			case 2:
			{
				if (pomReportSameREGNTableViewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReportSameREGNTableViewer->omFileName;
			}
			break;

			//3-L4-flights by destination
			case 3:
			{
				if (pomReportSameDESTableViewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReportSameDESTableViewer->omFileName;
			}
			break;

			//4-L5-flights by a/c type
			case 4:
			{
				if (pomReportSameACTTableViewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReportSameACTTableViewer->omFileName;
			}
			break;

			//5-L6-flights by nature code
			case 5:
			{
				if (pomReportSameTTYPTableViewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReportSameTTYPTableViewer->omFileName;
			}
			break;

			//12-L13-aircraft movements per hour
			case 12:
			{
				LPOPENFILENAME polOfn = new OPENFILENAME;
				char buffer[256] = "";
				char buffer2[256] = "";
				LPSTR lpszStr;
				lpszStr = buffer;
				//CString olStr = CString("c:\\tmp\\") + CString("*.txt");
				CString olStr = CCSLog::GetTmpPath("\\*.txt");
				strcpy(buffer,(LPCTSTR)olStr);

				memset(polOfn, 0, sizeof(*polOfn));
				polOfn->lStructSize = sizeof(*polOfn) ;
				polOfn->lpstrFilter = "Export File(*.txt)\0*.txt\0";
				polOfn->lpstrFile = (LPSTR) buffer;
				polOfn->nMaxFile = 256;
				strcpy(buffer2, GetString(IDS_STRING1077));
				polOfn->lpstrTitle = buffer2;

				//bmCommonDlg = true;
				if(GetOpenFileName(polOfn) == TRUE)
				{
					//bmCommonDlg = false;
					char pclDefPath[512];
					strcpy(pclDefPath, polOfn->lpstrFile);
					int ilPathlen = strlen(pclDefPath);
					//ilPathlen = ilPathlen -4;
					pclDefPath[(ilPathlen)] = '\0';
					for(int iLen = ilPathlen; iLen > 0; iLen--)
					{
						if(pclDefPath[iLen] == '\\')
						{
							break;
						}
					}
					if ((strstr(pclDefPath, ".")) == 0)
						strcat(pclDefPath, ".txt");

					pomAdditionalReport11TableViewer->PrintPlanToFile(pclDefPath);
				}
				delete polOfn;
			}
			break;

			//15-L14-inventory (a/c on ground)
			case 15:
			{
				if (pomReport16Viewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReport16Viewer->omFileName;
			}
			break;

			case 19:	//  Daily Counter-Plan
			{
				if (pomReport19Viewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReport19Viewer->omFileName;
			}
			break;

			//20-L21-overnight flight report
			case 20:
			{
				if (pomReportOvernightTableViewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReportOvernightTableViewer->omFileName;
			}
			break;

			//23-L24-load and pax report
			case 23:
			{
				if (pomReportArrDepLoadPaxTableViewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReportArrDepLoadPaxTableViewer->omFileName;
			}
			break;

			//25-L26-load and pax report user
			case 25:
			{
				if (pomReportArrDepLoadPaxTableViewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReportArrDepLoadPaxTableViewer->omFileName;
			}
			break;

			//24-L25-gpu usage report
			case 24:
			{
				if (pomReportFlightGPUTableViewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReportFlightGPUTableViewer->omFileName;
			}
			break;

			//26-L20-daily flight log
			case 26:
			{
				if (pomReportDailyFlightLogViewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReportDailyFlightLogViewer->omFileName;
			}
			break;

			//27-L27-delay report
			case 27:
			{
				if (pomReportFlightDelayViewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReportFlightDelayViewer->omFileName;
			}
			break;

			//28-Reason For Change Report - PRF 8379 
			case 28:
			{
				if (pomReportReasonForChangeViewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReportReasonForChangeViewer->omFileName;
			}
			break;
		}


		bool test = true; //only for testing error
		if (olFileName.IsEmpty() || olFileName.GetLength() > 255 || !test)
		{
			if (olFileName.IsEmpty())
			{
				CString mess = "Filename is empty !" + olFileName;
				CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
				return;
			}
			if (olFileName.GetLength() > 255 )
			{
				CString mess = "Length of the Filename > 255: " + olFileName;
				CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
				return;
			}

			CString mess = "Filename invalid: " + olFileName;
			CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}

		char pclTmp[256];
		strcpy(pclTmp, olFileName); 

	   /* Set up parameters to be sent: */
		char *args[4];
		args[0] = "child";
		args[1] = pclTmp;
		args[2] = NULL;
		args[3] = NULL;

	int li_Path = _spawnv( _P_NOWAIT , pclExcelPath, args );
	if(li_Path == -1)
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_EXCELPATH_ERROR), GetString(ST_FEHLER), MB_ICONERROR);

	}
}

void CReportTableDlg::OnPapier() 
{
	;
}

void CReportTableDlg::OnBeenden() 
{

	if (!bmCommonDlg)
		CDialog::OnCancel();

}

void CReportTableDlg::OnCancel() 
{

	if (!bmCommonDlg)
		CDialog::OnCancel();

}
