#if !defined(AFX_UNDODLG_H__F562F070_7882_11D1_8347_0080AD1DC701__INCLUDED_)
#define AFX_UNDODLG_H__F562F070_7882_11D1_8347_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// UndoDlg.h : header file
//

#include <resrc1.h>

/////////////////////////////////////////////////////////////////////////////
// UndoDlg dialog

class UndoDlg : public CDialog
{
// Construction
public:
	UndoDlg(CWnd* pParent = NULL);   // standard constructor

	~UndoDlg();

	void AppExit();	

	void SetMessage(CString opMessage);
	void Reset();


// Dialog Data
	//{{AFX_DATA(RotationJoinDlg)
	enum { IDD = IDD_UNDO };
	//}}AFX_DATA

	CListBox	m_List;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UndoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(UndoDlg)
	virtual void OnUndo();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UNDODLG_H__F562F070_7882_11D1_8347_0080AD1DC701__INCLUDED_)
