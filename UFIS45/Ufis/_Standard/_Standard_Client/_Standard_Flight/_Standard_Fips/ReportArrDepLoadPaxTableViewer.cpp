// ReportArrDepLoadPaxTableViewer.cpp : implementation file
// 
// according to arrival/departure load & pax flight each day


#include <stdafx.h>
#include <ReportArrDepLoadPaxTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>
#include <resrc1.h>

#include <ReportXXXCedaData.h>
#include <CedaBasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// ReportArrDepLoadPaxTableViewer
//

int ReportArrDepLoadPaxTableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                             ";
				olVias = olVias.Left(120);
			}


			olApc3 = olVias.Mid(1,3);
			olApc3.TrimLeft();
			sprintf(prlVia->Apc3, olApc3);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

	}
	return opVias->GetSize();
}



ReportArrDepLoadPaxTableViewer::ReportArrDepLoadPaxTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, 
												 int ipArrDep, char *pcpInfo, char *pcpSelect, bool bpUser)
{

	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;
	bmUser = bpUser;
	
	imArrDep = ipArrDep;

	bmIsFromSearch = false;
    pomTable = NULL;
	imFlightCount = 0;


	// load pax data
	CString olFlightUrnos;
	char buffer[65];

	for (int i=0; i < pomData->GetSize(); i++)
	{
		// collect flight-urnos
		ltoa((*pomData)[i].Urno, buffer, 10);
		olFlightUrnos += CString(buffer) + CString(",");
	}

	if(!olFlightUrnos.IsEmpty())
	{
		olFlightUrnos = olFlightUrnos.Left(olFlightUrnos.GetLength() - 1);
	}

	if (bmUser)
		omApxData.ReadFlnus(olFlightUrnos); 
	else
		omLoaData.ReadFlnus(olFlightUrnos); 



}

ReportArrDepLoadPaxTableViewer::~ReportArrDepLoadPaxTableViewer()
{
	omPrintHeadHeaderArray.DeleteAll();  // BWi
    DeleteAll();
}


void ReportArrDepLoadPaxTableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void ReportArrDepLoadPaxTableViewer::Attach(CCSTable *popTable)
{

    pomTable = popTable;

}


int ReportArrDepLoadPaxTableViewer::GetFlightCount()
{

	return imFlightCount;

}


void ReportArrDepLoadPaxTableViewer::ChangeViewTo(const char *pcpViewName)
{

    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}


/////////////////////////////////////////////////////////////////////////////
// ReportArrDepLoadPaxTableViewer -- code specific to this class

void ReportArrDepLoadPaxTableViewer::MakeLines()
{
	ROTATIONDLGFLIGHTDATA *prlAFlight;
	ROTATIONDLGFLIGHTDATA *prlDFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*pomData)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			prlAFlight = prlFlight;
			prlDFlight = NULL;
			if(prlNextFlight != NULL)
			{
				if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
				{
					prlDFlight = prlNextFlight;

					if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
					{
						ilLc++;
					}
					else
						blRDeparture = true;
				}
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				prlAFlight = NULL;
				prlDFlight = prlFlight;
				blRDeparture = false;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					if(blRDeparture)
					{
						blRDeparture = false;
						prlAFlight = prlFlight;
						prlDFlight = NULL;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								prlDFlight = prlNextFlight;
								if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
								{
									ilLc++;
								}
								else
									blRDeparture = true;
							}
						}
					}
					else
					{
						prlAFlight		= prlFlight;
						prlDFlight		= prlFlight;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								ilLc--;
								prlAFlight	 = NULL;
								blRDeparture = true;
							}
						}
					}
				}
			}
		}
		ilLineNo = MakeLine(prlAFlight, prlDFlight);

	} // for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)

//generate the headerinformation
	CString olName = GetString(REPORTS_RB_LoadandPaxReport);
	if (bmUser)
		olName = GetString(REPORTS_RB_LoadandPaxReportUser);

	CString olTimeSet = GetString(IDS_STRING1920); //UTC
	if(bgReportLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	char pclHeader[256];
	CString olPart = GetString(IDS_DEPARTURE);
	if (imArrDep == 0)
	{
		olPart = GetString(IDS_ARRIVAL);
		sprintf(pclHeader, GetString(IDS_R_HEADER1), olName, olPart, pcmInfo, imFlightCount, imFlightCount, 0, olTimeSet);
		if(bgReports)
		sprintf(pclHeader, "%s%s with %s from %s (Flights: %d / ARR: %d / DEP: %d) - %s",ogPrefixReports, olName, olPart, pcmInfo, imFlightCount, imFlightCount, 0, olTimeSet);
	}
	else
	{
		sprintf(pclHeader, GetString(IDS_R_HEADER1), olName, olPart, pcmInfo, imFlightCount, 0, imFlightCount, olTimeSet);
		if(bgReports)
		sprintf(pclHeader, "%s%s with %s from %s (Flights: %d / ARR: %d / DEP: %d) - %s",ogPrefixReports, olName, olPart, pcmInfo, imFlightCount, 0, imFlightCount, olTimeSet);
	}
		omTableName = pclHeader;

}

		

int ReportArrDepLoadPaxTableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
    SAMEARRDEPLOADPAXTABLE_LINEDATA rlLine;

	if((prpAFlight != NULL) && (prpDFlight != NULL) )
	{
		imFlightCount++;
		if (imArrDep == 0)
		{
			MakeLineData(prpAFlight, NULL, rlLine);
			return CreateLine(rlLine);
		}
		if (imArrDep == 1)
		{
			MakeLineData(NULL, prpDFlight, rlLine);
			return CreateLine(rlLine);
		}
	}
	if (prpAFlight != NULL)
	{
		if (imArrDep == 0)
		{
			imFlightCount++;
			MakeLineData(prpAFlight, NULL, rlLine);
			return CreateLine(rlLine);
		}
	}
	if (prpDFlight != NULL)
	{
		if (imArrDep == 1)
		{
			imFlightCount++;
			MakeLineData(NULL, prpDFlight, rlLine);
			return CreateLine(rlLine);
		}
	}
	return -1;
}




void ReportArrDepLoadPaxTableViewer::MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, SAMEARRDEPLOADPAXTABLE_LINEDATA &rpLine)
{

	CString olStr;
	if(prpAFlight != NULL)
	{
		rpLine.Adid = "A";
		rpLine.AUrno = prpAFlight->Urno;
		rpLine.ARkey = prpAFlight->Rkey;
		rpLine.AFlno = CString(prpAFlight->Flno);
		rpLine.AStoa = prpAFlight->Stoa; 
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.AStoa);
		//	rpLine.ADate = prpAFlight->Stoa.Format("%d.%m.%y");
/*DateToHourDivString(prpAFlight->Onbl, prpAFlight->Stoa);*/
		rpLine.AOrg3 = CString(prpAFlight->Org3);
		rpLine.AOrg4 = CString(prpAFlight->Org4);
		rpLine.ATtyp = CString(prpAFlight->Ttyp);
		rpLine.AAct  = CString(prpAFlight->Act3);
		rpLine.ALand = CTime(prpAFlight->Land); 
		if(bgReportLocal) 
		{
			ogBasicData.UtcToLocal(rpLine.ALand);
			ogBasicData.UtcToLocal(prpAFlight->Stoa);
		}
			rpLine.ADate = prpAFlight->Stoa.Format("%d.%m.%y");
/*DateToHourDivString(prpAFlight->Onbl, prpAFlight->Stoa);*/
		if(bgReports)
		{
			rpLine.Alc3= prpAFlight->Alc3;
			rpLine.Act5 = prpAFlight->Act5;
			
		}
		rpLine.ABlt1 = CString(prpAFlight->Blt1);
		rpLine.APsta = CString(prpAFlight->Psta);
		rpLine.ARegn = CString(prpAFlight->Regn);
		rpLine.AGta1 = CString(prpAFlight->Gta1);
		
		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpAFlight);

		if(opVias.GetSize() == 0)
			rpLine.AVia3 = "";
		else
		{
			rpLine.AVia3 =  opVias[ilViaCnt-1].Apc3;	// letzter Via vor Homeairport = letzte Zeile in Vial
		}
		opVias.DeleteAll();

		if (!bmUser)
		{
			if (strcmp(pcgHome, "WAW") == 0)
			{
				// load and pax data
				// set user load data
				rpLine.Mail = CString((omLoaData.GetLoaWAW(prpAFlight->Urno, CString("StripMAIL"),CString("LDM,USR,MVT,KRI"),CString("WAW"))->Valu));
				rpLine.Cgot = CString((omLoaData.GetLoaWAW(prpAFlight->Urno, CString("StripCGOT"),CString("LDM,USR,MVT,KRI"),CString("WAW"))->Valu));
				rpLine.Bagw = CString((omLoaData.GetLoaWAW(prpAFlight->Urno, CString("StripBAGW"),CString("LDM,USR,MVT,KRI"),CString("WAW"))->Valu));
				rpLine.Bagn = CString((omLoaData.GetLoaWAW(prpAFlight->Urno, CString("StripBAGN"),CString("LDM,USR,MVT,KRI"),CString("WAW"))->Valu));
				
				// set user passenger data
				rpLine.Pax1 = CString((omLoaData.GetLoaWAW(prpAFlight->Urno, CString("Pxfirst"),CString("LDM,USR,MVT,KRI"),CString("WAW"))->Valu));
				rpLine.Pax2 = CString((omLoaData.GetLoaWAW(prpAFlight->Urno, CString("Pxbus"),CString("LDM,USR,MVT,KRI"),CString("WAW"))->Valu));
				rpLine.Pax3 = CString((omLoaData.GetLoaWAW(prpAFlight->Urno, CString("Pxeco"),CString("LDM,USR,MVT,KRI"),CString("WAW"))->Valu));
				rpLine.Paxt = CString((omLoaData.GetLoaWAW(prpAFlight->Urno, CString("PXT"),CString("LDM,USR,MVT,KRI"),CString("WAW"))->Valu));

				rpLine.PaxInf = CString((omLoaData.GetLoaWAW(prpAFlight->Urno, CString("Pxinf"),CString("LDM,USR,MVT,KRI"),CString("WAW"))->Valu));
				rpLine.PaxId = CString((omLoaData.GetLoaWAW(prpAFlight->Urno, CString("PADeco"),CString("LDM,USR,MVT,KRI"),CString("WAW"))->Valu));
				rpLine.Paxi = CString((omLoaData.GetLoaWAW(prpAFlight->Urno, CString("TrPXT"),CString("LDM,USR,MVT,KRI"),CString("WAW"))->Valu));
				rpLine.Paxf = CString((omLoaData.GetLoaWAW(prpAFlight->Urno, CString("TP"),CString("LDM,USR,MVT,KRI"),CString("WAW"))->Valu));
			}
			else
			{
				// load and pax data
				// set user load data
				rpLine.Mail = CString((omLoaData.GetLoa(prpAFlight->Urno, CString("StripMAIL"))->Valu));
				rpLine.Cgot = CString((omLoaData.GetLoa(prpAFlight->Urno, CString("StripCGOT"))->Valu));
				rpLine.Bagw = CString((omLoaData.GetLoa(prpAFlight->Urno, CString("StripBAGW"))->Valu));
				rpLine.Bagn = CString((omLoaData.GetLoa(prpAFlight->Urno, CString("StripBAGN"))->Valu));
				
				// set user passenger data
				rpLine.Pax1 = CString((omLoaData.GetLoa(prpAFlight->Urno, CString("Pxfirst"))->Valu));
				rpLine.Pax2 = CString((omLoaData.GetLoa(prpAFlight->Urno, CString("Pxbus"))->Valu));
				rpLine.Pax3 = CString((omLoaData.GetLoa(prpAFlight->Urno, CString("Pxeco"))->Valu));
				rpLine.Paxt = CString((omLoaData.GetLoa(prpAFlight->Urno, CString("PXT"))->Valu));

				rpLine.PaxInf = CString((omLoaData.GetLoa(prpAFlight->Urno, CString("Pxinf"))->Valu));
				rpLine.PaxId = CString((omLoaData.GetLoa(prpAFlight->Urno, CString("PADeco"))->Valu));
				rpLine.Paxi = CString((omLoaData.GetLoa(prpAFlight->Urno, CString("TrPXT"))->Valu));
				rpLine.Paxf = CString((omLoaData.GetLoa(prpAFlight->Urno, CString("TP"))->Valu));
			}
		}
		else
		{
			rpLine.Pax1 = CString(prpAFlight->Pax1);
			rpLine.Pax2 = CString(prpAFlight->Pax2);
			rpLine.Pax3 = CString(prpAFlight->Pax3);
			rpLine.Paxt = CString(prpAFlight->Paxt);
			rpLine.Paxf = CString(prpAFlight->Paxf);
			rpLine.Paxi = CString(prpAFlight->Paxi);

			rpLine.Mail = CString(prpAFlight->Mail);
			rpLine.Cgot = CString(prpAFlight->Cgot);
			rpLine.Bagn = CString(prpAFlight->Bagn);
			rpLine.Bagw = CString(prpAFlight->Bagw);

			// get apx data
			APXDATA *prlApxData;
			if ((prlApxData = omApxData.GetApxFlnuTyp(prpAFlight->Urno, "INF")) != NULL)
			{
				rpLine.PaxInf = CString(prlApxData->Paxc);
			}
			if ((prlApxData = omApxData.GetApxFlnuTyp(prpAFlight->Urno, "ID")) != NULL)
			{
				rpLine.PaxId = CString(prlApxData->Paxc);
			}
		}


	}
	else
	{
		rpLine.AUrno =  0;
		rpLine.ARkey =  0;
		rpLine.AFlno = "";
		rpLine.AStoa = TIMENULL; 
		rpLine.ALand = TIMENULL; 
		rpLine.AEtai = TIMENULL; 
		rpLine.ATtyp = "";
		rpLine.ADate = ""; 
		rpLine.AOrg3 = "";
		rpLine.AOrg4 = "";
		rpLine.AVia3 = "";
		rpLine.AAct  = "";
		rpLine.AGta1 = "";
		rpLine.ABlt1 = "";
		rpLine.ARegn = "";
		rpLine.APsta = "";
	}


	if(prpDFlight != NULL)
	{
		rpLine.Adid = "D";

		rpLine.DUrno = prpDFlight->Urno;
		rpLine.DRkey = prpDFlight->Rkey;
		rpLine.DFlno = CString(prpDFlight->Flno); 
		rpLine.DDes3 = CString(prpDFlight->Des3);
		rpLine.DDes4 = CString(prpDFlight->Des4);
		rpLine.DTtyp = CString(prpDFlight->Ttyp);
		rpLine.DAct  = CString(prpDFlight->Act3);

		rpLine.DAirb = CTime(prpDFlight->Airb);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DAirb);
		rpLine.DEtdi = CTime(prpDFlight->Etdi);
		if(bgReportLocal) 
		{
			ogBasicData.UtcToLocal(rpLine.DEtdi);
		}
		rpLine.DStod = prpDFlight->Stod;
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.DStod);
	
		rpLine.DDate = prpDFlight->Stod.Format("%d.%m.%y");
		rpLine.DVian = prpDFlight->Vian;

		if (rpLine.AAct.IsEmpty())
			rpLine.AAct = rpLine.DAct;
		if(bgReports)
		{			
			rpLine.Alc3= prpDFlight->Alc3;
			rpLine.Act5 = prpDFlight->Act5;
			rpLine.Des4= prpDFlight->Des4;
		}

		rpLine.DGtd1 = CString(prpDFlight->Gtd1);
		rpLine.DPstd = CString(prpDFlight->Pstd);
		rpLine.DRegn = CString(prpDFlight->Regn);

		char pclUrno[32];
/*		CedaCcaData olCcaData;
		CString olSelection = CString("WHERE FLNU IN (");
		olSelection += CString(ltoa(prpDFlight->Urno, pclUrno, 10));
		olSelection += CString(")");
		olCcaData.Read(olSelection.GetBuffer(0));
		
		int ilCompare;
		int ilLow;
		int ilHigh;
		int ilCcaSize = olCcaData.omData.GetSize() ;
		if (ilCcaSize > 0)
		{
			int ilLow  = atoi(olCcaData.omData[0].Ckic);
			int ilHigh = atoi(olCcaData.omData[0].Ckic);
		}

		for(int i = 0; i < ilCcaSize; i++)
		{
			if( prpDFlight->Urno == olCcaData.omData[i].Flnu )
			{

				ilCompare = atoi(olCcaData.omData[i].Ckic);
				if (ilCompare > ilHigh)
					ilHigh = ilCompare;
				if (ilCompare < ilLow)
					ilLow = ilCompare;
			}

			if (ilHigh > ilLow)
			{
				CString olCkic;
				olCkic.Format("%i-%i", ilLow, ilHigh);
				rpLine.DCkic = olCkic;
			}
			else
			{
				rpLine.DCkic = olCcaData.omData[0].Ckic;
			}

		}

*/
		ReportXXXCedaData olReportXXXCedaData("FLNU", "CKIC", "CCA", true);
		olReportXXXCedaData.ReadCleartext(CString(ltoa(prpDFlight->Urno, pclUrno, 10)));

		// groessten und kleinesten Wert extrahieren
		int ilCompare;
		if (olReportXXXCedaData.omSameFlnu.GetSize() > 0)
		{
			int ilLow  = atoi(olReportXXXCedaData.omSameFlnu[0]);
			int ilHigh = atoi(olReportXXXCedaData.omSameFlnu[0]);

			for (int i = 0; i < olReportXXXCedaData.omSameFlnu.GetSize(); i++)
			{
				ilCompare = atoi(olReportXXXCedaData.omSameFlnu[i]);
				if (ilCompare > ilHigh)
					ilHigh = ilCompare;
				if (ilCompare < ilLow)
					ilLow = ilCompare;
			}

			if (ilHigh > ilLow)
			{
				CString olCkic;
				olCkic.Format("%i-%i", ilLow, ilHigh);
				rpLine.DCkic = olCkic;
			}
			else
			{
				rpLine.DCkic = olReportXXXCedaData.omSameFlnu[0];
			}
		}


		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpDFlight);

		if(opVias.GetSize() == 0)
			rpLine.DVia3 = "";
		else
			rpLine.DVia3 =  opVias[0].Apc3;	// erster Via nach Homeairport = erste Zeile in Vial
		opVias.DeleteAll();


		if (!bmUser)
		{
			if (strcmp(pcgHome, "WAW") == 0)
			{
				// load and pax data
				// set user load data
				rpLine.Mail = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("StripMAIL"))->Valu));
				rpLine.Cgot = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("StripCGOT"))->Valu));
				rpLine.Bagw = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("StripBAGW"))->Valu));
				rpLine.Bagn = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("StripBAGN"))->Valu));
				
				// set user passenger data
				rpLine.Pax1 = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("Pxfirst"))->Valu));
				rpLine.Pax2 = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("Pxbus"))->Valu));
				rpLine.Pax3 = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("Pxeco"))->Valu));
				rpLine.Paxt = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("PXT"))->Valu));

				rpLine.PaxInf = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("Pxinf"))->Valu));
				rpLine.PaxId = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("PADeco"))->Valu));
				rpLine.Paxi = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("TrPXT"))->Valu));
				rpLine.Paxf = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("TP"))->Valu));
			}
			else
			{
				// load and pax data
				// set user load data
				rpLine.Mail = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("StripMAIL"))->Valu));
				rpLine.Cgot = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("StripCGOT"))->Valu));
				rpLine.Bagw = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("StripBAGW"))->Valu));
				rpLine.Bagn = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("StripBAGN"))->Valu));
				
				// set user passenger data
				rpLine.Pax1 = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("Pxfirst"))->Valu));
				rpLine.Pax2 = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("Pxbus"))->Valu));
				rpLine.Pax3 = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("Pxeco"))->Valu));
				rpLine.Paxt = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("PXT"))->Valu));

				rpLine.PaxInf = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("Pxinf"))->Valu));
				rpLine.PaxId = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("PADeco"))->Valu));
				rpLine.Paxi = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("TrPXT"))->Valu));
				rpLine.Paxf = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("TP"))->Valu));
			}
		}
		else
		{
			rpLine.Pax1 = CString(prpDFlight->Pax1);
			rpLine.Pax2 = CString(prpDFlight->Pax2);
			rpLine.Pax3 = CString(prpDFlight->Pax3);
			rpLine.Paxt = CString(prpDFlight->Paxt);
			rpLine.Paxf = CString(prpDFlight->Paxf);
			rpLine.Paxi = CString(prpDFlight->Paxi);

			rpLine.Mail = CString(prpDFlight->Mail);
			rpLine.Cgot = CString(prpDFlight->Cgot);
			rpLine.Bagn = CString(prpDFlight->Bagn);
			rpLine.Bagw = CString(prpDFlight->Bagw);

			// get apx data
			APXDATA *prlApxData;
			if ((prlApxData = omApxData.GetApxFlnuTyp(prpDFlight->Urno, "INF")) != NULL)
			{
				rpLine.PaxInf = CString(prlApxData->Paxc);
			}
			if ((prlApxData = omApxData.GetApxFlnuTyp(prpDFlight->Urno, "ID")) != NULL)
			{
				rpLine.PaxId = CString(prlApxData->Paxc);
			}
		}
	}
	else 
	{
		rpLine.DUrno =  0; 
		rpLine.DRkey =  0;
		rpLine.DEtdi = TIMENULL; 
		rpLine.DAirb = TIMENULL; 
		rpLine.DStod = TIMENULL; 
		rpLine.DFlno = "";
		rpLine.DDate = ""; 
		rpLine.DDes3 = "";
		rpLine.DDes4 = "";
		rpLine.DTtyp = "";
		rpLine.DVia3 = "";
		rpLine.DAct  = "";
		rpLine.DRegn = "";
		rpLine.DGtd1 = "";
	}


    return;

}



int ReportArrDepLoadPaxTableViewer::CreateLine(SAMEARRDEPLOADPAXTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void ReportArrDepLoadPaxTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void ReportArrDepLoadPaxTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// ReportArrDepLoadPaxTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void ReportArrDepLoadPaxTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	SAMEARRDEPLOADPAXTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}




void ReportArrDepLoadPaxTableViewer::DrawHeader()
{
	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN rlHeader;


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 67; 
	rlHeader.Font = &ogCourier_Bold_8;
	rlHeader.Text = GetString(IDS_STRING1078);//FLIGHT
	omHeaderDataArray.New(rlHeader);
 
	
 	
	rlHeader.Length = 55; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING298);// ORG
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING315);// DES
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 35; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING323);//STA
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING316);//STD
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 35; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING304);//ATA
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING321);//ATD
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 57; 
 	rlHeader.Text = GetString(IDS_STRING311);//A/C
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_STRING310);//REGN
	omHeaderDataArray.New(rlHeader);

 	if (imArrDep == 0)
	{
		rlHeader.Length = 25;
		rlHeader.Text = GetString(IDS_STRING308);//POS
	}
	if (imArrDep == 1)
	{
		rlHeader.Length = 70;	
		rlHeader.Text = GetString(IDS_STRING1641);//Cki-Counter
	}
 	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_STRING1406);//Gate
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 25; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING1151);//Belt
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING308);//Pos
	omHeaderDataArray.New(rlHeader);

	// Load & Pax
 	rlHeader.Length = 20; 
 	rlHeader.Text = GetString(IDS_FIRSTCLASS); //F
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_ECONCLASS); //C
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_BUSICLASS); //Y
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 20; 
 	rlHeader.Text = GetString(IDS_INFANTS); //INF
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 30; 
 	rlHeader.Text = GetString(IDS_PAXTTL); // P/TTL
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_ID); //ID
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_TRANSIT); //TR
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_TRANSFER); //TRF
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 40; 
 	rlHeader.Text = GetString(IDS_BAGGAGE); //BAG
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 40; 
 	rlHeader.Text = GetString(IDS_CARGO); //CGO
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 40; 
 	rlHeader.Text = GetString(IDS_MAIL); //MAIL
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 40; 
 	rlHeader.Text = GetString(IDS_LOADTTL); // L/TTL
	omHeaderDataArray.New(rlHeader); 

  if(bgReports)
  {		
	
	rlHeader.Length = 55; 
 	rlHeader.Text = GetString(IDS_STRING332); // Date
	omHeaderDataArray.New(rlHeader); 
	
	rlHeader.Length = 20; 
 	rlHeader.Text = GetString(IDS_STRING2786); // NA
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 20; 
 	rlHeader.Text = GetString(IDS_STRING2791); // ALC
	omHeaderDataArray.New(rlHeader); 
		
  
  }	
	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}



void ReportArrDepLoadPaxTableViewer::MakeColList(SAMEARRDEPLOADPAXTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{

	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AFlno;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DFlno;
	olColList.NewAt(olColList.GetSize(), rlColumnData);
  if(!bgReports)
  {
	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AOrg3;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DDes3;
	olColList.NewAt(olColList.GetSize(), rlColumnData);
  }
	
  if(bgReports)
  {
	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AOrg3;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DDes3+"/"+prlLine->DDes4;
	olColList.NewAt(olColList.GetSize(), rlColumnData);
  }
	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AStoa.Format("%H:%M");
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DStod.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->ALand.Format("%H:%M");
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DAirb.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

  if(!bgReports)
  {
	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AAct;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DAct;
	olColList.NewAt(olColList.GetSize(), rlColumnData);
  }
	
 if(bgReports)
 {
    rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AAct+ "/"+ prlLine->Act5;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DAct+"/"+prlLine->Act5 ;
	olColList.NewAt(olColList.GetSize(), rlColumnData);	
 }	
	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->ARegn;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DRegn;
	olColList.NewAt(olColList.GetSize(), rlColumnData);
	
	
	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->APsta;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DCkic;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AGta1;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DGtd1;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->ABlt1;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DPstd;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	// Load & Pax
	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Pax1;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Pax2;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Pax3;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->PaxInf;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Paxt;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->PaxId;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Paxi;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Paxf;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Bagn;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Cgot;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Mail;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Bagw;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

  if(bgReports)
  {
	
	rlColumnData.Alignment = COLALIGN_LEFT;
 	if (imArrDep == 0)
		rlColumnData.Text = prlLine->ADate;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DDate;
	olColList.NewAt(olColList.GetSize(), rlColumnData);
	  
	  
	rlColumnData.Alignment = COLALIGN_LEFT;
 	if (imArrDep == 0)
		rlColumnData.Text = prlLine->ATtyp;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DTtyp;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Alc3;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

		
  
  }

}





int ReportArrDepLoadPaxTableViewer::CompareFlight(SAMEARRDEPLOADPAXTABLE_LINEDATA *prpLine1, SAMEARRDEPLOADPAXTABLE_LINEDATA *prpLine2)
{
	int	ilCompareResult;
	CTime olTime1;
	CTime olTime2;

	if (prpLine1->Adid == "A")
	{
		olTime1 = prpLine1->AStoa;
		olTime2 = prpLine2->AStoa;
	}
	else
	{
		olTime1 = prpLine1->DStod;
		olTime2 = prpLine2->DStod;
	}

	ilCompareResult = (olTime1 == olTime2)? 0:
			(olTime1 > olTime2)? 1: -1;

	return ilCompareResult;
}



//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------


void ReportArrDepLoadPaxTableViewer::GetHeader()
{
	
	omPrintHeadHeaderArray.DeleteAll();  // BWi

	TABLE_HEADER_COLUMN rlHeader;
 
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 60; 
	rlHeader.Font = &ogCourier_Bold_8;
	rlHeader.Text = GetString(IDS_STRING1078);//FLIGHT
	omPrintHeadHeaderArray.New(rlHeader);

 
	rlHeader.Length = 45; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING298);// ORG
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING315);// DES
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = 35; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING323);//STA
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING316);//STD
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = 35; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING304);//ATA
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING321);//ATD
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = 55; 
 	rlHeader.Text = GetString(IDS_STRING311);//A/C
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_STRING310);//REGN
	omPrintHeadHeaderArray.New(rlHeader);

 	if (imArrDep == 0)
	{
		rlHeader.Length = 25;
		rlHeader.Text = GetString(IDS_STRING308);//POS
	}
	if (imArrDep == 1)
	{
		rlHeader.Length = 70;
		rlHeader.Text = GetString(IDS_STRING1641);//Cki-Counter
	}
 	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_STRING1406);//Gate
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = 25; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING1151);//Belt
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING308);//Pos
	omPrintHeadHeaderArray.New(rlHeader);

	// Load & Pax
 	rlHeader.Length = 20; 
 	rlHeader.Text = GetString(IDS_FIRSTCLASS); //F
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_ECONCLASS); //C
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_BUSICLASS); //Y
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 20; 
 	rlHeader.Text = GetString(IDS_INFANTS); //INF
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 30; 
 	rlHeader.Text = GetString(IDS_PAXTTL); // P/TTL
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_ID); //ID
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_TRANSIT); //TR
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_TRANSFER); //TRF
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 40; 
 	rlHeader.Text = GetString(IDS_BAGGAGE); //BAG
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 40; 
 	rlHeader.Text = GetString(IDS_CARGO); //CGO
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 40; 
 	rlHeader.Text = GetString(IDS_MAIL); //MAIL
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 40; 
 	rlHeader.Text = GetString(IDS_LOADTTL); // L/TTL
	omPrintHeadHeaderArray.New(rlHeader); 

	if(bgReports)
  {		
	
	rlHeader.Length = 50; 
 	rlHeader.Text = GetString(IDS_STRING332); // Date
	omPrintHeadHeaderArray.New(rlHeader);	
	
	
	rlHeader.Length = 18; 
 	rlHeader.Text = GetString(IDS_STRING2786); // NA
	omPrintHeadHeaderArray.New(rlHeader);

	rlHeader.Length = 18; 
 	rlHeader.Text = GetString(IDS_STRING2791); // ALC
	omPrintHeadHeaderArray.New(rlHeader);
	
	
 		
	}	

}




void ReportArrDepLoadPaxTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];

	sprintf(pclFooter, omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	omFooterName = pclFooter;
	CString olTableName = GetString(REPORTS_RB_LoadandPaxReport);
	if (bmUser)
		olTableName = GetString(REPORTS_RB_LoadandPaxReportUser);

	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	pomPrint->imMaxLines = 38;  // (P=57,L=38)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			//rlDocInfo.lpszDocName = TABLENAME;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = imFlightCount;//omLines.GetSize();  

			olFooter1.Format("%s -   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );
			if(bgReports)
			{
			olFooter1.Format("%s -   %s-   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pcgUser );
			}
			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ReportArrDepLoadPaxTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;

	pomPrint->PrintUIFHeader("",omTableName,pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omPrintHeadHeaderArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ReportArrDepLoadPaxTableViewer::PrintTableLine(SAMEARRDEPLOADPAXTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	if(bgReports)
	{
			rlElement.pFont      = &pomPrint->ogCourierNew_Regular_6;
	}
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;;
		}
		rlElement.FrameLeft  = PRINT_FRAMETHIN;
		rlElement.FrameRight = PRINT_FRAMETHIN;
	if(!bgReports)
	{
		switch(i)
		{
			case 0:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AFlno;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DFlno;
			}
			break;
			case 1:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AOrg3;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DDes3;
			}
			break;
			case 2:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AStoa.Format("%H:%M");
				if (imArrDep == 1)
					rlElement.Text = prpLine->DStod.Format("%H:%M");
			}
			break;
			case 3:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->ALand.Format("%H:%M");
				if (imArrDep == 1)
					rlElement.Text = prpLine->DAirb.Format("%H:%M");
			}
			break;
			case 4:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AAct;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DAct;
			}
			break;
			case 5:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->ARegn;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DRegn;
			}
			break;
			case 6:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->APsta;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DCkic;
			}
			break;
			case 7:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AGta1;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DGtd1;
			}
			break;
			case 8:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->ABlt1;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DPstd;
			}
			break;
			case 9:
			{
				rlElement.Text = prpLine->Pax1;
			}
			break;
			case 10:
			{
				rlElement.Text = prpLine->Pax2;
			}
			break;
			case 11:
			{
				rlElement.Text = prpLine->Pax3;
			}
			break;
			case 12:
			{
				rlElement.Text = prpLine->PaxInf;
			}
			break;
			case 13:
			{
				rlElement.Text = prpLine->Paxt;
			}
			break;
			case 14:
			{
				rlElement.Text = prpLine->PaxId;
			}
			break;
			case 15:
			{
				rlElement.Text = prpLine->Paxi;
			}
			break;
			case 16:
			{
				rlElement.Text = prpLine->Paxf;
			}
			break;
			case 17:
			{
				rlElement.Text = prpLine->Bagn;
			}
			break;
			case 18:
			{
				rlElement.Text = prpLine->Cgot;
			}
			break;
			case 19:
			{
				rlElement.Text = prpLine->Mail;
			}
			break;
			case 20:
			{
				rlElement.Text = prpLine->Bagw;
				rlElement.FrameRight  = PRINT_FRAMETHIN;
			}
			break;
		}
}
		
		
	if(bgReports)
	{
			switch(i)
		{
			case 0:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AFlno;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DFlno;
			}
			break;
			case 1:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AOrg3;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DDes3+"/"+prpLine->DDes4;
			}
			break;
			case 2:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AStoa.Format("%H:%M");
				if (imArrDep == 1)
					rlElement.Text = prpLine->DStod.Format("%H:%M");
			}
			break;
			case 3:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->ALand.Format("%H:%M");
				if (imArrDep == 1)
					rlElement.Text = prpLine->DAirb.Format("%H:%M");
			}
			break;
			case 4:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AAct+"/"+prpLine->Act5;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DAct+"/"+prpLine->Act5;
			}
			break;
			case 5:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->ARegn;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DRegn;
			}
			break;
			case 6:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->APsta;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DCkic;
			}
			break;
			case 7:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AGta1;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DGtd1;
			}
			break;
			case 8:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->ABlt1;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DPstd;
			}
			break;
			case 9:
			{
				rlElement.Text = prpLine->Pax1;
			}
			break;
			case 10:
			{
				rlElement.Text = prpLine->Pax2;
			}
			break;
			case 11:
			{
				rlElement.Text = prpLine->Pax3;
			}
			break;
			case 12:
			{
				rlElement.Text = prpLine->PaxInf;
			}
			break;
			case 13:
			{
				rlElement.Text = prpLine->Paxt;
			}
			break;
			case 14:
			{
				rlElement.Text = prpLine->PaxId;
			}
			break;
			case 15:
			{
				rlElement.Text = prpLine->Paxi;
			}
			break;
			case 16:
			{
				rlElement.Text = prpLine->Paxf;
			}
			break;
			case 17:
			{
				rlElement.Text = prpLine->Bagn;
			}
			break;
			case 18:
			{
				rlElement.Text = prpLine->Cgot;
			}
			break;
			case 19:
			{
				rlElement.Text = prpLine->Mail;
			}
			break;
			case 20:
			{
				rlElement.Text = prpLine->Bagw;
			//	rlElement.FrameRight  = PRINT_FRAMETHIN;
			}
			break;
			
			case 21:
				{
					if (imArrDep == 0)
					rlElement.Text = prpLine->ADate;
					if (imArrDep == 1)
					rlElement.Text= prpLine->DDate;
					
				}
			break;
			case 22:
				{
					if (imArrDep == 0)
					rlElement.Text = prpLine->ATtyp;
					if (imArrDep == 1)
					rlElement.Text = prpLine->DTtyp;
				}
			break;
			case 23:
				{
					rlElement.Text = prpLine->Alc3;
					rlElement.FrameRight  = PRINT_FRAMETHIN;
				}
		
			break;
		}

	}
		
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
bool ReportArrDepLoadPaxTableViewer::PrintPlanToFile(char *opTrenner)
{
	ofstream of;
	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

 
 if(!bgReports)
 {
	if (imArrDep == 0)
	{
		of	<< GetString(IDS_STRING1078)
		<< setw(1) << opTrenner
			<< GetString(IDS_STRING298)
		<< setw(1) << opTrenner
			<< GetString(IDS_STRING323)
		<< setw(1) << opTrenner
			<< GetString(IDS_STRING304)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING311)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING310)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING308)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING1406)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING1151)
		<< setw(1) << opTrenner
		   << GetString(IDS_FIRSTCLASS)
		<< setw(1) << opTrenner
		   << GetString(IDS_ECONCLASS)
		<< setw(1) << opTrenner
		   << GetString(IDS_BUSICLASS)
		<< setw(1) << opTrenner
		   << GetString(IDS_INFANTS)
		<< setw(1) << opTrenner
		   << GetString(IDS_PAXTTL)
		<< setw(1) << opTrenner
		   << GetString(IDS_ID)
		<< setw(1) << opTrenner
		   << GetString(IDS_TRANSIT)
		<< setw(1) << opTrenner
		   << GetString(IDS_TRANSFER)
		<< setw(1) << opTrenner
		   << GetString(IDS_BAGGAGE)
		<< setw(1) << opTrenner
		   << GetString(IDS_CARGO)
		<< setw(1) << opTrenner
		   << GetString(IDS_MAIL)
		<< setw(1) << opTrenner
		   << GetString(IDS_LOADTTL)
		  
		   << endl;
	}

	if (imArrDep == 1)
	{
		of << GetString(IDS_STRING1078)
		<< setw(1) << opTrenner
			<< GetString(IDS_STRING315)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING316)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING321)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING311)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING310)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING1641)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING1406)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING308)
		<< setw(1) << opTrenner
		   << GetString(IDS_FIRSTCLASS)
		<< setw(1) << opTrenner
		   << GetString(IDS_ECONCLASS)
		<< setw(1) << opTrenner
		   << GetString(IDS_BUSICLASS)
		<< setw(1) << opTrenner
		   << GetString(IDS_INFANTS)
		<< setw(1) << opTrenner
		   << GetString(IDS_PAXTTL)
		<< setw(1) << opTrenner
		   << GetString(IDS_ID)
		<< setw(1) << opTrenner
		   << GetString(IDS_TRANSIT)
		<< setw(1) << opTrenner
		   << GetString(IDS_TRANSFER)
		<< setw(1) << opTrenner
		   << GetString(IDS_BAGGAGE)
		<< setw(1) << opTrenner
		   << GetString(IDS_CARGO)
		<< setw(1) << opTrenner
		   << GetString(IDS_MAIL)
		<< setw(1) << opTrenner
		   << GetString(IDS_LOADTTL)
		   << endl;
	}

	CStringArray olDaten;
	int ilCount = imFlightCount;//omLines.GetSize();


	for(int i = 0; i < ilCount; i++)
	{
		SAMEARRDEPLOADPAXTABLE_LINEDATA rlD = omLines[i];

		if (imArrDep == 0)
		{
			olDaten.Add(rlD.AFlno);
			olDaten.Add(rlD.AOrg3);
			olDaten.Add(rlD.AStoa.Format("%H:%M"));
			olDaten.Add(rlD.ALand.Format("%H:%M"));
			olDaten.Add(rlD.AAct);
			olDaten.Add(rlD.ARegn);
			olDaten.Add(rlD.APsta);
			olDaten.Add(rlD.AGta1);
			olDaten.Add(rlD.ABlt1);
		}
		if (imArrDep == 1)
		{
			olDaten.Add(rlD.DFlno);
			olDaten.Add(rlD.DDes3);
			olDaten.Add(rlD.DStod.Format("%H:%M"));
			olDaten.Add(rlD.DAirb.Format("%H:%M"));
			olDaten.Add(rlD.DAct);
			olDaten.Add(rlD.DRegn);
			olDaten.Add(rlD.DCkic);
			olDaten.Add(rlD.DGtd1);
			olDaten.Add(rlD.DPstd);
		}
		// load & pax
		olDaten.Add(rlD.Pax1);
		olDaten.Add(rlD.Pax2);
		olDaten.Add(rlD.Pax3);
		olDaten.Add(rlD.PaxInf);
		olDaten.Add(rlD.Paxt);
		olDaten.Add(rlD.PaxId);
		olDaten.Add(rlD.Paxi);
		olDaten.Add(rlD.Paxf);
		olDaten.Add(rlD.Bagn);
		olDaten.Add(rlD.Cgot);
		olDaten.Add(rlD.Mail);
		olDaten.Add(rlD.Bagw);

		if (imArrDep == 0)
		{
			of.setf(ios::left, ios::adjustfield);
			of   << setw(1) << olDaten[0].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[1].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[2].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[3].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[4].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[5].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[6].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[7].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[8].GetBuffer(0)
				<< setw(1) << opTrenner;
		}		
		if (imArrDep == 1)
		{
			of.setf(ios::left, ios::adjustfield);
			of   << setw(1) << olDaten[0].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[1].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[2].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[3].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[4].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[5].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[6].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[7].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[8].GetBuffer(0)
				<< setw(1) << opTrenner;
		}		
		// load & pax
		of	<< setw(1) << olDaten[9].GetBuffer(0)
			<< setw(1) << opTrenner
			<< setw(1) << olDaten[10].GetBuffer(0)
			<< setw(1) << opTrenner
			<< setw(1) << olDaten[11].GetBuffer(0)
			<< setw(1) << opTrenner
			<< setw(1) << olDaten[12].GetBuffer(0)
			<< setw(1) << opTrenner
			<< setw(1) << olDaten[13].GetBuffer(0)
			<< setw(1) << opTrenner
			<< setw(1) << olDaten[14].GetBuffer(0)
			<< setw(1) << opTrenner
			<< setw(1) << olDaten[15].GetBuffer(0)
			<< setw(1) << opTrenner
			<< setw(1) << olDaten[16].GetBuffer(0)
			<< setw(1) << opTrenner
			<< setw(1) << olDaten[17].GetBuffer(0)
			<< setw(1) << opTrenner
			<< setw(1) << olDaten[18].GetBuffer(0)
			<< setw(1) << opTrenner
			<< setw(1) << olDaten[19].GetBuffer(0)
			<< setw(1) << opTrenner
			<< setw(1) << olDaten[20].GetBuffer(0);
		of << endl;
		
		olDaten.RemoveAll();
	}
}	
if(bgReports)
{	

if (imArrDep == 0)
	{
		of	<< GetString(IDS_STRING1078)
		<< setw(1) << opTrenner
			<< GetString(IDS_STRING298)
		<< setw(1) << opTrenner
			<< GetString(IDS_STRING323)
		<< setw(1) << opTrenner
			<< GetString(IDS_STRING304)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING311)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING310)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING308)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING1406)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING1151)
		<< setw(1) << opTrenner
		   << GetString(IDS_FIRSTCLASS)
		<< setw(1) << opTrenner
		   << GetString(IDS_ECONCLASS)
		<< setw(1) << opTrenner
		   << GetString(IDS_BUSICLASS)
		<< setw(1) << opTrenner
		   << GetString(IDS_INFANTS)
		<< setw(1) << opTrenner
		   << GetString(IDS_PAXTTL)
		<< setw(1) << opTrenner
		   << GetString(IDS_ID)
		<< setw(1) << opTrenner
		   << GetString(IDS_TRANSIT)
		<< setw(1) << opTrenner
		   << GetString(IDS_TRANSFER)
		<< setw(1) << opTrenner
		   << GetString(IDS_BAGGAGE)
		<< setw(1) << opTrenner
		   << GetString(IDS_CARGO)
		<< setw(1) << opTrenner
		   << GetString(IDS_MAIL)
		<< setw(1) << opTrenner
		   << GetString(IDS_LOADTTL)
		  << setw(1) << opTrenner
		  << GetString(IDS_STRING332)
		   << setw(1) << opTrenner
		  << GetString(IDS_STRING2786)
		   << setw(1) << opTrenner
		  << GetString(IDS_STRING2791)
		   << endl;
	}

	if (imArrDep == 1)
	{
		of << GetString(IDS_STRING1078)
		<< setw(1) << opTrenner
			<< GetString(IDS_STRING315)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING316)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING321)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING311)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING310)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING1641)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING1406)
		<< setw(1) << opTrenner
		   << GetString(IDS_STRING308)
		<< setw(1) << opTrenner
		   << GetString(IDS_FIRSTCLASS)
		<< setw(1) << opTrenner
		   << GetString(IDS_ECONCLASS)
		<< setw(1) << opTrenner
		   << GetString(IDS_BUSICLASS)
		<< setw(1) << opTrenner
		   << GetString(IDS_INFANTS)
		<< setw(1) << opTrenner
		   << GetString(IDS_PAXTTL)
		<< setw(1) << opTrenner
		   << GetString(IDS_ID)
		<< setw(1) << opTrenner
		   << GetString(IDS_TRANSIT)
		<< setw(1) << opTrenner
		   << GetString(IDS_TRANSFER)
		<< setw(1) << opTrenner
		   << GetString(IDS_BAGGAGE)
		<< setw(1) << opTrenner
		   << GetString(IDS_CARGO)
		<< setw(1) << opTrenner
		   << GetString(IDS_MAIL)
		<< setw(1) << opTrenner
			<< GetString(IDS_LOADTTL)
		<< setw(1) << opTrenner  
			<< GetString(IDS_STRING332)
		<< setw(1) << opTrenner
		  << GetString(IDS_STRING2786)
		<< setw(1) << opTrenner
		  << GetString(IDS_STRING2791)
		   << endl;
	}

	CStringArray olDaten;
	int ilCount = imFlightCount;//omLines.GetSize();
	
	for(int i = 0; i < ilCount; i++)
	{
		SAMEARRDEPLOADPAXTABLE_LINEDATA rlD = omLines[i];
			

		if (imArrDep == 0)
		{
			rlD.AAct=rlD.AAct+"/"+rlD.Act5;
			olDaten.Add(rlD.AFlno);
			olDaten.Add(rlD.AOrg3);
			olDaten.Add(rlD.AStoa.Format("%H:%M"));
			olDaten.Add(rlD.ALand.Format("%H:%M"));
			olDaten.Add(rlD.AAct);
			olDaten.Add(rlD.ARegn);
			olDaten.Add(rlD.APsta);
			olDaten.Add(rlD.AGta1);
			olDaten.Add(rlD.ABlt1);
			olDaten.Add(rlD.Pax1);
			olDaten.Add(rlD.Pax2);
			olDaten.Add(rlD.Pax3);
			olDaten.Add(rlD.PaxInf);
			olDaten.Add(rlD.Paxt);
			olDaten.Add(rlD.PaxId);
			olDaten.Add(rlD.Paxi);
			olDaten.Add(rlD.Paxf);
			olDaten.Add(rlD.Bagn);
			olDaten.Add(rlD.Cgot);
			olDaten.Add(rlD.Mail);
			olDaten.Add(rlD.Bagw);
			olDaten.Add(rlD.ADate);
			olDaten.Add(rlD.ATtyp);
			olDaten.Add(rlD.Alc3);
		
		}
		if (imArrDep == 1)
		{
			rlD.DAct=rlD.DAct+"/"+rlD.Act5;
			rlD.DDes3=rlD.DDes3+"/"+rlD.Des4;
			olDaten.Add(rlD.DFlno);
			olDaten.Add(rlD.DDes3);
			olDaten.Add(rlD.DStod.Format("%H:%M"));
			olDaten.Add(rlD.DAirb.Format("%H:%M"));
			olDaten.Add(rlD.DAct);
			olDaten.Add(rlD.DRegn);
			olDaten.Add(rlD.DCkic);
			olDaten.Add(rlD.DGtd1);
			olDaten.Add(rlD.DPstd);
			olDaten.Add(rlD.Pax1);
			olDaten.Add(rlD.Pax2);
			olDaten.Add(rlD.Pax3);
			olDaten.Add(rlD.PaxInf);
			olDaten.Add(rlD.Paxt);
			olDaten.Add(rlD.PaxId);
			olDaten.Add(rlD.Paxi);
			olDaten.Add(rlD.Paxf);
			olDaten.Add(rlD.Bagn);
			olDaten.Add(rlD.Cgot);
			olDaten.Add(rlD.Mail);
			olDaten.Add(rlD.Bagw);
			olDaten.Add(rlD.DDate);
			olDaten.Add(rlD.DTtyp);
			olDaten.Add(rlD.Alc3);
		
		}
		// load & pax
		

		if (imArrDep == 0)
		{
			of.setf(ios::left, ios::adjustfield);
			of   << setw(1) << olDaten[0].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[1].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[2].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[3].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[4].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[5].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[6].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[7].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[8].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[9].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[10].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[11].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[12].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[13].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[14].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[15].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[16].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[17].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[18].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[19].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[20].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[21].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[22].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[23].GetBuffer(0);
				of << endl;
		
		}		
		if (imArrDep == 1)
		{
			of.setf(ios::left, ios::adjustfield);
			of   << setw(1) << olDaten[0].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[1].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[2].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[3].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[4].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[5].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[6].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[7].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[8].GetBuffer(0)
				<< setw(1) << opTrenner
				<< setw(1) << olDaten[9].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[10].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[11].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[12].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[13].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[14].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[15].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[16].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[17].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[18].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[19].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[20].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[21].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[22].GetBuffer(0)
				<< setw(1) << opTrenner
				 << setw(1) << olDaten[23].GetBuffer(0);
				of << endl;
		
		
		}		
		
	
		
		olDaten.RemoveAll();
	}
	


}
	


	of.close();
	return true;
}
