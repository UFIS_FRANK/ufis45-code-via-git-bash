#if !defined(AFX_ROTATIONJOINDLG_H__712D9FB1_6192_11D1_831E_0080AD1DC701__INCLUDED_)
#define AFX_ROTATIONJOINDLG_H__712D9FB1_6192_11D1_831E_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RotationJoinDlg.h : header file
//

#include <RotationDlgCedaFlightData.h>
#include <CCSEdit.h>
#include <CCSTime.h>

/////////////////////////////////////////////////////////////////////////////
// RotationJoinDlg dialog

class RotationJoinDlg : public CDialog
{
// Construction
public:
	RotationJoinDlg(CWnd* pParent, ROTATIONDLGFLIGHTDATA *prpFlight, char cpFPart, bool bpLocalTime);   // standard constructor

	~RotationJoinDlg();

	void AppExit();
	bool bmLocalTime;

// Dialog Data
	//{{AFX_DATA(RotationJoinDlg)
	enum { IDD = IDD_ROTATION_JOIN };
	CCSEdit	m_CE_Sto;
	CCSEdit	m_CE_Fltn;
	CCSEdit	m_CE_Flns;
	CCSEdit	m_CE_Alc3;
	CCSEdit	m_CE_Stev;
	CString	m_Alc3;
	CString	m_Flns;
	CString	m_Fltn;
	CString	m_Sto;
	CString	m_Stev;
	//}}AFX_DATA
	
	ROTATIONDLGFLIGHTDATA *prmFlight;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationJoinDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationJoinDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	char cmFPart;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONJOINDLG_H__712D9FB1_6192_11D1_831E_0080AD1DC701__INCLUDED_)
