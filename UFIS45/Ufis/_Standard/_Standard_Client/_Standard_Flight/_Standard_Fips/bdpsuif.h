// Machine generated IDispatch wrapper class(es) created with ClassWizard
/////////////////////////////////////////////////////////////////////////////
// IApplication wrapper class

class IApplication : public COleDispatchDriver
{
public:
	IApplication() {}		// Calls COleDispatchDriver default constructor
	IApplication(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IApplication(const IApplication& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:
	CString GetUsername();
	void SetUsername(LPCTSTR);
	CString GetPassword();
	void SetPassword(LPCTSTR);
	CString GetResourceTable();
	void SetResourceTable(LPCTSTR);
	long GetResourceUrno();
	void SetResourceUrno(long);
	CString GetCaller();
	void SetCaller(LPCTSTR); 

// Operations
public:
	BOOL SetCmdLine(LPCTSTR pszCmdLine);
	BOOL DisplayResource();
};
