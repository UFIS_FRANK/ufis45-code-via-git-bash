#ifndef _CCOLOR
#define _CCOLOR

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// CColor.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GanttBarReport window

struct CColor
{
	unsigned int r;
	unsigned int g;
	unsigned int b;

	CColor(void)
	{
		r = 0;
		g = 0;
		b = 0;
	}
};

#endif