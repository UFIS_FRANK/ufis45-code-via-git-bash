#ifndef _REPORT_ACT_CEDA_DATA_H_
#define _REPORT_ACT_CEDA_DATA_H_
 

#include <CCSGlobl.h>
#include <CCSDefines.h>




struct REPORTACTDATA
{
	char	Act3[5];	// 3-Letter Code
//	char	Acfn[34];	// Flugzeug-Bezeichnung
	char	Acfn[56];	// Flugzeug-Bezeichnung

	//DataCreated by this class
	int		IsChanged;

	REPORTACTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}

};
	


class ReportActCedaData : public CCSCedaData
{
// Attributes
public:
	char pcmAftFieldList[64];


// Operations
public:
    ReportActCedaData();
	~ReportActCedaData();
	

	CString ReadAcfn(CString opTtyp);

private:
	bool bmReadAllFlights;
	bool bmRotation;

};


#endif
