// HEADER 
#ifndef _CEDAFLZDATA_H_ 
#define _CEDAFLZDATA_H_ 

#include <CCSCedaData.h> 
//#include <CedaCcaData.h>

struct CCADATA;

struct FLZDATA 
{
	long 	 Rurn; 	
	long 	 Flgu; 	
	long 	 Urno; 	
	char 	 Utyp[2]; 	
	char 	 Lnam[6]; 	
	char 	 Lseq[2]; 	

	//DataCreated by this class
	int      IsChanged;

	bool	IsUsed;

	FLZDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		IsUsed = true;
	}

}; // end FLZDATA

class CedaFlzData : public CCSCedaData
{
public:
	CedaFlzData();
	~CedaFlzData();

	void Register(void);
	void UnRegister(void);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool IsPassFilter(FLZDATA *prpFlz);

    CMapPtrToPtr omUrnoMap;
	CString omRurns;

	CCSPtrArray<FLZDATA> omData;
	char pcmFList[512];
	
	void Read( CString  opRurnList, bool bpAdd = false);
	void Clear();

	bool ReadFlnus(const CString &ropFlnus);


	FLZDATA *GetFlzByUrno(  long lpAseq);

	CString GetGatFlgu(CString opLseq);
	CString GetCicLogoName(long lpCcaUrno);


	bool SaveGatLogo(long lpFlightUrno, CString opGatFlgu, CString opGatFlgu2);

	bool SaveCicLogo(CCSPtrArray<CCADATA> &ropCca, CStringArray &omCicLogoArray);


	//////////////////////////////////////

	bool Save(FLZDATA *prpStp);

	void DeleteInternal(FLZDATA *prpStp);
	void InsertInternal(FLZDATA *prpStp);
	bool UpdateInternal(FLZDATA *prpStp);
};

extern CedaFlzData ogFlzData;

#endif // _CEDAFLZDATA_H_ 
