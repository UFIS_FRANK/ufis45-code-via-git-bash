// TimeRange.cpp: implementation of the CTimeRange class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "fpms.h"
#include "TimeRange.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimeRange::CTimeRange()
{
	StartTime = -1 ;
	EndTime = -1 ;
};	// constructor

CTimeRange::~CTimeRange()
{
};	// destructor

// create a time range with sorted times
CTimeRange::CTimeRange( const CTime &start, const CTime &end )
{
	if( start <= end )
	{
		StartTime = start ;
		EndTime = end ;
	}
	else
	{
		StartTime = end ;
		EndTime = start ;
	}
};	// constructor

bool CTimeRange::IsInRange( const CTime &time )
{
	return StartTime <= time && time <= EndTime ;
};	// IsInRange

// completely within ?
bool CTimeRange::IsInRange( const CTimeRange &range )
{
#ifdef _MVyDEBUG
	TRACE( "CTimeRange::IsInRange  %s <= %s  &  %s <= %s\n"
		, StartTime.Format( "%c" )
		,	range.StartTime.Format( "%c" )
		, range.EndTime.Format( "%c" )
		, EndTime.Format( "%c" )
	);
#endif	// _MVyDEBUG

	return StartTime <= range.StartTime && range.EndTime <= EndTime ;
};	// IsInRange

// some overlapping ?
bool CTimeRange::IsOverlapping( const CTimeRange &range )
{
	return StartTime <= range.EndTime && range.StartTime <= EndTime ;
};	// IsOverlapping

// this can only occure on both start and end times are equal
bool CTimeRange::IsEmpty()
{
#pragma warning( disable : 4800 )
	return StartTime == EndTime ;		// I DON�T LIKE "PERFORMANCE" WARNINGS !
#pragma warning( default : 4800 )
};	// IsEmpty

// return the timerange that belongs to both ranges
CTimeRange CTimeRange::Clip( CTimeRange &range )
{
	CTime start = IsInRange( range.StartTime ) ? range.StartTime : range.IsInRange( StartTime ) ? StartTime : (CTime )-1 ;
	CTime end = IsInRange( range.EndTime ) ? range.EndTime : range.IsInRange( EndTime ) ? EndTime : (CTime )-1 ;
	ASSERT( ( start == -1 ) == ( end == -1 ) );
	CTimeRange clipped( start, end );
	return clipped ;
};	// Clip

CString CTimeRange::Dump()
{
	CTimeSpan span = GetTimeSpan();
	CString text ;
	text.Format( "%s  <%d:%d>  %s", 
		StartTime.Format( "%c" ), 
		span.GetTotalHours(), span.GetMinutes(), 
		EndTime.Format( "%c" ) );
	return text ;
};	// Dump

// conversion helper
CTimeSpan CTimeRange::GetTimeSpan()
{
	return EndTime - StartTime ;
};	// GetTimeSpan

// says if the end is before the beginning
bool CTimeRange::IsReverse()
{
	if(EndTime < StartTime)
		return true;
	else
		return false;

};	// IsReverse