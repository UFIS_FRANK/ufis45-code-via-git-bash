#if !defined(AFX_GANTTCHARTREPORT_H__004960BF_0881_11D3_AB42_00001C019D0B__INCLUDED_)
#define AFX_GANTTCHARTREPORT_H__004960BF_0881_11D3_AB42_00001C019D0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GanttChartReport.h : header file
//
#include <CColor.h>
#include <CCSPtrArray.h>
#include <CCSPrint.h>

/////////////////////////////////////////////////////////////////////////////
// GanttChartReport window

class GanttBarReport;

class GanttChartReport : public CScrollView
{
// Construction
public:
	GanttChartReport(CWnd* ppParent, CRect opRect, CString opXAxe, CString opYAxe, 
		             CColor opColor);
	GanttChartReport();
	DECLARE_DYNCREATE(GanttChartReport)

	virtual ~GanttChartReport();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GanttChartReport)
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetData(const CUIntArray &opData);
	void SetXAxeText(const CStringArray& opXAxeText);
	void SetBarText(const CStringArray& opBarText);
	int GetGanttBarWidth();
	int GetGanttBarWidthPrint();
	int GetGanttYBeginn();
	int GetGanttYBeginnPrint();
	CFont* GetGanttChartFont();
	void Print(CDC* popDC, CCSPrint* popPrint, CString opHeader, CString opFooterLeft);

	//Added new functions for the PRF 8494 by sisl
	void SetArrivalData(const CUIntArray &opData);
	void SetDepartureData(const CUIntArray &opData);
	int imArrivalHeight;
	int imArrivalData ;
	int imDeparture ;
	// Generated message map functions
protected:
	//{{AFX_MSG(GanttChartReport)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	void SetHighAndLowData();
	int GetBarHeight(int ipIndex);
	int GetBarHeightPrint(int ipIndex);

	// PRINTING
	void PrintHeader(CCSPrint* popPrint, CString opHeader);
	void PrintFooter(CCSPrint* popPrint, CString opFooter, int ipPageNo);
	void PrintCoordinateSystem(CDC* popDC);
	void PrintBars(CDC* popDC, CCSPrint* popPrint);

	int GetArrivalBarHeight(int ipIndex);//PRF 8494
	int GetArrivalBarHeightPrint(int ipIndex);//PRF 8494

private:
	CRect omRectWnd;
	CRect omRectPrn;
	CString omXAxe;
	CString omYAxe;
	CStringArray omXAxeText;
	CStringArray omBarText;
	CUIntArray omData;

	CUIntArray omArrivalData;// Added for the PRF 8494 by sisl 
	CUIntArray omDepartureData;
	
	CColor omColor;
	int imDataCount;
	CCSPtrArray <GanttBarReport> omBars;
	int imHigh;
	int imLow;
	double dmFactorX;
	double dmFactorY;
	CString omHeader;
	CString omFooterLeft;
	int imBarWidth;
	int imSizeX;
	CFont omArial_7;

	DECLARE_MESSAGE_MAP()


private:
	BYTE lmCharSet;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GANTTCHARTREPORT_H__004960BF_0881_11D3_AB42_00001C019D0B__INCLUDED_)
