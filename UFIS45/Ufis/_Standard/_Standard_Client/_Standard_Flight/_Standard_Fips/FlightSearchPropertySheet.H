// FlightSearchPropertySheet.H : header file
//


#ifndef _FLIGHTSEARCHPROPERTYSHEET_H_
#define _FLIGHTSEARCHPROPERTYSHEET_H_


#include <BasePropertySheet.h>
#include <PSUniFilterPage.h>
#include <PSSearchFlightPage.h>
//#include "PSunisortpage.h"
//#include "PSSortFlightPage.h"
//#include "PSspecfilterpage.h"


/////////////////////////////////////////////////////////////////////////////
// FlightSearchPropertySheet

class FlightSearchPropertySheet : public BasePropertySheet
{
// Construction
public:
	FlightSearchPropertySheet(CString opCalledFrom, CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0,  LPCSTR pszCaption = NULL, bool bpLocalTimes = false);
	~FlightSearchPropertySheet();

// Attributes
public:
	//TestPage m_TestPage;
	//CSpecFilterPage m_SpecialFilterPage;
	CSearchFlightPage m_SearchFlightPage;
	CPsUniFilter m_PSUniFilter;
	//CPSUniSortPage m_PSUniSortPage;
	//CSortFlightPage m_SpecialSortPage;
/*	FilterPage m_pageAirline;
	FilterPage m_pageArrival;
	FilterPage m_pageDeparture;
	FlightTableBoundFilterPage m_pageBound;
	FlightTableSortPage m_pageSort;
	ZeitPage m_pageZeit;
*/
// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	//virtual int QueryForDiscardChanges();


};

/////////////////////////////////////////////////////////////////////////////

#endif // _FLIGHTSEARCHPROPERTYSHEET_H_
