// gatechrt.cpp : implementation file
//


#include <stdafx.h>
#include <CCSGlobl.h>
#include <resource.h>
#include <FPMS.h>
#include <CCSButtonCtrl.h>
#include <CCSClientWnd.h>
#include <RotationCedaFlightData.h>
#include <RotationTableViewer.h>
#include <RotationTables.h>
#include <RotationTableChart.h>
#include <CCSTable.h>
#include <PrivList.h>
#include <AskBox.h>
#include <resrc1.h>
#include <utils.h>
#include <CedaFlightUtilsData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RotationTableChart

IMPLEMENT_DYNCREATE(RotationTableChart, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

RotationTableChart::RotationTableChart(CWnd *popParent, int ipGroupID /*= -1*/, CString opGroupText, RotationTableViewer *popViewer /*= NULL*/)
{
	pomParent = popParent;
	pomViewer  = popViewer;
	imGroupID  = ipGroupID;
	omGroupText  = opGroupText;

    pomStatusBar = NULL;
	bmSet = FALSE;
	bmIsResizing = FALSE;
	imStartVerticalScalePos = 30;    
	imStartTopScaleTextPos = 120;
    pomTopScaleText = NULL;
	imTableHeight = 0;

}

// 050224 MVy: return the parent window
// 050427 MVy: PRF6984.3: method name changed from GetParent() to GetParentRotationTable()
RotationTables *RotationTableChart::GetParentRotationTable()
{
	ASSERT( pomParent );		// Warning: requestor may work with null window. This need not cause a crash.
	return (RotationTables *)pomParent ;
};	// GetParentRotationTable

// 050302 MVy: accessing table
CCSTable* RotationTableChart::GetTable()
{
	return &omTable ;
};	// GetTable

RotationTableChart::~RotationTableChart()
{
    if (pomTopScaleText != NULL)
        delete  pomTopScaleText;
}

BEGIN_MESSAGE_MAP(RotationTableChart, CFrameWnd)
    //{{AFX_MSG_MAP(RotationTableChart)
    ON_WM_CREATE()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_PAINT()
    ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
    ON_BN_CLICKED(IDC_CHARTBUTTON, OnChartButton)
    ON_BN_CLICKED(IDC_CHARTBUTTON2, OnChartButton2)
    ON_BN_CLICKED(IDC_CHARTBUTTON3, OnChartButton3)
    ON_BN_CLICKED(IDC_CHARTBUTTON4, OnChartButton4)
    ON_BN_CLICKED(IDC_CHARTBUTTON_TOWING, OnChartButtonTowing)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_DESTROY()
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
    ON_MESSAGE(WM_EDIT_LBUTTONDBLCLK, OnEditLButtonDblclk)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
	ON_MESSAGE(WM_TABLE_IPEDIT, OnTableIPEdit)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

int RotationTableChart::GetHeight()
{
    if (imState == Minimized)
        return imStartVerticalScalePos; // height of ChartButtton/TopScaleText
    else                                // (imState == Normal) || (imState == Maximized)
/////////////////////////////////////////////////////////////////////////////
// Id 18 Sep - Pichet's version uses "imHeight" which will be fixed by OnCreated().
// That's not correct, and it's the reason for scroll bars happenes in
// non-bottommost gantt chart of a diagram.
        //return imStartVerticalScalePos + imHeight + 2;
		return imStartVerticalScalePos + /*omTable.GetTableHeight()*/imTableHeight + 2;
/////////////////////////////////////////////////////////////////////////////
}

/////////////////////////////////////////////////////////////////////////////
// RotationTableChart message handlers

#include <CCSClientWnd.h>

int RotationTableChart::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
        return -1;

	omDragDropTarget.RegisterTarget(this, this);


    CString olButtonText; 
	
	if(imGroupID == UD_ARRIVAL)
		olButtonText = GetString(IDS_STRING1030);//"Anflug";
	if(imGroupID == UD_AGROUND)
		olButtonText = GetString(IDS_LANDED);//"Boden";
	if(imGroupID == UD_DGROUND)
		olButtonText = GetString(IDS_STRING1032);//"Boden";
	if(imGroupID == UD_DEPARTURE)
		olButtonText = GetString(IDS_AIRBORNE);//"Abflug";

		
	CRect olRect;
	GetClientRect(&olRect);
    

	CRect olRectButton(5, 2, 70, 22);
	CRect olRectTopScale(imStartTopScaleTextPos + 6, 2, (int)((olRect.right / 3) * 2) , 23);


    omButton.Create(olButtonText, BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton, this, IDC_CHARTBUTTON);
    omButton.SetFont(&ogMSSansSerif_Bold_8, FALSE);

	CRect olRectButtonMax(75, 2, 110, 22);
    
    omButtonMax.Create(GetString(IDS_STRING1035), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButtonMax, this, IDC_CHARTBUTTON3);
    omButtonMax.SetFont(&ogMSSansSerif_Bold_8, FALSE);




    m_ChartButtonDragDrop.RegisterTarget(&omButton, this);

    pomTopScaleText = new CCS3DStatic(TRUE);
    pomTopScaleText->Create("", WS_CHILD | WS_VISIBLE, olRectTopScale, this);




    m_TopScaleTextDragDrop.RegisterTarget(pomTopScaleText, this);


	CRect olRectButton2(5 + olRectTopScale.right, 2, 110 + olRectTopScale.right, 22);

	CRect olRectButton3(olRectButton2);

	olRectButton3.OffsetRect( olRectButton2.right - olRectButton2.left + 20 , 0);


	if(imGroupID == UD_ARRIVAL)
	{
		omButton2.Create(GetString(IDS_STRING1033), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_CHARTBUTTON2);
		omButton2.SetFont(&ogMSSansSerif_Bold_8, FALSE);
	}
	if(imGroupID == UD_DEPARTURE)
	{
		omButton2.Create(GetString(IDS_STRING556), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton3, this, IDC_CHARTBUTTON2);
		omButton2.SetFont(&ogMSSansSerif_Bold_8, FALSE);
	}

	if(imGroupID == UD_DEPARTURE) //return flight
	{
		omButton4.Create(GetString(IDS_STRING1697), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_CHARTBUTTON4);
		omButton4.SetFont(&ogMSSansSerif_Bold_8, FALSE);
	}


	if(imGroupID == UD_DGROUND) // return taxi
	{
		omButton2.Create(GetString(IDS_STRING1778), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_CHARTBUTTON2);
		omButton2.SetFont(&ogMSSansSerif_Bold_8, FALSE);
	}

	if(imGroupID == UD_AGROUND) // towing
	{
		omButtonTowing.Create(GetString(IDS_TOWING), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_CHARTBUTTON_TOWING);
		omButtonTowing.SetFont(&ogMSSansSerif_Bold_8, FALSE);
	}



	if(imGroupID == UD_ARRIVAL)
	{
		SetWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_TouchAndGo"),omButton2);
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CS_NextLand"),(CWnd *)pomTopScaleText);

		#ifdef _FIPSVIEWER
		{
			omButton2.EnableWindow(FALSE);
		}
		#else
		{
		}
		#endif
	}

	if(imGroupID == UD_AGROUND)
	{
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CS_NextOnbl"),(CWnd *)pomTopScaleText);
		SetWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_Towing"),omButtonTowing);

		#ifdef _FIPSVIEWER
		{
			//omButtonTowing.EnableWindow(FALSE);
		}
		#else
		{
		}
		#endif
	}

	if(imGroupID == UD_DGROUND)
	{
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CS_NextOfbl"),(CWnd *)pomTopScaleText);
		SetWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CS_ReturnTaxi"),omButton2);

		#ifdef _FIPSVIEWER
		{
			omButton2.EnableWindow(FALSE);
		}
		#else
		{
		}
		#endif
	}

	if(imGroupID == UD_DEPARTURE)
	{
		SetWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_Platzrunde"),omButton2);
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CS_NextAirb"),(CWnd *)pomTopScaleText);

		// return flight
		SetWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_ReturnFlight"),omButton4);

		#ifdef _FIPSVIEWER
		{
			omButton2.EnableWindow(FALSE);
			omButton4.EnableWindow(FALSE);
		}
		#else
		{
		}
		#endif
	}


    // read all data
    SetState(Maximized);
    

	imTableHeight = olRect.bottom - imStartTopScaleTextPos;
    omTable.SetTableData(this, olRect.left, olRect.right, imStartTopScaleTextPos/*imStartVerticalScalePos*/, olRect.bottom);

    omTable.imSelectMode = 0;
    omTable.SetHeaderSpacing(0);
	omTable.SetTableEditable(true);

//rkr26032001
//	omTable.SetIPEditModus(false);
	omTable.SetIPEditModus(!bgDailyWithAlt);


	pomViewer->AddGroup(imGroupID, omGroupText, &omTable, this, pomTopScaleText);
    return 0;
}






void RotationTableChart::OnDestroy() 
{
    m_ChartButtonDragDrop.Revoke();
    m_TopScaleTextDragDrop.Revoke();

	CFrameWnd::OnDestroy();
}



void RotationTableChart::OnSize(UINT nType, int cx, int cy) 
{
    CFrameWnd::OnSize(nType, cx, cy);
    
    CRect olClientRect; GetClientRect(&olClientRect);
    CRect olRect(imStartTopScaleTextPos + 6, 4 + 1, olClientRect.right - 6, (4 + 20) - 1);
    omTable.SetPosition(0, cx+1, olRect.bottom + 2, olClientRect.bottom );
	 if(bmSet == FALSE)
	 {
		bmSet = TRUE;
 		//RST!imTableHeight = omTable.GetTableHeight();
	 }
}

BOOL RotationTableChart::OnEraseBkgnd(CDC* pDC) 
{
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);

	lmBkColor = ogColors[6];
    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
}

void RotationTableChart::OnPaint()
{
    CPaintDC dc(this); 
    
    CPen *polOldPen = (CPen *) dc.SelectStockObject(BLACK_PEN);

    CRect olClientRect;
    GetClientRect(&olClientRect);
    
	#define imHorizontalPos imStartVerticalScalePos
	#define imVerticalPos imStartTopScaleTextPos

    // draw horizontal seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 2);
    dc.LineTo(olClientRect.right, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 1);
    dc.LineTo(olClientRect.right, imHorizontalPos - 1);
    //

    // draw vertical seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(imVerticalPos - 2, olClientRect.top);
    //dc.LineTo(imVerticalPos - 2, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 2, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(imVerticalPos - 1, olClientRect.top);
    //dc.LineTo(imVerticalPos - 1, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 1, imHorizontalPos - 2);
    //
	 if(bmIsResizing == TRUE)
	 {
		 olClientRect.top = olClientRect.bottom+2;
		 olClientRect.bottom--;
		 dc.DrawFocusRect(&olClientRect);
	 }

    dc.SelectObject(polOldPen);
}

void RotationTableChart::OnChartButton()
{
    if (imState == Minimized)
	 {
        SetState(Maximized);
 		omTable.ShowWindow(SW_NORMAL);
	 }
    else
	 {
        SetState(Minimized);
		omTable.ShowWindow(SW_HIDE);
	 }
        
    GetParent() -> GetParent() -> SendMessage(WM_POSITIONCHILD, 0, 0L);
}


void RotationTableChart::OnChartButton3()
{
	    pomParent->SendMessage(WM_MAXIMIZE_CHILD, 0, (LPARAM) this);
}


void RotationTableChart::OnChartButton4()
{
	    pomParent->SendMessage(WM_RETURNFLIGHT, 0, (LPARAM) this);
}



void RotationTableChart::OnChartButton2()
{
	// Touch and go
	if(imGroupID == UD_ARRIVAL)
	{
	    pomParent->SendMessage(WM_TOUCHANDGO, 0, 0L);
	}
	// Platzrunde
	if(imGroupID == UD_DEPARTURE)
	{
	    pomParent->SendMessage(WM_PLACEROUND, 0, 0L);

	}

	if(imGroupID == UD_DGROUND)
	{
	    pomParent->SendMessage(WM_RETURNTAXI, 0, 0L);
	}


}

void RotationTableChart::OnChartButtonTowing()
{
	if(imGroupID == UD_AGROUND)
	{
	    pomParent->SendMessage(WM_TOWING, 0, 0L);
	}

}


void RotationTableChart::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CFrameWnd::OnLButtonDown(nFlags, point);
	CRect olRect;
	GetClientRect(&olRect);
	CPaintDC olDC(this);

	if(bmIsResizing == FALSE)
	{
		if(point.y >= olRect.bottom-3) //(nFlags & MK_LBUTTON)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENS));
			bmIsResizing = TRUE;
		}
		else
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		}
	}
	else
	{
		GetClientRect(&olRect);
		olRect.top = point.y-1;
		olRect.bottom = point.y+1;
		olDC.DrawFocusRect(olRect);
	}
}


void RotationTableChart::OnMouseMove(UINT nFlags, CPoint point)
{
	CRect olRect;
	GetClientRect(&olRect);
	CPaintDC olDC(this);
	if(bmIsResizing == FALSE)
	{
		if(point.y >= olRect.bottom-3) //(nFlags & MK_LBUTTON)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENS));
		}
		else
		{
			 SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		}
	}
	else
	{
		if( nFlags & MK_LBUTTON )
		{
			GetClientRect(&olRect);
			olRect.top = point.y-5;
			olRect.bottom = point.y+5;
			olDC.DrawFocusRect(olRect);
			/*			
			olRect.bottom = point.y+2;
			olRect.right = olRect.right + 
			MoveWindow(&olRect);
			CRect olClientRect; GetClientRect(&olClientRect);
			CRect olRect2(imStartTopScaleTextPos + 6, 4 + 1, olClientRect.right - 6, (4 + 20) - 1);
			omTable.SetPosition(-1, olRect.left+1, olRect2.bottom + 3, olClientRect.bottom - 3);
			*/
		}
		else
		{
			bmIsResizing = FALSE;
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		}
	}
}

void RotationTableChart::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CRect olRect;
	GetClientRect(&olRect);
	if(point.y >= olRect.bottom-3)
	{
      SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENS));
	}
	else
	{
       SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
	bmIsResizing = FALSE;
	CFrameWnd::OnLButtonUp(nFlags, point);
}


void RotationTableChart::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CFrameWnd::OnLButtonDblClk(nFlags, point);
}



void RotationTableChart::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}



void RotationTableChart::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}





LONG RotationTableChart::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	if(pomParent != NULL)
		pomParent->SendMessage(WM_TABLE_LBUTTONDBLCLK, wParam, lParam);
	return 0L;
}

LONG RotationTableChart::OnEditLButtonDblclk( UINT wParam, LPARAM lParam)
{
	if(pomParent != NULL)
		pomParent->SendMessage(WM_EDIT_LBUTTONDBLCLK, wParam, lParam);
	return 0L;
}


LONG RotationTableChart::OnTableLButtonDown(UINT wParam, LONG lParam)
{
	if(pomParent != NULL)
		pomParent->SendMessage(WM_TABLE_LBUTTONDOWN, wParam, lParam);

	ROTATIONTABLE_LINEDATA *prpLine = (ROTATIONTABLE_LINEDATA*)omTable.GetTextLineData(wParam);
	
	BOOL blpPost = pomViewer->CheckPostFlight(prpLine);
	HandlePostFlight (blpPost);
	HandleWindowText (blpPost);

	return 0L;
}


LONG RotationTableChart::OnTableRButtonDown(UINT wParam, LONG lParam)
{
	if(pomParent != NULL)
		pomParent->SendMessage(WM_TABLE_RBUTTONDOWN, wParam, lParam);
	return 0L;

}


LONG RotationTableChart::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	if(pomParent != NULL)
		pomParent->SendMessage(WM_TABLE_IPEDIT_KILLFOCUS, wParam, lParam);
	return 0L;
}



////////////////////////////////////////////////////////////////////////////
// Dragbeginn einer Drag&Drop Aktion
//
LONG RotationTableChart::OnTableDragBegin(UINT wParam, LONG lParam)
{
	if(ogPrivList.GetStat("ROTATIONTAB_DD_Join") != '1')
		return 0L;

	
	//if(ogPrivList.GetStat("SEASONTAB_DragAndDrop") != '1')
	//	return 0L;
	int ilLine;

	ROTATIONTABLE_LINEDATA *prlLine = (ROTATIONTABLE_LINEDATA*)omTable.GetTextLineData(wParam);

	//postflighthandling
	BOOL blpPost = pomViewer->CheckPostFlight(prlLine);
	HandleWindowText (blpPost);
	if (blpPost)
		return 0L;

	CPoint point;
    ::GetCursorPos(&point);
    omTable.pomListBox->ScreenToClient(&point);    
    //ScreenToClient(&point);    

	if (prlLine != NULL)
	{
		ilLine = omTable.GetLinenoFromPoint(point);
	}
	else
		return 0L;


	ogBcHandle.BufferBc();

	ogDdx.DataChanged((void *)this, SHOW_FLIGHT, (void*)&(prlLine->Urno));


	omDragDropObject.CreateDWordData(DIT_FLIGHT, 1);
	omDragDropObject.AddDWord(prlLine->Urno);

	omDragDropObject.BeginDrag();
	ogBcHandle.ReleaseBuffer();

	return 0L;
}


////////////////////////////////////////////////////////////////////////////
// Dragover einer Drag&Drop Aktion, wo auch immer
//
LONG RotationTableChart::OnDragOver(UINT wParam, LONG lParam)
{

	if(ogPrivList.GetStat("ROTATIONTAB_DD_Join") != '1')
		return 0L;


	int ilLine = -1;
	long llUrno = 0;

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
    omTable.pomListBox->ScreenToClient(&olDropPosition);    

	ilLine = omTable.GetLinenoFromPoint(olDropPosition);

	if(-1 == ilLine)
		return -1L;	

    int ilClass = omDragDropTarget.GetDataClass(); 

	if(ilClass != DIT_FLIGHT)
	{
		return -1L;	
	}
	llUrno = omDragDropTarget.GetDataDWord(0);

	
	ROTATIONTABLE_LINEDATA *prlLine = (ROTATIONTABLE_LINEDATA *) omTable.GetTextLineData(ilLine);
	if (prlLine == NULL)
	{
		return -1L;
	}

	long llUrno2 = prlLine->Urno;
	if(!ogRotationFlights.JoinFlightPreCheck(llUrno, llUrno2))
	{
		return -1L;
	}

	return 0L;
}

////////////////////////////////////////////////////////////////////////////
// Drop einer Drag&Drop Aktion, wo auch immer
//
LONG RotationTableChart::OnDrop(UINT i, LONG l)
{
	if(ogPrivList.GetStat("ROTATIONTAB_DD_Join") != '1')
		return 0L;


	if(OnDragOver(0,0) != 0L) //Last validation, sicher ist sicher
		return -1L;


	int ilLine = -1;

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
    omTable.pomListBox->ScreenToClient(&olDropPosition);    

	ilLine = omTable.GetLinenoFromPoint(olDropPosition);

	long llUrno1 = 0;
	long llUrno2 = 0;
	
	ROTATIONTABLE_LINEDATA *prlLine = (ROTATIONTABLE_LINEDATA *) omTable.GetTextLineData(omTable.GetLinenoFromPoint(olDropPosition));

    int ilClass = omDragDropTarget.GetDataClass(); 


	llUrno1 = omDragDropTarget.GetDataDWord(0);

	llUrno2 = prlLine->Urno;

	if(!ogRotationFlights.JoinFlightPreCheck(llUrno1, llUrno2))
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING949), GetString(ST_FEHLER), MB_ICONWARNING);
		return -1L;
	}

	ROTATIONFLIGHTDATA *prpAFlight = ogRotationFlights.GetFlightByUrno(llUrno1);
	ROTATIONFLIGHTDATA *prpDFlight = ogRotationFlights.GetFlightByUrno(llUrno2);

	if((prpDFlight == NULL) || (prpAFlight == NULL))
		return -1L;

	//postflighthandling
	BOOL blpPost = pomViewer->CheckPostFlight(prpAFlight, prpDFlight);
	HandleWindowText (blpPost);
	if (blpPost)
		return -1L;


	
	// join flights!
	CedaFlightUtilsData olFlightUtilsData;
	olFlightUtilsData.JoinFlight(prpAFlight->Urno, prpAFlight->Rkey, prpDFlight->Urno, prpDFlight->Rkey, false);

	return -1L;
}



LONG RotationTableChart::OnTableIPEdit(UINT wParam, LONG lParam)
{
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;

	ROTATIONTABLE_LINEDATA *prlLine = (ROTATIONTABLE_LINEDATA*)omTable.GetTextLineData(wParam);

	if (!prlLine || !prlNotify)
	    return pomParent->SendMessage(WM_TABLE_IPEDIT, wParam, lParam);

	ROTATIONFLIGHTDATA *prlFlight = ogRotationFlights.GetFlightByUrno(prlLine->Urno);

	if (!prlFlight)
	    return pomParent->SendMessage(WM_TABLE_IPEDIT, wParam, lParam);


	TABLE_HEADER_COLUMN *prlHeaderInfo = NULL;
	prlHeaderInfo = omTable.GetHeader(prlNotify->Column);
	if (prlHeaderInfo == NULL)
		return 0L;


	if(imGroupID == UD_ARRIVAL || imGroupID == UD_AGROUND)
	{
		if (prlHeaderInfo->Text == GetString(IDS_STRING308))
//		if(prlNotify->Column == 14)//position
		{
			CString olstr = prlFlight->Psta;
			if (prlFlight->Onbl != TIMENULL && !olstr.IsEmpty())
			{
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				return -1L;
			}
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING309))
//		if(prlNotify->Column == 15)//gate
		{
			CString olstr = prlFlight->Gta1;
			if (prlFlight->Ga1x != TIMENULL && !olstr.IsEmpty())
			{
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				return -1L;
			}

			olstr = prlFlight->Gta2;
			if (prlFlight->Ga2x != TIMENULL && !olstr.IsEmpty())
			{
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				return -1L;
			}
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING1151))
//		if(prlNotify->Column == 16)//belt
		{
			CString olstr = prlFlight->Blt1;
			if (prlFlight->B1ba != TIMENULL && !olstr.IsEmpty())
			{
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				return -1L;
			}

			olstr = prlFlight->Blt2;
			if (prlFlight->B2ba != TIMENULL && !olstr.IsEmpty())
			{
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				return -1L;
			}
		}
	}

	if(imGroupID == UD_DGROUND || imGroupID == UD_DEPARTURE)
	{
		if (prlHeaderInfo->Text == GetString(IDS_STRING308))
//		if(prlNotify->Column == 14)//position
		{
			CString olstr = prlFlight->Pstd;
			if (prlFlight->Ofbl != TIMENULL && !olstr.IsEmpty())
			{
				MessageBox( GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				return -1L;
			}
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING2014))
//		if(prlNotify->Column == 15)//gate1
		{
			CString olstr = prlFlight->Gtd1;
			if (prlFlight->Gd1x != TIMENULL && !olstr.IsEmpty())
			{
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				return -1L;
			}
		}
		if (prlHeaderInfo->Text == GetString(IDS_STRING2015))
//		if(prlNotify->Column == 16)//gate2
		{
			CString olstr = prlFlight->Gtd2;
			if (prlFlight->Gd2x != TIMENULL && !olstr.IsEmpty())
			{
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				return -1L;
			}
		}
	}

//	return 0L;
    return pomParent->SendMessage(WM_TABLE_IPEDIT, wParam, lParam);
}

BOOL RotationTableChart::HandlePostFlight(BOOL bplPost) 
{

	if(imGroupID == UD_ARRIVAL)
	{
		SetWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_TouchAndGo"),omButton2);
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CS_NextLand"),(CWnd *)pomTopScaleText);
	}

	if(imGroupID == UD_AGROUND)
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CS_NextOnbl"),(CWnd *)pomTopScaleText);

	if(imGroupID == UD_DGROUND)
	{
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CS_NextOfbl"),(CWnd *)pomTopScaleText);
		SetWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CS_ReturnTaxi"),omButton2);

	}

	if(imGroupID == UD_DEPARTURE)
	{
		SetWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_Platzrunde"),omButton2);
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CS_NextAirb"),(CWnd *)pomTopScaleText);

		// return flight
		SetWndStatAll(ogPrivList.GetStat("ROTATIONTAB_CB_ReturnFlight"),omButton4);

	}

	if (bplPost)
	{
		CWnd* wnd = GetDlgItem(IDC_CHARTBUTTON2);
		if (wnd)
			wnd->EnableWindow(FALSE);

		wnd = GetDlgItem(IDC_CHARTBUTTON4);
		if (wnd)
			wnd->EnableWindow(FALSE);

//		return TRUE;
	}
	else
	{
		CWnd* wnd = GetDlgItem(IDC_CHARTBUTTON2);
		if (wnd)
			wnd->EnableWindow(TRUE);

		wnd = GetDlgItem(IDC_CHARTBUTTON4);
		if (wnd)
			wnd->EnableWindow(TRUE);

//		return TRUE;
	}


	return TRUE;
}

BOOL RotationTableChart::HandleWindowText(BOOL bplPost)
{
	CWnd* opWnd = (CWnd*) pomParent;
	if (opWnd)
	{
		if (bplPost)
			ModifyWindowText(opWnd, GetString(IDS_STRINGWND_POSTFLIGHT), FALSE);
		else
			ModifyWindowText(opWnd, GetString(IDS_STRINGWND_POSTFLIGHT), TRUE);

		return TRUE;
	}
	return FALSE;
}

LONG RotationTableChart::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	if(pomParent != NULL)
		pomParent->SendMessage(WM_TABLE_RETURN_PRESSED, wParam, lParam);
	return 0L;
}

LONG RotationTableChart::OnTableSelChanged( UINT wParam, LPARAM lParam)
{
	if(pomParent != NULL)
		pomParent->SendMessage(WM_TABLE_SELCHANGE, wParam, lParam);
	return 0L;

}
