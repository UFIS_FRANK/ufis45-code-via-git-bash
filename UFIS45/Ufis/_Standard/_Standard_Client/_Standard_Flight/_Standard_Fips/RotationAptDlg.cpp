// RotationAptDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <RotationAptDlg.h>
#include <resrc1.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <AwBasicDataDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



// Local function prototype
static void AptCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// RotationAptDlg dialog


RotationAptDlg::RotationAptDlg(CWnd* pParent /*=NULL*/, CString opApc)
	: CDialog(RotationAptDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationAptDlg)
	m_Apc3 = _T("");
	m_Apc4 = _T("");
	m_Apfn = _T("");
	//}}AFX_DATA_INIT


	if(opApc.GetLength() == 4)
		m_Apc4 = opApc;
	else
		m_Apc3 = opApc;
	
	ogDdx.Register(this, APP_EXIT, CString("APTDLG"), CString("Appli. exit"), AptCf);

}


RotationAptDlg::~RotationAptDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
}


static void AptCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotationAptDlg *polDlg = (RotationAptDlg *)popInstance;

    if (ipDDXType == APP_EXIT)
        polDlg->AppExit();

}


void RotationAptDlg::AppExit()
{
	EndDialog(IDCANCEL);
}


void RotationAptDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationAptDlg)
	DDX_Control(pDX, IDC_APFN, m_CE_Apfn);
	DDX_Control(pDX, IDC_APC4, m_CE_Apc4);
	DDX_Control(pDX, IDC_APC3, m_CE_Apc3);
	DDX_Text(pDX, IDC_APC3, m_Apc3);
	DDX_Text(pDX, IDC_APC4, m_Apc4);
	DDX_Text(pDX, IDC_APFN, m_Apfn);
	DDV_MaxChars(pDX, m_Apfn, 32);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationAptDlg, CDialog)
	//{{AFX_MSG_MAP(RotationAptDlg)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationAptDlg message handlers

void RotationAptDlg::OnOK() 
{
	//killfocus - check for the curent field
	NextDlgCtrl(); 
	PrevDlgCtrl();

	m_CE_Apc3.GetWindowText(m_Apc3);
	m_CE_Apc4.GetWindowText(m_Apc4);
	m_CE_Apfn.GetWindowText(m_Apfn);


	if(!m_CE_Apc3.GetStatus())
		m_Apc3 = "";

	if(!m_CE_Apc4.GetStatus())
		m_Apc4 = "";


	if(m_Apc3.IsEmpty() && m_Apc4.IsEmpty())
		return;


	RecordSet *prlRecord = new RecordSet(ogBCD.GetFieldCount("APT"));
	prlRecord->Values[ogBCD.GetFieldIndex("APT", "APFN")] = m_Apfn;
	prlRecord->Values[ogBCD.GetFieldIndex("APT", "APC3")] = m_Apc3;
	prlRecord->Values[ogBCD.GetFieldIndex("APT", "APC4")] = m_Apc4;
	ogBCD.InsertRecord("APT", (*prlRecord), true);
	delete prlRecord;
	
	CDialog::OnOK();
}


LONG RotationAptDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
	if(prlNotify->Text.IsEmpty())
		return 0L;

	if((UINT)m_CE_Apc3.imID == wParam)
	{
		CString olTmp;
		prlNotify->UserStatus = true;
		prlNotify->Status = !ogBCD.GetField("APT", "APC3", prlNotify->Text, "APC4", olTmp );
		if(prlNotify->Status == false)
			MessageBox(GetString(IDS_STRING940), GetString(ST_FEHLER));
		return 0L;
	}
	if((UINT)m_CE_Apc4.imID == wParam)
	{
		CString olTmp;
		prlNotify->UserStatus = true;
		prlNotify->Status = !ogBCD.GetField("APT", "APC4", prlNotify->Text, "APC3", olTmp );
		if(prlNotify->Status == false)
			MessageBox(GetString(IDS_STRING941), GetString(ST_FEHLER));
		return 0L;
	}
	return 0L;

}

BOOL RotationAptDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_CE_Apc4.SetTypeToString("X|#X|#X|#X|#",4,0);
	m_CE_Apc3.SetTypeToString("X|#X|#X|#",3,0);

	
	if(ogPrivList.GetStat("INSERT_NEW_AIRPORT") == '1' || ogPrivList.GetStat("INSERT_NEW_AIRPORT") == ' ')
	{
		return TRUE;
	}
	else
	{
		MessageBox(GetString(IDS_STRING2845), GetString(ST_FEHLER));
		EndDialog(IDCANCEL);
		return FALSE;
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
