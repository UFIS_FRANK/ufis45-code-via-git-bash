// CCAExpandDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <CCAExpandDlg.h>
#include <CCSGlobl.h>
#include <CcaCedaFlightData.h>
#include <DiaCedaFlightData.h>
#include <resrc1.h>
#include <AskBox.h>
#include <process.h>
#include <CcaDiagram.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCAExpandDlg dialog

//
CCAExpandDlg::CCAExpandDlg(CWnd* pParent /*=NULL*/, int ipSubMod /*= SUB_MOD_ALL*/ , CString opStev, CTime opRefDate, CTime opViewFrom , CTime opViewTo )
	: CDialog(CCAExpandDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCAExpandDlg)
		// NOTE: the ClassWizard will add member initialization here
	m_check1 = TRUE;
	m_check2 = TRUE;
	m_check3 = TRUE;
	m_check4 = TRUE;
	m_check5 = TRUE;
	m_check6 = TRUE;
	m_check7 = TRUE;
	m_common = TRUE;
	m_flight = TRUE;
	m_PreCheck = TRUE;
	m_AC = TRUE;
	m_STD = TRUE;
	m_DEST = TRUE;
	m_POS = TRUE;
	m_GAT = TRUE;
	m_BLT = TRUE;
	m_WRO = TRUE;
	m_EXIT = TRUE;
	m_OverWrite = TRUE;
	//}}AFX_DATA_INIT

	omStev = opStev;
	imSubMod = ipSubMod;
	omRefDate = opRefDate;

	if(opViewFrom == TIMENULL)
		omViewFrom = CTime::GetCurrentTime();
	else
		omViewFrom = CTime( opViewFrom.GetYear(), opViewFrom.GetMonth(), opViewFrom.GetDay(), 0,0,0,-1);


	if(opViewTo == TIMENULL)
		omViewTo = CTime::GetCurrentTime();
	else
		omViewTo = CTime( opViewTo.GetYear(), opViewTo.GetMonth(), opViewTo.GetDay(), 23,59,59,-1);


	CString olSfrom = omViewFrom.Format("%d.%m.%Y-%H:%M");
	CString olSto = omViewTo.Format("%d.%m.%Y-%H:%M");



}
BOOL CCAExpandDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_From.SetTypeToDate(true);
	m_To.SetTypeToDate(true);
	m_only = false;

	CPoint point;
	::GetCursorPos(&point);
	this->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );

	
	m_Ref_Date.EnableWindow(TRUE);

	CTimeSpan olSpan3 = CTimeSpan(1,0,0,0);

	omRefDate += olSpan3;



	EXPANDDATA *prlExpandData = NULL;

	for( int i = 0; i < ogExandData.GetSize(); i++)
	{
		prlExpandData = &ogExandData[i];

		if(prlExpandData->SubMod == imSubMod)
			break;
		else
			prlExpandData = NULL;
	}


	if(prlExpandData != NULL)
	{
		m_check1 = prlExpandData->check1;
		m_check2 = prlExpandData->check2;
		m_check3 = prlExpandData->check3;
		m_check4 = prlExpandData->check4;
		m_check5 = prlExpandData->check5;
		m_check6 = prlExpandData->check6;
		m_check7 =  prlExpandData->check7;

		omStev = prlExpandData->Stev;
		omRefDate = prlExpandData->RefDate;

		m_common = prlExpandData->common;
		m_flight = prlExpandData->flight;
		m_PreCheck = prlExpandData->PreCheck ;
		m_AC = prlExpandData->AC;
		m_STD = prlExpandData->STD;
		m_DEST = prlExpandData->DEST;
		m_POS = prlExpandData->POS;
		m_GAT = prlExpandData->GAT;
		m_BLT = prlExpandData->BLT;
		m_WRO = prlExpandData->WRO;
		m_EXIT = prlExpandData->EXIT;
		m_OverWrite = prlExpandData->OverWrite;

		if (imSubMod == SUB_MOD_ALL)
		{
			prlExpandData->POS = FALSE;
			prlExpandData->GAT = FALSE;
			prlExpandData->BLT = FALSE;
			prlExpandData->WRO = FALSE;
			prlExpandData->EXIT = FALSE;
		}
		else
		{
			prlExpandData->common = FALSE;
			prlExpandData->flight = FALSE;
			prlExpandData->PreCheck = FALSE;
		}
	}




	CWnd* wnd = NULL;


	if (imSubMod == SUB_MOD_ALL)
	{
		wnd = GetDlgItem(IDC_POS);
		if (wnd)
			wnd->EnableWindow(FALSE);

		wnd = GetDlgItem(IDC_GAT);
		if (wnd)
			wnd->EnableWindow(FALSE);

		wnd = GetDlgItem(IDC_BLT);
		if (wnd)
			wnd->EnableWindow(FALSE);

		wnd = GetDlgItem(IDC_WRO);
		if (wnd)
			wnd->EnableWindow(FALSE);

		wnd = GetDlgItem(IDC_EXIT);
		if (wnd)
			wnd->EnableWindow(FALSE);
/*
		wnd = GetDlgItem(IDC_OVERWRITE);
		if (wnd)
			wnd->EnableWindow(FALSE);

*/
		//m_Ref_Date.EnableWindow(FALSE);
		//m_Ref_Date.SetWindowText("");

	
	}
	else
	{
		wnd = GetDlgItem(IDC_COMMON);
		if (wnd)
			wnd->EnableWindow(FALSE);

		wnd = GetDlgItem(IDC_FLIGHT);
		if (wnd)
			wnd->EnableWindow(FALSE);

		wnd = GetDlgItem(IDC_PRECHECK);
		if (wnd)
			wnd->EnableWindow(FALSE);


	}


	m_CB_check1.SetCheck( m_check1 );
	m_CB_check2.SetCheck( m_check2 );
	m_CB_check3.SetCheck( m_check3 );
	m_CB_check4.SetCheck( m_check4 );
	m_CB_check5.SetCheck( m_check5 );
	m_CB_check6.SetCheck( m_check6 );
	m_CB_check7.SetCheck( m_check7 );

	m_CB_common.SetCheck( m_common);
	m_CB_flight.SetCheck( m_flight );
	m_CB_Precheck.SetCheck( m_PreCheck );
	m_CB_AC.SetCheck( m_AC );
	m_CB_STD.SetCheck( m_STD );
	m_CB_DEST.SetCheck( m_DEST );
	m_CB_POS.SetCheck( m_POS );
	m_CB_GAT.SetCheck( m_GAT );
	m_CB_BLT.SetCheck( m_BLT );
	m_CB_WRO.SetCheck( m_WRO );
	m_CB_EXIT.SetCheck( m_EXIT );
	m_CB_Overwrite.SetCheck( m_OverWrite );


	m_Ref_Date.SetWindowText("");

	if(prlExpandData != NULL)
	{
		m_Ref_Date.SetWindowText(omRefDate.Format("%d.%m.%Y"));
		m_From.SetWindowText(prlExpandData->ExpandFrom.Format("%d.%m.%Y"));
		m_To.SetWindowText(prlExpandData->ExpandTo.Format("%d.%m.%Y"));
	}

	return TRUE;
}


void CCAExpandDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCAExpandDlg)
	DDX_Control(pDX, IDC_ALLVT, m_CB_AllVT);
	DDX_Control(pDX, IDC_PROG, m_CP_Progess);
	DDX_Control(pDX, IDC_DATEFROM, m_From);
	DDX_Control(pDX, IDC_DATETO, m_To);
	DDX_Control(pDX, IDC_REF_DATE, m_Ref_Date);
	DDX_Control(pDX, IDC_POS, m_CB_POS);
	DDX_Control(pDX, IDC_FLIGHT, m_CB_flight);
	DDX_Control(pDX, IDC_COMMON, m_CB_common);
	DDX_Control(pDX, IDC_PRECHECK, m_CB_Precheck);
	DDX_Control(pDX, IDC_CHECK_AC, m_CB_AC);
	DDX_Control(pDX, IDC_CHECK_STD, m_CB_STD);
	DDX_Control(pDX, IDC_CHECK_DEST, m_CB_DEST);
	DDX_Control(pDX, IDC_GAT, m_CB_GAT);
	DDX_Control(pDX, IDC_BLT, m_CB_BLT);
	DDX_Control(pDX, IDC_WRO, m_CB_WRO);
	DDX_Control(pDX, IDC_EXIT, m_CB_EXIT);
	DDX_Control(pDX, IDC_OVERWRITE, m_CB_Overwrite);
	DDX_Control(pDX, IDC_CHECK1, m_CB_check1);
	DDX_Control(pDX, IDC_CHECK2, m_CB_check2);
	DDX_Control(pDX, IDC_CHECK3, m_CB_check3);
	DDX_Control(pDX, IDC_CHECK4, m_CB_check4);
	DDX_Control(pDX, IDC_CHECK5, m_CB_check5);
	DDX_Control(pDX, IDC_CHECK6, m_CB_check6);
	DDX_Control(pDX, IDC_CHECK7, m_CB_check7);
	DDX_Check(pDX, IDC_CHECK1, m_check1);
	DDX_Check(pDX, IDC_CHECK2, m_check2);
	DDX_Check(pDX, IDC_CHECK3, m_check3);
	DDX_Check(pDX, IDC_CHECK4, m_check4);
	DDX_Check(pDX, IDC_CHECK5, m_check5);
	DDX_Check(pDX, IDC_CHECK6, m_check6);
	DDX_Check(pDX, IDC_CHECK7, m_check7);
	DDX_Check(pDX, IDC_COMMON, m_common);
	DDX_Check(pDX, IDC_FLIGHT, m_flight);
	DDX_Check(pDX, IDC_PRECHECK, m_PreCheck);
	DDX_Check(pDX, IDC_CHECK_AC, m_AC);
	DDX_Check(pDX, IDC_CHECK_STD, m_STD);
	DDX_Check(pDX, IDC_CHECK_DEST, m_DEST);
	DDX_Check(pDX, IDC_POS, m_POS);
	DDX_Check(pDX, IDC_GAT, m_GAT);
	DDX_Check(pDX, IDC_BLT, m_BLT);
	DDX_Check(pDX, IDC_WRO, m_WRO);
	DDX_Check(pDX, IDC_EXIT, m_EXIT);
	DDX_Check(pDX, IDC_OVERWRITE, m_OverWrite);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCAExpandDlg, CDialog)
	//{{AFX_MSG_MAP(CCAExpandDlg)
	ON_BN_CLICKED(IDC_ONLY, OnOnly)
	ON_BN_CLICKED(IDC_ALL, OnAll)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCAExpandDlg message handlers

void CCAExpandDlg::OnOnly() 
{

	m_only = true;
	OnAll();
	m_only = false;

	return;



	//kann nicht wahr sein!!!
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	CUIntArray olUrnos;
	CUIntArray olCommonUrnos;
	CCSPtrArray<DIACCADATA> olData;

	CTime olFrom(1998,10,25,0,0,0);
	CTime olTo(1999,03,27,23,59,59);


	if(!m_From.GetStatus())
	{
		MessageBox(GetString(IDS_NO_DATE), GetString(IDS_STRING908), MB_OK);
		return;
	}
	if(!m_To.GetStatus())
	{
		MessageBox(GetString(IDS_NO_DATE), GetString(IDS_STRING908), MB_OK);
		return;
	}
	CString olDatFrom, olDatTo;
	m_From.GetWindowText(olDatFrom);
	m_To.GetWindowText(olDatTo);
	olFrom = DateStringToDate(olDatFrom);
	olTo = DateStringToDate(olDatTo);


	if(olFrom == TIMENULL || olTo == TIMENULL)
	{
		MessageBox( GetString(IDS_STRING1657) ,  GetString(ST_FEHLER));
		return;
	}



	for(int i = ogCcaDiaFlightData.omData.GetSize() - 1; i >= 0; i--)
	{
		olData.RemoveAll();
		if(ogCcaDiaFlightData.omCcaData.GetCcaArray(ogCcaDiaFlightData.omData[i].Urno, olData))
		{

			for(int j = olData.GetSize() - 1; j >= 0; j--)
			{

				if(olData[j].IsSelected)
				{
					olUrnos.Add(ogCcaDiaFlightData.omData[i].Urno);
					break;
				}
			}
		}
	}


	//CTime olTime = CTime::GetCurrentTime();
	//TRACE("\nBEGIN %s", olTime.Format("%H:%M:%S"));


	ogCcaDiaFlightData.omCcaData.GetCommonUrnos(olCommonUrnos, true);


	int ilCount = olUrnos.GetSize();
	int ilCommonCount = olCommonUrnos.GetSize();

	m_CP_Progess.SetRange(0,ilCount + 1 + ilCommonCount);
	m_CP_Progess.SetStep(1);

	CTime olRefDate = ogCcaDiaFlightData.omFrom;


	bool blAll = false;
	
	if(m_CB_AllVT.GetCheck())
		blAll = true;


	for( i = 0; i < ilCommonCount; i++)
	{
		m_CP_Progess.OffsetPos(1);
//		ogCcaDiaFlightData.omCcaData.Expand(olCommonUrnos[i], olFrom, olTo,olRefDate, blAll);
	}

/*
	for( i = 0; i < ilCount; i++)
	{
		m_CP_Progess.OffsetPos(1);
		ogCcaDiaFlightData.Expand(olUrnos[i], olFrom, olTo);
	}
*/
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	m_CP_Progess.SetPos(0);
	
	//olTime = CTime::GetCurrentTime();
	//TRACE("\nEND %s", olTime.Format("%H:%M:%S"));
	
}

void CCAExpandDlg::CreateCICProtokoll(CStringArray& olCopied, CStringArray& olNotCopied)
{
	char pclConfigPath[256];
	char pclExcelPath[256];
	char opTrenner[64];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
		opTrenner, sizeof opTrenner, pclConfigPath);

	if(!strcmp(pclExcelPath, "DEFAULT"))
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING977), GetString(ST_FEHLER), MB_ICONERROR);

	ofstream ofResult;
	ofstream ofFailed;
	CString olFileResult;
	CString olFileFailed;
	CreateExcelFiles(ofResult, ofFailed, olFileResult, olFileFailed, opTrenner);

//	if (imSubMod == SUB_MOD_ALL)
//	{
		for (int ilCopied=0; ilCopied < olCopied.GetSize(); ilCopied++)
			ofFailed  << setw(1) << olCopied.GetAt(ilCopied) << endl;

		for (ilCopied=0; ilCopied < olNotCopied.GetSize(); ilCopied++)
			ofResult  << setw(1) << olNotCopied.GetAt(ilCopied) << endl;
//	}

	ofResult.close();
	ofFailed.close();

	if (olNotCopied.GetSize() > 0)
	{
		AskBox olDlg(this, GetString(ST_FRAGE), GetString(IDS_STRING2252), "Yes", "No");
 		if (olDlg.DoModal() != 1)
 			return;

		char pclTmp[256];
		strcpy(pclTmp, olFileResult + CString(" ") + olFileFailed); 

		char *args[4];
		args[0] = "child";
		args[1] = pclTmp;
		args[2] = NULL;
		args[3] = NULL;

		_spawnv( _P_NOWAIT , pclExcelPath, args );
	}
}



void CCAExpandDlg::OnAll() 
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	CTime olTime = CTime::GetCurrentTime();
	TRACE("\nBEGIN %s", olTime.Format("%H:%M:%S"));

	CTime olFrom(1998,10,25,0,0,0);
	CTime olTo(1999,03,27,23,59,59);
	CTime olRef(1999,03,27,23,59,59);

	if(!m_From.GetStatus())
	{
		MessageBox(GetString(IDS_NO_DATE), GetString(IDS_STRING908), MB_OK);
		return;
	}

	if(!m_To.GetStatus())
	{
		MessageBox(GetString(IDS_NO_DATE), GetString(IDS_STRING908), MB_OK);
		return;
	}

	CString olDatFrom, olDatTo, olRefDate2, olTmp;
	m_From.GetWindowText(olDatFrom);
	m_To.GetWindowText(olDatTo);
	m_Ref_Date.GetWindowText(olRefDate2);
	olFrom = DateStringToDate(olDatFrom);
	olTo = DateStringToDate(olDatTo);
	//olRef = DateStringToDate(olRefDate2);
	olTmp = "1200";
	olRef = DateHourStringToDate(olRefDate2, olTmp);

	CString olSfrom = omViewFrom.Format("%d.%m.%Y-%H:%M");
	CString olSto = omViewTo.Format("%d.%m.%Y-%H:%M");

	CString olSRef = olRef.Format("%d.%m.%Y-%H:%M");

	if(olRef == TIMENULL )
	{
		MessageBox( GetString(IDS_STRING2872) ,  GetString(ST_FEHLER));
		m_Ref_Date.SetFocus();
		return;
	}

	
	if(olRef < omViewFrom || olRef > omViewTo )
	{
		MessageBox( GetString(IDS_STRING2872) ,  GetString(ST_FEHLER));
		m_Ref_Date.SetFocus();
		return;
	}

	omRefDate = olRef;




	if(olFrom == TIMENULL || olTo == TIMENULL)
	{
		MessageBox( GetString(IDS_STRING1657) ,  GetString(ST_FEHLER));
		return;
	}

	if(olFrom < olTime)
	{
		MessageBox( GetString(IDS_STRING2262) ,  GetString(ST_FEHLER));
		return;
	}

	if (UpdateData(TRUE))
		UpdateData(FALSE);
/*
	if (bgGatPosLocal)
	{
		ogBasicData.LocalToUtc(olFrom); 
		ogBasicData.LocalToUtc(olTo);
		ogBasicData.LocalToUtc(olRef);
	}
*/	

	CTime olRefDate = TIMENULL;
	int ilCount = 0;
	bool blAll = true;

	CUIntArray olDayArray;
	olDayArray.Add(m_check1);
	olDayArray.Add(m_check2);
	olDayArray.Add(m_check3);
	olDayArray.Add(m_check4);
	olDayArray.Add(m_check5);
	olDayArray.Add(m_check6);
	olDayArray.Add(m_check7);

	CUIntArray olMustHasArray;
	olMustHasArray.Add(m_AC);
	olMustHasArray.Add(m_STD);
	olMustHasArray.Add(m_DEST);

	CMapStringToString olMustHaveMap;
	if (m_AC)
		olMustHaveMap.SetAt("AC","1");
	if (m_STD)
		olMustHaveMap.SetAt("STD","1");
	if (m_DEST)
		olMustHaveMap.SetAt("DEST","1");

	if (imSubMod == SUB_MOD_ALL)//old MOD for check-in
	{
		olRefDate = ogCcaDiaFlightData.omFrom;
		CCSPtrArray<DIACCADATA> olData;
		CMapStringToString opTypMap;
		
		if (m_common)
			opTypMap.SetAt("C","1");
		if (m_flight)
			opTypMap.SetAt("","1"); 
		if (m_PreCheck)
			opTypMap.SetAt("P","1");

//		if (!ogCcaDiaFlightData.omCcaData.GetCca(olData, opTypMap, m_only, true))
		if (!pogCcaDiagram->GetLoadedCca(olData, opTypMap, m_only, false))
		{
			MessageBox( GetString(IDS_STRING2263) ,  GetString(ST_FEHLER));
			return;
		}

		ilCount = olData.GetSize();
		if (ilCount == 0)
			return;

		CStringArray olCopied;
		CStringArray olNotCopied;
		ogCcaDiaFlightData.omCcaData.Expand(m_CP_Progess, olData, olFrom, olTo, olDayArray, olMustHasArray, olCopied, olNotCopied, m_OverWrite, omStev, olRef);

		CreateCICProtokoll(olCopied, olNotCopied);
		olData.RemoveAll();
	}
	else
	{
		olRefDate = ogPosDiaFlightData.omFrom;
		CCSPtrArray<DIAFLIGHTDATA> olSelectedFlights;
		if (!ogPosDiaFlightData.GetSelectedFlights(olSelectedFlights, imSubMod, m_only, false))
		{
			MessageBox( GetString(IDS_STRING2263) ,  GetString(ST_FEHLER));
			return;
		}

		ilCount = olSelectedFlights.GetSize();
		if (ilCount == 0)
			return;

		CMapStringToString olTypeMap;
		if (m_POS)
			olTypeMap.SetAt("POS","1");
		if (m_GAT)
			olTypeMap.SetAt("GAT","1");
		if (m_BLT)
			olTypeMap.SetAt("BLT","1");
		if (m_WRO)
			olTypeMap.SetAt("WRO","1");
		if (m_EXIT)
			olTypeMap.SetAt("EXIT","1");

		m_CP_Progess.SetRange(0,ilCount + 1);
		m_CP_Progess.SetStep(1);

		CStringArray olCopied;
		CStringArray olNotCopied;
		for(int i = 0; i < ilCount; i++)
		{
			m_CP_Progess.OffsetPos(1);
			ogPosDiaFlightData.Expand(olSelectedFlights[i].Urno, olFrom, olTo, olDayArray, olMustHaveMap, olCopied, olNotCopied, olTypeMap, m_OverWrite, omStev, olRef);
		}

		CreateCICProtokoll(olCopied, olNotCopied);
//YYYY
		olSelectedFlights.RemoveAll();
	}


	EXPANDDATA *prlExpandData = NULL;

	for( int i = 0; i < ogExandData.GetSize(); i++)
	{
		prlExpandData = &ogExandData[i];

		if(prlExpandData->SubMod == imSubMod)
			break;
		else
			prlExpandData = NULL;

	}
	
	if( prlExpandData == NULL)
	{
		prlExpandData = new EXPANDDATA();

		prlExpandData->check1 = m_check1;
		prlExpandData->check2 = m_check2;
		prlExpandData->check3 = m_check3;
		prlExpandData->check4 = m_check4;
		prlExpandData->check5 = m_check5;
		prlExpandData->check6 = m_check6;
		prlExpandData->check7 = m_check7;

		prlExpandData->common = m_common;
		prlExpandData->flight = m_flight;
		prlExpandData->PreCheck = m_PreCheck;
		prlExpandData->AC = m_AC;
		prlExpandData->STD = m_STD;
		prlExpandData->DEST = m_DEST;
		prlExpandData->POS = m_POS;
		prlExpandData->GAT = m_GAT;
		prlExpandData->BLT = m_BLT;
		prlExpandData->WRO = m_WRO;
		prlExpandData->EXIT = m_EXIT;
		prlExpandData->OverWrite = m_OverWrite;
		prlExpandData->SubMod = imSubMod;

		prlExpandData->Stev = omStev;
		prlExpandData->RefDate = omRefDate;
		prlExpandData->ExpandFrom = olFrom;
		prlExpandData->ExpandTo = olTo;

		ogExandData.Add(prlExpandData);

	}
	else
	{
		prlExpandData->check1 = m_check1;
		prlExpandData->check2 = m_check2;
		prlExpandData->check3 = m_check3;
		prlExpandData->check4 = m_check4;
		prlExpandData->check5 = m_check5;
		prlExpandData->check6 = m_check6;
		prlExpandData->check7 = m_check7;

		prlExpandData->common = m_common;
		prlExpandData->flight = m_flight;
		prlExpandData->PreCheck = m_PreCheck;
		prlExpandData->AC = m_AC;
		prlExpandData->STD = m_STD;
		prlExpandData->DEST = m_DEST;
		prlExpandData->POS = m_POS;
		prlExpandData->GAT = m_GAT;
		prlExpandData->BLT = m_BLT;
		prlExpandData->WRO = m_WRO;
		prlExpandData->EXIT = m_EXIT;
		prlExpandData->OverWrite = m_OverWrite;
		prlExpandData->SubMod = imSubMod;

		prlExpandData->Stev = omStev;
		prlExpandData->RefDate = omRefDate;
		prlExpandData->ExpandFrom = olFrom;
		prlExpandData->ExpandTo = olTo;

	}



	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	m_CP_Progess.SetPos(0);
	olTime = CTime::GetCurrentTime();
	TRACE("\nEND %s", olTime.Format("%H:%M:%S"));
}


bool CCAExpandDlg::CreateExcelFiles(ofstream& of, ofstream& ofRes, CString& olFileName, CString& olFileNameRes, char* opTrenner)
{
	if (imSubMod == SUB_MOD_ALL)
	{
		olFileName = GetString(IDS_STRING2253);
		olFileNameRes = GetString(IDS_STRING2260);
	}
	else
	{
		olFileName = GetString(IDS_STRING2286);
		olFileNameRes = GetString(IDS_STRING2287);
	}

	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	olFileName =  path + "\\" + olFileName + ".csv";
	olFileNameRes =  path + "\\" + olFileNameRes + ".csv";

	of.open( olFileName, ios::out);
	ofRes.open( olFileNameRes, ios::out);

	int ilwidth = 1;

	CString olTimeSet = GetString(IDS_STRING1920); //UTC
	if(bgGatPosLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	of  << setw(ilwidth) << olFileName << "     " << olTimeSet << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	if (imSubMod == SUB_MOD_ALL)
	{
		of  << setw(ilwidth) << GetString(IDS_STRING2254)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2285)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2258)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2259)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2255)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2257)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2256)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2261)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2283)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2279)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2281)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2280)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2282)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2284)
			<< endl;
		ofRes  << setw(ilwidth) << olFileNameRes << "     " << olTimeSet << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

		ofRes  << setw(ilwidth) << GetString(IDS_STRING2254)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2285)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2258)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2259)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2255)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2257)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2256)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2261)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2283)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2279)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2281)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2280)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2282)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2284)
			<< endl;
	}
	else
	{
		of  << setw(ilwidth) << GetString(IDS_STRING2289)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2288)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2285)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2258)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2259)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2255)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2257)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2256)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2261)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2283)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2279)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2281)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2280)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2282)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2284)
			<< endl;
		ofRes  << setw(ilwidth) << olFileNameRes << "     " << olTimeSet << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

		ofRes  << setw(ilwidth) << GetString(IDS_STRING2289)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2288)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2285)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2258)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2259)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2255)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2257)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2256)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2261)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2283)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2279)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2281)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2280)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2282)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING2284)
			<< endl;
	}

	return true;
}
