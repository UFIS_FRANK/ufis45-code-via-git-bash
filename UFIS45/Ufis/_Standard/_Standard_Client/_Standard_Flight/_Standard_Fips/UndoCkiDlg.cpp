// UndoCkiDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <UndoCkiDlg.h>
#include <resrc1.h>
#include <CCSGlobl.h>
#include <BasicData.h>
#include <CedaDiaCcaData.h>
#include <CcaCedaFlightData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void UndoCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// UndoCkiDlg dialog


UndoCkiDlg::UndoCkiDlg(CWnd* pParent )
	: CDialog(UndoCkiDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(UndoCkiDlg)
	//}}AFX_DATA_INIT
		
    CDialog::Create(UndoCkiDlg::IDD, pParent);

    ogDdx.Register(this, APP_EXIT, CString("UndoCkiDlg"), CString("Appli. exit"), UndoCf);

}


UndoCkiDlg::~UndoCkiDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
}


static void UndoCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    UndoCkiDlg *polDlg = (UndoCkiDlg *)popInstance;

    if (ipDDXType == APP_EXIT)
        polDlg->AppExit();

}


void UndoCkiDlg::AppExit()
{
	EndDialog(IDCANCEL);
}




BOOL UndoCkiDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_List.SetFont(&ogCourier_Bold_10);


	 // return TRUE unless you set the focus to a control
	 // EXCEPTION: OCX Property Pages should return FALSE
	return TRUE;
}



void UndoCkiDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(UndoCkiDlg)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(UndoCkiDlg, CDialog)
	//{{AFX_MSG_MAP(UndoCkiDlg)
	ON_BN_CLICKED(IDC_UNDO, OnUndo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UndoCkiDlg message handlers



void UndoCkiDlg::SetMessage(CString opMessage)
{
	m_List.AddString(opMessage);
	if ((m_List.GetCount()-12) > 0)
		m_List.SetTopIndex(m_List.GetCount()-12);
	UpdateWindow();
}


void UndoCkiDlg::OnUndo() 
{
	CString olText;
	int ilId = -1;

	int ilCount = m_List.GetCount( );

	if(ilCount <= 0)
		return;

	m_List.GetText( ilCount-1, olText); 


	m_List.DeleteString( ilCount-1);

	int ilPos = olText.Find("#");
	
	if( ilPos > 0)
	{
		olText = olText.Right(  olText.GetLength() - ilPos - 1);

		ilId = atol(olText);
	}

	if(ilId > 0)
		ogCcaDiaFlightData.omCcaData.Undo(ilId);


}

void UndoCkiDlg::Reset()
{
	if(m_hWnd != NULL)
	{
		m_List.ResetContent();
		ShowWindow(SW_HIDE);
	}

}
