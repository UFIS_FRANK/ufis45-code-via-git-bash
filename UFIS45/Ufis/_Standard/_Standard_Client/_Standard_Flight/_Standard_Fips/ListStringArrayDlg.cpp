
//CListStringArrayDlg.cpp : implementation file  

#include "stdafx.h"
#include "fpms.h"
#include "ListStringArrayDlg.h"
#include "CCSGlobl.h"
#include "CedaBasicData.h"
#include "CCSBasicFunc.h"
#include "PrivList.h"
#include "afxtempl.h"
#include "AwBasicDataDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CListStringArrayDlg dialog
CListStringArrayDlg::CListStringArrayDlg(CWnd * pParent,CString opCaption,CStringArray *popStrArray,CString olSelStr,bool blHideCancel)
	: CDialog(CListStringArrayDlg::IDD, pParent)
{
	omStrArray = popStrArray;
	omCaption = opCaption;
	omSelStr = olSelStr;	
	bmHideCancel = blHideCancel;
}
void CListStringArrayDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CListStringArrayDlg)
	DDX_Control(pDX, IDOK, m_CB_OK);
	DDX_Control(pDX, IDCANCEL, m_CB_Cancel);
	DDX_Control(pDX, IDC_HEADER, m_CS_Header);
	DDX_Control(pDX, IDC_LIST, m_CL_List);
	//}}AFX_DATA_MAP

}


BEGIN_MESSAGE_MAP(CListStringArrayDlg, CDialog)
	//{{AFX_MSG_MAP(CListStringArrayDlg)
	ON_MESSAGE(WM_LBUTTONDOWN,OnLButtonDown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListStringArrayDlg message handlers
BOOL CListStringArrayDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_CS_Header.SetWindowText(GetString(IDS_STRING2820));


	m_CL_List.ResetContent( );
	m_CL_List.SetFont(&ogCourier_Regular_10);
	m_CL_List.InitStorage( omStrArray->GetSize(), 100 );



	for(int i=0;i<omStrArray->GetSize();i++)
	{
		m_CL_List.AddString(omStrArray->GetAt(i));
	}
	SetWindowText(omCaption);
	if(omSelStr.GetLength()>0)		
	{
		m_CL_List.SetCurSel(m_CL_List.FindString(0,omSelStr));
		m_CL_List.SetFocus();
	}


	if(bmHideCancel)
		m_CB_Cancel.ShowWindow(SW_HIDE);

	return FALSE;	
}


CString CListStringArrayDlg::GetField()
{
	return omFieldStr;
}


void CListStringArrayDlg::OnOK() 
{
	int li_Count = m_CL_List.GetCurSel();
	if(li_Count >= 0)
	{
		m_CL_List.GetText(m_CL_List.GetCurSel(),omFieldStr);
	}

	omFieldStr.TrimLeft();
	omFieldStr.TrimRight();
	if(omFieldStr.GetLength() > 0)
		CDialog::OnOK();
}

void CListStringArrayDlg::OnCancel()
{
	if(!bmHideCancel)
		CDialog::OnCancel();
}

LRESULT CListStringArrayDlg::OnLButtonDown(WPARAM nFlags, LPARAM point ) 
{
	CDialog::OnLButtonDown(nFlags, point);

	return 0L;
}
