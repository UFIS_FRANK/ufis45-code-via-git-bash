#ifndef _RotationCedaFlightData_H_
#define _RotationCedaFlightData_H_
 
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CCSDefines.h>
#include <CedaCcaData.h>
#include <CedaCflData.h>

//void ProcessFlightCf(void *vpInstance,enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName);




/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//@Man: ROTATIONFLIGHTDATAStruct
//@See: RotationCedaFlightData
/*@Doc:
  A structure for reading flight data. We read all data from database (or get 
  the data from AFTLSG) into this struct and store the data in omData.

*/

 
struct ROTATIONFLIGHTDATA 
{
	char 	 Act3[5]; 	// Flugzeug-3-Letter Code
	char 	 Act5[7]; 	// Flugzeug-5-Letter Code
	CTime 	 Airb; 	// Startzeit (Beste Zeit)
	CTime 	 Airu; 	// Startzeit User
	char 	 Csgn[10]; 	// Call- Sign
	CTime 	 Slot; 		// Slot-Zeit
	char 	 Des3[5]; 	// Bestimmungsflughafen 4-Lettercode
	char 	 Des4[6]; 	// Bestimmungsflughafen 4-Lettercode
	CTime 	 Etdc; 	// ETD-Confirmed
	CTime 	 Etai; 	// ETA
	CTime 	 Etdi; 	// ETD
	CTime	 Etoa;	// ETA (Fids)
	CTime	 Etod;	// ETD (Fids)
	char 	 Flno[11]; 	// komplette Flugnummer (Airline, Nummer, Suffix)
	char 	 Fltn[7]; 	// Flugnummer
	char 	 Flns[3]; 	// Suffix
	char 	 Ftyp[3]; 	// Type des Flugs [F, P, R, X, D, T, ...]
	char     Flti[3];   // Flight ID [D, I, ...]
	char 	 Gta1[7]; 	// Gate 1 Ankunft
	char 	 Gta2[7]; 	// Gate 2 Ankunft
	char 	 Gtd1[7]; 	// Gate 1 Abflug
	char 	 Gtd2[7]; 	// Doppelgate Abflug
	char 	 Ifra[3]; 	// Anflugverfahren
	char 	 Ifrd[3]; 	// Abflugverfahren
	char 	 Isre[3]; 	// Bemerkungskennzeichen intern
	CTime 	 Land; 	// Landezeit (Beste Zeit)
	CTime 	 Lstu; 	// Datum letzte �nderung
	CTime 	 Ofbl; 	// Offblock- Zeit (Beste Zeit)
	CTime 	 Onbe; 	// Est. Onblock-Zeit
	CTime 	 Onbl; 	// Onblock-Zeit (Beste Zeit)
	char 	 Org3[5]; 	// Ausgangsflughafen 4-Lettercode
	char 	 Org4[6]; 	// Ausgangsflughafen 4-Lettercode
	char 	 Psta[7]; 	// Position Ankunft
	char 	 Pstd[7]; 	// Position Abflug
	char 	 Regn[14]; 	// LFZ-Kennzeichen
	char 	 Remp[6]; 	// Public Remark FIDS
	long 	 Rkey; 	// Rotationsschl�ssel
	char 	 Rwya[6]; 	// Landebahn
	char 	 Rwyd[6]; 	// Startbahn
	char 	 Stab[3]; 	// Datenstatus AIRB
	CTime 	 Stoa; 	// Planm��ige Ankunftszeit STA
	CTime 	 Stod; 	// Planm��ige Abflugzeit
	CTime 	 Tifa; 	// Zeitrahmen Ankunft
	CTime 	 Tifd; 	// Zeitrahmen Abflug
	CTime 	 Tmoa; 	// TMO (Beste Zeit)
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Via3[4]; 	// Liste der Zwischenstationen/-zeiten
	char 	 Via4[5]; 	// Liste der Zwischenstationen/-zeiten
	char 	 Vian[4]; 	// Anzahl der Zwischenstationen - 1
	char 	 Ttyp[7]; 	// Verkehrsart
	char 	 Chgi[22]; 	// 
	char 	 Rtyp[3]; 	// 
	char 	 Adid[3]; 	// 
	CTime 	 Nxti; 	// 
	char 	 Blt1[7]; 	// 
	CTime 	 Paba; 	// Belegung Position Ankunft aktueller Beginn
	CTime 	 Paea; 	// Belegung Position Ankunft aktuelles Ende
	CTime 	 Pdba; 	// Belegung Position Abflug aktueller Beginn
	CTime 	 Pdea; 	// Belegung Position Abflug aktuelles Ende
	char 	 Ming[6]; 	// Mindestbodenzeit
	char 	 Ader[3]; 	// Mindestbodenzeit
	CTime 	 Aird; 	
	CTime 	 Gd1x; 	
	char 	 Stat[11]; 	
	char 	 Ckif[7]; 	
	char 	 Ckit[7]; 	
	char 	 Usec[34]; 	// User who created the record
	CTime 	 Cdat; 	
	char 	 Blt2[7]; 	
	char 	 Ckf2[6];	// CKI second set (Terminal 2 CKI for mixed flights in Lisbon) 	
	char 	 Ckt2[6]; 	// CKI second set (Terminal 2 CKI for mixed flights in Lisbon)
	char 	 Stev[5]; 	
	char 	 Ste2[4]; 	// Keyfield
	char 	 Ste3[4]; 	// Keyfield
	char 	 Ste4[4]; 	// Keyfield

	CTime 	 Pabs; 	// Belegung Position Ankunft aktueller Beginn
	CTime 	 Paes; 	// Belegung Position Ankunft aktuelles Ende
	CTime 	 Pdbs; 	// Belegung Position Abflug aktueller Beginn
	CTime 	 Pdes; 	// Belegung Position Abflug aktuelles Ende

		
	CTime	B1bs;	
	CTime	B1es;	
	CTime	Ga1b;	
	CTime	Ga1e;	
	CTime	Ga2b;	
	CTime	Ga2e;	
	CTime	Gd1b;	
	CTime	Gd1e;	
	CTime	Gd2b;	
	CTime	Gd2e;	

	CTime	Ga1x;	
	CTime	Ga2x;	
//	CTime	Gd1x;	
	CTime	Gd2x;	
	CTime	B1ba;	
	CTime	B1ea;	
	CTime	B2ba;	
	char 	 Rem1[258]; 	// Bemerkung zum Flug
	char 	 Rem2[258]; 	// Bemerkung zum Flug

	char 	 Paid[3]; 	// Barzahler Kennzeichen
	char 	 Alc2[4]; 	// Fluggesellschaft (Airline 2-Letter Code)
	char 	 Alc3[5]; 	// Fluggesellschaft (Airline 3-Letter Code)

	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char	 Vers[22];	// Version


	CTime	Tobt;	
	char 	 Tobf[3]; 	// 
	char 	 Toba[3]; 	// 

	char 	 Pcom[201];  //Added by Christine
	
	char     Coby[101];   //Collected By
	char     Hdag[101];   //Handling Agent
	char     Blto[201];   //Bill To
	char     Prmk[201];   //Payment Remark
	char	 Mopa[2];     //For new Mode of Payment implementation

	char     Dela[255];
	int      Vcnt;//Vip Count 
	char 	 Vial[1026]; 	// Liste der Zwischenstationen/-zeiten
	char     Rtow[2];
	char     Adho[2];

	//DataCreated by this class
	int      IsChanged;

	ROTATIONFLIGHTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Tobt = -1;
		Airb = -1;
		Slot = -1;
		Etdc = -1;
		Etai = -1;
		Etdi = -1;
		Etoa = TIMENULL;
		Etod = TIMENULL;
		Land = -1;
		Lstu = -1;
		Ofbl = -1;
		Onbe = -1;
		Onbl = -1;
		Stoa = -1;
		Stod = -1;
		Tifa = -1;
		Tifd = -1;
		Tmoa = -1;
		Nxti = -1;
		Paba = -1;
		Paea = -1;
		Pdba = -1;
		Pdea = -1;
		Gd1x = -1;
		Aird = -1;
		Cdat = -1;
		Ga1x = -1;
		Ga2x = -1;
		Gd1x = -1;
		Gd2x = -1;
		B1ba = -1;
		B1ea = -1;
		B2ba = -1;
		Vcnt = 0;
	}

}; // end ROTATIONFLIGHTDATA
	




/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Read and write flight data
//@See: CedaData
/*@Doc:
  Reads and writes flight data from and to database. Stores flight data in memory 
  and provides methods for searching and retrieving flights from its internal list. 
  Does some data preparations like pre-formatting the flight number, storing flight 
  number and gate information for the corresponding inbound/outbound flight.

  Data of this class has to be updated by CEDA through broadcasts. This procedure 
  and the necessary methods will be described later with the description of 
  broadcast handling.

  {\bf RotationCedaFlightData} handles data from the EIOHDL server process.
	See the specification for table descriptions.
*/
class RotationCedaFlightData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Keya/Keyd fields of all loaded flights.
    //CMapStringToPtr omKeyMap;
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omPosMap;
	CMapPtrToPtr omRkeyMap;
   //@ManMemo: ROTATIONFLIGHTDATA records read by ReadAllFlight().
    CCSPtrArray<ROTATIONFLIGHTDATA> omData;

	char pcmAftFieldList[2048];


// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in 
	  {\bf ROTATIONFLIGHTDATA}, the {\bf pcmTableName} with table name {\bf AFTLSG}, 
	  the data members {\bf pcmFieldList} contains a list of used fields of 
	  {\bf ROTATIONFLIGHTDATAStruct}.
    */
    //RotationCedaFlightData(CCSCedaCom *popCommHandler = NULL);  : CCSCedaData(&ogCommHandler)
    RotationCedaFlightData();
    //@ManMemo: Default destructor
	~RotationCedaFlightData();
	
	void UnRegister();
	void Register();



    //@ManMemo: Clear all flights.
    /*@Doc:
      Clear all flights.
      This method will clear {\bf omData} and {\bf omKeyMap}.
    */
	void ClearAll(void);

	void SetFieldList(void);
	

    // internal data access.
	bool AddFlightInternal(ROTATIONFLIGHTDATA *prpFlight, bool bpDdx = true);
	bool DeleteFlightInternal(long lpUrno, bool bpDdx = true);
	bool DeleteFlightsInternal(long lpRkey, bool bpDdx = true);

	bool ReadAllFlights(CString &opWhere, bool bpRotation );
	void SetPreSelection(CTime &opFrom, CTime &opTo, CString &opFtyps);
	bool PreSelectionCheck(CString &opData, CString &opFields, bool bpCheckOldNewVal = false, CString opSelection = "");

	bool ReadFlights(char *pcpSelection, bool bpDdx = true, bool bpIngoreView = false);
	bool ReadRkeys(CString &olRkeys, char *pcpSelection);

	bool UpdateFlight(ROTATIONFLIGHTDATA *prpFlight, ROTATIONFLIGHTDATA *prpFlightSave);
	bool UpdateFlight(CString opUrnoList, char *pcpFieldList, char *pcpDataList);
	bool SaveSpecial(char *pclSelection, char *pclField, char *pclValue);
	
	bool JoinFlightPreCheck(long &ll1Urno, long &ll2Urno);
	
	bool DeleteFlight(CString opUrnoList );
	bool DeleteFlight(long lpUrno);

    //@ManMemo: Handle Broadcasts for flights.
	void ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void ReloadFlights(void *vpDataPointer);

	void CheckResKonf(ROTATIONFLIGHTDATA *prpFlightOld, ROTATIONFLIGHTDATA *prpFlight);

	
	ROTATIONFLIGHTDATA *GetArrival(ROTATIONFLIGHTDATA *prpFlight);
	ROTATIONFLIGHTDATA *GetDeparture(ROTATIONFLIGHTDATA *prpFlight);
	ROTATIONFLIGHTDATA *GetJoinFlight(ROTATIONFLIGHTDATA *prpFlight);

	bool AddToKeyMap(ROTATIONFLIGHTDATA *prpFlight );
	bool DeleteFromKeyMap(ROTATIONFLIGHTDATA *prpFlight );
	ROTATIONFLIGHTDATA *GetFlightByUrno(long lpUrno);

	CString CreateFlno(CString opAlc, CString opFltn, CString opFlns);


	bool ProcessFlightUFR(BcStruct *prlBcStruct);
	bool ProcessFlightIFR(BcStruct *prlBcStruct);
	bool ProcessFlightISF(BcStruct *prlBcStruct);

	void ProcessCflChange(CFLDATA *prpCfl);


	bool CreateFlno(char* pcpFlno, CString &olFlno, CString &opAlc, CString &opFltn, CString &opFlns);


	CString SearchLFZ(CString olLFZ, CTime opTifa);


	void ProcessCcaChange(long *lpUrno);

	CedaDiaCcaData omCcaData;

	CedaCflData omCflData;

	void ReadAllCca();
	void ReadFlightPermits();

	void Debug();

	struct RKEYLIST
	{
		CCSPtrArray<ROTATIONFLIGHTDATA> Rotation;

	};
	
	bool bmRotation ;
	CString omFtyps;
	CTime omFrom;
	CTime omTo;

	bool bmReadAll ;

	CString omSelection;
	bool bmReadAllFlights;

};


#endif
