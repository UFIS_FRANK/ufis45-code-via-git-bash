// FlightDiagram.cpp : implementation file
// Modification History:
// 19-feb-01	rkr	PRF 1305 Dubai: Gantchart-Anzeige unter local unterstützt
//

#include <stdafx.h>
#include <FlightDiagram.h>
#include <FlightDiaPropertySheet.h>
#include <DiaCedaFlightData.h>
#include <FlightChart.h>
#include <FlightGantt.h>
#include <TimePacket.h>
#include <RotationISFDlg.h>
#include <PrivList.h>
#include <FlightSearchTableDlg.h>
#include <Utils.h>
#include <WoResTableDlg.h>
#include <SpotAllocation.h>
#include <SeasonDlg.h>
#include <buttonlistdlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static void FlightDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// FlightDiagram
int igAnsichtPageIndex;

IMPLEMENT_DYNCREATE(FlightDiagram, CFrameWnd)

FlightDiagram::FlightDiagram()
{
	igAnsichtPageIndex = -1;
	omMaxTrackSize = CPoint(4000/*1024*/, 1500);
	omMinTrackSize = CPoint(1024 / 4, 768 / 4);
	omViewer.SetViewerKey("POSDIA");
	omViewer.SelectView("<Default>");
    imStartTimeScalePos = 294;
    imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = -1;
	lmBkColor = lgBkColor;
	pogFlightDiagram = this;
	bmIsViewOpen = false;
	bgKlebefunktion = false;
	m_key = "DialogPosition\\FlightDiagram";

    lmBkColor = COLORREF(SILVER);;
    lmTextColor = COLORREF(BLACK);;
    lmHilightColor = COLORREF(BLACK);;


}

FlightDiagram::~FlightDiagram()
{
    omPtrArray.RemoveAll();
	pogFlightDiagram = NULL;
}


BEGIN_MESSAGE_MAP(FlightDiagram, CFrameWnd)
	//{{AFX_MSG_MAP(FlightDiagram)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
	ON_BN_CLICKED(IDC_INSERT, OnInsert)
    ON_WM_GETMINMAXINFO()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_WM_SIZE()
    ON_WM_HSCROLL()
    ON_BN_CLICKED(IDC_PREV, OnNextChart)
    ON_BN_CLICKED(IDC_NEXT, OnPrevChart)
    ON_WM_TIMER()
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
    ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
	ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
	ON_BN_CLICKED(IDC_ZEIT, OnZeit)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_INSERT2, OnInsert2)
	ON_BN_CLICKED(IDC_CHANGES, OnChanges)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()





////////////////////////////////////////////////////////////////////////
// FlightDiagram -- implementation of DDX call back function

static void FlightDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	FlightDiagram *polDiagram = (FlightDiagram *)popInstance;
	TIMEPACKET *polTimePacket = NULL;
	
	switch (ipDDXType)
	{
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;

	case STAFFDIAGRAM_UPDATETIMEBAND:
		polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;

	case SAS_VIEW_CHANGED:
        polDiagram->ViewSelChange( (char *) vpDataPointer);
		break;

	case DATA_RELOAD:
        polDiagram->OnViewSelChange();
		break;

	case SHOW_FLIGHT:
		polDiagram->ShowFlight((long *)vpDataPointer);

	case SAS_GET_CUR_VIEW:
		polDiagram->GetCurView((char *) vpDataPointer);
		break;
	}
}

void FlightDiagram::GetCurView( char * pcpView)
{
	strcpy(pcpView, omViewer.GetViewName());
}


/////////////////////////////////////////////////////////////////////////////
// FlightDiagram message handlers
void FlightDiagram::PositionChild()
{
    CRect olRect;
    CRect olChartRect;
    FlightChart *polChart;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
        polChart = (FlightChart *) omPtrArray.GetAt(ilIndex);
        
        if (ilIndex < imFirstVisibleChart)
        {
            polChart->ShowWindow(SW_HIDE);
            continue;
        }
        
        polChart->GetClientRect(&olChartRect);
        olChartRect.right = olRect.right;

        olChartRect.top = ilLastY;

        ilLastY += polChart->GetHeight();
        olChartRect.bottom = ilLastY;
        
        // check
        if ((polChart->GetState() != Minimized) &&
			(olChartRect.top < olRect.bottom) && (olChartRect.bottom > olRect.bottom))
        {
            olChartRect.bottom = olRect.bottom;
            polChart->SetState(Normal);
        }
        //
        
        polChart->MoveWindow(&olChartRect, TRUE);
        //RST polChart->MoveWindow(&olChartRect, FALSE);
		//RST polChart->ShowWindow(SW_SHOW);
	}
    
	omClientWnd.Invalidate(TRUE);
	//SetAllStaffAreaButtonsColor();
	OnUpdatePrevNext();
	UpdateTimeBand();
}


void FlightDiagram::SetTSStartTime(CTime opTSStartTime)
{
    omTSStartTime = opTSStartTime;

	CTime olTSStartTime(omTSStartTime);
	if(bgDailyLocal)
		ogBasicData.UtcToLocal(olTSStartTime);

    char clBuf[8];
    sprintf(clBuf, "%02d%02d%02d",
//        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
        olTSStartTime.GetDay(), olTSStartTime.GetMonth(), olTSStartTime.GetYear() % 100);
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}

CListBox *FlightDiagram::GetBottomMostGantt()
{
	// Check the size of the area for displaying charts.
	// Pichet used "omClientWnd" not the diagram itself, so we will get the size of this window
	CRect olClientRect;
	omClientWnd.GetClientRect(olClientRect);
	omClientWnd.ClientToScreen(olClientRect);

	// Searching for the bottommost chart
	FlightChart *polChart;
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (FlightChart *)omPtrArray[ilLc];
		CRect olRect, olChartRect;
		polChart->GetClientRect(olChartRect);
		polChart->ClientToScreen(olChartRect);
		if (!olRect.IntersectRect(&olChartRect, &olClientRect))
			break;
	}

	// Check if the chart we have found is a valid one
	--ilLc;
	if (!(0 <= ilLc && ilLc <= omPtrArray.GetSize()-1))
		return NULL;

	return &((FlightChart *)omPtrArray[ilLc])->omGantt;
}


int FlightDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

    SetTimer(0, (UINT) 60 * 1000, NULL);
    SetTimer(1, (UINT) 60 * 1000 * 2, NULL);

	omDialogBar.Create( this, IDD_FLIGHTDIAGRAM, CBRS_TOP, IDD_FLIGHTDIAGRAM );
	SetWindowPos(&wndTop/*Most*/, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

    omViewer.Attach(this);
	UpdateComboBox();


    CRect olRect; GetClientRect(&olRect);
	CTime olCurrentTime;
	olCurrentTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrentTime);

//	m_KlebeFkt.Create( "&Klebefunktion", SS_CENTER | WS_CHILD | WS_VISIBLE, 
//						CRect(olRect.left + 400, 6,olRect.left + 480, 23), this, IDC_KLEBEFKT);

    omTSStartTime = olCurrentTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);

    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

	//Die wollen local
    CTime olUCT = CTime::GetCurrentTime();//olCurrentTime;
    CTime olRealUTC = olCurrentTime;
    char olBuf[16];
    sprintf(olBuf, "%02d%02d/%02d%02dz",
        olUCT.GetHour(), olUCT.GetMinute(),        
        olRealUTC.GetHour(), olRealUTC.GetMinute()
    );

    
	
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);

	char pclTimes[36];
	char pclDate[36];
	
	CTime olUtcTimeTmp = CTime::GetCurrentTime();
	olUtcTimeTmp -= ogUtcDiff;
	
	if(ogUtcDiff != 0)
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
	}
	else
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
	}
    
	
	omTime.Create(pclTimes, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 192, 3, olRect.right - 90, 22), this);

    omDate.Create(pclDate, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 88, 3, olRect.right - 8, 22), this);
        
    omDate.SetTextColor(RGB(255,0,0));
	
	omTime.ShowWindow(SW_HIDE);
	omDate.ShowWindow(SW_HIDE);
	

    sprintf(olBuf, "%02d%02d%02d",  omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
    omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(ilPos, 35, ilPos + 80, 52), this);

    // CBitmapButton
    omBB1.Create("PREV", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 36, 21 + 5, olRect.right - 19, (21 + 5) + 25), this, IDC_PREV);
    omBB1.LoadBitmaps("PREVU", "PREVD", NULL, "NEXTX");

    omBB2.Create("NEXT", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 19, 21 + 5, olRect.right - 2, (21 + 5) + 25), this, IDC_NEXT);
    omBB2.LoadBitmaps("NEXTU", "NEXTD", NULL, "NEXTX");

	omBB1.ShowWindow(SW_HIDE);
	omBB2.ShowWindow(SW_HIDE);

	omTimeScale.lmBkColor = lgBkColor;
	omTimeScale.lmTextColor =lgTextColor;
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62), this, 0, NULL);


//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgDailyLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);


	if(bgDailyLocal)
		omTimeScale.UpdateCurrentTimeLine(olLocalTime);
	else
		omTimeScale.UpdateCurrentTimeLine(olRealUTC);


    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,0);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);
        
	// This will fix the bug for the horizontal scroll bar in the preplan mode
    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);

    FlightChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
	OnUpdatePrevNext();
    int ilLastY = 0;
    for (int ilI = 0; ilI < omViewer.GetGroupCount(); ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new FlightChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
        polChart->Create(NULL, "FlightChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);
        
        omPtrArray.Add(polChart);

        ilLastY += polChart->GetHeight();
    }

    OnTimer(0);

	// Register DDX call back function
	//TRACE("FlightDiagram: DDX Registration\n");
	ogDdx.Register(this, REDISPLAY_ALL, CString("STAFFDIAGRAM"),
		CString("Redisplay all from What-If"), FlightDiagramCf);	// for what-if changes
	ogDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("STAFFDIAGRAM"),
		CString("Update Time Band"), FlightDiagramCf);	// for updating the yellow lines

 	ogDdx.Register(this, DATA_RELOAD,CString(" "), CString("DATA_RELOAD"),FlightDiagramCf);

	ogDdx.Register(this, SHOW_FLIGHT, CString(""),CString(""), FlightDiagramCf);	// for updating the yellow lines

 	ogDdx.Register(this, SAS_VIEW_CHANGED,CString(" "), CString("DATA_RELOAD"),FlightDiagramCf);
 	ogDdx.Register(this, SAS_GET_CUR_VIEW,CString(" "), CString("DATA_RELOAD"),FlightDiagramCf);

	ogPosDiaFlightData.Register();	 


    CButton *polCB;

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ANSICHT);
	SetpWndStatAll(ogPrivList.GetStat("FLIGHTDIAGRAMM_CB_Ansicht"),polCB);

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_SEARCH);
	SetpWndStatAll(ogPrivList.GetStat("FLIGHTDIAGRAMM_CB_Search"),polCB);

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_INSERT);
	SetpWndStatAll(ogPrivList.GetStat("FLIGHTDIAGRAMM_CB_Insert"),polCB);
	#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
	#else
	{
	}
	#endif


	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_INSERT2);
	SetpWndStatAll(ogPrivList.GetStat("FLIGHTDIAGRAMM_CB_Insert2"),polCB);
	#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
	#else
	{
	}
	#endif

	// Create 'Changes'-Button
 	WINDOWPLACEMENT olWndPlace;
	CButton* polButton = (CButton *)omDialogBar.GetDlgItem(IDC_CHANGES);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_Changes.Create(GetString(IDS_CHANGES), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_CHANGES);
    m_CB_Changes.SetFont(polButton->GetFont());
	m_CB_Changes.EnableWindow(true);
	m_CB_Changes.SetSecState(ogPrivList.GetStat("DESKTOP_CB_History"));



	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);


	if (bgNewGantDefault)
	{
		char pclView[100];
		strcpy(pclView, "<Default>");
		ogDdx.DataChanged((void *)this, SAS_GET_CUR_VIEW,(void *)pclView );
		omViewer.SelectView(pclView);
		ViewSelChange(pclView);
	}
	else
	{
		char pclView[100];
		strcpy(pclView, "<Default>");
		ogDdx.DataChanged((void *)this, SAS_GET_CUR_VIEW,(void *)pclView );
		omViewer.SelectView(pclView);

		CTime olFrom = TIMENULL;
		CTime olTo = TIMENULL;
		CString olFtyps;
		omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
		if (olFrom != TIMENULL && olTo != TIMENULL)
		{
			omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
			omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
		}
		ViewSelChange(pclView);
	}




	SetWndPos();	

	// set caption
	CString olTimes;
	if (bgDailyLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING1257);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);

	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
//	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );
	MoveWindow(olRect);

	 return 0;

}

void FlightDiagram::SaveToReg() 
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void FlightDiagram::OnChanges()
{
	CallHistory(CString("FLTPOSITION;GATE;BELT;LOUNGE;CHECKIN"));
}

void FlightDiagram::UpdateChangesButton(CString& opText, COLORREF opColor)
{
	CString olTmp;
	olTmp.Format("%s (%s)", GetString(IDS_CHANGES),opText);

	m_CB_Changes.SetWindowText(olTmp);
	m_CB_Changes.SetColors(opColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Changes.UpdateWindow();
}


void FlightDiagram::OnBeenden() 
{
	ogPosDiaFlightData.UnRegister();	 
	DestroyWindow();	
}


void FlightDiagram::OnInsert()
{
	RotationISFDlg dlg(this);
	dlg.DoModal();
}

void FlightDiagram::OnInsert2()
{
	pogSeasonDlg->NewData(this, 0L, 0L, DLG_NEW, bgGatPosLocal);
}

void FlightDiagram::OnAnsicht()
{
 
	FlightDiaPropertySheet olDlg("POSDIA", this, &omViewer, 0, GetString(IDS_STRING1650));
	bmIsViewOpen = true;
	if(olDlg.DoModal() != IDCANCEL)
	{
		ogSpotAllocation.InitializePositionMatrix();

		if (olDlg.FilterChanged())
			LoadFlights();
		else
		{
			// upate status bar
			omStatusBar.SetPaneText(0, GetString(IDS_SAS_CHECK));
			omStatusBar.UpdateWindow();

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			ogKonflikte.SASCheckAll();
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			// upate status bar
			omStatusBar.SetPaneText(0, "");
			omStatusBar.UpdateWindow();
		}

 		omViewer.MakeMasstab();
		CTime olT = omViewer.GetGeometrieStartTime();
		CString olS = olT.Format("%d.%m.%Y-%H:%M");
		SetTSStartTime(olT);
		omTSDuration = omViewer.GetGeometryTimeSpan();
//rkr
		//Set begin to local if local
		CTime olTSStartTime = omTSStartTime;
		if(bgDailyLocal)
			ogBasicData.UtcToLocal(olTSStartTime);
    
		omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//		omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale.Invalidate(TRUE);
		ChangeViewTo(omViewer.GetViewName(), false);
		long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
		long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();

		if (llTotalMin > 0)
			nPos = nPos * 1000L / llTotalMin;

		SetScrollPos(SB_HORZ, int(nPos), TRUE);

		char pclView[100];
		strcpy(pclView, omViewer.GetViewName());
		ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)pclView );
	}
	bmIsViewOpen = false;
	UpdateComboBox();
	//PRF 8363
	SetFocusToDiagram();
}

void FlightDiagram::LoadFlights(char *pspView)
{
	if(pspView != NULL)
	{
		omViewer.SelectView(pspView);
	}
	else
	{
		omViewer.SelectView(omViewer.GetViewName());
	}
/*
	if(pspView == NULL)
	{
		CString olView = omViewer.GetViewName();
		pspView = olView.GetBuffer(0);
		omViewer.SelectView(pspView);
		if (strcmp(pspView, "<Default>") == 0)
			return;
	}
*/	
	


	CString olWhere;
	CTime olFrom;
	CTime olTo;
	CString olFtyps;

	bool blRotation;
	if (!omViewer.GetZeitraumWhereString(olWhere, blRotation, CString("") ))
		return;
	// PRF 8382
	CString  olstring = omViewer.GetUnifilterWhereString();
	olstring.TrimLeft();
	olstring.TrimRight();
	if(!olstring.IsEmpty())
	{
		olWhere = olWhere + olstring;	
	}

	if (bgMustHaveFLNO)
		olWhere += CString(" AND FLNO <> ' '");
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	omStatusBar.SetPaneText(0, GetString(IDS_STRING1205));
	omStatusBar.UpdateWindow();

	
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);


//	CTime olCurr = CTime::GetCurrentTime();
//	if(bgDailyLocal) ogBasicData.LocalToUtc(olCurr);


	CButton *polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ZEIT);

	CTime olCurr;
	CTime olT1 = olFrom;
	CTime olT2 = olTo;
	GetCurrentUtcTime(olCurr);

/*	if (bgDailyLocal)
	{
		ogBasicData.LocalToUtc(olT1);
		ogBasicData.LocalToUtc(olT2);
	}
*/
	CString olf=olT1.Format("%d.%m.%Y %H:%M:%S");
	CString olt=olT2.Format("%d.%m.%Y %H:%M:%S");
	CString olC=olCurr.Format("%d.%m.%Y %H:%M:%S");

	if(olT1 < olCurr && olT2 > olCurr)
//	if(olFrom < olCurr && olTo > olCurr)
	{
		polCB->EnableWindow(TRUE);
	}
	else
	{
		polCB->EnableWindow(FALSE);
	}


	ogPosDiaFlightData.SetPreSelection(olFrom, olTo, olFtyps);
	
	ogPosDiaFlightData.ReadAllFlights(olWhere, blRotation, "");

	// set internal variables
	omDuration = (olTo - olFrom);// + CTimeSpan(1, 0, 0, 0);
	omStartTime = olFrom;// - CTimeSpan(1, 0, 0, 0);
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();


}

void FlightDiagram::OnDestroy() 
{
	SaveToReg();
//	if (bgModal == TRUE)
//		return;
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	ogDdx.UnRegister(&omViewer, NOTUSED);

	// Unregister DDX call back function
	TRACE("FlightDiagram: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogDdx.UnRegister(this, NOTUSED);

	CFrameWnd::OnDestroy();
}

void FlightDiagram::OnClose() 
{
    // Ignore close -- This makes Alt-F4 keys no effect
	CFrameWnd::OnClose();
}

void FlightDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL FlightDiagram::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void FlightDiagram::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);
    
    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 27); dc.LineTo(olRect.right, 27);
    dc.MoveTo(olRect.left, 62); dc.LineTo(olRect.right, 62);

    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 63); dc.LineTo(olRect.right, 63);
    
    
    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 27);
    dc.LineTo(imStartTimeScalePos - 2, 63);

    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 27);
    dc.LineTo(imStartTimeScalePos - 1, 63);

    dc.SelectObject(polOldPen);
    // Do not call CFrameWnd::OnPaint() for painting messages
}


void FlightDiagram::OnSize(UINT nType, int cx, int cy) 
{
    CFrameWnd::OnSize(nType, cx, cy);
    
    // TODO: Add your message handler code here
    CRect olRect; GetClientRect(&olRect);

    CRect olBB1Rect(olRect.right - 36, 27 + 5, olRect.right - 19, (27 + 5) + 25);
    omBB1.MoveWindow(&olBB1Rect, TRUE);
    
    CRect olBB2Rect(olRect.right - 19, 27 + 5, olRect.right - 2, (27 + 5) + 25);
    omBB2.MoveWindow(&olBB2Rect, TRUE);

    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    // LeftTop, RightBottom
    omClientWnd.MoveWindow(&olRect, TRUE);

//	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
//	SetAllStaffAreaButtonsColor();

	GetClientRect(&olRect);
	omTime.MoveWindow( CRect(olRect.right - 192, 3, olRect.right - 90, 22));
    omDate.MoveWindow( CRect(olRect.right - 88, 3, olRect.right - 8, 22));
 
	PositionChild();
}
 
void FlightDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    long llTotalMin;
    int ilPos;

	CTime olTSStartTime = omTSStartTime;
	CUIntArray olLines;
	int ilCurrLine;

	FlightChart *polChart;
	for (int ilLc = 0; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (FlightChart *)omPtrArray[ilLc];

		ilCurrLine = polChart->omGantt.GetTopIndex();
		break;
	}

    switch (nSBCode)
    {
        case SB_LINEUP :
            //OutputDebugString("LineUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                ShowTime(omStartTime);
            }
            else
                ShowTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));

        break;
        
        case SB_LINEDOWN :
            //OutputDebugString("LineDown\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                ShowTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin+1), 0));
            }
            else
                ShowTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));

        break;
        
        case SB_PAGEUP :
            //OutputDebugString("PageUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                ShowTime(omStartTime);
            }
            else
                ShowTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

        break;
        
        case SB_PAGEDOWN :
            //OutputDebugString("PageDown\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);

            if (ilPos >= 1000)
            {
                ilPos = 1000;
                ShowTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
                ShowTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

        break;
        
        case SB_THUMBTRACK /* pressed, any drag time */:
            //OutputDebugString("ThumbTrack\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
            
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));

			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
            omTimeScale.Invalidate(TRUE);
            SetScrollPos(SB_HORZ, nPos, TRUE);

            omClientWnd.Invalidate(FALSE);
        break;
        case SB_THUMBPOSITION:	// the thumb was just released?
            omClientWnd.Invalidate(FALSE);


        case SB_TOP :
        //break;
        case SB_BOTTOM :
            //OutputDebugString("TopBottom\n\r");
        break;
        
        case SB_ENDSCROLL :
            //OutputDebugString("EndScroll\n\r");
            //OutputDebugString("\n\r");
        break;
    }

	for (ilLc = 0; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (FlightChart *)omPtrArray[ilLc];
		polChart->omGantt.SetTopIndex(ilCurrLine);
		break;
	}
}

void FlightDiagram::OnPrevChart()
{
    if (imFirstVisibleChart > 0)
    {
        imFirstVisibleChart--;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void FlightDiagram::OnNextChart()
{
    if (imFirstVisibleChart < omPtrArray.GetUpperBound())
    {
        imFirstVisibleChart++;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void FlightDiagram::OnFirstChart()
{
	imFirstVisibleChart = 0;
	OnUpdatePrevNext();
	PositionChild();
}

void FlightDiagram::OnLastChart()
{
	imFirstVisibleChart = omPtrArray.GetUpperBound();
	OnUpdatePrevNext();
	PositionChild();
}

void FlightDiagram::OnTimer(UINT nIDEvent)
{
	if(bmNoUpdatesNow == FALSE)
	{


		CTime olLocalTime = CTime::GetCurrentTime();
		CTime olUtcTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olUtcTime);

		char pclTimes[100];
		char pclDate[100];
		
		CTime olUtcTimeTmp = CTime::GetCurrentTime();
		olUtcTimeTmp -= ogUtcDiff;
		
		if(ogUtcDiff != 0)
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
		}
		else
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
		}

		if(bgDailyLocal)
			omTimeScale.UpdateCurrentTimeLine(olLocalTime);
		else
			omTimeScale.UpdateCurrentTimeLine(olUtcTime);



		omTime.SetWindowText(pclTimes);
		omTime.Invalidate(FALSE);

		omDate.SetWindowText(pclDate);
		omDate.Invalidate(FALSE);

		FlightChart *polChart;
		for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
		{
			polChart = (FlightChart *) omPtrArray.GetAt(ilIndex);

			if(bgDailyLocal)
				polChart->GetGanttPtr()->SetCurrentTime(olLocalTime);
			else
				polChart->GetGanttPtr()->SetCurrentTime(olUtcTime);


		}


		CFrameWnd::OnTimer(nIDEvent);
	}
}


void FlightDiagram::OnSearch()
{
	pogFlightSearchTableDlg->SetUrnoFilter( &ogPosDiaFlightData.omUrnoMap);
	pogFlightSearchTableDlg->Activate(omViewer.GetBaseViewName(), omViewer.GetViewName(), FLIGHTDIAGRAM);
}


void FlightDiagram::SetMarkTime(CTime opStartTime, CTime opEndTime)
{
		FlightChart *polChart;
		for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
		{
			polChart = (FlightChart *) omPtrArray.GetAt(ilIndex);
			polChart->SetMarkTime(opStartTime, opEndTime);
		}
}


LONG FlightDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

LONG FlightDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
    CString olStr;
    int ipGroupNo = HIWORD(lParam);
    int ipLineNo = LOWORD(lParam);
	CTime olTime = CTime((time_t) lParam);

    FlightChart *polFlightChart = (FlightChart *) omPtrArray.GetAt(ipGroupNo);
    FlightGantt *polFlightGantt = polFlightChart -> GetGanttPtr();

	int ilCurrLine = polFlightGantt->GetTopIndex();
	CWnd::LockWindowUpdate();

    switch (wParam)
    {
        // group message
        case UD_INSERTGROUP :
            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart++;
				OnUpdatePrevNext();
			}
            PositionChild();
        break;

        case UD_UPDATEGROUP :
            olStr = omViewer.GetGroupText(ipGroupNo);
            polFlightChart->GetChartButtonPtr()->SetWindowText(olStr);
            polFlightChart->GetChartButtonPtr()->Invalidate(FALSE);

            olStr = omViewer.GetGroupTopScaleText(ipGroupNo);
            polFlightChart->GetTopScaleTextPtr()->SetWindowText(olStr);
            polFlightChart->GetTopScaleTextPtr()->Invalidate(TRUE);
        break;
        
        case UD_DELETEGROUP :
            delete omPtrArray.GetAt(ipGroupNo);
            omPtrArray.RemoveAt(ipGroupNo);
            
            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart--;
				OnUpdatePrevNext();
			}
            PositionChild();
        break;

        // line message
        case UD_INSERTLINE :
            polFlightGantt->InsertString(ipLineNo, "");
			polFlightGantt->RepaintItemHeight(ipLineNo);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);
            PositionChild();
        break;
        
        case UD_UPDATELINE :
            polFlightGantt->RepaintVerticalScale(ipLineNo, TRUE);
			polFlightGantt->RepaintItemHeight(ipLineNo);
            polFlightGantt->RepaintGanttChart(ipLineNo);
            PositionChild();
        break;

        case UD_DELETELINE :
            polFlightGantt->DeleteString(ipLineNo);
			PositionChild();
        break;

        case UD_UPDATELINEHEIGHT :
            polFlightGantt->RepaintItemHeight(ipLineNo);
            PositionChild();
		break;
	}

		CWnd::UnlockWindowUpdate();
		int ilCnt = polFlightGantt->GetCount();
		if (ilCurrLine < ilCnt)
			polFlightGantt->SetTopIndex(ilCurrLine);


    return 0L;
}

void FlightDiagram::OnUpdatePrevNext(void)
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
//	SetFocus();

	if (imFirstVisibleChart == 0)
		GetDlgItem(IDC_NEXT)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_NEXT)->EnableWindow(TRUE);

	if (imFirstVisibleChart == omPtrArray.GetUpperBound())
		GetDlgItem(IDC_PREV)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_PREV)->EnableWindow(TRUE);
}

///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void FlightDiagram::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{

		olViewName = omViewer.SelectView();//ogCfgData.rmUserSetup.STCV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}

void FlightDiagram::ChangeViewTo(const char *pcpViewName,bool RememberPositions)
{
	if (bgNoScroll == TRUE)
		return;

	CUIntArray olLines;
	int ilCurrLine;

	FlightChart *polChart;
	for (int ilLc = 0; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (FlightChart *)omPtrArray[ilLc];

		ilCurrLine = polChart->omGantt.GetTopIndex();
		olLines.Add(ilCurrLine);
	}


	int ilDwRef = m_dwRef;
	AfxGetApp()->DoWaitCursor(1);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	CCSPtrArray <int> olChartStates;
	olChartStates.RemoveAll();

	CRect olRect; omTimeScale.GetClientRect(&olRect);

	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgDailyLocal)
		ogBasicData.UtcToLocal(olTSStartTime);

    omViewer.ChangeViewTo(pcpViewName, olTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));


    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
		if (RememberPositions == TRUE)
		{
			int ilState = ((FlightChart *)omPtrArray[ilIndex])->GetState();
			olChartStates.NewAt(ilIndex,ilState);
		}
		// Id 30-Sep-96
		// This will remove a lot of warning message when the user change view.
		// If we just delete a staff chart, MFC will produce two warning message.
		// First, Revoke not called before the destructor.
		// Second, calling DestroyWindow() in CWnd::~CWnd.
        //delete (FlightChart *)omPtrArray.GetAt(ilIndex);
		((FlightChart *)omPtrArray[ilIndex])->DestroyWindow();
    }
    omPtrArray.RemoveAll();

//  FlightChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
	if (RememberPositions == FALSE)
	{
	    imFirstVisibleChart = 0;
	}
    int ilLastY = 0;
    for (int ilI = 0; ilI < omViewer.GetGroupCount(); ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new FlightChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
		if (ilI < olChartStates.GetSize())
		{
			polChart->SetState(olChartStates[ilI]);
		}
		else
		{
				polChart->SetState(Maximized);
		}

        polChart->Create(NULL, "FlightChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);

		CTime olCurr = CTime::GetCurrentTime();
		//Die wollen local
		if(!bgDailyLocal)
			ogBasicData.LocalToUtc(olCurr);
		polChart->GetGanttPtr()->SetCurrentTime(olCurr);
        
        omPtrArray.Add(polChart);

        ilLastY += polChart->GetHeight();
    }

	if (olLines.GetSize() >0)
	{
		for (ilLc = 0; ilLc < omPtrArray.GetSize(); ilLc++)
		{
			polChart = (FlightChart *)omPtrArray[ilLc];
			polChart->omGantt.SetTopIndex(ilCurrLine);
			break;
		}
	}

	PositionChild();
	AfxGetApp()->DoWaitCursor(-1);
	olChartStates.DeleteAll();
	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
	//PRF 8363
	//SetFocusToDiagram();
}

void FlightDiagram::OnViewSelChange()
{
	//PRF 8363
	SetFocusToDiagram();
    char clText[64];
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	if(strcmp(clText, "<Default>") == 0)
		return;

	ogSpotAllocation.InitializePositionMatrix();
	LoadFlights(clText);

	omViewer.SelectView(clText);
	omViewer.MakeMasstab();
	CTime olT = omViewer.GetGeometrieStartTime();
	CString olS = olT.Format("%d.%m.%Y-%H:%M");
	SetTSStartTime(olT);
	omTSDuration = omViewer.GetGeometryTimeSpan();

    
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	if (llTotalMin > 0)
		nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
	
	
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgDailyLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	SetTSStartTime(omTSStartTime);
	ChangeViewTo(clText, false);

	ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)clText );

}

void FlightDiagram::ViewSelChange(char *pcpView)
{

	omViewer.SelectView(pcpView);

	
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->SetCurSel(polCB->FindString( 0, pcpView ) );

	UpdateDia();
	//PRF 8363
	SetFocusToDiagram();

}

bool FlightDiagram::UpdateDia()
{
	CString olViewName = omViewer.GetViewName();
	if (olViewName == "<Default>")
	{
		ogPosDiaFlightData.ClearAll();
		ResetStateVariables();
	}

	
	if(IsIconic())  //RST
	{
		return false;
	}
	


	// set internal variables
	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	if (olFrom != TIMENULL && olTo != TIMENULL)
	{
		omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
		omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
	}


	omViewer.MakeMasstab();

	CTime olT = omViewer.GetGeometrieStartTime();
//	ogBasicData.UtcToLocal(olT); //RSTL
	SetTSStartTime(olT);

	omTSDuration = omViewer.GetGeometryTimeSpan();
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if (!bgNewGantDefault)
	{
		if(bgGatPosLocal)
			ogBasicData.UtcToLocal(olTSStartTime);
	}

	if (bgNewGantDefault)
	{
		if (olViewName != "<Default>")
		{
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);
		}
		else
		{
			if(!bgGatPosLocal)
				ogBasicData.LocalToUtc(olTSStartTime);
		}
	}

	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
 
	if (bgNewGantDefault)
		omTimeScale.SetDisplayStartTime(olTSStartTime);

   omTimeScale.Invalidate(TRUE);
	ChangeViewTo(omViewer.GetViewName(), false);

	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	if (llTotalMin > 0)
		nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);

	/*
	char pclView[100];
	strcpy(pclView, omViewer.GetViewName());
	ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)pclView );
	*/

	pogWoResTableDlg->Rebuild();

	return true;
}


void FlightDiagram::ResetStateVariables()
{
	CTime olUtcTime;
	GetCurrentUtcTime(olUtcTime);

    omTSStartTime = olUtcTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);

    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

}


void FlightDiagram::OnCloseupView() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
}

void FlightDiagram::OnZeit()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();

	// TODO: Add your command handler code here
	CTime olTime = CTime::GetCurrentTime();

	if(!bgDailyLocal)	
		ogBasicData.LocalToUtc(olTime);

	//Die wollen local
	olTime -= CTimeSpan(0, 1, 0, 0);
	SetTSStartTime(olTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	if (llTotalMin > 0)
		nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
    omClientWnd.Invalidate(FALSE);
}


BOOL FlightDiagram::DestroyWindow() 
{

	if ((bmIsViewOpen))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}
	pogFlightDiagram = NULL; 

	ogDdx.UnRegister(&omViewer, NOTUSED);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	BOOL blRc = CWnd::DestroyWindow();
	return blRc;
}

/////////XXXXX
////////////////////////////////////////////////////////////////////////
// FlightDiagram keyboard handling

void FlightDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	switch (nChar)
	{
	case VK_UP:		// move the bottom most gantt chart up/down one line
	case VK_DOWN:
		CListBox *polGantt;
		if ((polGantt = GetBottomMostGantt()) != NULL)
			polGantt->SendMessage(WM_USERKEYDOWN, nChar);
		break;
	case VK_PRIOR:
		blIsControl? OnFirstChart(): OnPrevChart();
		break;
	case VK_NEXT:
		blIsControl? OnLastChart(): OnNextChart();
		break;
	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	default:
		omDialogBar.SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void FlightDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}


void FlightDiagram::RedisplayAll()
{
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
}

void FlightDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}

void FlightDiagram::UpdateTimeBand()
{
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		FlightChart *polChart = (FlightChart *)omPtrArray[ilLc];
		polChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
	}
}



void FlightDiagram::SetWndPos()
{
	CRect olRect;

	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_FLIGHTDIA_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;

	bottom = ::GetSystemMetrics(SM_CYSCREEN);

	top = 95;  


	if(ilWhichMonitor == 1)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}

	MoveWindow(CRect(left, top, right, ::GetSystemMetrics(SM_CYSCREEN)));


	//SetWindowPos(&wndTop,left , top, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);



}



void FlightDiagram::ShowFlight(long *lpUrno)
{
	CTime olStart;
	CTime olEnd;


	DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(*lpUrno);
	
	if(prlFlight != NULL)
	{
		CTime olTime = prlFlight->Tifa;
		if(bgDailyLocal)
			ogBasicData.UtcToLocal(olTime);
		
		ShowTime(olTime - CTimeSpan(0, 1, 0, 0));

		CUIntArray olGroups,   olLines;


		int ilRkey = prlFlight->Rkey;
		if(omViewer.FindLine(ilRkey, olGroups, olLines) == true)
		{
			FlightChart *polChart;
			for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
			{
				polChart = (FlightChart *) omPtrArray.GetAt(ilIndex);
				polChart->GetGanttPtr()->SetTopIndex(olLines[0]);
			}
		}

	}
}

bool FlightDiagram::ShowFlight(long lpFlightUrno, bool bp)
{
	DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(lpFlightUrno);
	if (prlFlight == NULL)
		return false;
	else
	{
		if (IsArrival(prlFlight->Org3, prlFlight->Des3))
		{
			return ShowFlight(prlFlight, NULL);
		}
		else
		{
			return ShowFlight(NULL, prlFlight);
		}
	}
}


bool FlightDiagram::ShowFlight(DIAFLIGHTDATA *prpAFlight, DIAFLIGHTDATA *prpDFlight)
{
	if (prpAFlight == NULL && prpDFlight == NULL) 
		return false;

	int ilLine = omViewer.AdjustBar(prpAFlight, prpDFlight);

	FlightChart *polChart;
	for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
	{
		polChart = (FlightChart *) omPtrArray.GetAt(ilIndex);
		polChart->GetGanttPtr()->SetTopIndex(ilLine);
		return true;
	}


	return true;

/*
	DIAFLIGHTDATA *prlFlight = prpAFlight;
	if (prlFlight == NULL)
		prlFlight = prpDFlight;
	
	if(prlFlight != NULL)
	{
		CTime olTime = prlFlight->Tifa;
		if(bgDailyLocal)
			ogBasicData.UtcToLocal(olTime);
		
		//ShowTime(olTime - CTimeSpan(0, 1, 0, 0));

		CUIntArray olGroups,   olLines;
		int ilRkey = prlFlight->Rkey;
		if(omViewer.FindLine(ilRkey, olGroups, olLines) == true)
		{
			FlightChart *polChart;
			for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
			{
				polChart = (FlightChart *) omPtrArray.GetAt(ilIndex);
				int index = olLines[0] - 2;
				index = max(index,0);
				polChart->GetGanttPtr()->SetTopIndex(index);
			}
		}

	}

	return true;
*/
}


void FlightDiagram::ShowTime(CTime& ropTime)
{
	if(omTSStartTime == ropTime)
		return;

	
	SetTSStartTime(ropTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	CTime olTSStartTime(omTSStartTime);
	if(bgDailyLocal)	
		ogBasicData.UtcToLocal(olTSStartTime);


	omTimeScale.SetDisplayStartTime(olTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

/*	CRect olRect;
	omTimeScale.GetClientRect(&olRect);
	omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
	PositionChild();
*/
    omClientWnd.Invalidate(FALSE);
}


void FlightDiagram::SetFocusToDiagram()
{
	//PRF 8363, Change the focus to gantt chart //
	FlightGantt *polFlightGantt;
	FlightChart *polFlightChart = (FlightChart *) omPtrArray.GetAt(0);
	if(polFlightChart != NULL)
		polFlightGantt = polFlightChart -> GetGanttPtr();

	if((polFlightGantt != NULL)  && CWnd::GetForegroundWindow() == this)
		polFlightGantt->SetFocus();
}