using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Ufis.Utils;
using Ufis.Data;
using System.Threading;
using System.IO;
using System.Diagnostics;

namespace FlightPermits
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class frmMain : System.Windows.Forms.Form
    {
        #region MyMembers

        private bool bmUpdateEnabled = true;
        private bool bmAutoSizeColumns = false;

        // the following are required when called from FIPS
        private string omUsername = "";
        private string omPassword = "";
        private string omFpeUrno = "";
        private string omFlight = "";
        private string omFpePermitNumber = "";
        private string omFipsAlc3 = "";
        private string omFipsACT3 = "";
        private string omFipsACT5 = "";
        private string omFipsFltn = "";
        private string omFipsFlns = "";
        private string omFipsAdid = "";
        private string omFipsRegn = "";
        private string omFipsCsgn = "";
        private string omFipsMode = "";
        private string omFipsSDate = "";
        private string omFipsVafr = "";
        private string omFipsVato = "";
        private string omFipsDoop = "";
        private string omFipsAdfp = "";
        private string omFipsMtow = "";

        private string omFrom = "";
        private string omTo = "";
        private string omAlc3 = "";
        private string omFlno = "";
        private string omFlns = "";
        private string omPermitNumber = "";
        private string omATDNumber = "";
        private string omRegistration = "";
        private bool[] bmDayOfOperation = new bool[8]; // where 0 = daily, 1 = mon, 2= tues etc

        private string omFpeFieldList = "URNO,VAFR,VATO,PERN,REGN,ADID,ALCO,FLNO,SUFX,DOOP,ATDN,DTGN,REPN,MTOW,PERR,OBLR,USRR,CDAT,LSTU,USEC,USEU,ADFP,CSGN,ACT3,ACT5";
        private string omFpeFieldLens = "10,14,14,32,12,1,3,10,2,7,25,6,12,10,2000,2000,2000,14,14,32,32,1,8,3,5";

        private string omAltFieldList = "URNO,ALC2,ALC3";
        private string omAltFieldLens = "10,2,3";

        private string omAcrFieldList = "REGN,MTOW";
        private string omAcrFieldLens = "12,10";

        private string omTableHeader = "Permit Nr.,Valid From,Valid To,Op.Days,Airl,Flight,S,A/D,Registr.,Adhoc,ATD Number,DTGN,Ref Permit Nr.,MTOW,Call Sign,A/C Type 3,A/C Type 5,Permit Rem.,Obligation Rem.,User Rem.,URNO";
        // note: if don't want to display the URNO on the table then set the column width to 0
        private string omTableLogicalFields = "PERN,VAFR,VATO,DOOP,ALCO,FLNO,SUFX,ADID,REGN,ADFP,ATDN,DTGN,REPN,MTOW,CSGN,ACT3,ACT5,PERR,OBLR,USRR,URNO";
        private string[] omTableFields = null;
        private string omTableHeaderLens = "150,135,135,70,45,65,15,35,75,50,100,50,100,50,90,100,100,200,200,200,150" /*"32,14,14,8,4,10,3,12,5,25,6,12,10,25,25,25,20"*/;

        private IDatabase omDB = null;
        private ITable omFpeTab = null;
        private ITable omAltTab = null; // airline type table containing ALC2/ALC3
        private ITable omAcrTab = null;
        public string[] args;

        private bool bmCalledFromCommandLine = false;

        public const int DayOfOperationDaily = 0;
        public const int DayOfOperationMonday = 1;
        public const int DayOfOperationTuesday = 2;
        public const int DayOfOperationWednesday = 3;
        public const int DayOfOperationThursday = 4;
        public const int DayOfOperationFriday = 5;
        public const int DayOfOperationSaturday = 6;
        public const int DayOfOperationSunday = 7;

        ArrayList omAirlineCodes = new ArrayList();

        private DE.UpdateFpeData delegateUpdateFpeData = null;

        #endregion MyMembers

        #region Controls

        private System.Windows.Forms.StatusBar statusBar1;
        private System.Windows.Forms.Panel panel1;
        private AxTABLib.AxTAB MainTable;
        #endregion Controls
        private Ufis.Utils.BCBlinker bcBlinker1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button FilterButton;
        private System.Windows.Forms.Button InsertButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button PrintButton;
        private AxAATLOGINLib.AxAatLogin LoginControl;
        private System.Windows.Forms.Button HelpButton;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Button CopyButton;
        private Button btnPermitLetter;
        private bool[] sortOrder = null;	// Sorting Order

        public frmMain()
        {
            InitializeComponent();

            DateTime olNow = DateTime.Now;
            SetTimeframe(olNow.ToString("yyyyMMdd000000"), olNow.ToString("yyyyMMdd235900"));
            omTableFields = omTableLogicalFields.Split(',');
        }

        /// <summary>
        /// Set the timeframe
        /// </summary>
        /// <param name="opFrom">From time format yyymmddhhmmss</param>
        /// <param name="opTo">To time format yyymmddhhmmss</param>
        private void SetTimeframe(string opFrom, string opTo)
        {
            omFrom = opFrom;
            omTo = opTo;
            UT.TimeFrameFrom = UT.CedaDateToDateTime(omFrom);
            UT.TimeFrameTo = UT.CedaDateToDateTime(omTo);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);

        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.statusBar1 = new System.Windows.Forms.StatusBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.MainTable = new AxTABLib.AxTAB();
            this.bcBlinker1 = new Ufis.Utils.BCBlinker();
            this.LoginControl = new AxAATLOGINLib.AxAatLogin();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnPermitLetter = new System.Windows.Forms.Button();
            this.CopyButton = new System.Windows.Forms.Button();
            this.HelpButton = new System.Windows.Forms.Button();
            this.PrintButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.InsertButton = new System.Windows.Forms.Button();
            this.FilterButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusBar1
            // 
            this.statusBar1.Location = new System.Drawing.Point(0, 601);
            this.statusBar1.Name = "statusBar1";
            this.statusBar1.Size = new System.Drawing.Size(936, 22);
            this.statusBar1.TabIndex = 1;
            this.statusBar1.Text = "Ready.";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.MainTable);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 40, 0, 0);
            this.panel1.Size = new System.Drawing.Size(936, 601);
            this.panel1.TabIndex = 2;
            // 
            // MainTable
            // 
            this.MainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTable.Location = new System.Drawing.Point(0, 40);
            this.MainTable.Name = "MainTable";
            this.MainTable.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("MainTable.OcxState")));
            this.MainTable.Size = new System.Drawing.Size(936, 561);
            this.MainTable.TabIndex = 0;
            this.MainTable.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.SendLButtonDblClick);
            // 
            // bcBlinker1
            // 
            this.bcBlinker1.BackColor = System.Drawing.SystemColors.Control;
            this.bcBlinker1.Dock = System.Windows.Forms.DockStyle.Right;
            this.bcBlinker1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bcBlinker1.Location = new System.Drawing.Point(892, 0);
            this.bcBlinker1.Name = "bcBlinker1";
            this.bcBlinker1.Size = new System.Drawing.Size(44, 40);
            this.bcBlinker1.TabIndex = 3;
            this.bcBlinker1.TabStop = false;
            // 
            // LoginControl
            // 
            this.LoginControl.Enabled = true;
            this.LoginControl.Location = new System.Drawing.Point(632, 8);
            this.LoginControl.Name = "LoginControl";
            this.LoginControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("LoginControl.OcxState")));
            this.LoginControl.Size = new System.Drawing.Size(100, 16);
            this.LoginControl.TabIndex = 4;
            this.LoginControl.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnPermitLetter);
            this.panel2.Controls.Add(this.CopyButton);
            this.panel2.Controls.Add(this.HelpButton);
            this.panel2.Controls.Add(this.PrintButton);
            this.panel2.Controls.Add(this.DeleteButton);
            this.panel2.Controls.Add(this.InsertButton);
            this.panel2.Controls.Add(this.FilterButton);
            this.panel2.Controls.Add(this.bcBlinker1);
            this.panel2.Controls.Add(this.LoginControl);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(936, 40);
            this.panel2.TabIndex = 3;
            // 
            // btnPermitLetter
            // 
            this.btnPermitLetter.Location = new System.Drawing.Point(441, 8);
            this.btnPermitLetter.Name = "btnPermitLetter";
            this.btnPermitLetter.Size = new System.Drawing.Size(89, 23);
            this.btnPermitLetter.TabIndex = 10;
            this.btnPermitLetter.Text = "Permit Letter";
            this.btnPermitLetter.Click += new System.EventHandler(this.btnPermitLetter_Click);
            // 
            // CopyButton
            // 
            this.CopyButton.Location = new System.Drawing.Point(184, 8);
            this.CopyButton.Name = "CopyButton";
            this.CopyButton.Size = new System.Drawing.Size(75, 23);
            this.CopyButton.TabIndex = 9;
            this.CopyButton.Text = "&Copy";
            this.CopyButton.Click += new System.EventHandler(this.CopyButton_Click);
            // 
            // HelpButton
            // 
            this.HelpButton.Location = new System.Drawing.Point(536, 8);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(75, 23);
            this.HelpButton.TabIndex = 8;
            this.HelpButton.Text = "&Help";
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // PrintButton
            // 
            this.PrintButton.Location = new System.Drawing.Point(360, 8);
            this.PrintButton.Name = "PrintButton";
            this.PrintButton.Size = new System.Drawing.Size(75, 23);
            this.PrintButton.TabIndex = 7;
            this.PrintButton.Text = "&Print";
            this.PrintButton.Click += new System.EventHandler(this.PrintButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(272, 9);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteButton.TabIndex = 6;
            this.DeleteButton.Text = "&Delete";
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // InsertButton
            // 
            this.InsertButton.Location = new System.Drawing.Point(96, 9);
            this.InsertButton.Name = "InsertButton";
            this.InsertButton.Size = new System.Drawing.Size(75, 23);
            this.InsertButton.TabIndex = 5;
            this.InsertButton.Text = "&Insert";
            this.InsertButton.Click += new System.EventHandler(this.InsertButton_Click);
            // 
            // FilterButton
            // 
            this.FilterButton.Location = new System.Drawing.Point(8, 8);
            this.FilterButton.Name = "FilterButton";
            this.FilterButton.Size = new System.Drawing.Size(75, 23);
            this.FilterButton.TabIndex = 4;
            this.FilterButton.Text = "&Filter";
            this.FilterButton.Click += new System.EventHandler(this.FilterButton_Click);
            // 
            // frmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(936, 623);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusBar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Flight Permits Rules";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            
            frmMain olFormMain = new frmMain();
            olFormMain.args = args;
            olFormMain.Text = Application.ProductName + "   Version: " + Application.ProductVersion + "   Timeframe: " + UT.TimeFrameFrom.ToString("dd.MM.yyyy HH:mm") + " - " + UT.TimeFrameTo.ToString("dd.MM.yyyy HH:mm");
            Application.Run(olFormMain);
        }

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, "Unhandled Thread Exception");
            // here you can log the exception ...
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show((e.ExceptionObject as Exception).Message, "Unhandled UI Exception");
            // here you can log the exception ...
        }

        private void Form1_Load_1(object sender, System.EventArgs e)
        {
            string olCommand = "";
            DE.LoginControl = LoginControl;
            
            if (args.Length == 0)
            {
                try
                {
                    if (LoginProcedure() == false)
                    {
                        Application.Exit();
                        return;
                    }
                }
                catch
                {
                    Application.Exit();
                    return;
                }
            }
            else
            {


                // notes the args can be set for debugging purposes in:
                // SolutionExplorer-FlightPermits-Properties-ConfigurationProperties-Debugging-CommandLineArguments
                // example "bch Password INSERT ADID,ALC3,FLTN,FLNS,REGN,VAFR,VATO.DOOP,MTOW A,THA,923,,,20041212000000,20041212235959,7,"
                bmCalledFromCommandLine = true;
                string olFieldList = "", olData = "";
                for (int i = 0; i < args.GetLength(0); i++)
                {
                    //MessageBox.Show(args[i]);
                    switch (i)
                    {
                        case 0:
                            omUsername = args[i];
                            break;
                        case 1:
                            omPassword = args[i];
                            break;
                        case 2:
                            olCommand = args[i];
                            break;
                        case 3:
                            olFieldList = args[i];
                            break;
                        case 4:
                            olData = args[i];
                            break;
                    }
                }
                //If password is blank, reassign the argument value to respective parameter
                if (args.Length == 4)
                {
                    olData = olFieldList;
                    olFieldList = olCommand;
                    olCommand = omPassword;
                    omPassword = "";
                }

                //omUsername = "UFIS$ADMIN";
                //omPassword = "Password";
                //olCommand = "PERMIT";
                //olFieldList = "ALC3,ACT3,ACT5,FLTN,FLNS,URNO,CSGN,REGN,MODE,ST,ADID,MTOW,DOOP";
                //olData = "ETD,773,B773,371,,,ETD371,A6ETI,0,20120327083500,D,.00,2"; 
                //MessageBox.Show(args.Length.ToString());
                //MessageBox.Show("User Name : " + omUsername + "\n" + "Password : " + omPassword + "\n" +
                //                  "Field List : " + olFieldList + "\n" + "Data : " + olData);
                int ilIndex = -1;
                if (olCommand == "INSERT")
                {
                    omFpeUrno = "";
                    omFlight = "";
                    omFipsRegn = "";

                    ilIndex = UT.GetItemNo(olFieldList, "ALC3");
                    if (ilIndex != -1)
                        omFipsAlc3 = UT.GetItem(olData, ilIndex, ",");
                    ilIndex = UT.GetItemNo(olFieldList, "FLTN");
                    if (ilIndex != -1)
                        omFipsFltn = UT.GetItem(olData, ilIndex, ",");
                    ilIndex = UT.GetItemNo(olFieldList, "FLNS");
                    if (ilIndex != -1)
                        omFipsFlns = UT.GetItem(olData, ilIndex, ",");
                    omFlight = omFipsAlc3 + omFipsFltn + omFipsFlns;
                   // ilIndex = UT.GetItemNo(olFieldList, "ADID");

                    
                    ilIndex = UT.GetItemNo(olFieldList, "ADID");
                    if (ilIndex != -1)
                        omFipsAdid = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "REGN");
                    if (ilIndex != -1)
                        omFipsRegn = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "VAFR");
                    if (ilIndex != -1)
                        omFipsVafr = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "VATO");
                    if (ilIndex != -1)
                        omFipsVato = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "DOOP");
                    if (ilIndex != -1)
                        omFipsDoop = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "MTOW");
                    if (ilIndex != -1)
                        omFipsMtow = UT.GetItem(olData, ilIndex, ",");
                    char[] x = new char[5];
                    x[0] = '0';
                    omFipsMtow.TrimStart(x);

                    omFipsAdfp = "X"; // Adhoc by default
                }
                else if (olCommand == "UPDATE")
                {
                    omFipsRegn = "";
                    string olAlc3 = "", olFltn = "", olFlns = "";
                    ilIndex = UT.GetItemNo(olFieldList, "ALC3");
                    if (ilIndex != -1)
                        olAlc3 = UT.GetItem(olData, ilIndex, ",");
                    ilIndex = UT.GetItemNo(olFieldList, "FLTN");
                    if (ilIndex != -1)
                        olFltn = UT.GetItem(olData, ilIndex, ",");
                    ilIndex = UT.GetItemNo(olFieldList, "FLNS");
                    if (ilIndex != -1)
                        olFlns = UT.GetItem(olData, ilIndex, ",");
                    omFlight = olAlc3 + olFltn + olFlns;

                    ilIndex = UT.GetItemNo(olFieldList, "URNO");
                    if (ilIndex != -1)
                        omFpeUrno = UT.GetItem(olData, ilIndex, ",");

                    if (olAlc3.Length <= 0 && olFltn.Length <= 0 && olFlns.Length <= 0)
                    {
                        omFpeUrno = olData.TrimStart(',');
                    }
                }
                else if (olCommand == "PERMIT")
                {
                    omFpeUrno = "";
                    omFlight = "";
                    omFipsRegn = "";

                    //If the first item is blank , the UT.GetItemNo() and UT.GetItem() are wrong 
                    
                    
                    ilIndex = UT.GetItemNo(olFieldList, "ALC3");
                    if (ilIndex != -1)
                        omFipsAlc3 = UT.GetItem(olData, ilIndex, ",");
                    
                    ilIndex = UT.GetItemNo(olFieldList, "ACT3");
                    if (ilIndex != -1)
                        omFipsACT3 = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "ACT5");
                    if (ilIndex != -1)
                        omFipsACT5 = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "FLTN");
                    if (ilIndex != -1)
                        omFipsFltn = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "FLNS");
                    if (ilIndex != -1)
                        omFipsFlns = UT.GetItem(olData, ilIndex, ",");

                   
                    omFlight = omFipsAlc3 + omFipsFltn + omFipsFlns;

                    ilIndex = UT.GetItemNo(olFieldList, "ADID");
                    if (ilIndex != -1)
                        omFipsAdid = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "CSGN");
                    if (ilIndex != -1)
                        omFipsCsgn = UT.GetItem(olData, ilIndex, ",");

                  

                    ilIndex = UT.GetItemNo(olFieldList, "REGN");
                    if (ilIndex != -1)
                        omFipsRegn = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "MODE");
                    if (ilIndex != -1)
                        omFipsMode = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "ST");
                    if (ilIndex != -1)
                        omFipsSDate = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "URNO");
                    if (ilIndex != -1)
                        omFpeUrno = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "PERN");
                    if (ilIndex != -1)
                        omPermitNumber = UT.GetItem(olData, ilIndex, ",");                   

                    ilIndex = UT.GetItemNo(olFieldList, "DOOP");
                    if (ilIndex != -1)
                        omFipsDoop = UT.GetItem(olData, ilIndex, ",");

                    ilIndex = UT.GetItemNo(olFieldList, "MTOW");
                    if (ilIndex != -1)
                        omFipsMtow = UT.GetItem(olData, ilIndex, ",");
                    char[] x = new char[5];
                    x[0] = '0';
                    omFipsMtow.TrimStart(x);

                }
                if (LoginProcedure() == false)
                {
                    Application.Exit();
                    return;
                }
            }
            
            DE.SetPrivs(FilterButton, "MainScreenFilter");
            DE.SetPrivs(InsertButton, "MainScreenInsert");
            DE.SetPrivs(DeleteButton, "MainScreenDelete");
            DE.SetPrivs(PrintButton, "MainScreenPrint");
            DE.SetPrivs(CopyButton, "MainScreenCopy");
            DE.SetPrivs(btnPermitLetter, "MainScreenPermitLetter");
          
            bmUpdateEnabled = (DE.LoginControl.GetPrivileges("MainScreenUpdate") == "1") ? true : false;
            
            DE.OnUpdateFpeData += new FlightPermits.DE.UpdateFpeData(OnUpdateFpeData);

            InitMainTable();

            for (int d = 0; d < 8; d++)
                bmDayOfOperation[d] = true;

            InitDB();
            LoadAltTab();
            if (UT.Hopo == "AUH") this.btnPermitLetter.Visible = true;
            else
            {
                this.btnPermitLetter.Visible = false;
                this.HelpButton.Location = new System.Drawing.Point(441, 8);
            }
          
            if (bmCalledFromCommandLine)
            {
                if (olCommand != "PERMIT")
                {
                    string olWhere = "";
                    if (omFpeUrno.Length > 0)
                    {
                        olWhere = "WHERE URNO = " + omFpeUrno;
                    }
                    else
                    {
                        olWhere = "Initialize only";
                    }
                    LoadFpeTab(olWhere);
                }
            }
            else
            {
                LoadFpeTab("");
                DisplayFpeTab();
            }

            DE.InitDBObjects(bcBlinker1);

            if (!bmCalledFromCommandLine)
            {
                DisplayFpeTab();
            }
            else
            {
                if (olCommand == "PERMIT")
                {
                    bool bExactMatch = false;
                    string olWhere = string.Empty;
                    string omFipsSDateUTC = string.Empty;
                    omFipsSDateUTC = UT.DateTimeToCeda(UT.LocalToUtc(UT.CedaFullDateToDateTime(omFipsSDate)));

                    //Case 3
                    /*if (omFipsCsgn.Length > 0)
                    {
                        if (omFipsMode == "1") //Adhoc flight Alco and Flno will be blank
                        {
                            olWhere = "WHERE URNO = '" + omFpeUrno + "' AND CSGN = '" + omFipsCsgn + "' AND VAFR <= " + omFipsSDateUTC + " AND VATO >= " + omFipsSDateUTC + " ORDER BY VAFR";
                        }
                        else//not adhoc flight Alco and Flno and will be there
                        {
                            if (omFipsAlc3.Length == 0 && omFipsFltn.Length == 0)
                            {
                                olWhere = "WHERE URNO = '" + omFpeUrno + "' AND CSGN = '" + omFipsCsgn + "' AND VAFR <= " + omFipsSDateUTC + " AND VATO >= " + omFipsSDateUTC + " ORDER BY VAFR";
                            }
                            else 
                            {
                                //olWhere = "WHERE URNO = '" + omFpeUrno + "' AND ALCO = '" + omFipsAlc3 + "' AND FLNO = '" + omFipsFltn + "' AND VAFR <= " + omFipsSDateUTC + " AND VATO >= " + omFipsSDateUTC + " ORDER BY VAFR";
                                olWhere = "WHERE URNO = '" + omFpeUrno + "' AND ALCO = '" + omFipsAlc3 +  "' AND VAFR <= " + omFipsSDateUTC + " AND VATO >= " + omFipsSDateUTC + " ORDER BY VAFR";
                            }
                        }
                    }
                    else
                    {
                        if (omFipsAlc3.Length == 0)
                        {
                            olWhere = "WHERE URNO = '" + omFpeUrno + "' AND VAFR <= " + omFipsSDateUTC + " AND VATO >= " + omFipsSDateUTC + " ORDER BY VAFR";
                        }
                        else if (omFipsAlc3.Length > 0 )
                        {
                            olWhere = "WHERE URNO = '" + omFpeUrno + "' AND ALCO = '" + omFipsAlc3 + "' AND VAFR <= " + omFipsSDateUTC + " AND VATO >= " + omFipsSDateUTC + " ORDER BY VAFR";
                        }
                    }
                    */
                   
                    olWhere = "WHERE URNO = '" + omFpeUrno + "'";

                    //MessageBox.Show(olWhere);
                    LoadPermitFpeTab(olWhere);

                    if (omFpeTab.Count == 1)
                    {
                        IRow olRow = omFpeTab.RowsByIndexValue("URNO", omFpeUrno)[0];

                        //Case 2
                        if (olRow["FLNO"] == omFipsFltn && olRow["CSGN"] == omFipsCsgn && olRow["SUFX"] == omFipsFlns)
                        {
                            //Case 1
                            if (UT.DateTimeToCeda(UT.LocalToUtc(UT.CedaFullDateToDateTime(olRow["VAFR"]))) == omFipsSDateUTC && UT.DateTimeToCeda(UT.LocalToUtc(UT.CedaFullDateToDateTime(olRow["VATO"]))) == omFipsSDateUTC)
                                bExactMatch = true;
                            else
                                bExactMatch = false;
                        }

                        if (olRow["FLNO"] == "")
                        {
                            olRow["FLNO"] = omFipsFltn;
                        }

                        if (olRow["ALCO"] == "")
                        {
                            olRow["ALCO"] = omFipsAlc3;
                        }
                        if (olRow["REGN"] == "")
                        {
                            olRow["REGN"] = omFipsRegn;
                        }
                    }
                    else if (omFpeTab.Count == 0)
                    {
                        //Case 4
                        bExactMatch = false;
                    }


                    IRow[] olRows;
                    olRows = omFpeTab.RowsByIndexValue("URNO", omFpeUrno);

                    if (omFpeUrno.Length > 0 && bExactMatch)
                    {

                        IRow olRow = olRows[0];
                        frmDetail olDetailDlg = new frmDetail();
                        olDetailDlg.Command = olCommand;
                        olDetailDlg.SetData(ref olRow, omAirlineCodes, ref omAcrTab, bExactMatch);
                        olDetailDlg.Text = Application.ProductName + " " + Application.ProductVersion + " - Flight " + omFlight;
                        olDetailDlg.ShowDialog();
                        if (olDetailDlg.DialogResult == DialogResult.OK)
                        {
                            if (omFipsMode == "1")
                            {
                                omFpeTab.Save(olRows[0]);
                            }
                            else
                            {
                                MessageBox.Show("Don't have permission to change the Record", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }

                        this.BeginInvoke((MethodInvoker)delegate { this.Close(); });
                        return;
                    }
                    else if (omFpeUrno.Length == 0 || !bExactMatch)
                    {
                        IRow olNewRow = omFpeTab.CreateEmptyRow();
                        string olUrno = olNewRow["URNO"];
                        olNewRow["URNO"] = "";
                        if (omFpeTab.Count > 0)
                        {
                            IRow olRow = olRows[0];
                            olNewRow["PERN"] = olRow["PERN"];
                        }
                        // values received from FIPS
                        olNewRow["ADID"] = omFipsAdid;
                        olNewRow["ALCO"] = omFipsAlc3;
                        olNewRow["FLNO"] = omFipsFltn;
                        olNewRow["SUFX"] = omFipsFlns;
                        olNewRow["REGN"] = omFipsRegn;
                        olNewRow["VAFR"] = omFipsSDate;
                        olNewRow["VATO"] = omFipsSDate;
                        olNewRow["DOOP"] = omFipsDoop;
                        olNewRow["ADFP"] = omFipsAdfp;
                        olNewRow["MTOW"] = omFipsMtow;
                        olNewRow["CSGN"] = omFipsCsgn;
                        olNewRow["ACT3"] = omFipsACT3;
                        olNewRow["ACT5"] = omFipsACT5;
                        olNewRow["PERN"] = omPermitNumber;
                        olNewRow["URNO"] = omFpeUrno;
                        

                        frmDetail olDetailDlg = new frmDetail();
                        olDetailDlg.Command = olCommand;
                        olDetailDlg.SetData(ref olNewRow, omAirlineCodes, ref omAcrTab, bExactMatch);
                        olDetailDlg.Text = Application.ProductName + " " + Application.ProductVersion + " - Flight " + omFlight;
                        olDetailDlg.ShowDialog();
                        if (olDetailDlg.DialogResult == DialogResult.OK)
                        {
                            if (omFipsMode == "1")
                            {
                                olNewRow["URNO"] = olUrno;
                                omFpeTab.Add(olNewRow);
                                omFpeTab.Save(olNewRow);
                            }
                            else
                            {
                                MessageBox.Show("Don't have permission to change the Record", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
                else
                {
                    IRow[] olRows;
                    olRows = omFpeTab.RowsByIndexValue("URNO", omFpeUrno);

                    if (omFpeUrno.Length > 0 && olRows.Length <= 0)
                    {
                        string olMess = "Error - no flight permits found for the flight <" + omFlight + "> URNO <" + omFpeUrno + ">";
                        MessageBox.Show(olMess, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (omFpeUrno.Length <= 0 && olCommand == "INSERT")
                    {
                        IRow olNewRow = omFpeTab.CreateEmptyRow();
                        string olUrno = olNewRow["URNO"];
                        olNewRow["URNO"] = "";

                        // values received from FIPS
                        olNewRow["ADID"] = omFipsAdid;
                        olNewRow["ALCO"] = omFipsAlc3;
                        olNewRow["FLNO"] = omFipsFltn;
                        olNewRow["SUFX"] = omFipsFlns;
                        olNewRow["REGN"] = omFipsRegn;
                        olNewRow["VAFR"] = omFipsVafr;
                        olNewRow["VATO"] = omFipsVato;
                        olNewRow["DOOP"] = omFipsDoop;
                        olNewRow["ADFP"] = omFipsAdfp;
                        olNewRow["MTOW"] = omFipsMtow;
                        olNewRow["PERN"] = omPermitNumber;
                        olNewRow["URNO"] = omFpeUrno;
                        frmDetail olDetailDlg = new frmDetail();
                        olDetailDlg.SetData(ref olNewRow, omAirlineCodes, ref omAcrTab,false);
                        olDetailDlg.Command = olCommand;
                        olDetailDlg.Text = Application.ProductName + " " + Application.ProductVersion + " - Insert";
                        olDetailDlg.ShowDialog();
                        if (olDetailDlg.DialogResult == DialogResult.OK)
                        {
                            //olNewRow["URNO"] = omDB.GetNextUrno(); // done in CreateEmptyRow unfortunately
                            olNewRow["URNO"] = olUrno;
                            omFpeTab.Add(olNewRow);
                            omFpeTab.Save(olNewRow);
                        }
                    }
                    else
                    {
                        IRow olRow = olRows[0];
                        frmDetail olDetailDlg = new frmDetail();
                        olDetailDlg.SetData(ref olRow, omAirlineCodes, ref omAcrTab,false);
                        olDetailDlg.Text = Application.ProductName + " " + Application.ProductVersion + " - Flight " + omFlight;
                        olDetailDlg.ShowDialog();
                        if (olDetailDlg.DialogResult == DialogResult.OK)
                        {
                            omFpeTab.Save(olRows[0]);
                        }
                    }
                }
                Application.Exit();
                return;
            }
            this.Activate();
        }

        private bool LoginProcedure()
        {
            bool blRc = true;
            string LoginAnsw = "";
            string tmpRegStrg = "";
            string strRet = "";
            //LoginControl.UfisComCtrl = UT.GetUfisCom();
            LoginControl.ApplicationName = Application.ProductName;
            LoginControl.InfoCaption = Application.ProductName;
            LoginControl.VersionString = Application.ProductVersion;
            LoginControl.InfoCaption = "Info about " + Application.ProductName;
            LoginControl.InfoButtonVisible = true;
            LoginControl.InfoUfisVersion = "Ufis Version 4.5";
            LoginControl.InfoAppVersion = Application.ProductName + " " + Application.ProductVersion.ToString();
            LoginControl.InfoAAT = Application.CompanyName;
            LoginControl.UserNameLCase = true;
            tmpRegStrg = Application.ProductName + ",";

            //FKTTAB:  SUBD,FUNC,FUAL,TYPE,STAT
            tmpRegStrg += "InitModu,InitModu,Initialisieren (InitModu),B,-";
            tmpRegStrg += ",Main Screen,MainScreenFilter,Filter,B,-";
            tmpRegStrg += ",Main Screen,MainScreenInsert,Insert,B,-";
            tmpRegStrg += ",Main Screen,MainScreenUpdate,Update,B,-";
            tmpRegStrg += ",Main Screen,MainScreenDelete,Delete,B,-";
            tmpRegStrg += ",Main Screen,MainScreenPrint,Print,B,-";
            tmpRegStrg += ",Main Screen,MainScreenCopy,Copy,B,-";
            tmpRegStrg += ",Main Screen,MainScreenPermitLetter,Permit Letter,B,-";

            tmpRegStrg += ",Detail Dialog,DetailDialogSave,Save,B,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogPrint,Print,B,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogPern,Permit Number,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogAdid,Arrival/Departure,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogAlco,Airline Code,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogFlno,Flight Number,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogSufx,Suffix,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogRegn,Registration,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogVafr,Valid From,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogVato,Valid Until,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogDoop,Day Of Operation,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogAhfp,Adhoc Flight Permit,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogAtdn,ATD Number,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogDtgn,DTGN,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogRepn,REPN,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogMtow,MTOW,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogPerr,Permit Remark,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogOblr,Obligation Remark,E,1";
            tmpRegStrg += ",Detail Dialog,DetailDialogUsrr,User Remark,E,1";

            LoginControl.RegisterApplicationString = tmpRegStrg;

            if (bmCalledFromCommandLine)
            {
                //strLoginRet must be "OK"
                //MessageBox.Show (omUsername + "\n" + omPassword);               
                string strLoginRet = LoginControl.DoLoginSilentMode(omUsername, omPassword);
                //MessageBox.Show(omUsername + "\n" + omPassword);
                //if (strLoginRet != "OK")
                //{
                //    string olErr = "Error logging " + omUsername + " into the application - please contact your system administrator.";
                //    MessageBox.Show(olErr, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    blRc = false;
                //}
            }
            else
            {
                strRet = LoginControl.ShowLoginDialog();
                LoginAnsw = LoginControl.GetPrivileges("InitModu");
                if (LoginAnsw != "" && strRet != "CANCEL")
                    blRc = true;
                else
                    blRc = false;
            }

            if (blRc)
            {
                UT.UserName = LoginControl.GetUserName();
            }

            return blRc;
        }


        private void OnUpdateFpeData(object sender, string Urno, State state)
        {
            if (state == State.Created)
            {
                IRow[] olRows = omFpeTab.RowsByIndexValue("URNO", Urno);
                for (int i = 0; i < olRows.Length; i++)
                    RefreshTableForFpeInsert(olRows[i], false);
            }
            else if (state == State.Modified)
            {
                IRow[] olRows = omFpeTab.RowsByIndexValue("URNO", Urno);
                for (int i = 0; i < olRows.Length; i++)
                    RefreshTableForFpeUpdate(olRows[i], false);
            }
            else if (state == State.Deleted)
            {
                RefreshTableForFpeDelete(Urno, false);
            }
        }

        private void RefreshTableForFpeDelete(string opUrno, bool bpSelect)
        {
            int ilSel = MainTable.GetCurrentSelected();
            int ilLine = GetMainTableLineByUrno(opUrno);
            if (ilLine != -1)
                MainTable.DeleteLine(ilLine);
            if (bmAutoSizeColumns)
                MainTable.AutoSizeColumns();
            if (bpSelect && ilSel != -1)
            {
                int ilCount = MainTable.GetLineCount();
                if (ilSel < ilCount)
                    MainTable.SetCurrentSelection(ilSel);
                else
                    MainTable.SetCurrentSelection(ilCount - 1);
            }
        }

        private void RefreshTableForFpeInsert(IRow opFpeRec, bool bpSelect)
        {
            if (opFpeRec != null)
            {
                int ilLine = GetMainTableLineByUrno(opFpeRec["URNO"]);
                if (ilLine != -1)
                {
                    RefreshTableForFpeUpdate(opFpeRec, bpSelect);
                }
                else if (IsPassFilter(opFpeRec))
                {
                    string olPern = opFpeRec["PERN"];
                    int i = 0;
                    for (; i < MainTable.GetLineCount(); i++)
                        if (MainTable.GetFieldValue(i, "PERN").CompareTo(olPern) > 0)
                            break;

                    MainTable.InsertTextLineAt(i, FormatFpeRec(opFpeRec), true);
                    if (bmAutoSizeColumns)
                        MainTable.AutoSizeColumns();
                    if (bpSelect)
                        MainTable.SetCurrentSelection(i);
                }
            }
        }

        private int GetMainTableLineByUrno(string opUrno)
        {
            int ilLineFound = -1;
            for (int i = 0; i < MainTable.GetLineCount(); i++)
            {
                if (MainTable.GetFieldValue(i, "URNO").CompareTo(opUrno) == 0)
                {
                    ilLineFound = i;
                    break;
                }
            }

            return ilLineFound;
        }

        private void RefreshTableForFpeUpdate(IRow opFpeRec, bool bpSelect)
        {
            if (opFpeRec != null)
            {
                int ilLine = GetMainTableLineByUrno(opFpeRec["URNO"]);
                if (ilLine != -1)
                {
                    MainTable.UpdateTextLine(ilLine, FormatFpeRec(opFpeRec), true);
                    if (bmAutoSizeColumns)
                        MainTable.AutoSizeColumns();
                    if (bpSelect)
                        MainTable.SetCurrentSelection(ilLine);
                }
                else
                {
                    RefreshTableForFpeInsert(opFpeRec, bpSelect);
                }
            }
        }

        private void InitMainTable()
        {
            MainTable.HeaderString = omTableHeader;
            MainTable.EnableHeaderSizing(true);
            MainTable.ResetContent();
            MainTable.HeaderLengthString = omTableHeaderLens;
            MainTable.LogicalFieldList = omTableLogicalFields;
            MainTable.LifeStyle = true;
            MainTable.ShowHorzScroller(true);
            MainTable.DateTimeSetColumnFormat(UT.GetItemNo(MainTable.LogicalFieldList, "VAFR"), "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' 'hh':'mm");
            MainTable.DateTimeSetColumnFormat(UT.GetItemNo(MainTable.LogicalFieldList, "VATO"), "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' 'hh':'mm");

            //Sorting
            this.sortOrder = new bool[17];
            for (int i = 0; i < this.sortOrder.Length; i++)
            {
                sortOrder[i] = false;
            }
        }

        private void InitDB()        
        {
            
            {
                statusBar1.Text = "Connecting to the server...";

                omDB = UT.GetMemDB();
                omDB.IndexUpdateImmediately = true;

                //Read the entries from Ceda.ini
                Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
                UT.ServerName = myIni.IniReadValue("GLOBAL", "HOSTNAME");
                UT.Hopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");


                omDB.Connect(UT.ServerName, UT.UserName, UT.Hopo, "TAB", "BCH_Test");
                omFpeTab = omDB.Bind("FPE", "FPE", omFpeFieldList, omFpeFieldLens, omFpeFieldList);
                omAltTab = omDB.Bind("ALT", "ALT", omAltFieldList, omAltFieldLens, omAltFieldList);
                omAcrTab = omDB.Bind("ACR", "ACR", omAcrFieldList, omAcrFieldLens, omAcrFieldList);
                statusBar1.Text = "Ready";
            }
            
        }

        private void LoadFpeTab(string opWhere)
        {
            statusBar1.Text = "Loading FPETAB...";
            Cursor oldCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            if (opWhere != "Initialize only")
            {
                if (opWhere.Length == 0)
                {
                    opWhere = "WHERE VAFR <= '" + omTo + "' AND VATO >= '" + omFrom + "' ORDER BY VAFR";
                }
                omFpeTab.Clear();
                omFpeTab.Load(opWhere);
                omFpeTab.CreateIndex("URNO", "URNO");
            }
            omFpeTab.TimeFields = "VAFR,VATO,CDAT,LSTU";
            omFpeTab.TimeFieldsInitiallyInUtc = true;
            omFpeTab.TimeFieldsCurrentlyInUtc = false;

            statusBar1.Text = "Ready";
            Cursor.Current = oldCursor;
        }


        private void LoadPermitFpeTab(string opWhere)
        {
            statusBar1.Text = "Loading FPETAB...";
            Cursor oldCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            if (opWhere != "Initialize only")
            {
                omFpeTab.Clear();
                omFpeTab.Load(opWhere);
                omFpeTab.CreateIndex("URNO", "URNO");
            }
            omFpeTab.TimeFields = "VAFR,VATO,CDAT,LSTU";
            omFpeTab.TimeFieldsInitiallyInUtc = true;
            omFpeTab.TimeFieldsCurrentlyInUtc = false;

            statusBar1.Text = "Ready";
            Cursor.Current = oldCursor;
        }

        private void LoadAltTab()
        {
            statusBar1.Text = "Loading ALTTAB...";
            Cursor oldCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            omAltTab.Clear();
            omAltTab.Load("ORDER BY ALC2,ALC3");
            omAirlineCodes.Add(new AirlineCode("", ""));
            for (int i = 0; i < omAltTab.Count; i++)
            {
                IRow olRow = omAltTab[i];
                omAirlineCodes.Add(new AirlineCode(olRow["ALC2"], olRow["ALC3"]));
            }

            statusBar1.Text = "Ready";
            Cursor.Current = oldCursor;
        }

        private void DisplayFpeTab()
        {
            statusBar1.Text = "Displaying data...";
            MainTable.ResetContent();
            // omFpeTab.FillVisualizer(MainTable);
            for (int i = 0; i < omFpeTab.Count; i++)
            {
                string strValues = "";
                IRow olRow = omFpeTab[i];
                if (IsPassFilter(olRow))
                {
                    strValues += FormatFpeRec(olRow);
                    MainTable.InsertTextLine(strValues, false);
                }
            }

            MainTable.ShowHorzScroller(true);
            if (bmAutoSizeColumns)
                MainTable.AutoSizeColumns();
            MainTable.Sort("PERN", true, true);
            statusBar1.Text = "Ready";
        }

        private string FormatFpeRec(IRow opFpeRec)
        {
            string olFormattedFpeRec = "";
            for (int s = 0; s < omTableFields.Length; s++)
            {
                if (s != 0) olFormattedFpeRec += ",";
                olFormattedFpeRec += Format(opFpeRec[omTableFields[s]]);
            }

            return olFormattedFpeRec;
        }

        private string Format(string opVal)
        {
            if (opVal.Length > 0)
            {
                if (opVal.IndexOf('\x0002') != -1) //Replace the End of Line('\r\n') string in the place of '\x0002'
                    opVal = opVal.Replace('\x0002'.ToString(), "\r\n");
            }
            return opVal;

            return "      ";
        }

        private bool IsPassFilter(IRow opRow)
        {
            if (!bmDayOfOperation[DayOfOperationDaily]) // not daily
            {
                bool blFound = false;
                string olFreq = opRow["DOOP"];
                for (int i = 0; i < olFreq.Length; i++)
                {
                    string x = olFreq[i].ToString();
                    int d = int.Parse(x);
                    if (bmDayOfOperation[d])
                    {
                        blFound = true;
                        break;
                    }
                }
                if (!blFound)
                    return false;
            }

            if (omAlc3.Length > 0 && opRow["ALCO"] != omAlc3)
                return false;

            if (omFlno.Length > 0 && opRow["FLNO"] != omFlno)
                return false;

            if (omFlns.Length > 0 && opRow["SUFX"] != omFlns)
                return false;

            if (omPermitNumber.Length > 0 && opRow["PERN"] != omPermitNumber)
                return false;
            if (omATDNumber.Length > 0 && opRow["ATDN"] != omATDNumber)
                return false;
            if (omRegistration.Length > 0 && opRow["REGN"] != omRegistration)
                return false;

            return true;
        }

        private bool GetFilter()
        {
            bool blTimeChanged = false;
            frmFilter olFilter = new frmFilter();
            olFilter.omFrom = omFrom;
            olFilter.omTo = omTo;
            olFilter.bmDaily = bmDayOfOperation[DayOfOperationDaily];
            olFilter.bmMon = bmDayOfOperation[DayOfOperationMonday];
            olFilter.bmTue = bmDayOfOperation[DayOfOperationTuesday];
            olFilter.bmWed = bmDayOfOperation[DayOfOperationWednesday];
            olFilter.bmThu = bmDayOfOperation[DayOfOperationThursday];
            olFilter.bmFri = bmDayOfOperation[DayOfOperationFriday];
            olFilter.bmSat = bmDayOfOperation[DayOfOperationSaturday];
            olFilter.bmSun = bmDayOfOperation[DayOfOperationSunday];
            olFilter.omFlno = omFlno;
            olFilter.omFlns = omFlns;
            olFilter.omPermitNumber = omPermitNumber;
            olFilter.omATDNumber = omATDNumber;
            olFilter.omRegistration = omRegistration;
            olFilter.omAirlineCodes = omAirlineCodes;
            olFilter.omAlc3 = omAlc3;
            olFilter.ShowDialog();
            if (olFilter.DialogResult == DialogResult.OK)
            {

                if (olFilter.omFrom != omFrom || olFilter.omTo != omTo)
                {
                    SetTimeframe(olFilter.omFrom, olFilter.omTo);
                    blTimeChanged = true;
                }
                bmDayOfOperation[DayOfOperationDaily] = olFilter.bmDaily;
                bmDayOfOperation[DayOfOperationMonday] = olFilter.bmMon;
                bmDayOfOperation[DayOfOperationTuesday] = olFilter.bmTue;
                bmDayOfOperation[DayOfOperationWednesday] = olFilter.bmWed;
                bmDayOfOperation[DayOfOperationThursday] = olFilter.bmThu;
                bmDayOfOperation[DayOfOperationFriday] = olFilter.bmFri;
                bmDayOfOperation[DayOfOperationSaturday] = olFilter.bmSat;
                bmDayOfOperation[DayOfOperationSunday] = olFilter.bmSun;
                omAlc3 = olFilter.omAlc3;
                omFlno = olFilter.omFlno;
                omFlns = olFilter.omFlns;
                omPermitNumber = olFilter.omPermitNumber;
                omATDNumber = olFilter.omATDNumber;
                omRegistration = olFilter.omRegistration;
            }

            return blTimeChanged;
        }

        private void SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
        {

            if (bmUpdateEnabled)
            {
                string strUrno = MainTable.GetFieldValue(e.lineNo, "URNO");
                IRow[] olRows = omFpeTab.RowsByIndexValue("URNO", strUrno);

                // Clicking on the Table Header
                if (olRows.Length <= 0)
                {
                    //MessageBox.Show("No line selected", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //Sorting Functionality
                    this.sortOrder[e.colNo] = !this.sortOrder[e.colNo];
                    MainTable.Sort(e.colNo.ToString(), this.sortOrder[e.colNo], true);
                    MainTable.Refresh();
                }
                else
                {
                    IRow olRow = olRows[0];
                    frmDetail olDetailDlg = new frmDetail();
                    olDetailDlg.Command = "APP";
                    olDetailDlg.SetData(ref olRow, omAirlineCodes, ref omAcrTab,false); 
                    olDetailDlg.Text = "Update " + Application.ProductName + " " + Application.ProductVersion;
                    olDetailDlg.ShowDialog();
                    if (olDetailDlg.DialogResult == DialogResult.OK)
                    {
                        RefreshTableForFpeUpdate(olRows[0], true);
                    }
                }
            }
            else
            {
                MessageBox.Show("The update function has been deactivated in BDPS-SEC.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void FilterButton_Click(object sender, System.EventArgs e)
        {
            if (GetFilter())
            {
                LoadFpeTab("");
            }
            DisplayFpeTab();
            this.Text = Application.ProductName + "   Version: " + Application.ProductVersion + "   Timeframe: " + UT.TimeFrameFrom.ToString("dd.MM.yyyy HH:mm") + " - " + UT.TimeFrameTo.ToString("dd.MM.yyyy HH:mm");
        }

        private void InsertButton_Click(object sender, System.EventArgs e)
        {
            IRow olNewRow = omFpeTab.CreateEmptyRow();
            string olUrno = olNewRow["URNO"];
            olNewRow["URNO"] = "";
            if (UT.Hopo == "AUH") olNewRow["PERN"] = "Permit Reqd";
            else olNewRow["PERN"] = "";
            DateTime olStart = DateTime.Now;
            olStart = olStart.Subtract(new TimeSpan(0, olStart.Hour, olStart.Minute, olStart.Second));
            DateTime olEnd = olStart.Add(new TimeSpan(364, 23, 59, 59));
            olNewRow["VAFR"] = olStart.ToString("yyyyMMdd000000");
            olNewRow["VATO"] = olEnd.ToString("yyyyMMdd235900");

            frmDetail olDetailDlg = new frmDetail();
            olDetailDlg.Command = "APP";
            olDetailDlg.SetData(ref olNewRow, omAirlineCodes, ref omAcrTab,false);
            olDetailDlg.Text = "Insert " + Application.ProductName + " " + Application.ProductVersion;
            olDetailDlg.ShowDialog();
            if (olDetailDlg.DialogResult == DialogResult.OK)
            {
                //olNewRow["URNO"] = omDB.GetNextUrno(); // done in CreateEmptyRow unfortunately
                olNewRow["URNO"] = olUrno;
                omFpeTab.Add(olNewRow);
                omFpeTab.Save(olNewRow);
                RefreshTableForFpeInsert(olNewRow, true);
            }
        }

        private void DeleteButton_Click(object sender, System.EventArgs e)
        {
            int ilSelLine = MainTable.GetCurrentSelected();
            string strUrno = MainTable.GetFieldValue(ilSelLine, "URNO");
            IRow[] olRows = omFpeTab.RowsByIndexValue("URNO", strUrno);
            if (olRows.Length <= 0)
            {
                MessageBox.Show("No line selected", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult olResult = MessageBox.Show("Are you sure that you really want to delete the selected lines?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (olResult == DialogResult.Yes)
                {
                    for (int r = 0; r < olRows.Length; r++)
                    {
                        string olUrno = olRows[r]["URNO"];
                        olRows[r].Status = State.Deleted;
                        omFpeTab.Save(olRows[r]);
                        RefreshTableForFpeDelete(olUrno, true);
                    }
                }
            }
        }

        private void PrintButton_Click(object sender, System.EventArgs e)
        {
            rptMain olRptMain = new rptMain(MainTable);
            Ufis.Utils.frmPrintPreview olPrintPreview = new Ufis.Utils.frmPrintPreview(olRptMain);
            olPrintPreview.Show();
        }

        private void HelpButton_Click(object sender, System.EventArgs e)
        {
            ShowHelp();
        }

        private void ShowHelp()
        {
            IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");

            string HelpFilePath = myIni.IniReadValue("GLOBAL", "HelpDirectory");
            if (HelpFilePath.Length <= 0)
                HelpFilePath = @"C:\Ufis\Help";

            string HelpFileName = myIni.IniReadValue("FLIGHTPERMITS", "HELPFILE");
            if (HelpFileName.Length <= 0)
                HelpFileName = @"FlightPermits.chm";

            string HelpFile = HelpFilePath + @"\" + HelpFileName;

            Help.ShowHelp(this, HelpFile);
        }

        private void frmMain_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
        {
            ShowHelp();
        }

        /// <summary>
        /// This Function invokes with Copy button press. Opens the FrmDetail dialog with the 
        /// data of the selected ine in the table.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyButton_Click(object sender, EventArgs e)
        {
            int ilSelLine = MainTable.GetCurrentSelected();
            string strUrno = MainTable.GetFieldValue(ilSelLine, "URNO");
            IRow[] olRows = omFpeTab.RowsByIndexValue("URNO", strUrno);
            if (olRows.Length <= 0)
            {
                MessageBox.Show("No line selected", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                IRow olNewRow = omFpeTab.CreateEmptyRow();
                string olUrno = olNewRow["URNO"];
                olNewRow["URNO"] = "";
                olNewRow = olRows[0].CopyRaw();

                frmDetail olDetailDlg = new frmDetail();
                olDetailDlg.Command = "APP";
                olDetailDlg.SetData(ref olNewRow, omAirlineCodes, ref omAcrTab,false);
                olDetailDlg.Text = "Copy " + Application.ProductName + " " + Application.ProductVersion;
                olDetailDlg.SetCopyFlag();
                olDetailDlg.ShowDialog();
                if (olDetailDlg.DialogResult == DialogResult.OK)
                {
                    olNewRow["URNO"] = olUrno;
                    omFpeTab.Add(olNewRow);
                    omFpeTab.Save(olNewRow);
                    RefreshTableForFpeInsert(olNewRow, true);
                }
            }

        }

        private void btnPermitLetter_Click(object sender, EventArgs e)
        {
            int ilSelLine = MainTable.GetCurrentSelected();
            string strUrno = MainTable.GetFieldValue(ilSelLine, "URNO");
            string strPern = MainTable.GetFieldValue(ilSelLine, "PERN");
            IRow[] olRows = omFpeTab.RowsByIndexValue("URNO", strUrno);
            if (olRows.Length <= 0)
            {
                MessageBox.Show("No line selected", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (strPern == "" || strPern == "Permit Reqd")
            {
                MessageBox.Show("No permit letter for selected item.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    string AppPath = Application.StartupPath.ToString();
                    AppPath = AppPath + @"\Ufis.FlightPermits.exe";
                    string sLocalUTC = UT.IsTimeInUtc == true ? "U" : "L";
                    Process myProcess = new Process();
                    if (System.IO.File.Exists(AppPath))
                    {
                        if (strUrno != "")
                        //Process.Start(fileName, strUrno);
                        {
                            myProcess.StartInfo.Arguments = "PERN" + "|" + UT.UserName + "|" + LoginControl.GetUserPassword().ToString() + "|" + sLocalUTC + "|" + strUrno;
                            myProcess.StartInfo.UseShellExecute = false;
                            myProcess.StartInfo.FileName = AppPath;
                            myProcess.StartInfo.CreateNoWindow = true;
                            myProcess.Start();
                            myProcess.WaitForExit();
                        }
                        else
                            MessageBox.Show("No Permit Exists");
                    }
                    else
                    {

                        MessageBox.Show("There is no exe found");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }


        }
    }
}
