VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form DataPool 
   Caption         =   "DataPool"
   ClientHeight    =   6600
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13455
   ControlBox      =   0   'False
   Icon            =   "DataPool.frx":0000
   LinkTopic       =   "Form4"
   LockControls    =   -1  'True
   ScaleHeight     =   6600
   ScaleWidth      =   13455
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox Check2 
      Caption         =   "ScrollBars"
      Height          =   225
      Left            =   9360
      TabIndex        =   21
      Top             =   90
      Width           =   1125
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Auto Resize"
      Height          =   225
      Left            =   7950
      TabIndex        =   20
      Top             =   90
      Width           =   1245
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   4920
      TabIndex        =   19
      Top             =   30
      Width           =   2955
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Resize"
      Height          =   285
      Left            =   2040
      TabIndex        =   15
      Top             =   30
      Width           =   945
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   8
      Left            =   7590
      TabIndex        =   14
      Top             =   690
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   13
      Top             =   6315
      Width           =   13455
      _ExtentX        =   23733
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Load"
      Enabled         =   0   'False
      Height          =   285
      Left            =   1050
      TabIndex        =   12
      Top             =   30
      Width           =   945
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Save"
      Enabled         =   0   'False
      Height          =   285
      Left            =   60
      TabIndex        =   11
      Top             =   30
      Width           =   945
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3030
      TabIndex        =   4
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   4
      Left            =   3870
      TabIndex        =   3
      Top             =   690
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin VB.TextBox TableName 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   3930
      TabIndex        =   2
      ToolTipText     =   "Full Table Name"
      Top             =   30
      Width           =   915
   End
   Begin VB.TextBox TlxTabFields 
      Height          =   285
      Left            =   60
      TabIndex        =   1
      Tag             =   "32,32,32,32,32,32,32,32,32,32,32,32,32"
      ToolTipText     =   "List of TableFields (read from SYSTAB)"
      Top             =   360
      Width           =   13335
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   690
      Width           =   3825
      _Version        =   65536
      _ExtentX        =   6747
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   5
      Left            =   3870
      TabIndex        =   5
      Top             =   2100
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   1
      Left            =   0
      TabIndex        =   6
      Top             =   2100
      Width           =   3825
      _Version        =   65536
      _ExtentX        =   6747
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   6
      Left            =   3870
      TabIndex        =   7
      Top             =   3510
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   2
      Left            =   0
      TabIndex        =   8
      Top             =   3510
      Width           =   3825
      _Version        =   65536
      _ExtentX        =   6747
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   7
      Left            =   3870
      TabIndex        =   9
      Top             =   4920
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   3
      Left            =   0
      TabIndex        =   10
      Top             =   4920
      Width           =   3825
      _Version        =   65536
      _ExtentX        =   6747
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   9
      Left            =   7590
      TabIndex        =   16
      Top             =   2100
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   10
      Left            =   7590
      TabIndex        =   17
      Top             =   3510
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   11
      Left            =   7590
      TabIndex        =   18
      Top             =   4920
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TlkTab 
      Height          =   2775
      Index           =   0
      Left            =   11310
      TabIndex        =   22
      Top             =   690
      Width           =   2085
      _Version        =   65536
      _ExtentX        =   3678
      _ExtentY        =   4895
      _StockProps     =   64
   End
   Begin TABLib.TAB TfnTab 
      Height          =   2775
      Index           =   0
      Left            =   11310
      TabIndex        =   23
      Top             =   3510
      Width           =   2085
      _Version        =   65536
      _ExtentX        =   3678
      _ExtentY        =   4895
      _StockProps     =   64
   End
End
Attribute VB_Name = "DataPool"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_Snap As CSnapDialog
Public ShowLoadProgress As Boolean
Dim ShowFullTab As Boolean
Dim CurTab As TABLib.Tab
Dim UrnoCol As Long
Public Sub InitDataPool()
    Dim clData As String
    Dim clRecDat As String
    Dim clFldLst As String
    Dim count As Integer
    Dim retStr As String
    Dim empRec As String
    Dim lenList As String
    Dim AlgList As String
    Dim i As Integer
    clFldLst = ""
    count = -1
    If CedaIsConnected = True Then
        retStr = UfisServer.aCeda.CallServer("RT", "SYSTAB", "FINA", clData, "WHERE TANA='TLX' ORDER BY FINA", "120")
        count = UfisServer.aCeda.GetBufferCount - 1
        For i = 0 To count
            clRecDat = UfisServer.aCeda.GetBufferLine(i)
            If (Trim(clRecDat) <> "") Then
                clData = GetItem(clRecDat, 1, ",")
                clFldLst = clFldLst & clData & ","
            End If
        Next i
    Else
        If SaveLocalTestData Then LoadTestData -1
        If Trim(TlxTabFields.Text) = "" Then TlxTabFields.Text = "ALC3,BEME,CDAT,FKEY,FLNS,FLNU,FLTN,HOPO,LSTU,MORE,MSNO,MSTX,PRFL,SERE,STAT,TIME,TKEY,TTYP,TXNO,TXT1,TXT2,URNO,USEC,USEU,STYP,WSTA,FLDA"
    End If
    If clFldLst <> "" Then
        If InStr(clFldLst, "STYP") = 0 Then clFldLst = clFldLst & "STYP,"
        If InStr(clFldLst, "WSTA") = 0 Then clFldLst = clFldLst & "WSTA,"
        If InStr(clFldLst, "FLDA") = 0 Then clFldLst = clFldLst & "FLDA,"
        TlxTabFields.Text = Left(clFldLst, Len(clFldLst) - 1)
    End If
    TableName.Text = "TLX" & UfisServer.TblExt.Text
    empRec = CreateEmptyLine(TlxTabFields.Text)
    lenList = empRec
    lenList = Replace(lenList, ",", "14,", 1, -1)
    lenList = lenList & "14,14,14,14"
    AlgList = Replace(lenList, "14", "L", 1, -1, vbBinaryCompare)
    For i = 0 To TelexData.UBound
        TelexData(i).ResetContent
        TelexData(i).HeaderString = TlxTabFields.Text
        TelexData(i).HeaderLengthString = lenList
        TelexData(i).ColumnWidthString = lenList
        TelexData(i).HeaderAlignmentString = AlgList
        TelexData(i).ColumnAlignmentString = AlgList
        TelexData(i).LogicalFieldList = TlxTabFields.Text
        TelexData(i).FontName = "Courier"
        TelexData(i).HeaderFontSize = 14
        TelexData(i).FontSize = 14
        TelexData(i).LineHeight = 15
        'TelexData(i).ShowHorzScroller True
        TelexData(i).AutoSizeByHeader = True
    Next i
    UrnoCol = CLng(GetRealItemNo(TlxTabFields.Text, "URNO"))
    BrowserColumns = ""
    BrowserColumns = BrowserColumns & CStr(GetRealItemNo(DataPool.TlxTabFields, "STAT")) & ","
    BrowserColumns = BrowserColumns & CStr(GetRealItemNo(DataPool.TlxTabFields, "WSTA")) & ","
    BrowserColumns = BrowserColumns & CStr(GetRealItemNo(DataPool.TlxTabFields, "TTYP")) & ","
    BrowserColumns = BrowserColumns & CStr(GetRealItemNo(DataPool.TlxTabFields, "STYP")) & ","
    BrowserColumns = BrowserColumns & CStr(GetRealItemNo(DataPool.TlxTabFields, "CDAT")) & ","
    BrowserColumns = BrowserColumns & CStr(GetRealItemNo(DataPool.TlxTabFields, "TXNO")) & ","
    BrowserColumns = BrowserColumns & CStr(GetRealItemNo(DataPool.TlxTabFields, "PRFL")) & ","
    BrowserColumns = BrowserColumns & CStr(GetRealItemNo(DataPool.TlxTabFields, "URNO"))
    clFldLst = ""
    clData = ""
    If CedaIsConnected = True Then
        retStr = UfisServer.aCeda.CallServer("RT", "SYSTAB", "FINA", clData, "WHERE TANA='AFT' AND TYPE='DATE' ORDER BY FINA", "120")
        count = UfisServer.aCeda.GetBufferCount - 1
        For i = 0 To count
            clRecDat = UfisServer.aCeda.GetBufferLine(i)
            If (Trim(clRecDat) <> "") Then
                clData = GetItem(clRecDat, 1, ",")
                clFldLst = clFldLst & clData & ","
            End If
        Next i
    End If
    CedaTimeFields = clFldLst
    clFldLst = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "ADD_TMPL_TIME_FIELDS", "####")
    CedaTimeFields = CedaTimeFields & "," & clFldLst
    TemplateDelayNames = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEMPLATE_DELAY_NAMES", "DCD1,DCD2,DTD1,DTD2")
    TemplateDelayTypes = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEMPLATE_DELAY_TYPES", "DCD1,DCD2,DTD1,DTD2")
    TemplatePaxNames = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEMPLATE_PAX_NAMES", "PAX1,PAX2,PAX3")
    TemplatePaxTypes = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEMPLATE_PAX_TYPES", "PAX1,PAX2,PAX3")
    
End Sub
Private Sub InitCedaConfig()
Dim retStr As String
Dim clData As String
Dim clRecDat As String
Dim count As Integer
Dim i As Integer
    clData = ""
    count = -1
    If CedaIsConnected = True Then
        retStr = UfisServer.aCeda.CallServer("GFRC", "", "", clData, "WHERE [CONFIG]", "120")
        count = UfisServer.aCeda.GetBufferCount - 1
    End If
    For i = 0 To count
        clRecDat = UfisServer.aCeda.GetBufferLine(i)
        If (Trim(clRecDat) <> "") Then
            'MsgBox clRecDat
            'clData = GetItem(clRecDat, 1, ",")
        End If
    Next i
End Sub
Private Sub btnClose_Click()
    Me.Hide
End Sub

Public Sub SaveTestData(Index As Integer)
    Dim CurDataFile As String
    'CurDataFile = "c:\tmp\TlxFields.txt"
    CurDataFile = UFIS_TMP & "\TlxFields.txt"
    Open CurDataFile For Output As #1
    Print #1, TlxTabFields.Text
    Close 1
    If Index >= 0 Then
        'CurDataFile = "c:\tmp\TlxData" & CStr(Index) & ".txt"
        CurDataFile = UFIS_TMP & "\TlxData" & CStr(Index) & ".txt"
        TelexData(Index).WriteToFile CurDataFile, False
    End If
End Sub
Public Sub LoadTestData(Index As Integer)
    Dim CurDataFile As String
    Dim tmpData As String
    TableName.Text = "TLXTAB"
    'CurDataFile = "c:\tmp\TlxFields.txt"
    CurDataFile = UFIS_TMP & "\TlxFields.txt"
    Open CurDataFile For Input As #1
    Line Input #1, tmpData
    TlxTabFields.Text = tmpData
    Close 1
    If Index >= 0 Then
        TelexData(Index).ResetContent
        'CurDataFile = "c:\tmp\TlxData" & CStr(Index) & ".txt"
        CurDataFile = UFIS_TMP & "\TlxData" & CStr(Index) & ".txt"
        TelexData(Index).ReadFromFile CurDataFile
    End If
End Sub

Private Sub Check1_Click()
    If Check1.Value = 1 Then
        If ShowFullTab = True Then
            CurTab.AutoSizeByHeader = True
            CurTab.AutoSizeColumns
        End If
        Check1.Value = 0
    End If
End Sub

Private Sub Check2_Click()
    Dim i As Integer
    If Check2.Value = 1 Then
        For i = 0 To TelexData.UBound
            TelexData(i).ShowHorzScroller True
        Next
        Check2.Value = 0
    End If
End Sub

Private Sub Command3_Click()
    Dim i As Integer
    For i = 0 To TelexData.UBound
        TelexData(i).AutoSizeColumns
    Next
End Sub

Private Sub Form_Load()
    If Not ShutDownRequested Then
        If Not ApplIsInDesignMode Then
            Set m_Snap = New CSnapDialog
            m_Snap.hwnd = Me.hwnd
        End If
        InitDataPool
        InitTlkTab
        InitTfnTab
        'InitCedaConfig
    Else
        End
        'Unload Me
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Not ShutDownRequested Then
        If UnloadMode = 0 Then
            Cancel = CheckCancelExit(Me, 1)
        End If
    End If
End Sub

Private Sub Form_Resize()
    Dim NewVal As Long
    If ShowFullTab Then
        NewVal = Me.ScaleWidth - (CurTab.Left * 2)
        If NewVal > 300 Then CurTab.Width = NewVal
        NewVal = Me.ScaleHeight - CurTab.Top - StatusBar1.Height
        If NewVal > 300 Then CurTab.Height = NewVal
    End If
End Sub

Private Sub TelexData_PackageReceived(Index As Integer, ByVal lpPackage As Long)
    Static MaxPack As Long
    Static MaxLine As Long
    Dim tmpCapt As String
    MaxLine = MaxLine + lpPackage
    MaxPack = MaxPack + 1
    tmpCapt = CStr(MaxLine) & " Telexes Loaded [" & CStr(MaxPack) & "]"
    If ShowLoadProgress Then
        TelexPoolHead.fraLoadProgress.Caption = tmpCapt
        TelexPoolHead.fraLoadProgress.Refresh
        DoEvents
    End If
    If lpPackage < 500 Then
        MaxPack = 0
        MaxLine = 0
    End If
End Sub

Private Sub TelexData_SendLButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim NewVal As Long
    Dim NewTag As String
    If LineNo < 0 Then
        If TelexData(Index).Tag = "" Then
            Text1.Text = "STORAGE MEMORY " & CStr(Index)
            NewTag = ""
            NewTag = NewTag & CStr(TelexData(Index).Left) & ","
            NewTag = NewTag & CStr(TelexData(Index).Top) & ","
            NewTag = NewTag & CStr(TelexData(Index).Width) & ","
            NewTag = NewTag & CStr(TelexData(Index).Height) & ","
            TelexData(Index).Tag = NewTag
            TelexData(Index).Top = TelexData(0).Top
            TelexData(Index).Left = TelexData(0).Left
            TelexData(Index).ZOrder
            Set CurTab = TelexData(Index)
            ShowFullTab = True
            Form_Resize
            StatusBar1.ZOrder
        Else
            Text1.Text = ""
            NewTag = TelexData(Index).Tag
            TelexData(Index).Left = Val(GetItem(NewTag, 1, ","))
            TelexData(Index).Top = Val(GetItem(NewTag, 2, ","))
            TelexData(Index).Width = Val(GetItem(NewTag, 3, ","))
            TelexData(Index).Height = Val(GetItem(NewTag, 4, ","))
            StatusBar1.ZOrder
            ShowFullTab = False
            TelexData(Index).Tag = ""
        End If
    End If
End Sub
Public Function DataTabCreateEmptyLine(Index As Integer) As Long
    Dim i As Integer
    Dim tmpFields As String
    Dim tmpLine As String
    Dim tmpData As String
    tmpFields = TelexData(Index).LogicalFieldList
    tmpLine = ""
    i = 1
    tmpData = GetItem(tmpFields, i, ",")
    While tmpData <> ""
        tmpLine = tmpLine & ","
        i = i + 1
        tmpData = GetItem(tmpFields, i, ",")
    Wend
    TelexData(Index).InsertTextLine tmpLine, False
    DataTabCreateEmptyLine = TelexData(Index).GetLineCount - 1
End Function
Public Sub InsertOnlineTelex(Index, cpFields As String, cpData As String)
    Dim NewLine As Long
    NewLine = DataPool.DataTabCreateEmptyLine(9)
    DataPool.TelexData(9).SetFieldValues NewLine, cpFields, cpData
End Sub
Public Sub UpdateOnlineTelex(cpFields As String, cpData As String, cpSelKey As String)
    Dim tmpUrno As String
    Dim HitList As String
    Dim LineNo As Long
    tmpUrno = GetFieldValue("URNO", cpData, cpFields)
    If tmpUrno = "" Then tmpUrno = GetUrnoFromSqlKey(cpSelKey)
    If tmpUrno <> "" Then
        HitList = TelexData(9).GetLinesByColumnValue(UrnoCol, tmpUrno, 0)
        If HitList <> "" Then
            LineNo = Val(HitList)
            TelexData(9).SetFieldValues LineNo, cpFields, cpData
        End If
    End If
End Sub
Public Function ReleaseOnlineTelex(TlxUrno As String) As Long
    Dim HitList As String
    Dim LineNo As Long
    Dim LineNo8 As Long
    Dim TlxRec As String
    Dim tmpType As String
    Dim tmpText As String
    Dim tmpTxt1 As String
    Dim tmpTxt2 As String
    Dim tmpData As String
    LineNo = -1
    If TlxUrno <> "" Then
        HitList = TelexData(9).GetLinesByColumnValue(UrnoCol, TlxUrno, 0)
        If HitList <> "" Then
            LineNo = Val(HitList)
            TlxRec = TelexData(9).GetLineValues(LineNo)
            HitList = TelexData(8).GetLinesByColumnValue(UrnoCol, TlxUrno, 0)
            If HitList <> "" Then
                LineNo8 = Val(HitList)
                TelexData(8).DeleteLine LineNo8
            End If
            TelexData(8).InsertTextLine TlxRec, False
            TelexData(9).DeleteLine LineNo
            LineNo = TelexData(8).GetLineCount - 1
            tmpType = TelexData(8).GetFieldValue(LineNo, "TTYP")
            If tmpType = "KRIS" Then
                tmpText = TelexData(8).GetFieldValue(LineNo, "TXT1")
                tmpText = tmpText & TelexData(8).GetFieldValue(LineNo, "TXT2")
                GetKeyItem tmpData, tmpText, "<=SCREENCONTENT=>", "<=/SCREENCONTENT=>"
                If Len(tmpData) <= 4000 Then
                    tmpTxt1 = tmpData
                    tmpTxt2 = ""
                Else
                    tmpTxt1 = Left(tmpData, 4000)
                    tmpTxt2 = Mid(tmpData, 4001)
                End If
                TelexData(8).SetFieldValues LineNo, "TXT1,TXT2", tmpTxt1 & "," & tmpTxt2
            End If
        End If
    End If
    ReleaseOnlineTelex = LineNo
End Function
Public Sub ClearOnlineTelex(TlxUrno As String)
    Dim HitList As String
    Dim LineNo As Long
    Dim TlxRec As String
    LineNo = -1
    If TlxUrno <> "" Then
        HitList = TelexData(9).GetLinesByColumnValue(UrnoCol, TlxUrno, 0)
        If HitList <> "" Then
            LineNo = Val(HitList)
            TelexData(9).DeleteLine LineNo
        End If
    End If
End Sub

Private Sub TelexData_SendMouseMove(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal flags As String)
    Dim tmpCode As String
    tmpCode = "DATA.TABTLX." & CStr(Index)
    If MouseOverObjCode <> tmpCode Then Set MouseOverTabOcx = TelexData(Index)
    SetMouseWheelObjectCode tmpCode
    'Text1.Text = "STORAGE MEMORY " & CStr(Index)
End Sub

Private Sub InitTlkTab()
    Dim FldList As String
    Dim SizList As String
    Dim AlgList As String
    Dim i As Integer
    'ActFldLstI = "URNO,HOPO,TURN,FURN,ADID,FLNO,FDAT,DATC"
    'ActConditionI = "WHERE TURN = " & TlxUrnoFromRedirect
    FldList = "URNO,HOPO,TURN,FURN,ADID,FLNO,FDAT,DATC"
    SizList = "0020,0020,0020,0020,0020,0020,0020,0020"
    AlgList = "L,L,L,L,L,L,L,L"
    For i = 0 To TlkTab.UBound
        TlkTab(i).ResetContent
        TlkTab(i).LogicalFieldList = FldList
        TlkTab(i).HeaderString = FldList
        TlkTab(i).HeaderLengthString = SizList
        TlkTab(i).ColumnAlignmentString = AlgList
        TlkTab(i).HeaderAlignmentString = AlgList
        TlkTab(i).ColumnWidthString = SizList
        TlkTab(i).AutoSizeByHeader = True
    Next
End Sub
Private Sub InitTfnTab()
    'ActFldLstI = "URNO,HOPO,FNAM,FDAT,FREM,TURN,DATC"
    'ActConditionI = "WHERE TURN = " & TlxUrnoFromRedirect
    Dim FldList As String
    Dim SizList As String
    Dim AlgList As String
    Dim i As Integer
    FldList = "URNO,HOPO,FNAM,FDAT,FREM,TURN,DATC"
    SizList = "0020,0020,0020,0020,0020,0020,0020"
    AlgList = "L,L,L,L,L,L,L"
    For i = 0 To TfnTab.UBound
        TfnTab(i).ResetContent
        TfnTab(i).LogicalFieldList = FldList
        TfnTab(i).HeaderString = FldList
        TfnTab(i).HeaderLengthString = SizList
        TfnTab(i).ColumnAlignmentString = AlgList
        TfnTab(i).HeaderAlignmentString = AlgList
        TfnTab(i).ColumnWidthString = SizList
        TfnTab(i).AutoSizeByHeader = True
    Next

End Sub
