VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form AssignFlight 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Assign Telex to Flights or Aircrafts"
   ClientHeight    =   4830
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11085
   Icon            =   "AssignFlight.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4830
   ScaleWidth      =   11085
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame2 
      Caption         =   "Assign Flight (Direct Link)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4335
      Left            =   5130
      TabIndex        =   35
      Top             =   120
      Width           =   4965
      Begin VB.CheckBox chkLinkAssign 
         Caption         =   "Assign"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3990
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   3930
         Width           =   855
      End
      Begin VB.CheckBox chkLinkLoad 
         Caption         =   "Load"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3990
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   3630
         Width           =   855
      End
      Begin VB.Frame LinkListFrame 
         Enabled         =   0   'False
         Height          =   2355
         Left            =   120
         TabIndex        =   39
         Top             =   1860
         Width           =   3795
         Begin TABLib.TAB LinkFlights 
            Height          =   2265
            Left            =   0
            TabIndex        =   40
            Top             =   90
            Width           =   3795
            _Version        =   65536
            _ExtentX        =   6694
            _ExtentY        =   3995
            _StockProps     =   64
            HeaderString    =   "DLH,2460,F"
            HeaderLengthString=   "26,31,10,30,30,30,30,30"
            FontName        =   "Courier New"
         End
      End
      Begin VB.CheckBox chkLinkNew 
         Caption         =   "Activate"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3990
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   510
         Width           =   855
      End
      Begin VB.Frame LinkFlightFrame 
         Caption         =   "Search Flight"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   120
         TabIndex        =   37
         Top             =   1170
         Width           =   3795
         Begin VB.Frame LinkFlightCover 
            BorderStyle     =   0  'None
            Height          =   315
            Left            =   120
            TabIndex        =   48
            Top             =   240
            Visible         =   0   'False
            Width           =   3615
            Begin VB.TextBox LinkAlc 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   570
               TabIndex        =   54
               ToolTipText     =   "Flight Carrier ICAO (3LC)"
               Top             =   30
               Width           =   735
            End
            Begin VB.TextBox LinkFltn 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1320
               TabIndex        =   53
               ToolTipText     =   "Flight Number"
               Top             =   30
               Width           =   555
            End
            Begin VB.TextBox LinkFlns 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1890
               TabIndex        =   52
               ToolTipText     =   "Flight Suffix"
               Top             =   30
               Width           =   315
            End
            Begin VB.Frame Frame4 
               BorderStyle     =   0  'None
               Height          =   345
               Left            =   2310
               TabIndex        =   49
               Top             =   30
               Width           =   1305
               Begin VB.CheckBox chkLinkAdid 
                  Caption         =   "Dep."
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   1
                  Left            =   660
                  Style           =   1  'Graphical
                  TabIndex        =   51
                  Top             =   0
                  Width           =   615
               End
               Begin VB.CheckBox chkLinkAdid 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Arr."
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   0
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   50
                  Top             =   0
                  Width           =   615
               End
            End
            Begin VB.Label Label7 
               Caption         =   "Flight:"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   0
               TabIndex        =   55
               Top             =   90
               Width           =   495
            End
         End
      End
      Begin VB.Frame LinkPeriodFrame 
         Caption         =   "Time Period "
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   120
         TabIndex        =   36
         ToolTipText     =   "Operational days of this airport."
         Top             =   390
         Width           =   3795
         Begin VB.Frame LinkPeriodCover 
            BorderStyle     =   0  'None
            Height          =   375
            Left            =   120
            TabIndex        =   43
            Top             =   270
            Visible         =   0   'False
            Width           =   3615
            Begin VB.TextBox LinkDateTo 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2370
               TabIndex        =   45
               ToolTipText     =   "Day Date dd.mm.yyyy"
               Top             =   0
               Width           =   1215
            End
            Begin VB.TextBox LinkDateFrom 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   900
               TabIndex        =   44
               ToolTipText     =   "Day Date dd.mm.yyyy"
               Top             =   0
               Width           =   1215
            End
            Begin VB.Label Label4 
               Caption         =   "Date from"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   0
               TabIndex        =   47
               Top             =   30
               Width           =   885
            End
            Begin VB.Label Label5 
               Caption         =   "to"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   2160
               TabIndex        =   46
               Top             =   30
               Width           =   255
            End
         End
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   2
      Top             =   4515
      Width           =   11085
      _ExtentX        =   19553
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16933
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton btnClose 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   10170
      TabIndex        =   1
      Top             =   4170
      Width           =   855
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   10170
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   210
      Width           =   855
   End
   Begin VB.Frame Frame1 
      Caption         =   "Assign by Relation (Condition)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4335
      Left            =   60
      TabIndex        =   8
      Top             =   120
      Width           =   4965
      Begin VB.CheckBox chkNext 
         Caption         =   "Get Next"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3990
         Style           =   1  'Graphical
         TabIndex        =   56
         Top             =   810
         Width           =   855
      End
      Begin VB.Frame AdidFrame 
         BorderStyle     =   0  'None
         Height          =   345
         Left            =   2520
         TabIndex        =   30
         Top             =   1440
         Width           =   1305
         Begin VB.CheckBox chkAdid 
            Caption         =   "Dep."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   660
            Style           =   1  'Graphical
            TabIndex        =   32
            Top             =   0
            Value           =   1  'Checked
            Width           =   615
         End
         Begin VB.CheckBox chkAdid 
            Alignment       =   1  'Right Justify
            Caption         =   "Arr."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   31
            Top             =   0
            Value           =   1  'Checked
            Width           =   615
         End
      End
      Begin VB.CheckBox chkLoad 
         Caption         =   "Read All"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3990
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   510
         Width           =   855
      End
      Begin VB.Frame TypeFrame 
         BorderStyle     =   0  'None
         Caption         =   "Frame2"
         Enabled         =   0   'False
         Height          =   585
         Left            =   3990
         TabIndex        =   26
         Top             =   1275
         Width           =   885
         Begin VB.OptionButton optType 
            BackColor       =   &H0080FF80&
            Caption         =   "Flight"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   28
            Top             =   0
            Value           =   -1  'True
            Width           =   855
         End
         Begin VB.OptionButton optType 
            Caption         =   "Aircraft"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   27
            Top             =   300
            Width           =   855
         End
      End
      Begin VB.CheckBox chkSave 
         Caption         =   "Save"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3990
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   3930
         Width           =   855
      End
      Begin VB.CheckBox chkDelete 
         Caption         =   "Delete"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3990
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   3630
         Width           =   855
      End
      Begin VB.CheckBox chkNew 
         Caption         =   "New"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3990
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   3030
         Width           =   855
      End
      Begin VB.CheckBox chkEdit 
         Caption         =   "Edit"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3990
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   3330
         Width           =   855
      End
      Begin VB.Frame PeriodFrame 
         Caption         =   "Time Period "
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   120
         TabIndex        =   9
         ToolTipText     =   "Operational days of this airport."
         Top             =   390
         Width           =   3795
         Begin VB.TextBox DateTo 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2460
            TabIndex        =   11
            ToolTipText     =   "Day Date dd.mm.yyyy"
            Top             =   270
            Width           =   1215
         End
         Begin VB.TextBox DateFrom 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   990
            TabIndex        =   10
            ToolTipText     =   "Day Date dd.mm.yyyy"
            Top             =   270
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Date from"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   90
            TabIndex        =   13
            Top             =   300
            Width           =   885
         End
         Begin VB.Label Label2 
            Caption         =   "to"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   2250
            TabIndex        =   12
            Top             =   300
            Width           =   255
         End
      End
      Begin VB.Frame InputFrame 
         Caption         =   "Assigned Flight"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   1170
         Width           =   3795
         Begin VB.TextBox Flca 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   660
            TabIndex        =   17
            ToolTipText     =   "Flight Carrier ICAO (3LC)"
            Top             =   270
            Width           =   735
         End
         Begin VB.TextBox Fltn 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1410
            TabIndex        =   16
            ToolTipText     =   "Flight Number"
            Top             =   270
            Width           =   555
         End
         Begin VB.TextBox Flns 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1980
            TabIndex        =   15
            ToolTipText     =   "Flight Suffix"
            Top             =   270
            Width           =   315
         End
         Begin VB.Label Label6 
            Caption         =   "Flight:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   90
            TabIndex        =   18
            Top             =   330
            Width           =   495
         End
      End
      Begin VB.Frame InputFrame 
         Caption         =   "Assigned Aircraft (Registration)"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Index           =   1
         Left            =   120
         TabIndex        =   19
         Top             =   2670
         Visible         =   0   'False
         Width           =   3795
         Begin VB.TextBox Regn 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   810
            TabIndex        =   20
            ToolTipText     =   "Registration number without dash please."
            Top             =   270
            Width           =   1485
         End
         Begin VB.Label Label3 
            Caption         =   "Aircraft:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   90
            TabIndex        =   21
            Top             =   330
            Width           =   765
         End
      End
      Begin VB.Frame ListFrame 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   2415
         Left            =   30
         TabIndex        =   33
         Top             =   1890
         Width           =   3945
         Begin TABLib.TAB ListOfFlights 
            Height          =   2265
            Left            =   90
            TabIndex        =   34
            Top             =   60
            Width           =   3795
            _Version        =   65536
            _ExtentX        =   6694
            _ExtentY        =   3995
            _StockProps     =   64
            HeaderString    =   "DLH,2460,F"
            HeaderLengthString=   "26,31,10,30,30,30,30,30"
            FontName        =   "Courier New"
         End
      End
   End
   Begin VB.TextBox CurUrno 
      Height          =   285
      Left            =   11760
      TabIndex        =   3
      Text            =   "CurUrno"
      Top             =   600
      Width           =   885
   End
   Begin VB.TextBox CurAction 
      Height          =   285
      Left            =   11490
      TabIndex        =   4
      Text            =   "CMD"
      Top             =   900
      Width           =   465
   End
   Begin VB.TextBox CurIdx 
      Height          =   285
      Left            =   12210
      TabIndex        =   5
      Text            =   "Idx"
      Top             =   900
      Width           =   225
   End
   Begin VB.TextBox CurRtyp 
      Height          =   285
      Left            =   12450
      TabIndex        =   6
      Text            =   "Typ"
      Top             =   900
      Width           =   255
   End
   Begin VB.TextBox CurAdid 
      Height          =   285
      Left            =   11970
      TabIndex        =   7
      Text            =   "Adid"
      Top             =   900
      Width           =   225
   End
End
Attribute VB_Name = "AssignFlight"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private MyParent As Form
Private MyCallButton As Control
Private CurTelexRecord As String
Private CurFlightRecord As String
Private LinkAftArrRecord As String
Private LinkAftDepRecord As String
Private LastInput As String
Private FormIsUp As Boolean
Private CurTlxUrno As String
Private CurAftUrno As String
Private AssignedFlightIsArr As Boolean
Private AssignedFlightIsDep As Boolean

Public Sub RegisterMyParent(Caller As Form, CallButton As Control, TaskValue As String)
    Dim tmpFields As String
    Dim tmpKey As String
    Dim tmpOrg3 As String
    Dim tmpDes3 As String
    Dim NewArrRec As String
    Dim NewDepRec As String
    Set MyParent = Caller
    Set MyCallButton = CallButton
    CurTelexRecord = TaskValue
    CurTlxUrno = GetFieldValue("URNO", CurTelexRecord, DataPool.TlxTabFields)
    CurAftUrno = GetFieldValue("FLNU", CurTelexRecord, DataPool.TlxTabFields)
    If CurAftUrno = "" Then CurAftUrno = "0"
    LinkAftArrRecord = ""
    LinkAftDepRecord = ""
    AssignedFlightIsArr = False
    AssignedFlightIsDep = False
    LinkFlights.ResetContent
    chkLinkNew.Value = 0
    If (CurTlxUrno <> "") And (CurAftUrno <> "0") Then
        tmpFields = "FLNO,STOD,STOA,ORG3,DES3,VIA3,FTYP,REMP,URNO"
        tmpKey = "WHERE URNO=" & CurAftUrno
        UfisServer.CallCeda UserAnswer, "GFR", "AFTTAB", tmpFields, "", tmpKey, "", 0, True, False
        LinkAftArrRecord = ""
        LinkAftArrRecord = LinkAftArrRecord & GetFieldValue("FLNO", UserAnswer, tmpFields) & ","
        LinkAftArrRecord = LinkAftArrRecord & GetFieldValue("STOA", UserAnswer, tmpFields) & ","
        LinkAftArrRecord = LinkAftArrRecord & GetFieldValue("ORG3", UserAnswer, tmpFields) & ","
        LinkAftArrRecord = LinkAftArrRecord & GetFieldValue("VIA3", UserAnswer, tmpFields) & ","
        LinkAftArrRecord = LinkAftArrRecord & GetFieldValue("FTYP", UserAnswer, tmpFields) & ","
        LinkAftArrRecord = LinkAftArrRecord & GetFieldValue("REMP", UserAnswer, tmpFields) & ","
        LinkAftArrRecord = LinkAftArrRecord & GetFieldValue("URNO", UserAnswer, tmpFields)
        
        LinkAftDepRecord = LinkAftDepRecord & GetFieldValue("FLNO", UserAnswer, tmpFields) & ","
        LinkAftDepRecord = LinkAftDepRecord & GetFieldValue("STOD", UserAnswer, tmpFields) & ","
        LinkAftDepRecord = LinkAftDepRecord & GetFieldValue("DES3", UserAnswer, tmpFields) & ","
        LinkAftDepRecord = LinkAftDepRecord & GetFieldValue("VIA3", UserAnswer, tmpFields) & ","
        LinkAftDepRecord = LinkAftDepRecord & GetFieldValue("FTYP", UserAnswer, tmpFields) & ","
        LinkAftDepRecord = LinkAftDepRecord & GetFieldValue("REMP", UserAnswer, tmpFields) & ","
        LinkAftDepRecord = LinkAftDepRecord & GetFieldValue("URNO", UserAnswer, tmpFields)
        
        tmpOrg3 = GetFieldValue("ORG3", UserAnswer, tmpFields)
        tmpDes3 = GetFieldValue("DES3", UserAnswer, tmpFields)
        If (tmpOrg3 = HomeAirport) And (tmpDes3 = HomeAirport) Then
            AssignedFlightIsArr = True
            AssignedFlightIsDep = True
            chkLinkAdid(0).Value = 1
        ElseIf tmpOrg3 = HomeAirport Then
            AssignedFlightIsDep = True
            AssignedFlightIsArr = False
            LinkAftArrRecord = LinkAftDepRecord
            chkLinkAdid(1).Value = 1
        ElseIf tmpDes3 = HomeAirport Then
            AssignedFlightIsArr = True
            AssignedFlightIsDep = False
            LinkAftDepRecord = LinkAftArrRecord
            chkLinkAdid(0).Value = 1
        Else
        End If
        If Not FormIsUp Then SetLinkedFlightToTop CurAftUrno
    End If
    If FormIsUp Then
        ListOfFlights.ResetContent
        InitMyForm 10, False
        CheckWorkArea
        chkLinkAdid(0).Value = 1
        SetLinkedFlightToTop CurAftUrno
        LinkFlights.RedrawTab
    End If
End Sub

Private Sub InitMyForm(ipLines As Integer, FirstCall As Boolean)
    If FirstCall Then
        LinkFlights.ResetContent
        LinkFlights.FontName = "Courier New"
        LinkFlights.HeaderFontSize = 15
        LinkFlights.FontSize = 15
        LinkFlights.lineHeight = 15
        LinkFlights.Height = (ipLines * LinkFlights.lineHeight * Screen.TwipsPerPixelY) + 1 * Screen.TwipsPerPixelY
        LinkFlights.HeaderLengthString = "68,80,28,28,16,60,90"
        LinkFlights.HeaderString = "Flight No,  DAY     TIME ,APC,VIA,C,Remark,URNO"
        LinkFlights.SetMainHeaderValues "5,2", "List of Available Flights, System", "12632256,12632256"
        LinkFlights.MainHeader = True
        LinkFlights.DateTimeSetColumn 1
        LinkFlights.DateTimeSetInputFormatString 1, "YYYYMMDDhhmmss"
        LinkFlights.DateTimeSetOutputFormatString 1, "DDMMMYY'  'hh':'mm"
        LinkFlights.AutoSizeByHeader = True
        LinkFlights.AutoSizeColumns
        
        
        ListOfFlights.ResetContent
        ListOfFlights.FontName = "Courier New"
        ListOfFlights.HeaderFontSize = 15
        ListOfFlights.FontSize = 15
        ListOfFlights.lineHeight = 15
        ListOfFlights.Height = (ipLines * ListOfFlights.lineHeight * Screen.TwipsPerPixelY) + 1 * Screen.TwipsPerPixelY
        ListOfFlights.HeaderLengthString = "28,34,13,13,74,74,10,80"
        ListOfFlights.HeaderString = "Flc,Fltn,S,I,Date from,Date to,T,URNO"
        ListOfFlights.SetMainHeaderValues "6,2", "List of Assigned Flights, System", "12632256,12632256"
        ListOfFlights.MainHeader = True
        InputFrame(1).Top = InputFrame(0).Top
        InputFrame(1).Left = InputFrame(0).Left
        DateFrom.ToolTipText = "Day Date " & MySetUp.DefDateFormat.Text
        DateTo.ToolTipText = "Day Date " & MySetUp.DefDateFormat.Text
    End If
    LinkAlc.Text = GetFieldValue("ALC3", CurTelexRecord, DataPool.TlxTabFields)
    LinkFltn.Text = GetFieldValue("FLTN", CurTelexRecord, DataPool.TlxTabFields)
    LinkFlns.Text = GetFieldValue("FLNS", CurTelexRecord, DataPool.TlxTabFields)
    LinkDateFrom.Text = Format(CedaDateToVb(GetFieldValue("TIME", CurTelexRecord, DataPool.TlxTabFields)), MySetUp.DefDateFormat)
    LinkDateTo.Text = LinkDateFrom.Text
    ToggleFramesEnable False
    ResetInputFields
    LoadListOfAssign
End Sub

Private Sub LoadListOfAssign()
Dim TfrTabFields As String
Dim SqlKey As String
Dim Line As String
Dim tmpVal As String
Dim tmpRtyp As String
Dim AftRec As String
Dim TfrRec As String
Dim RetVal As Integer
Dim count As Long
Dim i As Long
    ListOfFlights.ResetContent
    If CurTlxUrno <> "" Then
        TfrTabFields = "URNO,RTYP,TURN,ALC3,FLTN,FLNS,ADID,REGN,VAFR,VATO"
        Line = ""
        Line = GetFieldValue("ALC3", CurTelexRecord, DataPool.TlxTabFields) & ","
        If Len(Line) > 1 Then
            Line = Line & GetFieldValue("FLTN", CurTelexRecord, DataPool.TlxTabFields) & ","
            Line = Line & GetFieldValue("FLNS", CurTelexRecord, DataPool.TlxTabFields) & ","
            Line = Line & "B,"
            Line = Line & Format(CedaDateToVb(GetFieldValue("TIME", CurTelexRecord, DataPool.TlxTabFields)), MySetUp.DefDateFormat) & ","
            Line = Line & Format(CedaDateToVb(GetFieldValue("TIME", CurTelexRecord, DataPool.TlxTabFields)), MySetUp.DefDateFormat) & ","
            Line = Line & "S,0"
            ListOfFlights.InsertTextLine Line, True
            ListOfFlights.SetLineColor 0, vbGreen, vbBlack
        End If
        SqlKey = "WHERE TURN=" & CurTlxUrno
        RetVal = UfisServer.CallCeda(tmpVal, "RTA", "TFRTAB", TfrTabFields, "", SqlKey, "", 0, False, False)
        count = UfisServer.DataBuffer(0).GetLineCount - 1
        If count >= 0 Then
            For i = 0 To count
                TfrRec = UfisServer.DataBuffer(0).GetLineValues(i)
                Line = ""
                tmpRtyp = GetFieldValue("RTYP", TfrRec, TfrTabFields)
                If tmpRtyp = "F" Then
                    Line = Line & GetFieldValue("ALC3", TfrRec, TfrTabFields) & ","
                    Line = Line & GetFieldValue("FLTN", TfrRec, TfrTabFields) & ","
                    Line = Line & GetFieldValue("FLNS", TfrRec, TfrTabFields) & ","
                ElseIf tmpRtyp = "R" Then
                    Line = Line & "," & GetFieldValue("REGN", TfrRec, TfrTabFields) & ",,"
                Else
                End If
                
                Line = Line & GetFieldValue("ADID", TfrRec, TfrTabFields) & ","
                Line = Line & Format(CedaDateToVb(GetFieldValue("VAFR", TfrRec, TfrTabFields)), MySetUp.DefDateFormat) & ","
                Line = Line & Format(CedaDateToVb(GetFieldValue("VATO", TfrRec, TfrTabFields)), MySetUp.DefDateFormat) & ","
                Line = Line & tmpRtyp & ","
                Line = Line & GetFieldValue("URNO", TfrRec, TfrTabFields) & ","
                ListOfFlights.InsertTextLine Line, False
            Next
        End If
        UfisServer.DataBuffer(0).ResetContent
    End If
    ListOfFlights.RedrawTab
End Sub
Private Sub CheckWorkArea()
    If ListOfFlights.GetLineCount = 0 Then
        chkNew.Enabled = True
        chkNew.Value = 1
    Else
        chkNew.Value = 0
        If CurTlxUrno = "" Then chkNew.Enabled = False Else chkNew.Enabled = True
        DisplayRecord ListOfFlights.GetCurrentSelected
    End If
End Sub

Private Sub CheckSaveButton()
    If LastInput <> CheckInput Then
        chkSave.Enabled = True
        TypeFrame.Enabled = False
    Else
        chkSave.Enabled = False
        If chkNew.Value = 1 Then TypeFrame.Enabled = True
    End If
End Sub

Private Function CheckInput() As String
Dim Result As String
    Result = DateFrom & vbNewLine & DateTo & vbNewLine & _
            Flca & vbNewLine & Fltn & vbNewLine & Flns & vbNewLine & Regn & _
            chkAdid(0) & vbNewLine & chkAdid(1)
    CheckInput = Result
End Function

Private Sub ResetInputFields()
    Dim tmpDate As String
    Dim tmpVbDate
    tmpDate = Left(GetFieldValue("TIME", CurTelexRecord, DataPool.TlxTabFields), 8)
    If tmpDate = "" Then
        tmpDate = Left(GetFieldValue("CDAT", CurTelexRecord, DataPool.TlxTabFields), 8)
    End If
    If tmpDate <> "" Then
        tmpVbDate = CedaDateToVb(tmpDate)
        tmpDate = Format(tmpVbDate, MySetUp.DefDateFormat)
    End If
    DateFrom.Text = tmpDate
    DateTo.Text = tmpDate
    'DateFrom = Format(Date, MySetUp.DefDateFormat)
    'DateFrom.Tag = Format(Date, "yyyymmdd")
    'DateTo = Format(Date, MySetUp.DefDateFormat)
    'DateTo.Tag = Format(Date, "yyyymmdd")
    Flca = GetFieldValue("ALC3", CurTelexRecord, DataPool.TlxTabFields)
    Fltn = GetFieldValue("FLTN", CurTelexRecord, DataPool.TlxTabFields)
    Flns = GetFieldValue("FLNS", CurTelexRecord, DataPool.TlxTabFields)
    Regn = ""
    CurUrno = ""
    CurRtyp = ""
    CurIdx = ""
    CurAction = ""
    chkAdid(0).Tag = "X"
    chkAdid(0).Value = 0
    chkAdid(1).Value = 0
    chkAdid(0).Tag = ""
    LastInput = CheckInput
    CheckSaveButton
End Sub

Private Sub ToggleFramesEnable(SetVal As Boolean)
    PeriodFrame.Enabled = SetVal
    InputFrame(0).Enabled = SetVal
    InputFrame(1).Enabled = SetVal
    AdidFrame.Enabled = SetVal
    optType(0).Enabled = SetVal
    optType(1).Enabled = SetVal
    chkAdid(0).Enabled = SetVal
    chkAdid(1).Enabled = SetVal
    ListFrame.Enabled = Not SetVal
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub chkDelete_Click()
    If chkDelete.Value = 1 Then
        CurAction = "DRT"
        If MyMsgBox.CallAskUser(0, 0, 0, "Delete", "Do you want to delete this entry ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
            chkSave.Enabled = True
            chkSave.Value = 1
        Else
            chkDelete.Value = 0
        End If
    Else
    End If
End Sub

Private Sub chkEdit_Click()
Dim SaveIt As Boolean
    On Error Resume Next
    If chkSave.Value = 0 Then
        If chkEdit.Value = 1 Then
            CurAction = "URT"
            ToggleFramesEnable True
            chkNew.Enabled = False
            chkDelete.Enabled = False
            If optType(0).Value = True Then Flca.SetFocus Else Regn.SetFocus
            optType(0).Enabled = False
            optType(1).Enabled = False
        Else
            SaveIt = False
            If chkSave.Enabled Then
                If MyMsgBox.CallAskUser(0, 0, 0, "Update", "Do you want to save your changes?", "ask", "Yes,No;F", UserAnswer) = 1 Then
                    SaveIt = True
                End If
            End If
            If SaveIt Then
                chkSave.Value = 1
            Else
                ToggleFramesEnable False
                CheckWorkArea
            End If
        End If
    End If
End Sub

Private Sub chkLinkAdid_Click(Index As Integer)
    If chkLinkAdid(Index).Value = 0 Then chkLinkAdid(Index).BackColor = vbButtonFace
    Select Case Index
        Case 0
            If chkLinkAdid(Index).Value = 1 Then
                If AssignedFlightIsArr Then
                    LinkFlights.HeaderString = "Flight No,  DAY     STA ,ORG,VIA,C,Remark,URNO"
                Else
                    LinkFlights.HeaderString = "Flight No,  DAY    TIME ,APC,VIA,C,Remark,URNO"
                End If
                chkLinkAdid(1).Value = 0
            Else
                chkLinkAdid(1).Value = 1
            End If
        Case 1
            If chkLinkAdid(Index).Value = 1 Then
                If AssignedFlightIsDep Then
                    LinkFlights.HeaderString = "Flight No,  DAY     STD ,DES,VIA,C,Remark,URNO"
                Else
                    LinkFlights.HeaderString = "Flight No,  DAY    TIME ,APC,VIA,C,Remark,URNO"
                End If
                chkLinkAdid(0).Value = 0
            Else
                chkLinkAdid(0).Value = 1
            End If
    End Select
    If chkLinkAdid(Index).Value = 1 Then
        chkLinkAdid(Index).BackColor = LightestGreen
        If chkLinkNew.Value = 1 Then chkLinkLoad.Value = 1
    End If
    LinkFlights.RedrawTab
End Sub

Private Sub chkLinkAssign_Click()
    If chkLinkAssign.Value = 1 Then
        chkLinkAssign.BackColor = LightestGreen
        SendFdiuToFdihdl
        chkLinkAssign.Value = 0
    Else
        chkLinkAssign.BackColor = vbButtonFace
    End If
End Sub
Private Sub SendFdiuToFdihdl()
    Dim CurLine As Long
    Dim TlxUrno As String
    Dim AftUrno As String
    Dim FdiuData As String
    Dim MsgTxt As String
    Dim RetVal As Integer
    CurLine = LinkFlights.GetCurrentSelected
    If CurLine >= 0 Then
        RetVal = 1
        If CurAftUrno <> "0" Then
            MsgTxt = "This telex is already assigned." & vbNewLine
            MsgTxt = MsgTxt & "Do you want to proceed ?"
            RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Assign Flight", MsgTxt, "ask", "Yes,No;F", UserAnswer)
        End If
        If RetVal = 1 Then
            TlxUrno = CurTlxUrno
            AftUrno = LinkFlights.GetColumnValue(CurLine, 6)
            FdiuData = AftUrno & "," & TlxUrno
            Screen.MousePointer = 11
            UfisServer.CallCeda UserAnswer, "FDIU", "LOATAB", "FLNU,TXNU", "", FdiuData, "", 0, False, False
            Screen.MousePointer = 0
        End If
    Else
    End If
End Sub

Private Sub chkLinkLoad_Click()
    If chkLinkLoad.Value = 1 Then
        chkLinkLoad.BackColor = LightestGreen
        LoadLinkFlights
        chkLinkLoad.Value = 0
    Else
        chkLinkLoad.BackColor = vbButtonFace
    End If
End Sub

Private Sub LoadLinkFlights()
    Dim SqlFields As String
    Dim SqlKey As String
    Dim RetVal As Integer
    Screen.MousePointer = 11
    LinkFlights.ResetContent
    LinkFlights.RedrawTab
    Me.Refresh
    LinkFlights.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    LinkFlights.CedaHopo = UfisServer.HOPO
    LinkFlights.CedaIdentifier = ""
    LinkFlights.CedaPort = "3357"
    LinkFlights.CedaReceiveTimeout = "250"
    LinkFlights.CedaRecordSeparator = vbLf
    LinkFlights.CedaSendTimeout = "250"
    LinkFlights.CedaServerName = UfisServer.HostName
    LinkFlights.CedaTabext = UfisServer.TblExt
    LinkFlights.CedaUser = gsUserName
    LinkFlights.CedaWorkstation = UfisServer.GetMyWorkStationName
    
    RetVal = 1
    
    If chkLinkAdid(0).Value = 1 Then
        SqlFields = "FLNO,STOA,ORG3,VIA3,FTYP,REMP,URNO"
        SqlKey = "WHERE TIFA BETWEEN '" & LinkDateFrom.Tag & "000000' AND '" & LinkDateTo.Tag & "235959'"
        SqlKey = SqlKey & " AND DES3='" & HomeAirport & "'"
    ElseIf chkLinkAdid(1).Value = 1 Then
        SqlFields = "FLNO,STOD,DES3,VIA3,FTYP,REMP,URNO"
        SqlKey = "WHERE TIFD BETWEEN '" & LinkDateFrom.Tag & "000000' AND '" & LinkDateTo.Tag & "235959'"
        SqlKey = SqlKey & " AND ORG3='" & HomeAirport & "'"
    Else
        RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Search Flights", "Please select 'Arr.' or 'Dep.' first.", "hand", "", UserAnswer)
        RetVal = -1
    End If
    
    If RetVal = 1 Then
        If LinkFltn.Text <> "" Then SqlKey = SqlKey & " AND FLTN='" & LinkFltn.Text & "'"
        If Len(LinkAlc.Text) = 2 Then
            SqlKey = SqlKey & " AND ALC2='" & LinkAlc.Text & "'"
        ElseIf Len(LinkAlc.Text) = 3 Then
            SqlKey = SqlKey & " AND ALC3='" & LinkAlc.Text & "'"
        End If
        If LinkFlns.Text <> "" Then SqlKey = SqlKey & " AND FLNS='" & LinkFlns.Text & "'"
        SqlKey = SqlKey & " AND FTYP NOT IN ('T','G')"
        
        LinkFlights.CedaAction "GFR", "AFTTAB", SqlFields, "", SqlKey
        LinkFlights.Sort 1, True, True
        LinkFlights.Sort 0, True, True
        LinkFlights.AutoSizeColumns
        SetLinkedFlightToTop CurAftUrno
        LinkFlights.RedrawTab
    End If
    chkLinkAssign.Enabled = False
    Screen.MousePointer = 0
End Sub
Private Sub SetLinkedFlightToTop(AftUrno As String)
    Dim LineList As String
    Dim CurLine As Long
    Dim CurValues As String
    If AftUrno <> "0" Then
        LineList = LinkFlights.GetLinesByColumnValue(6, AftUrno, 0)
        If LineList <> "" Then
            CurLine = Val(LineList)
            CurValues = LinkFlights.GetLineValues(CurLine)
            LinkFlights.DeleteLine CurLine
            LinkFlights.InsertTextLineAt 0, CurValues, False
            LinkFlights.SetLineColor 0, vbYellow, vbBlack
        Else
            If chkLinkAdid(0).Value = 1 Then
                LinkFlights.InsertTextLineAt 0, LinkAftArrRecord, False
                LinkFlights.SetLineColor 0, vbYellow, vbBlack
            ElseIf chkLinkAdid(1).Value = 1 Then
                LinkFlights.InsertTextLineAt 0, LinkAftDepRecord, False
                LinkFlights.SetLineColor 0, vbYellow, vbBlack
            Else
            End If
        End If
    End If
End Sub
Private Sub chkLinkNew_Click()
    If chkLinkNew.Value = 1 Then
        LinkPeriodFrame.Enabled = True
        LinkPeriodCover.Visible = True
        LinkFlightFrame.Enabled = True
        LinkFlightCover.Visible = True
        LinkListFrame.Caption = ""
        LinkListFrame.Enabled = True
        LinkFlights.Visible = True
        chkLinkLoad.Enabled = True
        If LinkFlights.GetLineCount > 1 Then chkLinkAssign.Enabled = True
        If Trim(LinkDateFrom.Text) = "" Then LinkDateFrom.Text = DateFrom.Text
        If Trim(LinkDateTo.Text) = "" Then LinkDateTo.Text = DateTo.Text
        chkLinkNew.BackColor = LightestGreen
    Else
        LinkPeriodFrame.Enabled = False
        LinkPeriodCover.Visible = False
        LinkFlightFrame.Enabled = False
        LinkFlightCover.Visible = False
        'LinkListFrame.Caption = "Available Flights"
        LinkListFrame.Enabled = False
        'LinkFlights.Visible = False
        chkLinkLoad.Enabled = False
        chkLinkAssign.Enabled = False
        chkLinkNew.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkLoad_Click()
    Dim clSqlKey As String
    If chkLoad.Value = 1 Then
        clSqlKey = "WHERE URNO IN (SELECT DISTINCT(TURN) FROM TFRTAB)"
        TelexPoolHead.LoadTelexData 1, "", clSqlKey, "", "ALL", "RCV"
        chkNext.Enabled = True
        chkLoad.Value = 0
    End If
End Sub

Private Sub chkNew_Click()
Dim SaveIt As Boolean
    On Error Resume Next
    If chkSave.Value = 0 Then
        If chkNew.Value = 1 Then
            ResetInputFields
            CurAction = "IRT"
            If optType(0).Value = True Then CurRtyp = "F" Else CurRtyp = "R"
            ToggleFramesEnable True
            chkEdit.Enabled = False
            chkDelete.Enabled = False
            If optType(0).Value = True Then Flca.SetFocus Else Regn.SetFocus
        Else
            SaveIt = False
            If chkSave.Enabled Then
                If MyMsgBox.CallAskUser(0, 0, 0, "Insert", "Do you want to save your input?", "ask", "Yes,No;F", UserAnswer) = 1 Then
                    SaveIt = True
                End If
            End If
            If SaveIt Then
                chkSave.Value = 1
            Else
                ToggleFramesEnable False
                CheckWorkArea
            End If
        End If
    End If
End Sub

Private Sub chkAdid_Click(Index As Integer)
    If chkAdid(0).Tag = "" Then
        If chkAdid(Index).Value = 0 Then
            chkAdid(1 - Index).Value = 1 - chkAdid(Index).Value
        End If
    End If
    If chkAdid(0).Value = chkAdid(1).Value Then
        If chkAdid(0).Value = 0 Then CurAdid = "?" Else CurAdid = "B"
    Else
        If chkAdid(0).Value = 1 Then CurAdid = "A" Else CurAdid = "D"
    End If
    CheckSaveButton
End Sub

Private Sub chkNext_Click()
    Dim LineNo As Long
    If chkNext.Value = 1 Then
        LineNo = TelexPoolHead.tabTelexList(0).GetCurrentSelected + 1
        If LineNo >= TelexPoolHead.tabTelexList(0).GetLineCount Then LineNo = 0
        TelexPoolHead.tabTelexList(0).SetCurrentSelection LineNo
        'TelexPoolHead.chkAssign.Value = 1
        chkNext.Value = 0
    End If
End Sub

Private Sub chkSave_Click()
    If chkSave.Value = 1 Then
        chkSave.BackColor = vbGreen
        chkEdit.Value = 0
        chkNew.Value = 0
        chkDelete.Value = 0
        SaveTfrRecord
        chkSave.Value = 0
    Else
        chkSave.BackColor = vbButtonFace
    End If
End Sub
Private Sub SaveTfrRecord()
    Dim tmpTable As String
    Dim SendToCeda As Boolean
    Dim tmpAdid As String
    Dim tmpFldLst As String
    Dim tmpSqlKey As String
    Dim tmpDatLst As String
    Dim ActCmd As String
    Dim tmpVal As String
    Dim RetVal As Integer
    Dim tmpFlca As String
    Dim tmpAlc3 As String
    SendToCeda = True
    ActCmd = CurAction
    tmpTable = "TFRTAB"
    If ActCmd <> "DRT" Then
        tmpFlca = Trim(Flca.Text)
        tmpAlc3 = Trim(Flca.Tag)
        If tmpAlc3 <> "" Then
            If Len(tmpAlc3) > 3 Then Flca.Tag = TelexPoolHead.AskForUniqueAlc(tmpAlc3, Flca.Text)
            tmpAlc3 = Trim(Flca.Tag)
        End If
        If (tmpFlca <> "") And (tmpAlc3 = "") Then
            If MyMsgBox.CallAskUser(0, 0, 0, "Basic Data Control", "The Airline Code doesn't exist.", "stop2", "", UserAnswer) = 1 Then DoNothing
            SendToCeda = False
        End If
        If tmpSqlKey = "IBT" Then tmpSqlKey = "           "
        tmpFldLst = "TURN,RTYP,VAFR,VATO,ADID"
        tmpDatLst = CurTlxUrno & "," & CurRtyp & "," & DateFrom.Tag & "000000," & DateTo.Tag & "235959," & CurAdid
        Select Case CurRtyp
            Case "S"                'System - Entry from TLXTAB Record
                SendToCeda = False  'Can't be activated
            Case "F"                'Flight - TFRTAB.RTYP
                tmpFldLst = tmpFldLst & ",ALC3,FLTN,FLNS"
                tmpDatLst = tmpDatLst & "," & Flca.Tag & "," & Fltn & "," & Flns
            Case "R"                'Registration - TFRTAB.RTYP
                tmpFldLst = tmpFldLst & ",REGN"
                tmpDatLst = tmpDatLst & "," & Regn
            Case Else
                SendToCeda = False
        End Select
        If ActCmd = "URT" Then
            tmpSqlKey = "WHERE URNO=" & CurUrno
        End If
        If ActCmd = "IRT" Then
            tmpFldLst = tmpFldLst & ",URNO"
            tmpDatLst = tmpDatLst & "," & UfisServer.UrnoPoolGetNext
            tmpSqlKey = "           "
        End If
    Else
        tmpSqlKey = "WHERE URNO=" & CurUrno
    End If
    If SendToCeda Then
        RetVal = UfisServer.CallCeda(tmpVal, ActCmd, tmpTable, tmpFldLst, tmpDatLst, tmpSqlKey, "", 0, True, False)
    End If
End Sub
Private Sub DateFrom_Change()
    If CheckDateField(DateFrom, MySetUp.DefDateFormat, False) <> True Then DoNothing
    CheckSaveButton
End Sub

Private Sub DateTo_Change()
    If CheckDateField(DateTo, MySetUp.DefDateFormat, False) <> True Then DoNothing
    CheckSaveButton
End Sub

Private Sub Flca_Change()
    Flca.Tag = TelexPoolHead.CheckAlcLookUp(Flca.Text, False)
    CheckSaveButton
End Sub

Private Sub Flca_GotFocus()
    SetTextSelected
End Sub

Private Sub Flca_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub Flns_Change()
    CheckSaveButton
End Sub

Private Sub Flns_GotFocus()
    SetTextSelected
End Sub

Private Sub Flns_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub Fltn_Change()
    CheckSaveButton
End Sub

Private Sub Fltn_GotFocus()
    SetTextSelected
End Sub

Private Sub Fltn_KeyPress(KeyAscii As Integer)
    'If KeyAscii < 48 Or KeyAscii > 57 Then KeyAscii = 0
End Sub

Private Sub Form_Activate()
    On Error Resume Next
    With MyParent
        Me.OnTop.Value = .OnTop.Value
    End With
    If Not FormIsUp Then
        btnClose.SetFocus
        FormIsUp = True
    End If
End Sub

Private Sub Form_Load()
    FormIsUp = False
    With MyParent
        Left = .Left + 50 * Screen.TwipsPerPixelX
        Top = .Top + 50 * Screen.TwipsPerPixelY
    End With
    InitMyForm 10, True
    Show
    Refresh
    CheckWorkArea
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'If IsObject(MyCallButton) Then MyCallButton.Value = 0
End Sub

Private Sub LinkAlc_GotFocus()
    SetTextSelected
End Sub

Private Sub LinkAlc_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub LinkAlc_LostFocus()
    LinkAlc.Text = Trim(LinkAlc.Text)
End Sub

Private Sub LinkDateFrom_Change()
    If CheckDateField(LinkDateFrom, MySetUp.DefDateFormat, False) <> True Then DoNothing
End Sub

Private Sub LinkDateTo_Change()
    If CheckDateField(LinkDateTo, MySetUp.DefDateFormat, False) <> True Then DoNothing
End Sub

Private Sub LinkFlights_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    '2013-08-29
    'MZA - Zaw Min Tun
    'UFIS-4049
    'If LineNo >= 1 Then
    'Actually the first row is row 0 and Assign button should be enabled any row is selected
    If LineNo >= 0 Then
        chkLinkAssign.Enabled = True
    Else
        chkLinkAssign.Enabled = False
    End If
End Sub

Private Sub LinkFlights_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        LinkFlights.Sort ColNo, True, True
        SetLinkedFlightToTop CurAftUrno
        LinkFlights.RedrawTab
    End If
End Sub

Private Sub LinkFlns_GotFocus()
    SetTextSelected
End Sub

Private Sub LinkFlns_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub LinkFlns_LostFocus()
    LinkFlns.Text = Trim(LinkFlns.Text)
End Sub

Private Sub LinkFltn_GotFocus()
    SetTextSelected
End Sub

Private Sub LinkFltn_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub LinkFltn_LostFocus()
    LinkFltn.Text = Trim(LinkFltn.Text)
End Sub

Private Sub DisplayRecord(Line As Long)
    Dim tmpData As String
    Dim tmpVal As String
    If (Line >= 0) And (Line < ListOfFlights.GetLineCount) Then
        tmpData = ListOfFlights.GetLineValues(Line)
        DateFrom.Text = GetItem(tmpData, 5, ",")
        DateTo.Text = GetItem(tmpData, 6, ",")
        tmpVal = GetItem(tmpData, 4, ",")
        chkAdid(0).Value = 1
        chkAdid(1).Value = 1
        Select Case tmpVal
            Case "A"
                chkAdid(1).Value = 0
            Case "D"
                chkAdid(0).Value = 0
        End Select
        tmpVal = GetItem(tmpData, 7, ",")
        CurRtyp = tmpVal
        Select Case tmpVal
            Case "F"   'Flight
                Flca = GetItem(tmpData, 1, ",")
                Fltn = GetItem(tmpData, 2, ",")
                Flns = GetItem(tmpData, 3, ",")
                chkEdit.Enabled = True
                chkDelete.Enabled = True
                optType(0).Value = True
            Case "S"    'Assigned Flight
                Flca = GetItem(tmpData, 1, ",")
                Fltn = GetItem(tmpData, 2, ",")
                Flns = GetItem(tmpData, 3, ",")
                chkEdit.Enabled = False
                chkDelete.Enabled = False
                optType(0).Value = True
            Case "R"    'Registration
                Regn = GetItem(tmpData, 2, ",")
                optType(1).Value = True
                chkEdit.Enabled = True
                chkDelete.Enabled = True
        End Select
        CurUrno = GetItem(tmpData, 8, ",")
        CurIdx.Text = str(Line)
        CurAction = ""
        chkNew.Value = 0
    Else
        ResetInputFields
        chkEdit.Enabled = False
        chkDelete.Enabled = False
    End If
    LastInput = CheckInput
    CheckSaveButton
End Sub

Private Sub ListOfFlights_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    On Error Resume Next
    If Selected Then
        DisplayRecord LineNo
        ListOfFlights.SetFocus
    End If
End Sub

Private Sub OnTop_Click()
    On Error Resume Next
    If OnTop.Value = 1 Then OnTop.BackColor = LightGreen Else OnTop.BackColor = vbButtonFace
    MyParent.OnTop.Value = Me.OnTop.Value
    Me.SetFocus
End Sub

Private Sub optType_Click(Index As Integer)
    On Error Resume Next
    If optType(Index).Value = True Then
        optType(1 - Index).BackColor = vbButtonFace
        optType(Index).BackColor = LightGreen
        InputFrame(Index).Visible = True
        InputFrame(1 - Index).Visible = False
        If InputFrame(Index).Enabled = True Then
            If Index = 0 Then
                CurRtyp = "F"
                Flca.SetFocus
            Else
                CurRtyp = "R"
                Regn.SetFocus
            End If
        End If
    End If
End Sub

Private Sub Regn_Change()
    CheckSaveButton
End Sub

Private Sub Regn_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Public Sub HandleBc()

End Sub
