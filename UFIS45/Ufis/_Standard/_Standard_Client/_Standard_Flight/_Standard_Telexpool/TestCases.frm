VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form TestCases 
   Caption         =   "Telex Pool Test Case Tool"
   ClientHeight    =   5370
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10620
   Icon            =   "TestCases.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5370
   ScaleWidth      =   10620
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   345
      Left            =   5280
      TabIndex        =   10
      Top             =   60
      Width           =   795
   End
   Begin VB.CheckBox chkTranslate 
      Caption         =   "Translate"
      Height          =   285
      Left            =   6060
      TabIndex        =   9
      Top             =   30
      Value           =   1  'Checked
      Width           =   1785
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   9570
      Top             =   60
   End
   Begin VB.CheckBox chkFlights 
      Caption         =   "Flights"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkPrepare 
      Caption         =   "Prepare"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkLoad 
      Caption         =   "Load"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1770
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkCopy 
      Caption         =   "Copy"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkSave 
      Caption         =   "Save"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3510
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkClose 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4380
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB TAB1 
      Height          =   3795
      Left            =   6230
      TabIndex        =   1
      Top             =   360
      Width           =   3765
      _Version        =   65536
      _ExtentX        =   6641
      _ExtentY        =   6694
      _StockProps     =   64
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   5055
      Width           =   10620
      _ExtentX        =   18733
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15637
         EndProperty
      EndProperty
   End
   Begin TABLib.TAB TAB2 
      Height          =   3795
      Left            =   0
      TabIndex        =   8
      Top             =   360
      Width           =   6250
      _Version        =   65536
      _ExtentX        =   11024
      _ExtentY        =   6694
      _StockProps     =   64
   End
End
Attribute VB_Name = "TestCases"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim MeIsVisible As Boolean

Private Sub chkClose_Click()
    Unload Me
End Sub

Private Sub chkCopy_Click()
    Dim tmpText As String
    If chkCopy.Value = 1 Then
        tmpText = EditTelex.txtTelexText.Text
        tmpText = CleanString(tmpText, FOR_SERVER, False)
        tmpText = ",,," & tmpText & ","
        TAB1.InsertTextLine tmpText, False
        TAB1.Refresh
        chkCopy.Value = 0
    End If
End Sub

Private Sub chkFlights_Click()
    If chkFlights.Value = 1 Then
        chkFlights.Refresh
        LoadFlightData
        chkFlights.Value = 0
    End If
End Sub

Private Sub LoadFlightData()
    Dim tmpFrom As String
    Dim tmpTo As String
    Dim tmpTime As String
    Dim tmpSqlKey As String
    Dim tmpFields As String
    Dim CurSrvTime
    TAB2.ResetContent
    TAB2.Refresh
    Me.MousePointer = 11
    tmpFields = "ADID,FLNO,ORG3,STOD,VIA3,DES3,STOA,FLDA,REGN,GTA1,PSTA,OFBL,AIRB,ETAI,LAND,ONBL,ETDI,GTD1,PSTD,ETOA,ETOD,DCD1,DCD2,DTD1,DTD2,PAX1,PAX2,PAX3"
'    tmpTime = MySetUp.GetUtcServerTime
'    CurSrvTime = CedaFullDateToVb(tmpTime)
'    CurSrvTime = DateAdd("h", -1, CurSrvTime)
'    tmpFrom = Format(CurSrvTime, "yyyymmddhhmm") & "00"
'    CurSrvTime = DateAdd("h", 14, CurSrvTime)
'    tmpTo = Format(CurSrvTime, "yyyymmddhhmm") & "00"
    TelexPoolHead.GetValidTimeFrame tmpFrom, tmpTo, True
    tmpSqlKey = "WHERE (TIFA BETWEEN '" & tmpFrom & "' AND '" & tmpTo & "') "
    tmpSqlKey = tmpSqlKey & " AND ADID='A' "
    tmpSqlKey = tmpSqlKey & " AND FTYP IN ('O','S') "
    TAB2.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    TAB2.CedaHopo = UfisServer.HOPO
    TAB2.CedaIdentifier = "IDX"
    TAB2.CedaPort = "3357"
    TAB2.CedaReceiveTimeout = "250"
    TAB2.CedaRecordSeparator = vbLf
    TAB2.CedaSendTimeout = "250"
    TAB2.CedaServerName = UfisServer.HostName
    TAB2.CedaTabext = UfisServer.TblExt
    TAB2.CedaUser = gsUserName
    TAB2.CedaWorkstation = UfisServer.GetMyWorkStationName
    If CedaIsConnected Then
        TAB2.CedaAction "GFR", "AFTTAB", tmpFields, "", tmpSqlKey
    End If
    tmpSqlKey = "WHERE (TIFD BETWEEN '" & tmpFrom & "' AND '" & tmpTo & "') "
    tmpSqlKey = tmpSqlKey & " AND ADID='D' "
    tmpSqlKey = tmpSqlKey & " AND FTYP IN ('O','S') "
    TAB2.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    TAB2.CedaHopo = UfisServer.HOPO
    TAB2.CedaIdentifier = "IDX"
    TAB2.CedaPort = "3357"
    TAB2.CedaReceiveTimeout = "250"
    TAB2.CedaRecordSeparator = vbLf
    TAB2.CedaSendTimeout = "250"
    TAB2.CedaServerName = UfisServer.HostName
    TAB2.CedaTabext = UfisServer.TblExt
    TAB2.CedaUser = gsUserName
    TAB2.CedaWorkstation = UfisServer.GetMyWorkStationName
    If CedaIsConnected Then
        TAB2.CedaAction "GFR", "AFTTAB", tmpFields, "", tmpSqlKey
    End If
    TAB2.Sort "1", True, True
    TAB2.AutoSizeColumns
    If TestCaseMode Then
        TAB2.SetCurrentSelection 0
    Else
        TAB2.SetCurrentSelection -1
    End If
    Me.MousePointer = 0
End Sub
Private Sub chkLoad_Click()
    If chkLoad.Value = 1 Then
        TAB1.ResetContent
        'TAB1.ReadFromFile "c:\ufis\system\TelexTestCases.txt"
        TAB1.ReadFromFile UFIS_SYSTEM & "\TelexTestCases.txt"
        TAB1.AutoSizeColumns
        If TestCaseMode Then
            TAB1.SetCurrentSelection 0
        Else
            TAB1.SetCurrentSelection -1
        End If
        TAB1.Refresh
        chkLoad.Value = 0
    End If
End Sub

Private Sub chkPrepare_Click()
    Dim tmpText As String
    Dim tmpData As String
    Dim tmpAdid As String
    Dim tmpFlno As String
    Dim tmpOrg3 As String
    Dim tmpDes3 As String
    Dim tmpFlda As String
    Dim tmpFdmo As String
    Dim tmpStod As String
    Dim tmpStoa As String
    Dim tmpOfbl As String
    Dim tmpAirb As String
    Dim tmpLand As String
    Dim tmpOnbl As String
    Dim tmpEtai As String
    Dim tmpEtdi As String
    Dim tmpRegn As String
    Dim tmpGta1 As String
    Dim tmpPsta As String
    Dim tmpGtd1 As String
    Dim tmpPstd As String
    Dim tmpStodTime
    Dim tmpStoaTime
    Dim tmpFldaTime
    Dim TlxLineNo As Long
    Dim FltLineNo As Long
    If chkPrepare.Value = 1 Then
        EditTelex.chkTransmit.Value = 0
        EditTelex.chkSend.Value = 0
        EditTelex.chkPreView.Value = 0
        FltLineNo = TAB2.GetCurrentSelected
        If TestCaseMode Then
            TlxLineNo = TAB1.GetCurrentSelected
        Else
            TlxLineNo = 0
        End If
        If ((TlxLineNo >= 0) And (FltLineNo >= 0)) Or (chkTranslate.Value = 0) Then
            'TAB2.HeaderString = "I,FLIGHT,ORG,STD,VIA,DES,STA,FLDA"
            If TestCaseMode Then
                tmpText = "No. " & TAB1.GetColumnValues(TlxLineNo, "0,1,2")
                tmpText = Replace(tmpText, ",", " / ", 1, -1, vbBinaryCompare)
                EditTelex.lblTestCase.Caption = tmpText
                tmpText = TAB1.GetColumnValue(TlxLineNo, 3)
            Else
                If EditTelex.txtTelexText.Tag <> "" Then
                    tmpText = EditTelex.txtTelexText.Tag
                Else
                    tmpText = EditTelex.txtTelexText.Text
                    EditTelex.txtTelexText.Tag = EditTelex.txtTelexText.Text
                End If
                tmpText = CleanString(tmpText, FOR_SERVER, False)
            End If
            If chkTranslate.Value = 1 Then
                tmpAdid = TAB2.GetColumnValue(FltLineNo, 0)
                tmpStod = Trim(TAB2.GetColumnValue(FltLineNo, 3))
                tmpStoa = Trim(TAB2.GetColumnValue(FltLineNo, 6))
                If tmpStod = "" Then
                    tmpStoaTime = CedaFullDateToVb(tmpStoa)
                    tmpStodTime = DateAdd("h", -2, tmpStoaTime)
                    tmpStod = Format(tmpStodTime, "yyyymmddhhmm")
                End If
                If tmpStoa = "" Then
                    tmpStodTime = CedaFullDateToVb(tmpStod)
                    tmpStoaTime = DateAdd("h", 2, tmpStodTime)
                    tmpStoa = Format(tmpStoaTime, "yyyymmddhhmm")
                End If
                tmpStodTime = CedaFullDateToVb(tmpStod)
                tmpStoaTime = CedaFullDateToVb(tmpStoa)
                tmpFlno = TAB2.GetColumnValue(FltLineNo, 1)
                tmpFlno = Replace(tmpFlno, " ", "", 1, -1, vbBinaryCompare)
                If Len(tmpFlno) > 0 Then
                    tmpText = Replace(tmpText, "[FLNO]", tmpFlno, 1, -1, vbBinaryCompare)
                End If
                tmpFlda = Trim(TAB2.GetColumnValue(FltLineNo, 7))
                If tmpFlda = "" Then tmpFlda = tmpStod
                tmpFdmo = Mid(DecodeSsimDayFormat(tmpFlda, "CEDA", "SSIM2"), 3, 3)
                tmpFlda = Mid(tmpFlda, 7, 2)
                If Len(tmpFlda) > 0 Then
                    tmpText = Replace(tmpText, "[FLDA]", tmpFlda, 1, -1, vbBinaryCompare)
                End If
                If Len(tmpFdmo) > 0 Then
                    tmpText = Replace(tmpText, "[FDMO]", tmpFdmo, 1, -1, vbBinaryCompare)
                End If
                If tmpAdid = "A" Then
                    tmpOrg3 = Trim(TAB2.GetColumnValue(FltLineNo, 4))   'VIA3
                    If tmpOrg3 = "" Then tmpOrg3 = Trim(TAB2.GetColumnValue(FltLineNo, 2)) 'ORG3
                    tmpDes3 = Trim(TAB2.GetColumnValue(FltLineNo, 5))   'DES3
                Else
                    tmpDes3 = Trim(TAB2.GetColumnValue(FltLineNo, 4))   'VIA3
                    If tmpDes3 = "" Then tmpDes3 = Trim(TAB2.GetColumnValue(FltLineNo, 5)) 'DES3
                    tmpOrg3 = Trim(TAB2.GetColumnValue(FltLineNo, 2))   'ORG3
                End If
                If Len(tmpOrg3) > 0 Then
                    tmpText = Replace(tmpText, "[ORG3]", tmpOrg3, 1, -1, vbBinaryCompare)
                End If
                If Len(tmpDes3) > 0 Then
                    tmpText = Replace(tmpText, "[DES3]", tmpDes3, 1, -1, vbBinaryCompare)
                End If
                
                tmpStoa = Trim(TAB2.GetColumnValue(FltLineNo, 6))
                tmpStoa = Mid(tmpStoa, 9, 4)
                If Len(tmpStoa) > 0 Then
                    tmpText = Replace(tmpText, "[STOA]", tmpStoa, 1, -1, vbBinaryCompare)
                End If
                tmpStod = Trim(TAB2.GetColumnValue(FltLineNo, 3))
                tmpStod = Mid(tmpStod, 9, 4)
                If Len(tmpStod) > 0 Then
                    tmpText = Replace(tmpText, "[STOD]", tmpStod, 1, -1, vbBinaryCompare)
                End If
                tmpOfbl = Trim(TAB2.GetColumnValue(FltLineNo, 11))
                tmpOfbl = Mid(tmpOfbl, 9, 4)
                If Len(tmpOfbl) > 0 Then
                    tmpText = Replace(tmpText, "[OFBL]", tmpOfbl, 1, -1, vbBinaryCompare)
                End If
                tmpAirb = Trim(TAB2.GetColumnValue(FltLineNo, 12))
                tmpAirb = Mid(tmpAirb, 9, 4)
                If Len(tmpAirb) > 0 Then
                    tmpText = Replace(tmpText, "[AIRB]", tmpAirb, 1, -1, vbBinaryCompare)
                    tmpAirb = Mid(tmpAirb, 3, 2)
                    tmpText = Replace(tmpText, "[AIR2]", tmpAirb, 1, -1, vbBinaryCompare)
                End If
                tmpEtai = Trim(TAB2.GetColumnValue(FltLineNo, 13))
                tmpEtai = Mid(tmpEtai, 9, 4)
                If Len(tmpEtai) > 0 Then
                    tmpText = Replace(tmpText, "[ETAI]", tmpEtai, 1, -1, vbBinaryCompare)
                End If
                tmpEtai = Trim(TAB2.GetColumnValue(FltLineNo, 19))
                tmpEtai = Mid(tmpEtai, 9, 4)
                If Len(tmpEtai) > 0 Then
                    tmpText = Replace(tmpText, "[ETOA]", tmpEtai, 1, -1, vbBinaryCompare)
                End If
                tmpLand = Trim(TAB2.GetColumnValue(FltLineNo, 14))
                tmpLand = Mid(tmpLand, 9, 4)
                If Len(tmpLand) > 0 Then
                    tmpText = Replace(tmpText, "[LAND]", tmpLand, 1, -1, vbBinaryCompare)
                End If
                tmpOnbl = Trim(TAB2.GetColumnValue(FltLineNo, 15))
                tmpOnbl = Mid(tmpOnbl, 9, 4)
                If Len(tmpOnbl) > 0 Then
                    tmpText = Replace(tmpText, "[ONBL]", tmpOnbl, 1, -1, vbBinaryCompare)
                End If
                tmpEtdi = Trim(TAB2.GetColumnValue(FltLineNo, 16))
                tmpEtdi = Mid(tmpEtdi, 9, 4)
                If Len(tmpEtdi) > 0 Then
                    tmpText = Replace(tmpText, "[ETDI]", tmpEtdi, 1, -1, vbBinaryCompare)
                End If
                tmpEtdi = Trim(TAB2.GetColumnValue(FltLineNo, 20))
                tmpEtdi = Mid(tmpEtdi, 9, 4)
                If Len(tmpEtdi) > 0 Then
                    tmpText = Replace(tmpText, "[ETOD]", tmpEtdi, 1, -1, vbBinaryCompare)
                End If
                tmpRegn = Trim(TAB2.GetColumnValue(FltLineNo, 8))
                If Len(tmpRegn) > 0 Then
                    tmpText = Replace(tmpText, "[REGN]", tmpRegn, 1, -1, vbBinaryCompare)
                End If
                tmpGta1 = Trim(TAB2.GetColumnValue(FltLineNo, 9))
                If Len(tmpGta1) > 0 Then
                    tmpText = Replace(tmpText, "[GTA1]", tmpGta1, 1, -1, vbBinaryCompare)
                End If
                tmpPsta = Trim(TAB2.GetColumnValue(FltLineNo, 10))
                If Len(tmpPsta) > 0 Then
                    tmpText = Replace(tmpText, "[PSTA]", tmpPsta, 1, -1, vbBinaryCompare)
                End If
                tmpGtd1 = Trim(TAB2.GetColumnValue(FltLineNo, 17))
                If Len(tmpGtd1) > 0 Then
                    tmpText = Replace(tmpText, "[GTD1]", tmpGtd1, 1, -1, vbBinaryCompare)
                End If
                tmpPstd = Trim(TAB2.GetColumnValue(FltLineNo, 18))
                If Len(tmpPstd) > 0 Then
                    tmpText = Replace(tmpText, "[PSTD]", tmpPstd, 1, -1, vbBinaryCompare)
                End If
            End If
            tmpText = CleanString(tmpText, FOR_CLIENT, False)
            EditTelex.txtTelexText.Text = tmpText
            If TestCaseMode Then
                EditTelex.chkPreView.Value = 1
            End If
        End If
        chkPrepare.Value = 0
    End If
End Sub

Private Sub chkSave_Click()
    If chkSave.Value = 1 Then
        'TAB1.WriteToFile "c:\ufis\system\TelexTestCases.txt", False
        TAB1.WriteToFile UFIS_SYSTEM & "\TelexTestCases.txt", False
        chkSave.Value = 0
    End If
End Sub

Private Sub chkTranslate_Click()
    chkPrepare.Value = 1
End Sub

Private Sub Command1_Click()
    Dim tmpFile As String
    Dim tmpPath As String
    tmpPath = UFIS_TMP
    
    'tmpFile = "d:\tmp\tlxgenflights.txt"
    tmpFile = tmpPath + "\tlxgenflights.txt"
    'TAB2.WriteToFile tmpFile, False
    TAB2.ReadFromFile tmpFile
    TAB2.AutoSizeColumns
End Sub

Private Sub Form_Activate()
    MeIsVisible = True
End Sub

Private Sub Form_Load()
    MeIsVisible = False
    If MyMainPurpose = "AUTO_SEND" Then
        TestCaseMode = False
        chkLoad.Visible = False
        chkSave.Visible = False
        chkCopy.Visible = False
        chkClose.Left = chkLoad.Left
    End If
    TAB1.ResetContent
    TAB1.HeaderString = "Case,Type,ST,Text,Remark"
    TAB1.HeaderLengthString = "100,100,100,1000,1000"
    TAB1.AutoSizeByHeader = True
    TAB2.ResetContent
    '   tmpFields = "ADID,FLNO,ORG3,STOD,VIA3,DES3,STOA,FLDA,REGN,GTA1,PSTA,OFBL,AIRB,ETAI,LAND,ONBL,ETDI,GTD1,PSTD,ETOA,ETOD,DCD1,DCD2,DTD1,DTD2,PAX1,PAX2,PAX3"
    TAB2.HeaderString = "I,FLIGHT,ORG,STD,VIA,DES,STA,FLDA,REGN,GTA1,PSTA,OFBL,AIRB,ETAI,LAND,ONBL,ETDI,GTD1,PSTD,ETOA,ETOD,DCD1,DCD2,DTD1,DTD2,PAX1,PAX2,PAX3"
    TAB2.HeaderLengthString = "30,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100"
    TAB2.AutoSizeByHeader = True
    TAB2.DateTimeSetColumn 3
    TAB2.DateTimeSetInputFormatString 3, "YYYYMMDDhhmmss"
    TAB2.DateTimeSetOutputFormatString 3, "DDMMMYY'/'hh':'mm"
    TAB2.DateTimeSetColumn 6
    TAB2.DateTimeSetInputFormatString 6, "YYYYMMDDhhmmss"
    TAB2.DateTimeSetOutputFormatString 6, "DDMMMYY'/'hh':'mm"
    TAB2.DateTimeSetColumn 7
    TAB2.DateTimeSetInputFormatString 7, "YYYYMMDD"
    TAB2.DateTimeSetOutputFormatString 7, "DDMMMYY"
    TAB2.ShowHorzScroller True
    TAB2.LifeStyle = True
    If Not TestCaseMode Then
        TAB1.Visible = False
        chkTranslate.Visible = False
        Me.Width = 6400
        chkLoad.Enabled = False
        TestCases.Caption = "Flight List"
    End If
    Timer1.Enabled = True
End Sub

Private Sub Form_Resize()
    Dim NewVal As Long
    NewVal = Me.ScaleWidth - TAB1.Left - 30
    If NewVal > 300 Then TAB1.Width = NewVal
    NewVal = Me.ScaleHeight - TAB1.Top - StatusBar1.Height
    If NewVal > 300 Then
        TAB1.Height = NewVal
        TAB2.Height = NewVal
    End If
End Sub

Private Sub TAB1_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    On Error Resume Next
    If (LineNo >= 0) And (Selected) Then
        chkPrepare.Value = 1
        If MeIsVisible Then Me.SetFocus
    End If
End Sub

Private Sub TAB2_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    On Error Resume Next
    If (LineNo >= 0) And (Selected) Then
        chkPrepare.Value = 1
        If MeIsVisible Then Me.SetFocus
    End If
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    EditTelex.OnTop.Value = 1
    Me.Refresh
    chkLoad.Value = 1
    chkFlights.Value = 1
End Sub

Public Sub RegisterMyParent(Caller As Form, CallButton As Control, TaskValue As String)
    'Set MyParent = Caller
    'Set MyCallButton = CallButton
End Sub

