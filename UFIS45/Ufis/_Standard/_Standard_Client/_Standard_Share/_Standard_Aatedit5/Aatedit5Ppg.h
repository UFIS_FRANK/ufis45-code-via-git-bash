#if !defined(AFX_AATEDIT5PPG_H__C3865255_FE62_11D4_90BC_00010204AA51__INCLUDED_)
#define AFX_AATEDIT5PPG_H__C3865255_FE62_11D4_90BC_00010204AA51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Aatedit5Ppg.h : Declaration of the CAatedit5PropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CAatedit5PropPage : See Aatedit5Ppg.cpp.cpp for implementation.

class CAatedit5PropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CAatedit5PropPage)
	DECLARE_OLECREATE_EX(CAatedit5PropPage)

// Constructor
public:
	CAatedit5PropPage();

// Dialog Data
	//{{AFX_DATA(CAatedit5PropPage)
	enum { IDD = IDD_PROPPAGE_AATEDIT5 };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CAatedit5PropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AATEDIT5PPG_H__C3865255_FE62_11D4_90BC_00010204AA51__INCLUDED)
