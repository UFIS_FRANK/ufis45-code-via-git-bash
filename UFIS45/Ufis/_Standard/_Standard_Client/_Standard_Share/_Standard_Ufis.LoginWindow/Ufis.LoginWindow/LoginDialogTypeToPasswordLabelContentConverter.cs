﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Ufis.LoginWindow
{

    /// <summary>
    /// Converts LoginDialogType to password label content.
    /// </summary>
    [ValueConversion(typeof(LoginDialogType), typeof(string))]
    public class LoginDialogTypeToPasswordLabelContentConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            string strLabel = "Password:";

            if (value != null)
            {
                LoginDialogType loginDialogType = (LoginDialogType)value;

                if (loginDialogType == LoginDialogType.ChangePasswordDialog)
                    strLabel = "Current password:";
            }

            return strLabel;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
