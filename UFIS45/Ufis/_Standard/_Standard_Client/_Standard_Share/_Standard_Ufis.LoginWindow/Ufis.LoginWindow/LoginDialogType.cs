﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.LoginWindow
{
    public enum LoginDialogType
    {
        LoginDialog,
        ChangePasswordDialog,
        RegisterModuleDialog
    }
}
