﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Ufis.MVVM.ViewModel;
using Ufis.Security;
using Ufis.Utilities;
using Ufis.Entities;
using Ufis.MVVM.Command;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Core;
using Ufis.ADLoginUser;

namespace Ufis.LoginWindow
{
    /// <summary>
    /// Represent the view model for login window.
    /// </summary>
    public class LoginViewModel : WorkspaceViewModel
    {
        #region Constants

        private const string INFORMATION_CAPTION = "Information";
        private const string WARNING_CAPTION = "Warning";
        private const string CONTACT_ADMINISTRATOR = "Please contact your System Administrator.";
        private const string APPLICATION_DISABLED = "The module is deactivated. " + CONTACT_ADMINISTRATOR;
        private const string APPLICATION_EXPIRED = "The module is either out of date or deactivated. " + CONTACT_ADMINISTRATOR;
        private const string DB_ERROR = "Error accessing the database. " + CONTACT_ADMINISTRATOR;
        private const string CHANGE_PASSWORD_REMINDER = "You must change your password within the next {0} day(s).";
        private const string INVALID_APPLICATION = "The module name does not exist! " + CONTACT_ADMINISTRATOR;
        private const string UNDEFINED_PROFILE = "User Name doesn't have a valid profile! " + CONTACT_ADMINISTRATOR;
        private const string INVALID_DOMAIN = "Current Windows login user is invalid to use the system!" + CONTACT_ADMINISTRATOR;
        private const string INVALID_CREDENTIAL = "The username or password you entered is incorrect.";
        private const string OVERDUE_LOGIN = "Your user account has been deactivated because it has not been used for {0} days.";
        private const string USER_DEACTIVATED = "The maximum number of login attempts has been exceeded. " +
            "The user {0} has been deactivated. " + CONTACT_ADMINISTRATOR;
        private const string INVALID_PASSWORD_USER_DEACTIVATED = "The password you entered is incorrect. " +
            USER_DEACTIVATED;
        private const string USER_MUST_CHANGE_PASSWORD = "You must change the password before you can use this account.";
        private const string NEED_TO_REGISTER_MODULE = "The application needs to be registered.\n\n" +
            "Please click the register button to register the application.";
        private const string REGISTRATION_ERROR = "Unable to register the application. The following error(s) occured:\n{0}";
        private const string CURRENT_AND_NEW_PASSWORD_SAME = "The new password must be different than the current one.";
        private const string NEW_PASSWORD_MISMATCHED = "The new password and the confirmation password are different.";
        private const string INVALID_NEW_PASSWORD_START_WITH_SPACE = "The new password must not start with blank space.";
        private const string INVALID_NEW_PASSWORD_LENGTH = "The length of the new password is less than {0} characters.";
        private const string INVALID_NEW_PASSWORD_CONSECUTIVE = "The new password can not contain more than %d consecutive char(s) of the user ID. ";
        private const string INVALID_NEW_PASSWORD_NUMBERS = "The new password must contain minimum %d number(s).";
        private const string INVALID_NEW_PASSWORD_SMALL_LETTERS = "The new password must contain minimum %d small letter(s).";
        private const string INVALID_NEW_PASSWORD_CAPITAL_LETTERS = "The new password must contain minimum %d capital letter(s).";
        private const string CHANGE_PASSWORD_SUCCEEDED = "Your password successfully changed.\n" + 
            "Please re-login with your new password.";
        private const string WAITING_APPLICATION_ENROLS = "Please wait while we are bringing {0} to you.";
        private const string  UNAUTHORIZED_ACCESS = "If you are not authorised to use the {0} UFIS system, " + 
            "please terminate the application immediately.\n" +
            "The use of this application for any illegal purposes or for any purpose other than as " +
            "permitted by {1} is strictly prohibited and such misuse may result in legal proceedings.";

        #endregion

        #region Fields

        private readonly IUfisUserAuthentication _userAuthentication;
        private UserPrivileges _userPrivileges;
        private MessageInfo _loginInfo;
        private bool _bloginSucceeded;
        private RelayCommand _loginCommand;
        private RelayCommand _registerModuleCommand;
        private RelayCommand _ignoreModuleRegistrationCommand;
        private RelayCommand _changePasswordCommand;
        private bool _bShowLoadingBar;
        private LoginDialogType _loginDialogType = LoginDialogType.LoginDialog;
        private int _iRemainingDaysToChangePassword;
        private Boolean _UseAD = false;
        private Boolean _CallFrom = false;
        #endregion // Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of login view model.
        /// </summary>
        public LoginViewModel() { }
        
        /// <summary>
        /// Initializes a new instance of login view model.
        /// </summary>
        /// <param name="appInfo">Application information.</param>
        /// <param name="userAuthentication">Class that provides functionality for user authentication.</param>
        public LoginViewModel(AppInfo appInfo, IUfisUserAuthentication userAuthentication)
            : this(appInfo, userAuthentication, new EntUser()) 
        {

        }

        /// <summary>
        /// Initializes a new instance of login view model.
        /// </summary>
        /// <param name="appInfo">Application information.</param>
        /// <param name="userAuthentication">Class that provides functionality for user authentication.</param>
        /// <param name="user">The user to login.</param>
        /// <param name="ADLogin">System.Boolean containg AD Validation Flag</param>
        public LoginViewModel(AppInfo appInfo, IUfisUserAuthentication userAuthentication, EntUser user)
        {
            ApplInfo = appInfo;
            User = user;
            _userAuthentication = userAuthentication;
            //Check AD Used or not
            if (ADLoginUser.ADLoginUser.CheckLDAP())
            {
                _UseAD = ADLoginUser.ADLoginUser.CheckLDAP();
                _CallFrom = true;
                Login(null);
            }

        }        
        #endregion // Constructor

        #region User Properties

        public string Username
        {
            get { return User.UserId; }
            set
            {
                if (value == User.UserId)
                    return;

                User.UserId = value;

                OnPropertyChanged("Username");
            }
        }

        #endregion // User Properties

        #region Public Interface

        /// <summary>
        /// Gets or sets the login dialog type.
        /// </summary>
        public LoginDialogType DialogType
        {
            get
            {
                return _loginDialogType;
            }
            set
            {
                if (_loginDialogType == value)
                    return;

                _loginDialogType = value;
                OnPropertyChanged("DialogType");
            }
        }

        /// <summary>
        /// Gets or sets the login information message.
        /// </summary>
        public MessageInfo LoginInfo
        {
            get
            {
                return _loginInfo;
            }
            set
            {
                if (_loginInfo == value)
                    return;

                _loginInfo = value;
                OnPropertyChanged("LoginInfo");
            }
        }

        /// <summary>
        /// Gets or sets whether to automatically save the user id when he/she successfuly login.
        /// </summary>
        public bool AutoSaveLastUser { get; set; }

        /// <summary>
        /// Gets or sets the application information.
        /// </summary>
        public AppInfo ApplInfo { get; set; }

        /// <summary>
        /// Gets or sets the project information.
        /// </summary>
        public ProjectInfo ProjectInfo { get; set; }

        /// <summary>
        /// Gets or sets the user that login to application.
        /// </summary>
        public EntUser User { get; set; }

        /// <summary>
        /// Gets the user privilages.
        /// </summary>
        public UserPrivileges UserPrivileges
        {
            get { return _userPrivileges; }
        }

        /// <summary>
        /// Gets a value indicating whether the login was successful.
        /// </summary>
        public bool LoginSucceeded
        {
            get { return _bloginSucceeded; }
        }

        /// <summary>
        /// Gets or sets whether the loading bar will be displayed on the screen.
        /// </summary>
        public bool ShowLoadingBar
        {
            get { return _bShowLoadingBar; }
            set
            {
                if (_bShowLoadingBar == value)
                    return;

                _bShowLoadingBar = value;
                OnPropertyChanged("ShowLoadingBar");
            }
        }

        /// <summary>
        /// Returns a command that logs in the user.
        /// </summary>
        public ICommand LoginCommand
        {
            get
            {
                if (_loginCommand == null)
                {
                    _loginCommand = new RelayCommand(Login);
                }
                return _loginCommand;
            }
        }

        /// <summary>
        /// Returns a command that registers the module.
        /// </summary>
        public ICommand RegisterModuleCommand
        {
            get
            {
                if (_registerModuleCommand == null)
                {
                    _registerModuleCommand = new RelayCommand(RegisterModule);
                }
                return _registerModuleCommand;
            }
        }

        /// <summary>
        /// Returns a command that ignores the module registration.
        /// </summary>
        public ICommand IgnoreModuleRegistrationCommand
        {
            get
            {
                if (_ignoreModuleRegistrationCommand == null)
                {
                    _ignoreModuleRegistrationCommand = new RelayCommand(IgnoreModuleRegistration);
                }
                return _ignoreModuleRegistrationCommand;
            }
        }

        /// <summary>
        /// Returns a command that changes user password.
        /// </summary>
        public ICommand ChangePasswordCommand
        {
            get
            {
                if (_changePasswordCommand == null)
                {
                    _changePasswordCommand = new RelayCommand(ChangePassword);
                }
                return _changePasswordCommand;
            }
        }

        #endregion // Public Interface

        #region Public Methods

        /// <summary>
        /// Logins the user.
        /// </summary>
        public void Login(object parameter)
        {
            AuthenticationResult result = AuthenticationResult.Default;
            UserPrivileges userPrivileges = new UserPrivileges();
            SecurityInfo info = new SecurityInfo();
            //Check if application using AD and Calling flag is true, call to single signon
            //Phyoe Khaing Min
            //24-Apr-2012
            if (_UseAD && _CallFrom)
            {
                EntADLoginUser objEntADLoginUser = new EntADLoginUser();
                
                objEntADLoginUser = ADLoginUser.ADLoginUser.GetADLoginUserEntity();

                if (objEntADLoginUser != null)
                {
                    if (objEntADLoginUser.ComputerName != objEntADLoginUser.DomainName)
                    {
                        result = _userAuthentication.SSOAuthenticateUser(objEntADLoginUser.UserName, objEntADLoginUser.DomainName, userPrivileges, info);
                    }
                    else
                    {
                        LoginInfo = new MessageInfo()
                        {
                            InfoText = INVALID_DOMAIN,
                            Severity = MessageInfo.MessageInfoSeverity.Error
                        };
                    }
                    
                }
                if (result == AuthenticationResult.Succeeded)
                {
                    _iRemainingDaysToChangePassword = info.RemainingDaysToChangePassword;
                    _userPrivileges = userPrivileges;
                    ContinueToApplication();
                }
            }
            else
            {
                PasswordBoxEdit passwordBox = (PasswordBoxEdit)parameter;                
                if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(passwordBox.Password))
                    result = AuthenticationResult.InvalidUserName;
                else
                    result = _userAuthentication.AuthenticateUser(Username, passwordBox.Password, userPrivileges, info);

            }
            _CallFrom = false;
            switch (result)
            {
                case AuthenticationResult.ApplicationDisabled:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = APPLICATION_DISABLED,
                        Severity = MessageInfo.MessageInfoSeverity.Error
                    };
                    break;

                case AuthenticationResult.ApplicationExpired:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = APPLICATION_EXPIRED,
                        Severity = MessageInfo.MessageInfoSeverity.Error
                    };
                    break;

                case AuthenticationResult.FailedUnknownReason:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = DB_ERROR,
                        Severity = MessageInfo.MessageInfoSeverity.Error
                    };
                    break;
                
                case AuthenticationResult.InvalidApplication:
                    LoginInfo = new MessageInfo() 
                    { 
                        InfoText = INVALID_APPLICATION, 
                        Severity = MessageInfo.MessageInfoSeverity.Error 
                    };
                    break;

                case AuthenticationResult.InvalidPassword:
                case AuthenticationResult.InvalidUserName:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = INVALID_CREDENTIAL,
                        Severity = MessageInfo.MessageInfoSeverity.Error
                    };
                    break;

                case AuthenticationResult.InvalidPasswordUserDeactivated:
                case AuthenticationResult.UserExpired:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = String.Format(INVALID_PASSWORD_USER_DEACTIVATED, Username),
                        Severity = MessageInfo.MessageInfoSeverity.Information
                    };
                    break;

                case AuthenticationResult.NeedToInitializeModule:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = NEED_TO_REGISTER_MODULE,
                        Severity = MessageInfo.MessageInfoSeverity.Information
                    };
                    DialogType = LoginDialogType.RegisterModuleDialog;
                    
                    _iRemainingDaysToChangePassword = info.RemainingDaysToChangePassword;
                    _userPrivileges = userPrivileges;

                    break;

                case AuthenticationResult.OverdueLogin:                    
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = String.Format(OVERDUE_LOGIN, info.InactiveDaysAllowed),
                        Severity = MessageInfo.MessageInfoSeverity.Error
                    };
                    break;

                case AuthenticationResult.Succeeded:
                    _iRemainingDaysToChangePassword = info.RemainingDaysToChangePassword;
                    _userPrivileges = userPrivileges;
                    ContinueToApplication();
                    break;
               
                case AuthenticationResult.UserDeactivated:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = String.Format(USER_DEACTIVATED, Username),
                        Severity = MessageInfo.MessageInfoSeverity.Error
                    };
                    break;

                case AuthenticationResult.UserMustChangePassword:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = USER_MUST_CHANGE_PASSWORD + info.AdditionalErrorMessage,
                        Severity = MessageInfo.MessageInfoSeverity.Information
                    };
                    DialogType = LoginDialogType.ChangePasswordDialog;
                    break;

                case AuthenticationResult.UndefinedProfile:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = UNDEFINED_PROFILE,
                        Severity = MessageInfo.MessageInfoSeverity.Error
                    };
                    break;
            }
        }

        private void ContinueToApplication()
        {
            LoginInfo = new MessageInfo()
            {
                InfoText = String.Format(WAITING_APPLICATION_ENROLS, ApplInfo.ProductTitle),
                Severity = MessageInfo.MessageInfoSeverity.OK
            };

            ShowChangePasswordReminder(_iRemainingDaysToChangePassword);
            ShowUnauthorizedAccessWarning();

            _bloginSucceeded = true;

            if (AutoSaveLastUser) SaveLastUserId(Username);
            
            CloseCommand.Execute(null);
            
        }

        /// <summary>
        /// Registers the module.
        /// </summary>
        public void RegisterModule(object parameter)
        {
            string strResult = _userAuthentication.RegisterModule(ApplInfo.RegistrationString);
            if (!string.IsNullOrEmpty(strResult))
            {
                LoginInfo = new MessageInfo()
                {
                    InfoText = String.Format(REGISTRATION_ERROR, strResult),
                    Severity = MessageInfo.MessageInfoSeverity.Error
                };
            }
            else
            {
                ContinueToApplication();
            }
        }

        /// <summary>
        /// Ignores module registration.
        /// </summary>
        public void IgnoreModuleRegistration(object parameter)
        {
            ContinueToApplication();
        }

        /// <summary>
        /// Changes user password.
        /// </summary>
        public void ChangePassword(object parameter)
        {
            object[] arrObjects = (object[])parameter;

            string strCurrentPassword = ((PasswordBoxEdit)arrObjects[0]).Password;
            string strNewPassword = ((PasswordBoxEdit)arrObjects[1]).Password;
            string strConfirmNewPassword = ((PasswordBoxEdit)arrObjects[2]).Password;

            ChangePasswordResult result;
            string strMessage = null;
            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(strCurrentPassword) || string.IsNullOrEmpty(strNewPassword))
                result = ChangePasswordResult.InvalidUser;
            else if (strNewPassword != strConfirmNewPassword)
                result = ChangePasswordResult.MismatchedNewPassword;
            else
                result = ChangePasswordExtended(Username, strCurrentPassword, strNewPassword, out strMessage);

            switch (result)
            {
                case ChangePasswordResult.InvalidUser:
                case ChangePasswordResult.InvalidPassword:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = INVALID_CREDENTIAL,
                        Severity = MessageInfo.MessageInfoSeverity.Error
                    };
                    break;

                case ChangePasswordResult.MismatchedNewPassword:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = NEW_PASSWORD_MISMATCHED,
                        Severity = MessageInfo.MessageInfoSeverity.Error
                    };
                    break;

                case ChangePasswordResult.InvalidNewPassword:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = strMessage,
                        Severity = MessageInfo.MessageInfoSeverity.Error
                    };
                    break;

                case ChangePasswordResult.Succeeded:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = CHANGE_PASSWORD_SUCCEEDED,
                        Severity = MessageInfo.MessageInfoSeverity.OK
                    };
                    DialogType = LoginDialogType.LoginDialog;
                    break;

                case ChangePasswordResult.FailedUnknownReason:
                    LoginInfo = new MessageInfo()
                    {
                        InfoText = DB_ERROR,
                        Severity = MessageInfo.MessageInfoSeverity.Error
                    };
                    break;
            }
        }

        private ChangePasswordResult ChangePasswordExtended(string userName, string oldPassword, string newPassword, out string message)
        {
            ChangePasswordResult result = ChangePasswordResult.Succeeded;
            message = null;
            
            if (oldPassword == newPassword)
            {
                message = CURRENT_AND_NEW_PASSWORD_SAME;
                result = ChangePasswordResult.InvalidNewPassword;
            }
            else if (newPassword.StartsWith(" "))
            {
                message = INVALID_NEW_PASSWORD_START_WITH_SPACE;
                result = ChangePasswordResult.InvalidNewPassword;
            }
            else
            {
                PasswordRule passwordRule = _userAuthentication.GetPasswordRule();
                if (passwordRule != null)
                    result = ValidatePasswordWithPasswordRule(userName, newPassword, passwordRule, out message);
                    
                if (result == ChangePasswordResult.Succeeded)
                    result = _userAuthentication.ChangePassword(userName, oldPassword, newPassword);
            }

            return result;
        }

        private ChangePasswordResult ValidatePasswordWithPasswordRule(string userName, string password, PasswordRule passwordRule, out string message)
        {
            ChangePasswordResult result = ChangePasswordResult.Succeeded;
            message = null;

            if (password.Length < passwordRule.MinLength)
            {
                message = INVALID_NEW_PASSWORD_LENGTH;
                result = ChangePasswordResult.InvalidNewPassword;
            }
            else
            {
                bool bInvalidIdentical = false;
                if (passwordRule.MaxIdenticalCharsWithUserName >= 2)
                {                    
                    for (int i = 0; i < userName.Length - passwordRule.MaxIdenticalCharsWithUserName + 1; i++)
                    {
                        string strTemp = userName.Substring(i, passwordRule.MaxIdenticalCharsWithUserName + 1);

                        if (password.Contains(strTemp))
                        {
                            bInvalidIdentical = true;
                            break;
                        }
                    }
                }

                if (bInvalidIdentical)
                {
                    message = string.Format(INVALID_NEW_PASSWORD_CONSECUTIVE, passwordRule.MaxIdenticalCharsWithUserName);
                    result = ChangePasswordResult.InvalidNewPassword;
                }
                else
                {
                    int iCaptitalLetters = 0;
                    int iSmallLetters = 0;
                    int iNumericChars = 0;

                    foreach (char c in password)
                    {
                        if (char.IsDigit(c))
                            iNumericChars++;

                        if (char.IsLower(c))
                            iSmallLetters++;

                        if (char.IsUpper(c))
                            iCaptitalLetters++;
                    }

                    if (iNumericChars < passwordRule.MinNumericChars)
                    {
                        message = string.Format(INVALID_NEW_PASSWORD_NUMBERS, passwordRule.MinNumericChars);
                        result = ChangePasswordResult.InvalidNewPassword;
                    }
                    else if (iSmallLetters < passwordRule.MinSmallLetters)
                    {
                        message = string.Format(INVALID_NEW_PASSWORD_SMALL_LETTERS, passwordRule.MinSmallLetters);
                        result = ChangePasswordResult.InvalidNewPassword;
                    }
                    else if (iCaptitalLetters < passwordRule.MinCapitalLetters)
                    {
                        message = string.Format(INVALID_NEW_PASSWORD_CAPITAL_LETTERS, passwordRule.MinCapitalLetters);
                        result = ChangePasswordResult.InvalidNewPassword;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the last user who logged in.
        /// </summary>
        /// <param name="getLastUser"></param>
        /// <returns></returns>
        public static EntUser GetLastUser()
        {
            return new EntUser()
            {
                UserId = GetLastUserId()
            };
        }
        
        #endregion // Public Methods

        #region Private Methods

        private static string GetLastUserId()
        {
            string strLastUserId = string.Empty;

            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.CurrentUser;
            regKey = regKey.OpenSubKey("AatLoginCtrl\\Login\\LastUserName");
            if (regKey != null)
            {
                strLastUserId = regKey.GetValue(null, strLastUserId).ToString();
            }

            return strLastUserId;
        }

        private bool SaveLastUserId(string userId)
        {
            bool bReturn = false;

            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.CurrentUser;
            regKey = regKey.OpenSubKey("AatLoginCtrl\\Login\\LastUserName", true);
            try
            {
                if (regKey == null)
                    regKey.CreateSubKey("AatLoginCtrl\\Login\\LastUserName");

                if (regKey != null)
                {
                    regKey.SetValue(null, userId);
                    bReturn = true;
                }
            }
            catch
            {
                bReturn = false;
            }

            return bReturn;
        }

        private void ShowUnauthorizedAccessWarning()
        {
            if (ProjectInfo == null)
                return;

            if (ProjectInfo.ProjectName == null || ProjectInfo.ProjectName == null)
                return;

            string strMessage = string.Format(UNAUTHORIZED_ACCESS, ProjectInfo.ProjectName, ProjectInfo.CustomerName);
            DXMessageBox.Show(strMessage, WARNING_CAPTION, System.Windows.MessageBoxButton.OK, 
                System.Windows.MessageBoxImage.Warning);
        }

        private void ShowChangePasswordReminder(int daysRemain)
        {
            if (daysRemain <= 0) 
                return;

            string strMessage = string.Format(CHANGE_PASSWORD_REMINDER, daysRemain);
            DXMessageBox.Show(strMessage, INFORMATION_CAPTION, System.Windows.MessageBoxButton.OK,
                System.Windows.MessageBoxImage.Information);
        }

        #endregion
    }
}
