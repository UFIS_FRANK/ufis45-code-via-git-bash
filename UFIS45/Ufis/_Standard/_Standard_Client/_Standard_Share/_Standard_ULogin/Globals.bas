Attribute VB_Name = "Globals"
Option Explicit

'Public Const DEFAULT_CEDA_INI = "C:\UFIS\SYSTEM\CEDA.INI"
Public DEFAULT_CEDA_INI As String
Public UFIS_SYSTEM As String
Public UFIS_HELP As String
Public UFIS_APPL As String
Public UFIS_TMP As String
'Public UFIS_UFIS As String

Public TableExtension  As String
Public HomeAirport As String

Public HostType  As String
Public HostName As String

Public UserName As String
Public UserPassword As String

Public ApplicationName As String
Public ApplicationRegisterString As String
Public gVersionString As String

Public CedaIsConnected As Boolean
Public strLoginResult As String
Public ogLoginAttempts As Integer

Public colStatus As New Collection

' members concerning the info-button
Public Info_ShowButton As Boolean
Public ogUfisVersion As String
Public ogApplication As String
Public ogCopyright As String
Public ogAAT As String
Public ogInfoCaption As String

' communicate with the database
Public UfisComControl As Object

'display the user-name in small letters?
Public ogUserNameLCase As Boolean

Public Sub GetUfisDir()
    Dim EnvString As String
    
    EnvString = Environ("UFISSYSTEM")
    If EnvString <> "" Then
        UFIS_SYSTEM = EnvString
    Else
        UFIS_SYSTEM = "C:\UFIS\SYSTEM"
    End If
    EnvString = Environ("UFISHELP")
    If EnvString <> "" Then
        UFIS_HELP = EnvString
    Else
        UFIS_HELP = "C:\UFIS\HELP"
    End If
    EnvString = Environ("UFISAPPL")
    If EnvString <> "" Then
        UFIS_APPL = EnvString
    Else
        UFIS_APPL = "C:\UFIS\APPL"
    End If
    EnvString = Environ("UFISTMP")
    If EnvString <> "" Then
        UFIS_TMP = EnvString
    Else
        UFIS_TMP = "C:\TMP"
    End If
    EnvString = Environ("CEDA")
    If EnvString <> "" Then
        DEFAULT_CEDA_INI = EnvString
    Else
        DEFAULT_CEDA_INI = "C:\UFIS\SYSTEM\CEDA.INI"
    End If
    'UFIS_UFIS = GetItem(DEFAULT_CEDA_INI, 1, "\") & "\" & GetItem(DEFAULT_CEDA_INI, 2, "\")
End Sub


