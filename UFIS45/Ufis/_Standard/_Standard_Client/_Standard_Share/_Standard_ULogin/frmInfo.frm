VERSION 5.00
Begin VB.Form frmInfo 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   3990
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6300
   Icon            =   "frmInfo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   266
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   420
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   360
      Left            =   2580
      TabIndex        =   0
      Tag             =   "1004"
      Top             =   3480
      Width           =   1140
   End
   Begin VB.Frame Frame1 
      Height          =   1800
      Left            =   150
      TabIndex        =   1
      Top             =   150
      Width           =   6015
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   120
         Picture         =   "frmInfo.frx":000C
         ScaleHeight     =   495
         ScaleWidth      =   1215
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   1080
         Width           =   1215
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   480
         Picture         =   "frmInfo.frx":0F18
         ScaleHeight     =   495
         ScaleWidth      =   495
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   360
         Width           =   495
      End
      Begin VB.Label lblAAT 
         Caption         =   "ABB Airport Technolgies GmbH / Information Systems"
         Height          =   255
         Left            =   1560
         TabIndex        =   7
         Top             =   1365
         Width           =   3975
      End
      Begin VB.Label lblCopyright 
         Caption         =   "Copyright 2001 AAT/I"
         Height          =   255
         Left            =   1560
         TabIndex        =   6
         Top             =   1095
         Width           =   3975
      End
      Begin VB.Label lblApplication 
         Caption         =   "Application XYZ 1.2.3.4 / 01.01.2001"
         Height          =   255
         Left            =   1560
         TabIndex        =   5
         Top             =   660
         Width           =   3975
      End
      Begin VB.Label lblUfisVersion 
         Caption         =   "UFIS, Version 4.4"
         Height          =   255
         Left            =   1560
         TabIndex        =   4
         Top             =   360
         Width           =   3975
      End
   End
   Begin VB.Label lblNumberLogins 
      Height          =   225
      Left            =   2280
      TabIndex        =   13
      Top             =   2775
      Width           =   3975
   End
   Begin VB.Label lblUser 
      Height          =   225
      Left            =   2280
      TabIndex        =   12
      Top             =   2475
      Width           =   3855
   End
   Begin VB.Label lblConnectionServer 
      Height          =   225
      Left            =   2280
      TabIndex        =   11
      Top             =   2175
      Width           =   3855
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "1038"
      Height          =   225
      Left            =   240
      TabIndex        =   10
      Tag             =   "1038"
      Top             =   2775
      Width           =   1935
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "1037"
      Height          =   225
      Left            =   240
      TabIndex        =   9
      Tag             =   "1037"
      Top             =   2475
      Width           =   1935
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "1036"
      Height          =   225
      Left            =   240
      TabIndex        =   8
      Tag             =   "1036"
      Top             =   2175
      Width           =   1935
   End
End
Attribute VB_Name = "frmInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdOK_Click()
    frmInfo.Hide
    Unload frmInfo
End Sub

Private Sub Form_Load()
    frmInfo.Caption = Globals.ogInfoCaption
    Label1.Caption = LoadResString(Label1.Tag)
    Label2.Caption = LoadResString(Label2.Tag)
    Label3.Caption = LoadResString(Label3.Tag)

    lblUfisVersion.Caption = Globals.ogUfisVersion
    lblApplication.Caption = Globals.ogApplication
    lblAAT.Caption = Globals.ogAAT
    lblCopyright.Caption = Globals.ogCopyright

    lblConnectionServer.Caption = Globals.HostName & " / " & Globals.HostType
    lblUser.Caption = "XXX"
    lblNumberLogins.Caption = ""
End Sub
