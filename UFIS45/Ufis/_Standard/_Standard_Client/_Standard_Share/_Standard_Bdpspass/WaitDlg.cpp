// WaitDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpspass.h>
#include <resource.h>
#include <WaitDlg.h>
#include <Ccsglobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WaitDlg dialog


WaitDlg::WaitDlg(CWnd* pParent /*=NULL*/,CString csText)
	: CDialog(WaitDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(WaitDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_csText = csText;

	// Nicht-modaler Dialog mu� per CDialog::Create() erzeugt werden
	CDialog::Create(IDD, pParent);
}


void WaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(WaitDlg)
	DDX_Control(pDX, IDC_ANIMATE1, m_oAnimate);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(WaitDlg, CDialog)
	//{{AFX_MSG_MAP(WaitDlg)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// WaitDlg message handlers
BOOL WaitDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText("BDPSPASS");
	CWnd *polWnd = GetDlgItem(1003); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_WAITDLG_LOADING));
	}

	// Warte Mouse
	AfxGetApp()->DoWaitCursor(1);
	
	// Laden des AVI Files und abspielen
	CAnimateCtrl *pCtrl=(CAnimateCtrl*)GetDlgItem(IDC_ANIMATE1);
	pCtrl->Open(IDR_FILECOPY);
	//pCtrl->Open(IDR_DATACOMP);
	pCtrl->Play(0,-1,-1);
	
	// Default Text setzen
	if (m_csText == "")	m_csText = "Please Wait...";

	SetDlgItemText(IDC_TEXT,m_csText);
	
	return TRUE;  
}

//***************************************************************************************
//
//***************************************************************************************

void WaitDlg::OnClose() 
{
	// Warte Mouse
	AfxGetApp()->DoWaitCursor(-1);
	CDialog::OnClose();
}
