#if !defined(AFX_NCOMBO_H__DE20178E_9000_11D2_8726_0040055C08D9__INCLUDED_)
#define AFX_NCOMBO_H__DE20178E_9000_11D2_8726_0040055C08D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// NCombo.h : main header file for NCOMBO.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CNComboApp : See NCombo.cpp for implementation.

class CNComboApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;
extern BOOL RegisterWindowClass(LPCTSTR lpszClassName);
extern void CloneFont(CFont* pFontSource, CFont* pFontDestination,  LPCTSTR lpszDefaultFont = _T("-11,0,0,0,400,0,0,0,0,3,2,1,34,Arial"));
extern BOOL CreateFontDisp(IDispatch* pDispatchFont, CFont& font);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NCOMBO_H__DE20178E_9000_11D2_8726_0040055C08D9__INCLUDED)
