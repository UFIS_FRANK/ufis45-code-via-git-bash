/************************************
  REVISION LOG ENTRY
  Revision By: Mihai Filimon
  Revised on 12/11/98 3:19:24 PM
  Comments: NWindow.h : header file
 ************************************/


#if !defined(AFX_NWINDOW_H__DE2017A0_9000_11D2_8726_0040055C08D9__INCLUDED_)
#define AFX_NWINDOW_H__DE2017A0_9000_11D2_8726_0040055C08D9__INCLUDED_

#include "NListCtrl.h"	// Added by ClassView
#include "NHeaderCtrl.h"	// Added by ClassView
#include "NColumn.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CNWindow window

#include <afxtempl.h>

class CNWindow : public CWnd
{
	friend class CNEdit;
	friend class CNListCtrl;
	friend class CNHeaderCtrl;
	friend class CNComboCtrl;
// Construction
public:
	CNWindow();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNWindow)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual short FindItemData(long nItemData);
	virtual short FindItemText(short nColumn, LPCTSTR lpszColumnText);
	virtual BOOL GetEnableColumn(int nColumn);
	virtual BOOL GetVisibleColumn(int nColumn);
	virtual long GetSelectItem();
	virtual CString GetColumnName(int nColumn);
	virtual BOOL GetResizing();
	virtual long GetItemCount();
	virtual long GetColumnCount();
	virtual void SetSelectItem(int nItem);
	virtual void SetResizing(BOOL bSizing = TRUE);
	virtual BOOL SetColumnWidth(int iColumn, int nNewValue);
	virtual DWORD GetItemData(int nItem);
	virtual CString GetItemText(int nItem, int nSubItem = 0);
	virtual BOOL SetItemText( int nItem, int nSubItem, LPCTSTR lpszText );
	virtual BOOL SetItemData( int nItem, DWORD dwData );
	virtual int InsertItem( int nItem, LPCTSTR lpszItem );
	virtual BOOL VisibleColumn(int nColumn, BOOL bVisible = TRUE);
	virtual BOOL EnableColumn(int nColumn, BOOL bEnable = TRUE);
	virtual BOOL DeleteColumn(int nCol);
	virtual int GetColumnKey();
	virtual void SetColumnKey(int nColumnKey);
	virtual CFont* GetLocalFont();
	virtual int InsertColumn(int nCol, LPCTSTR lpszColumnName, DWORD nWidth, UINT nJustify = LVCFMT_LEFT, BOOL bEnable = TRUE, BOOL bVisible = TRUE);
	virtual void Resize(int cx, int cy);
	virtual void Resize();
	virtual BOOL Create(DWORD wStyle, CRect rect, CWnd* pParent, UINT nID);
	virtual int OnInit();
	static void RegisterNWindow();
	CNListCtrl* GetListCtrl();
	CNHeaderCtrl* GetHeader();
	virtual ~CNWindow();

	// Generated message map functions
protected:
	virtual void NotifyChangeColumnKey(long nColumn);
	void RefreshEditIDs();
	int m_nColumnKey;
	virtual void CreateFont();
	CFont m_font;
	int m_nDefaultHeight;
	CNComboCtrl* m_pControl;
	CSize m_lastSizeListCtrl;
	CNHeaderCtrl m_nHeader;
	CNListCtrl m_nListCtrl;
	//{{AFX_MSG(CNWindow)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NWINDOW_H__DE2017A0_9000_11D2_8726_0040055C08D9__INCLUDED_)
