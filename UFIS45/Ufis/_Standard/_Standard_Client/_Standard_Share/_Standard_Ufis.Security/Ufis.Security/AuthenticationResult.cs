﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Security
{
    /// <summary>
    /// Enumeration for user authentication result.
    /// </summary>
    public enum AuthenticationResult
    {
        Succeeded,
        NeedToInitializeModule,
        InvalidUserName,
        InvalidPassword,
        InvalidPasswordUserDeactivated,
        InvalidApplication,
        ApplicationExpired,
        ApplicationDisabled,
        WorkstationExpired,
        WorkstationDisabled,
        UndefinedProfile,
        UserMustChangePassword,
        UserDisabled,
        UserDeactivated,
        UserExpired,
        OverdueLogin,
        FailedUnknownReason,
        Default
    }   
}
