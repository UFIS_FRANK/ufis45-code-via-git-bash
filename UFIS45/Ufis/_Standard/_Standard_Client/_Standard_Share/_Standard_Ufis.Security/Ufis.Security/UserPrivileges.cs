﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Security
{
    /// <summary>
    /// Represents the list of user's privilege.
    /// </summary>
    public class UserPrivileges
    {
        readonly Dictionary<string, UserAccessRight> _dicUserAccessRight;

        /// <summary>
        /// Add user privilege into this UserPrivileges object.
        /// </summary>
        /// <param name="key">User privilege's key.</param>
        /// <param name="accessRight">User privilege's value.</param>
        public void Add(string key, UserAccessRight accessRight)
        {
            _dicUserAccessRight.Add(key, accessRight);
        }

        /// <summary>
        /// Add user privilege into this UserPrivileges object.
        /// </summary>
        /// <param name="key">User privilege's key.</param>
        /// <param name="accessRightString">User privilege's value.</param>
        public void Add(string key, string accessRightString)
        {
            UserAccessRight accessRight;
            switch (accessRightString)
            {
                case "1":
                    accessRight = UserAccessRight.Activated;
                    break;
                case "0":
                    accessRight = UserAccessRight.Deactivated;
                    break;
                case "-":
                    accessRight = UserAccessRight.Hidden;
                    break;
                default:
                    accessRight = UserAccessRight.Unspecified;
                    break;
            }
            Add(key, accessRight);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key">User privilege's key.</param>
        /// <returns></returns>
        public UserAccessRight this[string key]
        {
            get
            {
                UserAccessRight accessRight;
                if (!_dicUserAccessRight.TryGetValue(key, out accessRight))
                {
                    accessRight = UserAccessRight.Unspecified;
                }
                return accessRight;
            }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Security.Ceda.UserPrivileges class.
        /// </summary>
        public UserPrivileges()
        {
            _dicUserAccessRight = new Dictionary<string, UserAccessRight>();
        }
    }
}
