﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Security
{
    /// <summary>
    /// Enumeration for password validation result.
    /// </summary>
    public enum PasswordValidationResult
    {
        None,
        Succeeded,
        BlankPassword,
        SameOldAndNewPassword,
        InvalidPasswordLength,
        InvalidCapitalLettersCount,
        InvalidSmallLettersCount,
        InvalidNumericCharsCount,
        InvalidSameCharsWithUserName
    }
}
