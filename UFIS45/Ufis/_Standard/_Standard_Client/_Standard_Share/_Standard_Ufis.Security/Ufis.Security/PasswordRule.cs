﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Security
{
    /// <summary>
    /// Represents the rule for user's password.
    /// </summary>
    public class PasswordRule
    {
        /// <summary>
        /// Minimum length of the password.
        /// </summary>
        public int MinLength { get; set; }

        /// <summary>
        /// Minimum number of capital letters in the password.
        /// </summary>
        public int MinCapitalLetters { get; set; }

        /// <summary>
        /// Minimum number of small letters in the password.
        /// </summary>
        public int MinSmallLetters { get; set; }

        /// <summary>
        /// Minimum number of numeric characters in the password.
        /// </summary>
        public int MinNumericChars { get; set; }

        /// <summary>
        /// Maximimum number of identical characters with user name in the password.
        /// </summary>
        public int MaxIdenticalCharsWithUserName { get; set; }
    }
}
