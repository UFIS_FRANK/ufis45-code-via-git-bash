/// ccscedadata.cpp - Provide methods for CEDA services and exchange method
// 
// Description:                                                                   
// Notes: The ccsCedaData calls an external procedures in a DLL via an import library,
// which was created from "IMPLIB yourdll.lib yourdll.dll" and then link it
// the same way as normal .LIB libraries.
//
//
// April 15, 1996
//
// Modification History:
// April 26, 1996   Damkerng    Nearly rewrite this class to make it run correctly,
//                              and follow to the documentation standardization with DOC++.
//
// May 3, 1996      Damkerng    Add a GNU action to the CedaAction method.
//
// May 7, 1996      Seksan      Change LoadData() routine to access internal buffer directly.
//                              This version call ::GetBufferHandle() in UFIS.DLL.
//
// May 12, 1996     Damkerng    Change LoadData() routine to be able to work correctly
//                              even if the buffer is larger than 64K.
//
// May 14, 1996     Damkerng    Optimize GetBufferRecord() for faster data conversion.
//                              The original version uses CString which is very slow (need
//                              many duplicating strings around). The newer version will
//                              use the standard C character pointer (char *). In effect,
//                              GetBufferRecord() takes time only 3/7 compared to the last
//                              version.

#include <stdafx.h>

#include <ufis.h>
#include <ccscedadata.h>
#include <ccsbasicfunc.h> 
#include <ccsdefines.h>
#include <ccsbchandle.h>
#include <CedaAptLocalUtc.h>
#include <fstream.h>
#include <iomanip.h>
#include <process.h>
#include "Excel9.h"

// Used in ccsCedaData::CedaAction()
#define GNU_BUFFER  "                "    // default 10 characters

char pcgDataBuf[2048] = "abcdefghijklmnopqrstuvwABCDEFGHIJKLMNOPQRSTUVWXYZ";


CTimeSpan CCSCedaData::omLocalDiff1 = 0;
CTimeSpan CCSCedaData::omLocalDiff2 = 0;
CTime	  CCSCedaData::omTich = TIMENULL;


char CCSCedaData::pcmTableExt[4] = "XXX";
char CCSCedaData::pcmHomeAirport[4] = "XXX";


char CCSCedaData::pcmApplName[32] = "XXX";
char CCSCedaData::pcmUser[32] = "XXX";
char CCSCedaData::pcmReqId[32] = "XXX";

char CCSCedaData::pcmVersion[32] = "";
char CCSCedaData::pcmInternalBuild[32] = "";
bool CCSCedaData::bmVersCheck = false; 

CString CCSCedaData::omTmpPath = CCSLog::GetTmpPath();


int CCSCedaData::imInstCount = 0;
IBcPrxy* CCSCedaData::smBcProxy = NULL;

#ifdef	AIA_RMS
	CString CCSCedaData::omClientChars = CString("\042\047,\012\015\072\173\175\133\135\174\136\77\50\51\73\100�");
	CString CCSCedaData::omServerChars = CString("\260\261�\264\263\277\223\224\225\226\227\230\231\233\234\235\236\021");
#else
	CString CCSCedaData::omClientChars = CString("\042\047,\012\015\072\173\175\133\135\174\136\77\50\51\73\100");
	CString CCSCedaData::omServerChars = CString("\260\261�\264\263\277\223\224\225\226\227\230\231\233\234\235\236");
#endif
/*
	omClientChars = CString("\042\047");  //\54\012\015\072");  // dezimal: 34,39,44,10,13,58

	omClientChars += CString(",") + CString("\012\015\072");
						    //  {   }   [   ]   |   ^   ? (  )  ;  @
	omClientChars += CString("\173\175\133\135\174\136\77\50\51\73\100");

	omServerChars = CString("\260\261"); //\375\264\263\277");  // dezimal:  176,177,253,180,179,191

	omServerChars += CString("�") + CString("\264\263\277");
							//  {   }   [   ]   |   ^   ?   (   )   ;   @
	omServerChars += CString("\223\224\225\226\227\230\231\233\234\235\236");
*/

CStringArray	*CCSCedaData::pomUfisCedaConfig = NULL;

// list of SQLHDL commands which handles "No data found" as error !
static CString	olSQLCmds = CString(",URT,RT,IRT,RTA,RTB,URF,DRT,DRF,UCD,DCD,SMU,SYS,SA32,SA23,S2O3,SQL,");

/************************************************************ 
 * Function:   GetNextDataItem
 * Parameter:  OUT     char *pcpResult
 *             IN/OUT  char **pcpInput
 *             IN      char *pcpDel    Delimiter
 *             IN      char *pcpDef    Default Result if token is empty
 *             IN      char *pcpTrim   " \0" = trim left with space
 *                                     "\0 " = trim right with space
 *                                     "::"  = trim left and right colon
 *                                     "\0\0" = no trim
 *										"  " = trim blanl left and right
 *             
 * Return Code: >=0          log. number of char. in result string
 *                           (blanks does not count as character )
 *                           pcpinput points behind hit 
 *              -1..-5       <number> argument is NULL
 *                           pcpinput not changed
 *                           empty pcpResult string
 *
 * Result:      *prpResult contains found string
 *           
 * Description: The function picks words from a list seperated
 *              by cpDel. The function manipulates *pcpInput to
 *              point to the next position behind cpDel. The 
 *              function interprets every delimiter as beginning
 *              of a new field. e.g.  ':' as delimiter and 
 *              ":Test:" as input string leads to
 *              ""       first call 
 *              "Test"   second call 
 *              ""       third call 
 *              The function can trim a result on the right or/and
 *              the left side by means of not returning those chars
 *              in th result string.
 *
 *************************************************************/
int GetNextDataItem(char *pcpResult,char **pcpInput,char *pcpDel,char *pcpDef,char *pcpTrim)
{
  int ilRC=0;
  int ilLastChar=0;
  int ilPos=0;
  if      ( pcpResult == NULL )
    ilRC = -1;
  else if ( *pcpInput == NULL )
    ilRC = -2;
  else if (pcpTrim == NULL)
    ilRC = -5;
  else
    {
      pcpResult[0] = '\0';
      
      if ( pcpTrim[0] != '\0')
	{
	  while (**pcpInput == pcpTrim[0])
	    (*pcpInput)++;
	}



      /* Search first Delimiter character */
      while(((**pcpInput) != pcpDel[0]) && ((**pcpInput) != 0) )
	{
	  pcpResult[ilPos] = **pcpInput;

	  if((pcpTrim != '\0' ) && (**pcpInput) != pcpTrim[1]){
	    ilLastChar = ilPos+1 ;
	  }
	  (*pcpInput)++;
	  ilPos++;
	}

      if (pcpTrim != '\0'){
	ilPos = ilLastChar;
      }
      pcpResult[ilPos] = '\0';
      ilRC = ilPos;

      if((ilPos == 0)  && (*pcpDef != '\0'))
	{
	  strcpy(pcpResult,pcpDef);  
	  ilRC = strlen(pcpDef)-1;
	  while((ilRC >= 0 ) && (pcpResult[ilRC] == ' '))
	    ilRC--;
	}

      if ((**pcpInput) == (pcpDel[0]))
	(*pcpInput)++;
    }
  return ilRC;
}/* GetNextDataItem */

/////////////////////////////////////////////////////////////////////////////
// Public methods


CCSCedaData::CCSCedaData(CCSCedaCom *popCommHandler)
{
	imInstCount++;
	pomCommHandler = popCommHandler;
    *pcmTableName = '\0';    // no default table name
    pcmFieldList = NULL;    // no default field list
    pcmSelection = NULL;    // no default selection criteria
    pcmSort = NULL;         // no default sort order
	lmBaseID = 0;		
	if(pomUfisCedaConfig == NULL)
		pomUfisCedaConfig = new CStringArray;


//END RST
}   




CCSCedaData::~CCSCedaData()
{
	imInstCount--;

// TRACE("CCSCedaData::~CCSCedaData\n");
	omFieldMap.RemoveAll();
	omRecInfo.DeleteAll();
	if(pomUfisCedaConfig != NULL && imInstCount <= 0)
		delete pomUfisCedaConfig;


}


// Short form of CedaAction()
//
bool CCSCedaData::CedaAction(char *pcpAction,
    char *pcpSelection, char *pcpSort,
    char *pcpData,char *pcpDest /*"BUF1"*/,bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{                           
		return CedaAction(pcpAction,
			CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,
			(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,
			(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,
			pcpData,pcpDest,bpIsWriteAction, lpID);
}

void CCSCedaData::SetCommHandler(CCSCedaCom *popCommHandler)
{ 
	pomCommHandler = popCommHandler;
}


void CCSCedaData::SetTableExt(CString opTableExt)
{
	CString olTmp(pcmTableName);
	olTmp = olTmp.Left(3) + opTableExt;
    strcpy(pcmTableName,olTmp);
}

void CCSCedaData::SetHomeAirport(CString opHomeAirport)
{
    strcpy(pcmHomeAirport,opHomeAirport);
}



bool CCSCedaData::CedaRereadBc(struct BcStruct *prlBcData)
{
    static char req_id[22];     // Is assigned by the application
    static char dest1[12];      // Normally CEDA
    static char dest2[12];      // Result receiver (normally empty which means "back to me")
    static char cmd[12];        // Command (RTA, URT, IRT, DRT, ...)
    static char object[512];    // Table Name (multiple table are allowed)
    static char seq[512];       // Sort Clause    
    static char tws[36];        // Time Window Start 
    static char twe[36];        // Time Window end
    static char *selection;			// Selection criterium, normally a WHERE clause
    static char *fields;				// DB field, normally database fields
    static char *pclData;				// Data
    static char data_dest[12];  // RETURN or BUF1 .. BUF4
	static char pclBuf[70000];   // Data ( blIsWriteAction)
	bool blRc = true;


	strcpy(dest2, pcmReqId);
	strcpy(dest1, pcmUser);

	strcpy(cmd, "RBC");
    strcpy(object, "");
    strcpy(seq, "");
    strcpy(twe, "");

	omLastErrorMessage.Empty();
	sprintf(prlBcData->Twe ,"%s,%s,%s", pcmHomeAirport, pcmTableExt, pcmApplName);
	strcpy(prlBcData->Dest2, pcmReqId);
	strcpy(prlBcData->Dest1, pcmUser);
	strcpy(prlBcData->Tws, "0");

	imLastReturnCode = ::CallCeda(prlBcData->ReqId, prlBcData->Dest1, prlBcData->Dest2, prlBcData->Cmd, 
		prlBcData->Object, prlBcData->Seq, prlBcData->Tws, prlBcData->Twe,
        prlBcData->Selection, prlBcData->Fields, prlBcData->Data, "RETURN");
	
	if (imLastReturnCode != RC_SUCCESS)
	{
#ifdef _DEBUG
		if (imLastReturnCode == RC_CEDA_FAIL)
		{
			// Note: This macro is available only in the debug version of MFC.
			TRACE("CCSCedaData: RC=%d, %s Cmd: <%s> <%s>\n",imLastReturnCode,omLastErrorMessage,
				cmd,object);
		}
#endif
		blRc = false;
		return false;
	}

	omReqId = CString(req_id);

    return true;
}





// Long form of CedaAction()
//
bool CCSCedaData::CedaAction(char *pcpAction,
    char *pcpTableName, char *pcpFieldList,
    char *pcpSelection, char *pcpSort,
    char *pcpData, char *pcpDest,bool bpIsWriteAction /* false */, long lpID /* = 0*/, CCSPtrArray<RecordSet> *pomData, int ipFieldCount 
	, CString add )
{
    static char req_id[22];     // Is assigned by the application
    static char dest1[12];      // Normally CEDA
    static char dest2[12];      // Result receiver (normally empty which means "back to me")
    static char cmd[12];        // Command (RTA, URT, IRT, DRT, ...)
    static char object[512];    // Table Name (multiple table are allowed)
    static char seq[512];       // Sort Clause    
    static char tws[36];        // Time Window Start 
    char twe[36];        // Time Window end
    static char selection[10000];			// Selection criterium, normally a WHERE clause
    static char fields[2048];				// DB field, normally database fields
    static char *pclData;				// Data
    static char data_dest[12];  // RETURN or BUF1 .. BUF4
	static char pclBuf[70000];   // Data ( blIsWriteAction)
	bool blRc = true;
    bool blIsReadAction;
    bool blIsGetUrnoAction;
	bool blIsGetManyUrnoAction;
	bool blIsRBC;
	bool blIsWriteAction;

	char pclTableName[64];


	strcpy(pclTableName, pcpTableName);

	if(strlen(pclTableName) == 3)
		strcat(pclTableName, pcmTableExt);

	if(bmVersCheck)
		sprintf(twe ,"%s,%s,%s,%s,%s", pcmHomeAirport, pcmTableExt, pcmApplName, pcmVersion, pcmInternalBuild);
	else
		sprintf(twe ,"%s,%s,%s", pcmHomeAirport, pcmTableExt, pcmApplName);

	blIsWriteAction = bpIsWriteAction;
	if (blIsWriteAction == true)
	{
		blIsReadAction = false;
	}
	
    blIsReadAction = ((CString(pcpAction) == "RTA") || (CString(pcpAction) == "RT") || (CString(pcpAction) == "GFR"));
	blIsGetUrnoAction = (CString(pcpAction) == "GNU" || CString(pcpAction) == "GMU");
	blIsGetManyUrnoAction = CString(pcpAction) == "GMU";
	blIsRBC = (strcmp("RBC",pcpAction) == 0);
	CString olCommand;
	olCommand.Format("|%s|",pcpAction);
	blIsWriteAction = (strstr("|IRT|URT|IBT|UBT|ISF|UFR|CGM|",olCommand) != NULL);

	if ((blIsWriteAction == false) && (blIsGetUrnoAction == false) && (blIsRBC == false))
	{
		blIsReadAction = true;
	}

    // Do not allow the user to use the local buffer on reading.
    // This is because the static data buffer is very small (limited to 2048 bytes).
    //
    if (blIsReadAction && CString(pcpDest) == "RETURN")
    {
        return false;
    }

	if(	CCSCedaData::bmUseNETIN == false)
	{
		bool blRet2 = true;
		char pclOldTable[32]="";
		char plcOldFields[2048]="";
		strcpy(pclOldTable, pcmTableName);
		bool blFldEmpty = false;


		myFieldList = CString(CCSCedaData::pcmFieldList);

		if(strcmp(pcpTableName, "") != 0)
		{
			strcpy(pcmTableName, pclTableName);
		}
		if(strcmp(pcmTableName, "") == 0)
		{
			strcpy(pcmTableName, pclTableName);
		}
		if((pcpFieldList != NULL && strcmp(pcpFieldList, "") != 0) || (strcmp(myFieldList, "") == 0))
		{
			myFieldList = CString(pcpFieldList);
		}

		blRet2 = CedaAction2(pcpAction, pcpSelection, pcpSort, pcpData, pcpDest, bpIsWriteAction, lpID, true, tws, twe);
		strcpy(pcmTableName, pclOldTable);
		if (blRet2 == true)
		{
			if (blIsReadAction)
			{
				// check for SQLHDL commands which handles "No data found" as error !
				CString olCmd;
				olCmd.Format(",%s,",pcpAction);
				if (olSQLCmds.Find(olCmd) != -1)
				{
					if (this->GetBufferSize() == 0)
						blRet2 = false;
				}
			}
			else	// eleminate "no data found" message from CDRHDL on URT
			{
				omLastErrorMessage.Empty();
			}
		}
		return blRet2;
	}

	if (pcpData == NULL)
	{
		pcpData = pcgDataBuf;
	}


    // To allow the current version of UFIS.DLL work correctly, parameters given to
    // the ::CallCeda() need to be in static data area. Hence, we need to copy all
    // and everything to these static buffer before ::CallCeda().
    //
    // Default sort order is none. Default selection criteria is all record. Default field
    // list is all fields. If the action is "RTA" (request table), the data destination will
    // be forced to be the "RETURN" which means the static data buffer.
    //
    // For the GNU (Get Unique Record Number) action, the buffer need to be 10 characters
    // in length to allow the UFIS.DLL copy URNO back to the "data" buffer.
    //



	strcpy(dest2,pcmReqId);
	strcpy(dest1,pcmUser);


	if(lpID == 0L)
		ltoa(lmBaseID, tws, 10);
	else
		ltoa(lpID, tws, 10);
    
	strcpy(cmd, pcpAction);
    strcpy(object, pclTableName);
    strcpy(seq, (pcpSort == NULL)? "": pcpSort);
    //strcpy(twe, "");
    strcpy(selection,(pcpSelection == NULL ? "": pcpSelection));
	if (!add.IsEmpty()) {
		strcpy(fields,add);
		strcat(fields, (pcpFieldList == NULL ? "*": pcpFieldList));
	}
	else {
    strcpy(fields, (pcpFieldList == NULL ? "*": pcpFieldList));
	}
    pclData = (blIsGetUrnoAction ? GNU_BUFFER : pcpData);
	if(blIsGetManyUrnoAction == TRUE)
	{
		pclData = pcpData;
	}
    strcpy(data_dest, (blIsReadAction)? pcpDest: "RETURN");

	if (blIsWriteAction)
	{
		if (pclBuf == NULL)
		{
			omLastErrorMessage = "Speicher nicht ausreichend ";
			return false;
		}
		CheckDataArea(pclBuf,pclData);
	}

	omLastErrorMessage.Empty();

	if(!blIsWriteAction && !blIsGetManyUrnoAction && !blIsGetUrnoAction)
	{
		if(pclData != pcgDataBuf)
		{
			if(strlen(pclData) < 1024)
			{
				CString olTmp(pclData);
				pclData = pcgDataBuf;
				strcpy(pclData, olTmp);
			}
		}

	}


	// check on GPR command
	if (strcmp(pcpAction,"GPR") == 0)
	{
		if (strstr(fields,"WKST") == NULL)
		{
			char pclWksName[256];
			if (::GetRealWorkstationName(pclWksName) == RC_SUCCESS)
			{
				strcat(fields, ",WKST");
				strcat(pclData,",");
				strcat(pclData,pclWksName);
			}

		}
	}


	imLastReturnCode = ::CallCeda(req_id, dest1, dest2, cmd, object, seq, tws, twe,
        selection, fields, (blIsWriteAction)? pclBuf: pclData, data_dest);


    if (imLastReturnCode  != RC_SUCCESS)
	{
		omLastErrorMessage = CString((blIsWriteAction)? pclBuf: pclData);
		if (imLastReturnCode == RC_CEDA_FAIL)
		{
			TRACE("CCSCedaData: RC=%d, %s Cmd: <%s> <%s>\n",imLastReturnCode,omLastErrorMessage,
				cmd,object);
		}
		blRc = false;
		return false;
	}

		omReqId = CString(req_id);
		strcpy(pcmExtSelection, selection);
		strcpy(pcmExtFields, fields);


    // On success, if the action is RTA (read record from table), retrieve data from the
    // internal buffer of UFIS.DLL to create an array of comma-separated string into the
    // CStringArray "omDataBuf".
    //
    if (blIsReadAction)
	{
        if ((imLastReturnCode=LoadData(data_dest, pomData, ipFieldCount )) != RC_SUCCESS) // error?
        {
            return false;
        }
	}
    // On success, if the action is GNU (get URNO -- Unique Record Number), copy the URNO
    // to given "omDataBuf" buffer. Please be warned that the "pcpData" cannot be an
    // empty string. It needed to contain some spaces (let's say 10 characters) to let
    // the UFIS.DLL know the length of the buffer to store the URNO back. However, the
    // CCSCedaData will handle this internally.
    //
    if (blIsGetUrnoAction)
    {
        omDataBuf.RemoveAll();
        omDataBuf.Add(pclData);
    }
	else
	{
		if (blIsRBC == true)
		{
			strcpy(pcpAction,cmd);
			strcpy(pclTableName,object);
			strcpy(pcpSelection,selection);
			strcpy(pcpFieldList,fields);
			strcpy(pcpData,pclData);
		}
	}

    return true;
}

bool CCSCedaData::GetBufferLine(int ipLineNo, CString &pcpListOfData)
{
	if(	CCSCedaData::bmUseNETIN == false)
	{
		return GetBufferLine2(ipLineNo, pcpListOfData);
	}	
    // Check if the line number is out-of-bound?
    if (!(0 <= ipLineNo && ipLineNo < omDataBuf.GetSize()))
    {
        return false;
    }

    pcpListOfData = omDataBuf.GetAt(ipLineNo);
    return true;
}

CString CCSCedaData::LastError(void)
{
    static char *szMessage[] = {
        "RC_SUCCESS: Everthing OK",
        "RC_FAIL: General serious error",
        "RC_COMM_FAIL: WINSOCK.DLL error",
        "RC_INIT_FAIL: Open the log file",
        "RC_CEDA_FAIL: CEDA reports an error",
        "RC_SHUTDOWN: CEDA reports shutdown",
        "RC_ALREADY_INIT: InitComm called up for a second line",
        "RC_NOT_FOUND: Data not found (More than 7 applications are running)",
        "RC_DATA_CUT: Data incomplete",
        "RC_TIMEOUT: CEDA reports a time-out"
    };
    static const int nMessages = sizeof(szMessage)/sizeof(szMessage[0]);

    if (!(-nMessages <= imLastReturnCode && imLastReturnCode <= 0))
        return CString("Unknown error");

	if (omLastErrorMessage.IsEmpty() == false)
	{
		return omLastErrorMessage;
	}
	else
	{
		// notice that "imLastReturnCode" is always negative
		return CString(szMessage[-imLastReturnCode]);
	}

}



bool CCSCedaData::GetItem(int ipItemNo,char *pcpItemList,char *pcpItemData)
{

	int ilActualItem = 1;
	int ilCount = 0;

	bool blFound = false;

	for(ilActualItem = 1, ilCount = 0; 
		pcpItemList[ilCount] != '\0' && ilActualItem <= ipItemNo;
					ilCount++)
	{
		if (ilActualItem == ipItemNo)
		{
			blFound = true;
			*pcpItemData = pcpItemList[ilCount];
			if (*pcpItemData == ',')
			{
				break;
			}
			pcpItemData++;
		}
		if (pcpItemList[ilCount] == ',')
		{
			ilActualItem++;
		}
	}

	*pcpItemData = '\0';

	return blFound;
}


bool CCSCedaData::GetRecordFromItemList(void *pvpDataStruct,
							char *pcpItemList,char *pcpDataList)
{
	
	return GetRecordFromItemList(&omRecInfo,pvpDataStruct,
						pcpItemList,pcpDataList);

}

bool CCSCedaData::GetRecordFromItemList(CCSPtrArray<CEDARECINFO> *pomRecInfo,
		void *pvpDataStruct,char *pcpItemList,char *pcpDataList)
{   
	char pclItemName[12];
	char pclItemText[2000];

	bool olRc = true;
    // Start conversion, field-by-field
    int ilMaxItems = pomRecInfo->GetSize();

	int ilItemNo = 1;

    while (GetItem(ilItemNo,pcpItemList, pclItemName))
    {
		olRc = GetItem(ilItemNo,pcpDataList,pclItemText);
		int ilActualFieldNo = -1;
	    for (int ilFieldNo = 0; ilFieldNo < ilMaxItems; ilFieldNo++)
		{
			CEDARECINFO rlRecInfo;
			memcpy(&rlRecInfo,&(*pomRecInfo)[ilFieldNo],sizeof(rlRecInfo));
			if (stricmp(pclItemName,(*pomRecInfo)[ilFieldNo].Name) == 0)
			{
				ilActualFieldNo = ilFieldNo;
				break;
			}
		}
		if (ilActualFieldNo > -1)
		{

        // Convert data from text-line to the corresponding field in record
			int type = (*pomRecInfo)[ilActualFieldNo].Type;
			void *p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[ilActualFieldNo].Offset);
			int length = (*pomRecInfo)[ilActualFieldNo].Length;
			int flags = (*pomRecInfo)[ilActualFieldNo].Flags;

			switch (type) 
			{
			case CEDACHAR:
				StoreChar(pclItemText, (char *)p, length, flags);
				break;
			case CEDAINT:
				StoreInt(pclItemText, (int *)p);
				break;
			case CEDALONG:
				StoreLong(pclItemText, (long *)p);
				break;
			case CEDADOUBLE:
				StoreDouble(pclItemText, (double *)p);
				break;
			case CEDADATE:
				StoreDate(pclItemText, (CTime *)p);
				break;
			case CEDAOLEDATE:
				StoreOleDate(pclItemText, (COleDateTime *)p);
				break;

			default:    // invalid field type
				return false;
			}
		}
		ilItemNo++;
	}

    return true;
}



/////////////////////////////////////////////////////////////////////////////
// Protected methods

bool CCSCedaData::GetBufferRecord(int ipLineNo, void *pvpDataStruct)
{   
	if(	CCSCedaData::bmUseNETIN == true)
	{
	    return GetBufferRecord(&omRecInfo,ipLineNo,pvpDataStruct);
	}
	else
	{
		return GetBufferRecord2(&omRecInfo,ipLineNo,pvpDataStruct);
	}
}


bool CCSCedaData::GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, int ipLineNo, void *pvpDataStruct)
{   
	if(	CCSCedaData::bmUseNETIN == false)
	{
		return GetBufferRecord2(pomRecInfo, ipLineNo, pvpDataStruct);
	}
    // Check if the line number is out-of-bound?
    if (!(0 <= ipLineNo && ipLineNo < omDataBuf.GetSize()))
    {
        return false;
    }
	return GetBufferRecord(pomRecInfo, omDataBuf[ipLineNo],pvpDataStruct);
}


/////////////////////////////////////////////////////////////////////////////
// Protected methods

bool CCSCedaData::GetFirstBufferRecord(void *pvpDataStruct)
{   
	bool blRc;

	if(	CCSCedaData::bmUseNETIN == true)
	{
		
		blRc = GetBufferRecord(&omRecInfo, 0,pvpDataStruct);
		if (blRc == true)
		{
			omDataBuf.RemoveAt(0);
		}
	}
	else
	{
		blRc = GetBufferRecord2(&omRecInfo, 0, pvpDataStruct);
		if (blRc == true)
		{
			CCSCedaData::pomFastCeda->omData.RemoveAt(0);
		}
	}
	return blRc;
}



bool CCSCedaData::GetFirstBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo,void *pvpDataStruct)
{   
	bool blRc;

	if(	CCSCedaData::bmUseNETIN == true)
	{
		
		blRc = GetBufferRecord(pomRecInfo, 0,pvpDataStruct);
		if (blRc == true)
		{
			omDataBuf.RemoveAt(0);
		}
	}
	else
	{
		blRc = GetBufferRecord2(pomRecInfo, 0, pvpDataStruct);
		if (blRc == true)
		{
			CCSCedaData::pomFastCeda->omData.RemoveAt(0);
		}
	}
	return blRc;
}





bool CCSCedaData::FillRecord(void *pvpDataStruct, CString &olFieldList, CString &olDataList)
{   
	omDataBuf.RemoveAll();
	omDataBuf.Add(olDataList);
    return GetBufferRecord(&omRecInfo,0,pvpDataStruct, olFieldList);
}




bool CCSCedaData::GetBufferRecord(int ipLineNo, void *pvpDataStruct, CString &olFieldList)
{   
	if(	CCSCedaData::bmUseNETIN == true)
	{
		return GetBufferRecord(&omRecInfo,ipLineNo,pvpDataStruct, olFieldList);
	}
	else
	{
		return GetBufferRecord2(&omRecInfo,ipLineNo,pvpDataStruct, olFieldList);
	}
}

bool CCSCedaData::GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, CString record, void *pvpDataStruct)
{
	if(	CCSCedaData::bmUseNETIN == true)
	{
		return GetBufferRecord(pomRecInfo,record,pvpDataStruct, CString( pcmFieldList));
	}
	else
	{
		return GetBufferRecord2(pomRecInfo,record,pvpDataStruct, CString(pcmFieldList));
	}
}
bool CCSCedaData::GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, CString record, void *pvpDataStruct,  CString &olFieldList)
{   
    int nfields = pomRecInfo->GetSize();
    int nbytes = record.GetLength();

	if(	CCSCedaData::bmUseNETIN == false)
	{
		return GetBufferRecord2(pomRecInfo, record, pvpDataStruct, CString(olFieldList));
	}

	if(omFieldMap.GetCount() == 0)
	{
		for(int i = pomRecInfo->GetSize() - 1; i >= 0; i--)
		{
			omFieldMap.SetAt((LPCSTR)(*pomRecInfo)[i].Name, (void*)&(*pomRecInfo)[i]);			
		}
	}

	int ilCountField;
	int ilCountData;
	CEDARECINFO *prlRecInfo;
	CStringArray olFieldArray;
	CStringArray olDataArray;

	ilCountField = 	ExtractItemList(CString(olFieldList), &olFieldArray, ',');
	ilCountData = 	ExtractItemList(record, &olDataArray, ',');

	for(int ilLc = 0; ilLc < ilCountField && ilLc < ilCountData; ilLc++)
	{
		CString olTmpName = olFieldArray[ilLc];
		olTmpName.MakeUpper();

		//if(omFieldMap.Lookup((LPCSTR)olFieldArray[ilLc], (void*&)prlRecInfo) == TRUE)
		if(omFieldMap.Lookup((LPCSTR)olTmpName, (void*&)prlRecInfo) == TRUE)
		{
			// Convert data from text-line to the corresponding field in record
			int type = prlRecInfo->Type;
			void *p = (void *)((char *)pvpDataStruct + prlRecInfo->Offset);
			int length = prlRecInfo->Length;
			int flags  = prlRecInfo->Flags;

			switch (type) {
			case CEDACHAR:
				StoreChar(olDataArray[ilLc], (char *)p, length, flags);
				break;
			case CEDAINT:
				StoreInt(olDataArray[ilLc], (int *)p);
				break;
			case CEDALONG:
				StoreLong(olDataArray[ilLc], (long *)p);
				break;
			case CEDADOUBLE:
				StoreDouble(olDataArray[ilLc], (double *)p);
				break;
			case CEDADATE:
				StoreDate(olDataArray[ilLc], (CTime *)p);
				break;
			case CEDAOLEDATE:
				StoreOleDate(olDataArray[ilLc], (COleDateTime *)p);
				break;

			default:    // invalid field type
				return false;
			}
		}
    }

    return true;
}


bool CCSCedaData::GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, int ipLineNo, void *pvpDataStruct, CString &olFieldList)
{   
	if(	CCSCedaData::bmUseNETIN == false)
	{
		return GetBufferRecord2(pomRecInfo, ipLineNo, pvpDataStruct, olFieldList);
	}	
    // Check if the line number is out-of-bound?
    if (!(0 <= ipLineNo && ipLineNo < omDataBuf.GetSize()))
    {
        return false;
    }
	return GetBufferRecord(pomRecInfo,omDataBuf[ipLineNo],pvpDataStruct, olFieldList);

}



bool CCSCedaData::MakeCedaData(CString &pcpListOfData, void *pvpDataStruct)
{
	return MakeCedaData(&omRecInfo,pcpListOfData,pvpDataStruct);
}

bool CCSCedaData::MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,
	CString &pcpListOfData, void *pvpDataStruct)
{
    pcpListOfData = "";

	CString olFieldData;
	CString olField;

	CStringArray olFields;
	int nfieldlist = ::ExtractItemList(pcmFieldList,&olFields,',');
	
    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();

    for (int i = 0; i < nfieldlist; i++)
    {
		olField = olFields[i];

		for (int fieldno = 0; fieldno < nfields; fieldno++)
		{
			if(olField ==  CString((*pomRecInfo)[fieldno].Name) )
			{
				CString s;
				char buf[33];   // ltoa() need buffer 33 bytes
				int type = (*pomRecInfo)[fieldno].Type;
				void *p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[fieldno].Offset);
				int length = (*pomRecInfo)[fieldno].Length;

				switch (type) {
				case CEDACHAR:
					s = (length == 0)? CString(" "): CString((char *)p).Left(length);
					MakeCedaString(s);
					olFieldData = s + ",";
					break;
				case CEDAINT:
					olFieldData = CString(itoa(*(int *)p, buf, 10)) + ",";
					break;
				case CEDALONG:
					olFieldData = CString(ltoa(*(long *)p, buf, 10)) + ",";
					break;
				case CEDADATE:  // format: YYYYMMDDHHMMSS
					olFieldData = ((CTime *)p)->Format("%Y%m%d%H%M%S") + ",";
					break;
				case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
					if(((COleDateTime *)p)->GetStatus() == COleDateTime::valid)
					{
						olFieldData = ((COleDateTime *)p)->Format("%Y%m%d%H%M%S") + ",";
					}
					else
					{
						olFieldData = " ,"; 
					}
					break;
				case CEDADOUBLE:
					char pclBuf[128];

					sprintf(pclBuf,"%lf",(double *)p);
					olFieldData = CString(pclBuf) + ",";
					break;
				default:    // invalid field type
					return false;
				}
				pcpListOfData += olFieldData;
				break;
			}
		}
	}

	if(pcpListOfData.GetLength() > 0)
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);

    return true;
}



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// RST

bool CCSCedaData::MakeCedaData(CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct)
{
	return MakeCedaData(&omRecInfo,pcpListOfData, pcpFieldList, pvpSaveDataStruct, pvpChangedDataStruct);
}



bool CCSCedaData::MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,
	CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct)
{
    pcpListOfData = "";
	char pclBuf[128];
    char buf[33];   // ltoa() need buffer 33 bytes
    CString sSave;
    CString sChanged;
    CString sOrg;
    CString pclFieldList;
	int ilFieldListPos = 0;
	int ilFieldListLength = 0;
	int ilTmpPos = 0;
	int ilLc = 0;
	CString olField;

    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();


	CStringArray olFields;
	int nfieldlist = ::ExtractItemList(pcmFieldList,&olFields,',');
	

    for (int i = 0; i < nfieldlist; i++)
    {
		olField = olFields[i];


		for (int fieldno = 0; fieldno < nfields; fieldno++)
		{
			if(olField ==  CString((*pomRecInfo)[fieldno].Name) )
			{

				int type = (*pomRecInfo)[fieldno].Type;

				void *pSave    = (void *)((char *)pvpSaveDataStruct + (*pomRecInfo)[fieldno].Offset);
				void *pChanged = (void *)((char *)pvpChangedDataStruct + (*pomRecInfo)[fieldno].Offset);

				int length = (*pomRecInfo)[fieldno].Length;

				switch (type) 
				{
				case CEDACHAR:
					{
						sSave = (length == 0)? CString(" "): CString((char *)pSave).Left(length);
						MakeCedaString(sSave);
						sChanged = (length == 0)? CString(" "): CString((char *)pChanged).Left(length);
						MakeCedaString(sChanged);
					}
					break;
				case CEDAINT:
					{
						sSave	 = CString(itoa(*(int *)pSave, buf, 10));
						sChanged = CString(itoa(*(int *)pChanged, buf, 10));
					}
					break;
				case CEDALONG:
					{
						sSave	 = CString(ltoa(*(long *)pSave, buf, 10));
						sChanged = CString(ltoa(*(long *)pChanged, buf, 10));
					}
					break;
				case CEDADATE:  // format: YYYYMMDDHHMMSS
					{
						sSave = ((CTime *)pSave)->Format("%Y%m%d%H%M00");
						sChanged = ((CTime *)pChanged)->Format("%Y%m%d%H%M00");
						sOrg = ((CTime *)pChanged)->Format("%Y%m%d%H%M%S");
					}
					break;
				case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
					{
						sSave = ((COleDateTime *)pSave)->Format("%Y%m%d%H%M%S");
						sChanged = ((COleDateTime *)pChanged)->Format("%Y%m%d%H%M%S");
					}
					break;
				case CEDADOUBLE:
					{
						sprintf(pclBuf,"%.2f",(double *)pSave);
						sSave = CString(pclBuf);
						sprintf(pclBuf,"%.2f",(double *)pChanged);
						sChanged = CString(pclBuf);
					}
					break;
				default:    // invalid field type
					return false;
				}

				//ilTmpPos = ilFieldListPos;
				
				//ilFieldListLength = pcpFieldList.GetLength();

				//for( ilFieldListPos++ ; ((ilFieldListPos < ilFieldListLength)  && (pcpFieldList[ilFieldListPos] != ',')); ilFieldListPos++);

				if(sChanged != sSave)
				{
					if(type == CEDADATE)
						pcpListOfData += sOrg + ",";
					else
						pcpListOfData += sChanged + ",";

					//if( (ilFieldListPos - ilTmpPos > 0) && (pcpFieldList.GetLength() > ilTmpPos + ilFieldListPos - ilTmpPos - 1) )
					//	pclFieldList += pcpFieldList.Mid(ilTmpPos, ilFieldListPos - ilTmpPos);
					pclFieldList += olField + CString(",");
				}
				break;
			}

		}
	}



	if(!pclFieldList.IsEmpty())
	{
		if(pclFieldList[0] == ',')
			pclFieldList.SetAt(0,' ');
		if(pclFieldList[pclFieldList.GetLength()-1] == ',')
			pclFieldList.SetAt(pclFieldList.GetLength()-1,' ');
		pclFieldList.TrimRight();
		pclFieldList.TrimLeft();
	}
	pcpFieldList = pclFieldList;
	if(!pcpListOfData.IsEmpty())
	{
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);
	}
    return true;
}


void CCSCedaData::MakeCedaString(CString &ropText)
{
	int ilIndex;
	for (int ilLc = 0; ilLc < ropText.GetLength(); ilLc++)
	{
		if ((ilIndex = omClientChars.Find(ropText.GetAt(ilLc))) > -1)
		{
			ropText.SetAt(ilLc,omServerChars[ilIndex]);
		}
	}
}

void CCSCedaData::MakeClientString(CString &ropText)
{
	for (int ilLc = 0; ilLc < ropText.GetLength(); ilLc++)
	{
		int ilIndex;
		if ((ilIndex = omServerChars.Find(ropText.GetAt(ilLc))) > -1)
		{
			ropText.SetAt(ilLc,omClientChars[ilIndex]);
		}
	}
}

void CCSCedaData::MakeClientString(char *pclText)
{
	int ilIndex;

	for (unsigned int ilLc = 0; ilLc < strlen(pclText); ilLc++)
	{
		if ((ilIndex = omServerChars.Find(pclText[ilLc])) > -1)
		{
			pclText[ilLc] = omClientChars.GetAt(ilIndex);
		}
	}
}

// Load data from UFIS.DLL (in "data_dest") to "omDataBuf", returns code from UFIS.DLL
int CCSCedaData::LoadData(char *data_dest, CCSPtrArray<RecordSet> *pomData, int ipFieldCount )
{
    int ilRc;
    HGLOBAL handle;
    char  *record,  *p, *lastpos;
	int ilIndex;
	RecordSet *prlRecord;


    // Get the handle of the internal data buffer of UFIS.DLL
    if ((ilRc = ::GetBufferHandle(data_dest, &handle)) != RC_SUCCESS)
        return ilRc;

    // Scan through the internal data buffer, record-by-record.
    // Please notice that the expression "p = (char huge *)(record)" is necessary,
    // since we need to access buffer which may be larger than 64K in 16-bit Windows.
    //
    omDataBuf.RemoveAll();

	if(	pomData == NULL )
	{
		CString olRecord;
		for (record = (char  *)GlobalLock(handle); *record != '\0'; record = p)
		{
			for (p = record; *p != '\0' && *p != '\r' && *p != '\n'; p++)
				;
			olRecord = CString(record, (int)(p-record));
			// MWO: This has to be removed !!
			// if somebody changes this part of the
			// please contact mwo to avoid further confusion!!!
			///MakeClientString(olRecord);
			omDataBuf.Add(olRecord);
    
			p += (*p == '\r')? 1: 0;    // skip '\n' at the end-of-line
			p += (*p == '\n')? 1: 0;    // skip '\n' at the end-of-line

		}
	}
	else
	{
		ilIndex = 0;
		p = lastpos = (char  *)GlobalLock(handle); 

		if( *p != '\0' )
		{
			prlRecord = new RecordSet(ipFieldCount);
			pomData->Add(prlRecord);

			for (; *p != '\0'; p++)
			{
				if((*p == ',') || (*p == '\r') || (*p == '\n'))
				{
					prlRecord->Values[ilIndex] =  CString(lastpos, (int)(p-lastpos));
					prlRecord->Values[ilIndex].TrimRight();
					// MWO: This has to be removed !!
					// if somebody changes this part of the
					// please contact mwo to avoid further confusion!!!
					MakeClientString(prlRecord->Values[ilIndex]);
					ilIndex++;

					if((*p == '\r') || (*p == '\n'))
					{
						prlRecord = new RecordSet(ipFieldCount);
						pomData->Add(prlRecord);
						ilIndex = 0;
					}
					p++;
					p += (*p == '\r')? 1: 0;    // skip '\n' at the end-of-line
					p += (*p == '\n')? 1: 0;    // skip '\n' at the end-of-line
					lastpos = p;
				}
			}
			if( lastpos != p)
			{
				prlRecord->Values[ilIndex] =  CString(lastpos, (int)(p-lastpos));
				prlRecord->Values[ilIndex].TrimRight();
				// MWO: This has to be removed !!
				// if somebody changes this part of the
				// please contact mwo to avoid further confusion!!!
				MakeClientString(prlRecord->Values[ilIndex]);
			}
		}

	}

    // Free the locked global memory, returns RC_SUCCESS       
    GlobalUnlock(handle);
    return ilRc;
}

// Convert CString to a character field
void CCSCedaData::StoreChar(const char *field, char *s, int n, int flags)
{
    strncpy(s, field, n-1);     // copy string (not more than given length)
    s[n-1] = '\0';

    if (flags & TOTRIMLEFT)
        TrimLeft(s);
    if (flags & TOTRIMRIGHT)
        TrimRight(s);

	MakeClientString((char *)s);
}

// Convert CString to an integer field
void CCSCedaData::StoreInt(const char *field, int *ip)
{
    *ip = atoi(field);
}

// Convert CString to a long integer field
void CCSCedaData::StoreLong(const char *field, long *lp)
{
    *lp = atol(field);
}

// Convert CString to a double field
void CCSCedaData::StoreDouble(const char *field, double *lp)
{
	char *p;

	while((p=strchr(field,'\262')) != NULL)    
	 {                                                
	   *p = '.';                                      
	 }                                                
    *lp = atof(field);
}

// Convert CString (in format YYYYMMDDHHMMSS) to a CTime field
// Warning: Since we use CTime as our date field in C struct, our date will be valid
// only between midnight January 1, 1970 and midnight February 5, 2036.
//
void CCSCedaData::StoreDate(const char *field, CTime *pTime)
{
	int year, month, day, hour = 0, min = 0, sec = 0;
	extern int _daylight;

    if (sscanf(field, "%4d %2d %2d %2d %2d %2d", &year, &month, &day, &hour, &min, &sec) < 3)
		*pTime = CTime(CTime((time_t)-1));
	else
	{
		struct tm atm;
		atm.tm_sec = sec;
		atm.tm_min = min;
		atm.tm_hour = hour;
		atm.tm_mday = day;
		atm.tm_mon = month - 1;     // tm_mon is 0 based
		atm.tm_year = year - 1900;  // tm_year is 1900 based
		atm.tm_isdst = -1; _daylight;
		*pTime = CTime(CTime(mktime(&atm)));
	}
}

void CCSCedaData::StoreOleDate(const char *field, COleDateTime *pTime)
{
	int year, month, day, hour = 0, min = 0, sec = 0;
	extern int _daylight;

    if (sscanf(field, "%4d %2d %2d %2d %2d %2d", &year, &month, &day, &hour, &min, &sec) < 3)
	{
		*pTime = COleDateTime();//COleDateTime(COleDateTime((time_t)-1));
		pTime->SetStatus(COleDateTime::null);
	}
	else
	{
		*pTime = COleDateTime(year, month, day, hour, min, sec);
	}
}

/////////////////////////////////////////////////////////////////////////////
// String routines used in data conversion

void CCSCedaData::TrimLeft(char *s)
{
	CString olTmp(s);
	
	olTmp.TrimLeft();
	
	strcpy( s , olTmp );
	
}

void CCSCedaData::TrimRight(char *s)
{
	CString olTmp(s);
	
	olTmp.TrimRight();
	
	strcpy( s , olTmp );
}

void CCSCedaData::CheckDataArea(char *pcpDest,char *pcpSrc)
{
        char *pclSrc,*pclDest;
        char clLast = ',';
        int ilFoundNull = false;

        pclSrc = pcpSrc;
        pclDest = pcpDest;

		if (*pclSrc == '\0')
		{
			strcpy(pcpDest," ");
		}
		else
		{
			while(*pclSrc != '\0')
			{
					if ((*pclSrc != ',') || (clLast != ','))
					{
							*pclDest = *pclSrc;
							clLast = *pclSrc;
							pclDest++; pclSrc++;
					}
					else
					{
							if (clLast == ',')
							{
									*pclDest = ' ';
									clLast = ' ';
									pclDest++;
									ilFoundNull = true;
							}
					}
			}
		}
	
       *pclDest = '\0';
	   if (clLast == ',')
	   {
		   strcat(pclDest," ");
	   }
}

long CCSCedaData::GetUrnoFromSelection(CString opSelection)
{
	// bch 18.03.2003 PRF 3991 -> replaced the code commented out below because
	// it does not work when the selection does not contain speechmarks
	// for example: "WHERE URNO = 119675853"
	// also need to handle "WHERE URNO = 119675853\n119675853" from relhdl
	CString olUrno;
	for(int i = 0; i < opSelection.GetLength(); i++)
	{
		// remove all non-numeric characters
		if(opSelection[i] >= '0' && opSelection[i] <= '9')
		{
			olUrno += opSelection[i];
		}
		else if(!olUrno.IsEmpty())
		{
			// end of the URNO found so break eg for "WHERE URNO = 119675853\n119675853"
			break;
		}
	}
	return atol(olUrno);
}

long CCSCedaData::GetNextUrno(void)
{
	bool olRc;

	olRc = CedaAction("GNU");
	if (olRc == true)
	{
		return(atol(omDataBuf[0]));
	}
	MessageBox(NULL,"Keine Verbindung zum Server oder Timeout,\nProgramm wird beendet",
		"Systemfehler",MB_OK);
	ExitProcess(1);
	return -1;	
}


int CCSCedaData::GetBufferSize(void)
{
	if(	CCSCedaData::bmUseNETIN == true)
	{
		return omDataBuf.GetSize();
	}
	else
	{
		return CCSCedaData::pomFastCeda->omData.GetSize();
	}
}


CStringArray * CCSCedaData::GetDataBuff()
{
	if(	CCSCedaData::bmUseNETIN == true)
	{
		return (&omDataBuf);
	}
	else
	{
		return &CCSCedaData::pomFastCeda->omData;
	}
}



bool CCSCedaData::EqualizeDataSruct(void *pvpBcDataStruct, void *pvpSaveDataStruct, void *pvpChangedDataStruct)
{
	CCSPtrArray<CEDARECINFO> *pomRecInfo = &omRecInfo;

	char pclBuf[128];
    char buf[33];   // ltoa() need buffer 33 bytes
    CString sSave;
    CString sChanged;
    CString sBc;
	CString pcpListOfData;

    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();

    
	for (int fieldno = 0; fieldno < nfields; fieldno++)
    {
        int type = (*pomRecInfo)[fieldno].Type;

        void *pBc	   = (void *)((char *)pvpBcDataStruct + (*pomRecInfo)[fieldno].Offset);
        void *pSave    = (void *)((char *)pvpSaveDataStruct + (*pomRecInfo)[fieldno].Offset);
        void *pChanged = (void *)((char *)pvpChangedDataStruct + (*pomRecInfo)[fieldno].Offset);

        int length = (*pomRecInfo)[fieldno].Length;

        switch (type) 
		{
        case CEDACHAR:
			{
				sSave = (length == 0)? CString(" "): CString((char *)pSave).Left(length);
				MakeCedaString(sSave);
				sChanged = (length == 0)? CString(" "): CString((char *)pChanged).Left(length);
				MakeCedaString(sChanged);
				sBc = (length == 0)? CString(" "): CString((char *)pBc).Left(length);
				MakeCedaString(sBc);
			}
            break;
        case CEDAINT:
			{
				sSave	 = CString(itoa(*(int *)pSave, buf, 10));
				sChanged = CString(itoa(*(int *)pChanged, buf, 10));
				sBc = CString(itoa(*(int *)pBc, buf, 10));
			}
            break;
        case CEDALONG:
			{
				sSave	 = CString(ltoa(*(long *)pSave, buf, 10));
				sChanged = CString(ltoa(*(long *)pChanged, buf, 10));
				sBc = CString(ltoa(*(long *)pBc, buf, 10));
			}
            break;
        case CEDADATE:  // format: YYYYMMDDHHMMSS
			{
				sSave = ((CTime *)pSave)->Format("%Y%m%d%H%M00");
				sChanged = ((CTime *)pChanged)->Format("%Y%m%d%H%M00");
				sBc = ((CTime *)pBc)->Format("%Y%m%d%H%M00");
			}
            break;
        case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
			{
				sSave = ((COleDateTime *)pSave)->Format("%Y%m%d%H%M%S");
				sChanged = ((COleDateTime *)pChanged)->Format("%Y%m%d%H%M%S");
				sBc = ((COleDateTime *)pBc)->Format("%Y%m%d%H%M%S");
			}
            break;
		case CEDADOUBLE:
			{
				sprintf(pclBuf,"%.2f",(double *)pSave);
				sSave = CString(pclBuf);
				sprintf(pclBuf,"%.2f",(double *)pChanged);
				sChanged = CString(pclBuf);
				sprintf(pclBuf,"%.2f",(double *)pBc);
				sBc = CString(pclBuf);
			}
			break;
        default:    // invalid field type
            return false;
        }

		if(sChanged != sBc)
		{
			pcpListOfData += sBc + ",";
		}
		else
		{

		if(sChanged != sSave)
		{
			pcpListOfData += sChanged + ",";
		}
		}

    }

    return true;
}




bool CCSCedaData::StructLocalToUtc(void *pvpDataStruct)
{

	if(pvpDataStruct == NULL)
		return false;

	CTime olTime;
	void *p;

	CCSPtrArray<CEDARECINFO> *pomRecInfo = &omRecInfo;

    int nfields = pomRecInfo->GetSize();
    
	for (int fieldno = 0; fieldno < nfields; fieldno++)
    {
        int type = (*pomRecInfo)[fieldno].Type;
		p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[fieldno].Offset);

		if(type == CEDADATE)
		{
			olTime = *(CTime*) p;

			if(olTime != TIMENULL)
			{
				LocalToUtc(olTime);
				StoreDate(olTime.Format("%Y%m%d%H%M%S"), (CTime *)p);
			}
		}
	}
	return true;
}

bool CCSCedaData::StructUtcToLocal(void *pvpDataStruct)
{
	if(pvpDataStruct == NULL)
		return false;

	CTime olTime;
	void *p;

	CCSPtrArray<CEDARECINFO> *pomRecInfo = &omRecInfo;

    int nfields = pomRecInfo->GetSize();
    
	for (int fieldno = 0; fieldno < nfields; fieldno++)
    {
        int type = (*pomRecInfo)[fieldno].Type;
		p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[fieldno].Offset);

		if(type == CEDADATE)
		{
			olTime = *(CTime*) p;

			if(olTime != TIMENULL)
			{
				UtcToLocal(olTime);
				StoreDate(olTime.Format("%Y%m%d%H%M%S"), (CTime *)p);
			}
		}
	}
	return true;

}




void CCSCedaData::LocalToUtc(CTime &opTime)
{
	if((opTime != TIMENULL) && (omTich != TIMENULL))
	{
		if(opTime < omTich)
		{
			opTime -= omLocalDiff1;
		}
		else
		{
			opTime -= omLocalDiff2;
		}
	}

}

void CCSCedaData::UtcToLocal(CTime &opTime)
{
	CTime olTichUtc;
	olTichUtc = omTich - CTimeSpan(omLocalDiff2);

	if((opTime != TIMENULL) && (omTich != TIMENULL))
	{
		if(opTime < olTichUtc)
		{
			opTime += omLocalDiff1;
		}
		else
		{
			CString olFromDate;
			olFromDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-2, omTich.GetMinute());
			CString olToDate;
			olToDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-1, omTich.GetMinute());
			CString olOpDate = opTime.Format("%Y%m%d%H%M");
			if(olOpDate >= olFromDate && olOpDate <= olToDate)
			{
				opTime += omLocalDiff1;
			}
			else
			{
				opTime += omLocalDiff2;
			}
		}
	}
}



void CCSCedaData::ReadUfisCedaConfig()
{

/*	MWO: 04.05.2004: 
//	This will not longer be used in order to avoid different 
//	definitions of mapping lists. The initialization is at top
//  of this file. We keep this method to avoid uneccesary changes in clients
//  which call this method.

	char pclTable[64] = "AFT";
	char pclFieldList[1024] = "DSSF";
	char pclData[3000] = "";
	char pclSelection[256] = "[CONFIG]";

	CString olTmp;
	CStringArray olArrray;

	CStringArray olSChr;
	CStringArray olCChr;


	TRACE("\n -------------------------------------------------");
	TRACE("\n Read Ufis Ceda Config:");

	bool blRet = CedaAction("GFR", pclTable, pclFieldList, pclSelection, "", pclData, "BUF1");

	for (int i = GetBufferSize() - 1; i >= 0; i--)
	{
		TRACE("\n%s", omDataBuf[i]);

		olTmp = omDataBuf[i];

		pomUfisCedaConfig->Add(olTmp);

		olArrray.RemoveAll();

		ExtractItemList(olTmp, &olArrray);

		if(olArrray.GetSize() > 0)
		{

			if(olArrray[0] == "SCHR")
			{
				ExtractItemList(olTmp, &olSChr);
			}

			if(olArrray[0] == "CCHR")
			{
				ExtractItemList(olTmp, &olCChr);
			}
		}

	}

	if(olCChr.GetSize()  > 3 && olCChr.GetSize() == olSChr.GetSize())
	{
		omClientChars = "";
		omServerChars = "";

		for( i = olCChr.GetSize() - 1; i >= 1; i--)
		{
			omClientChars += (char)atoi(olCChr[i]);
		}
		
		for( i = olSChr.GetSize() - 1; i >= 1; i--)
		{
			omServerChars += (char)atoi(olSChr[i]);
		}

	}
*/
}

//**********************************************************************************
// MWO For faster socket communication in order to read accelerated data with cdrhdl
//**********************************************************************************
bool CCSCedaData::CedaAction2	(char *pcpAction,
    char *pcpSelection, char *pcpSort,
    char *pcpData,char *pcpDest /*"BUF1"*/,bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/, 
	bool bpDelDataArray /* true */, char* tws /*= NULL*/, char* twe /*= NULL*/,
	char* pcpOraHint/*=NULL*/)
{   
	bool blRet = true;
	CString olIdentifier = CString("UFIS_CLIENT_ID");
	CString olPort = CString("3357");
	CString olTable;
	CString olWhere;
	CString olWks;
	CString olErrorText;
	CString olSeparator;
	CString olServer;
	CString olHopo;
	CString olAppl;
	CString olCmd;
	CString olFields;
	CString olTabext;
	CString olUser;
	CString olData;
	CString olTws;
	CString olTwe;
	CString olOraHint;

	if(pcpOraHint != NULL)
	{
		olOraHint = CString(pcpOraHint);
	}
	olUser = CString(pcmUser);
	olTabext = CString(pcmTableExt); 
	if(myFieldList == "")
	{
		olFields = CString(CCSCedaData::pcmFieldList);
	}
	else
	{
		olFields = myFieldList;
	}
	olCmd = CString(pcpAction);
	olAppl = CString(pcmApplName);
	olHopo = CString(pcmHomeAirport);

		// avoid null values
	olData = CString(pcpData);
	int ilIdx = -1;
	do 
	{
		ilIdx = olData.Replace(",,", ", ,");
	}
	while ( ilIdx != 0);

	if (olData.GetLength() > 0)
	{
		if(olData[olData.GetLength()-1] == ',')
		{
			olData += " ";
		}
		if(olData[0] == ',')
		{
			olData = " " + olData;
		}
	}

	// end avoid null values

	olServer = omServerName;
	if(tws != NULL)
		olTws = CString(tws);
	else
		olTws = "";
	if(twe != NULL)
		olTwe = CString(twe);
	else
		olTwe = "";

	olWks = pcmReqId;//GetWorkstationName(pcmReqId);
	if(pcpSelection != NULL)
	{
		olWhere = CString(pcpSelection);
	}
	else
	{
		olWhere = CString(CCSCedaData::pcmSelection);
	}

	olSeparator = CString("\n");
	olTable = CString(CString(pcmTableName));
	if(olTable.GetLength() == 3)
	{
		olTable += "TAB";
	}

	blRet = ReadDataAlternative(olAppl, olCmd, olFields, olData, olHopo, olIdentifier, olPort,
								olServer, olTabext, olTable, olUser, olWhere, olWks, 
								olErrorText, olSeparator, olTws, olTwe, olOraHint);
	
	omLastErrorMessage = olErrorText;

	if(olTable == "PAXTAB")
	{
		olTable = olTable;
	}
	if(tws != NULL)
	{
		strcpy(tws, olTws.GetBuffer(0));
	}
	if(twe != NULL)
	{
		strcpy(twe, olTwe.GetBuffer(0));
	}
	if(strcmp(pcpAction, "GMU") == 0)
	{
		if(pcpData != NULL)
		{
			strcpy(pcpData, olData.GetBuffer(0));
		}
	}
	myFieldList = CString("");
	return blRet;
}

bool CCSCedaData::ReadDataAlternative(CString &ropAppl, CString &ropCommand, CString &ropFields, CString &ropData,
						 CString &ropHopo, CString &ropIdentifier, CString &ropPort,
						 CString &ropServer,	CString &ropTabext,	CString &ropTable,
						 CString &ropUser,	CString &ropWhere, CString &ropWks, 
						 CString &ropErrorText,	CString &ropSepa, CString &ropTws, CString &ropTwe,
						 CString &ropOraHint)
{
	bool blRet = true; 
	CCSCedaData::pomFastCeda->omAppl =			ropAppl;
	CCSCedaData::pomFastCeda->omCommand =		ropCommand;
	CCSCedaData::pomFastCeda->omFields =		ropFields;
	CCSCedaData::pomFastCeda->omHopo =			ropHopo;
	CCSCedaData::pomFastCeda->omIdentifier =	ropIdentifier;
	// This is hardcoded but neccessary
	CCSCedaData::pomFastCeda->omPort =			"3357";
	CCSCedaData::pomFastCeda->omServer =		ropServer;
	CCSCedaData::pomFastCeda->omTabext =		ropTabext;
	CCSCedaData::pomFastCeda->omTable =			ropTable;
	CCSCedaData::pomFastCeda->omUser =			ropUser;
	CCSCedaData::pomFastCeda->omWhere =			ropWhere;
	CCSCedaData::pomFastCeda->omWks =			ropWks;
	CCSCedaData::pomFastCeda->omSepa =			ropSepa;
	CCSCedaData::pomFastCeda->omDataString =	ropData;
	CCSCedaData::pomFastCeda->omTwe	=			ropTwe;
	CCSCedaData::pomFastCeda->omTws	=			ropTws;
	CCSCedaData::pomFastCeda->omOraHint =       ropOraHint;	

//MWO: 28.08.03
	// the return value of CCSCedaData::pomFastCeda->CedaAction():
	// -1 : Socket Error
	// -2 : Oracle Error
	if(CCSCedaData::pomFastCeda->CedaAction() < 0)
	{
		blRet = false;
	}
	long cnt = CCSCedaData::pomFastCeda->omData.GetSize();
	ropErrorText = CCSCedaData::pomFastCeda->omErrorText;


//And copy it back 
	ropAppl =		CCSCedaData::pomFastCeda->omAppl;			
	ropCommand =	CCSCedaData::pomFastCeda->omCommand;		
	ropFields =		CCSCedaData::pomFastCeda->omFields;		
	ropHopo =		CCSCedaData::pomFastCeda->omHopo;			
	ropIdentifier = CCSCedaData::pomFastCeda->omIdentifier;	
	ropServer =		CCSCedaData::pomFastCeda->omServer;		
	ropTabext =		CCSCedaData::pomFastCeda->omTabext;		
	ropTable =		CCSCedaData::pomFastCeda->omTable;			
	ropUser =		CCSCedaData::pomFastCeda->omUser;			
	ropWhere =		CCSCedaData::pomFastCeda->omWhere;			
	ropWks =		CCSCedaData::pomFastCeda->omWks;			
	ropSepa =		CCSCedaData::pomFastCeda->omSepa;			
	ropData =		CCSCedaData::pomFastCeda->omDataString;
	ropTwe =		CCSCedaData::pomFastCeda->omTwe;
	ropTws =		CCSCedaData::pomFastCeda->omTws;

	return blRet;
}


bool CCSCedaData::GetBufferLine2(int ipLineNo, CString &pcpListOfData)
{
    // Check if the line number is out-of-bound?
    if (!(0 <= ipLineNo && ipLineNo < CCSCedaData::pomFastCeda->omData.GetSize()))
    {
        return false;
    }

    pcpListOfData = CCSCedaData::pomFastCeda->omData[ipLineNo];
    return true;

}


bool CCSCedaData::GetBufferRecord2(int ipLineNo, void *pvpDataStruct)
{
    return GetBufferRecord2(&omRecInfo,ipLineNo,pvpDataStruct);
}


bool CCSCedaData::GetBufferRecord2(CCSPtrArray<CEDARECINFO> *pomRecInfo, int ipLineNo, void *pvpDataStruct)
{
	char pclResult[4000]="";
    // Check if the line number is out-of-bound?
    if (!(0 <= ipLineNo && ipLineNo < CCSCedaData::pomFastCeda->omData.GetSize()))
    {
        return false;
    }
	return GetBufferRecord2(&omRecInfo,CCSCedaData::pomFastCeda->omData[ipLineNo],pvpDataStruct, CString(pcmFieldList));

}




bool CCSCedaData::GetBufferRecord2(CCSPtrArray<CEDARECINFO> *pomRecInfo, CString record, void *pvpDataStruct)
{
	return GetBufferRecord2(pomRecInfo,record, pvpDataStruct, CString(pcmFieldList));
}


void CCSCedaData::ClearFastSocketBuffer()
{
	CCSCedaData::pomFastCeda->ClearDataBuffer();
}

bool CCSCedaData::GetFirstBufferRecord2(void *pvpDataStruct)
{   
    if ( GetBufferRecord2(&omRecInfo,0, pvpDataStruct) ) 
	{
		CCSCedaData::pomFastCeda->omData.RemoveAt(0);
		return true;CCSCedaData::pomFastCeda->omData.RemoveAt(0);
	}
	return false;
}



bool CCSCedaData::GetBufferRecord2(int ipLineNo, void *pvpDataStruct, CString olFieldList)
{   
    return GetBufferRecord2(&omRecInfo,ipLineNo,pvpDataStruct, olFieldList);
}


bool CCSCedaData::GetBufferRecord2(CCSPtrArray<CEDARECINFO> *pomRecInfo, int ipLineNo, void *pvpDataStruct, CString &olFieldList)
{   
    // Check if the line number is out-of-bound?
    if (!(0 <= ipLineNo && ipLineNo < CCSCedaData::pomFastCeda->omData.GetSize()))
    {
        return false;
    }

	return GetBufferRecord2(&omRecInfo,CCSCedaData::pomFastCeda->omData[ipLineNo],pvpDataStruct, olFieldList);
}

bool CCSCedaData::GetBufferRecord2(CCSPtrArray<CEDARECINFO> *pomRecInfo, CString record, void *pvpDataStruct, CString &olFieldList)
{   

	int ilCountField;
	int ilCountData;
	if(omFieldMap.GetCount() == 0)
	{
		for(int i = pomRecInfo->GetSize() - 1; i >= 0; i--)
		{
			omFieldMap.SetAt((LPCSTR)(*pomRecInfo)[i].Name, (void*)&(*pomRecInfo)[i]);			
		}
	}

	CEDARECINFO *prlRecInfo;
	CStringArray olFieldArray;
	CStringArray olDataArray;

	ilCountField = 	ExtractItemList(CString(olFieldList), &olFieldArray, ',');
	ilCountData = 	ExtractItemList(record, &olDataArray, ',');

	if(ilCountField != ilCountData)
		return false;

	for(int ilLc = 0; ilLc < ilCountField; ilLc++)
	{
		CString olTmpName = olFieldArray[ilLc];
		olTmpName.MakeUpper();
		//if(omFieldMap.Lookup((LPCSTR)olFieldArray[ilLc], (void*&)prlRecInfo) == TRUE)
		if(omFieldMap.Lookup((LPCSTR)olTmpName, (void*&)prlRecInfo) == TRUE)
		{
			// Convert data from text-line to the corresponding field in record
			int type = prlRecInfo->Type;
			void *p = (void *)((char *)pvpDataStruct + prlRecInfo->Offset);
			int length = prlRecInfo->Length;
			int flags  = prlRecInfo->Flags;

			switch (type) {
			case CEDACHAR:
				StoreChar(olDataArray[ilLc], (char *)p, length, flags);
				break;
			case CEDAINT:
				StoreInt(olDataArray[ilLc], (int *)p);
				break;
			case CEDALONG:
				StoreLong(olDataArray[ilLc], (long *)p);
				break;
			case CEDADOUBLE:
				StoreDouble(olDataArray[ilLc], (double *)p);
				break;
			case CEDADATE:
				StoreDate(olDataArray[ilLc], (CTime *)p);
				break;
			case CEDAOLEDATE:
				StoreOleDate(olDataArray[ilLc], (COleDateTime *)p);
				break;

			default:    // invalid field type
				return false;
			}
		}
    }

    return true;
}

bool CCSCedaData::StructLocalToUtc(void *pvpDataStruct, const CString &opApt4)
{

	if(pvpDataStruct == NULL)
		return false;

	CTime olTime;
	void *p;

	CCSPtrArray<CEDARECINFO> *pomRecInfo = &omRecInfo;

    int nfields = pomRecInfo->GetSize();
    
	for (int fieldno = 0; fieldno < nfields; fieldno++)
    {
        int type = (*pomRecInfo)[fieldno].Type;
		p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[fieldno].Offset);

		if(type == CEDADATE)
		{
			olTime = *(CTime*) p;

			if(olTime != TIMENULL)
			{
				CedaAptLocalUtc::AptLocalToUtc (olTime, opApt4);
				StoreDate(olTime.Format("%Y%m%d%H%M%S"), (CTime *)p);
			}
		}
	}
	return true;
}

bool CCSCedaData::StructUtcToLocal(void *pvpDataStruct, const CString &opApt4)
{
	if(pvpDataStruct == NULL)
		return false;

	CTime olTime;
	void *p;

	CCSPtrArray<CEDARECINFO> *pomRecInfo = &omRecInfo;

    int nfields = pomRecInfo->GetSize();
    
	for (int fieldno = 0; fieldno < nfields; fieldno++)
    {
        int type = (*pomRecInfo)[fieldno].Type;
		p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[fieldno].Offset);

		if(type == CEDADATE)
		{
			olTime = *(CTime*) p;

			if(olTime != TIMENULL)
			{
				CedaAptLocalUtc::AptUtcToLocal (olTime, opApt4);
				StoreDate(olTime.Format("%Y%m%d%H%M%S"), (CTime *)p);
			}
		}
	}
	return true;

}

bool CCSCedaData::GenerateExcelOutput(const EXCELINFO& ropExcelInfo)
{
	CWaitCursor olWait;

	bool blResult = false;

	// general check
	if (this->pcmTableName == NULL)
		return blResult;

	CString olTableName(this->pcmTableName);
	if (olTableName != ropExcelInfo.omTable.omName)
		return blResult;

	try
	{
		if (ropExcelInfo.bmUseAutomation)
		{
			blResult = GenerateAutomatedExcelOutput(ropExcelInfo);
		}
		else
		{
			blResult = GenerateStreamedExcelOutput(ropExcelInfo);
		}
	}
	catch(...)
	{
		
	}

	return blResult;
}

//This function generates error when the no of fields in record structure 
//and no of columns to be shown to the user is different. So We added  new excel generation functions.
bool CCSCedaData::GenerateExcelOutput1(const EXCELINFO& ropExcelInfo)
{
	CWaitCursor olWait;

	bool blResult = false;

	// general check
	if (this->pcmTableName == NULL)
		return blResult;

	CString olTableName(this->pcmTableName);
	if (olTableName != ropExcelInfo.omTable.omName)
		return blResult;

	try
	{
		if (ropExcelInfo.bmUseAutomation)
		{
			blResult = GenerateAutomatedExcelOutput1(ropExcelInfo);
		}
		else
		{
			blResult = GenerateStreamedExcelOutput1(ropExcelInfo);
		}
	}
	catch(...)
	{
		
	}

	return blResult;
}

bool CCSCedaData::GenerateAutomatedExcelOutput(const EXCELINFO& ropExcelInfo)
{
	_Application olExcel;
	if (olExcel.CreateDispatch("Excel.Application"))
	{
		olExcel.SetVisible(TRUE);
		olExcel.SetWindowState(0xFFFFEFD1);
		LPDISPATCH pIDispatch = olExcel.GetWorkbooks();
		Workbooks olWorkbooks;
		olWorkbooks.AttachDispatch(pIDispatch);
		COleVariant olTemplate;
		olTemplate.ChangeType(VT_NULL);
		pIDispatch = olWorkbooks.Add(olTemplate);
		_Workbook olWorkbook;
		olWorkbook.AttachDispatch(pIDispatch);

		CString olTableName;
		olTableName.Format("%s -   %s", ropExcelInfo.omTable.omName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


		CString olFileName = olTableName;
		olFileName.Remove('*');
		olFileName.Remove('.');
		olFileName.Remove(':');
		olFileName.Remove('/');
		olFileName.Replace(" ", "_");
		
		olFileName =  omTmpPath + CString("\\") + olFileName + ".xsl";

		COleVariant olVarFilename(olFileName);
		COleVariant olVarFileformat((long)43);
		COleVariant olVarPassword("");
		COleVariant olVarWriteResPassword("");
		COleVariant olVarReadOnly((short)false,VT_BOOL);
		COleVariant olVarCreateBackup((short)true,VT_BOOL);
		long		llAccessMode = 3;
		COleVariant olVarConflictRes((long)1);
		COleVariant olVarAddToMru((short)true,VT_BOOL);
		COleVariant olVarCodepage("");
		COleVariant olVarVisualLayout("");
		olWorkbook.SaveAs(olVarFilename,olVarFileformat,olVarPassword,olVarWriteResPassword,olVarReadOnly,olVarCreateBackup,llAccessMode,olVarConflictRes,olVarAddToMru,olVarCodepage,olVarVisualLayout);


		pIDispatch = olWorkbook.GetActiveSheet();
		_Worksheet olWorksheet;
		olWorksheet.AttachDispatch(pIDispatch);
		olWorksheet.SetName(ropExcelInfo.omTable.omName);

		pIDispatch = olWorksheet.GetCells();
		Range olCells;
		olCells.AttachDispatch(pIDispatch);
		COleVariant olFormat("@");
		olCells.SetNumberFormat(olFormat);

		COleVariant olRowIndex((long)1);
		for (int i = 0; i < ropExcelInfo.omTable.omFields.GetSize(); i++)
		{
			COleVariant olColumnIndex((long)(i+1));
			COleVariant olValue(ropExcelInfo.omTable.omFields[i].omHeaderText);
			olCells.SetItem(olRowIndex,olColumnIndex,olValue);
		}
		
		// generate additional values
		for (i = 0; i < ropExcelInfo.omAdditional.GetSize(); i++)
		{
			COleVariant olColumnIndex((long)(i+1));
			COleVariant olValue(ropExcelInfo.omAdditional[i].omHeaderText);
			olCells.SetItem(olRowIndex,olColumnIndex,olValue);
		}
		
		// Generate urno map
		CMapStringToPtr olUrnoMap;
		for (i = 0; i < ropExcelInfo.omUrnos.GetSize(); i++)
		{
			olUrnoMap.SetAt(ropExcelInfo.omUrnos[i],NULL);
		}

		CUIntArray	olIndexes;	
		olIndexes.SetSize(ropExcelInfo.omTable.omFields.GetSize());

		int ilUrnoIndex = -1;

		for (i = 0; i < ropExcelInfo.omTable.omFields.GetSize(); i++)
		{
			for (int j = 0; j < this->omRecInfo.GetSize(); j++)
			{
				if (CString(this->omRecInfo[j].Name) == ropExcelInfo.omTable.omFields[i].omName)
				{
					olIndexes.SetAt(i,j);
					break;
				}
				else if (ilUrnoIndex == -1 && CString(this->omRecInfo[j].Name) == "URNO")
				{
					ilUrnoIndex = i;
				}
			}
		}

		for (i = 0; i < ropExcelInfo.omTable.omData.GetSize(); i++)
		{
			// convert struct to string
			CString olDataList;
			if (!this->MakeCedaData(olDataList,ropExcelInfo.omTable.omData[i]))
				continue;

			// convert string to string array
			CStringArray olData;
			::ExtractItemList(olDataList,&olData,',');

			// check on last comma !
			if (!olDataList.IsEmpty())
			{
				if (olDataList[olDataList.GetLength()-1] == ',')
					olData.Add("");
			}

			// check whether we must display this record or not
			void *polDummy;
			if (ropExcelInfo.omUrnos.GetSize() > 0 && !olUrnoMap.Lookup(olData[ilUrnoIndex],polDummy))		
				continue;

			// display all fields
			CTime olTime;
			COleDateTime olDateTime;
			char buffer[1024];

			for (int j = 0; j < ropExcelInfo.omTable.omFields.GetSize(); j++)
			{
				
				CString olValue;

				switch(this->omRecInfo[olIndexes[j]].Type)
				{
				case CEDADATE :
					StoreDate(olData[olIndexes[j]],&olTime);
					olValue = olTime.Format("%d.%m.%Y %H:%M");
				break;
				case CEDAOLEDATE :
					StoreOleDate(olData[olIndexes[j]],&olDateTime);
					olValue = olDateTime.Format("%d.%m.%Y %H:%M");
				break;
				case CEDACHAR:

					if(olData[olIndexes[j]].GetLength() > 0)
					{
						StoreChar(olData[olIndexes[j]], buffer, olData[olIndexes[j]].GetLength()+1, 1);
						olValue = CString(buffer);
					}
					else
					{
						olValue = CString("");
					}
				break;
				default:
					olValue = olData[olIndexes[j]];
				}

				COleVariant olRowIndex((long)(i+2));
				COleVariant olColumnIndex((long)(j+1));
				COleVariant olCellValue(olValue);
				olCells.SetItem(olRowIndex,olColumnIndex,olCellValue);
			}

			for (j = 0; j < ropExcelInfo.omAdditional.GetSize(); j++)
			{
				
				CString olValue = ropExcelInfo.omAdditional[j].omValues[i];

				COleVariant olRowIndex((long)(i+2));
				COleVariant olColumnIndex((long)(j + 1 + ropExcelInfo.omTable.omFields.GetSize()));
				COleVariant olCellValue(olValue);
				olCells.SetItem(olRowIndex,olColumnIndex,olCellValue);
			}

							
		}

		pIDispatch = olWorksheet.GetColumns();
		Range olColumns;
		olColumns.AttachDispatch(pIDispatch);

		olColumns.AutoFit();

		olWorkbook.Save();

		olExcel.ReleaseDispatch();			
	}

	return false;			
}

bool CCSCedaData::GenerateAutomatedExcelOutput1(const EXCELINFO& ropExcelInfo)
{
	_Application olExcel;
	if (olExcel.CreateDispatch("Excel.Application"))
	{
		olExcel.SetVisible(TRUE);
		olExcel.SetWindowState(0xFFFFEFD1);
		LPDISPATCH pIDispatch = olExcel.GetWorkbooks();
		Workbooks olWorkbooks;
		olWorkbooks.AttachDispatch(pIDispatch);
		COleVariant olTemplate;
		olTemplate.ChangeType(VT_NULL);
		pIDispatch = olWorkbooks.Add(olTemplate);
		_Workbook olWorkbook;
		olWorkbook.AttachDispatch(pIDispatch);

		CString olTableName;
		olTableName.Format("%s -   %s", ropExcelInfo.omTable.omName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


		CString olFileName = olTableName;
		olFileName.Remove('*');
		olFileName.Remove('.');
		olFileName.Remove(':');
		olFileName.Remove('/');
		olFileName.Replace(" ", "_");
		
		olFileName =  omTmpPath + CString("\\") + olFileName + ".xsl";

		COleVariant olVarFilename(olFileName);
		COleVariant olVarFileformat((long)43);
		COleVariant olVarPassword("");
		COleVariant olVarWriteResPassword("");
		COleVariant olVarReadOnly((short)false,VT_BOOL);
		COleVariant olVarCreateBackup((short)true,VT_BOOL);
		long		llAccessMode = 3;
		COleVariant olVarConflictRes((long)1);
		COleVariant olVarAddToMru((short)true,VT_BOOL);
		COleVariant olVarCodepage("");
		COleVariant olVarVisualLayout("");
		olWorkbook.SaveAs(olVarFilename,olVarFileformat,olVarPassword,olVarWriteResPassword,olVarReadOnly,olVarCreateBackup,llAccessMode,olVarConflictRes,olVarAddToMru,olVarCodepage,olVarVisualLayout);


		pIDispatch = olWorkbook.GetActiveSheet();
		_Worksheet olWorksheet;
		olWorksheet.AttachDispatch(pIDispatch);
		olWorksheet.SetName(ropExcelInfo.omTable.omName);

		pIDispatch = olWorksheet.GetCells();
		Range olCells;
		olCells.AttachDispatch(pIDispatch);
		COleVariant olFormat("@");
		olCells.SetNumberFormat(olFormat);

		COleVariant olRowIndex((long)1);
		for (int i = 0; i < ropExcelInfo.omTable.omFields.GetSize(); i++)
		{
			COleVariant olColumnIndex((long)(i+1));
			COleVariant olValue(ropExcelInfo.omTable.omFields[i].omHeaderText);
			olCells.SetItem(olRowIndex,olColumnIndex,olValue);
		}
		
		// generate additional values
		for (i = 0; i < ropExcelInfo.omAdditional.GetSize(); i++)
		{
			COleVariant olColumnIndex((long)(i+1));
			COleVariant olValue(ropExcelInfo.omAdditional[i].omHeaderText);
			olCells.SetItem(olRowIndex,olColumnIndex,olValue);
		}
		
		// Generate urno map
		CMapStringToPtr olUrnoMap;
		for (i = 0; i < ropExcelInfo.omUrnos.GetSize(); i++)
		{
			olUrnoMap.SetAt(ropExcelInfo.omUrnos[i],NULL);
		}

		CUIntArray	olIndexes;	
		olIndexes.SetSize(ropExcelInfo.omTable.omFields.GetSize());

		int ilUrnoIndex = -1;

		CStringArray olFields;
		
		int nfieldlist = ::ExtractItemList(pcmFieldList,&olFields,',');
		for (i = 0; i < ropExcelInfo.omTable.omFields.GetSize(); i++)
		{
			for (int j = 0; j < nfieldlist; j++)
			{
				if (olFields[j] == ropExcelInfo.omTable.omFields[i].omName)
				{
					olIndexes.SetAt(i,j);
					break;
				}
				else if (ilUrnoIndex == -1 && CString(this->omRecInfo[j].Name) == "URNO")
				{
					ilUrnoIndex = i;
				}
			}
		}

		for (i = 0; i < ropExcelInfo.omTable.omData.GetSize(); i++)
		{
			// convert struct to string
			CString olDataList;
			if (!this->MakeCedaData(olDataList,ropExcelInfo.omTable.omData[i]))
				continue;

			// convert string to string array
			CStringArray olData;
			::ExtractItemList(olDataList,&olData,',');

			// check on last comma !
			if (!olDataList.IsEmpty())
			{
				if (olDataList[olDataList.GetLength()-1] == ',')
					olData.Add("");
			}

			// check whether we must display this record or not
			void *polDummy;
			if (ropExcelInfo.omUrnos.GetSize() > 0 && !olUrnoMap.Lookup(olData[ilUrnoIndex],polDummy))		
				continue;

			// display all fields
			CTime olTime;
			COleDateTime olDateTime;
			char buffer[1024];

			for (int j = 0; j < ropExcelInfo.omTable.omFields.GetSize(); j++)
			{
				
				CString olValue;

				switch(this->omRecInfo[olIndexes[j]].Type)
				{
				case CEDADATE :
					StoreDate(olData[olIndexes[j]],&olTime);
					olValue = olTime.Format("%d.%m.%Y %H:%M");
				break;
				case CEDAOLEDATE :
					StoreOleDate(olData[olIndexes[j]],&olDateTime);
					olValue = olDateTime.Format("%d.%m.%Y %H:%M");
				break;
				case CEDACHAR:

					if(olData[olIndexes[j]].GetLength() > 0)
					{
						StoreChar(olData[olIndexes[j]], buffer, olData[olIndexes[j]].GetLength()+1, 1);
						olValue = CString(buffer);
					}
					else
					{
						olValue = CString("");
					}
				break;
				default:
					olValue = olData[olIndexes[j]];
				}

				COleVariant olRowIndex((long)(i+2));
				COleVariant olColumnIndex((long)(j+1));
				COleVariant olCellValue(olValue);
				olCells.SetItem(olRowIndex,olColumnIndex,olCellValue);
			}

			for (j = 0; j < ropExcelInfo.omAdditional.GetSize(); j++)
			{
				
				CString olValue = ropExcelInfo.omAdditional[j].omValues[i];

				COleVariant olRowIndex((long)(i+2));
				COleVariant olColumnIndex((long)(j + 1 + ropExcelInfo.omTable.omFields.GetSize()));
				COleVariant olCellValue(olValue);
				olCells.SetItem(olRowIndex,olColumnIndex,olCellValue);
			}

							
		}

		pIDispatch = olWorksheet.GetColumns();
		Range olColumns;
		olColumns.AttachDispatch(pIDispatch);

		olColumns.AutoFit();

		olWorkbook.Save();

		olExcel.ReleaseDispatch();			
	}

	return false;			
}
bool CCSCedaData::GenerateStreamedExcelOutput(const EXCELINFO& ropExcelInfo)
{
	ofstream of;
	CString olTableName;
	olTableName.Format("%s -   %s", ropExcelInfo.omTable.omName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	olFileName =  omTmpPath + CString("\\") + olFileName + ".csv";

	of.open(olFileName, ios::out);


	of  << setw(0) << ropExcelInfo.omTable.omName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	
	// generate header list
	for (int i = 0; i < ropExcelInfo.omTable.omFields.GetSize(); i++)
	{
		if (i != ropExcelInfo.omTable.omFields.GetSize() - 1)
		{
			of	<< setw(0) << ropExcelInfo.omTable.omFields[i].omHeaderText
				<< setw(0) << ropExcelInfo.omDelimiter;
		}
		else if (ropExcelInfo.omAdditional.GetSize() == 0)
		{
			of	<< setw(0) << ropExcelInfo.omTable.omFields[i].omHeaderText
				<< endl;
		}
	}		

	// generate additional fields
	for (i = 0; i < ropExcelInfo.omAdditional.GetSize(); i++)
	{

		if (i != ropExcelInfo.omAdditional.GetSize() - 1)
		{
			of	<< setw(0) << ropExcelInfo.omAdditional[i].omHeaderText
				<< setw(0) << ropExcelInfo.omDelimiter;
		}
		else
		{
			of	<< setw(0) << ropExcelInfo.omAdditional[i].omHeaderText
				<< endl;
		}
		
	}


	// Generate urno map
	CMapStringToPtr olUrnoMap;
	for (i = 0; i < ropExcelInfo.omUrnos.GetSize(); i++)
	{
		olUrnoMap.SetAt(ropExcelInfo.omUrnos[i],NULL);
	}

	CUIntArray	olIndexes;	
	olIndexes.SetSize(ropExcelInfo.omTable.omFields.GetSize());

	int ilUrnoIndex = -1;

	for (i = 0; i < ropExcelInfo.omTable.omFields.GetSize(); i++)
	{
		for (int j = 0; j < this->omRecInfo.GetSize(); j++)
		{
			if (CString(this->omRecInfo[j].Name) == ropExcelInfo.omTable.omFields[i].omName)
			{
				olIndexes.SetAt(i,j);
				break;
			}
			else if (ilUrnoIndex == -1 && CString(this->omRecInfo[j].Name) == "URNO")
			{
				ilUrnoIndex = i;
			}
		}
	}

	for (i = 0; i < ropExcelInfo.omTable.omData.GetSize(); i++)
	{
		// convert struct to string
		CString olDataList;
		if (!this->MakeCedaData(olDataList,ropExcelInfo.omTable.omData[i]))
			continue;

		// convert string to string array
		CStringArray olData;
		::ExtractItemList(olDataList,&olData,',');

		// check on last comma !
		if (!olDataList.IsEmpty())
		{
			if (olDataList[olDataList.GetLength()-1] == ',')
				olData.Add("");
		}

		// check whether we must display this record or not
		void *polDummy;
		if (ropExcelInfo.omUrnos.GetSize() > 0 && !olUrnoMap.Lookup(olData[ilUrnoIndex],polDummy))		
			continue;

		// display all fields
		CTime olTime;
		COleDateTime olDateTime;
		char buffer[1024];


		for (int j = 0; j < ropExcelInfo.omTable.omFields.GetSize(); j++)
		{
			
			CString olValue;

			switch(this->omRecInfo[olIndexes[j]].Type)
			{
			case CEDADATE :
				StoreDate(olData[olIndexes[j]],&olTime);
				olValue = olTime.Format("%d.%m.%Y %H:%M");
			break;
			case CEDAOLEDATE :
				StoreOleDate(olData[olIndexes[j]],&olDateTime);
				olValue = olDateTime.Format("%d.%m.%Y %H:%M");
			break;
			case CEDACHAR:

				if(olData[olIndexes[j]].GetLength() > 0)
				{
					StoreChar(olData[olIndexes[j]], buffer, olData[olIndexes[j]].GetLength()+1, 1);
					olValue = CString(buffer);
				}
				else
				{
					olValue = CString("");
				}
			break;
			default:
				olValue = olData[olIndexes[j]];
			}

			if (j != ropExcelInfo.omTable.omFields.GetSize() - 1)
			{
				of	<< olValue
					<< ropExcelInfo.omDelimiter;	
			}
			else if (ropExcelInfo.omAdditional.GetSize() == 0)
			{
				of	<< olValue
					<< endl;
			}
		}


		for (j = 0; j < ropExcelInfo.omAdditional.GetSize(); j++)
		{
			
			CString olValue = ropExcelInfo.omAdditional[j].omValues[i];

			if (j != ropExcelInfo.omAdditional.GetSize() - 1)
			{
				of	<< olValue
					<< ropExcelInfo.omDelimiter;	
			}
			else
			{
				of	<< olValue
					<< endl;
			}
		}


							
	}

	of.close();


   /* Set up parameters to be sent: */
	char *args[4];
	args[0] = "child";
	args[1] = olFileName.GetBuffer(0);
	args[2] = NULL;
	args[3] = NULL;

	_spawnv( _P_NOWAIT ,ropExcelInfo.omExcelPath, args );

	return true;
}

bool CCSCedaData::GenerateStreamedExcelOutput1(const EXCELINFO& ropExcelInfo)
{
	ofstream of;
	CString olTableName;
	olTableName.Format("%s -   %s", ropExcelInfo.omTable.omName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	olFileName =  omTmpPath + CString("\\") + olFileName + ".csv";

	of.open(olFileName, ios::out);


	of  << setw(0) << ropExcelInfo.omTable.omName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	
	// generate header list
	for (int i = 0; i < ropExcelInfo.omTable.omFields.GetSize(); i++)
	{
		if (i != ropExcelInfo.omTable.omFields.GetSize() - 1)
		{
			of	<< setw(0) << ropExcelInfo.omTable.omFields[i].omHeaderText
				<< setw(0) << ropExcelInfo.omDelimiter;
		}
		else if (ropExcelInfo.omAdditional.GetSize() == 0)
		{
			of	<< setw(0) << ropExcelInfo.omTable.omFields[i].omHeaderText
				<< endl;
		}
	}		

	// generate additional fields
	for (i = 0; i < ropExcelInfo.omAdditional.GetSize(); i++)
	{

		if (i != ropExcelInfo.omAdditional.GetSize() - 1)
		{
			of	<< setw(0) << ropExcelInfo.omAdditional[i].omHeaderText
				<< setw(0) << ropExcelInfo.omDelimiter;
		}
		else
		{
			of	<< setw(0) << ropExcelInfo.omAdditional[i].omHeaderText
				<< endl;
		}
		
	}


	// Generate urno map
	CMapStringToPtr olUrnoMap;
	for (i = 0; i < ropExcelInfo.omUrnos.GetSize(); i++)
	{
		olUrnoMap.SetAt(ropExcelInfo.omUrnos[i],NULL);
	}

	CUIntArray	olIndexes;	
	olIndexes.SetSize(ropExcelInfo.omTable.omFields.GetSize());

	int ilUrnoIndex = -1;

	CStringArray olFields;
	int nfieldlist = ::ExtractItemList(pcmFieldList,&olFields,',');
	for (i = 0; i < ropExcelInfo.omTable.omFields.GetSize(); i++)
	{
		for (int j = 0; j < nfieldlist; j++)
		{
			if (olFields[j] == ropExcelInfo.omTable.omFields[i].omName)
			{
				olIndexes.SetAt(i,j);
				break;
			}
			else if (ilUrnoIndex == -1 && CString(this->omRecInfo[j].Name) == "URNO")
			{
				ilUrnoIndex = i;
			}
		}
	}


	for (i = 0; i < ropExcelInfo.omTable.omData.GetSize(); i++)
	{
		// convert struct to string
		CString olDataList;
		if (!this->MakeCedaData(olDataList,ropExcelInfo.omTable.omData[i]))
			continue;

		// convert string to string array
		CStringArray olData;
		::ExtractItemList(olDataList,&olData,',');

		// check on last comma !
		if (!olDataList.IsEmpty())
		{
			if (olDataList[olDataList.GetLength()-1] == ',')
				olData.Add("");
		}

		// check whether we must display this record or not
		void *polDummy;
		if (ropExcelInfo.omUrnos.GetSize() > 0 && !olUrnoMap.Lookup(olData[ilUrnoIndex],polDummy))		
			continue;

		// display all fields
		CTime olTime;
		COleDateTime olDateTime;
		char buffer[1024];


		for (int j = 0; j < ropExcelInfo.omTable.omFields.GetSize(); j++)
		{
			
			CString olValue;

			switch(this->omRecInfo[olIndexes[j]].Type)
			{
			case CEDADATE :
				StoreDate(olData[olIndexes[j]],&olTime);
				olValue = olTime.Format("%d.%m.%Y %H:%M");
			break;
			case CEDAOLEDATE :
				StoreOleDate(olData[olIndexes[j]],&olDateTime);
				olValue = olDateTime.Format("%d.%m.%Y %H:%M");
			break;
			case CEDACHAR:

				if(olData[olIndexes[j]].GetLength() > 0)
				{
					StoreChar(olData[olIndexes[j]], buffer, olData[olIndexes[j]].GetLength()+1, 1);
					olValue = CString(buffer);
				}
				else
				{
					olValue = CString("");
				}
			break;
			default:
				olValue = olData[olIndexes[j]];
			}

			if (j != ropExcelInfo.omTable.omFields.GetSize() - 1)
			{
				of	<< olValue
					<< ropExcelInfo.omDelimiter;	
			}
			else if (ropExcelInfo.omAdditional.GetSize() == 0)
			{
				of	<< olValue
					<< endl;
			}
		}


		for (j = 0; j < ropExcelInfo.omAdditional.GetSize(); j++)
		{
			
			CString olValue = ropExcelInfo.omAdditional[j].omValues[i];

			if (j != ropExcelInfo.omAdditional.GetSize() - 1)
			{
				of	<< olValue
					<< ropExcelInfo.omDelimiter;	
			}
			else
			{
				of	<< olValue
					<< endl;
			}
		}


							
	}

	of.close();


   /* Set up parameters to be sent: */
	char *args[4];
	args[0] = "child";
	args[1] = olFileName.GetBuffer(0);
	args[2] = NULL;
	args[3] = NULL;

	_spawnv( _P_NOWAIT ,ropExcelInfo.omExcelPath, args );

	return true;
}
CString	CCSCedaData::GetTableName()
{
	if (this->pcmTableName != NULL)
		return this->pcmTableName;
	else
		return "";
}

CString	CCSCedaData::GetFieldList()
{
	if (this->pcmFieldList != NULL)
		return this->pcmFieldList;
	else
		return "";
}
