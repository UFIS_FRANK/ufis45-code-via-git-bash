// ccsbuttonctrl.cpp : implementation file
//

#include <stdafx.h>
#include <ccsbuttonctrl.h>
#include <CCSDefines.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#define MoveTo(a,b,c) {::MoveToEx(a,b,c,NULL);}
#define GetTextExtent(a,b,c) {SIZE rlSize;::GetTextExtentPoint32(a,b,c, &rlSize);}
/////////////////////////////////////////////////////////////////////////////
// ccsCCSButtonCtrl

IMPLEMENT_DYNCREATE(CCSButtonCtrl, CButton)

CCSButtonCtrl::CCSButtonCtrl(bool bpWithRecess /*= false*/)
{
	bmWithRecess = bpWithRecess;
	bmIsRecessed = false;
    m_BtnFace =   ::GetSysColor(COLOR_BTNFACE);;
    m_BtnShadow =  ::GetSysColor(COLOR_BTNSHADOW);
    m_BtnHilite = ::GetSysColor(COLOR_BTNHIGHLIGHT);
    m_BtnFaceDisabled = m_BtnFace;
	cmKey = '?';
	imInflateRectPixelHight = -3;
	imInflateRectPixelWidth = -3;
}

CCSButtonCtrl::~CCSButtonCtrl()
{
}

//MWO 06.02.2002
LONG CCSButtonCtrl::OnDragOver(UINT wParam, LONG lParam)
{
	if(pomParent == NULL)
		pomParent = GetParent();
    return pomParent->SendMessage(WM_DRAGOVER, wParam, lParam);
}


LONG CCSButtonCtrl::OnDrop(UINT wParam, LONG lParam)
{
	if(pomParent == NULL)
		pomParent = GetParent();
    return pomParent->SendMessage(WM_DROP, wParam, lParam);
}


void CCSButtonCtrl::DragDropRegister()
{
	omDragDrop.RegisterTarget(this, GetParent());
}

void CCSButtonCtrl::OnMouseMove(UINT nFlags, CPoint point)
{
	CButton::OnMouseMove(nFlags,point);
}

//END MWO
void CCSButtonCtrl::Recess(bool bpIsRecessed)
{
	if(bmWithRecess == true)
	{
		bmIsRecessed = bpIsRecessed;
		SetState(bmIsRecessed);
	}
}


/*
void CCSButtonCtrl::OnEnable(BOOL bEnable) 
{
	if(	bEnable && ((cmKey == '0') || (cmKey == '-')))
		return;
	
//don�t use, it�s not enough to aktivate the drawitem.
#if 0	
	if(bEnable == FALSE)
		ModifyStyle( 0 , WS_DISABLED ,0 );
	else
		ModifyStyle( WS_DISABLED , 0,   0 );
#endif

	CButton::OnEnable(bEnable);
}
*/

BOOL CCSButtonCtrl::ShowWindow( int nCmdShow )
{
	if((nCmdShow == SW_SHOW) && (cmKey == '-'))
		return FALSE;

	return CButton::ShowWindow( nCmdShow );

}


void CCSButtonCtrl::EnableWindow(BOOL bpEnable)
{
	if (cmKey != '1' && cmKey != '?')
		return;
//	OnEnable(bpEnable);
	CButton::EnableWindow(bpEnable);
}


void CCSButtonCtrl::SetSecState(char cpKey)
{
	if(cpKey == cmKey)
		return;

	cmKey = '?';

	switch(cpKey)
	{
	case '1': // Licens to edit
			ShowWindow(SW_SHOW);
			EnableWindow(TRUE);
		break;
	case '0': //Readonly
		{
			ShowWindow(SW_SHOW);
//			ModifyStyle(WS_TABSTOP, WS_DISABLED,0);
			EnableWindow(FALSE);
		}
		break;
	case '-': //Hide
		{
			ShowWindow(SW_HIDE);
//			ModifyStyle(WS_TABSTOP, WS_DISABLED,0);
			EnableWindow(FALSE);
		}
		break;
	default: //Error
		{
			ShowWindow(SW_HIDE);
			EnableWindow(FALSE);
//			ModifyStyle(WS_TABSTOP, 0,0);
		}
		break;
	}

	cmKey = cpKey;

}


bool CCSButtonCtrl::Recess()
{
    return bmIsRecessed;
}


BEGIN_MESSAGE_MAP(CCSButtonCtrl, CButton)
    //{{AFX_MSG_MAP(CCSButtonCtrl)
	ON_WM_ERASEBKGND()
	ON_WM_RBUTTONDOWN()
    ON_WM_GETDLGCODE()
    ON_WM_LBUTTONUP()
    ON_WM_LBUTTONDOWN()
    ON_WM_KILLFOCUS()
	ON_WM_ENABLE()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
    ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()





void CCSButtonCtrl::SetColors(COLORREF lpBtnFace,COLORREF lpBtnShadow,COLORREF BtnHilite)
{
	m_BtnFace =  lpBtnFace;
    m_BtnShadow = lpBtnShadow;
    m_BtnHilite = BtnHilite;
	Invalidate();

}

UINT CCSButtonCtrl::OnGetDlgCode()
{
    // Disallow the button for being a default button.
    // Since Windows has some bugs when tab processing on a a focused button
    // has a discrepancy. (It's usually draw that push button as a default
    // button, and does not clear that button to be normal again.)
    //
    return CButton::OnGetDlgCode() & ~(DLGC_DEFPUSHBUTTON | DLGC_UNDEFPUSHBUTTON);
}

void CCSButtonCtrl::OnLButtonUp(UINT nFlags, CPoint point)
{
    CButton::OnLButtonUp(nFlags, point);
    CButton::SetState(bmIsRecessed);    // redraw button
}                                          



void CCSButtonCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
    CButton::OnLButtonDown(nFlags, point);
    GetParent()->SendMessage(WM_BUTTON_CLICK,nFlags,(LPARAM) this);
}







void CCSButtonCtrl::OnKillFocus(CWnd* pNewWnd)
{
    CButton::SetState(false);   // allows OnKillFocus() to work correctly
    CButton::OnKillFocus(pNewWnd);
    CButton::SetState(bmIsRecessed);    // redraw button
}



/////////////////////////////////////////////////////////////////////////////
// CCSButtonCtrl message handlers

void CCSButtonCtrl::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{   
		//CButton::DrawItem(lpDIS);

    CDC *pDC = CDC::FromHandle(lpDIS->hDC);
    HDC dc = lpDIS->hDC;
    
    UINT state = lpDIS->itemState;
    CRect rect(&(lpDIS->rcItem));

    

	COLORREF    clrBtnFace =   m_BtnFace;
    COLORREF    clrBtnShadow = m_BtnShadow;
    COLORREF    clrBtnHilite = m_BtnHilite;
    
    HBRUSH      hbrBtnFace = ::CreateSolidBrush(clrBtnFace);
    HBRUSH      hbrBtnHilite = ::CreateSolidBrush(clrBtnHilite);
    HBRUSH      hbrBtnShadow = ::CreateSolidBrush(clrBtnShadow);
    
    
    // Fill the entire button with the button brush (gray)
    HBRUSH hOldBrush = (HBRUSH) ::SelectObject(dc, hbrBtnFace );
    
    CRect InnerRect = rect;
	InnerRect.InflateRect(-1,-1);
//MWO
//    InnerRect.InflateRect(-2,-2);
    FillRect(dc, InnerRect, hbrBtnFace);
    
    // Draw the outer border first (thick if we're focused)
    BOOL bThick = state & ODS_FOCUS;
    
    DrawOuterFrame(dc, rect, false /* bThick */);

	// disabled buttons
    if (state & ODS_DISABLED)
	    pDC->FillRect(&InnerRect, &CBrush(m_BtnFaceDisabled));
    
    // The rect is now inset from the border either 1 or 2 pixels
    // Draw the highlight and shadow next
    if (state & ODS_SELECTED)
    {
        HPEN shadowPen = ::CreatePen(PS_SOLID, 1, clrBtnShadow);
        HPEN OldPen = (HPEN) ::SelectObject(dc, shadowPen);

        MoveTo(dc, rect.left, rect.bottom-1);
         ::LineTo(dc, rect.left, rect.top);
        ::LineTo(dc, rect.right, rect.top);      
        ::SelectObject(dc, OldPen);
        ::DeleteObject(shadowPen);
    } else {
        // Button is up
//MWO
        HPEN hilitePen = ::CreatePen(PS_SOLID, 1, clrBtnHilite);

//        HPEN hilitePen = ::CreatePen(PS_SOLID, 2, clrBtnHilite);
        HPEN shadowPen = ::CreatePen(PS_SOLID, 1, clrBtnShadow);
        
        HPEN OldPen = (HPEN) ::SelectObject(dc, hilitePen);

        // Draw the hilite (white) part with the 2 pixel width pen
        //::Rectangle(dc, rect.left+1, rect.top+1, rect.right, rect.bottom);       
        //::Rectangle(dc, rect.left, rect.top, rect.right, rect.bottom);       
		MoveTo(dc, rect.left+1, rect.top);
			::LineTo(dc, rect.right-1, rect.top);
		MoveTo(dc, rect.left+1, rect.bottom-1);
			::LineTo(dc, rect.left+1, rect.top);
        // Draw the shadow on the bottom half
        ::SelectObject(dc, shadowPen);

	    MoveTo(dc, rect.left, rect.bottom-1);
        ::LineTo(dc, rect.right-1, rect.bottom-1);
        ::LineTo(dc, rect.right-1, rect.top-1);
        
//MWO        MoveTo(dc, rect.left+1, rect.bottom-2);
//        ::LineTo(dc, rect.right-2, rect.bottom-2);
//        ::LineTo(dc, rect.right-2, rect.top);
        
        ::SelectObject(dc, OldPen);
        ::DeleteObject(hilitePen);
        ::DeleteObject(shadowPen);
    }  
    
    
    // Calculate the width and height of the text
    CString text;
    GetWindowText(text);
    //CSize szText = GetTextExtent(dc, text, text.GetLength());
	SIZE rlSize;
	GetTextExtentPoint32(dc, text, text.GetLength(), &rlSize);
	CSize szText(rlSize); 

    CRect textRect = rect;
    //textRect.InflateRect(-((rect.Width() - szText.cx)/2), 
    //    -((rect.Height() - szText.cy)/2));
    textRect.InflateRect(imInflateRectPixelWidth, imInflateRectPixelHight);

    // Draw the text
    ::SetBkMode(dc, TRANSPARENT);
    //pDC->SetBkColor ( RGB ( 255, 255, 0));
    if (state & ODS_SELECTED)
        textRect.OffsetRect(1, 1);
    // Draw the text

	UINT ilAlign;
	if(GetButtonStyle() & BS_LEFTTEXT)
	{
		ilAlign = DT_LEFT;
	}
	else
	{
		ilAlign = DT_CENTER;
	}

	// text for disabled buttons
    if (state & ODS_DISABLED)
    {
		COLORREF olDisabledGray = ::GetSysColor(COLOR_GRAYTEXT);
        CRect olRectOffset = textRect;
        olRectOffset.OffsetRect(1, 1);

		pDC->SetTextColor(RGB ( 255, 255, 255)); //weiss
		pDC->DrawText(text, -1, olRectOffset, DT_SINGLELINE | ilAlign | DT_VCENTER | DT_NOCLIP);
				   
		pDC->SetTextColor(olDisabledGray);
		pDC->DrawText(text, -1, textRect, DT_SINGLELINE | ilAlign | DT_VCENTER | DT_NOCLIP);

	}
	else
		DrawText(dc, text, -1, textRect, DT_SINGLELINE | ilAlign | DT_VCENTER);

	if (bThick)
	{
    	HPEN blackPen = CreatePen(PS_DOT, 1, RGB(0,0,0));
	    HPEN olOldPen = (HPEN) ::SelectObject(dc, blackPen);
	    textRect.left--;
	    textRect.right += 2;
	   	DrawFocusRect(dc, &textRect);
	    SelectObject(dc, olOldPen);
	    DeleteObject(blackPen);
    }

  	SelectObject(dc, hOldBrush);
  	
    ::DeleteObject((HGDIOBJ) hbrBtnFace);
    ::DeleteObject((HGDIOBJ) hbrBtnShadow);
    ::DeleteObject((HGDIOBJ) hbrBtnHilite);
    
}

void CCSButtonCtrl::DrawOuterFrame(HDC dc, CRect& rect, bool bThick)
{
    HPEN blackPen = ::CreatePen(PS_SOLID, 1, RGB(0,0,0));
    HBRUSH brBlack = ::CreateSolidBrush(RGB(0,0,0));
    
    // Draw the first outside rectangle
    HPEN OldPen = (HPEN) ::SelectObject(dc, (HGDIOBJ)blackPen);
    
    // top side
    MoveTo(dc, rect.left+1, rect.top); 
    ::LineTo(dc, rect.right-1, rect.top);
    
    // right side
    MoveTo(dc, rect.right-1, rect.top+1);
    ::LineTo(dc, rect.right-1, rect.bottom-1);

    // bottom side  
    MoveTo(dc, rect.right-2, rect.bottom-1); 
    ::LineTo(dc, rect.left, rect.bottom-1);

    // left side    
    MoveTo(dc, rect.left, rect.bottom-2);
    ::LineTo(dc, rect.left, rect.top);

    rect.InflateRect(-1, -1);
    if (bThick)
    {
        ::FrameRect(dc, rect, brBlack);
        rect.InflateRect(-1, -1);
    }

    ::SelectObject(dc, OldPen);
    ::DeleteObject(brBlack);
    ::DeleteObject(blackPen);
}

BOOL CCSButtonCtrl::OnEraseBkgnd(CDC* pDC)
{
	// TODO: Add your message handler code here and/or call default
	return TRUE;
	
	//return CButton::OnEraseBkgnd(pDC);
}

void CCSButtonCtrl::OnRButtonDown(UINT nFlags, CPoint point) 
{
	SetFocus();

	long lParam = MAKELPARAM(point.x,point.y); 
	
    GetParent()->SendMessage(WM_CHART_RBUTTONDOWN,nFlags,(LPARAM) lParam);

    GetParent()->SendMessage(WM_BUTTON_RBUTTONDOWN,nFlags,(LPARAM) this);

}

//**************************************************************************
// class:			CTableButton
// derived from:	CCSButtonCtrl 
// Author:			MWO
//					29.04.1997
//**************************************************************************

//IMPLEMENT_DYNCREATE(CTableButton, CCSButtonCtrl)

CCSTableButton::CCSTableButton(int ipCell_X, int ipCell_Y, bool bpWithRecess)
			: CCSButtonCtrl(bpWithRecess)
{
	imTablePos_X = ipCell_X;
	imTablePos_Y = ipCell_Y;

}

CCSTableButton::~CCSTableButton()
{
}

BEGIN_MESSAGE_MAP(CCSTableButton, CCSButtonCtrl)
    //{{AFX_MSG_MAP(CCSTableButton)
    ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CCSTableButton::OnLButtonUp(UINT nFlags, CPoint point)
{
	CCSButtonCtrl::OnLButtonUp(nFlags, point);

	long lParam = MAKELPARAM(imTablePos_X, imTablePos_Y); 
	
    GetParent()->SendMessage(WM_TABLE_CTRL_BUTTONDOWN,nFlags,(LPARAM) lParam);

}                                          


CCSButtonCtrl::CCSButtonCtrl(const CCSButtonCtrl& s)
{
	imInflateRectPixelHight = -3;
	imInflateRectPixelWidth = -3;
}


const CCSButtonCtrl& CCSButtonCtrl::operator= ( const CCSButtonCtrl& s)
{
	if(this == &s)
	{
		return *this;
	}
	return *this;
}

void CCSButtonCtrl::SetInflateTextRectHight(int ipPixel)
{
	imInflateRectPixelHight = ipPixel;
}

void CCSButtonCtrl::SetInflateTextRectWidth(int ipPixel)
{
	imInflateRectPixelWidth = ipPixel;
}
