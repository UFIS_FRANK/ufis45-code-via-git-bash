// ccsddx.h
//
// Concept:
// In one application, there should be only one instance of this object.
// It was designed for 
//

#ifndef _CCSDDX
#define _CCSDDX

#include <afxwin.h>
#include <ccsptrarray.h>

#define NOTUSED -1


// type definition
typedef void (*DDXCALLBACK)(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: broadcast handling class
/*@Doc:
  This class handles all used broadcasts and therefore exists only once for each 
  application.
  Every broadcasttype possibly used is associated with a unique define. These 
  defines have to be made in the enum struct 'egDDXTypes' (see above).

*/
class CCSDdx: public CObject
{
public:
    //@ManMemo: Default constructor
	CCSDdx(void);
    //@ManMemo: Default destructor
	~CCSDdx(void);

	//@ManMemo: Data has been changed
	/*@Doc:
	  This method should be called from all Objects which change any global 
	  stored data
	*/
	void DataChanged(void *vpInstance, int ipDDXTypes, void *vpDataPointer );

	//@ManMemo: Register broadcasts
	/*@Doc:
	  For each broadcasttyp want to be notified when ProcessBroadcast() is in 
	  process, it has to register on this object, and specify the name of the 
	  table. The parameter "vpInstance" will be used to differentiate each caller 
	  of the same class.
	  Usually this method is called in the constructor of the classes which want
	  to register broadcasttyps. By calling this method for each new broadcasttyp
	  it is made known by the whole application.
	*/
	void Register(void *vpInstance, int ipDDXTypes, CString &DDXName, CString &InstanceName, DDXCALLBACK pfpCallBack);
	//@ManMemo: Unregister broadcasts
	/*@Doc:
	  By calling this method for one special broadcasttyp it can be unregistered
	  and is made unknown for the whole application.
	*/
	void UnRegister(void *vpInstance, int ipDDXTypes);
	//@ManMemo: Stop broadcasting
	/*@Doc:
	  All existing registrations for bradcasts are unregistered now. Therefore
	  no more broadcasting is possible for the moment until new registrations are
	  made.
	*/
	void UnRegisterAll(void);

// Internal attributes
private:
	
	// For each data change, scan "DDXType" in this table
	class DDXCallBackEntry
	{
	public:
        DDXCallBackEntry(){};
        ~DDXCallBackEntry(){};
        void *Instance;
		CString  InstanceName;
		CString  DDXName;
		int DDXType;
		DDXCALLBACK CallBack;
	};
	CCSPtrArray <DDXCallBackEntry> DDXCallBackTable;

	void DelFromKeyMap(DDXCallBackEntry *polDDXRegistration);	
	void AddToKeyMap(DDXCallBackEntry *polDDXRegistration);	

	CMapPtrToPtr KeyMap;

};


#endif
