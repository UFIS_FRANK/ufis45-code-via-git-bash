// CCSEdit.cpp : implementation file
//
//	RKR 19042001:	::SetWindowText(CString opText) added
//					::PreTranslateMessage(MSG* pMsg) added
//					PRF 1357 PRODUCT4.4: reset the origin text when ESC
//
//					::OnChange(); pomParent->SendMessage(WM_EDIT_CHANGED, imID, 0L);
//					reaktivated.

#include <stdafx.h>
#include <CCSEdit.h>
#include <CCSTable.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


int CCSEdit::imMaxID = 1;


CCSEdit::CCSEdit() : imID(++imMaxID)
{
	Initialize();
}


void CCSEdit::Initialize(void) 
{
	omBKColor = RGB(255,255,255); /*White*/
	omTextColor = RGB(0,0,0);	/*Black*/
	omTextChangedColor = RGB(0,128,0);	/*Green*/
	omTextErrColor = RGB(128,0,0);	/*Dunkelrot*/
	pomParent = NULL;
	pomTableWnd = NULL;
	pomTextFont = NULL;
	omFormatString = "";
	imRangeFrom = -32767;
	imRangeTo = 32767;
	imType = KT_STRING;
	imFloatIntPart = 3;
	imFloatFloatPart = 3;
	bmShift = false;
	bmStatus = true;
	imMinLength = 0;
	imMaxLength = 100;
	bmAutoMaxLength = false;
	bmAutoMinLength = false;
	bmEmptyIsValid = false;
	bmIsKeyDown = false;
	bmDateRequired = false;
	bmTimeChangeDay =  false;
	bmIPEdit = false;  
	bmInit = false;  
	bmChanged = false;
	imLineNo = -1;
	imColumnNo = -1;
	bmUserStatus = false;
	bmStatusByUser = false;
	bmDisabled = false;
	bmTest = false;
	bmTableKillFocus = true;
	bmReadOnly = false;
	bmUserReadOnly = false;
	bmIsKillFocus = false;
	cmKey = '?';
//	omBrush.CreateSolidBrush(RGB(192,192,192));
	omBrush.CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));
	bmDefaultMenu = true;
	bmRegularExpression = false; // MWO 31.03.00
	omName = "";
	bmBKColor = true;		
	bmErrorFlag = false;	
	bmDisabledBKColor = false;

}


CCSEdit::CCSEdit(CRect &popRect,
			     CWnd *popParent /*NULL*/,  
				 CFont *popFont /*NULL*/,
				 int ipMinLength /*0*/,
				 int ipMaxLength /*100*/,
				 bool bpIPEdit,
				 UINT nID  /*0*/) : imID(++imMaxID)
{
	Initialize();
	bmIPEdit = bpIPEdit;
	imMinLength = ipMinLength;
	imMaxLength = ipMaxLength;
	pomTextFont = popFont;
	pomParent = popParent;
	pomTableWnd = NULL;
	if(pomParent == NULL)
		pomParent = GetParent();

	pomTextFont = popFont;
	omRect = popRect;
	if(ipMaxLength == -1)
	{
		bmAutoMaxLength = true;
		imMinLength = 2;
	}
	if(ipMaxLength == -1)
	{
		bmAutoMinLength = true;
		imMaxLength = 10;
	}

	DWORD dwStyle;
	if(bmDisabled)
		dwStyle |= ~WS_POPUP | WS_CHILD | WS_BORDER; //| WS_EX_NOPARENTNOTIFY | WS_THICKFRAME | WS_BORDER;//| WS_TABSTOP;//WS_VSCROLL | WS_CLIPSIBLINGS;
	else
//		dwStyle |= ~WS_POPUP | WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER | ES_AUTOHSCROLL; //| WS_EX_NOPARENTNOTIFY | WS_THICKFRAME | WS_BORDER;//| WS_TABSTOP;//WS_VSCROLL | WS_CLIPSIBLINGS;
		dwStyle |=  WS_VISIBLE | WS_TABSTOP | WS_BORDER | ES_AUTOHSCROLL; //| WS_EX_NOPARENTNOTIFY | WS_THICKFRAME | WS_BORDER;//| WS_TABSTOP;//WS_VSCROLL | WS_CLIPSIBLINGS;


	Create(dwStyle, popRect, popParent, nID);
//	CEdit::CreateEx(WS_EX_CLIENTEDGE   , "", "", dwStyle, popRect, popParent, nID);

	CEdit::LimitText(imMaxLength);
	if(pomTextFont != NULL)
		SetFont(pomTextFont);
}


CCSEdit::CCSEdit(CRect &popRect, 
				 CWnd *popParent, 
				 CWnd *popTableWnd, 
				 CFont *opFont,
				 bool bpIPEdit,
				 CCSEDIT_ATTRIB *prpAttrib)  : imID(++imMaxID)
{
	Initialize();
	bmIPEdit = bpIPEdit;
	pomTextFont = opFont;
	pomParent = popParent;
	pomTableWnd = popTableWnd;
	pomTextFont = opFont;
	omRect = popRect;
	DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_EX_NOPARENTNOTIFY | WS_BORDER |  WS_EX_CLIENTEDGE| WS_TABSTOP | ES_AUTOHSCROLL | ES_LEFT;
	if(prpAttrib != NULL)
	{
		omTextErrColor	= prpAttrib->ErrColor;
		imType			= prpAttrib->Type;
		dmRangeFrom		= prpAttrib->dRangeFrom;
		dmRangeTo		= prpAttrib->dRangeTo;
		imFloatIntPart	= prpAttrib->FloatIntPart;
		imFloatFloatPart = prpAttrib->FloatFloatPart;
		imMaxLength		= prpAttrib->TextMaxLenght;
		imMinLength		= prpAttrib->TextMinLenght;
		imRangeFrom		= prpAttrib->RangeFrom;
		imRangeTo		= prpAttrib->RangeTo;
		omFormatString	= prpAttrib->Format;
		bmChanged		= prpAttrib->IsChanged;
		bmDateRequired =  prpAttrib->Required;
		bmTimeChangeDay =  prpAttrib->ChangeDay;
		dwStyle = dwStyle | prpAttrib->Style;
		if(imType == KT_STRING)
			CreateFormatList(omFormatString);
	}


	Create(dwStyle, popRect, popParent, 0);
//	CEdit::CreateEx(WS_EX_CLIENTEDGE   , "", "", dwStyle, popRect, popParent, 0);
	bmDefaultMenu = false;
	if(pomTextFont != NULL)
		SetFont(pomTextFont);
	CEdit::LimitText(imMaxLength);
    ShowWindow(SW_SHOW);
	SetFocus();
}


bool CCSEdit::Create( DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID)
{
//	if(CEdit::CreateEx(WS_EX_CLIENTEDGE   , "", "", dwStyle, rect, pParentWnd, nID)== false)
	if(CEdit::Create( dwStyle, rect, pParentWnd, nID ) == false)
	{
		return false;
	}
	if(imMaxLength > 0) LimitText(imMaxLength);
	if(pomTextFont != NULL)	SetFont(pomTextFont);
	/*
	 * <<< cla
	 * Assign the this pointer to parents CWnd class
	 * Necessary to get CCSEdit work as an ActiveX Control
	 */
	pomParent = pParentWnd;
	return true;
}



CCSEdit::~CCSEdit()
{
	omAllFormats.DeleteAll();
	omFormatList.DeleteAll();
	omBrush.DeleteObject();

	omMenuItems.DeleteAll();
}


BEGIN_MESSAGE_MAP(CCSEdit, CEdit)
	//{{AFX_MSG_MAP(CCSEdit)
	ON_WM_KEYDOWN()
	ON_WM_CHAR()
	ON_WM_KEYUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
    ON_WM_LBUTTONDBLCLK()
	ON_CONTROL_REFLECT(EN_CHANGE, OnChange)
	ON_CONTROL_REFLECT(EN_SETFOCUS, OnSetfocus)
	ON_CONTROL_REFLECT(EN_KILLFOCUS, OnKillfocus)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_ENABLE()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
    ON_WM_MOUSEMOVE()
	ON_COMMAND_RANGE( 100, 200, OnMenuSelect )	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP() 

/////////////////////////////////////////////////////////////////////////////
// CCSEdit message handlers

void CCSEdit::OnEnable(BOOL bEnable) 
{

	if( (!bmDisabled && (bEnable == TRUE)) || (bmDisabled && (bEnable == FALSE)) )
		return;
	
	CEdit::OnEnable(bEnable);

	if(bEnable == TRUE)
		bmDisabled = false;
	else
		bmDisabled = true;

	InvalidateRect(NULL);
	UpdateWindow();

}

BOOL CCSEdit::ShowWindow( int nCmdShow )
{
	if(cmKey == '-')
		return FALSE;

	return CEdit::ShowWindow( nCmdShow );
}


void CCSEdit::EnableWindow(BOOL bpEnable)
{
	if(cmKey == '0' || cmKey == '-')
		return;

	OnEnable(bpEnable);
}

void CCSEdit::SetSecState(char cpKey)
{
	if(cpKey == cmKey)
		return;

	switch(cpKey)
	{
	case '1': // Licens to edit
			bmDisabled = false;
			bmReadOnly = false;
		break;
	case '0': //Readonly
		{
			bmDisabled = false;
			bmReadOnly = true;
			ModifyStyle(WS_TABSTOP, 0,0);
		}
		break;
	case '-': //Hide
		{
			bmDisabled = true;
			bmReadOnly = true;
			CEdit::GetWindowText(omTextSave);
			ModifyStyle(WS_TABSTOP | WS_VISIBLE, 0, 0);
		}
		break;
	default: //Error
		{
			ModifyStyle(ES_READONLY, WS_VISIBLE, 0);
			bmDisabled = false;
			bmReadOnly = false;
			omBKColor = RGB(0,0,0);
		}
		break;
	}
	cmKey = cpKey;
	InvalidateRect(NULL);
	UpdateWindow();
}

void CCSEdit::SetDefaultMenu(void)
{
	bmDefaultMenu = true;
}

void CCSEdit::UnsetDefaultMenu(void)
{
	bmDefaultMenu = false;
}


void CCSEdit::SetReadOnly(bool bpSet)
{
	bmDisabled = false;
	if(bpSet)
	{
		ModifyStyle(WS_TABSTOP, 0,0);
		//ModifyStyle(WS_TABSTOP, ES_READONLY,0);
		bmUserReadOnly = true;
	}
	else
	{
		ModifyStyle(0, WS_TABSTOP,0);
		//ModifyStyle(ES_READONLY, WS_TABSTOP,0);
		bmUserReadOnly = false;
	}
}



void CCSEdit::SetInitText(CString opText, bool bpChangedColor)
{
	bmUserStatus = false;
	bmStatusByUser = false;
	bmKillFocus = false;
	bmInit = !bpChangedColor;
	SetWindowText(opText);
	bmStatus = CheckAll();
	bmInit = false;
	bmChanged = bpChangedColor;
}


bool CCSEdit::IsChanged()
{
	return bmChanged;
}


void CCSEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if(nChar == VK_RETURN)
	{
		if(pomParent != NULL)
			pomParent->SendMessage(WM_EDIT_RETURN, imID, nFlags);
	}
	//TRACE("\n KeyDown -> %d\n", nChar);
	if(bmReadOnly || bmDisabled || bmUserReadOnly)
		return;

	unsigned short ilRet;
	if(bmIPEdit)
	{
		bmTableKillFocus = false;
		switch(nChar)
		{
		case VK_TAB:
			ilRet = GetKeyState(VK_SHIFT);
			if((GetKeyState(VK_SHIFT) & 0xff81) && (ilRet != 1))
			{
				bmKillFocus = true;
				OnKillfocus();
				if(pomTableWnd != NULL)
					pomTableWnd->SendMessage(WM_EDIT_MOVE_IP_LEFT, imID, CheckAll()? TRUE : FALSE);
			}
			else 
			{
				bmKillFocus = true;
				OnKillfocus();
				if(pomTableWnd != NULL)
					pomTableWnd->SendMessage(WM_EDIT_MOVE_IP_RIGHT, imID, CheckAll()? TRUE : FALSE);
			}
			return;
			break;
		case VK_UP:
			bmKillFocus = true;
			OnKillfocus();
			if(pomTableWnd != NULL)
				pomTableWnd->SendMessage(WM_EDIT_MOVE_IP_UP, imID, CheckAll()? TRUE : FALSE);
			return;
			break;
		case VK_DOWN:
			bmKillFocus = true;
			OnKillfocus();
			if(pomTableWnd != NULL)
				pomTableWnd->SendMessage(WM_EDIT_MOVE_IP_DOWN, imID, CheckAll()? TRUE : FALSE);
			return;
			break;
		case VK_RETURN:
			bmKillFocus = true;
			OnKillfocus();
			if(pomTableWnd != NULL)
				pomTableWnd->SendMessage(WM_EDIT_IP_END, imID, CheckAll()? TRUE : FALSE);
			return;
			break;
		default:
			break;
		}
		bmTableKillFocus = true;
	}	
	/////////////////////////////////////
	CEdit::OnKeyDown(nChar, nRepCnt, nFlags);

	//if(nChar == 16) //SHIFT
	//	bmShift = true;
	

	
	bmIsKeyDown = true;

	if(nChar == 8)  // Backspace
	{
		if(omCurrentPath.GetSize() > 0)
			omCurrentPath.RemoveAt(omCurrentPath.GetSize() - 1);
		GetCurrentMinMaxLength();
		bmChanged = true;
		bmInit = false;
		return; // Backspace
	}

	/*	
	TRACE("\n\n OLD %s",omOldText);
	TRACE("\n PRE %s",omPreCursorText);
	TRACE("\n AFT %s\n",omAfterCursorText);
	*/
}



void CCSEdit::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if(bmReadOnly || bmDisabled || bmUserReadOnly)
		return;
	//TRACE("\n KeyUp -> %d\n", nChar);
	CEdit::OnKeyUp(nChar, nRepCnt, nFlags);
}



void CCSEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if(bmReadOnly || bmDisabled || bmUserReadOnly)
		return;
	CEdit::OnChar( nChar, nRepCnt, nFlags );
	//if(nChar == 8) return; // Backspace
}



void CCSEdit::OnChange() 
{
	if(bmReadOnly || bmDisabled || bmUserReadOnly)
		return;

	bmChanged = true;


	CCSEDITNOTIFY rlNotify;
	rlNotify.SourceControl = this;
	rlNotify.Text = omOldText;
	rlNotify.Status = bmStatus;
	rlNotify.IsChanged = bmChanged;
	
	
	
	if(!bmInit)
	{
		if(pomTableWnd != NULL)
		{
			pomTableWnd->SendMessage(WM_EDIT_CHANGED, imID, (LPARAM)&rlNotify);
		}

		if(pomParent == NULL)
			pomParent = GetParent();
		
		if(pomParent != NULL)
		{
			//rkr19042001 need this message (reaktivated)
			pomParent->SendMessage(WM_EDIT_CHANGED, imID, (LPARAM)&rlNotify);

			pomParent->SendMessage(EN_CHANGE, imID, (LPARAM)&rlNotify);
		}
	}


	if(bmTest)
	{
		SetSel(imPos, imPos);
		InvalidateRect(NULL);
		UpdateWindow();

		bmTest = false;
		return;
	}


	GetSel(imCurrentPosAnf, imCurrentPosEnd ) ;
	GetWindowText(omNewText);
	
	omAfterCursorText = omNewText.Right(omNewText.GetLength() - imCurrentPosEnd);
	omPreCursorText = omNewText.Left(imCurrentPosEnd);

	omInsertText = omPreCursorText;


	CheckField();


	InvalidateRect(NULL);
	UpdateWindow();

	CString olVaidatedText = omInsertText + omAfterCursorText; 

	if(olVaidatedText != omNewText)
	{
		imPos = omInsertText.GetLength();
		bmTest = true;
		//rkr19042001 call basicfunction because we won�t store the text
		CWnd::SetWindowText(olVaidatedText);
		//SetWindowText(olVaidatedText);
		// Sets the Cursor Position to End  
		SetSel(imPos, imPos);	
		bmTest=false;	
	}	
			
}



void CCSEdit::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if(bmReadOnly || bmDisabled || bmUserReadOnly)
		return;
	CEdit::OnLButtonDown(nFlags, point);

	CString olText;

	GetWindowText(olText);

	CCSEDITNOTIFY rlNotify;
	rlNotify.SourceControl = this;
	rlNotify.Text = olText;
	rlNotify.Status = bmStatus;
	rlNotify.IsChanged = bmChanged;
	rlNotify.Flags = nFlags;

	if(pomParent == NULL)
		pomParent = GetParent();
	if(pomParent != NULL)
		pomParent->SendMessage(WM_EDIT_LBUTTONDOWN, imID, (LPARAM)&rlNotify);


}

void CCSEdit::SetName(CString opName)//Name of the EditField
{
	omName = opName;
}

CString& CCSEdit::Name()//Name of the EditField
{
	return omName;
}



HBRUSH CCSEdit::CtlColor(CDC* pDC, UINT nCtlColor) 
{

	if(bmIPEdit && bmBKColor)	// Added bmBkColor Flag
		return NULL;

	HBRUSH hbr = 0;
	
	if(bmReadOnly || bmDisabled || bmUserReadOnly)
	{


//		pDC->SetTextColor(RGB(128,128,128));
		pDC->SetTextColor(RGB(0,0,0));
/*		
		if(omBKColor == RGB(255,255,255))
			omBKColor = RGB(192,192,192);

		if(omBKColor == RGB(192,192,192))
			pDC->SetTextColor(RGB(0,0,0));
		else
			pDC->SetTextColor(RGB(128,128,128));
*/		

//		pDC->SetBkColor(RGB(192,192,192));

		if(bmDisabledBKColor) 
			pDC->SetBkColor(omBKColor);
		else
			pDC->SetBkColor(::GetSysColor(COLOR_BTNFACE));


		omBrush.DeleteObject();
//		omBrush.CreateSolidBrush(RGB(192,192,192));
		omBrush.CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));
		hbr = omBrush;	
		return hbr;
	}

	/*
	if(bmDisabled)
	{
		hbr = CreateSolidBrush(RGB(192,192,192));
		pDC->SetBkColor(RGB(192,192,192));
		pDC->SetTextColor(RGB(192,192,192));
		return hbr;
	}
*/
	

	long llRc;
	COLORREF olCol  = omTextColor;
	CString olStr;


	GetWindowText(olStr);

	omBrush.DeleteObject();
	omBrush.CreateSolidBrush(omBKColor);
	hbr = omBrush;	

	llRc = pDC->SetBkColor(omBKColor);

	if((bmChanged) && (!bmInit))
		olCol = omTextChangedColor;

	if(bmKillFocus)
	{

		if(!bmStatus)
		{
			olCol = omTextErrColor;
		}
		else
		{
			if(bmStatusByUser && !bmUserStatus)
			{
				olCol = omTextErrColor;
			}
		}
		
	}


	if(bmStatusByUser && !bmUserStatus)
	{
		olCol = omTextErrColor;
	}

	llRc = pDC->SetTextColor(olCol);

	return hbr;
}



void CCSEdit::OnKillfocus() 
{
	if(bmIsKillFocus)
		return;

	bmIsKillFocus = true;

	bmShift = false;
	bmKillFocus = true;
	bmStatus = CheckAll();

	CString olStr;
	CCSEDITNOTIFY rlNotify;
	rlNotify.SourceControl = this;
	rlNotify.Text = omOldText;
	rlNotify.Status = bmStatus;
	rlNotify.IsChanged = bmChanged;

	GetWindowText(olStr);



	if(pomParent == NULL)
		pomParent = GetParent();

	if(pomTableWnd != NULL)
	{
		pomTableWnd->SendMessage(WM_EDIT_KILLFOCUS, imID, (LPARAM)&rlNotify);
		bmUserStatus = rlNotify.Status;
		bmStatusByUser = rlNotify.UserStatus;
		if(olStr != rlNotify.Text)
		{
			//rkr19042001 call basicfunction because we won�t store the text
			CWnd::SetWindowText(rlNotify.Text);
			//SetWindowText(rlNotify.Text);
			InvalidateRect(NULL,true);
		}
		if(bmTableKillFocus)
		{
			pomTableWnd->SendMessage(WM_EDIT_IP_END, imID, CheckAll()? TRUE : FALSE);
		}
	}
	else
	{
		if(pomParent != NULL)
		{
			pomParent->SendMessage(WM_EDIT_KILLFOCUS, imID, (LPARAM)&rlNotify);
			bmUserStatus = rlNotify.Status;
			bmStatusByUser = rlNotify.UserStatus;
		}
		InvalidateRect(NULL,true);
	}

	//TRACE("\n Killfocus: Status: %d",bmStatus);

}

void CCSEdit::SetStatus(bool bpStatus)
{
	bmUserStatus = bpStatus;
	bmStatusByUser = true;
	InvalidateRect(NULL);
	UpdateWindow();
}

COLORREF CCSEdit::GetTextColor()
{
	COLORREF olCol;
	CString olStr;

	if((bmChanged) && (!bmInit))
		olCol = omTextChangedColor;
	else
		olCol = omTextColor;


	GetWindowText(olStr);

	if((olStr.GetLength() < imMinLength) || (olStr.GetLength() > imMaxLength))
	{
		bmStatus = false;
	}
	if(!bmStatus)
	{
		olCol = omTextErrColor;
	}
	else
	{
		if(bmStatusByUser && !bmUserStatus)
		{
			olCol = omTextErrColor;
		}
	}
	return olCol;
}



bool CCSEdit::CheckAll() 
{
	GetWindowText(omOldText);
	omNewText = omOldText;
		
	omAfterCursorText = "";
	omPreCursorText = "";
	omInsertText = omNewText;
	//bmStatus = CheckField();

	if(bmEmptyIsValid && omNewText.IsEmpty())
		return true;

	bool blRet = true;

// MWO 31.03.00
	if(bmRegularExpression == true)
	{
		blRet = CheckRegMatch(omNewText);
		if(blRet && ((omNewText.GetLength() > imMaxLength) || (omNewText.GetLength() < imMinLength)))
			blRet = false;
	}
	else
	{
// END MWO
		switch(imType)
		{
		case KT_INT:
			blRet = IntCheck();
			break;
		case KT_DOUBLE:
			blRet = DoubleCheck();
			break;
		case KT_DATE:
				blRet = DateCheck();
			break;
		case KT_TIME: 
				blRet = TimeCheck();
			break;
		case KT_STRING:
			blRet = StringCheck();
			GetCurrentMinMaxLength();
			if(blRet && ((omNewText.GetLength() > imMaxLength) || (omNewText.GetLength() < imMinLength)))
				blRet = false;
			break;
		case KT_MONEY:
			blRet = DoubleCheck();
			break;
		default:
			break; // Do nothing
		}
	}
	bmStatus = blRet;
	
	return blRet;
}




void CCSEdit::OnSetfocus() 
{
	bmKillFocus = false;
	bmIsKillFocus = false;
	bmStatusByUser = false;
}


//**********************************************************************
// Routines for manipulating the edit control
//**********************************************************************

	
void CCSEdit::SetBKColor(COLORREF opColor)
{
	omBKColor = opColor;
	InvalidateRect(NULL,true);
}


// Sets the Back ground color of the Edit field with the input color.
void CCSEdit::SetBackGrColor(COLORREF opColor)
{
	bmBKColor = false;
	SetBKColor(opColor);
}

// Sets the Error Flag to retain the Valid data.
void CCSEdit::SetErrorFlag()
{
	bmErrorFlag = true;
}



void CCSEdit::SetTextColor(COLORREF opColor)
{
	omTextColor = opColor;
	InvalidateRect(NULL,true);
}
	
void CCSEdit::SetTextErrColor(COLORREF opColor)
{
	omTextErrColor = opColor;
	InvalidateRect(NULL,true);
}


					
void CCSEdit::SetTypeToDouble(int ipPrePoint /*3*/, int ipAfterPoint/*3*/, double dpFrom/*-999999.0*/, double dpTo/*999999.0*/,bool bpEmptyIsValid)
{
	imType = KT_DOUBLE;
	dmRangeFrom = dpFrom;
	dmRangeTo = dpTo;
	imFloatIntPart = ipPrePoint;
	imFloatFloatPart = ipAfterPoint;
	imMaxLength = imFloatIntPart + imFloatFloatPart + 1;
	SetTextLimit(imMaxLength);
	this->bmEmptyIsValid = bpEmptyIsValid;
}

void CCSEdit::SetTypeToMoney(int ipPrePoint /*3*/, int ipAfterPoint/*2*/, double dpFrom/*-999999.0*/, double dpTo/*999999.0*/)
{
	imType = KT_MONEY;
	dmRangeFrom = dpFrom;
	dmRangeTo = dpTo;
	imFloatIntPart = ipPrePoint;
	imFloatFloatPart = ipAfterPoint;
	imMaxLength = imFloatIntPart + imFloatFloatPart + 1;
	SetTextLimit(imMaxLength);
}


void CCSEdit::SetPrecision(int ipPrePoint, int ipAfterPoint)
{
	imFloatIntPart = ipPrePoint;
	imFloatFloatPart = ipAfterPoint;
	imMaxLength = imFloatIntPart + imFloatFloatPart + 1;
	SetTextLimit(imMaxLength);
}

void CCSEdit::SetRange(double dpFrom, double dpTo)
{
	dmRangeFrom = dpFrom;
	dmRangeTo = dpTo;
}

void CCSEdit::SetTypeToInt(int ipFrom/*-999999*/, int ipTo/*999999*/,bool bpEmptyIsValid)
{
	imType = KT_INT;
	imRangeFrom = ipFrom;
	imRangeTo = ipTo;

	char buffer1[20];
	char buffer2[20];
	itoa(abs(ipFrom), buffer1, 10);
	itoa(abs(ipTo), buffer2, 10);

	CString olTmp1(buffer1), olTmp2(buffer2);

	if(olTmp1.GetLength() < olTmp2.GetLength()) 
		imMaxLength = olTmp2.GetLength();
	else
		imMaxLength = olTmp1.GetLength();
	CEdit::LimitText(imMaxLength);

	this->bmEmptyIsValid = bpEmptyIsValid;
}

void CCSEdit::SetRange(int ipFrom, int ipTo)
{
	imRangeFrom = ipFrom;
	imRangeTo = ipTo;
}


void CCSEdit::SetTypeToTime(bool bpRequired, bool bpChangeDay)
{
	bmDateRequired = bpRequired;
	bmTimeChangeDay =  bpChangeDay;
	imType = KT_TIME;
	//imMaxLength = 5;
	//CEdit::LimitText(imMaxLength);
}

void CCSEdit::SetTypeToDate(bool bpRequired)
{
	bmDateRequired = bpRequired;
	imType = KT_DATE;
}

void CCSEdit::SetTextLimit(int ipMinLength, int ipMaxLength, bool bpEmptyIsValid)
{
	bmEmptyIsValid = bpEmptyIsValid;
	bmAutoMaxLength = false;
	bmAutoMinLength = false;
	imMaxLength = ipMaxLength;
	imMinLength = ipMinLength;
	if(ipMaxLength == -1)
	{
		bmAutoMaxLength = true;
		imMinLength = 0;
	}
	if(ipMaxLength == -1)
	{
		bmAutoMinLength = true;
		imMaxLength = 100;
	}
	
	if(imType == KT_STRING)
	{
		CEdit::LimitText(imMaxLength);
	}
}

void CCSEdit::SetFormat(CString opFormat)
{
	bmAutoMaxLength = true;
	bmAutoMinLength = true;
	omFormatString = opFormat;
	if(imType == KT_STRING)
		CreateFormatList(omFormatString);
}

// set the regular expression
void CCSEdit::SetRegularExpression(CString opExpression)
{
	omRegularExpression = opExpression;
	bmRegularExpression = true;
}
// check the current value against the regular expression
// anf return true or false (matched or not matched)
bool CCSEdit::CheckRegMatch(CString opValue)
{
	bool blRet = true;
	int flags = match_not_null;
	if(!opValue.IsEmpty() && !omRegularExpression.IsEmpty())
	{
		RegEx olRegEx(omRegularExpression, true);
		blRet = olRegEx.Match(opValue);
	}
	return blRet;
}

void CCSEdit::SetEditFont(CFont *popFont)
{
	pomTextFont = popFont;
	if(pomTextFont != NULL)
		SetFont(pomTextFont);

}


void CCSEdit::SetTypeToString(CString opFormat, int ipMaxLength, int ipMinLength)
{
	omFormatString = opFormat;
	imType = KT_STRING;
	SetTextLimit( ipMinLength, ipMaxLength);
	CreateFormatList(omFormatString);
}

bool CCSEdit::GetStatus()
{
	// Return False if the bmErrorFlag is True
	// This is required when the user enteres invalid data in the fields 
	// In this case Old valid data has to be retained 
	if(bmErrorFlag)	
	{
		bmErrorFlag = false;
		return false;
	}

	bmStatus = CheckAll();

	if(bmStatus)
	{
		if(	bmStatusByUser && !bmUserStatus)
			return bmUserStatus;
	}

	return bmStatus;
}

//**********************************************************************
// Routines for check the text
//**********************************************************************


bool CCSEdit::CheckField()
{
	bool blRet = true;
	switch(imType)
	{
	case KT_INT:
		GetCurrentMinMaxLength();
		break;
	case KT_DOUBLE:
		{
			//blRet = DoubleCheck();
		}
		break;
	case KT_DATE:
		// the on and only validation is made by OnKillFocus
		break;
	case KT_TIME: 
		// the on and only validation is made by OnKillFocus
		break;
	case KT_STRING:
			blRet = StringCheck();
			GetCurrentMinMaxLength();
		break;
	case KT_MONEY:
		{
			//blRet = DoubleCheck();
		}
		break;
	default:
		break; // Do nothing
	}
	return blRet;
}


bool CCSEdit::IntCheck()
{
	char clTmp;
	int ilOffSet = omPreCursorText.GetLength();

	if (omNewText.GetLength() == 0 && this->bmEmptyIsValid == false)
		return false;

	for(int ilLc = 0; ilLc < omNewText.GetLength(); ilLc++)
	{
		clTmp = omNewText[ilLc];
		if(ilLc == 0)
		{
			int ilMaxLength = imMaxLength;
			if(clTmp == '-')
			{
				++ilMaxLength;
				CEdit::LimitText(ilMaxLength);
			}
			else
			{
				CEdit::LimitText(ilMaxLength);
			}
		}
		
		if(!((isdigit(clTmp)) || ((clTmp == '-') && (ilLc == 0) && (imRangeFrom < 0))))
		{
			if((ilLc - ilOffSet) >= 0)
				omInsertText = omInsertText.Left(ilLc - ilOffSet);
			
			return false;
		}
	}


	if((atoi((LPCSTR)omNewText) < imRangeFrom) || (atoi((LPCSTR)omNewText) > imRangeTo)) 
	{
		omInsertText = "";
		return false;
	}
	else
		return true;

}

bool CCSEdit::DoubleCheck()
{

	char clTmp;
	bool blNeg = false;; 
	int ilPrePoint;
	int ilAfterPoint;
	//char buffer[64];
	bool blRet = true;

	
	if (omNewText.GetLength() == 0 && this->bmEmptyIsValid == false)
		return false;

	CString olStr = omNewText;


	if(olStr.Find(',') >= 0)
		olStr.SetAt(olStr.Find(','), '.');

	double dlValue = atof((LPCSTR)olStr);

	if((dlValue < dmRangeFrom) || (dlValue > dmRangeTo)) 
		return false;


	if(olStr.Find(',') >= 0)
		return false;

	if(olStr.Find('.') != olStr.ReverseFind('.')) 
	{
		return false;
	}

	if(olStr.Find('-') != olStr.ReverseFind('-')) 
	{
		return false;
	}


	if(omNewText.Find('-') >= 0)
		blNeg = true;


	for(int ilLc = olStr.GetLength() - 1; ilLc >= 0; ilLc--)
	{
		clTmp = olStr[ilLc];
		
		if(!((isdigit(clTmp)) || (clTmp == '-') || (clTmp == '.')))
		{
			return false;
		}
	}
	


	int ilPoint = olStr.Find('.');


	if(ilPoint < 0)
	{
		ilPrePoint = olStr.GetLength();

		if(blNeg)
			ilPrePoint--;

		if(ilPrePoint > imFloatIntPart)
			return false;

	}
	else
	{
		CString olTmp;

		olTmp = olStr.Left(ilPoint);

		ilPrePoint = olTmp.GetLength();

		if(olStr.GetLength() > ilPoint + 1)
		{
			olTmp = olStr.Mid(ilPoint + 1);
		}
		/*
		else
		{
			olTmp = olStr;
		}
		*/

		ilAfterPoint = olTmp.GetLength();

		if(blNeg)
			ilPrePoint--;


		if(ilPrePoint > imFloatIntPart)
			return false;

		if(ilAfterPoint > imFloatFloatPart)
			return false;

	}



/*
	//int ilOffSet = omPreCursorText.GetLength();



	if(ilPoint >= 0) 
		ilPrePoint = atoi((LPCSTR)omNewText.Left(ilPoint));
	else
		ilPrePoint = atoi((LPCSTR)omNewText);

	itoa(abs(ilPrePoint),buffer,10);
	olStr = CString(buffer);
	if(olStr.GetLength() > imFloatIntPart)
	{
		omInsertText = "";
		return false;
	}

	olStr = omNewText.Right(omNewText.GetLength() - ilPoint - 1 );
	if((olStr.GetLength() > imFloatFloatPart) && (ilPoint >= 0))
	{
		omInsertText = "";
		return false;
	}

*/


	return blRet;
}


bool CCSEdit::TimeCheck()
{

	bool blRet = true;
	if(omNewText.IsEmpty() == TRUE && !bmDateRequired)
	{
		blRet = true;
	}
	else
	{
		if(HourStringToDate(omNewText, bmTimeChangeDay) == TIMENULL)
		{
			blRet = false;
		}
	}
	return blRet;
}



bool CCSEdit::DateCheck()
{
	bool blRet = true;
	if(omNewText.IsEmpty() == TRUE && !bmDateRequired)
	{
		blRet = true;
	}
	else
	{
		COleDateTime olDate;
		olDate = OleDateStringToDate(omNewText);
		if(olDate.GetStatus() == COleDateTime::invalid)
		{
			blRet = false;
		}
	}
	return blRet;
}

/* Old (before modify by MWO
bool CCSEdit::DateCheck()
{
	bool blRet = true;
	if(omNewText.IsEmpty() == TRUE && !bmDateRequired)
	{
		blRet = true;
	}
	else
	{
		if(DateStringToDate(omNewText) == TIMENULL)
		{
			blRet = false;
		}
	}
	return blRet;
}
*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////
///methods for formatcheck///////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

bool CCSEdit::StringCheck()
{
	if(omFormatList.GetSize() == 0)
		return true;

	
	char clTmp;

	omCurrentPath.RemoveAll();


	for(int ilLc = 0; ilLc < omInsertText.GetLength(); ilLc++)
	{
		clTmp = omInsertText[ilLc];
		if(!CheckChar(clTmp))
		{
			omInsertText = omInsertText.Left(ilLc);
			return false;
		}
	}
	return true;
}




bool CCSEdit::CheckChar(char cpChar)
{
	bool blRet = false;
	FORMAT *prlLastFormat;
	FORMAT *prlFormat;
	int ilNodeIndex;

	

	if(omCurrentPath.GetSize() > 0)
	{
		prlLastFormat = &omCurrentPath[omCurrentPath.GetSize() - 1];


		if(prlLastFormat->Next != NULL)
		{
			prlFormat = prlLastFormat->Next;
			blRet = CompareChar(cpChar, prlFormat);
			omCurrentPath.Add(prlFormat);			
		}
		else
		{
			ilNodeIndex = (prlLastFormat->NodeIndex) + 1;
			if(ilNodeIndex >= omFormatList.GetSize())
				return false;
			if(omFormatList[ilNodeIndex].Format.GetSize() <= 0)
				return false;
			

			for(int ilLc = 0; ilLc < omFormatList[ilNodeIndex].Format.GetSize(); ilLc++)
			{
				prlFormat = &omFormatList[ilNodeIndex].Format[ilLc];
				if(CompareChar(cpChar, prlFormat))
				{
					omCurrentPath.Add(prlFormat);			
					blRet = true;
				}
			}
		}
	}
	else
	{
		ilNodeIndex = 0;
		if(ilNodeIndex >= omFormatList.GetSize())
			return false;
		if(omFormatList[ilNodeIndex].Format.GetSize() <= 0)
			return false;
		

		for(int ilLc = 0; ilLc < omFormatList[ilNodeIndex].Format.GetSize(); ilLc++)
		{
			prlFormat = &omFormatList[ilNodeIndex].Format[ilLc];
			if(CompareChar(cpChar, prlFormat))
			{
				omCurrentPath.Add(prlFormat);			
				blRet = true;
			}
		}
	}

	return blRet;
}




bool CCSEdit::CompareChar(char pcpChar, FORMAT *prpFormat)
{
	if (prpFormat == NULL) 
		return false;
	bool blRet = true;
	unsigned char olChar = pcpChar;

	switch(prpFormat->Type)
	{
		case '#':
			{
				if(!isdigit(olChar))
					blRet = false;

				if(prpFormat->RangeFrom != prpFormat->RangeTo)
				{
					if((atoi((LPCSTR)CString(pcpChar)) > prpFormat->RangeTo) || (atoi((LPCSTR)CString(pcpChar)) < prpFormat->RangeFrom))
						blRet = false;
				}
			}
			break;
		case 'A':
			{
				if((!isalpha(olChar)) || (!isupper(olChar)))
					blRet = false;
			}
			break;
		case 'a':
			{
				if((!isalpha(olChar)) || (!islower(olChar)))
					blRet = false;
			}
			break;
		case 'x':
			{
				if(!isalpha(olChar))
					blRet = false;
			}
			break;
		case 'X':
			{
				blRet = true;
			}
			break;
		case '\'':
			{
				if(pcpChar == prpFormat->Value)
					blRet = true;
				else
					blRet = false;
			}
			break;
		default:
			blRet = false;
	}
	return blRet;
}







bool CCSEdit::CreateFormatList(CString opFormatString)
{
	NODE	*prlLastNode;
	FORMAT	*prlLastFormat;
	imLastPos = -1;
	CString olStr;
	bool blCase = false;
	char clType;
	char clValue;
	char clTmp1;
	char clTmp2;
	int ilAnz;
	int ilLc;
	//int ilLen;
	int ilRangeFrom;
	int ilRangeTo;
	bool blNewNode = true;
	bool blNewList = true;
	bool blBracket = false;
	omAllFormats.DeleteAll();
	omFormatList.DeleteAll();

		
	while(!opFormatString.IsEmpty())
	{
		ilAnz = 1;
		ilRangeFrom = 0;
		ilRangeTo = 0;
		//blNewNode = true;
		//blNewList = true;

		clType = opFormatString[0];

		if(blBracket == false)
		{
			blNewNode = true;
			blNewList = true;
		}
		else
		{
			blNewNode = false;
			blNewList = false;
		}

		if(clType == ']')
		{
			if(!opFormatString.IsEmpty())
			{
				opFormatString = opFormatString.Right(opFormatString.GetLength() - 1);
				if(!opFormatString.IsEmpty())
					clType = opFormatString[0];
				else
					clType = ' ';
			}
			else
				clType = ' ';
			blNewNode = true;
			blNewList = true;
			blBracket = false;
		}
		if(clType == '|')
		{
			blNewNode = false;
			blNewList = true;
			opFormatString = opFormatString.Right(opFormatString.GetLength() - 1);
			clType = opFormatString[0];
		}
		if(clType == '[')
		{
			opFormatString = opFormatString.Right(opFormatString.GetLength() - 1);
			clType = opFormatString[0];
			blNewNode = false;
			blNewList = true;
			blBracket = true;
		}

		if(!opFormatString.IsEmpty())
			opFormatString = opFormatString.Right(opFormatString.GetLength() - 1);

		if((clType == 'x') || (clType == 'X') || (clType == '#') ||
		   (clType == 'A') || (clType == 'a') || (clType == '{') || (clType == '\''))
		{
			if(clType == '\'')
			{
				clValue = opFormatString[0];		
				opFormatString = opFormatString.Right(opFormatString.GetLength() - 2);
			}
			if(!opFormatString.IsEmpty())
			{
				if((opFormatString[0] == '(') && ((clType != '{') && (clType != '\'')))
				{
					opFormatString = opFormatString.Right(opFormatString.GetLength() - 1);
					if(opFormatString.Find(')') >= 0)
					{
						ilAnz =	atoi(opFormatString.Left(opFormatString.Find(')')));
					}
				}
			}
			if(clType == '{')
			{
				clType = '#';
				if(isdigit(opFormatString[0]))
				{
					clTmp1 =  opFormatString[0];
					clTmp2 =  opFormatString[2];
					if(isdigit(clTmp1) && isdigit(clTmp2))
					{
						olStr = clTmp1;
						ilRangeFrom = atoi((LPCSTR)olStr);
						olStr = clTmp2;
						ilRangeTo = atoi((LPCSTR)olStr);
						opFormatString = opFormatString.Right(opFormatString.GetLength() - 2);
					}
				}
			}
			for(ilLc = 0; ilLc < ilAnz; ilLc++)
			{
				FORMAT	*prlFormat = new FORMAT();

				if((omFormatList.GetSize() == 0) || (blNewNode) && (ilLc == 0))
				{
					NODE *prlNode = new NODE;
					omFormatList.Add(prlNode);
					prlLastNode = prlNode;
				}
				
				if((omFormatList.GetSize() == 0) || (blNewList) && (ilLc == 0))
					prlLastNode->Format.Add(prlFormat);
				else
					prlLastFormat->Next = prlFormat;

				omAllFormats.Add(prlFormat);
				prlLastFormat = prlFormat;
				prlFormat->NodeIndex = omFormatList.GetSize() - 1;
				prlFormat->Type = clType;
				prlFormat->Value = clValue;
				prlFormat->RangeTo = ilRangeTo;
				prlFormat->RangeFrom = ilRangeFrom;
			}
		}
	}
	return true;
}





void CCSEdit::GetCurrentMinMaxLength()
{
	if (imType == KT_INT)
	{
		char clTmp;
		if (omNewText.GetLength() > 0)
		{
			clTmp = omNewText[0];
			int ilMaxLength = imMaxLength;
			if(clTmp == '-')
			{
				++ilMaxLength;
				CEdit::LimitText(ilMaxLength);
			}
			else
			{
				CEdit::LimitText(ilMaxLength);
			}
		}
	}
	else
	{
		if(omFormatList.GetSize() == 0)
			return;


		FORMAT	*prlFormat;
		int ilLc;
		int ilLc2;
		int ilMax = 0;
		int ilMin = 0;
		int ilAnz = 0;
		if(bmAutoMaxLength)  imMaxLength = 0;
		if(bmAutoMinLength)  imMinLength = 0;
		int ilLastNode = 0;


		if(bmAutoMaxLength) imMaxLength = omCurrentPath.GetSize();
		if(bmAutoMinLength) imMinLength = omCurrentPath.GetSize();
		
		if(omCurrentPath.GetSize() > 0)
		{
			prlFormat = &omCurrentPath[omCurrentPath.GetSize() - 1];

			ilLastNode = prlFormat->NodeIndex + 1;
			
			
			prlFormat = prlFormat->Next;
			while(prlFormat != NULL)
			{
				ilAnz++;
				prlFormat = prlFormat->Next;
			}
		}

		if(bmAutoMaxLength) imMaxLength += ilAnz; 
		if(bmAutoMinLength) imMinLength += ilAnz;

		
		for(ilLc = omFormatList.GetSize() - 1; ilLc >= ilLastNode; ilLc--)
		{
			ilMax = 0;
			ilMin = 1000;
			for(ilLc2 = omFormatList[ilLc].Format.GetSize() - 1; ilLc2 >= 0; ilLc2--)
			{
				prlFormat = &omFormatList[ilLc].Format[ilLc2];
				ilAnz = 0;

				while(prlFormat != NULL)
				{
					ilAnz++;
					prlFormat = prlFormat->Next;
				}
				if(ilAnz > ilMax)
					ilMax = ilAnz;

				if(ilAnz < ilMin)
					ilMin = ilAnz;

			}
			if(bmAutoMaxLength) imMaxLength += ilMax;
			if(bmAutoMinLength) imMinLength += ilMin;
		}

		//TRACE("\n\n MAX: %d ",imMaxLength);
		//TRACE("\n MIN: %d \n",imMinLength);
	}
}



/*
enum {
	NOTHING,
	DONT_CARE,
	CHAR_ONLY,
	UPPERCASE,
	LOWERCASE,
	UPPER_DIGIT,
	LOWER_DIGIT,
	ENUM_CHAR,
	DIGIT,
	THIS_CHAR,
	SINGLE_RANGE,
	DIGTS_IN_RANGE
};
*/



LONG CCSEdit::OnDragOver(UINT wParam, LONG lParam)
{
	if(pomParent == NULL)
		pomParent = GetParent();
    return pomParent->SendMessage(WM_DRAGOVER, wParam, lParam);
}


LONG CCSEdit::OnDrop(UINT wParam, LONG lParam)
{
	if(pomParent == NULL)
		pomParent = GetParent();
    return pomParent->SendMessage(WM_DROP, wParam, lParam);
}


void CCSEdit::DragDropRegister()
{
	omDragDrop.RegisterTarget(this, GetParent());
}

void CCSEdit::OnMouseMove(UINT nFlags, CPoint point)
{
	CEdit::OnMouseMove(nFlags,point);
}


void CCSEdit::OnLButtonDblClk(UINT nFlags, CPoint point)
{

	if(bmReadOnly || bmDisabled || bmUserReadOnly)
		return;
	//CEdit::OnLButtonDown(nFlags, point);

	CString olText;

	GetWindowText(olText);

	CCSEDITNOTIFY rlNotify;
	rlNotify.SourceControl = this;
	rlNotify.Text = olText;
	rlNotify.Status = bmStatus;
	rlNotify.IsChanged = bmChanged;
	rlNotify.Flags = nFlags;
	rlNotify.Name = omName;

	if(pomParent == NULL)
		pomParent = GetParent();
	if(pomParent != NULL)
		pomParent->SendMessage(WM_EDIT_LBUTTONDBLCLK, imID, (LPARAM)&rlNotify);


}

void CCSEdit::OnRButtonDown(UINT nFlags, CPoint point) 
{
	if(bmReadOnly || bmDisabled || bmUserReadOnly)
		return;


	if(omMenuItems.GetSize() <= 0)
	{
		if(bmDefaultMenu)
			CEdit::OnRButtonDown(nFlags, point);
	}
	else
	{
		CMenu menu;
		menu.CreatePopupMenu();
		for (int i = menu.GetMenuItemCount(); i > 0; i--)
			 menu.RemoveMenu(i, MF_BYPOSITION);

		
		for (i = 0; i < omMenuItems.GetSize(); i++)
		{
			menu.AppendMenu(omMenuItems[i].Flags, omMenuItems[i].ID , omMenuItems[i].Text);	
		}

		//CPoint olPoint(LOWORD(lParam),HIWORD(lParam));
		ClientToScreen(&point);
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
	}



	CString olText;

	GetWindowText(olText);

	CCSEDITNOTIFY rlNotify;
	rlNotify.SourceControl = this;
	rlNotify.Text = olText;
	rlNotify.Status = bmStatus;
	rlNotify.IsChanged = bmChanged;
	rlNotify.Flags = nFlags;
	rlNotify.Point = point;
	rlNotify.Name = omName;

	ClientToScreen(&rlNotify.Point);

	if(pomParent == NULL)
		pomParent = GetParent();
	if(pomParent != NULL)
		pomParent->SendMessage(WM_EDIT_RBUTTONDOWN, imID, (LPARAM)&rlNotify);



}


void CCSEdit::AddMenuItem(UINT ipFlags, UINT ipID, CString opText) 
{
	MENUITEM *prlItem = new MENUITEM;

	omMenuItems.Add(prlItem);

	prlItem->ID = ipID;
	prlItem->Text = opText;
	prlItem->Flags = ipFlags;

}



void CCSEdit::OnMenuSelect( UINT nID)
{

	pomParent->SendMessage(WM_EDIT_MENU_SELECT, nID, (LPARAM)this);
	if(pomTableWnd != NULL)
		pomTableWnd->SendMessage(WM_EDIT_MENU_SELECT, nID, (LPARAM)this);

}

//rkr19042001 ESC-notify via message and reset the origin text
BOOL CCSEdit::PreTranslateMessage(MSG* pMsg)
{
	if (!pMsg)
		return FALSE;

	if( pMsg->message == WM_KEYDOWN )
	{
		if(pMsg->wParam == VK_ESCAPE)
		{
			bmKillFocus = true;
			CCSEDITNOTIFY rlNotify;
			rlNotify.SourceControl = this;
			rlNotify.Text = omTextReset;
			rlNotify.Status = bmStatus;
			rlNotify.IsChanged = bmChanged;
			IsEscapePressed = true;

			CString olStr;
			GetWindowText(olStr);

			if(pomParent == NULL)
				pomParent = GetParent();

			if(pomTableWnd != NULL)
			{
				//pomTableWnd->SendMessage(WM_EDIT_KILLFOCUS, imID, (LPARAM)&rlNotify);
				bmUserStatus = rlNotify.Status;
				bmStatusByUser = rlNotify.UserStatus;
				if(olStr != rlNotify.Text)
				{
					SetWindowText(rlNotify.Text);
					InvalidateRect(NULL,true);
				}
				if(bmTableKillFocus)
				{
					pomTableWnd->SendMessage(WM_EDIT_IP_END, imID, CheckAll()? TRUE : FALSE);
				}
			}
			else
			{
				if(pomParent != NULL)
				{
					pomParent->SendMessage(WM_EDIT_KILLFOCUS, imID, (LPARAM)&rlNotify);
					bmUserStatus = rlNotify.Status;
					bmStatusByUser = rlNotify.UserStatus;

					if(olStr != rlNotify.Text)
					{
						SetWindowText(rlNotify.Text);
						InvalidateRect(NULL,true);
					}
				}
			}

			return TRUE; // DO NOT process further
		}
		else
			IsEscapePressed = false;
	}

	return FALSE;
}

//rkr19042001
// overridden: we save the in omTextReset and reset if ESC is pressed
void CCSEdit::SetWindowText(CString opText)
{
	omTextReset = opText;

	CWnd::SetWindowText( opText );
	return;
}



void CCSEdit::DisabledBKColor( bool bpEnable)
{

	bmDisabledBKColor = bpEnable;

}
