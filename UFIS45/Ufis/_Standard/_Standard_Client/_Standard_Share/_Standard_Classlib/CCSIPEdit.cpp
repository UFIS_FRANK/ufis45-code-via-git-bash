// IPEdit.cpp : implementation file
//

//#include "CCSDefines.h"
#include <CCSTable.h>
#include <CCSIPEdit.h>
#include <CCSTime.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCSIPEdit

CCSIPEdit::CCSIPEdit(CRect &popRect,
			     CWnd *popParent /*NULL*/,  
			     int ipType /*KT_STRING*/, 
			     CString opFormat, 
				 CFont *opFont /*NULL*/,
			     int ipTextLimit /*100*/,
			     int ipRangeFrom /*0*/, 
			     int ipRangeTo /*99999999*/)
{
	imTextLimit = ipTextLimit;
	pomParent = NULL;
	pomTable = NULL;
	pomTextFont = NULL;
	if(popParent != NULL)
	{
		pomParent = popParent;
		CCSTableListBox *polLB = (CCSTableListBox*)pomParent;
		CCSTable *polTab = (CCSTable*)polLB->pomTableWindow;
		pomTable = polTab;
	}
	if(opFont != NULL)
	{
		pomTextFont = opFont;
	}
	omRect = popRect;
	imType = ipType;
	Create((WS_CHILD | WS_VISIBLE | WS_EX_NOPARENTNOTIFY), popRect, popParent, IDC_INPLACEEDIT);
}

CCSIPEdit::~CCSIPEdit()
{
}


BEGIN_MESSAGE_MAP(CCSIPEdit, CEdit)
	//{{AFX_MSG_MAP(CCSIPEdit)
	ON_CONTROL_REFLECT(EN_KILLFOCUS, OnKillfocus)
	ON_WM_PARENTNOTIFY_REFLECT()
	ON_WM_CHAR()
	ON_WM_CHARTOITEM()
	ON_WM_CHILDACTIVATE()
	ON_WM_CREATE()
	ON_WM_KEYDOWN()
	ON_WM_KILLFOCUS()
	ON_WM_NCHITTEST()
	ON_WM_PARENTNOTIFY()
	ON_WM_SYSCHAR()
	ON_WM_SYSCOMMAND()
	ON_WM_SYSKEYDOWN()
	ON_WM_SYSDEADCHAR()
	ON_WM_VKEYTOITEM()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	ON_MESSAGE(WM_NEXTDLGCTL, OnNextCtrl)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

LONG CCSIPEdit::OnNextCtrl(UINT wParam, LONG lParam)
{
	int i;
	i++;
	return 1L;
}
/////////////////////////////////////////////////////////////////////////////
// CCSIPEdit message handlers
BOOL CCSIPEdit::Create( DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID)
{

    dwStyle |= WS_CHILD | WS_VISIBLE | WS_EX_NOPARENTNOTIFY | WS_BORDER;//| WS_THICKFRAME | WS_BORDER;//| WS_TABSTOP;//WS_VSCROLL | WS_CLIPSIBLINGS;

	if(CEdit::Create( dwStyle, rect, pParentWnd, nID ) == FALSE)
	{
		return FALSE;
	}
	LimitText(imTextLimit);
	if(pomTextFont != NULL)
	{
		SetFont(pomTextFont);
	}
	//this->SetFocus();
    ShowWindow(SW_SHOW);
	//PostMessage( EM_SETSEL, 0, -1 );
	return TRUE;
}

BOOL CCSIPEdit::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}

LRESULT CCSIPEdit::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(message == WM_NEXTDLGCTL)
	{
		BOOL blType;

		blType = (BOOL) LOWORD(lParam);
		if(blType == FALSE)
		{
			if(wParam == 0)
			{
				int i;
				i++;
			}
			else
			{
				int i;
				i++;
			}
		}

	}
	
	return CEdit::DefWindowProc(message, wParam, lParam);
}

BOOL CCSIPEdit::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(message == WM_NEXTDLGCTL)
	{
		BOOL blType;

		blType = (BOOL) LOWORD(lParam);
		if(blType == FALSE)
		{
			if(wParam == 0)
			{
				int i;
				i++;
			}
			else
			{
				int i;
				i++;
			}
		}

	}
	return TRUE;
	//return CEdit::OnChildNotify(message, wParam, lParam, pLResult);
}

BOOL CCSIPEdit::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(nID == WM_NEXTDLGCTL)
	{
		BOOL blType;

		blType = TRUE;
	}
	
	return CEdit::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}


BOOL CCSIPEdit::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	// TODO: Add your specialized code here and/or call the base class
		BOOL blType;

		blType = (BOOL) LOWORD(lParam);
	return CEdit::OnNotify(wParam, lParam, pResult);
}

LRESULT CCSIPEdit::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(message == WM_NEXTDLGCTL)
	{
		BOOL blType;

		blType = (BOOL) LOWORD(lParam);
		if(blType == FALSE)
		{
			if(wParam == 0)
			{
				int i;
				i++;
			}
			else
			{
				int i;
				i++;
			}
		}

	}
	return CEdit::WindowProc(message, wParam, lParam);
}

void CCSIPEdit::OnKillfocus() 
{
	CCSTable *polTab = (CCSTable*)pomTable;
	//polTab->SetInplaceNextPosition();
	// TODO: Add your control notification handler code here
	
}

void CCSIPEdit::ParentNotify(UINT message, LPARAM lParam) 
{
	// TODO: Add your message handler code here
	if(message == WM_NEXTDLGCTL)
	{
		BOOL blType;

		blType = (BOOL) LOWORD(lParam);
	}
	
}

/*void CCSIPEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CEdit::OnChar(nChar, nRepCnt, nFlags);
}
*/
int CCSIPEdit::OnCharToItem(UINT nChar, CListBox* pListBox, UINT nIndex) 
{
	// TODO: Add your message handler code here and/or call default
	
	return CEdit::OnCharToItem(nChar, pListBox, nIndex);
}

void CCSIPEdit::OnChildActivate() 
{
	CEdit::OnChildActivate();
	
	// TODO: Add your message handler code here
	
}

int CCSIPEdit::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CEdit::OnCreate(lpCreateStruct) == -1)
		return -1;
	//SetSel( 0, -1);
	//PostMessage( EM_SETSEL, 0, -1 );
	//PostMessage(WM_LBUTTONDOWN, MK_LBUTTON, 0);
	//this->SetFocus();
	// TODO: Add your specialized creation code here
	
	return 0;
}


BOOL CCSIPEdit::CheckField(CString &opMessage)
{
	BOOL blRet = TRUE;
	if(imType == KT_STRING && omFormat == CString(""))
	{
		return blRet; // everything is ok
	}
	CString olValue;
	GetWindowText( olValue);
	switch(imType)
	{
	case KT_INT:
		{
			for(int i = 0; i < olValue.GetLength(); i++)
			{
				if(!isdigit(olValue[i]))
				{
					char pclTmp[100]="";
					sprintf(pclTmp, "Nur Zahlen mit %d Stellen erlaubt:", imTextLimit);
					opMessage = CString(pclTmp);
					blRet = FALSE;
					break;
				}
			}
		}
		break;
	case KT_DATE:
		{
			//CTime olDate;
			COleDateTime olDate
			olDate = ogCCSTime.DateStringToDate(olValue);
			if(olDate.GetStatus() == COleDateTime::invalid)
			{
				//opMessage = CString("Falsches Datumsformat:\nBeispiel: 12.12.1990");
				blRet = FALSE;
			}
		}
		break;
	case KT_TIME: 
		{
			CTime olDate;
			if((olDate = ogCCSTime.HourStringToDate(olValue)) == TIMENULL)
			{
				//opMessage = CString("Falsches Zeitformat:\nBeispiel: 10:30");
				blRet = FALSE;
			}
			else
			{
				SetWindowText( olValue);
			}
		}
		break;
	case KT_STRING:
		break;
	case KT_MONEY:
		break;
	default:
		break; // Do nothing
	}
	return blRet;
}

void CCSIPEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	CCSTable *polTab = (CCSTable*)pomTable;
	CString olMessage;
	switch(nChar)
	{
	case VK_TAB:
		{
			unsigned short ilRet = GetKeyState(VK_SHIFT);
			if((GetKeyState(VK_SHIFT) & 0xff81) && (ilRet != 1))
			{
				//polTab->SetInplaceNextPosition();
				if(CheckField(olMessage) == TRUE)
				{
					//polTab->MoveInplaceLeft();
				}
				else
				{
					//ogCCSTime.MessageBox(this, olMessage, "Hinweis", (MB_ICONEXCLAMATION|MB_OK));
				}
			}
			else 
			{
				if(CheckField(olMessage) == TRUE)
				{
					//polTab->MoveInplaceRight();
				}
				else
				{
					//ogCCSTime.MessageBox(this, olMessage, "Hinweis", (MB_ICONEXCLAMATION|MB_OK));
				}
				//polTab->SetInplacePrevPosition();
			}
		}
		break;
	case VK_UP:
		if(CheckField(olMessage) == TRUE)
		{
			//polTab->MoveInplaceUp();
		}
		else
		{
			//ogCCSTime.MessageBox(this, olMessage, "Hinweis", (MB_ICONEXCLAMATION|MB_OK));
		}
		break;
	case VK_DOWN:
		if(CheckField(olMessage) == TRUE)
		{
			//polTab->MoveInplaceDown();
		}
		else
		{
			//ogCCSTime.MessageBox(this, olMessage, "Hinweis", (MB_ICONEXCLAMATION|MB_OK));
		}
		break;
	case VK_RETURN:
		if(CheckField(olMessage) == TRUE)
		{
			polTab->EndInplaceEditing();
		}
		else
		{
			//ogCCSTime.MessageBox(this, olMessage, "Hinweis", (MB_ICONEXCLAMATION|MB_OK));
		}
		break;
	default:
		CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}


void CCSIPEdit::OnKillFocus(CWnd* pNewWnd) 
{
		CEdit::OnKillFocus(pNewWnd);
}

UINT CCSIPEdit::OnNcHitTest(CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	return CEdit::OnNcHitTest(point);
}

void CCSIPEdit::OnParentNotify(UINT message, LPARAM lParam) 
{
	CEdit::OnParentNotify(message, lParam);
	if(message == WM_NEXTDLGCTL)
	{
		BOOL blType;
		blType = TRUE;

	}
	
	// TODO: Add your message handler code here
	
}

void CCSIPEdit::OnSysChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CEdit::OnSysChar(nChar, nRepCnt, nFlags);
}

void CCSIPEdit::OnSysCommand(UINT nID, LPARAM lParam) 
{
	// TODO: Add your message handler code here and/or call default
	
	CEdit::OnSysCommand(nID, lParam);
}

void CCSIPEdit::OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CEdit::OnSysKeyDown(nChar, nRepCnt, nFlags);
}

void CCSIPEdit::OnSysDeadChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CEdit::OnSysDeadChar(nChar, nRepCnt, nFlags);
}

int CCSIPEdit::OnVKeyToItem(UINT nKey, CListBox* pListBox, UINT nIndex) 
{
	// TODO: Add your message handler code here and/or call default
	
	return CEdit::OnVKeyToItem(nKey, pListBox, nIndex);
}

BOOL CCSIPEdit::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_NEXTDLGCTL)
	{
		int i;
		i++;
	}
	return CEdit::PreTranslateMessage(pMsg);
}

//**********************************************************************
// Routines for manipulating the edit control
//**********************************************************************
void CCSIPEdit::SetEditType(int ipType /*KT_STRING*/)
{
	imType = ipType;
}
void CCSIPEdit::SetTextLimit(int ipCount)
{
	imTextLimit = ipCount;
}
void CCSIPEdit::SetFormat(CString opFormat)
{
	omFormat = opFormat;
}
void CCSIPEdit::SetRange(int ipFrom, int ipTo)
{
	imRangeFrom = ipFrom;
	imRangeTo = ipTo;
}
void CCSIPEdit::SetPrecision(int ipPrePoint, int ipAfterPoint)
{
	imFloatIntPart = ipPrePoint;
	imFloatFloatPart = ipAfterPoint;
}
void CCSIPEdit::SetEditFont(CFont *popFont)
{
	pomTextFont = popFont;
}
BOOL CCSIPEdit::InterpretFormat(int ipPosition, UINT ipChar)
{

	if(imType == KT_STRING && omFormat == CString(""))
	{
		return TRUE; // everything is ok
	}

	switch(imType)
	{
	case KT_INT:
		break;
	case KT_DATE:
		break;
	case KT_TIME:
		break;
	case KT_STRING:
		break;
	case KT_MONEY:
		break;
	default:
		break; // Do nothing
	}
	return TRUE;
}
void CCSIPEdit::OnChar( UINT nChar, UINT nRepCnt, UINT nFlags )
{

	CString olString;
	CString olOldString;
	GetWindowText(olOldString);
	CEdit::OnChar( nChar, nRepCnt, nFlags );
//	imCurrentLenght = olOldString.GetLength();
//	if(imTextLimit == imCurrentLenght)
//	{
//		return; // Textlimit is reached ==> no more char
//	}
	if(InterpretFormat(imCurrentLenght+1, nChar) != TRUE)
	{
		SetWindowText(olOldString);
		return;
	}

}

void CCSIPEdit::OnShowWindow( BOOL bShow, UINT nStatus ) //SW_PARENTOPENING
{
	CEdit::OnShowWindow(bShow, nStatus);
	//SendMessage(WM_LBUTTONDOWN, MK_LBUTTON, 0);
	//SendMessage(WM_LBUTTONUP, MK_LBUTTON, 0);
	//SendMessage( EM_SETSEL, 0, -1 );
	PostMessage(WM_LBUTTONDOWN, MK_LBUTTON, 0);
	PostMessage(WM_LBUTTONUP, MK_LBUTTON, 0);
	PostMessage( EM_SETSEL, 0, -1 );
}

void CCSIPEdit::OnPaint()
{
/*	CPaintDC dc(this);
	CPen olPen(PS_SOLID, 2, BLACK);
	CPen *polOldPen = dc.SelectObject(&olPen);
	CRect olRect;
	GetClientRect(&olRect);
	dc.MoveTo(olRect.left-1, olRect.top-1);
	dc.LineTo(olRect.right+1, olRect.top-1);
	dc.MoveTo(olRect.right+1, olRect.top-1);
	dc.LineTo(olRect.right+1, olRect.bottom+1);
	dc.MoveTo(olRect.right+1, olRect.bottom+1);
	dc.LineTo(olRect.left-1, olRect.bottom+1);
	dc.MoveTo(olRect.left-1, olRect.bottom+1);
	dc.LineTo(olRect.left-1, olRect.top-1);
	dc.SelectObject(polOldPen);
*/
	CEdit::OnPaint();

/*	dc.MoveTo(omRect.left, omRect.top);
	dc.LineTo(omRect.right, omRect.top);
	dc.MoveTo(omRect.right, omRect.top);
	dc.LineTo(omRect.right, omRect.bottom);
	dc.MoveTo(omRect.right, omRect.bottom);
	dc.LineTo(omRect.left, omRect.bottom);
	dc.MoveTo(omRect.left, omRect.bottom);
	dc.LineTo(omRect.left, omRect.top);
*/
}
