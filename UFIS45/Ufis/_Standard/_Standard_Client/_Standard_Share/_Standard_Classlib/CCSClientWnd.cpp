// ccsclientwnd.cpp : implementation file
//

#include <stdafx.h>
#include <ccsclientwnd.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCSClientWnd

CCSClientWnd::CCSClientWnd()
{
}

CCSClientWnd::~CCSClientWnd()
{
}


BEGIN_MESSAGE_MAP(CCSClientWnd, CWnd)
    //{{AFX_MSG_MAP(CCSClientWnd)
    ON_WM_ERASEBKGND()
    ON_WM_HSCROLL()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CCSClientWnd message handlers


BOOL CCSClientWnd::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CBrush olBrush(lmBkColor);
    CBrush *pOldBrush = pDC->SelectObject(&olBrush);
    
    CRect olRect;
    pDC->GetClipBox(&olRect);
    
    pDC->PatBlt(olRect.left, olRect.top, olRect.Width(), olRect.Height(), PATCOPY);
    pDC->SelectObject(pOldBrush);
    
    return TRUE;
    

    //return CWnd::OnEraseBkgnd(pDC);
}

void CCSClientWnd::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    // TODO: Add your message handler code here and/or call default
    
    //CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
    
    GetParent() -> SendMessage(WM_HSCROLL, nSBCode, MAKELPARAM(nPos, pScrollBar->GetSafeHwnd()));
}

void CCSClientWnd::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

void CCSClientWnd::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}
