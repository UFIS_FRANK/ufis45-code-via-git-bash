# Microsoft Developer Studio Project File - Name="CCSClass" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=CCSClass - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CCSClass.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CCSClass.mak" CFG="CCSClass - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CCSClass - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "CCSClass - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "CCSClass - Win32 Dll Release" (based on "Win32 (x86) Static Library")
!MESSAGE "CCSClass - Win32 Dll Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CCSClass - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "C:\Ufis_Bin\ClassLib\Release"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\ClassLib\Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FR /YX /FD /c
# ADD BASE RSC /l 0x407
# ADD RSC /l 0x407
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "CCSClass - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "C:\Ufis_Bin\ClassLib\Debug"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\ClassLib\Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MTd /W3 /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /FD /c
# ADD BASE RSC /l 0x407
# ADD RSC /l 0x407
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"Debug/CCSClass.bsc"
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "CCSClass - Win32 Dll Release"

# PROP BASE Use_MFC 1
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CCSClass___Win32_Dll_Release"
# PROP BASE Intermediate_Dir "CCSClass___Win32_Dll_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "C:\Ufis_Bin\ClassLib\SharedDll\Release"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\ClassLib\SharedDll\Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FR /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /FR /YX /FD /c
# ADD BASE RSC /l 0x407
# ADD RSC /l 0x407 /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "CCSClass - Win32 Dll Debug"

# PROP BASE Use_MFC 1
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CCSClass___Win32_Dll_Debug"
# PROP BASE Intermediate_Dir "CCSClass___Win32_Dll_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "C:\Ufis_Bin\ClassLib\SharedDll\Debug"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\ClassLib\SharedDll\Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /FD /c
# ADD CPP /nologo /MDd /W3 /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /FR /YX /FD /c
# ADD BASE RSC /l 0x407
# ADD RSC /l 0x407 /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "CCSClass - Win32 Release"
# Name "CCSClass - Win32 Debug"
# Name "CCSClass - Win32 Dll Release"
# Name "CCSClass - Win32 Dll Debug"
# Begin Source File

SOURCE=.\AatBitmapComboBox.cpp
# End Source File
# Begin Source File

SOURCE=.\AatBitmapComboBox.h
# End Source File
# Begin Source File

SOURCE=.\AatColorCheckBox.cpp
# End Source File
# Begin Source File

SOURCE=.\AatColorCheckBox.h
# End Source File
# Begin Source File

SOURCE=.\AatHelp.cpp
# End Source File
# Begin Source File

SOURCE=.\AatHelp.h
# End Source File
# Begin Source File

SOURCE=.\BcCCmdTarget.cpp
# End Source File
# Begin Source File

SOURCE=.\BcCCmdTarget.h
# End Source File
# Begin Source File

SOURCE=..\_Standard_Wrapper\bccomclient.cpp
# End Source File
# Begin Source File

SOURCE=..\_Standard_Wrapper\bccomclient.h
# End Source File
# Begin Source File

SOURCE=..\_Standard_Wrapper\bccomserver.cpp
# End Source File
# Begin Source File

SOURCE=..\_Standard_Wrapper\bccomserver.h
# End Source File
# Begin Source File

SOURCE=..\_Standard_Wrapper\bcproxy.cpp
# End Source File
# Begin Source File

SOURCE=..\_Standard_Wrapper\bcproxy.h
# End Source File
# Begin Source File

SOURCE=.\blockingsocket.cpp
# End Source File
# Begin Source File

SOURCE=.\blockingsocket.h
# End Source File
# Begin Source File

SOURCE=.\CCS3DStatic.cpp
# End Source File
# Begin Source File

SOURCE=.\CCS3DStatic.h
# End Source File
# Begin Source File

SOURCE=.\CCSBar.Cpp
# End Source File
# Begin Source File

SOURCE=.\CCSBar.h
# End Source File
# Begin Source File

SOURCE=.\CCSBasic.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSBasic.h
# End Source File
# Begin Source File

SOURCE=.\CCSBasicFunc.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSBasicFunc.h
# End Source File
# Begin Source File

SOURCE=.\CCSBchandle.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSBchandle.h
# End Source File
# Begin Source File

SOURCE=.\CCSButtonctrl.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSButtonctrl.h
# End Source File
# Begin Source File

SOURCE=.\CCSCedacom.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSCedacom.h
# End Source File
# Begin Source File

SOURCE=.\CCSCedadata.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSCedadata.h
# End Source File
# Begin Source File

SOURCE=.\CCSCell.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSCell.h
# End Source File
# Begin Source File

SOURCE=.\CCSClientWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSClientWnd.h
# End Source File
# Begin Source File

SOURCE=.\CCSColorButton.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSColorButton.h
# End Source File
# Begin Source File

SOURCE=.\CCSDDX.CPP
# End Source File
# Begin Source File

SOURCE=.\CCSDDX.H
# End Source File
# Begin Source File

SOURCE=.\CCSDefines.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSDefines.h
# End Source File
# Begin Source File

SOURCE=.\CCSDragDropCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSDragDropCtrl.h
# End Source File
# Begin Source File

SOURCE=.\CCSEdit.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSEdit.h
# End Source File
# Begin Source File

SOURCE=.\CCSLog.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSLog.h
# End Source File
# Begin Source File

SOURCE=.\CCSMessageBox.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSMessageBox.h
# End Source File
# Begin Source File

SOURCE=.\CCSPrint.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSPtrArray.h
# End Source File
# Begin Source File

SOURCE=.\CCSTable.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSTable.h
# End Source File
# Begin Source File

SOURCE=.\CCSTime.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSTime.h
# End Source File
# Begin Source File

SOURCE=.\CCSTimeScale.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSTimeScale.h
# End Source File
# Begin Source File

SOURCE=.\CCSTree.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSTree.h
# End Source File
# Begin Source File

SOURCE=.\CedaAPTData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaAPTData.h
# End Source File
# Begin Source File

SOURCE=.\CedaAptLocalUtc.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaAptLocalUtc.h
# End Source File
# Begin Source File

SOURCE=.\CedaBasicData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaBasicData.h
# End Source File
# Begin Source File

SOURCE=.\CedaObject.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaObject.h
# End Source File
# Begin Source File

SOURCE=.\CedaSEAData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSEAData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSysTabData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSysTabData.H
# End Source File
# Begin Source File

SOURCE=.\ClassLib.rc
# End Source File
# Begin Source File

SOURCE=.\excel9.cpp
# End Source File
# Begin Source File

SOURCE=.\excel9.h
# End Source File
# Begin Source File

SOURCE=.\FastCedaConnection.cpp
# End Source File
# Begin Source File

SOURCE=.\FastCedaConnection.h
# End Source File
# Begin Source File

SOURCE=.\FieldSet.cpp
# End Source File
# Begin Source File

SOURCE=.\FieldSet.h
# End Source File
# Begin Source File

SOURCE=.\FlightData.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightData.h
# End Source File
# Begin Source File

SOURCE=.\LibGlobl.cpp
# End Source File
# Begin Source File

SOURCE=.\ModulData.cpp
# End Source File
# Begin Source File

SOURCE=.\ModulData.h
# End Source File
# Begin Source File

SOURCE=.\NameConfigurationDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\NameConfigurationDlg.h
# End Source File
# Begin Source File

SOURCE=.\RecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\RecordSet.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TimePacket.h
# End Source File
# Begin Source File

SOURCE=.\UFIS.H
# End Source File
# Begin Source File

SOURCE=.\UfisOdbc.cpp
# End Source File
# Begin Source File

SOURCE=.\UfisOdbc.h
# End Source File
# Begin Source File

SOURCE=.\VersionInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\VersionInfo.h
# End Source File
# End Target
# End Project
