// ccscolorbutton.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCSColorButton window
#include <afxwin.h>

class CCSColorButton : public CButton
{
// Construction
public:
    CCSColorButton();

// Attributes
public:

// Operations
public:

// Overrides

// Implementation
public:
    virtual ~CCSColorButton();
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
    
    // Generated message map functions
protected:
    //{{AFX_MSG(CCSColorButton)
    afx_msg LONG OnSetCheck (UINT wParam, LONG lParam);
    afx_msg LONG OnSetStyle (UINT wParam, LONG lParam);
    afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
