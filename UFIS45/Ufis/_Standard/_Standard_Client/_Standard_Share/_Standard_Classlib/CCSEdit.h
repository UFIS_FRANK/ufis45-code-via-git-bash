// CCSEdit.h : header file
//
//	RKR 19042001:	::SetWindowText(CString opText) added,
//					::PreTranslateMessage(MSG* pMsg) added,
//					omTextReset added to store the text.
//					PRF 1357 PRODUCT4.4: reset the origin text when ESC
//
/////////////////////////////////////////////////////////////////////////////
// CCSEdit window


// Written by:
// RST			07.07.1996


//@Man:
//@Memo:	The CCSEdit class provides additional functionality of a MFC CEdit control. 

//@See:		MFC's CWnd, MFC's CEdit

/*@ManDoc:

			An CCSEdit control is a rectangular child window in which the user can enter text.
			In additional to the MFC CEdit control you can set a input type:
			- Integer with a range
			- Double with a range and precision 			
			- Money with a range and precision 			
			- Date 
			- Time 
			- Formatet string

			The control prove the user input and show or ignore it.
			By the types Date and Time, a validation is only made when the control lost the focus!

//================================================================================================
// Modification by:
// MWO
// Date: 31.03.00

// Reason: Introduction of regular expressions
		
// Members: CString omRegularExpression ==> stores the expression itself
//			bool bmRegularExpression

// Methods: void SetRegularExpression(CString opExpression)
//			Sets the expression and stores it in omRegularExpression
//			bool CheckRegMatch(CString opValue)
//			==> checks the value against teh expression

//New behavior: if the bmRegularExpression is set ==> only a check against the 
//			    expression will be performed and the status will be set true or false
//				depending on the CheckRegMatch() return value
// END MWO =======================================================================================

			The formatstring can include the following formats: 

			- #		digit
			- A		uppercase letter
			- a		lowercase letter
			- X		any character
			- x		upper or lowercase letter
			- '.'	only this character
			- #(n)	n digits
			- A(n)	n uppercase letters
			- a(n)	n lowercase letters
			- X(n)	n - any characters
			- x(n)	n - upper or lowercase letters
			- {n-m}	one digit in the range n to m (for n and m are one digit allowed)
			- #|a	OR operator, is allowed between all formatstrings and [  ], but not in [  ]
			- [ ]	one path of formatstrings



			The control send the message WM_EDIT_LOSTFOCUS when it losing the focus.
			- lParam conains the (public) unique control ID ( is set iternal )
			- wParam conains a pointer to a CCSEDITNOTIFY struct:

			typedef struct _CCSEDITNOTIFY
			{
				CCSEdit *SourceControl;
				CString	Text;
				bool	Status;	  //Return value
			}CCSEDITNOTIFY


			If you want support by Classwizzard, edit the xxxxxx.clw file:
			

				; CLW file contains information for the MFC ClassWizard

				[General Info]
				Version=1
				LastClass=CCSEdit
				LastTemplate=CDialog
				NewFileInclude1=#include "stdafx.h"
				NewFileInclude2=#include "TestCCSEdit.h"
			->	ExtraDDXCount=1															<-
			->	ExtraDDX1=E;;Control;CCSEdit;;Control;This is a new CCS type of CEdit.  <-

			
			--------------------------------------------------------------------------------------


			For more information on using CCSEdit control, ask RST.



*/


#if !defined(AFX_CCSEDIT_H__E50A217E_1893_11D1_82BD_0080AD1DC701__INCLUDED_)
#define AFX_CCSEDIT_H__E50A217E_1893_11D1_82BD_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <CCSDefines.h>
#include <ccsptrarray.h>
#include <ccstime.h>
#include <ccsdragdropctrl.h>
#include <regex.h>


enum egEditFieldType
{
	KT_STRING, // all inputs allowed
	KT_FORMAT, // optionally a format string could effect this type
	KT_DATE,   
	KT_TIME,   // i think we should have a fix format as MM:HH without seconds
	KT_INT,	   // range effects this field
	KT_DOUBLE,  // range also here , but you shoud use SetPrecision() for floatingpoint length
	KT_MONEY   // Floating point with fixed precision e.g. 2000.00
			   // is using the ranges
};

extern bool IsEscapePressed;
// Used by CCSTable for InplaceEdit
struct CCSEDIT_ATTRIB
{
	DWORD		Style;
	int			Type;
	int			RangeFrom;				// Only used for KT_INT
	int			RangeTo;
	double		dRangeFrom;
	double		dRangeTo;
	int			TextLimit;
	int			TextMinLenght;
	int			TextMaxLenght;
	int			FloatIntPart;
	int			FloatFloatPart;
	CString		Format;
	bool		IsChanged;
	bool		Required;
	bool		ChangeDay;
	COLORREF	ErrColor;
	CCSEDIT_ATTRIB()
	{
		Style			= 0;  
		Type			= KT_STRING;
		RangeFrom		= -9999999;	
		RangeTo			= 9999999;
		dRangeFrom		= -999999.9;
		dRangeTo		= 999999.9;
		TextMinLenght	= 0;
		TextMaxLenght	= 100;
		FloatIntPart	= 3;
		FloatFloatPart	= 3;
		Format			= "";
		ErrColor		= RGB(128,0,0);
		IsChanged		= false;
		Required		= false;
		ChangeDay		= false;
	}
};


class CCSEdit;

struct CCSEDITNOTIFY
{
	CCSEdit *SourceControl;
	CString	Text;
	CPoint	Point;
	bool	 Status;	//Return value
	bool	 UserStatus;
	bool	 IsChanged;
	UINT     Flags;
	CString	Name;
	CCSEDITNOTIFY()
	{
		Flags = 0;
		SourceControl = NULL;
		UserStatus = false;
		Status = false;
		IsChanged = false;
		Name = "";
	}
};



class CCSEdit : public CEdit
{
// Construction
public:
	CCSEdit();

	CCSEdit(CRect &popRect, 
			CWnd *popParent = NULL, 
			CFont *opFont = NULL,
			int ipMinLength = -1, /* if formatstring == "" ipMaxLength = 0   */
			int ipMaxLength = -1, /* if formatstring == "" ipMaxLength = 100 */
			bool bpIPEdit = false,  
			UINT nID = 0);

	CCSEdit(CRect &popRect, 
			CWnd *popParent,
			CWnd *popTableWnd, 
			CFont *opFont,
			bool bpIPEdit = false,  
			CCSEDIT_ATTRIB *prpAttrib = NULL);

	bool Create( DWORD dwStyle, const RECT& rect, CWnd* pParentWnd = NULL, UINT nID = 0);

	virtual ~CCSEdit();
	
	void Initialize(void);

// Attributes
public:
	//interanl ID, this is not the resource-ID
	const int imID; 
	CWnd  *pomParent;
	CWnd *pomTableWnd;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSEdit)
	//}}AFX_VIRTUAL

// Implementation
public:
	
	void SetName(CString opName);//Name of the EditField
	CString& Name();//Name of the EditField
	
	//Set the current type of the field. 
	void SetTypeToDouble(int ipPrePoint = 3, int ipAfterPoint = 3, double dpFrom = -999999.0, double dpTo = 999999.0,bool bpEmptyIsValid = true);
	
	void SetTypeToMoney(int ipPrePoint = 7, int ipAfterPoint = 2, double dpFrom = -999999.0, double dpTo = 999999.0);
	
	void SetTypeToInt(int ipFrom = -32767, int ipTo = 32767,bool bpEmptyIsValid = true);
	
	void SetTypeToTime(bool bpRequired = false, bool bpChangeDay = false);
	
	void SetTypeToDate(bool bpRequired = false);


	// sets a formatstring and the type KT_STRING
	// if opFormat is not empty and ipMaxLength or ipMinLength = -1 the ipMaxLength,ipMinLength is calculate automatic
	// if opFormat is empty and ipMaxLength or ipMinLength = -1 the ipMaxLength = 100 and ipMinLength = 0
	void SetTypeToString(CString opFormat = CString(""), int ipMaxLength = -1, int ipMinLength = -1);

	void SetInitText(CString opTextbp, bool ChangedColor = false);
	void SetReadOnly(bool bpSet = true);


	void	DisabledBKColor( bool bpEnable);

	// warning: Use this methode only by type KT_STRING 
	// TextLimit is set internal by the control, don't call CEdit::LimitText() !!!
	void SetTextLimit(int ipMinLength = -1, int ipMaxLength = -1, bool bpEmptyIsValid = false);

	// sets a formatstring. this method should be used for the type KT_STRING
 	// otherwise it will make no sence
	// "": Every character is allowed
	void SetFormat(CString opFormat = CString(""));

//MWO 31.03.00
	// set the regular expression
	void SetRegularExpression(CString opExpression);
	// check the current value against the regular expression
	// anf return true or false (matched or not matched)
	bool CheckRegMatch(CString opValue);
//END MWO

	// for the fieldtyp Integer we want to set a rage between that values
	// are allowed
	void SetRange(int ipFrom, int ipTo);

	// for the fieldtyp Double and Money we want to set a rage between that values
	// are allowed
	void SetRange(double dpFrom, double dpTo);

	// the type Double and Money needs - perhaps - a precision 
	// e.g. 5, 3 meens totally 5 digits and 3 digits after the decimal point
	void SetPrecision(int ipPrePoint, int ipAfterPoint);

	// perhaps we need a special font for the time of inplace-editing
	void SetEditFont(CFont *popFont);

	//Sets the current background color to the specified color.
	void SetBKColor(COLORREF opColor);

	// Sets the Back ground color for the editable CCSEDIT contrls.
	void SetBackGrColor(COLORREF opColor);	

	// Sets the Error Flag to retain the Valid data.
	void SetErrorFlag();	

	//Sets the current text color to the specified color.
	void SetTextColor(COLORREF opColor);


	void SetTextChangedColor(COLORREF opTextChangedColor) {omTextChangedColor = opTextChangedColor;};

	//void GetWindowText(CString opStr);

	void SetSecState(char cpKey);

	void AddMenuItem(UINT ipFlags, UINT ipID, CString opText);


	COLORREF GetTextColor();
	
	//Sets the text color to the specified color, when
	//the control lost the focus and the text are not valid or incomplete 
	//Only available by inputtype formated string
	void SetTextErrColor(COLORREF opColor);
	
	bool GetStatus();
	void SetStatus(bool bpStatus);

	void SetDefaultMenu(void);
	void UnsetDefaultMenu(void);


	bool IsChanged();

	void EnableWindow(BOOL bpEnable);
	BOOL ShowWindow( int nCmdShow );

	void DragDropRegister();	

//rkr19042001
	void SetWindowText(CString opText);

// Attributes
protected:
	static int imMaxID;

	struct FORMAT
	{
		char	Type;	
		char	Value;
		int		RangeFrom;
		int		RangeTo;
		FORMAT  *Next;
		int		NodeIndex;
		FORMAT(void)
		{
			memset(this,0,sizeof(*this));
			Type = 'N';
		};
	};

	struct NODE
	{
		CCSPtrArray<FORMAT> Format;
		NODE(void){;};
	};
	int imPos;
	bool bmTest;
	int imType;
	int imRangeFrom;
	int imRangeTo;
	double dmRangeFrom;
	double dmRangeTo;
	int imTextLimit;
	int imFloatIntPart;
	int imFloatFloatPart;
	CString omFormatString;
	CFont *pomTextFont;
	CRect omRect;
	bool bmShift;
	bool bmStatus;
	bool bmKillFocus;
	bool bmIsKeyDown; 
	bool bmDateRequired;
	bool bmTimeChangeDay;
	int imLastNodeIndex;
	FORMAT *prmLastFormat;
	bool bmInit;
	bool bmChanged;
	bool bmDisabled;
	bool bmReadOnly;	//set by SetSecState();
	bool bmUserReadOnly; //set by SetReadOnly();
	bool bmTableKillFocus;
	bool bmIsKillFocus;
	char cmKey;

	bool	bmDisabledBKColor;

	bool bmDefaultMenu;

	CCSPtrArray<FORMAT> omAllFormats;
	COLORREF omBKColor;
	COLORREF omTextChangedColor;
	COLORREF omTextColor;
	COLORREF omTextErrColor;
	CBrush omBrush;

	//for Inplaceedit
	bool bmIPEdit;  
	int imLineNo;
	int imColumnNo;

	bool bmUserStatus;
	bool bmStatusByUser;

	CString omOldText;
	CString omNewText;
	CString omInsertText;
	CString omPreCursorText;
	CString omAfterCursorText;

	CString omTextSave;
//rkr19042001
	CString omTextReset;

	int imLastPos;
	int imCurrentPos;
	int imCurrentPosAnf;
	int imCurrentPosEnd;
	int imCurrentFormatPos;

	int imMinLength;
	int imMaxLength;
	bool bmEmptyIsValid;

	bool bmAutoMinLength;
	bool bmAutoMaxLength;

	CCSPtrArray<NODE> omFormatList;
	CCSPtrArray<FORMAT> omCurrentPath;

	CCSPtrArray<MENUITEM> omMenuItems;
//MWO 31.03.00
	CString omRegularExpression;
	bool bmRegularExpression;

//END MWO

	CString omName;
	
private:

	bool bmBKColor;		
	bool bmErrorFlag;	

public:
	CCSDragDropCtrl omDragDrop;

	CWnd *omActXParent;
	
// Implementation
protected:
	bool CheckField();
	bool CheckAll();
	
	bool IntCheck();
	bool DoubleCheck(); //Is also used for the money-check!
	bool TimeCheck();
	bool DateCheck();

	bool StringCheck();
	bool CompareChar(char pcpChar, FORMAT *prpFormat);
	bool CheckChar(char cpChar);

	bool CreateFormatList(CString opFormatString);
	void GetCurrentMinMaxLength();



	
	// Generated message map functions
protected:
	//{{AFX_MSG(CCSEdit)
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnChange();
	afx_msg void OnSetfocus();
	afx_msg void OnKillfocus();
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnEnable(BOOL bEnable);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMenuSelect( UINT nID);
//rkr19042001
	virtual BOOL PreTranslateMessage( MSG* pMsg );
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};





/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCSEDIT_H__E50A217E_1893_11D1_82BD_0080AD1DC701__INCLUDED_)
