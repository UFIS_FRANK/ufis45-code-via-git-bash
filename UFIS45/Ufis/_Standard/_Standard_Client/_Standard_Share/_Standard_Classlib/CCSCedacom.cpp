// ccscedacomm.cpp - Class for Communication with CEDA server
//
// Notes: The CedaComm call an external procedures in a DLL via an import library,
// which was created from "IMPLIB yourdll.lib yourdll.dll" and then link it
// the same way as normal .LIB libraries.
//
//

#include <stdafx.h>
#include <string.h>
#include <ufis.h>
#include <ccscedacom.h>
#include <CCSCedaData.h>
#include <afxsock.h>		// MFC socket extensions
#include <versioninfo.h>


// private data members
int CCSCedaCom::imCount = 0;
bool CCSCedaCom::bmIsConnected = false;
int CCSCedaCom::imLastReturnCode = false;
FastCedaConnection* CCSCedaData::pomFastCeda = NULL;
bool CCSCedaData::bmUseNETIN = true;
CString CCSCedaData::omServerName = "";

CCSCedaCom::CCSCedaCom(CCSLog *popLog, const char *pcpAppName)
{                       
	if(pcpAppName != NULL)
		strcpy(pcmAppName, pcpAppName);
	pomLog = popLog;
    imCount++;  // count how many "CedaCom" instances
}

CCSCedaCom::CCSCedaCom(const char *pcpHostName, const char *pcpHostType, CCSLog *popLog, const char *pcpAppName )
{
	if(pcpAppName != NULL)
		strcpy(pcmAppName, pcpAppName);
    imCount++;  // count how many "CedaCom" instances
	pomLog = popLog;
 
    // establish connection automatically for the first time
    if (bmIsConnected == false)
        Initialize(pcpHostName, pcpHostType);  // use default host in configuration file
}


void CCSCedaCom::SetAppName(CString opName)
{
	strcpy(pcmAppName, opName);
}



CCSCedaCom::~CCSCedaCom()
{                    
	TRACE("CCSCedaCom::~CCSCedaCom called\n");
    imCount--;

    // close the connection if the last instance is destroyed
    if (imCount <= 0 && bmIsConnected == true)
    {
		TRACE("CCSCedaCom::CleanupCom() called\n");
        bmIsConnected = false;
		if(bmUseNetin == true)
		{
			::CleanupCom();
		}
		if(CCSCedaData::pomFastCeda != NULL)
		{
			if(CCSCedaData::pomFastCeda != NULL)
			{
				CCSCedaData::pomFastCeda->CloseConnection();
				delete 	CCSCedaData::pomFastCeda;
				CCSCedaData::pomFastCeda = NULL;
			}
		}

    }
}

bool CCSCedaCom::CleanUpCom()
{                    
	TRACE("CedaCom::CleanUpCom called\n");
    imCount--;

    // close the connection if any
    if (bmIsConnected == true)
    {
        bmIsConnected = false;
		if(bmUseNetin == true)
		{
	        ::CleanupCom();
		}
		if(CCSCedaData::pomFastCeda != NULL)
		{
			CCSCedaData::pomFastCeda->CloseConnection();
			delete 	CCSCedaData::pomFastCeda;
			CCSCedaData::pomFastCeda = NULL;
		}
    }
	return true;
}

void CCSCedaCom::SetUser(const char *pcpUser)
{
     strcpy(pcmUser, pcpUser); 
}

bool CCSCedaCom::Initialize(const char *pcpHostName, const char *pcpHostType)
{

    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(pcmConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pcmConfigPath, getenv("CEDA"));

    
	char pclDllTmpLogName[182];
	//char pclDllLogName[182];
	char pclDllLogMode[24];

	CCSCedaData::bmUseNETIN = true;
	bmUseNetin = true;

	CCSCedaData::pomFastCeda = new FastCedaConnection();
	CCSCedaData::pomFastCeda->AttachComHandler(this);
    //sprintf(pclDllTmpLogName,"C:\\TMP\\%sDll.Log",pcmAppName);
    sprintf(pclDllTmpLogName,CCSLog::GetTmpPath("\\%sDll.Log"),pcmAppName);
    
    GetPrivateProfileString(pcmAppName, "DLL-LOGMODE", "TRACE",
        pclDllLogMode, sizeof pclDllLogMode, pcmConfigPath);

	char pclUseNETIN[128]="";

	//MWO: 14.07.2004: Indicates if netin shall be used
	//                 by the application for writing purposes
	//				   This is an entry in the applications section
    GetPrivateProfileString("GLOBAL", "USENETIN", "TRUE",
        pclUseNETIN, sizeof pclUseNETIN, pcmConfigPath);
	if(strcmp(pclUseNETIN, "FALSE") == 0)
	{
		VersionInfo olVersionInfo;
		if (VersionInfo::GetVersionInfo(NULL,olVersionInfo))
		{
			char pclNETINAppl[512];
			GetPrivateProfileString("GLOBAL", "NETINAPPL", "",pclNETINAppl, sizeof pclNETINAppl, pcmConfigPath);
			if (strstr(pclNETINAppl,olVersionInfo.omProductName) == NULL)
			{
				CCSCedaData::bmUseNETIN = false;
				bmUseNetin = false;
				if (!AfxSocketInit())
				{
					AfxMessageBox("Socket Init Failed!");
					return FALSE;
				}
			}
		}
		else
		{
			CCSCedaData::bmUseNETIN = false;
			bmUseNetin = false;
			if (!AfxSocketInit())
			{
				AfxMessageBox("Socket Init Failed!");
				return FALSE;
			}
		}

	}
	//END MWO: 14.07.2004

	if(bmUseNetin == true)
	{
		::UfisDllAdmin("TRACEFN",pclDllTmpLogName,"");
		::UfisDllAdmin("TRACE","ON",pclDllTmpLogName);
	}
    // close any connection to CEDA server if exist
    if (bmIsConnected == true)
    {
        bmIsConnected = false;
		if(bmUseNetin == true)
		{
	        ::CleanupCom();
		}
/*		if(CCSCedaData::pomFastCeda != NULL)
		{
			delete 	CCSCedaData::pomFastCeda;
			CCSCedaData::pomFastCeda = NULL;
		}
*/
    }


    // check if we need to read host name and type from "CEDA.INI"
    // warning: both host name and type will be truncated to 12 characters.
    //
    if (pcpHostName == NULL)
        GetPrivateProfileString(pcmAppName, "HOSTNAME", "",
            pcmHostName, sizeof pcmHostName, pcmConfigPath);
    else
    {
        strncpy(pcmHostName, pcpHostName, sizeof pcmHostName);
        pcmHostName[sizeof pcmHostName - 1] = '\0';
    }
    // log.Trace("test", "HostName = \"%s\"", pcmHostName);

    if (pcpHostType == NULL)
        GetPrivateProfileString(pcmAppName, "HOSTTYPE", "",
            pcmHostType, sizeof pcmHostType, pcmConfigPath);
    else
    {
        strncpy(pcmHostType, pcpHostType, sizeof pcmHostType);
        pcmHostType[sizeof pcmHostType - 1] = '\0';
    }

    
	strcpy(pcmRealHostType, pcmHostType);
	strcpy(pcmRealHostName, pcmHostName);
	CCSCedaData::omServerName = CString(pcmRealHostName);

	//GetPrivateProfileString(pcmAppName, "WKSNAME", "CCSCL",
    //       pcmReqId, sizeof pcmReqId, pcmConfigPath);
	
	
//	if(pomLog != NULL)
//		pomLog->Trace("CEDACOM", "Host = \"%s\" \"%s\" ReqId \"%s\"", 
//			pcmHostType,pcmHostName,pcmReqId);

	if(bmUseNetin == true)
	{

		imLastReturnCode = ::InitCom(pcmRealHostType, pcmRealHostName);
		// initialize communication with CEDA
		if (  imLastReturnCode != RC_SUCCESS)
		{
			return false;
		}
	}
	::GetWorkstationName(pcmReqId);
	


	imLastReturnCode = 0;
    bmIsConnected = true;
    return true;
}


bool CCSCedaCom::RegisterBcWindow(CWnd *BcWindow)
{
	bool blRc = true;
	char pclBroadcasts[24];

	if(bmUseNetin == true)
	{
		GetPrivateProfileString(pcmAppName, "BROADCASTS", "YES",
				pclBroadcasts, sizeof pclBroadcasts, pcmConfigPath);
		
		if (stricmp(pclBroadcasts,"NO") != 0)
		{
			if (::RegisterBcWindow(BcWindow->m_hWnd) != RC_SUCCESS)
			{
				blRc = false;
				if(pomLog != NULL)
					pomLog->Trace("REGISTERBC","RegisterBcWindow FAILED: Rc=%s",blRc == true ? "true" : "false");
			}
			if(pomLog != NULL)
				pomLog->Trace("REGISTERBC","RegisterBcWindow OK: Rc=%s",blRc == true ? "true" : "false");
		}
		else
		{
			if(pomLog != NULL)
				pomLog->Trace("REGISTERBC","RegisterBcWindow: not registered, <%s> in CEDA.INI",pclBroadcasts);
		}
	}
    return blRc;

}

bool CCSCedaCom::UnRegisterBcWindow(CWnd *BcWindow)
{
	bool blRc = true;

	if(bmUseNetin == true)
	{
		if (::UnRegisterBcWindow(BcWindow->m_hWnd) != RC_SUCCESS)
		{
			blRc = true;
		}
	}
    return blRc;

}

CString CCSCedaCom::LastError(void)
{
	// MNE 000118
	// We got more error messages, so this function had to be updated

	/*static char *szMessage[] = {
        "RC_SUCCESS: Everthing OK",
        "RC_FAIL: General serious error",
        "RC_COMM_FAIL: WINSOCK.DLL error",
        "RC_INIT_FAIL: Open the log file",
        "RC_CEDA_FAIL: CEDA reports an error",
        "RC_SHUTDOWN: CEDA reports shutdown",
        "RC_ALREADY_INIT: InitComm called up for a second line",
        "RC_NOT_FOUND: Data not found (More than 7 applications are running)",
        "RC_DATA_CUT: Data incomplete",
        "RC_TIMEOUT: CEDA reports a time-out"
    };
    static const int nMessages = sizeof(szMessage)/sizeof(szMessage[0]);

    if (!(-nMessages <= imLastReturnCode && imLastReturnCode <= 0))
        return CString("Unknown error");

    // notice that "imLastReturnCode" is always negative
    return CString(szMessage[-imLastReturnCode]);*/

	CString olRet;

	switch (imLastReturnCode)
	{
		case 1:
				// will actually never happen coz only used within the dll
			olRet = CString("RC_EMPTY_MSG: DLL returned empty content");
			break;

		case 0:
			olRet = CString("RC_SUCCESS: Everything OK");
			break;

		case -1:
			olRet = CString("RC_FAIL: General serious error");
			break;

		case -2:
			olRet = CString("RC_COMM_FAIL: WINSOCK.DLL error");
			break;

		case -3:
			olRet = CString("RC_INIT_FAIL: Open the log file");
			break;

		case -4:
			olRet = CString("RC_CEDA_FAIL: CEDA reports an error");
			break;

		case -5:
			olRet = CString("RC_SHUTDOWN: CEDA reports shutdown");
			break;

		case -6:
			olRet = CString("RC_ALREADY_INIT: InitComm called up for a second time");
			break;

		case -7:
			olRet = CString("RC_NOT_FOUND: Data not found (More than 7 applications are running)");
			break;

		case -8:
			olRet = CString("RC_DATA_CUT: Data incomplete");
			break;

		case -9:
			olRet = CString("RC_TIMEOUT: CEDA reports a time-out");
			break;

		case -10:
			olRet = CString("RC_ALREADY_CONNECTED: InitComm has already been called");
			break;

		case -11:
			olRet = CString("RC_MISSING_ANSWER: No reply");
			break;

		case -31:
			olRet = CString("RC_BUSY: UFIS.DLL is busy");
			break;

		case -32:
			olRet = CString("RC_TO_MANY_APP: Too many Applications started (Kill BcServ32.exe)");
			break;

		default:
			olRet = CString("Unknown error"); 
			break;
	}

	return olRet;
}
