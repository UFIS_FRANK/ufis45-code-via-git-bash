#include <stdafx.h>
#include <UfisOdbc.h>
#include <RecordSet.h>
#include <CCSBasicFunc.h>

UfisOdbc::UfisOdbc(CString opConnectionString)
{
	omConnectionString = opConnectionString;
	isOdbcInit = false;
}

UfisOdbc::~UfisOdbc()
{
}

int UfisOdbc::CallDB(CString opAction, 
					 CString opTable, 
					 CString opFields, 
					 CString opWhere, 
					 CString opValues,
					 CCSPtrArray<RecordSet> *popData/* = NULL*/)
{
	CDatabase olDB;
	int sqlRet = 0;
	char	pclValue[2000]="";
//	SDWORD	OutputDataLen;
	unsigned char pclConnStrOut[256];
	int ilFieldCount=0;
	int ilValueCount=0;
	char  sDataArray[100][255];
	char pclDataBuffer[25600];

	char pclSqlState[6]="";
	long  ilNativeError;
	SQLCHAR pclErrorMsg[SQL_MAX_MESSAGE_LENGTH];
	short ilErrorLen;
	SDWORD dwDataLen[250];
	CStringArray olFields;
	CStringArray olValues;
	ilFieldCount = ExtractItemList(opFields, &olFields, ',');
	ilValueCount = ExtractItemList(opValues, &olValues, ',');
	omDataArray.RemoveAll();
	omLastErrorMessage="";

	pclErrorMsg[0] = '\0';
	if(isOdbcInit == false)
	{
		sqlRet = ::SQLAllocEnv(&hEnv);
		if(sqlRet == SQL_SUCCESS)
		{
			sqlRet = ::SQLAllocConnect(hEnv, &hDbConn);
			if(sqlRet == SQL_SUCCESS)
			{
				//sqlRet = ::SQLDriverConnect(hDbConn,0,(unsigned char*)"DSN=Peking-ODBC;UID=ceda;PWD=ceda", SQL_NTS, pclConnStrOut, 256, NULL, SQL_DRIVER_NOPROMPT);
				sqlRet = ::SQLDriverConnect(hDbConn,0,(unsigned char*)omConnectionString.GetBuffer(0), SQL_NTS, pclConnStrOut, 256, NULL, SQL_DRIVER_NOPROMPT);
				if(sqlRet == SQL_SUCCESS)
				{
					isOdbcInit = true;				
				}
				else
				{
					AfxMessageBox("Could not connect ODBC-Database!!");
					return -1;
				}
			}
		}
	} 

	CString olSql;
	if(opAction == "Select")
	{
		if(::SQLAllocStmt(hDbConn, &hStmt) == SQL_SUCCESS)
		{

			CString olSql = opAction + " " + opFields + " FROM " + opTable + " " + opWhere;
			if(::SQLExecDirect(hStmt, (unsigned char*)olSql.GetBuffer(0), SQL_NTS) == SQL_SUCCESS) //ilFieldCount
			{
			   //SQLNumResultCols(hStmt, &nCols);
			   for(int ilC =0; ilC < ilFieldCount; ilC++)
				  SQLBindCol(hStmt, (UWORD)(ilC+1), SQL_C_CHAR, (unsigned char*)sDataArray[ilC], 250, &dwDataLen[ilC]);

				for(sqlRet = ::SQLFetch(hStmt); sqlRet == SQL_SUCCESS; sqlRet = ::SQLFetch(hStmt))
				{
				
					RecordSet *prlRecord = NULL;
					if(popData != NULL)
					{
						prlRecord = new RecordSet(ilFieldCount);
					}
					pclDataBuffer[0]='\0';
				  for(int i=0;  i<ilFieldCount; i++) 
				  {
					 // check if the column is a null value?
					 CString olValue;
					 olValue = CString((dwDataLen[i]==SQL_NULL_DATA)?"":sDataArray[i]);
					 olValue.TrimRight();
					 if(popData != NULL)
					 {
						 prlRecord->Values[i] = olValue;
						 MakeClientString(prlRecord->Values[i]);
					 }
					 strcat(pclDataBuffer, (dwDataLen[i]==SQL_NULL_DATA)?"":sDataArray[i]);
					 int dwText = strlen(pclDataBuffer);
					 pclDataBuffer[dwText++] = ',';
					 pclDataBuffer[dwText] = '\0';
				  }
				  if (*pclDataBuffer)
					 pclDataBuffer[strlen(pclDataBuffer)-1]='\0';
				  else
					 break;
				  if(popData != NULL)
				  {
					popData->Add(prlRecord);
				  }
				  omDataArray.Add(pclDataBuffer);
				  pclDataBuffer[0]='\0';

				}
				omLastErrorMessage="";
			}
			else
			{
				::SQLGetDiagRec(SQL_HANDLE_STMT, hStmt, 1, (unsigned char*)pclSqlState, &ilNativeError, 
								pclErrorMsg, sizeof(pclErrorMsg), &ilErrorLen);
				omLastErrorMessage = CString(pclErrorMsg);
			}
			::SQLFreeStmt(hStmt, SQL_DROP);
		}

	}
	if(opAction == "Update" )
	{
		if(ilFieldCount != ilValueCount)
		{
			AfxMessageBox("Fieldlist not the same amount as Valuelist");
			return -1;
		}
		if(::SQLAllocStmt(hDbConn, &hStmt) == SQL_SUCCESS)
		{
			CString olSql = opAction + " " + opTable + " Set ";//opFields + " FROM " + "CEDA." + opTable + " " + opWhere;
			CString olPart;
			for(int i = 0; i < olValues.GetSize() && i < olFields.GetSize(); i++)
			{
				if(!olValues[i].IsEmpty() && !olFields[i].IsEmpty())
				{
					if((i+1) == olValues.GetSize())
					{
						olPart += olFields[i] + CString("=") + olValues[i] + CString(" ");
					}
					else
					{
						olPart += olFields[i] + CString("=") + olValues[i] + CString(",");
					}
				}
			}
			olSql += olPart + opWhere;
			if(::SQLExecDirect(hStmt, (unsigned char*)olSql.GetBuffer(0), SQL_NTS) != SQL_SUCCESS) //ilFieldCount
			{
				AfxMessageBox("Update could not be performed ==> Error occured!!");
			}
			else
			{
				::SQLGetDiagRec(SQL_HANDLE_STMT, hStmt, 1, (unsigned char*)pclSqlState, &ilNativeError, 
								pclErrorMsg, sizeof(pclErrorMsg), &ilErrorLen);
				omLastErrorMessage = CString(pclErrorMsg);
			}
			::SQLFreeStmt(hStmt, SQL_DROP);
		}
	}
	if(opAction == "Insert")
	{
		if(ilFieldCount != ilValueCount)
		{
			AfxMessageBox("Fieldlist not the same amount as Valuelist");
			return -1;
		}
		if(::SQLAllocStmt(hDbConn, &hStmt) == SQL_SUCCESS)
		{
			CString olSql = opAction + " INTO " + opTable + CString("(") + opFields +  CString(")") + " VALUES (" + opValues + CString(")");
			if(::SQLExecDirect(hStmt, (unsigned char*)olSql.GetBuffer(0), SQL_NTS) != SQL_SUCCESS) //ilFieldCount
			{
				AfxMessageBox("Insert could not be performed ==> Error occured!!");
			}
			else
			{
				::SQLGetDiagRec(SQL_HANDLE_STMT, hStmt, 1, (unsigned char*)pclSqlState, &ilNativeError, 
								pclErrorMsg, sizeof(pclErrorMsg), &ilErrorLen);
				omLastErrorMessage = CString(pclErrorMsg);
			}
			::SQLFreeStmt(hStmt, SQL_DROP);
		}
	}
	if(opAction == "Delete")
	{
		if(opWhere.IsEmpty())
		{
			AfxMessageBox("Delete without WHERE is not permitted!!");
			return -1;
		}
		CString olSql = opAction + " FROM " + opTable + CString(" ") + opWhere;
		if(::SQLExecDirect(hStmt, (unsigned char*)olSql.GetBuffer(0), SQL_NTS) != SQL_SUCCESS) //ilFieldCount
		{
			::SQLGetDiagRec(SQL_HANDLE_STMT, hStmt, 1, (unsigned char*)pclSqlState, &ilNativeError, 
							pclErrorMsg, sizeof(pclErrorMsg), &ilErrorLen);
			omLastErrorMessage = CString(pclErrorMsg);
//			AfxMessageBox("Delete could not be performed ==> Error occured!!");
		}
		::SQLFreeStmt(hStmt, SQL_DROP);

	}
	return 0;
}

bool UfisOdbc::GetManyUrnos(int ipCount, CUIntArray &opUrnos)
{
	bool blRet = true;
	int ilUrno,ilCurr;
	opUrnos.RemoveAll();
	if(CallDB("Select", "NUMTAB", "ACNU", "WHERE KEYS='SNOTAB'", "") == 0)
	{
		ilUrno = atoi(omDataArray[0].GetBuffer(0));
		ilCurr = ilUrno;
		for(int i = ilUrno; i < (ilUrno+ipCount); i++)
		{
			opUrnos.Add(ilCurr++);
		}
	}
	CString olCurr;
	olCurr.Format("%d", ilCurr);
	if(CallDB("Update", "NUMTAB", "ACNU", "WHERE KEYS='SNOTAB'", olCurr) != 0)
	{
		AfxMessageBox("DB Problem occured at GetManyUrnos");
		blRet = false;
	}
	return blRet;
}

int UfisOdbc::GetLineCount()
{
	return omDataArray.GetSize();
}

//return then number of values
int UfisOdbc::GetLine(int ipLineNo, CStringArray &ropValues)
{
	int ilValueCount=0;
	if(omDataArray.GetSize() > ipLineNo)
	{
		ilValueCount = ExtractItemList(omDataArray[ipLineNo], &ropValues, ',');
		for(int i = 0; i < ilValueCount; i++)
		{
			ropValues[i].TrimRight();
		}
	}
	return ilValueCount;
}

void UfisOdbc::MakeClientString(CString &ropText)
{
	CString omClientChars("\042\047\54\012\015");  // 34,39,44,10,13
	CString omServerChars("\260\261\262\264\263");  // 176,177,178,180,179
	for(int i = 0; i < omServerChars.GetLength(); i++)
	{
		ropText.Replace(omServerChars[i], omClientChars[i]);
	}
}
