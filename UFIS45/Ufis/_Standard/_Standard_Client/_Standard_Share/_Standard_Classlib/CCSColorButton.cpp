// ccscolorbutton.cpp : implementation file
//

#include <stdafx.h>
#include <ccscolorbutton.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////
// CCSColorButton

CCSColorButton::CCSColorButton()
{
}

CCSColorButton::~CCSColorButton()
{
	TRACE("CCSColorButton::~CCSColorButton called\n");
}

BEGIN_MESSAGE_MAP(CCSColorButton, CButton)
    //{{AFX_MSG_MAP(CCSColorButton)
    ON_MESSAGE(BM_SETCHECK, OnSetCheck)
    ON_MESSAGE(BM_SETSTYLE, OnSetStyle)
    ON_WM_MEASUREITEM()
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CCSColorButton message handlers

LONG CCSColorButton::OnSetCheck(UINT wParam, LONG lParam) 
{
    // TODO: Add your message handler code here
    //OutputDebugString("OnSetCheck\n\r");
    
    return 0L;
}

/*
LONG CCSColorButton::OnSetState(UINT wParam, LONG lParam) 
{
    // TODO: Add your message handler code here

    // only wParam == 1 comes
    char clBuf[64];
    wsprintf(clBuf, "OnSetState: [%d]\n\r", wParam);
    OutputDebugString(clBuf);
    
    return 0L;
}
*/

LONG CCSColorButton::OnSetStyle(UINT wParam, LONG lParam) 
{
    // TODO: Add your message handler code here
    //OutputDebugString("OnSetStyle\n\r");
    
    return 0L;
}

void CCSColorButton::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
    // TODO: Add your message handler code here and/or call default
    
    CButton::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CCSColorButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
    // TODO: Add your message handler code here and/or call default
    switch (lpDrawItemStruct->itemAction)
    {
        case ODA_FOCUS :
            OutputDebugString("Focus : ");
            if (lpDrawItemStruct->itemState & ODS_FOCUS)
                OutputDebugString("gains\n\r");
            else
                OutputDebugString("loses\n\r");
        break;
        
        case ODA_SELECT :
            OutputDebugString("Select : ");
            if (lpDrawItemStruct->itemState & ODS_SELECTED)
                OutputDebugString("yes\n\r");
            else
                OutputDebugString("no\n\r");
        break;
        
        case ODA_DRAWENTIRE :
            OutputDebugString("DrawEntire : \n\r");
            OutputDebugString("\n\r");
        break;
    }       


    // must not call it
    //CButton::DrawItem(lpDrawItemStruct);
}
