// CedaAptLocalUtc.cpp: implementation of the CedaAptLocalUtc class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <CcsDefines.h>
#include <CedaAptLocalUtc.h>
#include <CedaAPTData.h>
#include <CedaSEAData.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
static CedaAptLocalUtc *_this = NULL;

CedaAptLocalUtc::CedaAptLocalUtc()
{
	omHopo4 = "";
	omLastApt4LocalToUtc = "????";
	pomAptData = NULL;
	pomSeaData = NULL;
	InitAptLocalUtc ();
}

CedaAptLocalUtc::~CedaAptLocalUtc()
{
	if (pomAptData)
		delete pomAptData;

	if (pomSeaData)
		delete pomSeaData;

	pomAptData = NULL;
	pomSeaData = NULL;
	_this = NULL;
}

void CedaAptLocalUtc::FreeCedaAptLocalUtc ()
{
	if (_this)
		delete _this;
}

bool CedaAptLocalUtc::InitCedaAptLocalUtc (const CString &opHopo)
{
	if (! _this)
	{
		_this = new CedaAptLocalUtc();
	}

	if (_this)
		return _this->InitHopo4(opHopo);

	return false;
}

bool CedaAptLocalUtc::InitHopo4 (const CString &opHopo4)
{
	APTDATA *prlApt4 = pomAptData->FindApt4(opHopo4);
	if (prlApt4)
	{
		omHopo4 = opHopo4;
		return true;
	}

	return false;
}

CedaAptLocalUtc::CedaAptLocalUtc(const CedaAptLocalUtc &src)
{
}

bool CedaAptLocalUtc::InitAptLocalUtc()
{
	bool blRet = false;

	pomAptData = new CedaAPTDataUtcLoc();
	if (pomAptData)
	{
		blRet = pomAptData->Read("");
		if (blRet)
		{
			pomSeaData = new CedaSEADataUtcLoc();
			if (pomSeaData)
				blRet = pomSeaData->Read("");
			else
				blRet = false;
		}
	}

	return blRet;
}

bool CedaAptLocalUtc::AptUtcToLocal (CTime &opTime, const CString &opAPC4)
{
	if (! _this)
	{
		_this = new CedaAptLocalUtc();
	}

	if (_this)
		return _this->Apt4UtcToLocal(opTime, opAPC4);

	return false;
}

bool CedaAptLocalUtc::AptLocalToUtc (CTime &opTime, const CString &opAPC4)
{
	if (! _this)
	{
		_this = new CedaAptLocalUtc();
	}

	if (_this)
		return _this->Apt4LocalToUtc(opTime, opAPC4);

	return false;
}

bool CedaAptLocalUtc::Apt4UtcToLocal (CTime &opTime, const CString &opAPC4)
{
	if(opTime == TIMENULL)
		return false;

	if (!pomSeaData || !pomAptData)
		return false;

	APTDATA *prlApt4 = NULL;
	if (opAPC4.IsEmpty() && !omHopo4.IsEmpty())
		prlApt4 = pomAptData->FindApt4(omHopo4);
	else
	{
		prlApt4 = pomAptData->FindApt4(opAPC4);
		if (!prlApt4)
			prlApt4 = pomAptData->FindApt4(omHopo4);
	}

	if (prlApt4)
		return pomAptData->Apt4UtcToLocal(opTime, prlApt4->Apc4/*opAPC4*/, pomSeaData->IsWinterSeason(opTime, false));

/*
	if (opAPC4.IsEmpty() && !omHopo4.IsEmpty())
		return pomAptData->Apt4UtcToLocal(opTime, omHopo4, pomSeaData->IsWinterSeason(opTime, false));
	else
		return pomAptData->Apt4UtcToLocal(opTime, opAPC4, pomSeaData->IsWinterSeason(opTime, false));
*/
	return false;
}

bool CedaAptLocalUtc::Apt4LocalToUtc (CTime &opTime, const CString &opAPC4)
{
	if(opTime == TIMENULL)
		return false;

	if (!pomSeaData || !pomAptData)
		return false;

	APTDATA *prlApt4 = NULL;
	if (opAPC4.IsEmpty() && !omHopo4.IsEmpty())
		prlApt4 = pomAptData->FindApt4(omHopo4);
	else
	{
		prlApt4 = pomAptData->FindApt4(opAPC4);
		if (!prlApt4)
			prlApt4 = pomAptData->FindApt4(omHopo4);
	}

	if (prlApt4)
	{
		if (strcmp(omLastApt4LocalToUtc, opAPC4) != 0) 
		{
			// only convert again if a new apc4 is used
			omLastApt4LocalToUtc = opAPC4;
			if (! pomSeaData->Apt4ConvertSeaToLocal(prlApt4->LocalDiffSummer, prlApt4->LocalDiffWinter))
				return false;
		}

		return pomAptData->Apt4LocalToUtc(opTime, prlApt4->Apc4/*opAPC4*/, pomSeaData->IsWinterSeason(opTime, true));
	}

	return false;
}