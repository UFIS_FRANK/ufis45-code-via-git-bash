// UCDialog.h : main header file for the UCDIALOG application
//

#if !defined(AFX_AMTEST2_H__49E91F1B_1AD2_4688_AB1D_FCD45B45BCBA__INCLUDED_)
#define AFX_AMTEST2_H__49E91F1B_1AD2_4688_AB1D_FCD45B45BCBA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CUCDialogApp:
// See UCDialog.cpp for the implementation of this class
//

class CUCDialogApp : public CWinApp
{
public:
	CUCDialogApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUCDialogApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CUCDialogApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AMTEST2_H__49E91F1B_1AD2_4688_AB1D_FCD45B45BCBA__INCLUDED_)
