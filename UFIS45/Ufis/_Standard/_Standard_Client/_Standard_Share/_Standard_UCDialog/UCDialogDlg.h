// UCDialogDlg.h : header file
//
//{{AFX_INCLUDES()
#include "ucedit.h"
//}}AFX_INCLUDES

#if !defined(AFX_AMTEST2DLG_H__68B485A7_3C9D_4869_A831_28BBBFDCD0B1__INCLUDED_)
#define AFX_AMTEST2DLG_H__68B485A7_3C9D_4869_A831_28BBBFDCD0B1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "UtilUC.h"
#include "UFISAmSink.h"
#include "Resource.h"
/////////////////////////////////////////////////////////////////////////////
// CUCDialogDlg dialog

//IUFISAmPtr pConnect; // for com-interface

class CUCDialogDlg : public CDialog
{
// Construction
public:
	CUCDialogDlg(CWnd* pParent = NULL);	// standard constructor
	CUFISAmSink m_UFISAmSink;
	void ExternCall(LPCSTR popMessage);
	CStringArray omExtMesBuffer;
	bool InitCOMInterface();
// Dialog Data
	//{{AFX_DATA(CUCDialogDlg)
	enum { IDD = IDD_UCDIALOG_DIALOG };
	CUCEdit	m_UCEdit1;
	CUCEdit	m_UCEdit2;
	CUCEdit	m_UCEdit3;
	CUCEdit	m_UCEdit4;
	CUCEdit	m_UCEdit5;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUCDialogDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
	UtilUC* mobjUtilUC;
	bool m_bAirportFlag;
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CUCDialogDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnCancel();
	virtual void OnOK();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AMTEST2DLG_H__68B485A7_3C9D_4869_A831_28BBBFDCD0B1__INCLUDED_)
