// DlgProxy.h : header file
//

#if !defined(AFX_DLGPROXY_H__409F0E5C_5608_11D7_8009_00010215BFDE__INCLUDED_)
#define AFX_DLGPROXY_H__409F0E5C_5608_11D7_8009_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CAlerterDlg;

/////////////////////////////////////////////////////////////////////////////
// CAlerterDlgAutoProxy command target

class CAlerterDlgAutoProxy : public CCmdTarget
{
	DECLARE_DYNCREATE(CAlerterDlgAutoProxy)

	CAlerterDlgAutoProxy();           // protected constructor used by dynamic creation

// Attributes
public:
	CAlerterDlg* m_pDialog;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAlerterDlgAutoProxy)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CAlerterDlgAutoProxy();

	// Generated message map functions
	//{{AFX_MSG(CAlerterDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CAlerterDlgAutoProxy)

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CAlerterDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPROXY_H__409F0E5C_5608_11D7_8009_00010215BFDE__INCLUDED_)
