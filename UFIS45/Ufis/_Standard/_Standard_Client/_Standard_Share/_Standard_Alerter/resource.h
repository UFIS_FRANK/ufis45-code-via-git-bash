//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Alerter.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDS_ABOUTBOX                    101
#define IDD_ALERTER_DIALOG              102
#define IDS_STRING102                   102
#define IDS_STRING103                   103
#define IDS_STRING104                   104
#define IDS_STRING105                   105
#define IDS_STRING106                   106
#define IDS_STRING107                   107
#define IDS_STRING108                   108
#define IDS_STRING109                   109
#define IDS_STRING110                   110
#define IDS_STATIC_USER                 111
#define IDS_STATIC_LOGINTIME            112
#define IDS_ALERTER_ABOUT               113
#define IDS_STRING114                   114
#define IDS_STRING115                   115
#define IDS_STRING116                   116
#define IDS_STRING117                   117
#define IDS_STRING118                   118
#define IDS_STRING119                   119
#define IDS_STRING120                   120
#define IDS_STRING121                   121
#define IDS_STRING122                   122
#define IDS_STRING123                   123
#define IDS_STRING124                   124
#define IDS_STRING125                   125
#define IDS_STRING126                   126
#define IDS_STRING127                   127
#define IDR_MAINFRAME                   128
#define IDS_STRING128                   128
#define IDS_STRING129                   129
#define IDS_STRING130                   130
#define IDS_STRING131                   131
#define IDS_STRING132                   132
#define IDS_STRING133                   133
#define IDS_STRING134                   134
#define IDS_STRING135                   135
#define IDS_STRING136                   136
#define IDS_STRING137                   137
#define IDS_STRING138                   138
#define IDS_STRING139                   139
#define IDS_STRING140                   140
#define IDS_STRING141                   141
#define IDS_STRING142                   142
#define IDS_STRING143                   143
#define IDS_STATIC_SERVER               144
#define IDS_STRING111                   145
#define IDB_ABB                         167
#define IDC_AMPTESTCTRL1                1000
#define IDC_TABCTRL                     1001
#define IDC_UFISCOMCTRL1                1002
#define IDC_CONFIRM                     1003
#define IDC_CLOSE                       1004
#define IDC_REOPEN                      1005
#define IDC_TAB_PAGES                   1006
#define IDC_TABCTRL1                    1007
#define IDC_AATLOGINCONTROL1            1008
#define IDC_DETAIL_HEADER               1009
#define IDC_RADIO_UCS                   1010
#define IDC_RADIO_LOCAL                 1011
#define IDC_COPYRIGHT1                  1104
#define IDC_COPYRIGHT2                  1105
#define IDC_COPYRIGHT3                  1106
#define IDC_COPYRIGHT4                  1107
#define IDC_SERVER                      1108
#define IDC_USER                        1109
#define IDC_LOGINTIME                   1110
#define IDC_STATIC_LOGINTIME            1111
#define IDC_STATIC_SERVER               1112
#define IDC_STATIC_USER                 1113
#define IDC_STATIC_LOGINTIME2           1114
#define IDC_CLASSLIBVERSION             1115

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
