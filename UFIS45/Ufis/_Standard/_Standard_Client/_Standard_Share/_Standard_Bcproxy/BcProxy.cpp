// BcProxy.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <BcProxy.h>
#include <BcProxyDlg.h>
#include <initguid.h>
#include <BcProxy_i.c>
#include <BcPrxy.h>
#include <CCSGlobl.h>
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBcProxyApp

BEGIN_MESSAGE_MAP(CBcProxyApp, CWinApp)
	//{{AFX_MSG_MAP(CBcProxyApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBcProxyApp construction

CBcProxyApp::CBcProxyApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CBcProxyApp object

CBcProxyApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CBcProxyApp initialization

BOOL CBcProxyApp::InitInstance()
{
	if (!InitATL())
		return FALSE;

	AfxEnableControlContainer();

	if (!AfxSocketInit())
	{
		AfxMessageBox("IDP_SOCKETS_INIT_FAILED");
		return FALSE;
	}
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

//	if (cmdInfo.m_bRunEmbedded || cmdInfo.m_bRunAutomated)
//	{
//		return TRUE;
//	}



	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	// CCS-Initialization
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	CTime oltmptime = CTime::GetCurrentTime();
	CString oltmpstr = oltmptime.Format("Datum: %d.%m.%Y");
	//of_catch.open("C:\\Ufis\\System\\CritErr.txt", ios::app);
	of_catch.open(CCSLog::GetUfisSystemPath("\\CritErr.txt"), ios::app);
	of_catch << oltmpstr.GetBuffer(0) << endl;
	of_catch << "=================" << endl;



	/////////////////////////////////////////////////////////////////////////////
	// Standard CCS-Initialization
	/////////////////////////////////////////////////////////////////////////////

	
	ogLog.SetAppName(ogAppName);
	ogCommHandler.SetAppName(ogAppName);
	ogBcHandle.SetCloMessage("CLO");

    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
   		ogCommHandler.CleanUpCom();	
		ExitProcess(0);
   		return FALSE;
	}

	::UfisDllAdmin("TRACE", "ON", "FATAL");

	/*
	 * Init of Tablenames and Homeairport
	 */

	char pclConfigPath[256];
	char pclUser[256];
	char pclPassword[256];
	char pclDebug[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


	char pclShowMode[128];
	bgApplicationHidden = true;
    GetPrivateProfileString("BcProxy", "MODE", "HIDE", pclShowMode, sizeof pclShowMode, pclConfigPath);
	if(strcmp(pclShowMode, "SHOW") == 0)
	{
		bgApplicationHidden = false;
	}

    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "HAJ", pcgHome, sizeof pcgHome, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "HAJ", pcgTableExt, sizeof pcgTableExt, pclConfigPath);

    GetPrivateProfileString("GLOBAL", "USER", "DEFAULT", pclUser, sizeof pclUser, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "PASSWORD", "DEFAULT", pclPassword, sizeof pclPassword, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "DEBUG", "DEFAULT", pclDebug, sizeof pclDebug, pclConfigPath);


	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pcgHome);
	strcpy(CCSCedaData::pcmApplName, ogAppName);

	CBcProxyDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

	
CBcProxyModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
OBJECT_ENTRY(CLSID_BcPrxy, CBcPrxy)
END_OBJECT_MAP()

LONG CBcProxyModule::Unlock()
{
	AfxOleUnlockApp();
	return 0;
}

LONG CBcProxyModule::Lock()
{
	AfxOleLockApp();
	return 1;
}

LPCTSTR CBcProxyModule::FindOneOf(LPCTSTR p1, LPCTSTR p2)
{
	while (*p1 != NULL)
	{
		LPCTSTR p = p2;
		while (*p != NULL)
		{
			if (*p1 == *p)
				return CharNext(p1);
			p = CharNext(p);
		}
		p1++;
	}
	return NULL;
}

int CBcProxyApp::ExitInstance()
{
	if (m_bATLInited)
	{
		_Module.RevokeClassObjects();
		_Module.Term();
		CoUninitialize();
	}

	ogClientFilterArray.DeleteAll();
	ogCommHandler.CleanUpCom();	

	return CWinApp::ExitInstance();
}

BOOL CBcProxyApp::InitATL()
{
	m_bATLInited = TRUE;
	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox("Ole Init Failed");
		return FALSE;
	}

#if _WIN32_WINNT >= 0x0400
	HRESULT hRes = CoInitializeEx(NULL, COINIT_MULTITHREADED);
#else
	HRESULT hRes = CoInitialize(NULL);
#endif

	if (FAILED(hRes))
	{
		m_bATLInited = FALSE;
		return FALSE;
	}

	_Module.Init(ObjectMap, AfxGetInstanceHandle());
	_Module.dwThreadID = GetCurrentThreadId();

	LPTSTR lpCmdLine = GetCommandLine(); //this line necessary for _ATL_MIN_CRT
	TCHAR szTokens[] = _T("-/");

	BOOL bRun = TRUE;
	LPCTSTR lpszToken = _Module.FindOneOf(lpCmdLine, szTokens);
	while (lpszToken != NULL)
	{
		if (lstrcmpi(lpszToken, _T("UnregServer"))==0)
		{

			_Module.UpdateRegistryFromResource(IDR_BCPROXY, FALSE);
			_Module.UnregisterServer(TRUE); //TRUE means typelib is unreg'd
			bRun = FALSE;
			break;
		}
		if (lstrcmpi(lpszToken, _T("RegServer"))==0)
		{
						// Update the System Registry
			COleObjectFactory::UpdateRegistryAll();         // MFC Classes
			//VERIFY(SUCCEEDED(_Module.RegisterServer(TRUE)));// ATL Classes
			//COleObjectFactory::RegisterAll();

			_Module.UpdateRegistryFromResource(IDR_BCPROXY, TRUE);
			_Module.RegisterServer(TRUE);
			bRun = FALSE;
			break;
		}
		lpszToken = _Module.FindOneOf(lpszToken, szTokens);
	}

	if (!bRun)
	{
		m_bATLInited = FALSE;
		_Module.Term();
		CoUninitialize();
		return FALSE;
	}

	hRes = _Module.RegisterClassObjects(CLSCTX_LOCAL_SERVER, 
		REGCLS_MULTIPLEUSE);
	if (FAILED(hRes))
	{
		m_bATLInited = FALSE;
		CoUninitialize();
		return FALSE;
	}	
	return TRUE;
}
