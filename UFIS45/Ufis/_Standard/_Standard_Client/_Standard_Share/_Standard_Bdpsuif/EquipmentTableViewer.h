#ifndef __EquipmentTableViewer_H__
#define __EquipmentTableViewer_H__

#include <stdafx.h>
#include <CedaEquData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct EQUIPMENTTABLE_LINEDATA
{
	long 		Urno; 	// Eindeutige Datensatz-Nr.
	long		GKey;	// Equipment type urno
	CString		Gcde;	// Code
	CString		Enam;	// Name
	CString		Eqps;	// Parking stand
	CString		Etyp;	// Type
	CString		Ivnr;	// Inventory no.
	CString		Tele;	// telephone no.
	CString		Crqu;	// qualification
	CString		Rema;	// Remark
};

/////////////////////////////////////////////////////////////////////////////
// EquipmentTableViewer

	  
class EquipmentTableViewer : public CViewer
{
// Constructions
public:
    EquipmentTableViewer(CCSPtrArray<EQUDATA> *popData);
    ~EquipmentTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(EQUDATA *prpEquipment);
	int CompareEquipment(EQUIPMENTTABLE_LINEDATA *prpEquipment1, EQUIPMENTTABLE_LINEDATA *prpEquipment2);
    void MakeLines();
	void MakeLine(EQUDATA *prpEquipment);
	bool FindLine(long lpUrno, int &rilLineno);
	CString GetEquipmentType(long lpGKey);

// Operations
public:
	void DeleteAll();
	void CreateLine(EQUIPMENTTABLE_LINEDATA *prpEquipment);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(EQUIPMENTTABLE_LINEDATA *prpLine);
	void ProcessEquipmentChange(EQUDATA *prpEquipment);
	void ProcessEquipmentDelete(EQUDATA *prpEquipment);
	bool FindEquipment(char *prpEquipmentKeya, char *prpEquipmentKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomEquipmentTable;
	CCSPtrArray<EQUDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<EQUIPMENTTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(EQUIPMENTTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__EquipmentTableViewer_H__
