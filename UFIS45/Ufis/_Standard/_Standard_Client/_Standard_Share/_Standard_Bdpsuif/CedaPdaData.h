// CedaPdaData.h

#ifndef __CedaPdaData__
#define __CedaPdaData__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct PDADATA 
{
	char 	 Pdid[7]; 	// PDA ID
	char 	 Ipad[16]; 	// IP of PDA
	CTime 	 Cdat; 		// Erstellungsdatum
	CTime 	 Lstu; 		// Datum letzte �nderung
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime 	 Vafr; 		// G�ltig von
	CTime 	 Vato; 		// G�ltig bis
	CTime 	 Nafr; 		// G�ltig von
	CTime 	 Nato; 		// G�ltig bis
	long	 Ustf;		// STFTAB.URNO - assigend employee 
	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	PDADATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
		Vafr		=	TIMENULL;
		Vato		=	TIMENULL;
		Nafr		=	TIMENULL;
		Nato		=	TIMENULL;
	}

}; // end AadDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaPdaData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<PDADATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaPdaData();
	~CedaPdaData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(PDADATA *prpPda);
	bool InsertInternal(PDADATA *prpPda);
	bool Update(PDADATA *prpAad);
	bool UpdateInternal(PDADATA *prpPda);
	bool Delete(long lpUrno);
	bool DeleteInternal(PDADATA *prpPda);
	bool ReadSpecialData(CCSPtrArray<PDADATA> *popPda,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool Save(PDADATA *prpPda);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	PDADATA  *GetPdaByUrno(long lpUrno);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795

	// Private methods
private:
    void PrepareAadData(PDADATA *prpPdaData);
	CString omWhere; //Prf: 8795
	bool ValidatePdaBcData(const long& lrpUnro); //Prf: 8795

};

//---------------------------------------------------------------------------------------------------------

#endif //__CedaPdaData__
