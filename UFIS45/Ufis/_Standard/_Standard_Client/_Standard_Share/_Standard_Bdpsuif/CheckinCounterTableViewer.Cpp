// CheckinCounterTableViewer.cpp 
//

#include <stdafx.h>
#include <CheckinCounterTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif
  

// Local function prototype
static void CheckinCounterTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// CheckinCounterTableViewer
//

CheckinCounterTableViewer::CheckinCounterTableViewer(CCSPtrArray<CICDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomCheckinCounterTable = NULL;
    ogDdx.Register(this, CIC_CHANGE, CString("CHECKINCOUNTERTABLEVIEWER"), CString("CheckinCounter Update/new"), CheckinCounterTableCf);
    ogDdx.Register(this, CIC_DELETE, CString("CHECKINCOUNTERTABLEVIEWER"), CString("CheckinCounter Delete"), CheckinCounterTableCf);
}

//-----------------------------------------------------------------------------------------------

CheckinCounterTableViewer::~CheckinCounterTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void CheckinCounterTableViewer::Attach(CCSTable *popTable)
{
    pomCheckinCounterTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void CheckinCounterTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void CheckinCounterTableViewer::MakeLines()
{
	int ilCheckinCounterCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilCheckinCounterCount; ilLc++)
	{
		CICDATA *prlCheckinCounterData = &pomData->GetAt(ilLc);
		MakeLine(prlCheckinCounterData);
	}
}

//-----------------------------------------------------------------------------------------------

void CheckinCounterTableViewer::MakeLine(CICDATA *prpCheckinCounter)
{
	//if( !IsPassFilter(prpCheckinCounter)) return;

    // Update viewer data for this shift record
    CHECKINCOUNTERTABLE_LINEDATA rlCheckinCounter;

	rlCheckinCounter.Urno = prpCheckinCounter->Urno; 
	rlCheckinCounter.Cnam = prpCheckinCounter->Cnam; 
	rlCheckinCounter.Catr = prpCheckinCounter->Catr; 
	rlCheckinCounter.Edpe = prpCheckinCounter->Edpe; 
	rlCheckinCounter.Rgbl = prpCheckinCounter->Rgbl; 
	rlCheckinCounter.Term = prpCheckinCounter->Term; 
	rlCheckinCounter.Hall = prpCheckinCounter->Hall; 
	rlCheckinCounter.Tele = prpCheckinCounter->Tele; 
	rlCheckinCounter.Tel2 = prpCheckinCounter->Tel2;
	rlCheckinCounter.Cicr = prpCheckinCounter->Cicr; 
	rlCheckinCounter.Home = prpCheckinCounter->Home; 
	rlCheckinCounter.Cbaz = prpCheckinCounter->Cbaz; 
	rlCheckinCounter.Cicl = prpCheckinCounter->Cicl; 
	rlCheckinCounter.Vafr = prpCheckinCounter->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlCheckinCounter.Vato = prpCheckinCounter->Vato.Format("%d.%m.%Y %H:%M"); 

	CreateLine(&rlCheckinCounter);
}

//-----------------------------------------------------------------------------------------------

void CheckinCounterTableViewer::CreateLine(CHECKINCOUNTERTABLE_LINEDATA *prpCheckinCounter)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareCheckinCounter(prpCheckinCounter, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	CHECKINCOUNTERTABLE_LINEDATA rlCheckinCounter;
	rlCheckinCounter = *prpCheckinCounter;
    omLines.NewAt(ilLineno, rlCheckinCounter);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void CheckinCounterTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 10;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomCheckinCounterTable->SetShowSelection(TRUE);
	pomCheckinCounterTable->ResetContent();

	CString olFormat;
	if (ogBasicData.IsExtendedLMEnabled())
		olFormat = LoadStg(IDS_STRING1093);
	else
		olFormat = LoadStg(IDS_STRING258);

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(olFormat,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// name

	if (ogBasicData.IsExtendedLMEnabled())
	{
		rlHeader.Length = 42; 
		rlHeader.Text = GetListItem(olFormat,ilPos++,false,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// attr
	}

	rlHeader.Length = 83; 
	rlHeader.Text = GetListItem(olFormat,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// EDP

	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(olFormat,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// lounge

	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(olFormat,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Terminal

	rlHeader.Length = 66; 
	rlHeader.Text = GetListItem(olFormat,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// hall

	//uhi 22.3.01
	rlHeader.Length = 66; 
	rlHeader.Text = GetListItem(olFormat,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// region

	rlHeader.Length = 40; 
	rlHeader.Text = GetListItem(olFormat,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// line

	rlHeader.Length = 83; 
	rlHeader.Text = GetListItem(olFormat,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// phone 1

	rlHeader.Length = 83; 
	rlHeader.Text = GetListItem(olFormat,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// phone 2

	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(olFormat,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// valid from

	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(olFormat,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// valid to

	rlHeader.Length = 40; 
	rlHeader.Text = GetListItem(olFormat,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// carousel / target box



	pomCheckinCounterTable->SetHeaderFields(omHeaderDataArray);

	pomCheckinCounterTable->SetDefaultSeparator();
	pomCheckinCounterTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Cnam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if (ogBasicData.IsExtendedLMEnabled())
		{
			rlColumnData.Text = omLines[ilLineNo].Catr;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		rlColumnData.Text = omLines[ilLineNo].Edpe;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rgbl;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Term;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hall;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Cicr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Cicl;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tele;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tel2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Cbaz;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomCheckinCounterTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomCheckinCounterTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString CheckinCounterTableViewer::Format(CHECKINCOUNTERTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

BOOL CheckinCounterTableViewer::FindCheckinCounter(char *pcpCheckinCounterKeya, char *pcpCheckinCounterKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpCheckinCounterKeya) &&
			 (omLines[ilItem].Keyd == pcpCheckinCounterKeyd)    )
			return TRUE;*/
	}
	return FALSE;
}

//-----------------------------------------------------------------------------------------------

static void CheckinCounterTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CheckinCounterTableViewer *polViewer = (CheckinCounterTableViewer *)popInstance;
    if (ipDDXType == CIC_CHANGE) polViewer->ProcessCheckinCounterChange((CICDATA *)vpDataPointer);
    if (ipDDXType == CIC_DELETE) polViewer->ProcessCheckinCounterDelete((CICDATA *)vpDataPointer);
} 

//-----------------------------------------------------------------------------------------------
void CheckinCounterTableViewer::ProcessCheckinCounterChange(CICDATA *prpCheckinCounter)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpCheckinCounter->Cnam;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (ogBasicData.IsExtendedLMEnabled())
	{
		rlColumn.Text = prpCheckinCounter->Catr;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}

	rlColumn.Text = prpCheckinCounter->Edpe;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpCheckinCounter->Rgbl;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpCheckinCounter->Term;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpCheckinCounter->Hall;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpCheckinCounter->Cicr;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpCheckinCounter->Cicl;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpCheckinCounter->Tele;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpCheckinCounter->Tel2;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpCheckinCounter->Vafr.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpCheckinCounter->Vato.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpCheckinCounter->Cbaz;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);


	if (FindLine(prpCheckinCounter->Urno, ilItem))
	{
        CHECKINCOUNTERTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpCheckinCounter->Urno; 
		prlLine->Cnam = prpCheckinCounter->Cnam; 
		prlLine->Catr = prpCheckinCounter->Catr; 
		prlLine->Hall = prpCheckinCounter->Hall; 
		prlLine->Edpe = prpCheckinCounter->Edpe; 
		prlLine->Rgbl = prpCheckinCounter->Rgbl; 
		prlLine->Cicr = prpCheckinCounter->Cicr;
		prlLine->Home = prpCheckinCounter->Home;
		prlLine->Cbaz = prpCheckinCounter->Cbaz;
		prlLine->Cicl = prpCheckinCounter->Cicl;
		prlLine->Term = prpCheckinCounter->Term;
		prlLine->Tele = prpCheckinCounter->Tele; 
		prlLine->Tel2 = prpCheckinCounter->Tel2;
		
		prlLine->Vafr = prpCheckinCounter->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpCheckinCounter->Vato.Format("%d.%m.%Y %H:%M"); 
	
		pomCheckinCounterTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomCheckinCounterTable->DisplayTable();
	}
	else
	{
		MakeLine(prpCheckinCounter);
		if (FindLine(prpCheckinCounter->Urno, ilItem))
		{
	        CHECKINCOUNTERTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomCheckinCounterTable->AddTextLine(olLine, (void *)prlLine);
				pomCheckinCounterTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void CheckinCounterTableViewer::ProcessCheckinCounterDelete(CICDATA *prpCheckinCounter)
{
	int ilItem;
	if (FindLine(prpCheckinCounter->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomCheckinCounterTable->DeleteTextLine(ilItem);
		pomCheckinCounterTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

BOOL CheckinCounterTableViewer::IsPassFilter(CICDATA *prpCheckinCounter)
{

	return TRUE;
}

//-----------------------------------------------------------------------------------------------

int CheckinCounterTableViewer::CompareCheckinCounter(CHECKINCOUNTERTABLE_LINEDATA *prpCheckinCounter1, CHECKINCOUNTERTABLE_LINEDATA *prpCheckinCounter2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpCheckinCounter1->Tifd == prpCheckinCounter2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpCheckinCounter1->Tifd > prpCheckinCounter2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void CheckinCounterTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

BOOL CheckinCounterTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return TRUE;
	}
    return FALSE;
}
//-----------------------------------------------------------------------------------------------

void CheckinCounterTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void CheckinCounterTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING166);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool CheckinCounterTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool CheckinCounterTableViewer::PrintTableLine(CHECKINCOUNTERTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			if (ogBasicData.IsExtendedLMEnabled())
			{
				switch(i)
				{
				case 0:
					{
						rlElement.FrameLeft  = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Cnam;
					}
					break;
				case 1:
					{
						rlElement.Text		= prpLine->Catr;
					}
					break;
				case 2:
					{
						rlElement.Text		= prpLine->Edpe;
					}
					break;
				case 3:
					{
						rlElement.Text		= prpLine->Rgbl;
					}
					break;
				case 4:
					{
						rlElement.Text		= prpLine->Term;
					}
					break;
				case 5:
					{
						rlElement.Text		= prpLine->Hall;
					}
					break;
				case 6:
					{
						rlElement.Text		= prpLine->Cicr;
					}
					break;
				case 7:
					{
						rlElement.Text		= prpLine->Cicl;
					}
					break;
				case 8:
					{
						rlElement.Text		= prpLine->Tele;
					}
					break;
				case 9:
					{
						rlElement.Text		= prpLine->Tel2;
					}
					break;
				case 10:
					{
						rlElement.Text		= prpLine->Vafr;
					}
					break;
				case 11:
					{
						rlElement.Text		= prpLine->Vato;
					}
					break;
				case 12:
					{
						rlElement.FrameRight = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Cbaz;
					}
					break;
				}
			}
			else
			{
				switch(i)
				{
				case 0:
					{
						rlElement.FrameLeft  = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Cnam;
					}
					break;
				case 1:
					{
						rlElement.Text		= prpLine->Edpe;
					}
					break;
				case 2:
					{
						rlElement.Text		= prpLine->Rgbl;
					}
					break;
				case 3:
					{
						rlElement.Text		= prpLine->Term;
					}
					break;
				case 4:
					{
						rlElement.Text		= prpLine->Hall;
					}
					break;
				case 5:
					{
						rlElement.Text		= prpLine->Cicr;
					}
					break;
				case 6:
					{
						rlElement.Text		= prpLine->Cicl;
					}
					break;
				case 7:
					{
						rlElement.Text		= prpLine->Tele;
					}
					break;
				case 8:
					{
						rlElement.Text		= prpLine->Tel2;
					}
					break;
				case 9:
					{
						rlElement.Text		= prpLine->Vafr;
					}
					break;
				case 10:
					{
						rlElement.Text		= prpLine->Vato;
					}
					break;
				case 11:
					{
						rlElement.FrameRight = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Cbaz;
					}
					break;
				}
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
