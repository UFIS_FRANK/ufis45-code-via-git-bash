#ifndef __STANDARDVERKETTUNGENTABLEVIEWER_H__
#define __STANDARDVERKETTUNGENTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaStrData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct STANDARDVERKETTUNGENTABLE_LINEDATA
{
	long	Urno;
	CString	Act3;
	CString	Actm;
	CString	Flca;
	CString	Flna;
	CString	Flsa;
	CString	Daya;
	CString	Flcd;
	CString	Flnd;
	CString	Flsd;
	CString	Dayd;
	CString Vafr;
	CString Vato;
};

/////////////////////////////////////////////////////////////////////////////
// StandardverkettungenTableViewer

class StandardverkettungenTableViewer : public CViewer
{
// Constructions
public:
    StandardverkettungenTableViewer(CCSPtrArray<STRDATA> *popData);
    ~StandardverkettungenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(STRDATA *prpStandardverkettungen);
	int CompareStandardverkettungen(STANDARDVERKETTUNGENTABLE_LINEDATA *prpStandardverkettungen1, STANDARDVERKETTUNGENTABLE_LINEDATA *prpStandardverkettungen2);
    void MakeLines();
	void MakeLine(STRDATA *prpStandardverkettungen);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(STANDARDVERKETTUNGENTABLE_LINEDATA *prpStandardverkettungen);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(STANDARDVERKETTUNGENTABLE_LINEDATA *prpLine);
	void ProcessStandardverkettungenChange(STRDATA *prpStandardverkettungen);
	void ProcessStandardverkettungenDelete(STRDATA *prpStandardverkettungen);
	bool FindStandardverkettungen(char *prpStandardverkettungenKeya, char *prpStandardverkettungenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomStandardverkettungenTable;
	CCSPtrArray<STRDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<STANDARDVERKETTUNGENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(STANDARDVERKETTUNGENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__STANDARDVERKETTUNGENTABLEVIEWER_H__
