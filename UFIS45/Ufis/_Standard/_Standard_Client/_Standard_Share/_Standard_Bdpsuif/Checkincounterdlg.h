#if !defined(AFX_HECKINCOUNTERDLG_H__56BF0B48_2049_11D1_B38A_0000C016B067__INCLUDED_)
#define AFX_HECKINCOUNTERDLG_H__56BF0B48_2049_11D1_B38A_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CheckinCounterDlg.h : header file
//
#include <CedaCICData.h>
#include <CedaBlkData.h>
#include <CedaOccData.h>
#include <CCSEdit.h>
#include <CCSTable.h>

/////////////////////////////////////////////////////////////////////////////
// CheckinCounterDlg dialog

class CheckinCounterDlg : public CDialog
{
// Construction
public:
	CheckinCounterDlg(CICDATA *popCIC,CWnd* pParent = NULL);   // standard constructor
	CheckinCounterDlg(CICDATA *popCIC,int ipDlg,CWnd* pParent = NULL);   // standard constructor
	~CheckinCounterDlg();

	CCSPtrArray<BLKDATA> omBlkPtrA;
	CCSPtrArray<BLKDATA> omDeleteBlkPtrA;

	CCSPtrArray<OCCDATA> omOccPtrA;
	CCSPtrArray<OCCDATA> omDeleteOccPtrA;

// Dialog Data
	//{{AFX_DATA(CheckinCounterDlg)
	enum { IDD = IDD_CHECKINCOUNTERDLG };
	CCSEdit	m_TEL2;
	CButton	m_NOAVNEW;
	CButton	m_NOAVDEL;
	CStatic	m_NOAVFRAME;
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_CNAM;
	CCSEdit	m_GRUP;
	CCSEdit	m_EDPE;
	CCSEdit	m_RGBL;
	CCSEdit	m_TELE;
	CCSEdit	m_TERM;
	CCSEdit	m_HALL;
	CCSEdit	m_CICR;
	CCSEdit	m_CBAZ;
	CCSEdit	m_CICL;
	CCSEdit	m_HOME;
	CButton	m_UTC;
	CButton	m_LOCAL;
	CButton m_APIS;
	CButton m_WESC;
	CCSEdit m_OUBL;
	CCSEdit m_INBL;
	CStatic	m_EQUIP;
	CStatic	m_OBELT;
	CStatic	m_IBELT;

	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CheckinCounterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CheckinCounterDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnNoavDel();
	afx_msg void OnNoavNew();
	afx_msg	void OnUTC();
	afx_msg void OnLocal();	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


protected:

	CICDATA		*pomCIC;
	CCSTable	*pomTable;
	int			imLastSelection;

	void MakeNoavTable(CString opTabn, CString opBurn);
	void ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo);
	void UpdateNoavTable();
	void HandleEquipment();

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HECKINCOUNTERDLG_H__56BF0B48_2049_11D1_B38A_0000C016B067__INCLUDED_)
