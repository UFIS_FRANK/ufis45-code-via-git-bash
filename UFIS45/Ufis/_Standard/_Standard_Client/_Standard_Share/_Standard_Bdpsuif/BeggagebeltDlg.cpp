// BeggagebeltDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <BeggagebeltDlg.h>
#include <PrivList.h>
#include <NotAvailableDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BeggagebeltDlg dialog


BeggagebeltDlg::BeggagebeltDlg(BLTDATA *popBLT,CWnd* pParent /*=NULL*/) 
: CDialog(BeggagebeltDlg::IDD, pParent)
{
	pomBLT = popBLT;
	pomTable = NULL;
	pomTable = new CCSTable;

	//{{AFX_DATA_INIT(BeggagebeltDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

BeggagebeltDlg::BeggagebeltDlg(BLTDATA *popBLT,int ipDlg,CWnd* pParent /*=NULL*/) 
: CDialog(ipDlg, pParent)
{
	pomBLT = popBLT;
	pomTable = NULL;
	pomTable = new CCSTable;

	//{{AFX_DATA_INIT(BeggagebeltDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

BeggagebeltDlg::~BeggagebeltDlg()
{
	omBlkPtrA.DeleteAll();
	omDeleteBlkPtrA.DeleteAll();
	delete pomTable;

}

void BeggagebeltDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BeggagebeltDlg)
	DDX_Control(pDX, IDC_MAXF, m_MAXF);
	DDX_Control(pDX, IDC_DEFD, m_DEFD);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_NOAVNEW, m_NOAVNEW);
	DDX_Control(pDX, IDC_NOAVDEL, m_NOAVDEL);
	DDX_Control(pDX, IDC_NOAV,	 m_NOAVFRAME);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_BNAM,	 m_BNAM);
	DDX_Control(pDX, IDC_TELE,	 m_TELE);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_TERM,	 m_TERM);
	DDX_Control(pDX, IDC_STAT,	 m_STAT);
	DDX_Control(pDX, IDC_BLTT,	 m_BLTT);
	DDX_Control(pDX, IDC_HOME,	 m_HOME);
	DDX_Control(pDX, IDC_RADIO_UTC,	 m_UTC);
	DDX_Control(pDX, IDC_RADIO_LOCAL,m_LOCAL);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BeggagebeltDlg, CDialog)
	//{{AFX_MSG_MAP(BeggagebeltDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVDEL, OnNoavDel)
	ON_BN_CLICKED(IDC_NOAVNEW, OnNoavNew)
	ON_BN_CLICKED(IDC_RADIO_UTC,	OnUTC)
	ON_BN_CLICKED(IDC_RADIO_LOCAL,	OnLocal)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BeggagebeltDlg message handlers
//----------------------------------------------------------------------------------------

BOOL BeggagebeltDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING179) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("BEGGAGEBELTDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomBLT->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomBLT->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomBLT->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomBLT->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomBLT->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomBLT->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomBLT->);
	m_GRUP.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_BNAM.SetFormat("x|#x|#x|#x|#x|#");
	m_BNAM.SetTextLimit(1,5);
	m_BNAM.SetBKColor(YELLOW);
	m_BNAM.SetTextErrColor(RED);
	m_BNAM.SetInitText(pomBLT->Bnam);
	m_BNAM.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_BNAM"));
	//------------------------------------
	m_STAT.SetTypeToString("X(3)",3,1);
	m_STAT.SetTextErrColor(RED);
	m_STAT.SetInitText(pomBLT->Stat);
	m_STAT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_STAT"));
	//------------------------------------
	m_BLTT.SetTypeToString("X(1)",1,1);
	m_BLTT.SetTextErrColor(RED);
	m_BLTT.SetInitText(pomBLT->Bltt);
	m_BLTT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_BLTT"));
	//------------------------------------
	m_HOME.SetTypeToString("X(3)",3,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomBLT->Home);
	m_HOME.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_HOME"));
	//------------------------------------
	m_TERM.SetFormat("x|#");
	m_TERM.SetTextLimit(0,1);
	m_TERM.SetTextErrColor(RED);
	m_TERM.SetInitText(pomBLT->Term);
	m_TERM.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_TERM"));
	//------------------------------------
	m_TELE.SetTypeToString("X(10)",10,0);
	m_TELE.SetTextErrColor(RED);
	m_TELE.SetInitText(pomBLT->Tele);
	m_TELE.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_TELE"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomBLT->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomBLT->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomBLT->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomBLT->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_VATO"));
	//------------------------------------
	m_MAXF.SetFormat("#");
	m_MAXF.SetTextLimit(0,1);
	m_MAXF.SetTextErrColor(RED);
	m_MAXF.SetInitText(pomBLT->Maxf);
	m_MAXF.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_MAXF"));
	//------------------------------------
	m_DEFD.SetFormat("###");
	m_DEFD.SetTextLimit(0,3);
	m_DEFD.SetTextErrColor(RED);
	m_DEFD.SetInitText(pomBLT->Defd);
	m_DEFD.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_DEFD"));

	if (ogBasicData.UseLocalTimeAsDefault())
	{
		this->m_UTC.SetCheck(0);
		this->m_LOCAL.SetCheck(1);
		imLastSelection = 2;
	}
	else
	{
		this->m_UTC.SetCheck(1);
		this->m_LOCAL.SetCheck(0);
		imLastSelection = 1;
	}

	CString olUrno;
	olUrno.Format("%d",pomBLT->Urno);
	MakeNoavTable("BLT", olUrno);
	
	return TRUE;
}

//----------------------------------------------------------------------------------------

void BeggagebeltDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_BNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_BNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_TERM.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING296) + ogNotFormat;
	}
	if(m_TELE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING294) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	if(m_MAXF.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING855) + ogNotFormat;
	}
	
	if(m_DEFD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING856) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olBnam;
		char clWhere[100];
		CCSPtrArray<BLTDATA> olBltCPA;

		m_BNAM.GetWindowText(olBnam);
		sprintf(clWhere,"WHERE BNAM='%s'",olBnam);
		if(ogBLTData.ReadSpecialData(&olBltCPA,clWhere,"URNO,BNAM",false) == true)
		{
			if(olBltCPA[0].Urno != pomBLT->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
			}
		}
		olBltCPA.DeleteAll();
	}
	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_BNAM.GetWindowText(pomBLT->Bnam,7);	// length with terminating NUL - byte
		m_TERM.GetWindowText(pomBLT->Term,3);	// length with terminating NUL - byte
		m_TELE.GetWindowText(pomBLT->Tele,12);	// length with terminating NUL - byte
		m_STAT.GetWindowText(pomBLT->Stat,4);	// length with terminating NUL - byte
		m_BLTT.GetWindowText(pomBLT->Bltt,2);	// length with terminating NUL - byte
		m_HOME.GetWindowText(pomBLT->Home,4);	// length with terminating NUL - byte
		pomBLT->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomBLT->Vato = DateHourStringToDate(olVatod,olVatot);
		m_MAXF.GetWindowText(pomBLT->Maxf,2);
		m_DEFD.GetWindowText(pomBLT->Defd,4);
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_BNAM.SetFocus();
		m_BNAM.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------
void BeggagebeltDlg::MakeNoavTable(CString opTabn, CString opBurn)
{
	CRect olRectBorder;
	m_NOAVFRAME.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);
	pomTable->SetShowSelection(TRUE);
	pomTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	pomTable->SetHeaderFields(olHeaderDataArray);
	pomTable->SetDefaultSeparator();
	olHeaderDataArray.DeleteAll();

	char clWhere[100];
	CCSPtrArray<BLKDATA> olBlkCPA;
	sprintf(clWhere,"WHERE TABN='%s' AND BURN='%s' ORDER BY NAFR DESC",opTabn,opBurn);
	ogBlkData.ReadSpecialData(&omBlkPtrA,clWhere,"",false);

	UpdateNoavTable();
}

//----------------------------------------------------------------------------------------
void BeggagebeltDlg::UpdateNoavTable()
{
	int ilLines = omBlkPtrA.GetSize();

	pomTable->ResetContent();
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		CTime olNafr = omBlkPtrA[ilLineNo].Nafr;
		CTime olNato = omBlkPtrA[ilLineNo].Nato;
		if (this->imLastSelection == 2)	// local
		{
			ogBasicData.UtcToLocal(olNafr);
			ogBasicData.UtcToLocal(olNato);
		}

		rlColumnData.Text = olNafr.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = olNato.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Days;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		char pclTmp[10]="";
		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tifr) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = omBlkPtrA[ilLineNo].Tifr;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTifr);
				rlColumnData.Text = olTifr.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tifr).Left(2), CString(omBlkPtrA[ilLineNo].Tifr).Right(2));
				rlColumnData.Text = pclTmp;
			}
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tito) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = omBlkPtrA[ilLineNo].Tito;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTito);
				rlColumnData.Text = olTito.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tito).Left(2), CString(omBlkPtrA[ilLineNo].Tito).Right(2));
				rlColumnData.Text = pclTmp;
			}
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		(*omBlkPtrA[ilLineNo].Type == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Resn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomTable->DisplayTable();

}
//----------------------------------------------------------------------------------------

void BeggagebeltDlg::ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo)
{
	if(prpBlk != NULL)
	{
		TABLE_COLUMN rlColumn;
		CCSPtrArray <TABLE_COLUMN> olLine;

		rlColumn.Lineno = 0;
		rlColumn.VerticalSeparator = SEPA_NONE;
		rlColumn.SeparatorType = SEPA_NONE;
		rlColumn.BkColor = WHITE;
		rlColumn.Font = &ogCourier_Regular_10;
		rlColumn.Alignment = COLALIGN_LEFT;

		CTime olNafr = prpBlk->Nafr;
		CTime olNato = prpBlk->Nato;
		if (this->imLastSelection == 2)	// local
		{
			ogBasicData.UtcToLocal(olNafr);
			ogBasicData.UtcToLocal(olNato);
		}

		int ilColumnNo = 0;
		rlColumn.Text = olNafr.Format("%d.%m.%Y %H:%M");
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = olNato.Format("%d.%m.%Y %H:%M"); 
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Days;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		char pclTmp[10]="";
		rlColumn.Text = "";
		if(strlen(prpBlk->Tifr) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = prpBlk->Tifr;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTifr);
				rlColumn.Text = olTifr.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(prpBlk->Tifr).Left(2), CString(prpBlk->Tifr).Right(2));
				rlColumn.Text = pclTmp;
			}
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text = "";
		if(strlen(prpBlk->Tito) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = prpBlk->Tito;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTito);
				rlColumn.Text = olTito.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(prpBlk->Tito).Left(2), CString(prpBlk->Tito).Right(2));
				rlColumn.Text = pclTmp;
			}
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		(*prpBlk->Type == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Resn;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		if(ipLineNo>-1)
		{
			pomTable->ChangeTextLine(ipLineNo, &olLine, (void *)prpBlk);
			pomTable->DisplayTable();
		}
		else
		{
			if(prpBlk != NULL)
			{
				pomTable->AddTextLine(olLine, (void *)prpBlk);
				pomTable->DisplayTable();
			}
		}
		olLine.DeleteAll();
	}
	else
	{
		if(ipLineNo>-1)
		{
			pomTable->DeleteTextLine(ipLineNo);
			pomTable->DisplayTable();
		}
	}
}

//----------------------------------------------------------------------------------------

LONG BeggagebeltDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(ogPrivList.GetStat("BEGGAGEBELTDLG.m_NOAVCHA") == '1')
	{
		int ilLineNo = pomTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
		}
		else
		{
			BLKDATA *prlBlk = &omBlkPtrA[ilLineNo];
			if (prlBlk != NULL)
			{
				BLKDATA rlBlk = *prlBlk;
				NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
				m_BNAM.GetWindowText(polDlg->omName);
				if(polDlg->DoModal() == IDOK)
				{
					*prlBlk = rlBlk;
					if(prlBlk->IsChanged != DATA_NEW) prlBlk->IsChanged = DATA_CHANGED;
					ChangeNoavTable(prlBlk, ilLineNo);
				}
				delete polDlg;
			}
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void BeggagebeltDlg::OnNoavNew() 
{
	
	BLKDATA rlBlk;
	if(ogPrivList.GetStat("BEGGAGEBELTDLG.m_NOAVNEW") == '1')
	{
		NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
		m_BNAM.GetWindowText(polDlg->omName);
		if(polDlg->DoModal() == IDOK)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = rlBlk;
			omBlkPtrA.Add(prlBlk);
			prlBlk->IsChanged = DATA_NEW;
			ChangeNoavTable(prlBlk, -1);
		}
		delete polDlg;
	}
}

//----------------------------------------------------------------------------------------

void BeggagebeltDlg::OnNoavDel() 
{
	int ilLineNo = pomTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
	}
	else
	{
		if(ogPrivList.GetStat("BEGGAGEBELTDLG.m_NOAVDEL") == '1')
		{
			if (&omBlkPtrA[ilLineNo] != NULL)
			{
				BLKDATA *prlBlk = new BLKDATA;
				*prlBlk = omBlkPtrA[ilLineNo];
				prlBlk->IsChanged = DATA_DELETED;
				omDeleteBlkPtrA.Add(prlBlk);
				omBlkPtrA.DeleteAt(ilLineNo);
				ChangeNoavTable(NULL, ilLineNo);
			}
		}
	}
}

//----------------------------------------------------------------------------------------
void BeggagebeltDlg::OnUTC() 
{
	if (imLastSelection == 1)
		return;

	imLastSelection = 1;

	UpdateNoavTable();
}

//----------------------------------------------------------------------------------------
void BeggagebeltDlg::OnLocal() 
{
	if (imLastSelection == 2)
		return;

	imLastSelection = 2;

	UpdateNoavTable();
}
