// EngineTypeTableViewer.cpp 
//

#include <stdafx.h>
#include <EngineTypeTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void EngineTypeTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// EngineTypeTableViewer
//

EngineTypeTableViewer::EngineTypeTableViewer(CCSPtrArray<ENTDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomEngineTypeTable = NULL;
    ogDdx.Register(this, ENT_CHANGE, CString("EngineTypeTableViewer"), CString("EngineType Update"), EngineTypeTableCf);
    ogDdx.Register(this, ENT_NEW,    CString("EngineTypeTableViewer"), CString("EngineType New"),    EngineTypeTableCf);
    ogDdx.Register(this, ENT_DELETE, CString("EngineTypeTableViewer"), CString("EngineType Delete"), EngineTypeTableCf);
}

//-----------------------------------------------------------------------------------------------

EngineTypeTableViewer::~EngineTypeTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void EngineTypeTableViewer::Attach(CCSTable *popTable)
{
    pomEngineTypeTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void EngineTypeTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void EngineTypeTableViewer::MakeLines()
{
	int ilEngineTypeCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilEngineTypeCount; ilLc++)
	{
		ENTDATA *prlEngineTypeData = &pomData->GetAt(ilLc);
		MakeLine(prlEngineTypeData);
	}
}

//-----------------------------------------------------------------------------------------------

void EngineTypeTableViewer::MakeLine(ENTDATA *prpEngineType)
{

    //if( !IsPassFilter(prpEngineType)) return;

    // Update viewer data for this shift record
    ENGINETYPETABLE_LINEDATA rlEngineType;

	rlEngineType.Urno = prpEngineType->Urno;
	rlEngineType.Entc = prpEngineType->Entc;
	rlEngineType.Enam = prpEngineType->Enam;

	CreateLine(&rlEngineType);
}

//-----------------------------------------------------------------------------------------------

void EngineTypeTableViewer::CreateLine(ENGINETYPETABLE_LINEDATA *prpEngineType)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareEngineType(prpEngineType, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	ENGINETYPETABLE_LINEDATA rlEngineType;
	rlEngineType = *prpEngineType;
    omLines.NewAt(ilLineno, rlEngineType);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void EngineTypeTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomEngineTypeTable->SetShowSelection(TRUE);
	pomEngineTypeTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING641),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING641),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
/*	rlHeader.Length = 498; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING641),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
*/	pomEngineTypeTable->SetHeaderFields(omHeaderDataArray);

	pomEngineTypeTable->SetDefaultSeparator();
	pomEngineTypeTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Entc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Enam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomEngineTypeTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomEngineTypeTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString EngineTypeTableViewer::Format(ENGINETYPETABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool EngineTypeTableViewer::FindEngineType(char *pcpEngineTypeKeya, char *pcpEngineTypeKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpEngineTypeKeya) &&
			 (omLines[ilItem].Keyd == pcpEngineTypeKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void EngineTypeTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    EngineTypeTableViewer *polViewer = (EngineTypeTableViewer *)popInstance;
    if (ipDDXType == ENT_CHANGE || ipDDXType == ENT_NEW) polViewer->ProcessEngineTypeChange((ENTDATA *)vpDataPointer);
    if (ipDDXType == ENT_DELETE) polViewer->ProcessEngineTypeDelete((ENTDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void EngineTypeTableViewer::ProcessEngineTypeChange(ENTDATA *prpEngineType)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpEngineType->Entc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpEngineType->Enam;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	
	if (FindLine(prpEngineType->Urno, ilItem))
	{
        ENGINETYPETABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpEngineType->Urno;
		prlLine->Entc = prpEngineType->Entc;
		prlLine->Enam = prpEngineType->Enam;


		pomEngineTypeTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomEngineTypeTable->DisplayTable();
	}
	else
	{
		MakeLine(prpEngineType);
		if (FindLine(prpEngineType->Urno, ilItem))
		{
	        ENGINETYPETABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomEngineTypeTable->AddTextLine(olLine, (void *)prlLine);
				pomEngineTypeTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void EngineTypeTableViewer::ProcessEngineTypeDelete(ENTDATA *prpEngineType)
{
	int ilItem;
	if (FindLine(prpEngineType->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomEngineTypeTable->DeleteTextLine(ilItem);
		pomEngineTypeTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool EngineTypeTableViewer::IsPassFilter(ENTDATA *prpEngineType)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int EngineTypeTableViewer::CompareEngineType(ENGINETYPETABLE_LINEDATA *prpEngineType1, ENGINETYPETABLE_LINEDATA *prpEngineType2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpEngineType1->Tifd == prpEngineType2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpEngineType1->Tifd > prpEngineType2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void EngineTypeTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool EngineTypeTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void EngineTypeTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void EngineTypeTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING619);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool EngineTypeTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool EngineTypeTableViewer::PrintTableLine(ENGINETYPETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Entc;
				break;
			case 1:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Enam;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
