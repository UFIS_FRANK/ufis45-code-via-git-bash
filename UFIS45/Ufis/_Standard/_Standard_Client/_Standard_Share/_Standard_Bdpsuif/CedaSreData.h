// CedaSreData.h

#ifndef __CEDAPSREDATA__
#define __CEDAPSREDATA__
 
#include "stdafx.h"
#include "CCSCedaData.h"
#include "basicdata.h"
#include <afxdisp.h>
#include "resrc1.h"		// main symbols#
//---------------------------------------------------------------------------------------------------------

struct SREDATA 
{
	COleDateTime Cdat;
	char Hopo[3+2];
	COleDateTime Lstu;
	//char Regc[5+2];
	char Regi[1+2];
	//char Regn[40+2];
	long Surn;			// Referenz auf STF.URNO
	long Urno;			// Eindeutige Datensatz-Nr.
	char Usec[32+2];
	char Useu[32+2];
	
	COleDateTime Vpfr;	// G�ltig von
	COleDateTime Vpto;	// G�ltig bis

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SREDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		//Vpfr=COleDateTime(COleDateTime((time_t)-1)),Vpto=COleDateTime(COleDateTime((time_t)-1));; //<zB.(FIELD_DATE Felder)
		IsChanged = DATA_UNCHANGED;
	}

}; // end SREDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSreData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapPtrToPtr omStaffUrnoMap;

    CCSPtrArray<SREDATA> omData;

	char pcmListOfFields[2048];

// OSorations
public:
    CedaSreData();
	~CedaSreData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SREDATA *prpSre);
	bool InsertInternal(SREDATA *prpSre);
	bool Update(SREDATA *prpSre);
	bool UpdateInternal(SREDATA *prpSre);
	bool Delete(long lpUrno);
	bool DeleteInternal(SREDATA *prpSre);
	bool ReadSpecialData(CCSPtrArray<SREDATA> *popSre,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SREDATA *prpSre);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SREDATA  *GetSreByUrno(long lpUrno);
	SREDATA *GetValidSreBySurn(long lpSurn);
	void GetAllSreBySurn(long lpSurn,CCSPtrArray<SREDATA> &ropList);
	CString GetSreBySurnWithTime(long lpSurn, COleDateTime opDate);
	int FindFirstOfSurn(long lpSurn);

	// Private methods
private:
    void PrepareSreData(SREDATA *prpSreData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPSREDATA__
