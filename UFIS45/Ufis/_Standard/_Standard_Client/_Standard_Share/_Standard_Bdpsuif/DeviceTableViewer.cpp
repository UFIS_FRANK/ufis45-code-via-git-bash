// DeviceTableViewer.cpp 
//

#include <stdafx.h>
#include <DeviceTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <fstream.h>
#include <iomanip.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void DeviceTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// DeviceTableViewer
//

DeviceTableViewer::DeviceTableViewer(CCSPtrArray<DEVDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomDeviceTable = NULL;
    ogDdx.Register(this, DEV_CHANGE, CString("DEVICETABLEVIEWER"), CString("Device Update/new"), DeviceTableCf);
    ogDdx.Register(this, DEV_DELETE, CString("DEVICETABLEVIEWER"), CString("Device Delete"), DeviceTableCf);
}

//-----------------------------------------------------------------------------------------------

DeviceTableViewer::~DeviceTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void DeviceTableViewer::Attach(CCSTable *popTable)
{
    pomDeviceTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void DeviceTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void DeviceTableViewer::MakeLines()
{
	int ilDeviceCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilDeviceCount; ilLc++)
	{
		DEVDATA *prlDeviceData = &pomData->GetAt(ilLc);
		MakeLine(prlDeviceData);
	}
}

//-----------------------------------------------------------------------------------------------

void DeviceTableViewer::MakeLine(DEVDATA *prpDevice)
{

    // Update viewer data for this shift record
    DEVICETABLE_LINEDATA rlDevice;

	rlDevice.Urno = prpDevice->Urno; 
	rlDevice.Devn = prpDevice->Devn; 
	rlDevice.Grpn = prpDevice->Grpn; 
	rlDevice.Dadr = prpDevice->Dadr; 
	rlDevice.Dprt = prpDevice->Dprt; 
	rlDevice.Stat = prpDevice->Stat; 
	rlDevice.Dpid = prpDevice->Dpid; 
	rlDevice.Adid = prpDevice->Adid; 
	rlDevice.Drnp = prpDevice->Drnp; 
	rlDevice.Dffd = prpDevice->Dffd; 
	rlDevice.Dfco = prpDevice->Dfco;
	rlDevice.Dter = prpDevice->Dter;
	rlDevice.Dare = prpDevice->Dare;
	rlDevice.Algc = prpDevice->Algc;
	rlDevice.Dalz = prpDevice->Dalz;
	rlDevice.Ddgi = prpDevice->Ddgi;
	rlDevice.Loca = prpDevice->Loca;
	rlDevice.Rema = prpDevice->Rema;
	rlDevice.Divn = prpDevice->Divn;
	rlDevice.Drna = prpDevice->Drna;
	rlDevice.Dlct = prpDevice->Dlct.Format("%d.%m.%Y %H:%M");
	rlDevice.Dldc = prpDevice->Dldc.Format("%d.%m.%Y %H:%M");
	rlDevice.Dlrd = prpDevice->Dlrd;
	rlDevice.Dldt = prpDevice->Dldt.Format("%d.%m.%Y %H:%M");
	rlDevice.Drgi = prpDevice->Drgi;
	rlDevice.Dbrf = prpDevice->Dbrf;
	rlDevice.Dcof = prpDevice->Dcof;
	rlDevice.Vafr = prpDevice->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlDevice.Vato = prpDevice->Vato.Format("%d.%m.%Y %H:%M"); 
	
	CreateLine(&rlDevice);
}

//-----------------------------------------------------------------------------------------------

void DeviceTableViewer::CreateLine(DEVICETABLE_LINEDATA *prpDevice)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareDevice(prpDevice, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	DEVICETABLE_LINEDATA rlDevice;
	rlDevice = *prpDevice;
    omLines.NewAt(ilLineno, rlDevice);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void DeviceTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 20;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomDeviceTable->SetShowSelection(TRUE);
	pomDeviceTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 130; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// device name
	rlHeader.Length = 80; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// group name
	rlHeader.Length = 110; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// IP - address
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Port
	rlHeader.Length = 10; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Status
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// display id
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// advert id
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// cluster number
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// filter field
	rlHeader.Length = 130; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// filter content

	rlHeader.Length = 40; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// terminal	
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// area
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Alc
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// alarm zone
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// degauss interval
	rlHeader.Length = 130; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// location
	rlHeader.Length = 130; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// remark
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// inventory

	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Actual device no
	rlHeader.Length = 135; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Last connection time
	rlHeader.Length = 135; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Last disconnection time
	rlHeader.Length = 250; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Last reply
	rlHeader.Length = 135; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Last degauss time
	rlHeader.Length = 135; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Asynch. Carousel
	rlHeader.Length = 135; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Brightness factor
	rlHeader.Length = 135; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Contrast factor
	rlHeader.Length = 135; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// valid from
	rlHeader.Length = 135; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING932),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// valid to
	
	pomDeviceTable->SetHeaderFields(omHeaderDataArray);

	pomDeviceTable->SetDefaultSeparator();
	pomDeviceTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Devn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Grpn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dadr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dprt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Stat;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dpid;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Adid;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Drnp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dffd;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dfco;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dter;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dare;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Algc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dalz;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ddgi;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Loca;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Divn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		
		rlColumnData.Text = omLines[ilLineNo].Drna;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dlct;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dldc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dlrd;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dldt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Drgi;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dbrf;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dcof;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomDeviceTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomDeviceTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString DeviceTableViewer::Format(DEVICETABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool DeviceTableViewer::FindDevice(char *pcpDevice, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpDeviceKeya) &&
			 (omLines[ilItem].Keyd == pcpDeviceKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void DeviceTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    DeviceTableViewer *polViewer = (DeviceTableViewer *)popInstance;
    if (ipDDXType == DEV_CHANGE) 
		polViewer->ProcessDeviceChange((DEVDATA *)vpDataPointer);
    else if (ipDDXType == DEV_DELETE) 
		polViewer->ProcessDeviceDelete((DEVDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void DeviceTableViewer::ProcessDeviceChange(DEVDATA *prpDevice)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpDevice->Devn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Grpn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Dadr;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Dprt;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Stat;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Dpid;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Adid;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Drnp;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDevice->Dffd;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDevice->Dfco;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Dter;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Dare;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Algc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Dalz;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Ddgi;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Loca;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Rema;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Divn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDevice->Drna;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Dlct.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Dldc.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Dlrd;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Dldt.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDevice->Drgi;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Dbrf;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Dcof;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);


	rlColumn.Text = prpDevice->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDevice->Vato.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpDevice->Urno, ilItem))
	{
        DEVICETABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpDevice->Urno; 
		prlLine->Devn = prpDevice->Devn; 
		prlLine->Grpn = prpDevice->Grpn; 
		prlLine->Dadr = prpDevice->Dadr; 
		prlLine->Dprt = prpDevice->Dprt; 
		prlLine->Stat = prpDevice->Stat; 
		prlLine->Dpid = prpDevice->Dpid; 
		prlLine->Adid = prpDevice->Adid; 
		prlLine->Drnp = prpDevice->Drnp; 
		prlLine->Dffd = prpDevice->Dffd; 
		prlLine->Dfco = prpDevice->Dfco;
		prlLine->Dter = prpDevice->Dter;
		prlLine->Dare = prpDevice->Dare;
		prlLine->Algc = prpDevice->Algc;
		prlLine->Dalz = prpDevice->Dalz;
		prlLine->Ddgi = prpDevice->Ddgi;
		prlLine->Loca = prpDevice->Loca;
		prlLine->Rema = prpDevice->Rema;
		prlLine->Divn = prpDevice->Divn;
		prlLine->Drna = prpDevice->Drna;
		prlLine->Dlct = prpDevice->Dlct.Format("%d.%m.%Y %H:%M");
		prlLine->Dldc = prpDevice->Dldc.Format("%d.%m.%Y %H:%M");
		prlLine->Dlrd = prpDevice->Dlrd;
		prlLine->Dldt = prpDevice->Dldt.Format("%d.%m.%Y %H:%M");
		prlLine->Drgi = prpDevice->Drgi;
		prlLine->Dbrf = prpDevice->Dbrf;
		prlLine->Dcof = prpDevice->Dcof;
		prlLine->Vafr = prpDevice->Vafr.Format("%d.%m.%Y %H:%M"); 
		prlLine->Vato = prpDevice->Vato.Format("%d.%m.%Y %H:%M"); 

		pomDeviceTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomDeviceTable->DisplayTable();
	}
	else
	{
		MakeLine(prpDevice);
		if (FindLine(prpDevice->Urno, ilItem))
		{
	        DEVICETABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomDeviceTable->AddTextLine(olLine, (void *)prlLine);
				pomDeviceTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void DeviceTableViewer::ProcessDeviceDelete(DEVDATA *prpDevice)
{
	int ilItem;
	if (FindLine(prpDevice->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomDeviceTable->DeleteTextLine(ilItem);
		pomDeviceTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool DeviceTableViewer::IsPassFilter(DEVDATA *prpDevice)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int DeviceTableViewer::CompareDevice(DEVICETABLE_LINEDATA *prpDevice1, DEVICETABLE_LINEDATA *prpDevice2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------

void DeviceTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool DeviceTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void DeviceTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void DeviceTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING929);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	

//			calculate count of pages
			CUIntArray olColsPerPage;
			int ilPages = pomPrint->CalculateCountOfPages(this->omHeaderDataArray,olColsPerPage);

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for (int ilPage = 0; ilPage < ilPages; ilPage++)
			{
				pomPrint->imPageNo = 0;
				int ilLines = omLines.GetSize();
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				olFooter1.Format("%d %s",ilLines,omTableName);
				for(int i = 0; i < ilLines; i++ ) 
				{
					if(pomPrint->imLineNo >= pomPrint->imMaxLines)
					{
						if(pomPrint->imPageNo > 0)
						{
							olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
							pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
							pomPrint->omCdc.EndPage();
						}
						PrintTableHeader(ilPage,olColsPerPage);
					}
					if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],true);
					}
					else
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],false);
					}
				}
				olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
				pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
				pomPrint->omCdc.EndPage();
			}
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool DeviceTableViewer::PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage)
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		bool blColumn = false;
		if (ilPage == 0)
		{
			if (i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}
		else
		{
			if (i >= ropColsPerPage[ilPage-1] && i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}

		if (blColumn)
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool DeviceTableViewer::PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,DEVICETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		bool blColumn = false;
		if (ilPage == 0)
		{
			if (i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}
		else
		{
			if (i >= ropColsPerPage[ilPage-1] && i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}

		if(blColumn)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Devn;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Grpn;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Dadr;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->Dprt;
				}
				break;
			case 4:
				{
					rlElement.Text		= prpLine->Stat;
				}
				break;
			case 5:
				{
					rlElement.Text		= prpLine->Dpid;
				}
				break;
			case 6:
				{
					rlElement.Text		= prpLine->Adid;
				}
				break;
			case 7:
				{
					rlElement.Text		= prpLine->Drnp;
				}
				break;
			case 8:
				{
					rlElement.Text		= prpLine->Dffd;
				}
				break;
			case 9:
				{
					rlElement.Text		= prpLine->Dfco;
				}
				break;
			case 10:
				{
					rlElement.Text		= prpLine->Dter;
				}
				break;
			case 11:
				{
					rlElement.Text		= prpLine->Dare;
				}
				break;
			case 12:
				{
					rlElement.Text		= prpLine->Algc;
				}
				break;
			case 13:
				{
					rlElement.Text		= prpLine->Dalz;
				}
				break;
			case 14:
				{
					rlElement.Text		= prpLine->Ddgi;
				}
				break;
			case 15:
				{
					rlElement.Text		= prpLine->Loca;
				}
				break;
			case 16:
				{
					rlElement.Text		= prpLine->Rema;
				}
				break;
			case 17:
				{
					rlElement.Text		= prpLine->Divn;
				}
				break;
			case 18:
				{
					rlElement.Text		= prpLine->Drna;
				}
				break;
			case 19:
				{
					rlElement.Text		= prpLine->Dlct;
				}
				break;
			case 20:
				{
					rlElement.Text		= prpLine->Dldc;
				}
				break;
			case 21:
				{
					rlElement.Text		= prpLine->Dlrd;
				}
				break;
			case 22:
				{
					rlElement.Text		= prpLine->Dldt;
				}
				break;
			case 23:
				{
					rlElement.Text		= prpLine->Drgi;
				}
				break;
			case 24:
				{
					rlElement.Text		= prpLine->Dbrf;
				}
				break;
			case 25:
				{
					rlElement.Text		= prpLine->Dcof;
				}
				break;
			case 26:
				{
					rlElement.Text		= prpLine->Vafr;
				}
				break;
			case 27:
				{
					rlElement.FrameRight= PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Vato;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

bool DeviceTableViewer::CreateExcelFile(const CString& opTrenner,const CString& ropListSeparator)
{
	ofstream of;

	CString olTmpPath = CCSLog::GetTmpPath();

	CString olTableName;
	olTableName.Format("%s -   %s", "DEVTAB", (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, olTmpPath);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);


	of  << setw(0) << "DEVTAB" << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(0) << LoadStg(IDS_STRING935) //CString("Device name");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING936) //CString("Group name");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING937) //CString("Device address");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING938) //CString("Device port");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING939) //CString("Device status");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING940) //CString("Display name");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING941) //CString("Advertisement name");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING942) //CString("Device no");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING943) //CString("Filter field");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING944) //CString("Filter Content");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING945) //CString("Terminal");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING946) //CString("Device area");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING947) //CString("Airline code");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING948) //CString("Alarm zone");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING949) //CString("Degauss interval");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING950) //CString("Location");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING951) //CString("Remark");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING952) //CString("Inventory no");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING953) //CString("Actual device no");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING954) //CString("Last connected");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING955) //CString("Last disconnected");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING978) //CString("Last reply");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING979) //CString("Last degauss");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING1015) //CString("Asynch. Carousel");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING1016) //CString("Brightness factor");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING1017) //CString("Contrast factor");
		<< endl;


	int ilCount = omLines.GetSize();
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	for(int i = 0; i < ilCount; i++)
	{
		DEVICETABLE_LINEDATA rlLine = omLines[i];
		of.setf(ios::left, ios::adjustfield);

		of   << setw(0) << rlLine.Devn							// Name
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Grpn							// Vorname
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Dadr							// Personal Nummer
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dprt							// Organisationseinheit
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Stat							// Funktionen
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dpid							// Qualifikationen
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Adid							// Abk�rzung
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Drnp							// Initialien
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dffd							// Telephon B�ro
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dfco							// Telephon mobil
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dter							// Telephon privat
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dare							// Gender Ind
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Algc							// Pdgl
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dalz							// Pdkl
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Ddgi							// Pmag
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Loca;							// Pmak

	   of	 << setw(0) << opTrenner 
		     << setw(0) << rlLine.Rema							// Regi 
			 << setw(0) << opTrenner 
			 << setw(0) << rlLine.Divn
			 << setw(0) << opTrenner 
			 << setw(0) << rlLine.Drna
			 << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dlct
			 << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dldc
			 << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dlrd
			 << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dldt
			 << setw(0) << opTrenner 
			 << setw(0) << rlLine.Drgi
			 << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dbrf
			 << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dcof
		     << endl; 
		
	}

	of.close();
	return true;
}

//-----------------------------------------------------------------------------------------------
