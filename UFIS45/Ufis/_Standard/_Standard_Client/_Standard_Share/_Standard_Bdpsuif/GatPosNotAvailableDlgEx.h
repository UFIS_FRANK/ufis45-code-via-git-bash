#if !defined(AFX_GATPOSNOTAVAILABLEDLGEX_H__38C2CA33_CBB9_11D6_8217_00010215BFDE__INCLUDED_)
#define AFX_GATPOSNOTAVAILABLEDLGEX_H__38C2CA33_CBB9_11D6_8217_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatPosNotAvailableDlg.h : header file
//

#include <GatPosNotAvailableDlg.h>
#include <AatBitmapComboBox.h>

/////////////////////////////////////////////////////////////////////////////
// GatPosNotAvailableDlg dialog

class GatPosNotAvailableDlgEx : public GatPosNotAvailableDlg
{
// Construction
public:
	GatPosNotAvailableDlgEx(BLKDATA *popBlk,CWnd* pParent = NULL);   // standard constructor
	int  GetNames(CStringArray& ropNames);
	void SetNames(const CStringArray& ropNames);
// Dialog Data
	//{{AFX_DATA(GatPosNotAvailableDlgEx)
	enum { IDD = IDD_GATPOS_NOTAVAILABLEDLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GatPosNotAvailableDlgEx)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GatPosNotAvailableDlgEx)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	CListBox		omNames;		 
	CStringArray	omSelectedNames;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATPOSNOTAVAILABLEDLGEX_H__38C2CA33_CBB9_11D6_8217_00010215BFDE__INCLUDED_)
