// CedaTWYData.cpp
 
#include <stdafx.h>
#include <CedaTWYData.h>
#include <resource.h>


// Local function prototype
static void ProcessTWYCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaTWYData::CedaTWYData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for TWYDATA
	BEGIN_CEDARECINFO(TWYDATA,TWYDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_DATE		(Nafr,"NAFR")
		FIELD_DATE		(Nato,"NATO")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Resn,"RESN")
		FIELD_CHAR_TRIM	(Rgrw,"RGRW")
		FIELD_CHAR_TRIM	(Tnam,"TNAM")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Home,"HOME")
	END_CEDARECINFO //(TWYDataAStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(TWYDataRecInfo)/sizeof(TWYDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&TWYDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"TWY");
	strcpy(pcmTWYFieldList,"CDAT,LSTU,NAFR,NATO,PRFL,RESN,RGRW,TNAM,URNO,USEC,USEU,VAFR,VATO,HOME");
	pcmFieldList = pcmTWYFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//-------------------------------------------------------------------------------------------------------

void CedaTWYData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("NAFR");
	ropFields.Add("NATO");
	ropFields.Add("PRFL");
	ropFields.Add("RESN");
	ropFields.Add("RGRW");
	ropFields.Add("TNAM");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");
	ropFields.Add("HOME");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING364));
	ropDesription.Add(LoadStg(IDS_STRING365));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING366));
	ropDesription.Add(LoadStg(IDS_STRING474));
	ropDesription.Add(LoadStg(IDS_STRING475));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING711));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
}

//-------------------------------------------------------------------------------------------------------

void CedaTWYData::Register(void)
{
	ogDdx.Register((void *)this,BC_TWY_CHANGE,CString("TWYDATA"), CString("TWY-changed"),ProcessTWYCf);
	ogDdx.Register((void *)this,BC_TWY_DELETE,CString("TWYDATA"), CString("TWY-deleted"),ProcessTWYCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaTWYData::~CedaTWYData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaTWYData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaTWYData::Read(char *pcpWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		TWYDATA *prpTWY = new TWYDATA;
		if ((ilRc = GetFirstBufferRecord(prpTWY)) == true)
		{
			prpTWY->IsChanged = DATA_UNCHANGED;
			omData.Add(prpTWY);//Update omData
			omUrnoMap.SetAt((void *)prpTWY->Urno,prpTWY);
		}
		else
		{
			delete prpTWY;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaTWYData::InsertTWY(TWYDATA *prpTWY,BOOL bpSendDdx)
{
	prpTWY->IsChanged = DATA_NEW;
	if(SaveTWY(prpTWY) == false) return false; //Update Database
	InsertTWYInternal(prpTWY);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaTWYData::InsertTWYInternal(TWYDATA *prpTWY)
{
	//PrepareTWYData(prpTWY);
	ogDdx.DataChanged((void *)this, TWY_CHANGE,(void *)prpTWY ); //Update Viewer
	omData.Add(prpTWY);//Update omData
	omUrnoMap.SetAt((void *)prpTWY->Urno,prpTWY);
	return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaTWYData::DeleteTWY(long lpUrno)
{
	TWYDATA *prlTWY = GetTWYByUrno(lpUrno);
	if (prlTWY != NULL)
	{
		prlTWY->IsChanged = DATA_DELETED;
		if(SaveTWY(prlTWY) == false) return false; //Update Database
		DeleteTWYInternal(prlTWY);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaTWYData::DeleteTWYInternal(TWYDATA *prpTWY)
{
	ogDdx.DataChanged((void *)this,TWY_DELETE,(void *)prpTWY); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpTWY->Urno);
	int ilTWYCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilTWYCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpTWY->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaTWYData::PrepareTWYData(TWYDATA *prpTWY)
{
	// TODO: add code here
}

//--UPDATE------------------------------------------------------------------------------------------------

bool CedaTWYData::UpdateTWY(TWYDATA *prpTWY,BOOL bpSendDdx)
{
	if (GetTWYByUrno(prpTWY->Urno) != NULL)
	{
		if (prpTWY->IsChanged == DATA_UNCHANGED)
		{
			prpTWY->IsChanged = DATA_CHANGED;
		}
		if(SaveTWY(prpTWY) == false) return false; //Update Database
		UpdateTWYInternal(prpTWY);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaTWYData::UpdateTWYInternal(TWYDATA *prpTWY)
{
	TWYDATA *prlTWY = GetTWYByUrno(prpTWY->Urno);
	if (prlTWY != NULL)
	{
		*prlTWY = *prpTWY; //Update omData
		ogDdx.DataChanged((void *)this,TWY_CHANGE,(void *)prlTWY); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

TWYDATA *CedaTWYData::GetTWYByUrno(long lpUrno)
{
	TWYDATA  *prlTWY;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlTWY) == TRUE)
	{
		return prlTWY;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaTWYData::ReadSpecialData(CCSPtrArray<TWYDATA> *popTwy,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","TWY",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","TWY",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popTwy != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			TWYDATA *prpTwy = new TWYDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpTwy,CString(pclFieldList))) == true)
			{
				popTwy->Add(prpTwy);
			}
			else
			{
				delete prpTwy;
			}
		}
		if(popTwy->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaTWYData::SaveTWY(TWYDATA *prpTWY)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpTWY->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpTWY->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpTWY);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpTWY->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpTWY->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpTWY);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpTWY->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpTWY->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessTWYCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_TWY_CHANGE :
	case BC_TWY_DELETE :
		((CedaTWYData *)popInstance)->ProcessTWYBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaTWYData::ProcessTWYBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlTWYData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlTWYData->Selection);

	TWYDATA *prlTWY = GetTWYByUrno(llUrno);
	if(ipDDXType == BC_TWY_CHANGE)
	{
		if(prlTWY != NULL)
		{
			GetRecordFromItemList(prlTWY,prlTWYData->Fields,prlTWYData->Data);
			if(ValidateTWYBcData(prlTWY->Urno)) //Prf: 8795
			{
				UpdateTWYInternal(prlTWY);
			}
			else
			{
				DeleteTWYInternal(prlTWY);
			}
		}
		else
		{
			prlTWY = new TWYDATA;
			GetRecordFromItemList(prlTWY,prlTWYData->Fields,prlTWYData->Data);
			if(ValidateTWYBcData(prlTWY->Urno)) //Prf: 8795
			{
				InsertTWYInternal(prlTWY);
			}
			else
			{
				delete prlTWY;
			}
		}
	}
	if(ipDDXType == BC_TWY_DELETE)
	{
		if (prlTWY != NULL)
		{
			DeleteTWYInternal(prlTWY);
		}
	}
}

//Prf: 8795
//--ValidateTWYBcData--------------------------------------------------------------------------------------

bool CedaTWYData::ValidateTWYBcData(const long& lrpUrno)
{
	bool blValidateTWYBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<TWYDATA> olTwys;
		if(!ReadSpecialData(&olTwys,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateTWYBcData = false;
		}
	}
	return blValidateTWYBcData;
}

//---------------------------------------------------------------------------------------------------------
