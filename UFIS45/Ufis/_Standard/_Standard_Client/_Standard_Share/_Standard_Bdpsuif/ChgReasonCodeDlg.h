#if !defined(AFX_CHGREASONCODEDLG_H__979F7B8F_B292_4911_9839_1E5371E2544D__INCLUDED_)
#define AFX_CHGREASONCODEDLG_H__979F7B8F_B292_4911_9839_1E5371E2544D__INCLUDED_


#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ChgReasonCodeDlg.h : header file    //PRF 8378
//

/////////////////////////////////////////////////////////////////////////////
// ChgReasonCodeDlg dialog
//#include "CedaSphData.h"
#include <CCSEdit.h>
#include <CedaCRCData.h>

class ChgReasonCodeDlg : public CDialog
{
// Construction
public:
	ChgReasonCodeDlg(CRCDATA *popCrc, CWnd* pParent = NULL);   // standard constructor

	CRCDATA *pomCrc;
// Dialog Data
	//{{AFX_DATA(ChgReasonCodeDlg)
	enum { IDD = IDD_CHGREASONCODEDLG };
	CButton	m_OK;
	CCSEdit	m_CODE;
	CCSEdit	m_REMA;
	CCSEdit	m_HOPO;
	CButton	m_GATF;
	CButton	m_POSF;
	CButton	m_CXXF;
	CButton	m_BELT;
	CCSEdit m_VAFRD;
	CCSEdit m_VAFRT;
	CCSEdit m_VATOD;
	CCSEdit m_VATOT;
	CCSEdit m_CDATD;
	CCSEdit m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ChgReasonCodeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ChgReasonCodeDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHGREASONCODEDLG_H__979F7B8F_B292_4911_9839_1E5371E2544D__INCLUDED_)
