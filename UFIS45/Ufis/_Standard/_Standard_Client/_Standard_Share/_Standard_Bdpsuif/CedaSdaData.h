// CedaSdaData.h

#ifndef __CEDAPSDADATA__
#define __CEDAPSDADATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <afxdisp.h>

#include <resrc1.h>		// main symbols#


//---------------------------------------------------------------------------------------------------------

struct SDADATA 
{
	long Urno;			// Eindeutige Datensatz-Nr.
	long Surn;			// Referenz auf STF.URNO
	long Upol;			// Referenz auf POL.URNO
	COleDateTime Vpfr;	// G�ltig von
	COleDateTime Vpto;	// G�ltig bis

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SDADATA(void)
	{ memset(this,'\0',sizeof(*this));
	}

}; // end SDADataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSdaData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapPtrToPtr omStaffUrnoMap;

    CCSPtrArray<SDADATA> omData;

	char pcmListOfFields[2048];

// OSdaations
public:
    CedaSdaData();
	~CedaSdaData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SDADATA *prpSda);
	bool InsertInternal(SDADATA *prpSda);
	bool Update(SDADATA *prpSda);
	bool UpdateInternal(SDADATA *prpSda);
	bool Delete(long lpUrno);
	bool DeleteInternal(SDADATA *prpSda);
	bool ReadSpecialData(CCSPtrArray<SDADATA> *popSda,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SDADATA *prpSda);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SDADATA  *GetSdaByUrno(long lpUrno);
	SDADATA *GetValidSdaBySurn(long lpSurn);
	void GetAllSdaBySurn(long lpSurn,CCSPtrArray<SDADATA> &ropList);


	// Private methods
private:
    void PrepareSdaData(SDADATA *prpSdaData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPSDADATA__
