#ifndef __FIDSCOMMANDTABLEVIEWER_H__
#define __FIDSCOMMANDTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaFIDData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>
#include "Util.h"

struct FIDSCOMMANDTABLE_LINEDATA
{
	long	Urno;
	CString	Code;
	CString	Remi;
	CString	Remt;
	CString	Blkc;
	CString	Beme;
	CString	Bemd;
	CString	Bet3;
	CString	Bet4;
	CString Conr;
	CString	Vafr;
	CString	Vato;
	CString Rema;
};

/////////////////////////////////////////////////////////////////////////////
// FidsCommandTableViewer

class FidsCommandTableViewer : public CViewer
{
// Constructions
public:
    FidsCommandTableViewer(CCSPtrArray<FIDDATA> *popData);
    ~FidsCommandTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(FIDDATA *prpFidsCommand);
	int CompareFidsCommand(FIDSCOMMANDTABLE_LINEDATA *prpFidsCommand1, FIDSCOMMANDTABLE_LINEDATA *prpFidsCommand2);
    void MakeLines();
	void MakeLine(FIDDATA *prpFidsCommand);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(FIDSCOMMANDTABLE_LINEDATA *prpFidsCommand);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(FIDSCOMMANDTABLE_LINEDATA *prpLine);
	void ProcessFidsCommandChange(FIDDATA *prpFidsCommand);
	void ProcessFidsCommandDelete(FIDDATA *prpFidsCommand);
	bool FindFidsCommand(char *prpFidsCommandKeya, char *prpFidsCommandKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomFidsCommandTable;
	CCSPtrArray<FIDDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<FIDSCOMMANDTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(FIDSCOMMANDTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;
	UtilUC* m_Util;	
};

#endif //__FIDSCOMMANDTABLEVIEWER_H__
