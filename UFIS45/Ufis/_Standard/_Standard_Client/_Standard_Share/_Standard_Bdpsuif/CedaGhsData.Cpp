// CedaGhsData.cpp
 
#include <stdafx.h>
#include <CedaGhsData.h>
#include <resource.h>


void ProcessGhsCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaGhsData::CedaGhsData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(GHSDATA, GhsDataRecInfo)
		FIELD_CHAR_TRIM	(Atrn,"ATRN")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Disp,"DISP")
		FIELD_CHAR_TRIM	(Lkam,"LKAM")
		FIELD_CHAR_TRIM	(Lkan,"LKAN")
		FIELD_CHAR_TRIM	(Lkar,"LKAR")
		FIELD_CHAR_TRIM	(Lkbz,"LKBZ")
		FIELD_CHAR_TRIM	(Lkco,"LKCO")
		FIELD_CHAR_TRIM	(Lkcc,"LKCC")
		FIELD_CHAR_TRIM	(Lkda,"LKDA")
		FIELD_CHAR_TRIM	(Lkhc,"LKHC")
		FIELD_CHAR_TRIM	(Lknb,"LKNB")
		FIELD_CHAR_TRIM	(Lknm,"LKNM")
		FIELD_CHAR_TRIM	(Lkst,"LKST")
		FIELD_CHAR_TRIM	(Lkty,"LKTY")
		FIELD_CHAR_TRIM	(Lkvb,"LKVB")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Perm,"PERM")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Vrgc,"VRGC")
		FIELD_CHAR_TRIM	(Wtyp,"WTYP")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(GhsDataRecInfo)/sizeof(GhsDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&GhsDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"GHS");
    sprintf(pcmListOfFields, "ATRN,CDAT,DISP,LKAM,LKAN,LKAR,LKBZ,LKCO,LKCC,LKDA,LKHC,LKNB,LKNM,LKST,LKTY,LKVB,LSTU,PERM,PRFL,URNO,USEC,USEU,VRGC,WTYP");


	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaGhsData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("ATRN");
	ropFields.Add("CDAT");
	ropFields.Add("DISP");
	ropFields.Add("LKAM");
	ropFields.Add("LKAN");
	ropFields.Add("LKAR");
	ropFields.Add("LKBZ");
	ropFields.Add("LKCO");
	ropFields.Add("LKCC");
	ropFields.Add("LKDA");
	ropFields.Add("LKHC");
	ropFields.Add("LKNB");
	ropFields.Add("LKNM");
	ropFields.Add("LKST");
	ropFields.Add("LKTY");
	ropFields.Add("LKVB");
	ropFields.Add("LSTU");
	ropFields.Add("PERM");
	ropFields.Add("PRFL");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VRGC");
	ropFields.Add("WTYP");

	ropDesription.Add(LoadStg(IDS_STRING46));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING526));
	ropDesription.Add(LoadStg(IDS_STRING49));
	ropDesription.Add(LoadStg(IDS_STRING48));
	ropDesription.Add(LoadStg(IDS_STRING527));
	ropDesription.Add(LoadStg(IDS_STRING528));
	ropDesription.Add(LoadStg(IDS_STRING301));
	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING381));
	ropDesription.Add(LoadStg(IDS_STRING47));
	ropDesription.Add(LoadStg(IDS_STRING51));
	ropDesription.Add(LoadStg(IDS_STRING229));
	ropDesription.Add(LoadStg(IDS_STRING529));
	ropDesription.Add(LoadStg(IDS_STRING530));
	ropDesription.Add(LoadStg(IDS_STRING50));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING531));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING532));
	ropDesription.Add(LoadStg(IDS_STRING511));

	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaGhsData::Register(void)
{
	ogDdx.Register((void *)this,BC_GHS_CHANGE,	CString("GHSDATA"), CString("Ghs-changed"),	ProcessGhsCf);
	ogDdx.Register((void *)this,BC_GHS_NEW,		CString("GHSDATA"), CString("Ghs-new"),		ProcessGhsCf);
	ogDdx.Register((void *)this,BC_GHS_DELETE,	CString("GHSDATA"), CString("Ghs-deleted"),	ProcessGhsCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaGhsData::~CedaGhsData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaGhsData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaGhsData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		GHSDATA *prlGhs = new GHSDATA;
		if ((ilRc = GetFirstBufferRecord(prlGhs)) == true)
		{
			omData.Add(prlGhs);//Update omData
			omUrnoMap.SetAt((void *)prlGhs->Urno,prlGhs);
		}
		else
		{
			delete prlGhs;
		}
	}
    return true;

}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaGhsData::Insert(GHSDATA *prpGhs)
{
	prpGhs->IsChanged = DATA_NEW;
	if(Save(prpGhs) == false) return false; //Update Database
	InsertInternal(prpGhs);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaGhsData::InsertInternal(GHSDATA *prpGhs)
{
	ogDdx.DataChanged((void *)this, GHS_NEW,(void *)prpGhs ); //Update Viewer
	omData.Add(prpGhs);//Update omData
	omUrnoMap.SetAt((void *)prpGhs->Urno,prpGhs);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaGhsData::Delete(long lpUrno)
{
	GHSDATA *prlGhs = GetGhsByUrno(lpUrno);
	if (prlGhs != NULL)
	{
		prlGhs->IsChanged = DATA_DELETED;
		if(Save(prlGhs) == false) return false; //Update Database
		DeleteInternal(prlGhs);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaGhsData::DeleteInternal(GHSDATA *prpGhs)
{
	ogDdx.DataChanged((void *)this,GHS_DELETE,(void *)prpGhs); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpGhs->Urno);
	int ilGhsCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilGhsCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpGhs->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaGhsData::Update(GHSDATA *prpGhs)
{
	if (GetGhsByUrno(prpGhs->Urno) != NULL)
	{
		if (prpGhs->IsChanged == DATA_UNCHANGED)
		{
			prpGhs->IsChanged = DATA_CHANGED;
		}
		if(Save(prpGhs) == false) return false; //Update Database
		UpdateInternal(prpGhs);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaGhsData::UpdateInternal(GHSDATA *prpGhs)
{
	GHSDATA *prlGhs = GetGhsByUrno(prpGhs->Urno);
	if (prlGhs != NULL)
	{
		*prlGhs = *prpGhs; //Update omData
		ogDdx.DataChanged((void *)this,GHS_CHANGE,(void *)prlGhs); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

GHSDATA *CedaGhsData::GetGhsByUrno(long lpUrno)
{
	GHSDATA  *prlGhs;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlGhs) == TRUE)
	{
		return prlGhs;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaGhsData::ReadSpecialData(CCSPtrArray<GHSDATA> *popGhs,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","GHS",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","GHS",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popGhs != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			GHSDATA *prpGhs = new GHSDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpGhs,CString(pclFieldList))) == true)
			{
				popGhs->Add(prpGhs);
			}
			else
			{
				delete prpGhs;
			}
		}
		if(popGhs->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaGhsData::Save(GHSDATA *prpGhs)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpGhs->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpGhs->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpGhs);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpGhs->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGhs->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpGhs);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpGhs->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGhs->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessGhsCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogGhsData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaGhsData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlGhsData;
	prlGhsData = (struct BcStruct *) vpDataPointer;
	GHSDATA *prlGhs;
	if(ipDDXType == BC_GHS_NEW)
	{
		prlGhs = new GHSDATA;
		GetRecordFromItemList(prlGhs,prlGhsData->Fields,prlGhsData->Data);
		if(ValidateGhsBcData(prlGhs->Urno)) //Prf: 8795
		{
			InsertInternal(prlGhs);
		}
		else
		{
			delete prlGhs;
		}
	}
	if(ipDDXType == BC_GHS_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlGhsData->Selection);
		prlGhs = GetGhsByUrno(llUrno);
		if(prlGhs != NULL)
		{
			GetRecordFromItemList(prlGhs,prlGhsData->Fields,prlGhsData->Data);
			if(ValidateGhsBcData(prlGhs->Urno)) //Prf: 8795
			{
				UpdateInternal(prlGhs);
			}
			else
			{
				DeleteInternal(prlGhs);
			}
		}
	}
	if(ipDDXType == BC_GHS_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlGhsData->Selection);

		prlGhs = GetGhsByUrno(llUrno);
		if (prlGhs != NULL)
		{
			DeleteInternal(prlGhs);
		}
	}
}

//Prf: 8795
//--ValidateGhsBcData--------------------------------------------------------------------------------------

bool CedaGhsData::ValidateGhsBcData(const long& lrpUrno)
{
	bool blValidateGhsBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<GHSDATA> olGhss;
		if(!ReadSpecialData(&olGhss,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateGhsBcData = false;
		}
	}
	return blValidateGhsBcData;
}

//---------------------------------------------------------------------------------------------------------
