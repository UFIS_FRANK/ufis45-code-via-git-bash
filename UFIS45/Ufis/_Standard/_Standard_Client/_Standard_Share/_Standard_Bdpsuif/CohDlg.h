#if !defined(AFX_COHDLG_H__D6712673_E86B_11D4_BFFA_00D0B7E2A4B5__INCLUDED_)
#define AFX_COHDLG_H__D6712673_E86B_11D4_BFFA_00D0B7E2A4B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CohDlg.h : header file
//
#include <PrivList.h>
#include <CedaCOTData.h>
#include <CedaCohData.h>
#include <CCSEdit.h>
#include <AwDlg.h>

/////////////////////////////////////////////////////////////////////////////
// CCohDlg dialog

class CCohDlg : public CDialog
{
// Construction
public:
	CCohDlg(COHDATA *popCoh, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCohDlg)
	enum { IDD = IDD_COHDLG };
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_CTRC;
	CCSEdit	m_FAGE;
	CCSEdit	m_HOLD;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCohDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	COHDATA *pomCoh;
	CString Gui2DBmin(CString guimin);
// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCohDlg)
	afx_msg void OnBCotAw();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COHDLG_H__D6712673_E86B_11D4_BFFA_00D0B7E2A4B5__INCLUDED_)
