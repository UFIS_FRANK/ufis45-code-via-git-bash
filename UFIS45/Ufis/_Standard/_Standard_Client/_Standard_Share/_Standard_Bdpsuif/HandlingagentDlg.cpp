// HandlingagentDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <HandlingagentDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// HandlingagentDlg dialog


HandlingagentDlg::HandlingagentDlg(HAGDATA *popHAG,CWnd* pParent /*=NULL*/) : CDialog(HandlingagentDlg::IDD, pParent)
{
	pomHAG = popHAG;

	//{{AFX_DATA_INIT(HandlingagentDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void HandlingagentDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(HandlingagentDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_FAXN,   m_FAXN);
	DDX_Control(pDX, IDC_HNAM,   m_HNAM);
	DDX_Control(pDX, IDC_HSNA,   m_HSNA);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_TELE,   m_TELE);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(HandlingagentDlg, CDialog)
	//{{AFX_MSG_MAP(HandlingagentDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// HandlingagentDlg message handlers
//----------------------------------------------------------------------------------------

BOOL HandlingagentDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING160) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("HANDLINGAGENTDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomHAG->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomHAG->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomHAG->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomHAG->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomHAG->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomHAG->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_HSNA.SetFormat("x|#x|#x|#x|#x|#");
	m_HSNA.SetTextLimit(1,5);
	m_HSNA.SetBKColor(YELLOW);  
	m_HSNA.SetTextErrColor(RED);
	m_HSNA.SetInitText(pomHAG->Hsna);
	m_HSNA.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_HSNA"));
	//------------------------------------
	m_HNAM.SetTypeToString("X(40)",40,1);
	m_HNAM.SetBKColor(YELLOW);
	m_HNAM.SetTextErrColor(RED);
	m_HNAM.SetInitText(pomHAG->Hnam);
	m_HNAM.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_HNAM"));
	//------------------------------------
	m_TELE.SetTypeToString("X(10)",10,0);
	m_TELE.SetTextErrColor(RED);
	m_TELE.SetInitText(pomHAG->Tele);
	m_TELE.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_TELE"));
	//------------------------------------
	m_FAXN.SetTypeToString("X(10)",10,0);
	m_FAXN.SetTextErrColor(RED);
	m_FAXN.SetInitText(pomHAG->Faxn);
	m_FAXN.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_FAXN"));
	//------------------------------------	
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomHAG->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomHAG->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomHAG->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomHAG->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("HANDLINGAGENTDLG.m_VATO"));
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void HandlingagentDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_HSNA.GetStatus() == false)
	{
		ilStatus = false;
		if(m_HSNA.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_HNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_HNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_TELE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING294) + ogNotFormat;
	}
	if(m_FAXN.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING44) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olHsna;
		char clWhere[100];
		CCSPtrArray<HAGDATA> olHagCPA;

		m_HSNA.GetWindowText(olHsna);
		sprintf(clWhere,"WHERE HSNA='%s'",olHsna);
		if(ogHAGData.ReadSpecialData(&olHagCPA,clWhere,"URNO,HSNA",false) == true)
		{
			if(olHagCPA[0].Urno != pomHAG->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olHagCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_HSNA.GetWindowText(pomHAG->Hsna,6);
		m_FAXN.GetWindowText(pomHAG->Faxn,11);
		m_HNAM.GetWindowText(pomHAG->Hnam,41);
		m_TELE.GetWindowText(pomHAG->Tele,11);
		pomHAG->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomHAG->Vato = DateHourStringToDate(olVatod,olVatot);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_HSNA.SetFocus();
		m_HSNA.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------

