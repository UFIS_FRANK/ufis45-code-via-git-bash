// CedaPmxData.cpp
 
#include <stdafx.h>
#include <CedaPmxData.h>
#include <resource.h>
#include <resource.h>


void ProcessPmxCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaPmxData::CedaPmxData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for PMXDATA
	BEGIN_CEDARECINFO(PMXDATA,PmxDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Pofr,"POFR")
		FIELD_CHAR_TRIM	(Pomo,"POMO")
		FIELD_CHAR_TRIM	(Posa,"POSA")
		FIELD_CHAR_TRIM	(Posu,"POSU")
		FIELD_CHAR_TRIM	(Poth,"POTH")
		FIELD_CHAR_TRIM	(Potu,"POTU")
		FIELD_CHAR_TRIM	(Powe,"POWE")
		FIELD_CHAR_TRIM	(Tifr,"TIFR")
		FIELD_LONG		(Urno,"URNO")
		//uhi 7.5.01
		FIELD_OLEDATE	(Vafr,"VAFR")
		FIELD_OLEDATE	(Vato,"VATO")
	END_CEDARECINFO //(PMXDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(PmxDataRecInfo)/sizeof(PmxDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PmxDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"PMX");
	strcpy(pcmListOfFields,"CDAT,LSTU,USEC,USEU,POMO,POFR,POSA,POSU,POTH,POTU,POWE,TIFR,URNO,VAFR,VATO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaPmxData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("POMO");
	ropFields.Add("POFR");
	ropFields.Add("POSA");
	ropFields.Add("POSU");
	ropFields.Add("POTH");
	ropFields.Add("POTU");
	ropFields.Add("POWE");
	ropFields.Add("TIFR");
	ropFields.Add("URNO");
	//uhi 7.5.01
	ropFields.Add("VAFR");
	ropFields.Add("VATO");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING809));
	ropDesription.Add(LoadStg(IDS_STRING810));
	ropDesription.Add(LoadStg(IDS_STRING811));
	ropDesription.Add(LoadStg(IDS_STRING812));
	ropDesription.Add(LoadStg(IDS_STRING813));
	ropDesription.Add(LoadStg(IDS_STRING814));
	ropDesription.Add(LoadStg(IDS_STRING815));
	ropDesription.Add(LoadStg(IDS_STRING816));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaPmxData::Register(void)
{
	ogDdx.Register((void *)this,BC_PMX_CHANGE,	CString("PMXDATA"), CString("Pmx-changed"),	ProcessPmxCf);
	ogDdx.Register((void *)this,BC_PMX_NEW,		CString("PMXDATA"), CString("Pmx-new"),		ProcessPmxCf);
	ogDdx.Register((void *)this,BC_PMX_DELETE,	CString("PMXDATA"), CString("Pmx-deleted"),	ProcessPmxCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaPmxData::~CedaPmxData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaPmxData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaPmxData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		PMXDATA *prlPmx = new PMXDATA;
		if ((ilRc = GetFirstBufferRecord(prlPmx)) == true)
		{
			omData.Add(prlPmx);//Update omData
			omUrnoMap.SetAt((void *)prlPmx->Urno,prlPmx);
		}
		else
		{
			delete prlPmx;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaPmxData::Insert(PMXDATA *prpPmx)
{
	prpPmx->IsChanged = DATA_NEW;
	if(Save(prpPmx) == false) return false; //Update Database
	InsertInternal(prpPmx);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaPmxData::InsertInternal(PMXDATA *prpPmx)
{
	ogDdx.DataChanged((void *)this, PMX_NEW,(void *)prpPmx ); //Update Viewer
	omData.Add(prpPmx);//Update omData
	omUrnoMap.SetAt((void *)prpPmx->Urno,prpPmx);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaPmxData::Delete(long lpUrno)
{
	PMXDATA *prlPmx = GetPmxByUrno(lpUrno);
	if (prlPmx != NULL)
	{
		prlPmx->IsChanged = DATA_DELETED;
		if(Save(prlPmx) == false) return false; //Update Database
		DeleteInternal(prlPmx);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaPmxData::DeleteInternal(PMXDATA *prpPmx)
{
	ogDdx.DataChanged((void *)this,PMX_DELETE,(void *)prpPmx); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpPmx->Urno);
	int ilPmxCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilPmxCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpPmx->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaPmxData::Update(PMXDATA *prpPmx)
{
	if (GetPmxByUrno(prpPmx->Urno) != NULL)
	{
		if (prpPmx->IsChanged == DATA_UNCHANGED)
		{
			prpPmx->IsChanged = DATA_CHANGED;
		}
		if(Save(prpPmx) == false) return false; //Update Database
		UpdateInternal(prpPmx);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaPmxData::UpdateInternal(PMXDATA *prpPmx)
{
	PMXDATA *prlPmx = GetPmxByUrno(prpPmx->Urno);
	if (prlPmx != NULL)
	{
		*prlPmx = *prpPmx; //Update omData
		ogDdx.DataChanged((void *)this,PMX_CHANGE,(void *)prlPmx); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

PMXDATA *CedaPmxData::GetPmxByUrno(long lpUrno)
{
	PMXDATA  *prlPmx;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlPmx) == TRUE)
	{
		return prlPmx;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaPmxData::ReadSpecialData(CCSPtrArray<PMXDATA> *popPmx,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","PMX",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","PMX",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popPmx != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			PMXDATA *prpPmx = new PMXDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpPmx,CString(pclFieldList))) == true)
			{
				popPmx->Add(prpPmx);
			}
			else
			{
				delete prpPmx;
			}
		}
		if(popPmx->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaPmxData::Save(PMXDATA *prpPmx)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpPmx->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpPmx->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpPmx);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpPmx->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPmx->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpPmx);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpPmx->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPmx->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessPmxCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogPmxData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaPmxData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPmxData;
	prlPmxData = (struct BcStruct *) vpDataPointer;
	PMXDATA *prlPmx;
	if(ipDDXType == BC_PMX_NEW)
	{
		prlPmx = new PMXDATA;
		GetRecordFromItemList(prlPmx,prlPmxData->Fields,prlPmxData->Data);
		if(ValidatePmxBcData(prlPmx->Urno)) //Prf: 8795
		{
			InsertInternal(prlPmx);
		}
		else
		{
			delete prlPmx;
		}
	}
	if(ipDDXType == BC_PMX_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlPmxData->Selection);
		prlPmx = GetPmxByUrno(llUrno);
		if(prlPmx != NULL)
		{
			GetRecordFromItemList(prlPmx,prlPmxData->Fields,prlPmxData->Data);
			if(ValidatePmxBcData(prlPmx->Urno)) //Prf: 8795
			{
				UpdateInternal(prlPmx);
			}
			else
			{
				DeleteInternal(prlPmx);
			}
		}
	}
	if(ipDDXType == BC_PMX_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlPmxData->Selection);

		prlPmx = GetPmxByUrno(llUrno);
		if (prlPmx != NULL)
		{
			DeleteInternal(prlPmx);
		}
	}
}

//Prf: 8795
//--ValidatePmxBcData--------------------------------------------------------------------------------------

bool CedaPmxData::ValidatePmxBcData(const long& lrpUrno)
{
	bool blValidatePmxBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<PMXDATA> olPmxs;
		if(!ReadSpecialData(&olPmxs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidatePmxBcData = false;
		}
	}
	return blValidatePmxBcData;
}

//---------------------------------------------------------------------------------------------------------
