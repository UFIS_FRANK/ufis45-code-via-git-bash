// CedaALTData.h

#ifndef __CEDAALTDATA__
#define __CEDAALTDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct ALTDATA 
{
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Alc2[4]; 	// Fluggesellschaft 2-Letter Code
	char 	 Alc3[5]; 	// Fluggesellschaft 3-Letter Code
	char 	 Alfn[62]; 	// Fluggesellschaft Name
	char 	 Cash[3]; 	// Barzahler J/N
	char 	 Term[66]; 	// Terminal restriction for check-in
	CTime	 Vafr;	 	// G�ltig von
	CTime	 Vato;	 	// G�ltig bis
	char 	 Akey[7]; 	// Verbindungs-Schl�ssel
	char 	 Rprt[12]; 	// Verbindungs-Schl�ssel
	char 	 Wrko[12]; 	// Verbindungs-Schl�ssel
	char 	 Admd[12]; 	// Verbindungs-Schl�ssel
	char 	 Add1[62]; 	// Verbindungs-Schl�ssel
	char 	 Add2[62]; 	// Verbindungs-Schl�ssel
	char 	 Add3[42]; 	// Verbindungs-Schl�ssel
	char 	 Add4[42]; 	// Verbindungs-Schl�ssel
	char 	 Base[62]; 	// Verbindungs-Schl�ssel
	char 	 Cont[4]; 	// Verbindungs-Schl�ssel
	char 	 Ctry[12]; 	// Verbindungs-Schl�ssel
	char 	 Emps[7]; 	// Verbindungs-Schl�ssel
	char 	 Exec[32]; 	// Verbindungs-Schl�ssel
	char 	 Fond[6]; 	// Verbindungs-Schl�ssel
	char 	 Iano[6]; 	// Verbindungs-Schl�ssel
	char 	 Iata[4]; 	// Verbindungs-Schl�ssel
	char 	 Ical[14]; 	// Verbindungs-Schl�ssel
	char 	 Icao[5]; 	// Verbindungs-Schl�ssel
	char 	 Lcod[6]; 	// Verbindungs-Schl�ssel
	char 	 Phon[18]; 	// Verbindungs-Schl�ssel
	char 	 Self[4]; 	// Verbindungs-Schl�ssel
	char 	 Sita[9]; 	// Verbindungs-Schl�ssel
	char 	 Telx[18]; 	// Verbindungs-Schl�ssel
	char 	 Text[24]; 	// Verbindungs-Schl�ssel
	char 	 Tfax[18]; 	// Verbindungs-Schl�ssel
	char 	 Webs[42]; 	// Verbindungs-Schl�ssel
	char 	 Home[5]; 	// Verbindungs-Schl�ssel
	char 	 Doin[3]; 	// Verbindungs-Schl�ssel
	char	 Terg[66];	// Terminal restriction for gates
	char	 Terp[66];	// Terminal restriction for positions
	char	 Terb[66];	// Terminal restriction for baggage belts
	char	 Terw[66];	// Terminal restriction for waiting rooms
	char     Hsna[6];   // Handling Agent - PRM Modifications
	char     Task[6];   // Handling Agent - PRM Modifications
	char     Ofst[4];   // Offset for CCA -Added by Christine

	//DataCreated by this class
	int		 IsChanged;

	ALTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end ALTDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaALTData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<ALTDATA> omData;

// Operations
public:
    CedaALTData();
	~CedaALTData();

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<ALTDATA> *popAlt,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertALT(ALTDATA *prpALT,BOOL bpSendDdx = TRUE);
	bool InsertALTInternal(ALTDATA *prpALT);
	bool UpdateALT(ALTDATA *prpALT,BOOL bpSendDdx = TRUE);
	bool UpdateALTInternal(ALTDATA *prpALT);
	bool DeleteALT(long lpUrno);
	bool DeleteALTInternal(ALTDATA *prpALT);
	ALTDATA  *GetALTByUrno(long lpUrno);
	bool SaveALT(ALTDATA *prpALT);
	char pcmALTFieldList[2048];
	void ProcessALTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CString GetSortedAlc3List(const CString& ropDelimiter);	
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795


	// Private methods
private:
    void PrepareALTData(ALTDATA *prpALTData);
	bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct);
	bool MakeCedaData(CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct);
	bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct);
	CString omWhere; //Prf: 8795
	bool ValidateALTBcData(const long& lrpUnro); //Prf: 8795
	void SetFieldList(void);

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAALTDATA__
