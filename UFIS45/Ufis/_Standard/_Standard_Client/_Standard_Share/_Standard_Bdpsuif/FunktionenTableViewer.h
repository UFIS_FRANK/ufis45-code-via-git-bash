#ifndef __FUNKTIONENTABLEVIEWER_H__
#define __FUNKTIONENTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaPfcData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct FUNKTIONENTABLE_LINEDATA
{
	long 		 Urno; 	// Eindeutige Datensatz-Nr.
	CString 	 Dptc; 	// Organisationseinheit Code
	CString 	 Fctc; 	// Code
	CString 	 Fctn; 	// Bezeichnung
	//uhi 11.5.01
	//CString 	 Prio; 	// Prioritšt
	CString 	 Prfl; 	// Protokollierungskennzeichen
	CString 	 Rema; 	// Bemerkung


};

/////////////////////////////////////////////////////////////////////////////
// FunktionenTableViewer

class FunktionenTableViewer : public CViewer
{
// Constructions
public:
    FunktionenTableViewer(CCSPtrArray<PFCDATA> *popData);
    ~FunktionenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(PFCDATA *prpFunktionen);
	int CompareFunktionen(FUNKTIONENTABLE_LINEDATA *prpFunktionen1, FUNKTIONENTABLE_LINEDATA *prpFunktionen2);
    void MakeLines();
	void MakeLine(PFCDATA *prpFunktionen);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(FUNKTIONENTABLE_LINEDATA *prpFunktionen);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(FUNKTIONENTABLE_LINEDATA *prpLine);
	void ProcessFunktionenChange(PFCDATA *prpFunktionen);
	void ProcessFunktionenDelete(PFCDATA *prpFunktionen);
	bool FindFunktionen(char *prpFunktionenKeya, char *prpFunktionenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomFunktionenTable;
	CCSPtrArray<PFCDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<FUNKTIONENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(FUNKTIONENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__FUNKTIONENTABLEVIEWER_H__
