// PsFilterPage.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <Bdpsuif.h>
#include <PsSpfFilterPage.h>
#include <CedaPfcData.h>   

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite CPsSpfFilterPage 

IMPLEMENT_DYNCREATE(CPsSpfFilterPage, CPropertyPage)

CPsSpfFilterPage::CPsSpfFilterPage() : CPropertyPage(CPsSpfFilterPage::IDD)
{
	//{{AFX_DATA_INIT(CPsSpfFilterPage)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
	ogPfcData.GetAllCodes(omPossibleItems);
}

CPsSpfFilterPage::~CPsSpfFilterPage()
{
}


BOOL CPsSpfFilterPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	// TODO: Zus�tzliche Initialisierung hier einf�gen
	GetDlgItem(IDC_STATIC1)->SetWindowText(LoadStg(IDS_STRING878));
	GetDlgItem(IDC_STATIC2)->SetWindowText(LoadStg(IDS_STRING879));
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CPsSpfFilterPage::SetCaption(const char *pcpCaption)
{
	m_psp.pszTitle = pcpCaption;
	m_psp.dwFlags |= PSP_USETITLE;
}

void CPsSpfFilterPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPsSpfFilterPage)
	DDX_Control(pDX, IDC_BUTTON_REMOVE, m_RemoveButton);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_AddButton);
	DDX_Control(pDX, IDC_CONTENTLIST, m_ContentList);
	DDX_Control(pDX, IDC_INSERTLIST, m_InsertList);
	//}}AFX_DATA_MAP
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		int ilLc;
		
		// Do not update window while processing new data
		m_InsertList.SetRedraw(FALSE);
		m_ContentList.SetRedraw(FALSE);

		// Initialize all possible items into the left list box
		m_InsertList.ResetContent();
		m_InsertList.AddString(LoadStg(IDS_ALL));

		for (ilLc = 0; ilLc < omPossibleItems.GetSize(); ilLc++)
			m_InsertList.AddString(omPossibleItems[ilLc]);

		// Initialize the selected items into the right list box,
		// also remove the corresponding items from the left list box.
		m_ContentList.ResetContent();
		bool blFoundAlle = false;
		for (ilLc = 0; ilLc < omSelectedItems.GetSize()  && blFoundAlle == false; ilLc++)
		{
			if (omSelectedItems[ilLc] == LoadStg(IDS_ALL))
			{
				/************ using all *******************/
				m_AddButton.EnableWindow(FALSE);
				blFoundAlle = true;
				/*** remove all entries except "*Alle" from content list **/
				m_ContentList.ResetContent();
				m_ContentList.AddString(LoadStg(IDS_ALL));	
				/*** show empty insert list ***/
				m_InsertList.ResetContent();
			}
			else
			{
				m_ContentList.AddString(omSelectedItems[ilLc]);
				m_InsertList.DeleteString(m_InsertList.FindStringExact(-1, omSelectedItems[ilLc]));
			}
		}

		m_InsertList.SetRedraw(TRUE);
		m_ContentList.SetRedraw(TRUE);
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		// Copy data from the list box back to the array of string
		omSelectedItems.SetSize(m_ContentList.GetCount());
		for (int ilLc = 0; ilLc < omSelectedItems.GetSize(); ilLc++)
			m_ContentList.GetText(ilLc, omSelectedItems[ilLc]);
	}


}


BEGIN_MESSAGE_MAP(CPsSpfFilterPage, CPropertyPage)
	//{{AFX_MSG_MAP(CPsSpfFilterPage)
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, OnButtonRemove)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CPsSpfFilterPage 

void CPsSpfFilterPage::OnButtonAdd() 
{
	CString olText;
	bool blFoundAlle = false;

	// Move selected items from left list box to right list box
	for (int ilLc = m_InsertList.GetCount()-1; 
		ilLc >= 0 && blFoundAlle == false; ilLc--)
	{
		if (!m_InsertList.GetSel(ilLc))	// unselected item?
			continue;
		m_InsertList.GetText(ilLc, olText);	// load string to "olText"
		if (olText == LoadStg(IDS_ALL))
		{
			/************ using all *******************/
			m_AddButton.EnableWindow(FALSE);
			blFoundAlle = true;
			/*** remove all entries except "*Alle" from content list **/
			m_ContentList.ResetContent();
			m_ContentList.AddString(LoadStg(IDS_ALL));	
			/*** show empty insert list ***/
			m_InsertList.ResetContent();
		}
		else
		{
			m_ContentList.AddString(olText);	// move string from left to right box
			m_InsertList.DeleteString(ilLc);
		}
	}
}

void CPsSpfFilterPage::OnButtonRemove() 
{
	CString olText;
	bool blFoundAlle = false;

	// Move selected items from right list box to left list box
	for (int ilLc = m_ContentList.GetCount()-1; 
	ilLc >= 0  && blFoundAlle == false; ilLc--)
	{
		if (!m_ContentList.GetSel(ilLc))	// unselected item?
			continue;
		m_ContentList.GetText(ilLc,olText);	// load string to "s"
		if (olText == LoadStg(IDS_ALL))
		{
			m_AddButton.EnableWindow(TRUE);
			blFoundAlle = true;
			/*** show empty content list ***/
			m_ContentList.ResetContent();
			/*** rebuild insert list ***/
			m_InsertList.ResetContent();
			m_InsertList.AddString(LoadStg(IDS_ALL));
			for (ilLc = 0; ilLc < omPossibleItems.GetSize(); ilLc++)
			{
				m_InsertList.AddString(omPossibleItems[ilLc]);
			}

		}
		else
		{
			m_InsertList.AddString(olText);	// move string from right to left box
			m_ContentList.DeleteString(ilLc);
		}
	}
}

