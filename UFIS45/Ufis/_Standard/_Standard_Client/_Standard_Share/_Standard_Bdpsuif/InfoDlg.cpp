// InfoDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <InfoDlg.h>
#include <CCSCedadata.h>
#include <CCSCedacom.h>
#include <BasicData.h>
#include <VersionInfo.h>
#include <LibGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// InfoDlg dialog
//-----------------------------------------------------------------------------------------

InfoDlg::InfoDlg(CWnd* pParent /*=NULL*/) : CDialog(InfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(InfoDlg)
	//}}AFX_DATA_INIT
}


void InfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(InfoDlg)
	DDX_Control(pDX, IDC_CLASSLIBVERSION, m_ClassLibVersion);
	DDX_Control(pDX, IDC_USERNAME,		m_Username);
	DDX_Control(pDX, IDC_SERVER,		m_Server);
	DDX_Control(pDX, IDC_E_COPYRIGHT1,	m_COPYRIGHT1);
	DDX_Control(pDX, IDC_E_COPYRIGHT2,	m_COPYRIGHT2);
	DDX_Control(pDX, IDC_E_UFIS1,		m_UFIS1);
	DDX_Control(pDX, IDC_E_UFIS2,		m_UFIS2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(InfoDlg, CDialog)
	//{{AFX_MSG_MAP(InfoDlg)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// InfoDlg message handlers
//-----------------------------------------------------------------------------------------

BOOL InfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Versionsinformation ermitteln
	VersionInfo rlInfo;
	VersionInfo::GetVersionInfo(NULL,rlInfo);	// must use NULL (and it works), because AFX is not initialized yet and will cause an ASSERT

	CString olVersionString;
	olVersionString.Format("%s %s %s  Compiled: %s",rlInfo.omProductName,rlInfo.omFileVersion,rlInfo.omSpecialBuild,__DATE__);

	m_Username.SetWindowText(cgUserName);

	CString olServerText="";
	olServerText  += ogCommHandler.pcmRealHostName;
	olServerText  += " / ";
	olServerText  += ogCommHandler.pcmRealHostType;

	m_Server.SetWindowText(olServerText);

	m_COPYRIGHT1.SetBKColor(SILVER);
	m_COPYRIGHT2.SetBKColor(SILVER);
	m_UFIS1.SetBKColor(SILVER);
	m_UFIS2.SetBKColor(SILVER);


	m_UFIS1.SetInitText(rlInfo.omProductVersion);
	m_UFIS2.SetInitText(olVersionString);
	m_COPYRIGHT1.SetInitText(rlInfo.omLegalCopyright);
	m_COPYRIGHT2.SetInitText(rlInfo.omCompanyName);

	m_ClassLibVersion.SetWindowText(CString(pcgClassLibVersion));
	return TRUE;  
}

//-----------------------------------------------------------------------------------------

void InfoDlg::OnPaint() 
{
	CPaintDC dc(this);
	
/*	CDC olDC;
    olDC.CreateCompatibleDC( &dc );
    CBitmap olBitmap;

	if(ogHome != "SHA")
	{
		olBitmap.LoadBitmap( IDB_ABB ); //ABB-Logo
	}
	else
	{
		olBitmap.LoadBitmap( IDB_EMPTY ); 
	}

    olDC.SelectObject( &olBitmap );
    dc.BitBlt( 19, 96, 76, 52, &olDC, 0, 0, SRCCOPY );
    olDC.DeleteDC( );*/
}

//-----------------------------------------------------------------------------------------
