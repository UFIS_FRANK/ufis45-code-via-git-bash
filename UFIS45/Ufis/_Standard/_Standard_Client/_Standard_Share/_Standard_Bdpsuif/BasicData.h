// basicdat.h CCSBasicData class for providing general used methods

#ifndef _BASICDATA
#define _BASICDATA

#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CCSBasicFunc.h>
#include <CCSLog.h>
#include <CedaCfgData.h>
#include <CCSDdx.h>
#include <CCSCedaCom.h>
#include <CCSBcHandle.h>
#include <CCSMessageBox.h>
#include <CCSTime.h>
#include <AatBitmapComboBox.h>

int GetItemCount(CString olList, char cpTrenner  = ',' );
CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner  = ',');
CString DeleteListItem(CString &opList, CString olItem);
CString LoadStg(UINT nID);
CString SortItemList(CString opSubString, char cpTrenner);
CString GetMinutesFromHHMM(CString opHHMM);
CString GetHHMMFromMinutes(CString opMinutes, bool bpTrenner = false);
bool IsNumeric(CString opString);


/////////////////////////////////////////////////////////////////////////////
// class BasicData

class CBasicData : public CCSCedaData
{

public:

	CBasicData(void);
	~CBasicData(void);
	
	int imScreenResolutionX;
	int imScreenResolutionY;
	int imMonitorCount;
	long ConfirmTime;
	CString omUserID;

	long GetNextUrno(void);
	bool GetNurnos(int ipNrOfUrnos);
	int imNextOrder;


	int GetNextOrderNo();

	char *GetCedaCommand(CString opCmdType);
	void GetDiagramStartTime(CTime &opStart, CTime &opEnd);
	void SetDiagramStartTime(CTime opDiagramStartTime, CTime opEndTime);
	void SetWorkstationName(CString opWsName);
	CString GetWorkstationName();
	bool GetWindowPosition(CRect& rlPos,CString olMonitor);
	void SetWindowStat(const char *pcpKey, CWnd *popWnd);
	bool IsGatPosEnabled();
	bool UseUniqueActPair();
	bool GetCmdLineResource(CString& ropTable,long& rlpUrno);
	int	 GetGatesForPosition(long lpPosUrno,CStringArray& ropGates);
	bool SetGatesForPosition(long lpPosUrno,const CStringArray& ropGates);
	int	 GetBaggageBeltsForGate(long lpGatUrno,CStringArray& ropBaggageBelts);
	bool SetBaggageBeltsForGate(long lpGatUrno,const CStringArray& ropBaggageBelts);
	int	 GetWaitingRoomsForGate(long lpGatUrno,CStringArray& ropWaitingRooms);
	bool SetWaitingRoomsForGate(long lpGatUrno,const CStringArray& ropWaitingRooms);
	int	 GetExitsForBaggageBelt(long lpBltUrno,CStringArray& ropExits);
	bool SetExitsForBaggageBelt(long lpBltUrno,const CStringArray& ropExits);
	bool AddBlockingImages(CImageList& ropImageList);
	bool AddBlockingImages(AatBitmapComboBox& ropComboBox);
	bool DisplayDamagedDataWarningMessage(bool bpDelete = false);	
	bool BackDoorEnabled();
	bool SendBroadcast();
	bool IsColorCodeAvailable();
	bool IsExtendedLMEnabled();
	bool IsTerminalRestrictionForAirlinesAvailable();
	bool IsColumnForAbnormalFlightsReportAvailable();	
	bool DoesFieldExist(CString opTana, CString opFina);	
	bool HasHtytabAdid();	
	bool IsPrmHandlingAgentEnabled(); //PRM Modifications
	bool UseHandlingAgentFilter(); 
	bool IsChutesDisplayEnabled();
	int GetGatesForGate(long lpGatUrno,CStringArray& ropGates);
	bool SetGatesForGate(long lpGatUrno,const CStringArray& ropGates);

	WORD AircraftTypeDescriptionLength();
	CString	GetCustomerId();

	BOOL SetLocalDiff();

	CTimeSpan GetLocalDiff(CTime opDate)
	{
		if(opDate < omTich)
		{
			return omLocalDiff1;
		}
		else
		{
			return omLocalDiff2;
		}

		//return omLocalDiff;
	};

	CTimeSpan GetLocalDiff1() { return omLocalDiff1; };
	CTimeSpan GetLocalDiff2() { return omLocalDiff2; };
	CTime     GetTich() { return omTich; };

	void LocalToUtc(CTime &opTime);
	void UtcToLocal(CTime &opTime);

	bool UseLocalTimeAsDefault();

	COleDateTime DBStringToDateTime(const CString& ropDBString);
	CString		 COleDateTimeToDBString(const COleDateTime &opDateTime);

	//Added by Christine
	bool IsContinentAvailable();
	CString GetFieldByUrno( CString sTable, CString urno, CString sColumn );

public:
	static void TrimLeft(char *s);
	static void TrimRight(char *s);
	BOOL	LoadBitmapFromBMPFile( LPTSTR szFileName, HBITMAP *phBitmap,HPALETTE *phPalette );

private:

	char pcmCedaCmd[12];
	CTime omDiaStartTime;
	CTime omDiaEndTime;

	CString omDefaultComands;
	CString omActualComands;
	char pcmCedaComand[24];
	CString omWorkstationName; 
	bool	bmDisplayDamagedDataWarningMessage;
	bool	bmUseBackDoor;
	bool	omHtytabAdid;
private:
	CDWordArray omUrnos;

	CTimeSpan omLocalDiff1;
	CTimeSpan omLocalDiff2;
	CTime	  omTich;
	bool	  bmUseLocalTimeAsDefault;		
};


#endif
