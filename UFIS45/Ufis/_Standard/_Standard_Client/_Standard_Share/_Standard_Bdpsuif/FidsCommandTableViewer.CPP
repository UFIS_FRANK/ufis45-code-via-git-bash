// FidsCommandTableViewer.cpp 
//

#include <stdafx.h>
#include <FidsCommandTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void FidsCommandTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// FidsCommandTableViewer
//

FidsCommandTableViewer::FidsCommandTableViewer(CCSPtrArray<FIDDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomFidsCommandTable = NULL;
    ogDdx.Register(this, FID_CHANGE, CString("FIDSCOMMANDTABLEVIEWER"), CString("FidsCommand Update/new"), FidsCommandTableCf);
    ogDdx.Register(this, FID_DELETE, CString("FIDSCOMMANDTABLEVIEWER"), CString("FidsCommand Delete"), FidsCommandTableCf);

	m_Util = new UtilUC();
}

//-----------------------------------------------------------------------------------------------

FidsCommandTableViewer::~FidsCommandTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void FidsCommandTableViewer::Attach(CCSTable *popTable)
{
    pomFidsCommandTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void FidsCommandTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void FidsCommandTableViewer::MakeLines()
{
	int ilFidsCommandCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilFidsCommandCount; ilLc++)
	{
		FIDDATA *prlFidsCommandData = &pomData->GetAt(ilLc);
		MakeLine(prlFidsCommandData);
	}
}

//-----------------------------------------------------------------------------------------------

void FidsCommandTableViewer::MakeLine(FIDDATA *prpFidsCommand)
{

    //if( !IsPassFilter(prpFidsCommand)) return;

    // Update viewer data for this shift record
    FIDSCOMMANDTABLE_LINEDATA rlFidsCommand;

	rlFidsCommand.Urno = prpFidsCommand->Urno; 
	rlFidsCommand.Code = prpFidsCommand->Code; 
	rlFidsCommand.Remi = prpFidsCommand->Remi; 
	rlFidsCommand.Remt = prpFidsCommand->Remt; 
	rlFidsCommand.Blkc = prpFidsCommand->Blkc; 
	if(bgUnicode)
	{
		CString olStr = prpFidsCommand->Beme;
		CString olString ;
		m_Util->ConvOctStToByteArr(olStr,olString);
		rlFidsCommand.Beme = olString;
	}
	else
	{
	rlFidsCommand.Beme = prpFidsCommand->Beme; 
	}
	rlFidsCommand.Bemd = prpFidsCommand->Bemd; 
	rlFidsCommand.Bet3 = prpFidsCommand->Bet3; 
	rlFidsCommand.Bet4 = prpFidsCommand->Bet4; 
	rlFidsCommand.Conr = prpFidsCommand->Conr; 
	rlFidsCommand.Vafr = prpFidsCommand->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlFidsCommand.Vato = prpFidsCommand->Vato.Format("%d.%m.%Y %H:%M"); 
	if(ogFIDData.DoesRemarkCodeExist() == true)
		rlFidsCommand.Rema = prpFidsCommand->Rema;
	
	CreateLine(&rlFidsCommand);
}

//-----------------------------------------------------------------------------------------------

void FidsCommandTableViewer::CreateLine(FIDSCOMMANDTABLE_LINEDATA *prpFidsCommand)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFidsCommand(prpFidsCommand, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	FIDSCOMMANDTABLE_LINEDATA rlFidsCommand;
	rlFidsCommand = *prpFidsCommand;
    omLines.NewAt(ilLineno, rlFidsCommand);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void FidsCommandTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 9;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomFidsCommandTable->SetShowSelection(TRUE);
	pomFidsCommandTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	
	int iStrID;
	if(!bgUnicode)
		iStrID = IDS_STRING267;
	else 
		iStrID = IDS_STRING32804;	
	
	int ilPos = 1;
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 300; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	if(!bgUnicode)
	{
			//Comment 2
		rlHeader.Length = 300; 
			rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
			//Comment 3
		rlHeader.Length = 300; 
			rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
			//Comment 4
		rlHeader.Length = 300; 
			rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	if(ogFIDData.DoesRemarkCodeExist() == true)
	{
		rlHeader.Length = 300; 
		rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}

	pomFidsCommandTable->SetHeaderFields(omHeaderDataArray);
	pomFidsCommandTable->SetDefaultSeparator();
	pomFidsCommandTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Code;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Remi;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Remt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Blkc == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Beme;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if(!bgUnicode)
		{
			rlColumnData.Text = omLines[ilLineNo].Bemd;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omLines[ilLineNo].Bet3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omLines[ilLineNo].Bet4;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}
		rlColumnData.Text = omLines[ilLineNo].Conr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if(ogFIDData.DoesRemarkCodeExist() == true)
		{
			rlColumnData.Text = omLines[ilLineNo].Rema;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		pomFidsCommandTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomFidsCommandTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString FidsCommandTableViewer::Format(FIDSCOMMANDTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool FidsCommandTableViewer::FindFidsCommand(char *pcpFidsCommandKeya, char *pcpFidsCommandKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpFidsCommandKeya) &&
			 (omLines[ilItem].Keyd == pcpFidsCommandKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void FidsCommandTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    FidsCommandTableViewer *polViewer = (FidsCommandTableViewer *)popInstance;
    if (ipDDXType == FID_CHANGE) polViewer->ProcessFidsCommandChange((FIDDATA *)vpDataPointer);
    if (ipDDXType == FID_DELETE) polViewer->ProcessFidsCommandDelete((FIDDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void FidsCommandTableViewer::ProcessFidsCommandChange(FIDDATA *prpFidsCommand)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpFidsCommand->Code;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpFidsCommand->Remi;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpFidsCommand->Remt;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpFidsCommand->Blkc == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	if(bgUnicode)
	{
		CString olStr = prpFidsCommand->Beme;
		CString olString ;
		m_Util->ConvOctStToByteArr(olStr,olString);
		rlColumn.Text = olString;
	}
	else
	    rlColumn.Text = prpFidsCommand->Beme;

	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	if(!bgUnicode)
	{
		rlColumn.Text = prpFidsCommand->Bemd;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpFidsCommand->Bet3;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpFidsCommand->Bet4;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}
	rlColumn.Text = prpFidsCommand->Conr;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpFidsCommand->Vafr.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpFidsCommand->Vato.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if(ogFIDData.DoesRemarkCodeExist() == true)
	{
		rlColumn.Text = prpFidsCommand->Rema;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}

	if (FindLine(prpFidsCommand->Urno, ilItem))
	{
        FIDSCOMMANDTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpFidsCommand->Urno;
		prlLine->Code = prpFidsCommand->Code;
		prlLine->Remi = prpFidsCommand->Remi;
		prlLine->Remt = prpFidsCommand->Remt;
		prlLine->Blkc = prpFidsCommand->Blkc;

		if(bgUnicode)
		{
			CString olStr = prpFidsCommand->Beme;
			CString olString ;
			m_Util->ConvOctStToByteArr(olStr,olString);
			prlLine->Beme = olString;
		}
		else
			prlLine->Beme = prpFidsCommand->Beme;

		prlLine->Bemd = prpFidsCommand->Bemd;
		prlLine->Bet3 = prpFidsCommand->Bet3;
		prlLine->Bet4 = prpFidsCommand->Bet4;
		prlLine->Conr = prpFidsCommand->Conr;
		prlLine->Vafr = prpFidsCommand->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpFidsCommand->Vato.Format("%d.%m.%Y %H:%M");
		if(ogFIDData.DoesRemarkCodeExist() == true)
			prlLine->Rema = prpFidsCommand->Rema;

		pomFidsCommandTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomFidsCommandTable->DisplayTable();
	}
	else
	{
		MakeLine(prpFidsCommand);
		if (FindLine(prpFidsCommand->Urno, ilItem))
		{
	        FIDSCOMMANDTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomFidsCommandTable->AddTextLine(olLine, (void *)prlLine);
				pomFidsCommandTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void FidsCommandTableViewer::ProcessFidsCommandDelete(FIDDATA *prpFidsCommand)
{
	int ilItem;
	if (FindLine(prpFidsCommand->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomFidsCommandTable->DeleteTextLine(ilItem);
		pomFidsCommandTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool FidsCommandTableViewer::IsPassFilter(FIDDATA *prpFidsCommand)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int FidsCommandTableViewer::CompareFidsCommand(FIDSCOMMANDTABLE_LINEDATA *prpFidsCommand1, FIDSCOMMANDTABLE_LINEDATA *prpFidsCommand2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------

void FidsCommandTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool FidsCommandTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void FidsCommandTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void FidsCommandTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING172);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool FidsCommandTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool FidsCommandTableViewer::PrintTableLine(FIDSCOMMANDTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Code;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Remi;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Remt;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->Blkc;
				}
				break;
			case 4:
				{
					rlElement.Text		= prpLine->Beme;
				}
				break;
			case 5:
				{
					rlElement.Text		= prpLine->Bemd;
				}
				break;
			case 6:
				{
					rlElement.Text		= prpLine->Bet3;
				}
				break;
			case 7:
				{
					rlElement.Text		= prpLine->Bet4;
				}
				break;
			case 8:
				{
					rlElement.Text		= prpLine->Conr;
				}
				break;
			case 9:
				{
					rlElement.Text		= prpLine->Vafr;
				}
				break;
			case 10:
				{
					if(ogFIDData.DoesRemarkCodeExist() == false)
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Vato;
				}
				break;
			case 11:
				{
					if(ogFIDData.DoesRemarkCodeExist() == true)
					{
						rlElement.FrameRight = PRINT_FRAMETHIN;
						rlElement.Text = prpLine->Rema;				
					}
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
