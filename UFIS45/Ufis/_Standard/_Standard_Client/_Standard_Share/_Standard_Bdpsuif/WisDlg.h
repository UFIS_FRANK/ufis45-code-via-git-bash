#if !defined(AFX_WISDLG_H__D6712675_E86B_11D4_BFFA_00D0B7E2A4B5__INCLUDED_)
#define AFX_WISDLG_H__D6712675_E86B_11D4_BFFA_00D0B7E2A4B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WisDlg.h : header file
//
#include <PrivList.h>
#include <CedaORGData.h>
#include <CedaWISData.h>
#include <CCSEdit.h>
#include <AwDlg.h>

/////////////////////////////////////////////////////////////////////////////
// CWisDlg dialog

class CWisDlg : public CDialog
{
// Construction
public:
	CWisDlg(WISDATA *popWis, CWnd* pParent = NULL);   // standard constructor

	CStatic	*pomStatus;
// Dialog Data
	//{{AFX_DATA(CWisDlg)
	enum { IDD = IDD_WISDLG };
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_WISC;
	CCSEdit	m_WISD;
	CCSEdit	m_REMA;
	CCSEdit	m_ORGC;
	CButton	m_SHIR;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWisDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CWisDlg)
	afx_msg void OnBOrgAw();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	
private:
	WISDATA *pomWis;
	CString	omOldCode;
	bool bmChangeAction;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WISDLG_H__D6712675_E86B_11D4_BFFA_00D0B7E2A4B5__INCLUDED_)
