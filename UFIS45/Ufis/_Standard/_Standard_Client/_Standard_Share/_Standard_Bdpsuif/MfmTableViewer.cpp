// MfmTableViewer.cpp 
//

#include <stdafx.h>
#include <MfmTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void MfmTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// MfmTableViewer
//

MfmTableViewer::MfmTableViewer(CCSPtrArray<MFMDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomMfmTable = NULL;
    ogDdx.Register(this, MFM_CHANGE, CString("MFMTABLEVIEWER"), CString("Mfm Update"), MfmTableCf);
    ogDdx.Register(this, MFM_NEW,    CString("MFMTABLEVIEWER"), CString("Mfm New"),    MfmTableCf);
    ogDdx.Register(this, MFM_DELETE, CString("MFMTABLEVIEWER"), CString("Mfm Delete"), MfmTableCf);
}

//-----------------------------------------------------------------------------------------------

MfmTableViewer::~MfmTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void MfmTableViewer::Attach(CCSTable *popTable)
{
    pomMfmTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void MfmTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void MfmTableViewer::MakeLines()
{
	int ilMfmCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilMfmCount; ilLc++)
	{
		MFMDATA *prlMfmData = &pomData->GetAt(ilLc);
		MakeLine(prlMfmData);
	}
}

//-----------------------------------------------------------------------------------------------

void MfmTableViewer::MakeLine(MFMDATA *prpMfm)
{

    //if( !IsPassFilter(prpMfm)) return;

    // Update viewer data for this shift record
    MFMTABLE_LINEDATA rlMfm;

	rlMfm.Urno = prpMfm->Urno;
	rlMfm.Year = prpMfm->Year;
	rlMfm.Ctrc = prpMfm->Ctrc;
	rlMfm.Fm01 = prpMfm->Fm01;
	rlMfm.Fm02 = prpMfm->Fm02;
	rlMfm.Fm03 = prpMfm->Fm03;
	rlMfm.Fm04 = prpMfm->Fm04;
	rlMfm.Fm05 = prpMfm->Fm05;
	rlMfm.Fm06 = prpMfm->Fm06;
	rlMfm.Fm07 = prpMfm->Fm07;
	rlMfm.Fm08 = prpMfm->Fm08;
	rlMfm.Fm09 = prpMfm->Fm09;
	rlMfm.Fm10 = prpMfm->Fm10;
	rlMfm.Fm11 = prpMfm->Fm11;
	rlMfm.Fm12 = prpMfm->Fm12;

	CreateLine(&rlMfm);
}

//-----------------------------------------------------------------------------------------------

void MfmTableViewer::CreateLine(MFMTABLE_LINEDATA *prpMfm)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareMfm(prpMfm, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	MFMTABLE_LINEDATA rlMfm;
	rlMfm = *prpMfm;
    omLines.NewAt(ilLineno, rlMfm);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void MfmTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 13;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomMfmTable->SetShowSelection(TRUE);
	pomMfmTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING754),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);


	pomMfmTable->SetHeaderFields(omHeaderDataArray);
	pomMfmTable->SetDefaultSeparator();
	pomMfmTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Year;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ctrc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Fm01;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fm02;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fm03;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fm04;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fm05;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fm06;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fm07;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fm08;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fm09;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fm10;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fm11;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fm12;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;

		pomMfmTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomMfmTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString MfmTableViewer::Format(MFMTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool MfmTableViewer::FindMfm(char *pcpMfmKeya, char *pcpMfmKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpMfmKeya) &&
			 (omLines[ilItem].Keyd == pcpMfmKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void MfmTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    MfmTableViewer *polViewer = (MfmTableViewer *)popInstance;
    if (ipDDXType == MFM_CHANGE || ipDDXType == MFM_NEW) polViewer->ProcessMfmChange((MFMDATA *)vpDataPointer);
    if (ipDDXType == MFM_DELETE) polViewer->ProcessMfmDelete((MFMDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void MfmTableViewer::ProcessMfmChange(MFMDATA *prpMfm)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpMfm->Year;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMfm->Ctrc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = prpMfm->Fm01;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMfm->Fm02;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMfm->Fm03;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMfm->Fm04;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMfm->Fm05;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMfm->Fm06;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMfm->Fm07;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMfm->Fm08;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMfm->Fm09;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMfm->Fm10;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMfm->Fm11;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMfm->Fm12;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpMfm->Urno, ilItem))
	{
        MFMTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpMfm->Urno;
		prlLine->Year = prpMfm->Year;
		prlLine->Ctrc = prpMfm->Ctrc;
		prlLine->Fm01 = prpMfm->Fm01;
		prlLine->Fm02 = prpMfm->Fm02;
		prlLine->Fm03 = prpMfm->Fm03;
		prlLine->Fm04 = prpMfm->Fm04;
		prlLine->Fm05 = prpMfm->Fm05;
		prlLine->Fm06 = prpMfm->Fm06;
		prlLine->Fm07 = prpMfm->Fm07;
		prlLine->Fm08 = prpMfm->Fm08;
		prlLine->Fm09 = prpMfm->Fm09;
		prlLine->Fm10 = prpMfm->Fm10;
		prlLine->Fm11 = prpMfm->Fm11;
		prlLine->Fm12 = prpMfm->Fm12;

		pomMfmTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomMfmTable->DisplayTable();
	}
	else
	{
		MakeLine(prpMfm);
		if (FindLine(prpMfm->Urno, ilItem))
		{
	        MFMTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomMfmTable->AddTextLine(olLine, (void *)prlLine);
				pomMfmTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void MfmTableViewer::ProcessMfmDelete(MFMDATA *prpMfm)
{
	int ilItem;
	if (FindLine(prpMfm->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomMfmTable->DeleteTextLine(ilItem);
		pomMfmTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool MfmTableViewer::IsPassFilter(MFMDATA *prpMfm)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int MfmTableViewer::CompareMfm(MFMTABLE_LINEDATA *prpMfm1, MFMTABLE_LINEDATA *prpMfm2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpMfm1->Tifd == prpMfm2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpMfm1->Tifd > prpMfm2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void MfmTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool MfmTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void MfmTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void MfmTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING136);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool MfmTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool MfmTableViewer::PrintTableLine(MFMTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Year;
				break;
			case 1:
				rlElement.Text		= prpLine->Ctrc;
				break;
			case 2:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Fm01;
				break;
			case 3:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Fm02;
				break;
			case 4:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Fm03;
				break;
			case 5:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Fm04;
				break;
			case 6:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Fm05;
				break;
			case 7:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Fm06;
				break;
			case 8:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Fm07;
				break;
			case 9:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Fm08;
				break;
			case 10:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Fm09;
				break;
			case 11:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Fm10;
				break;
			case 12:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Fm11;
				break;
			case 13:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.FrameRight  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Fm12;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
