#ifndef __TAXIWAYTABLEVIEWER_H__
#define __TAXIWAYTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaTWYData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct TAXIWAYTABLE_LINEDATA
{
	long	Urno;
	CString	Tnam;
	CString	Rgrw;
	CString	Nafr;
	CString	Nato;
	CString	Resn;
	CString	Vafr;
	CString	Vato;
	CString	Home;
};

/////////////////////////////////////////////////////////////////////////////
// TaxiwayTableViewer

class TaxiwayTableViewer : public CViewer
{
// Constructions
public:
    TaxiwayTableViewer(CCSPtrArray<TWYDATA> *popData);
    ~TaxiwayTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	BOOL IsPassFilter(TWYDATA *prpTaxiway);
	int CompareTaxiway(TAXIWAYTABLE_LINEDATA *prpTaxiway1, TAXIWAYTABLE_LINEDATA *prpTaxiway2);
    void MakeLines();
	void MakeLine(TWYDATA *prpTaxiway);
	BOOL FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(TAXIWAYTABLE_LINEDATA *prpTaxiway);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(TAXIWAYTABLE_LINEDATA *prpLine);
	void ProcessTaxiwayChange(TWYDATA *prpTaxiway);
	void ProcessTaxiwayDelete(TWYDATA *prpTaxiway);
	BOOL FindTaxiway(char *prpTaxiwayKeya, char *prpTaxiwayKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	BOOL bmIsFromSearch;
// Attributes
private:
    CCSTable *pomTaxiwayTable;
	CCSPtrArray<TWYDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<TAXIWAYTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(TAXIWAYTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__TAXIWAYTABLEVIEWER_H__
