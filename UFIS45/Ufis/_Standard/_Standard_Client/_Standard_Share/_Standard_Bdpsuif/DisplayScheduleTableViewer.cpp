// DisplayScheduleTableViewer.cpp
//

#include <stdafx.h>
#include <DisplayScheduleTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <fstream.h>
#include <iomanip.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void DisplayScheduleTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// DisplayScheduleTableViewer
//

DisplayScheduleTableViewer::DisplayScheduleTableViewer(CCSPtrArray<DSCDATA> *popData)
{
	pomData = popData;
	bmIsFromSearch = FALSE;
    pomDisplayScheduleTable = NULL;
    ogDdx.Register(this, DSC_CHANGE, CString("DISPLAYSCHEDULETABLEVIEWER"), CString("DisplaySchedule Update/new"), DisplayScheduleTableCf);
    ogDdx.Register(this, DSC_DELETE, CString("DISPLAYSCHEDULETABLEVIEWER"), CString("DisplaySchedule Delete"), DisplayScheduleTableCf);
}

//-----------------------------------------------------------------------------------------------

DisplayScheduleTableViewer::~DisplayScheduleTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void DisplayScheduleTableViewer::Attach(CCSTable *popTable)
{
    pomDisplayScheduleTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void DisplayScheduleTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();
    MakeLines();
	UpdateDisplaySchedule();
}

//-----------------------------------------------------------------------------------------------

void DisplayScheduleTableViewer::MakeLines()
{
	int ilDisplayScheduleCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilDisplayScheduleCount; ilLc++)
	{
		DSCDATA *prlDisplayScheduleData = &pomData->GetAt(ilLc);
		MakeLine(prlDisplayScheduleData);
	}
}

//-----------------------------------------------------------------------------------------------

void DisplayScheduleTableViewer::MakeLine(DSCDATA *prpDisplaySchedule)
{

    // Update viewer data for this shift record
    DISPLAYSCHEDULETABLE_LINEDATA rlDisplaySchedule;

	rlDisplaySchedule.Urno = prpDisplaySchedule->Urno;
	rlDisplaySchedule.Devn = prpDisplaySchedule->Devn;
	rlDisplaySchedule.Vafr = PrepareDisplayDateFormat(prpDisplaySchedule->Vafr);
	rlDisplaySchedule.Vato = PrepareDisplayDateFormat(prpDisplaySchedule->Vato);

	CreateLine(&rlDisplaySchedule);
}

//-----------------------------------------------------------------------------------------------

void DisplayScheduleTableViewer::CreateLine(DISPLAYSCHEDULETABLE_LINEDATA *prpDisplaySchedule)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareDisplaySchedule(prpDisplaySchedule, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	DISPLAYSCHEDULETABLE_LINEDATA rlDisplaySchedule;
	rlDisplaySchedule = *prpDisplaySchedule;
    omLines.NewAt(ilLineno, rlDisplaySchedule);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplaySchedule: Load data selected by filter conditions to the DisplaySchedule by using "omTable"

void DisplayScheduleTableViewer::UpdateDisplaySchedule()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 3;
	omHeaderDataArray.DeleteAll();

	pomDisplayScheduleTable->SetShowSelection(TRUE);
	pomDisplayScheduleTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	CString olHeader(LoadStg(IDS_STRING1002));

	int ilPos = 1;
	rlHeader.Length = 100;
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader); //Device Name

	rlHeader.Length = 100;
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// valid from

	rlHeader.Length = 100;
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// valid to

	pomDisplayScheduleTable->SetHeaderFields(omHeaderDataArray);
	pomDisplayScheduleTable->SetDefaultSeparator();
	pomDisplayScheduleTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;

    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Devn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomDisplayScheduleTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomDisplayScheduleTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

static void DisplayScheduleTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    DisplayScheduleTableViewer *polViewer = (DisplayScheduleTableViewer *)popInstance;
    if (ipDDXType == DSC_CHANGE)
		polViewer->ProcessDisplayScheduleChange((DSCDATA *)vpDataPointer);
    else if (ipDDXType == DSC_DELETE)
		polViewer->ProcessDisplayScheduleDelete((DSCDATA *)vpDataPointer);
}


//-----------------------------------------------------------------------------------------------

void DisplayScheduleTableViewer::ProcessDisplayScheduleChange(DSCDATA *prpDisplaySchedule)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpDisplaySchedule->Devn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = PrepareDisplayDateFormat(prpDisplaySchedule->Vafr);
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = PrepareDisplayDateFormat(prpDisplaySchedule->Vato);
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpDisplaySchedule->Urno, ilItem))
	{
        DISPLAYSCHEDULETABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpDisplaySchedule->Urno;
		prlLine->Devn = prpDisplaySchedule->Devn;
		prlLine->Vafr = PrepareDisplayDateFormat(prpDisplaySchedule->Vafr);
		prlLine->Vato = PrepareDisplayDateFormat(prpDisplaySchedule->Vato);
		pomDisplayScheduleTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomDisplayScheduleTable->DisplayTable();
	}
	else
	{
		MakeLine(prpDisplaySchedule);
		if (FindLine(prpDisplaySchedule->Urno, ilItem))
		{
	        DISPLAYSCHEDULETABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomDisplayScheduleTable->AddTextLine(olLine, (void *)prlLine);
				pomDisplayScheduleTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void DisplayScheduleTableViewer::ProcessDisplayScheduleDelete(DSCDATA *prpDisplaySchedule)
{
	int ilItem;
	if (FindLine(prpDisplaySchedule->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomDisplayScheduleTable->DeleteTextLine(ilItem);
		pomDisplayScheduleTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

int DisplayScheduleTableViewer::CompareDisplaySchedule(DISPLAYSCHEDULETABLE_LINEDATA *prpDisplaySchedule1, DISPLAYSCHEDULETABLE_LINEDATA *prpDisplaySchedule2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------

void DisplayScheduleTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool DisplayScheduleTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

CString DisplayScheduleTableViewer::PrepareDisplayDateFormat(const CString& ropDate
															 ,bool blSetDefaultIncaseEmptyDate) const
{
	CString olReturnDateString;
	if(ropDate.GetLength() == 0 && blSetDefaultIncaseEmptyDate == true)
	{
		return CTime::GetCurrentTime().Format("%d.%m.%Y");
	}
	else if(ropDate.GetLength() > 0)
	{
		olReturnDateString = ropDate.Mid(6,2) + "." + ropDate.Mid(4,2) + "." + ropDate.Mid(0,4);
	}
	return olReturnDateString;
}
//-----------------------------------------------------------------------------------------------

void DisplayScheduleTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void DisplayScheduleTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING1001);
	int ilOrientation = PRINT_LANDSCAPE;
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;

//			calculate count of pages
			CUIntArray olColsPerPage;
			int ilPages = pomPrint->CalculateCountOfPages(this->omHeaderDataArray,olColsPerPage);

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for (int ilPage = 0; ilPage < ilPages; ilPage++)
			{
				pomPrint->imPageNo = 0;
				int ilLines = omLines.GetSize();
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				olFooter1.Format("%d %s",ilLines,omTableName);
				for(int i = 0; i < ilLines; i++ )
				{
					if(pomPrint->imLineNo >= pomPrint->imMaxLines)
					{
						if(pomPrint->imPageNo > 0)
						{
							olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
							pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
							pomPrint->omCdc.EndPage();
						}
						PrintTableHeader(ilPage,olColsPerPage);
					}
					if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],true);
					}
					else
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],false);
					}
				}
				olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
				pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
				pomPrint->omCdc.EndPage();
			}
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool DisplayScheduleTableViewer::PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage)
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{

		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength;
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool DisplayScheduleTableViewer::PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,DISPLAYSCHEDULETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{

		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength;
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		switch(i)
		{
		case 0:
			{
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Devn;
			}
			break;
		case 1:
			{
				rlElement.Text		= prpLine->Vafr;
			}
			break;
		case 2:
			{
				rlElement.Text		= prpLine->Vato;
			}
			break;
		default :
			break;
		}
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
bool DisplayScheduleTableViewer::CreateExcelFile(const CString& opTrenner,const CString& ropListSeparator)
{
	ofstream of;

	CString olTmpPath = CCSLog::GetTmpPath();

	CString olTableName;
	olTableName.Format("%s -   %s", "DSCTAB", (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");

	char pHeader[256];
	strcpy (pHeader, olTmpPath);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);


	of  << setw(0) << "DSCTAB" << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(0) << LoadStg(IDS_STRING935) //CString("Device name");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING230) //CString("Valid from");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING231) //CString("Valid to");
		;

	int ilCount = omLines.GetSize();
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	for(int i = 0; i < ilCount; i++)
	{
		DISPLAYSCHEDULETABLE_LINEDATA rlLine = omLines[i];
		of.setf(ios::left, ios::adjustfield);

		of   << setw(0) << rlLine.Devn						   // Device Name
		     << setw(0) << opTrenner
		     << setw(0) << rlLine.Vafr							// Valid From
		     << setw(0) << opTrenner
		     << setw(0) << rlLine.Vato							// Valid To
			 ;
	    of	<< endl;

	}

	of.close();
	return true;
}
//-----------------------------------------------------------------------------------------------