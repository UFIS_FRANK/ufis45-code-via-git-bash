#if !defined(AFX_EQUIPMENTTYPEDLG_H__AB36A834_7540_11D1_B430_0000B45A33F5__INCLUDED_)
#define AFX_EQUIPMENTTYPEDLG_H__AB36A834_7540_11D1_B430_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// EquipmentTypeDlg.h : header file
//
#include <CedaEqtData.h>

#include <CCSEdit.h>
#include <CCSTable.h>
#include <resrc1.h>		// main symbols#

/////////////////////////////////////////////////////////////////////////////
// EquipmentTypeDlg dialog

class EquipmentTypeDlg : public CDialog
{
// Construction
public:
	EquipmentTypeDlg(EQTDATA *popEqt, CWnd* pParent = NULL);   // standard constructor
	
	EQTDATA *pomEqt;
// Dialog Data
	//{{AFX_DATA(EquipmentTypeDlg)
	enum { IDD = IDD_EQUIPMENTTYPEDLG };
	CCSEdit	m_CODE;
	CCSEdit	m_NAME;
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(EquipmentTypeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
private:
	
protected:
// Generated message map functions
	//{{AFX_MSG(EquipmentTypeDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EQUIPMENTTYPEDLG_H__AB36A834_7540_11D1_B430_0000B45A33F5__INCLUDED_)
