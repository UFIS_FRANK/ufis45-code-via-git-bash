// CedaChtData.cpp
 
#include <stdafx.h>
#include <CedaChtData.h>
#include <resource.h>


void ProcessChtCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaChtData::CedaChtData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(CHTDATA, ChtDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Chtc,"CHTC")
		FIELD_CHAR_TRIM	(Chtn,"CHTN")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(ChtDataRecInfo)/sizeof(ChtDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ChtDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"CHT");
    sprintf(pcmListOfFields,"CDAT,LSTU,CHTC,CHTN,URNO,USEC,USEU,VAFR,VATO");


	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//----------------------------------------------------------------------------------------------------

void CedaChtData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("CHTC");
	ropFields.Add("CHTN");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING459));
	ropDesription.Add(LoadStg(IDS_STRING460));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaChtData::Register(void)
{
	ogDdx.Register((void *)this,BC_CHT_CHANGE,	CString("CHTDATA"), CString("Cht-changed"),	ProcessChtCf);
	ogDdx.Register((void *)this,BC_CHT_NEW,		CString("CHTDATA"), CString("Cht-new"),		ProcessChtCf);
	ogDdx.Register((void *)this,BC_CHT_DELETE,	CString("CHTDATA"), CString("Cht-deleted"),	ProcessChtCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaChtData::~CedaChtData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaChtData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaChtData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		CHTDATA *prlCht = new CHTDATA;
		if ((ilRc = GetFirstBufferRecord(prlCht)) == true)
		{
			omData.Add(prlCht);//Update omData
			omUrnoMap.SetAt((void *)prlCht->Urno,prlCht);
		}
		else
		{
			delete prlCht;
		}
	}
    return true;

}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaChtData::Insert(CHTDATA *prpCht)
{
	prpCht->IsChanged = DATA_NEW;
	if(Save(prpCht) == false) return false; //Update Database
	InsertInternal(prpCht);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaChtData::InsertInternal(CHTDATA *prpCht)
{
	ogDdx.DataChanged((void *)this, CHT_NEW,(void *)prpCht ); //Update Viewer
	omData.Add(prpCht);//Update omData
	omUrnoMap.SetAt((void *)prpCht->Urno,prpCht);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaChtData::Delete(long lpUrno)
{
	CHTDATA *prlCht = GetChtByUrno(lpUrno);
	if (prlCht != NULL)
	{
		prlCht->IsChanged = DATA_DELETED;
		if(Save(prlCht) == false) return false; //Update Database
		DeleteInternal(prlCht);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaChtData::DeleteInternal(CHTDATA *prpCht)
{
	ogDdx.DataChanged((void *)this,CHT_DELETE,(void *)prpCht); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpCht->Urno);
	int ilChtCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilChtCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpCht->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaChtData::Update(CHTDATA *prpCht)
{
	if (GetChtByUrno(prpCht->Urno) != NULL)
	{
		if (prpCht->IsChanged == DATA_UNCHANGED)
		{
			prpCht->IsChanged = DATA_CHANGED;
		}
		if(Save(prpCht) == false) return false; //Update Database
		UpdateInternal(prpCht);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaChtData::UpdateInternal(CHTDATA *prpCht)
{
	CHTDATA *prlCht = GetChtByUrno(prpCht->Urno);
	if (prlCht != NULL)
	{
		*prlCht = *prpCht; //Update omData
		ogDdx.DataChanged((void *)this,CHT_CHANGE,(void *)prlCht); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

CHTDATA *CedaChtData::GetChtByUrno(long lpUrno)
{
	CHTDATA  *prlCht;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCht) == TRUE)
	{
		return prlCht;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaChtData::ReadSpecialData(CCSPtrArray<CHTDATA> *popCht,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","CHT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","CHT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popCht != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			CHTDATA *prpCht = new CHTDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpCht,CString(pclFieldList))) == true)
			{
				popCht->Add(prpCht);
			}
			else
			{
				delete prpCht;
			}
		}
		if(popCht->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaChtData::Save(CHTDATA *prpCht)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpCht->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpCht->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpCht);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpCht->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCht->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpCht);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpCht->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCht->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessChtCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogChtData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaChtData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlChtData;
	prlChtData = (struct BcStruct *) vpDataPointer;
	CHTDATA *prlCht;
	if(ipDDXType == BC_CHT_NEW)
	{
		prlCht = new CHTDATA;
		GetRecordFromItemList(prlCht,prlChtData->Fields,prlChtData->Data);
		if(ValidateChtBcData(prlCht->Urno)) //Prf: 8795
		{
			InsertInternal(prlCht);
		}
		else
		{
			delete prlCht;
		}
	}
	if(ipDDXType == BC_CHT_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlChtData->Selection);
		prlCht = GetChtByUrno(llUrno);
		if(prlCht != NULL)
		{
			GetRecordFromItemList(prlCht,prlChtData->Fields,prlChtData->Data);
			if(ValidateChtBcData(prlCht->Urno)) //Prf: 8795
			{
				UpdateInternal(prlCht);
			}
			else
			{
				DeleteInternal(prlCht);
			}
		}
	}
	if(ipDDXType == BC_CHT_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlChtData->Selection);

		prlCht = GetChtByUrno(llUrno);
		if (prlCht != NULL)
		{
			DeleteInternal(prlCht);
		}
	}
}

//Prf: 8795
//--ValidateChtBcData--------------------------------------------------------------------------------------

bool CedaChtData::ValidateChtBcData(const long& lrpUrno)
{
	bool blValidateChtBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<CHTDATA> olChts;
		if(!ReadSpecialData(&olChts,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateChtBcData = false;
		}
	}
	return blValidateChtBcData;
}

//---------------------------------------------------------------------------------------------------------
