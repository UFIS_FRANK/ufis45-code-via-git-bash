#ifndef __SrcTableViewer_H__
#define __SrcTableViewer_H__

#include <stdafx.h>
#include <CedaSrcData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct SRCTABLE_LINEDATA
{
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	CString  Srcc; 	// Code
	CString  Srcd; 	// Bezeichnung

};

/////////////////////////////////////////////////////////////////////////////
// SrcTableViewer

	  
class SrcTableViewer : public CViewer
{
// Constructions
public:
    SrcTableViewer(CCSPtrArray<SRCDATA> *popData);
    ~SrcTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(SRCDATA *prpSrc);
	int CompareSrc(SRCTABLE_LINEDATA *prpSrc1, SRCTABLE_LINEDATA *prpSrc2);
    void MakeLines();
	void MakeLine(SRCDATA *prpSrc);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(SRCTABLE_LINEDATA *prpSrc);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(SRCTABLE_LINEDATA *prpLine);
	void ProcessSrcChange(SRCDATA *prpSrc);
	void ProcessSrcDelete(SRCDATA *prpSrc);
	bool FindSrc(char *prpSrcKeya, char *prpSrcKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomSrcTable;
	CCSPtrArray<SRCDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<SRCTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(SRCTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__SrcTableViewer_H__
