#if !defined(AFX_AWDLG_H__435D8F21_784E_11D1_B434_0000B45A33F5__INCLUDED_)
#define AFX_AWDLG_H__435D8F21_784E_11D1_B434_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// AwDlg.h : header file
//

#include <gridcontrol.h>
/////////////////////////////////////////////////////////////////////////////
// AwDlg dialog

class AwDlg : public CDialog
{
// Construction
public:
	AwDlg(CStringArray *popColumn1, 
		  CStringArray *popColumn2, 
		  CStringArray *popUrnos, 
		  CString opHeader1 = CString(""),
		  CString opHeader2 = CString(""),
		  CWnd* pParent = NULL);   // standard constructor

	~AwDlg();
public:
	CString			omReturnString;
	long			lmReturnUrno;
	bool			bmMultipleSelection;

	CStringArray	omReturnStrings;
	CUIntArray		omReturnUrnos;
// Dialog Data
private:
	//{{AFX_DATA(AwDlg)
	enum { IDD = IDD_AW_DLG };
	CListBox	m_Alc;
	CListBox	m_Act;
	CButton	m_OK;
	CStatic	m_Frame;
	//}}AFX_DATA

	CStringArray	*pomColumn1; 
	CStringArray	*pomColumn2;
	CStringArray	*pomUrnos;
	 CString		omHeader1;
	 CString		omHeader2;

	 //*** 27.08.99 SHA ***
	 CStringArray	*ogAct;
	 CStringArray	*ogAlt;
	 CString		olReturnAlc;
	 CString		olReturnAct;
	 BOOL			obShowCodes;	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AwDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AwDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
    afx_msg LONG OnGridLButtonDblclk(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CGridControl		*pomGrid;
	void InitGrid();
	void FillGrid();	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AWDLG_H__435D8F21_784E_11D1_B434_0000B45A33F5__INCLUDED_)
