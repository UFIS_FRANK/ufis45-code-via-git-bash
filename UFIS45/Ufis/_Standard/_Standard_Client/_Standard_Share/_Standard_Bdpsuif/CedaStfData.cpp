// CedaStfData.cpp
 
#include <stdafx.h>
#include <CedaStfData.h>
#include <resource.h>
#include <CedaHAGData.h>
#include <PrmConfig.h>

void ProcessStfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaStfData::CedaStfData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for STFDataStruct
	BEGIN_CEDARECINFO(STFDATA,STFDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_OLEDATE	(Dodm,"DODM")
		FIELD_OLEDATE	(Doem,"DOEM")
		FIELD_CHAR_TRIM	(Finm,"FINM")
		FIELD_CHAR_TRIM	(Gsmn,"GSMN")
		FIELD_CHAR_TRIM	(Ktou,"KTOU")
		FIELD_CHAR_TRIM	(Lanm,"LANM")
		FIELD_CHAR_TRIM	(Lino,"LINO")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Makr,"MAKR")
		FIELD_CHAR_TRIM	(Matr,"MATR")
		FIELD_CHAR_TRIM	(Peno,"PENO")
		FIELD_CHAR_TRIM	(Perc,"PERC")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Shnm,"SHNM")
		FIELD_CHAR_TRIM	(Sken,"SKEN")
		FIELD_CHAR_TRIM	(Teld,"TELD")
		FIELD_CHAR_TRIM	(Telh,"TELH")
		FIELD_CHAR_TRIM	(Telp,"TELP")
		FIELD_CHAR_TRIM	(Tohr,"TOHR")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Zutb,"ZUTB")
		FIELD_CHAR_TRIM	(Zutf,"ZUTF")
		FIELD_CHAR_TRIM	(Zutn,"ZUTN")
		FIELD_CHAR_TRIM	(Zutw,"ZUTW")
		FIELD_CHAR_TRIM	(Pnof,"PNOF")
		FIELD_CHAR_TRIM	(Pdgl,"PDGL")
		FIELD_CHAR_TRIM	(Pdkl,"PDKL")
		FIELD_CHAR_TRIM	(Pmag,"PMAG")
		FIELD_CHAR_TRIM	(Pmak,"PMAK")
		FIELD_CHAR_TRIM	(Rels,"RELS")
		FIELD_CHAR_TRIM	(Kind,"KIND")
		FIELD_CHAR_TRIM	(Kids,"KIDS")
		FIELD_CHAR_TRIM	(Nati,"NATI")
		FIELD_OLEDATE	(Dobk,"DOBK")
		FIELD_CHAR_TRIM	(Gebu,"GEBU")
		FIELD_CHAR_TRIM	(Regi,"REGI")
		FIELD_CHAR_TRIM	(Cano,"CANO")
		FIELD_CHAR_TRIM	(Filt,"FILT")
	END_CEDARECINFO //(STFDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(STFDataRecInfo)/sizeof(STFDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&STFDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"STF");
	strcpy(pcmListOfFields,"CDAT,DODM,DOEM,FINM,GSMN,KTOU,LANM,LINO,LSTU,MAKR,MATR,PENO,PERC,"
						   "PRFL,REMA,SHNM,SKEN,TELD,TELH,TELP,TOHR,URNO,USEC,USEU,ZUTB,"
						   "ZUTF,ZUTN,ZUTW,PNOF,PDGL,PDKL,PMAG,PMAK,RELS,KIND,KIDS,NATI,DOBK,GEBU,REGI,CANO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaStfData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("DODM");
	ropFields.Add("DOEM");
	ropFields.Add("FINM");
	ropFields.Add("GSMN");
	ropFields.Add("KTOU");
	ropFields.Add("LANM");
	ropFields.Add("LINO");
	ropFields.Add("LSTU");
	ropFields.Add("MAKR");
	ropFields.Add("MATR");
	ropFields.Add("PENO");
	ropFields.Add("PERC");
	ropFields.Add("PRFL");
	ropFields.Add("REMA");
	ropFields.Add("SHNM");
	ropFields.Add("SKEN");
	ropFields.Add("TELD");
	ropFields.Add("TELH");
	ropFields.Add("TELP");
	ropFields.Add("TOHR");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("ZUTB");
	ropFields.Add("ZUTF");
	ropFields.Add("ZUTN");
	ropFields.Add("ZUTW");
	ropFields.Add("PNOF");
	ropFields.Add("PDGL");
	ropFields.Add("PDKL");
	ropFields.Add("PMAG");
	ropFields.Add("PMAK");
	ropFields.Add("KIND");
	ropFields.Add("KIDS");
	ropFields.Add("NATI");
	ropFields.Add("DOBK");
	ropFields.Add("GEBU");
	ropFields.Add("CANO");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING65));
	ropDesription.Add(LoadStg(IDS_STRING67));
	ropDesription.Add(LoadStg(IDS_STRING69));
	ropDesription.Add(LoadStg(IDS_STRING638));
	ropDesription.Add(LoadStg(IDS_STRING70));
	ropDesription.Add(LoadStg(IDS_STRING288));
	ropDesription.Add(LoadStg(IDS_STRING639));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING523));
	ropDesription.Add(LoadStg(IDS_STRING640));
	ropDesription.Add(LoadStg(IDS_STRING71));
	ropDesription.Add(LoadStg(IDS_STRING72));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING342));
	ropDesription.Add(LoadStg(IDS_STRING301));
	ropDesription.Add(LoadStg(IDS_STRING524));
	ropDesription.Add(LoadStg(IDS_STRING73));
	ropDesription.Add(LoadStg(IDS_STRING74));
	ropDesription.Add(LoadStg(IDS_STRING75));
	ropDesription.Add(LoadStg(IDS_STRING525));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING76));
	ropDesription.Add(LoadStg(IDS_STRING77));
	ropDesription.Add(LoadStg(IDS_STRING78));
	ropDesription.Add(LoadStg(IDS_STRING79));
	ropDesription.Add(LoadStg(IDS_STRING734));
	ropDesription.Add(LoadStg(IDS_STRING735));
	ropDesription.Add(LoadStg(IDS_STRING736));
	ropDesription.Add(LoadStg(IDS_STRING737));
	ropDesription.Add(LoadStg(IDS_STRING738));
	ropDesription.Add(LoadStg(IDS_STRING740));
	ropDesription.Add(LoadStg(IDS_STRING880));
	ropDesription.Add(LoadStg(IDS_STRING741));
	ropDesription.Add(LoadStg(IDS_STRING742));
	ropDesription.Add(LoadStg(IDS_STRING748));
	ropDesription.Add(LoadStg(IDS_STRING858));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaStfData::Register(void)
{
	ogDdx.Register((void *)this,BC_STF_CHANGE,	CString("STFDATA"), CString("Stf-changed"),	ProcessStfCf);
	ogDdx.Register((void *)this,BC_STF_NEW,		CString("STFDATA"), CString("Stf-new"),		ProcessStfCf);
	ogDdx.Register((void *)this,BC_STF_DELETE,	CString("STFDATA"), CString("Stf-deleted"),	ProcessStfCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaStfData::~CedaStfData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaStfData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaStfData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
	char clSelection[512];
	bool blUseHag = ogBasicData.IsPrmHandlingAgentEnabled();

	char blHagHsna[10] = "XXX";

	if (blUseHag)
	{
		prmHag = ogPrmConfig.getHandlingAgent();
		if (prmHag != NULL)
		{
			strcpy(blHagHsna,prmHag->Hsna);
		}

		strcpy(pcmListOfFields,"CDAT,DODM,DOEM,FINM,GSMN,KTOU,LANM,LINO,LSTU,MAKR,MATR,PENO,PERC,"
						   "PRFL,REMA,SHNM,SKEN,TELD,TELH,TELP,TOHR,URNO,USEC,USEU,ZUTB,"
						   "ZUTF,ZUTN,ZUTW,PNOF,PDGL,PDKL,PMAG,PMAK,RELS,KIND,KIDS,NATI,DOBK,GEBU,REGI,CANO,FILT");

	}
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		if (blUseHag)
		{
			sprintf(clSelection," WHERE FILT = '%s'",ogPrmConfig.getHandlingAgentShortName());
			ilRc = CedaAction("RT",clSelection);
		}
		else
		{
			ilRc = CedaAction("RT");
		}
		
	}
	else
	{
		//ilRc = CedaAction("RT", pspWhere);
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		STFDATA *prlStf = new STFDATA;
		if ((ilRc = GetFirstBufferRecord(prlStf)) == true)
		{
			omData.Add(prlStf);//Update omData
			omUrnoMap.SetAt((void *)prlStf->Urno,prlStf);
		}
		else
		{
			delete prlStf;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaStfData::Insert(STFDATA *prpStf)
{
	prpStf->IsChanged = DATA_NEW;
	if(Save(prpStf) == false) return false; //Update Database
	InsertInternal(prpStf);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaStfData::InsertInternal(STFDATA *prpStf)
{
	ogDdx.DataChanged((void *)this, STF_NEW,(void *)prpStf ); //Update Viewer
	omData.Add(prpStf);//Update omData
	omUrnoMap.SetAt((void *)prpStf->Urno,prpStf);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaStfData::Delete(long lpUrno)
{
	STFDATA *prlStf = GetStfByUrno(lpUrno);
	if (prlStf != NULL)
	{
		prlStf->IsChanged = DATA_DELETED;
		//*** 29.09.99 SHA ***
		//if(Save(prlStf) == false) return false; //Update Database
		Save(prlStf);
		DeleteInternal(prlStf);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaStfData::DeleteInternal(STFDATA *prpStf)
{
	ogDdx.DataChanged((void *)this,STF_DELETE,(void *)prpStf); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpStf->Urno);
	int ilStfCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilStfCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpStf->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaStfData::Update(STFDATA *prpStf)
{
	if (GetStfByUrno(prpStf->Urno) != NULL)
	{
		if (prpStf->IsChanged == DATA_UNCHANGED)
		{
			prpStf->IsChanged = DATA_CHANGED;
		}
		if(Save(prpStf) == false) return false; //Update Database
		UpdateInternal(prpStf);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaStfData::UpdateInternal(STFDATA *prpStf)
{
	STFDATA *prlStf = GetStfByUrno(prpStf->Urno);
	if (prlStf != NULL)
	{
		*prlStf = *prpStf; //Update omData
		ogDdx.DataChanged((void *)this,STF_CHANGE,(void *)prlStf); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

STFDATA *CedaStfData::GetStfByUrno(long lpUrno)
{
	STFDATA  *prlStf;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlStf) == TRUE)
	{
		return prlStf;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaStfData::ReadSpecialData(CCSPtrArray<STFDATA> *popStf,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","STF",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","STF",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popStf != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			STFDATA *prpStf = new STFDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpStf,CString(pclFieldList))) == true)
			{
				popStf->Add(prpStf);
			}
			else
			{
				delete prpStf;
			}
		}
		if(popStf->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaStfData::Save(STFDATA *prpStf)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpStf->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	bool blUseHag = ogBasicData.IsPrmHandlingAgentEnabled();

	if (blUseHag)
	{
		if (prmHag != NULL)
		{
			strcpy(prpStf->Filt,prmHag->Hsna);
		}
		else
		{
			return false;
		}
	}


	switch(prpStf->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpStf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpStf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpStf->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpStf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpStf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpStf->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessStfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogStfData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaStfData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlStfData;
	prlStfData = (struct BcStruct *) vpDataPointer;
	STFDATA *prlStf;
	if(ipDDXType == BC_STF_NEW)
	{
		prlStf = new STFDATA;
		GetRecordFromItemList(prlStf,prlStfData->Fields,prlStfData->Data);
		if(ValidateStfBcData(prlStf->Urno)) //Prf: 8795
		{
			InsertInternal(prlStf);
		}
		else
		{
			delete prlStf;
		}
	}
	if(ipDDXType == BC_STF_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlStfData->Selection);
		prlStf = GetStfByUrno(llUrno);
		if(prlStf != NULL)
		{
			GetRecordFromItemList(prlStf,prlStfData->Fields,prlStfData->Data);
			if(ValidateStfBcData(prlStf->Urno)) //Prf: 8795
			{
				UpdateInternal(prlStf);
			}
			else
			{
				DeleteInternal(prlStf);
			}
		}
	}
	if(ipDDXType == BC_STF_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlStfData->Selection);

		prlStf = GetStfByUrno(llUrno);
		if (prlStf != NULL)
		{
			DeleteInternal(prlStf);
		}
	}
}

//Prf: 8795
//--ValidateStfBcData--------------------------------------------------------------------------------------

bool CedaStfData::ValidateStfBcData(const long& lrpUrno)
{
	bool blValidateStfBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<STFDATA> olStfs;
		if(!ReadSpecialData(&olStfs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateStfBcData = false;
		}
	}
	return blValidateStfBcData;
}

//---------------------------------------------------------------------------------------------------------
