// FlugplansaisonTableViewer.cpp 
//

#include <stdafx.h>
#include <FlugplansaisonTableViewer.h>
#include <CCSDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void FlugplansaisonTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// FlugplansaisonTableViewer
//

FlugplansaisonTableViewer::FlugplansaisonTableViewer(CCSPtrArray<SEADATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomFlugplansaisonTable = NULL;
    ogDdx.Register(this, SEA_NEW,	 CString("FLUGPLANSAISONTABLEVIEWER"), CString("Flugplansaison New"),	 FlugplansaisonTableCf);
    ogDdx.Register(this, SEA_CHANGE, CString("FLUGPLANSAISONTABLEVIEWER"), CString("Flugplansaison Update"), FlugplansaisonTableCf);
    ogDdx.Register(this, SEA_DELETE, CString("FLUGPLANSAISONTABLEVIEWER"), CString("Flugplansaison Delete"), FlugplansaisonTableCf);
}

//-----------------------------------------------------------------------------------------------

FlugplansaisonTableViewer::~FlugplansaisonTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void FlugplansaisonTableViewer::Attach(CCSTable *popTable)
{
    pomFlugplansaisonTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void FlugplansaisonTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void FlugplansaisonTableViewer::MakeLines()
{
	int ilFlugplansaisonCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilFlugplansaisonCount; ilLc++)
	{
		SEADATA *prlFlugplansaisonData = &pomData->GetAt(ilLc);
		MakeLine(prlFlugplansaisonData);
	}
}

//-----------------------------------------------------------------------------------------------

void FlugplansaisonTableViewer::MakeLine(SEADATA *prpFlugplansaison)
{

    //if( !IsPassFilter(prpFlugplansaison)) return;

    // Update viewer data for this shift record
    FLUGPLANSAISONTABLE_LINEDATA rlFlugplansaison;

	rlFlugplansaison.Urno = prpFlugplansaison->Urno; 
	rlFlugplansaison.Beme = prpFlugplansaison->Beme; 
	rlFlugplansaison.Seas = prpFlugplansaison->Seas; 
	rlFlugplansaison.Vpfr = prpFlugplansaison->Vpfr.Format("%d.%m.%Y %H:%M");
	rlFlugplansaison.Vpto = prpFlugplansaison->Vpto.Format("%d.%m.%Y %H:%M");

	CreateLine(&rlFlugplansaison);
}

//-----------------------------------------------------------------------------------------------

void FlugplansaisonTableViewer::CreateLine(FLUGPLANSAISONTABLE_LINEDATA *prpFlugplansaison)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlugplansaison(prpFlugplansaison, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	FLUGPLANSAISONTABLE_LINEDATA rlFlugplansaison;
	rlFlugplansaison = *prpFlugplansaison;
    omLines.NewAt(ilLineno, rlFlugplansaison);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void FlugplansaisonTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 3;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomFlugplansaisonTable->SetShowSelection(TRUE);
	pomFlugplansaisonTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING268),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 166; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING268),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING268),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING268),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomFlugplansaisonTable->SetHeaderFields(omHeaderDataArray);
	pomFlugplansaisonTable->SetDefaultSeparator();
	pomFlugplansaisonTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Seas;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Beme;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vpfr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vpto;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomFlugplansaisonTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomFlugplansaisonTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString FlugplansaisonTableViewer::Format(FLUGPLANSAISONTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool FlugplansaisonTableViewer::FindFlugplansaison(char *pcpFlugplansaisonKeya, char *pcpFlugplansaisonKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpFlugplansaisonKeya) &&
			 (omLines[ilItem].Keyd == pcpFlugplansaisonKeyd)    )
			return true;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void FlugplansaisonTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    FlugplansaisonTableViewer *polViewer = (FlugplansaisonTableViewer *)popInstance;
    if (ipDDXType == SEA_CHANGE || ipDDXType == SEA_NEW) polViewer->ProcessFlugplansaisonChange((SEADATA *)vpDataPointer);
    if (ipDDXType == SEA_DELETE) polViewer->ProcessFlugplansaisonDelete((SEADATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void FlugplansaisonTableViewer::ProcessFlugplansaisonChange(SEADATA *prpFlugplansaison)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpFlugplansaison->Seas;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpFlugplansaison->Beme;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpFlugplansaison->Vpfr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpFlugplansaison->Vpto.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);


	if (FindLine(prpFlugplansaison->Urno, ilItem))
	{
        FLUGPLANSAISONTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpFlugplansaison->Urno;
		prlLine->Beme = prpFlugplansaison->Beme;
		prlLine->Seas = prpFlugplansaison->Seas;
		prlLine->Vpfr = prpFlugplansaison->Vpfr.Format("%d.%m.%Y %H:%M");
		prlLine->Vpto = prpFlugplansaison->Vpto.Format("%d.%m.%Y %H:%M");

		pomFlugplansaisonTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomFlugplansaisonTable->DisplayTable();
	}
	else
	{
		MakeLine(prpFlugplansaison);
		if (FindLine(prpFlugplansaison->Urno, ilItem))
		{
	        FLUGPLANSAISONTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomFlugplansaisonTable->AddTextLine(olLine, (void *)prlLine);
				pomFlugplansaisonTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void FlugplansaisonTableViewer::ProcessFlugplansaisonDelete(SEADATA *prpFlugplansaison)
{
	int ilItem;
	if (FindLine(prpFlugplansaison->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomFlugplansaisonTable->DeleteTextLine(ilItem);
		pomFlugplansaisonTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool FlugplansaisonTableViewer::IsPassFilter(SEADATA *prpFlugplansaison)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int FlugplansaisonTableViewer::CompareFlugplansaison(FLUGPLANSAISONTABLE_LINEDATA *prpFlugplansaison1, FLUGPLANSAISONTABLE_LINEDATA *prpFlugplansaison2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpFlugplansaison1->Tifd == prpFlugplansaison2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpFlugplansaison1->Tifd > prpFlugplansaison2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void FlugplansaisonTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool FlugplansaisonTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void FlugplansaisonTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void FlugplansaisonTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING175);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool FlugplansaisonTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool FlugplansaisonTableViewer::PrintTableLine(FLUGPLANSAISONTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Seas;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Beme;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Vpfr;
				}
				break;
			case 3:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Vpto;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
