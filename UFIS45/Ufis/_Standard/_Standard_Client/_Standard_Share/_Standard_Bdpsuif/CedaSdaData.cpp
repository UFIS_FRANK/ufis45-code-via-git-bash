// CedaSdaData.cpp
 
#include <stdafx.h>
#include <CedaSdaData.h>
#include <resource.h>


void ProcessSdaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSdaData::CedaSdaData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SDADataStruct
	BEGIN_CEDARECINFO(SDADATA,SDADataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_LONG		(Upol,"UPOL")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
	END_CEDARECINFO //(SDADataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SDADataRecInfo)/sizeof(SDADataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SDADataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SDA");
	strcpy(pcmListOfFields,"URNO,SURN,UPOL,VPFR,VPTO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaSdaData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");
	ropFields.Add("SURN");
	ropFields.Add("UPOL");
	ropFields.Add("VPFR");
	ropFields.Add("VPTO");

	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING464));
	ropDesription.Add(LoadStg(IDS_STRING465));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSdaData::Register(void)
{
	ogDdx.Register((void *)this,BC_SDA_CHANGE,	CString("SDADATA"), CString("Sda-changed"),	ProcessSdaCf);
	ogDdx.Register((void *)this,BC_SDA_NEW,		CString("SDADATA"), CString("Sda-new"),		ProcessSdaCf);
	ogDdx.Register((void *)this,BC_SDA_DELETE,	CString("SDADATA"), CString("Sda-deleted"),	ProcessSdaCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSdaData::~CedaSdaData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSdaData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omStaffUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSdaData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	COleDateTime  olCurrTime = COleDateTime ::GetCurrentTime();
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omStaffUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SDADATA *prlSda = new SDADATA;
		if ((ilRc = GetFirstBufferRecord(prlSda)) == true)
		{
			omData.Add(prlSda);//Update omData
			omUrnoMap.SetAt((void *)prlSda->Urno,prlSda);
			bool blVptoIsOk = true;
			if(prlSda->Vpto.GetStatus() == COleDateTime::valid)
			{
				if(prlSda->Vpto < olCurrTime)
				{
					blVptoIsOk = false;
				}
			}

			if((prlSda->Vpfr < olCurrTime ) && blVptoIsOk)
			{
				omStaffUrnoMap.SetAt((void *)prlSda->Surn,prlSda);
			}

		}
		else
		{
			delete prlSda;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSdaData::Insert(SDADATA *prpSda)
{
	prpSda->IsChanged = DATA_NEW;
	if(Save(prpSda) == false) return false; //Update Database
	InsertInternal(prpSda);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSdaData::InsertInternal(SDADATA *prpSda)
{
	COleDateTime  olCurrTime = COleDateTime ::GetCurrentTime();
	ogDdx.DataChanged((void *)this, SDA_NEW,(void *)prpSda ); //Update Viewer
	omData.Add(prpSda);//Update omData
	omUrnoMap.SetAt((void *)prpSda->Urno,prpSda);
	bool blVptoIsOk = true;
	if(prpSda->Vpto.GetStatus() == COleDateTime::valid)
	{
		if(prpSda->Vpto < olCurrTime)
		{
			blVptoIsOk = false;
		}
	}

	if((prpSda->Vpfr < olCurrTime ) && blVptoIsOk)
	{
		omStaffUrnoMap.SetAt((void *)prpSda->Surn,prpSda);
	}

    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSdaData::Delete(long lpUrno)
{
	SDADATA *prlSda = GetSdaByUrno(lpUrno);
	if (prlSda != NULL)
	{
		prlSda->IsChanged = DATA_DELETED;
		if(Save(prlSda) == false) return false; //Update Database
		DeleteInternal(prlSda);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSdaData::DeleteInternal(SDADATA *prpSda)
{
	COleDateTime  olCurrTime = COleDateTime ::GetCurrentTime();

	bool blVptoIsOk = true;
	if(prpSda->Vpto.GetStatus() == COleDateTime::valid)
	{
		if(prpSda->Vpto < olCurrTime)
		{
			blVptoIsOk = false;
		}
	}

	if((prpSda->Vpfr < olCurrTime ) && blVptoIsOk)
	{
		omStaffUrnoMap.RemoveKey((void *)prpSda->Surn);
		int ilSdaCount = omData.GetSize();
		for (int ilLc = 0; ilLc < ilSdaCount; ilLc++)
		{
			if (omData[ilLc].Urno == prpSda->Urno)
			{
				continue;
			}
			else
			{
				bool blVptoIsOk = true;
				if (omData[ilLc].Vpto.GetStatus() == COleDateTime::valid)
				{
					if(omData[ilLc].Vpto < olCurrTime)
					{
						blVptoIsOk = false;
					}
				}

				if ((omData[ilLc].Vpfr < olCurrTime ) && blVptoIsOk)
				{
					omStaffUrnoMap.SetAt((void *)omData[ilLc].Surn,&omData[ilLc]);
				}

			}
		}
	}

	ogDdx.DataChanged((void *)this,SDA_DELETE,(void *)prpSda); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSda->Urno);

	int ilSdaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSdaCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSda->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSdaData::Update(SDADATA *prpSda)
{
	if (GetSdaByUrno(prpSda->Urno) != NULL)
	{
		if (prpSda->IsChanged == DATA_UNCHANGED)
		{
			prpSda->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSda) == false) return false; //Update Database
		UpdateInternal(prpSda);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSdaData::UpdateInternal(SDADATA *prpSda)
{
	COleDateTime  olCurrTime = COleDateTime ::GetCurrentTime();

	SDADATA *prlSda = GetSdaByUrno(prpSda->Urno);
	if (prlSda != NULL)
	{
		*prlSda = *prpSda; //Update omData

		bool blVptoIsOk = true;
		if(prlSda->Vpto.GetStatus() == COleDateTime::valid)
		{
			if(prlSda->Vpto < olCurrTime)
			{
				blVptoIsOk = false;
			}
		}

		if((prlSda->Vpfr < olCurrTime ) && blVptoIsOk)
		{
			omStaffUrnoMap.SetAt((void *)prlSda->Surn,prlSda);
		}


		ogDdx.DataChanged((void *)this,SDA_CHANGE,(void *)prlSda); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SDADATA *CedaSdaData::GetSdaByUrno(long lpUrno)
{
	SDADATA  *prlSda;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSda) == TRUE)
	{
		return prlSda;
	}
	return NULL;
}

//--GET-BY-SURN--------------------------------------------------------------------------------------------

SDADATA *CedaSdaData::GetValidSdaBySurn(long lpSurn)
{
	SDADATA  *prlSda;
	if (omStaffUrnoMap.Lookup((void *)lpSurn,(void *& )prlSda) == TRUE)
	{
		return prlSda;
	}
	return NULL;
}

//--GET-ALL BY-SURN--------------------------------------------------------------------------------------------

void CedaSdaData::GetAllSdaBySurn(long lpSurn,CCSPtrArray<SDADATA> &ropList)
{
	int ilSdaCount = omData.GetSize();
	ropList.RemoveAll();
	for (int ilLc = 0; ilLc < ilSdaCount; ilLc++)
	{
		if (omData[ilLc].Surn == lpSurn)
		{
			ropList.Add(&omData[ilLc]);
		}
	}
}


//--READSDACIALDATA-------------------------------------------------------------------------------------

bool CedaSdaData::ReadSpecialData(CCSPtrArray<SDADATA> *popSda,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","SDA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","SDA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSda != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SDADATA *prpSda = new SDADATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSda,CString(pclFieldList))) == true)
			{
				popSda->Add(prpSda);
			}
			else
			{
				delete prpSda;
			}
		}
		if(popSda->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSdaData::Save(SDADATA *prpSda)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSda->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSda->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSda);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSda->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSda->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSda);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSda->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSda->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSdaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSdaData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSdaData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSdaData;
	prlSdaData = (struct BcStruct *) vpDataPointer;
	SDADATA *prlSda;
	if(ipDDXType == BC_SDA_NEW)
	{
		prlSda = new SDADATA;
		GetRecordFromItemList(prlSda,prlSdaData->Fields,prlSdaData->Data);
		InsertInternal(prlSda);
	}
	if(ipDDXType == BC_SDA_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlSdaData->Selection);
		prlSda = GetSdaByUrno(llUrno);
		if(prlSda != NULL)
		{
			GetRecordFromItemList(prlSda,prlSdaData->Fields,prlSdaData->Data);
			UpdateInternal(prlSda);
		}
	}
	if(ipDDXType == BC_SDA_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlSdaData->Selection);

		prlSda = GetSdaByUrno(llUrno);
		if (prlSda != NULL)
		{
			DeleteInternal(prlSda);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
