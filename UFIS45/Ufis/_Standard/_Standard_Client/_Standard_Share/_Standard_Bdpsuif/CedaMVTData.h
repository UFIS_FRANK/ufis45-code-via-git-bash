// CedaMVTData.h

#ifndef __CEDAMVTDATA__
#define __CEDAMVTDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct MVTDATA 
{
	char 	 Alc3[5]; 	// Fluggesellschaft 3-Letter Code
	char 	 Apc3[5]; 	// Flughafen 3-Letter Code
	char 	 Beme[82]; 	// Bemerkung zu TEMA
	CTime	 Cdat;		// Erstellungsdatum
	CTime	 Lstu;		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Smvt[3]; 	// Sende MVT
	char 	 Tead[9]; 	// Telexadressen
	char 	 Tema[3]; 	// Manueller Eingriff n�tig
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr;		// G�ltig von
	CTime	 Vato;		// G�ltig bis
	char 	 Paye[5]; 	// Kostenstelle

	//DataCreated by this class
	int      IsChanged;

	MVTDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}


}; // end MVTDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaMVTData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<MVTDATA> omData;

	char pcmMVTFieldList[2048];

// Operations
public:
    CedaMVTData();
	~CedaMVTData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<MVTDATA> *popMvt,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool InsertMVT(MVTDATA *prpMVT,BOOL bpSendDdx = TRUE);
	bool InsertMVTInternal(MVTDATA *prpMVT);
	bool UpdateMVT(MVTDATA *prpMVT,BOOL bpSendDdx = TRUE);
	bool UpdateMVTInternal(MVTDATA *prpMVT);
	bool DeleteMVT(long lpUrno);
	bool DeleteMVTInternal(MVTDATA *prpMVT);
	MVTDATA  *GetMVTByUrno(long lpUrno);
	bool SaveMVT(MVTDATA *prpMVT);
	void ProcessMVTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795

	// Private methods
private:
    void PrepareMVTData(MVTDATA *prpMVTData);
	CString omWhere; //Prf: 8795
	bool ValidateMVTBcData(const long& lrpUnro); //Prf: 8795
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAMVTDATA__
