#ifndef __PDATABLEVIEWER_H__
#define __PDATABLEVIEWER_H__

#include <stdafx.h>
#include <CedaPDADATA.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct PDA_LINEDATA
{
	long	Urno;
	CString	Pdid;
	CString	Ipad;
	CString	Vafr;
	CString	Vato;
};

/////////////////////////////////////////////////////////////////////////////
// AadTableViewer

class PdaTableViewer : public CViewer
{
// Constructions
public:
    PdaTableViewer(CCSPtrArray<PDADATA> *popData);
    ~PdaTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(PDADATA *prpAad);
	int ComparePda(PDA_LINEDATA *prpAad1, PDA_LINEDATA *prpAad2);
    void MakeLines();
	void MakeLine(PDADATA *prpAad);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(PDA_LINEDATA *prpAad);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(PDA_LINEDATA *prpLine);
	void ProcessPdaChange(PDADATA *prpAad);
	void ProcessPdaDelete(PDADATA *prpAad);
	bool FindPda(char *prpAadKeya, char *prpAadKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomPdaTable;
	CCSPtrArray<PDADATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<PDA_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(PDA_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__PDA_TABLEVIEWER_H__
