// CedaMaaData.h

#ifndef __CEDAPMAADATA__
#define __CEDAPMAADATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------

struct MAADATA 
{
	char     Hopo[3+2];  // Hopo
	long	 Mawu; // Reference MAWTAB.URNO
    long 	 Urno; // Eindeutige Datensatz-Nr.
	char 	 Valu[5+2];  // Wert
	char	 Week[2+2];  // Woche
	char     Year[4+2];	 // Jahr	
	
	//DataCreated by this class
	int      IsChanged;
	
	MAADATA(void)
	{
		memset(this,'\0',sizeof(*this));
	}

}; 

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaMaaData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<MAADATA> omData;

	char pcmListOfFields[2048];

public:
    CedaMaaData();
	~CedaMaaData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(MAADATA *prpMaa);
	bool InsertInternal(MAADATA *prpMaa);
	bool Update(MAADATA *prpMaa);
	bool UpdateInternal(MAADATA *prpMaa);
	bool Delete(long lpUrno);
	bool DeleteInternal(MAADATA *prpMaa);
	bool ReadSpecialData(CCSPtrArray<MAADATA> *popMaa,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(MAADATA *prpMaa);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	MAADATA  *GetMaaByUrno(long lpUrno);


	// Private methods
private:
    void PrepareMaaData(MAADATA *prpMaaData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPMAADATA__
