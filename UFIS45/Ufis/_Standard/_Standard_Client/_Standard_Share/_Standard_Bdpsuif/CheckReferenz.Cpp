// CheckReferenz.cpp
 
#include <stdafx.h>
#include <resource.h>
#include <CheckReferenz.h>
#include <CCSGlobl.h>

#include <RecordSet.h>
#include <CedaBasicData.h>
#include <CedaBsdData.h>
#include <CedaCohData.h>
#include <CedaCotData.h>
#include <CedaGrmData.h>
#include <CedaMfmData.h>
#include <CedaOacData.h>
#include <CedaOdaData.h>
#include <CedaPacData.h>
#include <CedaPfcData.h>
#include <CedaWgpData.h>
#include <CedaWisData.h>

//-------------------------------------------------------------------------------------


CheckReferenz::CheckReferenz()
{

}

//-------------------------------------------------------------------------------------

CheckReferenz::~CheckReferenz()
{

}

//-------------------------------------------------------------------------------------

int CheckReferenz::Check(int ipTableID, long lpUrno, CString opCode)
{
	AfxGetApp()->DoWaitCursor(1);
	int ilNoOfEntrys = 0;
	int ilCount = 0;

	CString olWhere;
	CString olTmpWhere;
	CString olObject;
	CString olFields = "URNO";
	CCSPtrArray<RecordSet> olBuffer;
	switch (ipTableID)
	{
		case _BSD:
		{
			olObject = "DSR";
			olWhere.Format("WHERE SFCA='%s' OR SFCP='%s' OR SFCS='%s'", opCode, opCode, opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "GSP";
			olWhere.Format("WHERE BUMO='%ld' OR BUTU='%ld' OR BUWE='%ld' OR BUTH='%ld' OR BUFR='%ld' OR BUSA='%ld' OR BUSU='%ld'", lpUrno, lpUrno, lpUrno, lpUrno, lpUrno, lpUrno, lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "GSP";
			olWhere.Format("WHERE SUMO='%ld' OR SUTU='%ld' OR SUWE='%ld' OR SUTH='%ld' OR SUFR='%ld' OR SUSA='%ld' OR SUSU='%ld'", lpUrno, lpUrno, lpUrno, lpUrno, lpUrno, lpUrno, lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DRR";
			olWhere.Format("WHERE SCOD ='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "SDT";
			olWhere.Format("WHERE BSDC='%s' OR BSDU='%ld'", opCode, lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "MSD";
			olWhere.Format("WHERE BSDC='%s' OR BSDU='%ld'", opCode, lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DEL";
			olWhere.Format("WHERE BSDU='%ld'", lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "GRM";
			olWhere.Format("WHERE VALU='%ld'", lpUrno);
			CCSPtrArray<GRMDATA> olGrmBuffer;
			if (ogGrmData.ReadSpecialData(&olGrmBuffer, olWhere.GetBuffer(0),ogGrmData.pcmListOfFields,false))
				ilCount = olGrmBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olGrmBuffer.DeleteAll();

			break;
		}
		case _ODA:
		{
			olObject = "DEL";
			olWhere.Format("WHERE SDAC='%s'", opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DRA";
			olWhere.Format("WHERE SDAC ='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "OAC";
			olWhere.Format("WHERE SDAC ='%s'",opCode);
			CCSPtrArray<OACDATA> olOacBuffer;
			if (ogOacData.ReadSpecialData(&olOacBuffer, olWhere.GetBuffer(0),ogOacData.pcmListOfFields,false))
				ilCount = olOacBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olOacBuffer.DeleteAll();

			olObject = "DRR";
			olWhere.Format("WHERE SCOD='%s' OR BSDU='%ld'", opCode, lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "GSP";
			olWhere.Format("WHERE BUMO='%ld' OR BUTU='%ld' OR BUWE='%ld' OR BUTH='%ld' OR BUFR='%ld' OR BUSA='%ld' OR BUSU='%ld'", lpUrno, lpUrno, lpUrno, lpUrno, lpUrno, lpUrno, lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "GSP";
			olWhere.Format("WHERE SUMO='%ld' OR SUTU='%ld' OR SUWE='%ld' OR SUTH='%ld' OR SUFR='%ld' OR SUSA='%ld' OR SUSU='%ld'", lpUrno, lpUrno, lpUrno, lpUrno, lpUrno, lpUrno, lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			break;
		}
		case _STF:
		{
			olObject = "GSP";
			olWhere.Format("WHERE STFU='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "ACC";
			olWhere.Format("WHERE PENO='%s' OR STFU='%ld'", opCode, lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DRD";
			olWhere.Format("WHERE STFU='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DRS";
			olWhere.Format("WHERE STFU='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DRG";
			olWhere.Format("WHERE STFU='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DRW";
			olWhere.Format("WHERE STFU='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "REL";
			olWhere.Format("WHERE STFU='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "JOB";
			olWhere.Format("WHERE USTF='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DRA";
			olWhere.Format("WHERE STFU='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DRR";
			olWhere.Format("WHERE STFU='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			
			break;
		}
		case _PFC:
		{
			olObject = "DLG";
			olWhere.Format("WHERE FCTC='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "SEF";
			olWhere.Format("WHERE FCCO='%s' OR UPFC='%ld'",opCode,lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DRG";
			olWhere.Format("WHERE FCTC='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "MSD";
			olWhere.Format("WHERE FCTC='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "BSD";
			olWhere.Format("WHERE FCTC='%s'",opCode);
			CCSPtrArray<BSDDATA> olBsdBuffer;
			if (ogBsdData.ReadSpecialData(&olBsdBuffer, olWhere.GetBuffer(0),ogBsdData.pcmListOfFields,false))
				ilCount = olBsdBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBsdBuffer.DeleteAll();

			olObject = "DRR";
			olWhere.Format("WHERE FCTC='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "RPF";
			olWhere.Format("WHERE FCCO='%s' OR UPFC='%ld'",opCode,lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "SDT";
			olWhere.Format("WHERE FCTC='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "SPF";
			olWhere.Format("WHERE CODE='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DEL";
			olWhere.Format("WHERE FCTC='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DRD";
			olWhere.Format("WHERE FCTC='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "GSP";
			olWhere.Format("WHERE P1MO='%ld' OR P1TU='%ld' OR P1WE='%ld' OR P1TH='%ld' OR P1FR='%ld' OR P1SA='%ld' OR P1SU='%ld'", lpUrno, lpUrno, lpUrno, lpUrno, lpUrno, lpUrno, lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "GSP";
			olWhere.Format("WHERE P2MO='%ld' OR P2TU='%ld' OR P2WE='%ld' OR P2TH='%ld' OR P2FR='%ld' OR P2SA='%ld' OR P2SU='%ld'", lpUrno, lpUrno, lpUrno, lpUrno, lpUrno, lpUrno, lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();


			break;
		}
		case _PER:
		{
			olObject = "RPQ";
			olWhere.Format("WHERE QUCO='%s' OR UPER='%ld'",opCode,lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "SPE";
			olWhere.Format("WHERE CODE='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "SEQ";
			olWhere.Format("WHERE UPER='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "EQU";
			olWhere.Format("WHERE CRQU='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DRD";
			olWhere.Format("WHERE PRMC LIKE'%%%s%%'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();


			break;
		}
		case _ORG:
		{
			olObject = "WIS";
			olWhere.Format("WHERE ORGC='%s'",opCode);
			CCSPtrArray<WISDATA> olWisBuffer;
			if (ogWisData.ReadSpecialData(&olWisBuffer, olWhere.GetBuffer(0),ogWisData.pcmListOfFields,false))
				ilCount = olWisBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olWisBuffer.DeleteAll();

			olObject = "SOR";
			olWhere.Format("WHERE CODE='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "PFC";
			olWhere.Format("WHERE DPTC='%s'",opCode);
			CCSPtrArray<PFCDATA> olPfcBuffer;
			if (ogPfcData.ReadSpecialData(&olPfcBuffer, olWhere.GetBuffer(0),ogPfcData.pcmListOfFields,false))
				ilCount = olPfcBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olPfcBuffer.DeleteAll();

			olObject = "MFM";
			olWhere.Format("WHERE DPT1='%s'",opCode);
			CCSPtrArray<MFMDATA> olMfmBuffer;
			if (ogMfmData.ReadSpecialData(&olMfmBuffer, olWhere.GetBuffer(0),ogMfmData.pcmListOfFields,false))
				ilCount = olMfmBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olMfmBuffer.DeleteAll();

			olObject = "MAW";
			olWhere.Format("WHERE CODE='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "ODA";
			olWhere.Format("WHERE DPTC='%s'",opCode);
			CCSPtrArray<ODADATA> olOdaBuffer;
			if (ogOdaData.ReadSpecialData(&olOdaBuffer, olWhere.GetBuffer(0),ogOdaData.pcmListOfFields,false))
				ilCount = olOdaBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olOdaBuffer.DeleteAll();

			olObject = "DEL";
			olWhere.Format("WHERE DPT1='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DRD";
			olWhere.Format("WHERE DPT1='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "BSD";
			olWhere.Format("WHERE DPT1='%s'",opCode);
			CCSPtrArray<BSDDATA> olBsdBuffer;
			if (ogBsdData.ReadSpecialData(&olBsdBuffer, olWhere.GetBuffer(0),ogBsdData.pcmListOfFields,false))
				ilCount = olBsdBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBsdBuffer.DeleteAll();

			olObject = "COT";
			olWhere.Format("WHERE DPTC='%s'",opCode);
			CCSPtrArray<COTDATA> olCotBuffer;
			if (ogCotData.ReadSpecialData(&olCotBuffer, olWhere.GetBuffer(0),ogCotData.pcmListOfFields,false))
				ilCount = olCotBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olCotBuffer.DeleteAll();

			olObject = "GPL";
			olWhere.Format("WHERE ORGU='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();


			break;
		}
		case _COT:
		{
			olObject = "PAC";
			olWhere.Format("WHERE CTRC='%s'",opCode);
			CCSPtrArray<PACDATA> olPacBuffer;
			if (ogPacData.ReadSpecialData(&olPacBuffer, olWhere.GetBuffer(0),ogPacData.pcmListOfFields,false))
				ilCount = olPacBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olPacBuffer.DeleteAll();

			olObject = "ODA";
			olWhere.Format("WHERE CTRC='%s'",opCode);
			CCSPtrArray<ODADATA> olOdaBuffer;
			if (ogOdaData.ReadSpecialData(&olOdaBuffer, olWhere.GetBuffer(0),ogOdaData.pcmListOfFields,false))
				ilCount = olOdaBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olOdaBuffer.DeleteAll();

			olObject = "OAC";
			olWhere.Format("WHERE CTRC='%s'",opCode);
			CCSPtrArray<OACDATA> olOacBuffer;
			if (ogOacData.ReadSpecialData(&olOacBuffer, olWhere.GetBuffer(0),ogOacData.pcmListOfFields,false))
				ilCount = olOacBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olOacBuffer.DeleteAll();

			olObject = "SCO";
			olWhere.Format("WHERE CODE='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "COH";
			olWhere.Format("WHERE CTRC='%s'",opCode);
			CCSPtrArray<COHDATA> olCohBuffer;
			if (ogCohData.ReadSpecialData(&olCohBuffer, olWhere.GetBuffer(0),ogCohData.pcmListOfFields,false))
				ilCount = olCohBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olCohBuffer.DeleteAll();

			olObject = "ACI";
			olWhere.Format("WHERE CTRC='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "GPL";
			olWhere.Format("WHERE COTU='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			break;
		}
		case _POL:
		{
			olObject = "SDA";
			olWhere.Format("WHERE UPOL='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "POA";
			olWhere.Format("WHERE UPOL='%ld'",lpUrno);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			break;
		}
		case _WGP:
		{
			olObject = "DLG";
			olWhere.Format("WHERE WGPC='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "DRG";
			olWhere.Format("WHERE WGPC='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			olObject = "SWG";
			olWhere.Format("WHERE CODE='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			break;
		}
		case _WIS:
		{
			olObject = "DRW";
			olWhere.Format("WHERE WISC='%s'",opCode);
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olBuffer.DeleteAll();

			break;
		}
		case _PGP:
		{
			olObject = "WGP";
			olWhere.Format("WHERE PGPU='%ld'",lpUrno);
			CCSPtrArray<WGPDATA> olWgpBuffer;
			if (ogWgpData.ReadSpecialData(&olWgpBuffer, olWhere.GetBuffer(0),ogWgpData.pcmListOfFields,false))
				ilCount = olWgpBuffer.GetSize();
			else
				ilCount = 0;
			if(ilCount>0) ilNoOfEntrys += ilCount;
			olWgpBuffer.DeleteAll();

			break;
		}
		default:
		{
			ilNoOfEntrys = -1;
		}
	}
	AfxGetApp()->DoWaitCursor(-1);
	return ilNoOfEntrys;
}

//-------------------------------------------------------------------------------------
