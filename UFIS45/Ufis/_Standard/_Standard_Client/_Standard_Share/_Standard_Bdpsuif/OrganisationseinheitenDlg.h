#if !defined(AFX_ORGANISATIONSEINHEITENDLG_H__780A8AF1_64B4_11D1_B3D1_0000C016B067__INCLUDED_)
#define AFX_ORGANISATIONSEINHEITENDLG_H__780A8AF1_64B4_11D1_B3D1_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// OrganisationseinheitenDlg.h : header file
//
#include <CedaOrgData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// OrganisationseinheitenDlg dialog

class OrganisationseinheitenDlg : public CDialog
{
// Construction
public:
	OrganisationseinheitenDlg(ORGDATA *popOrg,CWnd* pParent = NULL);   // standard constructor

	CStatic	*pomStatus;
// Dialog Data
	//{{AFX_DATA(OrganisationseinheitenDlg)
	enum { IDD = IDD_ORGANISATIONSEINHEITENDLG };
	CComboBox	m_CB_ODG;
	CButton	m_OK;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_DPT1;
	CCSEdit	m_DPT2;
	CCSEdit	m_DPTN;
	CCSEdit	m_REMA;
	CCSEdit	m_GRUP;
	CCSEdit	m_ODGL;
	CCSEdit	m_ODKL;
	CCSEdit	m_ODSL;
	CString	m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(OrganisationseinheitenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(OrganisationseinheitenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDLG();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	ORGDATA *pomOrg;
	CString	omOldCode;
	bool	bmChangeAction;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ORGANISATIONSEINHEITENDLG_H__780A8AF1_64B4_11D1_B3D1_0000C016B067__INCLUDED_)
