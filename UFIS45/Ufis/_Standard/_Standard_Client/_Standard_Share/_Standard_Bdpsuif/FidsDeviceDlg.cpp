// FidsDeviceDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <FidsDeviceDlg.h>
#include <PrivList.h>
#include <CedaDSPData.h>
#include <CedaAltData.h>
#include <AwDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FidsDeviceDlg dialog


FidsDeviceDlg::FidsDeviceDlg(DEVDATA *popDEV,const CString &ropCaption,CWnd* pParent /*=NULL*/)
	: CDialog(FidsDeviceDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(FidsDeviceDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomDEV = popDEV;
	m_Caption = ropCaption;
}


void FidsDeviceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FidsDeviceDlg)
	DDX_Control(pDX, IDC_EDIT_LAST_REPLY, m_DLRD);
	DDX_Control(pDX, IDC_EDIT_LAST_DISCONNECTED, m_DLDC);
	DDX_Control(pDX, IDC_EDIT_LAST_DEGAUSS, m_DLDT);
	DDX_Control(pDX, IDC_EDIT_LAST_CONNECTED, m_DLCT);
	DDX_Control(pDX, IDC_EDIT_ACTUAL_DEVICE_NO, m_DRNA);
	DDX_Control(pDX, IDC_EDIT_REMARK, m_REMA);
	DDX_Control(pDX, IDC_EDIT_LOCATION, m_LOCA);
	DDX_Control(pDX, IDC_EDIT_INVENTORY_NO, m_DIVN);
	DDX_Control(pDX, IDC_EDIT_DEGAUSS_INTERVAL, m_DDGI);
	DDX_Control(pDX, IDC_EDIT_TERMINAL, m_DTER);
	DDX_Control(pDX, IDC_EDIT_FILTER_FIELD, m_DFFD);
	DDX_Control(pDX, IDC_EDIT_FILTER_CONTENT, m_DFCO);
	DDX_Control(pDX, IDC_EDIT_DEVICE_NO_CONFIG, m_DRNP);
	DDX_Control(pDX, IDC_EDIT_DEVICE_AREA, m_DARE);
	DDX_Control(pDX, IDC_EDIT_ALARAM_ZONE, m_DALZ);
	DDX_Control(pDX, IDC_EDIT_AIRLINE_CODE, m_ALGC);
	DDX_Control(pDX, IDC_EDIT_ADVERTISEMENT, m_ADID);
	DDX_Control(pDX, IDC_EDIT_STATUS, m_STAT);
	DDX_Control(pDX, IDC_EDIT_DISPLAY, m_DPID);
	DDX_Control(pDX, IDC_EDIT_PORT, m_DPRT);
	DDX_Control(pDX, IDC_EDIT_NAME, m_DEVN);
	DDX_Control(pDX, IDC_EDIT_GROUP, m_GRPN);
	DDX_Control(pDX, IDC_EDIT_ADDRESS, m_DADR);
	DDX_Control(pDX, IDC_VAFR_D2, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T1, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D1, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T1, m_VATOT);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_CHECK_ASYNCH_CAROUSEL, m_DRGI);
	DDX_Control(pDX, IDC_EDIT_BRIGHTNESS, m_DBRF);
	DDX_Control(pDX, IDC_EDIT_CONTRAST, m_DCOF);
	DDX_Control(pDX, IDCANCEL, m_CANCEL);
	DDX_Control(pDX, IDOK, m_OK);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FidsDeviceDlg, CDialog)
	//{{AFX_MSG_MAP(FidsDeviceDlg)
	ON_BN_CLICKED(IDC_BUTTON_DISPLAY, OnSelectDisplay)
	ON_BN_CLICKED(IDC_BUTTON_AIRLINE, OnSelectAlt3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FidsDeviceDlg message handlers

BOOL FidsDeviceDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_Caption = LoadStg(IDS_STRING929) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("DEVICEDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_DEVN.SetTypeToString("X(20)",20,1);
	m_DEVN.SetTextLimit(1,20,false);
	m_DEVN.SetTextErrColor(RED);
	m_DEVN.SetBKColor(YELLOW);
	m_DEVN.SetInitText(pomDEV->Devn);
	m_DEVN.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_DEVN"));
	//------------------------------------
	m_GRPN.SetTypeToString("X(12)",12,1);
	m_GRPN.SetTextLimit(1,12,false);
	m_GRPN.SetTextErrColor(RED);
	m_GRPN.SetBKColor(YELLOW);
	m_GRPN.SetInitText(pomDEV->Grpn);
	m_GRPN.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_GRPN"));
	//------------------------------------
	m_DADR.SetRegularExpression("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");
	m_DADR.SetTextErrColor(RED);
	m_DADR.SetBKColor(YELLOW);
	m_DADR.SetInitText(pomDEV->Dadr);
	m_DADR.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_DADR"));
	//------------------------------------
	m_DPRT.SetRegularExpression("[0-9]{1,4}");
	m_DPRT.SetTextErrColor(RED);
	m_DPRT.SetInitText(pomDEV->Dprt);
	m_DPRT.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_DPRT"));
	//------------------------------------
	m_STAT.SetFormat("X(1)");
	m_STAT.SetTextErrColor(RED);
	m_STAT.SetBKColor(YELLOW);
	m_STAT.SetInitText(pomDEV->Stat);
	m_STAT.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_STAT"));
	//------------------------------------
	m_DPID.SetFormat("X(12)");
	m_DPID.SetTextLimit(1,12,false);
	m_DPID.SetTextErrColor(RED);
	m_DPID.SetBKColor(YELLOW);
	m_DPID.SetInitText(pomDEV->Dpid);
	m_DPID.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_DPID"));

	//------------------------------------
	m_ADID.SetFormat("X(12)");
	m_ADID.SetTextLimit(-1,12,true);
	m_ADID.SetTextErrColor(RED);
	m_ADID.SetInitText(pomDEV->Adid);
	m_ADID.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_ADID"));

	//------------------------------------
	m_DRNP.SetFormat("#(10)");
	m_DRNP.SetTextLimit(-1,10,true);
	m_DRNP.SetTextErrColor(RED);
	m_DRNP.SetInitText(pomDEV->Drnp);
	m_DRNP.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_DRNP"));

	//------------------------------------
	m_DFFD.SetFormat("X(8)");
	m_DFFD.SetTextLimit(-1,8,true);
	m_DFFD.SetTextErrColor(RED);
	m_DFFD.SetInitText(pomDEV->Dffd);
	m_DFFD.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_DFFD"));

	//------------------------------------
	m_DFCO.SetFormat("X(128)");
	m_DFCO.SetTextLimit(-1,128,true);
	m_DFCO.SetTextErrColor(RED);
	m_DFCO.SetInitText(pomDEV->Dfco);
	m_DFCO.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_DFCO"));

	//------------------------------------
	m_DTER.SetFormat("X(5)");
	m_DTER.SetTextLimit(-1,5,true);
	m_DTER.SetTextErrColor(RED);
	m_DTER.SetInitText(pomDEV->Dter);
	m_DTER.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_DTER"));

	//------------------------------------
	m_DARE.SetFormat("X(16)");
	m_DARE.SetTextLimit(-1,16,true);
	m_DARE.SetTextErrColor(RED);
	m_DARE.SetInitText(pomDEV->Dare);
	m_DARE.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_DARE"));

	//------------------------------------

	int ilLenght = ogDEVData.GetALGCLength();


	m_ALGC.SetFormat("X(ilLenght)");
	m_ALGC.SetTextLimit(-1,ilLenght,true);
	m_ALGC.SetTextErrColor(RED);
	m_ALGC.SetInitText(pomDEV->Algc);
	m_ALGC.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_ALGC"));

	//------------------------------------
	m_DALZ.SetFormat("X(5)");
	m_DALZ.SetTextLimit(-1,5,true);
	m_DALZ.SetTextErrColor(RED);
	m_DALZ.SetInitText(pomDEV->Dalz);
	m_DALZ.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_DALZ"));

	//------------------------------------
	m_DDGI.SetFormat("#(4)");
	m_DDGI.SetTextLimit(-1,4,true);
	m_DDGI.SetTextErrColor(RED);
	m_DDGI.SetInitText(pomDEV->Ddgi);
	m_DDGI.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_DDGI"));

	//------------------------------------
	m_LOCA.SetFormat("X(128)");
	m_LOCA.SetTextLimit(-1,128,true);
	m_LOCA.SetTextErrColor(RED);
	m_LOCA.SetInitText(pomDEV->Loca);
	m_LOCA.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_LOCA"));

	//------------------------------------
	m_REMA.SetFormat("X(128)");
	m_REMA.SetTextLimit(-1,128,true);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomDEV->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_REMA"));

	//------------------------------------
	m_DIVN.SetFormat("X(20)");
	m_DIVN.SetTextLimit(-1,20,true);
	m_DIVN.SetTextErrColor(RED);
	m_DIVN.SetInitText(pomDEV->Divn);
	m_DIVN.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_DIVN"));

	//------------------------------------
	m_DRNA.SetFormat("#(10)");
	m_DRNA.SetTextLimit(-1,10,true);
	m_DRNA.SetBKColor(SILVER);
	m_DRNA.SetTextErrColor(RED);
	m_DRNA.SetInitText(pomDEV->Drna);

	//------------------------------------
	m_DLCT.SetFormat("X(14)");
	m_DLCT.SetTextLimit(-1,14,true);
	m_DLCT.SetBKColor(SILVER);
	m_DLCT.SetTextErrColor(RED);
	m_DLCT.SetInitText(pomDEV->Dlct.Format("%d.%m.%Y %H:%M"));

	//------------------------------------
	m_DLDC.SetFormat("X(14)");
	m_DLDC.SetTextLimit(-1,14,true);
	m_DLDC.SetBKColor(SILVER);
	m_DLDC.SetTextErrColor(RED);
	m_DLDC.SetInitText(pomDEV->Dldc.Format("%d.%m.%Y %H:%M"));

	//------------------------------------
	m_DLRD.SetFormat("X(128)");
	m_DLRD.SetTextLimit(-1,128,true);
	m_DLRD.SetBKColor(SILVER);
	m_DLRD.SetTextErrColor(RED);
	m_DLRD.SetInitText(pomDEV->Dlrd);

	//------------------------------------
	m_DLDT.SetFormat("X(14)");
	m_DLDT.SetTextLimit(-1,14,true);
	m_DLDT.SetBKColor(SILVER);
	m_DLDT.SetTextErrColor(RED);
	m_DLDT.SetInitText(pomDEV->Dldt.Format("%d.%m.%Y %H:%M"));

	//------------------------------------
	m_DRGI.SetCheck(0);
	if (pomDEV->Drgi[0] == '1')
	{
		m_DRGI.SetCheck(1);
	}
	//------------------------------------
	m_DBRF.SetFormat("#(3)");
	m_DBRF.SetTextLimit(-1,3,true);
	m_DBRF.SetTextErrColor(RED);
	m_DBRF.SetInitText(pomDEV->Dbrf);
	//------------------------------------
	m_DCOF.SetFormat("#(3)");
	m_DCOF.SetTextLimit(-1,3,true);
	m_DCOF.SetTextErrColor(RED);
	m_DCOF.SetInitText(pomDEV->Dcof);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);			
	m_CDATD.SetInitText(pomDEV->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomDEV->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomDEV->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomDEV->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomDEV->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomDEV->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_USEU"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomDEV->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomDEV->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomDEV->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomDEV->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_VATO"));
	//------------------------------------
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FidsDeviceDlg::OnOK() 
{
	// TODO: Add extra validation here
	CWaitCursor olWait;
	CString olErrorText;
	bool ilStatus = true;

	if(m_DEVN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DEVN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING935) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING935) + ogNotFormat;
		}
	}

	if(m_GRPN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_GRPN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING936) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING936) + ogNotFormat;
		}
	}


	CString olDadr;
	m_DADR.GetWindowText(olDadr);

	
	CStringArray olStrArray;
	ExtractItemList(olDadr,&olStrArray,'.');

	if (olStrArray.GetSize() != 4)
	{
		olErrorText += LoadStg(IDS_STRING937) + ogNotFormat;
		ilStatus = false;
	}
	else
	{
		CString olText;
		olText.Format("%d.%d.%d.%d", atoi(olStrArray[0]),atoi(olStrArray[1]),atoi(olStrArray[2]),atoi(olStrArray[3]));
		m_DADR.SetWindowText(olText);
	}		



	if(m_DADR.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DADR.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING937) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING937) + ogNotFormat;
		}
	}

	if(m_DPRT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING938) + ogNotFormat;
	}

	if(m_STAT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING939) + ogNotFormat;
	}

	if(m_DPID.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DPID.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING940) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING940) + ogNotFormat;
		} 
	}

	if(m_ADID.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING941) + ogNotFormat;
	}

	if(m_DRNP.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING942) + ogNotFormat;
	}

	if(m_DFFD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING943) + ogNotFormat;
	}

	if(m_DFCO.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING944) + ogNotFormat;
	}

	if(m_DTER.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING945) + ogNotFormat;
	}

	if(m_DARE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING946) + ogNotFormat;
	}

	if(m_ALGC.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING947) + ogNotFormat;
	}

	if(m_DALZ.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING948) + ogNotFormat;
	}

	if(m_DDGI.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING949) + ogNotFormat;
	}

	if(m_LOCA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING950) + ogNotFormat;
	}

	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING951) + ogNotFormat;
	}

	if(m_DIVN.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING952) + ogNotFormat;
	}

	if(m_DBRF.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING1016) + ogNotFormat;
	}

	if(m_DCOF.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING1017) + ogNotFormat;
	}


	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	/////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olDadr;
		CString olGrpn;
		CString olRegEx;


		char clWhere[100];
		CCSPtrArray<DEVDATA> olDEVCPA;
		
		m_DADR.GetWindowText(olDadr);
		m_GRPN.GetWindowText(olGrpn);	

		
		CStringArray olStrArray;
		ExtractItemList(olDadr,&olStrArray,'.');
		
		 olGrpn.MakeLower();

		if (olGrpn == "webfids" && olStrArray.GetSize() == 4)
		{

			for(int i = 0; i < 4; i++)
			{
				olRegEx += "0{0,2}" + olStrArray[i] + "";

				if(i < 3)
					olRegEx += "\\.";

			}

			sprintf(clWhere,"WHERE regexp_like (dadr,'%s') and lower(grpn) = 'webfids'",olRegEx);

			if(ogDEVData.ReadSpecialData(&olDEVCPA,clWhere,"URNO,DEVN",false) == true)
			{
				if(olDEVCPA[0].Urno != pomDEV->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING32818);
				}
			}
			olDEVCPA.DeleteAll();
		}
	}




	if(ilStatus == true)
	{
		CString olDevn;
		char clWhere[100];
		CCSPtrArray<DEVDATA> olDEVCPA;
		m_DEVN.GetWindowText(olDevn);
		if (olDevn.GetLength() > 0)
		{
			sprintf(clWhere,"WHERE DEVN='%s'",olDevn);
			if(ogDEVData.ReadSpecialData(&olDEVCPA,clWhere,"URNO,DEVN",false) == true)
			{
				if(olDEVCPA[0].Urno != pomDEV->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
				}
			}
			olDEVCPA.DeleteAll();
		}
	}






	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_DEVN.GetWindowText(pomDEV->Devn,sizeof(pomDEV->Devn));
		m_GRPN.GetWindowText(pomDEV->Grpn,sizeof(pomDEV->Grpn));
		m_DADR.GetWindowText(pomDEV->Dadr,sizeof(pomDEV->Dadr));
		m_DPRT.GetWindowText(pomDEV->Dprt,sizeof(pomDEV->Dprt));
		m_STAT.GetWindowText(pomDEV->Stat,sizeof(pomDEV->Stat));
		m_DPID.GetWindowText(pomDEV->Dpid,sizeof(pomDEV->Dpid));
		m_ADID.GetWindowText(pomDEV->Adid,sizeof(pomDEV->Adid));
		m_DRNP.GetWindowText(pomDEV->Drnp,sizeof(pomDEV->Drnp));
		m_DFFD.GetWindowText(pomDEV->Dffd,sizeof(pomDEV->Dffd));
		m_DFCO.GetWindowText(pomDEV->Dfco,sizeof(pomDEV->Dfco));
		m_DTER.GetWindowText(pomDEV->Dter,sizeof(pomDEV->Dter));
		m_DARE.GetWindowText(pomDEV->Dare,sizeof(pomDEV->Dare));
		m_ALGC.GetWindowText(pomDEV->Algc,sizeof(pomDEV->Algc));
		m_DALZ.GetWindowText(pomDEV->Dalz,sizeof(pomDEV->Dalz));
		m_DDGI.GetWindowText(pomDEV->Ddgi,sizeof(pomDEV->Ddgi));
		m_LOCA.GetWindowText(pomDEV->Loca,sizeof(pomDEV->Loca));
		m_REMA.GetWindowText(pomDEV->Rema,sizeof(pomDEV->Rema));
		m_DIVN.GetWindowText(pomDEV->Divn,sizeof(pomDEV->Divn));
		if (m_DRGI.GetCheck() == 1)
			strcpy(pomDEV->Drgi,"1");
		else
			strcpy(pomDEV->Drgi," ");

		m_DBRF.GetWindowText(pomDEV->Dbrf,sizeof(pomDEV->Dbrf));
		m_DCOF.GetWindowText(pomDEV->Dcof,sizeof(pomDEV->Dcof));

		pomDEV->Vafr = DateHourStringToDate(olVafrd,olVafrt); 
		pomDEV->Vato = DateHourStringToDate(olVatod,olVatot);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_DEVN.SetFocus();
		m_DEVN.SetSel(0,-1);

	}
}

void FidsDeviceDlg::OnSelectDisplay() 
{
	CStringArray olCol1, olCol2,olCol3;
	CCSPtrArray<DSPDATA> olList;

	AfxGetApp()->DoWaitCursor(1);
	if(ogDSPData.ReadSpecialData(&olList, "", "URNO,DPID", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol2.Add(CString(olList[i].Dpid));
		}

		AwDlg *pomDlg = new AwDlg(NULL,&olCol2,&olCol3,  LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_DPID.SetWindowText(pomDlg->omReturnString);
			m_DPID.SetFocus();
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

void FidsDeviceDlg::OnSelectAlt3() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<ALTDATA> olList;

	AfxGetApp()->DoWaitCursor(1);
	if(ogALTData.ReadSpecialData(&olList, "", "URNO,ALC3,ALFN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Alc3));
			olCol2.Add(CString(olList[i].Alfn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_ALGC.SetWindowText(pomDlg->omReturnString);
			m_ALGC.SetFocus();
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
	
}
