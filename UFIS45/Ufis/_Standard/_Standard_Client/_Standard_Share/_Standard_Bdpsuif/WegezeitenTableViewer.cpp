// WegezeitenTableViewer.cpp 
//

#include <stdafx.h>
#include <WegezeitenTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void WegezeitenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// WegezeitenTableViewer
//

WegezeitenTableViewer::WegezeitenTableViewer(CCSPtrArray<WAYDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomWegezeitenTable = NULL;
    ogDdx.Register(this, WAY_CHANGE, CString("WEGEZEITENTABLEVIEWER"), CString("Wegezeiten Update"), WegezeitenTableCf);
    ogDdx.Register(this, WAY_NEW,    CString("WEGEZEITENTABLEVIEWER"), CString("Wegezeiten New"),    WegezeitenTableCf);
    ogDdx.Register(this, WAY_DELETE, CString("WEGEZEITENTABLEVIEWER"), CString("Wegezeiten Delete"), WegezeitenTableCf);
}

//-----------------------------------------------------------------------------------------------

WegezeitenTableViewer::~WegezeitenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omGrnCPA.DeleteAll();
	omGrnUrnoMap.RemoveAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void WegezeitenTableViewer::Attach(CCSTable *popTable)
{
    pomWegezeitenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void WegezeitenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();
	omGrnCPA.DeleteAll();
	omGrnUrnoMap.RemoveAll();
	char clWhere[100];
	sprintf(clWhere,"WHERE TABN='PST%s'",ogTableExt);

	if(ogGrnData.ReadSpecialData(&omGrnCPA,clWhere,"URNO,GRSN",false) == true)
	{
		int ilGrnSize = omGrnCPA.GetSize();
		for(int i=0;i<ilGrnSize;i++)
		{
			omGrnUrnoMap.SetAt((void*)omGrnCPA[i].Urno,(void*)&omGrnCPA[i]);
		}
	}
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void WegezeitenTableViewer::MakeLines()
{
	int ilWegezeitenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilWegezeitenCount; ilLc++)
	{
		WAYDATA *prlWegezeitenData = &pomData->GetAt(ilLc);
		MakeLine(prlWegezeitenData);
	}
}

//-----------------------------------------------------------------------------------------------

void WegezeitenTableViewer::MakeLine(WAYDATA *prpWegezeiten)
{

    //if( !IsPassFilter(prpWegezeiten)) return;

    // Update viewer data for this shift record
    WEGEZEITENTABLE_LINEDATA rlWegezeiten;
	rlWegezeiten.Urno = prpWegezeiten->Urno; // Eindeutige Datensatz-Nr.
	rlWegezeiten.Pobe = prpWegezeiten->Pobe; // Ausgangsposition
	rlWegezeiten.Tybe = this->NameFromCode(prpWegezeiten->Tybe); // von P/A/S
	rlWegezeiten.Poen = prpWegezeiten->Poen; // Zielposition
	rlWegezeiten.Tyen = this->NameFromCode(prpWegezeiten->Tyen); // nach P/A/S
	rlWegezeiten.Ttgo = prpWegezeiten->Ttgo; // Wegezeit
	rlWegezeiten.Wtgo = prpWegezeiten->Wtgo; // Wegstrecke
	rlWegezeiten.Wtyp = prpWegezeiten->Wtyp; // Wegetyp (z.B. Schlepp, Gang zu Fu�, Ger�t)

	rlWegezeiten.Home = prpWegezeiten->Home; // Wegetyp (z.B. Schlepp, Gang zu Fu�, Ger�t)

	CreateLine(&rlWegezeiten);
}

//-----------------------------------------------------------------------------------------------

void WegezeitenTableViewer::CreateLine(WEGEZEITENTABLE_LINEDATA *prpWegezeiten)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareWegezeiten(prpWegezeiten, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	WEGEZEITENTABLE_LINEDATA rlWegezeiten;
	rlWegezeiten = *prpWegezeiten;
    omLines.NewAt(ilLineno, rlWegezeiten);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void WegezeitenTableViewer::UpdateDisplay()
{
	CString olTmp;
	int ilLines = omLines.GetSize();
	const int ilColumns = 7;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomWegezeitenTable->SetShowSelection(TRUE);
	pomWegezeitenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 180;													// Tybe
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING282),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 220;													// Pobe
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING282),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 180;													// Tyen
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING282),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 220;													// Poen
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING282),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 33;													// Ttgo
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING282),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 66;													// Wtgo
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING282),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42;													// xxxx
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING282),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomWegezeitenTable->SetHeaderFields(omHeaderDataArray);
	pomWegezeitenTable->SetDefaultSeparator();
	pomWegezeitenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Tybe;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pobe;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tyen;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Poen;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ttgo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if(omLines[ilLineNo].Tybe == NameFromCode("R") || omLines[ilLineNo].Tyen == NameFromCode("R"))
		{
			rlColumnData.Text = "";
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omLines[ilLineNo].Wtgo;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}
		else
		{
			rlColumnData.Alignment = COLALIGN_RIGHT;
			rlColumnData.Text = omLines[ilLineNo].Wtgo;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Alignment = COLALIGN_LEFT;
			rlColumnData.Text = "";
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		pomWegezeitenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomWegezeitenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString WegezeitenTableViewer::Format(WEGEZEITENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool WegezeitenTableViewer::FindWegezeiten(char *pcpWegezeitenKeya, char *pcpWegezeitenKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpWegezeitenKeya) &&
			 (omLines[ilItem].Keyd == pcpWegezeitenKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void WegezeitenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    WegezeitenTableViewer *polViewer = (WegezeitenTableViewer *)popInstance;
    if (ipDDXType == WAY_CHANGE || ipDDXType == WAY_NEW) 
		polViewer->ProcessWegezeitenChange((WAYDATA *)vpDataPointer);
    else if (ipDDXType == WAY_DELETE) 
		polViewer->ProcessWegezeitenDelete((WAYDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void WegezeitenTableViewer::ProcessWegezeitenChange(WAYDATA *prpWegezeiten)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CString olTmp;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = NameFromCode(prpWegezeiten->Tybe);
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpWegezeiten->Pobe;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = NameFromCode(prpWegezeiten->Tyen);
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpWegezeiten->Poen;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpWegezeiten->Ttgo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	if(*prpWegezeiten->Tybe == 'R' || *prpWegezeiten->Tyen == 'R')
	{
		rlColumn.Text = "";
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpWegezeiten->Wtgo;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}
	else
	{
		rlColumn.Alignment = COLALIGN_RIGHT;
		rlColumn.Text = prpWegezeiten->Wtgo;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Alignment = COLALIGN_LEFT;
		rlColumn.Text = "";
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}

	if (FindLine(prpWegezeiten->Urno, ilItem))
	{
        WEGEZEITENTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpWegezeiten->Urno; // Eindeutige Datensatz-Nr.
		prlLine->Pobe = prpWegezeiten->Pobe; // Ausgangsposition
		prlLine->Tybe = NameFromCode(prpWegezeiten->Tybe); // von P/A/S
		prlLine->Poen = prpWegezeiten->Poen; // Zielposition
		prlLine->Tyen = NameFromCode(prpWegezeiten->Tyen); // nach P/A/S
		prlLine->Ttgo = prpWegezeiten->Ttgo; // Wegezeit
		prlLine->Wtgo = prpWegezeiten->Wtgo; // Wegstrecke
		prlLine->Wtyp = prpWegezeiten->Wtyp; // Wegetyp (z.B. Schlepp, Gang zu Fu�, Ger�t)

		prlLine->Home = prpWegezeiten->Home; // Wegetyp (z.B. Schlepp, Gang zu Fu�, Ger�t)

		pomWegezeitenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomWegezeitenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpWegezeiten);
		if (FindLine(prpWegezeiten->Urno, ilItem))
		{
	        WEGEZEITENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomWegezeitenTable->AddTextLine(olLine, (void *)prlLine);
				pomWegezeitenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void WegezeitenTableViewer::ProcessWegezeitenDelete(WAYDATA *prpWegezeiten)
{
	int ilItem;
	if (FindLine(prpWegezeiten->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomWegezeitenTable->DeleteTextLine(ilItem);
		pomWegezeitenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool WegezeitenTableViewer::IsPassFilter(WAYDATA *prpWegezeiten)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int WegezeitenTableViewer::CompareWegezeiten(WEGEZEITENTABLE_LINEDATA *prpWegezeiten1, WEGEZEITENTABLE_LINEDATA *prpWegezeiten2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpWegezeiten1->Tifd == prpWegezeiten2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpWegezeiten1->Tifd > prpWegezeiten2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void WegezeitenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool WegezeitenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void WegezeitenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void WegezeitenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING196);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool WegezeitenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool WegezeitenTableViewer::PrintTableLine(WEGEZEITENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	CString olTmp;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Tybe;
				break;
			case 1:
				rlElement.Text		 = prpLine->Pobe;
				break;
			case 2:
				rlElement.Text		 = prpLine->Tyen;
				break;
			case 3:
				rlElement.Text		 = prpLine->Poen;
				break;
			case 4:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		 = prpLine->Ttgo;
				break;
			case 5:
				if(prpLine->Tybe == NameFromCode("R") || prpLine->Tyen == NameFromCode("R"))
				{
					rlElement.Text		 = "";
				}
				else
				{
					rlElement.Alignment  = PRINT_RIGHT;
					rlElement.Text		 = prpLine->Wtgo;

				}
				break;
			case 6:
				if(prpLine->Tybe == NameFromCode("R") || prpLine->Tyen == NameFromCode("R"))
				{
					rlElement.Text		 = prpLine->Wtgo;
				}
				else
				{
					rlElement.Text		 = "";

				}
				rlElement.FrameRight = PRINT_FRAMETHIN;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------

CString	WegezeitenTableViewer::NameFromCode(const CString& ropCode)
{
	CString name;
	switch(ropCode[0])
	{
	case 'C' :
		name = LoadStg(IDS_STRING997);
	break;
	case 'G' :
		name = LoadStg(IDS_STRING996);
	break;
	case 'P' :
		name = LoadStg(IDS_STRING422);
	break;
	case 'R' :
		name = LoadStg(IDS_STRING423);
	break;
	case 'A' :
		name = LoadStg(IDS_STRING424);
	break;
	default:
		;
	}

	return name;
}
//-----------------------------------------------------------------------------------------------
