// CedaWisData.cpp
 
#include <stdafx.h>
#include <CedaWisData.h>
#include <resource.h>


void ProcessWisCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaWisData::CedaWisData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for WISDataStruct
	BEGIN_CEDARECINFO(WISDATA,WISDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Orgc,"ORGC")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Shir,"SHIR")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Wisc,"WISC")
		FIELD_CHAR_TRIM	(Wisd,"WISD")

	END_CEDARECINFO //(WISDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(WISDataRecInfo)/sizeof(WISDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&WISDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"WIS");
	strcpy(pcmListOfFields,"CDAT,LSTU,ORGC,REMA,SHIR,URNO,USEC,USEU,WISC,WISD");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaWisData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("ORGC");
	ropFields.Add("REMA");
	ropFields.Add("SHIR");
	ropFields.Add("WISC");
	ropFields.Add("WISD");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING41));
	ropDesription.Add(LoadStg(IDS_STRING805));
	ropDesription.Add(LoadStg(IDS_STRING819));
	ropDesription.Add(LoadStg(IDS_STRING818));
	ropDesription.Add(LoadStg(IDS_STRING501));
	ropDesription.Add(LoadStg(IDS_STRING346));	// URNO
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaWisData::Register(void)
{
	ogDdx.Register((void *)this,BC_WIS_CHANGE,	CString("WISDATA"), CString("Wis-changed"),	ProcessWisCf);
	ogDdx.Register((void *)this,BC_WIS_NEW,		CString("WISDATA"), CString("Wis-new"),		ProcessWisCf);
	ogDdx.Register((void *)this,BC_WIS_DELETE,	CString("WISDATA"), CString("Wis-deleted"),	ProcessWisCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaWisData::~CedaWisData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaWisData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaWisData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		WISDATA *prlWis = new WISDATA;
		if ((ilRc = GetFirstBufferRecord(prlWis)) == true)
		{
			omData.Add(prlWis);//Update omData
			omUrnoMap.SetAt((void *)prlWis->Urno,prlWis);
		}
		else
		{
			delete prlWis;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaWisData::Insert(WISDATA *prpWis)
{
	prpWis->IsChanged = DATA_NEW;
	if(Save(prpWis) == false) return false; //Update Database
	InsertInternal(prpWis);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaWisData::InsertInternal(WISDATA *prpWis)
{
	ogDdx.DataChanged((void *)this, WIS_NEW,(void *)prpWis ); //Update Viewer
	omData.Add(prpWis);//Update omData
	omUrnoMap.SetAt((void *)prpWis->Urno,prpWis);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaWisData::Delete(long lpUrno)
{
	WISDATA *prlWis = GetWisByUrno(lpUrno);
	if (prlWis != NULL)
	{
		prlWis->IsChanged = DATA_DELETED;
		if(Save(prlWis) == false) return false; //Update Database
		DeleteInternal(prlWis);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaWisData::DeleteInternal(WISDATA *prpWis)
{
	ogDdx.DataChanged((void *)this,WIS_DELETE,(void *)prpWis); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpWis->Urno);
	int ilWisCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilWisCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpWis->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaWisData::Update(WISDATA *prpWis)
{
	if (GetWisByUrno(prpWis->Urno) != NULL)
	{
		if (prpWis->IsChanged == DATA_UNCHANGED)
		{
			prpWis->IsChanged = DATA_CHANGED;
		}
		if(Save(prpWis) == false) return false; //Update Database
		UpdateInternal(prpWis);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaWisData::UpdateInternal(WISDATA *prpWis)
{
	WISDATA *prlWis = GetWisByUrno(prpWis->Urno);
	if (prlWis != NULL)
	{
		*prlWis = *prpWis; //Update omData
		ogDdx.DataChanged((void *)this,WIS_CHANGE,(void *)prlWis); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

WISDATA *CedaWisData::GetWisByUrno(long lpUrno)
{
	WISDATA  *prlWis;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlWis) == TRUE)
	{
		return prlWis;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaWisData::ReadSpecialData(CCSPtrArray<WISDATA> *popWis,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","WIS",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","WIS",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popWis != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			WISDATA *prpWis = new WISDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpWis,CString(pclFieldList))) == true)
			{
				popWis->Add(prpWis);
			}
			else
			{
				delete prpWis;
			}
		}
		if(popWis->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaWisData::Save(WISDATA *prpWis)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpWis->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpWis->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpWis);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpWis->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWis->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpWis);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpWis->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWis->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessWisCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogWisData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaWisData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlWisData;
	prlWisData = (struct BcStruct *) vpDataPointer;
	WISDATA *prlWis;
	if(ipDDXType == BC_WIS_NEW)
	{
		prlWis = new WISDATA;
		GetRecordFromItemList(prlWis,prlWisData->Fields,prlWisData->Data);
		if(ValidateWisBcData(prlWis->Urno)) //Prf: 8795
		{
			InsertInternal(prlWis);
		}
		else
		{
			delete prlWis;
		}
	}
	if(ipDDXType == BC_WIS_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlWisData->Selection);
		prlWis = GetWisByUrno(llUrno);
		if(prlWis != NULL)
		{
			GetRecordFromItemList(prlWis,prlWisData->Fields,prlWisData->Data);
			if(ValidateWisBcData(prlWis->Urno)) //Prf: 8795
			{
				UpdateInternal(prlWis);
			}
			else
			{
				DeleteInternal(prlWis);
			}
		}
	}
	if(ipDDXType == BC_WIS_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlWisData->Selection);
		prlWis = GetWisByUrno(llUrno);
		if (prlWis != NULL)
		{
			DeleteInternal(prlWis);
		}
	}
}

//Prf: 8795
//--ValidateWisBcData--------------------------------------------------------------------------------------

bool CedaWisData::ValidateWisBcData(const long& lrpUrno)
{
	bool blValidateWisBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<WISDATA> olWiss;
		if(!ReadSpecialData(&olWiss,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateWisBcData = false;
		}
	}
	return blValidateWisBcData;
}

//---------------------------------------------------------------------------------------------------------
