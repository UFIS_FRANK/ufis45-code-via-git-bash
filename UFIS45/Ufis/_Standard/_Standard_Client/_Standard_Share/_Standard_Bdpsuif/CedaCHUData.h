// CedaCHUData.h

#ifndef __CEDACHUDATA__
#define __CEDACHUDATA__

#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct CHUDATA 
{
	long 	 Urno;          //Unique record number
	char     Hopo[4];       //Home airport
	char 	 Usec[33];      //User who created record
	char 	 Useu[33];      //User who updated record
	char     Cnam[6];       //Chute Name
	char     Term[2];       //Terminal
	char     Sort[2];       //Sorter
	char     Maxf[2];       //Max. Flights
	CTime	 Cdat;          //Creation date/time
	CTime	 Lstu;          //Last update date/time
	CTime	 Vafr;	 	    //Valid From
	CTime	 Vato;	 	    //Valid To

	//DataCreated by this class
	int		 IsChanged;

	CHUDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}
};


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaCHUData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr		 omUrnoMap;	
    CCSPtrArray<CHUDATA> omData;

// Operations
public:
    CedaCHUData();
	~CedaCHUData();

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<CHUDATA> *popCHUArray,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertCHU(CHUDATA *prpCHU,BOOL bpSendDdx = TRUE);
	bool InsertCHUInternal(CHUDATA *prpCHU);
	bool UpdateCHU(CHUDATA *prpCHU,BOOL bpSendDdx = TRUE);
	bool UpdateCHUInternal(CHUDATA *prpCHU);
	bool DeleteCHU(long lpUrno);
	bool DeleteCHUInternal(CHUDATA *prpCHU);
	CHUDATA  *GetCHUByUrno(long lpUrno);	
	bool SaveCHU(CHUDATA *prpCHU);
	char pcmCHUFieldList[2048];
	void ProcessCHUBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);	
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;}
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;}
	
private:
    void PrepareCHUData(CHUDATA *prpCHUData);	
	CString omWhere;
	bool ValidateCHUBcData(const long& lrpUnro);	
};


//---------------------------------------------------------------------------------------------------------

#endif //__CedaCHUData__
