// Sco_Dlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <Sco_Dlg.h>
#include <CedaBasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CSco_Dlg 
CSco_Dlg::CSco_Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSco_Dlg::IDD, pParent)
{
}

//---------------------------------------------------------------------------
CSco_Dlg::CSco_Dlg(CString Code, CString Kost, CString Cweh, CWnd* pParent /*=NULL*/) : CDialog(CSco_Dlg::IDD, pParent)
{
	omCode = Code;
	omKost = Kost;
	omCweh = Cweh;

	//{{AFX_DATA_INIT(CSco_Dlg)
	m_CWEH = _T("");
	m_KOST = _T("");
	//}}AFX_DATA_INIT
}


void CSco_Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSco_Dlg)
	DDX_Control(pDX, IDC_CWEH, m_ctrl_Cweh);
	DDX_Control(pDX, IDC_KOST, m_ctrl_Kost);
	DDX_Control(pDX, IDC_COMBO1, m_CB_SCO);
	DDX_Text(pDX, IDC_CWEH, m_CWEH);
	DDX_Text(pDX, IDC_KOST, m_KOST);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSco_Dlg, CDialog)
	//{{AFX_MSG_MAP(CSco_Dlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CSco_Dlg 

void CSco_Dlg::OnOK() 
{
	bool ilStatus = true;
	CString olErrorText;

	//uhi 18.4.01
	if(m_CB_SCO.GetCurSel() == CB_ERR && !omCode.IsEmpty()){
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING299) + ogNotFormat;
	}

	if(ilStatus == false) 
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
	}
	else
	{
		SaveSCOData();
		CDialog::OnOK();
	}
}

BOOL CSco_Dlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText("Staff <-> Contract DLG");
	// TODO: Zus�tzliche Initialisierung hier einf�gen
	ReadSCOData();
	ReadCOTData();

	//uhi 18.4.01
	m_ctrl_Cweh.SetTypeToString("X(5)",5,1);
	m_ctrl_Cweh.SetTextErrColor(RED);
	m_ctrl_Cweh.SetFormat("[##':'##]");

	SetSCOData();
	InitCombo();	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CSco_Dlg::InitCombo()
{
//	m_CB_SCO.SetFont(&ogCourier_Regular_8);

	// alle vorhandenen Eintr�ge l�schen
	CString opCODE;

	while (m_CB_SCO.GetCount() > 0) m_CB_SCO.DeleteString(0);

	for(int i=0;i<ogBCD.GetDataCount("COT");i++)
	{
		opCODE = ogBCD.GetField("COT",i,"CTRC");
		m_CB_SCO.AddString(opCODE);
	}

	if (!omCode.IsEmpty())
	{	//select!
		m_CB_SCO.SelectString(-1 , (LPCTSTR)  omCode);
	}
}

void CSco_Dlg::ReadSCOData()
{
		char querystr[1024];

		ogBCD.SetObject("SCO","URNO,SURN,CODE,VPFR,VPTO,KOST,CWEH");
		
		im_UrnoPos=ogBCD.GetFieldIndex("SCO","URNO");	// Eindeutige Datensatz-Nr.
		im_SurnPos=ogBCD.GetFieldIndex("SCO","SURN");	// Referenz auf STF.URNO
		im_CodePos=ogBCD.GetFieldIndex("SCO","CODE");	// Codereferenz ORG.CODE
		im_VpfrPos=ogBCD.GetFieldIndex("SCO","VPFR");	// G�ltig von
		im_VptoPos=ogBCD.GetFieldIndex("SCO","VPTO");	// G�ltig bis
		im_KostPos=ogBCD.GetFieldIndex("SCO","KOST");	// Kostenstelle
		im_CewhPos=ogBCD.GetFieldIndex("SCO","CWEH");	// vertragl. Wochenstunden [HHMM]

		sprintf(querystr,"");

		ogBCD.Read("SCO",querystr);

		int il = ogBCD.GetDataCount("SCO");
}

void CSco_Dlg::ReadCOTData()
{
		char querystr[1024];

		ogBCD.SetObject("COT","URNO,CTRC,CTRN,CDAT,DPTC,HOPO");

		sprintf(querystr,"");

		ogBCD.Read("COT",querystr);

		int il = ogBCD.GetDataCount("COT");
}

//---------------------------------------------------------------------------
bool CSco_Dlg::SaveSCOData()
{
	m_CB_SCO.GetWindowText(omCode);
	m_ctrl_Kost.GetWindowText(omKost);
	
	CString olTemp;
	m_ctrl_Cweh.GetWindowText(olTemp);
	if (olTemp.GetLength())
		omCweh.Format("%02d%02d", atoi(olTemp.Left(2)), atoi(olTemp.Right(2)));
	else
		omCweh = "";

	return true;
}
//---------------------------------------------------------------------------
void CSco_Dlg::SetSCOData()
{

	m_ctrl_Kost.SetWindowText(omKost);
	if(strlen(omCweh) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(omCweh).Left(2), CString(omCweh).Right(2));
		m_ctrl_Cweh.SetInitText(pclTmp);
	}
}
//---------------------------------------------------------------------------
