// CounterclassTableViewer.cpp 
//

#include <stdafx.h>
#include <CCCTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void CounterclassTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// CounterclassTableViewer
//

CounterclassTableViewer::CounterclassTableViewer(CCSPtrArray<CCCDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomCounterClassTable = NULL;
    ogDdx.Register(this, CCC_CHANGE, CString("CounterclassTableViewer"), CString("Counterclass Update"), CounterclassTableCf);
    ogDdx.Register(this, CCC_NEW,    CString("CounterclassTableViewer"), CString("Counterclass New"),    CounterclassTableCf);
    ogDdx.Register(this, CCC_DELETE, CString("CounterclassTableViewer"), CString("Counterclass Delete"), CounterclassTableCf);
}

//-----------------------------------------------------------------------------------------------

CounterclassTableViewer::~CounterclassTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void CounterclassTableViewer::Attach(CCSTable *popTable)
{
    pomCounterClassTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void CounterclassTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void CounterclassTableViewer::MakeLines()
{
	int ilCounterclassCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilCounterclassCount; ilLc++)
	{
		CCCDATA *prlCounterclassData = &pomData->GetAt(ilLc);
		MakeLine(prlCounterclassData);
	}
}

//-----------------------------------------------------------------------------------------------

void CounterclassTableViewer::MakeLine(CCCDATA *prpCounterclass)
{

    //if( !IsPassFilter(prpCounterclass)) return;

    // Update viewer data for this shift record
    COUNTERCLASSTABLE_LINEDATA rlCounterclass;

	rlCounterclass.Urno = prpCounterclass->Urno;
	rlCounterclass.Cicc = prpCounterclass->Cicc;
	rlCounterclass.Cicn = prpCounterclass->Cicn;
	rlCounterclass.Vafr = prpCounterclass->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlCounterclass.Vato = prpCounterclass->Vato.Format("%d.%m.%Y %H:%M"); 

	CreateLine(&rlCounterclass);
}

//-----------------------------------------------------------------------------------------------

void CounterclassTableViewer::CreateLine(COUNTERCLASSTABLE_LINEDATA *prpCounterclass)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareCounterclass(prpCounterclass, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	COUNTERCLASSTABLE_LINEDATA rlCounterclass;
	rlCounterclass = *prpCounterclass;
    omLines.NewAt(ilLineno, rlCounterclass);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void CounterclassTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomCounterClassTable->SetShowSelection(TRUE);
	pomCounterClassTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING613),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 249; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING613),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING613),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING613),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomCounterClassTable->SetHeaderFields(omHeaderDataArray);

	pomCounterClassTable->SetDefaultSeparator();
	pomCounterClassTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Cicc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Cicn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomCounterClassTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomCounterClassTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString CounterclassTableViewer::Format(COUNTERCLASSTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool CounterclassTableViewer::FindCounterclass(char *pcpCounterclassKeya, char *pcpCounterclassKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpCounterclassKeya) &&
			 (omLines[ilItem].Keyd == pcpCounterclassKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void CounterclassTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CounterclassTableViewer *polViewer = (CounterclassTableViewer *)popInstance;
    if (ipDDXType == CCC_CHANGE || ipDDXType == CCC_NEW) polViewer->ProcessCounterclassChange((CCCDATA *)vpDataPointer);
    if (ipDDXType == CCC_DELETE) polViewer->ProcessCounterclassDelete((CCCDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void CounterclassTableViewer::ProcessCounterclassChange(CCCDATA *prpCounterclass)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpCounterclass->Cicc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpCounterclass->Cicn;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpCounterclass->Vafr.Format("%d.%m.%Y %H:%M");
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpCounterclass->Vato.Format("%d.%m.%Y %H:%M");
	olLine.NewAt(olLine.GetSize(), rlColumn);

	
	if (FindLine(prpCounterclass->Urno, ilItem))
	{
        COUNTERCLASSTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpCounterclass->Urno;
		prlLine->Cicc = prpCounterclass->Cicc;
		prlLine->Cicn = prpCounterclass->Cicn;

		prlLine->Vafr = prpCounterclass->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpCounterclass->Vato.Format("%d.%m.%Y %H:%M");


		pomCounterClassTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomCounterClassTable->DisplayTable();
	}
	else
	{
		MakeLine(prpCounterclass);
		if (FindLine(prpCounterclass->Urno, ilItem))
		{
	        COUNTERCLASSTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomCounterClassTable->AddTextLine(olLine, (void *)prlLine);
				pomCounterClassTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void CounterclassTableViewer::ProcessCounterclassDelete(CCCDATA *prpCounterclass)
{
	int ilItem;
	if (FindLine(prpCounterclass->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomCounterClassTable->DeleteTextLine(ilItem);
		pomCounterClassTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool CounterclassTableViewer::IsPassFilter(CCCDATA *prpCounterclass)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int CounterclassTableViewer::CompareCounterclass(COUNTERCLASSTABLE_LINEDATA *prpCounterclass1, COUNTERCLASSTABLE_LINEDATA *prpCounterclass2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpCounterclass1->Tifd == prpCounterclass2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpCounterclass1->Tifd > prpCounterclass2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void CounterclassTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool CounterclassTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void CounterclassTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void CounterclassTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING202);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool CounterclassTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool CounterclassTableViewer::PrintTableLine(COUNTERCLASSTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Cicc;
				break;
			case 1:
				rlElement.Text		 = prpLine->Cicn;
				break;
			case 2:
				rlElement.Text		 = prpLine->Vafr;
				break;
			case 3:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Vato;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
