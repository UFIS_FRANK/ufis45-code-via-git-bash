// CedaHaiData.cpp
 
#include <stdafx.h>
#include <CedaHaiData.h>
#include <resource.h>



//--CEDADATA-----------------------------------------------------------------------------------------------

CedaHaiData::CedaHaiData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for HAIDATA
	BEGIN_CEDARECINFO(HAIDATA,HaiDataRecInfo)
		FIELD_CHAR_TRIM	(Hsna,"HSNA")
		FIELD_CHAR_TRIM	(Task,"TASK")
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Altu,"ALTU")
	END_CEDARECINFO //(HAIDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(HaiDataRecInfo)/sizeof(HaiDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&HaiDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"HAITAB");
	strcpy(pcmHaiFieldList,"HSNA,TASK,URNO,ALTU");
	pcmFieldList = pcmHaiFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//----------------------------------------------------------------------------------------------------




//--~CEDADATA----------------------------------------------------------------------------------------------

CedaHaiData::~CedaHaiData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaHaiData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
	omAltuMap.RemoveAll();
    omData.DeleteAll();
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaHaiData::Read(const char *pcpWhere)
{

	char pclSel[1024];

	strcpy(pclSel,pcpWhere);

    // Select data from the database
	bool ilRc = true;
	ClearAll();
 	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pclSel);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		HAIDATA *prpHai = new HAIDATA;
		if ((ilRc = GetFirstBufferRecord(prpHai)) == true)
		{
			omData.Add(prpHai);//Update omData
			omUrnoMap.SetAt((void *)prpHai->Urno,prpHai);
			omAltuMap.SetAt((void *)prpHai->Altu,prpHai);
		}
		else
		{
			delete prpHai;
		}
	}
    return true;
}


//--GET-BY-URNO--------------------------------------------------------------------------------------------

HAIDATA *CedaHaiData::GetHaiByAltu(long lpAltu)
{
	HAIDATA  *prlHai;
	if (omAltuMap.Lookup((void *)lpAltu,(void *& )prlHai) == TRUE)
	{
		return prlHai;
	}
	return NULL;
	
}
