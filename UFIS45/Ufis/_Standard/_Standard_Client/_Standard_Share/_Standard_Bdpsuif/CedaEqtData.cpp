// CedaEqtData.cpp
 
#include <stdafx.h>
#include <CedaEqtData.h>
#include <resource.h>


void ProcessEqtCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaEqtData::CedaEqtData() : CCSCedaData(&ogCommHandler)
{ 
	// Create an array of CEDARECINFO for EQTDataStruct
	BEGIN_CEDARECINFO(EQTDATA,EQTDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_CHAR_TRIM	(Hopo,"HOPO")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Name,"NAME")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
	END_CEDARECINFO //(EQTDataStruct)	

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(EQTDataRecInfo)/sizeof(EQTDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&EQTDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"EQT");
	//~~~~~ 13.08.99 SHA : 
	//*** ATTENTION FIELDS TATP AND TSAP REMOVED TEMPOR. !!!! ***
	strcpy(pcmListOfFields,"CDAT,CODE,HOPO,LSTU,NAME,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaEqtData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("CODE");
	ropFields.Add("LSTU");
	ropFields.Add("NAME");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING288));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaEqtData::Register(void)
{
	ogDdx.Register((void *)this,BC_EQT_CHANGE,	CString("EQTDATA"), CString("EQT-changed"),	ProcessEqtCf);
	ogDdx.Register((void *)this,BC_EQT_NEW,		CString("EQTDATA"), CString("EQT-new"),		ProcessEqtCf);
	ogDdx.Register((void *)this,BC_EQT_DELETE,	CString("EQTDATA"), CString("EQT-deleted"),	ProcessEqtCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaEqtData::~CedaEqtData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaEqtData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaEqtData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		EQTDATA *prlEqt = new EQTDATA;
		if ((ilRc = GetFirstBufferRecord(prlEqt)) == true)
		{
			omData.Add(prlEqt);//Update omData
			omUrnoMap.SetAt((void *)prlEqt->Urno,prlEqt);
		}
		else
		{
			delete prlEqt;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaEqtData::Insert(EQTDATA *prpEqt)
{
	prpEqt->IsChanged = DATA_NEW;
	if(Save(prpEqt) == false) return false; //Update Database
	InsertInternal(prpEqt);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaEqtData::InsertInternal(EQTDATA *prpEqt)
{
	ogDdx.DataChanged((void *)this, EQT_NEW,(void *)prpEqt ); //Update Viewer
	omData.Add(prpEqt);//Update omData
	omUrnoMap.SetAt((void *)prpEqt->Urno,prpEqt);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaEqtData::Delete(long lpUrno)
{
	EQTDATA *prlEqt = GetEqtByUrno(lpUrno);
	if (prlEqt != NULL)
	{
		prlEqt->IsChanged = DATA_DELETED;
		if(Save(prlEqt) == false) return false; //Update Database
		DeleteInternal(prlEqt);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaEqtData::DeleteInternal(EQTDATA *prpEqt)
{
	ogDdx.DataChanged((void *)this,EQT_DELETE,(void *)prpEqt); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpEqt->Urno);
	int ilEqtCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilEqtCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpEqt->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaEqtData::Update(EQTDATA *prpEqt)
{
	if (GetEqtByUrno(prpEqt->Urno) != NULL)
	{
		if (prpEqt->IsChanged == DATA_UNCHANGED)
		{
			prpEqt->IsChanged = DATA_CHANGED;
		}
		if(Save(prpEqt) == false) return false; //Update Database
		UpdateInternal(prpEqt);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaEqtData::UpdateInternal(EQTDATA *prpEqt)
{
	EQTDATA *prlEqt = GetEqtByUrno(prpEqt->Urno);
	if (prlEqt != NULL)
	{
		*prlEqt = *prpEqt; //Update omData
		ogDdx.DataChanged((void *)this,EQT_CHANGE,(void *)prlEqt); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

EQTDATA *CedaEqtData::GetEqtByUrno(long lpUrno)
{
	EQTDATA  *prlEqt;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlEqt) == TRUE)
	{
		return prlEqt;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaEqtData::ReadSpecialData(CCSPtrArray<EQTDATA> *popEqt,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","EQT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","EQT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popEqt != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			EQTDATA *prpEqt = new EQTDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpEqt,CString(pclFieldList))) == true)
			{
				popEqt->Add(prpEqt);
			}
			else
			{
				delete prpEqt;
			}
		}
		if(popEqt->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaEqtData::Save(EQTDATA *prpEqt)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpEqt->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpEqt->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpEqt);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpEqt->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpEqt->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpEqt);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpEqt->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpEqt->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessEqtCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogEqtData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaEqtData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlEqtData;
	prlEqtData = (struct BcStruct *) vpDataPointer;
	EQTDATA *prlEqt;
	if(ipDDXType == BC_EQT_NEW)
	{
		prlEqt = new EQTDATA;
		GetRecordFromItemList(prlEqt,prlEqtData->Fields,prlEqtData->Data);
		if(ValidateEqtBcData(prlEqt->Urno)) //Prf: 8795
		{
			InsertInternal(prlEqt);
		}
		else
		{
			delete prlEqt;
		}
	}
	if(ipDDXType == BC_EQT_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlEqtData->Selection);
		prlEqt = GetEqtByUrno(llUrno);
		if(prlEqt != NULL)
		{
			GetRecordFromItemList(prlEqt,prlEqtData->Fields,prlEqtData->Data);
			if(ValidateEqtBcData(prlEqt->Urno)) //Prf: 8795
			{
				UpdateInternal(prlEqt);
			}
			else
			{
				DeleteInternal(prlEqt);
			}
		}
	}
	if(ipDDXType == BC_EQT_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlEqtData->Selection);

		prlEqt = GetEqtByUrno(llUrno);
		if (prlEqt != NULL)
		{
			DeleteInternal(prlEqt);
		}
	}
}

//Prf: 8795
//--ValidateEqtBcData--------------------------------------------------------------------------------------

bool CedaEqtData::ValidateEqtBcData(const long& lrpUrno)
{
	bool blValidateEqtBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<EQTDATA> olEqts;
		if(!ReadSpecialData(&olEqts,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateEqtBcData = false;
		}
	}
	return blValidateEqtBcData;
}

//---------------------------------------------------------------------------------------------------------
