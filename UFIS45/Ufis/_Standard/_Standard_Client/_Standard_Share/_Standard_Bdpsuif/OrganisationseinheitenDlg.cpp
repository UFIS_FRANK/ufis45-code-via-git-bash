// OrganisationseinheitenDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <OrganisationseinheitenDlg.h>
#include <PrivList.h>
#include <Odg_Dlg.h>
#include <CheckReferenz.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// OrganisationseinheitenDlg dialog


OrganisationseinheitenDlg::OrganisationseinheitenDlg(ORGDATA *popOrg,CWnd* pParent) : CDialog(OrganisationseinheitenDlg::IDD, pParent)
{
	pomOrg = popOrg;

	//{{AFX_DATA_INIT(OrganisationseinheitenDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomStatus = NULL;
}


void OrganisationseinheitenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(OrganisationseinheitenDlg)
	DDX_Control(pDX, IDC_COMBO1, m_CB_ODG);
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_DPT1,	 m_DPT1);
	DDX_Control(pDX, IDC_DPT2,	 m_DPT2);
	DDX_Control(pDX, IDC_DPTN,	 m_DPTN);
	DDX_Control(pDX, IDC_REMA,	 m_REMA);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_ODGL,	 m_ODGL);
	DDX_Control(pDX, IDC_ODKL,	 m_ODKL);
	DDX_Control(pDX, IDC_ODSL,	 m_ODSL);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(OrganisationseinheitenDlg, CDialog)
	//{{AFX_MSG_MAP(OrganisationseinheitenDlg)
	ON_BN_CLICKED(IDC_BUTTON1, OnDLG)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// OrganisationseinheitenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL OrganisationseinheitenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if(m_Caption == LoadStg(IDS_STRING150))
		bmChangeAction = true;
	else
		bmChangeAction = false;

	m_Caption = LoadStg(IDS_STRING184) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomOrg->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomOrg->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomOrg->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomOrg->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomOrg->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomOrg->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomGAT->);
	m_GRUP.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_DPT1.SetFormat("X(8)");
	m_DPT1.SetTextLimit(1,8);
	m_DPT1.SetBKColor(YELLOW);  
	m_DPT1.SetTextErrColor(RED);
	m_DPT1.SetInitText(pomOrg->Dpt1);
	this->omOldCode = pomOrg->Dpt1;

	m_DPT1.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_DPT1"));
	//------------------------------------
	m_DPT2.SetFormat("X(8)");
	m_DPT2.SetTextLimit(0,8);
	m_DPT2.SetTextErrColor(RED);
	m_DPT2.SetInitText(pomOrg->Dpt2);
	m_DPT2.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_DPT2"));
	//------------------------------------
	m_DPTN.SetTypeToString("X(40)",40,1);
	m_DPTN.SetBKColor(YELLOW);  
	m_DPTN.SetTextErrColor(RED);
	m_DPTN.SetInitText(pomOrg->Dptn);
	m_DPTN.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_DPTN"));
	//------------------------------------
	m_REMA.SetTypeToString("X(60)",60,0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomOrg->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_REMA"));
	//------------------------------------
	m_ODGL.SetTypeToTime(false);
	m_ODGL.SetTextErrColor(RED);
	m_ODGL.SetInitText(GetHHMMFromMinutes(pomOrg->Odgl,true));
	//------------------------------------
	m_ODKL.SetTypeToTime(false);
	m_ODKL.SetTextErrColor(RED);
	m_ODKL.SetInitText(GetHHMMFromMinutes(pomOrg->Odkl,true));
	//------------------------------------
	m_ODSL.SetTypeToTime(false);
	m_ODSL.SetTextErrColor(RED);
	m_ODSL.SetInitText(GetHHMMFromMinutes(pomOrg->Odsl,true));
	//------------------------------------
	return TRUE; 
}

//----------------------------------------------------------------------------------------

void OrganisationseinheitenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText,olNewCode;
	bool ilStatus = true;
	if(m_ODSL.GetStatus() == false)
	{
		ilStatus = false;
			olErrorText += LoadStg(IDS_STRING838) + ogNotFormat;
	}
	if(m_ODGL.GetStatus() == false)
	{
		ilStatus = false;
			olErrorText += LoadStg(IDS_STRING836) + ogNotFormat;
	}
	if(m_ODKL.GetStatus() == false)
	{
		ilStatus = false;
			olErrorText += LoadStg(IDS_STRING837) + ogNotFormat;
	}
	if(m_DPT1.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DPT1.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_DPT2.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING92) + ogNotFormat;
	}
	if(m_DPTN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DPTN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olDpt1,olDpt2;
		char clWhere[100];

		CCSPtrArray<ORGDATA> olOrgCPA;
		m_DPT1.GetWindowText(olDpt1);
		olNewCode = olDpt1;
		sprintf(clWhere,"WHERE DPT1='%s'",olDpt1);
		if(ogOrgData.ReadSpecialData(&olOrgCPA,clWhere,"URNO,DPT1",false) == true)
		{
			if(olOrgCPA[0].Urno != pomOrg->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olOrgCPA.DeleteAll();
	}
	
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		bool blChangeCode = true;
		if(olNewCode != omOldCode && bmChangeAction)
		{
			// Referenz Check
			CheckReferenz olCheckReferenz;
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(IDS_STRING120));
			int ilCount = olCheckReferenz.Check(_ORG, pomOrg->Urno, omOldCode);
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(ST_OK));
			CString olTxt;
			if(ilCount>0)
			{
				blChangeCode = false;
				if (!ogBasicData.BackDoorEnabled())
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING119));
					MessageBox(olTxt,LoadStg(IDS_STRING145),MB_ICONERROR);
				}
				else
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING983));
					if(IDYES == MessageBox(olTxt,LoadStg(IDS_STRING149),(MB_ICONSTOP | MB_YESNOCANCEL | MB_DEFBUTTON2)))
					{
						blChangeCode = true;
					}
				}
			}
			// END Referenz Check
		}

		if(blChangeCode)
		{
			m_DPT1.GetWindowText(pomOrg->Dpt1,9);
			m_DPT2.GetWindowText(pomOrg->Dpt2,9);
			m_DPTN.GetWindowText(pomOrg->Dptn,41);
		
			CString olText;
			m_ODGL.GetWindowText(olText); 
			strcpy(pomOrg->Odgl, GetMinutesFromHHMM(olText));
			m_ODKL.GetWindowText(olText);
			strcpy(pomOrg->Odkl, GetMinutesFromHHMM(olText));
			m_ODSL.GetWindowText(olText);
			strcpy(pomOrg->Odsl, GetMinutesFromHHMM(olText));

			//wandelt Return in Blank//
			CString olTemp;
			m_REMA.GetWindowText(olTemp);
			for(int i=0;i>-1;)
			{
				i = olTemp.Find("\r\n");
				if(i != -1)
				{
					if(olTemp.GetLength() > (i+2))
						olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
					else
						olTemp = olTemp.Left(i);
				}
			}
			strcpy(pomOrg->Rema,olTemp);
			////////////////////////////

			CDialog::OnOK();
		}
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_DPT1.SetFocus();
		m_DPT1.SetSel(0,-1);
  }
}
//----------------------------------------------------------------------------------------

void OrganisationseinheitenDlg::OnDLG() 
{
	CString olOrgCode = "";
	CString olOrgUrno ="";
	olOrgUrno.Format("%ld",pomOrg->Urno);
		
	m_DPT1.GetWindowText(olOrgCode);

	COdg_Dlg *pomDlg = new COdg_Dlg(olOrgCode,olOrgUrno, this);

	if(pomDlg->DoModal() == IDOK)
	{

	}
	
}
