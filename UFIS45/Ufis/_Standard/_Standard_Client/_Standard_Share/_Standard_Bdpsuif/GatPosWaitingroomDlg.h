#if !defined(AFX_GATPOSWAITINGROOMDLG_H__7B7C3CE8_BF13_11D6_8216_00010215BFDE__INCLUDED_)
#define AFX_GATPOSWAITINGROOMDLG_H__7B7C3CE8_BF13_11D6_8216_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatPosWaitingroomDlg.h : header file
//
#include <WaitingroomDlg.h>
#include <AatBitmapComboBox.h>

/////////////////////////////////////////////////////////////////////////////
// GatPosWaitingroomDlg dialog

class GatPosWaitingroomDlg : public WaitingroomDlg
{
// Construction
public:
	GatPosWaitingroomDlg(WRODATA *popWRO,CWnd* pParent = NULL);   // standard constructor
	GatPosWaitingroomDlg(WRODATA *popWRO,int ipDlg,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(GatPosWaitingroomDlg)
	enum { IDD = IDD_GATPOS_WAITINGROOMDLG };
	AatBitmapComboBox	m_BlockingBitmap;
	CCSEdit	m_SHGN;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GatPosWaitingroomDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GatPosWaitingroomDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnNoavNew();
	afx_msg void OnNoavCopy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATPOSWAITINGROOMDLG_H__7B7C3CE8_BF13_11D6_8216_00010215BFDE__INCLUDED_)
