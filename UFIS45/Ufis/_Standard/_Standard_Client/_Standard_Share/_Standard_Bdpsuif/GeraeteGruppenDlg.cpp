// GeraeteGruppenDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <GeraeteGruppenDlg.h>
#include <CedaGegData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GeraeteGruppenDlg dialog


GeraeteGruppenDlg::GeraeteGruppenDlg(GEGDATA *popGeg, CWnd* pParent /*=NULL*/)
	: CDialog(GeraeteGruppenDlg::IDD, pParent)
{
	pomGeg = popGeg;
	//{{AFX_DATA_INIT(GeraeteGruppenDlg)
	//}}AFX_DATA_INIT
}


void GeraeteGruppenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GeraeteGruppenDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_GCAT, m_GCAT);
	DDX_Control(pDX, IDC_GCDE, m_GCDE);
	DDX_Control(pDX, IDC_GNAM, m_GNAM);
	DDX_Control(pDX, IDC_GSNM, m_GSNM);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_REMA, m_REMA);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GeraeteGruppenDlg, CDialog)
	//{{AFX_MSG_MAP(GeraeteGruppenDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GeraeteGruppenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL GeraeteGruppenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING180) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("GERAETEGRUPPENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomGeg->Cdat.Format("%d.%m.%Y"));
	//m_CDATD.SetSecState(ogPrivList.GetStat("GERAETEGRUPPENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomGeg->Cdat.Format("%H:%M"));
	//m_CDATT.SetSecState(ogPrivList.GetStat("GERAETEGRUPPENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomGeg->Lstu.Format("%d.%m.%Y"));
	//m_LSTUD.SetSecState(ogPrivList.GetStat("GERAETEGRUPPENDLG.m_LSTU"));
	//// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomGeg->Lstu.Format("%H:%M"));
	//m_LSTUT.SetSecState(ogPrivList.GetStat("GERAETEGRUPPENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomGeg->Usec);
	//m_USEC.SetSecState(ogPrivList.GetStat("GERAETEGRUPPENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomGeg->Useu);
	//m_USEU.SetSecState(ogPrivList.GetStat("GERAETEGRUPPENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_GCDE.SetFormat("x|#x|#x|#x|#x|#");
	m_GCDE.SetTextLimit(1,5);
	m_GCDE.SetBKColor(YELLOW);  
	m_GCDE.SetTextErrColor(RED);
	m_GCDE.SetInitText(pomGeg->Gcde);
	//m_GCDE.SetSecState(ogPrivList.GetStat("GERAETEGRUPPENDLG.m_GCDE"));
	//------------------------------------
	m_GSNM.SetTypeToString("X(12)",12,1);
	m_GSNM.SetBKColor(YELLOW);  
	m_GSNM.SetTextErrColor(RED);
	m_GSNM.SetInitText(pomGeg->Gsnm);
	//m_GSNM.SetSecState(ogPrivList.GetStat("GERAETEGRUPPENDLG.m_GSNM"));
	//------------------------------------
	m_GCAT.SetTypeToString("A(3)",3,0);
	m_GCAT.SetTextErrColor(RED);
	m_GCAT.SetInitText(pomGeg->Gcat);
	//m_GCAT.SetSecState(ogPrivList.GetStat("GERAETEGRUPPENDLG.m_GCAT"));
	//------------------------------------
	m_GNAM.SetTypeToString("X(40)",40,1);
	m_GNAM.SetBKColor(YELLOW);  
	m_GNAM.SetTextErrColor(RED);
	m_GNAM.SetInitText(pomGeg->Gnam);
	//m_GNAM.SetSecState(ogPrivList.GetStat("GERAETEGRUPPENDLG.m_GNAM"));
	//------------------------------------
	m_REMA.SetTypeToString("X(60)", 60, 0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomGeg->Rema);
	//m_REMA.SetSecState(ogPrivList.GetStat("GERAETEGRUPPENDLG.m_REMA"));
	//------------------------------------
	
	return TRUE;
}

//----------------------------------------------------------------------------------------

void GeraeteGruppenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_GCDE.GetStatus() == false)
	{
		ilStatus = false;
		if(m_GCDE.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_GSNM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_GSNM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING301) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING301) + ogNotFormat;
		}
	}
	if(m_GCAT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING43) + ogNotFormat;
	}
	if(m_GNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_GNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olGcde;//
		char clWhere[100];
		CCSPtrArray<GEGDATA> olList;

		if(m_GCDE.GetWindowTextLength() != 0)
		{
			m_GCDE.GetWindowText(olGcde);
			sprintf(clWhere,"WHERE GCDE='%s'",olGcde);
			if(ogGegData.ReadSpecialData(&olList,clWhere,"URNO",false) == true)
			{
				long llUrno = olList[0].Urno;
				if(pomGeg->Urno != llUrno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
				}
			}
			olList.DeleteAll();
		}
	}

	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_GCAT.GetWindowText(pomGeg->Gcat,6);
		m_GSNM.GetWindowText(pomGeg->Gsnm,13);
		m_GCDE.GetWindowText(pomGeg->Gcde,6);
		m_GNAM.GetWindowText(pomGeg->Gnam,41);
		//wandelt Return in Blank//
		CString olTemp;
		m_REMA.GetWindowText(olTemp);
		for(int i=0;i>-1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomGeg->Rema,olTemp);
		////////////////////////////

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_GCDE.SetFocus();
	}	
}

//----------------------------------------------------------------------------------------
