// PDAEquipmentDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <PDAEquipmentDlg.h>
#include <PrivList.h>
#include <CedaStfData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PDAEquipmentDlg dialog


PDAEquipmentDlg::PDAEquipmentDlg(PDADATA *popPda,CWnd* pParent) : CDialog(PDAEquipmentDlg::IDD, pParent)
{
	pomPda = popPda;

	//{{AFX_DATA_INIT(PDAEquipmentDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void PDAEquipmentDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PDAEquipmentDlg)
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_PDID,	 m_PDID);
	DDX_Control(pDX, IDC_IPAD,	 m_IPAD);
	DDX_Control(pDX, IDC_VPFR_D, m_VPFRD);
	DDX_Control(pDX, IDC_VPFR_T, m_VPFRT);
	DDX_Control(pDX, IDC_VPTO_D, m_VPTOD);
	DDX_Control(pDX, IDC_VPTO_T, m_VPTOT);
	DDX_Control(pDX, IDC_NATO_T, m_NATOT);
	DDX_Control(pDX, IDC_NATO_D, m_NATOD);
	DDX_Control(pDX, IDC_NAFR_D, m_NAFRD);
	DDX_Control(pDX, IDC_NAFR_T, m_NAFRT);
	DDX_Control(pDX, IDC_CB_USTF, m_USTF);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PDAEquipmentDlg, CDialog)
	//{{AFX_MSG_MAP(PDAEquipmentDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PDAEquipmentDlg message handlers
//----------------------------------------------------------------------------------------

BOOL PDAEquipmentDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING32805) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("PDAEQUIPMENTDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomPda->Cdat.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomPda->Cdat.Format("%H:%M"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomPda->Lstu.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomPda->Lstu.Format("%H:%M"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomPda->Usec);
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomPda->Useu);
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_PDID.SetFormat("X(5)");
	m_PDID.SetTextLimit(1,5);
	m_PDID.SetBKColor(YELLOW);  
	m_PDID.SetTextErrColor(RED);
	m_PDID.SetInitText(pomPda->Pdid);
	/*
	//------------------------------------
	m_IPAD.SetFormat("x(15)");
	m_IPAD.SetTextLimit(1,15);
	m_IPAD.SetBKColor(YELLOW);  
	m_IPAD.SetTextErrColor(RED);
	m_IPAD.SetInitText(pomPda->Ipad);
	*/
	//------------------------------------
	m_VPFRD.SetTypeToDate(true);
	m_VPFRD.SetTextErrColor(RED);
	m_VPFRD.SetBKColor(YELLOW);
	m_VPFRD.SetInitText(pomPda->Vafr.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_VPFRT.SetTypeToTime(true);
	m_VPFRT.SetTextErrColor(RED);
	m_VPFRT.SetBKColor(YELLOW);
	m_VPFRT.SetInitText(pomPda->Vafr.Format("%H:%M"));
	//------------------------------------
	m_VPTOD.SetTextErrColor(RED);
	m_VPTOD.SetInitText(pomPda->Vato.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_VPTOT.SetTextErrColor(RED);
	m_VPTOT.SetInitText(pomPda->Vato.Format("%H:%M"));
	//------------------------------------

	m_NATOD.SetTypeToDate(false);
	m_NATOD.SetTextErrColor(RED);
	m_NATOD.SetInitText(pomPda->Nato.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_NATOT.SetTypeToTime(false);
	m_NATOT.SetTextErrColor(RED);
	m_NATOT.SetInitText(pomPda->Nato.Format("%H:%M"));
	//------------------------------------

	m_NAFRD.SetTypeToDate(false);
	m_NAFRD.SetTextErrColor(RED);
	m_NAFRD.SetInitText(pomPda->Nafr.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_NAFRT.SetTypeToTime(false);
	m_NAFRT.SetTextErrColor(RED);
	m_NAFRT.SetInitText(pomPda->Nafr.Format("%H:%M"));
	//------------------------------------


	BYTE Byte1; 
	BYTE Byte2; 
	BYTE Byte3; 
	BYTE Byte4; 
	CString olTmp = CString(pomPda->Ipad);
	CStringArray olArray;
	
	ExtractItemList(olTmp, &olArray, '.');

	if(olArray.GetSize() == 4)
	{
		Byte1 = atoi(olArray[0]);
		Byte2 = atoi(olArray[1]);
		Byte3 = atoi(olArray[2]);
		Byte4 = atoi(olArray[3]);
		m_IPAD.SetAddress(Byte1,Byte2,Byte3,Byte4);
	}



/*
	char pclConfigPath[256];
	char pclOrgCodes[256] = "";
	
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI");
	else
		strcpy(pclConfigPath, getenv("CEDA"));
	
	GetPrivateProfileString(ogAppName,"PDA_EMPLOYEE_ORG_CODE", "", pclOrgCodes, sizeof pclOrgCodes, pclConfigPath);


	CString olWhere ;

	if(CString(pclOrgCodes).IsEmpty())
		olWhere = "";
	else
		olWhere = "where urno in (select surn from sortab where code='" + CString(pclOrgCodes) + CString("')");;


	ogStfData.Read(olWhere.GetBuffer(0));

*/

	STFDATA *prlStf;
	CString olTmp2 = " ";

	char buffer[11];

	m_USTF.AddString(olTmp2);

	for(int i=0; i < ogStfData.omData.GetSize();i++)
	{
		prlStf = &ogStfData.omData.GetAt(i);
		itoa(prlStf->Urno, buffer, 10);			

		olTmp2 = CString(prlStf->Peno) + CString(", ") + CString(prlStf->Lanm) + CString(", ") + CString(prlStf->Finm)  + CString("                                                                                       ,") + CString(buffer);

		m_USTF.AddString(olTmp2);

		if(pomPda->Ustf == prlStf->Urno)
		{
			m_USTF.SetCurSel(i+1);

		}
	}



	return TRUE;  
}

//----------------------------------------------------------------------------------------

void PDAEquipmentDlg::OnOK() 
{
	
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_PDID.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PDID.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING32810) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING32810) + ogNotFormat;
		}
	}


	if(m_VPFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VPFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VPFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VPFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VPTOD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VPTOD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VPTOT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VPTOT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}

	///////////////////////////////////////////////////////////////////
	CString olVpfrd,olVpfrt,olVptod,olVptot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VPFRD.GetWindowText(olVpfrd);
	m_VPFRT.GetWindowText(olVpfrt);
	m_VPTOD.GetWindowText(olVptod);
	m_VPTOT.GetWindowText(olVptot);

	if(m_VPFRD.GetStatus() == true && m_VPFRT.GetStatus() == true && m_VPTOD.GetStatus() == true && m_VPTOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVpfrd,olVpfrt);
		olTmpTimeTo = DateHourStringToDate(olVptod,olVptot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo <= olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	///////////////////////////////////////////////////////////////////
	CString olNafrd,olNafrt,olNatod,olNatot;
	m_NAFRD.GetWindowText(olNafrd);
	m_NAFRT.GetWindowText(olNafrt);
	m_NATOD.GetWindowText(olNatod);
	m_NATOT.GetWindowText(olNatot);

	if(m_NAFRD.GetStatus() == true && m_NAFRT.GetStatus() == true && m_NATOD.GetStatus() == true && m_NATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olNafrd,olNafrt);
		olTmpTimeTo = DateHourStringToDate(olNatod,olNatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo <= olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING32811);
		}
	}



	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olPdid;
		char clWhere[100];
		CCSPtrArray<PDADATA> olPdaCPA;

		m_PDID.GetWindowText(olPdid);
		sprintf(clWhere,"WHERE PDID='%s'",olPdid);
		if(ogPdaData.ReadSpecialData(&olPdaCPA,clWhere,"URNO,PDID",false) == true)
		{
			if(olPdaCPA[0].Urno != pomPda->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING32810) + LoadStg(IDS_EXIST);
			}
		}
		olPdaCPA.DeleteAll();
	}

	CString olTmp;
	CString olUstf;

	m_USTF.GetWindowText(olTmp);


	CStringArray olStrArray;
	ExtractItemList(olTmp, &olStrArray,',');

	if(olStrArray.GetSize() >= 4)
	{
		olUstf = olStrArray[3];
	}


	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_PDID.GetWindowText(pomPda->Pdid,6);
		pomPda->Vafr = DateHourStringToDate(olVpfrd,olVpfrt);
		pomPda->Vato = DateHourStringToDate(olVptod,olVptot);
		pomPda->Nafr = DateHourStringToDate(olNafrd,olNafrt);
		pomPda->Nato = DateHourStringToDate(olNatod,olNatot);

		pomPda->Ustf = atoi(olUstf); 

		BYTE Byte1; 
		BYTE Byte2; 
		BYTE Byte3; 
		BYTE Byte4; 
		m_IPAD.GetAddress(Byte1,Byte2,Byte3,Byte4);

		sprintf(pomPda->Ipad, "%u.%u.%u.%u", Byte1,Byte2,Byte3,Byte4);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_PDID.SetFocus();
		m_PDID.SetSel(0,-1);
  }

}

//----------------------------------------------------------------------------------------


