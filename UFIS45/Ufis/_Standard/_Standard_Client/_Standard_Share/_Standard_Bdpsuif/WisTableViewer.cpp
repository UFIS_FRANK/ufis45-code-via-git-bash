// WisTableViewer.cpp 
//

#include <stdafx.h>
#include <WisTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void WisTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// WisTableViewer
//

WisTableViewer::WisTableViewer(CCSPtrArray<WISDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomWisTable = NULL;
    ogDdx.Register(this, WIS_CHANGE, CString("WISTABLEVIEWER"), CString("Wis Update"), WisTableCf);
    ogDdx.Register(this, WIS_NEW,    CString("WISTABLEVIEWER"), CString("Wis New"),    WisTableCf);
    ogDdx.Register(this, WIS_DELETE, CString("WISTABLEVIEWER"), CString("Wis Delete"), WisTableCf);
}

//-----------------------------------------------------------------------------------------------

WisTableViewer::~WisTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void WisTableViewer::Attach(CCSTable *popTable)
{
    pomWisTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void WisTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void WisTableViewer::MakeLines()
{
	int ilWisCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilWisCount; ilLc++)
	{
		WISDATA *prlWisData = &pomData->GetAt(ilLc);
		MakeLine(prlWisData);
	}
}

//-----------------------------------------------------------------------------------------------

void WisTableViewer::MakeLine(WISDATA *prpWis)
{

    //if( !IsPassFilter(prpWis)) return;

    // Update viewer data for this shift record
    WISTABLE_LINEDATA rlWis;

	rlWis.Urno = prpWis->Urno;
	rlWis.Cdat = prpWis->Cdat;
	rlWis.Lstu = prpWis->Lstu;
	rlWis.Orgc = prpWis->Orgc;
	rlWis.Wisc = prpWis->Wisc;
	rlWis.Wisd = prpWis->Wisd;
	rlWis.Shir = prpWis->Shir;
	rlWis.Rema = prpWis->Rema;
	rlWis.Usec = prpWis->Usec;
	rlWis.Useu = prpWis->Useu;

	CreateLine(&rlWis);
}

//-----------------------------------------------------------------------------------------------

void WisTableViewer::CreateLine(WISTABLE_LINEDATA *prpWis)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareWis(prpWis, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	WISTABLE_LINEDATA rlWis;
	rlWis = *prpWis;
    omLines.NewAt(ilLineno, rlWis);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void WisTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 4;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomWisTable->SetShowSelection(TRUE);
	pomWisTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 70; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING762),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 70; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING762),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 300; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING762),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 20; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING762),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 300; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING762),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);



	pomWisTable->SetHeaderFields(omHeaderDataArray);
	pomWisTable->SetDefaultSeparator();
	pomWisTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Orgc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Wisc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Wisd;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Shir;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomWisTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomWisTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString WisTableViewer::Format(WISTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool WisTableViewer::FindWis(char *pcpWisKeya, char *pcpWisKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpWisKeya) &&
			 (omLines[ilItem].Keyd == pcpWisKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void WisTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    WisTableViewer *polViewer = (WisTableViewer *)popInstance;
    if (ipDDXType == WIS_CHANGE || ipDDXType == WIS_NEW) polViewer->ProcessWisChange((WISDATA *)vpDataPointer);
    if (ipDDXType == WIS_DELETE) polViewer->ProcessWisDelete((WISDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void WisTableViewer::ProcessWisChange(WISDATA *prpWis)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpWis->Orgc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpWis->Wisc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpWis->Wisd;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpWis->Shir;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpWis->Rema;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	if (FindLine(prpWis->Urno, ilItem))
	{
        WISTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Orgc = prpWis->Orgc;
		prlLine->Wisc = prpWis->Wisc;
		prlLine->Wisd = prpWis->Wisd;
		prlLine->Shir = prpWis->Shir;
		prlLine->Rema = prpWis->Rema;
		prlLine->Cdat = prpWis->Cdat;
		prlLine->Lstu = prpWis->Lstu;
		prlLine->Usec = prpWis->Usec;
		prlLine->Useu = prpWis->Useu;
		prlLine->Urno = prpWis->Urno;

		pomWisTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomWisTable->DisplayTable();
	}
	else
	{
		MakeLine(prpWis);
		if (FindLine(prpWis->Urno, ilItem))
		{
	        WISTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomWisTable->AddTextLine(olLine, (void *)prlLine);
				pomWisTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void WisTableViewer::ProcessWisDelete(WISDATA *prpWis)
{
	int ilItem;
	if (FindLine(prpWis->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomWisTable->DeleteTextLine(ilItem);
		pomWisTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool WisTableViewer::IsPassFilter(WISDATA *prpWis)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int WisTableViewer::CompareWis(WISTABLE_LINEDATA *prpWis1, WISTABLE_LINEDATA *prpWis2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpWis1->Tifd == prpWis2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpWis1->Tifd > prpWis2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void WisTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool WisTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void WisTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void WisTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING757);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool WisTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = 5;// omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool WisTableViewer::PrintTableLine(WISTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = 5; // omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Orgc;
				break;
			case 1:
				rlElement.Text		= prpLine->Wisc;
				break;
			case 2:
				rlElement.Text		= prpLine->Wisd;
				break;
			case 3:
				rlElement.Text		= prpLine->Shir;
				break;
			case 4:
				rlElement.FrameRight  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
