// ReduktionenDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <ReduktionenDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ReduktionenDlg dialog


ReduktionenDlg::ReduktionenDlg(PRCDATA *popPrc, CWnd* pParent /*=NULL*/)
	: CDialog(ReduktionenDlg::IDD, pParent)
{
	pomPrc = popPrc;
	//{{AFX_DATA_INIT(ReduktionenDlg)
	//}}AFX_DATA_INIT
}


void ReduktionenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ReduktionenDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_REDC, m_REDC);
	DDX_Control(pDX, IDC_REDN, m_REDN);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
	DDX_Control(pDX, IDC_REDL, m_REDL);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ReduktionenDlg, CDialog)
	//{{AFX_MSG_MAP(ReduktionenDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ReduktionenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL ReduktionenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_Caption = LoadStg(IDS_STRING187) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("REDUKTIONENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomPrc->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("REDUKTIONENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomPrc->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("REDUKTIONENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomPrc->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("REDUKTIONENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomPrc->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("REDUKTIONENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomPrc->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("REDUKTIONENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomPrc->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("REDUKTIONENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_REDC.SetFormat("x|#x|#x|#x|#x|#");
	m_REDC.SetTextLimit(1,5);
	m_REDC.SetBKColor(YELLOW);  
	m_REDC.SetTextErrColor(RED);
	m_REDC.SetInitText(pomPrc->Redc);
	m_REDC.SetSecState(ogPrivList.GetStat("REDUKTIONENDLG.m_REDC"));
	//------------------------------------
	m_REDL.SetFormat("##");
	m_REDL.SetTextLimit(1,2);
	m_REDL.SetBKColor(YELLOW);  
	m_REDL.SetTextErrColor(RED);
	char plcRedl[10]="";
	sprintf(plcRedl, "%ld", pomPrc->Redl);
	m_REDL.SetInitText(plcRedl);
	m_REDL.SetSecState(ogPrivList.GetStat("REDUKTIONENDLG.m_REDL"));
	//------------------------------------
	m_REDN.SetTypeToString("X(40)",40,1);
	m_REDN.SetBKColor(YELLOW);  
	m_REDN.SetTextErrColor(RED);
	m_REDN.SetInitText(pomPrc->Redn);
	m_REDN.SetSecState(ogPrivList.GetStat("REDUKTIONENDLG.m_REDN"));
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void ReduktionenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_REDC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_REDC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_REDL.GetStatus() == false)
	{
		ilStatus = false;
		if(m_REDL.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING97) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING97) + ogNotFormat;
		}
	}
	if(m_REDN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_REDN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olRedc,olRedl;
		CCSPtrArray<PRCDATA> olPrcCPA;
		char clWhere[100];
		m_REDC.GetWindowText(olRedc);
		m_REDL.GetWindowText(olRedl);
		sprintf(clWhere,"WHERE REDC='%s' AND REDL = '%s'",olRedc, olRedl);
		if(ogPrcData.ReadSpecialData(&olPrcCPA,clWhere,"URNO",false) == true)
		{
			if(olPrcCPA[0].Urno != pomPrc->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING98);
			}
		}
		olPrcCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		CString olText;
		m_REDC.GetWindowText(pomPrc->Redc,6);
		m_REDL.GetWindowText(olText);
		pomPrc->Redl = atoi(olText);
		m_REDN.GetWindowText(pomPrc->Redn,41);
		strcpy(pomPrc->Appl, "RMS");

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_REDC.SetFocus();
	}	

	
}
//----------------------------------------------------------------------------------------
