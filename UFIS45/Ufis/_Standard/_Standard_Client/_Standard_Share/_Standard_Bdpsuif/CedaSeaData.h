// CedaSeaData.h

#ifndef __CEDASEADATA__
#define __CEDASEADATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct SEADATA 
{
	char 	 Beme[22]; 	// Bemerkung
	CTime 	 Cdat; 		// Erstellungsdatum
	CTime 	 Lstu; 		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Seas[8]; 	// Name der Season
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime 	 Vpfr; 		// G�ltig von
	CTime 	 Vpto; 		// G�ltig bis

	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	SEADATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
		Vpfr		=	TIMENULL;
		Vpto		=	TIMENULL;
	}

}; // end SeaDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSeaData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SEADATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaSeaData();
	~CedaSeaData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SEADATA *prpSea);
	bool InsertInternal(SEADATA *prpSea);
	bool Update(SEADATA *prpSea);
	bool UpdateInternal(SEADATA *prpSea);
	bool Delete(long lpUrno);
	bool DeleteInternal(SEADATA *prpSea);
	bool ReadSpecialData(CCSPtrArray<SEADATA> *popSea,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool Save(SEADATA *prpSea);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SEADATA  *GetSeaByUrno(long lpUrno);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795

	// Private methods
private:
    void PrepareSeaData(SEADATA *prpSeaData);
	CString omWhere; //Prf: 8795
	bool ValidateSeaBcData(const long& lrpUnro); //Prf: 8795

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDASEADATA__
