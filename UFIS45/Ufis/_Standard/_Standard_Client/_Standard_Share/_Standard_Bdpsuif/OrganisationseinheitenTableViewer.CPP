// OrganisationseinheitenTableViewer.cpp 
//

#include <stdafx.h>
#include <OrganisationseinheitenTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void OrganisationseinheitenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// OrganisationseinheitenTableViewer
//

OrganisationseinheitenTableViewer::OrganisationseinheitenTableViewer(CCSPtrArray<ORGDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomOrganisationseinheitenTable = NULL;
    ogDdx.Register(this, ORG_CHANGE, CString("ORGANISATIONSEINHEITENTABLEVIEWER"), CString("Organisationseinheiten Update"), OrganisationseinheitenTableCf);
    ogDdx.Register(this, ORG_NEW,    CString("ORGANISATIONSEINHEITENTABLEVIEWER"), CString("Organisationseinheiten New"),    OrganisationseinheitenTableCf);
    ogDdx.Register(this, ORG_DELETE, CString("ORGANISATIONSEINHEITENTABLEVIEWER"), CString("Organisationseinheiten Delete"), OrganisationseinheitenTableCf);
}

//-----------------------------------------------------------------------------------------------

OrganisationseinheitenTableViewer::~OrganisationseinheitenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void OrganisationseinheitenTableViewer::Attach(CCSTable *popTable)
{
    pomOrganisationseinheitenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void OrganisationseinheitenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void OrganisationseinheitenTableViewer::MakeLines()
{
	int ilOrganisationseinheitenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilOrganisationseinheitenCount; ilLc++)
	{
		ORGDATA *prlOrganisationseinheitenData = &pomData->GetAt(ilLc);
		MakeLine(prlOrganisationseinheitenData);
	}
}

//-----------------------------------------------------------------------------------------------

void OrganisationseinheitenTableViewer::MakeLine(ORGDATA *prpOrganisationseinheiten)
{

    //if( !IsPassFilter(prpOrganisationseinheiten)) return;

    // Update viewer data for this shift record
    ORGANISATIONSEINHEITENTABLE_LINEDATA rlOrganisationseinheiten;

	rlOrganisationseinheiten.Urno = prpOrganisationseinheiten->Urno; 
	rlOrganisationseinheiten.Dpt1 = prpOrganisationseinheiten->Dpt1; 
	rlOrganisationseinheiten.Dpt2 = prpOrganisationseinheiten->Dpt2; 
	rlOrganisationseinheiten.Dptn = prpOrganisationseinheiten->Dptn; 
	//uhi 26.3.01
	rlOrganisationseinheiten.Odgl = GetHHMMFromMinutes(prpOrganisationseinheiten->Odgl, true); 
	rlOrganisationseinheiten.Odkl = GetHHMMFromMinutes(prpOrganisationseinheiten->Odkl, true); 
	rlOrganisationseinheiten.Odsl = GetHHMMFromMinutes(prpOrganisationseinheiten->Odsl, true);

	rlOrganisationseinheiten.Rema = prpOrganisationseinheiten->Rema; 

	CreateLine(&rlOrganisationseinheiten);
}

//-----------------------------------------------------------------------------------------------

void OrganisationseinheitenTableViewer::CreateLine(ORGANISATIONSEINHEITENTABLE_LINEDATA *prpOrganisationseinheiten)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareOrganisationseinheiten(prpOrganisationseinheiten, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	ORGANISATIONSEINHEITENTABLE_LINEDATA rlOrganisationseinheiten;
	rlOrganisationseinheiten = *prpOrganisationseinheiten;
    omLines.NewAt(ilLineno, rlOrganisationseinheiten);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void OrganisationseinheitenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 3;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomOrganisationseinheitenTable->SetShowSelection(TRUE);
	pomOrganisationseinheitenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 65; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING275),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 65; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING275),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING275),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	//uhi 26.3.01
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING275),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING275),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING275),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 498; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING275),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomOrganisationseinheitenTable->SetHeaderFields(omHeaderDataArray);
	pomOrganisationseinheitenTable->SetDefaultSeparator();
	pomOrganisationseinheitenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Dpt1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dpt2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dptn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		//uhi 26.3.01
		rlColumnData.Text = omLines[ilLineNo].Odgl;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Odkl;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Odsl;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomOrganisationseinheitenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomOrganisationseinheitenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString OrganisationseinheitenTableViewer::Format(ORGANISATIONSEINHEITENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool OrganisationseinheitenTableViewer::FindOrganisationseinheiten(char *pcpOrganisationseinheitenKeya, char *pcpOrganisationseinheitenKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpOrganisationseinheitenKeya) &&
			 (omLines[ilItem].Keyd == pcpOrganisationseinheitenKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void OrganisationseinheitenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    OrganisationseinheitenTableViewer *polViewer = (OrganisationseinheitenTableViewer *)popInstance;
    if (ipDDXType == ORG_CHANGE || ipDDXType == ORG_NEW) polViewer->ProcessOrganisationseinheitenChange((ORGDATA *)vpDataPointer);
    if (ipDDXType == ORG_DELETE) polViewer->ProcessOrganisationseinheitenDelete((ORGDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void OrganisationseinheitenTableViewer::ProcessOrganisationseinheitenChange(ORGDATA *prpOrganisationseinheiten)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpOrganisationseinheiten->Dpt1;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpOrganisationseinheiten->Dpt2;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpOrganisationseinheiten->Dptn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	//uhi 26.3.01
	rlColumn.Text = GetHHMMFromMinutes(prpOrganisationseinheiten->Odgl, true);
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = GetHHMMFromMinutes(prpOrganisationseinheiten->Odkl, true);
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = GetHHMMFromMinutes(prpOrganisationseinheiten->Odsl, true);
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpOrganisationseinheiten->Rema;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpOrganisationseinheiten->Urno, ilItem))
	{
        ORGANISATIONSEINHEITENTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpOrganisationseinheiten->Urno;
		prlLine->Dpt1 = prpOrganisationseinheiten->Dpt1;
		prlLine->Dpt2 = prpOrganisationseinheiten->Dpt2;
		prlLine->Dptn = prpOrganisationseinheiten->Dptn;
		
		//uhi 26.3.01
		prlLine->Odgl = GetHHMMFromMinutes(prpOrganisationseinheiten->Odgl, true);
		prlLine->Odkl = GetHHMMFromMinutes(prpOrganisationseinheiten->Odkl, true);
		prlLine->Odsl = GetHHMMFromMinutes(prpOrganisationseinheiten->Odsl, true);

		prlLine->Rema = prpOrganisationseinheiten->Rema;

		pomOrganisationseinheitenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomOrganisationseinheitenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpOrganisationseinheiten);
		if (FindLine(prpOrganisationseinheiten->Urno, ilItem))
		{
	        ORGANISATIONSEINHEITENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomOrganisationseinheitenTable->AddTextLine(olLine, (void *)prlLine);
				pomOrganisationseinheitenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void OrganisationseinheitenTableViewer::ProcessOrganisationseinheitenDelete(ORGDATA *prpOrganisationseinheiten)
{
	int ilItem;
	if (FindLine(prpOrganisationseinheiten->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomOrganisationseinheitenTable->DeleteTextLine(ilItem);
		pomOrganisationseinheitenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool OrganisationseinheitenTableViewer::IsPassFilter(ORGDATA *prpOrganisationseinheiten)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int OrganisationseinheitenTableViewer::CompareOrganisationseinheiten(ORGANISATIONSEINHEITENTABLE_LINEDATA *prpOrganisationseinheiten1, ORGANISATIONSEINHEITENTABLE_LINEDATA *prpOrganisationseinheiten2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpOrganisationseinheiten1->Tifd == prpOrganisationseinheiten2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpOrganisationseinheiten1->Tifd > prpOrganisationseinheiten2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void OrganisationseinheitenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool OrganisationseinheitenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void OrganisationseinheitenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void OrganisationseinheitenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING184);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool OrganisationseinheitenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool OrganisationseinheitenTableViewer::PrintTableLine(ORGANISATIONSEINHEITENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Dpt1;
				break;
			case 1:
				rlElement.Text		= prpLine->Dpt2;
				break;
			case 2:
				rlElement.Text		= prpLine->Dptn;
				break;
			
			//uhi 26.3.01
			case 3:
				rlElement.Text		= prpLine->Odgl;
				break;
			case 4:
				rlElement.Text		= prpLine->Odkl;
				break;
			case 5:
				rlElement.Text		= prpLine->Odsl;
				break;

			case 6:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
