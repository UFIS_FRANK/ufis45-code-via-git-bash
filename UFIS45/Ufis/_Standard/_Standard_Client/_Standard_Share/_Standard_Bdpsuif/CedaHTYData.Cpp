// CedaHTYData.cpp
 
#include <stdafx.h>
#include <CedaHTYData.h>
#include <resource.h>


// Local function prototype
static void ProcessHTYCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaHTYData::CedaHTYData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for HTYDATA
	BEGIN_CEDARECINFO(HTYDATA,HTYDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Hnam,"HNAM")
		FIELD_CHAR_TRIM	(Htyp,"HTYP")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Adid,"ADID")
	END_CEDARECINFO //(HTYDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(HTYDataRecInfo)/sizeof(HTYDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&HTYDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"HTY");
	strcpy(pcmHTYFieldList,"CDAT,LSTU,PRFL,HNAM,HTYP,URNO,USEC,USEU,VAFR,VATO");
	pcmFieldList = pcmHTYFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//----------------------------------------------------------------------------------------------------

void CedaHTYData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("HNAM");
	ropFields.Add("HTYP");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING448));
	ropDesription.Add(LoadStg(IDS_STRING15));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");

}

//----------------------------------------------------------------------------------------------------

void CedaHTYData::Register(void)
{
	ogDdx.Register((void *)this,BC_HTY_CHANGE,CString("HTYDATA"), CString("HTY-changed"),ProcessHTYCf);
	ogDdx.Register((void *)this,BC_HTY_DELETE,CString("HTYDATA"), CString("HTY-deleted"),ProcessHTYCf);


	if (ogBasicData.HasHtytabAdid())
		strcpy(pcmHTYFieldList,"CDAT,LSTU,PRFL,HNAM,HTYP,URNO,USEC,USEU,VAFR,VATO,ADID");


}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaHTYData::~CedaHTYData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaHTYData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaHTYData::Read(char *pcpWhere)
{
	if (ogBasicData.HasHtytabAdid())
		strcpy(pcmHTYFieldList,"CDAT,LSTU,PRFL,HNAM,HTYP,URNO,USEC,USEU,VAFR,VATO,ADID");


    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		HTYDATA *prpHTY = new HTYDATA;
		if ((ilRc = GetFirstBufferRecord(prpHTY)) == true)
		{
			prpHTY->IsChanged = DATA_UNCHANGED;
			omData.Add(prpHTY);//Update omData
			omUrnoMap.SetAt((void *)prpHTY->Urno,prpHTY);
		}
		else
		{
			delete prpHTY;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaHTYData::InsertHTY(HTYDATA *prpHTY,BOOL bpSendDdx)
{
	prpHTY->IsChanged = DATA_NEW;
	if(SaveHTY(prpHTY) == false) return false; //Update Database
	InsertHTYInternal(prpHTY);
    return true;
}

//--INSERT-INTERNAL---------------------------------------------------------------------------------------

bool CedaHTYData::InsertHTYInternal(HTYDATA *prpHTY)
{
	//PrepareHTYData(prpHTY);
	ogDdx.DataChanged((void *)this, HTY_CHANGE,(void *)prpHTY ); //Update Viewer
	omData.Add(prpHTY);//Update omData
	omUrnoMap.SetAt((void *)prpHTY->Urno,prpHTY);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaHTYData::DeleteHTY(long lpUrno)
{
	HTYDATA *prlHTY = GetHTYByUrno(lpUrno);
	if (prlHTY != NULL)
	{
		prlHTY->IsChanged = DATA_DELETED;
		if(SaveHTY(prlHTY) == false) return false; //Update Database
		DeleteHTYInternal(prlHTY);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaHTYData::DeleteHTYInternal(HTYDATA *prpHTY)
{
	ogDdx.DataChanged((void *)this,HTY_DELETE,(void *)prpHTY); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpHTY->Urno);
	int ilHTYCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilHTYCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpHTY->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaHTYData::PrepareHTYData(HTYDATA *prpHTY)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaHTYData::UpdateHTY(HTYDATA *prpHTY,BOOL bpSendDdx)
{
	if (GetHTYByUrno(prpHTY->Urno) != NULL)
	{
		if (prpHTY->IsChanged == DATA_UNCHANGED)
		{
			prpHTY->IsChanged = DATA_CHANGED;
		}
		if(SaveHTY(prpHTY) == false) return false; //Update Database
		UpdateHTYInternal(prpHTY);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaHTYData::UpdateHTYInternal(HTYDATA *prpHTY)
{
	HTYDATA *prlHTY = GetHTYByUrno(prpHTY->Urno);
	if (prlHTY != NULL)
	{
		*prlHTY = *prpHTY; //Update omData
		ogDdx.DataChanged((void *)this,HTY_CHANGE,(void *)prlHTY); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

HTYDATA *CedaHTYData::GetHTYByUrno(long lpUrno)
{
	HTYDATA  *prlHTY;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlHTY) == TRUE)
	{
		return prlHTY;
	}
	return NULL;
	
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaHTYData::ReadSpecialData(CCSPtrArray<HTYDATA> *popHty,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","HTY",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","HTY",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popHty != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			HTYDATA *prpHty = new HTYDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpHty,CString(pclFieldList))) == true)
			{
				popHty->Add(prpHty);
			}
			else
			{
				delete prpHty;
			}
		}
		if(popHty->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaHTYData::SaveHTY(HTYDATA *prpHTY)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpHTY->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpHTY->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpHTY);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpHTY->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpHTY->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpHTY);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpHTY->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpHTY->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessHTYCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_HTY_CHANGE :
	case BC_HTY_DELETE :
		((CedaHTYData *)popInstance)->ProcessHTYBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaHTYData::ProcessHTYBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlHTYData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlHTYData->Selection);

	HTYDATA *prlHTY = GetHTYByUrno(llUrno);
	if(ipDDXType == BC_HTY_CHANGE)
	{
		if (prlHTY != NULL)
		{
			GetRecordFromItemList(prlHTY,prlHTYData->Fields,prlHTYData->Data);
			if(ValidateHTYBcData(prlHTY->Urno)) //Prf: 8795
			{
				UpdateHTYInternal(prlHTY);
			}
			else
			{
				DeleteHTYInternal(prlHTY);
			}
		}
		else
		{
			prlHTY = new HTYDATA;
			GetRecordFromItemList(prlHTY,prlHTYData->Fields,prlHTYData->Data);
			if(ValidateHTYBcData(prlHTY->Urno)) //Prf: 8795
			{
				InsertHTYInternal(prlHTY);
			}
			else
			{
				delete prlHTY;
			}
		}
	}
	if(ipDDXType == BC_HTY_DELETE)
	{
		if (prlHTY != NULL)
		{
			DeleteHTYInternal(prlHTY);
		}
	}
}

//Prf: 8795
//--ValidateHTYBcData--------------------------------------------------------------------------------------

bool CedaHTYData::ValidateHTYBcData(const long& lrpUrno)
{
	bool blValidateHTYBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<HTYDATA> olHtys;
		if(!ReadSpecialData(&olHtys,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateHTYBcData = false;
		}
	}
	return blValidateHTYBcData;
}

//---------------------------------------------------------------------------------------------------------
