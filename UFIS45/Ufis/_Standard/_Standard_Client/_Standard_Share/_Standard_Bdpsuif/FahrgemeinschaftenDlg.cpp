// FahrgemeinschaftenDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <FahrgemeinschaftenDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FahrgemeinschaftenDlg dialog


FahrgemeinschaftenDlg::FahrgemeinschaftenDlg(TEADATA *popTea, CWnd* pParent /*=NULL*/)
	: CDialog(FahrgemeinschaftenDlg::IDD, pParent)
{
	pomTea = popTea;
	//{{AFX_DATA_INIT(FahrgemeinschaftenDlg)
	//}}AFX_DATA_INIT
}


void FahrgemeinschaftenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FahrgemeinschaftenDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_FGMC, m_FGMC);
	DDX_Control(pDX, IDC_FGMN, m_FGMN);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_REMA, m_REMA);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FahrgemeinschaftenDlg, CDialog)
	//{{AFX_MSG_MAP(FahrgemeinschaftenDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FahrgemeinschaftenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL FahrgemeinschaftenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_Caption = LoadStg(IDS_STRING171) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomTea->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomTea->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomTea->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomTea->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomTea->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomTea->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_FGMC.SetFormat("x|#x|#x|#x|#x|#");
	m_FGMC.SetTextLimit(1,5);
	m_FGMC.SetBKColor(YELLOW);  
	m_FGMC.SetTextErrColor(RED);
	m_FGMC.SetInitText(pomTea->Fgmc);
	m_FGMC.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_FGMC"));
	//------------------------------------
	m_FGMN.SetTypeToString("X(40)",40,1);
	m_FGMN.SetBKColor(YELLOW);  
	m_FGMN.SetTextErrColor(RED);
	m_FGMN.SetInitText(pomTea->Fgmn);
	m_FGMN.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_FGMN"));
	//------------------------------------
	m_REMA.SetTypeToString("X(60)",60,0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomTea->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_REMA"));
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void FahrgemeinschaftenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_FGMC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_FGMC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_FGMN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_FGMN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olText;//
		if(m_FGMC.GetWindowTextLength() != 0)
		{
			CCSPtrArray<TEADATA> olTeaCPA;
			char clWhere[100];
			m_FGMC.GetWindowText(olText);
			sprintf(clWhere,"WHERE FGMC='%s'",olText);
			if(ogTeaData.ReadSpecialData(&olTeaCPA,clWhere,"URNO",false) == true)
			{
				if(olTeaCPA[0].Urno != pomTea->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
				}
			}
			olTeaCPA.DeleteAll();
		}
	}
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_FGMC.GetWindowText(pomTea->Fgmc,6);
		m_FGMN.GetWindowText(pomTea->Fgmn,40);
		//wandelt Return in Blank//
		CString olTemp;
		m_REMA.GetWindowText(olTemp);
		for(int i=0;i>-1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomTea->Rema,olTemp);
		////////////////////////////

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_FGMC.SetFocus();
	}	

	
}

//----------------------------------------------------------------------------------------
