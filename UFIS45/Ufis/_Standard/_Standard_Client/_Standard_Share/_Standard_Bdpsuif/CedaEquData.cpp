// CedaEquData.cpp
 
#include <stdafx.h>
#include <CedaEquData.h>
#include <resource.h>


void ProcessEquCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaEquData::CedaEquData() : CCSCedaData(&ogCommHandler)
{ 
	// Create an array of CEDARECINFO for EQUDataStruct
	BEGIN_CEDARECINFO(EQUDATA,EQUDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Enam,"ENAM")
		FIELD_CHAR_TRIM	(Eqps,"EQPS")
		FIELD_CHAR_TRIM	(Etyp,"ETYP")
		FIELD_CHAR_TRIM	(Gcde,"GCDE")
		FIELD_LONG		(Gkey,"GKEY")
		FIELD_CHAR_TRIM	(Hopo,"HOPO")
		FIELD_CHAR_TRIM	(Ivnr,"IVNR")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Tele,"TELE")
		FIELD_CHAR_TRIM	(Crqu,"CRQU")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
	END_CEDARECINFO //(EQUDataStruct)	

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(EQUDataRecInfo)/sizeof(EQUDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&EQUDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"EQU");
	//~~~~~ 13.08.99 SHA : 
	//*** ATTENTION FIELDS TATP AND TSAP REMOVED TEMPOR. !!!! ***
	strcpy(pcmListOfFields,"CDAT,ENAM,EQPS,ETYP,GCDE,GKEY,HOPO,IVNR,LSTU,REMA,TELE,CRQU,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaEquData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("ENAM");
	ropFields.Add("EQPS");
	ropFields.Add("GCDE");
	ropFields.Add("GKEY");
	ropFields.Add("INVR");
	ropFields.Add("LSTU");
	ropFields.Add("REMA");
	ropFields.Add("TELE");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING859));
	ropDesription.Add(LoadStg(IDS_STRING860));
	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING862));
	ropDesription.Add(LoadStg(IDS_STRING863));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING805));
	ropDesription.Add(LoadStg(IDS_STRING864));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaEquData::Register(void)
{
	ogDdx.Register((void *)this,BC_EQU_CHANGE,	CString("EQUDATA"), CString("EQU-changed"),	ProcessEquCf);
	ogDdx.Register((void *)this,BC_EQU_NEW,		CString("EQUDATA"), CString("EQU-new"),		ProcessEquCf);
	ogDdx.Register((void *)this,BC_EQU_DELETE,	CString("EQUDATA"), CString("EQU-deleted"),	ProcessEquCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaEquData::~CedaEquData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaEquData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaEquData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		EQUDATA *prlEqu = new EQUDATA;
		if ((ilRc = GetFirstBufferRecord(prlEqu)) == true)
		{
			omData.Add(prlEqu);//Update omData
			omUrnoMap.SetAt((void *)prlEqu->Urno,prlEqu);
		}
		else
		{
			delete prlEqu;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaEquData::Insert(EQUDATA *prpEqu)
{
	prpEqu->IsChanged = DATA_NEW;
	if(Save(prpEqu) == false) return false; //Update Database
	InsertInternal(prpEqu);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaEquData::InsertInternal(EQUDATA *prpEqu)
{
	ogDdx.DataChanged((void *)this, EQU_NEW,(void *)prpEqu ); //Update Viewer
	omData.Add(prpEqu);//Update omData
	omUrnoMap.SetAt((void *)prpEqu->Urno,prpEqu);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaEquData::Delete(long lpUrno)
{
	EQUDATA *prlEqu = GetEquByUrno(lpUrno);
	if (prlEqu != NULL)
	{
		prlEqu->IsChanged = DATA_DELETED;
		if(Save(prlEqu) == false) return false; //Update Database
		DeleteInternal(prlEqu);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaEquData::DeleteInternal(EQUDATA *prpEqu)
{
	ogDdx.DataChanged((void *)this,EQU_DELETE,(void *)prpEqu); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpEqu->Urno);
	int ilEquCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilEquCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpEqu->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaEquData::Update(EQUDATA *prpEqu)
{
	if (GetEquByUrno(prpEqu->Urno) != NULL)
	{
		if (prpEqu->IsChanged == DATA_UNCHANGED)
		{
			prpEqu->IsChanged = DATA_CHANGED;
		}
		if(Save(prpEqu) == false) return false; //Update Database
		UpdateInternal(prpEqu);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaEquData::UpdateInternal(EQUDATA *prpEqu)
{
	EQUDATA *prlEqu = GetEquByUrno(prpEqu->Urno);
	if (prlEqu != NULL)
	{
		*prlEqu = *prpEqu; //Update omData
		ogDdx.DataChanged((void *)this,EQU_CHANGE,(void *)prlEqu); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

EQUDATA *CedaEquData::GetEquByUrno(long lpUrno)
{
	EQUDATA  *prlEqu;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlEqu) == TRUE)
	{
		return prlEqu;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaEquData::ReadSpecialData(CCSPtrArray<EQUDATA> *popEqu,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","EQU",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","EQU",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popEqu != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			EQUDATA *prpEqu = new EQUDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpEqu,CString(pclFieldList))) == true)
			{
				popEqu->Add(prpEqu);
			}
			else
			{
				delete prpEqu;
			}
		}
		if(popEqu->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaEquData::Save(EQUDATA *prpEqu)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpEqu->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpEqu->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpEqu);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpEqu->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpEqu->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpEqu);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpEqu->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpEqu->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessEquCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogEquData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaEquData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlEquData;
	prlEquData = (struct BcStruct *) vpDataPointer;
	EQUDATA *prlEqu;
	if(ipDDXType == BC_EQU_NEW)
	{
		prlEqu = new EQUDATA;
		GetRecordFromItemList(prlEqu,prlEquData->Fields,prlEquData->Data);
		if(ValidateEquBcData(prlEqu->Urno)) //Prf: 8795
		{
			InsertInternal(prlEqu);
		}
		else
		{
			delete prlEqu;
		}
	}
	if(ipDDXType == BC_EQU_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlEquData->Selection);
		prlEqu = GetEquByUrno(llUrno);
		if(prlEqu != NULL)
		{
			GetRecordFromItemList(prlEqu,prlEquData->Fields,prlEquData->Data);
			if(ValidateEquBcData(prlEqu->Urno)) //Prf: 8795
			{
				UpdateInternal(prlEqu);
			}
			else
			{
				DeleteInternal(prlEqu);
			}
		}
	}
	if(ipDDXType == BC_EQU_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlEquData->Selection);

		prlEqu = GetEquByUrno(llUrno);
		if (prlEqu != NULL)
		{
			DeleteInternal(prlEqu);
		}
	}
}

//Prf: 8795
//--ValidateEquBcData--------------------------------------------------------------------------------------

bool CedaEquData::ValidateEquBcData(const long& lrpUrno)
{
	bool blValidateEquBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<EQUDATA> olEqus;
		if(!ReadSpecialData(&olEqus,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateEquBcData = false;
		}
	}
	return blValidateEquBcData;
}

//---------------------------------------------------------------------------------------------------------
