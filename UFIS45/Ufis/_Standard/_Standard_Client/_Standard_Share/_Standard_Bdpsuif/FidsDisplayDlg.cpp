// FidsDisplayDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <FidsDisplayDlg.h>
#include <PrivList.h>
#include <AwDlg.h>
#include <CedaPAGData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FidsDisplayDlg dialog


FidsDisplayDlg::FidsDisplayDlg(DSPDATA *popDSP,const CString& ropCaption,CWnd* pParent /*=NULL*/)
	: CDialog(FidsDisplayDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(FidsDisplayDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomDSP	  = popDSP;
	m_Caption = ropCaption;
	pomTable = new CCSTable;
	ogPAGData.Read("ORDER BY PAGI");
}

FidsDisplayDlg::~FidsDisplayDlg()
{
	ogPAGData.ClearAll();
	delete pomTable;
}

void FidsDisplayDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FidsDisplayDlg)
	DDX_Control(pDX, IDC_EDIT_NO_OF_PAGES, m_DNOP);
	DDX_Control(pDX, IDC_EDIT_ALARM_PAGE, m_DAPN);
	DDX_Control(pDX, IDC_EDIT_DEFAULT_PAGE, m_DEMT);
	DDX_Control(pDX, IDC_EDIT_FIRST_PAGE, m_DFPN);
	DDX_Control(pDX, IDC_EDIT_NAME, m_DPID);
	DDX_Control(pDX, IDC_EDIT_DICF, m_DICF);

	DDX_Control(pDX, IDC_NOAVNEW, m_PAGENEW);
	DDX_Control(pDX, IDC_NOAVDEL, m_PAGEDEL);
	DDX_Control(pDX, IDC_PAGES,	  m_PAGEFRAME);


	DDX_Control(pDX, IDC_VAFR_D2, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T1, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D1, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T1, m_VATOT);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_REMA,   m_REMA);
	DDX_Control(pDX, IDCANCEL, m_CANCEL);
	DDX_Control(pDX, IDOK, m_OK);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FidsDisplayDlg, CDialog)
	//{{AFX_MSG_MAP(FidsDisplayDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVDEL, OnPageDel)
	ON_BN_CLICKED(IDC_NOAVNEW, OnPageNew)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FidsDisplayDlg message handlers

BOOL FidsDisplayDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_Caption = LoadStg(IDS_STRING930) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("DISPLAYDLG.m_OK");
	SetWndStatAll(clStat,m_OK);
	
	//------------------------------------
	m_DPID.SetFormat("X(12)");
	m_DPID.SetTextLimit(1,12,false);
	m_DPID.SetTextErrColor(RED);
	m_DPID.SetBKColor(YELLOW);
	m_DPID.SetInitText(pomDSP->Dpid);
	m_DPID.SetSecState(ogPrivList.GetStat("DISPLAYDLG.m_DPID"));

	//------------------------------------
	m_DNOP.SetFormat("#(3)");
	m_DNOP.SetTextLimit(1,3,false);
	m_DNOP.SetTextErrColor(RED);
	m_DNOP.SetBKColor(YELLOW);
	m_DNOP.SetInitText(pomDSP->Dnop);
	m_DNOP.SetSecState(ogPrivList.GetStat("DISPLAYDLG.m_DNOP"));
	//------------------------------------
	m_DFPN.SetFormat("#(3)");
	m_DFPN.SetTextLimit(1,3,false);
	m_DFPN.SetTextErrColor(RED);
	m_DFPN.SetBKColor(YELLOW);
	m_DFPN.SetInitText(pomDSP->Dfpn);
	m_DFPN.SetSecState(ogPrivList.GetStat("DISPLAYDLG.m_DFPN"));
	//------------------------------------
	m_DEMT.SetFormat("#(3)");
	m_DEMT.SetTextLimit(0,3,true);
	m_DEMT.SetTextErrColor(RED);
	m_DEMT.SetInitText(pomDSP->Demt);
	m_DEMT.SetSecState(ogPrivList.GetStat("DISPLAYDLG.m_DEMT"));
	//------------------------------------
	m_DAPN.SetFormat("#(3)");
	m_DAPN.SetTextLimit(0,3,true);
	m_DAPN.SetTextErrColor(RED);
	m_DAPN.SetInitText(pomDSP->Dapn);
	m_DAPN.SetSecState(ogPrivList.GetStat("DISPLAYDLG.m_DAPN"));
	//------------------------------------
	m_DICF.SetFormat("X(12)");
	m_DICF.SetTextLimit(0,12,true);
	m_DICF.SetTextErrColor(RED);
	m_DICF.SetInitText(pomDSP->Dicf);
	m_DICF.SetSecState(ogPrivList.GetStat("DISPLAYDLG.m_DICF"));

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);			
	m_CDATD.SetInitText(pomDSP->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomDSP->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomDSP->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomDSP->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomDSP->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomDSP->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_USEU"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomDSP->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomDSP->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomDSP->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomDSP->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("DEVICEDLG.m_VATO"));
	//------------------------------------

	if(ogDSPData.DoesRemarkCodeExist() == true)
	{
		m_REMA.SetTypeToString("X(512)",512,1);
		m_REMA.SetBKColor(WHITE);
		m_REMA.SetTextErrColor(RED);
		m_REMA.SetInitText(pomDSP->Rema);
		m_REMA.SetSecState(ogPrivList.GetStat("DISPLAYDLG.m_REMA"));
	}
	else
	{
		ResizeDialog();
	}
	//------------------------------------

	MakePageTable();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FidsDisplayDlg::OnOK() 
{
	// TODO: Add extra validation here
	CWaitCursor olWait;
	CString olErrorText;
	bool ilStatus = true;
	
	if(m_DPID.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DPID.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING940) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING940) + ogNotFormat;
		} 
	}

	if(m_DNOP.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DNOP.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING956) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING956) + ogNotFormat;
		} 
	}

	if(m_DFPN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DFPN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING957) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING957) + ogNotFormat;
		} 
	}

	if(m_DEMT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DEMT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING958) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING958) + ogNotFormat;
		} 
	}

	if(m_DAPN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DAPN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING959) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING959) + ogNotFormat;
		} 
	}

	if(m_DICF.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DICF.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING1019) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING1019) + ogNotFormat;
		} 
	}


	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	/////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olDpid;
		char clWhere[100];
		CCSPtrArray<DSPDATA> olDSPCPA;
		m_DPID.GetWindowText(olDpid);
		if (olDpid.GetLength() > 0)
		{
			sprintf(clWhere,"WHERE DPID='%s'",olDpid);
			if(ogDSPData.ReadSpecialData(&olDSPCPA,clWhere,"URNO,DPID",false) == true)
			{
				if(olDSPCPA[0].Urno != pomDSP->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
				}
			}
			olDSPCPA.DeleteAll();
		}

		CString olDnop;
		m_DNOP.GetWindowText(olDnop);
		if (atoi(olDnop) < 1 || atoi(olDnop) > 50)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING956) + ogNotFormat;
		}

		CString olDfpn;
		m_DFPN.GetWindowText(olDfpn);
		if (atoi(olDfpn) < 1 || atoi(olDfpn) > atoi(olDnop))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING957) + ogNotFormat;
		}

		CString olDemt;
		m_DEMT.GetWindowText(olDemt);
		if (olDemt.GetLength() > 0 && (atoi(olDemt) < 1 || atoi(olDemt) > atoi(olDnop)))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING958) + ogNotFormat;
		}

		CString olDapn;
		m_DAPN.GetWindowText(olDapn);
		if (olDapn.GetLength() > 0 && (atoi(olDapn) < 1 || atoi(olDapn) > atoi(olDnop)))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING959) + ogNotFormat;
		}

	}

	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_DPID.GetWindowText(pomDSP->Dpid,sizeof(pomDSP->Dpid));
		m_DNOP.GetWindowText(pomDSP->Dnop,sizeof(pomDSP->Dnop));
		m_DFPN.GetWindowText(pomDSP->Dfpn,sizeof(pomDSP->Dfpn));
		m_DEMT.GetWindowText(pomDSP->Demt,sizeof(pomDSP->Demt));
		m_DAPN.GetWindowText(pomDSP->Dapn,sizeof(pomDSP->Dapn));
		m_DICF.GetWindowText(pomDSP->Dicf,sizeof(pomDSP->Dicf));

		pomDSP->Vafr = DateHourStringToDate(olVafrd,olVafrt); 
		pomDSP->Vato = DateHourStringToDate(olVatod,olVatot);

		if(ogDSPData.DoesRemarkCodeExist() == true)
		{
			m_REMA.GetWindowText(pomDSP->Rema,sizeof(pomDSP->Rema));
		}


		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_DPID.SetFocus();
		m_DPID.SetSel(0,-1);

	}

}

void FidsDisplayDlg::MakePageTable()
{
	CRect olRectBorder;
	m_PAGEFRAME.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomTable->SetTableEditable(true);
    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);
	pomTable->SetShowSelection(TRUE);
	pomTable->SetIPEditModus(TRUE);
	pomTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 80; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING960),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING960),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING960),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	pomTable->SetHeaderFields(olHeaderDataArray);
	pomTable->SetDefaultSeparator();
	olHeaderDataArray.DeleteAll();

	int ilLines = atoi(pomDSP->Dnop);

	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(TRUE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;

	CCSEDIT_ATTRIB rlAttribC0;

	rlAttribC0.Format = "X(12)";
	rlAttribC0.TextMinLenght = 0;
	rlAttribC0.TextMaxLenght = 8;
	rlAttribC0.Style = ES_UPPERCASE;
	
	CCSEDIT_ATTRIB rlAttribC2;

	rlAttribC2.Format = "X(12)";
	rlAttribC2.TextMinLenght = 0;
	rlAttribC2.TextMaxLenght =12;
	rlAttribC2.Style = ES_UPPERCASE;
	
	CCSEDIT_ATTRIB rlAttribC1;

	rlAttribC1.Format = "#(2)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 8;
	rlAttribC1.Style = ES_UPPERCASE;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = this->PageIdFromIndex(ilLineNo);
		rlColumnData.EditAttrib = rlAttribC0;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = this->PageConfigFromIndex(ilLineNo);
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = this->PageNoFromIndex(ilLineNo);
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}

	pomTable->SetColumnEditable(0,false);
	pomTable->SetColumnEditable(1,false);
	pomTable->SetColumnEditable(2,true);
	pomTable->DisplayTable();
}

//----------------------------------------------------------------------------------------

void FidsDisplayDlg::ChangePageTable(char *popPageId,char *popPageConfig,char *popPageNo,int ipLineNo)
{
	if (popPageId != NULL && popPageNo != NULL)
	{
		TABLE_COLUMN rlColumn;
		CCSPtrArray <TABLE_COLUMN> olLine;

		rlColumn.Lineno = 0;
		rlColumn.VerticalSeparator = SEPA_NONE;
		rlColumn.SeparatorType = SEPA_NONE;
		rlColumn.BkColor = WHITE;
		rlColumn.Font = &ogCourier_Regular_10;
		rlColumn.Alignment = COLALIGN_LEFT;

		int ilColumnNo = 0;
		rlColumn.Text  = popPageId;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text  = popPageConfig; 
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		if (ipLineNo == -1)	// append
		{
			sprintf(popPageNo,"%d",pomTable->omLines.GetSize() + 1);	
		}

		rlColumn.Text  = popPageNo; 
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		if (ipLineNo > -1)
		{
			pomTable->ChangeTextLine(ipLineNo, &olLine,NULL);
			pomTable->DisplayTable();
		}
		else
		{
			pomTable->AddTextLine(olLine,NULL);
			ipLineNo = pomTable->omLines.GetSize() - 1; 
			pomTable->DisplayTable();
		}
		olLine.DeleteAll();

		EditPageNumber(ipLineNo,popPageNo);

	}
	else
	{
		if (ipLineNo > -1)
		{
			pomTable->DeleteTextLine(ipLineNo);
			pomTable->DisplayTable();
		}
	}
}

//----------------------------------------------------------------------------------------

LONG FidsDisplayDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(ogPrivList.GetStat("DISPLAYDLG.m_PAGECHA") == '1')
	{
		int ilLineNo = pomTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
		}
		else if (ilLineNo >= pomTable->omLines.GetSize())
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
		}
		else
		{
			char *polPageId = this->PageIdFromIndex(ilLineNo);
			char *polPageConfig = this->PageConfigFromIndex(ilLineNo);
			char *polPageNo = this->PageNoFromIndex(ilLineNo);
			if (polPageId != NULL && polPageNo != NULL)
			{
				if (this->SelectPage(polPageId))
				{
					ChangePageTable(polPageId,polPageConfig,polPageNo, ilLineNo);
				}

			}
		}
	}
	return 0L;
}

LONG FidsDisplayDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	if(prlNotify->Text.IsEmpty() || prlNotify->Status == false)
		return 0L;
	
	if(prlNotify->SourceTable == pomTable) 
	{
		if(prlNotify->Column == 2)
		{
			for (int i = 0; i < pomTable->omLines.GetSize(); i++)
			{
				if (strcmp(PageNoFromIndex(i),prlNotify->Text) == 0)
				{
					if (prlNotify->Line != i)	// duplicated !
					{
						prlNotify->UserStatus = false;
						return 0L;
					}
				}
			}
			strcpy(PageNoFromIndex(prlNotify->Line),prlNotify->Text);
			prlNotify->UserStatus = true;

//			update no of pages entry
			CString olDnop;
			m_DNOP.GetWindowText(olDnop);
			if (prlNotify->Line + 1 > atoi(olDnop))
			{
				olDnop.Format("%d",prlNotify->Line + 1);
				m_DNOP.SetInitText(olDnop);
			}

			return 0L;
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void FidsDisplayDlg::OnPageNew() 
{
	if (ogPrivList.GetStat("DISPLAYDLG.m_PAGENEW") == '1')
	{
		int ilLineNo = pomTable->omLines.GetSize();
		char *polPageId = this->PageIdFromIndex(ilLineNo);
		char *polPageConfig = this->PageConfigFromIndex(ilLineNo);
		char *polPageNo = this->PageNoFromIndex(ilLineNo);
		if (this->SelectPage(polPageId))
		{
			PAGDATA *polPag = ogPAGData.GetPAGByPagi(polPageId);
			if (polPag != NULL)
				polPageConfig = polPag->Pcfn;
			ChangePageTable(polPageId,polPageConfig,polPageNo, -1);
		}
	}
}

//----------------------------------------------------------------------------------------

void FidsDisplayDlg::OnPageDel() 
{
	int ilLineNo = pomTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
	}
	else
	{
		if(ogPrivList.GetStat("DISPLAYDLG.m_PAGEDEL") == '1')
		{
			char *polPageId = this->PageIdFromIndex(ilLineNo);
			char *polPageConfig = this->PageConfigFromIndex(ilLineNo);
			char *polPageNo = this->PageNoFromIndex(ilLineNo);
			if (polPageId != NULL && polPageConfig != NULL && polPageNo != NULL)
			{
				polPageId[0] = '\0';
				polPageConfig[0] = '\0';
				polPageNo[0] = '\0';
				ChangePageTable(NULL, NULL,NULL,ilLineNo);
				CString olDnop;
				m_DNOP.GetWindowText(olDnop);
				int ilDnop = atoi(olDnop) - 1;
				if (ilDnop <= 0)
				{
					m_DNOP.SetInitText("");
				}
				else
				{
					olDnop.Format("%d",ilDnop);
					m_DNOP.SetInitText(olDnop);
				}
			}
		}
	}
}


char *FidsDisplayDlg::PageIdFromIndex(int ipIndex)
{
	char *pclId = pomDSP->Pi01;
	pclId += ipIndex * (sizeof(pomDSP->Pi01) + sizeof(pomDSP->Pn01));
	return pclId;
}

char *FidsDisplayDlg::PageNoFromIndex(int ipIndex)
{
	char *pclId = pomDSP->Pi01;
	pclId += ipIndex * (sizeof(pomDSP->Pi01) + sizeof(pomDSP->Pn01));
	pclId += sizeof(pomDSP->Pi01);

	return pclId;

}

char *FidsDisplayDlg::PageConfigFromIndex(int ipIndex)
{
	char *pclId = pomDSP->Pi01;
	pclId += ipIndex * (sizeof(pomDSP->Pi01) + sizeof(pomDSP->Pn01));
	PAGDATA *polPag = ogPAGData.GetPAGByPagi(pclId);
	if (polPag)
		return polPag->Pcfn;
	else
		return NULL;
}

bool FidsDisplayDlg::SelectPage(char *popPageId)
{
	bool blOK = false;
	CStringArray olCol1, olCol2,olCol3;
	CCSPtrArray<PAGDATA> olList;

	AfxGetApp()->DoWaitCursor(1);
	if(ogPAGData.ReadSpecialData(&olList, "", "URNO,PAGI,PCFN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol2.Add(CString(olList[i].Pcfn));
			olCol1.Add(CString(olList[i].Pagi));
		}

		AwDlg *pomDlg = new AwDlg(&olCol1,&olCol2,&olCol3,LoadStg(IDS_STRING229),  LoadStg(IDS_STRING646), this);
		if(pomDlg->DoModal() == IDOK)
		{
			strcpy(popPageId,pomDlg->omReturnString);

			blOK = true;
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);

	return blOK;
}

bool FidsDisplayDlg::EditPageNumber(int ipLine,char *popPageNo)
{
	pomTable->SetIPValue(ipLine, 2,popPageNo);
	pomTable->imCurrentColumn = 2;
	pomTable->MakeInplaceEdit(ipLine,2);

	return true;
}

void FidsDisplayDlg::ResizeDialog()
{
	CRect olWindowRect;
	CRect olRemarkRect;
	CRect olCreatedRect;
	int ilOffset = 0;
	
	
	((CWnd*)GetDlgItem(IDC_REMA))->GetWindowRect(olRemarkRect);
	ScreenToClient(olRemarkRect);

	((CWnd*)GetDlgItem(IDC_CDAT_D))->GetWindowRect(olCreatedRect);
	ScreenToClient(olCreatedRect);

	ilOffset = olCreatedRect.top - olRemarkRect.top;
	CRect olDeflateRect(0,-ilOffset,0,ilOffset);	
	this->GetWindowRect(&olWindowRect);
	olWindowRect.bottom -= ilOffset;
	this->MoveWindow(olWindowRect);	

	((CWnd*)GetDlgItem(IDC_STATIC_REMA))->ShowWindow(SW_HIDE);
	((CWnd*)GetDlgItem(IDC_REMA))->ShowWindow(SW_HIDE);
	
	((CWnd*)GetDlgItem(IDC_GROUP_INFO))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.bottom -= ilOffset;
	((CWnd*)GetDlgItem(IDC_GROUP_INFO))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_STATIC_CREATED_ON))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_STATIC_CREATED_ON))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_STATIC_CREATED_BY))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_STATIC_CREATED_BY))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_STATIC_CHANGED_ON))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_STATIC_CHANGED_ON))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_STATIC_CHANGED_BY))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_STATIC_CHANGED_BY))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_CDAT_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_CDAT_D))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_CDAT_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_CDAT_T))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_LSTU_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_LSTU_D))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_LSTU_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_LSTU_T))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_USEC))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_USEC))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_USEU))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_USEU))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDOK))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDOK))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDCANCEL))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDCANCEL))->MoveWindow(olWindowRect);

}
