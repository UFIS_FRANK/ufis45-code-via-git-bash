#ifndef __GERAETEGRUPPENTABLEVIEWER_H__
#define __GERAETEGRUPPENTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaGegData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct GERAETEGRUPPENTABLE_LINEDATA
{
	long 	Urno;	// Eindeutige Datensatz-Nr.
	CString Gcde; 	// Code
	CString Gcat; 	// Kategorie
	CString Gnam; 	// Bezeichnung
	CString Gsnm; 	// Bezeichnung Kurzname
	CString Rema; 	// Bemerkung


};

/////////////////////////////////////////////////////////////////////////////
// GeraetegruppenTableViewer

class GeraetegruppenTableViewer : public CViewer
{
// Constructions
public:
    GeraetegruppenTableViewer(CCSPtrArray<GEGDATA> *popData);
    ~GeraetegruppenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(GEGDATA *prpGeraetegruppen);
	int CompareGeraetegruppen(GERAETEGRUPPENTABLE_LINEDATA *prpGeraetegruppen1, GERAETEGRUPPENTABLE_LINEDATA *prpGeraetegruppen2);
    void MakeLines();
	void MakeLine(GEGDATA *prpGeraetegruppen);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(GERAETEGRUPPENTABLE_LINEDATA *prpGeraetegruppen);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(GERAETEGRUPPENTABLE_LINEDATA *prpLine);
	void ProcessGeraetegruppenChange(GEGDATA *prpGeraetegruppen);
	void ProcessGeraetegruppenDelete(GEGDATA *prpGeraetegruppen);
	bool FindGeraetegruppen(char *prpGeraetegruppenKeya, char *prpGeraetegruppenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomGeraetegruppenTable;
	CCSPtrArray<GEGDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<GERAETEGRUPPENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(GERAETEGRUPPENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__GERAETEGRUPPENTABLEVIEWER_H__
