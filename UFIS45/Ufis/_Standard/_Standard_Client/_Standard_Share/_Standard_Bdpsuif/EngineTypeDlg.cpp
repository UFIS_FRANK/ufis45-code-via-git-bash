// EngineTypeDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <EngineTypeDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// EngineTypeDlg dialog


EngineTypeDlg::EngineTypeDlg(ENTDATA *popEnt, CWnd* pParent /*=NULL*/)
	: CDialog(EngineTypeDlg::IDD, pParent)
{
	pomEnt = popEnt;
	//{{AFX_DATA_INIT(EngineTypeDlg)
	//}}AFX_DATA_INIT
}


void EngineTypeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(EngineTypeDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_ENTC, m_ENTC);
	DDX_Control(pDX, IDC_ENAM, m_ENAM);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(EngineTypeDlg, CDialog)
	//{{AFX_MSG_MAP(EngineTypeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// EngineTypeDlg message handlers
//----------------------------------------------------------------------------------------

BOOL EngineTypeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_Caption = LoadStg(IDS_STRING619) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("ENGINETYPEDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomEnt->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("ENGINETYPEDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomEnt->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("ENGINETYPEDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomEnt->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("ENGINETYPEDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomEnt->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("ENGINETYPEDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomEnt->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("ENGINETYPEDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomEnt->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("ENGINETYPEDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_ENTC.SetFormat("x|#x|#x|#x|#x|#");
	m_ENTC.SetTextLimit(1,5);
	m_ENTC.SetBKColor(YELLOW);  
	m_ENTC.SetTextErrColor(RED);
	m_ENTC.SetInitText(pomEnt->Entc);
	m_ENTC.SetSecState(ogPrivList.GetStat("ENGINETYPEDLG.m_ENTC"));
	//------------------------------------
	m_ENAM.SetTypeToString("X(32)",32,1);
	m_ENAM.SetBKColor(YELLOW);  
	m_ENAM.SetTextErrColor(RED);
	m_ENAM.SetInitText(pomEnt->Enam);
	m_ENAM.SetSecState(ogPrivList.GetStat("ENGINETYPEDLG.m_ENAM"));
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void EngineTypeDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_ENTC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_ENTC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_ENAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_ENAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olText;//
		if(m_ENTC.GetWindowTextLength() != 0)
		{
			CCSPtrArray<ENTDATA> olEntCPA;
			char clWhere[100];
			m_ENTC.GetWindowText(olText);
			sprintf(clWhere,"WHERE ENTC='%s'",olText);
			if(ogENTData.ReadSpecialData(&olEntCPA,clWhere,"URNO",false) == true)
			{
				if(olEntCPA[0].Urno != pomEnt->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
				}
			}
			olEntCPA.DeleteAll();
		}
	}
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_ENTC.GetWindowText(pomEnt->Entc,6);
		m_ENAM.GetWindowText(pomEnt->Enam,33);
		//wandelt Return in Blank//
		////////////////////////////

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_ENTC.SetFocus();
	}	

	
}

//----------------------------------------------------------------------------------------
