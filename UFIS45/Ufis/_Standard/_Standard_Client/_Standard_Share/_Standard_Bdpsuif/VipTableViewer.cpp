// VeryImpPersTableViewer.cpp 
//

#include <stdafx.h>
#include <VipTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void VeryImpPersTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// VeryImpPersTableViewer
//

VeryImpPersTableViewer::VeryImpPersTableViewer(CCSPtrArray<VIPDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomVeryImpPersTable = NULL;
    ogDdx.Register(this, VIP_CHANGE, CString("VeryImpPersTableViewer"), CString("VeryImpPers Update"), VeryImpPersTableCf);
    ogDdx.Register(this, VIP_NEW,    CString("VeryImpPersTableViewer"), CString("VeryImpPers New"),    VeryImpPersTableCf);
    ogDdx.Register(this, VIP_DELETE, CString("VeryImpPersTableViewer"), CString("VeryImpPers Delete"), VeryImpPersTableCf);
}

//-----------------------------------------------------------------------------------------------

VeryImpPersTableViewer::~VeryImpPersTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void VeryImpPersTableViewer::Attach(CCSTable *popTable)
{
    pomVeryImpPersTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void VeryImpPersTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void VeryImpPersTableViewer::MakeLines()
{
	int ilVeryImpPersCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilVeryImpPersCount; ilLc++)
	{
		VIPDATA *prlVeryImpPersData = &pomData->GetAt(ilLc);
		MakeLine(prlVeryImpPersData);
	}
}

//-----------------------------------------------------------------------------------------------

void VeryImpPersTableViewer::MakeLine(VIPDATA *prpVeryImpPers)
{

    //if( !IsPassFilter(prpVeryImpPers)) return;

    // Update viewer data for this shift record
    VERYIMPPERSTABLE_LINEDATA rlVeryImpPers;

	rlVeryImpPers.Urno = prpVeryImpPers->Urno;
	rlVeryImpPers.Flnu = prpVeryImpPers->Flnu;
	rlVeryImpPers.Nogr = prpVeryImpPers->Nogr;
	rlVeryImpPers.Nopx = prpVeryImpPers->Nopx;
	rlVeryImpPers.Paxn = prpVeryImpPers->Paxn;
	rlVeryImpPers.Paxr = prpVeryImpPers->Paxr;

	CreateLine(&rlVeryImpPers);
}

//-----------------------------------------------------------------------------------------------

void VeryImpPersTableViewer::CreateLine(VERYIMPPERSTABLE_LINEDATA *prpVeryImpPers)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareVeryImpPers(prpVeryImpPers, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	VERYIMPPERSTABLE_LINEDATA rlVeryImpPers;
	rlVeryImpPers = *prpVeryImpPers;
    omLines.NewAt(ilLineno, rlVeryImpPers);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void VeryImpPersTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomVeryImpPersTable->SetShowSelection(TRUE);
	pomVeryImpPersTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING614),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING614),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 498; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING614),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 498; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING614),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	pomVeryImpPersTable->SetHeaderFields(omHeaderDataArray);

	pomVeryImpPersTable->SetDefaultSeparator();
	pomVeryImpPersTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Nogr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Nopx;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Paxn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Paxr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomVeryImpPersTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomVeryImpPersTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString VeryImpPersTableViewer::Format(VERYIMPPERSTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool VeryImpPersTableViewer::FindVeryImpPers(char *pcpVeryImpPersKeya, char *pcpVeryImpPersKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpVeryImpPersKeya) &&
			 (omLines[ilItem].Keyd == pcpVeryImpPersKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void VeryImpPersTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    VeryImpPersTableViewer *polViewer = (VeryImpPersTableViewer *)popInstance;
    if (ipDDXType == VIP_CHANGE || ipDDXType == VIP_NEW) polViewer->ProcessVeryImpPersChange((VIPDATA *)vpDataPointer);
    if (ipDDXType == VIP_DELETE) polViewer->ProcessVeryImpPersDelete((VIPDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void VeryImpPersTableViewer::ProcessVeryImpPersChange(VIPDATA *prpVeryImpPers)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpVeryImpPers->Nogr;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpVeryImpPers->Nopx;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpVeryImpPers->Paxn;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpVeryImpPers->Paxr;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpVeryImpPers->Urno, ilItem))
	{
        VERYIMPPERSTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpVeryImpPers->Urno;
		prlLine->Flnu = prpVeryImpPers->Flnu;
		prlLine->Nogr = prpVeryImpPers->Nogr;
		prlLine->Nopx = prpVeryImpPers->Nopx;
		prlLine->Paxn = prpVeryImpPers->Paxn;
		prlLine->Paxr = prpVeryImpPers->Paxr;


		pomVeryImpPersTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomVeryImpPersTable->DisplayTable();
	}
	else
	{
		MakeLine(prpVeryImpPers);
		if (FindLine(prpVeryImpPers->Urno, ilItem))
		{
	        VERYIMPPERSTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomVeryImpPersTable->AddTextLine(olLine, (void *)prlLine);
				pomVeryImpPersTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void VeryImpPersTableViewer::ProcessVeryImpPersDelete(VIPDATA *prpVeryImpPers)
{
	int ilItem;
	if (FindLine(prpVeryImpPers->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomVeryImpPersTable->DeleteTextLine(ilItem);
		pomVeryImpPersTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool VeryImpPersTableViewer::IsPassFilter(VIPDATA *prpVeryImpPers)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int VeryImpPersTableViewer::CompareVeryImpPers(VERYIMPPERSTABLE_LINEDATA *prpVeryImpPers1, VERYIMPPERSTABLE_LINEDATA *prpVeryImpPers2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpVeryImpPers1->Tifd == prpVeryImpPers2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpVeryImpPers1->Tifd > prpVeryImpPers2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void VeryImpPersTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool VeryImpPersTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void VeryImpPersTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void VeryImpPersTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING203);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool VeryImpPersTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool VeryImpPersTableViewer::PrintTableLine(VERYIMPPERSTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Nogr;
				break;
			case 1:
				rlElement.Text		 = prpLine->Nopx;
				break;
			case 2:
				rlElement.Text		 = prpLine->Paxn;
				break;
			case 3:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Paxr;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
