// CedaSpfData.h

#ifndef __CEDAPSPFDATA__
#define __CEDAPSPFDATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------

struct SPFDATA 
{
	long Urno;			// Eindeutige Datensatz-Nr.
	long Surn;			// Referenz auf STF.URNO
	char Code[7];		// Codereferenz ORG.CODE
	COleDateTime Vpfr;	// G�ltig von
	COleDateTime Vpto;	// G�ltig bis

	char Prio[2+2];		// Priorit�t der Funktion bei mehreren Funk. zur gleiche zeit
	

	//*** 27.08.99 SHA ***
	//*** Required by ALITALIA ***
	char	 Act3[5];	// Aircraft
	char	 Act5[7];
	char	 Alc2[4];	// Airline
	char	 Alc3[5];
	
	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SPFDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		//Vpfr=-1,Vpto=-1; //<zB.(FIELD_DATE Felder)
	}

}; // end SPFDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSpfData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapPtrToPtr omStaffUrnoMap;
	CMapPtrToPtr omStaffFctMap;	

    CCSPtrArray<SPFDATA> omData;

	char pcmListOfFields[2048];

// OSpfations
public:
    CedaSpfData();
	~CedaSpfData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SPFDATA *prpSpf);
	bool InsertInternal(SPFDATA *prpSpf);
	bool Update(SPFDATA *prpSpf);
	bool UpdateInternal(SPFDATA *prpSpf);
	bool Delete(long lpUrno);
	bool DeleteInternal(SPFDATA *prpSpf);
	bool ReadSpecialData(CCSPtrArray<SPFDATA> *popSpf,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SPFDATA *prpSpf);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SPFDATA  *GetSpfByUrno(long lpUrno);
	SPFDATA *GetValidSpfBySurn(long lpSurn);
	CString	GetValidFctBySurn(long lpSurn);

	void GetAllSpfBySurn(long lpSurn,CCSPtrArray<SPFDATA> &ropList);


	// Private methods
private:
    void PrepareSpfData(SPFDATA *prpSpfData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPSPFDATA__
