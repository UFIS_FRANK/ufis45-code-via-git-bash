// CedaDENData.h

#ifndef __CEDADENDATA__
#define __CEDADENDATA__

#include <iostream.h>
#include <fstream.h> 
#include <iomanip.h>
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

//@See: CedaDENData
//@Doc:	A structure for reading Duty data. We read all data from database and put them
//		into this struct and store the data in omData.  

//---------------------------------------------------------------------------------------------------------
struct DENDATA 
{
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Deca[4]; 	// Versp�tungs- Code alphabetisch
	char 	 Decn[4]; 	// Versp�tungs- Code numerisch
	char 	 Dena[77]; 	// Versp�tungs- Name
	CTime	 Lstu; 		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Alc3[5]; 	// Fluggesellschaft 3-Letter Code
	CTime	 Vafr;		// G�ltig von
	CTime	 Vato;		// G�ltig bis
	char	 RCol[4];	// Abnormal flight report column for PVG
	char     Decs[2];   // MultiDelay Sub Code

	//DataCreated by this class
	int		 IsChanged;

	DENDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end DENDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino


class CedaDENData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<DENDATA> omData;

// Operations
public:
    CedaDENData();
	~CedaDENData();
	
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);
    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<DENDATA> *popDen,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool InsertDEN(DENDATA *prpDEN,BOOL bpSendDdx = TRUE);
	bool InsertDENInternal(DENDATA *prpDEN);
	bool UpdateDEN(DENDATA *prpDEN,BOOL bpSendDdx = TRUE);
	bool UpdateDENInternal(DENDATA *prpDEN);
	bool DeleteDEN(long lpUrno);
	bool DeleteDENInternal(DENDATA *prpDEN);
	DENDATA  *GetDENByUrno(long lpUrno);
	bool SaveDEN(DENDATA *prpDEN);
	char pcmDENFieldList[2048];
	void ProcessDENBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795

	bool CreateExcelFile( CString opTrenner );
	void PrintExcelLine(ofstream &of, DENDATA *prpLine, CString opTrenner);
	bool PrintExcelHeader(ofstream &of, CString opTrenner);

	// Private methods
private:
    void PrepareDENData(DENDATA *prpDENData);
	bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct);
	bool MakeCedaData(CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct);
	bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct);
	CString omWhere; //Prf: 8795
	bool ValidateDENBcData(const long& lrpUnro); //Prf: 8795
	void PrepareFieldList( int option );//AM:20100914 Multi Delay Codes
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDADENDATA__
