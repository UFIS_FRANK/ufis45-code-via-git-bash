// GateDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <GateDlg.h>
#include <CedaPSTData.h>
#include <CedaBLTData.h>
#include <PrivList.h>
#include <NotAvailableDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GateDlg dialog


GateDlg::GateDlg(GATDATA *popGAT,CWnd* pParent /*=NULL*/) : CDialog(GateDlg::IDD, pParent)
{
	pomGAT = popGAT;

	pomTable = NULL;
	pomTable = new CCSTable;

	//{{AFX_DATA_INIT(GateDlg)
	//}}AFX_DATA_INIT
}

GateDlg::GateDlg(GATDATA *popGAT,int ipDlg,CWnd* pParent /*=NULL*/) 
: CDialog(ipDlg, pParent)
{
	pomGAT = popGAT;

	pomTable = NULL;
	pomTable = new CCSTable;

	//{{AFX_DATA_INIT(GateDlg)
	//}}AFX_DATA_INIT
}

GateDlg::~GateDlg()
{
	omBlkPtrA.DeleteAll();
	omDeleteBlkPtrA.DeleteAll();
	delete pomTable;

}


void GateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GateDlg)
	DDX_Control(pDX, IDC_DEFD, m_DEFD);
	DDX_Control(pDX, IDC_TEL4, m_TEL4);
	DDX_Control(pDX, IDC_TEL3, m_TEL3);
	DDX_Control(pDX, IDC_TEL2, m_TEL2);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_NOAVNEW, m_NOAVNEW);
	DDX_Control(pDX, IDC_NOAVDEL, m_NOAVDEL);
	DDX_Control(pDX, IDC_NOAV,	 m_NOAVFRAME);
	DDX_Control(pDX, IDC_BUSG,	 m_BUSG);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_GNAM,	 m_GNAM);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_NOBR,	 m_NOBR);
	DDX_Control(pDX, IDC_RBAB,	 m_RBAB);
	DDX_Control(pDX, IDC_RGA1,	 m_RGA1);
	DDX_Control(pDX, IDC_RGA2,	 m_RGA2);
	DDX_Control(pDX, IDC_TELE,	 m_TELE);
	DDX_Control(pDX, IDC_TERM,	 m_TERM);
	DDX_Control(pDX, IDC_HOME,	 m_HOME);
	DDX_Control(pDX, IDC_GTID,	 m_GTID);
	DDX_Control(pDX, IDC_GTYP,	 m_GTYP);
	DDX_Control(pDX, IDC_RADIO_UTC,	 m_UTC);
	DDX_Control(pDX, IDC_RADIO_LOCAL,m_LOCAL);
	DDX_Control(pDX, IDC_APIS,	m_APIS);
	DDX_Control(pDX, IDC_ABPR,  m_ABPR);
	DDX_Control(pDX, IDC_FIDS,  m_FIDS);
	DDX_Control(pDX, IDC_GCAP,  m_GCAP);
	DDX_Control(pDX, IDC_NOST,  m_NOST);
	DDX_Control(pDX, IDC_NOBP,  m_NOBP);
	DDX_Control(pDX, IDC_NOXM,  m_NOXM);
	DDX_Control(pDX, IDC_DCST,  m_DCST);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GateDlg, CDialog)
	//{{AFX_MSG_MAP(GateDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVDEL, OnNoavDel)
	ON_BN_CLICKED(IDC_NOAVNEW, OnNoavNew)
	ON_BN_CLICKED(IDC_RADIO_UTC,	OnUTC)
	ON_BN_CLICKED(IDC_RADIO_LOCAL,	OnLocal)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GateDlg message handlers
//----------------------------------------------------------------------------------------

BOOL GateDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING178) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("GATEDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomGAT->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("GATEDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomGAT->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("GATEDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomGAT->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("GATEDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomGAT->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("GATEDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomGAT->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("GATEDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomGAT->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("GATEDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomGAT->);
	m_GRUP.SetSecState(ogPrivList.GetStat("GATEDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_GNAM.SetFormat("x|#x|#x|#x|#x|#");
	m_GNAM.SetTextLimit(1,5);
	m_GNAM.SetBKColor(YELLOW);
	m_GNAM.SetTextErrColor(RED);
	m_GNAM.SetInitText(pomGAT->Gnam);
	m_GNAM.SetSecState(ogPrivList.GetStat("GATEDLG.m_GNAM"));
	//------------------------------------
	if (pomGAT->Busg[0] == 'X') m_BUSG.SetCheck(1);
	clStat = ogPrivList.GetStat("GATEDLG.m_BUSG");
	SetWndStatAll(clStat,m_BUSG);
	//------------------------------------
	m_RGA1.SetFormat("x|#x|#x|#x|#x|#");
	m_RGA1.SetTextLimit(0,5);
	m_RGA1.SetTextErrColor(RED);
	m_RGA1.SetInitText(pomGAT->Rga1);
	m_RGA1.SetSecState(ogPrivList.GetStat("GATEDLG.m_RGA1"));
	//------------------------------------
	m_RGA2.SetFormat("x|#x|#x|#x|#x|#");
	m_RGA2.SetTextLimit(0,5);
	m_RGA2.SetTextErrColor(RED);
	m_RGA2.SetInitText(pomGAT->Rga2);
	m_RGA2.SetSecState(ogPrivList.GetStat("GATEDLG.m_RGA2"));
	//------------------------------------
	m_RBAB.SetFormat("x|#x|#x|#x|#x|#");
	m_RBAB.SetTextLimit(0,5);
	m_RBAB.SetTextErrColor(RED);
	m_RBAB.SetInitText(pomGAT->Rbab);
	m_RBAB.SetSecState(ogPrivList.GetStat("GATEDLG.m_RBAB"));
	//------------------------------------
	m_TERM.SetFormat("x|#");
	m_TERM.SetTextLimit(0,1);
	m_TERM.SetTextErrColor(RED);
	m_TERM.SetInitText(pomGAT->Term);
	m_TERM.SetSecState(ogPrivList.GetStat("GATEDLG.m_TERM"));
	//------------------------------------
	m_TELE.SetTypeToString("X(10)",10,0);
	m_TELE.SetTextErrColor(RED);
	m_TELE.SetInitText(pomGAT->Tele);
	m_TELE.SetSecState(ogPrivList.GetStat("GATEDLG.m_TELE"));
	//------------------------------------
	m_HOME.SetTypeToString("X(3)",3,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomGAT->Home);
	m_HOME.SetSecState(ogPrivList.GetStat("GATEDLG.m_HOME"));
	//------------------------------------
	m_GTID.SetTypeToString("X(2)",2,1);
	m_GTID.SetTextErrColor(RED);
	m_GTID.SetInitText(pomGAT->Gtid);
	m_GTID.SetSecState(ogPrivList.GetStat("GATEDLG.m_GTID"));
	//------------------------------------
	m_GTYP.SetTypeToString("X(2)",2,1);
	m_GTYP.SetTextErrColor(RED);
	m_GTYP.SetInitText(pomGAT->Gtyp);
	m_GTYP.SetSecState(ogPrivList.GetStat("GATEDLG.m_GTYP"));
	//------------------------------------
	m_NOBR.SetTypeToInt(0,99);
	m_NOBR.SetTextErrColor(RED);
	m_NOBR.SetInitText(pomGAT->Nobr);
	m_NOBR.SetSecState(ogPrivList.GetStat("GATEDLG.m_NOBR"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomGAT->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("GATEDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomGAT->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("GATEDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomGAT->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("GATEDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomGAT->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("GATEDLG.m_VATO"));
	//------------------------------------

//#ifdef CLIENT_ZRH
	//------------------------------------
	m_TEL2.SetTypeToString("X(10)",10,0);
	m_TEL2.SetTextErrColor(RED);
	m_TEL2.SetInitText(pomGAT->Tel2);
	m_TEL2.SetSecState(ogPrivList.GetStat("GATEDLG.m_TELE"));
	//------------------------------------
	m_TEL3.SetTypeToString("X(10)",10,0);
	m_TEL3.SetTextErrColor(RED);
	m_TEL3.SetInitText(pomGAT->Tel3);
	m_TEL3.SetSecState(ogPrivList.GetStat("GATEDLG.m_TELE"));
	//------------------------------------
	m_TEL4.SetTypeToString("X(10)",10,0);
	m_TEL4.SetTextErrColor(RED);
	m_TEL4.SetInitText(pomGAT->Tel4);
	m_TEL4.SetSecState(ogPrivList.GetStat("GATEDLG.m_TELE"));

//#endif

	m_DEFD.SetFormat("###");
	m_DEFD.SetTextLimit(0,3);
	m_DEFD.SetTextErrColor(RED);
	m_DEFD.SetInitText(pomGAT->Defd);
	m_DEFD.SetSecState(ogPrivList.GetStat("GATEDLG.m_DEFD"));

	if (ogBasicData.UseLocalTimeAsDefault())
	{
		this->m_UTC.SetCheck(0);
		this->m_LOCAL.SetCheck(1);
		imLastSelection = 2;
	}
	else
	{
		this->m_UTC.SetCheck(1);
		this->m_LOCAL.SetCheck(0);
		imLastSelection = 1;
	}

	//UFIS-1087
	if (pomGAT->Apis[0] == 'X') m_APIS.SetCheck(1);
	clStat = ogPrivList.GetStat("GATEDLG.m_APIS");
	//SetWndStatAll(clStat,m_APIS);

	if (pomGAT->Abpr[0] == 'X') m_ABPR.SetCheck(1);
	clStat = ogPrivList.GetStat("GATEDLG.m_ABPR");
	//SetWndStatAll(clStat,m_ABPR);

	if (pomGAT->Fids[0] == 'X') m_FIDS.SetCheck(1);
	clStat = ogPrivList.GetStat("GATEDLG.m_FIDS");
	//SetWndStatAll(clStat,m_FIDS);

	//------------------------------------
	m_GCAP.SetFormat("x|#x|#x|#x|#");
	m_GCAP.SetTextLimit(0,4);
	m_GCAP.SetTextErrColor(RED);
	m_GCAP.SetInitText(pomGAT->Gcap);
	//m_GCAP.SetSecState(ogPrivList.GetStat("GATEDLG.m_GCAP"));
	//------------------------------------
	m_NOST.SetFormat("x|#x|#x|#x|#");
	m_NOST.SetTextLimit(0,4);
	m_NOST.SetTextErrColor(RED);
	m_NOST.SetInitText(pomGAT->Nost);
	//m_NOST.SetSecState(ogPrivList.GetStat("GATEDLG.m_NOST"));
	//------------------------------------
	m_NOBP.SetFormat("x|#x|#");
	m_NOBP.SetTextLimit(0,2);
	m_NOBP.SetTextErrColor(RED);
	m_NOBP.SetInitText(pomGAT->Nobp);
	//m_NOBP.SetSecState(ogPrivList.GetStat("GATEDLG.m_NOBP"));
	//------------------------------------
	m_NOXM.SetFormat("x|#x|#");
	m_NOXM.SetTextLimit(0,2);
	m_NOXM.SetTextErrColor(RED);
	m_NOXM.SetInitText(pomGAT->Noxm);
	//m_NOXM.SetSecState(ogPrivList.GetStat("GATEDLG.m_NOXM"));
	//------------------------------------
	m_DCST.SetTypeToString("X(25)",25,0);
	m_DCST.SetTextErrColor(RED);
	m_DCST.SetInitText(pomGAT->Dcst);
	//m_DCST.SetSecState(ogPrivList.GetStat("GATEDLG.m_DCST"));
	//------------------------------------
	
	CString olUrno;
	olUrno.Format("%d",pomGAT->Urno);
	MakeNoavTable("GAT", olUrno);

	return TRUE;
}

//----------------------------------------------------------------------------------------

void GateDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_GNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_GNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_RGA1.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING351) + ogNotFormat;
	}
	if(m_RGA2.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING352) + ogNotFormat;
	}
	if(m_RBAB.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING353) + ogNotFormat;
	}
	if(m_TERM.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING296) + ogNotFormat;
	}
	if(m_TELE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING294) + ogNotFormat;
	}
	if(m_NOBR.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING354) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	if(m_DEFD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING856) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olGnam,olRga1,olRga2,olRbab;
		char clWhere[100];
		CCSPtrArray<GATDATA> olGatCPA;

		m_GNAM.GetWindowText(olGnam);
		sprintf(clWhere,"WHERE GNAM='%s'",olGnam);
		if(ogGATData.ReadSpecialData(&olGatCPA,clWhere,"URNO,GNAM",false) == true)
		{
			if(olGatCPA[0].Urno != pomGAT->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
			}
		}
		olGatCPA.DeleteAll();

		if(m_RGA1.GetWindowTextLength() != 0)
		{
			m_RGA1.GetWindowText(olRga1);
			sprintf(clWhere,"WHERE PNAM='%s'",olRga1);
			if(ogPSTData.ReadSpecialData(NULL,clWhere,"PNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING361);
			}
		}

		if(m_RGA2.GetWindowTextLength() != 0)
		{
			m_RGA2.GetWindowText(olRga2);
			sprintf(clWhere,"WHERE PNAM='%s'",olRga2);
			if(ogPSTData.ReadSpecialData(NULL,clWhere,"PNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING362);
			}
		}

		if(m_RBAB.GetWindowTextLength() != 0)
		{
			m_RBAB.GetWindowText(olRbab);
			sprintf(clWhere,"WHERE BNAM='%s'",olRbab);
			if(ogBLTData.ReadSpecialData(NULL,clWhere,"BNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING376);
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_RBAB.GetWindowText(pomGAT->Rbab,6);
		m_RGA2.GetWindowText(pomGAT->Rga2,6);
		m_RGA1.GetWindowText(pomGAT->Rga1,6);
		m_GNAM.GetWindowText(pomGAT->Gnam,6);
		m_HOME.GetWindowText(pomGAT->Home,3);
		m_GTYP.GetWindowText(pomGAT->Gtyp,2);
		m_GTID.GetWindowText(pomGAT->Gtid,2);
		(m_BUSG.GetCheck() == 1) ? pomGAT->Busg[0] = 'X' : pomGAT->Busg[0] = ' ';
		m_TERM.GetWindowText(pomGAT->Term,2);
		m_TELE.GetWindowText(pomGAT->Tele,11);

		//*** 09.11.99 SHA ***
//#ifdef CLIENT_ZRH
			m_TEL2.GetWindowText(pomGAT->Tel2,11);
			m_TEL3.GetWindowText(pomGAT->Tel3,11);
			m_TEL4.GetWindowText(pomGAT->Tel4,11);
//#endif

		m_NOBR.GetWindowText(pomGAT->Nobr,3);
		pomGAT->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomGAT->Vato = DateHourStringToDate(olVatod,olVatot);
		m_DEFD.GetWindowText(pomGAT->Defd,4);

		//UFIS-1087
		(m_APIS.GetCheck() == 1) ? pomGAT->Apis[0] = 'X' : pomGAT->Apis[0] = ' ';
		(m_ABPR.GetCheck() == 1) ? pomGAT->Abpr[0] = 'X' : pomGAT->Abpr[0] = ' ';
		(m_FIDS.GetCheck() == 1) ? pomGAT->Fids[0] = 'X' : pomGAT->Fids[0] = ' ';

	
		m_GCAP.GetWindowText(pomGAT->Gcap,5);
		m_NOST.GetWindowText(pomGAT->Nost,5);
		m_NOBP.GetWindowText(pomGAT->Nobp,3);
		m_NOXM.GetWindowText(pomGAT->Noxm,3);
		m_DCST.GetWindowText(pomGAT->Dcst,26);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_GNAM.SetFocus();
		m_GNAM.SetSel(0,-1);
	}
}
//----------------------------------------------------------------------------------------

void GateDlg::MakeNoavTable(CString opTabn, CString opBurn)
{
	CRect olRectBorder;
	m_NOAVFRAME.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);
	pomTable->SetShowSelection(TRUE);
	pomTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	pomTable->SetHeaderFields(olHeaderDataArray);
	pomTable->SetDefaultSeparator();
	olHeaderDataArray.DeleteAll();

	char clWhere[100];
	CCSPtrArray<BLKDATA> olBlkCPA;
	sprintf(clWhere,"WHERE TABN='%s' AND BURN='%s' ORDER BY NAFR DESC",opTabn,opBurn);
	ogBlkData.ReadSpecialData(&omBlkPtrA,clWhere,"",false);
	UpdateNoavTable();
}

//----------------------------------------------------------------------------------------
void GateDlg::UpdateNoavTable()
{
	int ilLines = omBlkPtrA.GetSize();

	pomTable->ResetContent();
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		CTime olNafr = omBlkPtrA[ilLineNo].Nafr;
		CTime olNato = omBlkPtrA[ilLineNo].Nato;
		if (this->imLastSelection == 2)	// local
		{
			ogBasicData.UtcToLocal(olNafr);
			ogBasicData.UtcToLocal(olNato);
		}

		rlColumnData.Text = olNafr.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = olNato.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Days;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		char pclTmp[10]="";
		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tifr) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = omBlkPtrA[ilLineNo].Tifr;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTifr);
				rlColumnData.Text = olTifr.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tifr).Left(2), CString(omBlkPtrA[ilLineNo].Tifr).Right(2));
				rlColumnData.Text = pclTmp;
			}
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tito) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = omBlkPtrA[ilLineNo].Tito;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTito);
				rlColumnData.Text = olTito.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tito).Left(2), CString(omBlkPtrA[ilLineNo].Tito).Right(2));
				rlColumnData.Text = pclTmp;
			}
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		(*omBlkPtrA[ilLineNo].Type == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Resn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomTable->DisplayTable();
}
//----------------------------------------------------------------------------------------

void GateDlg::ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo)
{
	if(prpBlk != NULL)
	{
		TABLE_COLUMN rlColumn;
		CCSPtrArray <TABLE_COLUMN> olLine;

		rlColumn.Lineno = 0;
		rlColumn.VerticalSeparator = SEPA_NONE;
		rlColumn.SeparatorType = SEPA_NONE;
		rlColumn.BkColor = WHITE;
		rlColumn.Font = &ogCourier_Regular_10;
		rlColumn.Alignment = COLALIGN_LEFT;

		CTime olNafr = prpBlk->Nafr;
		CTime olNato = prpBlk->Nato;
		if (this->imLastSelection == 2)	// local
		{
			ogBasicData.UtcToLocal(olNafr);
			ogBasicData.UtcToLocal(olNato);
		}

		int ilColumnNo = 0;
		rlColumn.Text = olNafr.Format("%d.%m.%Y %H:%M");
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = olNato.Format("%d.%m.%Y %H:%M"); 
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Days;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		char pclTmp[10]="";
		rlColumn.Text = "";
		if(strlen(prpBlk->Tifr) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = prpBlk->Tifr;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTifr);
				rlColumn.Text = olTifr.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(prpBlk->Tifr).Left(2), CString(prpBlk->Tifr).Right(2));
				rlColumn.Text = pclTmp;
			}
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text = "";
		if(strlen(prpBlk->Tito) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = prpBlk->Tito;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTito);
				rlColumn.Text = olTito.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(prpBlk->Tito).Left(2), CString(prpBlk->Tito).Right(2));
				rlColumn.Text = pclTmp;
			}
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		(*prpBlk->Type == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Resn;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		if(ipLineNo>-1)
		{
			pomTable->ChangeTextLine(ipLineNo, &olLine, (void *)prpBlk);
			pomTable->DisplayTable();
		}
		else
		{
			if(prpBlk != NULL)
			{
				pomTable->AddTextLine(olLine, (void *)prpBlk);
				pomTable->DisplayTable();
			}
		}
		olLine.DeleteAll();
	}
	else
	{
		if(ipLineNo>-1)
		{
			pomTable->DeleteTextLine(ipLineNo);
			pomTable->DisplayTable();
		}
	}
}


//----------------------------------------------------------------------------------------
LONG GateDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(ogPrivList.GetStat("GATEDLG.m_NOAVCHA") == '1')
	{
		int ilLineNo = pomTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
		}
		else
		{
			BLKDATA *prlBlk = &omBlkPtrA[ilLineNo];
			if (prlBlk != NULL)
			{
				BLKDATA rlBlk = *prlBlk;
				NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
				m_GNAM.GetWindowText(polDlg->omName);
				if(polDlg->DoModal() == IDOK)
				{
					*prlBlk = rlBlk;
					if(prlBlk->IsChanged != DATA_NEW) prlBlk->IsChanged = DATA_CHANGED;
					ChangeNoavTable(prlBlk, ilLineNo);
				}
				delete polDlg;
			}
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void GateDlg::OnNoavNew() 
{
	BLKDATA rlBlk;

	if(ogPrivList.GetStat("GATEDLG.m_NOAVNEW") == '1')
	{
		NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
		m_GNAM.GetWindowText(polDlg->omName);
		if(polDlg->DoModal() == IDOK)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = rlBlk;
			omBlkPtrA.Add(prlBlk);
			prlBlk->IsChanged = DATA_NEW;
			ChangeNoavTable(prlBlk, -1);
		}
		delete polDlg;
	}
}

//----------------------------------------------------------------------------------------

void GateDlg::OnNoavDel() 
{
	int ilLineNo = pomTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
	}
	else
	{
		if(ogPrivList.GetStat("GATEDLG.m_NOAVDEL") == '1')
		{
			if (&omBlkPtrA[ilLineNo] != NULL)
			{
				BLKDATA *prlBlk = new BLKDATA;
				*prlBlk = omBlkPtrA[ilLineNo];
				prlBlk->IsChanged = DATA_DELETED;
				omDeleteBlkPtrA.Add(prlBlk);
				omBlkPtrA.DeleteAt(ilLineNo);
				ChangeNoavTable(NULL, ilLineNo);
			}
		}
	}
}

//----------------------------------------------------------------------------------------
void GateDlg::OnUTC() 
{
	if (imLastSelection == 1)
		return;

	imLastSelection = 1;

	UpdateNoavTable();
}

//----------------------------------------------------------------------------------------
void GateDlg::OnLocal() 
{
	if (imLastSelection == 2)
		return;

	imLastSelection = 2;

	UpdateNoavTable();
}
