// CCSComboBox.cpp : implementation file
//

#include <stdafx.h>
#include <CCSComboBox.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCSComboBox
//---------------------------------------------------------------------------

CCSComboBox::CCSComboBox()
{
	omBKColor = RGB(255,255,255);
	omActualBKColor = omBKColor;
	omTextColor = RGB(0,0,0);
	omActualTextColor = omTextColor;
	omBrush.CreateSolidBrush(omActualBKColor);
}

CCSComboBox::~CCSComboBox()
{
	omBrush.DeleteObject();
}


BEGIN_MESSAGE_MAP(CCSComboBox, CComboBox)
	//{{AFX_MSG_MAP(CCSComboBox)
	ON_WM_CTLCOLOR_REFLECT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCSComboBox message handlers
//---------------------------------------------------------------------------

HBRUSH CCSComboBox::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	HBRUSH hbr = 0;
	pDC->SetTextColor(omActualTextColor);
	pDC->SetBkColor(omActualBKColor);
	pDC->SetBkMode(TRANSPARENT);
	omBrush.DeleteObject();
	omBrush.CreateSolidBrush(omActualBKColor);
	hbr = omBrush;	
	return hbr;
}

//---------------------------------------------------------------------------

void CCSComboBox::SetBKColor(COLORREF opColor)
{
	omBKColor = opColor;
	omActualBKColor = omBKColor;
	InvalidateRect(NULL);
	UpdateWindow();
}

//---------------------------------------------------------------------------

void CCSComboBox::SetTextColor(COLORREF opColor)
{
	omTextColor = opColor;
	omActualTextColor = omTextColor;
	InvalidateRect(NULL);
	UpdateWindow();
}

//---------------------------------------------------------------------------

BOOL CCSComboBox::EnableWindow(BOOL BmEnable)
{
	BOOL BlEnable = CComboBox::EnableWindow(BmEnable);
	if(BmEnable == FALSE)
	{
		//PRF6932 omActualBKColor = RGB(192, 192, 192);
		omActualBKColor = ::GetSysColor(COLOR_BTNFACE);
		omActualTextColor = RGB(128, 128, 128);
	}
	else
	{
		omActualBKColor = omBKColor;
		omActualTextColor = omTextColor;
	}
	InvalidateRect(NULL);
	UpdateWindow();
	return BlEnable;
}

//---------------------------------------------------------------------------

int CCSComboBox::SetCurSelByString(CString omString,bool bmBool)
{
	CString olTmp;
	int ilCount = GetCount();
	for(int i=0; i<ilCount; i++)
	{
		GetLBText(i,olTmp);
		if((olTmp.Find(omString))!=-1)
		{
			if(bmBool==true)SetCurSel(i);
			return i;
		}
	}
	return -1;
}

//---------------------------------------------------------------------------
