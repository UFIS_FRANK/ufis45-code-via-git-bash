// CedaCrcData.cpp: implementation of the CedaCrcData class.   //PRF 8378
//
//////////////////////////////////////////////////////////////////////
#include <stdafx.h>
#include <CedaCrcData.h>
#include <resource.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void ProcessCrcCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaCrcData::CedaCrcData(): CCSCedaData(&ogCommHandler)
{
	
	// Create an array of CEDARECINFO for CRCDataStruct
	BEGIN_CEDARECINFO(CRCDATA,CRCDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Hopo,"HOPO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Gatf,"GATF")
		FIELD_CHAR_TRIM	(Posf,"POSF")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Cxxf,"CXXF")
		FIELD_CHAR_TRIM	(Belt,"BELT")
	END_CEDARECINFO //(CRCDataStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(CRCDataRecInfo)/sizeof(CRCDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CRCDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"CRC");
	strcpy(pcmListOfFields,"URNO,HOPO,USEC,CDAT,USEU,LSTU,CODE,REMA,GATF,POSF,VAFR,VATO,CXXF,BELT");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}
//---------------------------------------------------------------------------------------------------------

void CedaCrcData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");	ropDesription.Add(LoadStg(IDS_STRING346));		ropType.Add("String");
	ropFields.Add("HOPO");	ropDesription.Add(LoadStg(IDS_STRING711));		ropType.Add("String");	
	ropFields.Add("USEC");	ropDesription.Add(LoadStg(IDS_STRING347));		ropType.Add("String");
	ropFields.Add("CDAT");	ropDesription.Add(LoadStg(IDS_STRING343));		ropType.Add("Date");
	ropFields.Add("USEU");	ropDesription.Add(LoadStg(IDS_STRING348));		ropType.Add("String");
	ropFields.Add("LSTU");	ropDesription.Add(LoadStg(IDS_STRING344));		ropType.Add("Date");
	ropFields.Add("CODE");	ropDesription.Add(LoadStg(IDS_STRING32797));	ropType.Add("String");
	ropFields.Add("REMA");	ropDesription.Add(LoadStg(IDS_STRING32798));	ropType.Add("String");
	ropFields.Add("GATF");	ropDesription.Add(LoadStg(IDS_STRING32799));	ropType.Add("String");
	ropFields.Add("POSF");	ropDesription.Add(LoadStg(IDS_STRING32800));	ropType.Add("String");
	ropFields.Add("VAFR");	ropDesription.Add(LoadStg(IDS_STRING230));		ropType.Add("Date");
	ropFields.Add("VATO");	ropDesription.Add(LoadStg(IDS_STRING231));		ropType.Add("Date");
	if (bgCxxfDisp)
	{
		ropFields.Add("CXXF");	ropDesription.Add(LoadStg(IDS_STRING32817));	ropType.Add("String");
	}
	ropFields.Add("BELT");	ropDesription.Add(LoadStg(IDS_STRING32835));	ropType.Add("String");//UFIS-1184
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaCrcData::Register(void)
{
	ogDdx.Register((void *)this,BC_CRC_CHANGE,	CString("CRCDATA"), CString("Crc-changed"),	ProcessCrcCf);
	ogDdx.Register((void *)this,BC_CRC_NEW,		CString("CRCDATA"), CString("Crc-new"),		ProcessCrcCf);
	ogDdx.Register((void *)this,BC_CRC_DELETE,	CString("CRCDATA"), CString("Crc-deleted"),	ProcessCrcCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaCrcData::~CedaCrcData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaCrcData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaCrcData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		CRCDATA *prlCrc = new CRCDATA;
		if ((ilRc = GetFirstBufferRecord(prlCrc)) == true)
		{
			omData.Add(prlCrc);//Update omData
			omUrnoMap.SetAt((void *)prlCrc->Urno,prlCrc);
		}
		else
		{
			delete prlCrc;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaCrcData::InsertCRC(CRCDATA *prpCrc)
{
	prpCrc->IsChanged = DATA_NEW;
	if(Save(prpCrc) == false) return false; //Update Database
	InsertInternal(prpCrc);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaCrcData::InsertInternal(CRCDATA *prpCrc)
{
	ogDdx.DataChanged((void *)this, CRC_NEW,(void *)prpCrc ); //Update Viewer
	omData.Add(prpCrc);//Update omData
	omUrnoMap.SetAt((void *)prpCrc->Urno,prpCrc);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaCrcData::DeleteCRC(long lpUrno)
{
	CRCDATA *prlCrc = GetCrcByUrno(lpUrno);
	if (prlCrc != NULL)
	{
		prlCrc->IsChanged = DATA_DELETED;
		if(Save(prlCrc) == false) return false; //Update Database
		DeleteInternal(prlCrc);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaCrcData::DeleteInternal(CRCDATA *prpCrc)
{
	ogDdx.DataChanged((void *)this,CRC_DELETE,(void *)prpCrc); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpCrc->Urno);
	int ilCrcCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCrcCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpCrc->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaCrcData::UpdateCRC(CRCDATA *prpCrc)
{
	if (GetCrcByUrno(prpCrc->Urno) != NULL)
	{
		if (prpCrc->IsChanged == DATA_UNCHANGED)
		{
			prpCrc->IsChanged = DATA_CHANGED;
		}
		if(Save(prpCrc) == false) return false; //Update Database
		UpdateInternal(prpCrc);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaCrcData::UpdateInternal(CRCDATA *prpCrc)
{
	CRCDATA *prlCrc = GetCrcByUrno(prpCrc->Urno);
	if (prlCrc != NULL)
	{
		*prlCrc = *prpCrc; //Update omData
		ogDdx.DataChanged((void *)this,CRC_CHANGE,(void *)prlCrc); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

CRCDATA *CedaCrcData::GetCrcByUrno(long lpUrno)
{
	CRCDATA  *prlCrc;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCrc) == TRUE)
	{
		return prlCrc;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaCrcData::ReadSpecialData(CCSPtrArray<CRCDATA> *popCrc,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","CRC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","CRC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popCrc != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			CRCDATA *prpCrc = new CRCDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpCrc,CString(pclFieldList))) == true)
			{
				popCrc->Add(prpCrc);
			}
			else
			{
				delete prpCrc;
			}
		}
		if(popCrc->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaCrcData::Save(CRCDATA *prpCrc)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpCrc->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpCrc->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpCrc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpCrc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCrc->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpCrc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpCrc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCrc->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessCrcCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogCrcData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaCrcData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlCrcData;
	prlCrcData = (struct BcStruct *) vpDataPointer;
	CRCDATA *prlCrc;
	if(ipDDXType == BC_CRC_NEW)
	{
		prlCrc = new CRCDATA;
		GetRecordFromItemList(prlCrc,prlCrcData->Fields,prlCrcData->Data);
		if(ValidateCrcBcData(prlCrc->Urno)) //Prf: 8795
		{
			InsertInternal(prlCrc);
		}
		else
		{
			delete prlCrc;
		}
	}
	if(ipDDXType == BC_CRC_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlCrcData->Selection);
		prlCrc = GetCrcByUrno(llUrno);
		if(prlCrc != NULL)
		{
			GetRecordFromItemList(prlCrc,prlCrcData->Fields,prlCrcData->Data);
			if(ValidateCrcBcData(prlCrc->Urno)) //Prf: 8795
			{
				UpdateInternal(prlCrc);
			}
			else
			{
				DeleteInternal(prlCrc);
			}
		}
	}
	if(ipDDXType == BC_CRC_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlCrcData->Selection);

		prlCrc = GetCrcByUrno(llUrno);
		if (prlCrc != NULL)
		{
			DeleteInternal(prlCrc);
		}
	}
}

//Prf: 8795
//--ValidateCrcBcData--------------------------------------------------------------------------------------

bool CedaCrcData::ValidateCrcBcData(const long& lrpUrno)
{
	bool blValidateCrcBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<CRCDATA> olCrcs;
		if(!ReadSpecialData(&olCrcs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateCrcBcData = false;
		}
	}
	return blValidateCrcBcData;
}

//---------------------------------------------------------------------------------------------------------
