VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UFISRecordset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Enum CommandTypeEnum
    cmdReadTable = 0
    cmdGetFlightRecords = 1
End Enum

Private Const COLUMN_SEP = ","
Private Const ROW_SEP = vbCrLf
Private Const NO_ROWS = "No data found for this view or filter."

'property variables
Private m_ColumnSeparator As String
Private m_RowSeparator As String
Private m_ColumnHeader As String
Private m_ColumnList As String
Private m_TableName As String
Private m_CommandType As CommandTypeEnum
Private m_WhereClause As Variant
Private m_OrderByClause As String
Private m_IndexHint As String
Private m_UfisServer As UfisServer
Private m_ConvertSpecialChars As Boolean
Private m_CheckOldVersionWhenConverting As Boolean
Private m_ColumnTypeList As String
Private m_ColumnSizeList As String
Private m_SortKey As String
Private m_PopulateKeyStringList As Boolean
Private m_KeyColumn As String
Private m_KeyStringList As String
Private m_UniqueKeyStringList As Boolean

Private m_NeedToCloseUfisServer As Boolean

'Private Procedures/Functions
Private Function GetColumnAttrCollection() As Collection
    Dim rsColumns As ADODB.Recordset
    Dim vntColumns As Variant
    Dim strTypeList As String
    Dim strSizeList As String
    Dim i As Long
    Dim lngRowCount As Long
    Dim vntValues As Variant
    Dim strValue As String
    
    Set rsColumns = New ADODB.Recordset
    rsColumns.Fields.Append "FINA", adVarChar, 10, adFldIsNullable
    rsColumns.Fields.Append "FTYP", adVarChar, 1, adFldIsNullable
    rsColumns.Fields.Append "FLEN", adVarChar, 10, adFldIsNullable
    rsColumns.Open
    
    vntColumns = Split(m_ColumnList, COLUMN_SEP)
    For i = 0 To UBound(vntColumns)
        rsColumns.AddNew
        rsColumns.Fields("FINA").Value = vntColumns(i)
        rsColumns.Update
    Next i
    
    lngRowCount = m_UfisServer.RowCount
    For i = 0 To lngRowCount - 1
        vntValues = Split(m_UfisServer.Row(i), COLUMN_SEP)
        
        rsColumns.Find "FINA = '" & vntValues(0) & "'", , , 1
        If Not rsColumns.EOF Then
            Select Case RTrim(vntValues(2)) 'field type
                Case "C", "VC2"
                    Select Case RTrim(vntValues(3))
                        Case "DATE", "DTIM"
                            strValue = "D"
                        Case Else
                            strValue = "C"
                        End Select
                Case Else
                    strValue = RTrim(vntValues(2))
            End Select
            rsColumns.Fields("FTYP").Value = strValue
            
            Select Case strValue
                Case "N"
                    Select Case RTrim(vntValues(3))
                        Case "LONG"
                            strValue = "L"
                        Case Else
                            If RTrim(vntValues(1)) <= 10 Then
                                strValue = "L"
                            Else
                                strValue = "N"
                            End If
                    End Select
                Case Else
                    strValue = RTrim(vntValues(1))
            End Select
            
            rsColumns.Fields("FLEN").Value = strValue
            rsColumns.Update
        End If
    Next i
            
    If Not (rsColumns.BOF And rsColumns.EOF) Then
        rsColumns.MoveFirst
    End If
    
    strTypeList = ""
    strSizeList = ""
    Do While Not rsColumns.EOF
        strTypeList = strTypeList & "," & rsColumns.Fields("FTYP").Value
        strSizeList = strSizeList & "," & rsColumns.Fields("FLEN").Value
        
        rsColumns.MoveNext
    Loop
    If strTypeList <> "" Then strTypeList = Mid(strTypeList, 2)
    If strSizeList <> "" Then strSizeList = Mid(strSizeList, 2)
    
    Set GetColumnAttrCollection = New Collection
    GetColumnAttrCollection.Add strTypeList
    GetColumnAttrCollection.Add strSizeList
End Function

'Property
Public Property Let ColumnSeparator(ByVal sColumnSeparator As String)
    m_ColumnSeparator = sColumnSeparator
End Property

Public Property Get ColumnSeparator() As String
    ColumnSeparator = m_ColumnSeparator
End Property

Public Property Let RowSeparator(ByVal sRowSeparator As String)
    m_RowSeparator = sRowSeparator
End Property

Public Property Get RowSeparator() As String
    RowSeparator = m_RowSeparator
End Property

Public Property Let ColumnList(ByVal sColumnList As String)
    m_ColumnList = sColumnList
End Property

Public Property Get ColumnList() As String
    ColumnList = m_ColumnList
End Property

Public Property Let TableName(ByVal sTableName As String)
    m_TableName = sTableName
End Property

Public Property Get TableName() As String
    TableName = m_TableName
End Property

Public Property Let CommandType(ByVal eCommandType As CommandTypeEnum)
    m_CommandType = eCommandType
End Property

Public Property Get CommandType() As CommandTypeEnum
    CommandType = m_CommandType
End Property

Public Property Let WhereClause(ByVal vWhereClause As Variant)
    m_WhereClause = vWhereClause
End Property

Public Property Get WhereClause() As Variant
    WhereClause = m_WhereClause
End Property

Public Property Let OrderByClause(ByVal sOrderByClause As String)
    m_OrderByClause = sOrderByClause
End Property

Public Property Get OrderByClause() As String
    OrderByClause = m_OrderByClause
End Property

Public Property Let IndexHint(ByVal sIndexHint As String)
    m_IndexHint = sIndexHint
End Property

Public Property Get IndexHint() As String
    IndexHint = m_IndexHint
End Property

Public Property Set UfisServer(ByVal oUfisServer As UfisServer)
    Set m_UfisServer = oUfisServer
End Property

Public Property Get UfisServer() As UfisServer
    Set UfisServer = m_UfisServer
End Property

Public Property Let ConvertSpecialChars(ByVal bConvertSpecialChars As Boolean)
    m_ConvertSpecialChars = bConvertSpecialChars
End Property

Public Property Get ConvertSpecialChars() As Boolean
    ConvertSpecialChars = m_ConvertSpecialChars
End Property

Public Property Let CheckOldVersionWhenConverting(ByVal bCheckOldVersionWhenConverting As Boolean)
    m_CheckOldVersionWhenConverting = bCheckOldVersionWhenConverting
End Property

Public Property Get CheckOldVersionWhenConverting() As Boolean
    CheckOldVersionWhenConverting = m_CheckOldVersionWhenConverting
End Property

Public Property Let ColumnTypeList(ByVal sColumnTypeList As String)
    m_ColumnTypeList = sColumnTypeList
End Property

Public Property Get ColumnTypeList() As String
    ColumnTypeList = m_ColumnTypeList
End Property

Public Property Let ColumnSizeList(ByVal sColumnSizeList As String)
    m_ColumnSizeList = sColumnSizeList
End Property

Public Property Get ColumnSizeList() As String
    ColumnSizeList = m_ColumnSizeList
End Property

Public Property Let SortKey(ByVal sSortKey As String)
    m_SortKey = sSortKey
End Property

Public Property Get SortKey() As String
    SortKey = m_SortKey
End Property

Public Property Let PopulateKeyStringList(bPopulate As Boolean)
    m_PopulateKeyStringList = bPopulate
End Property

Public Property Get PopulateKeyStringList() As Boolean
    PopulateKeyStringList = m_PopulateKeyStringList
End Property

Public Property Let KeyColumn(sKeyColumn As String)
    m_KeyColumn = sKeyColumn
End Property

Public Property Get KeyColumn() As String
    KeyColumn = m_KeyColumn
End Property

Public Property Get KeyStringList() As String
    KeyStringList = m_KeyStringList
End Property

Public Property Let UniqueKeyStringList(bUnique As Boolean)
    m_UniqueKeyStringList = bUnique
End Property

Public Property Get UniqueKeyStringList() As Boolean
    UniqueKeyStringList = m_UniqueKeyStringList
End Property

'Method
Public Function CreateRecordset(Optional ByVal UseTabObject As Boolean = False, _
    Optional DataSource) As ADODB.Recordset
    
    Const DEFAULT_VARCHAR_LENGTH = 4000
    Const MAX_VARCHAR_LENGTH = 8184

    Dim myRecordset As ADODB.Recordset
    Dim blnCanProceed As Boolean
    Dim sColumnHeader As String
    Dim sDataSource As String
    Dim sWhereClause As String
    Dim sOrderByClause As String
    Dim sWhereOrderByClause As String
    Dim sCommandType As String
    Dim i As Integer, j As Integer
    Dim fieldValues As Variant
    Dim rowValues As Variant
    Dim fieldCount As Integer
    Dim sColumnTypeList As String
    Dim sColumnSizeList As String
    Dim eFieldDataType As DataTypeEnum
    Dim columnTypeValues As Variant
    Dim columnSizeValues As Variant
    Dim fieldValue As String
    Dim columnSize As Variant
    Dim colColumnAttr As Collection
    Dim sIndexHintAndColList As String
    Dim vntDate As Variant
    
    Dim blnDataNotEmpty As Boolean
    
    'For Calling With TabObject
    Dim TabObject As Object
    Dim intInitialLineCount As Integer
    Dim lngRowCount As Long
    Dim intColCount As Integer
    
    'On Error GoTo ErrCreate

    Set myRecordset = New ADODB.Recordset
    
    'make sure all properties have been provided
    If IsMissing(DataSource) Then
        blnCanProceed = (m_TableName <> "") And (m_ColumnList <> "")
    Else
        blnCanProceed = True
    End If
    
    If blnCanProceed Then
        If IsMissing(DataSource) Then
            'check if ufissever available
            If m_UfisServer Is Nothing Then
                Set m_UfisServer = New UfisServer
                m_NeedToCloseUfisServer = True
            End If
            
            'take columntype and size from SYSTAB
            m_UfisServer.CallCedaDirect "RTA", "SYSTAB", "FINA,FELE,FITY,TYPE", "", _
                "WHERE TANA = '" & UCase(Left(m_TableName, Len(m_TableName) - 3)) & "' AND " & _
                "FINA IN ('" & Replace(m_ColumnList, ",", "','") & "')", ""
        End If
    
        'make sure columnheader sync with columnlist
        fieldCount = UBound(Split(m_ColumnList, m_ColumnSeparator)) + 1
        If fieldCount = UBound(Split(m_ColumnHeader, m_ColumnSeparator)) + 1 Then
            sColumnHeader = m_ColumnHeader
        Else
            sColumnHeader = m_ColumnList
        End If

        'make sure columntype sync with columnlist
        If fieldCount = UBound(Split(m_ColumnTypeList, m_ColumnSeparator)) + 1 Then
            sColumnTypeList = m_ColumnTypeList
        Else
            If IsMissing(DataSource) Then
                If colColumnAttr Is Nothing Then
                    Set colColumnAttr = GetColumnAttrCollection()
                End If
                sColumnTypeList = colColumnAttr.Item(1)
            End If
            If sColumnTypeList = "" Then
                sColumnTypeList = Repeat(fieldCount, "C" & m_ColumnSeparator)
                sColumnTypeList = Left(sColumnTypeList, Len(sColumnTypeList) - 1)
            End If
        End If
        
        'make sure columnsize sync with columnlist
        If fieldCount = UBound(Split(m_ColumnSizeList, m_ColumnSeparator)) + 1 Then
            sColumnSizeList = m_ColumnSizeList
        Else
            If IsMissing(DataSource) Then
                If colColumnAttr Is Nothing Then
                    Set colColumnAttr = GetColumnAttrCollection()
                End If
                sColumnSizeList = colColumnAttr.Item(2)
            End If
            If sColumnSizeList = "" Then
                sColumnSizeList = String(fieldCount - 1, ",")
            End If
        End If
            
        If IsMissing(DataSource) Then
            'get the commandtype
            If m_CommandType = cmdGetFlightRecords Then
                sCommandType = "GFR"
            Else
                sCommandType = "RTA"
            End If
            
            'check the whereclause and orderbyclause
            If m_OrderByClause <> "" And Not (m_OrderByClause Like "ORDER BY*") Then
                sOrderByClause = "ORDER BY " & m_OrderByClause
            Else
                sOrderByClause = m_OrderByClause
            End If
                
            If IsArray(m_WhereClause) Then
                UseTabObject = True
            Else
                If m_WhereClause <> "" And Not (m_WhereClause Like "WHERE*") Then
                    sWhereClause = "WHERE " & m_WhereClause
                Else
                    sWhereClause = m_WhereClause
                End If
                sWhereOrderByClause = Trim(sWhereClause & " " & sOrderByClause)
            End If
            
            'Add index hint before column list
            sIndexHintAndColList = m_ColumnList
            If m_IndexHint <> "" Then
                sIndexHintAndColList = m_IndexHint & " " & sIndexHintAndColList
            End If
            
            If UseTabObject Then
                'create TabObject and prepare the attributes
                Set TabObject = CreateObject("TAB.TABCtrl.1")
                TabObject.LogicalFieldList = m_ColumnList
                TabObject.HeaderString = m_ColumnList
                TabObject.CedaServerName = m_UfisServer.HostName
                TabObject.CedaPort = "3357"
                TabObject.CedaHopo = m_UfisServer.HOPO
                TabObject.CedaCurrentApplication = m_UfisServer.ModName
                TabObject.CedaTabext = "TAB"
                TabObject.CedaUser = m_UfisServer.UserName
                TabObject.CedaWorkstation = m_UfisServer.GetMyWorkStationName
                TabObject.CedaSendTimeout = "3"
                TabObject.CedaReceiveTimeout = "240"
                TabObject.CedaRecordSeparator = Chr(10)
                TabObject.CedaIdentifier = m_TableName
                
                'save current line count
                intInitialLineCount = TabObject.GetLineCount
                
                'Now try to get the data
                If IsArray(m_WhereClause) Then
                    Dim vntWhereClause As Variant
                    
                    For Each vntWhereClause In m_WhereClause
                        If vntWhereClause <> "" And Not (vntWhereClause Like "WHERE*") Then
                            sWhereClause = "WHERE " & vntWhereClause
                        Else
                            sWhereClause = vntWhereClause
                        End If
                        sWhereOrderByClause = Trim(sWhereClause & " " & sOrderByClause)
                        
                        TabObject.CedaAction sCommandType, m_TableName, _
                                             sIndexHintAndColList, "", sWhereOrderByClause
                    Next vntWhereClause
                Else
                    TabObject.CedaAction sCommandType, m_TableName, _
                                         sIndexHintAndColList, "", sWhereOrderByClause
                End If
            Else
                'Now try to get the data
                m_UfisServer.CallCedaDirect sCommandType, m_TableName, _
                                            sIndexHintAndColList, "", sWhereOrderByClause, ""
            End If
        Else
            sDataSource = DataSource
        End If
        
        fieldValues = Split(sColumnHeader, m_ColumnSeparator)
        columnTypeValues = Split(sColumnTypeList, m_ColumnSeparator)
        columnSizeValues = Split(sColumnSizeList, m_ColumnSeparator)
        'Create data columns accordingly
        For i = 0 To UBound(fieldValues)
            columnSize = columnSizeValues(i)
        
            Select Case columnTypeValues(i)
                Case "D", "DT"
                    eFieldDataType = adDate
                Case "N"
                    Select Case columnSize
                        Case "I", "INT", "INTEGER"
                            eFieldDataType = adInteger
                        Case "L", "LONG"
                            eFieldDataType = adBigInt
                        Case "S", "SNG", "SINGLE"
                            eFieldDataType = adSingle
                        Case Else
                            eFieldDataType = adDouble
                    End Select
                Case Else
                    columnSize = Val(columnSize)
                    If columnSize = 0 Then
                        columnSize = DEFAULT_VARCHAR_LENGTH
                    Else
                        If columnSize > MAX_VARCHAR_LENGTH Then
                            columnSize = MAX_VARCHAR_LENGTH
                        End If
                    End If
                    eFieldDataType = adVarChar
            End Select
            
            If eFieldDataType = adVarChar Then
                myRecordset.Fields.Append fieldValues(i), eFieldDataType, columnSize, adFldIsNullable
            Else
                myRecordset.Fields.Append fieldValues(i), eFieldDataType, , adFldIsNullable
            End If
        Next i
        myRecordset.Open
        
        If m_SortKey <> "" Then
            If m_ColumnSeparator <> "," Then
                myRecordset.Sort = Replace(m_SortKey, m_ColumnSeparator, ",")
            Else
                myRecordset.Sort = m_SortKey
            End If
        End If

        'Adding data to datatable
        'check if the datasource is not empty
        If sDataSource <> "" Then
            blnDataNotEmpty = True
        Else
            If UseTabObject Then
                lngRowCount = TabObject.GetLineCount - intInitialLineCount
                intColCount = TabObject.Columns
            Else
                lngRowCount = m_UfisServer.RowCount
                If lngRowCount = 1 And m_UfisServer.Row(0) = "" Then
                    lngRowCount = 0
                End If
            End If
            blnDataNotEmpty = (lngRowCount > 0)
        End If
        If blnDataNotEmpty Then
            If m_PopulateKeyStringList And m_KeyColumn <> "" Then
                m_KeyStringList = ""
            End If
            
            If sDataSource <> "" Then
                rowValues = Split(sDataSource, m_RowSeparator)
                lngRowCount = UBound(rowValues) + 1
            End If
            For j = 0 To lngRowCount - 1
                myRecordset.AddNew
                If Not UseTabObject Then
                    If sDataSource <> "" Then
                        fieldValues = Split(rowValues(j), m_ColumnSeparator)
                    Else
                        fieldValues = Split(m_UfisServer.Row(j), m_ColumnSeparator)
                    End If
                    intColCount = UBound(fieldValues) + 1
                End If
                For i = 0 To intColCount - 1
                    If UseTabObject Then
                        fieldValue = TabObject.GetColumnValue(intInitialLineCount + j, i)
                    Else
                        fieldValue = fieldValues(i)
                    End If
                    Select Case columnTypeValues(i)
                        Case "D", "DT"
                            If Trim(fieldValue) = "" Then
                                myRecordset.Fields(i).Value = Null
                            Else
                                vntDate = CedaFullDateToVb(fieldValue)
                                If IsDate(vntDate) Then
                                    myRecordset.Fields(i).Value = vntDate
                                Else
                                    myRecordset.Fields(i).Value = Null
                                End If
                            End If
                        Case "N"
                            If Trim(fieldValue) = "" Then
                                myRecordset.Fields(i).Value = Null
                            Else
                                myRecordset.Fields(i).Value = Val(fieldValue)
                            End If
                        Case Else
                            If Len(fieldValue) > myRecordset.Fields(i).DefinedSize Then
                                fieldValue = Left(fieldValue, myRecordset.Fields(i).DefinedSize)
                            End If
                            If m_ConvertSpecialChars Then
                                myRecordset.Fields(i).Value = _
                                    UfisLib.CleanString(fieldValue, _
                                                SERVER_TO_CLIENT, _
                                                m_CheckOldVersionWhenConverting)
                            Else
                                myRecordset.Fields(i).Value = fieldValue
                            End If
                            myRecordset.Fields(i).Value = RTrim(myRecordset.Fields(i).Value)
                    End Select
                Next i
                myRecordset.Update
                
                If m_PopulateKeyStringList And m_KeyColumn <> "" Then
                    If m_UniqueKeyStringList Then
                        If InStr(m_KeyStringList & ",", "," & myRecordset.Fields(m_KeyColumn).Value & ",") <= 0 Then
                            m_KeyStringList = m_KeyStringList & "," & myRecordset.Fields(m_KeyColumn).Value
                        End If
                    Else
                        m_KeyStringList = m_KeyStringList & "," & myRecordset.Fields(m_KeyColumn).Value
                    End If
                End If
            Next j
            
            If m_PopulateKeyStringList And m_KeyColumn <> "" Then
                m_KeyStringList = Mid(m_KeyStringList, 2)
            End If
        End If
    End If
    
    If Not IsMissing(DataSource) Then
        If UseTabObject Then
            Set TabObject = Nothing
        End If
    End If
    
    If Not myRecordset Is Nothing Then
        If Not (myRecordset.BOF And myRecordset.EOF) Then
            myRecordset.MoveFirst
        End If
    End If
    
    Set CreateRecordset = myRecordset
    
    Exit Function

ErrCreate:
    Set CreateRecordset = Nothing
End Function

Private Sub Class_Initialize()
    m_ColumnSeparator = COLUMN_SEP
    m_RowSeparator = ROW_SEP
    m_TableName = ""
    m_ColumnList = ""
    m_ColumnHeader = ""
    m_CommandType = cmdReadTable
    m_ColumnTypeList = ""
    m_WhereClause = ""
    m_OrderByClause = ""
    Set m_UfisServer = Nothing
    
    m_NeedToCloseUfisServer = False
End Sub

Private Sub Class_Terminate()
    If m_NeedToCloseUfisServer Then
        Set m_UfisServer = Nothing
    End If
End Sub


