VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form MainForm 
   Caption         =   "Ceda CDR Alive Connector"
   ClientHeight    =   7770
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   11610
   Icon            =   "Form1.frx":0000
   LinkMode        =   1  'Source
   LockControls    =   -1  'True
   ScaleHeight     =   7770
   ScaleWidth      =   11610
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkNetin 
      Caption         =   "Init Netin"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6510
      Style           =   1  'Graphical
      TabIndex        =   51
      Top             =   390
      Width           =   1185
   End
   Begin VB.CheckBox chkOld 
      Caption         =   "Old Style"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5280
      Style           =   1  'Graphical
      TabIndex        =   50
      Top             =   390
      Width           =   1185
   End
   Begin VB.TextBox txtEndOff 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   4800
      TabIndex        =   47
      Top             =   390
      Width           =   345
   End
   Begin VB.TextBox txtBgnOff 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   4170
      TabIndex        =   46
      Top             =   390
      Width           =   345
   End
   Begin VB.TextBox txtVpfr 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   690
      Locked          =   -1  'True
      TabIndex        =   43
      Top             =   390
      Width           =   975
   End
   Begin VB.TextBox txtVpto 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   2400
      Locked          =   -1  'True
      TabIndex        =   42
      Top             =   390
      Width           =   975
   End
   Begin VB.TextBox HostName 
      Height          =   285
      Left            =   60
      TabIndex        =   41
      Text            =   "SIN1"
      Top             =   3810
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox TblExt 
      Height          =   285
      Left            =   60
      TabIndex        =   40
      Text            =   "TAB"
      Top             =   3480
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox HOPO 
      Height          =   285
      Left            =   60
      TabIndex        =   39
      Text            =   "SIN"
      Top             =   3150
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox txtPack 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   10380
      TabIndex        =   36
      Top             =   720
      Width           =   975
   End
   Begin VB.CheckBox chkClose 
      Caption         =   "Close All"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8970
      Style           =   1  'Graphical
      TabIndex        =   35
      Top             =   60
      Width           =   1185
   End
   Begin VB.TextBox txtStart 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   11010
      TabIndex        =   34
      Text            =   "20"
      Top             =   60
      Width           =   345
   End
   Begin VB.CheckBox chkStart 
      Caption         =   "Run"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   10410
      Style           =   1  'Graphical
      TabIndex        =   33
      Top             =   60
      Width           =   585
   End
   Begin VB.CheckBox chkLock 
      Caption         =   "Lock"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   32
      Top             =   5730
      Visible         =   0   'False
      Width           =   585
   End
   Begin VB.CheckBox chkDisconnect 
      Caption         =   "Disconnect"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7740
      Style           =   1  'Graphical
      TabIndex        =   31
      Top             =   60
      Width           =   1185
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "Load Data"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6510
      Style           =   1  'Graphical
      TabIndex        =   30
      Top             =   60
      Width           =   1185
   End
   Begin VB.CheckBox chkConnect 
      Caption         =   "Connect"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5280
      Style           =   1  'Graphical
      TabIndex        =   29
      Top             =   60
      Width           =   1185
   End
   Begin VB.TextBox txtData 
      Height          =   285
      Left            =   2400
      TabIndex        =   27
      Top             =   1710
      Width           =   8955
   End
   Begin VB.TextBox txtQueue 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   4185
      TabIndex        =   25
      Top             =   60
      Width           =   975
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Left            =   120
      Top             =   2640
   End
   Begin VB.TextBox txtPort 
      Height          =   285
      Left            =   2400
      TabIndex        =   23
      Text            =   "3357"
      Top             =   60
      Width           =   975
   End
   Begin VB.TextBox txtTws 
      Height          =   285
      Left            =   5880
      TabIndex        =   21
      Top             =   720
      Width           =   1485
   End
   Begin VB.TextBox txtTwe 
      Height          =   285
      Left            =   8100
      TabIndex        =   19
      Top             =   720
      Width           =   1455
   End
   Begin VB.TextBox txtWks 
      Height          =   285
      Left            =   690
      TabIndex        =   10
      Top             =   1710
      Width           =   975
   End
   Begin VB.TextBox txtUsr 
      Height          =   285
      Left            =   690
      TabIndex        =   9
      Top             =   1380
      Width           =   975
   End
   Begin VB.TextBox txtWhe 
      Height          =   285
      Left            =   2400
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   1380
      Width           =   8955
   End
   Begin VB.TextBox txtFld 
      Height          =   285
      Left            =   2400
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   1050
      Width           =   8955
   End
   Begin VB.TextBox txtBcNum 
      Height          =   285
      Left            =   690
      TabIndex        =   6
      Top             =   1050
      Width           =   975
   End
   Begin VB.TextBox txtTbl 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   4170
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   720
      Width           =   975
   End
   Begin VB.TextBox txtCmd 
      Height          =   285
      Left            =   2400
      Locked          =   -1  'True
      TabIndex        =   4
      Top             =   720
      Width           =   975
   End
   Begin VB.TextBox txtTot 
      Height          =   285
      Left            =   690
      TabIndex        =   3
      Top             =   720
      Width           =   975
   End
   Begin VB.TextBox txtServer 
      Height          =   285
      Left            =   690
      TabIndex        =   1
      Top             =   60
      Width           =   975
   End
   Begin TABLib.TAB TAB1 
      Height          =   1815
      Left            =   690
      TabIndex        =   0
      Top             =   5730
      Visible         =   0   'False
      Width           =   10665
      _Version        =   65536
      _ExtentX        =   18812
      _ExtentY        =   3201
      _StockProps     =   64
   End
   Begin MSWinsockLib.Winsock WS1 
      Index           =   0
      Left            =   120
      Top             =   2160
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin TABLib.TAB TAB2 
      Height          =   2715
      Left            =   690
      TabIndex        =   38
      Top             =   2100
      Width           =   10665
      _Version        =   65536
      _ExtentX        =   18812
      _ExtentY        =   4789
      _StockProps     =   64
   End
   Begin VB.Label Label19 
      Alignment       =   2  'Center
      Caption         =   "<>"
      Height          =   255
      Left            =   4560
      TabIndex        =   49
      Top             =   420
      Width           =   225
   End
   Begin VB.Label Label18 
      Caption         =   "Days:"
      Height          =   255
      Left            =   3540
      TabIndex        =   48
      Top             =   420
      Width           =   585
   End
   Begin VB.Label Label17 
      Caption         =   "Load:"
      Height          =   255
      Left            =   60
      TabIndex        =   45
      Top             =   420
      Width           =   585
   End
   Begin VB.Label Label13 
      Caption         =   "End:"
      Height          =   255
      Left            =   1770
      TabIndex        =   44
      Top             =   420
      Width           =   585
   End
   Begin VB.Label Label6 
      Caption         =   "PACK:"
      Height          =   255
      Left            =   9750
      TabIndex        =   37
      Top             =   750
      Width           =   585
   End
   Begin VB.Label Label4 
      Caption         =   "ERR:"
      Height          =   255
      Left            =   1770
      TabIndex        =   28
      Top             =   1740
      Width           =   585
   End
   Begin VB.Label Label16 
      Caption         =   "Socket:"
      Height          =   255
      Left            =   3540
      TabIndex        =   26
      Top             =   90
      Width           =   555
   End
   Begin VB.Label Label15 
      Caption         =   "Port:"
      Height          =   255
      Left            =   1770
      TabIndex        =   24
      Top             =   90
      Width           =   585
   End
   Begin VB.Label Label14 
      Caption         =   "TWS:"
      Height          =   255
      Left            =   5310
      TabIndex        =   22
      Top             =   750
      Width           =   585
   End
   Begin VB.Label Label12 
      Caption         =   "TWE:"
      Height          =   255
      Left            =   7530
      TabIndex        =   20
      Top             =   750
      Width           =   585
   End
   Begin VB.Label Label11 
      Caption         =   "WKS:"
      Height          =   255
      Left            =   60
      TabIndex        =   18
      Top             =   1740
      Width           =   585
   End
   Begin VB.Label Label10 
      Caption         =   "USR:"
      Height          =   255
      Left            =   60
      TabIndex        =   17
      Top             =   1410
      Width           =   585
   End
   Begin VB.Label Label9 
      Caption         =   "WHE:"
      Height          =   255
      Left            =   1770
      TabIndex        =   16
      Top             =   1410
      Width           =   585
   End
   Begin VB.Label Label8 
      Caption         =   "FLD:"
      Height          =   255
      Left            =   1770
      TabIndex        =   15
      Top             =   1080
      Width           =   585
   End
   Begin VB.Label Label7 
      Caption         =   "BYTES"
      Height          =   255
      Left            =   60
      TabIndex        =   14
      Top             =   1080
      Width           =   585
   End
   Begin VB.Label Label5 
      Caption         =   "TBL:"
      Height          =   255
      Left            =   3540
      TabIndex        =   13
      Top             =   750
      Width           =   585
   End
   Begin VB.Label Label3 
      Caption         =   "CMD:"
      Height          =   255
      Left            =   1770
      TabIndex        =   12
      Top             =   750
      Width           =   585
   End
   Begin VB.Label Label2 
      Caption         =   "COUNT"
      Height          =   255
      Left            =   60
      TabIndex        =   11
      Top             =   750
      Width           =   585
   End
   Begin VB.Label Label1 
      Caption         =   "Server:"
      Height          =   255
      Left            =   60
      TabIndex        =   2
      Top             =   90
      Width           =   585
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim TotalBuffer As String
Dim LastError As String
Dim CdrTransAction As Integer
Dim LastBcNum As Long
Dim CloseAllSent As Boolean
Dim StartedAsChild
Dim OutMsg As String
Dim TotPacks As Long
Dim TotBytes As Long
Dim SyncFile As String
Dim CdrOldStyle As Boolean
Dim DataComplete As Boolean
Dim UseNetin As Boolean

Private Sub chkClose_Click()
    Dim TcpAdr As String
    Dim TcpPort As String
    If chkClose.Value = 1 Then
        chkClose.BackColor = LightGreen
        Open SyncFile For Output As #1
        Close 1
        Screen.MousePointer = 11
        Timer1.Enabled = True
    Else
        chkClose.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkConnect_Click()
    Dim TcpAdr As String
    Dim TcpPort As String
    If chkConnect.Value = 1 Then
        chkConnect.BackColor = LightGreen
        chkDisconnect.Value = 0
        If chkNetin.Value = 0 Then
            If WS1(0).State <> sckClosed Then WS1(0).Close
            TotalBuffer = ""
            TcpAdr = txtServer.Text
            TcpPort = txtPort.Text
            WS1(0).RemoteHost = TcpAdr
            WS1(0).RemotePort = TcpPort
            WS1(0).Connect TcpAdr, TcpPort
            If Not CdrOldStyle Then
                CdrTransAction = 0
                chkFilter.Enabled = True
                chkDisconnect.Enabled = True
            End If
        Else
            UfisServer.ConnectToCeda
            chkDisconnect.Enabled = True
        End If
    Else
        chkDisconnect.Value = 1
        chkConnect.Caption = "Connect"
        If Not CdrOldStyle Then chkFilter.Enabled = False
        chkDisconnect.Enabled = False
        chkConnect.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkDisconnect_Click()
    If chkDisconnect.Value = 1 Then
        chkDisconnect.BackColor = LightGreen
        If Not UseNetin Then
            If Not CdrOldStyle Then
                WS1(0).SendData "{=TOT=}000000028{=CMD=}CLOSE"
                Timer1.Enabled = True
                chkConnect.Value = 0
            End If
        Else
            UfisServer.DisconnectFromCeda
            chkConnect.Value = 0
        End If
        chkDisconnect.Value = 0
    Else
        chkDisconnect.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkFilter_Click()
    If chkFilter.Value = 1 Then
        chkFilter.BackColor = LightGreen
        CdrTransAction = 1
        InitCedaFields "AFT"
        BuildOutMsg
        InitDataTab
        TotPacks = 0
        TotBytes = 0
        If Not CdrOldStyle Then
            WS1(0).SendData OutMsg
        Else
            chkConnect.Value = 1
        End If
        chkFilter.Enabled = False
    Else
        chkFilter.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkLock_Click()
    If chkLock.Value = 1 Then
        chkLock.BackColor = LightGreen
        TAB1.LockScroll True
    Else
        TAB1.LockScroll False
        chkLock.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkNetin_Click()
    If chkNetin.Value = 1 Then
        chkNetin.BackColor = LightGreen
        Me.Caption = "Ceda Multiple Netin Test"
        Load UfisServer
        UfisServer.aCeda.StayConnected = True
        UseNetin = True
        chkConnect.Value = 1
    Else
        chkConnect.Value = 0
        UseNetin = False
        Me.Caption = "Ceda CDR Alive Connector"
        chkNetin.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkOld_Click()
    If chkOld.Value = 1 Then
        chkOld.BackColor = LightGreen
        CdrOldStyle = True
        txtData.Text = ""
        chkConnect.Value = 0
        chkConnect.Enabled = False
        chkDisconnect.Enabled = False
        chkFilter.Enabled = True
    Else
        txtQueue.Text = ""
        CdrOldStyle = False
        txtData.Text = ""
        chkFilter.Enabled = False
        chkConnect.Enabled = True
        chkConnect.Value = 0
        chkOld.BackColor = vbButtonFace
    End If
    WS1_Close 0
End Sub

Private Sub chkStart_Click()
    Dim i As Integer
    Dim cnt As Integer
    Dim myAppl As String
    Dim myPara As String
    If chkStart.Value = 1 Then
        myPara = "{=BGN=}" & txtBgnOff.Text & "{=END=}" & txtEndOff.Text & "{=STYLE=}" & CStr(chkOld.Value)
        myAppl = App.Path & "\" & App.EXEName & ".exe CHILD " & myPara
        cnt = Val(txtStart.Text)
        If InStr(Command, "CHILD") = 0 Then cnt = cnt - 1
        For i = 1 To cnt
            Shell myAppl, vbNormalFocus
        Next
        chkStart.Value = 0
    End If
End Sub

Private Sub Form_Activate()
    Static IsActive As Boolean
    Dim tmpCheck As String
    Dim tmpData As String
    If Not IsActive Then
        txtServer.Text = HostName.Text
        Me.Refresh
        'SyncFile = "c:\tmp\CloseAllCdr.txt"
        SyncFile = UFIS_TMP & "\CloseAllCdr.txt"
        InitCedaFields "INIT"
        If InStr(Command, "CHILD") > 0 Then
            StartedAsChild = True
            chkConnect.Value = 1
            GetKeyItem tmpData, Command, "{=STYLE=}", "{="
            chkOld.Value = Val(tmpData)
            GetKeyItem tmpData, Command, "{=BGN=}", "{="
            txtBgnOff.Text = CStr(Val(tmpData))
            GetKeyItem tmpData, Command, "{=END=}", "{="
            txtEndOff.Text = CStr(Val(tmpData))
        Else
            StartedAsChild = False
            tmpCheck = Dir(SyncFile)
            If tmpCheck <> "" Then Kill SyncFile
            txtBgnOff.Text = "0"
            txtEndOff.Text = "0"
            chkConnect.Value = 1
        End If
        Timer1.Interval = 1000
        Timer1.Enabled = True
        IsActive = True
    End If
End Sub

Private Sub Form_Load()
    chkFilter.Enabled = False
    chkDisconnect.Enabled = False
    chkNetin.Value = 1
    HOPO.Text = GetIniEntry("", "GLOBAL", "", "HOMEAIRPORT", "???")
    TblExt.Text = GetIniEntry("", "GLOBAL", "", "TABLEEXTENSION", "TAB")
    HostName.Text = GetIniEntry("", "GLOBAL", "", "HOSTNAME", "")
    TAB1.ResetContent
    TAB1.HeaderString = "Received BroadCast KeyItem List"
    TAB1.HeaderLengthString = "1000"
    TAB1.SetFieldSeparator Chr(15)
    TAB1.SetHeaderFont 17, False, False, True, 0, "Courier New"
    TAB1.AutoSizeByHeader = True
    TAB1.AutoSizeColumns
    TAB2.ResetContent
    TAB2.HeaderString = "FETCHED TABLE DATA"
    TAB2.HeaderLengthString = "1000"
    'TAB2.SetHeaderFont 17, False, False, False, 0, "Courier New"
    TAB2.AutoSizeByHeader = True
    'TAB2.AutoSizeColumns
End Sub

Private Sub Form_Resize()
    Dim newSize As Long
    newSize = Me.ScaleHeight - TAB2.Top - 240
    If newSize > 600 Then TAB2.Height = newSize
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If chkNetin.Value = 1 Then
        UfisServer.DisconnectFromCeda
    End If
    End
End Sub

Private Sub Timer1_Timer()
    Dim tmpTag As String
    Dim tmpCheck As String
    On Error GoTo ErrHdl
    tmpTag = Timer1.Tag
    Select Case tmpTag
        Case ""
            tmpCheck = Dir(SyncFile)
            If tmpCheck <> "" Then
                chkConnect.Value = 0
                Timer1.Tag = "DOWN"
            End If
        Case "DOWN"
            End
        Case Else
    End Select
ErrHdl:

End Sub

Private Sub txtBgnOff_Change()
    InitFromTo
End Sub

Private Sub txtBgnOff_GotFocus()
    SetTextSelected
End Sub

Private Sub txtEndOff_Change()
    InitFromTo
End Sub

Private Sub txtEndOff_GotFocus()
    SetTextSelected
End Sub

Private Sub WS1_Close(Index As Integer)
    If WS1(Index).State <> sckClosed Then WS1(Index).Close
    txtQueue.Text = "CLOSED"
    chkConnect.Value = 0
End Sub

Private Sub WS1_Connect(Index As Integer)
    Dim tmpTxt As String
    txtQueue.Text = "OPEN"
    TotalBuffer = ""
    chkConnect.Caption = "Connected"
    Select Case CdrTransAction
        Case 0
            SendDataToBcOut Index, "KEEP", ""
        Case 1
            If CdrOldStyle Then WS1(0).SendData OutMsg
        Case Else
    End Select
End Sub

Private Sub WS1_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Dim GotMessage As Boolean
    Dim Data As String
    Dim tmpData As String
    Dim tmpResult As String
    Dim datalen As Integer
    Dim TotalLen As Long
    Dim Tot As String

    DataComplete = False
    WS1(Index).GetData Data, vbString
    TotalBuffer = TotalBuffer & Data
    GotMessage = True
    While GotMessage
        GotMessage = False
        If Len(TotalBuffer) >= 16 Then
            GetKeyItem Tot, TotalBuffer, "{=TOT=}", "{="
            TotalLen = Val(Tot)
            If Len(TotalBuffer) >= TotalLen Then
                GotMessage = True
                Data = Left(TotalBuffer, TotalLen)
                TotalBuffer = Mid(TotalBuffer, TotalLen + 1)
                GetKeyItem tmpData, Data, "{=ERR=}", "{="
                txtData.Text = tmpData
                
                Select Case CdrTransAction
                    Case 0
                        If Not CdrOldStyle Then
                            DataComplete = True
                            SendAckToCdrhdl
                            GetKeyItem tmpData, Data, "{=ALIVE=}", "{="
                            If tmpData <> "" Then
                                txtQueue.Text = tmpData
                            Else
                                chkOld.Value = 1
                            End If
                        End If
                    Case 1
                        GetKeyItem tmpResult, Data, "{=DAT=}", "{="
                        TAB2.InsertBuffer tmpResult, vbLf
                        GetKeyItem tmpData, Data, "{=PACK=}", "{="
                        TotPacks = TotPacks + 1
                        TotBytes = TotBytes + Len(tmpResult)
                        txtPack.Text = CStr(TotPacks)
                        txtPack.Refresh
                        txtTot.Text = CStr(TAB2.GetLineCount)
                        txtTot.Refresh
                        TAB2.OnVScrollTo TAB2.GetLineCount
                        If Val(tmpData) < Val(txtPack.Tag) Then
                            DataComplete = True
                            SendAckToCdrhdl
                            TAB2.OnVScrollTo 0
                            TAB2.AutoSizeColumns
                            TAB2.Refresh
                            txtTot.Text = CStr(TAB2.GetLineCount)
                            txtBcNum.Text = CStr(TotBytes)
                            chkFilter.Enabled = True
                            chkFilter.Value = 0
                        End If
                    Case Else
                End Select
            End If
        End If
    Wend
    
   
End Sub

Private Sub SendAckToCdrhdl()
    WS1(0).SendData "{=TOT=}000000026{=ACK=}ACK"
    If (DataComplete) And (CdrOldStyle) Then chkConnect.Value = 0
End Sub

Private Function GetKeyItem(ByRef rResult, ByRef rString, ByVal vStartSep, ByVal vEndSep) As Boolean
    Dim llPosStart As Long
    Dim llPosEnd As Long
    GetKeyItem = False
    rResult = ""
    llPosStart = InStr(1, rString, vStartSep, vbTextCompare)
    If llPosStart > 0 Then
        GetKeyItem = True
        llPosStart = llPosStart + Len(vStartSep)
        llPosEnd = InStr(llPosStart, rString, vEndSep, vbTextCompare)
        If llPosEnd > 0 Then
            rResult = Mid(rString, llPosStart, (llPosEnd - llPosStart))
        Else
            rResult = Right(rString, Len(rString) - llPosStart + 1)
        End If
    End If
End Function
                    
Private Sub WS1_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    Dim tmpMsg As String
    tmpMsg = "WS" & CStr(Index) & " (ERR" & CStr(Number) & "): " & Description
    txtData.Text = tmpMsg
    Select Case Index
        Case 0
            'If Timer1.Tag = "DOWN" Then chkDisconnect.Value = 1
        Case Else
    End Select
End Sub

Private Sub SendDataToBcOut(Index As Integer, BcCmd As String, Data As String)
    Dim tmpStr As String
    Dim tmpLen As Long
    tmpStr = Space(16)
    tmpStr = tmpStr + "{=CMD=}" + BcCmd
    If Data <> "" Then tmpStr = tmpStr + "{=DAT=}" + Data
    tmpLen = Len(tmpStr)
    Mid(tmpStr, 1, 16) = Left("{=TOT=}" & CStr(tmpLen) & "                  ", 16)
    WS1(Index).SendData tmpStr
End Sub

Private Function GetHexTcpIp() As String
    Dim MyTcpIpAdr As String
    Dim HexTcpAdr As String
    Dim tmpdat As String
    Dim tmpVal As Integer
    Dim ItmNbr As Integer
    
    MyTcpIpAdr = WS1(0).LocalIP
    HexTcpAdr = ""
    For ItmNbr = 1 To 4
        tmpVal = Val(GetItem(MyTcpIpAdr, ItmNbr, "."))
        tmpdat = Hex(tmpVal)
        HexTcpAdr = HexTcpAdr + Right("00" + tmpdat, 2)
    Next
    GetHexTcpIp = HexTcpAdr
End Function

Private Sub InitFromTo()
    Dim FrameDay
    Dim Days As Integer
    Days = Abs(Val(txtBgnOff.Text))
    FrameDay = DateAdd("d", -Days, Now)
    txtVpfr.Text = Format(FrameDay, "yyyymmdd")
    Days = Abs(Val(txtEndOff.Text))
    FrameDay = DateAdd("d", Days, Now)
    txtVpto.Text = Format(FrameDay, "yyyymmdd")
End Sub

Private Sub InitCedaFields(ForWhat As String)
    Dim tmpTxt As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    txtWks.Text = GetHexTcpIp()
    txtUsr.Text = "CdrTest"
    txtTws.Text = CStr(App.hInstance)
    txtTwe.Text = "SIN,TAB,CdrAlive"
    Select Case ForWhat
        Case "AFT"
            txtTbl.Text = "AFTTAB"
            txtCmd.Text = "GFR"
            txtFld.Text = UCase("Urno,Fdat,Flno,Alc2,Alc3,Fltn,Flns,Adid,Org3,Des3,Des4,Via3,Vian,Vial,Gta1,Gta2,Gtd1,Gtd2,Regn,Act3,Nose,Stod,Stoa,Etdi,Etai,Tifd,Tifa,Land,Slot,Airb,Onbl,Onbe,Ofbl,Tmoa,Tisd,Tisa,Styp,Dcd1,Dtd1,Dcd2,Dtd2,Nxti,Iskd,Ftyp,Psta,Pstd,Paxt,Pax2,Pax3,Lstu,Rkey,Stev,Skey,Jfno,Jcnt,Act5,Ttyp,Paxt,Cgot,Bagn,Mail,Blt1,Baz1,Ddlf,Bagw,Hapx,Hara")
            tmpTxt = "((ADID = 'A' AND TIFA BETWEEN 'VPFR' AND 'VPTO') OR (ADID = 'D' AND TIFD BETWEEN 'VPFR' AND 'VPTO'))"
            tmpVpfr = txtVpfr & "000000"
            tmpVpto = txtVpto & "235959"
            tmpTxt = Replace(tmpTxt, "VPFR", tmpVpfr, 1, -1, vbBinaryCompare)
            tmpTxt = Replace(tmpTxt, "VPTO", tmpVpto, 1, -1, vbBinaryCompare)
            txtWhe.Text = tmpTxt
            txtPack.Text = "1000"
        Case Else
    End Select
End Sub

Private Sub BuildOutMsg()
    Dim tmpCdrBuf As String
    Dim tmpLen As Integer
    Dim tmpTot As String
    tmpCdrBuf = ""
    tmpCdrBuf = tmpCdrBuf & "{=CMD=}" & txtCmd.Text
    tmpCdrBuf = tmpCdrBuf & "{=WKS=}" & txtWks.Text
    tmpCdrBuf = tmpCdrBuf & "{=USR=}" & txtUsr.Text
    tmpCdrBuf = tmpCdrBuf & "{=PACK=}" & txtPack.Text
    tmpCdrBuf = tmpCdrBuf & "{=TBL=}" & txtTbl.Text
    tmpCdrBuf = tmpCdrBuf & "{=FLD=}" & txtFld.Text
    tmpCdrBuf = tmpCdrBuf & "{=WHE=}WHERE " & txtWhe.Text
    tmpLen = Len(tmpCdrBuf) + 16
    tmpTot = "{=TOT=}" & Right("000000000" & CStr(tmpLen), 9)
    OutMsg = tmpTot & tmpCdrBuf
    txtPack.Tag = txtPack.Text
End Sub

Private Sub InitDataTab()
    TAB2.ResetContent
    TAB2.HeaderString = txtFld.Text
    TAB2.ShowHorzScroller True
    TAB2.AutoSizeByHeader = True
    TAB2.AutoSizeColumns
    TAB2.Refresh
End Sub

