VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form MainForm 
   Caption         =   "UFIS IMBIS TCP/IP Bridge Client"
   ClientHeight    =   7770
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   11610
   Icon            =   "Form1.frx":0000
   LinkMode        =   1  'Source
   LockControls    =   -1  'True
   ScaleHeight     =   7770
   ScaleWidth      =   11610
   StartUpPosition =   3  'Windows Default
   Begin VB.OptionButton Option1 
      Caption         =   "5BYTE_HEAD"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   4
      Left            =   10140
      TabIndex        =   27
      Top             =   1440
      Value           =   -1  'True
      Width           =   1455
   End
   Begin VB.OptionButton Option1 
      Caption         =   "10BYTE_HEAD"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   3
      Left            =   10140
      TabIndex        =   15
      Top             =   1200
      Width           =   1455
   End
   Begin VB.OptionButton Option1 
      Caption         =   "TCP_INFO"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   2
      Left            =   10140
      TabIndex        =   14
      Top             =   480
      Width           =   1455
   End
   Begin VB.OptionButton Option1 
      Caption         =   "GOCC-BRIDGE"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   10140
      TabIndex        =   13
      Top             =   960
      Width           =   1455
   End
   Begin VB.OptionButton Option1 
      Caption         =   "UFIS_BRIDGE"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   10140
      TabIndex        =   12
      Top             =   720
      Width           =   1455
   End
   Begin VB.TextBox txtMsg 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Left            =   90
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   11
      Top             =   480
      Width           =   9945
   End
   Begin VB.TextBox HostName 
      Height          =   285
      Left            =   6030
      TabIndex        =   10
      Text            =   "SIN1"
      Top             =   3780
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox TblExt 
      Height          =   285
      Left            =   6030
      TabIndex        =   9
      Text            =   "TAB"
      Top             =   3510
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.TextBox HOPO 
      Height          =   285
      Left            =   6030
      TabIndex        =   8
      Text            =   "SIN"
      Top             =   3180
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.CheckBox chkDisconnect 
      Caption         =   "Disconnect"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   7200
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   105
      Width           =   1140
   End
   Begin VB.CheckBox chkConnect 
      Caption         =   "Connect"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   6030
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   105
      Width           =   1140
   End
   Begin VB.TextBox txtQueue 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   0
      Left            =   4995
      TabIndex        =   4
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox txtPort 
      Height          =   285
      Index           =   0
      Left            =   3210
      TabIndex        =   2
      Text            =   "3368"
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox txtServer 
      Height          =   285
      Index           =   0
      Left            =   1110
      TabIndex        =   0
      Top             =   120
      Width           =   1395
   End
   Begin MSWinsockLib.Winsock WS1 
      Index           =   0
      Left            =   6090
      Top             =   4170
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin MSWinsockLib.Winsock WS1 
      Index           =   1
      Left            =   6090
      Top             =   4620
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.PictureBox ButtonPanel 
      BorderStyle     =   0  'None
      Height          =   405
      Index           =   1
      Left            =   0
      ScaleHeight     =   405
      ScaleWidth      =   11565
      TabIndex        =   16
      Top             =   7350
      Width           =   11565
      Begin VB.CheckBox Check1 
         Caption         =   "Auto Transmit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9540
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   0
         Width           =   1425
      End
      Begin VB.TextBox txtQueue 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   4995
         TabIndex        =   22
         Top             =   15
         Width           =   975
      End
      Begin VB.TextBox txtPort 
         Height          =   285
         Index           =   1
         Left            =   3210
         TabIndex        =   21
         Text            =   "3367"
         Top             =   15
         Width           =   975
      End
      Begin VB.TextBox txtServer 
         Height          =   285
         Index           =   1
         Left            =   1110
         TabIndex        =   20
         Top             =   15
         Width           =   1395
      End
      Begin VB.CheckBox chkSendMsg 
         Caption         =   "Send MSG"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8370
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   0
         Width           =   1140
      End
      Begin VB.CheckBox chkConnect 
         Caption         =   "Connect"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   6030
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   0
         Width           =   1140
      End
      Begin VB.CheckBox chkDisconnect 
         Caption         =   "Disconnect"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   7200
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   0
         Width           =   1140
      End
      Begin VB.Label Label16 
         Caption         =   "Socket:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   4350
         TabIndex        =   25
         Top             =   45
         Width           =   555
      End
      Begin VB.Label Label15 
         Caption         =   "Port:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   2700
         TabIndex        =   24
         Top             =   45
         Width           =   585
      End
      Begin VB.Label Label1 
         Caption         =   "Send to:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   1
         Left            =   120
         TabIndex        =   23
         Top             =   45
         Width           =   915
      End
   End
   Begin VB.Label Label16 
      Caption         =   "Socket:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   4350
      TabIndex        =   5
      Top             =   150
      Width           =   555
   End
   Begin VB.Label Label15 
      Caption         =   "Port:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   2640
      TabIndex        =   3
      Top             =   150
      Width           =   585
   End
   Begin VB.Label Label1 
      Caption         =   "Read from:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   150
      Width           =   1365
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim TotalBuffer As String
Dim LastError As String
Dim LastBcNum As Long
Dim CloseAllSent As Boolean
Dim StartedAsChild
Dim OutMsg As String
Dim TotPacks As Long
Dim TotBytes As Long
Dim CdrOldStyle As Boolean
Dim DataComplete As Boolean

Private Sub Check1_Click()
    If Check1.Value = 1 Then
        Check1.BackColor = LightGreen
    Else
        Check1.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkConnect_Click(Index As Integer)
    Dim TcpAdr As String
    Dim TcpPort As String
    If chkConnect(Index).Value = 1 Then
        chkConnect(Index).BackColor = LightGreen
        chkDisconnect(Index).Value = 0
        If WS1(Index).State <> sckClosed Then WS1(Index).Close
        TotalBuffer = ""
        TcpAdr = txtServer(Index).Text
        TcpPort = txtPort(Index).Text
        WS1(Index).RemoteHost = TcpAdr
        WS1(Index).RemotePort = TcpPort
        WS1(Index).Connect TcpAdr, TcpPort
    Else
        chkDisconnect(Index).Value = 1
        chkConnect(Index).Caption = "Connect"
        chkConnect(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkDisconnect_Click(Index As Integer)
    If chkDisconnect(Index).Value = 1 Then
        chkDisconnect(Index).BackColor = LightGreen
        If WS1(Index).State <> sckClosed Then WS1(Index).Close
        txtQueue(Index).Text = "CLOSED"
        chkConnect(Index).Value = 0
        chkDisconnect(Index).Value = 0
    Else
        chkDisconnect(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkSendMsg_Click()
    Dim tmpMsg As String
    Dim tmpHead As String
    Dim MsgLen As String
    Dim Index As Integer
    If chkSendMsg.Value = 1 Then
        Index = 1
        tmpMsg = txtMsg.Text
        tmpMsg = Replace(tmpMsg, vbCr, "", 1, -1, vbBinaryCompare)
        If Option1(0).Value = True Then
        ElseIf Option1(1).Value = True Then
        ElseIf Option1(2).Value = True Then
        ElseIf Option1(3).Value = True Then
            MsgLen = Len(tmpMsg)
            tmpHead = Right("0000000000" & CStr(MsgLen), 10)
            tmpMsg = tmpHead & tmpMsg
        ElseIf Option1(4).Value = True Then
            MsgLen = Len(tmpMsg)
            tmpHead = Right("0000000000" & CStr(MsgLen), 5)
            tmpMsg = tmpHead & tmpMsg
        End If
        If WS1(Index).State = sckConnected Then
            WS1(Index).SendData tmpMsg
        End If
        chkSendMsg.Value = 0
    Else
    End If
End Sub

Private Sub Form_Activate()
    Static IsActive As Boolean
    Dim tmpCheck As String
    Dim tmpData As String
    If Not IsActive Then
        txtServer(0).Text = HostName.Text
        txtServer(1).Text = HostName.Text
        Me.Refresh
        StartedAsChild = False
        'Timer1.Interval = 1000
        'Timer1.Enabled = True
        IsActive = True
    End If
End Sub

Private Sub Form_Load()
    HOPO.Text = GetIniEntry("", "GLOBAL", "", "HOMEAIRPORT", "???")
    TblExt.Text = GetIniEntry("", "GLOBAL", "", "TABLEEXTENSION", "TAB")
    HostName.Text = GetIniEntry("", "GLOBAL", "", "HOSTNAME", "")
End Sub

Private Sub Form_Resize()
    Dim newSize As Long
    newSize = Option1(0).Left - txtMsg.Left - 150
    If newSize > 300 Then txtMsg.Width = newSize
    ButtonPanel(1).Top = Me.ScaleHeight - ButtonPanel(1).Height
    newSize = ButtonPanel(1).Top - txtMsg.Top - 150
    If newSize > 300 Then txtMsg.Height = newSize
End Sub

Private Sub txtBgnOff_Change()
    InitFromTo
End Sub

Private Sub txtBgnOff_GotFocus()
    SetTextSelected
End Sub

Private Sub txtEndOff_Change()
    InitFromTo
End Sub

Private Sub txtEndOff_GotFocus()
    SetTextSelected
End Sub

Private Sub txtMsg_Change()
    Dim tmpText As String
    tmpText = txtMsg.Text
    'txtTot.Text = CStr(Len(tmpText))
    tmpText = Replace(tmpText, vbCr, "", 1, -1, vbBinaryCompare)
End Sub

Private Sub WS1_Close(Index As Integer)
    If WS1(Index).State <> sckClosed Then WS1(Index).Close
    txtQueue(Index).Text = "CLOSED"
    chkConnect(Index).Value = 0
End Sub

Private Sub WS1_Connect(Index As Integer)
    Dim tmpTxt As String
    txtQueue(Index).Text = "OPEN"
    TotalBuffer = ""
    chkConnect(Index).Caption = "Connected"
End Sub

Private Sub WS1_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Dim GotMessage As Boolean
    Dim Data As String
    Dim tmpData As String
    Dim tmpResult As String
    Dim DataLen As Integer
    Dim TotalLen As Long
    Dim Tot As String

    DataComplete = False
    WS1(Index).GetData Data, vbString
    TotalBuffer = TotalBuffer & Data
    GotMessage = True
    While GotMessage
        GotMessage = False
        If Option1(3).Value = True Then
            If Len(TotalBuffer) >= 10 Then
                Tot = Left(TotalBuffer, 10)
                TotalBuffer = Mid(TotalBuffer, 11)
                TotalLen = Val(Tot)
                If Len(TotalBuffer) >= TotalLen Then
                    GotMessage = True
                    Data = Left(TotalBuffer, TotalLen)
                    TotalBuffer = Mid(TotalBuffer, TotalLen + 1)
                    Data = Replace(Data, vbLf, vbNewLine, 1, -1, vbBinaryCompare)
                    txtMsg.Text = Data
                    If Check1.Value = 1 Then
                        chkSendMsg.Value = 1
                    End If
                End If
            End If
        ElseIf Option1(4).Value = True Then
            If Len(TotalBuffer) >= 5 Then
                Tot = Left(TotalBuffer, 5)
                TotalBuffer = Mid(TotalBuffer, 6)
                TotalLen = Val(Tot)
                If Len(TotalBuffer) >= TotalLen Then
                    GotMessage = True
                    Data = Left(TotalBuffer, TotalLen)
                    TotalBuffer = Mid(TotalBuffer, TotalLen + 1)
                    Data = Replace(Data, vbLf, vbNewLine, 1, -1, vbBinaryCompare)
                    txtMsg.Text = Data
                    If Check1.Value = 1 Then
                        chkSendMsg.Value = 1
                    End If
                End If
            End If
        End If
    Wend
    'txtMsg.Text = TotalBuffer
    'TotalBuffer = ""
End Sub

Private Sub SendAckToCdrhdl()
    'WS1(Index).SendData "{=TOT=}000000026{=ACK=}ACK"
    'If (DataComplete) And (CdrOldStyle) Then chkConnect(Index).Value = 0
End Sub

Private Function GetKeyItem(ByRef rResult, ByRef rString, ByVal vStartSep, ByVal vEndSep) As Boolean
    Dim llPosStart As Long
    Dim llPosEnd As Long
    GetKeyItem = False
    rResult = ""
    llPosStart = InStr(1, rString, vStartSep, vbTextCompare)
    If llPosStart > 0 Then
        GetKeyItem = True
        llPosStart = llPosStart + Len(vStartSep)
        llPosEnd = InStr(llPosStart, rString, vEndSep, vbTextCompare)
        If llPosEnd > 0 Then
            rResult = Mid(rString, llPosStart, (llPosEnd - llPosStart))
        Else
            rResult = Right(rString, Len(rString) - llPosStart + 1)
        End If
    End If
End Function
                    
Private Sub WS1_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    Dim tmpMsg As String
    tmpMsg = "WS" & CStr(Index) & " (ERR" & CStr(Number) & "): " & Description
    If WS1(Index).State <> sckClosed Then WS1(Index).Close
    chkConnect(Index).Value = 0
End Sub

Private Sub SendDataToBcOut(Index As Integer, BcCmd As String, Data As String)
    Dim tmpStr As String
    Dim tmpLen As Long
    'tmpStr = Space(16)
    'tmpStr = tmpStr + "{=CMD=}" + BcCmd
    'If Data <> "" Then tmpStr = tmpStr + "{=DAT=}" + Data
    'tmpLen = Len(tmpStr)
    'Mid(tmpStr, 1, 16) = Left("{=TOT=}" & CStr(tmpLen) & "                  ", 16)
    'WS1(Index).SendData tmpStr
End Sub

Private Function GetHexTcpIp() As String
    Dim MyTcpIpAdr As String
    Dim HexTcpAdr As String
    Dim tmpdat As String
    Dim tmpVal As Integer
    Dim ItmNbr As Integer
    
    MyTcpIpAdr = WS1(0).LocalIP
    HexTcpAdr = ""
    For ItmNbr = 1 To 4
        tmpVal = Val(GetItem(MyTcpIpAdr, ItmNbr, "."))
        tmpdat = Hex(tmpVal)
        HexTcpAdr = HexTcpAdr + Right("00" + tmpdat, 2)
    Next
    GetHexTcpIp = HexTcpAdr
End Function

Private Sub InitFromTo()
    Dim FrameDay
    Dim Days As Integer
    'Days = Abs(Val(txtBgnOff.Text))
    'FrameDay = DateAdd("d", -Days, Now)
    'txtVpfr.Text = Format(FrameDay, "yyyymmdd")
    'Days = Abs(Val(txtEndOff.Text))
    'FrameDay = DateAdd("d", Days, Now)
    'txtVpto.Text = Format(FrameDay, "yyyymmdd")
End Sub

Private Sub BuildOutMsg()
    Dim tmpCdrBuf As String
    Dim tmpLen As Integer
    Dim tmpTot As String
'    tmpCdrBuf = ""
'    tmpCdrBuf = tmpCdrBuf & "{=CMD=}" & txtCmd.Text
'    tmpCdrBuf = tmpCdrBuf & "{=WKS=}" & txtWks.Text
'    tmpCdrBuf = tmpCdrBuf & "{=USR=}" & txtUsr.Text
'    tmpCdrBuf = tmpCdrBuf & "{=PACK=}" & txtPack.Text
'    tmpCdrBuf = tmpCdrBuf & "{=TBL=}" & txtTbl.Text
'    tmpCdrBuf = tmpCdrBuf & "{=FLD=}" & txtFld.Text
'    tmpCdrBuf = tmpCdrBuf & "{=WHE=}WHERE " & txtWhe.Text
'    tmpLen = Len(tmpCdrBuf) + 16
'    tmpTot = "{=TOT=}" & Right("000000000" & CStr(tmpLen), 9)
'    OutMsg = tmpTot & tmpCdrBuf
'    txtPack.Tag = txtPack.Text
End Sub

