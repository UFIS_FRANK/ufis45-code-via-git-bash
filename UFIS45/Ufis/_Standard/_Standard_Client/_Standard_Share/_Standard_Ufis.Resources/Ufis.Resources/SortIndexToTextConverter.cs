﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using DevExpress.Xpf.Grid;

namespace Ufis.Resources
{
    /// <summary>
    /// Converts SortIndex to string text.
    /// </summary>
    public class SortIndexToTextConverter : IMultiValueConverter
    {
        #region IValueConverter Members

        public object Convert(object[] value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            string strSortIndexText = string.Empty;

            DataControlBase dataControlBase = (DataControlBase)value[0];
            int sortIndex = (int)value[1];
            if (dataControlBase is GridControl)
            {
                GridControl gridControl = (GridControl)dataControlBase;
                if (gridControl.SortInfo.Count - gridControl.GroupCount > 1)
                {
                    if (sortIndex >= 0)
                        strSortIndexText = (++sortIndex).ToString();
                }
            }

            return strSortIndexText;
        }

        public object[] ConvertBack(object value, Type[] targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
