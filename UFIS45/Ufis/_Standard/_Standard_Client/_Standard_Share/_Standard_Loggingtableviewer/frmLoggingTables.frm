VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmLoggingTables 
   Caption         =   "Special logging tables ..."
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   Icon            =   "frmLoggingTables.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB tabData 
      Height          =   3075
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4635
      _Version        =   65536
      _ExtentX        =   8176
      _ExtentY        =   5424
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmLoggingTables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Dim ilLeft As Integer
    Dim ilTop As Integer
    
'Read position from registry and move the window
    ilLeft = GetSetting(AppName:="LogTabViewer_Logtabs", _
                        section:="Startup", _
                        Key:="frmMainLeft", _
                        Default:=-1)
    ilTop = GetSetting(AppName:="LogTabViewer_Logtabs", _
                        section:="Startup", _
                        Key:="frmMainTop", _
                        Default:=-1)
    If ilTop = -1 Or ilLeft = -1 Then
        Me.Move 0, 0
    Else
        Me.Move ilLeft, ilTop
    End If
'END: Read position from registry and move the window
    tabData.ResetContent
    tabData.HeaderString = "Table,Logged,Table Description"
    tabData.HeaderLengthString = "80,80,450"
    tabData.EnableHeaderSizing True
    tabData.FontName = "Arial"
    tabData.HeaderFontSize = 14
    tabData.SetUniqueFields "0,1"
    tabData.ShowHorzScroller True
    If strUseAppFor = "COMMON" Then
        frmHiddenData.PrepareLoggingTableView
    End If

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = True
    End If
End Sub

'=============================================
' Save the settings for the position to
' be shwon the next time when form opens
'=============================================
Private Sub Form_Unload(Cancel As Integer)
    Dim ilLeft As Integer
    Dim ilTop As Integer

    ilLeft = Me.left
    ilTop = Me.top
    SaveSetting "LogTabViewer_Logtabs", _
                "Startup", _
                "frmMainLeft", _
                ilLeft
    SaveSetting "LogTabViewer_Logtabs", _
                "Startup", _
                "frmMainTop", ilTop
End Sub

Private Sub tabData_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim i As Long
    Dim cnt As Long
    Dim strLines As String
    Dim ilItem As Long
    Dim strTab As String
    Dim strLogTab As String
    
    frmLoadDefinition.cbField.Clear
    frmLoadDefinition.cbField.AddItem "**ALL FIELDS**"
    strTab = tabData.GetColumnValue(LineNo, 0)
    strLogTab = tabData.GetColumnValue(LineNo, 1)
    strLines = frmHiddenData.tabData(1).GetLinesByColumnValue(0, strTab, 0)
    cnt = ItemCount(strLines, ",")
    For i = 0 To cnt - 1
        ilItem = GetRealItem(strLines, i, ",")
        frmLoadDefinition.cbField.AddItem frmHiddenData.tabData(1).GetFieldValue(ilItem, "FINA")
    Next i
    frmLoadDefinition.bgReload = False
    frmLoadDefinition.cbField.Text = "**ALL FIELDS**"
    frmLoadDefinition.bgReload = True
    frmLoadDefinition.txtTable = strTab
    frmLoadDefinition.txtLogTable = strLogTab
End Sub
