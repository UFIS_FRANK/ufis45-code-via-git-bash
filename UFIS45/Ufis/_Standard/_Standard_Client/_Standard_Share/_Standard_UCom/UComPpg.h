#if !defined(AFX_UCOMPPG_H__F9478345_76BA_11D6_8067_0001022205E4__INCLUDED_)
#define AFX_UCOMPPG_H__F9478345_76BA_11D6_8067_0001022205E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UComPpg.h : Declaration of the CUComPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CUComPropPage : See UComPpg.cpp.cpp for implementation.

class CUComPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CUComPropPage)
	DECLARE_OLECREATE_EX(CUComPropPage)

// Constructor
public:
	CUComPropPage();

// Dialog Data
	//{{AFX_DATA(CUComPropPage)
	enum { IDD = IDD_PROPPAGE_UCOM };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CUComPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UCOMPPG_H__F9478345_76BA_11D6_8067_0001022205E4__INCLUDED)
