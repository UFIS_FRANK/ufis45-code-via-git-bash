#ifndef __UFIS_ODBC__
#define __UFIS_ODBC__

#include <SQL.h>
#include <CCSPtrArray.h>

class UfisOdbc
{
public:
	UfisOdbc();
	UfisOdbc(CString opConnectionString);
	~UfisOdbc();

	HENV	hEnv;
	HDBC	hDbConn;
	HSTMT	hStmt;

	CString omConnectionString;
	bool isOdbcInit;
	CString omXUser;

	CStringArray omData;

	CString omLastErrorMessage;

	void SetConnect(CString opConnect)
	{
		omConnectionString = opConnect;
	}
	int CallDB(CString opAction, 
			   CString opTable, 
		       CString opFields, 
		       CString opWhere, 
		       CString opValues);

	void RemoveInternalData(){omData.RemoveAll();}

	bool GetManyUrnos(int ipCount, CUIntArray &opUrnos);

	int GetLineCount();

	int GetLine(int ipLineNo, CStringArray &ropValues);

	void MakeClientString(CString &ropText);

};

#endif //__UFIS_ODBC__