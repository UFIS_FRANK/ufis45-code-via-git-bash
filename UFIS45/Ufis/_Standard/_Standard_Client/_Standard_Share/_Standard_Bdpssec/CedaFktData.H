#ifndef __CEDAFKT_DATA__
#define __CEDAFKT_DATA__

static void CedaFktDataCf(void *popInstance, int ipDDXType,
						   void *vpDataPointer, CString &ropInstanceName);

#include <CCSGlobl.h>
#include <CCSCedaData.h>

/////////////////////////////////////////////////////////////////////////////
struct FKTDATA {

	char	URNO[URNOLEN];
	char	FAPP[URNOLEN];
	char	SUBD[SUBDLEN];
	char	SDAL[SDALLEN];
	char	FUNC[FUNCLEN];
	char	FUAL[FUALLEN];
	char	TYPE[TYPELEN];
	char	STAT[STATLEN];

	int IsChanged;

	FKTDATA(void)
	{
		strcpy(URNO, "");
		strcpy(FAPP, "");
		strcpy(SUBD, "");
		strcpy(SDAL, "");
		strcpy(FUNC, "");
		strcpy(FUAL, "");
		strcpy(TYPE, "");
		strcpy(STAT, "");
		IsChanged = DATA_UNCHANGED;
	}

	FKTDATA& FKTDATA::operator=(const FKTDATA& rhs)
	{
		if (&rhs != this)
		{
			strcpy(URNO, rhs.URNO);
			strcpy(FAPP, rhs.FAPP);
			strcpy(SUBD, rhs.SUBD);
			strcpy(SDAL, rhs.SDAL);
			strcpy(FUNC, rhs.FUNC);
			strcpy(FUAL, rhs.FUAL);
			strcpy(TYPE, rhs.TYPE);
			strcpy(STAT, rhs.STAT);
			IsChanged = rhs.IsChanged;
		}		
		return *this;
	}
};

struct FKTMAP {

	char	URNO[URNOLEN];
	int		foundCount;

	FKTMAP(void)
	{
		strcpy(URNO, "");
		foundCount = 0;
	}

};

#define ORA_NOT_FOUND "ORA-01403"


class CedaFktData: public CCSCedaData
{

// Attributes
public:

    CCSPtrArray <FKTDATA> omData;
	char pcmErrorMessage[ERRMESSLEN];
	CMapStringToPtr omUrnoMap;
	CMapStringToPtr omApplMap;
	CMapStringToPtr omFappMap;

// Operations
public:
    CedaFktData();
	~CedaFktData();

	void ClearAll(void);

    bool Read();
	bool Add(FKTDATA *prpFkt);
	bool SetFkt( const char *pcpDataField, char *pcpErrorMessage);

	FKTDATA *GetFktByUrno(const char *pcpUrno);
	bool Update(const char *pcpUrno, const char *pcpStat);
	bool DeleteByFapp( const char  *pcpFapp );

	int  CreateApplMap( const char *pcpFapp );
	bool IsInApplMap( const char *pcpFfkt );

	void AddToFappMap(FKTDATA *prpFkt);
	void GetFktListByFapp(const char *pcpFapp, CCSPtrArray <FKTDATA> &ropFktList, bool bpReset=true);
	void ClearFappMap();
	bool DeleteFromFappMapByFapp(const char *pcpFapp);
	void DeleteFromFappMapByUrno(const char *pcpFapp, const char *pcpUrno);


	FKTDATA *AddFktInternal(FKTDATA &rrpFkt);
	void DeleteFktInternal(char *pcpUrno);
	void ProcessFktBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
};

extern CedaFktData ogCedaFktData;

#endif //__CEDAFKT_DATA__
