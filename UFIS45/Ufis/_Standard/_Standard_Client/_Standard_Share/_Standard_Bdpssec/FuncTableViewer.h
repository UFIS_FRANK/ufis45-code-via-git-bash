// FuncTableViewer.h
//
#ifndef __FUNCTABLEVIEWER_H__
#define __FUNCTABLEVIEWER_H__

#include <CCSPtrArray.h>
#include <CCSTable.h>
#include <CedaPrvData.h>
#include <CedaFktData.h>
#include <ProfileListViewer.h>


struct STATUS_COLOURS
{
	char		STAT[STATLEN];
	COLORREF	COLOUR;
};

#define NUMSTATS 3

#define descLEN 100

struct FUNCTAB_LINEDATA
{
	char	URNO[URNOLEN];
	char	FAPP[URNOLEN];	// URNO in SECTAB of the application
	char	SUBD[SUBDLEN];	// USID of of the application
	char	TYPE[TYPELEN];  // eg "B"
	char	TYPEdesc[descLEN];  // eg "Button"
	char	FUNC[FUNCLEN];  // name of the function eg "Insert"
	char	FUAL[FUALLEN];  // name of the function eg "Insert"
	char	oldSTAT[STATLEN];	// eg "1"/"0"/"-" (original value)
	char	newSTAT[STATLEN];	// eg "1"/"0"/"-" (value after update)
	char	STATdesc[descLEN];	// eg "Enabled"/"Disabled"/"Hidden"
	bool	UsedOnlyInOldProfiles; // true if this entry is only present because it is referenced by non-updated profiles 
									// - occurs after an application has been re-registered with a new configuration 
									// causeing this entry to be deleted or updated

	FUNCTAB_LINEDATA(void)
	{
		memset(URNO,0,URNOLEN);
		memset(FAPP,0,URNOLEN);
		memset(SUBD,0,SUBDLEN);
		memset(TYPE,0,TYPELEN);
		memset(TYPEdesc,0,descLEN);
		memset(FUNC,0,FUNCLEN);
		memset(FUAL,0,FUALLEN);
		memset(oldSTAT,0,STATLEN);
		memset(newSTAT,0,STATLEN);
		memset(STATdesc,0,descLEN);
		UsedOnlyInOldProfiles = false;
	}
};


class FuncTableViewer
{
// Constructions
public:
    FuncTableViewer();
    ~FuncTableViewer();

    void ChangeViewTo(CCSPtrArray <ProfileListObject> &ropPrvData);
    void ChangeViewTo();


// Internal data processing routines
public:

    void MakePrvLines(CCSPtrArray <ProfileListObject> &ropPrvData);
	void MakePrvLine(ProfileListObject *prpPrv);
    void MakeFktLines();
	void MakeFktLine(FKTDATA *prpFkt);
	int  CreateLine(FUNCTAB_LINEDATA *);
	void DeleteAll();
	void DeleteLine(int);

	bool IsPassFilter(const char *);
	bool IsThisFappSubd(const char *pcpFapp,const char *pcpSubd);
	int  CompareLine(FUNCTAB_LINEDATA *,FUNCTAB_LINEDATA *);
	int	 GetSize();
	void Update(char *pcpUsid);
	void UpdateDisplay();
	void Attach(CCSTable *popTable);
	COLORREF GetStatusColour(char *pcpStat);

// Attributes
public:
	STATUS_COLOURS	rmStatusColours[NUMSTATS];
	CCSTable	*pomTable;
    CCSPtrArray <FUNCTAB_LINEDATA> omLines;
	char		pcmFapp[URNOLEN]; // URNO of the application selected
	char		pcmSubd[SUBDLEN]; // URNO of the application selected
	char		pcmFsec[URNOLEN]; // URNO of the user or profile selected
//	char		pcmProf[URNOLEN]; // FSEC field in FUNCTAB/ FREL field in GRPTAB
	CCSPtrArray <int> omIndicies; // indicies into omLines of the lines displayed in the FuncTable

	// given the line number selected in the Functions CCSTable, update newSTAT
	void UpdateStatus(const int ipLine, CWnd *popParentWnd);
	void UpdateStatus(int *pipLines,int ipNumLines,char *pcpStat, CWnd *popParentWnd); // multi-line update
	void SelectLine(const int ipLine);

};

#endif //__FUNCTABLEVIEWER_H__
