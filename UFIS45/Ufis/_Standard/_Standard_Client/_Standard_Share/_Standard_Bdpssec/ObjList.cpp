#include <stdafx.h>
#include <ObjList.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// initialize the messageList
ObjList::ObjList()
{

} // end constructor


// delete all memory for the messageList
ObjList::~ObjList()
{

	RemoveAll();

} // end destructor

CString ObjList::GetLanguageFromCedaIni(void)
{
	CString olLanguage;
	char pclLanguage[100];
	memset(pclLanguage,0,sizeof(pclLanguage));
	char pclConfigFile[200];

    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigFile, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigFile, getenv("CEDA"));

	GetPrivateProfileString(pcgAppName, "INIFILE_LANGUAGE", "",pclLanguage, sizeof(pclLanguage), pclConfigFile);

	if(strlen(pclLanguage) > 0)
	{
		olLanguage.Format("[%s]", pclLanguage);
	}

	return olLanguage;
}

// read a config file and load the ObjList
// return TRUE if successfully read else FALSE and fill pcmLastError
bool ObjList::Load(const char *pcpFilename) 
{
	ASSERT(pcpFilename != NULL);

	// index 0 contains DESCriptions returned when the object type is unknown
	ProcessLine(CString(OBJLIST_KEYWORD)+",?,Unknown,?,Unknown");

	bool blSectionFound = false;

	CString olSectionName = GetLanguageFromCedaIni();
	if(!olSectionName.IsEmpty())
	{
		blSectionFound = ReadSection(pcpFilename, olSectionName);
	}

	if(!blSectionFound)
	{
		olSectionName.Format("[%s]",GetString(IDS_OBJLIST_SECTIONNAME));
		blSectionFound = ReadSection(pcpFilename, olSectionName);
	}

	if(!blSectionFound)
	{
		blSectionFound = ReadSection(pcpFilename, "[English]");
	}

	return blSectionFound;
}

// attempt to find the section eg [Italian] by
// 1) searching for INIFILE_LANGUAGE=Italian in ceda.ini and if not found by
// 2) searching for the language specified in IDS_OBJLIST_SECTIONNAME
// 3) search foe [English] as default
bool ObjList::ReadSection(const char *pcpFilename, CString opSectionName) 
{
	bool blSectionFound = false;

	CStdioFile olFile;
	CFileException olException;
	bool blProcessingSection = false;


	// attempt to open the file
	if( ! olFile.Open(pcpFilename,CFile::modeRead,&olException) )
	{
		if( olException.m_cause == CFileException::fileNotFound )
			// file not fouund
			sprintf(pcmLastError,"Error opening: \"%s\" - file not found",pcpFilename);
		else
			// other error
			sprintf(pcmLastError,"Error opening \"%s\" - error code %u",pcpFilename,olException.m_cause);
	}
	else
	{
		// declare CString to hold each line read
		CString olReadBuf;

		// read the file a line at a time and process and add it to the message list
		while(olFile.ReadString(olReadBuf))
		{
			olReadBuf.TrimLeft();
			olReadBuf.TrimRight();
			if(!olReadBuf.IsEmpty() && olReadBuf[0] != ';')
			{
				if(!blProcessingSection)
				{
					if(olReadBuf == opSectionName)
					{
						blProcessingSection = true;
						blSectionFound = true;
					}
				}
				else
				{
					if(olReadBuf[0] == '[' && olReadBuf[(olReadBuf.GetLength()-1)] == ']')
					{
						blProcessingSection = false;
					}
					else
					{
						ProcessLine(olReadBuf);
					}
				}
			}
		}

		olFile.Close();
	}

	return blSectionFound;
}

// pcpLine contains a list in the following format:
// B,Button,0,Disabled,1,Enabled,-,Hidden
// Copy each item to the object list
void ObjList::ProcessLine(const char *pcpLine)
{

	if( pcpLine != NULL && strlen(pcpLine) > 0)
	{
	
		char *pclNewLine = (char *) pcpLine;
		bmValidLine = TRUE;

		// get the "#OBJLIST" keyword
		char *pclKeyword;
		pclNewLine = ProcessItem(pclNewLine,&pclKeyword);


		// only process this line if it starts with the keyword
		if( !strcmp(pclKeyword,OBJLIST_KEYWORD) )
		{
			// alloc a new object
			OBJLIST *prlObj = new OBJLIST;

			// get the object type (FKTTAB.FKTP eg. "B")
			pclNewLine = ProcessItem(pclNewLine,&prlObj->TYPE);
			CheckForError(prlObj->TYPE);

			// get the object type DESCription (eg. "Button")
			pclNewLine = ProcessItem(pclNewLine,&prlObj->DESC);
			CheckForError(prlObj->DESC);

			// loop to get statuses and their DESCriptions (eg "1" = "Enabled")
			while(pclNewLine != NULL)
			{
				STATLIST *prlStat = new STATLIST;

				// read the status (FKTTAB.FLAG value eg. "1")
				pclNewLine = ProcessItem(pclNewLine,&prlStat->STAT);
				CheckForError(prlStat->STAT);

				// read the status DESCription (eg. "Enabled")
				pclNewLine = ProcessItem(pclNewLine,&prlStat->DESC);
				CheckForError(prlStat->DESC);

				if( bmValidLine )
					prlObj->Stats.Add(prlStat);
				else
					delete prlStat;
			}

			if( bmValidLine )
			{
				// line is valid so add the new object DESCription to the object list
				omObjList.Add(prlObj);
			}
			else
			{
				// line read was not valid so delete allocated memory (for statuses as well)
				DeleteRec(prlObj);
				delete prlObj;
			}
		}

		delete [] pclKeyword;

	} // end if(pclLine != NULL)

}

// if the item read was blank then the line is invalid
void ObjList::CheckForError(const char *pcpItem)
{

	if( pcpItem == NULL || strlen(pcpItem) <= 0)
		bmValidLine = FALSE;

}


// find the next comma (ignoring backslash+comma) or eol alloc,init and copy the item
// pppItem - address of pointer to hold the new item
// return a pointer to the start of the next item
char* ObjList::ProcessItem(char *pcpLine, char **pppItem)
{

	int ilLineLen = pcpLine != NULL ? strlen(pcpLine) : 0;
	char *pclThisChar = NULL;
	
	if( bmValidLine && ilLineLen > 0)
	{
		// declare a buffer big enough to hold the item
		char *pclItem = new char[ilLineLen+1];
		memset(pclItem,'\0',ilLineLen+1);

		char *pclItemPtr = pclItem; // points to the end of the item being added
		char *pclPrevChar = NULL;
		pclThisChar = pcpLine;

		// find the next comma (ignoring backslash+comma)
		for(BOOL blNotFound = TRUE; blNotFound && *pclThisChar != '\0'; pclPrevChar = pclThisChar, pclItemPtr++, pclThisChar++ )
		{
			if(*pclThisChar == ',' && *pclPrevChar != '\\' )
				blNotFound = FALSE; // comma found so this is the end of the item
			else if(*pclThisChar == ',' && *pclPrevChar == '\\' )
				*(--pclItemPtr) = *pclThisChar;	// backslash+comma - remove backslash from pclItem and add the comma
			else
				*pclItemPtr = *pclThisChar;	// copy the current char to pclItem
		}

		int ilItemLen = strlen(pclItem);

		if( ilItemLen <= 0 )
		{
			// the item is null
			*pppItem = NULL;
		}
		else
		{
			// alloc enough memory for the item and copy it
			*pppItem = new char[ilItemLen+1]; 
			memset(*pppItem,'\0',ilItemLen+1);
			strcpy(*pppItem,pclItem);

		}

		delete [] pclItem;
	}


	// return a pointer to the next char after the comma or NULL if eol
	return (pclThisChar == NULL || strlen(pclThisChar) <= 0) ? NULL : pclThisChar;	

}


// clear the message list
void ObjList::RemoveAll()
{

	// free allocated memory
	int ilObjListSize = omObjList.GetSize();

	for( int ilCnt = 0; ilCnt < ilObjListSize; ilCnt++ )
		DeleteRec((OBJLIST *) &omObjList[ilCnt]);

	omObjList.DeleteAll();

} // RemoveAll()


// delete the contents of a record
void ObjList::DeleteRec(OBJLIST *prpObjRec)
{

	delete [] prpObjRec->TYPE;
	delete [] prpObjRec->DESC;

	int ilStatListSize = prpObjRec->Stats.GetSize();
	for( int ilCnt = 0; ilCnt < ilStatListSize; ilCnt++ )
	{
		delete [] prpObjRec->Stats[ilCnt].STAT;
		delete [] prpObjRec->Stats[ilCnt].DESC;
		STATLIST *prlStat = &prpObjRec->Stats[ilCnt];
		delete prlStat;
	}

} // DeleteRec()



// given pcpDesc = "B" and pcpStat = "1", return pcpTypeDesc = "Button" and pcpStatDesc = "Enabled"
bool ObjList::GetDesc(const char *pcpType, const char *pcpStat, char *pcpTypeDesc, char *pcpStatDesc)
{

	int ilObjListSize = omObjList.GetSize();

	// init fields to "Unknown"
	strcpy(pcpTypeDesc,omObjList[0].DESC);
	strcpy(pcpStatDesc,omObjList[0].Stats[0].DESC);

	for( int ilCnt = 0; ilCnt < ilObjListSize; ilCnt++ )
	{
		if( ! strcmp(pcpType,omObjList[ilCnt].TYPE) )
		{
			strcpy(pcpTypeDesc,omObjList[ilCnt].DESC);
			int ilStatListSize = omObjList[ilCnt].Stats.GetSize();
			for( int ilCnt2 = 0; ilCnt2 < ilStatListSize; ilCnt2++ )
			{
				if( ! strcmp(pcpStat,omObjList[ilCnt].Stats[ilCnt2].STAT) )
				{
					strcpy(pcpStatDesc,omObjList[ilCnt].Stats[(ilCnt2)].DESC);
					return true;
				}
			}
		}
	}

	return false;

} // end GetDesc()



// given pcpType = "B" and pcpStat = "-" return pcpNextStat = "0" and pcpNextStatDescr = "Disabled"
bool ObjList::NextStat(const char *pcpType, const char *pcpStat, char *pcpNextStat, char *pcpNextStatDesc)
{

	int ilObjListSize = omObjList.GetSize();


	for( int ilCnt = 0; ilCnt < ilObjListSize; ilCnt++ )
	{
		if( ! strcmp(pcpType,omObjList[ilCnt].TYPE) )
		{
			int ilStatListSize = omObjList[ilCnt].Stats.GetSize();
			for( int ilCnt2 = 0; ilCnt2 < ilStatListSize; ilCnt2++ )
			{
				if( ! strcmp(pcpStat,omObjList[ilCnt].Stats[ilCnt2].STAT) )
				{
					if( ilCnt2 == (ilStatListSize-1) )
					{
						// get the first status in the list
						strcpy(pcpNextStatDesc,omObjList[ilCnt].Stats[0].DESC);
						strcpy(pcpNextStat,omObjList[ilCnt].Stats[0].STAT);
					}
					else
					{
						// get the next status in the list
						strcpy(pcpNextStatDesc,omObjList[ilCnt].Stats[(ilCnt2+1)].DESC);
						strcpy(pcpNextStat,omObjList[ilCnt].Stats[(ilCnt2+1)].STAT);
					}
					return true;
				}
			}
		}
	}

	// init fields to "Unknown"
	strcpy(pcpNextStatDesc,omObjList[0].Stats[0].DESC);
	strcpy(pcpNextStat,omObjList[0].Stats[0].STAT);

	return false;

} // end NextStat()




