#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#include <stdafx.h>
#include <MessList.h>


// initialize the messageList
MessList::MessList()
{

} // end constructor


// delete all memory for the messageList
MessList::~MessList()
{
	RemoveAll();
	
} // end destructor

// read a config file and load the messList
// return TRUE if successfully read else FALSE and fill pcmLastError
BOOL MessList::Load(const char *pcpFilename) 
{
	ASSERT(pcpFilename != NULL);

	BOOL ilRc = TRUE;

	CStdioFile olFile;
	CFileException olException;

	// attempt to open the file
	if( ! olFile.Open(pcpFilename,CFile::modeRead,&olException) )
	{
		if( olException.m_cause == CFileException::fileNotFound )
			// file not fouund
			sprintf(pcmLastError,"Error opening: \"%s\" - file not found",pcpFilename);
		else
			// other error
			sprintf(pcmLastError,"Error opening \"%s\" - error code %u",pcpFilename,olException.m_cause);

		ilRc = FALSE;
	}

	if( ilRc )
	{
		// declare CString to hold each line read
		CString olReadBuf;

		// read the file a line at a time and process and add it to the message list
		while(olFile.ReadString(olReadBuf))
			ProcessLine(olReadBuf);

		olFile.Close();
	}

	return ilRc; // TRUE if file read OK
}

// if the line contains the keyword get the key and mess and add them
// to the message list, otherwise ignore the line (comments/white space)
void MessList::ProcessLine(const char *pcpLine)
{
	if( pcpLine != NULL && strlen(pcpLine) > 0)
	{
		char *pclNewLine = (char *) pcpLine;
		bmValidLine = TRUE;


		// get the #MESSLIST keyword
		char *pclKeyword;
		pclNewLine = ProcessItem(pclNewLine,&pclKeyword);


		// only process this line if it starts with the keyword
		if( ! strcmp(pclKeyword,MESSLIST_KEYWORD) )
		{
			// allocate a new message list record
			MESSLIST *prlMessRec = new MESSLIST;

			// get the KEY
			pclNewLine = ProcessItem(pclNewLine,&prlMessRec->key);
			CheckForError(prlMessRec->key);

			// get the MESS
			pclNewLine = ProcessItem(pclNewLine,&prlMessRec->mess);
			CheckForError(prlMessRec->mess);

			if( bmValidLine )
			{
				// add the new key and mess to the list
				omMessList.Add(prlMessRec);
			}
			else
			{
				// the line read was invalid so delete any allocated memory
				delete [] prlMessRec->key;
				delete [] prlMessRec->mess;
				delete prlMessRec;
			}
		}

		delete [] pclKeyword;

	} // end if(pclLine != NULL)

}


// if the item read was blank then the line is invalid
void MessList::CheckForError(const char *pcpItem)
{
	if( pcpItem == NULL || strlen(pcpItem) <= 0)
		bmValidLine = FALSE;
}



// find the next comma (ignoring backslash+comma) or eol alloc,init and copy the item
// pppItem - address of pointer to hold the new item
// return a pointer to the start of the next item
char* MessList::ProcessItem(char *pcpLine, char **pppItem)
{	
	int ilLineLen = pcpLine != NULL ? strlen(pcpLine) : 0;
	char *pclThisChar = NULL;
	
	if( bmValidLine && ilLineLen > 0)
	{
		// declare a buffer big enough to hold the item
		char *pclItem = new char[ilLineLen+1];
		memset(pclItem,'\0',ilLineLen+1);

		char *pclItemPtr = pclItem;
		char *pclPrevChar = NULL;
		pclThisChar = pcpLine;

		// find the next comma (ignoring backslash+comma)
		for(BOOL blNotFound = TRUE; blNotFound && *pclThisChar != '\0'; pclPrevChar = pclThisChar, pclItemPtr++, pclThisChar++ )
		{
			if(*pclThisChar == ',' && *pclPrevChar != '\\' )
				blNotFound = FALSE; // comma found so this is the end of the item
			else if(*pclThisChar == ',' && *pclPrevChar == '\\' )
				*(--pclItemPtr) = *pclThisChar;	// backslash+comma - remove backslash from pclItem and add the comma
			else
				*pclItemPtr = *pclThisChar;	// copy the current char to pclItem
		}

		int ilItemLen = strlen(pclItem);

		if( ilItemLen <= 0 )
		{
			// the item is null
			*pppItem = NULL;
		}
		else
		{
			// alloc enough memory for the item and copy it
			*pppItem = new char[ilItemLen+1]; 
			memset(*pppItem,'\0',ilItemLen+1);
			strcpy(*pppItem,pclItem);

		}

		delete [] pclItem;
	}


	// return a pointer to the next char after the comma of NULL if eol
	return (pclThisChar == NULL || strlen(pclThisChar) <= 0) ? NULL : pclThisChar;	

}




// add a new message and its' key to the messageList
// this is implemented as a seperate function so that messages
// can be added from within an application itself (not just read from a file)
void MessList::Add(const char *pcpKey,const char *pcpMess)
{
	ASSERT(pcpKey != NULL);
	ASSERT(pcpMess != NULL);

	// allocate memory and add the new element to the message list
	
	MESSLIST *prlMessRec = new MESSLIST;
	prlMessRec->key = new char[strlen(pcpKey)+1];
	prlMessRec->mess = new char[strlen(pcpMess)+1];
	strcpy(prlMessRec->key,pcpKey);
	strcpy(prlMessRec->mess,pcpMess);
	omMessList.Add(prlMessRec);

} // end Add()


// clear the message list
void MessList::RemoveAll()
{
	// free allocated memory
	int ilMessListSize = omMessList.GetSize();

	for( int ilCnt = 0; ilCnt < ilMessListSize; ilCnt++ )
	{
		delete [] omMessList[ilCnt].key;
		delete [] omMessList[ilCnt].mess;
	}

	omMessList.DeleteAll();

} // RemoveAll()


// return the message, or an error message if not found
char* MessList::Get(const char *pcpKey)
{
	int ilMessListSize = omMessList.GetSize();

	// search the list for the desc and returns its' mess
	for( int ilCnt = 0; ilCnt < ilMessListSize; ilCnt++ )
		if( ! strcmp(pcpKey,omMessList[ilCnt].key) )
			return (omMessList[ilCnt].mess);


	sprintf(mDescNotFound,"\"%s\" message not found.",pcpKey);
	return mDescNotFound;

} // end Get()