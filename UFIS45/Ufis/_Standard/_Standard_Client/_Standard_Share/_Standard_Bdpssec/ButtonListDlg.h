// ButtonListDlg.h : header file
//
#ifndef _BUTTONLISTDLG_H_
#define _BUTTONLISTDLG_H_

#include <CCSButtonCtrl.h>
#include <SecTableViewer.h>
#include <CCSTable.h>
#include <resource.h>
#include <EditUserDlg.h>


#define STATBARWIDTH 55
                     
class MReqTable;
/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog dialog
#define TRAFFIC_LIGHT_RED RGB(255,   0,   0)
#define TRAFFIC_LIGHT_GREEN RGB(0,   240,   0)
#define TRAFFIC_LIGHT_YELLOW RGB(255,   255,   0)

class CButtonListDialog : public CDialog
{
// Construction
public:
    CButtonListDialog(CWnd* pParent = NULL);    // standard constructor
	~CButtonListDialog(void);

	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 
	LONG OnCcsButton(UINT /*wParam*/, LONG /*lParam*/);

	void UpdateTrafficLight(int ipState);
// Timer registration service
public:
	void RegisterTimer(CWnd *popWnd);
	void UnRegisterTimer(CWnd *popWnd);
private:
	CWnd *pomWndRequireTimer;	// the window which require the timer service
	
// Dialog Data
public:

	CMenu omMenu;
	CCSTable *pomTable;
    int m_nDialogBarHeight;
	SecTableViewer omSecTableViewer;
//	CString omAdminUsid; // usid of system adminstrator - cannot be deleted

    CTime omPrePlanTime;
	BOOL omPrePlanMode;

	void UpdateView();
    //{{AFX_DATA(CButtonListDialog)
	enum { IDD = IDD_BUTTONLIST };
	CStatic	m_StatusBar;
	CButton	m_InsertButton;
	CButton	m_UpdateButton;
	CButton	m_DeleteButton;
	CButton	m_PasswordButton;
	CButton	m_RightsButton;
	CButton	m_AllocateButton;
	CCSButtonCtrl	m_CB_BCStatus;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialogAppDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

    // Generated message map functions
    //{{AFX_MSG(CButtonListDialog)
    virtual void OnCancel();
    virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	afx_msg void OnInsertButton();
	afx_msg void OnUpdateButton();
	afx_msg void OnDeleteButton();
	afx_msg void OnPasswordButton();
	afx_msg void OnRightsButton();
	afx_msg void OnAllocateButton();
	afx_msg void OnExitBDPSSEC();
	afx_msg void OnUser();
	afx_msg void OnProfile();
	afx_msg void OnApplication();
	afx_msg void OnGroup();
	afx_msg void OnWks();
	afx_msg void OnWksGroup();
	afx_msg LONG OnTableLButtonDblClk(UINT ipParam, LONG lpParam);
	afx_msg LONG OnTableLButtonDown(UINT ipParam, LONG lpParam);
	afx_msg LONG OnTableRButtonDown(UINT ipParam, LONG lpParam);
	afx_msg LONG OnTableSelChange(UINT ipParam, LONG lpParam);
	afx_msg void OnTestButton();
	afx_msg void OnExitButton();
	afx_msg void OnPrintButton();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

public:

	char mCedaErrorMessage[ERRMESSLEN];
	void OnAppAbout();

	int imBroadcastState;
	bool bmBroadcastReceived;
	void ExitBDPSSEC();
	bool	bmBroadcastCheckMsg;

private:
	SECTAB_LINEDATA *prmPrevSelLine;
};

extern CButtonListDialog *pogMainScreenDlg;

#endif  // _BUTTONLISTDLG_H_
