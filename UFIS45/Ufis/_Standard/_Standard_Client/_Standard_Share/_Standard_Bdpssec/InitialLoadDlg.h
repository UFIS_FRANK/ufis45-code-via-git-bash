/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// InitialLoad dialog
#ifndef __INITIAL_LOAD_DLG__
#define __INITIAL_LOAD_DLG__

#include <resource.h>

class InitialLoadDlg : public CDialog
{
// Construction
public:
	InitialLoadDlg(CWnd* pParent = NULL);   // standard constructor
	
	~InitialLoadDlg(void);
	void SetProgress(int ipProgress);
	void SetMessage(CString opMessage);

	BOOL DestroyWindow(void);

// Dialog Data
	//{{AFX_DATA(InitialLoadDlg)
	enum { IDD = IDD_INITIALLOAD };
	CProgressCtrl	m_Progress;
	CListBox	m_MsgList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(InitialLoadDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(InitialLoadDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//extern InitialLoadDlg *pogInitialLoad;


#endif //__INITIAL_LOAD_DLG__
