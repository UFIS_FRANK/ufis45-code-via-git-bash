VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmShiftDetails 
   Caption         =   "ShiftDetails"
   ClientHeight    =   9480
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6825
   LinkTopic       =   "Form1"
   ScaleHeight     =   9480
   ScaleWidth      =   6825
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog dlgSave1 
      Left            =   4920
      Top             =   2520
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdShiftExport 
      Caption         =   "Export to Excel"
      Height          =   495
      Left            =   2160
      TabIndex        =   4
      Top             =   2640
      Width           =   2415
   End
   Begin VB.Frame Frame1 
      Caption         =   "Event Details"
      Height          =   5895
      Left            =   120
      TabIndex        =   1
      Top             =   3480
      Width           =   6615
      Begin TABLib.TAB tab_SHIFTDETAILS 
         Height          =   4935
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   6375
         _Version        =   65536
         _ExtentX        =   11245
         _ExtentY        =   8705
         _StockProps     =   64
      End
   End
   Begin TABLib.TAB tab_SHIFT 
      Height          =   2175
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6615
      _Version        =   65536
      _ExtentX        =   11668
      _ExtentY        =   3836
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_R01TABSHIFT 
      Height          =   615
      Left            =   5040
      TabIndex        =   3
      Tag             =   "{=TABLE=}R01TAB{=FIELDS=}ORNO,FLST,FVAL,USEC,TIME,SFKT,URNO"
      Top             =   1200
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   1085
      _StockProps     =   64
   End
End
Attribute VB_Name = "frmShiftDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'#######################################################################
'### SHOW THE JOB CHANGES FOR SELECTED JOB
'#######################################################################
Sub showShiftEventDetails(shiftOrnoSelected As String)
    
    Dim test As String
    Dim l As Integer
    Dim strFields As String
    Dim strValues As String
    Dim strLine As String
    Dim strQuery As String
    Dim strTemp As String
    
    'test = frmMain: gR02FieldDescList
     
    If initShiftFlag = False Then InitShiftTabs
    
    If (shiftOrnoSelected <> "") Then
        
        MousePointer = vbHourglass
        
        strQuery = "where orno = '" & shiftOrnoSelected & "'"
        
        tab_R01TABSHIFT.ResetContent
        frmData.LoadData tab_R01TABSHIFT, strQuery
        tab_R01TABSHIFT.Visible = False
        
        tab_SHIFT.ResetContent
        tab_SHIFTDETAILS.ResetContent
        
        For l = 0 To tab_R01TABSHIFT.GetLineCount - 1 Step 1
            
            'ornoTemp = tab_R02TABEVENT.GetColumnValues(l, 0)
        
            '### LOG COLUMNS
            strFields = tab_R01TABSHIFT.GetColumnValues(l, 1)
            strFields = Replace(strFields, Chr(30), ",")
            strValues = tab_R01TABSHIFT.GetColumnValues(l, 2)
            strValues = Replace(strValues, Chr(30), ",")
                       
            'strLine = User value
            strLine = tab_R01TABSHIFT.GetColumnValues(l, 3)
            '### No user in R01TAB FOUND?
            If strLine = "" Or UCase(strLine) = "EVENTSPOOL" Then
                '### TAKE USEU
                strLine = GetFieldValue("USEU", strValues, strFields)
                '### STILL EMPTY TAKE USEC
                If strLine = "" Then
                    strLine = GetFieldValue("USEC", strValues, strFields)
                End If
                '### STILL EMPTY PUT DEFAULT
                If strLine = "" Then
                    strLine = "User not available"
                End If
            End If
            If strLine = "SAP-HR" Then strLine = "SAP interface"
                
            
            '### TIME
            strLine = strLine + "," & CedaFullDateToVb(tab_R01TABSHIFT.GetColumnValues(l, 4))
            strLine = strLine + "," + tab_R01TABSHIFT.GetColumnValues(l, 5)
            strTemp = SortedR01Fields(strValues, strFields)
            strLine = strLine + "," + strTemp
            
            'Add the record to Log tab
            tab_SHIFT.InsertTextLine strLine, False
               
        Next l
            
        tab_SHIFT.ShowHorzScroller True
        tab_SHIFT.AutoSizeColumns
        'tab_JOBLOG.Sort 1, True, True
        tab_SHIFT.EnableHeaderSizing True
        tab_SHIFT.Refresh
        
        MousePointer = vbDefault
        
    End If
    
End Sub


'#######################################################################
'### INIT TABLES FOR EVENT DETAILS FORM
'#######################################################################
Sub InitShiftTabs()

    Dim i As Integer
    
    frmData.InitTabGeneral tab_R01TABSHIFT
    frmData.InitTabForCedaConnection tab_R01TABSHIFT
    tab_R01TABSHIFT.AutoSizeColumns
    
    tab_SHIFT.ResetContent
    tab_SHIFT.HeaderString = gR01FieldDescList
    'tab_SHIFT.HeaderString = gR01FieldList
    tab_SHIFT.AutoSizeColumns
    tab_SHIFT.Refresh
    
    tab_SHIFTDETAILS.ResetContent
    tab_SHIFTDETAILS.HeaderString = "FIELD,DATA"
    tab_SHIFTDETAILS.HeaderLengthString = "250,250"
    tab_SHIFTDETAILS.FontName = "Arial"
    tab_SHIFTDETAILS.FontSize = 15
    'tab_EVENTDETAILS.AutoSizeColumns
    tab_SHIFTDETAILS.Refresh
    
    initShiftFlag = True
    
End Sub

Private Sub cmdShiftExport_Click()
    Dim l As Integer
    Dim i As Integer
    Dim llLineCount As Integer
    Dim strTemp As String
    Dim FileNumber
    
    dlgSave1.DialogTitle = "Save as Excel"
    dlgSave1.Filter = "Excel (CSV)|*.csv"
    dlgSave1.FileName = "ShiftEvents"
    dlgSave1.ShowSave
    
    If dlgSave1.FileName <> "" Then
        
        l = tab_SHIFT.GetLineCount
        
        If l < 1 Then
            MsgBox "No Data to export.", vbExclamation
            Exit Sub
        End If
        
        'If gFilter <> "" Then MsgBox "Export with active filter.", vbInformation
        
        FileNumber = FreeFile   ' Get unused file number.
        Open dlgSave1.FileName For Output As #FileNumber    ' Create file name.
        Print #FileNumber, gR01FieldDescList
        'Print #FileNumber, gR01FieldList
        
        llLineCount = tab_SHIFT.GetLineCount
        
        For l = 0 To llLineCount - 1 Step 1
            strTemp = ""
            For i = 0 To gR02items - 1 Step 1
                strTemp = strTemp & tab_SHIFT.GetColumnValues(l, i) & ","
            Next i
            Print #FileNumber, strTemp
        Next l
        
        Close #FileNumber
        
    Else
        MsgBox ("Please enter filename to export")
    End If
End Sub

Private Sub tab_SHIFT_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    
    Dim l As Integer
    Dim i As Integer
    Dim strValuesCurrent As String
    Dim strFieldsCurrent As String
    Dim strValuesPrev As String
    Dim strPrev As String
    Dim strFieldsPrev As String
    Dim strKey As String
    Dim tmpVal As String
    Dim tmpValPrev As String
    
    l = tab_SHIFT.GetCurrentSelected
    
    If l <> -1 Then
        
        MousePointer = vbHourglass
        
        tab_SHIFTDETAILS.ResetContent
        
        For i = 1 To gR01items
            tab_SHIFTDETAILS.SetColumnValue i, 0, GetItem(gR01FieldDescList, i, ",")
            'tab_SHIFTDETAILS.SetColumnValue i, 0, GetItem(gR01FieldList, i, ",")
        Next i
        
        'Assign compulsory values
        tab_SHIFTDETAILS.SetColumnValue 0, 1, tab_R01TABSHIFT.GetColumnValues(l, 3)
        tab_SHIFTDETAILS.SetColumnValue 1, 1, CedaFullDateToVb(tab_R01TABSHIFT.GetColumnValues(l, 4))
        tab_SHIFTDETAILS.SetColumnValue 2, 1, tab_R01TABSHIFT.GetColumnValues(l, 5)
        
        ' Need to check whether any updates for each field or not? if so change the color of the fields.
        If (l <> 0) Then
        
            strFieldsCurrent = tab_R01TABSHIFT.GetColumnValues(l, 1)
            strFieldsCurrent = Replace(strFieldsCurrent, Chr(30), ",")
            strValuesCurrent = tab_R01TABSHIFT.GetColumnValues(l, 2)
            strValuesCurrent = Replace(strValuesCurrent, Chr(30), ",")
            
            strFieldsPrev = tab_R01TABSHIFT.GetColumnValues(l - 1, 1)
            strFieldsPrev = Replace(strFieldsPrev, Chr(30), ",")
            strValuesPrev = tab_R01TABSHIFT.GetColumnValues(l - 1, 2)
            strValuesPrev = Replace(strValuesPrev, Chr(30), ",")
            
            For i = 3 To gR01items Step 1
                strKey = gR01TAB(i)
                tmpVal = GetFieldValue(strKey, strValuesCurrent, strFieldsCurrent)
                tmpValPrev = GetFieldValue(strKey, strValuesPrev, strFieldsPrev)
                
                
                If tmpVal <> tmpValPrev And tmpVal <> "" Then tab_SHIFTDETAILS.SetLineColor i, H000080FF, &HC0C0FF
                
                If InStr(1, "TIME,CDAT,LSTU", strKey) > 0 Then
                    If tmpVal <> "" Then
                        tmpVal = CedaFullDateToVb(tmpVal)
                    End If
                End If
                
                If InStr(1, "ACFR,ACTO,PLFR,PLTO", strKey) > 0 Then
                    If tmpVal <> "" Then
                        tmpVal = DateAdd("h", gUTCOffset, CedaFullDateToVb(tmpVal))
                    End If
                End If
                
                'USTF - Urno Staff - get Staff info & show
                If strKey = "USTF" Then
                    tmpVal = frmMain.tab_STFCOMPLETE.GetLinesByColumnValue(1, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_STFCOMPLETE.GetColumnValue(CLng(tmpVal), 0)
                End If
                
                If strKey = "STFU" Then
                    tmpVal = frmMain.tab_STFCOMPLETE.GetLinesByColumnValue(1, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_STFCOMPLETE.GetColumnValue(CLng(tmpVal), 0)
                End If
                
                'If InStr(1, "USTF,STFU,USEU,USEC", strKey) > 0 Then
                '    tmpVal = frmMain.tab_STFCOMPLETE.GetLinesByColumnValue(1, tmpVal, 0)
                '    If tmpVal <> "" Then tmpVal = frmMain.tab_STFCOMPLETE.GetColumnValue(CLng(tmpVal), 0)
                'End If
                
                tab_SHIFTDETAILS.SetColumnValue i, 1, tmpVal
                
            Next i
            
        Else
        
            strFieldsCurrent = tab_R01TABSHIFT.GetColumnValues(l, 1)
            strFieldsCurrent = Replace(strFieldsCurrent, Chr(30), ",")
            strValuesCurrent = tab_R01TABSHIFT.GetColumnValues(l, 2)
            strValuesCurrent = Replace(strValuesCurrent, Chr(30), ",")
        
            For i = 3 To gR01items Step 1
                strKey = gR01TAB(i)
                tmpVal = GetFieldValue(strKey, strValuesCurrent, strFieldsCurrent)
                
                If InStr(1, "TIME,CDAT,LSTU", strKey) > 0 Then
                    If tmpVal <> "" Then
                        tmpVal = CedaFullDateToVb(tmpVal)
                    End If
                End If
                
                If InStr(1, "ACFR,ACTO,PLFR,PLTO", strKey) > 0 Then
                    If tmpVal <> "" Then
                        tmpVal = DateAdd("h", gUTCOffset, CedaFullDateToVb(tmpVal))
                    End If
                End If
                
                
                'USTF - Urno Staff - get Staff info & show
                If strKey = "USTF" Then
                    tmpVal = frmMain.tab_STFCOMPLETE.GetLinesByColumnValue(1, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_STFCOMPLETE.GetColumnValue(CLng(tmpVal), 0)
                End If
                
                If strKey = "STFU" Then
                    tmpVal = frmMain.tab_STFCOMPLETE.GetLinesByColumnValue(1, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_STFCOMPLETE.GetColumnValue(CLng(tmpVal), 0)
                End If
                
                'If InStr(1, "USTF,STFU,USEU,USEC", strKey) > 0 Then
                '    tmpVal = frmMain.tab_STFCOMPLETE.GetLinesByColumnValue(1, tmpVal, 0)
                '    If tmpVal <> "" Then tmpVal = frmMain.tab_STFCOMPLETE.GetColumnValue(CLng(tmpVal), 0)
                'End If
                
                tab_SHIFTDETAILS.SetColumnValue i, 1, tmpVal
                
            Next i
        End If
            
        'tab_EVENTDETAILS.InsertBuffer
        
        tab_SHIFTDETAILS.Refresh
        MousePointer = vbDefault
        
        
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    initShiftFlag = False
End Sub

