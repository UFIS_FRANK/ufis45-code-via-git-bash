// UfisAppMgr.cpp : Implementation of CUfisAppMgr
#include <stdafx.h>
#include <UfisApplMgr.h>
#include <UfisAppMgr.h>

/////////////////////////////////////////////////////////////////////////////
// CUfisAppMgr

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Converting a BSTR to an upper-case char*.
// Important for case-sensitive key in the maps, for searching
// applications (pending or started) and for sending data.
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
char* MakeUpperBSTR(BSTR opConvertString)
{
	TCHAR	tConvertString[GEN_PATH_LEN];
	char*	cpUpper;
	if (opConvertString != NULL)
	{
		WideCharToMultiByte (CP_ACP, 0, opConvertString, -1, tConvertString, 10001, 0, 0);
		cpUpper = _strupr(_strdup(tConvertString));
		return cpUpper;
	}
	else
	{
		return NULL;
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
//  maping a wide-character string to a new character string
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
char* GetBSTR(BSTR opSrc)
{
	TCHAR	tConvertString[GEN_PATH_LEN*2];
	char* cpReturn;
	if (opSrc != NULL)
	{
		WideCharToMultiByte (CP_ACP, 0, opSrc, -1, tConvertString, 10001, 0, 0);
		cpReturn = (char*)tConvertString;
		return cpReturn;
	}
	else
	{
		return NULL;
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ?
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IUfisAppMgr
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Event that happens, if a connected client terminates.
// Important: no notification, if client was cilled with the task-manager!
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::Unadvise(DWORD pdwCookie)
{
	::EnterCriticalSection(&omCriticalSection);

	AppMap::iterator olPos = omAppMapStarted.find (pdwCookie);

	if (olPos != omAppMapStarted.end())
	{
		omAppMapStarted.erase (olPos);
	}

	CProxy_IUfisAppMgrEvents< CUfisAppMgr >::Unadvise(pdwCookie);
	::LeaveCriticalSection(&omCriticalSection);
	return S_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Event that happens, if a client connects.
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::Advise(IUnknown* pUnkSink,DWORD* pdwCookie)
{
	// Store the pointer to the Sink object for later use
	m_pActUnk = pUnkSink;
	CProxy_IUfisAppMgrEvents< CUfisAppMgr >::Advise(pUnkSink,pdwCookie);
	m_cookie = *pdwCookie;

	// create a new wrapper. we only know the unique ID (m_cookie), not
	// the path an the name yet (see: Register)
	AppWrapper olAppWrapper;
	olAppWrapper.pUfisAppMgr = this;
	olAppWrapper.lId = m_cookie;
	::EnterCriticalSection(&omCriticalSection);
	omAppMapStarted.insert(AppMap::value_type(m_cookie, olAppWrapper));
	::LeaveCriticalSection(&omCriticalSection);

	return S_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// A client tells us to start any other application.
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::StartApp(BSTR AppPathName, BSTR CommandLine)
{
	//getting the uppercase-string
	TCHAR	tCommandLine[GEN_PATH_LEN * 2];
	TCHAR	tLaunchStr[GEN_PATH_LEN];
	char*	cpUpper = MakeUpperBSTR(AppPathName);
	strcpy (tLaunchStr,		cpUpper);
	strcpy (tCommandLine,	cpUpper);
	if (CommandLine != NULL)
	{
		strcat (tCommandLine, " ");
		strcat (tCommandLine, GetBSTR(CommandLine));
	}

	//starting the process
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si,sizeof(si));
	si.cb = sizeof(si);
	si.wShowWindow = SW_SHOWNORMAL;
	si.dwFlags = STARTF_USESHOWWINDOW;
	CreateProcess (NULL,tCommandLine,
				  NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,NULL,NULL,&si,&pi);

	//add app to pending-map
	::EnterCriticalSection(&omCriticalSection);
	AppMapPending::iterator olPosPending = omAppMapPending.find (tLaunchStr);
	if (olPosPending != omAppMapPending.end())
	{
		//increase number of pending apps with this name
		olPosPending->second.lId++;
	}
	else
	{
		//create a new wrapper for this app
		AppWrapper olAppWrapper;
		olAppWrapper.lId			= 1;
		olAppWrapper.pUfisAppMgr	= this;
		olAppWrapper.strAppPathName = tLaunchStr;
		omAppMapPending.insert(AppMapPending::value_type(tLaunchStr, olAppWrapper));
	}
	::LeaveCriticalSection(&omCriticalSection);

	return S_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Sending data to any application. We know only path & name of it.
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::SendDataByName(BSTR AppPathName, BSTR Data)
{
	::EnterCriticalSection(&omCriticalSection);
	AppMap::iterator olPos		= omAppMapStarted.end ();
	AppMap::iterator olPosBegin	= omAppMapStarted.begin();

	TCHAR	tAppPathName[GEN_PATH_LEN];
	char*	cpUpper = MakeUpperBSTR(AppPathName);
	strcpy (tAppPathName, cpUpper);

	while (olPos != olPosBegin)
	{
		olPos--;
		if (olPos->second.strAppPathName == tAppPathName)
		{
			(olPos->second.pUfisAppMgr)->Fire_OnDataReceive(Data);
		}
	}
	::LeaveCriticalSection(&omCriticalSection);
	return S_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Sending data to any application. We know 'only' the ID of it.
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::SendDataByID(long lID, BSTR Data)
{
	::EnterCriticalSection(&omCriticalSection);
	AppMap::iterator olPos		= omAppMapStarted.end ();
	AppMap::iterator olPosBegin	= omAppMapStarted.begin();

	while (olPos != olPosBegin)
	{
		olPos--;
		if (olPos->second.lId == (DWORD)lID)
		{
			(olPos->second.pUfisAppMgr)->Fire_OnDataReceive(Data);
		}
	}
	::LeaveCriticalSection(&omCriticalSection);

	return S_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// After any application is started, it must register at UFISAppMng so that
// we know, that it is started. Although we get the advise-event, we don't know
// the path and the name of the application. So the app tells us their name
// with this function.
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::Register(BSTR AppPathName)
{
	AppWrapper* polAppWrapper;

	TCHAR	tAppPathName[GEN_PATH_LEN];
	char*	cpUpper = MakeUpperBSTR(AppPathName);
	strcpy (tAppPathName, cpUpper);

	::EnterCriticalSection(&omCriticalSection);
	AppMap::iterator  olPos = omAppMapStarted.find (this->m_cookie);
	if (olPos != omAppMapStarted.end())
	{
		polAppWrapper = (AppWrapper*)&olPos->second;
		polAppWrapper->strAppPathName = tAppPathName;
	}

	//delete the app from the pending-map (if there is - the first app is not) 
	AppMapPending::iterator  olPosPending = omAppMapPending.find (tAppPathName);
	if (olPosPending != omAppMapPending.end())
	{
		olPosPending->second.lId--;
		if (olPosPending->second.lId == 0)
		{
			omAppMapPending.erase (olPosPending);
		}
	}

	::LeaveCriticalSection(&omCriticalSection);
	return S_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Getting status of any application.
// If the app did register itself, the status is started.
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::get_IsAppStarted(BSTR AppPathName, BOOL *pVal)
{
	BOOL blResult = FALSE;

	::EnterCriticalSection(&omCriticalSection);
	AppMap::iterator olPos		= omAppMapStarted.end ();
	AppMap::iterator olPosBegin	= omAppMapStarted.begin();

	TCHAR	tAppPathName[GEN_PATH_LEN];
	char*	cpUpper = MakeUpperBSTR(AppPathName);
	strcpy (tAppPathName, cpUpper);

	while (olPos != olPosBegin)
	{
		olPos--;
		if (olPos->second.strAppPathName == tAppPathName)
		{
			blResult = TRUE;
			break;
		}
	}

	*pVal = blResult;

	::LeaveCriticalSection(&omCriticalSection);
	return S_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Getting status of any application.
// If the app was started by UFISAppMng and did not register itself yet,
// the status is pending.
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::get_IsAppPending(BSTR AppPathName, BOOL *pVal)
{
	BOOL blResult = FALSE;

	TCHAR	tAppPathName[GEN_PATH_LEN];
	char*	cpUpper = MakeUpperBSTR(AppPathName);
	strcpy (tAppPathName, cpUpper);

	::EnterCriticalSection(&omCriticalSection);
	AppMapPending::iterator olPosPending = omAppMapPending.find (tAppPathName);
	if (olPosPending != omAppMapPending.end())
	{
		blResult = TRUE;
	}

	*pVal = blResult;
	::LeaveCriticalSection(&omCriticalSection);

	return S_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Delivers all IDs of the started apps with the specified name.
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::get_GetIDsByAppName(BSTR AppPathName, BSTR *pVal)
{
	CComBSTR olResult;
	char cpTmp[20];
	bool blFirst = true;

	TCHAR	tAppPathName[GEN_PATH_LEN];
	char*	cpUpper = MakeUpperBSTR(AppPathName);
	strcpy (tAppPathName, cpUpper);

	::EnterCriticalSection(&omCriticalSection);
	AppMap::iterator olPos		= omAppMapStarted.end ();
	AppMap::iterator olPosBegin	= omAppMapStarted.begin();

	while (olPos != olPosBegin)
	{
		olPos--;
		if (olPos->second.strAppPathName == tAppPathName)
		{
			if (blFirst != false)
			{
				sprintf(cpTmp, "%ld", olPos->second.lId);
				blFirst = false;
			}
			else
			{
				sprintf(cpTmp, ",%ld", olPos->second.lId);
			}
			olResult += CComBSTR(cpTmp);
		}
	}

	*pVal = olResult.Detach();
	::LeaveCriticalSection(&omCriticalSection);

	return S_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Delivers the ID of the asking app.
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::get_GetMyID(long *pVal)
{
	long llResult;
	llResult = this->m_cookie;
	*pVal = llResult;
	return S_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Delivers the name of the app specified with the ID.
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::get_GetNameByID(long lID, BSTR *pVal)
{
	::EnterCriticalSection(&omCriticalSection);
	AppMap::iterator olPos = omAppMapStarted.find ((DWORD)lID);

	CComBSTR olResult = "";
	if (olPos != omAppMapStarted.end())
	{
		olResult = olPos->second.strAppPathName;
	}

	*pVal = olResult.Detach();

	::LeaveCriticalSection(&omCriticalSection);
	return S_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Sending data to all connected applications.
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::SendDataToAll(BSTR Data)
{
	::EnterCriticalSection(&omCriticalSection);
	AppMap::iterator olPos		= omAppMapStarted.end ();
	AppMap::iterator olPosBegin	= omAppMapStarted.begin();

	while (olPos != olPosBegin)
	{
		olPos--;
		(olPos->second.pUfisAppMgr)->Fire_OnDataReceive(Data);
	}
	::LeaveCriticalSection(&omCriticalSection);

	return S_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Delivers the Version of the UFISAppMng.
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
STDMETHODIMP CUfisAppMgr::get_GetVersion(BSTR *pVal)
{
	CComBSTR bstrVersion("4.4.0.2");
	*pVal = bstrVersion.Detach();

	return S_OK;
}

STDMETHODIMP CUfisAppMgr::get_BuildDate(BSTR *pVal)
{
	char pclResult[100]="";

	sprintf(pclResult, "%s - %s", __DATE__, __TIME__);
	CComBSTR bstrBuildDate(pclResult);
	*pVal = bstrBuildDate.Detach();
	return S_OK;
}

STDMETHODIMP CUfisAppMgr::get_Version(BSTR *pVal)
{
	CComBSTR bstrVersion("4.4.0.2");
	*pVal = bstrVersion.Detach();


	return S_OK;
}
