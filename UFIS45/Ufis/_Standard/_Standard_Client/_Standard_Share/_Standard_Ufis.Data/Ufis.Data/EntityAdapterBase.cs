﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;

namespace Ufis.Data
{
    /// <summary>
    /// Represents an abstract class to provide functionality to retrieve and save back
    /// data to the data source.
    /// </summary>
    public abstract class EntityAdapterBase
    {                 
        private readonly EntityDataContextBase _dataContext;

        /// <summary>
        /// Gets the EntityDataContext of this instance.
        /// </summary>
        public EntityDataContextBase DataContext
        {
            get { return _dataContext; }
        }
        /// <summary>
        /// Gets or sets a value indicating whether Ufis.Entities.Entity.AcceptChanges() 
        /// is called on Ufis.Entities.Entity after it is added to the collection
        /// during any of the Fill operations.
        /// </summary>
        public bool AcceptChangesDuringFill { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether Ufis.Entities.Entity.AcceptChanges()  
        /// is called during a Ufis.Data.EntityAdapterBase.Update().
        /// </summary>
        public bool AcceptChangesDuringUpdate { get; set; }
        /// <summary>
        /// Gets or sets a value that specifies whether to generate an exception 
        /// when an error is encountered during a row update.
        /// </summary>
        public bool ContinueUpdateOnError { get; set; }
        /// <summary>
        /// Gets or sets a value that enables or disables batch processing support, 
        /// and specifies the number of commands that can be executed in a batch.
        /// </summary>
        public int UpdateBatchSize { get; set; }
        /// <summary>
        /// Gets or sets a value that specifies whether to send broadcast
        /// when commands are executed in a batch.
        /// </summary>
        public bool SendBroadcastOnUpdateBatch { get; set; }

        /// <summary>
        /// Create the collection of entities with the records fetched from the data source
        /// by executing the Ufis.Data.DataCommand object.
        /// </summary>
        /// <param name="selectCommand">A Ufis.Data.DataCommand to select records from data source.</param>
        public abstract EntityCollectionBase<T> CreateEntityCollection<T>(DataCommand selectCommand);

        /// <summary>
        /// Returns the unique record identifier.
        /// </summary>
        /// <returns></returns>
        public virtual object GetUniqueId()
        {
            return Guid.NewGuid();
        }

        /// <summary>
        /// Adds or refreshes collection in the list of entities with the records fetched from the data source
        /// by executing the Ufis.Data.DataCommand object.
        /// </summary>
        /// <param name="entityList">A Ufis.Data.EntityCollectionBase to fill with records.</param>  
        /// <param name="selectCommand">A Ufis.Data.DataCommand to select records from data source.</param>  
        public void Fill<T>(EntityCollectionBase<T> entityList, DataCommand selectCommand)
        {
            if (entityList == null)
                throw new ArgumentNullException("entityList");

            if (selectCommand == null)
                throw new ArgumentNullException(null, "DataCommand for EntityAdapterBase cannot be null");

            Type type = typeof(T);
            if (!typeof(Entity).IsAssignableFrom(type))
                throw new ArgumentException("Type provided is not Entity type.");

            DoFill<T>(entityList, selectCommand);
        }

        /// <summary>
        /// Adds or refreshes collection in the list of entities with the records from
        /// the Ufis.Data.DataRowCollection object.
        /// </summary>
        /// <param name="entityList">A Ufis.Data.EntityCollectionBase to fill with records.</param>  
        /// <param name="rows">A Ufis.Data.DataRowCollection that contains the records.</param>  
        public void Fill<T>(EntityCollectionBase<T> entityList, DataRowCollection rows)
        {
            DoFill<T>(entityList, rows);
        }

        /// <summary>
        /// Adds or refreshes collection in the list of entities with the records fetched from the data source
        /// by executing the Ufis.Data.DataCommand object.
        /// </summary>
        /// <param name="entityList">A Ufis.Data.EntityCollectionBase to fill with records.</param>  
        /// <param name="selectCommand">A Ufis.Data.DataCommand to select records from data source.</param>  
        protected abstract void DoFill<T>(EntityCollectionBase<T> entityList, DataCommand selectCommand);

        /// <summary>
        /// Adds or refreshes collection in the list of entities with the records from
        /// the Ufis.Data.DataRowCollection object.
        /// </summary>
        /// <param name="entityList">A Ufis.Data.EntityCollectionBase to fill with records.</param>  
        /// <param name="rows">A Ufis.Data.DataRowCollection that contains the records.</param>  
        protected abstract void DoFill<T>(EntityCollectionBase<T> entityList, DataRowCollection rows);

        /// <summary>
        /// Calls the respective INSERT, UPDATE, or DELETE statements for each inserted, updated, 
        /// or deleted row in the specified list of Ufis.Entities.Entity objects.
        /// </summary>
        /// <param name="entityList">A list of Ufis.Data.EntityollectionBase objects used to update the data source.</param>
        public void Update<T>(EntityCollectionBase<T> entityList)
        {
            Type type = typeof(T);
            if (!typeof(Entity).IsAssignableFrom(type))
                throw new ArgumentException("Type provided is not Entity type.");

            bool bUpdateBatch = (UpdateBatchSize >= 0 && UpdateBatchSize != 1);
            try
            {
                if (bUpdateBatch)
                {
                    UpdateBatch<T>(entityList);
                }
                else
                {
                    foreach (T item in entityList)
                    {
                        object objItem = (object)item;
                        Entity entity = (Entity)objItem;
                        Update(entity, entityList.ColumnList);
                    }
                }
            }
            catch (DataException cedaEx)
            {
                if (!ContinueUpdateOnError) throw cedaEx;
            }
            catch (Exception ex)
            {
                if (!ContinueUpdateOnError) throw ex;
            }
        }

        /// <summary>
        /// Calls the respective INSERT, UPDATE, or DELETE statements for specified Ufis.Entities.Entity object. 
        /// This command will ignore Ufis.Data.EntityAdapterBase.UpdateBatchSize setting.
        /// </summary>
        /// <param name="entity">The Ufis.Entities.Entity object used to update the data source.</param>
        /// <param name="columnList">List of columns to update to the data source.</param>
        public void Update(Entity entity, string columnList)
        {
            Entity.EntityState entityState = entity.GetEntityState();

            if (entityState == Entity.EntityState.Unchanged)
                return;

            DoUpdate(entity, columnList, DataContext.DefaultTimeZoneInfo);
        }
       
        /// <summary>
        /// Calls the respective INSERT, UPDATE, or DELETE statements for specified Ufis.Entities.Entity object. 
        /// This command will ignore Ufis.Data.EntityAdapterBase.UpdateBatchSize setting.
        /// </summary>
        /// <param name="entity">The Ufis.Entities.Entity object used to update the data source.</param>
        /// <param name="columnList">List of columns to update to the data source.</param>
        /// <param name="timeZoneInfo">The TimeZoneInfo class that represents local time zone.</param>
        protected abstract void DoUpdate(Entity entity, string columnList, TimeZoneInfo timeZoneInfo);

        /// <summary>
        /// Calls the respective INSERT, UPDATE, or DELETE statements in batch mode.
        /// </summary>
        protected abstract void UpdateBatch<T>(EntityCollectionBase<T> entityList);
        
        /// <summary>
        /// Initializes the new instance of Ufis.Data.EntityAdapterBase class.
        /// </summary>
        /// <param name="dataContext">The Ufis.Data.EntityDataContextBase that owns this instance of Ufis.Data.EntityAdapterBase class.</param>
        public EntityAdapterBase(EntityDataContextBase dataContext)
        {
            _dataContext = dataContext;
            AcceptChangesDuringFill = true;
            AcceptChangesDuringUpdate = true;
            ContinueUpdateOnError = true;
            UpdateBatchSize = 1;
            SendBroadcastOnUpdateBatch = true;
        }
    }
}
