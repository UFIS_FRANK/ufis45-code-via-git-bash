﻿using System;
using System.Collections.Generic;

namespace Ufis.Data
{
    /// <summary>
    /// Represents a collection of Ufis.Data.DataRow object.
    /// </summary>
    public class DataRowCollection
    {
        private readonly List<DataRow> _rows;

        /// <summary>
        /// Gets the total number of Ufis.Data.DataRow objects in this collection.
        /// </summary>
        public int Count
        {
            get { return _rows.Count; }
        }

        /// <summary>
        /// Returns an array of Ufis.Data.DataRow objects in this collection.
        /// </summary>
        /// <returns></returns>
        public DataRow[] GetRowArray()
        {
            return _rows.ToArray();
        }
        
        /// <summary>
        /// Adds Ufis.Data.DataRow objects to this collection.
        /// </summary>
        /// <param name="data">The data to add.</param>
        /// <param name="separator">The row separator.</param>
        public void AddRows(string data, params char[] separator)
        {
            string[] arrRows = data.Split(separator);
            AddRows(arrRows);
        }

        /// <summary>
        /// Adds Ufis.Data.DataRow objects to this collection.
        /// </summary>
        /// <param name="rows">An array of string contains data.</param>
        public void AddRows(string[] rows)
        {
            DataRow[] arrRow = new DataRow[rows.Length];
            int i = 0;
            foreach (string strRow in rows)
            {
                arrRow[i++] = new DataRow(strRow.Split(','));
            }
            AddRows(arrRow);
        }

        /// <summary>
        /// Adds Ufis.Data.DataRow objects to this collection.
        /// </summary>
        /// <param name="rows">The Ufis.Data.DataRowCollection object.</param>
        public void AddRows(DataRowCollection rows)
        {
            DataRow[] arrRow = rows.GetRowArray();
            AddRows(arrRow);
        }

        /// <summary>
        /// Adds Ufis.Data.DataRow objects to this collection.
        /// </summary>
        /// <param name="rows">An array of Ufis.Data.DataRow objects.</param>
        public void AddRows(DataRow[] rows)
        {
            _rows.AddRange(rows);
        }

        /// <summary>
        /// Adds Ufis.Data.DataRow object to this collection.
        /// </summary>
        /// <param name="row">The string contains data</param>
        /// <param name="separator">The item separator.</param>
        public void AddRow(string row, params char[] separator)
        {
            string[] arrItems = row.Split(separator);
            AddRow(arrItems);
        }

        /// <summary>
        /// Adds Ufis.Data.DataRow object to this collection.
        /// </summary>
        /// <param name="items">An array of string contains the values of row items.</param>
        public void AddRow(string[] items)
        {
            DataRow row = new DataRow(items);
            AddRow(row);
        }

        /// <summary>
        /// Adds Ufis.Data.DataRow object to this collection.
        /// </summary>
        /// <param name="row">The Ufis.Data.DataRow object.</param>
        public void AddRow(DataRow row)
        {
            _rows.Add(row);
        }

        /// <summary>
        /// Clear the collection of all rows.
        /// </summary>
        public void Clear()
        {
            _rows.Clear();
        }

        /// <summary>
        /// </summary>
        /// <param name="index">The zero-based index of row in the collection.</param>
        /// <returns></returns>
        public DataRow this[int index]
        {
            get { return _rows[index]; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.DataRowCollection class.
        /// </summary>
        public DataRowCollection()  
        {
            _rows = new List<DataRow>();
        }
    }
}
