﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Data
{
    public class DataUtils
    {
        /// <summary>
        /// Get the full table name from specified table name and table extension.
        /// </summary>
        /// <param name="tableName">The table name.</param>
        /// <param name="tableExtension">The table extension.</param>
        /// <returns>Full table name.</returns>
        public static string GetFullTableName(string tableName, string tableExtension)
        {
            string strFullTableName;
            if (string.IsNullOrEmpty(tableName))
                strFullTableName = string.Empty;
            else
            {
                if (tableName.EndsWith(tableExtension))
                    strFullTableName = tableName;
                else
                    strFullTableName = tableName + tableExtension;
            }

            return strFullTableName;
        }
    }
}
