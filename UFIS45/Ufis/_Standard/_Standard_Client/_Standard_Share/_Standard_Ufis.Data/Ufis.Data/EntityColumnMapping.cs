﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Ufis.Data
{
    /// <summary>
    /// Represents the mapping for entity's attribute to table's column.
    /// </summary>
    public class EntityColumnMapping
    {
        public class BooleanConverter
        {
            /// <summary>
            /// Gets or sets the equivalent for true value.
            /// </summary>
            public object TrueValue { get; set; }

            /// <summary>
            /// Gets or sets the equivalent for false value.
            /// </summary>
            public object FalseValue { get; set; }
        }

        /// <summary>
        /// Gets or sets the property info of entity's attribute.
        /// </summary>
        public PropertyInfo PropertyInfo { get; set; }
        /// <summary>
        /// Gets or sets the table's column name.
        /// </summary>
        public string ColumnName { get; set; }
        /// <summary>
        /// Gets or sets the table's column data type.
        /// </summary>
        public Type ColumnDataType { get; set; }
        /// <summary>
        /// Gets or sets whether this column is a primary key.
        /// </summary>
        public bool IsPrimaryKey { get; set; }
        /// <summary>
        /// Gets or sets whether this column is a unique field.
        /// </summary>
        public bool IsUnique { get; set; }
        /// <summary>
        /// Gets or sets whether this column is a mandatory field.
        /// </summary>
        public bool IsMandatory { get; set; }
        /// <summary>
        /// Gets or sets whether this column is a system read only field.
        /// </summary>
        public bool IsReadOnly { get; set; }
        /// <summary>
        /// Gets or sets the maximum length of strings to be stored in table's column.
        /// </summary>
        public int MaxLength { get; set; }
        /// <summary>
        /// Gets or sets the BooleanConverter object for this column.
        /// </summary>
        public BooleanConverter BoolConverter { get; set; }
    }
}
