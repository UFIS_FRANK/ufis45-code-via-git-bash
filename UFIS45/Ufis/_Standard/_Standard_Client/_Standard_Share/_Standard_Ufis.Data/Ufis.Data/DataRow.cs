﻿using System;

namespace Ufis.Data
{
    /// <summary>
    /// Represents a row of data returned from server.
    /// </summary>
    public class DataRow
    {
        /// <summary>
        /// Gets or sets all the values for this row through an array.
        /// </summary>
        public string[] ItemArray { get; set; }

        /// <summary>
        /// </summary>
        /// <param name="index">The zero-based index of an item in the row.</param>
        /// <returns></returns>
        public string this[int index]
        {
            get { return ItemArray[index]; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.DataRow class.
        /// </summary>
        public DataRow() : this(null) { }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.DataRow class.
        /// </summary>
        /// <param name="items">An array of string contains the values for this row.</param>
        public DataRow(string[] items)
        {
            ItemArray = items;
        }
    }
}
