﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Broadcast;
using Ufis.Entities;

namespace Ufis.Data
{
    /// <summary>
    /// Represents an abstract class for entity database.
    /// </summary>
    public abstract class EntityDataContextBase : IDisposable
    {
        private IUfisBroadcast _ufisBroadcast;        
        private readonly EntityTableMappingCollection _entityTableMaps = new EntityTableMappingCollection();
        private readonly List<object> _entityCollectionList = new List<object>();
        private TimeZoneInfo _defaultTimeZoneInfo;
        
        /// <summary>
        /// Gets the entity to database table mapping object.
        /// </summary>
        public EntityTableMappingCollection EntityTableMappings
        {
            get
            {
                return _entityTableMaps;
            }
        }

        /// <summary>
        /// Gets or sets the brodcast manager.
        /// </summary>
        public IUfisBroadcast BroadcastManager
        {
            get
            {
                return _ufisBroadcast;
            }
            set
            {
                if (_ufisBroadcast != value)
                {
                    if (_ufisBroadcast != null)
                        _ufisBroadcast.BroadcastReceived -= BroadcastManager_OnBroadcastReceived;
                    if (value != null)
                    {
                        _ufisBroadcast = value;
                        _ufisBroadcast.BroadcastReceived += BroadcastManager_OnBroadcastReceived;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the connection for this data context.
        /// </summary>
        public ConnectionBase Connection { get; set; }

        /// <summary>
        /// Gets or sets the entity adapter for reading and saving data to data source.
        /// </summary>
        public EntityAdapterBase EntityAdapter { get; protected set; }

        /// <summary>
        /// Gets or sets the default time zone for this data context.
        /// </summary>
        public TimeZoneInfo DefaultTimeZoneInfo 
        {
            get 
            {
                if (_defaultTimeZoneInfo == null)
                    _defaultTimeZoneInfo = GetTimeZoneInfo();
                return _defaultTimeZoneInfo;
            }
            set
            {
                _defaultTimeZoneInfo = value;
            }
        }

        /// <summary>
        /// Occurs when broadcast is received.
        /// </summary>
        public event EventHandler<BroadcastEventArgs> BroadcastReceived;

        protected virtual void OnBroadcastReceived(BroadcastEventArgs e)
        {
            EventHandler<BroadcastEventArgs> handler = BroadcastReceived;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs after broadcast is processed or handled.
        /// </summary>
        public event EventHandler<BroadcastEventArgs> BroadcastHandled;

        protected virtual void OnBroadcastHandled(BroadcastEventArgs e)
        {
            EventHandler<BroadcastEventArgs> handler = BroadcastHandled;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Opens and adds entity collection object to data context.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selectCommand">Select command to retrieve data from data source.</param>
        /// <returns></returns>
        public EntityCollectionBase<T> OpenEntityCollection<T>(DataCommand selectCommand)
        {
            string strTableExtension = Connection.TableExtension;

            _entityTableMaps.Add(typeof(T), strTableExtension);

            EntityCollectionBase<T> entityCollection = CreateEntityCollection<T>(selectCommand);
            if (entityCollection != null)
            {
                _entityCollectionList.Add(entityCollection);
            }

            return entityCollection;
        }

        /// <summary>
        /// Fills entity collection with data retrieve from select command.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entityCollection">A Ufis.Data.EntityCollectionBase to fill with records.</param>  
        /// <param name="selectCommand">Select command to retrieve data from data source.</param>
        public void FillEntityCollection<T>(EntityCollectionBase<T> entityCollection, DataCommand selectCommand)
        {
            EntityAdapter.Fill<T>(entityCollection, selectCommand);
        }

        /// <summary>
        /// Fills entity collection with the records from
        /// the Ufis.Data.DataRowCollection object.
        /// </summary>
        /// <param name="entityCollection">A Ufis.Data.EntityCollectionBase to fill with records.</param>  
        /// <param name="selectCommand">A Ufis.Data.DataRowCollection that contains the records.</param>  
        public void FillEntityCollection<T>(EntityCollectionBase<T> entityCollection, DataRowCollection rows)
        {
            EntityAdapter.Fill<T>(entityCollection, rows);
        }

        /// <summary>
        /// Closes an entity collection and removes it from the data context.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entityCollection">The entity collection to close.</param>
        internal void CloseEntityCollection<T>(EntityCollectionBase<T> entityCollection)
        {
            if (_entityCollectionList.Contains(entityCollection))
            {
                _entityCollectionList.Remove(entityCollection);
                entityCollection = null;
            }
        }

        /// <summary>
        /// Creates entity collection object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selectCommand">Select command to retrieve data from data source.</param>
        /// <returns></returns>
        protected abstract EntityCollectionBase<T> CreateEntityCollection<T>(DataCommand selectCommand);
        /// <summary>
        /// Executes the command that will not return data.
        /// </summary>
        /// <param name="dataCommand">Data command to execute.</param>
        public abstract void ExecuteNonQueryCommand(DataCommand dataCommand);
        /// <summary>
        /// Executes the command that will return the data row collection object.
        /// </summary>
        /// <param name="dataCommand">Data command to execute.</param>
        /// <returns></returns>
        public abstract DataRowCollection ExecuteQueryCommand(DataCommand dataCommand);
        /// <summary>
        /// Save the collection of entity to the data source.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entityCollection">The entity collection.</param>
        public abstract void SaveEntityCollection<T>(EntityCollectionBase<T> entityCollection);
        /// <summary>
        /// Save an entity to the data source.
        /// </summary>
        /// <param name="entity">The entity object to save.</param>
        /// <param name="columnList">The list of columns to save.</param>
        public abstract void SaveEntity(Entity entity, string columnList);

        /// <summary>
        /// Initializes a new instance of Ufis.Data.EntityDataContextBase class.
        /// </summary>
        public EntityDataContextBase()
        {
        }

        /// <summary>
        /// Initializes a new instance of Ufis.Data.EntityDataContextBase class.
        /// </summary>
        /// <param name="connection">The connection used for this data context.</param>
        public EntityDataContextBase(ConnectionBase connection)
        {
            Connection = connection;
        }

        /// <summary>
        /// Initializes a new instance of Ufis.Data.EntityDataContextBase class.
        /// </summary>
        /// <param name="connection">The connection used for this data context.</param>
        /// <param name="ufisBroadcast">The brodcast manager.</param>
        public EntityDataContextBase(ConnectionBase connection, IUfisBroadcast ufisBroadcast)
        {
            Connection = connection;
            BroadcastManager = ufisBroadcast;
        }

        private void BroadcastManager_OnBroadcastReceived(object sender, BroadcastEventArgs e)
        {
            OnBroadcastReceived(e);
            if (!e.Handled)
            {
                string strTableName = GetTableNameFromBroadcast(e.BroadcastMessage);
                EntityTableMapping tableMaps = _entityTableMaps[strTableName];
                if (tableMaps != null)
                {
                    foreach (object entityCollection in _entityCollectionList)
                    {
                        Type entityCollectionType = entityCollection.GetType();
                        Type entityType = entityCollectionType.GetGenericArguments()[0];
                        tableMaps = _entityTableMaps[entityType];
                        if (tableMaps != null && tableMaps.TableName == strTableName)
                        {
                            System.Reflection.MethodInfo methodInfo = entityCollectionType.GetMethod("RaiseBroadcastReceived");
                            if (methodInfo != null)
                            {
                                methodInfo.Invoke(entityCollection, new object[] { e });
                            }
                        }
                    }
                }
            }
            OnBroadcastHandled(e);
        }

        /// <summary>
        /// Gets the table name from broadcast message.
        /// </summary>
        /// <param name="bcMessage">The broadcast message.</param>
        /// <returns></returns>
        protected abstract string GetTableNameFromBroadcast(BroadcastMessageBase bcMessage);

        /// <summary>
        /// Get System.TimeZoneInfo class for home airport.
        /// </summary>
        /// <returns></returns>
        public TimeZoneInfo GetTimeZoneInfo()
        {
            return GetTimeZoneInfo(Connection.HomeAirport);
        }

        /// <summary>
        /// Get System.TimeZoneInfo class from specific airport.
        /// </summary>
        /// <param name="airportCode">The airport code.</param>
        /// <returns></returns>
        public virtual TimeZoneInfo GetTimeZoneInfo(string airportCode)
        {
            //TODO: Read airport database and get the TimeZoneInfo
            return TimeZoneInfo.Local;
        }

        /// <summary>
        /// Disposes and cleans up the data context.
        /// </summary>
        public void Dispose()
        {
            if (_ufisBroadcast != null)
            {
                _ufisBroadcast.BroadcastReceived -= BroadcastManager_OnBroadcastReceived;
                _ufisBroadcast.Dispose();
            }
        }
    }
}
