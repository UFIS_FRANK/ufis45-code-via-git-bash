/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 *	UFISAppMang:	UFIS Client Application Manager Project
 *
 *	Proxy Communicator for Client Applications
 *	
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Author			Date			Comment
 *	------------------------------------------------------------------
 *
 *		cla				02/09/2000		Initial version
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
// ClntCon.cpp: Implementierung der Klasse CClntCon.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <ClntCon.h>

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CClntCon::CClntCon()
{

}

CClntCon::~CClntCon()
{

}

CClntCon::CClntCon(IUnknown* pUnkSink, DWORD pdwCookie,ClntTags appID)
{
	m_pUnk = pUnkSink;
	m_appID = appID;
	m_cookie = pdwCookie;
}

CClntCon::CClntCon(const CClntCon& rhs)
{
	m_pUnk = rhs.m_pUnk;
	m_appID = rhs.m_appID;
	m_cookie = rhs.m_cookie;
}

CClntCon& CClntCon::operator=(const CClntCon& rhs)
{
	m_pUnk = rhs.m_pUnk;
	m_appID = rhs.m_appID;
	m_cookie = rhs.m_cookie;
	return(*this);
}

bool CClntCon::operator==(const CClntCon& rhs)const
{
	if(m_cookie== rhs.m_cookie)
	{
		return(true);
	}
	return(false);
}

bool CClntCon::operator==(const DWORD cookie)const
{
	if(m_cookie == cookie)
	{
		return(true);
	}
	return(false);
}
