/*******************************************************************

    netutil.c

    This file contains routines of general utility.

********************************************************************/

#include <time.h>

#include <netutil.h>
#include <ufismult.h>
                   

// External Variables
                   
extern char ufis_ct[40];
extern char bufmanag_ct[40];
extern MULTI multi[MAX_NO_OF_APPL];				/* Multi application variables		*/
extern char nam_service[];						/* Namsrv service name: CEDA, EXCO, CEDA2 ...*/

// Defines

#define UFIS_PORT_NO 			3350 
#define SYMAP_PORT_NO 			3450 
#define UFIS_BC_PORT_NO 		3351 
#define UFIS_NAM_SERVICE_NAME 	3340
                       

// Pointer to external trace buffer
extern char trace_buffer[];

/* Define GETSERVBYNAME_WORKS, if it realy works */
#undef GETSERVBYNAME_WORKS

/* Define GETHOSTBYNAME_WORKS, if it realy works */
#define GETHOSTBYNAME_WORKS

/* Define USE_BCHEAD_NETWORK_BYTE_ORDER, if BC_HEAD should be converted */
#undef USE_BCHEAD_NETWORK_BYTE_ORDER


/* Prototypes */
static int SendBcBuffer (SOCKET socket, LPSTR Pdata, int buflen, short bc_port_no)  ;

// Statics                                   
                                   

/*******************************************************************

    NAME:       SockerrToString

    SYNOPSIS:   Maps a socket error (like WSAEINTR) to a displayable
                form (like "Interrupted system call").

    ENTRY:      serr - The error to map.

    RETURNS:    LPSTR - The displayable form of the error.  Will be
                    "Unknown" for unknown errors.

********************************************************************/
LPSTR SockerrToString( SOCKERR serr )
{
	static char errbuf[32];
	
    switch( serr )
    {
    case WSAENAMETOOLONG :
        return "Name too long";

    case WSANOTINITIALISED :
        return "Not initialized";

    case WSASYSNOTREADY :
        return "System not ready";

    case WSAVERNOTSUPPORTED :
        return "Version is not supported";

    case WSAESHUTDOWN :
        return "Can't send after socket shutdown";

    case WSAEINTR :
        return "Interrupted system call";

    case WSAHOST_NOT_FOUND :
        return "Host not found";

    case WSATRY_AGAIN :
        return "Try again";

    case WSANO_RECOVERY :
        return "Non-recoverable error";

    case WSANO_DATA :
        return "No data record available";

    case WSAEBADF :
        return "Bad file number";

    case WSAEWOULDBLOCK :
        return "Operation would block";

    case WSAEINPROGRESS :
        return "Operation now in progress";

    case WSAEALREADY :
        return "Operation already in progress";

    case WSAEFAULT :
        return "Bad address";

    case WSAEDESTADDRREQ :
        return "Destination address required";

    case WSAEMSGSIZE :
        return "Message too long";

    case WSAEPFNOSUPPORT :
        return "Protocol family not supported";

    case WSAENOTEMPTY :
        return "Directory not empty";

    case WSAEPROCLIM :
        return "EPROCLIM returned";

    case WSAEUSERS :
        return "EUSERS returned";

    case WSAEDQUOT :
        return "Disk quota exceeded";

    case WSAESTALE :
        return "ESTALE returned";

    case WSAEINVAL :
        return "Invalid argument";

    case WSAEMFILE :
        return "Too many open files";

    case WSAEACCES :
        return "Access denied";

    case WSAELOOP :
        return "Too many levels of symbolic links";

    case WSAEREMOTE :
        return "The object is remote";

    case WSAENOTSOCK :
        return "Socket operation on non-socket";

    case WSAEADDRNOTAVAIL :
        return "Can't assign requested address";

    case WSAEADDRINUSE :
        return "Address already in use";

    case WSAEAFNOSUPPORT :
        return "Address family not supported by protocol family";

    case WSAESOCKTNOSUPPORT :
        return "Socket type not supported";

    case WSAEPROTONOSUPPORT :
        return "Protocol not supported";

    case WSAENOBUFS :
        return "No buffer space is supported";

    case WSAETIMEDOUT :
        return "Connection timed out";

    case WSAEISCONN :
        return "Socket is already connected";

    case WSAENOTCONN :
        return "Socket is not connected";

    case WSAENOPROTOOPT :
        return "Bad protocol option";

    case WSAECONNRESET :
        return "Connection reset by peer";

    case WSAECONNABORTED :
        return "Software caused connection abort";

    case WSAENETDOWN :
        return "Network is down";

    case WSAENETRESET :
        return "Network was reset";

    case WSAECONNREFUSED :
        return "Connection refused";

    case WSAEHOSTDOWN :
        return "Host is down";

    case WSAEHOSTUNREACH :
        return "Host is unreachable";

    case WSAEPROTOTYPE :
        return "Protocol is wrong type for socket";

    case WSAEOPNOTSUPP :
        return "Operation not supported on socket";

    case WSAENETUNREACH :
        return "ICMP network unreachable";

    case WSAETOOMANYREFS :
        return "Too many references";

    case 0 :
        return "No error";

    default :
    	sprintf (errbuf, "Unknown error code %d", serr);
        return errbuf;
    }
}


/*******************************************************************

    NAME:       ResetSocket

    SYNOPSIS:   Performs a "hard" close on the given socket.

    ENTRY:      sock - The socket to close.

    RETURNS:    SOCKERR - RC_SUCCESS if successful, RC_FAIL if not.

********************************************************************/
SOCKERR ResetSocket( SOCKET sock )
{                
	int rc = RC_SUCCESS;
    LINGER linger;

    if( sock == INVALID_SOCKET )
    {
        //
        //  Ignore invalid sockets.
        //

        return RC_FAIL;
    }

    //
    //  Enable linger with a timeout of zero.  This will
    //  force the hard close when we call closesocket().
    //
    //  We ignore the error return from setsockopt.  If it
    //  fails, we'll just try to close the socket anyway.
    //

    linger.l_onoff  = TRUE;
    linger.l_linger = 0;

    setsockopt( sock,
                SOL_SOCKET,
                SO_LINGER,
                (CHAR FAR *)&linger,
                sizeof(linger) );

    //
    //  Close the socket.
    //

    rc = closesocket( sock );
    if (rc < 0)
    	rc = RC_FAIL;
    	
    return rc;

}   // ResetSocket

                           
/*******************************************************************

    NAME:       InitWinsock

    SYNOPSIS:   Init of the Windows Socket Library

    ENTRY:      address - The IP address for the socket.  NOT USED TODAY

    RETURNS:    SOCKERR - RC_SUCCESS if successful, RC_FAIL if not.

********************************************************************/

int InitWinsock  (void)
{                
	int rc = RC_SUCCESS;      
	SOCKERR serr=0;
    WSADATA wsadata;
	char timebuf[20];	
	int t_i=-1;
	char buf[140];

	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return RC_NOT_FOUND;
			    
	/* Date and Time to trace file */	              
	_strdate (timebuf);   
	timebuf[8] = ' ';
	_strtime (timebuf + 9);
	
	if (multi[t_i].outpt != NULL)
	{
		fprintf(multi[t_i].outpt, 
		"UFISMULT-DLL by CCS GmbH, Heusenstamm:\nInitWinsock started %s\n"
		"Compile times\n\tufismult.c: %s\n\tnetutil.c:  %s\n\tbufmanag.c: %s\n", 
		timebuf, ufis_ct, __TIMESTAMP__, bufmanag_ct);
    }
    
//	trace_buffer = GetTraceBuf();
	                 
    //
    //  Initialize the sockets library.
    //

    serr = WSAStartup( DESIRED_WINSOCK_VERSION, &wsadata );

    if( serr != 0 )
    {
		if (multi[t_i].outpt != NULL) 
		{
			fprintf(multi[t_i].outpt,
				"UFIS-DLL:\nInitWinsock: Cannot initialize socket library, error %d: %s",
                serr, SockerrToString( serr ) );
			fflush(multi[t_i].outpt);
		}
        return RC_COMM_FAIL;
    }

    if( wsadata.wVersion < MINIMUM_WINSOCK_VERSION )
    {
		if (multi[t_i].outpt != NULL) 
		{
	        fprintf(multi[t_i].outpt,
				"InitWinsock:Windows Sockets version %02X.%02X, I need at least %02X.%02X",
                LOBYTE(wsadata.wVersion),
                HIBYTE(wsadata.wVersion),
                LOBYTE(MINIMUM_WINSOCK_VERSION),
                HIBYTE(MINIMUM_WINSOCK_VERSION) );
			fflush(multi[t_i].outpt);
		}

        return RC_COMM_FAIL;
    }  
	if (multi[t_i].outpt != NULL) 
	{
	    fprintf(multi[t_i].outpt,
			"InitWinsock:Windows Sockets version %02X.%02X\n",
                LOBYTE(wsadata.wVersion),
                HIBYTE(wsadata.wVersion),
                LOBYTE(MINIMUM_WINSOCK_VERSION),
                HIBYTE(MINIMUM_WINSOCK_VERSION) );
    	fprintf(multi[t_i].outpt,
			"Winsock Description: \n%s\nWinsock Status:\n%s\n", wsadata.szDescription, 
				wsadata.szSystemStatus);
    	fprintf(multi[t_i].outpt,
			"MaxSockets: %d MaxUdpDatagram Size = %d Bytes\n", wsadata.iMaxSockets, 
				wsadata.iMaxUdpDg);
		rc = GetWorkstationName ((LPSTR) buf);
		if (rc != RC_SUCCESS)
			strcpy (buf, "unknown");
    	fprintf(multi[t_i].outpt, 
    	"-----------------------------------------------Hostname: %s ----------\n",buf);
		fflush(multi[t_i].outpt); 
    } 
        
    return rc;
}
                           
/*******************************************************************

    NAME:       CreateSocket

    SYNOPSIS:   Creates a data socket for the specified address & port.
                                                                          
    ENTRY:      psock - Will receive the new socket ID if successful.

                type - The socket type (SOCK_DGRAM or SOCK_STREAM).

                service - The IP service name. 
                
                non_blocking - Bool, if socket should be non_blocking or blocking

    RETURNS:    SOCKERR - RC_SUCCESS if successful, RC_FAIL if not.

********************************************************************/
SOCKERR CreateSocket( SOCKET FAR * psock, int type, LPSTR service,
					  short non_blocking )
{
    SOCKET  sNew;
    SOCKERR serr;

    //
    //  Create the socket.
    //

    sNew = socket( AF_INET, type, 0 );
    serr = ( sNew == INVALID_SOCKET)  ? WSAGetLastError() : 0;

    if( serr != 0 )
    {
		serr = WSAGetLastError();
		sprintf(trace_buffer,"CreateSocket: %Fs\n", 
			SockerrToString( serr ));
		WriteTrace(trace_buffer, WT_ERROR);
        ResetSocket( sNew );
        sNew = INVALID_SOCKET;
	    return RC_COMM_FAIL;
    }
    else
    { 
    	*psock = sNew;
		sprintf(trace_buffer,"CreateSocket:blocking socket %d created\n", 
			 sNew);
		WriteTrace(trace_buffer, WT_DEBUG);
    	return RC_SUCCESS;
    }
    
}   // CreateSocket


/*******************************************************************

    NAME:       CreateSocketBind

    SYNOPSIS:   Creates a data socket for the specified address & port.
                                                                          
    ENTRY:      psock - Will receive the new socket ID if successful.

                type - The socket type (SOCK_DGRAM or SOCK_STREAM).

                service - The IP service name. 
                
                non_blocking - Bool, if socket should be non_blocking or blocking
                
                port_no - Port Number

    RETURNS:    SOCKERR - RC_SUCCESS if successful, RC_FAIL if not.

********************************************************************/
SOCKERR CreateSocketBind( SOCKET FAR * psock, int type, LPSTR service,
					  short non_blocking, short port_no )
{
    SOCKET  sNew;
    SOCKERR serr;
    SOCKADDR_IN dummy;
    int rc;
    
    //
    //  Create the socket.
    //

    sNew = socket( AF_INET, type, 0 );
    serr = ( sNew == INVALID_SOCKET)  ? WSAGetLastError() : 0;

    if( serr != 0 )
    {
        ResetSocket( sNew );
        sNew = INVALID_SOCKET;
	    return RC_COMM_FAIL;
    }
    else
    { 
    	*psock = sNew;
		sprintf(trace_buffer,"CreateSocket:blocking socket %d created\n",sNew);
		WriteTrace(trace_buffer, WT_DEBUG);
		
		dummy.sin_addr.s_addr = 0;
		dummy.sin_family = AF_INET;
		dummy.sin_port = htons(port_no);
		rc = bind( sNew, (SOCKADDR FAR *)&dummy, sizeof(SOCKADDR_IN) );
		sprintf(trace_buffer,"BindSocket: Return Code %d \n",rc);
		WriteTrace(trace_buffer, WT_DEBUG);
		
		if (rc == 0)
		{
    		return RC_SUCCESS;
    	}
    	else
		{
    		return RC_COMM_FAIL;
    	}    	
    }
    
}   // CreateSocketBind


/*******************************************************************

    NAME:       ListenSocket

    SYNOPSIS:   Establishes a socket for incomming connections
                                                                          
    ENTRY:      socket - An unconnected socket

                backlog - The maximum length to which the queue of pending
                		  connections may grow

    RETURNS:    SOCKERR - RC_SUCCESS if successful, RC_FAIL if not.

********************************************************************/
SOCKERR ListenSocket( SOCKET socket, int backlog )
{
    int rc;
    SOCKERR serr;
    
    //
    //  Listen to socket.
    //

    rc = listen( (SOCKET) socket, backlog );
    
    if (rc != 0)
    {
    	serr = WSAGetLastError();
    	sprintf(trace_buffer,"ListenSocket: %d , Listen: %s\n",socket, SockerrToString(serr));
    	WriteTrace(trace_buffer,WT_ERROR);
    	rc = RC_COMM_FAIL;
    }
    else
    {
		sprintf(trace_buffer,"ListenSocket %d successful\n",socket);
		WriteTrace(trace_buffer, WT_DEBUG);
    	rc = RC_SUCCESS;
    }
    
    return rc;
    
}   // ListenSocket


/*******************************************************************

    NAME:       AcceptSocket

    SYNOPSIS:   Accept a connection on a socket
                                                                          
    ENTRY:      psock - Will receive the new socket ID if successful.

       	        socket - An unconnected socket

    RETURNS:    SOCKERR - RC_SUCCESS if successful, RC_FAIL if not.

********************************************************************/
SOCKERR AcceptSocket( SOCKET FAR * psock, SOCKET socket )
{
    SOCKET  sNew;
    SOCKERR serr;
    SOCKADDR_IN remote_addr;
    int remote_len = sizeof(SOCKADDR_IN);

    //
    //  Accept connection.
    //
    
/*
    serr = WSACancelBlockingCall();
    if (serr != 0)
    {
    	serr = WSAGetLastError();
    	sprintf(trace_buffer,"AcceptSocket: %d , CancelBlockingCall: %s\n",socket, SockerrToString(serr));
    	WriteTrace(trace_buffer,WT_ERROR);
    }
    else
    {
    	sprintf(trace_buffer,"AcceptSocket: %d , CancelBlockingCall: OK\n",socket);
    	WriteTrace(trace_buffer,WT_ERROR);
    }
*/
    
	sNew = INVALID_SOCKET;
    sNew = accept( socket, (struct sockaddr FAR *)&remote_addr, (int FAR *)&remote_len);
    serr = ( sNew == INVALID_SOCKET)  ? WSAGetLastError() : 0;

	sprintf(trace_buffer,"AcceptSocket: sNew = %d\n", sNew);
	WriteTrace(trace_buffer, WT_DEBUG);

    if( serr != 0 )
    {
        ResetSocket( sNew );
        sNew = INVALID_SOCKET;
    	sprintf(trace_buffer,"AcceptSocket: %d , Accept: %s\n",socket, SockerrToString(serr));
    	WriteTrace(trace_buffer,WT_ERROR);
	    return RC_COMM_FAIL;
    }
    else
    { 
    	*psock = sNew;
		sprintf(trace_buffer,"AcceptSocket: %d , socket %d connected\n", socket, sNew);
		WriteTrace(trace_buffer, WT_DEBUG);
    	return RC_SUCCESS;
    }
    
}   // AcceptSocket


/*******************************************************************

    NAME:       OpenConnection

    SYNOPSIS:   Opens a connection to the specified host & port on the
    			specified socket.


    RETURNS:    RC_SUCCESS if successful, RC_FAIL if not.

********************************************************************/
int OpenConnection (SOCKET socket, LPSTR hostname, short remote_port_no)
{
		int rc=RC_FAIL;
/*		HOSTENT FAR *hp; */
		LPHOSTENT hp; 
		SOCKADDR_IN	dummy;
		SOCKERR serr=0;
#ifdef GETSERVBYNAME_WORKS
    	LPSERVENT sp;
#endif

		if ( ( hp = gethostbyname( hostname)) == NULL ) {
			serr = WSAGetLastError();
			sprintf(trace_buffer,"OpenConnection:Gethostbyname: %Fs  %Fs\n", 
				SockerrToString( serr ), hostname );
			WriteTrace(trace_buffer, WT_ERROR);
			return RC_FAIL;
		} /* end if */

		memcpy((char FAR *)&dummy.sin_addr,
				*hp->h_addr_list,
				 hp->h_length);
				 
#ifdef GETSERVBYNAME_WORKS
		/*    Doesn't work  ... */
		sprintf(trace_buffer,"NETIF:got host by name\n"); WriteTrace(trace_buffer, WT_DEBUG);
		if ( ( sp = getservbyname((char FAR *) "UFIS",NULL) ) == NULL ) {
			serr = WSAGetLastError();
			sprintf(trace_buffer,"OpenConnection: Getservbyname: %s\n", SockerrToString( serr ) );
			WriteTrace(trace_buffer, WT_ERROR);
			return RC_FAIL;
		} 
//		sprintf(trace_buffer,"adr hp = 0x%lx sp = 0x%lx\n",hp,sp); WriteTrace(trace_buffer, WT_DEBUG);
		
		dummy.sin_port   = sp->s_port;
		sprintf(trace_buffer,"OpenConnection:got service UFIS\n"); WriteTrace(trace_buffer, WT_DEBUG);

#else
		dummy.sin_port   = htons (remote_port_no);
#endif

		dummy.sin_family = AF_INET;
		
		if ((rc = connect(socket,(SOCKADDR FAR *) &dummy,sizeof(dummy))) <0) 
		{
			sprintf(trace_buffer,"OpenConnection: ConnectFailed: \n");
			WriteTrace(trace_buffer, WT_ERROR);    
			
			serr = WSAGetLastError();
			sprintf(trace_buffer,"OpenConnection: Connect: %s\n", SockerrToString( serr ) );
			WriteTrace(trace_buffer, WT_ERROR);    
			return RC_FAIL;
		} /* end if */

		sprintf(trace_buffer,"OpenConnection:got connection\n"); WriteTrace(trace_buffer, WT_DEBUG);
	
 		return RC_SUCCESS;                                         
}  /* OpenConnection */                                         



/*******************************************************************

    NAME:       CleanupWinsock

    SYNOPSIS:   Ends the Socket communication


    RETURNS:    RC_SUCCESS if successful, RC_FAIL if not.

********************************************************************/
int CleanupWinsock (void)
{
	SOCKERR serr=0;
	char timebuf[20];	
	int t_i=-1;

	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return RC_NOT_FOUND;
			    
 
    serr = WSACleanup ();  
    if (serr != 0)
    {
		serr = WSAGetLastError();
		sprintf(trace_buffer,"Cleanup: %s\n",SockerrToString( serr ) );
		WriteTrace(trace_buffer, WT_DEBUG);
		return RC_FAIL;	
	}
	else
	{	
		/* Date and Time to trace file */	              
		_strdate (timebuf);   
		timebuf[8] = ' ';
		_strtime (timebuf + 9);
	
		if (multi[t_i].outpt != NULL)
		{
			fprintf(multi[t_i].outpt, "UFISMULT-DLL: CleanupWinsock: Ends %s\n", timebuf);
    	}
		
		return RC_SUCCESS;	
	}   /* fi */
}  /* CleanupWinsock */


#ifdef FUTUR

/* ******************************************************************** */
/* Following the ForwardData function					*/
/* ******************************************************************** */
int ForwardData( SOCKET sock, LPSTR hostname, short port_no, LPSTR data, 
						int length)
{
	int	rc;
	LPSTR errstr;
	SOCKERR sockerr;
    SOCKADDR_IN sockAddr;         
	HOSTENT FAR *hp;


	if ( ( hp = gethostbyname( hostname)) == NULL ) {
		sockerr = WSAGetLastError();
		sprintf(trace_buffer,"ForwardData:Gethostbyname: %Fs  %Fs\n", 
			SockerrToString( sockerr ), hostname );
		WriteTrace(trace_buffer, WT_ERROR);
		return RC_FAIL;
	} /* end if */
			
	sprintf(trace_buffer,
	"ForwardData:Debug: name=%Fs sin_addr = 0x%lx len=%d\n",
		hp->h_name, 
		(u_long) (hp->h_addr), hp->h_length );
	WriteTrace(trace_buffer, WT_DEBUG);

	memcpy((char FAR *)&sockAddr.sin_addr,
			  *hp->h_addr_list,
			  hp->h_length);
    sockAddr.sin_family      = AF_INET;
    sockAddr.sin_port        = htons (port_no);
    
	rc =RC_FAIL;
	
	rc = sendto(sock,data,length,0, (SOCKADDR FAR *) &sockAddr, 
				sizeof (SOCKADDR_IN));
	if ( rc <0 ) {
		sockerr = WSAGetLastError();
		errstr = SockerrToString (sockerr);
	  	sprintf(trace_buffer,"ForwardData: sendto: socket = 0x%x: %s\n", sock,
	  		errstr);
		WriteTrace(trace_buffer, WT_ERROR);
	  	sprintf(trace_buffer,"ForwardData: rc = %d : str=%s \n"
		  ,rc,strerror(errno));
		WriteTrace(trace_buffer, WT_ERROR);
	} /* end if */
	else {
		sprintf(trace_buffer,"ForwardData: %d bytes send \n",rc);
		WriteTrace(trace_buffer, WT_DEBUG);
	} /* end else */
	
	if (rc < 0 )
		return RC_FAIL;
	else
		return RC_SUCCESS;
} /* end of forward */

#endif /* FURUR */

  
/* ******************************************************************** */
/* Following the ReceiveData function									*/ 
/*																		*/ 
/* Gets the length of the buffer packet in *length and returns the 		*/
/* number of bytes got there. If the rc is RC_SUCCESS and the returned	*/
/* length is 0, no data has been arrived.								*/
/* if the rc is RC_SHUTDOWN, the connection has been closed by the other*/
/* host.																*/
/* ******************************************************************** */ 

int ReceiveData( SOCKET sock, LPSTR packet, int FAR *length)
{
	int	rc = RC_SUCCESS; 
	int cnt=0;
	int r_len=0;
	SOCKERR serr;                      
	SOCKADDR_IN remote_addr;
	int remote_len=sizeof(SOCKADDR_IN);
	LPSTR cur_packet=packet;
	fd_set rfds;
	struct timeval timeout;
	
	FD_ZERO (&rfds);	
	FD_SET  (sock, &rfds);
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;
	
	for (r_len=0; rc == RC_SUCCESS && r_len < *length; r_len += cnt, cur_packet += cnt)
	{	                   
		
	  rc = select (0, (fd_set far *) &rfds, NULL, NULL, (struct timeval far *) &timeout);
	  sprintf(trace_buffer,"ReceiveData: Select: returned %d",rc); 
	  WriteTrace(trace_buffer, WT_DEBUG);	
	  
	  if (rc == SOCKET_ERROR)
	  {
		sprintf(trace_buffer,"ReceiveData: Select: %s  socket=%d", 
				SockerrToString( serr ),sock );
		WriteTrace(trace_buffer, WT_ERROR);	
		rc = RC_FAIL;
	  }
	  else
	  {
	    if (rc == 0)
	    {
	    	rc = RC_TIMEOUT;
	    }
	    else
	    {
	    	rc = RC_SUCCESS;
	    }
	  }
	  
	  if (rc == RC_SUCCESS)
	  {	
  		cnt = recvfrom (sock, cur_packet, *length, 0, 
						  (struct sockaddr FAR *) &remote_addr, 
						  (int FAR *) &remote_len);
		if (cnt == SOCKET_ERROR)
		{
			serr = WSAGetLastError();
			if (serr != WSAEWOULDBLOCK)
			{
				sprintf(trace_buffer,"ReceiveData: Recvfrom: %s  socket=%d", 
					SockerrToString( serr ),sock );
				WriteTrace(trace_buffer, WT_ERROR);	
			} /* fi */
			if (serr == WSAEWOULDBLOCK)
			{
				rc = RC_NOT_FOUND;
			}
			else
			{
				rc = RC_FAIL ;
			} /* end if */
		}
		else
		{   
			if (cnt == 0)
			{
				rc = RC_NOT_FOUND;
				sprintf(trace_buffer,
					"ReceiveData: 0 Bytes received from ip 0x%lx remote_len=%d", 
					remote_addr.sin_addr.s_addr, remote_len);
				WriteTrace(trace_buffer, WT_ERROR);
			}
			else
			{
				sprintf(trace_buffer,
					"ReceiveData: received  %d bytes from ip 0x%lx",
			 		cnt, remote_addr.sin_addr.s_addr); 
				WriteTrace(trace_buffer, WT_DEBUG);
			} /* fi */
		} /* fi */                 
	  } /* fi */                 
    } /* for */
    
    if (rc == RC_SUCCESS)
    {
    	*length = r_len;
    } 
	else
	{
		*length = 0;
    } /* fi */
    
	return rc;
}  /* ReceiveData */


/* ******************************************************************** */
/* Following the bchead_hton function									*/
/* Converts from network to host order 									*/
/* ******************************************************************** */
void bchead_hton  (BC_HEAD FAR *Pbc_head)
{
#ifdef USE_BCHEAD_NETWORK_BYTE_ORDER

  Pbc_head->bc_num = htons (Pbc_head->bc_num);
  Pbc_head->tot_buf = htons (Pbc_head->tot_buf);
  Pbc_head->act_buf = htons (Pbc_head->act_buf);
  Pbc_head->rc = htons (Pbc_head->rc);
  Pbc_head->tot_size = htons (Pbc_head->tot_size);
  Pbc_head->cmd_size = htons (Pbc_head->cmd_size);
  Pbc_head->data_size = htons (Pbc_head->data_size);

#endif
}  /* bchead_hton */


/* ******************************************************************** */
/* Following the bchead_hton function									*/
/* Converts from network to host order 									*/
/* ******************************************************************** */
void bchead_ntoh  (BC_HEAD FAR *Pbc_head)
{
#ifdef USE_BCHEAD_NETWORK_BYTE_ORDER

  Pbc_head->bc_num = ntohs (Pbc_head->bc_num);
  Pbc_head->tot_buf = ntohs (Pbc_head->tot_buf);
  Pbc_head->act_buf = ntohs (Pbc_head->act_buf);
  Pbc_head->rc = ntohs (Pbc_head->rc);
  Pbc_head->tot_size = ntohs (Pbc_head->tot_size);
  Pbc_head->cmd_size = ntohs (Pbc_head->cmd_size);
  Pbc_head->data_size = ntohs (Pbc_head->data_size);

#endif
}  /* bchead_hton */
	

/* ******************************************************************** */
/* Following the GetWorkstationName function							*/
/* Returns the Host name of the Workstation configured as local host		*/
/* ******************************************************************** */
int far pascal GetWorkstationName (LPSTR ws_name)
{
	int rc = RC_SUCCESS;
	char	pclTmpBuf[512];
	char	pclConfigPath[512];
	static BOOL	blUseIpAddress = -1;
	SOCKERR serr;                      

	Yield();

	if (blUseIpAddress == -1)
	{
		blUseIpAddress = FALSE;
	    if (getenv("CEDA") == NULL)
	        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	    else
	        strcpy(pclConfigPath, getenv("CEDA"));
	
		GetPrivateProfileString("GLOBAL","USEWORKSTATIONNAME","YES",pclTmpBuf,512,pclConfigPath);
		if (stricmp(pclTmpBuf,"NO") == 0)
		{
			blUseIpAddress = TRUE;
		}
	}
	
	if ( rc = gethostname( ws_name,130) == SOCKET_ERROR ) 
	{
		serr = WSAGetLastError();
		sprintf(trace_buffer,"GetWorkstationName:gethostname: %Fs\n",SockerrToString( serr ));
		WriteTrace(trace_buffer, WT_ERROR);
		return RC_FAIL;
	}
	else if (blUseIpAddress)
	{
		struct hostent *polHostEnt = gethostbyname(ws_name);
		if (polHostEnt != NULL)
		{
			IN_ADDR olIpAddress = (*(IN_ADDR *)polHostEnt->h_addr_list[0]);
			sprintf(ws_name,"%X",olIpAddress.S_un.S_addr);
		}
		else
		{
			serr = WSAGetLastError();
			sprintf(trace_buffer,"GetWorkstationName:gethostbyname: %Fs\n",SockerrToString( serr ));
			WriteTrace(trace_buffer, WT_ERROR);
			return RC_FAIL;
		}
	}

	return rc;
} /* GetWorkstationName */	          

/* ********************************************************************			*/
/* Following the GetWorkstationName function									*/
/* Returns the "Real" Host name of the Workstation configured as local host		*/
/* ********************************************************************			*/
int far pascal GetRealWorkstationName (LPSTR ws_name)
{
	int rc = RC_SUCCESS;
	SOCKERR serr;                      

	Yield();

	if ( rc = gethostname( ws_name,130) == SOCKET_ERROR ) 
	{
		serr = WSAGetLastError();
		sprintf(trace_buffer,"GetRealWorkstationName:gethostname: %Fs\n",SockerrToString( serr ));
		WriteTrace(trace_buffer, WT_ERROR);
		return RC_FAIL;
	}

	return rc;
} /* GetRealWorkstationName */	          

// -----------------------------------------------------------------
//
// Function: SendBcBuffer
//
// Purpose : Sends the BC_HEAD buffer on the broadcast socket
//
// Params  : see below
//
// Returns : 0 Operation was successful. 
//			 < 0:  error
//
// Comments: 
//           
// -----------------------------------------------------------------
static int SendBcBuffer (SOCKET socket, LPSTR Pdata, int buflen, short bc_port_no)  
{             
	int rc = RC_SUCCESS;
	SOCKERR serr = 0;
	SOCKADDR_IN dummy;
	
	sprintf(trace_buffer,"SendBcBuffer: Start: sock=%d len=%d Pdata=%p", socket, buflen, Pdata);
	WriteTrace(trace_buffer, WT_DEBUG);    

	memset ((LPSTR) &dummy, 0, sizeof(SOCKADDR_IN));
	dummy.sin_addr.s_addr = (u_long) INADDR_BROADCAST;
	dummy.sin_family = AF_INET;
	dummy.sin_port = htons(bc_port_no);

	sprintf(trace_buffer,"SendBcBuffer: Vor sendto", socket);
	WriteTrace(trace_buffer, WT_DEBUG);    
	rc = sendto (socket, Pdata, buflen, 0, (SOCKADDR FAR *) &dummy,
				 sizeof (SOCKADDR_IN));

	sprintf(trace_buffer,"SendBcBuffer: done: %d", socket);
	WriteTrace(trace_buffer, WT_DEBUG);    
	if ( rc == SOCKET_ERROR ) {
		serr = WSAGetLastError();
		sprintf(trace_buffer,"SendBcBuffer: send: %s", SockerrToString( serr ) );
		WriteTrace(trace_buffer, WT_DEBUG);    
		rc = RC_COMM_FAIL;
	} /* end if */

    if (rc != RC_COMM_FAIL)
    	rc = RC_SUCCESS;
 	return rc;
} /* end SendBcBuffer */




// -----------------------------------------------------------------
//
// Function: GetCedaHost
//
// Purpose : 
//
// Params  : see below
//
// Returns : 0 Operation was successful. 
//			 < 0:  error
//
// Comments: 
//           
// -----------------------------------------------------------------
int GetCedaHost (LPSTR Phostname)  
{             
	int rc = RC_SUCCESS;    
	SOCKET bc_socket_r=0;	
	SOCKET bc_socket_w=0;	
	short broadcast_bool = 1;	/* Arg for setsockopt */
    SOCKERR serr;
    SOCKADDR_IN sockAddr;
  	int length;
    LPHOSTENT lpHost;
	CMDBLK cmdblk;
	CMDBLK rec_cmdblk; 
    char own_name[16];
	char buf[132];    
	int i=0;
	int lcnt;
	int rc1;
	
    if (rc == RC_SUCCESS)
    { 
    	rc = CreateSocket( (SOCKET FAR *) &bc_socket_r, SOCK_DGRAM, 
    					(LPSTR) UFIS_NAM_SERVICE_NAME, FALSE );
		sprintf(trace_buffer,"GetCedaHost: Nach CreateSocket (bc1) %d",rc); 
		WriteTrace(trace_buffer, WT_DEBUG);
    }

    if (rc == RC_SUCCESS)
    {							
		if ( rc = gethostname(buf,130) == SOCKET_ERROR ) 
		{
			serr = WSAGetLastError();
			sprintf(trace_buffer,"GetCedaHost:Debug gethostname: %Fs\n",SockerrToString( serr ));
			WriteTrace(trace_buffer, WT_ERROR);
			rc = RC_FAIL;
		}

    	if (rc == RC_SUCCESS)
    	{
    		lpHost = gethostbyname (buf);
    	} /* fi */

    	if (rc != RC_SUCCESS || lpHost != NULL)
    	{
	        //
    	    //  Bind an address to the socket.
        	//

			memcpy((char FAR *)&sockAddr.sin_addr,
				  	(char FAR *) *(lpHost->h_addr_list),	lpHost->h_length);


			sprintf(trace_buffer,
			"GetCedaHost:Debug gethostbyname: name=%Fs sin_addr = 0x%lx len=%d\n",
				lpHost->h_name, 
				(u_long) (lpHost->h_addr), lpHost->h_length );
			WriteTrace(trace_buffer, WT_DEBUG);

			strcpy ((LPSTR) own_name, (LPSTR) lpHost->h_name);
	       	sockAddr.sin_family  = AF_INET;
    	   	memset (sockAddr.sin_zero, '\0', sizeof (sockAddr.sin_zero)); 
       		sockAddr.sin_port        = htons (UFIS_NAM_SERVICE_NAME);
   		
	   		if( bind( bc_socket_r, (LPSOCKADDR)&sockAddr, sizeof(SOCKADDR_IN)) != 0 )
   			{
				serr = WSAGetLastError();
				sprintf(trace_buffer,"GetCedaHost:bind: %Fs\n", SockerrToString( serr ));
				WriteTrace(trace_buffer, WT_ERROR);    
				rc = RC_FAIL;
			}
		}
		else
		{
			serr = WSAGetLastError();
			sprintf(trace_buffer,"GetCedaHost:gethostbyname: %Fs\n", SockerrToString( serr ));
			WriteTrace(trace_buffer, WT_ERROR);    
			rc = RC_FAIL;
		}
    }
        
        
    if (rc == RC_SUCCESS)
    { 
    	rc = CreateSocket( (SOCKET FAR *) &bc_socket_w, SOCK_DGRAM, 
    					(LPSTR) UFIS_NAM_SERVICE_NAME, FALSE );
		sprintf(trace_buffer,"GetCedaHost: Nach CreateSocket (bc2) %d",rc); 
		WriteTrace(trace_buffer, WT_DEBUG);
    }

    if (rc == RC_SUCCESS)
    {
    	rc = setsockopt( bc_socket_w, SOL_SOCKET, SO_BROADCAST, (char *) &broadcast_bool, 
    					 sizeof (broadcast_bool) );
		sprintf(trace_buffer,"GetCedaHost: Nach SetSockOpt (SO_BROADCAST) %d",rc); 
		WriteTrace(trace_buffer, WT_DEBUG);
    }
            
    if (rc == RC_SUCCESS)
    {
	    memset ((LPSTR) &rec_cmdblk, 0, sizeof(CMDBLK));
   	 	strcpy ((LPSTR) cmdblk.obj_name, nam_service );
    	strcpy ((LPSTR) cmdblk.command, NAMSRV_REQ_CMD);
    	strcpy ((LPSTR) cmdblk.tw_end, (LPSTR) own_name);
                    
		rc = SendBcBuffer (bc_socket_w, (LPSTR) &cmdblk, sizeof(CMDBLK), 
			(short)UFIS_NAM_SERVICE_NAME);                    
		sprintf(trace_buffer,"GetCedaHost: Nach SendBcBuffer rc= %d, Params = %s , %s , %s",
				rc,(LPSTR)cmdblk.obj_name,(LPSTR)cmdblk.command,(LPSTR)cmdblk.tw_end); 
		WriteTrace(trace_buffer, WT_DEBUG);
	}
    
    if (rc == RC_SUCCESS)
    {
    	length=sizeof(CMDBLK);
		rc = ReceiveData( bc_socket_r, (LPSTR) &rec_cmdblk, (int FAR *) &length) ;
		sprintf(trace_buffer,"GetCedaHost: Nach ReceiveData rc= %d length=%d\n"
		"Looking for Hosttype %s",rc, length, nam_service); 
		WriteTrace(trace_buffer, WT_DEBUG);
	}

	// If own packet. again (for MS TCP/IP and other)
    if (rc == RC_SUCCESS && strcmp ((LPSTR)own_name, (LPSTR)rec_cmdblk.tw_end) == 0)
    {
	   	length=sizeof(CMDBLK);
		rc = ReceiveData( bc_socket_r, (LPSTR) &rec_cmdblk, (int FAR *) &length) ;
		sprintf(trace_buffer,
		"GetCedaHost: Rec own packet:Nach ReceiveData 2 rc= %d length=%d\n"
		"Looking for Hosttype %s",rc, length, nam_service); 
		WriteTrace(trace_buffer, WT_DEBUG);
	} /* fi */

	if (rc == RC_TIMEOUT)
	{
		rc = RC_NOT_FOUND;
	}

	rc1 = RC_FAIL;
	lcnt = 0;		
    while (rc == RC_SUCCESS && rc1 == RC_FAIL && lcnt < 10)
    {
 		if (strcmp ((LPSTR) rec_cmdblk.command, (LPSTR) NAMSRV_INFO_CMD) != 0 ||
		    strcmp ((LPSTR) rec_cmdblk.obj_name, (LPSTR) nam_service) != 0)
		{
			sprintf(trace_buffer,"GetCedaHost: received wrong packet: %d Params = %s , %s , %s",
					lcnt+2,(LPSTR)rec_cmdblk.obj_name,(LPSTR)rec_cmdblk.command,
					(LPSTR)rec_cmdblk.tw_end); 
			WriteTrace(trace_buffer, WT_DEBUG);
		   	length=sizeof(CMDBLK);
			rc = ReceiveData( bc_socket_r, (LPSTR) &rec_cmdblk, (int FAR *) &length) ;
			sprintf(trace_buffer,
			"GetCedaHost: Nach ReceiveData %d rc= %d length=%d\n"
			"Looking for Hosttype %s",lcnt+2,rc, length, nam_service); 
			WriteTrace(trace_buffer, WT_DEBUG);
			lcnt++;
		}
		else
		{
			rc1 = RC_SUCCESS;
		}
	}
	
    if (rc == RC_SUCCESS)
    {
 		if (strcmp ((LPSTR) rec_cmdblk.command, (LPSTR) NAMSRV_INFO_CMD) == 0 &&
		    strcmp ((LPSTR) rec_cmdblk.obj_name, (LPSTR) nam_service) == 0)
		{
	  		for (i=0;rec_cmdblk.tw_start[i] != EOS && 
	  				 rec_cmdblk.tw_start[i] != '.'; i++)
	    		;
	  		rec_cmdblk.tw_start[i] = EOS;
			sprintf(trace_buffer,"GetCedaHost:received answer packet from %s!",
				rec_cmdblk.tw_start); 
			WriteTrace(trace_buffer, WT_DEBUG);
			sprintf(trace_buffer,"GetCedaHost: received answer: Params = %s , %s , %s",
					(LPSTR)rec_cmdblk.obj_name,(LPSTR)rec_cmdblk.command,(LPSTR)rec_cmdblk.tw_end); 
			WriteTrace(trace_buffer, WT_DEBUG);

	  		strcpy (Phostname, (LPSTR) rec_cmdblk.tw_start);
		  	rc = RC_SUCCESS;
		}
		else
		{
			sprintf(trace_buffer,"GetCedaHost:received wrong packet from %s!",
				rec_cmdblk.tw_start); 
			WriteTrace(trace_buffer, WT_DEBUG);      
			sprintf(trace_buffer,"GetCedaHost: received wrong packet: Params = %s , %s , %s",
					(LPSTR)rec_cmdblk.obj_name,(LPSTR)rec_cmdblk.command,(LPSTR)rec_cmdblk.tw_end); 
			WriteTrace(trace_buffer, WT_DEBUG);
			rc = RC_FAIL;
		}
	}  
                   
	if (bc_socket_r > 0)
	{                   
		ResetSocket (bc_socket_r);
	}
	if (bc_socket_w > 0)
	{                   
		ResetSocket (bc_socket_w);
	}
	
	sprintf(trace_buffer,"GetCedaHost:reeturned %d!",rc);
	WriteTrace(trace_buffer, WT_DEBUG);      
	return rc;
} /* GetCedaHost */