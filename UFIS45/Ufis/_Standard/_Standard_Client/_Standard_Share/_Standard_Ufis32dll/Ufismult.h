// -----------------------------------------------------------------
// File name:  UFIS.H
//
// This header file contains the function prototypes for functions
// exported by the DLL named DLLSKEL.
//
// -----------------------------------------------------------------
                                                      
// Includes
                                                      
#include <sys/types.h> 
#include <stdio.h>
#include <STRING.H>
#include <memory.h> 

#define MAX_BC_BUF_LEN     (100000)

// Traces
#define DEFAULT_TRACE_FN	"c:\\tmp\\ufisdef.log"        
#define TRACE_FN_LEN		(80)                 

// Maximum number of applications
#define MAX_NO_OF_APPL	8

// UFIS QUIT BCSERV Message
#define UFIS_QUIT WM_USER+21

// GVC SYNC Message
#define GVC_SYNC WM_USER+22

// Number of big buffers per task
#define NO_OF_DATA_BUFFERS 4

// Max number of connections
#define NO_OF_CONNECTIONS 40

// Types

// BC entry

struct BcStruct
{
	BOOL	IsValid;
	time_t	BcReceived;
	BC_HEAD	BcHead;
	CMDBLK	CmdBlk;
	char    *BcData;
	struct BcStruct *NextBc;
};

typedef struct BcStruct BC_STRUCT;

// Following the MULTI type

typedef struct {
	int used;  						/* Bool: Is entry this currently used ?	*/
	DWORD task;						/* Actual task handle				*/
	SOCKET gl_conn_socket;			/* Socket for connection 			*/
	SOCKET gl_conn_DDC_socket;		/* Socket for connection (DDC)		*/
	SOCKET gl_bc_socket;			/* Socket for broadcasts			*/
	SOCKET gl_conn_gvc[NO_OF_CONNECTIONS]; /* Sockets for GVCs			*/
	short InCallCeda;				/* Are we currently in CallCeda ? 	*/
	short InGetBc;					/* Are we currently in GetBc ? 	*/
	short ActBcNum;					/* Actual BC-Num                   */
	short FirstBcNum;					/* Actual BC-Num                   */
	short BcFirst;					/* Number of oldest received BC */
	short BcLast;					/* Number of newest received BC  */
	time_t LastBcTime;				/* time of the last bc we received */
	HWND  BcWindow;					/* Handle of window to receive the WM_BCADD Message */
	short IsInit;					/* InitComm has been called 		*/
	char  TraceFileName [TRACE_FN_LEN];	/* Current trace file name 		*/
	short do_trace;					/* Write trace infos 				*/
	char  ceda_host_name[82];		/* network name for CEDA host 		*/
	HGLOBAL cc_handle; 				/* Handle for CallCeda buffer		*/
	short IsNonBlock;				/* Is the conn socket in blocking mode ?*/ 
	unsigned long data_buf_size;	/* Data buffer size in toolbook 	*/ 
	FILE *outpt;					/* Trace FILE Pointer				*/
//	char PendingBCs[MAX_BC_BUF_LEN];/* pending BCs for this Application    */
	HGLOBAL buf_handle[NO_OF_DATA_BUFFERS];	/* Buffer Handle			*/
	DWORD buf_len[NO_OF_DATA_BUFFERS];/* Buffer Length					*/
} MULTI;
              
/*
 *
 * Following the FileTransfer Header .....FT_HEAD
 *
 *
 */                        
typedef struct {
	short		command;
	char		param[128];
	char		dest_param[128];
	char		data[1];
}FT_HEAD;
 
/*
 *
 * Following the Message Block MSGBLK
 *
 *
 */                        
typedef struct {
	long	msg_no;
	long	prio;
	char	data[1]; // 4 Strings: Flight_no, rot_no, ref_fld, msg_data
} MSGBLK;




extern HANDLE ghDLLInst;      // Global to store DLL instance handle
                              

// Defines
// For FileTransfer
                               
#define		RC_ERROR		0xff                           
#define		WRITE_FILE		0x1
#define		GET_FILE		0x2
#define		RESULT			0x3
#define		PASSWD			0x4                                      
#define		RESET_JOB		0x5                                      


// WriteTrace options
#define WT_DEBUG		1
#define WT_ERROR		2                              
#define WT_FATAL_ERROR	3                              


// Function Prototypes

int  PASCAL InitCom (char * HostType, char * CedaHost);
int  PASCAL RegisterBcWindow(HWND hlBcWindow);
int  PASCAL UnRegisterBcWindow(HWND hlBcWindow);
int  PASCAL InitComDDC  (char * IpAddr, char * CedaHost, int bind);
int  PASCAL CleanupCom  (void);
int  PASCAL CleanupComGvc  (int gvcInd);
int  PASCAL CallCeda (char * req_id,char * dest1, char * dest2, char * cmd, char * object,
	          char * seq, char * tws, char * twe, char * selection, char * fields,
	          char * data, char * data_dest) ;
int  PASCAL SendBc (char * req_id,char * dest1, char * dest2, char * cmd, char * object,
	          char * seq, char * tws, char * twe, char * selection, char * fields,
	          char * data, char * data_dest) ;
int  PASCAL GetBc (char * req_id,char * dest1, char * dest2, char * cmd, char * object,
	          char * seq, char * tws, char * twe, char * selection, char * fields,
	          char * data, char * bc_num) ;
void WriteTrace ( char * buf, int prio);
void snap (char * buf, int len);
void NewSnap (char * buf, int buflen,FILE *prpOut);

int  PASCAL UfisDllAdmin  (char * Pcommand, char * Parg1, char * Parg2);
char *GetTraceBuf  (void);  
int MakeNewMultiStruct (void);
//int  PASCAL BcBufAdd (char * Pentry);
int  PASCAL BcBufAdd (short spBcNum);
//int PASCAL do_filter (char * Pdata);
int FAR PASCAL do_filter(LPSTR Pentry,char *pcpTrace);

int PASCAL GetActualBcNum (void);
int PASCAL GetFirstBcNum (void);

void CCSMemCpy (char  *Psrc, char  *Pdest, DWORD cnt);
DWORD CCSStrLen  (char  *Pbuf);
void DoFree ( HGLOBAL handle);
char * DoMalloc (long buflen, HGLOBAL *Phandle );
char * DoRealloc (DWORD buflen, HGLOBAL *Phandle );
int  PASCAL SendText(char *text);
int  PASCAL StopBCSERV (void);
int  PASCAL StartBCSERV (void);

// BUFMANAG
int StoreBuffer (BC_HEAD  *data, DWORD buflen, char * data_dest, int t_i);
int  PASCAL GetResultBuffer (char * Presult, int buflen, char * buf_slot, int line_no, 
					 int line_cnt, char * item_list);
int  PASCAL GetNumberOfLines (char  *buf_slot);
int  PASCAL BufferDeleteLine (int line_no, char * buf_slot);
int  PASCAL BufferInsertLine (int line_no, char * data, char * buf_slot);
int  PASCAL BufferUpdateLine (int line_no, char * item_list, char * data, 
								 char * buf_slot);
int  PASCAL ConvDateBuf  (char * Pbuf_slot, int ItemNo);
int  pascal ConvCharBuf  (char * Pbuf_slot, int ItemNo, char * OldChar, char * NewChar);
int  pascal BufGetRowNumber  (char * Pbuf_slot, int line_no, char * Pitems, 
								 char * Pdata);
int  pascal BufAllocate (char * data_dest, DWORD buflen);
int  pascal BufU2TbArray  (char * Pbuf_slot, int ItemNo);
int  pascal BufferAppendLine (char * data, char * buf_slot);
int  pascal WriteBufToFile  (char * Pbuf_slot, char * Path);
int  pascal BufU2TbArray2  (char * Pbuf_slot, int ItemNo);
int  pascal GetBufferHandle (char * buf_slot, HGLOBAL *Phandle);
int  PASCAL GetLostBcCount();