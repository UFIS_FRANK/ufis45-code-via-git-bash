﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("_Standard_Bdpsuif_Addon")]
[assembly: AssemblyDescription("Last Changes On - 07.May.2012")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("UFIS Airport Solutions GmbH")]
[assembly: AssemblyProduct("_Standard_Bdpsuif_Addon")]
[assembly: AssemblyCopyright("Copyright ©UFIS Airport Solutions GmbH")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("1e0cc622-f350-49a5-bd99-df9a814d024b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("4.5.0.2")]
[assembly: AssemblyFileVersion("4.5.0.2")]

//4.5.0.2
//Zaw Min Tun
//Changed to .Net 2.0
//All using System.Linq are commented for that
//And while receving the data parsed from BDPSUIF.exe V 4.5.2.2, the parameter passed will be parsed in a way as follows
//if it is b or k, it will be taken as it is, if not, it will be taken as BLANK