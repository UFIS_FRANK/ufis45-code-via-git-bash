﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Xml;
using Ufis.Data;
using Ufis.Utils;
using _Standard_Bdpsuif_Addon.Helper;

namespace _Standard_Bdpsuif_Addon.Ctrl
{
    public class CtrlBDIns
    {
        private IDatabase myDB = CtrlData.GetInstance().MyDB;

        public class Table
        {
            public enum TableTypeEnum
            {
                MasterTable = 1,
                DetailTable = 2
            }

            public class Column
            {
                public enum ColumnDataTypeEnum
                {
                    String,
                    Int,
                    Double
                }

                public string Name { get; set; }
                public ColumnDataTypeEnum DataType { get; set; }
                public string ReferenceColumnName { get; set; }
                public bool UpdateMasterTable { get; set; }
                public string Expression { get; set; }
                public string Value { get; set; }

                public static ColumnDataTypeEnum GetColumnDataType(string dataType)
                {
                    switch (dataType.ToUpper())
                    {
                        case "INT":
                        case "INT32":
                            return ColumnDataTypeEnum.Int;
                        case "DBL":
                        case "DOUBLE":
                        case "FLOAT":
                            return ColumnDataTypeEnum.Double;
                        default:
                            return ColumnDataTypeEnum.String;
                    }
                }

                public Column(string name, string value)
                {
                    this.Name = name;
                    this.DataType = ColumnDataTypeEnum.String;
                    this.ReferenceColumnName = name;
                    this.UpdateMasterTable = false;
                    this.Expression = string.Empty;
                    this.Value = value;
                }

                public Column(string name) : this(name, null) { }

                public Column() : this(string.Empty, null) { }

                public override string ToString()
                {
                    return ToString("\"");
                }

                public string ToString(string stringSeparator)
                {
                    string value = (this.Value == null ? string.Empty : this.Value);
                    switch (this.DataType)
                    {
                        case Table.Column.ColumnDataTypeEnum.String:
                            return stringSeparator + value + stringSeparator;
                        case Table.Column.ColumnDataTypeEnum.Double:
                            double dblValue;
                            if (!double.TryParse(value, out dblValue))
                            {
                                dblValue = 0;
                            }
                            return string.Format("{0:0.00}", dblValue);
                        default:
                            return value;
                    }
                }
            }

            public class ColumnCollection : List<Column>
            {
                private Dictionary<string, Column> columns = null;

                public bool TryGetColumnByName(string columnName, out Column column)
                {
                    return TryGetColumnByName(columnName, false, out column);
                }

                public bool TryGetColumnByName(string columnName, bool recalculate, out Column column)
                {
                    if (recalculate || columns == null)
                    {
                        if (columns == null)
                            columns = new Dictionary<string, Column>();
                        foreach (Column col in this)
                        {
                            columns.Add(col.Name, col);
                        }
                    }
                    if (columns == null)
                    {
                        column = null;
                        return false;
                    }
                    else
                    {
                        return columns.TryGetValue(columnName, out column);
                    }
                }
            }

            public string TableName { get; set; }
            public TableTypeEnum Type { get; set; }
            public ColumnCollection Columns { get; set; }

            public Table()
            {
                this.TableName = string.Empty;
                this.Type = TableTypeEnum.DetailTable;
                this.Columns = new ColumnCollection();
            }

            public static TableTypeEnum GetTableType(string tableType)
            {
                switch (tableType.ToUpper())
                {
                    case "MASTER":
                    case "MASTERTABLE":
                        return TableTypeEnum.MasterTable;
                    default:
                        return TableTypeEnum.DetailTable;
                }
            }
        }

        public class TableCollection : List<Table>
        {
            public bool TryGetTable(string tableName, out Table table)
            {
                foreach (Table tbl in this)
                {
                    if (tbl.TableName == tableName)
                    {
                        table = tbl;
                        return true;
                    }
                }
                table = null;
                return false;
            }

            public Table MasterTable
            {
                get
                {
                    foreach (Table table in this)
                    {
                        if (table.Type == Table.TableTypeEnum.MasterTable)
                        {
                            return table;
                        }
                    }

                    return null;
                }
            }

            public TableCollection DetailTables
            {
                get
                {
                    TableCollection detailTables = new TableCollection();

                    foreach (Table table in this)
                    {
                        if (table.Type == Table.TableTypeEnum.DetailTable)
                        {
                            detailTables.Add(table);
                        }
                    }

                    return detailTables;
                }
            }
        }

        public class SeparatorCollection : List<char>
        {

        }

        public SeparatorCollection Separators { get; set; }
        public TableCollection Tables { get; set; }

        public CtrlBDIns()
        {
            this.Tables = new TableCollection();
            this.Separators = new SeparatorCollection();
        }

        public bool InsertToValidityTable(string urno, string fieldAndValueList)
        {
            bool success = false;

            Table masterTable = this.Tables.MasterTable;
            if (masterTable != null)
            {
                AddMasterTableColumns(masterTable, urno, fieldAndValueList);

                string masterFields = string.Empty, masterValues = string.Empty;

                TableCollection detailTables = this.Tables.DetailTables;
                foreach (Table detailTable in detailTables)
                {
                    string fullTableName = detailTable.TableName + "TAB";
                    string fields = string.Empty, values = string.Empty;
                                
                    foreach (Table.Column column in detailTable.Columns)
                    {
                        string value = string.Empty;
                        if (!string.IsNullOrEmpty(column.Expression))
                        {
                            string expression = EvaluateExpression(column.Expression, masterTable.Columns);
                            object result = RuntimeEvalHelper.Eval(expression);
                            if (result != null)
                            {
                                column.Value = result.ToString();
                                value = column.ToString("");
                            }
                        }
                        else
                        {
                            string refCol = column.ReferenceColumnName;
                            if (string.IsNullOrEmpty(refCol))
                                refCol = column.Name;
                            Table.Column masterColumn;
                            if (masterTable.Columns.TryGetColumnByName(refCol, out masterColumn))
                            {
                                value = masterColumn.Value;
                            }
                        }

                        if (string.IsNullOrEmpty(value)) value = " ";

                        fields += "," + column.Name;
                        values += "," + value;

                        if (column.UpdateMasterTable)
                        {
                            masterFields += "," + column.Name;
                            masterValues += "," + value;
                        }
                    }

                    if (!string.IsNullOrEmpty(fields)) fields = fields.Substring(1);
                    if (!string.IsNullOrEmpty(values)) values = values.Substring(1);

                    myDB.Writer.CallServer("IRT", fullTableName, fields, values, "", "1");
                }
                
                if ((!string.IsNullOrEmpty(masterFields)) && (!string.IsNullOrEmpty(masterValues)))
                {
                    string fullTableName = masterTable.TableName + "TAB";

                    masterFields = masterFields.Substring(1);
                    masterValues = masterValues.Substring(1);

                    Table.Column masterColumn;
                    if (masterTable.Columns.TryGetColumnByName("URNO", out masterColumn))
                    {
                        myDB.Writer.CallServer("URT", fullTableName, masterFields, masterValues,
                                "WHERE URNO = " + masterColumn.ToString("'"), "1");
                    }
                }

                success = true;
            }

            return success;
        }

        private void AddMasterTableColumns(Table masterTable, string urno, string fieldAndValueList)
        {
            Table.Column masterColumn;
            if (masterTable.Columns.TryGetColumnByName("URNO", out masterColumn))
            {
                masterColumn.Value = urno;
            }
            else
            {
                masterTable.Columns.Add(new Table.Column("URNO", urno));
            }

            string[] fieldsAndValues = fieldAndValueList.Split(this.Separators[0]);
            if (fieldsAndValues.Length == 2)
            {
                string[] fields = fieldsAndValues[0].Split(this.Separators[1]);
                string[] values = fieldsAndValues[1].Split(this.Separators[1]);

                int fieldCount = fields.Length;
                if (fieldCount == values.Length)
                {
                    for (int i = 0; i < fieldCount; i++)
                    {
                        if (masterTable.Columns.TryGetColumnByName(fields[i], out masterColumn))
                        {
                            masterColumn.Value = values[i];
                        }
                        else
                        {
                            masterTable.Columns.Add(new Table.Column(fields[i], values[i]));
                        }
                    }
                }
            }            
        }

        private string EvaluateExpression(string expression, Table.ColumnCollection columns)
        {
            string result = expression;

            foreach (Table.Column column in columns )
            {
                result = result.Replace("[" + column.Name + "]",
                    column.ToString());
            }

            return result;
        }
    }
}
