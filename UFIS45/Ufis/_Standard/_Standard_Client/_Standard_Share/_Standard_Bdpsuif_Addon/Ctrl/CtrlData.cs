﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

using Ufis.Data;
using Ufis.Utils;

namespace _Standard_Bdpsuif_Addon.Ctrl
{
    public class CtrlData
    {
        public IDatabase MyDB { get; set; }

        #region Singleton

        private CtrlData()
        {
            IniFile myIni = CtrlConfig.GetInstance().MyIni;
            String strServer = string.Empty;
            String strHopo = string.Empty;
            String strTableExt = string.Empty;

            strServer = myIni.IniReadValue("GLOBAL", "HOSTNAME");
            strHopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
            strTableExt = myIni.IniReadValue("GLOBAL", "TABLEEXTENSION");

            UT.ServerName = strServer;
            UT.Hopo = strHopo;

            if (UT.UserName.Length == 0)
                UT.UserName = Program.UserName;

            MyDB = UT.GetMemDB();
            MyDB.IndexUpdateImmediately = true;
            MyDB.Connect(UT.ServerName, UT.UserName, UT.Hopo, strTableExt, "BDPS-UIF");
        }

        private static CtrlData _this = null;
        public static CtrlData GetInstance()
        {
            if (_this == null)
            {
                _this = new CtrlData();
            }
            return _this;
        }
        #endregion
    }
}
