﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace _Standard_Bdpsuif_Addon.Ctrl
{
    public class CtrlConverter
    {
        private static string clientPatchList =
            BuildBinaryString(new int[] { 34, 39, 44, 10, 13, 40, 41 });
        private static string newServerPatchList =
            BuildBinaryString(new int[] { 23, 24, 25, 28, 29, 40, 41 });
        private static string oldServerPatchList =
            //BuildBinaryString(new int[] { 176, 177, 178, 179, 180, 155, 156 });
            BuildBinaryString(new int[] { 176, 177, 178, 179, 180, 8250, 339 });

        public enum ConvertMethodEnum
        {
            FOR_CLIENT,
            FOR_SERVER,
            SERVER_TO_CLIENT,
            CLIENT_TO_SERVER
        }

        private static string BuildBinaryString(int[] myAscList)
        {
            string result = string.Empty;

            foreach (int myAsc in myAscList)
            {
                result += (char)myAsc;
            }

            return result;
        }

        private static string ReplaceChars(string text, string oldValue, string newValue)
        {
            string result = text;
            int i = 0;

            foreach (char oldChar in oldValue)
            {
                char newChar = newValue[i];
                result = result.Replace(oldChar, newChar);

                i++;
            }

            return result;
        }

        public static string ConvertString(string text, ConvertMethodEnum method, bool checkOldBehaviour)
        {
            string result = text;

            if (method == ConvertMethodEnum.FOR_SERVER)
            {
                result = result.Replace("\r\n", "\n");
            }

            switch (method)
            {
                case ConvertMethodEnum.FOR_CLIENT:
                case ConvertMethodEnum.SERVER_TO_CLIENT:
                    result = ReplaceChars(text, newServerPatchList, clientPatchList);
                    if (result.Equals(text) && checkOldBehaviour)
                    {//Nothing changed, so we try the old version
                        result = ReplaceChars(text, oldServerPatchList, clientPatchList);
                    }
                    break;
                case ConvertMethodEnum.FOR_SERVER:
                case ConvertMethodEnum.CLIENT_TO_SERVER:
                    if (checkOldBehaviour)
                    {
                        result = ReplaceChars(result, clientPatchList, oldServerPatchList);
                    }
                    else
                    {
                        result = ReplaceChars(result, clientPatchList, newServerPatchList);
                    }
                    break;
            }

            if (method == ConvertMethodEnum.FOR_CLIENT)
            {
                result = result.Replace("\n", "\r\n");
            }

            return result;
        }

        public static string ConvertStringForClient(string text)
        {
            return ConvertString(text, ConvertMethodEnum.SERVER_TO_CLIENT, true);
        }

        public static string ConvertStringForServer(string text)
        {
            return ConvertString(text, ConvertMethodEnum.CLIENT_TO_SERVER, false);
        }

        public static string ConvertStringForServer(string text, bool useOldCharset)
        {
            return ConvertString(text, ConvertMethodEnum.CLIENT_TO_SERVER, useOldCharset);
        }
    }
}
