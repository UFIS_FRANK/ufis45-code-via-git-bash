﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;

using _Standard_Bdpsuif_Addon.UI;
using _Standard_Bdpsuif_Addon.UserControl;

using _Standard_Bdpsuif_Addon.Ctrl;

namespace _Standard_Bdpsuif_Addon
{
    public partial class frmMain : Form
    {

        
        public bool IsValidLogin(string userName, string password)
        {
            loginControl.ApplicationName = "BDPS-UIF";
            //loginControl.VersionString = Application.ProductVersion;
            //loginControl.InfoCaption = "BDPS-UIF";
            //loginControl.InfoButtonVisible = true;
            //loginControl.InfoUfisVersion = "Ufis Version 4.5";
            //loginControl.InfoAppVersion = "BDPS-UIF " + Application.ProductVersion.ToString();
            //loginControl.InfoAAT = "UFIS Airport Solutions GmbH";
            //loginControl.UserNameLCase = true;
            //string tmpRegStrg = "BDPS-UIF" + ",";
            //tmpRegStrg += "InitModu,InitModu,Initialisieren (InitModu),B,-";
            //loginControl.RegisterApplicationString = tmpRegStrg;


            bool result = false;
            string strRet = "";
            strRet = loginControl.DoLoginSilentMode(userName, password);
            if (strRet == "OK")
            {
                result = true;
            }
            else
            {
                //strRet = LoginControl.ShowLoginDialog();
                MessageBox.Show("Invalid credential!");
                result = false;
            }
            return result;
        }

        //#endregion


        public frmMain()
        {
            
            InitializeComponent();
            //LoadImageCombo();

        }

        private void LoadImageCombo()
        {
            Dictionary<string, string> imgKeyPath = new Dictionary<string, string>();
            imgKeyPath.Add("1", @"D:\Ufis\System\notvalid1.bmp");
            imgKeyPath.Add("2", @"D:\Ufis\System\notvalid2.bmp");
            imgKeyPath.Add("3", @"D:\Ufis\System\notvalid3.bmp");

            ImageComboBox imgCombo = new ImageComboBox(imgKeyPath, new Size(56, 16));

            Point comboPoint = new Point(511, 26);
            //imgCombo.Location = comboPoint;
            //panel1.Controls.Add(imgCombo);
            //this.Controls.Add(imgCombo);
            
        }

        private void btnBlockPst_Click(object sender, EventArgs e)
        {
            frmBlockParkingStand blockPst = new frmBlockParkingStand();
            blockPst.ShowDialog();

        }

        private void btnCon_Click(object sender, EventArgs e)
        {
            frmContinentList dlgCon = new frmContinentList();
            dlgCon.ShowDialog();
        }

       
    }
}
