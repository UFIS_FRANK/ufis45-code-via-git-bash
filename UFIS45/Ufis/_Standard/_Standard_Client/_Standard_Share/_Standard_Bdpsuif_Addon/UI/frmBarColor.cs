﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using _Standard_Bdpsuif_Addon.Ctrl;
using Ufis.Data;
using Ufis.Utils;

namespace _Standard_Bdpsuif_Addon.UI
{
    public partial class frmBarColor : Form
    {
        private string appType;
        private bool isReadOnly = false;
        private DataTable dataTable;
        private DataView dataView;
        private IDatabase myDB;
        private ITable myNAT;
        private ITable myVCD;
        private ColorDialog colorDialog;
        private Int32 redValue;
        private Int32 greenValue;
        private Int32 blueValue;
        private string hexValue;

        public frmBarColor(string appType, string permission)
        {
            this.appType = appType;

            if (permission == "0" || permission == "1")
            {
                isReadOnly = (permission == "0");

                InitializeComponent();
            }
        }

        private void frmBarColor_Load(object sender, EventArgs e)
        {
            myDB = UT.GetMemDB();
            PrepareDataTable();
            PrepareDataSource(UT.UserName);
            PrepareLayout(dgvData);
            PrepareColorLayout(dgvData);
            ReadRegistry();
            this.BringToFront();
            this.TopMost = true;
            this.TopMost = false;
        }

        private void PrepareDataTable()
        {
            dataTable = new DataTable();
            DataColumn colURNO = new DataColumn("URNO");
            colURNO.MaxLength = 10;
            colURNO.Caption = "URNO";
            dataTable.Columns.Add(colURNO);
            DataColumn colNCode = new DataColumn("TTYP");
            colNCode.MaxLength = 15;
            colNCode.Caption = "Nature Code";
            dataTable.Columns.Add(colNCode);
            DataColumn colDesc = new DataColumn("TNAM");
            colDesc.MaxLength = 30;
            colDesc.Caption = "Desc";
            dataTable.Columns.Add(colDesc);
            DataColumn colBarColor = new DataColumn("BCL");
            colBarColor.MaxLength = 15;
            colBarColor.Caption = "Bar Color";
            dataTable.Columns.Add(colBarColor);
            DataColumn colTextColor = new DataColumn("TCL");
            colTextColor.MaxLength = 15;
            colTextColor.Caption = "Text Color";
            dataTable.Columns.Add(colTextColor);
            DataColumn colPreview = new DataColumn("TEXT");
            colPreview.MaxLength = 15;
            colPreview.Caption = "Preview";
            dataTable.Columns.Add(colPreview);
            DataColumn colBarColorCode = new DataColumn("BCLC");
            colBarColorCode.MaxLength = 6;
            colBarColorCode.Caption = "Bar Color";
            dataTable.Columns.Add(colBarColorCode);
            DataColumn colTextColorCode = new DataColumn("TCLC");
            colTextColorCode.MaxLength = 6;
            colTextColorCode.Caption = "Text Color";
            dataTable.Columns.Add(colTextColorCode);
            DataColumn colPreviewCode = new DataColumn("TEXTC");
            colPreviewCode.MaxLength = 13;
            colPreviewCode.Caption = "Preview";
            dataTable.Columns.Add(colPreviewCode);
        }


        private void PrepareDataSource(string userDefault)
        {
            string whereString = "WHERE APPN = 'CCS_FPMS' AND CTYP LIKE 'BAR%%' AND (PKNO = " + "'" + userDefault + "' OR CKEY = '#__DEFAULT__#')";

            myDB.Unbind("VCD");
            myDB.Unbind("NAT");

            myVCD = myDB.Bind("VCD", "VCD", "URNO,APPN,CKEY,CTYP,HOPO,PKNO,TEXT", "10,32,32,32,3,32,30", "URNO,APPN,CKEY,CTYP,HOPO,PKNO,TEXT");
            myVCD.CreateIndex("URNO", "URNO");
            myVCD.CreateIndex("CKEY", "CKEY");

            myVCD.Clear();
            myVCD.Load(whereString);

            myNAT = myDB.Bind("NAT", "NAT", "TTYP,HOPO,TNAM", "5,3,30", "TTYP,HOPO,TNAM");
            myNAT.Clear();
            myNAT.Load("WHERE HOPO = '" + UT.Hopo + "'");
            if (myVCD.Count >= 0 && myNAT.Count > 0)
            {
                IRow[] rows = myVCD.RowsByIndexValue("CKEY", "#__DEFAULT__#");

                if (rows.Length > 0)
                {
                    txtBarColor.BackColor = ColorTranslator.FromHtml("#" + rows[0]["TEXT"].Split('#')[0]);
                    txtTextColor.BackColor = ColorTranslator.FromHtml("#" + rows[0]["TEXT"].Split('#')[1]);
                    DataRow row = dataTable.NewRow();
                    row["TTYP"] = "#__DEFAULT__#";
                    row["TNAM"] = "#__DEFAULT__#";

                    row["BCLC"] = rows[0]["TEXT"].Split('#')[0];
                    row["TCLC"] = rows[0]["TEXT"].Split('#')[1];
                    row["TEXTC"] = rows[0]["TEXT"];
                    txtBarColor.BackColor = ColorTranslator.FromHtml("#" + rows[0]["TEXT"].Split('#')[0]);
                    txtTextColor.BackColor = ColorTranslator.FromHtml("#" + rows[0]["TEXT"].Split('#')[1]);
                    dataTable.Rows.Add(row);
                }
                else
                {
                    txtBarColor.BackColor = ColorTranslator.FromHtml("#40E0D0");
                    txtTextColor.BackColor = ColorTranslator.FromHtml("#000000");

                    DataRow row = dataTable.NewRow();
                    row["TTYP"] = "#__DEFAULT__#";
                    row["TNAM"] = "#__DEFAULT__#";
                    row["BCLC"] = "40E0D0";
                    row["TCLC"] = "000000";
                    row["TEXTC"] = "40E0D0#000000";
                    dataTable.Rows.Add(row);
                }

                
                if (myVCD.Count <= 1)
                {
                    if (userDefault == "#__DEFAULT__#")
                        MessageBox.Show("Default record not found!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("User specific record not found!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    for (int i = 0; i < myNAT.Count; i++)
                    {
                        DataRow row = dataTable.NewRow();
                        row["TTYP"] = myNAT[i]["TTYP"];
                        row["TNAM"] = myNAT[i]["TNAM"];

                        row["BCL"] = "not defined";
                        row["TCL"] = "not defined";
                        row["TEXT"] = "not defined";

                        dataTable.Rows.Add(row);
                    }
                }
                else
                {
                    for (int i = 0; i < myNAT.Count; i++)
                    {
                        DataRow row = dataTable.NewRow();

                        row["TTYP"] = myNAT[i]["TTYP"];
                        row["TNAM"] = myNAT[i]["TNAM"];
                        IRow[] rowvcd = myVCD.RowsByIndexValue("CKEY", myNAT[i]["TTYP"].ToString());
                        if (rows.Length > 0)
                        {
                            row["TEXT"] = rowvcd[0]["TEXT"];
                            row["BCLC"] = rowvcd[0]["TEXT"].Split('#')[0];
                            row["TCLC"] = rowvcd[0]["TEXT"].Split('#')[1];
                            row["TEXTC"] = rowvcd[0]["TEXT"];
                        }
                        else
                        {
                            row["BCL"] = "not defined";
                            row["TCL"] = "not defined";
                            row["TEXT"] = "not defined";
                        }
                        dataTable.Rows.Add(row);
                    }
                }

                dataTable.AcceptChanges();
            }
            
            else if (myNAT.Count == 0)
            {
                MessageBox.Show("No record found!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void PrepareLayout(DataGridView dataGridView)
        {
            ////add handler
            //dataTable.RowChanged += new DataRowChangeEventHandler(dataTable_RowChanged);

            dataView = new DataView(dataTable);

            dataView.RowFilter = string.Format("{0} <> '#__DEFAULT__#'", "TTYP");

            dataGridView.AutoGenerateColumns = false;
            dataGridView.DataSource = dataView;
            dataGridView.ReadOnly = isReadOnly;
            dataGridView.AllowUserToAddRows = isReadOnly;
            btnOK.Visible = !isReadOnly;

            DataGridViewColumn dataGridViewColumn;

            foreach (DataColumn temp in dataTable.Columns)
            {
                DataGridViewCellStyle contentCellStyle = new DataGridViewCellStyle();
                DataGridViewTextBoxColumn dgvTextBoxColumn = new DataGridViewTextBoxColumn();
                dataGridView.Columns.Add(dgvTextBoxColumn);
                dataGridViewColumn = dataGridView.Columns[dgvTextBoxColumn.Index];

                dataGridViewColumn.DataPropertyName = temp.ColumnName;
                dataGridViewColumn.Name = temp.ColumnName;
                dataGridViewColumn.HeaderText = temp.Caption;

                if (temp.ColumnName == "TTYP" || temp.ColumnName == "BCL" || temp.ColumnName == "TCL")
                {
                    dataGridViewColumn.Width = 100;
                }
                else
                    dataGridViewColumn.Width = 175;

                if (temp.ColumnName == "TTYP" || temp.ColumnName == "TNAM" || temp.ColumnName == "TEXT")
                {
                    contentCellStyle.BackColor = System.Drawing.SystemColors.ButtonFace;
                    dataGridViewColumn.ReadOnly = !isReadOnly;
                }

                if (temp.ColumnName == "URNO" || temp.ColumnName == "BCLC" || temp.ColumnName == "TCLC" || temp.ColumnName == "TEXTC")
                {
                    dataGridViewColumn.Visible = isReadOnly;
                }

                dataGridViewColumn.DefaultCellStyle.BackColor = contentCellStyle.BackColor;

            }
        }

        private void PrepareColorLayout(DataGridView dataGridView)
        {
            for (Int32 i = 0; i < dataView.Count; i++)
            {
                if (dgvData["BCL", i].Value != "not defined")
                    dgvData["BCL", i].Style.BackColor = ColorTranslator.FromHtml("#" + dgvData["BCLC", i].Value);
                if (dgvData["TCL", i].Value != "not defined")
                    dgvData["TCL", i].Style.BackColor = ColorTranslator.FromHtml("#" + dgvData["TCLC", i].Value);
                if (dgvData["TEXT", i].Value.ToString() != "not defined")
                {
                    dgvData["TEXT", i].Style.BackColor = ColorTranslator.FromHtml("#" + dgvData["TEXTC", i].Value.ToString().Split('#')[0]);
                    dgvData["TEXT", i].Style.ForeColor = ColorTranslator.FromHtml("#" + dgvData["TEXTC", i].Value.ToString().Split('#')[1]);
                }
            }
        }


        private void txtBarColor_Click(object sender, EventArgs e)
        {
            this.colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                txtBarColor.BackColor = colorDialog.Color;
            }

        }

        private void txtTextColor_Click(object sender, EventArgs e)
        {
            this.colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                txtTextColor.BackColor = colorDialog.Color;
            }
        }


        private void btnDefault_Click(object sender, EventArgs e)
        {
            //int rowIndex = 0;

            //foreach (DataRowView dvRow in dataView)
            //{
            //    dvRow.Row.ClearErrors();

            //    redValue = txtBarColor.BackColor.R;
            //    greenValue = txtBarColor.BackColor.G;
            //    blueValue = txtBarColor.BackColor.B;

            //    dgvData["BCLC", rowIndex].Value = String.Format("{0:X}", redValue).PadLeft(2, '0') + String.Format("{0:X}", greenValue).PadLeft(2, '0') + String.Format("{0:X}", blueValue).PadLeft(2, '0');
            //    dgvData["BCL", rowIndex].Value = string.Empty;
            //    dgvData["BCL", rowIndex].Style.BackColor = ColorTranslator.FromHtml("#" + dgvData["BCLC", rowIndex].Value);

            //    redValue = txtTextColor.BackColor.R;
            //    greenValue = txtTextColor.BackColor.G;
            //    blueValue = txtTextColor.BackColor.B;

            //    dgvData["TCLC", rowIndex].Value = String.Format("{0:X}", redValue).PadLeft(2, '0') + String.Format("{0:X}", greenValue).PadLeft(2, '0') + String.Format("{0:X}", blueValue).PadLeft(2, '0');
            //    dgvData["TCL", rowIndex].Value = string.Empty;
            //    dgvData["TCL", rowIndex].Style.BackColor = ColorTranslator.FromHtml("#" + dgvData["TCLC", rowIndex].Value);

            //    dgvData["TEXTC", rowIndex].Value = dgvData["BCLC", rowIndex].Value + "#" + dgvData["TCLC", rowIndex].Value;
            //    dgvData["TEXT", rowIndex].Value = dgvData["BCLC", rowIndex].Value + "#" + dgvData["TCLC", rowIndex].Value;
            //    dgvData["TEXT", rowIndex].Style.BackColor = ColorTranslator.FromHtml("#" + dgvData["TEXTC", rowIndex].Value.ToString().Split('#')[0]);
            //    dgvData["TEXT", rowIndex].Style.ForeColor = ColorTranslator.FromHtml("#" + dgvData["TEXTC", rowIndex].Value.ToString().Split('#')[1]);
            //    rowIndex++;
            //}

            int rowIndex = 0;

            for (Int32 i = 1; i < dataTable.Rows.Count; i++)
            {
                dataTable.Rows[i].ClearErrors();

                redValue = txtBarColor.BackColor.R;
                greenValue = txtBarColor.BackColor.G;
                blueValue = txtBarColor.BackColor.B;

                dataTable.Rows[i]["BCLC"] = String.Format("{0:X}", redValue).PadLeft(2, '0') + String.Format("{0:X}", greenValue).PadLeft(2, '0') + String.Format("{0:X}", blueValue).PadLeft(2, '0');
                dataTable.Rows[i]["BCL"] = string.Empty;
                dgvData["BCL", rowIndex].Style.BackColor = ColorTranslator.FromHtml("#" + dataTable.Rows[i]["BCLC"]);

                redValue = txtTextColor.BackColor.R;
                greenValue = txtTextColor.BackColor.G;
                blueValue = txtTextColor.BackColor.B;

                dataTable.Rows[i]["TCLC"] = String.Format("{0:X}", redValue).PadLeft(2, '0') + String.Format("{0:X}", greenValue).PadLeft(2, '0') + String.Format("{0:X}", blueValue).PadLeft(2, '0');
                dataTable.Rows[i]["TCL"] = string.Empty;
                dgvData["TCL", rowIndex].Style.BackColor = ColorTranslator.FromHtml("#" + dataTable.Rows[i]["TCLC"]);

                dataTable.Rows[i]["TEXTC"] = dgvData["BCLC", rowIndex].Value + "#" + dgvData["TCLC", rowIndex].Value;
                dgvData["TEXT", rowIndex].Value = dgvData["BCLC", rowIndex].Value + "#" + dgvData["TCLC", rowIndex].Value;
                dgvData["TEXT", rowIndex].Style.BackColor = ColorTranslator.FromHtml("#" + dataTable.Rows[i]["TEXTC"].ToString().Split('#')[0]);
                dgvData["TEXT", rowIndex].Style.ForeColor = ColorTranslator.FromHtml("#" + dataTable.Rows[i]["TEXTC"].ToString().Split('#')[1]);
                rowIndex++;
            }
        }

        private void dgvData_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == 3 || e.ColumnIndex == 4)
            {
                this.colorDialog = new ColorDialog();
                if (colorDialog.ShowDialog() == DialogResult.OK)
                {
                    redValue = colorDialog.Color.R;
                    greenValue = colorDialog.Color.G;
                    blueValue = colorDialog.Color.B;
                    dgvData[e.ColumnIndex, e.RowIndex].Style.BackColor = colorDialog.Color;

                    if (e.ColumnIndex == 3)
                    {
                        dgvData["BCL", e.RowIndex].Value = string.Empty;
                        dgvData["BCLC", e.RowIndex].Value = String.Format("{0:X}", redValue).PadLeft(2, '0') + String.Format("{0:X}", greenValue).PadLeft(2, '0') + String.Format("{0:X}", blueValue).PadLeft(2, '0');
                    }
                    else
                    {
                        dgvData["TCL", e.RowIndex].Value = string.Empty;
                        dgvData["TCLC", e.RowIndex].Value = String.Format("{0:X}", redValue).PadLeft(2, '0') + String.Format("{0:X}", greenValue).PadLeft(2, '0') + String.Format("{0:X}", blueValue).PadLeft(2, '0');
                    }

                    if (dgvData["TCL", e.RowIndex].Value != "not defined" && dgvData["BCL", e.RowIndex].Value != "not defined")
                    {
                        dgvData["TEXTC", e.RowIndex].Value = dgvData["BCLC", e.RowIndex].Value + "#" + dgvData["TCLC", e.RowIndex].Value;
                        dgvData["TEXT", e.RowIndex].Value = dgvData["BCLC", e.RowIndex].Value + "#" + dgvData["TCLC", e.RowIndex].Value;
                    }


                    dgvData["TEXT", e.RowIndex].Style.BackColor = dgvData["BCL", e.RowIndex].Style.BackColor;
                    dgvData["TEXT", e.RowIndex].Style.ForeColor = dgvData["TCL", e.RowIndex].Style.BackColor;

                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string errorMsg = string.Empty;
            if (ValidateRecords("#__DEFAULT__#"))
            {
                if (SaveRecords(dataTable, "#__DEFAULT__#"))
                {
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Failed to save to database!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                this.Close();
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            dataTable.Rows.Clear();
            PrepareDataSource("#__DEFAULT__#");
            PrepareColorLayout(dgvData);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string errorMsg = string.Empty;
            if (ValidateRecords(UT.UserName))
            {
                if (SaveRecords(dataTable, UT.UserName))
                {
                    CtrlUtil ctl = new CtrlUtil();
                    ctl.SendReloadMsgToFipsApp();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Failed to save to database!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DataView dvChanges = new DataView(dataTable, "", "", DataViewRowState.Added | DataViewRowState.ModifiedCurrent);

            if (dvChanges.Count > 0)
            {
                DialogResult result = MessageBox.Show(
                    "Do you want to save the changes?", this.Text,
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1);
                switch (result)
                {
                    case DialogResult.Yes:
                        btnOK_Click(sender, e);
                        break;
                    case DialogResult.No:
                        this.Close();
                        break;
                }
            }
            else
            {
                this.Close();
            }
        }

        private bool ValidateRecords(string userDefault)
        {
            bool result = true;

            string primaryKey = "URNO";
            string whereString = "WHERE APPN = 'CCS_FPMS' AND CTYP LIKE 'BAR%%' AND (PKNO = " + "'" + userDefault + "' OR CKEY = '#__DEFAULT__#')";
            try
            {
                myVCD.Clear();
                myVCD.Load(whereString);

                foreach (DataRow dataRow in dataTable.Rows)
                {
                    
                    if (dataRow["TTYP"] == "#__DEFAULT__#")
                    {
                        IRow[] row = myVCD.RowsByIndexValue("CKEY", "#__DEFAULT__#");

                        redValue = txtBarColor.BackColor.R;
                        greenValue = txtBarColor.BackColor.G;
                        blueValue = txtBarColor.BackColor.B;

                        dataRow["BCLC"] = String.Format("{0:X}", redValue).PadLeft(2, '0') + String.Format("{0:X}", greenValue).PadLeft(2, '0') + String.Format("{0:X}", blueValue).PadLeft(2, '0');


                        redValue = txtTextColor.BackColor.R;
                        greenValue = txtTextColor.BackColor.G;
                        blueValue = txtTextColor.BackColor.B;

                        dataRow["TCLC"] = String.Format("{0:X}", redValue).PadLeft(2, '0') + String.Format("{0:X}", greenValue).PadLeft(2, '0') + String.Format("{0:X}", blueValue).PadLeft(2, '0');

                        dataRow["TEXTC"] = dataRow["BCLC"].ToString() + "#" + dataRow["TCLC"].ToString();

                        if (row.Length > 0)
                        {
                            dataRow["URNO"] = row[0]["URNO"];

                        }
                        else
                        {
                            dataRow[primaryKey] = DBNull.Value;
                        }
                    }
                    else
                    {
                        IRow[] rows = myVCD.RowsByIndexValue("CKEY", dataRow["TTYP"].ToString());
                        if (rows.Length > 0)
                        {
                            dataRow["URNO"] = rows[0]["URNO"];
                        }
                        else
                        {
                            dataRow[primaryKey] = DBNull.Value;
                        }
                    }


                    dataRow.ClearErrors();

                    if (dataRow["BCL"].ToString() == "not defined")
                    {
                        dataRow.SetColumnError("BCL", "Bar Color not defined!");
                        result = false;
                    }

                    if (dataRow["TCL"].ToString() == "not defined")
                    {
                        dataRow.SetColumnError("TCL", "Text Color not defined!");
                        result = false;
                    }

                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        private bool SaveRecords(DataTable dataTable, string userDefault)
        {
            bool result = false;
            string primaryKey = "URNO";
            try
            {
                //save detail table
                if (myVCD.UniqueFields != null)
                {
                    if (myVCD.UniqueFields.Count == 1)
                    {
                        primaryKey = myVCD.UniqueFields[0].ToString();
                    }
                }


                foreach (DataRow dataRow in dataTable.Rows)
                {
                    if (dataRow[primaryKey] == DBNull.Value)
                    {
                        IRow iRow = myVCD.CreateEmptyRow();

                        if (!SaveRow(dataRow, iRow, State.Created, userDefault))
                        {
                            return result;
                        }
                        myVCD.Add(iRow);
                    }
                    else
                    {
                        if (dataRow.RowState == DataRowState.Modified)
                        {
                            IRow[] iRows = myVCD.RowsByIndexValue(primaryKey, dataRow[primaryKey].ToString());
                            if (iRows != null && iRows.Length > 0)
                            {
                                IRow iRow = iRows[0];

                                if (!SaveRow(dataRow, iRow, State.Modified, userDefault))
                                {
                                    return result;
                                }
                            }
                        }
                    }
                }

                myVCD.Save();
                dataTable.AcceptChanges();
                result = true;
            }
            catch
            {
                result = false;
            }

            return result;
        }

        private bool SaveRow(DataRow dataRow, IRow iRow, State state, string userDefault)
        {
            bool result = true;

            try
            {
                DataColumn dataColumn = dataRow.Table.Columns["URNO"];
                if (dataColumn.ColumnName == "URNO")
                {
                    if (state == State.Created)
                    {
                        dataRow[dataColumn] = iRow["URNO"];
                    }
                }

                iRow["APPN"] = "CCS_FPMS";
                iRow["CKEY"] = dataRow["TTYP"].ToString();
                iRow["CTYP"] = "BARCOLOR";
                iRow["HOPO"] = UT.Hopo;
                iRow["PKNO"] = userDefault;
                iRow["TEXT"] = dataRow["TEXTC"].ToString();

                iRow.Status = state;
            }
            catch
            {
                result = false;
            }

            return result;
        }

        private void ReadRegistry()
        {
            int myX = -1, myY = -1, myW = -1, myH = -1;
            FormWindowState windowState = FormWindowState.Normal;
            string regVal = "";
            string theKey = "Software\\BDPSUIF_Addon\\" + appType;
            RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
            if (rk != null)
            {
                regVal = rk.GetValue("X", "-1").ToString();
                myX = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Y", "-1").ToString();
                myY = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Width", "-1").ToString();
                myW = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Height", "-1").ToString();
                myH = Convert.ToInt32(regVal);
                try
                {
                    windowState = (FormWindowState)Enum.Parse(typeof(FormWindowState),
                        rk.GetValue("WindowState", "Normal").ToString());
                }
                catch
                {
                    windowState = FormWindowState.Normal;
                }
            }

            //save current screens width and height
            int screenWidth = 0, screenHeight = 0;
            foreach (Screen screen in Screen.AllScreens)
            {
                screenWidth += screen.Bounds.Width;
                screenHeight += screen.Bounds.Height;
            }

            //left and top
            if (myX == -1) myX = this.Left;
            if (myY == -1) myY = this.Top;

            //check for width
            if (myW != -1)
            {
                if (myW > screenWidth)
                {
                    myX = this.Left;
                    myW = this.Width;
                }
                else
                {
                    if (myX + myW > screenWidth)
                    {
                        myX = screenWidth - myW;
                    }
                }
            }
            else
            {
                myW = this.Width;
            }

            //check for height
            if (myH != -1)
            {
                if (myH > screenHeight)
                {
                    myH = this.Height;
                }
                else
                {
                    if (myY + myH > screenHeight)
                    {
                        myY = screenHeight - myH;
                    }
                }
            }
            else
            {
                myH = this.Height;
            }

            this.Left = myX;
            this.Top = myY;
            this.Width = myW;
            this.Height = myH;

            this.WindowState = windowState;
        }

        private void dgvData_Sorted(object sender, EventArgs e)
        {
            PrepareColorLayout(dgvData);
        }

        private void SaveToRegistry()
        {
            string theKey = "Software\\BDPSUIF_Addon\\" + appType;
            RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
            if (rk == null)
            {//If No registry key then create it
                rk = Registry.CurrentUser.OpenSubKey("Software\\BDPSUIF_Addon", true);
                if (rk == null)
                {
                    rk = Registry.CurrentUser.OpenSubKey("Software", true);
                    rk.CreateSubKey("BDPSUIF_Addon");
                    rk.Close();
                    rk = Registry.CurrentUser.OpenSubKey("Software\\BDPSUIF_Addon", true);
                }
                rk.CreateSubKey(appType);
                rk.Close();
                rk = Registry.CurrentUser.OpenSubKey(theKey, true);
            }

            if (this.WindowState == FormWindowState.Normal)
            {
                rk.SetValue("X", this.Left.ToString());
                rk.SetValue("Y", this.Top.ToString());
                rk.SetValue("Width", this.Width.ToString());
                rk.SetValue("Height", this.Height.ToString());
            }
            rk.SetValue("WindowState", this.WindowState);
        }

        private void frmBarColor_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveToRegistry();
        }
    }
}
