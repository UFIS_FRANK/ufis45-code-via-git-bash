﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;
using System.Globalization;

using Ufis.Data;
using Ufis.Utils;

using _Standard_Bdpsuif_Addon.Ctrl;
using _Standard_Bdpsuif_Addon.Helper;
using _Standard_Bdpsuif_Addon.UserControl;


namespace _Standard_Bdpsuif_Addon.UI
{
    public partial class frmBlockParkingStand : Form
    {
        ImageComboBox imgCombo;

        private static string pstDbColName = "URNO,PNAM";
        private static string pstDbColLgName = pstDbColName;
        private static string pstDbColLength = "10,5";

        private static string blkDbColName = "URNO,BURN,DAYS,NAFR,NATO,RESN,TABN,TIFR,TITO,TYPE,IBIT,UMBL";
        private static string blkDbColLgName = blkDbColName;
        private static string blkDbColLength = "10,10,7,14,14,40,3,4,4,1,3,10";

        private static string mblDbColName = "URNO,MNAM,DAYS,NAFR,NATO,RESN,TIFR,TITO,IBIT,UPST";
        private static string mblDbColLgName = mblDbColName;
        private static string mblDbColLength = "10,25,7,14,14,40,4,4,3,1024";

        public string UserName { get; set; }

        private DateTime _timeFrom;
        public DateTime TimeFrom
        {
            get
            {
                return _timeFrom;
            }
            set
            {
                _timeFrom = value;
            }
        }

        private DateTime _timeTo;
        public DateTime TimeTo
        {
            get
            {
                return _timeTo;
            }
            set
            {
                _timeTo = value;
            }
        }

        private DateTime _vFrom;
        public DateTime VFrom 
        {
            get
            {
                return _vFrom;
            }
            set
            {
                _vFrom = value;
            } 
        }

        private DateTime _vTo;
        public DateTime VTo 
        {
            get 
            {
                return _vTo; 
            }
            set
            {
                _vTo = value;
            } 
        }

        public string Days { get; set; }

        private IniFile myIni = null;
        private IDatabase myDB = null;
        private ITable pstTab = null;
        private ITable blkTab = null;
        private ITable mblTab = null; //igu on 22/06/2011

        private List<string> mblNames = new List<string>();
        private char urnoListSep = ',';

        private bool clearFormOnNewRecord = true; //igu on 15/07/2011

        public frmBlockParkingStand()
        {
            InitializeComponent();

            switch(Program.Permit)
            {
                case "1":
                    this.Enabled = true;
                    break;
                case "0":
                    this.Enabled = false;
                    break;
                case "-":
                    return;
            }

            myIni = CtrlConfig.GetInstance().MyIni;
            myDB = CtrlData.GetInstance().MyDB;
            UserName = Program.UserName;

            LoadImageCombo();            
            LoadData();

            //if (!InitUtils())
            //{
            //    MessageBox.Show("Error connecting!");
            //}           

        }

        private void LoadImageCombo()
        {
            int imgCount = 0;
            int.TryParse(myIni.IniReadValue("GATPOS", "BlockedBitmapCount"), out imgCount);

            Dictionary<string, string> imgKeyPath = new Dictionary<string, string>();
            string path = string.Empty;

            for (int i = 0; i < imgCount; i++)
            {
                path = myIni.IniReadValue("GATPOS", "BlockedBitmap" + (i+1).ToString());
                imgKeyPath.Add(i.ToString(),path);
            }
            imgCombo = new ImageComboBox(imgKeyPath, new Size(50, 20));
            pnlImageCombo.Controls.Add(imgCombo);
            //this.Controls.Add(imgCombo);

        }

        private void LoadData()
        {
            PST item ;
            pstTab = myDB.Bind("PST", "PST", pstDbColLgName, pstDbColLength, pstDbColName);
            pstTab.Load("ORDER BY PNAM");
            pstTab.CreateIndex("URNO", "URNO");

            for (int i = 0; i < pstTab.Count; i++)
			{
                IRow row = pstTab[i];
                item = new PST(row["PNAM"], row["URNO"]);
                lsLeft.Items.Add(item);
            }

            //----
            //igu on 15/07/2011
            //populate ALL the records of MBLTAB
            /*
            //igu on 21/06/2011
            //read the current valid multi-block and populate it into combobox
            DateTime now = UT.LocalToUtc(DateTime.Now);
            string strWhere = "WHERE (NATO >= {0} OR NATO = ' ' OR NATO IS NULL) " + 
                "ORDER BY MNAM";
            strWhere = string.Format(strWhere, "'" + now.ToString("yyyyMMddHHmmss") + "'");
            */
            string strWhere = "ORDER BY MNAM";
            //----

            mblTab = myDB.Bind("MBL", "MBL", mblDbColLgName, mblDbColLength, mblDbColName);
            mblTab.Load(strWhere);
            mblTab.CreateIndex("URNO", "URNO");

            MultiBlock multiBlock = new MultiBlock();
            cboMultiBlockName.TabStop = false;
            cboMultiBlockName.Items.Add(multiBlock);
            for (int i = 0; i < mblTab.Count; i++)
            {
                IRow row = mblTab[i];
                multiBlock = new MultiBlock(CtrlConverter.ConvertStringForClient(row["MNAM"]), row["URNO"]);
                cboMultiBlockName.Items.Add(multiBlock);

                mblNames.Add(multiBlock.Name);
            }
            cboMultiBlockName.SelectedIndex = 0;
        }

        //private bool InitUtils()
        //{
        //    //Read ceda.ini and set it global to UT (UfisTools)
        //    String strServer = "";
        //    String strHopo = "";            
        //    strServer = myIni.IniReadValue("GLOBAL", "HOSTNAME");
        //    strHopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
        //    UT.ServerName = strServer;
        //    UT.Hopo = strHopo;

        //    if (UT.UserName.Length == 0)
        //        UT.UserName = UserName;

        //    myDB = UT.GetMemDB();
        //    bool result = myDB.Connect(UT.ServerName, UT.UserName, UT.Hopo, "TAB", "DATACHNG");
        //    return result;
        //}

        private void btnRight_Click(object sender, EventArgs e)
        {
            PST item;
            while(lsLeft.SelectedItems.Count>0)
            {
                item = (PST)lsLeft.SelectedItems[0];
                lsRight.Items.Add(item);
                lsLeft.Items.Remove(item);
            }
       }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            PST item;
            while (lsRight.SelectedItems.Count > 0)
            {
                item = (PST)lsRight.SelectedItems[0];
                lsLeft.Items.Add(item);
                lsRight.Items.Remove(item);
            }
        }

        private void ch_Click(object sender, EventArgs e)
        {
            chDaily.Checked = true;
            foreach (CheckBox ch in pnlWeekdays.Controls)
            {
                if (!ch.Checked)
                {
                    chDaily.Checked = false;
                    break;
                }
            }
        }


       private void chDaily_Click(object sender, EventArgs e)
        {
            foreach (CheckBox ch in pnlWeekdays.Controls)
            {
                ch.Checked = chDaily.Checked;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool canProceed = true;
            Button btnSender = (Button)sender;
            List<string> deletedUrnos = new List<string>();

            if (btnSender.Equals(btnDelete))
            {
                DialogResult dialogResult =
                    MessageBox.Show("Are you sure to delete this record?", "Delete Multi-Blocking",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                canProceed = (dialogResult == DialogResult.Yes);
            }
            else
            {
                canProceed = IsInputValid();
            }
            if (canProceed)
            {
                IRow mblRow = null;
                MultiBlock multiBlock = null;

                blkTab = myDB.Bind("BLK", "BLK", blkDbColLgName, blkDbColLength, blkDbColName);
                if (cboMultiBlockName.SelectedIndex != -1)
                {
                    multiBlock = (MultiBlock)cboMultiBlockName.SelectedItem;
                    if (string.IsNullOrEmpty(multiBlock.Urno)) //insert new
                    {
                        mblRow = mblTab.CreateEmptyRow();
                        mblRow.Status = State.Created;
                        mblTab.Add(mblRow);
                    }
                    else //edit or delete
                    {
                        blkTab.Load("WHERE UMBL = " + multiBlock.Urno + " ORDER BY BURN");
                        blkTab.CreateIndex("BURN", "BURN");

                        IRow[] rows = mblTab.RowsByIndexValue("URNO", multiBlock.Urno);
                        if (rows == null || rows.Length > 0)
                        {
                            mblRow = rows[0];
                            mblRow.Status = State.Modified;
                        }
                    }
                }

                if (mblRow != null)
                {
                    if (btnSender.Equals(btnDelete))
                    {
                        for (int i = 0; i < blkTab.Count; i++)
                        {
                            deletedUrnos.Add(blkTab[i]["URNO"]);
                            //blkTab[i].Status = State.Deleted;
                            //blkTab.Remove(i);
                        }

                        mblRow.Status = State.Deleted;
                        //mblTab.Remove(mblRow);
                    }
                    else
                    {
                        mblRow["MNAM"] = CtrlConverter.ConvertStringForServer(txtName.Text.TrimEnd());
                        mblRow["DAYS"] = ((Days.Length > 0 && Days.Length <= 7) ? Days : "1234567");
                        mblRow["NAFR"] = UT.DateTimeToCeda(UT.LocalToUtc(VFrom));
                        mblRow["NATO"] = VTo == DateTime.MinValue ? " " : UT.DateTimeToCeda(UT.LocalToUtc(VTo));
                        mblRow["RESN"] = CtrlConverter.ConvertStringForServer(txtReason.Text);
                        mblRow["TIFR"] = TimeFrom == DateTime.MinValue ? " " : UT.LocalToUtc(TimeFrom).ToString("HHmm");
                        mblRow["TITO"] = TimeTo == DateTime.MinValue ? " " : UT.LocalToUtc(TimeTo).ToString("HHmm");
                        mblRow["IBIT"] = imgCombo.SelectedIndex.ToString(); 
                       
                        //BDPSUIF uses Old Server Char Set
                        string oldCharSetReason = CtrlConverter.ConvertStringForServer(txtReason.Text, true);

                        string pstUrnos = string.Empty;
                        foreach (PST pst in lsRight.Items)
                        {
                            bool insertNew = true;
                            IRow blkRow = null;

                            if (!string.IsNullOrEmpty(multiBlock.Urno)) //edit
                            {
                                IRow[] rows = blkTab.RowsByIndexValue("BURN", pst.Urno);
                                if (rows != null && rows.Length > 0)
                                {
                                    insertNew = false;

                                    blkRow = rows[0];
                                    blkRow.Status = State.Modified;                            
                                }
                            }
                            if (insertNew)
                            {
                                blkRow = blkTab.CreateEmptyRow();
                                blkRow.Status = State.Created;
                                blkRow["BURN"] = pst.Urno;
                                blkRow["UMBL"] = mblRow["URNO"];
                                blkTab.Add(blkRow);
                            }
                            
                            blkRow["DAYS"] = mblRow["DAYS"];
                            blkRow["NAFR"] = mblRow["NAFR"];
                            blkRow["NATO"] = mblRow["NATO"];
                            blkRow["RESN"] = oldCharSetReason;
                            blkRow["TABN"] = "PST";
                            blkRow["TIFR"] = mblRow["TIFR"];
                            blkRow["TITO"] = mblRow["TITO"];
                            blkRow["TYPE"] = " ";
                            blkRow["IBIT"] = mblRow["IBIT"];

                            pstUrnos += urnoListSep + pst.Urno;
                        }
                        if (pstUrnos.Length > 0) pstUrnos = pstUrnos.Substring(1);
                        mblRow["UPST"] = CtrlConverter.ConvertStringForServer(pstUrnos);

                        for (int i = 0; i < blkTab.Count; i++)
                        {
                            if (blkTab[i].Status == State.Unchanged)
                            {
                                deletedUrnos.Add(blkTab[i]["URNO"]);
                                //blkTab[i].Status = State.Deleted;
                                //blkTab.Remove(i);
                            }
                        }
                    }

                    if (mblTab.Save() && blkTab.Save())
                    {
                        foreach (string deletedUrno in deletedUrnos)
                        {
                            myDB.Writer.CallServer("DRT", "BLKTAB", "", "",
                                "WHERE URNO = '" + deletedUrno + "'", "1");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Cannot save the multi-blocking records!");
                    }

                    btnCancel_Click(sender, e);

                    //blkTab = myDB.Bind("BLK", "BLK", blkDbColLgName, blkDbColLength, blkDbColName);
                    ////blkTab.TimeFieldsCurrentlyInUtc = false;
                    //IRow blkRow;

                    //try
                    //{
                    //    int index = 0;

                    //    if (lsRight.Items.Count == 0)
                    //    {
                    //        MessageBox.Show("No airline code was selected.");
                    //        return;
                    //    }
                    //    foreach (PST pst in lsRight.Items)
                    //    {
                    //        blkRow = blkTab.CreateEmptyRow();

                    //        blkRow["BURN"] = pst.Urno;
                    //        blkRow["DAYS"] = Days;
                    //        blkRow["NAFR"] = UT.DateTimeToCeda(UT.LocalToUtc(VFrom));
                    //        blkRow["NATO"] = VTo == DateTime.MinValue ? string.Empty : UT.DateTimeToCeda(UT.LocalToUtc(VTo));
                    //        blkRow["RESN"] = txtReason.Text;
                    //        blkRow["TABN"] = "PST";
                    //        blkRow["TIFR"] = TimeFrom == DateTime.MinValue ? string.Empty : UT.LocalToUtc(TimeFrom).ToString("HHmm");
                    //        blkRow["TITO"] = TimeTo == DateTime.MinValue ? string.Empty : UT.LocalToUtc(TimeTo).ToString("HHmm");
                    //        blkRow["TYPE"] = string.Empty;
                    //        blkRow["IBIT"] = imgCombo.SelectedIndex.ToString();

                    //        blkRow.Status = State.Created;
                    //        index++;

                    //        blkTab.Add(blkRow);
                    //        blkTab.Save(blkRow);

                    //    }
                    //    //blkTab.Save();
                    //    //blkTab.Release(index + 1, blkDbColName, null);
                    //    this.Close();
                    //}
                    //catch (Exception ex)
                    //{
                    //    MessageBox.Show(ex.Message);
                    //}

                }
            }
        }

        private bool IsInputValid()
        {
            bool isValid = true ;

            StringBuilder sb = new StringBuilder();

            //igu on 21/06/2011
            if (cboMultiBlockName.SelectedIndex != -1)
            {
                MultiBlock multiBlock = (MultiBlock)cboMultiBlockName.SelectedItem;
                if (string.IsNullOrEmpty(multiBlock.Urno))
                {
                    string mblName = txtName.Text.TrimEnd();
                    isValid = IsNameValid(mblName);
                    if (!isValid)
                    {
                        sb.AppendLine("Multi-blocking name is empty or already exists!");
                    }
                }
            }

            //Time checking
            //Time From
            string fromTimeStr = txtTimeFrom.Text.Replace(":","");
            string toTimeStr = txtTimeTo.Text.Replace(":", "");

            if (string.IsNullOrEmpty(fromTimeStr))
            {
                _timeFrom = DateTime.MinValue;
            }
            else
            {
                if (!DateTimeHelper.ParseDateString("01011900" + fromTimeStr, "ddMMyyyyHHmm", ref _timeFrom))
                {
                    sb.AppendLine("From(Time)-> does not correspond to the input format!");
                    isValid = false;
                }
            }

            //Time To
            if (string.IsNullOrEmpty(toTimeStr))
            {
                _timeTo = DateTime.MinValue;
            }
            else
            {
                if (!DateTimeHelper.ParseDateString("01011900" + toTimeStr, "ddMMyyyyHHmm", ref _timeTo))
                {
                    sb.AppendLine("To(Time)-> does not correspond to the input format!");
                    isValid = false;
                }
            }

            //Valid checking
            //Validity From
            string fromString = txtVFromDate.Text.Replace(".", "");
            string toString = txtVToDate.Text.Replace(".", "");

            if (string.IsNullOrEmpty(fromString))
            {
                sb.AppendLine("Valid from(Date)->no data found");
                if (string.IsNullOrEmpty(txtVFromTime.Text))
                {
                    sb.AppendLine("Valid from(Time)->no data found");
                }
                isValid = false;
            }
            else
            {
                if (string.IsNullOrEmpty(txtVFromTime.Text))
                {
                    sb.AppendLine("Valid from(Time)->no data found");
                    isValid = false;
                }
                else
                {
                    fromString += txtVFromTime.Text.Replace(":", "");
                }

                if(isValid)
                    if (!DateTimeHelper.ParseDateString(fromString, "ddMMyyyyHHmm", ref _vFrom))
                {
                    sb.AppendLine("Valid from(Date/Time)-> does not correspond to the input format!");
                    isValid = false;
                }
            }

            //Validity To
            if (string.IsNullOrEmpty(toString))
            {
                //sb.AppendLine("Valid to(date)->no data found");
                //isValid = false;
                if (!string.IsNullOrEmpty(txtVToTime.Text))
                {
                    sb.AppendLine("Valid to(Date)->no data found");
                    isValid = false;
                }
                _vTo = DateTime.MinValue;
            }
            else
            {
                if (string.IsNullOrEmpty(txtVToTime.Text))
                {
                    sb.AppendLine("Valid to(Time)->no data found");
                    isValid = false;
                }
                else
                {
                    toString += txtVToTime.Text.Replace(":", "");
                }

                if (isValid)
                    if (!DateTimeHelper.ParseDateString(toString, "ddMMyyyyHHmm", ref _vTo))
                {
                    sb.AppendLine("Valid to(Date/Time)-> does not correspond to the input format!");
                    isValid = false;
                }
            }

            //Validity for Valid from and to.

            if (_vTo != DateTime.MinValue)
            {
                if (_vFrom > _vTo)
                {
                    if (isValid)
                    {
                        sb.AppendLine("Valid from/to > \"Valid from\" date/time must be before \"Valid to\"");
                        isValid = false;
                    }
                }
            }

            if (_timeTo != DateTime.MinValue)
            {
                if (_timeFrom > _timeTo)
                {
                    if (isValid)
                    {
                        sb.AppendLine("Time from/to > \"Time from\" time must be before \"Time to\"");
                        isValid = false;
                    }
                }
            }

            Days = string.Empty;
            List<string> daysList = new List<string>();

            foreach (CheckBox ch in pnlWeekdays.Controls)
            {
                if (ch.Checked)
                {
                    daysList.Add(ch.Text);
                    //Days += ch.Text;
                }
            }
            if (daysList.Count > 0)
            {
                daysList.Sort();
                Days = string.Join("", daysList.ToArray());
            }


            if (string.IsNullOrEmpty(Days))
            {
                //sb.AppendLine("Please check the Weekday(s)!");
                //isValid = false;
                Days = "1234567";
            }

            //igu on 22/06/2011
            //check if position(s) selected
            if (lsRight.Items.Count.Equals(0))
            {
                sb.AppendLine("No positions selected.");
                isValid = false;
            }

            if (!string.IsNullOrEmpty(sb.ToString()))
            {
                MessageBox.Show(sb.ToString());
            }

            return isValid;
        }

        private bool IsNameValid(string name)
        {
            bool isValid = true;

            if (string.IsNullOrEmpty(name.TrimStart()))
            {
                isValid = false;
            }
            else
            {
                if (mblNames.Contains(name))
                {
                    isValid = false;
                }
                else
                {
                    ITable mblName = myDB.Bind("MBLNAME", "MBL", "MNAM", "25", "MNAM");
                    mblName.Load("WHERE MNAM = '" + CtrlConverter.ConvertStringForServer(name) + "'");
                    if (mblName.Count > 0)
                    {
                        mblNames.Add(name);
                        isValid = false;
                    }

                    myDB.Unbind("MBLNAME");
                }
            }

            return isValid;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();            
        }

        private void cboMultiBlockName_Validating(object sender, CancelEventArgs e)
        {
            int index = cboMultiBlockName.FindString(cboMultiBlockName.Text);
            if (index < 0)
            {
                MessageBox.Show("Multi-blocking name is not found in the list!", this.Text);
                e.Cancel = true;
            }            
        }

        private void cboMultiBlockName_SelectedIndexChanged(object sender, EventArgs e)
        {   
            int index = cboMultiBlockName.SelectedIndex;

            if (index != -1)
            {
                MultiBlock multiBlock = (MultiBlock)cboMultiBlockName.SelectedItem;

                FillInForm(multiBlock);
            }
        }

        private void FillInForm(MultiBlock multiBlock)
        {
            int comboListIndex = 0;

            if (string.IsNullOrEmpty(multiBlock.Urno))
            {
                txtName.Text = string.Empty;

                if (clearFormOnNewRecord) //igu on 15/07/2011
                {
                    ClearForm();
                }

                txtName.TabStop = true;
                txtName.ReadOnly = false;
                btnDelete.Visible = false;
                btnCopy.Visible = false; //igu on 15/07/2010
                btnRemove.Visible = false; //igu on 15/07/2010
            }
            else
            {
                txtName.Text = cboMultiBlockName.Text;

                IRow[] rows = mblTab.RowsByIndexValue("URNO", multiBlock.Urno);
                if (rows.Length == 0)
                {
                    ClearForm();
                }
                else
                {
                    IRow row = rows[0];
                    DateTime fromDate;
                    DateTime toDate;
                    DateTime fromTime = new DateTime();
                    DateTime toTime = new DateTime();

                    if (!int.TryParse(row["IBIT"], out comboListIndex))
                        comboListIndex = 0;

                    imgCombo.SelectedIndex = comboListIndex;
                    string timeString =row["TIFR"];
                    if (string.IsNullOrEmpty(timeString) || timeString.Length != 4)
                    {
                        txtTimeFrom.Text = string.Empty;
                    }
                    else
                    {
                        int hours = 0;
                        int minutes = 0;

                        if (!int.TryParse(timeString.Substring(0, 2), out hours)) hours = 0;
                        if (!int.TryParse(timeString.Substring(2), out minutes)) minutes = 0;

                        TimeSpan ts = new TimeSpan(hours, minutes, 0);
                        fromTime = UT.UtcToLocal(fromTime.Add(ts));

                        txtTimeFrom.Text = fromTime.ToString("HH:mm");
                    }
                    timeString = row["TITO"];
                    if (string.IsNullOrEmpty(timeString) || timeString.Length != 4)
                    {
                        txtTimeTo.Text = string.Empty;
                    }
                    else
                    {
                        int hours = 0;
                        int minutes = 0;

                        if (!int.TryParse(timeString.Substring(0, 2), out hours)) hours = 0;
                        if (!int.TryParse(timeString.Substring(2), out minutes)) minutes = 0;

                        TimeSpan ts = new TimeSpan(hours, minutes, 0);
                        toTime = UT.UtcToLocal(toTime.Add(ts));

                        txtTimeTo.Text = toTime.ToString("HH:mm");
                    }
                    chDaily.Checked = true;
                    foreach (CheckBox ch in pnlWeekdays.Controls)
                    {
                        if (row["DAYS"].IndexOf(ch.Text) >= 0)
                        {
                            ch.Checked = true;
                        }
                        else
                        {
                            ch.Checked = false;
                            chDaily.Checked = false;
                        }
                    }
                    txtReason.Text = CtrlConverter.ConvertStringForClient(row["RESN"]);
                    if (string.IsNullOrEmpty(row["NAFR"]))
                    {
                        txtVFromDate.Text = string.Empty;
                        txtVFromTime.Text = string.Empty;
                    }
                    else
                    {
                        fromDate = UT.UtcToLocal(UT.CedaFullDateToDateTime(row["NAFR"]));

                        txtVFromDate.Text = fromDate.ToString("dd.MM.yyyy");
                        txtVFromTime.Text = fromDate.ToString("HH:mm");
                    }
                    if (string.IsNullOrEmpty(row["NATO"]))
                    {
                        txtVToDate.Text = string.Empty;
                        txtVToTime.Text = string.Empty;
                    }
                    else
                    {
                        toDate = UT.UtcToLocal(UT.CedaFullDateToDateTime(row["NATO"]));

                        txtVToDate.Text = toDate.ToString("dd.MM.yyyy");
                        txtVToTime.Text = toDate.ToString("HH:mm");
                    }
                    string[] pstUrnos = CtrlConverter.ConvertStringForClient(row["UPST"]).Split(urnoListSep);
                    while (lsRight.Items.Count > 0)
                    {
                        PST item = (PST)lsRight.Items[0];
                        lsLeft.Items.Add(item);
                        lsRight.Items.Remove(item);
                    }
                    List<PST> psts = new List<PST>();
                    foreach (PST pst in lsLeft.Items)
                    {
                        if (((IList<string>)pstUrnos).Contains(pst.Urno))
                        {
                            psts.Add(pst);                        
                        }
                    }
                    foreach (PST pst in psts)
                    {
                        lsRight.Items.Add(pst);
                        lsLeft.Items.Remove(pst);
                    }
                }

                txtName.TabStop = false;
                txtName.ReadOnly = true;
                btnDelete.Visible = true;
                btnCopy.Visible = true; //igu on 15/07/2010
                btnRemove.Visible = true; //igu on 15/07/2010
            }
        }

        private void ClearForm()
        {
            imgCombo.SelectedIndex = 0;
            txtTimeFrom.Text = string.Empty;
            txtTimeTo.Text = string.Empty;
            foreach (CheckBox ch in pnlWeekdays.Controls)
            {
                ch.Checked = false;
            }
            chDaily.Checked = false;
            txtReason.Text = string.Empty;
            txtVFromDate.Text = string.Empty;
            txtVFromTime.Text = string.Empty;
            txtVToDate.Text = string.Empty;
            txtVToTime.Text = string.Empty;
            while (lsRight.Items.Count > 0)
            {
                PST item = (PST)lsRight.Items[0];
                lsLeft.Items.Add(item);
                lsRight.Items.Remove(item);
            }
        }

        //igu on 15/07/2011
        private void btnCopy_Click(object sender, EventArgs e)
        {
            clearFormOnNewRecord = false;
            cboMultiBlockName.SelectedIndex = 0;
            clearFormOnNewRecord = true;
        }

        //igu on 15/07/2011
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (cboMultiBlockName.SelectedIndex > 0)
            {
                DialogResult dialogResult =
                    MessageBox.Show("Are you sure to remove this record from the list?", "Remove Multi-Blocking",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dialogResult == DialogResult.Yes)
                {
                    bool isRemoved = false;
            
                    MultiBlock multiBlock = (MultiBlock)cboMultiBlockName.SelectedItem;
                    if (!string.IsNullOrEmpty(multiBlock.Urno))
                    {
                        IRow[] rows = mblTab.RowsByIndexValue("URNO", multiBlock.Urno);
                        if (rows == null || rows.Length > 0)
                        {
                            IRow mblRow = rows[0];
                            mblRow.Status = State.Deleted;

                            if (mblTab.Save())
                            {
                                //remove from the combobox
                                cboMultiBlockName.Items.Remove(multiBlock);
                                cboMultiBlockName.SelectedIndex = 0;

                                mblTab.Remove(mblRow);
                                isRemoved = true;
                            }
                        }
                    }

                    if (!isRemoved)
                    {
                        MessageBox.Show("Failed to remove the multi-blocking record!");
                    }
                }
            }
        }
        
    }

    class PST
    {
        public string Name { get; set; }
        public string Urno { get; set; }


        public PST(string name, string urno)
        {
            Name = name;
            Urno = urno;
        }

        public override string  ToString()
        {
 	         return Name.ToString();
        }
    }

    //igu on 21/06/2011
    class MultiBlock
    {
        public string Name { get; set; }
        public string Urno { get; set; }

        public MultiBlock()
        {
        }

        public MultiBlock(string name, string urno)
        {
            Name = name;
            Urno = urno;
        }

        public override string  ToString()
        {
            if (string.IsNullOrEmpty(Urno))
            {
                return "(New record)";
            }
            else
            {
                return Name;
            }
        }
    }
}
