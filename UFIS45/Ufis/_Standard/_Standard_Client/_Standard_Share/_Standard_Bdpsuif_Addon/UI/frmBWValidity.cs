﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using _Standard_Bdpsuif_Addon.Ctrl;
using Ufis.Data;
using Microsoft.Win32;
using _Standard_Bdpsuif_Addon.Helper;
using Ufis.Utils;
using System.Globalization;

namespace _Standard_Bdpsuif_Addon.UI
{
    public partial class frmBWValidity : Form
    {
        private enum SpecialColumnEnum
        {
            ValidFrom,
            ValidTo,
            CreatedBy,
            CreatedOn,
            LastUpdatedBy,
            LastUpdatedOn,
            DeletionFlag
        }

        private CtrlBDValidity bdValidity;
        private DataTable dataTable;
        private DataView dataView;
        private Dictionary<SpecialColumnEnum, string> specialColumns = new Dictionary<SpecialColumnEnum, string>();
        private bool isDeletionFlagColumnSpecified = false;
        private bool isReadOnly = false;
        private string appType;

        //ufis data
        private IDatabase myDB;
        private ITable myMasterTable;
        private ITable myDetailTable;
        private ITable mySchedulerTable = null;
        DataView dvChanges;

        /// <summary>
        /// Read the Registry to get the Form Location and size parameter
        ///    and then set it.
        /// </summary>
        private void ReadRegistry()
        {
            int myX = -1, myY = -1, myW = -1, myH = -1;
            FormWindowState windowState = FormWindowState.Normal;
            string regVal = "";
            string theKey = "Software\\BDPSUIF_Addon\\BWA_" + appType;
            RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
            if (rk != null)
            {
                regVal = rk.GetValue("X", "-1").ToString();
                myX = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Y", "-1").ToString();
                myY = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Width", "-1").ToString();
                myW = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Height", "-1").ToString();
                myH = Convert.ToInt32(regVal);
                try
                {
                    windowState = (FormWindowState)Enum.Parse(typeof(FormWindowState),
                        rk.GetValue("WindowState", "Normal").ToString());
                }
                catch
                {
                    windowState = FormWindowState.Normal;
                }
            }

            //save current screens width and height
            int screenWidth = 0, screenHeight = 0;
            foreach (Screen screen in Screen.AllScreens)
            {
                screenWidth += screen.Bounds.Width;
                screenHeight += screen.Bounds.Height;
            }

            //left and top
            if (myX == -1) myX = this.Left;
            if (myY == -1) myY = this.Top;

            //check for width
            if (myW != -1)
            {
                if (myW > screenWidth)
                {
                    myX = this.Left;
                    myW = this.Width;
                }
                else
                {
                    if (myX + myW > screenWidth)
                    {
                        myX = screenWidth - myW;
                    }
                }
            }
            else
            {
                myW = this.Width;
            }

            //check for height
            if (myH != -1)
            {
                if (myH > screenHeight)
                {
                    myH = this.Height;
                }
                else
                {
                    if (myY + myH > screenHeight)
                    {
                        myY = screenHeight - myH;
                    }
                }
            }
            else
            {
                myH = this.Height;
            }

            this.Left = myX;
            this.Top = myY;
            this.Width = myW;
            this.Height = myH;

            this.WindowState = windowState;
        }

        /// <summary>
        /// Save Form Location and size parameter
        ///    to the registry.
        /// </summary>
        private void SaveToRegistry()
        {
            string theKey = "Software\\BDPSUIF_Addon\\" + appType;
            RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
            if (rk == null)
            {//If No registry key then create it
                rk = Registry.CurrentUser.OpenSubKey("Software\\BDPSUIF_Addon", true);
                if (rk == null)
                {
                    rk = Registry.CurrentUser.OpenSubKey("Software", true);
                    rk.CreateSubKey("BDPSUIF_Addon");
                    rk.Close();
                    rk = Registry.CurrentUser.OpenSubKey("Software\\BDPSUIF_Addon", true);
                }
                rk.CreateSubKey(appType);
                rk.Close();
                rk = Registry.CurrentUser.OpenSubKey(theKey, true);
            }

            if (this.WindowState == FormWindowState.Normal)
            {
                rk.SetValue("X", this.Left.ToString());
                rk.SetValue("Y", this.Top.ToString());
                rk.SetValue("Width", this.Width.ToString());
                rk.SetValue("Height", this.Height.ToString());
            }
            rk.SetValue("WindowState", this.WindowState);
        }

        private bool PrepareDataSource(CtrlBDValidity.BDDataSource bdDataSource)
        {
            bool result = true;

            try
            {
                myDB = Ctrl.CtrlData.GetInstance().MyDB;

                foreach (CtrlBDValidity.BDDataSource.Table table in bdDataSource.Tables)
                {
                    string tableName = table.TableName;
                    if (string.IsNullOrEmpty(table.TableAlias))
                    {
                        table.TableAlias = tableName;
                    }
                    string tableAlias = table.TableAlias;
                    string colNameList = string.Empty;
                    string colLengthList = string.Empty;
                    string dateColList = string.Empty;
                    string loadParam = string.Empty;

                    if (!string.IsNullOrEmpty(table.Filter)) loadParam += "WHERE " + table.Filter;
                    if (!string.IsNullOrEmpty(table.Sort)) loadParam += " ORDER BY " + table.Sort;

                    bool isUrnoColExist = false;
                    foreach (CtrlBDValidity.BDDataSource.Table.Column col in table.Columns)
                    {
                        isUrnoColExist = isUrnoColExist || (col.Name.ToUpper() == "URNO");

                        colNameList += "," + col.Name;
                        colLengthList += "," + col.Length.ToString();
                        switch (col.DataType)
                        {
                            case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.DateTime:
                            case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.CreationDate:
                            case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.ModificationDate:
                            case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.ValidFrom:
                            case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.ValidTo:
                                dateColList += "," + col.Name;
                                break;
                        }
                    }
                    if (colNameList.Length > 0) colNameList = colNameList.Substring(1);
                    if (colLengthList.Length > 0) colLengthList = colLengthList.Substring(1);
                    if (dateColList.Length > 0) dateColList = dateColList.Substring(1);

                    ITable iTable = myDB.Bind(tableAlias, tableName, colNameList, colLengthList, colNameList);
                    iTable.AllowToSaveToDatabase = (isUrnoColExist);
                    if (!string.IsNullOrEmpty(dateColList))
                    {
                        iTable.TimeFields = dateColList;
                        iTable.TimeFieldsInitiallyInUtc = true;
                        iTable.TimeFieldsCurrentlyInUtc = false;
                    }
                    if (isUrnoColExist)
                    {
                        iTable.Command("insert", ",IRT,");
                        iTable.Command("update", ",URT,");
                        iTable.Command("delete", ",DRT,");
                    }
                    iTable.Load(loadParam.Trim());

                    if (((table.Type & CtrlBDValidity.BDDataSource.Table.TableTypeEnum.HelperTable) !=
                          CtrlBDValidity.BDDataSource.Table.TableTypeEnum.HelperTable) && isUrnoColExist)
                    {
                        iTable.CreateIndex("URNO", "URNO");
                    }
                }

                //create index for HelperTable
                foreach (CtrlBDValidity.BDLayout.BodySection.PairViewColumn pairColumn in bdValidity.Layout.Body.PairColumns)
                {
                    ITable iTable = myDB[pairColumn.DataSource];
                    foreach (CtrlBDValidity.BDLayout.BodySection.PairViewColumn.Column column in pairColumn.Columns)
                    {
                        iTable.CreateIndex(column.ViewColumnName, column.FieldName);
                    }
                }

                myMasterTable = myDB[bdDataSource.Tables.MasterTable.TableAlias];
                myDetailTable = myDB[bdDataSource.Tables.DetailTable.TableAlias];
                if (bdDataSource.Tables.SchedulerTable != null)
                {
                    mySchedulerTable = myDB[bdDataSource.Tables.SchedulerTable.TableAlias];

                    //create index for SchedulerTable
                    CtrlBDValidity.BDDataSource.ColumnMap column =
                        bdValidity.DataSource.ColumnMaps.GetKeyColumnMap();
                    if (column != null)
                    {
                        mySchedulerTable.CreateIndex(column.SchedulerColumnName, column.SchedulerColumnName);
                    }
                }

                if (myMasterTable.Count != 1)
                {
                    MessageBox.Show("No record or multiple records found!", this.Text,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    result = false;
                }
            }
            catch
            {
                MessageBox.Show("Error reading data source!", this.Text,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = false;
            }

            return result;
        }

        private void PreparePanelLayout(FlowLayoutPanel panel, CtrlBDValidity.BDLayout.Section bdSection)
        {
            foreach (CtrlBDValidity.BDLayout.Section.SectionItem item in bdSection.Items)
            {
                Label lbl = new Label();
                string labelText = item.Value;
                switch (item.ValueType)
                {
                    case CtrlBDValidity.BDLayout.Section.SectionItem.SectionItemValueTypeEnum.Expression:
                        object oText = RuntimeEvalHelper.Eval(item.Value);
                        if (oText != null) labelText = oText.ToString();
                        break;
                    case CtrlBDValidity.BDLayout.Section.SectionItem.SectionItemValueTypeEnum.BoundField:
                        try
                        { labelText = myDB[item.DataSource][0][labelText]; }
                        catch { }
                        break;
                }
                lbl.Text = labelText;
                lbl.BorderStyle = item.BorderStyle;
                lbl.AutoSize = true;
                lbl.Font = SetFont(item.FontName, item.FontSize,
                    item.FontBold, item.FontItalic, item.FontUnderline);

                panel.Controls.Add(lbl);
            }

            panel.Visible = (panel.Controls.Count > 0);
        }

        private Font SetFont(string name, float size, bool bold, bool italic, bool underline)
        {
            Font font = new Font(name, size);

            if (font != null)
            {
                FontStyle fontStyle = font.Style;
                if (bold)
                {
                    if ((fontStyle & FontStyle.Bold) == 0)
                    {
                        fontStyle |= FontStyle.Bold;
                        font = new Font(font, fontStyle);
                    }
                }
                if (italic)
                {
                    if ((fontStyle & FontStyle.Italic) == 0)
                    {
                        fontStyle |= FontStyle.Italic;
                        font = new Font(font, fontStyle);
                    }
                }
                if (underline)
                {
                    if ((fontStyle & FontStyle.Underline) == 0)
                    {
                        fontStyle |= FontStyle.Underline;
                        font = new Font(font, fontStyle);
                    }
                }
            }
            return font;
        }

        private void PrepareDataGridView(DataGridView dataGridView, CtrlBDValidity.BDLayout.BodySection bdSection, CtrlBDValidity.BDDataSource.Table detailTable)
        {
            dataTable = GetDataTableFromBDValidityTable(detailTable, true);
            //add handler
            dataTable.RowChanged += new DataRowChangeEventHandler(dataTable_RowChanged);

            dataView = new DataView(dataTable);

            string colName;
            if (specialColumns.TryGetValue(SpecialColumnEnum.DeletionFlag, out colName))
            {
                dataView.RowFilter = string.Format("{0} <> 'X'", colName);
            }


            dataGridView.AutoGenerateColumns = false;
            dataGridView.DataSource = dataView;

            dataGridView.ReadOnly = isReadOnly;
            dataGridView.AllowUserToAddRows = !isReadOnly;
            btnOK.Visible = !isReadOnly;

            if ((!isReadOnly) && bdSection.ShowDeleteButton)
            {
                DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();
                buttonColumn.Text = "Delete";
                buttonColumn.UseColumnTextForButtonValue = true;
                buttonColumn.Width = 45;
                dataGridView.Columns.Add(buttonColumn);

            }

            foreach (CtrlBDValidity.BDLayout.BodySection.ViewColumn viewColumn in bdSection.Columns)
            {
                DataGridViewCellStyle contentCellStyle = new DataGridViewCellStyle();
                contentCellStyle.Format = viewColumn.DisplayFormat;
                contentCellStyle.Alignment = viewColumn.ContentAlignment;
                if (viewColumn.ReadOnly)
                {
                    contentCellStyle.BackColor = System.Drawing.SystemColors.ButtonFace;
                }

                DataGridViewCellStyle headerCellStyle = new DataGridViewCellStyle();
                headerCellStyle.Alignment = viewColumn.HeaderAlignment;

                DataGridViewColumn dataGridViewColumn;

                switch (viewColumn.ColumnType)
                {
                    case CtrlBDValidity.BDLayout.BodySection.ViewColumn.ViewColumnTypeEnum.List:
                        DataTable dtTable = null;

                        DataGridViewComboBoxColumn dgvComboBoxColumn = new DataGridViewComboBoxColumn();
                        if (viewColumn.SourceType == CtrlBDValidity.BDLayout.BodySection.ViewColumn.ViewColumnSourceTypeEnum.ValueList)
                        {
                            dtTable = GetDataTableFromValueList(viewColumn.DataSource);
                            if (dtTable != null)
                            {
                                dgvComboBoxColumn.DataSource = dtTable;
                                dgvComboBoxColumn.ValueMember = "Col" + viewColumn.ValueMember;
                                dgvComboBoxColumn.DisplayMember = "Col" + viewColumn.DisplayMember;
                            }
                        }
                        else
                        {
                            CtrlBDValidity.BDDataSource.Table bdTable = null;
                            if (bdValidity.DataSource.Tables.TryGetTable(viewColumn.DataSource, out bdTable))
                            {
                                dtTable = GetDataTableFromBDValidityTable(bdTable);
                                dgvComboBoxColumn.DataSource = dtTable;
                                dgvComboBoxColumn.ValueMember = viewColumn.ValueMember;
                                dgvComboBoxColumn.DisplayMember = viewColumn.DisplayMember;
                            }
                        }
                        dgvComboBoxColumn.FlatStyle = FlatStyle.Flat;
                        dataGridView.Columns.Add(dgvComboBoxColumn);
                        dataGridViewColumn = dataGridView.Columns[dgvComboBoxColumn.Index];
                        break;
                    case CtrlBDValidity.BDLayout.BodySection.ViewColumn.ViewColumnTypeEnum.Text:
                    default:
                        DataGridViewTextBoxColumn dgvTextBoxColumn = new DataGridViewTextBoxColumn();
                        dataGridView.Columns.Add(dgvTextBoxColumn);
                        dataGridViewColumn = dataGridView.Columns[dgvTextBoxColumn.Index];
                        break;
                }

                dataGridViewColumn.DataPropertyName = viewColumn.BoundField;
                dataGridViewColumn.Name = viewColumn.BoundField;
                dataGridViewColumn.HeaderText = viewColumn.Caption;
                dataGridViewColumn.Width = viewColumn.Width;
                dataGridViewColumn.ReadOnly = viewColumn.ReadOnly;
                dataGridViewColumn.Visible = viewColumn.Visible;
                dataGridViewColumn.DefaultCellStyle = contentCellStyle;
                dataGridViewColumn.HeaderCell.Style = headerCellStyle;
            }

        }

        private DataTable GetDataTableFromBDValidityTable(CtrlBDValidity.BDDataSource.Table bdTable)
        {
            return GetDataTableFromBDValidityTable(bdTable, false);
        }

        private DataTable GetDataTableFromBDValidityTable(CtrlBDValidity.BDDataSource.Table bdTable, bool getSpecialColumns)
        {
            int colIndex = 0;

            DataTable dataTable = new DataTable(bdTable.TableAlias);
            ITable iTable = myDB[bdTable.TableAlias];
            foreach (string field in iTable.FieldList)
            {
                CtrlBDValidity.BDDataSource.Table.Column column =
                    bdTable.Columns[colIndex];
                Type columnType = Type.GetType("System.String");
                switch (column.DataType)
                {
                    case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.Int:
                        columnType = Type.GetType("System.Int32");
                        break;
                    case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.Double:
                        columnType = Type.GetType("System.Double");
                        break;
                    case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.DateTime:
                    case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.CreationDate:
                        if (getSpecialColumns && !specialColumns.ContainsKey(SpecialColumnEnum.CreatedOn))
                        {
                            specialColumns.Add(SpecialColumnEnum.CreatedOn, field);
                        }
                        columnType = Type.GetType("System.DateTime");
                        break;
                    case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.ModificationDate:
                        if (getSpecialColumns && !specialColumns.ContainsKey(SpecialColumnEnum.LastUpdatedOn))
                        {
                            specialColumns.Add(SpecialColumnEnum.LastUpdatedOn, field);
                        }
                        columnType = Type.GetType("System.DateTime");
                        break;
                    case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.ValidFrom:
                        if (getSpecialColumns && !specialColumns.ContainsKey(SpecialColumnEnum.ValidFrom))
                        {
                            specialColumns.Add(SpecialColumnEnum.ValidFrom, field);
                        }
                        columnType = Type.GetType("System.DateTime");
                        break;
                    case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.ValidTo:
                        if (getSpecialColumns && !specialColumns.ContainsKey(SpecialColumnEnum.ValidTo))
                        {
                            specialColumns.Add(SpecialColumnEnum.ValidTo, field);
                        }
                        columnType = Type.GetType("System.DateTime");
                        break;
                    case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.CreatedBy:
                        if (getSpecialColumns && !specialColumns.ContainsKey(SpecialColumnEnum.CreatedBy))
                        {
                            specialColumns.Add(SpecialColumnEnum.CreatedBy, field);
                        }
                        break;
                    case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.DeletionFlag:
                        isDeletionFlagColumnSpecified = true;
                        if (getSpecialColumns && !specialColumns.ContainsKey(SpecialColumnEnum.DeletionFlag))
                        {
                            specialColumns.Add(SpecialColumnEnum.DeletionFlag, field);
                        }
                        break;
                    case CtrlBDValidity.BDDataSource.Table.Column.ColumnDataTypeEnum.ModifiedBy:
                        if (getSpecialColumns && !specialColumns.ContainsKey(SpecialColumnEnum.LastUpdatedBy))
                        {
                            specialColumns.Add(SpecialColumnEnum.LastUpdatedBy, field);
                        }
                        break;
                }

                DataColumn dataColumn = new DataColumn();
                dataColumn.ColumnName = field;
                dataColumn.DataType = columnType;
                if (column.DefaultValue != null)
                {
                    if (column.DefaultValueType == CtrlBDValidity.BDDataSource.Table.Column.ColumnDefaultValueTypeEnum.Text)
                    {
                        dataColumn.DefaultValue = column.DefaultValue;
                    }
                    else
                    {
                        dataColumn.DefaultValue = RuntimeEvalHelper.Eval(column.DefaultValue.ToString());
                    }
                }
                dataColumn.AllowDBNull = !column.Required;
                dataTable.Columns.Add(dataColumn);

                colIndex++;
            }

            if (!isDeletionFlagColumnSpecified)
            {
                string field = "__STAT__";
                specialColumns.Add(SpecialColumnEnum.DeletionFlag, field);

                DataColumn dataColumn = new DataColumn(field, Type.GetType("System.String"));
                dataColumn.DefaultValue = "";
                dataTable.Columns.Add(dataColumn);
            }

            for (int i = 0; i < iTable.Count; i++)
            {
                DataRow dataRow = dataTable.NewRow();
                for (int j = 0; j < iTable.FieldList.Count; j++)
                {
                    string dataValue = iTable[i][j];
                    FillInCellValue(dataRow, j, dataValue);
                }
                dataTable.Rows.Add(dataRow);
            }

            dataTable.AcceptChanges();

            return dataTable;
        }

        private DataTable GetDataTableFromValueList(string valueList)
        {
            int colIndex = 0;

            DataTable dataTable = new DataTable();

            try
            {
                string[] rows = valueList.Split(';');
                string[] cols = rows[0].Split(',');

                foreach (string col in cols)
                {
                    DataColumn dataColumn = new DataColumn();
                    dataColumn.ColumnName = "Col" + (colIndex + 1).ToString();
                    dataColumn.DataType = Type.GetType("System.String");
                    dataTable.Columns.Add(dataColumn);

                    colIndex++;
                }

                foreach (string row in rows)
                {
                    colIndex = 0;
                    cols = row.Split(',');

                    DataRow dataRow = dataTable.NewRow();
                    foreach (string col in cols)
                    {
                        FillInCellValue(dataRow, colIndex, col);

                        colIndex++;
                    }
                    dataTable.Rows.Add(dataRow);
                }
            }
            catch
            {
                dataTable = null;
            }

            return dataTable;
        }

        private void FillInCellValue(DataRow dataRow, int colIndex, string value)
        {
            DataColumn dataColumn = dataRow.Table.Columns[colIndex];

            if (string.IsNullOrEmpty(value) && dataColumn.DataType != Type.GetType("System.String"))
            {
                dataRow[colIndex] = DBNull.Value;
            }
            else
            {
                if (dataColumn.DataType == Type.GetType("System.Int32"))
                {
                    Int32 intValue;
                    if (Int32.TryParse(value, out intValue))
                    {
                        dataRow[colIndex] = intValue;
                    }
                    else
                    {
                        dataRow[colIndex] = DBNull.Value;
                    }
                }
                if (dataColumn.DataType == Type.GetType("System.Double"))
                {
                    Double dblValue;
                    if (Double.TryParse(value, out dblValue))
                    {
                        dataRow[colIndex] = dblValue;
                    }
                    else
                    {
                        dataRow[colIndex] = DBNull.Value;
                    }
                }
                if (dataColumn.DataType == Type.GetType("System.DateTime"))
                {
                    dataRow[colIndex] = Ufis.Utils.UT.CedaFullDateToDateTime(value);
                }
                if (dataColumn.DataType == Type.GetType("System.String"))
                {
                    dataRow[colIndex] = value;
                }
            }
        }

        private void PrepareLayout()
        {
            this.Text = bdValidity.Layout.Title;

            PreparePanelLayout(panelHeader, bdValidity.Layout.Header);
            PreparePanelLayout(panelFooter, bdValidity.Layout.Footer);
            PrepareDataGridView(dgvData, bdValidity.Layout.Body, bdValidity.DataSource.Tables.DetailTable);
        }

        private bool ValidateRecords(out List<DataRowView> currentAndFutureRows)
        {
            bool result = true;
            currentAndFutureRows = new List<DataRowView>();

            string statusCol = "STAT";
            string validFromCol = string.Empty;
            string validToCol = string.Empty;
            string deletionFlagColumn = string.Empty;

            if (!specialColumns.TryGetValue(SpecialColumnEnum.ValidFrom, out validFromCol))
                return true;

            specialColumns.TryGetValue(SpecialColumnEnum.ValidTo, out validToCol);

            specialColumns.TryGetValue(SpecialColumnEnum.DeletionFlag, out deletionFlagColumn);

            int rowIndex = 0;
            DateTime prevValidFrom = DateTime.MinValue;
            DateTime prevValidTo = DateTime.MinValue;
            DateTime now = DateTime.Now;
            //var modify = false;
            string sortCon = " ASC";

            dataView = new DataView(dataTable);

            dataView.Sort = validFromCol + sortCon;

            //dgvData.DataSource = dataView;

            var count = dataView.Count - 1;

            foreach (DataRowView dvRow in dataView)
            {
                DateTime validFrom;
                DateTime validTo = DateTime.MaxValue;

                dvRow.Row.ClearErrors();

                if (dvRow[deletionFlagColumn] != "X")
                {

                    if (string.IsNullOrEmpty(dvRow[statusCol].ToString()))
                    {
                        dvRow.Row.SetColumnError(statusCol, "Status can not be empty");
                        result = false;
                    }

                    if (rowIndex != count)
                    {
                        if (dvRow[validToCol] == DBNull.Value)
                        {
                            dvRow.Row.SetColumnError(validToCol, "Date can not be empty");
                            result = false;
                        }
                    }
                    if (dvRow[validFromCol] == DBNull.Value)
                    {
                        dvRow.Row.SetColumnError(validFromCol, "Date can not be empty");
                        result = false;
                    }
                    else
                    {
                        validFrom = (DateTime)dvRow[validFromCol];

                        if (dvRow.Row.RowState == DataRowState.Added)
                        {

                            if (validFrom < now)
                            {
                                dvRow.Row.SetColumnError(validFromCol, "Valid from must be greater than Now!");
                                result = false;
                            }
                        }
                        if (validFrom <= prevValidFrom)
                        {
                            dvRow.Row.SetColumnError(validFromCol, "Valid From must be greater than previous Valid From");
                            result = false;
                        }

                        prevValidFrom = validFrom;

                        validTo = DateTime.MaxValue;
                        if (validFrom > now || (validFrom <= now && validTo >= now))
                        {
                            if (myDetailTable != mySchedulerTable && (dvRow.Row.RowState != DataRowState.Unchanged))
                                currentAndFutureRows.Add(dvRow);
                        }

                        if (dvRow[validToCol] != DBNull.Value)
                        {
                            validTo = (DateTime)dvRow[validToCol];

                            if (validFrom >= validTo)
                            {
                                dvRow.Row.SetColumnError(validToCol, "Valid To must be greater than Valid From");
                                result = false;
                            }
                            else
                            {
                                if (validFrom.Date > now.Date || (validFrom.Date <= now.Date && validTo >= now))
                                {
                                    if (myDetailTable != mySchedulerTable && (dvRow.Row.RowState != DataRowState.Unchanged))
                                        currentAndFutureRows.Add(dvRow);
                                }
                                else
                                {
                                    if (dvRow.Row.RowState != DataRowState.Unchanged)
                                    {
                                        dvRow.Row.SetColumnError(validToCol, "Valid To must be greater than Now!");
                                        result = false;
                                    }
                                }
                            }
                        }


                        if (validFrom <= prevValidTo && prevValidTo != DateTime.MinValue)
                        {
                            dvRow.Row.SetColumnError(validFromCol, "Valid From must be greater than previous Valid To");

                            result = false;
                        }

                        prevValidTo = validTo;

                    }

                    rowIndex++;
                }
            }

            //filter for deleted rows
            if (myDetailTable != mySchedulerTable)
            {
                if (specialColumns.TryGetValue(SpecialColumnEnum.DeletionFlag, out deletionFlagColumn))
                {
                    DataView dvDeleted = new DataView(dataTable, "URNO IS NOT NULL AND " + deletionFlagColumn + " = 'X'",
                        "URNO", DataViewRowState.ModifiedCurrent);
                    foreach (DataRowView dvRow in dvDeleted)
                    {
                        currentAndFutureRows.Add(dvRow);
                    }
                }
            }


            dataView.Sort = "";

            return result;
        }

        private bool SaveRecords(DataTable dataTable, ITable detailTable, ITable masterTable, List<DataRowView> currentandFutureRows)
        {
            bool result = false;

            try
            {
                DataRow dataRowNTS = null;
                string urnoNTS = string.Empty;

                string primaryKey = "URNO";

                //save detail table
                if (detailTable.UniqueFields != null)
                {
                    if (detailTable.UniqueFields.Count == 1)
                    {
                        primaryKey = detailTable.UniqueFields[0].ToString();
                    }
                }

                string deletionFlagField = string.Empty;
                specialColumns.TryGetValue(SpecialColumnEnum.DeletionFlag, out deletionFlagField);

                foreach (DataRow dataRow in dataTable.Rows)
                {
                    if (dataRow[primaryKey] == DBNull.Value)
                    {
                        if (dataRow[deletionFlagField].ToString() != "X")
                        {
                            IRow iRow = detailTable.CreateEmptyRow();
                            if (!SaveRow(dataRow, iRow, State.Created, deletionFlagField))
                            {
                                return result;
                            }
                            detailTable.Add(iRow);

                            if (myDetailTable == mySchedulerTable)
                            {
                                dataRowNTS = dataRow;
                                urnoNTS = iRow["URNO"];
                            }
                        }
                    }
                    else
                    {
                        if (dataRow.RowState == DataRowState.Modified)
                        {
                            IRow[] iRows = detailTable.RowsByIndexValue(primaryKey, dataRow[primaryKey].ToString());
                            if (iRows != null && iRows.Length > 0)
                            {
                                IRow iRow = iRows[0];
                                if (dataRow[deletionFlagField].ToString() == "X" && !isDeletionFlagColumnSpecified)
                                {
                                    iRow.Status = State.Deleted;
                                }
                                else
                                {
                                    if (!SaveRow(dataRow, iRow, State.Modified, deletionFlagField))
                                    {
                                        return result;
                                    }

                                    if (myDetailTable == mySchedulerTable)
                                    {
                                        dataRowNTS = dataRow;
                                        urnoNTS = iRow["URNO"];
                                    }
                                }
                            }
                        }
                    }
                }

                detailTable.Save();
                dataTable.AcceptChanges();

                //save to scheduler table
                if (mySchedulerTable != null)
                {
                    if (myDetailTable == mySchedulerTable)
                    {
                        if (dataRowNTS != null && (!string.IsNullOrEmpty(urnoNTS)))
                        {
                            SendNTSCommand(dataRowNTS, urnoNTS);
                        }
                    }
                    else
                    {
                        if (!SaveToSchedulerTable(mySchedulerTable, currentandFutureRows, bdValidity.DataSource.ColumnMaps))
                        {
                            return result;
                        }
                    }
                }

                result = true;
            }
            catch
            {
                result = false;
            }

            return result;
        }

        private void SendNTSCommand(DataRow dataRow, string urno)
        {
            string validFromCol;
            string validToCol;
            DateTime now = DateTime.Now.Date;

            if (specialColumns.TryGetValue(SpecialColumnEnum.ValidFrom, out validFromCol))
            {
                specialColumns.TryGetValue(SpecialColumnEnum.ValidTo, out validToCol);

                DateTime validFrom = (DateTime)dataRow[validFromCol];
                DateTime validTo = DateTime.MaxValue.Date;
                if (!string.IsNullOrEmpty(validToCol))
                {
                    if (dataRow[validToCol] != DBNull.Value)
                    {
                        validTo = (DateTime)dataRow[validToCol];
                    }
                }

                if (validFrom <= now && validTo >= now)
                {//curent validity row   
                    //send command to backend                
                    myDB.Writer.CallServer("NTS", "NTSTAB", "URNO", urno, "", "1");
                }
            }
        }

        private bool SaveToSchedulerTable(ITable schedulerTable, List<DataRowView> dataRowViews, CtrlBDValidity.BDDataSource.ColumnMapCollection columnMaps)
        {
            bool result = false;
            string validFromCol;
            string validToCol;
            DateTime now = DateTime.Now.Date;

            if (specialColumns.TryGetValue(SpecialColumnEnum.ValidFrom, out validFromCol))
            {
                specialColumns.TryGetValue(SpecialColumnEnum.ValidTo, out validToCol);

                CtrlBDValidity.BDDataSource.ColumnMap columnMap =
                    bdValidity.DataSource.ColumnMaps.GetKeyColumnMap();
                if (columnMap != null)
                {
                    string deletionFlagColumn = string.Empty;
                    specialColumns.TryGetValue(SpecialColumnEnum.DeletionFlag, out deletionFlagColumn);

                    string urno = string.Empty;
                    foreach (DataRowView dataRowView in dataRowViews)
                    {
                        DateTime validFrom = (DateTime)dataRowView[validFromCol];
                        validFrom = validFrom.Date;
                        DateTime validTo = DateTime.MaxValue.Date;
                        if (!string.IsNullOrEmpty(validToCol))
                        {
                            if (dataRowView[validToCol] != DBNull.Value)
                            {
                                validTo = (DateTime)dataRowView[validToCol];
                            }
                        }

                        //is it in the scheduler table?
                        IRow[] rows = schedulerTable.RowsByIndexValue(
                                columnMap.SchedulerColumnName,
                                dataRowView[columnMap.Value].ToString());
                        IRow row = null;
                        if (rows != null && rows.Length > 0)
                        {//found, update or delete
                            row = rows[0];
                            if (dataRowView[deletionFlagColumn].ToString() == "X")
                            {//delete
                                row.Status = State.Deleted;
                            }
                            else
                            {//update
                                if (!SaveSchedulerRow(dataRowView, row, bdValidity.DataSource.ColumnMaps, State.Modified))
                                {
                                    return result;
                                }

                                if (validFrom <= now && validTo >= now)
                                {//curent validity row   
                                    //save the urno
                                    urno = row["URNO"];
                                }
                            }
                        }
                        else
                        {//not found, insert
                            if (dataRowView[deletionFlagColumn].ToString() != "X")
                            {
                                row = schedulerTable.CreateEmptyRow();
                                if (!SaveSchedulerRow(dataRowView, row, bdValidity.DataSource.ColumnMaps, State.Created))
                                {
                                    return result;
                                }
                                schedulerTable.Add(row);

                                if (validFrom <= now && validTo >= now)
                                {//curent validity row   
                                    //save the urno
                                    urno = row["URNO"];
                                }
                            }
                        }
                    }

                    schedulerTable.Save();

                    //send command to backend                
                    if (!string.IsNullOrEmpty(urno))
                    {
                        myDB.Writer.CallServer("NTS", "NTSTAB", "URNO", urno, "", "1");
                    }
                }
            }

            result = true;
            return result;
        }

        private bool SaveRow(DataRow dataRow, IRow iRow, State state, string deletionFlagField)
        {
            bool result = true;

            try
            {
                for (int i = 0; i < iRow.Count; i++)
                {
                    DataColumn dataColumn = dataRow.Table.Columns[i];
                    if (dataColumn.ColumnName == "URNO")
                    {
                        if (state == State.Created)
                        {
                            dataRow[dataColumn] = iRow[i];
                        }
                    }
                    else
                    {
                        if (isDeletionFlagColumnSpecified || dataColumn.ColumnName != deletionFlagField)
                        {
                            string nullString = (dataColumn.DataType == Type.GetType("System.DateTime") ? "" : " ");
                            iRow[i] = ToCedaString(dataRow[i], nullString);
                        }
                    }
                }
                iRow.Status = state;
            }
            catch
            {
                result = false;
            }

            return result;
        }

        private bool SaveSchedulerRow(DataRowView dataRowView, IRow iRow, CtrlBDValidity.BDDataSource.ColumnMapCollection columnMaps, State state)
        {
            bool result = true;

            try
            {
                bool dataChanged = false;
                foreach (CtrlBDValidity.BDDataSource.ColumnMap columnMap in columnMaps)
                {
                    if (!columnMap.IsKeyColumn)
                    {
                        string currValue = iRow[columnMap.SchedulerColumnName];
                        string newValue = string.Empty;
                        switch (columnMap.ValueType)
                        {
                            case CtrlBDValidity.BDDataSource.ColumnMap.ColumnMapValueTypeEnum.Text:
                                newValue = columnMap.Value;
                                break;
                            case CtrlBDValidity.BDDataSource.ColumnMap.ColumnMapValueTypeEnum.Expression:
                                newValue = EvaluateMap(columnMap.Value, dataRowView);
                                object value = RuntimeEvalHelper.Eval(newValue);
                                if (value != null) newValue = value.ToString();
                                break;
                            case CtrlBDValidity.BDDataSource.ColumnMap.ColumnMapValueTypeEnum.DetailColumnName:
                                string[] fields = columnMap.Value.Split(columnMap.Separator);
                                foreach (string field in fields)
                                {
                                    string nullString =
                                        (dataRowView.Row.Table.Columns[field].DataType == Type.GetType("System.DateTime") ? "" : " ");
                                    newValue += columnMap.Separator.ToString() +
                                        ToCedaString(dataRowView[field], nullString);
                                }
                                if (newValue.Length > 0) newValue = newValue.Substring(1);
                                break;
                        }

                        iRow[columnMap.SchedulerColumnName] = newValue;
                        dataChanged = dataChanged || (currValue.Trim() != newValue.Trim());
                    }
                }

                if (dataChanged) iRow.Status = state;
            }
            catch
            {
                result = false;
            }

            return result;
        }

        private string ToCedaString(object value)
        {
            return ToCedaString(value, " ");
        }

        private string ToCedaString(object value, string nullString)
        {
            string newValue = string.Empty;

            if (value == DBNull.Value)
            {
                newValue = nullString;
            }
            else
            {
                if (value is System.DateTime)
                {
                    DateTime dateTime = (DateTime)value;
                    newValue = Ufis.Utils.UT.DateTimeToCeda(dateTime);
                }
                else
                {
                    if (value is System.Double)
                    {
                        newValue = string.Format("{0:0.00}", value);
                    }
                    else
                    {
                        newValue = value.ToString();
                        if (newValue == string.Empty) newValue = " ";
                    }
                }
            }

            return newValue;
        }

        private string EvaluateMap(string mapValue, DataRowView dataRowView)
        {
            string result = mapValue;

            foreach (DataColumn column in dataRowView.Row.Table.Columns)
            {
                result = result.Replace("[" + column.ColumnName + "]",
                    ToStringEx(dataRowView[column.ColumnName]));
            }

            return result;
        }

        private string EvaluateRule(string rule, DataGridView dgView, int rowIndex)
        {
            string result = rule;

            foreach (DataGridViewColumn column in dgvData.Columns)
            {
                result = result.Replace("[" + column.Name + "]",
                    ToStringEx(dgView[column.Name, rowIndex].Value));
            }

            return result;
        }

        private string ToStringEx(object value)
        {
            if (value is string)
            {
                return "\"" + value.ToString() + "\"";
            }

            if (value is double)
            {
                return string.Format("{0:0.00}", value);
            }

            return value.ToString();
        }

        public frmBWValidity(string appType, string urno, string permission)
        {
            this.appType = appType;

            if (permission == "0" || permission == "1")
            {
                isReadOnly = (permission == "0");

                bdValidity = CtrlConfig.GetBDValidity(appType, urno, UT.UserName);
                InitializeComponent();
            }
        }

        private void frmBWValidity_Load(object sender, EventArgs e)
        {
            if (bdValidity != null)
            {
                if (PrepareDataSource(bdValidity.DataSource))
                {
                    PrepareLayout();

                    ReadRegistry();
                    this.BringToFront();
                    this.TopMost = true;
                    this.TopMost = false;
                }
                else
                {
                    this.Close();
                }
            }
            else
            {
                this.Close();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            List<DataRowView> currentAndFutureRows;


            if (ValidateRecords(out currentAndFutureRows))
            {
                if (SaveRecords(dataTable, myDetailTable, myMasterTable, currentAndFutureRows))
                {
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Failed to save to database!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error); ;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DataView dvChanges = new DataView(dataTable, "", "", DataViewRowState.Added |
                DataViewRowState.ModifiedCurrent);

            if (dvChanges.Count > 0)
            {
                DialogResult result = MessageBox.Show(
                    "Do you want to save the changes?", this.Text,
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1);
                switch (result)
                {
                    case DialogResult.Yes:
                        btnOK_Click(sender, e);
                        break;
                    case DialogResult.No:
                        this.Close();
                        break;
                }
            }
            else
            {
                this.Close();
            }
        }

        private void dgvData_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Invalid Date Time Format(DD/MM/YYYY HH:MM)!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void dgvData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvData.Columns[e.ColumnIndex] is DataGridViewButtonColumn
                && e.RowIndex != -1 && e.RowIndex != dgvData.NewRowIndex)
            {
                string colName;
                if (specialColumns.TryGetValue(SpecialColumnEnum.DeletionFlag, out colName))
                {
                    DialogResult result = MessageBox.Show(
                        "Are you sure to delete this record?", this.Text,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button2);
                    if (result == DialogResult.Yes)
                    {
                        dataView[e.RowIndex][colName] = "X";
                        this.BindingContext[this.dgvData.DataSource].EndCurrentEdit();
                    }
                }
            }
        }

        private void dgvData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            dataView = new DataView(dataTable); 

            if (e.RowIndex < dataView.Count)
            {
                DataColumn dataColumn = dataTable.Columns[dgvData.Columns[e.ColumnIndex].Name];
                if (dataView[e.RowIndex].Row.GetColumnError(dataColumn) != "")
                    dataView[e.RowIndex].Row.SetColumnError(dataColumn, "");
            }
        }

        private void dgvData_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.RowIndex < dataView.Count)
            {
                bool isRefreshNeeded = false;

                //Update LSTU,USEU
                if (!dataView[e.RowIndex].IsNew && dataView[e.RowIndex]["URNO"] != DBNull.Value)
                {
                    string colName;
                    if (specialColumns.TryGetValue(SpecialColumnEnum.LastUpdatedBy, out colName))
                    {
                        dataView[e.RowIndex][colName] = UT.UserName;
                        isRefreshNeeded = true;
                    }
                    if (specialColumns.TryGetValue(SpecialColumnEnum.LastUpdatedOn, out colName))
                    {
                        dataView[e.RowIndex][colName] = DateTime.Now;
                        isRefreshNeeded = true;
                    }
                }
                //

                //Update the pair columns
                string dataColName = dgvData.Columns[e.ColumnIndex].DataPropertyName;
                CtrlBDValidity.BDLayout.BodySection.PairViewColumn pairColumn =
                    bdValidity.Layout.Body.PairColumns.FindByName(dataColName);
                if (pairColumn != null)
                {
                    string value = dgvData[e.ColumnIndex, e.RowIndex].Value.ToString();

                    ITable iTable = myDB[pairColumn.DataSource];
                    IRow[] rows = iTable.RowsByIndexValue(dataColName, value);
                    if (rows != null && rows.Length > 0)
                    {
                        IRow row = rows[0];
                        foreach (CtrlBDValidity.BDLayout.BodySection.PairViewColumn.Column column in pairColumn.Columns)
                        {
                            if (column.ViewColumnName != dataColName)
                            {
                                dataView[e.RowIndex][column.ViewColumnName] = row[column.FieldName];
                                isRefreshNeeded = true;
                            }
                        }
                    }
                    else
                    {
                        foreach (CtrlBDValidity.BDLayout.BodySection.PairViewColumn.Column column in pairColumn.Columns)
                        {
                            if (column.ViewColumnName != dataColName)
                            {
                                dataView[e.RowIndex][column.ViewColumnName] = DBNull.Value;
                                isRefreshNeeded = true;
                            }
                        }
                    }
                }
                //

                //  System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("fr-FR");

                //Date Time part for VAFR and VATO
                string validFromCol = string.Empty;
                if (specialColumns.TryGetValue(SpecialColumnEnum.ValidFrom, out validFromCol))
                {
                    if (dataColName == validFromCol)
                    {
                        object validFrom = dgvData[e.ColumnIndex, e.RowIndex].Value;
                        if (validFrom != DBNull.Value)
                        {
                            dataView[e.RowIndex][validFromCol] = ((DateTime)validFrom);
                            isRefreshNeeded = true;
                        }
                    }
                }

                string validToCol = string.Empty;
                if (specialColumns.TryGetValue(SpecialColumnEnum.ValidTo, out validToCol))
                {
                    if (dataColName == validToCol)
                    {
                        object validTo = dgvData[e.ColumnIndex, e.RowIndex].Value;
                        if (validTo != DBNull.Value)
                        {
                            dataView[e.RowIndex][validToCol] = ((DateTime)validTo);
                            isRefreshNeeded = true;
                        }
                    }
                }
                //

                ////Auto fill-in previous VATO    
                //if (dataColName == validFromCol)
                //{
                //    if (!string.IsNullOrEmpty(validToCol))
                //    {
                //        object value = dgvData[e.ColumnIndex, e.RowIndex].Value;
                //        if (value != DBNull.Value)
                //        {
                //            DateTime newDate = (DateTime)value;

                //            string condition = validFromCol + " < #" + newDate.ToString(
                //                System.Globalization.DateTimeFormatInfo.InvariantInfo) + "# " +
                //                "AND " + validToCol + " IS NULL";
                //            string deletionFlagCol = string.Empty;
                //            if (specialColumns.TryGetValue(SpecialColumnEnum.DeletionFlag, out deletionFlagCol))
                //                condition += " AND " + deletionFlagCol + " <> 'X'";
                //            string order = validFromCol + " DESC";

                //            DataRow[] dataRows =
                //                dataTable.Select(condition, order);
                //            if (dataRows.Length > 0)
                //            {
                //                DataRow dataRow = dataRows[0];
                //                if (dataRow[validToCol] == DBNull.Value)
                //                {
                //                    dataRow[validToCol] = newDate.Date.AddSeconds(-1);
                //                    isRefreshNeeded = true;
                //                }
                //            }
                //        }
                //    }
                //}
                //

                //Apply the ValueChangedRule
                CtrlBDValidity.BDLayout.BodySection.ViewColumn viewColumn =
                    bdValidity.Layout.Body.Columns.FindByName(dataColName);
                if (viewColumn != null && viewColumn.ValueChangedRule != null)
                {
                    try
                    {
                        string rule = EvaluateRule(viewColumn.ValueChangedRule.Expression,
                            dgvData, e.RowIndex);
                        rule = rule.Replace("{:value}", ToStringEx(dataView[e.RowIndex][dataColName]));

                        object result = RuntimeEvalHelper.Eval(rule);
                        if (result != null)
                        {
                            dataView[e.RowIndex][viewColumn.ValueChangedRule.ColumnName] = result;
                            isRefreshNeeded = true;
                        }
                    }
                    catch { }
                }
                //

                //if (isRefreshNeeded)
                this.BindingContext[this.dataView].EndCurrentEdit();

                this.dgvData.DataSource = dataView;
            }
        }

        private void dgvData_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                TextBox txt = (TextBox)e.Control;
                txt.CharacterCasing = CharacterCasing.Upper;
            }
        }

        private void dgvData_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            string dataColName = dgvData.Columns[e.ColumnIndex].DataPropertyName;
            CtrlBDValidity.BDLayout.BodySection.ViewColumn viewColumn =
                bdValidity.Layout.Body.Columns.FindByName(dataColName);
            if (viewColumn != null && viewColumn.EditableRule != "")
            {
                string rule = EvaluateRule(viewColumn.EditableRule, dgvData, e.RowIndex);
                object result = RuntimeEvalHelper.Eval(rule);
                if (result != null && result is bool)
                {
                    bool editable = (bool)result;
                    e.Cancel = !editable;
                }
            }
        }

        private void dataTable_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            switch (e.Action)
            {
                case DataRowAction.Add:
                    string colName = string.Empty;
                    if (specialColumns.TryGetValue(SpecialColumnEnum.CreatedBy, out colName))
                        e.Row[colName] = UT.UserName;
                    if (specialColumns.TryGetValue(SpecialColumnEnum.CreatedOn, out colName))
                        e.Row[colName] = DateTime.Now;
                    break;
            }
        }




        private void dgvData_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            if (dgvData[e.ColumnIndex, e.RowIndex].IsInEditMode)
            {
                DataGridViewColumn dgvColumn = dgvData.Columns[e.ColumnIndex];
                if (dgvColumn.ValueType == Type.GetType("System.DateTime"))
                {
                    string dateString = e.FormattedValue.ToString();

                    CtrlBDValidity.BDLayout.BodySection.ViewColumn viewColumn =
                        bdValidity.Layout.Body.Columns.FindByName(dgvColumn.Name);
                    if (viewColumn.InputFormats != null)
                    {
                        foreach (string inputFormat in viewColumn.InputFormats)
                        {
                            DateTime dateTime;
                            try
                            {
                                dateTime = DateTime.ParseExact(dateString, inputFormat, null);
                                // if (DateTime.TryParseExact(dateString, inputFormat, null,
                                //System.Globalization.DateTimeStyles.None, out dateTime))
                                // {

                                dgvData[e.ColumnIndex, e.RowIndex].Value = dateTime;

                                break;
                                //}
                            }
                            catch
                            {
                                continue;
                            } 
                        }
                    }
                }
            }
        }

        private void frmBWValidity_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Store current geometry in Registry
            //To use back this setting on next time
            SaveToRegistry();
        }
    }
}