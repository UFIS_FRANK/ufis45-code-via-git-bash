﻿namespace _Standard_Bdpsuif_Addon
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnBlockPst = new System.Windows.Forms.Button();
            this.loginControl = new AxAATLOGINLib.AxAatLogin();
            this.btnCon = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.loginControl)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBlockPst
            // 
            this.btnBlockPst.Location = new System.Drawing.Point(205, 22);
            this.btnBlockPst.Name = "btnBlockPst";
            this.btnBlockPst.Size = new System.Drawing.Size(75, 23);
            this.btnBlockPst.TabIndex = 2;
            this.btnBlockPst.Text = "Block Multi Pst";
            this.btnBlockPst.UseVisualStyleBackColor = true;
            this.btnBlockPst.Click += new System.EventHandler(this.btnBlockPst_Click);
            // 
            // loginControl
            // 
            this.loginControl.Enabled = true;
            this.loginControl.Location = new System.Drawing.Point(12, 89);
            this.loginControl.Name = "loginControl";
            this.loginControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("loginControl.OcxState")));
            this.loginControl.Size = new System.Drawing.Size(100, 50);
            this.loginControl.TabIndex = 3;
            this.loginControl.Visible = false;
            // 
            // btnCon
            // 
            this.btnCon.Location = new System.Drawing.Point(204, 73);
            this.btnCon.Name = "btnCon";
            this.btnCon.Size = new System.Drawing.Size(75, 34);
            this.btnCon.TabIndex = 4;
            this.btnCon.Text = "Continents";
            this.btnCon.UseVisualStyleBackColor = true;
            this.btnCon.Click += new System.EventHandler(this.btnCon_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.btnCon);
            this.Controls.Add(this.loginControl);
            this.Controls.Add(this.btnBlockPst);
            this.Name = "frmMain";
            this.Text = "Test Form";
            ((System.ComponentModel.ISupportInitialize)(this.loginControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnBlockPst;
        private AxAATLOGINLib.AxAatLogin loginControl;
        private System.Windows.Forms.Button btnCon;
    }
}