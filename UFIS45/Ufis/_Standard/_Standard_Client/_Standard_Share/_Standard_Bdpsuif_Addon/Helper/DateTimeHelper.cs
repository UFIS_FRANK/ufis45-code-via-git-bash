﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

using System.Globalization;

namespace _Standard_Bdpsuif_Addon.Helper
{
    public class DateTimeHelper
    {
        public static bool ParseDateString(string strToParse, string format, ref DateTime outPut)
        {
            if (DateTime.TryParseExact(strToParse, format, CultureInfo.CurrentCulture, DateTimeStyles.NoCurrentDateDefault, out outPut))
            {
                return true;
            }
            return false;
        }
    }
}
