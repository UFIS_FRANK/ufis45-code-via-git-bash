﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Ufis.MVVM.ViewModel;
using Ufis.Utilities;
using Ufis.MVVM.Command;

namespace Ufis.NotificationWindow
{
    /// <summary>
    /// Represents the view model for notification window.
    /// </summary>
    public class NotificationViewModel : WorkspaceViewModel
    {
        #region Fields

        MessageInfo _notificationInfo;
        RelayCommand _quitCommand;        

        #endregion // Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of notification view model.
        /// </summary>
        public NotificationViewModel() {}
        
        /// <summary>
        /// Initializes a new instance of notification view model.
        /// </summary>
        /// <param name="notificationInfo">Notification message to display.</param>
        public NotificationViewModel(MessageInfo notificationInfo) 
        {
            _notificationInfo = notificationInfo;
        } 

        #endregion // Constructor

        #region Public Interface
        
        /// <summary>
        /// Gets or sets the notification message for this notification view model.
        /// </summary>
        public MessageInfo NotificationInfo
        {
            get { return _notificationInfo; }
            set
            {
                if (value == _notificationInfo)
                    return;

                _notificationInfo = value;

                base.OnPropertyChanged("NotificationInfo");
            }
        }

        /// <summary>
        /// Returns a command that log in the user.
        /// </summary>
        public ICommand QuitCommand
        {
            get
            {
                if (_quitCommand == null)
                {
                    _quitCommand = new RelayCommand(Quit);
                }
                return _quitCommand;
            }
        }

        /// <summary>
        /// Gets a value indicating wheter the quit command has been executed.
        /// </summary>
        public bool IsQuitCommandExecuted { get; private set; }
        
        /// <summary>
        /// Gets or sets wheter continue button will be displayed on the notification window.
        /// </summary>
        public bool CanContinue { get; set; }

        #endregion 

        #region Public Methods

        /// <summary>
        /// Quits the application.
        /// </summary>
        public void Quit(object parameter)
        {
            IsQuitCommandExecuted = true;

            CloseCommand.Execute(null);
        }

        #endregion // Public Methods
    }
}