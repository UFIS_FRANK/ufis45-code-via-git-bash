//***********************************************************************
// CopPast.h : interface of the CCopPast class
//***********************************************************************
#ifndef __COPPAST_H__
#define __COPPAST_H__
class CCopPast;

#include <stdafx.h>

typedef CTypedPtrArray<CPtrArray, CGXStyle*> CStyles;	// the style collection

class CCopPast
{
public:
	bool mCanPastRow;
	bool mCanPastRegion;
	ROWCOL m_ColNum;			// the column number
	CStyles mStyles;			// the particular styles

	bool mIsInUndoTransaction;	// the undo transaction is currently performed
	bool mCanUndoPastRegion;
	bool mNowUndoUpdate;
	CStyles mUndoStyles;		// the particular undo styles
	CRowColArray mUndoRows;	// indexes of the updated rows
public:
	CCopPast();					// constructor
	~CCopPast();				// destructor
	void Clear();
	void ClearUndo();
};

#endif // __COPPAST_H__