// DBDPSView.h : interface of the CDBDPSView class
//
/////////////////////////////////////////////////////////////////////////////

class CDBDPSView : public CGXBrowserView
{
protected: // create from serialization only
	CDBDPSView();
	DECLARE_DYNCREATE(CDBDPSView)

// Attributes
public:
	ROWCOL mMaxVisibleCols;		// MapCol[mMaxVisibleCols] is the last real column number
										// it is also the number of all visible columns
	ROWCOL MapCol[MAX_COLUMNS];	// MapCol[nCol] gives the real column number
											// out of the visible nCol number
// Operations
public:
	CDBDPSDoc* GetDocument();


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDBDPSView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	void OnQueryChange();
	virtual void DeleteRows(const CRowColArray& awRowStart, const CRowColArray& awRowEnd);
	virtual BOOL OnFlushRecord(ROWCOL nRow, CString* ps = NULL);
	virtual void OnFlushCellValue(ROWCOL nRow, ROWCOL nCol, LPCTSTR pszChangedValue);
	virtual BOOL OnLoadCellStyle(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, LPCTSTR pszExistingValue);
	virtual BOOL StoreStyleRowCol(ROWCOL nRow, ROWCOL nCol, const CGXStyle *pStyle, GXModifyType mt, int nType);
	virtual long OnGetRecordCount();
	virtual short GetFieldFromCol(ROWCOL nCol);
	virtual long GetRecordFromRow(ROWCOL nRow);
	virtual void OnMovedCurrentCell(ROWCOL nRow, ROWCOL nCol);
	BOOL GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle & style, GXModifyType mt, int nType);

	//*** 15.11.99 SHA ***
	//virtual	BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	virtual	void OnTextNotFound(LPCTSTR);
	BOOL bmSortAscend;
	BOOL bmSortNumerical;
	//*** 16.11.99 SHA ***
	void OnSaveSettings();
	void OnDeleteSettings();
	void ReadSettings();
	void ReadIniFile(LPCTSTR pSection, LPCTSTR pKeyName, LPTSTR pDefaultString, LPCTSTR pIniFile);
	void ConvertToArray(CString &olIn, CStringArray &olOut);
	int	imRowHeight;
	CStringArray omColWidth;


	//virtual ~CDBDPSView();
	virtual ~CDBDPSView();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	void EnableMenuItem(CCmdUI*);
	void EnableSec2MenuItem
		(
			CCmdUI* pCmdUI,
			LPCTSTR pObjectId,
			LPCTSTR pControlId
		);
// Generated message map functions
protected:
	virtual BOOL CanAppend();
	//{{AFX_MSG(CDBDPSView)
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnExport();
	afx_msg void OnSort();
	afx_msg void OnFilterSort();
	afx_msg void OnDeleteRow();
	afx_msg void OnEditPasteRow();
	afx_msg void OnEditPasteRegion();
	afx_msg void OnEditUndoPasteRegion();
	afx_msg void OnUpdateExport(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFilePrintPreview(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFilePrintSetup(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSort(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFilterSort(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeleteRow(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);	
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);	
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);	
	afx_msg void OnUpdateEditPasteRow(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditPasteRegion(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditUndoPasteRegion(CCmdUI* pCmdUI);
	//}}AFX_MSG
	//*** 16.11.99 SHA ***
	afx_msg void OnFind();
	afx_msg void OnDoFile( UINT nID );
	afx_msg void OnMore();
	afx_msg LONG OnBcMessage(UINT, LONG);
	DECLARE_MESSAGE_MAP()
private:
//	virtual BOOL CanPaste();
	virtual BOOL Copy();
	virtual BOOL Cut();
	virtual BOOL OnDeleteCell(ROWCOL nRow, ROWCOL nCol);
};

#ifndef _DEBUG  // debug version in DBDPSView.cpp
inline CDBDPSDoc* CDBDPSView::GetDocument()
   { return (CDBDPSDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////
