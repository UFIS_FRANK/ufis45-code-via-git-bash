// RegisterDlg.cpp : implementation file
//

#include <stdafx.h>
#include <dbdps.h>
#include <RegisterDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

UINT mTimer;
int mSecRemain;
TCHAR myString[10];

/////////////////////////////////////////////////////////////////////////////
// CRegisterDlg dialog


CRegisterDlg::CRegisterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRegisterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRegisterDlg)
	//}}AFX_DATA_INIT
}


void CRegisterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRegisterDlg)
	DDX_Control(pDX, IDC_STATIC_SEC, m_StaticSec);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRegisterDlg, CDialog)
	//{{AFX_MSG_MAP(CRegisterDlg)
	ON_WM_TIMER()
	ON_BN_CLICKED(ID_REGISTRY, OnRegistry)
	ON_BN_CLICKED(ID_START, OnStart)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRegisterDlg message handlers

BOOL CRegisterDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	mTimer = SetTimer(1, 1000, NULL);
	mSecRemain = 5;

	_itot(mSecRemain, myString, 10);
	m_StaticSec.SetWindowText(myString);
	
	return TRUE;
}

void CRegisterDlg::OnTimer(UINT nIDEvent) 
{
	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == mTimer)
	{
		mSecRemain--;
		_itot(mSecRemain, myString, 10);
		m_StaticSec.SetWindowText(myString);
		if(mSecRemain <= 0)
		{
			KillTimer(mTimer);
			OnStart();
		}
	}
}

void CRegisterDlg::OnRegistry() 
{
	KillTimer(mTimer);
	EndDialog(IDYES);
}

void CRegisterDlg::OnStart() 
{
	KillTimer(mTimer);
	EndDialog(IDNO);
}
