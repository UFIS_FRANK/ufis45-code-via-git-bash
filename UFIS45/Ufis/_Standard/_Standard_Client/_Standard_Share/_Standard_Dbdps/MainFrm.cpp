// MainFrm.cpp : implementation of the CMainFrame class
//

#include <stdafx.h>
#include <DBDPS.h>
#include <Globals.h>
#include <DBDPSDoc.h>
#include <DBDPSView.h>
#include <MainFrm.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_CBN_SELCHANGE(IDC_COMBO_FILTER, OnSelChangeSavedView)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_pWndRecordInfo = NULL;
}


CMainFrame::~CMainFrame()
{
	delete m_pWndRecordInfo;
	AllSavedViews.RemoveAll();
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.Create(this) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Remove this if you don't want tool tips or a resizeable toolbar
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	// create dialog-bar with the view combo
	if (!CreateDlgBar())
		return -1;

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFrameWnd::PreCreateWindow(cs);
}

//-----------------------------------------------------------------------
// CreateDlgBar
//	
//	The function creates dialog-bar with the view combo
//-----------------------------------------------------------------------
BOOL CMainFrame::CreateDlgBar()
{
	if (!m_wndDlgBar.Create(this, IDD_DIALOG_TOOLBAR,
		CBRS_TOP|CBRS_TOOLTIPS|CBRS_FLYBY, IDD_DIALOG_TOOLBAR))
	{
		TRACE0("Failed to create DlgBar\n");
		return FALSE;      // fail to create
	}

	if(!RefreshCombo())
		return FALSE;

	return TRUE;
}

//-----------------------------------------------------------------------
// FillCombo
//	
//	The function fills view combo from intern structure of saved views
//-----------------------------------------------------------------------
BOOL CMainFrame::FillCombo(CComboBox* myCombo)
{
	CString UrnoText;
	CString ViewName;
	CString TableName;
	CString FilterText;
	CString SortText;

	myCombo->ResetContent();	// remove all items from the list box and edit
	for(int i = 0; i < AllSavedViews.GetSize(); i++)
	{
		AllSavedViews.GetAt(i, UrnoText, ViewName, TableName, FilterText, SortText);
		if (myCombo->AddString(ViewName) < 0) return FALSE;
	}

	return TRUE;
}

//-----------------------------------------------------------------------
// RefreshCombo
//	
//	The function refreshed the intern structure of saved views and then
// fills view combo from this structure
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 14.04.1999 | Martin Danihel    | Added 'default' view functionality
//-----------------------------------------------------------------------
BOOL CMainFrame::RefreshCombo()
{
	CString myTableName;
	BOOL tmpDefault = FALSE;

	if(gTableLoad)
	{
		myTableName = gTableName;
		tmpDefault = TRUE;	// get the 'default' view too
	}

	// fills the intern structure of saved views
	if(!LoadSavedViews(myTableName, &AllSavedViews, tmpDefault))
		return FALSE;
	// fills view combo from this structure
	if(!FillCombo((CComboBox*) m_wndDlgBar.GetDlgItem(IDC_COMBO_FILTER)))
		return FALSE;

	return TRUE;
}

//-----------------------------------------------------------------------
// SetSavedViewSelection
//	
//	The function sets the selection in view combo
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CMainFrame::SetSavedViewSelection(const CString &pViewUrno)
{
	int mySelection = -1;
	CComboBox* pCBox = (CComboBox*)m_wndDlgBar.GetDlgItem(IDC_COMBO_FILTER);

	if(!pViewUrno.IsEmpty()) // if some view is selected in the application toolbar
	{
		CString UrnoText;
		CString ViewName;
		CString TableName;
		CString FilterText;
		CString SortText;
		for(int i = 0; i < AllSavedViews.GetSize(); i++)
		{
			// search for the URNO of the view
			AllSavedViews.GetAt(i, UrnoText, ViewName, TableName, FilterText, SortText);
			if(UrnoText == pViewUrno)
			{
				CString tmpViewName;
				for(int myPosition = 0; myPosition < pCBox->GetCount( ) ; myPosition++)
				{
					pCBox->GetLBText(myPosition, tmpViewName);
					if(tmpViewName == ViewName)
						mySelection = myPosition;
				}
			}
		}
	}

	pCBox->SetCurSel(mySelection);
}

//-----------------------------------------------------------------------
// GetUrnoFromSelectedView
//	
//	The function returned UrnoId of the selected View
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 02.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
CString CMainFrame::GetUrnoFromSelectedView()
{
	int position;
	CComboBox* pCBox = (CComboBox*)m_wndDlgBar.GetDlgItem(IDC_COMBO_FILTER);

	position = pCBox->GetCurSel();
	if(position == CB_ERR)
		return _T("");
	else
	{
		CString myViewName;
		CString UrnoText;
		CString ViewName;
		CString TableName;
		CString FilterText;
		CString SortText;

		pCBox->GetLBText(position, myViewName);	// get the view name
		for(int i = 0; i < AllSavedViews.GetSize(); i++)
		{
			AllSavedViews.GetAt(i, UrnoText, ViewName, TableName, FilterText, SortText);
			if(ViewName == myViewName)
				return UrnoText;
		}
	}
	return _T("");
}

//-----------------------------------------------------------------------
// OnNcActivate
//
// This function avoids flashing of the titlebar when a CGXComboBox
// or CGXTabbedComboBox is dropped down
//-----------------------------------------------------------------------
BOOL CMainFrame::OnNcActivate(BOOL bActive)
{
	if (GXDiscardNcActivate())
		return TRUE;

	return CFrameWnd::OnNcActivate(bActive);
}


/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	// record info wnd without support for dynamic splitter views
	m_pWndRecordInfo = new CGXRecordInfoWnd;

	return m_pWndRecordInfo->Create(this, pContext, WS_CHILD|WS_VISIBLE|WS_HSCROLL);

}

//-----------------------------------------------------------------------
// OnSelChangeSavedView
//	
//	The message handler of selecting view from view combo
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 14.04.1999 | Martin Danihel    | Identification of selected view by
//            |                   | URNO
//-----------------------------------------------------------------------
void CMainFrame::OnSelChangeSavedView()
{
	CString strText;
	CString strItem;
	CComboBox* pCBox = (CComboBox*)m_wndDlgBar.GetDlgItem(IDC_COMBO_FILTER);

	// get selected view
	int nIndex = pCBox->GetCurSel();
	if (nIndex == CB_ERR)
		return;

	// test, if grid is in valid state
	if(gTableLoad)	// if grid is filld with data
		if (((CDBDPSView*) GetActiveView())->GetBrowseParam()->m_nEditMode != GRID_NO_EDIT_MODE)
		{
			// grid is in edit mode
			AfxMessageBox("OnSelChangeSavedView(). Current table is not in the valid state. First validate current record.", MB_OK + MB_ICONSTOP);
			pCBox->SetCurSel(-1);
			return;
		}

	CString UrnoText;
	CString ViewName;
	CString TableName;
	CString FilterText;
	CString SortText;
	CString tmpFilterText;

	// get the selected view from the intern structure
	AllSavedViews.GetAt(nIndex, UrnoText, ViewName, TableName, tmpFilterText, SortText);
	if(tmpFilterText.GetLength() > 0)
		FilterText = FilterText + WHERE_STRING + tmpFilterText;
	
	// fill intern data structure of the grid data
	ASSERT(GetActiveDocument()->IsKindOf(RUNTIME_CLASS(CDBDPSDoc)));
	if (!((CDBDPSDoc*) GetActiveDocument())->FillDataStructure(TableName.Left(SYS_FIELD_LENGTH[F_TANA]), gTableExt, FilterText))
		UrnoText.Empty();	// no view will be selected in view combo

	// refresh view combo (set views for current table)
	RefreshCombo();

	// set selected view in view combo
	int tmpSetIndex = -1;
	CString tmpUrnoText;

	for(int i = 0; i < AllSavedViews.GetSize(); i++)
	{
		AllSavedViews.GetAt(i, tmpUrnoText, ViewName, TableName, FilterText, SortText);
		if(UrnoText == tmpUrnoText)
		{
			tmpSetIndex = i;
			break;
		}
	}

	if(tmpSetIndex > -1 && 
		pCBox->GetCount() != CB_ERR && pCBox->GetCount() > tmpSetIndex)
		pCBox->SetCurSel(tmpSetIndex);

	// set global sort-clause object and execute sorting on the intern data structure
	ClearSortedFields();
	if(!ConvertFieldsToSortIndex(TableName.Left(SYS_FIELD_LENGTH[F_TANA]), SortText))
	{
		AfxMessageBox("OnSelChangeSavedView(). Table cannot be sorted.", MB_OK + MB_ICONSTOP);
		return;
	}
	else
		((CDBDPSDoc*) GetActiveDocument())->DoSort();

	ASSERT(GetActiveView()->IsKindOf(RUNTIME_CLASS(CDBDPSView)));
	((CDBDPSView*)GetActiveView())->OnQueryChange();
}

//-----------------------------------------------------------------------
// ShowDialogBar
//	
//	The function shows or hides the dialog-bar
//-----------------------------------------------------------------------
void CMainFrame::ShowDialogBar(int pCmdShow)
{
	m_wndDlgBar.ShowWindow(pCmdShow);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//*** 28.09.99 SHA ***
void CMainFrame::OnClose() 
{
	CString olMessage;

	//*** ATTENTION ***
	//*** THIS IS TEMP. HAS TO BE SOLVED IN THE NEXT STEP WITH STRINGTABLES !
	//*** 28.09.99 SHA ******
	//olMessage = "Si desidera uscire dall applicazione ?";
	//olMessage = "Wollen Sie Anwendung beenden ?";
	olMessage = "Do you want to quit the application ?";
	
	if (MessageBox(olMessage,"DBDPS",MB_ICONQUESTION|MB_YESNO)==IDYES)	
	
		CFrameWnd::OnClose();
}
