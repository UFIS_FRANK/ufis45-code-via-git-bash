// Machine generated IDispatch wrapper class(es) created with ClassWizard
/////////////////////////////////////////////////////////////////////////////
// _IBcPrxyEvents wrapper class

class _IBcPrxyEvents : public COleDispatchDriver
{
public:
	_IBcPrxyEvents() {}		// Calls COleDispatchDriver default constructor
	_IBcPrxyEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	_IBcPrxyEvents(const _IBcPrxyEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	// method 'OnBcReceive' not emitted because of invalid return type or parameter type
};
/////////////////////////////////////////////////////////////////////////////
// IBcPrxy wrapper class

class IBcPrxy : public COleDispatchDriver
{
public:
	IBcPrxy() {}		// Calls COleDispatchDriver default constructor
	IBcPrxy(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IBcPrxy(const IBcPrxy& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	void RegisterObject(LPCTSTR table, LPCTSTR command, long ownBc, LPCTSTR appl);
	void UnregisterObject(LPCTSTR table, LPCTSTR command);
	CString GetVersion();
	void SetSpoolOn();
	void SetSpoolOff();
	void GetNextBufferdBC(BSTR* pReqId, BSTR* pDest1, BSTR* pDest2, BSTR* pCmd, BSTR* pObject, BSTR* pSeq, BSTR* pTws, BSTR* pTwe, BSTR* pSelection, BSTR* pFields, BSTR* pData, BSTR* pBcNum);
	void SetFilterRange(LPCTSTR table, LPCTSTR sFromField, LPCTSTR sToField, LPCTSTR sFromValue, LPCTSTR sToValue);
	CString GetBuildDate();
};
