# Microsoft Developer Studio Project File - Name="_Standard_Wrapper" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=_Standard_Wrapper - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "_Standard_Wrapper.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "_Standard_Wrapper.mak" CFG="_Standard_Wrapper - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "_Standard_Wrapper - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "_Standard_Wrapper - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "_Standard_Wrapper - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "_Standard_Wrapper - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ  /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /GZ   /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "_Standard_Wrapper - Win32 Release"
# Name "_Standard_Wrapper - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\_Standard_Wrapper.cpp
# End Source File
# Begin Source File

SOURCE=.\_Standard_Wrapper.odl
# End Source File
# Begin Source File

SOURCE=.\_Standard_Wrapper.rc
# End Source File
# Begin Source File

SOURCE=.\_Standard_WrapperDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\aatlogin.cpp
# End Source File
# Begin Source File

SOURCE=.\bccomclient.cpp
# End Source File
# Begin Source File

SOURCE=.\bccomserver.cpp
# End Source File
# Begin Source File

SOURCE=.\bcproxy.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgProxy.cpp
# End Source File
# Begin Source File

SOURCE=.\dtab.cpp
# End Source File
# Begin Source File

SOURCE=.\ducom.cpp
# End Source File
# Begin Source File

SOURCE=.\dufiscom.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\tab.cpp
# End Source File
# Begin Source File

SOURCE=.\trafficlight.cpp
# End Source File
# Begin Source File

SOURCE=.\ucom.cpp
# End Source File
# Begin Source File

SOURCE=.\ufiscom.cpp
# End Source File
# Begin Source File

SOURCE=.\ugantt.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\_Standard_Wrapper.h
# End Source File
# Begin Source File

SOURCE=.\_Standard_WrapperDlg.h
# End Source File
# Begin Source File

SOURCE=.\aatlogin.h
# End Source File
# Begin Source File

SOURCE=.\bccomclient.h
# End Source File
# Begin Source File

SOURCE=.\bccomserver.h
# End Source File
# Begin Source File

SOURCE=.\bcproxy.h
# End Source File
# Begin Source File

SOURCE=.\DlgProxy.h
# End Source File
# Begin Source File

SOURCE=.\dtab.h
# End Source File
# Begin Source File

SOURCE=.\ducom.h
# End Source File
# Begin Source File

SOURCE=.\dufiscom.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\tab.h
# End Source File
# Begin Source File

SOURCE=.\trafficlight.h
# End Source File
# Begin Source File

SOURCE=.\ucom.h
# End Source File
# Begin Source File

SOURCE=.\ufiscom.h
# End Source File
# Begin Source File

SOURCE=.\ugantt.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\_Standard_Wrapper.ico
# End Source File
# Begin Source File

SOURCE=.\res\_Standard_Wrapper.rc2
# End Source File
# End Group
# Begin Source File

SOURCE=.\_Standard_Wrapper.reg
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
# Section _Standard_Wrapper : {BB73C4E9-3E22-4D7E-8EE3-A1BFA9A639FE}
# 	2:5:Class:CAatLogin
# 	2:10:HeaderFile:aatlogin.h
# 	2:8:ImplFile:aatlogin.cpp
# End Section
# Section _Standard_Wrapper : {EA6DE32C-D8C1-474D-96DD-90ADDFA68767}
# 	2:21:DefaultSinkHeaderFile:bccomclient.h
# 	2:16:DefaultSinkClass:CBCComClient
# End Section
# Section _Standard_Wrapper : {A2F31E95-C74F-11D3-A251-00500437F607}
# 	2:21:DefaultSinkHeaderFile:ufiscom.h
# 	2:16:DefaultSinkClass:CUfisCom
# End Section
# Section _Standard_Wrapper : {64E8E383-05E2-11D2-9B1D-9B0630BC8F12}
# 	2:5:Class:CTAB
# 	2:10:HeaderFile:tab.h
# 	2:8:ImplFile:tab.cpp
# End Section
# Section _Standard_Wrapper : {6782E136-3223-11D4-996A-0000863DE95C}
# 	2:5:Class:CUGantt
# 	2:10:HeaderFile:ugantt.h
# 	2:8:ImplFile:ugantt.cpp
# End Section
# Section _Standard_Wrapper : {F9478336-76BA-11D6-8067-0001022205E4}
# 	2:21:DefaultSinkHeaderFile:ucom.h
# 	2:16:DefaultSinkClass:CUCom
# End Section
# Section _Standard_Wrapper : {A605F53D-67AD-4B35-B973-C7095D670128}
# 	2:5:Class:CBCComClient
# 	2:10:HeaderFile:bccomclient.h
# 	2:8:ImplFile:bccomclient.cpp
# End Section
# Section _Standard_Wrapper : {A2F31E93-C74F-11D3-A251-00500437F607}
# 	2:5:Class:CUfisCom
# 	2:10:HeaderFile:ufiscom.h
# 	2:8:ImplFile:ufiscom.cpp
# End Section
# Section _Standard_Wrapper : {64E8E385-05E2-11D2-9B1D-9B0630BC8F12}
# 	2:21:DefaultSinkHeaderFile:tab.h
# 	2:16:DefaultSinkClass:CTAB
# End Section
# Section _Standard_Wrapper : {6782E138-3223-11D4-996A-0000863DE95C}
# 	2:21:DefaultSinkHeaderFile:ugantt.h
# 	2:16:DefaultSinkClass:CUGantt
# End Section
# Section _Standard_Wrapper : {F9478334-76BA-11D6-8067-0001022205E4}
# 	2:5:Class:CUCom
# 	2:10:HeaderFile:ucom.h
# 	2:8:ImplFile:ucom.cpp
# End Section
# Section _Standard_Wrapper : {17D7E209-2DEC-48BC-83E2-3E9F43B90D59}
# 	2:21:DefaultSinkHeaderFile:aatlogin.h
# 	2:16:DefaultSinkClass:CAatLogin
# End Section
