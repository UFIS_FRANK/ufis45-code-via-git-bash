// Machine generated IDispatch wrapper class(es) created with ClassWizard

#include "stdafx.h"
#include "dtab.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// _DTAB properties

long _DTAB::GetColumns()
{
	long result;
	GetProperty(0x1, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetColumns(long propVal)
{
	SetProperty(0x1, VT_I4, propVal);
}

long _DTAB::GetLines()
{
	long result;
	GetProperty(0x2, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetLines(long propVal)
{
	SetProperty(0x2, VT_I4, propVal);
}

CString _DTAB::GetHeaderString()
{
	CString result;
	GetProperty(0x3, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetHeaderString(LPCTSTR propVal)
{
	SetProperty(0x3, VT_BSTR, propVal);
}

CString _DTAB::GetHeaderLengthString()
{
	CString result;
	GetProperty(0x4, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetHeaderLengthString(LPCTSTR propVal)
{
	SetProperty(0x4, VT_BSTR, propVal);
}

short _DTAB::GetLineHeight()
{
	short result;
	GetProperty(0x5, VT_I2, (void*)&result);
	return result;
}

void _DTAB::SetLineHeight(short propVal)
{
	SetProperty(0x5, VT_I2, propVal);
}

short _DTAB::GetFontSize()
{
	short result;
	GetProperty(0x6, VT_I2, (void*)&result);
	return result;
}

void _DTAB::SetFontSize(short propVal)
{
	SetProperty(0x6, VT_I2, propVal);
}

short _DTAB::GetHeaderFontSize()
{
	short result;
	GetProperty(0x7, VT_I2, (void*)&result);
	return result;
}

void _DTAB::SetHeaderFontSize(short propVal)
{
	SetProperty(0x7, VT_I2, propVal);
}

CString _DTAB::GetFontName()
{
	CString result;
	GetProperty(0x8, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetFontName(LPCTSTR propVal)
{
	SetProperty(0x8, VT_BSTR, propVal);
}

long _DTAB::GetLeftTextOffset()
{
	long result;
	GetProperty(0x9, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetLeftTextOffset(long propVal)
{
	SetProperty(0x9, VT_I4, propVal);
}

BOOL _DTAB::GetVScrollMaster()
{
	BOOL result;
	GetProperty(0xa, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetVScrollMaster(BOOL propVal)
{
	SetProperty(0xa, VT_BOOL, propVal);
}

short _DTAB::GetPostEnterBehavior()
{
	short result;
	GetProperty(0xb, VT_I2, (void*)&result);
	return result;
}

void _DTAB::SetPostEnterBehavior(short propVal)
{
	SetProperty(0xb, VT_I2, propVal);
}

BOOL _DTAB::GetHScrollMaster()
{
	BOOL result;
	GetProperty(0xc, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetHScrollMaster(BOOL propVal)
{
	SetProperty(0xc, VT_BOOL, propVal);
}

CString _DTAB::GetNoFocusColumns()
{
	CString result;
	GetProperty(0x37, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetNoFocusColumns(LPCTSTR propVal)
{
	SetProperty(0x37, VT_BSTR, propVal);
}

CString _DTAB::GetVersion()
{
	CString result;
	GetProperty(0xd, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetVersion(LPCTSTR propVal)
{
	SetProperty(0xd, VT_BSTR, propVal);
}

BOOL _DTAB::GetInplaceEditUpperCase()
{
	BOOL result;
	GetProperty(0xe, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetInplaceEditUpperCase(BOOL propVal)
{
	SetProperty(0xe, VT_BOOL, propVal);
}

long _DTAB::GetGridLineColor()
{
	long result;
	GetProperty(0xf, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetGridLineColor(long propVal)
{
	SetProperty(0xf, VT_I4, propVal);
}

long _DTAB::GetEmptyAreaBackColor()
{
	long result;
	GetProperty(0x10, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetEmptyAreaBackColor(long propVal)
{
	SetProperty(0x10, VT_I4, propVal);
}

CString _DTAB::GetColumnWidthString()
{
	CString result;
	GetProperty(0x11, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetColumnWidthString(LPCTSTR propVal)
{
	SetProperty(0x11, VT_BSTR, propVal);
}

CString _DTAB::GetColumnAlignmentString()
{
	CString result;
	GetProperty(0x12, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetColumnAlignmentString(LPCTSTR propVal)
{
	SetProperty(0x12, VT_BSTR, propVal);
}

long _DTAB::GetSelectBackColor()
{
	long result;
	GetProperty(0x13, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetSelectBackColor(long propVal)
{
	SetProperty(0x13, VT_I4, propVal);
}

long _DTAB::GetSelectTextColor()
{
	long result;
	GetProperty(0x14, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetSelectTextColor(long propVal)
{
	SetProperty(0x14, VT_I4, propVal);
}

long _DTAB::GetEmptyAreaRightColor()
{
	long result;
	GetProperty(0x15, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetEmptyAreaRightColor(long propVal)
{
	SetProperty(0x15, VT_I4, propVal);
}

long _DTAB::GetSelectColumnBackColor()
{
	long result;
	GetProperty(0x16, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetSelectColumnBackColor(long propVal)
{
	SetProperty(0x16, VT_I4, propVal);
}

long _DTAB::GetSelectColumnTextColor()
{
	long result;
	GetProperty(0x17, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetSelectColumnTextColor(long propVal)
{
	SetProperty(0x17, VT_I4, propVal);
}

long _DTAB::GetDisplayTextColor()
{
	long result;
	GetProperty(0x18, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetDisplayTextColor(long propVal)
{
	SetProperty(0x18, VT_I4, propVal);
}

long _DTAB::GetDisplayBackColor()
{
	long result;
	GetProperty(0x19, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetDisplayBackColor(long propVal)
{
	SetProperty(0x19, VT_I4, propVal);
}

BOOL _DTAB::GetMainHeader()
{
	BOOL result;
	GetProperty(0x1a, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetMainHeader(BOOL propVal)
{
	SetProperty(0x1a, VT_BOOL, propVal);
}

BOOL _DTAB::GetMainHeaderOnly()
{
	BOOL result;
	GetProperty(0x1b, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetMainHeaderOnly(BOOL propVal)
{
	SetProperty(0x1b, VT_BOOL, propVal);
}

BOOL _DTAB::GetShowRowSelection()
{
	BOOL result;
	GetProperty(0x1c, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetShowRowSelection(BOOL propVal)
{
	SetProperty(0x1c, VT_BOOL, propVal);
}

CString _DTAB::GetSetLocaleLanguageString()
{
	CString result;
	GetProperty(0x1d, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetSetLocaleLanguageString(LPCTSTR propVal)
{
	SetProperty(0x1d, VT_BSTR, propVal);
}

CString _DTAB::GetCedaCurrentApplication()
{
	CString result;
	GetProperty(0x1e, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetCedaCurrentApplication(LPCTSTR propVal)
{
	SetProperty(0x1e, VT_BSTR, propVal);
}

CString _DTAB::GetCedaPort()
{
	CString result;
	GetProperty(0x1f, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetCedaPort(LPCTSTR propVal)
{
	SetProperty(0x1f, VT_BSTR, propVal);
}

CString _DTAB::GetCedaHopo()
{
	CString result;
	GetProperty(0x20, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetCedaHopo(LPCTSTR propVal)
{
	SetProperty(0x20, VT_BSTR, propVal);
}

CString _DTAB::GetCedaServerName()
{
	CString result;
	GetProperty(0x21, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetCedaServerName(LPCTSTR propVal)
{
	SetProperty(0x21, VT_BSTR, propVal);
}

CString _DTAB::GetCedaTabext()
{
	CString result;
	GetProperty(0x22, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetCedaTabext(LPCTSTR propVal)
{
	SetProperty(0x22, VT_BSTR, propVal);
}

CString _DTAB::GetCedaUser()
{
	CString result;
	GetProperty(0x23, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetCedaUser(LPCTSTR propVal)
{
	SetProperty(0x23, VT_BSTR, propVal);
}

CString _DTAB::GetCedaWorkstation()
{
	CString result;
	GetProperty(0x24, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetCedaWorkstation(LPCTSTR propVal)
{
	SetProperty(0x24, VT_BSTR, propVal);
}

short _DTAB::GetCedaSendTimeout()
{
	short result;
	GetProperty(0x25, VT_I2, (void*)&result);
	return result;
}

void _DTAB::SetCedaSendTimeout(short propVal)
{
	SetProperty(0x25, VT_I2, propVal);
}

short _DTAB::GetCedaReceiveTimeout()
{
	short result;
	GetProperty(0x26, VT_I2, (void*)&result);
	return result;
}

void _DTAB::SetCedaReceiveTimeout(short propVal)
{
	SetProperty(0x26, VT_I2, propVal);
}

CString _DTAB::GetCedaRecordSeparator()
{
	CString result;
	GetProperty(0x27, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetCedaRecordSeparator(LPCTSTR propVal)
{
	SetProperty(0x27, VT_BSTR, propVal);
}

CString _DTAB::GetCedaIdentifier()
{
	CString result;
	GetProperty(0x28, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetCedaIdentifier(LPCTSTR propVal)
{
	SetProperty(0x28, VT_BSTR, propVal);
}

CString _DTAB::GetHeaderAlignmentString()
{
	CString result;
	GetProperty(0x29, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetHeaderAlignmentString(LPCTSTR propVal)
{
	SetProperty(0x29, VT_BSTR, propVal);
}

OLE_HANDLE _DTAB::GetHWnd()
{
	OLE_HANDLE result;
	GetProperty(DISPID_HWND, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetHWnd(OLE_HANDLE propVal)
{
	SetProperty(DISPID_HWND, VT_I4, propVal);
}

BOOL _DTAB::GetAutoSizeByHeader()
{
	BOOL result;
	GetProperty(0x2a, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetAutoSizeByHeader(BOOL propVal)
{
	SetProperty(0x2a, VT_BOOL, propVal);
}

BOOL _DTAB::GetInplaceEditSendKeyEvents()
{
	BOOL result;
	GetProperty(0x2b, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetInplaceEditSendKeyEvents(BOOL propVal)
{
	SetProperty(0x2b, VT_BOOL, propVal);
}

long _DTAB::GetComboMinWidth()
{
	long result;
	GetProperty(0x38, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetComboMinWidth(long propVal)
{
	SetProperty(0x38, VT_I4, propVal);
}

CString _DTAB::GetLogicalFieldList()
{
	CString result;
	GetProperty(0x2c, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetLogicalFieldList(LPCTSTR propVal)
{
	SetProperty(0x2c, VT_BSTR, propVal);
}

CString _DTAB::GetCedaFieldSeparator()
{
	CString result;
	GetProperty(0x2d, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetCedaFieldSeparator(LPCTSTR propVal)
{
	SetProperty(0x2d, VT_BSTR, propVal);
}

CString _DTAB::GetMyTag()
{
	CString result;
	GetProperty(0x2e, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetMyTag(LPCTSTR propVal)
{
	SetProperty(0x2e, VT_BSTR, propVal);
}

CString _DTAB::GetMyName()
{
	CString result;
	GetProperty(0x2f, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetMyName(LPCTSTR propVal)
{
	SetProperty(0x2f, VT_BSTR, propVal);
}

BOOL _DTAB::GetEnabled()
{
	BOOL result;
	GetProperty(DISPID_ENABLED, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetEnabled(BOOL propVal)
{
	SetProperty(DISPID_ENABLED, VT_BOOL, propVal);
}

CString _DTAB::GetBuildDate()
{
	CString result;
	GetProperty(0x30, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetBuildDate(LPCTSTR propVal)
{
	SetProperty(0x30, VT_BSTR, propVal);
}

BOOL _DTAB::GetDefaultCursor()
{
	BOOL result;
	GetProperty(0x31, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetDefaultCursor(BOOL propVal)
{
	SetProperty(0x31, VT_BOOL, propVal);
}

BOOL _DTAB::GetLifeStyle()
{
	BOOL result;
	GetProperty(0x32, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetLifeStyle(BOOL propVal)
{
	SetProperty(0x32, VT_BOOL, propVal);
}

BOOL _DTAB::GetCursorLifeStyle()
{
	BOOL result;
	GetProperty(0x33, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetCursorLifeStyle(BOOL propVal)
{
	SetProperty(0x33, VT_BOOL, propVal);
}

BOOL _DTAB::GetColumnSelectionLifeStyle()
{
	BOOL result;
	GetProperty(0x34, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetColumnSelectionLifeStyle(BOOL propVal)
{
	SetProperty(0x34, VT_BOOL, propVal);
}

BOOL _DTAB::GetSortOrderASC()
{
	BOOL result;
	GetProperty(0x35, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetSortOrderASC(BOOL propVal)
{
	SetProperty(0x35, VT_BOOL, propVal);
}

long _DTAB::GetCurrentSortColumn()
{
	long result;
	GetProperty(0x36, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetCurrentSortColumn(long propVal)
{
	SetProperty(0x36, VT_I4, propVal);
}

BOOL _DTAB::GetComboOwnerDraw()
{
	BOOL result;
	GetProperty(0xd7, VT_BOOL, (void*)&result);
	return result;
}

void _DTAB::SetComboOwnerDraw(BOOL propVal)
{
	SetProperty(0xd7, VT_BOOL, propVal);
}

LPDISPATCH _DTAB::GetUCom()
{
	LPDISPATCH result;
	GetProperty(0xd8, VT_DISPATCH, (void*)&result);
	return result;
}

void _DTAB::SetUCom(LPDISPATCH propVal)
{
	SetProperty(0xd8, VT_DISPATCH, propVal);
}

CString _DTAB::GetSqlAccessMethod()
{
	CString result;
	GetProperty(0xd9, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetSqlAccessMethod(LPCTSTR propVal)
{
	SetProperty(0xd9, VT_BSTR, propVal);
}

long _DTAB::GetCedaPacketSize()
{
	long result;
	GetProperty(0xda, VT_I4, (void*)&result);
	return result;
}

void _DTAB::SetCedaPacketSize(long propVal)
{
	SetProperty(0xda, VT_I4, propVal);
}

CString _DTAB::GetCedaErrorSimulation()
{
	CString result;
	GetProperty(0xdb, VT_BSTR, (void*)&result);
	return result;
}

void _DTAB::SetCedaErrorSimulation(LPCTSTR propVal)
{
	SetProperty(0xdb, VT_BSTR, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// _DTAB operations

void _DTAB::SetLineCount(long ipCount)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ipCount);
}

void _DTAB::SetColumnCount(long ipCount)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ipCount);
}

void _DTAB::SetHeaderText(LPCTSTR HeaderText)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x3b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 HeaderText);
}

void _DTAB::ResetContent()
{
	InvokeHelper(0x3c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _DTAB::InsertTextLine(LPCTSTR LineText, BOOL bpRedraw)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL;
	InvokeHelper(0x3d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineText, bpRedraw);
}

long _DTAB::GetLineCount()
{
	long result;
	InvokeHelper(0x3e, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void _DTAB::SetLineColor(long LineNo, long TextColor, long BackColor)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4;
	InvokeHelper(0x3f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, TextColor, BackColor);
}

void _DTAB::InsertTextLineAt(long LineNo, LPCTSTR LineText, BOOL bpRedraw)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BOOL;
	InvokeHelper(0x40, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, LineText, bpRedraw);
}

short _DTAB::DeleteLine(long LineNo)
{
	short result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x41, DISPATCH_METHOD, VT_I2, (void*)&result, parms,
		LineNo);
	return result;
}

long _DTAB::GetCurrentSelected()
{
	long result;
	InvokeHelper(0x42, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString _DTAB::GetLineValues(long LineNo)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x43, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		LineNo);
	return result;
}

CString _DTAB::GetLinesByTextColor(long Color)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x44, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		Color);
	return result;
}

CString _DTAB::GetLinesByBackColor(long Color)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x45, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		Color);
	return result;
}

void _DTAB::Init()
{
	InvokeHelper(0x46, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

CString _DTAB::GetColumnValue(long LineNo, long ColNo)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x47, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		LineNo, ColNo);
	return result;
}

void _DTAB::SetColumnValue(long LineNo, long ColNo, LPCTSTR Value)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR;
	InvokeHelper(0x48, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo, Value);
}

CString _DTAB::GetLinesByColor(long BackColor, long TextColor)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x49, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		BackColor, TextColor);
	return result;
}

void _DTAB::UpdateTextLine(long LineNo, LPCTSTR Values, BOOL Redraw)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BOOL;
	InvokeHelper(0x4a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, Values, Redraw);
}

void _DTAB::RedrawTab()
{
	InvokeHelper(0x4b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _DTAB::SetCurrentSelection(long LineNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo);
}

void _DTAB::SetFieldSeparator(LPCTSTR NewSeparator)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 NewSeparator);
}

void _DTAB::LockScroll(BOOL Locked)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x4e, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Locked);
}

CString _DTAB::GetLinesByColumnValue(long ColNo, LPCTSTR Value, short CompareMethod)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_I2;
	InvokeHelper(0x4f, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		ColNo, Value, CompareMethod);
	return result;
}

void _DTAB::GetLineColor(long LineNo, long* txtColor, long* bkColor)
{
	static BYTE parms[] =
		VTS_I4 VTS_PI4 VTS_PI4;
	InvokeHelper(0x50, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, txtColor, bkColor);
}

CString _DTAB::GetSelectedColumns()
{
	CString result;
	InvokeHelper(0x51, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

void _DTAB::EnableHeaderSelection(BOOL bEnable)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x52, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 bEnable);
}

void _DTAB::EnableInlineEdit(BOOL IE_Enable)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x53, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 IE_Enable);
}

void _DTAB::EnableInlineMoveInternal(BOOL IE_Move)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x54, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 IE_Move);
}

long _DTAB::InsertBuffer(LPCTSTR StrBuffer, LPCTSTR LineSeparator)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x55, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		StrBuffer, LineSeparator);
	return result;
}

void _DTAB::ShowHorzScroller(BOOL Show)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x56, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Show);
}

void _DTAB::EnableHeaderSizing(BOOL bEnable)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x57, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 bEnable);
}

void _DTAB::SetTabFontBold(BOOL bold)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x58, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 bold);
}

void _DTAB::ColSelectionRemoveAll()
{
	InvokeHelper(0x59, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _DTAB::ColSelectionRemove(long ColNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo);
}

void _DTAB::ColSelectionAdd(long ColNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo);
}

void _DTAB::OnVScrollTo(long LineNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo);
}

long _DTAB::GetColumnCount()
{
	long result;
	InvokeHelper(0x5d, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString _DTAB::GetHeaderText()
{
	CString result;
	InvokeHelper(0x5e, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

void _DTAB::ShowVertScroller(BOOL Show)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x5f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Show);
}

void _DTAB::OnHScrollTo(long ColNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x60, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo);
}

void _DTAB::SetRegExFormatString(LPCTSTR FormatString)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x61, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 FormatString);
}

BOOL _DTAB::CheckRegExMatchCell(long LineNo, long ColNo)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x62, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LineNo, ColNo);
	return result;
}

CString _DTAB::CheckRegExMatchLine(long LineNo, BOOL TrueFalse)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_BOOL;
	InvokeHelper(0x63, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		LineNo, TrueFalse);
	return result;
}

void _DTAB::SetInplaceEdit(long LineNo, long ColNo, BOOL bAssumeValue)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BOOL;
	InvokeHelper(0x64, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo, bAssumeValue);
}

long _DTAB::GetVScrollPos()
{
	long result;
	InvokeHelper(0x65, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long _DTAB::GetHScrollPos()
{
	long result;
	InvokeHelper(0x66, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void _DTAB::Sort(LPCTSTR ColNo, BOOL bAscending, BOOL bCaseSensitive)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL VTS_BOOL;
	InvokeHelper(0x67, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo, bAscending, bCaseSensitive);
}

CString _DTAB::SelectDistinct(LPCTSTR Cols, LPCTSTR StrPrefix, LPCTSTR StrAppendix, LPCTSTR StrDelimiter, BOOL Distinct)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL;
	InvokeHelper(0x68, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		Cols, StrPrefix, StrAppendix, StrDelimiter, Distinct);
	return result;
}

long _DTAB::GetLineStatusValue(long LineNo)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x69, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		LineNo);
	return result;
}

CString _DTAB::GetLinesByStatusValue(long Value, short CompareMethod)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_I2;
	InvokeHelper(0x6a, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		Value, CompareMethod);
	return result;
}

void _DTAB::SetLineStatusValue(long LineNo, long Value)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x6b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, Value);
}

void _DTAB::SetMainHeaderValues(LPCTSTR Ranges, LPCTSTR Values, LPCTSTR Colors)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x6c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Ranges, Values, Colors);
}

CString _DTAB::GetMainHeaderRanges()
{
	CString result;
	InvokeHelper(0x6d, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

CString _DTAB::GetMainHeaderValues()
{
	CString result;
	InvokeHelper(0x6e, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

CString _DTAB::GetMainHeaderColors()
{
	CString result;
	InvokeHelper(0x6f, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

CString _DTAB::GetNextLineByColumnValue(long ColNo, LPCTSTR Value, short CompareMethod)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_I2;
	InvokeHelper(0x70, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		ColNo, Value, CompareMethod);
	return result;
}

void _DTAB::ResetCellProperty(long LineNo, long ColNo)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x71, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo);
}

BOOL _DTAB::SetCellProperty(long LineNo, long ColNo, LPCTSTR ObjID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR;
	InvokeHelper(0x72, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LineNo, ColNo, ObjID);
	return result;
}

void _DTAB::CreateCellObj(LPCTSTR ObjID, long BackColor, long TextColor, short TextSize, BOOL bItalic, BOOL bUnderline, BOOL bBold, short CharSet, LPCTSTR FontName)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4 VTS_I2 VTS_BOOL VTS_BOOL VTS_BOOL VTS_I2 VTS_BSTR;
	InvokeHelper(0x73, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ObjID, BackColor, TextColor, TextSize, bItalic, bUnderline, bBold, CharSet, FontName);
}

CString _DTAB::GetLinesByColumnValues(long ColNo, LPCTSTR Values, short CompareMethod)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_I2;
	InvokeHelper(0x74, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		ColNo, Values, CompareMethod);
	return result;
}

void _DTAB::CreateCellBitmpapObj(LPCTSTR ObjID, LPCTSTR BitmapFile)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x75, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ObjID, BitmapFile);
}

BOOL _DTAB::SetCellBitmapProperty(long LineNo, long ColNo, LPCTSTR ObjID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR;
	InvokeHelper(0x76, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LineNo, ColNo, ObjID);
	return result;
}

void _DTAB::ResetCellBitmapProperty(long LineNo, long ColNo)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x77, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo);
}

CString _DTAB::GetBitmapFileName(long LineNo, long ColNo)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x78, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		LineNo, ColNo);
	return result;
}

void _DTAB::ResetCellProperties(long LineNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x79, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo);
}

void _DTAB::ResetBitmapProperties(long LineNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo);
}

CString _DTAB::GetLinesByMultipleColumnValue(LPCTSTR Columns, LPCTSTR Values, short CompareMethod)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I2;
	InvokeHelper(0x7b, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		Columns, Values, CompareMethod);
	return result;
}

BOOL _DTAB::SetColumnProperty(long ColNo, LPCTSTR ObjID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x7c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ColNo, ObjID);
	return result;
}

void _DTAB::ResetColumnProperty(long ColNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo);
}

BOOL _DTAB::SetHeaderFont(short TextSize, BOOL bItalic, BOOL bUnderline, BOOL bBold, short CharSet, LPCTSTR FontName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I2 VTS_BOOL VTS_BOOL VTS_BOOL VTS_I2 VTS_BSTR;
	InvokeHelper(0x7e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		TextSize, bItalic, bUnderline, bBold, CharSet, FontName);
	return result;
}

BOOL _DTAB::SetHeaderColors(long BackColor, long FontColor)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x7f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		BackColor, FontColor);
	return result;
}

BOOL _DTAB::SetMainHeaderFont(short TextSize, BOOL bItalic, BOOL bUnderline, BOOL bBold, short CharSet, LPCTSTR FontName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I2 VTS_BOOL VTS_BOOL VTS_BOOL VTS_I2 VTS_BSTR;
	InvokeHelper(0x80, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		TextSize, bItalic, bUnderline, bBold, CharSet, FontName);
	return result;
}

void _DTAB::TimerSetValue(long LineNo, short Value)
{
	static BYTE parms[] =
		VTS_I4 VTS_I2;
	InvokeHelper(0x81, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, Value);
}

void _DTAB::TimerCheck()
{
	InvokeHelper(0x82, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _DTAB::PrintTabShowDialog(LPCTSTR sDocTitle, LPCTSTR sDate)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x83, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sDocTitle, sDate);
}

void _DTAB::PrintTab(long lhDC, LPCTSTR sDocTitle, LPCTSTR sDate)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BSTR;
	InvokeHelper(0x84, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 lhDC, sDocTitle, sDate);
}

void _DTAB::PrintSetPageHeaderHeight(long lHeight)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x85, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 lHeight);
}

long _DTAB::PrintGetPageHeaderHeight()
{
	long result;
	InvokeHelper(0x86, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void _DTAB::PrintSetPageFooterHeight(long lHeight)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x87, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 lHeight);
}

long _DTAB::PrintGetPageFooterHeight()
{
	long result;
	InvokeHelper(0x88, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void _DTAB::PrintSetDatePosition(LPCTSTR sLocation, LPCTSTR sAlignment)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x89, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sLocation, sAlignment);
}

void _DTAB::PrintGetDatePosition(BSTR* psLocation, BSTR* psAlignment)
{
	static BYTE parms[] =
		VTS_PBSTR VTS_PBSTR;
	InvokeHelper(0x8a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 psLocation, psAlignment);
}

void _DTAB::PrintSetTitlePosition(LPCTSTR sLocation, LPCTSTR sAlignment)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x8b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sLocation, sAlignment);
}

void _DTAB::PrintGetTitlePosition(BSTR* psLocation, BSTR* psAlignment)
{
	static BYTE parms[] =
		VTS_PBSTR VTS_PBSTR;
	InvokeHelper(0x8c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 psLocation, psAlignment);
}

void _DTAB::PrintSetPageEnumeration(LPCTSTR sLoaction, LPCTSTR sAlignment, long lStyle)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x8d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sLoaction, sAlignment, lStyle);
}

void _DTAB::PrintGetPageEnumeration(BSTR* psLocation, BSTR* psAlignment, long* plStyle)
{
	static BYTE parms[] =
		VTS_PBSTR VTS_PBSTR VTS_PI4;
	InvokeHelper(0x8e, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 psLocation, psAlignment, plStyle);
}

void _DTAB::PrintSetLogo(LPCTSTR sLocation, LPCTSTR sAlignment, LPCTSTR sPath)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x8f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sLocation, sAlignment, sPath);
}

void _DTAB::PrintGetLogo(BSTR* psLocation, BSTR* psAlignment, BSTR* psPath)
{
	static BYTE parms[] =
		VTS_PBSTR VTS_PBSTR VTS_PBSTR;
	InvokeHelper(0x90, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 psLocation, psAlignment, psPath);
}

void _DTAB::PrintSetSeparatorLineTop(long lDistance, long lWidth)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x91, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 lDistance, lWidth);
}

void _DTAB::PrintGetSeparatorLineTop(long* plDistance, long* plWidth)
{
	static BYTE parms[] =
		VTS_PI4 VTS_PI4;
	InvokeHelper(0x92, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 plDistance, plWidth);
}

void _DTAB::PrintSetSeparatorLineBottom(long lDistance, long lWidth)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x93, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 lDistance, lWidth);
}

void _DTAB::PrintGetSeparatorLineBottom(long* plDistance, long* plWidth)
{
	static BYTE parms[] =
		VTS_PI4 VTS_PI4;
	InvokeHelper(0x94, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 plDistance, plWidth);
}

void _DTAB::PrintSetFitToPage(BOOL bFitToPage)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x95, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 bFitToPage);
}

BOOL _DTAB::PrintGetFitToPage()
{
	BOOL result;
	InvokeHelper(0x96, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void _DTAB::PrintSetColOrderString(LPCTSTR sColString)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x97, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sColString);
}

CString _DTAB::PrintGetColOrderString()
{
	CString result;
	InvokeHelper(0x98, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

void _DTAB::PrintSetMargins(long lTop, long lBottom, long lLeft, long lRight)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_I4;
	InvokeHelper(0x99, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 lTop, lBottom, lLeft, lRight);
}

void _DTAB::PrintGetMargins(long* plTop, long* plBottom, long* plLeft, long* plRight)
{
	static BYTE parms[] =
		VTS_PI4 VTS_PI4 VTS_PI4 VTS_PI4;
	InvokeHelper(0x9a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 plTop, plBottom, plLeft, plRight);
}

BOOL _DTAB::PrintSetPageTitleFont(short TextSize, BOOL bItalic, BOOL bUnderline, BOOL bBold, short CharSet, LPCTSTR FontName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I2 VTS_BOOL VTS_BOOL VTS_BOOL VTS_I2 VTS_BSTR;
	InvokeHelper(0x9b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		TextSize, bItalic, bUnderline, bBold, CharSet, FontName);
	return result;
}

void _DTAB::PrintRepeatStatus(short sRepeatStatus)
{
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x9c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sRepeatStatus);
}

CString _DTAB::GetBuffer(long lFromLine, long lToLine, LPCTSTR LineSeparator)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR;
	InvokeHelper(0x9d, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		lFromLine, lToLine, LineSeparator);
	return result;
}

BOOL _DTAB::CedaAction(LPCTSTR Cmd, LPCTSTR DBTable, LPCTSTR DBFields, LPCTSTR DBData, LPCTSTR DBWhere)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x9e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Cmd, DBTable, DBFields, DBData, DBWhere);
	return result;
}

CString _DTAB::GetLastCedaError()
{
	CString result;
	InvokeHelper(0x9f, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL _DTAB::WriteToFile(LPCTSTR FileName, BOOL WithBasicInfo)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL;
	InvokeHelper(0xa0, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		FileName, WithBasicInfo);
	return result;
}

BOOL _DTAB::ReadFromFile(LPCTSTR FileName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xa1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		FileName);
	return result;
}

void _DTAB::DateTimeSetColumn(long ColNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo);
}

void _DTAB::DateTimeResetColumn(long ColNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa3, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo);
}

void _DTAB::DateTimeSetInputFormatString(long ColNo, LPCTSTR FormatString)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0xa4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo, FormatString);
}

CString _DTAB::DateTimeGetInputFormatString(long ColNo)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa5, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		ColNo);
	return result;
}

void _DTAB::DateTimeSetOutputFormatString(long ColNo, LPCTSTR FormatString)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0xa6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo, FormatString);
}

CString _DTAB::DateTimeGetOutputFormatString(long ColNo)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa7, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		ColNo);
	return result;
}

void _DTAB::DateTimeSetUTCOffsetMinutes(long ColNo, long OffsetMinutes)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0xa8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo, OffsetMinutes);
}

long _DTAB::DateTimeGetUTCOffsetMinutes(long ColNo)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa9, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ColNo);
	return result;
}

void _DTAB::DateTimeSetMonthNames(LPCTSTR MonthNames)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xaa, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 MonthNames);
}

CString _DTAB::DateTimeGetMonthNames()
{
	CString result;
	InvokeHelper(0xab, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL _DTAB::WriteToFileWithProperties(LPCTSTR FileName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xac, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		FileName);
	return result;
}

BOOL _DTAB::ReadFromFileWithProperties(LPCTSTR FileName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xad, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		FileName);
	return result;
}

void _DTAB::SetLineTag(long LineNo, LPCTSTR Values)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0xae, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, Values);
}

CString _DTAB::GetLineTag(long LineNo)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xaf, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		LineNo);
	return result;
}

CString _DTAB::GetLineTagKeyItem(long LineNo, LPCTSTR KeyItem, LPCTSTR StartSeparator, LPCTSTR EndSeparator)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0xb0, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		LineNo, KeyItem, StartSeparator, EndSeparator);
	return result;
}

void _DTAB::AutoSizeColumns()
{
	InvokeHelper(0xb1, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL _DTAB::SetColumnBoolProperty(long ColNo, LPCTSTR TrueValue, LPCTSTR FalseValue)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BSTR;
	InvokeHelper(0xb2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ColNo, TrueValue, FalseValue);
	return result;
}

void _DTAB::ResetColumnBoolProperty(long ColNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xb3, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo);
}

void _DTAB::CreateDecorationObject(LPCTSTR ID, LPCTSTR LocationList, LPCTSTR PixelList, LPCTSTR ColorList)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0xb4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ID, LocationList, PixelList, ColorList);
}

void _DTAB::SetDecorationObject(long LineNo, long ColNo, LPCTSTR ID)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR;
	InvokeHelper(0xb5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo, ID);
}

void _DTAB::ResetCellDecoration(long LineNo, long ColNo)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0xb6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo);
}

void _DTAB::ResetLineDecorations(long LineNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xb7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo);
}

void _DTAB::ComboAddTextLines(LPCTSTR ComboID, LPCTSTR Buffer, LPCTSTR LineSeparator)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0xb8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ComboID, Buffer, LineSeparator);
}

void _DTAB::ComboSetGridStyle(LPCTSTR ComboID, short PixelWidth, long GridColor)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I2 VTS_I4;
	InvokeHelper(0xb9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ComboID, PixelWidth, GridColor);
}

void _DTAB::ComboDeleteLine(LPCTSTR ComboID, long LineNo)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0xba, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ComboID, LineNo);
}

void _DTAB::ComboSetLineValues(LPCTSTR ComboID, long LineNo, LPCTSTR Values)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0xbb, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ComboID, LineNo, Values);
}

void _DTAB::ComboSetResultColumn(LPCTSTR ComboID, long ColNo)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0xbc, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ComboID, ColNo);
}

void _DTAB::SetComboColumn(LPCTSTR ComboID, long ColNo)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0xbd, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ComboID, ColNo);
}

void _DTAB::ComboSetLineColors(LPCTSTR ComboID, long LineNo, long TextColor, long BackColor)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4 VTS_I4;
	InvokeHelper(0xbe, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ComboID, LineNo, TextColor, BackColor);
}

void _DTAB::ComboSetColumnLengthString(LPCTSTR ComboID, LPCTSTR LenString)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0xbf, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ComboID, LenString);
}

void _DTAB::CreateComboObject(LPCTSTR ComboID, long TabColumn, long ComboColumns, LPCTSTR Style, long ListboxWidth)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4 VTS_BSTR VTS_I4;
	InvokeHelper(0xc0, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ComboID, TabColumn, ComboColumns, Style, ListboxWidth);
}

void _DTAB::ComboResetContent(LPCTSTR ComboID)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xc1, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ComboID);
}

void _DTAB::ComboResetObject(LPCTSTR ComboID)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xc2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ComboID);
}

BOOL _DTAB::CedaFreeCommand(LPCTSTR strCommand)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xc3, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		strCommand);
	return result;
}

void _DTAB::SetBoolPropertyReadOnly(long ColNo, BOOL SetResetValue)
{
	static BYTE parms[] =
		VTS_I4 VTS_BOOL;
	InvokeHelper(0xc4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo, SetResetValue);
}

CString _DTAB::GetColumnValues(long LineNo, LPCTSTR ColumnList)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0xc5, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		LineNo, ColumnList);
	return result;
}

void _DTAB::ComboShow(LPCTSTR ComboID, BOOL Show)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL;
	InvokeHelper(0xc6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ComboID, Show);
}

CString _DTAB::GetFieldSeparator()
{
	CString result;
	InvokeHelper(0xc7, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

void _DTAB::ResetConfiguration()
{
	InvokeHelper(0xc8, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _DTAB::IndexCreate(LPCTSTR Name, long ColNo)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0xc9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Name, ColNo);
}

void _DTAB::IndexDestroy(LPCTSTR IndexName)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xca, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 IndexName);
}

CString _DTAB::GetLinesByIndexValue(LPCTSTR IndexName, LPCTSTR SearchValue, long CompareMethod)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0xcb, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		IndexName, SearchValue, CompareMethod);
	return result;
}

long _DTAB::IndexGetDataLineNo(LPCTSTR IndexName, long IndexLine)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0xcc, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		IndexName, IndexLine);
	return result;
}

void _DTAB::Refresh()
{
	InvokeHelper(DISPID_REFRESH, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

CString _DTAB::GetFieldValue(long LineNo, LPCTSTR FieldName)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0xcd, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		LineNo, FieldName);
	return result;
}

void _DTAB::SetUniqueFields(LPCTSTR FieldList)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xce, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 FieldList);
}

void _DTAB::SetInternalLineBuffer(BOOL blOn)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xcf, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 blOn);
}

long _DTAB::GetNextResultLine()
{
	long result;
	InvokeHelper(0xd0, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void _DTAB::DateTimeSetColumnFormat(long ColNo, LPCTSTR InputFormat, LPCTSTR OutputFormat)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BSTR;
	InvokeHelper(0xd1, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo, InputFormat, OutputFormat);
}

void _DTAB::SetFieldValues(long LineNo, LPCTSTR FieldList, LPCTSTR ValueList)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BSTR;
	InvokeHelper(0xd2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, FieldList, ValueList);
}

CString _DTAB::GetFieldValues(long LineNo, LPCTSTR FieldList)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0xd3, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		LineNo, FieldList);
	return result;
}

CString _DTAB::GetBufferByFieldList(long lFromLine, long lToLine, LPCTSTR FieldList, LPCTSTR LineSeparator)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR;
	InvokeHelper(0xd4, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		lFromLine, lToLine, FieldList, LineSeparator);
	return result;
}

void _DTAB::ReorgIndexes()
{
	InvokeHelper(0xd5, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _DTAB::CursorDecoration(LPCTSTR LogicalFields, LPCTSTR LocationList, LPCTSTR PixelList, LPCTSTR ColorList)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0xd6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LogicalFields, LocationList, PixelList, ColorList);
}

void _DTAB::AboutBox()
{
	InvokeHelper(0xfffffdd8, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// _DTABEvents properties

/////////////////////////////////////////////////////////////////////////////
// _DTABEvents operations

void _DTABEvents::SendLButtonClick(long LineNo, long ColNo)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo);
}

void _DTABEvents::SendLButtonDblClick(long LineNo, long ColNo)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo);
}

void _DTABEvents::SendRButtonClick(long LineNo, long ColNo)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo);
}

void _DTABEvents::SendMouseMove(long LineNo, long ColNo, LPCTSTR Flags)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo, Flags);
}

void _DTABEvents::ColumnSelectionChanged(long ColNo, BOOL Selected)
{
	static BYTE parms[] =
		VTS_I4 VTS_BOOL;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo, Selected);
}

void _DTABEvents::OnVScroll(long LineNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo);
}

void _DTABEvents::InplaceEditCell(long LineNo, long ColNo, LPCTSTR NewValue, LPCTSTR OldValue)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo, NewValue, OldValue);
}

void _DTABEvents::OnHScroll(long ColNo)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ColNo);
}

void _DTABEvents::RowSelectionChanged(long LineNo, BOOL Selected)
{
	static BYTE parms[] =
		VTS_I4 VTS_BOOL;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, Selected);
}

void _DTABEvents::HitKeyOnLine(short Key, long LineNo)
{
	static BYTE parms[] =
		VTS_I2 VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Key, LineNo);
}

void _DTABEvents::TimerExpired(long LineNo, long LineStatus)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, LineStatus);
}

void _DTABEvents::PrintFinishedTab()
{
	InvokeHelper(0xc, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _DTABEvents::ComboSelChanged(long LineNo, long ColNo, LPCTSTR OldValue, LPCTSTR NewValues)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo, OldValue, NewValues);
}

void _DTABEvents::EditPositionChanged(long LineNo, long ColNo, LPCTSTR Value)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo, Value);
}

void _DTABEvents::CloseInplaceEdit(long LineNo, long ColNo)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo);
}

void _DTABEvents::BoolPropertyChanged(long LineNo, long ColNo, BOOL NewValue)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BOOL;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 LineNo, ColNo, NewValue);
}

void _DTABEvents::PackageReceived(long lpPackage)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 lpPackage);
}
