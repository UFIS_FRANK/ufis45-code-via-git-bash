#if !defined(AFX_UCOM_H__45B7B5BA_9A29_48FE_821B_267E046BC524__INCLUDED_)
#define AFX_UCOM_H__45B7B5BA_9A29_48FE_821B_267E046BC524__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Machine generated IDispatch wrapper class(es) created by Microsoft Visual C++

// NOTE: Do not modify the contents of this file.  If this class is regenerated by
//  Microsoft Visual C++, your modifications will be overwritten.

/////////////////////////////////////////////////////////////////////////////
// CUCom wrapper class

class CUCom : public CWnd
{
protected:
	DECLARE_DYNCREATE(CUCom)
public:
	CLSID const& GetClsid()
	{
		static CLSID const clsid
			= { 0xf9478336, 0x76ba, 0x11d6, { 0x80, 0x67, 0x0, 0x1, 0x2, 0x22, 0x5, 0xe4 } };
		return clsid;
	}
	virtual BOOL Create(LPCTSTR lpszClassName,
		LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect,
		CWnd* pParentWnd, UINT nID,
		CCreateContext* pContext = NULL)
	{ return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID); }

    BOOL Create(LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect, CWnd* pParentWnd, UINT nID,
		CFile* pPersist = NULL, BOOL bStorage = FALSE,
		BSTR bstrLicKey = NULL)
	{ return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID,
		pPersist, bStorage, bstrLicKey); }

// Attributes
public:
	CString GetApplicationName();
	void SetApplicationName(LPCTSTR);
	CString GetPort();
	void SetPort(LPCTSTR);
	CString GetHopo();
	void SetHopo(LPCTSTR);
	CString GetServerName();
	void SetServerName(LPCTSTR);
	CString GetTableExtension();
	void SetTableExtension(LPCTSTR);
	CString GetUserName();
	void SetUserName(LPCTSTR);
	CString GetWorkstationName();
	void SetWorkstationName(LPCTSTR);
	CString GetSendTimeoutSeconds();
	void SetSendTimeoutSeconds(LPCTSTR);
	CString GetReceiveTimeoutSeconds();
	void SetReceiveTimeoutSeconds(LPCTSTR);
	CString GetRecordSeparator();
	void SetRecordSeparator(LPCTSTR);
	CString GetCedaIdentifier();
	void SetCedaIdentifier(LPCTSTR);
	CString GetVersion();
	void SetVersion(LPCTSTR);
	CString GetBuildDate();
	void SetBuildDate(LPCTSTR);
	CString GetSQLAccessMethod();
	void SetSQLAccessMethod(LPCTSTR);
	CString GetSQLConnectionString();
	void SetSQLConnectionString(LPCTSTR);
	long GetPacketSize();
	void SetPacketSize(long);
	CString GetErrorSimulation();
	void SetErrorSimulation(LPCTSTR);

// Operations
public:
	CString GetLastError();
	BOOL CedaAction(LPCTSTR Command, LPCTSTR DBTable, LPCTSTR DBFields, LPCTSTR DBData, LPCTSTR DBWhere);
	long GetBufferCount();
	CString GetRecord(long RecordNo);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UCOM_H__45B7B5BA_9A29_48FE_821B_267E046BC524__INCLUDED_)
