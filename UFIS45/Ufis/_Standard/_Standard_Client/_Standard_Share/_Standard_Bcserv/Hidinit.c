//****************************************************************************
// File: hidinit.c
//
// Purpose: Contains initialization functions
//
// Functions:
//    InitApplication() - registers the window class for first instance of app
//    InitInstance() - creates main window
//
// Development Team:   Brian Scott
//
//
// Written by Microsoft Product Support Services, Windows Developer Support
// Copyright (c) 1992 Microsoft Corporation. All rights reserved.
//****************************************************************************

// The following #define is used by HIDGLOB.H -- it tells it that we
// want the globals _declared_ in this module.
#define IN_INIT

#include <windows.h>
#include <bcserv32.h>



//****************************************************************************
// Function: InitApplication
//
// Purpose: Called by WinMain on first instance of app.  Registers
//          the window class.
//
// Parameters:
//    hInst == Handle to _this_ instance.
//
// Returns: TRUE on success, FALSE otherwise.
//
// Comments:
//
// History:  Date       Author        Reason
//           1/27/92    BS            Created
//****************************************************************************

BOOL InitApplication (HANDLE hInst)
{
   WNDCLASS wc;
   char szMenuName[STR_LEN];
   char szClassName[STR_LEN];

//   LoadString(hInst, IDS_MAINCLASSNAME, szClassName, STR_LEN);
   strcpy(szClassName,"BCSERVCLASS");

   wc.style = CS_HREDRAW |
              CS_VREDRAW;
   wc.lpfnWndProc = (WNDPROC)MainWndProc;
   wc.cbClsExtra = 0;
   wc.cbWndExtra = 0;
   wc.hInstance = hInst; 
   wc.hIcon = NULL;
   wc.hCursor = LoadCursor(NULL, IDC_ARROW);
   wc.hbrBackground = (HBRUSH  ) COLOR_WINDOW + 1;
   wc.lpszMenuName = szMenuName;
   wc.lpszClassName = szClassName;

   // Register the window class and return success/failure code.

   return RegisterClass(&wc);
}

//****************************************************************************
// Function: InitInstance
//
// Purpose: Called by WinMain on instance startup.  Creates and
//            displays the main, overlapped window.
//
// Parameters:
//    hInstance     == Handle to _this_ instance.
//    nCmdShow      == How app should come up (i.e. minimized/normal)
//
// Returns: TRUE on success, FALSE otherwise.
//
// Comments:
//
// History:  Date       Author        Reason
//           1/27/92    DAW           Created
//****************************************************************************

BOOL InitInstance (HANDLE hInstance,
                   int nCmdShow)
{
   HWND hWnd;
   char szTitle[STR_LEN];
   char szClassName[STR_LEN];

   // Load some necessary strings from the string table.

   LoadString(hInstance, IDS_PROGNAME, szTitle, STR_LEN);
 //  LoadString(hInstance, IDS_MAINCLASSNAME, szClassName, STR_LEN);
   
   strcpy(szClassName,"BCSERVCLASS");

   // Save the instance handle in static variable, which will be used in
   // many subsequence calls from this application to Windows.

   ghInst = hInstance;


   // Create a main window for this application instance.

   hWnd = CreateWindow(szClassName,    // See RegisterClass() call.
                       szTitle,        // Text for window title bar.
                       WS_OVERLAPPED,  // Window style.
                       CW_USEDEFAULT,  // Default horizontal position.
                       CW_USEDEFAULT,  // Default vertical position.
                       CW_USEDEFAULT,  // Default width.
                       CW_USEDEFAULT,  // Default height.
                       NULL,           // Overlapped windows have no parent.
                       NULL,           // Use the window class menu.
                       hInstance,      // This instance owns this window.
                       NULL);          // Pointer not used.


   // We'll keep a global of the main window's handle.  This is useful
   //  for calls to MessageBox(), etc.

   ghWnd = hWnd;


   // If window could not be created, return "failure"

   if (!hWnd)
      return (FALSE);

   return(TRUE);

}
