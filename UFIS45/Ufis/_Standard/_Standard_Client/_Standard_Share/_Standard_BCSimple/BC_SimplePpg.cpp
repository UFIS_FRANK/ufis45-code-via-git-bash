// BC_SimplePpg.cpp : Implementation of the CBC_SimplePropPage property page class.

#include "stdafx.h"
#include "BC_Simple.h"
#include "BC_SimplePpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CBC_SimplePropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CBC_SimplePropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CBC_SimplePropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CBC_SimplePropPage, "BCSIMPLE.BCSimplePropPage.1",
	0xc72e0f2b, 0xb29c, 0x4166, 0x95, 0x1, 0xdb, 0x7a, 0xb7, 0xe0, 0xec, 0xab)


/////////////////////////////////////////////////////////////////////////////
// CBC_SimplePropPage::CBC_SimplePropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CBC_SimplePropPage

BOOL CBC_SimplePropPage::CBC_SimplePropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_BC_SIMPLE_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CBC_SimplePropPage::CBC_SimplePropPage - Constructor

CBC_SimplePropPage::CBC_SimplePropPage() :
	COlePropertyPage(IDD, IDS_BC_SIMPLE_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CBC_SimplePropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CBC_SimplePropPage::DoDataExchange - Moves data between page and properties

void CBC_SimplePropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CBC_SimplePropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CBC_SimplePropPage message handlers
