#if !defined(AFX_BC_SIMPLEPPG_H__09F20B66_81FE_45CC_AC90_58CB3F0FA8D0__INCLUDED_)
#define AFX_BC_SIMPLEPPG_H__09F20B66_81FE_45CC_AC90_58CB3F0FA8D0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// BC_SimplePpg.h : Declaration of the CBC_SimplePropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CBC_SimplePropPage : See BC_SimplePpg.cpp.cpp for implementation.

class CBC_SimplePropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CBC_SimplePropPage)
	DECLARE_OLECREATE_EX(CBC_SimplePropPage)

// Constructor
public:
	CBC_SimplePropPage();

// Dialog Data
	//{{AFX_DATA(CBC_SimplePropPage)
	enum { IDD = IDD_PROPPAGE_BC_SIMPLE };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CBC_SimplePropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BC_SIMPLEPPG_H__09F20B66_81FE_45CC_AC90_58CB3F0FA8D0__INCLUDED)
