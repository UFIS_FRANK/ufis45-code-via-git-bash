// TabCombo.cpp : implementation file
//

#include "stdafx.h"
#include "tab.h"
#include "TabCombo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTabCombo

CTabCombo::CTabCombo()
{
	pomProperties = NULL;
	imLineHeight = 20;
	pomParent = NULL;
	OwnerDraw = TRUE;
}

CTabCombo::CTabCombo(COMBO_OBJECT_PROP *popProperties, CWnd* popParent)
{
	pomProperties = popProperties;
	imLineHeight = 20;
	pomParent = popParent;
	OwnerDraw = TRUE;
}

CTabCombo::~CTabCombo()
{
}


BEGIN_MESSAGE_MAP(CTabCombo, CComboBox)
	//{{AFX_MSG_MAP(CTabCombo)
	ON_WM_DRAWITEM()
	ON_WM_ERASEBKGND()
	ON_CONTROL_REFLECT(CBN_SELCHANGE, OnSelchange)
	ON_CONTROL_REFLECT(CBN_SELENDOK, OnSelendok)
	ON_CONTROL_REFLECT(CBN_KILLFOCUS, OnKillfocus)
	ON_WM_MEASUREITEM()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTabCombo message handlers
void CTabCombo::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if(OwnerDraw == FALSE)
	{
		//CComboBox::DrawItem(lpDrawItemStruct); 
	}
	else
	{
		CDC dc;
		CRect rect;
		BOOL blToggleFocusOnly;


		if (pomProperties == NULL)
		{
			return;
		}

		dc.Attach(lpDrawItemStruct->hDC);

		// draw the given item 
		blToggleFocusOnly = !(lpDrawItemStruct->itemAction & (ODA_DRAWENTIRE | ODA_SELECT));
		DrawItem(dc, lpDrawItemStruct->itemID,
			lpDrawItemStruct->rcItem,
			blToggleFocusOnly,
			lpDrawItemStruct->itemState & ODS_SELECTED,
			lpDrawItemStruct->itemState & ODS_FOCUS);

		// fill blank items at the end of the table, if necessary
		if ((lpDrawItemStruct->itemID == (UINT)this->GetCount() - 1) &&   // last line?
			(lpDrawItemStruct->itemAction & ODA_DRAWENTIRE))
		{
			int bottom, height;
			int id = lpDrawItemStruct->itemID;

			// calculate size and position of each line
			GetClientRect(&rect);
			bottom = rect.bottom;
			rect = lpDrawItemStruct->rcItem;
			height = rect.bottom - rect.top;

			// draw lower empty part of table content line-by-line
			for (rect.top += height; rect.top <= bottom; rect.top += height)
			{
				rect.bottom = min(rect.top + height, bottom);
				DrawItem(dc, ++id, rect, false, false, false);
			}
		}

		dc.Detach();
	}
}

void CTabCombo::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if(OwnerDraw == FALSE)
	{
		CComboBox::OnDrawItem(nIDCtl, lpDrawItemStruct);
	}
	else
	{
		CDC dc;
		CRect rect;
		BOOL blToggleFocusOnly;


		if (pomProperties == NULL)
		{
			return;
		}

		dc.Attach(lpDrawItemStruct->hDC);

		// draw the given item 
		blToggleFocusOnly = !(lpDrawItemStruct->itemAction & (ODA_DRAWENTIRE | ODA_SELECT));
		DrawItem(dc, lpDrawItemStruct->itemID,
			lpDrawItemStruct->rcItem,
			blToggleFocusOnly,
			lpDrawItemStruct->itemState & ODS_SELECTED,
			lpDrawItemStruct->itemState & ODS_FOCUS);

		// fill blank items at the end of the table, if necessary
		if ((lpDrawItemStruct->itemID == (UINT)this->GetCount() - 1) &&   // last line?
			(lpDrawItemStruct->itemAction & ODA_DRAWENTIRE))
		{
			int bottom, height;
			int id = lpDrawItemStruct->itemID;

			// calculate size and position of each line
			GetClientRect(&rect);
			bottom = rect.bottom;
			rect = lpDrawItemStruct->rcItem;
			height = rect.bottom - rect.top;

			// draw lower empty part of table content line-by-line
			for (rect.top += height; rect.top <= bottom; rect.top += height)
			{
				rect.bottom = min(rect.top + height, bottom);
				DrawItem(dc, ++id, rect, false, false, false);
			}
		}

		dc.Detach();
	}
}

void CTabCombo::DrawItem(CDC &dc, UINT itemID, const CRect &rectClip,
    BOOL bpIsToggleFocusOnly, BOOL bpIsSelected, BOOL bpIsFocus)
{
	if(OwnerDraw == FALSE)
	{
		//CComboBox::DrawItem(dc, itemID, rectClip, bpIsToggleFocusOnly, bpIsSelected, bpIsFocus);
	}
	else
	{
		int i;
		int ilCount;
		int ilCurrX=0;
		int ilCols = pomProperties->omColumnLen.GetSize();
		int ilHeight;
		int ilTop,
			ilBottom;
		CBrush olBackColorBrush;
		CBrush olTextBrush;
		CPen   olGridPen(PS_SOLID, 1, COLORREF(12632256));//(RGB(255,255,255))/*pomProperties->GridColor*/));;
		CPen *pOldPen;
		CBrush *pOldBrush;
		CFont *pOldFont;
		CFont *pFont;
		
		ilCount = pomProperties->omLines.GetSize();
		if (itemID >= (UINT)ilCount)
		{
			return;
		}
		if(bpIsSelected == TRUE)
		{
			long llTextCol = (long)COLORREF(RGB(255,255,255));
	//		llTextCol &= (long)COLORREF (pomProperties->omLines[itemID].TextColor);
			long llBackColor = (long)COLORREF (RGB(0,0,255));
	//		llBackColor &= ~(long)COLORREF(pomProperties->omLines[itemID].BackColor);
			olBackColorBrush.CreateSolidBrush(COLORREF(llBackColor));
			dc.SetTextColor (COLORREF (llTextCol));
		}
		else
		{
			olBackColorBrush.CreateSolidBrush(COLORREF(pomProperties->omLines[itemID].BackColor));
			dc.SetTextColor (COLORREF (pomProperties->omLines[itemID].TextColor));
		}
		pOldBrush = dc.SelectObject(&olBackColorBrush);
		pFont = GetFont(); 
		pOldFont = dc.SelectObject(pFont);
		CRect olHRect;
		olHRect = rectClip;
		olHRect.bottom--;
		ilHeight = GetItemHeight(itemID);
		dc.SetBkMode(TRANSPARENT);
		for( i = 0; i < ilCols; i++)
		{
			olHRect.left = ilCurrX+2;
			olHRect.right = olHRect.left + pomProperties->omColumnLen[i]-1;
			dc.FillRect(olHRect, &olBackColorBrush);
			olHRect.left = ilCurrX+2;
			dc.DrawText (pomProperties->omLines[itemID].Values[i], olHRect, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
			ilCurrX += pomProperties->omColumnLen[i];
		}
		ilCurrX = 0;
		ilTop = olHRect.top;
		ilBottom = olHRect.bottom;
		pOldPen = dc.SelectObject(&olGridPen);
		for( i = 0; i < ilCols; i++)
		{
			dc.MoveTo(ilCurrX, ilTop);
			dc.LineTo(ilCurrX, ilBottom);
			ilCurrX += pomProperties->omColumnLen[i];

		}
		dc.MoveTo(0, olHRect.bottom);
		dc.LineTo(rectClip.right, olHRect.bottom);
		dc.SelectObject(pOldFont);
		dc.SelectObject(pOldBrush);
		dc.SelectObject(pOldPen);
	}
}

BOOL CTabCombo::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	return CComboBox::OnEraseBkgnd(pDC);
}

void CTabCombo::OnSelchange() 
{
	if(pomParent != NULL)
	{
		pomParent->PostMessage(USR_SELCHANGED, (UINT)GetCurSel(), (LONG)pomProperties );
	}
	
}

void CTabCombo::OnSelendok() 
{
	// TODO: Add your control notification handler code here
	
}

void CTabCombo::OnKillfocus() 
{
	// TODO: Add your control notification handler code here
	
}

afx_msg void CTabCombo::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	// TODO: Add your message handler code here and/or call default
	lpMeasureItemStruct->itemHeight = imLineHeight;
	//CComboBox::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}


void CTabCombo::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	lpMeasureItemStruct->itemHeight = imLineHeight;
	
}

