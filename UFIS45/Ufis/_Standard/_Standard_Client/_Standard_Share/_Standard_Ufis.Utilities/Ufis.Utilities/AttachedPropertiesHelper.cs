﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Reflection;

namespace Ufis.Utilities
{
    /// <summary>
    /// Provides the functionity for special attached dependancy properties.
    /// </summary>
    public class AttachedPropertiesHelper
    {
        readonly static FrameworkPropertyMetadata metadata = new FrameworkPropertyMetadata(
            string.Empty, OnElementNamePropertyChanged);

        public static readonly DependencyProperty ElementNameProperty = DependencyProperty.RegisterAttached(
            "ElementName", typeof(string), typeof(AttachedPropertiesHelper), metadata);

        public static void SetElementName(UIElement element, string value)
        {
            element.SetValue(ElementNameProperty, value);
        }

        public static string GetElementName(UIElement element)
        {
            return element.GetValue(ElementNameProperty).ToString();
        }

        private static void OnElementNamePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PropertyInfo prop = d.GetType().GetProperty("Name");
            if (prop != null)
                prop.SetValue(d, e.NewValue, null);
        }
    }
}
