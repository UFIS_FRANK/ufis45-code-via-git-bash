#if !defined(AFX_UFISCOMPPG_H__A2F31EA5_C74F_11D3_A251_00500437F607__INCLUDED_)
#define AFX_UFISCOMPPG_H__A2F31EA5_C74F_11D3_A251_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UfisComPpg.h : Declaration of the CUfisComPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CUfisComPropPage : See UfisComPpg.cpp.cpp for implementation.

class CUfisComPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CUfisComPropPage)
	DECLARE_OLECREATE_EX(CUfisComPropPage)

// Constructor
public:
	CUfisComPropPage();

// Dialog Data
	//{{AFX_DATA(CUfisComPropPage)
	enum { IDD = IDD_PROPPAGE_UFISCOM };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CUfisComPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISCOMPPG_H__A2F31EA5_C74F_11D3_A251_00500437F607__INCLUDED)
