#if !defined(AFX_UFISCOM_H__A2F31E9B_C74F_11D3_A251_00500437F607__INCLUDED_)
#define AFX_UFISCOM_H__A2F31E9B_C74F_11D3_A251_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UfisCom.h : main header file for UFISCOM.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include <resource.h>       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CUfisComApp : See UfisCom.cpp for implementation.

class CUfisComApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISCOM_H__A2F31E9B_C74F_11D3_A251_00500437F607__INCLUDED)
