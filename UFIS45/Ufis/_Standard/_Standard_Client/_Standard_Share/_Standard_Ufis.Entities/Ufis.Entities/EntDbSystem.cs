﻿using System;


namespace Ufis.Entities
{
    [Entity(SerializedName = "SYSTAB")]

    public class EntDbSystem : BaseEntity
    {
        //[Field(SerializedName = "LOGD")]
        private string _loggedData;
        [Entity(SerializedName = "LOGD", MaxLength = 4000)]
        public virtual string LoggedData
        {
            get { return _loggedData; }
            set
            {
                if (_loggedData != value)
                {
                    _loggedData = value;
                    OnPropertyChanged("LoggedData");
                }
            }
        }


        //[Field(SerializedName = "FINA")]
        private string _fieldName;
        [Entity(SerializedName = "FINA", MaxLength = 4)]
        public virtual string FieldName
        {
            get { return _fieldName; }
            set
            {
                if (_fieldName != value)
                {
                    _fieldName = value;
                    OnPropertyChanged("FieldName");
                }
            }
        }


        //[Field(SerializedName = "TANA")]
        private string _tableName;
        [Entity(SerializedName = "TANA", MaxLength = 3)]
        public virtual string TableName
        {
            get { return _tableName; }
            set
            {
                if (_tableName != value)
                {
                    _tableName = value;
                    OnPropertyChanged("TableName");
                }
            }
        }

    }
}