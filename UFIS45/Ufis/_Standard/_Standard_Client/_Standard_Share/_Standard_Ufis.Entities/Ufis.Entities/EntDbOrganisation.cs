﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : EntDbOrganisation.cs

Version         : 1.0.0
Created Date    : 22 - Dec - 2011
Complete Date   : 22 - Dec - 2011
Created By      : Siva

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion
using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "ORGTAB")]
    public class EntDbOrganisation : ValidityBaseEntity
    {
        private string _organisationCode;
        private string _departmentCode;
        private string _departmentName;        
        private string _longDuration;
        private string _shortDuration;
        private string _courseDuration;
        private string _flag;
        private string _remark;


        [Entity(SerializedName = "DPT1", MaxLength = 8, IsMandatory = true)]
        public string OrganisationCode
        {
            get { return _organisationCode; }
            set
            {
                if (_organisationCode != value)
                {
                    _organisationCode = value;
                    OnPropertyChanged("OrganisationCode");
                }
            }
        }

        [Entity(SerializedName = "DPT2", MaxLength = 8, IsMandatory = true)]
        public string DepartmentCode
        {
            get { return _departmentCode; }
            set
            {
                if (_departmentCode != value)
                {
                    _departmentCode = value;
                    OnPropertyChanged("DepartmentCode");
                }
            }
        }

        [Entity(SerializedName = "DPTN", MaxLength = 40, IsMandatory = true)]
        public string DepartmentName
        {
            get { return _departmentName; }
            set
            {
                if (_departmentName != value)
                {
                    _departmentName = value;
                    OnPropertyChanged("DepartmentName");
                }
            }
        }

        [Entity(SerializedName = "ODGL", MaxLength = 4)]
        public string LongDuration
        {
            get { return _longDuration; }
            set
            {
                if (_longDuration != value)
                {
                    _longDuration = value;
                    OnPropertyChanged("LongDuration");
                }
            }
        }

        [Entity(SerializedName = "ODKL", MaxLength = 4)]
        public string ShortDuration
        {
            get { return _shortDuration; }
            set
            {
                if (_shortDuration != value)
                {
                    _shortDuration = value;
                    OnPropertyChanged("ShortDuration");
                }
            }
        }

        [Entity(SerializedName = "ODSL", MaxLength = 4)]
        public string CourseDuration
        {
            get { return _courseDuration; }
            set
            {
                if (_courseDuration != value)
                {
                    _courseDuration = value;
                    OnPropertyChanged("CourseDuration");
                }
            }
        }

        [Entity(SerializedName = "PRFL", MaxLength = 1)]
        public string Flag
        {
            get { return _flag; }
            set
            {
                if (_flag != value)
                {
                    _flag = value;
                    OnPropertyChanged("Flag");
                }
            }
        }


        [Entity(SerializedName = "REMA", MaxLength = 60)]
        public string Remark
        {
            get { return _remark; }
            set
            {
                if (_remark != value)
                {
                    _remark = value;
                    OnPropertyChanged("Remark");
                }
            }
        }
    }
}


