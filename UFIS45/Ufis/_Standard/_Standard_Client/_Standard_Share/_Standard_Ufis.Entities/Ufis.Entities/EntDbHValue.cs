﻿using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "")]
    public class EntDbHvalue : BaseEntity
    {

      //[Field(SerializedName = "VALU")]
        private string _secondValue;
        [Entity(SerializedName = "VALU", MaxLength = 4000)]
        public virtual string SecondValue
        {
            get { return _secondValue; }
            set
            {
                if (_secondValue != value)
                {
                    _secondValue = value;
                    OnPropertyChanged("SecondValue");
                }
            }
        }

        //[Field(SerializedName = "OVAL")]
        private string _originalValue;
        [Entity(SerializedName = "OVAL", MaxLength = 4000)]
        public virtual string OriginalValue
        {
            get { return _originalValue; }
            set
            {
                if (_originalValue != value)
                {
                    _originalValue = value;
                    OnPropertyChanged("OriginalValue");
                }
            }
        }

        //[Field(SerializedName = "UREF")]
        private int _urnoOfFlight;
        [Entity(SerializedName = "UREF", MaxLength = 22, SourceDataType = typeof(String))]
        public virtual int UrnoOfFlight
        {
            get { return _urnoOfFlight; }
            set
            {
                if (_urnoOfFlight != value)
                {
                    _urnoOfFlight = value;
                    OnPropertyChanged("UrnoOfFlight");
                }
            }
        }

        //[Field(SerializedName = "FINA")]
        private string _fieldName;
        [Entity(SerializedName = "FINA", MaxLength = 4)]
        public virtual string FieldName
        {
            get { return _fieldName; }
            set
            {
                if (_fieldName != value)
                {
                    _fieldName = value;
                    OnPropertyChanged("FieldName");
                }
            }
        }


        //[Field(SerializedName = "TANA")]
        private string _tableName;
        [Entity(SerializedName = "TANA", MaxLength = 3)]
        public virtual string TableName
        {
            get { return _tableName; }
            set
            {
                if (_tableName != value)
                {
                    _tableName = value;
                    OnPropertyChanged("TableName");
                }
            }
        }
        private DateTime? _time;//CHON
        [Entity(SerializedName = "TIME", SourceDataType = typeof(String))]
        public virtual DateTime? Time
        {
            get { return _time; }
            set
            {
                if (_time != value)
                {
                    _time = value;
                    OnPropertyChanged("Time");
                }
            }
        }

    }
}
