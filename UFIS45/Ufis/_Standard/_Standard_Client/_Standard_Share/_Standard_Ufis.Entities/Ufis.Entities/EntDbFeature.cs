﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Entities
{
    [Entity(SerializedName = "FEATAB")]
    public class EntDbFeature : ValidityBaseEntity
    {
        private string _applicationName;
        private string _featureName;
        private string _referenceTable;
        private string _referenceField;

        [Entity(SerializedName = "APPL", MaxLength = 128)]
        public virtual string ApplicationName
        {
            get { return _applicationName; }
            set
            {
                if (_applicationName != value)
                {
                    _applicationName = value;
                    OnPropertyChanged("ApplicationName");
                }
            }
        }

        [Entity(SerializedName = "FENM", MaxLength = 128)]
        public virtual string FeatureName
        {
            get { return _featureName; }
            set
            {
                if (_featureName != value)
                {
                    _featureName = value;
                    OnPropertyChanged("FeatureName");
                }
            }
        }


        [Entity(SerializedName = "TANA", MaxLength = 128)]
        public virtual string ReferenceTable
        {
            get { return _referenceTable; }
            set
            {
                if (_referenceTable != value)
                {
                    _referenceTable = value;
                    OnPropertyChanged("ReferenceTable");
                }
            }
        }

        [Entity(SerializedName = "FINA", MaxLength = 32)]
        public virtual string ReferenceField
        {
            get { return _referenceField; }
            set
            {
                if (_referenceField != value)
                {
                    _referenceField = value;
                    OnPropertyChanged("ReferenceField");
                }
            }
        } 
        
    }
}
