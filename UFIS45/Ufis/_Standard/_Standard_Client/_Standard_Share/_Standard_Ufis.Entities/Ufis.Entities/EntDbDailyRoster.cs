﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : EntDbEmployeeData.cs

Version         : 1.0.0
Created Date    : 22 - Dec - 2011
Complete Date   : 22 - Dec - 2011
Created By      : Siva

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion
using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "DRRTAB")]
    public class EntDbDailyRoster : ValidityBaseEntity
    {

        private DateTime? _availableFrom;
        private DateTime? _availableTo;
        private bool _breakPaid;
        private string _breakOffset;
        private Int32 _urnoBSD;
        private string _trunkedNumber;
        private string _shiftNumber;
        private string _beforeBasicShiftTime;
        private string _percentageDisability;
        private string _afterBasicShiftTime;
        private string _completeShift;
        private bool _shiftSupplement;
        private bool _colorShiftCode;
        private string _shiftFunction;
        private string _remark;
        private string _planningLevel;
        private string _planningStatus;
        private DateTime? _shiftBreakFrom;
        private string _breakLengthPaid;
        private string _breakLengthNotPaid;
        private DateTime? _shiftBreakTo;
        private string _shiftCode;
        private string _oldShiftCode;
        private string _dayShift;
        private Int32 _shiftOffset;
        private Int32 _urnoSTF;

        [Entity(SerializedName = "AVFR", SourceDataType = typeof(String), IsMandatory = true)]
        public DateTime? AvailableFrom
        {
            get { return _availableFrom; }
            set
            {
                if (_availableFrom != value)
                {
                    _availableFrom = value;
                    OnPropertyChanged("AvailableFrom");
                }
            }
        }

        [Entity(SerializedName = "AVTO", SourceDataType = typeof(String), IsMandatory = true)]
        public DateTime? AvailableTo
        {
            get { return _availableTo; }
            set
            {
                if (_availableTo != value)
                {
                    _availableTo = value;
                    OnPropertyChanged("AvailableTo");
                }
            }
        }

        [Entity(SerializedName = "BKDP", SourceDataType = typeof(String), MaxLength = 1, TrueValue = "X", FalseValue = " ")]
        public bool BreakPaid
        {
            get { return _breakPaid; }
            set
            {
                if (_breakPaid != value)
                {
                    _breakPaid = value;
                    OnPropertyChanged("BreakPaid");
                }
            }
        }

        [Entity(SerializedName = "BROS", MaxLength = 4)]
        public string BreakOffset
        {
            get { return _breakOffset; }
            set
            {
                if (_breakOffset != value)
                    _breakOffset = value;
                OnPropertyChanged("BreakOffset");
            }
        }

        [Entity(SerializedName = "BSDU", MaxLength = 10)]
        public Int32 UrnoBSD
        {
            get { return _urnoBSD; }
            set
            {
                if (_urnoBSD != value)
                    _urnoBSD = value;
                OnPropertyChanged("UrnoBSD");
            }
        }

        [Entity(SerializedName = "BUFU", MaxLength = 15)]
        public string TrunkedNumber
        {
            get { return _trunkedNumber; }
            set
            {
                if (_trunkedNumber != value)
                    _trunkedNumber = value;
                OnPropertyChanged("TrunkedNumber");
            }
        }

        [Entity(SerializedName = "DRRN", MaxLength = 1)]
        public string ShiftNumber
        {
            get { return _shiftNumber; }
            set
            {
                if (_shiftNumber != value)
                    _shiftNumber = value;
                OnPropertyChanged("ShiftNumber");
            }
        }

        [Entity(SerializedName = "DRS1", MaxLength = 1)]
        public string BeforeBasicShiftTime
        {
            get { return _beforeBasicShiftTime; }
            set
            {
                if (_beforeBasicShiftTime != value)
                    _beforeBasicShiftTime = value;
                OnPropertyChanged("BeforeBasicShiftTime");
            }
        }

        [Entity(SerializedName = "DRS2", MaxLength = 1)]
        public string PercentageDisability
        {
            get { return _percentageDisability; }
            set
            {
                if (_percentageDisability != value)
                    _percentageDisability = value;
                OnPropertyChanged("PercentageDisability");
            }
        } 


        [Entity(SerializedName = "DRS3", MaxLength = 1)]
        public string AfterBasicShiftTime
        {
            get { return _afterBasicShiftTime; }
            set
            {
                if (_afterBasicShiftTime != value)
                    _afterBasicShiftTime = value;
                OnPropertyChanged("AfterBasicShiftTime");
            }
        } 
      
        [Entity(SerializedName = "DRS4", MaxLength = 1)]
        public string CompleteShift
        {
            get { return _completeShift; }
            set
            {
                if (_completeShift != value)
                    _completeShift = value;
                OnPropertyChanged("CompleteShift");
            }
        }

        [Entity(SerializedName = "DRSF", SourceDataType = typeof(String), MaxLength = 1, TrueValue = "1", FalseValue = " ")]
        public bool ShiftSupplement
        {
            get { return _shiftSupplement; }
            set
            {
                if (_shiftSupplement != value)
                {
                    _shiftSupplement = value;
                    OnPropertyChanged("ShiftSupplement");
                }
            }
        }
        

        [Entity(SerializedName = "EXPF", SourceDataType = typeof(String), MaxLength = 1, TrueValue = "A", FalseValue = " ")]
        public bool ColorShiftCode
        {
            get { return _colorShiftCode; }
            set
            {
                if (_colorShiftCode != value)
                {
                    _colorShiftCode = value;
                    OnPropertyChanged("ColorShiftCode");
                }
            }
        }


        [Entity(SerializedName = "FCTC", MaxLength = 8)]
        public string ShiftFunction
        {
            get { return _shiftFunction; }
            set
            {
                if (_shiftFunction != value)
                {
                    _shiftFunction = value;
                    OnPropertyChanged("ShiftFunction");
                }
            }
        }

        [Entity(SerializedName = "REMA", MaxLength = 60)]
        public string Remark
        {
            get { return _remark; }
            set
            {
                if (_remark != value)
                {
                    _remark = value;
                    OnPropertyChanged("Remark");
                }
            }
        }

        [Entity(SerializedName = "ROSL", MaxLength = 1)]
        public string PlanningLevel
        {
            get { return _planningLevel; }
            set
            {
                if (_planningLevel != value)
                {
                    _planningLevel = value;
                    OnPropertyChanged("PlanningLevel");
                }
            }
        }

        [Entity(SerializedName = "ROSS", MaxLength = 1)]
        public string PlanningStatus
        {
            get { return _planningStatus; }
            set
            {
                if (_planningStatus != value)
                {
                    _planningStatus = value;
                    OnPropertyChanged("PlanningStatus");
                }
            }
        }
        
        [Entity(SerializedName = "SBFR", SourceDataType = typeof(String), IsMandatory = true)]
        public DateTime? ShiftBreakFrom
        {
            get { return _shiftBreakFrom; }
            set
            {
                if (_shiftBreakFrom != value)
                {
                    _shiftBreakFrom = value;
                    OnPropertyChanged("ShiftBreakFrom");
                }
            }
        }

        [Entity(SerializedName = "SBLP", MaxLength = 4)]
        public string BreakLengthPaid
        {
            get { return _breakLengthPaid; }
            set
            {
                if (_breakLengthPaid != value)
                    _breakLengthPaid = value;
                OnPropertyChanged("BreakLengthPaid");
            }
        }

        [Entity(SerializedName = "SBLU", MaxLength = 4)]
        public string BreakLengthNotPaid
        {
            get { return _breakLengthNotPaid; }
            set
            {
                if (_breakLengthNotPaid != value)
                    _breakLengthNotPaid = value;
                OnPropertyChanged("BreakLengthNotPaid");
            }
        }

        [Entity(SerializedName = "SBTO", SourceDataType = typeof(String), IsMandatory = true)]
        public DateTime? ShiftBreakTo
        {
            get { return _shiftBreakTo; }
            set
            {
                if (_shiftBreakTo != value)
                {
                    _shiftBreakTo = value;
                    OnPropertyChanged("ShiftBreakTo");
                }
            }
        }

        [Entity(SerializedName = "SCOD", MaxLength = 8)]
        public string ShiftCode
        {
            get { return _shiftCode; }
            set
            {
                if (_shiftCode != value)
                    _shiftCode = value;
                OnPropertyChanged("ShiftCode");
            }
        }

        [Entity(SerializedName = "SCOO", MaxLength = 8)]
        public string OldShiftCode
        {
            get { return _oldShiftCode; }
            set
            {
                if (_oldShiftCode != value)
                    _oldShiftCode = value;
                OnPropertyChanged("OldShiftCode");
            }
        }

        [Entity(SerializedName = "SDAY", MaxLength = 8)]
        public string DayShift
        {
            get { return _dayShift; }
            set
            {
                if (_dayShift != value)
                    _dayShift = value;
                OnPropertyChanged("DayShift");
            }
        }

        [Entity(SerializedName = "SDOF", MaxLength = 2)]
        public Int32 ShiftOffset
        {
            get { return _shiftOffset; }
            set
            {
                if (_shiftOffset != value)
                    _shiftOffset = value;
                OnPropertyChanged("ShiftOffset");
            }
        }

        [Entity(SerializedName = "STFU", MaxLength = 10)]
        public Int32 UrnoSTF
        {
            get { return _urnoSTF; }
            set
            {
                if (_urnoSTF != value)
                    _urnoSTF = value;
                OnPropertyChanged("UrnoSTF");
            }
        }

    }
}
