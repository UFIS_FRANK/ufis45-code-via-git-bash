﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Entities
{
    [Entity(SerializedName = "CRATAB")]
    public class EntCraTab : BaseEntity
    {
        private string strCURN;//Delay Reason URNO to link to CRCTAB URNO
        private string strFURN;//Flight URNO
        private string strCDAT;//CDAT Field

        [Entity(SerializedName = "CURN")]
        public string DelayReasonURNO
        {
            get
            {
                if (String.IsNullOrEmpty(strCURN))
                    return "";
                return strCURN;
            }

            set
            {
                strCURN = value;
                OnPropertyChanged("DelayReasonURNO");
            }
        }

        [Entity(SerializedName = "FURN")]
        public string FlightURNO
        {
            get
            {
                if (String.IsNullOrEmpty(strFURN))
                    return "";
                return strFURN;
            }

            set
            {
                strFURN = value;
                OnPropertyChanged("FlightURNO");
            }
        }

        [Entity(SerializedName = "CDAT")]
        public string CRADateTime
        {
            get
            {
                if (String.IsNullOrEmpty(strCDAT))
                    return "";
                return strCDAT;
            }

            set
            {
                strCDAT = value;
                OnPropertyChanged("CRADateTime");
            }
        }
    }
}
