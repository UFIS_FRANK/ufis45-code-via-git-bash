﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : EntDbStaffOrganisation.cs

Version         : 1.0.0
Created Date    : 22 - Dec - 2011
Complete Date   : 22 - Dec - 2011
Created By      : Siva

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion
using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "SORTAB")]
    public class EntDbStaffOrganisation : ValidityBaseEntity
    {
        //private int _linkurno;
        private string _organisationCode;
        private string _groupLeader;
        private string _groupCode;
        private int _staffurno;        

        //[EntityAttribute(SerializedName = "URNO", IsMandatory = true)]
        //public int LinkUrno
        //{
        //    get { return _linkurno; }
        //    set
        //    {
        //        if (_linkurno != value)
        //        {
        //            _linkurno = value;
        //            OnPropertyChanged("LinkUrno");
        //        }
        //    }
        //}


        [Entity(SerializedName = "CODE", MaxLength = 8, IsMandatory = true)]
        public string OrganisationCode
        {
            get { return _organisationCode; }
            set
            {
                if (_organisationCode != value)
                {
                    _organisationCode = value;
                    OnPropertyChanged("OrganisationCode");
                }
            }
        }

        [Entity(SerializedName = "LEAD", MaxLength = 1)]
        public string GroupLeader
        {
            get { return _groupLeader; }
            set
            {
                if (_groupLeader != value)
                {
                    _groupLeader = value;
                    OnPropertyChanged("GroupLeader");
                }
            }
        }

        [Entity(SerializedName = "ODGC", MaxLength = 8)]
        public string GroupCode
        {
            get { return _groupCode; }
            set
            {
                if (_groupCode != value)
                {
                    _groupCode = value;
                    OnPropertyChanged("GroupCode");
                }
            }
        }

        [Entity(SerializedName = "SURN")]
        public int StaffURNO
        {
            get { return _staffurno; }
            set
            {
                if (_staffurno != value)
                {
                    _staffurno = value;
                    OnPropertyChanged("StaffURNO");
                }
            }
        }        
    }
}