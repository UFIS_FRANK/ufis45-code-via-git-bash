﻿using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "VCDTAB")]
    public class EntDbUserDefinedView : ValidityBaseEntity
    {
        private string _applicationName;
        private string _key;
        private string _type;
        private string _text;
        private string _userId;

        [Entity(SerializedName = "APPN", MaxLength = 32)]
        public string ApplicationName
        {
            get { return _applicationName; }
            set
            {
                if (_applicationName != value)
                {
                    _applicationName = value;
                    OnPropertyChanged("ApplicationName");
                }
            }
        }

        [Entity(SerializedName = "CKEY", MaxLength = 32)]
        public string Key
        {
            get { return _key; }
            set
            {
                if (_key != value)
                {
                    _key = value;
                    OnPropertyChanged("Key");
                }
            }
        }

        [Entity(SerializedName = "CTYP", MaxLength = 32)]
        public string Type
        {
            get { return _type; }
            set
            {
                if (_type != value)
                {
                    _type = value;
                    OnPropertyChanged("Type");
                }
            }
        }

        [Entity(SerializedName = "TEXT", MaxLength = 2000)]
        public string Text
        {
            get { return _text; }
            set
            {
                if (_text != value)
                {
                    _text = value;
                    OnPropertyChanged("Text");
                }
            }
        }

        [Entity(SerializedName = "PKNO", MaxLength = 32)]
        public string UserId
        {
            get { return _userId; }
            set
            {
                if (_userId != value)
                {
                    _userId = value;
                    OnPropertyChanged("UserId");
                }
            }
        }
    }
}
