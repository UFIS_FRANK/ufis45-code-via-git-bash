﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : EntDbGlobalConfig.cs

Version         : 1.0.0
Created Date    : 11 - Jul - 2012
Complete Date   : 11 - Jul - 2012
Developed By    : Indra Gunawan

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion
using System;

namespace Ufis.Entities
{
  
    [Entity(SerializedName = "GCFTAB")]
    public class EntDbConfiguration : BaseEntity
    {
        private string _applicationName;
        [EntityAttribute(SerializedName = "APNA", MaxLength = 32)]
        public string ApplicationName
        {
            get { return _applicationName; }
            set
            {
                if (_applicationName != value)
                {
                    _applicationName = value;
                    OnPropertyChanged("ApplicationName");
                }
            }
        }

        private string _sectionName;
        [EntityAttribute(SerializedName = "SECT", MaxLength = 32)]
        public virtual string SectionName
        {
            get { return _sectionName; }
            set
            {
                if (_sectionName != value)
                {
                    _sectionName = value;
                    OnPropertyChanged("SectionName");
                }
            }
        }

        private string _parameterName;
        [EntityAttribute(SerializedName = "PARA", MaxLength = 32)]
        public string ParameterName
        {
            get { return _parameterName; }
            set
            {
                if (_parameterName != value)
                {
                    _parameterName = value;
                    OnPropertyChanged("ParameterName");
                }
            }
        }
        
        private string _parameterValue;
        [EntityAttribute(SerializedName = "VALU", MaxLength = 4000)]
        public string ParameterValue
        {
            get { return _parameterValue; }
            set
            {
                if (_parameterValue != value)
                {
                    _parameterValue = value;
                    OnPropertyChanged("ParameterValue");
                }
            }
        }

        private string _homeAirport;
        [EntityAttribute(SerializedName = "HOPO", IsReadOnly = true)]
        public virtual string HomeAirport
        {
            get { return _homeAirport; }
            set
            {
                if (_homeAirport != value)
                {
                    _homeAirport = value;
                    OnPropertyChanged("HomeAirport");
                }
            }
        }
    }

    }

