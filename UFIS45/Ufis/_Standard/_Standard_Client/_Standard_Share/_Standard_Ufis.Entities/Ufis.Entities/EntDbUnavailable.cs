﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Ufis.Entities
{
    [Entity(SerializedName = "BLKTAB")]
    public class EntDbUnavailable : BaseEntity
    {
        private IList<object> _days = new List<object>();
        private string _actualDays;
        private bool _changeable;
        private string _referenceTable;
        private string _reason;
        private int _relatedUrno; //BURN
        private DateTime? _timeFrom; //TIFR
        private DateTime? _timeTo; //TITO
        private string _actualTimeFrom; //TIFR
        private string _actualTimeTo; //TITO
        private int _indexOfBlockingImage; //IBIT
        private DateTime? _validFrom;
        private DateTime? _validTo;
         

        [EntityAttribute(SerializedName = "NAFR", SourceDataType = typeof(string), IsMandatory = true)]
        public DateTime? ValidFrom
        {
            get { return _validFrom; }
            set
            {
                if (_validFrom != value)
                {
                    _validFrom = value;
                    OnPropertyChanged("ValidFrom");
                }
            }
        }

        [EntityAttribute(SerializedName = "NATO", SourceDataType = typeof(string))]
        public DateTime? ValidTo
        {
            get { return _validTo; }
            set
            {
                if (_validTo != value)
                {
                    _validTo = value;
                    OnPropertyChanged("ValidTo");
                }
            }
        }

        public virtual IList<object> Days
        {
            get { return _days; }
            set
            {
                if (_days != value)
                {
                    _days = value;
                    _actualDays = string
                        .Join(",", _days)
                        .Replace(",", string.Empty);

                    OnPropertyChanged("Days");
                }
            }
        }

        [Entity(SerializedName = "DAYS", MaxLength = 0)]
        public virtual string ActualDays
        {
            get { return _actualDays; }
            set
            {
                if (_actualDays != value)
                {
                    _actualDays = value;
                    _days.Clear();

                    foreach (var day in _actualDays)
                    {
                        _days.Add(day.ToString());
                    }

                    OnPropertyChanged("ActualDays");
                }
            }
        }

       
        public virtual bool Changeable
        {
            get { return _changeable; }
            set
            {
                if (_changeable != value)
                { 
                    _actualChangeable = (value) ? "X" : " "; 
                    _changeable = value;
                    OnPropertyChanged("Changeable");
                }
            }
        }

        private string _actualChangeable;

        [Entity(SerializedName = "TYPE", MaxLength = 1)]
        public virtual string ActualChangeable
        {
            get { return _actualChangeable; }
            set
            {
                if (_actualChangeable != value)
                {
                    if (!string.IsNullOrEmpty(value))
                    { 
                        _changeable = ("X" == value) ? true : false;
                    }

                    _actualChangeable = value;
                    OnPropertyChanged("ActualChangeable");
                }
            }
        }

        [Entity(SerializedName = "TABN", MaxLength = 3)]
        public virtual string ReferenceTable
        {
            get { return _referenceTable; }
            set
            {
                if (_referenceTable != value)
                {
                    _referenceTable = value;
                    OnPropertyChanged("ReferenceTable");
                }
            }
        }

        [Entity(SerializedName = "RESN", MaxLength = 40)]
        public virtual string Reason
        {
            get { return _reason; }
            set
            {
                if (_reason != value)
                {
                    _reason = value;
                    OnPropertyChanged("Reason");
                }
            }
        }

        [Entity(SerializedName = "BURN", MaxLength = 10)]
        public virtual int RelatedUrno
        {
            get { return _relatedUrno; }
            set
            {
                if (_relatedUrno != value)
                {
                    _relatedUrno = value;
                    OnPropertyChanged("RelatedUrno");
                }
            }
        }

        public virtual DateTime? TimeFrom
        {
            get { return _timeFrom; }
            set
            {
                if (null != value && _timeFrom != value)
                {
                    _timeFrom = value;
                    _actualTimeFrom = _timeFrom.Value.ToString(BaseEntity.DB_TIME_FORMAT);
                    OnPropertyChanged("TimeFrom");
                }
            }
        }

        [Entity(SerializedName = "TIFR", MaxLength = 4)]
        public virtual string ActualTimeFrom
        {
            get { return _actualTimeFrom; }
            set
            {
                if (_actualTimeFrom != value)
                {
                    _actualTimeFrom = value;

                    if (!string.IsNullOrEmpty(_actualTimeFrom))
                    {
                        var dateTime = new DateTime();

                        if (DateTime.TryParseExact(_actualTimeFrom, BaseEntity.DB_TIME_FORMAT,
                            BaseEntity.cultureInfo, DateTimeStyles.None,
                            out dateTime))
                        {
                            _timeFrom = new Nullable<DateTime>(dateTime);
                        }
                    }

                    OnPropertyChanged("ActualTimeFrom");
                }
            }
        }

        public virtual DateTime? TimeTo
        {
            get { return _timeTo; }
            set
            {
                if (null != value && _timeTo != value)
                {
                    _timeTo = value;
                    _actualTimeTo = _timeTo.Value.ToString(BaseEntity.DB_TIME_FORMAT);
                    OnPropertyChanged("TimeTo");
                }
            }
        }

        [Entity(SerializedName = "TITO", MaxLength = 4)]
        public virtual string ActualTimeTo
        {
            get { return _actualTimeTo; }
            set
            {
                if (_actualTimeTo != value)
                {
                    _actualTimeTo = value;

                    if (!string.IsNullOrEmpty(_actualTimeTo))
                    {
                        var dateTime = new DateTime();

                        if (DateTime.TryParseExact(
                            _actualTimeTo,
                            BaseEntity.DB_TIME_FORMAT,
                            BaseEntity.cultureInfo,
                            DateTimeStyles.None,
                            out dateTime))
                        {
                            _timeTo = new Nullable<DateTime>(dateTime);
                        }
                    }

                    OnPropertyChanged("ActualTimeTo");
                }
            }
        }

        [Entity(SerializedName = "IBIT", MaxLength = 3)]
        public virtual int IndexOfBlockingImage
        {
            get { return _indexOfBlockingImage; }
            set
            {
                if (_indexOfBlockingImage != value)
                {
                    _indexOfBlockingImage = value;
                    OnPropertyChanged("IndexOfBlockingImage");
                }
            }
        }

    }
}
