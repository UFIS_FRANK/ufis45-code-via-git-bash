﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Ufis.Entities
{     
	[Entity(SerializedName = "PSTTAB")]
    public class EntDbParkingStand : ValidityBaseEntity 
    {
        private string _name;
        private int _defaultAllocationDuration;
        private string _maxAircraftType;

        [Entity(SerializedName = "PNAM", MaxLength = 5, IsMandatory = true, IsUnique = true)]
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        [Entity(SerializedName = "MXAC", MaxLength = 5, SourceDataType = typeof(string), IsMandatory = true)]
        public virtual string MaxAircraftType
        {
            get
            {
                return _maxAircraftType;
            }
            set
            {
                if (_maxAircraftType != value)
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        _maxAircraftType = value.Trim();
                    }

                    OnPropertyChanged("MaxAircraftType");
                }
            }
        }

        [Entity(SerializedName = "DEFD", SourceDataType = typeof(string), MaxLength = 3)]
        public virtual int DefaultAllocationDuration
        {
            get
            {
                return _defaultAllocationDuration;
            }
            set
            {
                if (_defaultAllocationDuration != value)
                {
                    _defaultAllocationDuration = value;
                    OnPropertyChanged("DefaultAllocationDuration");
                }
            }
        }

        public IList<EntDbUnavailable> Unavailables { get; set; }

        public EntDbParkingStand()
        {
            Unavailables = new ObservableCollection<EntDbUnavailable>();
        }
    }
} 