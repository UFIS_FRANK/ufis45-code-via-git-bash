﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Entities
{
    [Entity(SerializedName = "FMLTAB")]
    public class EntDbFML:BaseEntity
	{
        private long? _FLightURNo;
        [Entity(SerializedName = "FLNU")]
        public long? FLightURNo
        {
            get { return _FLightURNo; }
            set
            {
                if (_FLightURNo != value)
                {
                    _FLightURNo = value;
                    OnPropertyChanged("FLightURNo");
                }
            }
        }

        private string _type;
        [Entity(SerializedName = "TYPE")]
        public string TYPE
        {
            get { return _type; }
            set
            {
                if (_type != value)
                {
                    _type = value;
                    OnPropertyChanged("TYPE");
                }
            }
        }

        private string _tmtp;
        [Entity(SerializedName = "TMTP")]
        public string TMTP
        {
            get { return _tmtp; }
            set
            {
                if (_tmtp != value)
                {
                    _tmtp = value;
                    OnPropertyChanged("TMTP");
                }
            }
        }

        private string _text;
        [Entity(SerializedName = "TEXT")]
        public string TEXT
        {
            get { return _text; }
            set
            {
                if (_text != value)
                {
                    _text = value;
                    OnPropertyChanged("TEXT");
                }
            }
        }
	}
}
