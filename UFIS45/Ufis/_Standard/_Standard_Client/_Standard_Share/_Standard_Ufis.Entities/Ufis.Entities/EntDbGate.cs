﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Entities
{
    [Entity(SerializedName = "GATTAB")]
    public class EntDbGate : ValidityBaseEntity
    {
        private string _gateID;
        private string _gateName;
        private string _gateType;        
        private string _busGate;
        private string _terminal;

        [Entity(SerializedName = "GTID", MaxLength = 10)]
        public virtual string GateID
        {
            get { return _gateID; }
            set
            {
                if (_gateID != value)
                {
                    _gateID = value;
                    OnPropertyChanged("GateID");
                }
            }
        }

        [Entity(SerializedName = "GNAM", MaxLength = 10)]
        public virtual string GateName
        {
            get { return _gateName; }
            set
            {
                if (_gateName != value)
                {
                    _gateName = value;
                    OnPropertyChanged("GateName");
                }
            }
        }


        [Entity(SerializedName = "GTYP", MaxLength = 1)]
        public virtual string GateType
        {
            get { return _gateType; }
            set
            {
                if (_gateType != value)
                {
                    _gateType = value;
                    OnPropertyChanged("GateType");
                }
            }
        }

        [Entity(SerializedName = "BUSG", MaxLength = 1)]
        public virtual string BusGate
        {
            get { return _busGate; }
            set
            {
                if (_busGate != value)
                {
                    _busGate = value;
                    OnPropertyChanged("BusGate");
                }
            }
        }

        [Entity(SerializedName = "TERM", MaxLength = 1)]
        public virtual string Terminal
        {
            get { return _terminal; }
            set
            {
                if (_terminal != value)
                {
                    _terminal = value;
                    OnPropertyChanged("Terminal");
                }
            }
        }
    }

}
