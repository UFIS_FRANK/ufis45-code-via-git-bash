﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Entities
{
    [Entity(SerializedName = "ALTTAB")]
    public class EntDbAirline : ValidityBaseEntity
    {
        private string _IATACode;
        private string _ICAOCode;
        private string _name;
        private string _remark;
        private string _ckicOffset;
        private string _administrator;
        private string _paymentMode;
        private string _terminalRestrictionForCheckInCounter;
        private string _terminalRestrictionForBaggageBelt;
        private string _terminalRestrictionForGate;
        private string _terminalRestrictionForParkingStand;
        private string _terminalRestrictionForLounge;

        [Entity(SerializedName = "ALC2", MaxLength = 2)]
        public virtual string IATACode
        {
            get { return _IATACode; }
            set
            {
                if (_IATACode != value)
                {
                    _IATACode = value;
                    OnPropertyChanged("IATACode");
                }
            }
        }

        [Entity(SerializedName = "ALC3", IsMandatory = true, MaxLength = 3, IsUnique = true)]
        public virtual string ICAOCode
        {
            get { return _ICAOCode; }
            set
            {
                if (_ICAOCode != value)
                {
                    _ICAOCode = value;
                    OnPropertyChanged("ICAOCode");
                }
            }
        }

        [Entity(SerializedName = "ALFN", IsMandatory = true, MaxLength = 60, IsUnique = true)]
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        [Entity(SerializedName = "TEXT", MaxLength = 20)]
        public virtual string Remark
        {
            get { return _remark; }
            set
            {
                if (_remark != value)
                {
                    _remark = value;
                    OnPropertyChanged("Remark");
                }
            }
        }

        //[Entity(SerializedName = "OFST", MaxLength = 10)]
        public virtual string CKICOffset
        {
            get { return _ckicOffset; }
            set
            {
                if (_ckicOffset != value)
                {
                    _ckicOffset = value;
                    OnPropertyChanged("CKICOffset");
                }
            }
        }

        [Entity(SerializedName = "ADMD", MaxLength = 10)]
        public virtual string Administrator
        {
            get { return _administrator; }
            set
            {
                if (_administrator != value)
                {
                    _administrator = value;
                    OnPropertyChanged("Administrator");
                }
            }
        }

        [Entity(SerializedName = "CASH", MaxLength = 1)]
        public virtual string PaymentMode
        {
            get { return _paymentMode; }
            set
            {
                if (_paymentMode != value)
                {
                    _paymentMode = value;
                    OnPropertyChanged("PaymentMode");
                }
            }
        }

        [Entity(SerializedName = "TERM", MaxLength = 64)]
        public virtual string TerminalRestrictionForCheckInCounter
        {
            get { return _terminalRestrictionForCheckInCounter; }
            set
            {
                if (_terminalRestrictionForCheckInCounter != value)
                {
                    _terminalRestrictionForCheckInCounter = value;
                    OnPropertyChanged("TerminalRestrictionForCheckInCounter");
                }
            }
        }

        [Entity(SerializedName = "TERB", MaxLength = 64)]
        public virtual string TerminalRestrictionForBaggageBelt
        {
            get { return _terminalRestrictionForBaggageBelt; }
            set
            {
                if (_terminalRestrictionForBaggageBelt != value)
                {
                    _terminalRestrictionForBaggageBelt = value;
                    OnPropertyChanged("TerminalRestrictionForBaggageBelt");
                }
            }
        }

        [Entity(SerializedName = "TERG", MaxLength = 64)]
        public virtual string TerminalRestrictionForGate
        {
            get { return _terminalRestrictionForGate; }
            set
            {
                if (_terminalRestrictionForGate != value)
                {
                    _terminalRestrictionForGate = value;
                    OnPropertyChanged("TerminalRestrictionForGate");
                }
            }
        }

        [Entity(SerializedName = "TERP", MaxLength = 64)]
        public virtual string TerminalRestrictionForParkingStand
        {
            get { return _terminalRestrictionForParkingStand; }
            set
            {
                if (_terminalRestrictionForParkingStand != value)
                {
                    _terminalRestrictionForParkingStand = value;
                    OnPropertyChanged("TerminalRestrictionForParkingStand");
                }
            }
        }

        [Entity(SerializedName = "TERW", MaxLength = 64)]
        public virtual string TerminalRestrictionForLounge
        {
            get { return _terminalRestrictionForLounge; }
            set
            {
                if (_terminalRestrictionForLounge != value)
                {
                    _terminalRestrictionForLounge = value;
                    OnPropertyChanged("TerminalRestrictionForLounge");
                }
            }
        }

        ////[OneToMany]
        //public virtual IList<Agent> Agents { get; set; }

        //public Airline()
        //{
        //    Agents = new ObservableCollection<Agent>();
        //} 
    }
}