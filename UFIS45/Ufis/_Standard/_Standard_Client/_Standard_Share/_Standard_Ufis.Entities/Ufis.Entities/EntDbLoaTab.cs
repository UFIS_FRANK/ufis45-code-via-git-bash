﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Entities
{
    [Entity(SerializedName = "LOATAB")]
    public class EntDbLoaTab : BaseEntity
    {
        //where TYPE='DIS' AND STYP='F' AND SSTP='          ' AND SSST='CAL' AND DSSN='SYS' order by flnu
        private long? _FLightURNo;
        [Entity(SerializedName = "FLNU")]
        public long? FLightURNo
        {
            get { return _FLightURNo; }
            set
            {
                if (_FLightURNo != value)
                {
                    _FLightURNo = value;
                    OnPropertyChanged("FLightURNo");
                }
            }
        }

        private string _type;
        [Entity(SerializedName = "TYPE")]
        public string TYPE
        {
            get { return _type; }
            set
            {
                if (_type != value)
                {
                    _type = value;
                    OnPropertyChanged("TYPE");
                }
            }
        }

        private string _styp;
        [Entity(SerializedName = "STYP")]
        public string STYP
        {
            get { return _styp; }
            set
            {
                if (_styp != value)
                {
                    _styp = value;
                    OnPropertyChanged("STYP");
                }
            }
        }

        private string _sstp;
        [Entity(SerializedName = "SSTP")]
        public string SSTP
        {
            get { return _sstp; }
            set
            {
                if (_sstp != value)
                {
                    _sstp = value;
                    OnPropertyChanged("SSTP");
                }
            }
        }

        private string _ssst;
        [Entity(SerializedName = "SSST")]
        public string SSST
        {
            get { return _ssst; }
            set
            {
                if (_ssst != value)
                {
                    _ssst = value;
                    OnPropertyChanged("SSST");
                }
            }
        }

        private string _dssn;
        [Entity(SerializedName = "DSSN")]
        public string DSSN
        {
            get { return _dssn; }
            set
            {
                if (_dssn != value)
                {
                    _dssn = value;
                    OnPropertyChanged("DSSN");
                }
            }
        }

        private string _valu;
        [Entity(SerializedName = "VALU")]
        public string VALU
        {
            get { return _valu; }
            set
            {
                if (_valu != value)
                {
                    _valu = value;
                    OnPropertyChanged("VALU");
                }
            }
        }
    }
}
