﻿using System;


namespace Ufis.Entities
{
    
    [Entity(SerializedName = "FPETAB")]
    public class EntDbFlightPermit: ValidityBaseEntity
    {
        //[Field(SerializedName = "PERN")]
        private string _flightPermitNumber;
        [Entity(SerializedName = "PERN", MaxLength = 32)]
        public virtual string FlightPermitNumber
        {
            get { return _flightPermitNumber; }
            set
            {
                if (_flightPermitNumber != value)
                {
                    _flightPermitNumber = value;
                    OnPropertyChanged("FlightPermitNumber");
                }
            }
        }

        //[Field(SerializedName = "REGN")]
        private string _registrationNumber;
        [Entity(SerializedName = "REGN", MaxLength = 12)]
        public virtual string RegistrationNumber
        {
            get { return _registrationNumber; }
            set
            {
                if (_registrationNumber != value)
                {
                    _registrationNumber = value;
                    OnPropertyChanged("RegistrationNumber");
                }
            }
        }

        //[Field(SerializedName = "ADID")]
        private string _arrivalDepartureId;
        [Entity(SerializedName = "ADID", MaxLength = 1)]
        public virtual string ArrivalDepartureId
        {
            get { return _arrivalDepartureId; }
            set
            {
                if (_arrivalDepartureId != value)
                {
                    _arrivalDepartureId = value;
                    OnPropertyChanged("ArrivalDepartureId");
                }
            }
        }

        //[Field(SerializedName = "ADFP")]
        //[Field(SerializedName = "ALCO")]
        //[Field(SerializedName = "FLNO")]
        private string _fullFlightNumber;
        [Entity(SerializedName = "FLNO", MaxLength = 9)]
        public virtual string FullFlightNumber
        {
            get { return _fullFlightNumber; }
            set
            {
                if (_fullFlightNumber != value)
                {
                    _fullFlightNumber = value;
                    OnPropertyChanged("FullFlightNumber");
                }
            }
        }

        //[Field(SerializedName = "SUFX")]
        //[Field(SerializedName = "DOOP")]
        //[Field(SerializedName = "ATDN")]
        //[Field(SerializedName = "DTGN")]
        //[Field(SerializedName = "MTOW")]
        //[Field(SerializedName = "PERR")]
        //[Field(SerializedName = "OBLR")]
        //[Field(SerializedName = "USRR")]
   
     
        //[Field(SerializedName = "REPN")]
        //[Field(SerializedName = "CSGN")]
        private string _callSign;
        [Entity(SerializedName = "CSGN", MaxLength = 8)]
        public virtual string CallSign
        {
            get { return _callSign; }
            set
            {
                if (_callSign != value)
                {
                    _callSign = value;
                    OnPropertyChanged("CallSign");
                }
            }
        }

        //[Field(SerializedName = "ACT3")]
        private string _aircraftIATACode;
        [Entity(SerializedName = "ACT3", MaxLength = 3)]
        public virtual string AircraftIATACode
        {
            get { return _aircraftIATACode; }
            set
            {
                if (_aircraftIATACode != value)
                {
                    _aircraftIATACode = value;
                    OnPropertyChanged("AircraftIATACode");
                }
            }
        }

        //[Field(SerializedName = "ACT5")]
        private string _aircraftICAOCode;
        [Entity(SerializedName = "ACT5", MaxLength = 5)]
        public virtual string AircraftICAOCode
        {
            get { return _aircraftICAOCode; }
            set
            {
                if (_aircraftICAOCode != value)
                {
                    _aircraftICAOCode = value;
                    OnPropertyChanged("AircraftICAOCode");
                }
            }
        }

        private DateTime? _approvalDate;
        [Entity(SerializedName = "APRD", SourceDataType = typeof(String),IsMandatory=true)]
        public virtual DateTime? ApprovalDate
        {
            get { return _approvalDate; }
            set
            {
                if (_approvalDate != value)
                {
                    _approvalDate = value;
                    OnPropertyChanged("ApprovalDate");
                }
            }
        }

      
        private DateTime? _requestDate;
        [Entity(SerializedName = "REQD", SourceDataType = typeof(String))]
        public virtual DateTime? RequestDate
        {
            get { return _requestDate; }
            set
            {
                if (_requestDate != value)
                {
                    _requestDate = value;
                    OnPropertyChanged("RquestDate");
                }
            }
        }

        private string _agtName;
        [Entity(SerializedName = "AGNT", MaxLength = 64,IsMandatory=true)]
        public virtual string AgtName
        {
            get { return _agtName; }
            set
            {
                if (_agtName != value)
                {
                    _agtName = value;
                    OnPropertyChanged("AgtName");
                }
            }
        }

        private string _email;
        [Entity(SerializedName = "EMLE", MaxLength = 64)]
        public virtual string Email
        {
            get { return _email; }
            set
            {
                if (_email != value)
                {
                    _email = value;
                    OnPropertyChanged("Email");
                }
            }
        }

        private string _contact;
        [Entity(SerializedName = "TELX", MaxLength = 16)]
        public virtual string Contact
        {
            get { return _contact; }
            set
            {
                if (_contact != value)
                {
                    _contact = value;
                    OnPropertyChanged("Contact");
                }
            }
        }

        private string _operatorName;
        [Entity(SerializedName = "OPNA", MaxLength = 64,IsMandatory=true)]
        public virtual string OperatorName
        {
            get { return _operatorName; }
            set
            {
                if (_operatorName != value)
                {
                    _operatorName = value;
                    OnPropertyChanged("OperatorName");
                }
            }
        }
        

        private string _flightPurpose;
        [Entity(SerializedName = "PURP", MaxLength = 255,IsMandatory=true)]
        public virtual string FlightPurpose
        {
            get { return _flightPurpose; }
            set
            {
                if (_flightPurpose != value)
                {
                    _flightPurpose = value;
                    OnPropertyChanged("FlightPurpose");
                }
            }
        }

        private string _requestBay;
        [Entity(SerializedName = "RBAY", MaxLength = 64,IsMandatory=true)]
        public virtual string RequestBay
        {
            get { return _requestBay; }
            set
            {
                if (_requestBay != value)
                {
                    _requestBay = value;
                    OnPropertyChanged("RequestBay");
                }
            }
        }


    }
}
