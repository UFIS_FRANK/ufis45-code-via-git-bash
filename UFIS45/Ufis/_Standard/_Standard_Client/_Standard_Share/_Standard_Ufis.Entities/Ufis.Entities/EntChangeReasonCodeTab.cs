﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Entities
{
    [Entity(SerializedName = "CRCTAB")]
    public class EntChangeReasonCodeTab : BaseEntity
    {
        private string strCODE;
        private string strREMA;

        [Entity(SerializedName = "CODE")]
        public string DelayCode
        {
            get { return strCODE; }

            set
            {
                if (strCODE != value)
                {
                    strCODE = value;
                    OnPropertyChanged("DelayCode");
                }
            }
        }

        [Entity(SerializedName = "REMA")]
        public string Remark
        {
            get { return strREMA; }

            set
            {
                if (strREMA != value)
                {
                    strREMA = value;
                    OnPropertyChanged("Remark");
                }
            }
        }
    }
}
