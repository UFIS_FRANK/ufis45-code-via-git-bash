﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Entities
{  
    [Entity(SerializedName = "UAATAB")]
    public class EntDbUserAccountAuthorisation : ValidityBaseEntity
    { 
        private string _departmentName;
        private int _groupUrno;
        private int _featureUrno;
        private string _data;


        [Entity(SerializedName = "DPT1", MaxLength = 8)]
        public string DepartmentName
        {
            get { return _departmentName; }
            set
            {
                if (_departmentName != value)
                {
                    _departmentName = value;
                    OnPropertyChanged("DepartmentName");
                }
            }
        }

        [Entity(SerializedName = "GRPU", MaxLength = 32)]
        public int GroupUrno
        {
            get { return _groupUrno; }
            set
            {
                if (_groupUrno != value)
                {
                    _groupUrno = value;
                    OnPropertyChanged("GroupUrno");
                }
            }
        }

        [Entity(SerializedName = "UFEA")]
        public int FeatureUrno
        {
            get { return _featureUrno; }
            set
            {
                if (_featureUrno != value)
                {
                    _featureUrno = value;
                    OnPropertyChanged("FeatureUrno");
                }
            }
        }


        [Entity(SerializedName = "DATA", MaxLength = 32)]
        public string Data  
        {
            get { return _data; }
            set
            {
                if (_data != value)
                {
                    _data = value;
                    OnPropertyChanged("Data");
                }
            }
        }
    }
}
