﻿using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "UDLTAB")]
    public class EntDbUserDefinedLayout : ValidityBaseEntity
    {
        private string _applicationName;
        private string _section;
        private string _subsection;
        private string _name;
        private string _value;
        private string _accessRights;
        private string _userId;

        [Entity(SerializedName = "APPN", MaxLength = 32)]
        public string ApplicationName
        {
            get { return _applicationName; }
            set
            {
                if (_applicationName != value)
                {
                    _applicationName = value;
                    OnPropertyChanged("ApplicationName");
                }
            }
        }

        [Entity(SerializedName = "LSEC", MaxLength = 32)]
        public string Section
        {
            get { return _section; }
            set
            {
                if (_section != value)
                {
                    _section = value;
                    OnPropertyChanged("Section");
                }
            }
        }

        [Entity(SerializedName = "LSSE", MaxLength = 32)]
        public string Subsection
        {
            get { return _subsection; }
            set
            {
                if (_subsection != value)
                {
                    _subsection = value;
                    OnPropertyChanged("Subsection");
                }
            }
        }

        [Entity(SerializedName = "LNAM", MaxLength = 32)]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        [Entity(SerializedName = "LVAL", MaxLength = 2000)]
        public string Value
        {
            get { return _value; }
            set
            {
                if (_value != value)
                {
                    _value = value;
                    OnPropertyChanged("Value");
                }
            }
        }

        [Entity(SerializedName = "LACR", MaxLength = 2000)]
        public string AccessRights
        {
            get { return _accessRights; }
            set
            {
                if (_accessRights != value)
                {
                    _accessRights = value;
                    OnPropertyChanged("AccessRights");
                }
            }
        }

        [Entity(SerializedName = "PKNO", MaxLength = 32)]
        public string UserId
        {
            get { return _userId; }
            set
            {
                if (_userId != value)
                {
                    _userId = value;
                    OnPropertyChanged("UserId");
                }
            }
        }
    }
}
