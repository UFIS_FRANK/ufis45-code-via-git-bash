// InputGrid.h: interface for the CInputGrid class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INPUTGRID_H__26B83E42_EE83_11D3_93ED_00001C033B5D__INCLUDED_)
#define AFX_INPUTGRID_H__26B83E42_EE83_11D3_93ED_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CInputGrid : public CGXGridWnd  
{
public:
	CInputGrid();
	CInputGrid(CWnd *popParent);
	virtual ~CInputGrid();

protected:
	bool	bmSortAscend;
	bool	bmSortEnabled;
	bool	bmNowPasting;
	ROWCOL	imLastSortCol;
	ROWCOL	imLastFilledRow;
	CWnd	*pomParent;

	void SetParent(CWnd *popParent);
	void OnLButtonUp(UINT nFlags, CPoint point);
	void SortTable ( ROWCOL ipCol );
public:
	BOOL OnValidateCell(ROWCOL nRow, ROWCOL nCol);
	BOOL DeleteRow ( ROWCOL ipRow );
	BOOL SetStyleRange( const CGXRange& range, const CGXStyle* pStyle,
						GXModifyType mt, int nType= 0, 
						const CObArray* pCellsArray= NULL,
						UINT flags=GX_UPDATENOW, GXCmdType ctCmd=gxDo );
	BOOL Paste();



protected:
	//{{AFX_MSG(CInputGrid)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

#endif // !defined(AFX_INPUTGRID_H__26B83E42_EE83_11D3_93ED_00001C033B5D__INCLUDED_)
