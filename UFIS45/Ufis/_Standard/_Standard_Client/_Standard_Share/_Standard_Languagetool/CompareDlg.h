#if !defined(AFX_COMPAREDLG_H__6FA57FB1_EA95_11D3_93EA_00001C033B5D__INCLUDED_)
#define AFX_COMPAREDLG_H__6FA57FB1_EA95_11D3_93EA_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <GxGridTab.h>
#include <resource.h>
#include <CompTxtGrid.h>

// CompareDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCompareDlg dialog

class CCompareDlg : public CDialog
{
	DECLARE_DYNCREATE(CCompareDlg)
// Construction
public:
	CCompareDlg(CWnd* pParent = NULL);   // standard constructor
	~CCompareDlg();

// Dialog Data
	//{{AFX_DATA(CCompareDlg)
	enum { IDD = IDD_NEW_COMPARE };
	CComboBox	omRightCocoCb;
	CComboBox	omLeftCocoCb;
	CComboBox	omApplCb;
	int		imSelApplIdx;
	int		imSelLeftCoco;
	int		imSelRightCoco;
	BOOL	bmShowDifferences;
	//}}AFX_DATA
	UINT imDataStatus ;
protected:
	CCompTxtGrid omGrid;
	BOOL		*pbmHideArray;	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCompareDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
public:
	void FillGrid ();

// Implementation
protected:
	void IniComboBoxes ();
	void IniGrid ();
	void SetColWidths ();


	// Generated message map functions
	//{{AFX_MSG(CCompareDlg)
	afx_msg void OnSelendokLeftCb();
	afx_msg void OnSelendokRightCb();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelendokApplCb();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnShowDiffChk();
	afx_msg void OnCopy();
	afx_msg void OnClose();
	//}}AFX_MSG
	void OnGridChangeText( WPARAM nRow, LPARAM nCol );
	void OnGridSorted( WPARAM nRow, LPARAM nCol );
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMPAREDLG_H__6FA57FB1_EA95_11D3_93EA_00001C033B5D__INCLUDED_)
