#if !defined(AFX_GXGRIDTAB_H__0B02FE9B_7E37_11D2_A14D_0000B4984BBE__INCLUDED_)
#define AFX_GXGRIDTAB_H__0B02FE9B_7E37_11D2_A14D_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GxGridTab.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GxGridTab window
#ifndef _GXRESRC_H_
#include <gxresrc.h>
#endif

#ifndef _GXVW_H_
#include <gxvw.h>
#endif

#ifndef _GXEXT_H_
#include <gxext.h>
#endif

#ifndef _GXMSG_H_
#include <gxmsg.h>
#endif


#define GX_ROWCOL_CHANGED		(WM_USER + 9000)
#define GX_CELL_BUTTONCLICKED	(WM_USER + 9001)
#define GX_GRID_SORTED			(WM_USER + 9002)


class GxGridTab : public CGXGridWnd
{
DECLARE_DYNCREATE(GxGridTab)

// Construction
public:
	GxGridTab();
	GxGridTab(CWnd *popParent);
	void SetParent(CWnd *popParent);
	CWnd *pomParent;
// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GxGridTab)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~GxGridTab();
	virtual void OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol);
	virtual void OnLButtonUp(UINT nFlags, CPoint point);			// wes
	virtual BOOL OnStartTracking(ROWCOL nRow, ROWCOL nCol, int nTrackingMode);	// wes
	virtual void OnEndTracking(ROWCOL nRow, ROWCOL nCol, int nTrackingMode, CSize& size);	// wes
	virtual BOOL OnEndEditing(ROWCOL nRow, ROWCOL nCol);	// wes
	virtual BOOL OnStartEditing(ROWCOL nRow, ROWCOL nCol);	// wes
	virtual BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);	// wes
	virtual void OnTextNotFound(LPCTSTR);	// wes
	virtual BOOL OnStartSelection(ROWCOL nRow, ROWCOL nCol, UINT flags, CPoint point);	// wes
//	virtual BOOL OnGridKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);	// wes
	virtual BOOL ProcessKeys(CWnd* pSender, UINT nMessage, UINT nChar, UINT nRepCnt = 1, UINT flags = 0);
	// Generated message map functions
	virtual void SortTable ( ROWCOL ipCol );
protected:
	BOOL OnTrackColWidth(ROWCOL nCol);

protected:
	//{{AFX_MSG(GxGridTab)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GXGRIDTAB_H__0B02FE9B_7E37_11D2_A14D_0000B4984BBE__INCLUDED_)
