// Amp_testCtl.cpp : Implementation of the CAmp_testCtrl ActiveX Control class.

#include <stdafx.h>
#include <TrafficLight.h>


#include <Amp_testCtl.h>
#include <Auto_test.h>
#include <Amp_testPpg.h>
#include <Proppage2.h>

 
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



IMPLEMENT_DYNCREATE(CAmp_testCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CAmp_testCtrl, COleControl)
	//{{AFX_MSG_MAP(CAmp_testCtrl)
	ON_WM_LBUTTONDOWN()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CAmp_testCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CAmp_testCtrl)
	DISP_PROPERTY_NOTIFY(CAmp_testCtrl, "BuildDate", m_buildDate, OnBuildDateChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CAmp_testCtrl, "Version", m_version, OnVersionChanged, VT_BSTR)
	DISP_FUNCTION(CAmp_testCtrl, "SetDispatch", SetDispatch, VT_EMPTY, VTS_DISPATCH)
	DISP_FUNCTION(CAmp_testCtrl, "Init", Init, VT_I4, VTS_NONE)
	DISP_FUNCTION(CAmp_testCtrl, "SetDispBc", SetDispBc, VT_EMPTY, VTS_DISPATCH)
	DISP_FUNCTION(CAmp_testCtrl, "SetColor", SetColor, VT_BOOL, VTS_I2)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CAmp_testCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
	//following lines are for the property exchange...
	DISP_PROPERTY_EX(CAmp_testCtrl, "Interval", GetInterval, SetInterval, VT_I4)
	DISP_PROPERTY_EX(CAmp_testCtrl, "Note", GetNote, SetNote, VT_BOOL)
	DISP_PROPERTY_EX(CAmp_testCtrl, "YTime" , GetYellowTime, SetYellowTime,VT_I4)
	DISP_PROPERTY_EX(CAmp_testCtrl, "RTime" , GetRedTime, SetRedTime, VT_I4)
	DISP_PROPERTY_EX(CAmp_testCtrl, "Radio" , GetGraphicState, SetGraphicState, VT_I4)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CAmp_testCtrl, COleControl)
	//{{AFX_EVENT_MAP(CAmp_testCtrl)
	EVENT_CUSTOM("time", FireTime, VTS_I4)
	EVENT_CUSTOM_ID("Click", DISPID_CLICK, FireClick, VTS_NONE)
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CAmp_testCtrl, 2)
	PROPPAGEID(CAmp_testPropPage::guid)
	PROPPAGEID(CProppage2::guid)
END_PROPPAGEIDS(CAmp_testCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CAmp_testCtrl, "AMPTEST.AmptestCtrl.1",
	0x59bec158, 0x923c, 0x11d5, 0x99, 0x69, 0, 0, 0x86, 0x50, 0x98, 0xd4)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CAmp_testCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DAmp_test =
		{ 0x59bec156, 0x923c, 0x11d5, { 0x99, 0x69, 0, 0, 0x86, 0x50, 0x98, 0xd4 } };
const IID BASED_CODE IID_DAmp_testEvents =
		{ 0x59bec157, 0x923c, 0x11d5, { 0x99, 0x69, 0, 0, 0x86, 0x50, 0x98, 0xd4 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwAmp_testOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CAmp_testCtrl, IDS_AMP_TEST, _dwAmp_testOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CAmp_testCtrl::CAmp_testCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CAmp_testCtrl

BOOL CAmp_testCtrl::CAmp_testCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_AMP_TEST,
			IDB_AMP_TEST,
			afxRegApartmentThreading,
			_dwAmp_testOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}



/////////////////////////////////////////////////////////////////////////////
// CAmp_testCtrl::CAmp_testCtrl - Constructor

CAmp_testCtrl::CAmp_testCtrl()
{
	AfxEnableControlContainer ();
	EnableAutomation();
	InitializeIIDs(&IID_DAmp_test, &IID_DAmp_testEvents);
	pomAuto_test = NULL;
	
	bmPropertyNote = FALSE;
	m_version = CString("4.5.0.2");
	m_buildDate = CString(__DATE__) + " - " + CString(__TIME__);

	CDC memDC;
	memDC.CreateCompatibleDC (NULL);
	bitmapTlc.CreateBitmap (32,32,1,memDC.GetDeviceCaps (BITSPIXEL),NULL);
	
	bitmapGreen.CreateBitmap (32,32,1,memDC.GetDeviceCaps (BITSPIXEL),NULL);
	bitmapGreen.Detach();
	bitmapGreen.LoadBitmap (IDB_AMP_KL_GR);//IDB_AMP_GR_HI
	
	bitmapYellow.CreateBitmap (32,32,1,memDC.GetDeviceCaps (BITSPIXEL),NULL);
	bitmapYellow.Detach();
	bitmapYellow.LoadBitmap (IDB_AMP_KL_GE);//IDB_AMP_GE_HI
	
	bitmapRed.CreateBitmap (32,32,1,memDC.GetDeviceCaps (BITSPIXEL),NULL);
	bitmapRed.Detach();
	bitmapRed.LoadBitmap (IDB_AMP_KL_RO);//IDB_AMP_RO_HI
	memDC.Detach ();
	memDC.DeleteDC ();
	pActBitmap = &bitmapRed;

	imTry = 0;
	bmSomething_wrong = false;
	bmSent = false;
	bsomebc = false;
	bmReceived = false;
	bmWait = false;
	pmIDispUfisCom = NULL;
	pmIDispBcPrxy = NULL;
	smTlcState = 3;// red light by default
	// the colors of the traffic light are controled by the smTlcState param
	// smTlcState	= 1			: green
	// smTlcState	= 2			: yellow
	// smTlcState !=1 and !=2	: red
	// change color by changing smTlcState or by calling �SetTlc� with params:
	// short <1,2,3> and bool refresh (<true> for update picture)
	SetTlc(smTlcState, false);

	lmPropertyAppearance = 1;// default state is the small traffic light
	lmPropIntervall	 = 60;
	imPropTimeYellow = 50;
	imPropTimeRed	 = 500;
}


/////////////////////////////////////////////////////////////////////////////
// CAmp_testCtrl::~CAmp_testCtrl - Destructor

CAmp_testCtrl::~CAmp_testCtrl()
{
	// cancel server connection 
	static const IID IID_BcPrxyEvents = {0x68e25ff2, 0x6790, 0x11d4, {0x90, 0x3b, 0x0, 0x01, 0x02, 0x04, 0xaa, 0x51}};  
	if (m_dwCookie != 0)
	{
		AfxConnectionUnadvise(pmIDispBcPrxy,IID_BcPrxyEvents,pIUnknown_Auto,FALSE,m_dwCookie);
		m_dwCookie = 0;


		ULONG oldRefCount = pIUnknown_Auto->Release();
		pIUnknown_Auto = NULL;
	}

	if (this->pmIDispBcPrxy != NULL)
	{
		ULONG oldRefCount = pmIDispBcPrxy->Release();
		this->pmIDispBcPrxy = NULL;
	}

	if (this->pmIDispUfisCom != NULL)
	{
		ULONG oldRefCount = pmIDispUfisCom->Release();
		this->pmIDispUfisCom = NULL;
	}

	// delete Traffic Light Bitmap
	bitmapTlc.Detach ();
	bitmapTlc.DeleteObject ();
	bitmapGreen.Detach();
	bitmapGreen.DeleteObject();
	bitmapYellow.Detach();
	bitmapYellow.DeleteObject();
	bitmapRed.Detach();
	bitmapRed.DeleteObject();
}



/////////////////////////////////////////////////////////////////////////////
// CAmp_testCtrl::OnDraw - Drawing function

void CAmp_testCtrl::OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.
	CDC memDC;
	memDC.CreateCompatibleDC (pdc);
	pOldBitmap = memDC.SelectObject (pActBitmap); //(&bitmapTlc); // bitmapTlc contains actual traffic light picture
	pdc->BitBlt (0,0,32,32,&memDC,0,0,SRCCOPY);
	memDC.SelectObject (pOldBitmap);
	memDC.DeleteDC ();
	
}


/////////////////////////////////////////////////////////////////////////////
// CAmp_testCtrl::DoPropExchange - Persistence support

void CAmp_testCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.
	PX_Long (pPX, _T("Interval"), lmPropIntervall, 60);
	PX_Bool (pPX, _T("Note"), bmPropertyNote, FALSE);
	PX_Long (pPX, _T("YTime"), imPropTimeYellow, 50);
	PX_Long (pPX, _T("RTime"), imPropTimeRed, 500);
	PX_Long (pPX, _T("Radio"), lmPropertyAppearance, 1);	
}


/////////////////////////////////////////////////////////////////////////////
// CAmp_testCtrl::OnResetState - Reset control to default state

void CAmp_testCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CAmp_testCtrl::AboutBox - Display an "About" box to the user

void CAmp_testCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_AMP_TEST);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CAmp_testCtrl message handlers

bool CAmp_testCtrl::StartBc()
{
	HRESULT res;
	res = S_OK;
	//IID of interface BcPrxyEvents
	static const IID IID_BcPrxyEvents = {0x68e25ff2, 0x6790, 0x11d4, {0x90, 0x3b, 0x0, 0x01, 0x02, 0x04, 0xaa, 0x51}};  
	
	//create instance of automation class CAuto
	if (pomAuto_test == NULL)
	{
		pomAuto_test = new Auto_test();
		pomAuto_test->SetControl(this);
		
	}
	//requesting pointer to IUnknown of automation class 
	if (pomAuto_test)
	{
		pIUnknown_Auto = pomAuto_test->GetIDispatch (FALSE);
	}
	
	//connect server to automation class
	int erg = AfxConnectionAdvise(pmIDispBcPrxy,IID_BcPrxyEvents,pIUnknown_Auto,FALSE,&m_dwCookie);
						
	if (erg == 0)
	{
		MessageBox(_T("TLC : no connection !"));
		return false;
	}
	
	SetTlc(2,true);
	return true;
}

void CAmp_testCtrl::SetTlc(short sColor,bool refresh)
{
	// loads picture according to requested color into bitmapTlc
	//bitmapTlc.Detach ();
	
	smTlcState = sColor;
	switch (sColor)
	{
	case 1:
		pActBitmap = &bitmapGreen;
		//bitmapTlc.LoadBitmap (IDB_AMP_GR_HI);
		break;
	case 2:
		pActBitmap = &bitmapYellow;
		//bitmapTlc.LoadBitmap (IDB_AMP_GE_HI);
		break;
	default:
		pActBitmap = &bitmapRed;
		//bitmapTlc.LoadBitmap (IDB_AMP_RO_HI);
		smTlcState = 3;
		break;
	}
	
	if (refresh) // draw new picture of traffic light ?
	{
		Invalidate ();
	}
	
	return;
}

void CAmp_testCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	
	CRgn rgn;
	rgn.CreateRectRgn (0,0,32,97);
	if (rgn.PtInRegion (point))
	{
		if (!bmSent && !bmWait)
		{
			SendRequest();
		}
		
		// not used at this time
		
		//////////////////////////////////////////////////////////////////
		
	}
	COleControl::OnLButtonDown(nFlags, point);
}

bool CAmp_testCtrl::StartUfisCom()
{
	
	// everything�s done by SetDispatch(..)
	// but is still called by the method Init()
	
	// is to be deleted ...
	
	
	SetTlc(1,true);
	
	return true;

}



void CAmp_testCtrl::OnTimer(UINT nIDEvent) 
{
	COleControl::OnTimer(nIDEvent);
	
	// Timer2 raises the elapsed time when a request (ping) to the database is out till
	// the broadcast is received or the elapsed time gets out of range
	// Timer2 starts when SendRequest() is called
	if (nIDEvent == IDT_TIMER2)
	{
		
		if (bmSent)
		{
			lmElapsedTime++;
			
			if (lmElapsedTime > MAX_DELAY_TIME) 
			{
				KillTimer(IDT_TIMER2);
				lmElapsedTime = MAX_DELAY_TIME;
				bmSent = false;
				bmReceived = true;

			}

			if (bmReceived == true)//broadcast has arrived
			{
				bmSent = false;
				
				Calculate(lmElapsedTime);
			}
			
		}
	}

	// Timer1 sets the half time period for looking for broadcasts 
	// Timer1 starts when Init() is called from the container
	// Timer1 restarts when the period was changed by using the controls property page
	// The Timer1 event raises two times in one period. The first time the search for
	// broadcasts gets reset. When the second timer event raises and there is still no
	// broadcast in (indicated through "bsomebc" ), the SendRequest - mechanism starts
	else if (nIDEvent == IDT_TIMER1)
	{
		if (bsomebc && !bmSent && !bmWait)// some broadcasts arrived and no demand, so show green
		{
			smTlcState = 1;
			SetTlc(smTlcState,true);
		}
		
		if (imTry == 0 && !bmSent && !bmWait)// first timer event -> start looking for some bc�s
		{
			bsomebc = false;
			imTry = 1;
		}
		else // second timer event
		{
			if (!bsomebc && !bmSent && !bmWait)// still not any broadcasts -> so start SBC
			{
				SendRequest();
			}
			
			imTry = 0;
		}
		
	}
	
	
}

void CAmp_testCtrl::SendRequest()
{
	// Starts timer2	
	lmElapsedTime = 0;
	bmSent = true;
	bmReceived = false;
	SetTlc(2,true);// switch tlc to yellow (not neccessary)
	if (!bmSomething_wrong)
	{
		SetTimer (IDT_TIMER2,1,NULL);
	}
}

void CAmp_testCtrl::Calculate(long lduration)
{
	//is called from Auto_test::OnBroadcast() or from OnTimer() 
	// sets the color of the traffic light according to the elapsed time
	CString strduration;
	strduration.Format(_T("%d"), (int)lduration);
	
	if (lduration > imPropTimeRed) 
	{
		SetTlc(3,true);// --> red light
		if (bmPropertyNote == TRUE)
		{
			MessageBox (_T("no broadcasts in desired time limit !"));
		}

	}
	if (lduration <= imPropTimeRed) SetTlc(2,true); // --> yellow light
	if (lduration <= imPropTimeYellow) SetTlc(1,true); // --> green light
	FireTime(lduration);// inform container about elapsed time (it may want to know)
	// reset for new request...
	lmElapsedTime = 0;
	bmSent = false;
	bmReceived = false;
	KillTimer(IDT_TIMER2);
	
	return;
}

void CAmp_testCtrl::Setreceived(bool state)
{
	bmReceived = state;
	return;
}

bool CAmp_testCtrl::GetSent()
{
	return bmSent;
}

void CAmp_testCtrl::SetDispatch(LPDISPATCH FAR pIDispatch) 
{
	this->pmIDispUfisCom = pIDispatch;
	ULONG oldRefCount = this->pmIDispUfisCom->AddRef();
}

long CAmp_testCtrl::Init() 
{
	bmSomething_wrong = false;	
	//connect BcPrxy
	if (!StartBc())
	{
		MessageBox (_T("couldn�t connect to BcProxy !"));
		bmSomething_wrong = true;
	}
	
	if (this->pmIDispUfisCom != NULL && !bmSomething_wrong)
	{
		if (!StartUfisCom())
		{
			MessageBox(_T("TLC : Failed to start UfisCom !"));
			bmSomething_wrong = true;
		}
	}

	if (!bmSomething_wrong)
	{
		SetTimer (IDT_TIMER1,(1000*lmPropIntervall),NULL);		
	}

	if (!bmSomething_wrong)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

void CAmp_testCtrl::SetDispBc(LPDISPATCH FAR pDispBc) 
{
	// take pointer to BcPrxy�s interface from container
	pmIDispBcPrxy = pDispBc;
	ULONG oldRefCount = pmIDispBcPrxy->AddRef();
}






//////////////////////
// Transfer methods for the controls properties

void CAmp_testCtrl::SetInterval (long lNewTime)
{
	lmPropIntervall = lNewTime;
	KillTimer (IDT_TIMER1);
	SetTimer(IDT_TIMER1,(1000*lmPropIntervall),NULL);
}

long CAmp_testCtrl::GetInterval ()
{
	return lmPropIntervall;
}

void CAmp_testCtrl::SetNote(BOOL notify)
{
	bmPropertyNote = notify;
}

BOOL CAmp_testCtrl::GetNote()
{
	return bmPropertyNote;
}

void CAmp_testCtrl::SetYellowTime (long lNewTime)
{
	imPropTimeYellow = lNewTime;
}

long CAmp_testCtrl::GetYellowTime ()
{
	return imPropTimeYellow;
}

void CAmp_testCtrl::SetRedTime (long lNewTime)
{
	imPropTimeRed = lNewTime;
}

long CAmp_testCtrl::GetRedTime ()
{
	return imPropTimeRed;
}

void CAmp_testCtrl::SetGraphicState (long lNewState)
{
	//CString strState;
	//strState.Format (_T("%d"), (int)lNewState);
	//MessageBox (strState);
	if (lNewState == 1)
	{
		lmPropertyAppearance = 2;// colored field
		bitmapGreen.Detach();
		bitmapGreen.LoadBitmap (IDB_AMP_BUT_GR);
		bitmapYellow.Detach();
		bitmapYellow.LoadBitmap (IDB_AMP_BUT_GE);
		bitmapRed.Detach();
		bitmapRed.LoadBitmap (IDB_AMP_BUT_RO);
	}
	else
	{
		lmPropertyAppearance = 1;// small traffic light
		bitmapGreen.Detach();
		bitmapGreen.LoadBitmap (IDB_AMP_KL_GR);
		bitmapYellow.Detach();
		bitmapYellow.LoadBitmap (IDB_AMP_KL_GE);
		bitmapRed.Detach();
		bitmapRed.LoadBitmap (IDB_AMP_KL_RO);
	}

}

long CAmp_testCtrl::GetGraphicState ()
{
	if (lmPropertyAppearance == 1)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}
//////////////////////////////////////////

BOOL CAmp_testCtrl::SetColor(short sColor) 
{
	if (sColor == 0)
	{
		// restart request mechanism
		bmWait = false;
		bsomebc = false;
		bmSent = false;
		imTry = 0;
		SetTimer(IDT_TIMER1,(1000*lmPropIntervall),NULL);
		return TRUE;
	}
	else
	{
		// stop requesting-process and show desired color
		bmWait = true;
		KillTimer (IDT_TIMER1);
		bmSent = false;
		bmReceived = false;
		SetTlc(sColor,true); // don�t care of sColor : SetTlc will switch to red if sColor <> 1,2
	}
	return TRUE;
}

bool CAmp_testCtrl::GetWait()
{
	return bmWait;
}

void CAmp_testCtrl::SetSomeBc(bool bstate)
{
	
	bsomebc = bstate;
	return;
}

void CAmp_testCtrl::OnBuildDateChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CAmp_testCtrl::OnVersionChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}
