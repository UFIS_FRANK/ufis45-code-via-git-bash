// DlgProxy.h : header file
//

#if !defined(AFX_DLGPROXY_H__8942EEE2_9796_11D5_9970_0000865098D4__INCLUDED_)
#define AFX_DLGPROXY_H__8942EEE2_9796_11D5_9970_0000865098D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CLb_containerDlg;

/////////////////////////////////////////////////////////////////////////////
// CLb_containerDlgAutoProxy command target

class CLb_containerDlgAutoProxy : public CCmdTarget
{
	DECLARE_DYNCREATE(CLb_containerDlgAutoProxy)

	CLb_containerDlgAutoProxy();           // protected constructor used by dynamic creation

// Attributes
public:
	CLb_containerDlg* m_pDialog;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLb_containerDlgAutoProxy)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CLb_containerDlgAutoProxy();

	// Generated message map functions
	//{{AFX_MSG(CLb_containerDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CLb_containerDlgAutoProxy)

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CLb_containerDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPROXY_H__8942EEE2_9796_11D5_9970_0000865098D4__INCLUDED_)
