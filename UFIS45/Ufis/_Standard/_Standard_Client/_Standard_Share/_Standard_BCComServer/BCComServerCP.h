#ifndef _BCCOMSERVERCP_H_
#define _BCCOMSERVERCP_H_

#undef	MT
#define	MT

#ifndef	MT
template <class T>
class CProxy_IBcComAtlEvents : public IConnectionPointImpl<T, &DIID__IBcComAtlEvents, CComDynamicUnkArray>
#else

#include "ATLCPImplMT.h"

template <class T>
class CProxy_IBcComAtlEvents : public IConnectionPointImplMT<T, &DIID__IBcComAtlEvents, CComDynamicUnkArray>
#endif
{
	//Warning this class may be recreated by the wizard.
public:
	HRESULT Fire_OnLostBc(INT ilErr)
	{
		CComVariant varResult;
		T* pT = static_cast<T*>(this);
		int nConnectionIndex;
		CComVariant* pvars = new CComVariant[1];
		int nConnections = m_vec.GetSize();
		
		for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
		{
#ifndef	MT
			pT->Lock();
			CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
			pT->Unlock();
#else
			CComPtr<IUnknown> sp;
			sp.Attach(GetInterfaceAt(nConnectionIndex));
#endif
			IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
			if (pDispatch != NULL)
			{
				VariantClear(&varResult);
				pvars[0] = ilErr;
				DISPPARAMS disp = { pvars, NULL, 1, 0 };
				pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &varResult, NULL, NULL);
			}
		}
		delete[] pvars;
		return varResult.scode;
	
	}
	HRESULT Fire_OnLostBcEx(BSTR strException)
	{
		CComVariant varResult;
		T* pT = static_cast<T*>(this);
		int nConnectionIndex;
		CComVariant* pvars = new CComVariant[1];
		int nConnections = m_vec.GetSize();
		
		for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
		{
#ifndef	MT
			pT->Lock();
			CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
			pT->Unlock();
#else
			CComPtr<IUnknown> sp;
			sp.Attach(GetInterfaceAt(nConnectionIndex));
#endif
			IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
			if (pDispatch != NULL)
			{
				VariantClear(&varResult);
				pvars[0] = strException;
				DISPPARAMS disp = { pvars, NULL, 1, 0 };
				pDispatch->Invoke(0x2, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &varResult, NULL, NULL);
			}
		}
		delete[] pvars;
		return varResult.scode;
	
	}
	HRESULT Fire_OnBc(LONG size, LONG currPackage, LONG totalPackages, BSTR strData)
	{
		CComVariant varResult;
		T* pT = static_cast<T*>(this);
		int nConnectionIndex;
		CComVariant* pvars = new CComVariant[4];
		int nConnections = m_vec.GetSize();
		
		for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
		{
#ifndef	MT
			pT->Lock();
			CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
			pT->Unlock();
#else
			CComPtr<IUnknown> sp;
			sp.Attach(GetInterfaceAt(nConnectionIndex));
#endif
			IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
			if (pDispatch != NULL)
			{
				VariantClear(&varResult);
				pvars[3] = size;
				pvars[2] = currPackage;
				pvars[1] = totalPackages;
				pvars[0] = strData;
				DISPPARAMS disp = { pvars, NULL, 4, 0 };
				pDispatch->Invoke(0x3, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &varResult, NULL, NULL);
			}
		}
		delete[] pvars;
		return varResult.scode;
	
	}
};
#endif