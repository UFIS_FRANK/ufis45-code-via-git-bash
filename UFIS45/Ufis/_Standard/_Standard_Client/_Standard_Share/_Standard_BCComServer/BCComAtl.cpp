// BcComAtl.cpp : Implementation of CBcComAtl
#include "stdafx.h"
#include "BcComServer.h"
#include "BcComAtl.h"
#include "BlockingSocket.h"
#include "MonitorDlg.h"


char		CBcComAtl::pcmHostName[512];
int			CBcComAtl::imTcpPort = 3358;
CWinThread*	CBcComAtl::pomWorkerThread = NULL;
CString		CBcComAtl::m_bCQueueID = "";
BOOL		CBcComAtl::m_amIConnected = FALSE;
HANDLE		CBcComAtl::hEventWorkerThreadExit = NULL;

/////////////////////////////////////////////////////////////////////////////
// CBcComAtl
CBcComAtl::CBcComAtl()
{
	lmMaxBCRecords			=	0;
	lmBCSpoolTimer			=	100;
	this->pomSendThread		=	NULL;
	this->hEvent			=	NULL;
	bmTimerIsSet			=	false;
	bmSendSuspended			=	false;
	lmItemsReceived			=	0;
	lmItemsSend				=	0;

	if (CBcComAtl::hEventWorkerThreadExit == NULL)
	{
		CBcComAtl::hEventWorkerThreadExit = CreateEvent(NULL,false,false,"CBcComAtlWorkerThreadExit");
	}

	if (CBcComServerApp::pogMonitorDlg->IsMultiThreaded())
	{
		this->hEvent = CreateEvent(NULL,false,false,"CBcComAtl");
	}

	char pclConfigPath[256];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	lmCurrWriteCount		=	0;
	lmCurrFileNo			=	0;

	lmMaxBCRecords = ::GetPrivateProfileInt("GLOBAL", "EXTBCLOG", 0, pclConfigPath);
	lmBCSpoolTimer = ::GetPrivateProfileInt("GLOBAL", "BCSPOOLTIMER", 100, pclConfigPath);

	if (CBcComAtl::pomWorkerThread == NULL)
	{
		CBcComAtl::imTcpPort = ::GetPrivateProfileInt("GLOBAL", "BCTCPPORT",3358, pclConfigPath);
		::GetPrivateProfileString("GLOBAL", "HOSTNAME", "", CBcComAtl::pcmHostName, sizeof CBcComAtl::pcmHostName, pclConfigPath);
	}

	ProxyInstance* prlProxyInstance = new ProxyInstance();
	prlProxyInstance->pomBcCom = this;
	CBcComServerApp::ogAtlConnections.Add(prlProxyInstance);

}

CBcComAtl::~CBcComAtl()
{
	// TODO: Cleanup your control's instance data here.
	if (this->pomSendThread != NULL)
	{
		this->pomSendThread->SuspendThread();
		delete this->pomSendThread;
		this->pomSendThread = NULL;
	}

	if (this->hEvent != NULL)
	{
		CloseHandle(this->hEvent);
	}

	if(lmMaxBCRecords > 0)
	{
		of.close();
	}
	mapFilter.RemoveAll();

	for (int i = 0; i < CBcComServerApp::ogAtlConnections.GetSize(); i++)
	{
		if (CBcComServerApp::ogAtlConnections[i].pomBcCom == this)
		{
			CBcComServerApp::ogAtlConnections.DeleteAt(i);
			break;
		}
	}

	if (CBcComServerApp::ogAtlConnections.GetSize() == 0)
	{
		if (CBcComAtl::m_bCQueueID.GetLength() > 0)
		{
			CBcComAtl::StopConnection(CBcComAtl::pcmHostName,CBcComAtl::imTcpPort,CBcComAtl::m_bCQueueID);
			if (CBcComAtl::pomWorkerThread != NULL)
			{
				WaitForSingleObject(CBcComAtl::hEventWorkerThreadExit,1000);
			}
		}

		if (CBcComAtl::pomWorkerThread != NULL)
		{
			ULONG ulExitCode;
			if (::GetExitCodeThread(CBcComAtl::pomWorkerThread->m_hThread,&ulExitCode) && ulExitCode == STILL_ACTIVE)
			{
				DWORD dwStatus = CBcComAtl::pomWorkerThread->SuspendThread();
			}

			delete CBcComAtl::pomWorkerThread;
			CBcComAtl::pomWorkerThread = NULL;

		}

		CloseHandle(CBcComAtl::hEventWorkerThreadExit);

		::PostQuitMessage(0);	
	}
	else
	{
		CBcComServerApp::pogMonitorDlg->TransmitFilter();	
	}

}

STDMETHODIMP CBcComAtl::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IBcComAtl
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CBcComAtl::SetPID(BSTR strAppl,BSTR strPID)
{
	// TODO: Add your implementation code here
	this->omAppl = CString(strAppl);
	this->omPID  = CString(strPID);

	CString olFileName;

	char *pclTmpPath = NULL;
	CString olTmpPath = "C:\\TMP";
    if((pclTmpPath = getenv("UFISTMP")) != NULL)
	{
		olTmpPath = CString(pclTmpPath);
	}

	olFileName.Format("%s\\%s_BC_%s_%ld.log",olTmpPath,this->omAppl,this->omPID,lmCurrFileNo);

	omCurrFileName = olFileName;

	if (lmMaxBCRecords > 0)
	{
		of.open(olFileName.GetBuffer(0), ios::out);
	}

	return S_OK;
}

STDMETHODIMP CBcComAtl::SetFilter(BSTR strTable, BSTR strCommand, BOOL bOwnBC, BSTR application)
{
	// TODO: Add your implementation code here
	this->mapFilter.SetAt(CString(strTable),(void *)strTable);

	return S_OK;
}

STDMETHODIMP CBcComAtl::TransmitFilter()
{
	CBcComServerApp::pogMonitorDlg->TransmitFilter();	
	return S_OK;
}

void	CBcComAtl::TransmitFilter(const CMapStringToPtr& ropMapFilter)
{
	// TODO: Add your implementation code here
	// check, if we do have a valid queue id
	if (CBcComAtl::m_bCQueueID.GetLength() == 0)
		return;

	CString olNewFilter;
	int i =0;

	for (POSITION pos2 = ropMapFilter.GetStartPosition(); pos2 != NULL;)
	{
		CString olString;
		CString *p = NULL;
		ropMapFilter.GetNextAssoc(pos2,olString,(void *&)p);
		if (!olString.IsEmpty())
			olNewFilter += olString + ";";
	}

	if (olNewFilter.GetLength() > 0)
	{
		olNewFilter = olNewFilter.Left(olNewFilter.GetLength()-1);
	}


	CBlockingSocket sock;
	CSockAddr addr;
	try
	{

		if (sock.Create() == TRUE)
		{
			addr = sock.GetHostByName(CBcComAtl::pcmHostName,CBcComAtl::imTcpPort);

			if (sock.Connect(addr) == TRUE)
			{
				int ilLen = CBcComAtl::m_bCQueueID.GetLength();
				int ilFilterLen = olNewFilter.GetLength();
				char pclS[10000]="";
				sprintf(pclS, "{=TOT=}%09d{=CMD=}BCKEY{=QUE=}%s{=DAT=}%s", ilLen + 42 + ilFilterLen, CBcComAtl::m_bCQueueID.GetBuffer(0), olNewFilter.GetBuffer(0));
				for (int i = 0; i < 10; i++)
				{
					int err = sock.Send(pclS, ilLen + 42 + ilFilterLen);
					if (err != ilLen + 42 + ilFilterLen)
					{
						int ilError = WSAGetLastError();
						TRACE("Send filter failed with ec = %d\n",ilError);
					}
					else
					{
						TRACE("Filter successfully sent, i = %d\n",i);
						break;
					}
	//				TRACE("Filter=%s,err=%d\n",pclS,err);
				}
			}
		}

	}
	catch(const char* e)
	{
		CString olMsg;
		int ilErr = sock.GetLastError();
		if(ilErr != 0)
		{
			olMsg.Format("Transmit Filter: CEDA-Socket Error: %d", ilErr);
			AfxMessageBox(olMsg);
		}
		else
		{
			olMsg.Format("Transmit Filter: Exception: %s occured", e);
			AfxMessageBox(olMsg);
		}
	}

	sock.Cleanup();

}

STDMETHODIMP CBcComAtl::StartBroadcasting()
{
	// TODO: Add your implementation code here
	if (CBcComAtl::pomWorkerThread == NULL)
	{
		if (CBcComAtl::CreateThread())
		{
			CBcComAtl::pomWorkerThread->m_bAutoDelete = FALSE;
			CBcComAtl::pomWorkerThread->ResumeThread();
		}

		// main thread must wait until worker thread has got the queue id !
		DWORD dwStatus = WaitForSingleObject(CBcComAtl::hEventWorkerThreadExit,1000);
		if (dwStatus != WAIT_TIMEOUT)
		{
			delete CBcComAtl::pomWorkerThread;
			CBcComAtl::pomWorkerThread = NULL;
			return 4711;
		}
	}
	else 
	{
		ULONG ulExitCode;
		if (::GetExitCodeThread(CBcComAtl::pomWorkerThread->m_hThread,&ulExitCode) && ulExitCode == STILL_ACTIVE)
		{
			CBcComAtl::pomWorkerThread->ResumeThread();
		}
		else
		{
			return S_OK;				
		}
	}

	if (CBcComServerApp::pogMonitorDlg->IsMultiThreaded())
	{
		if (this->pomSendThread == NULL)
		{
			if (CreateSendThread())
			{
				this->pomSendThread->ResumeThread();
			}
		}
		else
		{
			this->pomSendThread->ResumeThread();
		}
	}

	this->bmSendSuspended = false;

	return S_OK;
}

STDMETHODIMP CBcComAtl::StopBroadcasting()
{
	// TODO: Add your implementation code here
	if (this->pomSendThread != NULL)
	{
		this->pomSendThread->SuspendThread();
	}

	this->bmSendSuspended = true;

	return S_OK;
}

bool CBcComAtl::CreateThread()
{
	CBcComAtl::pomWorkerThread = AfxBeginThread(WorkerFunction,NULL,THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED,NULL);
	if (CBcComAtl::pomWorkerThread != NULL)
	{
		return true;
	}
	else
		return false;
}

UINT CBcComAtl::WorkerFunction( LPVOID pParam )
{
	// initialize COM
	HRESULT hr = CoInitializeEx(NULL,COINIT_MULTITHREADED);
	if (FAILED(hr))
	{
		// Worker thread has been shut down..
		SetEvent(CBcComAtl::hEventWorkerThreadExit);
		AfxEndThread(1);
	}

	CString olMsg;
	char pclRetFirst[100];
	int	ilErr = 0;
	CBlockingSocket sock;
	CSockAddr addr;
	STR_DESC rlMsg;
	rlMsg.Value = NULL;
	rlMsg.AllocatedSize = 0;
	rlMsg.UsedLen = 0;

	FILE *fp = NULL;

	char pclConfigPath[256];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

resume:
	try
	{
		char pclTmpText[512];
		GetPrivateProfileString("GLOBAL","THREADBCLOG","",pclTmpText, sizeof pclTmpText, pclConfigPath);
		if (strlen(pclTmpText) > 0)
		{
			fp = fopen(pclTmpText,"wt");
		}

		if (!sock.Create())
		{
			throw "Socket creation failed";
		}

		addr = sock.GetHostByName(CBcComAtl::pcmHostName,CBcComAtl::imTcpPort);

		if (!sock.Connect(addr))
		{
			throw "Socket connection failed";
		}

		int len = sock.Send("{=TOT=}       28{=CMD=}BCOUT", 28);

		memset((void*)pclRetFirst, 0x00, 100);
		len = sock.Receive(pclRetFirst, 99,2000);

		char pclResult[256];
		long llSize = 0;
		if (CBcComAtl::GetKeyItem(pclResult, &llSize, pclRetFirst, "{=QUE=}", "{=", true) != NULL)
		{
			CBcComAtl::m_bCQueueID = CString(pclResult);
		}

		len = sock.Send("{=TOT=}       27{=ACK=}NEXT", 27);

		while(true)
		{
			if (ReadMsgFromSock(fp,sock, &rlMsg, FALSE) == FALSE)
			{
				CBcComServerApp::pogMonitorDlg->AddToSpooler(rlMsg);
			}
		}

	}
	catch(const char* e)
	{
		ilErr = sock.GetLastError();
		if(ilErr != 0)
		{
			bool blResume = CBcComServerApp::pogMonitorDlg->Fire_OnLostBc(ilErr);
			if (fp != NULL)
			{
				fclose(fp);
				fp = NULL;
			}

			if (blResume)
			{
				sock.Cleanup();
				goto resume;
			}
		}
		else
		{
			CComBSTR olMsg(e);
			bool blResume = CBcComServerApp::pogMonitorDlg->Fire_OnLostBcEx(olMsg);
			if (fp != NULL)
			{
				fclose(fp);
				fp = NULL;
			}

			if (blResume)
			{
				sock.Cleanup();
				goto resume;
			}
		}

	}

	sock.Cleanup();
	if (fp != NULL)
		fclose(fp);

	// Worker thread has been shut down..
	SetEvent(CBcComAtl::hEventWorkerThreadExit);

	CoUninitialize();
	AfxEndThread(0);
	return 0;
}
/* *************************************************************** */
/* *************************************************************** */
BOOL CBcComAtl::ReadMsgFromSock(FILE *fp,CBlockingSocket &ropSock, STR_DESC *rpMsg, BOOL ipGetAck)
{
  int ilStep = 0;
  int ilRcvNow = 0;
  int ilGotNow = 0;
  int ilRcvLen = 0;
  BOOL blBreakOut = FALSE;
  long llDatLen = 0;
  char pclKeyVal[32];
  char *pclPtr = NULL;

  blBreakOut = FALSE;
  ilRcvNow = 16;
  rpMsg->UsedLen = 0;
  while ((ilStep < 2) && (blBreakOut == FALSE)) 
  {
    ilStep++;
    ilRcvLen = ilRcvNow + 16;
    if (rpMsg->AllocatedSize < ilRcvLen)
    {
      rpMsg->Value = (char*)realloc(rpMsg->Value, ilRcvLen+16+1);
      rpMsg->AllocatedSize = ilRcvLen;
    }
    pclPtr = rpMsg->Value + rpMsg->UsedLen;
    *pclPtr = 0x00;

	if (fp != NULL)
	    fprintf(fp,"%s READING %d BYTES FROM SOCKET\n", "ReadMsgFromSock 1013", ilRcvNow);

    ilRcvLen = 0;
    while ((ilRcvLen < ilRcvNow) && (blBreakOut == FALSE))
    {
		if (fp != NULL)
			fprintf(fp,"%s TRYING TO RECEIVE %d BYTES\n", "ReadMsgFromSock 1017", ilRcvNow-ilRcvLen);
		ilGotNow = ropSock.Receive(pclPtr, ilRcvNow-ilRcvLen,-1);

        if (ilGotNow > 0)
        {
          ilRcvLen += ilGotNow;
          pclPtr += ilGotNow;
          rpMsg->UsedLen += ilGotNow;
          if (ilRcvLen < ilRcvNow)
          {
			if (fp != NULL)
				fprintf(fp,"%s RECEIVED THIS TIME %d BYTES\n", "ReadMsgFromSock 1026", ilGotNow);
            *pclPtr = 0x00;
            if (strcmp(rpMsg->Value,"{=ACK=}") == 0)
            {
				if (fp != NULL)
					fprintf(fp,"%s RECEIVED <%s> FROM CLIENT\n", "ReadMsgFromSock 1030",rpMsg->Value);
				blBreakOut = TRUE;
            }
          }
        }
        if (ilGotNow < 0)
        {
			if (fp != NULL)
				fprintf(fp,"%s SOCKET ERROR (%d) DETECTED\n", "ReadMsgFromSock 1037", ilGotNow);
			blBreakOut = TRUE;
			throw "Socket error detected";
        }
        if (ilGotNow == 0)
        {
			if (fp != NULL)
				fprintf(fp,"%s LOST SERVERCONNECTION DETECTED\n", "ReadMsgFromSock 1042");
			blBreakOut = TRUE;
			throw "Lost server connection detected";
        }
    } /* end while read  */
    *pclPtr = 0x00;
    
    if (blBreakOut == FALSE)
    {
      if (ilStep == 1)
      {
        CBcComAtl::GetKeyItem(pclKeyVal,&llDatLen,rpMsg->Value,"{=TOT=}","{=", TRUE);
        ilRcvNow = atoi(pclKeyVal) - 16;
        if (ilRcvNow <=0)
        {
			if (fp != NULL)
			{
				fprintf(fp,"%s RECEIVED <%s> FROM CLIENT\n", "ReadMsgFromSock 1056",rpMsg->Value);
				fprintf(fp,"%s NO FURTHER DATA EXPECTED\n", "ReadMsgFromSock 1057");
			}
          blBreakOut = TRUE;
        }
      }
    }
  } /* end while ilStep < 2*/
  *pclPtr = 0x00;

  if ((blBreakOut == FALSE) && (ipGetAck == TRUE))
  {
    CBcComAtl::GetKeyItem(pclKeyVal,&llDatLen,rpMsg->Value,"{=ACK=}","{=", TRUE);
	if (fp != NULL)
		fprintf(fp,"%s RECEIVED %s <%s> LEN=%d\n", "ReadMsgFromSock 1068","{=ACK=}", pclKeyVal, llDatLen);
    if (llDatLen > 0)
    {
/*      if (strcmp(pclKeyVal,"CLOSE") == 0)
      {
        blBreakOut = TRUE;
      }
      if (strcmp(pclKeyVal,"NEXT") == 0)
      {
        igTmpAlive = TRUE;
      }
      if (strcmp(pclKeyVal,"KEEP") == 0)
      {
        igTmpAlive = TRUE;
      }
      if (strcmp(pclKeyVal,"ALIVE") == 0)
      {
        igTmpAlive = TRUE;
      }
*/
    }
    else
    {
		if (fp != NULL)
			fprintf(fp,"%s EXPECTED ACK NOT FOUND IN <%s>\n","ReadMsgFromSock 1091",rpMsg->Value);
		blBreakOut = TRUE;
    }

	if (fp != NULL)
	{
		fprintf(fp,"Broadcast completed\n");
		fflush(fp);
	}
  }

  return blBreakOut;
}

char* CBcComAtl::GetKeyItem(char *pcpResultBuff, long *plpResultSize,char *pcpTextBuff, char *pcpKeyWord,char *pcpItemEnd, bool bpCopyData) 
{ 
	long llDataSize = 0L; 
	char *pclDataBegin = NULL; 
	char *pclDataEnd = NULL; 
	pclDataBegin = strstr(pcpTextBuff, pcpKeyWord); 
	/* Search the keyword */ 
	if (pclDataBegin != NULL) 
	{ 
		/* Did we find it? Yes. */ 
		pclDataBegin += strlen(pcpKeyWord); 
		/* Skip behind the keyword */ 
		pclDataEnd = strstr(pclDataBegin, pcpItemEnd); 
		/* Search end of data */ 
		if (pclDataEnd == NULL) 
		{ 
			/* End not found? */ 
			pclDataEnd = pclDataBegin + strlen(pclDataBegin); 
			/* Take the whole string */ 
		} /* end if */ 
		llDataSize = pclDataEnd - pclDataBegin; 
		/* Now calculate the length */ 
		if (bpCopyData == true) 
		{ 
			/* Shall we copy? */ 
			strncpy(pcpResultBuff, pclDataBegin, llDataSize); 
			/* Yes, strip out the data */ 
		} /* end if */ 
	} /* end if */ 
	if (bpCopyData == true) 
	{ 
		/* Allowed to set EOS? */ 
		pcpResultBuff[llDataSize] = 0x00; 
		/* Yes, terminate string */ 
	} /* end if */ 
	*plpResultSize = llDataSize; 
	/* Pass the length back */ 
	return pclDataBegin; 
	/* Return the data's begin */ 
} /* end GetKeyItem */ 

CString CBcComAtl::BC_To_Client()
{
	char *pclDataBegin;
	char pclKeyWord[100];
	char pclResult[25000];
	char *pclData=NULL;
	long llDataSize;
	char *pclAttach=NULL;
	long llAttachSize;
	long llSize;
	CString ReqId,  
			Dest1,  
			Dest2,  
			Cmd,  
			Object,  
			Seq,  
			Tws,  
			Twe,  
			Selection,  
			Fields,  
			Data,  
			BcNum,
			Queue,
			olAttach;

	CString	olResult;
	CString myBCData;

	// check if send is suspended for the moment..
	if (this->bmSendSuspended)
		return "";

	CSingleLock lock(&omSection);
	if (lock.Lock())
	{
		if(omSpooler.GetSize() > 0)
		{
			myBCData = omSpooler[0];
			omSpooler.RemoveAt(0);
			++this->lmItemsSend;
		}
		lock.Unlock();
	}

	if (myBCData.GetLength() == 0)
	{
		return "";
	}
	else
	{
		olResult = myBCData;
	}

	strcpy(pclKeyWord,"{=QUE=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Queue = CString(pclResult);
		m_bCQueueID = Queue;
	}
	strcpy(pclKeyWord,"{=USR=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		ReqId = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=USR=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Dest1  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=WKS=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Dest2  = CString(pclResult);
		ReqId = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=CMD=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Cmd  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=TBL=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Object  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=XXX=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Seq  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=TWS=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Tws  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=TWE=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Twe  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=WHE=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Selection  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=FLD=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Fields  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=DAT=}"); 
	pclData = GetKeyItem("", &llDataSize, myBCData.GetBuffer(0), pclKeyWord, "{=", false); 
	if (pclData != NULL) 
	{ 
		char tmp = pclData[llDataSize];
		pclData[llDataSize] = '\0';
		Data  = CString(pclData);
		pclData[llDataSize] = tmp;

	}
	strcpy(pclKeyWord,"{=BCNUM=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		BcNum  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=ATTACH=}"); 
	pclAttach = GetKeyItem("", &llAttachSize, myBCData.GetBuffer(0), pclKeyWord, "{=\\ATTACH=}", false); 
	if (pclAttach != NULL) 
	{ 
		pclAttach[llAttachSize] = '\0';
		olAttach = CString(pclAttach);
	}

	if (Cmd == "BCOUT" && !Queue.IsEmpty())
	{
		//m_bCQueueID = Queue;
		m_amIConnected = TRUE;
	}
	else
	{
		CString myBC;
		myBC = CString(myBCData.GetBuffer(0));
		//myBC.Replace(",", "\017");
		//MWO Dont log, becaus this makes no sense in this control.
		if(lmMaxBCRecords > 0)
		{
			of << myBCData.GetBuffer(0) << endl;
		}

		lmCurrWriteCount++;
		if(lmCurrWriteCount == lmMaxBCRecords)
		{
			lmCurrWriteCount=0;
			lmCurrFileNo++;
			CString olFileName;

			char *pclTmpPath = NULL;
			CString olTmpPath = "C:\\TMP";
			if((pclTmpPath = getenv("UFISTMP")) != NULL)
			{
				olTmpPath = CString(pclTmpPath);
			}

			olFileName.Format("%s\\%s_BC_%s%ld.log", olTmpPath, this->omAppl,this->omPID,lmCurrFileNo);
			if(lmMaxBCRecords > 0)
			{
				of.close();
				of.open(olFileName.GetBuffer(0), ios::out);
			}
		}

		// check if broadcast matches filter
		void *polValue = NULL;
		if (this->mapFilter.GetCount() == 0 || this->mapFilter.Lookup(Object,polValue))
		{
			//fire out
			long nLength  = myBCData.GetLength();
			long nPackets = nLength / 100000;
			if ((nLength %  100000) > 0)
				++nPackets;

			for (long i = 1; i <= nPackets; i++)
			{
				long len = 100000 < myBCData.GetLength() ? 100000 : myBCData.GetLength();
				CComBSTR olPacket = myBCData.Left(len);
				this->Fire_OnBc(nLength,i,nPackets,olPacket);		
				myBCData = myBCData.Mid(len);
			}
		}
	}

	return olResult;
}

bool CBcComAtl::CreateSendThread()
{
	this->pomSendThread = AfxBeginThread(SendFunction,this,THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED,NULL);
	if (this->pomSendThread != NULL)
		return true;
	else
		return false;
}

UINT CBcComAtl::SendFunction( LPVOID pParam )
{
	// initialize COM
	HRESULT hr = CoInitializeEx(NULL,COINIT_MULTITHREADED);
	if (FAILED(hr))
	{
		return 0;
	}

	try
	{
		CBcComAtl *popBcCom = (CBcComAtl *)pParam;

		if (popBcCom != NULL)
		{
			while(true)
			{
				CString olBc;
				WaitForSingleObject(popBcCom->hEvent,INFINITE);
				do
				{
					olBc = popBcCom->BC_To_Client();
					if (!olBc.IsEmpty())
					{
						olBc = popBcCom->Application() + CString('.') + popBcCom->ProcessID() + CString(':') + olBc;

						char *pszBuffer = new char[olBc.GetLength()+1];
						strncpy(pszBuffer,olBc,olBc.GetLength());
						pszBuffer[olBc.GetLength()] = '\0';
						HWND hWnd = CBcComServerApp::pogMonitorDlg->m_hWnd;
						::PostMessage(hWnd,WM_THREADFIREEVENT,(WPARAM)NULL,(LPARAM)pszBuffer);
					}
				}
				while (!olBc.IsEmpty());
			}
		}

	}
	catch(...)
	{
		AfxMessageBox("Exception in SendFunction occured");
	}

	CoUninitialize();
	return 0;
}

bool CBcComAtl::IsFilterDefined()
{
	return this->mapFilter.GetCount() == 0 ? false : true;
}

bool	CBcComAtl::GetServerInfo(const CString& ropHostName,int ipTcpPort,CStringArray& ropResult)
{
	CBlockingSocket sock;
	CSockAddr		addr;
	char			pclS[128];
	int				len;
	char			pclRetFirst[128];
	char			pclResult[256];

	try
	{

		if (sock.Create() == TRUE)
		{
			addr = sock.GetHostByName(ropHostName,ipTcpPort);

			if (sock.Connect(addr) == TRUE)
			{
				strcpy(pclS,"{=TOT=}       28{=CMD=}BCINF");
				len = sock.Send(pclS,28);

				memset((void*)pclRetFirst, 0x00, 100);
				int len = sock.Receive(pclRetFirst,16,2000);

				long llSize = 0;
				if (GetKeyItem(pclResult, &llSize, pclRetFirst, "{=TOT=}", "{=", true) == NULL)
				{
					sock.Close();
					sock.Cleanup();
					return false;
				}

				long size  = atol(pclResult);
				char *data = new char[size + 2];							
				
				memmove(data,pclRetFirst,16);
										
				if (size > 16)
				{
					len = sock.Receive(&data[16],size - 16);
					if (len <= 0)
					{
						delete[] data;
						sock.Close();
						sock.Cleanup();
						return false;
					}
				}
				
				data[size] = '\0';

				len = sock.Send("{=TOT=}       23{=ACK=}", 23);
				
				// evaluate data sent from server
				if (GetKeyItem(pclResult,&size,data,"{=COUNT=}", "{=",TRUE) == NULL)
				{
					delete[] data;
					sock.Close();
					sock.Cleanup();
					return false;
				}

				long count = atol(pclResult);
				if (count > 0)
				{
					char *pclData = GetKeyItem(pclResult,&size,data,"{=DATA=}", "{=\\DATA=}",FALSE);
					if (pclData == NULL)
					{
						delete[] data;
						sock.Close();
						sock.Cleanup();
						return false;
					}

					char *pclNextData;
					for (long i = 0; i < count; i++)
					{
						pclNextData = strchr(pclData,'\n');
						if (pclNextData != NULL)
						{
							*pclNextData = '\0';
						}

						ropResult.Add(pclData);

						if (pclNextData == NULL)
						{
							break;
						}
						else
						{
							pclData = pclNextData + 1;
						}
					}
				}

				delete[] data;
			}
		}
	}
	catch(const char* e)
	{
		CString olMsg;
		int ilErr = sock.GetLastError();
		if(ilErr != 0)
		{
			olMsg.Format("CServerInfoDlg: CEDA-Socket Error: %d", ilErr);
			AfxMessageBox(olMsg);
		}
		else
		{
			olMsg.Format("CServerInfoDlg: Exception: %s occured", e);
			AfxMessageBox(olMsg);
		}
	}

	sock.Cleanup();
	return true;

}

bool	CBcComAtl::StopConnection(const CString& ropHostName,int ipTcpPort,const CString& ropQueueId)
{
	CBlockingSocket sock;
	CSockAddr		addr;
	char			pclS[512];
	int				len;
	char			pclData[256];

	try
	{
		if (sock.Create() == TRUE)
		{
			addr = sock.GetHostByName(ropHostName,ipTcpPort);

			if (sock.Connect(addr) == TRUE)
			{
				sprintf(pclData,"{=QUE=}%s",ropQueueId);
				len = 28+strlen(pclData);

				sprintf(pclS,"{=TOT=}%09d{=CMD=}BCOFF",len);
				strcat(pclS,pclData);

				len = sock.Send(pclS,len);
			}
		}
	}
	catch(const char* e)
	{
		CString olMsg;
		int ilErr = sock.GetLastError();
		if(ilErr != 0)
		{
			olMsg.Format("CServerInfoDlg: CEDA-Socket Error: %d", ilErr);
			AfxMessageBox(olMsg);
		}
		else
		{
			olMsg.Format("CServerInfoDlg: Exception: %s occured", e);
			AfxMessageBox(olMsg);
		}
	}

	sock.Cleanup();
	return true;

}

STDMETHODIMP CBcComAtl::GetNextBufferdBC(BSTR *pReqId, BSTR *pDest1, BSTR *pDest2, BSTR *pCmd, BSTR *pObject, BSTR *pSeq, BSTR *pTws, BSTR *pTwe, BSTR *pSelection, BSTR *pFields, BSTR *pData, BSTR *pBcNum)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	// TODO: Add your implementation code here
	CString myBCData;

	CSingleLock lock(&omSection);
	if (lock.Lock())
	{
		if(omSpooler.GetSize() > 0)
		{
			myBCData = omSpooler[0];
			omSpooler.RemoveAt(0);
			++this->lmItemsSend;
		}
		lock.Unlock();
	}

	if (myBCData.GetLength() > 0)
	{
		char *pclDataBegin;
		char pclKeyWord[100];
		char pclResult[25000];
		char *pclData=NULL;
		long llDataSize;
		long llSize;

		CComBSTR bstrReqId;
		CComBSTR bstrDest1;		
		CComBSTR bstrDest2;		
		CComBSTR bstrCmd;   		
		CComBSTR bstrObject;		
		CComBSTR bstrSeq;		
		CComBSTR bstrTws;		
		CComBSTR bstrTwe;		
		CComBSTR bstrSelection;
		CComBSTR bstrFields;		
		CComBSTR bstrData;		
		CComBSTR bstrBcNum;		

		strcpy(pclKeyWord,"{=USR=}"); 
		pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
		if (pclDataBegin != NULL) 
		{ 
			bstrReqId = pclResult;
		}
		strcpy(pclKeyWord,"{=USR=}"); 
		pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
		if (pclDataBegin != NULL) 
		{ 
			bstrDest1  = pclResult;
		}
		strcpy(pclKeyWord,"{=WKS=}"); 
		pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
		if (pclDataBegin != NULL) 
		{ 
			bstrDest2  = pclResult;
			bstrReqId = pclResult;
		}
		strcpy(pclKeyWord,"{=CMD=}"); 
		pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
		if (pclDataBegin != NULL) 
		{ 
			bstrCmd  = pclResult;
		}
		strcpy(pclKeyWord,"{=TBL=}"); 
		pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
		if (pclDataBegin != NULL) 
		{ 
			bstrObject  = pclResult;
		}
		strcpy(pclKeyWord,"{=XXX=}"); 
		pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
		if (pclDataBegin != NULL) 
		{ 
			bstrSeq  = pclResult;
		}
		strcpy(pclKeyWord,"{=TWS=}"); 
		pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
		if (pclDataBegin != NULL) 
		{ 
			bstrTws  = pclResult;
		}
		strcpy(pclKeyWord,"{=TWE=}"); 
		pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
		if (pclDataBegin != NULL) 
		{ 
			bstrTwe  = pclResult;
		}
		strcpy(pclKeyWord,"{=WHE=}"); 
		pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
		if (pclDataBegin != NULL) 
		{ 
			bstrSelection  = pclResult;
		}
		strcpy(pclKeyWord,"{=FLD=}"); 
		pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
		if (pclDataBegin != NULL) 
		{ 
			bstrFields  = pclResult;
		}
		strcpy(pclKeyWord,"{=DAT=}"); 
		pclData = GetKeyItem("", &llDataSize, myBCData.GetBuffer(0), pclKeyWord, "{=", false); 
		if (pclData != NULL) 
		{ 
			char tmp = pclData[llDataSize];
			pclData[llDataSize] = '\0';
			bstrData  = pclData;
			pclData[llDataSize] = tmp;

		}
		strcpy(pclKeyWord,"{=BCNUM=}"); 
		pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
		if (pclDataBegin != NULL) 
		{ 
			bstrBcNum  = pclResult;
		}


		*pReqId			= bstrReqId.Detach();
		*pDest1			= bstrDest1.Detach();
		*pDest2			= bstrDest2.Detach();
		*pCmd			= bstrCmd.Detach();
		*pObject		= bstrObject.Detach();
		*pSeq			= bstrSeq.Detach();
		*pTws			= bstrTws.Detach();
		*pTwe			= bstrTwe.Detach();
		*pSelection		= bstrSelection.Detach();
		*pFields		= bstrFields.Detach();
		*pData			= bstrData.Detach();
		*pBcNum			= bstrBcNum.Detach();
	}
	return S_OK;
}

STDMETHODIMP CBcComAtl::get_Version(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	// TODO: Add your implementation code here
	CComBSTR bstrVersion("4.5.1.8");
	*pVal = bstrVersion.Detach();

	return S_OK;
}

STDMETHODIMP CBcComAtl::get_BuildDate(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	// TODO: Add your implementation code here
	CComBSTR bstrVersion(CString(__DATE__) + " - " + CString(__TIME__));
	*pVal = bstrVersion.Detach();

	return S_OK;
}
