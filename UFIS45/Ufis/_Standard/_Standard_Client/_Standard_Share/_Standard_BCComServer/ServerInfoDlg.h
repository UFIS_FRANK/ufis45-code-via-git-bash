#if !defined(AFX_SERVERINFODLG_H__1839CA81_593C_4273_96EF_CC24CB0DADCD__INCLUDED_)
#define AFX_SERVERINFODLG_H__1839CA81_593C_4273_96EF_CC24CB0DADCD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ServerInfoDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CServerInfoDlg dialog

class CServerInfoDlg : public CDialog
{
// Construction
public:
	CServerInfoDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CServerInfoDlg)
	enum { IDD = IDD_INFODLG };
	CListCtrl	m_InfoList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServerInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CServerInfoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonStopthread();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner);

private:
	int					imTcpPort;
	char				pcmHostName[512];

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVERINFODLG_H__1839CA81_593C_4273_96EF_CC24CB0DADCD__INCLUDED_)
