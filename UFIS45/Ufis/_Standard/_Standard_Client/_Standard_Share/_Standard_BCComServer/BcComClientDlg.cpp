// BcComClientDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bccomserver.h"
#include "BcComClientDlg.h"
#include "BcComAtl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BcComClientDlg dialog


BcComClientDlg::BcComClientDlg(CWnd* pParent /*=NULL*/)
	: CDialog(BcComClientDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(BcComClientDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void BcComClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BcComClientDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Control(pDX, IDC_LIST1, m_InfoList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BcComClientDlg, CDialog)
	//{{AFX_MSG_MAP(BcComClientDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BcComClientDlg message handlers

BOOL BcComClientDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_InfoList.InsertColumn(0,"Application  Id",LVCFMT_CENTER,70);
	m_InfoList.InsertColumn(1,"Process Id",LVCFMT_CENTER,70);
	m_InfoList.InsertColumn(2,"Items received",LVCFMT_CENTER,70);
	m_InfoList.InsertColumn(3,"Items send",LVCFMT_CENTER,70);


	for (int i = CBcComServerApp::ogAtlConnections.GetSize()-1; i >= 0;i--)
	{
		try
		{
			CBcComAtl *polDlg = CBcComServerApp::ogAtlConnections[i].pomBcCom;
			if (polDlg != NULL)
			{

				CSingleLock lock(&polDlg->omSection);
				if (lock.Lock())
				{
					int iItem = 0;
					LVITEM olItem;

					olItem.mask		= LVIF_TEXT;
					olItem.iItem	= iItem;
					olItem.iSubItem = 0;
					olItem.pszText  = polDlg->omAppl.GetBuffer(0);
					olItem.cchTextMax = polDlg->omAppl.GetLength();
					iItem = m_InfoList.InsertItem(&olItem);

					olItem.mask		= LVIF_TEXT;
					olItem.iItem	= iItem;
					olItem.iSubItem = 1;
					olItem.pszText  = polDlg->omPID.GetBuffer(0);
					olItem.cchTextMax = polDlg->omPID.GetLength();
					m_InfoList.SetItem(&olItem);

					CString olItemsReceived;
					olItemsReceived.Format("%ld",polDlg->lmItemsReceived);

					olItem.mask		= LVIF_TEXT;
					olItem.iItem	= iItem;
					olItem.iSubItem = 2;
					olItem.pszText  = olItemsReceived.GetBuffer(0);
					olItem.cchTextMax = polDlg->omPID.GetLength();
					m_InfoList.SetItem(&olItem);


					CString olItemsSend;
					olItemsSend.Format("%ld",polDlg->lmItemsSend);

					olItem.mask		= LVIF_TEXT;
					olItem.iItem	= iItem;
					olItem.iSubItem = 3;
					olItem.pszText  = olItemsSend.GetBuffer(0);
					olItem.cchTextMax = olItemsSend.GetLength();
					m_InfoList.SetItem(&olItem);
				}
			}
		}
		catch(...)	// COM - object no longer valid due to client abort etc...
		{
			CBcComServerApp::ogAtlConnections.DeleteAt(i);
			if (CBcComServerApp::ogAtlConnections.GetSize() == 0)
			{
				::PostQuitMessage(0);	
			}
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
