#ifndef __PROXY_INSTANCE__
#define __PROXY_INSTANCE__
//**************************************************************************
// Class to map the instances of Atl and the corresponding socket with
// Broadcast filter
//**************************************************************************

class CBcComAtl;
class MonitorDlg;

class ProxyInstance : public CObject
{
public:
	ProxyInstance(){};
	~ProxyInstance(){};

	CBcComAtl	*pomBcCom;
	MonitorDlg	*pomMonitorDlg;
};

#endif //__PROXY_INSTANCE__