// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

#if !defined(AFX_STDAFX_H__284A89F2_3BF0_426E_83E9_DC4E0371A88C__INCLUDED_)
#define AFX_STDAFX_H__284A89F2_3BF0_426E_83E9_DC4E0371A88C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define STRICT
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0400
#endif
#define _ATL_APARTMENT_THREADED

#include <afxwin.h>   // MFC core and standard components
#include <afxext.h>   // MFC extensions
#include <afxdisp.h>  // MFC Automation extensions 
#include <afxsock.h>  // MFC socket extensions
#include <afxmt.h>	  // MFC multithreading support	
#include <afxole.h>	  // MFC ole extensions
#include <afxcmn.h>	  // MFC list control support

#include <atlbase.h>

#include "ProxyInstance.h"
#include <CCSPtrArray.h>
#include "Resource.h"

//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
class CExeModule : public CComModule
{
public:
	LONG Unlock();
	DWORD dwThreadID;
	HANDLE hEventShutdown;
	void MonitorShutdown();
	bool StartMonitor();
	bool bActivity;
};
extern CExeModule _Module;
#include <atlcom.h>

class MonitorDlg;
class CBcComServerApp : public CWinApp
{
	public:
		virtual BOOL InitInstance();
        virtual int ExitInstance();
// attributes
	public:
		static BOOL	bgApplicationHidden;
		static char	sgHostName[512];
		static MonitorDlg	*pogMonitorDlg;
		static CCSPtrArray<ProxyInstance> ogAtlConnections;

	protected:
		BOOL m_bRun;
}; 
extern CBcComServerApp theApp;


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__284A89F2_3BF0_426E_83E9_DC4E0371A88C__INCLUDED)
