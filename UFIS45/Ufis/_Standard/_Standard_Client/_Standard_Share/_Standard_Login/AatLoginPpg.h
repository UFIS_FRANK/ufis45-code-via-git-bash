#if !defined(AFX_AATLOGINPPG_H__B26245DB_49EF_4B2D_9E73_8DB1E7F1DACF__INCLUDED_)
#define AFX_AATLOGINPPG_H__B26245DB_49EF_4B2D_9E73_8DB1E7F1DACF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// AatLoginPpg.h : Declaration of the CAatLoginPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CAatLoginPropPage : See AatLoginPpg.cpp.cpp for implementation.

class CAatLoginPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CAatLoginPropPage)
	DECLARE_OLECREATE_EX(CAatLoginPropPage)

// Constructor
public:
	CAatLoginPropPage();

// Dialog Data
	//{{AFX_DATA(CAatLoginPropPage)
	enum { IDD = IDD_PROPPAGE_AATLOGIN };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CAatLoginPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AATLOGINPPG_H__B26245DB_49EF_4B2D_9E73_8DB1E7F1DACF__INCLUDED)
