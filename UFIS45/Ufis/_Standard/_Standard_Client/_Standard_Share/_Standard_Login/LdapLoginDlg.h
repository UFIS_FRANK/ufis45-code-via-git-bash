#if !defined(AFX_LDAPLOGINDLG_H__11AEFEFA_1CB6_464D_ADC7_F1D62C6C24AF__INCLUDED_)
#define AFX_LDAPLOGINDLG_H__11AEFEFA_1CB6_464D_ADC7_F1D62C6C24AF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LdapLoginDlg.h : header file
//

const int GPD_TTOB = 0;
const int GPD_BTOT = 1;
const int GPD_LTOR = 2;
const int GPD_RTOL = 3;
const UINT GPC_RED = 0x00000001;
const UINT GPC_GREEN = 0x00000002;
const UINT GPC_BLUE = 0x00000004;

const UINT GRADIENT_COLOR = 0x00000007;
const UINT GRADIENT_STEP = 128;

/////////////////////////////////////////////////////////////////////////////
// LdapLoginDlg dialog

class LdapLoginDlg : public CDialog
{
// Construction
public:
	LdapLoginDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(LdapLoginDlg)
	enum { IDD = IDD_LDAPLOGIN };
	CEdit	m_User;
	CString	m_strUser;
	CString	m_strPassword;
	//}}AFX_DATA


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(LdapLoginDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

public:
	bool LdapLoginOK;
	char *LdapIP;
	char *LdapPort;
	char *LdapDomain;

	int LdapLogin(unsigned char *UserName, unsigned char* Password, bool bAsCurrentUser);
	void GetCurrentUser(char** User, char** Domain);

private:
	CBrush* m_pEditBkBrush;
	char *substring(char *string, int position, int length);

	const int m_nPaintSteps, m_nPaintDir;
    const UINT m_nPaintRGB;
	CPalette m_Pal;        
    void PaintGradientRect(CDC *pDC, const RECT &rect) const;

protected:

	// Generated message map functions
	//{{AFX_MSG(LdapLoginDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LDAPLOGINDLG_H__11AEFEFA_1CB6_464D_ADC7_F1D62C6C24AF__INCLUDED_)
