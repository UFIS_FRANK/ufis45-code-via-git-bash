// LdapLoginDlg.cpp : implementation file
//

#include "stdafx.h"
#include "aatlogin.h"
#include "LdapLoginDlg.h"

#include "windows.h"
#include "winldap.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

#define SECURITY_KERNEL
#include "Security.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// LdapLoginDlg dialog

LdapLoginDlg::LdapLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialog(LdapLoginDlg::IDD, pParent),
	m_nPaintSteps(GRADIENT_STEP),     // the number of steps
    m_nPaintDir(GPD_BTOT),  // the direction
    m_nPaintRGB(GRADIENT_COLOR)   // the color
{
	LdapLoginOK = false;
	m_pEditBkBrush = new CBrush(RGB(255, 255, 0));
	
	//{{AFX_DATA_INIT(LdapLoginDlg)
	m_strUser = _T("");
	m_strPassword = _T("");
	//}}AFX_DATA_INIT
}


void LdapLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(LdapLoginDlg)
	DDX_Control(pDX, IDC_PASSWORD, m_User);
	DDX_Text(pDX, IDC_USERNAME, m_strUser);
	DDX_Text(pDX, IDC_PASSWORD, m_strPassword);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(LdapLoginDlg, CDialog)
	//{{AFX_MSG_MAP(LdapLoginDlg)
	ON_WM_CTLCOLOR()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// LdapLoginDlg message handlers

void LdapLoginDlg::OnCancel() 
{
	LdapLoginOK = false;
	CDialog::OnCancel();
}

void LdapLoginDlg::OnOK() 
{
	UpdateData(true);
	if(LdapLogin((unsigned char*)(LPCTSTR)m_strUser,(unsigned char*)(LPCTSTR)m_strPassword, false)!=0)
	{
		LdapLoginOK = false;
		MessageBoxA("Cannot login to the LDAP server!", "LDAP LOGIN FAILS!");
	}
	else
	{
		LdapLoginOK = true;
		CDialog::OnOK();
	}
}

//Return -1 : Not OK
//Return 0 : OK
int LdapLoginDlg::LdapLogin(unsigned char *UserName, unsigned char* Password, bool bAsCurrentUser)
{	
	//CONFIG LDAP_IP : 192.168.1.99
	//CONFIG LDAP_PORT : 389
	//CONFIG LDAP_DOMAIN : 192.168.1.99 (Note : This value and LDAP_IP can be different, this value can be an IP or a literal name)

	//Steps to follow to login to LDAP
	//--------------------------------
	//Initializde the LDAP connection
	//Set the LDAP connection version to 3 (Default is 2) 
	//Bind the LDAP
	//Search the LDAP entries
	//Count the result entries
	
	LDAP* pLdapConnection = NULL;      
	int iResult = -1;
	ULONG version = LDAP_VERSION3; 

	//Initializde the LDAP connection
	pLdapConnection = ldap_init(LdapIP, atoi(LdapPort)); //(LDAP_IP, LDAP_PORT)

	//Set the LDAP connection version to 3 (Default is 2) 
	int iReturn = ldap_set_option(pLdapConnection, LDAP_OPT_PROTOCOL_VERSION, (void*)&version);

	//Bind the LDAP
	
	SEC_WINNT_AUTH_IDENTITY secIdent; 
	PCHAR pUserName = NULL;
	PCHAR pPassword = NULL;
    
	/******* This value is taking no effect as per the test & trial *******/
	/******* But not passing this value will raise the error        *******/
	PCHAR pHostName = "ANYNAME_WILL_DO";	

	char *DomainUserName;
	char *DomainName;
	int iSlash = strstr((char*)UserName,"\\") - (char*)UserName;
	int iLen = strlen((char*)UserName);
	if(iSlash>0)
	{
		DomainName = substring((char*)UserName, 0, iSlash);
		DomainUserName = substring((char*)UserName, iSlash + 2, iLen-iSlash - 1);

		pUserName = (char*)DomainUserName;
		pHostName = (char*)DomainName;
	}
	else
	{
		pUserName = (char*)UserName;
	}
	
	pPassword = (char*)Password;

    secIdent.User = (unsigned char*)pUserName;
    secIdent.UserLength = strlen((const char*)pUserName);
    secIdent.Password = (unsigned char*)pPassword;
    secIdent.PasswordLength = strlen((const char*)pPassword);
    secIdent.Domain = (unsigned char*)pHostName;
    secIdent.DomainLength = strlen((const char*)pHostName);
    secIdent.Flags = SEC_WINNT_AUTH_IDENTITY_ANSI;

	if(bAsCurrentUser)
		iReturn = ldap_bind_s(
                pLdapConnection,      // Session Handle
                LdapDomain,       // CONFIG : LDAP_DOMAIN : Domain DN
                NULL,     // Credential structure
				LDAP_AUTH_NEGOTIATE); // Auth mode
	else
		iReturn = ldap_bind_s(
                pLdapConnection,      // Session Handle
                LdapDomain,       // CONFIG : LDAP_DOMAIN : Domain DN
                (PCHAR)&secIdent,     // Credential structure
				LDAP_AUTH_NEGOTIATE); // Auth mode

	if(iReturn == LDAP_SUCCESS)
	{
		LdapLoginOK = true;
		iResult = 0;
	}

	return iResult;
}


void LdapLoginDlg::GetCurrentUser(char** User, char** Domain)
{
	char UserName[INFO_BUFFER_SIZE+1];
	DWORD  bufCharCount = INFO_BUFFER_SIZE;
	GetUserNameEx(NameSamCompatible, UserName, &bufCharCount);

	if(!(strlen(UserName)>0))
	{
		GetUserName(UserName, &bufCharCount);
	}

	int iSlash = strstr(UserName,"\\") - UserName;
	int iLen = strlen(UserName);
	if(iSlash>0)
	{
		*User = substring(UserName, 0, iSlash);
		*Domain = substring(UserName, iSlash + 2, iLen-iSlash - 1);
	}
}

/*C substring function: It returns a pointer to the substring */
char *LdapLoginDlg::substring(char *string, int position, int length) 
{
   char *pointer;
   int c;
 
   pointer = (char *)malloc(length+1);
 
   if (pointer == NULL)
   {
      printf("Unable to allocate memory.\n");
      exit(EXIT_FAILURE);
   }
 
   for (c = 0 ; c < position -1 ; c++) 
      string++; 
 
   for (c = 0 ; c < length ; c++)
   {
      *(pointer+c) = *string;      
      string++;   
   }
 
   *(pointer+c) = '\0';
 
   return pointer;
}

HBRUSH LdapLoginDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	switch (nCtlColor) 
	{
		case CTLCOLOR_EDIT:
		case CTLCOLOR_MSGBOX:
			pDC->SetTextColor(RGB(100, 100, 0));//Dark olive color
			pDC->SetBkColor(RGB(255, 255, 0));//Yellow color
			return (HBRUSH)(m_pEditBkBrush->GetSafeHandle());
		case CTLCOLOR_DLG:
		case CTLCOLOR_STATIC:
			pDC->SetTextColor(RGB(0,0,0));    
			pDC->SetBkMode (TRANSPARENT);
			return (HBRUSH)GetStockObject(NULL_BRUSH);
    }

	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	return hbr;
}

void LdapLoginDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	delete m_pEditBkBrush;
}

void LdapLoginDlg::OnPaint() 
{
	CPaintDC dc(this);
	CPalette *pPalOld = dc.SelectPalette(&m_Pal, FALSE);
    dc.RealizePalette();
    
    RECT rect;
    this->GetClientRect(&rect);    
    this->PaintGradientRect(&dc, rect);    
    dc.SelectPalette(pPalOld, FALSE);
}

void LdapLoginDlg::PaintGradientRect(CDC *pDC, const RECT &rect) const
{
    ASSERT_POINTER(pDC, CDC);
    ASSERT_KINDOF(CDC, pDC);
    ASSERT(m_nPaintSteps > 0);
    
    // initialize
    RECT rectVar = { rect.left, rect.top, rect.left, rect.top };
    int nTotalSize;
    if (m_nPaintDir == GPD_TTOB || m_nPaintDir == GPD_BTOT)
    {
        rectVar.right = rect.right;
        nTotalSize = rect.bottom - rect.top;
    }
    else
    {
        rectVar.bottom = rect.bottom;
        nTotalSize = rect.right - rect.left;
    }
    
    // paint nSteps times
    for (int nIndex = 0; nIndex < m_nPaintSteps; nIndex++)
    {
        // calculate the rectangle
        if (m_nPaintDir == GPD_TTOB || m_nPaintDir == GPD_BTOT)
        {
            rectVar.top = rectVar.bottom;
            rectVar.bottom = rect.top +
                             ::MulDiv(nIndex + 1, nTotalSize, m_nPaintSteps);
        }
        else
        {
            rectVar.left = rectVar.right;
            rectVar.right = rect.left +
                            ::MulDiv(nIndex + 1, nTotalSize, m_nPaintSteps);
        }
        
        // calculate the color value
        int nColor = ::MulDiv(nIndex, 255, m_nPaintSteps);
        if (m_nPaintDir == GPD_BTOT || m_nPaintDir == GPD_RTOL)
        {
            nColor = 255 - nColor;
        }

        const COLORREF clrBr =
            PALETTERGB((BYTE)(m_nPaintRGB & GPC_RED ? nColor : 0),
                       (BYTE)(m_nPaintRGB & GPC_GREEN ? nColor : 0),
                       (BYTE)(m_nPaintRGB & GPC_BLUE ? nColor : 0));
        
        // paint the rectangle with the brush
        CBrush brush(clrBr);
        pDC->FillRect(&rectVar, &brush);
    }
}
