; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=LdapLoginDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "aatlogin.h"
LastPage=0
CDK=Y

ClassCount=9
Class1=CAatLoginCtrl
Class2=CAatLoginPropPage
Class3=AatStatic
Class4=CDummyDlg
Class5=CInfoDlg
Class6=CLoginDlg
Class7=CPasswordDlg
Class8=CRegisterDlg

ResourceCount=8
Resource1=IDD_ABOUTBOX (English (U.K.))
Resource2=IDD_REGISTERDLG (English (U.K.))
Resource3=IDD_BDPSPASS_DIALOG (English (U.K.))
Resource4=IDD_DUMMY (English (U.K.))
Resource5=IDD_LOGIN (English (U.K.))
Resource6=IDD_PROPPAGE_AATLOGIN (English (U.K.))
Resource7=IDD_ABOUTBOX_AATLOGIN (English (U.K.))
Class9=LdapLoginDlg
Resource8=IDD_LDAPLOGIN

[CLS:CAatLoginCtrl]
Type=0
BaseClass=COleControl
HeaderFile=AatLoginCtl.h
ImplementationFile=AatLoginCtl.cpp
LastObject=CAatLoginCtrl

[CLS:CAatLoginPropPage]
Type=0
BaseClass=COlePropertyPage
HeaderFile=AatLoginPpg.h
ImplementationFile=AatLoginPpg.cpp

[CLS:AatStatic]
Type=0
BaseClass=CStatic
HeaderFile=AatStatic.h
ImplementationFile=AatStatic.cpp
LastObject=AatStatic

[CLS:CDummyDlg]
Type=0
BaseClass=CDialog
HeaderFile=DummyDlg1.h
ImplementationFile=DummyDlg1.cpp

[CLS:CInfoDlg]
Type=0
BaseClass=CDialog
HeaderFile=InfoDlg.h
ImplementationFile=InfoDlg.cpp

[CLS:CLoginDlg]
Type=0
BaseClass=CDialog
HeaderFile=LoginDlg.h
ImplementationFile=LoginDlg.cpp
Filter=D
VirtualFilter=dWC
LastObject=CLoginDlg

[CLS:CPasswordDlg]
Type=0
BaseClass=CDialog
HeaderFile=PasswordDlg.h
ImplementationFile=PasswordDlg.cpp

[CLS:CRegisterDlg]
Type=0
BaseClass=CDialog
HeaderFile=RegisterDlg.h
ImplementationFile=RegisterDlg.cpp

[DLG:IDD_PROPPAGE_AATLOGIN]
Type=1
Class=CAatLoginPropPage

[DLG:IDD_DUMMY]
Type=1
Class=CDummyDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CInfoDlg

[DLG:IDD_LOGIN]
Type=1
Class=CLoginDlg

[DLG:IDD_BDPSPASS_DIALOG]
Type=1
Class=CPasswordDlg

[DLG:IDD_REGISTERDLG]
Type=1
Class=CRegisterDlg

[DLG:IDD_ABOUTBOX_AATLOGIN (English (U.K.))]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_PROPPAGE_AATLOGIN (English (U.K.))]
Type=1
Class=?
ControlCount=1
Control1=IDC_STATIC,static,1342308352

[DLG:IDD_LOGIN (English (U.K.))]
Type=1
Class=?
ControlCount=12
Control1=IDC_USIDCAPTION,static,1342308352
Control2=IDC_USERNAME,edit,1350631552
Control3=IDC_PASSCAPTION,static,1342308352
Control4=IDC_PASSWORD,edit,1350631584
Control5=IDOK,button,1342242817
Control6=IDCANCEL,button,1342242816
Control7=IDC_ABOUT,button,1342242816
Control8=IDC_STATIC_PICTURE,static,1342177283
Control9=IDC_VERSION_STRING,static,1342308354
Control10=IDC_STATIC_SCENE,static,1342308352
Control11=IDC_COMBO_SCENE,combobox,1344340226
Control12=IDC_SCENE,edit,1484849280

[DLG:IDD_REGISTERDLG (English (U.K.))]
Type=1
Class=?
ControlCount=6
Control1=IDC_START,button,1342242816
Control2=IDC_ABORT,button,1342242816
Control3=IDC_REGISTER,button,1342242816
Control4=IDC_TIME,edit,1342179328
Control5=IDC_STATIC,static,1350565902
Control6=IDC_TEXT,static,1342308352

[DLG:IDD_ABOUTBOX (English (U.K.))]
Type=1
Class=?
ControlCount=13
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,button,1342177287
Control3=IDC_UFIS_VERSION,static,1342308352
Control4=IDC_APPLICATION,static,1342308352
Control5=IDC_COPYRIGHT,static,1342308352
Control6=IDC_AAT,static,1342308352
Control7=IDC_STATIC,static,1342177283
Control8=IDC_CONNECTION,static,1342308352
Control9=IDC_USER,static,1342308352
Control10=IDC_LOGINTIME,static,1342308352
Control11=IDC_LABEL3,static,1342308352
Control12=IDC_LABEL1,static,1342308352
Control13=IDC_LABEL2,static,1342308352

[DLG:IDD_DUMMY (English (U.K.))]
Type=1
Class=?
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_UFISCOMCTRL1,{A2F31E95-C74F-11D3-A251-00500437F607},1342242816

[DLG:IDD_BDPSPASS_DIALOG (English (U.K.))]
Type=1
Class=?
ControlCount=10
Control1=IDC_USIDHEAD,static,1342308352
Control2=IDC_USID,edit,1350631552
Control3=IDC_OLDPASSHEAD,static,1342308352
Control4=IDC_OLDPASS,edit,1350631584
Control5=IDC_NEWPASS1HEAD,static,1342308352
Control6=IDC_NEWPASS1,edit,1350631584
Control7=IDC_NEWPASS2HEAD,static,1342308352
Control8=IDC_NEWPASS2,edit,1350631584
Control9=IDOK,button,1342242816
Control10=IDCANCEL,button,1342242816

[DLG:IDD_LDAPLOGIN]
Type=1
Class=LdapLoginDlg
ControlCount=8
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC_PICTURE,static,1342177283
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_USERNAME,edit,1350631552
Control8=IDC_PASSWORD,edit,1350631584

[CLS:LdapLoginDlg]
Type=0
HeaderFile=LdapLoginDlg.h
ImplementationFile=LdapLoginDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=LdapLoginDlg
VirtualFilter=dWC

