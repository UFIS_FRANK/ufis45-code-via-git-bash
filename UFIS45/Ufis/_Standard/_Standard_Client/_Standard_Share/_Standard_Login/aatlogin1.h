#if !defined(AFX_AATLOGIN1_H__F6D78310_D88F_4F99_B2A1_73133D1D8736__INCLUDED_)
#define AFX_AATLOGIN1_H__F6D78310_D88F_4F99_B2A1_73133D1D8736__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Machine generated IDispatch wrapper class(es) created by Microsoft Visual C++

// NOTE: Do not modify the contents of this file.  If this class is regenerated by
//  Microsoft Visual C++, your modifications will be overwritten.

/////////////////////////////////////////////////////////////////////////////
// CAatLogin wrapper class

class CAatLogin : public CWnd
{
protected:
	DECLARE_DYNCREATE(CAatLogin)
public:
	CLSID const& GetClsid()
	{
		static CLSID const clsid
			= { 0x17d7e209, 0x2dec, 0x48bc, { 0x83, 0xe2, 0x3e, 0x9f, 0x43, 0xb9, 0xd, 0x59 } };
		return clsid;
	}
	virtual BOOL Create(LPCTSTR lpszClassName,
		LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect,
		CWnd* pParentWnd, UINT nID,
		CCreateContext* pContext = NULL)
	{ return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID); }

    BOOL Create(LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect, CWnd* pParentWnd, UINT nID,
		CFile* pPersist = NULL, BOOL bStorage = FALSE,
		BSTR bstrLicKey = NULL)
	{ return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID,
		pPersist, bStorage, bstrLicKey); }

// Attributes
public:
	CString GetRegisterApplicationString();
	void SetRegisterApplicationString(LPCTSTR);
	CString GetApplicationName();
	void SetApplicationName(LPCTSTR);
	short GetLoginAttempts();
	void SetLoginAttempts(short);
	CString GetVersionString();
	void SetVersionString(LPCTSTR);
	BOOL GetInfoButtonVisible();
	void SetInfoButtonVisible(BOOL);
	CString GetInfoUfisVersion();
	void SetInfoUfisVersion(LPCTSTR);
	CString GetInfoCopyright();
	void SetInfoCopyright(LPCTSTR);
	CString GetInfoAAT();
	void SetInfoAAT(LPCTSTR);
	CString GetInfoCaption();
	void SetInfoCaption(LPCTSTR);
	LPDISPATCH GetUfisComCtrl();
	void SetUfisComCtrl(LPDISPATCH);
	CString GetInfoAppVersion();
	void SetInfoAppVersion(LPCTSTR);
	BOOL GetUserNameLCase();
	void SetUserNameLCase(BOOL);
	CString GetVersion();
	void SetVersion(LPCTSTR);
	CString GetBuildDate();
	void SetBuildDate(LPCTSTR);
	BOOL GetUseVersionInfo();
	void SetUseVersionInfo(BOOL);

// Operations
public:
	CString ShowLoginDialog();
	CString DoLoginSilentMode(LPCTSTR strUserName, LPCTSTR strUserPassword);
	CString GetPrivileges(LPCTSTR strItem);
	CString GetUserName_();
	CString GetUserPassword();
	BOOL WriteErrToLog(LPCTSTR sFileName, LPCTSTR sFunction, LPDISPATCH* ErrObj);
	CString GetPrivilegList();
	CString GetWorkstationName();
	CString GetRealWorkstationName();
	CString GetProfileList();
	CString GetTableExt();
	CString ShowLoginDialogMultiAirport(BOOL bShowMultiAirport);
	void AboutBox();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AATLOGIN1_H__F6D78310_D88F_4F99_B2A1_73133D1D8736__INCLUDED_)
