// PasswordDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AatLogin.h"
#include "PasswordDlg.h"
#include "AatLoginCtl.h"
#include <DUfisCom.h>
#include <CCSBasicFunc.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
#define SILVER  RGB(192, 192, 192)          // light gray
#define RED     RGB(255,   0,   0)
#define ORANGE  RGB(255, 132,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255,   0)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(  0, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)

/////////////////////////////////////////////////////////////////////////////
// CPasswordDlg dialog


CPasswordDlg::CPasswordDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPasswordDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPasswordDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	this->pomLoginCtrl = (CAatLoginCtrl *)pParent;
	bmExtendedSecurity = FALSE;
	imMinPasswordLength= 1;

}

CPasswordDlg::CPasswordDlg(CWnd* pParent,CAatLoginCtrl *popLoginCtrl)
	: CDialog(CPasswordDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPasswordDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	this->pomLoginCtrl = popLoginCtrl;
	bmExtendedSecurity = FALSE;
	imMinPasswordLength= 1;

}


void CPasswordDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPasswordDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Control(pDX, IDC_NEWPASS1, m_NewPass1);
	DDX_Control(pDX, IDC_NEWPASS2, m_NewPass2);
	DDX_Control(pDX, IDC_OLDPASS, m_OldPass);
	DDX_Control(pDX, IDC_USID, m_Usid);
	DDX_Control(pDX, IDC_USIDHEAD, m_LblUsid);
	DDX_Control(pDX, IDC_OLDPASSHEAD, m_LblOldPass);
	DDX_Control(pDX, IDC_NEWPASS1HEAD, m_LblNewPass1);
	DDX_Control(pDX, IDC_NEWPASS2HEAD, m_LblNewPass2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPasswordDlg, CDialog)
	//{{AFX_MSG_MAP(CPasswordDlg)
		// NOTE: the ClassWizard will add message map macros here
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPasswordDlg message handlers

BOOL CPasswordDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	igPasswordLength = 3;
	igMandCaptitalLetter = 0;
	igMandSmallLetter = 0;
	igMandNumbers = 0;
	igNumConsecutive = 0 ;


	bool blRc = true;

	ConnectToCeda();

	// Read from ceda
	_DUfisCom olUfisCom;
	olUfisCom.AttachDispatch(pomLoginCtrl->GetUfisComCtrl(),TRUE);

	olUfisCom.SetUserName(pomLoginCtrl->omUserName);

	CString olData;
	blRc = olUfisCom.CallServer("RT","PARTAB","PAID,VALU",olData,"WHERE APPL='SEC'","360");

	CString olKey;
	CString olValue;

	if(blRc == 0)
	{
		if (olUfisCom.GetBufferCount() > 0 )
		{
            int llRecords = olUfisCom.GetBufferCount();
            for (int i = 0; i < llRecords; i++)
			{
				CString olTmpStr = olUfisCom.GetBufferLine(i);
				CStringArray olItemList;
				::ExtractItemList(olTmpStr,&olItemList,',');
				if (olItemList.GetSize() >= 2)
				{
					CString olKey = olItemList[0];
					olKey.TrimRight();
					olKey.TrimLeft();

					CString olValue = olItemList[1];
					olValue.TrimRight();
					olValue.TrimLeft();

					if(olKey == CString("ID_MAND_NUMBER"))
					{
						igMandNumbers = atoi(olValue.GetBuffer(0));
					}
					if(olKey == CString("ID_MAND_CAPITAL"))
					{
						igMandCaptitalLetter = atoi(olValue.GetBuffer(0));
					}
					if(olKey == CString("ID_MAND_SMALL"))
					{
						igMandSmallLetter = atoi(olValue.GetBuffer(0));
					}
					if(olKey == CString("ID_PWD_LENGTH"))
					{
						igPasswordLength = atoi(olValue.GetBuffer(0));
					}
					if(olKey == CString("ID_PWD_CONSEC"))
					{
						igNumConsecutive = atoi(olValue.GetBuffer(0));
					}
				}
			}
		}
	}


	if (pomLoginCtrl->GetHomeAirport() == "SIN")
	{
		this->bmExtendedSecurity = TRUE;
		this->imMinPasswordLength= igPasswordLength;
	}

	m_Usid.SetTypeToString("X(32)",32,1); 
	m_Usid.SetBKColor(YELLOW);
	m_Usid.SetTextErrColor(RED);
	m_Usid.SetFocus();

	m_OldPass.SetTypeToString("X(32)",32,0);
	m_OldPass.SetBKColor(YELLOW);
	m_OldPass.SetTextErrColor(RED);

	m_NewPass1.SetTypeToString("X(32)",32,3);
	m_NewPass1.SetBKColor(YELLOW);
	m_NewPass1.SetTextErrColor(RED);

	m_NewPass2.SetTypeToString("X(32)",32,3);
	m_NewPass2.SetBKColor(YELLOW);
	m_NewPass2.SetTextErrColor(RED);

	SetText(IDC_USIDHEAD, IDS_USIDHEAD);
	SetText(IDC_OLDPASSHEAD, IDS_OLDPASSHEAD);
	SetText(IDC_NEWPASS1HEAD, IDS_NEWPASS1HEAD);
	SetText(IDC_NEWPASS2HEAD, IDS_NEWPASS2HEAD);


	m_Usid.SetWindowText(pomLoginCtrl->omUserName);
	m_OldPass.SetWindowText(pomLoginCtrl->omPassword);
	m_NewPass1.SetFocus();

	return FALSE;  // return TRUE  unless you set the focus to a control
}

void CPasswordDlg::OnOK() 
{
	bool blRc = true;

	CString olUsid,olOldPass,olNewPass1,olNewPass2;

	m_Usid.GetWindowText(olUsid);
	m_OldPass.GetWindowText(olOldPass);
	m_NewPass1.GetWindowText(olNewPass1);
	m_NewPass2.GetWindowText(olNewPass2);

	if(m_Usid.GetStatus() == false)
	{
		MessageBox(LoadStg(IDS_INVALIDUSERNAME),LoadStg(IDS_ERRORCAPTION),MB_ICONINFORMATION);
		m_Usid.SetFocus();
		blRc = false;
	}

	if (olNewPass1.GetLength() < igPasswordLength)
	{
		CString olMsg;
		olMsg.Format(LoadStg(IDS_INVALID_PASSWORD_LENGTH),igPasswordLength);
		AfxMessageBox(olMsg,MB_ICONSTOP|MB_OK,0);
		blRc = false;
	}



	if( blRc && olOldPass == olNewPass1 )
	{
		MessageBox(LoadStg(IDS_PASSWORD_MISMATCH),LoadStg(IDS_ERRORCAPTION),MB_ICONINFORMATION);
		m_NewPass1.SetFocus();
		blRc = false;
	}

	if( blRc && olNewPass1 != olNewPass2 )
	{
		MessageBox(LoadStg(IDS_DIFFERENTPASSWORDS),LoadStg(IDS_ERRORCAPTION),MB_ICONINFORMATION);
		m_NewPass1.SetFocus();
		blRc = false;
	}

	if(blRc)
	{
		if (olNewPass1[0] == ' ')
		{
			MessageBox(LoadStg(IDS_INVALIDPASSWORD),LoadStg(IDS_ERRORCAPTION),MB_ICONINFORMATION);
			m_NewPass1.SetFocus();
			blRc = false;
		}
	}


	char c;

	int ilCaptitalLetter = 0;
	int ilSmallLetter = 0;
	int ilNumbers = 0;


	for(int i = 0; i < olNewPass1.GetLength(); i++)
	{
		c = olNewPass1[i];	

		if(c >= 48 && c <= 57)
			ilNumbers++;

		if(c >= 97 && c <= 122)
			ilSmallLetter++;

		if(c >= 65 && c <= 90)
			ilCaptitalLetter++;
	}
		
	CString olTmp;

	bool blHint = false;

	if(igNumConsecutive >= 2)
	{
		for(int i = 0; i < olUsid.GetLength() - igNumConsecutive + 1; i++)
		{
			olTmp = olUsid.Mid(i, igNumConsecutive + 1);
			
			if(olNewPass1.Find(olTmp) >= 0)
				blHint = true;
		}

	}
	CString olTxt;
	// bgNumConsecutive 
	if(blHint)
	{
		olTxt.Format(GetString(IDS_CONSECUTIVE), igNumConsecutive);
		MessageBox(olTxt,GetString(IDS_ERRORCAPTION),MB_ICONINFORMATION);
		m_NewPass1.SetFocus();
		blRc = false;

	}

	if(ilNumbers < igMandNumbers)
	{
		olTxt.Format(GetString(IDS_NUMBERS), igMandNumbers);
		MessageBox(olTxt,GetString(IDS_ERRORCAPTION),MB_ICONINFORMATION);
		m_NewPass1.SetFocus();
		blRc = false;

	}

	if(ilSmallLetter < igMandSmallLetter)
	{
		olTxt.Format(GetString(IDS_SMALL_LETTER), igMandSmallLetter);
		MessageBox(olTxt,GetString(IDS_ERRORCAPTION),MB_ICONINFORMATION);
		m_NewPass1.SetFocus();
		blRc = false;

	}

	if(ilCaptitalLetter < igMandCaptitalLetter)
	{
		olTxt.Format(GetString(IDS_CAPITAL_LETTER), igMandCaptitalLetter);
		MessageBox(olTxt,GetString(IDS_ERRORCAPTION),MB_ICONINFORMATION);
		m_NewPass1.SetFocus();
		blRc = false;

	}





	if(blRc)
	{
		ConnectToCeda();

		// Read from ceda
		_DUfisCom olUfisCom;
		olUfisCom.AttachDispatch(pomLoginCtrl->GetUfisComCtrl(),TRUE);

		olUfisCom.SetUserName(olUsid);

		CString olData;
		olData.Format("%s,%s,%s",olUsid,olOldPass,olNewPass1);

		// Get Privileges
		blRc = olUfisCom.CallServer("SEC",pomLoginCtrl->GetHomeAirport(),"USID,OLDP,NEWP",olData,"NEWPWD","360");


		if(blRc == 0)
		{
			this->pomLoginCtrl->omPassword = olNewPass1;
			this->pomLoginCtrl->SetLoginResult("OK");
			blRc = 1;
		}
		else
		{
			if (olUfisCom.GetLastErrorMessage() == "INVALID_USER")
			{
				MessageBox(LoadStg(IDS_USERNAME_NOTFOUND),LoadStg(IDS_ERRORCAPTION),MB_ICONINFORMATION);
				m_Usid.SetFocus();
			}
			else if (olUfisCom.GetLastErrorMessage() == "INVALID_PASSWORD")
			{
				MessageBox(LoadStg(IDS_INVALIDOLDPASSWORD),LoadStg(IDS_ERRORCAPTION),MB_ICONINFORMATION);
				m_OldPass.SetFocus();
			}
			else
			{
				// database error so force exit
				blRc = true;
				CString olMsg;
				olMsg.Format(LoadStg(IDS_DB_ERROR),olUfisCom.GetLastErrorMessage());
				MessageBox(olMsg,LoadStg(IDS_ERRORCAPTION),MB_ICONINFORMATION);
			}

			this->pomLoginCtrl->SetLoginResult("ERROR");
			blRc = 0;

		}
	}

	if(blRc)
		CDialog::OnOK();
}

void CPasswordDlg::SetText(int ipId, int ipStringId)
{
	CWnd *polWnd = GetDlgItem(ipId);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(ipStringId));
	}
}

BOOL CPasswordDlg::ConnectToCeda()
{
	this->pomLoginCtrl->SetCedaIsConnected(FALSE);

	_DUfisCom olUfisCom;
	olUfisCom.AttachDispatch(pomLoginCtrl->GetUfisComCtrl(),TRUE);

    olUfisCom.CleanupCom();

	olUfisCom.SetCedaPerameters(olUfisCom.GetUserName(),pomLoginCtrl->GetHomeAirport(),pomLoginCtrl->TableExtension());

    int ilRet = olUfisCom.InitCom(pomLoginCtrl->HostName(),"CEDA");

    if (ilRet == 0)
	{
		CString olMsg;
		olMsg.Format("Connection to CEDA failed! \n Homeairport: %s \n Hostname: %s \n TableExtension: %s\n ConnectType: %s",
					pomLoginCtrl->GetHomeAirport(),
					pomLoginCtrl->HostName(),
					pomLoginCtrl->TableExtension(),
					"CEDA"
					);

		MessageBox(olMsg,LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP|MB_OK);

        this->pomLoginCtrl->SetLoginResult("ERROR");
	}
    else
	{
        this->pomLoginCtrl->SetCedaIsConnected(TRUE);
	}

	return this->pomLoginCtrl->GetCedaIsConnected();
}

void CPasswordDlg::OnCancel()
{
	this->pomLoginCtrl->SetLoginResult("ERROR");

	CDialog::OnCancel();
}

void CPasswordDlg::OnPaint() 
{
	// TODO: Add your message handler code here
	if (pomLoginCtrl != NULL)
	{
		CPaintDC dc(this); // device context for painting
		CRect olRect;
		GetClientRect( &olRect );
		pomLoginCtrl->DrawLifeStyle(&dc,olRect,7,0,true,70);
	}
}

CString CPasswordDlg::GetString(UINT nID)
{
	CString olString = "";			// cleare string
	olString.LoadString(nID);		// load string from Res
	return olString;		// cut all not needed "Extensions" (like *REM* ...)
}

