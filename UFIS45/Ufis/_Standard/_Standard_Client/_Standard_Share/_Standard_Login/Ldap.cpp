// Ldap.cpp: implementation of the CLdap class.
//
//////////////////////////////////////////////////////////////////////
#define _WIN32_WINNT 0x0501
#include "stdafx.h"
#include "stdio.h"
#include "windows.h"



#include "Ldap.h"

#pragma comment(lib, "advapi32")

#define DOMAINUSER "NT AUTHORITY\\SYSTEM"
//#define USER "SYSTEM"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


#define HOSTNAME "auhdomain1.net" 
#define PORT_NUMBER  389 
#define FIND_DN "uid=lti, ou=People, dc=example,dc=com" 
#define ROOT_DN "cn=Manager,dc=example,dc=com" 
#define PASS "Password123" 
static CHAR *pLdapRootString = "LDAP://" ;

#define MAXSTRLEN 255
#define MINSTRLEN 255

static char *DNSTRING            = TEXT("dn") ;
static char *CHANGETYPESTRING    = TEXT("changetype") ;
static char *ADDSTRING           = TEXT("Add") ;
static char *MODIFYSTRING        = TEXT("Modify") ;
static char *DELETESTRING        = TEXT("Delete") ;

static int    DNSTRINGLen         = 2 ;
static int    CHANGETYPESTRINGLen = 10 ;
static int    ADDSTRINGLen        = 3 ;
static int    MODIFYSTRINGLen     = 6 ;
static int    DELETESTRINGLen     = 6 ;


const int iMinStringLen   = 128 ;
const int iLdapFreeResult = 1 ;
const int iLdapKeepResult = 0 ;

#ifdef WIN32

   #define TIMEVAL struct l_timeval
   #define SPrintFormated xsprintf
   #define StrDuplicate   xstrdup
   #define StrLength      xstrlen

#else

   #define TIMEVAL struct timeval
   #define SPrintFormated sprintf
   #define StrDuplicate   strdup
   #define StrLength      strlen

#endif

char *LDAP_NAMEATTR_RDN          = TEXT("rdn") ;
char *LDAP_NAMEATTR_DN           = TEXT("dn") ;
char *LDAP_NAMEATTR_ObjectClass  = TEXT("objectClass") ;
char *LDAP_NAMEATTR_CN           = TEXT("cn") ;
char *LDAP_NAMEATTR_SAMAccount   = TEXT("sAMAccountName") ;
char *LDAP_NAMEATTR_displayName  = TEXT("displayName") ;
char *LDAP_NAMEATTR_userPassword = TEXT("userPassword") ;
char *LDAP_TEMPLATE_CN           = TEXT("cn=%s") ;
char *LDAP_TEMPLATE_ACCT         = TEXT("sAMAccountName=%s") ;
char *LDAP_TEMPLATE_USER_DN      = TEXT("cn=%s,%s") ;
char *LDAP_TEMPLATE_ACCT_DN      = TEXT("sAMAccountName=%s,%s") ;
int   LDAP_NAMEATTR_userPasswordLen  = 12 ;


CLdap::CLdap()
{
	pLogUserId       = NULL ;
	pLogPassword     = NULL ;
	pLdapHost        = NULL ;

	pHttpHost           = NULL ;
	pHttpService        = NULL ;
	pHttpRequest        = NULL ;
	pHttpResponse       = NULL ;
	pHttpFirstHeadChars = NULL ;

	// default timeout in milliseconds...
   DefaultTimeout   = 1000 ;
}

CLdap::~CLdap()
{
	if (pLogUserId)       free(pLogUserId) ;
	if (pLogPassword)     free(pLogPassword) ;
	if (pLdapHost)        free(pLdapHost) ;

	if (pHttpHost)           free(pHttpHost) ;
	if (pHttpService)        free(pHttpService) ;
	if (pHttpRequest)        free(pHttpRequest) ;
	if (pHttpResponse)       free(pHttpResponse) ;
	if (pHttpFirstHeadChars) free(pHttpFirstHeadChars) ;
}

CString CLdap::GetActiveUser()
{
	// Declare variables
	CString sUsername = "", sDomainName = "", sBuffer, sResult = "";
	PSID pSid = NULL;
	BYTE bySidBuffer[256];
	DWORD dwSidSize = sizeof(bySidBuffer), dwDomainNameSize = 1024, dwUsernameSize = 1024;
	SID_NAME_USE sidType;   

	// Initialize variables
	pSid = (PSID)bySidBuffer;
	dwSidSize = sizeof(bySidBuffer);

	// Get current username
	GetUserName(sBuffer.GetBuffer(dwUsernameSize), &dwUsernameSize);
	sUsername = sBuffer.GetBuffer(dwUsernameSize);
	sBuffer.ReleaseBuffer();

	// Get the domain name
	LookupAccountName(NULL, sUsername, (PSID) pSid, &dwSidSize,
		sBuffer.GetBuffer(dwDomainNameSize), &dwDomainNameSize,	(PSID_NAME_USE)&sidType);
	sDomainName = sBuffer.GetBuffer(dwDomainNameSize);
	sBuffer.ReleaseBuffer();

	// Return right result
	if (!sDomainName.IsEmpty())
		sResult = sDomainName + "\\";
	sResult += sUsername;

	return sResult;
}

bool CLdap::IsDomainUser()
{
	CString strDomain = GetDomainName();
	CString strComputer = GetLoginComputerName();
	bool bDomainUser= false;
	if(strDomain!=strComputer)
	{
		bDomainUser = true;
	}
	return bDomainUser;
}

CString CLdap::GetDomainName()
{
	// Declare variables
	CString sUsername = "", sDomainName = "", sBuffer, sResult = "";
	PSID pSid = NULL;
	BYTE bySidBuffer[256];
	DWORD dwSidSize = sizeof(bySidBuffer), dwDomainNameSize = 1024, dwUsernameSize = 1024;
	SID_NAME_USE sidType;   

	// Initialize variables
	pSid = (PSID)bySidBuffer;
	dwSidSize = sizeof(bySidBuffer);

	// Get current username
	GetUserName(sBuffer.GetBuffer(dwUsernameSize), &dwUsernameSize);
	sUsername = sBuffer.GetBuffer(dwUsernameSize);
	sBuffer.ReleaseBuffer();

	// Get the domain name
	LookupAccountName(NULL, sUsername, (PSID) pSid, &dwSidSize,
		sBuffer.GetBuffer(dwDomainNameSize), &dwDomainNameSize,	(PSID_NAME_USE)&sidType);
	sDomainName = sBuffer.GetBuffer(dwDomainNameSize);
	sBuffer.ReleaseBuffer();

	// Return right result
	if (!sDomainName.IsEmpty())
		sResult = sDomainName + "\\";
	sResult += sUsername;

	return sDomainName;
}
CString CLdap::GetLoginUserName()
{
	CString sUsername = "",sBuffer;
	DWORD dwUsernameSize = 1024;
	
	// Get current username
	GetUserName(sBuffer.GetBuffer(dwUsernameSize), &dwUsernameSize);
	sUsername = sBuffer.GetBuffer(dwUsernameSize);

	return sUsername;
}

CString CLdap::GetLoginComputerName()
{
	CString sUsername = "",sBuffer;
	DWORD dwUsernameSize = 1024;
	
	// Get current username
	GetComputerName(sBuffer.GetBuffer(dwUsernameSize), &dwUsernameSize);
	sUsername = sBuffer.GetBuffer(dwUsernameSize);

	return sUsername;
}

bool CLdap::Connect(char *pLdapServer)
{
	psLdap = ldap_open(pLdapServer,LDAP_PORT);
	//psLdap = ldap_init(pLdapServer,LDAP_PORT) ;

	if (psLdap == NULL) 
	{
      #ifdef WIN32
         // map ldap error to Win32 friendly error message...
         ULONG uErr = LdapGetLastError() ;
         uLastErr   = LdapMapErrorToWin32(uErr) ;

         if (uLastErr == NULL)
            uLastErr = LDAP_OTHER ;
      #else
         uLastErr   = LDAP_OTHER ;
      #endif
   }
   else
   {
      uLastErr = LDAP_SUCCESS ;
	}

    

	return (uLastErr == LDAP_SUCCESS);

}


void CLdap::LoadSettings()
{
	pLogUserId       = NULL ;
	pLogPassword     = NULL ;
	pLdapHost        = NULL ;

	pHttpHost           = NULL ;
	pHttpService        = NULL ;
	pHttpRequest        = NULL ;
	pHttpResponse       = NULL ;
	pHttpFirstHeadChars = NULL ;
}


CString CLdap::GetErrorDescription(int errcode)
{
	CString strError("");

	#ifdef WIN32
         // map ldap error to Win32 friendly error message...
         ULONG uErr = LdapGetLastError() ;
         uLastErr   = LdapMapErrorToWin32(uErr) ;

         if (uLastErr == NULL)
            uLastErr = LDAP_OTHER ;
      #else
         uLastErr   = LDAP_OTHER ;
      #endif

	switch (uLastErr)
	{
		case LDAP_SUCCESS:                         strError="Sucessful request."; break;
		case LDAP_OPERATIONS_ERROR:                strError="Intialization of LDAP library failed.";break;
		case LDAP_PROTOCOL_ERROR:                  strError="Protocol error occurred.";break;
		case LDAP_TIMELIMIT_EXCEEDED:              strError="Time limit has exceeded.";break;
		case LDAP_SIZELIMIT_EXCEEDED:              strError="Size limit has exceeded.";break;
		case LDAP_COMPARE_FALSE:                   strError="Compare yielded FALSE.";break;
		case LDAP_COMPARE_TRUE:                    strError="Compare yielded TRUE.";break;
		case LDAP_AUTH_METHOD_NOT_SUPPORTED:       strError="The authentication method is not supported.";break;
		case LDAP_STRONG_AUTH_REQUIRED:            strError="Strong authentication is required.";break;
		case LDAP_REFERRAL_V2:                     strError="LDAP version 2 referral.";break;
		//case LDAP_PARTIAL_RESULTS:                 strError="Partial results and referrals received.";break;
		case LDAP_REFERRAL:                        strError="Referral occurred.";break;
		case LDAP_ADMIN_LIMIT_EXCEEDED:            strError="Administration limit on the server has exceeded.";break;
		case LDAP_UNAVAILABLE_CRIT_EXTENSION:      strError="Critical extension is unavailable.";break;
		case LDAP_CONFIDENTIALITY_REQUIRED:        strError="Confidentiality is required.";break;
		case LDAP_NO_SUCH_ATTRIBUTE:               strError="Requested attribute does not  exist.";break;
		case LDAP_UNDEFINED_TYPE:                  strError="The type is not defined.  ";break;
		case LDAP_INAPPROPRIATE_MATCHING:          strError="An inappropriate matching  occurred. ";break;
		case LDAP_CONSTRAINT_VIOLATION:            strError="A constraint violation occurred.";break;
		case LDAP_ATTRIBUTE_OR_VALUE_EXISTS:       strError="The attribute exists or the value has been assigned.";break;
		case LDAP_INVALID_SYNTAX:                  strError="The syntax is invalid.";break;
		case LDAP_NO_SUCH_OBJECT:                  strError="Object does not exist.";break;
		case LDAP_ALIAS_PROBLEM:                   strError="The alias is invalid.";break;
		case LDAP_INVALID_DN_SYNTAX:               strError="The distinguished name has an invalid syntax.";break;
		case LDAP_IS_LEAF:                         strError="The object is a leaf.";break;
		case LDAP_ALIAS_DEREF_PROBLEM:             strError="Cannot de-reference the alias.";break;
		case LDAP_INAPPROPRIATE_AUTH:              strError="Authentication is inappropriate.";break;
		case LDAP_INVALID_CREDENTIALS:             strError="The supplied credential is  invalid.";break;
		case LDAP_INSUFFICIENT_RIGHTS:             strError="The user has insufficient access rights.";break;
		case LDAP_BUSY :                           strError="The server is busy.";break;
		case LDAP_UNAVAILABLE:                     strError="The server is unavailable.";break;
		case LDAP_UNWILLING_TO_PERFORM:            strError="The server does not handle directory requests.";break;
		case LDAP_LOOP_DETECT :                    strError="The chain of referrals has looped  back to a referring server.";break;
		case LDAP_NAMING_VIOLATION :               strError="There was a naming violation.";break;
		case LDAP_OBJECT_CLASS_VIOLATION :         strError="There was an object class  violation.";break;
		case LDAP_NOT_ALLOWED_ON_NONLEAF :         strError="Operation is not allowed on a  non-leaf object.";break;
		case LDAP_NOT_ALLOWED_ON_RDN :             strError="Operation is not allowed on RDN.";break;
		case LDAP_ALREADY_EXISTS   :               strError="The object already exists.";break;
		case LDAP_NO_OBJECT_CLASS_MODS :           strError="Cannot modify object class.";break;
		case LDAP_RESULTS_TOO_LARGE :              strError="Results returned are too large.";break;
		case LDAP_AFFECTS_MULTIPLE_DSAS:           strError="Multiple directory service agents  are affected.";break;
		case LDAP_OTHER  :                         strError="Unknown error occurred.";break;
		case LDAP_SERVER_DOWN  :                   strError="Cannot contact the LDAP server.";break;
		case LDAP_LOCAL_ERROR :                    strError="Local error occurred.";break;
		case LDAP_ENCODING_ERROR  :                strError="Encoding error occurred.";break;
		case LDAP_DECODING_ERROR :                 strError="Decoding error occurred.";break;
		case LDAP_TIMEOUT:                         strError="The search was timed out.";break;
		case LDAP_AUTH_UNKNOWN :                   strError="Unknown authentication error  occurred.";break;
		case LDAP_FILTER_ERROR :                   strError="The search filter is incorrect.";break;
		case LDAP_USER_CANCELLED:                  strError="The user has canceled the operation.";break;
		case LDAP_PARAM_ERROR :                    strError="An incorrect parameter was passed  to a routine.";break;
		case LDAP_NO_MEMORY :                      strError="The system is out of memory.";break;
		case LDAP_CONNECT_ERROR  :                 strError="Cannot establish a connection to the server.";break;
		case LDAP_NOT_SUPPORTED:                   strError="The feature is not supported.";break;
		case LDAP_CONTROL_NOT_FOUND  :             strError="The ldap function did not find the  specified control.";break;
		case LDAP_NO_RESULTS_RETURNED:             strError="The feature is not supported.";break;
		case LDAP_MORE_RESULTS_TO_RETURN:          strError="Additional results are to be returned.";break;
		case LDAP_CLIENT_LOOP :                    strError="Client loop was detected.";break;
		case LDAP_REFERRAL_LIMIT_EXCEEDED:         strError="The referral limit was exceeded.";break;
		//case LDAP_SASL_BIND_IN_PROGRESS :          strError="Intermediary bind result for multi-stage binds.";break;
		
	}
	return strError;
}


bool CLdap::AuthenticateUser(char *pUserId,char *pPassword,AUTHENTICATION_METHOD eAuthMethod)
{  if (!psLdap)
      return(false) ; 

   int iMessid ;

   switch (eAuthMethod) {
      case AUTH_ANNONYMOUS:  // authenticate as nobody...
         iMessid = (ULONG)ldap_simple_bind( psLdap, NULL, NULL ) ;
         WaitForOneResult(iMessid) ;
         break ;

      case AUTH_CLEAR_TEXT:  // authenticate using clear text...
         {  
			char dn[iMinStringLen+1] ;
            //BuildUserDistinguishedName(dn,pUserId,iMinStringLen) ;
            iMessid = (ULONG)ldap_simple_bind( psLdap, dn, pPassword ) ;
            WaitForOneResult(iMessid) ;
         }
         break ;

      case AUTH_LOGGED_USER:  // authenticate using current logged user

         // *** WIN32
         // only LDAP_AUTH_SIMPLE is supported by the Asynchrous ldap_bind
         // therefore to do an LDAP_AUTH_NEGOTIATE we need to use the '_s'
         // version... (see MSDN as of April 2000)

         uLastErr = ldap_bind_s(psLdap,NULL,NULL,LDAP_AUTH_NEGOTIATE) ;
         break ;

      default : uLastErr = LDAP_OTHER ;
   }

//   bLogged = uLastErr == LDAP_SUCCESS ;

   return(true) ;
}  

/*
 * -----------------------------------------------------------------------------
 * Function : ClLdap::WaitForOneResult(int iMessid)
 * Purpose  : Wait for the result of the last request...
 * In       : iMessid = Message id (int)
 * Date     : Jun/2000  (ESob)
 */

LDAP_WAIT_RESULT CLdap::WaitForOneResult(int iMessid)
{  LDAP_WAIT_RESULT rval ;
   TIMEVAL tval ;
   LDAPMessage *pResult ;

   tval.tv_sec  = 0 ;
   tval.tv_usec = DefaultTimeout ;

   uLastErr = ldap_result(psLdap,iMessid,LDAP_MSG_ONE,&tval,&pResult) ;

   if (uLastErr == -1) {
      rval = LDAP_WAIT_ERROR ;
      uLastErr = psLdap->ld_errno ;
   }
   else
   if (uLastErr == 0) {
      ldap_abandon(psLdap,iMessid) ;
      rval = LDAP_WAIT_TIMEOUT ;
      uLastErr = LDAP_TIMEOUT ;
   }
   else {
      uLastErr = ldap_result2error(psLdap,pResult,iLdapFreeResult) ;
      rval = uLastErr == LDAP_SUCCESS ? LDAP_WAIT_SUCCESS : LDAP_WAIT_ERROR ;
   }

   return(rval) ;
}  // end of wait for result...

// support for extracting the DN from an LDIF line...
inline char *GetDNFromLDIFLine(char *pLine) {
   char *pRVal = NULL ;

   while (*pLine && (*pLine == ' ')) pLine++ ;

   if (!memcmp(pLine,DNSTRING,DNSTRINGLen)) {
      pLine += DNSTRINGLen ;

      while (*pLine && (*pLine == ' ')) pLine++ ;
      if (*pLine == ':') pLine++ ;
      while (*pLine && (*pLine == ' ')) pLine++ ;

      if (*pLine)
         pRVal = pLine ;
   }

   return(pRVal) ;
}  // end of get DN from LDIF line...


bool LDAPEntry::Find(char *pBase,ULONG scope,char *pFilter,char *pAttr)
{  
	
	char DName[MAXSTRLEN+1] ;
   //FString::String str(DName,MAXSTRLEN) ;
	CString str;
   char *pAttrs[2] ;

   if (pNamingContext)
      str + pBase + ',' + pNamingContext ;
   else
      str + pBase ;

   pAttrs[0] = pAttr ;
   pAttrs[1] = NULL ;

   pResult = NULL ;
   pEntry  = NULL ;
   ULONG rval = ldap_search_s(
      psLdap,                   // LDAP connection
      DName,                    // container name (NULL means root DSE)
      scope,                    // search scope
      pFilter,                  // search filter
      pAttrs,                   // return givne attribute
      (ULONG)false,             // not attrs only
      &pResult) ;               // the result structure


    //MessageBox("Criteria is fine","CAatLoginCtrl::FindSSOUser",MB_OK);
   

   if ((rval == LDAP_SUCCESS) && (pResult != NULL)) {
      LDAPMessage *entry ;
      if ((entry = ldap_first_entry(psLdap,pResult)) != NULL)
         pEntry = entry ;
   }

   Eof = !First() ;
   return(!Eof) ;
}  // end of find a given entry...

/*
 * Function : LDAPEntry::Find(char *pBase,ULONG scope,char *pFilter,
 *          :    char **pAttrs)
 * Purpose  : Find given attributes of a given entry.
 * In       : pBase   = entry point to find (char*)
 *          : scope   = search scope (ULONG)
 *          : pFilter = search filter (char*)
 *          : pAttrs  = an array of attributes (char*)
 * Date     : Jun/2000  (ESob)
 */

bool LDAPEntry::Find(char *pBase,ULONG scope,char *pFilter,char **pAttrs)
{  char DName[MAXSTRLEN+1] ;
//   FString::String str(DName,MAXSTRLEN) ;
	CString str;

   if (pNamingContext)
      str + pBase + ',' + pNamingContext ;
   else
      str + pBase ;

   pResult = NULL ;
   pEntry  = NULL ;
   ULONG rval = ldap_search_s(
      psLdap,                   // LDAP connection
      DName,                    // container name (NULL means root DSE)
      scope,                    // search scope
      pFilter,                  // search filter
      pAttrs,                   // return givne attribute
      (ULONG)false,             // not attrs only
      &pResult) ;               // the result structure

   if ((rval == LDAP_SUCCESS) && (pResult != NULL)) {
      LDAPMessage *entry ;
      if ((entry = ldap_first_entry(psLdap,pResult)) != NULL)
         pEntry = entry ;
   }

   Eof = !First() ;
   return(!Eof) ;
}  // end of find a given entry...

/*
 * Function : LDAPEntry::Find(char *pBase,ULONG scope,char *pFilter)
 * Purpose  : Find an entry.
 * In       : pBase   = entry point to find (char*)
 *          : scope   = search scope (ULONG)
 *          : pFilter = search filter (char*)
 * Date     : Jun/2000  (ESob)
 */

bool LDAPEntry::Find(char *pBase,ULONG scope,char *pFilter)
{  char DName[MAXSTRLEN+1] ;
//   FString::String str(DName,MAXSTRLEN) ;
CString str;

   if (pNamingContext)
      str + pBase + ',' + pNamingContext ;
   else
      str + pBase ;

   pResult = NULL ;
   pEntry  = NULL ;
   ULONG rval = ldap_search_s(
      psLdap,                   // LDAP connection
      DName,                    // container name (NULL means root DSE)
      scope,                    // search scope
      pFilter,                  // search filter
      NULL,                     // return all attributes
      (ULONG)false,             // not attrs only
      &pResult) ;               // the result structure

   if ((rval == LDAP_SUCCESS) && (pResult != NULL)) {
      LDAPMessage *entry ;
      if ((entry = ldap_first_entry(psLdap,pResult)) != NULL)
         pEntry = entry ;
   }

   Eof = !First() ;
   return(!Eof) ;
}  // end of find a given entry..

/*
 * Function : LDAPEntry::First()
 * Purpose  : Find first entry name and value. Results are stored in
 *          : 'pEntryName' and 'pEntryValue'.
 * Date     : Jun/2000 (ESob)
 */

bool LDAPEntry::First()
{  // free previously allocated resources...

   if (pAttr)
      ldap_memfree(pAttr) ;
   if (pVals)
      ldap_value_free(pVals) ;
   if (pEntryValue)
      free(pEntryValue) ;

   // get first entry

   iVal = -1 ;
   pAttr = NULL ;
   pVals = NULL ;
   pEntryValue = NULL ;
   pAttr = ldap_first_attribute(psLdap,pEntry,&pBer) ;
   if (pAttr) {
      pVals = ldap_get_values(psLdap,pEntry,pAttr) ;
      if (pVals) {
         iVal = 0 ;
         pEntryName  = pAttr ;
         //pEntryValue = xstrdup(pVals[iVal]) ;
      }
   }

   Eof = (iVal < 0) ;
   return(!Eof) ;
}  // end of find first entry...

/*
 * Function : LDAPEntry::Next()
 * Purpose  : Find next entry value.
 * Date     : Jun/2000 (ESob)
 */

bool LDAPEntry::Next()
{
   // get next entry attribute value

   if (pAttr) {
      if (pVals[iVal]) {
         if (pEntryValue) {
            free(pEntryValue) ;
            pEntryValue = NULL ;
         }

         iVal++ ;
         if (pVals[iVal] != NULL)
		 {
            //pEntryValue = xstrdup(pVals[iVal]) ;
		 }
         else {
            iVal = -1 ;

            if (pAttr)
               ldap_memfree(pAttr) ;
            if (pVals)
               ldap_value_free(pVals) ;

            pVals = NULL ;
            pAttr = ldap_next_attribute(psLdap,pEntry,pBer) ;
            if (pAttr) {
               pEntryName = pAttr ;
               pVals = ldap_get_values(psLdap,pEntry,pAttr) ;
               if (pVals) {
                  iVal = 0 ;
                  pEntryName  = pAttr ;
//                  pEntryValue = xstrdup(pVals[iVal]) ;
               }
            }
         }
      }
   }

   Eof = (iVal < 0) ;
   return(!Eof) ;
}  // end of find first entry...


