// ccs3dstatic.h : header file
//

#ifndef AATSTATIC_H
#define AATSTATIC_H

#include <afxwin.h>



/////////////////////////////////////////////////////////////////////////////
// AatStatic window


/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Basic class {\bf AatStatic}
/*@Doc:
*/
class AatStatic : public CStatic
{
// Construction
public:
    //@ManMemo: Default constructor
    /*@Doc:
    */
    AatStatic();

// Attributes
public:

// Operations
public:

// Overrides

// Implementation
public:
    //@ManMemo: Default destructor
    virtual ~AatStatic();

    // Generated message map functions
public:
    //{{AFX_MSG(AatStatic)
    afx_msg bool OnEraseBkgnd(CDC* pDC);
    afx_msg void OnPaint();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
    CFont	*pomFont;
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;
};

/////////////////////////////////////////////////////////////////////////////

#endif
