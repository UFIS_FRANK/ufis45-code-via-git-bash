#if !defined(AFX_AATLOGIN_H__3E166B49_9A1E_4ECE_A2AD_EE4C620C4331__INCLUDED_)
#define AFX_AATLOGIN_H__3E166B49_9A1E_4ECE_A2AD_EE4C620C4331__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define INFO_BUFFER_SIZE 1024

// AatLogin.h : main header file for AATLOGIN.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CAatLoginApp : See AatLogin.cpp for implementation.

class CAatLoginApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AATLOGIN_H__3E166B49_9A1E_4ECE_A2AD_EE4C620C4331__INCLUDED)
