﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.ADLoginuser
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : ADLoginUser.cs

Version         : 1.0.0
Created Date    : 24 - Apr - 2012
Complete Date   : 24 - Apr - 2012
Created By      : Phyoe Khaing Min

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Ufis.Entities;
using Ufis.IO;
namespace Ufis.ADLoginUser
{
    public class ADLoginUser
    {
        /// <summary>
        /// Get Current Windows Login username and domain name
        /// </summary>
        /// <returns>Return List string array that contain user name and domain name</returns>
        public static List<string> GetADLoginUserList()
        {
            List<string> sLoginUser = new List<string>();
            

            sLoginUser.Add(WindowsIdentity.GetCurrent().Name.Contains("\\") == true ? WindowsIdentity.GetCurrent().Name.Substring(WindowsIdentity.GetCurrent().Name.IndexOf("\\") + 1) : WindowsIdentity.GetCurrent().Name);
            sLoginUser.Add(System.Environment.UserDomainName);
            sLoginUser.Add(System.Environment.MachineName);

            return sLoginUser;
        }
        /// <summary>
        /// Get Current Windows Login user name and domain name
        /// </summary>
        /// <returns>Return Entity that contain Windows Login UserName and Domain Name</returns>
        public static EntADLoginUser GetADLoginUserEntity()
        {
            EntADLoginUser objADUser = new EntADLoginUser();

            objADUser.UserName = WindowsIdentity.GetCurrent().Name.Contains("\\") == true ? WindowsIdentity.GetCurrent().Name.Substring(WindowsIdentity.GetCurrent().Name.IndexOf("\\") + 1) : WindowsIdentity.GetCurrent().Name;
            objADUser.DomainName = System.Environment.UserDomainName;
            objADUser.ComputerName = System.Environment.MachineName;
            return objADUser;
        }
        /// <summary>
        /// Validate Domain name
        /// </summary>
        /// <returns>Return System.String containing sUserName</returns>
        public static string ValidateDomainLogin()
        {
            string sUserName = "";
            string sDomainName;
            string strCedaPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.Ceda);
            
            IniFile myIni = new IniFile(strCedaPath);

            if (!string.IsNullOrEmpty(myIni.IniReadValue("GLOBAL", "ADNAME")))
                sDomainName = myIni.IniReadValue("GLOBAL", "ADNAME");
            else
                sDomainName = "";            

            string[] arrDomainName = sDomainName.Split(':');
            
            EntADLoginUser objADLoginUser = GetADLoginUserEntity();
            
            foreach (string strDomain in arrDomainName)
            {
                if (strDomain == objADLoginUser.DomainName)
                {
                    sUserName = objADLoginUser.UserName;
                    break;
                }
            }
            return sUserName;
 
        }
        /// <summary>
        /// Check LDAP use or not
        /// </summary>
        /// <returns>System.Boolean containing bStatus</returns>
        public static Boolean CheckLDAP()
        {
            Boolean bStatus = false;

            string strCedaPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.Ceda);

            IniFile myIni = new IniFile(strCedaPath);

            if (!string.IsNullOrEmpty(myIni.IniReadValue("GLOBAL", "USE_LDAP")))
                bStatus = Convert.ToBoolean(myIni.IniReadValue("GLOBAL", "USE_LDAP"));           
                
            return bStatus; 
        }
    }
     
    public class EntADLoginUser : Entity
    {
        #region ++++ Properties +++++
        private string _UserName = "";
        public string UserName
        {
            get { return _UserName; }
            set
            {
            	if (_UserName != value)
                {
                    _UserName = value;
                    OnPropertyChanged (UserName);
                }
            }
        }

        private string _DomainName = "";
        public string DomainName
        {
            get { return _DomainName; }

            set
            {
                if (_DomainName != value)
                {
                    _DomainName = value;
                    OnPropertyChanged(DomainName);
                }
            }
        }
        private string _ComputerName = "";
        public string ComputerName
        {
            get { return _ComputerName; }

            set
            {
                if (_ComputerName != value)
                {
                    _ComputerName = value;
                    OnPropertyChanged(ComputerName);
                }
            }
        }
        #endregion
    }
}
