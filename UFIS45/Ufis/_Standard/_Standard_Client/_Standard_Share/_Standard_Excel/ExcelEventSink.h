#if !defined(AFX_EXCELEVENTSINK_H__5C5E9844_B502_11D2_A0A1_0080C7F3B56B__INCLUDED_)
#define AFX_EXCELEVENTSINK_H__5C5E9844_B502_11D2_A0A1_0080C7F3B56B__INCLUDED_

/*----------------------------------------------------------------------------*/
#include "stdafx.h"
#include "ConnectionAdvisor.h"
#include "UfisExcelCtl.h"

#pragma warning (disable:4146)
#import "C:\Program Files\Microsoft Office\Office\MSO9.DLL"
#import "C:\Program Files\Common Files\Microsoft Shared\VBA\VBA6\VBE6EXT.OLB"
#pragma warning (default:4146)
#import "C:\\Program Files\\Microsoft Office\\Office\\excel9.olb" \
	rename ("DialogBox","XlDialogBox") rename("RGB","XlRGB")
/*----------------------------------------------------------------------------*/

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

/*----------------------------------------------------------------------------*/

class CExcelEventSink : public IDispatch
{
public:

	ULONG refCount;

	 CExcelEventSink(CUfisExcelCtrl *popCtrl) 
	 :m_pomCtrl(popCtrl),m_AppEventsAdvisor(__uuidof(Excel::AppEvents)) 
	 {
		refCount = 1;
	 }

	 ~CExcelEventSink() 
	 {
	 }

	 BOOL Advise(IUnknown* pSource, REFIID iid)
	 {
		if (iid == __uuidof(Excel::AppEvents))
		{
			return m_AppEventsAdvisor.Advise(this, pSource);
		}
		else 
		{
			return FALSE;
		}
	 }

	 BOOL Unadvise(REFIID iid)
	 {
		if (iid ==  __uuidof(Excel::AppEvents))
		{
			return m_AppEventsAdvisor.Unadvise();
		}
		else 
		{
			return FALSE;
		}
	 }

	// IUnknown methods.
	 virtual HRESULT __stdcall QueryInterface(REFIID riid, void **ppvObject) 
	 {
	    if (IsEqualGUID(riid, IID_IDispatch) || IsEqualGUID(riid, IID_IUnknown)) 
		{
	       this->AddRef();
	       *ppvObject = this;
	       return S_OK;
	    }
	    *ppvObject = NULL;
	    return E_NOINTERFACE;
	 }

	virtual ULONG _stdcall AddRef(void) 
	{
	    return ++refCount;
	}

	virtual ULONG _stdcall Release(void) 
	{
	    if(--refCount <= 0) 
		{
	       //Delete this;
	       return 0;
	    }
	    return refCount;
	 }

	// IDispatch methods.
	 virtual HRESULT _stdcall GetTypeInfoCount(UINT *pctinfo) 
	 {
	    if(pctinfo) *pctinfo = 0;
	    return E_NOTIMPL;
	 }

	virtual HRESULT _stdcall GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo) 
	{
	    return E_NOTIMPL;
	}

	virtual HRESULT _stdcall GetIDsOfNames(REFIID riid, LPOLESTR *rgszNames, UINT cNames, LCID lcid,DISPID *rgDispId) 
	{
	    return E_NOTIMPL;
	}

	virtual HRESULT _stdcall Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags,DISPPARAMS *pDispParams, VARIANT *pVarResult,EXCEPINFO *pExcepInfo, UINT *puArgErr) 
	{
		char *ptr = "Unknown-Event";
	   BOOL  olBool;

		switch(dispIdMember) 
		{
			case 0x61d: 
				ptr = "NewWorkbook";
				m_pomCtrl->FireNewWorkBook(pDispParams->rgvarg[0].pdispVal);				
				break;
	       case 0x616: 
				ptr = "SheetSelectionChange"; 
				m_pomCtrl->FireSheetSelectionChange(pDispParams->rgvarg[0].pdispVal,pDispParams->rgvarg[1].pdispVal);
				break;
	       case 0x617: 
				ptr = "SheetBeforeDoubleClick"; 
				olBool = *pDispParams->rgvarg[2].pboolVal;
				m_pomCtrl->FireSheetBeforeDoubleClick(pDispParams->rgvarg[0].pdispVal,pDispParams->rgvarg[1].pdispVal,&olBool);
				*pDispParams->rgvarg[2].pboolVal = olBool;
				break;
	       case 0x618: ptr = "SheetBeforeRightClick"; 
				olBool = *pDispParams->rgvarg[2].pboolVal;
				m_pomCtrl->FireSheetBeforeRightClick(pDispParams->rgvarg[0].pdispVal,pDispParams->rgvarg[1].pdispVal,&olBool);
				*pDispParams->rgvarg[2].pboolVal = olBool;
				break;
	       case 0x619: 
				ptr = "SheetActivate"; 
				m_pomCtrl->FireSheetActivate(pDispParams->rgvarg[0].pdispVal);				
				break;
	       case 0x61a: 
				ptr = "SheetDeactivate"; 
				m_pomCtrl->FireSheetDeactivate(pDispParams->rgvarg[0].pdispVal);				
				break;
	       case 0x61b: 
				ptr = "SheetCalculate"; 
				m_pomCtrl->FireSheetCalculate(pDispParams->rgvarg[0].pdispVal);				
				break;
	       case 0x61c: 
				ptr = "SheetChange"; 
				m_pomCtrl->FireSheetChange(pDispParams->rgvarg[0].pdispVal,pDispParams->rgvarg[1].pdispVal);
				break;
	       case 0x61f: 
				ptr = "WorkbookOpen"; 
				m_pomCtrl->FireWorkbookOpen(pDispParams->rgvarg[0].pdispVal);				
				break;
	       case 0x620: 
				ptr = "WorkbookActivate"; 
				m_pomCtrl->FireWorkbookActivate(pDispParams->rgvarg[0].pdispVal);				
				break;
	       case 0x621: 
				ptr = "WorkbookDeactivate"; 
				m_pomCtrl->FireWorkbookDeactivate(pDispParams->rgvarg[0].pdispVal);				
				break;
	       case 0x622: 
				ptr = "WorkbookBeforeClose"; 
				olBool = *pDispParams->rgvarg[1].pboolVal;
				m_pomCtrl->FireWorkbookBeforeClose(pDispParams->rgvarg[0].pdispVal,&olBool);
				*pDispParams->rgvarg[1].pboolVal = olBool;
				break;
	       case 0x623: 
				ptr = "WorkbookBeforeSave"; 
				olBool = *pDispParams->rgvarg[2].pboolVal;
				m_pomCtrl->FireWorkbookBeforeSave(pDispParams->rgvarg[0].pdispVal,pDispParams->rgvarg[1].boolVal,&olBool);
				*pDispParams->rgvarg[2].pboolVal = olBool;
				break;
	       case 0x624: 
				ptr = "WorkbookBeforePrint"; 
				olBool = *pDispParams->rgvarg[1].pboolVal;
				m_pomCtrl->FireWorkbookBeforePrint(pDispParams->rgvarg[0].pdispVal,&olBool);
				*pDispParams->rgvarg[1].pboolVal = olBool;
				break;
	       case 0x625: 
				ptr = "WorkbookNewSheet"; 
				m_pomCtrl->FireWorkbookNewSheet(pDispParams->rgvarg[0].pdispVal,pDispParams->rgvarg[1].pdispVal);
				break;
	       case 0x626: 
				ptr = "WorkbookAddinInstall"; 
				m_pomCtrl->FireWorkbookAddinInstall(pDispParams->rgvarg[0].pdispVal);				
				break;
	       case 0x627: 
				ptr = "WorkbookAddinUninstall"; 
				m_pomCtrl->FireWorkbookAddinUninstall(pDispParams->rgvarg[0].pdispVal);				
				break;
	       case 0x612: 
				ptr = "WindowResize"; 
				m_pomCtrl->FireWindowResize(pDispParams->rgvarg[0].pdispVal,pDispParams->rgvarg[1].pdispVal);
				break;
	       case 0x614: 
				ptr = "WindowActivate"; 
				m_pomCtrl->FireWindowActivate(pDispParams->rgvarg[0].pdispVal,pDispParams->rgvarg[1].pdispVal);
				break;
	       case 0x615: 
				ptr = "WindowDeactivate"; 
				m_pomCtrl->FireWindowDeactivate(pDispParams->rgvarg[0].pdispVal,pDispParams->rgvarg[1].pdispVal);
				break;
		}

//		MessageBox(NULL, ptr, "Event was fired!!!", MB_SETFOREGROUND);
		return S_OK;
	}

protected:

private:
	CConnectionAdvisor	m_AppEventsAdvisor;
	CUfisExcelCtrl*		m_pomCtrl;
};

/*----------------------------------------------------------------------------*/

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

/*----------------------------------------------------------------------------*/

#endif // !defined(AFX_EXCELEVENTSINK_H__5C5E9844_B502_11D2_A0A1_0080C7F3B56B__INCLUDED_)

