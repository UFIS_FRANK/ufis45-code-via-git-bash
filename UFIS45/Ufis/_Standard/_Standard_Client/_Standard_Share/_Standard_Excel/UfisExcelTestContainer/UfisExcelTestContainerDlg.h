// UfisExcelTestContainerDlg.h : header file
//
//{{AFX_INCLUDES()
#include "tab.h"
#include "ufisexcel.h"
//}}AFX_INCLUDES

#if !defined(AFX_UFISEXCELTESTCONTAINERDLG_H__4E9E7639_E975_4D36_94B6_0086D0B3622D__INCLUDED_)
#define AFX_UFISEXCELTESTCONTAINERDLG_H__4E9E7639_E975_4D36_94B6_0086D0B3622D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CUfisExcelTestContainerDlg dialog

class CUfisExcelTestContainerDlg : public CDialog
{
// Construction
public:
	CUfisExcelTestContainerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CUfisExcelTestContainerDlg)
	enum { IDD = IDD_UFISEXCELTESTCONTAINER_DIALOG };
	CTAB	m_Tab;
	CUfisExcel	m_Excel;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUfisExcelTestContainerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CUfisExcelTestContainerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonExcelxsl();
	afx_msg void OnButtonExcelcsv();
	afx_msg void OnButtonExceltab();
	afx_msg void OnNewWorkBookUfisexcelctrl1(LPDISPATCH popWorkbook);
	afx_msg void OnSheetChangeUfisexcelctrl1(LPDISPATCH popSheet, LPDISPATCH popTarget);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISEXCELTESTCONTAINERDLG_H__4E9E7639_E975_4D36_94B6_0086D0B3622D__INCLUDED_)
