// CCSTimeScale.cpp : implementation file
// 

#include <stdafx.h>
#include <CCSTimeScale.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif





/////////////////////////////////////////////////////////////////////////////
// CCSTimeScale



CCSTimeScale::CCSTimeScale(CWnd *popParent)
{
    CDC dc;
    dc.CreateCompatibleDC(NULL);

    LOGFONT lolFont;
    memset(&lolFont, 0, sizeof(LOGFONT));

	pomFont = new CFont;
	pomLifeStyleFont = new CFont;

	lolFont.lfCharSet= DEFAULT_CHARSET;
    lolFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    lolFont.lfWeight = FW_NORMAL;
    lolFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(lolFont.lfFaceName, "Small Fonts");
    pomFont->CreateFontIndirect(&lolFont);


	memset(&lolFont, 0, sizeof(LOGFONT));


	lolFont.lfCharSet= DEFAULT_CHARSET;
	lolFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
	lolFont.lfWeight = FW_BOLD;
	lolFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
	lstrcpy(lolFont.lfFaceName, "Arial");
	pomLifeStyleFont->CreateFontIndirect(&lolFont);


	lmBkColor = ::GetSysColor(COLOR_BTNFACE);//RGB(192, 192, 192);
	lmTextColor = RGB(0,0,128);
	lmHilightColor = RGB(255,0,0);

	pomParent = popParent;
    bmDisplayCurrentTime = true;

   	omCurrentTime = COleDateTime::GetCurrentTime();

	prmCurrMarker = NULL;
	bmLButtonDown = false;

	pomStaticToolTip = NULL;
	//MWO: 25.06.03
	lfTimeScale_ColorIdx = 7;
	lfTimeScale_Percent  = 50;
	lfTimeScale_StyleUp  = FALSE;
	lfTimeScale_Type     = 2;
}

CCSTimeScale::~CCSTimeScale()
{
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This code has been written to avoid AfxAssert() failed(). The situation
// is ~GateDagram() is called the ~CCSTimeScale() is called but there is no
// more time scale window opened.
    for (int ilIndex = 0; ilIndex < omTSIArray.GetSize(); ilIndex++)
    {
        delete (CTopScaleIndicator *) omTSIArray[ilIndex];
    }
////////////////////////////////////////////////////////////////////////
// RST 06/08/1997:

	DeleteAllMarker();
	delete pomFont;
	delete pomLifeStyleFont;
////////////////////////////////////////////////////////////////////////


/*
    while (omTSIArray.GetSize() > 0)
	{
		CTopScaleIndicator *p = (CTopScaleIndicator *)omTSIArray[0];
		delete p;
	}

    omTSIArray.RemoveAll();
*/
////////////////////////////////////////////////////////////////////////
}

BEGIN_MESSAGE_MAP(CCSTimeScale, CWnd)
    //{{AFX_MSG_MAP(CCSTimeScale)
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_CREATE()
	ON_WM_CTLCOLOR()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


void CCSTimeScale::EnableDisplayCurrentTime(bool bpDisplayCurrentTime)
{
    bmDisplayCurrentTime = bpDisplayCurrentTime;
}


void CCSTimeScale::SetDisplayTimeFrame(COleDateTime opDisplayStart, COleDateTimeSpan opDuration, COleDateTimeSpan opInterval)
{
    SetDisplayStartTime(opDisplayStart);
    SetTimeInterval(opInterval);
    
    CRect olClientRect; GetClientRect(&olClientRect);

    imP0 = olClientRect.Height() / 2;
    imP1 = imP0 * 125 / 100;
    imP2 = imP0 * 150 / 100;
    imP3 = olClientRect.bottom;

    int ilSlotCount = int (opDuration.GetTotalMinutes() / opInterval.GetTotalMinutes());
    // for the right most pixel
    fmIntervalWidth = (double) (olClientRect.Width() - 1) / ilSlotCount;
	//TRACE("number of slot %d, width %g", ilSlotCount, fmIntervalWidth);
}

void CCSTimeScale::SetDisplayStartTime(COleDateTime opDisplayStart)
{
    // DisplayStart Time must begin at 0 seconds
    omDisplayStart = COleDateTime(
        opDisplayStart.GetYear(), opDisplayStart.GetMonth(),
        opDisplayStart.GetDay(), opDisplayStart.GetHour(),
        opDisplayStart.GetMinute(), 0
    );

    
    //omDisplayStart = opDisplayStart;
}

COleDateTime CCSTimeScale::GetDisplayStartTime(void)
{
    return omDisplayStart;
}
   

void CCSTimeScale::SetTimeInterval(COleDateTimeSpan opInterval)
{
    omInterval = opInterval;
}

COleDateTimeSpan CCSTimeScale::GetDisplayDuration(void)
{
    CRect olClientRect; GetClientRect(&olClientRect);

    return COleDateTimeSpan(0, 0, int (omInterval.GetTotalMinutes() * (olClientRect.Width() - 1) / fmIntervalWidth), 0);
}


////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This block of code is used for make the TimeScale be able to help
// the other classes which desire to calculate the time backward from the
// given point.
COleDateTime CCSTimeScale::GetTimeFromX(int ipX)	// offset of ipX is zero
{
    /*
	long llSeconds = (long)((ipX) * omInterval.GetTotalSeconds() / (fmIntervalWidth * 60));
        // llSeconds is measured from omDisplayStart
        // fmIntervalWidth keep number of pixel per each interval
    long llMinutes = llSeconds / 60;
    llSeconds %= 60;
    */
    long llSeconds = 0;
    long llMinutes = (long)((ipX) * omInterval.GetTotalMinutes() / fmIntervalWidth);

// DTT Jul.29 -- I've found some bug, the GetTimeFromX() and GetXFromTime()
// cannot work together correctly, i.e., they are not invert function of each other.
// So when we convert X to time back-and-forth again-and-again, some error will
// happened. I still unsure about what the real reason of this. But I think we
// may very ugly and dirty to fix this bug by plus one minute.
	llMinutes++;
////////////////

    long llHours = llMinutes / 60;
    llMinutes %= 60;
    return (omDisplayStart + COleDateTimeSpan(0, llHours, llMinutes, llSeconds));
}
////////////////////////////////////////////////////////////////////////

int CCSTimeScale::GetXFromTime(COleDateTime opTime)
{
	COleDateTimeSpan olDiff = opTime - omDisplayStart;
    double llTotalMin = olDiff.GetTotalMinutes();
    int ilRet = (int)(fmIntervalWidth * llTotalMin / omInterval.GetTotalMinutes());
	return ilRet;
}

void CCSTimeScale::UpdateCurrentTimeLine(COleDateTime opTime)
{
	int ilX		= GetXFromTime(opTime);
	int ilOldX	= GetXFromTime(omCurrentTime);

	if (ilX != ilOldX)
	{
		omCurrentTime = opTime;

		CRect olTSRect;
		GetClientRect(&olTSRect);

		olTSRect.left  = ilX;
		olTSRect.right = ilX + 1;	// must be +1

		InvalidateRect(&olTSRect);
    }
}

void CCSTimeScale::AddTopScaleIndicator(COleDateTime opStartTime, COleDateTime opEndTime, COLORREF lpColor)
{
    CTopScaleIndicator *polTSI = new CTopScaleIndicator;
    polTSI->omStart = opStartTime;
    polTSI->omEnd = opEndTime;
    polTSI->lmColor = lpColor;
    
    AddTopScaleIndicator(polTSI);
}

void CCSTimeScale::DisplayTopScaleIndicator(CDC *popDC)
{
    CPen *polOldPen = (CPen *) popDC->SelectStockObject(BLACK_PEN);
    
    CPen olPen;
    CTopScaleIndicator *polTSI;
    for (int ilIndex = 0; ilIndex < omTSIArray.GetSize(); ilIndex++)
    {
        polTSI = (CTopScaleIndicator *) omTSIArray.GetAt(ilIndex);

        olPen.CreatePen(PS_SOLID, 1, polTSI->lmColor);
                
        popDC->SelectObject(&olPen);
        
        popDC->MoveTo(GetXFromTime(polTSI->omStart), (imP3 - 3) - (ilIndex * (2 + 2)));
        popDC->LineTo(GetXFromTime(polTSI->omEnd) + 1, (imP3 - 3) - (ilIndex * (2 + 2)));
        
        popDC->MoveTo(GetXFromTime(polTSI->omStart), (imP3 - 3) - 1 - (ilIndex * (2 + 2)));
        popDC->LineTo(GetXFromTime(polTSI->omEnd) + 1, (imP3 - 3) - 1 - (ilIndex * (2 + 2)));


        /* TRACE("Display TSI: [%ld][%03d]\n",
            (polTSI->omStart - omDisplayStart).GetTotalMinutes(),
            GetXFromTime(polTSI->omStart)
        ); */

        
        //popDC->SelectObject(polOldPen);
        
        olPen.DeleteObject();
    }
    
    //popDC->SelectObject(polOldPen);
}

bool CCSTimeScale::TSIPos(int *ipLeft, int *ipRight)
{
    bool blRet = true;
    int ilTSISize = omTSIArray.GetSize();
    if (ilTSISize == 0)
        blRet = false;
    else
    {
        int ilMinLeft, ilMaxRight;
        CTopScaleIndicator *polTSI = (CTopScaleIndicator *) omTSIArray.GetAt(0);

        if (ilTSISize == 1)
        {
            *ipLeft = GetXFromTime(polTSI->omStart);
            *ipRight = GetXFromTime(polTSI->omEnd);
        }
        else
        {
            ilMinLeft = GetXFromTime(polTSI->omStart);
            ilMaxRight = GetXFromTime(polTSI->omEnd);
        
            int ilLeft, ilRight;
            for (int ilIndex = 1; ilIndex < ilTSISize; ilIndex++)
            {
                polTSI = (CTopScaleIndicator *) omTSIArray.GetAt(ilIndex);


                ilLeft = GetXFromTime(polTSI->omStart);
                if (ilLeft < ilMinLeft)
                    ilMinLeft = ilLeft;
            
                ilRight = GetXFromTime(polTSI->omEnd);
                if (ilRight > ilMaxRight)
                    ilMaxRight = ilRight;
            }
            *ipLeft = ilMinLeft;
            *ipRight = ilMaxRight;
        }
    }

    (*ipRight)++;                       // InvalidateRect()'s rule of thumb
    return blRet;
}

void CCSTimeScale::DisplayTopScaleIndicator(void)
{
    // find the right place to InvalidateRect
    int ilLeft, ilRight;
    if (TSIPos(&ilLeft, &ilRight))
    {
        CRect olClientRect; GetClientRect(&olClientRect);
        CRect olRect(ilLeft, olClientRect.top, ilRight, olClientRect.bottom);
        
        InvalidateRect(&olRect, false);
    }
}
 
void CCSTimeScale::RemoveAllTopScaleIndicator(void)
{
    // find the right place to InvalidateRect
    int ilLeft, ilRight;
    if (TSIPos(&ilLeft, &ilRight))
    {
        CRect olClientRect; GetClientRect(&olClientRect);
        CRect olRect(ilLeft, olClientRect.top, ilRight, olClientRect.bottom);
        
        InvalidateRect(&olRect, true);

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This code has been written to avoid AfxAssert() failed(). The situation
// is ~GateDagram() is called the ~CCSTimeScale() is called but there is no
// more time scale window opened.
/*		while (omTSIArray.GetSize() > 0)
		{
			CTopScaleIndicator *p = (CTopScaleIndicator *)omTSIArray[0];
			delete p;
		}
*/

        for (int ilIndex = 0; ilIndex < omTSIArray.GetSize(); ilIndex++)
        {
            delete (CTopScaleIndicator *) omTSIArray[ilIndex];
        }

        omTSIArray.RemoveAll();
////////////////////////////////////////////////////////////////////////
    }
}
 
/////////////////////////////////////////////////////////////////////////////
// CCSTimeScale message handlers


void CCSTimeScale::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	MyDraw(&dc);
}

void CCSTimeScale::MyDraw( CDC* pdc )
{	
   	CDC memDC;
	memDC.CreateCompatibleDC (pdc);
	memDC.SelectObject (&bmpArea);
    
    // Do not call CWnd::OnPaint() for painting messages
    CRect olClientRect; GetClientRect(&olClientRect);
	CBrush olBrush(lmBkColor);
	CBrush *polOldBrush = memDC.SelectObject(&olBrush);
	if(m_lifeStyle == FALSE)
	{
		memDC.PatBlt(olClientRect.left, olClientRect.top,
					 olClientRect.Width(), olClientRect.Height(),
					 PATCOPY);
		lmTextColor = RGB(0,0,128);

	}
	else
	{
		DrawLifeStyle(&memDC, olClientRect, lfTimeScale_ColorIdx, lfTimeScale_Type, lfTimeScale_StyleUp, lfTimeScale_Percent);
		lmTextColor = RGB(0,0,255);
	}

	memDC.SelectObject(polOldBrush);

    int ilW;
	ilW = olClientRect.right - olClientRect.left;
	if(ilW < 10)
	{
		return;
	}
    CFont *polOldFont;
	if(m_lifeStyle == FALSE)
	{
		polOldFont = memDC.SelectObject(pomFont);
	}
	else
	{
		polOldFont = memDC.SelectObject(pomLifeStyleFont);
	}
    CPen olPen(PS_SOLID, 1, lmTextColor);
    CPen *polOldPen = memDC.SelectObject(&olPen);
    memDC.SetBkMode(TRANSPARENT);
    memDC.SetTextColor(lmTextColor);
   
    char clHeader[18];
    int ilDiffMin = omDisplayStart.GetMinute() % (int) omInterval.GetTotalMinutes();
    
    double flCurX = 0.0;
    COleDateTime olCurTime = omDisplayStart;
    if (ilDiffMin > 0)
    {
        // for the left most pixel
        
    
        ilDiffMin = (int) omInterval.GetTotalMinutes() - ilDiffMin;
        
        flCurX = ilDiffMin * fmIntervalWidth / omInterval.GetTotalMinutes();
        olCurTime += COleDateTimeSpan(0, 0, ilDiffMin, 0);
    }
    //TRACE("Display First Time: %03d %07.3f\n", ilDiffMin, flCurX);
    int liCnt = 0;

    while (flCurX <= olClientRect.right)
    {
        memDC.MoveTo((int) flCurX, imP3);
        //liCnt++;
        if ((olCurTime.GetMinute() % 60) == 0)
        {
            memDC.LineTo((int) flCurX, imP0);

            sprintf(clHeader, "%02d00", olCurTime.GetHour());
            memDC.TextOut((int) flCurX - (memDC.GetTextExtent(clHeader, strlen (clHeader)).cx / 2), 2,
                clHeader, strlen (clHeader));
			if (liCnt==0)
			{
				sprintf(clHeader, "%02d/%02d/%04d", olCurTime.GetDay(), olCurTime.GetMonth(), olCurTime.GetYear());
				memDC.TextOut((int) flCurX + 2, memDC.GetTextExtent(clHeader, strlen(clHeader)).cy + 2,
					clHeader, strlen (clHeader));
				liCnt = liCnt+1;
			}
            //TRACE("Display Time: %s %03d\n", clHeader, (int) flCurX);
        }                      
        else if ((olCurTime.GetMinute() % 30) == 0)
            memDC.LineTo((int) flCurX, imP1);
        else
            memDC.LineTo((int) flCurX, imP2);
        
        
        olCurTime += omInterval;
        flCurX += fmIntervalWidth;
    }
    //

    DisplayTopScaleIndicator((CDC *) &memDC);
    
    // display current time 
    if (bmDisplayCurrentTime)
    {
        //COleDateTime olLTime = COleDateTime::GetCurrentTime();
        //int ilX = GetXFromTime(olLTime);
		int ilX = GetXFromTime(omCurrentTime);

        CPen olHPen(PS_SOLID, 1, lmHilightColor);
        memDC.SelectObject(&olHPen);
    
        memDC.MoveTo(ilX, olClientRect.top);
        memDC.LineTo(ilX, imP3);
    
        //omOldCurrentTime = olLTime;
		//omOldCurrentTime = omCurrentTime;
    }
    //
    //memDC.SelectObject(polOldPen);
    //memDC.SelectObject(polOldFont);


	////////////////////////////////////////////////////////////////////////	
	// RST 06/08/1997:
	// draws the marker
	
	MARKER *prlMarker;
	CPoint olPoints[3];
    polOldPen = memDC.SelectObject(&olPen);
    CBrush *polBrush;

	CRgn *polRgn;
	int ilXPos;
	int ilSize;
	COleDateTime olTime;
	int ilAnzMarkerAtTime;
	int ilLc;
	int ildy;

    for (int  ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		ilAnzMarkerAtTime = 0;
	    for ( ilLc = omMarker.GetSize() - 1; ilLc > ilIndex ; ilLc--)
	    {
			if(omMarker[ilIndex].ActTime == omMarker[ilLc].ActTime) 
				ilAnzMarkerAtTime++;
		}

		prlMarker = &omMarker[ilIndex];
		olTime = prlMarker->ActTime;

		ilXPos = GetXFromTime(olTime) ;
		ilSize = (int)((olClientRect.bottom - olClientRect.top) / 2);
		ildy = (int)(ilAnzMarkerAtTime * (int)(ilSize / 2));

		if(prlMarker->Align == TSM_LEFT)
		{
			olPoints[0].x = ilXPos ;
			olPoints[0].y = olClientRect.bottom - ildy;

			olPoints[1].x = ilXPos ; 
			olPoints[1].y = olClientRect.bottom - ilSize - ildy;
			
			olPoints[2].x = ilXPos + (int)(ilSize / 1.8);
			olPoints[2].y = olClientRect.bottom  - (int)(ilSize / 2) - ildy;
		}

		if(prlMarker->Align == TSM_RIGHT)
		{
			olPoints[0].x = ilXPos + 1;
			olPoints[0].y = olClientRect.bottom - ildy;

			olPoints[1].x = ilXPos + 1; 
			olPoints[1].y = olClientRect.bottom - ilSize - ildy;
			
			olPoints[2].x = ilXPos - (int)(ilSize / 1.8) + 1 ;
			olPoints[2].y = olClientRect.bottom  - (int)(ilSize / 2) - ildy;
		}

		if(prlMarker->Align == TSM_UP)
		{
			olPoints[0].x = ilXPos + 1;
			olPoints[0].y = olClientRect.bottom - ildy;

			olPoints[1].x = ilXPos + (int)(ilSize / 2) ; 
			olPoints[1].y = olClientRect.bottom - (int)(ilSize / 2) - ildy;
			
			olPoints[2].x = ilXPos - (int)(ilSize / 2)  ;
			olPoints[2].y = olClientRect.bottom  - (int)(ilSize / 2) - ildy;
		}
		polBrush = new CBrush(prlMarker->Color);
		polRgn = new CRgn;
		polRgn->CreatePolygonRgn( olPoints, 3,  WINDING);
		memDC.FillRgn( polRgn, polBrush );
		delete polRgn;
		delete polBrush;
	}
    //memDC.SelectObject(polOldPen);
	//calculate the bitmap dimensions
	int x	= olClientRect.left;
	int y	= olClientRect.top;
	int dx	= olClientRect.right  - x;
	int dy	= olClientRect.bottom - y;

	//you have finished the bitmap ;o))
	//now move it to the real screen
	pdc->BitBlt ( x, y, dx, dy, &memDC, x, y, SRCCOPY);

	// RST
	////////////////////////////////////////////////////////////////////////	
}

/////////////////////////////////////////////////////////////////////////////
// RST   14.08.1997

void CCSTimeScale::OnMouseMove(UINT nFlags, CPoint point) 
{

	MARKER *prlMarker;
	CPoint olPoints[3];
	CRect olClientRect;
	CRect olMarkerRect;
	CRect olMarkerNewRect;
	GetClientRect(&olClientRect);
	
	bool blOK = false;
	int ilXPos;
	COleDateTime olTime;
	COleDateTime olActMarkerTime;
	int ilSize = (int)((olClientRect.bottom - olClientRect.top) / 2);

	SetCapture( );
	
	if((!olClientRect.PtInRect(point)) && (prmCurrMarker == NULL))
	{
		ReleaseCapture();
		if(pomStaticToolTip != NULL)
		{
			pomStaticToolTip->DestroyWindow();
			delete pomStaticToolTip;
			pomStaticToolTip = NULL;
			UpdateWindow();
		}
	}
	else
	{
		if((prmCurrMarker != NULL) && bmLButtonDown)
		{
				olActMarkerTime = prmCurrMarker->ActTime;
				ilXPos = GetXFromTime(olActMarkerTime) ;

				olMarkerRect.top	= olClientRect.top;
				olMarkerRect.bottom = olClientRect.bottom;
				olMarkerRect.left	= ilXPos - (int)(ilSize);
				olMarkerRect.right	= ilXPos + (int)(ilSize);

				olTime = GetTimeFromX(point.x);
				int ilMinute = olTime.GetMinute();
				int ilHour = olTime.GetHour();
				int ilMinutes = ilHour * 60 + ilMinute;

				int ilRest = (int)(ilMinutes % prmCurrMarker->RasterWidth);
				ilMinutes = ilMinutes - ilRest;


				ilHour = (int)(ilMinutes / 60);
				if(ilMinutes > 0) 
					ilMinute = (int)(ilMinutes % 60);
				else
					ilMinute = ilMinutes;

				if(ilRest > (int)(prmCurrMarker->RasterWidth / 2))
					ilMinute += prmCurrMarker->RasterWidth;


				COleDateTime olTimeTmp(COleDateTime(olTime.GetYear(), olTime.GetMonth(), olTime.GetDay(), ilHour, ilMinute, 0));
				
				if((olTimeTmp != olActMarkerTime) && 
				   (prmCurrMarker->RangeFrom != NULL) && 
				   (prmCurrMarker->RangeFrom != NULL))
				{
					if((prmCurrMarker->RangeFrom <= olTimeTmp) && 
					   (prmCurrMarker->RangeTo >= olTimeTmp) && 
					   (TestMarkerAssignPos(prmCurrMarker, olTimeTmp)))
						blOK = true;
				}

				if(blOK)
				{
					ilXPos = GetXFromTime(olTimeTmp) ;
					prmCurrMarker->ActTime = olTimeTmp;

					olMarkerNewRect.top	= olClientRect.top;
					olMarkerNewRect.bottom = olClientRect.bottom;
					olMarkerNewRect.left	= ilXPos - (int)(ilSize) ;
					olMarkerNewRect.right	= ilXPos + (int)(ilSize) ;

					InvalidateRect(olMarkerNewRect);
					InvalidateRect(olMarkerRect);

					if(pomParent == NULL)
						pomParent = GetParent();
					if(pomParent != NULL)
						pomParent->SendMessage(WM_TSCALE_MARKERMOVED,prmCurrMarker->ID,(LPARAM)&prmCurrMarker->ActTime);
			
				}
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
		}
		else
		{
			CCSPtrArray<int> olHits;
			CCSPtrArray<int> olSide;

			prmCurrMarker = NULL;

			for (int  ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
			{
				prlMarker = &omMarker[ilIndex];
				olTime = prlMarker->ActTime;

				ilXPos = GetXFromTime(olTime) ;
				ilSize = (int)((olClientRect.bottom - olClientRect.top) / 2);

				olMarkerRect.top	= olClientRect.top;
				olMarkerRect.bottom = olClientRect.bottom;
				olMarkerRect.left	= ilXPos - 2;
				olMarkerRect.right	= ilXPos + 2;

				if(olMarkerRect.PtInRect(point))
				{
					olHits.New(ilIndex);
				}
			}
			if(olHits.GetSize() > 0)
			{
				int ilAnz = olHits.GetSize();
				int ilBottom = 0;
				int ilTop	= olClientRect.top;
				CString olToolTipText;

				for(int ilLc = 0; ilLc < olHits.GetSize(); ilLc++)
				{
					prlMarker = &omMarker[olHits[ilLc]];
					olTime = prlMarker->ActTime;
					olToolTipText = prlMarker->ToolTipText;

					ilXPos = GetXFromTime(olTime) ;
					ilTop = olClientRect.bottom - (ilSize / 2) * (ilLc + 1);
					ilBottom = olClientRect.bottom - (ilSize / 2) * ilLc;

					if((point.y < ilBottom ) && (point.y > ilTop))
						break;

				}

				if(olClientRect.bottom - (ilSize +  (olHits.GetSize() - 1) * (ilSize / 2)) < point.y) 
				{
					prmCurrMarker = prlMarker; 
					if(!olToolTipText.IsEmpty())
					{
						int ilXMid = (GetXFromTime(omDisplayStart + GetDisplayDuration())- GetXFromTime(omDisplayStart)) / 2;
						CDC *polDC;
						polDC = GetDC();
						CSize olTextSize = polDC->GetTextExtent(olToolTipText );

						olMarkerRect.top	= 3; 
						olMarkerRect.bottom = 3 + olTextSize.cy + 2;
						if(point.x > ilXMid)
						{
							olMarkerRect.left	= ilXPos - (ilSize / 2) - olTextSize.cx - 6;
							olMarkerRect.right	= ilXPos - (ilSize / 2);
						}
						else
						{
							olMarkerRect.left	= ilXPos + (ilSize / 2);
							olMarkerRect.right	= ilXPos + (ilSize / 2) + olTextSize.cx + 6;
						}

						if(pomStaticToolTip != NULL)
						{
							CString olStr;
							pomStaticToolTip->GetWindowText(olStr);
							if(olStr != olToolTipText)
							{
								pomStaticToolTip->DestroyWindow();
								delete pomStaticToolTip;
								pomStaticToolTip = NULL;
								UpdateWindow();
							}
						}

						if(pomStaticToolTip == NULL)
						{
							pomStaticToolTip = new CStatic;
							pomStaticToolTip->Create( olToolTipText, WS_CHILD | WS_VISIBLE | WS_BORDER | SS_CENTER , olMarkerRect , this, 1);
							pomStaticToolTip->UpdateWindow();
						}
					}
					if(pomParent == NULL)
						pomParent = GetParent();
					if(pomParent != NULL)
						pomParent->SendMessage(WM_TSCALE_MARKERSELECT,prmCurrMarker->ID ,(LPARAM)&olTime);
				}
			}		
			else
			{
				olTime = GetTimeFromX(point.x);
				if(pomParent == NULL)
					pomParent = GetParent();
				if(pomParent != NULL)
					pomParent->SendMessage(WM_TSCALE_MARKERSELECT,NULL ,(LPARAM)&olTime);
				if(pomStaticToolTip != NULL)
				{
					pomStaticToolTip->DestroyWindow();
					delete pomStaticToolTip;
					pomStaticToolTip = NULL;
					UpdateWindow();
				}
			}
		}
	}
	CWnd::OnMouseMove(nFlags, point);
}



void CCSTimeScale::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if(pomStaticToolTip != NULL)
	{
		pomStaticToolTip->DestroyWindow();
		delete pomStaticToolTip;
		pomStaticToolTip = NULL;
	}

	if(prmCurrMarker != NULL)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
		bmLButtonDown = true;
	}


	CWnd::OnLButtonDown(nFlags, point);
}


void CCSTimeScale::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CRect olClientRect;
	GetClientRect(&olClientRect);
	
	if(!olClientRect.PtInRect(point))
		ReleaseCapture();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	prmCurrMarker = NULL;
	bmLButtonDown = false;
	CWnd::OnLButtonUp(nFlags, point);
}

  
void CCSTimeScale::SetMarkerRaster(int ipID, int ipRasterWidth)
{

	MARKER *prlMarker;
    for (int ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		if(ipID == omMarker[ilIndex].ID)
		{
			prlMarker = &omMarker[ilIndex];
			prlMarker->RasterWidth		= ipRasterWidth;
			return;
		}
    }
	
	return ;
}


bool CCSTimeScale::ChangeMarkerRange(int ipID, COleDateTime opNewTime, COleDateTime opRangeFrom, COleDateTime opRangeTo)
{
	MARKER *prlMarker;
    for (int ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		if(ipID == omMarker[ilIndex].ID)
		{
			prlMarker = &omMarker[ilIndex];
			prlMarker->RangeFrom	= opRangeFrom;
			prlMarker->RangeTo		= opRangeTo;
			if(opNewTime != NULL)
				prlMarker->ActTime		= opNewTime;
			return true;
		}
    }
	return false;
}


bool CCSTimeScale::ChangeMarkerColor(int ipID, COLORREF opColor)
{
	MARKER *prlMarker;
    for (int ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		if(ipID == omMarker[ilIndex].ID)
		{
			prlMarker = &omMarker[ilIndex];
			prlMarker->Color = opColor;
			return true;
		}
    }
	return false;
}


bool CCSTimeScale::ChangeMarkerPos(int ipID, COleDateTime opTime)
{
	MARKER *prlMarker;
    for (int ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		if(ipID == omMarker[ilIndex].ID)
		{
			prlMarker = &omMarker[ilIndex];
			prlMarker->ActTime		= opTime;
			InvalidateRect(NULL);
			return true;
		}
    }
	return false;
}

void CCSTimeScale::AddMarker(int ipID, COleDateTime opTime, 
				   COLORREF opColor, 
				   int ipRasterWidth, 
				   int ipAlign,
				   int ipIDAssign,
				   COleDateTime opRangeFrom, 
				   COleDateTime opRangeTo,
				   CString opToolTipText)
{
	DeleteMarker(ipID);
	MARKER *polMarker = new MARKER;
	omMarker.Add(polMarker);

	polMarker->RangeFrom	= opRangeFrom;
	polMarker->RangeTo		= opRangeTo;
	polMarker->ID			= ipID;
	polMarker->Align		= ipAlign;
	polMarker->Assign		= ipIDAssign;
	polMarker->ActTime		= opTime;
	polMarker->Color		= opColor;
	polMarker->RasterWidth	= ipRasterWidth;
	polMarker->ToolTipText	= opToolTipText;
}


void CCSTimeScale::DeleteMarker(int ipID)
{
	MARKER *prlMarker;
	int ilSize, ilXPos;
	CRect	olMarkerRect;
	CRect olClientRect;
	GetClientRect(&olClientRect);

    for (int ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		if(ipID == omMarker[ilIndex].ID)
		{
			prlMarker = &omMarker[ilIndex];
		
			ilSize = (int)((olClientRect.bottom - olClientRect.top) / 2);
			ilXPos = GetXFromTime(prlMarker->ActTime) ;
			olMarkerRect.top	= olClientRect.bottom - ilSize;
			olMarkerRect.bottom = olClientRect.bottom;
			olMarkerRect.left	= ilXPos - (int)(ilSize / 1,7) ;
			olMarkerRect.right	= ilXPos + (int)(ilSize / 1.7) ;
			InvalidateRect(olMarkerRect);

			omMarker.DeleteAt(ilIndex);
			break;
		}
    }
}


void CCSTimeScale::DeleteAllMarker()
{
    omMarker.DeleteAll();
}


bool CCSTimeScale::TestMarkerAssignPos(MARKER *prpMarker, COleDateTime opNewTime)
{
	if(prpMarker->Assign == 0)
		return true;

	MARKER *prlMarker;
    for (int ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		if(prpMarker->Assign == omMarker[ilIndex].ID)
		{
			prlMarker = &omMarker[ilIndex];
		}
	}
	if(prlMarker == NULL)
		return true;

	if(prlMarker->ActTime > prpMarker->ActTime)
	{
		if(prlMarker->ActTime <= opNewTime)
			return false;
	}
	else
	{
		if(prlMarker->ActTime >= opNewTime)
			return false;
	}

	return true;
}
/*
HBRUSH CCSTimeScale::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = 0;

	CWnd *polWnd = GetDlgItem(1);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		pDC->SetBkColor(RGB(255,255,217));
		hbr = CreateSolidBrush(RGB(255,255,217));
		return hbr;
	}
	
	return hbr;
}
*/

// RST  
/////////////////////////////////////////////////////////////////////////////

int CCSTimeScale::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	//- - - - - - - - - - - - - - - -
	//- - bitmap section
	//- - - - - - - - - - - - - - - -
	// get rid of old bitmaps
	CRect olClientRect; GetClientRect(&olClientRect);
	if (bmpArea.m_hObject != NULL)
		bmpArea.DeleteObject();

	CClientDC dc (this);
	CDC dcMem;
	dcMem.CreateCompatibleDC (&dc);

	//now create the bitmaps - without the moving bitmap, because its size changes!
	bmpArea.CreateCompatibleBitmap	(&dc, olClientRect.right, olClientRect.bottom);

	ASSERT (bmpArea.m_hObject != NULL);

	dcMem.DeleteDC();
	
	return 0;
}

void CCSTimeScale::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);

	CRect olClientRect;
	GetClientRect(&olClientRect);

	if (bmpArea.m_hObject != NULL)
		bmpArea.DeleteObject();
	CClientDC dc (this);
	CDC dcMem;
	dcMem.CreateCompatibleDC (&dc);
	bmpArea.CreateCompatibleBitmap	(&dc, olClientRect.right, olClientRect.bottom);
	if (bmpArea.m_hObject != NULL)
	{
		return;
	}
	dcMem.DeleteDC();
	
}

void CCSTimeScale::DrawLifeStyle(CDC *pdc, CRect opRect, int MyColor, int ipMode, BOOL DrawDown, int ipPercent)
{
    int intBLUESTART = 255;
    int intBLUEEND = 0;
    double intBANDHEIGHT = 1.01;//15;
    int intSHADOWSTART = 64;
    int intSHADOWCOLOR = 0;
    int intTEXTSTART = 0;
    int intTEXTCOLOR = 15;
    int intRed = 1;
    int intGreen = 2;
    int intBlue = 4;
    int intBackRed = 8;
    int intBackGreen = 16;
    int intBackBlue = 32;
    float sngBlueCur;
    float sngBlueStep;
    long intFormHeight;
    long intFormWidth;
    long intX;
    long intY;
    int iColor;
	int ilColorValue = 255;

	ilColorValue = (int)(ilColorValue/100)*ipPercent;
    long iRed, iBlue, iGreen;
    CString prntText;
    COLORREF ReturnColor;
	CPen *pOldPen;
	sngBlueCur = 0;
	
	//pOldPen = pdc->SelectObject(&olPen);
    ReturnColor = COLORREF(RGB(255,255,255)); //vbWhite
    if( MyColor >= 0)
	{
        intFormHeight = opRect.bottom - opRect.top; //MyPanel.ScaleHeight
        intFormWidth =  opRect.right - opRect.left; //MyPanel.ScaleWidth
    
        iColor = MyColor;
        sngBlueCur = (float)intBLUESTART;
    
        if( DrawDown == TRUE)
		{
            if( ipMode == 0)
			{
                sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormHeight);
				sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
				//if(sngBlueStep == 0) sngBlueStep = 1;
                for( intY = 0; intY <= intFormHeight; intY+= (int)intBANDHEIGHT)
				{
					CPen olPen;
                    if( iColor & intBlue) iBlue = (long)sngBlueCur;
                    if( iColor & intRed) iRed = (long)sngBlueCur;
                    if( iColor & intGreen) iGreen = (long)sngBlueCur;
                    if( iColor & intBackBlue) iBlue = 255 - (long)sngBlueCur;
                    if( iColor & intBackRed) iRed = 255 - (long)sngBlueCur;
                    if( iColor & intBackGreen) iGreen = 255 - (long)sngBlueCur;

					olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
					pOldPen = pdc->SelectObject(&olPen);
					pdc->MoveTo(0, intY);
					pdc->LineTo(intFormWidth, intY);
                    //MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intB&HEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep;
                    if( intY == 0 ) ReturnColor = COLORREF(RGB(iRed, iGreen, iBlue));
					pdc->SelectObject(pOldPen); 
                }
			}
            if(ipMode == 1)
			{
                sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormWidth);
				sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
                for( intX = 0;  intX <= intFormWidth; intX += (int)intBANDHEIGHT)
				{
					CPen olPen;
                    if( iColor & intBlue ) iBlue = (long)sngBlueCur;
                    if( iColor & intRed ) iRed = (long)sngBlueCur;
                    if( iColor & intGreen ) iGreen = (long)sngBlueCur;
                    if( iColor & intBackBlue ) iBlue = 255 - (long)sngBlueCur;
                    if( iColor & intBackRed ) iRed = 255 - (long)sngBlueCur;
                    if( iColor & intBackGreen ) iGreen = 255 - (long)sngBlueCur;
					olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
					pOldPen = pdc->SelectObject(&olPen);
					pdc->MoveTo(intX, 0);
					pdc->LineTo(intX + (int)intBANDHEIGHT, intFormHeight);
                    //MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep;
					pdc->SelectObject(pOldPen); 
                }
			}
		}
        else
		{
            if(  ipMode == 0 )
			{
                sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormHeight);
				sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
                for( intY = intFormHeight; intY >= -1; intY -= (int)intBANDHEIGHT)
				{
					CPen olPen;
                    if( iColor & intBlue ) iBlue = (long)sngBlueCur;
                    if( iColor & intRed ) iRed = (long)sngBlueCur;
                    if( iColor & intGreen ) iGreen = (long)sngBlueCur;
                    if( iColor & intBackBlue ) iBlue = 255 - (long)sngBlueCur;
                    if( iColor & intBackRed ) iRed = 255 - (long)sngBlueCur;
                    if( iColor & intBackGreen ) iGreen = 255 - (long)sngBlueCur;
					olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
					pOldPen = pdc->SelectObject(&olPen);
					pdc->MoveTo(0, intY);
					pdc->LineTo(intFormWidth, intY + (int)intBANDHEIGHT);
                    //MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep;
					pdc->SelectObject(pOldPen); 
                }
			}
            if( ipMode == 1)
			{
                sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormWidth);
				sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
                for( intX = intFormWidth; intX >= 0; intX -= (int)intBANDHEIGHT)
				{
					CPen olPen;
                    if( iColor & intBlue ) iBlue = (long)sngBlueCur;
                    if( iColor & intRed ) iRed = (long)sngBlueCur;
                    if( iColor & intGreen ) iGreen = (long)sngBlueCur;
                    if( iColor & intBackBlue ) iBlue = 255 - (long)sngBlueCur;
                    if( iColor & intBackRed ) iRed = 255 - (long)sngBlueCur;
                    if( iColor & intBackGreen ) iGreen = 255 - (long)sngBlueCur;
					olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
					pOldPen = pdc->SelectObject(&olPen);
					pdc->MoveTo(intX, 0);
					pdc->LineTo(intX /*+ (int)intBANDHEIGHT*/, intFormHeight);
                    //MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep;
					pdc->SelectObject(pOldPen); 
                }
			}
		}
		if( ipMode == 2)
		{
            sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormHeight)*2;
			sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
			//if(sngBlueStep == 0) sngBlueStep = 1;
            for( intY = ((int)intFormHeight/2); intY >= 0; intY-= (int)intBANDHEIGHT)
			{
				CPen olPen;
                if( iColor & intBlue) iBlue = (long)sngBlueCur;
                if( iColor & intRed) iRed = (long)sngBlueCur;
                if( iColor & intGreen) iGreen = (long)sngBlueCur;
                if( iColor & intBackBlue) iBlue = 255 - (long)sngBlueCur;
                if( iColor & intBackRed) iRed = 255 - (long)sngBlueCur;
                if( iColor & intBackGreen) iGreen = 255 - (long)sngBlueCur;

				olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
				pOldPen = pdc->SelectObject(&olPen);
				pdc->MoveTo(0, intY);
				pdc->LineTo(intFormWidth, intY);
                //MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intB&HEIGHT), RGB(iRed, iGreen, iBlue), BF
                sngBlueCur = sngBlueCur + sngBlueStep;
                if( intY == 0 ) ReturnColor = COLORREF(RGB(iRed, iGreen, iBlue));
				pdc->SelectObject(pOldPen); 
            }
			sngBlueCur = -1;
            for( intY = (int)intFormHeight/2; intY <= intFormHeight; intY+= (int)intBANDHEIGHT)
			{
				CPen olPen;
                if( iColor & intBlue) iBlue = (long)sngBlueCur;
                if( iColor & intRed) iRed = (long)sngBlueCur;
                if( iColor & intGreen) iGreen = (long)sngBlueCur;
                if( iColor & intBackBlue) iBlue = 255 - (long)sngBlueCur;
                if( iColor & intBackRed) iRed = 255 - (long)sngBlueCur;
                if( iColor & intBackGreen) iGreen = 255 - (long)sngBlueCur;

				olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
				pOldPen = pdc->SelectObject(&olPen);
				pdc->MoveTo(0, intY);
				pdc->LineTo(intFormWidth, intY);
                //MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intB&HEIGHT), RGB(iRed, iGreen, iBlue), BF
                sngBlueCur = sngBlueCur + sngBlueStep;
                if( intY == 0 ) ReturnColor = COLORREF(RGB(iRed, iGreen, iBlue));
				pdc->SelectObject(pOldPen); 
            }
		}
		if( ipMode == 3)
		{
		}
    }
}

void CCSTimeScale::SetLifeStyle(int pTimeScale_ColorIdx, int pTimeScale_Percent, BOOL pTimeScale_StyleUp, int pTimeScale_Type)
{
	lfTimeScale_ColorIdx = pTimeScale_ColorIdx;
	lfTimeScale_Percent  = pTimeScale_Percent;
	lfTimeScale_StyleUp  = pTimeScale_StyleUp;
	lfTimeScale_Type     = pTimeScale_Type;
}
