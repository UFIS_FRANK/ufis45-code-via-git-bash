// UGanttArrowData.h: interface for the CUGanttArrowData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UGANTTARROWDATA_H__378AC985_B614_11D4_BFF6_0001022205E4__INCLUDED_)
#define AFX_UGANTTARROWDATA_H__378AC985_B614_11D4_BFF6_0001022205E4__INCLUDED_

#include <ccsptrarray.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CUGanttArrowData  
{
public:

	int				ArrowHead,	//the style of the head of the arrow
					ArrowTyp;	//0=S2S, 1=S2F, 2=F2S, 3=F2F
	BOOL			IsSelected;	//draw the arrow in a different color if it is selected
	CString			BarSrc,		//draw arrow from this bar
					BarDest;	//draw arrow to this bar
	COLORREF		Color;		//the default-color of the arrow

	CUGanttArrowData();

	virtual ~CUGanttArrowData();

};

#endif // !defined(AFX_UGANTTARROWDATA_H__378AC985_B614_11D4_BFF6_0001022205E4__INCLUDED_)


class CUGanttRect : public CObject
{

private:
    
public:
	CRect	m_rect;

    CUGanttRect()
	{
		m_rect = CRect (0,0,0,0);
	}

    CUGanttRect (CRect rect)
	{
		m_rect = rect;
	}

    CUGanttRect (const CUGanttRect& opO)
	{
		m_rect = opO.m_rect;
	}

    const CUGanttRect& operator=(const CUGanttRect& opO)
    {
        m_rect = opO.m_rect; 
		return *this;
    }
    BOOL operator==(CUGanttRect opO)
    {
        return m_rect == opO.m_rect;
    }
};

