Copyright (c) 1998-9
Dr John Maddock

Permission to use, copy, modify, distribute and sell this software
and its documentation for any purpose is hereby granted without fee,
provided that the above copyright notice appear in all copies and
that both that copyright notice and this permission notice appear
in supporting documentation.  Dr John Maddock makes no representations
about the suitability of this software for any purpose.  
It is provided "as is" without express or implied warranty.

Footnotes:
1) Localised versions may translate the above notice into the native
   language of the version.
2) The above notice applies to source code and documentation, users
   who are distributing only binary forms, need only indicate, somewhere
   in their documentation or in their help|about notice that the product
   uses this library and that the copyright is Dr John Maddock 1998-9

FUER AAT LEUTE. UNBEDINGT BEACHTEN .................
ABB /AAT -> Dem Wortlaut des Copyrights - Vermerks nach, solltet Ihr in
der Dokumentation und im Source - Code folgendes Vermerk hinterlassen:

     "Format checking done using regex++, Copyright Dr. John Maddock
      1998-9 all rights reserved"
....................................................