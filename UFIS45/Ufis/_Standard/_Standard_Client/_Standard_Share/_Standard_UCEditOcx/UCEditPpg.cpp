// UCEditPpg.cpp : Implementation of the CUCEditPropPage property page class.

#include "stdafx.h"
#include "UCEdit.h"
#include "UCEditPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CUCEditPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CUCEditPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CUCEditPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CUCEditPropPage, "UCEDIT.UCEditPropPage.1",
	0x1680f4a3, 0x659a, 0x4e30, 0x96, 0xe3, 0x75, 0xb9, 0xe4, 0x72, 0x46, 0xcb)


/////////////////////////////////////////////////////////////////////////////
// CUCEditPropPage::CUCEditPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CUCEditPropPage

BOOL CUCEditPropPage::CUCEditPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_UCEDIT_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CUCEditPropPage::CUCEditPropPage - Constructor

CUCEditPropPage::CUCEditPropPage() :
	COlePropertyPage(IDD, IDS_UCEDIT_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CUCEditPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT

	SetHelpInfo(_T("Names to appear in the control"), _T("UCEDIT.HLP"), 0);
}


/////////////////////////////////////////////////////////////////////////////
// CUCEditPropPage::DoDataExchange - Moves data between page and properties

void CUCEditPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CUCEditPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CUCEditPropPage message handlers
