VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmSetup 
   Caption         =   "Setup registration check..."
   ClientHeight    =   1875
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8850
   Icon            =   "frmSetup.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1875
   ScaleWidth      =   8850
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnSave 
      Caption         =   "&OK"
      Height          =   375
      Left            =   3878
      TabIndex        =   1
      Top             =   1200
      Width           =   1095
   End
   Begin TABLib.TAB t1 
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8595
      _Version        =   65536
      _ExtentX        =   15161
      _ExtentY        =   1296
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmSetup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btnSave_Click()
    frmHiddenData.tabSetup.ResetContent
    frmHiddenData.tabSetup.InsertBuffer t1.GetBuffer(0, t1.GetLineCount - 1, vbCr), vbCr
    frmHiddenData.tabSetup.RedrawTab
    Unload Me
End Sub

Private Sub Form_Load()
    t1.ResetContent
    t1.HeaderLengthString = "80,80,80,80,150,80"
    t1.HeaderString = "Table,Fields,Unique,Blank,Exclude Char,Min.Len."
    t1.SetColumnBoolProperty 2, "Y", "N"
    t1.SetColumnBoolProperty 3, "Y", "N"
    t1.EnableInlineEdit True
    t1.NoFocusColumns = "2,3"
    t1.InsertBuffer frmHiddenData.tabSetup.GetBuffer(0, frmHiddenData.tabSetup.GetLineCount - 1, vbCr), vbCr
    t1.RedrawTab
End Sub
