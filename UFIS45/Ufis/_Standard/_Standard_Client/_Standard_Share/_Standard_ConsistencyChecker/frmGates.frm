VERSION 5.00
Begin VB.Form frmGates 
   Caption         =   "Edit rule for gate <x>"
   ClientHeight    =   5385
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8445
   Icon            =   "frmGates.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   5385
   ScaleWidth      =   8445
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   8
      Left            =   1980
      TabIndex        =   26
      Top             =   3000
      Width           =   4935
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   7
      Left            =   1980
      TabIndex        =   25
      Top             =   2580
      Width           =   4935
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   6
      Left            =   1980
      TabIndex        =   24
      Top             =   2160
      Width           =   4935
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   5
      Left            =   1980
      TabIndex        =   23
      Top             =   960
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   4
      Left            =   6840
      TabIndex        =   22
      Top             =   960
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   3
      Left            =   6840
      TabIndex        =   21
      Top             =   600
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CommandButton Command1 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   2955
      TabIndex        =   20
      Top             =   4860
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   4275
      TabIndex        =   19
      Top             =   4860
      Width           =   1215
   End
   Begin VB.CheckBox cbG 
      Caption         =   "&Outbaound Gate"
      Height          =   195
      Index           =   1
      Left            =   5100
      TabIndex        =   8
      Top             =   1020
      Width           =   1695
   End
   Begin VB.CheckBox cbG 
      Caption         =   "&Inbound Gate"
      Height          =   195
      Index           =   0
      Left            =   5100
      TabIndex        =   7
      Top             =   660
      Width           =   1635
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   2
      Left            =   1980
      TabIndex        =   4
      Top             =   540
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   1
      Left            =   5100
      TabIndex        =   3
      Top             =   120
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   0
      Left            =   1980
      TabIndex        =   1
      Top             =   120
      Width           =   915
   End
   Begin VB.Label Label21 
      Caption         =   "e.g.: !DLH !US"
      Height          =   195
      Left            =   6960
      TabIndex        =   18
      Top             =   2160
      Width           =   1455
   End
   Begin VB.Label Label22 
      Caption         =   "e.g.: !JFK =AMS"
      Height          =   195
      Left            =   6960
      TabIndex        =   17
      Top             =   2640
      Width           =   1455
   End
   Begin VB.Label Label23 
      Caption         =   "e.g.: !J C"
      Height          =   195
      Left            =   6960
      TabIndex        =   16
      Top             =   3060
      Width           =   1455
   End
   Begin VB.Label Label25 
      Caption         =   "! Exclude this value"
      Height          =   195
      Left            =   1980
      TabIndex        =   15
      Top             =   3720
      Width           =   3255
   End
   Begin VB.Label Label26 
      Caption         =   "= Exclusively used by this value"
      Height          =   195
      Left            =   1980
      TabIndex        =   14
      Top             =   4020
      Width           =   3255
   End
   Begin VB.Label Label27 
      Caption         =   "without prefix: preferred use this value"
      Height          =   195
      Left            =   1980
      TabIndex        =   13
      Top             =   4320
      Width           =   3255
   End
   Begin VB.Label Label18 
      Caption         =   "A/L Parameter:"
      Height          =   195
      Left            =   240
      TabIndex        =   12
      Top             =   2220
      Width           =   1695
   End
   Begin VB.Label Label19 
      Caption         =   "Rig/Dest parameter:"
      Height          =   195
      Left            =   240
      TabIndex        =   11
      Top             =   2640
      Width           =   1695
   End
   Begin VB.Label Label20 
      Caption         =   "Nature code parameter:"
      Height          =   195
      Left            =   240
      TabIndex        =   10
      Top             =   3060
      Width           =   1695
   End
   Begin VB.Label Label24 
      Caption         =   "Format description:"
      Height          =   195
      Left            =   240
      TabIndex        =   9
      Top             =   3720
      Width           =   1635
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00000000&
      BorderWidth     =   2
      X1              =   120
      X2              =   8280
      Y1              =   1860
      Y2              =   1860
   End
   Begin VB.Label Label4 
      Caption         =   "Multiple Flights:"
      Height          =   195
      Left            =   240
      TabIndex        =   6
      Top             =   1020
      Width           =   1335
   End
   Begin VB.Label Label3 
      Caption         =   "Priority:"
      Height          =   195
      Left            =   240
      TabIndex        =   5
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Max. PAX:"
      Height          =   195
      Left            =   3840
      TabIndex        =   2
      Top             =   180
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Flight Id:"
      Height          =   195
      Left            =   240
      TabIndex        =   0
      Top             =   180
      Width           =   975
   End
End
Attribute VB_Name = "frmGates"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Fieldlist As String
Public Datalist As String
Public Fieldlens As String
Public Typelist As String
Public Tabindex As Long
Public CaptionPart As String
Public WhichRules As String

Private Sub cbG_Click(Index As Integer)
    If cbG(Index).Value = 1 Then
        txtValue(Index + 3).Text = "X"
    Else
        txtValue(Index + 3).Text = ""
    End If
End Sub

Private Sub Command1_Click()
    Dim strResult As String
    Dim strValue As String
    Dim cnt As Integer
    Dim strErr As String
    Dim strVbCrLf As String
    Dim ilFieldCount As Integer
    Dim strLineNo As String
    Dim llLineNo As Long
    Dim i As Integer
    Dim strUrno As String
'===============================
' 1. Collect the possible errors
    strResult = strResult + CheckAirlinesList(MakeItemList(txtValue(6))) + vbCrLf
    strResult = strResult + CheckAirportList(MakeItemList(txtValue(7))) + vbCrLf
    strResult = strResult + CheckNatureList(MakeItemList(txtValue(8))) + vbCrLf
    strVbCrLf = vbCrLf + vbCrLf + vbCrLf
    If strResult <> strVbCrLf Then
        strErr = vbCrLf + "Following irregularities occured:" + vbCrLf
        strErr = strErr + "========================" + vbCrLf + strResult
        MsgBox strErr
    End If
'========================================================
' 2. Collect data and refresh them in the gate rules grid
' 0   1  2   3  4    5   6  7 8 9 10 11        12          13
'URNO,#,Gate,T,R.Blt,ID,Max,M,I,O,P,Nat,A/L Parameter,Dest. Parameter
    strUrno = GetRealItem(Datalist, 0, ",")
    strLineNo = frmMain.tabGates.GetLinesByColumnValue(0, GetRealItem(Datalist, 0, ","), 1)
    llLineNo = CLng(strLineNo)
    strValue = strValue + txtValue(0) + ";"
    frmMain.tabGates.SetColumnValue llLineNo, 5, txtValue(0)
    strValue = strValue + txtValue(1) + ";"
    frmMain.tabGates.SetColumnValue llLineNo, 6, txtValue(1)
    strValue = strValue + txtValue(2) + ";"
    frmMain.tabGates.SetColumnValue llLineNo, 7, txtValue(2)
    strValue = strValue + txtValue(3) + ";"
    frmMain.tabGates.SetColumnValue llLineNo, 8, txtValue(3)
    strValue = strValue + txtValue(4) + ";"
    frmMain.tabGates.SetColumnValue llLineNo, 9, txtValue(4)
    strValue = strValue + txtValue(5) + ";"
    frmMain.tabGates.SetColumnValue llLineNo, 10, txtValue(5)
    strValue = strValue + txtValue(8) + ";"
    frmMain.tabGates.SetColumnValue llLineNo, 11, txtValue(8)
    strValue = strValue + txtValue(6) + ";"
    frmMain.tabGates.SetColumnValue llLineNo, 12, txtValue(6)
    strValue = strValue + txtValue(7) + ";"
    frmMain.tabGates.SetColumnValue llLineNo, 13, txtValue(7)
    
    frmMain.tabGates.RedrawTab
'========================================================
' 3. Update GATTAB grid in hidden data and update DB
    strLineNo = frmHiddenData.tabData(2).GetLinesByColumnValue(0, GetRealItem(Datalist, 0, ","), 1)
    llLineNo = CLng(strLineNo)
    frmHiddenData.tabData(2).SetColumnValue llLineNo, 21, strValue
    frmHiddenData.tabData(2).RedrawTab
    frmHiddenData.Ufis.CallServer "URT", "GATTAB", "GATR", strValue, "WHERE URNO=" + strUrno, "120"
    Unload Me
End Sub

Private Sub Command2_Click()
    Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
        Unload Me
    End If
End Sub

Private Sub Form_Load()
    PrepareValues
End Sub
'================================================
' Loops through the field and datalist and
' sets the values into the edit fields
'================================================
Public Sub PrepareValues()
    Dim ilFieldCount As Integer
    Dim strCurrValue As String
    Dim idx As Integer
    Dim i As Integer
    If Fieldlist = "" Then
        Exit Sub
    End If
    ilFieldCount = ItemCount(Fieldlist, ",")
    For i = 0 To ilFieldCount - 2
        txtValue(i) = GetRealItem(Datalist, i + 1, ",") ' i+1 because urno does not interest
    Next i
    Me.Caption = "Edit rule for position " + CaptionPart
    If txtValue(3).Text = "X" Then
        cbG(0).Value = 1
    Else
        cbG(0).Value = 0
    End If
    If txtValue(4).Text = "X" Then
        cbG(1).Value = 1
    Else
        cbG(1).Value = 0
    End If
End Sub


Private Sub txtValue_GotFocus(Index As Integer)
    txtValue(Index).selstart = 0
    txtValue(Index).SelLength = Len(txtValue(Index).Text)
End Sub

Private Sub txtValue_Change(Index As Integer)
    Dim Fieldlen As Integer
    Dim strVal As String

    Fieldlen = CInt(GetRealItem(Fieldlens, Index + 1, ","))
    strVal = txtValue(Index).Text
    If Len(strVal) >= Fieldlen Then
        txtValue(Index).Text = left(txtValue(Index).Text, Fieldlen)
        txtValue(Index).selstart = Fieldlen
    End If
        
End Sub
Private Sub txtValue_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim Fieldlen As Integer
    Dim strVal As String
    Dim FieldType As String
    Dim sellen As Integer
    Dim selstart As Integer
    Dim strBefore As String
    Dim strAfter As String

    Fieldlen = CInt(GetRealItem(Fieldlens, Index + 1, ","))
    FieldType = GetRealItem(Typelist, Index + 1, ",")
    strVal = txtValue(Index).Text
    sellen = txtValue(Index).SelLength
    selstart = txtValue(Index).selstart
    If KeyAscii = vbKeyBack Then
        Exit Sub
    End If
    Select Case FieldType
    Case "long"
        Select Case KeyAscii
            Case 48 To 57
            Case Else
            KeyAscii = 0
        End Select
    Case "double"
        Select Case KeyAscii
            Case 48 To 57
            Case 46
            Case 44
                KeyAscii = 46
            Case Else
                KeyAscii = 0
        End Select
    Case "string"
        'Everything allowed
    End Select
    If Len(strVal) = Fieldlen And sellen = 0 Then
        If KeyAscii <> vbKeyBack Then  'Backspace
            KeyAscii = 0
        End If
    End If
End Sub

