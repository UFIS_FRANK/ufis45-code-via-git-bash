VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmCheckResults 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Results of check ..."
   ClientHeight    =   6855
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7095
   Icon            =   "frmCheckResults.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6855
   ScaleWidth      =   7095
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin TABLib.TAB tabRes 
      Height          =   6795
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7035
      _Version        =   65536
      _ExtentX        =   12409
      _ExtentY        =   11986
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmCheckResults"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public WhichData As String
Private Sub Form_Load()
    tabRes.ResetContent
    tabRes.HeaderString = "Line,Remark"
    tabRes.HeaderLengthString = "100,500"
End Sub

Public Sub SetTabData(data As String)
    tabRes.ResetContent
    tabRes.HeaderString = "Line,Remark"
    tabRes.HeaderLengthString = "50,700"
    tabRes.InsertBuffer data, vbCrLf
    tabRes.RedrawTab
End Sub


Private Sub tabRes_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    Dim strVal As Long
    If Key = vbKeyReturn Then
        strVal = CLng(tabRes.GetColumnValue(LineNo, 0))
        If WhichData = "POS" Then
            frmMain.tabPositions.SetCurrentSelection strVal - 1
            frmMain.tabPositions.OnVScrollTo strVal - 1
            frmMain.ShowPositionDilalogForLine strVal - 1, 0
            tabRes.SetFocus
        End If
        If WhichData = "GAT" Then
            frmMain.tabGates.SetCurrentSelection strVal - 1
            frmMain.tabGates.OnVScrollTo strVal - 1
            frmMain.ShowGateDilalogForLine strVal - 1, 0
            tabRes.SetFocus
        End If
    End If
End Sub

Private Sub tabRes_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim strVal As Long
    
    strVal = CLng(tabRes.GetColumnValue(LineNo, 0))
    If WhichData = "POS" Then
        frmMain.tabPositions.SetCurrentSelection strVal - 1
        frmMain.tabPositions.OnVScrollTo strVal - 1
        tabRes.SetFocus
    End If
    If WhichData = "GAT" Then
        frmMain.tabGates.SetCurrentSelection strVal - 1
        frmMain.tabGates.OnVScrollTo strVal - 1
        tabRes.SetFocus
    End If

End Sub

Private Sub tabRes_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strVal As Long
    
    strVal = CLng(tabRes.GetColumnValue(LineNo, 0))
    If WhichData = "POS" Then
        frmMain.tabPositions.SetCurrentSelection strVal - 1
        frmMain.tabPositions.OnVScrollTo strVal - 1
        tabRes.SetFocus
    End If
    If WhichData = "GAT" Then
        frmMain.tabGates.SetCurrentSelection strVal - 1
        frmMain.tabGates.OnVScrollTo strVal - 1
        tabRes.SetFocus
    End If
End Sub

Private Sub tabRes_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strVal As Long
    
    strVal = CLng(tabRes.GetColumnValue(LineNo, 0))
    If WhichData = "POS" Then
        frmMain.tabPositions.SetCurrentSelection strVal - 1
        frmMain.tabPositions.OnVScrollTo strVal - 1
        frmMain.ShowPositionDilalogForLine strVal - 1, 0
    End If
    If WhichData = "GAT" Then
        frmMain.tabGates.SetCurrentSelection strVal - 1
        frmMain.tabGates.OnVScrollTo strVal - 1
        frmMain.ShowGateDilalogForLine strVal - 1, 0
    End If

End Sub
