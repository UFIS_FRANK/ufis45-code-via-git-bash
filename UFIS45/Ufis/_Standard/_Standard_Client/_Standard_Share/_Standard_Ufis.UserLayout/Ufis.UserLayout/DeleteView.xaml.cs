﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Bars;
using Ufis.Utilities;

namespace Ufis.UserLayout
{
    /// <summary>
    /// Interaction logic for DeleteView.xaml
    /// </summary>
    public partial class DeleteView : UserControl
    {
        public DeleteView()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            ClosePopup();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button != null)
            {
                ICommand command = button.Tag as ICommand;
                if (command != null)
                    command.Execute(button.CommandParameter);

                ClosePopup();
            }
        }

        private void ClosePopup()
        {
            cboViewName.EditValue = null;
            DxPopupHelper.ClosePopup(this);
        }
    }
}
