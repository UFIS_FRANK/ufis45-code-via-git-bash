﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Broadcast;
using Ufis.Entities;
using Ufis.Broadcast.Ceda;

namespace Ufis.Data.Ceda
{
    /// <summary>
    /// Represents a class for ceda specific entity database.
    /// </summary>
    public class CedaEntityDataContext : EntityDataContextBase
    {
        private CedaDataReader _cedaDataReader;
        private CedaDataWriter _cedaDataWriter;

        /// <summary>
        /// Gets the table name from broadcast message.
        /// </summary>
        /// <param name="bcMessage">The broadcast message.</param>
        /// <returns></returns>
        protected override string GetTableNameFromBroadcast(BroadcastMessageBase bcMessage)
        {
            CedaBroadcastMessage message = (CedaBroadcastMessage)bcMessage;

            return message.Object;
        }

        /// <summary>
        /// Executes the command that will not return data.
        /// </summary>
        /// <param name="dataCommand">Data command to execute.</param>
        public override void ExecuteNonQueryCommand(DataCommand dataCommand)
        {
            if (Connection == null)
            {
                throw new ArgumentNullException(null, "Connection for CedaEntityDataContext cannot be null");
            }

            CedaConnection connection = (CedaConnection)Connection;
            if (_cedaDataWriter == null)
                _cedaDataWriter = new CedaDataWriter(connection, dataCommand);
            else
                _cedaDataWriter.Command = dataCommand;
            try
            {
                _cedaDataWriter.Execute();
            }
            catch (DataException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Executes the command that will return the data row collection object.
        /// </summary>
        /// <param name="dataCommand">Data command to execute.</param>
        /// <returns></returns>
        public override DataRowCollection ExecuteQueryCommand(DataCommand dataCommand)
        {
            DataRowCollection rows = null;

            if (Connection == null)
            {
                throw new ArgumentNullException(null, "Connection for CedaEntityDataContext cannot be null");
            }

            string[] arrSelectCommands = new string[] { "RT", "RTA" };
            bool bIsCedaReaderCommand = (Array.IndexOf(arrSelectCommands, dataCommand.CommandText.ToUpper()) >= 0);

            CedaConnection connection = (CedaConnection)Connection;
            if (connection.UseCedaReader && bIsCedaReaderCommand)
            {
                if (_cedaDataReader == null)
                    _cedaDataReader = new CedaDataReader(connection, dataCommand);
                else
                    _cedaDataReader.SelectCommand = dataCommand;
                try
                {
                    rows = _cedaDataReader.Execute();
                }
                catch (DataException ex)
                {
                    throw ex;
                }
            }
            else
            {
                if (_cedaDataWriter == null)
                    _cedaDataWriter = new CedaDataWriter(connection, dataCommand);
                else
                    _cedaDataWriter.Command = dataCommand;
                try
                {
                    rows = _cedaDataWriter.ExecuteQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return rows;
        }

        /// <summary>
        /// Creates entity collection object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selectCommand">Select command to retrieve data from data source.</param>
        /// <returns></returns>
        protected override EntityCollectionBase<T> CreateEntityCollection<T>(DataCommand selectCommand)
        {
            EntityCollectionBase<T> entityCollection = null;            

            Type type = typeof(T);
            if (!typeof(Entity).IsAssignableFrom(type))
            {
                throw new ArgumentException("Type provided is not Entity type.");
            }

            try
            {
                entityCollection = EntityAdapter.CreateEntityCollection<T>(selectCommand);
            }
            catch (DataException ex)
            {
                throw ex;
            }

            return entityCollection;
        }

        /// <summary>
        /// Save the collection of entity to the data source.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entityCollection">The entity collection.</param>
        public override void SaveEntityCollection<T>(EntityCollectionBase<T> entityCollection)
        {
            EntityAdapter.Update<T>(entityCollection);
        }

        /// <summary>
        /// Save an entity to the data source.
        /// </summary>
        /// <param name="entity">The entity object to save.</param>
        /// <param name="columnList">The list of columns to save.</param>
        public override void SaveEntity(Entity entity, string columnList)
        {
            EntityAdapter.Update(entity, columnList);
        }

        /// <summary>
        /// Get System.TimeZoneInfo class from specific airport.
        /// </summary>
        /// <param name="airportCode">The airport code.</param>
        /// <returns></returns>
        public override TimeZoneInfo GetTimeZoneInfo(string airportCode)
        {
            //TODO: Get TimeZoneInfo for specific airport.
            //Currently it will ignore the parameter and always take TimeZoneInfo of home airport.
            int intMinutes = GetUtcOffset();
            TimeSpan utcToLocalTimeSpan = new TimeSpan(0, intMinutes, 0);
            TimeZoneInfo timeZoneInfo = TimeZoneInfo.CreateCustomTimeZone(airportCode, utcToLocalTimeSpan,
                airportCode + " Time", airportCode + " Standard Time");
            return timeZoneInfo;
        }

        private int GetUtcOffset()
        {
            int intUtcOffset = 0;

            DataRowCollection rows = GetServerConfigs();
            for (int i = 0; i < rows.Count - 1; i++)
            {
                DataRow row = rows[i];
                if (row[0] == "UTCD")
                {
                    string strUtcOffset = row[1];
                    if (!int.TryParse(strUtcOffset, out intUtcOffset))
                        intUtcOffset = 0;
                    break;
                }
            }

            return intUtcOffset;
        }

        private DataRowCollection GetServerConfigs()
        {
            DataCommand command = new CedaSqlCommand()
            {
                CommandText = "GFR",
                WhereClause = "[CONFIG]"
            };

            return ExecuteQueryCommand(command);
        }

        /// <summary>
        /// Initializes a new instance of Ufis.Data.Ceda.CedaEntityDataContext class.
        /// </summary>
        public CedaEntityDataContext()
        {
            EntityAdapter = new CedaEntityAdapter(this);
        }

        /// <summary>
        /// Initializes a new instance of Ufis.Data.Ceda.CedaEntityDataContext class.
        /// </summary>
        /// <param name="connection">The connection used for this data context.</param>
        public CedaEntityDataContext(ConnectionBase connection)
            : base(connection)
        {
            EntityAdapter = new CedaEntityAdapter(this);
        }

        /// <summary>
        /// Initializes a new instance of Ufis.Data.Ceda.CedaEntityDataContext class.
        /// </summary>
        /// <param name="connection">The connection used for this data context.</param>
        /// <param name="ufisBroadcast">The brodcast manager.</param>
        public CedaEntityDataContext(ConnectionBase connection, IUfisBroadcast ufisBroadcast)
            : base(connection, ufisBroadcast)
        {
            EntityAdapter = new CedaEntityAdapter(this);
        }
    
    }
}
