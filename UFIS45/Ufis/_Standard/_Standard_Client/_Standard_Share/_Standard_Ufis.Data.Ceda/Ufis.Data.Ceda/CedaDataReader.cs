﻿#define UseEncoder

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Ufis.Data.Ceda
{
    /// <summary>
    /// Provides a way of reading data from CEDA.
    /// <para>
    /// This class provides the functionality to access the CDRHDL CEDA process for
    /// data requests. The CDRHDL fetches the data from oracle and sends it directly 
    /// to this class. The advantage is to get the data very fast. After the data has
    /// been received the data strean is extracted into the single sections and the 
    /// application is able to store all data.
    /// </para>
    /// </summary>
    public class CedaDataReader
    {
        //********************************
		// internally used members
		//********************************
		private string _strErrorText;
		private	StringBuilder _sbDataBuffer;
		private long _lCurrentPacketSize;
        private bool _bError;
        private DataRowCollection _rows;
        private Socket _socket;
        
        /// <summary>
        /// Gets or sets the Ufis.Data.DataCommand used by this instance of 
        /// Ufis.Data.Ceda.CedaDataReader
        /// </summary>
        public DataCommand SelectCommand { get; set; }

        /// <summary>
        /// Gets or sets the Ufis.Data.Ceda.CedaConnection used by this instance of 
        /// Ufis.Data.Ceda.CedaDataReader
        /// </summary>
        public CedaConnection Connection { get; set; }

        /// <summary>
        /// Gets the error text as a string. For the case of no error has occured, 
        /// an empty string will be returned.
        /// </summary>
        /// <returns>The error string.</returns>
        public string LastErrorMessage
        {
            get { return this._strErrorText; }
        }

        /// <summary>
        /// Gets the Ufis.Data.Ceda.CedaDataRowCollection fetched from the server.
        /// </summary>
        public DataRowCollection Rows
        {
            get { return _rows; }
        }

        /// <summary>
        /// Executes the command against CEDA server and returns Ufis.Data.Ceda.CedaDataRowCollection object fetched from CEDA server.
        /// </summary>
		public DataRowCollection Execute()
		{
            DataRowCollection rows = null;

            ClearDataBuffer();
			this._bError = false;

            try
            {
                ConnectToCeda();

                try
                {
                    this.SendData();
                    this.ReceiveData();
                }
                catch (DataException ex)
                {
                    throw ex;
                }
                finally
                {
                    DisconnectFromCeda();
                }

                rows = this._rows;
            }
            catch (DataException ex)
            {
                throw ex;
            }

            return rows;
		}

		/// <summary>
		/// Sends the request with all parameters to the CDRHDL.
		/// </summary>
		/// <returns></returns>
		/// <remarks>
		/// The parameters which must have been set befor the function can work properly are:
		/// <list type="bullet">
		///		<item><see cref="omCommand"/></item>
		///		<item><see cref="omIdentifier"/></item>
		///		<item><see cref="omTable"/></item>
		///		<item><see cref="omTabext"/></item>
		///		<item><see cref="omHopo"/></item>
		///		<item><see cref="omFields"/></item>
		///		<item><see cref="omWhere"/></item>
		///		<item><see cref="omUser"/></item>
		///		<item><see cref="omWks"/></item>
		///		<item><see cref="omSepa"/></item>
		///		<item><see cref="omAppl"/></item>
		///		<item><see cref="imReceiveTimeout"/></item>
		///		<item><see cref="lmPacketSize"/></item>
		/// </list>
		/// </remarks>
		private void SendData()
		{
			string strCommStr;
			string strTmpStr = "";
			string strTotPart;
			string strT2;
			long lBytes = 0;

			this._bError = false;

			strTotPart = "{=TOT=}";

			strTmpStr += "{=CMD=}" + this.SelectCommand.CommandText;
			strTmpStr += "{=IDN=}" + this.Connection.Identifier;
			strTmpStr += "{=TBL=}" + this.SelectCommand.Parameters[0];
			strTmpStr += "{=EXT=}" + this.Connection.TableExtension;
			strTmpStr += "{=HOPO=}" + this.Connection.HomeAirport;
			strTmpStr += "{=FLD=}" + this.SelectCommand.Parameters[1];
			strTmpStr += "{=WHE=}" + this.SelectCommand.Parameters[3];
			strTmpStr += "{=USR=}" + this.Connection.UserName;
			strTmpStr += "{=WKS=}" + this.Connection.WorkstationName;
			strTmpStr += "{=SEPA=}" + this.Connection.RowSeparator;
			strTmpStr += "{=APP=}" + this.Connection.ApplicationName;

			// use packaging ?
			if (this.Connection.PacketSize > 0)
			{
				strTmpStr += "{=PACK=}" + this.Connection.PacketSize.ToString();
			}

			// simulate error ?
			if (this.Connection.SimulateError.Length > 0)
			{
				strTmpStr += "{=SIMERR=}" + this.Connection.SimulateError;
			}


			lBytes = strTmpStr.Length + 16;
			strT2 = lBytes.ToString("D9");
			strCommStr = strTotPart + strT2 + strTmpStr;
			lBytes = strCommStr.Length;

            Encoding olAE = Encoding.GetEncoding(this.Connection.WindowsCodePage);
			string strMsg;
			int iSend;
			try
			{
				byte[] olBuffer = new byte[strCommStr.Length];
				olAE.GetBytes(strCommStr,0,strCommStr.Length,olBuffer,0);

				iSend = _socket.Send(olBuffer,olBuffer.Length, SocketFlags.None);
			}
			catch(System.Exception e)
			{
				strMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
				System.Diagnostics.Debug.WriteLine(strMsg);

                throw new DataException(strMsg, e);
			}			
		}

		/// <summary>
		/// This method receives the data from the CDRHDL. The data, which is expected depends
		/// on the settings of the public members.
		/// </summary>
		/// <returns>true=success, false=failed. Please refer to <see cref="GetError()"/>.</returns>
		/// <remarks>
		/// The parameters which must have been set befor the function can work properly are:
		/// <list type="bullet">
		///		<item><see cref="omCommand"/></item>
		///		<item><see cref="omIdentifier"/></item>
		///		<item><see cref="omTable"/></item>
		///		<item><see cref="omTabext"/></item>
		///		<item><see cref="omHopo"/></item>
		///		<item><see cref="omFields"/></item>
		///		<item><see cref="omWhere"/></item>
		///		<item><see cref="omUser"/></item>
		///		<item><see cref="omWks"/></item>
		///		<item><see cref="omSepa"/></item>
		///		<item><see cref="omAppl"/></item>
		///		<item><see cref="imReceiveTimeout"/></item>
		///		<item><see cref="lmPacketSize"/></item>
		/// </list>
		/// </remarks>
		private void ReceiveData()
		{
			bool bOK		   = true;	
			bool bCompleted   = false;
			bool bFirstPacket = true;

			string strMsg;

            if (_socket == null) return;

			while(bOK && !bCompleted)
			{
				bOK = ReceivePacketData(bFirstPacket, ref bCompleted);
				bFirstPacket = false;
			}

			if (bOK)
			{
				if (this._sbDataBuffer.Length > 0)
				{
					ExtractTextLineFast(this._rows, this._sbDataBuffer.ToString(), 
                        this.Connection.RowSeparator);
				}

				try
				{
					Encoding olAE = Encoding.GetEncoding(this.Connection.WindowsCodePage);
					string strAnswer = "{=ACK=}ACK";
					byte[] arrBuffers = new byte[strAnswer.Length];
					olAE.GetBytes(strAnswer, 0, strAnswer.Length, arrBuffers, 0);
					_socket.Send(arrBuffers, arrBuffers.Length, SocketFlags.None);
				}
				catch(System.Exception e)
				{
					strMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
                    System.Diagnostics.Debug.WriteLine(strMsg);

                    throw new DataException(strMsg, e);
				}
			}
		}

		bool ReceivePacketData(bool firstPacket,ref bool completed)
		{
			string strMsg;

			byte[]	arrBuf = new byte[25001];
			string	strRetFirst;
			string	strKeyWord;
			string  strResult		= "";
			int		iDataBegin;
			byte[]  arrTotalDataBuffer = null;

			int  iBytes;
			int	 iSize;
			int  iTransferBytes = 0;
			int	 iReceive = 0;

			Encoding olAE = Encoding.GetEncoding(this.Connection.WindowsCodePage);
            
            try
			{
				byte[] arrBuffers = new byte[100];
				iReceive = _socket.Receive(arrBuffers, 16, SocketFlags.None);
				strRetFirst = olAE.GetString(arrBuffers,0,16);
			}
			catch(System.Exception e)
			{
				strMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
                System.Diagnostics.Debug.WriteLine(strMsg);

                throw new DataException(strMsg, e);
			}

			strKeyWord   = "{=TOT=}"; 
			iDataBegin = GetKeyItem(out strResult,out iSize, strRetFirst, strKeyWord, "{=", true); 
			if (iDataBegin == -1)
				return false;

			int iTotal = int.Parse(strResult);
			iTotal -= 16;
			if (iTotal < 0) //take care that it is not negative
			{
				iTotal = 0;
			}

			if (arrTotalDataBuffer == null)
			{
				arrTotalDataBuffer = new byte[iTotal+1];
			}

			try
			{
				iBytes = _socket.Receive(arrTotalDataBuffer, iTotal,  SocketFlags.None);
			}
			catch(System.Exception e)
			{
				strMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
                System.Diagnostics.Debug.WriteLine(strMsg);

                throw new DataException(strMsg, e);
			}

			iTransferBytes += iBytes;

			while (iTransferBytes < iTotal)
			{
				try
				{
					iBytes = _socket.Receive(arrTotalDataBuffer,iTransferBytes, (iTotal-iTransferBytes), SocketFlags.None);
					iTransferBytes += iBytes;
				}
				catch(System.Exception e)
				{
					strMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
                    System.Diagnostics.Debug.WriteLine(strMsg);

                    throw new DataException(strMsg, e);
				}
			}

			//----------------------------
			// Extract the incomming data
#if	UseEncoder
			// we can't use UTF8Encoder class because this class tries to convert 2 bytes to a single unicode character
			// if there are invalid byte combinations, the UTF8Encoder will ignore this bytes (see UnicodeCategory)
			string strTotalDataBuffer = olAE.GetString(arrTotalDataBuffer,0,iTransferBytes);
#else
			string strTotalDataBuffer = string.Empty;
			StringBuilder sbBuilder = new StringBuilder(iTransferBytes);
			byte[]	arrPair = new byte[2]; 
			arrPair[1] = 0;
			for (int i = 0; i < iTransferBytes; i++)
			{
				arrPair[0] = arrTotalDataBuffer[i];
				sbBuilder.Append(BitConverter.ToChar(arrPair,0));
			}
			strTotalDataBuffer = sbBuilder.ToString();
#endif
			strKeyWord = "{=CMD=}"; 
			iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", true); 
			if (iDataBegin != -1) 
			{ 
				//omCommand = pclResult;
			} 

			strKeyWord = "{=IDN=}"; 
			iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", true); 
			if (iDataBegin != -1) 
			{ 
				//omIdentifier = pclResult;
			} 

			strKeyWord = "{=TBL=}"; 
			iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", true); 
			if (iDataBegin != -1) 
			{ 
				//omTable = pclResult;
			} 

			strKeyWord = "{=EXT=}"; 
			iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", true); 
			if (iDataBegin != -1) 
			{ 
				//omTabext = pclResult;
			} 

			strKeyWord = "{=HOPO=}"; 
			iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", true); 
			if (iDataBegin != -1) 
			{ 
				//omHopo = pclResult;
			} 

			strKeyWord = "{=FLD=}"; 
			iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", true); 
			if (iDataBegin != -1) 
			{ 
				//omFields = pclResult;
			} 

			strKeyWord = "{=WHE=}"; 
			iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", true); 
			if (iDataBegin != -1) 
			{ 
				//omWhere = pclResult;
			} 

			strKeyWord = "{=USR=}"; 
			iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", true); 
			if (iDataBegin != -1) 
			{ 
				//omUser = pclResult;
			} 

			strKeyWord = "{=WKS=}"; 
			iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", true); 
			if (iDataBegin != -1) 
			{ 
				//omWks  = pclResult;
			} 

			strKeyWord = "{=APP=}"; 
			iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", true); 
			if (iDataBegin != -1) 
			{ 
				//omAppl  = pclResult;
			} 

			strKeyWord = "{=SEPA=}"; 
			iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", true); 
			if (iDataBegin != -1) 
			{ 
				//omSepa  = pclResult;
			} 

			strKeyWord = "{=ERR=}"; 
			iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", true); 
			if (iDataBegin != -1) 
			{ 
				_strErrorText = strResult;
				this._bError = false;
				if (_strErrorText.Length > 0)
				{
                    this._bError = true;

                    throw new DataException("Error returned from Ceda server: " +
                        _strErrorText);
				}
			} 

			strKeyWord = "{=PACK=}"; 
			iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", true); 
			if (iDataBegin != -1) 
			{ 
				this._lCurrentPacketSize = long.Parse(strResult);
			}
			else
			{
				this._lCurrentPacketSize = 0;
			}

			int iDataLen = 0;
			if (_bError == false)
			{
				strKeyWord = "{=DAT=}"; 
				iDataBegin = GetKeyItem(out strResult, out iSize, strTotalDataBuffer, strKeyWord, "{=", false); 
				if (iDataBegin != -1) 
				{ 
					if (firstPacket)
					{
						ClearDataBuffer();
						iDataLen = strTotalDataBuffer.Length - iDataBegin;
						if (iDataLen > 0)
						{
							this._sbDataBuffer.Append(strTotalDataBuffer.Substring(iDataBegin));
							//this.omAppl		  = pclResult;
						}
					}
					else
					{
						iDataLen = strTotalDataBuffer.Length - iDataBegin;
						if (iDataLen > 0)
						{
							this._sbDataBuffer.Append(this.Connection.RowSeparator);
							this._sbDataBuffer.Append(strTotalDataBuffer.Substring(iDataBegin));
						}
					}

					if (this.Connection.PacketSize <= 0)
					{
						completed = true;
					}
                    else if (_lCurrentPacketSize < this.Connection.PacketSize)
					{
						completed = true;
					}
					else
					{
						completed = false;
					}

				}
				else
				{
                    if (this.Connection.PacketSize <= 0)
					{
						completed = true;
					}
                    else if (_lCurrentPacketSize < this.Connection.PacketSize)
					{
						completed = true;
					}
					else
					{
						completed = false;
					}
				}
			}
			else
			{
				ClearDataBuffer();
			}

			return this._bError == false;
		}

		//frees all allocated memory
		void ClearDataBuffer()
		{
			this._sbDataBuffer.Length = 0;
			this._rows.Clear();
		}

		private int ExtractTextLineFast(DataRowCollection items, string lineText, string sepa)
		{
			items.AddRows(lineText, sepa.ToCharArray());
			return items.Count;
		}

        /// <summary>
        /// Connects to the ceda based on connection object
        /// </summary>
        private void ConnectToCeda()
        {
            DataErrorCollection errors = new DataErrorCollection();
            DataError error;
            bool bConnected = false;

            CedaConnection connection = this.Connection;
            if (connection.State == ConnectionState.Open)
            {
                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                error = ConnectToCedaServer(_socket, connection.ConnectedServer, connection.ReaderPort);
                if (error == null)
                    bConnected = true;
                else
                    errors.Add(error);
            }

            if (!bConnected)
            {
                string[] arrServers = this.Connection.ServerList.Split(',');
                foreach (string strServer in arrServers)
                {
                    if (connection.State == ConnectionState.Open && connection.ConnectedServer == strServer)
                    {
                        //do nothing
                    }
                    else
                    {
                        _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                        error = ConnectToCedaServer(_socket, strServer, connection.ReaderPort);
                        if (error == null)
                            bConnected = true;
                        else
                            errors.Add(error);
                    }

                    if (bConnected) break;
                }
            }

            if (!bConnected) throw new DataException(errors);
        }

        /// <summary>
        /// Connects to the ceda based on connection object
        /// </summary>
        /// <param name="socket">The System.Net.Sockets.Socket object to perform the connection.</param>
        /// <param name="serverName">The name of server to connect to.</param>
        /// <param name="port">The port number to connect to.</param>
        /// <returns>Ufis.Data.Ceda.CedaError object if connection failed or null if succeeded</returns>
        private DataError ConnectToCedaServer(Socket socket, string serverName, int port)
        {
            DataError error = null;

            try
            {
                IPHostEntry ipHostEntry = Dns.GetHostEntry(serverName);
                IPAddress ipAddress = ipHostEntry.AddressList[0];
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, port);
                socket.Connect(ipEndPoint);
            }
            catch (Exception ex)
            {
                error = new DataError("Unable to connect to server " + serverName, ex);
            }

            return error;
        }

        /// <summary>
        /// Disconnects from the ceda server
        /// </summary>
        private void DisconnectFromCeda()
        {
            if (this._socket != null)
            {
                this._socket.Shutdown(SocketShutdown.Both);
                this._socket.Close();
                this._socket = null;
            }
        }

		//This method is responsible to extract the values behind a keyword
		// in the syntax "{=CMD=}" or "{=HOP=}"
		private int GetKeyItem(out string pcpResultBuff,out int plpResultSize,string pcpTextBuff, string pcpKeyWord, string pcpItemEnd, bool bpCopyData)
		{
			int iDataSize  = 0; 
			int iDataBegin = -1; 
			int iDataEnd   = -1; 
			pcpResultBuff   = "";

			iDataBegin = pcpTextBuff.IndexOf(pcpKeyWord); 

			/* Search the keyword */ 
			if (iDataBegin >= 0) 
			{ 
				/* Did we find it? Yes. */ 
				iDataBegin += pcpKeyWord.Length; 
				/* Skip behind the keyword */ 
				iDataEnd = pcpTextBuff.IndexOf(pcpItemEnd,iDataBegin); 
				/* Search end of data */ 
				if (iDataEnd < 0) 
				{ 
					/* End not found? */ 
					iDataEnd = pcpTextBuff.Length; 
					/* Take the whole string */ 
				} /* end if */ 
				iDataSize = iDataEnd - iDataBegin; 
				/* Now calculate the length */ 
				if (bpCopyData == true) 
				{ 
					/* Shall we copy? */ 
					pcpResultBuff = pcpTextBuff.Substring(iDataBegin,iDataSize); 
					/* Yes, strip out the data */ 
				} /* end if */ 
			} /* end if */ 
			if (bpCopyData == true) 
			{ 
				/* Allowed to set EOS? */ 
				//				pcpResultBuff[llDataSize] = 0x00; 
				/* Yes, terminate string */ 
			} /* end if */ 

			/* Pass the length back */ 
			plpResultSize = iDataSize; 

			/* Return the data's begin */ 
			return iDataBegin; 
		}

		/// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaDataReader class.
		/// </summary>
		public CedaDataReader() : this(null, null) { }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaDataReader class.
        /// </summary>
        /// <param name="connection">The Ufis.Data.Ceda.CedaConnection used by this instance of 
        /// Ufis.Data.Ceda.CedaDataReader</param>
        /// <param name="selectCommand">The Ufis.Data.DataCommand used by this instance of 
        /// Ufis.Data.Ceda.CedaDataReader</param>
        public CedaDataReader(CedaConnection connection, DataCommand selectCommand)
		{
            this.Connection = connection;
            this.SelectCommand = selectCommand;

			//default values
			_sbDataBuffer  = new StringBuilder();
			_rows = new DataRowCollection();
		}
    }
}
