﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Broadcast;
using Ufis.Broadcast.Ceda;
using Ufis.Entities;
using System.Reflection;

namespace Ufis.Data.Ceda
{
    /// <summary>
    /// Represents a class of ceda specific observable collection of entities.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CedaEntityCollection<T> : EntityCollectionBase<T>
    {
        /// <summary>
        /// Handle the broadcast.
        /// </summary>
        /// <param name="e">Broadcast event arguments.</param>
        protected override void HandleBroadcast(BroadcastEventArgs e)
        {
            if (!typeof(BaseEntity).IsAssignableFrom(typeof(T)))
                return;

            string[] arrColumns = ColumnList.Split(',');
            if (Array.IndexOf(arrColumns, "URNO") < 0)
                return;

            CedaBroadcastMessage message = (CedaBroadcastMessage)e.BroadcastMessage;

            Dictionary<string, string> dicData = CedaUtils.GetFieldDataDictionary(message.Fields, message.Data);

            int intUrno = CedaUtils.GetUrnoFromBroadcast(message.Selection, dicData);
            if (intUrno < 0)
                return;

            Type type = typeof(T);
            EntityTableMapping entityTableMapping = DataContext.EntityTableMappings[type];
            if (entityTableMapping == null)
                return;

            EntityColumnMappingCollection entityColumnMappings = entityTableMapping.ColumnMappings;
            object objItem = Activator.CreateInstance(type);
            BaseEntity entity = (BaseEntity)objItem;
            entity.Urno = intUrno;
            T item = (T)objItem;
            int intIndex = IndexOf(item);
            if (intIndex >= 0)
                item = this[intIndex];
            
            switch (message.Command)
            {
                case "IRT":
                case "IFR":
                    HandleInsert(item, dicData, entityColumnMappings);
                    break;
                case "URT":
                case "UFR":
                    if (intIndex >= 0) HandleUpdate(item, dicData, entityColumnMappings);
                    break;
                case "DRT":
                case "DFR":
                    if (intIndex >= 0) HandleDelete(item);
                    break;
                default:
                    //Unrecognized command
                    break;
            }
        }

        private void HandleInsert(T item, Dictionary<string, string> dicData, EntityColumnMappingCollection entityColumnMappings)
        {
            bool bInsert = !Contains(item);
            
            HandleUpdate(item, dicData, entityColumnMappings);
            if (bInsert) 
                Add(item);
        }

        private void HandleUpdate(T item, Dictionary<string, string> dicData, EntityColumnMappingCollection entityColumnMappings)
        {
            CedaDataConverter converter = new CedaDataConverter();
            foreach (string strColumn in dicData.Keys)
            {
                EntityColumnMapping entityColumnMapping = entityColumnMappings[strColumn];
                if (entityColumnMapping != null)
                {
                    PropertyInfo info = entityColumnMapping.PropertyInfo;
                    object objValue = converter.CedaItemToDataItem(info.PropertyType, dicData[strColumn], DataContext.DefaultTimeZoneInfo, entityColumnMapping.BoolConverter);
                    info.SetValue(item, objValue, null);
                }
            }
        }

        private void HandleDelete(T item)
        {
            Remove(item);
        }

        /// <summary>
        /// Initializes a new instance of Ufis.Data.CedaEntityCollection class.
        /// </summary>
        /// <param name="dataContext">The Ufis.Data.EntityDataContextBase that owns this instance of Ufis.Data.EntityCollectionBase class.</param>
        public CedaEntityCollection(EntityDataContextBase dataContext)
            : base(dataContext)
        {
            
        }
        /// <summary>
        /// Initializes a new instance of Ufis.Data.CedaEntityCollection class.
        /// </summary>
        /// <param name="list">The list form which the elements are copied.</param>
        /// <param name="dataContext">The Ufis.Data.EntityDataContextBase that owns this instance of Ufis.Data.EntityCollectionBase class.</param>
        public CedaEntityCollection(List<T> list, EntityDataContextBase dataContext)
            : base(list, dataContext)
        {
            
        }
        /// <summary>
        /// Initializes a new instance of Ufis.Data.CedaEntityCollection class.
        /// </summary>
        /// <param name="collection">The collection form which the elements are copied.</param>
        /// <param name="dataContext">The Ufis.Data.EntityDataContextBase that owns this instance of Ufis.Data.EntityCollectionBase class.</param>
        public CedaEntityCollection(IEnumerable<T> collection, EntityDataContextBase dataContext)
            : base(collection, dataContext)
        {
            
        }
    }
}
