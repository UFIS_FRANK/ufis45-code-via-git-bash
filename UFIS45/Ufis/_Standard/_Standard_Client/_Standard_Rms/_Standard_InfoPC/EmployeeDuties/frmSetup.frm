VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmSetup 
   Caption         =   "Setup"
   ClientHeight    =   6435
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12045
   Icon            =   "frmSetup.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   429
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   803
   StartUpPosition =   2  'CenterScreen
   Tag             =   "120"
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   9360
      Top             =   5625
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame2 
      Height          =   5430
      Left            =   7320
      TabIndex        =   9
      Top             =   240
      Width           =   4530
      Begin VB.TextBox txtMISC 
         Height          =   285
         Index           =   10
         Left            =   2580
         TabIndex        =   41
         Text            =   "Text1"
         Top             =   3855
         Width           =   1455
      End
      Begin VB.TextBox txtMISC 
         Height          =   330
         Index           =   12
         Left            =   2880
         TabIndex        =   37
         Text            =   "Text1"
         Top             =   4320
         Width           =   465
      End
      Begin VB.TextBox txtMISC 
         Height          =   330
         Index           =   11
         Left            =   1395
         TabIndex        =   34
         Text            =   "Text1"
         Top             =   4320
         Width           =   510
      End
      Begin VB.CommandButton cmdChoose 
         Height          =   240
         Index           =   3
         Left            =   4050
         TabIndex        =   33
         Top             =   2835
         Width           =   240
      End
      Begin VB.CommandButton cmdChoose 
         Height          =   240
         Index           =   2
         Left            =   4050
         TabIndex        =   32
         Top             =   2520
         Width           =   240
      End
      Begin VB.CommandButton cmdChoose 
         Height          =   240
         Index           =   1
         Left            =   4050
         TabIndex        =   31
         Top             =   2205
         Width           =   240
      End
      Begin VB.CommandButton cmdChoose 
         Height          =   240
         Index           =   0
         Left            =   4050
         TabIndex        =   30
         Top             =   1845
         Width           =   240
      End
      Begin VB.TextBox txtMISC 
         Height          =   285
         Index           =   9
         Left            =   2520
         TabIndex        =   29
         Text            =   "Text1"
         Top             =   3510
         Width           =   1455
      End
      Begin VB.TextBox txtMISC 
         Height          =   285
         Index           =   8
         Left            =   2520
         TabIndex        =   27
         Text            =   "Text1"
         Top             =   3150
         Width           =   1455
      End
      Begin VB.TextBox txtMISC 
         Height          =   285
         Index           =   7
         Left            =   2520
         TabIndex        =   25
         Text            =   "Text1"
         Top             =   2835
         Width           =   1455
      End
      Begin VB.TextBox txtMISC 
         Height          =   285
         Index           =   6
         Left            =   2520
         TabIndex        =   23
         Text            =   "Text1"
         Top             =   2490
         Width           =   1455
      End
      Begin VB.TextBox txtMISC 
         Height          =   285
         Index           =   5
         Left            =   2520
         TabIndex        =   22
         Text            =   "Text1"
         Top             =   2160
         Width           =   1455
      End
      Begin VB.TextBox txtMISC 
         Height          =   285
         Index           =   4
         Left            =   2520
         TabIndex        =   21
         Text            =   "Text1"
         Top             =   1830
         Width           =   1455
      End
      Begin VB.TextBox txtMISC 
         Height          =   285
         Index           =   3
         Left            =   2520
         TabIndex        =   20
         Text            =   "Text1"
         Top             =   1500
         Width           =   1455
      End
      Begin VB.TextBox txtMISC 
         Height          =   285
         Index           =   2
         Left            =   2520
         TabIndex        =   19
         Text            =   "Text1"
         Top             =   1170
         Width           =   1455
      End
      Begin VB.TextBox txtMISC 
         Height          =   285
         Index           =   1
         Left            =   2520
         TabIndex        =   13
         Text            =   "Text1"
         Top             =   840
         Width           =   1455
      End
      Begin VB.TextBox txtMISC 
         Height          =   285
         Index           =   0
         Left            =   2520
         TabIndex        =   10
         Text            =   "Text1"
         Top             =   495
         Width           =   1455
      End
      Begin VB.Label lblMisc 
         Caption         =   "Breite des Spaltepaar-Trenners....."
         Height          =   255
         Index           =   10
         Left            =   135
         TabIndex        =   42
         Tag             =   "121"
         Top             =   3885
         Width           =   2415
      End
      Begin VB.Label lblMisc 
         Caption         =   "h danach"
         Height          =   255
         Index           =   13
         Left            =   3390
         TabIndex        =   38
         Tag             =   "113"
         Top             =   4410
         Width           =   945
      End
      Begin VB.Label lblMisc 
         Caption         =   "h davor"
         Height          =   255
         Index           =   12
         Left            =   1965
         TabIndex        =   36
         Tag             =   "112"
         Top             =   4365
         Width           =   855
      End
      Begin VB.Label lblMisc 
         Caption         =   "Relative Periode"
         Height          =   255
         Index           =   11
         Left            =   135
         TabIndex        =   35
         Tag             =   "111"
         Top             =   4365
         Width           =   1200
      End
      Begin VB.Label lblMisc 
         Caption         =   "Daten-Ladeintervall [min].............."
         Height          =   255
         Index           =   9
         Left            =   90
         TabIndex        =   28
         Tag             =   "110"
         Top             =   3555
         Width           =   2415
      End
      Begin VB.Label lblMisc 
         Caption         =   "Broadcast Markierungszeit [s]......."
         Height          =   255
         Index           =   8
         Left            =   90
         TabIndex        =   26
         Tag             =   "109"
         Top             =   3240
         Width           =   2415
      End
      Begin VB.Label lblMisc 
         Caption         =   "Farbe der geraden Zeilen............."
         Height          =   255
         Index           =   7
         Left            =   90
         TabIndex        =   24
         Tag             =   "108"
         Top             =   2880
         Width           =   2415
      End
      Begin VB.Label lblMisc 
         Caption         =   "Farbe der ungeraden Zeilen........."
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   18
         Tag             =   "107"
         Top             =   2565
         Width           =   2415
      End
      Begin VB.Label lblMisc 
         Caption         =   "Schriftgrösse................................."
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   17
         Tag             =   "106"
         Top             =   2250
         Width           =   2415
      End
      Begin VB.Label lblMisc 
         Caption         =   "Schriftart......................................."
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   16
         Tag             =   "105"
         Top             =   1935
         Width           =   2415
      End
      Begin VB.Label lblMisc 
         Caption         =   "Zeilenhöhe [Pixel]........................."
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   15
         Tag             =   "104"
         Top             =   1575
         Width           =   2415
      End
      Begin VB.Label lblMisc 
         Caption         =   "Anzahl Zeilen..............................."
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   14
         Tag             =   "103"
         Top             =   1260
         Width           =   2415
      End
      Begin VB.Label lblMisc 
         Caption         =   "Spaltenbreite [Pixel]......................"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   12
         Tag             =   "102"
         Top             =   945
         Width           =   2415
      End
      Begin VB.Label lblMisc 
         Caption         =   "Spaltenpaare..............................."
         Height          =   255
         Index           =   0
         Left            =   135
         TabIndex        =   11
         Tag             =   "101"
         Top             =   585
         Width           =   2415
      End
   End
   Begin VB.CommandButton btnCancel 
      Caption         =   "&Abbrechen"
      Height          =   375
      Left            =   6083
      TabIndex        =   1
      Tag             =   "115"
      Top             =   5865
      Width           =   1215
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "&OK"
      Height          =   375
      Left            =   4763
      TabIndex        =   0
      Tag             =   "114"
      Top             =   5865
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Height          =   5430
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   6855
      Begin VB.CommandButton btnDelete 
         Caption         =   "<<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   3015
         TabIndex        =   40
         Top             =   3285
         Width           =   735
      End
      Begin VB.CommandButton btnAdd 
         Caption         =   ">>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   3015
         TabIndex        =   39
         Top             =   1440
         Width           =   735
      End
      Begin VB.ListBox lstSource 
         Height          =   4545
         Left            =   240
         MultiSelect     =   2  'Extended
         Sorted          =   -1  'True
         TabIndex        =   6
         Top             =   630
         Width           =   2535
      End
      Begin VB.ListBox lstDest 
         Height          =   4545
         Left            =   3960
         MultiSelect     =   2  'Extended
         Sorted          =   -1  'True
         TabIndex        =   5
         Top             =   630
         Width           =   2535
      End
      Begin VB.CommandButton btnAdd 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   3015
         TabIndex        =   4
         Top             =   2070
         Width           =   735
      End
      Begin VB.CommandButton btnDelete 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   3015
         TabIndex        =   3
         Top             =   2670
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "116"
         Height          =   255
         Left            =   270
         TabIndex        =   8
         Tag             =   "116"
         Top             =   360
         Width           =   2505
      End
      Begin VB.Label Label2 
         Caption         =   "117"
         Height          =   255
         Left            =   3990
         TabIndex        =   7
         Tag             =   "117"
         Top             =   360
         Width           =   2505
      End
   End
End
Attribute VB_Name = "frmSetup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btnCancel_Click()
    Unload frmSetup
End Sub

Private Sub btnOK_Click()
    Dim count As Long
    Dim i As Integer
    Dim strResult As String

    count = lstDest.ListCount
    For i = 0 To count - 1
        If i < count - 1 Then
            strResult = strResult + lstDest.List(i) + ","
        Else
            strResult = strResult + lstDest.List(i)
        End If
    Next i

    save_COLUMN_PAIRS = txtMISC(0).Text
    save_COLUMN_WIDTH = txtMISC(1).Text
    save_TAB_LINES = txtMISC(2).Text
    save_LINEHEIGHT = txtMISC(3).Text
    save_TAB_FONT = txtMISC(4).Text
    save_TAB_FONT_SIZE = txtMISC(5).Text
    save_COLOR1 = txtMISC(6).Text
    save_COLOR2 = txtMISC(7).Text
    save_BC_HIGHLIGHT_SECONDS = txtMISC(8).Text
    save_RELOAD_MINUTES = txtMISC(9).Text
    save_COLUMN_PAIR_SEPARATOR = CInt(txtMISC(10).Text)
    save_MINUS_X_HOURS = txtMISC(11).Text
    save_PLUS_X_HOURS = txtMISC(12).Text
    save_FUNCTIONS = strResult

    'MainForm.FUNCTIONS = strResult
    MainForm.SaveConfigFile
    Unload frmSetup
End Sub

Private Sub cmdChoose_Click(Index As Integer)
    On Error GoTo ErrHdl
    Dim strRet As String
    Select Case Index
        Case 0:
            CommonDialog1.Flags = cdlCFScreenFonts
            CommonDialog1.FontName = txtMISC(Index + 4).Text
            CommonDialog1.FontSize = txtMISC(Index + 5).Text
            CommonDialog1.ShowFont
            txtMISC(Index + 4).Text = CommonDialog1.FontName
            txtMISC(Index + 4).Font = txtMISC(Index + 4).Text
            txtMISC(Index + 5).Text = CommonDialog1.FontSize
         Case 1:
            CommonDialog1.Flags = cdlCFScreenFonts
            CommonDialog1.FontName = txtMISC(Index + 4).Text
            CommonDialog1.FontSize = txtMISC(Index + 5).Text
            CommonDialog1.ShowFont
            txtMISC(Index + 3).Text = CommonDialog1.FontName
            txtMISC(Index + 3).Font = txtMISC(Index + 3).Text
            txtMISC(Index + 4).Text = CommonDialog1.FontSize
        Case 2:
            CommonDialog1.Color = txtMISC(Index + 4).BackColor
            CommonDialog1.ShowColor
            txtMISC(Index + 4).Text = CommonDialog1.Color
            txtMISC(Index + 4).BackColor = txtMISC(Index + 4).Text
        Case 3:
            CommonDialog1.Color = txtMISC(Index + 4).BackColor
            CommonDialog1.ShowColor
            txtMISC(Index + 4).Text = CommonDialog1.Color
            txtMISC(Index + 4).BackColor = txtMISC(Index + 4).Text
    End Select
ErrHdl:
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim count As Long
    Dim i As Long, j As Long
    Dim strTmp As String
    Dim arrFkt() As String
    Dim fktCount As Integer
    Dim found As Boolean
    Dim ilIdx As Integer

    LoadResStrings Me

    arrFkt = Split(save_FUNCTIONS, ",")
    fktCount = UBound(arrFkt)
    count = frmTables.tabPFC.GetLineCount

    For i = 0 To count - 1
        found = False
        For j = 0 To fktCount
            If arrFkt(j) = frmTables.tabPFC.GetColumnValue(i, 1) Then
                found = True
                j = fktCount + 1
            End If
        Next j
        If found = False Then
            lstSource.AddItem frmTables.tabPFC.GetColumnValue(i, 1)
        End If
    Next i
    
    ilIdx = GetItemNo(frmTables.PfcFields, "FCTC") - 1
    For i = 0 To fktCount
        If (frmTables.tabPFC.GetLinesByColumnValue(ilIdx, arrFkt(i), 0) <> "") Then
            lstDest.AddItem arrFkt(i)
        End If
    Next i

    InitControls
End Sub
    
Private Function InitControls()
    Dim i As Integer

    For i = 0 To 10
        lblMisc(i).Height = 255
        lblMisc(i).Width = 2415
        lblMisc(i).Left = 120
        lblMisc(i).Top = (lblMisc(i).Height + 120) * i + 630

        txtMISC(i).Height = 285
        txtMISC(i).Width = 1455
        txtMISC(i).Left = lblMisc(i).Left + lblMisc(i).Width + 50
        txtMISC(i).Top = lblMisc(i).Top - (txtMISC(i).Height - lblMisc(i).Height) - 60
    Next i

    For i = 0 To 3
        cmdChoose(i).Height = 285
        cmdChoose(i).Width = 285
        cmdChoose(i).Top = txtMISC(i + 4).Top
        cmdChoose(i).Left = txtMISC(i + 4).Left + txtMISC(i + 4).Width + 60
        cmdChoose(i).Caption = "..."
    Next i

    For i = 11 To 13
        lblMisc(i).Height = 255
        lblMisc(i).Top = (lblMisc(i).Height + 120) * 11 + 730
    Next i

    For i = 11 To 12
        txtMISC(i).Height = 285
        txtMISC(i).Top = lblMisc(11).Top - (txtMISC(11).Height - lblMisc(11).Height) - 60
    Next i

    txtMISC(0).Text = save_COLUMN_PAIRS
    txtMISC(1).Text = save_COLUMN_WIDTH
    txtMISC(2).Text = save_TAB_LINES
    txtMISC(3).Text = save_LINEHEIGHT
    txtMISC(4).Text = save_TAB_FONT
    txtMISC(5).Text = save_TAB_FONT_SIZE
    txtMISC(10).Text = CStr(save_COLUMN_PAIR_SEPARATOR)
    txtMISC(11).Text = save_MINUS_X_HOURS
    txtMISC(12).Text = save_PLUS_X_HOURS
    txtMISC(6).Text = save_COLOR1
    txtMISC(7).Text = save_COLOR2
    txtMISC(8).Text = save_BC_HIGHLIGHT_SECONDS
    txtMISC(9).Text = save_RELOAD_MINUTES

    txtMISC(4).Font = txtMISC(4).Text
    txtMISC(6).BackColor = txtMISC(6).Text
    txtMISC(7).BackColor = txtMISC(7).Text
End Function

Private Sub lstDest_DblClick()
    Dim i As Integer
    Dim j As Integer

    i = 0
    While i < lstDest.ListCount
        If lstDest.Selected(i) = True Then
            lstSource.AddItem lstDest.List(i)
            lstDest.RemoveItem (i)
        Else
            i = i + 1
        End If
    Wend
End Sub

Private Sub lstSource_DblClick()
    Dim i As Integer
    Dim j As Integer

    i = 0
    While i < lstSource.ListCount
        If lstSource.Selected(i) = True Then
            lstDest.AddItem lstSource.List(i)
            lstSource.RemoveItem (i)
        Else
            i = i + 1
        End If
    Wend
End Sub

Private Sub btnAdd_Click(Index As Integer)
    Dim llCount As Long
    Dim i As Integer
    llCount = lstSource.ListCount

    For i = llCount - 1 To 0 Step -1
        Select Case Index
            Case 0: 'only move the selected ones
                If lstSource.Selected(i) = True Then
                    lstDest.AddItem lstSource.List(i)
                    lstSource.RemoveItem i
                End If

            Case 1: 'move all ones
                lstDest.AddItem lstSource.List(i)
                lstSource.RemoveItem i
        End Select
    Next i
End Sub

Private Sub btnDelete_Click(Index As Integer)
    Dim llCount As Long
    Dim i As Integer
    llCount = lstDest.ListCount
    
    For i = llCount - 1 To 0 Step -1
        Select Case Index
            Case 0: 'only move the selected ones
                If lstDest.Selected(i) = True Then
                    lstSource.AddItem lstDest.List(i)
                    lstDest.RemoveItem i
                End If

            Case 1: 'move all ones
                lstSource.AddItem lstDest.List(i)
                lstDest.RemoveItem i
        End Select
    Next i
End Sub

