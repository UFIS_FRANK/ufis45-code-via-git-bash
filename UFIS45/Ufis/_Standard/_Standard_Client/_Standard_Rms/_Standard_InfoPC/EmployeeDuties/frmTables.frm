VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Begin VB.Form frmTables 
   Caption         =   "Tables"
   ClientHeight    =   9855
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10830
   LinkTopic       =   "Form1"
   ScaleHeight     =   9855
   ScaleWidth      =   10830
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   7440
      Top             =   4320
   End
   Begin TABLib.TAB tabResult 
      Height          =   2655
      Left            =   90
      TabIndex        =   25
      Top             =   7140
      Width           =   7575
      _Version        =   65536
      _ExtentX        =   13361
      _ExtentY        =   4683
      _StockProps     =   0
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Reset"
      Height          =   225
      Left            =   9930
      TabIndex        =   11
      Top             =   7620
      Width           =   885
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Linecount"
      Height          =   225
      Left            =   9930
      TabIndex        =   10
      Top             =   7380
      Width           =   885
   End
   Begin VB.CommandButton Command1 
      Caption         =   "CallCeda"
      Height          =   285
      Left            =   9930
      TabIndex        =   9
      Top             =   7080
      Width           =   885
   End
   Begin TABLib.TAB t1 
      Height          =   795
      Left            =   7770
      TabIndex        =   8
      Top             =   7050
      Width           =   2145
      _Version        =   65536
      _ExtentX        =   3784
      _ExtentY        =   1402
      _StockProps     =   0
   End
   Begin UFISCOMLib.UfisCom aUfis 
      Left            =   1530
      Top             =   0
      _Version        =   65536
      _ExtentX        =   1032
      _ExtentY        =   397
      _StockProps     =   0
   End
   Begin TABLib.TAB tabJOB 
      Height          =   915
      Left            =   90
      TabIndex        =   0
      Tag             =   "JOBTAB"
      Top             =   240
      Width           =   5745
      _Version        =   65536
      _ExtentX        =   10134
      _ExtentY        =   1614
      _StockProps     =   0
   End
   Begin TABLib.TAB tabSTF 
      Height          =   915
      Left            =   90
      TabIndex        =   1
      Tag             =   "STFTAB"
      Top             =   1380
      Width           =   5745
      _Version        =   65536
      _ExtentX        =   10134
      _ExtentY        =   1614
      _StockProps     =   0
   End
   Begin TABLib.TAB tabJTY 
      Height          =   915
      Left            =   90
      TabIndex        =   4
      Tag             =   "JTYTAB"
      Top             =   2520
      Width           =   5745
      _Version        =   65536
      _ExtentX        =   10134
      _ExtentY        =   1614
      _StockProps     =   0
   End
   Begin TABLib.TAB tabPOL 
      Height          =   825
      Left            =   9360
      TabIndex        =   6
      Top             =   6090
      Width           =   1455
      _Version        =   65536
      _ExtentX        =   2566
      _ExtentY        =   1455
      _StockProps     =   0
   End
   Begin TABLib.TAB tabDRD 
      Height          =   915
      Left            =   90
      TabIndex        =   12
      Tag             =   "DRDTAB"
      Top             =   3660
      Width           =   5745
      _Version        =   65536
      _ExtentX        =   10134
      _ExtentY        =   1614
      _StockProps     =   0
   End
   Begin TABLib.TAB tabDEL 
      Height          =   915
      Left            =   90
      TabIndex        =   14
      Tag             =   "DELTAB"
      Top             =   4830
      Width           =   5745
      _Version        =   65536
      _ExtentX        =   10134
      _ExtentY        =   1614
      _StockProps     =   0
   End
   Begin TABLib.TAB tabDRR 
      Height          =   915
      Left            =   90
      TabIndex        =   16
      Tag             =   "DRRTAB"
      Top             =   5970
      Width           =   5745
      _Version        =   65536
      _ExtentX        =   10134
      _ExtentY        =   1614
      _StockProps     =   0
   End
   Begin TABLib.TAB tabBSD 
      Height          =   915
      Left            =   6060
      TabIndex        =   18
      Tag             =   "BSDTAB"
      Top             =   240
      Width           =   4305
      _Version        =   65536
      _ExtentX        =   7594
      _ExtentY        =   1614
      _StockProps     =   0
   End
   Begin TABLib.TAB tabSPF 
      Height          =   915
      Left            =   6030
      TabIndex        =   20
      Tag             =   "SPFTAB"
      Top             =   1380
      Width           =   4305
      _Version        =   65536
      _ExtentX        =   7594
      _ExtentY        =   1614
      _StockProps     =   0
   End
   Begin TABLib.TAB tabPFC 
      Height          =   915
      Left            =   6030
      TabIndex        =   23
      Tag             =   "PFCTAB"
      Top             =   2520
      Width           =   4305
      _Version        =   65536
      _ExtentX        =   7594
      _ExtentY        =   1614
      _StockProps     =   0
   End
   Begin VB.Label Label12 
      Caption         =   "Result:"
      Height          =   195
      Left            =   120
      TabIndex        =   26
      Top             =   6930
      Width           =   7545
   End
   Begin VB.Label Label11 
      Caption         =   "PFC:"
      Height          =   225
      Left            =   6060
      TabIndex        =   24
      Top             =   2280
      Width           =   1395
   End
   Begin VB.Label Label10 
      Caption         =   "----------------------------- Erst mal nicht lesen ! ----------------------------------"
      ForeColor       =   &H00FF0000&
      Height          =   225
      Left            =   6120
      TabIndex        =   22
      Top             =   5550
      Width           =   4665
   End
   Begin VB.Label Label9 
      Caption         =   "SPF:"
      Height          =   225
      Left            =   6060
      TabIndex        =   21
      Top             =   1140
      Width           =   1395
   End
   Begin VB.Label Label8 
      Caption         =   "BSD:"
      Height          =   225
      Left            =   6090
      TabIndex        =   19
      Top             =   0
      Width           =   1395
   End
   Begin VB.Label Label7 
      Caption         =   "DRR:"
      Height          =   225
      Left            =   120
      TabIndex        =   17
      Top             =   5730
      Width           =   1395
   End
   Begin VB.Label Label6 
      Caption         =   "DEL:"
      Height          =   225
      Left            =   120
      TabIndex        =   15
      Top             =   4590
      Width           =   1395
   End
   Begin VB.Label Label5 
      Caption         =   "DRD:"
      Height          =   225
      Left            =   120
      TabIndex        =   13
      Top             =   3450
      Width           =   1395
   End
   Begin VB.Label Label4 
      Caption         =   "POL:"
      Height          =   225
      Left            =   9390
      TabIndex        =   7
      Top             =   5850
      Width           =   1395
   End
   Begin VB.Label Label3 
      Caption         =   "JTY:"
      Height          =   225
      Left            =   120
      TabIndex        =   5
      Top             =   2310
      Width           =   1395
   End
   Begin VB.Label Label2 
      Caption         =   "STF:"
      Height          =   225
      Left            =   120
      TabIndex        =   3
      Top             =   1170
      Width           =   1395
   End
   Begin VB.Label Label1 
      Caption         =   "Jobs:"
      Height          =   225
      Left            =   90
      TabIndex        =   2
      Top             =   0
      Width           =   1395
   End
End
Attribute VB_Name = "frmTables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public StfFields As String
Public JtyFields As String
Public JobFields As String
Public DrdFields As String
Public DelFields As String
Public DrrFields As String
Public BsdFields As String
Public SpfFields As String
Public PfcFields As String

Public EmptyStfLine As String
Public EmptyJtyLine As String
Public EmptyJobLine As String
Public EmptyDrdLine As String
Public EmptyDelLine As String
Public EmptyDrrLine As String
Public EmptyBsdLine As String
Public EmptySpfLine As String
Public EmptyPfcLine As String

Public TimeFrameStart As String
Public TimeFrameEnd As String

Public Function LoadAllTables()
    Dim isSTF_Fields As String
    Dim isJTY_Fields As String
    Dim recCount As Long
    Dim i As Integer
    Dim tmpStr As String
    Dim colVal As String
    Dim rawString As String
    Dim LineStr As String
'---------------------------------------------------------------
'STF
    tabSTF.ResetContent
    tabSTF.HeaderLengthString = "100,100,130"
    StfFields = "URNO,SHNM,LANM,PERC"
    EmptyStfLine = ",,"
    tabSTF.HeaderString = StfFields
    If aUfis.CallServer("RT", "STFTAB", StfFields, "", "", 240) = 0 Then
        recCount = aUfis.GetBufferCount
        For i = 0 To recCount - 1
            rawString = aUfis.GetBufferLine(i)
            LineStr = Trim(GetItem(rawString, 1, ",")) + "," + Trim(GetItem(rawString, 2, ",")) + "," + Trim(GetItem(rawString, 3, ",")) + "," + Trim(GetItem(rawString, 4, ","))
            tmpStr = tmpStr + LineStr + Chr(10)
            If (i Mod 100) = 0 Then
                tabSTF.InsertBuffer tmpStr, Chr(10)
                tmpStr = ""
            End If
        Next i
        tabSTF.InsertBuffer tmpStr, Chr(10)
    End If
'---------------------------------------------------------------
'JTY
    tmpStr = ""
    tabJTY.ResetContent
    tabJTY.HeaderLengthString = "100,100"
    JtyFields = "URNO,NAME"
    EmptyJtyLine = ","
    tabJTY.HeaderString = JtyFields
    If aUfis.CallServer("RT", "JTYTAB", JtyFields, "", "", 240) = 0 Then
        recCount = aUfis.GetBufferCount
        For i = 0 To recCount - 1
            rawString = aUfis.GetBufferLine(i)
            LineStr = Trim(GetItem(rawString, 1, ",")) + "," + Trim(GetItem(rawString, 2, ","))
            tmpStr = tmpStr + LineStr + Chr(10)
            If (i Mod 100) = 0 Then
                tabJTY.InsertBuffer tmpStr, Chr(10)
                tmpStr = ""
            End If
        Next i
        tabJTY.InsertBuffer tmpStr, Chr(10)
    End If
    tmpStr = ""
    LoadJobs
    LoadDRD
    LoadDEL
    LoadDRR
    LoadBSD
    LoadSPF
    LoadPFC
    PrepareResultList
End Function
'---------------------------------------------------------------
'JOB
'---------------------------------------------------------------
Public Function LoadJobs()
    Dim lsFields As String
    Dim recCount As Long
    Dim i As Integer
    Dim tmpStr As String
    Dim Where As String
    Dim rawString As String
    Dim LineStr As String
    
    tabJOB.ResetContent
    tabJOB.HeaderLengthString = "100,100,100,100,100,100,100,100,100"
    '             0   1    2    3    4    5    6    7    8
    JobFields = "URNO,UDRD,UDEL,UDSR,ACFR,ACTO,STAT,UJTY,USTF"
    EmptyJobLine = ",,,,,,,,"
    tabJOB.HeaderString = JobFields
    Where = MakeJobWhere()
    If aUfis.CallServer("RT", "JOBTAB", JobFields, "", Where, 240) = 0 Then
        recCount = aUfis.GetBufferCount
        For i = 0 To recCount - 1
            rawString = aUfis.GetBufferLine(i)
            LineStr = Trim(GetItem(rawString, 1, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 2, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 3, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 4, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 5, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 6, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 7, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 8, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 9, ","))
            tmpStr = tmpStr + LineStr + Chr(10)
            If (i Mod 100) = 0 Then
                tabJOB.InsertBuffer tmpStr, Chr(10)
                tmpStr = ""
            End If
        Next i
        tabJOB.InsertBuffer tmpStr, Chr(10)
'    Else
'        If aUfis.LastErrorMessage <> "" Then
'            MsgBox aUfis.LastErrorMessage
'        End If
    End If
End Function

'---------------------------------------------------------------
'DRD
'---------------------------------------------------------------
Public Function LoadDRD()
    Dim recCount As Long
    Dim i As Long

    Dim datNow As Date
    Dim ldatEnd As Date
    Dim datStart As Date

    Dim strEnd As String
    Dim tmpStr As String
    Dim LineStr As String
    Dim strStart As String
    Dim strWhere As String
    Dim rawString As String

    datNow = Now

    datStart = DateAdd("h", -MainForm.MINUS_X_HOURS, datNow)
    ldatEnd = DateAdd("h", MainForm.PLUS_X_HOURS, datNow)
    strStart = Left(MakeCedaDate(datStart), 8)
    strEnd = Left(MakeCedaDate(ldatEnd), 8)
    strWhere = "WHERE SDAY BETWEEN '" + strStart + "' AND '" + strEnd + "'"

    tabDRD.ResetContent
    tabDRD.HeaderLengthString = "100,100"
    DrdFields = "URNO,FCTC"
    EmptyDrdLine = ","
    tabDRD.HeaderString = DrdFields

    If aUfis.CallServer("RT", "DRDTAB", DrdFields, "", strWhere, 240) = 0 Then
        recCount = aUfis.GetBufferCount
        For i = 0 To recCount - 1
            rawString = aUfis.GetBufferLine(i)
            LineStr = Trim(GetItem(rawString, 1, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 2, ","))
            tmpStr = tmpStr + LineStr + Chr(10)
            If (i Mod 100) = 0 Then
                tabDRD.InsertBuffer tmpStr, Chr(10)
                tmpStr = ""
            End If
        Next i
        tabDRD.InsertBuffer tmpStr, Chr(10)
    End If

End Function
'---------------------------------------------------------------
'DEL
'---------------------------------------------------------------
Public Function LoadDEL()
    Dim lsFields As String
    Dim recCount As Long
    Dim i As Integer
    Dim tmpStr As String
    Dim Where As String
    Dim rawString As String
    Dim LineStr As String
    
    tabDEL.ResetContent
    tabDEL.HeaderLengthString = "100,100"
    DelFields = "URNO,FCTC"
    EmptyDelLine = ","
    tabDEL.HeaderString = DelFields
    If aUfis.CallServer("RT", "DELTAB", DelFields, "", Where, 240) = 0 Then
        recCount = aUfis.GetBufferCount
        For i = 0 To recCount - 1
            rawString = aUfis.GetBufferLine(i)
            LineStr = Trim(GetItem(rawString, 1, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 2, ","))
            tmpStr = tmpStr + LineStr + Chr(10)
            If (i Mod 100) = 0 Then
                tabDEL.InsertBuffer tmpStr, Chr(10)
                tmpStr = ""
            End If
        Next i
        tabDEL.InsertBuffer tmpStr, Chr(10)
    End If
End Function

'---------------------------------------------------------------
'DRR
'---------------------------------------------------------------
Public Function LoadDRR()
    Dim i As Long
    Dim recCount As Long

    Dim tmpStr As String
    Dim strEnd As String
    Dim LineStr As String
    Dim strStart As String
    Dim retWhere As String
    Dim strWhere As String
    Dim rawString As String

    Dim datNow As Date
    Dim ldatEnd As Date
    Dim datStart As Date

    datNow = Now

    datStart = DateAdd("h", -MainForm.MINUS_X_HOURS, datNow)
    ldatEnd = DateAdd("h", MainForm.PLUS_X_HOURS, datNow)
    strStart = Left(MakeCedaDate(datStart), 8)
    strEnd = Left(MakeCedaDate(ldatEnd), 8)
    strWhere = "WHERE SDAY BETWEEN '" + strStart + "' AND '" + strEnd + "'"
    
    tabDRR.ResetContent
    tabDRR.HeaderLengthString = "100,100,100"
    DrrFields = "URNO,FCTC,BSDU"
    EmptyDrrLine = ",,"
    tabDRR.HeaderString = DrrFields
    If aUfis.CallServer("RT", "DRRTAB", DrrFields, "", strWhere, 240) = 0 Then
        recCount = aUfis.GetBufferCount
        For i = 0 To recCount - 1
            rawString = aUfis.GetBufferLine(i)
            LineStr = Trim(GetItem(rawString, 1, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 2, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 3, ","))
            tmpStr = tmpStr + LineStr + Chr(10)
            If (i Mod 100) = 0 Then
                tabDRR.InsertBuffer tmpStr, Chr(10)
                tmpStr = ""
            End If
        Next i
        tabDRR.InsertBuffer tmpStr, Chr(10)
    End If
End Function

'---------------------------------------------------------------
'BSD
'---------------------------------------------------------------
Public Function LoadBSD()
    Dim lsFields As String
    Dim recCount As Long
    Dim i As Integer
    Dim tmpStr As String
    Dim Where As String
    Dim rawString As String
    Dim LineStr As String
    
    tabBSD.ResetContent
    tabBSD.HeaderLengthString = "100,100"
    BsdFields = "URNO,FCTC"
    EmptyBsdLine = ","
    tabBSD.HeaderString = BsdFields
    If aUfis.CallServer("RT", "BSDTAB", BsdFields, "", Where, 240) = 0 Then
        recCount = aUfis.GetBufferCount
        For i = 0 To recCount - 1
            rawString = aUfis.GetBufferLine(i)
            LineStr = Trim(GetItem(rawString, 1, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 2, ","))
            tmpStr = tmpStr + LineStr + Chr(10)
            If (i Mod 100) = 0 Then
                tabBSD.InsertBuffer tmpStr, Chr(10)
                tmpStr = ""
            End If
        Next i
        tabBSD.InsertBuffer tmpStr, Chr(10)
    End If

End Function
'---------------------------------------------------------------
'SPF
'---------------------------------------------------------------
Public Function LoadSPF()
    Dim lsFields As String
    Dim recCount As Long
    Dim i As Integer
    Dim tmpStr As String
    Dim Where As String
    Dim rawString As String
    Dim LineStr As String
    
    tabSPF.ResetContent
    tabSPF.HeaderLengthString = "100,100,80,100,100,100"
    '           0    1    2    3    4    5
    SpfFields = "URNO,SURN,CODE,VPFR,VPTO,PRIO"
    EmptySpfLine = ",,,,,"
    tabSPF.HeaderString = SpfFields
    If aUfis.CallServer("RT", "SPFTAB", SpfFields, "", Where, 240) = 0 Then
        recCount = aUfis.GetBufferCount
        For i = 0 To recCount - 1
            rawString = aUfis.GetBufferLine(i)
            LineStr = Trim(GetItem(rawString, 1, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 2, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 3, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 4, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 5, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 6, ","))
            tmpStr = tmpStr + LineStr + Chr(10)
            If (i Mod 100) = 0 Then
                tabSPF.InsertBuffer tmpStr, Chr(10)
                tmpStr = ""
            End If
        Next i
        tabSPF.InsertBuffer tmpStr, Chr(10)
    End If

End Function
'---------------------------------------------------------------
'PFC
'---------------------------------------------------------------
Public Function LoadPFC()
    Dim lsFields As String
    Dim recCount As Long
    Dim i As Integer
    Dim tmpStr As String
    Dim Where As String
    Dim rawString As String
    Dim LineStr As String
    
    tabPFC.ResetContent
    tabPFC.HeaderLengthString = "100,100"
    PfcFields = "URNO,FCTC"
    EmptyPfcLine = ","
    tabPFC.HeaderString = PfcFields
    If aUfis.CallServer("RT", "PFCTAB", PfcFields, "", Where, 240) = 0 Then
        recCount = aUfis.GetBufferCount
        For i = 0 To recCount - 1
            rawString = aUfis.GetBufferLine(i)
            LineStr = Trim(GetItem(rawString, 1, ",")) + ","
            LineStr = LineStr + Trim(GetItem(rawString, 2, ","))
            tmpStr = tmpStr + LineStr + Chr(10)
            If (i Mod 100) = 0 Then
                tabPFC.InsertBuffer tmpStr, Chr(10)
                tmpStr = ""
            End If
        Next i
        tabPFC.InsertBuffer tmpStr, Chr(10)
    End If

End Function
'--------------------------------------------------
'Make JobWhere
'--------------------------------------------------
Public Function MakeJobWhere() As String
    Dim datNow As Date
    Dim ldatEnd As Date
    Dim ldatStart As Date

    Dim strEnd As String
    Dim strStart As String
    Dim retWhere As String

    datNow = Now - (sgUTCOffsetHours / 24)
    ldatStart = DateAdd("h", -MainForm.MINUS_X_HOURS, datNow)
    ldatEnd = DateAdd("h", MainForm.PLUS_X_HOURS, datNow)
    strStart = MakeCedaDate(ldatStart)
    strEnd = MakeCedaDate(ldatEnd)
    TimeFrameStart = strStart
    TimeFrameEnd = strEnd
    retWhere = "WHERE ACFR BETWEEN '" + strStart + "' AND '" + strEnd + "' AND UJTY IN (" + MainForm.JOBTYPES + ")"
    MakeJobWhere = retWhere
End Function


Private Sub Command1_Click()
    t1.ServerName = "Dieburg"
    t1.Port = "3357"
    t1.HOPO = "ZRH"
    t1.CurrentApplication = "Test"
    t1.Tabext = "TAB"
    t1.User = "MWO"
    t1.WorkStation = "wks104"
    t1.SendTimeout = "3"
    t1.ReceiveTimeout = "240"
    t1.CedaRecordSeparator = Chr(10)
    t1.CedaIdentifier = "Test_App"
    t1.ResetContent
    t1.RedrawTab
    t1.ShowHorzScroller True
    t1.HeaderString = "FCTC"
    t1.HeaderLengthString = "100"
    t1.CedaAction "RT", "PFCTAB", "FCTC", "", "WHERE URNO > 0"
End Sub

Private Sub Command2_Click()
    MsgBox t1.GetLineCount
End Sub

Private Sub Command3_Click()
t1.ResetContent
t1.RedrawTab
End Sub

Private Sub Form_Load()
    Dim ret As Integer
    ret = aUfis.InitCom(MainForm.SERVER, "CEDA")
    ret = aUfis.SetCedaPerameters("XXX", MainForm.HOPO, "TAB")
    tabJOB.ShowHorzScroller True
    tabJTY.ShowHorzScroller True
    tabSTF.ShowHorzScroller True
    tabPOL.ShowHorzScroller True
    tabDRD.ShowHorzScroller True
    tabDEL.ShowHorzScroller True
    tabDRR.ShowHorzScroller True
    tabBSD.ShowHorzScroller True
    tabSPF.ShowHorzScroller True
    sgUTCOffsetHours = GetUTCOffset
    Timer1.Enabled = True
End Sub
Private Sub Form_Unload(Cancel As Integer)
    aUfis.CleanupCom
End Sub
'-------------------------------------------------
' Prepares the resultlist and checks the functions
' as the filter criteria, which indicates whether
' a record shall be shown or not
'-------------------------------------------------
Public Function PrepareResultList()
    Dim ilJobCount As Long
    Dim i As Long
    Dim strJobLine As String

    tabResult.ResetContent
    tabResult.ColumnWidthString = "10,14,3"
    tabResult.ColumnAlignmentString = "L,L,L"
    tabResult.HeaderString = "URNO,TIME,EMPL"
    tabResult.HeaderLengthString = "100,100,100"

    ilJobCount = tabJOB.GetLineCount
    For i = 0 To ilJobCount - 1
        strJobLine = tabJOB.GetLineValues(i)
        If CheckJob(strJobLine) = True Then
            AddJobToResultList (strJobLine)
        End If
    Next i

    tabResult.Sort "1", True, True
    tabResult.RedrawTab
End Function

'-------------------------------------------------------
' Add a prepared jobline to the result list and
' checks whether the employee is already informed
'-------------------------------------------------------
Public Function AddJobToResultList(ByRef strJobLine As String)
    Dim strSTAT As String
    Dim strURNO As String
    Dim strUSTF As String
    Dim strLine As String
    Dim strStart As String
    Dim strLineNo As String
    Dim strCurrLine As String
    Dim strEmpAbbrev As String

    Dim i As Integer
    Dim iIdx As Integer

    iIdx = GetItemNo(tabJOB.HeaderString, "STAT")
    strSTAT = GetItem(strJobLine, iIdx, ",")

    If CheckEmpInformed(strSTAT) = False Then
        iIdx = GetItemNo(tabJOB.HeaderString, "URNO")
        strURNO = GetItem(strJobLine, iIdx, ",")

        iIdx = GetItemNo(tabJOB.HeaderString, "ACFR")
        strStart = GetItem(strJobLine, iIdx, ",")

        iIdx = GetItemNo(tabJOB.HeaderString, "USTF")
        strUSTF = GetItem(strJobLine, iIdx, ",")

        iIdx = GetItemNo(tabSTF.HeaderString, "URNO") - 1
        strLineNo = tabSTF.GetLinesByColumnValue(iIdx, strUSTF, 0)

        If strLineNo <> "" Then
            For i = 1 To ItemCount(strLineNo, ",")
                strLine = tabSTF.GetLineValues(CLng(GetItem(strLineNo, i, ",")))
                If strLine <> "" Then
                    iIdx = GetItemNo(tabSTF.HeaderString, "SHNM")
                    strEmpAbbrev = GetItem(strLine, iIdx, ",")
                    If Trim(strEmpAbbrev) = "" Then
                        iIdx = GetItemNo(tabSTF.HeaderString, "PERC")
                        strEmpAbbrev = GetItem(strLine, iIdx, ",")
                    End If
                End If
            Next i

            strCurrLine = strURNO + "," + strStart + "," + strEmpAbbrev
            tabResult.InsertTextLine strCurrLine, False
        End If
    End If
End Function

'-------------------------------------------------------
' Add a prepared jobline to the result list and
' checks whether the employee is already informed<
'-------------------------------------------------------
Public Function UpdateJobResultList(strJobLine As String)
    Dim strSTAT As String
    Dim strLine As String
    Dim strLineNo As String
    Dim strACFR As String
    Dim strUSTF As String
    Dim strJobUrno As String
    Dim strEmpAbbrev As String
    Dim strResultLineNo As String
    Dim strInsert As String
    Dim strURNO As String

    Dim i As Integer
    Dim iIdx As Integer

    iIdx = GetItemNo(tabJOB.HeaderString, "STAT")
    strSTAT = Trim(GetItem(strJobLine, iIdx, ","))

    iIdx = GetItemNo(tabJOB.HeaderString, "URNO")
    strJobUrno = Trim(GetItem(strJobLine, iIdx, ","))
    strResultLineNo = tabResult.GetLinesByColumnValue(iIdx - 1, strJobUrno, 1)

    If CheckEmpInformed(strSTAT) = False Then

        iIdx = GetItemNo(tabJOB.HeaderString, "ACFR")
        strACFR = Trim(GetItem(strJobLine, iIdx, ","))

        iIdx = GetItemNo(tabJOB.HeaderString, "USTF")
        strUSTF = Trim(GetItem(strJobLine, iIdx, ","))

        iIdx = GetItemNo(tabSTF.HeaderString, "URNO") - 1
        strLineNo = tabSTF.GetLinesByColumnValue(iIdx, strUSTF, 0)

        If strLineNo <> "" Then
            For i = 1 To ItemCount(strLineNo, ",")
                strLine = tabSTF.GetLineValues(GetItem(strLineNo, i, ","))
                If strLine <> "" Then
                    iIdx = GetItemNo(tabSTF.HeaderString, "SHNM")
                    strEmpAbbrev = Trim(GetItem(strLine, iIdx, ","))
                    If Trim(strEmpAbbrev) = "" Then
                        iIdx = GetItemNo(tabSTF.HeaderString, "PERC")
                        strEmpAbbrev = GetItem(strLine, iIdx, ",")
                    End If
                End If
            Next i

            If (IsNumeric(strResultLineNo) = True) Then
                iIdx = GetItemNo(tabResult.HeaderString, "TIME") - 1
                tabResult.SetColumnValue CLng(strResultLineNo), iIdx, strACFR

                iIdx = GetItemNo(tabResult.HeaderString, "EMPL") - 1
                tabResult.SetColumnValue CLng(strResultLineNo), 2, strEmpAbbrev
            Else
                iIdx = GetItemNo(tabJOB.HeaderString, "URNO")
                strURNO = GetItem(strJobLine, iIdx, ",")

                strInsert = strURNO & "," + strACFR + "," + strEmpAbbrev
                tabResult.InsertTextLine strInsert, False
            End If
        End If
    Else
        iIdx = GetItemNo(tabJOB.HeaderString, "URNO")
        strURNO = GetItem(strJobLine, iIdx, ",")

        iIdx = GetItemNo(tabResult.HeaderString, "URNO") - 1
        strLineNo = tabResult.GetLinesByColumnValue(iIdx, strURNO, 0)

        If strLineNo <> "" Then
            For i = 1 To ItemCount(strLineNo, ",")
                tabResult.DeleteLine GetItem(strLineNo, i, ",")
            Next i
        End If
    End If
End Function

Public Function CheckJob(JobLine As String) As Boolean
    Dim blFktFound As Boolean
    blFktFound = False
    
    If blFktFound = False Then blFktFound = CheckDRD(JobLine)

    If blFktFound = False Then blFktFound = CheckDEL(JobLine)

    If blFktFound = False Then blFktFound = CheckDRR(JobLine)

    If blFktFound = False Then blFktFound = CheckHighestPrioFunction(JobLine)
    
    CheckJob = blFktFound
End Function

Private Function CheckDRD(ByRef JobLine As String) As Boolean
    Dim i As Integer
    Dim iIdx As Integer
    Dim strItem As String
    Dim strLine As String
    Dim strLineNo As String

    CheckDRD = False

    iIdx = GetItemNo(tabJOB.HeaderString, "UDRD")
    strItem = GetItem(JobLine, iIdx, ",")

    iIdx = GetItemNo(tabDRD.HeaderString, "URNO") - 1
    strLineNo = tabDRD.GetLinesByColumnValue(iIdx, strItem, 0)

    For i = 1 To ItemCount(strLineNo, ",")
        strLine = tabDRD.GetLineValues(CLng(GetItem(strLineNo, i, ",")))
        If strLine <> "" Then
            iIdx = GetItemNo(tabDRD.HeaderString, "FCTC")
            strItem = GetItem(strLine, iIdx, ",")

            If strItem <> "" Then
                If InStr(MainForm.FUNCTIONS, strItem) > 0 Then
                    CheckDRD = True
                End If
            End If
        End If
    Next i
End Function

Private Function CheckDEL(ByRef JobLine As String) As Boolean
    Dim i As Integer
    Dim iIdx As Integer
    Dim strItem As String
    Dim strLine As String
    Dim strLineNo As String

    CheckDEL = False

    iIdx = GetItemNo(tabJOB.HeaderString, "UDEL")
    strItem = GetItem(JobLine, iIdx, ",")

    iIdx = GetItemNo(tabDEL.HeaderString, "URNO") - 1
    strLineNo = tabDEL.GetLinesByColumnValue(iIdx, strItem, 0)

    For i = 1 To ItemCount(strLineNo, ",")
        strLine = tabDEL.GetLineValues(CLng(GetItem(strLineNo, i, ",")))
        If strLine <> "" Then
            iIdx = GetItemNo(tabDEL.HeaderString, "FCTC")
            strItem = GetItem(strLine, iIdx, ",")

            If strItem <> "" Then
                If InStr(MainForm.FUNCTIONS, strItem) > 0 Then
                    CheckDEL = True
                End If
            End If
        End If
    Next i
End Function

Private Function CheckDRR(ByRef JobLine As String) As Boolean
    Dim i As Integer
    Dim iIdx As Integer
    Dim strItem As String
    Dim strLine As String
    Dim strLineNo As String

    CheckDRR = False

    iIdx = GetItemNo(tabJOB.HeaderString, "UDSR")
    strItem = GetItem(JobLine, iIdx, ",")

    iIdx = GetItemNo(tabDRR.HeaderString, "URNO") - 1
    strLineNo = tabDRR.GetLinesByColumnValue(iIdx, strItem, 0)

    For i = 1 To ItemCount(strLineNo, ",")
        strLine = tabDRR.GetLineValues(CLng(GetItem(strLineNo, i, ",")))
        If strLine <> "" Then
            iIdx = GetItemNo(tabDRR.HeaderString, "FCTC")
            strItem = GetItem(strLine, iIdx, ",")

            If strItem <> "" Then
                If InStr(MainForm.FUNCTIONS, strItem) > 0 Then
                    CheckDRR = True
                End If
            Else
                iIdx = GetItemNo(tabDRR.HeaderString, "BSDU")
                strItem = GetItem(strLine, iIdx, ",")

                iIdx = GetItemNo(tabBSD.HeaderString, "URNO") - 1
                strLineNo = tabBSD.GetLinesByColumnValue(iIdx, strItem, 0)

                If (strLineNo <> "") And (IsNumeric(strLineNo) = True) Then
                    iIdx = GetItemNo(tabBSD.HeaderString, "FCTC") - 1
                    strItem = tabBSD.GetColumnValue(CLng(strLineNo), iIdx)

                    If strItem <> "" Then
                        If InStr(MainForm.FUNCTIONS, strItem) > 0 Then
                            CheckDRR = True
                        End If
                    End If
                End If
            End If
        End If
    Next i
End Function

Private Function CheckHighestPrioFunction(ByRef JobLine As String) As Boolean
    Dim strLineNo As String
    Dim strLine As String
    Dim strItem As String
    Dim strVPFR As String
    Dim strVPTO As String
    Dim strPRIO As String
    Dim strNOW As String

    Dim iIdx As Integer
    Dim i As Integer

    Dim blAlreadyPrio1 As Boolean

    blAlreadyPrio1 = False
    CheckHighestPrioFunction = False
    strNOW = Format(Now, "yyyymmddhhmmss")

    iIdx = GetItemNo(tabJOB.HeaderString, "USTF")
    strItem = GetItem(JobLine, iIdx, ",")

    iIdx = GetItemNo(tabSPF.HeaderString, "SURN") - 1
    strLineNo = tabSPF.GetLinesByColumnValue(iIdx, strItem, 0)

    For i = 1 To ItemCount(strLineNo, ",")
        strLine = tabSPF.GetLineValues(CLng(GetItem(strLineNo, i, ",")))

        iIdx = GetItemNo(tabSPF.HeaderString, "VPFR")
        strVPFR = GetItem(strLine, iIdx, ",")

        iIdx = GetItemNo(tabSPF.HeaderString, "VPTO")
        strVPTO = GetItem(strLine, iIdx, ",")

        If (IsNumeric(strVPFR) = True) Then
            If (IsNumeric(strVPTO) = True) Then
                If (strVPFR < strNOW) And (strVPTO > strNOW) Then
                    iIdx = GetItemNo(tabSPF.HeaderString, "PRIO")
                    strPRIO = Trim(GetItem(strLine, iIdx, ","))
    
                    If strPRIO = "" Or strPRIO = "1" Then
                        iIdx = GetItemNo(tabSPF.HeaderString, "CODE")
                        strItem = Trim(GetItem(strLine, iIdx, ","))
                    End If
                End If
            ElseIf Trim(strVPTO) = "" Then
                If (strVPFR < strNOW) Then
                    iIdx = GetItemNo(tabSPF.HeaderString, "PRIO")
                    strPRIO = Trim(GetItem(strLine, iIdx, ","))
    
                    If strPRIO = "1" Then
                        iIdx = GetItemNo(tabSPF.HeaderString, "CODE")
                        strItem = Trim(GetItem(strLine, iIdx, ","))
                        blAlreadyPrio1 = True
                    ElseIf (strPRIO = "") And (blAlreadyPrio1 = False) Then
                        iIdx = GetItemNo(tabSPF.HeaderString, "CODE")
                        strItem = Trim(GetItem(strLine, iIdx, ","))
                        blAlreadyPrio1 = True
                    End If
                End If
            End If
        End If
    Next i

    If strItem <> "" Then
        If InStr(MainForm.FUNCTIONS, strItem) > 0 Then
            CheckHighestPrioFunction = True
        End If
    End If
End Function

Public Function CheckEmpInformed(ByRef strStatus As String) As Boolean
    Dim strTmp As String

    CheckEmpInformed = False

    If Len(strStatus) > 0 Then
        '1. Zeichen gibt an, ob job bereits angetreten
        strTmp = Left(strStatus, 1)
        If strTmp <> "P" Then
            CheckEmpInformed = True
        End If

        strTmp = ""
        If CheckEmpInformed <> True Then
            If Len(strStatus) > 4 Then
                '5. Zeichen gibt an, ob job noch offen
                strTmp = Mid(strStatus, 5, 1)
                If strTmp = 1 Then
                    CheckEmpInformed = True
                End If
            End If
        End If
    End If
End Function

Private Sub tabResult_TimerExpired(ByVal LineNo As Long, ByVal LineStatus As Long)
    Dim llLineNo As Long
    Dim llDestLineNo As Long
    Dim llDestColNo As Long

    llLineNo = LineNo

    'reset the status-value
    tabResult.SetLineStatusValue llLineNo, 0
    
    'calculate the lineno & colno of the display-tab (TAB1)
    llDestLineNo = (llLineNo Mod MainForm.TAB_LINES)
    llDestColNo = (llLineNo \ MainForm.TAB_LINES) * 2

    'reset the cell-objects
    MainForm.TAB1.ResetCellProperty llDestLineNo, llDestColNo
    MainForm.TAB1.ResetCellProperty llDestLineNo, llDestColNo + 1
    MainForm.TAB1.RedrawTab
End Sub

Private Sub Timer1_Timer()
    tabResult.TimerCheck
End Sub

Private Function GetUTCOffset() As Single
    Dim strUtcArr() As String
    Dim strArr() As String
    Dim CurrStr As String
    Dim tmpStr As String

    Dim istrRet As Integer
    Dim count As Integer
    Dim i As Integer

    GetUTCOffset = 0

    aUfis.Twe = sHopo + "," + sTableExt + "," + sAppl
    If aUfis.CallServer("GFR", "", "", "", "[CONFIG]", "360") = 0 Then
        tmpStr = aUfis.GetDataBuffer(True)
    End If
    strArr = Split(tmpStr, Chr(10))
    count = UBound(strArr)
    For i = 0 To count
        CurrStr = strArr(i)
        istrRet = InStr(1, CurrStr, "UTCD", 0)
        If istrRet <> 0 Then
            strUtcArr = Split(strArr(i), ",")
            i = count + 1
            GetUTCOffset = CSng(strUtcArr(1) / 60)
        End If
    Next i
End Function

