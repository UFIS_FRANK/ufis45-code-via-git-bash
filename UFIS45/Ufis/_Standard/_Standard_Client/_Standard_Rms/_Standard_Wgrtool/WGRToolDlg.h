// WGRToolDlg.h : header file
//

#if !defined(AFX_WGRTOOLDLG_H__2D5C9709_94FA_11D4_9702_0050DAE32E8C__INCLUDED_)
#define AFX_WGRTOOLDLG_H__2D5C9709_94FA_11D4_9702_0050DAE32E8C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <ccsglobl.h>
#include <CViewer.h>

/////////////////////////////////////////////////////////////////////////////
// CWGRToolDlg dialog

class CGridFenster;
class CWGRToolDlg : public CDialog
{
// Construction
public:
	CWGRToolDlg(CWnd* pParent = NULL);	// standard constructor
	~CWGRToolDlg();

// Dialog Data
	//{{AFX_DATA(CWGRToolDlg)
	enum { IDD = IDD_WGRTOOL_DIALOG };
	CListBox	m_GroupedList;
	CButton	m_OK;
	CListBox	m_ContentList;
	CListBox	m_InsertList;
	CListBox	m_AllList;
	CButton	m_RemoveButton;
	CButton	m_AddButton;
	CButton	m_SaveGroup;
	CButton	m_HelpButton;
	CButton	m_NewGroup;
	CButton	m_DeleteGroup;
	CComboBox	m_GroupBox;
	CComboBox	m_ViewBox;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWGRToolDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CWGRToolDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDelete();
	afx_msg void OnSelchangeGroupbox();
	afx_msg void OnLocadd();
	afx_msg void OnLocremove();
	afx_msg void OnNew();
	afx_msg void OnSave();
	afx_msg void OnView();
	afx_msg void OnSelchangeViewbox();
	virtual void OnOK();
	afx_msg void OnHelp();
	afx_msg void OnAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private: // helpers
	bool	ChangeViewTo(const CString& opViewName);
	bool	CreateAllList();
	bool	CreateGroupedList();
	bool	ChangeGroupTo(const CString& opGroupName);
	bool	SaveGroup(const CString& opGroupName);
	bool	DeleteGroup(const CString& opGroupName);
	bool	SaveNeccessary();
	bool	GetRecordsToInsert(const CString& opGroupName,CStringArray& opRecordsToInsert);
	bool	GetRecordsToDelete(const CString& opGroupName,CStringArray& opRecordsToDelete);
	bool	CreateGroup(const CString& opGroupName);

private: // implementation data
	CViewer			omViewer;
	CGridFenster*	pomPossilbeList;
	CGridFenster*	pomSelectedList;
	// count of columns to display
	int				imColCount;
	// start index of column(s) to hide
	int				imHideColStart;
	// contains the list of possible items
	CStringArray	omPossibleItems;
	// contains the list of selected items
	CStringArray	omSelectedItems;
	// the current group
	CString			omCurrentGroup;
	// checks whether initialized or not
	bool			bmIsInitialized;
	// index of ALID in WGRTAB record 
	int				imPosALID;
	// index of ALGR in WGRTAB record
	int				imPosALGR;
	// index of ALOC in WGRTAB record
	int				imPosALOC;
	// index of HOPO in WGRTAB record
	int				imPosHOPO;
	// index of ALGR in WGNTAB record
	int				imWGNPosALGR;
	// index of HOPO in WGNTAB record
	int				imWGNPosHOPO;


};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WGRTOOLDLG_H__2D5C9709_94FA_11D4_9702_0050DAE32E8C__INCLUDED_)
