// WGRTool.h : main header file for the WGRTOOL application
//

#if !defined(AFX_WGRTOOL_H__2D5C9707_94FA_11D4_9702_0050DAE32E8C__INCLUDED_)
#define AFX_WGRTOOL_H__2D5C9707_94FA_11D4_9702_0050DAE32E8C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CWGRToolApp:
// See WGRTool.cpp for the implementation of this class
//

class CWGRToolApp : public CWinApp
{
public:
	CWGRToolApp();

	bool	InitialLoad();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWGRToolApp)
	public:
	virtual BOOL InitInstance();
	virtual void WinHelp(DWORD dwData, UINT nCmd = HELP_CONTEXT);
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CWGRToolApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:	
	void	InitializeDefaultView();
};


//***************************************************************************
// About Dialog
//***************************************************************************

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CStatic	m_ClassLibVersion;
	CStatic	m_StcUser;
	CStatic	m_StcServer;
	CStatic	m_StcLogintime;
	CStatic	m_Copyright4;
	CStatic	m_Copyright3;
	CStatic	m_Copyright2;
	CStatic	m_Copyright1;
	CStatic	m_Server;
	CStatic	m_User;
	CStatic	m_Logintime;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WGRTOOL_H__2D5C9707_94FA_11D4_9702_0050DAE32E8C__INCLUDED_)
