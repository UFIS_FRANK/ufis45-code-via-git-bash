// cCfgd.cpp - Class for handling Cfgloyee data
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <ccsptrarray.h>
#include <CCScedacom.h>
#include <CCScedadata.h>
#include <ccsddx.h>
#include <CCSbchandle.h>
#include <ccsddx.h>
#include <basicdata.h>
#include <CedaCfgData.h>
#include <cviewer.h>
#include <resource.h>

//long  igWhatIfUrno;
//CString ogWhatIfKey;

CedaCfgData::CedaCfgData()
{                  
    // Create an array of CEDARECINFO for CFGDATA
    BEGIN_CEDARECINFO(CFGDATA, CfgDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_CHAR_TRIM(Appn,"APPN")
        FIELD_CHAR_TRIM(Ctyp,"CTYP")
        FIELD_CHAR_TRIM(Ckey,"CKEY")
        FIELD_DATE(Vafr,"VAFR")
        FIELD_DATE(Vato,"VATO")
        FIELD_CHAR_TRIM(Pkno,"PKNO")
        FIELD_CHAR_TRIM(Text,"TEXT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CfgDataRecInfo)/sizeof(CfgDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CfgDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
	strcpy(pcmTableName,"VCD");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields, "URNO,APPN,CTYP,CKEY,VAFR,VATO,PKNO,TEXT");
	pcmFieldList = pcmListOfFields;

	sprintf(rmMonitorSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmMonitorSetup.Ctyp, "MONITORS");
	strcpy(rmMonitorSetup.Ckey, "");
	rmMonitorSetup.Vafr = CTime::GetCurrentTime();
	rmMonitorSetup.Vato = CTime(2020, 12, 31, 23, 59, 00);
	strcpy(rmMonitorSetup.Pkno, ogBasicData.omUserID);
	rmMonitorSetup.IsChanged = DATA_NEW;
	sprintf(rmMonitorSetup.Text, "%s=1#%s=1#%s=1#%s=1#%s=1#%s=1#%s=1",
								MON_COUNT_STRING,		
								MON_STAFF_BOARD_STRING,	
								MON_NORESOURCE_STRING,	
								MON_FLIGHTCHANGES_STRING,
								MON_FLIGHTSCHEDULE_STRING,
								MON_COVERAGE_STRING,		
								MON_RULES_STRING,
								MON_DUTYROSTER_STRING);
}

CedaCfgData::~CedaCfgData()
{
	ogDdx.UnRegister(this,NOTUSED);
	omData.DeleteAll();
	//omViews.DeleteAll();
	omUrnoMap.RemoveAll();
	omCkeyMap.RemoveAll();
	omRecInfo.DeleteAll();
	ClearAllViews();
}

bool CedaCfgData::ReadCfgData()
{
    char pclWhere[512];
	bool ilRc = true;

    // Select data from the database
	//MWO TO DO CCS_FPMS ==> später APPLNAME nehmen
/*    sprintf(pclWhere, 
		    "WHERE APPN='CCS_FPMS' AND (CTYP = 'WHAT-IF' OR CTYP = 'CKI-SETUP' OR CTYP = 'WIFLFNDR')");
    if (CedaAction("RT", pclWhere) == false)
        return false;

// this section is responsible for WHATIF and SETUP
    // Load data from CedaData into the dynamic array of record
    omCkeyMap.RemoveAll();
	omUrnoMap.RemoveAll();
    omData.DeleteAll();

    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		CFGDATA *prlCfg = new CFGDATA;
		if ((ilRc = GetBufferRecord(ilLc,prlCfg)) == true)
		{
			PrepareCfgData(prlCfg);
			omData.Add(prlCfg);
			omCkeyMap.SetAt(prlCfg->Ckey,prlCfg);
			omUrnoMap.SetAt((void *)prlCfg->Urno,prlCfg);
		}
		else
		{
			delete prlCfg;
		}
	}

	MakeCurrentUser(); 
*/
// this section is responsible for the VIEWS in charts and lists
	ilRc = true;
    omCkeyMap.RemoveAll();
	omUrnoMap.RemoveAll();
    omData.DeleteAll();

    // Select data from the database
	//MWO TO DO CCS_FPMS ==> später APPLNAME nehmen
    sprintf(pclWhere, "WHERE APPN='CCS_%s' AND (CTYP = 'VIEW-DATA' AND PKNO = '%s')", ogAppName, ogBasicData.omUserID);

	
	if (CedaAction("RT", pclWhere) == false)
        return false;

	//Initialize the datastructure of for the views
	ClearAllViews();
    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		CFGDATA rlCfg;
		if ((ilRc = GetBufferRecord(ilLc,&rlCfg)) == true)
		{
			PrepareViewData(&rlCfg);
		}
	}

    return true;
}

bool CedaCfgData::ReadMonitorSetup()
{
	char pclWhere[2048]="";
	bool ilRc = true;
    sprintf(pclWhere, "WHERE APPN='CCS_%s' AND (CTYP = 'MONITORS' AND PKNO = '%s')", ogAppName, ogBasicData.omUserID);

	rmMonitorSetup.Urno = ogBasicData.GetNextUrno();
	sprintf(rmMonitorSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmMonitorSetup.Ctyp, "MONITORS");
	strcpy(rmMonitorSetup.Ckey, "");
	rmMonitorSetup.Vafr = CTime::GetCurrentTime();
	rmMonitorSetup.Vato = CTime(2020, 12, 31, 23, 59, 00);
	strcpy(rmMonitorSetup.Pkno, ogBasicData.omUserID);
	rmMonitorSetup.IsChanged = DATA_NEW;
	sprintf(rmMonitorSetup.Text, "%s=1#%s=1#%s=1#%s=1#%s=1#%s=1#%s=1",
								MON_COUNT_STRING,		
								MON_STAFF_BOARD_STRING,	
								MON_NORESOURCE_STRING,	
								MON_FLIGHTCHANGES_STRING,
								MON_FLIGHTSCHEDULE_STRING,
								MON_COVERAGE_STRING,		
								MON_RULES_STRING,
								MON_DUTYROSTER_STRING);
	
	if (CedaAction("RT", pclWhere) == false)
        return false;

	//Initialize the datastructure of for the views
	CFGDATA *prlCfg = new CFGDATA;
	bool blFound = true;
    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		if ((ilRc = GetBufferRecord(ilLc,prlCfg)) == false)
		{
			blFound = false;
/*			rmMonitorSetup.Urno = ogBasicData.GetNextUrno();
			sprintf(rmMonitorSetup.Appn, "CCS_%s", ogAppName);
			strcpy(rmMonitorSetup.Ctyp, "MONITORS");
			strcpy(rmMonitorSetup.Ckey, "");
			rmMonitorSetup.Vafr = CTime::GetCurrentTime();
			rmMonitorSetup.Vato = CTime(2020, 12, 31, 23, 59, 00);
			strcpy(rmMonitorSetup.Pkno, ogBasicData.omUserID);
			rmMonitorSetup.IsChanged = DATA_NEW;
			sprintf(rmMonitorSetup.Text, "%s=1#%s=1#%s=1#%s=1#%s=1#%s=1#%s=1",
										MON_COUNT_STRING,		
										MON_STAFF_BOARD_STRING,	
										MON_NORESOURCE_STRING,	
										MON_FLIGHTCHANGES_STRING,
										MON_FLIGHTSCHEDULE_STRING,
										MON_COVERAGE_STRING,		
										MON_RULES_STRING,
										MON_DUTYROSTER_STRING);
*/
		}
		else
		{
			rmMonitorSetup = *prlCfg;
			rmMonitorSetup.IsChanged = DATA_UNCHANGED;
		}
	}
	delete prlCfg;
    return true;
}
int CedaCfgData::GetMonitorForWindow(CString opWindow)
{
	int ilRet = 1;
	CStringArray olSetupList;
	ExtractItemList(CString(rmMonitorSetup.Text), &olSetupList, '#');
	for(int i = 0; i < olSetupList.GetSize(); i++)
	{
		if(olSetupList[i].Find(opWindow) != -1)
		{
			int ilPos = olSetupList[i].Find("=");
			if(ilPos != -1)
			{
				char pclTmp[100];
				char x[111];
				CString olString = olSetupList[i];
				strcpy(x, olString);
				sprintf(pclTmp, "%c", olString[ilPos+1]);
				ilRet = atoi(pclTmp);
			}
		}
	}
	return ilRet;
}

int CedaCfgData::GetMonitorCount()
{
	int ilRet = 1;
	CStringArray olSetupList;
	ExtractItemList(CString(rmMonitorSetup.Text), &olSetupList, '#');
	for(int i = 0; i < olSetupList.GetSize(); i++)
	{
		if(olSetupList[i].Find(MON_COUNT_STRING) != -1)
		{
			int ilPos = olSetupList[i].Find("=");
			if(ilPos != -1)
			{
				char pclTmp[100];
				CString olString = olSetupList[i];
				sprintf(pclTmp, "%c", olString[ilPos+1]);
				ilRet = atoi(pclTmp);
			}
		}
	}
	return ilRet;
}

bool CedaCfgData::ReadConflicts(CStringArray &opLines)
{
	char pclWhere[1024]="";
	int ilLc = 0;

	opLines.RemoveAll();
	bool ilRc = true;
	bool olRc = true;
//Now we read the conflict configuration
	ilRc = true;

    // Select data from the database
    sprintf(pclWhere, "WHERE APPN='CCS_%s' AND (CTYP LIKE 'CONFLICT%%' AND PKNO = '%s') ORDER BY CTYP", ogAppName,ogBasicData.omUserID);
    olRc = CedaAction("RT", pclWhere);

    for (ilLc = 0; ilRc == true; ilLc++)
    {
		CFGDATA rlCfg;
		if ((ilRc = GetBufferRecord(ilLc,&rlCfg)) == true)
		{
			CString olStr = CString(rlCfg.Text);
			opLines.Add(olStr);
		}
	}
	if((ilLc < 15) || (olRc == false))
	{
		opLines.RemoveAll();
		opLines.Add(CString("0;0;Kein Konflikt;0;0;!;?;65280;8454016;Kein Konflikt"));
		opLines.Add(CString("1;1;Kein Einsatz;90;60;!;?;255;33023;Es ist noch kein Bedarf fuer diesem Flug abgedeckt"));
		opLines.Add(CString("2;2;Unterdeckung;80;0;!;?;33023;16711680;Es ist nur ein Bedarf fuer diesen Flug abgedeckt"));
		opLines.Add(CString("3;4;Unterdeckung;70;10;!;?;8388863;32896;Es sind noch nicht alle Bedarfe abgedeckt"));
		opLines.Add(CString("4;8;Delay;60;3;6;6;65535;65280;Neuer Text"));
		opLines.Add(CString("5;16;Cancellation;40;0;!;?;8388736;65280;Kein Konflikt"));
		opLines.Add(CString("6;32;Diversion;30;0;!;?;8421631;65280;Kein Konflikt"));
		opLines.Add(CString("7;64;Einsatzueberlappung;40;0;!;?;12615680;33023;Überlappung mit nachfolgendem Einsatz"));
		opLines.Add(CString("8;128;Bestätigung;30;0;!;?;4227327;255;Keine Einsatzbestätigung 15 Minuten vor Einsatzbeginn"));
		opLines.Add(CString("9;256;Gate Change;30;120;!;?;4210816;65280;Neues Gate"));
		opLines.Add(CString("10;512;Kein Bedarf;99;0;!;?;16777088;65280;Kein Bedarf"));
		opLines.Add(CString("11;1024;Equipment Change;30;0;!;?;4227327;65280;Anderer Flugzeugtyp"));
		opLines.Add(CString("12;2048;Überbuchung;30;0;!;?;16711680;65280;Anderer Flugzeugtyp"));
		opLines.Add(CString("13;4096;Bedarfsüberdeckung;30;0;!;?;8421376;65280;Anderer Flugzeugtyp"));
		opLines.Add(CString("14;8192;Keine Rückmeldung;30;0;!;?;65535;65280;Anderer Flugzeugtyp"));
		opLines.Add(CString("15;16384;Nicht im Pool;30;0;!;?;32896;65280;Mitarbeiter ist nicht dem Pool zugeteilt"));
	}
    return true;
}

bool CedaCfgData::SaveConflictData(CFGDATA *prpCfg)
{
	CString olListOfData;
	bool olRc = true;
	char pclData[824];
	char pclSelection[1024]="";
	//First we delete the entry
	//MWO TO DO CCS_FPMS durch APPN ersetzen
	sprintf(pclSelection,"WHERE APPN='CCS_%s' AND (PKNO = '%s' AND CTYP = '%s')", ogAppName, ogBasicData.omUserID, prpCfg->Ctyp);
	olRc = CedaAction("DRT",pclSelection);

	//and create it new
	MakeCedaData(olListOfData,prpCfg);
	strcpy(pclData,olListOfData);
	olRc = CedaAction("IRT","","",pclData);

	return olRc;
}

void CedaCfgData::MakeCurrentUser(void)
{
	// Fills the setup struct for the user with current login-USERID
	CCSPtrArray<CFGDATA> olData;
	int ilCountSets = 0;
	int ilCount = omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		CFGDATA rlC = omData[i];
		if((strcmp(rlC.Pkno, ogBasicData.omUserID) == 0) && (strcmp(rlC.Ctyp, "CKI-SETUP") == 0))
		{
			olData.NewAt(0, rlC);
			ilCountSets++;
		}
	}
	
	if(ilCountSets == 2)
	{
		CString olParameters;
		CString olTmp1, olTmp2;
		CFGDATA rlC1 = olData.GetAt(0);
		CFGDATA rlC2 = olData.GetAt(1);
		olTmp1 = rlC1.Text;
		olTmp2 = rlC2.Text;
		olParameters = olTmp1 + olTmp2;
		InterpretSetupString(olParameters, &rmUserSetup);
	}
	olData.DeleteAll();
}

void CedaCfgData::DeleteViewFromDiagram(CString opDiagram, CString olView)
{
	bool olRc = true;
	char pclSelection[1024]="";
	char pclDiagram[100]="";
	char pclViewName[100]="";

	strcpy(pclDiagram, opDiagram);
	strcpy(pclViewName, olView);
	//MWO TO DO CCS_FPMS ==> APPL
	sprintf(pclSelection,"WHERE APPN='CCS_%s' AND (PKNO = '%s' AND CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE '%%VIEW=%s%%')", ogAppName, ogBasicData.omUserID, pclDiagram, pclViewName);
	olRc = CedaAction("DRT",pclSelection);
}

void CedaCfgData::UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName)
{
	bool olRc = true; 
	CString olListOfData;
	char pclSelection[1024]="";
	char pclData[824];
	char pclDiagram[100]="";
	char pclText[401]="";
	char pclTmp[512]="";
	//

	strcpy(pclDiagram, opDiagram);

	MakeCedaString(opViewName);

	sprintf(pclSelection,"WHERE APPN='CCS_%s' AND (PKNO = '%s' AND "
		"CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE 'VIEW=%s%%')", 
		ogAppName, ogBasicData.omUserID, pclDiagram,opViewName);

	
	olRc = CedaAction("DRT",pclSelection);
	// after removing we create a completely new configuration
	int ilC1 = prpView.omNameData.GetSize();

	for(int i = 0; i < ilC1; i++)
	{
		if (opViewName == prpView.omNameData[i].ViewName) 
		{

			int ilC2 = prpView.omNameData[i].omTypeData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(prpView.omNameData[i].omTypeData[j].Type == "FILTER")
				{
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						int ilC3 = prpView.omNameData[i].omTypeData[j].omTextData.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							sprintf(pclText, "VIEW=%s#;", prpView.omNameData[i].ViewName);
							sprintf(pclTmp, "TYPE=%s;", prpView.omNameData[i].omTypeData[j].Type );
							strcat(pclText, pclTmp);
							sprintf(pclTmp, "PAGE=%s;TEXT=", prpView.omNameData[i].omTypeData[j].omTextData[k].Page);
							strcat(pclText, pclTmp);
							pclTmp[0]='\0';
							int ilC4 = prpView.omNameData[i].omTypeData[j].omTextData[k].omValues.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								char pclS[100]="";
								sprintf(pclS, "%s|", prpView.omNameData[i].omTypeData[j].omTextData[k].omValues[l]);
								if (strncmp(pclS,"STARTDATE=",10) && strncmp(pclS,"ENDDATE=",8))
								{
									strcat(pclTmp, pclS);
								}
								if(strlen(pclTmp)>300)
								{
									//we have to devide the row into two parts
									CFGDATA rlCfg2;
									rlCfg2.Urno = ogBasicData.GetNextUrno();
									strcpy(rlCfg2.Ckey, rlCfg.Ckey);
									strcpy(rlCfg2.Pkno, rlCfg.Pkno);
									strcpy(rlCfg2.Ctyp, rlCfg.Ctyp);
									strcpy(rlCfg2.Text, pclText);
									strcat(pclTmp, ";");
									strcat(rlCfg2.Text, pclTmp);
									pclTmp[0]='\0';
									rlCfg2.Vafr = CTime::GetCurrentTime();
									rlCfg2.Vato = rlCfg.Vafr + CTimeSpan(365*10, 0, 0, 0);									//And save the record
									MakeCedaData(olListOfData,&rlCfg2);
									strcpy(pclData,olListOfData);

								
									olRc = CedaAction("IRT","","",pclData);

								}
							}
							rlCfg.Urno = ogBasicData.GetNextUrno();
							strcat(pclTmp, ";");
							strcat(pclText, pclTmp);
							strcpy(rlCfg.Text, pclText);
							rlCfg.Vafr = CTime::GetCurrentTime();
							rlCfg.Vato = rlCfg.Vafr + CTimeSpan(365*10, 0, 0, 0);							//and save to database
							MakeCedaData(olListOfData,&rlCfg);
							strcpy(pclData,olListOfData);

							olRc = CedaAction("IRT","","",pclData);

							rlCfg.IsChanged = DATA_UNCHANGED;
							pclText[0]='\0';
						}
					}
				}
				else //not filter but ==> Group, Sort etc.
				{
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						rlCfg.Urno = ogBasicData.GetNextUrno();
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						sprintf(pclText, "VIEW=%s#;", prpView.omNameData[i].ViewName);
						sprintf(pclTmp, "TYPE=%s;TEXT=", prpView.omNameData[i].omTypeData[j].Type );
						strcat(pclText, pclTmp);
						pclTmp[0]='\0';
						int ilC3 = prpView.omNameData[i].omTypeData[j].omValues.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							char pclS[100]="";
							sprintf(pclS, "%s|", prpView.omNameData[i].omTypeData[j].omValues[k]);
							strcat(pclTmp, pclS);
						}
						strcat(pclTmp, ";");
						strcat(pclText, pclTmp);
						strcpy(rlCfg.Text, pclText);
						rlCfg.Vafr = CTime::GetCurrentTime();
						rlCfg.Vato = rlCfg.Vafr + CTimeSpan(365*10, 0, 0, 0);						//and save to database
						MakeCedaData(olListOfData,&rlCfg);
						strcpy(pclData,olListOfData);

						olRc = CedaAction("IRT","","",pclData);

						rlCfg.IsChanged = DATA_UNCHANGED;
						pclText[0]='\0';
					}
				}
			}
		i = ilC1;
		}
	}
}

/******************************
void CedaCfgData::UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[2024]="";
	char pclData[2048];
	char pclDiagram[100]="";
	char pclText[2024]="";
	char pclTmp[2024]="";
	//

	strcpy(pclDiagram, opDiagram);

	

	// to make it easier we delete all rows of the user and his configuration for the
	// specified diagram
	//MWO TO DO CCS_FPMS ==> APPL
	sprintf(pclSelection,"WHERE APPN='CCS_%s' AND (PKNO = '%s' AND "
		"CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE 'VIEW=%s%%')", 
		ogAppName, ogBasicData.omUserID, pclDiagram,opViewName);

	
	olRc = CedaAction("DRT",pclSelection);
	// after removing we create a completely new configuration
	int ilC1 = prpView.omNameData.GetSize();

	for(int i = 0; i < ilC1; i++)
	{
		if (opViewName == prpView.omNameData[i].ViewName) 
		{

			int ilC2 = prpView.omNameData[i].omTypeData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(prpView.omNameData[i].omTypeData[j].Type == "FILTER")
				{
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						int ilC3 = prpView.omNameData[i].omTypeData[j].omTextData.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							sprintf(pclText, "VIEW=%s#", prpView.omNameData[i].ViewName);
							sprintf(pclTmp, "TYPE=%s#", prpView.omNameData[i].omTypeData[j].Type );
							strcat(pclText, pclTmp);
							sprintf(pclTmp, "PAGE=%s#TEXT=", prpView.omNameData[i].omTypeData[j].omTextData[k].Page);
							strcat(pclText, pclTmp);
							pclTmp[0]='\0';
							int ilC4 = prpView.omNameData[i].omTypeData[j].omTextData[k].omValues.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								char pclS[1800]="";
								sprintf(pclS, "%s@", prpView.omNameData[i].omTypeData[j].omTextData[k].omValues[l]);
								strcat(pclTmp, pclS);
								if(strlen(pclTmp)>1800)
								{
									//we have to devide the row into two parts
									CFGDATA rlCfg2;
									rlCfg2.Urno = ogBasicData.GetNextUrno();
									strcpy(rlCfg2.Ckey, rlCfg.Ckey);
									strcpy(rlCfg2.Pkno, rlCfg.Pkno);
									strcpy(rlCfg2.Ctyp, rlCfg.Ctyp);
									strcpy(rlCfg2.Text, pclText);
									strcat(pclTmp, "#");
									strcat(rlCfg2.Text, pclTmp);
									pclTmp[0]='\0';
									//And save the record
									MakeCedaData(olListOfData,&rlCfg2);
									strcpy(pclData,olListOfData);
									olRc = CedaAction("IRT","","",pclData);
								}
							}
							rlCfg.Urno = ogBasicData.GetNextUrno();
							strcat(pclTmp, "#");
							strcat(pclText, pclTmp);
							strcpy(rlCfg.Text, pclText);
							rlCfg.Vafr = CTime::GetCurrentTime();
							rlCfg.Vato = rlCfg.Vafr + CTimeSpan(365*10, 0, 0, 0);
							//and save to database
							MakeCedaData(olListOfData,&rlCfg);
							strcpy(pclData,olListOfData);
							olRc = CedaAction("IRT","","",pclData);
							rlCfg.IsChanged = DATA_UNCHANGED;
							pclText[0]='\0';
						}
					}
				}
				else //not filter but ==> Group, Sort etc.
				{
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						rlCfg.Urno = ogBasicData.GetNextUrno();
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						sprintf(pclText, "VIEW=%s#", prpView.omNameData[i].ViewName);
						sprintf(pclTmp, "TYPE=%s#TEXT=", prpView.omNameData[i].omTypeData[j].Type );
						strcat(pclText, pclTmp);
						pclTmp[0]='\0';
						int ilC3 = prpView.omNameData[i].omTypeData[j].omValues.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							char pclS[100]="";
							sprintf(pclS, "%s|", prpView.omNameData[i].omTypeData[j].omValues[k]);
							strcat(pclTmp, pclS);
						}
						strcat(pclTmp, "#");
						strcat(pclText, pclTmp);
						strcpy(rlCfg.Text, pclText);
						//and save to database
						MakeCedaData(olListOfData,&rlCfg);
						strcpy(pclData,olListOfData);
						olRc = CedaAction("IRT","","",pclData);
						rlCfg.IsChanged = DATA_UNCHANGED;
						pclText[0]='\0';
					}
				}
			}
		i = ilC1;
		}
	}
}

  *************************/
////////////////////////////////////////////////////////////////
// Interprets the raw data for one propertypage

BOOL CedaCfgData::MakeRawViewData(CFGDATA *prpCfg, RAW_VIEWDATA *prpRawData)
{
	strcpy(prpRawData->Ckey, prpCfg->Ckey);
	char *psp = NULL;
	char pclOriginal[1024]="";
	char pclVIEW[100]="";
	char pclTYPE[100]="";
	char pclPAGE[100]="";
	char pclTEXT[1024]="";

	strcpy(pclOriginal, prpCfg->Text);
	psp = strstr(pclOriginal, ";");
	while(psp != NULL)
	{
		char *pclTmp;
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if((pclTmp = strstr(pclPart, "VIEW=")) != NULL)
			strcpy(pclVIEW, pclPart);
		else if((pclTmp = strstr(pclPart, "TYPE=")) != NULL)
			strcpy(pclTYPE, pclPart);
		else if((pclTmp = strstr(pclPart, "PAGE=")) != NULL)
			strcpy(pclPAGE, pclPart);
		else if((pclTmp = strstr(pclPart, "TEXT=")) != NULL)
			strcpy(pclTEXT, pclPart);

		psp = strstr(pclOriginal, ";");
	}

	psp = strstr(pclTYPE, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Type, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclVIEW, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal,psp);
		if (pclOriginal[strlen(pclOriginal)-1] == '#')
		{
			pclOriginal[strlen(pclOriginal)-1] = '\0';
		}
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Name, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclPAGE, "=");
	if(psp == NULL)
	{
		if(prpRawData->Type == "FILTER")
		{
			return FALSE;
		}
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Page, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclTEXT, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Values, pclOriginal);
		}
	}
	psp = NULL;

	return TRUE;
}
/*****************************
BOOL CedaCfgData::MakeRawViewData(CFGDATA *prpCfg, RAW_VIEWDATA *prpRawData)
{
	strcpy(prpRawData->Ckey, prpCfg->Ckey);
	char *psp = NULL;
	char pclOriginal[2024]="";
	char pclVIEW[100]="";
	char pclTYPE[100]="";
	char pclPAGE[100]="";
	char pclTEXT[1024]="";

	strcpy(pclOriginal, prpCfg->Text);
	psp = strstr(pclOriginal, "#");
	while(psp != NULL)
	{
		char *pclTmp;
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if((pclTmp = strstr(pclPart, "VIEW=")) != NULL)
			strcpy(pclVIEW, pclPart);
		else if((pclTmp = strstr(pclPart, "TYPE=")) != NULL)
			strcpy(pclTYPE, pclPart);
		else if((pclTmp = strstr(pclPart, "PAGE=")) != NULL)
			strcpy(pclPAGE, pclPart);
		else if((pclTmp = strstr(pclPart, "TEXT=")) != NULL)
			strcpy(pclTEXT, pclPart);

		psp = strstr(pclOriginal, "#");
	}

	psp = strstr(pclTYPE, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Type, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclVIEW, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Name, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclPAGE, "=");
	if(psp == NULL)
	{
		if(prpRawData->Type == "FILTER")
		{
			return FALSE;
		}
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Page, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclTEXT, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Values, pclOriginal);
		}
	}
	psp = NULL;

	return TRUE;
}
********************/
////////////////////////////////////////////////////////////////
//MWO: extracts the values which are comma-separated and copies them
//     into CStringArray of VIEW_TEXTDATA

// FILTER: pcpSepa = "@"
// SONST   pcpSepa = "|"
void CedaCfgData::MakeViewValues(char *pspValues, CCSPtrArray<CString> *popValues)
{
	char *psp = NULL;
	char pclOriginal[1024]="";
	char pclValue[100]="";

	strcpy(pclOriginal, pspValues);
	psp = strstr(pclOriginal, "|");
	while(psp != NULL)
	{
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if(strcmp(pclPart, "") != 0)
			if (strncmp(pclPart,"STARTDATE=",10))
				if (strncmp(pclPart,"ENDDATE=",8))
					popValues->NewAt(popValues->GetSize(), pclPart);

		psp = strstr(pclOriginal, "|");
	}
}
/************************
void CedaCfgData::MakeViewValues(char *pspValues, CCSPtrArray<CString> *popValues, char *pcpSepa )
{
	char *psp = NULL;
	char pclOriginal[2024]="";
	char pclValue[100]="";

	strcpy(pclOriginal, pspValues);
	psp = strstr(pclOriginal, pcpSepa);
	while(psp != NULL)
	{
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if(strcmp(pclPart, "") != 0)
			 popValues->NewAt(popValues->GetSize(), pclPart);

		psp = strstr(pclOriginal, pcpSepa);
	}
}

************************/
////////////////////////////////////////////////////////////////
// MWO: prepares the nested arrays for the CFGDATA
void CedaCfgData::PrepareViewData(CFGDATA *prpCfg)
{
	VIEWDATA *prlFoundViewData;
	VIEW_VIEWNAMES *prlFoundViewName;
	VIEW_TYPEDATA *prlFoundTypeData;
	VIEW_TEXTDATA *prlFoundTextData;
	RAW_VIEWDATA rlRawData;

	if(MakeRawViewData(prpCfg, &rlRawData) != TRUE)
	{
		return; //then the row is corrupt
	}
	prlFoundViewData = FindViewData(prpCfg);
	if(prlFoundViewData == NULL) 
	{
		//if the key does not exist we have generate an new on one from the root
		VIEW_TEXTDATA rlTextData;
		VIEW_TYPEDATA rlTypeData;
		VIEW_VIEWNAMES rlNameData;
		VIEWDATA rlViewData;
		rlViewData.Ckey = CString(rlRawData.Ckey);
		rlNameData.ViewName = CString(rlRawData.Name);
		rlTypeData.Type = CString(rlRawData.Type);
		if(strcmp(rlRawData.Type, "FILTER") == 0)
		{
			rlTextData.Page = CString(rlRawData.Page);
			MakeViewValues(rlRawData.Values, &rlTextData.omValues);
			rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
		}
		else
		{
			MakeViewValues(rlRawData.Values, &rlTypeData.omValues);
		}
		rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
		rlViewData.omNameData.NewAt(rlViewData.omNameData.GetSize(), rlNameData);
		omViews.NewAt(omViews.GetSize(), rlViewData);
	}
	else
	{
		//the key exists, we have to look for the rest of the tree and
		//evaluate, whether parts of it do exist and merge them together
		//Now first let's look for existing of the view-name
		prlFoundViewName = FindViewNameData(&rlRawData);
		if(prlFoundViewName == NULL)
		{
			//the view itself exists but there are no entries

			VIEW_TEXTDATA rlTextData;
			VIEW_TYPEDATA rlTypeData;
			VIEW_VIEWNAMES rlNameData;
			rlNameData.ViewName = CString(rlRawData.Name);
			rlTypeData.Type = CString(rlRawData.Type);
			if(strcmp(rlRawData.Type, "FILTER") == 0)
			{
				rlTextData.Page = CString(rlRawData.Page);
				MakeViewValues(rlRawData.Values, &rlTextData.omValues);
				rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
			}
			else
			{
				MakeViewValues(rlRawData.Values, &rlTypeData.omValues);
			}
			rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
			prlFoundViewData->omNameData.NewAt(prlFoundViewData->omNameData.GetSize(), rlNameData);
		}
		else
		{
			//there are name entries => let's have a look at typs and their data
			prlFoundTypeData = FindViewTypeData(&rlRawData);
			if(prlFoundTypeData == NULL) //***** OK
			{
				//View + name exist but no Type entries
				VIEW_TYPEDATA rlTypeData;
				VIEW_TEXTDATA rlTextData;
				rlTypeData.Type = CString(rlRawData.Type);
				if(strcmp(rlRawData.Type, "FILTER") == 0)
				{
					rlTextData.Page = CString(rlRawData.Page);
					MakeViewValues(rlRawData.Values, &rlTextData.omValues);
					rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
				}
				else
				{
					MakeViewValues(rlRawData.Values, &rlTypeData.omValues);
				}
				prlFoundViewName->omTypeData.NewAt(prlFoundViewName->omTypeData.GetSize(), rlTypeData);
			}
			else
			{
				if(rlRawData.Type == CString("FILTER"))//***
				{   
					prlFoundTextData = FindViewTextData(&rlRawData);
					if(prlFoundTextData == NULL) //*** OK
					{
						//View + Name + Type exist but no values for Filter
						VIEW_TEXTDATA rlTextData;
						rlTextData.Page = CString(rlRawData.Page);
						MakeViewValues(rlRawData.Values, &rlTextData.omValues);
						prlFoundTypeData->omTextData.NewAt(prlFoundTypeData->omTextData.GetSize(), rlTextData);
					}
					else
					{ 
						MakeViewValues(rlRawData.Values, &prlFoundTextData->omValues);
					}
				}
				else
				{

				}
			}
		}
	}
}
/***********************
void CedaCfgData::PrepareViewData(CFGDATA *prpCfg)
{
	VIEWDATA *prlFoundViewData;
	VIEW_VIEWNAMES *prlFoundViewName;
	VIEW_TYPEDATA *prlFoundTypeData;
	VIEW_TEXTDATA *prlFoundTextData;
	RAW_VIEWDATA rlRawData;

	if(MakeRawViewData(prpCfg, &rlRawData) != TRUE)
	{
		return; //then the row is corrupt
	}
	prlFoundViewData = FindViewData(prpCfg);
	if(prlFoundViewData == NULL) 
	{
		//if the key does not exist we have generate an new on one from the root
		VIEW_TEXTDATA rlTextData;
		VIEW_TYPEDATA rlTypeData;
		VIEW_VIEWNAMES rlNameData;
		VIEWDATA rlViewData;
		rlViewData.Ckey = CString(rlRawData.Ckey);
		rlNameData.ViewName = CString(rlRawData.Name);
		rlTypeData.Type = CString(rlRawData.Type);
		if(strcmp(rlRawData.Type, "FILTER") == 0)
		{
			rlTextData.Page = CString(rlRawData.Page);
			MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
			rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
		}
		else
		{
			MakeViewValues(rlRawData.Values, &rlTypeData.omValues, "|");
		}
		rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
		rlViewData.omNameData.NewAt(rlViewData.omNameData.GetSize(), rlNameData);
		omViews.NewAt(omViews.GetSize(), rlViewData);

	}
	else
	{

		//the key exists, we have to look for the rest of the tree and
		//evaluate, whether parts of it do exist and merge them together
		//Now first let's look for existing of the view-name
		prlFoundViewName = FindViewNameData(&rlRawData);
		if(prlFoundViewName == NULL)
		{
			//the view itself exists but there are no entries

			VIEW_TEXTDATA rlTextData;
			VIEW_TYPEDATA rlTypeData;
			VIEW_VIEWNAMES rlNameData;
			rlNameData.ViewName = CString(rlRawData.Name);
			rlTypeData.Type = CString(rlRawData.Type);
			if(strcmp(rlRawData.Type, "FILTER") == 0)
			{
				rlTextData.Page = CString(rlRawData.Page);
				MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
				rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
			}
			else
			{
				MakeViewValues(rlRawData.Values, &rlTypeData.omValues, "|");
			}
			rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
			prlFoundViewData->omNameData.NewAt(prlFoundViewData->omNameData.GetSize(), rlNameData);
		}
		else
		{
			//there are name entries => let's have a look at typs and their data
			prlFoundTypeData = FindViewTypeData(&rlRawData);
			if(prlFoundTypeData == NULL) //***** OK
			{
				//View + name exist but no Type entries
				VIEW_TYPEDATA rlTypeData;
				VIEW_TEXTDATA rlTextData;
				rlTypeData.Type = CString(rlRawData.Type);
				if(strcmp(rlRawData.Type, "FILTER") == 0)
				{
					rlTextData.Page = CString(rlRawData.Page);
					MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
					rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
				}
				else
				{
					MakeViewValues(rlRawData.Values, &rlTypeData.omValues, "|");
				}
				prlFoundViewName->omTypeData.NewAt(prlFoundViewName->omTypeData.GetSize(), rlTypeData);
			}
			else
			{
				if(rlRawData.Type == CString("FILTER"))//***
				{   
					prlFoundTextData = FindViewTextData(&rlRawData);
					if(prlFoundTextData == NULL) //*** OK
					{
						//View + Name + Type exist but no values for Filter
						VIEW_TEXTDATA rlTextData;
						rlTextData.Page = CString(rlRawData.Page);
						MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
						prlFoundTypeData->omTextData.NewAt(prlFoundTypeData->omTextData.GetSize(), rlTextData);
					}
					else
					{ 
						MakeViewValues(rlRawData.Values, &prlFoundTextData->omValues, "@");
					}
				}
				else
				{

				}
			}
		}
	}
			
}

*********************/
/////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of a view, e.g. Staffdia
VIEWDATA * CedaCfgData::FindViewData(CFGDATA *prlCfg)
{
	int ilCount = omViews.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prlCfg->Ckey)
		{
			return &omViews[i];
		}
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////////////
//MWO: Evaluates the existing of a viewname, e.g. Heute, Morgen
VIEW_VIEWNAMES * CedaCfgData::FindViewNameData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == prpRawData->Name)
				{	
					return &omViews[i].omNameData[j];
				}
			}
		}
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of a viewtyp, e.g. FILTER, GROUP
VIEW_TYPEDATA * CedaCfgData::FindViewTypeData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == CString(prpRawData->Name))
				{	
					int ilC3 = omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(omViews[i].omNameData[j].omTypeData[k].Type == CString(prpRawData->Type))
						{
							return  &omViews[i].omNameData[j].omTypeData[k];
						}
					}
				}
			}
		}
	}
	return NULL;
}
////////////////////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of values for FILTER
VIEW_TEXTDATA * CedaCfgData::FindViewTextData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == CString(prpRawData->Name))
				{	
					int ilC3 = omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(omViews[i].omNameData[j].omTypeData[k].Type == CString(prpRawData->Type))
						{
							int ilC4 = omViews[i].omNameData[j].omTypeData[k].omTextData.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								if(omViews[i].omNameData[j].omTypeData[k].omTextData[l].Page == CString(prpRawData->Page))
								{
									return &omViews[i].omNameData[j].omTypeData[k].omTextData[l];
								}
							}
						}
					}
				}
			}
		}
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////
//MWO: Clear all buffers allocated in omViews
void CedaCfgData::ClearAllViews()
{
	while(omViews.GetSize() > 0)
	{
	//	VIEWDATA *prlView = &omViews[0];
		while(omViews[0].omNameData.GetSize() > 0)
		{
	//		VIEW_VIEWNAMES *prlViewName = &omViews[0].omNameData[0];
			while(omViews[0].omNameData[0].omTypeData.GetSize() > 0)
			{
	//			VIEW_TYPEDATA *prlViewType = &omViews[0].omNameData[0].omTypeData[0];
				while(omViews[0].omNameData[0].omTypeData[0].omTextData.GetSize() > 0)
				{
	//				VIEW_TEXTDATA *prlViewText = &omViews[0].omNameData[0].omTypeData[0].omTextData[0];
					while(omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues.GetSize() > 0)
					{
						CString *prlText = &omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues[0];
						omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues.DeleteAt(0);
					}
					omViews[0].omNameData[0].omTypeData[0].omTextData.DeleteAt(0);
				}
			//	omViews[0].omNameData[0].omTypeData[0].omValues.DeleteAll();
			
				while(omViews[0].omNameData[0].omTypeData[0].omValues.GetSize() > 0)
				{
					omViews[0].omNameData[0].omTypeData[0].omValues.DeleteAt(0);
				}
			
				omViews[0].omNameData[0].omTypeData.DeleteAt(0);
			}
			omViews[0].omNameData.DeleteAt(0);
		}
		omViews.DeleteAt(0);
	}
}
// Prepare some whatif data, not read from database
void CedaCfgData::PrepareCfgData(CFGDATA *prpCfg)
{
}


/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class and StaffTable class)

bool CedaCfgData::InsertCfg(const CFGDATA *prpCfgData)
{

    if (CfgExist(prpCfgData->Urno))
	{
        return (UpdateCfgRecord(prpCfgData));
	}
    else
	{
        return (InsertCfgRecord(prpCfgData));
	}
}

bool CedaCfgData::UpdateCfg(const CFGDATA *prpCfgData)
{
    return(UpdateCfgRecord(prpCfgData));
}


BOOL CedaCfgData::CfgExist(long lpUrno)
{
	// don't read from database anymore, just check internal array
	CFGDATA *prpData;

	return(omUrnoMap.Lookup((void *) &lpUrno,(void *&)prpData) );
}

bool CedaCfgData::InsertCfgRecord(const CFGDATA *prpCfgData)
{
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "URNO,CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // CFGCKI ~ 430 Byte
    sprintf(CfgData, "%ld,%s,%s,%s,%s,%s,%s,%s",
        prpCfgData->Urno, 
		prpCfgData->Appn,
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Pkno,
		prpCfgData->Text);

   return (CedaAction("IRT", "CFGCKI", CfgFields, "", "", CfgData));
}

bool CedaCfgData::UpdateCfgRecord(const CFGDATA *prpCfgData)
{
    // update Cfgl data in table "SHFCKI"
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // SHFCKI ~ 140 byte
	char pclSelection[64];
    sprintf(CfgData, "%s,%s,%s,%s,%s,%s,%s",
		prpCfgData->Appn,
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Pkno,
		prpCfgData->Text);
	sprintf(pclSelection," where URNO = '%ld%'",prpCfgData->Urno);
	bool olRc = CedaAction("URT", "CFGCKI", CfgFields, pclSelection, "", CfgData);
	//ogDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfgData);
	return olRc;
}


bool CedaCfgData::CreateCfgRequest(const CFGDATA *prpCfgData)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
    // update Cfgl data in table "SHFCKI"
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // SHFCKI ~ 140 byte
	char pclSelection[64];
    sprintf(CfgData, "%s,%s,%s,%s,%s,%s",
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Text,
		prpCfgData->Pkno);
	sprintf(pclSelection,"'%s'",prpCfgData->Ckey);
	bool olRc = CedaAction("CKO", "CFGCKI", CfgFields, pclSelection, "", CfgData);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	if (olRc == true)
	{
		MessageBox(NULL,CfgData,LoadStg(IDS_STRING430),MB_OK);
	}
	else
	{
		MessageBox(NULL,LoadStg(IDS_STRING430),LoadStg(IDS_STRING431),MB_OK);
	}
//	ogDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfgData);
	return olRc;
}


// the callback function for storing broadcasted fligthdata in FLIGHTDATA array

void  ProcessCfgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	 ogCfgData.ProcessCfgBc(ipDDXType,vpDataPointer,ropInstanceName);
}


void  CedaCfgData::ProcessCfgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	if ((ipDDXType == BC_CFG_CHANGE) || (ipDDXType == BC_CFG_INSERT))
	{
		CFGDATA *prpCfg;
		struct BcStruct *prlCfgData;

		prlCfgData = (struct BcStruct *) vpDataPointer;
		long llUrno = GetUrnoFromSelection(prlCfgData->Selection);

		if (omUrnoMap.Lookup((void *)llUrno,(void *& )prpCfg) == TRUE)
		{
			GetRecordFromItemList(prpCfg,
				prlCfgData->Fields,prlCfgData->Data);
			ogDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfg);
		}
		else
		{
			prpCfg = new CFGDATA;
			GetRecordFromItemList(prpCfg,
				prlCfgData->Fields,prlCfgData->Data);
			PrepareCfgData(prpCfg);
			omData.Add(prpCfg);
			omCkeyMap.SetAt(prpCfg->Ckey,prpCfg);
			omUrnoMap.SetAt((void *)prpCfg->Urno,prpCfg);
			ogDdx.DataChanged((void *)this,CFG_INSERT,(void *)prpCfg);
		}
	}
}

long  CedaCfgData::GetUrnoById(char *pclWiid)
{
	int ilWhatifCount = omData.GetSize();
	for ( int i = 0; i < ilWhatifCount; i++)
	{
		if (strcmp(omData[i].Ckey,pclWiid) == 0)
		{
			return omData[i].Urno;
		}
	}
	return 0L;
}

BOOL  CedaCfgData::GetIdByUrno(long lpUrno,char *pcpWiid)
{
	CFGDATA  *prlCfg;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCfg) == TRUE)
	{
		strcpy(pcpWiid,prlCfg->Ckey);
		return TRUE;
	}
	return FALSE;
}


CFGDATA  *CedaCfgData::GetCfgByUrno(long lpUrno)
{
	CFGDATA  *prlCfg;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCfg) == TRUE)
	{
		return prlCfg;
	}
	return NULL;
}

bool CedaCfgData::AddCfg(CFGDATA *prpCfg)
{
	CFGDATA *prlCfg = new CFGDATA;
	memcpy(prlCfg,prpCfg,sizeof(CFGDATA));
	prlCfg->IsChanged = DATA_NEW;

	omData.Add(prlCfg);
	omUrnoMap.SetAt((void *)prlCfg->Urno, prlCfg);
	omCkeyMap.SetAt(prlCfg->Ckey, prlCfg); 

	ogDdx.DataChanged((void *)this,CFG_INSERT,(void *)prlCfg);
	SaveCfg(prlCfg);
    return true;
}

bool CedaCfgData::ChangeCfgData(CFGDATA *prpCfg)
{
//	int ilLc;

	if (prpCfg->IsChanged == DATA_UNCHANGED)
	{
		prpCfg->IsChanged = DATA_CHANGED;
	}
	SaveCfg(prpCfg);

    return true;
}

bool CedaCfgData::DeleteCfg(long lpUrno)
{

	CFGDATA *prpCfg = GetCfgByUrno(lpUrno);
	if (prpCfg != NULL)
	{
		prpCfg->IsChanged = DATA_DELETED;

		omUrnoMap.RemoveKey((void *)lpUrno);
		omCkeyMap.RemoveKey(prpCfg->Ckey);

		SaveCfg(prpCfg);
	}
    return true;
}

bool CedaCfgData::SaveCfg(CFGDATA *prpCfg)
{

	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[824];

	if ((prpCfg->IsChanged == DATA_UNCHANGED) || (! bgOnline))
	{
		return true; // no change, nothing to do
	}
	switch(prpCfg->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(olListOfData,prpCfg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpCfg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCfg->Urno);
		MakeCedaData(olListOfData,prpCfg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		if (olRc != true)
		{
			prpCfg->IsChanged = DATA_NEW;
			SaveCfg(prpCfg);
		}
		else
		{
			prpCfg->IsChanged = DATA_UNCHANGED;
		}
		break;
	case DATA_DELETED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCfg->Urno);
		olRc = CedaAction("DRT",pclSelection);
		break;
	}

	
    return true;

}

BOOL CedaCfgData::InterpretSetupString(CString popSetupString, 
									   USERSETUPDATA *prpSetupData)
{
	char *psp = NULL;
	char pclOriginal[1024]="";
	char pclStr[1024]="";
	char pclResult[256]="";

	char pclMONS[300]="";
	char pclRESO[300]="";
	char pclGACH[300]="";
	char pclSTCH[300]="";
	char pclCCCH[300]="";
	char pclFPTB[300]="";
	char pclSTTB[300]="";
	char pclGATB[300]="";
	char pclCCTB[300]="";
	char pclRQTB[300]="";
	char pclPKTB[300]="";
	char pclCVTB[300]="";
	char pclVPTB[300]="";
	char pclCFTB[300]="";
	char pclATTB[300]="";
	char pclINFO[300]="";
	char pclLHHS[300]="";
	char pclTACK[300]="";
	char pclFBCK[300]="";
	char pclSBCK[300]="";
	char pclSTCV[300]="";
	char pclGACV[300]="";
	char pclCCCV[300]="";
	char pclSTLV[300]="";
	char pclFPLV[300]="";
	char pclGBLV[300]="";
	char pclCCBV[300]="";
	char pclRQSV[300]="";
	char pclPEAV[300]="";
	char pclVPLV[300]="";
	char pclCONV[300]="";
	char pclATTV[300]="";

	strcpy(pclOriginal, popSetupString);

	// First we have to devide the incomming string into valid Commands

	// the parts of incomming string
	

	psp = strstr(pclOriginal, "#");
	while(psp != NULL)
	{
		char *pclTmp;
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if((pclTmp = strstr(pclPart, "MONS=")) != NULL)
			strcpy(pclMONS, pclPart);
		else if((pclTmp = strstr(pclPart, "RESO=")) != NULL)
			strcpy(pclRESO, pclPart);
		else if((pclTmp = strstr(pclPart, "GACH=")) != NULL)
			strcpy(pclGACH, pclPart);
		else if((pclTmp = strstr(pclPart, "STCH=")) != NULL)
			strcpy(pclSTCH, pclPart);
		else if((pclTmp = strstr(pclPart, "CCCH=")) != NULL)
			strcpy(pclCCCH, pclPart);
		else if((pclTmp = strstr(pclPart, "FPTB=")) != NULL)
			strcpy(pclFPTB, pclPart);
		else if((pclTmp = strstr(pclPart, "STTB=")) != NULL)
			strcpy(pclSTTB, pclPart);
		else if((pclTmp = strstr(pclPart, "GATB=")) != NULL)
			strcpy(pclGATB, pclPart);
		else if((pclTmp = strstr(pclPart, "CCTB=")) != NULL)
			strcpy(pclCCTB, pclPart);
		else if((pclTmp = strstr(pclPart, "RQTB=")) != NULL)
			strcpy(pclRQTB, pclPart);
		else if((pclTmp = strstr(pclPart, "PKTB=")) != NULL)
			strcpy(pclPKTB, pclPart);
		else if((pclTmp = strstr(pclPart, "CVTB=")) != NULL)
			strcpy(pclCVTB, pclPart);
		else if((pclTmp = strstr(pclPart, "VPTB=")) != NULL)
			strcpy(pclVPTB, pclPart);
		else if((pclTmp = strstr(pclPart, "CFTB=")) != NULL)
			strcpy(pclCFTB, pclPart);
		else if((pclTmp = strstr(pclPart, "ATTB=")) != NULL)
			strcpy(pclATTB, pclPart);
		else if((pclTmp = strstr(pclPart, "INFO=")) != NULL)
			strcpy(pclINFO, pclPart);
		else if((pclTmp = strstr(pclPart, "LHHS=")) != NULL)
			strcpy(pclLHHS, pclPart);
		else if((pclTmp = strstr(pclPart, "TACK=")) != NULL)
			strcpy(pclTACK, pclPart);
		else if((pclTmp = strstr(pclPart, "FBCK=")) != NULL)
			strcpy(pclFBCK, pclPart);
		else if((pclTmp = strstr(pclPart, "SBCK=")) != NULL)
			strcpy(pclSBCK, pclPart);
		else if((pclTmp = strstr(pclPart, "STCV=")) != NULL)
			strcpy(pclSTCV, pclPart);
		else if((pclTmp = strstr(pclPart, "GACV=")) != NULL)
			strcpy(pclGACV, pclPart);
		else if((pclTmp = strstr(pclPart, "CCCV=")) != NULL)
			strcpy(pclCCCV, pclPart);
		else if((pclTmp = strstr(pclPart, "STLV=")) != NULL)
			strcpy(pclSTLV, pclPart);
		else if((pclTmp = strstr(pclPart, "FPLV=")) != NULL)
			strcpy(pclFPLV, pclPart);
		else if((pclTmp = strstr(pclPart, "GBLV=")) != NULL)
			strcpy(pclGBLV, pclPart);
		else if((pclTmp = strstr(pclPart, "CCBV=")) != NULL)
			strcpy(pclCCBV, pclPart);
		else if((pclTmp = strstr(pclPart, "RQSV=")) != NULL)
			strcpy(pclRQSV, pclPart);
		else if((pclTmp = strstr(pclPart, "PEAV=")) != NULL)
			strcpy(pclPEAV, pclPart);
		else if((pclTmp = strstr(pclPart, "VPLV=")) != NULL)
			strcpy(pclVPLV, pclPart);
		else if((pclTmp = strstr(pclPart, "CONV=")) != NULL)
			strcpy(pclCONV, pclPart);
		else if((pclTmp = strstr(pclPart, "ATTV=")) != NULL)
			strcpy(pclATTV, pclPart);

		psp = strstr(pclOriginal, "#");

	}


	
	psp = strstr(pclMONS, "=");
	if(psp == NULL)
	{
			prpSetupData->MONS = CString("1");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->MONS = CString("1");
		}
		else
		{
			prpSetupData->MONS =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclRESO, "=");
	if(psp == NULL)
	{
			prpSetupData->RESO = CString("1024x768");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->RESO = CString("1024x768");
		}
		else
		{
			prpSetupData->RESO =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclGACH, "=");
	if(psp == NULL)
	{
			prpSetupData->GACH  = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->GACH  = CString("L");
		}
		else
		{
			prpSetupData->GACH =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSTCH, "=");
	if(psp == NULL)
	{
			prpSetupData->STCH = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->STCH = CString("L");
		}
		else
		{
			prpSetupData->STCH =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCCCH, "=");
	if(psp == NULL)
	{
			prpSetupData->CCCH = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCCH = CString("L");
		}
		else
		{
			prpSetupData->CCCH =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclFPTB, "=");
	if(psp == NULL)
	{
			prpSetupData->FPTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->FPTB = CString("L");
		}
		else
		{
			prpSetupData->FPTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSTTB, "=");
	if(psp == NULL)
	{
			prpSetupData->STTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->STTB = CString("L");
		}
		else
		{
			prpSetupData->STTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclGATB, "=");
	if(psp == NULL)
	{
			prpSetupData->GATB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->GATB = CString("L");
		}
		else
		{
			prpSetupData->GATB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCCTB, "=");
	if(psp == NULL)
	{
			prpSetupData->CCTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCTB = CString("L");
		}
		else
		{
			prpSetupData->CCTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclRQTB, "=");
	if(psp == NULL)
	{
			prpSetupData->RQTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->RQTB = CString("L");
		}
		else
		{
			prpSetupData->RQTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclPKTB, "=");
	if(psp == NULL)
	{
			prpSetupData->PKTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->PKTB = CString("L");
		}
		else
		{
			prpSetupData->PKTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCVTB, "=");
	if(psp == NULL)
	{
			prpSetupData->CVTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CVTB = CString("L");
		}
		else
		{
			prpSetupData->CVTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclVPTB, "=");
	if(psp == NULL)
	{
			prpSetupData->VPTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->VPTB = CString("L");
		}
		else
		{
			prpSetupData->VPTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCFTB, "=");
	if(psp == NULL)
	{
			prpSetupData->CFTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CFTB = CString("L");
		}
		else
		{
			prpSetupData->CFTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclATTB, "=");
	if(psp == NULL)
	{
			prpSetupData->ATTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->ATTB = CString("L");
		}
		else
		{
			prpSetupData->ATTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclINFO, "=");
	if(psp == NULL)
	{
			prpSetupData->INFO = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->INFO = CString("L");
		}
		else
		{
			prpSetupData->INFO =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclLHHS, "=");
	if(psp == NULL)
	{
			prpSetupData->LHHS = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->LHHS = CString("L");
		}
		else
		{
			prpSetupData->LHHS =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclTACK, "=");
	if(psp == NULL)
	{
			prpSetupData->TACK = CString("J");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->TACK = CString("J");
		}
		else
		{
			prpSetupData->TACK =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclFBCK, "=");
	if(psp == NULL)
	{
			prpSetupData->FBCK = CString("J");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->FBCK =  CString(pclOriginal);
		}
		else
		{
			prpSetupData->FBCK =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSBCK, "=");
	if(psp == NULL)
	{
			prpSetupData->SBCK = CString("J");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->SBCK = CString("J");
		}
		else
		{
			prpSetupData->SBCK =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSTCV, "=");
	if(psp == NULL)
	{
			prpSetupData->STCV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->STCV = CString("<Default>");
		}
		else
		{
			prpSetupData->STCV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclGACV, "=");
	if(psp == NULL)
	{
			prpSetupData->GACV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->GACV = CString("<Default>");
		}
		else
		{
			prpSetupData->GACV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCCCV, "=");
	if(psp == NULL)
	{
			prpSetupData->CCCV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCCV = CString("<Default>");
		}
		else
		{
			prpSetupData->CCCV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSTLV, "=");
	if(psp == NULL)
	{
			prpSetupData->STLV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->STLV = CString("<Default>");
		}
		else
		{
			prpSetupData->STLV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclFPLV, "=");
	if(psp == NULL)
	{
			prpSetupData->FPLV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->FPLV = CString("<Default>");
		}
		else
		{
			prpSetupData->FPLV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclGBLV, "=");
	if(psp == NULL)
	{
			prpSetupData->GBLV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->GBLV = CString("<Default>");
		}
		else
		{
			prpSetupData->GBLV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCCBV, "=");
	if(psp == NULL)
	{
			prpSetupData->CCBV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCBV = CString("<Default>");
		}
		else
		{
			prpSetupData->CCBV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclRQSV, "=");
	if(psp == NULL)
	{
			prpSetupData->RQSV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->RQSV = CString("<Default>");
		}
		else
		{
			prpSetupData->RQSV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclPEAV, "=");
	if(psp == NULL)
	{
			prpSetupData->PEAV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->PEAV = CString("<Default>");
		}
		else
		{
			prpSetupData->PEAV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclVPLV, "=");
	if(psp == NULL)
	{
			prpSetupData->VPLV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->VPLV = CString("<Default>");
		}
		else
		{
			prpSetupData->VPLV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCONV, "=");
	if(psp == NULL)
	{
			prpSetupData->CONV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CONV = CString("<Default>");
		}
		else
		{
			prpSetupData->CONV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclATTV, "=");
	if(psp == NULL)
	{
			prpSetupData->ATTV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->ATTV = CString("<Default>");
		}
		else
		{
			prpSetupData->ATTV =  CString(pclOriginal);
		}
	}
	psp = NULL;


	return TRUE;
}

void CedaCfgData::SetCfgData(void)
{
	//ogCommHandler.bmTurnaroundBars = ogCfgData.rmUserSetup.TACK[0] == 'J';
	//ogCommHandler.bmTurnaroundBars = ogCfgData.rmUserSetup.TACK[0] == 'J';
	//ogCommHandler.bmDemandsOnly = ogCfgData.rmUserSetup.FBCK[0] == 'N';
	//ogCommHandler.bmShowShadowBars = ogCfgData.rmUserSetup.SBCK[0] == 'J';
	
/*	CViewer rlViewer;
	rlViewer.SetViewerKey("GateDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.GACV);

	rlViewer.SetViewerKey("StaffDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.STCV);

	rlViewer.SetViewerKey("CciDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.CCCV);
	rlViewer.SetViewerKey("StaffTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.STLV);
	rlViewer.SetViewerKey("FltSched");
	rlViewer.SelectView(ogCfgData.rmUserSetup.FPLV);
	rlViewer.SetViewerKey("GateTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.GBLV);
	rlViewer.SetViewerKey("CciTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.CCBV);
	rlViewer.SetViewerKey("ReqTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.RQSV);
	rlViewer.SetViewerKey("ConfTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.CONV);
	rlViewer.SetViewerKey("PPTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.VPLV);
*/
}
