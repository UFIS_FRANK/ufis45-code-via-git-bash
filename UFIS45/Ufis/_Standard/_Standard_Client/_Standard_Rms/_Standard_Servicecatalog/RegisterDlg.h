#if !defined(AFX_REGISTERDLG_H__B8DDC081_A281_11D3_93A7_00001C033B5D__INCLUDED_)
#define AFX_REGISTERDLG_H__B8DDC081_A281_11D3_93A7_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RegisterDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// RegisterDlg dialog

class RegisterDlg : public CDialog
{
// Construction
public:
	RegisterDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(RegisterDlg)
	enum { IDD = IDD_REGISTERDLG };
	CEdit	m_TIME;
	CEdit	m_TEXT;
	CButton	m_ABBRECHEN;
	CButton	m_STARTEN;
	CButton	m_RREGISTER;
	//}}AFX_DATA
private:

	int imTimer;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RegisterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	
	void SetBdpsState();

	// Generated message map functions
	//{{AFX_MSG(RegisterDlg)
	afx_msg void OnAbbrechen();
	afx_msg void OnRegistrieren();
	afx_msg void OnStarten();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGISTERDLG_H__B8DDC081_A281_11D3_93A7_00001C033B5D__INCLUDED_)
