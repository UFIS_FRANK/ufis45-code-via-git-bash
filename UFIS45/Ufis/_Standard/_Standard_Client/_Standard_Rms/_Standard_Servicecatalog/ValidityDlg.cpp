// ValidityDlg.cpp : implementation file
//

#include <stdafx.h>

#include <basicdata.h>
#include <utilities.h>
#include <ServiceCatalog.h>
#include <ValidityDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CValidityDlg dialog


CValidityDlg::CValidityDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CValidityDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CValidityDlg)
	m_VafrDate = COleDateTime::GetCurrentTime();
	m_VafrTime = COleDateTime::GetCurrentTime();
	m_VatoDate = COleDateTime::GetCurrentTime();
	m_VatoTime = COleDateTime::GetCurrentTime();
	m_bUnlimited = FALSE;
	//}}AFX_DATA_INIT
	int ilYear = m_VatoDate.GetYear() ;
	int ilMonth = m_VatoDate.GetMonth() ;
	int ilDay = m_VatoDate.GetDay() ;         
	m_VatoDate.SetDate ( ilYear+10, ilMonth, ilDay );
	m_VatoTime = m_VatoDate;
	m_VatoTime.SetTime(23, 59, 59);
}


void CValidityDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CValidityDlg)
	DDX_Control(pDX, IDC_VATO_TIME, m_TimeToCtrl);
	DDX_Control(pDX, IDC_VATO_DATE, m_DateToCtrl);
	DDX_DateTimeCtrl(pDX, IDC_VAFR_DATE, m_VafrDate);
	DDX_DateTimeCtrl(pDX, IDC_VAFR_TIME, m_VafrTime);
	DDX_DateTimeCtrl(pDX, IDC_VATO_DATE, m_VatoDate);
	DDX_DateTimeCtrl(pDX, IDC_VATO_TIME, m_VatoTime);
	DDX_Check(pDX, IDC_UNLIMITED_CHK, m_bUnlimited);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CValidityDlg, CDialog)
	//{{AFX_MSG_MAP(CValidityDlg)
	ON_BN_CLICKED(IDC_VATO_TXT, OnVatoTxt)
	ON_BN_CLICKED(IDC_UNLIMITED_CHK, OnUnlimitedChk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CValidityDlg message handlers


void CValidityDlg::SetValidity ( COleDateTime &ropVafrDate, COleDateTime &ropVafrTime, 
							    COleDateTime &ropVatoDate, COleDateTime &ropVatoTime )
{
	m_VafrDate = ropVafrDate;
	m_VafrTime = ropVafrTime;
	m_VatoDate = ropVatoDate;
	m_VatoTime = ropVatoTime;
}

void CValidityDlg::GetValidity ( COleDateTime &ropVafrDate, COleDateTime &ropVafrTime, 
							    COleDateTime &ropVatoDate, COleDateTime &ropVatoTime )
{
	ropVafrDate = m_VafrDate;
	ropVafrTime = m_VafrTime;
	ropVatoDate = m_VatoDate;
	ropVatoTime = m_VatoTime;
}

void CValidityDlg::OnOK() 
{
	// TODO: Add extra validation here
	if ( !UpdateData () )
		return;

	COleDateTime olVafr = OneCOleDateTimeFromTwo( m_VafrDate, m_VafrTime );
	COleDateTime olVato = OneCOleDateTimeFromTwo( m_VatoDate, m_VatoTime );

	if ( olVafr > olVato )
	{
		LangMsgBox ( m_hWnd, IDS_VALID_PERIOD_NOT_OK, IDS_ERROR,
						 MB_ICONEXCLAMATION|MB_OK );
		return;
	}
	CDialog::OnOK();
}

void CValidityDlg::IniStatics ()
{
	SetWindowText ( GetString ( IDS_VALIDITY_TXT ) );
	SetDlgItemText ( IDC_VAFR_TXT, GetString ( IDS_VALID_FROM ) ) ;
	SetDlgItemText ( IDC_VATO_TXT, GetString ( IDS_VALID_TO	) ) ;
	SetDlgItemText ( IDC_UNLIMITED_CHK, GetString ( IDS_UNLIMITED ) );  
	SetDlgItemText ( IDOK, GetString ( IDS_OK ) ) ;
	SetDlgItemText ( IDCANCEL, GetString ( IDS_CANCEL ) );  
}

BOOL CValidityDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	IniStatics ();
	m_DateToCtrl.EnableWindow ( !m_bUnlimited );
	m_TimeToCtrl.EnableWindow ( !m_bUnlimited );
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CValidityDlg::OnVatoTxt() 
{
	// TODO: Add your control notification handler code here
	
}

void CValidityDlg::OnUnlimitedChk() 
{
	// TODO: Add your control notification handler code here
	UpdateData ();
	m_DateToCtrl.EnableWindow ( !m_bUnlimited );
	m_TimeToCtrl.EnableWindow ( !m_bUnlimited );
}

void CValidityDlg::SetValidity ( CString &ropVafr, CString &ropVato )
{
	CTime olTime;
	olTime = DBStringToDateTime ( ropVafr );
	CTimeToTwoCOleDateTime( olTime, m_VafrDate, m_VafrTime );

	m_bUnlimited = TRUE;
	if ( !ropVato.IsEmpty() && (ropVato[0] != ' ' ) )
	{
		olTime = DBStringToDateTime ( ropVato );
		if ( CTimeToTwoCOleDateTime( olTime, m_VatoDate, m_VatoTime ) )
			m_bUnlimited = FALSE; 
	}
}

void CValidityDlg::GetValidity ( CString &ropVafr, CString &ropVato )
{
	ropVafr = CTwoOleDateTimeToDBString( m_VafrDate, m_VafrTime );
	if ( m_bUnlimited )
		ropVato.Empty();
	else
		ropVato = CTwoOleDateTimeToDBString( m_VatoDate, m_VatoTime );
}
