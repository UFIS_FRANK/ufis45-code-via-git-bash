// ServiceList.cpp: implementation of the CServiceList class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <RecordSet.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <GridControl.h>
#include <ServiceCatalog.h>
#include <SelectService.h>
#include <ServiceList.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CServiceList::CServiceList(CWnd* pParent /*= NULL*/)
	:CSelectService(pParent)
{
	//{{AFX_DATA_INIT(CServiceList)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

}

CServiceList::~CServiceList()
{

}

void CServiceList::DoDataExchange(CDataExchange* pDX)
{
	CSelectService::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CServiceList)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CServiceList, CSelectService)
	//{{AFX_MSG_MAP(CServiceList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CServiceList::DoModeless ( const char *pcpTabName, RecordSet *popSelectedService )
{
	BOOL	ok;
	strcpy ( pcmTabName, pcpTabName );
	pomSelectedService = popSelectedService;
	if ( ok = Create(CSelectService::IDD,m_pParentWnd) )
		ShowWindow ( SW_SHOW );
	return ok;
}

void CServiceList::OnOK() 
{
	// TODO: Add extra validation here
	ROWCOL lRow, lCol;
	CString olStr;
	RecordSet   olRecord;
	CGXStyle	olStyle;
	DWORD		ilUrno;
	
	if ( !pomServiceList->GetCurrentCell( lRow, lCol ) )
	{
		return;
	}
	pomServiceList->ComposeStyleRowCol( lRow,1, &olStyle );
	ilUrno = (DWORD)olStyle.GetItemDataPtr();
	if ( !ilUrno )
	{
		TRACE ("Urno=0 in SelectService f�r Zeile%d\n", lRow );
		return;
	}
	else
		olStr.Format( "%ld", ilUrno );
	//if ( !ogBCD.GetRecord(pcmTabName, lRow-1,  olRecord ) )
	if ( !ogBCD.GetRecord(pcmTabName, "URNO", olStr, olRecord ) )
		return;
	if ( pomSelectedService )
		*pomSelectedService = olRecord;
	DestroyWindow();
}


void CServiceList::OnCancel ()
{
	DestroyWindow();
}
