// CheckReference.h

#ifndef _CHECKREFERENCE_
#define _CHECKREFERENCE_
 
#include <stdafx.h>

enum TableID
{
	_SER
};
//---------------------------------------------------------------------------------------------------------
// Class declaration

class CheckReference
{
// Attributes
public:

// Operations
public:
    CheckReference();
	~CheckReference();
	
	//Return: >0 = Record exist, 0 = Record don't exist
	int Check(int ipTableID, long lpUrno, CString opCode);

	bool BackDoorEnabled();

// Private methods
private:
	bool bmUseBackDoor;
};

//---------------------------------------------------------------------------------------------------------

#endif //_CHECKREFERENCE_
