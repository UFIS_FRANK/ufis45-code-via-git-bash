// BasePropertySheet.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <ccsglobl.h>
// These following include files are necessary
#include <cviewer.h>
#include <BasePropertySheet.h>
#include <resource.h>
#include <BasicData.h>

IMPLEMENT_DYNAMIC(BasePropertySheet, CPropertySheet)


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BasePropertySheet
//
int BasePropertySheet::imCount = 0;


BasePropertySheet::BasePropertySheet(LPCSTR pszCaption,
	CWnd* pParentWnd, CViewer *popViewer, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	pomViewer = popViewer;
	pomParentWnd = NULL;
}

void BasePropertySheet::LoadDataFromViewer()
{
}

void BasePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
}

int BasePropertySheet::QueryForDiscardChanges()
{
	return IDOK;
}

BasePropertySheet::~BasePropertySheet()
{
}


BEGIN_MESSAGE_MAP(BasePropertySheet, CPropertySheet)
	//{{AFX_MSG_MAP(BasePropertySheet)
	ON_CBN_SELCHANGE(IDC_SHEET_VIEW, OnViewSelChange)
	ON_BN_CLICKED(IDC_SHEET_SAVE, OnSave)
	ON_BN_CLICKED(IDC_SHEET_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_SHEET_APPLY, OnApply)
	ON_BN_CLICKED(IDOK, OnOK)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BasePropertySheet implementation

int BasePropertySheet::DoModal() 
{
	int ilRc = 0;

	//-- exception handling
	if (imCount > 0)
	return IDCANCEL;

	imCount++;

	// Checking the given viewer, don't open the dialog if the viewer is invalid
	if (pomViewer == NULL || pomViewer->SelectView().IsEmpty())
	{
		imCount--;
		return IDCANCEL;
	}

	// This line do nothing, just fix bug in CViewer (by setting m_ViewName)
	pomViewer->SelectView(pomViewer->SelectView());
	ilRc =  CPropertySheet::DoModal();

	imCount--;
	
	return ilRc;
}

/////////////////////////////////////////////////////////////////////////////
// BasePropertySheet message handlers

BOOL BasePropertySheet::OnInitDialog() 
{
	BOOL ilRet = FALSE;

	//-- exception handling
	CCS_TRY

	ilRet = CPropertySheet::OnInitDialog();

	// Resize the window for new combo-box and buttons
	CRect rcWindow;
	GetWindowRect(rcWindow);
	rcWindow.InflateRect(0, 20);
	MoveWindow(rcWindow);

	// Prepare the position for new control objects
	CRect rcApply;
	GetDlgItem(ID_APPLY_NOW)->GetWindowRect(rcApply);
	ScreenToClient(rcApply);
	CRect rcCancel;
	GetDlgItem(IDCANCEL)->GetWindowRect(rcCancel);
	ScreenToClient(rcCancel);

	omView.Create(WS_VISIBLE | CBS_DROPDOWN | WS_VSCROLL,
		CRect(6, rcApply.top, 140, rcApply.top + 100),
		this, IDC_SHEET_VIEW);
    omView.SetFont(GetDlgItem(IDCANCEL)->GetFont());
	
	omSave.Create(GetString(IDS_SAVE), WS_VISIBLE | BS_PUSHBUTTON,
		CRect(145, rcApply.top, 145 + rcApply.Width(), rcApply.bottom),
		this, IDC_SHEET_SAVE);

    omSave.SetFont(GetDlgItem(IDCANCEL)->GetFont());
	
	omDelete.Create(GetString(IDS_DELETE), WS_VISIBLE | BS_PUSHBUTTON,
		CRect(147 + rcApply.Width(), rcApply.top, 147 + 2*rcApply.Width(), rcApply.bottom),
		this, IDC_SHEET_DELETE);

    omDelete.SetFont(GetDlgItem(IDCANCEL)->GetFont());

	omApply.Create(GetString(IDS_APPLY), WS_VISIBLE | BS_PUSHBUTTON,
		CRect(149 + 2*rcApply.Width(), rcApply.top, 149 + 3*rcApply.Width(), rcApply.bottom),
		this, IDC_SHEET_APPLY);

    omApply.SetFont(GetDlgItem(IDCANCEL)->GetFont());

	// Draw the separator between save/delete buttons and standard close/help buttons
	CRect rcClient;
	GetClientRect(rcClient);
	omSeparator.Create("", WS_VISIBLE | SS_BLACKFRAME,
		CRect(6, rcApply.bottom + 7, rcClient.Width() - 6, rcApply.bottom + 9), this);

	// Remove the button "Apply"; also shift "OK" and "Cancel" buttons
	GetDlgItem(ID_APPLY_NOW)->DestroyWindow();
	CRect rcOK = rcCancel;
	rcOK.OffsetRect(-70, rcOK.Height() + 16);//5 => -70
	GetDlgItem(IDOK)->MoveWindow(rcOK);
	rcCancel = rcApply;
	rcCancel.OffsetRect(-30, rcCancel.Height() + 16);//5 => -30
	GetDlgItem(IDCANCEL)->MoveWindow(rcCancel);
	
	GetDlgItem(IDCANCEL)->SetWindowText(GetString(IDS_CANCEL));

	CWnd *olHelpButton = GetDlgItem(IDHELP);
	if (olHelpButton != NULL)
	{
		CRect rcHelp;
		olHelpButton->GetWindowRect(rcHelp);
		ScreenToClient(rcHelp);
		rcHelp.OffsetRect(0, rcApply.Height() + 16);
		olHelpButton->MoveWindow(rcHelp);
		olHelpButton->SetWindowPos(&wndBottom,0,0,0,0,SWP_HIDEWINDOW | SWP_NOMOVE );
		olHelpButton->EnableWindow(FALSE);
	}

	UpdateComboBox();	// update data in the combo-box
	LoadDataFromViewer();	// update data in every pages
	GetActivePage()->UpdateData(FALSE);

	//-- exception handling
	CCS_CATCH_ALL

	return ilRet;
}

BOOL BasePropertySheet::DestroyWindow() 
{
	/*CWnd *pWnd = NULL;
	pWnd = GetDlgItem(IDC_SHEET_VIEW);
	
	if (::IsWindow(pWnd->GetSafeHwnd()))
		delete (CComboBox *)pWnd;

	delete (CButton *)GetDlgItem(IDC_SHEET_SAVE);
	delete (CButton *)GetDlgItem(IDC_SHEET_DELETE);
	delete (CButton *)GetDlgItem(IDC_SHEET_APPLY);*/

	return CPropertySheet::DestroyWindow();
}

void BasePropertySheet::OnViewSelChange()
{
	CCS_TRY

	if (GetActivePage()->UpdateData() == FALSE)
		return;
	if (QueryForDiscardChanges() != IDOK)
	{
		UpdateComboBox();
		return;
	}

	// Check if user didn't change anything, so we don't have to reload the view
	if (GetDlgItem(IDCANCEL)->IsWindowEnabled() &&		// no change made before?
		GetComboBoxText() == pomViewer->SelectView())	// still on the old view?
		return;

	// Discard changes and restore data from the registry database
	CString olViewName = GetComboBoxText();
	pomViewer->SelectView(olViewName);
	UpdateComboBox();
	LoadDataFromViewer();
	GetActivePage()->UpdateData(FALSE);
	GetActivePage()->CancelToClose();

	CCS_CATCH_ALL
}

void BasePropertySheet::OnSave()
{
	//-- exception handling
	// Don't allow the user to change the default view
	CString olViewName = GetComboBoxText();

	if (olViewName == "<Default>")
		return;

	// Validate the current page before saving
	if (GetActivePage()->UpdateData() == FALSE)
		return;

	// Save data to the registry database
	CStringArray olFilters;
	pomViewer->GetFilterPage(olFilters);	// use filters in the last view
	pomViewer->CreateView(olViewName, olFilters);
	pomViewer->SelectView(olViewName);	// just make sure that CViewer() work correctly
	SaveDataToViewer(olViewName,TRUE);
	UpdateComboBox();
	CString olWhere;

	EndDialog(IDOK);

}

void BasePropertySheet::OnApply()
{
	CCS_TRY

	// Don't allow the user to change the default view
	CString olViewName = GetComboBoxText();
	
	if (olViewName == "<Default>")
		return;

	// Validate the current page before saving
	if (GetActivePage()->UpdateData() == FALSE)
		return;

	// Save data to the registry database
	CStringArray olFilters;
	pomViewer->GetFilterPage(olFilters);	// use filters in the last view
	bool blWithDBDelete = false;
	pomViewer->CreateView(olViewName, olFilters, blWithDBDelete);
	pomViewer->SelectView(olViewName);	// just make sure that CViewer() work correctly
	SaveDataToViewer(olViewName,FALSE);
	UpdateComboBox();
	EndDialog(IDOK);

	CCS_CATCH_ALL
}


void BasePropertySheet::OnDelete()
{
	CCS_TRY

	// Don't allow the user to delete the default view
	CString olViewName = GetComboBoxText();
	if (olViewName == "<Default>")
		return;

	// Delete the selected view from the registry database
	pomViewer->DeleteView(olViewName);
	pomViewer->SelectView("<Default>");
	UpdateComboBox();
	LoadDataFromViewer();
	GetActivePage()->UpdateData(FALSE);

	CCS_CATCH_ALL
}

void BasePropertySheet::OnOK()
{
	CCS_TRY

	if (GetActivePage()->UpdateData() == FALSE)
		return;
	if (QueryForDiscardChanges() != IDOK)
	{
		UpdateComboBox();
		return;
	}

	// Terminate the PropertySheet
	EndDialog(IDOK);

	CCS_CATCH_ALL
}

/////////////////////////////////////////////////////////////////////////////
// BasePropertySheet -- help routines

void BasePropertySheet::UpdateComboBox()
{
	CCS_TRY

	CComboBox *polView = (CComboBox *)GetDlgItem(IDC_SHEET_VIEW);
	polView->ResetContent();
	CStringArray olViews;
	pomViewer->GetViews(olViews);
	for (int ilIndex = 0; ilIndex < olViews.GetSize(); ilIndex++)
	{
		polView->AddString(olViews[ilIndex]);
		if (olViews[ilIndex] == pomViewer->SelectView())
			polView->SetCurSel(ilIndex);
	}

	BOOL blIsDefaultView = pomViewer->SelectView() == "<Default>";

	CCS_CATCH_ALL
}

CString BasePropertySheet::GetComboBoxText()
{
	CString olViewName;

	CCS_TRY
	
	CComboBox *polView = (CComboBox *)GetDlgItem(IDC_SHEET_VIEW);
	int ilItemID;
	
	if ((ilItemID = polView->GetCurSel()) == CB_ERR)
	{
		polView->GetWindowText(olViewName);
	}
	else
	{
		polView->GetLBText(ilItemID, olViewName);
	}

	CCS_CATCH_ALL

	return olViewName;

}

BOOL BasePropertySheet::IsIdentical(CStringArray &ropArray1, CStringArray &ropArray2)
{
	CCS_TRY

	if (ropArray1.GetSize() != ropArray2.GetSize())	// incompatible array size?
		return FALSE;

	for (int i = ropArray1.GetSize()-1; i >= 0; i--)
		if (!IsInArray(ropArray1[i], ropArray2))	// unidentical element found?
			return FALSE;

	CCS_CATCH_ALL

	return TRUE;
}

BOOL BasePropertySheet::IsInArray(CString &ropString, CStringArray &ropArray)
{
	CCS_TRY

	for (int i = ropArray.GetSize()-1; i >= 0; i--)
		if (ropString == ropArray[i])	// identical string found?
			return TRUE;

	CCS_CATCH_ALL

	return FALSE;
}
