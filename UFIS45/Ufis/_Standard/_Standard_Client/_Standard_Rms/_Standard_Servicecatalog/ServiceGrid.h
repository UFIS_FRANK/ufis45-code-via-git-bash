// ServiceGrid.h: interface for the CServiceGrid class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVICEGRID_H__5027F310_3A7D_11D3_933D_00001C033B5D__INCLUDED_)
#define AFX_SERVICEGRID_H__5027F310_3A7D_11D3_933D_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CServiceCatalogDoc;

#include <GridControl.h>

class CServiceGrid : public CTitleGridControl  
{
public:
	CServiceGrid( CWnd *popParent, UINT nID, CString opTitle );
	CServiceGrid( CWnd *popParent, UINT nID, char* opTableName, 
				  CServiceCatalogDoc *popDoc );
	virtual ~CServiceGrid();
protected:
	CString omTableName;
	CString omCodeField;
	CString omDisplayField;
	int		imDBRecords;
//  Implementation
public:
	virtual void ResetValues ();
	int GetLineCount ();
	virtual bool GetLineValues ( int ipLine, CString &ropTable, 
								 CString &ropUrno, CString &ropCode, 
								 CString &ropAmount, CString &ropPriority, 
								 CString &ropSubType, bool &rbpStaticGroup );
	virtual bool SetLineValues ( int ipResNo, CString &ropTable, CString &ropCode,
								 CString &ropAmount, CString &ropPriority );
	virtual bool DoppelteResourcen ();
	virtual CString GetTableName ( ROWCOL ipRow );
	int GetMaxAmount ();
	int GetSumOfAmounts ();
	virtual void ModifyChoiceLists ( CString opTable, CString opSubtype );


protected:
	void IniLayOut ();
	bool CreateComboBoxes ();
	bool CreateComboBoxes ( CString &ropChoiceList );
	void DisplayInfo ( ROWCOL nRow, ROWCOL nCol );
	BOOL OnEndEditing( ROWCOL nRow, ROWCOL nCol) ;
	void DoAutoGrow ( ROWCOL nRow, ROWCOL nCol);
	virtual bool IsResourceAllReadySelected ( CString &ropResource,
											  CString &ropTableName,
											  ROWCOL ipDontTest, 
											  bool bpErrMsg=true );
	void OnModifyCell (ROWCOL nRow, ROWCOL nCol);
	BOOL OnValidateCell(ROWCOL nRow, ROWCOL nCol);
	virtual bool DeleteEmptyRows ( ROWCOL nRow, ROWCOL nCol ) ;
	void SetDefaultValues ( ROWCOL nRow );
	bool IsSingleResourceSelected ( ROWCOL ipLine, CString &ropTable, 
									CString &ropCode );
};

class CVarServiceGrid : public CServiceGrid
{
public:
	CVarServiceGrid( CWnd *popParent, UINT nID, CString opTitle );
protected:
	//  CStringArray omTableNames;
//  Implementation
protected:
	BOOL OnEndEditing( ROWCOL nRow, ROWCOL nCol) ;
	bool SetComboBox ( char *pcpTableName, char *pcpCodeField, ROWCOL ipLine );
	bool SetComboBox ( CString &ropChoiceList, ROWCOL ipLine );
	void EnableLine ( ROWCOL ipLine, BOOL enable );
	BOOL InsertBottomRow ();
	void SetEquTableName ( ROWCOL ipRow, char *pcpEquType );
	void SetTableName ( ROWCOL ipRow, char *pcpTableName );
	bool IsResourceAllReadySelected ( CString &ropResource, 
									  CString &ropTableName, ROWCOL ipDontTest, 
									  bool bpErrMsg=true );
public:
	bool SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked );
	bool SetOneTable ( CString opTableName, ROWCOL ipLine, CString opSubtype );

	void ResetValues ();
	bool GetLineValues ( int ipLine, CString &ropTable, CString &ropUrno, 
					     CString &ropCode, CString &ropAmount, 
						 CString &ropPriority, CString &ropSubType, bool &rbpStaticGroup );
	bool SetLineValues ( int ipResNo, CString &ropTable, CString &ropCode,
						 CString &ropAmount, CString &ropPriority );
	CString GetTableName ( ROWCOL ipRow );
	bool DoppelteResourcen ();
	CString GetSubtype ( ROWCOL ipRow );
	void ModifyChoiceLists ( CString opTable, CString opSubtype );
};

#endif // !defined(AFX_SERVICEGRID_H__5027F310_3A7D_11D3_933D_00001C033B5D__INCLUDED_)
