#if !defined(AFX_VALIDITYDLG_H__B74E3343_59EE_11D3_935A_00001C033B5D__INCLUDED_)
#define AFX_VALIDITYDLG_H__B74E3343_59EE_11D3_935A_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ValidityDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CValidityDlg dialog

class CValidityDlg : public CDialog
{
// Construction
public:
	CValidityDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CValidityDlg)
	enum { IDD = IDD_VALIDITY_DLG };
	CDateTimeCtrl	m_TimeToCtrl;
	CDateTimeCtrl	m_DateToCtrl;
	COleDateTime	m_VafrDate;
	COleDateTime	m_VafrTime;
	COleDateTime	m_VatoDate;
	COleDateTime	m_VatoTime;
	BOOL	m_bUnlimited;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CValidityDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	void GetValidity ( COleDateTime &ropVafrDate, COleDateTime &ropVafrTime, 
				      COleDateTime &ropVatoDate, COleDateTime &ropVatoTime );
	void SetValidity ( COleDateTime &ropVafrDate, COleDateTime &ropVafrTime, 
					  COleDateTime &ropVatoDate, COleDateTime &ropVatoTime );
	void SetValidity ( CString &ropVafr, CString &ropVato );
	void GetValidity ( CString &ropVafr, CString &ropVato );
protected:
	void IniStatics ();
	// Generated message map functions
	//{{AFX_MSG(CValidityDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnVatoTxt();
	afx_msg void OnUnlimitedChk();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VALIDITYDLG_H__B74E3343_59EE_11D3_935A_00001C033B5D__INCLUDED_)
