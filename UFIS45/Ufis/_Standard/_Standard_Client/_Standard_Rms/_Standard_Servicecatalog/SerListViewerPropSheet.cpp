// SerListViewerPropSheet.cpp : implementation file
//

#include <stdafx.h>
#include <servicecatalog.h>
#include <SerListViewerPropSheet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSerListViewerPropSheet

IMPLEMENT_DYNAMIC(CSerListViewerPropSheet, BasePropertySheet)


CSerListViewerPropSheet::CSerListViewerPropSheet(LPCTSTR pszCaption, CWnd* pParentWnd, CViewer *popViewer, UINT iSelectPage)
	:BasePropertySheet(pszCaption, pParentWnd,popViewer, iSelectPage )
{
	AddPage(&m_SerListView);
	m_SerListView.SetCaption ( GetString ( IDS_SERVICES ) );

}

CSerListViewerPropSheet::~CSerListViewerPropSheet()
{
}


BEGIN_MESSAGE_MAP(CSerListViewerPropSheet, BasePropertySheet)
	//{{AFX_MSG_MAP(CSerListViewerPropSheet)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerListViewerPropSheet message handlers

void CSerListViewerPropSheet::LoadDataFromViewer()
{
	pomViewer->GetFilter("SERLIST", m_SerListView.omValues);
}

void CSerListViewerPropSheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	//-- exception handling

	pomViewer->SetFilter("SERLIST", m_SerListView.omValues);
	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
	
	//-- exception handling

}

