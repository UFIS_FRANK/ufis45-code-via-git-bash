#if !defined(AFX_SERLISTVIEW_H__E1749A51_8539_11D7_9777_18CA9C000000__INCLUDED_)
#define AFX_SERLISTVIEW_H__E1749A51_8539_11D7_9777_18CA9C000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SerListView.h : header file
//

#include <CCSPTRArray.h>
#include <RecordSet.h>

/////////////////////////////////////////////////////////////////////////////
// CSerListView dialog

class CSerListView : public CPropertyPage
{
	DECLARE_DYNCREATE(CSerListView)

// Construction
public:
	CSerListView();
	~CSerListView();

// Dialog Data
	//{{AFX_DATA(CSerListView)
	enum { IDD = IDD_GHSVIEW };
	CListBox	m_VrgcLst;
	CListBox m_VrgcFilter;
	//}}AFX_DATA

	CStringArray omValues;
	CCSPtrArray <RecordSet> omSerData;
	char pcmCaption[100];

	void SetData(CStringArray &ropValues);
	BOOL GetData(CStringArray &ropValues);
	void SetCaption(const char *pcpCaption);

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSerListView)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CSerListView)
	virtual BOOL OnInitDialog();
	afx_msg void OnAdd();
	afx_msg void OnRemove();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERLISTVIEW_H__E1749A51_8539_11D7_9777_18CA9C000000__INCLUDED_)
