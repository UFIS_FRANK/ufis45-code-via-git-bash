// GridRecordXChange.cpp: implementation of the CGridRecordXChange class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <cedabasicdata.h>

#include <ccsglobl.h>
#include <ServiceCatalog.h>
#include <ServiceCatalogDoc.h>
#include <ServiceGrid.h>
#include <utilities.h>
#include <personelldata.h>
#include <personelldata.h>
#include <personalgrid.h>
#include <GridRecordXChange.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGridRecordXChange::CGridRecordXChange(char *pcpTableName, char *pcpAmountField, 
									   char *pcpResUrnoField, char *ResCodeField, 
									   CServiceGrid *popGrid, char *pcpTypeField/*=0*/ )
{
	omTableName = pcpTableName;
	omAmountField = pcpAmountField;
	omResourceUrnoField = pcpResUrnoField;
	omResourceCodeField = ResCodeField;
	pomGrid = popGrid;
	pomDisplayedService = 0;
	imTypeInd = -1;

	//  Initialize indices into RecordSet
	imResCodeInd = ogBCD.GetFieldIndex(omTableName, omResourceCodeField );
	imResUrnoInd = ogBCD.GetFieldIndex(omTableName, omResourceUrnoField );
	imAmountInd = ogBCD.GetFieldIndex(omTableName, omAmountField );
	imGroupTabNameInd = ogBCD.GetFieldIndex(omTableName, "GTAB" );
	imPrioInd = ogBCD.GetFieldIndex(omTableName, "PRIO" );	
	imServiceUrnoInd = ogBCD.GetFieldIndex(omTableName, "USRV" );	
	imFieldsInRecord = ogBCD.GetFieldCount ( omTableName );
	if ( pcpTypeField )
	{
		omTypeField = pcpTypeField;
		imTypeInd = ogBCD.GetFieldIndex(omTableName, pcpTypeField );	
	}
}

CGridRecordXChange::~CGridRecordXChange()
{
	omRecordList.DeleteAll ();
}


//  Mitteilung, da� die Urno des dargestellten Serivice sich ge�ndert hat
//  d.h. beim n�chsten Speichern m�ssen alle Datens�tze in der zugeh�rigen
//	Tabelle (SEQ, SEG etc.) neu geschrieben werden.
void CGridRecordXChange::OnServiceUrnoChanged ()
{
	omRecordList.DeleteAll ();
}

int CGridRecordXChange::Grid2RecordList ()
{
	if ( !pomGrid )
		return 0;		//  Keine Records gelesen, da Grid nicht initialisiert

	bool			blOk;
	int				i, ilRecordAnz=0;
	int    			ilLineAnz = pomGrid->GetLineCount ();
	CString			olResTable, olResUrno, olResCode, olAmount, olPriority, olSubType;
	CMapStringToPtr	olMapUsedResUrno;
	RecordSet		*polRecord;	
	bool			blStaticGroup;

	//  index der Urno der Resource innerhalb des verwendeten Datensatzes bestimmen
	//  z.b.     UPER                in                  SEQTAB
	GetMapForRecordList ( omRecordList, olMapUsedResUrno, imResUrnoInd );
	
	//  Alle bisherigen Records mit DELETED vorbesetzen
	for ( i=0; i<omRecordList.GetSize(); i++ )
		omRecordList[i].Status = DATA_DELETED;
	
	//DATA_UNCHANGED, DATA_NEW, DATA_CHANGED, 
	for ( i=1; i<=ilLineAnz; i++ )
	{
		blOk = pomGrid->GetLineValues ( i, olResTable, olResUrno, olResCode, olAmount, 
										olPriority, olSubType, blStaticGroup );
		if ( !blOk )
			continue;
		
		if ( olMapUsedResUrno.Lookup( olResUrno, (void *&)polRecord ) )
		{	//  Diese Resource wurde auch vorher verwendet
			if ( (polRecord->Values[imAmountInd]!=olAmount) ||
				 (polRecord->Values[imPrioInd]!=olPriority) )
			{	//  werte haben sich ge�ndert
				polRecord->Values[imAmountInd]=olAmount;
				polRecord->Values[imPrioInd]=olPriority;
				polRecord->Status = DATA_CHANGED;
			}
			else
				polRecord->Status = DATA_UNCHANGED;
		}
		else
		{	//  Resource kommt neu dazu
			polRecord = new RecordSet ( imFieldsInRecord );
			polRecord->Values[imAmountInd] = olAmount;
			polRecord->Values[imPrioInd] = olPriority;
			polRecord->Values[imResUrnoInd] = olResUrno; 
			polRecord->Values[imResCodeInd] = GetResCodeFieldEntry ( olResTable, olResCode,
																	 blStaticGroup ) ;
			if ( blStaticGroup )
				polRecord->Values[imGroupTabNameInd] = "SGR.GRPN";
			else
				polRecord->Values[imGroupTabNameInd] = "";
			if ( imTypeInd >=0 )
				polRecord->Values[imTypeInd] = olSubType;
			polRecord->Status = DATA_NEW;
			omRecordList.Add ( polRecord );
		}
		ilRecordAnz ++;
	}
	return ilRecordAnz;
}

bool CGridRecordXChange::RecordList2Grid ()
{
	return false;
}

//	Anzahl der dargestellten Datens�tze
//  
int CGridRecordXChange::SetService ( RecordSet *popService )
{
	CString		olServiceUrno, olResCode, olAmount, olPrio, olGTabEntry, olResUrno;
	int			ilResourceAnz = 0, i, ilDisplayed=0;
	RecordSet	*polRecord;
	CString		olResTable;

	pomDisplayedService = popService;
	omRecordList.DeleteAll();
	if ( popService )
	{
		olServiceUrno = popService->Values[ogBCD.GetFieldIndex(pcgTableName,"URNO")];
		ogBCD.GetRecords( omTableName, "USRV", olServiceUrno, &omRecordList );
		ilResourceAnz = omRecordList.GetSize ();
		for ( i=0; (i<ilResourceAnz) && pomGrid; i++ )
		{
			polRecord = &omRecordList.ElementAt(i);
			olGTabEntry = polRecord->Values[imGroupTabNameInd];
			if ( !olGTabEntry.IsEmpty () && (olGTabEntry!=" ") )
			{	//  es handelt sich um eine Gruppe
				CString olGrpTable = "SGR";
				CString olGrpCodeField = "GRPN";

				CLocationGridRecordXChange::ParseResCodeFieldEntry ( olGTabEntry, 
													olGrpTable, olGrpCodeField ) ;

				olResUrno = polRecord->Values[imResUrnoInd] ; 
				olResCode = ogBCD.GetField ( olGrpTable, "URNO", olResUrno, olGrpCodeField );
				olResCode = "*"+ olResCode;
			}
			else
				olResCode = polRecord->Values[imResCodeInd];
			olAmount = polRecord->Values[imAmountInd];
			olPrio = polRecord->Values[imPrioInd];
			if ( omTableName == "SEE" )
				olResTable = "EQU";
			if ( pomGrid->SetLineValues ( ilDisplayed, olResTable, olResCode, olAmount, olPrio ) )
				ilDisplayed++;
		}
			
	}
	return ilResourceAnz;
}


CString	CGridRecordXChange::GetResCodeFieldEntry ( CString &opResTable, CString &opResCode, 
												   bool blStaticGroup ) 
{
	if ( blStaticGroup )
		return CString("");
	else
		return opResCode;
}

void CGridRecordXChange::ResetService ()
{
	omRecordList.DeleteAll();
	pomDisplayedService =0;
}


//  Alle S�tze dieser Resource-Gruppe in SEQ, SEE, SEL, SER bzw SEG speichern
//	wenn pcpServiceUrno != 0, zuvor USRV auf pcpServiceUrno setzen
bool CGridRecordXChange::SaveRecords ( char *pcpServiceUrno /*=0*/)
{
	RecordSet *olRecord;
	bool	  blOk=TRUE, blChanges = FALSE;

	int urnoIndex = ogBCD.GetFieldIndex ( omTableName, "URNO" );

	theApp.BeginWaitCursor ();
	for ( int i=0; i<omRecordList.GetSize(); i++ )
	{
		olRecord = &omRecordList[i];
		if ( !olRecord )
			continue;
		if ( pcpServiceUrno )
			olRecord->Values[imServiceUrnoInd] = pcpServiceUrno;
		
		switch ( olRecord->Status )
		{
			case DATA_DELETED:	//  Satz l�schen
				olRecord->Status = 0;	//  Status zur�cksetzen;
				blOk &= ogBCD.DeleteRecord ( omTableName, "URNO", 
											 olRecord->Values[urnoIndex] );
				blChanges = true;
				break;
			case DATA_UNCHANGED:	//  nichts zu tun
				olRecord->Status = 0;	//  Status zur�cksetzen
				break;
			case DATA_NEW:	//  Satz einf�gen
				olRecord->Status = 0;	//  Status zur�cksetzen
				blOk &= ogBCD.InsertRecord(omTableName, *olRecord, FALSE );
				blChanges = true;
				break;
			case DATA_CHANGED:	//  Satz �ndern
				olRecord->Status = 0;	//  Status zur�cksetzen
				blOk &= ogBCD.SetRecord ( omTableName, "URNO", 
										  olRecord->Values[urnoIndex], 
										  olRecord->Values);
				blChanges = true;
				break;
			default:
				blOk = false;
				break;		//  darf nicht vorkommen
		}
	}
	if ( blOk && blChanges )
		blOk = ogBCD.Save( omTableName );
	theApp.EndWaitCursor ();
	if ( !blOk )
	{
		CString olErrorTxt = GetString(IDS_WRITE_TABLE_ERR);
		FileErrMsg ( GetActiveWindow (), pCHAR(olErrorTxt), pCHAR(omTableName), 
					 "<CGridRecordXChange::SaveRecords>", 
					 MB_ICONEXCLAMATION|MB_OK);
	}
	return blOk;
}


CLocationGridRecordXChange::CLocationGridRecordXChange
									  (char *pcpTableName, char *pcpAmountField, 
									   char *pcpResUrnoField, char *ResCodeField, 
									   CServiceGrid *popGrid ):
		CGridRecordXChange ( pcpTableName, pcpAmountField, pcpResUrnoField, 
						   	 ResCodeField, popGrid )
{
}

CString	CLocationGridRecordXChange::GetResCodeFieldEntry ( CString &opResTable, CString &opResCode, 
														   bool blStaticGroup ) 
{
	RESOURCETABLEINFO  *sopResTableInfo;
	CServiceCatalogDoc *popDoc = GetOpenDocument();
	ASSERT (popDoc );
	CString olReturn = opResTable;
	olReturn += ".";
	if  ( sopResTableInfo = popDoc->GetResourceTableInfo ( pCHAR(opResTable) ) )
		olReturn += sopResTableInfo->codefield;
	return olReturn;
}

int CLocationGridRecordXChange::SetService ( RecordSet *popService )
{
	CString olServiceUrno, olResCode, olAmount, olPrio, olREFT, olResTable;
	CString olGTabEntry, olResCodeField, olResUrno;
	int		ilResourceAnz = 0, i, ilDisplayed=0;
	RecordSet *polRecord;

	pomDisplayedService = popService;
	omRecordList.DeleteAll();
	if ( popService )
	{
		olServiceUrno = popService->Values[ogBCD.GetFieldIndex(pcgTableName,"URNO")];
		ogBCD.GetRecords( omTableName, "USRV", olServiceUrno, &omRecordList );
		ilResourceAnz = omRecordList.GetSize ();
		for ( i=0; (i<ilResourceAnz) && pomGrid; i++ )
		{
			polRecord = &omRecordList.ElementAt(i);
			olREFT = polRecord->Values[imResCodeInd];
			if ( !ParseResCodeFieldEntry ( olREFT, olResTable, olResCodeField ) )
				continue;
			olResUrno = polRecord->Values[imResUrnoInd];

			olGTabEntry = polRecord->Values[imGroupTabNameInd];
			if ( !olGTabEntry.IsEmpty () && (olGTabEntry!=" ") )
			{	//  es handelt sich um eine Gruppe
				CString olGrpTable = "SGR";
				CString olGrpCodeField = "GRPN";
				ParseResCodeFieldEntry ( olGTabEntry, olGrpTable, olGrpCodeField ) ;

				olResCode = ogBCD.GetField ( olGrpTable, "URNO", olResUrno, olGrpCodeField );
				olResCode = "*"+ olResCode;
			}
			else
				olResCode = ogBCD.GetField ( olResTable, "URNO", olResUrno, olResCodeField );

			olAmount = polRecord->Values[imAmountInd];
			olPrio = polRecord->Values[imPrioInd];
			if ( pomGrid->SetLineValues ( ilDisplayed, olResTable, olResCode, olAmount, olPrio ) )
				ilDisplayed++;
		}
	}
	return ilResourceAnz;
}

bool CLocationGridRecordXChange::ParseResCodeFieldEntry ( CString &entry, CString &opResTable, 
														  CString &opResCodeField ) 
{
	char *pclTemp, *ps;

	pclTemp = new char[entry.GetLength() + 1];
	if ( !pclTemp )
		return false;
	strcpy ( pclTemp, pCHAR(entry) );
	if ( ps = strchr ( pclTemp, '.' ) )
	{
		*ps = '\0';
		opResTable = pclTemp;
		opResCodeField = ps+1;
	}
	delete pclTemp;
	return (ps!=0);
}

//////////////////////////////////////////////////////////////////////
// CPersonalXChange Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CPersonalXChange::CPersonalXChange(	CFunctionsGrid *popFuncGrid,
								    CQualificationsGrid *popQualGrid )
{
	pomDisplayedService = 0;
	pomFuncGrid = popFuncGrid;
	pomQualGrid = popQualGrid;

	//  Initialize indices into RecordSet
	omTableName[0] = "SEF";		
	omTableName[1] = "SEQ";		
	omResourceTable[0] = "PFC";		
	omResourceTable[1] = "PER";		
	omResourceCodeField[0] = "FCTC";
	omResourceCodeField[1] = "PRMC";
	
	imPersCodeInd[0] = ogBCD.GetFieldIndex("SEF", "FCCO" );	//  Function
	imPersCodeInd[1] = ogBCD.GetFieldIndex("SEQ", "QUCO" );	//  Qualifikation
	
	imPersResUrnoInd[0] = ogBCD.GetFieldIndex("SEF", "UPFC" );
	imPersResUrnoInd[1] = ogBCD.GetFieldIndex("SEQ", "UPER" );

	imPersAmountInd[0] = ogBCD.GetFieldIndex("SEF", "FCNO" );
	imPersAmountInd[1] = ogBCD.GetFieldIndex("SEQ", "QUNO" );

	for ( int i=TYP_FUNC-1; i<TYP_QUALI; i++ )
	{
		imPersGroupTabNameInd[i] = ogBCD.GetFieldIndex(omTableName[i], "GTAB" );
		imPersPrioInd[i] = ogBCD.GetFieldIndex( omTableName[i], "PRIO" );	
		imPersServiceUrnoInd[i] = ogBCD.GetFieldIndex( omTableName[i], "USRV" );	
		imFieldsInPersRecord[i] = ogBCD.GetFieldCount ( omTableName[i] );
		imPersUrnoInd[i] = ogBCD.GetFieldIndex( omTableName[i], "URNO" );	
	}
	
	imFieldsInAssignRecord = ogBCD.GetFieldCount ( "SFQ" );
	imAssignUrnoInd = ogBCD.GetFieldIndex( "SFQ", "URNO" );	
}

CPersonalXChange::~CPersonalXChange()
{
	omFuncRecords.DeleteAll ();
	omQualRecords.DeleteAll ();
	omAssignRecords.DeleteAll ();
}

int CPersonalXChange::Data2RecordList (CPersonellData *popData)
{
	int				ilRecordAnz=0;

	ilRecordAnz = Data2PersRecords (popData, TYP_FUNC);
	ilRecordAnz += Data2PersRecords (popData, TYP_QUALI);
	Data2AssignRecords (popData);
	return ilRecordAnz;
}


int CPersonalXChange::Data2PersRecords (CPersonellData *popData, UINT ipTyp )
{
	int				i, ilRecordAnz=0;
	CString			olResUrno, olResCode, olPriority, olRecordUrno;
	CMapStringToPtr	olMapUsedUrno;
	RecordSet		*polRecord;	
	bool			blStaticGroup;
	PERSONALRECORD	*prlPersRec;
	int				k = ipTyp-1;		//  Index f�r Funktion / Qualifikation
	CCSPtrArray<RecordSet> *polRecordList;
	CCSPtrArray<PERSONALRECORD> *polDataList;

	if ( (k<0) || (k>1) )
		return 0;

	polRecordList = k==0 ? &omFuncRecords : &omQualRecords;
	polDataList =  k==0 ? &(popData->omFuncs) : &(popData->omQualis);

	//  index der Urno der Resource innerhalb des verwendeten Datensatzes bestimmen
	GetMapForRecordList ( *polRecordList, olMapUsedUrno, imPersUrnoInd[k] );
	
	//  Alle bisherigen Records mit DELETED vorbesetzen
	for ( i=0; i<polRecordList->GetSize(); i++ )
		(*polRecordList)[i].Status = DATA_DELETED;
	
	//  Update List of SEF-Records
	for ( i=0; i<polDataList->GetSize(); i++ )
	{
		prlPersRec = &(polDataList->ElementAt(i));
		if ( !prlPersRec || prlPersRec->code.IsEmpty() )
			continue;
		olResCode = prlPersRec->code;
		blStaticGroup = ( olResCode[0] == '*' );
		if ( blStaticGroup )
			olResCode = olResCode.Right ( olResCode.GetLength()-1 );

		if (blStaticGroup) 
			olResUrno = ogBCD.GetField( "SGR", "GRPN", olResCode, "URNO" );
		else
			olResUrno = ogBCD.GetField( omResourceTable[k], 
										omResourceCodeField[k],
										olResCode, "URNO" );

		olRecordUrno = prlPersRec->urno;
		olPriority = prlPersRec->prio;
		if ( !olRecordUrno.IsEmpty() &&
			 olMapUsedUrno.Lookup( olRecordUrno, (void *&)polRecord ) )
		{	//  Diese Resource wurde auch vorher verwendet
			if ( (polRecord->Values[imPersAmountInd[k]]!="1") ||
				 (polRecord->Values[imPersPrioInd[k]]!=olPriority) )
			{	//  werte haben sich ge�ndert
				polRecord->Values[imPersAmountInd[k]]="1";
				polRecord->Values[imPersPrioInd[k]]=olPriority;
				polRecord->Status = DATA_CHANGED;
			}
			else
				polRecord->Status = DATA_UNCHANGED;
		}
		else
		{	//  Resource kommt neu dazu
			polRecord = new RecordSet ( imFieldsInPersRecord[k] );
			polRecord->Values[imPersAmountInd[k]] = "1";
			polRecord->Values[imPersPrioInd[k]] = olPriority;
			polRecord->Values[imPersResUrnoInd[k]] = olResUrno; 
			if ( blStaticGroup )
			{
				polRecord->Values[imPersGroupTabNameInd[k]] = "SGR.GRPN";
				polRecord->Values[imPersCodeInd[k]] = 	"";
			}
			else
			{
				polRecord->Values[imPersGroupTabNameInd[k]] = "";
				polRecord->Values[imPersCodeInd[k]] = olResCode ;
			}
			olRecordUrno.Format("%lu",  ogBCD.GetNextUrno() );
			polRecord->Values[imPersUrnoInd[k]] = 
				prlPersRec->urno = olRecordUrno;
			polRecord->Status = DATA_NEW;
			polRecordList->Add ( polRecord );
		}
		ilRecordAnz ++;
	}
	return ilRecordAnz;
}


int CPersonalXChange::Data2AssignRecords (CPersonellData *popData)
{
	int				i, ilRecordAnz=0;
	CString			olRecordUrno;
	CMapStringToPtr	olMapUsedAssignUrno;
	RecordSet		*polRecord;	
	ASSIGNRECORD	*prlRec;
	int				ilFuncUrnoIdx, ilQualUrnoIdx;
		
	ilFuncUrnoIdx = ogBCD.GetFieldIndex( "SFQ", "USEF" );	
	ilQualUrnoIdx = ogBCD.GetFieldIndex( "SFQ", "USEQ" );	

	//  index der Urno der Resource innerhalb des verwendeten Datensatzes bestimmen
	GetMapForRecordList ( omAssignRecords, olMapUsedAssignUrno, 
						  imAssignUrnoInd );
	
	//  Alle bisherigen Records mit DELETED vorbesetzen
	for ( i=0; i<omAssignRecords.GetSize(); i++ )
		omAssignRecords[i].Status = DATA_DELETED;
	
	//  Update List of SFQ-Records
	for ( i=0; i<popData->omAssigns.GetSize(); i++ )
	{
		prlRec = &popData->omAssigns[i];
		if ( !prlRec || !prlRec->pFunc || !prlRec->pQual )
			continue;
		olRecordUrno = prlRec->urno;
		if ( !olRecordUrno.IsEmpty() &&
			 olMapUsedAssignUrno.Lookup( olRecordUrno, (void *&)polRecord ) )
		{	//  Diese Resource wurde auch vorher verwendet
			if ( (polRecord->Values[ilFuncUrnoIdx]!=prlRec->pFunc->urno) ||
				 (polRecord->Values[ilQualUrnoIdx]!=prlRec->pQual->urno) )
			{	//  werte haben sich ge�ndert
				polRecord->Values[ilFuncUrnoIdx]=prlRec->pFunc->urno;
				polRecord->Values[ilQualUrnoIdx]=prlRec->pQual->urno;
				polRecord->Status = DATA_CHANGED;
			}
			else
				polRecord->Status = DATA_UNCHANGED;
		}
		else
		{	//  Resource kommt neu dazu
			polRecord = new RecordSet ( imFieldsInAssignRecord );
			polRecord->Values[ilFuncUrnoIdx]=prlRec->pFunc->urno;
			polRecord->Values[ilQualUrnoIdx]=prlRec->pQual->urno;
			olRecordUrno.Format("%lu",  ogBCD.GetNextUrno() );
			polRecord->Values[imAssignUrnoInd] = 
				prlRec->urno = olRecordUrno;
			polRecord->Status = DATA_NEW;
			omAssignRecords.Add ( polRecord );
		}
		ilRecordAnz ++;
	}
	return ilRecordAnz;
}


//  Alle S�tze dieser Resource-Gruppe in SEQ, SEE, SEL, SER bzw SEG speichern
//	wenn pcpServiceUrno != 0, zuvor USRV auf pcpServiceUrno setzen
bool CPersonalXChange::SaveRecords ( char *pcpServiceUrno /*=0*/)
{
	bool blOk ;
	blOk = SaveRecordList ( &omFuncRecords, "SEF", pcpServiceUrno );
	blOk &= SaveRecordList ( &omQualRecords, "SEQ", pcpServiceUrno );
	blOk &= SaveRecordList ( &omAssignRecords, "SFQ", 0 );
	return blOk;
}

bool CPersonalXChange::SaveRecordList ( CCSPtrArray<RecordSet> *popRecords, 
										CString opTable, char *pcpServiceUrno )
{
	RecordSet *olRecord;
	bool	  blOk=TRUE, blChanges = FALSE;

	int urnoIndex = ogBCD.GetFieldIndex ( opTable, "URNO" );

	theApp.BeginWaitCursor ();
	for ( int i=0; i<popRecords->GetSize(); i++ )
	{
		olRecord = &popRecords->ElementAt(i);
		if ( !olRecord )
			continue;
		if ( pcpServiceUrno )
		{	
			int idx=ogBCD.GetFieldIndex(opTable,"USRV") ;
			if ( idx >= 0 )
				olRecord->Values[idx] = pcpServiceUrno;
		}
		
		switch ( olRecord->Status )
		{
			case DATA_DELETED:	//  Satz l�schen
				olRecord->Status = 0;	//  Status zur�cksetzen;
				blOk &= ogBCD.DeleteRecord ( opTable, "URNO", 
											 olRecord->Values[urnoIndex] );
				blChanges = true;
				break;
			case DATA_UNCHANGED:	//  nichts zu tun
				olRecord->Status = 0;	//  Status zur�cksetzen
				break;
			case DATA_NEW:	//  Satz einf�gen
				olRecord->Status = 0;	//  Status zur�cksetzen
				blOk &= ogBCD.InsertRecord(opTable, *olRecord, FALSE );
				blChanges = true;
				break;
			case DATA_CHANGED:	//  Satz �ndern
				olRecord->Status = 0;	//  Status zur�cksetzen
				blOk &= ogBCD.SetRecord ( opTable, "URNO", 
										  olRecord->Values[urnoIndex], 
										  olRecord->Values);
				blChanges = true;
				break;
			default:
				blOk = false;
				break;		//  darf nicht vorkommen
		}
	}
	if ( blOk && blChanges )
		blOk = ogBCD.Save( opTable );
	theApp.EndWaitCursor ();
	if ( !blOk )
	{
		CString olErrorTxt = GetString(IDS_WRITE_TABLE_ERR);
		FileErrMsg ( GetActiveWindow (), pCHAR(olErrorTxt), pCHAR(opTable), 
					 "<CPersonalXChange::SaveRecords>", 
					 MB_ICONEXCLAMATION|MB_OK);
	}
	return blOk;
}

void CPersonalXChange::ResetService ()
{
	omFuncRecords.DeleteAll();
	omQualRecords.DeleteAll();
	omAssignRecords.DeleteAll();
	pomDisplayedService =0;
}

int CPersonalXChange::SetService ( RecordSet *popService, CPersonellData *popData )
{
	CString		olServiceUrno; 
	int			ilResourceAnz=0;
	
	ResetService ();
	popData->Reset();

	pomDisplayedService = popService;

	if ( popService )
	{
		olServiceUrno = popService->Values[ogBCD.GetFieldIndex(pcgTableName,"URNO")];
		ogBCD.GetRecords( "SEF", "USRV", olServiceUrno, &omFuncRecords );
		ogBCD.GetRecords( "SEQ", "USRV", olServiceUrno, &omQualRecords );
		
		ilResourceAnz = Records2PersData ( popData, TYP_FUNC );
		ilResourceAnz += Records2PersData ( popData, TYP_QUALI );
		Records2AssignData ( popData );
	}
	//EnablePersonnelGrids ( ilResourceAnz>0 );
	pomFuncGrid->EnableGrid ( ilResourceAnz>0 );
	pomQualGrid->EnableGrid ( ilResourceAnz>0 );

	pomFuncGrid->UpdateDisplay();
	return ilResourceAnz;
}

int CPersonalXChange::Records2PersData (CPersonellData *popData, UINT ipTyp )
{
	CString		olResCode, olAmount, olPrio, olGTabEntry, olResUrno, olPersUrno;
	CCSPtrArray<RecordSet> *polRecordList;
	int		k = ipTyp-1;		//  Index f�r Funktion / Qualifikation
	int		i, ilResourceAnz ;
	UINT    ilCount, ilDisplayed=0;
	RecordSet *polRecord ;

	if ( (k<0) || (k>1) )
		return 0;

	polRecordList = k==0 ? &omFuncRecords : &omQualRecords;
	ilResourceAnz = polRecordList->GetSize();

	for ( i=0; i<ilResourceAnz; i++ )
	{
		polRecord = &(polRecordList->ElementAt(i) );
		olGTabEntry = polRecord->Values[imPersGroupTabNameInd[k]];
		if ( !olGTabEntry.IsEmpty () && (olGTabEntry!=" ") )
		{	//  es handelt sich um eine Gruppe
			CString olGrpTable = "SGR";
			CString olGrpCodeField = "GRPN";

			CLocationGridRecordXChange::ParseResCodeFieldEntry ( olGTabEntry, 
												olGrpTable, olGrpCodeField ) ;
			olResUrno = polRecord->Values[imPersResUrnoInd[k]] ; 
			olResCode = ogBCD.GetField ( olGrpTable, "URNO", olResUrno, olGrpCodeField );
			olResCode = "*"+ olResCode;
		}
		else
			olResCode = polRecord->Values[imPersCodeInd[k]];
		olPrio = polRecord->Values[imPersPrioInd[k]];
		olAmount = polRecord->Values[imPersAmountInd[k]];
		if ( !olAmount.IsEmpty() && ( olAmount!=' ' ) )
			ilCount = atoi ( olAmount );
		else
			ilCount = 1;
		olPersUrno = polRecord->Values[imPersUrnoInd[k]];
		popData->AddPersonal ( ipTyp, olResCode, olPersUrno, olPrio, ilCount );
		ilDisplayed += ilCount;
	}
	return ilDisplayed;
}

int CPersonalXChange::Records2AssignData ( CPersonellData *popData )
{
	CString			olFuncUrno, olQualUrno, olAssignUrno;
	int				i, ilAnz, ilRet=0;
	PERSONALRECORD	*prlFuncRec, *prlQualRec;
	int				ilFuncUrnoIdx, ilQualUrnoIdx;
	RecordSet		olRecord(imFieldsInAssignRecord);
		
	ilFuncUrnoIdx = ogBCD.GetFieldIndex( "SFQ", "USEF" );	
	ilQualUrnoIdx = ogBCD.GetFieldIndex( "SFQ", "USEQ" );	

	ilAnz = ogBCD.GetDataCount("SFQ");

	for ( i=0; i<ilAnz; i++ )
	{
		if ( !ogBCD.GetRecord ( "SFQ", i, olRecord  ) )
			continue;
		olFuncUrno = olRecord.Values[ilFuncUrnoIdx];
		olQualUrno = olRecord.Values[ilQualUrnoIdx];
		prlFuncRec = popData->FindPersonalByUrno ( TYP_FUNC, olFuncUrno ); 
		prlQualRec = popData->FindPersonalByUrno ( TYP_QUALI, olQualUrno );
		olAssignUrno = olRecord.Values[imAssignUrnoInd];
		if ( prlFuncRec && prlQualRec )
		{
			popData->AddAssignment ( olAssignUrno, prlFuncRec, prlQualRec );
			omAssignRecords.New(olRecord);
			ilRet ++;
		}
	}
	return ilRet;
}

void CPersonalXChange::OnServiceUrnoChanged ()
{
	omFuncRecords.DeleteAll();
	omQualRecords.DeleteAll();
	omAssignRecords.DeleteAll();
}
