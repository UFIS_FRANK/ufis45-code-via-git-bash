VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmJobDetails 
   Caption         =   "Job Details"
   ClientHeight    =   12705
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7035
   LinkTopic       =   "Form1"
   ScaleHeight     =   12705
   ScaleWidth      =   7035
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "Event Details"
      Height          =   9735
      Left            =   120
      TabIndex        =   2
      Top             =   2880
      Width           =   6855
      Begin TABLib.TAB tab_EVENTDETAILS 
         Height          =   9255
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   6495
         _Version        =   65536
         _ExtentX        =   11456
         _ExtentY        =   16325
         _StockProps     =   64
      End
   End
   Begin TABLib.TAB tab_R02TABEVENT 
      Height          =   495
      Left            =   4680
      TabIndex        =   1
      Tag             =   "{=TABLE=}R02TAB{=FIELDS=}ORNO,FLST,FVAL,USEC,TIME,SFKT,URNO"
      Top             =   960
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   873
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_EVENT 
      Height          =   2655
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   6855
      _Version        =   65536
      _ExtentX        =   12091
      _ExtentY        =   4683
      _StockProps     =   64
   End
End
Attribute VB_Name = "frmJobDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


'#######################################################################
'### SHOW THE JOB CHANGES FOR SELECTED JOB
'#######################################################################
Sub showJobEventDetails(jobOrnoSelected As String)
    
    Dim test As String
    Dim l As Integer
    Dim strFields As String
    Dim strValues As String
    Dim strLine As String
    Dim strQuery As String
    Dim strTemp As String
    
    'test = frmMain: gR02FieldDescList
    
    If initFlag = False Then InitEventTabs
    
    If (jobOrnoSelected <> "") Then
        
        MousePointer = vbHourglass
        
        strQuery = "where orno = '" & jobOrnoSelected & "'"
        
        tab_R02TABEVENT.ResetContent
        frmData.LoadData tab_R02TABEVENT, strQuery
        tab_R02TABEVENT.Visible = False
        
        tab_EVENT.ResetContent
        tab_EVENTDETAILS.ResetContent
        
        For l = 0 To tab_R02TABEVENT.GetLineCount - 1 Step 1
            
            'ornoTemp = tab_R02TABEVENT.GetColumnValues(l, 0)
        
            '### LOG COLUMNS
            strFields = tab_R02TABEVENT.GetColumnValues(l, 1)
            strFields = Replace(strFields, Chr(30), ",")
            strValues = tab_R02TABEVENT.GetColumnValues(l, 2)
            strValues = Replace(strValues, Chr(30), ",")
                       
            'strLine = User value
            strLine = tab_R02TABEVENT.GetColumnValues(l, 3)
            
            '### TIME
            strLine = strLine + "," & CedaFullDateToVb(tab_R02TABEVENT.GetColumnValues(l, 4))
            strLine = strLine + "," + tab_R02TABEVENT.GetColumnValues(l, 5)
            strTemp = SortedR02Fields(strValues, strFields)
            strLine = strLine + "," + strTemp
            
            'Add the record to Log tab
            tab_EVENT.InsertTextLine strLine, False
               
        Next l
            
        tab_EVENT.ShowHorzScroller True
        tab_EVENT.AutoSizeColumns
        'tab_JOBLOG.Sort 1, True, True
        tab_EVENT.EnableHeaderSizing True
        tab_EVENT.Refresh
        
        MousePointer = vbDefault
        
    End If
    
    'MsgBox ("In showJobEventDetails")
    'MsgBox ("Field list" & gR02FieldDescList)
    
End Sub


'#######################################################################
'### INIT TABLES FOR EVENT DETAILS FORM
'#######################################################################
Sub InitEventTabs()

    Dim i As Integer
    
    frmData.InitTabGeneral tab_R02TABEVENT
    frmData.InitTabForCedaConnection tab_R02TABEVENT
    tab_R02TABEVENT.AutoSizeColumns
    
    tab_EVENT.ResetContent
    tab_EVENT.HeaderString = gR02FieldDescList
    tab_EVENT.AutoSizeColumns
    tab_EVENT.Refresh
    
    tab_EVENTDETAILS.ResetContent
    tab_EVENTDETAILS.HeaderString = "FIELD,DATA"
    tab_EVENTDETAILS.HeaderLengthString = "250,250"
    tab_EVENTDETAILS.FontName = "Arial"
    tab_EVENTDETAILS.FontSize = 15
    'tab_EVENTDETAILS.AutoSizeColumns
    tab_EVENTDETAILS.Refresh
    
    initFlag = True
    
    
    'MsgBox (gR02items)
    
    'For i = 1 To gR02items
    '    tab_EVENTDETAILS.SetColumnValue i, 0, GetItem(gR02FieldDescList, i, ",")
         'MsgBox (GetItem(gR02FieldDescList, i, ","))
    'Next i
    
End Sub

Private Sub tab_EVENT_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    
    Dim l As Integer
    Dim i As Integer
    Dim strValuesCurrent As String
    Dim strFieldsCurrent As String
    Dim strValuesPrev As String
    Dim strPrev As String
    Dim strFieldsPrev As String
    Dim strKey As String
    Dim tmpVal As String
    Dim tmpValPrev As String
    
    l = tab_EVENT.GetCurrentSelected
    
    If l <> -1 Then
        
        MousePointer = vbHourglass
        
        tab_EVENTDETAILS.ResetContent
        
        For i = 1 To gR02items
            tab_EVENTDETAILS.SetColumnValue i, 0, GetItem(gR02FieldDescList, i, ",")
        Next i
        
        'Assign compulsory values
        tab_EVENTDETAILS.SetColumnValue 0, 1, tab_R02TABEVENT.GetColumnValues(l, 3)
        tab_EVENTDETAILS.SetColumnValue 1, 1, CedaFullDateToVb(tab_R02TABEVENT.GetColumnValues(l, 4))
        tab_EVENTDETAILS.SetColumnValue 2, 1, tab_R02TABEVENT.GetColumnValues(l, 5)
        
        ' Need to check whether any updates for each field or not? if so change the color of the fields.
        If (l <> 0) Then
        
            strFieldsCurrent = tab_R02TABEVENT.GetColumnValues(l, 1)
            strFieldsCurrent = Replace(strFieldsCurrent, Chr(30), ",")
            strValuesCurrent = tab_R02TABEVENT.GetColumnValues(l, 2)
            strValuesCurrent = Replace(strValuesCurrent, Chr(30), ",")
            
            strFieldsPrev = tab_R02TABEVENT.GetColumnValues(l - 1, 1)
            strFieldsPrev = Replace(strFieldsPrev, Chr(30), ",")
            strValuesPrev = tab_R02TABEVENT.GetColumnValues(l - 1, 2)
            strValuesPrev = Replace(strValuesPrev, Chr(30), ",")
            
            For i = 3 To gR02items Step 1
                strKey = gR02TAB(i)
                tmpVal = GetFieldValue(strKey, strValuesCurrent, strFieldsCurrent)
                tmpValPrev = GetFieldValue(strKey, strValuesPrev, strFieldsPrev)
                
                
                If tmpVal <> tmpValPrev And tmpVal <> "" Then tab_EVENTDETAILS.SetLineColor i, H000080FF, &HC0C0FF
                
                If InStr(1, "TIME,CDAT,LSTU", strKey) > 0 Then
                    If tmpVal <> "" Then
                        tmpVal = CedaFullDateToVb(tmpVal)
                    End If
                End If
                
                If InStr(1, "ACFR,ACTO,PLFR,PLTO", strKey) > 0 Then
                    If tmpVal <> "" Then
                        tmpVal = DateAdd("h", gUTCOffset, CedaFullDateToVb(tmpVal))
                    End If
                End If
                
                'UTPL - URNO Template
                If strKey = "UTPL" Then
                    tmpVal = frmMain.tab_TPLTAB.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_TPLTAB.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                'UJTY - URNO JOBTYPE
                If strKey = "UJTY" Then
                    tmpVal = frmMain.tab_JTYTAB1.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_JTYTAB1.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                'USTF - Urno Staff - get Staff info & show
                If strKey = "USTF" Then
                    tmpVal = frmMain.tab_STFCOMPLETE.GetLinesByColumnValue(1, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_STFCOMPLETE.GetColumnValue(CLng(tmpVal), 0)
                End If
                
                'UGHS - Service Name
                If strKey = "UGHS" Then
                    tmpVal = frmMain.tab_SERTAB.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" And tmpVal <> "0" Then tmpVal = frmMain.tab_SERTAB.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                tab_EVENTDETAILS.SetColumnValue i, 1, tmpVal
                
            Next i
            
        Else
        
            strFieldsCurrent = tab_R02TABEVENT.GetColumnValues(l, 1)
            strFieldsCurrent = Replace(strFieldsCurrent, Chr(30), ",")
            strValuesCurrent = tab_R02TABEVENT.GetColumnValues(l, 2)
            strValuesCurrent = Replace(strValuesCurrent, Chr(30), ",")
        
            For i = 3 To gR02items Step 1
                strKey = gR02TAB(i)
                tmpVal = GetFieldValue(strKey, strValuesCurrent, strFieldsCurrent)
                
                If InStr(1, "TIME,CDAT,LSTU", strKey) > 0 Then
                    If tmpVal <> "" Then
                        tmpVal = CedaFullDateToVb(tmpVal)
                    End If
                End If
                
                If InStr(1, "ACFR,ACTO,PLFR,PLTO", strKey) > 0 Then
                    If tmpVal <> "" Then
                        tmpVal = DateAdd("h", gUTCOffset, CedaFullDateToVb(tmpVal))
                    End If
                End If
                
                'UTPL - URNO Template
                If strKey = "UTPL" Then
                    tmpVal = frmMain.tab_TPLTAB.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_TPLTAB.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                'UJTY - URNO JOBTYPE
                If strKey = "UJTY" Then
                    tmpVal = frmMain.tab_JTYTAB1.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_JTYTAB1.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                'USTF - Urno Staff - get Staff info & show
                If strKey = "USTF" Then
                    tmpVal = frmMain.tab_STFCOMPLETE.GetLinesByColumnValue(1, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_STFCOMPLETE.GetColumnValue(CLng(tmpVal), 0)
                End If
                
                'UGHS - Service Name
                If strKey = "UGHS" Then
                    tmpVal = frmMain.tab_SERTAB.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_SERTAB.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                tab_EVENTDETAILS.SetColumnValue i, 1, tmpVal
                
            Next i
            
            'For i = 1 To gR02items - 2
            '    tab_EVENTDETAILS.SetColumnValue i + 2, 1, GetItem(strValues, i, ",")
            'Next i
        
        End If
            
        'tab_EVENTDETAILS.InsertBuffer
        
        tab_EVENTDETAILS.Refresh
        MousePointer = vbDefault
        
        
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    initFlag = False
End Sub
