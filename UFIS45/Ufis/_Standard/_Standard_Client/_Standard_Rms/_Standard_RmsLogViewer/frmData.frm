VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmData 
   Caption         =   "frmData"
   ClientHeight    =   9675
   ClientLeft      =   165
   ClientTop       =   480
   ClientWidth     =   16320
   LinkTopic       =   "Form1"
   ScaleHeight     =   9675
   ScaleWidth      =   16320
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin TABLib.TAB tab_ALOTAB 
      Height          =   2295
      Left            =   9960
      TabIndex        =   18
      Tag             =   "{=TABLE=}ALOTAB{=FIELDS=}URNO,ALOC"
      Top             =   6600
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   4048
      _StockProps     =   64
   End
   Begin VB.Timer TimerReloadAft 
      Interval        =   60000
      Left            =   10080
      Top             =   120
   End
   Begin TABLib.TAB tab_CONFIG 
      Height          =   975
      Left            =   6420
      TabIndex        =   6
      Top             =   480
      Width           =   3285
      _Version        =   65536
      _ExtentX        =   5794
      _ExtentY        =   1720
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_AFTTAB 
      Height          =   2655
      Left            =   0
      TabIndex        =   4
      Tag             =   "{=TABLE=}AFTTAB{=FIELDS=}URNO,FLNO,STOD,DES3,TIFD"
      Top             =   3480
      Width           =   6705
      _Version        =   65536
      _ExtentX        =   11827
      _ExtentY        =   4683
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_HATTAB 
      Height          =   1050
      Left            =   105
      TabIndex        =   0
      Tag             =   "{=TABLE=}HATTAB{=FIELDS=}URNO,NAME,PASW,LGIN,LGOU"
      Top             =   435
      Width           =   5820
      _Version        =   65536
      _ExtentX        =   10266
      _ExtentY        =   1852
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_HCFTAB 
      Height          =   810
      Left            =   120
      TabIndex        =   2
      Tag             =   "{=TABLE=}HCFTAB{=FIELDS=}URNO,UHAT,SERL,ALC2,GRPT,COMD,EVNT,TIMI,TIHR,STAT,ACTN,CMDN,NPGL,NPGC,NPGK,LPGL,LPGC,LPGK"
      Top             =   1920
      Width           =   5745
      _Version        =   65536
      _ExtentX        =   10134
      _ExtentY        =   1429
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_SEVTAB 
      Height          =   435
      Left            =   6960
      TabIndex        =   10
      Tag             =   "{=TABLE=}SEVTAB{=FIELDS=}UAFT,STAT"
      Top             =   3600
      Width           =   1725
      _Version        =   65536
      _ExtentX        =   3043
      _ExtentY        =   767
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_ORGTAB 
      Height          =   2235
      Left            =   240
      TabIndex        =   12
      Tag             =   "{=TABLE=}ORGTAB{=FIELDS=}DPT1"
      Top             =   6600
      Width           =   1605
      _Version        =   65536
      _ExtentX        =   2831
      _ExtentY        =   3942
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_SORTAB 
      Height          =   2235
      Left            =   2040
      TabIndex        =   14
      Tag             =   "{=TABLE=}SORTAB{=FIELDS=}CODE,SURN,VPFR,VPTO"
      Top             =   6600
      Width           =   3765
      _Version        =   65536
      _ExtentX        =   6641
      _ExtentY        =   3942
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_TPLTAB 
      Height          =   2235
      Left            =   6120
      TabIndex        =   16
      Tag             =   "{=TABLE=}TPLTAB{=FIELDS=}TNAM,APPL,TPST,URNO"
      Top             =   6600
      Width           =   1605
      _Version        =   65536
      _ExtentX        =   2831
      _ExtentY        =   3942
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_SYSTAB 
      Height          =   2235
      Left            =   7920
      TabIndex        =   17
      Tag             =   "{=TABLE=}SYSTAB{=FIELDS=}TANA,FINA,LOGD,ADDI,URNO"
      Top             =   6600
      Width           =   1605
      _Version        =   65536
      _ExtentX        =   2831
      _ExtentY        =   3942
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_DPXTAB 
      Height          =   2595
      Left            =   10170
      TabIndex        =   19
      Tag             =   "{=TABLE=}DXPTAB{=FIELDS=}PRID, NAME, PRMT, FLNU, TFLU, USEC, LSTU"
      Top             =   3840
      Width           =   6015
      _Version        =   65536
      _ExtentX        =   10610
      _ExtentY        =   4577
      _StockProps     =   64
   End
   Begin VB.Label Label9 
      Caption         =   "DPXTAB :"
      Height          =   360
      Left            =   10170
      TabIndex        =   20
      Top             =   3420
      Width           =   3150
   End
   Begin VB.Label Label8 
      Caption         =   "SORTAB"
      Height          =   360
      Left            =   2040
      TabIndex        =   15
      Top             =   6360
      Width           =   3150
   End
   Begin VB.Label Label7 
      Caption         =   "ORGTAB"
      Height          =   360
      Left            =   240
      TabIndex        =   13
      Top             =   6360
      Width           =   3150
   End
   Begin VB.Label Label6 
      Caption         =   "SEVTAB (ScheduledEventsTab):"
      Height          =   360
      Left            =   6960
      TabIndex        =   11
      Top             =   3435
      Width           =   3150
   End
   Begin VB.Label lblAftWhere 
      Caption         =   "where tifd between xxx and yyy order by stod"
      Height          =   510
      Left            =   2145
      TabIndex        =   9
      Top             =   2760
      Width           =   7830
   End
   Begin VB.Label Label5 
      Caption         =   "Last AFT-where statement:"
      Height          =   285
      Left            =   45
      TabIndex        =   8
      Top             =   2775
      Width           =   1980
   End
   Begin VB.Label Label4 
      Caption         =   "Config-File:"
      Height          =   240
      Left            =   6390
      TabIndex        =   7
      Top             =   120
      Width           =   2580
   End
   Begin VB.Label Label3 
      Caption         =   "AFTTAB:"
      Height          =   195
      Left            =   45
      TabIndex        =   5
      Top             =   3120
      Width           =   765
   End
   Begin VB.Label Label2 
      Caption         =   "HCFTAB (Has ConFiguration table):"
      Height          =   225
      Left            =   105
      TabIndex        =   3
      Top             =   1560
      Width           =   3405
   End
   Begin VB.Label Label1 
      Caption         =   "HATTAB (Has Authorization Table):"
      Height          =   225
      Left            =   105
      TabIndex        =   1
      Top             =   90
      Width           =   3405
   End
End
Attribute VB_Name = "frmData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private strFieldSeparator As String
Private strConfigSepa As String
Private colURNOs As New Collection
Private strHOPO As String
Private strTableExt As String
Private strServer As String
Private strAccessMethod As String
Private sglUTCOffsetHours As Single
Private strConfigFile As String
Private ilCntMinutes As Integer
Private ilReloadMinutes As Integer
Private strTemplatePath As String
Private ilOffset As Integer

'#######################################################################
'# INITIAL LOAD
'#######################################################################
Public Sub InitialLoadData()
    Me.MousePointer = vbHourglass
    Dim strAftWhere As String

    '### Load Basic Data for RMS LOG VIEWER
    
    '### ORG UNITS
    '### ORGTAB
    'InitTabGeneral tab_ORGTAB
    'InitTabForCedaConnection tab_ORGTAB
    'LoadData tab_ORGTAB, ""
    'tab_HATTAB.AutoSizeColumns

    '### LINKS ORG UNIT TO STAFF
    '### SORTAB
    InitTabGeneral tab_SORTAB
    InitTabForCedaConnection tab_SORTAB
    'LoadData tab_ORGTAB, ""
    'tab_ORGTAB.AutoSizeColumns
    'tab_ORGTAB.Sort 0, True, False
        
    InitTabGeneral tab_TPLTAB
    InitTabForCedaConnection tab_TPLTAB
    LoadData tab_TPLTAB, "where tpst='1' and appl='RULE_AFT'"
    tab_TPLTAB.AutoSizeColumns
    tab_TPLTAB.Sort 0, True, False
    tab_TPLTAB.Refresh
    
    InitTabGeneral tab_SYSTAB
    InitTabForCedaConnection tab_SYSTAB
    
    InitTabGeneral tab_ALOTAB
    InitTabForCedaConnection tab_ALOTAB
    InitTabGeneral tab_DPXTAB
    
    LoadData tab_ALOTAB, ""
    tab_ALOTAB.AutoSizeColumns
    tab_ALOTAB.Sort 0, True, False
    tab_ALOTAB.Refresh
        
        
    Me.MousePointer = vbDefault
    
Exit Sub
    
    strAftWhere = GenerateAftWhere()

    InitTabGeneral tab_HATTAB
    InitTabGeneral tab_HCFTAB
    InitTabGeneral tab_AFTTAB
    InitTabGeneral tab_SEVTAB
    
    InitTabForCedaConnection tab_HATTAB
    InitTabForCedaConnection tab_HCFTAB
    InitTabForCedaConnection tab_AFTTAB
    InitTabForCedaConnection tab_SEVTAB
    InitTabForCedaConnection tab_DPXTAB
    
    LoadData tab_HATTAB, ""
    LoadData tab_HCFTAB, ""
    LoadData tab_AFTTAB, strAftWhere
    LoadData tab_SEVTAB, "WHERE UAFT IN (" & tab_AFTTAB.SelectDistinct("0", "", "", ",", False) & ") AND STAT IN ('ACTIVE','CANCELLED')"
    LoadData tab_DPXTAB, ""
    

    tab_HATTAB.AutoSizeColumns
    tab_HCFTAB.AutoSizeColumns
    tab_AFTTAB.AutoSizeColumns
    tab_SEVTAB.AutoSizeColumns
    tab_DPXTAB.AutoSizeColumns
    
    'starting the timer to reload every 60 minutes the AFT-data
    ilCntMinutes = 0
    TimerReloadAft.Enabled = True
    Me.MousePointer = vbDefault
End Sub

Public Sub InitTabGeneral(ByRef rTab As TABLib.TAB)
    rTab.ResetContent
    rTab.FontName = "Courier New"
    rTab.HeaderFontSize = "18"
    rTab.FontSize = "16"
    rTab.EnableHeaderSizing True
    rTab.ShowHorzScroller True
    rTab.AutoSizeByHeader = True
    rTab.SetFieldSeparator strFieldSeparator

    Dim ilCnt As Integer
    Dim i As Integer
    Dim strFields As String
    Dim strHeaderLengthString As String
    GetKeyItem strFields, rTab.Tag, "{=FIELDS=}", "{="
    ilCnt = ItemCount(strFields, ",")
    For i = 1 To ilCnt Step 1
        strHeaderLengthString = strHeaderLengthString & "10,"
    Next i
    strHeaderLengthString = Left(strHeaderLengthString, Len(strHeaderLengthString) - 1)
    rTab.HeaderLengthString = strHeaderLengthString

    strFields = Replace(strFields, ",", strFieldSeparator)
    rTab.HeaderString = strFields
End Sub

Public Sub InitTabForCedaConnection(ByRef rTab As TABLib.TAB)
    'do the initializing for the CEDA-connection
    rTab.CedaServerName = strServer
    rTab.CedaPort = "3357"
    rTab.CedaHopo = strHOPO
    rTab.CedaCurrentApplication = gsAppName
    rTab.CedaTabext = strTableExt
    rTab.CedaUser = gsUserName
    rTab.CedaWorkstation = frmMain.AATLoginControl1.GetWorkStationName
    rTab.CedaSendTimeout = "120"
    rTab.CedaReceiveTimeout = "240"
    rTab.CedaRecordSeparator = Chr(10)
    rTab.CedaIdentifier = gsAppName
End Sub

Public Sub LoadData(ByRef rTab As TABLib.TAB, ByRef pWhere As String)
    Dim strCommand As String
    Dim strTable As String
    Dim strFieldList As String
    Dim strData As String
    Dim strWhere As String

    strCommand = "RT"
    
    GetKeyItem strTable, rTab.Tag, "{=TABLE=}", "{="
    GetKeyItem strFieldList, rTab.Tag, "{=FIELDS=}", "{="
    strData = ""
    strWhere = pWhere
    If Len(strWhere) = 0 Then
        GetKeyItem strWhere, rTab.Tag, "{=WHERE=}", "{="
    End If
    rTab.CedaAction strCommand, strTable, strFieldList, strData, strWhere
End Sub

Public Function GetUrno() As String
    Dim strRet As String
    ' do we have any reserved URNOs in "colURNOs"?
    If colURNOs.count < 1 Then
        ' no - we have to load some from the server
        Dim strURNOs As String
        Dim llRecords As Long
        Dim i As Integer

        ConnectUfisCom
        If frmMain.UfisCom1.CallServer("GMU", "", "*", "20", "", "360") = 0 Then
            llRecords = frmMain.UfisCom1.GetBufferCount
            If llRecords > 1 Then
                For i = 0 To llRecords - 1
                    colURNOs.Add GetItem(frmMain.UfisCom1.GetBufferLine(0), 1, ",")
                Next i
            Else
                strURNOs = frmMain.UfisCom1.GetBufferLine(0)
                llRecords = ItemCount(strURNOs, ",")
                For i = 1 To llRecords
                    colURNOs.Add GetItem(strURNOs, i, ",")
                Next i
            End If
        End If
        DisconnetUfisCom
    End If
    strRet = colURNOs.Item(1)
    colURNOs.Remove 1

    GetUrno = strRet
End Function

Private Sub ConnectUfisCom()
    frmMain.UfisCom1.SetCedaPerameters frmMain.AATLoginControl1.GetUserName, strHOPO, strTableExt
    frmMain.UfisCom1.InitCom strServer, strAccessMethod
End Sub

Private Sub DisconnetUfisCom()
    frmMain.UfisCom1.CleanupCom
End Sub

Public Sub InternalSaveHATTAB(ByRef rTab As TABLib.TAB)
    'View Fields: Name,Password,Login,Logout
    '{=TABLE=}HATTAB{=FIELDS=}URNO,NAME,PASW,LGIN,LGOU
    Dim strLineNo As String
    Dim strLine As String
    Dim strUrnoLine As String
    Dim strURNO As String
    Dim ilCnt As Integer
    Dim i As Integer
    Dim ilIdx As Integer
    Dim strData As String

    strLineNo = rTab.GetLinesByBackColor(vbGreen)
    ilCnt = ItemCount(strLineNo, ",")
    For i = 1 To ilCnt Step 1
        strLine = GetItem(strLineNo, i, ",")
        strURNO = rTab.GetLineTagKeyItem(CLng(strLine), "", "{=URNO=}", "{=")

        'strTmp = rTab.GetColumnValue(CLng(strLine), 1) ' chr(178)
        strData = strURNO & strFieldSeparator & rTab.GetColumnValue(CLng(strLine), 0) & _
                strFieldSeparator & EncryptString(rTab.GetColumnValue(CLng(strLine), 1)) & _
                strFieldSeparator & rTab.GetColumnValue(CLng(strLine), 2) & _
                strFieldSeparator & rTab.GetColumnValue(CLng(strLine), 3)

        While InStr(1, strData, strFieldSeparator & strFieldSeparator, vbBinaryCompare) > 1
            strData = Replace(strData, strFieldSeparator & strFieldSeparator, strFieldSeparator & " " & strFieldSeparator)
        Wend

        strUrnoLine = tab_HATTAB.GetLinesByColumnValue(0, strURNO, 0)
        If IsNumeric(strUrnoLine) = True Then
            'update
            tab_HATTAB.UpdateTextLine CLng(strUrnoLine), strData, False
            tab_HATTAB.SetLineTag CLng(strUrnoLine), "UPDATE"
        Else
            'insert
            tab_HATTAB.InsertTextLine strData, False
            tab_HATTAB.SetLineTag tab_HATTAB.GetLineCount - 1, "INSERT"
        End If
        rTab.SetLineColor CLng(strLine), vbBlack, vbWhite
    Next i

    tab_HATTAB.RedrawTab
End Sub

Private Sub Form_Load()
    ilOffset = 42
    strFieldSeparator = ","
    strConfigSepa = Chr(15)
    strServer = GetIniEntry("", gsAppName, "GLOBAL", "HOSTNAME", "XXX")
    strHOPO = GetIniEntry("", gsAppName, "GLOBAL", "HOMEAIRPORT", "XXX")
    strTableExt = GetIniEntry("", gsAppName, "GLOBAL", "TABLEEXTENSION", "TAB")
    strAccessMethod = "CEDA"
    strConfigFile = GetIniEntry("", gsAppName, "GLOBAL", "CONFIG_FILE", "XXX")
    sglUTCOffsetHours = GetUTCOffset
End Sub



Private Sub tab_HATTAB_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    MsgBox "LineTag: " & tab_HATTAB.GetLineTag(LineNo)
End Sub

Public Sub UpdateDB()
    ConnectUfisCom
    frmMain.StatusBar1.Panels(1).Text = "Saving authentification (HATTAB) inserts..."
    frmMain.Refresh
    WriteToDB tab_HATTAB, "INSERT", "IRT"
    frmMain.StatusBar1.Panels(1).Text = "Saving authentification (HATTAB) updates..."
    frmMain.Refresh
    WriteToDB tab_HATTAB, "UPDATE", "URT"
    frmMain.StatusBar1.Panels(1).Text = "Saving authentification (HATTAB) deletes..."
    frmMain.Refresh
    WriteToDB tab_HATTAB, "DELETE", "DRT"

    frmMain.StatusBar1.Panels(1).Text = "Saving configuration (HCFTAB) inserts..."
    frmMain.Refresh
    WriteToDB tab_HCFTAB, "INSERT", "IRT"
    frmMain.StatusBar1.Panels(1).Text = "Saving configuration (HCFTAB) updates..."
    frmMain.Refresh
    WriteToDB tab_HCFTAB, "UPDATE", "URT"
    frmMain.StatusBar1.Panels(1).Text = "Saving configuration (HCFTAB) deletes..."
    frmMain.Refresh
    WriteToDB tab_HCFTAB, "DELETE", "DRT"
    frmMain.StatusBar1.Panels(1).Text = "Ready."
    frmMain.Refresh
    DisconnetUfisCom
End Sub

Private Sub WriteToDB(ByRef rTab As TABLib.TAB, ByRef strSearchLineTag As String, ByRef strCmd As String)
    Dim strLineNo As String
    Dim strLine As String
    Dim strFields As String
    Dim strTable As String
    Dim strWhere As String
    Dim strData As String
    Dim i As Integer

    GetKeyItem strFields, rTab.Tag, "{=FIELDS=}", "{="
    GetKeyItem strTable, rTab.Tag, "{=TABLE=}", "{="

    For i = rTab.GetLineCount - 1 To 0 Step -1
        If rTab.GetLineTag(CLng(i)) = strSearchLineTag Then
            strData = rTab.GetLineValues(CLng(i))
            strWhere = "WHERE URNO = " & rTab.GetColumnValue(CLng(i), 0)
            While InStr(1, strData, ",,") > 0
                strData = Replace(strData, ",,", ", ,")
            Wend
            If Right(strData, 1) = "," Then
                strData = strData & " "
            End If
            If frmMain.UfisCom1.CallServer(strCmd, strTable, strFields, strData, strWhere, "360") <> 0 Then
                MsgBox frmMain.UfisCom1.LastErrorMessage, vbCritical
                Exit Sub
            End If

            If strSearchLineTag = "DELETE" Then
                rTab.DeleteLine CLng(i)
            Else
                rTab.SetLineTag CLng(i), ""
            End If
        End If
    Next i
End Sub



Public Sub InternalSaveHCFTAB(ByRef rTab As TABLib.TAB)
    'View Fields: Seq.,ALC2,Group,Cmd,Event,Timer min.,Timer hrs.,Status,ACTN
    '{=TABLE=}HCFTAB{=FIELDS=}URNO,UHAT,SERL,ALC2,GRPT,COMD,EVNT,TIMI,TIHR,STAT,ACTN
    Dim strLineNo As String
    Dim strLine As String
    Dim strUrnoLine As String
    Dim strURNO As String
    Dim strUHAT As String
    Dim ilCnt As Integer
    Dim i As Integer
    Dim j As Integer
    Dim ilIdx As Integer
    Dim strData As String

    strLineNo = rTab.GetLinesByBackColor(vbGreen)
    ilCnt = ItemCount(strLineNo, ",")
    For i = 1 To ilCnt Step 1
        strLine = GetItem(strLineNo, i, ",")
        strURNO = rTab.GetLineTagKeyItem(CLng(strLine), "", "{=URNO=}", "{=")
        strUHAT = rTab.GetLineTagKeyItem(CLng(strLine), "", "{=UHAT=}", "{=")

        strData = strURNO & "|SEPA|" & strUHAT
        For j = 0 To 15 Step 1
            strData = strData & "|SEPA|" & rTab.GetColumnValue(CLng(strLine), j)
        Next j

        ' replace "," with "�"
        strData = Replace(strData, ",", Chr(178))

        ' replace "|SEPA|" with strFieldSeparator (probably ",")
        strData = Replace(strData, "|SEPA|", strFieldSeparator)
        While InStr(1, strData, strFieldSeparator & strFieldSeparator, vbBinaryCompare) > 1
            strData = Replace(strData, strFieldSeparator & strFieldSeparator, strFieldSeparator & " " & strFieldSeparator)
        Wend
        strUrnoLine = tab_HCFTAB.GetLinesByColumnValue(0, strURNO, 0)
        If IsNumeric(strUrnoLine) = True Then
            'update
            tab_HCFTAB.UpdateTextLine CLng(strUrnoLine), strData, False
            tab_HCFTAB.SetLineTag CLng(strUrnoLine), "UPDATE"
        Else
            'insert
            tab_HCFTAB.InsertTextLine strData, False
            tab_HCFTAB.SetLineTag tab_HCFTAB.GetLineCount - 1, "INSERT"
        End If
        rTab.SetLineColor CLng(strLine), vbBlack, vbWhite
    Next i

    tab_HCFTAB.RedrawTab
End Sub

Public Sub DeleteInternal(ByRef rTab As TABLib.TAB, ByRef strURNO As String)
    Dim strLineNo As String
    Dim strLine As String
    Dim ilCnt As Integer
    Dim i As Integer

    strLineNo = rTab.GetLinesByColumnValue(0, strURNO, 0)
    ilCnt = ItemCount(strLineNo, ",")
    For i = 1 To ilCnt Step 1
        strLine = GetItem(strLineNo, i, ",")
        rTab.SetLineTag CLng(strLine), "DELETE"
    Next i

    strLineNo = rTab.GetLinesByColumnValue(1, strURNO, 0)
    ilCnt = ItemCount(strLineNo, ",")
    For i = 1 To ilCnt Step 1
        strLine = GetItem(strLineNo, i, ",")
        rTab.SetLineTag CLng(strLine), "DELETE"
    Next i
End Sub

Public Sub GenerateView_HATTAB(ByRef rTab As TABLib.TAB)
    'View Fields: Name,Password,Login,Logout
    '{=TABLE=}HATTAB{=FIELDS=}URNO,NAME,PASW,LGIN,LGOU

    Dim ilCnt As Integer
    Dim i As Integer
    Dim j As Integer

    Dim strLine As String
    Dim strSep As String

    rTab.ResetContent
    strSep = rTab.GetFieldSeparator
    ilCnt = tab_HATTAB.GetLineCount - 1

    For i = 0 To ilCnt Step 1
        strLine = ""
        For j = 1 To 4 Step 1
            If j = 2 Then
                strLine = strLine & DecryptString(tab_HATTAB.GetColumnValue(CLng(i), CLng(j))) & strSep
            Else
                strLine = strLine & tab_HATTAB.GetColumnValue(CLng(i), CLng(j)) & strSep
            End If
        Next j
        strLine = Left(strLine, Len(strLine) - Len(strSep))
        rTab.InsertTextLine strLine, False
        rTab.SetLineTag rTab.GetLineCount - 1, "{=URNO=}" & tab_HATTAB.GetColumnValue(CLng(i), 0)
    Next i
End Sub

Public Sub GenerateView_HCFTAB(ByRef rTab As TABLib.TAB, ByRef strUHAT As String)
    'View Fields: Seq.,ALC2,Group,Cmd,Event,Timer min.,Timer hrs.,Status,ACTN
    '{=TABLE=}HCFTAB{=FIELDS=}URNO,UHAT,SERL,ALC2,GRPT,COMD,EVNT,TIMI,TIHR,STAT,ACTN
    Dim ilCnt As Integer
    Dim i As Integer
    Dim j As Integer

    Dim llLine As Long

    Dim strLineNo As String
    Dim strLine As String
    Dim strLineTag As String
    Dim strSep As String

    rTab.ResetContent
    strSep = rTab.GetFieldSeparator
    strLineNo = tab_HCFTAB.GetLinesByColumnValue(1, strUHAT, 0)
    ilCnt = ItemCount(strLineNo, ",")

    For i = 1 To ilCnt Step 1
        llLine = CLng(GetItem(strLineNo, i, ","))
        strLine = ""

        For j = 2 To 17 Step 1
            strLine = strLine & tab_HCFTAB.GetColumnValue(CLng(llLine), j) & "|SEPA|"
        Next j
        strLine = Left(strLine, Len(strLine) - Len("|SEPA|"))

        ' replace "�" with ","
        strLine = Replace(strLine, Chr(178), ",")

        ' replace "|SEPA|" with strFieldSeparator (probably ",")
        strLine = Replace(strLine, "|SEPA|", strSep)

        rTab.InsertTextLine strLine, False
        strLineTag = "{=URNO=}" & tab_HCFTAB.GetColumnValue(llLine, 0) & "{=UHAT=}" & tab_HCFTAB.GetColumnValue(llLine, 1)
        rTab.SetLineTag rTab.GetLineCount - 1, strLineTag
    Next i
End Sub


Private Function GetUTCOffset() As Single
    Dim strUtcArr() As String
    Dim strArr() As String
    Dim CurrStr As String
    Dim tmpStr As String

    Dim istrRet As Integer
    Dim count As Integer
    Dim i As Integer

    GetUTCOffset = 0

    ConnectUfisCom

    If frmMain.UfisCom1.CallServer("GFR", "", "", "", "[CONFIG]", "360") = 0 Then
        tmpStr = frmMain.UfisCom1.GetDataBuffer(True)
    End If
    strArr = Split(tmpStr, Chr(10))
    count = UBound(strArr)
    For i = 0 To count
        CurrStr = strArr(i)
        istrRet = InStr(1, CurrStr, "UTCD", 0)
        If istrRet <> 0 Then
            strUtcArr = Split(strArr(i), ",")
            GetUTCOffset = CSng(strUtcArr(1) / 60)
            Exit For
        End If
    Next i
    DisconnetUfisCom
End Function

Private Function GenerateAftWhere() As String
    Dim olFrom As Date
    Dim olTo As Date

    Dim ilStart As Integer
    Dim ilEnd As Integer
    
    Dim strRet As String
    Dim strLineNoStart As String
    Dim strLineNoEnd As String

    strLineNoStart = tab_CONFIG.GetLinesByColumnValue(0, "TIMEFRAME_START", 0)
    strLineNoEnd = tab_CONFIG.GetLinesByColumnValue(0, "TIMEFRAME_END", 0)

    If IsNumeric(strLineNoStart) = True And IsNumeric(strLineNoEnd) = True Then
        ilStart = tab_CONFIG.GetColumnValue(CLng(strLineNoStart), 1)
        ilEnd = tab_CONFIG.GetColumnValue(CLng(strLineNoEnd), 1)

        olFrom = Now() + (ilStart / 24) + (sglUTCOffsetHours / 24)
        olTo = Now() + (ilEnd / 24) + (sglUTCOffsetHours / 24)

        Dim strFormat As String
        strFormat = "yyyymmddhhmmss"
        strRet = "WHERE TIFD BETWEEN '" & _
                 Format(olFrom, strFormat) & "' AND '" & _
                 Format(olTo, strFormat) & "' AND ADID = 'D' ORDER BY TIFD,FLNO"
    Else
        strRet = "ERROR in HASConfigTool.frmData.GenerateAftWhere()!"
    End If

    lblAftWhere.Caption = strRet
    GenerateAftWhere = strRet
End Function

Private Sub TimerReloadAft_Timer()
    ilCntMinutes = ilCntMinutes + 1
    If ilCntMinutes = ilReloadMinutes Then
        Me.MousePointer = vbHourglass
        TimerReloadAft.Enabled = False
        frmMain.StatusBar1.Panels(1).Text = "Reloading flight data (AFTTAB) ..."
        Dim strAftWhere As String
        strAftWhere = GenerateAftWhere()
        tab_AFTTAB.ResetContent
        tab_SEVTAB.ResetContent
        LoadData tab_AFTTAB, strAftWhere
        LoadData tab_SEVTAB, "WHERE UAFT IN (" & tab_AFTTAB.SelectDistinct("0", "", "", ",", False) & ") AND STAT IN ('ACTIVE','CANCELLED')"
        If gbDebug = True Then
            tab_AFTTAB.AutoSizeColumns
        End If
        'frmMain.RefreshManualInputTab
        ilCntMinutes = 0
        TimerReloadAft.Enabled = True
        Me.MousePointer = vbDefault
        frmMain.StatusBar1.Panels(1).Text = "Ready."
    End If
End Sub

Private Sub ReadConfigFile()
    Dim strLineNo As String

    tab_CONFIG.ResetContent
    tab_CONFIG.HeaderLengthString = "100,1000"
    tab_CONFIG.EnableInlineEdit True
    tab_CONFIG.ShowHorzScroller True
    tab_CONFIG.SetFieldSeparator strConfigSepa
    tab_CONFIG.HeaderString = "Parameter" & strConfigSepa & "Setting"
    tab_CONFIG.ReadFromFile strConfigFile

    Dim i As Integer
    Dim strLine As String
    For i = 0 To tab_CONFIG.GetLineCount - 1 Step 1
        strLine = tab_CONFIG.GetLineValues(i)
        strLine = Replace(strLine, "=", Chr(15), 1, 1)
        tab_CONFIG.UpdateTextLine i, strLine, False
    Next i
    tab_CONFIG.RedrawTab

    ' Reading TEMPLATE_PATH
    strLineNo = tab_CONFIG.GetLinesByColumnValue(0, "TEMPLATE_PATH", 0)
    If IsNumeric(strLineNo) = True Then
        strTemplatePath = tab_CONFIG.GetColumnValue(CLng(strLineNo), 1)
        If Right(strTemplatePath, 1) <> "\" Then
            strTemplatePath = strTemplatePath & "\"
        End If
    Else
        strTemplatePath = "d:\ufis\system\templates\" 'default
    End If

    ' Reading RELOAD_INTERVAL
    strLineNo = tab_CONFIG.GetLinesByColumnValue(0, "RELOAD_INTERVAL", 0)
    If IsNumeric(strLineNo) = True Then
        ilReloadMinutes = CInt(tab_CONFIG.GetColumnValue(CLng(strLineNo), 1))
    Else
        ilReloadMinutes = 60 'default
    End If
End Sub

Public Sub GenerateView_AFTTAB(ByRef rTab As TABLib.TAB)
    'View Fields: FLNO,STD,DEST
    '{=TABLE=}AFTTAB{=FIELDS=}URNO,FLNO,STOD,DES3,TIFD

    Dim ilCnt As Integer
    Dim ilCntActive As Integer
    Dim ilCntCancelled As Integer
    Dim i As Integer
    Dim j As Integer

    Dim strLine As String
    Dim strLineNo As String
    Dim strSep As String
    Dim strURNO As String
    Dim strSTAT As String

    rTab.ResetContent
    strSep = rTab.GetFieldSeparator
    ilCnt = tab_AFTTAB.GetLineCount - 1
    

    For i = 0 To ilCnt Step 1
        ilCntActive = 0
        ilCntCancelled = 0
        strLine = ""

        ' getting 'normal' values
        For j = 1 To 3 Step 1
            strLine = strLine & tab_AFTTAB.GetColumnValue(CLng(i), CLng(j)) & strSep
        Next j

        'looking if active or not
        strURNO = tab_AFTTAB.GetColumnValue(CLng(i), 0)
        strLine = strLine & GetSevtabStat(strURNO, ilCntActive, ilCntCancelled)

        If ilCntActive = 0 And ilCntCancelled = 0 Then
            If Len(strLine) > 2 Then
                strLine = Left(strLine, Len(strLine) - 1)
            End If
        End If

        rTab.InsertTextLine strLine, False
        rTab.SetLineTag rTab.GetLineCount - 1, "{=URNO=}" & strURNO
        If ilCntActive > 0 And ilCntCancelled > 0 Then
            rTab.SetLineColor rTab.GetLineCount - 1, vbBack, vbGrayed
        End If
    Next i
    rTab.Sort 1, True, True
End Sub

Public Sub GenerateView_COMMANDS(ByRef rTab As TABLib.TAB, ByRef strALC2 As String)
    'View Fields: Command
    'config-tab:Parameter=Setting

    Dim ilCnt As Integer
    Dim i As Integer
    Dim j As Integer

    Dim strLine As String
    Dim strSep As String

    rTab.ResetContent
    strSep = rTab.GetFieldSeparator
    ilCnt = tab_CONFIG.GetLineCount - 1

    For i = 0 To ilCnt Step 1
        If tab_CONFIG.GetColumnValue(CLng(i), 0) = "TEMPLATE" Then
            strLine = tab_CONFIG.GetColumnValue(CLng(i), 1)
            If GetItem(strLine, 1, ";") = strALC2 Then
                rTab.InsertTextLine GetItem(strLine, 2, ";"), False
                rTab.SetLineTag rTab.GetLineCount - 1, "{=FULL_TEMPLATE_PATH=}" & strTemplatePath & GetItem(strLine, 3, ";")
            End If
        End If
    Next i
End Sub

Public Sub LoadTemplateFile(ByRef rTextBox As TextBox, ByRef strFileName As String)
    If ExistFile(strFileName) = True Then
        Dim ilFileNo As Integer
        Dim strTextLine As String
        Dim strFile As String
        Dim strScreen As String

        ilFileNo = FreeFile ' Get unused file number.
        Open strFileName For Input As #ilFileNo
        Do While Not EOF(ilFileNo)
           Line Input #ilFileNo, strTextLine
           strFile = strFile & strTextLine & vbCrLf
        Loop
        Close ilFileNo
        GetKeyItem strScreen, strFile, "<=SCREENCONTENT=>", "<="
        If Len(strScreen) > 0 Then
            While Left(strScreen, 2) = vbCrLf
                strScreen = Right(strScreen, Len(strScreen) - 2)
            Wend
            While Right(strScreen, 2) = vbCrLf
                strScreen = Left(strScreen, Len(strScreen) - 2)
            Wend
        Else
            strScreen = strFile
        End If

        rTextBox.Text = strScreen
    Else
        MsgBox "The file " & strFileName & " does not exist!" & vbCrLf & _
               "Please check the configuration-file '" & strConfigFile & _
               "'.", vbCritical, "No template file"
    End If
End Sub

Public Sub SendToServer(ByRef rScreenText As String, ByRef rCommand As String, ByRef rFlightUrno As String)
    Dim strCmd As String
    Dim strData As String
    Dim strSelection As String
    Dim strScreenContent As String

    If Len(rScreenText) = 0 Then
        strCmd = "EXRQ"
        strSelection = rFlightUrno
        strData = rCommand
    Else
        strCmd = "FDI"
        strSelection = "TELEX,7"
        strScreenContent = Replace(rScreenText, vbCrLf, vbCr)

        strData = "<=SOURCE=>KRISCOM<=/SOURCE=>" & vbCr & "<=COMMAND=>"
        strData = strData & rCommand & "<=/COMMAND=>" & vbCr & "<=URNO=>"
        strData = strData & rFlightUrno & "<=/URNO=>" & vbCr & "<=SCREENCONTENT=>" & vbCr
        strData = strData & strScreenContent & vbCr & "<=/SCREENCONTENT=>"
    End If

    ConnectUfisCom
    frmMain.UfisCom1.CallServer strCmd, "", "", strData, strSelection, "1"
    DisconnetUfisCom
End Sub

Private Function DecryptString(ByRef rString As String)
    Dim i As Integer
    Dim ilLen As Integer
    Dim strRet As String
    Dim ilNewCharCode As Integer

    ilLen = Len(rString)

    For i = 1 To ilLen Step 1
        ilNewCharCode = Asc(Mid(rString, i, 1)) - ilOffset
        If ilNewCharCode < 0 Then
            ilNewCharCode = ilNewCharCode + 255
        End If
        strRet = strRet & Chr(ilNewCharCode)
    Next i
    DecryptString = strRet
End Function

Private Function EncryptString(ByRef rString As String)
    Dim i As Integer
    Dim ilLen As Integer
    Dim strRet As String
    Dim ilNewCharCode As Integer

    ilLen = Len(rString)

    For i = 1 To ilLen Step 1
        ilNewCharCode = Asc(Mid(rString, i, 1)) + ilOffset
        If ilNewCharCode > 255 Then
            ilNewCharCode = ilNewCharCode - 255
        End If
        strRet = strRet & Chr(ilNewCharCode)
    Next i
    EncryptString = strRet
End Function

Public Function GetSevtabStat(ByRef sAftUrno As String, ByRef iCntActive As Integer, ByRef iCntCancelled As Integer)
    Dim i As Integer
    Dim strLineNo As String
    Dim strSTAT As String

    iCntActive = 0
    iCntCancelled = 0
    strLineNo = tab_SEVTAB.GetLinesByColumnValue(0, sAftUrno, 0)

    If Len(strLineNo) > 0 Then
        For i = 1 To ItemCount(strLineNo, ",") Step 1
            strSTAT = tab_SEVTAB.GetColumnValue(CLng(GetItem(strLineNo, i, ",")), 1)
            If strSTAT = "ACTIVE" Then
                iCntActive = iCntActive + 1
            ElseIf strSTAT = "CANCELLED" Then
                iCntCancelled = iCntCancelled + 1
            End If
        Next i
    End If
    If iCntActive > 0 Then
        GetSevtabStat = "1"
    ElseIf iCntCancelled > 0 Then
        GetSevtabStat = "0"
    Else
        GetSevtabStat = ""
    End If
End Function

Public Sub UpdateSevtab(ByRef sAftUrno As String, ByRef sStatus As String)
    Dim i As Integer
    Dim strLineNo As String
    Dim strWhere As String

    ' update database
    ConnectUfisCom
    strWhere = "UPDATE SEVTAB SET STAT = '" & sStatus & "' WHERE UAFT = " & sAftUrno & " AND STAT IN ('CANCELLED','ACTIVE')"
    If frmMain.UfisCom1.CallServer("SQL", "SEVTAB", "", "", strWhere, "360") <> 0 Then
        MsgBox "Update database failed!" & vbCrLf & "Where-statement: " & strWhere & vbCrLf & _
            "Message: " & frmMain.UfisCom1.LastErrorMessage, vbCritical, "Update failed"
        DisconnetUfisCom
        Exit Sub
    End If
    DisconnetUfisCom

    ' update internal
    strLineNo = tab_SEVTAB.GetLinesByColumnValue(0, sAftUrno, 0)
    If Len(strLineNo) > 0 Then
        For i = 1 To ItemCount(strLineNo, ",") Step 1
            tab_SEVTAB.SetColumnValue CLng(GetItem(strLineNo, i, ",")), 1, sStatus
        Next i
    End If

End Sub
