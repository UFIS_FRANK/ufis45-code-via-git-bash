// PSGeometrie.cpp : implementation file
//

#include <stdafx.h>
#include <CCSEdit.h>
#include <CCSGlobl.h>
#include <PSGeometrie.h>
#include <UniEingabe.h>
//#include "CedaPfcData.h"
//#include "CedaGegData.h"
#include <CCSTime.h>
#include <StringConst.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PSGeometrie property page

IMPLEMENT_DYNCREATE(PSGeometrie, CPropertyPage)


PSGeometrie::PSGeometrie() : CPropertyPage(PSGeometrie::IDD)
{
	//{{AFX_DATA_INIT(PSGeometrie)
	//}}AFX_DATA_INIT
    bmNotMoreThan12 = FALSE;
	pomCurrentGroup = NULL;
	blIsInit = false;
	bmMaxDefault = true;
	bmLinesDefault = true;
	bmTimeDefault = true;
	bmVertDefault = true;
	bmDisplayBeginDefault = true;
}

PSGeometrie::~PSGeometrie()
{
	for(int i = omGroupData.GetSize()-1; i >= 0; i-- )
	{
		omGroupData[i].Codes.DeleteAll();
	}
	omGroupData.DeleteAll();  
}
void PSGeometrie::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}
void PSGeometrie::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PSGeometrie)
	DDX_Control(pDX, IDC_TEST, m_TestText);
	DDX_Control(pDX, IDC_FONT, m_Font);
	DDX_Control(pDX, IDC_STARTDATE, m_Date);
	DDX_Control(pDX, IDC_RB_MIN, m_Min);
	DDX_Control(pDX, IDC_RB_MAX, m_Max);
	DDX_Control(pDX, IDC_PFC, m_Pfc);
	DDX_Control(pDX, IDC_GEG, m_Geg);
	DDX_Control(pDX, IDC_NEW, m_New);
	DDX_Control(pDX, IDC_LIST1, m_ResList);
	DDX_Control(pDX, IDC_HEIGHT, m_Height);
	DDX_Control(pDX, IDC_FLT_TIMELINES, m_TimeLines);
	DDX_Control(pDX, IDC_DELETE, m_Delete);
	DDX_Control(pDX, IDC_24H, m_24h);
	DDX_Control(pDX, IDC_12H, m_12h);
	DDX_Control(pDX, IDC_10H, m_10h);
	DDX_Control(pDX, IDC_8H, m_8h);
	DDX_Control(pDX, IDC_6H, m_6h);
	DDX_Control(pDX, IDC_4H, m_4h);
	DDX_Control(pDX, IDC_100PERCENT, m_100Proz);
	DDX_Control(pDX, IDC_75PERCENT, m_75Proz);
	DDX_Control(pDX, IDC_50PERCENT, m_50Proz);
	DDX_Control(pDX, IDC_GRUPPE, m_GRUPPE);
	DDX_Control(pDX, IDC_STARTTIME, m_Time);
	//}}AFX_DATA_MAP
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}

}


BEGIN_MESSAGE_MAP(PSGeometrie, CPropertyPage)
	//{{AFX_MSG_MAP(PSGeometrie)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_BN_CLICKED(IDC_GEG, OnGeg)
	ON_BN_CLICKED(IDC_PFC, OnPfc)
	ON_BN_CLICKED(IDC_10H, On10h)
	ON_BN_CLICKED(IDC_12H, On12h)
	ON_BN_CLICKED(IDC_24H, On24h)
	ON_BN_CLICKED(IDC_4H, On4h)
	ON_BN_CLICKED(IDC_6H, On6h)
	ON_BN_CLICKED(IDC_8H, On8h)
	ON_LBN_SELCHANGE(IDC_LIST1, OnSelchangeList1)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
	ON_MESSAGE(WM_DRAGOVER, OnDragOver)
	ON_MESSAGE(WM_DROP, OnDrop)
	ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PSGeometrie message handlers

BOOL PSGeometrie::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	m_DragDropTarget.RegisterTarget(this, this);

	CRect rect;
	m_GRUPPE.GetWindowRect(&rect);
    rect.InflateRect(1, 1);     // hiding the CTable window border
	ScreenToClient(rect);

    if (bmNotMoreThan12)
         GetDlgItem(IDC_24H) -> EnableWindow(FALSE);

    
    m_Height.SetRange(0, 120);
	m_Height.SetTicFreq(10);
    m_Height.SetPos(m_Hour * ONE_HOUR_POINT);

	m_Font.SetRange(0,19);
	m_Font.SetTicFreq(1);
    m_Font.SetPos(m_FontHeight);
	m_Pfc.SetCheck(1);
	m_Geg.SetCheck(0);

/*	int ilCount = ogPfcData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		omPfcData.Add(&ogPfcData.omData[i]);
	}
	ilCount = ogGegData.omData.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		omGegData.Add(&ogGegData.omData[i]);
	}
	omGegData.Sort(CompareGeg);
	omPfcData.Sort(ComparePfc);
*/
	m_100Proz.ShowWindow(SW_HIDE);
	m_75Proz.ShowWindow(SW_HIDE);
	m_50Proz.ShowWindow(SW_HIDE);


/*	for(int i = 0; i < omPfcData.GetSize(); i++)
	{
		m_ResList.AddString(CString(omPfcData[i].Fctc));
	}
*/
	ShowTable();
	if(bmMaxDefault == true)
	{
		m_Max.SetCheck(1);
		m_Min.SetCheck(0);
	}
	if(bmVertDefault == true)
	{
		m_FontHeight = 8;
		m_Font.SetPos(m_FontHeight);
		m_TestText.SetFont(&ogScalingFonts[m_FontHeight]);
	}
	if(bmTimeDefault == true)
	{
		m_10h.SetCheck(1);
		m_Height.SetPos(10 * ONE_HOUR_POINT);
	}
	if(bmDisplayBeginDefault == true)
	{
		CTime olTime = CTime::GetCurrentTime();
		//m_Date.SetWindowText(olTime.Format("%d.%m.%Y"));
		//m_Time.SetWindowText(olTime.Format("%H:%M"));
	}

	if(omCalledFrom == "COVERAGEDIA")
	{
		m_Pfc.ShowWindow(SW_HIDE);
		m_Geg.ShowWindow(SW_HIDE);
		m_ResList.ShowWindow(SW_HIDE);
		m_New.ShowWindow(SW_HIDE);
		m_Delete.ShowWindow(SW_HIDE);
		m_GRUPPE.ShowWindow(SW_HIDE);
	}
	blIsInit = true;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
void PSGeometrie::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default

    //CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
    //UpdateData();

    m_Hour = m_Height.GetPos() / ONE_HOUR_POINT;
    //m_24h = -1;
    if (m_Hour == 4)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(0);
        m_10h.SetCheck(0);
        m_8h.SetCheck(0);
        m_6h.SetCheck(0);
        m_4h.SetCheck(1);
	}
    else if (m_Hour == 6)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(0);
        m_10h.SetCheck(0);
        m_8h.SetCheck(0);
        m_6h.SetCheck(1);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 8)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(0);
        m_10h.SetCheck(0);
        m_8h.SetCheck(1);
        m_6h.SetCheck(0);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 10)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(0);
        m_10h.SetCheck(1);
        m_8h.SetCheck(0);
        m_6h.SetCheck(0);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 12)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(1);
        m_10h.SetCheck(0);
        m_8h.SetCheck(0);
        m_6h.SetCheck(0);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 24)
	{
        m_24h.SetCheck(1);
        m_12h.SetCheck(0);
        m_10h.SetCheck(0);
        m_8h.SetCheck(0);
        m_6h.SetCheck(0);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 0)
    {
        m_Hour = 2;
    }

    
    if (bmNotMoreThan12 && (m_Hour > 12))
        m_Hour = 12;
    m_Height.SetPos(m_Hour * ONE_HOUR_POINT);


	int ilFontPos = m_Font.GetPos();
	m_TestText.SetFont(&ogScalingFonts[ilFontPos]);
	m_TestText.UpdateWindow();
    //UpdateData(FALSE);


/*    m_TimeScale = CTime(m_startDate.GetYear(), m_startDate.GetMonth(), m_startDate.GetDay(),
                atoi(m_startTime), 0, 0);

    if (pomTS != NULL)
    {
        pomTS->SetDisplayTimeFrame(m_TimeScale, CTimeSpan(0, m_Hour, 0, 0), omTSI);
        pomTS->Invalidate(TRUE);
    }

    if (pomTS1 != NULL)
    {
        pomTS1->SetDisplayTimeFrame(m_TimeScale, CTimeSpan(0, m_Hour, 0, 0), omTSI);
        pomTS1->Invalidate(TRUE);
    }
*/
}

void PSGeometrie::OnNew() 
{

}

void PSGeometrie::OnGeg() 
{
/*	int ilCount = omGegData.GetSize();
	m_ResList.ResetContent();
	for(int i = 0; i < ilCount; i++)
	{
		m_ResList.AddString(omGegData[i].Gcde);
	}
	if(pomCurrentGroup != NULL)
	{
		pomCurrentGroup->Typ = CString("G");
	}
*/
}

void PSGeometrie::OnPfc() 
{
/*	int ilCount = omPfcData.GetSize();
	m_ResList.ResetContent();
	for(int i = 0; i < ilCount; i++)
	{
		m_ResList.AddString(omPfcData[i].Fctc);
	}
	if(pomCurrentGroup != NULL)
	{
		pomCurrentGroup->Typ = CString("F");
	}
*/
}

void PSGeometrie::On10h() 
{
    m_Height.SetPos(10 * ONE_HOUR_POINT);
}

void PSGeometrie::On12h() 
{
    m_Height.SetPos(12 * ONE_HOUR_POINT);
}

void PSGeometrie::On24h() 
{
    m_Height.SetPos(24 * ONE_HOUR_POINT);
}

void PSGeometrie::On4h() 
{
    m_Height.SetPos(4 * ONE_HOUR_POINT);
}

void PSGeometrie::On6h() 
{
    m_Height.SetPos(6 * ONE_HOUR_POINT);
}

void PSGeometrie::On8h() 
{
    m_Height.SetPos(8 * ONE_HOUR_POINT);
}

void PSGeometrie::OnSelchangeList1() 
{
	if(pomCurrentGroup != NULL)
	{
		pomCurrentGroup->Codes.DeleteAll();
		int *pilItems = new int[m_ResList.GetCount()];
		if(m_Pfc.GetCheck() == 1)
		{
			pomCurrentGroup->Typ = "F";
		}
		if(m_Geg.GetCheck() == 1)
		{
			pomCurrentGroup->Typ = "G";
		}
		int ilC = m_ResList.GetSelItems( m_ResList.GetCount(),  pilItems);
		for(int k = 0; k < ilC; k++)
		{
			CString olCode;
			m_ResList.GetText( pilItems[k], olCode);
			pomCurrentGroup->Codes.NewAt(pomCurrentGroup->Codes.GetSize(), olCode);
		}
		delete pilItems;
	}
}

LONG PSGeometrie::OnTableDragBegin(UINT wParam, LONG lParam)
{
//	CCSTABLENOTIFY *polNotify;
//	polNotify = (CCSTABLENOTIFY*)lParam;
//	TABLE_COLUMN *prlColumn = (TABLE_COLUMN*)polNotify->Data;
//	CString olText = prlColumn->Text;
	m_DragDropSource.CreateDWordData(DIT_ANSICHT_GRP, 1);
	m_DragDropSource.AddDWord(wParam);
	m_DragDropSource.BeginDrag();

	return 0L;
}
LONG PSGeometrie::OnDragOver(UINT, LONG)
{
	return -1;
}

LONG PSGeometrie::OnTableLButtonDown(UINT wParam, LONG lParam)
{
	int ilItem = wParam;
	if(ilItem != -1)
	{
		if(ilItem < omGroupData.GetSize())
		{
			pomCurrentGroup = &omGroupData[ilItem];
			ShowResList();
		}
	}
	return 0L;
}

LONG PSGeometrie::OnDrop(UINT wParam, LONG lParam)
{
	return 0L; 
}

void PSGeometrie::ShowTable()
{

}

void PSGeometrie::ShowResList()
{
	if(pomCurrentGroup != NULL)
	{
		if(strcmp(pomCurrentGroup->Typ, "G") == 0)
		{
			m_Pfc.SetCheck(0);
			m_Geg.SetCheck(1);
			OnGeg();
		}
		else
		{
			m_Pfc.SetCheck(1);
			m_Geg.SetCheck(0);
			OnPfc();
		}
		for(int i = 0; i < pomCurrentGroup->Codes.GetSize(); i++)
		{
			int ilIdx = m_ResList.FindString(-1, pomCurrentGroup->Codes[i]);
			if(ilIdx != -1)
			{
				m_ResList.SetSel(ilIdx);
			}
		}
	}
	else
	{
		OnGeg();
	}
}

void PSGeometrie::OnDelete() 
{
}

void PSGeometrie::SetData()
{
	omGroupData.DeleteAll();
	CStringArray olGroups;
	CString olTotalString;
	for(int i = 0; i < omValues.GetSize(); i++)
	{
		olTotalString += omValues[i];
	}
	//Ma�stab
	int ilMassFrom = olTotalString.Find('<');
	int ilMassTo = olTotalString.Find('>');
	if(ilMassFrom != -1 && ilMassTo != -1)
	{
		CString olMassString = olTotalString.Mid(ilMassFrom+1, ilMassTo-1);
		if(!olMassString.IsEmpty())
		{
			int ilMinPos = olMassString.Find('M');
			int ilLinePos = olMassString.Find('F');
			int ilTimePos = olMassString.Find('Z');
			int ilVertPos = olMassString.Find('V');
			int ilShowTimePos = olMassString.Find('T');
			if(ilMinPos != -1)
			{
				CString T = olMassString.Mid(ilMinPos+2, 1);
				if(!T.IsEmpty())
				{
					if(T == "A")
					{
						m_Max.SetCheck(1);
						m_Min.SetCheck(0);
					}
					else
					{
						m_Max.SetCheck(0);
						m_Min.SetCheck(1);
					}
					bmMaxDefault = false;
				}
			}
			if(ilLinePos != -1)
			{
				CString T = olMassString.Mid(ilLinePos+2, 1);
				if(!T.IsEmpty())
				{
					if(T == "J")
					{
						m_TimeLines.SetCheck(1);
					}
					else
					{
						m_TimeLines.SetCheck(0);
					}
					bmLinesDefault = false;
				}
			}
			if(ilTimePos != -1)
			{
				CString T = olMassString.Mid(ilTimePos+2, 2);
				if(!T.IsEmpty())
				{
					m_Hour = atoi(T);
				    m_Height.SetPos(m_Hour * ONE_HOUR_POINT);
					if (m_Hour == 4)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(1);
					}
					else if (m_Hour == 6)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(1);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 8)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(1);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 10)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(1);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 12)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(1);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 24)
					{
						m_24h.SetCheck(1);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 0)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
						m_Hour = 2;
					}
					else
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					bmTimeDefault = false;
				}
			}
			if(ilVertPos != -1)
			{
				CString T = olMassString.Mid(ilVertPos+2, 2);
				if(!T.IsEmpty())
				{
					int M = atoi(T);
					m_Font.SetPos(M);
					m_FontHeight = M;
					m_TestText.SetFont(&ogScalingFonts[m_FontHeight]);
/*					if(M == 0)
					{
						m_100Proz.SetCheck(1);
						m_75Proz.SetCheck(0);
						m_50Proz.SetCheck(0);
					}
					else if(M == 1)
					{
						m_100Proz.SetCheck(0);
						m_75Proz.SetCheck(1);
						m_50Proz.SetCheck(0);
					}
					else if(M == 2)
					{
						m_100Proz.SetCheck(0);
						m_75Proz.SetCheck(0);
						m_50Proz.SetCheck(1);
					}
*/
					bmVertDefault = false;
				}
			}
			if(ilShowTimePos != -1)
			{
				CString T = olMassString.Mid(ilShowTimePos+2, 14);
				if(!T.IsEmpty())
				{
					CTime olDate;
					olDate = DBStringToDateTime(T);
					if(olDate != TIMENULL)
					{
						CString olDateStr, olTimeStr;
						olDateStr = olDate.Format("%d.%m.%Y");
						olTimeStr = olDate.Format("%H:%M");
						m_Date.SetWindowText(olDateStr);
						m_Time.SetWindowText(olTimeStr);
						bmDisplayBeginDefault = false;
					}
					else
					{
						CString olDateStr, olTimeStr;
						olDate = CTime::GetCurrentTime();
						olDateStr = olDate.Format("%d.%m.%Y");
						olTimeStr = olDate.Format("%H:%M");
						//RST! m_Date.SetWindowText(olDateStr);
						//RST! m_Time.SetWindowText(olTimeStr);
					}
				}
				else
				{
					CString olDateStr, olTimeStr;
					CTime olDate = CTime::GetCurrentTime();
					olDateStr = olDate.Format("%d.%m.%Y");
					olTimeStr = olDate.Format("%H:%M");
					//RST! m_Date.SetWindowText(olDateStr);
					//RST! m_Time.SetWindowText(olTimeStr);
				}
			}
			else
			{
				CString olDateStr, olTimeStr;
				CTime olDate = CTime::GetCurrentTime();
				olDateStr = olDate.Format("%d.%m.%Y");
				olTimeStr = olDate.Format("%H:%M");
				//RST! m_Date.SetWindowText(olDateStr);
				//RST! m_Time.SetWindowText(olTimeStr);
			}
		}
	}
	//Rest
	CString olSubString = olTotalString;
	if(!olSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		bool blEnd = false;
		CString olText;
		while(blEnd == false)
		{
			pos = olSubString.Find(';');
			if(pos == -1)
			{
				blEnd = true;
				olText = olSubString;
			}
			else
			{
				olText = olSubString.Mid(0, olSubString.Find(';'));
				olSubString = olSubString.Mid(olSubString.Find(';')+1, olSubString.GetLength( )-olSubString.Find(';')+1);
			}
			olGroups.Add(olText);
		}
	}
	for(i = 0; i < olGroups.GetSize(); i++)
	{
		PS_GEO_GROUP_DATA rlGrp;// = new PS_GEO_GROUP_DATA;
		CString olRest;
		CString olName;
		CString olPart;
		int ilPos1 = olGroups[i].Find('=');
		if(ilPos1 != -1)
		{
			olPart = olGroups[i].Mid(0, olGroups[i].Find('='));
			olRest = olGroups[i].Mid(olGroups[i].Find('=')+1, olGroups[i].GetLength());
			ilPos1 = olPart.Find('|');

			rlGrp.Name = olPart.Mid(0, ilPos1);
			olPart = olPart.Mid(olPart.Find('|')+1, olPart.GetLength());
			rlGrp.Typ = olPart;
		}
		if(!rlGrp.Name.IsEmpty())
		{
			olSubString = olRest;
			if(!olSubString.IsEmpty())
			{
				int pos;
				int olPos = 0;
				bool blEnd = false;
				CString olText;
				while(blEnd == false)
				{
					pos = olSubString.Find('|');
					if(pos == -1)
					{
						blEnd = true;
						olText = olSubString;
					}
					else
					{
						olText = olSubString.Mid(0, olSubString.Find('|'));
						olSubString = olSubString.Mid(olSubString.Find('|')+1, olSubString.GetLength( )-olSubString.Find('|')+1);
					}
					rlGrp.Codes.NewAt(rlGrp.Codes.GetSize(), olText);
				}
			}
			omGroupData.NewAt(omGroupData.GetSize(), rlGrp);
		}
	}
	if(blIsInit = true)
	{
		ShowTable();
	}
}

void PSGeometrie::GetData()
{
	omValues.RemoveAll();
	//Erst mal die Ma�stab-Elemente abfragen
	CString olMass;
	if(m_Min.GetCheck() == 1)
	{
		olMass += "M=I|";
	}
	else
	{
		olMass += "M=A|";
	}
	if(m_TimeLines.GetCheck() == 1)
	{
		olMass += "F=J|";
	}
	else
	{
		olMass += "F=N|";
	}
	m_Hour = m_Height.GetPos() / ONE_HOUR_POINT;
	char pclHour[100]="";
	char pclFont[5]="";
	m_FontHeight = m_Font.GetPos();
	sprintf(pclFont, "%02d", m_FontHeight);
	sprintf(pclHour, "%02ld", m_Hour);
	olMass += "Z=" + CString(pclHour) + "|";
	olMass += "V=" + CString(pclFont) + "|";
/*	if(m_100Proz.GetCheck() == 1)
	{
		olMass += "V=0|";
	}
	else if((m_75Proz.GetCheck() == 1))
	{
		olMass += "V=1|";
	}
	else if((m_50Proz.GetCheck() == 1))
	{
		olMass += "V=2|";
	}
*/
	CString olStrDate, olStrTime;
	m_Date.GetWindowText(olStrDate);
	m_Time.GetWindowText(olStrTime);
	CTime olDate, olTime;
	olDate = DateStringToDate(olStrDate);
	if(olDate != TIMENULL)
	{
		olTime = HourStringToDate(olStrTime, olDate);
		olMass += "T=" + olTime.Format("%Y%m%d%H%M00");
	}
	olMass = "<" + olMass + ">;";
	omValues.Add(olMass);
	for(int i = 0; i < omGroupData.GetSize(); i++)
	{
		if(omGroupData[i].Name != CString(""))
		{
			CString olValue;
			olValue += omGroupData[i].Name + "|" + omGroupData[i].Typ + "=";
			for(int j = 0; j < omGroupData[i].Codes.GetSize(); j++)
			{
				olValue += omGroupData[i].Codes[j] + "|";
			}
			olValue += ";";
			omValues.Add(olValue);
		}
	}
}

