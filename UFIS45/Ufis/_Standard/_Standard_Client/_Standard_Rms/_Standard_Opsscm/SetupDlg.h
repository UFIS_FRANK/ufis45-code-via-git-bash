#ifndef AFX_SETUPDLG_H__EC726D71_1630_11D2_8D95_0000C002916B__INCLUDED_
#define AFX_SETUPDLG_H__EC726D71_1630_11D2_8D95_0000C002916B__INCLUDED_

// SetupDlg.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld SetupDlg 
#include <Resource.h>
#include <CCSGlobl.h>
#include <FPMS.h>
#include <CedaCfgData.h>
#include <CCSEdit.h>
#include <stdafx.h>



class SetupDlg : public CDialog
{
// Konstruktion
public:
	SetupDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~SetupDlg();
// Dialogfelddaten
	//{{AFX_DATA(SetupDlg)
	enum { IDD = IDD_MONITOR_SETUP };
	CStatic	m_CS_Pops_Report_Daily_Alc;
	CCSEdit	m_CE_PopsReportDailyAlc;
	CStatic	m_CS_Font;
	CComboBox	m_UserCB;
	int		m_Monitors;
	int		m_Batch1;
	int		m_Daily1;
	int		m_DailyRot1;
	int		m_FlightDia1;
	int		m_Season1;
	int		m_SeasonRot1;
	int		m_CcaDia1;
	CString	m_PopsReportDailyAlc;
	//}}AFX_DATA


	 CObList omMonitorButtons1;
	 CObList omMonitorButtons2;
	 CObList omMonitorButtons3;
	//CCSPtrArray<CButton> omMonitorButtons2;
	//CCSPtrArray<CButton> omMonitorButtons3;
// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(SetupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(SetupDlg)
	afx_msg void OnSelchangePknoCb();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnRadio3();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnFont();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SETUPDLG_H__EC726D71_1630_11D2_8D95_0000C002916B__INCLUDED_
