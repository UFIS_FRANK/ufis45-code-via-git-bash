// seasontableviewer.cpp : implementation file
// 
// Modification History: 


#include <stdafx.h>
#include <CicDemandTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <DataSet.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif




// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer
//

CicDemandTableViewer::CicDemandTableViewer()
{
    pomTable = NULL;
	bmInit = false;
}


void CicDemandTableViewer::UnRegister()
{
	ogDdx.UnRegister(this, NOTUSED);
 
}

CicDemandTableViewer::~CicDemandTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void CicDemandTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

void CicDemandTableViewer::ClearAll()
{
	bmInit = false;
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
    pomTable->ResetContent();
}



void CicDemandTableViewer::ChangeViewTo(const char *pcpViewName)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	ogDdx.UnRegister(this, NOTUSED);

	ogDdx.Register(this, D_FLIGHT_CHANGE, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	ogDdx.Register(this, D_FLIGHT_DELETE, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	ogDdx.Register(this, D_FLIGHT_INSERT, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);

    pomTable->ResetContent();
    DeleteAll();    
    MakeLines(&ogCcaDiaFlightData.omData);
	UpdateDisplay();

	bmInit = true;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}





bool CicDemandTableViewer::IsPassFilter(DIAFLIGHTDATA *prpFlight)
{
	if(prpFlight == NULL)
		return false;
	
	bool blRet = true;

	if(CString(prpFlight->Adid) == "A")
	{
		return false;
	}


    return (blRet);
}



/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer -- code specific to this class

void CicDemandTableViewer::MakeLines(CCSPtrArray<DIAFLIGHTDATA> *popFlights, bool bpInsert)
{
	DIAFLIGHTDATA *prlFlight;

	int ilFlightCount = popFlights->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*popFlights)[ilLc];

		if(CString(prlFlight->Adid) == "D")
		{
			MakeLine(prlFlight, bpInsert);
		}
	}

}

		




bool CicDemandTableViewer::MakeLine(DIAFLIGHTDATA *prpFlight, bool bpInsert)
{
	CCSPtrArray<RecordSet> *polGHPs;
	CCSPtrArray<RecordSet> *polGPMs;
	RecordSet *prlGHP;
	RecordSet *prlGPM;
	int ilLineNo;	

    CICDEMANDTABLE_LINEDATA rlLine;
	
	if(!IsPassFilter(prpFlight))
		return false;

	int ilUrnoIndex = ogBCD.GetFieldIndex("GHP", "URNO");
	int ilDtimIndex = ogBCD.GetFieldIndex("GPM", "DTIM");
	int ilPotaIndex = ogBCD.GetFieldIndex("GPM", "POTA");



	polGHPs = new CCSPtrArray<RecordSet>;
	polGPMs = new CCSPtrArray<RecordSet>;

	ogDataSet.GetGhpDataForFlight(polGHPs, NULL, prpFlight);



	int ilCount = polGHPs->GetSize();
	if(ilCount == 0)
	{
		strcpy(prpFlight->Remark, GetString(IDS_STRING1448)); // Keine regel gefunden
	}
	if(ilCount > 1)
	{
		sprintf(prpFlight->Remark,	GetString(IDS_STRING1449),	ogDataSet.GetRulesName(polGHPs, "GHP"));
	}
	if(ilCount == 1)
	{
		strcpy(prpFlight->Remark, ogDataSet.GetRulesName(polGHPs, "GHP"));
	}


	if(ilCount != 1)
	{
		MakeLineData(prpFlight, rlLine);
		ilLineNo =  CreateLine(rlLine);
		if(bpInsert)
			InsertDisplayLine(ilLineNo);
	}
	else
	{
		prlGHP = &(*polGHPs)[0];
			
		CCSPtrArray<DIACCADATA> olData;

		ogCcaDiaFlightData.omCcaData.GetCcaArray(prpFlight->Urno, olData);



		int ilCount2 = ogDataSet.GetGPMs(prlGHP->Values[ilUrnoIndex], polGPMs);


		CString olTmp;

		olTmp.Format("  %d/%d", olData.GetSize(), ilCount2);

		olTmp = CString(prpFlight->Remark) + olTmp;	

		strcpy(prpFlight->Remark, olTmp);

		if(ilCount2 > olData.GetSize())
		{
			for(int i = ilCount2 - 1; i >= 0; i--)
			{
				prlGPM = &(*polGPMs)[i];


				CTimeSpan olPota(0,0, atoi(prlGPM->Values[ilPotaIndex]),0);
				CTimeSpan olDtim(0,0, atoi(prlGPM->Values[ilDtimIndex]),0);

				prpFlight->DCcaTo  = prpFlight->Stod + olPota;
				prpFlight->DCcaFrom  = prpFlight->Stod - olDtim;


				MakeLineData(prpFlight, rlLine);
				ilLineNo =  CreateLine(rlLine);
				if(bpInsert)
					InsertDisplayLine(ilLineNo);
			}
		}
	}

	polGHPs->DeleteAll();	
	delete polGHPs;

	polGPMs->DeleteAll();	
	delete polGPMs;

	return true;
}




void CicDemandTableViewer::MakeLineData(DIAFLIGHTDATA *prpFlight, CICDEMANDTABLE_LINEDATA &rpLine)
{

	rpLine.Urno =  prpFlight->Urno;
	rpLine.Act3 = CString(prpFlight->Act3); 
	rpLine.Flno = CString(prpFlight->Flno); 
	rpLine.Remark = CString(prpFlight->Remark); 

	rpLine.Sto = prpFlight->Stod; 
	rpLine.OrgDes = CString(prpFlight->Des3); 

	rpLine.Ckes = prpFlight->DCcaTo; 
	rpLine.Ckbs = prpFlight->DCcaFrom; 
	rpLine.Nose = CString(prpFlight->Nose);
	rpLine.Ftyp = CString(prpFlight->Ftyp);

	if(bgGatPosLocal) ogBasicData.UtcToLocal(rpLine.Sto);
	if(bgGatPosLocal) ogBasicData.UtcToLocal(rpLine.Ckes);
	if(bgGatPosLocal) ogBasicData.UtcToLocal(rpLine.Ckbs);

    return;
}



int CicDemandTableViewer::CreateLine(CICDEMANDTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void CicDemandTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);

	pomTable->DeleteTextLine(ipLineno);

}



bool CicDemandTableViewer::FindLine(long lpUrno, int &rilLineno)
{
	rilLineno = -1;
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if(omLines[i].Urno == lpUrno)
	  {
		rilLineno = i;
		return true;
	  }
	}
	return false;
}






/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer - CICDEMANDTABLE_LINEDATA array maintenance

void CicDemandTableViewer::DeleteAll()
{
    omLines.DeleteAll();
}


/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void CicDemandTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	CICDEMANDTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}



void CicDemandTableViewer::InsertDisplayLine( int ipLineNo)
{
	if(!((ipLineNo >= 0) && (ipLineNo < omLines.GetSize())))
		return;
	CCSPtrArray<TABLE_COLUMN> olColList;
	MakeColList(&omLines[ipLineNo], olColList);
	pomTable->InsertTextLine(ipLineNo, olColList, &omLines[ipLineNo]);
	olColList.DeleteAll();
    //pomTable->DisplayTable();
}






void CicDemandTableViewer::DrawHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &ogCourier_Bold_10;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 67; 
	rlHeader.Text =  GetString(IDS_STRING347);//CString("Flugnummer");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = GetString(IDS_STRING316);//CString("STD");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 65; 
	rlHeader.Text = GetString(IDS_STRING332);//CString("Datum");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 30; 
	rlHeader.Text = GetString(IDS_STRING1447);//CString("ORG/DES");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 30; 
	rlHeader.Text = GetString(IDS_STRING311);//CString("A/C");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = GetString(IDS_STRING1073);//CString("Von");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = GetString(HD_DSR_AVTA_TIME);//CString("bis");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 50; 
	rlHeader.Text = CString("Conf");//CString("bis");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = CString("Ftyp");//CString("bis");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 300; 
	rlHeader.Text = "";//GetString(IDS_STRING334);//CString("Remak");
	omHeaderDataArray.New(rlHeader);


	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}









void CicDemandTableViewer::MakeColList(CICDEMANDTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{
		TABLE_COLUMN rlColumnData;

		rlColumnData.VerticalSeparator = SEPA_NONE;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogCourier_Regular_9;
	
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Flno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Sto.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Sto.Format("%d.%m.%y");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->OrgDes;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Act3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Ckbs.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Ckes.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
//MWO
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Nose;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Ftyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

//end MWO
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Remark;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

}



static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CicDemandTableViewer *polViewer = (CicDemandTableViewer *)popInstance;

    if (ipDDXType == D_FLIGHT_CHANGE)
        polViewer->ProcessFlightChange((DIAFLIGHTDATA *)vpDataPointer);
	if (ipDDXType == D_FLIGHT_DELETE)
        polViewer->ProcessFlightDelete((DIAFLIGHTDATA *)vpDataPointer);
}




void CicDemandTableViewer::ProcessFlightChange(DIAFLIGHTDATA *prpFlight)
{
	if(prpFlight == NULL)
		return;

	DeleteFlight(prpFlight->Urno);

	MakeLine(prpFlight, true);

	return;
}







void CicDemandTableViewer::ProcessFlightDelete(DIAFLIGHTDATA *prpFlight)
{
	if(prpFlight == NULL)
		return;

	DeleteFlight(prpFlight->Urno);
}





int CicDemandTableViewer::CompareFlight(CICDEMANDTABLE_LINEDATA *prpLine1, CICDEMANDTABLE_LINEDATA *prpLine2)
{
		return (prpLine1->Sto == prpLine2->Sto)? 0:	(prpLine1->Sto > prpLine2->Sto)? 1: -1;
}






bool CicDemandTableViewer::DeleteFlight(long lpUrno)
{
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if(omLines[i].Urno == lpUrno)
	  {
		omLines.DeleteAt(i);
		pomTable->DeleteTextLine(i);
	  }
	}
	return false;
}


