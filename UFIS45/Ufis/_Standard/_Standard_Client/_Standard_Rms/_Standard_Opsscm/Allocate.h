#ifndef __ALLOCATE_
#define __ALLOCATE_


#include <CedaBasicData.h>
#include <CcaCedaFlightData.h>
#include <CedaDemandData.h>


typedef CCSPtrArray<DEMANDDATA> DEMANDLIST;


class Allocate
{
public:
	Allocate(int ipMode);
	~Allocate();


	void Run();

	void AllocateDemands( CCSPtrArray<DEMANDDATA> &ropDemands, CCAFLIGHTDATA *prpFlight);

	void AllocateDemands( CCSPtrArray<DEMANDLIST> &rlDemandArrays, CCSPtrArray<CCAFLIGHTDATA> &rlFlightArray);



};

#endif //__ALLOCATE_CCA__