#if !defined(AFX_CCSPSUNIFILTER_H__601EC771_25CE_11D1_987D_000001014864__INCLUDED_)
#define AFX_CCSPSUNIFILTER_H__601EC771_25CE_11D1_987D_000001014864__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ccspsunifilter.h : header file
//
#include <CCSEdit.h>
//#include "CCSPageBuffer.h"
//#include "CCSBasePSPage.h"
#include <Ansicht.h>
/////////////////////////////////////////////////////////////////////////////
// CPsUniFilter dialog

class CPsUniFilter : public CPropertyPage//CBasePropSheetPage
{
	DECLARE_DYNCREATE(CPsUniFilter)

// Construction
public:
	CPsUniFilter();
	~CPsUniFilter();


	void SetCaption(const char *pcpCaption);
	CStringArray omDescArray;
	CStringArray omFieldArray;
	CStringArray omTypeArray;
	CStringArray omOperators;
	CString omFilter;

	CStringArray omValues;
	CString omCalledFrom;

	void SetCalledFrom(CString opCalledFrom);
// Dialog Data
	//{{AFX_DATA(CPsUniFilter)
	enum { IDD = IDD_PSUNIFILTER_PAGE };
	CComboBox	m_ComboDatenfeld;
	CComboBox	m_ComboOperator;
	CComboBox	m_ComboTabelle;
	CString	m_EditBedingung;
	CCSEdit	m_CtrlBedingung;
	CCSEdit m_Where;
	CEdit E_EditFilter;
	CString	m_EditFilter;
	CButton		m_And;
	CButton		m_Or;
	//}}AFX_DATA

	void InitMask();
	void SetFieldAndDescriptionArrays();
	void SetData();
	void GetData();
	void StripSign(CString &ropString, char cpChar);
	bool ReverseDateString(CString &ropSource);

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPsUniFilter)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL
/*   void ResetBufferPtr()
   {
	   CBasePropSheetPage::ResetBufferPtr(); 
	   pomPageBuffer = NULL;
   };
*/
	CString GetPrivList(){return("");};

// Implementation
protected:
	//CPageBuffer *pomPageBuffer;
	CString omConnection;
	bool blIsInit;
	
	// Generated message map functions
	//{{AFX_MSG(CPsUniFilter)
	afx_msg void OnBUTTONNeu();
	afx_msg void OnBUTTONSetzen();
	afx_msg void OnSelchangeCOMBOTabelle();
	afx_msg void OnSelchangeCOMBODatenfeld();
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioAnd();
	afx_msg void OnRadioOr();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

/*	void InitialValues(int ipRow);
	void *GetData();
    void InitialScreen();
	int CreateFilter(CString &ropFilter, 
							  CString &ropTabelle,
							  CString &ropDatenfeld,
							  CString &ropOperator,
							  CString &ropValue);
*/
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCSPSUNIFILTER_H__601EC771_25CE_11D1_987D_000001014864__INCLUDED_)
