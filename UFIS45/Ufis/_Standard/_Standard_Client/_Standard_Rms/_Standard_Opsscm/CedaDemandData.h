#ifndef _CDEMANDD_H_
#define _CDEMANDD_H_

#include <CCSCedaData.h>
#include <CedaRloData.h>


void  ProcessDemandCf(void * popInstance, int ipDDXType, void *vpDataPointer, 
					  CString &ropInstanceName);



struct CCAFLIGHTDATA;

extern enum enumRecordState;

#define TURNAROUND_DEMAND '0'
#define INBOUND_DEMAND	'1'
#define OUTBOUND_DEMAND '2'
#define	CCI_DEMAND		'6'

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct DEMANDDATA {
    long    Urno;           // Unique Record Number in DEMCKI
    long    Ouro;           // Outbound Flight URNO
    char    Alid[11];        // ID of Allocation Unit
	long	Urud;			// URNO that links recs in DEMTAB, RPTTAB (functions) and RPQTAB (qualifications)
    CTime   Debe;           // Begin of Demand
    CTime   Deen;           // End of Demand
	char	Dety[2];		// Duty Type 0=Turnaround/1=Arr/2=Dep (0+2 belong to Dep,1 belongs to Arr)
//	char	Rety[3];		// Type 001=locat�on demand
	long    KKey;
	int		FlightFreq;

	int NextC;
	int PrevC;
	int Freq;

	DEMANDDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		KKey = 0;
		Debe = TIMENULL;
		Deen = TIMENULL;
		Freq = 0;
		FlightFreq = 0;
		NextC = 0;
		PrevC = 0;
	}

};

typedef CCSPtrArray<DEMANDDATA> DEMANDLIST;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaDemandData: public CCSCedaData
{
// Attributes
public:


	CString omFlnus;

    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omOuroMap;
    CCSPtrArray<DEMANDDATA> omData;

	CUIntArray omKKeyArray;
	CMapPtrToPtr omKKeyMap;

// Operations
public:
    CedaDemandData();
    ~CedaDemandData();

	void Register(void);

	bool ReadAll(CString &opUrnos, CTime opFrom, CTime opTo, bool bpClearAll = true);
	bool Read(char *pspWhere /*NULL*/, bool bpClearAll, bool bpDdx);


	bool InsertInternal(DEMANDDATA *prpDem, bool bpDdx = true);
	bool UpdateInternal(DEMANDDATA *prpDem, bool bpDdx);
	bool DeleteInternal(DEMANDDATA *prpDem, bool bpDdx  = true);

	bool ClearAll(bool bpWithRegistration = true);

	void AddToKeyMap(DEMANDDATA *prpDem);
	void DelFromKeyMap(DEMANDDATA *prpDem);

	bool  GetDemansByOuro(CCSPtrArray<DEMANDDATA> &ropDemandList, long lpOuro);

	DEMANDDATA  *GetDemandByUrno(long lpUrno);

	void  ProcessDemandBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	
	bool  IsPassFilter(DEMANDDATA *prlDemand);

	bool CompareCompressCca(DEMANDDATA *prpDemand1, DEMANDDATA *prpDemand2);

	void KompressFreq(CCSPtrArray<CCAFLIGHTDATA> &opFLights);

	void Kompress(CCSPtrArray<CCAFLIGHTDATA> &opFLights);

	bool GetDemandsByKKey(CCSPtrArray<DEMANDDATA> &ropDemandList, long lpKKey);

	void  GetCcas(DEMANDDATA *prlDemand, CStringArray &ropCcas);

	void GetDemandArrayByGroups(CCSPtrArray<DEMANDLIST> &ropDemandArrays, CCSPtrArray<DEMANDDATA> &ropDemands);

	bool GetCommonDemands(CCSPtrArray<DEMANDDATA> &ropDemandList);

	CString GetGroupNames( long opDemUrno);

	void KompressCommonDemands();

	int	 GetFlightsFromCciDemand(DEMANDDATA *prpDemand,CDWordArray& ropFlights);
	CString	 GetTextFromCciDemand(long opDemUrno);
	CString	 GetTextFromCciDemand(CCSPtrArray<DEMANDDATA> &ropDemands);

	void ProcessCcaDelete(long *plpCcaGhpu);
//	void ProcessCcaFlightChange(CCAFLIGHTDATA *prpFlight);
	
	CedaRloData omRloData;


};

#endif
