// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__1D5121C8_9846_11D3_97CB_00008630DDD6__INCLUDED_)
#define AFX_MAINFRM_H__1D5121C8_9846_11D3_97CB_00008630DDD6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <CcaDiaViewer.h>
#include <CCSClientWnd.h>
#include <CCS3dStatic.h>
#include <CCSButtonCtrl.h>
#include <CCSTimeScale.h>
#include <CViewer.h>
#include <ScrollToolBar.h>

/////////////////////////////////////////////////////////////////////////////
// CcaDiagram frame

enum 
{
	CCA_ZEITRAUM_PAGE,
	CCA_FLUGSUCHEN_PAGE,
	CCA_GEOMETRIE_PAGE
};




//#include "ChildView.h"

class CMainFrame : public CFrameWnd
{
	
public:
	CMainFrame();
protected: 
	DECLARE_DYNAMIC(CMainFrame)

// Attributes
public:


	CComboBox *pomViewCombo;
	CComboBox *pomViewSeasonCombo;

	CScrollToolBar* pomToolBar1;
	CScrollToolBar* pomToolBar2;

	void SetWndPos();
    void PositionChild();
    void SetTSStartTime(CTime opTSStartTime);
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName,bool RememberPositions = true);
	void OnUpdatePrevNext(void);
	void PrePlanMode(BOOL bpToSet,CTime opPrePlanTime);
	void ToggleOnline(void);
    void OnFirstChart();
    void OnLastChart();
	bool IsPassFlightFilter(CCAFLIGHTDATA *prpFlight);
	bool HasAllocatedCcas(CCSPtrArray<DIACCADATA> &ropCcaList);

	void ActivateTables();

// Attributes
public:

	CcaDiagramViewer omViewer;

	CString omOldWhere;
	void LoadFlights(char *pspView = NULL);

    CCS3DStatic omTime;
    CCS3DStatic omDate;
    CCS3DStatic omTSDate;

    CButton m_KlebeFkt;
    CCSTimeScale omTimeScale;
    CBitmapButton omBB1, omBB2;
    
    CCSClientWnd omClientWnd;
    CStatusBar omStatusBar;
    
    CTime omStartTime;
    CTimeSpan omDuration;
    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
    
    CPtrArray omPtrArray;
    int imFirstVisibleChart;
    int imStartTimeScalePos;
    
	CString omCaptionText;
	bool bmIsViewOpen;


	int imWhichMonitor;
	int imMonitorCount;


// Redisplay all methods for DDX call back function
public:
	BOOL bmNoUpdatesNow;

	void RedisplayAll();
	CListBox *GetBottomMostGantt();
// Time Band data, use the TIMENULL value for hiding the marker
public:
	void SetTimeBand(CTime opStartTime, CTime opEndTime);
	void UpdateTimeBand();
	void SetCaptionText(void);
	void SetMarkTime(CTime opStartTime, CTime opEndTime);
private:
	CTime omTimeBandStartTime;
	CTime omTimeBandEndTime;

private:
    CPoint omMaxTrackSize;
    CPoint omMinTrackSize;
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;
	
	bool bmSymbolText;
	bool CreateToolBar(void);

	CMapPtrToWord omPrivMap;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL
	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 

	void GetClientRect( LPRECT olRect);

	// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CScrollRebar     m_wndToolBar;

	bool SetPrivileges(void) ;

	// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnView();
	afx_msg void OnDelete();
	afx_msg void OnViewSelChange();
	afx_msg void OnAllocate();
	afx_msg void OnCreateDemands();
	afx_msg void OnUpdateCreateDemands(CCmdUI* pCmdUI);
	afx_msg LONG RepaintAll(WPARAM wParam, LPARAM lParam);
	afx_msg void OnWoDemand();
	afx_msg void OnDemand();
	afx_msg void OnConflicts();
	afx_msg void OnPrint();
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnNow();
	afx_msg void OnRules();
	afx_msg void OnBdpsuif();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg void OnAppExit();
	afx_msg void OnSeasonViewSelChange();
	afx_msg void OnFlightConflicts();
	afx_msg void OnSymbolText();
	afx_msg void OnReport();
	afx_msg void OnReportSeason();
	afx_msg void OnUpdateReportSeason(CCmdUI* pCmdUI); 
	afx_msg void OnUpdateSymbolText(CCmdUI* pCmdUI);
	afx_msg void OnUpdateToolBarButtons(CCmdUI* pCmdUI);
	afx_msg void OnClose();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnCreateCommon();
	afx_msg void OnUpdateCreateCommon(CCmdUI* pCmdUI); 
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__1D5121C8_9846_11D3_97CB_00008630DDD6__INCLUDED_)
