#if !defined(AFX_CICFCONFTABLEDLG_H__A5B9DCE1_B3B9_11D3_95E4_00001C018ACE__INCLUDED_)
#define AFX_CICFCONFTABLEDLG_H__A5B9DCE1_B3B9_11D3_95E4_00001C018ACE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CicFConfTableDlg.h : header file
//

#include <CicFConfTableViewer.h>
#include <CCSTable.h>
#include <CCSDragDropCtrl.h>


/////////////////////////////////////////////////////////////////////////////
// CicFConfTableDlg dialog

class CicFConfTableDlg : public CDialog
{
// Construction
public:
	void Reset();
	void Activate();
	CicFConfTableDlg(CWnd* pParent = NULL);   // standard constructor
	~CicFConfTableDlg();

	bool isCreated;

	CCSTable *pomTable; 
	
	CicFConfTableViewer omViewer;

	CCSDragDropCtrl omDragDropObject;

// Dialog Data
	//{{AFX_DATA(CicFConfTableDlg)
	enum { IDD = IDD_CICFCONFTABLE };
	CButton	m_R_AllConf;
	CButton	m_R_FlightConf;
	int m_R_Conf_Value;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CicFConfTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	long lmButtonSpace;  // y-coordinate of the bottom of the confirm button 
	CedaCflData *pomCurrData;  // current CFL-Data witch is displayed in the table  

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CicFConfTableDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBConfirmAll();
	afx_msg void OnBConfirm();
	afx_msg void OnRAllConf();
	afx_msg void OnRFlightConf();
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CICFCONFTABLEDLG_H__A5B9DCE1_B3B9_11D3_95E4_00001C018ACE__INCLUDED_)
