// ReportTableDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <OPSSCM.h>
#include <ReportTableDlg.h>
 
#ifdef _DEBUG 
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportTableDlg 

CReportTableDlg::CReportTableDlg(CWnd* pParent, const CTime &ropMinDate, const CTime &ropMaxDate)
	: CDialog(CReportTableDlg::IDD, pParent), romMinDate(ropMinDate), romMaxDate(ropMaxDate),
	imDlgBorder(5)
{
	
	bmIsCreated = false;
	pomReportGXGrid = NULL;
}


CReportTableDlg::~CReportTableDlg()
{
 
	delete pomReportGXGrid;
}


void CReportTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportTableDlg)
	//DDX_Control(pDX, IDC_BEENDEN, m_CB_Beenden);
	DDX_Control(pDX, IDC_PRINTER, m_RB_Printer);
	DDX_Control(pDX, IDC_FILE, m_RB_File);
 	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportTableDlg, CDialog)
	//{{AFX_MSG_MAP(CReportTableDlg)
	ON_WM_SIZE()
 	ON_BN_CLICKED(IDC_DRUCKEN, OnDrucken)
 	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportTableDlg 

BOOL CReportTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Get the top coordinate of the grid window
	WINDOWPLACEMENT* prlWinPlace = new WINDOWPLACEMENT;
	GetDlgItem(IDC_CUSTOM)->GetWindowPlacement(prlWinPlace);
	imTopSpace = prlWinPlace->rcNormalPosition.top;
	delete prlWinPlace;


	// Create stingray grid
	pomReportGXGrid = new CGXGridWnd;

	pomReportGXGrid->SubclassDlgItem(IDC_CUSTOM, this);
	pomReportGXGrid->Initialize();
	pomReportGXGrid->GetParam()->EnableUndo(FALSE);
	pomReportGXGrid->LockUpdate(TRUE);

	// No row headers
	pomReportGXGrid->HideCols(0,0);
	pomReportGXGrid->GetParam()->EnableTrackColWidth(GX_TRACK_DEFAULT | GX_TRACK_NOTHEADER); 

	pomReportGXGrid->GetParam()->EnableSelection(FALSE);

	pomReportGXGrid->SetRowCount(0);
	pomReportGXGrid->SetColCount(REPORTTABLE_COLCOUNT);
	//pomReportGXGrid->SetStyleRange(CGXRange(0, 0, 0, 10), CGXStyle( ).SetControl(GX_IDS_CTRL_STATIC));   

	pomReportGXGrid->SetDrawingTechnique(gxDrawUsingMemDC);
	//...{weiterer Code}...
	pomReportGXGrid->LockUpdate(FALSE);

	bmIsCreated = true;
 
 	omReportViewer.Attach(pomReportGXGrid);

 	// Load line-data into viewer
	omReportViewer.ChangeViewTo(romMinDate, romMaxDate);	
	
	SetWindowText(GetString(IDS_STRING1706));

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

 
void CReportTableDlg::OnDrucken() 
{
	TRACE("OnDrucken\n");
	if (m_RB_Printer.GetCheck() == 1)
	{
		omReportViewer.PrintTable();
	}
	if (m_RB_File.GetCheck() == 1)
	{
		// Get filename
		LPOPENFILENAME polOfn = new OPENFILENAME;

		memset(polOfn, 0, sizeof(*polOfn));

		polOfn->lStructSize = sizeof(*polOfn) ;
		polOfn->hwndOwner = m_hWnd;
		polOfn->lpstrFilter = "Excel File(*.xls)\0*.xls\0";
		char olFileNameBuf[256] = "c:\\tmp\\*.xls";
		polOfn->lpstrFile = (LPSTR) olFileNameBuf;
		polOfn->nMaxFile = 256;

		//strcpy(buffer2, GetString(IDS_GET_FILENAME));
		//polOfn->lpstrTitle = buffer2;
		if(GetOpenFileName(polOfn) != 0)
		{
	 		if (strlen(olFileNameBuf) != 0)
			{
				omReportViewer.PrintExcelFile(olFileNameBuf);
			}
			else
			{
				MessageBox(GetString(IDS_NO_FILE_SELECTED), GetString(ST_FEHLER), MB_ICONERROR); 
			}
		}
	}
}


void CReportTableDlg::OnSize(UINT nType, int cx, int cy) 
{
  	CDialog::OnSize(nType, cx, cy);
	if (nType != SIZE_MINIMIZED && bmIsCreated)
	{
  		pomReportGXGrid->SetWindowPos(NULL, 0, 0, cx-(2*imDlgBorder), cy-imDlgBorder-imTopSpace, SWP_NOZORDER | SWP_NOMOVE);
		pomReportGXGrid->Redraw(); 
	}
 
}


void CReportTableDlg::OnBeenden() 
{
	CDialog::OnCancel();
}


void CReportTableDlg::OnCancel() 
{
	CDialog::OnCancel();
}
