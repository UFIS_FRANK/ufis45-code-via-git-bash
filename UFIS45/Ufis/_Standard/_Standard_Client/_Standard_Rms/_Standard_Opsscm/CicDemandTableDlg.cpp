// CicDemandTableDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsscm.h>
#include <CicDemandTableDlg.h>
#include <CcaCedaFlightData.h>
#include <DataSet.h>
#include <SeasonDlg.h>
#include <CciDemandDetailDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CicDemandTableDlg dialog


CicDemandTableDlg::CicDemandTableDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CicDemandTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CicDemandTableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	isCreated = false;
	pomParent = pParent;

    CDialog::Create(CicDemandTableDlg::IDD, pParent);
	isCreated = true;

}

CicDemandTableDlg::~CicDemandTableDlg()
{
	delete pomTable;
}

void CicDemandTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CicDemandTableDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CicDemandTableDlg, CDialog)
	//{{AFX_MSG_MAP(CicDemandTableDlg)
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_DRAGOVER, OnDragOver)
	ON_MESSAGE(WM_DROP, OnDrop)  
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CicDemandTableDlg message handlers



void CicDemandTableDlg::Activate()
{


	if(!omViewer.bmInit)
		omViewer.ChangeViewTo("");

}


void CicDemandTableDlg::Reset()
{
	omViewer.ClearAll();
}



BOOL CicDemandTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	omDragDropObject.RegisterTarget(this, this);

	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	pomTable->SetHeaderSpacing(0);

	pomTable->bmMultiDrag = true;

	CRect olRect;
    GetClientRect(&olRect);

	//olRect.top = olRect.top + imDialogBarHeight;
	//olRect.bottom = olRect.bottom;// - imDialogBarHeight;

    olRect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);

	omViewer.Attach(pomTable);

	omViewer.ChangeViewTo("");

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CicDemandTableDlg::OnClose() 
{
	ShowWindow(SW_HIDE);	

//	omViewer.UnRegister();

	//CDialog::OnClose();
}


////////////////////////////////////////////////////////////////////////////
// Dragbeginn einer Drag&Drop Aktion
//
LONG CicDemandTableDlg::OnTableDragBegin(UINT wParam, LONG lParam)
{

	int ilCol;
	int ilLine;

	CCSPtrArray<DIACCADATA> olData;

	CICDEMANDTABLE_LINEDATA *prlLine = (CICDEMANDTABLE_LINEDATA*)pomTable->GetTextLineData(wParam);
	CPoint point;
    ::GetCursorPos(&point);
    pomTable->pomListBox->ScreenToClient(&point);    
    //ScreenToClient(&point);    

	if (prlLine != NULL)
	{
		ilLine = pomTable->GetLinenoFromPoint(point);
		ilCol = pomTable->GetColumnnoFromPoint(point);
	}
	else
		return 0L;


	int ilItems[300];
	int ilAnz = 300;
	int ilAnzSel = 0;

	ilAnz = (pomTable->GetCTableListBox())->GetSelItems(ilAnz, ilItems);
	if(ilAnz <= 0)
	{
		return 0L;
	}


	if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
	{
		omDragDropObject.CreateDWordData(DIT_CCA_DEMAND_KKEY, ilAnz);
		
		for(int i = 0; i < ilAnz; i++)
		{
			prlLine = (CICDEMANDTABLE_LINEDATA *)pomTable->GetTextLineData(ilItems[i]);
			if(prlLine->Urno != 0)
			{
				omDragDropObject.AddDWord((long)prlLine->KKey);
			}
		}
	}
	else
	{

		omDragDropObject.CreateDWordData(DIT_CCA_DEMAND, ilAnz);
		
		for(int i = 0; i < ilAnz; i++)
		{
			prlLine = (CICDEMANDTABLE_LINEDATA *)pomTable->GetTextLineData(ilItems[i]);
			if(prlLine->Urno != 0)
			{
				DEMANDDATA *prlDemand = ogCcaDiaFlightData.omDemandData.GetDemandByUrno(prlLine->Urno);

				if(prlDemand == NULL)
				{
					return 0L;
				}
				else
				{
					//if(strcmp(prlCca->Ctyp, "E") != 0)
					{
						omDragDropObject.AddDWord((long)prlLine->Urno);
					}
				}
			}
		}
	}

	ogBcHandle.BufferBc();
	omDragDropObject.BeginDrag();
	ogBcHandle.ReleaseBuffer();

	return 0L;
}


////////////////////////////////////////////////////////////////////////////
// linke Maustaste auf der Flighttable gedr�ckt
//
LONG CicDemandTableDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	
	UINT ipItem = wParam;
	CICDEMANDTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (CICDEMANDTABLE_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlTableLine != NULL)
	{
		if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
		{
			CCSPtrArray<CCAFLIGHTDATA> olFlights;
			CCSPtrArray<DEMANDDATA> olCcas;

			if( ogCcaDiaFlightData.omDemandData.GetDemandsByKKey(olCcas, prlTableLine->KKey) )
			{
				CUIntArray olUrnos;
				for(int i = 0; i < olCcas.GetSize(); i++)
				{
					olUrnos.Add(olCcas[i].Urno);
				}

				new CciDemandDetailDlg(this, olUrnos, "");
			}
			return 0L;
		}


		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlTableLine->FlightUrno);;
		if(prlFlight != NULL)
		{
			if (pogSeasonDlg)
				pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgGatPosLocal);
		}
		else
		{
			CUIntArray olUrnos;
			olUrnos.Add(prlTableLine->Urno);
//			if (prlTableLine->Flno == "COMMON")
				new CciDemandDetailDlg(this, olUrnos, "");
		}
	}
	
	return 0L;
}




LONG CicDemandTableDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{

	UINT ipItem = wParam;
	CICDEMANDTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (CICDEMANDTABLE_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlTableLine != NULL)
	{
		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlTableLine->Urno);;

		if(prlFlight != NULL)
			ogDdx.DataChanged((void *)this, SHOW_FLIGHT, (void*)&(prlFlight->Urno));
	}


	return 0L;

}

void CicDemandTableDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(isCreated != false)
	{
		CDialog::OnSize(nType, cx, cy);
	
		if (nType != SIZE_MINIMIZED)
		{
			pomTable->SetPosition(1, cx+1, 1/*m_nDialogBarHeight-1*/, cy+1);
		}
	}
}
LONG CicDemandTableDlg::OnDragOver(UINT wParam, LONG lParam)
{
	int ilLine = -1;
	int ilCol = -1;
	int ilLogicLine = -1;
	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);

   int ilClass = omDragDropObject.GetDataClass(); 
	if ((ilClass == DIT_CCA_GANTT) || (ilClass == DIT_CCA_KKEY))
	{
		//long ilUrno = m_DragDropTarget.GetDataDWord(0);
		//GHDDATA *prlGhd = ogGhdData.GetJobByUrno(ilUrno);
		//if(prlGhd!= NULL)
		//{
		//	if(strcmp(prlGhd->Dtyp, "D") == 0)
		//	{
		//		return 0;
		//	}
		//	else
		//	{
		//		return -1L;
		//	}
		//}
		//TO DO: Pr�fen, ob DTY OK
		return 0;	// cannot interpret this object
	}

	return -1L;
}

LONG CicDemandTableDlg::OnDrop(UINT wParam, LONG lParam)
{
	CCSDragDropCtrl *pomDragDropCtrl = &omDragDropObject;

	DROPEFFECT lpDropEffect = wParam;

	switch (pomDragDropCtrl->GetDataClass())
	{
	case DIT_CCA_GANTT: // Drop from this gantt to another point
		return ProcessDropCcaDuty(pomDragDropCtrl, lpDropEffect);
	case DIT_CCA_KKEY: // Drop from this gantt to another point
		return ProcessDropCcaKKeyDuty(pomDragDropCtrl, lpDropEffect);
	}
    return -1L;

}

LONG CicDemandTableDlg::ProcessDropCcaDuty(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	long ilUrno = popDragDropCtrl->GetDataDWord(0);
	DIACCADATA *prlCca = ogCcaDiaFlightData.omCcaData.GetCcaByUrno(ilUrno);
	if(prlCca != NULL)
	{
		ogDataSet.DeAssignCca(prlCca->Urno);
			//pomParent->SendMessage(WM_REPAINT_ALL,0,0);

	}
	return 0L;
}


LONG CicDemandTableDlg::ProcessDropCcaKKeyDuty(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	long ilKKey = popDragDropCtrl->GetDataDWord(0);
	ogDataSet.DeAssignKKeyCca(ilKKey);
			//pomParent->SendMessage(WM_REPAINT_ALL,0,0);
	return 0L;
}

