#if !defined(AFX_CICCREATEDEMANDSDLG_H__B9B7DBF3_C8C5_11D3_8FB3_0000B4984BBE__INCLUDED_)
#define AFX_CICCREATEDEMANDSDLG_H__B9B7DBF3_C8C5_11D3_8FB3_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CicCreateDemandsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CicCreateDemandsDlg dialog

class CicCreateDemandsDlg : public CDialog
{
// Construction
public:
	CicCreateDemandsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CicCreateDemandsDlg)
	enum { IDD = IDD_CREATE_DEMANDS };
	CString	m_SToTimeVal;
	CString	m_SToDateVal;
	CString	m_SFromDateVal;
	CString	m_SFromTimeVal;
	CString	m_LstuDateVal;
	CString	m_FAirlineVal;
	CString	m_FlightNrVal;
	CString	m_FlightSuffixVal;
	CString	m_Act3Val;
	CString	m_RegnVal;
	CString	m_AirpVal;
	CString	m_Airp2Val;
	CString	m_RemaVal;
	BOOL	m_BetriebVal;
	BOOL	m_PlanungVal;
	CString	m_LstuTimeVal;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CicCreateDemandsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


public:
	CString omSearchText;

// Implementation

protected:

	bool GetSearchText(CString &ropWhere);

	// Generated message map functions
	//{{AFX_MSG(CicCreateDemandsDlg)
	afx_msg void OnCreateDemandsOk();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CICCREATEDEMANDSDLG_H__B9B7DBF3_C8C5_11D3_8FB3_0000B4984BBE__INCLUDED_)
