// CedaInitModuData.cpp
 
#include <stdafx.h>
#include <CedaInitModuData.h>

CedaInitModuData ogInitModuData;


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaInitModuData::CedaInitModuData()
{
	strcpy(pcmInitModuFieldList,"APPL,SUBD,FUNC,FUAL,TYPE,STAT");
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaInitModuData::~CedaInitModuData(void)
{
	TRACE("CedaInitModuData::~CedaInitModuData called\n");
}

//--SEND INIT MODULS-------------------------------------------------------------------------------------------

bool CedaInitModuData::SendInitModu()
{
	bool ilRc = true;

//INITMODU//////////////////////////////////////////////////////////////
	omInitModuData.Empty();

	GetInitModuData1();

//INITMODU END//////////////////////////////////////////////////////////


	int ilCount = omInitModuData.GetLength();

	char *pclInitModuData = new char[omInitModuData.GetLength()+3];
	strcpy(pclInitModuData,omInitModuData);

	int ilCount2 = strlen(pclInitModuData);

	char pclTableExt[256];

	strcpy(pclTableExt, pcgTableExt);
	strcat(pclTableExt, " ");

	ilRc = CedaAction("SMI",pclTableExt,pcmInitModuFieldList,"","",pclInitModuData);
	TRACE("SendInitModu: CedaReturn %d \n",ilRc);
	if(ilRc==false)
	{
		AfxMessageBox(CString("SendInitModu:\n") + omLastErrorMessage,MB_ICONEXCLAMATION);
	}
	delete pclInitModuData;
	omInitModuData.Empty();
	return ilRc;
}

//--------------------------------------------------------------------------------------------------------------



CString CedaInitModuData::GetInitModuTxt()
{
	omInitModuData.Empty();

	GetInitModuData1();

	return omInitModuData;
}
//--------------------------------------------------------------------------------------------------------------



void CedaInitModuData::GetInitModuData1()
{
		omInitModuData =  ogAppName + ",InitModu,InitModu,Initialisieren (InitModu),B,-,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Daten_Laden," + GetString(IMFK_LOAD_DATA) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Jetzt," + GetString(IMFK_NOW) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_L�schen," + GetString(IMFK_DELETE) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Einteilen," + GetString(IMFK_ALLOCATE) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Fl�ge_ohne_Bedarfe," + GetString(IMFK_WITHOUT_DEMANDS) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Bedarfe," + GetString(IMFK_DEMANDS) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Flug_Konflikte," + GetString(IMFK_FLIGHT_CONFLICTS) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Konflikte," + GetString(IMFK_KONFLICTS) + ",B,1,";		
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Stammdaten," + GetString(IMFK_BASEDATA) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Regelwerk," + GetString(IMFK_RULES) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Drucken," + GetString(IMFK_PRINT) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Info," + GetString(IMFK_INFO) + ",B,1,";





//		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_Act3," + GetString(IMFK_SEADLG_ACT3) + ",E,1";

		// Season Dlg			
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DCxx," + GetString(IMFK_SEADLG_DCXX) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_ACxx," + GetString(IMFK_SEADLG_ACXX) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DNoop," + GetString(IMFK_SEADLG_DNOOP) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_ANoop," + GetString(IMFK_SEADLG_ANOOP) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_OK," + GetString(IMFK_SEADLG_OK) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_ZURUECK," + GetString(IMFK_SEADLG_PREV) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_WEITER," + GetString(IMFK_SEADLG_NEXT) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_ARESET," + GetString(IMFK_SEADLG_ARESET) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DRESET," + GetString(IMFK_SEADLG_DRESET) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_Agents," + GetString(IDS_STRING558) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DailyMask," + GetString(IDS_STRING2049) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DDaly," + GetString(IMFK_SEADLG_DDALY) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DAlc3List23," + GetString(IMFK_SEADLG_DALC3LIST23) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AShowJfno," + GetString(IMFK_SEADLG_DSHOWJFNO) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DViaList," + GetString(IMFK_SEADLG_DVIALIST) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AViaList," + GetString(IMFK_SEADLG_AVIALIST) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_ADaly," + GetString(IMFK_SEADLG_ADALY) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AAlc3List," + GetString(IMFK_SEADLG_AALC3LIST) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AAlc3List23," + GetString(IMFK_SEADLG_AALC3LIST23) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DStatus," + GetString(IMFK_SEADLG_DSTAT_S) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DShowVia," + GetString(IMFK_SEADLG_DSHOWVIA) + ",B,1,";
		
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DShowJfno," + GetString(IDS_STRING405) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AStatus," + GetString(IDS_STRING406) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AStatus2," + GetString(IDS_STRING407) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AStatus3," + GetString(IDS_STRING408) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DStatus2," + GetString(IDS_STRING409) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DStatus3," + GetString(IDS_STRING410) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AShowVia," + GetString(IDS_STRING411) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AOrg3List," + GetString(IDS_STRING412) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DDes3List," + GetString(IDS_STRING413) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DAlc3List," + GetString(IDS_STRING414) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_Act35List," + GetString(IDS_STRING415) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DTtypList," + GetString(IDS_STRING416) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DStypList," + GetString(IDS_STRING417) + ",B,1,";
//		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DHtypList," + GetString(IDS_STRING418) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_ATTypList," + GetString(IDS_STRING419) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AStypList," + GetString(IDS_STRING420) + ",B,1,";
//		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AHtypList," + GetString(IDS_STRING421) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DWro1List," + GetString(IDS_STRING422) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DPstdList," + GetString(IDS_STRING423) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DGtd2List," + GetString(IDS_STRING424) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DGtd1List," + GetString(IDS_STRING425) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_DCinsList," + GetString(IDS_STRING426) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_APstaList," + GetString(IDS_STRING427) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AGta2List," + GetString(IDS_STRING428) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AGta1List," + GetString(IDS_STRING429) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AExt2List," + GetString(IDS_STRING430) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_AExt1List," + GetString(IDS_STRING431) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_ABlt2List," + GetString(IDS_STRING432) + ",B,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CB_ABlt1List," + GetString(IDS_STRING433) + ",B,1,";

		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ATmb2," + GetString(IDS_STRING434) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DLastChange," + GetString(IDS_STRING435) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DFluko," + GetString(IDS_STRING436) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DCreated," + GetString(IDS_STRING437) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DAlc3," + GetString(IDS_STRING438) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ALastChange," + GetString(IDS_STRING439) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AFluko," + GetString(IDS_STRING440) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ACreated," + GetString(IDS_STRING441) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_Seas," + GetString(IDS_STRING442) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DRem1," + GetString(IDS_STRING443) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DGtd2," + GetString(IDS_STRING444) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DGtd1," + GetString(IDS_STRING445) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AExt2," + GetString(IDS_STRING446) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AExt1," + GetString(IDS_STRING447) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ARem1," + GetString(IDS_STRING448) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_Act5," + GetString(IDS_STRING449) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ABlt2," + GetString(IDS_STRING450) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ABlt1," + GetString(IDS_STRING451) + ",E,1,";
//		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_Act3," + GetString(IDS_STRING452) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_Act3," + GetString(IMFK_SEADLG_ACT3) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_Ming," + GetString(IDS_STRING453) + ",E,1,";
//		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DHtyp," + GetString(IDS_STRING454) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DStev," + GetString(IDS_STRING455) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DTtyp," + GetString(IDS_STRING456) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DStyp," + GetString(IDS_STRING457) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DFlns," + GetString(IDS_STRING458) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DFltn," + GetString(IMFK_SEATAB_DFLNO) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DFlti," + GetString(IDS_STRING1820) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DStod," + GetString(IDS_STRING459) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DStoa," + GetString(IDS_STRING459) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DEtdi," + GetString(IDS_STRING460) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AEtai," + GetString(IDS_STRING461) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DDes3," + GetString(IDS_STRING462) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DDes4," + GetString(IDS_STRING1848) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_Pbis," + GetString(IDS_STRING463) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_Pvom," + GetString(IDS_STRING464) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_Freq," + GetString(IDS_STRING465) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DOrg3," + GetString(IDS_STRING466) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AGta2," + GetString(IMFK_SEATAB_AGTA2) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AGta1," + GetString(IMFK_SEATAB_AGTA1) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AStoa," + GetString(IDS_STRING467) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AStod," + GetString(IDS_STRING468) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AOrg3," + GetString(IDS_STRING469) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AOrg4," + GetString(IDS_STRING1847) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AStev," + GetString(IDS_STRING470) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AStyp," + GetString(IDS_STRING471) + ",E,1,";
//		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AHtyp," + GetString(IDS_STRING472) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ATtyp," + GetString(IDS_STRING473) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AFlns," + GetString(IDS_STRING474) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AFltn," + GetString(IDS_STRING475) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AFlti," + GetString(IDS_STRING1818) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AAlc3," + GetString(IDS_STRING476) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ADes3," + GetString(IDS_STRING477) + ",E,1,";

		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ADays," + GetString(IDS_STRING478) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DDays," + GetString(IDS_STRING479) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_APsta," + GetString(IMFK_SEATAB_APSTA) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DPstd," + GetString(IMFK_SEATAB_DPSTD) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_APabs," + GetString(IDS_STRING480) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_APaes," + GetString(IDS_STRING481) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DPdbs," + GetString(IDS_STRING482) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DPdes," + GetString(IDS_STRING483) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ATet1," + GetString(IDS_STRING484) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ATet2," + GetString(IDS_STRING485) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AGa1b," + GetString(IDS_STRING486) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AGa2b," + GetString(IDS_STRING487) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AGa1e," + GetString(IDS_STRING488) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AGa2e," + GetString(IDS_STRING489) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ATga1," + GetString(IDS_STRING490) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ATga2," + GetString(IDS_STRING491) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DTgd1," + GetString(IDS_STRING492) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DTgd2," + GetString(IDS_STRING493) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DGd1b," + GetString(IDS_STRING494) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DGd2b," + GetString(IDS_STRING495) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DGd1e," + GetString(IDS_STRING496) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DGd2e," + GetString(IDS_STRING497) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DWro1," + GetString(IDS_STRING498) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DTwr1," + GetString(IDS_STRING499) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DW1bs," + GetString(IDS_STRING500) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DW1es," + GetString(IDS_STRING501) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_ATmb1," + GetString(IDS_STRING502) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AB1bs," + GetString(IDS_STRING503) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AB2bs," + GetString(IDS_STRING504) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AB1es," + GetString(IDS_STRING505) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_AB2es," + GetString(IDS_STRING506) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DCins," + GetString(IDS_STRING507) + ",E,1,";

		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DBaz1," + GetString(IDS_STRING1849) + ",E,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CE_DBaz4," + GetString(IDS_STRING1850) + ",E,1,";

		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CL_ARemp," + GetString(IDS_STRING563) + ",C,1,";
		omInitModuData += GetString(IMSD_ROTDLG_SEA) + ",SEASONDLG_CL_DRemp," + GetString(IDS_STRING564) + ",C,1";

		
		/*
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// ButtonList			
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Warteraeume," + GetString(IMFK_WAITROOM) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Telexpool," + GetString(IMFK_TELEXPOOL) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Tagesflugplanerfassen," + GetString(IMFK_EDIT_DAY) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Stammdaten," + GetString(IMFK_BASICDATA) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Setup," + GetString(IMFK_SETUP) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Rueckgaengig," + GetString(IMFK_UNDO) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Regeln," + GetString(IMFK_RULES) + ",B,1,";

		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Online," + GetString(IMFK_ONLINE) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_LHHost," + GetString(IMFK_LH_HOST) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_LFZPositionen," + GetString(IMFK_AIRCRAFT_POS) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Konflikte," + GetString(IMFK_KONFLICTS) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Info," + GetString(IMFK_INFO) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Hilfe," + GetString(IMFK_HELP) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Gepaeckbaender," + GetString(IMFK_BELTS) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Gates," + GetString(IMFK_GATES) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Flugdiagramm," + GetString(IMFK_FLIGHTDIAGRAM) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Fluggastinfo," + GetString(IMFK_PASSINFO) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Coverage," + GetString(IMFK_COVERAGE) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Checkin," + GetString(IMFK_CHECKIN) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Acin," + GetString(IMFK_AC_IRR) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Achtung," + GetString(IMFK_ATTENTION) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Flugplanpflegen," + GetString(IMFK_EDIT_SCHEDULE) + ",B,1,";
		omInitModuData += GetString(IMSD_DESKTOP) + ",DESKTOP_CB_Flugplanerfassen," + GetString(IMFK_CREATE_SCHEDULE) + ",B,1,";


		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Season ButtonList			
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Verbinden," + GetString(IMFK_JOIN) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Teilen," + GetString(IMFK_SPLIT) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Sammelbearbeitung," + GetString(IMFK_BATCH_PRO) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Report," + GetString(IMFK_REPORT) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Drucken," + GetString(IMFK_PRINT) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Import," + GetString(IMFK_IMPORT) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Freigeben," + GetString(IMFK_RELEASE) + ",B,1,";
		//omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Fluko," + GetString(IMFK_FIKO) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Excel," + GetString(IMFK_EXCEL) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Entf," + GetString(IMFK_DELETE) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Einfuegen," + GetString(IMFK_INSERT) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Cxx," + GetString(IMFK_CXX) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Ansicht," + GetString(IMFK_VIEW) + ",B,1,";
		//omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Analyse," + GetString(IMFK_ANALYSE) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Aendern," + GetString(IMFK_CHANGE) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CB_Kopieren," + GetString(IMFK_COPY) + ",B,1,";
		omInitModuData += GetString(IMSD_SEA_BUTTONLIST) + ",SEASONBTL_CC_Ansicht," + GetString(IMFL_COMBO_VIEW) + ",C,1,";


		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Rotation Buttonlist			
		omInitModuData += GetString(IMSD_DAY_BUTTONLIST) + ",ROTATIONTAB_CB_Ansicht," + GetString(IMFK_VIEW) + ",B,1,";
		omInitModuData += GetString(IMSD_DAY_BUTTONLIST) + ",ROTATIONTAB_CC_Ansicht," + GetString(IMFK_VIEW) + ",C,1,";
		omInitModuData += GetString(IMSD_DAY_BUTTONLIST) + ",ROTATIONTAB_CB_Details," + GetString(IDS_STRING508) + ",B,1,";
		omInitModuData += GetString(IMSD_DAY_BUTTONLIST) + ",ROTATIONTAB_CB_Insert," + GetString(ST_EINFUEGEN) + ",B,1,";
		omInitModuData += GetString(IMSD_DAY_BUTTONLIST) + ",ROTATIONTAB_CB_JustNow," + GetString(IDS_STRING509) + ",B,1,";
		omInitModuData += GetString(IMSD_DAY_BUTTONLIST) + ",ROTATIONTAB_CB_Print," + GetString(IMFK_PRINT) + ",B,1,";

		omInitModuData += GetString(IMSD_DAY_TAB_ARRIVAL) + ",ROTATIONTAB_IL_PSTA_BY_ONBL," + GetString(IDS_STRING1545) + ",E,1,";
		*/

}


