#if !defined(AFX_SEASONASKDLG_H__727B86C1_5D07_11D1_8317_0080AD1DC701__INCLUDED_)
#define AFX_SEASONASKDLG_H__727B86C1_5D07_11D1_8317_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// SeasonAskDlg.h : header file
//
#include <resource.h>

/////////////////////////////////////////////////////////////////////////////
// SeasonAskDlg dialog

class SeasonAskDlg : public CDialog
{
// Construction
public:
	SeasonAskDlg(CWnd* pParent = NULL);   // standard constructor
	
	~SeasonAskDlg(void);
	void SetMessage(CString opMessage);
	void RemoveAllMessage();
	BOOL DestroyWindow(void);

// Dialog Data
	//{{AFX_DATA(SeasonAskDlg)
	enum { IDD = IDD_SEASONASK };
	CButton	m_CB_Cancel;
	CButton	m_CB_DownLoad;
	CListBox	m_MsgList;
	BOOL	m_CC_ViewActiv;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SeasonAskDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(SeasonAskDlg)
	virtual void OnCancel();
	afx_msg void OnDownload();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEASONASKDLG_H__727B86C1_5D07_11D1_8317_0080AD1DC701__INCLUDED_)
