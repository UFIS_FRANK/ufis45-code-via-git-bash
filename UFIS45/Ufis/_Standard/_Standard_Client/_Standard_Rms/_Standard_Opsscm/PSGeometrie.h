#if !defined(AFX_PSGEOMETRIE_H__1B055C53_8B1E_11D1_B444_0000B45A33F5__INCLUDED_)
#define AFX_PSGEOMETRIE_H__1B055C53_8B1E_11D1_B444_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// PSGeometrie.h : header file
//
#include <CCSTable.h>
#include <resource.h>
#include <CCSDragDropCtrl.h>
#include <CCSPtrArray.h>
//#include "CedaPfcData.h"
//#include "CedaGegData.h"


struct PS_GEO_GROUP_DATA
{
	CString Name;
	CString Typ;
	CCSPtrArray<CString> Codes;
};

#define ONE_HOUR_POINT  (10 / 2)
/////////////////////////////////////////////////////////////////////////////
// PSGeometrie dialog

class PSGeometrie : public CPropertyPage
{
	DECLARE_DYNCREATE(PSGeometrie)

// Construction
public:
	PSGeometrie();
	~PSGeometrie();

	CCSPtrArray<PS_GEO_GROUP_DATA> omGroupData;
	PS_GEO_GROUP_DATA *pomCurrentGroup;

	CStringArray omValues;
	CString omCalledFrom;
	bool blIsInit;
    BOOL bmNotMoreThan12;
    CTime   m_TimeScale;
	int		m_Percent;
	int		m_Hour;
	int		m_FontHeight;

	bool bmMaxDefault,
		 bmLinesDefault,
		 bmTimeDefault,
		 bmVertDefault,
		 bmDisplayBeginDefault;
//	CCSPtrArray<GEGDATA>	omGegData;
//	CCSPtrArray<PFCDATA>	omPfcData;
	CCSTable *pomGroupTable;

	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;

	void SetCalledFrom(CString opCalledFrom);
// Dialog Data
	//{{AFX_DATA(PSGeometrie)
	enum { IDD = IDD_PSGEOMETRIE };
	CStatic	m_TestText;
	CSliderCtrl	m_Font;
	CEdit	m_Date;
	CButton	m_Min;
	CButton	m_Max;
	CButton	m_Pfc;
	CButton	m_Geg;
	CButton	m_New;
	CListBox	m_ResList;
	CSliderCtrl	m_Height;
	CButton	m_TimeLines;
	CButton	m_Delete;
	CButton	m_24h;
	CButton	m_12h;
	CButton	m_10h;
	CButton	m_8h;
	CButton	m_6h;
	CButton	m_4h;
	CButton	m_100Proz;
	CButton	m_75Proz;
	CButton	m_50Proz;
	CStatic	m_GRUPPE;
	CCSEdit	m_Time;
	//}}AFX_DATA

	void ShowTable();
	void ShowResList();
	void SetData();
	void GetData();
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(PSGeometrie)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(PSGeometrie)
	virtual BOOL OnInitDialog();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnNew();
	afx_msg void OnGeg();
	afx_msg void OnPfc();
	afx_msg void On10h();
	afx_msg void On12h();
	afx_msg void On24h();
	afx_msg void On4h();
	afx_msg void On6h();
	afx_msg void On8h();
	afx_msg void OnSelchangeList1();
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnDelete();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSGEOMETRIE_H__1B055C53_8B1E_11D1_B444_0000B45A33F5__INCLUDED_)
