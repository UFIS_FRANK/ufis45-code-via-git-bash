// DialogGrid.cpp : implementation file
//

#include <stdafx.h>
#include <PoolAlloc.h>
#include <PoolAllocDlg.h>
#include <DialogGrid.h>
#include <BasicData.h>

#include <CedaBasicData.h>
#include <BasicData.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//---------------------------------------------------------------------------------------------------------------------
//					defines, global functions, etc.
//---------------------------------------------------------------------------------------------------------------------

#define MIN_COL_WIDTH		20		// minimum column width for all grid columns


int StringComp(const RecordSet** ppRecord1, const RecordSet** ppRecord2);



//---------------------------------------------------------------------------------------------------------------------
//					construction / destruction
//---------------------------------------------------------------------------------------------------------------------
CGridFenster::CGridFenster()
{
	pomParent = NULL;
	bmSortNumerical = false;
	bmSortAscend = true;
	bmIsSorting = true;
	bmCopyStyle = true;
	bmIsComboRegistered = false;

	imSortKey = 1;
	imCurrentRow = -1;
	imGrowLeavingCol = -1;
}

CGridFenster::CGridFenster(CWnd *pParent)
{
	pomParent = pParent;
	bmSortNumerical = false;
	bmSortAscend = true;
	bmIsSorting = true;
	imSortKey  = 1;
	imCurrentRow = -1;
	imGrowLeavingCol = -1;
}
//---------------------------------------------------------------------------------------------------------------------

CGridFenster::~CGridFenster()
{
	omSpecialFormats.RemoveAll ();
}

//---------------------------------------------------------------------------------------------------------------------
//					message map
//---------------------------------------------------------------------------------------------------------------------
BEGIN_MESSAGE_MAP(CGridFenster, CGXGridWnd)
	//{{AFX_MSG_MAP(CGridFenster)
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


//---------------------------------------------------------------------------------------------------------------------
//					message handlers
//---------------------------------------------------------------------------------------------------------------------
void CGridFenster::OnLButtonUp(UINT nFlags, CPoint point) 
{
	//--
	// checks sorting
	//-- 

	CGXGridWnd ::OnLButtonUp(nFlags, point);

    ROWCOL  Row;
	ROWCOL  Col;

	//--- Betroffene Zelle ermitten
    HitTest(point, &Row,  &Col, NULL);

	//--- Check f�r StingRay Fehler
	if(Col > GetColCount())
		return;


	//--- Sortieren je nachdem welcher Header gedr�ckt wurde
	if(Row == 0 && Col != 0 && bmIsSorting)
	{
		CGXSortInfoArray  sortInfo;
		sortInfo .SetSize(1);
		
		// switch between sorting in ascending / descending order with each click
		if (bmSortAscend == true)
		{
			sortInfo[0].sortOrder = CGXSortInfo::descending;
			bmSortAscend = false;
		}
		else
		{
			sortInfo[0].sortOrder = CGXSortInfo::ascending;
			bmSortAscend = true;
		}
		
		sortInfo[0].nRC = Col;                       
		sortInfo[0].sortType = CGXSortInfo::autodetect;  
		SortRows( CGXRange().SetTable(), sortInfo); 

		//--- Merke welche Spalte f�r sorting verwendet wurde
		imSortKey  = Col;

		//--- Check ob numerisch sortiert wurde 
		CGXStyle  il_Cell_Style;
		GetStyleRowCol(1, Col, il_Cell_Style);


		if(il_Cell_Style.GetValueType() ==  GX_VT_NUMERIC )
		  bmSortNumerical = true;
		else
		  bmSortNumerical = false;
	}
}
//---------------------------------------------------------------------------------------------------------------------

BOOL  CGridFenster::OnSelDragDrop (ROWCOL nStartRow, ROWCOL nStartCol, ROWCOL nDragRow, ROWCOL nDragCol)
{
	//--
	// check dragging of rows
	//--

	 //---  Verschieben der Zeilen nur wenn numerisch sortiert
	 if( bmSortNumerical == true)
	 {
	      CGXGridCore::OnSelDragDrop (nStartRow, nStartCol, nDragRow, nDragCol);

		  //--- Neu durchnumerieren 
		  SetReadOnly(false);

	        int  il_ilSize = GetRowCount();

	        for(int  i = 1; i <= il_ilSize; i++) 
                 SetValueRange(CGXRange( i , 1), (WORD) i);  

		    Redraw();

		  SetReadOnly(true);

		  return true;
     }		   
	 else
	 {
	    // AfxMessageBox(GetString(IDS_STRING_NO_MOVE));
 	    return false;

	 }
}
//---------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::OnTrackColWidth(ROWCOL nCol)
{
	//------------------------------------
	// overwritten virtual of CGXGridCore
	//------------------------------------

	if (IsColHidden(nCol) == TRUE)
		return FALSE;
	else
		return TRUE;

}
//---------------------------------------------------------------------------------------------------------------------

int CGridFenster::GetColWidth(ROWCOL nCol)
{
	//----------------------------------
	// test if cols can be hidden or not
	//----------------------------------

	int nRet = CGXGridCore::GetColWidth(nCol);
	
	for (int i = 0; i < omVisibleCols.GetSize(); i++)
	{
		int ilColNo = (int) omVisibleCols.GetAt(i);
		if ((int)nCol == ilColNo)
		{
			if (nRet < MIN_COL_WIDTH)
			{
				nRet = MIN_COL_WIDTH;
			}
			else
			{
				nRet = 0;
			}
		}
	}

	//-- columns 6 to 8 stay hidden (width = 0)
	/*if (nCol <= 5)
	{
		//-- first 5 columns' width will be set to at least MIN_COL_WIDTH
		if (nRet < MIN_COL_WIDTH)
		{
			nRet = MIN_COL_WIDTH;
		}
	}
	else
	{
		nRet = 0;
	}*/

	return nRet;
}
//------------------------------------------------------------------------------------------------------------------------

void  CGridFenster::OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol) 
{
	//--------------------------------------
	// Click onto a Pushbutton inside the grid
	// ==> Message to parent dialog
	//--------------------------------------


	CELLPOS rlPos;
	rlPos.Col = nCol;
	rlPos.Row = nRow;

	CDialog*  polDlg = NULL;
	
	if(pomParent == NULL)
	{
		polDlg = (CDialog*) GXGetParentWnd(this, RUNTIME_CLASS(CDialog), TRUE);
	}
	else 
	{
		polDlg = (CDialog*) pomParent;
	}

	pomParent->SendMessage(GRID_MESSAGE_BUTTONCLICK, (WPARAM)this, (LPARAM)&rlPos);
}
//------------------------------------------------------------------------------------------------------------------------

BOOL  CGridFenster::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt) 
{
	//---
	// Click into a cell of the grid
	// ==> Message to parent dialog
	//---

	CELLPOS rlPos;
	rlPos.Col = nCol;
	rlPos.Row = nRow;

	CDialog*  polDlg = NULL;

	if(pomParent == NULL)
	{
		polDlg = (CDialog*) GXGetParentWnd(this, RUNTIME_CLASS(CDialog), TRUE);
	}
	else 
	{
		polDlg = (CDialog*) pomParent;
	}
		
	polDlg->SendMessage(GRID_MESSAGE_CELLCLICK, (WPARAM)this, (LPARAM)&rlPos);

	return TRUE;
}
//------------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	//---
	// DoubleClick into a cell of the grid
	// ==> Message to parent dialog
	//---

	CELLPOS rlPos;
	rlPos.Col = nCol;
	rlPos.Row = nRow;

	CDialog*  polDlg = NULL;

	if(pomParent == NULL)
	{
		polDlg = (CDialog*) GXGetParentWnd(this, RUNTIME_CLASS(CDialog), TRUE);
	}
	else 
	{
		polDlg = (CDialog*) pomParent;
	}
		
	polDlg->SendMessage(GRID_MESSAGE_DOUBLECLICK, (WPARAM)this, (LPARAM)&rlPos);

	return TRUE;
}
//------------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	CELLPOS rlPos;
	rlPos.Col = nCol;
	rlPos.Row = nRow;
	rlPos.x = pt.x;
	rlPos.y = pt.y;

	CDialog*  polDlg = NULL;
	
	if(pomParent == NULL)
	{
		polDlg = (CDialog*) GXGetParentWnd(this, RUNTIME_CLASS(CDialog), TRUE);
	}
	else 
	{
		polDlg = (CDialog*) pomParent;
	}

	polDlg->SendMessage(GRID_MESSAGE_RCELLCLICK, (WPARAM)this, (LPARAM)&rlPos);
	return TRUE;
}
//------------------------------------------------------------------------------------------------------------------------

BOOL  CGridFenster::OnStartEditing(ROWCOL nRow, ROWCOL nCol)
{
	CELLPOS rlPos;
	rlPos.Col = nCol;
	rlPos.Row = nRow;

	CDialog*  polDlg = NULL;

	if (pomParent == NULL)
	{
		polDlg = (CDialog*) GXGetParentWnd(this, RUNTIME_CLASS(CDialog), TRUE);
	}
	else 
	{
		polDlg = (CDialog*) pomParent;
	}
		
	polDlg->SendMessage(GRID_MESSAGE_STARTEDITING, (WPARAM)this, (LPARAM)&rlPos);

	return TRUE;
}
//---------------------------------------------------------------------------------------------------------------------

BOOL  CGridFenster::OnEndEditing(ROWCOL nRow, ROWCOL nCol)
{
	//--- automatically add one line at bottom of grid ?
	if (bmAutoGrow == true)
	{
		if ((imGrowLeavingCol != -1))
		{
			if (imGrowLeavingCol == (int) nCol)
			{
				DoAutoGrow (nRow, nCol);
			}
		}
		else
		{
			DoAutoGrow(nRow, nCol);
		}
	}

	//--- send message to parent dialog
	CELLPOS rlPos;
	rlPos.Col = nCol;
	rlPos.Row = nRow;

	CDialog*  polDlg = NULL;

	if(pomParent == NULL)
	{
		polDlg = (CDialog*) GXGetParentWnd(this, RUNTIME_CLASS(CDialog), TRUE);
	}
	else 
	{
		polDlg = (CDialog*) pomParent;
	}
		
	polDlg->SendMessage(GRID_MESSAGE_ENDEDITING, (WPARAM)this, (LPARAM)&rlPos);
	// DisplayInfo (nRow, nCol);
	// theApp.SetStatusBarText ( CString("") );


	return TRUE;
}



//---------------------------------------------------------------------------------------------------------------------
//					helper functions
//---------------------------------------------------------------------------------------------------------------------
void CGridFenster::DoAutoGrow (ROWCOL nRow, ROWCOL nCol) 
{
	ROWCOL ilRowCount = GetRowCount();
	if (bmAutoGrow && (nRow >= ilRowCount))
	{	
		//  es wird bereits in letzter Zeile editiert, d.h. evt. Grid verl�ngern
		if (!GetValueRowCol(nRow, nCol).IsEmpty())
			InsertBottomRow ();
	}
}
//---------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::InsertBottomRow()
{
	ROWCOL ilRowCount = GetRowCount();
	
	BOOL blRet = InsertRows(ilRowCount + 1, 1);
	
	if (bmCopyStyle == true)
	{
		CopyStyleLastLineFromPrev();
	}
	
	return blRet;
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::CopyStyleLastLineFromPrev ( bool bpResetValues/*=true*/)
{
	ROWCOL ilRowCount = GetRowCount();
	CopyStyleOfLine(ilRowCount - 1, ilRowCount, bpResetValues);
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::CopyStyleOfLine(ROWCOL ipSourceLine, ROWCOL ipDestLine, bool bpResetValues/*=true*/)
{
	ROWCOL ilColCount = GetColCount ();
	CGXStyle olStyle;

	for (ROWCOL i = 0; i <= ilColCount; i++)
	{
		olStyle.Free();
		ComposeStyleRowCol(ipSourceLine, i, &olStyle);
		SetStyleRange(CGXRange(ipDestLine, i), olStyle);
		if (bpResetValues)
		{
			SetValueRange (CGXRange(ipDestLine, i), "");
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::ResetContent()
{
	BOOL blRet = TRUE;
	
	int ilCount = GetRowCount();
	if (ilCount > 0)
	{
		blRet &= RemoveRows(1, ilCount);
	}
	
	Redraw();

	return blRet;
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::OnModifyCell (ROWCOL nRow, ROWCOL nCol)
{
	CGXGridWnd::OnModifyCell(nRow, nCol);

	ROWCOL ilRowCount, ilColCount;
	ilRowCount = GetRowCount();
	ilColCount = GetColCount();

	if ( (nRow<=0) || (nRow>ilRowCount) || (nCol<=0) || (nCol>ilColCount) )
		return;
	DisplayInfo (nRow, nCol);
}

void CGridFenster::DisplayInfo ( ROWCOL nRow, ROWCOL nCol )
{
   	/*CString olWert, olAnzeige;
	CString	olRefTable, olRefCodeField;
	CHOICE_LISTS	*polChoice;
	CGXStyle		olStyle;

	ComposeStyleRowCol( nRow, nCol, &olStyle );
	if ( !olStyle.GetIncludeItemDataPtr () )
		return;
	olWert = GetValueRowCol ( nRow, nCol );
	
	polChoice = (CHOICE_LISTS*)olStyle.GetItemDataPtr();
	if ( polChoice && !olWert.IsEmpty() && 
		 ParseReftFieldEntry ( polChoice->RefTabField, olRefTable, olRefCodeField ) )
		olAnzeige = ogBCD.GetField( olRefTable, olRefCodeField, olWert, 
									polChoice->ToolTipField );
	SetStyleRange( CGXRange(nRow, nCol), 
				   CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, olAnzeige ));
	theApp.SetStatusBarText ( olAnzeige );*/
}


//---------------------------------------------------------------------------------------------------------------------
//					set methods
//---------------------------------------------------------------------------------------------------------------------
void CGridFenster::SetComboBox(CString opChoiceList, bool bpSort, bool bpTextFit, 
							   int ipRowFrom, int ipColFrom, int ipRowTo, int ipColTo)
{
	if (bmIsComboRegistered == false)
	{
		RegisterGxCbsDropDown(bpSort, bpTextFit);
	}

	int ilRowTo, ilColTo;
	if (ipRowTo == -1)
	{
		ilRowTo = ipRowFrom;
	}
	else
	{
		ilRowTo = ipRowTo;
	}

	if (ipColTo == -1)
	{
		ilColTo = ipColFrom;
	}
	else
	{
		ilColTo = ipColTo;
	}
	
	SetStyleRange(CGXRange(ipRowFrom, ipColFrom, ilRowTo, ilColTo), 
					CGXStyle().SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
							  .SetChoiceList(opChoiceList));
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::SetNumSort(bool b)
{
	bmSortNumerical = b;
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::SetGrowLeavingCol(int ipVal)
{
	imGrowLeavingCol = ipVal;
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::SetAutoGrow(bool b, bool bpCopyStyle)
{
	bmAutoGrow = b;
	bmCopyStyle = bpCopyStyle;
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::SetColsAlwaysVisible(CPtrArray opCols)
{
	omVisibleCols.RemoveAll();
	for (int i = 0; i < opCols.GetSize(); i++)
	{
		int	ilColNo = (int) opCols.GetAt(i);
		omVisibleCols.Add((void*)&ilColNo);
	}
}
//---------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::SetGridEnabled(bool bpEnable)
{
	BOOL blRet;

	int ilRowCount = GetRowCount();
	int ilColCount = GetColCount();

	if (ilRowCount > 1 && ilColCount > 1)
	{
		if (bpEnable == true)
		{
			blRet = SetStyleRange(CGXRange(1, 1, ilRowCount, ilColCount), CGXStyle().SetInterior(WHITE)
																					 .SetEnabled(TRUE)
																					 .SetReadOnly(FALSE));
			GetParam()->GetProperties()->SetColor(GX_COLOR_BACKGROUND, RGB(255,255,255));
			bmIsGridEnabled = true;
		}
		else
		{
			blRet = SetStyleRange(CGXRange(1, 1, ilRowCount, ilColCount), CGXStyle().SetInterior(SILVER)
																					 .SetValue(CString(""))
																					 .SetEnabled(FALSE)
																					 .SetReadOnly(TRUE));
			GetParam()->GetProperties()->SetColor(GX_COLOR_BACKGROUND, RGB(126,126,126));
			bmIsGridEnabled = false;
		}
	}
	Redraw();

	return blRet;
}
//-----------------------------------------------------------------------------------------------------------------------

void CGridFenster::SetSortingEnabled(bool bpEnable)
{
	if (bpEnable == true)
	{
		bmIsSorting = true;
	}
	else
	{
		bmIsSorting = false;
	}
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::RegisterGxCbsDropDown ( bool bpSort, bool bpTextFit )
{ 
	if (bmIsComboRegistered == false)
	{
		CGXComboBoxWnd *pWnd = new CGXComboBoxWnd(this);
		unsigned long flags = WS_VSCROLL | CBS_DROPDOWN | CBS_AUTOHSCROLL ;
		if ( bpSort )
			flags |= CBS_SORT;
		if ( bpTextFit )
			flags |= GXCOMBO_TEXTFIT;
		pWnd->Create ( flags, 0);
		pWnd->m_bFillWithChoiceList = TRUE;
		pWnd->m_bWantArrowKeys = TRUE;
		RegisterControl(GX_IDS_CTRL_CBS_DROPDOWN, pWnd);

		bmIsComboRegistered = true;
	}
}

//---------------------------------------------------------------------------------------------------------------------
//						get methods
//---------------------------------------------------------------------------------------------------------------------
// Sets the sort key to "ZZZZZZZZZZ" if the cell is empty
void CGridFenster::GetSortKey(CGXControl* pControl, ROWCOL nRow, ROWCOL nCol, const CGXSortInfo& sortInfo, const CGXStyle& style, CString& sKey)
{
	CGXGridCore::GetSortKey(pControl, nRow, nCol, sortInfo, style, sKey);
	
	if (sKey.IsEmpty() == TRUE)
	{
		sKey = "ZZZZZZZZZZZZZZZZZZZZ";
	}
}
//---------------------------------------------------------------------------------------------------------------------

bool CGridFenster::GetNumSort()
{
	return bmSortNumerical;
}
//---------------------------------------------------------------------------------------------------------------------

int CGridFenster::GetSortKey()
{
	return imSortKey;
}
//---------------------------------------------------------------------------------------------------------------------

int  CGridFenster::GetCurrentRow()
{
	return imCurrentRow;
}
//---------------------------------------------------------------------------------------------------------------------

int  CGridFenster::GetCurrentCol()
{
	return imCurrentCol;
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::SetSpecialFormat ( ROWCOL ipRow, ROWCOL ipCol, CString opForm )
{
	char    key[41];

	sprintf( key, "%d %d", ipRow, ipCol );
	if ( opForm.IsEmpty () )
		omSpecialFormats.RemoveKey ( key );
	else
		omSpecialFormats.SetAt ( key, opForm );
}
//---------------------------------------------------------------------------------------------------------------------

//  L�scht Zeile, wenn Zelle (ipRow,ipCol) leer ist
bool CGridFenster::DeleteEmptyRow (ROWCOL ipRow, ROWCOL ipCol) 
{
	ROWCOL  ilRowCount = GetRowCount();
	ROWCOL  ilLastRowToCheck = bmAutoGrow ? ilRowCount-1 : ilRowCount;

	if ((ipRow >= 1) && (ipRow <= ilLastRowToCheck) && GetValueRowCol(ipRow, ipCol).IsEmpty() == TRUE)
	{
		RemoveRows( ipRow, ipRow );
		return true;
	}
	else 
	{
		return false;
	}
}
//---------------------------------------------------------------------------------------------------------------------

bool CGridFenster::FormatSpecial ( ROWCOL ipRow, ROWCOL ipCol, CString &ropValue )
{
	char    key[41];
	CString olFormat;
	CString olFormated="-------";
	bool	blRet=false;


	sprintf( key, "%d %d", ipRow, ipCol );
	if ( !omSpecialFormats.Lookup ( key, olFormat ) )
		return false;
	if ( olFormat == "WDAY" )
	{
		ropValue = ropValue.Left(7);
		for ( int i=0; i<7; i++ )
			if ( ropValue.Find ( '1'+i ) >= 0 )
				olFormated.SetAt ( i, '1'+i );
		ropValue = olFormated;
		blRet = true;
	}
	return blRet;
}
//---------------------------------------------------------------------------------------------------------------------

bool CGridFenster::UnFormatSpecial ( ROWCOL ipRow, ROWCOL ipCol, CString &ropValue )
{
	char    key[41];
	CString olFormat;
	CString olUnFormated="";
	bool	blRet=false;


	sprintf( key, "%d %d", ipRow, ipCol );
	if ( !omSpecialFormats.Lookup ( key, olFormat ) )
		return false;
	if ( olFormat == "WDAY" )
	{
		ropValue = ropValue.Left(7);
		for ( int i=0; i<7; i++ )
			if ( ropValue.Find ( '1'+i ) >= 0 )
				olUnFormated += CString ( '1'+i );
		ropValue = olUnFormated;
		blRet = true;
	}
	return blRet;
}
//---------------------------------------------------------------------------------------------------------------------

//  Markiere Text ropValue als ung�ltig, wenn er nicht in Auswahlliste 
//	der Zelle (ipRow,ipCol) vorkommt
//	return:  true, wenn Wert als ung�ltig markiert worden ist
//			 false, wenn Wert g�ltig ist, oder Fehler aufgetreten ist
bool CGridFenster::MarkInValid (  ROWCOL ipRow, ROWCOL ipCol, CString &ropValue )
{
	CGXControl	*polCB; 
	CGXStyle	olGXStyle;
	CString		olFound, olChoiceList;
	int			index=0;
	COLORREF	ilColor = ogColors[WHITE_IDX];

	if ( !ropValue.IsEmpty () &&
		 GetStyleRowCol ( ipRow, ipCol, olGXStyle ) &&
		 olGXStyle.GetIncludeChoiceList() && 
		 ( polCB = GetControl ( ipRow, ipCol ) )
	   )
	{
		olChoiceList = olGXStyle.GetChoiceList();
		index = polCB->FindStringInChoiceList( olFound, ropValue, 
											   olChoiceList, TRUE );
		if ( index < 0 )
			ilColor = ogColors[ORANGE_IDX];
	}
	SetStyleRange ( CGXRange(ipRow, ipCol), CGXStyle().SetInterior(ilColor) );

	return (index<0);
}
//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
