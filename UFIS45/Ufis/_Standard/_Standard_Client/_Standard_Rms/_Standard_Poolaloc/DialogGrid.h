#if !defined(AFX_DIALOGGRID_H__041C8F83_B1E0_11D2_AAEE_00001C018CF3__INCLUDED_)
#define AFX_DIALOGGRID_H__041C8F83_B1E0_11D2_AAEE_00001C018CF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////////////////
//
// GridFenster.h : header file
//
// Notes: The CGridFenster class is derived from the standard CGXGridWnd class . 
//         
//        The class offers the possibility to create Templates for the display in the main
//        FormView Window . The Dialog offers all possible fields from the TSR table in a gridform
//        display on the left hand side for the 3 areas (turnaround, arrival, departure).
//         
//        From these lists the apropriate fields can be choosen and will then appear in the corresponding 
//        grid on the right handside .
//
//        The selections can be sorted alphabeticly and lines in the grids can be moved in order to 
//        represent the priority by position .
//
//        The finiched selections are stored in the TPL table .
//
//
// Date : March 1999
//
// Author : EDE 
//
// Modification History:
//
//


#include <CCSGlobl.h>




typedef struct RowColPos
{
	int Row;
	int Col;
	int x;
	int y;

	RowColPos(void)
	{
		Row = -1;
		Col = -1;
		x = -1;
		y = -1;
	}

} CELLPOS;





class CGridFenster : public CGXGridWnd
{

	//--- Construction / Destruction
public:
	CGridFenster();	// default constructor
	CGridFenster(CWnd *pParent);
	~CGridFenster();

//--- Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGridFenster)
	//}}AFX_VIRTUAL


//--- Implementation
	//--- ‹berschreiben von CGXGridCore virtual Funktionen
	BOOL OnSelDragDrop(ROWCOL nStartRow, ROWCOL nStartCol, ROWCOL nDragRow, ROWCOL nDragCol);
	BOOL OnStartEditing(ROWCOL nRow, ROWCOL nCol);
	BOOL OnEndEditing(ROWCOL nRow, ROWCOL nCol);
	BOOL OnTrackColWidth(ROWCOL nCol);

    BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	void OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol);
	void OnModifyCell (ROWCOL nRow, ROWCOL nCol);
	
	// helper functions
	BOOL ResetContent();
	BOOL InsertBottomRow();
	void DoAutoGrow (ROWCOL nRow, ROWCOL nCol);
	bool DeleteEmptyRow (ROWCOL ipRow, ROWCOL ipCol);
	void CopyStyleLastLineFromPrev(bool bpResetValues = true);
	void CopyStyleOfLine(ROWCOL ipSourceLine, ROWCOL ipDestLine, bool bpResetValues = true);
	void DisplayInfo ( ROWCOL nRow, ROWCOL nCol );
	void RegisterGxCbsDropDown ( bool bpSort, bool bpTextFit );

	// Set
	void SetComboBox(CString opChoiceList, bool bpSort, bool bpTextFit, 
                      int ipRowFrom, int ipColFrom, int ipRowTo = -1, int ipColTo = -1);
	void SetNumSort(bool b);
	void SetAutoGrow(bool b, bool bpCopyStyle = true);
	void SetSortingEnabled(bool bpEnable);
	BOOL SetGridEnabled(bool bpEnable);
	void SetColsAlwaysVisible(CPtrArray opCols);
	void SetGrowLeavingCol(int ipVal);
	

	// Get
	bool GetNumSort();
	int GetSortKey();
	int GetCurrentRow();
	int GetCurrentCol();
	int GetColWidth(ROWCOL nCol);
	void GetSortKey(CGXControl* pControl, ROWCOL nRow, ROWCOL nCol, const CGXSortInfo& sortInfo, const CGXStyle& style, CString& sKey);



//--- Generated message map functions
public:
	CMapStringToString omSpecialFormats;
	void SetSpecialFormat ( ROWCOL ipRow, ROWCOL ipCol, CString opForm );
	bool FormatSpecial ( ROWCOL ipRow, ROWCOL ipCol, CString &ropValue ); 
	bool UnFormatSpecial ( ROWCOL ipRow, ROWCOL ipCol, CString &ropValue ); 
	bool MarkInValid (  ROWCOL ipRow, ROWCOL ipCol, CString &ropValue );
protected:
	//{{AFX_MSG(CGridFenster)
	afx_msg  void  OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


//--- Attributes
	CWnd *pomParent;
	CPtrArray omVisibleCols;
	bool bmSortNumerical; //--- Ist numerisch sortiert ? 
	bool bmSortAscend;    //--- sort asdcending or descending ? 
	bool bmIsSorting;	  //--- Is sorting enabled ?
	bool bmAutoGrow;	  //--- Is AutoGrow enabled ?
	bool bmIsGridEnabled; //--- Is grid activated or deactivated ?
	bool bmCopyStyle;
	bool bmIsComboRegistered;

	int imSortKey; //--- Nach welchem Feld wird sortiert ?
	int imCurrentRow; 
	int imCurrentCol;	
	int imGrowLeavingCol;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOGGRID_H__041C8F83_B1E0_11D2_AAEE_00001C018CF3__INCLUDED_)
