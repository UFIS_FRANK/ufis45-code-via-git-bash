// CCSGlobl.h: interface for the CCSGlobl class.
//
//////////////////////////////////////////////////////////////////////

// modified by MNE, March 1999:

// REMARK:	Most of the structs used anywhere in the program can be found in this file.
//			There's one major exception: All structs that have to do with views
//			are contained in CedaCfgData. 



#if !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
#define AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_

#ifndef __CCSGLOBL_H__
#define __CCSGLOBL_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <CCSDefines.h>
#include <ccsdragdropctrl.h>
#include <CCSTime.h>
#include <CCSPtrArray.h>
#include <PrivList.h>
#include <DataSet.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <GuiLng.h>


//----------------------------------
//		forward declarations
//----------------------------------
class CCSBcHandle;
class CCSCedaCom;
class CCSDdx;
class CCSLog;
class CCSBcHandle;
class CedaBasicData;
class CInitialLoadDlg;
// actually the things contained in the local BasicData class should be part of CCSBasic (at least most of them)
class CCSBasic;		// (lib)
class CBasicData;	// (local)




//----------------------------------
//			define section
//----------------------------------
//--- messages
// grid
#define GRID_MESSAGE_CELLCLICK			(WM_USER + 1)
#define GRID_MESSAGE_BUTTONCLICK		(WM_USER + 2)
#define GRID_MESSAGE_DOUBLECLICK		(WM_USER + 3)
#define GRID_MESSAGE_STARTEDITING		(WM_USER + 4)
#define GRID_MESSAGE_ENDEDITING			(WM_USER + 5)
#define GRID_MESSAGE_CURRENTCELLCHANGED (WM_USER + 6)
#define GRID_MESSAGE_RCELLCLICK			(WM_USER + 7)

#define CM_DELETE						(WM_USER + 20)
// other



//--- Symbolic colors 
#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
#define SILVER  RGB(192, 192, 192)          // light gray
#define RED     RGB(255,   0,   0)
#define ORANGE  RGB(255, 132,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255,   0)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(  0, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)
#define LGREEN  RGB(128, 255, 128)
#define PYELLOW RGB(255, 255, 180)			// pastel yellow
#define PGREEN  RGB(190, 255, 190)			// pastel green
#define PBLUE   RGB(157, 255, 255)			// pastel blue
#define ROSE    RGB(255, 160, 160)			// pastel red
#define DARKBLUE RGB( 0,   0, 160)			// dark blue

//--- some values for color stuff
#define MAXCOLORS 64


//----------------------------------
//				enums
//----------------------------------
enum drool {SKIP = -1, NO, YES};


// colors 
enum enumColorIndexes
{
	RED_IDX = 0, WHITE_IDX, BLACK_IDX, BLUE_IDX, YELLOW_IDX, GREEN_IDX, GRAY_IDX, ORANGE_IDX, SILVER_IDX, MAROON_IDX, 
	LGREEN_IDX, OLIVE_IDX, NAVY_IDX, PURPLE_IDX, TEAL_IDX, LIME_IDX, FUCHSIA_IDX, AQUA_IDX, PYELLOW_IDX, PGREEN_IDX, 
	PBLUE_IDX, ROSE_IDX, DARKBLUE_IDX,
};


//--- DDX
enum enumDDXTypes
{
	BC_POA_NEW = 0, BC_POA_DELETE, BC_POA_CHANGE,	// POA
	POA_NEW, POA_DELETE, POA_CHANGE,	
	BC_POL_NEW = 0, BC_POL_DELETE, BC_POL_CHANGE,	// POL
	POL_NEW, POL_DELETE, POL_CHANGE,	
	BC_SDA_NEW = 0, BC_SDA_DELETE, BC_SDA_CHANGE,	// SDA
	SDA_NEW, SDA_DELETE, SDA_CHANGE,	
	SGR_TMP_NEW, SGR_TMP_DELETE, SGR_TMP_CHANGE,
	SGM_TMP_NEW, SGM_TMP_DELETE, SGM_TMP_CHANGE,
	POL_TMP_NEW, POL_TMP_DELETE, POL_TMP_CHANGE
};


//----------------------------------
//			structs
//----------------------------------
typedef struct ReferenceTime
{
	UINT		ID;
	CString		Name;
	CString		Field;

} REFTIME;
//----------------------------------

typedef struct ReferenceTable
{
	CString			RTAB;	// reference table
	CStringArray	Fields;	// fields in reference table

} REFTABLE;
//----------------------------------

typedef struct ValueUrno
{
	CString Field;
	CString Value;
	CString Urno;

} VALUE_URNO;
//----------------------------------

typedef struct RefTables
{
	CString Table;
	CCSPtrArray <VALUE_URNO> ValUrnoArr;

} REFTAB;
//----------------------------------

typedef struct ChoiceList
{
	CString		TabFieldName;	// reference (Base) field (Format: TAB.NAME)
	CString		ValueList;		// list of values

} CHOICE_LIST;




//----------------------------------
//			global functions
//----------------------------------
CString GetResString(UINT ID);
CString GetString(UINT ipIDS);
bool GetString (CString opRefField, CString opIDS, CString &ropText);

void DeleteBrushes();
void CreateBrushes();
CString GetDBString(CString ID);
bool SetDlgItemLangText ( CWnd *popWnd, UINT ilIDC, CString opRefField, 
						  CString opIDS );
bool SetWindowLangText ( CWnd *popWnd, UINT ipIDS );
bool EnableDlgItem ( CWnd *popWnd, UINT ipIDC, BOOL bpEnable, BOOL bpShow );
BOOL MoveDlgItem ( CWnd *popWnd, UINT ipIDC, int x, int y );
BOOL ResizeDlgItem ( CWnd *popWnd, UINT ipIDC, int cx, int cy );


//----------------------------------
//		external definitions
//----------------------------------

//--- field indices
// WGP (workgroups)
extern igWgpPgpuIdx;
extern igWgpPrflIdx;
extern igWgpRemaIdx;
extern igWgpUrnoIdx;
extern igWgpWgpcIdx;
extern igWgpWgpnIdx;
// SWG (?)
/*extern igSwgCodeIdx;
extern igSwgSurnIdx;
extern igSwgUrnoIdx;
extern igSwgVpfrIdx;
extern igSwgVpfoIdx;
// SPF (service functions)
extern igSpfFccoIdx;
extern igSpfFcnoIdx;
extern igSpfGtabIdx;
extern igSpfPrioIdx;
extern igSpfUpfcIdx;
extern igSpfUrnoIdx;
extern igSpfUsrvIdx;
// SPE (service permits)
extern igSpeCodeIdx;
extern igSpeSurnIdx;
extern igSpeUrnoIdx;
extern igSpeVpfrIdx;
extern igSpeVpfoIdx;
// SOR (?)
extern igSorCodeIdx;
extern igSorSurnIdx;
extern igSorUrnoIdx;
extern igSorVpfrIdx;
extern igSorVpfoIdx;*/
// SGR (Static Groups)
extern igSgrApplIdx;
extern igSgrFldnIdx;
extern igSgrGrdsIdx;
extern igSgrGrpnIdx;
extern igSgrGrsnIdx;
extern igSgrPrflIdx;
extern igSgrTabnIdx;
extern igSgrUrnoIdx;
extern igSgrUgtyIdx;
// SGM (Group Members)
extern igSgmPrflIdx;
extern igSgmTabnIdx;
extern igSgmUgtyIdx;
extern igSgmUrnoIdx;
extern igSgmUsgrIdx;
extern igSgmUvalIdx;
extern igSgmValuIdx;
// SDA (staff default allocation)
/*extern igSdaVptoIdx;
extern igSdaVpfrIdx;
extern igSdaUrnoIdx;
extern igSdaUpolIdx;
extern igSdaSurnIdx;
// SCO (?)
extern igScoCodeIdx;
extern igScoSurnIdx;
extern igScoUrnoIdx;
extern igScoVpfrIdx;
extern igScoVpfoIdx;*/
// POL (pools)
extern igPolUrnoIdx;
extern igPolPoolIdx;
extern igPolNameIdx;
// POA (pool assignments)
extern igPoaUvalIdx;
extern igPoaUrnoIdx;
extern igPoaUpolIdx;
extern igPoaReftIdx;
// PGP (planning groups)
extern igPgpMaxmIdx;
extern igPgpMinmIdx;
extern igPgpPgpcIdx;
extern igPgpPgpmIdx;
extern igPgpPgpnIdx;
extern igPgpPrflIdx;
extern igPgpRemaIdx;
extern igPgpUrnoIdx;
// PFC (personnel functions)
extern igPfcDptcIdx;
extern igPfcFctcIdx;
extern igPfcFctnIdx;
extern igPfcPrflIdx;
extern igPfcRemaIdx;
extern igPfcUrnoIdx;
// PER (permits)
extern igPerPrflIdx;
extern igPerPrioIdx;
extern igPerPrmcIdx;
extern igPerPrmnIdx;
extern igPerRemaIdx;
extern igPerUrnoIdx;
// ORG (organisation units)
extern igOrgDpt1Idx;
extern igOrgDpt2Idx;
extern igOrgDptnIdx;
extern igOrgPrflIdx;
extern igOrgRemaIdx;
extern igOrgUrnoIdx;
// COT (contract types)
extern igCotCtrcIdx;
extern igCotCtrnIdx;
extern igCotDptcIdx;
extern igCotMislIdx;
extern igCotMxslIdx;
extern igCotNowdIdx;
extern igCotPrflIdx;
extern igCotRemaIdx;
extern igCotSbpaIdx;
extern igCotUrnoIdx;
extern igCotWhpwIdx;
extern igCotWrkdIdx;
// ALO (allocation)
extern igAloAlocIdx;
extern igAloAlodIdx;
extern igAloAlotIdx;
extern igAloReftIdx;
extern igAloUrnoIdx;


// index for array sort function
extern igIdx;

//--- color and brush arrays
extern COLORREF ogColors[];
extern CBrush *ogBrushs[];

//--- command line strings
extern CStringArray ogCmdLineArgsArray;

//--- class objects
extern CCSBcHandle ogBcHandle;
extern CCSCedaCom ogCommHandler;  // The one and only CedaCom object
extern CCSDdx ogDdx;
extern CCSLog ogLog;
extern CedaBasicData ogBCD;
extern CCSBasic ogCCSBasic;
extern CBasicData ogBasicData;
extern PrivList ogPrivList;
extern DataSet ogDataSet;
extern CGUILng* pogGUILng;

extern CMapStringToString ogRestrictions;

extern CCSPtrArray <REFTABLE> ogRefTables;
extern CCSPtrArray <CHOICE_LIST> ogChoiceLists;
extern CCSPtrArray <REFTAB> ogRef;

extern CString ogAppName;
extern CString ogCallingApp;
extern char pcgUser[33];
extern char pcgPasswd[33];
extern char pcgConfigPath[142];
extern char pcgHome[4];
extern char pcgTableExt[10];

extern bool bgUseResourceStrings;
extern bool bgDebug;



//----------------------------------
//			defines
//----------------------------------
// radio button states
#define CHECKED		1
#define UNCHECKED	0

// color stuff
#define MAXCOLORS 64
#define FIRSTCONFLICTCOLOR 31



//----------------------------------
//			error handling
//----------------------------------
extern ofstream of_catch;

#define  CCS_TRY try{

#define  CCS_CATCH_ALL }\
						catch(...)\
						{\
						    char pclExcText[512]="";\
						    char pclText[1024]="";\
						    sprintf(pclText, GetString(136),\
							                 __FILE__, __LINE__);\
						    strcat(pclText, GetString(137));\
						    sprintf(pclExcText, GetString(138), __FILE__, __LINE__);\
						    of_catch << pclExcText << endl;\
							if(::MessageBox(NULL, pclText, GetString(139), (MB_YESNO)) == IDNO)\
							{\
						       ExitProcess(0);\
							}\
						}



//----------------------------------
//			BDPS-SEC macros
//----------------------------------
#define SetWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat=='-') plWnd.ShowWindow(SW_HIDE);

#define SetWndStatPrio_1(clStat1,clStat2, plWnd)\
	if(clStat1=='-'||clStat2=='-') plWnd.ShowWindow(SW_HIDE);\
	else if(clStat1=='0'||clStat2=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat1=='1'||clStat2=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}

#define SetpWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd->ShowWindow(SW_SHOW);plWnd->EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd->ShowWindow(SW_SHOW);plWnd->EnableWindow(FALSE);}\
	else if(clStat=='-') plWnd->ShowWindow(SW_HIDE);


// end globals
/////////////////////////////////////////////////////////////////////////////


#endif //__CCSGLOBL_H__
#endif // !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
