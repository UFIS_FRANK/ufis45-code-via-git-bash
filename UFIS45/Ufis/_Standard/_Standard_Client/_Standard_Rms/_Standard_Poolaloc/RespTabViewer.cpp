// RespTabViewer.cpp: implementation of the RespTabViewer class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <RespTabViewer.h>
#include <poolalloc.h>
#include <CedaBasicData.h>
#include <ccsglobl.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif


//------------------------------------------------------------------------------------
//					global functions
//------------------------------------------------------------------------------------
static void RespTabCf(void *popInstance, enum enumDDXTypes ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


//------------------------------------------------------------------------------------
//					Construction/Destruction
//------------------------------------------------------------------------------------
RespTabViewer::RespTabViewer()
{
	pomTable = NULL;
	//ogCCSDdx.Register(this, ALO_CHANGE, CString("STAFFTABLEVIEWER"), CString("Gpe Update/new"), StaffTableCf);
}
//------------------------------------------------------------------------------------

RespTabViewer::~RespTabViewer()
{
	// ogCCSDdx.UnRegister(this, NOTUSED);
    ClearAll();
}



//------------------------------------------------------------------------------------
//					implementation
//------------------------------------------------------------------------------------
void RespTabViewer::Attach(Table *popTable)
{
    pomTable = popTable;
}
//------------------------------------------------------------------------------------

void RespTabViewer::UpdateDisplay()
{
    //pomTable->SetHeaderFields("Allocation|Member");
	pomTable->SetHeaderFields(GetString(317) + "|" + GetString(318));
    pomTable->SetFormatList("22|30");

    // Load filtered and sorted data into the table content
	pomTable->ResetContent();
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		pomTable->AddTextLine(Format(&omLines[ilLc]), &omLines[ilLc]);
    }                                           

    // Update the table content in the display
    pomTable->DisplayTable();
}
//------------------------------------------------------------------------------------

void RespTabViewer::ChangeViewTo(CString opSgrUrno)
{
	
	omCurrSgr = opSgrUrno;

    ClearAll();
    MakeLines();

	UpdateDisplay();
}
//------------------------------------------------------------------------------------

void RespTabViewer::MakeLines()
{
	CCSPtrArray <RecordSet> olSgmArr;
	ogBCD.GetRecords("SGM_TMP", "USGR", omCurrSgr, &olSgmArr);
	int ilSgmCount = olSgmArr.GetSize();
	

	for (int i = 0; i < ilSgmCount; i++)
	{
		MakeLine(&(olSgmArr[i]));
	}
	
	olSgmArr.DeleteAll();
}
//------------------------------------------------------------------------------------

void RespTabViewer::MakeLine(RecordSet *popSgm)
{

    /*if( !IsPassFilter(prpGpe->Sftb,prpGpe->Sfte))
        return;*/
 

	CString olRightValue, olReft, olAloUrno, olRefUrno;
	RecordSet olAlo(ogBCD.GetFieldCount("ALO"));
	RESPTABLE_LINEDATA rlRespLine;
	
	olReft = popSgm->Values[igSgmTabnIdx];
	olAloUrno = popSgm->Values[igSgmUgtyIdx];
	
	
	if (olReft >= 8)
	{
		olRefUrno = popSgm->Values[igSgmUvalIdx];
		olRightValue = ogBCD.GetField(olReft.Left(3), "URNO", olRefUrno, olReft.Right(4));
		bool blTest = ogBCD.GetRecord("ALO", "URNO", olAloUrno, olAlo);
	}
	
	if (olRightValue.IsEmpty() == FALSE)
	{
		rlRespLine.AloText = olAlo.Values[igAloAlodIdx];	// ALO.ALOD for left column
		rlRespLine.AloUrno = olAloUrno;						// ALO.URNO (not shown)
		rlRespLine.SgmReft = olReft;						// reference table and field (REF.FLDN)
		rlRespLine.SgmUval = olRefUrno;						// urno of referenced item
		rlRespLine.MemberText = olRightValue;				// reference field in referenced table (right column)
		rlRespLine.SgmUrno = popSgm->Values[igSgmUrnoIdx];	// SGM.URNO

		CreateLine(&rlRespLine);
	}
}
//------------------------------------------------------------------------------------

int RespTabViewer::CreateLine(RESPTABLE_LINEDATA *prpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareLine(prpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]*/

	
	omLines.NewAt(ilLineno, (*prpLine));

    return ilLineno;
}
//------------------------------------------------------------------------------------

void RespTabViewer::AddLine(CString opAloText, CString opAloUrno, CString opReft, CString opGrpMem, CString opUval)
{
	RESPTABLE_LINEDATA rlLine;
	rlLine.AloText = opAloText;
	rlLine.AloUrno = opAloUrno;
	rlLine.MemberText = opGrpMem;
	rlLine.SgmReft = opReft;
	rlLine.SgmUval = opUval;

	CreateLine(&rlLine);
}
//------------------------------------------------------------------------------------

void RespTabViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}
//------------------------------------------------------------------------------------

BOOL RespTabViewer::FindLine(CString opMemberText, int &ripLineno)
{
    for (ripLineno = 0; ripLineno < omLines.GetSize(); ripLineno++)
	{
        if (omLines[ripLineno].MemberText == opMemberText)
            return TRUE;
	}
    return FALSE;
}
//------------------------------------------------------------------------------------

RESPTABLE_LINEDATA *RespTabViewer::GetLine(int ipLineno)
{
	if ((ipLineno >= 0) && (ipLineno < omLines.GetSize()))
	{
		return &(omLines[ipLineno]);
	}

	return NULL;
}
//------------------------------------------------------------------------------------

int RespTabViewer::GetLineCount()
{
	return omLines.GetSize();
}
//------------------------------------------------------------------------------------

void RespTabViewer::ClearAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}
//------------------------------------------------------------------------------------

CString RespTabViewer::Format(RESPTABLE_LINEDATA *prpLine)
{
    CString s = prpLine->AloText + "|"
		+ prpLine->MemberText;
		
	return s;
}
//------------------------------------------------------------------------------------

int RespTabViewer::CompareLine(RESPTABLE_LINEDATA *prpLine1, RESPTABLE_LINEDATA *prpLine2)
{
	int ilCompareResult;

	if (prpLine1->AloText == prpLine2->AloText)
	{
		ilCompareResult = ((prpLine1->MemberText == prpLine2->MemberText) ? 0 :
				((prpLine1->MemberText > prpLine2->MemberText) ? 1 : -1));
	}
	else if (prpLine1->AloText > prpLine2->AloText)
	{
		ilCompareResult = 1;
	}
	else
	{
		ilCompareResult = -1;
	}

	/*int	ilCompareResult = (prpLine1->AloText == prpLine2->AloText) ? 
		(prpLine1->MemberText == prpLine2->MemberText ? 0 : 
			(prpLine1->MemberText > prpLine2->MemberText) ?):
				(prpLine1->AloText > prpLine2->AloText)? 1: -1;*/

     return ilCompareResult;
}
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------