// RegisterDlg.cpp : implementation file
//

#include <stdafx.h>
#include <PoolAlloc.h>
#include <RegisterDlg.h>
#include <CedaInitModuData.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RegisterDlg dialog


RegisterDlg::RegisterDlg(CWnd* pParent /*=NULL*/) : CDialog(RegisterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RegisterDlg)
	//}}AFX_DATA_INIT
}


void RegisterDlg::DoDataExchange(CDataExchange* pDX)
{
	CCS_TRY

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RegisterDlg)
	DDX_Control(pDX, IDC_TIME, m_TIME);
	DDX_Control(pDX, IDC_TEXT, m_TEXT);
	DDX_Control(pDX, IDC_ABBRECHEN,		m_ABBRECHEN);
	DDX_Control(pDX, IDC_STARTEN,		m_STARTEN);
	DDX_Control(pDX, IDC_REGISTRIEREN,	m_RREGISTER);
	//}}AFX_DATA_MAP

	CCS_CATCH_ALL
}


BEGIN_MESSAGE_MAP(RegisterDlg, CDialog)
	//{{AFX_MSG_MAP(RegisterDlg)
	ON_BN_CLICKED(IDC_ABBRECHEN, OnAbbrechen)
	ON_BN_CLICKED(IDC_REGISTRIEREN, OnRegistrieren)
	ON_BN_CLICKED(IDC_STARTEN, OnStarten)
    ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RegisterDlg message handlers
//---------------------------------------------------------------------------

BOOL RegisterDlg::OnInitDialog() 
{
	CCS_TRY

	CDialog::OnInitDialog();
	SetStaticTexts();


	CString olTmpText,olTmpTime;
	if(ogPrivList.GetSize() > 1)
	{
		olTmpText = CString(GetString(IDS_STRING1353))+
				 CString(GetString(IDS_STRING1354))+
				 CString(GetString(IDS_STRING1355))+
				 CString(GetString(IDS_STRING1356));
		olTmpTime = GetString(1357);
		SetTimer(4711,1000,NULL);
		imTimer = 15;
	}
	if(ogPrivList.GetSize() == 1)
	{
		olTmpText = CString(GetString(IDS_STRING1358)) +
				 CString(GetString(IDS_STRING1359));

		m_STARTEN.EnableWindow(FALSE);	
	}
	m_TEXT.SetWindowText(olTmpText);
	m_TIME.SetWindowText(olTmpTime);

	SetBdpsState();	//-- activate/deactivate elements according to state of BDPS-SEC 

	CCS_CATCH_ALL

	return TRUE;  
}
//---------------------------------------------------------------------------

void RegisterDlg::OnAbbrechen() 
{
	KillTimer(4711);
	CDialog::OnCancel();
}
//---------------------------------------------------------------------------

void RegisterDlg::OnRegistrieren() 
{
	CCS_TRY

	KillTimer(4711);
	m_TIME.SetWindowText(GetString(IDS_STRING1360));
	m_TIME.InvalidateRect(NULL);
	m_TIME.UpdateWindow();
	AfxGetApp()->DoWaitCursor(1);
	m_ABBRECHEN.EnableWindow(FALSE);	
	m_STARTEN.EnableWindow(FALSE);	
	m_RREGISTER.EnableWindow(FALSE);
	ogInitModuData.SendInitModu();
	AfxGetApp()->DoWaitCursor(-1);
	CDialog::OnCancel();

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------

void RegisterDlg::OnStarten() 
{
	KillTimer(4711);
	CDialog::OnOK();
}
//---------------------------------------------------------------------------

void RegisterDlg::OnTimer(UINT nIDEvent)
{
	CCS_TRY

	CDialog::OnTimer(nIDEvent);
	CString olTmpTime;
	imTimer--;
	if(imTimer == 0)
	{
		KillTimer(4711);
		OnStarten();
	}
	else
	{
		olTmpTime.Format(GetString(IDS_STRING1361),imTimer);
		m_TIME.SetWindowText(olTmpTime);
	}

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------

void RegisterDlg::SetBdpsState()
{
	// Set activation state acording to BDPS-SEC

	CCS_TRY

	CWnd *polWnd = NULL;
	CWnd *polWnd1 = NULL;

	polWnd = GetDlgItem(IDC_TEXT);
	if(polWnd != NULL)
	{
		switch(ogPrivList.GetStat("REGISTERDLG_TEXT"))
		{
		case '0':
			// Disable text
			polWnd->EnableWindow(FALSE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		case '-':
			// Hide text
			polWnd->ShowWindow(SW_HIDE);
			break;
		case '1':
			// Activate text
			polWnd->EnableWindow(TRUE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}
	} 

	polWnd = GetDlgItem(IDC_TIME);
	if(polWnd != NULL)
	{
		switch(ogPrivList.GetStat("REGISTERDLG_TIME"))
		{
		case '0':
			// Disable time
			polWnd->EnableWindow(FALSE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		case '-':
			// Hide time
			polWnd->ShowWindow(SW_HIDE);
			break;
		case '1':
			// Activate time
			polWnd->EnableWindow(TRUE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}
	} 

	polWnd = GetDlgItem(IDC_STARTEN);
	if(polWnd != NULL)
	{
		switch(ogPrivList.GetStat("REGISTERDLG_START"))
		{
		case '0':
			// Disable Start Button
			polWnd->EnableWindow(FALSE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		case '-':
			// Hide Start Button
			polWnd->ShowWindow(SW_HIDE);
			break;
		case '1':
			// Activate Start Button
			polWnd->EnableWindow(TRUE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}
	} 

	polWnd = GetDlgItem(IDC_REGISTRIEREN);
	if(polWnd != NULL)
	{
		switch(ogPrivList.GetStat("REGISTERDLG_REGISTER"))
		{
		case '0':
			// Disable Register Button
			polWnd->EnableWindow(FALSE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		case '-':
			// Hide Register Button
			polWnd->ShowWindow(SW_HIDE);
			break;
		case '1':
			// Activate Register Button
			polWnd->EnableWindow(TRUE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}
	} 

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------

void RegisterDlg::SetStaticTexts()
{
	SetWindowText(GetString(57344));

	CWnd *pWnd = GetDlgItem(IDC_STARTEN);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1371));

	pWnd = GetDlgItem(IDC_REGISTRIEREN);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1373));

	pWnd = GetDlgItem(IDC_ABBRECHEN);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(244));
}