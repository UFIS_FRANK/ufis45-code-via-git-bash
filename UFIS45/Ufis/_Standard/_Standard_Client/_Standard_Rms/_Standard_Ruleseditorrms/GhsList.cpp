// GhsList.cpp : implementation file
//

#include <stdafx.h>
#include <CCSEdit.h>
#include <GhsList.h>
#include <GhsListViewerPropertySheet.h> 
//#include "DutyPreferences.h"
#include <PrivList.h>
#include <TableWithGrid.h>
#include <mainfrm.h>
#include <RulesFormView.h>
#include <CedaBasicData.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif


//-------------------------------------------------------------------------------------------------------
//					static functions
//-------------------------------------------------------------------------------------------------------

static void ProcessSerCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
//-------------------------------------------------------------------------------------------------------

static int CompareByName(const SER_LINEDATA **opSerUrno1, const SER_LINEDATA **opSerUrno2)
{	
	CString olStr1, olStr2;
	ogBCD.GetField("SER","URNO",(**opSerUrno1).Urno, "SNAM", olStr1);
	ogBCD.GetField("SER","URNO",(**opSerUrno2).Urno, "SNAM", olStr2);

	return (strcmp(olStr1, olStr2));
}



//-------------------------------------------------------------------------------------------------------
//					construction / destruction
//-------------------------------------------------------------------------------------------------------
GhsList::GhsList(CWnd* pParent, int ipSelectMode, CString opCalledFrom, long *lpUrno)
	: CDialog(GhsList::IDD, pParent)
{
	CCS_TRY
	
	pomParent = pParent;
	omSerUrno.Format("%d", lpUrno);
	imSelectMode = LBS_MULTIPLESEL | LBS_EXTENDEDSEL;
	omCalledFrom = opCalledFrom;
	m_nDialogBarHeight = 0;
	pomTable = 0;

	Create();
		
	//{{AFX_DATA_INIT(GhsList)
		// NOTE: the ClassWizard will add member initialization here
	m_Chb_CreateUlnk = FALSE;
	//}}AFX_DATA_INIT

	ogDdx.Register((void *)this,SER_CHANGE,	CString("SERVICE DATA"), CString("Ser-changed"), ProcessSerCf);
	ogDdx.Register((void *)this,SER_NEW,	CString("SERVICE DATA"), CString("Ser-new"), ProcessSerCf);
	ogDdx.Register((void *)this,SER_DELETE,	CString("SERVICE DATA"), CString("Ser-deleted"), ProcessSerCf);

	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------

GhsList::~GhsList()
{
	CCS_TRY

	omLines.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
	delete pomTable;
	
	CCS_CATCH_ALL
}



//-------------------------------------------------------------------------------------------------------
//					creation
//-------------------------------------------------------------------------------------------------------
void GhsList::Create()
{
	CCS_TRY

	CDialog::Create(GhsList::IDD, pomParent);
	ShowWindow(SW_SHOWNORMAL);

	CCS_CATCH_ALL
}



//-------------------------------------------------------------------------------------------------------
//					data exchange, message map
//-------------------------------------------------------------------------------------------------------
void GhsList::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	
	//{{AFX_DATA_MAP(GhsList)
	DDX_Control(pDX, IDC_GHSCBLIST, m_SerCBList);
	DDX_Check(pDX, IDC_CHB_CREATE_ULNK, m_Chb_CreateUlnk);
	//}}AFX_DATA_MAP
}
//-------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(GhsList, CDialog)
	//{{AFX_MSG_MAP(GhsList)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
	ON_CBN_SELCHANGE(IDC_GHSCBLIST, OnSelchangeGhscblist)
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_WM_DESTROY()
	ON_WM_GETMINMAXINFO()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//-------------------------------------------------------------------------------------------------------
//					message handling
//-------------------------------------------------------------------------------------------------------
BOOL GhsList::OnInitDialog() 
{
	CCS_TRY

	CDialog::OnInitDialog();
	
	SetStaticTexts();

	CRect rect;
    GetClientRect(&rect);
	m_nDialogBarHeight = 30;
	bmIsViewOpen = false;
	omSerListViewer.SetViewerKey("SERLIST");
	UpdateComboBox();
	m_SerCBList.SetCurSel ( -1 );
		
	CRect olParentRect;
	GetParent()->GetWindowRect(&olParentRect);
    rect.left = olParentRect.left;
    rect.top = 200;
    rect.right = rect.left + 600;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 350;
    MoveWindow(&rect);
	HICON h_icon = AfxGetApp()->LoadIcon(IDI_UFIS);
	SetIcon(h_icon, FALSE);
	pomTable = new CTableWithGrid;

	
	GetClientRect(&rect);
	rect.OffsetRect(0, m_nDialogBarHeight);
	rect.InflateRect(1, 1);     // hiding the CTable window border
	pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom, ::GetSysColor(COLOR_WINDOWTEXT), ::GetSysColor(COLOR_WINDOW), ::GetSysColor(COLOR_HIGHLIGHTTEXT), ::GetSysColor(COLOR_HIGHLIGHT), &ogMSSansSerif_Regular_8);
	
 	UpdateDisplay();
	pomTable->ResetContent();

	SetBdpsState();	//-- activate/deactivate elements according to state of BDPS-SEC 
	
	CCS_CATCH_ALL

	return TRUE;
}
//-------------------------------------------------------------------------------------------------------

void GhsList::OnLButtonDown(UINT nFlags, CPoint point) 
{      
}
//--------------------------------------------------------------------------------------------------------------------

void GhsList::OnOK() 
{
	CMainFrame		*polMainWnd=0;
	CRulesFormView	*polView=0;
	CStringArray	olSerUrnos;
	if ( pomTable && pomTable->pomGrid )
	{
		int ilMaxItems = pomTable->GetSelCount ( pomTable->pomGrid );
		int ilDataCount;
		if ( ilMaxItems > 0 )
		{
			SendCreateLinkMessage();

			SER_LINEDATA ** polSelDataPtr = new SER_LINEDATA*[ilMaxItems];
			SER_LINEDATA *polActDataPtr;
			ilDataCount = pomTable->GetSelDataPtrs ( pomTable->pomGrid, 
													 (void**)polSelDataPtr, 
													 ilMaxItems ) ;
			//--- prepare drag
			m_DragDropSource.CreateDWordData(DIT_SERLIST, ilDataCount);
			for ( int i=0; i<ilDataCount; i++ ) 
			{
				polActDataPtr = polSelDataPtr[i];
				olSerUrnos.Add(polActDataPtr->Urno);
			}

			delete polSelDataPtr;
		}
		if ( ilDataCount > 0 )
		{
			polMainWnd = (CMainFrame*) AfxGetMainWnd(); 
			if ( polMainWnd )
				polView = (CRulesFormView*)polMainWnd->GetActiveView();
			if ( polView )
				polView ->AddServices ( olSerUrnos );
		}
	}
	bgIsServicesOpen = false;
	DestroyWindow();
}
//-------------------------------------------------------------------------------------------------------

void GhsList::OnCancel() 
{
	bgIsServicesOpen = false;
	DestroyWindow();
}
//-------------------------------------------------------------------------------------------------------

LONG GhsList::OnTableDragBegin(UINT wParam, LONG lParam)
{
	CCS_TRY

	int ilMaxItems = pomTable->GetSelCount ( (void*)lParam );
	int ilDataCount;
	if ( ilMaxItems > 0 )
	{
		SendCreateLinkMessage();
		
		// collect data for drag operation
		SER_LINEDATA ** polSelDataPtr = new SER_LINEDATA*[ilMaxItems];
		SER_LINEDATA *polActDataPtr;
		ilDataCount = pomTable->GetSelDataPtrs ( (void*)lParam, 
												 (void**)polSelDataPtr, 
												 ilMaxItems ) ;
		//--- prepare drag
		m_DragDropSource.CreateDWordData(DIT_SERLIST, ilDataCount);
		for ( int i=0; i<ilDataCount; i++ ) 
		{
			polActDataPtr = polSelDataPtr[i];
			m_DragDropSource.AddDWord(atol(polActDataPtr->Urno));
		}
		m_DragDropSource.BeginDrag();

		delete polSelDataPtr;
	}

	CCS_CATCH_ALL

	return 0L;
}
//-------------------------------------------------------------------------------------------------------

void GhsList::OnView() 
{	
	CCS_TRY

	CString olCaption ;
	olCaption = GetString (IDS_VIEW_SERVICES);
	GhsListViewerPropertySheet olDlg("SERLIST", this, &omSerListViewer, 0, olCaption );
	bmIsViewOpen = true;
	if(olDlg.DoModal() != IDCANCEL)
	{
		UpdateComboBox();
		UpdateDisplay();
	}

	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------

void GhsList::OnSelchangeGhscblist() 
{
	CCS_TRY

	int ilCurSelIndex = m_SerCBList.GetCurSel();
	CString olCurSelText = "";
	if(ilCurSelIndex!=CB_ERR)
	{
		m_SerCBList.GetLBText( ilCurSelIndex, olCurSelText);
		omSerListViewer.ChangeViewTo(olCurSelText);
		UpdateDisplay();
	}

	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------

BOOL GhsList::DestroyWindow() 
{
	return CDialog::DestroyWindow();
}
//-------------------------------------------------------------------------------------------------------

void GhsList::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
	lpMMI->ptMinTrackSize.x =448;	
	lpMMI->ptMinTrackSize.y =180;	
	CDialog::OnGetMinMaxInfo(lpMMI);
}
//-------------------------------------------------------------------------------------------------------

void GhsList::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);
	CRect rect;
	GetClientRect(&rect);
	rect.top += m_nDialogBarHeight;
	rect.InflateRect(1, 1);
	if ( pomTable && pomTable->m_hWnd )
	{
		pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom );
	}
}



//-------------------------------------------------------------------------------------------------------
//					updating functions				
//-------------------------------------------------------------------------------------------------------
void GhsList::UpdateDisplay()
{
	CCS_TRY

	pomTable->DisplayTable();
	CString olHeaderFields = GetString(CODE)+"|";
	olHeaderFields += GetString(NAME)+"|";
	olHeaderFields += GetString(BEZUG)+"|";
	olHeaderFields += GetString(1469)+"|";
	olHeaderFields += GetString(1470)+"|";
	olHeaderFields += GetString(1471)+"|";
	olHeaderFields += GetString(1472)+"|";
	olHeaderFields += GetString(1473);
	pomTable->SetHeaderFields( olHeaderFields );
	
	pomTable->SetFormatList("20|50|22|10|10|10|10|10");
	
	//--- reset data array and table
	omLines.DeleteAll();
	pomTable->ResetContent();
	
	//--- get filter string
	CString olSerUrnos;
	CStringArray olFilter;

	omSerListViewer.GetFilter("SERLIST", olFilter);
	if(olFilter.GetSize() > 1)
	{
		olSerUrnos = olFilter[1];
	}
	else
	{	//  <Default> - Filter
		for (int i = 0; i < ogBCD.GetDataCount("SER"); i++)
		{
			olSerUrnos +=  ogBCD.GetField ( "SER", i, "URNO") ;
			olSerUrnos +=  " ";
		}
	}
	
	//--- copy parts of filter string into string array
	CStringArray olTmpStr;
	int ilCount = ExtractItemList(olSerUrnos, &olTmpStr, ' ');

	//--- rebuild data array
	CCSPtrArray <SER_LINEDATA> olSerData;
	CString olUrnoStr;
	for(int i = 0; i < ilCount; i++)
	{
		// fill struct
		SER_LINEDATA olSer;
		olUrnoStr = olTmpStr[i];
		olUrnoStr.Remove ( '|');
		olSer.LineNo = i;
		olSer.Urno = ogBCD.GetField("SER", "URNO", olUrnoStr, "URNO");
		olSer.Snam = ogBCD.GetField("SER", "URNO", olUrnoStr, "SNAM");

		// append struct to array
		if (olSer.Urno != "")
		{
			olSerData.NewAt(0, olSer);
		}
	}
	
	//--- sort array
	olSerData.Sort(CompareByName);
	for(i = 0; i < olSerData.GetSize(); i++)
	{
		SER_LINEDATA rlSer = olSerData[i];
		CreateLine(&rlSer);
		pomTable->AddTextLine(Format(omLines[i].Urno), &omLines[i]);
	}
	olSerData.DeleteAll();

	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------

void GhsList::UpdateComboBox()
{
	CCS_TRY

	CStringArray olViewArray;
	int ilIndex = 0;
	int ilComboIndex = 0;
	
	omSerListViewer.GetViews(olViewArray);
	m_SerCBList.ResetContent();
	for (ilIndex = 0; ilIndex < olViewArray.GetSize(); ilIndex++)
	{	
		//-- Add views to combobox
		ilComboIndex = m_SerCBList.AddString(olViewArray[ilIndex]);
	
		//-- Select the view last added
		if (olViewArray[ilIndex] == omSerListViewer.SelectView())
		{
			m_SerCBList.SetCurSel(ilComboIndex);
		}
	}

	CCS_CATCH_ALL	
}



//-------------------------------------------------------------------------------------------------------
//					all the rest					
//-------------------------------------------------------------------------------------------------------
CString GhsList::Format(CString opSerUrno)
{
	CString s;

	CCS_TRY

	CString olSeco;
	CString olSnam;
	CString olSeer;
	CString olFfis;
	CString olBezug;
	CString olVal;
	CString olSdut;
	CString olSsut;
	CString olSsdt;
	CString olFwtt;
	CString olFwtf;
	RecordSet olRecord;

	if ( !ogBCD.GetRecord( "SER", "URNO", opSerUrno, olRecord ) )
		return "";
	olVal = olRecord.Values[igSerSecoIdx];
	s = CString(olVal) + "|";

	olVal = olRecord.Values[igSerSnamIdx];
	s += CString(olVal) + "|";

	olSeer = olRecord.Values[igSerSeerIdx];
	olFfis = olRecord.Values[igSerFfisIdx];
	
	if ( olFfis == "1" )
		olBezug = GetString(IDS_FLIGHTINDIPENDENT);
	else
	{
		if ( olSeer == "0" )
			olBezug = GetString(IDS_TURNAROUND) ;
		if ( olSeer == "1" )
			olBezug = GetString(IDS_INBOUND) ;
		if ( olSeer == "2" )
			olBezug = GetString(IDS_OUTBOUND) ;

	}
	s += olBezug + "|";
	if ( olRecord.Values[igSerFdutIdx] == "1" ) 
		olSdut = olRecord.Values[igSerSdutIdx];
	s += olSdut + "|";

	if ( olRecord.Values[igSerFsutIdx] == "1" )
	   olSsut = olRecord.Values[igSerSsutIdx];
	s += olSsut + "|";

	if ( olRecord.Values[igSerFsdtIdx] == "1" )
	   olSsdt = olRecord.Values[igSerSsdtIdx];
	s += olSsdt + "|";

	if ( olRecord.Values[igSerFwttIdx] == "1" ) 
	   olFwtt = olRecord.Values[igSerSwttIdx];
	s += olFwtt + "|";

	if ( olRecord.Values[igSerFwtfIdx] == "1" )
	   olFwtf = olRecord.Values[igSerSwtfIdx]; 
	s += olFwtf ;

	olRecord.Values.RemoveAll();
	CCS_CATCH_ALL	

	return s;
}
//-------------------------------------------------------------------------------------------------------

void GhsList::CreateLine(SER_LINEDATA *prpSer)
{
	SER_LINEDATA rlSer;
	rlSer = *prpSer;
	omLines.NewAt(omLines.GetSize(), rlSer);
}
//-------------------------------------------------------------------------------------------------------

// Set activation state acording to BDPS-SEC
void GhsList::SetBdpsState()
{
	CCS_TRY

	CCS_CATCH_ALL	
}
//-------------------------------------------------------------------------------------------------------

// This function sets the static texts for the controls from TXTTAB
void GhsList::SetStaticTexts()
{
	SetWindowLangText(this, SERVICES);
	SetDlgItemLangText(this, IDC_VIEW, IDS_VIEW);
	SetDlgItemLangText(this, IDOK, OK);
	SetDlgItemLangText(this, IDCANCEL, 1375);
	SetDlgItemLangText(this,IDC_CHB_CREATE_ULNK, 1686); 
}
//-------------------------------------------------------------------------------------------------------

void GhsList::PostNcDestroy() 
{
	delete this;
	CDialog::PostNcDestroy();
}
//-------------------------------------------------------------------------------------------------------

void GhsList::SendCreateLinkMessage()
{
	// tell other parts of the program if SERs should be linked to a group
	// (i.e. filling the RUD.ULNK field with a new urno)
	CMainFrame* polMainWnd = (CMainFrame*) AfxGetMainWnd(); 
	CRulesFormView *polView =0;

	if ( polMainWnd )
		polView = (CRulesFormView*)polMainWnd->GetActiveView();
	if ( polView )
	{
		UINT msgID = WM_SERLIST_NOLINK;
	
		CButton *pBtn = (CButton*)GetDlgItem(IDC_CHB_CREATE_ULNK);
		if (pBtn != NULL)
		{
			if (pBtn->GetCheck() == CHECKED)
				msgID = WM_SERLIST_LINK;
		}
		polView->SendMessage(msgID);
	}
}


//-------------------------------------------------------------------------------------------------------
//					Processing functions
//-------------------------------------------------------------------------------------------------------
static void ProcessSerCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    GhsList *polDlg = (GhsList *)vpInstance;
	polDlg->UpdateDisplay();
}
//-------------------------------------------------------------------------------------------------------



