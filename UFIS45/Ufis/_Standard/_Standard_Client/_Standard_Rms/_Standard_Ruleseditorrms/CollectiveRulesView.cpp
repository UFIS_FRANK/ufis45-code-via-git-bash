// CollectiveRulesView.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <ccsglobl.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <Dialog_VorlagenEditor.h>
#include <Ghslist.h>
#include <dialoggrid.h>
#include <dutygrid.h>
// #include "indepviewer.h"
#include <RegelWerkView.h>
#include <CollectiveRulesView.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif

#define IDC_LEFT_EDIT	   47110813

//----------------------------------------------------------------------------
//		globals, defines and declaration of static or global functions
//----------------------------------------------------------------------------

//--- Local function prototype
static void IndepDDXCallback(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);




//----------------------------------------------------------------------------
//						creation, construction, destruction
//----------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CollectiveRulesView, CRulesFormView)


CollectiveRulesView::CollectiveRulesView()
	: CRulesFormView(CollectiveRulesView::IDD)
{
	CCS_TRY
	
	imRessourceType = 0;

	pomLeftGrid = new CGridFenster(this);
	pomRightGrid = new CGridFenster(this);
	pomDutyGrid = new CColDutyGrid(this);
	pomViewer = NULL;	
	
	omCurrDgrUrno = "";
	
	//{{AFX_DATA_INIT(CollectiveRulesView)
	m_RuleName = _T("");
	m_RuleShortName = _T("");
	//}}AFX_DATA_INIT


	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

CollectiveRulesView::~CollectiveRulesView()
{
	CCS_TRY
	

	delete pomLeftGrid;
	delete pomRightGrid;
	delete pomDutyGrid;


	CCS_CATCH_ALL
}



//----------------------------------------------------------------------------
//						data exchange, message map
//----------------------------------------------------------------------------
void CollectiveRulesView::DoDataExchange(CDataExchange* pDX)
{
	CRulesFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CollectiveRulesView)
	DDX_Control(pDX, IDC_LATEST, omEditLito);
	DDX_Control(pDX, IDC_EARLIEST, omEditLifr);
	DDX_Control(pDX, IDC_MINCOUNT, omEditMinCount);
	DDX_Control(pDX, IDC_MAXCOUNT, omEditMaxCount);
	DDX_Control(pDX, IDC_FOND_CHK, omFondChk);
	DDX_Control(pDX, IDC_FADD_CHK, omNoDemandsChk);
	DDX_Control(pDX, IDC_MAXTIME, omEditSplitMax);
	DDX_Control(pDX, IDC_MINTIME, omEditSplitMin);
	DDX_Control(pDX, IDC_DIDE_CHK, omDideChk);
	DDX_Text(pDX, IDC_EDT_RUNA, m_RuleName);
	DDX_Text(pDX, IDC_EDT_RUSN, m_RuleShortName);
	DDX_Control(pDX, IDC_COB_ALOC, m_AlocCB);
	DDX_Control(pDX, IDC_COB_REF, m_RefCB);
	DDX_Control(pDX, IDC_DEACTIVATED, m_Chb_Deactivated);
	DDX_Control(pDX, IDC_EDT_RUSN, m_Edt_RuleShortName);
	DDX_Control(pDX, IDC_EDT_RUNA, m_Edt_RuleName);


	DDX_Control(pDX, IDC_EARLIEST2, start);
	DDX_Control(pDX, IDC_EARLIEST3, endt);

		DDX_Control(pDX, IDC_CHECK1, cflag);
	//}}AFX_DATA_MAP
}
//----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(CollectiveRulesView, CRulesFormView)
	//{{AFX_MSG_MAP(CollectiveRulesView)
	ON_MESSAGE(GRID_MESSAGE_ENDEDITING, OnEndEditing)
	ON_COMMAND(CM_COLLECT_MENU_DELETEBAR, OnMenuDelete)
	ON_COMMAND(CM_COLLECT_MENU_CREATELNK, OnMenuCreateLink)
	ON_COMMAND(CM_COLLECT_MENU_DELETELNK, OnMenuDeleteLink)
	ON_COMMAND(CM_COLLECT_MENU_COPYBAR, OnMenuCopyBar)
	ON_CBN_SELENDOK(IDC_COB_ALOC, OnSelendokCobAlod)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_DIDE_CHK, OnDideChk)
	ON_CBN_SELENDOK(IDC_COB_REF, OnSelendokCobRef)
	ON_MESSAGE(WM_DROP, OnDrop)
	ON_MESSAGE(WM_DRAGOVER, OnDragOver)
	ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnRButtonDown)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,OnLButtonDblClk)
	ON_COMMAND(ID_FILE_SAVE, OnSaveRule)
	ON_BN_CLICKED(IDC_FADD_CHK, OnFaddChk)
	ON_BN_CLICKED(IDC_FOND_CHK, OnFondChk)


	ON_BN_CLICKED(IDC_CHECK1, OnCheckChange)
	//}}AFX_MSG_MAP
	ON_MESSAGE(GRID_MESSAGE_CURRENTCELLCHANGED, OnGridCurrCellChanged)
	ON_MESSAGE(GRID_MESSAGE_BUTTONCLICK, OnGridButton)
	ON_MESSAGE(GRID_MESSAGE_RBUTTONCLICK, OnGridRButton)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillFocus)
END_MESSAGE_MAP()


//----------------------------------------------------------------------------
//						message handling
//----------------------------------------------------------------------------
void CollectiveRulesView::OnInitialUpdate() 
{
	CCS_TRY


	CRulesFormView::OnInitialUpdate();
	SetStaticTexts();

	imActiveRow = -1;  


	if (!ccicheck)
	{
	        start.ShowWindow(SW_HIDE);
            cflag.ShowWindow(SW_HIDE);
	        endt.ShowWindow(SW_HIDE);


			GetDlgItem(IDC_STA_EARLIEST2)-> ShowWindow(SW_HIDE); 
	        GetDlgItem(IDC_STA_EARLIEST3)-> ShowWindow(SW_HIDE); 
	
	}

	//---  set the pointers to the mainframe wnd items
    pomFrameWnd = static_cast< CMainFrame* >(AfxGetMainWnd()); 
	pomComboBox = pomFrameWnd->m_wndToolBar.pomComboBox;
	pomStatusBar = &pomFrameWnd->m_wndStatusBar;

	//--- set control attributes
	m_Edt_RuleName.SetBKColor(YELLOW);
	m_Edt_RuleShortName.SetBKColor(YELLOW);

	m_Edt_RuleName.SetTextLimit(1,64,false);
	m_Edt_RuleShortName.SetTextLimit(1,14,false);
	
	omEditLito.SetTypeToTime();
	omEditLifr.SetTypeToTime();

	m_DragDropTarget.RegisterTarget(this, this);
	
	// rule demands
	ogDdx.Register(this, RUD_TMP_NEW, CString("FlightIndepView"), CString("RUD_TMP New"), IndepDDXCallback);
	ogDdx.Register(this, RUD_TMP_CHANGE, CString("FlightIndepView"), CString("RUD_TMP Changed"), IndepDDXCallback);
	ogDdx.Register(this, RUD_TMP_DELETE, CString("FlightIndepView"), CString("RUD_TMP Deleted"), IndepDDXCallback);
	// static groups (heads)
	ogDdx.Register(this, SGR_NEW, CString("FlightIndepView"), CString("SGR New"), IndepDDXCallback);
	ogDdx.Register(this, SGR_CHANGE, CString("FlightIndepView"), CString("SGR Change"), IndepDDXCallback);
	ogDdx.Register(this, SGR_DELETE, CString("FlightIndepView"), CString("SGR Delete"), IndepDDXCallback);
	// static group members)
	/*ogDdx.Register(this, SGM_NEW, CString("CollectiveRulesView"), CString("SGM New"), IndepDDXCallback);
	ogDdx.Register(this, SGM_CHANGE, CString("CollectiveRulesView"), CString("SGM Change"), IndepDDXCallback);
	ogDdx.Register(this, SGM_DELETE, CString("CollectiveRulesView"), CString("SGM Delete"), IndepDDXCallback);*/
	
	TablesFirstTimeInit();
	
	EnableDemandDetailControls(false);
	DemandDetailDlg::IniAlocCB(&m_AlocCB);
	// IniRefCB(); erst Aufrufen, wenn RUD ausgew�hlt. 
	

	OnNewRule();
	m_HScroll.ShowWindow(SW_HIDE); // this control is just for compatibility with the base class

	m_RefCB.EnableWindow(FALSE);	

	CCS_CATCH_ALL
}

//----------------------------------------------------------------------------

void CollectiveRulesView::OnEndEditing(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

	CGridFenster*polGrid = (CGridFenster*) wParam;
	CELLPOS		*prlCellPos = (CELLPOS*) lParam;
	
	ROWCOL		ilCol = (ROWCOL) prlCellPos->Col;
	ROWCOL		ilRow = (ROWCOL) prlCellPos->Row;
	RecordSet	olRecord(ogBCD.GetFieldCount("RUD"));
	CString		olReft, olText, olField;
	CString		olCellText ;
		


//---	Left Grid		  
	CString olCode, olName, olUrno, olUdgr, olUrud;
	bool    blStaticGroup;
	if ( (polGrid == pomLeftGrid) && ( ilRow > 0 ) )
	{
		if (imRessourceType == 0)	// functions 
		{
			if (ilCol == 1)
			{
				CString olPfcUrno;

				olCode = pomLeftGrid->GetValueRowCol(ilRow, 1);
				if ( olCode.IsEmpty () )	// L�schen von Funktionen hier nicht erlaubt
				{							// statt dessen au�erhalb Balken l�schen
					pomLeftGrid->Undo ();
					return;					
				}

				olUrno = pomLeftGrid->GetValueRowCol(ilRow, 3);
				blStaticGroup = !IsSingleResourceSelected ( /*ilRow, ilCol, 
															pomLeftGrid,
												           "PFC",*/ olCode );
				if ( blStaticGroup )
					ogBCD.GetFields("SGR", "GRPN", olCode, "GRDS", "URNO", olName, olPfcUrno);
				else
					ogBCD.GetFields("PFC", "FCTC", olCode, "FCTN", "URNO", olName, olPfcUrno);

				olUdgr = omCurrDgrUrno;
				if ( !olUrno.IsEmpty() )	// change RPF_TMP entry (line contained an entry before)
				{
					ogDataSet.AddOrChangeRessources("RPF", olUrno, omCurrRudUrno, olUdgr, 
													olPfcUrno, olCode, true, blStaticGroup );
				}
				else
				{
					//  Neuen RUD_TMP-Record einf�gen
					CString olTplUrno, olSerUrno;
					RecordSet olSerRecord;
					RecordSet olRudRec(ogBCD.GetFieldCount("RUD_TMP") );

					olSerUrno = ogBCD.GetField("RUD_TMP", "UDGR", olUdgr, "UGHS" );
					if ( !olSerUrno.IsEmpty () &&
						 ogBCD.GetRecord ( "SER", "URNO", olSerUrno, olSerRecord ) )
					{
						olRudRec.Values[igRudUdgrIdx] = olUdgr;
						olRudRec.Values[igRudUrnoIdx].Format("%d", ogBCD.GetNextUrno());
						olRudRec.Values[igRudUrueIdx] = omCurrRueUrno;			// rule urno
						olRudRec.Values[igRudRetyIdx] = CString("100");			// ressource type set to function
						if ( pomComboBox && 
							 GetSelectedTplUrno ( *pomComboBox, olTplUrno, FALSE, true ) )
						{		
							olRudRec.Values[igRudAlocIdx] = ogBCD.GetField ( "TPL", "URNO", olTplUrno, "DALO" );
							if ( igRudUtplIdx >= 0 )
								olRudRec.Values[igRudUtplIdx] = olTplUrno;
						}
						ogDataSet.CreateRudRecord ( olRudRec, olSerRecord, false );
						if (ogBCD.InsertRecord("RUD_TMP", olRudRec) == false)
						{
							ogLog.Trace("DEBUG", "[DataSet::CreateRudFromSerList]  InsertRecord into Logical table RUD_TMP failed");
							pomLeftGrid->SetValueRange(CGXRange(ilRow, 1) , "" );
							olName.Empty();
						}
						else
						{
							//omCurrRudUrno = olRudRec.Values[igRudUrnoIdx];
							SetCurrentRudUrno ( olRudRec.Values[igRudUrnoIdx] );
							olUrno.Format("%d", ogBCD.GetNextUrno());
							pomLeftGrid->SetValueRange(CGXRange(ilRow, 3), olUrno);
							ogDataSet.AddOrChangeRessources("RPF", olUrno, omCurrRudUrno, olUdgr,
															olPfcUrno, olCode, false, blStaticGroup);
							pomDutyGrid->OnNewRudTmp ( &olRudRec );
							DisplayQualisForRud ( omCurrRudUrno );
						}
					}
					else
					{
						ogLog.Trace("DEBUG", "Unable to load Service for Demandgroup %s", olUdgr );
						pomLeftGrid->SetValueRange(CGXRange(ilRow, 1) , "" );
						olName.Empty();
					}
						
				}
				//  Fehler beseitigt:  (hag000126)
				//  Aufgrund von Broadcasts wird aktivier Balken deselektiert 
				//	und dadurch das Grid geleert
				if ( ilRow <= pomLeftGrid->GetRowCount () )
					pomLeftGrid->SetValueRange(CGXRange(ilRow, 2), olName);
			}
		}
		
		if (imRessourceType == 1)	// equipment
		{
			// Left grid isn't enabled for equipment
		}
		
		if (imRessourceType == 2)
		{	
			// Left grid isn't enabled for locations
		}
	}


//---	Right Grid
	if( (polGrid == pomRightGrid) && ( ilRow > 0 ) && (ilCol == 1) )
	{
		if (imRessourceType == 0)	// qualifications
		{
			// set corresponding name in right column
			CString olPerUrno;
			olCode = pomRightGrid->GetValueRowCol(ilRow, 1);
			olUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
			if ( olCode.IsEmpty () )	// hag990924
			{
				if ( !olUrno.IsEmpty () && 
					 ogDataSet.DeleteRessource ( "RPQ", olUrno ) )
					pomRightGrid->RemoveRows ( ilRow, ilRow );
				return;	
			}
			blStaticGroup = !IsSingleResourceSelected ( /*ilRow, ilCol, 
														pomRightGrid,
												       "PER",*/ olCode );
			if ( blStaticGroup )
				ogBCD.GetFields("SGR", "GRPN", olCode, "GRDS", "URNO", olName, olPerUrno);
			else
				ogBCD.GetFields("PER", "PRMC", olCode, "PRMN", "URNO", olName, olPerUrno);

			// change RPQ_TMP entry (line contained an entry before)
			if (olUrno.IsEmpty() == FALSE)
			{	//  �nderung, Status Einzel-/ Gruppenqualifikation beibehalten
				olUdgr = ogBCD.GetField ( "RPQ_TMP", "URNO", olUrno, "UDGR" );
				if ( olUdgr.IsEmpty () )
				{
					olUrud = omCurrRudUrno;
					if ( ogDataSet.IsSingleQualiUsedForRud ( olUrno, olUrud,
															 olPerUrno ) )
					{
						MessageBox ( GetString(RES_ALREADY_SEL) );
						//  vorherigen Code wieder setzen
						ogBCD.GetRecord (  "RPQ_TMP", "URNO", olUrno, olRecord );
						if ( GetGroupName ( &olRecord, igRpqGtabIdx, 
											igRpqUperIdx, olCode ) )
							olCode = "*"+ olCode;
						else
							olCode = olRecord.Values[igRpqQucoIdx];
						pomRightGrid->SetValueRange(CGXRange(ilRow, ilCol),
													olCode);
						return;
					}
				}
				else
					olUrud.Empty ();
				ogDataSet.AddOrChangeRessources("RPQ", olUrno, olUrud, olUdgr,
												olPerUrno, olCode, true, blStaticGroup);
			}
			// add RPQ_TMP entry (line contained no entry before)
			else
			{	//  neue Qualifikationen sind immer Einzelqualifikationen !
				CString olNewUrno;
				if ( ogDataSet.IsSingleQualiUsedForRud ( olNewUrno, omCurrRudUrno, 
														 olPerUrno ) )
				{
					MessageBox ( GetString(RES_ALREADY_SEL) );
					pomRightGrid->SetValueRange (CGXRange(ilRow,ilCol), "");
					pomRightGrid->DeleteEmptyRow ( ilRow,ilCol );
					return;
				}
				olNewUrno.Format("%d", ogBCD.GetNextUrno());
				pomRightGrid->SetValueRange(CGXRange(ilRow, 3), olNewUrno);
				ogDataSet.AddOrChangeRessources("RPQ", olNewUrno, omCurrRudUrno, "",
												olPerUrno, olCode, false, blStaticGroup);
			}
			//  Fehler beseitigt:  (hag000126)
			//  Aufgrund von Broadcasts wird aktivier Balken deselektiert 
			//	und dadurch das Grid geleert
			if ( ilRow <= pomRightGrid->GetRowCount () )
				pomRightGrid->SetValueRange(CGXRange(ilRow, 2), olName);
		}
		
		if (imRessourceType == 1)	// equipment
		{
			CString olEquUrno;

			olCode = pomRightGrid->GetValueRowCol(ilRow, 1);
			if ( olCode.IsEmpty () )	// L�schen von Funktionen hier nicht erlaubt
			{							// statt dessen au�erhalb Balken bzw. in Dutygrid l�schen
				pomRightGrid->Undo ();
				return;					
			}
			olUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
			blStaticGroup = !IsSingleResourceSelected ( /*ilRow, ilCol, pomRightGrid,
												       "EQU",*/ olCode );
			if ( blStaticGroup )
				ogBCD.GetFields("SGR", "GRPN", olCode, "GRDS", "URNO", olName, olEquUrno);
			else
				ogBCD.GetFields("EQU", "GCDE", olCode, "ENAM", "URNO", olName, olEquUrno);
			pomRightGrid->SetValueRange(CGXRange(ilRow, 2), olName);
			ogDataSet.AddOrChangeRessources("REQ", olUrno, omCurrRudUrno, olUdgr, 
											olEquUrno, olCode, true, blStaticGroup);
			
		}

		if (imRessourceType == 2)
		{
			CString olUrnoToSave, olReft;
			CString olTab, olFldn, olNewValue;
			olNewValue = pomRightGrid->GetValueRowCol(ilRow, 1);
			if ( olNewValue.IsEmpty () )	// L�schen von Locations hier nicht erlaubt
			{								// statt dessen au�erhalb Balken l�schen
				pomRightGrid->Undo ();
				return;					
			}
			olUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
			olReft = ogBCD.GetField("RLO_TMP", "URNO", olUrno, "REFT");

			if (olReft.GetLength() == 8)
			{
				olTab = olReft.Left(3);
				olFldn = olReft.Right(4);
			}
			blStaticGroup = !IsSingleResourceSelected ( /*ilRow, ilCol, 
														pomRightGrid,
														olTab,*/ olNewValue );
			if ( blStaticGroup )
				olUrnoToSave = ogBCD.GetField("SGR", "GRPN", olNewValue, "URNO");
			else
				olUrnoToSave = ogBCD.GetField(olTab, olFldn, olNewValue, "URNO");

			ogDataSet.AddOrChangeRessources("RLO", olUrno, omCurrRudUrno, olUdgr, 
											olReft, olUrnoToSave, true, blStaticGroup); 
		}
	}


	CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------------

void CollectiveRulesView::OnLoadRule(CString opRueUrno, CString opTplName, bool bpNoComboChange)
{
	CCS_TRY
	
	OnNewRule();		//  alles Zur�cksetzen

	bmNewFlag = false;
	omCurrRueUrno = opRueUrno;
	ogDataSet.FillLogicRudObject(omCurrRueUrno);
	ogDataSet.FillLogicRpfObject();
	ogDataSet.FillLogicRpqObject();
	ogDataSet.FillLogicRloObject();
	ogDataSet.FillLogicReqObject();

	//--- get record
	RecordSet olRecord(ogBCD.GetFieldCount("RUE"));
	ogBCD.GetRecord("RUE", "URNO", omCurrRueUrno, olRecord);

   //--- set names and description
	CString olName;
	CString olShortName;

	olName = olRecord.Values[igRueRunaIdx];
	olShortName = olRecord.Values[igRueRusnIdx];
	m_Edt_RuleName.SetWindowText(olName);
	m_Edt_RuleShortName.SetWindowText(olShortName);
	// m_Edt_Descript.SetWindowText(olDescription);
	
	//--- set CheckBox "Deactivated"
	CString olRust;
	olRust = olRecord.Values[igRueRustIdx];
	if (olRust == CString("0"))
		m_Chb_Deactivated.SetCheck(CHECKED);	// deactivated
	else
		m_Chb_Deactivated.SetCheck(UNCHECKED);	// activated

	UpdateServiceTable();
	EnableDemandDetailControls(true);

	//FSC changes start 18.06.01
	omDideChk.EnableWindow(false);
	omDideChk.SetCheck(UNCHECKED);
	omEditSplitMin.SetWindowText("");	
	omEditSplitMax.SetWindowText("");	
	omEditSplitMin.EnableWindow(FALSE);
	omEditSplitMax.EnableWindow(FALSE);
	//FSC changes end 18.06.01

	//--- initialize tables


	pomDutyGrid->DisplayRudTmp ();
	
	ogChangeInfo.SetCurrRueUrno(omCurrRueUrno);
	
	RudTmp_Dump();

	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

void CollectiveRulesView::OnNewRule()
{
	CCS_TRY


	// reset data
	ogDataSet.FillLogicRudObject(CString("-1"));
	ogDataSet.ResetLogicResourceTables ();


	//--- reset controls
	m_Edt_RuleName.SetWindowText("");
	m_Edt_RuleShortName.SetWindowText("");
	// m_Edt_Descript.SetWindowText("");
	
	m_Chb_Deactivated.SetCheck(CHECKED);

	// pomLeftGrid->ResetContent();
	// pomRightGrid->ResetContent();

	long llUrno = ogBCD.GetNextUrno();
	bmNewFlag = true;
	omCurrRueUrno.Format("%d", llUrno);

	pomDutyGrid->ResetContent();
	ClearRessourceTables();
	SetCurrentRudUrno("");
	imActiveRow = -1;  

	omCurrRudUrno = omCurrDgrUrno = "";

	ogChangeInfo.SetCurrRueUrno(omCurrRueUrno);
	bgIsValSaved = false;
	
	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

void CollectiveRulesView::OnSaveRule()
{
	CCS_TRY
	
	SetFocus();
	//-- make wait cursor 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	//-- get data from window
	UpdateData();


//--- break conditions
	// Are name and short name given correctly ?
	if (m_Edt_RuleName.GetStatus() == false)
	{
		MessageBox(GetString(1411), GetString(1412), MB_OK | MB_ICONEXCLAMATION);
		return;
	}
	if (m_Edt_RuleShortName.GetStatus() == false)
	{
		MessageBox(GetString(1457), GetString(1412), MB_OK | MB_ICONEXCLAMATION);
		return;
	}

	CString olName;
	CString olShortName;
	
	m_Edt_RuleName.GetWindowText(olName);
	m_Edt_RuleShortName.GetWindowText(olShortName);

	int ilCount = ogBCD.GetDataCount("RUE");
	for (int i = 0; i < ilCount; i++)
	{
		CString olShortNameInDB = ogBCD.GetField("RUE", i, "RUSN");
		CString olStatusInDB = ogBCD.GetField("RUE", i, "RUST");

		//  archived rules may have the name of the new rule
		if( (olShortNameInDB == olShortName) && bmNewFlag && (olStatusInDB!="2"))
		{
			VALDATA rlOldValidity;
			CString olOldUrno = ogBCD.GetField("RUE", i, "URNO");
			if (!::LoadValidity(olOldUrno, rlOldValidity) ||
				 DoValiditiesOverlap(rlOldValidity, pomFrameWnd->rmValData))
			{
				AfxMessageBox(GetString(164));
				return;
			}
		}
	}


//--- create recordset
	// Urno der aktuell in FormView->ComboBox ausgew�hlten Vorlage
	CString olTplUrno;
	if (!GetSelectedTplUrno(*pomComboBox, olTplUrno, FALSE, true))
		return;


//--- Fill Record
	int ilSize = ogBCD.GetFieldCount("RUE");
	RecordSet olRecord(ilSize);

   if (bmNewFlag == false)
	   ogBCD.GetRecord ("RUE","URNO",omCurrRueUrno,olRecord);

	olRecord.Values[igRueUrnoIdx] = omCurrRueUrno;	   
	olRecord.Values[igRueApplIdx] = CString("RULE_AFT");	   
	olRecord.Values[igRueRunaIdx] = olName;					// name
	olRecord.Values[igRueRusnIdx] = olShortName;			// short name
	olRecord.Values[igRueUtplIdx] = olTplUrno;				// template urno 
	olRecord.Values[igRueRutyIdx] = "1";					// collective rule
	olRecord.Values[igRueLstuIdx] = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");
	olRecord.Values[igRueUseuIdx] = pcgUser;
	
	// fill fields not needed with blanks
	olRecord.Values[igRueEvtaIdx] = CString(" ");		
	olRecord.Values[igRueEvtdIdx] = CString(" ");		
	olRecord.Values[igRueEvtyIdx] = CString(" ");		
	olRecord.Values[igRueExclIdx] = CString(" ");		
	olRecord.Values[igRueFsplIdx] = CString(" ");		
	olRecord.Values[igRueMaxtIdx] = CString(" ");		
	olRecord.Values[igRueMistIdx] = CString(" ");		

	olRecord.Values[igRueEvttIdx] = " ";		
	olRecord.Values[igRueEvrmIdx] = " ";
	olRecord.Values[igRuePrioIdx] = " ";


	// FISU (flag IsUsed)
	/*
	bool blIsUsed = false;
	CString olFisu;
	olFisu = ogBCD.GetField("RUE", "URNO", omCurrRueUrno, "FISU");
	if (olFisu == CString("1"))
	{
			blIsUsed = true;
	}
	else
	{
		   olRecord.Values[igRueFisuIdx] = CString("0");
		   blIsUsed = false;
	}
	*/	
	//  UARC
	olRecord.Values[igRueUarcIdx] = ogBCD.GetField("RUE", "URNO", omCurrRueUrno, "UARC");	//hag990901

	// RUST (rule status)
	if (m_Chb_Deactivated.GetCheck() == CHECKED)
	{
		olRecord.Values[igRueRustIdx] = CString("0"); // deactivated
	}
	else
	{
		olRecord.Values[igRueRustIdx] = CString("1"); // activated
	}


//--- write data to database
	bool blOk = SaveRule( olRecord );
	// make regular cursor 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

/*	CopyDemandRequirements isn't used anywhere
bool CollectiveRulesView::CopyDemandRequirements()
{
	bool blRet = false;

	CCS_TRY

	CStringArray olRudUrnos;
	CString olNewUrno;
	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD_TMP"));
	int ilRudCount = ogBCD.GetDataCount("RUD_TMP");
	
	for (int i = 0; i < ilRudCount; i++)
	{
		ogBCD.GetRecord("RUD_TMP", i, olRudRecord);
		olNewUrno.Format("%d", ogBCD.GetNextUrno());
		olRudRecord.Values[igRudUrnoIdx] = olNewUrno;
		olRudUrnos.Add(olNewUrno);
		olRudRecord.Values[igRudUrueIdx] = omCurrRueUrno;	//hag990901
		ogBCD.InsertRecord("RUD", olRudRecord, false);
	}

	blRet = ogBCD.Save("RUD");

	CopyRessources(olRudUrnos);
	

	CCS_CATCH_ALL

	return blRet;
}
*/

//-----------------------------------------------------------------------------------------

/*	only used in CopyDemandRequirements which isn't used anywhere
bool CollectiveRulesView::CopyRessources(CStringArray &opRudUrnos)
{
	bool blRet = true;
	
	CCS_TRY

	
	CString olRud;
	CString olUrno, olUdgr;
		
	CCSPtrArray <RecordSet> olRpfArr;
	CCSPtrArray <RecordSet> olRpqArr;
	CCSPtrArray <RecordSet> olReqArr;
	CCSPtrArray <RecordSet> olRloArr;
	CCSPtrArray <RecordSet> olRudArr;
	CStringList				olUdgrList;
	POSITION				pos;
	int j ;

	int ilSize = opRudUrnos.GetSize();
	RecordSet olRpqRecord(ogBCD.GetFieldCount("RPQ_TMP"));
	
	for (int i = 0; i < ilSize; i++)
	{
		olRud = opRudUrnos.GetAt(i);
	
		// functions
		RecordSet olRpfRecord(ogBCD.GetFieldCount("RPF_TMP"));
		ogBCD.GetRecords("RPF", "URUD", olRud, &olRpfArr);
		int ilCount = olRpfArr.GetSize();
		for ( j = ilCount - 1; j >= 0; j--)
		{
			olRpfRecord = olRpfArr[j];
			olUrno.Format("%d", ogBCD.GetNextUrno());
			olRpfRecord.Values[igRpfUrnoIdx] = olUrno;
			olRpfRecord.Values[igRpfUrudIdx] = olRud;
			ogBCD.InsertRecord("RPF", olRpfRecord);
		}

		// qualifications
		ogBCD.GetRecords("RPQ", "URUD", olRud, &olRpqArr);
		ilCount = olRpqArr.GetSize();
		//for (j = ilCount - 1; j >= 0; j--)
		for (j = 0; j < ilCount; j++)	//hag990923
		{
			olRpqRecord = olRpqArr[j];
			olUrno.Format("%d", ogBCD.GetNextUrno());
			olRpqRecord.Values[igRpqUrnoIdx] = olUrno;
			olRpqRecord.Values[igRpqUrudIdx] = olRud;
			olRpqRecord.Values[igRpqUdgrIdx] = "";
			ogBCD.InsertRecord("RPQ", olRpqRecord);
		}
		//  Liste mit UDGR's aufbauen f�r Suche von nicht zugeordneten Qualis
		if ( ogBCD.GetField ( "RUD_TMP", "URNO", olRud, " UDGR", olUdgr ) &&
			 !olUdgr.IsEmpty() && !olUdgrList.Find(olUdgr) )
			olUdgrList.AddTail(olUdgr);

		// equipment
		RecordSet olReqRecord(ogBCD.GetFieldCount("REQ_TMP"));
		ogBCD.GetRecords("REQ", "URUD", olRud, &olReqArr);
		ilCount = olReqArr.GetSize();
		for (j = ilCount - 1; j >= 0; j--)
		{
			olReqRecord = olReqArr[j];
			olUrno.Format("%d", ogBCD.GetNextUrno());
			olReqRecord.Values[igReqUrnoIdx] = olUrno;
			olReqRecord.Values[igReqUrudIdx] = olRud;
			ogBCD.InsertRecord("REQ", olReqRecord);
		}
		
		// locations
		RecordSet olRloRecord(ogBCD.GetFieldCount("RLO_TMP"));
		ogBCD.GetRecords("RLO", "URUD", olRud, &olRloArr);
		ilCount = olRloArr.GetSize();
		for (j = ilCount - 1; j >= 0; j--)
		{
			olRloRecord = olRloArr[j];
			olUrno.Format("%d", ogBCD.GetNextUrno());
			olRloRecord.Values[igRloUrnoIdx] = olUrno;
			olRloRecord.Values[igRloUrudIdx] = olRud;
			ogBCD.InsertRecord("RLO", olRloRecord);
		}

		olRpfArr.DeleteAll();
		olRpqArr.DeleteAll();
		olReqArr.DeleteAll();
		olRloArr.DeleteAll();
	}

	//  Kopiere alle nicht zugeordneten (Gruppen-)Qualifikationen
	pos = olUdgrList.GetHeadPosition ();
	while ( pos )
	{
		olUdgr = olUdgrList.GetNext ( pos );
		if ( !olUdgr.IsEmpty() )
		{
			ogBCD.GetRecords("RPQ", "UDGR", olUdgr, &olRpqArr);
			
			for (j = 0; j < olRpqArr.GetSize(); j++)
			{
				olRpqRecord = olRpqArr[j];
				olUrno.Format("%d", ogBCD.GetNextUrno());
				olRpqRecord.Values[igRpqUrnoIdx] = olUrno;
				olRpqRecord.Values[igRpqUrudIdx] = "";
				olRpqRecord.Values[igRpqUdgrIdx] = olUdgr;
				ogBCD.InsertRecord("RPQ", olRpqRecord);
			}
			olRpqArr.DeleteAll();
		}
	}
	
	blRet &= ogBCD.Save("RPF");
	blRet &= ogBCD.Save("RPQ");
	blRet &= ogBCD.Save("REQ");
	blRet &= ogBCD.Save("RLO");


	CCS_CATCH_ALL
	return blRet;
}
*/

//-----------------------------------------------------------------------------------------------------------------------

BOOL CollectiveRulesView::OnHelpInfo(HELPINFO* pHelpInfo)
{
	return TRUE;
}
//----------------------------------------------------------------------------

/*		wird z.Z. nicht benutzt
void CollectiveRulesView::OnDelete()
{
	CCS_TRY

	CCS_CATCH_ALL

}
*/

//----------------------------------------------------------------------------

void CollectiveRulesView::OnLeistungen()
{
	CCS_TRY


	UpdateData(TRUE);
	GhsList *polSerList = NULL;
	
	if (omCurrRueUrno != "")
	{
		polSerList = new GhsList(this, LBS_MULTIPLESEL);
	}
	else
	{
		// Do not open services list in case no rule has been chosen.
		//	Otherwise there'd be no Rule Urno for DataSet::CreateRudFromSerList() to use (RUD.URUE).
		MessageBox(GetString(1408), GetString(1409), MB_OK | MB_ICONEXCLAMATION);
		bgIsServicesOpen = false;
	}


	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

// not used at the moment, mne 010122
/*void CollectiveRulesView::OnContractBtn()
{
	CCS_TRY


	CCS_CATCH_ALL
}*/
//----------------------------------------------------------------------------

// not used at the moment, mne 010122
/*void CollectiveRulesView::OnGroupBtn()
{
	CCS_TRY


	CCS_CATCH_ALL
}*/
//----------------------------------------------------------------------------

void CollectiveRulesView::OnSelEndOK()
{
	OnNewRule();
}
//----------------------------------------------------------------------------

void CollectiveRulesView::OnCopy()
{	
	CCS_TRY


	long llUrno = ogBCD.GetNextUrno();
	CString olOldRueUrno = omCurrRueUrno;
	omCurrRueUrno.Format("%d", llUrno);
	m_Edt_RuleShortName.SetWindowText(CString(""));
	bmNewFlag = true;
	bool blOk = ogDataSet.OnCopyRue ( olOldRueUrno, omCurrRueUrno );
	UpdateServiceTable();
	pomDutyGrid->DisplayRudTmp (false);
	

	bgIsValSaved = false;

	// inform ChangeInfo that rule is copied
	ogChangeInfo.SetCopiedRuleFlag();
	ogChangeInfo.SetCurrRueUrno(omCurrRueUrno);


	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

LONG CollectiveRulesView::OnDragOver(UINT wParam, LONG lParam)
{
	CCSDragDropCtrl *pomDragDropCtrl = &m_DragDropTarget;

	int ilClass = m_DragDropTarget.GetDataClass(); 
	
	if (ilClass == DIT_SERLIST)
	{
		return 0;
	}
	return -1L;
}
//---------------------------------------------------------------------------------------------

LONG CollectiveRulesView::OnDrop(UINT wParam, LONG lParam)
{
	CCS_TRY


	CCSDragDropCtrl *polDragDropCtrl = &m_DragDropTarget;
	CStringArray	olSerUrnos;

	for (int ilIndex = 0; ilIndex < polDragDropCtrl->GetDataCount(); ilIndex++)
	{
		CString olUrno;
		olUrno.Format("%d", polDragDropCtrl->GetDataDWord(ilIndex));
		if (olUrno != "" && olUrno != " ")
		{
			olSerUrnos.Add(olUrno);
		}
	}

	AddServices ( olSerUrnos );
    pomDutyGrid->DisplayRudTmp (false);

	CCS_CATCH_ALL

	return -1L;
}
//----------------------------------------------------------------------------

void CollectiveRulesView::OnRButtonDown(UINT itemID, LPARAM lParam)
{
	CPoint point(lParam);
	
	// get context menu
	MakeBarMenu(itemID, point);
	CWnd::OnRButtonDown(0, point);

}
//----------------------------------------------------------------------------

void CollectiveRulesView::OnMenuDelete()
{
	CCS_TRY
	CStringArray olUrnoList;

	pomDutyGrid->GetSelectedUrnos(olUrnoList);

	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	if(	pomDutyGrid->IsExpanded())
	{

		for (int i = 0; i < olUrnoList.GetSize(); i++)
		{
			if(ogBCD.GetRecord("RUD_TMP", "URNO", olUrnoList.GetAt(i), olRecord))
			{
				if ( ogBCD.DeleteRecord("RUD_TMP", "URNO", olUrnoList.GetAt(i)) )
				{
					// tell viewer that the record was deleted
					ogDdx.DataChanged((void *)this,RUD_TMP_DELETE,(void *)&olRecord);
				}
			}
		}
	}
	else
	{
		CCSPtrArray<RecordSet>	olRudsOfDgr;
		CString					olUrno;

		for (int i = 0; i < olUrnoList.GetSize(); i++)
		{
			ogBCD.GetRecords ( "RUD_TMP", "UDGR", olUrnoList.GetAt(i), &olRudsOfDgr );
			for ( int i=olRudsOfDgr.GetSize()-1; i>=0; i-- )
			{
				olRecord = olRudsOfDgr[i];
				olUrno = olRecord.Values[igRudUrnoIdx];
				// delete this record from RUD_TMP
				if ( ogBCD.DeleteRecord("RUD_TMP", "URNO", olUrno ) )
				{
					// tell viewer that the record was deleted
					ogDdx.DataChanged((void *)this,RUD_TMP_DELETE,(void *)&olRecord);
				}
			}			
			olRudsOfDgr.DeleteAll();
		}
	}

	CCS_CATCH_ALL
}

// this function creates a connnection between different RUDs
// (eg. a location and a staff member) by filling RUD.ULNK with a new urno
void CollectiveRulesView::OnMenuCreateLink()
{
	long llNewUrno;
	CStringArray olUrnoList;
	llNewUrno = ogBCD.GetNextUrno();

	RecordSet olRud(ogBCD.GetFieldCount("RUD"));

	pomDutyGrid->GetSelectedUrnos(olUrnoList);
	for (int i = 0; i < olUrnoList.GetSize(); i++)
	{
		if(ogBCD.GetRecord("RUD_TMP", "URNO", olUrnoList.GetAt(i), olRud))
		{
			olRud.Values[igRudUlnkIdx].Format("%d", llNewUrno);
			ogBCD.SetRecord("RUD_TMP", "URNO", olRud.Values[igRudUrnoIdx], olRud.Values);
			ogDdx.DataChanged((void *)this,RUD_TMP_CHANGE,(void *)&olRud);
		}
	}

	// The RUD_CON_CHANGE message causes the whole gantt to be redrawn. We 
	// only send it once to avoid a flickering screen, which might otherwise
	// happen on a slow computer
	ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRud);
	pomDutyGrid->DisplayRudTmp (false);
	
}
//---------------------------------------------------------------------------------------------

// this function deletes a connnection between different RUDs by emptying RUD.ULNK
void CollectiveRulesView::OnMenuDeleteLink()
{
	CStringArray olUrnoList;

	pomDutyGrid->GetSelectedUrnos(olUrnoList);

	RecordSet olRud(ogBCD.GetFieldCount("RUD"));
	for (int i = 0; i < olUrnoList.GetSize(); i++)
	{
		if(ogBCD.GetRecord("RUD_TMP", "URNO", olUrnoList.GetAt(i), olRud))
		{
			olRud.Values[igRudUlnkIdx].Empty();
			ogBCD.SetRecord("RUD_TMP", "URNO", olRud.Values[igRudUrnoIdx], olRud.Values);
			ogDdx.DataChanged((void *)this,RUD_TMP_CHANGE,(void *)&olRud);
		}
	}
	pomDutyGrid->DisplayRudTmp (false);
}

void CollectiveRulesView::OnMenuCopyBar()
{
	
	CCS_TRY

	CStringArray olUrnoList;
	CString olNewRudUrno;
	pomDutyGrid->GetSelectedUrnos(olUrnoList);

	if(	pomDutyGrid->IsExpanded())
	{
		for (int i = 0; i < olUrnoList.GetSize(); i++)
		{
			ogDataSet.CopyOneRud ( olUrnoList.GetAt(i), olNewRudUrno );
		}
	}
	
	CCS_CATCH_ALL
}

//----------------------------------------------------------------------------
//						helper functions
//----------------------------------------------------------------------------
void CollectiveRulesView::MakeBarMenu(int itemID,  CPoint point) 
{
	CCS_TRY
	
	if(itemID != -1)
	{
		//-- create menu
		CMenu menu;
		menu.CreatePopupMenu();
		
		//-- remove all (old) menu items
		for (int i = menu.GetMenuItemCount() - 1; i >= 0; i--)
		{
			menu.RemoveMenu(i, MF_BYPOSITION);
		}
		
		//-- create menu items
		menu.AppendMenu(MF_STRING, CM_COLLECT_MENU_DELETEBAR, GetString(131));	// remove demand requirement from listbox
		menu.AppendMenu(MF_STRING, CM_COLLECT_MENU_COPYBAR, GetString(IDS_COPY) );
		menu.AppendMenu(MF_SEPARATOR);
		menu.AppendMenu(MF_STRING, CM_COLLECT_MENU_CREATELNK, GetString(2003));				// create grouping (Staff - location)
		menu.AppendMenu(MF_STRING, CM_COLLECT_MENU_DELETELNK, GetString(2004));				// delete grouping
		
		// calculate correct point
		// (offset btw. rect of view and rect of table)
		CRect olViewRect, olTableRect;
		GetWindowRect(olViewRect);
		pomDutyGrid-> GetWindowRect(olTableRect);
		int ilDiffX = olTableRect.left;
		int ilDiffY = olTableRect.top;
		point.x += ilDiffX + 10;
		point.y += ilDiffY;

		//-- show menu 
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
	}


	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

// This function contains the table initialization that has to be done only once
// The rest can be found in FillTable()
void CollectiveRulesView::TablesFirstTimeInit()
{
	CCS_TRY
	CRect olRect;
	int   ilWidth;


//--- left ressources grid
	pomLeftGrid->SubclassDlgItem(INDEP_CUS_LEFT, this);
	pomLeftGrid->Initialize();
	
	pomLeftGrid->LockUpdate(TRUE);

	pomLeftGrid->SetColCount(3);

	pomLeftGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomLeftGrid->SetColWidth(0,0,20);			// row header
	pomLeftGrid->SetColWidth(1,1,ilWidth / 2);	// code
	pomLeftGrid->SetColWidth(2,2,ilWidth / 2);	// name

	pomLeftGrid->HideCols(3, 3);

	pomLeftGrid->SetValueRange(CGXRange(0,1), GetString(CODE));	// code
	pomLeftGrid->SetValueRange(CGXRange(0,2), GetString(NAME));	// name
	pomLeftGrid->SetValueRange(CGXRange(0,3), CString("Urno"));	// urno
	
	CString olCode;
	CString olName;

	pomLeftGrid->GetParam()->SetNumberedRowHeaders(FALSE); 
	pomLeftGrid->GetParam()->EnableSelection(GX_SELNONE );	
	pomLeftGrid->GetParam()->EnableTrackRowHeight(0); 
	
	pomLeftGrid->RegisterGxCbsDropDown(true, true);

	pomLeftGrid->Redraw();
	pomLeftGrid->LockUpdate(FALSE);



//--- right ressources grid
	pomRightGrid->SubclassDlgItem(INDEP_CUS_RIGHT, this);
	pomRightGrid->Initialize();
	
	pomRightGrid->SetColCount(3);

	pomRightGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomRightGrid->SetColWidth(0,0,20);			// row header
	pomRightGrid->SetColWidth(1,1,ilWidth / 2);	// code
	pomRightGrid->SetColWidth(2,2,ilWidth / 2);	// name

	pomRightGrid->HideCols(3, 3);

	pomRightGrid->SetValueRange(CGXRange(0,1), GetString(CODE));	// code
	pomRightGrid->SetValueRange(CGXRange(0,2), GetString(NAME));	// name
	pomRightGrid->SetValueRange(CGXRange(0,3), CString("Urno"));	// urno
	
	pomRightGrid->GetParam()->SetNumberedRowHeaders(FALSE); 
	pomRightGrid->GetParam()->EnableSelection(GX_SELNONE );	
	pomRightGrid->GetParam()->EnableTrackRowHeight(0); 

	pomRightGrid->RegisterGxCbsDropDown ( true, true );

	
	
//--- duty grid
	pomDutyGrid->SubclassDlgItem(IDC_GRID, this);
	pomDutyGrid->Initialize();
	pomDutyGrid->IniLayout ();
	pomDutyGrid->GetWindowRect(&omDutyOriginalRect);
	pomDutyGrid->SetExpanded(true);
	
	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

void CollectiveRulesView::EnableDemandDetailControls(bool bpEnable)
{
	if (bpEnable == false)
	{
		pomLeftGrid->SetStyleRange(CGXRange().SetTable(), CGXStyle().SetEnabled(FALSE)
																	.SetReadOnly(TRUE));
		pomRightGrid->SetStyleRange(CGXRange().SetTable(), CGXStyle().SetEnabled(FALSE)
																	.SetReadOnly(TRUE));
	}
	else
	{
		pomLeftGrid->SetStyleRange(CGXRange().SetTable(), CGXStyle().SetEnabled(TRUE)
																	.SetReadOnly(FALSE));
		pomRightGrid->SetStyleRange(CGXRange().SetTable(), CGXStyle().SetEnabled(TRUE)
																	.SetReadOnly(FALSE));
	}

} 
//----------------------------------------------------------------------------

void CollectiveRulesView::FillDemandDetailControls() 
{
	CCS_TRY


	ClearRessourceTables();

	if (!pomGantt)
	{
		imRessourceType = pomDutyGrid->GetResTypeForSelection();
	}
	else
	{	
		//  Gantt-Darstellung
		CString olRety;
		if (!omCurrRudUrno.IsEmpty() )
		{
			olRety = ogBCD.GetField("RUD_TMP", "URNO", omCurrRudUrno, "RETY" );
			imRessourceType = olRety.Find ('1') ;
		}
	}
		

	switch (imRessourceType)
	{
		case 0:
			SetTablesForStaffView();
			break;

		case 1:
			SetTablesForEquipmentView();
			break;

		case 2:
			SetTablesForLocationsView();
	}

	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

void CollectiveRulesView::SetTablesForStaffView()
{
	CCS_TRY


	CCSPtrArray <RecordSet> olLeftArr;

	int ilLeftCount = GetResRecordsToDisplay("RPF_TMP", &olLeftArr);
	
	if (ilLeftCount >= 1)
	{
		SetCurrentRudUrno(olLeftArr[0].Values[igRpfUrudIdx]);
	}
	else
	{
		SetCurrentRudUrno("");
	}

	
	ClearRessourceTables();
	
	if (!ilLeftCount)
	{
		return;
	}


//--- make functions choicelist
	CString olChoiceList;
	olChoiceList = GetChoiceList("PFC", "FCTC");


//--- functions table
	RecordSet olRecord;
	CString olName;
	CString olCode;
	CString olUrno;
	CRect olRect;
	int		ilWidth;

	pomLeftGrid->LockUpdate(TRUE);
	int ilRowsInTable = ilLeftCount;
	if(	!pomDutyGrid->IsExpanded() && !pomGantt )	//  Darstellung von T�tigkeiten
		ilRowsInTable++;
	
	pomLeftGrid->SetAutoGrow(!pomDutyGrid->IsExpanded() && !pomGantt);

	pomLeftGrid->InsertRows(1,ilRowsInTable);
	
	// reset grids in case locations were shown before
	pomLeftGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomLeftGrid->SetColWidth(0,0,20);			// row header
	pomLeftGrid->SetColWidth(1,1,ilWidth / 2);	// code
	pomLeftGrid->SetColWidth(2,2,ilWidth / 2);	// name
	pomLeftGrid->HideCols(2, 2, FALSE);	// show second column

	pomLeftGrid->SetStyleRange(CGXRange(1, 1, ilRowsInTable, 1), CGXStyle()
										 .SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
										 .SetChoiceList(olChoiceList));

	//- column names
	pomLeftGrid->SetValueRange(CGXRange(0,1), GetString(CODE));
	pomLeftGrid->SetValueRange(CGXRange(0,2), GetString(NAME));
	pomLeftGrid->SetValueRange(CGXRange(0,3), CString("Urno"));
	
	//- fill cells with data
	for (int i = 0; i < ilLeftCount; i++)
	{
		olRecord = olLeftArr[i];

		olUrno = olRecord.Values[igRpfUrnoIdx];
		if ( GetGroupName ( &olRecord, igRpfGtabIdx, igRpfUpfcIdx, olCode ) )	//hag990827
		{
			ogBCD.GetField("SGR", "GRPN", olCode, "GRDS", olName);				//hag990827
			olCode = "*"+ olCode;
		}
		else
		{
			olCode = olRecord.Values[igRpfFccoIdx];
			ogBCD.GetField("PFC", "FCTC", olCode, "FCTN", olName);
		}
		pomLeftGrid->SetValueRange(CGXRange(i+1,1), olCode);
		pomLeftGrid->SetValueRange(CGXRange(i+1,2), olName);
		pomLeftGrid->SetValueRange(CGXRange(i+1,3), olUrno);
	}

	pomLeftGrid->SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetEnabled(FALSE) );
	pomLeftGrid->LockUpdate(FALSE);
	pomLeftGrid->Redraw();
	
//--- make qualifications choicelist
	olChoiceList = GetChoiceList("PER", "PRMC");

	//--- qualifications table
	pomRightGrid->LockUpdate(TRUE);
	pomRightGrid->InsertRows(1, 1);

	// reset grid settings in case locations were shown before
	pomRightGrid->SetAutoGrow(true);
	pomRightGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomRightGrid->SetColWidth(0,0,20);			// row header
	pomRightGrid->SetColWidth(1,1,ilWidth / 2);	// code
	pomRightGrid->SetColWidth(2,2,ilWidth / 2);	// name
	pomRightGrid->HideCols(2, 2, FALSE);	// show second column

	pomRightGrid->SetStyleRange(CGXRange(1, 1), CGXStyle()
										 .SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
										 .SetChoiceList(olChoiceList));
	//- column names
	pomRightGrid->SetValueRange(CGXRange(0, 1), GetString(CODE));
	pomRightGrid->SetValueRange(CGXRange(0, 2), GetString(NAME));
	pomRightGrid->SetValueRange(CGXRange(0, 3), CString("Urno"));
	pomRightGrid->LockUpdate(FALSE);

	// set qualification entries
	DisplayQualisForRud (omCurrRudUrno);

	olLeftArr.DeleteAll();
	SetDlgItemText ( INDEP_STA_VRGC, GetString(PFC_HEADER) );
	SetDlgItemText ( INDEP_STA_PERM, GetString(PER_HEADER) );

	
	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

void CollectiveRulesView::SetTablesForEquipmentView()
{
	CCS_TRY


	CCSPtrArray <RecordSet> olRightArr;
	CStringArray olTypeArray;	
	CString olItem;
	CString olChoiceList;
	CRect		olRect;
	int			ilWidth;
	
	int ilRightCount = GetResRecordsToDisplay( "REQ_TMP", &olRightArr );

	if ( ilRightCount >= 1 )
		SetCurrentRudUrno ( olRightArr[0].Values[igReqUrudIdx] );	
	else
		SetCurrentRudUrno ("");

	int i/*, j, ilSize*/;
	RecordSet olRecord;
	CString olName;
	CString olCode;
	CString olUrno, olType;

	ClearRessourceTables();

	//--- location table
	pomRightGrid->SetAutoGrow(false);
	pomRightGrid->LockUpdate(TRUE);
	pomRightGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomRightGrid->SetColWidth(0,0,20);			// row header
	pomRightGrid->SetColWidth(1,1,ilWidth / 2);	// code
	pomRightGrid->SetColWidth(2,2,ilWidth / 2);	// name
	pomRightGrid->HideCols(2, 2, FALSE);	// show second column

	//- column names
	pomRightGrid->SetValueRange(CGXRange(0,1), GetString(CODE));
	pomRightGrid->SetValueRange(CGXRange(0,2), GetString(NAME));
	pomRightGrid->SetValueRange(CGXRange(0,3), CString("Urno"));
	
	//- fill cells with data
	for (i = 0; i < ilRightCount; i++)
	{
		olRecord = olRightArr[i];

		olUrno = olRecord.Values[igReqUrnoIdx];
		olTypeArray.SetAtGrow(i, olRecord.Values[igReqEtypIdx]);
		if ( GetGroupName ( &olRecord, igReqGtabIdx, igReqUequIdx, olCode ) )
		{
			ogBCD.GetField("SGR", "GRPN", olCode, "GRDS", olName);			
			olCode = "*"+ olCode;
		}
		else
		{
			olCode = olRecord.Values[igReqEqcoIdx];
			ogBCD.GetField("EQU", "GCDE", olCode, "ENAM", olName);
		}
		pomRightGrid->InsertRows(i + 1, 1);
		olChoiceList = GetChoiceList("EQU", "GCDE", &olRecord.Values[igReqEtypIdx] );
		pomRightGrid->SetStyleRange(CGXRange(i + 1, 1), CGXStyle()
											.SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
											.SetChoiceList(olChoiceList));
		//- fill cell with data
		pomRightGrid->SetValueRange(CGXRange(i+1,1), olCode);
		pomRightGrid->SetValueRange(CGXRange(i+1,2), olName);
		pomRightGrid->SetValueRange(CGXRange(i+1,3), olUrno);
	}

	pomRightGrid->SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetEnabled(FALSE) );
	pomRightGrid->LockUpdate(FALSE);
	pomRightGrid->Redraw();	


	//--- equipment table types
	//- column names
	pomLeftGrid->SetValueRange(CGXRange(0,1), GetString(NAME));
	pomLeftGrid->SetValueRange(CGXRange(0,2), "");

	//- fill cells with data
	if (igEqtNameIdx >= 0)		//  Wenn ALO da und gef�llt
	{
		pomLeftGrid->LockUpdate(TRUE);
		for ( i=0; i<ilRightCount; i++ )
		{
			CString olReft, olTable, olCodeField, olDesc;
			olType = olTypeArray.GetAt ( i );
	
			olName = ogBCD.GetField("EQT", "URNO", olType, "NAME");
			pomLeftGrid->InsertRows(i + 1, 1);
			pomLeftGrid->SetValueRange(CGXRange(i+1,1), olName);
			pomLeftGrid->SetValueRange(CGXRange(i+1,2), "");
			pomLeftGrid->SetStyleRange(CGXRange().SetRows(i + 1), CGXStyle().SetEnabled(FALSE));
		}
		pomLeftGrid->GetClientRect(&olRect);
		olRect.DeflateRect(3, 0);
		ilWidth = olRect.Width() - 20;
		pomLeftGrid->SetColWidth(1,1,ilWidth);	// code
		pomLeftGrid->HideCols(2, 2);
		pomLeftGrid->LockUpdate(FALSE);
		pomLeftGrid->Redraw();
	}
	olRightArr.DeleteAll();
	SetDlgItemText ( INDEP_STA_VRGC, GetString(IDS_EQUIPMENT_TYPES) );
	SetDlgItemText ( INDEP_STA_PERM, GetString(EQU_HEADER) );


	CCS_CATCH_ALL
}

//----------------------------------------------------------------------------

void CollectiveRulesView::SetTablesForLocationsView()
{
	CCS_TRY


	CCSPtrArray <RecordSet> olRightArr;
	CString olItem;
	CString olChoiceList;
	CStringArray olReftArray;	
	CRect		olRect;
	int			ilWidth;
	
	int ilRightCount = GetResRecordsToDisplay( "RLO_TMP", &olRightArr );

	if ( ilRightCount >= 1 )
		SetCurrentRudUrno ( olRightArr[0].Values[igRloUrudIdx] );	
	else
		SetCurrentRudUrno ("");

	int i/*, j, ilSize*/;
	int ilLeftCount = ogBCD.GetDataCount ( "ALO" );

	ClearRessourceTables();

	//--- location table
	pomRightGrid->SetAutoGrow(false);
	pomRightGrid->LockUpdate(TRUE);
	for (i = 0; i < ilRightCount; i++)
	{
		CString olCode, olTable, olCodeField;
		if ( GetGroupName ( &(olRightArr[i]), igRloGtabIdx, igRloRlocIdx, olCode ) )
			olCode = "*"+ olCode;
		else
			if ( !GetLocationCode(olRightArr[i].Values[igRloRlocIdx], olRightArr[i].Values[igRloReftIdx], olCode) )
			{
				olCode = "???";
			}
		olReftArray.SetAtGrow(i, olRightArr[i].Values[igRloReftIdx]);
		olChoiceList = CString("");
		if (ParseReftFieldEntry(olRightArr[i].Values[igRloReftIdx], olTable, olCodeField))
		{
			olChoiceList = GetChoiceList(olTable, olCodeField);
		}
		pomRightGrid->InsertRows(i + 1, 1);
		pomRightGrid->SetStyleRange(CGXRange(i + 1, 1), CGXStyle()
											.SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
											.SetChoiceList(olChoiceList));
		//- fill cell with data
		pomRightGrid->SetValueRange(CGXRange(i + 1, 1), olCode);
		pomRightGrid->SetValueRange(CGXRange(i + 1, 3), olRightArr[i].Values[igRloUrnoIdx]);
	}

	//- column names
	pomRightGrid->SetValueRange(CGXRange(0, 1), GetString(CODE));
	pomRightGrid->SetValueRange(CGXRange(0, 2), "");
	pomRightGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomRightGrid->SetColWidth(1,1,ilWidth);	// code

	pomRightGrid->HideCols(2, 2);
	pomRightGrid->LockUpdate(FALSE);
	pomRightGrid->Redraw();

	//--- location table types
	//- column names
	pomLeftGrid->SetValueRange(CGXRange(0,1), GetString(CODE));
	pomLeftGrid->SetValueRange(CGXRange(0,2), "");

	//- fill cells with data
	if (ilLeftCount > 0)		//  Wenn ALO da und gef�llt
	{
		pomLeftGrid->LockUpdate(TRUE);
		for ( i=0; i<ilRightCount; i++ )
		{
			CString olReft, olTable, olCodeField, olDesc;
			olReft = olReftArray.GetAt ( i );
	
			olDesc = ogBCD.GetField("ALO", "REFT", olReft, "ALOD");
			pomLeftGrid->InsertRows(i + 1, 1);
			pomLeftGrid->SetValueRange(CGXRange(i+1,1), olDesc);
			pomLeftGrid->SetValueRange(CGXRange(i+1,2), "");
			pomLeftGrid->SetStyleRange(CGXRange().SetRows(i + 1), CGXStyle().SetEnabled(FALSE));
		}
		pomLeftGrid->GetClientRect(&olRect);
		olRect.DeflateRect(3, 0);
		ilWidth = olRect.Width() - 20;
		pomLeftGrid->SetColWidth(1,1,ilWidth);	// code
		pomLeftGrid->HideCols(2, 2);
		pomLeftGrid->LockUpdate(FALSE);
		pomLeftGrid->Redraw();
	}
	olRightArr.DeleteAll();
	SetDlgItemText ( INDEP_STA_VRGC, GetString(LOCATION_TYP) );
	SetDlgItemText ( INDEP_STA_PERM, GetString(IDS_LOCATION) );


	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

void CollectiveRulesView::ClearRessourceTables()
{
	CCS_TRY

	
	// left grid
	int ilCount = pomLeftGrid->GetRowCount();
	if (ilCount > 0)
	{
		pomLeftGrid->RemoveRows(1, ilCount);
	}
	pomLeftGrid->Redraw();

	// right grid
	ilCount = pomRightGrid->GetRowCount();
	if (ilCount > 0)
	{
		pomRightGrid->RemoveRows(1, ilCount);
	}
	pomRightGrid->Redraw();

	
	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

bool CollectiveRulesView::SaveDemandRequirements()
{
	bool blReturn = true;

	CCS_TRY

	
	//--- copy all the rud entries from RUD_TMP to RUD
	RecordSet olRecord;
	int ilSize = ogBCD.GetDataCount("RUD_TMP");

	if (ilSize > 0)
	{
		for (int i = 0; i < ilSize; i++)
		{
			bool blErr1 = ogBCD.GetRecord("RUD_TMP", i, olRecord);
			
			// check if end > start
			/*if ( imRuty2 )*/
			{	//  zeitgesteuerte Regel 
				CString olTimeStrB, olTimeStrE;
				CTime olTimeB, olTimeE;

				// int ilStartIdx = (bmIsFloating != TRUE ? igRudDebeIdx : igRudEadbIdx);
				// int ilEndIdx = (bmIsFloating != TRUE ? igRudDeenIdx : igRudLadeIdx);

				// olTimeStrB = olRecord.Values[ilStartIdx]; 
				// olTimeStrE = olRecord.Values[ilEndIdx]; 
				olTimeStrB = olRecord.Values[igRudDebeIdx]; 
				olTimeStrE = olRecord.Values[igRudDeenIdx]; 
				
				olTimeB = DBStringToDateTime(olTimeStrB);
				olTimeE = DBStringToDateTime(olTimeStrE);

				CString TestUrno, TestDedu;
				TestUrno = olRecord.Values[igRudUrnoIdx];
				TestDedu = olRecord.Values[igRudDeduIdx];

				/*if (bmIsFloating)
				{
					olTimeStrB = olRecord.Values[igRudDebeIdx]; 
					olTimeStrE = olRecord.Values[igRudLadeIdx];

					olTimeB = DBStringToDateTime(olTimeStrB);
					olTimeE = DBStringToDateTime(olTimeStrE);
				}
				else
				{
					olTimeStrB = olRecord.Values[igRudDebeIdx]; 
					olTimeStrE = olRecord.Values[igRudDeenIdx];

					olTimeB = DBStringToDateTime(olTimeStrB);
					olTimeE = DBStringToDateTime(olTimeStrE);
				}*/
				

				/*if (olTimeE < olTimeB)
				{
					CTimeSpan olSpan;
					olSpan = CTimeSpan(1, 0, 0, 0);
					olTimeE += olSpan;

					olRecord.Values[ilEndIdx] = olTimeE.Format("%Y%m%d%H%M%S");
				}
				
				if ( (ilStartIdx == igRudDebeIdx) && (ilEndIdx == igRudDeenIdx) )
				{
					// calculate duration if not floating 
					long llDuration = (olTimeE - olTimeB).GetTotalSeconds();
					while (llDuration > 86400)
						llDuration -= 86400;
					olRecord.Values[igRudDeduIdx].Format("%d", llDuration);
				}*/
			}
			/*else
			{
				olRecord.Values[igRudDebeIdx] = 
				olRecord.Values[igRudDeenIdx] = 
				olRecord.Values[igRudEadbIdx] = 
				olRecord.Values[igRudLadeIdx] = "";
			}*/
			bool blErr2 = ogBCD.InsertRecord("RUD", olRecord, false);
			if (blErr1 == false || blErr2 == false)
			{
				ogLog.Trace("DEBUG", "[RegelWerkView::SaveDemandPrefs] Failed appending RUD record [index %d] to object.", i);
				blReturn = false;
				break;
			}
		}

		// save RUD records to database
		if (blReturn)
		{ 
			bool blErr = ogBCD.Save("RUD");
			if (blErr == true)
			{
				ogLog.Trace("WRITE2DB", "[RegelWerkView::SaveDemandPrefs] Saved %d RUD records to database", i);
			}
			else
			{
				blReturn = false;
				ogLog.Trace("DEBUG","Failed saving RUD records to database");
			}
		}
	}

	CCS_CATCH_ALL

	return blReturn;
}
//----------------------------------------------------------------------------

void CollectiveRulesView::UpdateServiceTable()
{
	int ilRudCount = ogBCD.GetDataCount("RUD_TMP");

	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	CString olSerUrno;
	CString olName;
	long llRudUrno;
	for (int i = 0; i < ilRudCount; i++)
	{
		ogBCD.GetRecord("RUD_TMP", i, olRecord);
		
		olSerUrno = olRecord.Values[igRudUghsIdx];
		llRudUrno = atol(olRecord.Values[igRudUrnoIdx]);
		olName = ogBCD.GetField("SER", "URNO", olSerUrno, "SNAM");
	}
}
//----------------------------------------------------------------------------

bool CollectiveRulesView::IsPassFilter(RecordSet *popRud)
{	
	// Exception handling should not be implemented in this function 
	// Frequently repeated exception handling code might slow down program execution

	//--- return value 
	bool blRet = false;

	//--- get data from CedaBasicData
	CString olUrue;
	olUrue = popRud->Values[igRudUrueIdx];

	//--- compare data with filter conditions
	if(olUrue == omCurrRueUrno)
	{
		blRet = true;
	}

	return blRet;
}



//----------------------------------------------------------------------------
//						some stuff (diagnostics)
//----------------------------------------------------------------------------
#ifdef _DEBUG
void CollectiveRulesView::AssertValid() const
{
	CRulesFormView::AssertValid();
}
//----------------------------------------------------------------------------

void CollectiveRulesView::Dump(CDumpContext& dc) const
{
	CRulesFormView::Dump(dc);
}
#endif //_DEBUG



//---------------------------------------------------------------------------
//					processing functions
//---------------------------------------------------------------------------
void CollectiveRulesView::ProcessRudTmpNew(RecordSet *popRecord)
{
	if (popRecord != NULL)
	{

		
		pomDutyGrid->OnNewRudTmp ( popRecord );
		if (IsPassFilter(popRecord))
		{
			CString olSerUrno;
			CString olName;

			olSerUrno = popRecord->Values[igRudUghsIdx];
			olName = ogBCD.GetField("SER", "URNO", olSerUrno, "SNAM");
			long llUrno = atol(popRecord->Values[igRudUrnoIdx]);

			pomDutyGrid->DisplayRudTmp (false);
		}
	}
}
//---------------------------------------------------------------------------

void CollectiveRulesView::ProcessRudTmpChange(RecordSet *popRecord)
{
	
}
//---------------------------------------------------------------------------

void CollectiveRulesView::ProcessRudTmpDelete(RecordSet *popRecord)
{
	pomDutyGrid->OnDeleteRudTmp ( popRecord );

	if ( pomGantt && !omCurrDgrUrno.IsEmpty() )
	{
		CCSPtrArray<RecordSet> olDgrMembers;
		ogBCD.GetRecords ( "RUD_TMP", "UDGR", omCurrDgrUrno, &olDgrMembers );
		if ( olDgrMembers.GetSize() < 1 )
			// CloseGantt ();
		olDgrMembers.DeleteAll ();
	}
	ogDataSet.DeleteRessources(*popRecord);		
}
//---------------------------------------------------------------------------
/*
bool CollectiveRulesView::IsSingleResourceSelected ( ROWCOL ipRow, ROWCOL ipCol,
													  CGridFenster *popGrid,
													  CString opTable, CString &ropCode )
{
	int			ilIndex, ilSingles;
	CString		olFound, olChoiceList;
	CGXComboBoxWnd	*polCBWnd;
	CGXStyle		olStyle;
	
	ASSERT(popGrid);
	//  Choicelist f�r das angegebene Feld holen
	polCBWnd = (CGXComboBoxWnd*)popGrid->GetControl ( ipRow, ipCol );
	if ( !polCBWnd  )
		return true;		//  default fall
	popGrid->ComposeStyleRowCol( ipRow, ipCol, &olStyle);
	if ( !olStyle.GetIncludeChoiceList() )
		return true;		//  default fall

	olChoiceList = olStyle.GetChoiceList() ;
	//  Index der gew�hlten Resource in Combobo und Anzahl der Datens�tze 
	//  im entsprechenden Resourcetable ermitteln
	ilIndex = polCBWnd->FindStringInChoiceList( olFound, (const char*)ropCode, 
											    (const char*)olChoiceList, TRUE );
	ilSingles = ogBCD.GetDataCount ( opTable );
	if ( !ropCode.IsEmpty() && ropCode[0] == '*' )
		ropCode = ropCode.Right ( ropCode.GetLength()-1 );

	return ( ilIndex<ilSingles );
}
*/
//---------------------------------------------------------------------------

bool CollectiveRulesView::CheckChanges()
{
	bool blRet = true;
	CString olTmp;


	CCS_TRY

	if (bmNewFlag == false)
	{
		RecordSet olRue(ogBCD.GetFieldCount("RUE"));
		ogBCD.GetRecord("RUE", "URNO", omCurrRueUrno, olRue);
		
		// RUSN
		m_Edt_RuleShortName.GetWindowText(olTmp);	
		blRet &= (olRue.Values[igRueRusnIdx] == olTmp);

		// RUNA
		m_Edt_RuleName.GetWindowText(olTmp);	
		blRet &= (olRue.Values[igRueRunaIdx] == olTmp);

		// EVTY
		// olTmp = imRuty2  ? "2" : "3";
		blRet &= (olRue.Values[igRueRutyIdx] == "1");
		
		// REMA
		/*m_Edt_Descript.GetWindowText(olTmp);
		if (olTmp.GetLength() > 254)
		{
			olTmp = olTmp.Left(254);
		}
		MakeCedaString(olTmp);
		blRet &= (olRue.Values[igRueRemaIdx] == olTmp);*/
		
		// RUST
		olTmp = (m_Chb_Deactivated.GetCheck() == CHECKED) ? "0" : "1";
		blRet &= (olTmp == olRue.Values[igRueRustIdx]);
	}
	else
	{
		// RUSN
		m_Edt_RuleShortName.GetWindowText(olTmp);
		blRet &= (olTmp == CString(""));

		// RUNA
		m_Edt_RuleName.GetWindowText(olTmp);
		blRet &= (olTmp == CString(""));
	}


	CCS_CATCH_ALL

	return blRet;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void CollectiveRulesView::OnGridButton(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
	
	 CCS_CATCH_ALL
}

//---------------------------------------------------------------------------



void CollectiveRulesView::SetStaticTexts()
{
	SetWindowText(GetString(1363));

	SetDlgItemLangText ( this, IDC_LIMITATIONS, IDS_LIMITATIONS );
	SetDlgItemLangText ( this, IDC_STA_RUSN, 219 );
	SetDlgItemLangText ( this, IDC_STA_RUNA, 217 );
	SetDlgItemLangText ( this, IDC_DEACTIVATED, IDS_DEAKTIVIERT );
	SetDlgItemLangText ( this, INDEP_STA_VRGC, PFC_HEADER );
	SetDlgItemLangText ( this, INDEP_STA_PERM, PER_HEADER );
	SetDlgItemLangText ( this, IDC_STA_ALOC, IDS_ZUORDNUNGSEINHEIT );
	SetDlgItemLangText ( this, IDC_STA_REF, IDS_REFERENZ );
	SetDlgItemLangText ( this, IDC_DIDE_CHK, IDS_TEILBAR );
	SetDlgItemLangText ( this, IDC_STA_MINTIME, IDS_SPLITMIN );
	SetDlgItemLangText ( this, IDC_STA_MAXTIME, IDS_SPLITMAX );
	SetDlgItemLangText ( this, IDC_FOND_CHK, IDS_NOT_OPERATIV );
	SetDlgItemLangText ( this, IDC_FADD_CHK, IDS_NO_DEMANDS );
	SetDlgItemLangText ( this, IDC_STA_EARLIEST, IDS_VON );
	SetDlgItemLangText ( this, IDC_STA_LATEST, IDS_BIS );
	SetDlgItemLangText ( this, IDC_STA_MINCOUNT, IDS_MINCOUNT );
	SetDlgItemLangText ( this, IDC_STA_MAXCOUNT, IDS_MAXCOUNT );

}
//---------------------------------------------------------------------------


int CollectiveRulesView::GetResRecordsToDisplay(CString opTable, CCSPtrArray <RecordSet> *popRecords)
{
	CCSPtrArray <RecordSet> olRudRecs;
	RecordSet olResRec;
	CString olRudUrno;
	int i;
	
	if (pomGantt || pomDutyGrid->IsExpanded())	// function for one duty
	{
		ogBCD.GetRecords(opTable, "URUD", omCurrRudUrno, popRecords);
	}
	else
	{
		// alle RUD_TMP-S�tze dieser Demandgroup holen
		ogBCD.GetRecords("RUD_TMP", "UDGR", omCurrDgrUrno, &olRudRecs );
		for (i = 0; i < olRudRecs.GetSize(); i++)
		{
			olRudUrno = olRudRecs[i].Values[igRudUrnoIdx];
			if (!olRudUrno.IsEmpty() && ogBCD.GetRecord(opTable, "URUD", olRudUrno, olResRec))
			{
				popRecords->New(olResRec);
			}
		}
		olRudRecs.DeleteAll();
	}
	return popRecords->GetSize();
}
//----------------------------------------------------------------------------------------------------------------------

void CollectiveRulesView::OnGridCurrCellChanged(WPARAM wParam, LPARAM lParam)
{
	CGridFenster *polGrid = (CGridFenster*) wParam;
	
	CELLPOS *prlCellPos = (CELLPOS*) lParam;
	ROWCOL ilCol = (ROWCOL) prlCellPos->Col;
	ROWCOL ilRow = (ROWCOL) prlCellPos->Row;
	int    ilMarkInLeftGrid=-1;
	int    ilMarkInRightGrid=-1;

	//  aktive Zelle im Duty-Grid hat sich ge�ndert
	bool blDutySelected;
	CString olNewRudUrno ;
		
	if (polGrid == pomDutyGrid)
	{
		if (ilRow != imActiveRow)
		{
			imActiveRow = ilRow;
			CString olNewDgrUrno ;
			blDutySelected = pomDutyGrid->GetSelectionInfo (olNewDgrUrno, olNewRudUrno);
			EnableDemandDetailControls(blDutySelected);

			if (blDutySelected) 
			{	
				if ((olNewDgrUrno != omCurrDgrUrno) || (olNewRudUrno != omCurrRudUrno)) 
				{
					omCurrDgrUrno = olNewDgrUrno;
					SetCurrentRudUrno(olNewRudUrno);
					FillDemandDetailControls();
				}
				if (imRessourceType)
				{
					ilMarkInRightGrid = 1;
				}
				else
				{
					ilMarkInLeftGrid = 1;
				}
			}
			else
			{
				ClearRessourceTables();
			}

			pomLeftGrid->SelectRowHeader(ilMarkInLeftGrid); 
			pomRightGrid->SelectRowHeader(ilMarkInRightGrid); 
			pomDutyGrid->SelectRowHeader(ilRow ? ilRow : -1);
		}
	}
	if ((polGrid == pomLeftGrid) && (imRessourceType == 0))
	{
		if (ilRow > 0)
		{
			CString olRpfUrno;
			olRpfUrno = pomLeftGrid->GetValueRowCol(ilRow, 3);
			if (olRpfUrno.IsEmpty())
			{
				olNewRudUrno.Empty();
			}
			else
			{
				olNewRudUrno = ogBCD.GetField("RPF_TMP", "URNO", olRpfUrno, "URUD");
			}
			
			if (olNewRudUrno != omCurrRudUrno)
			{
				DisplayQualisForRud (olNewRudUrno);
			}
			SetCurrentRudUrno(olNewRudUrno);
			ilMarkInLeftGrid = ilRow;
		}
		pomLeftGrid->SelectRowHeader(ilMarkInLeftGrid); 

	}
	
	if ( (polGrid == pomRightGrid) &&
		 ( (imRessourceType==1) || (imRessourceType==2) )
	   )
	{
		CString olResUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
		CString olResTable = imRessourceType == 2 ? "RLO_TMP" : "REQ_TMP";
		if (olResUrno.IsEmpty())
		{
			SetCurrentRudUrno("");
		}
		else
		{
			SetCurrentRudUrno(ogBCD.GetField( olResTable, "URNO", olResUrno, "URUD"));
		}
		pomRightGrid->SelectRowHeader(omCurrRudUrno.IsEmpty() ? -1 : ilRow) ;

	}	
}
//----------------------------------------------------------------------------------------------------------------------

void CollectiveRulesView::DisplayQualisForRud ( CString &ropRudUrno )
{
	CCSPtrArray <RecordSet> olRightArr;
	int						ilRows, ilCols, i;
	BOOL					ilOk;
	RecordSet				olRecord;
	CString					olName, olCode, olUrno, olUdgr ;

	pomRightGrid->LockUpdate(TRUE);
	
	ResetResourceTable ( pomRightGrid );
	if ( ropRudUrno.IsEmpty() )
	{
		pomRightGrid->LockUpdate(FALSE);
		return;
	}
	
	ogBCD.GetRecords("RPQ_TMP", "URUD", ropRudUrno, &olRightArr);
	int ilRightCount = olRightArr.GetSize();

	//- fill cells with data
	for (i = 0; i < ilRightCount; i++)
	{
		//pomRightGrid->DoAutoGrow(i, 1);
		olRecord = olRightArr[i];

		olUrno = olRecord.Values[igRpqUrnoIdx];
		if ( GetGroupName ( &olRecord, igRpqGtabIdx, igRpqUperIdx, olCode ) )	//hag990827
		{
			ogBCD.GetField("SGR", "GRPN", olCode, "GRDS", olName);				//hag990827
			olCode = "*"+ olCode;
		}
		else
		{
			olCode = olRecord.Values[igRpqQucoIdx];
			ogBCD.GetField("PER", "PRMC", olCode, "PRMN", olName);
		}
		
		pomRightGrid->SetValueRange(CGXRange(i + 1, 1), olCode);
		pomRightGrid->SetValueRange(CGXRange(i + 1, 2), olName);
		pomRightGrid->SetValueRange(CGXRange(i + 1, 3), olUrno);
		pomRightGrid->DoAutoGrow(i+1, 1);
	}

	//  Zus�tzlich nicht zugeordnete Qualifikationen dieser Demandgroup anzeigen
	ilCols = pomRightGrid->GetColCount ();
	if ( ogBCD.GetField("RUD_TMP", "URNO", ropRudUrno, "UDGR", olUdgr ) &&
		 !olUdgr.IsEmpty() )
	{
		ogBCD.GetRecords("RPQ_TMP", "UDGR", olUdgr, &olRightArr);
		
		for (i = 0; i < olRightArr.GetSize(); i++)
		{
			olRecord = olRightArr[i];
	
			olUrno = olRecord.Values[igRpqUrnoIdx];
			if ( GetGroupName ( &olRecord, igRpqGtabIdx, igRpqUperIdx, olCode ) )	
			{
				ogBCD.GetField("SGR", "GRPN", olCode, "GRDS", olName);				
				olCode = "*"+ olCode;
			}
			else
			{
				olCode = olRecord.Values[igRpqQucoIdx];
				ogBCD.GetField("PER", "PRMC", olCode, "PRMN", olName);
			}
			ilRows = ilRightCount + i + 1;
			ilOk = pomRightGrid->SetValueRange(CGXRange(ilRows, 1), olCode);
			pomRightGrid->SetValueRange(CGXRange(ilRows, 2), olName);
			pomRightGrid->SetValueRange(CGXRange(ilRows, 3), olUrno);
			pomRightGrid->DoAutoGrow(ilRows, 1);				//hag990831
			pomRightGrid->SetStyleRange(CGXRange(ilRows, 1, ilRows, 2 ), CGXStyle().SetInterior(ogColors[PYELLOW_IDX]));
		}
		
	}
	pomRightGrid->SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetEnabled(FALSE) );
	pomRightGrid->Redraw();
	pomRightGrid->LockUpdate(FALSE);
	olRightArr.DeleteAll();
}
//----------------------------------------------------------------------------------------------------------------------

//  Grid popGrid leeren, d.h. es bleibt nur eine leere Zeile �brig
void CollectiveRulesView::ResetResourceTable ( CGridFenster *popGrid )
{
	popGrid->LockUpdate(TRUE);
	ROWCOL ilRows = popGrid->GetRowCount();
	ROWCOL ilCols = popGrid->GetColCount();
	if ( ilRows >= 2 )
		popGrid->RemoveRows ( 2, ilRows );
	if ( ( ilRows >= 1 ) && ( ilCols >= 1 ) )
	{
		popGrid->SetStyleRange(CGXRange(1,1,1,ilCols),
							   CGXStyle().SetInterior(ogColors[WHITE_IDX])
										 .SetValue(""));

	}
	popGrid->LockUpdate(FALSE);
}	
//----------------------------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------

void CollectiveRulesView::OnSelendokCobAlod() 
{
	int		ilSel = m_AlocCB.GetCurSel();
	long	llAloUrno = m_AlocCB.GetItemData(ilSel);
	CString	olAloUrno, olAloCode;

	if ( (llAloUrno != CB_ERR) && !omCurrRudUrno.IsEmpty() )
	{
		olAloUrno.Format("%d", llAloUrno);
		olAloCode = ogBCD.GetField ( "ALO", "URNO", olAloUrno, "ALOC" );
		ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "ALOC", olAloCode );
	}
	IniRefCB();
}
//---------------------------------------------------------------------------

//	Set new Rud-Urno
//	Update all displays for current RUD
void CollectiveRulesView::SetCurrentRudUrno ( CString opRudUrno )
{
	if ( omCurrRudUrno == opRudUrno )
		return ;		//  no change of urno, return
	omCurrRudUrno = opRudUrno;
	DemandDetailDlg::SelectAloc ( &m_AlocCB, omCurrRudUrno );
	IniRefCB();
	DisplayRudInControls();
	EnableControls();
}
//---------------------------------------------------------------------------

void CollectiveRulesView::OnGridRButton(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

		
	CGridFenster*polGrid = (CGridFenster*) wParam;
	CELLPOS		*prlCellPos = (CELLPOS*) lParam;
	if ( !polGrid || ! prlCellPos || (polGrid !=pomRightGrid)  )
		return;
	ROWCOL ilCol = (ROWCOL) prlCellPos->Col;
	ROWCOL ilRow = (ROWCOL) prlCellPos->Row;
	if ( /*bmExpanded &&*/ !omCurrRudUrno.IsEmpty() && !imRessourceType )
	{
		CString		olUrno, olUdgr, olUrud;
		RecordSet	olRecord;

		olUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
		if ( !olUrno.IsEmpty () &&	
			 ogBCD.GetRecord ( "RPQ_TMP", "URNO", olUrno, olRecord ) )
		{
			olUrud = olRecord.Values[igRpqUrudIdx];
			olUdgr = olRecord.Values[igRpqUdgrIdx];
			if ( olUrud.IsEmpty () )	//  Gruppenqualifikation wird zugeordnet
			{
				olRecord.Values[igRpqUrudIdx] = omCurrRudUrno;
				olRecord.Values[igRpqUdgrIdx] = "";
			}
			else			//  Einzelqualifikation wird Gruppenqualifikation 
			{
				olRecord.Values[igRpqUrudIdx] = "";
				olRecord.Values[igRpqUdgrIdx] = omCurrDgrUrno;
			}
			ogBCD.SetRecord ( "RPQ_TMP", "URNO", olUrno, olRecord.Values ) ;
			DisplayQualisForRud ( omCurrRudUrno );
		}
	}

	
	CCS_CATCH_ALL
}

//------------------------------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------------------------


void CollectiveRulesView::OnLButtonDblClk(UINT itemID, LPARAM lParam)
{
	/*CPoint point(lParam);
	
	if ( !omCurrDgrUrno.IsEmpty() && !pomDutyGrid->IsExpanded() && !imRuty2 )
	{
		if ( !pomGantt )
			OpenGantt (omCurrDgrUrno);
		else
			CloseGantt ();
	}*/
}
//------------------------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------


void CollectiveRulesView::OnMouseMove(UINT nFlags, CPoint point) 
{
	CRulesFormView::OnMouseMove(nFlags, point);
}
//-----------------------------------------------------------------------------------------------------

void CollectiveRulesView::OnEditKillFocus(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

	CCSEDITNOTIFY *prlNotify;
	prlNotify = (CCSEDITNOTIFY*) lParam;

	UINT ilID = (UINT) wParam;
	RecordSet olRecord;

	
	if ( omCurrRudUrno.IsEmpty() || !ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord) )
		return;

	if ( prlNotify->SourceControl == &omEditSplitMax)
		OnChangeMaxtime();
	if ( prlNotify->SourceControl == &omEditSplitMin)
		OnChangeMintime();
	if ( prlNotify->SourceControl == &omEditLifr)
		OnChangeLifr();
	if ( prlNotify->SourceControl == &omEditLito)
		OnChangeLito();
	if ( prlNotify->SourceControl == &omEditMaxCount)
		OnChangeMaxc();
	if ( prlNotify->SourceControl == &omEditMinCount)
		OnChangeMinc();



       ccistart();
       cciend();


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------

/*bool CollectiveRulesView::SelectRudBar ( CString &ropRudUrno )
{
	int ilLineno=-1, ilBarno=-1;
	if ( pomGantt && !ropRudUrno.IsEmpty() )
	{
		if (pomViewer->FindBarGlobal(ropRudUrno, ilLineno, ilBarno) && 
			(ilLineno>=0) && (ilBarno>=0) )
		{
			pomViewer->DeselectAllBars(); 
			pomViewer->SetBarSelected(ilLineno, ilBarno, true);
		}
		else
			return false;
	}
	return true;
}*/
//---------------------------------------------------------------------------------------------------------------------

void CollectiveRulesView::AddServices(CStringArray	&ropSerUrnos)
{
	CString	olTplUrno, olTplDalo, *polAloc = 0;
	if(ropSerUrnos.GetSize() > 0)
	{
		if (omCurrRueUrno != "")
		{
			if (pomComboBox)
			{		
				if (GetSelectedTplUrno (*pomComboBox, olTplUrno, FALSE, true))
					olTplDalo = ogBCD.GetField ("TPL", "URNO", olTplUrno, "DALO");
				if (!olTplDalo.IsEmpty())
					polAloc = &olTplDalo;
			}
			ogDataSet.CreateRudFromSerList(true, ropSerUrnos, omCurrRueUrno, olTplUrno, "-1", polAloc, bmCreateGroup );
			// ogDataSet.SetAllDbarAndDear(imRuty2 > 0);
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------

void CollectiveRulesView::IniRefCB()
{
	CString olRudAloc, olAloReft, olDspTxt, olUrno;
	int ilCount, i;

	m_RefCB.ResetContent();
	omRefTab.Empty();

	if ( ogBCD.GetField("RUD_TMP", "URNO", omCurrRudUrno, "ALOC", olRudAloc ) &&
		 ogBCD.GetField("ALO", "ALOC", olRudAloc, "REFT", olAloReft ) )
	{
		if ( (olAloReft.GetLength() >= 3) && 
			 (olAloReft.Left ( 3 ) == "CIC") )
		{
			omRefTab = "CCC";		//  tabelle, die in Referen-Combobox angezeigt wird
			omRefDsp = "CICN";		//  Feld, das in Referen-Combobox angezeigt wird
			omRefVal = "URNO";		//  Feld, auf das im RUD-Satz verwiesen wird
		}
	}
	if ( !omRefTab.IsEmpty() )
	{
		CString olRudVref, olRudReft, olSelectedUrno;
		ogBCD.GetField("RUD_TMP", "URNO", omCurrRudUrno, "VREF", olRudVref );
		ogBCD.GetField("RUD_TMP", "URNO", omCurrRudUrno, "REFT", olRudReft );
		if ( ( olRudReft.GetLength() >= 8 ) && 
			 ( olRudReft.Right(4) == omRefVal ) &&
			 ( olRudReft.Left(3) == omRefTab ) )	//  gleicher Bezug
			olSelectedUrno = ogBCD.GetField(omRefTab, omRefVal, olRudVref, "URNO" );
		
		ilCount = ogBCD.GetDataCount(omRefTab);
		for (i = 0; i < ilCount; i++)
		{
			olDspTxt = ogBCD.GetField(omRefTab, i, omRefDsp);
			olUrno = ogBCD.GetField(omRefTab, i, "URNO");
			int ilPos = m_RefCB.AddString(olDspTxt);
			m_RefCB.SetItemData(ilPos, atol(olUrno));
			if ( olUrno == olSelectedUrno )
				m_RefCB.SetCurSel ( ilPos );
		}
	}
	else
	{
		ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "VREF", "" );
		ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "REFT", "" );
	}
	bool blDsp = !omRefTab.IsEmpty() ;
	EnableDlgItem ( this, IDC_COB_REF, blDsp, blDsp );
	EnableDlgItem ( this, IDC_STA_REF, blDsp, blDsp );

}
//---------------------------------------------------------------------------------------------------------------------

void CollectiveRulesView::ProcessSgrNew(RecordSet *popRecord)
{
	CCS_TRY

	CString olGridRefTab;	// reference TAB.FLDN from grid
	CString olDdxRefTab;		// reference TAB.FLDN from record sent by DDX
	CString olGroupName;		// name of static group sent by DDX
	CString olChoiceList;
	CGXStyle olStyle;
	bool	blSgrFitsActTpl = true;


	olDdxRefTab = popRecord->Values[igSgrTabnIdx];
	olGroupName = popRecord->Values[igSgrGrpnIdx];

	blSgrFitsActTpl = IsSgrForActTpl ( popRecord );
	
	// search in Conditionsgrid

	if ( imRessourceType == 0 )			//  Personal	
	{
		if ( olDdxRefTab=="PFC" )
			pomLeftGrid->ModifyAllResChoiceLists ( olGroupName, blSgrFitsActTpl, true );
		if ( olDdxRefTab=="PER" )
			pomRightGrid->ModifyAllResChoiceLists ( olGroupName, blSgrFitsActTpl, true );
	}
	if ( imRessourceType == 2 )			//  Locations	
	{
		//  alle Zeilen checken, ob dargestellte Tabelle==olDdxRefTab
		for ( ROWCOL i=1; i<=pomRightGrid->GetRowCount(); i++ )
		{
			CString olUrno, olReft, olResTable, olCodeField;
			olUrno = pomRightGrid->GetValueRowCol(i, 3);
			if ( !olUrno.IsEmpty() )
				olReft = ogBCD.GetField("RLO_TMP", "URNO", olUrno, "REFT");
			if ( ParseReftFieldEntry ( olReft, olResTable, olCodeField ) &&
				 (olResTable==olDdxRefTab) )
				pomRightGrid->ModifyResChoiceList( olGroupName, blSgrFitsActTpl, true, i );
		}
	}
	ChangeResChoiceLists ( olDdxRefTab, popRecord, true, blSgrFitsActTpl );

	CCS_CATCH_ALL	
}
//----------------------------------------------------------------------------

void CollectiveRulesView::ProcessSgrDelete(RecordSet *popRecord)
{
	CString olGridRefTab;	// reference TAB.FLDN from grid
	CString olDdxRefTab;	// reference TAB.FLDN from record sent by DDX
	CString olGroupName;	// name of static group sent by DDX
	CString olChoiceList;
	CGXStyle olStyle;


	olDdxRefTab = popRecord->Values[igSgrTabnIdx];
	olGroupName = popRecord->Values[igSgrGrpnIdx];

	if ( imRessourceType == 0 )			//  Personal	
	{
		if ( olDdxRefTab=="PFC" )
			pomLeftGrid->ModifyAllResChoiceLists ( olGroupName, false, true );
		if ( olDdxRefTab=="PER" )
			pomRightGrid->ModifyAllResChoiceLists ( olGroupName, false, true );
	}
	if ( imRessourceType == 2 )			//  Locations	
	{
		//  alle Zeilen checken, ob dargestellte Tabelle==olDdxRefTab
		for ( ROWCOL i=1; i<=pomRightGrid->GetRowCount(); i++ )
		{
			CString olUrno, olReft, olResTable, olCodeField;
			olUrno = pomRightGrid->GetValueRowCol(i, 3);
			if ( !olUrno.IsEmpty() )
				olReft = ogBCD.GetField("RLO_TMP", "URNO", olUrno, "REFT");
			if ( ParseReftFieldEntry ( olReft, olResTable, olCodeField ) &&
				 (olResTable==olDdxRefTab) )
				pomRightGrid->ModifyResChoiceList( olGroupName, false, true, i );
		}
	}
	ChangeResChoiceLists ( olDdxRefTab, popRecord, true, false );

}


//---------------------------------------------------------------------------
//			implementation of static and global functions
//---------------------------------------------------------------------------
static void IndepDDXCallback(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    CollectiveRulesView *polView = (CollectiveRulesView *)popInstance;
		
	RecordSet *polRecord = (RecordSet *) vpDataPointer;
	CString olRueUrno;

	switch ( ipDDXType )
	{	
		case RUD_TMP_NEW:
		case RUD_TMP_CHANGE:
		case RUD_TMP_DELETE:
			olRueUrno = polRecord->Values[igRudUrueIdx];
			// only process rud changes here, if rule event type is set to flight independent (EVTY=2)
			if (ogBCD.GetField("RUE", "URNO", olRueUrno, "EVTY" == CString("2")))
			{
				if (ipDDXType == RUD_TMP_NEW)
					polView->ProcessRudTmpNew(polRecord);
				if (ipDDXType == RUD_TMP_CHANGE)
					polView->ProcessRudTmpChange(polRecord);
				if (ipDDXType == RUD_TMP_DELETE)
					polView->ProcessRudTmpDelete(polRecord);
			}
			break;
		case SGR_NEW:
		case SGR_CHANGE:
			polView->ProcessSgrNew(polRecord);
			break;
		case SGR_DELETE:
			polView->ProcessSgrDelete(polRecord);
			break;
	}
}
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void CollectiveRulesView::OnDideChk() 
{
	// TODO: Add your control notification handler code here
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);

	omEditSplitMax.SetWindowText(CString(""));
	omEditSplitMin.SetWindowText(CString(""));

	if (omDideChk.GetCheck() == CHECKED)
	{
		omDideChk.SetCheck(UNCHECKED);
		olRecord.Values[igRudDideIdx] = CString("0");	// not divisable
		omEditSplitMin.EnableWindow(FALSE);
		omEditSplitMax.EnableWindow(FALSE);
	}
	else
	{
		omDideChk.SetCheck(CHECKED);
		olRecord.Values[igRudDideIdx] = CString("1");	// divisable
		omEditSplitMin.EnableWindow(TRUE);
		omEditSplitMax.EnableWindow(TRUE);
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);

	
}

void CollectiveRulesView::OnSelendokCobRef() 
{
	// TODO: Add your control notification handler code here
	long  ilRefUrno=-1;
	CString olRudReft, olRudVref, olUrno;
	int		ilSel = m_RefCB.GetCurSel();
	if ( ilSel >= 0 )
		ilRefUrno = m_RefCB.GetItemData(ilSel);
	if ( ilRefUrno > 0 )
	{
		olRudReft = omRefTab + "." + omRefVal;
		olUrno.Format ( "%ld", ilRefUrno );
		olRudVref = ogBCD.GetField(omRefTab, "URNO", olUrno, omRefVal );
	}
	ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "VREF", olRudVref );
	ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "REFT", olRudReft );
	
}


void CollectiveRulesView::OnChangeMaxc() 
{
	CString olMinc, olMaxc,olWndText;
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);
	
	omEditMinCount.GetWindowText(olMinc);
	omEditMaxCount.GetWindowText(olMaxc);
	if(igRudMaxcIdx>=0 && igRudMincIdx >= 0)
	{

		olWndText = olRecord.Values[igRudMaxcIdx];
		olMinc.TrimRight();
		olMaxc.TrimRight();
		olWndText.TrimRight();
		int ilVal = atoi(olWndText);
		int ilVal2 = atoi(olMaxc);
	   
		if(olMaxc.IsEmpty())
		{
			ilVal = -1;
		}

		if(olWndText.IsEmpty())
		{
			ilVal2 = -1;
		}

		if(ilVal2 != ilVal)
		{
			
			if(!olMaxc.IsEmpty())
			{
				if(!olMinc.IsEmpty() && atoi(olMinc) > atoi(olMaxc))
				{
					MessageBox(GetString(57346), GetString(1412), MB_OK | MB_ICONEXCLAMATION);
					omEditMaxCount.SetFocus();
					omEditMaxCount.SetSel( 0, -1 ); 
				}
				else
				{
					olRecord.Values[igRudMaxcIdx].Format("%d", atoi(olMaxc) );
				}
			}
			else
			{
				olRecord.Values[igRudMaxcIdx] = "";
			}		
		}
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);

}

void CollectiveRulesView::OnChangeMinc() 
{
	// TODO: Add your control notification handler code here
	CString olMinc, olMaxc,olWndText;
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);
	
	omEditMinCount.GetWindowText(olMinc);
	omEditMaxCount.GetWindowText(olMaxc);
	if(igRudMaxcIdx>=0 && igRudMincIdx >= 0)
	{
		olWndText = olRecord.Values[igRudMincIdx];
	
		olMinc.TrimRight();
		olMaxc.TrimRight();
		olWndText.TrimRight();
		int ilVal = atoi(olWndText);
		int ilVal2 = atoi(olMinc);


		if(olMinc.IsEmpty())
		{
			ilVal = -1;
		}

		if(olWndText.IsEmpty())
		{
			ilVal2 = -1;
		}
		if(ilVal2 != ilVal)
		{
		
			if(!olMinc.IsEmpty())
			{
				if(!olMaxc.IsEmpty() && atoi(olMaxc) < atoi(olMinc))
				{
					MessageBox(GetString(57346), GetString(1412), MB_OK | MB_ICONEXCLAMATION);
					omEditMinCount.SetFocus();
					omEditMinCount.SetSel( 0, -1 ); 
				}
				else
				{
					olRecord.Values[igRudMincIdx].Format("%d", atoi(olMinc));
				}
			}
			else
			{
				olRecord.Values[igRudMincIdx] = "";
			}
		}
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);

}

void CollectiveRulesView::OnChangeLifr() 
{
	bool blInvalidInput = false;
	CString olLifr,olLito;
	CTime olTime = CTime::GetCurrentTime();
	CTime olTimeB;
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);

	if(igRudLifrIdx>=0 && igRudLitoIdx>=0)
	{
		blInvalidInput = true;
		if(omEditLifr.GetStatus())
		{
			omEditLifr.GetWindowText(olLifr);
			olLifr.TrimRight();
			olLito = olRecord.Values[igRudLitoIdx];
			if(!olLifr.IsEmpty())
			{
				olTimeB = HourStringToDate(olLifr, olTime);
				olLifr = olTimeB.Format("%H%M");
			}
			if(olLifr.IsEmpty() || olLito.IsEmpty() || atoi(olLifr) != atoi(olLito))
			{
				olRecord.Values[igRudLifrIdx] = olLifr;
				blInvalidInput = false;
			}
		}
	}

	if(blInvalidInput)
	{
		MessageBox(GetString(57346), GetString(1412), MB_OK | MB_ICONEXCLAMATION);
		omEditLifr.SetFocus();
		omEditLifr.SetSel( 0, -1 ); 
	}
	else
	{
		ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);
	}

}

void CollectiveRulesView::OnChangeLito() 
{

	bool blInvalidInput = false;
	CString olLifr,olLito;
	CTime olTime = CTime::GetCurrentTime();
	CTime olTimeB;
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);

	if(igRudLifrIdx>=0 && igRudLitoIdx>=0)
	{
		blInvalidInput = true;
		if(omEditLito.GetStatus())
		{
			omEditLito.GetWindowText(olLito);
			olLito.TrimRight();
			olLifr = olRecord.Values[igRudLifrIdx];
			if(!olLito.IsEmpty())
			{
				olTimeB = HourStringToDate(olLito, olTime);
				olLito = olTimeB.Format("%H%M");
			}
			if(olLifr.IsEmpty() || olLito.IsEmpty() || atoi(olLifr) != atoi(olLito))
			{
				olRecord.Values[igRudLitoIdx] = olLito;
				blInvalidInput = false;
			}
		}
	}

	if(blInvalidInput)
	{
		MessageBox(GetString(57346), GetString(1412), MB_OK | MB_ICONEXCLAMATION);
		omEditLito.SetFocus();
		omEditLito.SetSel( 0, -1 ); 
	}
	else
	{
		ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);
	}

}

void CollectiveRulesView::OnChangeMintime() 
{
	// TODO: Add your control notification handler code here
	CString olSplitMin;
	int		ilValue;
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);
	
	if (omDideChk.GetCheck() == CHECKED)
	{
		omEditSplitMin.GetWindowText(olSplitMin);
		ilValue = atoi(olSplitMin);
		if ( ilValue > 0 )
			olRecord.Values[igRudMindIdx].Format("%d", (ilValue * 60) );
		else
			olRecord.Values[igRudMindIdx] = "";
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);
	
}

void CollectiveRulesView::OnChangeMaxtime() 
{
	// TODO: Add your control notification handler code here
	CString olSplitMax;
	int ilValue;
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);
	
	if (omDideChk.GetCheck() == CHECKED)
	{
		omEditSplitMax.GetWindowText(olSplitMax);
		ilValue = atoi(olSplitMax);
		if ( ilValue > 0 )
			olRecord.Values[igRudMaxdIdx].Format("%d", ilValue * 60);
		else
			olRecord.Values[igRudMaxdIdx] = "";
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);

}

void CollectiveRulesView::OnFaddChk() 
{
	// TODO: Add your control notification handler code here
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);

	if (omNoDemandsChk.GetCheck() == CHECKED)
	{
		omNoDemandsChk.SetCheck(UNCHECKED);
		olRecord.Values[igRudFaddIdx] = CString("1");	// not active
	}
	else
	{
		omNoDemandsChk.SetCheck(CHECKED);
		olRecord.Values[igRudFaddIdx] = CString("0");	// active
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);

}

void CollectiveRulesView::OnFondChk() 
{
	// TODO: Add your control notification handler code here
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);

	if (omFondChk.GetCheck() == CHECKED)
	{
		omFondChk.SetCheck(UNCHECKED);
		olRecord.Values[igRudFondIdx] = CString("0");	// not active
	}
	else
	{
		omFondChk.SetCheck(CHECKED);
		olRecord.Values[igRudFondIdx] = CString("1");	// active
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);

}


void CollectiveRulesView::EnableControls()
{
	int ilSplitState = omDideChk.GetCheck(); 
	BOOL blEnable = !omCurrRudUrno.IsEmpty();
	
	omDideChk.EnableWindow(blEnable);
	omEditSplitMax.EnableWindow(blEnable && (ilSplitState== CHECKED) );
	omEditSplitMin.EnableWindow(blEnable && (ilSplitState== CHECKED) );

	omEditMinCount.EnableWindow(blEnable && (igRudMincIdx>=0));
	omEditMaxCount.EnableWindow(blEnable && (igRudMaxcIdx>=0));
	omEditLifr.EnableWindow(blEnable && (igRudLifrIdx>=0));
	omEditLito.EnableWindow(blEnable && (igRudLitoIdx>=0));

	omFondChk.EnableWindow(blEnable);
	omNoDemandsChk.EnableWindow(blEnable);
	m_AlocCB.EnableWindow(blEnable);
	m_RefCB.EnableWindow(blEnable);

}
 int test=0;
void CollectiveRulesView::DisplayRudInControls()
{
	
	//--- get current rud record
	CString olDide, olMaxd, olMind, olLifr, olLito, olMaxc, olMinc, olCheck;
	int ilValue;
	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD_TMP"));
	if ( !omCurrRudUrno.IsEmpty() && 
		 ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRudRecord) )
	{
		olDide = olRudRecord.Values[igRudDideIdx];
		omFondChk.SetCheck ( olRudRecord.Values[igRudFondIdx]=="1" );
		CString olFadd = olRudRecord.Values[igRudFaddIdx];
		omNoDemandsChk.SetCheck ( olFadd != "1" );
		if ( igRudMincIdx>=0)		// mu� auf Server mit entsprechendem Feld in RUD getestet werden
			olMinc = olRudRecord.Values[igRudMincIdx];
		if ( igRudMaxcIdx>=0)		// mu� auf Server mit entsprechendem Feld in RUD getestet werden
			olMaxc = olRudRecord.Values[igRudMaxcIdx];
		if ( igRudLifrIdx>=0)		// mu� auf Server mit entsprechendem Feld in RUD getestet werden
			olLifr = olRudRecord.Values[igRudLifrIdx];
		if ( igRudLitoIdx>=0)		// mu� auf Server mit entsprechendem Feld in RUD getestet werden
			olLito = olRudRecord.Values[igRudLitoIdx];	
        
		if(ccicheck)
		{
            
	    	int	igRudReflIdx = ogBCD.GetFieldIndex("RUD", "REFL");
	    	if ( igRudReflIdx>=0)	
		      olCheck=olRudRecord.Values[igRudReflIdx];	
		}
	}
	
	
	cflag.SetCheck(olCheck=="1");

	if (olCheck=="1")
	{
	 	
	    omEditLifr.SetReadOnly(TRUE);
		omEditLito.SetReadOnly(TRUE);

		omEditMinCount.SetReadOnly(TRUE);

	   start.EnableWindow(TRUE);
       endt.EnableWindow(TRUE);
	
	}

	else
	{
	   	

    	omEditLifr.SetReadOnly(FALSE);
		omEditLito.SetReadOnly(FALSE);

		omEditMinCount.SetReadOnly(FALSE);
	
	   start.EnableWindow(FALSE);
       endt.EnableWindow(FALSE);
	

		
	}
	
	omDideChk.SetCheck(olDide=="1");
	if ( olDide=="1" )
	{
		olMaxd = olRudRecord.Values[igRudMaxdIdx];
		ilValue = atoi (olMaxd) / 60;
		if ( ilValue > 0 )
			olMaxd.Format ( "%d", ilValue );
		else 
			olMaxd.Empty();
		olMind = olRudRecord.Values[igRudMindIdx];
		ilValue = atoi (olMind) / 60;
		if ( ilValue > 0 )
			olMind.Format ( "%d", ilValue );
		else
			olMind.Empty();
	}


	
	if(ccicheck)
	{
	
	    CString st, end;
		int from = ogBCD.GetFieldIndex("RUD", "REFR");
		int to = ogBCD.GetFieldIndex("RUD", "RETO");
		st = olRudRecord.Values[from];
		end = olRudRecord.Values[to];
		start.SetWindowText ( st );
		endt.SetWindowText ( end );
	}
	
	

	omEditSplitMax.SetWindowText ( olMaxd );
	omEditSplitMin.SetWindowText ( olMind );

	omEditMinCount.SetWindowText ( olMinc );
	omEditMaxCount.SetWindowText ( olMaxc );
	omEditLifr.SetWindowText ( olLifr );
	omEditLito.SetWindowText ( olLito );



}


void CollectiveRulesView::ccistart ()
{
  CString ccifrom;
   
	
  int from = ogBCD.GetFieldIndex("RUD", "REFR");
  
  RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
 ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);


		
			start.GetWindowText(ccifrom);

			ccifrom.TrimRight();
			
			if (ccifrom.GetLength()==5 && ccifrom[2]==':')
			{
		       ccifrom.Format("%c%c%c%c",ccifrom[0],ccifrom[1],ccifrom[3],ccifrom[4]);
			}
				

					
			olRecord.Values[from] = ccifrom;

	

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);


}

void CollectiveRulesView::cciend()
{
  
	
		
	CString cciend;
   
	
    int end = ogBCD.GetFieldIndex("RUD", "RETO");
  
    RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);

	endt.GetWindowText(cciend);

	cciend.TrimRight();	
	
	if (cciend.GetLength()==5 && cciend[2]==':')
	{
	      cciend.Format("%c%c%c%c",cciend[0],cciend[1],cciend[3],cciend[4]);
	}
	
	
	
	olRecord.Values[end] = cciend;

	

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);


}


void CollectiveRulesView::OnCheckChange()
{

//	MessageBox("checkchange");

   CString checkflag="";
   int nStatus = ((CButton*)GetDlgItem(IDC_CHECK1))->GetCheck();

   int flag = ogBCD.GetFieldIndex("RUD", "REFL");
    RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);

   
   
   
   if (nStatus==1)
   {
    
	      checkflag="1";
   
        omEditLifr.EnableWindow(FALSE);
        omEditLito.EnableWindow(FALSE);
		omEditMinCount.EnableWindow(FALSE);
        start.EnableWindow(TRUE);
        endt.EnableWindow(TRUE);
   
   
   }
   else 
   {
        checkflag="0";


		   omEditLifr.EnableWindow(TRUE);
        omEditLito.EnableWindow(TRUE);
		 omEditMinCount.EnableWindow(TRUE);

		 start.EnableWindow(FALSE);
        endt.EnableWindow(FALSE);

		omEditLifr.SetReadOnly(FALSE);
		omEditLito.SetReadOnly(FALSE);

		omEditMinCount.SetReadOnly(FALSE);

		
   
   }


 olRecord.Values[flag] = checkflag;

 ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);


}