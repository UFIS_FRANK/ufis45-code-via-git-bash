// GUILng.h: Schnittstelle f�r die Klasse CGUILng.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GUILNG_H__0D0861A4_C1E3_11D3_8FCD_0050DA1CAD13__INCLUDED_)
#define AFX_GUILNG_H__0D0861A4_C1E3_11D3_8FCD_0050DA1CAD13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "Rostering.h"
#include <CedaBasicData.h>
//#include "CCSGlobl.h"


class CGUILng : public CString  
{
private:
	static const int		GUI_MAX_RES;			// current max res ID
	static const int		MAX_TEMPSTR;
	static const CString	RES_DESCRIPTION;		// the Part after this is ignored
	static const CString	APPL_CODE;				// Part of String = this is an Appl-relevant string
	static const CString	RES_FORMAT;				// for future use, the class is only searching for '%'
	static const CString	TAB_NAME;
	static const CString	APPL_FIELD;
	static const CString	CDAT_FIELD;
	static const CString	COCO_FIELD;
	static const CString	HOPO_FIELD;
	static const CString	LSTU_FIELD;
	static const CString	STAT_FIELD;
	static const CString	STID_FIELD;
	static const CString	STRG_FIELD;
	static const CString	TXID_FIELD;
	static const CString	URNO_FIELD;
	static const CString	USEC_FIELD;
	static const CString	USEU_FIELD;
	static const CString	StoreInDB;
	static const CString	ClearDB;
	static const CString	StoreNotInDB;
	static const CString	UpdateRangeInDB;
	static const CString	DatDir;
	static const CString	FileExt;
	static const int		NEW;
	static const int		UPD;
	static const int		DEL;

	CGUILng();
	void InitData();
	void HdlTxtDat();
	UINT HdlAllRes();
	bool HdlDBRes(CString olTxt, UINT nID, int Switch);
	bool UpdateIDRange(UINT nIDmin,UINT nIDmax);
	CString GetResString(UINT nID);
	int IsResInDB(UINT nID);
	int GetFirstMatch(int i, int j, int k);

	void CGUILngTest();	// for testing make it public
	static int TxtBCFunc (BcStruct *prpBc);

	static CGUILng* _theOne;
	bool bm_UseRes;
	bool bm_UseDef;
	int	imPos_APPL,imPos_CDAT,imPos_COCO,imPos_HOPO,imPos_LSTU,imPos_STAT,
		imPos_STID,imPos_STRG,imPos_TXID,imPos_URNO,imPos_USEC,imPos_USEU;
	static CString	om_APPL;
	static CString om_CurrLng;
	CString om_Param;
	CString om_User;
	CString om_HoPo;
	bool bm_GlobalInit;

public:
	bool MemberInit(CString *OutError, CString User, CString Appl, CString HoPo, CString Lng, CString DBParam);
	const bool GetString(CString *OutStrg, CString RefField1, CString RefField2, CString RefValue1, CString RefValue2);
	static CGUILng* TheOne();
	virtual ~CGUILng();

	const CString GetString(UINT nID);
	const CString GetString(CString olTxt);
	const CString GetString(CString RefField1, CString RefField2, CString RefValue1, CString RefValue2);

//	void SetStoreInDB(CString olParam);
	const CString GetStoreInDB();
//	void SetLanguage(CString Lng);
	const CString GetLanguage();
};

#endif // !defined(AFX_GUILNG_H__0D0861A4_C1E3_11D3_8FCD_0050DA1CAD13__INCLUDED_)

