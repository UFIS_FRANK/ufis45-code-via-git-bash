// RegelEditor.cpp : implementation file
//

#include <stdafx.h>
#include <RegelWerk.h>
#include <RegelWerkView.h>

#include <CedaBasicData.h>
#include <CedaObject.h>
#include <CCSGlobl.h>
#include <BasicData.h>

#include <Dialog_VorlagenEditor.h>
#include <RulesList.h>
#include <Dialog_ExpressionEditor.h>
#include <GhsList.h>
#include <CCSTime.h>
#include <ProgressDlg.h>
#include <InfoDlg.h>
#include <ComboBoxBar.h>

#include <time.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <process.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif


//----------------------------------------------------------------------------
//		globals, defines and declaration of static or global functions
//----------------------------------------------------------------------------
int StringComp(const RecordSet** ppRecord1, const RecordSet** ppRecord2);

static void ProcessDetailCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


//----------------------------------------------------------------------------
//						creation, construction, destruction
//----------------------------------------------------------------------------

IMPLEMENT_DYNCREATE(CRegelEditorFormView, CRulesFormView)


CRegelEditorFormView::CRegelEditorFormView(): 
	CRulesFormView(CRegelEditorFormView::IDD)
{
	CCS_TRY
	
	//{{AFX_DATA_INIT(CRegelEditorFormView)
	//}}AFX_DATA_INIT

	//--- initializing some variables (there's some more in OnInitialUpdate)
	bmNoComboChange = false;
	bmRelative = true;
	omCurrRueUrno = "";	// currently active rule

	// grids
	pomRotGrid = new CGridFenster(this);
	pomArrGrid = new CGridFenster(this);
	pomDepGrid = new CGridFenster(this);

	// sortable comboboxes
	pomRotCombo = new CGXComboBoxWnd(pomRotGrid);
	pomArrCombo = new CGXComboBoxWnd(pomRotGrid);
	pomDepCombo = new CGXComboBoxWnd(pomRotGrid);

	//-- load bitmaps 
	omVL2RBmp.LoadBitmap("VL2R_small");
	omHL2RBmp.LoadBitmap("HL2R_small");
	omVR2LBmp.LoadBitmap("VR2L_small");
	omHR2LBmp.LoadBitmap("HR2L_small");
	
	//--- DDX register (the fixed ones that we always need)
	// templates
	ogDdx.Register(this, TPL_NEW, CString("RegelWerkView"), CString("TPL New"), ProcessDetailCf);
	ogDdx.Register(this, TPL_CHANGE, CString("RegelWerkView"), CString("TPL Change"), ProcessDetailCf);
	ogDdx.Register(this, TPL_DELETE, CString("RegelWerkView"), CString("TPL Delete"), ProcessDetailCf);
	// static groups (head)	
	ogDdx.Register(this, SGR_NEW, CString("RegelWerkView"), CString("SGR New"), ProcessDetailCf);
	ogDdx.Register(this, SGR_CHANGE, CString("RegelWerkView"), CString("SGR Change"), ProcessDetailCf);
	ogDdx.Register(this, SGR_DELETE, CString("RegelWerkView"), CString("SGR Delete"), ProcessDetailCf);
	// static group members
	ogDdx.Register(this, SGM_NEW, CString("RegelWerkView"), CString("SGM New"), ProcessDetailCf);
	ogDdx.Register(this, SGM_CHANGE, CString("RegelWerkView"), CString("SGM Change"), ProcessDetailCf);
	ogDdx.Register(this, SGM_DELETE, CString("RegelWerkView"), CString("SGM Delete"), ProcessDetailCf);
	// rule events
	//ogDdx.Register(this, RUE_NEW, CString("RegelWerkView"), CString("RUE New"), ProcessDetailCf);
	//ogDdx.Register(this, RUE_CHANGE, CString("RegelWerkView"), CString("RUE Change"), ProcessDetailCf);
	//ogDdx.Register(this, RUE_DELETE, CString("RegelWerkView"), CString("RUE Delete"), ProcessDetailCf);
	// dynamic groups
	// ogDdx.Register(this, DGR_NEW, CString("RegelWerkView"), CString("DGR New"), ProcessDetailCf);
	// ogDdx.Register(this, DGR_CHANGE, CString("RegelWerkView"), CString("DGR Change"), ProcessDetailCf);
	ogDdx.Register(this, DGR_DELETE, CString("RegelWerkView"), CString("DGR Delete"), ProcessDetailCf);

	bmOutbound = FALSE;
	bmInbound  = FALSE;

	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

CRegelEditorFormView::~CRegelEditorFormView()
{
	CCS_TRY

	
	delete pomRotGrid;
	delete pomArrGrid;
	delete pomDepGrid;

	delete pomRotCombo;
	delete pomArrCombo;
	delete pomDepCombo;
	

	CCS_CATCH_ALL
}



//------------------------------------------------------------
//				message map
//------------------------------------------------------------
BEGIN_MESSAGE_MAP(CRegelEditorFormView, CRulesFormView)
//	ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
	//{{AFX_MSG_MAP(CRegelEditorFormView)
	ON_BN_CLICKED(IDC_INBOUND_RB, OnInbound)
	ON_BN_CLICKED(IDC_OUTBOUND_RB, OnOutbound)
	ON_BN_CLICKED(IDC_TURNAROUND_RB, OnTurnaround)
	ON_MESSAGE(GRID_MESSAGE_ENDEDITING, OnEndEditing)
	ON_MESSAGE(GRID_MESSAGE_BUTTONCLICK, OnGridButton)
	ON_BN_CLICKED(RULED_RBT_ABS, OnAbsolute)
	ON_BN_CLICKED(RULED_RBT_REL, OnRelative)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_FSPL_CHB, OnFsplChb)
	ON_MESSAGE(WM_BCADD,OnBcAdd)
	ON_COMMAND(ID_FILE_SAVE, OnSaveRule)
	ON_BN_CLICKED(IDC_EXCLUDE_CHB, OnExcludeChb)
	//}}AFX_MSG_MAP 
END_MESSAGE_MAP()



LONG CRegelEditorFormView::OnBcAdd(UINT wParam, LONG /*lParam*/)
{
	ogBcHandle.GetBc(wParam);
	return TRUE;
}

   
//----------------------------------------------------------------------------
//						data exchange
//----------------------------------------------------------------------------
void CRegelEditorFormView::DoDataExchange(CDataExchange* pDX)
{
	CCS_TRY

	CRulesFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRegelEditorFormView)
	DDX_Control(pDX, IDS_ROTATION, m_Sta_Rotation);
	DDX_Control(pDX, IDS_PRSN, m_Sta_Prsn);
	DDX_Control(pDX, IDS_PRNA, m_Sta_Prna);
	DDX_Control(pDX, IDS_ANKUNFT, m_Sta_Arrival);
	DDX_Control(pDX, IDS_ABFLUG, m_Sta_Departure);
	DDX_Control(pDX, IDC_TURN_FRAME, m_Fra_Turn);
	DDX_Control(pDX, IDC_SPLIT_FRAME, m_Fra_Split);
	DDX_Control(pDX, IDC_INBOUND_RB, m_RBtn_Inbound);
	DDX_Control(pDX, IDC_OUTBOUND_RB, m_RBtn_Outbound);
	DDX_Control(pDX, IDC_TURNAROUND_RB, m_RBtn_Turnaround);
	DDX_Control(pDX, RULED_RBT_ABS, m_RBtn_Absolute);
	DDX_Control(pDX, RULED_RBT_REL, m_RBtn_Relative);
	DDX_Control(pDX, IDC_MINTIME_STATIC, m_Sta_MinTime);
	DDX_Control(pDX, IDC_MINTIME_EDIT, m_Edt_MinTime);
	DDX_Control(pDX, IDC_MAXT_STATIC2, m_Sta_Maxt2);
	DDX_Control(pDX, IDC_MAXT_STATIC, m_Sta_Maxt);
	DDX_Control(pDX, IDC_IOT_FRAME, m_Fra_IOT);
	DDX_Control(pDX, IDC_FSPL_CHB, m_Chb_Fspl);
	DDX_Control(pDX, IDC_EXCLUDE_CHB, m_Chb_Excl);
	DDX_Control(pDX, IDC_EXCL_FRAME, m_Fra_Excl);
	DDX_Control(pDX, IDC_ACTIVE_CHB, m_Chb_Acti);
	DDX_Control(pDX, IDC_ACTI_FRAME, m_Fra_Acti);
	DDX_Control(pDX, IDC_MAXT, m_MaxtEdit);
	DDX_Control(pDX, ID_EDIT_RULENAME, m_RuleNameEdit);
	DDX_Control(pDX, IDC_EDIT_RULESHORTNAME, m_RuleShortNameEdit);
	//}}AFX_DATA_MAP
		
	CCS_CATCH_ALL
}



//----------------------------------------------------------------------------
//						RegelEditor diagnostics
//----------------------------------------------------------------------------
#ifdef _DEBUG
void CRegelEditorFormView::AssertValid()const
{
	CRulesFormView::AssertValid();
}
//----------------------------------------------------------------------------

void CRegelEditorFormView::Dump(CDumpContext& dc)const
{
	CRulesFormView::Dump(dc);
}
#endif //_DEBUG



//----------------------------------------------------------------------------
//						RegelEditor message handlers
//----------------------------------------------------------------------------

//-----------------------------------------
//		initial update
//-----------------------------------------
void CRegelEditorFormView::OnInitialUpdate()
{
	CCS_TRY
	
	CRulesFormView::OnInitialUpdate();
	SetStaticTexts();

	//--- subscribe to broadcasts
	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pcgConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0 || stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		//MWO/RRO
		//ogCommHandler.RegisterBcWindow(this);
		ogBcHandle.StartBc(); 
		//END MWO/RRO
	}
	else
	{
		ogCommHandler.RegisterBcWindow(this);
	}

	//---  set the pointers to the mainframe wnd items
    pomFrameWnd = static_cast< CMainFrame* >(AfxGetMainWnd()); 
	pomComboBox = pomFrameWnd->m_wndToolBar.pomComboBox;
	pomStatusBar = &pomFrameWnd->m_wndStatusBar;

	//--- fill combobox with template names
	pomFrameWnd->FillTemplateCombo();
	/*CString olName, olUrno, olTpst, olTmp;
	int  ilRecordCount = ogBCD.GetDataCount("TPL");
	for(int i = 0; i < ilRecordCount; i++)
	{
 		olName = olTmp = ogBCD.GetField("TPL", i, "TNAM");
        olUrno = ogBCD.GetField("TPL", i, "URNO");
		olTpst =  ogBCD.GetField("TPL", i,"TPST");

		if (olTpst == "0") // template deactivated
				olName += CString(" ") + GetString(303);

		if (( olTpst != '2') && (pomComboBox->FindStringExact(-1, olTmp) == CB_ERR) &&
			(pomComboBox->FindStringExact(-1, olTmp) == CB_ERR))
		{	
			int ilPos = pomComboBox->InsertString(0, olName);
		}
	}*/
	pomComboBox->SetCurSel(0);
	
	//--- set rule state to 'disabled' (default when creating new rules !)
	m_Chb_Acti.SetCheck(CHECKED);

    //---    Initialize grids
	BOOL blErr = pomRotGrid->SubclassDlgItem(IDC_ROT_LIST, this);
	     blErr = pomArrGrid->SubclassDlgItem(IDC_ARR_LIST, this);
  		 blErr = pomDepGrid->SubclassDlgItem(IDC_DEP_LIST, this);

	pomRotGrid->Initialize();
    pomArrGrid->Initialize();
	pomDepGrid->Initialize();
	
	//--- one-time initialization of grids
	pomRotGrid->IniLayoutForConditions();
	pomArrGrid->IniLayoutForConditions();
	pomDepGrid->IniLayoutForConditions();

	//--- update whole window
	pomFrameWnd->MoveWindow(0, 0, 1024, 768);	// necessary for Gantt to get right size

	//--- initialize gantt
	MakeTimeScale();
	MakeRelativeTimeScale();
	MakeViewer();
	MakeGantt();
	m_HScroll.SetScrollRange(0, 12, FALSE);
	
	//--- hide absolute timescale, show relative one
	pomTimeScale->ShowWindow(SW_HIDE);
	pomRelTimeScale->ShowWindow(SW_SHOW);
	
	//--- get sec state
	SetBdpsState();
	
	//--- fill mandatory fields with YELLOW background
	m_RuleNameEdit.SetBKColor(YELLOW);
	m_RuleShortNameEdit.SetBKColor(YELLOW);
	m_MaxtEdit.SetBKColor(YELLOW);
	
	//--- bitmap for evaluation order 
	SetEvorBitmap();

	//--- some preparation for controls
	m_RBtn_Relative.SetCheck(CHECKED);
	m_RBtn_Absolute.SetCheck(UNCHECKED);
	m_MaxtEdit.SetTypeToInt(0, 9999);
	m_RuleNameEdit.SetTextLimit(1,64,false);
	m_RuleShortNameEdit.SetTextLimit(1,14,false);
	m_Chb_Fspl.SetCheck(UNCHECKED);
	m_Edt_MinTime.SetTypeToInt(0, 999);
	m_Edt_MinTime.EnableWindow(FALSE);
	
	//--- calc window size, turn scrolling off
	GetParentFrame()->RecalcLayout();
	CSize olSize = CSize(1024, 768);
	SetScaleToFitSize(olSize);

	// set main window's caption
	//pomFrameWnd->SetWindowText(GetString(1414));

	
	//--- prepare for input of new rule 
	OnNewRule();	// call this _AT THE END_ of OnInitialUpdate() (It will need a lot of things initialized before)

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------

void CRegelEditorFormView::OnPaint()
{
	CWnd::OnPaint();
}
//-----------------------------------------------------------------------------------------

/*void CRegelEditorFormView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	long llTotalMin = 720;
    int ilPos = 0;
    
    switch (nSBCode)
    {
        case SB_LINELEFT:	// scroll one hour left
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            // ilPos = m_HScroll.GetScrollPos() - int (60 * 1000L / llTotalMin);
			ilPos = m_HScroll.GetScrollPos() - 60;
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
			{
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));
			}

            pomTimeScale->SetDisplayStartTime(omTSStartTime);
            pomTimeScale->Invalidate(TRUE);
			pomRelTimeScale->SetDisplayStartTime(omTSStartTime);
            pomRelTimeScale->Invalidate(TRUE);
			pomViewer->ChangeViewTo(omCurrRueUrno);
            m_HScroll.SetScrollPos(ilPos, TRUE);
			break;
        
        case SB_LINERIGHT:		// scroll one hour right
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
            if (llTotalMin != 0)
			{
				//ilPos = m_HScroll.GetScrollPos() + int (60 * 1000L / llTotalMin);
				ilPos = m_HScroll.GetScrollPos() + 60;
				if (ilPos >= 1000)
				{
					ilPos = 1000;
					SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
				}
				else
					SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));

				pomTimeScale->SetDisplayStartTime(omTSStartTime);
				pomTimeScale->Invalidate(TRUE);
				pomRelTimeScale->SetDisplayStartTime(omTSStartTime);
				pomRelTimeScale->Invalidate(TRUE);
				pomViewer->ChangeViewTo(omCurrRueUrno);
				m_HScroll.SetScrollPos(ilPos, TRUE);
			}
			break;
        
        case SB_PAGELEFT:
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
			if (llTotalMin != 0)
			{
				// ilPos = m_HScroll.GetScrollPos() - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
				ilPos = m_HScroll.GetScrollPos() - (int)(omTSDuration.GetTotalMinutes() / 2);
				if (ilPos <= 0)
				{
					ilPos = 0;
					SetTSStartTime(omStartTime);
				}
				else
					SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

				pomTimeScale->SetDisplayStartTime(omTSStartTime);
				pomTimeScale->Invalidate(TRUE);
				pomRelTimeScale->SetDisplayStartTime(omTSStartTime);
				pomRelTimeScale->Invalidate(TRUE);
				pomViewer->ChangeViewTo(omCurrRueUrno);
				m_HScroll.SetScrollPos(ilPos, TRUE);
			}
			break;
        
        case SB_PAGERIGHT:
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            //ilPos = m_HScroll.GetScrollPos() + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
			ilPos = m_HScroll.GetScrollPos() + (int)(omTSDuration.GetTotalMinutes() / 2);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

            pomTimeScale->SetDisplayStartTime(omTSStartTime);
            pomTimeScale->Invalidate(TRUE);
			pomRelTimeScale->SetDisplayStartTime(omTSStartTime);
            pomRelTimeScale->Invalidate(TRUE);
			pomViewer->ChangeViewTo(omCurrRueUrno);
            m_HScroll.SetScrollPos(ilPos, TRUE);
			break;
        
        case SB_THUMBTRACK:
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
            pomTimeScale->SetDisplayStartTime(omTSStartTime);
            pomTimeScale->Invalidate(TRUE);
			pomRelTimeScale->SetDisplayStartTime(omTSStartTime);
            pomRelTimeScale->Invalidate(TRUE);
            m_HScroll.SetScrollPos(nPos, TRUE);
			pomViewer->ChangeViewTo(omCurrRueUrno);
			break;

        case SB_THUMBPOSITION:	// the thumb was just released?
			return;
////////////////////////////////////////////////////////////////////////
        case SB_ENDSCROLL :
			break;
    }
	
	CRect olRect;
	m_HScroll.GetWindowRect(olRect);
	// ScreenToClient(olRect);
	InvalidateRect(olRect);
}*/
//-----------------------------------------------------------------------------------------

void CRegelEditorFormView::OnFsplChb() 
{
	if (m_Chb_Fspl.GetCheck() == CHECKED)
	{
		m_Edt_MinTime.EnableWindow(TRUE);
		m_Edt_MinTime.SetBKColor(YELLOW);
	}
	else
	{
		m_Edt_MinTime.EnableWindow(FALSE);
		m_Edt_MinTime.SetBKColor(WHITE);
	}
}
//-----------------------------------------------------------------------------------------

void CRegelEditorFormView::OnSelEndOK()
{
	 bmNoComboChange = false;
	 OnNewRule();
	 SetEvorBitmap();
}     
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------


// for exclusion rules: hide services list button and 
// disable condition grids
void CRegelEditorFormView::OnExcludeChb() 
{
	if (m_Chb_Excl.GetCheck() == CHECKED)
		SetExclusionRule(true);
	else
		SetExclusionRule(false);
}
//-----------------------------------------------------------------------------------------

void CRegelEditorFormView::OnInbound() 
{
	if ((BOOL)bmInbound != m_RBtn_Inbound.GetCheck())
	{
		if (CheckIOTChange(1))
			SetInbound();
		else
			SetPreviousIOT();
	}
}
//-----------------------------------------------------------------------------------------

void CRegelEditorFormView::OnOutbound() 
{
	if ((BOOL)bmOutbound != m_RBtn_Outbound.GetCheck())
	{
   		if(CheckIOTChange(2))
			SetOutbound();
		else
			SetPreviousIOT();
	}
}
//-----------------------------------------------------------------------------------------

void CRegelEditorFormView::OnTurnaround() 
{
	if ((BOOL)bmTurnaround != m_RBtn_Turnaround.GetCheck())
	{
		SetTurnaround();
	}
}
//-----------------------------------------------------------------------------------------

void CRegelEditorFormView::OnAbsolute() 
{
	m_RBtn_Relative.SetCheck(UNCHECKED);
	m_RBtn_Absolute.SetCheck(CHECKED);
	
	bmRelative = false;
	pomTimeScale->ShowWindow(SW_SHOW);
	
	// take care of correct drawing of window
	pomFrameWnd->UpdateViewArea();
}
//--------------------- --------------------------------------------------------------------

void CRegelEditorFormView::OnRelative() 
{
	m_RBtn_Relative.SetCheck(CHECKED);
	m_RBtn_Absolute.SetCheck(UNCHECKED);

	// take care of correct drawing of window
	bmRelative = true;
	CRect olRect;
	pomGantt->pomTimeScale->GetWindowRect(olRect);

	ScreenToClient(olRect);
	InvalidateRect(olRect);
	pomTimeScale->ShowWindow(SW_HIDE);
}

//-----------------------------------------------------------------------------------------

void CRegelEditorFormView::OnTemplateEditor()
{
	CCS_TRY

		
	//--- Call Template Editor
	CString olSelectedTpl;
	int ilPos = pomComboBox->GetCurSel();
	if ( ilPos >= 0 )
		pomComboBox->GetLBText( ilPos, olSelectedTpl );
	CDialog_TemplateEditor	olEditor;
    olEditor.DoModal();
        
  //--- delete old entries from combo box
	pomComboBox->ResetContent();

  //--- refill combo box
	pomFrameWnd->FillTemplateCombo();
	
	//--- Selection in ComboBox neu setzen
	/* ilPos = pomComboBox->FindStringExact( -1, olSelectedTpl ) ; */
	ilPos = pomFrameWnd->FindTplInComboBox( olSelectedTpl );

	pomComboBox->SetCurSel( max(ilPos,0) );
	SetEvorBitmap();

	//--- initialize tables
	if ( ilPos < 0 )	//  zuletzt gew�hltes Template wurde gel�scht
		OnNewRule ();
	else
	{
		/*ClearTables();
		InitializeTable(pomRotGrid, "FLDT");
		InitializeTable(pomArrGrid, "FLDA");
		InitializeTable(pomDepGrid, "FLDD");*/
		if ( !bmNewFlag )
			OnLoadRule(omCurrRueUrno, olSelectedTpl, false);
		else
			OnNewRule ();
	}	
	ogChangeInfo.SetChangeState(RULE_CHECK);
	
	CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::OnLoadRule(CString opRueUrno, CString opTemplateName, bool bpNoComboChange)
{
	CCS_TRY
	

	bmIsLoading = true;
	bmNewFlag = false;
	bmNoComboChange = bpNoComboChange;
	omCurrRueUrno = opRueUrno;


	// close dialogs
	if (::IsWindow(pomGantt->pomDetailDlg->GetSafeHwnd()))
	{
		pomGantt->pomDetailDlg->Destroy();
	}

	if (::IsWindow(pomGantt->pomConnectionDlg->GetSafeHwnd()))
	{
		pomGantt->pomConnectionDlg->Destroy();
	}

	 // some initialization stuff
	bmTurnaround = false;
	bmInbound = false;
	bmOutbound = false;
	m_RBtn_Turnaround.SetCheck(UNCHECKED);
	m_RBtn_Inbound.SetCheck(UNCHECKED);
	m_RBtn_Outbound.SetCheck(UNCHECKED);

	
   //--- get record
	   RecordSet olRecord(ogBCD.GetFieldCount("RUE"));
	   ogBCD.GetRecord("RUE", "URNO", omCurrRueUrno, olRecord);

   //--- set names
	   CString olName = olRecord.Values[igRueRunaIdx];
       CString olShortName = olRecord.Values[igRueRusnIdx];
       m_RuleNameEdit.SetInitText(olName);
       m_RuleShortNameEdit.SetInitText(olShortName);

   //--- get flag and radio button values
		int ilRust = atoi(olRecord.Values[igRueRustIdx]);
		int ilEvty = atoi(olRecord.Values[igRueEvtyIdx]);
		int ilRuty = atoi(olRecord.Values[igRueRutyIdx]);
		int ilExcl = atoi(olRecord.Values[igRueExclIdx]);
		int ilFspl = atoi(olRecord.Values[igRueFsplIdx]);
		int ilFisu = atoi(olRecord.Values[igRueFisuIdx]);

   //--- get data strings
		CString olDataStringRotation;
		CString olDataStringArrival;
		CString olDataStringDeparture;
		olDataStringRotation = olRecord.Values[igRueEvttIdx];
		olDataStringArrival = olRecord.Values[igRueEvtaIdx];
		olDataStringDeparture = olRecord.Values[igRueEvtdIdx];

   //--- initialize tables
	   if (ilRuty == 0)		// single rule
	   {
			pomRotGrid->SetGridEnabled(true);
			m_Chb_Excl.EnableWindow(TRUE);
			m_RBtn_Inbound.EnableWindow(TRUE);
			m_RBtn_Outbound.EnableWindow(TRUE);
			m_RBtn_Turnaround.EnableWindow(TRUE);
	   }

	   if (bmNoComboChange == false)
	   {
		   ClearTables();
		   InitializeTable(pomRotGrid, "FLDT");
		   InitializeTable(pomArrGrid, "FLDA");
		   InitializeTable(pomDepGrid, "FLDD");
	   }
		
	   pomRotGrid->LockUpdate();
	   pomArrGrid->LockUpdate();
	   pomDepGrid->LockUpdate();

	   pomRotGrid->ResetConditionsTable();
	   pomArrGrid->ResetConditionsTable();
	   pomDepGrid->ResetConditionsTable();

	   SetRBtnSC(ilRuty);

	   if (ilEvty == 2)
	   {
		   SetOutbound();
	   }
	   else if (ilEvty == 1)
	   {
		   SetInbound();
	   }
	   else
	   {
		   SetTurnaround();
	   }

	   // MNE TEST
	   /*int ilRowCount = pomRotGrid->GetRowCount();
	   ilRowCount = pomArrGrid->GetRowCount();
	   ilRowCount = pomDepGrid->GetRowCount();*/
	   // MNE TEST

	   ReConstructString(pomRotGrid, olDataStringRotation);
	   ReConstructString(pomArrGrid, olDataStringArrival);
	   ReConstructString(pomDepGrid, olDataStringDeparture);
		
		
	   pomRotGrid->LockUpdate(FALSE);
	   pomArrGrid->LockUpdate(FALSE);
	   pomDepGrid->LockUpdate(FALSE);
	   pomRotGrid->Redraw();
	   pomArrGrid->Redraw();
	   pomDepGrid->Redraw();



   //--- set view data
	   CString olMaxt;
	   int ilMaxt;
	   if ( !olRecord.Values[igRueMaxtIdx].IsEmpty() )
	   {
		   ilMaxt = atoi(olRecord.Values[igRueMaxtIdx]);
		   olMaxt.Format("%d", ilMaxt);	// Remove whitespace from String
	   }
	   m_MaxtEdit.SetInitText(olMaxt);

		m_Chb_Acti.SetCheck(! ilRust);	// 0 = deactivated = CHECKED
		m_Chb_Excl.SetCheck(ilExcl);
		SetExclusionRule(ilExcl);
		
		
		if (ilFspl == 1)
		{
			m_Chb_Fspl.SetCheck(CHECKED);
			m_Edt_MinTime.EnableWindow(TRUE);
			m_Edt_MinTime.SetInitText(olRecord.Values[igRueMistIdx]);
		}
		else
		{
			m_Chb_Fspl.SetCheck(UNCHECKED);
			m_Edt_MinTime.EnableWindow(FALSE);
			m_Edt_MinTime.SetInitText(CString(""));
		}
		
		// gantt
		ogDataSet.FillLogicRudObject(omCurrRueUrno);
		ogDataSet.FillLogicRpfObject();
		ogDataSet.FillLogicRpqObject();
		ogDataSet.FillLogicRloObject();
		ogDataSet.FillLogicReqObject();
		
		pomViewer->ResetLinkColorIndex(); // reset color index for linked bars
		pomViewer->ChangeViewTo(omCurrRueUrno);
		SaveBarDisplayPositions();

		pomGantt->SetScrollPos(SB_VERT, 0, TRUE);
		pomGantt->SetTopIndex(0);
		m_HScroll.SetScrollPos(SB_HORZ, TRUE);
		pomGantt->Invalidate();

		bmIsLoading = false;
		ogChangeInfo.SetCurrRueUrno(omCurrRueUrno);

	CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::OnNewRule()
{
	CCS_TRY


	if (::IsWindow(pomGantt->pomDetailDlg->GetSafeHwnd()))
	{
		pomGantt->pomDetailDlg->Destroy();
	}

	if (::IsWindow(pomGantt->pomConnectionDlg->GetSafeHwnd()))
	{
		pomGantt->pomConnectionDlg->Destroy();
	}

	//--- set new-flag
 	bmNewFlag = true;

	//--- reset controls
    m_RuleNameEdit.SetInitText("");
    m_RuleShortNameEdit.SetInitText("");
	m_MaxtEdit.SetInitText("");
	m_Edt_MinTime.SetInitText("");
	m_Edt_MinTime.EnableWindow(FALSE);
	m_Chb_Excl.SetCheck(UNCHECKED);
	m_Chb_Fspl.SetCheck(UNCHECKED);
	m_Chb_Acti.SetCheck(CHECKED);
	SetExclusionRule(false);
	

	// set radio buttons
	bmSCRule = true;

	bmSingle = true;
	bmTurnaround = true;
	OnTurnaround();
	  	
	//--- get new urno
     omCurrRueUrno.Format("%d", ogBCD.GetNextUrno());
	 pomViewer->SetCurrentRuleUrno(omCurrRueUrno);

    //--- initialize tables
    pomRotGrid->LockUpdate();
	pomArrGrid->LockUpdate();
	pomDepGrid->LockUpdate();

    //InitializeTable(pomRotGrid, "FLDT");
    //InitializeTable(pomArrGrid, "FLDA");
    //InitializeTable(pomDepGrid, "FLDD");
	if (bmNoComboChange == false)
	{
		ClearTables();
		InitializeTable(pomRotGrid, "FLDT");
		InitializeTable(pomArrGrid, "FLDA");
		InitializeTable(pomDepGrid, "FLDD");
	}
	else
	{
	   pomRotGrid->ResetConditionsTable();
	   pomArrGrid->ResetConditionsTable();
	   pomDepGrid->ResetConditionsTable();
	}

	 pomRotGrid->LockUpdate(FALSE);
	 pomArrGrid->LockUpdate(FALSE);
	 pomDepGrid->LockUpdate(FALSE);
	 pomRotGrid->Redraw();
	 pomArrGrid->Redraw();
	 pomDepGrid->Redraw();

	 //--- reset gantt
	 ogDataSet.FillLogicRudObject(CString("-1"));
	 ogDataSet.ResetLogicResourceTables ();
	 pomViewer->ResetLinkColorIndex();  // reset color index for linked bars

	 pomViewer->ChangeViewTo(omCurrRueUrno);
	 CRect rect;
	 pomGantt->GetWindowRect(rect);
	 ScreenToClient(rect);
	 pomGantt->SetScrollPos(SB_VERT, 0, TRUE);
	 m_HScroll.SetScrollPos(SB_HORZ, TRUE);
	 InvalidateRect(rect);
	
	 // set change info so that changes can be watched	
	 ogChangeInfo.SetCurrRueUrno(omCurrRueUrno);
	 
	 bgIsValSaved = false;

	 CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::OnCopy()
{

	CCS_TRY

	bmNewFlag = true;
	CString olOldRueUrno;
	
	olOldRueUrno = omCurrRueUrno;
	long llUrno = ogBCD.GetNextUrno();
	omCurrRueUrno.Format("%d", llUrno);
	
	m_RuleShortNameEdit.SetInitText(CString(""));
	bool blOk = ogDataSet.OnCopyRue(olOldRueUrno, omCurrRueUrno);

	pomViewer->ChangeViewTo(omCurrRueUrno);

	bgIsValSaved = false;

	// inform ChangeInfo that rule is copied
	ogChangeInfo.SetCopiedRuleFlag();
	ogChangeInfo.SetCurrRueUrno(omCurrRueUrno);


	CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------------

bool CRegelEditorFormView::OnSaveRule()
{
	bool blOk;
	CCS_TRY
       

	SetFocus();  

	UpdateData();

//--- break conditions
	// Are name and short name given correctly ?
	if (m_RuleNameEdit.GetStatus() == false)
	{
		MessageBox(GetString(1411), GetString(1412), MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	if (m_RuleShortNameEdit.GetStatus() == false)
	{
		MessageBox(GetString(1457), GetString(1412), MB_OK | MB_ICONEXCLAMATION);
		return false;
	} 
	    
	// Has validity been specified ?
	/*if (pomFrameWnd->rmValData.IsFilled == false)
	{
		MessageBox(GetString(1450), GetString(1412), MB_OK | MB_ICONEXCLAMATION);
		return false;
	}*/

	// In case of a turnaround rule: Is m_Maxt given and is it < 9999 ?
	CString olCheckMaxt;
	m_MaxtEdit.GetWindowText(olCheckMaxt);
	if (bmSingle && bmTurnaround && (m_MaxtEdit.GetStatus() == false || olCheckMaxt.IsEmpty() == TRUE))
	{
		MessageBox(GetString(1423), GetString(1412), MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	// In case of a new rule: Is the given code a unique one or do we at least have different VAL periods ?
	CString olName;
	CString olShortName;
	m_RuleNameEdit.GetWindowText(olName);
	m_RuleShortNameEdit.GetWindowText(olShortName);
	if (bmNewFlag == true)
	{
		int ilCount = ogBCD.GetDataCount("RUE");
		for(int i = 0; i < ilCount; i++)
		{
			CString olShortNameInDB = ogBCD.GetField("RUE", i, "RUSN");
			CString olStatusInDB = ogBCD.GetField("RUE", i, "RUST");

			//  archived rules are allowed to have the short name of the new rule
			if((olShortNameInDB == olShortName) && (olStatusInDB != "2"))
			{
				VALDATA rlOldValidity;
				CString olOldUrno = ogBCD.GetField("RUE", i, "URNO");
				if (!LoadValidity(olOldUrno, rlOldValidity) || DoValiditiesOverlap(rlOldValidity, pomFrameWnd->rmValData))
				{
					AfxMessageBox(GetString(164));
					return false;
				}
			}
		}
	}
	
	// if split flag is set: has a minimum split time been specified ?
	CString olMinSplitTime;
	if (bmSingle == true)
	{
		if (m_Chb_Fspl.GetCheck() == CHECKED)
		{
			m_Edt_MinTime.GetWindowText(olMinSplitTime);
			if (m_Edt_MinTime.GetStatus() == false || olMinSplitTime.IsEmpty() == TRUE)
			{
				MessageBox(GetString(1474), GetString(1412), MB_OK | MB_ICONEXCLAMATION);
				return false;
			}
		}
	}
	

//--- create recordset
	// Urno der aktuell in FormView->ComboBox ausgew�hlten Vorlage
	CString olTplUrno;
	if ( !GetSelectedTplUrno ( *pomComboBox, olTplUrno, FALSE, true ) )
		return false;
	
	// make wait cursor 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));


	 // create string part ROTATION
		CString olStrRotation;
		ConstructString(pomRotGrid, olStrRotation);

	 // create string part ARRIVAL
		CString olStrArrival;
		if (bmOutbound == false)
		{
			ConstructString(pomArrGrid, olStrArrival);
		}

	 // create string part DEPARTURE
		CString olStrDeparture;
		if (bmInbound == false)
		{
			ConstructString(pomDepGrid, olStrDeparture);
		}
		
	// check length of condition strings
	   if (olStrArrival.GetLength() > 800)
	   {
			olStrArrival = olStrArrival.Left(799);
			olStrArrival += '~';
	   }
	   if (olStrDeparture.GetLength() > 800)
	   {
			olStrDeparture = olStrDeparture.Left(799);
			olStrDeparture += '~';
	   }
	   if (olStrRotation.GetLength() > 400)
	   {
			olStrRotation = olStrRotation.Left(399);
			olStrRotation += '~';
	   }
	
	// Make string for RmsHdl
	   CString olConditions = ConstructEVRM();

   // Fill Record
	   int ilSize = ogBCD.GetFieldCount("RUE");
	   RecordSet olRecord(ilSize);
	   
	   if (bmNewFlag == false)
		   ogBCD.GetRecord ("RUE","URNO",omCurrRueUrno,olRecord);
	   
	   olRecord.Values[igRueUrnoIdx] = omCurrRueUrno;	   
	   olRecord.Values[igRueApplIdx] = CString("RULE_AFT");	   
	   olRecord.Values[igRuePrioIdx] = ConstructPrioString();	// priority string
	   olRecord.Values[igRueEvtaIdx] = olStrArrival;			// string for arrival grid
	   olRecord.Values[igRueEvtdIdx] = olStrDeparture;			// string for departure grid
	   olRecord.Values[igRueEvttIdx] = olStrRotation;			// string for rotation grid
	   olRecord.Values[igRueEvrmIdx] = olConditions;			// string for RmsHdl
	   olRecord.Values[igRueRunaIdx] = olName;					// name
	   olRecord.Values[igRueRusnIdx] = olShortName;				// short name
	   olRecord.Values[igRueUtplIdx] = olTplUrno;				// template urno
	   olRecord.Values[igRueLstuIdx] = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");
	   olRecord.Values[igRueUseuIdx] = pcgUser;

	   // following two fields might be changed further down in case of internal copy
	   
	// FISU (flag IsUsed)
	/*
	   bool blIsUsed = false;
	   CString olFisu;
	   olFisu = ogBCD.GetField("RUE", "URNO", omCurrRueUrno, "FISU");
	   if (olFisu.Left(1) == CString("1"))
	   {
			blIsUsed = true;
	   }
	   else
	   {
		   olRecord.Values[igRueFisuIdx] = CString("0");
		   blIsUsed = false;
	   }
	*/
	//  UARC
	olRecord.Values[igRueUarcIdx] = ogBCD.GetField("RUE", "URNO", omCurrRueUrno, "UARC");	//hag990901
	   	
	// RUST (rule status)
	if (m_Chb_Acti.GetCheck() == CHECKED)
	{
			olRecord.Values[igRueRustIdx] = CString("0"); // deactivated
	}
	else
	{
			olRecord.Values[igRueRustIdx] = CString("1"); // activated
	}

	// MAXT (maximum ground time)
	CString olMaxt;
	if (bmTurnaround)
	{
			m_MaxtEdit.GetWindowText(olMaxt);
	}
	else
	{
			olMaxt = CString(" ");
	}
	while (olMaxt.GetLength(	) < 4)
	{
			olMaxt = CString(" ") + olMaxt;
	}
	olRecord.Values[igRueMaxtIdx] = olMaxt;					// maximum ground time 
	
	// FSPL (split turnaround at day change ?)
	if (m_Chb_Fspl.GetCheck() == CHECKED && bmTurnaround)
	{
		olRecord.Values[igRueFsplIdx] = CString("1");
		if (olMinSplitTime.IsEmpty() == TRUE || olMinSplitTime == CString(" "))
		{
			olMinSplitTime = CString("0");
		}
		olRecord.Values[igRueMistIdx] = olMinSplitTime;
	}
	else
	{
		olRecord.Values[igRueFsplIdx] = CString("0");
		olRecord.Values[igRueMistIdx] = CString(" ");
	}
		
	// EXCL (exclude flag)
	if (m_Chb_Excl.GetCheck() == CHECKED)
	{
		olRecord.Values[igRueExclIdx] = CString("1");
	}
	else
	{
		olRecord.Values[igRueExclIdx] = CString("0");
	}

	// EVTY (event type)
	if (bmTurnaround == true)
	{
		olRecord.Values[igRueEvtyIdx] = CString("0");	// turnaround
	}
	else if (bmOutbound == true)
	{
		olRecord.Values[igRueEvtyIdx] = CString("2");	// outbound
	}
	else if (bmInbound == true)
	{
	   olRecord.Values[igRueEvtyIdx] = CString("1");		// inbound
	}
		
	// RUTY (rule type: single / collective / flight independent)
	if (bmSCRule == true)
	{
		if (bmSingle == true)
		{
			olRecord.Values[igRueRutyIdx] = CString("0");	// single rule
		}
		else
		{
			olRecord.Values[igRueRutyIdx] = CString("1");	// collective rule
		}
	}
	else
	{
		olRecord.Values[igRueEvtyIdx] = CString("2");		// flight independent rule
	}


	   
	//--- write data to database
	blOk = SaveRule( olRecord );
	
	// make regular cursor 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));


	CCS_CATCH_ALL

	return blOk;
}
//------------------------------------------------------------------------------------------------------------------------

/*		wird z.Z. nicht benutzt
void CRegelEditorFormView::OnDelete()
{
	//--- exception handling
	CCS_TRY

	 if(!omCurrRueUrno.IsEmpty())
	 {
		BOOL blErr   = ogBCD.DeleteRecord("RUE", "URNO", omCurrRueUrno, TRUE);
		OnNewRule();
	 }
	
	 //--- set omCurrRueUrno to Urno of currently selected rule
	 int ilSel = pomComboBox->GetCurSel();
	 if (ilSel != -1)
	 {
		omCurrRueUrno.Format("%d", pomComboBox->GetItemData(ilSel));
	 }
	 else
	 {
		omCurrRueUrno.Empty();
	 }

	//--- exception handling
	CCS_CATCH_ALL
}
*/

//------------------------------------------------------------------------------------------------------------------------
/*
LONG CRegelEditorFormView::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
    CString olStr;
    int ipGroupNo = (int) HIWORD(lParam);
    int ipLineNo = (int) LOWORD(lParam);
	 CTime olTime = CTime((time_t) lParam);


    switch (wParam)
    {
        case UD_INSERTLINE:
        {
			pomGantt->InsertString(ipLineNo, "");
			pomGantt->RepaintItemHeight(ipLineNo);
			break;
		}
        case UD_UPDATELINE:
		{
			if (pomViewer->omLines[ipLineNo].Arrows.GetSize() < 1)
			{
				pomGantt->RepaintItemHeight(ipLineNo);
			}
			else
			{
				pomGantt->RepaintVerticalScale(ipLineNo);
				pomGantt->RepaintGanttChart(ipLineNo);
			}
			break;
		}
        case UD_DELETELINE:
		{
            pomGantt->DeleteString(ipLineNo);
			break;
		}
		case UD_RESETCONTENT:
		{
			pomGantt->ResetContent();
			break;
		}
		default:
		{
			break;
		}
    }

    return 0L;
}*/
//------------------------------------------------------------------------------------------------------------------------

/*  not used, message handled by CMainFrame
void CRegelEditorFormView::OnGroupBtn() 
{
	//----------------------------------------
	// calling external exe (static grouping)
	//		The binary needs its own section in ceda.ini !
	//----------------------------------------
	
	char pclGroupingPath[500];
    GetPrivateProfileString("REGELWERK", "GROUPING", "NOTFOUND", pclGroupingPath, sizeof pclGroupingPath, pcgConfigPath);

	if(!strcmp(pclGroupingPath,"NOTFOUND") || strlen(pclGroupingPath) <= 0)
	{
		MessageBox(GetString(1463), GetString(AFX_IDS_APP_TITLE), MB_ICONSTOP);
	}
	else
	{
		_flushall(); // flush all streams before system call
		char *pclArgs[4];
		char pclRunTxt[256];
		pclArgs[0] = "child";
		sprintf(pclRunTxt,"%s,%s,%s,%s",ogAppName,pcgTableExt,pcgUser,pcgPasswd);
		pclArgs[1] = pclRunTxt;
		pclArgs[2] = NULL;
		pclArgs[3] = NULL;
		_spawnv(_P_NOWAIT,pclGroupingPath,pclArgs);
	}
}
*/

//------------------------------------------------------------------------------------------------------------------------

BOOL CRegelEditorFormView::OnHelpInfo(HELPINFO* pHelpInfo)
{
	AfxGetApp()->WinHelp(HID_TOPIC1);

	return CRulesFormView::OnHelpInfo(pHelpInfo);
}
//------------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::OnLeistungen() 
{
	UpdateData(TRUE);
	GhsList *polSerList = NULL;

	if (omCurrRueUrno != "" && checkIsNumber(omCurrRueUrno))
	{
		polSerList = new GhsList(this, LBS_MULTIPLESEL);
	}
	else
	{
		// Do not open services list in case no rule has been chosen.
		// Otherwise there'd be no Rule Urno for DataSet::CreateRudFromSerList() to use (RUD.URUE).
		oglogData = "get invalid rule urno <" + omCurrRueUrno + ">";
		WriteInlogFile();
		MessageBox(GetString(1408), GetString(1409), MB_OK | MB_ICONEXCLAMATION);
		bgIsServicesOpen = false;
	}
}
//------------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::OnEndEditing(WPARAM wParam, LPARAM lParam)
{
	//-----------------
	// when leaving a cell, this function checks for text in the other columns of the row and 
	// removes it when necessary (There's only one column per row to be filled)
	//-----------------

	CGridFenster *polGrid = (CGridFenster*) wParam;

	CELLPOS *prlCellPos = (CELLPOS*) lParam;

	ROWCOL ilCol = (ROWCOL) prlCellPos->Col;
	ROWCOL ilRow = (ROWCOL) prlCellPos->Row;
	CString olCellText ;
	olCellText = polGrid->GetValueRowCol(ilRow, ilCol);

	if (ilRow != 0 && olCellText.IsEmpty() == FALSE && olCellText != CString(" "))
	{
		//  Reset tooltips
		polGrid->SetStyleRange(CGXRange(ilRow, 1, ilRow, 5), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, ""));
		
		if (ilCol == SIMPLE_VAL)
		{
			polGrid->SetValueRange(CGXRange(ilRow, STATIC_GRP), CString(""));
			polGrid->SetValueRange(CGXRange(ilRow, DYNAMIC_GRP), CString(""));

			if (polGrid->FormatSpecial(ilRow, ilCol, olCellText))
			{
				polGrid->SetValueRange(CGXRange(ilRow, ilCol), olCellText);
			}
			
		}

		if (ilCol == STATIC_GRP)
		{
			polGrid->SetValueRange(CGXRange(ilRow, SIMPLE_VAL),  CString(""));
			polGrid->SetValueRange(CGXRange(ilRow, DYNAMIC_GRP),  CString(""));
		}
		
		if (ilCol == DYNAMIC_GRP)
		{
			polGrid->SetValueRange(CGXRange(ilRow, SIMPLE_VAL),  CString(""));
			polGrid->SetValueRange(CGXRange(ilRow, STATIC_GRP),  CString(""));
		}
	}

	polGrid->MarkInValid(ilRow, ilCol, olCellText);
}
//------------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::OnGridButton(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
	

	CString olTplUrno;

	if (GetSelectedTplUrno(*pomComboBox, olTplUrno, FALSE, true) == false)
	{
		return;
	}


	CGridFenster *polGrid = (CGridFenster*) wParam;
	CELLPOS *prlCellPos = (CELLPOS*) lParam;
	if (!polGrid || !prlCellPos)
	{
		return;
	}

	ROWCOL ilCol = (ROWCOL) prlCellPos->Col;
	ROWCOL ilRow = (ROWCOL) prlCellPos->Row;
	
	polGrid->OnConditionsGridButton(olTplUrno, ilRow, ilCol);


	 CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------


// Everything that must be done every time the grids are re-initialized goes in this function
// All the one-time initialization is placed in IniLayoutForConditions ()
bool CRegelEditorFormView::InitializeTable(CGridFenster *popGrid, const CString&  opWhichPart)
{
	CCS_TRY

		
	// make wait cursor 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	// Grids f�llen mit Daten aus TPL  je nach Auswahl in der ComboBox der Toolbar
	CString olUrno;

	if (GetSelectedTplUrno(*pomComboBox, olUrno, FALSE))
	{
		CString olTPLString;
		olTPLString = ogBCD.GetField("TPL", "URNO", olUrno, opWhichPart);
		popGrid->IniGridFromTplString(olTPLString, olUrno);
	}

	// make regular cursor
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));


	CCS_CATCH_ALL		
	
	return true;
}
//------------------------------------------------------------------------------------------------------------------------


void  CRegelEditorFormView::ClearTables() 
{
	CCS_TRY

	// empty grids
	pomRotGrid->ClearTable();
	pomArrGrid->ClearTable();
	pomDepGrid->ClearTable();

	CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------
/*
void  CRegelEditorFormView::ConstructString(CGridFenster* popGrid, CString &ropResult) 
{
	//--- exception handling
	CCS_TRY

    int ilCount = popGrid->GetRowCount();

    for(int i = 0; i < ilCount; i++)
	{   
		CString  olTable  = popGrid->GetValueRowCol(i + 1, 7); // Tabelle
		CString  olSimpleVal = popGrid->GetValueRowCol(i + 1, 2); 
		CString  olDynGrp = popGrid->GetValueRowCol(i + 1, 3); 
		CString  olStatGrp = popGrid->GetValueRowCol(i + 1, 5); 

		if(olSimpleVal.IsEmpty() == FALSE && olSimpleVal != CString(" "))
		{
			popGrid->UnFormatSpecial ( i + 1, 2, olSimpleVal ); 
            ropResult += CString("0.") + olTable + CString("=") + olSimpleVal + CString(";");
		}
		else if(olDynGrp.IsEmpty() == FALSE && olDynGrp != CString(" "))
		{
            ropResult += CString("1.") + olTable + CString("=") + olDynGrp + CString(";");
		}
		else if(olStatGrp.IsEmpty() == FALSE && olStatGrp != CString(" "))
		{
            ropResult += CString("2.") + olTable + CString("=") + olStatGrp + CString(";");
		}
	}

	// remove ";" from end of string
	if (ropResult.Right(1) == CString(";"))
	{
		ropResult = ropResult.Left(ropResult.GetLength() - 1);
	}

	//--- exception handling
	CCS_CATCH_ALL
}
*/
//------------------------------------------------------------------------------------------------------------------
/*
void  CRegelEditorFormView::ReConstructString(CGridFenster* popGrid, CString &ropString) 
{
	//--- exception handling
	CCS_TRY

      //--- Zeilen in der Tabelle
      int ilRowCount = popGrid->GetRowCount();

	  //--- String zerlegen 
	  CStringArray olDataArray;
	  int ilData = ExtractItemList(ropString, &olDataArray, ';');

	  //--- more data than rows in grid ? Uuups !!!
	  if(ilRowCount < ilData)
	  {
		  AfxMessageBox(GetString(165));
		  return;
	  }

	  //--- write data to grid
	  int ilPos = -1;
	  CString olData;
	  CString olColumn;
	  CString olValue;
	  CString olFieldName;
	  CString olRowContents;
      for(int i = 0; i < ilData; i++)
	  {   
		   olData = olDataArray[i];
		   if (olData.IsEmpty() == FALSE && olData != CString(" "))
		   {
			   ilPos = olData.Find("=");
			   if(ilPos > 0)
			   {
				   olColumn = olData.Left(1);
				   olFieldName = olData.Mid(2,8);
				   olValue = olData.Right(olData.GetLength() - ilPos - 1);
				   
				   for (int ilRow = 1; ilRow <= ilRowCount; ilRow++)
				   {
						olRowContents = popGrid->GetValueRowCol(ilRow, 7);
						if (olFieldName == olRowContents)
						{
							if(atoi(olColumn) == 0)
							{
								popGrid->FormatSpecial ( ilRow, 2, olValue );
								popGrid->SetValueRange(CGXRange(ilRow, 2), olValue);
								//popGrid->DisplayInfo ( ilRow, 2, true );
								popGrid->SetToolTipForValue ( ilRow, 2, olValue );
								popGrid->MarkInValid ( ilRow, 2, olValue );
							}
							if(atoi(olColumn) == 1)
							{
								popGrid->SetValueRange(CGXRange(ilRow, 3), olValue);
								//popGrid->DisplayInfo ( ilRow, 3, true );
								popGrid->SetToolTipForValue ( ilRow, 3, olValue );
								popGrid->MarkInValid ( ilRow, 3, olValue );
							}
						    if(atoi(olColumn) == 2)
							{
								popGrid->SetValueRange(CGXRange(ilRow, 5), olValue);
								//popGrid->DisplayInfo ( ilRow, 5, true );
								popGrid->SetToolTipForValue ( ilRow, 5, olValue );
								popGrid->MarkInValid ( ilRow, 5, olValue );
							}
							break;	//  conventions !? Mmh...ain't it faster this way !?
					   }
				   }
			   }
		   }
	  }

	  //--- exception handling
	CCS_CATCH_ALL
}
*/
//------------------------------------------------------------

CString  CRegelEditorFormView::ConstructEVRM() 
{
	CString olReturn;
	
	CCS_TRY

	// The field name strings are stored in the hidden column 7. 
	// We use SOH (ASCII 0x01) to separate parts of a condition and STX (ASCII 0x02) to separate several conditions

	CString olSimple;
	CString olStatic;
	CString olDynamic;
	CString olTmp;
	CString olRefStr;
	CString olBaseStr;

	int ilRotSize = pomRotGrid->GetRowCount();
	int ilArrSize = pomArrGrid->GetRowCount();
	int ilDepSize = pomDepGrid->GetRowCount();
	
//--- rotation grid
	for (int i = 0; i < ilRotSize; i++)
	{
		olRefStr = pomRotGrid->GetValueRowCol( i + 1, 8);
		olBaseStr = pomRotGrid->GetValueRowCol(i + 1, 7);
		
		if ((olRefStr.IsEmpty() == TRUE) || (olRefStr == CString(".")))
		{
			olRefStr = olBaseStr;
		}

		olRefStr.Replace('.', SOH);				// replace dot by ASCII 0x01 ("SOH")
		olBaseStr.Replace('.', SOH);			// replace dot by ASCII 0x01 ("SOH")
		//--- simple values
		olTmp = pomRotGrid->GetValueRowCol(i + 1, 2);
		if (!olTmp.IsEmpty() && olTmp != CString(" "))
		{
			olSimple += olBaseStr;	
			olSimple += SOH;
			olSimple += CString("0");	// 0 = field from rotation grid (turnaround)
			olSimple += SOH;
			olSimple += CString("0");	// 0 = simple condition
			olSimple += SOH;
			olSimple += CString("=");	// operator (=, <>, <=, >=, IN)
			olSimple += SOH;
			olSimple += olTmp;			// 
			olSimple += SOH;
			olSimple += olRefStr;
			/*PRF6475:   dot must not be replaced in the complete string, but only in TABLE.FIELD 
			olSimple.Replace('.', SOH);		// replace dot by ASCII 0x01 ("SOH")	*/
			olSimple += STX;
		}
		
		// dynamic groups
		olTmp = pomRotGrid->GetValueRowCol(i + 1, 4);
		if (!olTmp.IsEmpty() && olTmp != CString(" "))
		{
			olDynamic += olBaseStr;	
			olDynamic += SOH;
			olDynamic += CString("0");	// 0 = field from rotation grid (turnaround)
			olDynamic += SOH;
			olDynamic += CString("1");	// 1 = dynamic group
			olDynamic += SOH;
			olDynamic += CString("IN");
			olDynamic += SOH;
			olDynamic += olTmp;
			olDynamic += SOH;
			olDynamic += olRefStr;
			/*PRF6475:   dot must not be replaced in the complete string, but only in TABLE.FIELD 
			olDynamic.Replace('.', SOH);		// replace dot by ASCII 0x01 ("SOH")	*/
			olDynamic += STX;
		}

		// static groups
		olTmp = pomRotGrid->GetValueRowCol(i + 1, 3);
		if (!olTmp.IsEmpty() && olTmp != CString(" "))
		{
			olStatic += olBaseStr;
			olStatic += SOH;					// add SOH to end of string
			olStatic += CString("0");	// 0 = field from rotation grid
			olStatic += SOH;
			olStatic += CString("2");	// 2 = static group
			olStatic += SOH;
			olStatic += CString("IN");
			olStatic += SOH;
			olStatic += olTmp;
			olStatic += SOH;
			olStatic += olRefStr;
			/*PRF6475:   dot must not be replaced in the complete string, but only in TABLE.FIELD 
			olStatic.Replace('.', SOH);		// replace dot by ASCII 0x01 ("SOH")	*/
			olStatic += STX;
		}
	}

//--- arrival grid
	for (i = 0; i < ilArrSize && !bmOutbound; i++)
	{
		olRefStr = pomArrGrid->GetValueRowCol(i + 1, 8);
		olBaseStr = pomArrGrid->GetValueRowCol(i + 1, 7);
	
		if ((olRefStr.IsEmpty() == TRUE) || (olRefStr == CString(".")))
		{
			olRefStr = olBaseStr;
		}

		olRefStr.Replace('.', SOH);				// replace dot by ASCII 0x01 ("SOH")
		olBaseStr.Replace('.', SOH);			// replace dot by ASCII 0x01 ("SOH")
		// simple values
		olTmp = pomArrGrid->GetValueRowCol(i + 1, 2);
		if (!olTmp.IsEmpty() && olTmp != CString(" "))
		{
			olSimple += olBaseStr;
			olSimple += SOH;
			olSimple += CString("1");	// field from arrival grid
			olSimple += SOH;
			olSimple += CString("0");	// 0 = simple condition
			olSimple += SOH;
			olSimple += CString("=");
			olSimple += SOH;
			olSimple += olTmp;
			olSimple += SOH;
			olSimple += olRefStr;
			/*PRF6475:   dot must not be replaced in the complete string, but only in TABLE.FIELD 
			olSimple.Replace('.', SOH);		// replace dot by ASCII 0x01 ("SOH")	*/
			olSimple += STX;
		}
		
		// dynamic groups
		olTmp = pomArrGrid->GetValueRowCol(i + 1, 4);
		if (!olTmp.IsEmpty() && olTmp != CString(" "))
		{
			olDynamic += olBaseStr;
			olDynamic += SOH;
			olDynamic += CString("1");	// field from arrival grid
			olDynamic += SOH;
			olDynamic += CString("1");	// 1 = dynamic group
			olDynamic += SOH;
			olDynamic += CString("IN");
			olDynamic += SOH;
			olDynamic += olTmp;
			olDynamic += SOH;
			olDynamic += olRefStr;
			/*PRF6475:   dot must not be replaced in the complete string, but only in TABLE.FIELD 
			olDynamic.Replace('.', SOH);		// replace dot by ASCII 0x01 ("SOH")	*/
			olDynamic += STX;
		}

		// static groups
		olTmp = pomArrGrid->GetValueRowCol(i + 1, 3);
		if (!olTmp.IsEmpty() && olTmp != CString(" "))
		{
			olStatic += olBaseStr;
			olStatic += SOH;
			olStatic += CString("1");	// field from arrival grid
			olStatic += SOH;
			olStatic += CString("2");	// 2 = static group
			olStatic += SOH;
			olStatic += CString("IN");
			olStatic += SOH;
			olStatic += olTmp;
			olStatic += SOH;
			olStatic += olRefStr;
			/*PRF6475:   dot must not be replaced in the complete string, but only in TABLE.FIELD 
			olStatic.Replace('.', SOH);		// replace dot by ASCII 0x01 ("SOH")	*/
			olStatic += STX;

		}
	}

//--- departure grid
	for (i = 0; i < ilDepSize && !bmInbound; i++)
	{
		olRefStr = pomDepGrid->GetValueRowCol( i + 1, 8);
		olBaseStr = pomDepGrid->GetValueRowCol(i + 1, 7);
		
		if ((olRefStr.IsEmpty() == TRUE) || (olRefStr == CString(".")))
		{
			olRefStr = olBaseStr;
		}

		olRefStr.Replace('.', SOH);				// replace dot by ASCII 0x01 ("SOH")
		olBaseStr.Replace('.', SOH);			// replace dot by ASCII 0x01 ("SOH")
		// simple values
		olTmp = pomDepGrid->GetValueRowCol(i + 1, 2);
		if (!olTmp.IsEmpty() && olTmp != CString(" "))
		{
			olSimple += olBaseStr;
			olSimple += SOH;
			olSimple += CString("2");	// field from departure grid
			olSimple += SOH;
			olSimple += CString("0");	// 0 = simple condition
			olSimple += SOH;
			olSimple += CString("=");
			olSimple += SOH;
			olSimple += olTmp;
			olSimple += SOH;
			olSimple += olRefStr;
			/*PRF6475:   dot must not be replaced in the complete string, but only in TABLE.FIELD 
			olSimple.Replace('.', SOH);		// replace dot by ASCII 0x01 ("SOH")	*/
			olSimple += STX;
		}
		
		// dynamic groups
		olTmp = pomDepGrid->GetValueRowCol(i + 1, 4);
		if (!olTmp.IsEmpty() && olTmp != CString(" "))
		{
			olDynamic += olBaseStr;
			olDynamic += SOH;
			olDynamic += CString("2");	// field from departure grid
			olDynamic += SOH;
			olDynamic += CString("1");	// 1 = dynamic group
			olDynamic += SOH;
			olDynamic += CString("IN");
			olDynamic += SOH;
			olDynamic += olTmp;
			olDynamic += SOH;
			olDynamic += olRefStr;
			/*PRF6475:   dot must not be replaced in the complete string, but only in TABLE.FIELD 
			olDynamic.Replace('.', SOH);	// replace dot by ASCII 0x01 ("SOH")	*/
			olDynamic += STX;
		}

		// static groups
		olTmp = pomDepGrid->GetValueRowCol(i + 1, 3);
		if (!olTmp.IsEmpty() && olTmp != CString(" "))
		{
			olStatic += olBaseStr;
			olStatic += SOH;
			olStatic += CString("2");	// field from departure grid
			olStatic += SOH;
			olStatic += CString("2");	// 2 = static group
			olStatic += SOH;
			olStatic += CString("IN");
			olStatic += SOH;
			olStatic += olTmp;
			olStatic += SOH;
			olStatic += olRefStr;
			/*PRF6475:   dot must not be replaced in the complete string, but only in TABLE.FIELD 
			olStatic.Replace('.', SOH);		// replace dot by ASCII 0x01 ("SOH")	*/
			olStatic += STX;
		}
	}

	olReturn = olSimple + olDynamic + olStatic;		// build return string
	olReturn = olReturn.Left(olReturn.GetLength() - 1);		// remove STX from end of string

	//--- exception handling
	CCS_CATCH_ALL

	return olReturn;
}
//------------------------------------------------------------

CString CRegelEditorFormView::ConstructPrioString()
{
	CString olReturn;
	int ilRow = 0;

	CCS_TRY


//--- get evaluation order from TPLTAB
	CString olTplUrno;
	int ilEvalOrder = 1;
	
	if (GetSelectedTplUrno(*pomComboBox, olTplUrno, FALSE))
	{
		ilEvalOrder = atoi(ogBCD.GetField("TPL", "URNO", olTplUrno, "EVOR"));
	}
	

//--- construct rotation part of string (omit header line)
	int ilRotCount = (int)pomRotGrid->GetRowCount();
	for (ilRow = 1; ilRow <= ilRotCount; ilRow++)
	{
		olReturn += pomRotGrid->GetLinePrio(ilRow, 5);
	}


//--- construct arrival and departure part of string
	
	int ilArrCount = (int)pomArrGrid->GetRowCount();
	int ilDepCount = (int)pomDepGrid->GetRowCount();
	int ilMaxCount = max(ilArrCount, ilDepCount);

	switch (ilEvalOrder)
	{
		case 1:			// vertically left to right (left table first, then right table)
			for (ilRow = 1; ilRow <= ilArrCount; ilRow++)
			{
				olReturn += pomArrGrid->GetLinePrio(ilRow, 5);
			}
			for (ilRow = 1; ilRow <= ilDepCount; ilRow++)
			{
				olReturn += pomDepGrid->GetLinePrio(ilRow, 5);
			}
			break;

		case 2:			// horizontally left to right (line by line, starting upper left cell)
			ilRow = 1;
			while (ilRow <= ilMaxCount)
			{
				if (ilRow <= ilArrCount)
				{
					olReturn += pomArrGrid->GetLinePrio(ilRow, 5);
				}

				if (ilRow <= ilDepCount)					
				{
					olReturn += pomDepGrid->GetLinePrio(ilRow, 5);
				}
				ilRow++;
			}
			break;

		case 3:			// vertically right to left (right table first, then left table)
			for (ilRow = 1; ilRow <= ilDepCount; ilRow++)
			{
				olReturn += pomDepGrid->GetLinePrio(ilRow, 5);
			}
			for (ilRow = 1; ilRow <= ilArrCount; ilRow++)
			{
				olReturn += pomArrGrid->GetLinePrio(ilRow, 5);
			}
			break;

		case 4:			// horizontally left to right (line by line, starting upper right cell)
			ilRow = 0;
			while (ilRow <= ilMaxCount)
			{
				if (ilRow <= ilDepCount)
				{
					olReturn += pomDepGrid->GetLinePrio(ilRow, 5);
				}
				if (ilRow <= ilArrCount)
				{
					olReturn += pomArrGrid->GetLinePrio(ilRow, 5);
				}
				ilRow++;
			}
			break;

		default:
			ogLog.Trace("DEBUG", "[RegelWerkView::ConstructPrioString]  Invalid value for evaluation order (%d)", ilEvalOrder);
			break;
	}


	CCS_CATCH_ALL

	return olReturn;
}
//------------------------------------------------------------
/*
bool CRegelEditorFormView::CheckIfLineEmpty(CGridFenster *popGrid, int ipRow, int ipMaxCol)
{
	bool blReturn = false;

	for (int ilCol = 2; ilCol <= ipMaxCol; ilCol++)
	{
		CString olContents = (popGrid->GetValueRowCol(ipRow, ilCol));
		if (olContents.IsEmpty() == FALSE && olContents != CString(" "))
		{
			blReturn = true;
			break;
		}
	}

	return blReturn;
}
*/

//------------------------------------------------------------
/*
void CRegelEditorFormView::MakeTimeScale()
{
	//--- exception handling
	CCS_TRY

	// check if object exists, if so: delete it and make a new one
	if (pomTimeScale == NULL)
	{
		pomTimeScale = new CCSTimeScale(this);
	}

	// get view button rect as an anchor-point for the gantt
	CRect olRect;
	CWnd *polAnchor = GetDlgItem(IDC_ARR_LIST);
	polAnchor->GetWindowRect(olRect);
	ScreenToClient(olRect);
	
	// get RegelWerkView's client rect
	CRect olTSRect;
	GetClientRect(olTSRect);

	// compute size for timescale rect
	int ilWidth = olTSRect.Width();
	int ilHeight = olTSRect.Height();
	olTSRect.top = olRect.bottom + 50;
	olTSRect.left = olRect.left + imVerticalScaleWidth + 1;
	olTSRect.right = ilWidth - 2 * olRect.left;
	olTSRect.bottom = olTSRect.top + 32;
	
	// create time scale
	pomTimeScale->Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE, olTSRect, this, 0, NULL);

	// register new timescale with gantt
	if (pomGantt != NULL)
			pomGantt->SetTimeScale(pomTimeScale);

	// set time values 
	omTSStartTime = CTime(1980, 1, 1, 0, 30, 0); // 1.Jan 1980, 00:00h
    omTSDuration = CTimeSpan(0, 7, 0, 0);	// 7 hours complete span
    omTSInterval = CTimeSpan(0, 0, 5, 0);	// 10 min interval
	pomTimeScale->SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

	//--- exception handling
	CCS_CATCH_ALL
}*/
//------------------------------------------------------------
/*
void CRegelEditorFormView::MakeRelativeTimeScale()
{
	CCS_TRY

	// check if object exists, if so: delete it and make a new one
	if (pomRelTimeScale == NULL)
	{
		pomRelTimeScale = new RelativeTimeScale(this);
	}

	// get view button rect as an anchor-point for the gantt
	CRect olRect;
	CWnd *polAnchor = GetDlgItem(IDC_ARR_LIST);
	polAnchor->GetWindowRect(olRect);
	ScreenToClient(olRect);
	
	// get RegelWerkView's client rect
	CRect olTSRect;
	GetClientRect(olTSRect);

	// compute size for timescale rect
	int ilWidth = olTSRect.Width();
	int ilHeight = olTSRect.Height();
	olTSRect.top = olRect.bottom + 50;
	olTSRect.left = olRect.left + imVerticalScaleWidth + 1;
	olTSRect.right = ilWidth - 2 * olRect.left;
	olTSRect.bottom = olTSRect.top + 32;
	
	// create time scale
	pomRelTimeScale->Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE, olTSRect, this, 0, NULL);

	// set time values 
	omTSStartTime = CTime(1980, 1, 1, 0, 30, 0); // 1.Jan 1980, 00:00h
    omTSDuration = CTimeSpan(0, 7, 0, 0);	// 7 hours complete span
    omTSInterval = CTimeSpan(0, 0, 5, 0);	// 10 min interval
	pomRelTimeScale->SetReferenceTimes(omOnBlock, omOfBlock);
	pomRelTimeScale->SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

	CCS_CATCH_ALL
}*/
//------------------------------------------------------------
/*
void CRegelEditorFormView::MakeGantt()
{
	//--- exception handling
	CCS_TRY

	if (pomGantt != NULL) 
	{
		delete pomGantt;
	}
	pomGantt = new RulesGantt(pomViewer, imVerticalScaleWidth, imVerticalScaleIndent, &ogSmallFonts_Regular_6, &ogCourier_Regular_8, 14, 21, 2);
	
	// get timescale rect
	CRect olTSRect;
	pomTimeScale->GetWindowRect(&olTSRect);
	ScreenToClient(olTSRect);
	olTSRect.left;
	
	// get gantt rect
	CRect olGanttRect;
	CWnd *polStaticWnd = GetDlgItem(IDC_GANTT_RECT);
	polStaticWnd->GetWindowRect(olGanttRect);
	ScreenToClient(olGanttRect);
	polStaticWnd->ShowWindow(SW_HIDE);

	// set some pointers
    pomGantt->SetTimeScale(pomTimeScale);
	pomGantt->SetStatusBar((CStatic*)pomStatusBar);
    pomGantt->SetViewer(pomViewer);

	// some more attributes
	omStartTime = pomTimeScale->GetDisplayStartTime();    // 01.01.1980
	omDuration = CTimeSpan(0,24,0,0); //pomTimeScale->GetDisplayDuration();  // 7 hours
	pomGantt->SetVerticalScaleWidth(imVerticalScaleWidth);		// Width of  LeftScale
	pomGantt->SetVerticalScaleIndent(imVerticalScaleIndent);    // Indent of LeftScale (space between left edge and start of LeftScale)
    pomGantt->SetDisplayWindow(omStartTime, omStartTime + omDuration);
	pomGantt->SetVerticalScaleColors(BLACK, SILVER, AQUA, SILVER);
	pomGantt->SetGanttChartColors(NAVY, SILVER, YELLOW, SILVER);
	pomGantt->SetBorderPrecision(10);
	pomGantt->SetGutters(4,10);
    
	// create gantt and attach this window
	pomGantt->Create(WS_BORDER, olGanttRect, (CWnd*)this);
	pomGantt->AttachWindow(this);	
	
	//--- make gantt known to viewer
	pomViewer->SetGantt(pomGantt);
	
	//--- exception handling
	CCS_CATCH_ALL
}*/
//------------------------------------------------------------

void CRegelEditorFormView::MakeViewer()
{
	if (pomViewer != NULL)
	{
		delete pomViewer;
	}

	pomViewer = new RulesGanttViewer(-1);
	pomViewer->Attach(this);
	pomViewer->SetReferenceTimes(omOnBlock, omOfBlock);
	pomViewer->ChangeViewTo();
}
//-----------------------------------------------------------------------------------------------------------------------

/*	only used in CopyDemandRequirements which isn't used anywhere
bool CRegelEditorFormView::CopyRessources(CStringArray &opRudUrnos)
{
	bool blRet = true;
	
	CCS_TRY

	
	CString olRud;
	CString olUrno, olUdgr;
	CCSPtrArray <RecordSet> olRpfArr;
	CCSPtrArray <RecordSet> olRpqArr;
	CCSPtrArray <RecordSet> olReqArr;
	CCSPtrArray <RecordSet> olRloArr;
	CCSPtrArray <RecordSet> olRudArr;
	CStringList				olUdgrList;
	POSITION				pos;
	int j ;
	RecordSet olRpqRecord(ogBCD.GetFieldCount("RPQ_TMP"));

	int ilSize = opRudUrnos.GetSize();
	for (int i = 0; i < ilSize; i++)
	{
		olRud = opRudUrnos.GetAt(i);
	
		// functions
		RecordSet olRpfRecord(ogBCD.GetFieldCount("RPF_TMP"));
		ogBCD.GetRecords("RPF_TMP", "URUD", olRud, &olRpfArr);	
		int ilCount = olRpfArr.GetSize();
		for ( j = ilCount - 1; j >= 0; j--)
		{
			olRpfRecord = olRpfArr[j];
			olUrno.Format("%d", ogBCD.GetNextUrno());
			olRpfRecord.Values[igRpfUrnoIdx] = olUrno;
			olRpfRecord.Values[igRpfUrudIdx] = olRud;
			ogBCD.InsertRecord("RPF", olRpfRecord);
		}

		// qualifications
		//	ogBCD.GetRecords("RPQ", "URUD", olRud, &olRpqArr);
		ogBCD.GetRecords("RPQ_TMP", "URUD", olRud, &olRpqArr);	//hag990901
		ilCount = olRpqArr.GetSize();
		for (j = ilCount - 1; j >= 0; j--)
		{
			olRpqRecord = olRpqArr[j];
			olUrno.Format("%d", ogBCD.GetNextUrno());
			olRpqRecord.Values[igRpqUrnoIdx] = olUrno;
			olRpqRecord.Values[igRpqUrudIdx] = olRud;
			olRpqRecord.Values[igRpqUdgrIdx] = "";
			ogBCD.InsertRecord("RPQ", olRpqRecord);
		}
		//  Liste mit UDGR's aufbauen f�r Suche von nicht zugeordneten Qualis
		if ( ogBCD.GetField ( "RUD_TMP", "URNO", olRud, " UDGR", olUdgr ) &&
			 !olUdgr.IsEmpty() && !olUdgrList.Find(olUdgr) )
			olUdgrList.AddTail(olUdgr);

		// equipment
		RecordSet olReqRecord(ogBCD.GetFieldCount("REQ_TMP"));
		ogBCD.GetRecords("REQ_TMP", "URUD", olRud, &olReqArr);	//hag990901
		ilCount = olReqArr.GetSize();
		for (j = ilCount - 1; j >= 0; j--)
		{
			olReqRecord = olReqArr[j];
			olUrno.Format("%d", ogBCD.GetNextUrno());
			olReqRecord.Values[igReqUrnoIdx] = olUrno;
			olReqRecord.Values[igReqUrudIdx] = olRud;
			ogBCD.InsertRecord("REQ", olReqRecord);
		}
		
		// locations
		RecordSet olRloRecord(ogBCD.GetFieldCount("RLO_TMP"));
		ogBCD.GetRecords("RLO_TMP", "URUD", olRud, &olRloArr);			//hag990901
		ilCount = olRloArr.GetSize();
		for (j = ilCount - 1; j >= 0; j--)
		{
			olRloRecord = olRloArr[j];
			olUrno.Format("%d", ogBCD.GetNextUrno());
			olRloRecord.Values[igRloUrnoIdx] = olUrno;
			olRloRecord.Values[igRloUrudIdx] = olRud;
			ogBCD.InsertRecord("RLO", olRloRecord);
		}

		olRpfArr.DeleteAll();
		olRpqArr.DeleteAll();
		olReqArr.DeleteAll();
		olRloArr.DeleteAll();
	}

	//  Kopiere alle nicht zugeordneten (Gruppen-)Qualifikationen
	pos = olUdgrList.GetHeadPosition ();
	while ( pos )
	{
		olUdgr = olUdgrList.GetNext ( pos );
		if ( !olUdgr.IsEmpty() )
		{
			ogBCD.GetRecords("RPQ_TMP", "UDGR", olUdgr, &olRpqArr);
			
			for (j = 0; j < olRpqArr.GetSize(); j++)
			{
				olRpqRecord = olRpqArr[j];
				olUrno.Format("%d", ogBCD.GetNextUrno());
				olRpqRecord.Values[igRpqUrnoIdx] = olUrno;
				olRpqRecord.Values[igRpqUrudIdx] = "";
				olRpqRecord.Values[igRpqUdgrIdx] = olUdgr;
				ogBCD.InsertRecord("RPQ", olRpqRecord);
			}
		}
	}
	olRpqArr.DeleteAll();

	blRet &= ogBCD.Save("RPF");
	blRet &= ogBCD.Save("RPQ");
	blRet &= ogBCD.Save("REQ");
	blRet &= ogBCD.Save("RLO");
	

	CCS_CATCH_ALL
	return blRet;
}
*/

//-----------------------------------------------------------------------------------------------------------------------
/*	CopyDemandRequirements isn't used anywhere
bool CRegelEditorFormView::CopyDemandRequirements()
{
	bool blRet = false;

	CCS_TRY

	CStringArray olRudUrnos;
	CString olNewUrno, olOldRudUrno;
	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD_TMP"));
	int ilRudCount = ogBCD.GetDataCount("RUD_TMP");
	
	for (int i = 0; i < ilRudCount; i++)
	{
		ogBCD.GetRecord("RUD_TMP", i, olRudRecord);
		olNewUrno.Format("%d", ogBCD.GetNextUrno());
		olOldRudUrno = olRudRecord.Values[igRudUrnoIdx];	//hag990901
		olRudRecord.Values[igRudUrnoIdx] = olNewUrno;
		olRudUrnos.Add(olOldRudUrno);						//hag990901
		olRudRecord.Values[igRudUrueIdx] = omCurrRueUrno;		//hag990901
		ogBCD.InsertRecord("RUD", olRudRecord, false);
	}

	blRet = ogBCD.Save("RUD");

	CopyRessources(olRudUrnos);
	

	CCS_CATCH_ALL

	return blRet;
}
*/

//-----------------------------------------------------------------------------------------

bool CRegelEditorFormView::SaveDemandRequirements() 
{
	bool blReturn = true;

	CCS_TRY

	SaveBarDisplayPositions();

	//--- copy all the rud entries from RUD_TMP to RUD
	RecordSet olRecord(ogBCD.GetFieldCount("RUD"));
	int ilCount = ogBCD.GetDataCount("RUD_TMP");

	if (ilCount > 0)
	{
		for (int i = 0; i < ilCount; i++)
		{
			bool blErr1 = ogBCD.GetRecord("RUD_TMP", i, olRecord);
			bool blErr2;

			if ( blErr1 == true )
			{
				TRACE("[SaveDemandRequirements] Urno: %s\n", olRecord.Values[igRudUrnoIdx]);

				blErr2 = ogBCD.InsertRecord("RUD", olRecord, false);
			}
			if (blErr1 == false || blErr2 == false)
			{
				ogLog.Trace("DEBUG", "[RegelWerkView::SaveDemandRequirements] Failed appending RUD record [index %d] to object.", i);
				blReturn = false;
				break;
			}
		}

		// save RUD records to database
		if (blReturn)
		{
			bool blErr = ogBCD.Save("RUD");
			if (blErr == true)
			{
				ogDataSet.FillLogicRudObject(omCurrRueUrno);
				ogLog.Trace("WRITE2DB", "[RegelWerkView::SaveDemandRequirements] Saved %d RUD records to database", i);
			}
			else
			{
				blReturn = false;
				ogLog.Trace("DEBUG","Failed saving RUD records to database");
			}
		}
	}

	CCS_CATCH_ALL

	return blReturn;
}
//-----------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::SaveBarDisplayPositions()
{
	//----------------------------------------------
	// Saves the display position of the bars in the gantt, 
	// eg. the order the bars should be diplayed in.
	//----------------------------------------------

	int ilCount = pomViewer->GetLineCount();	
	int ilDisp = 0;

	for (int i = 0; i < ilCount; i++)
	{
		if (pomViewer->omLines[i].Bars.GetSize() > 0)
		{
			ilDisp++;
			pomViewer->omLines[i].Disp = ilDisp;

			RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
			ogBCD.GetRecord("RUD_TMP", "URNO", pomViewer->omLines[i].Urno, olRecord);
			olRecord.Values[igRudDispIdx].Format("%d", ilDisp);
			ogBCD.SetRecord("RUD_TMP", "URNO", pomViewer->omLines[i].Urno, olRecord.Values);
		}
	}
}
//-----------------------------------------------------------------------------------------------------------------------
/*
bool CRegelEditorFormView::SaveQualifications() 
{
	bool blRet = true;

	CCS_TRY

	
	RecordSet olRecord(ogBCD.GetFieldCount("RPQ_TMP"));

	int ilCount = ogBCD.GetDataCount("RPQ_TMP");
	
	if (ilCount > 0)
	{
		for (int i = 0; i < ilCount; i++)
		{
			bool blErr1 = ogBCD.GetRecord("RPQ_TMP", i, olRecord);		
			bool blErr2 = ogBCD.InsertRecord("RPQ", olRecord);
			if (blErr1 == false || blErr2 == false)
			{
				ogLog.Trace("DEBUG", "[RegelWerkView::SaveQualifications] Failed appending RPQ record [index %d] to object.", i);
				blRet = false;
				break;
			}
		}

		// save RUD records to database
		if (blRet)
		{
			bool blErr = ogBCD.Save("RPQ");
			if (blErr == true)
			{
				ogLog.Trace("WRITE2DB", "[RegelWerkView::SaveQualifications] Saved %d RPQ records to database", i);
			}
			else
			{
				blRet = false;
				ogLog.Trace("DEBUG","[RegelWerkView::SaveQualificatons] Failed saving RPQ records to database");
			}
		}
	}


	CCS_CATCH_ALL

	return blRet;
}
*/
//-----------------------------------------------------------------------------------------------------------------------
/*
bool CRegelEditorFormView::SaveFunctions() 
{
	bool blRet = true;

	CCS_TRY

	
	RecordSet olRecord(ogBCD.GetFieldCount("RPF_TMP"));

	int ilCount = ogBCD.GetDataCount("RPF_TMP");
	
	if (ilCount > 0)
	{
		for (int i = 0; i < ilCount; i++)
		{
			bool blErr1 = ogBCD.GetRecord("RPF_TMP", i, olRecord);		
			bool blErr2 = ogBCD.InsertRecord("RPF", olRecord);
			if (blErr1 == false || blErr2 == false)
			{
				ogLog.Trace("DEBUG", "[RegelWerkView::SaveFunctions] Failed appending RPF record [index %d] to object.", i);
				blRet = false;
				break;
			}
		}

		// save RUD records to database
		if (blRet)
		{
			bool blErr = ogBCD.Save("RPF");
			if (blErr == true)
			{
				ogLog.Trace("WRITE2DB", "[RegelWerkView::SaveFunctions] Saved %d RPF records to database", i);
			}
			else
			{
				blRet = false;
				ogLog.Trace("DEBUG","[RegelWerkView::SaveFunctions] Failed saving RPF records to database");
			}
		}
	}


	CCS_CATCH_ALL

	return blRet;
}*/

//-----------------------------------------------------------------------------------------------------------------------
/*
bool CRegelEditorFormView::SaveEquipment() 
{
	bool blRet = true;

	CCS_TRY



	CCS_CATCH_ALL

	return blRet;
}
*/
//-----------------------------------------------------------------------------------------------------------------------
/*
bool CRegelEditorFormView::SaveLocations() 
{
	bool blRet = true;

	CCS_TRY

	RecordSet olRecord(ogBCD.GetFieldCount("RLO_TMP"));

	int ilCount = ogBCD.GetDataCount("RLO_TMP");
	
	if (ilCount > 0)
	{
		for (int i = 0; i < ilCount; i++)
		{
			bool blErr1 = ogBCD.GetRecord("RLO_TMP", i, olRecord);		
			bool blErr2 = ogBCD.InsertRecord("RLO", olRecord);
			if (blErr1 == false || blErr2 == false)
			{
				ogLog.Trace("DEBUG", "[RegelWerkView::SaveLocations] Failed appending RLO record [index %d] to object.", i);
				blRet = false;
				break;
			}
		}

		// save RUD records to database
		if (blRet)
		{
			bool blErr = ogBCD.Save("RLO");
			if (blErr == true)
			{
				ogLog.Trace("WRITE2DB", "[RegelWerkView::SaveLocations] Saved %d RLO records to database", i);
			}
			else
			{
				blRet = false;
				ogLog.Trace("DEBUG","[RegelWerkView::SaveLocations] Failed saving RLO records to database");
			}
		}
	}

	CCS_CATCH_ALL

	return blRet;
}*/
//-----------------------------------------------------------------------------------------------------------------------
/*	In die Basisklasse �bernommen hag991126
bool CRegelEditorFormView::SaveValidity() 
{
	bool blReturn = true;

	CCS_TRY
	
	RecordSet olRecord(ogBCD.GetFieldCount("VAL"));
	
	olRecord.Values[igValUvalIdx] = omCurrRueUrno;
	olRecord.Values[igValApplIdx] = "RULE_AFT";
	olRecord.Values[igValTimtIdx] = pomFrameWnd->rmValData.FixT;
	olRecord.Values[igValTimfIdx] = pomFrameWnd->rmValData.FixF;
	
	// freq
	CString olFreq;
	olFreq = pomFrameWnd->rmValData.Freq;
	if (olFreq.IsEmpty() == TRUE || olFreq == CString(" ")) 
	{
		olFreq = CString("1111111");
	}
	olRecord.Values[igValFreqIdx] = olFreq;


//--- get exclusion periods and create VAL records
	CTime olVafr;
	CTime olVato;
	CTime olX;
	bool blErr = true;
	int ilCount = pomFrameWnd->rmValData.Excls.GetSize();
	int i;
	if (ilCount > 0)
	{
		for (i = 0; i < ilCount; i++)
		{
			if (i == 0)
			{
				olRecord.Values[igValVafrIdx] = pomFrameWnd->rmValData.Vafr;
				olVato = pomFrameWnd->rmValData.Excls[i].FirstDay;
				olVato -= CTimeSpan(1, 0, 0, 0); 
				olRecord.Values[igValVatoIdx] = CTimeToDBString(olVato, olX);
			}
			else if (i == ilCount - 1)
			{
				olRecord.Values[igValVatoIdx] = pomFrameWnd->rmValData.Vato;
				olVafr = pomFrameWnd->rmValData.Excls[i].LastDay;
				olVafr += CTimeSpan(1, 0, 0, 0); 
				olRecord.Values[igValVafrIdx] = CTimeToDBString(olVafr, olX);
			}
			else
			{
				olVafr = pomFrameWnd->rmValData.Excls[i - 1].LastDay;
				olVafr += CTimeSpan(1, 0, 0, 0);
				olRecord.Values[igValVafrIdx] = CTimeToDBString(olVafr, olX);
		
				olVato = pomFrameWnd->rmValData.Excls[i].FirstDay;
				olVato -= CTimeSpan(1, 0, 0, 0); 
				olRecord.Values[igValVatoIdx] = CTimeToDBString(olVato, olX);
			}

			blErr &= ogBCD.InsertRecord("VAL", olRecord, false);
		}
	}
	else
	{
		CString test1 = pomFrameWnd->rmValData.Vafr;
		CString test2 = pomFrameWnd->rmValData.Vato;

		olRecord.Values[igValVafrIdx] = pomFrameWnd->rmValData.Vafr;
		olRecord.Values[igValVatoIdx] = pomFrameWnd->rmValData.Vato;
		blErr &= ogBCD.InsertRecord("VAL", olRecord, false);
	}
	
	if (blErr == false)
	{
		ogLog.Trace("DEBUG", "[RegelWerkView::SaveValidity] Failed appending a VAL record to object.");
		blReturn = false;
	}

	// save VAL record to database
	if (blReturn)
	{
		bool blErr = ogBCD.Save("VAL");
		if (blErr == true)
		{
			ogLog.Trace("WRITE2DB", "[RegelWerkView::SaveValidity] Saved %d VAL records to database", --i);
		}
		else
		{
			blReturn = false;
			ogLog.Trace("DEBUG","Failed saving a VAL record to database");
		}
	}
	
	CCS_CATCH_ALL
	
	return blReturn;
}
*/
//-----------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::SetExclusionRule(bool bpIsExclude)
{
	if (bpIsExclude)
	{
		// it is an exclusion rule
		pomFrameWnd->HideServicesListBtn(true);
		//pomRotGrid->SetGridEnabled(false, false);
		//pomArrGrid->SetGridEnabled(false, false);
		//pomDepGrid->SetGridEnabled(false, false);
	}
	else
	{
		// it is no exclusion rule 
		pomFrameWnd->HideServicesListBtn(false);
	}
	//	pomRotGrid->SetGridEnabled(true, true);
	//	pomArrGrid->SetGridEnabled(true, true);
	//	pomDepGrid->SetGridEnabled(true, true);
	//}
}
//-----------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::SetPreviousIOT()
{
	if (bmTurnaround)
	{
		m_RBtn_Turnaround.SetCheck(CHECKED);
		m_RBtn_Inbound.SetCheck(UNCHECKED);
		m_RBtn_Outbound.SetCheck(UNCHECKED);
	}
	else if (bmInbound)
	{
		m_RBtn_Inbound.SetCheck(CHECKED);
		m_RBtn_Turnaround.SetCheck(UNCHECKED);
		m_RBtn_Outbound.SetCheck(UNCHECKED);
	}
	else if (bmOutbound)
	{
		m_RBtn_Outbound.SetCheck(CHECKED);
		m_RBtn_Inbound.SetCheck(UNCHECKED);
		m_RBtn_Turnaround.SetCheck(UNCHECKED);
	}
}
//-----------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::SetOutbound()
{
	SetIOT(2);
	
	// turnaround
	if (m_RBtn_Turnaround.GetCheck() == CHECKED)
	{
		m_RBtn_Turnaround.SetCheck(UNCHECKED);
	}

	// inbound
	if (m_RBtn_Inbound.GetCheck() == CHECKED)
	{
		m_RBtn_Inbound.SetCheck(UNCHECKED);
	}
	if (pomArrGrid->bmIsGridEnabled == true)
	{
		pomArrGrid->SetGridEnabled(false);
	}

	// outbound
	if (m_RBtn_Outbound.GetCheck() == UNCHECKED)
	{
		m_RBtn_Outbound.SetCheck(CHECKED);
	}

	if ((pomDepGrid->bmIsGridEnabled == false) && (bmSingle == true))
	{
		pomDepGrid->SetGridEnabled(true);
		InitializeTable(pomDepGrid, "FLDD");
	}
}
//-----------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::SetInbound()
{
	SetIOT(1);
	
	// turnaround
	if (m_RBtn_Turnaround.GetCheck() == CHECKED)
	{
		m_RBtn_Turnaround.SetCheck(UNCHECKED);
	}

	// inbound
	if (m_RBtn_Inbound.GetCheck() == UNCHECKED)
	{
		m_RBtn_Inbound.SetCheck(CHECKED);
	}
	if ((pomArrGrid->bmIsGridEnabled == false) && (bmSingle == true))
	{
		pomArrGrid->SetGridEnabled(true);
		InitializeTable(pomArrGrid, "FLDA");
	}

	// outbound 
	if (m_RBtn_Outbound.GetCheck() == CHECKED)
	{
		m_RBtn_Outbound.SetCheck(UNCHECKED);
	}
	if (pomDepGrid->bmIsGridEnabled == true)
	{  
		pomDepGrid->SetGridEnabled(false);
	}	
}
//-----------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::SetTurnaround()
{
	SetIOT(0);
	
	// turnaround
	if (m_RBtn_Turnaround.GetCheck() == UNCHECKED)
	{
		m_RBtn_Turnaround.SetCheck(CHECKED);
	}

	// inbound
	if (m_RBtn_Inbound.GetCheck() == CHECKED)
	{
		m_RBtn_Inbound.SetCheck(UNCHECKED);
	}
	
	if ((pomArrGrid->bmIsGridEnabled== false) && bmSingle )
	{
		pomArrGrid->SetGridEnabled(true);
		InitializeTable(pomArrGrid, "FLDA");
	}

	// outbound
	if (m_RBtn_Outbound.GetCheck() == CHECKED)
	{
		m_RBtn_Outbound.SetCheck(UNCHECKED);
	}
	if ((pomDepGrid->bmIsGridEnabled== false) && bmSingle )
	{
		pomDepGrid->SetGridEnabled(true);
		InitializeTable(pomDepGrid, "FLDD");
	}	
}
//-----------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::SetIOT(int ipIOT) 
{
	//-----------------------------------------
	//  controls for inbound/outbound/turnaround rule 
	//  
	// -1 = keep current check state
	//  0 = set to turnaround rule
	//  1 = set to inbound rule
	//  2 = set to outbound rule
	//-----------------------------------------

	CCS_TRY
	
	bmTurnaround = (ipIOT == 0 ? true : false);
	bmInbound = (ipIOT == 1 ? true : false);
	bmOutbound = (ipIOT == 2 ? true : false);

	if (ipIOT != 0)
	{
		// disable MAXT (max. ground time)
		m_MaxtEdit.SetInitText(CString(""));
		m_MaxtEdit.EnableWindow(FALSE);
		m_Fra_Turn.EnableWindow(FALSE);
		m_Sta_Maxt.EnableWindow(FALSE);
		m_Sta_Maxt2.EnableWindow(FALSE);
		
		// disable FSPL (split)
		m_Chb_Fspl.SetCheck(UNCHECKED);
		m_Chb_Fspl.EnableWindow(FALSE);
		m_Edt_MinTime.SetInitText(CString(""));
		m_Edt_MinTime.EnableWindow(FALSE);
		m_Sta_MinTime.EnableWindow(FALSE);
		m_Fra_Split.EnableWindow(FALSE);
	}
	else
	{
		// enable MAXT (max. ground time)
		m_MaxtEdit.EnableWindow(TRUE);
		m_Fra_Turn.EnableWindow(TRUE);
		m_Sta_Maxt.EnableWindow(TRUE);
		m_Sta_Maxt2.EnableWindow(TRUE);
		
		// enable FSPL (split)
		m_Chb_Fspl.EnableWindow(TRUE);
		m_Sta_MinTime.EnableWindow(TRUE);
		m_Fra_Split.EnableWindow(TRUE);
	}
	
	// set timescale
	pomRelTimeScale->SetRuleType(ipIOT);
	pomRelTimeScale->Invalidate(TRUE);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

bool CRegelEditorFormView::CheckIOTChange(int ipIOT)
{
	bool blRet = true;
	CString olIOT;
	olIOT.Format("%d", ipIOT);
	
	RecordSet olRud(ogBCD.GetFieldCount("RUD_TMP"));
	CStringArray olWrongBars;
	CString olBarUrno, olDrty;
	bool blErr = true;
	bool blNoneWrong = true;
	int ilRes;

// find bars not matching rule event type
	int ilCount = ogBCD.GetDataCount("RUD_TMP");
	for (int i = 0; i < ilCount; i++)
	{
		olBarUrno = ogBCD.GetField("RUD_TMP", i, "URNO");
		if (IsBarTypeMatching(olBarUrno, olIOT) == false)
		{
			blNoneWrong = false;
			olWrongBars.Add(olBarUrno);
		}
	}

	if (blNoneWrong == false)
	{
		Beep(800, 200);
		ilRes = MessageBox(GetString(1573), GetString(288), (MB_YESNOCANCEL | MB_ICONQUESTION));
		

		// MNE 000926
		int ilRest = 0;
		switch (ilRes)
		{
		case IDYES:
				// change bars with wrong types
				ogDataSet.ResetOffsetsAndConnections();
				ilCount = olWrongBars.GetSize();
				for (i = 0; i < ilCount; i++)
				{
					ogBCD.GetRecord("RUD_TMP", "URNO", olWrongBars.GetAt(i), olRud);
					ogDataSet.ChangeWrongReference(&olRud, ipIOT);
				}
				break;

		case IDNO:
				// delete bars with wrong types
				ilRest = ogDataSet.DeleteWrongTypeFromRudTmp(ipIOT);
				break;

		case IDCANCEL:
				blRet = false;
				break;

		default:
				break;
		}
		/*if (ilRes == IDYES)
		{
			// change bars with wrong types
			ogDataSet.ResetOffsetsAndConnections();
			ilCount = olWrongBars.GetSize();
			for (i = 0; i < ilCount; i++)
			{
				ogBCD.GetRecord("RUD_TMP", "URNO", olWrongBars.GetAt(i), olRud);
				ogDataSet.ChangeWrongReference(&olRud, ipIOT);
			}
		}
		else
		{
			// delete bars with wrong types
			int ilRest = ogDataSet.DeleteWrongTypeFromRudTmp(ipIOT);
		}*/

		// END MNE 000926
	}
	
	if (blRet == true)	// mne 000926
	{
		pomViewer->ChangeViewTo(omCurrRueUrno);
		pomGantt->SetScrollPos(SB_VERT, 0, TRUE);
		pomGantt->UpdateWindow();
		
		if (::IsWindow(pomGantt->pomDetailDlg->GetSafeHwnd()))
		{
			pomGantt->pomDetailDlg->Destroy();
		}
	}
	
	return blRet;
}
//-----------------------------------------------------------------------------------------------------------------------
void CRegelEditorFormView::CloseDetailWindows()
{
	// close dialogs
	if (::IsWindow(pomGantt->pomDetailDlg->GetSafeHwnd()))
	{
		pomGantt->pomDetailDlg->Destroy();
	}

	if (::IsWindow(pomGantt->pomConnectionDlg->GetSafeHwnd()))
	{
		pomGantt->pomConnectionDlg->Destroy();
	}
	
}
//-----------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::SetRBtnSC(int ipSCI) 
{
	//-----------------------------------------
	//  radio buttons for single/collective rule 
	//  
	//  0 = single rule
	//  1 = collective rule
	//  2 = flight independent rule
	//-----------------------------------------

	CCS_TRY

	//--- set false as default value
	bmSCRule = false;
	bmSingle = false;

//---   single or collective rule
	if (ipSCI == 0 || ipSCI == 1)
	{	
		bmSCRule = true;
		
		if (ipSCI == 0)
		{
			bmSingle = true;
		}
	
		if (m_RBtn_Turnaround.IsWindowEnabled() == FALSE)
		{
			m_RBtn_Turnaround.EnableWindow(TRUE);
		}
		if (m_RBtn_Inbound.IsWindowEnabled() == FALSE)
		{
			m_RBtn_Inbound.EnableWindow(TRUE);
		}
		if (m_RBtn_Outbound.IsWindowEnabled() == FALSE)
		{
			m_RBtn_Outbound.EnableWindow(TRUE);
		}

	}
	
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::SetTSStartTime(CTime opTSStartTime)
{
    omTSStartTime = opTSStartTime;
    if ((omTSStartTime < omStartTime) ||
		(omTSStartTime > omStartTime + omDuration))
    {
        omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
            0, 0, 0) - CTimeSpan(1, 0, 0, 0);
    }
        
    /*char clBuf[8];
    sprintf(clBuf, "%02d%02d%02d", omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 1000);*/
}
//-----------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::SetEvorBitmap() 
{
	if (pomComboBox != NULL)
	{
		CString olTplUrno;
		CString olEvor="1";
		if (GetSelectedTplUrno(*pomComboBox, olTplUrno, FALSE))
			olEvor = ogBCD.GetField("TPL", "URNO", olTplUrno, "EVOR");
		
		CStatic *polBitmap = (CStatic*)GetDlgItem(RULED_BMP_EVOR);
		if (polBitmap != NULL)
		{
			int ilEvor = atoi(olEvor);
			switch (ilEvor)
			{
				case 1:
					polBitmap->SetBitmap((HBITMAP) omVL2RBmp);
					break;

				case 2:
					polBitmap->SetBitmap((HBITMAP) omHL2RBmp);
					break;

				case 3:
					polBitmap->SetBitmap((HBITMAP) omVR2LBmp);
					break;

				case 4:
					polBitmap->SetBitmap((HBITMAP) omHR2LBmp);
					break;
				
				default:
					polBitmap->SetBitmap((HBITMAP) omVL2RBmp);
					break;
			}
		}
	}
}
//-----------------------------------------------------------------------------------------------------------------------

void CRegelEditorFormView::SetStaticTexts()
{
	CCS_TRY
	
	CWnd *pWnd = GetDlgItem(IDS_PRSN);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(KURZNAME));	

	pWnd = GetDlgItem(IDS_PRNA);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(NAME));
	
	SetDlgItemLangText ( this, IDS_ROTATION, IDS_ROTATION_TXT ); 
	SetDlgItemLangText ( this, IDS_ANKUNFT, IDS_ANKUNFT_TXT ); 
	SetDlgItemLangText ( this, IDS_ABFLUG, IDS_ABFLUG_TXT ); 

	/*pWnd = GetDlgItem(IDC_SINGLE);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1401));

	pWnd = GetDlgItem(IDC_COLLECT);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1402));*/

	pWnd = GetDlgItem(IDC_INBOUND_RB);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(250));

	pWnd = GetDlgItem(IDC_OUTBOUND_RB);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(251));

	pWnd = GetDlgItem(IDC_TURNAROUND_RB);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(252));

	SetDlgItemLangText ( this, IDC_EXCLUDE_CHB, IDS_EXCLUDE ); 
	SetDlgItemLangText ( this, IDC_ACTIVE_CHB, IDS_DEAKTIVIERT ); 

	if(bgMaxGroundTimeIsMin)
		SetDlgItemLangText ( this, IDC_MAXT_STATIC, IDS_MIN_GROUND_TIME ); 
	else
		SetDlgItemLangText ( this, IDC_MAXT_STATIC, IDS_MAX_GROUND_TIME ); 


	SetDlgItemLangText ( this, IDC_MAXT_STATIC2, IDS_MIN ); 
	SetDlgItemLangText ( this, IDC_FSPL_CHB, IDS_TILE ); 
	SetDlgItemLangText ( this, IDC_MINTIME_STATIC, IDS_MIN_TEIL_ZEIT ); 
	
	SetDlgItemLangText ( this, RULED_RBT_ABS, IDS_ABSOLUT ); 
	SetDlgItemLangText ( this, RULED_RBT_REL, IDS_RELATIV ); 

	UpdateData(FALSE);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------


//  BDPS-SEC
void CRegelEditorFormView::SetBdpsState()
{
	//-- exception handling
	CCS_TRY

	//-- exception handling
	CCS_CATCH_ALL
}



//-----------------------------------------------------------------------------------------
//					Processing functions
//-----------------------------------------------------------------------------------------
void CRegelEditorFormView::AddGridListItem(CGridFenster *popGrid, CString opTable, RecordSet *popRecord/*CString opItem*/)
{
	//--- exception handling
	CCS_TRY


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));	

	CString olFieldNameStr;
	CString olField;
	CString olTable;
	CString olItem;
	CString olChoiceList;
	CStringArray olChoiceArr;
	CGXStyle olStyle;

	// search in rotation grid
	int ilRotCount = popGrid->GetRowCount();
	for (int ilRow = 1; ilRow < ilRotCount; ilRow++)
	{
		olFieldNameStr = popGrid->GetValueRowCol(ilRow, 7);
		if (olFieldNameStr.GetLength() >= 8)
		{
			olTable = olFieldNameStr.Left(3);
			olField = olFieldNameStr.Right(4);
			olItem = popRecord->Values[ogBCD.GetFieldIndex(olTable, olField)];
			if (olTable == opTable)
			{
				popGrid->GetStyleRowCol(ilRow, 2, olStyle);
				if ( olStyle.GetIncludeChoiceList () )	//hag990827
					olChoiceList = olStyle.GetChoiceList();
				else
					olChoiceList.Empty ();	//hag990827
				if (olChoiceList.IsEmpty() == FALSE)
				{
					if (olChoiceList.Find(olItem) == -1)
					{
						olChoiceList += '\n' + olItem;
						olStyle.SetChoiceList(olChoiceList);
						popGrid->SetStyleRange(CGXRange(ilRow, 2), olStyle);
					}
				}
			}
		}
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));	


	// exception handling
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------

CString CRegelEditorFormView::GetRuleEventType()
{
	CString olEvty;
	olEvty = bmOutbound ? "2" : (bmInbound ? "1" : "0");
	
	return olEvty;
}

//-----------------------------------------------------------------------------------------

bool CRegelEditorFormView::IsBarTypeMatching(CString opBarUrno, CString opIOT)
{
	bool blRet = true;
	
	if (opBarUrno.IsEmpty() == TRUE)
	{
		blRet = false;
	}
	else
	{
		CString olDrty;
		int ilFieldCount = ogBCD.GetFieldCount("RUD_TMP");
		RecordSet olRud(ilFieldCount);
		ogBCD.GetRecord("RUD_TMP", "URNO", opBarUrno, olRud);
		olDrty = olRud.Values[igRudDrtyIdx];

		if (opIOT != "0")	// turnaround eats everything
		{
			if (olDrty != opIOT)
			{
				blRet = false;
			}
		}
	}

	return blRet;
}
//---------------------------------------------------------------------------------------------

void CRegelEditorFormView::RemoveDynGrp(CGridFenster *popGrid, RecordSet *popDgr)
{
	CString olChoiceList, olTmp;
	CString olRefTable;
	CString olTabNameComp;
	CString olGroupName;
	CString olTplUrno;
	CCSPtrArray <RecordSet> olDgrArr;

	for (int i = 1; i < popGrid->GetRowCount(); i++)
	{
		// get reference table from hidden column
		olRefTable = popGrid->GetValueRowCol(i, 8);
		if(olRefTable.IsEmpty() == FALSE) 
		{
			// find the suitable dynamic groups
			GetSelectedTplUrno(*pomComboBox, olTplUrno, false);
			olTabNameComp = popGrid->GetValueRowCol(i, 8);
			ogBCD.GetRecordsExt("DGR", "REFT", "UTPL", olTabNameComp, olTplUrno, &olDgrArr);
 			
			for(int j = 0; j < olDgrArr.GetSize (); j++)
			{
				olGroupName = olDgrArr[j].Values[igDgrGnamIdx];
				olChoiceList += olGroupName + "\n";
			}
			olDgrArr.DeleteAll();
			olChoiceList = olChoiceList.Left(olChoiceList.GetLength() - 1); // remove trailing newline character

			if(olChoiceList.IsEmpty() == FALSE)
			{
				popGrid->SetStyleRange(CGXRange(i + 1, DYNAMIC_GRP), CGXStyle().SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
															.SetChoiceList(olChoiceList));
			}
			olChoiceList.Empty();
		}
	}
}
//---------------------------------------------------------------------------------------------

void CRegelEditorFormView::RemoveGridListItem(CGridFenster *popGrid, CString opTable, RecordSet *popRecord)
{
	//--- exception handling
	CCS_TRY


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));	
	
	CString olFieldNameStr;
	CString olChoiceList;
	CString olTable;
	CString olField;
	CString olItem;
	CString olValue;
	CString *polValue;
	CStringArray olChoiceArr;
	CGXStyle olStyle;

	// search in rotation grid
	int ilRotCount = popGrid->GetRowCount();
	for (int ilRow = 1; ilRow < ilRotCount; ilRow++)
	{
		olFieldNameStr = popGrid->GetValueRowCol(ilRow, 7);
		olTable = olFieldNameStr.Left(3);
		olField = olFieldNameStr.Right(4);
		olItem = popRecord->Values[ogBCD.GetFieldIndex(olTable, olField)];
		if (olTable == opTable)
		{
			// look if item is in choice list already
			int ilIsFound = 0;
			CMapStringToPtr *polTableMap;
			polTableMap = ogBCD.GetMapPointer(olTable, olField);
			for(POSITION ilPos = polTableMap->GetStartPosition(); ilPos != NULL && ilIsFound < 2; )
			{
				polTableMap->GetNextAssoc(ilPos, olField, (void*&) polValue);
				olValue = *(CString*)polValue;
				if (olValue == olItem)
				{
					ilIsFound++;
				}
			}
			
			// remove item from choicelist
			if (ilIsFound < 2)
			{
				popGrid->GetStyleRowCol(ilRow, 2, olStyle);
				if ( olStyle.GetIncludeChoiceList () )	//hag990827
					olChoiceList = olStyle.GetChoiceList();
				else
					olChoiceList.Empty ();	//hag990827
				
				int ilStart = olChoiceList.Find(olItem);
				if (ilStart != -1)
				{
					int ilEnd = olChoiceList.Find('\n', ilStart);
					olChoiceList = olChoiceList.Left(ilStart) + olChoiceList.Right(olChoiceList.GetLength() - ilEnd - 1);
				}

				olStyle.SetChoiceList(olChoiceList);
				popGrid->SetStyleRange(CGXRange(ilRow, 2), olStyle);
			}
		}
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));	

	// exception handling
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------

void CRegelEditorFormView::ProcessDgrNew(RecordSet *popRecord)
{
	// This DDX call is not registered
}

void CRegelEditorFormView::ProcessDgrChange(RecordSet *popRecord)
{
	// This DDX call is not registered
}

void CRegelEditorFormView::ProcessDgrDelete(RecordSet *popRecord)
{
	RemoveDynGrp(pomRotGrid, popRecord);
	RemoveDynGrp(pomArrGrid, popRecord);
	RemoveDynGrp(pomDepGrid, popRecord);
}

void CRegelEditorFormView::ProcessPerNew(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessPerChange(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessPerDelete(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessRudNew(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessRudChange(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessRudDelete(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessRueNew(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessRueChange(RecordSet *popRecord)
{
	// mne test
	/*CString olTest;
	olTest = popRecord->Values[igRueUrnoIdx];
	olTest = popRecord->Values[igRueRusnIdx];
	olTest = popRecord->Values[igRueFisuIdx];
	olTest = popRecord->Values[igRueRustIdx];*/
	// end test
}

void CRegelEditorFormView::ProcessRueDelete(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessSgmNew(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessSgmChange(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessSgmDelete(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessSgrNew(RecordSet *popRecord)
{
	CCS_TRY

	//CString olGridRefTab;	// reference TAB.FLDN from grid
	CString olDdxRefTab;	// reference TAB.FLDN from record sent by DDX
	CString olGroupName;	// name of static group sent by DDX
	bool	blSgrFitsActTpl = true;

	olDdxRefTab = popRecord->Values[igSgrTabnIdx];
	olGroupName = popRecord->Values[igSgrGrpnIdx];

	blSgrFitsActTpl = IsSgrForActTpl ( popRecord );
	
	pomRotGrid->ChangeCondTableChoiceList ( olDdxRefTab, STATIC_GRP, 
											olGroupName, blSgrFitsActTpl );
	pomArrGrid->ChangeCondTableChoiceList ( olDdxRefTab, STATIC_GRP, 
											olGroupName, blSgrFitsActTpl );
	pomDepGrid->ChangeCondTableChoiceList ( olDdxRefTab, STATIC_GRP, 
											olGroupName, blSgrFitsActTpl );

	ChangeResChoiceLists ( olDdxRefTab, popRecord, true, blSgrFitsActTpl );

	CCS_CATCH_ALL	
}

void CRegelEditorFormView::ProcessSgrChange(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessSgrDelete(RecordSet *popRecord)
{
	CString olDdxRefTab;	// reference TAB.FLDN from record sent by DDX
	CString olGroupName;	// name of static group sent by DDX

	olDdxRefTab = popRecord->Values[igSgrTabnIdx];
	olGroupName = popRecord->Values[igSgrGrpnIdx];
	
	pomRotGrid->ChangeCondTableChoiceList ( olDdxRefTab, STATIC_GRP, 
											olGroupName, false );
	pomArrGrid->ChangeCondTableChoiceList ( olDdxRefTab, STATIC_GRP, 
											olGroupName, false );
	pomDepGrid->ChangeCondTableChoiceList ( olDdxRefTab, STATIC_GRP, 
											olGroupName, false );
	ChangeResChoiceLists ( olDdxRefTab, popRecord, true, false );
}

void CRegelEditorFormView::ProcessTplNew(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessTplChange(RecordSet *popRecord)
{

}

void CRegelEditorFormView::ProcessTplDelete(RecordSet *popRecord)
{
	
}



//---------------------------------------------------------------------------
//			implementation of static and global functions
//---------------------------------------------------------------------------
static void ProcessDetailCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CRegelEditorFormView *polMainDlg = (CRegelEditorFormView *)popInstance;

	// APT
	switch (ipDDXType)
	{
		case ACR_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "ACR", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "ACR", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "ACR", (RecordSet *)vpDataPointer);	
			break;

		case ACR_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "ACR", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "ACR", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "ACR", (RecordSet *)vpDataPointer);	
			break;

		case ACR_CHANGE:
			break;

		case ACT_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "ACT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "ACT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "ACT", (RecordSet *)vpDataPointer);	
			break;

		case ACT_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "ACT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "ACT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "ACT", (RecordSet *)vpDataPointer);	
			break;

		case ACT_CHANGE:
			break;

		case ALT_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "ALT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "ALT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "ALT", (RecordSet *)vpDataPointer);	
			break;

		case ALT_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "ALT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "ALT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "ALT", (RecordSet *)vpDataPointer);	
			break;

		case ALT_CHANGE:
			break;

		case APT_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "APT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "APT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "APT", (RecordSet *)vpDataPointer);	
			break;

		case APT_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "APT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "APT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "APT", (RecordSet *)vpDataPointer);	
			break;

		case APT_CHANGE:
			break;

		case BLT_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "BLT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "BLT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "BLT", (RecordSet *)vpDataPointer);	
			break;

		case BLT_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "BLT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "BLT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "BLT", (RecordSet *)vpDataPointer);	
			break;

		case BLT_CHANGE:
			break;

		case CCC_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "CCC", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "CCC", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "CCC", (RecordSet *)vpDataPointer);	
			break;

		case CCC_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "CCC", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "CCC", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "CCC", (RecordSet *)vpDataPointer);	
			break;

		case CCC_CHANGE:
			break;

		case CHT_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "CHT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "CHT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "CHT", (RecordSet *)vpDataPointer);	
			break;

		case CHT_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "CHT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "CHT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "CHT", (RecordSet *)vpDataPointer);	
			break;

		case CHT_CHANGE:
			break;

		case CIC_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "CIC", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "CIC", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "CIC", (RecordSet *)vpDataPointer);	
			break;

		case CIC_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "CIC", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "CIC", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "CIC", (RecordSet *)vpDataPointer);	
			break;

		case CIC_CHANGE:
			break;

		case DGR_NEW:
			polMainDlg->ProcessDgrNew((RecordSet *)vpDataPointer);
			break;

		case DGR_DELETE:
			polMainDlg->ProcessDgrDelete((RecordSet *)vpDataPointer);
			break;

		case DGR_CHANGE:
			polMainDlg->ProcessDgrChange((RecordSet *)vpDataPointer);
			break;

		case EQU_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "EQU", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "EQU", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "EQU", (RecordSet *)vpDataPointer);	
			break;

		case EQU_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "EQU", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "EQU", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "EQU", (RecordSet *)vpDataPointer);	
			break;

		case EQU_CHANGE:
			break;

		case EXT_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "EXT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "EXT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "EXT", (RecordSet *)vpDataPointer);	
			break;

		case EXT_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "EXT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "EXT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "EXT", (RecordSet *)vpDataPointer);	
			break;

		case EXT_CHANGE:
			break;

		case GAT_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "GAT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "GAT", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "GAT", (RecordSet *)vpDataPointer);	
			break;

		case GAT_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "GAT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "GAT", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "GAT", (RecordSet *)vpDataPointer);	
			break;

		case GAT_CHANGE:
			break;

		case GEG_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "GEG", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "GEG", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "GEG", (RecordSet *)vpDataPointer);	
			break;

		case GEG_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "GEG", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "GEG", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "GEG", (RecordSet *)vpDataPointer);	
			break;

		case GEG_CHANGE:
			break;

		case HAG_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "HAG", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "HAG", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "HAG", (RecordSet *)vpDataPointer);	
			break;

		case HAG_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "HAG", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "HAG", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "HAG", (RecordSet *)vpDataPointer);	
			break;

		case HAG_CHANGE:
			break;

		case HTY_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "HTY", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "HTY", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "HTY", (RecordSet *)vpDataPointer);	
			break;

		case HTY_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "HTY", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "HTY", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "HTY", (RecordSet *)vpDataPointer);	
			break;

		case HTY_CHANGE:
			break;

		case PER_NEW:
			polMainDlg->ProcessPerNew((RecordSet *)vpDataPointer);
			break;

		case PER_DELETE:
			polMainDlg->ProcessPerDelete((RecordSet *)vpDataPointer);
			break;

		case PER_CHANGE:
			polMainDlg->ProcessPerChange((RecordSet *)vpDataPointer);
			break;

		case PST_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "PST", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "PST", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "PST", (RecordSet *)vpDataPointer);	
			break;

		case PST_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "PST", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "PST", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "PST", (RecordSet *)vpDataPointer);	
			break;

		case PST_CHANGE:
			break;

		case RUD_NEW:
			polMainDlg->ProcessRudNew((RecordSet *)vpDataPointer);
			break;

		case RUD_DELETE:
			polMainDlg->ProcessRudDelete((RecordSet *)vpDataPointer);
			break;

		case RUD_CHANGE:
			polMainDlg->ProcessRudChange((RecordSet *)vpDataPointer);
			break;

		case RUE_NEW:
			polMainDlg->ProcessRueNew((RecordSet *)vpDataPointer);
			break;

		case RUE_DELETE:
			polMainDlg->ProcessRueDelete((RecordSet *)vpDataPointer);
			break;

		case RUE_CHANGE:
			polMainDlg->ProcessRueChange((RecordSet *)vpDataPointer);
			break;

		case RWY_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "RWY", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "RWY", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "RWY", (RecordSet *)vpDataPointer);	
			break;

		case RWY_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "RWY", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "RWY", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "RWY", (RecordSet *)vpDataPointer);	
			break;

		case RWY_CHANGE:
			break;
		
			case SGM_NEW:
			polMainDlg->ProcessSgmNew((RecordSet *)vpDataPointer);
			break;

		case SGM_DELETE:
			polMainDlg->ProcessSgmDelete((RecordSet *)vpDataPointer);
			break;

		case SGM_CHANGE:
			polMainDlg->ProcessSgmChange((RecordSet *)vpDataPointer);
			break;

		case SGR_NEW:
			polMainDlg->ProcessSgrNew((RecordSet *)vpDataPointer);
			break;

		case SGR_DELETE:
			polMainDlg->ProcessSgrDelete((RecordSet *)vpDataPointer);
			break;

		case SGR_CHANGE:
			polMainDlg->ProcessSgrNew((RecordSet *)vpDataPointer);
			break;

		case TPL_NEW:
			polMainDlg->ProcessTplNew((RecordSet *)vpDataPointer);
			break;

		case TPL_DELETE:
			polMainDlg->ProcessTplDelete((RecordSet *)vpDataPointer);
			break;

		case TPL_CHANGE:
			polMainDlg->ProcessTplChange((RecordSet *)vpDataPointer);
			break;

		case TWY_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "TWY", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "TWY", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "TWY", (RecordSet *)vpDataPointer);	
			break;

		case TWY_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "TWY", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "TWY", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "TWY", (RecordSet *)vpDataPointer);	
			break;

		case TWY_CHANGE:
			break;

		case WRO_NEW:
			polMainDlg->AddGridListItem(polMainDlg->pomRotGrid, "WRO", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomArrGrid, "WRO", (RecordSet *)vpDataPointer);	
			polMainDlg->AddGridListItem(polMainDlg->pomDepGrid, "WRO", (RecordSet *)vpDataPointer);	
			break;

		case WRO_DELETE:
			polMainDlg->RemoveGridListItem(polMainDlg->pomRotGrid, "WRO", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomArrGrid, "WRO", (RecordSet *)vpDataPointer);	
			polMainDlg->RemoveGridListItem(polMainDlg->pomDepGrid, "WRO", (RecordSet *)vpDataPointer);	
			break;

		case WRO_CHANGE:
			break;
		
		
		default:
			break;

	}
	
}
//-----------------------------------------------------------------------------------------------------------------------

int StringComp(const RecordSet** ppRecord1, const RecordSet** ppRecord2)
{
	RecordSet olRecord1 = *(*ppRecord1);
	RecordSet olRecord2 = *(*ppRecord2);

    CString olGroupName1 = olRecord1[2];
    CString olGroupName2 = olRecord2[2];

	return olGroupName1.Collate(olGroupName2);
}



//---------------------------------------------------------------------------
//					testing  functions 
//---------------------------------------------------------------------------
void CRegelEditorFormView::Dump1(CString opMsg, int ipXVal, CTime opTime)
{
	TRACE("[%s] X-Value: %d, Time: %s\n", opMsg, ipXVal, opTime.Format("%H:%M:%S"));
}
//---------------------------------------------------------------------------

void CRegelEditorFormView::CalcGanttRect( LPRECT lpRect )
{
	// get gantt rect
	CWnd *polStaticWnd = GetDlgItem(IDC_GANTT_RECT);
	polStaticWnd->GetWindowRect(lpRect);
	ScreenToClient(lpRect);
	polStaticWnd->ShowWindow(SW_HIDE);
}
//---------------------------------------------------------------------------

void CRegelEditorFormView::CalcTimeScaleRect( LPRECT lpRect )
{
	// get view button rect as an anchor-point for the gantt
	CRect olRect;
	CWnd *polAnchor = GetDlgItem(IDC_ARR_LIST);
	polAnchor->GetWindowRect(olRect);
	ScreenToClient(olRect);
	
	// get RegelWerkView's client rect
	CRect olClientRect;
	GetClientRect(olClientRect);

	// compute size for timescale rect
	int ilWidth = olClientRect.Width();
	lpRect->top = olRect.bottom + 50;
	lpRect->left = olRect.left + imVerticalScaleWidth + 1;
	lpRect->right = ilWidth - 2 * olRect.left;
	lpRect->bottom = lpRect->top + 32;
}
//---------------------------------------------------------------------------

void CRegelEditorFormView::AddServices ( CStringArray	&ropSerUrnos )
{
	CString olEvty;
	CString		olTplUrno, olTplDalo, *polAloc=0;
	
	if(ropSerUrnos.GetSize() > 0)
	{
		if (omCurrRueUrno != "")
		{
			if (pomComboBox)
			{		
				if (GetSelectedTplUrno (*pomComboBox, olTplUrno, FALSE, true))
				{
					olTplDalo = ogBCD.GetField("TPL", "URNO", olTplUrno, "DALO");
					if (olTplDalo.IsEmpty() == FALSE)
					{
						polAloc = &olTplDalo;
					}
				}
			}

			olEvty = GetRuleEventType();
			ogDataSet.CreateRudFromSerList(true, ropSerUrnos, omCurrRueUrno, olTplUrno, olEvty, polAloc, bmCreateGroup);
			m_HScroll.SetScrollPos(0);
			m_HScroll.Invalidate();
		}
	}
}
//---------------------------------------------------------------------------



bool CRegelEditorFormView::CheckChanges()
{
	bool blRet = true;
	CString olTmp;
	CString olDebug;
	int idx;


	CCS_TRY

	if (bmNewFlag == false)
	{
		RecordSet olRue(ogBCD.GetFieldCount("RUE"));
		ogBCD.GetRecord("RUE", "URNO", omCurrRueUrno, olRue);
		
		// RUSN
		m_RuleShortNameEdit.GetWindowText(olTmp);
		olDebug = olRue.Values[igRueRusnIdx];
		blRet &= (olRue.Values[igRueRusnIdx] == olTmp);
		// mne test
		idx = -1;
		Debug(blRet, idx);

		// RUNA
		m_RuleNameEdit.GetWindowText(olTmp);	
		blRet &= (olRue.Values[igRueRunaIdx] == olTmp);
		Debug(blRet, idx);

		// EVTY
		olTmp = m_RBtn_Turnaround.GetCheck() ? "0" : (m_RBtn_Inbound.GetCheck() ? "1" : (m_RBtn_Outbound.GetCheck() ? "2" : "-1"));
		blRet &= (olRue.Values[igRueEvtyIdx] == olTmp);
		Debug(blRet, idx);

		// EXCL
		olTmp = m_Chb_Excl.GetCheck() ? "1" : "0";
		blRet &= (olRue.Values[igRueExclIdx] == olTmp);
		if ( !blRet )
		{
			TRACE("DEBUG", "CRegelEditorFormView::CheckChanges: EXCL different <%s>  <%s>\n", 
				  olRue.Values[igRueEvtyIdx], olTmp);
		}
		Debug(blRet, idx);

		// FSPL
		olTmp = m_Chb_Fspl.GetCheck() ? "1" : "0";
		blRet &= (olRue.Values[igRueFsplIdx] == olTmp);
		Debug(blRet, idx);

		CString olTest;
		if (olTmp == "1")
		{
			// MIST
			m_Edt_MinTime.GetWindowText(olTmp);
			olTest = (olRue.Values[igRueMistIdx]);
			olTest.TrimLeft();
			if (olTmp == "0")
			{
				olTmp.Empty();
			}
			blRet &= (olTest == olTmp);
		}
		Debug(blRet, idx);
		
		// MAXT
		if ( m_RBtn_Turnaround.GetCheck() )
		{
			m_MaxtEdit.GetWindowText(olTmp);
			olTest = (olRue.Values[igRueMaxtIdx]);
			olTest.TrimLeft();
			/*
			if (olTmp == "0") // taken out, mne 001018
			{
				olTmp.Empty();
			}*/
			blRet &= (olTest == olTmp);
			Debug(blRet, idx);
			if ( !blRet )
			{
				TRACE( "CRegelEditorFormView::CheckChanges: MAXT different <%s>  <%s>\n", 
					  olRue.Values[igRueMaxtIdx], olTmp);
			}
		}

		// RUST
		olTmp = (m_Chb_Acti.GetCheck() ? "0" : "1");
		blRet &= (olRue.Values[igRueRustIdx] == olTmp);
		Debug(blRet, idx);
		if ( !blRet )
		{
			TRACE( "CRegelEditorFormView::CheckChanges: RUST different <%s>  <%s>\n", 
				  olRue.Values[igRueRustIdx], olTmp);
		}

		// RUTY
		/*olTmp = (m_RBtn_Single.GetCheck() ? "0" : (m_RBtn_Collective.GetCheck() ? "1" : "-1"));
		olTest = olRue.Values[igRueRutyIdx];
		blRet &= (olRue.Values[igRueRutyIdx] == olTmp);
		Debug(blRet, idx);*/

		//if ( m_RBtn_Single.GetCheck() )	//now always SingleRule
		{
			ConstructString(pomRotGrid, olTmp, 400 ) ;
			olTest = olRue.Values[igRueEvttIdx];
			blRet &= (olRue.Values[igRueEvttIdx] == olTmp);
			if (bmOutbound == false)
			{
				ConstructString(pomArrGrid, olTmp, 800 );
				olTest = olRue.Values[igRueEvtaIdx];
				blRet &= (olRue.Values[igRueEvtaIdx] == olTmp);
			}
			if (bmInbound == false)
			{
				ConstructString(pomDepGrid, olTmp, 800 );
				olTest = olRue.Values[igRueEvtdIdx];
				blRet &= (olRue.Values[igRueEvtdIdx] == olTmp);
			}
		}

		// mne test
/*		if (!blRet)
		{
			ogLog.Trace("DEBUG", "[RegelWerkView::CheckChanges] differences found comparing RUE\n");	
		}*/
		// end test
	}
	else
	{
		// RUSN
		m_RuleShortNameEdit.GetWindowText(olTmp);
		blRet &= (olTmp == CString(""));

		// RUNA
		m_RuleNameEdit.GetWindowText(olTmp);
		blRet &= (olTmp == CString(""));

		// MAXT
		m_MaxtEdit.GetWindowText(olTmp);
		blRet &= (olTmp == CString(""));

		// mne debug
		if (!blRet)
			TRACE("ELSE-ZWEIG\n");
		// end debug
	}


	CCS_CATCH_ALL
	if ( !blRet )
		TRACE ( "CRegelEditorFormView::CheckChanges:  Record changed URUE=<%s>", omCurrRueUrno );
	return blRet;
}
//---------------------------------------------------------------------------

void CRegelEditorFormView::Debug(bool bRes, int &idx)
{
	idx++;

	if (!bRes)
	{
		TRACE("[CRegelEditorFormView::Debug] Checking says change occurred at index %d\n", idx);
	}
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
