// ValidityDlg.cpp : implementation file
//

#include <stdafx.h>

#include <CedaBasicData.h>

#include <Regelwerk.h>
#include <ValidityDlg.h>
#include <CCSGlobl.h>
#include <DDXDDV.h>
#include <gxall.h>
#include <BasicData.h>
#include <ruleslist.h>
#include <MainFrm.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif


//-----------------------------------------------------------------------------------------------------------------------
//				defines
//-----------------------------------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------------------------
//				construction / destruction
//-----------------------------------------------------------------------------------------------------------------------
ValidityDlg::ValidityDlg(CString opRueUrno, bool bpNew, CWnd* pParent /*=NULL*/)
	: CDialog(ValidityDlg::IDD, pParent)
{
	CCS_TRY
	
		
	omRueUrno = opRueUrno;
	bmFixedTimes = false;
	bmIsGridEnabled = false;
	bmNewFlag = bpNew;

	//{{AFX_DATA_INIT(ValidityDlg)
	m_bUnlimited = FALSE;
	//}}AFX_DATA_INIT
	
	pomParent = pParent;
	pomGrid = new CGridFenster(this);
	
	imExclCount = 0;

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

ValidityDlg::~ValidityDlg()
{
	if (rmValData.Excls.GetSize() > 0)
	{
		rmValData.Excls.DeleteAll();
	}
	delete pomGrid;
}


//-----------------------------------------------------------------------------------------------------------------------
//				data exchange, message map
//-----------------------------------------------------------------------------------------------------------------------
void ValidityDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ValidityDlg)
	DDX_Control(pDX, IDC_COMBO_VTYPE, m_ValidityType);
	DDX_Control(pDX, IDC_UNLIMITED_CHK, m_Chb_Unlimited);
	DDX_Control(pDX, IDC_TIMETO, m_TimeToCtrl);
	DDX_Control(pDX, IDC_TIMEFROM, m_TimeFromCtrl);
	DDX_Control(pDX, IDC_FIXED_TO, m_FixTimeTo);
	DDX_Control(pDX, IDC_FIXED_FROM, m_FixTimeFrom);
	DDX_Control(pDX, IDC_DATEFROM, m_DateFromCtrl);
	DDX_Control(pDX, IDC_DATETO, m_DateToCtrl);
	DDX_Control(pDX, IDC_FIXED_BTN, m_FixedBtn);
	DDX_Control(pDX, IDC_TIME_LBL4, m_TimeStatic4);
	DDX_Control(pDX, IDC_TIME_LBL3, m_TimeStatic3);
	DDX_Control(pDX, IDC_TIME_LBL2, m_TimeStatic2);
	DDX_Control(pDX, IDC_TIME_LBL1, m_TimeStatic);
	DDX_Control(pDX, IDC_UNTIL_STATIC, m_UntilFrame);
	DDX_Control(pDX, IDC_FROM_STATIC, m_FromFrame);
	DDX_Control(pDX, IDC_DETAILS_BTN, m_DetailsBtn);
	DDX_Control(pDX, IDC_WEDNESDAY_CHB, m_WednesdayChb);
	DDX_Control(pDX, IDC_TUESDAY_CHB, m_TuesdayChb);
	DDX_Control(pDX, IDC_THURSDAY_CHB, m_ThursdayChb);
	DDX_Control(pDX, IDC_SUNDAY_CHB, m_SundayChb);
	DDX_Control(pDX, IDC_SATURDAY_CHB, m_SaturdayChb);
	DDX_Control(pDX, IDC_MONDAY_CHB, m_MondayChb);
	DDX_Control(pDX, IDC_FRIDAY_CHB, m_FridayChb);
	DDX_Control(pDX, IDC_UNTIL_SLIDER, m_UntilSlider);
	DDX_Control(pDX, IDC_FROM_SLIDER, m_FromSlider);
	DDX_Check(pDX, IDC_UNLIMITED_CHK, m_bUnlimited);
	//}}AFX_DATA_MAP
}
//-----------------------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(ValidityDlg, CDialog)
	//{{AFX_MSG_MAP(ValidityDlg)
	ON_BN_CLICKED(IDC_INVERT_BTN, OnInvert)
	ON_BN_CLICKED(IDC_DETAILS_BTN, OnDetails)
	ON_WM_LBUTTONDOWN()
	ON_WM_HSCROLL()
	ON_MESSAGE(GRID_MESSAGE_STARTEDITING, OnStartEditing)
	ON_MESSAGE(GRID_MESSAGE_ENDEDITING, OnEndEditing)
	ON_MESSAGE(GRID_MESSAGE_CELLCLICK, OnGridClick)
	ON_BN_CLICKED(IDC_FIXED_BTN, OnFixedBtn)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_FIXED_FROM, OnChangeFixedFrom)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_FIXED_TO, OnChangeFixedTo)
	ON_BN_CLICKED(IDC_UNLIMITED_CHK, OnUnlimitedChk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//-----------------------------------------------------------------------------------------------------------------------
//				message handlers
//-----------------------------------------------------------------------------------------------------------------------
BOOL ValidityDlg::OnInitDialog() 
{
	CCS_TRY

	CDialog::OnInitDialog();
	
	SetStaticTexts();

	//---    Initialize grids
	BOOL blErr = pomGrid->SubclassDlgItem(IDC_CUSTOM, this);
	pomGrid->Initialize();
    InitializeTable();

	COleDateTime olMinCTime ( 1970, 1, 1, 0, 0, 0 );
	COleDateTime olMaxCTime ( 2037, 12, 31, 23, 59, 59 );

	m_DateFromCtrl.SetRange( &olMinCTime, &olMaxCTime );
	m_DateToCtrl.SetRange( &olMinCTime, &olMaxCTime );
	
	// valid from 
	CString olTmp;
	CTime olTime;
	olTmp = rmValData.Vafr;
	if (olTmp.GetLength() == 14)
	{
		olTime = DBStringToDateTime(olTmp);
		m_DateFromCtrl.SetTime(&olTime);
		m_TimeFromCtrl.SetTime(&olTime);
	}
	else
	{
		SetVafrToCurrentTime();
	}

	// valid to
	olTmp = rmValData.Vato;
	m_bUnlimited = (olTmp.GetLength() < 14);
	if (m_bUnlimited == FALSE)
	{
		olTime = DBStringToDateTime(olTmp);
		m_DateToCtrl.SetTime(&olTime);
		m_TimeToCtrl.SetTime(&olTime);
	}
	else
	{
		olTime = CTime::GetCurrentTime();
		int ilYear = olTime.GetYear();
		olTime = CTime(ilYear + 10, olTime.GetMonth(), olTime.GetDay(), 23, 59, 59);
		m_DateToCtrl.SetTime(&olTime);
		m_TimeToCtrl.SetTime(&olTime);
	}

	// frequency
	CString olFreq;
	olFreq = rmValData.Freq;
	if (olFreq.IsEmpty() == FALSE && olFreq != CString(" "))	//  hag20010108
	{
		olFreq.Mid(0, 1) == CString("1")  ?  m_MondayChb.SetCheck(CHECKED)    :  m_MondayChb.SetCheck(UNCHECKED);
		olFreq.Mid(1, 1) == CString("1")  ?  m_TuesdayChb.SetCheck(CHECKED)   :  m_TuesdayChb.SetCheck(UNCHECKED);
		olFreq.Mid(2, 1) == CString("1")  ?  m_WednesdayChb.SetCheck(CHECKED) :  m_WednesdayChb.SetCheck(UNCHECKED);
		olFreq.Mid(3, 1) == CString("1")  ?  m_ThursdayChb.SetCheck(CHECKED)  :  m_ThursdayChb.SetCheck(UNCHECKED);
		olFreq.Mid(4, 1) == CString("1")  ?  m_FridayChb.SetCheck(CHECKED)    :  m_FridayChb.SetCheck(UNCHECKED);
		olFreq.Mid(5, 1) == CString("1")  ?  m_SaturdayChb.SetCheck(CHECKED)  :  m_SaturdayChb.SetCheck(UNCHECKED);
		olFreq.Mid(6, 1) == CString("1")  ?  m_SundayChb.SetCheck(CHECKED)    :  m_SundayChb.SetCheck(UNCHECKED);
	}
	else
	{
		m_MondayChb.SetCheck(CHECKED);
		m_TuesdayChb.SetCheck(CHECKED);
		m_WednesdayChb.SetCheck(CHECKED);
		m_ThursdayChb.SetCheck(CHECKED);
		m_FridayChb.SetCheck(CHECKED);
		m_SaturdayChb.SetCheck(CHECKED);
		m_SundayChb.SetCheck(CHECKED);
	}
	
	// excludes
	int ilExclCount = rmValData.Excls.GetSize();
	pomGrid->SetRowCount(1);
	CString olStartDate, olEndDate;
	
	for (int i = 0; i < ilExclCount; i++)
	{
		olStartDate = rmValData.Excls[i].FirstDay.Format("%d.%m.%Y");
		olEndDate = rmValData.Excls[i].LastDay.Format("%d.%m.%Y");

		pomGrid->SetValueRange(CGXRange(i + 1, 1), olStartDate);
		pomGrid->SetValueRange(CGXRange(i + 1, 2), olEndDate);
		pomGrid->DoAutoGrow(i + 1, 1);
	}

	//-- fixed times 
	int n = 0, ilFixedFrom, ilFixedTill;

	if (rmValData.FixF[0])
	{
		n = sscanf(rmValData.FixF, "%d", &ilFixedFrom);
	}

	if (rmValData.FixT[0])
	{
		n += sscanf(rmValData.FixT, "%d", &ilFixedTill);
	}
	
	m_FromSlider.SetRange(0, 1439, FALSE);
	m_FromSlider.SetTicFreq(60);
	m_UntilSlider.SetRange(0, 1439, TRUE);
	m_UntilSlider.SetTicFreq(60);

	bmFixedTimes = (n == 2);

	m_FromSlider.SetPos(bmFixedTimes ? ilFixedFrom : 0);
	m_UntilSlider.SetPos(bmFixedTimes ? ilFixedTill : 1439);

	if (bmFixedTimes)
	{
		CTime olTime;
		olTime = GetTimeFromMinutes(ilFixedFrom);
		m_FixTimeFrom.SetTime(&olTime);

		olTime = GetTimeFromMinutes(ilFixedTill);
		m_FixTimeTo.SetTime(&olTime);
		SetSliderEnabled(TRUE);
	}
	else
	{
		SetSliderEnabled(FALSE);
	}
	
	/*m_FromSlider.ClearTics(bmFixedTimes);
	m_UntilSlider.ClearTics(bmFixedTimes);*/
	
	//added by MAX
	//validity type ,VTYP
	if (bgUseValidityType == true)
	{
		if (igValVtypIdx == -1)
			m_ValidityType.EnableWindow(false);
		else
		{
			if (rmValData.IsTurnaround == true)
			{
				m_ValidityType.ShowWindow(SW_SHOWNORMAL);
				m_ValidityType.InsertString(0,"TIFA");
				m_ValidityType.InsertString(1,"TIFD");
				m_ValidityType.InsertString(2,"TIFAD");
				if (strcmp(rmValData.Vtyp ,"TIFA")==NULL)
				{
					m_ValidityType.SetCurSel(0);
				}
				else if (strcmp(rmValData.Vtyp ,"TIFD")==NULL)
				{
					m_ValidityType.SetCurSel(1);
				}
				else if (strncmp(rmValData.Vtyp ,"TIFAD",5)==NULL)
				{
					m_ValidityType.SetCurSel(2);
				}
				else
					m_ValidityType.SetCurSel(-1);
			}
		    else
			{
				CWnd *pWnd = GetDlgItem(IDC_STATIC1);
				if (pWnd  != NULL)
					pWnd->ShowWindow(HIDE_WINDOW);
			    m_ValidityType.ShowWindow(HIDE_WINDOW);
			}
		}
	}
	else
	{
		CWnd *pWnd = GetDlgItem(IDC_STATIC1);
		if (pWnd  != NULL)
		pWnd->ShowWindow(HIDE_WINDOW);
		m_ValidityType.ShowWindow(HIDE_WINDOW);
	}

	// grid
	pomGrid->SetSortingEnabled(false);
		
	m_DateToCtrl.EnableWindow(!m_bUnlimited);
	m_TimeToCtrl.EnableWindow(!m_bUnlimited);
	UpdateData(FALSE);

	// hide details part of window
	if ((bmFixedTimes == false) && (ilExclCount < 1) && (olFreq == CString("1111111")))
	{
		CRect olRect;
		GetWindowRect(&olRect);
		olRect.top -= 20;
		olRect.bottom -= 460;
		MoveWindow(olRect);
		bmDetails = false;
	}
	else
	{
		CString olText;
		m_DetailsBtn.GetWindowText(olText);
		olText.Replace(">","<");
		m_DetailsBtn.SetWindowText(olText);
		bmDetails = true;
	}
	

	bgIsValSaved = false;

	CMainFrame *polMainWnd = (CMainFrame*) AfxGetMainWnd();
	if ( !polMainWnd || !polMainWnd->CanModifyActualTemplate () )
	{
		CWnd *pWnd = GetDlgItem(IDOK);
		if (pWnd  != NULL)
			pWnd->EnableWindow(FALSE);
	}
	CCS_CATCH_ALL

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnOK() 
{
	CCS_TRY

	// make wait cursor 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));	

	bool blCanSave = true;		//true: Everything OK, data can be written to DB

//--- validate data and fill data struct
	CTime olVafr;
	CTime olVato;
	CTime olDafr;
	CTime olDato;
	CTime olTito;
	CTime olTifr;
	CTime olToday;

	//-- get dates and times
	m_DateFromCtrl.GetTime(olDafr);
	m_TimeFromCtrl.GetTime(olTifr);
	m_DateToCtrl.GetTime(olDato);
	m_TimeToCtrl.GetTime(olTito);

	olToday = CTime::GetCurrentTime();
	olToday = CTime(olToday.GetYear(), olToday.GetMonth(), olToday.GetDay(), 0, 0, 0);
	olVafr = CTime(olDafr.GetYear(), olDafr.GetMonth(), olDafr.GetDay(), olTifr.GetHour(), olTifr.GetMinute(), 0);
	olVato = CTime(olDato.GetYear(), olDato.GetMonth(), olDato.GetDay(), olTito.GetHour(), olTito.GetMinute(), 0);
	

	
	//-- validate
	if ((!m_bUnlimited &&(olVafr >= olVato)) || ((olVafr < olToday) && bmNewFlag))
	{
		SetVafrToCurrentTime();
		MessageBox(GetString(1428), GetString(AFX_IDS_APP_TITLE), MB_OK | MB_ICONEXCLAMATION);
	}
	else
	{
		//-- vafr string
		CString olText;
		olText = CTimeToDBString(olVafr, olVafr); 
		strcpy(rmValData.Vafr, olText);
		
		//-- vato string
		if (m_bUnlimited)
		{
			rmValData.Vato[0]='\0';
		}
		else
		{
			olText = CTimeToDBString(olVato, olVato);
			strcpy(rmValData.Vato, olText);
		}

		//-- frequency
		if ((m_MondayChb.GetCheck() || m_TuesdayChb.GetCheck() || m_WednesdayChb.GetCheck() || m_ThursdayChb.GetCheck() || 
			 m_FridayChb.GetCheck() || m_SaturdayChb.GetCheck() || m_SundayChb.GetCheck()) == UNCHECKED)
		{
			int ilReply = MessageBox(GetString(1435), GetString(1433), MB_YESNO);
			if (ilReply == IDNO)
			{
				blCanSave = false;
			}
		}

		olText = "";
		m_MondayChb.GetCheck()    ?  olText += CString("1")  :  olText += CString("0");
		m_TuesdayChb.GetCheck()   ?  olText += CString("1")  :  olText += CString("0");
		m_WednesdayChb.GetCheck() ?  olText += CString("1")  :  olText += CString("0");
		m_ThursdayChb.GetCheck()  ?  olText += CString("1")  :  olText += CString("0");
		m_FridayChb.GetCheck()    ?  olText += CString("1")  :  olText += CString("0");
		m_SaturdayChb.GetCheck()  ?  olText += CString("1")  :  olText += CString("0");
		m_SundayChb.GetCheck()    ?  olText += CString("1")  :  olText += CString("0");
		strcpy(rmValData.Freq, olText);
		

		//-- get excluded days from grid
		CString olStartDate, olEndDate;
		EXCLUDES rlExcl;

		int ilCount = pomGrid->GetRowCount();
		if (ilCount > 1)
		{
			SortExcludes();
		}
		rmValData.Excls.DeleteAll ();	// hag991129
		for (int i = 1; i <= ilCount; i++)
		{
			olStartDate = pomGrid->GetValueRowCol(i, 1);
			olEndDate = pomGrid->GetValueRowCol(i, 2);
			
			if (olStartDate.IsEmpty() == FALSE && olStartDate != CString(" "))
			{
				if (olEndDate.IsEmpty() == TRUE)
				{
					olEndDate = olStartDate;
				}

				rlExcl.FirstDay = DateStringToDate(olStartDate);
				rlExcl.LastDay = DateStringToDate(olEndDate);
				rlExcl.LastDay += CTimeSpan(0,23,59,59);
					
				rmValData.Excls.New(rlExcl);
			}
		}

		//-- fixed times 
		if (bmFixedTimes == true) 
		{
			CTime olTimeFrom;
			m_FixTimeFrom.GetTime(olTimeFrom);
			// ***** Hier liegt noch ein Fehler vor ! GetMinutesFromTime liefert keine Zeit !!!
			sprintf(rmValData.FixF, "%d", GetMinutesFromTime(olTimeFrom));

			CTime olTimeTo;
			m_FixTimeTo.GetTime(olTimeTo);
			sprintf(rmValData.FixT, "%d", GetMinutesFromTime(olTimeTo));
		}
		else
		{
			rmValData.FixF[0] = '\0';
			rmValData.FixT[0] = '\0';
		}
		
		//-- validity type
		//added by MAX
		if (rmValData.IsTurnaround == true && bgUseValidityType == true)
		{
			int myIdx = m_ValidityType.GetCurSel();
			if (myIdx == -1)
			{
				//char *tmp=' ';
				strcpy(rmValData.Vtyp ," ");
			}
			else
			{
				CString olText;
				m_ValidityType.GetLBText(myIdx,olText);
				strcpy(rmValData.Vtyp ,olText.GetBuffer(0));
			}
		}
		
	//--- save data
		if (blCanSave == true)
		{
			// rmValData.IsFilled = true;	// superfluous. MNE. 000124
			CString olRueUrno;
			CMainFrame *polMainWnd = (CMainFrame*) AfxGetMainWnd();
			
			olRueUrno = polMainWnd->GetRueUrno();
			if ( olRueUrno != "-1" )	//hag20011219:	PRF2551 save would lead to ORACLE-error 
			{							//				and record w/o reference to a rule
				if (SaveValidity(olRueUrno, rmValData, bmNewFlag) == true)
				{
					bgIsValSaved = true;
				}
			}
			else
				TRACE ( "SaveValidity: Didn't save validity because RuleUrno=-1\n" );
			CDialog::OnOK();
		}
		else
		{
			Beep(900, 150);
		}
	}

	// make regular cursor 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnStartEditing(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

	ROWCOL ilCol = 0;
	ROWCOL ilRow = 0;
	CELLPOS *pCellPos;
	pCellPos = (CELLPOS*)lParam;
	ilCol = pCellPos->Col;
	ilRow = pCellPos->Row;

	if (ilRow > 0)
	{
		if (ilCol == 2)
		{
			CString olText, olText2;
			olText = pomGrid->GetValueRowCol(ilRow, 1);
			olText2 = pomGrid->GetValueRowCol(ilRow, 2);
			if ( olText2.IsEmpty () )
				pomGrid->SetValueRange(CGXRange(ilRow, 2), olText);
		}
	}

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnEndEditing(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

	ROWCOL ilCol = 0;
	ROWCOL ilRow = 0;
	CELLPOS *pCellPos;
	pCellPos = (CELLPOS*)lParam;
	ilCol = pCellPos->Col;
	ilRow = pCellPos->Row;
	
	// don't run through function when destroying 
	// the surrounding window
	CGXControl *polCellControl;
	polCellControl = pomGrid->GetControl(ilRow, ilCol);

	if (polCellControl != NULL)
	{
		if (polCellControl->GetModify() == FALSE)
		{
			return;	
		}
	}


	bool blValid = false;
	CTime olVafr, olVato, olDate2Test;
	CTime olDafr, olDato, olTifr, olTito;

	CString olText;
	olText = pomGrid->GetValueRowCol(ilRow, ilCol);

	if (ilRow > 0)
	{
		m_DateFromCtrl.GetTime(olDafr);
		m_TimeFromCtrl.GetTime(olTifr);
		m_DateToCtrl.GetTime(olDato);
		m_TimeToCtrl.GetTime(olTito);

		olVafr = CTime(olDafr.GetYear(), olDafr.GetMonth(), olDafr.GetDay(), olTifr.GetHour(), olTifr.GetMinute(), 0);
		olVato = CTime(olDato.GetYear(), olDato.GetMonth(), olDato.GetDay(), olTito.GetHour(), olTito.GetMinute(), 0);
		
		if (ilCol == 1)
		{
			blValid = (DateStringToDateExt(olText, olDate2Test) == TRUE);
			blValid &= CheckExclIsValid(olDate2Test, ilRow, ilCol);
			if ((blValid == false)/* || (olDate2Test <= olVafr) || 
					((olDate2Test >= olVato) && (m_Chb_Unlimited.GetCheck() == UNCHECKED))*/)
			{
				Beep(800, 200);
				pomGrid->SetValueRange(CGXRange(ilRow, ilCol), CString(""));
				pomGrid->DeleteEmptyRow(ilRow, ilCol);
			}
		}
	
		if (ilCol == 2)
		{
			CString olLeft = pomGrid->GetValueRowCol(ilRow, 1);
			if (olLeft.IsEmpty() == TRUE)
			{
				Beep(800, 200);
				pomGrid->SetValueRange(CGXRange(ilRow, ilCol), CString(""));
				pomGrid->DeleteEmptyRow(ilRow, ilCol);
			}
			else
			{
				CTime olLeftDate;
				blValid = (DateStringToDateExt(olLeft, olLeftDate) == TRUE);
				blValid &= (DateStringToDateExt(olText, olDate2Test) == TRUE);
				if ((blValid == false) || ((olDate2Test > olVato) && (m_Chb_Unlimited.GetCheck() == UNCHECKED)) ||
						(olDate2Test < olLeftDate))
				{
					Beep(800, 200);
					pomGrid->SetValueRange(CGXRange(ilRow, ilCol), pomGrid->GetValueRowCol(ilRow, 1));
				}
			}
		}
	}
	
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnGridClick(WPARAM wParam, LPARAM lParam)
{
	CELLPOS *pCellPos;
	pCellPos = (CELLPOS*)lParam;

	ROWCOL ilCol = 0;
	ROWCOL ilRow = 0;	
	ilCol = pCellPos->Col;
	ilRow = pCellPos->Row;

	if (ilRow == 0)
	{
		SortExcludes();
	}
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnCancel() 
{
	if (bmNewFlag)
	{
		bgIsValSaved = false;
	}
	else
	{
		bgIsValSaved = true;
	}
	CDialog::OnCancel();
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnInvert() 
{
	CCS_TRY

	UpdateData();

	m_MondayChb.GetCheck()	  ? m_MondayChb.SetCheck(UNCHECKED) : m_MondayChb.SetCheck(CHECKED);
	m_TuesdayChb.GetCheck()	  ? m_TuesdayChb.SetCheck(UNCHECKED) : m_TuesdayChb.SetCheck(CHECKED);
	m_WednesdayChb.GetCheck() ? m_WednesdayChb.SetCheck(UNCHECKED) : m_WednesdayChb.SetCheck(CHECKED);
	m_ThursdayChb.GetCheck()  ? m_ThursdayChb.SetCheck(UNCHECKED) : m_ThursdayChb.SetCheck(CHECKED);
	m_FridayChb.GetCheck()	  ? m_FridayChb.SetCheck(UNCHECKED) : m_FridayChb.SetCheck(CHECKED);
	m_SaturdayChb.GetCheck()  ? m_SaturdayChb.SetCheck(UNCHECKED) : m_SaturdayChb.SetCheck(CHECKED);
	m_SundayChb.GetCheck()	  ? m_SundayChb.SetCheck(UNCHECKED) : m_SundayChb.SetCheck(CHECKED);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnDetails() 
{
	CCS_TRY

	CString olText;
	CRect olRect;
	GetWindowRect(&olRect);

	if (bmDetails == false)
	{
		//--- disable fixed time controls
		m_FromSlider.EnableWindow(bmFixedTimes);
		m_FixTimeFrom.EnableWindow(bmFixedTimes);
		m_FromFrame.EnableWindow(bmFixedTimes);
		m_TimeStatic.EnableWindow(bmFixedTimes);
		m_TimeStatic2.EnableWindow(bmFixedTimes);
		m_TimeStatic3.EnableWindow(bmFixedTimes);
		m_TimeStatic4.EnableWindow(bmFixedTimes);
		m_FixTimeTo.EnableWindow(bmFixedTimes);
		m_UntilSlider.EnableWindow(bmFixedTimes);
		m_UntilFrame.EnableWindow(bmFixedTimes);
		
		//--- increase window
		olRect.bottom += 460;

		//--- set button text
		m_DetailsBtn.GetWindowText(olText);
		olText.Replace(">","<");
		m_DetailsBtn.SetWindowText(olText);

		//--- set activation flag
		bmDetails = true;
	}
	else
	{
		//--- increase window
		olRect.bottom -= 460;

		//--- set button text
		m_DetailsBtn.GetWindowText(olText);
		olText.Replace("<",">");
		m_DetailsBtn.SetWindowText(olText);

		//--- set activation flag
		bmDetails = false;
	}

	MoveWindow(olRect);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnFixedBtn() 
{
	CCS_TRY

	if (bmFixedTimes == false)
	{
		SetSliderEnabled(TRUE);
	}
	else
	{
		SetSliderEnabled(FALSE);
	}

	m_FromSlider.SetPos(0);
	m_UntilSlider.SetPos(1439);

	CTime olTime = CTime(1980, 1, 1, 0, 0, 0);
	m_FixTimeFrom.SetTime(&olTime);
	olTime = CTime(1980, 1, 1, 23, 59, 0);
	m_FixTimeTo.SetTime(&olTime);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnChangeFixedFrom(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CTime olTime;
	m_FixTimeFrom.GetTime(olTime);
	m_FromSlider.SetPos(GetMinutesFromTime(olTime));
	
	*pResult = 0;
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnChangeFixedTo(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CTime olTime;
	m_FixTimeTo.GetTime(olTime);
	m_UntilSlider.SetPos(GetMinutesFromTime(olTime));
	
	*pResult = 0;
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CDialog::OnLButtonDown(nFlags, point);
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// compare handles to find out which slider is used
	// Don't insert exception handling code here, it might slow down the program too much !

	if (m_FromSlider.m_hWnd == pScrollBar->m_hWnd)
	{
		// set FROM text in DateTimeCtrl according to slider
		int ilPos = m_FromSlider.GetPos();
		CTime olTime = GetTimeFromMinutes(ilPos);
		m_FixTimeFrom.SetTime(&olTime);
	}

	if (m_UntilSlider.m_hWnd == pScrollBar->m_hWnd)
	{
		// set UNTIL text in DateTimeCtrl according to slider
		int ilPos = m_UntilSlider.GetPos();
		CTime olTime = GetTimeFromMinutes(ilPos);
		m_FixTimeTo.SetTime(&olTime);
	}
	
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}



//-----------------------------------------------------------------------------------------------------------------------
//						get methods						
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::GetValData(VALDATA &rrpValData)
{
	EXCLUDES rlExcl;
	
	rrpValData.Excls.DeleteAll();

	int ilCount = rmValData.Excls.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		rlExcl = rmValData.Excls[i];
		rrpValData.Excls.New(rlExcl);

		// MNE TEST
		CTime test1 = rlExcl.FirstDay;
		CTime test2 = rlExcl.LastDay;
		// END TEST
	}
	
	strcpy(rrpValData.FixF, rmValData.FixF);
	strcpy(rrpValData.FixT, rmValData.FixT);
	strcpy(rrpValData.Freq, rmValData.Freq);
	strcpy(rrpValData.Vafr, rmValData.Vafr);
	strcpy(rrpValData.Vato, rmValData.Vato);

	//
	strcpy(rrpValData.Vtyp, rmValData.Vtyp);
	
	// rrpValData.IsFilled = rmValData.IsFilled;		// superfluous. MNE. 000124


	// MNE TEST
	/*
	for (i = 0; i < ilCount; i++)
	{
		rlExcl = rmValData.Excls[i];
		rrpValData.Excls.New(rlExcl);

		CString test1 = rlExcl.FirstDay.Format("%d.%m.%Y - %H:%M:%S");
		CString test2 = rlExcl.LastDay.Format("%d.%m.%Y - %H:%M:%S");
	}*/
	// END TEST
}




//-----------------------------------------------------------------------------------------------------------------------
//						set methods						
//-----------------------------------------------------------------------------------------------------------------------
void ValidityDlg::SetValData(VALDATA *prpValData)
{
	EXCLUDES rlExcl;
	
	int ilCount = prpValData->Excls.GetSize();

	for (int i = 0; i < ilCount; i++)
	{
		rlExcl = prpValData->Excls[i];
		rmValData.Excls.New(rlExcl);

		// MNE TEST
		CTime test1 = rlExcl.FirstDay;
		CTime test2 = rlExcl.LastDay;
		// END TEST
	}

	strcpy(rmValData.FixF, prpValData->FixF);
	strcpy(rmValData.FixT, prpValData->FixT);
	strcpy(rmValData.Freq, prpValData->Freq);
	strcpy(rmValData.Vafr, prpValData->Vafr);
	strcpy(rmValData.Vato, prpValData->Vato);

	//added by MAX
	strcpy(rmValData.Vtyp, prpValData->Vtyp);
	rmValData.IsTurnaround = prpValData->IsTurnaround;
	
	//rmValData.IsFilled = prpValData->IsFilled;		// superfluous. MNE. 000124
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::SetSliderEnabled(BOOL ipEnable)
{
	if (ipEnable == TRUE)
	{
		bmFixedTimes = true;
		m_FixedBtn.SetWindowText(GetString(1425));
	}
	else
	{
		bmFixedTimes = false;
		m_FixedBtn.SetWindowText(GetString(1424));
	}

	m_TimeStatic.EnableWindow(ipEnable);
	m_TimeStatic2.EnableWindow(ipEnable);
	m_TimeStatic3.EnableWindow(ipEnable);
	m_TimeStatic4.EnableWindow(ipEnable);
	m_FromSlider.EnableWindow(ipEnable);
	m_FixTimeFrom.EnableWindow(ipEnable);
	m_FromFrame.EnableWindow(ipEnable);
	m_UntilSlider.EnableWindow(ipEnable);
	m_FixTimeTo.EnableWindow(ipEnable);
	m_UntilFrame.EnableWindow(ipEnable);
}


//-----------------------------------------------------------------------------------------------------------------------
//						helper functions
//-----------------------------------------------------------------------------------------------------------------------
int ValidityDlg::GetMinutesFromTime(CTime opTime)
{
	int ilReturn = 0;

	CCS_TRY
	

	int ilHour = atoi(opTime.Format("%H"));
	int ilMinute = atoi(opTime.Format("%M"));

	ilReturn = ilHour * 60 + ilMinute;


	CCS_CATCH_ALL

	return ilReturn;
}
//-----------------------------------------------------------------------------------------------------------------------

CTime ValidityDlg::GetTimeFromMinutes(int ipMinutes)
{
	CTime olReturn;

//--- exception handling
	CCS_TRY

	int ilHour = (int)(ipMinutes / 60);
	int ilMinute = ipMinutes % 60;

	olReturn = CTime(1980, 1, 1, ilHour, ilMinute, 0);

//--- exception handling
	CCS_CATCH_ALL

	return olReturn;
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::InitializeTable()
{
	CCS_TRY

//--- set background color to white
	pomGrid->GetParam()->GetProperties()->SetColor(GX_COLOR_BACKGROUND, RGB(255,255,255));

	pomGrid->SetAutoGrow(true);

//--- set col and row count 
    BOOL blTest = pomGrid->SetRowCount(1);
	blTest = pomGrid->SetColCount(3);

	
//--- make header	
	pomGrid->SetColWidth(0, 0, 30);
	pomGrid->SetColWidth(1, 1, 180);
	pomGrid->SetColWidth(2, 2, 180);
	
	pomGrid->SetValueRange(CGXRange(0,1), GetString(1571));
	pomGrid->SetValueRange(CGXRange(0,2), GetString(1572));
	
	pomGrid->HideCols(3, 3);
	pomGrid->GetParam()->EnableMoveCols(false);

//--- make date/time controls
	CString olText = CString ("...\n");
	int ilRowCount = (int)(pomGrid->GetRowCount());
	for (int ilRow = 1; ilRow < ilRowCount + 1; ilRow++)
	{
		pomGrid->SetStyleRange(CGXRange(ilRow, 1), CGXStyle().SetControl(GX_IDS_CTRL_DATETIME)
					 .SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT, "dd.MM.yyyy"));
		pomGrid->SetStyleRange(CGXRange(ilRow, 2), CGXStyle().SetControl(GX_IDS_CTRL_DATETIME) 
		    		 .SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT, "dd.MM.yyyy"));
	}

//--- disable numbering, col resizing, row moving
	pomGrid->GetParam()->SetNumberedColHeaders(FALSE);                 
    pomGrid->GetParam()->EnableTrackRowHeight(0); 
    pomGrid->GetParam()->EnableMoveRows(false);
	pomGrid->GetParam()->SetNumberedColHeaders(FALSE);                 
	pomGrid->GetParam()->SetNumberedRowHeaders(FALSE); 

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::SetStaticTexts()
{
	SetWindowLangText ( this, VALIDITY_TXT );
	
	CWnd *pWnd = GetDlgItem(IDOK);
	if (pWnd  != NULL)
		pWnd->SetWindowText(GetString(243));

	pWnd = GetDlgItem(IDCANCEL);
	if (pWnd  != NULL)
		pWnd->SetWindowText(GetString(244));

	pWnd = GetDlgItem(IDC_STATIC1);
	if (pWnd  != NULL)
		pWnd->SetWindowText(GetString(274));

	SetDlgItemLangText ( this, IDC_DETAILS_BTN, IDS_DETAILS );

	pWnd = GetDlgItem(IDS_VAFR_STATIC);
	if (pWnd  != NULL)
		pWnd->SetWindowText(GetString(VALFROM));

	pWnd = GetDlgItem(IDC_VATO_STATIC);
	if (pWnd  != NULL)
		pWnd->SetWindowText(GetString(VALTO));

	SetDlgItemLangText ( this, IDC_UNLIMITED_CHK, IDS_UNLIMITED );

	SetDlgItemLangText ( this, IDC_MONDAY_CHB, IDS_MONDAY );
	SetDlgItemLangText ( this, IDC_TUESDAY_CHB, IDS_TUESDAY );
	SetDlgItemLangText ( this, IDC_WEDNESDAY_CHB, IDS_WEDNESDAY );
	SetDlgItemLangText ( this, IDC_THURSDAY_CHB, IDS_THURSDAY );
	SetDlgItemLangText ( this, IDC_FRIDAY_CHB, IDS_FRIDAY );
	SetDlgItemLangText ( this, IDC_SATURDAY_CHB, IDS_SATURDAY);
	SetDlgItemLangText ( this, IDC_SUNDAY_CHB, IDS_SUNDAY );

	SetDlgItemLangText ( this, IDC_INVERT_BTN, IDS_INVERT_SELECTION );
	SetDlgItemLangText ( this, IDC_EXCLUDE_BTN, IDS_EXCLUDE_DAYS );


	SetDlgItemLangText ( this, IDC_FROM_STATIC,IDS_VON);
	SetDlgItemLangText ( this, IDC_UNTIL_STATIC, IDS_BIS );

	pWnd = GetDlgItem(IDC_FIXED_BTN);
	if (pWnd  != NULL)
		pWnd->SetWindowText(GetString(1424));
}
//-----------------------------------------------------------------------------------------------------------------------
/*
bool LoadValidity( CString &ropUval, VALDATA &ropValData ) 
{
	bool blReturn = true;

	//--- exception handling
	CCS_TRY

	CCSPtrArray <RecordSet> olValRecs;
	ogBCD.GetRecords("VAL", "UVAL", ropUval, &olValRecs);

	int ilSize = olValRecs.GetSize();
	if (ilSize > 0)
	{
		// create entries for excludes-table
		RecordSet olRecord(ogBCD.GetFieldCount("VAL"));
		for (int i = 0; i < ilSize; i++)
		{
			olRecord = olValRecs[i];
			strcpy(ropValData.Vato, olRecord.Values[igValVatoIdx]);
			strcpy(ropValData.Vafr, olRecord.Values[igValVafrIdx]);
			strcpy(ropValData.FixT, olRecord.Values[igValTimtIdx]);
			strcpy(ropValData.FixF, olRecord.Values[igValTimfIdx]);
			strcpy(ropValData.Freq, olRecord.Values[igValFreqIdx]);
			ropValData.IsFilled = true;
		}
	}
	else
	{
		blReturn = false;
	}
	olValRecs.DeleteAll();	
	//--- exception handling
	CCS_CATCH_ALL

	return blReturn;
}
*/

bool LoadValidity( CString &ropUval, VALDATA &ropValData ) 
{
	bool blReturn = true;
	CString olFreq;
	CString olExclBegin;
	CString olExclEnd;

	CCS_TRY
	
	strcpy(ropValData.Vato, "\0");
	strcpy(ropValData.Vafr, "\0");
	strcpy(ropValData.Freq, "\0");
	strcpy(ropValData.FixF, "\0");
	strcpy(ropValData.FixT, "\0");
    //
	strcpy(ropValData.Vtyp, "\0");

	ropValData.Excls.DeleteAll();

	CCSPtrArray <RecordSet> olValRecs;
	ogBCD.GetRecords("VAL", "UVAL", ropUval, &olValRecs);
	int ilSize = olValRecs.GetSize();
	if (ilSize <= 0)
	{
		return false;
	}

	olValRecs.Sort(&ValComp);		//  Validity-Datens�tze sortieren
	
	// mne test
	/*TRACE("===========================\n");
	for (int k=0; k < olValRecs.GetSize(); k++)
	{
		TRACE("[LoadValidity] (%d) Vafr: %s, Vato: %s\n",k, olValRecs[k].Values[igValVafrIdx], olValRecs[k].Values[igValVatoIdx]);
	}
	TRACE("===========================\n");*/
	// end test

	RecordSet olRecord(ogBCD.GetFieldCount("VAL"));
	EXCLUDES rlExcl;
	
	for (int i = 0; i < ilSize; i++)
	{
		if ( i==0 )			//  Vafr vom 1. Datensatz �bernehmen
		{
			strcpy(ropValData.Vafr, olValRecs[i].Values[igValVafrIdx] );
			olRecord = olValRecs[i];
		}
		else						//  sp�ter
		{
			olExclEnd = olValRecs[i].Values[igValVafrIdx];

			rlExcl.FirstDay = DBStringToDateTime(olExclBegin);
			rlExcl.FirstDay += CTimeSpan(0, 0, 0, 1);	// last valid time + 1 sec = first excluded time
			rlExcl.LastDay = DBStringToDateTime(olExclEnd);
			rlExcl.LastDay -= CTimeSpan(0, 0, 0, 1);	// first valid time - 1 sec = last excluded time
			ropValData.Excls.New(rlExcl);
		}
		olExclBegin = olValRecs[i].Values[igValVatoIdx];
	}
	olValRecs.DeleteAll();
		
	strcpy(ropValData.Vato, olExclBegin);	//  letzter Vato-Eintrag wird Vato

	// FREQ
	olFreq = olRecord.Values[igValFreqIdx];
	if (olFreq.IsEmpty() == TRUE || olFreq == CString(" "))
	{
		olFreq = CString("1111111");
	}
	strcpy(ropValData.Freq, olFreq);
	
	// FIXT, FIXF
	strcpy(ropValData.FixT, olRecord.Values[igValTimtIdx]);
	strcpy(ropValData.FixF, olRecord.Values[igValTimfIdx]);

	// rest
	ropValData.IsFilled = true;

	//VTYP
	if (igValVtypIdx > 0)
	{
		strcpy(ropValData.Vtyp, olRecord.Values[igValVtypIdx]);
	}
	//add by MAX
	//--- get rule event type
	CString olEvty;
	olEvty = ogBCD.GetField("RUE", "URNO", ropUval, "EVTY");
	if (atoi(olEvty) == 0)
	{
		ropValData.IsTurnaround = true;
	}
	else
		ropValData.IsTurnaround = false;

	CCS_CATCH_ALL

	return blReturn;
}

//-----------------------------------------------------------------------------------------------------------------------

bool SaveValidity(CString &ropUval, VALDATA &ropValData, bool bpNew) 
{
	bool blReturn = true;

	CCS_TRY

	RecordSet olRecord(ogBCD.GetFieldCount("VAL"));
	olRecord.Values[igValUvalIdx] = ropUval;
	olRecord.Values[igValApplIdx] = "RULE_AFT";
	olRecord.Values[igValTabnIdx] = "RUE";
	olRecord.Values[igValTimtIdx] = ropValData.FixT;
	olRecord.Values[igValTimfIdx] = ropValData.FixF;

	// vtyp
	if (igValVtypIdx > 0)
	{
		olRecord.Values[igValVtypIdx] = ropValData.Vtyp;
	}
	
	
	// freq
	CString olFreq;
	olFreq = ropValData.Freq;
	if (olFreq.IsEmpty() || (olFreq==" ")) 
	{
		olFreq = CString("1111111");
	}
	olRecord.Values[igValFreqIdx] = olFreq;

	// get old record. It may be needed
	RecordSet olVal(ogBCD.GetFieldCount("VAL"));
	bool blTest = ogBCD.GetRecord("VAL", "UVAL", ropUval, olVal);

	//
	if(igValVtypIdx > 0)
		olVal.Values[igValVtypIdx] = ropValData.Vtyp;
//--- get exclusion periods and create VAL records
	CTime olValidFrom;
	CTime olValidTo;
	
	int ilCount = ropValData.Excls.GetSize();
	if (ilCount > 0)
	{
		// delete all val records. Not elegant, but save'n'easy :)
		CCSPtrArray <RecordSet> olValArr;
		ogBCD.GetRecords("VAL", "UVAL", ropUval, &olValArr);
		for (int i = 0; i < olValArr.GetSize(); i++)
		{
			ogBCD.DeleteRecord("VAL", "URNO", olValArr[i].Values[igValUrnoIdx]);
		}
		olValArr.DeleteAll();
		
		for (i = 0; i <= ilCount; i++)	//hag991207 
		{
			if (i < ilCount)
			{
				olValidTo = ropValData.Excls[i].FirstDay;
				olValidTo -= CTimeSpan(0, 0, 0, 1); 
				olRecord.Values[igValVatoIdx] = CTimeToDBString(olValidTo, olValidTo);
			}
			else
			{
				olRecord.Values[igValVatoIdx] = ropValData.Vato;
			}

			if (i == 0) 
			{
				olRecord.Values[igValVafrIdx] = ropValData.Vafr;
			}
			else
			{
				olValidFrom = ropValData.Excls[i - 1].LastDay;
				olValidFrom += CTimeSpan(0, 0, 0, 1);
				olRecord.Values[igValVafrIdx] = CTimeToDBString(olValidFrom, olValidFrom);
			}
				
			//  urno leer setzen, sonst werden alle Val-Datens�tze mit der 
			//  gleichen Urno gespeichert !!!      hag991207
			olRecord.Values[igValUrnoIdx] = "";

			blReturn &= ogBCD.InsertRecord("VAL", olRecord, false);
		}
	}
	else
	{
		olVal.Values[igValVafrIdx] = ropValData.Vafr;
		olVal.Values[igValVatoIdx] = ropValData.Vato;
		olVal.Values[igValUvalIdx] = ropUval;
		olVal.Values[igValApplIdx] = "RULE_AFT";
		olVal.Values[igValTabnIdx] = "RUE";
		olVal.Values[igValTimtIdx] = ropValData.FixT;
		olVal.Values[igValTimfIdx] = ropValData.FixF;
		olVal.Values[igValFreqIdx] = olFreq;
		

		if ( (bpNew == true) || olVal.Values[igValUrnoIdx].IsEmpty() )
		{
			blReturn &= ogBCD.InsertRecord("VAL", olVal, false);
		}
		else
		{
			blReturn &= ogBCD.SetRecord("VAL", "URNO", olVal.Values[igValUrnoIdx], olVal.Values);
		}
	}
	
	if ( !blReturn )
	{
		ogLog.Trace("DEBUG", "[SaveValidity] Failed appending a VAL record to object.");
	}
	else
	{	// save VAL records to database
		if (!ogBCD.Save("VAL"))
		{
			blReturn = false;
			ogLog.Trace("DEBUG","[SaveValidity] Failed saving VAL records to database");
		}
	}
	

	CCS_CATCH_ALL
	
	return blReturn;
}
//-----------------------------------------------------------------------------------------------------------------------

CTimeSpan GetUrnoTimeTillExpiration ( CString &ropUval )
{
	CTimeSpan	olDiff(20000,0,0,0);
	CString		olVato;		

	//if ( ogBCD.GetField( "VAL", "UVAL", ropUval, "VATO", olVato ) )
	//	olDiff = GetVatoTimeTillExpiration ( olVato );

	CCSPtrArray <RecordSet> olValArray;
	ogBCD.GetRecords("VAL", "UVAL", ropUval, &olValArray);
	
	int ilSize = olValArray.GetSize();
	if (ilSize > 0)
	{
		olValArray.Sort(&ValComp);
		olVato = olValArray[ilSize - 1].Values[igValVatoIdx];
		olDiff = GetVatoTimeTillExpiration ( olVato );
	}
	olValArray.DeleteAll();


	return olDiff;
}
//-----------------------------------------------------------------------------------------------------------------------
	
CTimeSpan GetVatoTimeTillExpiration ( CString &ropVato )
{
	CTime		olNow, olExpiration;
	CTimeSpan	olDiff(20000,0,0,0);

	olNow = CTime::GetCurrentTime ();
	olExpiration = DBStringToDateTime(ropVato);
	if ( olExpiration != TIMENULL )
		olDiff = olExpiration - olNow;
	return olDiff;
}
//-----------------------------------------------------------------------------------------------------------------------

bool CheckForExpirationToWarn ()
{
	int ilCount ;
	RecordSet olRueRecord;
	int ilRust ;
	CString	olRueDBVato, olTnam;
	CTimeSpan olTimeSpan0(0,0,0,0);
	CTimeSpan olTimeTillExpiraton;

	UINT  ilWarnBevor = GetPrivateProfileInt ( ogAppName, "EXPIRATION_WARNING",
											   0, pcgConfigPath);
	if ( ilWarnBevor == 0 )
		return false;
	ilCount = ogBCD.GetDataCount("RUE");
	if ( ilCount == 0 )
		return false;
	CTimeSpan olWarnBevor(ilWarnBevor, 0, 0, 0 );
			
	for (int j = 0; j < ilCount; j++)
	{	
		ogBCD.GetRecord("RUE", j, olRueRecord);
					
		// wenn rule status ==  archiviert, Regel nicht untersuchen
		ilRust = atoi(olRueRecord.Values[igRueRustIdx]);
		if ( ilRust == 2 )
			continue;

		olTnam = ogBCD.GetField ( "TPL", "URNO", olRueRecord.Values[igRueUtplIdx], "TNAM" );
		if ( !CanModifyTemplate ( olTnam ) )		/* PRF5800 */
			continue;

		olTimeTillExpiraton = GetUrnoTimeTillExpiration ( olRueRecord.Values[igRueUrnoIdx] );
		if ( ( olTimeTillExpiraton > olTimeSpan0 )	&&  //  noch nicht abgelaufen und
			 ( olTimeTillExpiraton <= olWarnBevor ) )	//  l�uft in den n�chsten 
			return true;								//  ilWarnBevor Tagen aus
	}
	return false;
}
//-----------------------------------------------------------------------------------------------------------------------

COleDateTime CTimeToCOleDateTime(CTime &opTime)
{
	COleDateTime olTime;
	olTime.SetStatus(COleDateTime::invalid);
	if(opTime != -1)
	{
		olTime = COleDateTime(opTime.GetYear(), opTime.GetMonth(), opTime.GetDay(), opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
	}
	return olTime;
}
//-----------------------------------------------------------------------------------------------------------------------

COleDateTime DBStringToOleDateTime(CString &opString)
{
	int ilYear, ilMonth, ilDay, ilHour, ilMin, ilSec;
	COleDateTime olTime;
	olTime.SetStatus(COleDateTime::invalid);

	if(opString.GetLength() < 14)
		return olTime;


	ilYear = atoi(opString.Left(4));
	ilMonth = atoi(opString.Mid(4,2));
	ilDay = atoi(opString.Mid(6,2));
	ilHour = atoi(opString.Mid(8,2));
	ilMin =  atoi(opString.Mid(10,2));
	ilSec =  atoi(opString.Mid(12,2));

	if(!((ilYear >= 1900) && (ilYear <= 2200)))
		return olTime;

	if(!((ilMonth >= 1) && (ilMonth <= 12)))
		return olTime;
	
	if(!((ilDay >= 1) && (ilDay <= 31)))
		return olTime;
	
	if(!((ilHour >= 0) && (ilHour <= 23)))
		return olTime;
	
	if(!((ilMin >= 0) && (ilMin <= 60)))
		return olTime;
	
	if(!((ilSec >= 0) && (ilSec <= 60)))
		return olTime;
	
	return  COleDateTime::COleDateTime(ilYear, ilMonth, ilDay, ilHour, ilMin, ilSec);
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnUnlimitedChk() 
{
	UpdateData ();
	m_DateToCtrl.EnableWindow(!m_bUnlimited);
	m_TimeToCtrl.EnableWindow(!m_bUnlimited);
}
//-----------------------------------------------------------------------------------------------------------------------

bool ValidityDlg::CheckExclIsValid(CTime opTestVal, int ipRow, int ipCol)
{
	bool blRet = true;

	CCS_TRY


	// olTestVal must not be out of validity range (vafr - vato)
	CTime olVafr, olDafr, olTifr;
	CTime olVato, olDato, olTito;

	m_DateFromCtrl.GetTime(olDafr);
	m_TimeFromCtrl.GetTime(olTifr);
	m_DateToCtrl.GetTime(olDato);
	m_TimeToCtrl.GetTime(olTito);

	olVafr = CTime(olDafr.GetYear(), olDafr.GetMonth(), olDafr.GetDay(), olTifr.GetHour(), olTifr.GetMinute(), 0);
	olVato = CTime(olDato.GetYear(), olDato.GetMonth(), olDato.GetDay(), olTito.GetHour(), olTito.GetMinute(), 0);

	if ((opTestVal <= olVafr) || ((opTestVal >= olVato) && (m_bUnlimited == false)))
	{
		blRet = false;
	}


	// olTestVal must not lie within any other exclude period
	CString olStartVal, olEndVal;
	CTime olStartExcl, olEndExcl;

	int ilRowCount = (int)pomGrid->GetRowCount();
	for (int i = 1; i <= ilRowCount; i++)
	{
		if (i != ipRow)	// don't compare value with itself
		{
			olStartVal = pomGrid->GetValueRowCol(i, 1);
			olEndVal = pomGrid->GetValueRowCol(i, 2);

			if (olStartVal.IsEmpty() == FALSE)
			{
				DateStringToDateExt(olStartVal, olStartExcl);
				DateStringToDateExt(olEndVal, olEndExcl);
			
				if ((opTestVal >= olStartExcl) && (opTestVal <= olEndExcl))
				{
					blRet = false;
				}
			}
		}
	}


	// end date has to be later than start date
	if (ipCol == 2)
	{
		olStartVal = pomGrid->GetValueRowCol(ipRow, 1);

		olStartVal = pomGrid->GetValueRowCol(i, 1);

		if (olStartVal.IsEmpty() == FALSE)
		{
			DateStringToDateExt(olStartVal, olStartExcl);
			if (opTestVal <= olStartExcl)
			{
				blRet = false;
			}
		}
	}


	CCS_CATCH_ALL

	return blRet;
}
//-----------------------------------------------------------------------------------------------------------------------

// sorts exclude grid by start date
void ValidityDlg::SortExcludes()
{
	int ilRowCount = pomGrid->GetRowCount();

	CString olText;
	CTime olTime;

//--- set sorting key
	for (int ilRow = 1; ilRow <= ilRowCount; ilRow++)
	{
		olText = pomGrid->GetValueRowCol(ilRow, 1);
		if (olText.IsEmpty() == FALSE)
		{
			DateStringToDateExt(olText, olTime);
			olText.Format("%04d%02d%02d", olTime.GetYear(), olTime.GetMonth(), olTime.GetDay());
		}
		else
		{
			olText = CString("99999999");
		}

		pomGrid->SetValueRange(CGXRange(ilRow, 3), olText);
	}

	
//--- sort grid
	CGXSortInfoArray rlSortInfo;
	rlSortInfo.SetSize(1);       // 1 key only
			
	rlSortInfo[0].nRC = 3;		 // column 3 is the key
	rlSortInfo[0].bCase = FALSE; // not case sensitive
	rlSortInfo[0].sortType = CGXSortInfo::numeric;   
	rlSortInfo[0].sortOrder = CGXSortInfo::ascending;

	pomGrid->m_nTopRow = (ROWCOL) 1;
	pomGrid->SortRows(CGXRange().SetTable(), rlSortInfo);
	pomGrid->Redraw();


}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::SetVafrToCurrentTime()
{
	CTime olTime;
	olTime = CTime::GetCurrentTime();
	olTime = CTime(olTime.GetYear(), olTime.GetMonth(), olTime.GetDay(), 0, 0, 0);
	m_DateFromCtrl.SetTime(&olTime);
	m_TimeFromCtrl.SetTime(&olTime);
}
//-----------------------------------------------------------------------------------------------------------------------
