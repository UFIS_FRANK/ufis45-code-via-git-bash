// FlightIndepRulesView.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <ccsglobl.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <Dialog_VorlagenEditor.h>
#include <Ghslist.h>
#include <dialoggrid.h>
#include <dutygrid.h>
#include <indepviewer.h>
#include <RegelWerkView.h>
#include <FlightIndepRulesView.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif

#define IDC_LEFT_EDIT	   47110813

//----------------------------------------------------------------------------
//		globals, defines and declaration of static or global functions
//----------------------------------------------------------------------------

//--- Local function prototype
static void IndepDDXCallback(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);




//----------------------------------------------------------------------------
//						creation, construction, destruction
//----------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(FlightIndepRulesView, CRulesFormView)


FlightIndepRulesView::FlightIndepRulesView()
	: CRulesFormView(FlightIndepRulesView::IDD)
{
	CCS_TRY
	
	imRessourceType = 0;

	pomLeftGrid = new CGridFenster(this);
	pomRightGrid = new CGridFenster(this);
	pomConditionsGrid = new CGridFenster(this);
	pomDutyGrid = new CDutyGrid(this);
	pomTimesGrid = new CGridFenster(this);
	pomViewer = NULL;	
	
	omCurrDgrUrno = "";
	
	SetBdpsState();

	//{{AFX_DATA_INIT(FlightIndepRulesView)
	m_RuleName = _T("");
	m_RuleShortName = _T("");
	imRuty2 = 1;
	biDisplaySingleDuties = 0;
	//}}AFX_DATA_INIT


	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

FlightIndepRulesView::~FlightIndepRulesView()
{
	CCS_TRY
	

	delete pomLeftGrid;
	delete pomRightGrid;
	delete pomConditionsGrid;
	delete pomDutyGrid;
	delete pomTimesGrid;


	CCS_CATCH_ALL
}



//----------------------------------------------------------------------------
//						data exchange, message map
//----------------------------------------------------------------------------
void FlightIndepRulesView::DoDataExchange(CDataExchange* pDX)
{
	CRulesFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FlightIndepRulesView)
	DDX_Control(pDX, IDC_DIDE_CHK, omDideChk);
	DDX_Control(pDX, IDC_MINTIME, omEditSplitMin);
	DDX_Control(pDX, IDC_MAXTIME, omEditSplitMax);
	DDX_Control(pDX, TEMPL_COB_ALOD, m_AlocCB);
	DDX_Control(pDX, INDEP_EDT_REMA, m_Edt_Descript);
	DDX_Control(pDX, INDEP_DTC_DUEN, m_Dtc_Duen);
	DDX_Control(pDX, INDEP_DTC_DUBE, m_Dtc_Dube);
	DDX_Control(pDX, INDEP_CHB_DEACTI, m_Chb_Acti);
	DDX_Control(pDX, INDEP_EDT_SHORTNAME, m_Edt_RuleShortName);
	DDX_Control(pDX, INDEP_EDT_NAME, m_Edt_RuleName);
	DDX_Text(pDX, INDEP_EDT_NAME, m_RuleName);
	DDX_Text(pDX, INDEP_EDT_SHORTNAME, m_RuleShortName);
	DDX_Radio(pDX, IDC_RADIO_RUTY3, imRuty2);
	DDX_Radio(pDX, IDC_RADIO_ACTIVITIES, biDisplaySingleDuties);
	//}}AFX_DATA_MAP
}
//----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(FlightIndepRulesView, CRulesFormView)
	//{{AFX_MSG_MAP(FlightIndepRulesView)
	ON_MESSAGE(GRID_MESSAGE_ENDEDITING, OnEndEditing)
	ON_COMMAND(CM_INDEP_MENU_DELETEBAR, OnMenuDeleteBar)
	ON_COMMAND(CM_INDEP_MENU_CREATELNK, OnMenuCreateLink)
	ON_COMMAND(CM_INDEP_MENU_DELETELNK, OnMenuDeleteLink)
	ON_BN_CLICKED(IDC_RADIO_RUTY2, OnRadioRuty2)
	ON_BN_CLICKED(IDC_RADIO_RUTY3, OnRadioRuty3)
	ON_BN_CLICKED(IDC_RADIO_ACTIVITIES, OnRadioActivities)
	ON_CBN_SELENDOK(TEMPL_COB_ALOD, OnSelendokCobAlod)
	ON_BN_CLICKED(IDC_FLOATING_CHK, OnFloatingChk)
	ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_DIDE_CHK, OnDideChk)
	ON_BN_CLICKED(IDC_FADD_CHK, OnFaddChk)
	ON_MESSAGE(WM_DROP, OnDrop)
	ON_MESSAGE(WM_DRAGOVER, OnDragOver)
	ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnRButtonDown)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,OnLButtonDblClk)
	ON_COMMAND(ID_FILE_SAVE, OnSaveRule)
	ON_BN_CLICKED(IDC_RADIO_SINGLE_DUTY, OnRadioActivities)
	//}}AFX_MSG_MAP
	ON_MESSAGE(GRID_MESSAGE_CURRENTCELLCHANGED, OnGridCurrCellChanged)
	ON_MESSAGE(GRID_MESSAGE_BUTTONCLICK, OnGridButton)
	ON_MESSAGE(GRID_MESSAGE_RBUTTONCLICK, OnGridRButton)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillFocus)
	ON_MESSAGE(GRID_MESSAGE_RESORTED, OnGridResorted)
END_MESSAGE_MAP()



//----------------------------------------------------------------------------
//						message handling
//----------------------------------------------------------------------------
void FlightIndepRulesView::OnInitialUpdate() 
{
	CCS_TRY


	CRulesFormView::OnInitialUpdate();
	SetStaticTexts();

	imActiveRow = -1;  

	//---  set the pointers to the mainframe wnd items
    pomFrameWnd = static_cast< CMainFrame* >(AfxGetMainWnd()); 
	pomComboBox = pomFrameWnd->m_wndToolBar.pomComboBox;
	pomStatusBar = &pomFrameWnd->m_wndStatusBar;

	//--- set control attributes
	m_Edt_RuleName.SetBKColor(YELLOW);
	m_Edt_RuleShortName.SetBKColor(YELLOW);

	m_Edt_RuleName.SetTextLimit(1,64,false);
	m_Edt_RuleShortName.SetTextLimit(1,14,false);
	
	m_DragDropTarget.RegisterTarget(this, this);
	
	m_Dtc_Duen.SetTypeToTime(true,false);
	m_Dtc_Dube.SetTypeToTime(true,false);
	// rule demands
	ogDdx.Register(this, RUD_TMP_NEW, CString("FlightIndepView"), CString("RUD_TMP New"), IndepDDXCallback);
	ogDdx.Register(this, RUD_TMP_CHANGE, CString("FlightIndepView"), CString("RUD_TMP Changed"), IndepDDXCallback);
	ogDdx.Register(this, RUD_TMP_DELETE, CString("FlightIndepView"), CString("RUD_TMP Deleted"), IndepDDXCallback);
	// static groups (heads)
	ogDdx.Register(this, SGR_NEW, CString("FlightIndepView"), CString("SGR New"), IndepDDXCallback);
	ogDdx.Register(this, SGR_CHANGE, CString("FlightIndepView"), CString("SGR Change"), IndepDDXCallback);
	ogDdx.Register(this, SGR_DELETE, CString("FlightIndepView"), CString("SGR Delete"), IndepDDXCallback);
	// static group members)
	/*ogDdx.Register(this, SGM_NEW, CString("FlightIndepRulesView"), CString("SGM New"), IndepDDXCallback);
	ogDdx.Register(this, SGM_CHANGE, CString("FlightIndepRulesView"), CString("SGM Change"), IndepDDXCallback);
	ogDdx.Register(this, SGM_DELETE, CString("FlightIndepRulesView"), CString("SGM Delete"), IndepDDXCallback);*/
	
	bmIsFloating = FALSE;	// mne 001109
	TablesFirstTimeInit();
	IniTimesGrid(); // doesn't set any values, just 
	
	IniControlPositions();
	EnableDemandDetailControls(false);
	DemandDetailDlg::IniAlocCB (&m_AlocCB, true);
	

//--- BDPS-SEC for time/condition - radio buttons
	CWnd *polControl = NULL;
	if (bmTimeControlAllow == false)
	{
		polControl = GetDlgItem(IDC_RADIO_RUTY2);
		if (polControl != NULL)
		{
			polControl->EnableWindow(FALSE);
		}
	}
	
	if (bmCondControlAllow == false)
	{
		polControl = GetDlgItem(IDC_RADIO_RUTY3);
		if (polControl != NULL)
		{
			polControl->EnableWindow(FALSE);
		}
	}

	omEditSplitMin.SetTypeToInt( 1 );
	omEditSplitMax.SetTypeToInt( 1 );
	omEditSplitMin.SetTextLimit( 0, 10, true );
	omEditSplitMax.SetTextLimit( 0, 10, true );

	OnNewRule();
	m_HScroll.ShowWindow(SW_HIDE);

	
	CCS_CATCH_ALL
}

//----------------------------------------------------------------------------

void FlightIndepRulesView::OnDatetimechangeDube() 
{
	CString olTimeDBString;
	CTime olTimeB;
	CString olSTimeB;
	bool blErr = true;
	CTime olTime = CTime::GetCurrentTime();
	long llDedu = 0;
	if (!pomDutyGrid->IsExpanded() && !pomGantt)
	{
		CString olCurrDgrUrno, olCurrRudUrno;
		bool blDutySelected = pomDutyGrid->GetSelectionInfo(olCurrDgrUrno, olCurrRudUrno);

		if (blDutySelected)
		{
			if(m_Dtc_Dube.GetStatus())
			{
				m_Dtc_Dube.GetWindowText(olSTimeB);
				olTimeB = HourStringToDate(olSTimeB,olTime);
			}
			else
			{
				return;
			}

			/*m_Dtc_Dube.GetTime(olTimeB);*/
			olTimeDBString = CTimeToDBString(olTimeB, olTimeB);

			CCSPtrArray <RecordSet> olRudArr;
			ogBCD.GetRecords("RUD_TMP", "UDGR", olCurrDgrUrno, &olRudArr);

			for (int i = 0; i < olRudArr.GetSize(); i++)
			{
				if (olRudArr[i].Values[igRudDeflIdx] == "1")
				{
					olRudArr[i].Values[igRudEadbIdx] = olTimeDBString;
				}
				else
				{
					olRudArr[i].Values[igRudDebeIdx] = olTimeDBString;
				}
				/*
				CalcAndDisplayDuration(olRudArr[i]);
				*/
				
				CString olDeen = olRudArr[i].Values[igRudDeenIdx];
				CString olDebe = olRudArr[i].Values[igRudDebeIdx];
				CString olDedu = olRudArr[i].Values[igRudDeduIdx];
				CString olDbfl = olRudArr[i].Values[igRudDbflIdx];
				CString olDefl = olRudArr[i].Values[igRudDeflIdx];
				llDedu = CalcAndDisplayDuration(olTimeDBString,olDeen,olDedu,olDbfl,olDefl);
				if(llDedu >= 0)
				{
					olRudArr[i].Values[igRudDeduIdx].Format("%ld",llDedu);
				}
				
				ogBCD.SetRecord("RUD_TMP", "URNO", olRudArr[i].Values[igRudUrnoIdx], olRudArr[i].Values);
			}
			olRudArr.DeleteAll();
		}
	}
	else
	{
		if(m_Dtc_Dube.GetStatus())
		{
			m_Dtc_Dube.GetWindowText(olSTimeB);
			olTimeB = HourStringToDate(olSTimeB,olTime);
		}
		else
		{
			return;
		}

	/*	m_Dtc_Dube.GetTime(olTimeB);*/
		olTimeDBString = CTimeToDBString(olTimeB, olTimeB);

		RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
		blErr &= ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);

		if ( imRuty2 )
		{	//  Anfangszeit hat sich ge�ndert, neu Dauer berechnen und darstellen
			if ( olRecord.Values[igRudDeflIdx] == "1" )
				olRecord.Values[igRudEadbIdx] = olTimeDBString;
			else
			{
				olRecord.Values[igRudDebeIdx] = olTimeDBString;
				/*
				CalcAndDisplayDuration ( olRecord );
				*/
				llDedu = CalcAndDisplayDuration(olRecord.Values[igRudDebeIdx],olRecord.Values[igRudDeenIdx],olRecord.Values[igRudDeduIdx],
					olRecord.Values[igRudDbflIdx],olRecord.Values[igRudDeflIdx]);
				if(llDedu >= 0)
				{
					olRecord.Values[igRudDeduIdx].Format("%ld",llDedu);
				}

			}
		}
		blErr &= ogBCD.SetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord.Values);
	}

}
//----------------------------------------------------------------------------

void FlightIndepRulesView::OnDatetimechangeDuen() 
{
	CString olTimeDBString;
	CTime olTimeB, olTimeE;
	CString olSTimeB,olSTimeE;
	bool blErr = true;
	long llDedu = 0;
	CTime olTime = CTime::GetCurrentTime();
	if (!pomDutyGrid->IsExpanded()&& !pomGantt)
	{
		CString olCurrDgrUrno, olCurrRudUrno;
		bool blDutySelected = pomDutyGrid->GetSelectionInfo(olCurrDgrUrno, olCurrRudUrno);

		if (blDutySelected)
		{
			if(m_Dtc_Dube.GetStatus())
			{
				m_Dtc_Dube.GetWindowText(olSTimeB);
				olTimeB = HourStringToDate(olSTimeB,olTime);
			}
			else
			{
				return;
			}

			if(m_Dtc_Duen.GetStatus())
			{
				m_Dtc_Duen.GetWindowText(olSTimeE);
				olTimeE = HourStringToDate(olSTimeE,olTime);
			}
			else
			{
				return;
			}

			// if end time < start time take end time to the next day
			if (olTimeE < olTimeB)
			{
				CTimeSpan olSpan;
				olSpan = CTimeSpan(1, 0, 0, 0);
				olTimeE += olSpan;
			}/*			m_Dtc_Duen.GetTime(olTimeB);*/
			olTimeDBString = CTimeToDBString(olTimeE, olTimeE);

			CCSPtrArray <RecordSet> olRudArr;
			ogBCD.GetRecords("RUD_TMP", "UDGR", olCurrDgrUrno, &olRudArr);

			for (int i = 0; i < olRudArr.GetSize(); i++)
			{
				if (olRudArr[i].Values[igRudDeflIdx] == "1")
					olRudArr[i].Values[igRudLadeIdx] = olTimeDBString;
				else
				{
					olRudArr[i].Values[igRudDeenIdx] = olTimeDBString;
				}
				/*
				CalcAndDisplayDuration(olRudArr[i]);
				*/
				llDedu = CalcAndDisplayDuration(olRudArr[i].Values[igRudDebeIdx],
						olRudArr[i].Values[igRudDeenIdx],
						olRudArr[i].Values[igRudDeduIdx],
						olRudArr[i].Values[igRudDbflIdx],
						olRudArr[i].Values[igRudDeflIdx]);
				if(llDedu >= 0)
				{
					olRudArr[i].Values[igRudDeduIdx].Format("%ld",llDedu);
				}

				ogBCD.SetRecord("RUD_TMP", "URNO", olRudArr[i].Values[igRudUrnoIdx], olRudArr[i].Values);
			}
			olRudArr.DeleteAll();
		}
	}
	else
	{
		RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
		blErr &= ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);

		if(m_Dtc_Dube.GetStatus())
		{
			m_Dtc_Dube.GetWindowText(olSTimeB);
			olTimeB = HourStringToDate(olSTimeB,olTime);
		}
		else
		{
			return;
		}

		if(m_Dtc_Duen.GetStatus())
		{
			m_Dtc_Duen.GetWindowText(olSTimeE);
			olTimeE = HourStringToDate(olSTimeE,olTime);
		}
		else
		{
			return;
		}
/*
		m_Dtc_Dube.GetTime(olTimeB);
		m_Dtc_Duen.GetTime(olTimeE);
*/		
		// if end time < start time take end time to the next day
		if (olTimeE < olTimeB)
		{
			CTimeSpan olSpan;
			olSpan = CTimeSpan(1, 0, 0, 0);
			olTimeE += olSpan;
		}

		olTimeDBString = CTimeToDBString(olTimeE, olTimeE);	//hag991102

		if ( imRuty2 )
		{	//  Anfangszeit hat sich ge�ndert, neu Dauer berechnen und darstellen
			if ( olRecord.Values[igRudDeflIdx] == "1" )
				olRecord.Values[igRudLadeIdx] = olTimeDBString;
			else
			{
				olRecord.Values[igRudDeenIdx] = olTimeDBString;
				/*
				CalcAndDisplayDuration ( olRecord );
				*/
				llDedu = CalcAndDisplayDuration(olRecord.Values[igRudDebeIdx],
						olRecord.Values[igRudDeenIdx],
						olRecord.Values[igRudDeduIdx],
						olRecord.Values[igRudDbflIdx],
						olRecord.Values[igRudDeflIdx]);
				if(llDedu >= 0)
				{
					olRecord.Values[igRudDeduIdx].Format("%ld",llDedu);
				}

			}
		}
		blErr &= ogBCD.SetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord.Values);
	}
	
}
//----------------------------------------------------------------------------

void FlightIndepRulesView::OnEndEditing(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

	CGridFenster*polGrid = (CGridFenster*) wParam;
	CELLPOS		*prlCellPos = (CELLPOS*) lParam;
	
	ROWCOL		ilCol = (ROWCOL) prlCellPos->Col;
	ROWCOL		ilRow = (ROWCOL) prlCellPos->Row;
	RecordSet	olRecord(ogBCD.GetFieldCount("RUD"));
	CString		olReft, olText, olField;
	CString		olCellText ;
		
	if ((polGrid == pomTimesGrid) && (omCurrRudUrno.IsEmpty() == FALSE) && (ilCol == 2) && (ilRow >= 1))
	{
		

		olCellText = polGrid->GetValueRowCol(ilRow, ilCol);

		olCellText.TrimRight();
		if (olCellText.IsEmpty() || (olCellText == " "))
		{
			if (pomDutyGrid->IsExpanded())
			{
				olCellText = CString("0");
			}
			else
			{
				olCellText = CString("");
			}

		}
		else if (atoi(olCellText) < 0)
		{
			Beep(800, 200);
			pomTimesGrid->Undo();
			return;
		}
		else
		{
			if (!pomDutyGrid->IsExpanded()&& !pomGantt)
			{
				CString olCurrDgrUrno, olCurrRudUrno;
				bool blDutySelected = pomDutyGrid->GetSelectionInfo(olCurrDgrUrno, olCurrRudUrno);

				if (blDutySelected && !olCellText.IsEmpty())
				{
					CCSPtrArray <RecordSet> olRudArr;
					ogBCD.GetRecords("RUD_TMP", "UDGR", olCurrDgrUrno, &olRudArr);

					for (int i = 0; i < olRudArr.GetSize(); i++)
					{
						if (ilRow == 1)		//  duration
						{
							olRudArr[i].Values[igRudDeduIdx].Format("%d", atoi(olCellText) * 60);
						}
						else if (ilRow == 2)	// preparation time
						{
							olRudArr[i].Values[igRudSutiIdx].Format("%d", atoi(olCellText) * 60);
						}
						else if (ilRow == 3)	// follow-up treatment 
						{
							olRudArr[i].Values[igRudSdtiIdx].Format("%d", atoi(olCellText) * 60);
						}
						else if (ilRow == 4)	// way there
						{
							olRudArr[i].Values[igRudTtgtIdx].Format("%d", atoi(olCellText) * 60);
						}
						else if (ilRow == 5)	// way back
						{
							olRudArr[i].Values[igRudTtgfIdx].Format("%d", atoi(olCellText) * 60);
						}
						ogBCD.SetRecord("RUD_TMP", "URNO", olRudArr[i].Values[igRudUrnoIdx], olRudArr[i].Values);
						ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRecord);

					}
					olRudArr.DeleteAll();
				}

			}
			else
			{

				ogBCD.GetRecord ("RUD_TMP", "URNO", omCurrRudUrno, olRecord);
				if (ilRow == 1)		//  duration
				{
					olRecord.Values[igRudDeduIdx].Format("%d", atoi(olCellText) * 60);
				}
				else if (ilRow == 2)	// preparation time
				{
					olRecord.Values[igRudSutiIdx].Format("%d", atoi(olCellText) * 60);
				}
				else if (ilRow == 3)	// follow-up treatment 
				{
					olRecord.Values[igRudSdtiIdx].Format("%d", atoi(olCellText) * 60);
				}
				else if (ilRow == 4)	// way there
				{
					olRecord.Values[igRudTtgtIdx].Format("%d", atoi(olCellText) * 60);
				}
				else if (ilRow == 5)	// way back
				{
					olRecord.Values[igRudTtgfIdx].Format("%d", atoi(olCellText) * 60);
				}

				if (pomViewer != NULL)
				{
					if (ilRow != 1)
					{
						pomViewer->MoveThisBar(ilRow - 1, olCellText, &olRecord);
					}
					
					pomViewer->MoveTargetBars(ilRow - 1, olCellText, &olRecord);
				}

				bool blRet = ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);
				ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRecord);
				SelectRudBar (olRecord.Values[igRudUrnoIdx]);
			}
		}
	}
	


//---	Left Grid		  
	CString olCode, olName, olUrno, olUdgr, olUrud;
	bool    blStaticGroup;
	if ( (polGrid == pomLeftGrid) && ( ilRow > 0 ) )
	{
		if (imRessourceType == 0)	// functions 
		{
			if (ilCol == 1)
			{
				CString olPfcUrno;

				olCode = pomLeftGrid->GetValueRowCol(ilRow, 1);
				if ( olCode.IsEmpty () )	// L�schen von Funktionen hier nicht erlaubt
				{							// statt dessen au�erhalb Balken l�schen
					pomLeftGrid->Undo ();
					return;					
				}

				olUrno = pomLeftGrid->GetValueRowCol(ilRow, 3);
				blStaticGroup = !IsSingleResourceSelected ( /*ilRow, ilCol, 
															pomLeftGrid,
												           "PFC",*/ olCode );
				if ( blStaticGroup )
					ogBCD.GetFields("SGR", "GRPN", olCode, "GRDS", "URNO", olName, olPfcUrno);
				else
					ogBCD.GetFields("PFC", "FCTC", olCode, "FCTN", "URNO", olName, olPfcUrno);

				olUdgr = omCurrDgrUrno;
				if ( !olUrno.IsEmpty() )	// change RPF_TMP entry (line contained an entry before)
				{
					ogDataSet.AddOrChangeRessources("RPF", olUrno, omCurrRudUrno, olUdgr, 
													olPfcUrno, olCode, true, blStaticGroup );
				}
				else
				{
					//  Neuen RUD_TMP-Record einf�gen
					CString olTplUrno, olSerUrno;
					RecordSet olSerRecord;
					RecordSet olRudRec(ogBCD.GetFieldCount("RUD_TMP") );

					olSerUrno = ogBCD.GetField("RUD_TMP", "UDGR", olUdgr, "UGHS" );
					if ( !olSerUrno.IsEmpty () &&
						 ogBCD.GetRecord ( "SER", "URNO", olSerUrno, olSerRecord ) )
					{
						olRudRec.Values[igRudUdgrIdx] = olUdgr;
						olRudRec.Values[igRudUrnoIdx].Format("%d", ogBCD.GetNextUrno());
						olRudRec.Values[igRudUrueIdx] = omCurrRueUrno;			// rule urno
						olRudRec.Values[igRudRetyIdx] = CString("100");			// ressource type set to function
						if ( pomComboBox && 
							 GetSelectedTplUrno ( *pomComboBox, olTplUrno, TRUE, true ) )
						{
							olRudRec.Values[igRudAlocIdx] = ogBCD.GetField ( "TPL", "URNO", olTplUrno, "DALO" );
						}
						if ( igRudUtplIdx >= 0 )
						{
							olRudRec.Values[igRudUtplIdx] = olTplUrno;
						}
						ogDataSet.CreateRudRecord ( olRudRec, olSerRecord, false );
						if (ogBCD.InsertRecord("RUD_TMP", olRudRec) == false)
						{
							ogLog.Trace("DEBUG", "[DataSet::CreateRudFromSerList]  InsertRecord into Logical table RUD_TMP failed");
							pomLeftGrid->SetValueRange(CGXRange(ilRow, 1) , "" );
							olName.Empty();
						}
						else
						{
							//omCurrRudUrno = olRudRec.Values[igRudUrnoIdx];
							SetCurrentRudUrno ( olRudRec.Values[igRudUrnoIdx] );
							olUrno.Format("%d", ogBCD.GetNextUrno());
							pomLeftGrid->SetValueRange(CGXRange(ilRow, 3), olUrno);
							ogDataSet.AddOrChangeRessources("RPF", olUrno, omCurrRudUrno, olUdgr,
															olPfcUrno, olCode, false, blStaticGroup);
							pomDutyGrid->OnNewRudTmp ( &olRudRec );
							DisplayQualisForRud ( omCurrRudUrno );
						}
					}
					else
					{
						ogLog.Trace("DEBUG", "Unable to load Service for Demandgroup %s", olUdgr );
						pomLeftGrid->SetValueRange(CGXRange(ilRow, 1) , "" );
						olName.Empty();
					}
						
				}
				//  Fehler beseitigt:  (hag000126)
				//  Aufgrund von Broadcasts wird aktivier Balken deselektiert 
				//	und dadurch das Grid geleert
				if ( ilRow <= pomLeftGrid->GetRowCount () )
					pomLeftGrid->SetValueRange(CGXRange(ilRow, 2), olName);
			}
		}
		
		if (imRessourceType == 1)	// equipment
		{
			// Left grid isn't enabled for equipment
		}
		
		if (imRessourceType == 2)
		{	// Left grid isn't enabled for locations
		}
	}


//---	Right Grid
	if( (polGrid == pomRightGrid) && ( ilRow > 0 ) && (ilCol==1) )
	{
		if (imRessourceType == 0)	// qualifications
		{
			// set corresponding name in right column
			CString olPerUrno;
			olCode = pomRightGrid->GetValueRowCol(ilRow, 1);
			olUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
			if ( olCode.IsEmpty () )	// hag990924
			{
				if ( !olUrno.IsEmpty () && 
					 ogDataSet.DeleteRessource ( "RPQ", olUrno ) )
					pomRightGrid->RemoveRows ( ilRow, ilRow );
				return;	
			}
			blStaticGroup = !IsSingleResourceSelected ( /*ilRow, ilCol, 
														pomRightGrid,
												       "PER",*/ olCode );
			if ( blStaticGroup )
				ogBCD.GetFields("SGR", "GRPN", olCode, "GRDS", "URNO", olName, olPerUrno);
			else
				ogBCD.GetFields("PER", "PRMC", olCode, "PRMN", "URNO", olName, olPerUrno);

			// change RPQ_TMP entry (line contained an entry before)
			if (olUrno.IsEmpty() == FALSE)
			{	//  �nderung, Status Einzel-/ Gruppenqualifikation beibehalten
				olUdgr = ogBCD.GetField ( "RPQ_TMP", "URNO", olUrno, "UDGR" );
				if ( olUdgr.IsEmpty () )
				{
					olUrud = omCurrRudUrno;
					if ( ogDataSet.IsSingleQualiUsedForRud ( olUrno, olUrud,
															 olPerUrno ) )
					{
						MessageBox ( GetString(RES_ALREADY_SEL) );
						//  vorherigen Code wieder setzen
						ogBCD.GetRecord (  "RPQ_TMP", "URNO", olUrno, olRecord );
						if ( GetGroupName ( &olRecord, igRpqGtabIdx, 
											igRpqUperIdx, olCode ) )
							olCode = "*"+ olCode;
						else
							olCode = olRecord.Values[igRpqQucoIdx];
						pomRightGrid->SetValueRange(CGXRange(ilRow, ilCol),
													olCode);
						return;
					}
				}
				else
					olUrud.Empty ();
				ogDataSet.AddOrChangeRessources("RPQ", olUrno, olUrud, olUdgr,
												olPerUrno, olCode, true, blStaticGroup);
			}
			// add RPQ_TMP entry (line contained no entry before)
			else
			{	//  neue Qualifikationen sind immer Einzelqualifikationen !
				CString olNewUrno;
				if ( ogDataSet.IsSingleQualiUsedForRud ( olNewUrno, omCurrRudUrno, 
														 olPerUrno ) )
				{
					MessageBox ( GetString(RES_ALREADY_SEL) );
					pomRightGrid->SetValueRange (CGXRange(ilRow,ilCol), "");
					pomRightGrid->DeleteEmptyRow ( ilRow,ilCol );
					return;
				}
				olNewUrno.Format("%d", ogBCD.GetNextUrno());
				pomRightGrid->SetValueRange(CGXRange(ilRow, 3), olNewUrno);
				ogDataSet.AddOrChangeRessources("RPQ", olNewUrno, omCurrRudUrno, "",
												olPerUrno, olCode, false, blStaticGroup);
			}
			//  Fehler beseitigt:  (hag000126)
			//  Aufgrund von Broadcasts wird aktivier Balken deselektiert 
			//	und dadurch das Grid geleert
			if ( ilRow <= pomRightGrid->GetRowCount () )
				pomRightGrid->SetValueRange(CGXRange(ilRow, 2), olName);
		}
		
		if (imRessourceType == 1)	// equipment
		{
			CString olEquUrno;

			olCode = pomRightGrid->GetValueRowCol(ilRow, 1);
			if ( olCode.IsEmpty () )	// L�schen von Funktionen hier nicht erlaubt
			{							// statt dessen au�erhalb Balken bzw. in Dutygrid l�schen
				pomRightGrid->Undo ();
				return;					
			}
			olUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
			blStaticGroup = !IsSingleResourceSelected ( /*ilRow, ilCol, pomRightGrid,
												       "EQU",*/ olCode );
			if ( blStaticGroup )
				ogBCD.GetFields("SGR", "GRPN", olCode, "GRDS", "URNO", olName, olEquUrno);
			else
				ogBCD.GetFields("EQU", "GCDE", olCode, "ENAM", "URNO", olName, olEquUrno);
			pomRightGrid->SetValueRange(CGXRange(ilRow, 2), olName);
			ogDataSet.AddOrChangeRessources("REQ", olUrno, omCurrRudUrno, olUdgr, 
											olEquUrno, olCode, true, blStaticGroup);
		}

		if (imRessourceType == 2)
		{
			CString olUrnoToSave, /*olAloUrno,*/ olReft;
			CString olTab, olFldn, olNewValue;
			olNewValue = pomRightGrid->GetValueRowCol(ilRow, 1);
			if ( olNewValue.IsEmpty () )	// L�schen von Locations hier nicht erlaubt
			{								// statt dessen au�erhalb Balken l�schen
				pomRightGrid->Undo ();
				return;					
			}
			olUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
			olReft = ogBCD.GetField("RLO_TMP", "URNO", olUrno, "REFT");

			if (olReft.GetLength() == 8)
			{
				olTab = olReft.Left(3);
				olFldn = olReft.Right(4);
			}
			blStaticGroup = !IsSingleResourceSelected ( /*ilRow, ilCol, 
														pomRightGrid,
														olTab,*/ olNewValue );
			if ( blStaticGroup )
				olUrnoToSave = ogBCD.GetField("SGR", "GRPN", olNewValue, "URNO");
			else
				olUrnoToSave = ogBCD.GetField(olTab, olFldn, olNewValue, "URNO");

			ogDataSet.AddOrChangeRessources("RLO", olUrno, omCurrRudUrno, olUdgr, 
											olReft, olUrnoToSave, true, blStaticGroup); 
		}
	}
	if ( polGrid==pomConditionsGrid )
	{
		olCellText = polGrid->GetValueRowCol(ilRow, ilCol);

		if ( (ilRow!=0) && !olCellText.IsEmpty() && olCellText != " " )
		{
			//  Reset tooltips
			polGrid->SetStyleRange( CGXRange(ilRow, 1, ilRow, 5), 
						   CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, "" ));
	
			if (ilCol == 2)
			{
				polGrid->SetValueRange(CGXRange(ilRow, 3, ilRow, 5),  "" );
				if ( polGrid->FormatSpecial ( ilRow, ilCol, olCellText ) )
					polGrid->SetValueRange(CGXRange(ilRow, ilCol), olCellText );
				
			}
			if (ilCol == 3)
			{
				polGrid->SetValueRange(CGXRange(ilRow, 2), "" );
				polGrid->SetValueRange(CGXRange(ilRow, 5), "" );
			}
			if (ilCol == 5)
			{
				polGrid->SetValueRange(CGXRange(ilRow, 2, ilRow, 3),  CString(""));
			}
		}
		polGrid->MarkInValid ( ilRow, ilCol, olCellText );
	}	

	CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------------

void FlightIndepRulesView::OnLoadRule(CString opRueUrno, CString opTplName, bool bpNoComboChange)
{
	CCS_TRY
	
	OnNewRule();		//  alles Zur�cksetzen

	bmNewFlag = false;
	omCurrRueUrno = opRueUrno;
	ogDataSet.FillLogicRudObject(omCurrRueUrno);
	ogDataSet.FillLogicRpfObject();
	ogDataSet.FillLogicRpqObject();
	ogDataSet.FillLogicRloObject();
	ogDataSet.FillLogicReqObject();

	//--- get record
	RecordSet olRecord(ogBCD.GetFieldCount("RUE"));
	ogBCD.GetRecord("RUE", "URNO", omCurrRueUrno, olRecord);

   //--- set names and description
	CString olName;
	CString olShortName;
	CString olDescription;
	
	olDescription = olRecord.Values[igRueRemaIdx];
	CCSCedaData::MakeClientString(olDescription);

	olName = olRecord.Values[igRueRunaIdx];
	olShortName = olRecord.Values[igRueRusnIdx];
	m_Edt_RuleName.SetWindowText(olName);
	m_Edt_RuleShortName.SetWindowText(olShortName);
	m_Edt_Descript.SetWindowText(olDescription);
	
	//--- set CheckBox "Deactivated"
	CString olRust;
	olRust = olRecord.Values[igRueRustIdx];
	if (olRust == CString("0"))
		m_Chb_Acti.SetCheck(CHECKED);	// deactivated
	else
		m_Chb_Acti.SetCheck(UNCHECKED);	// activated

	//hag010509: Da passiert doch gar nichts UpdateServiceTable();	
	EnableDemandDetailControls(true);

	if ( !bpNoComboChange )
	{
		pomConditionsGrid->ClearTable();
		InitializeCondTable();
	}
	pomConditionsGrid->ResetConditionsTable();
	
	//--- initialize tables
	int ilRuty = atoi(olRecord.Values[igRueRutyIdx]);
	CheckDlgButton( IDC_RADIO_RUTY3, (ilRuty==3) );
	CheckDlgButton( IDC_RADIO_RUTY2, (ilRuty==2) );
	OnChangedRuty();
	CString olConditionString;
	if ( ilRuty == 3 )
	{
		olConditionString = olRecord.Values[igRueEvttIdx];
		ReConstructString(pomConditionsGrid, 
												olConditionString );
	}
	pomDutyGrid->DisplayRudTmp ();

	ogChangeInfo.SetCurrRueUrno(omCurrRueUrno);
	
	// mne 001110
	// wenn nach dem Laden einer Regel keine current cell gesetzt ist, 
	// wird die Funktion OnGridCurrCellChanged nicht aufgerufen
	SetTimeValues(omCurrRueUrno);
	RudTmp_Dump();

	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

void FlightIndepRulesView::OnNewRule()
{
	CCS_TRY


	// reset data
	CloseGantt ();
	ogDataSet.FillLogicRudObject(CString("-1"));
	ogDataSet.ResetLogicResourceTables ();


	//--- reset controls
	m_Edt_RuleName.SetWindowText("");
	m_Edt_RuleShortName.SetWindowText("");
	m_Edt_Descript.SetWindowText("");
	
	m_Chb_Acti.SetCheck(CHECKED);

	CTime olTime;
	olTime = CTime::GetCurrentTime();

	olTime = CTime(olTime.GetYear(), olTime.GetMonth(), olTime.GetDay(), olTime.GetHour(), olTime.GetMinute(), 0);
/*	m_Dtc_Dube.SetTime(&olTime);*/
	bmSetDebe = true;
	bmSetDeen = true;
	m_Dtc_Dube.SetInitText(olTime.Format("%H:%M"), true );

	olTime = CTime(olTime.GetYear(), olTime.GetMonth(), olTime.GetDay(), olTime.GetHour(), olTime.GetMinute() + 9, 59);
/*	m_Dtc_Duen.SetTime(&olTime);*/
	m_Dtc_Duen.SetInitText(olTime.Format("%H:%M"), true);

	pomLeftGrid->ResetContent();
	pomRightGrid->ResetContent();

	long llUrno = ogBCD.GetNextUrno();
	bmNewFlag = true;
	omCurrRueUrno.Format("%d", llUrno);

	pomConditionsGrid->ClearTable();
	InitializeCondTable();
	pomDutyGrid->ResetContent();
	pomDutyGrid->SetCurrentCell(0, 0);
	omCurrRudUrno = omCurrDgrUrno = "";
	imActiveRow = -1;  

	OnChangedRuty();
	
	ogChangeInfo.SetCurrRueUrno(omCurrRueUrno);
	bgIsValSaved = false;
	
	omDideChk.EnableWindow(FALSE);
	omDideChk.SetCheck(UNCHECKED);
	omEditSplitMin.SetWindowText("");	
	omEditSplitMax.SetWindowText("");	
	omEditSplitMin.EnableWindow(FALSE);
	omEditSplitMax.EnableWindow(FALSE);

	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

void FlightIndepRulesView::OnSaveRule()
{
	CCS_TRY
	
	SetFocus(); 

	//-- make wait cursor 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	//-- get data from window
	UpdateData();


//--- break conditions
	// Are name and short name given correctly ?
	if (m_Edt_RuleName.GetStatus() == false)
	{
		MessageBox(GetString(1411), GetString(1412), MB_OK | MB_ICONEXCLAMATION);
		return;
	}
	if (m_Edt_RuleShortName.GetStatus() == false)
	{
		MessageBox(GetString(1457), GetString(1412), MB_OK | MB_ICONEXCLAMATION);
		return;
	}

	CString olName;
	CString olShortName;
	CString olDescription;
	
	m_Edt_RuleName.GetWindowText(olName);
	m_Edt_RuleShortName.GetWindowText(olShortName);
	

	int ilCount = ogBCD.GetDataCount("RUE");
	for (int i = 0; i < ilCount; i++)
	{
		CString olShortNameInDB = ogBCD.GetField("RUE", i, "RUSN");
		CString olStatusInDB = ogBCD.GetField("RUE", i, "RUST");

		//  archived rules may have the name of the new rule
		if( (olShortNameInDB == olShortName) && bmNewFlag && (olStatusInDB!="2"))
		{
			VALDATA rlOldValidity;
			CString olOldUrno = ogBCD.GetField("RUE", i, "URNO");
			if (!::LoadValidity(olOldUrno, rlOldValidity) ||
				 DoValiditiesOverlap(rlOldValidity, pomFrameWnd->rmValData))
			{
				AfxMessageBox(GetString(164));
				return;
			}
		}
	}


//--- create recordset
	// Urno der aktuell in FormView->ComboBox ausgew�hlten Vorlage
	CString olTplUrno;
	if (!GetSelectedTplUrno(*pomComboBox, olTplUrno, TRUE, true))
		return;


//--- Fill Record
	m_Edt_Descript.GetWindowText(olDescription);
	if (olDescription.GetLength() > 254)
	{
		olDescription = olDescription.Left(254);
	}
	
	CCSCedaData::MakeCedaString(olDescription);

	int ilSize = ogBCD.GetFieldCount("RUE");
	RecordSet olRecord(ilSize);

   if (bmNewFlag == false)
	   ogBCD.GetRecord ("RUE","URNO",omCurrRueUrno,olRecord);

	olRecord.Values[igRueUrnoIdx] = omCurrRueUrno;	   
	olRecord.Values[igRueApplIdx] = CString("RULE_AFT");	   
	olRecord.Values[igRueRunaIdx] = olName;					// name
	olRecord.Values[igRueRusnIdx] = olShortName;				// short name
	olRecord.Values[igRueRemaIdx] = olDescription;			// description
	olRecord.Values[igRueUtplIdx] = olTplUrno;				// template urno 
	olRecord.Values[igRueRutyIdx] = imRuty2  ? "2" : "3";	// rule type (2 = flight independent) 	
	olRecord.Values[igRueLstuIdx] = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");
	olRecord.Values[igRueUseuIdx] = pcgUser;
	
	// fill fields not needed with blanks
	olRecord.Values[igRueEvtaIdx] = CString(" ");		
	olRecord.Values[igRueEvtdIdx] = CString(" ");		
	olRecord.Values[igRueEvtyIdx] = CString(" ");		
	olRecord.Values[igRueExclIdx] = CString(" ");		
	olRecord.Values[igRueFsplIdx] = CString(" ");		
	olRecord.Values[igRueMaxtIdx] = CString(" ");		
	olRecord.Values[igRueMistIdx] = CString(" ");		
	if (!imRuty2 )
	{
		CString olConditionString;
		olRecord.Values[igRuePrioIdx] = ConstructPrioString();	// priority string
		ConstructString(pomConditionsGrid, olConditionString);

		if (olConditionString.GetLength() > 400)
		{
			olConditionString = olConditionString.Left(399);
			olConditionString += '~';
		}
		olRecord.Values[igRueEvttIdx] = olConditionString;
		olConditionString = ConstructEVRM();
		olRecord.Values[igRueEvrmIdx] = olConditionString;
	}
	else
	{
		olRecord.Values[igRueEvttIdx] = " ";		
		olRecord.Values[igRueEvrmIdx] = " ";
		olRecord.Values[igRuePrioIdx] = " ";
	}

/*
	// FISU (flag IsUsed)
	bool blIsUsed = false;
	CString olFisu;
	olFisu = ogBCD.GetField("RUE", "URNO", omCurrRueUrno, "FISU");
	if (olFisu == CString("1"))
	{
			blIsUsed = true;
	}
	else
	{
		   olRecord.Values[igRueFisuIdx] = CString("0");
		   blIsUsed = false;
	}
*/		
	//  UARC
	olRecord.Values[igRueUarcIdx] = ogBCD.GetField("RUE", "URNO", omCurrRueUrno, "UARC");	//hag990901

	// RUST (rule status)
	if (m_Chb_Acti.GetCheck() == CHECKED)
	{
		olRecord.Values[igRueRustIdx] = CString("0"); // deactivated
	}
	else
	{
		olRecord.Values[igRueRustIdx] = CString("1"); // activated
	}



	
//--- write data to database
	bool blOk = SaveRule( olRecord );
	
	// make regular cursor 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------
/*	CopyDemandRequirements isn't used anywhere
bool FlightIndepRulesView::CopyDemandRequirements()
{
	bool blRet = false;

	CCS_TRY

	CStringArray olRudUrnos;
	CString olNewUrno;
	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD_TMP"));
	int ilRudCount = ogBCD.GetDataCount("RUD_TMP");
	
	for (int i = 0; i < ilRudCount; i++)
	{
		ogBCD.GetRecord("RUD_TMP", i, olRudRecord);
		olNewUrno.Format("%d", ogBCD.GetNextUrno());
		olRudRecord.Values[igRudUrnoIdx] = olNewUrno;
		olRudUrnos.Add(olNewUrno);
		olRudRecord.Values[igRudUrueIdx] = omCurrRueUrno;	//hag990901
		ogBCD.InsertRecord("RUD", olRudRecord, false);
	}

	blRet = ogBCD.Save("RUD");

	CopyRessources(olRudUrnos);
	

	CCS_CATCH_ALL

	return blRet;
}
*/

//-----------------------------------------------------------------------------------------

/*	only used in CopyDemandRequirements which isn't used anywhere
bool FlightIndepRulesView::CopyRessources(CStringArray &opRudUrnos)
{
	bool blRet = true;
	
	CCS_TRY

	
	CString olRud;
	CString olUrno, olUdgr;
		
	CCSPtrArray <RecordSet> olRpfArr;
	CCSPtrArray <RecordSet> olRpqArr;
	CCSPtrArray <RecordSet> olReqArr;
	CCSPtrArray <RecordSet> olRloArr;
	CCSPtrArray <RecordSet> olRudArr;
	CStringList				olUdgrList;
	POSITION				pos;
	int j ;

	int ilSize = opRudUrnos.GetSize();
	RecordSet olRpqRecord(ogBCD.GetFieldCount("RPQ_TMP"));
	
	for (int i = 0; i < ilSize; i++)
	{
		olRud = opRudUrnos.GetAt(i);
	
		// functions
		RecordSet olRpfRecord(ogBCD.GetFieldCount("RPF_TMP"));
		ogBCD.GetRecords("RPF", "URUD", olRud, &olRpfArr);
		int ilCount = olRpfArr.GetSize();
		for ( j = ilCount - 1; j >= 0; j--)
		{
			olRpfRecord = olRpfArr[j];
			olUrno.Format("%d", ogBCD.GetNextUrno());
			olRpfRecord.Values[igRpfUrnoIdx] = olUrno;
			olRpfRecord.Values[igRpfUrudIdx] = olRud;
			ogBCD.InsertRecord("RPF", olRpfRecord);
		}

		// qualifications
		ogBCD.GetRecords("RPQ", "URUD", olRud, &olRpqArr);
		ilCount = olRpqArr.GetSize();
		//for (j = ilCount - 1; j >= 0; j--)
		for (j = 0; j < ilCount; j++)	//hag990923
		{
			olRpqRecord = olRpqArr[j];
			olUrno.Format("%d", ogBCD.GetNextUrno());
			olRpqRecord.Values[igRpqUrnoIdx] = olUrno;
			olRpqRecord.Values[igRpqUrudIdx] = olRud;
			olRpqRecord.Values[igRpqUdgrIdx] = "";
			ogBCD.InsertRecord("RPQ", olRpqRecord);
		}
		//  Liste mit UDGR's aufbauen f�r Suche von nicht zugeordneten Qualis
		if ( ogBCD.GetField ( "RUD_TMP", "URNO", olRud, " UDGR", olUdgr ) &&
			 !olUdgr.IsEmpty() && !olUdgrList.Find(olUdgr) )
			olUdgrList.AddTail(olUdgr);

		// equipment
		RecordSet olReqRecord(ogBCD.GetFieldCount("REQ_TMP"));
		ogBCD.GetRecords("REQ", "URUD", olRud, &olReqArr);
		ilCount = olReqArr.GetSize();
		for (j = ilCount - 1; j >= 0; j--)
		{
			olReqRecord = olReqArr[j];
			olUrno.Format("%d", ogBCD.GetNextUrno());
			olReqRecord.Values[igReqUrnoIdx] = olUrno;
			olReqRecord.Values[igReqUrudIdx] = olRud;
			ogBCD.InsertRecord("REQ", olReqRecord);
		}
		
		// locations
		RecordSet olRloRecord(ogBCD.GetFieldCount("RLO_TMP"));
		ogBCD.GetRecords("RLO", "URUD", olRud, &olRloArr);
		ilCount = olRloArr.GetSize();
		for (j = ilCount - 1; j >= 0; j--)
		{
			olRloRecord = olRloArr[j];
			olUrno.Format("%d", ogBCD.GetNextUrno());
			olRloRecord.Values[igRloUrnoIdx] = olUrno;
			olRloRecord.Values[igRloUrudIdx] = olRud;
			ogBCD.InsertRecord("RLO", olRloRecord);
		}

		olRpfArr.DeleteAll();
		olRpqArr.DeleteAll();
		olReqArr.DeleteAll();
		olRloArr.DeleteAll();
	}

	//  Kopiere alle nicht zugeordneten (Gruppen-)Qualifikationen
	pos = olUdgrList.GetHeadPosition ();
	while ( pos )
	{
		olUdgr = olUdgrList.GetNext ( pos );
		if ( !olUdgr.IsEmpty() )
		{
			ogBCD.GetRecords("RPQ", "UDGR", olUdgr, &olRpqArr);
			
			for (j = 0; j < olRpqArr.GetSize(); j++)
			{
				olRpqRecord = olRpqArr[j];
				olUrno.Format("%d", ogBCD.GetNextUrno());
				olRpqRecord.Values[igRpqUrnoIdx] = olUrno;
				olRpqRecord.Values[igRpqUrudIdx] = "";
				olRpqRecord.Values[igRpqUdgrIdx] = olUdgr;
				ogBCD.InsertRecord("RPQ", olRpqRecord);
			}
			olRpqArr.DeleteAll();
		}
	}
	
	blRet &= ogBCD.Save("RPF");
	blRet &= ogBCD.Save("RPQ");
	blRet &= ogBCD.Save("REQ");
	blRet &= ogBCD.Save("RLO");


	CCS_CATCH_ALL
	return blRet;
}
*/

//-----------------------------------------------------------------------------------------------------------------------

BOOL FlightIndepRulesView::OnHelpInfo(HELPINFO* pHelpInfo)
{
	return TRUE;
}
//----------------------------------------------------------------------------

/*		wird z.Z. nicht benutzt
void FlightIndepRulesView::OnDelete()
{
	CCS_TRY

	CCS_CATCH_ALL

}
*/

//----------------------------------------------------------------------------

void FlightIndepRulesView::OnLeistungen()
{
	CCS_TRY


	UpdateData(TRUE);
	GhsList *polSerList = NULL;
	
	if (omCurrRueUrno != "")
	{
		polSerList = new GhsList(this, LBS_MULTIPLESEL);
	}
	else
	{
		// Do not open services list in case no rule has been chosen.
		//	Otherwise there'd be no Rule Urno for DataSet::CreateRudFromSerList() to use (RUD.URUE).
		MessageBox(GetString(1408), GetString(1409), MB_OK | MB_ICONEXCLAMATION);
		bgIsServicesOpen = false;
	}


	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

// not used at the moment, mne 010122
/*void FlightIndepRulesView::OnContractBtn()
{
	CCS_TRY


	CCS_CATCH_ALL
}*/
//----------------------------------------------------------------------------

// not used at the moment, mne 010122
/*void FlightIndepRulesView::OnGroupBtn()
{
	CCS_TRY


	CCS_CATCH_ALL
}*/
//----------------------------------------------------------------------------

void FlightIndepRulesView::OnSelEndOK()
{
	OnNewRule();
}
//----------------------------------------------------------------------------

void FlightIndepRulesView::OnCopy()
{	
	CCS_TRY


	long llUrno = ogBCD.GetNextUrno();
	CString olOldRueUrno = omCurrRueUrno;
	omCurrRueUrno.Format("%d", llUrno);
	m_Edt_RuleShortName.SetWindowText(CString(""));
	bmNewFlag = true;
	bool blOk = ogDataSet.OnCopyRue ( olOldRueUrno, omCurrRueUrno );
	//hag010509: Da passiert doch gar nichts UpdateServiceTable();
	pomDutyGrid->DisplayRudTmp ();

	bgIsValSaved = false;

	// inform ChangeInfo that rule is copied
	ogChangeInfo.SetCopiedRuleFlag();
	ogChangeInfo.SetCurrRueUrno(omCurrRueUrno);


	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

LONG FlightIndepRulesView::OnDragOver(UINT wParam, LONG lParam)
{
	CCSDragDropCtrl *pomDragDropCtrl = &m_DragDropTarget;

	int ilClass = m_DragDropTarget.GetDataClass(); 
	
	if (ilClass == DIT_SERLIST)
	{
		return 0;
	}
	return -1L;
}
//---------------------------------------------------------------------------------------------

LONG FlightIndepRulesView::OnDrop(UINT wParam, LONG lParam)
{
	CCS_TRY


	CCSDragDropCtrl *polDragDropCtrl = &m_DragDropTarget;
	CStringArray	olSerUrnos;

	for (int ilIndex = 0; ilIndex < polDragDropCtrl->GetDataCount(); ilIndex++)
	{
		CString olUrno;
		olUrno.Format("%d", polDragDropCtrl->GetDataDWord(ilIndex));
		if (olUrno != "" && olUrno != " ")
		{
			olSerUrnos.Add(olUrno);
		}
	}

	AddServices ( olSerUrnos );
    CloseGantt ();
	pomDutyGrid->DisplayRudTmp ();

	
	CCS_CATCH_ALL

	return -1L;
}
//----------------------------------------------------------------------------

void FlightIndepRulesView::OnRButtonDown(UINT itemID, LPARAM lParam)
{
	CPoint point(lParam);
	
	// get context menu
	MakeBarMenu(itemID, point);
	CWnd::OnRButtonDown(0, point);

}
//----------------------------------------------------------------------------



void FlightIndepRulesView::OnMenuDeleteBar()
{
	CCS_TRY
	CStringArray olUrnoList;


	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	if(	pomDutyGrid->IsExpanded())
	{
		pomDutyGrid->GetSelectedUrnos(olUrnoList);

		for (int i = 0; i < olUrnoList.GetSize(); i++)
		{
			if(ogBCD.GetRecord("RUD_TMP", "URNO", olUrnoList.GetAt(i), olRecord))
			{
				if ( ogBCD.DeleteRecord("RUD_TMP", "URNO", olUrnoList.GetAt(i)) )
				{
					// tell viewer that the record was deleted
					ogDdx.DataChanged((void *)this,RUD_TMP_DELETE,(void *)&olRecord);
				}
			}
		}
	}
	else
	{
		if ( !omCurrDgrUrno.IsEmpty() )
		{	//  eine Demand-Group ist selektiert u. soll gel�scht werden
			CCSPtrArray<RecordSet>	olRudsOfDgr;
			CString					olUrno;

			ogBCD.GetRecords ( "RUD_TMP", "UDGR", omCurrDgrUrno, &olRudsOfDgr );
			for ( int i=olRudsOfDgr.GetSize()-1; i>=0; i-- )
			{
				olRecord = olRudsOfDgr[i];
				olUrno = olRecord.Values[igRudUrnoIdx];
				// delete this record from RUD_TMP
				if ( ogBCD.DeleteRecord("RUD_TMP", "URNO", olUrno ) )
				{
					// tell viewer that the record was deleted
					ogDdx.DataChanged((void *)this,RUD_TMP_DELETE,(void *)&olRecord);
				}
			}			
			olRudsOfDgr.DeleteAll();
		}
	}
	pomDutyGrid->DisplayRudTmp ();

	CCS_CATCH_ALL
}

/*
void FlightIndepRulesView::OnMenuDeleteBar()
{
	CCS_TRY


	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));

	if(	pomDutyGrid->IsExpanded() && !omCurrRudUrno.IsEmpty() )
	{
		// get record (we need this for our DDX call)
		ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);

		// delete this record from RUD_TMP
		if ( ogBCD.DeleteRecord("RUD_TMP", "URNO", omCurrRudUrno) )
		{
			// tell viewer that the record was deleted
			ogDdx.DataChanged((void *)this,RUD_TMP_DELETE,(void *)&olRecord);
		}
	}
	else
	{
		if ( !omCurrDgrUrno.IsEmpty() )
		{	//  eine Demand-Group ist selektiert u. soll gel�scht werden
			CCSPtrArray<RecordSet>	olRudsOfDgr;
			CString					olUrno;

			ogBCD.GetRecords ( "RUD_TMP", "UDGR", omCurrDgrUrno, &olRudsOfDgr );
			for ( int i=olRudsOfDgr.GetSize()-1; i>=0; i-- )
			{
				olRecord = olRudsOfDgr[i];
				olUrno = olRecord.Values[igRudUrnoIdx];
				// delete this record from RUD_TMP
				if ( ogBCD.DeleteRecord("RUD_TMP", "URNO", olUrno ) )
				{
					// tell viewer that the record was deleted
					ogDdx.DataChanged((void *)this,RUD_TMP_DELETE,(void *)&olRecord);
				}
			}			
			olRudsOfDgr.DeleteAll();
		}
	}


	CCS_CATCH_ALL
}
*/


//----------------------------------------------------------------------------
//						helper functions
//----------------------------------------------------------------------------
void FlightIndepRulesView::MakeBarMenu(int itemID,  CPoint point) 
{
	CCS_TRY

	CString olUdgr, olUrud;

	bool blMultiDgr = ( !pomDutyGrid->IsExpanded() && !pomDutyGrid->GetSelectionInfo ( olUdgr, olUrud ) );
			
	if ( (itemID != -1) && !blMultiDgr )
	{
		//-- create menu
		CMenu menu;
		menu.CreatePopupMenu();
		
		//-- remove all (old) menu items
		for (int i = menu.GetMenuItemCount() - 1; i >= 0; i--)
		{
			menu.RemoveMenu(i, MF_BYPOSITION);
		}
		
		//-- create menu items
		menu.AppendMenu(MF_STRING, CM_INDEP_MENU_DELETEBAR, GetString(131));	// remove demand requirement from listbox
		if ( pomDutyGrid->IsExpanded() )
		{
			menu.AppendMenu(MF_STRING, CM_INDEP_MENU_CREATELNK, GetString(2003));				// create grouping (Staff - location)
			menu.AppendMenu(MF_STRING, CM_INDEP_MENU_DELETELNK, GetString(2004));				// delete grouping
		}
		// calculate correct point
		// (offset btw. rect of view and rect of table)
		CRect olViewRect, olTableRect;
		GetWindowRect(olViewRect);
		pomDutyGrid-> GetWindowRect(olTableRect);
		int ilDiffX = olTableRect.left;
		int ilDiffY = olTableRect.top;
		point.x += ilDiffX + 10;
		point.y += ilDiffY;

		//-- show menu 
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
	}


	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

// This function contains the table initialization that has to be done only once
// The rest can be found in FillTable()
void FlightIndepRulesView::TablesFirstTimeInit()
{
	CCS_TRY
	CRect olRect;
	int   ilWidth;

//--- conditions grid
	pomConditionsGrid->SubclassDlgItem(IDC_CONDITION_LIST, this);
	pomConditionsGrid->Initialize();
	pomConditionsGrid->IniLayoutForConditions();

//--- left ressources grid
	pomLeftGrid->SubclassDlgItem(INDEP_CUS_LEFT, this);
	pomLeftGrid->Initialize();
	
	pomLeftGrid->LockUpdate(TRUE);

	pomLeftGrid->SetColCount(3);

	pomLeftGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomLeftGrid->SetColWidth(0,0,20);			// row header
	pomLeftGrid->SetColWidth(1,1,ilWidth*4 / 10);	// code
	pomLeftGrid->SetColWidth(2,2,ilWidth*6 / 10);	// name

	pomLeftGrid->HideCols(3, 3);

	pomLeftGrid->SetValueRange(CGXRange(0,1), GetString(CODE));	// code
	pomLeftGrid->SetValueRange(CGXRange(0,2), GetString(NAME));	// name
	pomLeftGrid->SetValueRange(CGXRange(0,3), CString("Urno"));	// urno
	
	CString olCode;
	CString olName;

	pomLeftGrid->GetParam()->SetNumberedRowHeaders(FALSE); 
	pomLeftGrid->GetParam()->EnableSelection(GX_SELNONE );	
	pomLeftGrid->GetParam()->EnableTrackRowHeight(0); 
	
	pomLeftGrid->RegisterGxCbsDropDown(true, true);

	pomLeftGrid->Redraw();
	pomLeftGrid->LockUpdate(FALSE);



//--- right ressources grid
	pomRightGrid->SubclassDlgItem(INDEP_CUS_RIGHT, this);
	pomRightGrid->Initialize();
	
	pomRightGrid->SetColCount(3);

	pomRightGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomRightGrid->SetColWidth(0,0,20);			// row header
	pomRightGrid->SetColWidth(1,1,ilWidth*4 / 10);	// code
	pomRightGrid->SetColWidth(2,2,ilWidth*6 / 10);	// name

	pomRightGrid->HideCols(3, 3);

	pomRightGrid->SetValueRange(CGXRange(0,1), GetString(CODE));	// code
	pomRightGrid->SetValueRange(CGXRange(0,2), GetString(NAME));	// name
	pomRightGrid->SetValueRange(CGXRange(0,3), CString("Urno"));	// urno
	
	pomRightGrid->GetParam()->SetNumberedRowHeaders(FALSE); 
	pomRightGrid->GetParam()->EnableSelection(GX_SELNONE );	
	pomRightGrid->GetParam()->EnableTrackRowHeight(0); 

	pomRightGrid->RegisterGxCbsDropDown ( true, true );

	
	
//--- duty grid
	pomDutyGrid->SubclassDlgItem(IDC_DUTY_LIST, this);
	pomDutyGrid->Initialize();
	pomDutyGrid->IniLayout ();
	pomDutyGrid->GetWindowRect(&omDutyOriginalRect);
	
	ScreenToClient (&omDutyOriginalRect);

	
	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

void FlightIndepRulesView::EnableDemandDetailControls(bool bpEnable)
{
	if (bpEnable == false)
	{
		pomLeftGrid->SetStyleRange(CGXRange().SetTable(), CGXStyle().SetEnabled(FALSE)
																	.SetReadOnly(TRUE));
		pomRightGrid->SetStyleRange(CGXRange().SetTable(), CGXStyle().SetEnabled(FALSE)
																	.SetReadOnly(TRUE));
		DecideTimeControls ();
	}
	else
	{
		pomLeftGrid->SetStyleRange(CGXRange().SetTable(), CGXStyle().SetEnabled(TRUE)
																	.SetReadOnly(FALSE));
		pomRightGrid->SetStyleRange(CGXRange().SetTable(), CGXStyle().SetEnabled(TRUE)
																	.SetReadOnly(FALSE));
	}

} 
//----------------------------------------------------------------------------

void FlightIndepRulesView::FillDemandDetailControls() 
{
	CCS_TRY


	ClearRessourceTables();

	if (!pomGantt)
	{
		imRessourceType = pomDutyGrid->GetResTypeForSelection();
	}
	else
	{	
		//  Gantt-Darstellung
		CString olRety;
		if (!omCurrRudUrno.IsEmpty() )
		{
			olRety = ogBCD.GetField("RUD_TMP", "URNO", omCurrRudUrno, "RETY" );
			imRessourceType = olRety.Find ('1') ;
		}
	}
		

	switch (imRessourceType)
	{
		case 0:
			SetTablesForStaffView();
			break;

		case 1:
			SetTablesForEquipmentView();
			break;

		case 2:
			SetTablesForLocationsView();
	}


//--- time fields and number of ressources
	DecideTimeControls ();

	if (pomDutyGrid->IsExpanded() && !omCurrRudUrno.IsEmpty() && imRuty2)
	{
		CTime olTime;
		RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
		ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord);
		
		if (olRecord.Values[igRudDbflIdx] == "1")
		{
			olTime = DBStringToDateTime(olRecord.Values[igRudEadbIdx]);
		}
		else
		{
			olTime = DBStringToDateTime(olRecord.Values[igRudDebeIdx]);
		}

/*		m_Dtc_Dube.SetTime(&olTime);*/
		m_Dtc_Dube.SetInitText(olTime.Format("%H:%M"), true);

		if (olRecord.Values[igRudDeflIdx] == "1")
		{
			olTime = DBStringToDateTime(olRecord.Values[igRudLadeIdx]);
		}
		else
		{
			olTime = DBStringToDateTime(olRecord.Values[igRudDeenIdx]);
		}

/*		m_Dtc_Duen.SetTime(&olTime);*/
		m_Dtc_Duen.SetInitText(olTime.Format("%H:%M"), true);

	}
	else
	{	
		//  Zeitgesteuert und Sammeldarstellung
		if (!pomDutyGrid->IsExpanded() && !omCurrDgrUrno.IsEmpty() && imRuty2)
		{
			SetTimeControlsForDgr ( omCurrDgrUrno ); 
		}
	}

	DisplaySplitValues ();
	DecideTimeControls ();

	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

void FlightIndepRulesView::SetTablesForStaffView()
{
	CCS_TRY


	CCSPtrArray <RecordSet> olLeftArr;

	int ilLeftCount = GetResRecordsToDisplay("RPF_TMP", &olLeftArr);
	
	if (ilLeftCount >= 1)
	{
		SetCurrentRudUrno(olLeftArr[0].Values[igRpfUrudIdx]);
	}
	else
	{
		SetCurrentRudUrno("");
	}

	
	ClearRessourceTables();
	
	if (!ilLeftCount)
	{
		return;
	}


//--- make functions choicelist
	CString olChoiceList;
	olChoiceList = GetChoiceList("PFC", "FCTC");


//--- functions table
	RecordSet olRecord;
	CString olName;
	CString olCode;
	CString olUrno;
	CRect olRect;
	int		ilWidth;

	pomLeftGrid->LockUpdate(TRUE);
	int ilRowsInTable = ilLeftCount;
	if(	!pomDutyGrid->IsExpanded() && !pomGantt )	//  Darstellung von T�tigkeiten
		ilRowsInTable++;
	
	pomLeftGrid->SetAutoGrow(!pomDutyGrid->IsExpanded() && !pomGantt);

	pomLeftGrid->InsertRows(1,ilRowsInTable);
	
	// reset grids in case locations were shown before
	pomLeftGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomLeftGrid->SetColWidth(0,0,20);			// row header
	pomLeftGrid->SetColWidth(1,1,ilWidth*4 / 10);	// code
	pomLeftGrid->SetColWidth(2,2,ilWidth*6 / 10);	// name
	pomLeftGrid->HideCols(2, 2, FALSE);	// show second column

	pomLeftGrid->SetStyleRange(CGXRange(1, 1, ilRowsInTable, 1), CGXStyle()
										 .SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
										 .SetChoiceList(olChoiceList));

	//- column names
	pomLeftGrid->SetValueRange(CGXRange(0,1), GetString(CODE));
	pomLeftGrid->SetValueRange(CGXRange(0,2), GetString(NAME));
	pomLeftGrid->SetValueRange(CGXRange(0,3), CString("Urno"));
	
	//- fill cells with data
	for (int i = 0; i < ilLeftCount; i++)
	{
		olRecord = olLeftArr[i];

		olUrno = olRecord.Values[igRpfUrnoIdx];
		if ( GetGroupName ( &olRecord, igRpfGtabIdx, igRpfUpfcIdx, olCode ) )	//hag990827
		{
			ogBCD.GetField("SGR", "GRPN", olCode, "GRDS", olName);				//hag990827
			olCode = "*"+ olCode;
		}
		else
		{
			olCode = olRecord.Values[igRpfFccoIdx];
			ogBCD.GetField("PFC", "FCTC", olCode, "FCTN", olName);
		}
		pomLeftGrid->SetValueRange(CGXRange(i+1,1), olCode);
		pomLeftGrid->SetValueRange(CGXRange(i+1,2), olName);
		pomLeftGrid->SetValueRange(CGXRange(i+1,3), olUrno);
	}

	pomLeftGrid->SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetEnabled(FALSE) );
	pomLeftGrid->LockUpdate(FALSE);
	pomLeftGrid->Redraw();
	
//--- make qualifications choicelist
	olChoiceList = GetChoiceList("PER", "PRMC");

	//--- qualifications table
	pomRightGrid->LockUpdate(TRUE);
	pomRightGrid->InsertRows(1, 1);

	// reset grid settings in case locations were shown before
	pomRightGrid->SetAutoGrow(true);
	pomRightGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomRightGrid->SetColWidth(0,0,20);			// row header
	pomRightGrid->SetColWidth(1,1,ilWidth*4 / 10);	// code
	pomRightGrid->SetColWidth(2,2,ilWidth*6 / 10);	// name
	pomRightGrid->HideCols(2, 2, FALSE);	// show second column

	pomRightGrid->SetStyleRange(CGXRange(1, 1), CGXStyle()
										 .SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
										 .SetChoiceList(olChoiceList));
	//- column names
	pomRightGrid->SetValueRange(CGXRange(0, 1), GetString(CODE));
	pomRightGrid->SetValueRange(CGXRange(0, 2), GetString(NAME));
	pomRightGrid->SetValueRange(CGXRange(0, 3), CString("Urno"));
	pomRightGrid->LockUpdate(FALSE);

	// set qualification entries
	DisplayQualisForRud (omCurrRudUrno);

	olLeftArr.DeleteAll();
	SetDlgItemText ( INDEP_STA_VRGC, GetString(PFC_HEADER) );
	SetDlgItemText ( INDEP_STA_PERM, GetString(PER_HEADER) );

	
	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

void FlightIndepRulesView::SetTablesForEquipmentView()
{
	CCS_TRY


	CCSPtrArray <RecordSet> olRightArr;
	CStringArray olTypeArray;	
	CString olItem;
	CString olChoiceList;
	CRect		olRect;
	int			ilWidth;
	
	int ilRightCount = GetResRecordsToDisplay( "REQ_TMP", &olRightArr );

	if ( ilRightCount >= 1 )
		SetCurrentRudUrno ( olRightArr[0].Values[igReqUrudIdx] );	
	else
		SetCurrentRudUrno ("");

	int i/*, j, ilSize*/;
	RecordSet olRecord;
	CString olName;
	CString olCode;
	CString olUrno, olType;

	ClearRessourceTables();

	//--- location table
	pomRightGrid->SetAutoGrow(false);
	pomRightGrid->LockUpdate(TRUE);
	pomRightGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomRightGrid->SetColWidth(0,0,20);			// row header
	pomRightGrid->SetColWidth(1,1,ilWidth*4 / 10);	// code
	pomRightGrid->SetColWidth(2,2,ilWidth*6 / 10);	// name
	pomRightGrid->HideCols(2, 2, FALSE);	// show second column

	//- column names
	pomRightGrid->SetValueRange(CGXRange(0,1), GetString(CODE));
	pomRightGrid->SetValueRange(CGXRange(0,2), GetString(NAME));
	pomRightGrid->SetValueRange(CGXRange(0,3), CString("Urno"));
	
	//- fill cells with data
	for (i = 0; i < ilRightCount; i++)
	{
		olRecord = olRightArr[i];

		olUrno = olRecord.Values[igReqUrnoIdx];
		olTypeArray.SetAtGrow(i, olRecord.Values[igReqEtypIdx]);
		if ( GetGroupName ( &olRecord, igReqGtabIdx, igReqUequIdx, olCode ) )
		{
			ogBCD.GetField("SGR", "GRPN", olCode, "GRDS", olName);			
			olCode = "*"+ olCode;
		}
		else
		{
			olCode = olRecord.Values[igReqEqcoIdx];
			ogBCD.GetField("EQU", "GCDE", olCode, "ENAM", olName);
		}
		pomRightGrid->InsertRows(i + 1, 1);
		olChoiceList = GetChoiceList("EQU", "GCDE", &olRecord.Values[igReqEtypIdx] );
		pomRightGrid->SetStyleRange(CGXRange(i + 1, 1), CGXStyle()
											.SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
											.SetChoiceList(olChoiceList));
		//- fill cell with data
		pomRightGrid->SetValueRange(CGXRange(i+1,1), olCode);
		pomRightGrid->SetValueRange(CGXRange(i+1,2), olName);
		pomRightGrid->SetValueRange(CGXRange(i+1,3), olUrno);
	}

	pomRightGrid->SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetEnabled(FALSE) );
	pomRightGrid->LockUpdate(FALSE);
	pomRightGrid->Redraw();	


	//--- equipment table types
	//- column names
	pomLeftGrid->SetValueRange(CGXRange(0,1), GetString(NAME));
	pomLeftGrid->SetValueRange(CGXRange(0,2), "");

	//- fill cells with data
	if (igEqtNameIdx >= 0)		//  Wenn ALO da und gef�llt
	{
		pomLeftGrid->LockUpdate(TRUE);
		for ( i=0; i<ilRightCount; i++ )
		{
			CString olReft, olTable, olCodeField, olDesc;
			olType = olTypeArray.GetAt ( i );
	
			olName = ogBCD.GetField("EQT", "URNO", olType, "NAME");
			pomLeftGrid->InsertRows(i + 1, 1);
			pomLeftGrid->SetValueRange(CGXRange(i+1,1), olName);
			pomLeftGrid->SetValueRange(CGXRange(i+1,2), "");
			pomLeftGrid->SetStyleRange(CGXRange().SetRows(i + 1), CGXStyle().SetEnabled(FALSE));
		}
		pomLeftGrid->GetClientRect(&olRect);
		olRect.DeflateRect(3, 0);
		ilWidth = olRect.Width() - 20;
		pomLeftGrid->SetColWidth(1,1,ilWidth);	// code
		pomLeftGrid->HideCols(2, 2);
		pomLeftGrid->LockUpdate(FALSE);
		pomLeftGrid->Redraw();
	}
	olRightArr.DeleteAll();
	SetDlgItemText ( INDEP_STA_VRGC, GetString(IDS_EQUIPMENT_TYPES) );
	SetDlgItemText ( INDEP_STA_PERM, GetString(EQU_HEADER) );


	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

void FlightIndepRulesView::SetTablesForLocationsView()
{
	CCS_TRY


	CCSPtrArray <RecordSet> olRightArr;
	CString olItem;
	CString olChoiceList;
	CStringArray olReftArray;	
	CRect		olRect;
	int			ilWidth;
	
	int ilRightCount = GetResRecordsToDisplay( "RLO_TMP", &olRightArr );

	if ( ilRightCount >= 1 )
		SetCurrentRudUrno ( olRightArr[0].Values[igRloUrudIdx] );	
	else
		SetCurrentRudUrno ("");

	int i/*, j, ilSize*/;
	int ilLeftCount = ogBCD.GetDataCount ( "ALO" );

	ClearRessourceTables();

	//--- location table
	pomRightGrid->SetAutoGrow(false);
	pomRightGrid->LockUpdate(TRUE);
	for (i = 0; i < ilRightCount; i++)
	{
		CString olCode, olTable, olCodeField;
		if ( GetGroupName ( &(olRightArr[i]), igRloGtabIdx, igRloRlocIdx, olCode ) )
			olCode = "*"+ olCode;
		else
			if ( !GetLocationCode(olRightArr[i].Values[igRloRlocIdx], olRightArr[i].Values[igRloReftIdx], olCode) )
			{
				olCode = "???";
			}
		olReftArray.SetAtGrow(i, olRightArr[i].Values[igRloReftIdx]);
		olChoiceList = CString("");
		if (ParseReftFieldEntry(olRightArr[i].Values[igRloReftIdx], olTable, olCodeField))
		{
			olChoiceList = GetChoiceList(olTable, olCodeField);
		}
		pomRightGrid->InsertRows(i + 1, 1);
		pomRightGrid->SetStyleRange(CGXRange(i + 1, 1), CGXStyle()
											.SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
											.SetChoiceList(olChoiceList));
		//- fill cell with data
		pomRightGrid->SetValueRange(CGXRange(i + 1, 1), olCode);
		pomRightGrid->SetValueRange(CGXRange(i + 1, 3), olRightArr[i].Values[igRloUrnoIdx]);
	}

	//- column names
	pomRightGrid->SetValueRange(CGXRange(0, 1), GetString(CODE));
	pomRightGrid->SetValueRange(CGXRange(0, 2), "");
	pomRightGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomRightGrid->SetColWidth(1,1,ilWidth);	// code

	pomRightGrid->HideCols(2, 2);
	pomRightGrid->LockUpdate(FALSE);
	pomRightGrid->Redraw();

	//--- location table types
	//- column names
	pomLeftGrid->SetValueRange(CGXRange(0,1), GetString(CODE));
	pomLeftGrid->SetValueRange(CGXRange(0,2), "");

	//- fill cells with data
	if (ilLeftCount > 0)		//  Wenn ALO da und gef�llt
	{
		pomLeftGrid->LockUpdate(TRUE);
		for ( i=0; i<ilRightCount; i++ )
		{
			CString olReft, olTable, olCodeField, olDesc;
			olReft = olReftArray.GetAt ( i );
	
			olDesc = ogBCD.GetField("ALO", "REFT", olReft, "ALOD");
			pomLeftGrid->InsertRows(i + 1, 1);
			pomLeftGrid->SetValueRange(CGXRange(i+1,1), olDesc);
			pomLeftGrid->SetValueRange(CGXRange(i+1,2), "");
			pomLeftGrid->SetStyleRange(CGXRange().SetRows(i + 1), CGXStyle().SetEnabled(FALSE));
		}
		pomLeftGrid->GetClientRect(&olRect);
		olRect.DeflateRect(3, 0);
		ilWidth = olRect.Width() - 20;
		pomLeftGrid->SetColWidth(1,1,ilWidth);	// code
		pomLeftGrid->HideCols(2, 2);
		pomLeftGrid->LockUpdate(FALSE);
		pomLeftGrid->Redraw();
	}
	olRightArr.DeleteAll();
	SetDlgItemText ( INDEP_STA_VRGC, GetString(LOCATION_TYP) );
	SetDlgItemText ( INDEP_STA_PERM, GetString(IDS_LOCATION) );


	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

void FlightIndepRulesView::ClearRessourceTables()
{
	CCS_TRY

	
	// left grid
	int ilCount = pomLeftGrid->GetRowCount();
	if (ilCount > 0)
	{
		pomLeftGrid->RemoveRows(1, ilCount);
	}
	pomLeftGrid->Redraw();

	// right grid
	ilCount = pomRightGrid->GetRowCount();
	if (ilCount > 0)
	{
		pomRightGrid->RemoveRows(1, ilCount);
	}
	pomRightGrid->Redraw();

	
	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------

bool FlightIndepRulesView::SaveDemandRequirements()
{
	bool blReturn = true;

	CCS_TRY

	
	//--- copy all the rud entries from RUD_TMP to RUD
	RecordSet olRecord;
	int ilSize = ogBCD.GetDataCount("RUD_TMP");

	if (ilSize > 0)
	{
		for (int i = 0; i < ilSize; i++)
		{
			bool blErr1 = ogBCD.GetRecord("RUD_TMP", i, olRecord);
			
			// check if end > start
			if ( imRuty2 )
			{	//  zeitgesteuerte Regel 
				CString olTimeStrB, olTimeStrE;
				CTime olTimeB, olTimeE;

				int ilStartIdx = (bmIsFloating != TRUE ? igRudDebeIdx : igRudEadbIdx);
				int ilEndIdx = (bmIsFloating != TRUE ? igRudDeenIdx : igRudLadeIdx);

				olTimeStrB = olRecord.Values[ilStartIdx]; 
				olTimeStrE = olRecord.Values[ilEndIdx]; 
				
				olTimeB = DBStringToDateTime(olTimeStrB);
				olTimeE = DBStringToDateTime(olTimeStrE);

				CString TestUrno, TestDedu;
				TestUrno = olRecord.Values[igRudUrnoIdx];
				TestDedu = olRecord.Values[igRudDeduIdx];

				
				// HIER MUSS GE�NDERT WERDEN

				if (olTimeE < olTimeB)
				{
					CTimeSpan olSpan;
					olSpan = CTimeSpan(1, 0, 0, 0);
					olTimeE += olSpan;

					olRecord.Values[ilEndIdx] = olTimeE.Format("%Y%m%d%H%M%S");
				}
				
				if ( (ilStartIdx == igRudDebeIdx) && (ilEndIdx == igRudDeenIdx) )
				{
					// calculate duration if not floating 
					long llDuration = (olTimeE - olTimeB).GetTotalSeconds();
					while (llDuration > 86400)
						llDuration -= 86400;
					olRecord.Values[igRudDeduIdx].Format("%d", llDuration);
				}
			}
			else
			{
				olRecord.Values[igRudDebeIdx] = 
				olRecord.Values[igRudDeenIdx] = 
				olRecord.Values[igRudEadbIdx] = 
				olRecord.Values[igRudLadeIdx] = "";
				//  �nderung mu� auch in RUD_TMP zur�ckgeschrieben werden, sonst 
				//  wird sp�ter angebliche �nderung des Records gemeldet
				ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values );
			}
			bool blErr2 = ogBCD.InsertRecord("RUD", olRecord, false);
			if (blErr1 == false || blErr2 == false)
			{
				ogLog.Trace("DEBUG", "[RegelWerkView::SaveDemandPrefs] Failed appending RUD record [index %d] to object.", i);
				blReturn = false;
				break;
			}
		}

		// save RUD records to database
		if (blReturn)
		{ 
			bool blErr = ogBCD.Save("RUD");
			if (blErr == true)
			{
				ogDataSet.FillLogicRudObject(omCurrRueUrno);
				ogLog.Trace("WRITE2DB", "[RegelWerkView::SaveDemandPrefs] Saved %d RUD records to database", i);
			}
			else
			{
				blReturn = false;
				ogLog.Trace("DEBUG","Failed saving RUD records to database");
			}
		}
	}

	CCS_CATCH_ALL

	return blReturn;
}
//----------------------------------------------------------------------------

void FlightIndepRulesView::UpdateServiceTable()
{
	int ilRudCount = ogBCD.GetDataCount("RUD_TMP");

	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	CString olSerUrno;
	CString olName;
	long llRudUrno;
	for (int i = 0; i < ilRudCount; i++)
	{
		ogBCD.GetRecord("RUD_TMP", i, olRecord);
		
		olSerUrno = olRecord.Values[igRudUghsIdx];
		llRudUrno = atol(olRecord.Values[igRudUrnoIdx]);
		olName = ogBCD.GetField("SER", "URNO", olSerUrno, "SNAM");
	}
}
//----------------------------------------------------------------------------

bool FlightIndepRulesView::IsPassFilter(RecordSet *popRud)
{	
	// Exception handling should not be implemented in this function 
	// Frequently repeated exception handling code might slow down program execution

	//--- return value 
	bool blRet = false;

	//--- get data from CedaBasicData
	CString olUrue;
	olUrue = popRud->Values[igRudUrueIdx];

	//--- compare data with filter conditions
	if(olUrue == omCurrRueUrno)
	{
		blRet = true;
	}

	return blRet;
}



//----------------------------------------------------------------------------
//						some stuff (diagnostics)
//----------------------------------------------------------------------------
#ifdef _DEBUG
void FlightIndepRulesView::AssertValid() const
{
	CRulesFormView::AssertValid();
}
//----------------------------------------------------------------------------

void FlightIndepRulesView::Dump(CDumpContext& dc) const
{
	CRulesFormView::Dump(dc);
}
#endif //_DEBUG



//---------------------------------------------------------------------------
//					processing functions
//---------------------------------------------------------------------------
void FlightIndepRulesView::ProcessRudTmpNew(RecordSet *popRecord)
{
	if (popRecord != NULL)
	{
		pomDutyGrid->OnNewRudTmp ( popRecord );
		if (IsPassFilter(popRecord))
		{
			CString olSerUrno;
			CString olName;

			olSerUrno = popRecord->Values[igRudUghsIdx];
			olName = ogBCD.GetField("SER", "URNO", olSerUrno, "SNAM");
			long llUrno = atol(popRecord->Values[igRudUrnoIdx]);

			pomDutyGrid->DisplayRudTmp ();
		}
	}
}
//---------------------------------------------------------------------------

void FlightIndepRulesView::ProcessRudTmpChange(RecordSet *popRecord)
{
	
}
//---------------------------------------------------------------------------

void FlightIndepRulesView::ProcessRudTmpDelete(RecordSet *popRecord)
{
	pomDutyGrid->OnDeleteRudTmp ( popRecord );

	if ( pomGantt && !omCurrDgrUrno.IsEmpty() )
	{
		CCSPtrArray<RecordSet> olDgrMembers;
		ogBCD.GetRecords ( "RUD_TMP", "UDGR", omCurrDgrUrno, &olDgrMembers );
		if ( olDgrMembers.GetSize() < 1 )
			CloseGantt ();
		olDgrMembers.DeleteAll ();
	}
	ogDataSet.DeleteRessources(*popRecord);					
}
//---------------------------------------------------------------------------
/*
bool FlightIndepRulesView::IsSingleResourceSelected ( ROWCOL ipRow, ROWCOL ipCol,
													  CGridFenster *popGrid,
													  CString opTable, CString &ropCode )
{
	int			ilIndex, ilSingles;
	CString		olFound, olChoiceList;
	CGXComboBoxWnd	*polCBWnd;
	CGXStyle		olStyle;
	
	ASSERT(popGrid);
	//  Choicelist f�r das angegebene Feld holen
	polCBWnd = (CGXComboBoxWnd*)popGrid->GetControl ( ipRow, ipCol );
	if ( !polCBWnd  )
		return true;		//  default fall
	popGrid->ComposeStyleRowCol( ipRow, ipCol, &olStyle);
	if ( !olStyle.GetIncludeChoiceList() )
		return true;		//  default fall

	olChoiceList = olStyle.GetChoiceList() ;
	//  Index der gew�hlten Resource in Combobo und Anzahl der Datens�tze 
	//  im entsprechenden Resourcetable ermitteln
	ilIndex = polCBWnd->FindStringInChoiceList( olFound, (const char*)ropCode, 
											    (const char*)olChoiceList, TRUE );
	ilSingles = ogBCD.GetDataCount ( opTable );
	if ( !ropCode.IsEmpty() && ropCode[0] == '*' )
		ropCode = ropCode.Right ( ropCode.GetLength()-1 );

	return ( ilIndex<ilSingles );
}
*/
//---------------------------------------------------------------------------

bool FlightIndepRulesView::CheckChanges()
{
	bool blRet = true;
	CString olTmp;


	CCS_TRY

	if (bmNewFlag == false)
	{
		RecordSet olRue(ogBCD.GetFieldCount("RUE"));
		ogBCD.GetRecord("RUE", "URNO", omCurrRueUrno, olRue);
		
		// RUSN
		m_Edt_RuleShortName.GetWindowText(olTmp);	
		blRet &= (olRue.Values[igRueRusnIdx] == olTmp);

		// RUNA
		m_Edt_RuleName.GetWindowText(olTmp);	
		blRet &= (olRue.Values[igRueRunaIdx] == olTmp);

		// EVTY
		olTmp = imRuty2  ? "2" : "3";
		blRet &= (olRue.Values[igRueRutyIdx] == olTmp);

		if ( !imRuty2 )	//controlled by condition
		{
			ConstructString(pomConditionsGrid, olTmp, 400 ) ;
			blRet &= (olRue.Values[igRueEvttIdx] == olTmp);
		}
		
		// REMA
		m_Edt_Descript.GetWindowText(olTmp);
		if (olTmp.GetLength() > 254)
		{
			olTmp = olTmp.Left(254);
		}
		CCSCedaData::MakeCedaString(olTmp);
		blRet &= (olRue.Values[igRueRemaIdx] == olTmp);
		
		// RUST
		olTmp = (m_Chb_Acti.GetCheck() == CHECKED) ? "0" : "1";
		blRet &= (olTmp == olRue.Values[igRueRustIdx]);
	}
	else
	{
		// RUSN
		m_Edt_RuleShortName.GetWindowText(olTmp);
		blRet &= (olTmp == CString(""));

		// RUNA
		m_Edt_RuleName.GetWindowText(olTmp);
		blRet &= (olTmp == CString(""));
	}


	CCS_CATCH_ALL

	return blRet;
}
//---------------------------------------------------------------------------

void FlightIndepRulesView::OnRadioRuty2() 
{
	CheckDlgButton(IDC_RADIO_RUTY2, 1);
	CheckDlgButton(IDC_RADIO_RUTY3, 0);
	OnChangedRuty();
}
//---------------------------------------------------------------------------

void FlightIndepRulesView::OnRadioRuty3() 
{
	CheckDlgButton ( IDC_RADIO_RUTY2, 0 );
	CheckDlgButton ( IDC_RADIO_RUTY3, 1 );
	OnChangedRuty();
}
//---------------------------------------------------------------------------

void FlightIndepRulesView::OnChangedRuty() 
{
	int ilOldRuty = imRuty2;

	UpdateData ();

	if ( ilOldRuty != imRuty2 )
	{
		CloseGantt ();
	}
	ogDataSet.SetAllDbarAndDear ( imRuty2>0 );
	CWnd *pCtrl = GetDlgItem( IDC_CONDITION_TXT );
	if ( pCtrl )
		pCtrl->EnableWindow ( !imRuty2 );
	pomConditionsGrid->EnableConditionsTable(!imRuty2);

	UpdateAllRudsStartEnd ();
	DecideTimeControls ();
	SetTimeValues ( omCurrRudUrno );
}
//---------------------------------------------------------------------------

// Everything that must be done every time the grids are re-initialized goes within this function
// All the one-time initialization is placed in IniLayoutForConditions ()
bool FlightIndepRulesView::InitializeCondTable()
{
	int ilSelection ;

	CCS_TRY
	
	// make wait cursor 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	//---- get current template name
    ilSelection = pomComboBox->GetCurSel();
      
	if(ilSelection >= 0)
	{
		// Grids f�llen mit Daten aus TPL  je nach Auswahl in der ComboBox der Toolbar
		CString olUrno;
		if (GetSelectedTplUrno(*pomComboBox, olUrno, TRUE))
		{  
			CString olTPLString = ogBCD.GetField("TPL", "URNO", olUrno, "FLDT");
			pomConditionsGrid->IniGridFromTplString(olTPLString, olUrno);

			if ( olTPLString.IsEmpty() )
			{	//  Wenn Template leer ist, keine bedingungsgesteuerten Regeln zulassen
				 bmTimeControlAllow;
				CheckDlgButton ( IDC_RADIO_RUTY2, bmTimeControlAllow );
				CheckDlgButton ( IDC_RADIO_RUTY3, FALSE );
				EnableDlgItem ( this, IDC_RADIO_RUTY3, FALSE, TRUE );
			}
			else
				EnableDlgItem ( this, IDC_RADIO_RUTY3, bmCondControlAllow, TRUE );
		}
	}
	// make regular cursor
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	CCS_CATCH_ALL		
	
	return (ilSelection >= 0);
}

//---------------------------------------------------------------------------
void FlightIndepRulesView::OnGridButton(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
	
	CString olTplUrno;
	if ( !GetSelectedTplUrno ( *pomComboBox, olTplUrno, TRUE, true ) )
		return;

	CGridFenster *polGrid = (CGridFenster*) wParam;
	CELLPOS *prlCellPos = (CELLPOS*) lParam;
	if ( !polGrid || ! prlCellPos || (polGrid !=pomConditionsGrid) )
		return;
	ROWCOL ilCol = (ROWCOL) prlCellPos->Col;
	ROWCOL ilRow = (ROWCOL) prlCellPos->Row;
	
	polGrid->OnConditionsGridButton ( olTplUrno, ilRow, ilCol );

	 CCS_CATCH_ALL
}

//---------------------------------------------------------------------------

CString FlightIndepRulesView::ConstructPrioString()
{
	CString olReturn;
	ROWCOL ilRow = 0;

	CCS_TRY

	ROWCOL ilRotCount = pomConditionsGrid->GetRowCount();
	for ( ilRow = 1; ilRow<=ilRotCount; ilRow++ )
	{
		if ( pomConditionsGrid->CheckIfLineEmpty(ilRow, 5, 2) )
			olReturn += "1";
		else
			olReturn += "0";
	}

	CCS_CATCH_ALL

	return olReturn;
}

//---------------------------------------------------------------------------

CString FlightIndepRulesView::ConstructEVRM() 
{
	CString olReturn;
	
	CCS_TRY

	// The field name strings are stored in the hidden column 7. 
	// We use SOH (ASCII 0x01) to separate parts of a condition and STX (ASCII 0x02) to separate several conditions

	CString olSimple;
	CString olStatic;
	CString olDynamic;
	CString olTmp;
	CString olRefStr;
	CString olBaseStr;

	int ilRotSize = pomConditionsGrid->GetRowCount();
	
//--- rotation grid
	for (int i = 0; i < ilRotSize; i++)
	{
		olRefStr = pomConditionsGrid->GetValueRowCol( i + 1, 8);
		olBaseStr = pomConditionsGrid->GetValueRowCol(i + 1, 7);
		
		olRefStr.Replace('.', SOH);				// replace dot by ASCII 0x01 ("SOH")
		olBaseStr.Replace('.', SOH);			// replace dot by ASCII 0x01 ("SOH")
		//--- simple values
		olTmp = pomConditionsGrid->GetValueRowCol(i + 1, 2);
		if (!olTmp.IsEmpty() && olTmp != CString(" "))
		{
			olSimple += olBaseStr;	// dot will be replaced by <SOH> further down
			olSimple += SOH;
			olSimple += CString("0");	// 0 = field from rotation grid (turnaround)
			olSimple += SOH;
			olSimple += CString(SIMPLE_VAL);	
			olSimple += SOH;
			olSimple += CString("=");	// operator (=, <>, <=, >=, IN)
			olSimple += SOH;
			olSimple += olTmp;			// 
			olSimple += SOH;
			olSimple += olRefStr;
			/*PRF6475:   dot must not be replaced in the complete string, but only in TABLE.FIELD 
			olSimple.Replace('.', SOH);		// replace dot by ASCII 0x01 ("SOH")	*/
			olSimple += STX;
		}
		
		// dynamic groups
		olTmp = pomConditionsGrid->GetValueRowCol(i + 1, 3);
		if (!olTmp.IsEmpty() && olTmp != CString(" "))
		{
			olDynamic += olBaseStr;
			olDynamic += SOH;
			olDynamic += CString("0");	// 0 = field from rotation grid (turnaround)
			olDynamic += SOH;
			olDynamic += CString(DYNAMIC_GRP);	
			olDynamic += SOH;
			olDynamic += CString("IN");
			olDynamic += SOH;
			olDynamic += olTmp;
			olDynamic += SOH;
			olDynamic += olRefStr;
			/*PRF6475:   dot must not be replaced in the complete string, but only in TABLE.FIELD 
			olDynamic.Replace('.', SOH);	// replace dot by ASCII 0x01 ("SOH")	*/
			olDynamic += STX;
		}

		// static groups
		olTmp = pomConditionsGrid->GetValueRowCol(i + 1, 5);
		if (!olTmp.IsEmpty() && olTmp != CString(" "))
		{
			olStatic += olBaseStr;
			olStatic += SOH;					// add SOH to end of string
			olStatic += CString("0");	// 0 = field from rotation grid
			olStatic += SOH;
			olStatic += CString(STATIC_GRP);	
			olStatic += SOH;
			olStatic += CString("IN");
			olStatic += SOH;
			olStatic += olTmp;
			olStatic += SOH;
			olStatic += olRefStr;
			/*PRF6475:   dot must not be replaced in the complete string, but only in TABLE.FIELD 
			olStatic.Replace('.', SOH);		// replace dot by ASCII 0x01 ("SOH")	*/
			olStatic += STX;
		}
	}

	olReturn = olSimple + olDynamic + olStatic;		// build return string
	olReturn = olReturn.Left(olReturn.GetLength() - 1);		// remove STX from end of string

	CCS_CATCH_ALL

	return olReturn;
}
//---------------------------------------------------------------------------

void FlightIndepRulesView::SetStaticTexts()
{
	SetWindowText(GetString(1363));

	CWnd *pWnd = GetDlgItem(INDEP_STA_SHORTNAME);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(219));

	pWnd = GetDlgItem(INDEP_STA_NAME);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(217));
	
	SetDlgItemLangText ( this, INDEP_CHB_DEACTI, IDS_DEAKTIVIERT );

	SetDlgItemLangText ( this, IDC_RADIO_RUTY2, IDS_RADIO_RUTY2 );
	SetDlgItemLangText ( this, IDC_RADIO_RUTY3, IDS_RADIO_RUTY3 );
	SetDlgItemLangText ( this, IDC_CONDITION_TXT, IDS_CONDITIONS );
	SetDlgItemLangText ( this, INDEP_FRA_EMPL, IDS_EINSATZDATEN );

	pWnd = GetDlgItem(INDEP_STA_VRGC);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(200));

	pWnd = GetDlgItem(INDEP_STA_PERM);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(201));

	SetDlgItemLangText ( this, INDEP_STA_DUBE, IDS_START_EINSATZ );
	SetDlgItemLangText ( this, INDEP_STA_DUEN, IDS_ENDET );
	SetDlgItemLangText ( this, INDEP_STA_REMA, IDS_BESCHREIBUNG );
	
	SetDlgItemLangText ( this, TEMPL_STA_ALOD, IDS_ZUORDNUNGSEINHEIT );
	SetDlgItemLangText ( this, IDC_RADIO_ACTIVITIES, IDS_ACTIVITIES );
	SetDlgItemLangText ( this, IDC_RADIO_SINGLE_DUTY, IDS_SINGLE_DUTIES );

	SetDlgItemLangText ( this, IDC_FLOATING_CHK, IDS_FLOATING );
	SetDlgItemLangText ( this, IDC_EARLIEST_TXT, IDS_EARLIEST );
	SetDlgItemLangText ( this, IDC_LATEST_TXT, IDS_LATEST );

	SetDlgItemLangText ( this, IDC_DIDE_CHK, IDS_TEILBAR );
	SetDlgItemLangText ( this, IDC_STA_MINTIME, IDS_SPLITMIN );
	SetDlgItemLangText ( this, IDC_STA_MAXTIME, IDS_SPLITMAX );
	
	SetDlgItemLangText ( this, IDC_FADD_CHK, IDS_NO_DEMANDS );
}
//---------------------------------------------------------------------------

void FlightIndepRulesView::OnRadioActivities() 
{
	UpdateData();
	CloseGantt();
	omCurrRudUrno = omCurrDgrUrno = "";
	pomDutyGrid->SetExpanded(biDisplaySingleDuties == 1);
}
//----------------------------------------------------------------------------------------------------------------------

int FlightIndepRulesView::GetResRecordsToDisplay(CString opTable, CCSPtrArray <RecordSet> *popRecords)
{
	CCSPtrArray <RecordSet> olRudRecs;
	RecordSet olResRec;
	CString olRudUrno;
	int i;
	
	if (pomGantt || pomDutyGrid->IsExpanded())	// function for one duty
	{
		ogBCD.GetRecords(opTable, "URUD", omCurrRudUrno, popRecords);
	}
	else
	{
		// alle RUD_TMP-S�tze dieser Demandgroup holen
		ogBCD.GetRecords("RUD_TMP", "UDGR", omCurrDgrUrno, &olRudRecs );
		for (i = 0; i < olRudRecs.GetSize(); i++)
		{
			olRudUrno = olRudRecs[i].Values[igRudUrnoIdx];
			if (!olRudUrno.IsEmpty() && ogBCD.GetRecord(opTable, "URUD", olRudUrno, olResRec))
			{
				popRecords->New(olResRec);
			}
		}
		olRudRecs.DeleteAll();
	}
	return popRecords->GetSize();
}
//----------------------------------------------------------------------------------------------------------------------

void FlightIndepRulesView::OnGridCurrCellChanged(WPARAM wParam, LPARAM lParam)
{
	CGridFenster *polGrid = (CGridFenster*) wParam;
	
	CELLPOS *prlCellPos = (CELLPOS*) lParam;
	ROWCOL ilCol = (ROWCOL) prlCellPos->Col;
	ROWCOL ilRow = (ROWCOL) prlCellPos->Row;
	int    ilMarkInLeftGrid=-1;
	int    ilMarkInRightGrid=-1;

	//  aktive Zelle im Duty-Grid hat sich ge�ndert
	bool blDutySelected;
	CString olNewRudUrno ;
		
	if (polGrid == pomDutyGrid)
	{
		if (ilRow != imActiveRow)
		{
			imActiveRow = ilRow;
			CString olNewDgrUrno ;
			blDutySelected = pomDutyGrid->GetSelectionInfo (olNewDgrUrno, olNewRudUrno);
			EnableDemandDetailControls(blDutySelected);

			if (blDutySelected) 
			{	
				if ((olNewDgrUrno != omCurrDgrUrno) || (olNewRudUrno != omCurrRudUrno)) 
				{
					omCurrDgrUrno = olNewDgrUrno;
					SetCurrentRudUrno(olNewRudUrno);
					FillDemandDetailControls();
				}
				if (imRessourceType)
				{
					ilMarkInRightGrid = 1;
				}
				else
				{
					ilMarkInLeftGrid = 1;
				}
			}
			else
			{
				ClearRessourceTables();
			}

			pomLeftGrid->SelectRowHeader(ilMarkInLeftGrid); 
			pomRightGrid->SelectRowHeader(ilMarkInRightGrid); 
			pomDutyGrid->SelectRowHeader(ilRow ? ilRow : -1);
		}
	}
	if ((polGrid == pomLeftGrid) && (imRessourceType == 0))
	{
		if (ilRow > 0)
		{
			CString olRpfUrno;
			olRpfUrno = pomLeftGrid->GetValueRowCol(ilRow, 3);
			if (olRpfUrno.IsEmpty())
			{
				olNewRudUrno.Empty();
			}
			else
			{
				olNewRudUrno = ogBCD.GetField("RPF_TMP", "URNO", olRpfUrno, "URUD");
			}
			
			if (olNewRudUrno != omCurrRudUrno)
			{
				DisplayQualisForRud (olNewRudUrno);
			}
			SetCurrentRudUrno(olNewRudUrno);
			ilMarkInLeftGrid = ilRow;
		}
		pomLeftGrid->SelectRowHeader(ilMarkInLeftGrid); 

	}
	
	if ( (polGrid == pomRightGrid) &&
		 ( (imRessourceType==1) || (imRessourceType==2) )
	   )
	{
		CString olResUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
		CString olResTable = imRessourceType == 2 ? "RLO_TMP" : "REQ_TMP";
		if (olResUrno.IsEmpty())
		{
			SetCurrentRudUrno("");
		}
		else
		{
			SetCurrentRudUrno(ogBCD.GetField( olResTable, "URNO", olResUrno, "URUD"));
		}
		pomRightGrid->SelectRowHeader(omCurrRudUrno.IsEmpty() ? -1 : ilRow) ;

	}	
}
//----------------------------------------------------------------------------------------------------------------------

void FlightIndepRulesView::DisplayQualisForRud ( CString &ropRudUrno )
{
	CCSPtrArray <RecordSet> olRightArr;
	int						ilRows, ilCols, i;
	BOOL					ilOk;
	RecordSet				olRecord;
	CString					olName, olCode, olUrno, olUdgr ;

	pomRightGrid->LockUpdate(TRUE);
	
	ResetResourceTable ( pomRightGrid );
	if ( ropRudUrno.IsEmpty() )
	{
		pomRightGrid->LockUpdate(FALSE);
		return;
	}
	
	ogBCD.GetRecords("RPQ_TMP", "URUD", ropRudUrno, &olRightArr);
	int ilRightCount = olRightArr.GetSize();

	//- fill cells with data
	for (i = 0; i < ilRightCount; i++)
	{
		//pomRightGrid->DoAutoGrow(i, 1);
		olRecord = olRightArr[i];

		olUrno = olRecord.Values[igRpqUrnoIdx];
		if ( GetGroupName ( &olRecord, igRpqGtabIdx, igRpqUperIdx, olCode ) )	//hag990827
		{
			ogBCD.GetField("SGR", "GRPN", olCode, "GRDS", olName);				//hag990827
			olCode = "*"+ olCode;
		}
		else
		{
			olCode = olRecord.Values[igRpqQucoIdx];
			ogBCD.GetField("PER", "PRMC", olCode, "PRMN", olName);
		}
		
		pomRightGrid->SetValueRange(CGXRange(i + 1, 1), olCode);
		pomRightGrid->SetValueRange(CGXRange(i + 1, 2), olName);
		pomRightGrid->SetValueRange(CGXRange(i + 1, 3), olUrno);
		pomRightGrid->DoAutoGrow(i+1, 1);
	}

	//  Zus�tzlich nicht zugeordnete Qualifikationen dieser Demandgroup anzeigen
	ilCols = pomRightGrid->GetColCount ();
	if ( ogBCD.GetField("RUD_TMP", "URNO", ropRudUrno, "UDGR", olUdgr ) &&
		 !olUdgr.IsEmpty() )
	{
		ogBCD.GetRecords("RPQ_TMP", "UDGR", olUdgr, &olRightArr);
		
		for (i = 0; i < olRightArr.GetSize(); i++)
		{
			olRecord = olRightArr[i];
	
			olUrno = olRecord.Values[igRpqUrnoIdx];
			if ( GetGroupName ( &olRecord, igRpqGtabIdx, igRpqUperIdx, olCode ) )	
			{
				ogBCD.GetField("SGR", "GRPN", olCode, "GRDS", olName);				
				olCode = "*"+ olCode;
			}
			else
			{
				olCode = olRecord.Values[igRpqQucoIdx];
				ogBCD.GetField("PER", "PRMC", olCode, "PRMN", olName);
			}
			ilRows = ilRightCount + i + 1;
			ilOk = pomRightGrid->SetValueRange(CGXRange(ilRows, 1), olCode);
			pomRightGrid->SetValueRange(CGXRange(ilRows, 2), olName);
			pomRightGrid->SetValueRange(CGXRange(ilRows, 3), olUrno);
			pomRightGrid->DoAutoGrow(ilRows, 1);				//hag990831
			pomRightGrid->SetStyleRange(CGXRange(ilRows, 1, ilRows, 2 ), CGXStyle().SetInterior(ogColors[PYELLOW_IDX]));
		}
		
	}
	pomRightGrid->SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetEnabled(FALSE) );
	pomRightGrid->Redraw();
	pomRightGrid->LockUpdate(FALSE);
	olRightArr.DeleteAll();
}
//----------------------------------------------------------------------------------------------------------------------

//  Grid popGrid leeren, d.h. es bleibt nur eine leere Zeile �brig
void FlightIndepRulesView::ResetResourceTable ( CGridFenster *popGrid )
{
	popGrid->LockUpdate(TRUE);
	ROWCOL ilRows = popGrid->GetRowCount();
	ROWCOL ilCols = popGrid->GetColCount();
	if ( ilRows >= 2 )
		popGrid->RemoveRows ( 2, ilRows );
	if ( ( ilRows >= 1 ) && ( ilCols >= 1 ) )
	{
		popGrid->SetStyleRange(CGXRange(1,1,1,ilCols),
							   CGXStyle().SetInterior(ogColors[WHITE_IDX])
										 .SetValue(""));

	}
	popGrid->LockUpdate(FALSE);
}	
//----------------------------------------------------------------------------------------------------------------------

void FlightIndepRulesView::IniTimesGrid ()
{
	pomTimesGrid->SubclassDlgItem(DEMDETAIL_CUS_TIMES, this);
	pomTimesGrid->Initialize();

//--- times grid (left side)
	pomTimesGrid->LockUpdate(TRUE);
	pomTimesGrid->SetColCount(3);
	pomTimesGrid->SetRowCount(5); 

	pomTimesGrid->GetParam()->SetNumberedColHeaders(FALSE);                 
	pomTimesGrid->GetParam()->SetNumberedRowHeaders(FALSE); 

	pomTimesGrid->GetParam()->EnableSelection(GX_SELNONE );	
	pomTimesGrid->GetParam()->EnableTrackRowHeight(0);
	pomTimesGrid->GetParam()->EnableTrackColWidth(0);
	pomTimesGrid->SetSortingEnabled(false);
	
	pomTimesGrid->SetValueRange(CGXRange(0,1), GetString(IDS_ZEITEN) );
	pomTimesGrid->SetValueRange(CGXRange(0,2), CString("[min]"));
	pomTimesGrid->SetValueRange(CGXRange(1,1), GetString(1469));	// DEDU
	pomTimesGrid->SetValueRange(CGXRange(1,3), CString("RUD.DEDU"));
	pomTimesGrid->SetValueRange(CGXRange(2,1), GetString(1470));	// SUTI
	pomTimesGrid->SetValueRange(CGXRange(2,3), CString("RUD.SUTI"));
	pomTimesGrid->SetValueRange(CGXRange(3,1), GetString(1471));	// SDTI
	pomTimesGrid->SetValueRange(CGXRange(3,3), CString("RUD.SDTI"));
	pomTimesGrid->SetValueRange(CGXRange(4,1), GetString(1472));	// TTGT
	pomTimesGrid->SetValueRange(CGXRange(4,3), CString("RUD.TTGT"));
	pomTimesGrid->SetValueRange(CGXRange(5,1), GetString(1473));	// TTGF
	pomTimesGrid->SetValueRange(CGXRange(5,3), CString("RUD.TTGF"));

	pomTimesGrid->SetStyleRange(CGXRange(0,0,pomTimesGrid->GetRowCount(), 1), 
									CGXStyle().SetEnabled(FALSE)
									          .SetReadOnly(TRUE));
	//  Spaltenbreiten setzen																							
	CRect olRect;
	pomTimesGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	pomTimesGrid->SetColWidth(0,0,20);	// header
	int ilWidthRest = olRect.Width()- 20;
	pomTimesGrid->SetColWidth(1,1,ilWidthRest*2/3);	// name
	pomTimesGrid->SetColWidth(2,2,ilWidthRest/3);	// value

	pomTimesGrid->HideCols(3,3);		// TAB.FLDN (eg. "RUD.DEDU")
	pomTimesGrid->SetStyleRange ( CGXRange(1,2), CGXStyle().SetMaxLength(7) );
	pomTimesGrid->SetStyleRange ( CGXRange(2,2,5,2), CGXStyle().SetMaxLength(3) );

	pomTimesGrid->LockUpdate(FALSE);
	pomTimesGrid->Redraw();
}
//----------------------------------------------------------------------------------------------------------------------

void FlightIndepRulesView::SetTimeValues ( CString &ropRudUrno )
{
	CCS_TRY

		
	//--- get current rud record
	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD_TMP"));

	//--- set name and shortname 
	int ilValue;
	CString olValue;
	
	pomTimesGrid->LockUpdate(TRUE);

	pomTimesGrid->SetStyleRange(CGXRange().SetRows(1),
								CGXStyle().SetReadOnly(FALSE) );
	if ( !ropRudUrno.IsEmpty () )
	{
		bool blTest = ogBCD.GetRecord("RUD_TMP", "URNO", ropRudUrno, olRudRecord);

		if (blTest) // mne 001110
		{
			if (!pomDutyGrid->IsExpanded() && !pomGantt)
			{
				CString olCurrDgrUrno, olCurrRudUrno;
				bool blDutySelected = pomDutyGrid->GetSelectionInfo(olCurrDgrUrno, olCurrRudUrno);

				if (blDutySelected)
				{
					CCSPtrArray <RecordSet> olRudArr;
					ogBCD.GetRecords("RUD_TMP", "UDGR", olCurrDgrUrno, &olRudArr);
			
					// DEDU (demand duration)
					pomTimesGrid->SetValueRange(CGXRange(1,2), CheckValuesDifferent(olRudArr,igRudDeduIdx));
					
					// SUTI (setup time)
					pomTimesGrid->SetValueRange(CGXRange(2,2), CheckValuesDifferent(olRudArr,igRudSutiIdx));
					
					// SDTI (setdown time)
					pomTimesGrid->SetValueRange(CGXRange(3,2), CheckValuesDifferent(olRudArr,igRudSdtiIdx));

					// TTGT (time to go to)
					pomTimesGrid->SetValueRange(CGXRange(4,2), CheckValuesDifferent(olRudArr,igRudTtgtIdx));

					// TTGF (time to go from)
					pomTimesGrid->SetValueRange(CGXRange(5,2), CheckValuesDifferent(olRudArr,igRudTtgfIdx));
			
					if ( omOffsetEdit.m_hWnd )
					{
						omOffsetEdit.SetWindowText(CheckValuesDifferent(olRudArr,igRudTsdbIdx));
					}
					olRudArr.DeleteAll();
				}
			}
			else
			{
				//  Umwandlung von Sec. wieder in Min.
				// DEDU (demand duration)
				ilValue = atoi(olRudRecord.Values[igRudDeduIdx]) / 60;
				olValue.Format("%d", ilValue);
				pomTimesGrid->SetValueRange(CGXRange(1,2), olValue);
				
				// SUTI (setup time)
				ilValue = atoi(olRudRecord.Values[igRudSutiIdx]) / 60;
				olValue.Format("%d", ilValue);
				pomTimesGrid->SetValueRange(CGXRange(2,2), olValue);
				
				// SDTI (setdown time)
				ilValue = atoi(olRudRecord.Values[igRudSdtiIdx]) / 60;
				olValue.Format("%d", ilValue);
				pomTimesGrid->SetValueRange(CGXRange(3,2), olValue);

				// TTGT (time to go to)
				ilValue = atoi(olRudRecord.Values[igRudTtgtIdx]) / 60;
				olValue.Format("%d", ilValue);
				pomTimesGrid->SetValueRange(CGXRange(4,2), olValue);

				// TTGF (time to go from)
				ilValue = atoi(olRudRecord.Values[igRudTtgfIdx]) / 60;
				olValue.Format("%d", ilValue);
				pomTimesGrid->SetValueRange(CGXRange(5,2), olValue);

				//  Umwandlung von Sec. wieder in Min.
				if ( omOffsetEdit.m_hWnd )
				{
					ilValue = atoi(olRudRecord.Values[igRudTsdbIdx]) / 60;
					olValue.Format ( "%d", ilValue);
					omOffsetEdit.SetWindowText(olValue);
				}
			}
		}
	}
	else
	{
		pomTimesGrid->SetValueRange(CGXRange(1,2,5,2), "" );
		if ( omOffsetEdit.m_hWnd )
			omOffsetEdit.SetWindowText(olValue);
	}	
	pomTimesGrid->LockUpdate(FALSE);
	pomTimesGrid->Redraw();


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------


long FlightIndepRulesView::CalcAndDisplayDuration(CString opDebe,CString opDeen,CString opDedu, CString opDbfl,CString opDefl)
{
	CString	olDiffStr;
	long llDiffSecs ;
/*
	CTime olBeginTime = TIMENULL;
	CTime olEndTime = TIMENULL;
	
	olBeginTime = DBStringToDateTime(opDebe);
	olEndTime = DBStringToDateTime(opDeen);
	while(olBeginTime > olEndTime)
	{
		olEndTime += CTimeSpan(1,0,0,0);
	}

	llDiffSecs = (olEndTime - olBeginTime).GetTotalSeconds();

	if ((opDbfl != "1") && (opDefl != "1"))
	{		
		
		while (llDiffSecs > 86400) // no duties > 24 h
			llDiffSecs -= 86400;
		pomTimesGrid->SetStyleRange(CGXRange().SetRows(1),
									CGXStyle().SetReadOnly(FALSE) );
		olDiffStr.Format("%ld", llDiffSecs/60);
		pomTimesGrid->SetValueRange(CGXRange(1,2), olDiffStr);

	}
	else
	{
		// duration for floating demands can't be longer than 
		// difference of times given
		long llDuration = atol(opDedu);
		if (llDiffSecs <= llDuration)
		{
			llDiffSecs = -1;
		}
	}*/
	llDiffSecs = CalcDutyDuration( opDebe,opDeen,opDedu, opDbfl,opDefl);
	if ((opDbfl != "1") && (opDefl != "1")&&(llDiffSecs>=0))
	{		
		pomTimesGrid->SetStyleRange(CGXRange().SetRows(1),
									CGXStyle().SetReadOnly(FALSE) );
		olDiffStr.Format("%ld", llDiffSecs/60);
		pomTimesGrid->SetValueRange(CGXRange(1,2), olDiffStr);

	}
	return llDiffSecs;
}
//---------------------------------------------------------------------------

void FlightIndepRulesView::OnSelendokCobAlod() 
{
	// TODO: Add your control notification handler code here
	int		ilSel = m_AlocCB.GetCurSel();
	long	llAloUrno = m_AlocCB.GetItemData(ilSel);
	CString	olAloUrno, olAloCode;
	CCSPtrArray <RecordSet> olRudArr;
	CString olCurrDgrUrno, olCurrRudUrno;

	if ( !omCurrRudUrno.IsEmpty() )
	{
		if (llAloUrno != CB_ERR) 
		{
			olAloUrno.Format("%d", llAloUrno);
			olAloCode = ogBCD.GetField ( "ALO", "URNO", olAloUrno, "ALOC" );
		}
		else
			olAloCode.Empty();
		//  ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "ALOC", olAloCode );
		
		GetSelectedRuds ( olRudArr );
		for ( int i=0; i<olRudArr.GetSize(); i++)
			ogBCD.SetField( "RUD_TMP", "URNO", olRudArr[i].Values[igRudUrnoIdx], 
							"ALOC", olAloCode );
		olRudArr.DeleteAll ();

	}
}
//---------------------------------------------------------------------------

//	Set new Rud-Urno
//	Update all displays for current RUD
void FlightIndepRulesView::SetCurrentRudUrno ( CString opRudUrno )
{
	if ( omCurrRudUrno == opRudUrno )
		return ;		//  no change of urno, return
	omCurrRudUrno = opRudUrno;
	DemandDetailDlg::SelectAloc ( &m_AlocCB, omCurrRudUrno );
	SetTimeValues ( omCurrRudUrno );
}
//---------------------------------------------------------------------------

void FlightIndepRulesView::OnGridRButton(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

		
	CGridFenster*polGrid = (CGridFenster*) wParam;
	CELLPOS		*prlCellPos = (CELLPOS*) lParam;
	if ( !polGrid || ! prlCellPos || (polGrid !=pomRightGrid)  )
		return;
	ROWCOL ilCol = (ROWCOL) prlCellPos->Col;
	ROWCOL ilRow = (ROWCOL) prlCellPos->Row;
	if ( /*bmExpanded &&*/ !omCurrRudUrno.IsEmpty() && !imRessourceType )
	{
		CString		olUrno, olUdgr, olUrud;
		RecordSet	olRecord;

		olUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
		if ( !olUrno.IsEmpty () &&	
			 ogBCD.GetRecord ( "RPQ_TMP", "URNO", olUrno, olRecord ) )
		{
			olUrud = olRecord.Values[igRpqUrudIdx];
			olUdgr = olRecord.Values[igRpqUdgrIdx];
			if ( olUrud.IsEmpty () )	//  Gruppenqualifikation wird zugeordnet
			{
				olRecord.Values[igRpqUrudIdx] = omCurrRudUrno;
				olRecord.Values[igRpqUdgrIdx] = "";
			}
			else			//  Einzelqualifikation wird Gruppenqualifikation 
			{
				olRecord.Values[igRpqUrudIdx] = "";
				olRecord.Values[igRpqUdgrIdx] = omCurrDgrUrno;
			}
			ogBCD.SetRecord ( "RPQ_TMP", "URNO", olUrno, olRecord.Values ) ;
			DisplayQualisForRud ( omCurrRudUrno );
		}
	}

	
	CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------------

void FlightIndepRulesView::OnFloatingChk() 
{
	RecordSet olRecord;
	bmIsFloating = IsDlgButtonChecked(IDC_FLOATING_CHK);
	if ( bmIsFloating == 2 )
		bmIsFloating = 0;
		
	if(	imRuty2 && pomDutyGrid->IsExpanded() && !omCurrRudUrno.IsEmpty() &&
		ogBCD.GetRecord ( "RUD_TMP", "URNO", omCurrRudUrno, olRecord ) )
	{
		ogDataSet.ChangeRudStartEnd ( olRecord, bmIsFloating );
		ogBCD.SetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord.Values);
		DecideTimeControls ();
		OnDatetimechangeDuen();
	}
	else
		if ( /*!imRuty2 && */ !omCurrDgrUrno.IsEmpty () )
		{	//  bei Sammeldarstellung: f�r alle RUD's aus der aktuellen
			//  Demandgroup flags "DBFL" und "DEFL" setzen
			ogDataSet.SetDgrFloating ( "RUD_TMP", omCurrDgrUrno, bmIsFloating );
			DecideTimeControls ();
		}
}
//------------------------------------------------------------------------------------------------------------------------

void FlightIndepRulesView::DecideTimeControls ()
{
	CPoint olPos;
	BOOL blEnableFloating = FALSE;
	BOOL blShowTimes = FALSE;
	// BOOL blEnableTimes = FALSE;
	BOOL blEnableTimes = TRUE;
	//BOOL blIsFloating = FALSE;
	BOOL blShowFloating = /*FALSE*/ TRUE;
	CString	olDbfl, olDefl, olValue;
	BOOL blEnableEarliest = FALSE;
	int  ilFloatingState=2, ilNoDemands = 2;
	CCSPtrArray <RecordSet> olRudRecs;
	bool blEqual = false;
	

	if ( !imRuty2 && pomDutyGrid->IsExpanded() )
		blEnableFloating = false;
	else
		blEnableFloating = true;
	if ( blEnableFloating )
	{
		if ( pomDutyGrid->IsExpanded() && !omCurrRudUrno.IsEmpty() )
			ogBCD.GetRecords("RUD_TMP", "URNO", omCurrRudUrno, &olRudRecs );
		if ( !pomDutyGrid->IsExpanded() && !omCurrDgrUrno.IsEmpty() )
			ogBCD.GetRecords("RUD_TMP", "UDGR", omCurrDgrUrno, &olRudRecs );
		
		ilFloatingState = GetFloatingState(olRudRecs );
		olRudRecs.DeleteAll();
	}

	if(	imRuty2 )
		blShowTimes = TRUE;

	blEnableTimes = ( ilFloatingState <2 ) ;
		
		
	EnableDlgItem ( this, INDEP_DTC_DUEN, blEnableTimes, blShowTimes );
	EnableDlgItem ( this, INDEP_DTC_DUBE, blEnableTimes, blShowTimes );

	EnableDlgItem ( this, INDEP_STA_DUBE, TRUE, blShowTimes );
	EnableDlgItem ( this, INDEP_STA_DUEN, TRUE, blShowTimes );

	EnableDlgItem ( this, IDC_FLOATING_CHK, blEnableFloating, blShowFloating );
	if ( imRuty2 )
		blEnableEarliest = (ilFloatingState==1)&&blShowTimes;
	else

		blEnableEarliest = (ilFloatingState==1) && pomGantt;
	EnableDlgItem ( this, IDC_EARLIEST_TXT, TRUE, blEnableEarliest );

	EnableDlgItem ( this, IDC_LATEST_TXT, TRUE, (ilFloatingState==1)&&blShowTimes );
	if ( blShowFloating )
		CheckDlgButton ( IDC_FLOATING_CHK, ilFloatingState );
	bool blEnableDutyTime = !imRuty2 || (ilFloatingState==1);

	if ( imRuty2 && pomDutyGrid->IsExpanded() && !omCurrRudUrno.IsEmpty() )
		ogBCD.GetField ( "RUD_TMP", "URNO", omCurrRudUrno, "FADD", olValue ) ;
	else
		ogDataSet.GetFieldForDgr ( "RUD_TMP", omCurrDgrUrno, "FADD", olValue, blEqual ) ;
	if ( olValue == "1" )
		ilNoDemands = 0;
	else if ( olValue == "0" )
		ilNoDemands = 1;
	CheckDlgButton ( IDC_FADD_CHK, ilNoDemands );

	bmSetDebe = true;
	bmSetDeen = true;

	// mne 010316, hag010605
	if (blShowTimes && !pomDutyGrid->IsExpanded()) 
	{
		bool blStart = true, blEnd = true;
		if (IsSameTimeInDgr(blStart, blEnd))
		{
			CTime olTmpTime = TIMENULL;
			if (!blStart)
				ClearDlgItem (this, INDEP_DTC_DUBE);
		/*		EnableDlgItem(this,INDEP_DTC_DUBE,false,false);*/
			if (!blEnd)
/*				EnableDlgItem(this,INDEP_DTC_DUEN,false,false);*/
				ClearDlgItem (this, INDEP_DTC_DUEN);
				
			bmSetDebe = blStart;
			bmSetDeen = blEnd;
		}
	}
	// mne 010316

	pomTimesGrid->SetStyleRange(CGXRange().SetRows(1),
								CGXStyle().SetEnabled(blEnableDutyTime)
										.SetReadOnly(!blEnableDutyTime)
										.SetInterior(!blEnableDutyTime ? SILVER : WHITE));
	
	olPos = imRuty2 ? omFloatingPosRuty2 : omFloatingPosRuty3;
	ClientToView ( &olPos ); 
	MoveDlgItem ( this, IDC_FLOATING_CHK, olPos.x, olPos.y );

	if (::IsWindow(omOffsetEdit.GetSafeHwnd()) )
		omOffsetEdit.EnableWindow ( pomGantt && !imRuty2 && 
									!omCurrRudUrno.IsEmpty() );
	
	int ilSplitState = omDideChk.GetCheck(); 
	BOOL blEnable = !omCurrRudUrno.IsEmpty();
	
	omDideChk.EnableWindow(blEnable);
	omEditSplitMax.EnableWindow(blEnable && (ilSplitState== CHECKED) );
	omEditSplitMin.EnableWindow(blEnable && (ilSplitState== CHECKED) );

}	
//------------------------------------------------------------------------------------------------------------------------

bool FlightIndepRulesView::SetTimeControlsForDgr ( CString &ropDgrUrno )
{
	CCSPtrArray <RecordSet> olRudRecs;
//	CString olEarliestStart, olLatestEnd;
	CString olStart, olEnd;
//	BOOL    blFloating;
	CTime	olTimeStart, olTimeEnd;
	int ilFloatingState;

	ogBCD.GetRecords("RUD_TMP", "UDGR", omCurrDgrUrno, &olRudRecs );
	ilFloatingState = GetFloatingState(olRudRecs);
	if ( ilFloatingState == 0 ) 
	{
		 IsSameTimeValue(olRudRecs, igRudDebeIdx, olStart );
		 IsSameTimeValue(olRudRecs, igRudDeenIdx, olEnd );
	}
	if ( ilFloatingState == 1 ) 
	{
		 IsSameTimeValue(olRudRecs, igRudEadbIdx, olStart );
		 IsSameTimeValue(olRudRecs, igRudLadeIdx, olEnd );
	}

	/*
	for ( int i=0; i<olRudRecs.GetSize(); i++ )
	{
		ogDataSet.GetRudStartTime ( olRudRecs[i], blFloating, olStart );
		ogDataSet.GetRudEndTime ( olRudRecs[i], blFloating, olEnd );
		if ( olEarliestStart.IsEmpty() || 
			 ( !olStart.IsEmpty() && (olStart<olEarliestStart) ) )
			olEarliestStart = olStart;
		if ( olLatestEnd.IsEmpty() || 
			 ( !olEnd.IsEmpty() && (olEnd>olLatestEnd) ) )
			olLatestEnd = olEnd;
	}
	olRudRecs.DeleteAll();
	olTimeStart = DBStringToDateTime(olEarliestStart);
	olTimeEnd = DBStringToDateTime(olLatestEnd);
	if ( ( olTimeStart !=TIMENULL ) && ( olTimeEnd !=TIMENULL ) )
	{
		if(bmSetDebe)
		{
			m_Dtc_Dube.SetWindowText(olTimeStart.Format("%H:%M"));
			m_Dtc_Dube.GetStatus();
		}
		if(bmSetDeen)
		{
			m_Dtc_Duen.SetWindowText(olTimeEnd.Format("%H:%M"));
			m_Dtc_Duen.GetStatus();
		}
		return true;
	}
	else 
		return false;*/
	
	olRudRecs.DeleteAll();
	if(bmSetDebe)
	{
		m_Dtc_Dube.SetInitText(olStart, true);
		m_Dtc_Dube.GetStatus();
	}
	if(bmSetDeen)
	{
		m_Dtc_Duen.SetInitText(olEnd, true);
		m_Dtc_Duen.GetStatus();
	}
	return !(olStart.IsEmpty() || olEnd.IsEmpty() );
}
//------------------------------------------------------------------------------------------------------------------------

//  Falsch oder nicht gesetzte Anfangs- und Endzeiten auf g�ltige Werte setzen
//  return:  true, wenn Anpassung vorgenommen werden mu�te
//			 false, wenn alles OK war
bool FlightIndepRulesView::UpdateRudStartEnd ( RecordSet &ropRudRecord )
{
	CString olDedu = ropRudRecord.Values[igRudDeduIdx];
	CString olDBEnd, olDBStart;
	BOOL blStartFloating, blEndFloating;
	CTime	olStartTime, olEndTime;
	CTimeSpan olDiffTime;
	long  ilDedu;

	if ( sscanf ( olDedu, "%ld", &ilDedu ) <= 0 )
	{
		ogLog.Trace ( "[FlightIndepRulesView::UpdateRudStartEnd]: Unable to load duty time for RUD=%s",
					  ropRudRecord.Values[igRudUrnoIdx] );
		return false;	//  konnte 
	}
	if ( !imRuty2 )    //  Start- und Endzeiten bei bedingungsgesteuerten Regeln egal
		return false;
	CTimeSpan olDutyTime( 0, 0, 0, ilDedu );
	bool blChanged=false;

	ogDataSet.GetRudStartTime ( ropRudRecord, blStartFloating, olDBStart );
	ogDataSet.GetRudEndTime ( ropRudRecord, blEndFloating, olDBEnd );
	olStartTime = DBStringToDateTime ( olDBStart );
	olEndTime = DBStringToDateTime ( olDBEnd );
	if ( (olStartTime!=TIMENULL) && (olEndTime!=TIMENULL) )
	{
		//  beide Zeiten g�ltig -> Testen, ob Zeitspanne gro� genug
		olDiffTime = olEndTime - olStartTime;
		if ( olDiffTime < olDutyTime )
		{	//  Zeitspanne zu klein
			olEndTime = olStartTime+olDutyTime;
			blChanged = true;
		}
	}
	else
	{
		if ( (olStartTime==TIMENULL) && (olEndTime==TIMENULL) )
		{	//  beide Zeiten sind in der Datenbank ung�ltig 
			olStartTime = CTime::GetCurrentTime();
			olEndTime = olStartTime+olDutyTime;
		}
		else
			if ( olEndTime==TIMENULL )	//  nur Endzeit ung�ltig
				olEndTime = olStartTime+olDutyTime;
			else						//  nur Startzeit ung�ltig
				olStartTime = olEndTime-olDutyTime;
		blChanged = true;
	}
	if ( blChanged )
	{
		olDBStart = CTimeToDBString(olStartTime,olStartTime);
		olDBEnd = CTimeToDBString(olEndTime,olEndTime);
		ogDataSet.SetRudStartEnd ( ropRudRecord, blStartFloating||blEndFloating, 
								   olDBStart, olDBEnd );
	}
	return blChanged;
}
//------------------------------------------------------------------------------------------------------------------------

void FlightIndepRulesView::UpdateAllRudsStartEnd ()
{
	if ( !imRuty2 )    //  Start- und Endzeiten bei bedingungsgesteuerten Regeln egal
		return ;
	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD_TMP"));
	int ilRudCount = ogBCD.GetDataCount("RUD_TMP");
	
	for (int i = 0; i < ilRudCount; i++)
	{
		if ( ogBCD.GetRecord("RUD_TMP", i, olRudRecord) &&
			 UpdateRudStartEnd ( olRudRecord ) )
			ogBCD.SetRecord("RUD_TMP", "URNO", olRudRecord.Values[igRudUrnoIdx],
						    olRudRecord.Values);
	}
}
//------------------------------------------------------------------------------------------------------------------------

void FlightIndepRulesView::OnLButtonDblClk(UINT itemID, LPARAM lParam)
{
	CPoint point(lParam);
	
	if ( !omCurrDgrUrno.IsEmpty() && !pomDutyGrid->IsExpanded() && !imRuty2 )
	{
		if ( !pomGantt )
			OpenGantt (omCurrDgrUrno);
		else
			CloseGantt ();
	}
}
//------------------------------------------------------------------------------------------------------------------------

bool FlightIndepRulesView::OpenGantt (CString &ropUdgr)
{
	//  Nur noch aktuelle Demandgroup in 1. Zeile des Dutygrid anzeigen
	pomDutyGrid->ResetContent();
	if ( !pomDutyGrid->DisplayDemandGroup ( ropUdgr ) )
	{
		pomDutyGrid->DisplayRudTmp ();
		return false;
	}

		//--- initialize gantt
	MakeTimeScale();
	MakeRelativeTimeScale();
	pomRelTimeScale->SetRuleType(1);
	pomRelTimeScale->SetReferenceTexts("ONBL", "" );
	MakeViewer();
	((CIndepViewer*)pomViewer)->SetCurrentUdgr(ropUdgr);
	MakeGantt();
	pomGantt->EnableEditRud (FALSE);

	m_HScroll.ShowWindow ( SW_SHOW );
	m_HScroll.SetScrollRange(0, 1000, FALSE);
	
	CRect	olRect;
	CPoint	olPos;
	pomGantt->GetWindowRect(olRect);
	ScreenToClient(olRect);

	pomDutyGrid->HideRows(0, 0, TRUE );

	ResizeDlgItem ( this, IDC_DUTY_LIST, olRect.Width(), 
					pomDutyGrid->GetRowHeight(1)+5 );
	pomDutyGrid->Redraw();
	
	pomGantt->SetScrollPos(SB_VERT, 0, TRUE);
	m_HScroll.SetScrollPos(SB_HORZ, TRUE);

	olPos.x = olRect.left;

	//--- hide absolute timescale, show relative one
	pomTimeScale->ShowWindow(SW_HIDE);
	pomRelTimeScale->ShowWindow(SW_SHOW);
	
	pomRelTimeScale->GetWindowRect ( olRect );
	olRect.right = olRect.left-2;
	olRect.left = olRect.right -35 ;
	olRect.top = olRect.bottom - 18;
	ScreenToClient ( olRect );
	omMinuteText.Create( "min.", WS_VISIBLE | WS_CHILD | SS_LEFT, olRect, this );

	olPos.y = olRect.top;

	olRect.right = olRect.left-4;
	olRect.left = olRect.right -35 ;
	olRect.top -= 5;
	omOffsetEdit.CreateEx(WS_EX_CLIENTEDGE, "EDIT", "0", 
						  WS_VISIBLE | WS_CHILD | ES_RIGHT, 
						  olRect, this, 47110813, NULL);
	omOffsetEdit.SetTypeToInt ( -1440, 1440 );
	MoveDlgItem ( this, IDC_EARLIEST_TXT, olPos.x, olPos.y );
	olPos = omFloatingPosRuty3;
	ClientToView ( &olPos ); 
	
	pomViewer->ChangeViewTo(omCurrRueUrno);
	return true;
}
//-----------------------------------------------------------------------------------------------------

void FlightIndepRulesView::MakeViewer()
{
	if (pomViewer != NULL)
	{
		delete pomViewer;
	}

	pomViewer = new CIndepViewer(-1);
	pomViewer->Attach(this);
	pomViewer->SetReferenceTimes(omOnBlock, omOfBlock);
	pomViewer->ChangeViewTo();
}
//-----------------------------------------------------------------------------------------------------

void FlightIndepRulesView::CloseGantt ()
{
	if ( !pomGantt )
		return;
	if (::IsWindow(pomGantt->pomConnectionDlg->GetSafeHwnd()))
	{
		pomGantt->pomConnectionDlg->Destroy();
	}

	if ( pomViewer )
		delete pomViewer;
	pomViewer = 0;
	if ( pomGantt )
		delete pomGantt;
	pomGantt = 0;
	if ( pomTimeScale )
		delete pomTimeScale;
	pomTimeScale = 0;

	omMinuteText.DestroyWindow();
	omOffsetEdit.DestroyWindow();
	if ( pomRelTimeScale )
		delete pomRelTimeScale;
	pomRelTimeScale = 0;

	pomDutyGrid->HideRows(0, 0, FALSE );
	pomDutyGrid->SetWindowPos( 0, 0, 0, omDutyOriginalRect.Width(), 
							   omDutyOriginalRect.Height(),
							   SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW );
	pomDutyGrid->DisplayRudTmp ();
	//  aktuelle Demandgroup wieder markieren
	CString olLastDgr;
	olLastDgr = omCurrDgrUrno;
	omCurrDgrUrno= "";	//  Damit Demanddetailcontrols initialisiert werden
	pomDutyGrid->MarkLine ( olLastDgr );

	m_HScroll.ShowWindow ( SW_HIDE );

	CPoint olPos;
	olPos = omEarliestPosRuty2;
	ClientToView ( &olPos ); 
	MoveDlgItem ( this, IDC_EARLIEST_TXT, olPos.x, olPos.y );
	olPos = omFloatingPosRuty2;
	ClientToView ( &olPos ); 
	MoveDlgItem ( this, IDC_FLOATING_CHK, olPos.x, olPos.y );
	DecideTimeControls();
}
//-----------------------------------------------------------------------------------------------------

void FlightIndepRulesView::CalcTimeScaleRect( LPRECT lpRect )
{
	CRect olCurrCellRect;
	*lpRect = omDutyOriginalRect;
	olCurrCellRect = pomDutyGrid->CalcRectFromRowCol(1, 0, 1, 0 );
	lpRect->left += imVerticalScaleWidth + 1;
	lpRect->top += olCurrCellRect.bottom +1;
	lpRect->bottom = lpRect->top +32;
	ClientToView ( lpRect );
}
//-----------------------------------------------------------------------------------------------------

void FlightIndepRulesView::CalcGanttRect( LPRECT lpRect )
{
	CRect olTimeScaleRect;
	*lpRect = omDutyOriginalRect;
	ClientToView ( lpRect );
	pomTimeScale->GetWindowRect (olTimeScaleRect);
	ScreenToClient ( &olTimeScaleRect );
	lpRect->top = olTimeScaleRect.bottom +1;
}
//-----------------------------------------------------------------------------------------------------

LONG FlightIndepRulesView::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
	if (pomGantt)
	{
		pomGantt->SetFocus();
	}

	LONG llRet = CRulesFormView::OnUpdateDiagram(wParam, lParam);

	int ilSelectedRuds = ogSelectedRuds.GetSize ();
	CString olSelRudUrno ;

	if (ilSelectedRuds == 1)
	{
		olSelRudUrno = ogSelectedRuds.GetAt(0);
	}
	if (omCurrRudUrno != olSelRudUrno)
	{
		SetCurrentRudUrno(olSelRudUrno);
		FillDemandDetailControls();
	}
	return llRet;
}
//-----------------------------------------------------------------------------------------------------

bool FlightIndepRulesView::IniControlPositions ()
{
	bool blRet = true;
	CRect olRect;
	CWnd *pCtrl = GetDlgItem ( IDC_FLOATING_CHK );

	if (pCtrl)
	{
		pCtrl->GetWindowRect ( &olRect );
		ScreenToClient ( &olRect );
		omFloatingPosRuty2 = olRect.TopLeft( );
		pomTimesGrid->GetWindowRect ( &olRect );
		ScreenToClient ( &olRect );
		omFloatingPosRuty3.x = olRect.left;
		omFloatingPosRuty3.y = omFloatingPosRuty2.y;
	}
	else
		blRet = false;
	
	pCtrl = GetDlgItem ( IDC_EARLIEST_TXT );
	if ( !pCtrl ) 
		blRet = false;
	else
	{
		pCtrl->GetWindowRect ( &olRect );
		ScreenToClient ( &olRect );
		omEarliestPosRuty2 = olRect.TopLeft( );
	}
	return true;
}
//-----------------------------------------------------------------------------------------------------

void FlightIndepRulesView::OnMouseMove(UINT nFlags, CPoint point) 
{
	CRulesFormView::OnMouseMove(nFlags, point);
}
//-----------------------------------------------------------------------------------------------------

void FlightIndepRulesView::OnEditKillFocus(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

	CCSEDITNOTIFY *prlNotify;
	prlNotify = (CCSEDITNOTIFY*) lParam;

	UINT ilID = (UINT) wParam;
	RecordSet olRecord;

	
	if ( !::IsWindow(omOffsetEdit.GetSafeHwnd()) || 
		 (prlNotify->SourceControl != &omOffsetEdit) )
	{
		if (prlNotify->SourceControl == &m_Dtc_Duen)
		{
			OnDatetimechangeDuen();
		}
		else if(prlNotify->SourceControl == &m_Dtc_Dube)
		{
			OnDatetimechangeDube();
		}
		else if (prlNotify->SourceControl == &omEditSplitMax)
			OnChangeMaxtime();
		else if ( prlNotify->SourceControl == &omEditSplitMin)
			OnChangeMintime();
		return;
	}
	if ( omCurrRudUrno.IsEmpty() || !ogBCD.GetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord) )
		return;
	CString olValue;
	olValue = prlNotify->Text;
	int ilValue = atoi(olValue) * 60;
	int ilDiff = 0, ilOldTsdb=0;
	
	olValue = olRecord.Values[igRudTsdbIdx];
	if ( sscanf ( olValue, "%d", &ilOldTsdb ) <= 0 )
		ilOldTsdb = 0;

	if (omOffsetEdit.GetStatus() == true)
	{
		if (BarWindow::IsMovingOK(&olRecord) == true)
		{
			ilDiff = ilValue - ilOldTsdb;
			olRecord.Values[igRudTsdbIdx].Format("%d", ilValue);
			BarWindow::SetFollowingTimeIntervals(&olRecord, ilDiff);
		}
		else
		{
			olValue.Format("%d", ilOldTsdb / 60);
			omOffsetEdit.SetWindowText(olValue);
		}
		ogBCD.SetRecord("RUD_TMP", "URNO", omCurrRudUrno, olRecord.Values);
		ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRecord);
		SelectRudBar (olRecord.Values[igRudUrnoIdx]);
	}
	
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------

void FlightIndepRulesView::SetBdpsState()
{
	CCS_TRY

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------

bool FlightIndepRulesView::SelectRudBar ( CString &ropRudUrno )
{
	int ilLineno=-1, ilBarno=-1;
	if ( pomGantt && !ropRudUrno.IsEmpty() )
	{
		if (pomViewer->FindBarGlobal(ropRudUrno, ilLineno, ilBarno) && 
			(ilLineno>=0) && (ilBarno>=0) )
		{
			pomViewer->DeselectAllBars(); 
			pomViewer->SetBarSelected(ilLineno, ilBarno, true);
		}
		else
			return false;
	}
	return true;
}
//---------------------------------------------------------------------------------------------------------------------

void FlightIndepRulesView::AddServices(CStringArray	&ropSerUrnos)
{
	CString		olTplUrno, olTplDalo, *polAloc=0;
	if(ropSerUrnos.GetSize() > 0)
	{
		if (omCurrRueUrno != "")
		{
			if ( pomComboBox )
			{		
				if ( GetSelectedTplUrno ( *pomComboBox, olTplUrno, TRUE, true ) )
					olTplDalo = ogBCD.GetField ( "TPL", "URNO", olTplUrno, "DALO" );
				if ( !olTplDalo.IsEmpty () )
					polAloc = &olTplDalo;
			}
			ogDataSet.CreateRudFromSerList(false, ropSerUrnos, omCurrRueUrno, olTplUrno, "-1", polAloc );
			ogDataSet.SetAllDbarAndDear(imRuty2 > 0);
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------

bool FlightIndepRulesView::IsSameTimeInDgr(bool &rbStart, bool &rbEnd)
{
	bool blRet = false;
	CTime olStartTime;
	CTime olEndTime;
	CTime olStartTime2;
	CTime olEndTime2;

	// Actually one should think we could use omCurrDgrUrno in this place. 
	// But watch out, this variable is sometimes cleared before calling 
	// DecideTimeControls(), so we can't rely on it. Therefore we use 
	// pomDutyGrid->GetSelectionInfo().
	CString olCurrDgrUrno, olCurrRudUrno;
	bool blDutySelected = pomDutyGrid->GetSelectionInfo(olCurrDgrUrno, olCurrRudUrno);

	if (blDutySelected)
	{
		CCSPtrArray <RecordSet> olRudArr;
		int ilFloatingState=2;
		CString olValue;
		ogBCD.GetRecords("RUD_TMP", "UDGR", olCurrDgrUrno, &olRudArr);

		ilFloatingState = GetFloatingState(olRudArr );
		if ( ilFloatingState == 2 )
			rbStart = rbEnd = false;
		else
		{
			rbStart = IsSameTimeValue(olRudArr, 
									  ilFloatingState ? igRudEadbIdx:igRudDebeIdx, 
									  olValue );

			rbEnd = IsSameTimeValue(olRudArr, 
								    ilFloatingState ? igRudLadeIdx:igRudDeenIdx, 
									olValue );
		}
		
		/*
		CString olStart, olStart2;
		CString olEnd, olEnd2;
		bool	blFloating, blFloating2;

		
		if (olRudArr.GetSize() > 0)
		{
			blFloating = (olRudArr[0].Values[igRudDbflIdx]=="1");
			olStart = olRudArr[0].Values[blFloating ? igRudEadbIdx: igRudDebeIdx];
			olEnd = olRudArr[0].Values[blFloating ? igRudLadeIdx: igRudDeenIdx];
			
			olStartTime = DBStringToDateTime ( olStart );
			olEndTime = DBStringToDateTime ( olEnd );

			for (int i = 1; i < olRudArr.GetSize(); i++)
			{
				blRet = true;

				blFloating2 = (olRudArr[i].Values[igRudDbflIdx]=="1");	
				if ( blFloating != blFloating2 )
				{
					blRet = false;
					break;
				}
				// mne debug
				olStart2 = olRudArr[i].Values[blFloating2 ? igRudEadbIdx: igRudDebeIdx];
				olEnd2 = olRudArr[i].Values[blFloating2 ? igRudLadeIdx: igRudDeenIdx];
				// end debug
				olStartTime2 = DBStringToDateTime ( olStart2 );
				olEndTime2 = DBStringToDateTime ( olEnd2 );
	
				if( (olStartTime == -1) || (olStartTime2 == -1) )
					rbStart = false;
				else
					if(olStartTime.GetHour() != olStartTime2.GetHour() ||
						olStartTime.GetMinute() != olStartTime2.GetMinute())
					{
						rbStart = false;
					}

				if( (olEndTime == -1) || (olEndTime2 == -1) )
					rbEnd = false;
				else
					if(olEndTime.GetHour() != olEndTime2.GetHour() ||
						olEndTime.GetMinute() != olEndTime2.GetMinute())
					{
						rbEnd = false;
					}

				if (!(rbStart || rbEnd))
					break;
			}
			*/
		if ( rbStart && rbEnd )
			blRet = true;
		olRudArr.DeleteAll();
	}

	return blRet;	
}
//---------------------------------------------------------------------------------------------------------------------

void FlightIndepRulesView::ProcessSgrNew(RecordSet *popRecord)
{
	CCS_TRY

	CString olGridRefTab;	// reference TAB.FLDN from grid
	CString olDdxRefTab;		// reference TAB.FLDN from record sent by DDX
	CString olGroupName;		// name of static group sent by DDX
	CString olChoiceList;
	CGXStyle olStyle;
	bool	blSgrFitsActTpl = true;

	olDdxRefTab = popRecord->Values[igSgrTabnIdx];
	olGroupName = popRecord->Values[igSgrGrpnIdx];

	blSgrFitsActTpl = IsSgrForActTpl ( popRecord );

	pomConditionsGrid->ChangeCondTableChoiceList ( olDdxRefTab, STATIC_GRP, 
												   olGroupName, blSgrFitsActTpl );

	
	// search in Conditionsgrid
	/*  replaced by ChangeCondTableChoiceList
	int ilCondCount = pomConditionsGrid->GetRowCount();
	for (int ilRow = 1; ilRow <= ilCondCount; ilRow++)
	{
		olGridRefTab = pomConditionsGrid->GetValueRowCol(ilRow, 8);
		if (olGridRefTab.GetLength() == 8)
		{
			olGridRefTab = olGridRefTab.Left(3);
			if (olGridRefTab == olDdxRefTab)
			{
				olStyle.SetDefault();	//  lokale Variable zur�cksetzen
				pomConditionsGrid->GetStyleRowCol(ilRow, 5, olStyle);
				if ( olStyle.GetIncludeChoiceList () )	
					olChoiceList = olStyle.GetChoiceList();
				else
					olChoiceList.Empty ();	

				if (olChoiceList.Find(olGroupName) == -1)
				{
					if ( !olChoiceList.IsEmpty() &&
						 (olChoiceList.Right(1) != "\n") )
					{
						olChoiceList += "\n";
					}
					olChoiceList += olGroupName;
					olStyle.SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
						   .SetChoiceList(olChoiceList);
					pomConditionsGrid->SetStyleRange(CGXRange(ilRow, 5), olStyle);
				}
			}
		}
	}*/
	if ( imRessourceType == 0 )			//  Personal	
	{
		if ( olDdxRefTab=="PFC" )
			pomLeftGrid->ModifyAllResChoiceLists ( olGroupName, blSgrFitsActTpl, true );
		if ( olDdxRefTab=="PER" )
			pomRightGrid->ModifyAllResChoiceLists ( olGroupName, blSgrFitsActTpl, true );
	}
	if ( imRessourceType == 2 )			//  Locations	
	{
		//  alle Zeilen checken, ob dargestellte Tabelle==olDdxRefTab
		for ( ROWCOL i=1; i<=pomRightGrid->GetRowCount(); i++ )
		{
			CString olUrno, olReft, olResTable, olCodeField;
			olUrno = pomRightGrid->GetValueRowCol(i, 3);
			if ( !olUrno.IsEmpty() )
				olReft = ogBCD.GetField("RLO_TMP", "URNO", olUrno, "REFT");
			if ( ParseReftFieldEntry ( olReft, olResTable, olCodeField ) &&
				 (olResTable==olDdxRefTab) )
				pomRightGrid->ModifyResChoiceList( olGroupName, blSgrFitsActTpl, true, i );
		}
	}

	ChangeResChoiceLists ( olDdxRefTab, popRecord, true, blSgrFitsActTpl );

	CCS_CATCH_ALL	
}
//----------------------------------------------------------------------------

void FlightIndepRulesView::ProcessSgrDelete(RecordSet *popRecord)
{
	CString olGridRefTab;	// reference TAB.FLDN from grid
	CString olDdxRefTab;	// reference TAB.FLDN from record sent by DDX
	CString olGroupName;	// name of static group sent by DDX
	CString olChoiceList;
	CGXStyle olStyle;


	olDdxRefTab = popRecord->Values[igSgrTabnIdx];
	olGroupName = popRecord->Values[igSgrGrpnIdx];

	if ( imRessourceType == 0 )			//  Personal	
	{
		if ( olDdxRefTab=="PFC" )
			pomLeftGrid->ModifyAllResChoiceLists ( olGroupName, false, true );
		if ( olDdxRefTab=="PER" )
			pomRightGrid->ModifyAllResChoiceLists ( olGroupName, false, true );
	}
	if ( imRessourceType == 2 )			//  Locations	
	{
		//  alle Zeilen checken, ob dargestellte Tabelle==olDdxRefTab
		for ( ROWCOL i=1; i<=pomRightGrid->GetRowCount(); i++ )
		{
			CString olUrno, olReft, olResTable, olCodeField;
			olUrno = pomRightGrid->GetValueRowCol(i, 3);
			if ( !olUrno.IsEmpty() )
				olReft = ogBCD.GetField("RLO_TMP", "URNO", olUrno, "REFT");
			if ( ParseReftFieldEntry ( olReft, olResTable, olCodeField ) &&
				 (olResTable==olDdxRefTab) )
				pomRightGrid->ModifyResChoiceList( olGroupName, false, true, i );
		}
	}
	ChangeResChoiceLists ( olDdxRefTab, popRecord, true, false );

}


//---------------------------------------------------------------------------
//			implementation of static and global functions
//---------------------------------------------------------------------------
static void IndepDDXCallback(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    FlightIndepRulesView *polView = (FlightIndepRulesView *)popInstance;
		
	RecordSet *polRecord = (RecordSet *) vpDataPointer;
	CString olRueUrno;

	switch ( ipDDXType )
	{	
		case RUD_TMP_NEW:
		case RUD_TMP_CHANGE:
		case RUD_TMP_DELETE:
			olRueUrno = polRecord->Values[igRudUrueIdx];
			// only process rud changes here, if rule event type is set to flight independent (EVTY=2)
			if (ogBCD.GetField("RUE", "URNO", olRueUrno, "EVTY" == CString("2")))
			{
				if (ipDDXType == RUD_TMP_NEW)
					polView->ProcessRudTmpNew(polRecord);
				if (ipDDXType == RUD_TMP_CHANGE)
					polView->ProcessRudTmpChange(polRecord);
				if (ipDDXType == RUD_TMP_DELETE)
					polView->ProcessRudTmpDelete(polRecord);
			}
			break;
		case SGR_NEW:
		case SGR_CHANGE:
			polView->ProcessSgrNew(polRecord);
			break;
		case SGR_DELETE:
			polView->ProcessSgrDelete(polRecord);
			break;
	}
}


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------


CString FlightIndepRulesView::CheckValuesDifferent(CCSPtrArray <RecordSet> opRecordList,int ipValueIdx)
{
	bool blIsDifferent = false;
	CString olValue1 = "",olValue2 = "";
	int ilValue = 0;
	bool blInit = true;
	for (int i = 0; i < opRecordList.GetSize() && !blIsDifferent; i++)
	{
		if(blInit)
		{
			olValue1 = opRecordList[i].Values[ipValueIdx];
			blInit = false;
		}

		olValue2 = opRecordList[i].Values[ipValueIdx];
		
		blIsDifferent = (olValue2 != olValue1);
	}
	if(blIsDifferent)
	{
		olValue1 = "";	
	}
	else
	{
		ilValue = atoi(olValue1) / 60;
		if(ilValue > 0)
		{
			olValue1.Format("%d", ilValue);
		}
	}
	return olValue1;
}

						
bool FlightIndepRulesView::IsSameTimeValue(CCSPtrArray <RecordSet> &ropRecordList,
										   int ipValueIdx, CString &ropValue )
{
	CTime olTime1=-1, olTime2=-1;
	bool  blSameValue=true;
	CString olValue;

	olValue = ropRecordList[0].Values[ipValueIdx];
	olTime1 = DBStringToDateTime ( olValue );

	for (int i=1; (i < ropRecordList.GetSize()) && blSameValue ; i++)
	{
		olValue = ropRecordList[i].Values[ipValueIdx];
		olTime2 = DBStringToDateTime ( olValue );

		if( (olTime1 == -1) || (olTime2 == -1) )
			blSameValue = false;
		else
		{
			if( olTime1.GetHour() != olTime2.GetHour() ||
				olTime1.GetMinute() != olTime2.GetMinute())
			{
				blSameValue = false;
			}
		}
	}
	if ( blSameValue )
		ropValue = olTime1.Format("%H:%M");
	else	
		ropValue = "";

	return blSameValue;
}

//  Get Status for tristate-checkbox "floating"
int FlightIndepRulesView::GetFloatingState(CCSPtrArray <RecordSet> &ropRecordList )
{
	CString olValue1 = "",olValue2 = "";
	CString olUrno;
	int ilValue, ilRet = -1;

	for (int i = 0; (i < ropRecordList.GetSize()) && (ilRet<2) ; i++)
	{
		olValue1 = ropRecordList[i].Values[igRudDbflIdx];
		olValue2 = ropRecordList[i].Values[igRudDeflIdx];
		olUrno = ropRecordList[i].Values[igRudUrnoIdx];
		if ( (olValue1=="1") ||	(olValue2=="1") )
			ilValue = 1;
		else
			ilValue = 0;
		if ( ilRet==-1)
			ilRet = ilValue;
		else
			if ( ilRet != ilValue )
				ilRet = 2;	/* different floating status */
	}
	if ( ilRet >= 0 )
		return ilRet;
	else
		return 2;
}

// this function creates a connnection between different RUDs
// (eg. a location and a staff member) by filling RUD.ULNK with a new urno
void FlightIndepRulesView::OnMenuCreateLink()
{
	long llNewUrno;
	CStringArray olUrnoList;
	llNewUrno = ogBCD.GetNextUrno();

	RecordSet olRud(ogBCD.GetFieldCount("RUD"));

	pomDutyGrid->GetSelectedUrnos(olUrnoList);
	for (int i = 0; i < olUrnoList.GetSize(); i++)
	{
		if(ogBCD.GetRecord("RUD_TMP", "URNO", olUrnoList.GetAt(i), olRud))
		{
			olRud.Values[igRudUlnkIdx].Format("%d", llNewUrno);
			ogBCD.SetRecord("RUD_TMP", "URNO", olRud.Values[igRudUrnoIdx], olRud.Values);
			ogDdx.DataChanged((void *)this,RUD_TMP_CHANGE,(void *)&olRud);
		}
	}

	// The RUD_CON_CHANGE message causes the whole gantt to be redrawn. We 
	// only send it once to avoid a flickering screen, which might otherwise
	// happen on a slow computer
	ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRud);
	pomDutyGrid->DisplayRudTmp (false);
	
}
//---------------------------------------------------------------------------------------------

// this function deletes a connnection between different RUDs by emptying RUD.ULNK
void FlightIndepRulesView::OnMenuDeleteLink()
{
	CStringArray olUrnoList;

	pomDutyGrid->GetSelectedUrnos(olUrnoList);

	RecordSet olRud(ogBCD.GetFieldCount("RUD"));
	for (int i = 0; i < olUrnoList.GetSize(); i++)
	{
		if(ogBCD.GetRecord("RUD_TMP", "URNO", olUrnoList.GetAt(i), olRud))
		{
			olRud.Values[igRudUlnkIdx].Empty();
			ogBCD.SetRecord("RUD_TMP", "URNO", olRud.Values[igRudUrnoIdx], olRud.Values);
			ogDdx.DataChanged((void *)this,RUD_TMP_CHANGE,(void *)&olRud);
		}
	}
	pomDutyGrid->DisplayRudTmp (false);
}

void FlightIndepRulesView::OnGridResorted(WPARAM wParam, LPARAM lParam)
{
	CGridFenster *polGrid = (CGridFenster*) wParam;
	CELLPOS *prlCellPos = (CELLPOS*) lParam;
	if ( !polGrid || ! prlCellPos || (polGrid !=pomDutyGrid) )
		return;
	ROWCOL ilCol = (ROWCOL) prlCellPos->Col;
	ROWCOL ilRow = (ROWCOL) prlCellPos->Row;
	if ( ( polGrid->GetRowCount() > 1 ) && ( ilRow == 0 ) )
	{	// sorting possibly changed
		imActiveRow = -1;  
		pomDutyGrid->SetCurrentCell(1, 0);
	}
}
void FlightIndepRulesView::OnDideChk() 
{
	// TODO: Add your control notification handler code here
	CCSPtrArray <RecordSet> olRudArr;
	CString	olDide;

	GetSelectedRuds ( olRudArr );

	omEditSplitMax.SetWindowText(CString(""));
	omEditSplitMin.SetWindowText(CString(""));

	if (omDideChk.GetCheck() == CHECKED)
	{
		omDideChk.SetCheck(UNCHECKED);
		olDide = CString("0");	// not divisable
		omEditSplitMin.EnableWindow(FALSE);
		omEditSplitMax.EnableWindow(FALSE);
	}
	else
	{
		omDideChk.SetCheck(CHECKED);
		olDide = CString("1");	// divisable
		omEditSplitMin.EnableWindow(TRUE);
		omEditSplitMax.EnableWindow(TRUE);
	}
	for ( int i=0; i<olRudArr.GetSize(); i++ )
	{
		olRudArr[i].Values[igRudDideIdx] = olDide;
		ogBCD.SetRecord("RUD_TMP", "URNO", olRudArr[i].Values[igRudUrnoIdx], olRudArr[i].Values);
	}	
	olRudArr.DeleteAll();
	
}

void FlightIndepRulesView::DisplaySplitValues ()
{
	CCSPtrArray <RecordSet> olRudArr;
	CString olCurrDgrUrno, olCurrRudUrno, olMaxd, olMind;
	int ilVal, ilMind=-1, ilMaxd=-1, ilDide=-1;
	
	GetSelectedRuds ( olRudArr );

	if ( olRudArr.GetSize() >= 1 )
	{
		ilDide = ( olRudArr[0].Values[igRudDideIdx] == "1" ) ? 1 : 0;
		if ( ilDide && ( sscanf ( olRudArr[0].Values[igRudMindIdx], "%d", &ilVal ) >= 1 ) )
			ilMind = ilVal;
		if ( ilDide && ( sscanf ( olRudArr[0].Values[igRudMaxdIdx], "%d", &ilVal ) >= 1 ) )
			ilMaxd = ilVal;
		for (int i = 1; (ilDide>0) && (i < olRudArr.GetSize()); i++)
		{
			ilVal = ( olRudArr[i].Values[igRudDideIdx] == "1" ) ? 1 : 0;
			if ( ilVal != ilDide )	
				ilDide = -1; 

			if ( ilMaxd >= 0 ) 
			{
				 if ( ( sscanf ( olRudArr[i].Values[igRudMaxdIdx], "%d", &ilVal ) < 1 )  ||
					  (ilVal!= ilMaxd) )
					  ilMaxd = -1;
			}
			if ( ilMind >= 0 ) 
			{
				 if ( ( sscanf ( olRudArr[i].Values[igRudMindIdx], "%d", &ilVal ) < 1 )  ||
					  (ilVal!= ilMind) )
					  ilMind = -1;
			}
		}
	}

	omDideChk.SetCheck(ilDide==1);
	if ( ilDide==1 )
	{
		if ( ilMind >= 0 )
			olMind.Format ( "%d", ilMind / 60 );
		if ( ilMaxd >= 0 )
			olMaxd.Format ( "%d", ilMaxd / 60 );
	}
	omEditSplitMax.SetWindowText ( olMaxd );
	omEditSplitMin.SetWindowText ( olMind );
	olRudArr.DeleteAll();
}


void FlightIndepRulesView::OnChangeMintime() 
{
	// TODO: Add your control notification handler code here
	CString olSplitMin, olVal;
	CString olCurrDgrUrno, olCurrRudUrno;
	CCSPtrArray <RecordSet> olRudArr;
	int		ilValue;

	if( !omEditSplitMax.GetStatus() )
		return;

	GetSelectedRuds ( olRudArr );

	if (omDideChk.GetCheck() == CHECKED)
	{
		omEditSplitMin.GetWindowText(olSplitMin);
		ilValue = atoi(olSplitMin);
		if ( ilValue > 0 )
			olVal.Format("%d", (ilValue * 60) );
	}
	for ( int i=0; i<olRudArr.GetSize(); i++)
		ogBCD.SetField( "RUD_TMP", "URNO", olRudArr[i].Values[igRudUrnoIdx], 
						"MIND", olVal );
	olRudArr.DeleteAll ();
	
}

void FlightIndepRulesView::OnChangeMaxtime() 
{
	// TODO: Add your control notification handler code here
	// TODO: Add your control notification handler code here
	CCSPtrArray <RecordSet> olRudArr;
	CString olSplitMax, olVal;
	int		ilValue;
	CString olCurrDgrUrno, olCurrRudUrno;

	if( !omEditSplitMax.GetStatus() )
		return;

	GetSelectedRuds ( olRudArr );

	if (omDideChk.GetCheck() == CHECKED)
	{
		omEditSplitMax.GetWindowText(olSplitMax);
		ilValue = atoi(olSplitMax);
		if ( ilValue > 0 )
			olVal.Format("%d", (ilValue * 60) );
	}
	for ( int i=0; i<olRudArr.GetSize(); i++)
		ogBCD.SetField( "RUD_TMP", "URNO", olRudArr[i].Values[igRudUrnoIdx], 
						"MAXD", olVal );
	olRudArr.DeleteAll ();

}

int FlightIndepRulesView::GetSelectedRuds ( CCSPtrArray <RecordSet> &opRudArr )
{
	CString olCurrDgrUrno, olCurrRudUrno;
	opRudArr.DeleteAll ();

	if (!pomDutyGrid->IsExpanded() && !pomGantt )
	{
		bool blDutySelected = pomDutyGrid->GetSelectionInfo(olCurrDgrUrno, olCurrRudUrno);
		if ( blDutySelected )
			ogBCD.GetRecords("RUD_TMP", "UDGR", olCurrDgrUrno, &opRudArr);
	}
	else
		if ( !omCurrRudUrno.IsEmpty() )
			ogBCD.GetRecords("RUD_TMP", "URNO", omCurrRudUrno, &opRudArr ) ;
	return opRudArr.GetSize(); 
}

void FlightIndepRulesView::OnFaddChk() 
{
	// TODO: Add your control notification handler code here
	CString olValue;
	int ilNoDemands = IsDlgButtonChecked(IDC_FADD_CHK);
	
	olValue = ( ilNoDemands == 1 ) ? "0" : "1";
		
	if(	imRuty2 && pomDutyGrid->IsExpanded() && !omCurrRudUrno.IsEmpty() )
	{
		ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "FADD", olValue);
		DecideTimeControls ();
	}
	else
		if ( /*!imRuty2 && */ !omCurrDgrUrno.IsEmpty () )
		{	//  bei Sammeldarstellung: f�r alle RUD's aus der aktuellen
			//  Demandgroup flag "FADD" setzen
			ogDataSet.SetFieldForDgr ( "RUD_TMP", omCurrDgrUrno, "FADD", olValue );
			DecideTimeControls ();
		}
	
}

