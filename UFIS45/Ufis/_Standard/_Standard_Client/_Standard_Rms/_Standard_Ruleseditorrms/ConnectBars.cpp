// ConnectBars.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <ConnectBars.h>
#include <CedaBasicData.h>
#include <CCSGlobl.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif



//---------------------------------------------------------------------------------------------
//						construction / destruction
//---------------------------------------------------------------------------------------------
ConnectBars::ConnectBars(RulesGanttViewer *pViewer, CWnd* pParent /*=NULL*/)
	: CDialog(ConnectBars::IDD, pParent)
{
	pomParent = pParent;
	pomGanttViewer = pViewer;
	
	bmIsItemChosen = false;
	//{{AFX_DATA_INIT(ConnectBars)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	Create();
}
//---------------------------------------------------------------------------------------------

ConnectBars::~ConnectBars()
{

}



//-----------------------------------------------------------------------------------------------------------------------
//					create, destroy
//-----------------------------------------------------------------------------------------------------------------------
void ConnectBars::Create()
{
	CDialog::Create(IDD_CONNECT_BARS, pomParent);
}
//-----------------------------------------------------------------------------------------------------------------------

void ConnectBars::Destroy()
{
	DestroyWindow();
	delete this;
}
//-----------------------------------------------------------------------------------------------------------------------

void ConnectBars::OnDestroy() 
{
	CDialog::OnDestroy();
	
	if (pomParent != NULL)
	{
		pomParent->SendMessage(WM_DIALOG_CLOSED, (WPARAM) this);
	}
}

//---------------------------------------------------------------------------------------------
//					data exchange, message map
//---------------------------------------------------------------------------------------------
void ConnectBars::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ConnectBars)
	DDX_Control(pDX, CONNECT_CHB_SAMERES, m_Chb_SameRes);
	DDX_Control(pDX, CONNECT_EDT_OFFSET, m_Edt_Offset);
	DDX_Control(pDX, CONNECT_COB_TYPES, m_ConnectionTypes);
	//}}AFX_DATA_MAP
}
//---------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(ConnectBars, CDialog)
	//{{AFX_MSG_MAP(ConnectBars)
	ON_WM_DESTROY()
	ON_BN_CLICKED(CONNECT_CHB_SAMERES, OnSameResourceChb)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//---------------------------------------------------------------------------------------------
//					message handlers
//---------------------------------------------------------------------------------------------
BOOL ConnectBars::OnInitDialog() 
{
	// -------------------------------------------------------
	// actual initialization on opening this dialog on an
	// existing connection happens in InitData()
	// -------------------------------------------------------

	CCS_TRY

	CDialog::OnInitDialog();
	
	SetStaticTexts();

	// add selectable connection types to list
	int ilIndex = -1;
	ilIndex = m_ConnectionTypes.AddString(GetString(1526));	// start to finish (0)
	m_ConnectionTypes.SetItemData(ilIndex, 0);

	ilIndex = m_ConnectionTypes.AddString(GetString(1527));	// start to start (1)
	m_ConnectionTypes.SetItemData(ilIndex, 1);

	ilIndex = m_ConnectionTypes.AddString(GetString(1528));	// finish to start (2)
	m_ConnectionTypes.SetItemData(ilIndex, 2);	

	ilIndex = m_ConnectionTypes.AddString(GetString(1529));	// finish to finish (3)
	m_ConnectionTypes.SetItemData(ilIndex, 3);


	m_Chb_SameRes.SetCheck(UNCHECKED);

	CCS_CATCH_ALL

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
//---------------------------------------------------------------------------------------------

void ConnectBars::OnOK() 
{
	CCS_TRY


	RecordSet olSourceRec;
	RecordSet olTargetRec;

	// get RUD data
	CButton *polFirstItem = (CButton*) GetDlgItem(CONNECT_RBT_ORDER1);
	if (polFirstItem->GetCheck() == CHECKED)
	{
		olSourceRec = omRudRecord1;
		olTargetRec = omRudRecord2;
	}
	else
	{
		olSourceRec = omRudRecord2;
		olTargetRec = omRudRecord1;
	}
	
	if (bmIsUpdate == false)
	{
		// bar can't be target of two connections
		if (olTargetRec.Values[igRudUpdeIdx].IsEmpty() == FALSE && olTargetRec.Values[igRudUpdeIdx] != CString(" "))
		{
			// mne test 
			// CString TgtUpde = olTargetRec.Values[igRudUpdeIdx];
			// CString SrcUpde = olSourceRec.Values[igRudUpdeIdx];
			// end test 

			MessageBox(GetString(1553), GetString(1525));
			return;
		}
		// bar can't be source of two connections
		else if (olSourceRec.Values[igRudUndeIdx].IsEmpty() != TRUE && olSourceRec.Values[igRudUndeIdx] != CString(" "))
		{
			MessageBox(GetString(1554), GetString(1525));
			return;
		}
		// circular chained connections
		else if(IsCyclicConnection(&olSourceRec, &olTargetRec) == true)
		{
			MessageBox(GetString(1555), GetString(1530));
			return;
		}
		else
		{
			// NOTHING HERE
		}
	}
	
	// get list box selection
	int ilIndex = m_ConnectionTypes.GetCurSel();
	long llType = m_ConnectionTypes.GetItemData(ilIndex);
	
	// get offset value
	CString olOffset;	
	m_Edt_Offset.GetWindowText(olOffset);
	int ilOffset = atoi(olOffset);
	

	switch (llType)
	{
		case 0:
			olSourceRec.Values[igRudRendIdx] = CString("SF");
			olTargetRec.Values[igRudRepdIdx] = CString("SF");
			break;

		case 1:
			olSourceRec.Values[igRudRendIdx] = CString("SS");
			olTargetRec.Values[igRudRepdIdx] = CString("SS");
			break;

		case 2:
			olSourceRec.Values[igRudRendIdx] = CString("FS");
			olTargetRec.Values[igRudRepdIdx] = CString("FS");
			break;

		case 3:
			olSourceRec.Values[igRudRendIdx] = CString("FF");
			olTargetRec.Values[igRudRepdIdx] = CString("FF");

			break;

		default:
			break;
	}
	
	
	// create connection data in memory object RUD_TMP
	olSourceRec.Values[igRudUndeIdx] = olTargetRec.Values[igRudUrnoIdx];
	olTargetRec.Values[igRudUpdeIdx] = olSourceRec.Values[igRudUrnoIdx];
	olSourceRec.Values[igRudTsndIdx].Format("%d", ilOffset * 60);
	olTargetRec.Values[igRudTspdIdx].Format("%d", ilOffset * 60);


	// get same resources flag
	char clSameRes = '0';		
	if (m_Chb_SameRes.GetCheck())
		clSameRes = '1';
	
	{
		CString olRede;
		olRede = olTargetRec.Values[igRudRedeIdx];
		if (olRede.GetLength() < 3)		/* hag010529: sicherheitshalber */
			olRede = "0#0";
		/*if (olRede.GetLength() >= 1) hag010529*/
		{
			/*olRede.Left(1) = "1";*/
			olRede.SetAt(0, clSameRes );	/* hag010529 */
			olTargetRec.Values[igRudRedeIdx] = olRede;
		}
		
		olRede = olSourceRec.Values[igRudRedeIdx];
		if (olRede.GetLength() < 3)		/* hag010529: sicherheitshalber */
			olRede = "0#0";
		/*if (olRede.GetLength() >= 1) hag010529*/
		{
			/*  olRede.Right(1) = "1";  */
			olRede.SetAt(2, clSameRes );	/* hag010529 */
			olSourceRec.Values[igRudRedeIdx] = olRede;
		}
	}
	
	
	bool blTest = true;

	if (bmIsUpdate == false)
	{
		if (olTargetRec.Values[igRudDrtyIdx] == "0")
		{
			MessageBox(GetString(1536), GetString(IDS_ERROR));
			olSourceRec.Values[igRudUndeIdx] = CString(" ");
			olTargetRec.Values[igRudUpdeIdx] = CString(" ");
			olSourceRec.Values[igRudTsndIdx].Format("%d", 0);
			olTargetRec.Values[igRudTspdIdx].Format("%d", 0);
		}
		else
		{
			MoveConnectedBars(ilOffset, &olSourceRec, &olTargetRec);
		}
	}
	else
	{
		// change direction of an existing connection
		if (IsCyclicConnection(&olSourceRec, &olTargetRec) == true)
		{
			olSourceRec.Values[igRudUpdeIdx] = CString("");
			olSourceRec.Values[igRudRepdIdx] = CString("");
			olSourceRec.Values[igRudTspdIdx] = CString("");
			olTargetRec.Values[igRudUndeIdx] = CString("");
			olTargetRec.Values[igRudRendIdx] = CString("");
			olTargetRec.Values[igRudTsndIdx] = CString("");

			MoveConnectedBars(ilOffset, &olSourceRec, &olTargetRec);
		}
	}

	
	// mne test
	/*CString test1 = olSourceRec.Values[igRudTsndIdx];
	CString test2 = olSourceRec.Values[igRudTspdIdx];
	CString test3 = olTargetRec.Values[igRudTsndIdx];
	CString test4 = olTargetRec.Values[igRudTspdIdx];*/
	// end test
	

	// write data
	blTest &= ogBCD.SetRecord("RUD_TMP", "URNO", olSourceRec.Values[igRudUrnoIdx], olSourceRec.Values);
	blTest &= ogBCD.SetRecord("RUD_TMP", "URNO", olTargetRec.Values[igRudUrnoIdx], olTargetRec.Values);

	// tell viewer that the records were changed
	ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olSourceRec);

	CDialog::OnOK();


	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

void ConnectBars::OnCancel() 
{
	Destroy();	
}



//---------------------------------------------------------------------------------------------
//						other stuff
//---------------------------------------------------------------------------------------------

// opSource: Urno of bar connection starts at
// opTarget: Urno of bar connection leads to
// ipType: 0 = SF, 1 = SS, 2 = FS, 3 = FF
void ConnectBars::InitData(CString opSource, CString opTarget, int ipType)
{
	//------------------------------------
	// ipType:	0 = Start to Finish
	//			1 = Start to Start
	//			2 = Finish to Start
	//			3 = Finish to Finish
	//------------------------------------

	CCS_TRY

	// get bar texts 
	CString olText1, olText2;
	int ilLine1 = -1;
	int ilLine2 = -1;
	int ilBar1 = -1;
	int ilBar2 = -1;


	// get RUD records
	if (opSource != "-1" && opTarget != "-1")
	{
		bool blTest = true;

		blTest &= ogBCD.GetRecord("RUD_TMP", "URNO", opSource, omRudRecord1);
		blTest &= ogBCD.GetRecord("RUD_TMP", "URNO", opTarget, omRudRecord2);
		
		blTest &= pomGanttViewer->FindBarGlobal(&omRudRecord1, ilLine1, ilBar1);
		blTest &= pomGanttViewer->FindBarGlobal(&omRudRecord2, ilLine2, ilBar2);

		if (blTest == true)
		{
			BARDATA *prlBar1 = pomGanttViewer->GetBar(ilLine1, ilBar1);
			BARDATA *prlBar2 = pomGanttViewer->GetBar(ilLine2, ilBar2);
			olText1 = prlBar1->Text;
			olText2 = prlBar2->Text;
			bmIsItemChosen = true;
		}
	}
	else
	{
		olText1 = GetString(1535) + CString(" 1");
		olText2 = GetString(1535) + CString(" 2"); 
		bmIsItemChosen = false;
	}

	// check if there is a connection between these two bars already
	bmIsUpdate = false;
	CString olTestTarget;
	if (pomGanttViewer->GetArrowTargetBySource(opSource, olTestTarget) == true)
	{
		if (olTestTarget == opTarget)
		{
			bmIsUpdate = true;
		}
	}
	
	// set button texts
	CButton *polFirstBtn = (CButton*) GetDlgItem(CONNECT_RBT_ORDER1);
	CButton *polSecondBtn = (CButton*) GetDlgItem(CONNECT_RBT_ORDER2);
	polFirstBtn->SetWindowText(olText1);
	polSecondBtn->SetWindowText(olText2);
	
	// set controls
	if (ipType >= 0 && ipType < 4)
	{
		for (int ilIndex = 0; ilIndex < m_ConnectionTypes.GetCount(); ilIndex++)
		{
			if (ipType == (int)m_ConnectionTypes.GetItemData(ilIndex))
			{
				m_ConnectionTypes.SetCurSel(ilIndex);
				CString olTest;
				m_ConnectionTypes.GetLBText(ilIndex, olTest);
			}
		}
	}
	else
	{
		m_ConnectionTypes.SetCurSel(-1);
	}
	
	polFirstBtn->SetCheck(CHECKED);
	polSecondBtn->SetCheck(UNCHECKED);	
	
	// set checkbox for same resources
	if (omRudRecord1.Values[igRudRedeIdx].Right(1) == CString("1"))
	{
		m_Chb_SameRes.SetCheck(CHECKED);
	}
	
	// set offset value
	CString olOffset, olTmp;
	olTmp = omRudRecord1.Values[igRudTsndIdx];
	int ilOffset = atoi(olTmp) / 60;
	olOffset.Format("%d", ilOffset);
	CWnd *polOffset = GetDlgItem(CONNECT_EDT_OFFSET);
	polOffset->SetWindowText(olOffset);
	

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

bool ConnectBars::IsCyclicConnection(RecordSet *popSource, RecordSet *popTarget)
{
	bool blRet = true;

	CString olPreviousUrno;
	CString olTargetUrno;

	olPreviousUrno = popSource->Values[igRudUpdeIdx];
	olTargetUrno = popTarget->Values[igRudUrnoIdx];

	if (olPreviousUrno.IsEmpty() == FALSE && olPreviousUrno != CString(" "))
	{
		if (olPreviousUrno == olTargetUrno)
		{
			blRet = true;
		}
		else
		{
			RecordSet olPrevious(ogBCD.GetFieldCount("RUD_TMP"));
			ogBCD.GetRecord("RUD_TMP", "URNO", olPreviousUrno, olPrevious);
			blRet = IsCyclicConnection(&olPrevious, popTarget);
		}
	}
	else
	{
		blRet = false;
	}

	return blRet;	
}
//---------------------------------------------------------------------------------------------

void ConnectBars::MoveConnectedBars(int ipOffset, RecordSet *popSource, RecordSet *popTarget)
{
	CCS_TRY

	CString olConType;
	olConType = popTarget->Values[igRudRepdIdx];

	int ilLine = 0;
	int ilBar = 0;
	BARDATA *prlSourceBar = NULL;
	BARDATA *prlTargetBar = NULL;

	if (pomGanttViewer->FindBarGlobal(popSource->Values[igRudUrnoIdx], ilLine, ilBar) == true)
	{
		prlSourceBar =	pomGanttViewer->GetBar(ilLine, ilBar);
	}
	else
	{
		return;
	}

	if (pomGanttViewer->FindBarGlobal(popTarget->Values[igRudUrnoIdx], ilLine, ilBar) == true)
	{
		prlTargetBar = pomGanttViewer->GetBar(ilLine, ilBar);
	}
	else
	{
		return;
	}
	
	// turnaround bars can't be connected
	if (/*(olConType == "SF" || olConType == "FS") &&*/ popTarget->Values[igRudDrtyIdx] == "0")
	{
		MessageBox(GetString(1536), GetString(IDS_ERROR));
	}
	else
	{
		CTimeSpan olDiff;
	
		if (olConType == "SF")
		{
			CTime olNewEnd = prlSourceBar->Debe;
			CTime olOldEnd = prlTargetBar->Deen;
			olDiff = olNewEnd - olOldEnd;
		}
		else if (olConType == "SS")
		{
			CTime olNewStart = prlSourceBar->Debe;
			CTime olOldStart = prlTargetBar->Debe;
			olDiff = olNewStart - olOldStart;
		}
		else if (olConType == "FS")
		{
			CTime olNewStart = prlSourceBar->Deen;
			CTime olOldStart = prlTargetBar->Debe;
			olDiff = olNewStart - olOldStart;
		}
		else if (olConType == "FF")
		{
			CTime olNewEnd = prlSourceBar->Deen;
			CTime olOldEnd = prlTargetBar->Deen;
			olDiff = olNewEnd - olOldEnd;
		}
		else
		{
			ogLog.Trace("ERROR", "Invalid REPD value (%s)", olConType);
			return;
		}
		
		WriteData(olDiff.GetTotalSeconds(), popTarget, popSource);

		// are there more arrows going out from our target bar ? ==> Recursion !
		if (popTarget->Values[igRudUndeIdx].IsEmpty() == FALSE && popTarget->Values[igRudUndeIdx] != " ")
		{
			pomGanttViewer->ChangeViewTo(pomGanttViewer->omRuleUrno);
			RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
			ogBCD.GetRecord("RUD_TMP", "URNO", popTarget->Values[igRudUndeIdx], olRecord);
			MoveConnectedBars(0, popTarget, &olRecord);
		}
	}

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

void ConnectBars::WriteData(int ipSecs, RecordSet *popTarget, RecordSet *popSource)
{
	//----------------------------------------------------------
	// get interval (TSDB or TSDE) from RUD_TMP, 
	// add or subtract new value and write it back
	//----------------------------------------------------------

	if (ipSecs != 0)
	{
		int ilBarType = 1;
		int ilInterval = 0;
		int ilDummy;
		if (sscanf(popTarget->Values[igRudDrtyIdx], "%d", &ilDummy) >= 1)
		{
			ilBarType = ilDummy;
		}

		switch (ilBarType)
		{
			case 0:		// turnaround
				break;
   
			case 1:		// inbound
				ilInterval = atoi(popTarget->Values[igRudTsdbIdx]);
				ilInterval += ipSecs;
				popTarget->Values[igRudTsdbIdx].Format("%d", ilInterval);
				break;

			case 2:		// outbound
				ilInterval = atoi(popTarget->Values[igRudTsdeIdx]);
				ilInterval += ipSecs;
				popTarget->Values[igRudTsdeIdx].Format("%d", ilInterval);
				break;

			default:
				break;
		}
	}
	
	// mne test
	CString olTest1 = popTarget->Values[igRudTsndIdx];
	CString olTest2 = popTarget->Values[igRudTspdIdx];
	CString olTest3 = popSource->Values[igRudTsndIdx];
	CString olTest4 = popSource->Values[igRudTspdIdx];
	int ilTest = 1;
	// end test

	// writing these records is necessary even if nothing changed in this function
	ogBCD.SetRecord("RUD_TMP", "URNO", popTarget->Values[igRudUrnoIdx], popTarget->Values);
	ogBCD.SetRecord("RUD_TMP", "URNO", popSource->Values[igRudUrnoIdx], popSource->Values);
}
//---------------------------------------------------------------------------------------------

void ConnectBars::SetStaticTexts()
{
	CCS_TRY

	// caption
	CString olText;
	olText = GetString(1530);
	SetWindowText(olText);

	// buttons
	CWnd *pWnd = GetDlgItem(IDOK);
	if ( pWnd )
	{
		olText = GetString(OK);
		pWnd->SetWindowText(olText);
	}
	pWnd = GetDlgItem(IDCANCEL);
	if ( pWnd )
	{	
		olText = GetString(CANCEL);
		pWnd->SetWindowText(olText);
	}

	SetDlgItemLangText ( this, CONNECT_FRA_ORDER, IDS_CONSTART );
	SetDlgItemLangText ( this, CONNECT_RBT_ORDER1, IDS_BALKEN1 );
	SetDlgItemLangText ( this, CONNECT_RBT_ORDER2, IDS_BALKEN2 );
	SetDlgItemLangText ( this, CONNECT_STA_TYPES, IDS_KIND_OF_CONNECTION );
	SetDlgItemLangText ( this, CONNECT_STA_OFFSET, IDS_OFFSET );
	SetDlgItemLangText ( this, CONNECT_STA_MIN, IDS_MIN );
	SetDlgItemLangText ( this, CONNECT_CHB_SAMERES, IDS_STRING172);

	CCS_CATCH_ALL
}
//--------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------


void ConnectBars::OnSameResourceChb() 
{
	// TODO: Add your control notification handler code here
	
}
