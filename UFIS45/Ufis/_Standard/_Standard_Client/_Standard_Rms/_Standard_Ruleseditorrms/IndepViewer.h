// IndepViewer.h: interface for the CIndepViewer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INDEPVIEWER_H__DF998FA3_9E5D_11D3_93A4_00001C033B5D__INCLUDED_)
#define AFX_INDEPVIEWER_H__DF998FA3_9E5D_11D3_93A4_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <RulesGanttViewer.h>
class RecordSet;

class CIndepViewer : public RulesGanttViewer  
{
public:
	CIndepViewer(long Rkey);
	virtual ~CIndepViewer();

	void SetCurrentUdgr(CString opUdgr);
	bool IsPassFilter(RecordSet *popRud);
//  Data members
	CString omUdgr;
};

#endif // !defined(AFX_INDEPVIEWER_H__DF998FA3_9E5D_11D3_93A4_00001C033B5D__INCLUDED_)
