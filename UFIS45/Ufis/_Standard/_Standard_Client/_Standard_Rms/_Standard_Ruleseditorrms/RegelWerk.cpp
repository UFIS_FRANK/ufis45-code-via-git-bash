// RegelWerk.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>

#include <RegelWerk.h>
#include <MainFrm.h>
#include <RegelWerkDoc.h>
#include <RegelWerkView.h>
#include <AboutBox.h>
#include <CedaBasicData.h>
#include <CCSGlobl.h>
#include <CCSCedaCom.h>
#include <VersionInfo.h>
#include <PrivList.h>
#include <LoginDlg.h>
#include <RegisterDlg.h>
#include <Cviewer.h>
#include <BasicData.h>
#include <CCSLOG.h>
#include <StartDlg.h>
#include <RulesList.h>
#include <WinNT_Enumerator.h>
#include <AatLogin.h>
#include <CedaInitModuData.h>

/*#include "d:\mark2\develop\debugging\MemoryTracking\MemoryTracking.h"
USE_MEMORYTRACKING("c:\\temp\\memleakfinder.log",true)
#pragma warning(disable:4073)							
#pragma warning(disable:4786)
#pragma init_seg(lib)*/


//ge�ndert HAG
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif

/* recursive Groups allowed ? Is false as long GroupEditor does't   */
/* provide this functionality									    */
#define NESTED_GROUPS 0  

//------------------------------------------------------------------------
//		global and static function declarations
//------------------------------------------------------------------------
// (implementation: see at the end of this file)
static int GetDisplayInfo(int &ripScrWidth, int &ripScrHeight);
static void InitDefaultView();



//------------------------------------------------------------------------
//					construction / destruction
//------------------------------------------------------------------------
CRegelWerkApp::CRegelWerkApp()
{
	//
}
//------------------------------------------------------------------------

CRegelWerkApp::~CRegelWerkApp()
{
	ogChoiceLists.DeleteAll();
	ogResChoiceLists.DeleteAll();
	ogRefTables.DeleteAll();
	
	ogInboundReferences.DeleteAll();
	ogOutboundReferences.DeleteAll();
	omProcList.DeleteAll();
}


//------------------------------------------------------------------------
//					The one and only CRegelWerkApp object
//------------------------------------------------------------------------
CRegelWerkApp theApp;



//------------------------------------------------------------------------
//						message map
//------------------------------------------------------------------------
BEGIN_MESSAGE_MAP(CRegelWerkApp, CWinApp)
	//{{AFX_MSG_MAP(CRegelWerkApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()



//------------------------------------------------------------------------
//					Initialization
//------------------------------------------------------------------------
BOOL CRegelWerkApp::InitInstance()
{
	CString olWhere;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

//--- standard initialization
	AfxEnableControlContainer();
	Enable3dControlsStatic();	// Call this when linking to MFC statically
	// Standard initialization
	SetDialogBkColor(::GetSysColor(COLOR_BTNFACE));        // Set dialog background color to gray

	//--- Change the registry key under which our settings are stored.
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	//--- Load standard INI file options (including MRU)
	LoadStdProfileSettings();  
		
	//--- initialize error handling
/*		PRF5468: no longer using CritErr.txt
	CTime olTmpTime = CTime::GetCurrentTime();
	CString olTmpStr = olTmpTime.Format("Datum: %d.%m.%Y");
	of_catch.open("CritErr.txt", ios::app);
	of_catch << olTmpStr.GetBuffer(0) << endl;
	of_catch << "=================" << endl;
*/	
	// MNE: THIS IS ONLY FOR MEM LEAK DETECTING AND MUST BE TAKEN OUT FOR DELIVERY VERSIONS !!!
	// HMODULE hMod = (HMODULE)::LoadLibrary("dll.dll");
	// END MNE
	
	//--- initialize drag'n'drop
	if (!AfxOleInit())
	{
		return FALSE;
	}

	//--- create fonts and brushes
    InitFont();
	CreateBrushes();
	
	//--- basic grid initialization
	GXInit();

//--- register form view
	CSingleDocTemplate* pNewDocTemplate = new CSingleDocTemplate(
			IDR_MAINFRAME                          /*IDR_REGELEDITOR_TMPL */,
			RUNTIME_CLASS(CRegelWerkDoc),    	   // document class
			RUNTIME_CLASS(CMainFrame),	           // main SDI frame window
			RUNTIME_CLASS(CRegelEditorFormView)); // view class

	AddDocTemplate(pNewDocTemplate);

//--- initialize CEDA
	if (getenv("CEDA") != NULL)
	{
		strcpy(pcgConfigPath, getenv("CEDA"));
	}
	else
	{
		strcpy(pcgConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	}
	
	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pcgConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0) 
	{
		ogBcHandle.UseBcProxy();
		ogBcHandle.StopBc();
	}
	else if (stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		ogBcHandle.UseBcCom();
		ogBcHandle.StopBc();
	}

//--- path to logfile for ccslog
	char pclTmp[128];
	GetPrivateProfileString(ogAppName, "LOGFILE", CCSLog::GetUfisSystemPath("\\RULESLOG.CFG"), pclTmp, sizeof(pclTmp), pcgConfigPath);
	ogLog.Load(pclTmp);

//#ifdef _DEBUG
	//if(strcmp(pcgLogFilePath, "DEFAULT"))
	//{
		// in Debug-Version erstellen wir Logfile mit Datumsteil
		COleDateTime olDate;
		olDate = COleDateTime::GetCurrentTime();
		CString olNewLog;
		olNewLog.Format(pclTmp);
		olNewLog.Insert(olNewLog.Find('.'),olDate.Format("%Y%m%d_%H%M"));

		strcpy(pcgLogFilePath, (LPCTSTR)olNewLog);
	//}
//#endif _DEBUG
	//test logfile
	//char tmp[256];
	//sprintf(tmp,"test \n");
	//CString tmp_;
	//tmp_.Format(tmp);
	oglogData += pclTmp;
	WriteInlogFile();
//--- should relative timescale be adapted ?

	char pclTmp1[4];
	GetPrivateProfileString(ogAppName, "ADAPT_SCALE", "OFF", pclTmp1, sizeof(pclTmp1), pcgConfigPath);
	bgAdaptScale = false;
	if (strcmp(pclTmp1, CString("ON")) == 0)
	{
		bgAdaptScale = true;
	}


	char pclTmp7[10];
	GetPrivateProfileString(ogAppName, "MAX_GROUND_TIME_IS_MIN", "FALSE", pclTmp7, sizeof(pclTmp7), pcgConfigPath);
	if (strcmp(pclTmp7, CString("TRUE")) == 0)
	{
		bgMaxGroundTimeIsMin = true;
	}

   //ccicheck

    char cci[6];
	GetPrivateProfileString(ogAppName, "CCI_TIME_RESTRICTION", "FALSE", cci, sizeof(cci), pcgConfigPath);

	
	
	if (strcmp(cci, "TRUE") == 0)
	{
			ccicheck = true;
	}
	else
	{       ccicheck = false;          }

	
	
	
	
	//--- get debugging status
	char pclTmp3[4];
	GetPrivateProfileString(ogAppName, "DEBUG", "OFF", pclTmp3, sizeof(pclTmp3), pcgConfigPath);
	bgDebug = false;
	if (strcmp(pclTmp3, "ON") == 0)
	{
		bgDebug = true;
	}

//--- get reference times
	int ilRefCount = 0;
	int ilPos = 0;
	char pclTmp2[1024];
	REFTIME rlRefTime;
	CString olTmp;
	CStringArray olTmpArr;

	GetPrivateProfileString(ogAppName, "TIFA", "STOA(STA),ETOA(ETA),TMOA(TMO),LAND(LAND),ONBE(Est. OnBl.),ONBL(OnBl.)", pclTmp2, sizeof(pclTmp2), pcgConfigPath);
	ExtractItemList(pclTmp2, &olTmpArr, ',');
	ilRefCount = olTmpArr.GetSize();
	for (int i = 0; i < ilRefCount; i++)
	{
		rlRefTime.ID = i;
		ilPos = olTmpArr[i].Find("(");
		rlRefTime.Name = olTmpArr[i].Mid(ilPos + 1, olTmpArr[i].GetLength() - ilPos - 2);
		rlRefTime.Field = olTmpArr[i].Left(ilPos);
		
		ogInboundReferences.New(rlRefTime);
	}
	
	GetPrivateProfileString(ogAppName, "TIFD", "STOD(STD),ETOD(ETD),AIRB(Airborne),OFBL(OfBl.)", pclTmp2, sizeof(pclTmp2), pcgConfigPath);
	ExtractItemList(pclTmp2, &olTmpArr, ',');
	ilRefCount = olTmpArr.GetSize();
	for (i = 0; i < ilRefCount; i++)
	{
		rlRefTime.ID = i;
		ilPos = olTmpArr[i].Find("(");
		rlRefTime.Name = olTmpArr[i].Mid(ilPos + 1, olTmpArr[i].GetLength() - ilPos - 2);
		rlRefTime.Field = olTmpArr[i].Left(ilPos);
		
		ogOutboundReferences.New(rlRefTime);
	}

//--- get initialization parameters for CCSCedaData
	GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof(pcgTableExt), pcgConfigPath);
	GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "ZRH", pcgHome, sizeof(pcgHome), pcgConfigPath);
	
	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pcgHome);
	strcpy(CCSCedaData::pcmApplName, ogAppName);


//---	commandline parameters
	ogCmdLineArgsArray.RemoveAll();
	CString olCmdLine =  LPCTSTR(m_lpCmdLine);
	if(olCmdLine.GetLength() == 0)	// set defaults 
	{
		//default for REGELWERK
		ogCmdLineArgsArray.Add(ogAppName);
		ogCmdLineArgsArray.Add("");
		ogCmdLineArgsArray.Add("");
	}
	else
	{
		int ilParamCount = ExtractItemList(olCmdLine,&ogCmdLineArgsArray, ' ');
		if(ilParamCount != 3)
		{
			MessageBox(NULL,GetString(1487), GetString(AFX_IDS_APP_TITLE), MB_ICONERROR);
			return FALSE;
		}
	}

	VersionInfo olInfo;
	VersionInfo::GetVersionInfo(NULL,olInfo);
	CCSCedaData::bmVersCheck = true;
	strcpy(CCSCedaData::pcmVersion, olInfo.omFileVersion);
	strcpy(CCSCedaData::pcmInternalBuild, olInfo.omPrivateBuild);

//--- comm handler initialization
	ogCommHandler.SetAppName(ogAppName);
    
	bool  blErr  = ogCommHandler.Initialize();
	int   ilErr = 0;

    if (blErr != true)  
	{
		AfxMessageBox(ogCommHandler.LastError());
        return FALSE;
	}

	ogBCD.SetTableExtension(pcgTableExt);
	
//--- read text strings 
	/*
	GetPrivateProfileString("GLOBAL", "LANGUAGE", "", pclTmp, sizeof(pclTmp), pcgConfigPath);
	if ( strlen ( pclTmp ) >= 2 )
	{
		CString olLang = pclTmp;
		char    pclWhere[41];
		olLang = olLang.Left(2);
		strupr ( pclTmp );
		if ( strstr ( pclTmp, "TEST" ) )
			bgUseResourceStrings = false;
		
		ogCfgData.ReadUfisCedaConfig(); 

		ogBCD.SetObject("TXT");
		sprintf ( pclWhere, "WHERE COCO='%s'", olLang );
		ogBCD.Read("TXT",pclWhere);
		ogBCD.AddKeyMap("TXT", "STID");
		int ilRecAnz = ogBCD.GetDataCount ( "TXT" );
	}
	*/
	ogCfgData.ReadUfisCedaConfig(); 

	//  text-Tabelle Lesen
	char	pclDBParam[11];
	CString olErrorTxt;
	GetPrivateProfileString("GLOBAL", "LANGUAGE", "", pclTmp, sizeof(pclTmp), pcgConfigPath);
   	GetPrivateProfileString(ogAppName, "DBParam", "0", pclDBParam, sizeof pclDBParam, pcgConfigPath);
	bool blLangInitOk = ogGUILng->MemberInit(&olErrorTxt, "XXX", "RULE_AFT", pcgHome, 
											 pclTmp, pclDBParam );

// --- make sure program's running only once
	if (IsAlreadyRunning() == true)
	{
		MessageBox(NULL,"The program is running already","Error",MB_OK);
		ExitProcess(0);
	} 
	
//--- Help initialization
	if (!AatHelp::Initialize(ogAppName))
	{
		CString olErrorMsg;
		AatHelp::State elErrorState;
		elErrorState = AatHelp::GetLastError(olErrorMsg);
		MessageBox(NULL,olErrorMsg,"Error",MB_OK);
	}

	// templates (Vorlagen)
	blErr = ogBCD.SetObject("TPL"/*, "URNO,DALO,EVOR,FLDA,FLDD,FLDT,FISU,RELX,TNAM,TPST"*/);
    ilErr = ogBCD.Read("TPL", "where TPST != '2' and APPL like 'RUL%'"); // $$$$$
			ogBCD.SetDdxType("TPL", "IRT", TPL_NEW);
			ogBCD.SetDdxType("TPL", "URT", TPL_CHANGE);
			ogBCD.SetDdxType("TPL", "DRT", TPL_DELETE);

	//--- Login Dialog
	bool blAutomaticStartUp = false;
#if	0

	if(ogCmdLineArgsArray.GetAt(0) != ogAppName)
	{
		if(olLoginDlg.Login(ogCmdLineArgsArray.GetAt(1),ogCmdLineArgsArray.GetAt(2)) == true)
		{
			blAutomaticStartUp = true;
		}
	}
	
	if (blAutomaticStartUp == false)
	{
		if( olLoginDlg.DoModal() != IDCANCEL)
		{
			if(ogPrivList.GetStat("InitModu") == '1')
			{
				RegisterDlg olRegisterDlg;
				if (olRegisterDlg.DoModal() != IDOK)
				{
					return FALSE;
				}
			}
		}
		else
		{
			return FALSE;
		} 	
	}
#else
	CDialog olDummyDlg;	
	olDummyDlg.Create(IDD_WAIT_DIALOG,NULL);
	CAatLogin olLoginCtrl;
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetApplicationName("REGELWERK");
	olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());

	if(ogCmdLineArgsArray.GetAt(0) != ogAppName)
	{
		if(olLoginCtrl.DoLoginSilentMode(ogCmdLineArgsArray.GetAt(1),ogCmdLineArgsArray.GetAt(2)) == "OK")
		{
			blAutomaticStartUp = true;
		}
	}
	
	if (blAutomaticStartUp == false)
	{

		if (olLoginCtrl.ShowLoginDialog() != "OK")
		{
			olDummyDlg.DestroyWindow();
			return FALSE;
		}
	}

	ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
	strcpy(pcgUser, olLoginCtrl.GetUserName_());
	strcpy(pcgPasswd, olLoginCtrl.GetUserPassword());

	strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);
	ogBasicData.omUserID = pcgUser;

	ogPrivList.Add(olLoginCtrl.GetPrivilegList());

	olDummyDlg.DestroyWindow();

#endif

//--- read table data into global object
    //make StartDlg
	StartDlg *polDlg = new StartDlg;
	
	// SYS (SYSTAB)
	//polDlg->AddString(GetString(1456));
    blErr = ogBCD.SetObject("SYS", "URNO,FELE,FINA,TANA");
 	//  ogBCD.AddKeyMap("SYS", "TANA");
 	//  ogBCD.AddKeyMap("SYS", "FINA");
	//	ogD.SetObject("SYS", "URNO,FELR (template source)
	polDlg->AddString(GetString(1443));
    blErr = ogBCD.SetObject("TSR"/*, "URNO,BFLD,BTAB,EXTE,FIRE,FORM,NAME,OPER,RFLD,RTAB,TEXT,TYPE"*/);
    ogBCD.AddKeyMap("TSR", "BTAB");
	ogBCD.AddKeyMap("TSR", "BFLD");
    ilErr = ogBCD.Read("TSR");
	//ogBCD.AddKeyMap("TSR", "URNO");
	//ogBCD.AddKeyMap("TSR", "RTAB");
	//ogBCD.AddKeyMap("TSR", "RFLD");
	//ogBCD.AddKeyMap("TSR", "EXTE");	

	//--- load all tables referred to by TSR and ALO
		// This is how I do it: I use a CCSPtrArray with REFTABLEs {CString olRtab, CStrArray Fields}
		// If TSR.RTAB isn't empty, I look if that table name (RTAB) is already in my PtrArray. 
		// If so, I look through the Fields-Array to see if I already gathered the field and add it to the StringArray if
		// necessary.
		// If not, I add a new struct with the table and write the field as first String to the StringArray
		// Besides, the table names will be used to register the necessary DDX functions in our main window later on

	// do not make choice lists if TSR says no-no
	// if (olFormat == CString("NONE"))
	//	continue;

	CString olRtab, olRfld, olBtab, olCompare;
	bool blLoad;

	for (i = 0; i < ogBCD.GetDataCount("TSR"); i++)
	{
		// added, MNE 010208
		blLoad = (ogBCD.GetField("TSR", i, "FORM") == "NONE") ? false : true;
		// end MNE

		olRtab = ogBCD.GetField("TSR", i, "RTAB");
		olRfld = ogBCD.GetField("TSR", i, "RFLD");
		olBtab = ogBCD.GetField("TSR", i, "BTAB");
		olBtab = ogBCD.GetField("TSR", i, "BTAB");

		if (olBtab == olRtab)
		{				//  hag991207 und tsc:  sonst h�ngt sich das 
			continue;	//  System mit BTab=Rtab='AFT' auf
		}

		if (olRtab.IsEmpty() == false)
		{
			bool blTabFound = false;
			bool blFldFound = false;
			for (int j = 0; j < ogRefTables.GetSize(); j++)
			{
				if (olRtab == ogRefTables[j].RTAB)
				{
					blTabFound = true;
					for (int k = 0; k < ogRefTables[j].Fields.GetSize(); k++)
					{
						olCompare = ogRefTables[j].Fields.GetAt(k);
						if (olRfld == olCompare)
						{
							blFldFound = true;
							break;
						}
					}

					if (blFldFound == false)
					{
						ogRefTables[j].Fields.Add(olRfld);
						ogRefTables[j].bLoad = blLoad;
					}
				}
			}

			if (blTabFound == false)
			{
				REFTABLE *polRefTab = new REFTABLE;
				polRefTab->RTAB = olRtab;
				polRefTab->Fields.Add(olRfld);
				polRefTab->bLoad = blLoad;
				ogRefTables.Add(polRefTab);
			}
		}
	}

	if ( DoesTableExist ( "EQT" ) )
	{
		polDlg->AddString(GetString(IDS_EQUIPMENT_TYPES));
		if ( ogBCD.SetObject("EQT" ) )
		{
			ilErr = ogBCD.Read("EQT" );
			igEqtNameIdx = ogBCD.GetFieldIndex ( "EQT", "NAME" );
			igEqtUrnoIdx = ogBCD.GetFieldIndex ( "EQT", "URNO" );
		}
	}

	// ALO (allocation units)
	polDlg->AddString(GetString(1514));
	blErr = ogBCD.SetObject("ALO"/*, "URNO,ALOC,ALOD,ALOT,REFT"*/);
	ilErr = ogBCD.Read("ALO", "WHERE ALOT='0'" );
	//		ogBCD.AddKeyMap("ALO", "URNO");
			ogBCD.SetDdxType("ALO", "IRT", ALO_NEW);
			ogBCD.SetDdxType("ALO", "URT", ALO_CHANGE);
			ogBCD.SetDdxType("ALO", "DRT", ALO_DELETE);
	
	// load all tables referred to in ALO
	CString olReft, olTab, olField, olArrTab, olArrField, olSort/*hag990831*/;
	int ilAloCount = ogBCD.GetDataCount("ALO");
	for (i = 0; i < ilAloCount; i++)
	{
		olReft = ogBCD.GetField("ALO", i, "REFT");
		if (olReft.GetLength() == 8)
		{
			olTab = olReft.Left(3);
			olField = olReft.Right(4);
		}

		bool blTabFound = false;
		bool blFieldFound = false;
		int ilRefTabCount = ogRefTables.GetSize();
		for (int j = 0; j < ilRefTabCount; j++)
		{	
			olArrTab = ogRefTables[j].RTAB;
			if (olArrTab == olTab)
			{
				blTabFound = true;
				for (int k = 0; k < ogRefTables[j].Fields.GetSize(); k++)
				{
					olArrField = ogRefTables[j].Fields.GetAt(k);
					if (olArrField == olField)
					{
						blFieldFound = true;
						k = ogRefTables[j].Fields.GetSize();
					}
				}
				
				if (blFieldFound == false)
				{
					ogRefTables[j].Fields.Add(olField);	
				}
				j = ilRefTabCount;	// break loop now
			}
		}

		if (blTabFound == false)
		{
			REFTABLE *polRefTab = new REFTABLE;
			polRefTab->RTAB = olTab;
			polRefTab->Fields.Add(olField);
			ogRefTables.Add(polRefTab);
		}
	}
	
	// read all referred tables (TSR and ALO) into memory
	for (i = 0; i < ogRefTables.GetSize(); i++)
	{
		if (ogRefTables[i].bLoad != false)
		{
			CString olFieldList, olTmp;
			char    pclEntry[21], pclInfoField[11];
			bool blIsUrno = false;
			bool blAddInfoField = false;

			olWhere.Empty();
			sprintf ( pclEntry, "%s_Info", ogRefTables[i].RTAB );
			GetPrivateProfileString ( ogAppName, pclEntry, "", pclInfoField, 11, 
									  pcgConfigPath);
			//  wenn Info-feld f�r diese Tabelle eingetragen (d.h. <> "")
			if ( pclInfoField[0] )
				blAddInfoField = true;
			for (int j = 0; j < ogRefTables[i].Fields.GetSize(); j++)
			{
				olTmp = ogRefTables[i].Fields.GetAt(j); 
				olFieldList += olTmp;
				if (olTmp == CString("URNO"))
				{ 
					blIsUrno = true;
				}
				if ( blAddInfoField && ( olTmp == CString(pclInfoField) ) )
					blAddInfoField = false;
			}

			if (blIsUrno == false)
			{
				olFieldList += CString("URNO");
			}
			if ( blAddInfoField )
				olFieldList += CString(pclInfoField);
				
			blErr = ogBCD.SetObject(ogRefTables[i].RTAB, olFieldList);
			SetWhereValidString ( ogRefTables[i].RTAB, olWhere );
			ogBCD.Read(ogRefTables[i].RTAB, olWhere);
			olSort = ogRefTables[i].Fields[0] ;	//  hag990831
			if ( !olSort.IsEmpty() )
			{
				olSort += "+";
				ogBCD.SetSort(ogRefTables[i].RTAB, olSort, true );
			}
			//ogBCD.AddKeyMap(ogRefTables[i].RTAB, "URNO");
			polDlg->AddString(GetString(1451) + CString (" ") + ogRefTables[i].RTAB);

		}
	}

	polDlg->AddString(GetString(1451) + CString (" ") + "CCC");
	blErr = ogBCD.SetObject("CCC");
    ilErr = ogBCD.Read("CCC");
	
	// dynamic groups
	polDlg->AddString(GetString(1438));
	blErr = ogBCD.SetObject("DGR"/*, "URNO,EXPR,REFT,GNAM,UTPL"*/);
    ilErr = ogBCD.Read("DGR");
	//		ogBCD.AddKeyMap("DGR", "URNO");
			ogBCD.SetDdxType("DGR", "IRT", DGR_NEW);
			ogBCD.SetDdxType("DGR", "URT", DGR_CHANGE);
			ogBCD.SetDdxType("DGR", "DRT", DGR_DELETE);

	// services (Leistungen)
	polDlg->AddString(GetString(1440));
	blErr = ogBCD.SetObject("SER"/*, "URNO,FDUT,FFIS,FISU,FSDT,FSET,FSUT,FWTF,FWTT,RTWF,RTWT,SDUT,SEAX,SECO,SEER,SEHC,SEIV,SETY,SNAM,SSAP,SSDT,SSNM,SSUT,SWTF,SWTT,VAFR,VATO"*/);
    ilErr = ogBCD.Read("SER");
	//		ogBCD.AddKeyMap("SER", "URNO");
			ogBCD.SetDdxType("SER", "IRT", SER_NEW);
			ogBCD.SetDdxType("SER", "URT", SER_CHANGE);
			ogBCD.SetDdxType("SER", "DRT", SER_DELETE);
	
	// services catalog functions
	blErr = ogBCD.SetObject("SEF"/*, "URNO,FCCO,FCNO,GTAB,PRIO,USRV,UPFC"*/);
	ogBCD.AddKeyMap("SEF", "USRV");
    ilErr = ogBCD.Read("SEF");
	//		ogBCD.AddKeyMap("SEF", "URNO");
	//		ogBCD.AddKeyMap("SEF", "FCCO");
	
	// services catalog qualifications
	blErr = ogBCD.SetObject("SEQ"/*, "URNO,USRV,UPER,QUCO,QUNO,PRIO,GTAB"*/);
	ogBCD.AddKeyMap("SEQ", "USRV");
    ilErr = ogBCD.Read("SEQ");
	//		ogBCD.AddKeyMap("SEQ", "URNO");
	//		ogBCD.AddKeyMap("SEQ", "QUCO");

	// services catalog qualifications assignments
	blErr = ogBCD.SetObject("SFQ", "URNO,USEF,USEQ");
	ogBCD.AddKeyMap("SEQ", "USEQ");
    ilErr = ogBCD.Read("SFQ");
	//		ogBCD.AddKeyMap("SEQ", "USEF");
	//		ogBCD.AddKeyMap("SEQ", "USEQ");
	
	// services catalog equipment
	blErr = ogBCD.SetObject("SEE"/*, "URNO,USRV,UEQU,GTAB,EQCO,EQNO,PRIO"*/);
	ogBCD.AddKeyMap("SEE", "USRV");
    ilErr = ogBCD.Read("SEE");
	//		ogBCD.AddKeyMap("SEE", "URNO");
	//		ogBCD.AddKeyMap("SEE", "EQCO");
	
	// services catalog locations
	blErr = ogBCD.SetObject("SEL"/*, "URNO,USRV,REFT,RLOC,LONO,PRIO,GTAB"*/);
	ogBCD.AddKeyMap("SEL", "USRV");
    ilErr = ogBCD.Read("SEL");
	//		ogBCD.AddKeyMap("SEL", "URNO");
	//		ogBCD.AddKeyMap("SEL", "RLOC");

	// functions
	polDlg->AddString(GetString(1464));
	blErr = ogBCD.SetObject("PFC"/*, "URNO,DPTC,FCTC,FCTN,PRFL"*/);
	ogBCD.AddKeyMap("PFC", "FCTC");
    ilErr = ogBCD.Read("PFC");
	if ( !ilErr )									//hag990831
		ogBCD.SetSort ("PFC", "FCTC+", true );
	//ogBCD.AddKeyMap("PFC", "URNO");

	// qualifications
	polDlg->AddString(GetString(1465));
	blErr = ogBCD.SetObject("PER"/*, "URNO,PRFL,PRMC,PRMN"*/);
	ogBCD.AddKeyMap("PER", "PRMC");
    ilErr = ogBCD.Read("PER");
	if ( !ilErr )									//hag990831
		ogBCD.SetSort ("PER", "PRMC+", true );
	//ogBCD.AddKeyMap("PER", "URNO");

	// equipment numbers
	polDlg->AddString(GetString(EQU_HEADER));
	blErr = ogBCD.SetObject("EQU");
    ogBCD.AddKeyMap("EQU", "GKEY");
	ogBCD.AddKeyMap("EQU", "GCDE");
	ilErr = ogBCD.Read("EQU");
	if ( !ilErr )									
		ogBCD.SetSort ("EQU", "GCDE+", true );
		
	// static groups
	polDlg->AddString(GetString(1439));
	blErr = ogBCD.SetObject("SGR"/*, "URNO,APPL,FLDN,GRDS,GRPN,GRSN,PRFL,TABN,UGTY"*/);
	ogBCD.AddKeyMap("SGR", "GRPN");
	ogBCD.AddKeyMap("SGR", "TABN");
    ilErr = ogBCD.Read("SGR","WHERE APPL='RULE_AFT'");
	if ( !ilErr )									//hag990831
		ogBCD.SetSort ("SGR", "GRPN+", true );
	//ogBCD.AddKeyMap("SGR", "URNO");
	ogBCD.SetDdxType("SGR", "IRT", SGR_NEW);
	ogBCD.SetDdxType("SGR", "URT", SGR_CHANGE);
	ogBCD.SetDdxType("SGR", "DRT", SGR_DELETE);

	// static group members
	// blErr = ogBCD.SetObject("SGM", "URNO,PRFL,USGR,VALU");
	blErr = ogBCD.SetObject("SGM", "USGR,UVAL,TABN,URNO" );
	ogBCD.AddKeyMap("SGM", "USGR");
	olWhere = "where USGR in (select urno from SGRTAB where APPL='RULE_AFT')";
    ilErr = ogBCD.Read("SGM", olWhere);
	//		ogBCD.AddKeyMap("SGM", "URNO");
			ogBCD.SetDdxType("SGM", "IRT", SGM_NEW);
			ogBCD.SetDdxType("SGM", "URT", SGM_CHANGE);
			ogBCD.SetDdxType("SGM", "DRT", SGM_DELETE);

	// validities (G�ltigkeiten)
	polDlg->AddString(GetString(1446));
	blErr = ogBCD.SetObject("VAL", "", false);
	ogBCD.AddKeyMap("VAL", "UVAL");
	ogBCD.AddKeyMap("VAL", "VTYP");
	olWhere = "where appl='RULE_AFT'";
    ilErr = ogBCD.Read("VAL", olWhere );
	//		ogBCD.AddKeyMap("VAL", "URNO");
			ogBCD.SetDdxType("VAL", "IRT", VAL_NEW);
			ogBCD.SetDdxType("VAL", "URT", VAL_CHANGE);
			ogBCD.SetDdxType("VAL", "DRT", VAL_DELETE);
	
	// rules
	polDlg->AddString(GetString(1442));
    //blErr = ogBCD.SetObject("RUE", "URNO,APPL,CDAT,EVRM,EVTA,EVTD,EVTT,EVTY,EXCL,FISU,FSPL,LSTU,MAXT,MIST,PRIO,REMA,RUNA,RUSN,RUST,RUTY,UARC,USEC,USEU,UTPL");
	blErr = ogBCD.SetObject("RUE", "", false ); // mne 001018
	ogBCD.AddKeyMap("RUE", "UTPL");
	//olWhere = "where RUST != '2'";
	olWhere.Format ( "where UTPL in (select URNO from TPL%s where TPST != '2' and APPL like 'RUL%%')", pcgTableExt );
    ilErr = ogBCD.Read("RUE", olWhere );
	//		ogBCD.AddKeyMap("RUE", "URNO");
			ogBCD.SetDdxType("RUE", "IRT", RUE_NEW);
			ogBCD.SetDdxType("RUE", "URT", RUE_CHANGE);
			ogBCD.SetDdxType("RUE", "DRT", RUE_DELETE);

	// rule demands  (Bedarfe)
	polDlg->AddString(GetString(1441));
	blErr = ogBCD.SetObject("RUD", "", false );
	ogBCD.AddKeyMap("RUD","URUE" );
	ogBCD.AddKeyMap("RUD","UCRU" );

	//  Calling SetLogicalFields() in SetLogicalTableFields() fails for the following
	//	tables when SetObject wasn't called before
	blErr = ogBCD.SetObject("RPF", "", false );		//  hag990830
	ogBCD.AddKeyMap("RPF", "URUD");

    blErr = ogBCD.SetObject("RPQ", "", false );		//  hag990830
	ogBCD.AddKeyMap("RPQ", "URUD");
	ogBCD.AddKeyMap("RPQ", "UDGR");

    blErr = ogBCD.SetObject("REQ", "", false );		//  hag990830
	ogBCD.AddKeyMap("REQ", "URUD");

    blErr = ogBCD.SetObject("RLO", "", false );	//  hag990830
	ogBCD.AddKeyMap("RLO", "URUD");
    
	//--- Add Logical objects
	
	olWhere = "where URUE in (select urno from RUETAB where RUST != '2')";
	ilErr = ogBCD.Read("RUD", olWhere );
	//		ogBCD.AddKeyMap("RUD", "URNO");
			ogBCD.SetDdxType("RUD", "IRT", RUD_NEW);
			ogBCD.SetDdxType("RUD", "URT", RUD_CHANGE);
			ogBCD.SetDdxType("RUD", "DRT", RUD_DELETE);
	
	// rules personnel functions
	olWhere = "where urud in (SELECT URNO FROM RUDTAB WHERE (URUE in (select urno from RUETAB where RUST != '2' and utpl in (select urno from tpltab where rust!='2') )))";
	ilErr = ogBCD.Read("RPF", olWhere );
	//		ogBCD.AddKeyMap("RPF", "URNO");
	//		ogBCD.AddKeyMap("RPF", "FCCO");
	
	// rules equipment
	ilErr = ogBCD.Read("REQ", olWhere );
	//		ogBCD.AddKeyMap("REQ", "URNO");
	//		ogBCD.AddKeyMap("REQ", "EQCO");
	
	// rules locations
	ilErr = ogBCD.Read("RLO", olWhere );
			ogBCD.AddKeyMap("RLO", "URNO");
			ogBCD.AddKeyMap("RLO", "RLOC");

	olWhere += " or udgr in (SELECT UDGR FROM RUDTAB WHERE (URUE in (select urno from RUETAB where RUST != '2' and utpl in (select urno from tpltab where rust!='2') )))";
	// rules personnel qualifications
	ilErr = ogBCD.Read("RPQ", olWhere );
	//		ogBCD.AddKeyMap("RPQ", "URNO");
	//		ogBCD.AddKeyMap("RPQ", "QUCO");
	
//--- Add Logical objects
	// RUD_TMP
	polDlg->AddString(GetString(1458));
	CCSPtrArray <FieldSet> olFieldSets;
	ogBCD.CloneFieldSet("RUD", olFieldSets);
	ogBCD.SetObject("RUD_TMP", olFieldSets);
	//ogBCD.AddKeyMap("RUD_TMP", "URNO");
	ogBCD.SetDdxType("RUD_TMP", "IRT", RUD_TMP_NEW);
	ogBCD.SetDdxType("RUD_TMP", "URT", RUD_TMP_CHANGE);
	ogBCD.SetDdxType("RUD_TMP", "DRT", RUD_TMP_DELETE);
	olFieldSets.DeleteAll();
	
	// PRF_TMP
	ogBCD.CloneFieldSet("RPF", olFieldSets);
	ogBCD.SetObject("RPF_TMP", olFieldSets);
	//ogBCD.AddKeyMap("RPF_TMP", "URNO");
	ogBCD.SetDdxType("RPF_TMP", "IRT", RPF_TMP_NEW);
	ogBCD.SetDdxType("RPF_TMP", "URT", RPF_TMP_CHANGE);
	ogBCD.SetDdxType("RPF_TMP", "DRT", RPF_TMP_DELETE);
	olFieldSets.DeleteAll();

	// PRQ_TMP
	ogBCD.CloneFieldSet("RPQ", olFieldSets);
	ogBCD.SetObject("RPQ_TMP", olFieldSets);
	//ogBCD.AddKeyMap("RPQ_TMP", "URNO");
	ogBCD.SetDdxType("RPQ_TMP", "IRT", RPQ_TMP_NEW);
	ogBCD.SetDdxType("RPQ_TMP", "URT", RPQ_TMP_CHANGE);
	ogBCD.SetDdxType("RPQ_TMP", "DRT", RPQ_TMP_DELETE);
	olFieldSets.DeleteAll();

	// RLO_TMP
	ogBCD.CloneFieldSet("RLO", olFieldSets);
	ogBCD.SetObject("RLO_TMP", olFieldSets);
	//ogBCD.AddKeyMap("RLO_TMP", "URNO");
	ogBCD.SetDdxType("RLO_TMP", "IRT", RLO_TMP_NEW);
	ogBCD.SetDdxType("RLO_TMP", "URT", RLO_TMP_CHANGE);
	ogBCD.SetDdxType("RLO_TMP", "DRT", RLO_TMP_DELETE);
	olFieldSets.DeleteAll();

	// REQ_TMP
	ogBCD.CloneFieldSet("REQ", olFieldSets);
	ogBCD.SetObject("REQ_TMP", olFieldSets);
	ogBCD.SetDdxType("REQ_TMP", "IRT", REQ_TMP_NEW);
	ogBCD.SetDdxType("REQ_TMP", "URT", REQ_TMP_CHANGE);
	ogBCD.SetDdxType("REQ_TMP", "DRT", REQ_TMP_DELETE);
	olFieldSets.DeleteAll();
	
	// RUE_TMP
	ogBCD.CloneFieldSet("RUE", olFieldSets);
	ogBCD.SetObject("RUE_TMP", olFieldSets);
	olFieldSets.DeleteAll();

	//--- do some preparations for our global data object
	SetGlobalFieldIndices();

	// make choice lists for comboboxes (call this _AFTER_ SetGlobalFieldIndices() - the indices will be used herein)
	polDlg->AddString(GetString(1459));
	MakeChoiceLists();		// choice lists for main condition grids
	//see below  CreateResourceChoiceLists();	// choice lists for resource detail grids
	
	//--- read data not contained in global data object 
	//		 The ogCfgData object _MUST_ be initialized _AFTER_ the login dialog in order to 
	//		 be able to read views from the VCD table. The reason herefore is that the views are 
	//	     user-specific and the user's name is not known until the user has logged in.
	polDlg->AddString(GetString(1447));
	ogCfgData.Init();
	ogCfgData.ReadCfgData();
	ogCfgData.SetCfgData();
	
	//--- initialize data object to hold urnos to update
	ogUpdItems.SetAt("RUD", &ogRudItems);
	ogUpdItems.SetAt("RPF", &ogRpfItems);
	ogUpdItems.SetAt("RPQ", &ogRpqItems);
	ogUpdItems.SetAt("RLO", &ogRloItems);
	ogUpdItems.SetAt("REQ", &ogReqItems);

//--- some more initialization stuff

	//--- Register broadcasts
	polDlg->AddString(GetString(1460));
	CString olTableName;
	CString olTableExt = CString(pcgTableExt);
	olTableName = CString("SER") + olTableExt;

	//--- set time diff local to UTC
	polDlg->AddString(GetString(1461));
	ogBasicData.SetLocalDiff();
	CCSCedaData::omLocalDiff1 = ogBasicData.GetLocalDiff1();
	CCSCedaData::omLocalDiff2 = ogBasicData.GetLocalDiff2();
	CCSCedaData::omTich = ogBasicData.GetTich(); 

	//--- Initialize view
	//		InitDefaultView() _MUST_ be placed _AFTER_ reading data into ogCfgData. When filling ogCfgData
	//		all data will be removed, i.e. a default view created before that would then be destroyed again.
	InitDefaultView();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	//--- delete dialog "Loading Data from Database"
	delete polDlg;

	//--- Parse command line for standard shell commands, DDE, file open
	  

	//--- Dispatch commands specified on the command line
	//	  MNE: Don't call ParseCommandLine() here ! Otherwise, program tries to parse the parameters given
	//		   on automatic startup, which will fail !
	CCommandLineInfo cmdInfo;  
	if (!ProcessShellCommand(cmdInfo))
	  	   return FALSE;
	  
	//-- delete maximize button on main window
	m_pMainWnd->ModifyStyle(WS_MAXIMIZEBOX, 0);

	//-- show window
	// set main window's caption
	m_pMainWnd->SetWindowText(GetString(1414));
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	//
	CreateResourceChoiceLists();	// put here to be sure that templates combobox has been filled

	if ( CheckForExpirationToWarn () )
	{
		CString olText, olForm;

		UINT  ilWarnBevor = GetPrivateProfileInt ( ogAppName, "EXPIRATION_WARNING", 0, pcgConfigPath);
		olForm = GetString(IDS_EXPIRATION_WARN);
		if ( !GetString(IDS_STRING1522).IsEmpty() ) 
			olText.Format ( olForm, ilWarnBevor );
		else
			olText = GetString(IDS_STRING1522);
		int ilRet = m_pMainWnd->MessageBox ( olText, "",  MB_ICONQUESTION|MB_YESNO );

		if ( ilRet == IDYES )
		{	
			RulesList olDialog ( true, m_pMainWnd );
			if ( olDialog.DoModal() == IDOK )
			{
				CString olRueUrno = olDialog.omDataStringRuleUrno;
				CString olTplName = olDialog.omTemplateName;

				((CMainFrame*)m_pMainWnd)->DoLoadRule ( olRueUrno, olTplName );
			}
		}

	}
	GetPrivateProfileString ( ogAppName, "ERRORFILE", CCSLog::GetTmpPath("\\InconsistentRules.txt"), 
							  pclTmp, 128, pcgConfigPath);
	GetPrivateProfileString ( ogAppName, "CHECKRULES", "YES", pclTmp3, 4, pcgConfigPath);
	
	
	if ( strcmp ( pclTmp3, "NO" ) && ogDataSet.CheckRules ( pclTmp ) )
	{
		//  Show File "C:\\TMP\\InconsistentRules.txt" 
		CString olTxt = GetString ( IDS_INCONSISTENCY_TXT );
		AfxMessageBox ( olTxt );

		char pclArgStr[256], clEditor[128];

		_flushall(); // flush all streams before system call

		GetPrivateProfileString ( ogAppName, "EDITOR", "NOTEPAD.EXE", 
								  clEditor, 128, pcgConfigPath);
		sprintf(pclArgStr, "%s %s", clEditor, pclTmp );	
		
		// initialize STARTUPINFO struct
		STARTUPINFO Info;
		ZeroMemory(&Info, sizeof(Info));
		Info.dwFlags = STARTF_FORCEONFEEDBACK; // changes mouse cursor 
		Info.cb = sizeof(Info);

		// initialize PROCESS_INFORMATION struct
		PROCESS_INFORMATION *pProcInfo = new PROCESS_INFORMATION;
		ZeroMemory(pProcInfo, sizeof(pProcInfo));

		BOOL ilRet = CreateProcess(NULL,pclArgStr,NULL,NULL,FALSE,CREATE_DEFAULT_ERROR_MODE,NULL,NULL,&Info,pProcInfo);
		if (ilRet == FALSE)
		{
			DWORD dwErr = GetLastError();
			char pclErrMsg[1024];
			FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,NULL,dwErr,0,pclTmp2,sizeof(pclTmp2),NULL);
			sprintf(pclErrMsg, "%s %s %s\n%s", GetString(300), clEditor, GetString(301), pclTmp2);
			AfxMessageBox(pclErrMsg);
		}
		else
		{
			DWORD dwPID = pProcInfo->dwProcessId;
			theApp.AddProcID(dwPID);
		}

		delete pProcInfo;
	}
	
	// MNE: THIS IS ONLY FOR MEM LEAK DETECTING AND MUST BE TAKEN OUT FOR DELIVERY VERSIONS !!!
	// ::FreeLibrary(hMod);
	// END MNE

	return TRUE;
}
//------------------------------------------------------------------------



void CRegelWerkApp::MakeChoiceLists()
{
	CCS_TRY

	CString		olRefTable;
	CString		olRefFieldName;
	CString		olBaseTable;
	CString		olBaseFieldName;
	CString		olFormat;
	CString		olChoiceList;	
	RecordSet	olRecord;
	char		pclEntry[21], pclInfoField[11];

	int ilCount = ogBCD.GetDataCount("TSR");
	for (int i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("TSR", i, olRecord);
		olRefTable = olRecord.Values[igTsrRtabIdx];
		olFormat = olRecord.Values[igTsrFormIdx];


		// mne test
		// if (olRecord.Values[igTsrBfldIdx] == "ANNX")
		// 	int haltan = 1;
		// end test


		// do not make choice lists if TSR says no-no
		if (olFormat == CString("NONE"))
			continue;
		
		if(!olRefTable.IsEmpty()) 
		{	  
			 olRefFieldName = olRecord.Values[igTsrRfldIdx];
			 olBaseTable = olRecord.Values[igTsrBtabIdx];
			 olBaseFieldName = olRecord.Values[igTsrBfldIdx];
			 if ( olBaseTable == olRefTable )
				 continue;

			 //--- ChoiceList anlegen mit Daten aus Referenz-Tabelle
			 CedaObject *prlObject;
			 int ilSize = 0;

			 if (ogBCD.omObjectMap.Lookup((LPCSTR)olRefTable, (void *&)prlObject) == TRUE)
			 {
				 CString olRefFieldData;
				 int ilFieldIdx = ogBCD.GetFieldIndex(olRefTable, olRefFieldName);
				 ilSize = prlObject->Data.GetSize();
				 
				 olChoiceList.Empty();
				 for(int ilLC = 0; ilLC < ilSize; ilLC++)
				 {
 					olRefFieldData = prlObject->Data[ilLC].Values[ilFieldIdx];
					
					if((!olRefFieldData.IsEmpty()))
					{
						if (!IsAlreadyInList(olChoiceList, olRefFieldData))
							olChoiceList += olRefFieldData + "\n";						
					}
				 }
			 }

			 CHOICE_LISTS olChoice;
			 olChoice.TabFieldName = olBaseTable + CString (".") + olBaseFieldName;
			 olChoice.ValueList = olChoiceList;
			 olChoice.RefTabField = olRefTable + CString (".") + olRefFieldName;
			 sprintf ( pclEntry, "%s_Info", olRefTable );
			 GetPrivateProfileString ( ogAppName, pclEntry, "", pclInfoField, 11, 
									   pcgConfigPath);
			 olChoice.ToolTipField = pclInfoField;
			 ogChoiceLists.New(olChoice);
		}
	}
	
	CCS_CATCH_ALL
}
//------------------------------------------------------------------------

bool CRegelWerkApp::IsAlreadyInList(CString opList, CString opItem)
{
	CString olItem;
	olItem = "\n" + opItem + "\n";
	if (opList.Find(olItem, 0) != -1)
	{
		return true;	// found it somewhere in the middle of the list	
	}

	olItem = opItem + "\n";
	if (opList.Left(olItem.GetLength() + 1) == olItem)
	{
		return true;	// found it at start of the list
	}

	olItem = "\n" + opItem;
	if (opList.Right(olItem.GetLength() + 1) == olItem)
	{
		return true;	// found it at end of the list
	}

	return false;
}
//------------------------------------------------------------------------

void CRegelWerkApp::SetGlobalFieldIndices()
{
	// To append a field you also need to create a global variable in CCSGLOBL

	// Attention: Not all fields we create an index for are necessarily part of ogBCD.
	//            It depends on the field lists given to the ogBCD.Read() - method in InitInstance()


	CCS_TRY
	// (ALO) allocation units
	igAloAlocIdx = ogBCD.GetFieldIndex("ALO", "ALOC");
	igAloAlodIdx = ogBCD.GetFieldIndex("ALO", "ALOD");
	igAloAlotIdx = ogBCD.GetFieldIndex("ALO", "ALOT");
	igAloReftIdx = ogBCD.GetFieldIndex("ALO", "REFT");
	igAloUrnoIdx = ogBCD.GetFieldIndex("ALO", "URNO");
	// DGR (dynamic groups)
	igDgrExprIdx = ogBCD.GetFieldIndex("DGR", "EXPR"); 
	igDgrGnamIdx = ogBCD.GetFieldIndex("DGR", "GNAM"); 
	igDgrReftIdx = ogBCD.GetFieldIndex("DGR", "REFT"); 
	igDgrUrnoIdx = ogBCD.GetFieldIndex("DGR", "URNO"); 
	igDgrUtplIdx = ogBCD.GetFieldIndex("DGR", "UTPL"); 
	// RPF (rules personnel functions)
	igRpfFccoIdx = ogBCD.GetFieldIndex("RPF", "FCCO");
	igRpfFcnoIdx = ogBCD.GetFieldIndex("RPF", "FCNO");
	igRpfPrioIdx = ogBCD.GetFieldIndex("RPF", "PRIO");
	igRpfUpfcIdx = ogBCD.GetFieldIndex("RPF", "UPFC");
	igRpfUrnoIdx = ogBCD.GetFieldIndex("RPF", "URNO");
	igRpfUrudIdx = ogBCD.GetFieldIndex("RPF", "URUD");
	igRpfGtabIdx = ogBCD.GetFieldIndex("RPF", "GTAB");		//  hag990826
	// RPQ (rules personnel qualifications)
	igRpqPrioIdx = ogBCD.GetFieldIndex("RPQ", "PRIO");
	igRpqQucoIdx = ogBCD.GetFieldIndex("RPQ", "QUCO");
	igRpqQunoIdx = ogBCD.GetFieldIndex("RPQ", "QUNO");
	igRpqUdgrIdx = ogBCD.GetFieldIndex("RPQ", "UDGR");
	igRpqUperIdx = ogBCD.GetFieldIndex("RPQ", "UPER");
	igRpqUrnoIdx = ogBCD.GetFieldIndex("RPQ", "URNO");
	igRpqUrudIdx = ogBCD.GetFieldIndex("RPQ", "URUD");
	igRpqGtabIdx = ogBCD.GetFieldIndex("RPQ", "GTAB");		//  hag990826
	// REQ (rules personnel qualifications)
	igReqEqcoIdx = ogBCD.GetFieldIndex("REQ", "EQCO");
	igReqEqnoIdx = ogBCD.GetFieldIndex("REQ", "EQNO");
	igReqPrioIdx = ogBCD.GetFieldIndex("REQ", "PRIO");
	igReqUequIdx = ogBCD.GetFieldIndex("REQ", "UEQU");
	igReqUrnoIdx = ogBCD.GetFieldIndex("REQ", "URNO");
	igReqUrudIdx = ogBCD.GetFieldIndex("REQ", "URUD");
	igReqGtabIdx = ogBCD.GetFieldIndex("REQ", "GTAB");		//  hag990826
	igReqEtypIdx = ogBCD.GetFieldIndex("REQ", "ETYP");		
	// RLO (rules personnel qualifications)
	igRloRlocIdx = ogBCD.GetFieldIndex("RLO", "RLOC");
	igRloLonoIdx = ogBCD.GetFieldIndex("RLO", "LONO");
	igRloPrioIdx = ogBCD.GetFieldIndex("RLO", "PRIO");
	igRloReftIdx = ogBCD.GetFieldIndex("RLO", "REFT");
	igRloUrnoIdx = ogBCD.GetFieldIndex("RLO", "URNO");
	igRloUrudIdx = ogBCD.GetFieldIndex("RLO", "URUD");
	igRloGtabIdx = ogBCD.GetFieldIndex("RLO", "GTAB");		//  hag990826
	// URNO,FCND,FFPD,FNCD,FOND,FSAD,LADE,LIRE,LODE,MAXD,MIND,NRES,REDE,REND,REPD,RTDB,RTDE,SDTI,SUTI,STDE,TSDB,TSDE,TSND,TSPD,TTGF,TTGT,UCRU,UDEM,UDGR,UGHC,UGHS,UNDE,UPDE,UPRO,URUE
	// RUD (Rule Demands)
	igRudAlocIdx = ogBCD.GetFieldIndex("RUD", "ALOC"); // 29
	igRudDbflIdx = ogBCD.GetFieldIndex("RUD", "DBFL");
	igRudDeflIdx = ogBCD.GetFieldIndex("RUD", "DEFL");
	igRudCoduIdx = ogBCD.GetFieldIndex("RUD", "CODU"); // 29
	igRudDbarIdx = ogBCD.GetFieldIndex("RUD", "DBAR"); //  5
	igRudDearIdx = ogBCD.GetFieldIndex("RUD", "DEAR"); //  6
	igRudDebeIdx = ogBCD.GetFieldIndex("RUD", "DEBE"); //  7
	igRudDecoIdx = ogBCD.GetFieldIndex("RUD", "DECO"); // 44
	igRudDeduIdx = ogBCD.GetFieldIndex("RUD", "DEDU"); // 13
	igRudDeenIdx = ogBCD.GetFieldIndex("RUD", "DEEN"); //  8
	igRudDideIdx = ogBCD.GetFieldIndex("RUD", "DIDE"); // 27
	igRudDindIdx = ogBCD.GetFieldIndex("RUD", "DIND"); // 28
	igRudDispIdx = ogBCD.GetFieldIndex("RUD", "DISP");
	igRudDrtyIdx = ogBCD.GetFieldIndex("RUD", "DRTY"); 
	igRudEadbIdx = ogBCD.GetFieldIndex("RUD", "EADB"); // 16
	igRudFaddIdx = ogBCD.GetFieldIndex("RUD", "FADD"); // 37
	igRudFcndIdx = ogBCD.GetFieldIndex("RUD", "FCND"); // 42
	igRudFfpdIdx = ogBCD.GetFieldIndex("RUD", "FFPD"); // 35
	igRudFncdIdx = ogBCD.GetFieldIndex("RUD", "FNCD"); // 36
	igRudFondIdx = ogBCD.GetFieldIndex("RUD", "FOND"); // 38
	igRudFsadIdx = ogBCD.GetFieldIndex("RUD", "FSAD"); // 40
	igRudLadeIdx = ogBCD.GetFieldIndex("RUD", "LADE"); // 17
	igRudLifrIdx = ogBCD.GetFieldIndex("RUD", "LIFR");
	igRudLitoIdx = ogBCD.GetFieldIndex("RUD", "LITO");

   

	

	igRudMaxcIdx = ogBCD.GetFieldIndex("RUD", "MAXC");
	igRudMincIdx = ogBCD.GetFieldIndex("RUD", "MINC");
	igRudLodeIdx = ogBCD.GetFieldIndex("RUD", "LODE"); // 30
	igRudMaxdIdx = ogBCD.GetFieldIndex("RUD", "MAXD"); // 14
	igRudMindIdx = ogBCD.GetFieldIndex("RUD", "MIND"); // 15
	igRudNresIdx = ogBCD.GetFieldIndex("RUD", "NRES"); //  4
	igRudRedeIdx = ogBCD.GetFieldIndex("RUD", "REDE"); // 31
	igRudRendIdx = ogBCD.GetFieldIndex("RUD", "REND"); // 19
	igRudRepdIdx = ogBCD.GetFieldIndex("RUD", "REPD"); // 22
	igRudRetyIdx = ogBCD.GetFieldIndex("RUD", "RETY"); // 22
	igRudRtdbIdx = ogBCD.GetFieldIndex("RUD", "RTDB"); //  9
	igRudRtdeIdx = ogBCD.GetFieldIndex("RUD", "RTDE"); // 10
	igRudSdtiIdx = ogBCD.GetFieldIndex("RUD", "SDTI"); //  3
	igRudSutiIdx = ogBCD.GetFieldIndex("RUD", "SUTI"); //  2
	igRudStdeIdx = ogBCD.GetFieldIndex("RUD", "STDE"); // 45
	igRudTsdbIdx = ogBCD.GetFieldIndex("RUD", "TSDB"); // 11
	igRudTsdeIdx = ogBCD.GetFieldIndex("RUD", "TSDE"); // 12
	igRudTsndIdx = ogBCD.GetFieldIndex("RUD", "TSND"); // 20
	igRudTspdIdx = ogBCD.GetFieldIndex("RUD", "TSPD"); // 23
	igRudTtgfIdx = ogBCD.GetFieldIndex("RUD", "TTGF"); //  1
	igRudTtgtIdx = ogBCD.GetFieldIndex("RUD", "TTGT"); //  0 
	igRudUcruIdx = ogBCD.GetFieldIndex("RUD", "UCRU"); // 24
	igRudUdemIdx = ogBCD.GetFieldIndex("RUD", "UDEM"); // 25
	igRudUdgrIdx = ogBCD.GetFieldIndex("RUD", "UDGR"); // 33
	igRudUghcIdx = ogBCD.GetFieldIndex("RUD", "UGHC"); // 43
	igRudUghsIdx = ogBCD.GetFieldIndex("RUD", "UGHS"); // 39
	igRudUlnkIdx = ogBCD.GetFieldIndex("RUD", "ULNK"); // 39
	igRudUndeIdx = ogBCD.GetFieldIndex("RUD", "UNDE"); // 18
	igRudUpdeIdx = ogBCD.GetFieldIndex("RUD", "UPDE"); // 21
	igRudUproIdx = ogBCD.GetFieldIndex("RUD", "UPRO"); // 26
	igRudUrnoIdx = ogBCD.GetFieldIndex("RUD", "URNO"); // 32
	igRudUrueIdx = ogBCD.GetFieldIndex("RUD", "URUE"); // 34
	igRudUsesIdx = ogBCD.GetFieldIndex("RUD", "USES"); // 34
	igRudReftIdx = ogBCD.GetFieldIndex("RUD", "REFT"); 
	igRudVrefIdx = ogBCD.GetFieldIndex("RUD", "VREF"); 
	igRudUtplIdx = ogBCD.GetFieldIndex("RUD", "UTPL"); 
 
	// RUE (Rule Events)
	igRueApplIdx = ogBCD.GetFieldIndex("RUE", "APPL");
	igRueCdatIdx = ogBCD.GetFieldIndex("RUE", "CDAT");
	igRueEvrmIdx = ogBCD.GetFieldIndex("RUE", "EVRM");
	igRueEvtaIdx = ogBCD.GetFieldIndex("RUE", "EVTA");
	igRueEvtdIdx = ogBCD.GetFieldIndex("RUE", "EVTD");
	igRueEvttIdx = ogBCD.GetFieldIndex("RUE", "EVTT");
	igRueEvtyIdx = ogBCD.GetFieldIndex("RUE", "EVTY");
	igRueExclIdx = ogBCD.GetFieldIndex("RUE", "EXCL");
	igRueFisuIdx = ogBCD.GetFieldIndex("RUE", "FISU");
	igRueFsplIdx = ogBCD.GetFieldIndex("RUE", "FSPL");
	igRueLstuIdx = ogBCD.GetFieldIndex("RUE", "LSTU");
	igRueMaxtIdx = ogBCD.GetFieldIndex("RUE", "MAXT");
	igRueMistIdx = ogBCD.GetFieldIndex("RUE", "MIST");
	igRuePrioIdx = ogBCD.GetFieldIndex("RUE", "PRIO");
	igRueRemaIdx = ogBCD.GetFieldIndex("RUE", "REMA");
	igRueRunaIdx = ogBCD.GetFieldIndex("RUE", "RUNA");
	igRueRusnIdx = ogBCD.GetFieldIndex("RUE", "RUSN");
	igRueRustIdx = ogBCD.GetFieldIndex("RUE", "RUST");
	igRueRutyIdx = ogBCD.GetFieldIndex("RUE", "RUTY");
	igRueUarcIdx = ogBCD.GetFieldIndex("RUE", "UARC");
	igRueUrnoIdx = ogBCD.GetFieldIndex("RUE", "URNO");
	igRueUsecIdx = ogBCD.GetFieldIndex("RUE", "USEC");
	igRueUseuIdx = ogBCD.GetFieldIndex("RUE", "USEU");
	igRueUtplIdx = ogBCD.GetFieldIndex("RUE", "UTPL");
	// SEE (service catalog equipment)
	igSeeEqcoIdx = ogBCD.GetFieldIndex("SEE", "EQCO");
	igSeeEqnoIdx = ogBCD.GetFieldIndex("SEE", "EQNO");
	igSeeEtypIdx = ogBCD.GetFieldIndex("SEE", "ETYP");
	igSeeGtabIdx = ogBCD.GetFieldIndex("SEE", "GTAB");
	igSeePrioIdx = ogBCD.GetFieldIndex("SEE", "PRIO");
	igSeeUequIdx = ogBCD.GetFieldIndex("SEE", "UEQU");
	igSeeUpfcIdx = ogBCD.GetFieldIndex("SEE", "UPFC");
	igSeeUrnoIdx = ogBCD.GetFieldIndex("SEE", "URNO");
	igSeeUsrvIdx = ogBCD.GetFieldIndex("SEE", "USRV");
	// SEF (service catalog personnel functions)
	igSefFccoIdx = ogBCD.GetFieldIndex("SEF", "FCCO");
	igSefFcnoIdx = ogBCD.GetFieldIndex("SEF", "FCNO");
	igSefGtabIdx = ogBCD.GetFieldIndex("SEF", "GTAB");
	igSefPrioIdx = ogBCD.GetFieldIndex("SEF", "PRIO");
	igSefUpfcIdx = ogBCD.GetFieldIndex("SEF", "UPFC");
	igSefUrnoIdx = ogBCD.GetFieldIndex("SEF", "URNO");
	igSefUsrvIdx = ogBCD.GetFieldIndex("SEF", "USRV");
	// SEL (service catalog locations)
	igSelGtabIdx = ogBCD.GetFieldIndex("SEL", "GTAB");
	igSelLonoIdx = ogBCD.GetFieldIndex("SEL", "LONO");
	igSelPrioIdx = ogBCD.GetFieldIndex("SEL", "PRIO");
	igSelReftIdx = ogBCD.GetFieldIndex("SEL", "REFT");
	igSelRlocIdx = ogBCD.GetFieldIndex("SEL", "RLOC");
	igSelUrnoIdx = ogBCD.GetFieldIndex("SEL", "URNO");
	igSelUsrvIdx = ogBCD.GetFieldIndex("SEL", "USRV");
	// SEQ (service catalog personnel qualifications)
	igSeqGtabIdx = ogBCD.GetFieldIndex("SEQ", "GTAB");
	igSeqPrioIdx = ogBCD.GetFieldIndex("SEQ", "PRIO");
	igSeqQucoIdx = ogBCD.GetFieldIndex("SEQ", "QUCO");
	igSeqQunoIdx = ogBCD.GetFieldIndex("SEQ", "QUNO");
	igSeqUperIdx = ogBCD.GetFieldIndex("SEQ", "UPER");
	igSeqUrnoIdx = ogBCD.GetFieldIndex("SEQ", "URNO");
	igSeqUsrvIdx = ogBCD.GetFieldIndex("SEQ", "USRV");
	// SER (Static Groups)
	igSerFdutIdx = ogBCD.GetFieldIndex("SER", "FDUT");
	igSerFfisIdx = ogBCD.GetFieldIndex("SER", "FFIS");
	igSerFisuIdx = ogBCD.GetFieldIndex("SER", "FISU");
	igSerFsdtIdx = ogBCD.GetFieldIndex("SER", "FSDT");
	igSerFsetIdx = ogBCD.GetFieldIndex("SER", "FSET");
	igSerFsutIdx = ogBCD.GetFieldIndex("SER", "FSUT");
	igSerFwtfIdx = ogBCD.GetFieldIndex("SER", "FWTF");
	igSerFwttIdx = ogBCD.GetFieldIndex("SER", "FWTT");
	igSerRtwfIdx = ogBCD.GetFieldIndex("SER", "RTWF");
	igSerRtwtIdx = ogBCD.GetFieldIndex("SER", "RTWT");
	igSerSdutIdx = ogBCD.GetFieldIndex("SER", "SDUT");
	igSerSeaxIdx = ogBCD.GetFieldIndex("SER", "SEAX");
	igSerSecoIdx = ogBCD.GetFieldIndex("SER", "SECO");
	igSerSeerIdx = ogBCD.GetFieldIndex("SER", "SEER");
	igSerSehcIdx = ogBCD.GetFieldIndex("SER", "SEHC");
	igSerSeivIdx = ogBCD.GetFieldIndex("SER", "SEIV");
	igSerSetyIdx = ogBCD.GetFieldIndex("SER", "SETY");
	igSerSnamIdx = ogBCD.GetFieldIndex("SER", "SNAM");
	igSerSsapIdx = ogBCD.GetFieldIndex("SER", "SSAP");
	igSerSsdtIdx = ogBCD.GetFieldIndex("SER", "SSDT");
//	igSerSsnmIdx = ogBCD.GetFieldIndex("SER", "SSNM");
	igSerSsutIdx = ogBCD.GetFieldIndex("SER", "SSUT");
	igSerSwtfIdx = ogBCD.GetFieldIndex("SER", "SWTF");
	igSerSwttIdx = ogBCD.GetFieldIndex("SER", "SWTT");
	igSerUrnoIdx = ogBCD.GetFieldIndex("SER", "URNO");
	igSerVafrIdx = ogBCD.GetFieldIndex("SER", "VAFR");
	igSerVatoIdx = ogBCD.GetFieldIndex("SER", "VATO");
	// SGR (Static Groups)
	igSgrApplIdx = ogBCD.GetFieldIndex("SGR", "APPL");
	igSgrFldnIdx = ogBCD.GetFieldIndex("SGR", "FLDN");
	igSgrGrdsIdx = ogBCD.GetFieldIndex("SGR", "GRDS");
	igSgrGrpnIdx = ogBCD.GetFieldIndex("SGR", "GRPN");
	igSgrGrsnIdx = ogBCD.GetFieldIndex("SGR", "GRSN");
	igSgrPrflIdx = ogBCD.GetFieldIndex("SGR", "PRFL");
	igSgrTabnIdx = ogBCD.GetFieldIndex("SGR", "TABN");
	igSgrUgtyIdx = ogBCD.GetFieldIndex("SGR", "UGTY");
	igSgrUrnoIdx = ogBCD.GetFieldIndex("SGR", "URNO");
	igSgrUtplIdx = ogBCD.GetFieldIndex("SGR", "UTPL");
	// SGM (Group Members)
	//igSgmPrflIdx = ogBCD.GetFieldIndex("SGM", "PRFL");
	igSgmTabnIdx = ogBCD.GetFieldIndex("SGM", "TABN");
	igSgmUrnoIdx = ogBCD.GetFieldIndex("SGM", "URNO");
	igSgmUsgrIdx = ogBCD.GetFieldIndex("SGM", "USGR");
	igSgmUvalIdx = ogBCD.GetFieldIndex("SGM", "UVAL");
	//igSgmValuIdx = ogBCD.GetFieldIndex("SGM", "VALU");
	// TPL (Templates)
	igTplApplIdx = ogBCD.GetFieldIndex("TPL", "APPL");
	igTplDaloIdx = ogBCD.GetFieldIndex("TPL", "DALO");
	igTplEvorIdx = ogBCD.GetFieldIndex("TPL", "EVOR");
	igTplFisuIdx = ogBCD.GetFieldIndex("TPL", "FISU");
	igTplFldaIdx = ogBCD.GetFieldIndex("TPL", "FLDA");
	igTplFlddIdx = ogBCD.GetFieldIndex("TPL", "FLDD");
	igTplFldtIdx = ogBCD.GetFieldIndex("TPL", "FLDT");
	igTplRelxIdx = ogBCD.GetFieldIndex("TPL", "RELX");
	igTplTnamIdx = ogBCD.GetFieldIndex("TPL", "TNAM");
	igTplTpstIdx = ogBCD.GetFieldIndex("TPL", "TPST");
	igTplUrnoIdx = ogBCD.GetFieldIndex("TPL", "URNO");
	igTplFtyaIdx = ogBCD.GetFieldIndex("TPL", "FTYA");
	igTplFtydIdx = ogBCD.GetFieldIndex("TPL", "FTYD");
	// TSR (Template Sources)
	igTsrBfldIdx = ogBCD.GetFieldIndex("TSR", "BFLD");
	igTsrBtabIdx = ogBCD.GetFieldIndex("TSR", "BTAB");
	igTsrExteIdx = ogBCD.GetFieldIndex("TSR", "EXTE");
	igTsrFireIdx = ogBCD.GetFieldIndex("TSR", "FIRE");
	igTsrFormIdx = ogBCD.GetFieldIndex("TSR", "FORM");
	igTsrNameIdx = ogBCD.GetFieldIndex("TSR", "NAME");
	igTsrOperIdx = ogBCD.GetFieldIndex("TSR", "OPER");
	igTsrRfldIdx = ogBCD.GetFieldIndex("TSR", "RFLD");
	igTsrRtabIdx = ogBCD.GetFieldIndex("TSR", "RTAB");
	igTsrTextIdx = ogBCD.GetFieldIndex("TSR", "TEXT");
	igTsrTypeIdx = ogBCD.GetFieldIndex("TSR", "TYPE");
	igTsrUrnoIdx = ogBCD.GetFieldIndex("TSR", "URNO");
	// VAL (validity)
	igValApplIdx = ogBCD.GetFieldIndex("VAL", "APPL");
	igValFreqIdx = ogBCD.GetFieldIndex("VAL", "FREQ");
	igValTimfIdx = ogBCD.GetFieldIndex("VAL", "TIMF");
	igValTimtIdx = ogBCD.GetFieldIndex("VAL", "TIMT");
	igValUrnoIdx = ogBCD.GetFieldIndex("VAL", "URNO");
	igValUvalIdx = ogBCD.GetFieldIndex("VAL", "UVAL");
	igValVafrIdx = ogBCD.GetFieldIndex("VAL", "VAFR");
	igValVatoIdx = ogBCD.GetFieldIndex("VAL", "VATO");
	igValTabnIdx = ogBCD.GetFieldIndex("VAL", "TABN");
	igValVtypIdx = ogBCD.GetFieldIndex("VAL", "VTYP");
	// EQU (equipment numbers)
	igEquEnamIdx = ogBCD.GetFieldIndex("EQU", "ENAM");
	igEquEtypIdx = ogBCD.GetFieldIndex("EQU", "ETYP");
	igEquGcdeIdx = ogBCD.GetFieldIndex("EQU", "GCDE");
	igEquGkeyIdx = ogBCD.GetFieldIndex("EQU", "GKEY");
	igEquUrnoIdx = ogBCD.GetFieldIndex("EQU", "URNO");
	//--- exception handling
	CCS_CATCH_ALL
}
//------------------------------------------------------------------------
    
void CRegelWerkApp::SetLogicalTableFields()
{
	// ------------------------------------------------------------------------------------------------------------------
	// This method appends some logical fields to some tables. Logical fields are not contained in the database, 
	// but provide necessary information (e.g. flags like 'IsSelected').
	// Formerly these logical members were contained in structs in the different CedaXXXData-classes.
	// A single base class named CedaBasicData now replaces all the different CedaXXXData-classes and we no longer
	// use separate structs to store data. 
	// Instead, we use logical fields now. They have to be appended to the CedaBasicData-object ("ogBCD" within this 
	// project) once when the program starts (This is what happens here). 
	// ..................................................................................................................
	// NOTE: CedaBasicdata::SetLogicalFields() for a table has to be called _BEFORE_ CedaBasicData::Read(). 
	// ..................................................................................................................
	// REMARK: The indices of these fields are not globally defined, so every time we want to use a RecordSet-object to 
	//         access the contents we need to call GetFieldIndex() first (see documentation of CedaBasicData :-)).
	// ------------------------------------------------------------------------------------------------------------------

	//-- exception handling
	CCS_TRY

	CCSPtrArray <FieldSet> olFields;
	FieldSet olItem;
	// Field: name of the field (16 chars max)
	// Type: DATE, LONG, TRIM, ...
	// Length: integer value 

	//-------
	//* RUD *
	//-------
	olItem.Field = "IsSelected";
	olItem.Type = "TRIM";
	olItem.Length = 1;
	olFields.New(olItem);

	olItem.Field = "IsExisting";
	olItem.Type = "TRIM";
	olItem.Length = 1;
	olFields.New(olItem);

	ogBCD.SetLogicalFields("RUD", olFields);
	
	olFields.DeleteAll();
	
	
	//-------
	//* RPF *
	//-------
	olItem.Field = "IsExisting";
	olItem.Type = "TRIM";
	olItem.Length = 1;
	olFields.New(olItem);

	ogBCD.SetLogicalFields("RPF", olFields);
	
	olFields.DeleteAll();


	//-------
	//* RPQ *
	//-------
	olItem.Field = "IsExisting";
	olItem.Type = "TRIM";
	olItem.Length = 1;
	olFields.New(olItem);

	ogBCD.SetLogicalFields("RPQ", olFields);
	
	olFields.DeleteAll();


	//-------
	//* RLO *
	//-------
	olItem.Field = "IsExisting";
	olItem.Type = "TRIM";
	olItem.Length = 1;
	olFields.New(olItem);

	ogBCD.SetLogicalFields("RLO", olFields);
	
	olFields.DeleteAll();

	
	//-------
	//* REQ *
	//-------
	olItem.Field = "IsExisting";
	olItem.Type = "TRIM";
	olItem.Length = 1;
	olFields.New(olItem);

	ogBCD.SetLogicalFields("REQ", olFields);
	
	olFields.DeleteAll();



	//--- exception handling
	CCS_CATCH_ALL
}



//------------------------------------------------------------------------
//		message handlers
//------------------------------------------------------------------------
void CRegelWerkApp::OnAppAbout()
{
	CDialog *polAboutBox = new CAboutDlg;

	polAboutBox->DoModal();
	delete polAboutBox;
}
//-------------------------------------------------------------------------

int CRegelWerkApp::ExitInstance() 
{
	KillOtherProcesses();

	DeleteBrushes();

	delete CGUILng::TheOne();
	ogGUILng = 0;
	TRACE ( "ExitInstance\n" );

	//--- stop error handling
/*		PRF5468: no longer using CritErr.txt
	of_catch.close();	
*/

	// end CEDA communication
	ogCommHandler.CleanUpCom();

	AatHelp::ExitApp();

	return CWinApp::ExitInstance();
}
//--------------------------------------------------------------------------



// kills other processes started from within the application
// as there might be Grp.exe or ServiceCatalog.exe
void CRegelWerkApp::KillOtherProcesses()
{
	// MNE 000915
	// quick and dirty:
	// This is _no_ clean way to end other processes
	// There should certainly be a COM object, via that a messageused to send 
	// a message to the other processes which tells them to 
	// call ExitProcess()

	AfxGetApp()->DoWaitCursor(1);
	
	for (int i = 0; i < omProcList.GetSize(); i++)
	{
		HANDLE process;
		process = OpenProcess(PROCESS_TERMINATE, 0, (omProcList[i]));
		TerminateProcess(process, (unsigned)-1);	
	}
	
	AfxGetApp()->DoWaitCursor(-1);
}
//--------------------------------------------------------------------------

CDocument* CRegelWerkApp::OpenDocumentFile(LPCTSTR lpsilLCFileName) 
{
	int stop = 0;
	return CWinApp::OpenDocumentFile(lpsilLCFileName);
}
//--------------------------------------------------------------------------

void CRegelWerkApp::SetStatusBarText ( CString &ropText )
{
	CMainFrame *polFrm = (CMainFrame *)m_pMainWnd;
	if ( polFrm )
		polFrm->SetMessageText ( ropText );
}
//--------------------------------------------------------------------------

void CRegelWerkApp::AddProcID(DWORD dwPID)
{
	omProcList.New(dwPID);
}


//---------------------------------------------------------------------------
//			implementation of global and static functions
//---------------------------------------------------------------------------
static int GetDisplayInfo(int &ripScrWidth, int &ripScrHeight)
{
	// Usually there are 3 monitors, so that the screen width is 3 times the value of one device.
	// If we want to know the actual number of monitors, we need to look if the screen width we get from
	// Windows can be divided by one of the standard values without rest.

	// It makes no sense to run UFIS with less than 1024 x 768 pixels. Regardlessly, this function will even compute
	// the correct number of monitors when resolution is set to 640 x 480 pixels as long as one of the standard 
	// resolutions is used. If a non-standard resolution is used, the correct number of monitors can't be figured out 
	// and the function returns -1. The reference variables ripScrWidth and ripScrHeight will have the correct 
	// values, though. 


	int ilReturnValue = -1;

	// screen width and height (if there is more than one monitor, width refers to total of all monitors)
	ripScrWidth = GetSystemMetrics(SM_CXSCREEN);
	ripScrHeight = GetSystemMetrics(SM_CYSCREEN);
	
	if (ripScrWidth % 1600 == 0)
	{	
		// 1600 x 1200
		ilReturnValue = ripScrWidth / 1600;
	}
	else if (ripScrWidth % 1280 == 0)
	{	
		// 1280 x 1024
		ilReturnValue = ripScrWidth / 1280;
	}
	else if (ripScrWidth % 1152 == 0)
	{	
		// 1152 x 968
		ilReturnValue = ripScrWidth / 1152;
	}
	else if (ripScrWidth % 1024 == 0)
	{	
		// 1024 x 768
		ilReturnValue = ripScrWidth / 1024;
	}
	else if (ripScrWidth % 800 == 0)
	{	
		// 800 x 600
		ilReturnValue = ripScrWidth / 800;
	}
	else if (ripScrWidth % 640 == 0)
	{	
		// 480 x 640
		ilReturnValue = ripScrWidth / 640;
	}

	return ilReturnValue;
}
//---------------------------------------------------------------------------

static void InitDefaultView()
{
	// make viewer
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olGeoFilter;
	CStringArray olRuleFilter;
	// (Read the default value from the database in the server)

//GhsList Viewer ---------------------------
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("SERLIST");
	olViewer.SetViewerKey("SERLIST");
	CString olLastView  = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("SERLIST",olRuleFilter);
	if (olLastView != "") olViewer.SelectView(olLastView);	// restore the previously selected view
}
//---------------------------------------------------------------------------

//	GetLocationCode:
//	IN: ropUrno:	String mit URNO der Location
//		ropReftStr: String der Form Tablename.Fieldname z.B: "PST.PNAM"
//					mit Name des Feldes, in dem der Code f�r ropUrno steht
//	OUT: ropLocCode:Code der Location
bool GetLocationCode(CString &ropUrno, CString &ropReftStr, CString &ropLocCode)
{
	CString olTable, olCodeField;

	ropLocCode.Empty();
	
	if (!ParseReftFieldEntry(ropReftStr, olTable, olCodeField))
	{
		return false;
	}

	ropLocCode = ogBCD.GetField(olTable, "URNO", ropUrno, olCodeField);
	
	return (ropLocCode.IsEmpty()) ? false : true;
}



// ***** Testfunktion von HAG 990820 *****
/*bool DoesTableExist ( char *table )
{
	CedaSysTabData olSystab;	
	char pclSelection[81];

	sprintf(pclSelection, "WHERE TANA='%s'", table );
	olSystab.SetTableExtension(pcgTableExt);

	return olSystab.Read(pclSelection, true, "FINA,TYPE,FELE,FITY,SYST");
}*/
// *****

//  IN:		ropReftEntry "Tablename.Fieldname" z.B: PST.PNAM
//  OUT:	ropResTable: z.B: "PST"
//  OUT:	ropResCodeField: z.B: "PNAM"
bool ParseReftFieldEntry ( CString &ropReftEntry, CString &ropResTable, 
						   CString &ropResCodeField ) 
{
	char *pclTemp, *ps;

	pclTemp = new char[ropReftEntry.GetLength() + 1];
	if ( !pclTemp )
		return false;
	strcpy ( pclTemp, (const char*)ropReftEntry );
	if ( ps = strchr ( pclTemp, '.' ) )
	{
		*ps = '\0';
		ropResTable = +pclTemp;
		ropResCodeField = ps+1;
	}
	delete pclTemp;
	return (ps!=0);
}
//-------------------------------------------------------------------------------------------------------------------

int AddGroupsToChoiceList(CString &ropChoiceList, CString &ropResTable) 
{
	//  Gruppen, die diese Resource enthalten noch hinzuf�gen
	CStringList	olGruppen;
	int			ilGrpAnz;
	POSITION	slPos;
	CString		olUrno, olCode;

	ilGrpAnz = FindGroupsWithResourceType(ropResTable, olGruppen);

	// append '\n' if not already last character
	if (ropChoiceList.Right(1) != CString("\n"))
	{
		ropChoiceList += "\n";  
	}


	slPos = olGruppen.GetHeadPosition();
	while (slPos)
	{
		olUrno = olGruppen.GetNext (slPos);
		if ( ! IsSgrForActTpl ( olUrno ) )
			continue;
		olCode = ogBCD.GetField ( "SGR", "URNO", olUrno, "GRPN" );
		ropChoiceList += "*";		//  Kennzeichnung von Gruppen
		ropChoiceList += olCode;
		ropChoiceList += "\n";
	}

	olGruppen.RemoveAll();

	return ilGrpAnz;
}
//-------------------------------------------------------------------------------------------------------------------


//  FindGroupsWithResourceType:  Finde alle Gruppen in SGRTAB, in denen 
//								 (u.U. auch rekursiv) Resourcen des gew�nschten
//								 Typs vorkommen. 
//	IN:		opReqRes:		requested resource
//	IN/OUT:	ropUrnoList:	Liste der Urnos der Gruppen, die den Resourcetyp
//							enthalten ( wird erweitert )
//			ropInspUrnos:	
int FindGroupsWithResourceType(CString opReqRes, CStringList &ropUrnoList)
{
	// start performance test
	/*double duration;
	clock_t start, finish, intermed;
	TRACE("//////\n");
	TRACE("FindGroupsWithResourceType");
	start = clock();*/
	// test


	CStringList olInspUrnos;	//  Liste der Urnos der Gruppen, die bereits �berpr�ft 
								//	wurden 	(um unendliche Rekursion zu vermeiden )		
	int			ilRecAnz = 0, i;
	bool		blFound;
	CString		olSgrUrno, olSgrTabn;
	
	ilRecAnz = ogBCD.GetDataCount("SGR");
	for (i = 0; i < ilRecAnz; i++)
	{
		olSgrUrno = ogBCD.GetField( "SGR", i, "URNO" );
		olSgrTabn = ogBCD.GetField( "SGR", i, "TABN" );
		if ( olSgrTabn == opReqRes )
		{	//  group of required TABN -> don't investigate group members
			if ( !olInspUrnos.Find(olSgrUrno) )
				olInspUrnos.AddTail(olSgrUrno);
			if ( !ropUrnoList.Find(olSgrUrno) )
				ropUrnoList.AddTail(olSgrUrno);
		}
		else
			if (!olSgrUrno.IsEmpty() && NESTED_GROUPS )
			{
				blFound = IsResourceTypeInGroup(opReqRes, olSgrUrno, ropUrnoList, olInspUrnos);
			}

		// performance test
		/*intermed = clock();
		duration = (double) (intermed - start) / CLOCKS_PER_SEC;
		TRACE("for (%d): %2.1f\n", i, duration);*/
		// test
	}

	
	// Trace performance test
	/*finish = clock();
	duration = (double)(finish - start) / CLOCKS_PER_SEC;
	TRACE( "%2.1f seconds\n", duration );
	TRACE("End FindGroupsWithResourceType");
	TRACE("//////\n");*/
	// performance test


	return ropUrnoList.GetCount();
}
//-------------------------------------------------------------------------------------------------------------------


//  IsResourceTypeInGroup:  Check ob in Finde alle Gruppen in SGRTAB, in denen 
//								 (u.U. auch rekursiv) Resourcen des gew�nschten
//								 Typs vorkommen. 
//	IN:		opReqRes:		requested resource type
//  
//	IN/OUT:	ropUrnoList:	Liste der Urnos der Gruppen, die den Resourcetyp
//							enthalten ( wird erweitert )
//			ropInspUrnos:	Liste der Urnos der Gruppen, die bereits �berpr�ft 
//							wurden 	( wird erweitert, um unendliche Rekursion zu vermeiden )		
bool IsResourceTypeInGroup (CString opReqRes, CString opSgrUnro, 
							 CStringList &ropUrnoList, CStringList &ropInspUrnos )
{
	CCSPtrArray<RecordSet>	olRecords;
	int						ilAnz = 0, i;
	CString					olTana, olUrno;
	bool					blFound=false;

	//  Wenn Gruppe bereits untersucht worden ist, fertig
	//  return-Wert h�ngt davon ab, ob bei fr�herer Untersuchung die Urno in
	//  ropUrnoList eingetragen worden ist
	if (ropInspUrnos.Find(opSgrUnro))
	{
		return (ropUrnoList.Find(opSgrUnro) ? true : false);
	}
	else
	{
		//  sofort als untersuchte Gruppe eintragen, damit Rekursion abbrechen kann
		ropInspUrnos.AddTail(opSgrUnro);
	}
	//  Hole alle Datens�tze aus SGMTAB, die zu opSgrUnro geh�ren
	ogBCD.GetRecords( "SGM", "USGR", opSgrUnro, &olRecords );
	ilAnz = olRecords.GetSize ();
	for (i = 0; (i < ilAnz) && !blFound; i++)
	{
		olTana = olRecords[i].Values[ogBCD.GetFieldIndex("SGM", "TABN")];
		olUrno = olRecords[i].Values[ogBCD.GetFieldIndex("SGM", "URNO")];
		if (olTana.Left(3) == opReqRes.Left(3))
		{
			//  einen Member dieser Gruppe gefunden, der Bedingung erf�llt, also 
			//  Gruppe ggf. in ropUrnoList aufnehmen und fertig
			if (!ropUrnoList.Find(opSgrUnro))
			{
				ropUrnoList.AddTail(opSgrUnro);
			}
			blFound = true;
		}
	
		//  Handelt es sich um ein Member, das selbst statische Gruppe ist ?
		if (olTana.Left(3) == "SGR")
		{
			blFound = IsResourceTypeInGroup(opReqRes, olUrno, ropUrnoList, ropInspUrnos);
		}
	}
	olRecords.DeleteAll ();
	return blFound;
}
//-------------------------------------------------------------------------------------------------------------------

//	GetLocationCode:
//	IN: popRecord:		RecordSet aus Tabelle RLO, RPF oder RPQ
//		ipTabnIndex:	Index f�r das Feld "GTAB" innerhalb des RecordSet
//		ipResUrnoIndex:	Index f�r die Urno der Resource innerhalb des RecordSet
//	OUT: ropGroupName:	Code der Location
//  RETURN:	true, wenn in popRecord eine Gruppe von Resourcen verwendet wird
//			false, wenn in popRecord eine einzelne Resource verwendet wird
bool GetGroupName ( RecordSet *popRecord, int ipGTabIndex, int ipResUrnoIndex,
					CString &ropGroupName )
{
	CString olGTabEntry;
	olGTabEntry = popRecord->Values[ipGTabIndex];
	if ( !olGTabEntry.IsEmpty () && (olGTabEntry!=" ") )
	{	//  es handelt sich um eine Gruppe
		CString olGrpTable = "SGR";
		CString olGrpCodeField = "GRPN";
		ParseReftFieldEntry ( olGTabEntry, olGrpTable, olGrpCodeField ) ;
		ropGroupName = ogBCD.GetField ( olGrpTable, "URNO", 
										popRecord->Values[ipResUrnoIndex],
										olGrpCodeField );
		return true;
	}
	else
		return false;
}
//-------------------------------------------------------------------------------------------------------------------

void SetWhereValidString ( CString opTableName, CString &ropWhere )
{
	CString olTime, olFieldList;
	char    pclWhere[101]="";
	CTime	olToday;
	//CString olVatoField = ogBCD.GetFieldExt( "SYS", "TANA", "FINA", opTableName, "VATO", "FINA" );
	CedaObject *prlObject;
	if ( ogBCD.omObjectMap.Lookup((LPCSTR)opTableName, (void *&)prlObject) )
		olFieldList = prlObject->GetFieldList();

	if ( olFieldList.Find( "VATO", 0 ) >= 0 )
	{	//  Tabelle 'opTableName' beinhaltet Feld "VATO"
		olToday = CTime::GetCurrentTime();
		olTime = CTimeToDBString(olToday, olToday);
		sprintf ( pclWhere, "WHERE (VATO=' ' OR VATO='' OR VATO>'%s')", (LPCSTR)olTime );
	}
	ropWhere = pclWhere;
}
//-----------------------------------------------------------------------------------------------------------------------


//  Test, ob sich die G�ltigkeitszeitr�ume ropValData1 und ropValData2  �berlappen
//	z.Z. werden nur die die Felder Vato und Vafr verarbeitet, da die anderen 
//	vom Bediener noch nicht gef�llt werden k�nnen 
bool DoValiditiesOverlap ( VALDATA &ropValData1, VALDATA &ropValData2 )
{
	CTime olVafr1 = DBStringToDateTime( CString(ropValData1.Vafr) );
	CTime olVafr2 = DBStringToDateTime( CString(ropValData2.Vafr) );
	CTime olVato1 = DBStringToDateTime( CString(ropValData1.Vato) );
	CTime olVato2 = DBStringToDateTime( CString(ropValData2.Vato) );
	CTimeSpan olOneDay( 1, 0, 0, 0 );
	//  Wenn beide Anfangszeiten oder beiden Endzeiten nicht gesetzt sind,
	//  �berlappen sich die Zeitr�ume
	if ( ( olVafr1 == TIMENULL ) && ( olVafr2 == TIMENULL ) )
		return TRUE;
	if ( ( olVato1 == TIMENULL ) && ( olVato2 == TIMENULL ) )
		return TRUE;
	//  Ist eine der beiden Anfangszeiten nicht gesetzt, wird sie auf einen Tag 
	//	fr�her als die andere gesetzt
	if ( olVafr1 == TIMENULL ) 
		olVafr1 = olVafr2 - olOneDay;
	else 
		if ( olVafr2 == TIMENULL ) 
			olVafr2 = olVafr1 - olOneDay;	

	//  Ist eine der beiden Endzeiten nicht gesetzt, wird sie auf einen Tag 
	//	sp�ter als die andere gesetzt
	if ( olVato1 == TIMENULL ) 
		olVato1 = olVato2 + olOneDay;
	else 
		if ( olVato2 == TIMENULL ) 
			olVato2 = olVato1 + olOneDay;	
	//  nun sind auf alle F�lle alle vier Zeiten g�ltig
	if ( ( olVafr1<olVafr2 ) && ( olVato1>olVafr2 ) )
		return true;
	if ( ( olVafr2<olVafr1 ) && ( olVato2>olVafr1 ) )
		return true;
	return false;
}
//-----------------------------------------------------------------------------------------------------------------------

//  String von TSR.TEXT:
//	'NAME(IDS_XXX)#ACWS(IDS_WINGSPAN)|SEAT(IDS_SITZE)'	-->
//  ropNames[0]= STRG from TXTTAB where TXID='IDS_XXX'
//	ropNames[1]= STRG from TXTTAB where TXID='IDS_WINGSPAN'
//	ropNames[2]= STRG from TXTTAB where TXID='IDS_SITZE'

bool GetNameOfTSRRecord ( CString &ropUrno, CString &ropName )
{
	RecordSet	olRecord;
	CString		olTSRText;
	int			n;

	if  ( !ogBCD.GetRecord ( "TSR", "URNO", ropUrno, olRecord ) )
		return false;
	olTSRText = olRecord.Values[igTsrTextIdx];
	n = olTSRText.Find('#') ;
	if ( n>0 ) 
	{
		olTSRText = olTSRText.Left ( n-1 );
		if ( GetStringForTSRTextEntry ( olTSRText, ropName ) )
			return true;
	}
	//  keine (vorhandene) TXID in TSR.TEXT gefunden 
	ropName = olRecord.Values[igTsrNameIdx];
	return true;
}
//-----------------------------------------------------------------------------------------------------------------------

bool FillNamesArray ( CString &ropUrno, CStringArray &ropNames )
{
	RecordSet		olRecord;
	CStringArray	olIDList;	
	CString			olTSRText, olText, olTSRTextRight;
	int				n;

	ropNames.RemoveAll();
	if  ( !ogBCD.GetRecord ( "TSR", "URNO", ropUrno, olRecord ) )
		return false;
	olTSRText = olRecord.Values[igTsrTextIdx];
	n = olTSRText.Find('#') ;
	if ( n<=0 ) 
		return false;
	
	CutLeft( olTSRText, olTSRTextRight, "#" );
	if ( !GetStringForTSRTextEntry ( olTSRText, olText ) )
		olText = olRecord.Values[igTsrNameIdx];
	ropNames.Add ( olText );

	n = ExtractItemList( olTSRTextRight, &olIDList, '|' );
	for ( int i=0; i<n; i++ )
	{
		olTSRText = olIDList[i];
		if ( !GetStringForTSRTextEntry ( olTSRText, olText ) )
			olText = "";
		ropNames.Add ( olText );
	}
	return true;
}
//-----------------------------------------------------------------------------------------------------------------------

bool GetStringForTSRTextEntry ( CString opEntry, CString &ropString )
{
	char	pclText[41], *ps;
	CString	olStrID, olUrno ;
	
	if ( ogBCD.GetDataCount("TXT") <= 0 )
		return false;

	strcpy ( pclText, opEntry.Left (40) );
	ps = strchr ( pclText, ')' );
	if ( ps )
		*ps = '\0';
	ps = strchr ( pclText, '(' );
	olStrID = ps ? ps+1 : pclText;

	olUrno = ogBCD.GetFieldExt( "TXT", "TXID", "APPL", olStrID, "TSRTAB", "URNO" );
	if ( !olUrno.IsEmpty() )
		return ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", ropString ) ;
	else 
		return false;
}
//-----------------------------------------------------------------------------------------------------------------------

bool IsAlreadyRunning()
{
	HANDLE hMapping;
	hMapping = CreateFileMapping((HANDLE) 0xffffffff, NULL, PAGE_READONLY, 0, 32, "MyTestMap");
	if (hMapping)
	{
		if (GetLastError() == ERROR_ALREADY_EXISTS)
		{
			return true;
		}
	}

	return false;
}
//-----------------------------------------------------------------------------------------------------------------------

void CreateResourceChoiceLists()
{
	CCS_TRY
	int i;

	ogResChoiceLists.New(MakeList("PFC", "FCTC"));	 // Functions
	ogResChoiceLists.New(MakeList("PER", "PRMC"));	 // Qualifications

	// Locations
	CString olReft, olTabn, olFldn, olType;
	CCSPtrArray <RecordSet> olAloArr;
	ogBCD.GetRecords("ALO", "ALOT", "0", &olAloArr);
	for (i = 0; i < olAloArr.GetSize(); i++)
	{
		olReft = olAloArr[i].Values[igAloReftIdx];
		if (olReft.GetLength() == 8)
		{
			olTabn = olReft.Left(3);
			olFldn = olReft.Right(4);
			
			ogResChoiceLists.New(MakeList(olTabn, olFldn));
		}
	}

	if ( igEqtUrnoIdx >= 0)
	{	//  Table EQTTAB exists
		for (i = 0; i < ogBCD.GetDataCount("EQT"); i++)
		{
			olType = ogBCD.GetField ("EQT", i, "URNO" );
			if ( !olType.IsEmpty() )
				ogResChoiceLists.New( MakeEquList( "GCDE", olType ) );
		}
	}
	olAloArr.DeleteAll();

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

RES_CHOICELIST MakeList(CString opResTab, CString opResField)
{
	CString olList;

	int ilCount = ogBCD.GetDataCount(opResTab);
	for (int i = 0; i < ilCount; i++)
	{
		olList += (ogBCD.GetField(opResTab, i, opResField) + CString("\n"));
	}

	olList = olList.Left(olList.GetLength() - 1); // remove last "\n"
	AddGroupsToChoiceList(olList, opResTab);

	RES_CHOICELIST rlChoiceList;
	rlChoiceList.ValueList = olList;	// Is this a source for mem leaks ?
	rlChoiceList.Table = opResTab;
	rlChoiceList.Field = opResField;
	rlChoiceList.SubType = "";
	return rlChoiceList;
}
//-----------------------------------------------------------------------------------------------------------------------

CString GetChoiceList(CString opResTab, CString opResField, CString *popType/*=0*/ )
{
	
	CString olChoiceList;
	bool blFound = false;
	
	int i = 0;
	int ilSize = ogResChoiceLists.GetSize();

	while ((i < ilSize) && (blFound == false))
	{
		if ((ogResChoiceLists[i].Table == opResTab) && (ogResChoiceLists[i].Field == opResField))
			if ( !popType || (ogResChoiceLists[i].SubType == *popType ) )
			{
				olChoiceList = ogResChoiceLists[i].ValueList;
				blFound = true;
			}
		i++;
	}

	return olChoiceList;
}
//-----------------------------------------------------------------------------------------------------------------------
/*	hag20020124: not used at the moment
bool SetChoiceList(CString opTab, CString opField, CString opChoiceList)
{
	for (int i = 0; i < ogResChoiceLists.GetSize(); i++)
	{
		if ((ogResChoiceLists[i].Table == opTab) && (ogResChoiceLists[i].Field == opField))
		{
			ogResChoiceLists[i].ValueList = opChoiceList;
			return true;
		}
	}
	return false;
}
*/

bool ChangeResChoiceLists ( CString opTable, RecordSet *popRecord, bool bpGroup, bool bpAdd )
{
	CString olCode, olField, olChoiceList="\n";
	CString  olItem = bpGroup ? "\n*" : "\n";
	int		idx, ilLength, ilTypIdx=-1;

	if ( !popRecord )
		return false;

	if ( opTable == "EQU" )
		ilTypIdx = (bpGroup) ? ogBCD.GetFieldIndex ("SGR","STYP" ) : igEquGkeyIdx;
	for (int i = 0; i < ogResChoiceLists.GetSize(); i++)
	{
		if ( ogResChoiceLists[i].Table != opTable) 
			continue;
		if ( ( ilTypIdx >= 0 ) && 
			 ( ogResChoiceLists[i].SubType !=  popRecord->Values[ilTypIdx] ) )
		{	//  popRecord doesn't fit type of Equipment of this choicelist
			continue;
		}
		idx =  -1;
		if ( !bpGroup )
		{
			olField = ogResChoiceLists[i].Field;
			idx = ogBCD.GetFieldIndex ( opTable, ogResChoiceLists[i].Field );
		}
		else
			idx = igSgrGrpnIdx;
		if ( idx >= 0 )
		{
			olItem = bpGroup ? "\n*" : "\n";
			olCode = popRecord->Values[idx];
			olItem += olCode;
			olItem += "\n";
			olChoiceList = ogResChoiceLists[i].ValueList ;

			if (!olChoiceList.IsEmpty() && (olChoiceList.Right(1) != "\n"))
			{
				olChoiceList += "\n";
			}

			int ilPos = olChoiceList.Find(olItem);
			if (ilPos == -1)
			{
				if (bpAdd == false)	//  L�schen und nicht gefunden fertig
				{
					continue;
				}

				//  Einf�gen
				olItem.Replace("\n", "");
				olChoiceList += olItem;
			}
			else		//  item ist in Auswahlliste
			{
				if ( bpAdd )  //  zuf�gen -> fertig
					continue;
				//  l�schen
				olChoiceList.Replace ( olItem, "\n" );
				ilLength = olChoiceList.GetLength();
				if ( olChoiceList[ilLength-1]=='\n' )
					 olChoiceList = olChoiceList.Left(ilLength-1);
			}	
			//  vorderen '\n' wieder entfernen
			ilLength = olChoiceList.GetLength();
			if ( (ilLength>0) && ( olChoiceList[0]=='\n' ) )
				olChoiceList = olChoiceList.Right(ilLength-1);
			ogResChoiceLists[i].ValueList = olChoiceList;
		}
	}
	return true;
}
/*
bool DoesTableExist ( char *table )
{
	CString olUrno = ogBCD.GetFieldExt("SYS", "TANA", "FINA", table, "URNO", "URNO");
	return !olUrno.IsEmpty();
}
*/
bool DoesTableExist ( char *table )
{
	CString olWhere;
	olWhere.Format (" WHERE TANA='%s' and FINA='URNO'", table );
	ogBCD.Read("SYS", olWhere);
	int ilRecs = ogBCD.GetDataCount("SYS");
	return (ilRecs>=1);
}


//-----------------------------------------------------------------------------------------------------------------------
RES_CHOICELIST MakeEquList( CString opResField, CString opType )
{
	CString					olSgrUrno;
	int						ilDBRecords, j, ilEquCodeIdx;
	CString					olCode, olChoiceList;
	CCSPtrArray<RecordSet>	olRecords;

	ilEquCodeIdx = ogBCD.GetFieldIndex ( "EQU", opResField );
	
	if ( (ilEquCodeIdx>=0) && (igSgrGrpnIdx>=0) )
	{
		ogBCD.GetRecords ( "EQU", "GKEY", opType, &olRecords );

		ilDBRecords = olRecords.GetSize(); 
		for ( j=ilDBRecords-1; j>=0; j-- )
		{
			olCode = olRecords[j].Values[ilEquCodeIdx];
			olChoiceList += olCode;
			olChoiceList += "\n" ;
		}
		olRecords.DeleteAll ();	
		
		//  Gruppen, die diese Resource enthalten
		ogBCD.GetRecords ( "SGR", "STYP", opType, &olRecords );
		ilDBRecords = olRecords.GetSize(); 
		for ( j=ilDBRecords-1; j>=0; j-- )
		{
			olSgrUrno = olRecords[j].Values[igSgrUrnoIdx];
			if ( ! IsSgrForActTpl ( olSgrUrno ) )
				continue;
			olCode = olRecords[j].Values[igSgrGrpnIdx];
			olChoiceList  += "*";		//  Kennzeichnung von Gruppen
			olChoiceList  += olCode;
			olChoiceList  += "\n" ;
		}
		olRecords.DeleteAll ();	
	}
	j = olChoiceList.GetLength();
	if ( j > 0 )
		olChoiceList = olChoiceList.Left(j - 1); // remove last "\n"

	RES_CHOICELIST rlChoiceList;
	rlChoiceList.ValueList = olChoiceList;	
	rlChoiceList.Table = "EQU";
	rlChoiceList.Field = opResField;
	rlChoiceList.SubType = opType;
	return rlChoiceList;
}

//  Is ropCode (from a resource grid) a single resource or a static group
//  if it is a group, the asteric (*) will be removed from ropCode
bool IsSingleResourceSelected ( CString &ropCode )
{
	if ( !ropCode.IsEmpty() && ropCode[0] == '*' )
	{
		ropCode = ropCode.Right ( ropCode.GetLength()-1 );
		return false;
	}
	return true;
}


//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------

void CRegelWerkApp::WinHelp(DWORD dwData, UINT nCmd) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	AatHelp::WinHelp(dwData, nCmd);
}

