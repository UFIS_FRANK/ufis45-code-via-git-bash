// DutyGrid.cpp: implementation of the CDutyGrid class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <cedabasicdata.h>
#include <regelwerk.h>
#include <DutyGrid.h>
#include <CCSGlobl.h>
#include <vector>


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif

//---------------------------------------------------------------------------------------------------------------------
//					Construction/Destruction
//---------------------------------------------------------------------------------------------------------------------
using namespace std;

CDutyGrid::CDutyGrid(CWnd *pParent)
	:CGridFenster(pParent)
{
	bmExpanded=false;
	// background colors for group links
	rmLinkColors[0] = PYELLOW;
	rmLinkColors[1] = PBLUE;
	rmLinkColors[2] = ORANGE;
	rmLinkColors[3] = LIGHTBROWN;//AQUA;
	rmLinkColors[4] = YELLOW;
	rmLinkColors[5] = FUCHSIA;
	rmLinkColors[6] = LGREEN;
	rmLinkColors[7] = TEAL;
	rmLinkColors[8] = LIME;
	rmLinkColors[9] = ROSE;
	rmLinkColors[10] = PGREEN;
	
}
//---------------------------------------------------------------------------------------------------------------------

CDutyGrid::~CDutyGrid()
{

}



//---------------------------------------------------------------------------------------------------------------------
//					initialization
//---------------------------------------------------------------------------------------------------------------------

void CDutyGrid::IniLayout()
{

	//--- set number of columns
	SetColCount(9);

	//--- set column widths
	CRect olRect;
	GetClientRect(&olRect);

	olRect.DeflateRect(7, 0);

	int ColWidth0 = 25;
	int ColWidth1 = (olRect.Width()- ColWidth0) / 6;
	
	SetColWidth(0, 0, ColWidth0);	// header
	SetColWidth(1, 1, ColWidth1);	// Code
	SetColWidth(2, 2, ColWidth1*2);	// Name
	SetColWidth(3, 3, ColWidth1);	// Personal
	SetColWidth(4, 4, ColWidth1);	// Equipment
	SetColWidth(5, 5, ColWidth1);	// Location
	// Hidden columns:
	// 6	-	UDGR
	// 7	-   URUD
	// 8	-   DISP
	// 9	-   ULNK
	GetParam()->SetNumberedColHeaders(FALSE);                 
	GetParam()->SetNumberedRowHeaders(FALSE); 
	GetParam()->EnableTrackRowHeight(FALSE); 
	GetParam()->EnableTrackColWidth(FALSE);
	GetParam()->EnableSelection	(GX_SELROW|GX_SELMULTIPLE);

	ChangeColHeaderStyle(CGXStyle().SetEnabled(FALSE));
	SetValueRange(CGXRange(0,1), GetString(CODE) );
	SetValueRange(CGXRange(0,2), GetString(NAME));
	SetValueRange(CGXRange(0,3), GetString(IDS_PERSONAL));
	SetValueRange(CGXRange(0,4), GetString(IDS_EQUIPMENT));
	SetValueRange(CGXRange(0,5), GetString(IDS_LOCATION));

	SetStyleRange(CGXRange().SetCols(3,5), 
				  CGXStyle().SetHorizontalAlignment(DT_CENTER));

	//--- these columns are only used to save data 
	HideCols(6, 9); 

	//--- row height can't be changed
	GetParam()->EnableMoveRows(false);
	
	ResetLinkColorIndex();
	SetReadOnly();
}
//---------------------------------------------------------------------------------------------------------------------

void CDutyGrid::SetExpanded(bool bpExpanded)
{
	bmExpanded = bpExpanded;
	DisplayRudTmp();
	SetCurrentCell(0, 0);
	SetSortingEnabled(!bmExpanded);
}
//---------------------------------------------------------------------------------------------------------------------

void CDutyGrid::DisplayRudTmp(bool bpInitLinkColors /*true*/)
{
	// added by mne 001019
	GetParam()->SetLockReadOnly(FALSE);
	SetEnabledParam(TRUE);
	LockUpdate();
	// end mne

	ResetContent();	

	if(bpInitLinkColors)
	{
		ResetLinkColorIndex();
	}
	CStringList olUdgrList;
	CString olUdgr;
	int i, ilRudCount = ogBCD.GetDataCount( "RUD_TMP" );
	
	// MNE TEST 001110
	RudTmp_Dump();
	// END TEST

	//  Liste aller UDGR's erstellen (keine doppelten)
	for ( i=0; i<ilRudCount; i++ )
	{
		olUdgr = ogBCD.GetField ( "RUD_TMP", i, "UDGR" );
		if ( ( olUdgr!="NULL" ) && !olUdgrList.Find(olUdgr) )
			olUdgrList.AddTail ( olUdgr );
	}
	
	//  Alle Demand-Groups darstellen
	POSITION	pos;
	ROWCOL		ilRows ;
	pos = olUdgrList.GetHeadPosition();
	while ( pos )
	{
		olUdgr = olUdgrList.GetNext (pos);
		DisplayDemandGroup ( olUdgr );
		if ( bmExpanded && pos )	//  TrennStrich zw. verschiedenen DGR's 
		{
			ilRows = GetRowCount();
			BOOL ok= SetStyleRange ( CGXRange().SetRows(ilRows),
							CGXStyle().SetBorders(gxBorderBottom, 
												  CGXPen(PS_SOLID, 2, RGB(0,0,255) ) )); 
		}		
	}

	// added by mne 001019
	LockUpdate(FALSE);
	Redraw();
	SetEnabledParam(FALSE);
	GetParam()->SetLockReadOnly(TRUE);
	// end mne
}
//---------------------------------------------------------------------------------------------------------------------

bool CDutyGrid::DisplayDemandGroup ( CString &ropUdgr )
{
	CString					olSerUrno, olSerCode, olSerSnam, olRety, olUrud,olUlnk, olDisp;
	CCSPtrArray<RecordSet>	olRudsOfDgr;
	int						i, ilPersonal=0, ilEquipment=0, ilLocations=0;
	int						ilRetyLength;
	ROWCOL					ilRows, ilCols;
	char					pclStr[11];

	BOOL blLock = GetParam()->IsLockReadOnly();
	if ( blLock )
		GetParam()->SetLockReadOnly(FALSE);

	CGXStyle olStyle;
	olStyle.SetEnabled(FALSE);

	ogBCD.GetRecords ( "RUD_TMP", "UDGR", ropUdgr, &olRudsOfDgr );
	if ( olRudsOfDgr.GetSize () <= 0 )
		return false;
	olSerUrno = olRudsOfDgr[0].Values[igRudUghsIdx];
	olSerSnam = ogBCD.GetField("SER", "URNO", olSerUrno, "SNAM");
	olSerCode = ogBCD.GetField("SER", "URNO", olSerUrno, "SECO");
	
	ilRows = GetRowCount ();
	ilCols = GetColCount ();
	//GetParam()->SetLockReadOnly(FALSE);
	
	for ( i=0; i<olRudsOfDgr.GetSize(); i++ )
	{
		olRety = olRudsOfDgr[i].Values[igRudRetyIdx];
		olUrud = olRudsOfDgr[i].Values[igRudUrnoIdx];
		olUlnk = olRudsOfDgr[i].Values[igRudUlnkIdx];
		olDisp = olRudsOfDgr[i].Values[igRudDispIdx];

		ilRetyLength = olRety.GetLength();
		
		olStyle.SetInterior(CreateLinkBkColor(olUlnk));

		if ( ( ilRetyLength >= 1 ) && ( olRety[0] == '1' ) )
			ilPersonal ++;
		if ( ( ilRetyLength >= 2 ) && ( olRety[1] == '1' ) )
			ilEquipment++;
		if ( ( ilRetyLength >= 3 ) && ( olRety[2] == '1' ) )
			ilLocations++;
		if ( bmExpanded && InsertRows(ilRows+ 1, 1) )
		{
			ilRows++;
			SetValueRange ( CGXRange(ilRows,1), olSerCode );
			SetValueRange ( CGXRange(ilRows,2), olSerSnam );
			SetValueRange ( CGXRange(ilRows,3), _itoa(ilPersonal,pclStr,10) );
			SetValueRange ( CGXRange(ilRows,4), _itoa(ilEquipment,pclStr,10) );
			SetValueRange ( CGXRange(ilRows,5), _itoa(ilLocations,pclStr,10) );
			SetValueRange ( CGXRange(ilRows,6), ropUdgr );
			SetValueRange ( CGXRange(ilRows,7), olUrud );
			SetValueRange ( CGXRange(ilRows,8), olDisp );
			SetValueRange ( CGXRange(ilRows,9), olUlnk );
			SetStyleRange(CGXRange(ilRows, 1, ilRows, ilCols), olStyle);
			ilPersonal = ilEquipment = ilLocations = 0;
		}
	}
	if ( !bmExpanded && InsertRows(ilRows + 1, 1) )
	{
		ilRows++;
		SetValueRange ( CGXRange(ilRows,1), olSerCode );
		SetValueRange ( CGXRange(ilRows,2), olSerSnam );
		SetValueRange ( CGXRange(ilRows,3), _itoa(ilPersonal,pclStr,10) );
		SetValueRange ( CGXRange(ilRows,4), _itoa(ilEquipment,pclStr,10) );
		SetValueRange ( CGXRange(ilRows,5), _itoa(ilLocations,pclStr,10) );
		SetValueRange ( CGXRange(ilRows,6), ropUdgr );
		SetValueRange ( CGXRange(ilRows,7), "" );
		SetValueRange ( CGXRange(ilRows,8), "" );
		SetValueRange ( CGXRange(ilRows,9), "" );
	}
	//GetParam()->SetLockReadOnly(TRUE);
	olRudsOfDgr.DeleteAll();
	if ( blLock )
		GetParam()->SetLockReadOnly(TRUE);
	return true;
}
//---------------------------------------------------------------------------------------------------------------------

BOOL CDutyGrid::ResetContent()
{
	BOOL blLock = GetParam()->IsLockReadOnly();
	if ( blLock )
		GetParam()->SetLockReadOnly(FALSE);
	BOOL blRet = CGridFenster::ResetContent();
	if ( blLock )
		GetParam()->SetLockReadOnly(TRUE);
	return blRet;	
}
//---------------------------------------------------------------------------------------------------------------------

void CDutyGrid::SetEnabledParam(BOOL bpState)
{
	BOOL ok =TRUE;
	BOOL blLock = GetParam()->IsLockReadOnly();
	if ( blLock )
		GetParam()->SetLockReadOnly(FALSE);
	int ilCols = GetColCount();
	int ilRows = GetRowCount();
	if ( (ilCols>=1) && (ilRows>=1) )
		ok= SetStyleRange ( CGXRange(1,1,ilRows,ilCols),
							CGXStyle().SetEnabled(bpState));	
	if ( blLock )
		GetParam()->SetLockReadOnly(TRUE);
}
//---------------------------------------------------------------------------------------------------------------------

bool CDutyGrid::GetSelectionInfo ( CString &ropUdgr, CString &ropUrud ) 
{
	CRowColArray olRowArr;
	if ( GetRowCount() < 1 )
		return false;
	ROWCOL ilSelRows = GetSelectedRows(olRowArr);
	if ( ilSelRows==1 )
	{
		ropUdgr = GetValueRowCol ( olRowArr[0], 6 );
		ropUrud = GetValueRowCol ( olRowArr[0], 7 );
	}
	return (ilSelRows==1);
}
//---------------------------------------------------------------------------------------------------------------------

BOOL CDutyGrid::OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	if ( (nRow>0) && (nCol == 0) && pomParent )
	{
		LPARAM lParam = MAKELPARAM(pt.x, pt.y);
		SetCurrentCell ( nRow, nCol );
		pomParent->SendMessage(WM_TABLE_RBUTTONDOWN, nRow, lParam );
		return TRUE;
	}
	else
		return FALSE;
}
//---------------------------------------------------------------------------------------------------------------------

bool CDutyGrid::OnDeleteRudTmp ( RecordSet *popRecord )
{
	CString olUrno, olUdgr, olRety, olWert;
	BOOL	blRet=FALSE;
	int		ilIdx, i, ilAnz;
	BOOL blLock = GetParam()->IsLockReadOnly();
	if ( blLock )
		GetParam()->SetLockReadOnly(FALSE);
	if ( bmExpanded )
	{
		olUrno = popRecord->Values[igRudUrnoIdx];
		for ( i=GetRowCount(); i>=1; i-- )
		{	
			olWert = GetValueRowCol ( i, 7 );
			if ( olWert == olUrno )
			{
				blRet = RemoveRows ( i, i );
				SetCurrentCell (0,0);
			}
		}
	}
	else
	{
		olUdgr = popRecord->Values[igRudUdgrIdx];
		olRety = popRecord->Values[igRudRetyIdx];
		for ( i=GetRowCount(); i>=1; i-- )
		{	
			olWert = GetValueRowCol ( i, 6 );
			if ( olWert == olUdgr )
			{
				ilIdx = olRety.Find ('1');	//  bestimme Spalte, deren Anzahl verringert werden mu�
				if ( ilIdx >= 0 )
				{	
					olWert = GetValueRowCol ( i, 3+ilIdx );
					if ( sscanf ( olWert, "%d", &ilAnz ) >= 1 )
					{
						olWert.Format ( "%d", max(0,ilAnz-1) );
						blRet = SetValueRange  ( CGXRange( i, 3+ilIdx ), olWert );
					}
				}
				if ( ( GetValueRowCol(i,3) == "0" ) &&
					 ( GetValueRowCol(i,4) == "0" ) &&
					 ( GetValueRowCol(i,5) == "0" ) )
				{
					blRet &= RemoveRows ( i, i );
					SetCurrentCell (0,0);
				}
			}
		}

	}
	if ( blLock )
		GetParam()->SetLockReadOnly(TRUE);
	return (blRet) ? true : false;
}
//----------------------------------------------------------------------------------------------------------------------

bool CDutyGrid::OnNewRudTmp ( RecordSet *popRecord )
{
	CString olUrno, olUdgr, olRety, olWert,olSerUrno, olSerSnam, olSerCode, olDisp, olUlnk ;
	BOOL	blRet=FALSE;
	int		ilIdx, i, ilAnz;
	BOOL blLock = GetParam()->IsLockReadOnly();

	if ( blLock )
		GetParam()->SetLockReadOnly(FALSE);

	olSerUrno = popRecord->Values[igRudUghsIdx];
	olSerSnam = ogBCD.GetField("SER", "URNO", olSerUrno, "SNAM");
	olSerCode = ogBCD.GetField("SER", "URNO", olSerUrno, "SECO");
	olUdgr = popRecord->Values[igRudUdgrIdx];	
	olUrno = popRecord->Values[igRudUrnoIdx];
	olRety = popRecord->Values[igRudRetyIdx];
	olUlnk = popRecord->Values[igRudUlnkIdx];	
	olDisp = popRecord->Values[igRudDispIdx];
	if ( olRety.GetLength() < 3 )
		olRety = "100";		//  default-Fall

	if ( bmExpanded )
	{
		for ( i=GetRowCount(); i>=1; i-- )
		{	
			olWert = GetValueRowCol ( i, 6 );
			if ( olWert == olUdgr )
			{
				blRet = InsertRows(i+ 1, 1);
				if ( blRet )
				{
					SetValueRange ( CGXRange(i+1,1), olSerCode );
					SetValueRange ( CGXRange(i+1,2), olSerSnam );
					SetValueRange ( CGXRange(i+1,3), olRety.Left(1) );
					SetValueRange ( CGXRange(i+1,4), olRety.Mid(1) );
					SetValueRange ( CGXRange(i+1,5), olRety.Right(1) );
					SetValueRange ( CGXRange(i+1,6), olUdgr );
					SetValueRange ( CGXRange(i+1,7), olUrno );
					SetValueRange ( CGXRange(i+1,8), olDisp );
					SetValueRange ( CGXRange(i+1,9), olUlnk );
				}
				break;
			}
		}
	}
	else
	{
		for ( i=GetRowCount(); i>=1; i-- )
		{	
			olWert = GetValueRowCol ( i, 6 );
			if ( olWert == olUdgr )
			{
				ilIdx = olRety.Find ('1');	//  bestimme Spalte, deren Anzahl verringert werden mu�
				if ( ilIdx >= 0 )
				{	
					olWert = GetValueRowCol ( i, 3+ilIdx );
					if ( sscanf ( olWert, "%d", &ilAnz ) >= 1 )
					{
						olWert.Format ( "%d", max(1,ilAnz+1) );
						blRet = SetValueRange  ( CGXRange( i, 3+ilIdx ), olWert );
					}
				}
			}
		}
	}

	if ( blLock )
		GetParam()->SetLockReadOnly(TRUE);
	return (blRet) ? true : false;
}
//----------------------------------------------------------------------------------------------------------------------

BOOL CDutyGrid::SetCurrentCell(ROWCOL nRow, ROWCOL nCol, UINT flags )
{
	BOOL blRet = CGridFenster::SetCurrentCell ( nRow, nCol, flags );
	if ((nRow == 0) && (nCol == 0))
	{
		OnInitCurrentCell (nRow,nCol);
	}
	
	return blRet;
}
//-----------------------------------------------------------------------------------------------------------------------

int CDutyGrid::GetResTypeForSelection ()
{
	int		ilRet = -1;
	CString olRety, olWert;
	BOOL	blCurrCellOk;
	CString olUdgr, olUrud;
	ROWCOL	ilRow, ilCol;


	if ( !GetSelectionInfo ( olUdgr, olUrud ) )
		return -1;
	if ( bmExpanded )
	{	//  Single-Duty-Darstellung
		olRety = ogBCD.GetField("RUD_TMP", "URNO", olUrud, "RETY" );
		ilRet = olRety.Find ('1') ;
	}
	else
	{
		blCurrCellOk = GetCurrentCell ( ilRow, ilCol );
		olWert = GetValueRowCol ( ilRow, ilCol );
		if ( (ilCol>=3) && (ilCol<=5) && (olWert!="0") )
			ilRet = ilCol-3;
		for ( ilCol=3; (ilRet<0)&&(ilCol<=5); ilCol++ )
		{
			olWert = GetValueRowCol ( ilRow, ilCol );
			if ( olWert!= "0" )
				ilRet = ilCol-3;
		}
	}
	return ilRet;
}
//-----------------------------------------------------------------------------------------------------------------------

BOOL CDutyGrid::OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	if ((nRow > 0) && (nCol >= 0) && pomParent)
	{
		LPARAM lParam = MAKELPARAM(pt.x, pt.y);
		SetCurrentCell(nRow, nCol);
		pomParent->SendMessage(WM_TABLE_LBUTTONDBLCLK, nRow, lParam);
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
//-----------------------------------------------------------------------------------------------------------------------

void CDutyGrid::GetSelectedUrnos(CStringArray &ropUrnoList)
{
	CString olText = "";
	CRowColArray olRows;

	ropUrnoList.RemoveAll();

	int ilSelCount = (int)GetSelectedRows( olRows);

	if(ilSelCount > 0)
	{	
		for (int ilLc = olRows.GetSize()-1;ilLc >= 0 ; ilLc--)
		{			
			int ilDummy = (int)olRows[ilLc];
			if(ilDummy > 0)
			{
				olText = GetValueRowCol(olRows[ilLc], 7);
				ropUrnoList.Add(olText);
			}
		}
	}
	
}



//  Zeile markieren, in der opDgrUrno und (falls Einzeldarstellung) 
//	opRudUrno dargestellt werden
bool CDutyGrid::MarkLine ( CString opDgrUrno, CString opRudUrno/*=""*/ )
{
	ROWCOL ilDgrIdx=0, ilRudIdx=0;
	ROWCOL ilRowCount, i, ilRow=0, ilCol;

	//  Wenn einzelne Demands dargestellt und opRudUrno nicht leer ist, 
	//	Zeile f�r opRudUrno suchen

	bool blSearchRud = bmExpanded && !opRudUrno.IsEmpty() ;
	bool blRet;
	
	ilRowCount = GetRowCount();
	for ( i=1; (i<=ilRowCount) && !ilRow; i++ )
	{
		if ( !ilDgrIdx && (GetValueRowCol ( i, 6 ) == opDgrUrno) )
		{
			ilDgrIdx = i;
			if ( !blSearchRud )
				ilRow = ilDgrIdx;
		}
		if ( blSearchRud && (GetValueRowCol ( i, 7 )==opRudUrno) )
			ilRow = i;
	}
	blRet = (ilRow>0); 
	ilCol = ilRow ? 1 : 0;
	SetCurrentCell ( ilRow, ilCol );
	return blRet;
}
//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
//Logik f�r die Hintergrundfabe bei Gruppen
// Check if color for background link is already in map
// If not, add it. The color will be used in DrawLink() in the Gantt
// This function handles only indices, the actual colors are stored 
// in the rmLinkColors array which is filled in the gantt's constructor
// IN:		CString opUlnk - linking Urno (RUD.ULNK)
// RETURN:	long - Index of corresponding color in array
long CDutyGrid::CreateLinkBkColor(CString opUlnk)
{
	long llColorIdx = MAXLINKCOLORS - 1;
	CString olUlnk = opUlnk;

	olUlnk.TrimRight();
	if (olUlnk.IsEmpty() == NULL)
	{
		if (omUsedLinkColors.Lookup(opUlnk, (void*&)llColorIdx) == FALSE)
		{
			// if all colors in array have been used, 
			// start over with the first one
			if (++imActiveLinkColorIdx >= MAXLINKCOLORS) 
					imActiveLinkColorIdx = 0;

			omUsedLinkColors.SetAt(opUlnk, (void *)imActiveLinkColorIdx);
			llColorIdx = imActiveLinkColorIdx;
		}
		return rmLinkColors[llColorIdx];
	}
	return WHITE;
	
}
//---------------------------------------------------------------------------

long CDutyGrid::GetLinkBkColor(CString opUlnk)
{
	long llColorIdx = MAXLINKCOLORS-1;
	omUsedLinkColors.Lookup(opUlnk, (void*&)llColorIdx);
	return rmLinkColors[llColorIdx];
}
//---------------------------------------------------------------------------

void CDutyGrid::ResetLinkColorIndex()
{
	imActiveLinkColorIdx = 0;
	omUsedLinkColors.RemoveAll();
}
//---------------------------------------------------------------------------

void CDutyGrid::OnDeleteLink(CString opUlnk)
{
	omUsedLinkColors.RemoveKey(opUlnk);
}


CColDutyGrid::CColDutyGrid(CWnd *pParent)
		:CDutyGrid(pParent)
{
	bmExpanded=true;
}

void CColDutyGrid::DisplayRudTmp(bool bpInitLinkColors /*true*/)
{
	// added by mne 001019
	SetReadOnly(FALSE);
	SetEnabledParam(TRUE);
	LockUpdate();
	// end mne

	ResetContent();	

	if(bpInitLinkColors)
	{
		ResetLinkColorIndex();
	}
	CStringList olUdgrList;
	CString olUdgr;
	int i, ilRudCount = ogBCD.GetDataCount( "RUD_TMP" );
	
	// MNE TEST 001110
	RudTmp_Dump();
	// END TEST

	//  List all UDGR's create 

	for ( i=0; i<ilRudCount; i++ )
	{
		olUdgr = ogBCD.GetField ( "RUD_TMP", i, "UDGR" );
	
		if ( ( olUdgr!="NULL" ) && !olUdgrList.Find(olUdgr) )
			olUdgrList.AddTail ( olUdgr );
	}
	
	//  Alle Demand-Groups darstellen
	POSITION	pos;
	pos = olUdgrList.GetHeadPosition();
	while ( pos )
	{
		olUdgr = olUdgrList.GetNext (pos);
		
		DisplayDemandGroup ( olUdgr );
	}
	if ( bmExpanded )
		SortTable ( "9,8", "A,A" );
	// added by mne 001019
	SetEnabledParam(FALSE);
	LockUpdate(FALSE);
	Redraw();
	SetReadOnly();
	// end mne
	if ( GetRowCount() > 1) 
	{	//  workaround, damit in Detailcontrols auch wirklich die 1. Duty dargestellt wird
		SetCurrentCell ( 0, 0 );
		SetCurrentCell ( 1, 0 );
	}
}

void CColDutyGrid::IniLayout()
{

	//--- set number of columns
	SetColCount(10);

	//--- set column widths
	CRect olRect;
	GetClientRect(&olRect);

	olRect.DeflateRect(7, 0);

	int ColWidth0 = 25;
	int ColWidth1 = (olRect.Width()- ColWidth0) / 9;
	
	SetColWidth(0, 0, ColWidth0);	// header
	SetColWidth(1, 1, ColWidth1);	// Code
	SetColWidth(2, 2, ColWidth1*3);	// Name
	SetColWidth(3, 3, ColWidth1);	// Personal
	SetColWidth(4, 4, ColWidth1);	// Equipment
	SetColWidth(5, 5, ColWidth1);	// Location
	SetColWidth(10, 10, ColWidth1*2);	// Location
	// Hidden columns:
	// 6	-	UDGR
	// 7	-   URUD
	// 8	-   DISP
	// 9	-   ULNK
	GetParam()->SetNumberedColHeaders(FALSE);                 
	GetParam()->SetNumberedRowHeaders(FALSE); 
	GetParam()->EnableTrackRowHeight(FALSE); 
	GetParam()->EnableTrackColWidth(FALSE);
	GetParam()->EnableSelection	(GX_SELROW|GX_SELMULTIPLE);

	ChangeColHeaderStyle(CGXStyle().SetEnabled(FALSE));
	SetValueRange(CGXRange(0,1), GetString(CODE) );
	SetValueRange(CGXRange(0,2), GetString(NAME));
	SetValueRange(CGXRange(0,3), GetString(IDS_PERSONAL));
	SetValueRange(CGXRange(0,4), GetString(IDS_EQUIPMENT));
	SetValueRange(CGXRange(0,5), GetString(IDS_LOCATION));
	SetValueRange(CGXRange(0,10), GetString(IDS_STRING1401) );

	SetStyleRange(CGXRange().SetCols(3,5), 
				  CGXStyle().SetHorizontalAlignment(DT_CENTER));

	//--- these columns are only used to save data 
	HideCols(6, 9); 

	//--- row height can't be changed
	GetParam()->EnableMoveRows(false);
	
	ResetLinkColorIndex();
	EnableGridToolTips(TRUE);
	SetReadOnly();
}

bool CColDutyGrid::DisplayDemandGroup ( CString &ropUdgr )
{
 	CString					olText, olUrud;
	int						i, ilRows;

	if ( CDutyGrid::DisplayDemandGroup ( ropUdgr ) && bmExpanded )
	{
	
	
		ilRows = GetRowCount ();		//GetParam()->SetLockReadOnly(FALSE);
	 		

	    for ( i=1; i<=ilRows; i++ )
		{
			olUrud = GetValueRowCol ( i, 7 );
		
     		if (!olUrud.IsEmpty() && GetSingleRules ( olUrud, olText ) )
			{
			
				SetValueRange( CGXRange(i,10), olText );
				SetStyleRange( CGXRange(i,10), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, olText ));

				
			}
		}
 
       CString rule, code; //QDO
      
		for ( i=1; i<=ilRows; i++ )
		{
	       rule=""; code="";
           CString name= GetValueRowCol ( i, 10);
		   int a=name.Find(',');
		   if (a!=-1)
		   {
		      if (name[0]==name[a+1] && name[1]==name[a+2]&& name[2]==name[a+3])
			 {
			    int n=name.ReverseFind(',');
				  
				rule=name.Left(name.GetLength()-1-n);
				
				code=GetValueRowCol ( i, 1);
		

                for (int j=1; j<=ilRows; j++ )
				{
				
				  if (GetValueRowCol (j, 1)==code)
				  {
					 
			           SetValueRange( CGXRange(j,10), rule );
			           SetStyleRange( CGXRange(j,10), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, rule ));
				  }
							
				}
			 }
		   }
		}   

	}
	return true;
}
/*
bool CColDutyGrid::OnNewRudTmp ( RecordSet *popRecord )
{
	CString olUrno, olUdgr, olRety, olWert,olSerUrno, olSerSnam, olSerCode, olDisp, olUlnk, olText ;
	BOOL	blRet=FALSE;
	int		i;
	BOOL blLock = GetParam()->IsLockReadOnly();

	if ( blLock )
		GetParam()->SetLockReadOnly(FALSE);

	olSerUrno = popRecord->Values[igRudUghsIdx];
	olSerSnam = ogBCD.GetField("SER", "URNO", olSerUrno, "SNAM");
	olSerCode = ogBCD.GetField("SER", "URNO", olSerUrno, "SECO");
	olUdgr = popRecord->Values[igRudUdgrIdx];	
	olUrno = popRecord->Values[igRudUrnoIdx];
	olRety = popRecord->Values[igRudRetyIdx];
	olUlnk = popRecord->Values[igRudUlnkIdx];	
	olDisp = popRecord->Values[igRudDispIdx];
	if ( olRety.GetLength() < 3 )
		olRety = "100";		//  default-Fall

	if ( bmExpanded )
	{
		for ( i=GetRowCount(); i>=1; i-- )
		{	
			olWert = GetValueRowCol ( i, 6 );
			if ( olWert == olUdgr )
			{
				blRet = InsertRows(i+ 1, 1);
				if ( blRet )
				{
					SetValueRange ( CGXRange(i+1,1), olSerCode );
					SetValueRange ( CGXRange(i+1,2), olSerSnam );
					SetValueRange ( CGXRange(i+1,3), olRety.Left(1) );
					SetValueRange ( CGXRange(i+1,4), olRety.Mid(1) );
					SetValueRange ( CGXRange(i+1,5), olRety.Right(1) );
					SetValueRange ( CGXRange(i+1,6), olUdgr );
					SetValueRange ( CGXRange(i+1,7), olUrno );
					SetValueRange ( CGXRange(i+1,8), olDisp );
					SetValueRange ( CGXRange(i+1,9), olUlnk );
					if ( !olUrno.IsEmpty() &&
						 GetSingleRules ( olUrno, olText ) )
					{
		 				SetValueRange ( CGXRange(i+1, 10), olText );
						SetStyleRange( CGXRange(i+1, 10), 
									   CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, olText ));
					}
				}
				break;
			}
		}
	}

	if ( blLock )
		GetParam()->SetLockReadOnly(TRUE);
	return (blRet) ? true : false;
}
*/

bool CColDutyGrid::GetSingleRules ( CString &ropUrud, CString &ropRuty0Codes )
{
	CCSPtrArray<RecordSet>	olRudsRuty0;
	RecordSet  olRecord( ogBCD.GetFieldCount("RUE"));
	CString					olUrue, olRusnTyp0, olRustTyp0;

	ropRuty0Codes.Empty();
	ogBCD.GetRecords( "RUD", "UCRU", ropUrud, &olRudsRuty0 );
	for ( int i=0; i<olRudsRuty0.GetSize(); i++ )
	{
		olUrue = olRudsRuty0[i].Values[igRudUrueIdx];
		if ( ogBCD.GetRecord("RUE", "URNO", olUrue, olRecord ) )
		{
			olRusnTyp0 = olRecord.Values[igRueRusnIdx];
			olRustTyp0 = olRecord.Values[igRueRustIdx];
			if ( olRustTyp0 != "2" )  // keine Archivierten
			{
				if ( !ropRuty0Codes.IsEmpty() )
					ropRuty0Codes += ",";
				ropRuty0Codes += olRusnTyp0;
			}
		}
	}
	olRudsRuty0.DeleteAll();
	return true;
}
