//***********************************************************************************************************************
//
//									RegelwerkView.h
//
//***********************************************************************************************************************
//
//	Author:	MNE			Date:	Apr 30, 1999
//
//	
//	--- FLAGS:
//			- bmSCRule is TRUE, if single/collective rule was chosen (via the togglebutton). You should see this in the 
//				main window's caption. The toggle button shows "U" (German) resp. "I" (English) in that case. Set TRUE
//				at start of program and every time the toggle button is pressed to get S/C rules layout. FALSE if toggle 
//				button is pressed to get layout for flight independent rules. 
//			- bmSingle is TRUE, if the "Single" radio button is checked. If it is hidden or if the "Collective" radio 
//				button is checked, it's FALSE.
//			- bmTurnaround is TRUE if Turnaround is chosen
//			- bmInbound is TRUE if Inbound is chosen
//			- bmOutbound is TRUE if Outbound is chosen 
//
// --- WINDOW STATES (ACTIVATED/DEACTIVATED/HIDDEN):
//			- The method SetBdpsState() handles all (de-)activating of controls according to the flags set
//
//***********************************************************************************************************************
//
// Modification history:
// =====================
//
//	Last change:				Date:	
//
//  {Explain here what you've changed)
//
//***********************************************************************************************************************


#if !defined(AFX_REGELEDITOR_H__01F9D1C5_B50B_11D2_AAF2_00001C018CF3__INCLUDED_)
#define AFX_REGELEDITOR_H__01F9D1C5_B50B_11D2_AAF2_00001C018CF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RegelEditor.h : header file
//

#include <resource.h>
#include <FlightIndepRulesView.h>
#include <DialogGrid.h>
#include <MainFrm.h>
#include <RulesGantt.h>
#include <RulesGanttViewer.h>
#include <CCSEdit.h>
#include <ValidityDlg.h>
#include <RelativeTimeScale.h>
#include <RulesFormView.h>


#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

extern char SOH;
extern char STX;
extern char ETX;

typedef struct ControlStates
{
	CWnd*	pWnd;	// pointer to control window
	CRect   rect;	// rect (screen coord.)
	int		ID;		// control ID

	ControlStates(void)
	{
		pWnd = NULL;
	}
} WNDSTATE;


class CRegelEditorFormView : public CRulesFormView
{

public:

	CRegelEditorFormView();   // protected constructor used by dynamic creation
	
	DECLARE_DYNCREATE(CRegelEditorFormView)

// Form Data
	//{{AFX_DATA(CRegelEditorFormView)
	enum { IDD = IDD_REGELEDITOR_FORM };
	CStatic	m_Sta_Vrgc;
	CStatic	m_Sta_Vato;
	CStatic	m_Sta_Vafr;
	CStatic	m_Sta_Rema;
	CStatic	m_Sta_Perm;
	CStatic	m_Sta_Noma;
	CStatic	m_Sta_Nfco;
	CStatic	m_Sta_Lchg;
	CStatic	m_Sta_Duen;
	CStatic	m_Sta_Dube;
	CStatic	m_Sta_Day7;
	CStatic	m_Sta_Day6;
	CStatic	m_Sta_Day5;
	CStatic	m_Sta_Day4;
	CStatic	m_Sta_Day3;
	CStatic	m_Sta_Day2;
	CStatic	m_Sta_Day1;
	CStatic	m_Sta_Daily;
	CButton	m_Fra_Empl;
	CButton	m_Fra_Days;
	CListBox	m_Edt_Vrgc;
	CEdit	m_Edt_Vato;
	CEdit	m_Edt_Vafr;
	CEdit	m_Edt_Rema;
	CListBox	m_Edt_Perm;
	CEdit	m_Edt_Noma;
	CEdit	m_Edt_Nfco;
	CEdit	m_Edt_Lchg;
	CEdit	m_Edt_Duen;
	CEdit	m_Edt_Dube;
	CButton	m_Chb_Day7;
	CButton	m_Chb_Day6;
	CButton	m_Chb_Day5;
	CButton	m_Chb_Day4;
	CButton	m_Chb_Day3;
	CButton	m_Chb_Day2;
	CButton	m_Chb_Day1;
	CButton	m_Chb_Daily;
	CButton	m_Btn_FullList;
	CStatic	m_Sta_Rotation;
	CStatic	m_Sta_Prsn;
	CStatic	m_Sta_Prna;
	CStatic	m_Sta_Arrival;
	CStatic	m_Sta_Departure;
	CButton	m_Fra_Turn;
	CButton	m_Fra_Split;
	CButton m_RBtn_Inbound;
	CButton m_RBtn_Outbound;
	CButton m_RBtn_Turnaround;
	CButton m_RBtn_Absolute;
	CButton m_RBtn_Relative;
	CStatic	m_Sta_MinTime;
	CCSEdit	m_Edt_MinTime;
	CStatic	m_Sta_Maxt2;
	CStatic	m_Sta_Maxt;
	CButton	m_Fra_IOT;
	CButton	m_Chb_Fspl;
	CButton	m_Chb_Excl;
	CButton	m_Fra_Excl;
	CButton	m_Chb_Acti;
	CButton	m_Fra_Acti;
	CCSEdit	m_MaxtEdit;
	CCSEdit	m_RuleNameEdit;
	CCSEdit	m_RuleShortNameEdit;
	//}}AFX_DATA



//------- eigene Funktionen
public:
	//void TablesFirstTimeInit(CGridFenster *popGrid);
	bool InitializeTable(CGridFenster* popGrid, const CString& opWhichPart);
	void ClearTables();
	void ResetTable(CGridFenster *popGrid);
//	static void ConstructString(CGridFenster* popGrid, CString& opResult); // F�r Speicherung
//	static void ReConstructString(CGridFenster* popGrid, CString& opString); // F�r Laden
	CString ConstructEVRM();
	CString ConstructPrioString();
	void OnClickedGridButton(ROWCOL nRow, ROWCOL nCol);
//	bool RemoveRessources(CStringArray &opRudUrnos);
//	bool CopyRessources(CStringArray &opRudUrnos);
//	bool CopyDemandRequirements();
	bool SaveDemandRequirements();
//	bool SaveValidity();
//	bool SaveQualifications();
//	bool SaveFunctions();
//	bool SaveLocations(); 
//	bool SaveEquipment();
	
	void SaveBarDisplayPositions();

//	bool CheckIfLineEmpty(CGridFenster *popGrid, int ipRow, int ipMaxCol);
//	void MakeTimeScale();
//	void MakeRelativeTimeScale();
//	void MakeGantt();
	void MakeViewer();
	void MakeSortableGridCombo(CGridFenster *popGrid);


	void AddGridListItem(CGridFenster *popGrid, CString opTable, RecordSet *popRecord/*CString opItem*/);
	void RemoveGridListItem(CGridFenster *popGrid, CString opTable, RecordSet *popRecord);
	void RemoveDynGrp(CGridFenster *popGrid, RecordSet *popDgr);
	void SetIOT(int ipIOT); 
	void SetRBtnSC(int ipSCI);
	BOOL SetGridEnabled(CGridFenster *popGrid, bool bpEnable);
	void SetTSStartTime(CTime opTSStartTime);
	void SetEvorBitmap(); 
	void SetBdpsState();
	void SetStaticTexts();
	
	
	bool IsBarTypeMatching(CString opBarUrno, CString opIOT);
	bool CheckIOTChange(int ipIOT);
	
	void SetTurnaround();
	void SetInbound();
	void SetOutbound();
	void SetPreviousIOT();
	void SetExclusionRule(bool bpIsExclude);

	CString GetRuleEventType();

	// message handling functions (called from MainFrm.cpp)
	void OnLoadRule(CString opRueUrno, CString opTemplateName, bool bpNoComboChange);
	void OnTemplateEditor();
	void OnNewRule();
	bool OnSaveRule();
	BOOL OnHelpInfo(HELPINFO* pHelpInfo) ;
//	void OnDelete();
	void OnLeistungen();
//	void OnGroupBtn();
	void OnSelEndOK();
	void OnCopy();
	void AddServices ( CStringArray	&ropSerUrnos );
	void CloseDetailWindows();
	void CalcGanttRect( LPRECT lpRect );
	void CalcTimeScaleRect( LPRECT lpRect );
	bool CheckChanges();

	// a little routine for the test of CheckChanghes()
	void Debug(bool bRes, int &idx);
	// end test


//---- Attribute
public:
//	CString omCurrRueUrno;		// Urno of GPM currently shown

//	CTime omStartTime;
//	CTimeSpan omDuration;
	
//	CTime omTSStartTime;
//	CTimeSpan omTSDuration;
//	CTimeSpan omTSInterval;
	
	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 
	
//	CTime omOnBlock;
//	CTime omOfBlock;

	//--- Abgeleitet von CWnd 
	CGridFenster *pomRotGrid;
	CGridFenster *pomArrGrid;
	CGridFenster *pomDepGrid;

	//-- comboboxes 
	CGXComboBoxWnd *pomRotCombo;
	CGXComboBoxWnd *pomArrCombo;
	CGXComboBoxWnd *pomDepCombo;

	//-- Main frame window
//	CMainFrame *pomFrameWnd;
//	CStatusBar *pomStatusBar;
//	CComboBox *pomComboBox;
	
	//--- gantt
//	RulesGanttViewer *pomViewer;
//	RulesGantt *pomGantt;
//  CCSTimeScale *pomTimeScale;
//	RelativeTimeScale *pomRelTimeScale;

	//--- some gantt attributes
//	int imVerticalScaleIndent;
//	int imVerticalScaleWidth;


	CCSPtrArray <WNDSTATE> rmIndepWnds;
	CCSPtrArray <WNDSTATE> rmSCWnds;
	
	//--- flags
//	bool bmNewFlag;			// Was new rule created ?
	// bool bmIsValChanged;	// Has ValData been changed ?
	bool bmNoComboChange;	// Has selected template changed ?
	bool bmSCRule;			// single/collective rule chosen ?
	bool bmRelative;
	bool bmSingle;			// "Single" radio button checked ? 
	bool bmInbound;			// "Inbound" radio button checked ?
	bool bmOutbound;		// "Outbound" radio button checked ?
	bool bmTurnaround;		// "Turnaround" radio button checked ?
	
	bool bmRuleChanged;		// anything changed ?
	bool bmRudChanged;		// any RUD changed ?

	bool bmIsLoading;		// is set true at start of loading procedure and reset after
	
	int imIOT;

	CBitmap omVL2RBmp;
	CBitmap omHL2RBmp;
	CBitmap omVR2LBmp;
	CBitmap omHR2LBmp;

	HBITMAP rmOldEvorBitmap;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegelEditorFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CRegelEditorFormView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
//	afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	//{{AFX_MSG(CRegelEditorFormView)
	afx_msg void OnInbound();
	afx_msg void OnOutbound();
	afx_msg void OnTurnaround();
	afx_msg void OnEndEditing(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridButton(WPARAM wParam, LPARAM lParam);
	afx_msg void OnAbsolute();
	afx_msg void OnRelative();
	afx_msg void OnPaint();
	afx_msg void OnFsplChb();
	afx_msg void OnExcludeChb();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()


//-----------------------------------------------------------------------------------------
//					Processing functions
//-----------------------------------------------------------------------------------------
public:
	void ProcessDgrNew(RecordSet *popRecord);
	void ProcessDgrChange(RecordSet *popRecord);
	void ProcessDgrDelete(RecordSet *popRecord);
	void ProcessPerNew(RecordSet *popRecord);
	void ProcessPerChange(RecordSet *popRecord);
	void ProcessPerDelete(RecordSet *popRecord);
	void ProcessPstNew(RecordSet *popRecord);
	void ProcessPstChange(RecordSet *popRecord);
	void ProcessPstDelete(RecordSet *popRecord);
	void ProcessRudNew(RecordSet *popRecord);
	void ProcessRudChange(RecordSet *popRecord);
	void ProcessRudDelete(RecordSet *popRecord);
	void ProcessRueNew(RecordSet *popRecord);
	void ProcessRueChange(RecordSet *popRecord);
	void ProcessRueDelete(RecordSet *popRecord);
	void ProcessSgmNew(RecordSet *popRecord);
	void ProcessSgmChange(RecordSet *popRecord);
	void ProcessSgmDelete(RecordSet *popRecord);
	void ProcessSgrNew(RecordSet *popRecord);
	void ProcessSgrChange(RecordSet *popRecord);
	void ProcessSgrDelete(RecordSet *popRecord);	
	void ProcessTplNew(RecordSet *popRecord);
	void ProcessTplChange(RecordSet *popRecord);
	void ProcessTplDelete(RecordSet *popRecord);
	

	// testing functions
	void Dump1(CString opMsg, int ipXVal, CTime opTime);

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGELEDITOR_H__01F9D1C5_B50B_11D2_AAF2_00001C018CF3__INCLUDED_)
