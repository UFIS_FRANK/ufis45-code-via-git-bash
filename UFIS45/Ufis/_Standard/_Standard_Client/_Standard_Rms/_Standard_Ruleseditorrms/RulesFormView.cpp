// RulesFormView.cpp : implementation file
//

#include <stdafx.h>

#include <BasicData.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <regelwerk.h>
#include <RelativeTimeScale.h>
#include <mainfrm.h>
#include <RulesGantt.h>

#include <RulesFormView.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif


/////////////////////////////////////////////////////////////////////////////
// CRulesFormView

IMPLEMENT_DYNCREATE(CRulesFormView, CFormView)

CRulesFormView::CRulesFormView()
	: CFormView(CRulesFormView::IDD)
{
	IniDataMembers();
}

CRulesFormView::CRulesFormView(UINT nIDTemplate)
	: CFormView(nIDTemplate)
{
	IniDataMembers();
}

CRulesFormView::~CRulesFormView()
{
	if ( pomTimeScale )
		delete pomTimeScale;
	if ( pomRelTimeScale )
		delete pomRelTimeScale;
	if ( pomGantt )
		delete pomGantt;
	if ( pomViewer )
		delete pomViewer;
}

void CRulesFormView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRulesFormView)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Control(pDX, IDC_HSCROLL, m_HScroll);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRulesFormView, CFormView)
	//{{AFX_MSG_MAP(CRulesFormView)
	ON_WM_MOUSEACTIVATE()
	ON_WM_HSCROLL()
	ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
	ON_MESSAGE(WM_SERLIST_NOLINK,OnCreateNoGroup)
	ON_MESSAGE(WM_SERLIST_LINK,OnCreateGroup)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRulesFormView diagnostics

#ifdef _DEBUG
void CRulesFormView::AssertValid() const
{
	CFormView::AssertValid();
}

void CRulesFormView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRulesFormView message handlers

void CRulesFormView::IniDataMembers ()
{
	//{{AFX_DATA_INIT(CRulesFormView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	bmNewFlag = false;

	omCurrRueUrno = "";	// currently active rule

	pomStatusBar = NULL;
	pomTimeScale = NULL;
	pomRelTimeScale = NULL;
	pomViewer = NULL;
	pomGantt = NULL;
	pomFrameWnd = NULL;
	pomComboBox = NULL;
	
	//-- some gantt attributes
	//-- anyone changing these values is himself responsible for incorrect gantt updating !!!
	imVerticalScaleIndent = 2;
	imVerticalScaleWidth = 150; 
	omOnBlock = CTime(1980, 1, 1, 2, 1,	0);
	omOfBlock = CTime(1980, 1, 1, 6, 1, 0);
	pomGridToPrint = 0;
	pomWndCalledPreview = 0;
}
//----------------------------------------------------------------------------------------------------------------------

void  CRulesFormView::ConstructString(CGridFenster* popGrid, CString &ropResult, int ipMaxLen/*=-1*/ ) 
{
	CCS_TRY


    int ilCount = popGrid->GetRowCount();

	ropResult.Empty(); 

    for(int i = 0; i < ilCount; i++)
	{   
		CString  olTable  = popGrid->GetValueRowCol(i + 1, 7); // Tabelle
		CString  olSimpleVal = popGrid->GetValueRowCol(i + 1, SIMPLE_VAL); 
		CString  olDynGrp = popGrid->GetValueRowCol(i + 1, DYNAMIC_GRP); 
		CString  olStatGrp = popGrid->GetValueRowCol(i + 1, STATIC_GRP); 

		if(olSimpleVal.IsEmpty() == FALSE && olSimpleVal != CString(" "))
		{
			popGrid->UnFormatSpecial ( i + 1, 2, olSimpleVal ); 
            ropResult += CString("0.") + olTable + CString("=") + olSimpleVal + CString(";");
		}
		else if(olStatGrp.IsEmpty() == FALSE && olStatGrp != CString(" "))
		{
            ropResult += CString("2.") + olTable + CString("=") + olStatGrp + CString(";");
		}
		else if(olDynGrp.IsEmpty() == FALSE && olDynGrp != CString(" "))
		{
            ropResult += CString("1.") + olTable + CString("=") + olDynGrp + CString(";");
		}
	}

	// remove ";" from end of string
	if (ropResult.Right(1) == CString(";"))
	{
		ropResult = ropResult.Left(ropResult.GetLength() - 1);
	}
	if ( (ipMaxLen >= 0) && (ropResult.GetLength() > ipMaxLen) )
	{
		ropResult = ropResult.Left(ipMaxLen-1);
		ropResult += '~';
	}

	CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------

void  CRulesFormView::ReConstructString(CGridFenster* popGrid, CString &ropString) 
{
	CCS_TRY


      //--- Zeilen in der Tabelle
      int ilRowCount = popGrid->GetRowCount();

	  //--- String zerlegen 
	  CStringArray olDataArray;
	  int ilData = ExtractItemList(ropString, &olDataArray, ';');

	  //--- more data than rows in grid ? Uuups !!!
	  if(ilRowCount < ilData)
	  {
		  AfxMessageBox(GetString(165));
		  //return;
	  }

	  //--- write data to grid
	  int ilPos = -1;
	  CString olData;
	  CString olValueType;
	  CString olValue;
	  CString olFieldName;
	  CString olRowContents;

      for(int i = 0; i < ilData; i++)
	  {   
		   olData = olDataArray[i];
		   if (olData.IsEmpty() == FALSE && olData != CString(" "))
		   {
			   ilPos = olData.Find("=");
			   if(ilPos > 0)
			   {
				   olValueType = olData.Left(1);
				   olFieldName = olData.Mid(2,8);
				   olValue = olData.Right(olData.GetLength() - ilPos - 1);
				   
				   for (int ilRow = 1; ilRow <= ilRowCount; ilRow++)
				   {
						olRowContents = popGrid->GetValueRowCol(ilRow, 7);
						if (olFieldName == olRowContents)
						{
							if(atoi(olValueType) == 0)	// simple value
							{
								popGrid->FormatSpecial(ilRow, SIMPLE_VAL, olValue);
								popGrid->SetValueRange(CGXRange(ilRow, SIMPLE_VAL), olValue);
								popGrid->SetToolTipForValue(ilRow, SIMPLE_VAL, olValue);
								popGrid->MarkInValid(ilRow, SIMPLE_VAL, olValue);
							}
							if(atoi(olValueType) == 2)	// static value
							{
								popGrid->SetValueRange(CGXRange(ilRow, STATIC_GRP), olValue);
								popGrid->SetToolTipForValue(ilRow, STATIC_GRP, olValue);
								popGrid->MarkInValid(ilRow, STATIC_GRP, olValue);
							}
						    if(atoi(olValueType) == 1)	// dynamic value
							{
								popGrid->SetValueRange(CGXRange(ilRow, DYNAMIC_GRP), olValue);
								popGrid->SetToolTipForValue(ilRow, DYNAMIC_GRP, olValue);
								popGrid->MarkInValid(ilRow, DYNAMIC_GRP, olValue);
							}
							break;	//  conventions !? Mmh...ain't it faster this way !?
					   }
				   }
			   }
		   }
	  }


	CCS_CATCH_ALL
}
//------------------------------------------------------------

bool CRulesFormView::SaveQualifications() 
{
	bool blRet = true;

	ogLog.Trace("RUNORDER", "Entering CRulesFormView::SaveQualifications...");

	CCS_TRY

	
	RecordSet olRecord(ogBCD.GetFieldCount("RPQ_TMP"));

	int ilCount = ogBCD.GetDataCount("RPQ_TMP");
	
	if (ilCount > 0)
	{
		for (int i = 0; i < ilCount; i++)
		{
			bool blErr1 = ogBCD.GetRecord("RPQ_TMP", i, olRecord);		
			bool blErr2 = ogBCD.InsertRecord("RPQ", olRecord);
			if (blErr1 == false || blErr2 == false)
			{
				ogLog.Trace("DEBUG", "[CRulesFormView::SaveQualifications] Failed appending RPQ record [index %d] to object.", i);
				blRet = false;
				break;
			}
		}

		// save RUD records to database
		if (blRet)
		{
			bool blErr = ogBCD.Save("RPQ");
			if (blErr == true)
			{
				ogLog.Trace("WRITE2DB", "[CRulesFormView::SaveQualifications] Saved %d RPQ records to database", i);
			}
			else
			{
				blRet = false;
				ogLog.Trace("DEBUG","[CRulesFormView::SaveQualificatons] Failed saving RPQ records to database");
			}
		}
	}


	CCS_CATCH_ALL

	ogLog.Trace("RUNORDER", "Exiting CRulesFormView::SaveQualifications...");

	return blRet;
}
//------------------------------------------------------------

bool CRulesFormView::SaveFunctions() 
{
	bool blRet = true;


	CCS_TRY

	
	RecordSet olRecord(ogBCD.GetFieldCount("RPF_TMP"));

	int ilCount = ogBCD.GetDataCount("RPF_TMP");
	
	if (ilCount > 0)
	{
		for (int i = 0; i < ilCount; i++)
		{
			bool blErr1 = ogBCD.GetRecord("RPF_TMP", i, olRecord);		
			bool blErr2 = ogBCD.InsertRecord("RPF", olRecord);
			if (blErr1 == false || blErr2 == false)
			{
				ogLog.Trace("DEBUG", "[CRulesFormView::SaveFunctions] Failed appending RPF record [index %d] to object.", i);
				blRet = false;
				break;
			}
		}

		// save RUD records to database
		if (blRet)
		{
			bool blErr = ogBCD.Save("RPF");
			if (blErr == true)
			{
				ogLog.Trace("WRITE2DB", "[CRulesFormView::SaveFunctions] Saved %d RPF records to database", i);
			}
			else
			{
				blRet = false;
				ogLog.Trace("DEBUG","[CRulesFormView::SaveFunctions] Failed saving RPF records to database");
			}
		}
	}


	CCS_CATCH_ALL

	ogLog.Trace("RUNORDER", "Exiting CRulesFormView::SaveFunctions...");

	return blRet;
}
//------------------------------------------------------------

bool CRulesFormView::SaveLocations() 
{
	bool blRet = true;

	CCS_TRY


	RecordSet olRecord(ogBCD.GetFieldCount("RLO_TMP"));

	int ilCount = ogBCD.GetDataCount("RLO_TMP");
	
	if (ilCount > 0)
	{
		for (int i = 0; i < ilCount; i++)
		{
			bool blErr1 = ogBCD.GetRecord("RLO_TMP", i, olRecord);		
			bool blErr2 = ogBCD.InsertRecord("RLO", olRecord);
			if (blErr1 == false || blErr2 == false)
			{
				ogLog.Trace("DEBUG", "[CRulesFormView::SaveLocations] Failed appending RLO record [index %d] to object.", i);
				blRet = false;
				break;
			}
		}

		// save RUD records to database
		if (blRet)
		{
			bool blErr = ogBCD.Save("RLO");
			if (blErr == true)
			{
				ogLog.Trace("WRITE2DB", "[CRulesFormView::SaveLocations] Saved %d RLO records to database", i);
			}
			else
			{
				blRet = false;
				ogLog.Trace("DEBUG","[CRulesFormView::SaveLocations] Failed saving RLO records to database");
			}
		}
	}

	CCS_CATCH_ALL

	return blRet;
}
//------------------------------------------------------------

bool CRulesFormView::SaveEquipment() 
{
	bool blRet = true;


	CCS_TRY

	
	RecordSet olRecord(ogBCD.GetFieldCount("REQ_TMP"));

	int ilCount = ogBCD.GetDataCount("REQ_TMP");
	
	if (ilCount > 0)
	{
		for (int i = 0; i < ilCount; i++)
		{
			bool blErr1 = ogBCD.GetRecord("REQ_TMP", i, olRecord);		
			CString olUrno = olRecord.Values[igReqUrnoIdx];
			bool blErr2 = ogBCD.InsertRecord("REQ", olRecord);
			if (blErr1 == false || blErr2 == false)
			{
				ogLog.Trace("DEBUG", "[CRulesFormView::SaveFunctions] Failed appending REQ record [index %d] to object.", i);
				blRet = false;
				break;
			}
		}

		// save RUD records to database
		if (blRet)
		{
			bool blErr = ogBCD.Save("REQ");
			if (blErr == true)
			{
				ogLog.Trace("WRITE2DB", "[CRulesFormView::SaveEquipment] Saved %d REQ records to database", i);
			}
			else
			{
				blRet = false;
				ogLog.Trace("DEBUG","[CRulesFormView::SaveEquipment] Failed saving REQ records to database");
			}
		}
	}

	CCS_CATCH_ALL

	ogLog.Trace("RUNORDER", "Exiting CRulesFormView::SaveEquipment...");

	return blRet;
}

//-----------------------------------------------------------------------------------------------------------------------
/*
bool CRulesFormView::SaveValidity() 
{
	bool blReturn = true;

	CCS_TRY
	
	RecordSet olRecord(ogBCD.GetFieldCount("VAL"));
	
	olRecord.Values[igValUvalIdx] = omCurrRueUrno;
	olRecord.Values[igValTimtIdx] = pomFrameWnd->rmValData.FixT;
	olRecord.Values[igValTimfIdx] = pomFrameWnd->rmValData.FixF;
	
	// freq
	CString olFreq;
	olFreq = pomFrameWnd->rmValData.Freq;
	if (olFreq.IsEmpty() == TRUE || olFreq == CString(" ")) 
	{
		olFreq = CString("1111111");
	}
	olRecord.Values[igValFreqIdx] = olFreq;


//--- get exclusion periods and create VAL records
	CTime olVafr;
	CTime olVato;
	CTime olX;
	bool blErr = true;
	int ilCount = pomFrameWnd->rmValData.Excls.GetSize();
	int i;
	if (ilCount > 0)
	{
		for (i = 0; i < ilCount; i++)
		{
			if (i == 0)
			{
				olRecord.Values[igValVafrIdx] = pomFrameWnd->rmValData.Vafr;
				olVato = pomFrameWnd->rmValData.Excls[i].FirstDay;
				olVato -= CTimeSpan(1, 0, 0, 0); 
				olRecord.Values[igValVatoIdx] = CTimeToDBString(olVato, olX);
			}
			else if (i == ilCount - 1)
			{
				olRecord.Values[igValVatoIdx] = pomFrameWnd->rmValData.Vato;
				olVafr = pomFrameWnd->rmValData.Excls[i].LastDay;
				olVafr += CTimeSpan(1, 0, 0, 0); 
				olRecord.Values[igValVafrIdx] = CTimeToDBString(olVafr, olX);
			}
			else
			{
				olVafr = pomFrameWnd->rmValData.Excls[i - 1].LastDay;
				olVafr += CTimeSpan(1, 0, 0, 0);
				olRecord.Values[igValVafrIdx] = CTimeToDBString(olVafr, olX);
		
				olVato = pomFrameWnd->rmValData.Excls[i].FirstDay;
				olVato -= CTimeSpan(1, 0, 0, 0); 
				olRecord.Values[igValVatoIdx] = CTimeToDBString(olVato, olX);
			}

			blErr &= ogBCD.InsertRecord("VAL", olRecord, false);
		}
	}
	else
	{
		olRecord.Values[igValVafrIdx] = pomFrameWnd->rmValData.Vafr;
		olRecord.Values[igValVatoIdx] = pomFrameWnd->rmValData.Vato;
		blErr &= ogBCD.InsertRecord("VAL", olRecord, false);
	}
	
	if (blErr == false)
	{
		ogLog.Trace("DEBUG", "[CRulesFormView::SaveValidity] Failed appending a VAL record to object.");
		blReturn = false;
	}

	// save VAL record to database
	if (blReturn)
	{
		bool blErr = ogBCD.Save("VAL");
		if (blErr == true)
		{
			ogLog.Trace("WRITE2DB", "[CRulesFormView::SaveValidity] Saved %d VAL records to database", --i);
		}
		else
		{
			blReturn = false;
			ogLog.Trace("DEBUG","Failed saving a VAL record to database");
		}
	}
	
	CCS_CATCH_ALL
	
	return blReturn;
}
*/
//-----------------------------------------------------------------------------------------------------------------------

void CRulesFormView::SetTSStartTime(CTime opTSStartTime)
{
    omTSStartTime = opTSStartTime;
    if ((omTSStartTime < omStartTime) ||
		(omTSStartTime > omStartTime + omDuration))
    {
        omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
            0, 0, 0) - CTimeSpan(1, 0, 0, 0);
    }
}
//-----------------------------------------------------------------------------------------------------------------------

void CRulesFormView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CTimeSpan olSpan;
    int ilPos = 0;
	long llTotalMin = 0;

	if (pScrollBar && (pScrollBar == &m_HScroll))
	{
		if (pomTimeScale && pomRelTimeScale && pomViewer)
		{
			bool blNeg = false;
			switch (nSBCode)
			{
				case SB_LINELEFT:	// scroll one hour left
					ilPos = m_HScroll.GetScrollPos() - 2;
					if (ilPos <= 0)
					{
						ilPos = 0;
						SetTSStartTime(omStartTime);
					}
					else
					{
						SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));
					}

					pomTimeScale->SetDisplayStartTime(omTSStartTime);
					pomTimeScale->Invalidate(TRUE);
					pomRelTimeScale->SetDisplayStartTime(omTSStartTime);
					pomRelTimeScale->Invalidate(TRUE);
					pomViewer->ChangeViewTo(omCurrRueUrno);
					m_HScroll.SetScrollPos(ilPos, TRUE);
					break;
        
				case SB_LINERIGHT:		// scroll one hour right
					ilPos = m_HScroll.GetScrollPos() + 2;
					if (ilPos > 12)
					{
						ilPos = 12;
						CTimeSpan olSpan;
						GetTimeSpanFromMinutes(ilPos * 30, olSpan, blNeg);
						SetTSStartTime(omStartTime + olSpan);
					}
					else
					{
						SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));
					}
					pomTimeScale->SetDisplayStartTime(omTSStartTime);
					pomTimeScale->Invalidate(TRUE);
					pomRelTimeScale->SetDisplayStartTime(omTSStartTime);
					pomRelTimeScale->Invalidate(TRUE);
					pomViewer->ChangeViewTo(omCurrRueUrno);
					m_HScroll.SetScrollPos(ilPos, TRUE);
					break;
        
				case SB_PAGELEFT:
					ilPos = m_HScroll.GetScrollPos() - 6;
					if (ilPos <= 0)
					{
						ilPos = 0;
						SetTSStartTime(omStartTime);
					}
					else
					{
						SetTSStartTime(omTSStartTime - CTimeSpan(0, 3, 0, 0));
					}

					pomTimeScale->SetDisplayStartTime(omTSStartTime);
					pomTimeScale->Invalidate(TRUE);
					pomRelTimeScale->SetDisplayStartTime(omTSStartTime);
					pomRelTimeScale->Invalidate(TRUE);
					pomViewer->ChangeViewTo(omCurrRueUrno);
					m_HScroll.SetScrollPos(ilPos, TRUE);
					break;
        
				case SB_PAGERIGHT:
					ilPos = m_HScroll.GetScrollPos() + 6;
					if (ilPos >= 12)
					{
						ilPos = 12;
						CTimeSpan olSpan;
						GetTimeSpanFromMinutes(ilPos * 30, olSpan, blNeg);
						SetTSStartTime(omStartTime + olSpan);
					}
					else
					{
						SetTSStartTime(omTSStartTime + CTimeSpan(0, 3, 0, 0));
					}
					pomTimeScale->SetDisplayStartTime(omTSStartTime);
					pomTimeScale->Invalidate(TRUE);
					pomRelTimeScale->SetDisplayStartTime(omTSStartTime);
					pomRelTimeScale->Invalidate(TRUE);
					pomViewer->ChangeViewTo(omCurrRueUrno);
					m_HScroll.SetScrollPos(ilPos, TRUE);
					break;
        
				case SB_THUMBTRACK:
					ilPos = m_HScroll.GetScrollPos();
					GetTimeSpanFromMinutes(ilPos * 30, olSpan, blNeg);
					SetTSStartTime(omStartTime + olSpan);
					pomTimeScale->SetDisplayStartTime(omTSStartTime);
					pomTimeScale->Invalidate(TRUE);
					pomRelTimeScale->SetDisplayStartTime(omTSStartTime);
					pomRelTimeScale->Invalidate(TRUE);
					m_HScroll.SetScrollPos(nPos, TRUE);
					pomViewer->ChangeViewTo(omCurrRueUrno);
					break;

				case SB_THUMBPOSITION:	// the thumb was just released?
					return;
				case SB_ENDSCROLL :
					break;
			}
			
			CRect olRect;
			m_HScroll.GetWindowRect(olRect);
			InvalidateRect(olRect);
		}
	}
	else
		CFormView::OnHScroll(nSBCode, nPos, pScrollBar);
}
//-----------------------------------------------------------------------------------------------------------------------


void CRulesFormView::MakeTimeScale()
{
	CCS_TRY

	// check if object exists, if so: delete it and make a new one
	if (pomTimeScale == NULL)
	{
		pomTimeScale = new CCSTimeScale(this);
	}

	CRect olTSRect;
	CalcTimeScaleRect ( &olTSRect );
	// create time scale
	pomTimeScale->Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE, olTSRect, this, 0, NULL);

	// register new timescale with gantt
	if (pomGantt != NULL)
		pomGantt->SetTimeScale(pomTimeScale);

	// set time values 
	omTSStartTime = CTime(1980, 1, 1, 0, 30, 0); // 1.Jan 1980, 00:00h
    omTSDuration = CTimeSpan(0, 7, 0, 0);	// 7 hours complete span
    omTSInterval = CTimeSpan(0, 0, 5, 0);	// 10 min interval
	pomTimeScale->SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CRulesFormView::MakeRelativeTimeScale()
{
	CCS_TRY

	// check if object exists, if so: delete it and make a new one
	if (pomRelTimeScale == NULL)
	{
		pomRelTimeScale = new RelativeTimeScale(this);
	}

	CRect olTSRect;
	CalcTimeScaleRect ( &olTSRect );
	
	// create time scale
	pomRelTimeScale->Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE, olTSRect, this, 0, NULL);

	// set time values 
	omTSStartTime = CTime(1980, 1, 1, 0, 30, 0); // 1.Jan 1980, 00:00h
    omTSDuration = CTimeSpan(0, 7, 0, 0);	// 7 hours complete span
    omTSInterval = CTimeSpan(0, 0, 5, 0);	// 10 min interval
	pomRelTimeScale->SetReferenceTimes(omOnBlock, omOfBlock);
	pomRelTimeScale->SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CRulesFormView::MakeGantt()
{
	CCS_TRY

	if (pomGantt != NULL) 
	{
		delete pomGantt;
	}
	pomGantt = new RulesGantt(pomViewer, imVerticalScaleWidth, imVerticalScaleIndent, &ogSmallFonts_Regular_6, &ogCourier_Regular_8, 14, 21, 2);
	
	CRect olGanttRect;
	CalcGanttRect ( &olGanttRect );
	// set some pointers
    pomGantt->SetTimeScale(pomTimeScale);
	pomGantt->SetStatusBar((CStatic*)pomStatusBar);
    pomGantt->SetViewer(pomViewer);

	// some more attributes
	omStartTime = pomTimeScale->GetDisplayStartTime();    // 01.01.1980
	omDuration = CTimeSpan(0,12,0,0);
	pomGantt->SetVerticalScaleWidth(imVerticalScaleWidth);		// Width of  LeftScale
	pomGantt->SetVerticalScaleIndent(imVerticalScaleIndent);    // Indent of LeftScale (space between left edge and start of LeftScale)
    pomGantt->SetDisplayWindow(omStartTime, omStartTime + omDuration);
	pomGantt->SetVerticalScaleColors(BLACK, SILVER, AQUA, SILVER);
	pomGantt->SetGanttChartColors(NAVY, SILVER, YELLOW, SILVER);
	pomGantt->SetBorderPrecision(10);
	pomGantt->SetGutters(4,10);
    
	// create gantt and attach this window
	pomGantt->Create(WS_BORDER, olGanttRect, (CWnd*)this);
	pomGantt->AttachWindow(this);	
	
	//--- make gantt known to viewer
	pomViewer->SetGantt(pomGantt);
	
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

LONG CRulesFormView::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
    CString olStr;
    int ipGroupNo = (int) HIWORD(lParam);
    int ipLineNo = (int) LOWORD(lParam);
	 CTime olTime = CTime((time_t) lParam);


    switch (wParam)
    {
        case UD_INSERTLINE:
        {
			pomGantt->InsertString(ipLineNo, "");
			pomGantt->RepaintItemHeight(ipLineNo);
			break;
		}
        case UD_UPDATELINE:
		{
			if (pomViewer->omLines[ipLineNo].Arrows.GetSize() < 1)
			{
				pomGantt->RepaintItemHeight(ipLineNo);
			}
			else
			{
				pomGantt->RepaintVerticalScale(ipLineNo);
				pomGantt->RepaintGanttChart(ipLineNo);
			}
			break;
		}
        case UD_DELETELINE:
		{
			pomGantt->DeleteString(ipLineNo);
			break;
		}
		case UD_RESETCONTENT:
		{
			pomGantt->ResetContent();
			break;
		}
		default:
		{
			break;
		}
    }

    return 0L;
}
//-----------------------------------------------------------------------------------------------------------------------

void CRulesFormView::ClientToView ( LPPOINT lpPoint )
{
	CPoint olScrollPos = GetDeviceScrollPosition() ; 
	lpPoint->x -= olScrollPos.x;
	lpPoint->y -= olScrollPos.y;
}
//-----------------------------------------------------------------------------------------------------------------------

void CRulesFormView::ClientToView( LPRECT lpRect )
{
	CPoint olScrollPos = GetDeviceScrollPosition() ; 
	lpRect->left   -= olScrollPos.x;
	lpRect->top	   -= olScrollPos.y;
	lpRect->right  -= olScrollPos.x;
	lpRect->bottom -= olScrollPos.y;
}
//-----------------------------------------------------------------------------------------------------------------------

void CRulesFormView::OnCreateGroup()
{
	bmCreateGroup = true;
}
//------------------------------------------------------------------------------------------------------------------------

void CRulesFormView::OnCreateNoGroup()
{
	bmCreateGroup = false;
}
//------------------------------------------------------------------------------------------------------------------------

bool CRulesFormView::IsSgrForActTpl ( RecordSet *popRecord )
{
	CString olTplUrno, olTplTnam, olSgrUtpl;
	if ( igSgrUtplIdx < 0 )  //  This DB doesn't know the Field SGR.UTPL
		return true;
	if ( !popRecord )
		return false;
	olSgrUtpl = popRecord->Values[igSgrUtplIdx];
	if ( olSgrUtpl.IsEmpty() )
		return true;	//  Universal groups are always ok
	if ( !pomComboBox )
	{
		TRACE ( "IsSgrForActTpl: pomComboBox is NULL!" );
		return false;
	}	
	pomComboBox->GetWindowText(olTplTnam);
	olTplUrno = ogBCD.GetFieldExt ( "TPL", "TNAM", "APPL", 
									olTplTnam, "RULE_AFT", "URNO" );
	if ( olTplUrno == olSgrUtpl )
		return true;
	olTplUrno = ogBCD.GetFieldExt ( "TPL", "TNAM", "APPL", 
									olTplTnam, "RULE_FIR", "URNO" );
	if ( olTplUrno == olSgrUtpl )
		return true;
	return false;
}

void CRulesFormView::SetGridToPrint ( CGXGridWnd *popGrid )
{
	pomGridToPrint = popGrid;
}

bool IsSgrForActTpl ( CString &ropSgrUrno )
{
	CMainFrame		*polMainWnd ;
	CRulesFormView	*polView=0;
	RecordSet		rlRecord;
	bool			blRet = false;

	if ( !ogBCD.GetRecord("SGR", "URNO", ropSgrUrno, rlRecord ) )
		return false;

	polMainWnd = (CMainFrame*) AfxGetMainWnd(); 
	if ( polMainWnd )
		polView = (CRulesFormView*)polMainWnd->GetActiveView();

	if ( polView )
		blRet = polView->IsSgrForActTpl ( &rlRecord );
	return blRet;
}


BOOL CRulesFormView::OnPreparePrinting(CPrintInfo* pInfo) 
{
	// TODO: call DoPreparePrinting to invoke the Print dialog box
	BOOL ilRet;
	pInfo->SetMaxPage(0xffff);

	pInfo->m_pPD->m_pd.Flags &= ~PD_NOSELECTION;
	pInfo->m_pPD->m_pd.hInstance = AfxGetInstanceHandle();

	// default preparation
	ilRet = DoPreparePrinting(pInfo);
	if ( !ilRet )
	{	//  es wird nicht gedruckt
		pomGridToPrint = 0;
	}
	return ilRet;
}

void CRulesFormView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	if ( pomGridToPrint )
	{		
		pomGridToPrint->OnEndPrinting( pDC, pInfo ) ;
		//pomGridToPrint = 0;
	}
	if ( pomWndCalledPreview )
	{
		CMainFrame* polMainWnd = (CMainFrame*) AfxGetMainWnd(); 
		if ( polMainWnd )
		{
			polMainWnd->EnableWindow ( FALSE );
		}
		pomWndCalledPreview->ShowWindow (SW_SHOW);
		pomWndCalledPreview->EnableWindow (TRUE);
	}
}

void CRulesFormView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	DoPrintSettings ();
	if ( pomGridToPrint )
		pomGridToPrint->OnBeginPrinting ( pDC, pInfo ) ;
}

void CRulesFormView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	if ( pomGridToPrint )
	{
		pomGridToPrint->OnGridPrint ( pDC, pInfo ) ;
	}
}

void CRulesFormView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	if ( pomGridToPrint )
		pomGridToPrint->OnGridPrepareDC ( pDC, pInfo ) ;
}

void CRulesFormView::DoPrintSettings ()
{
	if ( pomGridToPrint )
	{
		int  top, left, bottom, right;
		//ROWCOL rowCount;
		CGXGridParam* polParam = pomGridToPrint->GetParam();
		CGXProperties* polProp = polParam->GetProperties();
		polProp->GetMargins( top, left, bottom, right) ;
		polProp->SetMargins( 72, 18, 60, 0 ) ;
		polProp->GetDistances( top, bottom);
		polProp->SetDistances( 18, 18 );
		
		CGXData& rolDataHeader = polProp->GetDataHeader();
		rolDataHeader.DeleteContents();
		rolDataHeader.StoreStyleRowCol(1, 1, CGXStyle().SetValue(GetString(IDS_RULE_LIST)).
										SetFont(CGXFont().SetSize(14).SetBold(TRUE)), gxCopy );
		rolDataHeader.StoreStyleRowCol(1, 3, CGXStyle().SetValue("$P/$N").
										SetFont(CGXFont().SetSize(14).SetBold(TRUE)), gxCopy );
	
		

		CGXData& rolDataFooter = polProp->GetDataFooter();
		rolDataFooter.DeleteContents();
		rolDataFooter.StoreStyleRowCol(1, 1, CGXStyle().SetValue(""), gxCopy );
		rolDataFooter.StoreStyleRowCol(1, 2, CGXStyle().SetValue("UFIS Airport Solutions GmbH,  $D{%d.%m.%Y %H:%M}").SetFont(CGXFont().SetSize(12)), gxCopy );
		rolDataFooter.StoreStyleRowCol(1, 3, CGXStyle().SetValue(""), gxCopy );
		
	}
}


void CRulesFormView::OnFilePrint() 
{
	// TODO: Add your command handler code here
	if ( pomGridToPrint )	
		CFormView::OnFilePrint ();
}

void CRulesFormView::PreviewGrid ( CWnd *popWndCaller )
{
	CMainFrame* polMainWnd = (CMainFrame*) AfxGetMainWnd(); 
	pomWndCalledPreview = popWndCaller;
	if ( polMainWnd && pomGridToPrint )
	{
		polMainWnd->EnableWindow ( TRUE );
		if ( popWndCaller )
		{
			popWndCaller->EnableWindow ( FALSE );
			popWndCaller->ShowWindow (SW_HIDE);
		}
		OnFilePrintPreview();
		CView *polView = polMainWnd->GetActiveView();
		if ( polView )
		{
			CWnd* pDialogBar = polMainWnd->GetDescendantWindow( AFX_IDW_PREVIEW_BAR );
			if ( pDialogBar )
			{
				for ( int i=AFX_ID_PREVIEW_CLOSE; i<=AFX_ID_PREVIEW_ZOOMOUT; i++ )
				{	
					CWnd *pWnd = pDialogBar->GetDlgItem ( i );
					if ( pWnd )
					{
						pWnd->SetWindowText ( GetString(i) );
						pWnd->EnableWindow ();
					}
				}
			}
		}
	}
}


bool CRulesFormView::SaveRule( RecordSet &ropRecord )
{
	   
	//--- write data to database
	CString olErr, olOldUrno;
	CString olDate, olUser;
	bool blSave = true;
	olDate = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");
	olUser = pcgUser;
 
	ogDataSet.CheckRudUtpl ( ropRecord.Values[igRueUtplIdx] );

	// FISU (flag IsUsed)
	bool blIsUsed = false;
	CString olFisu;
	olFisu = ogBCD.GetField("RUE", "URNO", omCurrRueUrno, "FISU");
	if (olFisu.Left(1) == CString("1"))
	{
		blIsUsed = true;
	}
	else
	{
		ropRecord.Values[igRueFisuIdx] = CString("0");
		blIsUsed = false;
	}

	ropRecord.Values[igRueUseuIdx] = olUser;
	ropRecord.Values[igRueLstuIdx] = olDate;

 	if (bmNewFlag == true)
	{
		// It is a new rule (Possibility 1)
		// Everything will be freshly inserted
		ropRecord.Values[igRueCdatIdx] = olDate;
		ropRecord.Values[igRueUsecIdx] = olUser;
		ropRecord.Values[igRueUarcIdx] = olOldUrno;				

		if ( !ogBCD.InsertRecord("RUE", ropRecord, true) )
		{
			olErr = CString("14");	// error on inserting rue record 
			blSave = false;
		}
		else
		{
			if ( SaveDemandRequirements())
			{
				if ( SaveFunctions() && SaveQualifications() && SaveEquipment() && SaveLocations() )
				{
					if (bgIsValSaved == false)
					{
						 bgIsValSaved = SaveValidity(omCurrRueUrno, pomFrameWnd->rmValData, bmNewFlag);	
					}

					if ( !bgIsValSaved )
					{
						olErr = CString("13");	// error on writing validity data
					}
				}
				else
				{
					olErr = CString("12");	// error on writing ressources
				}
			}
			else
			{
				olErr = CString("11");	// error on writing demands
			}
		}
	}
	else
	{
		// It is a rule loaded from database
		if ( blIsUsed && !ogChangeInfo.ArchiveRule(ropRecord) )
			blIsUsed = false;
		if (blIsUsed == false)
		{
			// There have been no demands created using this rule (Possibility 2)
			// ==> it can be updated
			if (ogBCD.SetRecord("RUE", "URNO", omCurrRueUrno, ropRecord.Values, true) == false)
			{
				olErr = CString("24");	// error on updating rue record 
				blSave = false;
			}
			else
			{
				if ( ogDataSet.SaveModifiedRule ( omCurrRueUrno ) )
				{
					if ( !SaveValidity(omCurrRueUrno, pomFrameWnd->rmValData, bmNewFlag))
					{
						olErr = CString("23");	// error on writing validity data
					}
				}
				else
					olErr = CString("21");	// error on writing duty req.
			}
		}
		else
		{
			// There have been demands created using this rule (Possibility 3)	
			// ==> archive old rue and insert copies for the rest
			
			// first: keep old and get new rue urno
			olOldUrno = ropRecord.Values[igRueUrnoIdx];

			long llNewUrno = ogBCD.GetNextUrno();
			omCurrRueUrno.Format("%d", llNewUrno);
			ropRecord.Values[igRueFisuIdx] = CString("0");	//hag990901

			// insert copy with new urno
			ropRecord.Values[igRueUrnoIdx] = omCurrRueUrno;
			ropRecord.Values[igRueUarcIdx] = olOldUrno;	
			
			if ( ogBCD.InsertRecord("RUE", ropRecord, false))
			{
				/*  new rule should be broadcasted first to give a second application 
					a chance to decide whether old rule has been modified or deleted */
				blSave = ogBCD.Save("RUE");	
				
				// deactivate original rule (status = 2) and set LSTU and USEU
				ogBCD.SetField("RUE", "URNO", olOldUrno, "RUST", "2");
				ogBCD.SetField("RUE", "URNO", olOldUrno, "LSTU", olDate);
				ogBCD.SetField("RUE", "URNO", olOldUrno, "USEU", olUser);
				ogBCD.SetField("RUE", "URNO", omCurrRueUrno, "USEU", olUser );
				ogBCD.SetField("RUE", "URNO", omCurrRueUrno, "LSTU", olDate);
				blSave &= ogBCD.Save("RUE");
			}
			else
			{
				olErr = CString("33");
			}

			CString olErrFunc;
			if (!ogDataSet.OnCopyRue(olOldUrno, omCurrRueUrno ))
				olErrFunc += "OnCopyRue, ";
			if (!SaveDemandRequirements())
			{
				olErrFunc += "SaveDemandRequirements, ";
				olErr = CString("31");
			}
			else
			{
				if (!SaveFunctions())
				{
					olErrFunc += "SaveFunctions, ";
					olErr = CString("32");
				}
				if (!SaveQualifications())
				{
					olErrFunc += "SaveQualifications, ";
					olErr = CString("32");
				}
				if (!SaveEquipment())
				{
					olErrFunc += "SaveEquipment, ";
					olErr = CString("32");
				}
				if (!SaveLocations())
				{
					olErrFunc += "SaveLocations, ";
					olErr = CString("32");
				}
			}
			if (!olErrFunc.IsEmpty ())
				ogLog.Trace("ERROR", "Return-Code not ok in function(s):\n%s\n", (const char*)olErrFunc);
			else	// everything ok !
			{
				if ( !SaveValidity(omCurrRueUrno, pomFrameWnd->rmValData, true) )
				{
					olErr = CString("33");
				}
			}
			if ( pomViewer )
				pomViewer->ChangeViewTo(omCurrRueUrno);
		}	
	}
                        
	//-- write everything to database and set TPL.FISU	
	if (olErr.IsEmpty() == TRUE)
	{
		if (blSave == true)
		{
			// set TPL.FISU = 1 
			RecordSet olTpl(ogBCD.GetFieldCount("TPL"));
			ogBCD.GetRecord("TPL", "URNO", ropRecord.Values[igRueUtplIdx], olTpl);
			if (olTpl.Values[igTplFisuIdx] != "1")
			{
				olTpl.Values[igTplFisuIdx] = "1";
				ogBCD.SetRecord("TPL", "URNO", ropRecord.Values[igRueUtplIdx], olTpl.Values);
				ogBCD.Save("TPL");
			}
			bmNewFlag = false;
		}
	}
	else
	{
		// In case anything went wrong: show message
		CString olMsg;
		switch (atoi(olErr.Right(1)))
		{
			case 1:	
				olMsg = GetString(1503);
				break;

			case 2:
				olMsg = GetString(1516);
				break;	
			
			case 3:
				olMsg = GetString(1504);
				break;

			case 4:
				olMsg = GetString(1505);
				break;
			
			default:
				break;
		}

		switch (atoi(olErr.Left(1)))
		{
			case 1:	
				olMsg += GetString(1500);
				break;

			case 2:
				olMsg += GetString(1501);
				break;	
			
			case 3:
				olMsg += GetString(1502);
				break;

			default:
				break;
		}

		MessageBox(olMsg, GetString(AFX_IDS_APP_TITLE), MB_OK | MB_ICONEXCLAMATION);
	}
	
	// if writing to database failed, tell the user
	if (blSave == false)
	{
		MessageBox(GetString(1507), GetString(AFX_IDS_APP_TITLE), MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	else
	{
		// save change state
		ogChangeInfo.SetCurrRueUrno(omCurrRueUrno);
		ogChangeInfo.SetChangeState(RULE_CHECK);

		// tell MainFrame class that rule has been saved OK
		long llCurrRueUrno = atol(omCurrRueUrno);
		CMainFrame *polMainWnd = (CMainFrame*) AfxGetMainWnd();
		if (::IsWindow(polMainWnd->GetSafeHwnd()))
		{
			polMainWnd->SendMessage(WM_RULE_SAVED_OK, (WPARAM)this, (LPARAM)llCurrRueUrno);
		}
	}
	
	return true;
}

int CRulesFormView::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message)
{
	int nResult = CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
	if (nResult == MA_NOACTIVATE || nResult == MA_NOACTIVATEANDEAT)
		return nResult;   // frame does not want to activate

	CFrameWnd* pParentFrame = GetParentFrame();
	if (pParentFrame != NULL)
	{
		//  eat it if this will cause activation
		//  PRF3748: following ASSERT evaluates false in some cases, but it seem 
		//			 to be not critical
		//  ASSERT(pParentFrame == pDesktopWnd || pDesktopWnd->IsChild(pParentFrame));

		// either re-activate the current view, or set this view to be active
		CView* pView = pParentFrame->GetActiveView();
		HWND hWndFocus = ::GetFocus();
		if (pView == this &&
			m_hWnd != hWndFocus && !::IsChild(m_hWnd, hWndFocus))
		{
			// re-activate this view
			OnActivateView(TRUE, this, this);
		}
		else
		{
			// activate this view
			pParentFrame->SetActiveView(this);
		}
	}
	return nResult;
}
