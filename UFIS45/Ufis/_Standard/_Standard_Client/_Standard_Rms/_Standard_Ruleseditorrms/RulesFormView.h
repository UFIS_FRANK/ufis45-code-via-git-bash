#if !defined(AFX_RULESFORMVIEW_H__DF998FA4_9E5D_11D3_93A4_00001C033B5D__INCLUDED_)
#define AFX_RULESFORMVIEW_H__DF998FA4_9E5D_11D3_93A4_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RulesFormView.h : header file
//

class CMainFrame;
class RulesGantt;
class CCSTimeScale;
class RulesGanttViewer;
class RelativeTimeScale;
class CGXGridWnd;
	

/////////////////////////////////////////////////////////////////////////////
// CRulesFormView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CRulesFormView : public CFormView
{

protected:
	CRulesFormView(UINT nIDTemplate);           // protected constructor used by dynamic creation
	CRulesFormView();
	DECLARE_DYNCREATE(CRulesFormView)

// Form Data
public:
	enum { IDD = IDD_REGELEDITOR_FORM };
	//{{AFX_DATA(CRulesFormView)
		// NOTE: the ClassWizard will add data members here
	CScrollBar	m_HScroll;
	//}}AFX_DATA

// Attributes
public:
	CString omCurrRueUrno;		// Urno of GPM currently shown
	CComboBox *pomComboBox;
	bool bmNewFlag;


protected:
	//--- flags
	bool bmChangeFlag;
	bool bmCreateGroup;

	//-- Main frame window
	CMainFrame *pomFrameWnd;
	CStatusBar *pomStatusBar;
	
	//--- gantt
	RulesGanttViewer *pomViewer;
	RulesGantt *pomGantt;
    CCSTimeScale *pomTimeScale;
	RelativeTimeScale *pomRelTimeScale;
	CGXGridWnd	 *pomGridToPrint;
	CWnd		*pomWndCalledPreview;
		


	//--- some gantt attributes
	int imVerticalScaleIndent;
	int imVerticalScaleWidth;
	CTime omOnBlock;
	CTime omOfBlock;
	CTime omStartTime;
	CTimeSpan omDuration;
	
	CTime omTSStartTime;
	CTimeSpan omTSDuration;
	CTimeSpan omTSInterval;


// Operations
public:
	void SetTSStartTime(CTime opTSStartTime);
	virtual CString GetRuleEventType(){return "";};
	void ClientToView ( LPPOINT lpPoint );
	void ClientToView( LPRECT lpRect );
	virtual void AddServices ( CStringArray	&ropSerUrnos ){};
	virtual void OnCreateNoGroup();
	virtual void OnCreateGroup();
	virtual void OnNewRule(){};
	bool IsSgrForActTpl ( RecordSet *popRecord );
	void SetGridToPrint ( CGXGridWnd *popGrid );
	void DoPrintSettings ();
	void PreviewGrid ( CWnd *popWndCaller );

protected:
	void ConstructString(CGridFenster* popGrid, CString& opResult, int ipMaxLen=-1 ); // F�r Speicherung
	void ReConstructString(CGridFenster* popGrid, CString& opString); // F�r Laden
	bool SaveQualifications();
	bool SaveFunctions();
	bool SaveLocations(); 
	bool SaveEquipment();
	virtual void CalcTimeScaleRect( LPRECT lpRect ){};
	virtual void CalcGanttRect( LPRECT lpRect ){};
	void MakeTimeScale();
	void MakeRelativeTimeScale();
	void MakeGantt();
	bool SaveRule( RecordSet &ropRecord );
	virtual bool SaveDemandRequirements () { return false; };


public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRulesFormView)
	public:
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CRulesFormView();
	void IniDataMembers ();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CRulesFormView)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg void OnFilePrint();
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/*no longer needed
	bool CopyRessources(CStringArray &opRudUrnos);
	bool CopyDemandRequirements();
*/
/////////////////////////////////////////////////////////////////////////////
bool IsSgrForActTpl ( CString &ropSgrUrno );
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RULESFORMVIEW_H__DF998FA4_9E5D_11D3_93A4_00001C033B5D__INCLUDED_)
