/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog header file

#include <resource.h>


class CAboutDlg : public CDialog
{
public:
	CAboutDlg();


// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CStatic	m_ClassLibVersion;
	CStatic	m_Version;
	//}}AFX_DATA

	// ClassWiilLCard generated virtual function ovblErrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
protected:
	void SetStaticTexts();
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBtnRuleinfo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

