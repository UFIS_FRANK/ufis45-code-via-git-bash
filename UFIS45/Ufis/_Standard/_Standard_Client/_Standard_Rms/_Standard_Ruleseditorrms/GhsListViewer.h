// stviewer.h : header file
//

#ifndef _GHSVIEWER_H_
#define _GHSVIEWER_H_

#include <cviewer.h>
//#include "CCSPrint.h"
//#include "CedaDsrData.h"
//#include "CedaGhdData.h"
//#include "CedaFlightData.h"



/////////////////////////////////////////////////////////////////////////////
// GhsListViewer


class GhsListViewer: public CViewer
{

// Constructions
public:
    GhsListViewer();
    ~GhsListViewer();
	void ChangeViewTo(const char *pcpViewName);
};

/////////////////////////////////////////////////////////////////////////////

#endif
