// InfoDlg.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <InfoDlg.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <DDXDDV.h>
#include "BasicData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



//-----------------------------------------------------------------------------------------------------------------------
//						construction / destruction
//-----------------------------------------------------------------------------------------------------------------------
InfoDlg::InfoDlg(CString opRueUrno, CWnd* pParent /*=NULL*/)
	: CDialog(InfoDlg::IDD, pParent)
{
	pomParent = pParent;
	omCurrRueUrno = opRueUrno;

	//{{AFX_DATA_INIT(InfoDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	//Create();
}
//-----------------------------------------------------------------------------------------------------------------------

InfoDlg::~InfoDlg()
{
	
}


//-----------------------------------------------------------------------------------------------------------------------
//						data exchange, message map
//-----------------------------------------------------------------------------------------------------------------------
void InfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(InfoDlg)
	DDX_Control(pDX, IDC_CDAT, m_CdatEdit);
	DDX_Control(pDX, IDC_LSTU_, m_LstuEdit);
	DDX_Control(pDX, IDC_USEU, m_UseuEdit);
	DDX_Control(pDX, IDC_USEC, m_UsecEdit);
	//}}AFX_DATA_MAP
}
//-----------------------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(InfoDlg, CDialog)
	//{{AFX_MSG_MAP(InfoDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//-----------------------------------------------------------------------------------------------------------------------
//						creation
//-----------------------------------------------------------------------------------------------------------------------
void InfoDlg::Create()
{
	// This method is not used as long as we open the dialog with DoModal()

	CDialog::Create(IDD_INFO_DLG, pomParent);
}



//-----------------------------------------------------------------------------------------------------------------------
//						message handling
//-----------------------------------------------------------------------------------------------------------------------
BOOL InfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetStaticTexts();

	//--- exit dialog if no rule urno specified
	if (omCurrRueUrno.IsEmpty() || omCurrRueUrno == " ")
	{
		MessageBox(GetString(1426), GetString(AFX_IDS_APP_TITLE));
		EndDialog(0);
	}

	//--- Set window caption 
	CString olText = GetString(1427) + CString(" ") + ogBCD.GetField("RUE", "URNO", omCurrRueUrno, "RUNA");
	SetWindowText(olText);
	
	CString olCdat;
	CString olUsec;
	CString olLstu;
	CString olUseu;

	//--- Get values
	if (omCurrRueUrno == CString("-1"))
	{
		CTime olTime;
		olTime = CTime::GetCurrentTime();

		olCdat = olTime.Format("%Y%m%d%H%M%S");
		olUsec = pcgUser;
		olLstu = olTime.Format("%Y%m%d%H%M%S");
		olUseu = pcgUser;
	}
	else
	{
		RecordSet olRecord(ogBCD.GetFieldCount("RUE"));
		ogBCD.GetRecord("RUE", "URNO", omCurrRueUrno, olRecord);

		olCdat = olRecord.Values[igRueCdatIdx];
		olUsec = olRecord.Values[igRueUsecIdx];
		olLstu = olRecord.Values[igRueLstuIdx];
		olUseu = olRecord.Values[igRueUseuIdx];
	}


	//--- format values
	CString olTmp;
	if (olCdat.GetLength() < 14)
	{
		olCdat = CString(GetString(1513));
	}
	else
	{
		olCdat = DBStringToDateString(olCdat);
	}

	if (olLstu.GetLength() < 14)
	{
		olLstu = CString(GetString(1513));
	}
	else
	{
		 olLstu = DBStringToDateString(olLstu);
	}
	
	//--- write values to controls
	m_CdatEdit.SetWindowText(olCdat);
	m_UsecEdit.SetWindowText(olUsec);
	m_LstuEdit.SetWindowText(olLstu);
	m_UseuEdit.SetWindowText(olUseu);

	// UpdateData();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
//-----------------------------------------------------------------------------------------------------------------------

void InfoDlg::SetStaticTexts()
{
	CCS_TRY

	
	SetWindowLangText ( this, ID_ABOUT_RULE );

	SetDlgItemLangText ( this, IDC_USEC_STATIC, IDS_CREATED );
	SetDlgItemLangText ( this, IDS_USEU_STATIC, IDS_LAST_CHANGE );
	SetDlgItemLangText ( this, IDOK, 1398 );

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------
