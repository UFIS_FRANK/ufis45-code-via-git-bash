#if !defined(AFX_CollectiveRulesView_H__2B6A6851_35F7_11D3_A647_0000C007916B__INCLUDED_)
#define AFX_CollectiveRulesView_H__2B6A6851_35F7_11D3_A647_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif


#include <CCSEdit.h>
#include <CCSDragDropCtrl.h>
#include <RulesFormView.h>

class CGridFenster;
class CColDutyGrid;
class CMainFrame;


class CollectiveRulesView : public CRulesFormView
{
public:
	CollectiveRulesView();           // protected constructor used by dynamic creation
	~CollectiveRulesView();
	
	DECLARE_DYNCREATE(CollectiveRulesView)

//--- Implementation
	// message handling (called from MainFrm.cpp)
	void OnLoadRule(CString opRueUrno, CString opTplName, bool bpNoComboChange);
	void OnNewRule();
	void OnSaveRule();

	BOOL OnHelpInfo(HELPINFO* pHelpInfo) ;
	void OnLeistungen();
	void OnSelEndOK();
	void OnCopy();
	
	// helper functions
	void MakeBarMenu(int itemID, CPoint point);
	void TablesFirstTimeInit();
	void EnableDemandDetailControls(bool bpEnable);
	void FillDemandDetailControls();
	void UpdateServiceTable();
	
	void ClearRessourceTables();
	void SetTablesForStaffView();
	void SetTablesForEquipmentView();
	void SetTablesForLocationsView();

//	bool CopyDemandRequirements();
//	bool CopyRessources(CStringArray &opRudUrnos);

	bool SaveDemandRequirements();
	
	bool IsPassFilter(RecordSet *popRud);

	// processing functions
	void ProcessRudTmpNew(RecordSet *popRecord);
	void ProcessRudTmpChange(RecordSet *popRecord);
	void ProcessRudTmpDelete(RecordSet *popRecord);
	CString GetRuleEventType() {return "1";};
	void ProcessSgrNew(RecordSet *popRecord);
	void ProcessSgrDelete(RecordSet *popRecord);
	void AddServices ( CStringArray	&ropSerUrnos );

//	bool IsSingleResourceSelected ( ROWCOL ipRow, ROWCOL ipCol,
//									CGridFenster *popGrid,
//									CString opTable, CString &ropCode );
	void SetStaticTexts();
	void DisplayQualisForRud ( CString &ropRudUrno );
	int GetResRecordsToDisplay( CString opTable,
								CCSPtrArray <RecordSet> *popRecords );

	void ResetResourceTable ( CGridFenster *popGrid );
	void SetCurrentRudUrno ( CString opRudUrno );
	bool SetTimeControlsForDgr ( CString &ropDgrUrno );
	void UpdateAllRudsStartEnd ();
	bool UpdateRudStartEnd ( RecordSet &ropRudRecord );
	bool CheckChanges();
	void IniRefCB();
	void EnableControls();
	void DisplayRudInControls();


// attributes
public:
	CGridFenster *pomLeftGrid;
	CGridFenster *pomRightGrid;
	CColDutyGrid *pomDutyGrid;
	
	CGridFenster *pomGrid;
	
	CCSDragDropCtrl m_DragDropTarget;

	CString omCurrRudUrno;
	CString omCurrDgrUrno;
	CString omRefTab;
	CString omRefDsp;
	CString omRefVal;
	
	int	imSelectMode;
	int	imRessourceType;
	int imActiveRow;
	
	CRect	omDutyOriginalRect;

	//{{AFX_DATA(CollectiveRulesView)
	enum { IDD = IDD_COLLECTIVE_VIEW };
	CCSEdit	omEditLito;
	CCSEdit	omEditLifr;
	CCSEdit	omEditMinCount;
	CCSEdit	omEditMaxCount;
	CButton	omFondChk;
	CButton	omNoDemandsChk;
	CCSEdit	omEditSplitMax;
	CCSEdit	omEditSplitMin;
	CButton	omDideChk;
	CString	m_RuleName;
	CString	m_RuleShortName;
	CComboBox	m_AlocCB;
	CComboBox   m_RefCB;
	CButton	m_Chb_Deactivated;
	CCSEdit	m_Edt_RuleShortName;
	CCSEdit	m_Edt_RuleName;

    CCSEdit	start;
	CCSEdit	endt;
    CButton	cflag;

	//}}AFX_DATA
	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CollectiveRulesView)
	public:
	virtual void OnInitialUpdate();
	virtual LONG OnDragOver(UINT wParam, LONG lParam);
	virtual LONG OnDrop(UINT wParam, LPARAM lParam);
	virtual void OnRButtonDown(UINT itemID, LONG lParam);
	virtual void OnLButtonDblClk(UINT itemID, LPARAM lParam);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CollectiveRulesView)
	afx_msg void OnEndEditing(WPARAM wParam, LPARAM lParam);
	afx_msg void OnMenuDelete();
	afx_msg void OnMenuCreateLink();
	afx_msg void OnMenuDeleteLink();
	afx_msg void OnMenuCopyBar();
	afx_msg void OnSelendokCobAlod();
	afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnDideChk();
	afx_msg void OnSelendokCobRef();
	afx_msg void OnChangeMaxtime();
	afx_msg void OnChangeMintime();
	afx_msg void OnChangeLifr();
	afx_msg void OnChangeLito();
	afx_msg void OnChangeMinc();
	afx_msg void OnChangeMaxc();
	afx_msg void OnFaddChk();
	afx_msg void OnFondChk();

    

	afx_msg void OnCheckChange();
    afx_msg void ccistart();
	afx_msg void cciend();


	//}}AFX_MSG
	afx_msg void OnGridButton(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridRButton(WPARAM wParam, LPARAM lParam);
	afx_msg void OnEditKillFocus(WPARAM wParam, LPARAM lParam);
	// afx_msg void OnChangedRuty();
	afx_msg void OnGridCurrCellChanged(WPARAM wParam, LPARAM lParam);
	
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CollectiveRulesView_H__2B6A6851_35F7_11D3_A647_0000C007916B__INCLUDED_)
