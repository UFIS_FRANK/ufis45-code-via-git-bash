// RelativeTimeScale.cpp : implementation file
// 
#include <stdafx.h>
#include <RelativeTimeScale.h>
#include <CCSGlobl.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif





/////////////////////////////////////////////////////////////////////////////
// RelativeTimeScale



RelativeTimeScale::RelativeTimeScale(CWnd *popParent)
{
    CDC dc;
    dc.CreateCompatibleDC(NULL);

    LOGFONT lolFont;
    memset(&lolFont, 0, sizeof(LOGFONT));

	pomFont = new CFont;

	lolFont.lfCharSet= DEFAULT_CHARSET;
    lolFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    lolFont.lfWeight = FW_NORMAL;
    lolFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(lolFont.lfFaceName, "Small Fonts");
    pomFont->CreateFontIndirect(&lolFont);

	lmBkColor = ::GetSysColor(COLOR_BTNFACE);
	//lmTextColor = RGB(0,0,128);
	lmTextColor = ::GetSysColor(COLOR_WINDOWTEXT);
	lmHilightColor = RGB(255,0,0);

	imRuleType = 0; // turnaround as default
	
	pomParent = popParent;
    bmDisplayCurrentTime = true;

   	omCurrentTime = CTime::GetCurrentTime();

     
	prmCurrMarker = NULL;
	bmLButtonDown = false;
	pomStaticToolTip = NULL;
	omInboundText = "Inbound";
	omOutboundText = "Outbound";
}

RelativeTimeScale::~RelativeTimeScale()
{
	delete pomFont;


// Damkerng 06/27/96:
// This code has been written to avoid AfxAssert() failed(). The situation
// is ~GateDagram() is called the ~RelativeTimeScale() is called but there is no
// more time scale window opened.
    for (int ilIndex = 0; ilIndex < omTSIArray.GetSize(); ilIndex++)
    {
        delete (CRelativeTopScaleIndicator *) omTSIArray[ilIndex];
    }

	DeleteAllMarker();
}

BEGIN_MESSAGE_MAP(RelativeTimeScale, CWnd)
    //{{AFX_MSG_MAP(RelativeTimeScale)
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


void RelativeTimeScale::EnableDisplayCurrentTime(bool bpDisplayCurrentTime)
{
    bmDisplayCurrentTime = bpDisplayCurrentTime;
}

void RelativeTimeScale::SetDisplayTimeFrame(CTime opDisplayStart, CTimeSpan opDuration, CTimeSpan opInterval)
{
    SetDisplayStartTime(opDisplayStart);
    SetTimeInterval(opInterval);
    
    CRect olClientRect; GetClientRect(&olClientRect);

// ********** MNE: Hier werden die Punkte f�r's Zeichnen festgelegt
    imP0 = olClientRect.Height() / 2;
    imP1 = imP0 * 125 / 100;
    imP2 = imP0 * 150 / 100;
    imP3 = olClientRect.bottom;
// ********** MNE

    int ilSlotCount = int (opDuration.GetTotalMinutes() / opInterval.GetTotalMinutes());
    // for the right most pixel
    fmIntervalWidth = (double) (olClientRect.Width() - 1) / ilSlotCount;
	//TRACE("number of slot %d, width %g", ilSlotCount, fmIntervalWidth);
}

void RelativeTimeScale::SetReferenceTimes(CTime opInb, CTime opOutb)
{
	omOnBlock = opInb;
	omOfBlock = opOutb;
}

void RelativeTimeScale::SetDisplayStartTime(CTime opDisplayStart)
{
    // DisplayStart Time must begin at 0 seconds
    omDisplayStart = CTime(
        opDisplayStart.GetYear(), opDisplayStart.GetMonth(),
        opDisplayStart.GetDay(), opDisplayStart.GetHour(),
        opDisplayStart.GetMinute(), 0
    );

    
    //omDisplayStart = opDisplayStart;
}

CTime RelativeTimeScale::GetDisplayStartTime(void)
{
    return omDisplayStart;
}
   

void RelativeTimeScale::SetTimeInterval(CTimeSpan opInterval)
{
    omInterval = opInterval;
}

CTimeSpan RelativeTimeScale::GetDisplayDuration(void)
{
    CRect olClientRect; GetClientRect(&olClientRect);

    return CTimeSpan(0, 0, int (omInterval.GetTotalMinutes() * (olClientRect.Width() - 1) / fmIntervalWidth), 0);
}


////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This block of code is used for make the TimeScale be able to help
// the other classes which desire to calculate the time backward from the
// given point.
CTime RelativeTimeScale::GetTimeFromX(int ipX)	// offset of ipX is zero
{
    /*
	long llSeconds = (long)((ipX) * omInterval.GetTotalSeconds() / (fmIntervalWidth * 60));
        // llSeconds is measured from omDisplayStart
        // fmIntervalWidth keep number of pixel per each interval
    long llMinutes = llSeconds / 60;
    llSeconds %= 60;
    */
    long llSeconds = 0;
    long llMinutes = (long)((ipX) * omInterval.GetTotalMinutes() / fmIntervalWidth);

// DTT Jul.29 -- I've found some bug, the GetTimeFromX() and GetXFromTime()
// cannot work together correctly, i.e., they are not invert function of each other.
// So when we convert X to time back-and-forth again-and-again, some error will
// happened. I still unsure about what the real reason of this. But I think we
// may very ugly and dirty to fix this bug by plus one minute.
	llMinutes++;
////////////////

    long llHours = llMinutes / 60;
    llMinutes %= 60;
    return (omDisplayStart + CTimeSpan(0, llHours, llMinutes, llSeconds));
}
////////////////////////////////////////////////////////////////////////

int RelativeTimeScale::GetXFromTime(CTime opTime)
{
    long llTotalMin = (opTime - omDisplayStart).GetTotalMinutes();
    int ilRet = int (fmIntervalWidth * llTotalMin / omInterval.GetTotalMinutes());
	/* TRACE("RelativeTimeScale: GetXFromTime() return %03d (%7.3f %ld)\n",
        ilRet,
        fmIntervalWidth,
        (opTime - omDisplayStart).GetTotalMinutes()
    ); */
	return ilRet;
}

void RelativeTimeScale::UpdateCurrentTimeLine(void)
{
	CTime olTime = CTime::GetCurrentTime();
	UpdateCurrentTimeLine(olTime);
}

void RelativeTimeScale::UpdateCurrentTimeLine(CTime opTime)
{
//	LocalToUtc(opTime);
    int ilX = GetXFromTime(opTime);
    //int ilOldX = GetXFromTime(omOldCurrentTime);
	int ilOldX = GetXFromTime(omCurrentTime);
    if (ilX != ilOldX)
    {
		omCurrentTime = opTime;

        CRect olTSRect; GetClientRect(&olTSRect);
        olTSRect.left = ilOldX;

        olTSRect.right = ilX + 1;       // must be +1
		// old current time should be the left of current time so comment next line
        //olTSRect.NormalizeRect();
        InvalidateRect(&olTSRect, true);
    }
}

void RelativeTimeScale::AddTopScaleIndicator(CTime opStartTime, CTime opEndTime, COLORREF lpColor)
{
    CRelativeTopScaleIndicator *polTSI = new CRelativeTopScaleIndicator;
    polTSI->omStart = opStartTime;
    polTSI->omEnd = opEndTime;
    polTSI->lmColor = lpColor;
    
    AddTopScaleIndicator(polTSI);
}

void RelativeTimeScale::DisplayTopScaleIndicator(CDC *popDC)
{
    CPen *polOldPen = (CPen *) popDC->SelectStockObject(BLACK_PEN);
    
    CPen olPen;
    CRelativeTopScaleIndicator *polTSI;
    for (int ilIndex = 0; ilIndex < omTSIArray.GetSize(); ilIndex++)
    {
        polTSI = (CRelativeTopScaleIndicator *) omTSIArray.GetAt(ilIndex);

        olPen.CreatePen(PS_SOLID, 1, polTSI->lmColor);
                
        popDC->SelectObject(&olPen);
        
        popDC->MoveTo(GetXFromTime(polTSI->omStart), (imP3 - 3) - (ilIndex * (2 + 2)));
        popDC->LineTo(GetXFromTime(polTSI->omEnd) + 1, (imP3 - 3) - (ilIndex * (2 + 2)));
        
        popDC->MoveTo(GetXFromTime(polTSI->omStart), (imP3 - 3) - 1 - (ilIndex * (2 + 2)));
        popDC->LineTo(GetXFromTime(polTSI->omEnd) + 1, (imP3 - 3) - 1 - (ilIndex * (2 + 2)));


        /* TRACE("Display TSI: [%ld][%03d]\n",
            (polTSI->omStart - omDisplayStart).GetTotalMinutes(),
            GetXFromTime(polTSI->omStart)
        ); */

        
        popDC->SelectObject(polOldPen);
        
        olPen.DeleteObject();
    }
    
    popDC->SelectObject(polOldPen);
}

bool RelativeTimeScale::TSIPos(int *ipLeft, int *ipRight)
{
    bool blRet = true;
    int ilTSISize = omTSIArray.GetSize();
    if (ilTSISize == 0)
        blRet = false;
    else
    {
        int ilMinLeft, ilMaxRight;
        CRelativeTopScaleIndicator *polTSI = (CRelativeTopScaleIndicator *) omTSIArray.GetAt(0);

        if (ilTSISize == 1)
        {
            *ipLeft = GetXFromTime(polTSI->omStart);
            *ipRight = GetXFromTime(polTSI->omEnd);
        }
        else
        {
            ilMinLeft = GetXFromTime(polTSI->omStart);
            ilMaxRight = GetXFromTime(polTSI->omEnd);
        
            int ilLeft, ilRight;
            for (int ilIndex = 1; ilIndex < ilTSISize; ilIndex++)
            {
                polTSI = (CRelativeTopScaleIndicator *) omTSIArray.GetAt(ilIndex);


                ilLeft = GetXFromTime(polTSI->omStart);
                if (ilLeft < ilMinLeft)
                    ilMinLeft = ilLeft;
            
                ilRight = GetXFromTime(polTSI->omEnd);
                if (ilRight > ilMaxRight)
                    ilMaxRight = ilRight;
            }
            *ipLeft = ilMinLeft;
            *ipRight = ilMaxRight;
        }
    }

    (*ipRight)++;                       // InvalidateRect()'s rule of thumb
    return blRet;
}

void RelativeTimeScale::DisplayTopScaleIndicator(void)
{
    // find the right place to InvalidateRect
    int ilLeft, ilRight;
    if (TSIPos(&ilLeft, &ilRight))
    {
        CRect olClientRect; GetClientRect(&olClientRect);
        CRect olRect(ilLeft, olClientRect.top, ilRight, olClientRect.bottom);
        
        InvalidateRect(&olRect, false);
    }
}
 
void RelativeTimeScale::RemoveAllTopScaleIndicator(void)
{
    // find the right place to InvalidateRect
    int ilLeft, ilRight;
    if (TSIPos(&ilLeft, &ilRight))
    {
        CRect olClientRect; GetClientRect(&olClientRect);
        CRect olRect(ilLeft, olClientRect.top, ilRight, olClientRect.bottom);
        
        InvalidateRect(&olRect, true);

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This code has been written to avoid AfxAssert() failed(). The situation
// is ~GateDagram() is called the ~RelativeTimeScale() is called but there is no
// more time scale window opened.
/*		while (omTSIArray.GetSize() > 0)
		{
			CRelativeTopScaleIndicator *p = (CRelativeTopScaleIndicator *)omTSIArray[0];
			delete p;
		}
*/

        for (int ilIndex = 0; ilIndex < omTSIArray.GetSize(); ilIndex++)
        {
            delete (CRelativeTopScaleIndicator *) omTSIArray[ilIndex];
        }

        omTSIArray.RemoveAll();
////////////////////////////////////////////////////////////////////////
    }
}
 
/////////////////////////////////////////////////////////////////////////////
// RelativeTimeScale message handlers


bool RelativeTimeScale::OnEraseBkgnd(CDC* pDC)
{
	CRect olClipRect;
	pDC->GetClipBox(&olClipRect);

	CBrush olBrush(lmBkColor);
	CBrush *polOldBrush = pDC->SelectObject(&olBrush);
	pDC->PatBlt(olClipRect.left, olClipRect.top,
		olClipRect.Width(), olClipRect.Height(),
		PATCOPY);
	pDC->SelectObject(polOldBrush);

    return true;
}

void RelativeTimeScale::OnPaint()
{
	
	CPaintDC dc(this); // device context for painting
	COLORREF llGreen = GREEN;
    COLORREF llWhite = NAVY;

    // Do not call CWnd::OnPaint() for painting messages
    CRect olClientRect; GetClientRect(&olClientRect);
	
    CFont *polOldFont = dc.SelectObject(&ogSmallFonts_Bold_7);
    // CFont *polOldFont = dc.SelectObject(pomFont);
    CPen olPen(PS_SOLID, 1, lmTextColor);
    CPen *polOldPen = dc.SelectObject(&olPen);
    dc.SetBkMode(TRANSPARENT);
    dc.SetTextColor(lmTextColor);

    /*
    // dump time
    char clBuf[64];
    CTime olTime;
    
    olTime = omDisplayStart;
    wsprintf(clBuf, "omDisplayStart: [%d %d %d] [%d:%d:%d]\n\r",
        olTime.GetYear(), olTime.GetMonth(), olTime.GetDay(),
        olTime.GetHour(), olTime.GetMinute(), olTime.GetSecond());
    OutputDebugString(clBuf);

    olTime = omDisplayEnd;
    wsprintf(clBuf, "omDisplayEnd: %d %d %d %d %d %d \n\r",
        olTime.GetYear(), olTime.GetMonth(), olTime.GetDay(),
        olTime.GetHour(), olTime.GetMinute(), olTime.GetSecond());
    OutputDebugString(clBuf);
    */

    int ilDiffMin = omDisplayStart.GetMinute() % (int) omInterval.GetTotalMinutes();
    
    double flCurX = 0.0;
    CTime olCurTime = omDisplayStart;
    if (ilDiffMin > 0)
    {
        // for the left most pixel
        ilDiffMin = (int) omInterval.GetTotalMinutes() - ilDiffMin;
        
        flCurX = ilDiffMin * fmIntervalWidth / omInterval.GetTotalMinutes();
        olCurTime += CTimeSpan(0, 0, ilDiffMin, 0);
    }
    //TRACE("Display First Time: %03d %07.3f\n", ilDiffMin, flCurX);

    while (flCurX <= olClientRect.right)
    {
        dc.MoveTo((int) flCurX, imP3);
        
		int ilY = imP3 - 20;
		int ilCorr = 10;	// correction value to get texts centered about lines 
		CString olText;

		if (Near(olCurTime, omOnBlock - CTimeSpan(0,1,0,0)) == true)
		{	
			if (imRuleType == 2 && bgAdaptScale)
			{
				dc.SetTextColor(llWhite);
				olText = "-300";
			}
			else
			{
				dc.SetTextColor(llGreen);
				olText = "-60";
			}
			dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx / 2) + ilCorr, ilY, olText, olText.GetLength());
		}

		if (Near(olCurTime, omOnBlock) == true)
		{	
			if (imRuleType == 2 && bgAdaptScale)
			{
				dc.SetTextColor(llWhite);
				olText = "-240";
			}
			else
			{
				dc.SetTextColor(llGreen);
				olText = omInboundText;
			}
			dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx / 2) + ilCorr, ilY, olText, olText.GetLength());
		}
		
		if (Near(olCurTime, omOnBlock + CTimeSpan(0,1,0,0)) == true)
		{	
			if (imRuleType == 2 && bgAdaptScale)
			{
				dc.SetTextColor(llWhite);
				olText = "-180";
			}
			else
			{
				dc.SetTextColor(llGreen);
				olText = "+60";
			}
			dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx / 2) + ilCorr, ilY, olText, olText.GetLength());
			
		}
		
		if (Near(olCurTime, omOnBlock + CTimeSpan(0,2,0,0)) == true)
		{	
			if (imRuleType == 1 && bgAdaptScale)
			{
				dc.SetTextColor(llGreen);
				olText = "+120";
				dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx / 2) + ilCorr, ilY, olText, olText.GetLength());
			}
			else if (imRuleType == 2 && bgAdaptScale)
			{
				dc.SetTextColor(llWhite);
				olText = "-120";
				dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx / 2) + ilCorr, ilY, olText, olText.GetLength());
			}
			else
			{
				dc.SetTextColor(llGreen);
				olText = "+120 ";
				dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx) + ilCorr, ilY, olText, olText.GetLength());
				dc.SetTextColor(llWhite);
				olText = " -120";
				dc.TextOut((int) flCurX + /*(dc.GetTextExtent(olText).cx) + */ilCorr, ilY, olText, olText.GetLength());
			}
			
		}

		if (Near(olCurTime, omOfBlock - CTimeSpan(0,1,0,0)) == true)
		{	
			if (imRuleType == 1 && bgAdaptScale)
			{
				dc.SetTextColor(llGreen);
				olText = "+180";
			}
			else
			{
				dc.SetTextColor(llWhite);
				olText = "-60";
			}
			
			dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx / 2) + ilCorr, ilY, olText, olText.GetLength());
		}

		if (Near(olCurTime, omOfBlock) == true)
		{
			if (imRuleType == 1 && bgAdaptScale)
			{
				dc.SetTextColor(llGreen);
				olText = "+240";
			}
			else
			{
				dc.SetTextColor(llWhite);
				//olText = "Outbound";
				olText = omOutboundText ;
			}
			dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx / 2) + ilCorr, ilY, olText, olText.GetLength());
		}
        
		if (Near(olCurTime, omOfBlock + CTimeSpan(0,1,0,0)) == true)
		{	if (imRuleType == 1 && bgAdaptScale)
			{
				dc.SetTextColor(llGreen);
				olText = "+300";
			}
			else
			{
				dc.SetTextColor(llWhite);
				olText = "+60";
			}
			
			dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx / 2) + ilCorr, ilY, olText, olText.GetLength());
		}
		
		if (Near(olCurTime, omOfBlock + CTimeSpan(0,2,0,0)) == true)
		{	if (imRuleType == 1 && bgAdaptScale)
			{
				dc.SetTextColor(llGreen);
				olText = "+360";
			}
			else
			{
				dc.SetTextColor(llWhite);
				olText = "+120";
			}
			dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx / 2) + ilCorr, ilY, olText, olText.GetLength());
		}
		
		if (Near(olCurTime, omOfBlock + CTimeSpan(0,3,0,0)) == true)
		{	if (imRuleType == 1 && bgAdaptScale)
			{
				dc.SetTextColor(llGreen);
				olText = "+420";
			}
			else
			{
				olText = "+180";
				dc.SetTextColor(llWhite);
			}
			dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx / 2) + ilCorr, ilY, olText, olText.GetLength());
		}
		
		if (Near(olCurTime, omOfBlock + CTimeSpan(0,4,0,0)) == true)
		{	if (imRuleType == 1 && bgAdaptScale)
			{
				dc.SetTextColor(llGreen);
				olText = "+480";
			}
			else
			{
				olText = "+240";
				dc.SetTextColor(llWhite);
			}
			dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx / 2) + ilCorr, ilY, olText, olText.GetLength());
		}

		if (Near(olCurTime, omOfBlock + CTimeSpan(0,5,0,0)) == true)
		{	if (imRuleType == 1 && bgAdaptScale)
			{
				dc.SetTextColor(llGreen);
				olText = "+540";
			}
			else
			{
				olText = "+300";
				dc.SetTextColor(llWhite);
			}
			dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx / 2) + ilCorr, ilY, olText, olText.GetLength());
		}
		
		if (Near(olCurTime, omOfBlock + CTimeSpan(0,6,0,0)) == true)
		{	if (imRuleType == 1 && bgAdaptScale)
			{
				dc.SetTextColor(llGreen);
				olText = "+600";
			}
			else
			{
				olText = "+360";
				dc.SetTextColor(llWhite);
			}
			dc.TextOut((int) flCurX - (dc.GetTextExtent(olText).cx / 2) + ilCorr, ilY, olText, olText.GetLength());
		}

        olCurTime += omInterval;
        flCurX += fmIntervalWidth;
    }

    DisplayTopScaleIndicator((CDC *) &dc);
    
    // display current time 
    if (bmDisplayCurrentTime)
    {
		int ilX = GetXFromTime(omCurrentTime);

        CPen olHPen(PS_SOLID, 1, lmHilightColor);
        dc.SelectObject(&olHPen);
    
        dc.MoveTo(ilX, olClientRect.top);
        dc.LineTo(ilX, imP3);
    }
    //
    dc.SelectObject(polOldPen);
    dc.SelectObject(polOldFont);


	////////////////////////////////////////////////////////////////////////	
	// RST 06/08/1997:
	// draws the marker
	
	REL_MARKER *prlMarker;
	CPoint olPoints[3];
    polOldPen = dc.SelectObject(&olPen);
    CBrush *polBrush;

	CRgn *polRgn;
	int ilXPos;
	int ilSize;
	CTime olTime;
	int ilAnzMarkerAtTime;
	int ilLc;
	int ildy;

    for (int  ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		ilAnzMarkerAtTime = 0;
	    for ( ilLc = omMarker.GetSize() - 1; ilLc > ilIndex ; ilLc--)
	    {
			if(omMarker[ilIndex].ActTime == omMarker[ilLc].ActTime) 
				ilAnzMarkerAtTime++;
		}

		prlMarker = &omMarker[ilIndex];
		olTime = prlMarker->ActTime;

		ilXPos = GetXFromTime(olTime) ;
		ilSize = (int)((olClientRect.bottom - olClientRect.top) / 2);
		ildy = (int)(ilAnzMarkerAtTime * (int)(ilSize / 2));

		if(prlMarker->Align == TSM_LEFT)
		{
			olPoints[0].x = ilXPos ;
			olPoints[0].y = olClientRect.bottom - ildy;

			olPoints[1].x = ilXPos ; 
			olPoints[1].y = olClientRect.bottom - ilSize - ildy;
			
			olPoints[2].x = ilXPos + (int)(ilSize / 1.8);
			olPoints[2].y = olClientRect.bottom  - (int)(ilSize / 2) - ildy;
		}

		if(prlMarker->Align == TSM_RIGHT)
		{
			olPoints[0].x = ilXPos + 1;
			olPoints[0].y = olClientRect.bottom - ildy;

			olPoints[1].x = ilXPos + 1; 
			olPoints[1].y = olClientRect.bottom - ilSize - ildy;
			
			olPoints[2].x = ilXPos - (int)(ilSize / 1.8) + 1 ;
			olPoints[2].y = olClientRect.bottom  - (int)(ilSize / 2) - ildy;
		}

		if(prlMarker->Align == TSM_UP)
		{
			olPoints[0].x = ilXPos + 1;
			olPoints[0].y = olClientRect.bottom - ildy;

			olPoints[1].x = ilXPos + (int)(ilSize / 2) ; 
			olPoints[1].y = olClientRect.bottom - (int)(ilSize / 2) - ildy;
			
			olPoints[2].x = ilXPos - (int)(ilSize / 2)  ;
			olPoints[2].y = olClientRect.bottom  - (int)(ilSize / 2) - ildy;
		}
		polBrush = new CBrush(prlMarker->Color);
		polRgn = new CRgn;
		polRgn->CreatePolygonRgn( olPoints, 3,  WINDING);
		dc.FillRgn( polRgn, polBrush );
		delete polRgn;
		delete polBrush;
	}
    dc.SelectObject(polOldPen);

	// RST
	////////////////////////////////////////////////////////////////////////	
}

/////////////////////////////////////////////////////////////////////////////
// RST   14.08.1997

void RelativeTimeScale::OnMouseMove(UINT nFlags, CPoint point) 
{

	REL_MARKER *prlMarker;
	CPoint olPoints[3];
	CRect olClientRect;
	CRect olMarkerRect;
	CRect olMarkerNewRect;
	GetClientRect(&olClientRect);
	
	bool blOK = false;
	int ilXPos;
	CTime olTime;
	CTime olActMarkerTime;
	int ilSize = (int)((olClientRect.bottom - olClientRect.top) / 2);

	SetCapture( );
	
	if((!olClientRect.PtInRect(point)) && (prmCurrMarker == NULL))
	{
		ReleaseCapture();
		if(pomStaticToolTip != NULL)
		{
			pomStaticToolTip->DestroyWindow();
			delete pomStaticToolTip;
			pomStaticToolTip = NULL;
			UpdateWindow();
		}
	}
	else
	{
		if((prmCurrMarker != NULL) && bmLButtonDown)
		{
				olActMarkerTime = prmCurrMarker->ActTime;
				ilXPos = GetXFromTime(olActMarkerTime) ;

				olMarkerRect.top	= olClientRect.top;
				olMarkerRect.bottom = olClientRect.bottom;
				olMarkerRect.left	= ilXPos - (int)(ilSize);
				olMarkerRect.right	= ilXPos + (int)(ilSize);

				olTime = GetTimeFromX(point.x);
				int ilMinute = olTime.GetMinute();
				int ilHour = olTime.GetHour();
				int ilMinutes = ilHour * 60 + ilMinute;

				int ilRest = (int)(ilMinutes % prmCurrMarker->RasterWidth);
				ilMinutes = ilMinutes - ilRest;


				ilHour = (int)(ilMinutes / 60);
				if(ilMinutes > 0) 
					ilMinute = (int)(ilMinutes % 60);
				else
					ilMinute = ilMinutes;

				if(ilRest > (int)(prmCurrMarker->RasterWidth / 2))
					ilMinute += prmCurrMarker->RasterWidth;


				CTime olTimeTmp(CTime(olTime.GetYear(), olTime.GetMonth(), olTime.GetDay(), ilHour, ilMinute, 0));
				
				if((olTimeTmp != olActMarkerTime) && 
				   (prmCurrMarker->RangeFrom != NULL) && 
				   (prmCurrMarker->RangeFrom != NULL))
				{
					if((prmCurrMarker->RangeFrom <= olTimeTmp) && 
					   (prmCurrMarker->RangeTo >= olTimeTmp) && 
					   (TestMarkerAssignPos(prmCurrMarker, olTimeTmp)))
						blOK = true;
				}

				if(blOK)
				{
					ilXPos = GetXFromTime(olTimeTmp) ;
					prmCurrMarker->ActTime = olTimeTmp;

					olMarkerNewRect.top	= olClientRect.top;
					olMarkerNewRect.bottom = olClientRect.bottom;
					olMarkerNewRect.left	= ilXPos - (int)(ilSize) ;
					olMarkerNewRect.right	= ilXPos + (int)(ilSize) ;

					InvalidateRect(olMarkerNewRect);
					InvalidateRect(olMarkerRect);

					if(pomParent == NULL)
						pomParent = GetParent();
					if(pomParent != NULL)
						pomParent->SendMessage(WM_TSCALE_MARKERMOVED,prmCurrMarker->ID,(LPARAM)&prmCurrMarker->ActTime);
			
				}
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
		}
		else
		{
			CCSPtrArray<int> olHits;

			prmCurrMarker = NULL;

			for (int  ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
			{
				prlMarker = &omMarker[ilIndex];
				olTime = prlMarker->ActTime;

				ilXPos = GetXFromTime(olTime) ;
				ilSize = (int)((olClientRect.bottom - olClientRect.top) / 2);

				olMarkerRect.top	= olClientRect.top;
				olMarkerRect.bottom = olClientRect.bottom;
				olMarkerRect.left	= ilXPos - 2;
				olMarkerRect.right	= ilXPos + 2;

				if(olMarkerRect.PtInRect(point))
				{
					olHits.New(ilIndex);
				}
			}
			if(olHits.GetSize() > 0)
			{
				int ilAnz = olHits.GetSize();
				int ilBottom = 0;
				int ilTop	= olClientRect.top;
				CString olToolTipText;

				for(int ilLc = 0; ilLc < olHits.GetSize(); ilLc++)
				{
					prlMarker = &omMarker[olHits[ilLc]];
					olTime = prlMarker->ActTime;
					olToolTipText = prlMarker->ToolTipText;

					ilXPos = GetXFromTime(olTime) ;
					ilTop = olClientRect.bottom - (ilSize / 2) * (ilLc + 1);
					ilBottom = olClientRect.bottom - (ilSize / 2) * ilLc;

					if((point.y < ilBottom ) && (point.y > ilTop))
						break;

				}

				if(olClientRect.bottom - (ilSize +  (olHits.GetSize() - 1) * (ilSize / 2)) < point.y) 
				{
					prmCurrMarker = prlMarker; 
					if(!olToolTipText.IsEmpty())
					{
						int ilXMid = (GetXFromTime(omDisplayStart + GetDisplayDuration())- GetXFromTime(omDisplayStart)) / 2;
						CDC *polDC;
						polDC = GetDC();
						CSize olTextSize = polDC->GetTextExtent(olToolTipText );

						olMarkerRect.top	= 3; 
						olMarkerRect.bottom = 3 + olTextSize.cy + 2;
						if(point.x > ilXMid)
						{
							olMarkerRect.left	= ilXPos - (ilSize / 2) - olTextSize.cx - 6;
							olMarkerRect.right	= ilXPos - (ilSize / 2);
						}
						else
						{
							olMarkerRect.left	= ilXPos + (ilSize / 2);
							olMarkerRect.right	= ilXPos + (ilSize / 2) + olTextSize.cx + 6;
						}

						if(pomStaticToolTip != NULL)
						{
							CString olStr;
							pomStaticToolTip->GetWindowText(olStr);
							if(olStr != olToolTipText)
							{
								pomStaticToolTip->DestroyWindow();
								delete pomStaticToolTip;
								pomStaticToolTip = NULL;
								UpdateWindow();
							}
						}

						if(pomStaticToolTip == NULL)
						{
							pomStaticToolTip = new CStatic;
							pomStaticToolTip->Create( olToolTipText, WS_CHILD | WS_VISIBLE | WS_BORDER | SS_CENTER , olMarkerRect , this, 1);
							pomStaticToolTip->UpdateWindow();
						}
					}
					if(pomParent == NULL)
						pomParent = GetParent();
					if(pomParent != NULL)
						pomParent->SendMessage(WM_TSCALE_MARKERSELECT,prmCurrMarker->ID ,(LPARAM)&olTime);
				}
			}		
			else
			{
				olTime = GetTimeFromX(point.x);
				if(pomParent == NULL)
					pomParent = GetParent();
				if(pomParent != NULL)
					pomParent->SendMessage(WM_TSCALE_MARKERSELECT,NULL ,(LPARAM)&olTime);
				if(pomStaticToolTip != NULL)
				{
					pomStaticToolTip->DestroyWindow();
					delete pomStaticToolTip;
					pomStaticToolTip = NULL;
					UpdateWindow();
				}
			}
			olHits.DeleteAll();
		}
	}
	CWnd::OnMouseMove(nFlags, point);
}



void RelativeTimeScale::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if(pomStaticToolTip != NULL)
	{
		pomStaticToolTip->DestroyWindow();
		delete pomStaticToolTip;
		pomStaticToolTip = NULL;
	}

	if(prmCurrMarker != NULL)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
		bmLButtonDown = true;
	}


	CWnd::OnLButtonDown(nFlags, point);
}


void RelativeTimeScale::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CRect olClientRect;
	GetClientRect(&olClientRect);
	
	if(!olClientRect.PtInRect(point))
		ReleaseCapture();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	prmCurrMarker = NULL;
	bmLButtonDown = false;
	CWnd::OnLButtonUp(nFlags, point);
}

  
void RelativeTimeScale::SetMarkerRaster(int ipID, int ipRasterWidth)
{

	REL_MARKER *prlMarker;
    for (int ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		if(ipID == omMarker[ilIndex].ID)
		{
			prlMarker = &omMarker[ilIndex];
			prlMarker->RasterWidth		= ipRasterWidth;
			return;
		}
    }
	
	return ;
}


bool RelativeTimeScale::ChangeMarkerRange(int ipID, CTime opNewTime, CTime opRangeFrom, CTime opRangeTo)
{
	REL_MARKER *prlMarker;
    for (int ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		if(ipID == omMarker[ilIndex].ID)
		{
			prlMarker = &omMarker[ilIndex];
			prlMarker->RangeFrom	= opRangeFrom;
			prlMarker->RangeTo		= opRangeTo;
			if(opNewTime != NULL)
				prlMarker->ActTime		= opNewTime;
			return true;
		}
    }
	return false;
}


bool RelativeTimeScale::ChangeMarkerColor(int ipID, COLORREF opColor)
{
	REL_MARKER *prlMarker;
    for (int ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		if(ipID == omMarker[ilIndex].ID)
		{
			prlMarker = &omMarker[ilIndex];
			prlMarker->Color = opColor;
			return true;
		}
    }
	return false;
}


bool RelativeTimeScale::ChangeMarkerPos(int ipID, CTime opTime)
{
	REL_MARKER *prlMarker;
    for (int ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		if(ipID == omMarker[ilIndex].ID)
		{
			prlMarker = &omMarker[ilIndex];
			prlMarker->ActTime		= opTime;
			InvalidateRect(NULL);
			return true;
		}
    }
	return false;
}

void RelativeTimeScale::AddMarker(int ipID, CTime opTime, 
				   COLORREF opColor, 
				   int ipRasterWidth, 
				   int ipAlign,
				   int ipIDAssign,
				   CTime opRangeFrom, 
				   CTime opRangeTo,
				   CString opToolTipText)
{
	DeleteMarker(ipID);
	REL_MARKER *polMarker = new REL_MARKER;
	omMarker.Add(polMarker);

	polMarker->RangeFrom	= opRangeFrom;
	polMarker->RangeTo		= opRangeTo;
	polMarker->ID			= ipID;
	polMarker->Align		= ipAlign;
	polMarker->Assign		= ipIDAssign;
	polMarker->ActTime		= opTime;
	polMarker->Color		= opColor;
	polMarker->RasterWidth	= ipRasterWidth;
	polMarker->ToolTipText	= opToolTipText;
}


void RelativeTimeScale::DeleteMarker(int ipID)
{
	REL_MARKER *prlMarker;
	int ilSize, ilXPos;
	CRect	olMarkerRect;
	CRect olClientRect;
	GetClientRect(&olClientRect);

    for (int ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		if(ipID == omMarker[ilIndex].ID)
		{
			prlMarker = &omMarker[ilIndex];
		
			ilSize = (int)((olClientRect.bottom - olClientRect.top) / 2);
			ilXPos = GetXFromTime(prlMarker->ActTime) ;
			olMarkerRect.top	= olClientRect.bottom - ilSize;
			olMarkerRect.bottom = olClientRect.bottom;
			olMarkerRect.left	= ilXPos - (int)(ilSize / 1,7) ;
			olMarkerRect.right	= ilXPos + (int)(ilSize / 1.7) ;
			InvalidateRect(olMarkerRect);

			omMarker.DeleteAt(ilIndex);
			break;
		}
    }
}


void RelativeTimeScale::DeleteAllMarker()
{
    omMarker.DeleteAll();
}


bool RelativeTimeScale::TestMarkerAssignPos(REL_MARKER *prpMarker, CTime opNewTime)
{
	if(prpMarker->Assign == 0)
		return true;

	REL_MARKER *prlMarker;
    for (int ilIndex = omMarker.GetSize() - 1; ilIndex >= 0 ; ilIndex--)
    {
		if(prpMarker->Assign == omMarker[ilIndex].ID)
		{
			prlMarker = &omMarker[ilIndex];
		}
	}
	if(prlMarker == NULL)
		return true;

	if(prlMarker->ActTime > prpMarker->ActTime)
	{
		if(prlMarker->ActTime <= opNewTime)
			return false;
	}
	else
	{
		if(prlMarker->ActTime >= opNewTime)
			return false;
	}

	return true;
}



void RelativeTimeScale::SetRuleType(int ipType)
{
	imRuleType = ipType;
}


bool RelativeTimeScale::Near(CTime opTime1, CTime opTime2)
{ 
	// ---------------------------------------------
	// if both times are less than 80 seconds apart, 
	// function returns true, otherweise zero
	// ---------------------------------------------

	if (-80 < (opTime1 - opTime2).GetTotalSeconds() && (opTime1 - opTime2).GetTotalSeconds() < 80)
	{
		return true;
	}
	
	return false;	
}


void RelativeTimeScale::SetReferenceTexts(CString opInb, CString opOutb)
{
	omInboundText = opInb;
	omOutboundText = opOutb;
}


/*
HBRUSH RelativeTimeScale::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = 0;

	CWnd *polWnd = GetDlgItem(1);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		pDC->SetBkColor(RGB(255,255,217));
		hbr = CreateSolidBrush(RGB(255,255,217));
		return hbr;
	}
	
	return hbr;
}
*/

// RST  
/////////////////////////////////////////////////////////////////////////////
