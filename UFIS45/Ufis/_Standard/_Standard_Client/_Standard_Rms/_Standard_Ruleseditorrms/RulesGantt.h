// stgantt.h : header file
//

#ifndef __RULESGANTT__
#define __RULESGANTT__

///////

//#include "stdafx.h"
//#include "jobdv.h"
#include <CCSTimeScale.h>
#include <CCSDragDropCtrl.h>
#include <RulesGanttViewer.h>
#include <DemandDetailDlg.h>
#include <ConnectBars.h>

// Special time value definition
#ifndef TIMENULL
#define TIMENULL    CTime((time_t)-1)
#endif

//#define BAR_CHANGED (WM_USER+11006)
#define SER_BAR_DELETED (WM_USER+11007)

class CRulesFormView;
/////////////////////////////////////////////////////////////////////////////
// JobDetailGantt window


class RulesGantt : public CListBox
{
// Operations
public:
	RulesGantt::RulesGantt(RulesGanttViewer *popViewer = NULL, 
        int ipVerticalScaleWidth = 0, int ipVerticalScaleIndent = 15,
        CFont *popVerticalScaleFont = NULL, CFont *popGanttChartFont = NULL,
        int ipGutterHeight = 2, int ipOverlapHeight = 4, double dpArrowHeightFactor = 1.6,
        COLORREF lpVerticalScaleTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightVerticalScaleTextColor = ::GetSysColor(COLOR_HIGHLIGHTTEXT),
        COLORREF lpHighlightVerticalScaleBackgroundColor = ::GetSysColor(COLOR_HIGHLIGHT),
        COLORREF lpGanttChartTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightGanttChartTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW));
	
	RulesGantt::~RulesGantt();


    void SetViewer(RulesGanttViewer *popViewer);
    void SetVerticalScaleWidth(int ipWidth);
	void SetVerticalScaleIndent(int ipIndent);
    void SetFonts(int index1, int index2);
    void SetGutters(int ipGutterHeight, int ipOverlapHeight);
    void SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);
    void SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);

    int GetGanttChartHeight();
    int GetLineHeight(int ipMaxOverlapLevel);
	int GetVerticalScaleWidth();

    BOOL Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID = 0);
    void SetStatusBar(CStatic *popStatusBar);
	void SetTimeScale(CCSTimeScale *popTimeScale);
    void SetBorderPrecision(int ipBorderPrecision);
	void SetArrowHitWidth(int ipArrowHitWidth);

    // This group of function will automatically repaint the window
    void SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling = TRUE);
    void SetDisplayStart(CTime opDisplayStart);
    void SetCurrentTime(CTime opCurrentTime);
    void SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd);

    void RepaintVerticalScale(int ipLineno = -1, BOOL bpErase = TRUE);
    void RepaintGanttChart(int ipLineno = -1, CTime opStartTime = TIMENULL, CTime opEndTime = TIMENULL, BOOL bpErase = TRUE);
    void RepaintItemHeight(int ipLineno);
	void RepaintLine(int ipLineno);
	void ResetItemHeight(int ipLineno);
	void AttachWindow(CRulesFormView *popWnd = NULL);
	void EnableEditRud ( bool bpEnable );
	
	// void DragDutyBarBegin(int ipLineno);
	void DrawThickLine(int itemID, int ilOldLine);

// Attributes
public:
	CRulesFormView*pomRegelWerkView;

    RulesGanttViewer *pomViewer;	// corresponding viewer
	CStatic *pomStatusBar;			// the status bar where the notification message goes
	CCSTimeScale *pomTimeScale;		// the time scale for top scale indicators display
	DemandDetailDlg *pomDetailDlg;	// 
	ConnectBars *pomConnectionDlg;   // dialog, in which connection arrows can be edited
	
	CCSPtrArray <CON_POSDATA> omSelArrows;

	BARDATA rmActiveBar;		// Bar which is currently moved or sized
	BARDATA *prmContextBar;		// Save bar for Context Menu handling
	BARDATA *prmPreviousBar;		// Last used bar	
	
	CTime omDisplayStart;
    CTime omDisplayEnd;
    CTime omCurrentTime;                    // the red vertical time line
    CTime omMarkTimeStart, omMarkTimeEnd;   // two yellow vertical time lines

	CPoint omPointResize;       // the point where moving/resizing begin
    CRect omRectResize;         // current focus rectangle for moving/resizing

    UINT imResizeMode;          // HTNOWHERE, HTCAPTION (moving), HTLEFT, or HTRIGHT
	int imGroupno;              // index to a group in the viewer
    int imVerticalScaleWidth;
	int imVerticalScaleIndent;	// number of pixels used to indent text in VerticalScale
    int imBarHeight;            // calculated when SetGanttChartFont()
    int imGutterHeight;         // space between the GanttLine and the topmost-level bar
    int imLeadingHeight;        // space between the bar and the border
    int imOverlapHeight;        // space between the overlapped bar
    int imWindowWidth;          // in pixels, for pixel/time ratio (updated by WM_SIZE)
	int imHighlightLine;        // the current line that painted with highlight color
    int imCurrentBar;           // the current bar that the user places mouse over
	int imBorderPreLeft;        // define the sensitivity when user detects bar border
    int imBorderPreRight;       // define the sensitivity when user detects bar border
	int imArrowHitWidth;		// define the sensitivity when user detects connection arrow
	
	bool bmMoveBar;				// dragging bars is enabled/disabled
	int imActiveItem;			// when dragging bars: line where mouse button went down
	int imCurrItem;				// line where cursor is over currently 
	int imPrevItem;				// line where cursor was over last
	
	double dmArrowHeightFactor;	// factor for the line height to grow when there's an arrow passing the bar horizontically
	
	CString omActiveLine;	// Urno of last selected line
	CString omCurrRudUrno;
	
	// bool bmIsConnectionMode;
	BOOL bmIsMouseInWindow;
    BOOL bmIsControlKeyDown;
	BOOL bmActiveBarSet;
	BOOL bmContextBarSet;
	BOOL bmActiveLineSet;  // Is there a selected line ?
	BOOL bmIsFixedScaling;	// There are two modes for updating the display window when WM_SIZE is sent: 
							// variable and fixed scaling. // The mode of this operation will be given in 
							// the method SetDisplayWindow().

	COLORREF lmVerticalScaleTextColor;
    COLORREF lmVerticalScaleBackgroundColor;
    COLORREF lmHighlightVerticalScaleTextColor;
    COLORREF lmHighlightVerticalScaleBackgroundColor;
    COLORREF lmGanttChartTextColor;
    COLORREF lmGanttChartBackgroundColor;
    COLORREF lmHighlightGanttChartTextColor;
    COLORREF lmHighlightGanttChartBackgroundColor;

	CFont *pomVerticalScaleFont;
    CFont *pomGanttChartFont;

	long rmLinkColors[10];
	
// Implementation
public:
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
    virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual int OnToolHitTest(CPoint point, TOOLINFO* pTI) const;

private:
	void DrawDotLines(CDC *pDC, int itemID, const CRect &rcItem);
    void DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem);
    void DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
	void DrawConnections(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
	void DrawLinks(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
    void DrawTimeLines(CDC *pDC, int top, int bottom);
	void DrawFocusRect(CDC *pDC, const CRect &rect);
	void MakeBarMenu(int itemID, UINT nFlags, CPoint point, bool bpIsArrow);
	void GetLinkedBar(CString &opLinkedBar, int &ipLinkedLine);
	CRect GetDrawingRect(CString opLinkUrno, CRect opItem);
	bool GetBarRect(int ipLineno, int ipBarno, CRect &ropRect );
	void DrawUnits(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
	void SwitchBars();

protected:
    // Generated message map functions
    //{{AFX_MSG(RulesGantt)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMenuEdit();
	afx_msg void OnMenuActivate();
	afx_msg void OnMenuOperative();
    afx_msg void OnMenuPartialDemand();
	afx_msg void OnMenuCalculatedDemand();
	afx_msg void OnMenuDeleteBar();
	afx_msg void OnMenuCopyBar();
	afx_msg void OnMenuConnectBar();
	afx_msg void OnMenuRemoveConnection();
	afx_msg void OnMenuCreateLink();
	afx_msg void OnMenuDeleteLink();
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnDetailDlgClosed(WPARAM, LPARAM);
	afx_msg void OnMenuCreateUnit();
	afx_msg void OnMenuDeleteUnit();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
    void UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown);
    void UpdateBarStatus(int ipLineno, int ipBarno);
    bool BeginMovingOrResizing(CPoint point);
    bool OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown);
    bool CheckIncreasedLineHeight(int itemID);
	int  GetItemFromPoint(CPoint point)const;
    int  GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1) const;
	void CheckDetailWindow();
    

	// HitTest will use the precision defined in "imBorderPrecision"
	UINT GetHit(int ipLineno, int ipBarno, CPoint point);
	int GetBarYPos(CRect rcItem, int sourceID, int itemID, int ipBar);
	bool CheckConnectionHit(CPoint point, int &ripLine, int &ripArrow, CString &ropArrowID);
	
	BOOL IsDropOnDutyBar(CPoint point);

private:
	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl	m_DragDropTarget;
	CPoint			omDropPosition;
	bool			bmEditEnabled;
	void DragServiceBarBegin(int ipLineno, int ipBarno);
	LONG ProcessDropDutyBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
};

/////////////////////////////////////////////////////////////////////////////

#endif //__RULESGANTT__
