
// ComboBoxBar.cpp : implementation file
//

#include <stdafx.h>
#include <RegelWerk.h>
#include <ComboBoxBar.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CComboBoxBar

//--------------------------
CComboBoxBar::CComboBoxBar()
{
   pomComboBox = NULL;
   // pomToggleButton = NULL;
   pomToolBar = NULL;
}


CComboBoxBar::~CComboBoxBar()
{
}



BOOL  CComboBoxBar::LoadToolBar(UINT nIDResource) 
{
	pomToolBar = AddScrollToolBar ( nIDResource );

	if(!pomToolBar)
		return FALSE;

	//--- set style to flat
	pomToolBar->ModifyStyle(0 , TBSTYLE_TOOLTIPS);

	//--- set text 
	CToolBarCtrl&  bar = pomToolBar->GetToolBarCtrl();


	TBBUTTON tb;
	for(int ilIndex = bar.GetButtonCount() - 1; ilIndex >= 0; ilIndex--)
	{
		 ZeroMemory(&tb , sizeof(TBBUTTON));
		 bar.GetButton(ilIndex , &tb);

		//--- if separator
		if((tb.fsStyle & TBSTYLE_SEP) == TBSTYLE_SEP)
		continue; 
		
		 //--- if ID invalid
		 if((tb.idCommand) == 0)
			 continue;
	
		 //--- create string
		 LPCSTR   lpszButtonText = NULL;
		 CString  strButtonText(_T(""));
		 _TCHAR   seps[]= _T("\n");
		 CString olString, strText;
		 
		//strText.LoadString(tb.idCommand);
		olString = GetString(tb.idCommand);
		olString.LockBuffer();
		strText = olString;
		olString.UnlockBuffer();

		if(!strText.IsEmpty())
		{
			lpszButtonText = _tcstok((LPSTR) (LPCSTR) strText , seps); 

			while(lpszButtonText)
			{
				strButtonText  = lpszButtonText;
				lpszButtonText = _tcstok(NULL , seps);
			}
		}

		if(! strButtonText.IsEmpty())
			pomToolBar->SetButtonText(ilIndex , strButtonText);

	}

	//--- Resize  Buttons
//	CSize  sizeMax(80 , 45);

//	pomToolBar->SetSizes(sizeMax , CSize(30 , 25));

	return TRUE;
}


void  CComboBoxBar::HideServicesListBtn(bool bpHide)
{
	/*
	CToolBarCtrl&  bar = pomToolBar->GetToolBarCtrl();

	bool blTest = bar.IsButtonHidden(MENU_OPT_SERVLIST);

	if (bpHide && !bar.IsButtonHidden(MENU_OPT_SERVLIST))
		bar.HideButton(MENU_OPT_SERVLIST, TRUE);

	if (!bpHide && bar.IsButtonHidden(MENU_OPT_SERVLIST))
		bar.HideButton(MENU_OPT_SERVLIST, FALSE);*/
	HideToolbarBtn ( MENU_OPT_SERVLIST, bpHide );
}

BOOL  CComboBoxBar::HideToolbarBtn(int ipID, bool bpHide)
{
	CToolBarCtrl&  bar = pomToolBar->GetToolBarCtrl();

	if ( bar.GetState ( ipID ) == -1 )
		return FALSE;

	BOOL blRet = TRUE;
	bool blTest = bar.IsButtonHidden(ipID);

	if (bpHide && !bar.IsButtonHidden(ipID))
		blRet = bar.HideButton(ipID, TRUE);

	if (!bpHide && bar.IsButtonHidden(ipID))
		blRet = bar.HideButton(ipID, FALSE);
	return blRet;
}

/////////////////////////////////////////////////////////////////////////////
// CComboBoxBar message handlers

//---- Test eigene Message Map -------

 BEGIN_MESSAGE_MAP(CComboBoxBar , CScrollRebar)
   //{{AFX_MSG_MAP(CComboBoxBar) 
  //}}AFX_MSG_MAP  
 END_MESSAGE_MAP()


