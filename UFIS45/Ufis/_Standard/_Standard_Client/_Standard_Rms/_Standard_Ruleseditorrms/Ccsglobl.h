// CCSGlobl.h: interface for the CCSGlobl class.
//
//////////////////////////////////////////////////////////////////////

// modified by MNE, March 1999:

// REMARK:	Most of the structs used anywhere in the program can be found in this file.
//			There's one major exception: All structs that have to do with views
//			are contained in CedaCfgData. 



#if !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
#define AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_

#ifndef __CCSGLOBL_H__
#define __CCSGLOBL_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <CCSDefines.h>
#include <ccsdragdropctrl.h>
#include <CCSTime.h>
#include <CCSPtrArray.h>
#include <PrivList.h>
#include <DataSet.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <ChangeInfo.h>
#include <GuiLng.h>
#include <vector>
// #include "DialogGrid.h"

//----------------------------------
//		forward declarations
//----------------------------------
class CCSBcHandle;
class CCSCedaCom;
class CCSDdx;
class CCSLog;
class CCSBcHandle;
class CedaBasicData;
class CInitialLoadDlg;
// actually the things contained in the local BasicData class should be part of CCSBasic (at least most of them)
class CCSBasic;		// (lib)
class CBasicData;	// (local)
class ChangeInfo;
class CGridFenster;



//----------------------------------
//			define section
//----------------------------------

#define IDS_GRID_FORMATSTR 1

//--- CFont : translate from logical point to physical pixel
#define PT_TO_PIXELS(dc, pt)    (- MulDiv( pt, (dc).GetDeviceCaps( LOGPIXELSY ), 72 ))

//--- IDs from the origin of a conflict: 
#define MOD_ID_ROTATION					0x00000001L
#define MOD_ID_SEASON					0x00000004L
#define MOD_ID_DIA						0x00000008L

//--- messages
// grid
#define GRID_MESSAGE_CELLCLICK			(WM_USER + 1)
#define GRID_MESSAGE_BUTTONCLICK		(WM_USER + 2)
#define GRID_MESSAGE_DOUBLECLICK		(WM_USER + 3)
#define GRID_MESSAGE_ENDEDITING			(WM_USER + 4)
#define GRID_MESSAGE_STARTEDITING		(WM_USER + 5)
#define GRID_MESSAGE_CURRENTCELLCHANGED (WM_USER + 6)
#define GRID_MESSAGE_RBUTTONCLICK		(WM_USER + 7)
#define GRID_MESSAGE_ENDSELECTION		(WM_USER + 8)
#define GRID_MESSAGE_RESORTED			(WM_USER + 9)
// gantt context menu
#define CM_GANTT_MENU_EDIT				(WM_USER + 19)					
#define CM_GANTT_MENU_ACTIVATE			(WM_USER + 20)
#define CM_GANTT_MENU_OPERATIVE 		(WM_USER + 21)
#define CM_GANTT_MENU_PARTIALDEM		(WM_USER + 22)
#define CM_GANTT_MENU_CALCULATEDDEM		(WM_USER + 23)
#define CM_GANTT_MENU_DELETEBAR			(WM_USER + 24)
#define CM_GANTT_MENU_CONNECTBAR		(WM_USER + 25)
#define CM_GANTT_MENU_REMOVECON			(WM_USER + 26)
#define CM_GANTT_MENU_CREATELNK			(WM_USER + 27)
#define CM_GANTT_MENU_DELETELNK			(WM_USER + 28)
#define CM_GANTT_MENU_COPYBAR			(WM_USER + 29)
#define CM_GANTT_MENU_CREATEUNIT		(WM_USER + 30)
#define CM_GANTT_MENU_DELETEUNIT		(WM_USER + 31)
#define CM_INDEP_MENU_DELETEBAR			(WM_USER + 40)
#define CM_COLLECT_MENU_CREATELNK		(WM_USER + 41)
#define CM_COLLECT_MENU_DELETELNK		(WM_USER + 42)
#define CM_COLLECT_MENU_COPYBAR			(WM_USER + 43)
#define CM_COLLECT_MENU_DELETEBAR		(WM_USER + 44)
#define CM_INDEP_MENU_CREATELNK			(WM_USER + 45)
#define CM_INDEP_MENU_DELETELNK			(WM_USER + 46)
#define CM_INDEP_MENU_CREATEUNIT		(WM_USER + 47)
#define CM_INDEP_MENU_DELETEUNIT		(WM_USER + 48)


// ghslist (create group messages)
#define WM_SERLIST_LINK					(WM_USER + 50) // mne 000911
#define WM_SERLIST_NOLINK				(WM_USER + 51) // mne 000911
// other
#define WM_POSITIONCHILD				(WM_USER + 201)
#define WM_UPDATEDIAGRAM				(WM_USER + 202)
#define UD_UPDATEGROUP                  (WM_USER + 203)
#define UD_DELETELINE                   (WM_USER + 204)
#define UD_DELETEGROUP                  (WM_USER + 205)
#define UD_INSERTLINE                   (WM_USER + 206)
#define UD_INSERTGROUP                  (WM_USER + 207)
#define UD_UPDATELINEHEIGHT             (WM_USER + 208)
#define UD_UPDATELINE					(WM_USER + 209)
#define UD_UPDATEDLG					(WM_USER + 210)
#define UD_RESETCONTENT					(WM_USER + 211)
#define WM_REASSIGNFINISHED				(WM_USER + 212)
#define WM_ATTENTIONSETCOLOR			(WM_USER + 213)
#define UD_REASSIGNFINISHED				(WM_USER + 213)
#define WM_SETTIMELINES					(WM_USER + 216)
#define WM_REPAINT_ALL					(WM_USER + 217)
#define WM_DIALOG_CLOSED				(WM_USER + 218)
#define WM_RULE_SAVED_OK				(WM_USER + 219)
#define WM_RULE_MODIFIED				(WM_USER + 229)

#define WM_ADAPTTIME     				(WM_USER + 300)

#define WM_CCSBUTTON_RBUTTONDOWN		(WM_USER + 350)
#define WM_MARK_BAR						(WM_USER + 351)

#define BAR_CHANGED						(WM_USER + 11006)
#define WM_STAFFTABLE_EXIT				(WM_APP + 20)	/* tables */
#define WM_GEQTABLE_EXIT				(WM_APP + 21)	/* tables */
#define WM_INFTABLE_EXIT				(WM_APP + 22)	/* tables */
#define WM_HWDIA_EXIT					(WM_APP + 23)	/* tables */
#define WM_FLIGHTDIA_EXIT				(WM_APP + 24)	/* tables */
#define WM_STAFFDIA_EXIT				(WM_APP + 25)	/* tables */
#define WM_CONFTABLE_EXIT				(WM_APP + 26)	/* tables */
#define WM_FLIGHTTABLE_EXIT				(WM_APP + 27)	/* tables */
#define WM_MAGNETICTABLE_EXIT			(WM_APP + 28)	/* tables */
#define WM_ATTENTIONTABLE_EXIT			(WM_APP + 29)	/* tables */
#define WM_CONFLICTTABLE_EXIT			(WM_APP + 30)	/* tables */
#define WM_LOCKWINDOWUPDATE				(WM_APP + 31)	/* tables */
#define WM_ZOLLTABLE_EXIT				(WM_APP + 32)	/* tables */
#define WM_PLACEROUND					(WM_APP + 33)	/* tables */
#define WM_TOUCHANDGO					(WM_APP + 34)	/* tables */
#define WM_MAXIMIZE_CHILD				(WM_APP + 35)	

//--- IDs
#define IDC_TIMESCALE					0x4001
#define IDC_VERTICALSCALE				0x4002
#define IDC_GANTTBAR					0x4003
#define IDD_CHART						0x4004
#define IDC_CHARTBUTTON					0x4005
#define IDC_CHARTBUTTON2				0x4006
#define IDD_GANTT						0x4007
#define IDC_PREV						0x4008
#define IDC_NEXT						0x4009
#define IDC_ARRIVAL						0x400a
#define IDC_DEPARTURE					0x400b
#define IDC_INPLACEEDIT					0x400c
#define IDC_CHARTBUTTON3				0x400d
#define IDD_GANTTCHART					0x4101

#define IDM_ROTATION					11L
#define IDM_SEASON						14L
#define IDM_IMPORT						17L
#define IDM_COMMONCCA					19L

//--- Symbolic colors (helper constants for CGateDiagramDialog -- testing purpose only)
#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
//#define SILVER  RGB(192, 192, 192)          // light gray
#define SILVER  ::GetSysColor(COLOR_BTNFACE)
#define RED     RGB(255,   0,   0)
#define ORANGE  RGB(255, 132,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255,   0)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(  0, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)
#define LGREEN  RGB(128, 255, 128)
#define PYELLOW RGB(255, 255, 180)			// pastel yellow
#define PGREEN  RGB(190, 255, 190)			// pastel green
#define PBLUE   RGB(157, 255, 255)			// pastel blue
#define ROSE    RGB(255, 160, 160)			// pastel red
#define DARKBLUE	RGB( 0,   0, 160)			// dark blue
#define NOT_SO_DARK_BLUE RGB(20,20,200)			// not so dark blue
#define GREENBROWN  RGB(128,  96,   0)			// greenish brown
#define LIGHTBROWN  RGB(228, 185, 105)			// light brown
#define LROSE    RGB(255, 192, 192)			// light rose


//--- some values for color stuff
#define MAXCOLORS 64
#define FIRSTCONFLICTCOLOR 31

//----------------------------------
//				enums
//----------------------------------

enum drool {SKIP = -1, NO, YES};

//--- Chart State
enum ChartState{ Minimized, Normal, Maximized };

//--- colors 
enum enumColorIndexes
{
	RED_IDX = 0, WHITE_IDX, BLACK_IDX, BLUE_IDX, YELLOW_IDX, GREEN_IDX, GRAY_IDX, ORANGE_IDX, SILVER_IDX, MAROON_IDX, 
	LGREEN_IDX, OLIVE_IDX, NAVY_IDX, PURPLE_IDX, TEAL_IDX, LIME_IDX, FUCHSIA_IDX, AQUA_IDX, PYELLOW_IDX, PGREEN_IDX, 
	PBLUE_IDX, ROSE_IDX, DARKBLUE_IDX,GREENBROWN_IDX, LIGHTBROWN_IDX
};


//--- fonts
enum
{
	MS_SANS6, MS_SANS8, MS_SANS12, MS_SANS16, 
    MS_SANS6BOLD, MS_SANS8BOLD, MS_SANS12BOLD, MS_SANS16BOLD
};

//--- Drag Information Type
enum 
{
	DIT_SERLIST
};

//--- ?
enum
{
	DLG_NEW,
	DLG_COPY,
	DLG_CHANGE
};

//--- DDX
enum enumDDXTypes
{
	//---------------------------------------------------
	//	When editing this first part of enumDDXTypes, don't 
	//  forget to adapt 
	//---------------------------------------------------
	BC_ACR_NEW = 0, BC_ACR_DELETE, BC_ACR_CHANGE,	// ACR
	ACR_NEW, ACR_DELETE, ACR_CHANGE,	
	BC_ACT_NEW, BC_ACT_DELETE, BC_ACT_CHANGE,	// ACT
	ACT_NEW, ACT_DELETE, ACT_CHANGE,
	BC_ALO_NEW, BC_ALO_DELETE, BC_ALO_CHANGE,	// ALO
	ALO_NEW, ALO_DELETE, ALO_CHANGE,
	BC_ALT_NEW, BC_ALT_DELETE, BC_ALT_CHANGE,	// ALT
	ALT_NEW, ALT_DELETE, ALT_CHANGE,
	BC_APT_NEW, BC_APT_DELETE, BC_APT_CHANGE,	// APT
	APT_NEW, APT_DELETE, APT_CHANGE,
	BC_BLT_NEW, BC_BLT_DELETE, BC_BLT_CHANGE,	// BLT
	BLT_NEW, BLT_DELETE, BLT_CHANGE,
	BC_CCC_NEW, BC_CCC_DELETE, BC_CCC_CHANGE,	// CCC
	CCC_NEW, CCC_DELETE, CCC_CHANGE,
	BC_CHT_NEW, BC_CHT_DELETE, BC_CHT_CHANGE,	// CHT
	CHT_NEW, CHT_DELETE, CHT_CHANGE,
	BC_CIC_NEW, BC_CIC_DELETE, BC_CIC_CHANGE,	// CIC
	CIC_NEW, CIC_DELETE, CIC_CHANGE,
	BC_DGR_NEW, BC_DGR_DELETE, BC_DGR_CHANGE,	// DGR
	DGR_NEW, DGR_DELETE, DGR_CHANGE,
	BC_EQU_NEW, BC_EQU_DELETE, BC_EQU_CHANGE,	// EQU
	EQU_NEW, EQU_DELETE, EQU_CHANGE,
	BC_EXT_NEW, BC_EXT_DELETE, BC_EXT_CHANGE,	// EXT
	EXT_NEW, EXT_DELETE, EXT_CHANGE,
	BC_GAT_NEW, BC_GAT_DELETE, BC_GAT_CHANGE,	// GAT
	GAT_NEW, GAT_DELETE, GAT_CHANGE,
	BC_GEG_NEW, BC_GEG_DELETE, BC_GEG_CHANGE,	// GEG
	GEG_NEW, GEG_DELETE, GEG_CHANGE,
	BC_HAG_NEW, BC_HAG_DELETE, BC_HAG_CHANGE,	// HAG
	HAG_NEW, HAG_DELETE, HAG_CHANGE,
	BC_HTY_NEW, BC_HTY_DELETE, BC_HTY_CHANGE,	// HTY
	HTY_NEW, HTY_DELETE, HTY_CHANGE,
	BC_PER_NEW, BC_PER_DELETE, BC_PER_CHANGE,	// PER
	PER_NEW, PER_DELETE, PER_CHANGE,
	BC_PST_NEW, BC_PST_DELETE, BC_PST_CHANGE,	// PST
	PST_NEW, PST_DELETE, PST_CHANGE,
	BC_RUD_NEW, BC_RUD_DELETE, BC_RUD_CHANGE,	// RUD
	RUD_NEW, RUD_DELETE, RUD_CHANGE, 
	BC_RUE_NEW, BC_RUE_DELETE, BC_RUE_CHANGE,	// RUE
	RUE_NEW, RUE_DELETE, RUE_CHANGE,
	BC_RWY_NEW, BC_RWY_DELETE, BC_RWY_CHANGE,	// RWY
	RWY_NEW, RWY_DELETE, RWY_CHANGE,
	BC_SER_NEW, BC_SER_DELETE, BC_SER_CHANGE,	// SER
	SER_NEW, SER_DELETE, SER_CHANGE,
	BC_SGR_NEW, BC_SGR_DELETE, BC_SGR_CHANGE,	// SGR
	SGR_NEW, SGR_DELETE, SGR_CHANGE,
	SGM_NEW, SGM_DELETE, SGM_CHANGE,
	BC_SGM_NEW, BC_SGM_DELETE, BC_SGM_CHANGE,	// SGM
	BC_TPL_NEW, BC_TPL_DELETE, BC_TPL_CHANGE,	// TPL
	TPL_NEW, TPL_DELETE, TPL_CHANGE,
	BC_TWY_NEW, BC_TWY_DELETE, BC_TWY_CHANGE,	// TWY
	TWY_NEW, TWY_DELETE, TWY_CHANGE,
	BC_VAL_NEW, BC_VAL_DELETE, BC_VAL_CHANGE,	// VAL
	VAL_NEW, VAL_DELETE, VAL_CHANGE,
	BC_WRO_NEW, BC_WRO_DELETE, BC_WRO_CHANGE,	// WRO
	WRO_NEW, WRO_DELETE, WRO_CHANGE,
	//---------------------------------------------------

	RUD_CLICK, RUD_CON_CHANGE,
	RUD_TMP_NEW, RUD_TMP_CHANGE, RUD_TMP_DELETE, // RUD_TMP (Logical RUD Object)
	RPF_TMP_NEW, RPF_TMP_CHANGE, RPF_TMP_DELETE, // RPF_TMP (Logical RPF Object)
	RPQ_TMP_NEW, RPQ_TMP_CHANGE, RPQ_TMP_DELETE, // RPQ_TMP (Logical RPQ Object)
	RLO_TMP_NEW, RLO_TMP_CHANGE, RLO_TMP_DELETE, // RLO_TMP (Logical RLO Object)
	REQ_TMP_NEW, REQ_TMP_CHANGE, REQ_TMP_DELETE, // RLO_TMP (Logical REQ Object)

	APP_EXIT,
	DATA_RELOAD,
	TABLE_FONT_CHANGED,
    UNDO_CHANGE, APP_LOCKED, APP_UNLOCK,
    BC_CFG_INSERT, BC_CFG_CHANGE, CFG_CHANGE, CFG_INSERT,CFG_DELETE,
	KONF_CHANGE,KONF_DELETE,KONF_INSERT,

    // from here, there are defines for your project
	S_FLIGHT_CHANGE, S_FLIGHT_DELETE, S_FLIGHT_UPDATE,
	S_FLIGHT_INSERT,
	D_FLIGHT_CHANGE, D_FLIGHT_DELETE, D_FLIGHT_UPDATE,
	D_FLIGHT_INSERT,
	CCA_FLIGHT_CHANGE, CCA_FLIGHT_DELETE, CCA_FLIGHT_UPDATE,
	CCA_FLIGHT_INSERT,
	R_DLG_FLIGHT_CHANGE,
	R_DLG_ROTATION_CHANGE,
	RG_DLG_FLIGHT_CHANGE,
	RG_DLG_ROTATION_CHANGE,
	S_DLG_FLIGHT_CHANGE,
	S_DLG_ROTATION_CHANGE,
	S_DLG_FLIGHT_DELETE,
	R_FLIGHT_CHANGE, R_FLIGHT_DELETE,
	C_FLIGHT_CHANGE, C_FLIGHT_DELETE,C_FLIGHT_UPDATE,
	BC_FLIGHT_CHANGE, BC_FLIGHT_DELETE,
	S_DLG_GET_NEXT,
	S_DLG_GET_PREV,
	DIACCA_CHANGE,DIACCA_SELECT,CCA_KKEY_CHANGE,
	BLK_CHANGE,
	BCD_CIC_CHANGE,
	BCD_WRO_CHANGE,
	BCD_PST_CHANGE,
	BCD_BLT_CHANGE,
	BCD_GAT_CHANGE,
	BC_CCA_CHANGE, BC_CCA_NEW, BC_CCA_DELETE,
	CCA_NEW, CCA_DELETE, CCA_CHANGE,
	BC_INF_NEW,BC_INF_DELETE,BC_INF_CHANGE,INF_NEW,INF_DELETE,INF_CHANGE,
	MARK_BAR,
	REDISPLAY_ALL, STAFFDIAGRAM_UPDATETIMEBAND,
	SHOW_FLIGHT
};



//----------------------------------
//			structs
//----------------------------------
struct REFTIME
{
	UINT		ID;
	CString		Name;
	CString		Field;
};

struct REFTABLE
{
	CString			RTAB;	// reference table
	CStringArray	Fields;	// fields in reference table
	bool			bLoad;	// indicate if table should be loaded

	REFTABLE (void)
	{
		bLoad = true;
	}
};

struct FONT_INDEXES
{
	int VerticalScale;
	int Chart;
	FONT_INDEXES(void)
	{VerticalScale=0;Chart=0;}
};

// this is the struct for the coicelists in the main window grids
typedef struct ChoiceList
{
	CString		TabFieldName;	// reference (Base) field (Format: TAB.NAME)
	CString		ValueList;		// list of values
	CString		RefTabField;	// reference field (Format: TAB.Feld)
	CString     ToolTipField;	// feldname f�r Beschreibung der einzelnen items
} CHOICE_LISTS;

// this is the struct for the choicelist in DetailDlg resp. FlightIndepView
typedef struct ResChoiceList
{
	CString		Table;			// reference table, e.g. "PFC"
	CString		SubType;		// type of equipment (URNO aus EQTTAB
	CString		Field;			// reference field, e.g. "FCTC"
	CString		ValueList;		// items to be contained in combobox list
} RES_CHOICELIST;

typedef struct UpdatedItem
{
	long	Urno;
	int		Status;		// 0 = deleted, 1 = updated, 2 = new

	UpdatedItem(void)
	{
		Urno = -1;
		Status = -1;
	}
} UPD_ITEM;


typedef struct ItemsToSave
{
	char					Table[3];
	CCSPtrArray <UPD_ITEM>  *pItems;

	ItemsToSave(void)
	{
		Table[0] = '\0';
	}
} TAB_ITEM;


typedef CArray<int,int> WIDTHS;

//----------------------------------
//			global functions
//----------------------------------
CString GetResString(UINT ID);
CString GetString(UINT ipIDS);
bool GetString (CString opRefField, CString opIDS, CString &ropText);


void RudTmp_Dump();
void InitFont();
void DeleteBrushes();
void CreateBrushes();
CString GetDBString(CString ID);
bool SetDlgItemLangText ( CWnd *popWnd, UINT ipIDC, UINT ipIDS ) ;
bool SetWindowLangText ( CWnd *popWnd, UINT ipIDS );
bool SetDlgItemLangText ( CWnd *popWnd, UINT ilIDC, CString opRefField, 
						  CString opIDS );
bool SetWindowLangText ( CWnd *popWnd, CString opRefField, CString opIDS );
bool EnableDlgItem ( CWnd *popWnd, UINT ipIDC, BOOL bpEnable, BOOL bpShow );
bool ClearDlgItem ( CWnd *popWnd, UINT ipIDC);
BOOL MoveDlgItem ( CWnd *popWnd, UINT ipIDC, int x, int y );
BOOL ResizeDlgItem ( CWnd *popWnd, UINT ipIDC, int cx, int cy );
bool AddItemToUpdList(CString opUrno, CString opTable, int ipStatus);
bool SaveWindowPosition ( CWnd *popWnd, char *pcpIniEntry );
bool IniWindowPosition ( CWnd *popWnd, char *pcpIniEntry );
bool GetPrivProfString ( LPCTSTR pcpSection, LPCTSTR pcpEntry, LPCTSTR pcpDefault, 
					     LPTSTR pcpValue, DWORD ipSize, LPCTSTR pcpFileName );

//----------------------------------
//		external definitions
//----------------------------------

//--- field indices
// ALO (allocation units)
extern igAloAlocIdx;
extern igAloAlodIdx;
extern igAloAlotIdx;
extern igAloReftIdx;
extern igAloUrnoIdx;
// EQT (equipment types)
extern igEqtNameIdx;
extern igEqtUrnoIdx;
// EQU (equipment numbers)
extern igEquEnamIdx;
extern igEquEtypIdx;
extern igEquGcdeIdx;
extern igEquGkeyIdx;
extern igEquUrnoIdx;
// DGR (dynamic groups)
extern igDgrExprIdx;
extern igDgrGnamIdx;
extern igDgrReftIdx;
extern igDgrUrnoIdx;
extern igDgrUtplIdx;
// PFC (Funktionen)
extern igPfcDptcIdx;
extern igPfcFctcIdx;
extern igPfcFctnIdx;
extern igPfcPrflIdx;
extern igPfcRemaIdx;
extern igPfcUrnoIdx;
// RPF (rules personnel functions)
extern igRpfFccoIdx;
extern igRpfFcnoIdx;
extern igRpfPrioIdx;
extern igRpfUpfcIdx;
extern igRpfUrnoIdx;
extern igRpfUrudIdx;
extern igRpfGtabIdx;	//  hag990826
// RPQ (rules personnel qualifications)
extern igRpqPrioIdx;
extern igRpqQucoIdx;
extern igRpqQunoIdx;
extern igRpqUdgrIdx;
extern igRpqUperIdx;
extern igRpqUrnoIdx;
extern igRpqUrudIdx;
extern igRpqGtabIdx;	//  hag990826
// REQ (rules equipment)
extern igReqEqcoIdx;
extern igReqEqnoIdx;
extern igReqEtypIdx;
extern igReqPrioIdx;
extern igReqUequIdx;
extern igReqUrnoIdx;
extern igReqUrudIdx;
extern igReqGtabIdx;	//  hag990826
// RLO (rules locations)
extern igRloRlocIdx;
extern igRloLonoIdx;
extern igRloPrioIdx;
extern igRloReftIdx;
extern igRloUrnoIdx;
extern igRloUrudIdx;
extern igRloGtabIdx;	//  hag990826
// RUD (Rule Demands)
extern igRudAlocIdx;
extern igRudDbflIdx;
extern igRudDeflIdx;
extern igRudCoduIdx;
extern igRudDbarIdx;
extern igRudDearIdx;
extern igRudDebeIdx;
extern igRudDecoIdx;
extern igRudDeduIdx;
extern igRudDeenIdx;
extern igRudDideIdx;
extern igRudDindIdx;
extern igRudDispIdx;
extern igRudDrtyIdx;
extern igRudEadbIdx;
extern igRudEdemIdx;
extern igRudFaddIdx;
extern igRudFcndIdx;
extern igRudFfpdIdx;
extern igRudFncdIdx;
extern igRudFondIdx;
extern igRudFsadIdx;
extern igRudLadeIdx;
extern igRudLifrIdx;
extern igRudLireIdx;
extern igRudLitoIdx;


extern igRudLodeIdx;
extern igRudMaxcIdx;
extern igRudMaxdIdx;
extern igRudMincIdx;
extern igRudMindIdx;
extern igRudNresIdx;
extern igRudRedeIdx;
extern igRudRemaIdx; 
extern igRudRendIdx;
extern igRudRepdIdx;
extern igRudRetyIdx;
extern igRudRtdbIdx; 
extern igRudRtdeIdx; 
extern igRudSdtiIdx;
extern igRudSutiIdx;
extern igRudStdeIdx; 
extern igRudTsdbIdx; 
extern igRudTsdeIdx; 
extern igRudTsndIdx; 
extern igRudTspdIdx; 
extern igRudTtgfIdx;
extern igRudTtgtIdx;
extern igRudUcruIdx; 
extern igRudUdemIdx; 
extern igRudUdgrIdx;
extern igRudUghcIdx;
extern igRudUghsIdx;
extern igRudUlnkIdx;
extern igRudUndeIdx; 
extern igRudUpdeIdx;
extern igRudUproIdx;  
extern igRudUrnoIdx;
extern igRudUrueIdx;
extern igRudUsesIdx;
extern igRudReftIdx;
extern igRudVrefIdx;
extern igRudUtplIdx;
// RUE (Rule Events)
extern igRueApplIdx;
extern igRueCdatIdx;
extern igRueEvrmIdx;
extern igRueEvtaIdx;
extern igRueEvtdIdx;
extern igRueEvttIdx;
extern igRueEvtyIdx;
extern igRueExclIdx;
extern igRueFisuIdx;
extern igRueFsplIdx;
extern igRueLstuIdx;
extern igRueMaxtIdx;
extern igRueMistIdx;
extern igRuePrioIdx;
extern igRueRemaIdx;
extern igRueRunaIdx;
extern igRueRusnIdx;
extern igRueRustIdx;
extern igRueRutyIdx;
extern igRueUarcIdx;
extern igRueUrnoIdx;
extern igRueUsecIdx;
extern igRueUseuIdx;
extern igRueUtplIdx;
// SEE (service catalogue personnel equipment)
extern igSeeEqcoIdx;
extern igSeeEqnoIdx;
extern igSeeEtypIdx;
extern igSeeGtabIdx;
extern igSeePrioIdx;
extern igSeeUequIdx;
extern igSeeUpfcIdx;
extern igSeeUrnoIdx;
extern igSeeUsrvIdx;
// SEF (service catalogue personnel functions)
extern igSefFccoIdx;
extern igSefFcnoIdx;
extern igSefFcnoIdx;
extern igSefGtabIdx;
extern igSefPrioIdx;
extern igSefUpfcIdx;
extern igSefUrnoIdx;
extern igSefUsrvIdx;
// SEL (service catalogue personnel locations)
extern igSelGtabIdx;
extern igSelLonoIdx;
extern igSelPrioIdx;
extern igSelReftIdx;
extern igSelRlocIdx;
extern igSelUrnoIdx;
extern igSelUsrvIdx;
// SEQ (service catalogue personnel qualifications)
extern igSeqGtabIdx;
extern igSeqPrioIdx;
extern igSeqQucoIdx;
extern igSeqQunoIdx;
extern igSeqUperIdx;
extern igSeqUrnoIdx;
extern igSeqUsrvIdx;
// SER (Static Groups)
extern igSerFdutIdx;
extern igSerFfisIdx;
extern igSerFisuIdx;
extern igSerFsdtIdx;
extern igSerFsetIdx;
extern igSerFsutIdx;
extern igSerFwtfIdx;
extern igSerFwttIdx;
extern igSerRtwfIdx;
extern igSerRtwtIdx;
extern igSerSdutIdx;
extern igSerSeaxIdx;
extern igSerSecoIdx;
extern igSerSeerIdx;
extern igSerSehcIdx;
extern igSerSeivIdx;
extern igSerSetyIdx;
extern igSerSnamIdx;
extern igSerSsapIdx;
extern igSerSsdtIdx;
//extern igSerSsnmIdx;
extern igSerSsutIdx;
extern igSerSwtfIdx;
extern igSerSwttIdx;
extern igSerUrnoIdx;
extern igSerVafrIdx;
extern igSerVatoIdx;
// SGR (Static Groups)
extern igSgrApplIdx;
extern igSgrFldnIdx;
extern igSgrGrdsIdx;
extern igSgrGrpnIdx;
extern igSgrGrsnIdx;
extern igSgrPrflIdx;
extern igSgrTabnIdx;
extern igSgrUrnoIdx;
extern igSgrUgtyIdx;
extern igSgrUtplIdx;
// SGM (Group Members)
extern igSgmPrflIdx;
extern igSgmTabnIdx;
extern igSgmUrnoIdx;
extern igSgmUsgrIdx;
extern igSgmUvalIdx;
extern igSgmValuIdx;
// TPL (Templates)
extern igTplApplIdx;
extern igTplDaloIdx;
extern igTplEtypIdx;
extern igTplEvorIdx;
extern igTplFisuIdx;
extern igTplFldaIdx;
extern igTplFlddIdx;
extern igTplFldtIdx;
extern igTplRelxIdx;
extern igTplTnamIdx;
extern igTplTpstIdx;
extern igTplUrnoIdx;
extern igTplFtyaIdx;
extern igTplFtydIdx;

// TSR (template sources)
extern igTsrBfldIdx;
extern igTsrBtabIdx;
extern igTsrExteIdx;
extern igTsrFireIdx;
extern igTsrFormIdx;
extern igTsrNameIdx;
extern igTsrOperIdx;
extern igTsrRfldIdx;
extern igTsrRtabIdx;
extern igTsrTextIdx;
extern igTsrTypeIdx;
extern igTsrUrnoIdx;
// VAL (Validity)
extern igValApplIdx;
extern igValFreqIdx;
extern igValTabnIdx;
extern igValTimfIdx;
extern igValTimtIdx;
extern igValUrnoIdx;
extern igValUvalIdx;
extern igValVafrIdx;
extern igValVatoIdx;
extern igValVtypIdx;


extern char SOH;
extern char STX;
extern char ETX;

//--- brushes
/*extern CBrush *pogGatBrush;
extern CBrush *pogPosBrush;
extern CBrush *pogBltBrush;
extern CBrush *pogCcaBrush;
extern CBrush *pogWroBrush;*/

//--- enums
extern enum enumRecordState egRecordState;

//--- fixed fonts
extern CFont ogTimesNewRoman_9;
extern CFont ogTimesNewRoman_12;
extern CFont ogTimesNewRoman_16;
extern CFont ogTimesNewRoman_30;
extern CFont ogSmallFonts_Regular_6;
extern CFont ogSmallFonts_Regular_7;
extern CFont ogSmallFonts_Regular_8;
extern CFont ogSmallFonts_Bold_7;
extern CFont ogMSSansSerif_Regular_8;
extern CFont ogMSSansSerif_Bold_8;
extern CFont ogMSSansSerif_Bold_7;
extern CFont ogCourier_Regular_8;
extern CFont ogCourier_Regular_9;
extern CFont ogCourier_Regular_10;
extern CFont ogCourier_Bold_8;
extern CFont ogCourier_Bold_10;


extern CFont ogSetupFont;

//--- scalable fonts
extern CFont ogScalingFonts[30];
extern int igFontIndex1;
extern int igFontIndex2;

//--- color and brush arrays
extern COLORREF ogColors[];
extern CBrush *ogBrushs[];
extern CBrush ogHatchBrushTurn, ogHatchBrushIn, ogHatchBrushOut;

//--- colorrefs
extern COLORREF lgBkColor;
extern COLORREF lgTextColor;
extern COLORREF lgHilightColor;

//--- class objects
extern CInitialLoadDlg *pogInitialLoad;
extern CCSBcHandle ogBcHandle;
extern CCSCedaCom ogCommHandler;  // The one and only CedaCom object
extern CCSDdx ogDdx;
extern CCSLog ogLog;
extern CedaBasicData ogBCD;
extern CCSBasic ogCCSBasic;
extern CBasicData ogBasicData;
extern PrivList ogPrivList;
extern DataSet ogDataSet;
extern ChangeInfo ogChangeInfo;

//--- other application globals
extern CStringArray ogSelectedRuds; 
extern CStringArray ogCmdLineArgsArray;

extern CCSPtrArray <REFTIME> ogInboundReferences;
extern CCSPtrArray <REFTIME> ogOutboundReferences;

extern CCSPtrArray <CHOICE_LISTS> ogChoiceLists;
extern CCSPtrArray <RES_CHOICELIST> ogResChoiceLists;
extern CCSPtrArray <REFTABLE> ogRefTables;

extern CCSPtrArray <UPD_ITEM> ogRudItems;
extern CCSPtrArray <UPD_ITEM> ogRpfItems;
extern CCSPtrArray <UPD_ITEM> ogRpqItems;
extern CCSPtrArray <UPD_ITEM> ogRloItems;
extern CCSPtrArray <UPD_ITEM> ogReqItems;
extern CMapStringToPtr ogUpdItems;

extern CString ogAppName;
extern CString ogCallingApp;
extern char pcgUser[33];
extern char pcgPasswd[33];
extern char pcgConfigPath[142];
extern char pcgHome[4];
extern char pcgTableExt[10];


extern bool ccicheck;
extern bool bgMaxGroundTimeIsMin;
extern bool bgAdaptScale;
extern bool bgDebug;
extern bool bgUseValidityType;
extern bool bgNoScroll;
extern bool bgOnline;
extern bool bgIsButtonListMovable;
extern bool bgIsServicesOpen;
extern bool bgSeasonLocal;
extern bool bgUseResourceStrings;
extern bool bgIsValSaved;
extern BOOL bgIsInitialized;
extern CGUILng* ogGUILng ;
//extern const char pcgIniFile[_MAX_PATH+1];
extern const CString ogIniFile;

extern char pcgLogFilePath[256];
extern CString oglogData;
extern bool bgFirstLog;	
//----------------------------------
//			radio button states
//----------------------------------
#define CHECKED		1
#define UNCHECKED	0


//----------------------------------
//			main view grid cols
//----------------------------------
#define SIMPLE_VAL	2
#define STATIC_GRP	3
#define DYNAMIC_GRP	4


//----------------------------------
//			resource tables update status
//----------------------------------
#define ITEM_DEL	0	// item deleted
#define ITEM_UPD	1	// item updated
#define ITEM_NEW    2	// new item (to be inserted)


//----------------------------------
//			rule change states
//----------------------------------
#define RULE_CHECK	0	// status unknown (check for changes)
#define RULE_NEW	1	// rule new (check for changes)
#define RULE_COPY   2	// rule copied (user must be asked to save)
#define RULE_DELETE 3   // rule deleted (do not ask user)


//----------------------------------
//			error handling
//----------------------------------
/*		PRF5468: no longer using CritErr.txt
extern ofstream of_catch;
*/

//#ifndef _DEBUG
#define  CCS_TRY try{
#define  CCS_CATCH_ALL }\
						catch(...)\
						{\
						    char pclExcText[512]="";\
						    char pclText[1024]="";\
						    sprintf(pclText, GetString(136),\
							                 __FILE__, __LINE__);\
						    strcat(pclText, GetString(137));\
						    sprintf(pclExcText, GetString(138), __FILE__, __LINE__);\
							CString tmplogdata;\
							tmplogdata.Format(pclText);\
							oglogData += tmplogdata;\
							WriteInlogFile();\
						}

//#else
//#define  CCS_TRY 
//#define  CCS_CATCH_ALL 
//#endif

//----------------------------------
//			BDPS-SEC macros
//----------------------------------
#define SetWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat=='-') plWnd.ShowWindow(SW_HIDE);

#define SetWndStatPrio_1(clStat1,clStat2, plWnd)\
	if(clStat1=='-'||clStat2=='-') plWnd.ShowWindow(SW_HIDE);\
	else if(clStat1=='0'||clStat2=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat1=='1'||clStat2=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}

#define SetpWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd->ShowWindow(SW_SHOW);plWnd->EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd->ShowWindow(SW_SHOW);plWnd->EnableWindow(FALSE);}\
	else if(clStat=='-') plWnd->ShowWindow(SW_HIDE);


// end globals
/////////////////////////////////////////////////////////////////////////////


#endif //__CCSGLOBL_H__
#endif // !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
