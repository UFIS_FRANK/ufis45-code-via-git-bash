#if !defined(AFX_INFODLG_H__8E682523_038C_11D3_A61C_0000C007916B__INCLUDED_)
#define AFX_INFODLG_H__8E682523_038C_11D3_A61C_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InfoDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// InfoDlg dialog

class InfoDlg : public CDialog
{
// Construction
public:
	InfoDlg(CString opRueUrno, CWnd* pParent = NULL);   // standard constructor
	~InfoDlg();

// Dialog Data
	//{{AFX_DATA(InfoDlg)
	enum { IDD = IDD_INFO_DLG };
	CStatic	m_CdatEdit;
	CStatic	m_LstuEdit;
	CStatic	m_UseuEdit;
	CStatic	m_UsecEdit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(InfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	
	void Create();
	void SetStaticTexts();
	// Generated message map functions
	//{{AFX_MSG(InfoDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Attributes
CWnd *pomParent;
CString omCurrRueUrno;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INFODLG_H__8E682523_038C_11D3_A61C_0000C007916B__INCLUDED_)
