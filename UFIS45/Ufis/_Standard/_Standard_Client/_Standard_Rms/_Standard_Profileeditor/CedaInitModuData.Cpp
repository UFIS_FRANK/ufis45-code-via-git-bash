// CedaInitModuData.cpp
 
#include <stdafx.h>
#include <resource.h>
#include <CedaInitModuData.h>


const igNoOfPackets = 10;
CedaInitModuData ogInitModuData;


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaInitModuData::CedaInitModuData()
{
	strcpy(pcmInitModuFieldList,"APPL,SUBD,FUNC,FUAL,TYPE,STAT");
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaInitModuData::~CedaInitModuData(void)
{
}

//--SEND INIT MODULS-------------------------------------------------------------------------------------------

bool CedaInitModuData::SendInitModu()
{
	bool blRc = true;

	//-- exception handling
	CCS_TRY

	//INITMODU//////////////////////////////////////////////////////////////

	CString olInitModuData = GetInitModuTxt();
	
	int ilCount = olInitModuData.GetLength();

	char *pclInitModuData = new char[olInitModuData.GetLength()+3];
	strcpy(pclInitModuData,olInitModuData);

	int ilCount2 = strlen(pclInitModuData);

	char pclTableExt[65];
	strcpy(pclTableExt, pcgTableExt);
	strcat(pclTableExt, " ");


	blRc = CedaAction("SMI",pclTableExt,pcmInitModuFieldList,"","",pclInitModuData);
	if(blRc==false)
	{
		AfxMessageBox(CString("SendInitModu:") + omLastErrorMessage,MB_ICONEXCLAMATION);
	}

	delete pclInitModuData;
	
	//-- exception handling
	CCS_CATCH_ALL

	return blRc;
}


//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxt() 
{
	CString olInitModuData = ogAppName + ",InitModu,InitModu,Initialisieren (InitModu),B,-,";
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ButtonList			
	olInitModuData += GetInitModuTxtG();
	return olInitModuData;
}
//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxtG() 
{
	CString olStr="";

	CCS_TRY

	// Build register string for BDPS-SEC	
	//		Attention: You can't use '+=' too often with CString. When reaching a certain number
	//				   you get an error message. In that case you can do the following: 
	//				   Build several strings up to the max length before causing the error.	
	//				   Afterwards you can concatenate those strings (It'll work, I don't know why).
	CString olResStr;
	CString olDistribStr;

	olDistribStr = GetString ( IDS_DISTRIBUTION );
	olResStr = GetString ( IDS_COMMON );
	olStr += olDistribStr + ",IDC_DCOP_EDIT," + olResStr + ",E,1,";

	olResStr = GetString ( IDS_CURB );
	olStr += olDistribStr + ",IDC_DCUP_EDIT," + olResStr + ",E,1,";

	olResStr = GetString( IDS_GATE );
	olStr += olDistribStr + ",IDC_DGAP_EDIT," + olResStr + ",E,1,";

	olResStr = GetString ( IDS_SELF );
	olStr += olDistribStr + ",IDC_DSEP_EDIT," + olResStr + ",E,1,";

	olResStr = GetString ( IDS_TOTAL );
	olStr += olDistribStr + ",IDC_TOTAL_EDIT," + olResStr + ",E,1";
	CCS_CATCH_ALL

	return olStr;
}
