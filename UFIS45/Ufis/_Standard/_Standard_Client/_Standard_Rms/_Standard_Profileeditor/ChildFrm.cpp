// ChildFrm.cpp : implementation of the CChildFrame class
//

#include <stdafx.h>
#include <afxpriv.h>
#include <ProfileEditor.h>

#include <ChildFrm.h>
#include <ProfileChart.h>
#include <ProfileEditorView.h>
#include <ProfileEditorDoc.h>
#include <utilities.h>
#include <mainfrm.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CChildFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_VALIDITY, OnValidity)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildFrame construction/destruction

CChildFrame::CChildFrame()
{
	// TODO: add member initialization code here
}

CChildFrame::~CChildFrame()
{
/*	for ( int i=omCharts.GetSize()-1; i>=0; i-- )
	{
		CProfileChart *pomChart = (CProfileChart*)omCharts[i];
		if ( pomChart )
			delete pomChart;
	}
	*/
}

BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CChildFrame diagnostics

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CChildFrame message handlers

int CChildFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}


BOOL CChildFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	omMainSplitter.CreateStatic(this, 2,1);   
	CRect olRect;
	GetClientRect(&olRect);
	int ilHeight = (int)((olRect.bottom - olRect.top)/2);
	CSize olSizeMin (50,20);


	if (!omMainSplitter.CreateView(0, 0, RUNTIME_CLASS(CProfileEditorView), CSize(920,250), pContext))
	{
		TRACE0("Failed to create CGridView pane\n");
		return FALSE;
	}
	omMainSplitter.CreateView(1, 0, RUNTIME_CLASS(CProfileChart), CSize(920,200), pContext);

	return TRUE;  // CMDIChildWnd::OnCreateClient(lpcs, pContext);
}


void CChildFrame::OnValidity() 
{
	// TODO: Add your command handler code here
	CProfileEditorDoc* polDoc = (CProfileEditorDoc*)GetActiveDocument( );

	if ( polDoc )
	{
		bool blNew=(polDoc->omUrno.IsEmpty()==TRUE);
		if ( DoValidityDlg ( this, polDoc->rmValData, blNew ) == IDOK )
			polDoc->SetModified ( VAL_CHANGED );
	}
	
}

void CChildFrame::OnEditCut() 
{
	// TODO: Add your command handler code here
	CProfileEditorDoc* polDoc = (CProfileEditorDoc*)GetActiveDocument( );

	if ( polDoc )
	{/*
		CString olText = "Do you really want to delete profile %s ?";
		olText.Replace ( "%s", polDoc->GetTitle () );
		int ilReturn = AfxMessageBox ( olText, MB_ICONQUESTION|MB_YESNO );*/
		int ilReturn = FileErrMsg ( m_hWnd, IDS_ASK_DEL_PROFILE, 
									pCHAR(polDoc->GetTitle()), 0, 
									MB_ICONQUESTION|MB_YESNO );
		if ( ilReturn == IDNO )		//  Doch nicht l�schen
			return ;

		if ( polDoc->DeleteFromDB () && theApp.IsDocumentPointer ( polDoc ) && polDoc->bmValid )
			polDoc->OnCloseDocument();
	}	
}

void CChildFrame::OnAppAbout() 
{
	// TODO: Add your command handler code here
	CProfileEditorDoc* polDoc = (CProfileEditorDoc*)GetActiveDocument( );
	CAboutDlg aboutDlg;
	aboutDlg.DoModal(polDoc );

}


void CChildFrame::OnUpdateFrameMenu(BOOL bActivate, CWnd* pActivateWnd,
									HMENU hMenuAlt)
{
	CMDIChildWnd::OnUpdateFrameMenu(bActivate, pActivateWnd, hMenuAlt);
/*
	CMainFrame *pFrm = (CMainFrame*)GetParentFrame();
	CMenu		*polMenu=0;
	UINT		ilItems;

	if ( pFrm )
	{
		polMenu = pFrm->GetMenu ();
		if ( polMenu )
			ilItems = polMenu->GetMenuItemCount ();
		if ( ilItems > 0 )
			pFrm->TranslateMenu();
	}
	if ( m_hMenuDefault )
		ilItems = GetMenuItemCount ( m_hMenuDefault );
		*/
}

void CChildFrame::GetMessageString(UINT nID, CString& rMessage) const
{
	if ( (nID>=ID_FILE_MRU_FILE1) && (nID<=ID_FILE_MRU_FILE16) )
		nID = ID_FILE_MRU_FILE1;
	rMessage = GetString(nID);
	// first newline terminates actual string
	rMessage.Replace ( '\n', '\0' );

}
