#if !defined(AFX_HISTOGRAMM_H__3E54BDC0_D32F_11D3_93D0_00001C033B5D__INCLUDED_)
#define AFX_HISTOGRAMM_H__3E54BDC0_D32F_11D3_93D0_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Histogramm.h : header file
//

#include <histocolumn.h>
#include <CCSPtrArray.h>

class CProfileChart;
class CChartData;

/////////////////////////////////////////////////////////////////////////////
// CHistogramm window

class CHistogramm : public CWnd
{
// Construction
public:
	CHistogramm();

// Attributes
public:
	CCSPtrArray<CHistoColumn>	omColumns;
	float		fmUnitsX;
	float		fmUnitsY;
	CRect		omUpdateArea;  // = ClientArea + Scrollbar
	CRect		omClientArea;
	CRect		omViewPort;
	CPoint		omRightBottom;
	CWnd		*pomParent;
	int			imColumnWidth;	//  Breite der S�ulen in Minuten = CChartData::imDspStep
//	COLORREF	ilBkColor;
//	COLORREF	ilRegularColColor;
//	COLORREF	ilMarkedColColor;
	float		fmScaleStepY;
	int			imNksY;
	int			imFirstXVal, imLastXVal;

// Operations
public:
	BOOL Open ( CWnd *popChart, UINT idc );
	void DisplayData ( CChartData *popData, bool bpNeu=true );
	int GetXClient ( int ipMinute );
	int GetYClient ( float fpPercent );
	//void InvalidateWindow();

protected:
	void CalcScaleFaktor ( CChartData *popData );
	void DrawXAxis (CDC* pDC);
	void DrawYAxis (CDC* pDC);
	void IniScrollBar( CChartData *popData, bool bpReset=false ) ;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHistogramm)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CHistogramm();

	// Generated message map functions
protected:
	//{{AFX_MSG(CHistogramm)
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

float FindScaleStep ( float fpRange, int ipSteps, int &ipNks );
float FindScaleStep4Value ( float fpValue, float fpMuliplyBy, int &ipNks );
int Rnd ( float fpVal );
float Rnd ( float fpVal, int nks );

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HISTOGRAMM_H__3E54BDC0_D32F_11D3_93D0_00001C033B5D__INCLUDED_)
