// RecordInfoDlg.cpp : implementation file
//

#include <stdafx.h>

#include <RecordSet.h>
#include <cedabasicdata.h>

#include <Resource.h>
#include <ccsglobl.h>
#include <utilities.h>

#include <RecordInfoDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRecordInfoDlg dialog


CRecordInfoDlg::CRecordInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRecordInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRecordInfoDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomTitle = 0;
	pomAktRecord = 0;
}


void CRecordInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRecordInfoDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRecordInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CRecordInfoDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRecordInfoDlg message handlers

BOOL CRecordInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	IniStatics ();
	SetInfoFields ();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CRecordInfoDlg::DoModal(RecordSet *popRecord, CString opTable, 
							CString *popTitle/*=0*/) 
{
	// TODO: Add your specialized code here and/or call the base class
	pomAktRecord = popRecord;
	omTable = opTable;
	pomTitle = popTitle;
	if ( pomAktRecord && !omTable.IsEmpty() )
		return CDialog::DoModal();
	else
		return IDCANCEL;
}

void CRecordInfoDlg::IniStatics ()
{
	SetDlgItemLangText ( this, IDOK, IDS_OK );
	SetDlgItemLangText ( this, IDC_CDAT_TXT, IDS_CREATED_ON );
	SetDlgItemLangText ( this, IDC_LSTU_TXT, IDS_CHANGED_ON );
	SetDlgItemLangText ( this, IDC_USEC_TXT, IDS_CREATED_BY );
	SetDlgItemLangText ( this, IDC_USEU_TXT, IDS_CHANGED_BY );
	SetDlgItemLangText ( this, IDC_CHANGED_GRP, IDS_LAST_CHANGE );
	if ( pomTitle )
		SetWindowText ( *pomTitle );
	else
		SetWindowLangText ( this, IDS_INFO_ON_RECORD );
}

void CRecordInfoDlg::SetInfoFields ()
{
	
	CTime olTime ;
	if ( !pomAktRecord )
		return;

	CString olWert ;
	int ilIdx = ogBCD.GetFieldIndex(omTable,"CDAT");
	if ( ilIdx>=0 )
	{
		olWert = pomAktRecord->Values[ilIdx];
		if ( !olWert.IsEmpty () )
		{
			olTime = DBStringToDateTime(olWert);
			SetDlgItemText ( IDC_CDAT_DATE, olTime.Format( "%d.%m.%Y" ) );
			SetDlgItemText ( IDC_CDAT_TIME, olTime.Format( "%H:%M" ) );
		}
	}

	ilIdx = ogBCD.GetFieldIndex(omTable,"LSTU");
	if ( ilIdx>=0 )
	{
		olWert = pomAktRecord->Values[ilIdx];
		if ( !olWert.IsEmpty () )
		{
			olTime = DBStringToDateTime(olWert);
			SetDlgItemText ( IDC_LSTU_DATE, olTime.Format( "%d.%m.%Y" ) );
			SetDlgItemText ( IDC_LSTU_TIME, olTime.Format( "%H:%M" ) );
		}
	}
	ilIdx = ogBCD.GetFieldIndex(omTable,"USEC");
	if ( ilIdx>=0 )
		SetDlgItemText ( IDC_USEC, pomAktRecord->Values[ilIdx] );
	ilIdx = ogBCD.GetFieldIndex(omTable,"USEU");
	if ( ilIdx>=0 )
	SetDlgItemText ( IDC_USEU, pomAktRecord->Values[ilIdx] );
}
