#if !defined(AFX_RECORDINFODLG_H__3AFE5F22_CF13_11D3_93CD_00001C033B5D__INCLUDED_)
#define AFX_RECORDINFODLG_H__3AFE5F22_CF13_11D3_93CD_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RecordInfoDlg.h : header file
//

class RecordSet;

/////////////////////////////////////////////////////////////////////////////
// CRecordInfoDlg dialog

class CRecordInfoDlg : public CDialog
{
// Construction
public:
	CRecordInfoDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRecordInfoDlg)
	enum { IDD = IDD_INFO_ON_RECORD };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	CString *pomTitle;
	CString omTable;
	RecordSet	*pomAktRecord;

	virtual int DoModal(RecordSet *popRecord, CString opTable, 
						CString *popTitle=0);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRecordInfoDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void IniStatics ();
	void SetInfoFields ();

	// Generated message map functions
	//{{AFX_MSG(CRecordInfoDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RECORDINFODLG_H__3AFE5F22_CF13_11D3_93CD_00001C033B5D__INCLUDED_)
