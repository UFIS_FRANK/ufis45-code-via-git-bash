// ConditionsGrid.h: interface for the CConditionsGrid class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONDITIONSGRID_H__25ECB414_C738_11D3_93C5_00001C033B5D__INCLUDED_)
#define AFX_CONDITIONSGRID_H__25ECB414_C738_11D3_93C5_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <gridcontrol.h>

#define SIMPLE_VAL	2
#define STATIC_GRP	3	//  vorher 5
#define DYNAMIC_GRP	4	//  vorher 3
#define PUSHBTN_GRP	5	//  vorher 4

class CConditionsGrid : public CGridControl  
{
public:
	CConditionsGrid( CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows );

	virtual ~CConditionsGrid();
	void IniLayoutForConditions ();
	void IniGridFromTplString ( CString &opTPLString );
	void DisplayInfo ( ROWCOL nRow, ROWCOL nCol, bool bpToolTip=false );
	void ResetValues();
	void ModifyChoiceList ( CString opTable, bool bpAdd, RecordSet *popData );
	CString ConstructPrioString();
	void SetToolTipForValue ( ROWCOL nRow, ROWCOL nCol, CString &ropValue );
protected:
	void OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol);
	BOOL OnEndEditing( ROWCOL nRow, ROWCOL nCol) ;
	
};

#endif // !defined(AFX_CONDITIONSGRID_H__25ECB414_C738_11D3_93C5_00001C033B5D__INCLUDED_)
