// OpenProfileDlg.cpp : implementation file
//

#include <stdafx.h>

#include <cedabasicdata.h>

#include <ccsglobl.h>
#include <ProfileEditor.h>
#include <OpenProfileDlg.h>
#include <gridcontrol.h>
#include <validitydlg.h>
#include <utilities.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COpenProfileDlg dialog


COpenProfileDlg::COpenProfileDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COpenProfileDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COpenProfileDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	imProfilesCount = 0;
	pomProfileList = 0;
	imLastWidth = 0;
	imLastHeight = 0;
}


void COpenProfileDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, pomDlgButtons[0]);
	DDX_Control(pDX, IDCANCEL, pomDlgButtons[1]);
	DDX_Control(pDX, ID_EDIT_FIND, pomDlgButtons[2]);
	//{{AFX_DATA_MAP(COpenProfileDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COpenProfileDlg, CDialog)
	//{{AFX_MSG_MAP(COpenProfileDlg)
	ON_BN_CLICKED(ID_EDIT_FIND, OnEditFind)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_LBUTTONDBLCLK, OnGridLbDblClicked)
	ON_MESSAGE(WM_GRID_LBUTTONCLICK, OnGridLbClicked)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COpenProfileDlg message handlers

void COpenProfileDlg::OnEditFind() 
{
	// TODO: Add your control notification handler code here
	if ( pomProfileList	)
		pomProfileList->OnShowFindReplaceDialog(TRUE);
	
}

void COpenProfileDlg::OnOK() 
{
	// TODO: Add extra validation here
	ROWCOL		lRow;
	CGXStyle	olStyle;
	DWORD		ilUrno;
	CRowColArray olSelRows;
	
	if ( pomProfileList->GetSelectedRows(olSelRows, FALSE, FALSE ) != 1 )
	{
		return;
	}
	lRow = olSelRows[0];
	pomProfileList->ComposeStyleRowCol( lRow,1, &olStyle );
	ilUrno = (DWORD)olStyle.GetItemDataPtr();

	if ( !ilUrno )
	{
		TRACE ("Urno=0 in SelectService f�r Zeile%d\n", lRow );
		return;
	}
	else
		omSelProfileUrno.Format( "%lu", ilUrno );

	PreDestroyWindow ();
	CDialog::OnOK();
}

void COpenProfileDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	PreDestroyWindow ();	
	CDialog::OnCancel();
}

void COpenProfileDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	if ( pomProfileList )
		delete pomProfileList;
	pomProfileList = 0;
	
}

void COpenProfileDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	OnCancel ();
}

BOOL COpenProfileDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	IniStatics ();
	imProfilesCount = ogBCD.GetDataCount("PRO");
	IniGrid ();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void COpenProfileDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	CRect olChildRect, olCliRect;
	int ilWidthDiff, ilHeightDiff;

	GetClientRect ( &olCliRect );
	if ( pomProfileList	)
	{
		pomProfileList->GetWindowRect ( &olChildRect );
		if ( imLastWidth && imLastHeight )
		{
			ilWidthDiff = olCliRect.Width () - imLastWidth;
			ilHeightDiff = olCliRect.Height () - imLastHeight;
			pomProfileList->SetWindowPos( 0, 0, 0, 
										  olChildRect.Width() + ilWidthDiff, 
										  olChildRect.Height () + ilHeightDiff, 
										  SWP_NOMOVE | SWP_NOZORDER );
			pomProfileList->Redraw ();

			for ( int i=0; i<3; i ++ )
			{
				pomDlgButtons[i].GetWindowRect ( &olChildRect );
				ScreenToClient ( &olChildRect );
				pomDlgButtons[i].SetWindowPos( 0, olChildRect.left, 
											   olChildRect.top + ilHeightDiff, 0, 0, 
											   SWP_NOSIZE | SWP_NOZORDER );
			}
		}

	}
	imLastWidth = olCliRect.Width ();
	imLastHeight = olCliRect.Height ();
	
}

void COpenProfileDlg::IniGrid ()
{
	RecordSet   olRecord;
	bool		blRead;
	CString		olWert, olCicc, olVato, olVafr, olUrno, olUccc;
	DWORD		ilUrno;
	CTime		olTime;
	CCSPtrArray<RecordSet> olClasses;
	int			ilUrnoIdx, ilNameIdx, ilRemaIdx, ilPxcUccc, ilPrstIdx;
	CGXStyle olStyle;
	
	pomProfileList = new CGridControl ( this, IDC_PROFILESLIST, 
										5, imProfilesCount );

	pomProfileList->SetValueRange(CGXRange(0,1), GetString(IDS_NAME) );
	pomProfileList->SetValueRange(CGXRange(0,2), GetString(IDS_REMARK) );
	pomProfileList->SetValueRange(CGXRange(0,3), GetString(IDS_CLASSES) );
	pomProfileList->SetValueRange(CGXRange(0,4), GetString(IDS_VALFROM) );
	pomProfileList->SetValueRange(CGXRange(0,5), GetString(IDS_VALTO) );
	 
	IniColWidths ();

	ilUrnoIdx = ogBCD.GetFieldIndex("PRO","URNO");
	ilNameIdx = ogBCD.GetFieldIndex("PRO","NAME");
	ilRemaIdx = ogBCD.GetFieldIndex("PRO","REMA");
	ilPrstIdx = ogBCD.GetFieldIndex("PRO","PRST");
	ilPxcUccc = ogBCD.GetFieldIndex("PXC","UCCC");
	ROWCOL ilRow=0;
	for ( int i=0; i<imProfilesCount; i++ )
	{
		VALDATA rlValData;

		blRead=ogBCD.GetRecord("PRO", i, olRecord);
		if ( olRecord.Values[ilPrstIdx]=="2" )	//  archiviertes Profil
			continue;
		if ( blRead )
		{
			ilRow ++;	// aktuelle Zeile

			olUrno=olRecord.Values[ilUrnoIdx];
			if ( !sscanf ( pCHAR(olUrno), "%ld", &ilUrno ) )
				continue;
			pomProfileList->SetStyleRange ( CGXRange(ilRow,1), 
								CGXStyle().SetItemDataPtr( (void*)ilUrno ) );

			olWert=olRecord.Values[ilNameIdx];
			pomProfileList->SetValueRange(CGXRange(ilRow,1), olWert );

			olWert=olRecord.Values[ilRemaIdx];
			pomProfileList->SetValueRange(CGXRange(ilRow,2), olWert );

			ogBCD.GetRecords ( "PXC", "UPRO", olUrno, &olClasses );
			olWert.Empty();
			for ( int j=0; j<olClasses.GetSize (); j++ )
			{
				olUccc = olClasses[j].Values[ilPxcUccc];
				if ( olUccc.IsEmpty () || 
					 !ogBCD.GetField("CCC", "URNO", olUccc, "CICC", olCicc ) )
					continue;
				if ( !olWert.IsEmpty() )
					olWert += " / ";
				olWert += olCicc;
			}
			olClasses.DeleteAll ();
			pomProfileList->SetValueRange(CGXRange(ilRow,3), olWert );
			
			if ( LoadValidity ( olUrno, rlValData ) )
			{
				olTime = DBStringToDateTime(CString(rlValData.Vafr));
				olVafr = olTime.Format ( "%d.%m.%Y %H:%M" );
				olTime = DBStringToDateTime(CString(rlValData.Vato));
				olVato = olTime.Format ( "%d.%m.%Y %H:%M" );
			}
			else
			{
				olVafr.Empty();
				olVato.Empty();
			}
			pomProfileList->SetValueRange(CGXRange(ilRow,4), olVafr );
			pomProfileList->SetValueRange(CGXRange(ilRow,5), olVato );
			if ( olRecord.Values[ilPrstIdx]!="1" )	//  inaktives Profil
			{
				olStyle.SetInterior(SILVER);
				pomProfileList->SetStyleRange(CGXRange().SetRows(ilRow), olStyle); 
			}
		}
	}
	ROWCOL   ilRowCount = pomProfileList->GetRowCount();
	if ( ilRowCount > ilRow )
		pomProfileList->RemoveRows ( ilRow+1, ilRowCount );
	ilRowCount = pomProfileList->GetRowCount();
	if ( ilRowCount > 0 )
	{
		CGXRange olRange;
		olRange.SetRows ( 1, ilRowCount );
		//  pomProfileList->SetStyleRange( olRange, CGXStyle().SetEnabled(FALSE) );
	}
	pomProfileList->SetStyleRange( CGXRange().SetTable(), CGXStyle()
									.SetVerticalAlignment(DT_VCENTER)
									.SetReadOnly(TRUE));
	pomProfileList->EnableAutoGrow ( FALSE );
	pomProfileList->GetParam()->EnableSelection ( GX_SELROW );
	pomProfileList->SortTable ( 0, 1 );

}
 

void COpenProfileDlg::IniColWidths ()
{
	CRect olRect = pomProfileList->GetGridRect( );
	pomProfileList->SetColWidth ( 0, 0, 20 );
	int cx = olRect.Width() - 5-20;

	pomProfileList->SetColWidth ( 1, 1, (int)((float)cx*0.15) );
	pomProfileList->SetColWidth ( 2, 2, (int)((float)cx*0.3) );
	pomProfileList->SetColWidth ( 3, 3, (int)((float)cx*0.15) );
	pomProfileList->SetColWidth ( 4, 5, (int)((float)cx*0.2) );
}


void COpenProfileDlg::PreDestroyWindow ()
{
	//SaveWindowPosition ( this, "ProfileListe" );
	//SaveColWidths ();
	if ( pomProfileList && pomProfileList->m_hWnd )
		pomProfileList->DestroyWindow ();
}	


void COpenProfileDlg::OnGridLbDblClicked ( WPARAM wparam, LPARAM lparam )
{
	if ( lparam )
	{
		OnGridLbClicked( wparam, lparam );
		OnOK();
	}
}

void COpenProfileDlg::OnGridLbClicked( WPARAM wparam, LPARAM lparam )
{
	GRIDNOTIFY rlNotify;
	if ( lparam )
	{
		rlNotify = *((GRIDNOTIFY*)lparam);
		if ( rlNotify.row>0)
			pomProfileList->SelectRange(CGXRange().SetRows(rlNotify.row), TRUE);
	}
}

void COpenProfileDlg::IniStatics()
{
	//  Set Static Texts in selected language
	SetDlgItemLangText ( this, ID_EDIT_FIND, IDS_SEARCH );
	SetDlgItemLangText ( this, IDOK, IDS_OK );
	SetDlgItemLangText ( this, IDCANCEL, IDS_CANCEL );
	
	SetWindowLangText ( this, IDS_ROFILE_LIST );

}
