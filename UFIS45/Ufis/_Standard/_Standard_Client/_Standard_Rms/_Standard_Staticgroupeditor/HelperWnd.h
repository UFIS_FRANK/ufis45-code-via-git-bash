#if !defined(AFX_HELPERWND_H__C134D523_8D0C_11D3_9391_00001C033B5D__INCLUDED_)
#define AFX_HELPERWND_H__C134D523_8D0C_11D3_9391_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HelperWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CHelperWnd window

class CHelperWnd : public CWnd
{
// Construction
public:
	CHelperWnd();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHelperWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CHelperWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CHelperWnd)
	afx_msg void OnActivateApp(BOOL bActive, HTASK hTask);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HELPERWND_H__C134D523_8D0C_11D3_9391_00001C033B5D__INCLUDED_)
