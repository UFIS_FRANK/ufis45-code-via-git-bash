// logindlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg dialog

#ifndef _CLOGINDLG
#define _CLOGINDLG

#include "resource.h"
#include "CCSEdit.h"

class CLoginDlg : public CDialog 
{
// Construction
public:
    CLoginDlg(CWnd* pParent = NULL);     // standard constructor

// Dialog Data
    //{{AFX_DATA(CLoginDlg)
	enum { IDD = IDD_LOGIN };
	CString	m_UserName;
	CString	m_PassWord;
	//}}AFX_DATA

	CCSEdit	m_UsernameCtrl;
	CCSEdit	m_PasswordCtrl;
// Implementation
protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    // Generated message map functions
    //{{AFX_MSG(CLoginDlg)
    afx_msg void OnPaint();
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	void OnAppAbout(); 
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};
#endif
