// ButtonListDlg.h : header file
//

#if !defined(AFX_ButtonListDlg_H__FC3BA6F9_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
#define AFX_ButtonListDlg_H__FC3BA6F9_1AED_11D1_82C3_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CAutoProxyDlg;

/////////////////////////////////////////////////////////////////////////////
// CButtonListDlg dialog

class CButtonListDlg : public CDialog
{
	DECLARE_DYNAMIC(CButtonListDlg);
	friend class CAutoProxyDlg;

// Construction
public:
	CButtonListDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CButtonListDlg();
	void MoveWindow( int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE );

// Dialog Data
public:
    int m_nDialogBarHeight;

// Dialog Data
	//{{AFX_DATA(CButtonListDlg)
	enum { IDD = IDD_BUTTONLIST };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CButtonListDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CAutoProxyDlg* m_pAutoProxy;
	HICON m_hIcon;

	BOOL CanExit();
	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 

	// Generated message map functions
	//{{AFX_MSG(CButtonListDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnBeenden();
	afx_msg void OnNcLButtonDown(UINT nHitTest, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ButtonListDlg_H__FC3BA6F9_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
