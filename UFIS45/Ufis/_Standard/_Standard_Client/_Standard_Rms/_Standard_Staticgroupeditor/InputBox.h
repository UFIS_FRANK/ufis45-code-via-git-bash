#if !defined(AFX_INPUTBOX_H__A47AED54_2312_11D3_97E6_00001C0411B3__INCLUDED_)
#define AFX_INPUTBOX_H__A47AED54_2312_11D3_97E6_00001C0411B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InputBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInputBox dialog

class CInputBox : public CDialog
{
// Construction
public:
	CString GetInput();
	CString m_Title;
	CString m_Text;
	CString m_Default;
	long	m_Length;
	int Set(CString cTitle, CString cText, CString cDefault, long iLen);
	CInputBox(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CInputBox)
	enum { IDD = IDD_INPUTBOX };
	CEdit	m_cInput;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInputBox)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	void OnOK();
	void OnCancel();
	bool NameAllowed ( CString opText );

	// Generated message map functions
	//{{AFX_MSG(CInputBox)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INPUTBOX_H__A47AED54_2312_11D3_97E6_00001C0411B3__INCLUDED_)
