// CedaEmpData.cpp - Class for handling employee data
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <ccsobj.h>
#include <CCSCedaData.h>
#include <CedaEmpData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <resource.h>
#include <BasicData.h>
#include <NameConfig.h>
#include <PrmConfig.h>

CedaEmpData::CedaEmpData()
{                  
    // Create an array of CEDARECINFO for STAFFDATA
    BEGIN_CEDARECINFO(EMPDATA, EmpDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_CHAR_TRIM(Peno,"PENO")
        FIELD_CHAR_TRIM(Lanm,"LANM")
        FIELD_CHAR_TRIM(Finm,"FINM")
        FIELD_DATE(Doem,"DOEM")
        FIELD_CHAR_TRIM(Telp,"TELP")
        FIELD_CHAR_TRIM(Telh,"TELH")
        FIELD_CHAR_TRIM(Teld,"TELD")
        FIELD_CHAR_TRIM(Perc,"PERC")
        FIELD_CHAR_TRIM(Shnm,"SHNM")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(EmpDataRecInfo)/sizeof(EmpDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&EmpDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
//	strcpy(pcmTableName,"empcki");
	strcpy(pcmTableName,"STFTAB");
//	pcmFieldList = "Peno,disp,lnam,fnam,sxid,bdat,edat,rdat,mari,adr1,adr2,adr3,adr4,tel1,tel2,chld,bdc1,bdc2,bdc3";
	pcmFieldList = "URNO,PENO,LANM,FINM,DOEM,TELP,TELH,TELD,PERC,SHNM";

	ogCCSDdx.Register((void *)this,BC_EMP_CHANGE,CString("BC_EMPCHANGE"), CString("EmpDataChange"),ProcessEmpCf);

    omPenoMap.InitHashTable(629,117);
    omData.SetSize(0,600);

}

CString CedaEmpData::Dump(long lpUrno)
{
	CString olDumpStr;
	EMPDATA *prlEmp = GetEmpByUrno(lpUrno);
	if(prlEmp == NULL)
	{
		olDumpStr.Format("No EMPDATA Found for EMP.URNO <%ld>",lpUrno);
	}
	else
	{
		CString olData;
		MakeCedaData(&omRecInfo,olData,prlEmp);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();
		if(ilNumFields != ilNumData)
		{
			olDumpStr.Format("<%s>\n<%s>\n",pcmFieldList,olData);
		}
		else
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + ">    ";
			}
		}
	}
	return olDumpStr;
}

CedaEmpData::~CedaEmpData()
{
	TRACE("CedaEmpData::~CedaEmpData called\n");
	ogCCSDdx.UnRegister(this,NOTUSED);
	ClearAll();
}

void CedaEmpData::ClearAll()
{
    omUrnoMap.RemoveAll();
    omPenoMap.RemoveAll();
	omData.DeleteAll();
}

void CedaEmpData::Add(EMPDATA *prpEmp)
{
	omData.Add(prpEmp);
	omUrnoMap.SetAt((void *)prpEmp->Urno,prpEmp);
	omPenoMap.SetAt(prpEmp->Peno,prpEmp);
}

bool CedaEmpData::ReadEmpData(void)
{
	bool blRc = true;
	int ilEmpCount = 0;

	char pclWhere[200] = "";
	char pclCom[10] = "RT";

	if (bgIsPrm)
	{
		char clPrmSelection[132];
		
		if (ogBasicData.IsPrmHandlingAgentEnabled())
		{
			sprintf(clPrmSelection," AND FILT = '%s' ",ogPrmConfig.getHandlingAgentShortName());
			strcat(cgSelection,clPrmSelection);
		}
	}


    if((blRc = CedaAction2(pclCom,pclWhere)) != true)
	{
		ogBasicData.LogCedaError("CedaEmpData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{

		ClearAll();
		for (int ilLc = 0; blRc; ilLc++)
		{
			EMPDATA *prlEmp = new EMPDATA;
			if ((blRc = GetBufferRecord2(ilLc,prlEmp)) == true)
			{
				Add(prlEmp);
				ilEmpCount++;
			}
			else
			{
				delete prlEmp;
			}
		}
		blRc = true;
	}

	ogBasicData.Trace("CedaEmpData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(EMPDATA), pclWhere);
    return blRc;
}


//// Prepare some staff data, not read from database
//void CedaEmpData::PrepareEmpData(EMPDATA *prpEmp)
//{
//
//}



/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class)

//CCSReturnCode CedaEmpData::InsertEmployee(const EMPDATA *prpEmpData)
//{
//   	EMPDATA *prlData;
//
//    if (EmpExist(prpEmpData->Peno,&prlData))
//	{
//        return (UpdateEmpRecord(prpEmpData));
//	}
//    else
//	{
//        return (InsertEmpRecord(prpEmpData));
//	}
//}

//CCSReturnCode CedaEmpData::UpdateEmployee(const EMPDATA *prpEmpData)
//{
//	EMPDATA *prlData;
//
//    if (EmpExist(prpEmpData->Peno,&prlData) == RCSuccess)
//	{
//		memcpy(prlData,prpEmpData,sizeof(EMPDATA));
//		return(UpdateEmpRecord(prpEmpData));
//	}
//	else
//	{
//		RC(RCFailure);
//		return RCFailure;
//	}
//}


bool CedaEmpData::EmpExist(const char *Peno,EMPDATA **prpData)
{
	// don't read from database anymore, just check internal array
	EMPDATA *prlData;
	bool blRc = true;

	if(omPenoMap.Lookup(Peno,(void *&)prlData)  == TRUE)
	{
		*prpData = prlData;
	}
	else
	{
		blRc = false;
	}
	return blRc;
}

//CCSReturnCode CedaEmpData::InsertEmpRecord(const EMPDATA *prpEmpData)
//{
//	return(RCSuccess);
//    // insert new employee record into table "EMPCKI"
//    char *EmpFields = "Peno, lnam, fnam, sxid, bdat, edat, rdat, "
//        "mari, adr1, adr2, adr3, adr4, tel1, tel2, chld, bdc1, bdc2, bdc3";
//    char EmpData[512]; // EMPCKI ~ 340 byte
//    sprintf(EmpData, "%s,%s,%s,%s,%s,%s,%s,%s,%s,"
//					 "%s,%s,%s,%s,%s,%s,%d,%s,%s,%s",
//        prpEmpData->Peno, 
//        prpEmpData->Lnam, prpEmpData->Fnam,
//		prpEmpData->Sxid, prpEmpData->Bdat,
//		prpEmpData->Edat, prpEmpData->Rdat,
//		prpEmpData->Mari, prpEmpData->Adr1,
//		prpEmpData->Adr2, prpEmpData->Adr3,
//		prpEmpData->Adr4, prpEmpData->Tel1,
//		prpEmpData->Tel2, prpEmpData->Chld,
//		prpEmpData->Bdc1, prpEmpData->Bdc2,
//		prpEmpData->Bdc3);
//
//   return (CedaAction("IRT", "EMPCKI", EmpFields, "", "", EmpData));
//}

//CCSReturnCode CedaEmpData::UpdateEmpRecord( const EMPDATA *prpEmpData)
//{
//	char pclData[824];
//	return(RCSuccess);
//
//    // update employee data in table "EMPCKI"
//    // insert new employee record into table "EMPCKI"
//    char *EmpFields = " lnam, fnam, sxid, bdat, edat, rdat, "
//        "mari, adr1, adr2, adr3, adr4, tel1, tel2, chld, bdc1, bdc2, bdc3";
//    
//	//char EmpData[512]; // EMPCKI ~ 340 byte
//	char pclSelection[64]; 
//    /*
//	sprintf(EmpData, "%s,%s,%s,%s,%s,%s,%s,%s,"
//					 "%s,%s,%s,%s,%s,%s,%d,%s,%s,%s",
//        prpEmpData->Lnam, prpEmpData->Fnam,
//		prpEmpData->Sxid, prpEmpData->Bdat,
//		prpEmpData->Edat, prpEmpData->Rdat,
//		prpEmpData->Mari, prpEmpData->Adr1,
//		prpEmpData->Adr2, prpEmpData->Adr3,
//		prpEmpData->Adr4, prpEmpData->Tel1,
//		prpEmpData->Tel2, prpEmpData->Chld,
//		prpEmpData->Bdc1, prpEmpData->Bdc2,
//		prpEmpData->Bdc3);
//	*/
//
//   sprintf(pclSelection," where Peno = '%s' ",prpEmpData->Peno);
//	CString olListOfData;
//	strcpy((char *)prpEmpData->Bdc3,"000");
//	MakeCedaData(olListOfData,(void *)prpEmpData);
//	strcpy(pclData,olListOfData);
//
//   return (CedaAction("URT",pclSelection,"",pclData));
////	       CedaAction("URT",pclWhere,"",olListOfData);
//
//}

// the callback function for storing broadcasted fligthdata in FLIGHTDATA array

void  ProcessEmpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogEmpData.ProcessEmpBc(ipDDXType,vpDataPointer,ropInstanceName);
}


void  CedaEmpData::ProcessEmpBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	char pclPeno[24];

	if (ipDDXType == BC_EMP_CHANGE)
	{
		EMPDATA *prpEmp;
		struct BcStruct *prlEmpData = (struct BcStruct *) vpDataPointer;
		ogBasicData.LogBroadcast(prlEmpData);

		strcpy(pclPeno,prlEmpData->Selection);
		if (omPenoMap.Lookup(pclPeno,(void *& )prpEmp) == TRUE)
		{
			GetRecordFromItemList(prpEmp,prlEmpData->Fields,prlEmpData->Data);
			TRACE("ProcessEmpBc %s %s\n",prpEmp->Peno,(LPCSTR)ropInstanceName);
		}
		else
		{
			prpEmp = new EMPDATA;
			GetRecordFromItemList(prpEmp,prlEmpData->Fields,prlEmpData->Data);
//			PrepareEmpData(prpEmp);
			Add(prpEmp);
			TRACE("Process new Emp %s %s\n",prpEmp->Peno,(LPCSTR)ropInstanceName);
		}
	}
}

EMPDATA * CedaEmpData::GetEmpByUrno(long lpUrno)
{
	EMPDATA *prlEmp = NULL;
	omUrnoMap.Lookup((void *) lpUrno,(void *& )prlEmp);
	return prlEmp;
}

EMPDATA * CedaEmpData::GetEmpByPeno(const char *pcpPeno)
{
	EMPDATA *prlEmp = NULL;
	omPenoMap.Lookup(pcpPeno,(void *& )prlEmp);
	return prlEmp;
}

bool CedaEmpData::GetEmpsByLanm(CCSPtrArray <EMPDATA> &ropEmployees,CString omLnam)
{
	ropEmployees.RemoveAll();

	int ilLen = omLnam.GetLength();
	int ilCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCount; ilLc++)
	{
		if (strnicmp(omLnam,omData[ilLc].Lanm,ilLen) == 0)
		{
			ropEmployees.Add(&omData[ilLc]);
		}
	}
	return true;
}

CString CedaEmpData::GetPenoByUrno(long lpUrno)
{
	CString olPeno;
	EMPDATA *prlEmp = GetEmpByUrno(lpUrno);
	if(prlEmp != NULL)
	{
		olPeno = prlEmp->Peno;
	}
	return olPeno;
}

CString CedaEmpData::GetTableName(void)
{
	return CString(pcmTableName);
}

CString CedaEmpData::GetEmpName(long lpUrno, bool bpFullName /*=false*/)
{
	return GetEmpName(GetEmpByUrno(lpUrno), bpFullName);
}

CString CedaEmpData::GetEmpName(EMPDATA *prpEmp, bool bpFullName /*=false*/)
{
	CString olEmpName;
	if(prpEmp != NULL)
	{
		if(!bpFullName)
		{
			olEmpName = ogNameConfig.GetEmpName(prpEmp->Finm, prpEmp->Lanm, prpEmp->Shnm, prpEmp->Perc, prpEmp->Peno);
		}

		if(olEmpName.IsEmpty())
		{
			olEmpName.Format("%s, %s", prpEmp->Lanm, prpEmp->Finm);
		}
	}
	else
	{
		olEmpName = "prpEmp is NULL in CedaEmpData::GetEmpName";
	}

	return olEmpName;
}