// ReleaseDialog.h : header file
//
#ifndef _CRELEASEDIALOG_H_
#define _CRELEASEDIALOG_H_

/////////////////////////////////////////////////////////////////////////////
// ReleaseDialog dialog

class ReleaseDialog : public CDialog
{
// Construction
public:
	ReleaseDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ReleaseDialog)
	enum { IDD = IDD_RELEASE };
	CListBox	m_List;
	//}}AFX_DATA


	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ReleaseDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ReleaseDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
};
#endif _CRELEASEDIALOG_H_