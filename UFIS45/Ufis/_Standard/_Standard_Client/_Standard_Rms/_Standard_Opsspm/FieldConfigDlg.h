#if !defined(AFX_FIELDCONFIGDLG_H__D73971FB_B759_4CF1_9764_48BAB2A83439__INCLUDED_)
#define AFX_FIELDCONFIGDLG_H__D73971FB_B759_4CF1_9764_48BAB2A83439__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FieldConfigDlg.h : header file
//
#include <CCSPtrArray.h>
#include <CCSGlobl.h>

/////////////////////////////////////////////////////////////////////////////
// CFieldConfigDlg dialog
struct FieldDataStruct
{
	CString Field;  // eg. "Employee Name" or "Function"
	CString Text;
	int NumChars;
	CString Suffix;
	int Key; // integer key faster to access data than string
	int SortOrder; // used to specify if the fields must be displayed in a certain order

	FieldDataStruct(void)
	{
		NumChars = 0;
		SortOrder = -1;
	}

	FieldDataStruct& FieldDataStruct::operator=(const FieldDataStruct& rhs)
	{
		if (&rhs != this)
		{
			Field = rhs.Field;
			Text = rhs.Text;
			NumChars = rhs.NumChars;
			Suffix = rhs.Suffix;
			Key = rhs.Key;
			SortOrder = rhs.SortOrder;
		}		
		return *this;
	}
};
typedef struct FieldDataStruct FIELDDATA;

struct FieldTypeStruct
{
	CString	FieldType;		// the index of the string in FieldTypeText
	CString	FieldTypeText;	// the text held in the "Where" combo box
	CCSPtrArray <FIELDDATA> PossibleFields;
	CCSPtrArray <FIELDDATA> SelectedFields;
};
typedef struct FieldTypeStruct FIELDTYPE;




class CFieldConfigDlg : public CDialog
{
// Construction
public:
	CFieldConfigDlg(CWnd* pParent = NULL);   // standard constructor
	~CFieldConfigDlg();

// Dialog Data
	//{{AFX_DATA(CFieldConfigDlg)
	enum { IDD = IDD_FIELDCONFIGDLG };
	CComboBox	m_FieldTypeCtrl;
	CListBox	m_List2Ctrl;
	CListBox	m_List1Ctrl;
	int		m_NumChars;
	CString	m_Preview;
	CString	m_SampleText;
	CString	m_Suffix;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFieldConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFieldConfigDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnAdd();
	afx_msg void OnRemove();
	afx_msg void OnSelchangeList2();
	afx_msg void OnKillfocusNumchars();
	afx_msg void OnKillfocusSuffix();
	afx_msg void OnSelchangeFieldtype();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void SetControlText(int ipControlId, int ipStringId);
	void AddField(int ipFieldType, int ipKey, int ipFieldId, CString opSampleText, int ipDefaultNumChars, int ipSortOrder = -1);
	void GetFieldFormat(int ipFieldType, CUIntArray &ropKeys, CUIntArray &ropNumChars, CStringArray &ropSuffixes);

private:
	CCSPtrArray <FIELDTYPE> omFieldTypes;
	FIELDTYPE *pomSelectedFieldType;
	void UpdateFieldData();
	FIELDDATA *GetSelectedField();
	void UpdatePreview();
	void SaveSettings(void);
	void LoadSettings(void);
	void ExtractSettings(CString &ropValue, CString &ropField, int &ripNumChars, CString &ropSuffix);
	CString FormatField(FIELDDATA *prpField);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class CFieldDataArray
{
// Construction
public:
	CFieldDataArray();   // standard constructor
	~CFieldDataArray();
	void Add(FIELDDATA *prpFieldData);
	void Add(CString opField, CString opText, int ipNumChars, CString opSuffix, int ipKey = -1, int ipSortOrder = -1);
	RemoveAll();
	int GetSize();
	const FIELDDATA &operator [](int n) const { return *(FIELDDATA *)&omData[n]; }
private:
	CCSPtrArray <FIELDDATA> omData;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

struct ConfiguredFieldStruct
{
	CStringArray	Fields;
	CUIntArray		Keys;
	CUIntArray		NumChars;
	CStringArray	Suffixes;
	CUIntArray		SortOrders;
};

typedef struct ConfiguredFieldStruct CONFIGUREDFIELDDATA;


class CFieldConfig
{
// Construction
public:
	CFieldConfig();   // standard constructor
	~CFieldConfig();

	CMapStringToPtr omFieldFormats;
	void Initialize();
	void ExtractFieldsFromValue(CString &ropValue, CStringArray &ropFields, CUIntArray &ropKeys, CUIntArray &ropNumChars, CStringArray &ropSuffixes, CUIntArray &ropSortOrders);
	void AddConfiguredField(CString opFieldType, CStringArray &ropFields, CUIntArray &ropKeys, CUIntArray &ropNumChars, CStringArray &ropSuffixes, CUIntArray &ropSortOrders);
	CONFIGUREDFIELDDATA *GetFieldFormat(CString opFieldType);
	CString ConfigureField(CONFIGUREDFIELDDATA *prpFieldFormat, CStringArray &ropValues);
	void ConfigureField(CONFIGUREDFIELDDATA *prpFieldFormat, CStringArray &ropValues, CFieldDataArray &ropFields);
	CString ConfigureField(int ipFieldType, char *fmt, ...);
	void ConfigureField(int ipFieldType, CFieldDataArray &ropFields, char *fmt, ...);
};

extern CFieldConfig ogFieldConfig;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_FIELDCONFIGDLG_H__D73971FB_B759_4CF1_9764_48BAB2A83439__INCLUDED_)
