#if !defined(AFX_SELECTSHIFTDLG_H__4F3118BE_5B30_48FC_B251_633AE88A69DB__INCLUDED_)
#define AFX_SELECTSHIFTDLG_H__4F3118BE_5B30_48FC_B251_633AE88A69DB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectShiftDlg.h : header file
//
#include <BasicData.h>
#include <opsspm.h>
#include <CedaShiftData.h>

/////////////////////////////////////////////////////////////////////////////
// CSelectShiftDlg dialog

class CSelectShiftDlg : public CDialog
{
// Construction
public:
	CSelectShiftDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelectShiftDlg)
	enum { IDD = IDD_SELECTSHIFTDLG };
	CListBox	m_ShiftListCtrl;
	int		m_Sort;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectShiftDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelectShiftDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDblclkShiftlist();
	afx_msg void OnSortByEmp();
	afx_msg void OnSortByShift();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CCSPtrArray <SHIFTDATA> *pomShifts;
	SHIFTDATA *prmSelectedShift;
	void SetSday(CString opSday);

private:
	void DisplayShifts();
	CString omSday;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTSHIFTDLG_H__4F3118BE_5B30_48FC_B251_633AE88A69DB__INCLUDED_)
