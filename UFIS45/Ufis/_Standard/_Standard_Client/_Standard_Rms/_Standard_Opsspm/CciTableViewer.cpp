// ccitblvw.cpp -- the viewer for CCI Table
//
// Written by:
// somebody which I assumed that it should be Manfred at the middle period of
// the project.
//
// Modification History:
// Damkerng Thammathakerngkit	Sep 29th, 1996
//	Tries to revise (especially on the DDX processing) this viewer for
//	drag-and-drop functionality (If the viewer does not process DDX callback,
//	dropping a pool job to CCI Table will always begin at the same time.

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <AllocData.h>

#include <OpssPm.h>
#include <table.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <PrintControl.h>

#include <CciTableViewer.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <CedaCicData.h>

class CCITable; //Singapore
extern bool BGS;

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

static int ByName(const ALLOCUNIT **ppp1, const ALLOCUNIT **ppp2)
{
	return (int)(strcmp((*ppp1)->Name,(*ppp2)->Name));
}

static int ByName(const CICDATA **ppp1, const CICDATA **ppp2)
{
	return (int)(strcmp((*ppp1)->Cnam,(*ppp2)->Cnam));
}

// Groupping definition:
// This will make the program faster a little bit because it will replace string comparison.
enum {
	GROUP_BY_TERMINAL,
	GROUP_BY_HALL,
	GROUP_BY_REGION,
	GROUP_BY_LINE,
};

#define GROUP_BY_DEFAULT	GROUP_BY_TERMINAL	// define a bullet-proof grouping symbol

// Sorting definition:
enum {
    BY_ALID,
    BY_GROUP,
	BY_NONE
};

/////////////////////////////////////////////////////////////////////////////
// CciTableViewer

CciTableViewer::CciTableViewer()
{
    SetViewerKey("CciTab");
    pomTable = NULL;
    ogCCSDdx.Register(this, JOB_NEW,	CString("CCITBLVW"), CString("Job New"),	CciTableCf);
    ogCCSDdx.Register(this, JOB_CHANGE, CString("CCITBLVW"), CString("Job Change"), CciTableCf);
    ogCCSDdx.Register(this, JOB_DELETE, CString("CCITBLVW"), CString("Job Delete"), CciTableCf);


	CTime olDay = ogBasicData.GetTimeframeStart();
	omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omEndTime	= CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);
}

CciTableViewer::~CciTableViewer()
{
	ogCCSDdx.UnRegister(this,NOTUSED);
	DeleteAll();
}

void CciTableViewer::Attach(CTable *popTable)
{
    pomTable = popTable;
}

void CciTableViewer::ChangeViewTo(const char *pcpViewName, CString opDate)
{
	omDate = opDate;
	ChangeViewTo(pcpViewName);
}

void CciTableViewer::ChangeViewTo(const char *pcpViewName)
{
	ogCfgData.rmUserSetup.CCBV = pcpViewName;
    SelectView(pcpViewName);
	PrepareGrouping();
    PrepareFilter();
    PrepareSorter();

    DeleteAll();
    MakeLines();
	UpdateDisplay();
}

void CciTableViewer::PrepareGrouping()
{
	CString olGroupBy = CViewer::GetGroup();
	if (olGroupBy == "Halle")
		omGroupBy = GROUP_BY_TERMINAL;
	else if (olGroupBy == "Hall")
		omGroupBy = GROUP_BY_HALL;
	else if (olGroupBy == "Region")
		omGroupBy = GROUP_BY_REGION;
	else if (olGroupBy == "Line")
		omGroupBy = GROUP_BY_LINE;
	else
		omGroupBy = GROUP_BY_DEFAULT;
}

void CciTableViewer::PrepareFilter()
{
	CStringArray olFilterValues;
	int i, n;

	GetFilter("Halle", olFilterValues);
	omCMapForTerminal.RemoveAll();
	n = olFilterValues.GetSize();
	for (i = 0; i < n; i++)
		omCMapForTerminal[olFilterValues[i]] = NULL;
			// we can use NULL here since what the map mapped to is unimportant

	GetFilter("Hall", olFilterValues);
	omCMapForHall.RemoveAll();
	n = olFilterValues.GetSize();
	for (i = 0; i < n; i++)
		omCMapForHall[olFilterValues[i]] = NULL;

	GetFilter("Region", olFilterValues);
	omCMapForRegion.RemoveAll();
	n = olFilterValues.GetSize();
	for (i = 0; i < n; i++)
		omCMapForRegion[olFilterValues[i]] = NULL;

	GetFilter("Line", olFilterValues);
	omCMapForLine.RemoveAll();
	n = olFilterValues.GetSize();
	for (i = 0; i < n; i++)
		omCMapForLine[olFilterValues[i]] = NULL;
}

void CciTableViewer::PrepareSorter()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);

    omSortOrder.RemoveAll();
    for (int i = 0; i < olSortOrder.GetSize(); i++)
    {
        int ilSortOrderEnumeratedValue;
        if (olSortOrder[i] == "Desk")
            ilSortOrderEnumeratedValue = BY_ALID;
        else if (olSortOrder[i] == "Group")
            ilSortOrderEnumeratedValue = BY_GROUP;
		else
            ilSortOrderEnumeratedValue = BY_NONE;

        omSortOrder.Add(ilSortOrderEnumeratedValue);
    }
}

int CciTableViewer::CompareCci(CCIDATA_LINE *prpCci1, CCIDATA_LINE *prpCci2)
{
    int n = omSortOrder.GetSize();
    for (int i = 0; i < n; i++)
    {
        int ilCompareResult = 0;

        switch (omSortOrder[i])
        {
        case BY_ALID:
            ilCompareResult = (prpCci1->Alid == prpCci2->Alid)? 0:
                (prpCci1->Alid > prpCci2->Alid)? 1: -1;
            break;
        case BY_GROUP:
            ilCompareResult = (prpCci1->GroupName == prpCci2->GroupName)? 0:
                (prpCci1->GroupName > prpCci2->GroupName)? 1: -1;
            break;
        }

        if (ilCompareResult != 0)
            return ilCompareResult;
    }

    return 0;   
}

BOOL CciTableViewer::IsPassFilter(const char *pcpTerminal, const char *pcpHall,const char *pcpRegion,const char *pcpLine)
{
	void *p;
	BOOL blIsTerminalOk = omCMapForTerminal.Lookup(CString(pcpTerminal), p);
	BOOL blIsHallOk		= omCMapForHall.Lookup(CString(pcpHall), p);
	BOOL blIsRegionOk	= omCMapForRegion.Lookup(CString(pcpRegion), p);
	BOOL blIsLineOk		= omCMapForLine.Lookup(CString(pcpLine), p);

	switch (omGroupBy)
	{
	case GROUP_BY_TERMINAL: return blIsTerminalOk;
	case GROUP_BY_HALL:		return blIsHallOk;
	case GROUP_BY_REGION:	return blIsRegionOk;
	case GROUP_BY_LINE:		return blIsLineOk;
	}

	return FALSE;
}

BOOL CciTableViewer::IsSameGroup(CCIDATA_LINE *prpLine1, CCIDATA_LINE *prpLine2)
{
    // Compare in the sort order, from the outermost to the innermost
	return (prpLine1->GroupName == prpLine2->GroupName);
}

/////////////////////////////////////////////////////////////////////////////
// CciTableViewer -- code specific to this class

void CciTableViewer::MakeLines()
{
	CString olGroupBy;
	switch (omGroupBy)
	{
	case GROUP_BY_TERMINAL:
		olGroupBy = "Halle";
		break;
	case GROUP_BY_HALL:
		olGroupBy = "Hall";
		break;
	case GROUP_BY_REGION:
		olGroupBy = "Region";
		break;
	case GROUP_BY_LINE:
		olGroupBy = "Line";
		break;
	default:	// error: invalid groupping specified
		return;
	}

	CStringArray olGroupIds;
	GetFilter(olGroupBy, olGroupIds);
	int ilNumGroups = olGroupIds.GetSize();
	for(int i = 0; i < ilNumGroups ; i++)
	{
		MakeLines(olGroupIds[i]);
	}
}

void CciTableViewer::MakeLines(const CString& ropGroup)
{
	switch (omGroupBy)
	{
		case GROUP_BY_TERMINAL: 
			MakeLinesWhenGroupByTerminal(ropGroup); 
		break;
		case GROUP_BY_HALL:		
			MakeLinesWhenGroupByHall(ropGroup); 
		break;
		case GROUP_BY_REGION:	
			MakeLinesWhenGroupByRegion(ropGroup); 
		break;
		case GROUP_BY_LINE:		
			MakeLinesWhenGroupByLine(ropGroup); 
		break;
	}
}

void CciTableViewer::MakeLinesWhenGroupByTerminal(const CString& ropGroupName)
{
	// Create desks from this found terminal
	CCSPtrArray <CICDATA> olCics;
	CCSPtrArray <ALLOCUNIT> olAllocUnitCics;

	// counters grouped according to the static group
	ogAllocData.GetUnitsByGroup(ALLOCUNITTYPE_CICGROUP,ropGroupName,olAllocUnitCics);
	olAllocUnitCics.Sort(ByName);
	int ilNumCics = olAllocUnitCics.GetSize();

	CICDATA *prlCicDesk = NULL;

	for(int ilCic = 0; ilCic < ilNumCics; ilCic++)
	{
		prlCicDesk = ogCicData.GetCicByUrno(olAllocUnitCics[ilCic].Urno);

		if(prlCicDesk != NULL)
		{
			CString olDeskType = ogBasicData.GetDeskType(prlCicDesk->Cnam);

			if (!IsPassFilter(ropGroupName, prlCicDesk->Hall,prlCicDesk->Region,prlCicDesk->Line))
				continue;

			MakeLine(prlCicDesk->Cnam,ropGroupName,olDeskType);
		}
	}

}


void CciTableViewer::MakeLinesWhenGroupByHall(const CString& ropGroupName)
{
	// Create desks from this found terminal
	CCSPtrArray <CICDATA> olCics;
	CCSPtrArray <ALLOCUNIT> olAllocUnitCics;

	// counters grouped by the hall defined in CICTAB
	ogCicData.GetCicsByHall(olCics, ropGroupName);
	olCics.Sort(ByName);
	int ilNumCics = olCics.GetSize();

	CICDATA *prlCicDesk = NULL;
	for(int ilCic = 0; ilCic < ilNumCics; ilCic++)
	{
		prlCicDesk = &olCics[ilCic];

		if(prlCicDesk != NULL)
		{
			CString olDeskType = ogBasicData.GetDeskType(prlCicDesk->Cnam);

			if (!IsPassFilter(ropGroupName,prlCicDesk->Hall,prlCicDesk->Region,prlCicDesk->Line))
				continue;
			MakeLine(prlCicDesk->Cnam,ropGroupName,olDeskType);
		}
	}

}

void CciTableViewer::MakeLinesWhenGroupByRegion(const CString& ropGroupName)
{
	// Create desks from this found terminal
	CCSPtrArray <CICDATA> olCics;
	CCSPtrArray <ALLOCUNIT> olAllocUnitCics;

	// counters grouped by the hall defined in CICTAB
	ogCicData.GetCicsByRegion(olCics, ropGroupName);
	olCics.Sort(ByName);
	int ilNumCics = olCics.GetSize();

	CICDATA *prlCicDesk = NULL;
	for(int ilCic = 0; ilCic < ilNumCics; ilCic++)
	{
		prlCicDesk = &olCics[ilCic];

		if(prlCicDesk != NULL)
		{
			CString olDeskType = ogBasicData.GetDeskType(prlCicDesk->Cnam);

			if (!IsPassFilter(ropGroupName,prlCicDesk->Hall,prlCicDesk->Region,prlCicDesk->Line))
				continue;
			MakeLine(prlCicDesk->Cnam,ropGroupName,olDeskType);
		}
	}

}

void CciTableViewer::MakeLinesWhenGroupByLine(const CString& ropGroupName)
{
	// Create desks from this found terminal
	CCSPtrArray <CICDATA> olCics;
	CCSPtrArray <ALLOCUNIT> olAllocUnitCics;

	// counters grouped by the hall defined in CICTAB
	ogCicData.GetCicsByLine(olCics, ropGroupName);
	olCics.Sort(ByName);
	int ilNumCics = olCics.GetSize();

	CICDATA *prlCicDesk = NULL;
	for(int ilCic = 0; ilCic < ilNumCics; ilCic++)
	{
		prlCicDesk = &olCics[ilCic];

		if(prlCicDesk != NULL)
		{
			CString olDeskType = ogBasicData.GetDeskType(prlCicDesk->Cnam);

			if (!IsPassFilter(ropGroupName,prlCicDesk->Hall,prlCicDesk->Region,prlCicDesk->Line))
				continue;
			MakeLine(prlCicDesk->Cnam,ropGroupName,olDeskType);
		}
	}

}


void CciTableViewer::MakeLine(const CString& ropCicName,const CString& ropGroupName,const CString& ropDeskType)
{
	CCIDATA_LINE olLine;
	olLine.Alid		 = ropCicName;
	olLine.GroupName = ropGroupName;
	olLine.DeskType  = ropDeskType;

	int ilCic = CreateLine(&olLine);

	CCSPtrArray<JOBDATA> olJobs;
	ogJobData.GetJobsByAlid(olJobs, ropCicName, ALLOCUNITTYPE_CIC);
	int ilJobCount = olJobs.GetSize();
	for (int ilJobno = 0; ilJobno < ilJobCount; ilJobno++)
	{
		JOBDATA *prlJob = &olJobs[ilJobno];
		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);

		CCIDATA_JOB olAssignment;
		olAssignment.Urno = prlJob->Urno;
		olAssignment.Peno = CString(prlJob->Peno);
		olAssignment.Name = (prlEmp) ? ogEmpData.GetEmpName(prlEmp) : "(unknown)";
		olAssignment.Acfr = prlJob->Acfr;
		olAssignment.Acto = prlJob->Acto;
		if (omStartTime <= prlJob->Acto && prlJob->Acfr <= omEndTime)
			CreateAssignment(ilCic, &olAssignment);
	}
}

BOOL CciTableViewer::FindDesk(const CString &ropDesk, int &ripLineno)
{
    for (ripLineno = 0; ripLineno < omLines.GetSize(); ripLineno++)
        if (omLines[ripLineno].Alid == ropDesk)
            return TRUE;
    return FALSE;
}

BOOL CciTableViewer::FindAssignment(long lpJobUrno, int &ripLineno, int &ripAssignmentno)
{
    for (ripLineno = 0; ripLineno < omLines.GetSize(); ripLineno++)
	{
        // we have to scan every assignments in this desk
		CCIDATA_LINE *prlLine = &omLines[ripLineno];
		int ilAssignmentCount = omLines[ripLineno].JobData.GetSize();
		for (ripAssignmentno = 0; ripAssignmentno < ilAssignmentCount; ripAssignmentno++)
			if (omLines[ripLineno].JobData[ripAssignmentno].Urno == lpJobUrno)
				return TRUE;
	}
    return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// CciTableViewer -- CCIDATA_LINE array maintenance
//
// DeleteLine() is unnecessary because we can assume that there is no possibility
// that a CCI Desk will be removed out of the system. Furthermore, there is no
// DDX processing for the CCI Desk changes also.

void CciTableViewer::DeleteAll()
{
	int n = omLines.GetSize();
	for (int i = 0; i < n; i++)
		omLines[i].JobData.DeleteAll();
	omLines.DeleteAll();
}

int CciTableViewer::CreateLine(CCIDATA_LINE *prpCci)
{
    int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareCci(prpCci, &omLines[ilLineno]) <= 0)
            break;  
#else
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareCci(prpCci, &omLines[ilLineno-1]) >= 0)
            break;  
#endif

	CCIDATA_LINE pCci = *prpCci;
    omLines.NewAt(ilLineno, pCci);
    return ilLineno;
}

int CciTableViewer::CreateAssignment(int ipLineno, CCIDATA_JOB *prpAssignment)
{
    int ilAssignmentCount = omLines[ipLineno].JobData.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilAssignmentno = 0; ilAssignmentno < ilAssignmentCount; ilAssignmentno++)
        if (prpAssignment->Acfr <= omLines[ipLineno].JobData[ilAssignmentno].Acfr)
            break;  // should be inserted before Lines[ilAssignmentno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilAssignmentno = ilAssignmentCount; ilAssignmentno > 0; ilAssignmentno--)
        if (prpAssignment->Acfr >= omLines[ipLineno].JobData[ilAssignmentno-1].Acfr)
            break;  // should be inserted after Lines[ilAssignmentno-1]
#endif

    omLines[ipLineno].JobData.NewAt(ilAssignmentno, *prpAssignment);
    return ilAssignmentno;
}

void CciTableViewer::DeleteAssignment(int ipLineno, int ipAssignmentno)
{
	if (ipLineno < omLines.GetSize())
		omLines[ipLineno].JobData.DeleteAt(ipAssignmentno);
}

/////////////////////////////////////////////////////////////////////////////
// CciTableViewer - display drawing routine

void CciTableViewer::UpdateDisplay()
{
    //pomTable->SetHeaderFields("Checkin Desk|Group|Name|Von-bis|Name|Von-bis|Name|Von-bis|Name|Von-bis|Name|Von-bis|Name|Von-bis");
    
	if (BGS) pomTable->SetHeaderFieldsBGS(GetString(IDS_STRING61559));

	else   pomTable->SetHeaderFields(GetString(IDS_STRING61559));
	

    pomTable->SetFormatList("5|20|20|9|20|9|20|9|20|9|20|9|20|9");

	pomTable->ResetContent();
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++) 
	{
		pomTable->AddTextLine(Format(&omLines[ilLc]), &omLines[ilLc]);

        // Display grouping effect
        if (ilLc == omLines.GetSize()-1 ||
			!IsSameGroup(&omLines[ilLc], &omLines[ilLc+1]))
            pomTable->SetTextLineSeparator(ilLc, ST_THICK);
	}

	pomTable->DisplayTable();
}

CString CciTableViewer::Format(CCIDATA_LINE *prpLine)
{
    CString s;
	s =  prpLine->Alid + "|" + prpLine->GroupName;
	
	int nLineCount = prpLine->JobData.GetSize();
    for (int i = 0; i < nLineCount; i++)
    {
        CCIDATA_JOB *pJobData = &prpLine->JobData[i];
        s += "|" + pJobData->Name + "|";
		s += pJobData->Acfr.Format("%H%M") + "-";
		s += pJobData->Acto.Format("%H%M");
    }
    return s;
}

/////////////////////////////////////////////////////////////////////////////
// CciTableViewer -- DDX processing

void CciTableViewer::CciTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    CciTableViewer *polViewer = (CciTableViewer *)popInstance;

    if (ipDDXType == JOB_NEW)
	{
        polViewer->ProcessJobDelete((JOBDATA *)vpDataPointer);
        polViewer->ProcessJobNew((JOBDATA *)vpDataPointer);
	}
	else if (ipDDXType == JOB_CHANGE)
	{
        if (polViewer->ProcessJobDelete((JOBDATA *)vpDataPointer))
			polViewer->ProcessJobNew((JOBDATA *)vpDataPointer);
	}
	else if (ipDDXType == JOB_DELETE)
        polViewer->ProcessJobDelete((JOBDATA *)vpDataPointer);

	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{	
		if(polViewer->pomTable != NULL && polViewer->pomTable->m_hWnd != NULL)
		{
			CWnd* polWnd = polViewer->pomTable->GetParent();
			(CCITable*)(polWnd)->SendMessage(WM_TABLE_UPDATE_DATACOUNT,0,0);
		}
	}
}

BOOL CciTableViewer::ProcessJobNew(JOBDATA *prpJob)
{
    // If it's not a CCI Desk job, ignore it
	if (CString(prpJob->Aloc) != ALLOCUNITTYPE_CIC)
		return FALSE;

    // Create a new assignment
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);
    CCIDATA_JOB rlAssignment;
    rlAssignment.Peno = prpJob->Peno;
	rlAssignment.Name = (prlEmp) ? ogEmpData.GetEmpName(prlEmp) : "(unknown)";
	rlAssignment.Acfr = prpJob->Acfr;
    rlAssignment.Acto = prpJob->Acto;

    int ilLineno;
    if (FindDesk(prpJob->Alid, ilLineno))  // corresponding desk found?
	{
		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);
		CCIDATA_JOB olAssignment;
		olAssignment.Urno = prpJob->Urno;
		olAssignment.Peno = CString(prpJob->Peno);
		olAssignment.Name = (prlEmp) ? ogEmpData.GetEmpName(prlEmp) : "(unknown)";
		olAssignment.Acfr = prpJob->Acfr;
		olAssignment.Acto = prpJob->Acto;
		if (omStartTime <= prpJob->Acto && prpJob->Acfr <= omEndTime)
		{
			CreateAssignment(ilLineno, &olAssignment);
	        pomTable->ChangeTextLine(ilLineno, Format(&omLines[ilLineno]), &omLines[ilLineno]);
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CciTableViewer::ProcessJobDelete(JOBDATA *prpJob)
{
    // If it's not a CCI Desk job, ignore it
	if (CString(prpJob->Aloc) != ALLOCUNITTYPE_CIC)
		return FALSE;

	int ilLineno, ilAssignmentno;
	if (FindAssignment(prpJob->Urno, ilLineno, ilAssignmentno))
	{
		DeleteAssignment(ilLineno, ilAssignmentno);
        pomTable->ChangeTextLine(ilLineno, Format(&omLines[ilLineno]), &omLines[ilLineno]);
		return TRUE;
	}

	return FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Printing functions

BOOL CciTableViewer::PrintCCILine(CCIDATA_LINE *prpLine,BOOL bpIsLastLine)
{
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 1910;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	else
	{
		if (pomPrint->imLineNo == 0)
		{
			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 120;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = GetString(IDS_STRING61358); // "Desk"
			
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 130;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING61359); // "T"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 370;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING61531); // "Mitarbeiter"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61362); // "Von"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61363); // "Bis"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 370;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING61531); // "Mitarbeiter"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61362); // "Von"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61363);  // "Bis"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 370;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING61531); // "Mitarbeiter"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61362); // "Von"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61363);  // "Bis"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 370;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING61531); // "Mitarbeiter"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61362); // "Von"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = GetString(IDS_STRING61363);  // "Bis"
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();

		}

		CTime olActual = TIMENULL;
		CString olActualMark;
			
		int ilCount = prpLine->JobData.GetSize();
		if (ilCount > 0)
		{
			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 120;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= ilBottomFrame;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = prpLine->Alid;
			
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = prpLine->GroupName;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.FrameTop   = PRINT_NOFRAME;

			//Now the jobs in line; at the moment only 5 Jobs for more Jobs 
			// there is currently no solution

		//int ilCount = prpLine->JobData.GetSize();
			int ilJobNo = 0;
			for(int i = 0; (i < ilCount) || (ilJobNo % 4 != 0); i++)
			{
				CCIDATA_JOB rlJob;

				if (ilJobNo > 3)
				{
					ilJobNo = 0;
					pomPrint->PrintLine(rlPrintLine);
					rlPrintLine.DeleteAll();

					rlElement.Alignment  = PRINT_CENTER;
					rlElement.FrameLeft  = PRINT_NOFRAME;
					rlElement.FrameRight = PRINT_NOFRAME;
					rlElement.FrameTop   = PRINT_NOFRAME;
					rlElement.FrameBottom= PRINT_NOFRAME;
					rlElement.pFont       = &pomPrint->omSmallFont_Bold;
					rlElement.Length     = 330;
					rlElement.Text       = "";
					rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				}
				if (i < ilCount)
				{
					rlJob = prpLine->JobData[i];
				}
				else
				{
					// we have no more jobs, so print empty jobs
					rlJob.Name.Empty();
					rlJob.Acfr = TIMENULL;
					rlJob.Acto = TIMENULL;
				}

				rlElement.Alignment  = PRINT_LEFT;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.FrameTop   = PRINT_NOFRAME;
				rlElement.FrameBottom= ilBottomFrame;
				rlElement.Length     = 370;
				if(i < ilCount)
				{
					rlElement.Text       = rlJob.Name;
				}
				else 
				{
					rlElement.Text       = "";
				}
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.Alignment  = PRINT_CENTER;
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.FrameTop   = PRINT_NOFRAME;
				rlElement.Length     = 90;
				if(i < ilCount)
					rlElement.Text       = rlJob.Acfr.Format("%H:%M");
				else
					rlElement.Text       = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.FrameTop   = PRINT_NOFRAME;
				rlElement.Length     = 90;
				if(i < ilCount)
					rlElement.Text       = rlJob.Acto.Format("%H:%M");
				else
					rlElement.Text       = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
				ilJobNo++;
			}
				pomPrint->PrintLine(rlPrintLine);
				rlPrintLine.DeleteAll();
		}
	}

	return TRUE;
}

BOOL CciTableViewer::PrintCCIHeader(CCSPrint *pomPrint)
{
	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader();
	return TRUE;
}

void CciTableViewer::PrintView()
{  
	CString omTarget;
	omTarget.Format(GetString(IDS_STRING61285),"",omStartTime.Format("%d.%m.%Y %H:%M"),omEndTime.Format("%d.%m.%Y %H:%M"));
	omTarget += GetString(IDS_STRING61292) + ogCfgData.rmUserSetup.CCBV;		
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomTable,PRINT_LANDSCAPE,60,500,250,GetString(IDS_STRING61355),olPrintDate,omTarget);
	pomPrint->imMaxLines = 20;

	if (pomPrint != NULL)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61356),pcgAppName);
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			pomPrint->imLineHeight = 62;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STRING61355));
			rlDocInfo.lpszDocName = pclDocName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				if( pomPrint->imLineNo >= pomPrint->imMaxLines )
				{
					if ( pomPrint->imPageNo > 0)
					{
						PrintCCILine(&omLines[i],TRUE);
						pomPrint->PrintFooter(olTxt,GetString(IDS_STRING61355));
						pomPrint->imLineNo = pomPrint->imMaxLines + 1;
						pomPrint->omCdc.EndPage();
						pomPrint->imLineTop = pomPrint->imFirstLine;
					}
					PrintCCIHeader(pomPrint);
				}
				PrintCCILine(&omLines[i],FALSE);
			}
			PrintCCILine(NULL,TRUE);
			CString olTxt;
			pomPrint->PrintFooter(olTxt,GetString(IDS_STRING61355));
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	pomPrint = NULL;
	}

	/*
	CPrintCtrl *prn = new CPrintCtrl(FALSE);
	prn->FlightSchedulePrint(&ogShiftData, (char *)(const char *)omDate);
	delete prn;
	*/
}


CTime CciTableViewer::StartTime() const
{
	return omStartTime;
}

CTime CciTableViewer::EndTime() const
{
	return omEndTime;
}

int CciTableViewer::Lines() const
{
	return omLines.GetSize();
}

CCIDATA_LINE* CciTableViewer::GetLine(int ipLineNo)
{
	if (ipLineNo >= 0 && ipLineNo < omLines.GetSize())
		return &omLines[ipLineNo];
	else
		return NULL;
}

void CciTableViewer::SetStartTime(CTime opStartTime)
{
	omStartTime = opStartTime;
}

void CciTableViewer::SetEndTime(CTime opEndTime)
{
	omEndTime = opEndTime;
}

