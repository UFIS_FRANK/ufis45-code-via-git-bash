// CPrintPreviewView.cpp : implementation of the CPrintPreviewView class
//

#include "stdafx.h"
#include "CPrintPreviewView.h"
#include "PreplanTable.h"
#include <Winspool.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintPreviewView

BOOL bgSelectPrinter = FALSE;

IMPLEMENT_DYNCREATE(CPrintPreviewView, CScrollView)

BEGIN_MESSAGE_MAP(CPrintPreviewView, CScrollView)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintPreviewView construction/destruction

CPrintPreviewView::CPrintPreviewView()
{
	// TODO: add construction code here
	SetScrollSizes(MM_TEXT, CSize(0, 0));
	m_bPrint = FALSE;
	CWindowDC dc (NULL);
	m_nLogPixelsY		= dc.GetDeviceCaps (LOGPIXELSY);
	m_nLogPixelsX		= dc.GetDeviceCaps (LOGPIXELSX);
	pomOldFrame = NULL;
}

CPrintPreviewView::~CPrintPreviewView()
{
}

BOOL CPrintPreviewView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CScrollView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CPrintPreviewView drawing

void CPrintPreviewView::OnDraw(CDC* pDC)
{
}

void CPrintPreviewView::OnFilePrintPreview() 
{
	CView::OnFilePrintPreview();
}

/////////////////////////////////////////////////////////////////////////////
// CPrintPreviewView printing

BOOL CPrintPreviewView::OnPreparePrinting(CPrintInfo* pInfo)
{	
	return DoPreparePrinting(pInfo);
}

void CPrintPreviewView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	PRINT_PREVIEW_DCINFO olPrintPreviewDcInfo;
	olPrintPreviewDcInfo.pomDC = pDC;
	olPrintPreviewDcInfo.pomPrintInfo = pInfo;
	pomCallMsgWindow->SendMessage(WM_BEGIN_PRINTING,NULL, (LPARAM)&olPrintPreviewDcInfo);
}

void CPrintPreviewView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	PRINT_PREVIEW_DCINFO olPrintPreviewDcInfo;
	olPrintPreviewDcInfo.pomDC = pDC;
	olPrintPreviewDcInfo.pomPrintInfo = pInfo;
	pomCallMsgWindow->SendMessage(WM_END_PRINTING,NULL, (LPARAM)&olPrintPreviewDcInfo);
}

void CPrintPreviewView::OnEndPrintPreview(CDC *pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView)
{
	CView::OnEndPrintPreview(pDC, pInfo, point, pView);    
    GetParentFrame()->DestroyWindow();
}

/////////////////////////////////////////////////////////////////////////////
// CPrintPreviewView diagnostics

#ifdef _DEBUG
void CPrintPreviewView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CPrintPreviewView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPrintPreviewView message handlers

void CPrintPreviewView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	PRINT_PREVIEW_DCINFO olPrintPreviewDcInfo;
	olPrintPreviewDcInfo.pomDC = pDC;
	olPrintPreviewDcInfo.pomPrintInfo = pInfo;
	pomCallMsgWindow->SendMessage(WM_PRINT_PREVIEW,NULL, (LPARAM)&olPrintPreviewDcInfo);
}