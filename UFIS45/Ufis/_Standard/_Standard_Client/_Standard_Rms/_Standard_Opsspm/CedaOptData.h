#ifndef _COPTD_H_
#define _COPTD_H_

#include <CCSGlobl.h> 
#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//@Man: OPTDATA
/*@Doc:

  \begin{verbatim}
    SELECT  stid,vafr,vato,days,shfr,shto,
    FROM    shfcki
  \end{verbatim}
*/
struct OptDataStruct 
{
    // Data fields from table OPTCKI for whatif-rows
	long		Urno;				// Unique Record Number of OPTCKI
	char		Appn[22];		// name of application
	char		Ctyp[12];		// Type of Row; in Whaif constant string "WHAT-IF"
	char		Ckey[34];		// Name of what-if row
	char		Erst[22];		// Creator
	CTime		Erda;				// Date of creation
	char		Para[42];		// paraset
	CTime		Vafr;				// Valid from
	CTime		Vato;				// Valid to
	CTime		Expr;				// Expire at
	char		Text[252];		// Comment
	char		Lis1[514];		// commaseparated first list
	char		Lis2[514];		// commaseparated second list
	char		Ftxt[82];		// Freetext-command
	int		Kind;
	int		Mafm;
	int		Aufm;
	int		Mapo;
	int		Aupo;
	int		Mafl;
	int		Aufl;
	int		Macc;
	int		Aucc;
	int		Mabr;
	int		Aubr;
	int		Mazb;
	int		Auzb;
	int		Smrt;
	int		Saop;
	int		Cemf;
	int		Cfmf;
	int		Sdis;
	int		Shis;
	int		Sjmp;
	int		Swol;
	int		Svmf;
	int		Sbre;
	int		Squa;
	int		Sdtl;
	int		Stec;
	int		Solp;
	int		Srmx;
	int		Spra;
	int		Snea;
	char		Peno[12];		// Staff-/User-ID
	int		IsChanged;		// Is changed flag

	OptDataStruct(void) 
	{ memset(this,'\0',sizeof(*this));
	  strcpy(Appn,pcgAppName);
	  IsChanged=DATA_UNCHANGED;Mafm=0;Aufm=0;Mapo=0;Aupo=0;
	  Mafl=0;Aufl=0;Macc=0;Aucc=0;Mabr=0;Aubr=0;Mazb=0;Auzb=0;Auzb=0;Smrt=0;Saop=0;Cemf=0;
	  Cfmf=0;Sdis=0;Shis=0;Sjmp=0;Swol=0;Svmf=0;Sbre=0;Squa=0;Sdtl=0;Stec=0;Solp=0;Srmx=0;
	  Spra=0;Snea=0;}
};	

typedef struct OptDataStruct OPTDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Class for handling peak data
//@See: CCSCedaData, CedaJobData, PrePlanTable
/*@Doc:
  Reads and writes shift data from/to database and stores it in memory. 
  The shift data class will be used in {\bf ShiftTable} table and the 
  {\bf PrePlanTable}.

*/
class CedaOptData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: OPTDATA records read by ReadShiftData()
    CCSPtrArray<OPTDATA> omData;
    CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);

    //@ManMemo: A map, containing the PKNO fields of all loaded employees.
// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf SHIFTDATA},
      the {\bf pcmTableName} with table name {\bf STFCKI}, the data members {\bf omFieldList}
      contains a list of used fields of database view {\bf STFCKI}.
    */
    CedaOptData();
	//CedaOptData(CString opTableType);
    //@ManMemo: Destructor, Unregisters the CedaShiftData object from DataDistribution
	~CedaOptData();

    //@ManMemo: Read all shift types from database at program start
	CCSReturnCode	ReadOptData();
	CCSReturnCode	AddOpt(OPTDATA * prpOpt);
	CCSReturnCode	ChangeOpt(OPTDATA * prpOpt);
	CCSReturnCode	AddOptInternal(OPTDATA * prpOpt);
	CCSReturnCode	SaveOpt(OPTDATA * prpOpt);
	CCSReturnCode	SaveAllOpts(void);
	CCSReturnCode	DeleteOpt(long lpUrno);
	CCSReturnCode	UseOpt(long lpUrno);
	OPTDATA			*GetOptByUrno(long lpUrno);
	long				GetUrnoById(char *pclWiid);
	CCSReturnCode CedaOptData::CreateCkoRequest(OPTDATA * prpOpt);

	void Add(OPTDATA& rrlOptData);
	void ProcessOptBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

public:
	int imOptCount;
	int imInfoCount;
	CString omTableType;


private:

};


extern CedaOptData ogOptData;
//extern CedaOptData ogInfos;

#endif

