// FilterPage.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <FilterPage.h>
#include <Ccsglobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FilterPage property page

IMPLEMENT_DYNCREATE(FilterPage, CPropertyPage)

FilterPage::FilterPage() : CPropertyPage(FilterPage::IDD)
{
	//{{AFX_DATA_INIT(FilterPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	bmSelectAllEnabled = false;
	omAllString = GetString(IDS_ALLSTRING);
	CString olTitle = GetString(IDS_STRING32945);
	SetCaption(olTitle);
}

FilterPage::~FilterPage()
{
}

void FilterPage::SetCaption(const char *pcpCaption)
{
	memset(pcmCaption,0,100);
	strcpy(pcmCaption,pcpCaption);
	m_psp.pszTitle = pcmCaption;
	m_psp.dwFlags |= PSP_USETITLE;
}

void FilterPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FilterPage)
	DDX_Control(pDX, IDC_LIST2, m_List2);
	DDX_Control(pDX, IDC_LIST1, m_List1);
	DDX_Control(pDX, IDC_COMBO1, m_ComboBox1);
	//}}AFX_DATA_MAP

	// Extended data exchange -- for member variables with Control type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		int i;

		// Do not update window while processing new data
		m_List1.SetRedraw(FALSE);
		m_List2.SetRedraw(FALSE);

		// Initialize all possible items into the left list box
		/*	hag
		m_List1.ResetContent();
		for (i = 0; i < omPossibleItems.GetSize(); i++)
			m_List1.AddString(omPossibleItems[i]);

		// Initialize the selected items into the right list box,
		// also remove the corresponding items from the left list box.
		m_List2.ResetContent();
		for (i = 0; i < omSelectedItems.GetSize(); i++)
		{
			m_List2.AddString(omSelectedItems[i]);
			m_List1.DeleteString(m_List1.FindStringExact(-1, omSelectedItems[i]));
		}
		*/
		m_List1.ResetContent();
		m_List2.ResetContent();
		bool blAllLeft = false, blAllRight = false;
		if ( bmSelectAllEnabled )
		{
			blAllRight = (omSelectedItems.GetSize() > 0 && omSelectedItems[0] == omAllString);
			blAllLeft = !blAllRight;
		}
		if ( blAllRight )
			m_List2.AddString(omAllString);
		else
		{
			if ( blAllLeft )
				m_List1.AddString(omAllString);
			for (i = 0; i < omPossibleItems.GetSize(); i++)
				m_List1.AddString(omPossibleItems[i]);

			m_List2.ResetContent();
			for (i = 0; i < omSelectedItems.GetSize(); i++)
			{
				m_List2.AddString(omSelectedItems[i]);
				m_List1.DeleteString(m_List1.FindStringExact(-1, omSelectedItems[i]));
			}
		}
		// Update the window according to the new data
		m_List1.SetRedraw(TRUE);
		m_List2.SetRedraw(TRUE);
	}

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		// Copy data from the list box back to the array of string
		omSelectedItems.SetSize(m_List2.GetCount());
		for (int i = 0; i < omSelectedItems.GetSize(); i++)
			m_List2.GetText(i, omSelectedItems[i]);
	}
}


BEGIN_MESSAGE_MAP(FilterPage, CPropertyPage)
	//{{AFX_MSG_MAP(FilterPage)
	ON_BN_CLICKED(IDC_FILTERADD, OnFilterAdd)
	ON_BN_CLICKED(IDC_FILTERREMOVE, OnFilterRemove)
	ON_BN_CLICKED(IDC_FILTERADDALL, OnFilterAddAll)
	ON_BN_CLICKED(IDC_FILTERREMOVEALL, OnFilterRemoveAll)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FilterPage message handlers

BOOL FilterPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	if (HIWORD(wParam) == BN_CLICKED)
		CancelToClose();
	return CPropertyPage::OnCommand(wParam, lParam);
}

void FilterPage::OnFilterAdd() 
{


	
	// Move selected items from left list box to right list box
	for (int i = m_List1.GetCount()-1; i >= 0; i--)
	{
		if (!m_List1.GetSel(i))	// unselected item?
			continue;
		CString s;
		m_List1.GetText(i, s);	// load string to "s"
		if ( s == omAllString )
		{
			m_List2.ResetContent(); 
			m_List1.ResetContent(); 
			m_List2.AddString(s);	// move string from left to right box
			break;	//  fertig, mehr als alle geht nicht
		}
		m_List2.AddString(s);	// move string from left to right box
		m_List1.DeleteString(i);
	}
}

void FilterPage::OnFilterAddAll() 
{
	// Move selected items from left list box to right list box
	for (int i = m_List1.GetCount()-1; i >= 0; i--)
	{
		CString s;
		m_List1.GetText(i, s);	// load string to "s"
		m_List2.AddString(s);	// move string from left to right box
		m_List1.DeleteString(i);
	}
}

void FilterPage::OnFilterRemove() 
{
	// Move selected items from right list box to left list box
	for (int i = m_List2.GetCount()-1; i >= 0; i--)
	{
		if (!m_List2.GetSel(i))	// unselected item?
			continue;
		CString s;
		m_List2.GetText(i, s);	// load string to "s"
		if ( s == omAllString )
		{
			m_List2.ResetContent(); 
			m_List1.ResetContent(); 
			m_List1.AddString(s);	// move string from left to right box
			for (int j = 0; j < omPossibleItems.GetSize(); j++)
				m_List1.AddString(omPossibleItems[j]);
			break;	//  fertig, mehr als alle geht nicht
		}
		m_List1.AddString(s);	// move string from right to left box
		m_List2.DeleteString(i);
	}
}

void FilterPage::OnFilterRemoveAll() 
{
	// Move selected items from right list box to left list box
	for (int i = m_List2.GetCount()-1; i >= 0; i--)
	{
		CString s;
		m_List2.GetText(i, s);	// load string to "s"
		m_List1.AddString(s);	// move string from right to left box
		m_List2.DeleteString(i);
	}
}

BOOL FilterPage::OnInitDialog() 
{
	bmGroupEnabled = TRUE;

	if(!bmGroupEnabled)	
	{
		GetDlgItem(IDC_COMBO1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_FILTERGRPADD)->ShowWindow(SW_HIDE);

		CRect olRect;
		GetDlgItem(IDC_LIST1)->GetWindowRect(olRect);
		olRect.SetRect(olRect.left, olRect.top, olRect.right, olRect.bottom+20);
		ScreenToClient(olRect);
		GetDlgItem(IDC_LIST1)->MoveWindow(olRect);

		GetDlgItem(IDC_LIST2)->GetWindowRect(olRect);
		olRect.SetRect(olRect.left, olRect.top, olRect.right, olRect.bottom+20);
		ScreenToClient(olRect);
		GetDlgItem(IDC_LIST2)->MoveWindow(olRect);		
	}


	CPropertyPage::OnInitDialog();
	
//	SetWindowText(GetString(IDS_STRING32945));
	CWnd *polWnd = GetDlgItem(IDC_FILTERADD); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32946));
	}
	polWnd = GetDlgItem(IDC_FILTERREMOVE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32947));
	}
	// TODO: Add extra initialization here
	
    


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

