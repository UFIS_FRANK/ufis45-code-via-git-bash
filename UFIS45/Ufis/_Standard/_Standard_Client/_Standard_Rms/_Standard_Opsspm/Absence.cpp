// Absence.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <Absence.h>
#include <CedaOdaData.h>
#include <CCSCedaData.h>
#include<vector>
#include <OpssPm.h>
#include <BasicData.h>
#include <CedaEqaData.h>
#include <CedaDraData.h>


using namespace std;
//#include <CedaDraData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern CTime from , to;
extern vector<CTime> currform,currto;
extern vector<CString> code, remark;
extern vector<long> URNO;

extern CString GetString(UINT ipStringId);
/////////////////////////////////////////////////////////////////////////////
// Absence dialog


Absence::Absence(CWnd* pParent /*=NULL*/)
	: CDialog(Absence::IDD, pParent)
{
	//{{AFX_DATA_INIT(Absence)
	//}}AFX_DATA_INIT
}


void Absence::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Absence)
	DDX_Control(pDX, IDC_EDIT9, m_rema);
	DDX_Control(pDX, IDC_LIST1, m_list);
	DDX_Control(pDX, IDC_EDIT3, m_edit4);
	DDX_Control(pDX, IDC_EDIT10, m_edit3);
	DDX_Control(pDX, IDC_EDIT2, m_edit2);
	DDX_Control(pDX, IDC_EDIT1, m_edit1);
	DDX_Control(pDX, IDC_COMBO1, m_com);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Absence, CDialog)
	//{{AFX_MSG_MAP(Absence)
	ON_WM_CTLCOLOR()
	ON_LBN_SELCHANGE(IDC_LIST1, OnSelchangeList1)
	ON_BN_CLICKED(IDC_BUTTON1, OnDel)
	ON_BN_CLICKED(IDC_BUTTON2, OnUpdate)
	ON_LBN_DBLCLK(IDC_LIST1, OnDblclkList1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Absence message handlers

BOOL Absence::OnInitDialog() 
{

	CDialog::OnInitDialog();
	CString olFormat;
	//CedaOdaData	ogOdaData;
	int ilOda = ogOdaData.omData.GetSize();

	
    for (int i = 0; i < ilOda; i++)
	{
		ODADATA *prlOda = &ogOdaData.omData[i];  
		
		if (strcmp(prlOda->Sdac,"003")==0||strcmp(prlOda->Sdac,"008")==0||strcmp(prlOda->Sdac,"011")==0||
			strcmp(prlOda->Sdac,"013")==0||strcmp(prlOda->Sdac,"022")==0||strcmp(prlOda->Sdac,"027")==0||
			strcmp(prlOda->Sdac,"028")==0||strcmp(prlOda->Sdac,"030")==0||strcmp(prlOda->Sdac,"041")==0||
			strcmp(prlOda->Sdac,"051")==0||strcmp(prlOda->Sdac,"054")==0||strcmp(prlOda->Sdac,"055")==0||
			strcmp(prlOda->Sdac,"069")==0||strcmp(prlOda->Sdac,"070")==0||strcmp(prlOda->Sdac,"071")==0
			)
	    	continue;
		
		olFormat.Format("%s                           %s",prlOda->Sdac,prlOda->Sdan);
		m_com.AddString(olFormat);
	}

    
	
	
	CString strfrom;
	strfrom=from.Format("%d.%m.%Y");
	CString strto;
	strto=to.Format("%d.%m.%Y");
	m_edit1.SetWindowText(strfrom);
	m_edit3.SetWindowText(strto);

	CString fromtime, totime;
	fromtime=from.Format("%H:%M");
	totime=to.Format("%H:%M");
	m_edit2.SetWindowText(fromtime);
	m_edit4.SetWindowText(totime);




	  for (int g=0;g<currto.size();g++)      	//for (int g=currto.size()-1;g>=0;g--)
	 {
	
	    CString curfrom =currform[g].Format("%d.%m.%Y  %H:%M");
		CString curto =currto[g].Format("%d.%m.%Y  %H:%M");
		
    	code[g].TrimRight();
		if (code[g].GetLength()==3)
			m_list.AddString(" "+code[g]+"      "+curfrom+"       "+curto+"       "+remark[g]);
		else if (code[g].GetLength()==4)
	        m_list.AddString(" "+code[g]+"   "+curfrom+"       "+curto+"       "+remark[g]);
		else if (code[g].GetLength()==2)
			m_list.AddString(" "+code[g]+"      "+curfrom+"       "+curto+"       "+remark[g]);

	
 
	 }


  
	  
	  
	  
	  
	  
	  
	  
	  GetDlgItem(IDC_EDIT11)->EnableWindow(false);
    GetDlgItem(IDC_EDIT12)->EnableWindow(false);
	 GetDlgItem(IDC_EDIT4)->EnableWindow(false);
	  GetDlgItem(IDC_EDIT5)->EnableWindow(false);
	   GetDlgItem(IDC_BUTTON2)->EnableWindow(false);
	   GetDlgItem(IDC_BUTTON1)->EnableWindow(false);
	   
	   
	   
	   SetWindowText(GetString(IDS_61932));
	  
	   
	   SetDlgItemText(IDC_STATIC1,GetString(IDS_61933));
SetDlgItemText(IDC_STATIC3,GetString(IDS_61934));
SetDlgItemText(IDC_STATIC4,GetString(IDS_61935));
SetDlgItemText(IDC_STATIC5,GetString(IDS_61936));
SetDlgItemText(IDC_STATIC6,GetString(IDS_61937));
SetDlgItemText(IDC_STATIC7,GetString(IDS_61938));
SetDlgItemText(IDC_STATIC8,GetString(IDS_61939));
SetDlgItemText(IDC_STATIC9,GetString(IDS_61940));
SetDlgItemText(IDC_STATIC10,GetString(IDS_61943));


SetDlgItemText(IDOK,GetString(IDS_61941));
SetDlgItemText(IDCANCEL,GetString(IDS_61942));

	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

COLORREF d=RGB(255,255,0);
COLORREF font=RGB(0,0,0);
HBRUSH Absence::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
	if ( pWnd->GetDlgCtrlID()==IDC_EDIT1 || pWnd->GetDlgCtrlID()==IDC_EDIT2 || pWnd->GetDlgCtrlID()==IDC_EDIT3 || pWnd->GetDlgCtrlID()==IDC_EDIT10)
	{
		pDC->SetBkMode(TRANSPARENT);
		
	//	pDC->SetTextColor(font);
		HBRUSH B = CreateSolidBrush(d);
		return (HBRUSH) B;
	}		
	// TODO: Return a different brush if the default is not desired
	return hbr;
}


CString str1,str2,str3,str4,str5,str6;
CTime insertFrom, insertTo;
CString insertRema, sdac;
long bsdu;


void Absence::OnOK() 
{

	m_edit1.GetWindowText(str1);m_edit2.GetWindowText(str2);m_edit3.GetWindowText(str3);m_edit4.GetWindowText(str4);
	 //int pos =m_list.GetCurSel();
   
    str1.TrimRight();
	str2.TrimRight();
    str3.TrimRight();
	str4.TrimRight();

	
	if (str1.GetLength()!=10||str1[0]>'9'||str1[0]<'0'||str1[1]>'9'||str1[1]<'0'||str1[3]>'9'||str1[3]<'0'||
		str1[4]>'9'||str1[4]<'0'||str1[6]>'9'||str1[6]<'0'||str1[7]>'9'||str1[7]<'0'||
		str1[8]>'9'||str1[8]<'0'||str1[9]>'9'||str1[9]<'0'||str1[2]!='.'||str1[5]!='.' )
	{
		AfxMessageBox(GetString(IDS_61945),MB_OK|MB_ICONINFORMATION);
		m_edit1.SetFocus();

		return;
	}
	
    if (str3.GetLength()!=10||str3[0]>'9'||str3[0]<'0'||str3[1]>'9'||str3[1]<'0'||str3[3]>'9'||str3[3]<'0'||
		str3[4]>'9'||str3[4]<'0'||str3[6]>'9'||str3[6]<'0'||str3[7]>'9'||str3[7]<'0'||
		str3[8]>'9'||str3[8]<'0'||str3[9]>'9'||str3[9]<'0'||str3[2]!='.'||str3[5]!='.' )
	{
		AfxMessageBox(GetString(IDS_61946),MB_OK|MB_ICONINFORMATION);
		m_edit3.SetFocus();
		return;
	}
	
   
	
	if (str2.GetLength()==4)
	{
	    if (str2[0]>'9'||str2[0]<'0'||str2[1]>'9'||str2[1]<'0'||str2[2]>'9'||str2[2]<'0'||
		str2[3]>'9'||str2[3]<'0' )
		{
	    	AfxMessageBox(GetString(IDS_61947),MB_OK|MB_ICONINFORMATION);
		    m_edit2.SetFocus();
		    return;
		}

	     else str2.Insert(2,':');
	
	}
	
		
	if (str2.GetLength()==5) {
	
	if (str2[0]>'9'||str2[0]<'0'||str2[1]>'9'||str2[1]<'0'||str2[3]>'9'||str2[3]<'0'||
		str2[4]>'9'||str2[4]<'0'||str2[2]!=':' )
	{
		AfxMessageBox(GetString(IDS_61947),MB_OK|MB_ICONINFORMATION);
		m_edit2.SetFocus();
		return;
	}

  
	}
	
	if (str2.GetLength()!=5 && str2.GetLength()!=4) {
	
	
		AfxMessageBox(GetString(IDS_61947),MB_OK|MB_ICONINFORMATION);
		m_edit2.SetFocus();
		return;
	  
	}
	
	
	if (str4.GetLength()==4)
	{
	    if (str4[0]>'9'||str4[0]<'0'||str4[1]>'9'||str4[1]<'0'||str4[2]>'9'||str4[2]<'0'||
		str4[3]>'9'||str4[3]<'0' )
		{
	    	AfxMessageBox(GetString(IDS_61948),MB_OK|MB_ICONINFORMATION);
		    m_edit4.SetFocus();
		    return;
		}

	     else str4.Insert(2,':');
	
	}
	
	
	
	
	if (str4.GetLength()==5) {
	
	
	if (str4[0]>'9'||str4[0]<'0'||str4[1]>'9'||str4[1]<'0'||str4[3]>'9'||str4[3]<'0'||
		str4[4]>'9'||str4[4]<'0'||str4[2]!=':' )
	{
		AfxMessageBox(GetString(IDS_61948),MB_OK|MB_ICONINFORMATION);
		m_edit4.SetFocus();
		return;
	}
	}

    if (str4.GetLength()!=5 && str4.GetLength()!=4) {
		
		AfxMessageBox(GetString(IDS_61948),MB_OK|MB_ICONINFORMATION);
		m_edit4.SetFocus();
		return;
	  
	}



    CString strfrom=str1+" "+str2;
	
	 int   nYear,   nMonth,   nDate,   nHour,   nMin;  
     sscanf(strfrom,   "%d.%d.%d %d:%d",   &nDate, &nMonth, &nYear,   &nHour,   &nMin);  
     
	 
    if (nMonth>12||nMonth<1||nDate>31||nDate<1) 
	{
		AfxMessageBox(GetString(IDS_61945),MB_OK|MB_ICONINFORMATION);
		m_edit1.SetFocus();
		return;
	}

	else if (nHour>23||nHour<0||nMin>59||nMin<0)
	{
		AfxMessageBox(GetString(IDS_61947),MB_OK|MB_ICONINFORMATION);
		m_edit2.SetFocus();
		return;
	}

    CTime   abnfrom(nYear,   nMonth,   nDate,   nHour,   nMin,   00);



    CString strto=str3+" "+str4;
	sscanf(strto,   "%d.%d.%d %d:%d",   &nDate, &nMonth, &nYear,   &nHour,   &nMin);  
     
	 
    if (nMonth>12||nMonth<1||nDate>31||nDate<1) 
	{
		AfxMessageBox(GetString(IDS_61945),MB_OK|MB_ICONINFORMATION);
		m_edit1.SetFocus();
		return;
	}

	else if (nHour>23||nHour<0||nMin>59||nMin<0)
	{
		AfxMessageBox(GetString(IDS_61947),MB_OK|MB_ICONINFORMATION);
		m_edit2.SetFocus();
		return;
	}

	CTime  abnto(nYear,   nMonth,   nDate,   nHour,   nMin,   00);
	 
		
	if (abnto<=abnfrom)
	  
	{
	   AfxMessageBox(GetString(IDS_61949),MB_OK|MB_ICONINFORMATION);
	   return;
		
	}
	  
	
	if (abnfrom<from || abnfrom>=to)
	{
     	AfxMessageBox(GetString(IDS_61950),MB_OK|MB_ICONINFORMATION);
	    return;
	}
	
	
	if (abnto<=from || abnto>to)
	{
     	AfxMessageBox(GetString(IDS_61951),MB_OK|MB_ICONINFORMATION);
	    return;
	}
		

	for (int j=0;j<currto.size();j++)
	{
  
	     if (abnfrom>currform[j] && abnfrom<currto[j])
		 {
		   AfxMessageBox(GetString(IDS_61952),MB_OK|MB_ICONINFORMATION);
	       return;
		 }
	     if (abnto>currform[j] && abnto<currto[j])
		 {
		   AfxMessageBox(GetString(IDS_61953),MB_OK|MB_ICONINFORMATION);
	       return;
		 }

         if (abnto>=currto[j] && abnfrom<=currform[j])
		 {
		   AfxMessageBox(GetString(IDS_61954),MB_OK|MB_ICONINFORMATION);
	       return;
		 }

	}


	((CComboBox*)GetDlgItem(IDC_COMBO1))->GetWindowText(str5);
		  
	  if (strcmp(str5,"")==0) 
	{
      	AfxMessageBox(GetString(IDS_61955),MB_OK|MB_ICONINFORMATION);
		return;
 
	}
	
	  insertFrom=abnfrom;  insertTo=abnto; 
      m_rema.GetWindowText(insertRema);

      
     
	  str6.Format("%c%c%c%c",str5[0],str5[1],str5[2],str5[3]);
	  sdac=str6;
	  
	   	  
	  
	  for (int y=0;y<1000;y++)
	 {
	   ODADATA *prlOda = &ogOdaData.omData[y];

       if (strlen(prlOda->Sdac)==2)
	   {
	   
	    if (prlOda->Sdac[0]==str6[0] && prlOda->Sdac[1]==str6[1]  )
		{
	      bsdu=prlOda->Urno;
		  break;
		}
	   
	   }



	   
	   if (prlOda->Sdac[0]==str6[0] && prlOda->Sdac[1]==str6[1] &&prlOda->Sdac[2]==str6[2] )
	   {
	      bsdu=prlOda->Urno;
		  break;
	   }


 	    
	 }



   CDialog::OnOK();

}

void Absence::OnSelchangeList1() 
{

   	
  
	
	
	
	GetDlgItem(IDC_BUTTON1)->EnableWindow(true);

   
  
  
  
    GetDlgItem(IDC_EDIT11)->EnableWindow(true);
    GetDlgItem(IDC_EDIT12)->EnableWindow(true);
	GetDlgItem(IDC_EDIT4)->EnableWindow(true);
	GetDlgItem(IDC_EDIT5)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON2)->EnableWindow(true);
	
	
   //CString curfrom =currform[m_list.GetCurSel()].Format("%d.%m.%Y  %H:%M");
		
	  CString curfromdate =currform[m_list.GetCurSel()].Format("%d.%m.%Y");
	  CString curfromtime =currform[m_list.GetCurSel()].Format("%H:%M");
	  CString curtodate =currto[m_list.GetCurSel()].Format("%d.%m.%Y");
      CString curtotime =currto[m_list.GetCurSel()].Format("%H:%M");

	  
	  GetDlgItem(IDC_EDIT11)->SetWindowText(curfromdate);
      GetDlgItem(IDC_EDIT12)->SetWindowText(curtodate);
      GetDlgItem(IDC_EDIT4)->SetWindowText(curfromtime);
      GetDlgItem(IDC_EDIT5)->SetWindowText(curtotime);


}

void Absence::OnDel() 
{
    CedaDraData ogDraData11;
	if (m_list.GetCurSel()==-1) return;

	else
	{


	  if (MessageBox("Are you sure you want to delete this record?","OPSS-PM", MB_ICONQUESTION|MB_YESNO)==IDYES)
	   {
     
		  ogDraData11.DeleteTemp(URNO[m_list.GetCurSel()]);
	//	  m_list.DeleteString (m_list.GetCurSel());

	     //GetDlgItem(IDC_BUTTON1)->EnableWindow(false);
		 
			SendMessage(WM_CLOSE);
	   }


	}

// TODO: Add your control notification handler code here
	
}

void Absence::OnUpdate() 
{
	CString fromdate, fromtime, todate, totime;
	
	GetDlgItem(IDC_EDIT11)->GetWindowText(fromdate);
    GetDlgItem(IDC_EDIT4)->GetWindowText(fromtime);
	GetDlgItem(IDC_EDIT12)->GetWindowText(todate);
	GetDlgItem(IDC_EDIT5)->GetWindowText(totime);
	
	CString newfrom =fromdate+" "+fromtime;
	CString newto   =todate+" "+totime;

    int   nYear,   nMonth,   nDate,   nHour,   nMin;  
    sscanf(newfrom,   "%d.%d.%d %d:%d",   &nDate, &nMonth, &nYear,   &nHour,   &nMin);  
    CTime   abnfrom(nYear,   nMonth,   nDate,   nHour,   nMin,   00);
    sscanf(newto,   "%d.%d.%d %d:%d",   &nDate, &nMonth, &nYear,   &nHour,   &nMin);  
    CTime   abnto(nYear,   nMonth,   nDate,   nHour,   nMin,   00);

    
    CedaDraData ogDraData22;
    
	
	//ogDraData22.UpdateTemp(URNO[m_list.GetCurSel()],abnfrom,abnto);

     DRADATA p=  ogDraData22.ReadDraByUrno(URNO[m_list.GetCurSel()]);



    p.Abfr=abnfrom;
	p.Abto=abnto;

	int a= m_list.GetCount();
	

    //strcpy(p.Drrn,"2");

	



    ogDraData22.DeleteTemp(URNO[m_list.GetCurSel()]);

    ogDraData22.InsertDraRecord(p);


	SendMessage(WM_CLOSE);
	
	
	
	

}

void Absence::OnDblclkList1() 
{
	
	
   


	
}
