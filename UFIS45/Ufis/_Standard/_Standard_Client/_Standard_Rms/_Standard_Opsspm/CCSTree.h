// CCSTree.h : header file
//
#ifndef _CCSTree_H_
#define _CCSTree_H_

#define bool BOOL
#define false FALSE
#define true TRUE



#include <stdafx.h>
#include <ccsglobl.h>
#include <afxtempl.h>
#include <CCSDragDropCtrl.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// CCSTree window


//@Man:
//@Memo: Display data as a tree of (limited) textlines 
//@See: MFC's CWnd, MFC's CTreeCtrl
/*Doc
  The class CCSTree displays trees of textlines. The apperance of such a CCSTable
  field had already shown in the SYSTAB-editor. The CCSTree class contains these
  following graphical elements:

  \begin{itemize}
  \item The windows-setting are used for the Background, Textfont and Textcolor (you can't custmize)
  \item Buttons on the left side of the text and lines to root and childitems
  \item Bitmaps between buttons and text (optional)
  \item Vertical scroll bar will be displayed when the number of Items is larger than
        the viewing area.
  \item Horizontal scroll bar will be displayed when a textline from one Item is larger
        than the viewing area.


  \item The surrounding window can call the SetPosition(..) member function with appropriate
        parameters to adjust the table window to the new size and new position.
  \end{itemize}    

*/


// a pointer to this Notifystruct contains lParam from the CCSTree-Messages 


struct TREEITEM;
struct CCSTREENOTIFY
{
	CWnd *SourceTree;
	TREEITEM *Item;
	bool Focus;
	bool Selected;
	void *Data;
	CPoint Point;
};

class  CCSTree;


// use this struct in your viewer
struct TREEITEM
{
	CString		Text;
	TREEITEM	*Parent;
	int			ItemType;
	void		*pVData;				
	CCSPtrArray<TREEITEM> ChildItems;
	TREEITEM()
	{
		ItemType	= -1;
		Text		= "";
		Parent		= NULL;
		pVData		= NULL;
	}
	TREEITEM(TREEITEM *prpTI)
	{
		ItemType	= prpTI->ItemType;
		Text		= prpTI->Text;
		Parent		= prpTI->Parent;
		pVData		= prpTI->pVData;
	}
};

////////////////////////////////////////////////////////////////////////////////////////////
// this two structs are used in the Class CCSTree -> do NOT use this structs in your viewer
////////////////////////////////////////////////////////////////////////////////////////////
struct TREEITEMDEF	
{
	int			ItemType;
	int			TextLimit;
	bool		Bold;
	bool		Edit;
	int			Image;
	int			SelImage;
	bool		MultiSel;
	TREEITEMDEF()
	{
		ItemType	= -1;
		TextLimit	= 10;
		Bold		= false;
		Edit		= false;
		Image		= -1;
		SelImage	= -1;
		MultiSel	= false;
	}
};

struct CCSTREEITEM : public TREEITEMDEF //used CCSTree-Internal
{
	HTREEITEM	hItem;
	CCSTREEITEM	*Parent;
	void		*pData;
	CCSPtrArray<CCSTREEITEM> ChildItems;
	CCSTREEITEM()
	{
		hItem		= NULL;
		Parent		= NULL;
		pData		= NULL;
	}
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


class CCSTree : public CTreeCtrl
{
// Construction
public:
    /*@ManDoc:
      Creates an empty CCSTree object.
    */
	CCSTree(CWnd *popParentWindow = NULL);

    /*@ManDoc:
      Creates CCSTree object. Initializes data members with data passed as parameters.
      It will not create or display the window tree, this should be dont by the
      SetTreeData(..) method only. The calling function is responsible for passing valid
      data. See also: CCSTree::SetTreeData().
    */

	CCSTree(CWnd *popParentWindow, int ipXStart, int ipXEnd, int ipYStart, int ipYEnd, int ipCXBitmap = 0,  int ipCYBitmap = 0, bool bpMultiSelection = false);
	
	virtual	~CCSTree();

	bool	SetTreeData(CWnd *popParentWindow, int ipXStart, int ipXEnd, int ipYStart, int ipYEnd, int ipCXBitmap = 0,  int ipCYBitmap = 0, bool bpMultiSelection = false);
    bool	SetPosition(int ipXStart, int ipXEnd, int ipYStart, int ipYEnd);

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSTree)
	//}}AFX_VIRTUAL

// Implementation
public:

	void	SetMultiSelect(bool bpMultiSelect = true);
	void	SetLabelEdit(bool bpLabelEdit = true);

	bool	SetTreeItemDef(int	ipItemType, UINT ipBitmapID, UINT ipSelBitmapID, int ipTextLimit = 32, bool bpEdit = false, bool bpMultiSel = false, bool bpBold = false);

	bool	InsertItem(TREEITEM *prpTI, TREEITEM *prpTIAfter = NULL, bool bpSelect = false);
	bool	InsertItem(TREEITEM *prpTI, TREEITEMDEF *prpTIDef, TREEITEM *prpTIAfter = NULL, bool bpSelect = false);

	bool	DeleteItem(TREEITEM *prpTI);

    //@ManMemo: Use this funktion to change the atributes and/or the position of the item
	bool	ChangeItem(TREEITEM *prpTI, TREEITEMDEF *prpTIDef,TREEITEM *prpTIAfter, bool bpSelect);
    //@ManMemo: Use this funktion to change the atributes and not the position (the Parent must be valid)
	bool	ChangeItem(TREEITEM *prpTI, TREEITEMDEF *prpTIDef = NULL, bool bpSelect = false);

     //@ManMemo: Remove all items from the table content.
    bool	ResetContent();

     //@ManMemo: Insert a tree in a tree
	bool	InsertTree(TREEITEM *prpTI, TREEITEM *prpTIAfter);
	bool	InsertTree(CCSPtrArray<TREEITEM> prpTItems, TREEITEM *prpTIAfter);
	
    //@ManMemo: Returns the TREEITEM pointer of the item that has the focus rectangle.
	TREEITEM	*GetFocusItem();

	bool		SelectItem(TREEITEM *prpTI, bool bpFocus = false);


	int			GetLevel(TREEITEM *prpTI = NULL);
	bool		TestItemPos(TREEITEM *prpTI, TREEITEM *prpTIAfter);
	bool		TestItemParent(TREEITEM *prpTI);

	int			GetCountSelItems();
					
	TREEITEM	*GetSelectedItem(int ipIndex = 0);
	void		DeSelectAll() ;

	//Sets the current background color to the specified color.
	void		SetBKColor(COLORREF opColor);

	//Sets the current text color to the specified color.
	void		SetTextColor(COLORREF opColor);

	// dedug
	void		TraceTree();

protected:	
	TREEITEMDEF	*GetTreeItemDef(int ipItemType);
	int			GetImageByDef(int ipItemType);
	int			GetSelImageByDef(int ipItemType);

public:


// Attributes
public:

protected:
	CCSPtrArray<TREEITEMDEF> omTreeItemDefs;
	CCSPtrArray<CCSTREEITEM> omRootItems;
	CMapPtrToPtr omSelItems;
	CMapPtrToPtr omPtrMap;
	CImageList omImageList;
	CWnd *pomParentWindow;
    int imXStart, imXEnd;
    int imYStart, imYEnd;
	int imCXBitmap, imCYBitmap;
	bool bmHasButtons;
	CPoint	omPoint;	
	bool	bmDeleteMode;
	bool bmMultiSelection;
	COLORREF omBKColor;
	COLORREF omTextColor;
	

	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;


// Generated message map functions
protected:
	//{{AFX_MSG(CCSTree)
	afx_msg void OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBeginlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSelchanging(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemexpanded(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemexpanding(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
#endif
