#ifndef __FLIGHTJOBSTABLEVIEWER_H__
#define __FLIGHTJOBSTABLEVIEWER_H__

#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaFlightData.h>

#include <cviewer.h>
#include <table.h>
#include <ccsprint.h>

struct FLIGHTJOBSTABLE_JOB
{
	long JobUrno;
	CString Name;
	CTime Acfr;
	CTime Acto;
};

struct FLIGHTJOBSTABLE_LINE	// each line represents one flight (no rotation or turnaround)
{
	long FlightUrno;
	int ReturnFlightType;
	CString Fnum;
	CString Adid;
	CString Regn;
    CString Act3;
	CTime FlightTime;
	CTime FlightActualTime;
	char TimeStatus[2];
	CCSPtrArray <FLIGHTJOBSTABLE_JOB> Jobs;

	FLIGHTJOBSTABLE_LINE(void)
	{
		ReturnFlightType = ID_NOT_RETURNFLIGHT;
	}
};


////////////////////////////////////// EXPORT STRUCTURES
typedef struct
{
	CStringArray Functions;
	CString Title;

} EXPORT_COLUMN_INFO;

typedef struct
{
	CString Name;
	CString AssignedFunction;

} EXPORT_JOB;

typedef struct ExportLine
{
	CString WorkGroup;
	CString ShiftCode;
	CCSPtrArray <EXPORT_JOB> Jobs;
	EXPORT_JOB OTHER;

} EXPORT_LINE;

typedef struct ExportFlight
{
	CString Flight;
	CString DemType; // A=Arr D=Dep T=Turnrnd X=None
	CString Regn;
	CString Act3;
	CString Time;
	CString ActualTime;
	CString Tisa;

	CCSPtrArray <EXPORT_LINE> Lines;

	ExportFlight(void)
	{
		DemType = "X";
	}

} EXPORT_FLIGHT;
////////////////////////////////////// EXPORT STRUCTURES


class FlightJobsTableViewer: public CViewer
{
// Constructions
public:
    FlightJobsTableViewer();
    ~FlightJobsTableViewer();

    void Attach(CTable *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName, CString opDate);
    void ChangeViewTo(const char *pcpViewName);
	CTime StartTime() const;
	CTime EndTime() const;
	int	  Lines() const;
	FLIGHTJOBSTABLE_LINE*	GetLine(int ipLineNo);
	void  SetStartTime(CTime opStartTime);	
	void  SetEndTime(CTime opEndTime);
	int imColumns;
	bool bmUseAllAlcs;
	bool bmUseAllDetys;

private:
	static void FlightJobsTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
    void PrepareFilter();
	void PrepareSorter();

	BOOL IsPassFilter(FLIGHTDATA *prpFlight);
	BOOL IsPassFilter(JOBDATA *prpJob);
	int CompareFlight(FLIGHTJOBSTABLE_LINE *prpFlight1, FLIGHTJOBSTABLE_LINE *prpFlight2);
	long GetRotationUrnoForJob(JOBDATA *prpJob, long lpFlightUrno);

	int GetReturnFlightTypes(FLIGHTDATA *prpFlight, CUIntArray &ropReturnFlightTypes);
	void MakeLines();
	void MakeLine(FLIGHTDATA *prpFlight);
	void MakeLine(FLIGHTDATA *prpFlight, int ipReturnFlightType);
	void MakeJobs();
	BOOL FindFlight(long lpFlightUrno, int &ripLineno, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	BOOL FindJob(long lpJobUrno, int &ripLineno, int &ripJobno);

	void DeleteAll();
	int CreateLine(FLIGHTJOBSTABLE_LINE *prpFlightLine);
	void DeleteLine(int ipLineno);
	void CreateJob(int ipLineno, FLIGHTJOBSTABLE_JOB *prpJob);
	void CreateJob(FLIGHTJOBSTABLE_LINE &ropLine, FLIGHTJOBSTABLE_JOB *prpJob);
	void DeleteJob(int ipLineno, int ipJobno);
	void SetFlightData(FLIGHTJOBSTABLE_LINE &ropLine, FLIGHTDATA *prpFlight, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	void SetJobData(FLIGHTJOBSTABLE_LINE &ropLine);

	void UpdateDisplay();
	CString Format(FLIGHTJOBSTABLE_LINE *prpLine);

// Attributes used for filtering condition
private:
    int omGroupBy;              // enumerated value -- define in "flviewer.cpp" (use first sorting)
    CWordArray omSortOrder;
	CMapStringToPtr omCMapForDety;
	CMapStringToPtr omCMapForAlcd;

// Attributes
private:
    CTable *pomTable;
    CCSPtrArray<FLIGHTJOBSTABLE_LINE> omLines;
	CTime omStartTime;
	CTime omEndTime;
    CString omDate;
	bool	bmTime1Ascending;
	bool	bmTime2Ascending;

// DDX callback function processing
private:
	void ProcessJobNew(JOBDATA *prlJob, bool bpUpdate = false);
	void ProcessJobDelete(JOBDATA *prlJob, bool bpUpdate = false);
	void ProcessJobChanges(int ipDDXType,JOBDATA *prpJob);
	void ProcessFlightChanges(int ipDDXType,FLIGHTDATA *prpFlight);
	void ProcessFlightSelect(FLIGHTDATA *prpFlight);

// Printing functions
private:
	BOOL PrintFlightJobsTableLine(FLIGHTJOBSTABLE_LINE *prpLine,BOOL bpIsLastLine);
	BOOL PrintFlightJobsTableHeader(CCSPrint *pomPrint);
	CCSPrint *pomPrint;
	int imMaxJobsPerLine;

	int imFlightNameLen, imAdidNameLen, imRegnNameLen, imActNameLen, imStaStdNameLen, imTimeNameLen, imTimeTypeNameLen, imEmpNameLen, imJobTimeNameLen;
	bool	bmSortDemStartAscending;
	bool	bmSortDemEndAscending;
	BOOL	bmDisplayArr;
	BOOL	bmDisplayDep;

public:
	void PrintView();
	void CreateExport(const char *pclFileName, const char *pclSeperator, const char *pclFunctions);
};

#endif //__FLIGHTJOBSTABLEVIEWER_H__