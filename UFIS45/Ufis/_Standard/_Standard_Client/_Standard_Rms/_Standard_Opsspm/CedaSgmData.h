// Meta Groups
#ifndef _CEDASGMDATA_H_
#define _CEDASGMDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct SgmDataStruct
{
	long	Urno;		// Unique Record Number
	long	Usgr;		// Group Urno
	long	Uval;		// Single Urno
	char	Tabn[7];	// Table name with further info about this group
};

typedef struct SgmDataStruct SGMDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaSgmData: public CCSCedaData
{
public:
    CCSPtrArray <SGMDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omUsgrMap;
	CString GetTableName(void);
	CedaSgmData();
	~CedaSgmData();
	void ProcessSgmBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool ReadSgmData(void);
	SGMDATA *GetSgmByUrno(long lpUrno);
	void GetSgmListByUsgr(long lpUsgr, CCSPtrArray <SGMDATA> &ropSgmList, bool bpReset = true);
	void GetSgmListByUval(long lpUval, CCSPtrArray <SGMDATA> &ropSgmList, bool bpReset = true);

private:
	SGMDATA *AddSgmInternal(SGMDATA &rrpSgm);
	void DeleteSgmInternal(long lpUrno);
	void ClearAll();
};


extern CedaSgmData ogSgmData;
#endif _CEDASGMDATA_H_
