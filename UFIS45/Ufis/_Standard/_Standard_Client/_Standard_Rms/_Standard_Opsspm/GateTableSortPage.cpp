// GateTableSortPage.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <GateTableSortPage.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Change this key identifiers for name used in the registry database
#define NUMBER_OF_SORTKEYS	5
static CString ogSortKeys[NUMBER_OF_SORTKEYS] =
	{ "Gate", "Fnum", "Iofl","Time", "none" };
#define NOSORT	(NUMBER_OF_SORTKEYS - 1)	// must be "none"

/////////////////////////////////////////////////////////////////////////////
// GateTableSortPage property page

IMPLEMENT_DYNCREATE(GateTableSortPage, CPropertyPage)

GateTableSortPage::GateTableSortPage() : CPropertyPage(GateTableSortPage::IDD)
{
	//{{AFX_DATA_INIT(GateTableSortPage)
	m_Group = FALSE;
	m_SortOrder0 = -1;
	m_SortOrder1 = -1;
	m_SortOrder2 = -1;
	//}}AFX_DATA_INIT
	omTitle = GetString(IDS_STRING32900);
	m_psp.pszTitle = omTitle;
	m_psp.dwFlags |= PSP_USETITLE;
}

GateTableSortPage::~GateTableSortPage()
{
}

void GateTableSortPage::DoDataExchange(CDataExchange* pDX)
{
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		omSortOrders.SetSize(3);
		m_SortOrder0 = GetSortOrder(omSortOrders[0]);
		m_SortOrder1 = GetSortOrder(omSortOrders[1]);
		m_SortOrder2 = GetSortOrder(omSortOrders[2]);
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GateTableSortPage)
	DDX_Check(pDX, IDC_CHECK1, m_Group);
	DDX_Radio(pDX, IDC_RADIO1, m_SortOrder0);
	DDX_Radio(pDX, IDC_RADIO8, m_SortOrder1);
	DDX_Radio(pDX, IDC_RADIO15, m_SortOrder2);
	//}}AFX_DATA_MAP

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		omSortOrders.RemoveAll();
		omSortOrders.Add(GetSortKey(m_SortOrder0));
		omSortOrders.Add(GetSortKey(m_SortOrder1));
		omSortOrders.Add(GetSortKey(m_SortOrder2));
	}
}


BEGIN_MESSAGE_MAP(GateTableSortPage, CPropertyPage)
	//{{AFX_MSG_MAP(GateTableSortPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GateTableSortPage message handlers

BOOL GateTableSortPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	BOOL bResult = CPropertyPage::OnCommand(wParam, lParam);

	if (HIWORD(wParam) == BN_CLICKED)
	{
		UpdateData();	// caution: this assume there is always no error

		// Disallow using the same sorting order more than once
		if (m_SortOrder1 == m_SortOrder0)
			omSortOrders[1] = "none";
		if (m_SortOrder2 == m_SortOrder0 || m_SortOrder2 == m_SortOrder1)
			omSortOrders[2] = "none";
		UpdateData(FALSE);
	}
	return bResult;
}

/////////////////////////////////////////////////////////////////////////////
// GateTableSortPage -- helper routines

int GateTableSortPage::GetSortOrder(const char *pcpSortKey)
{
	for (int i = 0; i < NUMBER_OF_SORTKEYS; i++)
		if (ogSortKeys[i] == pcpSortKey)
			return i;

	// If there is no sorting order matched, assume "none" for no sorting
	return NOSORT;
}

CString GateTableSortPage::GetSortKey(int ipSortOrder)
{
	if (0 <= ipSortOrder && ipSortOrder <= NUMBER_OF_SORTKEYS-1)
		return ogSortKeys[ipSortOrder];

	return "";	// invalid sort order
}

BOOL GateTableSortPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	//SetWindowText(GetString(IDS_STRING32900));
	
	CWnd *polWnd = GetDlgItem(IDC_TXT4_SORTGATE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61637));
	}
	polWnd = GetDlgItem(IDC_TXT4_SORTFLIGHT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32987));
	}
	polWnd = GetDlgItem(IDC_TXT4_SORTINOUT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32977));
	}
	polWnd = GetDlgItem(IDC_TXT4_SORTTIME); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61843));
	}
	polWnd = GetDlgItem(IDC_TXT4_SORTNONE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61328));
	}
	polWnd = GetDlgItem(IDC_CHECK1); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32902));
	}
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
