// SearchPropertySheet.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <SearchPropertySheet.h>
#include <Ccsglobl.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SearchPropertySheet

IMPLEMENT_DYNAMIC(SearchPropertySheet, CPropertySheet)

SearchPropertySheet::SearchPropertySheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage, int *pipSelectedPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	pimSelectedPage = pipSelectedPage;
	AddPage(&m_FlightPage);
	AddPage(&m_EmpPage);
}

SearchPropertySheet::SearchPropertySheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage, int *pipSelectedPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	pimSelectedPage = pipSelectedPage;
	AddPage(&m_FlightPage);
	AddPage(&m_EmpPage);
}

SearchPropertySheet::~SearchPropertySheet()
{
}

BOOL SearchPropertySheet::OnInitDialog() 
{
	BOOL bResult = CPropertySheet::OnInitDialog();

	SetTitle(GetString(IDS_STRING32874));

	CRect olRect;
	CRect olWndRect;
	GetWindowRect(olWndRect);
	olRect = olWndRect;
	int ilHeight = olWndRect.bottom - olWndRect.top;
	olRect.top = (int)(::GetSystemMetrics(SM_CYSCREEN)/2)-(int)(ilHeight/2);
	olRect.bottom = (int)(::GetSystemMetrics(SM_CYSCREEN)/2)+(int)(ilHeight/2);
	
	MoveWindow(olRect);
	CRect rcApply;
	GetDlgItem(ID_APPLY_NOW)->GetWindowRect(rcApply);
	ScreenToClient(rcApply);

	CRect rcCancel;
	GetDlgItem(IDCANCEL)->GetWindowRect(rcCancel);
	ScreenToClient(rcCancel);

	GetDlgItem(ID_APPLY_NOW)->DestroyWindow();
	CRect rcOK = rcCancel;
//	rcOK.OffsetRect(0, rcOK.Height() + 16);
	GetDlgItem(IDOK)->MoveWindow(rcOK);
	CWnd *polWnd = GetDlgItem(IDOK); 
	polWnd->SetWindowText(GetString(IDS_STRING61697));
	rcCancel = rcApply;
//	rcCancel.OffsetRect(0, rcCancel.Height() + 16);
	GetDlgItem(IDCANCEL)->MoveWindow(rcCancel);
	polWnd = GetDlgItem(IDCANCEL); 
	polWnd->SetWindowText(GetString(IDS_STRING61698));

	CWnd *olHelpButton = GetDlgItem(IDHELP);
	if (olHelpButton != NULL)
	{
		CRect rcHelp;
		olHelpButton->GetWindowRect(rcHelp);
		ScreenToClient(rcHelp);
		rcHelp.OffsetRect(0, rcApply.Height() + 16);
		olHelpButton->MoveWindow(rcHelp);
		olHelpButton->SetWindowPos(&wndBottom,0,0,0,0,SWP_HIDEWINDOW | SWP_NOMOVE );
		olHelpButton->EnableWindow(FALSE);
	}
	return bResult;
}

BOOL SearchPropertySheet::FlightDetailDisplay() const
{
	if (*pimSelectedPage == PS_FLIGHTS)
	{
		if (m_FlightPage.m_SingleFlight == 0)
			return FALSE;
		else
			return TRUE;
	}
	else
		return TRUE;
}

BOOL SearchPropertySheet::EmployeeDetailDisplay() const
{
	if (*pimSelectedPage == PS_EMPLOYEES)
	{
		if (m_EmpPage.m_SingleEmployee == 0)
			return FALSE;
		else
			return TRUE;
	}
	else
		return TRUE;
}

BEGIN_MESSAGE_MAP(SearchPropertySheet, CPropertySheet)
	//{{AFX_MSG_MAP(SearchPropertySheet)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	ON_BN_CLICKED(IDOK, OnOK)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SearchPropertySheet message handlers

void SearchPropertySheet::OnOK()
{
	int ilPageIndex = 0;
	char pclTmp[6]="";

	ilPageIndex = GetActiveIndex();
	TRACE("OnOK\n");
	if (GetActivePage()->UpdateData() == FALSE)
		return;
	if(ilPageIndex == 0) //Flights
	{
		*pimSelectedPage = PS_FLIGHTS;
				
		strcpy(ogSearchFilter.rlFlightFilterData.Airline, m_FlightPage.m_Airline);

		if ( m_FlightPage.m_Flightno.IsEmpty() )
			ogSearchFilter.rlFlightFilterData.Flno[0] = '\0';
		else
			sprintf(ogSearchFilter.rlFlightFilterData.Flno, "%05s",  m_FlightPage.m_Flightno);
//		ogSearchFilter.rlFlightFilterData.Date = m_FlightPage.m_Date;
		CTime olDate = ogBasicData.GetDateByDisplayDateOffset(m_FlightPage.m_Date.GetCurSel());
		ogSearchFilter.rlFlightFilterData.Date = ogBasicData.ZeroHHMMSS(olDate);
	}
	else if(ilPageIndex == 1) //Employees
	{
		*pimSelectedPage = PS_EMPLOYEES;
		strcpy(ogSearchFilter.rlEmpFilterData.Name, m_EmpPage.m_Name);
		strcpy(ogSearchFilter.rlEmpFilterData.Shift, m_EmpPage.m_Shift);
		strcpy(ogSearchFilter.rlEmpFilterData.Rank, m_EmpPage.m_Rank);
//		ogSearchFilter.rlEmpFilterData.Date = m_EmpPage.m_Date;
		CTime olDate = ogBasicData.GetDateByDisplayDateOffset(m_EmpPage.m_Date.GetCurSel());
		ogSearchFilter.rlEmpFilterData.Date = ogBasicData.ZeroHHMMSS(olDate);
	}
	EndDialog(IDOK);
}