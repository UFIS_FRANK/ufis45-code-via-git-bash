#ifndef _CEMPD_H_
#define _CEMPD_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CCSDefines.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct EMPDATA {
	long	Urno;		// Unique record number
	char	Peno[20];	// Emp ID
	char	Lanm[41];	// Emp Last Name
	char	Finm[21];	// Emp First Name
	CTime	Doem;		// Date of Employment
	char	Telp[21];	// Home tel no
	char	Telh[21];	// Mobile tel no
	char	Teld[61];	// Telephone at work
	char	Perc[6];	// Initials
	char	Shnm[61];	// Shortname/nickname

	EMPDATA(void)
	{
		Urno = 0L;
		strcpy(Peno,"");
		strcpy(Lanm,"");
		strcpy(Finm,"");
		Doem = TIMENULL;
		strcpy(Telp,"");
		strcpy(Telh,"");
		strcpy(Teld,"");
		strcpy(Perc,"");
		strcpy(Shnm,"");
	}
};	


void ProcessEmpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaEmpData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<EMPDATA> omData;
    CMapStringToPtr omPenoMap;
    CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);

// Operations
public:
    CedaEmpData();
	~CedaEmpData();

	void ClearAll();
	void Add(EMPDATA *);
	bool ReadEmpData(void);
//	void PrepareEmpData(EMPDATA *prpEmp);
//    CCSReturnCode InsertEmployee(const EMPDATA *prpEmpData);    // used in PrePlanTable only
//    CCSReturnCode UpdateEmployee(const EMPDATA *prpEmpData);    // used in PrePlanTable only
	void ProcessEmpBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	EMPDATA * GetEmpByUrno(long lpUrno);
	EMPDATA * GetEmpByPeno(const char *pcpPeno);
	bool GetEmpsByLanm(CCSPtrArray <EMPDATA> &ropEmployees,CString omLnam);
	CString GetPenoByUrno(long lpUrno);
	CString Dump(long lpUrno);
	CString GetEmpName(long lpUrno, bool bpFullName = false);
	CString GetEmpName(EMPDATA *prpEmp, bool bpFullName = false);

private:
    bool EmpExist(const char *Peno,EMPDATA **prpData);
//    CCSReturnCode InsertEmpRecord(const EMPDATA *prpEmpData);
//    CCSReturnCode UpdateEmpRecord(const EMPDATA *prpEmpData);
};

extern CedaEmpData ogEmpData;

#endif
