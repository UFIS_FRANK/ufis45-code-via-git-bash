// DebugInfoMsgBox.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <DebugInfoMsgBox.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDebugInfoMsgBox dialog


CDebugInfoMsgBox::CDebugInfoMsgBox(CWnd* pParent /*=NULL*/)
	: CDialog(CDebugInfoMsgBox::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDebugInfoMsgBox)
	m_MsgBox = _T("");
	//}}AFX_DATA_INIT
}


void CDebugInfoMsgBox::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDebugInfoMsgBox)
	DDX_Text(pDX, IDC_MSGBOX, m_MsgBox);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDebugInfoMsgBox, CDialog)
	//{{AFX_MSG_MAP(CDebugInfoMsgBox)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDebugInfoMsgBox message handlers
