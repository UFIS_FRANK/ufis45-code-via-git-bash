// CCITablePropertySheet.h : header file
//
#ifndef _CCITBLPS_H_
#define _CCITBLPS_H_
/////////////////////////////////////////////////////////////////////////////
// CCITablePropertySheet

#include <BasePropertySheet.h>
#include <CciDiagramFilterPage.h>
#include <CciDiagramGroupPage.h>
#include <CciTableSortPage.h>

class CCITablePropertySheet : public BasePropertySheet
{
// Construction
public:
	CCITablePropertySheet(CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	CCIDiagramGroupPage  m_pageGroup;
	CCIDiagramFilterPage m_pageHall;
	CCIDiagramFilterPage m_pageRegion;
	CCIDiagramFilterPage m_pageLine;	
	CCIDiagramFilterPage m_pageTerminal;
	CCITableSortPage	 m_pageSort;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int	 QueryForDiscardChanges();
// Overrides
public:
    // Generated message map functions
    //{{AFX_MSG(CCITablePropertySheet)
	afx_msg LONG OnCciDiagramGroupPageChanged(UINT wParam, LONG lParam);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

// Helper routines
private:
	void UpdateEnableFlagInFilterPages();
};

#endif // _CCITBLPS_H_
/////////////////////////////////////////////////////////////////////////////
