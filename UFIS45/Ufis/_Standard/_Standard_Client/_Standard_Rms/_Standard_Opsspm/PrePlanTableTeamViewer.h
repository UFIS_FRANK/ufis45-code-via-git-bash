// pviewer.h : header file
//

#ifndef _PPTEAMVIEWER_H_
#define _PPTEAMVIEWER_H_

#include <PrePlanTableViewer.h>
/////////////////////////////////////////////////////////////////////////////
// PrePlanTableViewer

class PrePlanTableTeamViewer: public CViewer
{
// Constructions
public:
    PrePlanTableTeamViewer();
    ~PrePlanTableTeamViewer();

    void Attach(CTable *popAttachWnd);
    
// Internal data processing routines
private:
 //   void PrepareGrouping();
 //   void PrepareFilter();
 //   void PrepareSorter();
 //   BOOL IsPassFilter(const char *pcpRank, const char *pcpTeamId,
 //		const char *pcpShiftCode, CTime opAcfr);
 //   BOOL IsSameGroup(PREPLANT_SHIFTDATA *prpShift1, PREPLANT_SHIFTDATA *prpShift2);

	static void PrePlanTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
    int CompareShift(PREPLANT_SHIFTDATA *prpShift1, PREPLANT_SHIFTDATA *prpShift2);
    void MakeLines(CCSPtrArray<PREPLANT_SHIFTDATA> *polTeamMember);
    void MakeAssignments();
    void MakeLine(PREPLANT_SHIFTDATA *prpShift);

	/* use from PrePlanTable */
    int RecalcAssignmentStatus(PREPLANT_SHIFTDATA *prpShift);
    void RecalcTeamAssignmentStatus(int ipLineno);

// Operations
public:
    BOOL FindTeam(SHIFTDATA *prpShift, int &rilLineno);
    BOOL FindShift(long lpShiftUrno, int &rilLineno);
    BOOL FindTeamMember(long lpShiftUrno, int &rilLineno, int &rilTeamMemberno);
    void DeleteAll();
    int CreateLine(PREPLANT_SHIFTDATA *prpShift);
    void DeleteLine(int ipLineno);
    int CreateAssignment(int ipLineno, PREPLANT_ASSIGNMENT *prpAssignment);
	void DeleteAssignmentJob(long lpJobUrno);
    void DeleteAssignment(int ipLineno, int ipAssignmentno);
    int CreateTeamMember(int ipLineno, PREPLANT_SHIFTDATA *prpShift);
    void DeleteTeamMember(int ipLineno, int ipTeamMemberno);
    int CreateTeamMemberAssignment(int ipLineno, int ipTeamMemberno, PREPLANT_ASSIGNMENT *prpAssignment);
    void DeleteTeamMemberAssignment(int ipLineno, int ipTeamMemberno, int ipAssignmentno);
	void SetTeamID(CString opTeamID);
	CTime StartTime() const;
	CTime EndTime() const;
	void SetStartTime(CTime opStartTime);
	void SetEndTime(CTime opEndTime);
	int	 Lines() const;
	void DeleteAssignmentJob(int ipLineNo,int ipIndex);

// Window refreshing routines
public:
    void UpdateDisplay(CCSPtrArray<PREPLANT_SHIFTDATA> *polTeamMember);
    CString Format(PREPLANT_LINEDATA *prpLine);
	CString GetPoolName(const char *pcpPoolId);
	CString GetGateAreaName(const char *pcpGateAreaId);
	BOOL FindItemByUrno(JOBDATA *prpJob,int& ripItem, int& ripAssignment);

    // Attributes used for filtering condition
private:
    int omGroupBy;              // enumerated value -- define in "ppviewer.cpp" (use first sorting)
    CStringArray omSortOrder;     // array of enumerated value -- defined in "ppviewer.cpp"
    CMapStringToPtr omCMapForRank;
    CMapStringToPtr omCMapForTeam;
    CMapStringToPtr omCMapForShiftCode;
    CString omDate;
    BOOL bmIsCompleteTeam;
	CTime omStartTime;
	CTime omEndTime;

// Attributes
private:
    CTable *pomTable;
    CCSPtrArray<PREPLANT_LINEDATA> omLines;
	CString omTeamID;

// Methods which handle changes (from Data Distributor)
private:
    void ProcessJobNew(JOBDATA *prpJob);
	void ProcessJobDelete(JOBDATA *prpJob);
	void ProcessShiftChange(SHIFTDATA *prpShift);
};

#endif  // __PPVTEAMIEWER_H_
