// CciDeskDlg.h : header file
//
#ifndef _CCIDESKDIALOG_H_
#define _CCIDESKDIALOG_H_

/////////////////////////////////////////////////////////////////////////////
// CCciDeskDialog dialog
//void WINAPI DDX_CCSTime( CDataExchange *pDX, int nIDC, CTime& tm );
class CCciDeskDialog : public CDialog
{
// Construction
public:
	CCciDeskDialog(CWnd* pParent = NULL,CTime opDate = TIMENULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCciDeskDialog)
	enum { IDD = IDD_CCIDESK };
	CString	m_CciName;
	CString	m_Desk;
	CTime	m_Hour;
	CTime	m_Date;
	int		m_Min;
	int		m_Duration;
	//}}AFX_DATA


// Overrides
//protected:

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCciDeskDialog)
    protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCciDeskDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnKillfocusMin();
	afx_msg void On1std();
	afx_msg void On2std();
	afx_msg void OnMin30();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#endif // _CCIDESKDIALOG_H_