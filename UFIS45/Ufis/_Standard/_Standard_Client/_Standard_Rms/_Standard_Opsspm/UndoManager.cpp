#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSDDX.h>
#include <CedaJobData.h> 
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <UndoManager.h>
#include <OpssPm.h> //PRF 8712
#include <BasicData.h> //PRF 8712

/////////////////////////////////////////////////////////////////////////////
// UndoObject

UndoObject::~UndoObject()
{
	// Do nothing: just providing a template for the derived class
}

CString UndoObject::UndoText() const
{
	TRACE("Warnung: There is no definition for UndoText().\n");
	return "Unknown";
}

BOOL UndoObject::Undo(CWnd *popParentWnd)
{
	TRACE("Warning: There is no action for Undo(). Please check your class implementation.\n");
	return FALSE;
}

BOOL UndoObject::Redo(CWnd *popParentWnd)
{
	TRACE("Warning: There is no action for Redo(). Please check your class implementation.\n");
	return FALSE;
}

////////////////////////////////////////////////////////////////////////////
// UndoManager

// Local function prototype
static void UndoManagerCf(void *popInstance, int ipDDXType,
	void *vpDataPointer, CString &ropInstanceName);

UndoManager::UndoManager(int ipUndoSize)
{
	imUndoSize = ipUndoSize;	// set the depth of this undo instance
	imActionno = 0;				// reset the action number
}

UndoManager::~UndoManager()
{
	ogCCSDdx.UnRegister(this, NOTUSED);
	DeleteAllUndoRedoSteps();
}

void UndoManager::RegisterDDX()
{
//	// register the DDX handler for discard every undo steps if we changes what-if
//	ogCCSDdx.Register(this, REDISPLAY_ALL,
//		CString("UNDO_MANAGER"), CString("Redisplay All on switching What-If"), UndoManagerCf);
}

BOOL UndoManager::SetUndoSize(int ipUndoSize)
{
	if (omUndoStep.GetSize() + omRedoStep.GetSize() > ipUndoSize)	// cannot truncate undo stack
	{
		TRACE("Error: UndoManager::SetUndoSize() cannot shrink the size of the undo stack.\n"
			"  This setting will be ignored.\n");
		return FALSE;
	}

	imUndoSize = ipUndoSize;	// set the depth of this undo instance
	return TRUE;
}

void UndoManager::NewAction()
{
	imActionno++;
}

void UndoManager::NewStep(const CString &ropKey, UndoObject *popUndoObject)
{
	STEP olStep;
	olStep.Key = ropKey;
	olStep.Actionno = imActionno;
	olStep.p = popUndoObject;
	omUndoStep.NewAt(0, olStep);

	// limit the number of steps to "imUndoSize"
	if (omUndoStep.GetSize() > imUndoSize)
		omUndoStep.DeleteAt(imUndoSize);

	// tell the UndoDialog to refresh itself, if it is opening
	ogCCSDdx.DataChanged((void *)this, UNDO_CHANGE, NULL);
}

void UndoManager::DeleteAllUndoRedoSteps()
{
	// remove every entries in the step arrays
	while (omUndoStep.GetSize() > 0)
	{
		delete omUndoStep[0].p;		// delete the undo object in this entry
		omUndoStep.DeleteAt(0);
	}
	while (omRedoStep.GetSize() > 0)
	{
		delete omRedoStep[0].p;		// delete the undo object in this entry
		omRedoStep.DeleteAt(0);
	}
}

void UndoManager::UndoOneAction(CWnd *popParentWnd)
{
	if (omUndoStep.GetSize() == 0)
	{
		//AfxGetMainWnd()->MessageBox("Keine Aktion f�r Undo/Redo verf�gbar.");
		AfxGetMainWnd()->MessageBox(GetString(IDS_STRING61417));
		return;
	}
	int ilSteps = 0;
	int ilActionno = omUndoStep[0].Actionno;
	while (omUndoStep.GetSize() > 0 && omUndoStep[0].Actionno == ilActionno)
	{
		if (!omUndoStep[0].p->Undo(popParentWnd))	// is there any problems in undo action?
		{
			// we have to recover every things previously done in this method
			for (int i = 0; i < ilSteps; i++)
			{
				omRedoStep[0].p->Redo(popParentWnd);	// no checking on recovering
				omUndoStep.InsertAt(0, &omRedoStep[0]);
				omRedoStep.RemoveAt(0);
			}
			break;
		}
		// we will put the successfully done steps to the redo list
		ilSteps++;
		omRedoStep.InsertAt(0, &omUndoStep[0]);
		omUndoStep.RemoveAt(0);
	}
}

void UndoManager::RedoOneAction(CWnd *popParentWnd)
{
	if (omRedoStep.GetSize() == 0)
	{
		//AfxGetMainWnd()->MessageBox("Keine Aktion f�r Undo/Redo verf�gbar.");
		AfxGetMainWnd()->MessageBox(GetString(IDS_STRING61417));
		return;
	}
	int ilSteps = 0;
	int ilActionno = omRedoStep[0].Actionno;
	while (omRedoStep.GetSize() > 0 && omRedoStep[0].Actionno == ilActionno)
	{
		if (!omRedoStep[0].p->Redo(popParentWnd))	// is there any problems in redo action?
		{
			// we have to recover every things previously done in this method
			for (int i = 0; i < ilSteps; i++)
			{
				omUndoStep[0].p->Undo(popParentWnd);	// no checking on recovering
				omRedoStep.InsertAt(0, &omUndoStep[0]);
				omUndoStep.RemoveAt(0);
			}
			return;
		}
		// we will put the successfully done steps to the undo list
		ilSteps++;
		omUndoStep.InsertAt(0, &omRedoStep[0]);
		omRedoStep.RemoveAt(0);
	}
}

void UndoManager::DoModalUndoDialog(CWnd *popParentWnd /* = NULL */)
{
	// Check first if there is anything to undo/redo
	CUndoDialog dialog(this, popParentWnd);
	dialog.DoModal();
}

void UndoManager::GetUndoList(CStringArray &ropUndoList)
{
	for(int i = 0; i < omUndoStep.GetSize(); i++)
		ropUndoList.Add(omUndoStep[i].p->UndoText());
}


static void UndoManagerCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	((UndoManager *)popInstance)->DeleteAllUndoRedoSteps();
}

// Local function prototype
static void UndoDialogCf(void *popInstance, int ipDDXType,
	void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// CUndoDialog dialog

CUndoDialog::CUndoDialog(UndoManager *popUndoManager, CWnd* pParent /*=NULL*/)
	: CDialog(CUndoDialog::IDD, pParent)
{
	pomUndoManager = popUndoManager;
	pomParentWnd = pParent;

	//{{AFX_DATA_INIT(CUndoDialog)
	m_RadioButton = 0;
	//}}AFX_DATA_INIT
}

void CUndoDialog::RefreshDialog()
{
	m_RadioButton = 0;	// default is for undoing
	UpdateShowRadioButtons();
	LoadStepsToListBox();
}

void CUndoDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUndoDialog)
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_Radio(pDX, IDC_SHOWUNDOLIST, m_RadioButton);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CUndoDialog, CDialog)
	//{{AFX_MSG_MAP(CUndoDialog)
	ON_BN_CLICKED(IDC_SHOWREDOLIST, OnShowredolist)
	ON_BN_CLICKED(IDC_SHOWUNDOLIST, OnShowundolist)
	ON_LBN_SELCHANGE(IDC_LIST, OnSelchangeList)
	ON_BN_CLICKED(IDC_DOIT, OnDoit)
	ON_WM_MOVE() //PRF 8712
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUndoDialog message handlers

BOOL CUndoDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING33077));
	CWnd *polWnd = GetDlgItem(IDC_UNDOSELECT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33078));
	}
	polWnd = GetDlgItem(IDC_SHOWUNDOLIST); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33079));
	}
	polWnd = GetDlgItem(IDC_SHOWREDOLIST); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33080));
	}
	polWnd = GetDlgItem(IDC_DOIT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61337));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32831));
	}

	// Fix it to be the top most window
	SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

	// register the DDX handler for update dialog while undo manager is working
	ogCCSDdx.Register(this, UNDO_CHANGE,
		CString("UNDO_CHANGE"), CString("Undo Change"), UndoDialogCf);

	// Enable appropriated radio buttons and select one
	UpdateShowRadioButtons();
	LoadStepsToListBox();

	BOOL blMinimized = FALSE; 
	CRect olRect;
	GetWindowRect(&olRect);
	ogBasicData.GetDialogFromReg(olRect, COpssPmApp::UNDO_DIALOG_WINDOWPOS_REG_KEY,blMinimized);

	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olRect);

	SetWindowPos(&wndTop, olRect.left,olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUndoDialog::OnShowundolist() 
{
	m_RadioButton = 0;
	LoadStepsToListBox();
	//GetDlgItem(IDC_DOIT)->SetWindowText("Undo");
	GetDlgItem(IDC_DOIT)->SetWindowText(GetString(IDS_STRING61337));
}

void CUndoDialog::OnShowredolist() 
{
	m_RadioButton = 1;
	LoadStepsToListBox();
	//GetDlgItem(IDC_DOIT)->SetWindowText("Redo");
	GetDlgItem(IDC_DOIT)->SetWindowText(GetString(IDS_STRING61338));
}

void CUndoDialog::OnSelchangeList() 
{
	BOOL blSomeItemIsSelected = (m_List.GetSelCount() != 0);
	if (blSomeItemIsSelected)
	{
		int  ilSelSize   = m_List.GetSelCount();
		int* pilSelected = new int[ilSelSize];
		ilSelSize = m_List.GetSelItems(ilSelSize,pilSelected);

		int ilMinIndex = 0;
		int ilMaxIndex = 0;
		for (int ilC = 0; ilC < ilSelSize; ilC++)		
		{
			if (pilSelected[ilC] > ilMaxIndex)
				ilMaxIndex = pilSelected[ilC];
		}

		// ensure that everything from top of stack is selected
		if (ilMinIndex != ilMaxIndex)
			VERIFY(m_List.SelItemRange(TRUE,ilMinIndex,ilMaxIndex) != LB_ERR);

		delete [] pilSelected;
	}

	GetDlgItem(IDC_DOIT)->EnableWindow(m_RadioButton != -1 && blSomeItemIsSelected);
}

void CUndoDialog::OnDoit() 
{
	switch (m_RadioButton)
	{
	case 0:	// undo steps
		UndoSelectedSteps();
		UpdateShowRadioButtons();
		LoadStepsToListBox();
		break;
	case 1:	// redo steps
		RedoSelectedSteps();
		UpdateShowRadioButtons();
		LoadStepsToListBox();
		break;
	}
}

void CUndoDialog::OnCancel() 
{
	ogCCSDdx.UnRegister(this, NOTUSED);
	ogBasicData.WriteDialogToReg(this,COpssPmApp::UNDO_DIALOG_WINDOWPOS_REG_KEY,omWindowRect);
	CDialog::OnCancel();
}

/////////////////////////////////////////////////////////////////////////////
// CUndoDialog helper routines

void CUndoDialog::UpdateShowRadioButtons()
{
	bmIsUndoStepExist = (pomUndoManager->omUndoStep.GetSize() != 0);
	bmIsRedoStepExist = (pomUndoManager->omRedoStep.GetSize() != 0);
	GetDlgItem(IDC_SHOWUNDOLIST)->EnableWindow(bmIsUndoStepExist);
	GetDlgItem(IDC_SHOWREDOLIST)->EnableWindow(bmIsRedoStepExist);

	if (m_RadioButton == 0 && !bmIsUndoStepExist)
		m_RadioButton = (bmIsRedoStepExist)? 1: -1;
	if (m_RadioButton == 1 && !bmIsRedoStepExist)
		m_RadioButton = (bmIsUndoStepExist)? 0: -1;

	switch (m_RadioButton)
	{
	case 0:	// show undo list
		OnShowundolist();
		break;
	case 1:	// show redo list
		OnShowredolist();
		break;
	}
	UpdateData(FALSE);
}

void CUndoDialog::LoadStepsToListBox()
{
	int i;

	m_List.ResetContent();
	switch (m_RadioButton)
	{
	case 0:	// undo list
		for (i = 0; i < pomUndoManager->omUndoStep.GetSize(); i++)
			m_List.AddString(pomUndoManager->omUndoStep[i].p->UndoText());
		break;
	case 1:	// redo list
		for (i = 0; i < pomUndoManager->omRedoStep.GetSize(); i++)
			m_List.AddString(pomUndoManager->omRedoStep[i].p->UndoText());
		break;
	default:
		//m_List.AddString("Keine Aktion f�r Undo/Redo verf�gbar.");
		m_List.AddString(GetString(IDS_STRING61577));
		break;
	}

	if (m_List.GetCount( ) > 0)
	{
		m_List.SetSel(0,TRUE);
	}
//	GetDlgItem(IDC_DOIT)->EnableWindow(FALSE);
}

void CUndoDialog::UndoSelectedSteps()
{
	int ilItemCount = m_List.GetSelCount();
	if (ilItemCount == 0 || pomUndoManager->omUndoStep.GetSize() == 0)
	{
		//AfxGetMainWnd()->MessageBox("Keine Aktion f�r Undo/Redo verf�gbar.");
		AfxGetMainWnd()->MessageBox(GetString(IDS_STRING61417));
		return;
	}

	// Check which items is selected by the user
	AfxGetApp()->BeginWaitCursor();
	int ilSteps = 0;
	int *pilIndexes = new int [ilItemCount];
	m_List.GetSelItems(ilItemCount, pilIndexes);

	// For each selected item, trying to undo it. Stop at first error found.
	for (int i = 0; i < ilItemCount; i++)
	{
		int ilStepno = pilIndexes[i];
		if (!pomUndoManager->omUndoStep[ilStepno].p->Undo(this))	// is there any problems in undo action?
		{
			// we have to recover every things previously done in this method
			for (int i = ilSteps-1; i >= 0; i--)
			{
				int ilStepno = pilIndexes[i];
				pomUndoManager->omUndoStep[ilStepno].p->Redo(this);	// no checking on recovering
			}
			delete pilIndexes;
			AfxGetApp()->EndWaitCursor();
			return;
		}
		// we will put the successfully done steps to the redo list
		ilSteps++;
	}

	// All step was complete successfully, now move data from omUndoStep to omRedoStep
	for (i = 0; i < ilItemCount; i++)
	{
		int ilStepno = pilIndexes[i];
		pomUndoManager->omRedoStep.InsertAt(0, &pomUndoManager->omUndoStep[ilStepno]);
	}
	for (i = ilItemCount-1; i >= 0; i--)	// have to use a backward loop
	{
		int ilStepno = pilIndexes[i];
		pomUndoManager->omUndoStep.RemoveAt(ilStepno);
	}
	delete pilIndexes;
	AfxGetApp()->EndWaitCursor();
}

void CUndoDialog::RedoSelectedSteps()
{
	int ilItemCount = m_List.GetSelCount();
	if (ilItemCount == 0 || pomUndoManager->omRedoStep.GetSize() == 0)
	{
		//AfxGetMainWnd()->MessageBox("Keine Aktion f�r Undo/Redo verf�gbar.");
		AfxGetMainWnd()->MessageBox(GetString(IDS_STRING61417));
		return;
	}

	// Check which items is selected by the user
	AfxGetApp()->BeginWaitCursor();
	int ilSteps = 0;
	int *pilIndexes = new int [ilItemCount];
	m_List.GetSelItems(ilItemCount, pilIndexes);

	// For each selected item, trying to undo it. Stop at first error found.
	for (int i = 0; i < ilItemCount; i++)
	{
		int ilStepno = pilIndexes[i];
		if (!pomUndoManager->omRedoStep[ilStepno].p->Redo(this))	// is there any problems in redo action?
		{
			// we have to recover every things previously done in this method
			for (int i = ilSteps-1; i >= 0; i--)
			{
				int ilStepno = pilIndexes[i];
				pomUndoManager->omRedoStep[ilStepno].p->Undo(this);	// no checking on recovering
			}
			delete pilIndexes;
			AfxGetApp()->EndWaitCursor();
			return;
		}
		// we will put the successfully done steps to the redo list
		ilSteps++;
	}

	// All step was complete successfully, now move data from omRedoStep to omUndoStep
	for (i = 0; i < ilItemCount; i++)
	{
		int ilStepno = pilIndexes[i];
		pomUndoManager->omUndoStep.InsertAt(0, &pomUndoManager->omRedoStep[ilStepno]);
	}
	for (i = ilItemCount-1; i >= 0; i--)	// have to use a backward loop
	{
		int ilStepno = pilIndexes[i];
		pomUndoManager->omRedoStep.RemoveAt(ilStepno);
	}
	delete pilIndexes;
	AfxGetApp()->EndWaitCursor();
}

//PRF 8712
void CUndoDialog::OnMove(int x, int y)
{
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CUndoDialog -- Methods which handle changes (from Data Distributor)

static void UndoDialogCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	((CUndoDialog *)popInstance)->RefreshDialog();
}
