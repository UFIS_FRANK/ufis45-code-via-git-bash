#ifndef _CCSSTR_H
#define _CCSSTR_H

//@ManMemo: Simple String class.
//@Doc: Usefull, if you don't have a {\em better} String class.
class CCSString
{
  private:

    // data
    int		len;		// the number of chars in s. -1 -> not known
    int		mem;		// amount of memory allocated for this string
    int		chunk_size;	// grab memory in hunks of this size
    
    // methods
    void	BigEnough(const int);
    void	InitVars();
    int		InRange(const long) const;
    void	Length(const int new_length); // assigns to len.

  protected:

    // data
    char*	contents; // the internal string

    // methods
    int		Compare(const CCSString&) const;
    int		Compare(const char*) const;
    void	Concat(const CCSString&);
    void	Concat(const char*);
    void	Concat(const char&);
    void	Copy(const CCSString&);
    void	Copy(const char*);
    void	Copy(const char&);
    int		IsNull() const;

  public:

    // construction/destruction
    ///default constructor
    CCSString();
    ///copy constructor
    CCSString(const CCSString&);
    ///type conversion constructor
    CCSString(const char *);
    ///
    CCSString(char);
    ///
    CCSString(long);
    ///
    CCSString(double);
    // destructor
    virtual ~CCSString();

    // methods

    ///what size hunks is memory grabbed in
    int		Chunksize() const; 
    ///change size of memory hunks
    void	Chunksize(const int new_size);
    ///is the string empty or NULL
    int		Empty() const;
    ///how many chars in CCSString
    int		Length() const;
    ///assignment
    CCSString& 	operator=(const CCSString&);
    ///
    CCSString& 	operator=(const char*);
    ///return ith char, i < Length()
    const char&	operator[](const long i)const;
    ///concatenation
    CCSString&	operator+=(const CCSString&);    
    CCSString&	operator+=(const char*);
    ///conversion operator
    operator const char*() const;
    ///convert to lower case
    CCSString ToLower() const;

    // friends
    
    ///concatenation
    friend CCSString operator+(const CCSString&, const CCSString&);
    ///
    friend CCSString operator+(const CCSString&, const char*);
    ///
    friend CCSString operator+(const char*, const CCSString&);

    ///equality
    friend int operator==(const CCSString&, const CCSString&);
    ///
    friend int operator==(const CCSString&, const char*);
    ///
    friend int operator==(const char*, const CCSString&);

    ///inequality
    friend int operator!=(const CCSString&, const CCSString&);
    ///
    friend int operator!=(const CCSString&, const char*);
    ///
    friend int operator!=(const char*, const CCSString&);

    ///greater than
    friend int operator>(const CCSString&, const CCSString&);
    ///
    friend int operator>(const CCSString&, const char*);
    ///
    friend int operator>(const char*, const CCSString&);

    ///greater than or equal to
    friend int operator>=(const CCSString&, const CCSString&);
    ///
    friend int operator>=(const CCSString&, const char*);
    ///
    friend int operator>=(const char*, const CCSString&);

    ///less than
    friend int operator<(const CCSString&, const CCSString&);
    ///
    friend int operator<(const CCSString&, const char*);
    ///
    friend int operator<(const char*, const CCSString&);

    ///less than or equal to
    friend int operator<=(const CCSString&, const CCSString&);
    ///
    friend int operator<=(const CCSString&, const char*);
    ///
    friend int operator<=(const char*, const CCSString&);
};

// printf(3) style constructor
extern CCSString CCSStringFormat (const char *fmt, ...);

#endif

