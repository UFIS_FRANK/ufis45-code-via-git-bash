// DemandTable.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>

#include <OpssPm.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <DemandTableViewer.h>
#include <DemandTable.h>

#include <cviewer.h>
#include <FilterPage.h>
#include <ZeitPage.h>
#include <DemandFilterPage.h>
#include <DemandTableSortPage.h>
#include <BasePropertySheet.h>
#include <DemandTablePropertySheet.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <CedaCfgData.h>
#include <dataset.h>

#include <cxbutton.h>
#include <CedaShiftData.h>
#include <CCIDiagram.h>
#include <CCITable.h>
#include <ConflictTable.h>
#include <ConflictConfigTable.h>
#include <StaffDiagram.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <PrePlanTable.h>
#include <ButtonList.h>
#include <FlightDetailWindow.h>
#include <CciDemandDetailDlg.h>
#include <ccstable.h>
#include <AvailableEmpsDlg.h>
#include <AvailableEquipmentDlg.h>
#include <PrintTerminalSelection.h> //Singapore
#include <SetDisplayTimeframeDlg.h> // Singapore

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const CString DemandTable::Terminal2 = "T2";
const CString DemandTable::Terminal3 = "T3";
// Prototypes
#define IsTotallyInside(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

/////////////////////////////////////////////////////////////////////////////
// DemandTable dialog

DemandTable::DemandTable(CWnd* pParent /*=NULL*/)
	: CDialog(DemandTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(DemandTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
    pomTable->tempFlag = 2;
	pomTable->bmMultiDrag = true;

	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{
		pomActiveTable = pomTable;
		pomTableT3 = new CCSTable;
		pomTableT3->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
		pomTableT3->tempFlag = 2;
		pomTableT3->bmMultiDrag = true;
		pomViewerT3 = new DemandTableViewer;		
	}

    CDialog::Create(DemandTable::IDD);
    CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.DETB,ogCfgData.rmUserSetup.MONS);
	olRect.top += 40;
	BOOL blMinimized = FALSE;
	ogBasicData.GetDialogFromReg(olRect, COpssPmApp::DEMAND_TABLE_WINDOWPOS_REG_KEY,blMinimized);

	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olRect);

    MoveWindow(&olRect);
	bmIsViewOpen = FALSE;

	CTime tm = ogBasicData.GetTime();

    CRect rect;
    GetClientRect(&rect);
	rect.top += m_nDialogBarHeight;
	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{
		pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom/2);
		pomViewer = new DemandTableViewer;
		pomActiveViewer = pomViewer;
		pomViewer->SetTerminal(Terminal2);
		pomViewer->Attach(pomTable);
		
		pomTableT3->SetTableData(this, rect.left, rect.right, rect.bottom/2 + 1, rect.bottom);// + rect.bottom/2 + 1);				
		pomViewerT3->SetTerminal(Terminal3);
		pomViewerT3->Attach(pomTableT3);

		bmIsT2Visible = TRUE;
		bmIsT3Visible = TRUE;
	}
	else
	{
		pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
		pomViewer = new DemandTableViewer;
		pomViewer->Attach(pomTable);
				
		pomActiveTable = pomTable;
		pomActiveViewer = pomViewer;
		
		bmIsT2Visible = FALSE;
		bmIsT3Visible = FALSE;
	}

	UpdateView();
	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}
}

DemandTable::DemandTable(DemandTableViewer *popViewer,CWnd* pParent /*=NULL*/)
	: CDialog(DemandTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(DemandTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT


	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
    pomTable->tempFlag = 2;
	pomTable->bmMultiDrag = true;

	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{		
	
		pomActiveTable = pomTable;
		pomTableT3 = new CCSTable;
		pomTableT3->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
		pomTableT3->tempFlag = 2;
		pomTableT3->bmMultiDrag = true;
		pomViewerT3 = new DemandTableViewer;

		bmIsT2Visible = TRUE;
		bmIsT3Visible = TRUE;
	}


	if (popViewer)
		pomViewer = popViewer;
	else
		pomViewer = new DemandTableViewer;



	pomActiveViewer = pomViewer; //Singapore
    CDialog::Create(DemandTable::IDD);
    CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.DETB,ogCfgData.rmUserSetup.MONS);
	olRect.top += 40;
	BOOL blMinimized = FALSE;
	ogBasicData.GetDialogFromReg(olRect, COpssPmApp::DEMAND_TABLE_WINDOWPOS_REG_KEY,blMinimized);

	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olRect);

    MoveWindow(&olRect);
	bmIsViewOpen = FALSE;

    CRect rect;
    GetClientRect(&rect);
	rect.top += m_nDialogBarHeight;

	

	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{
		pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom/2);
		pomViewer->SetTerminal(Terminal2);
		pomViewer->Attach(pomTable);
		
		pomTableT3->SetTableData(this, rect.left, rect.right, rect.bottom/2 + 1 , rect.bottom);// + rect.bottom/2 + 1);
		pomViewerT3->SetTerminal(Terminal3);
		pomViewerT3->Attach(pomTableT3);
	}
	else
	{
		pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
		pomViewer->Attach(pomTable);
		pomActiveTable = pomTable;
		pomActiveViewer = pomViewer;
		
		bmIsT2Visible = FALSE;
		bmIsT3Visible = FALSE;
	}


	
	UpdateView();
	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}
}

DemandTable::~DemandTable()
{
	TRACE("DemandTable::~DemandTable()\n");
	pomViewer->Attach(NULL);
	delete pomTable;	
	delete pomViewer;

	if(ogBasicData.IsPaxServiceT2() == true)
	{
		pomViewerT3->Attach(NULL);
		delete pomTableT3;
		delete pomViewerT3;
	}
}

void DemandTable::SetCaptionText(void)
{
//	CString olCaptionText = CString("Demands: ") + 
//		pomViewer->omStartTime.Format("%d/%H%M") + "-" + 
//			pomViewer->omEndTime.Format("%H%M ");;
	CString olCaptionText = GetString(IDS_STRING61870) + pomViewer->StartTime().Format("%d/%H%M") + "-" + pomViewer->EndTime().Format("%H%M ");;
//	if (bgOnline)
//	{
//		//olCaptionText += "  Online";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61301);
//	}
//	else
//	{
//		//olCaptionText += "  OFFLINE";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61302);
//	}

	SetWindowText(olCaptionText);

}

void DemandTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DemandTable)
	DDX_Control(pDX, IDC_DATE, m_Date);
	DDX_Control(pDX, IDC_BUTTONT2,m_bTerminal2); //Singapore
	DDX_Control(pDX, IDC_BUTTONT3,m_bTerminal3); //Singapore
	DDX_Control(pDX, IDC_DEM_SETTIME, m_bSetTime);//Singapore
	//}}AFX_DATA_MAP
}

void DemandTable::UpdateView()
{

	CString  olViewName = pomViewer->GetViewName();
	olViewName = pomViewer->GetViewName();

	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.DETV;
	}
	
	BOOL blIsGrouped = *pomViewer->GetGroup() - '0';
	
	CWaitCursor olWait;
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
       

		pomViewer->ChangeViewTo(olViewName);
        pomViewerT3->ChangeViewTo(olViewName);
	}
	else
	{
		//here slow
		pomViewer->ChangeViewTo(olViewName);
	}

	SetCaptionText();

		
	
	if (blIsGrouped)
	{
	
		
		CWnd *polWnd = GetDlgItem(IDC_EXPANDGROUP); 
		if(polWnd != NULL)
		{
			if(ogBasicData.bmDisplayTeams)
			{
				polWnd->ShowWindow(SW_SHOW);
			}
			else
			{
				polWnd->ShowWindow(SW_HIDE);
			}
		}

		polWnd = GetDlgItem(IDC_COMPRESSGROUP); 
		if(polWnd != NULL)
		{
			if(ogBasicData.bmDisplayTeams)
			{
				polWnd->ShowWindow(SW_SHOW);
			}
			else
			{
				polWnd->ShowWindow(SW_HIDE);
			}
		}
		
	}
	else
	{
		
		
		CWnd *polWnd = GetDlgItem(IDC_EXPANDGROUP); 
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}

		polWnd = GetDlgItem(IDC_COMPRESSGROUP); 
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}

	}
	
	//Singapore
	OnTableUpdateDataCount();


	
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
//	pomTable->GetCTableListBox()->SetFocus();
}

void DemandTable::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
	polCB->ResetContent();
	CStringArray olStrArr;
	pomViewer->GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = pomViewer->GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.DETV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}

}

BEGIN_MESSAGE_MAP(DemandTable, CDialog)
	//{{AFX_MSG_MAP(DemandTable)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_MOVE() //PRF 8712
	ON_CBN_SELCHANGE(IDC_VIEWCOMBO, OnSelchangeViewcombo)
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_CBN_CLOSEUP(IDC_VIEWCOMBO, OnCloseupViewcombo)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_DATE, OnSelchangeDate)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
	ON_BN_CLICKED(IDC_EXPANDGROUP,OnExpandGroup)
	ON_BN_CLICKED(IDC_COMPRESSGROUP,OnCompressGroup)
    ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelchange)
	ON_COMMAND(11, OnMenuWorkOn)
	ON_COMMAND(12, OnMenuAvailableEmployees)
	ON_COMMAND(13, OnMenuAutomaticAssignment)
	ON_COMMAND(14, OnMenuAvailableEquipment)
	ON_COMMAND(15, OnMenuDeactivateDemand)
	ON_BN_CLICKED(IDC_BUTTONT2,OnButtonT2) //Singapore
	ON_BN_CLICKED(IDC_BUTTONT3,OnButtonT3) //Singapore
	ON_MESSAGE(WM_TABLE_UPDATE_DATACOUNT,OnTableUpdateDataCount) //Singapore
	ON_BN_CLICKED(IDC_DEM_SETTIME, OnSetTime)//Nyan
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DemandTable message handlers

BOOL DemandTable::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetWindowText(GetString(IDS_STRING61870)); 

	CWnd *polWnd = GetDlgItem(IDC_VIEW); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	polWnd = GetDlgItem(IDC_PRINT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}
	
	polWnd = GetDlgItem(IDC_EXPANDGROUP); 
	if(polWnd != NULL)
	{
		if(ogBasicData.bmDisplayTeams)
		{
			polWnd->SetWindowText(GetString(IDS_EXPANDGROUP));
		}
		else
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}

	polWnd = GetDlgItem(IDC_COMPRESSGROUP); 
	if(polWnd != NULL)
	{
		if(ogBasicData.bmDisplayTeams)
		{
			polWnd->SetWindowText(GetString(IDS_CONTRACTGROUP));
		}
		else
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}

	//Singapore
	if(ogBasicData.IsPaxServiceT2() == false)
	{
		CWnd* polWnd = GetDlgItem(IDC_STATIC_T2T3);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_BUTTONT2);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_BUTTONT3);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}
	else
	{
		m_bTerminal2.EnableWindow(TRUE);
		m_bTerminal2.ShowWindow(SW_SHOW);
		m_bTerminal3.EnableWindow(TRUE);
		m_bTerminal3.ShowWindow(SW_SHOW);
		m_bTerminal2.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_bTerminal3.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}

	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
	{	
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	}
	else
	{
		strcpy(pclConfigPath, getenv("CEDA"));
	}

	GetPrivateProfileString(pcgAppName, "DEMAND_SETTIME_BTN",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (stricmp(pclTmpText,"YES")==0)
	{		
		m_bSetTime.ShowWindow(SW_SHOW);
	}
	else
	{
		m_bSetTime.ShowWindow(SW_HIDE);
	}


	// TODO: Add extra initialization here
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

	UpdateComboBox();

    // extend dialog window to current screen width
    rect.left = 10;
    rect.top = 56;
    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 10;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 10;
    //MoveWindow(&rect); //Commented - Singapore PRF 8712

	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{
		m_DragDropTarget.RegisterTarget(this, this);
	}
	else
	{
		m_DragDropTarget.RegisterTarget(this, this,pomTable->GetCTableListBox());
	}

	// Register DDX call back function
	TRACE("DemandTable: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("DEMANDTABLE"),CString("Redisplay all"), DemandTableCf);
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("DEMANDTABLE"),CString("Global Date Change"), DemandTableCf);
	
	ogBasicData.SetWindowStat("DEMANDTABLE IDC_VIEW",GetDlgItem(IDC_VIEW));
	ogBasicData.SetWindowStat("DEMANDTABLE IDC_VIEWCOMBO",GetDlgItem(IDC_VIEWCOMBO));
	ogBasicData.SetWindowStat("DEMANDTABLE IDC_PRINT",GetDlgItem(IDC_PRINT));

	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(olTimeframeList[ilDay]);
	}
	if(ilNumDays > 0)
	{
		m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
		SetViewerDate();
	}

	ShowWindow(SW_HIDE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DemandTable::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	ogBasicData.WriteDialogToReg(this,COpssPmApp::DEMAND_TABLE_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	// Unregister DDX call back function
	TRACE("DemandTable: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CDialog::OnDestroy();
}

void DemandTable::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	DestroyWindow();
//	AfxGetMainWnd()->SendMessage(WM_DEMANDTABLE_EXIT);
   	pogButtonList->m_wndDemandTable = NULL;
	pogButtonList->pomDemandTableButton->Recess(FALSE);
}

void DemandTable::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
   if (nType != SIZE_MINIMIZED)
   {
	   if(ogBasicData.IsPaxServiceT2() == true)
	   {
			if(bmIsT2Visible == TRUE && bmIsT3Visible == TRUE) //Singapore
			{
				pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy/2+1);
				pomTableT3->SetPosition(-1, cx+1, cy/2+2, cy+1);
			}
			else if(bmIsT2Visible == TRUE && bmIsT3Visible == FALSE)
			{
				pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy);			
			}
			else if(bmIsT2Visible == FALSE && bmIsT3Visible == TRUE)
			{
				pomTableT3->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
			}
	   }
	   else
	   {
			pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
	   }
	   GetWindowRect(&omWindowRect); //PRF 8712
   }
}

void DemandTable::OnExpandGroup()
{
	CWaitCursor olWait;
	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{		
		pomViewer->CompressGroups(false);
		pomViewerT3->CompressGroups(false);
	}
	else
	{
		pomViewer->CompressGroups(false);
	}
}

void DemandTable::OnCompressGroup()
{
	CWaitCursor olWait;
	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{
		pomViewer->CompressGroups(true);
		pomViewerT3->CompressGroups(true);
	}
	else
	{
		pomViewer->CompressGroups(true);
	}
}

void DemandTable::OnView() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
//	pomTable->GetCTableListBox()->SetFocus();
	DemandTablePropertySheet dialog(this, pomViewer);
	bmIsViewOpen = TRUE;
	if (dialog.DoModal() != IDCANCEL)
	{
		UpdateComboBox();
		UpdateView();
	}
	bmIsViewOpen = FALSE;
}

void DemandTable::OnSelchangeViewcombo() 
{
    char clText[64];
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		pomViewer->SelectView(clText);
		pomViewerT3->SelectView(clText);
	}
	else
	{
		pomViewer->SelectView(clText);
	}
	UpdateView();		
}

void DemandTable::OnCloseupViewcombo() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
//    pomTable->GetCTableListBox()->SetFocus();
}

void DemandTable::OnPrint() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
//	pomTable->GetCTableListBox()->SetFocus();
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		CPrintTerminalSelection olTerminalSelection;
		CString olWindowText;GetWindowText(olWindowText);
		olTerminalSelection.SetWindowHeaderText(olWindowText);
		if(olTerminalSelection.DoModal() == IDOK)
		{
			if(olTerminalSelection.IsTerminal2Selected() == TRUE)
			{
	            pomViewer->PrintView();
			}
			else if(olTerminalSelection.IsTerminal3Selected() == TRUE)
			{
				pomViewerT3->PrintView();
			}
			else if(olTerminalSelection.IsBothTerminalSelected() == TRUE)
			{	
				DemandTableViewer olDemandTableViewer;
				olDemandTableViewer.Attach(pomTable);
				CCSPtrArray<DEMDATA_LINE> olLines;
				DEMDATA_LINE olLineData;					
				pomViewer->GetLines(olLines);
				for(int i = 0 ; i < olLines.GetSize(); i++)
				{
					olLineData = olLines.GetAt(i);
					olDemandTableViewer.GetLines().NewAt(olDemandTableViewer.GetLines().GetSize(),olLineData);
				}
				pomViewerT3->GetLines(olLines);
				for(i = 0 ; i < olLines.GetSize(); i++)
				{
					olLineData = olLines.GetAt(i);
					olDemandTableViewer.GetLines().NewAt(olDemandTableViewer.GetLines().GetSize(),olLineData);
				}
				olDemandTableViewer.PrintView();
				olDemandTableViewer.GetLines(olLines);
				for(i = 0; i < olLines.GetSize(); i++)
				{
					olLines.GetAt(i).TeamLines.DeleteAll();
				}
				olDemandTableViewer.GetLines().DeleteAll();
				olDemandTableViewer.Attach(NULL);
			}
		}
	}
	else
	{
		pomViewer->PrintView();
	}
}

//Nyan OnSetTime()
void DemandTable::OnSetTime() 
{
	CSetDisplayTimeframeDlg olDlg;
	olDlg.omFrom = pomViewer->StartTime();
	olDlg.omTo = pomViewer->EndTime();
	olDlg.omFromReadOnly = false;
	olDlg.omToReadOnly = false;

	if(olDlg.DoModal() == IDOK)
	{

		
		pomViewer->SetStartTime(olDlg.omFrom);
		pomViewer->SetEndTime(olDlg.omTo);
		if(ogBasicData.IsPaxServiceT2() == true)
		{
			pomViewerT3->SetStartTime(olDlg.omFrom);
			pomViewerT3->SetEndTime(olDlg.omTo);
		}
		UpdateView();
	}
}


BOOL DemandTable::DestroyWindow() 
{
	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}

	pogButtonList->SendMessage(WM_DEMANDTABLE_EXIT);

	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

////////////////////////////////////////////////////////////////////////
// DemandTable -- implementation of DDX call back function

void DemandTable::DemandTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	DemandTable *polTable = (DemandTable *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
	{
		polTable->UpdateView();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE)
	{
		polTable->HandleGlobalDateUpdate();
	}
}

//*****
/////////////////////////////////////////////////////////////////////////////
// DemandTable -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a job (flight or non flight dependend).
//
// and the user will have opportunities to:
//	-- drop from StaffTable (single or multiple staff shift) onto any non-empty lines.
//		We will create normal flight or flight independent jobs for those staffs.
//	-- drop from DutyBar onto onto any non-empty lines.
//		We will create a normal flight job for that staff.
//

LONG DemandTable::OnTableDragBegin(UINT wParam, LONG lParam)
{
		

	CCSTABLENOTIFY *polNotify = reinterpret_cast<CCSTABLENOTIFY *>(lParam); //Singapore
	if(SetActiveTableAndViewer(polNotify) == FALSE)
	{
		return 0L;
	}

	CUIntArray olSelectedLines;
	int ilNumSelectedLines = GetSelectedLines(olSelectedLines);
	DEMDATA_LINE *prlLine = NULL;
	CDWordArray olDemUrnos;

	if(ilNumSelectedLines > 0)
	{
		TIMEPACKET olTimePacket;
		olTimePacket.StartTime = TIMENULL;
		olTimePacket.EndTime = TIMENULL;
		for (int i = 0; i < ilNumSelectedLines; i++)
		{
			if((prlLine = (DEMDATA_LINE *)pomActiveTable->GetTextLineData(olSelectedLines[i])) != NULL) //Singapore
			{
				if(olTimePacket.StartTime == TIMENULL || prlLine->Debe < olTimePacket.StartTime)
					olTimePacket.StartTime = prlLine->Debe;
				if(olTimePacket.EndTime == TIMENULL || prlLine->Deen > olTimePacket.EndTime)
					olTimePacket.EndTime   = prlLine->Deen;

				olDemUrnos.Add(prlLine->DemUrno);
				if (prlLine->IsTeamLine && !prlLine->Expanded)
				{
					for (int i = 0; i < prlLine->TeamLines.GetSize(); i++)
						olDemUrnos.Add(prlLine->TeamLines[i].DemUrno);
				}
			}
		}
		ogCCSDdx.DataChanged(this,STAFFDIAGRAM_UPDATETIMEBAND,&olTimePacket);

		int ilNumDemUrnos = olDemUrnos.GetSize();
		m_DragDropTarget.CreateDWordData(DIT_DEMANDBAR, ilNumDemUrnos);
		for(int ilDU = 0; ilDU < ilNumDemUrnos; ilDU++)
		{
		
		
			m_DragDropTarget.AddDWord(olDemUrnos[ilDU]);


		}
		
		
		
		m_DragDropTarget.BeginDrag();
	}

	return 0L;
}

LONG DemandTable::OnDragOver(UINT wParam, LONG lParam)
{
  
		
	int ilClass = m_DragDropTarget.GetDataClass(); 
	if (ilClass != DIT_DUTYBAR && ilClass != DIT_EQUIPMENT)
		return -1L;	// cannot interpret this object

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	if(FindActiveTableAndViewer(olDropPosition) == FALSE) //Singapore
	{
		CheckScrolling(olDropPosition); //Singapore
		return -1L;
	}
	CheckScrolling(olDropPosition); //Singapore
	pomActiveTable->GetCTableListBox()->ScreenToClient(&olDropPosition);
	int ilDropLineno = pomActiveTable->GetLinenoFromPoint(olDropPosition);
	if (!(0 <= ilDropLineno && ilDropLineno <= pomActiveViewer->Lines() - 1))
		return -1L;	// cannot drop on a blank line

	DEMDATA_LINE *prlLine = pomActiveViewer->GetLine(ilDropLineno);
	if (!prlLine)
		return -1L;

	imDropDemandUrno = prlLine->DemUrno;

	DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(imDropDemandUrno);
	if(prlDem == NULL)
		return -1L;

	if(ilClass == DIT_EQUIPMENT && !ogDemandData.DemandIsOfType(prlDem, EQUIPMENTDEMANDS))
		return -1L; // can only drop equipment onto equipment demands

	if(ilClass == DIT_DUTYBAR && !ogDemandData.DemandIsOfType(prlDem, PERSONNELDEMANDS))
		return -1L; // can only drop employees onto personnel demands

	return 0L;
}

LONG DemandTable::OnDrop(UINT wParam, LONG lParam)
{ 
	
	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	if(FindActiveTableAndViewer(olDropPosition) == FALSE) //Singapore
	{
		return -1L;
	}
	pomActiveTable->GetCTableListBox()->ScreenToClient(&olDropPosition);
	int ilDropLineno = pomActiveTable->GetLinenoFromPoint(olDropPosition);
	if (!(0 <= ilDropLineno && ilDropLineno <= pomActiveViewer->Lines() - 1))
		return -1L;	// cannot drop on a blank line

	DEMDATA_LINE *prlLine = pomActiveViewer->GetLine(ilDropLineno);
	if (!prlLine)
		return -1L;

	imDropDemandUrno = prlLine->DemUrno;

    int ilClass = m_DragDropTarget.GetDataClass(); 
	if(ilClass == DIT_DUTYBAR)
	{
		if (strcmp(prlLine->Obty,"FID") == 0)
		{
			// create job flight from duty with (default) demand
			CDWordArray olPoolJobUrnos;
			int ilDataCount = m_DragDropTarget.GetDataCount() - 1;
			if (ilDataCount > 0)
			{
				for (int ilPoolJob = 0; ilPoolJob < ilDataCount; ilPoolJob++)
				{
					olPoolJobUrnos.Add(m_DragDropTarget.GetDataDWord(ilPoolJob));
				}

				bool blTeamAlloc = m_DragDropTarget.GetDataDWord(ilDataCount) == 1 ? true : false;

				ogDataSet.CreateFlightIndependentJobs(this, olPoolJobUrnos, imDropDemandUrno);
			}
		}
		else
		{
		    
			
			
			CDWordArray olPoolJobUrnos;
			int ilDataCount = m_DragDropTarget.GetDataCount() - 1;
			if (ilDataCount > 0)
			{
				for (int ilPoolJob = 0; ilPoolJob < ilDataCount; ilPoolJob++)
				{
					olPoolJobUrnos.Add(m_DragDropTarget.GetDataDWord(ilPoolJob));
				}

				bool blTeamAlloc = m_DragDropTarget.GetDataDWord(ilDataCount) == 1 ? true : false;

				DEMANDDATA *prlDemData = ogDemandData.GetDemandByUrno(imDropDemandUrno);
				if (prlDemData)
				{
					if (prlDemData->Dety[0] == CCI_DEMAND)
						
					{
				
						ogDataSet.CreateCommonCheckinCounterJobs(this,olPoolJobUrnos,prlDemData->Urno);
					}
					else
					{
						long llFlightUrno = prlDemData->Ouri;
						if (prlDemData->Dety[0] == OUTBOUND_DEMAND)
							llFlightUrno = prlDemData->Ouro;
						if (prlLine->IsTeamLine && !prlLine->Expanded)	// team alloc ?
						{
							ogDataSet.CreateJobFlightFromDuty(this,olPoolJobUrnos,llFlightUrno,prlDemData->Aloc,prlDemData->Alid,blTeamAlloc,NULL);
						}
						else
						{
							CCSPtrArray <DEMANDDATA> olDemands;
							olDemands.Add(prlDemData);
							ogDataSet.CreateJobFlightFromDuty(this,olPoolJobUrnos,llFlightUrno,prlDemData->Aloc,prlDemData->Alid,blTeamAlloc,&olDemands);
						}
					}
				}
			}
		}
	}
	else if(ilClass == DIT_EQUIPMENT)
	{
		// a piece of equipment dropped on an equipment demand
		long llEquUrno = m_DragDropTarget.GetDataDWord(0);
		ogDataSet.CreateEquipmentJob(GetParent(), llEquUrno, prlLine->DemUrno);
	}
	//Singapore
	OnTableUpdateDataCount();
	return 0L;
}

void DemandTable::OnClose() 
{
   	pogButtonList->m_wndDemandTable = NULL;
	pogButtonList->pomDemandTableButton->Recess(FALSE);
	CDialog::OnClose();
}

void DemandTable::OnSelchangeDate() 
{
	SetViewerDate();
	UpdateView();
}

void DemandTable::SetViewerDate()
{
	CTime olStart = ogBasicData.GetTimeframeStart();
	CTime olDay = CTime(olStart.GetYear(),olStart.GetMonth(),olStart.GetDay(),0,0,0)  + CTimeSpan(m_Date.GetCurSel(),0,0,0);

	pomViewer->SetStartTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0));
	pomViewer->SetEndTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59));

	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		pomViewerT3->SetStartTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0));
		pomViewerT3->SetEndTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59));
	}
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void DemandTable::HandleGlobalDateUpdate()
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
	OnSelchangeDate();
}

LONG DemandTable::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	CCSTABLENOTIFY *polNotify = reinterpret_cast<CCSTABLENOTIFY *>(lpLParam); //Singapore
	if(SetActiveTableAndViewer(polNotify) == FALSE)
	{
		return 0L;
	}
	
	UINT ilNumLines = pomActiveViewer->Lines();
	if (ipItem >= 0 && ipItem < ilNumLines)
	{
		DEMDATA_LINE *prlLine = pomActiveViewer->GetLine(ipItem);
		if (prlLine != NULL)
		{
			DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prlLine->DemUrno);
			if (prlDemand)
			{
				int ilDemandType = ogDemandData.GetDemandType(prlDemand);

				if (!ogDemandData.IsFlightDependingDemand(prlDemand->Dety))
				{
					if (prlDemand->Dety[0] == CCI_DEMAND)
						new CciDemandDetailDlg(this,prlDemand->Urno);
				}
				else if (prlDemand->Dety[0] == OUTBOUND_DEMAND)
					// lli: we should not pass the Ouri if the demand is OUTBOUND otherwise the arrival flight mask will be shown
					new FlightDetailWindow(this,0,prlDemand->Ouro,FALSE,prlDemand->Aloc, NULL, ilDemandType, prlDemand->Debe);
				else
					new FlightDetailWindow(this,prlDemand->Ouri,prlDemand->Ouro,FALSE,prlDemand->Aloc, NULL, ilDemandType, prlDemand->Debe);
			}
		}
	}

	return 0L;
}

LONG DemandTable::OnTableSelchange(UINT wParam,LONG lParam)
{
	CUIntArray olSelectedLines;
	int ilNumSelectedLines = GetSelectedLines(olSelectedLines);
	DEMDATA_LINE *prlLine = NULL;
	if(ilNumSelectedLines > 0)
	{
		TIMEPACKET olTimePacket;
		olTimePacket.StartTime = TIMENULL;
		olTimePacket.EndTime = TIMENULL;
		for (int i = 0; i < ilNumSelectedLines; i++)
		{
			if((prlLine = (DEMDATA_LINE *)pomActiveTable->GetTextLineData(olSelectedLines[i])) != NULL) //Singapore
			{
				if(olTimePacket.StartTime == TIMENULL || prlLine->Debe < olTimePacket.StartTime)
					olTimePacket.StartTime = prlLine->Debe;
				if(olTimePacket.EndTime == TIMENULL || prlLine->Deen > olTimePacket.EndTime)
					olTimePacket.EndTime   = prlLine->Deen;
			}
		}
		ogCCSDdx.DataChanged(this,STAFFDIAGRAM_UPDATETIMEBAND,&olTimePacket);
	}

	return 0l;
}

LONG DemandTable::OnTableLButtonDown(UINT ipItem, LONG lpLParam)
{
	CCSTABLENOTIFY *polNotify = reinterpret_cast<CCSTABLENOTIFY *>(lpLParam); //Singapore
	if(SetActiveTableAndViewer(polNotify) == FALSE)
	{
		return 0L;
	}

	return 0l;
}

int DemandTable::GetSelectedLines(CUIntArray &ropSelectedLines)
{
	int ilSelCount = 0;
    CListBox *polListBox = pomActiveTable->GetCTableListBox(); //Singapore
	if(polListBox)
	{
		if((ilSelCount = polListBox->GetSelCount()) > 0)
		{
			int *polSelectedLines = new int[ilSelCount];
			polListBox->GetSelItems(ilSelCount,polSelectedLines);
			for(int ilC = 0; ilC < ilSelCount; ilC++)
				ropSelectedLines.Add(polSelectedLines[ilC]);
			delete [] polSelectedLines;
		}
	}

	return ilSelCount;
}

LONG DemandTable::OnTableRButtonDown(UINT ipItem, LONG lpLParam)
{
	CCSTABLENOTIFY *polNotify = reinterpret_cast<CCSTABLENOTIFY *>(lpLParam); //Singapore
	if(SetActiveTableAndViewer(polNotify) == FALSE)
	{
		return 0L;
	}

	CUIntArray olSelectedLines;
	int ilNumSelectedLines = GetSelectedLines(olSelectedLines);
	if(ilNumSelectedLines > 0)
	{
		CMenu olMenu;
		olMenu.CreatePopupMenu();
		CCSTABLENOTIFY *polNotify = reinterpret_cast<CCSTABLENOTIFY *>(lpLParam);
		ASSERT(polNotify);
		CPoint olPoint(polNotify->Point);

		if(ogBasicData.IsPaxServiceT2() == true)
		{
			if(omActiveTerminal == Terminal3 && bmIsT2Visible == TRUE)
			{
				CRect olRect;
				pomTable->GetClientRect(&olRect);
				olPoint.y += olRect.Height();
			}
		}

		DEMANDDATA *prlDem = NULL;
		bool blCannotEdit = false, blEquipmentDems = false, blPersonnelDems = false, blDeactivatedDems = false, blAutoAssignActivated = true;
		DEMDATA_LINE *prlLine = NULL;
		for (int i = 0; i < ilNumSelectedLines; i++)
		{
			//Singapore
			if((prlLine = (DEMDATA_LINE *)pomActiveTable->GetTextLineData(olSelectedLines[i])) != NULL &&
				(prlDem = ogDemandData.GetDemandByUrno(prlLine->DemUrno)) != NULL)
			{
				if(!strcmp(prlLine->Obty,"FID"))
					blCannotEdit = true;

				if(ogDemandData.DemandIsOfType(prlDem,PERSONNELDEMANDS))
					blPersonnelDems = true;
				else if(ogDemandData.DemandIsOfType(prlDem,EQUIPMENTDEMANDS))
					blEquipmentDems = true;

				if (!ogDemandData.AutomaticAssignmentEnabled(prlDem))
					blAutoAssignActivated = false;

				if(ogDemandData.DemandIsDeactivated(prlDem))
					blDeactivatedDems = true;
			}
		}

		olMenu.AppendMenu(MF_STRING,11,GetString(IDS_STRING61352));	// edit
		if(blPersonnelDems)
			olMenu.AppendMenu(MF_STRING,12, GetString(IDS_STRING61376));	// available employees
		if(blEquipmentDems)
			olMenu.AppendMenu(MF_STRING,14, GetString(IDS_AVAILABLEEQUIPMENT));	// available equipment
		olMenu.AppendMenu(blAutoAssignActivated ? MF_STRING|MF_CHECKED : MF_STRING|MF_UNCHECKED,13,GetString(IDS_STRING62504)); // Auto Assign
		olMenu.AppendMenu(MF_STRING,15,blDeactivatedDems ? GetString(IDS_DEMACTIVE) : GetString(IDS_DEMDEACTIVE));// "Activate/Deactivte Demand"

		if(ilNumSelectedLines > 0 || blCannotEdit)
			olMenu.EnableMenuItem(11,MF_BYCOMMAND || MF_GRAYED);

		if(!bgOnline)
		{
			olMenu.EnableMenuItem(15,MF_BYCOMMAND || MF_GRAYED);
			olMenu.EnableMenuItem(13,MF_BYCOMMAND || MF_GRAYED);
		}

		ClientToScreen( &olPoint);
		olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y + 55, this, NULL);
	}
	return 0L;
}

void DemandTable::OnMenuWorkOn()
{
	CUIntArray olSelectedLines;
	int ilNumSelectedLines = GetSelectedLines(olSelectedLines);
	DEMDATA_LINE *prlLine = NULL;
	CDWordArray olDemUrnos;

	if(ilNumSelectedLines == 1)
	{
		if((prlLine = (DEMDATA_LINE *)pomActiveTable->GetTextLineData(olSelectedLines[0])) != NULL) //Singapore
		{
			DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prlLine->DemUrno);
			if (prlDemand)
			{
				int ilDemandType = ogDemandData.GetDemandType(prlDemand);
				if (!ogDemandData.IsFlightDependingDemand(prlDemand->Dety))
				{
					if (prlDemand->Dety[0] == CCI_DEMAND)
						new CciDemandDetailDlg(this,prlDemand->Urno);
				}
				else if (prlDemand->Dety[0] == OUTBOUND_DEMAND)
					new FlightDetailWindow(this,prlDemand->Ouro,0l,FALSE,prlDemand->Aloc,NULL,ilDemandType, prlDemand->Debe);
				else
					new FlightDetailWindow(this,prlDemand->Ouri,prlDemand->Ouro,FALSE,prlDemand->Aloc,NULL,ilDemandType, prlDemand->Debe);
			}
		}
	}
}

void DemandTable::OnMenuAvailableEmployees()
{
	CUIntArray olSelectedLines;
	int ilNumSelectedLines = GetSelectedLines(olSelectedLines);
	DEMDATA_LINE *prlLine = NULL;
	CDWordArray olDemUrnos;
	CCSPtrArray <DEMANDDATA> olDemands;

	if(ilNumSelectedLines > 0)
	{
		for (int i = 0; i < ilNumSelectedLines; i++)
		{
			if((prlLine = (DEMDATA_LINE *)pomActiveTable->GetTextLineData(olSelectedLines[i])) != NULL) //Singapore
			{
				DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prlLine->DemUrno);
				if(prlDemand != NULL && ogDemandData.DemandIsOfType(prlDemand, PERSONNELDEMANDS))
				{
					olDemands.Add(prlDemand);
				}
			}
		}

		DEMANDDATA *prlDemand = NULL;
		bool blAssigning = true;
		while(blAssigning)
		{
			blAssigning = false;
			int ilNumEmps = 0;
			bool blDemandsToAssign = false;
			CAvailableEmpsDlg olAvailableEmpsDlg;
			//olAvailableEmpsDlg.SetAllocUnit(olAloc, olAlid);
			olAvailableEmpsDlg.SetCaptionObject("");
			int ilNumDems = olDemands.GetSize();
			for(int ilD = 0; ilD < ilNumDems; ilD++)
			{
				prlDemand = &olDemands[ilD];
				if(!ogJodData.DemandHasJobs(prlDemand->Urno))
				{
					ilNumEmps += olAvailableEmpsDlg.GetAvailableEmployeesForDemand(prlDemand, true);
					olAvailableEmpsDlg.AddDemand(prlDemand);
					blDemandsToAssign = true;
				}
			}
			if(blDemandsToAssign)
			{
				if(ilNumEmps <= 0)
				{
					// no employees available for the demands
					MessageBox(GetString(IDS_STRING61266), GetString(IDS_AVAILEMPSCAPTION), MB_ICONEXCLAMATION);
				}
				else
				{
					if(olAvailableEmpsDlg.DoModal() == IDOK && olAvailableEmpsDlg.omSelectedPoolJobUrnos.GetSize() > 0)
					{
						blAssigning = true;
						prlDemand = olAvailableEmpsDlg.prmSelectedDemand;

						if(!strcmp(prlDemand->Obty,"FID"))
						{
							ogDataSet.CreateFlightIndependentJobs(this, olAvailableEmpsDlg.omSelectedPoolJobUrnos,prlDemand->Urno);
						}
						else if(prlDemand->Dety[0] == CCI_DEMAND)
						{
							ogDataSet.CreateCommonCheckinCounterJobs(this, olAvailableEmpsDlg.omSelectedPoolJobUrnos,prlDemand->Urno);
						}
						else
						{
							CCSPtrArray <DEMANDDATA> olDemandsToAssign;
							olDemandsToAssign.Add(prlDemand);
							ogDataSet.CreateJobFlightFromDuty(this, olAvailableEmpsDlg.omSelectedPoolJobUrnos,ogDemandData.GetFlightUrno(prlDemand), prlDemand->Aloc, prlDemand->Alid, false, &olDemandsToAssign);
						}
					}
				}
			}
		}
	}
}



void DemandTable::OnMenuAvailableEquipment()
{
	CUIntArray olSelectedLines;
	int ilNumSelectedLines = GetSelectedLines(olSelectedLines);
	DEMDATA_LINE *prlLine = NULL;
	CDWordArray olDemUrnos;
	CCSPtrArray <DEMANDDATA> olDemands;

	if(ilNumSelectedLines > 0)
	{
		for (int i = 0; i < ilNumSelectedLines; i++)
		{
			if((prlLine = (DEMDATA_LINE *)pomActiveTable->GetTextLineData(olSelectedLines[i])) != NULL) //Singapore
			{
				DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prlLine->DemUrno);
				if(prlDemand != NULL && ogDemandData.DemandIsOfType(prlDemand, EQUIPMENTDEMANDS))
				{
					olDemands.Add(prlDemand);
				}
			}
		}

		DEMANDDATA *prlDemand = NULL;
		bool blAssigning = true;
		while(blAssigning)
		{
			blAssigning = false;
			int ilNumEqu = 0;
			bool blDemandsToAssign = false;
			CAvailableEquipmentDlg olAvailableEquDlg;
			//olAvailableEquDlg.SetAllocUnit(olAloc, olAlid);
			olAvailableEquDlg.SetCaptionObject("");
			int ilNumDems = olDemands.GetSize();
			for(int ilD = 0; ilD < ilNumDems; ilD++)
			{
				prlDemand = &olDemands[ilD];
				if(!ogJodData.DemandHasJobs(prlDemand->Urno))
				{
					ilNumEqu += olAvailableEquDlg.GetAvailableEquipmentForDemand(prlDemand, true);
					olAvailableEquDlg.AddDemand(prlDemand);
					blDemandsToAssign = true;
				}
			}
			if(blDemandsToAssign)
			{
				if(ilNumEqu <= 0)
				{
					// no equipment available for the demands
					MessageBox(GetString(IDS_STRING61266), GetString(IDS_AVEQ_CAPTION), MB_ICONEXCLAMATION);
				}
				else
				{
					if(olAvailableEquDlg.DoModal() == IDOK)
					{
						blAssigning = true;
						for(int ilE = 0; ilE < olAvailableEquDlg.omSelectedEquUrnos.GetSize(); ilE++)
						{
							prlDemand = olAvailableEquDlg.prmSelectedDemand;
							long llEquUrno = olAvailableEquDlg.omSelectedEquUrnos[ilE];
							ogDataSet.CreateEquipmentJob(GetParent(), llEquUrno, prlDemand->Urno);
						}
					}
				}
			}
		}
	}
}


void DemandTable::OnMenuAutomaticAssignment()
{
	CUIntArray olSelectedLines;
	int ilNumSelectedLines = GetSelectedLines(olSelectedLines), i;
	DEMDATA_LINE *prlLine = NULL;
	CDWordArray olDemUrnos;
	CCSPtrArray <DEMANDDATA> olDemands;

	bool blEnable = false;
	if(ilNumSelectedLines > 0)
	{
		for(i = 0; i < ilNumSelectedLines; i++)
		{
			if((prlLine = (DEMDATA_LINE *)pomActiveTable->GetTextLineData(olSelectedLines[i])) != NULL) //Singapore
			{
				DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prlLine->DemUrno);
				if(!ogDemandData.AutomaticAssignmentEnabled(prlDemand))
				{
					blEnable = true;
					break;
				}
			}
		}
		for(i = 0; i < ilNumSelectedLines; i++)
		{
			if((prlLine = (DEMDATA_LINE *)pomActiveTable->GetTextLineData(olSelectedLines[i])) != NULL)
			{
				ogDataSet.ChangeDemandAutoAssignment(this,prlLine->DemUrno,blEnable);
			}
		}
	}
}

void DemandTable::OnMenuDeactivateDemand()
{
	CUIntArray olSelectedLines;
	int ilNumSelectedLines = GetSelectedLines(olSelectedLines), i;
	DEMDATA_LINE *prlLine = NULL;
	CDWordArray olDemUrnos;
	CCSPtrArray <DEMANDDATA> olDemands;

	bool blDeactivatedDemandsFound = false;
	if(ilNumSelectedLines > 0)
	{
		for(i = 0; i < ilNumSelectedLines; i++)
		{
			if((prlLine = (DEMDATA_LINE *)pomActiveTable->GetTextLineData(olSelectedLines[i])) != NULL) //Singapore
			{
				DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prlLine->DemUrno);
				if(ogDemandData.DemandIsDeactivated(prlDemand))
				{
					blDeactivatedDemandsFound = true;
					break;
				}
			}
		}
		for(i = 0; i < ilNumSelectedLines; i++)
		{
			if((prlLine = (DEMDATA_LINE *)pomActiveTable->GetTextLineData(olSelectedLines[i])) != NULL)
			{
				ogDataSet.DeactivateDemand(this,prlLine->DemUrno,!blDeactivatedDemandsFound);
			}
		}
	}
}

//Singapore
void DemandTable::OnTableUpdateDataCount()
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			if(ogBasicData.IsPaxServiceT2() == true)
			{
				char olDataCountT2[10];
				char olDataCountT3[10];
				polWnd->EnableWindow();			
				itoa(pomViewer->Lines(),olDataCountT2,10);
				itoa(pomViewerT3->Lines(),olDataCountT3,10);		
				CString olText;
				olText.Format("%s/%s",olDataCountT2,olDataCountT3);
				polWnd->SetWindowText(olText);
			}
		}
	}
}

//Singapore
BOOL DemandTable::FindActiveTableAndViewer(CPoint& ropPoint)
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{	
		CPoint olPoint = ropPoint;
		CRect olClientRect;
	
		if(bmIsT2Visible == FALSE && bmIsT3Visible == FALSE)
		{
			return FALSE;
		}

		int ilOffset = 0;
		SCROLLINFO scrollInfo;
		BOOL blScrollInfo = pomTable->GetCTableListBox()->GetScrollInfo(SB_HORZ,&scrollInfo);
		int ilHorzExtent = pomTable->GetCTableListBox()->GetHorizontalExtent();

		if(blScrollInfo == TRUE)
		{
			ilOffset = ilHorzExtent > scrollInfo.nPage ? pomTable->imItemHeight : 0;
		}

		pomTable->GetCTableListBox()->GetWindowRect(&olClientRect);	
		if(bmIsT2Visible == TRUE && (olClientRect.bottom - ilOffset) >=ropPoint.y && olClientRect.top <= ropPoint.y)
		{			
			pomActiveTable = pomTable;
			pomActiveViewer = pomViewer;
			omActiveTerminal = Terminal2;
			return TRUE;
		}
		else
		{	
			blScrollInfo = pomTableT3->GetCTableListBox()->GetScrollInfo(SB_HORZ,&scrollInfo);
			ilHorzExtent = pomTableT3->GetCTableListBox()->GetHorizontalExtent();

			if(blScrollInfo == TRUE)
			{
				ilOffset = ilHorzExtent > scrollInfo.nPage ? pomTableT3->imItemHeight : 0;
			}

			pomTableT3->GetCTableListBox()->GetWindowRect(&olClientRect);	
			if(bmIsT3Visible == TRUE && (olClientRect.bottom - ilOffset) >=ropPoint.y && olClientRect.top <= ropPoint.y)
			{				
				pomActiveTable = pomTableT3;
				pomActiveViewer = pomViewerT3;
				omActiveTerminal = Terminal3;
				return TRUE;
			}
		}
	}
	else
	{
		pomActiveTable = pomTable;
		pomActiveViewer = pomViewer;
		return TRUE;
	}
	return FALSE;
}

//Singapore
BOOL DemandTable::SetActiveTableAndViewer(CCSTABLENOTIFY *prpNotify)
{
	if(prpNotify != NULL)
	{
		if(ogBasicData.IsPaxServiceT2() == true)
		{
			if(pomTable == (CCSTable*)prpNotify->SourceTable)
			{
				pomActiveTable = pomTable;
				pomActiveViewer = pomViewer;
				omActiveTerminal = Terminal2;
				pomTableT3->GetCTableListBox()->SetSel(-1,FALSE);
				return TRUE;
			}
			else if(pomTableT3 == (CCSTable*)prpNotify->SourceTable)
			{
				pomActiveTable = pomTableT3;
				pomActiveViewer = pomViewerT3;
				omActiveTerminal = Terminal3;
				pomTable->GetCTableListBox()->SetSel(-1,FALSE);
				return TRUE;
			}
		}
		else
		{
			pomActiveTable = pomTable;
			pomActiveViewer = pomViewer;
			return TRUE;
		}
	}
	else
	{
		return FALSE;
	}
	return FALSE;
}

//Singapore
void DemandTable::OnButtonT2()
{
	CRect olClientRect;
	GetWindowRect(&olClientRect);
	ScreenToClient(&olClientRect);

	CButton* polButton = (CButton*)GetDlgItem(IDC_BUTTONT2);	
	bmIsT2Visible = !bmIsT2Visible;

	if(bmIsT2Visible == TRUE)
	{
		m_bTerminal2.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	else
	{
		m_bTerminal2.SetColors(::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}

	if(bmIsT2Visible == FALSE && bmIsT3Visible == FALSE)
	{	
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this);
		pomTable->ShowWindow(SW_HIDE);
	}	
	else if(bmIsT2Visible == TRUE && bmIsT3Visible == FALSE)
	{	
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this,pomTable->GetCTableListBox());
		pomTable->ShowWindow(SW_SHOW);
				
		CRect olRectT2;
		pomTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);
		
		CRect olRectT3;
		pomTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		pomTable->SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	if(bmIsT2Visible == FALSE && bmIsT3Visible == TRUE)
	{		
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this,pomTableT3->GetCTableListBox());
		
		CRect olRectT2;
		pomTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		pomTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		pomTable->ShowWindow(SW_HIDE);
		pomTableT3->SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	else if(bmIsT2Visible == TRUE && bmIsT3Visible == TRUE)
	{		
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this);
		pomTable->ShowWindow(SW_SHOW);
		
		CRect olRectT2;
		pomTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		pomTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		pomTable->SetPosition(-1,olClientRect.right-3,olRectT2.top,(olClientRect.bottom - olRectT2.top)/2);
		pomTableT3->SetPosition(-1,olClientRect.right-3,(olClientRect.bottom - olRectT2.top)/2,olClientRect.bottom-3);
	}
}

//Singapore
void DemandTable::OnButtonT3()
{
	CRect olClientRect;
	GetWindowRect(&olClientRect);
	ScreenToClient(&olClientRect);

	CButton* polButton = (CButton*)GetDlgItem(IDC_BUTTONT3);	
	bmIsT3Visible = !bmIsT3Visible;

	if(bmIsT3Visible == TRUE)
	{
		m_bTerminal3.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));		
	}
	else
	{
		m_bTerminal3.SetColors(::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));		
	}

	if(bmIsT3Visible == FALSE && bmIsT2Visible == FALSE)
	{	
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this);
		pomTableT3->ShowWindow(SW_HIDE);

	}	
	else if(bmIsT3Visible == TRUE && bmIsT2Visible == FALSE)
	{	
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this,pomTableT3->GetCTableListBox());
		pomTableT3->ShowWindow(SW_SHOW);
				
		CRect olRectT2;
		pomTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);
		
		CRect olRectT3;
		pomTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		pomTableT3->SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	else if(bmIsT3Visible == FALSE && bmIsT2Visible == TRUE)
	{	
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this,pomTable->GetCTableListBox());

		CRect olRectT2;
		pomTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		pomTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);
						
		pomTableT3->ShowWindow(SW_HIDE);
		pomTable->SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	else if(bmIsT3Visible == TRUE && bmIsT2Visible == TRUE)
	{
		m_DragDropTarget.Revoke();
		m_DragDropTarget.RegisterTarget(this, this);
		pomTableT3->ShowWindow(SW_SHOW);

		CRect olRectT2;
		pomTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		pomTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);
		
		pomTable->SetPosition(-1,olClientRect.right-3,olRectT2.top,(olClientRect.bottom - olRectT2.top)/2);
		pomTableT3->SetPosition(-1,olClientRect.right-3,(olClientRect.bottom - olRectT2.top)/2,olClientRect.bottom-3);
	}
}

//Singapore
void DemandTable::CheckScrolling(CPoint& ropPoint)
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{		
		if(bmIsT2Visible == TRUE && bmIsT3Visible == TRUE)
		{
			const int ilHorzOffset = 15;
			CPoint olPoint = ropPoint;
			CRect olClientRect;
			CRect olClientListRect;
		
			pomTable->GetWindowRect(&olClientRect);	
			pomTable->GetCTableListBox()->GetWindowRect(&olClientListRect);

			if(olClientListRect.bottom >= ropPoint.y && olClientListRect.top <= ropPoint.y)
			{
				if((olClientRect.left + ilHorzOffset) >= ropPoint.x)
				{
					pomTable->GetCTableListBox()->SendMessage(WM_HSCROLL,SB_LINELEFT,0);
				}
				else if((olClientRect.right - ilHorzOffset) <= ropPoint.x)
				{
					pomTable->GetCTableListBox()->SendMessage(WM_HSCROLL,SB_LINERIGHT,0);
				}
			}
			else
			{
				pomTableT3->GetWindowRect(&olClientRect);
				pomTableT3->GetCTableListBox()->GetWindowRect(&olClientListRect);

				if(olClientListRect.bottom >= ropPoint.y && olClientListRect.top <= ropPoint.y)
				{					
					if((olClientRect.left + ilHorzOffset) >= ropPoint.x)
					{
						pomTableT3->GetCTableListBox()->SendMessage(WM_HSCROLL,SB_LINELEFT,0);
					}
					else if((olClientRect.right - ilHorzOffset) <= ropPoint.x)
					{
						pomTableT3->GetCTableListBox()->SendMessage(WM_HSCROLL,SB_LINERIGHT,0);
					}
				}
			}

			pomTable->GetWindowRect(&olClientRect);	
			pomTable->GetCTableListBox()->GetWindowRect(&olClientListRect);

			if((olClientRect.bottom - pomTable->imItemHeight) <=ropPoint.y && olClientRect.bottom >=ropPoint.y)
			{
				pomTable->GetCTableListBox()->SendMessage(WM_VSCROLL,SB_LINEDOWN,0);
			}
			else if(olClientListRect.top >=ropPoint.y && (olClientListRect.top - pomTable->imItemHeight) <= ropPoint.y)
			{
				pomTable->GetCTableListBox()->SendMessage(WM_VSCROLL,SB_LINEUP,0);
			}
			else
			{				
				pomTableT3->GetWindowRect(&olClientRect);		
				pomTableT3->GetCTableListBox()->GetWindowRect(&olClientListRect);

				if((olClientRect.bottom - pomTable->imItemHeight) <=ropPoint.y && olClientRect.bottom >=ropPoint.y)
				{
					pomTableT3->GetCTableListBox()->SendMessage(WM_VSCROLL,SB_LINEDOWN,0);
				}
				else if(olClientListRect.top >=ropPoint.y && (olClientListRect.top - pomTableT3->imItemHeight) <= ropPoint.y)
				{
					pomTableT3->GetCTableListBox()->SendMessage(WM_VSCROLL,SB_LINEUP,0);
				}
			}
		}
	}
}

//PRF 8712
void DemandTable::OnMove(int x, int y)
{
	CDialog::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}