// GateTableSortPage.h : header file
//
#ifndef _GATETBSO_H_
#define _GATETBSO_H_
/////////////////////////////////////////////////////////////////////////////
// GateTableSortPage dialog

class GateTableSortPage : public CPropertyPage
{
	DECLARE_DYNCREATE(GateTableSortPage)

// Construction
public:
	GateTableSortPage();
	~GateTableSortPage();

// Dialog Data
	CStringArray omSortOrders;
	CString omTitle;

	//{{AFX_DATA(GateTableSortPage)
	enum { IDD = IDD_GATETABLE_SORT_PAGE };
	BOOL	m_Group;
	int		m_SortOrder0;
	int		m_SortOrder1;
	int		m_SortOrder2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(GateTableSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(GateTableSortPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
};

#endif // _GATETBSO_H_
