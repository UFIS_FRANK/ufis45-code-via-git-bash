#if !defined(AFX_JOBWITHOUTDEMDLG_H__D70F1283_AC51_11D7_8259_00010215BFE5__INCLUDED_)
#define AFX_JOBWITHOUTDEMDLG_H__D70F1283_AC51_11D7_8259_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// JobWithoutDemDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// JobWithoutDemDlg dialog

class JobWithoutDemDlg : public CDialog
{
// Construction
public:
	JobWithoutDemDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(JobWithoutDemDlg)
	enum { IDD = IDD_JOBWITHOUTDEM_DLG };
	CTime		m_FromDate;
	CTime		m_FromTime;
	CTime		m_ToDate;
	CTime		m_ToTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(JobWithoutDemDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

public:
	CTime omFrom;
	CTime omTo;
	CString omAloc;
	long lmUtpl;
	void InitDefaultValues(CTime opFrom, CTime opTo, CString opAloc = "", long lpUtpl = 0L);
	void DisableAlocationUnit();
	void DisableTemplate();
	bool bmDisableAlocationUnit;
	bool bmDisableTemplate;

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(JobWithoutDemDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_JOBWITHOUTDEMDLG_H__D70F1283_AC51_11D7_8259_00010215BFE5__INCLUDED_)
