/////////////////////////////////////////////////////////////////////////////
// CNewWif dialog
#ifndef _NEWIF_H_
#define _NEWIF_H_
  
class CNewWif : public CDialog
{
// Construction
public:
	CNewWif(CWnd* pParent, char *pcpNewWif);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewWif)
	enum { IDD = IDD_NEWWIF };
	CEdit	m_NewWif;
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CNewWif)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewWif)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:

//Attributes
public:

protected:

private:
	char  *pcmNewWif;
};

#endif _NEWIF_C_