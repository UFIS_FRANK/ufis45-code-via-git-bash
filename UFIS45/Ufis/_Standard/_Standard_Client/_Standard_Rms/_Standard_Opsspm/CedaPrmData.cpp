// CedaPrmData.cpp: Auto assignment parameters for line maintenence (Reduktionstufen)
//
//////////////////////////////////////////////////////////////////////

#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaPrmData.h>
#include <BasicData.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CedaPrmData::CedaPrmData()
{                  
    BEGIN_CEDARECINFO(PRMDATA, PRMDATARecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Dscr,"DESC")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(PRMDATARecInfo)/sizeof(PRMDATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PRMDATARecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"PRMTAB");
    pcmFieldList = "URNO,DSCR";


}
 
CedaPrmData::~CedaPrmData()
{
	TRACE("CedaPrmData::~CedaPrmData called\n");
	ClearAll();
}

void CedaPrmData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omNameMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaPrmData::ReadPrmData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaPrmData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		PRMDATA rlPRMDATA;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlPRMDATA)) == RCSuccess)
			{
				AddPrmInternal(rlPRMDATA);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaPrmData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(PRMDATA), pclWhere);
    return ilRc;
}


void CedaPrmData::AddPrmInternal(PRMDATA &rrpPrm)
{
	PRMDATA *prlPrm = new PRMDATA;
	*prlPrm = rrpPrm;
	omData.Add(prlPrm);
	omUrnoMap.SetAt((void *)prlPrm->Urno,prlPrm);
	omNameMap.SetAt(prlPrm->Dscr,prlPrm);
}


PRMDATA* CedaPrmData::GetPrmByUrno(long lpUrno)
{
	PRMDATA *prlPrm = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlPrm);
	return prlPrm;
}

PRMDATA* CedaPrmData::GetPrmByName(const char *pcpName)
{
	PRMDATA *prlPrm = NULL;
	omNameMap.Lookup(pcpName, (void *&) prlPrm);
	return prlPrm;
}

long CedaPrmData::GetPrmUrnoByName(const char *pcpName)
{
	long llUrno = 0L;
	PRMDATA *prlPrm = GetPrmByName(pcpName);
	if(prlPrm != NULL)
	{
		llUrno = prlPrm->Urno;
	}
	return llUrno;
}


CString CedaPrmData::GetTableName(void)
{
	return CString(pcmTableName);
}


void CedaPrmData::GetAllPrmNames(CStringArray &ropPrmNames)
{
	int ilNumPrm = omData.GetSize();
	for(int ilPrm = 0; ilPrm < ilNumPrm; ilPrm++)
	{
		ropPrmNames.Add(omData[ilPrm].Dscr);
	}
}
