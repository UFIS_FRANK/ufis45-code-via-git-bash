// NameConfig.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <NameConfig.h>
#include <CCSGlobl.h>
#include <CedaCfgData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNameConfig


CNameConfig::CNameConfig()
{
	pomNameConfigurationDlg = NULL;
	bmInitialised = false;
}

CNameConfig::~CNameConfig()
{
	if(bmInitialised) delete pomNameConfigurationDlg;
}

bool CNameConfig::Initialise()
{
	bmInitialised = true;

	// create the name configuration dialog
	CStringArray olNcdStrings;
	olNcdStrings.Add(GetString(IDS_NCD_TITLE));
	olNcdStrings.Add(GetString(IDS_NCD_FIRSTNAME));
	olNcdStrings.Add(GetString(IDS_NCD_LASTNAME));
	olNcdStrings.Add(GetString(IDS_NCD_NICKNAME));
	olNcdStrings.Add(GetString(IDS_NCD_INITIALS));
	olNcdStrings.Add(GetString(IDS_NCD_PERSONNELNO));
	olNcdStrings.Add(GetString(IDS_NCD_FIELD));
	olNcdStrings.Add(GetString(IDS_NCD_NOOFCHARS));
	olNcdStrings.Add(GetString(IDS_NCD_SUFFIX));
	CString olPreview;
	olPreview.Format(GetString(IDS_NCD_PREVIEW),NAMECONFIG_MAXNAMELEN);
	olNcdStrings.Add(olPreview);

	if((pomNameConfigurationDlg = new NameConfigurationDlg(&olNcdStrings, "")) == NULL)
	{
		bmInitialised = false;
	}

	// load the saved name configuration for this user
	if(bmInitialised)
	{
		CFGDATA *prlCfg = ogCfgData.GetCfgByCtypAndCkey(NAMECONFIG_KEY, NAMECONFIG_TEXT);
		if(prlCfg != NULL)
		{
			omConfigString = prlCfg->Text;
		}
		pomNameConfigurationDlg->SetConfigString(omConfigString);
		pomNameConfigurationDlg->SetMaxNameLength(NAMECONFIG_MAXNAMELEN);
	}

	return bmInitialised;
}

// allow the user to set the name configuration parameters
bool CNameConfig::SetNameConfig()
{
	bool blConfigHasChanged = false;
	CString olOldConfigString = omConfigString;

	pomNameConfigurationDlg->SetConfigString(omConfigString);
	if(pomNameConfigurationDlg->DoModal() == IDOK)
	{
		AfxGetApp()->DoWaitCursor(1);
		omConfigString = pomNameConfigurationDlg->GetConfigString();
		if(omConfigString != olOldConfigString)
		{
			// save the name configuration for this user
			ogCfgData.DeleteByCtyp(NAMECONFIG_KEY);
			ogCfgData.CreateCfg(NAMECONFIG_KEY, NAMECONFIG_TEXT, omConfigString);
			blConfigHasChanged = true;
		}
		AfxGetApp()->DoWaitCursor(-1);
	}

	return blConfigHasChanged;
}

// return the employee name according to the configuration defined
CString CNameConfig::GetEmpName(char *pcpFinm, char *pcpLanm, char *pcpShnm, char *pcpPerc, char *pcpPeno)
{
	CString olEmpName;

	if(bmInitialised)
	{
		olEmpName = pomNameConfigurationDlg->GetNameString(pcpFinm, pcpLanm, pcpShnm, pcpPerc, pcpPeno);
	}
	else
	{
		olEmpName = "CNameConfig not initialised!";
	}

	return olEmpName;
}
