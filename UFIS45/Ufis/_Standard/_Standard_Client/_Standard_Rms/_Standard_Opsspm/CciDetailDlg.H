// CciDetailDlg.h : header file
//


#ifndef _CCIDD_
#define _CCIDD_


/////////////////////////////////////////////////////////////////////////////
// CciDetailDialog dialog

class CciDetailDialog : public CDialog
{
// Construction
public:
	CciDetailDialog(CWnd* pParent = NULL);   // standard constructor
	void SetAlocAlidPtr(char *pcpAlocAlid) { pcmAlocAlid = pcpAlocAlid; };

// Dialog Data
	//{{AFX_DATA(CciDetailDialog)
	enum { IDD = IDD_CCI_DETAIL };
	CString m_Alid;
	CString m_Type;
	CString m_Comm;
	CString m_Teln;
	CTime m_Nafr;
	CTime m_Nato;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CciDetailDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
    void OnOK();
    void OnCancel();

	// Generated message map functions
	//{{AFX_MSG(CciDetailDialog)
	afx_msg void OnSchlieben();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
    char *pcmAlocAlid;
};

#endif // _CCIDD_

