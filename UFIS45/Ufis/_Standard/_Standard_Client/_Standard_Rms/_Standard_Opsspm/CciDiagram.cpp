// ccidia.cpp : implementation file
//   

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaCom.h>
#include <CCSCedaData.h>
#include <cxbutton.h>
#include <CCSButtonctrl.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaShiftData.h>
#include <OpssPm.h>
#include <CCSDragDropCtrl.h>
#include <TimeScaleDlg.h>
#include <cxbutton.h>
#include <clientwn.h>
#include <cviewer.h>
#include <CciViewer.h>
#include <CciGantt.h>
#include <CciChart.h>
#include <CciDiagram.h>

#include <FilterPage.h>
#include <CciDiagramFilterPage.h>
#include <CciDiagramGroupPage.h>
#include <BasePropertySheet.h>
#include <CciDiagramPropertySheet.h>

#include <BasicData.h>
#include <ccsddx.h>
#include <CedaCfgData.h>
#include <conflict.h>

#include <CCITable.h>
#include <GateTable.h>
#include <ConflictTable.h>
#include <ConflictConfigTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <RegnDiagram.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <PrePlanTable.h>
#include <ButtonList.h>
#include <CiCDemandTable.h>
#include <CicAutoAssignDlg.h>
#include <AutoAssignDlg.h>
#include <cedapoldata.h>
#include <SelReportDlg.h>
#include <CicPrintDlg.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

const CString CCIDIA = "CciDia"; //Singapore
// Prototypes

/////////////////////////////////////////////////////////////////////////////
// CCIDiagram

IMPLEMENT_DYNCREATE(CCIDiagram, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern CString date;

CCIDiagram::CCIDiagram()
{
	bmNoUpdatesNow   = TRUE;
	bmScrolling = false;
	CCIDiagram(FALSE,ogBasicData.GetTime());
}

CCIDiagram::CCIDiagram(BOOL bpPrePlanMode,CTime opPrePlanTime)
{
	bmNoUpdatesNow = TRUE;

	omViewer.SetViewerKey(CCIDIA);
    imStartTimeScalePos = 50;
    imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = -1;

	omPrePlanTime = opPrePlanTime;
	omPrePlanMode = bpPrePlanMode;

	bmIsViewOpen = FALSE;
	m_wndDemandTable = NULL;

	CRect olRect;
	ogBasicData.GetWindowPosition(olRect, ogCfgData.rmUserSetup.CCCH,ogCfgData.rmUserSetup.MONS);

	olRect.top += 40;
	//WS_VISIBLE is commented out to avoid double screen display while resizing - PRF 8712
    ASSERT(Create(NULL,"CCI-Diagramm", WS_HSCROLL | WS_OVERLAPPEDWINDOW | /*WS_VISIBLE |*/ WS_POPUP,
		olRect,pogMainWnd,NULL,0,NULL));

	bmScrolling = false;
	SetCaptionText();

	BOOL blMinimized = FALSE;
	CRect olTempRect;
	ogBasicData.GetWindowPosition(olTempRect, ogCfgData.rmUserSetup.CCCH,ogCfgData.rmUserSetup.MONS);
	ogBasicData.GetDialogFromReg(olTempRect, COpssPmApp::CHECKIN_DIAGRAM_WINDOWPOS_REG_KEY,blMinimized);	
	
	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olTempRect);

	SetWindowPos(&wndTop, olTempRect.left,olTempRect.top, olTempRect.Width(), olTempRect.Height(), SWP_SHOWWINDOW);
	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}
}

CCIDiagram::~CCIDiagram()
{
    omPtrArray.RemoveAll();

	pomDemandTableButton->Detach();
	delete pomDemandTableButton;

	if(pomAssignButton != NULL)
	{
		pomAssignButton->Detach();
		delete pomAssignButton;
	}
}


BEGIN_MESSAGE_MAP(CCIDiagram, CFrameWnd)
    //{{AFX_MSG_MAP(CCIDiagram)
    ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_WM_GETMINMAXINFO()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_WM_SIZE()
    ON_WM_MOVE() //PRF 8712
    ON_WM_HSCROLL()
    ON_BN_CLICKED(IDC_PREV, OnNextChart)
    ON_BN_CLICKED(IDC_NEXT, OnPrevChart)
    ON_BN_CLICKED(IDC_MABSTAB, OnMabstab)
    ON_WM_TIMER()
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
	ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
    ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
	ON_BN_CLICKED(IDC_ZEIT, OnZeit)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_UPDATE_COMMAND_UI(IDC_ZEIT,OnUpdateUIZeit)
	ON_UPDATE_COMMAND_UI(IDC_MABSTAB,OnUpdateUIMabstab)
	ON_UPDATE_COMMAND_UI(IDC_PRINT,OnUpdateUIPrint)
	ON_UPDATE_COMMAND_UI(IDC_ASSIGN,OnUpdateUIAssign)
	ON_UPDATE_COMMAND_UI(IDC_ANSICHT,OnUpdateUIAnsicht)
	ON_UPDATE_COMMAND_UI(IDC_VIEW,OnUpdateUIView)
	ON_UPDATE_COMMAND_UI(IDC_DEMANDLIST,OnUpdateUIDemandList)
	ON_UPDATE_COMMAND_UI(IDC_ALLOCATE,OnUpdateUIAllocate)
	ON_BN_CLICKED(IDC_DEMANDLIST, OnDemandList)
	ON_MESSAGE(WM_CICDEMANDTABLE_EXIT, OnDemandTableExit)
	ON_BN_CLICKED(IDC_ASSIGN,  OnAssign)
	ON_BN_CLICKED(IDC_ALLOCATE,OnAllocate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


void CCIDiagram::SetCaptionText(void)
{
	if (omPrePlanMode)
	{
		//omCaptionText.Format("CCI Diagramm - VORPLANUNG f�r %s",omPrePlanTime.Format("%d%m%y  "));
		omCaptionText.Format(GetString(IDS_STRING61299),omPrePlanTime.Format("%d%m%y  "));
	}
	else
	{
		//omCaptionText = CString("CCI Diagramm - ");
		omCaptionText = GetString(IDS_STRING61300);
	}
//	if(bgOnline)
//	{
//		//omCaptionText += "  Online";
//		omCaptionText += CString("  ") + GetString(IDS_STRING61301);
//	}
//	else
//	{
//		// "Offline"
//		//omCaptionText += "  OFFLINE";
//		omCaptionText += CString("  ") + GetString(IDS_STRING61302);
//	}
	SetWindowText(omCaptionText);
}

void CCIDiagram::SetCciAreaButtonColor(int ipGroupno)
{
	CTime ilStart = ogBasicData.GetTime();
	CTime ilEnd = ilStart + CTimeSpan(0,3,0,0);
	CCIChart *polCciChart;
	int ilColorIndex;

	ilColorIndex = omViewer.GetGroupColorIndex(ipGroupno,ilStart,ilEnd);
	polCciChart = (CCIChart *) omPtrArray.GetAt(ipGroupno);
	if (polCciChart != NULL)
	{
		CCSButtonCtrl *prlButton =polCciChart->GetChartButtonPtr();
		if (prlButton != NULL)
		{
			if (ilColorIndex == FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2))
			{
				prlButton->SetColors(::GetSysColor(COLOR_BTNFACE),
					::GetSysColor(COLOR_BTNSHADOW),
					::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
			else
			{
				prlButton->SetColors(ogColors[ilColorIndex],
					::GetSysColor(COLOR_BTNSHADOW),
					::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
		}
	}
}

void CCIDiagram::SetAllCciAreaButtonsColor(void)
{
	int ilGroupCount = omViewer.GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		SetCciAreaButtonColor(ilGroupno);
	}
}

void CCIDiagram::PrePlanMode(BOOL bpToSet,CTime opPrePlanTime)
{
	if (bpToSet)
	{
		omPrePlanTime = opPrePlanTime;
		omPrePlanMode = TRUE;

		SetTSStartTime(opPrePlanTime);
		omTimeScale.SetDisplayStartTime(omTSStartTime);
		omTimeScale.Invalidate(TRUE);
		omTimeScale1.SetDisplayStartTime(omTSStartTime);
		omTimeScale1.Invalidate(TRUE);
		omClientWnd.Invalidate(FALSE);

		// update scroll bar position
		long llTotalMin = CalcTotalMinutes();
		long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
		nPos = nPos * 1000L / llTotalMin;
		SetScrollPos(SB_HORZ, int(nPos), TRUE);
	}
	else
	{
		omPrePlanTime = opPrePlanTime;
		omPrePlanMode = FALSE;
	}

	SetCaptionText();

	// Id 30-Sep-96
	// Force the diagram to redisplay data again when switch to and from preplan mode.
	// This will fix the bug that the diagram confuse the time and need refreshing by a HScroll.
	ChangeViewTo(ogCfgData.rmUserSetup.CCCV);
}

void CCIDiagram::PositionChild()
{
    CRect olRect;
    CRect olChartRect;
    CCIChart *polChart;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
        polChart = (CCIChart *) omPtrArray.GetAt(ilIndex);

        if (ilIndex < imFirstVisibleChart)
        {
            polChart->ShowWindow(SW_HIDE);
            continue;
        }
        
        polChart->GetClientRect(&olChartRect);
        olChartRect.right = olRect.right;

        olChartRect.top = ilLastY;

        ilLastY += polChart->GetHeight();
        olChartRect.bottom = ilLastY;
        
        // check
        if ((polChart->GetState() != Minimized)
        && (olChartRect.top < olRect.bottom)
        && (olChartRect.bottom > olRect.bottom))
        {
            olChartRect.bottom = olRect.bottom;
            polChart->SetState(Normal);
        }
        //
        
        polChart->MoveWindow(&olChartRect, FALSE);
        polChart->ShowWindow(SW_SHOW);
   }
    
    omClientWnd.Invalidate(TRUE);
	OnUpdatePrevNext();
}

void CCIDiagram::SetTSStartTime(CTime opTSStartTime)
{
    omTSStartTime = opTSStartTime;
    
    char clBuf[8];
    
    sprintf(clBuf, "%02d%02d%02d",
        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}

CListBox *CCIDiagram::GetBottomMostGantt()
{
	// Check the size of the area for displaying charts.
	// Pichet used "omClientWnd" not the diagram itself, so we will get the size of this window
	CRect olClientRect;
	omClientWnd.GetClientRect(olClientRect);
	omClientWnd.ClientToScreen(olClientRect);

	// Searching for the bottommost chart
	CCIChart *polChart;
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (CCIChart *)omPtrArray[ilLc];
		CRect olRect, olChartRect;
		polChart->GetClientRect(olChartRect);
		polChart->ClientToScreen(olChartRect);
		if (!olRect.IntersectRect(&olChartRect, &olClientRect))
			break;
	}

	// Check if the chart we have found is a valid one
	--ilLc;
	if (!(0 <= ilLc && ilLc <= omPtrArray.GetSize()-1))
		return NULL;

	return &((CCIChart *)omPtrArray[ilLc])->omLeftGantt;
}

/////////////////////////////////////////////////////////////////////////////
// CCIDiagram message handlers

int CCIDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
        return -1;
    
    // TODO: Add your specialized creation code here
    omViewer.Attach(this);
    CRect olRect; GetClientRect(&olRect);


    // height is 20 point
    int ilRc = omDialogBar.Create(this, IDD_CCIDIAGRAM, CBRS_TOP, IDD_CCIDIAGRAM);
	UpdateComboBox();

	CWnd *polWnd = omDialogBar.GetDlgItem(IDC_ANSICHT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	polWnd = omDialogBar.GetDlgItem(IDC_MABSTAB);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61842));
	}
	polWnd = omDialogBar.GetDlgItem(IDC_PRINT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}

	polWnd = omDialogBar.GetDlgItem(IDC_DEMANDLIST);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61973));
	}

	pomAssignButton = NULL;
	polWnd = omDialogBar.GetDlgItem(IDC_ASSIGN);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61974));
		if(ogBasicData.IsEnabled("CCIDIAGRAM IDC_ASSIGN"))
		{
			pomAssignButton = new CCSButtonCtrl(true);
			pomAssignButton->Attach(polWnd->m_hWnd);
			if(ogJobData.bmAutoAssignInProgress)
			{
				pomAssignButton->SetColors(RED,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
		}
	}

	polWnd = omDialogBar.GetDlgItem(IDC_ALLOCATE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61975));
	}

	CTime olCurrentTime = ogBasicData.GetTime();
    CTime olCurrentUtcTime = ogBasicData.GetUtcTime();
	CTime olTimeframeStart = ogBasicData.GetTimeframeStart();
	CTime olTimeframeEnd = ogBasicData.GetTimeframeEnd();

	CTimeScaleDialog* polTSD = NULL;
	if(pogButtonList->pomMapDiagramToScaleDlg->Lookup(CCIDIA,(CObject*&)polTSD) == FALSE)
	{
		// the current time window displayed
		omTSDuration = CTimeSpan(0, 6, 0, 0);
		// intervals on the timescale
		omTSInterval = CTimeSpan(0, 0, 10, 0);
		// start time of the whole gantt chart (ie not just the time window currently displayed)
		omStartTime = olTimeframeStart;
		// duration of the whole gantt chart --> minimum 8 hours (+1 minute to stop divide by zero)
		omDuration = max(olTimeframeEnd - olTimeframeStart,omTSDuration)+CTimeSpan(0,0,1,0);
		// the display start time (typically: current time - 1 hour)
		omTSStartTime = ogBasicData.GetDisplayDate() - ogBasicData.GetDisplayOffset();
	}
	else
	{	// the current time window displayed
		omTSDuration = CTimeSpan(0,polTSD->m_Hour,0,0);
		// intervals on the timescale
		omTSInterval = polTSD->omTSI;
		// start time of the whole gantt chart (ie not just the time window currently displayed)
		omStartTime = olTimeframeStart;
		// duration of the whole gantt chart --> minimum 8 hours (+1 minute to stop divide by zero)
		omDuration = max(olTimeframeEnd - olTimeframeStart,omTSDuration)+CTimeSpan(0,0,1,0);
		// the display start time (typically: current time - 1 hour)
		omTSStartTime = polTSD->m_TimeScale;
	}
    
	pomDemandTableButton = new CCSButtonCtrl(true);
	polWnd = omDialogBar.GetDlgItem(IDC_DEMANDLIST);
	if (polWnd)
	{
		pomDemandTableButton->Attach(polWnd->m_hWnd);
	}

    char olBuf[16];
    sprintf(olBuf, "%02d%02d/%02d%02dz",olCurrentTime.GetHour(), olCurrentTime.GetMinute(), olCurrentUtcTime.GetHour(), olCurrentUtcTime.GetMinute());
    omTime.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,CRect(olRect.right - 172, 6, olRect.right - 92, 23), this);

    sprintf(olBuf, "%02d%02d%02d",olCurrentTime.GetDay(), olCurrentTime.GetMonth(), olCurrentTime.GetYear() % 100);
    omDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,CRect(olRect.right - 88, 6, olRect.right - 8, 23), this);

    sprintf(olBuf, "%02d%02d%02d",omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    int ilPos = (100 - olRect.left - 80) / 2;
    
	
	CString thedate="", s="";

	s.Format("%c",date[0]);  	thedate+=s;
    s.Format("%c",date[1]); 	thedate+=s;
	s.Format("%c",date[3]); 	thedate+=s;
    s.Format("%c",date[4]);  	thedate+=s;
	s.Format("%c",date[8]); 	thedate+=s;
    s.Format("%c",date[9]);  	thedate+=s;	


	
	omTSDate.Create(thedate, SS_CENTER | WS_CHILD | WS_VISIBLE,CRect(ilPos, 6, ilPos + 80, 23), this);

    // CBitmapButton
    omBB1.Create("PREV", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 36, 33 + 5, olRect.right - 19, (33 + 5) + 25), this, IDC_PREV);
    omBB1.LoadBitmaps("PREVU", "PREVD", NULL, "PREVX");

    omBB2.Create("NEXT", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 19, 33 + 5, olRect.right - 2, (33 + 5) + 25), this, IDC_NEXT);
    omBB2.LoadBitmaps("NEXTU", "NEXTD", NULL, "NEXTX");

    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos, 34, ((olRect.right - (36 + 2)) / 2), 68), this, 0, NULL);
    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

    omTimeScale1.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(((olRect.right - (36 + 2)) / 2) + imStartTimeScalePos, 34, olRect.right - (36 + 2), 68), this, 0, NULL);
    omTimeScale1.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,AFX_IDW_STATUS_BAR);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    olRect.top += 70;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);
        
	if (bgIsPreplanMode)
	{
		SetTSStartTime(omPrePlanTime);
		omTimeScale.SetDisplayStartTime(omTSStartTime);
		omTimeScale1.SetDisplayStartTime(omTSStartTime);
	}

    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = CalcTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);
	SetTimeBand(ogBasicData.omTimebandStart, ogBasicData.omTimebandEnd);
    
    CCIChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
	OnUpdatePrevNext();
    int ilLastY = 0;
    for (int ilI = 0; ilI < omViewer.GetGroupCount(); ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new CCIChart;
        polChart->SetTimeScale(&omTimeScale, &omTimeScale1);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
        polChart->Create(NULL, "CCIChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);
        
        omPtrArray.Add(polChart);

        ilLastY += polChart->GetHeight();
    }
    
	ChangeViewTo(ogCfgData.rmUserSetup.CCCV);
    //SetAllCciAreaButtonsColor();
		 OnTimer(0);
    
	// Register DDX call back function
	TRACE("CciDiagram: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("CCIDIAGRAM"),CString("Redisplay all from What-If"), CciDiagramCf);
	ogCCSDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("CCIDIAGRAM"),CString("Update Time Band"),CciDiagramCf);
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("CCIDIAGRAM"),CString("Global Date Change"), CciDiagramCf);
	ogCCSDdx.Register(this, PREPLAN_DATE_UPDATE, CString("CCIDIAGRAM"),CString("Preplan Date Change"), CciDiagramCf);
	ogCCSDdx.Register(this, AFLEND_SBC, CString("CCIDIAGRAM"),CString("End Assignment"), CciDiagramCf); // auto-assignment finished
	ogCCSDdx.Register(this, STARTASSIGNMENT, CString("CCIDIAGRAM"),CString("Start Assignment"), CciDiagramCf); // auto-assignment finished
    
	polWnd = GetDlgItem(IDC_ZEIT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32896));
		ogBasicData.SetWindowStat("CCIDIAGRAM IDC_ZEIT",polWnd);
		if(olCurrentTime < olTimeframeStart || olCurrentTime > olTimeframeEnd)
		{
			// cannot set the gantt to the current local time because it is outside of the timeframe
			polWnd->EnableWindow(FALSE);
		}
	}

    m_DragDropTarget.RegisterTarget(this, this);
    return 0;
}

void CCIDiagram::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	ogBasicData.WriteDialogToReg(this,COpssPmApp::CHECKIN_DIAGRAM_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	bmNoUpdatesNow = TRUE;
	ogCCSDdx.UnRegister(&omViewer, NOTUSED);

	// Unregister DDX call back function
	TRACE("CciDiagram: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	m_DragDropTarget.Revoke();
	CFrameWnd::OnDestroy();
}

void CCIDiagram::OnClose() 
{
   	pogButtonList->m_wndCciDiagram = NULL;
	pogButtonList->m_CciDiagramButton.Recess(FALSE);

	if (m_wndDemandTable)
	{
		m_wndDemandTable->DestroyWindow();
		m_wndDemandTable = NULL;
	}

    CFrameWnd::OnClose();
}

void CCIDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL CCIDiagram::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
}

void CCIDiagram::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);
    
    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 33); dc.LineTo(olRect.right, 33);
    dc.MoveTo(olRect.left, 68); dc.LineTo(olRect.right, 68);

    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 69); dc.LineTo(olRect.right, 69);
    
    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 33);
    dc.LineTo(imStartTimeScalePos - 2, 69);

    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 33);
    dc.LineTo(imStartTimeScalePos - 1, 69);
    
    // draw vertical line (CCI)
    dc.SelectObject(&olTPen);
    dc.MoveTo(((olRect.right - (36 + 2)) / 2), 33);
    dc.LineTo(((olRect.right - (36 + 2)) / 2), 69);

    dc.SelectObject(&olHPen);
    dc.MoveTo(((olRect.right - (36 + 2)) / 2) + 1, 33);
    dc.LineTo(((olRect.right - (36 + 2)) / 2) + 1, 69);

    // draw vertical line (CCI)
    dc.SelectObject(&olTPen);
    dc.MoveTo(((olRect.right - (36 + 2)) / 2) + imStartTimeScalePos - 2, 33);
    dc.LineTo(((olRect.right - (36 + 2)) / 2) + imStartTimeScalePos - 2, 69);

    dc.SelectObject(&olHPen);
    dc.MoveTo(((olRect.right - (36 + 2)) / 2) + imStartTimeScalePos - 1, 33);
    dc.LineTo(((olRect.right - (36 + 2)) / 2) + imStartTimeScalePos - 1, 69);

    dc.SelectObject(polOldPen);
    // Do not call CFrameWnd::OnPaint() for painting messages
}

void CCIDiagram::OnSize(UINT nType, int cx, int cy) 
{
    CFrameWnd::OnSize(nType, cx, cy);
    
    // TODO: Add your message handler code here
    CRect olRect; GetClientRect(&olRect);
   
    CRect olBB1Rect(olRect.right - 36, 33 + 5, olRect.right - 19, (33 + 5) + 25);
    omBB1.MoveWindow(&olBB1Rect, TRUE);
    
    CRect olBB2Rect(olRect.right - 19, 33 + 5, olRect.right - 2, (33 + 5) + 25);
    omBB2.MoveWindow(&olBB2Rect, TRUE);

    CRect olTSRect(imStartTimeScalePos, 34, ((olRect.right - (36 + 2)) / 2), 68);
    omTimeScale.MoveWindow(&olTSRect, FALSE);

    CRect olTSRect1(((olRect.right - (36 + 2)) / 2) + imStartTimeScalePos, 34, olRect.right - (36 + 2), 68);
    omTimeScale1.MoveWindow(&olTSRect1, FALSE);
    
    olRect.top += 70;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    // LeftTop, RightBottom
    omClientWnd.MoveWindow(&olRect, TRUE);

	// Update top scale text
	omTimeScale.GetClientRect(&olRect);
	omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

    PositionChild();  
    if (nType != SIZE_MINIMIZED)
    {
	    GetWindowRect(&omWindowRect); //PRF 8712
    }
}

void CCIDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    long llTotalMin;
    int ilPos;
	CRect olRect;
    
    switch (nSBCode)
    {
    case SB_LINEUP :
        llTotalMin = CalcTotalMinutes();            
        ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
        if (ilPos <= 0)
        {
            ilPos = 0;
            SetTSStartTime(omStartTime);
        }
        else
		{
            SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));
		}

        omTimeScale.SetDisplayStartTime(omTSStartTime);
        omTimeScale.Invalidate(TRUE);
        omTimeScale1.SetDisplayStartTime(omTSStartTime);
        omTimeScale1.Invalidate(TRUE);

		// Update top scale text
		omTimeScale.GetClientRect(&olRect);
		omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

        SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
        omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
	    break;
    
    case SB_LINEDOWN :
        llTotalMin = CalcTotalMinutes();            
        ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
        if (ilPos >= 1000)
        {
            ilPos = 1000;
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
        }
        else
		{
            SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));
		}

        omTimeScale.SetDisplayStartTime(omTSStartTime);
        omTimeScale.Invalidate(TRUE);
        omTimeScale1.SetDisplayStartTime(omTSStartTime);
        omTimeScale1.Invalidate(TRUE);

		// Update top scale text
		omTimeScale.GetClientRect(&olRect);
		omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

        SetScrollPos(SB_HORZ, ilPos, TRUE);
        omClientWnd.Invalidate(FALSE);
	    break;
    
    case SB_PAGEUP :
        llTotalMin = CalcTotalMinutes();            
        ilPos = GetScrollPos(SB_HORZ)
            - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
        if (ilPos <= 0)
        {
            ilPos = 0;
            SetTSStartTime(omStartTime);
        }
        else
            SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

        omTimeScale.SetDisplayStartTime(omTSStartTime);
        omTimeScale.Invalidate(TRUE);
        omTimeScale1.SetDisplayStartTime(omTSStartTime);
        omTimeScale1.Invalidate(TRUE);
        
		// Update top scale text
		omTimeScale.GetClientRect(&olRect);
		omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

        SetScrollPos(SB_HORZ, ilPos, TRUE);
        omClientWnd.Invalidate(FALSE);
		break;
    
    case SB_PAGEDOWN :
        llTotalMin = CalcTotalMinutes();            
        ilPos = GetScrollPos(SB_HORZ)
            + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
        if (ilPos >= 1000)
        {
            ilPos = 1000;
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
        }
        else
            SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

        omTimeScale.SetDisplayStartTime(omTSStartTime);
        omTimeScale.Invalidate(TRUE);
        omTimeScale1.SetDisplayStartTime(omTSStartTime);
        omTimeScale1.Invalidate(TRUE);

		// Update top scale text
		omTimeScale.GetClientRect(&olRect);
		omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

        SetScrollPos(SB_HORZ, ilPos, TRUE);
        omClientWnd.Invalidate(FALSE);

		break;
    
    case SB_THUMBTRACK /* pressed, any drag time */:
        llTotalMin = CalcTotalMinutes();
        SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
        omTimeScale.SetDisplayStartTime(omTSStartTime);
        omTimeScale.Invalidate(TRUE);
        omTimeScale1.SetDisplayStartTime(omTSStartTime);
        omTimeScale1.Invalidate(TRUE);

//		omTimeScale.GetClientRect(&olRect);
//		omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

        
		omClientWnd.Invalidate(FALSE);
        SetScrollPos(SB_HORZ, nPos, TRUE);
	    break;

    case SB_THUMBPOSITION:	// the thumb was just released?
		// Update top scale text
		omTimeScale.GetClientRect(&olRect);
		omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
        omClientWnd.Invalidate(FALSE);
		return;

    case SB_TOP:
    case SB_BOTTOM:
    case SB_ENDSCROLL:
	    break;
    }
}

void CCIDiagram::OnNextChart()
{
    if (imFirstVisibleChart < omPtrArray.GetUpperBound())
    {
        imFirstVisibleChart++;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void CCIDiagram::OnPrevChart()
{
    if (imFirstVisibleChart > 0)
    {
        imFirstVisibleChart--;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void CCIDiagram::OnFirstChart()
{
	imFirstVisibleChart = 0;
	OnUpdatePrevNext();
	PositionChild();
}

void CCIDiagram::OnLastChart()
{
	imFirstVisibleChart = omPtrArray.GetUpperBound();
	OnUpdatePrevNext();
	PositionChild();
}

void CCIDiagram::OnTimer(UINT nIDEvent)
{
	if(bmNoUpdatesNow == FALSE)
	{
		if(nIDEvent == 0)
		{
			char clBuf[16];
			CTime olCurrentTime = ogBasicData.GetTime();
			omTimeScale.UpdateCurrentTimeLine(olCurrentTime);
			omTimeScale1.UpdateCurrentTimeLine(olCurrentTime);

			CTime olCurrentUtcTime = ogBasicData.GetUtcTime();
			sprintf(clBuf, "%02d%02d/%02d%02dz",
				olCurrentTime.GetHour(), olCurrentTime.GetMinute(),        
				olCurrentUtcTime.GetHour(), olCurrentUtcTime.GetMinute()
			);
			omTime.SetWindowText(clBuf);
			omTime.Invalidate(FALSE);

			sprintf(clBuf, "%02d%02d%02d",
				olCurrentTime.GetDay(), olCurrentTime.GetMonth(), olCurrentTime.GetYear() % 100);
			omDate.SetWindowText(clBuf);
			omDate.Invalidate(FALSE);
			//

			CCIChart *polChart;
			for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
			{
				polChart = (CCIChart *) omPtrArray.GetAt(ilIndex);
				polChart->GetLeftGanttPtr()->SetCurrentTime(olCurrentTime);
				polChart->GetRightGanttPtr()->SetCurrentTime(olCurrentTime);
			}
			SetAllCciAreaButtonsColor();
		}
		else if(nIDEvent == AUTOSCROLL_TIMER_EVENT)
		{
			// called during drag&drop, does automatic scrolling when the cursor
			// is outside the main gantt chart
			OnAutoScroll();
		}

		CFrameWnd::OnTimer(nIDEvent);
	}
}

LONG CCIDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

LONG CCIDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
    CString olStr;
    int ipGroupNo = HIWORD(lParam);
    int ipLineNo = LOWORD(lParam);
	CTime olTime = CTime((time_t) lParam);

	if(ipGroupNo >= omPtrArray.GetSize())
		return -1L;

    CCIChart *polCCIChart = (CCIChart *) omPtrArray.GetAt(ipGroupNo);
	if(polCCIChart == NULL) return 0L;
    CciGantt *polLCCIGantt = polCCIChart -> GetLeftGanttPtr();
	if(polLCCIGantt == NULL) return 0L;
	CciGantt *polRCCIGantt = polCCIChart -> GetRightGanttPtr();
	if(polRCCIGantt == NULL) return 0L;

    switch (wParam)
    {
        // group message
        case UD_INSERTGROUP :
            // CreateChild(ipGroupNo);

            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart++;
				OnUpdatePrevNext();
			}
            PositionChild();
        break;

        case UD_UPDATEGROUP :
            olStr = omViewer.GetGroupText(ipGroupNo);
            polCCIChart->GetChartButtonPtr()->SetWindowText(olStr);
            polCCIChart->GetChartButtonPtr()->Invalidate(FALSE);

            olStr = omViewer.GetGroupTopScaleText(ipGroupNo);
            polCCIChart->GetTopScaleTextPtr()->SetWindowText(olStr);
            polCCIChart->GetTopScaleTextPtr()->Invalidate(TRUE);
			SetCciAreaButtonColor(ipGroupNo);
        break;
        
        case UD_DELETEGROUP :
            delete omPtrArray.GetAt(ipGroupNo);
            //omPtrArray.GetAt(ipGroupNo) -> DestroyWindow();
            omPtrArray.RemoveAt(ipGroupNo);
            
            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart--;
				OnUpdatePrevNext();
			}
            PositionChild();
        break;

        // line message
        case UD_INSERTLINE :
            polLCCIGantt->InsertString(ipLineNo, "");
			polLCCIGantt->RepaintItemHeight(ipLineNo);

            polRCCIGantt->InsertString(ipLineNo, "");
			polRCCIGantt->RepaintItemHeight(ipLineNo);

            PositionChild();
			SetCciAreaButtonColor(ipGroupNo);
        break;
        
        case UD_UPDATELINE :
            polLCCIGantt->RepaintVerticalScale(ipLineNo);
            polLCCIGantt->RepaintGanttChart(ipLineNo);

            polRCCIGantt->RepaintVerticalScale(ipLineNo);
            polRCCIGantt->RepaintGanttChart(ipLineNo);
			SetCciAreaButtonColor(ipGroupNo);
        break;

        case UD_DELETELINE :
            polLCCIGantt->DeleteString(ipLineNo);
			polRCCIGantt->DeleteString(ipLineNo);
            PositionChild();
			SetCciAreaButtonColor(ipGroupNo);
        break;

        case UD_UPDATELINEHEIGHT :
            polLCCIGantt->RepaintItemHeight(ipLineNo);
			polRCCIGantt->RepaintItemHeight(ipLineNo);
            PositionChild();
			SetCciAreaButtonColor(ipGroupNo);
        break;

		case UD_PREPLANMODE :
			SetTSStartTime(olTime);
			//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

			omTimeScale.SetDisplayStartTime(omTSStartTime);
			omTimeScale.Invalidate(TRUE);
			omTimeScale1.SetDisplayStartTime(omTSStartTime);
			omTimeScale1.Invalidate(TRUE);
    
			omClientWnd.Invalidate(FALSE);
				
			// update scroll bar position
			long llTotalMin = CalcTotalMinutes();
			long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
			nPos = nPos * 1000L / llTotalMin;
			SetScrollPos(SB_HORZ, int(nPos), TRUE);
		break;
    }

    return 0L;
}

void CCIDiagram::OnUpdatePrevNext(void)
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();

	if (imFirstVisibleChart == 0)
		GetDlgItem(IDC_NEXT)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_NEXT)->EnableWindow(TRUE);

	if (imFirstVisibleChart == omPtrArray.GetUpperBound())
		GetDlgItem(IDC_PREV)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_PREV)->EnableWindow(TRUE);
}

///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void CCIDiagram::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.CCCV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}

}

void CCIDiagram::ChangeViewTo(const char *pcpViewName)
{
	CRect olRect;

	AfxGetApp()->DoWaitCursor(1);
	bmNoUpdatesNow = TRUE;
	omTimeScale.GetClientRect(&olRect);
    omViewer.ChangeViewTo(pcpViewName,
		omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
		// Id 30-Sep-96
		// This will remove a lot of warning message when the user change view.
		// If we just delete a staff chart, MFC will produce two warning message.
		// First, Revoke not called before the destructor.
		// Second, calling DestroyWindow() in CWnd::~CWnd.
        //delete (CCIChart *) omPtrArray.GetAt(ilIndex);
		((CCIChart *)omPtrArray[ilIndex])->DestroyWindow();
    }
    omPtrArray.RemoveAll();

    CCIChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
//	SetAllCciAreaButtonsColor();
	OnUpdatePrevNext();
    int ilLastY = 0;
    for (int ilI = 0; ilI < omViewer.GetGroupCount(); ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new CCIChart;
        polChart->SetTimeScale(&omTimeScale, &omTimeScale1);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
        polChart->Create(NULL, "CCIChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);

        polChart->GetLeftGanttPtr()->SetCurrentTime(ogBasicData.GetTime());
		polChart->GetRightGanttPtr()->SetCurrentTime(ogBasicData.GetTime());

        omPtrArray.Add(polChart);

        ilLastY += polChart->GetHeight();
    }

	PositionChild();
	AfxGetApp()->DoWaitCursor(-1);
	bmNoUpdatesNow = FALSE;

	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();
}

void CCIDiagram::OnAnsicht()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();
	
	CCIDiagramPropertySheet dialog(this, &omViewer);
	bmIsViewOpen = TRUE;
	if (dialog.DoModal() != IDCANCEL)
		ChangeViewTo(omViewer.GetViewName());
	bmIsViewOpen = FALSE;
	UpdateComboBox();
}

void CCIDiagram::OnViewSelChange()
{
    char clText[64];
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);
    TRACE("CCIDiagram::OnComboBox() [%s]", clText);
	ChangeViewTo(clText);
}

void CCIDiagram::OnMabstab()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();
	
	//Singapore
	CTimeScaleDialog* polTSD = NULL;
	if(pogButtonList->pomMapDiagramToScaleDlg->Lookup(CCIDIA,(CObject*&)polTSD) == FALSE)
	{
		polTSD = new CTimeScaleDialog(this);
		polTSD->bmNotMoreThan12 = TRUE;
		polTSD->pomTS = &omTimeScale;
		polTSD->pomTS1 = &omTimeScale1;
		polTSD->omTSI = omTSInterval;
		polTSD->m_TimeScale = omTSStartTime;		
		polTSD->m_Hour = (int) omTSDuration.GetTotalHours();
		polTSD->imMaxHours = omDuration.GetTotalHours();
		pogButtonList->pomMapDiagramToScaleDlg->SetAt(CCIDIA,polTSD);
	}
    CTimeScaleDialog olTSD(this);
	olTSD.bmNotMoreThan12 = TRUE;
    olTSD.pomTS = polTSD->pomTS;
	olTSD.pomTS1 = polTSD->pomTS1;
	olTSD.omTSI = polTSD->omTSI;
	//olTSD.m_TimeScale = polTSD->m_TimeScale;		
	olTSD.m_TimeScale = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
                atoi(polTSD->m_startTime), 0, 0);
	olTSD.m_Hour = polTSD->m_Hour;
	olTSD.imMaxHours = polTSD->imMaxHours;
	
    /*CTimeScaleDialog olTSD(this); //Commented Singapore

    olTSD.bmNotMoreThan12 = TRUE;
    olTSD.pomTS = &omTimeScale;
    olTSD.pomTS1 = &omTimeScale1;
    olTSD.omTSI = omTSInterval;
    olTSD.m_TimeScale = omTSStartTime;
    //olTSD.m_TimeScale = ogBasicData.GetTime();
    olTSD.m_Hour = (int) omTSDuration.GetTotalHours();
	olTSD.imMaxHours = omDuration.GetTotalHours();*/
	if(ogCCiIndexes.Chart == MS_SANS6)
	{
	    olTSD.m_Percent = 50;
	}
	else if(ogCCiIndexes.Chart == MS_SANS8)
	{
	    olTSD.m_Percent = 75;
	}
	else if(ogCCiIndexes.Chart == MS_SANS12)
	{
	    olTSD.m_Percent = 100;
	}
    if (olTSD.DoModal() == IDOK)
    {
		//Singapore
		polTSD->pomTS = olTSD.pomTS;
		polTSD->pomTS1 = olTSD.pomTS1;
		polTSD->omTSI = olTSD.omTSI;
		polTSD->m_TimeScale = olTSD.m_TimeScale;	
		polTSD->m_Hour = olTSD.m_Hour;
		polTSD->imMaxHours = olTSD.imMaxHours;
		polTSD->m_startTime = olTSD.m_startTime;

        SetTSStartTime(olTSD.m_TimeScale);
        omTSDuration = CTimeSpan(0, olTSD.m_Hour, 0, 0);
        if(olTSD.m_Percent == 50)
		{
			ogCCiIndexes.VerticalScale = MS_SANS8;
			ogCCiIndexes.Chart = MS_SANS6;
		}
        else if(olTSD.m_Percent == 75)
		{
			ogCCiIndexes.VerticalScale = MS_SANS16;
			ogCCiIndexes.Chart = MS_SANS8;
		}
        else if(olTSD.m_Percent == 100)
		{
			ogCCiIndexes.VerticalScale = MS_SANS12;
			ogCCiIndexes.Chart = MS_SANS12;
		}
		else
		{
			ogCCiIndexes.VerticalScale = MS_SANS6;
			ogCCiIndexes.Chart = MS_SANS6;
		}
        
        omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale.Invalidate(TRUE);
        omTimeScale1.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale1.Invalidate(TRUE);
        
        ChangeViewTo(ogCfgData.rmUserSetup.CCCV);

        omClientWnd.Invalidate(FALSE);

		// update scroll bar position
        long llTotalMin = CalcTotalMinutes();
		long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
		nPos = nPos * 1000L / llTotalMin;
        SetScrollPos(SB_HORZ, int(nPos), TRUE);
	}
    else
    {
        omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale.Invalidate(TRUE);
        omTimeScale1.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale1.Invalidate(TRUE);
    }
}

void CCIDiagram::OnZeit() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();
	
	// TODO: Add your command handler code here
	CTime olTime = ogBasicData.GetTime();
	olTime -= CTimeSpan(0, 1, 0, 0);

	SetTSStartTime(olTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);
    omTimeScale1.SetDisplayStartTime(omTSStartTime);
    omTimeScale1.Invalidate(TRUE);
    
    omClientWnd.Invalidate(FALSE);
		
	// update scroll bar position
    long llTotalMin = CalcTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);
}

void CCIDiagram::OnPrint() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();

	CSelReportDlg olSelReportDlg;
	olSelReportDlg.SetReports(GetString(IDS_CCIGANTT_PRINT1));
	olSelReportDlg.SetReports(GetString(IDS_CCIGANTT_PRINT2));
//	olSelReportDlg.SetReports(GetString(IDS_CCIGANTT_PRINT3));
	if(olSelReportDlg.DoModal() == IDOK)
	{
		if(!strcmp(olSelReportDlg.omSelectedReport,GetString(IDS_CCIGANTT_PRINT1)))
		{
			omViewer.PrintDiagram(omPtrArray);
		}
		else if(!strcmp(olSelReportDlg.omSelectedReport,GetString(IDS_CCIGANTT_PRINT2)))
		{
			CCicPrintDlg olPrintDlg;
			olPrintDlg.SetTime(omTSStartTime, omTSStartTime + omTSDuration);
			if(olPrintDlg.DoModal() == IDOK)
			{
				CTime olFrom, olTo; 
				bool blSortByShift, blSortByStd, blDisplaySta, blDisplayEta, blDisplayStd, blDisplayEtd, blDisplayGate, blDisplayArr;
				olPrintDlg.GetSettings(olFrom, olTo, blSortByShift, blSortByStd, blDisplaySta, blDisplayEta, blDisplayStd, blDisplayEtd, blDisplayGate, blDisplayArr);
				omViewer.PrintView(olFrom, olTo, blSortByShift, blSortByStd, blDisplaySta, blDisplayEta, blDisplayStd, blDisplayEtd, blDisplayGate, blDisplayArr);
			}
		}
//		else if(!strcmp(olSelReportDlg.omSelectedReport,GetString(IDS_CCIGANTT_PRINT3)))
//			omViewer.PrintView(false);
	}
}
	
BOOL CCIDiagram::DestroyWindow() 
{

	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}
	bmNoUpdatesNow = TRUE;
	ogCCSDdx.UnRegister(&omViewer, NOTUSED);

	if (m_wndDemandTable)
	{
		m_wndDemandTable->DestroyWindow();
		m_wndDemandTable = NULL;
	}

	BOOL blRc = CWnd::DestroyWindow();
	return blRc;
}

////////////////////////////////////////////////////////////////////////
// CCIDiagram keyboard handling

void CCIDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	switch (nChar)
	{
	case VK_UP:		// move the bottom most gantt chart up/down one line
	case VK_DOWN:
		CListBox *polGantt;
		if ((polGantt = GetBottomMostGantt()) != NULL)
			polGantt->SendMessage(WM_USERKEYDOWN, nChar);
		break;
	case VK_PRIOR:
		blIsControl? OnFirstChart(): OnPrevChart();
		break;
	case VK_NEXT:
		blIsControl? OnLastChart(): OnNextChart();
		break;
	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	case VK_ESCAPE:
		OnClose();
		break;
	default:
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void CCIDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

////////////////////////////////////////////////////////////////////////
// CciDiagram -- implementation of DDX call back function

void CCIDiagram::CciDiagramCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	CCIDiagram *polDiagram = (CCIDiagram *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
	{
		polDiagram->RedisplayAll();
	}
	else if (ipDDXType == STAFFDIAGRAM_UPDATETIMEBAND)
	{
		TIMEPACKET *polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE || ipDDXType == PREPLAN_DATE_UPDATE)
	{
		polDiagram->HandleGlobalDateUpdate(*((CTime *) vpDataPointer));
	}
	else if(ipDDXType == AFLEND_SBC)
	{
		polDiagram->ProcessEndAssignment();
	}
	else if(ipDDXType == STARTASSIGNMENT)
	{
		polDiagram->ProcessStartAssignment();
	}
}

void CCIDiagram::RedisplayAll()
{
	ChangeViewTo(ogCfgData.rmUserSetup.CCCV);
	SetCaptionText();
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void CCIDiagram::HandleGlobalDateUpdate(CTime opDate)
{
	//omPrePlanTime = opDate; //Singapore
	omPrePlanTime = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),omTSStartTime.GetHour(),
		                  omTSStartTime.GetMinute(),omTSStartTime.GetSecond()); //Singapore
	SetTSStartTime(omPrePlanTime);
	omTimeScale.SetDisplayStartTime(omTSStartTime);
	omTimeScale.Invalidate(TRUE);
	CRect olRect;
	omTimeScale.GetClientRect(&olRect);
	omViewer.UpdateManagers(omTSStartTime,omTimeScale.GetTimeFromX(olRect.Width() + 38));

	omClientWnd.Invalidate(FALSE);

	// update scroll bar position
	long llTotalMin = CalcTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);

	SetCaptionText();
}

void CCIDiagram::OnUpdateUIZeit(CCmdUI *pCmdUI)
{
	// check, if current time is inside time frame
	if (!ogBasicData.IsDisplayDateInsideTimeFrame())
	{
		pCmdUI->Enable(FALSE);
	}
	else
	{
		CWnd *pWnd = omDialogBar.GetDlgItem(IDC_ZEIT);
		if (pWnd)
		{
			ogBasicData.SetWindowStat("CCIDIAGRAM IDC_ZEIT",pWnd);
			pCmdUI->Enable(pWnd->IsWindowEnabled());
		}
	}
}

void CCIDiagram::OnUpdateUIMabstab(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_MABSTAB);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("CCIDIAGRAM IDC_MABSTAB",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
	}
}

void CCIDiagram::OnUpdateUIPrint(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_PRINT);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("CCIDIAGRAM IDC_PRINT",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
	}
}

void CCIDiagram::OnUpdateUIAssign(CCmdUI *pCmdUI)
{
	CWnd *polWnd = omDialogBar.GetDlgItem(IDC_ASSIGN); 
	if(polWnd != NULL)
	{
		if(bgOnline)
		{
			ogBasicData.SetWindowStat("CCIDIAGRAM IDC_ASSIGN",polWnd);
			pCmdUI->Enable(polWnd->IsWindowEnabled());
		}
		else
		{
			pCmdUI->Enable(FALSE);
		}
	}
}

void CCIDiagram::OnUpdateUIAllocate(CCmdUI *pCmdUI)
{
	CWnd *polWnd = omDialogBar.GetDlgItem(IDC_ALLOCATE); 
	if(polWnd != NULL)
	{
		if(bgOnline)
		{
			ogBasicData.SetWindowStat("CCIDIAGRAM IDC_ALLOCATE",polWnd);
			pCmdUI->Enable(polWnd->IsWindowEnabled());
		}
		else
		{
			pCmdUI->Enable(FALSE);
		}
	}
}

void CCIDiagram::OnUpdateUIAnsicht(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_ANSICHT);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("CCIDIAGRAM IDC_ANSICHT",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
	}
}

void CCIDiagram::OnUpdateUIView(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_VIEW);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("CCIDIAGRAM IDC_VIEW",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
	}
}

void CCIDiagram::OnUpdateUIDemandList(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_DEMANDLIST);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("CCIDIAGRAM IDC_DEMANDLIST",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
		pCmdUI->Enable(true);
	}
}

void CCIDiagram::SetDemandColor(COLORREF lpColor)
{
	pomDemandTableButton->SetColors(lpColor,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
}

void CCIDiagram::SetDemandColor(COLORREF lpColor,BOOL bpRecess)
{
	pomDemandTableButton->Recess(bpRecess == TRUE);
	pomDemandTableButton->SetColors(lpColor,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
}

void CCIDiagram::OnDemandList()
{
    // TODO: Add your control notification handler code here
    if (m_wndDemandTable == NULL)  // this window didn't open yet?
    {
		m_wndDemandTable = new CicDemandTable(NULL,&omViewer,this);
		SetDemandColor(::GetSysColor(COLOR_BTNFACE),TRUE);
    }
    else    // close window
    {
        if (m_wndDemandTable->DestroyWindow())
		{
			SetDemandColor(::GetSysColor(COLOR_BTNFACE),FALSE);
			m_wndDemandTable = NULL;
		}
    }
}

LONG CCIDiagram::OnDemandTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
    if (m_wndDemandTable != NULL)  // this window is open ?
    {
		SetDemandColor(::GetSysColor(COLOR_BTNFACE),FALSE);
		m_wndDemandTable = NULL;
    }

    return 0L;
}

void CCIDiagram::OnAssign()
{
	if(ogJobData.NoAssignmentInProgress(this))
	{
		CStringArray olReductionList; // not used for the moment
		CAutoAssignDlg olAutoAssignDlg(ogBasicData.GetTimeframeStart(),ogBasicData.GetTimeframeEnd(),ALLOCUNITTYPE_CIC,olReductionList,this);

		CUIntArray olDemandUrnoList;
		omViewer.GetDemandList(olDemandUrnoList);

		CStringArray olPoolNames;
		CString olPoolString; 

		int ilNumPools = ogPolData.omData.GetSize();
		for(int ilPool = 0; ilPool < ilNumPools; ilPool++)
		{
			POLDATA *prlPol = &ogPolData.omData[ilPool];
			olPoolString.Format("%s#%ld",prlPol->Name,prlPol->Urno);
			olPoolNames.Add(olPoolString);
		}

		CTime olStartTime = (ogBasicData.GetTimeframeStart() > omTSStartTime) ? ogBasicData.GetTimeframeStart() : omTSStartTime;
		CTime olEndTime = olStartTime + omTSDuration;
		if(olEndTime > ogBasicData.GetTimeframeEnd())
		{
			olEndTime = ogBasicData.GetTimeframeEnd();
		}
		olAutoAssignDlg.SetData(olDemandUrnoList,olPoolNames,olStartTime,olEndTime);
		olAutoAssignDlg.DoModal();
	}
}

void CCIDiagram::ProcessStartAssignment()
{
	pomAssignButton->SetColors(RED,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
}

void CCIDiagram::ProcessEndAssignment()
{
	pomAssignButton->SetColors(::GetSysColor(COLOR_BTNFACE),::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
}

void CCIDiagram::OnAllocate()
{
	CicAutoAssignDlg olAutoAssignDlg(&omViewer,this);

	CDWordArray olSelectedDemandUrnos;
	olAutoAssignDlg.SetData(omTSStartTime,omTSStartTime+omTSDuration,olSelectedDemandUrnos);
	olAutoAssignDlg.DoModal();
}

void CCIDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}

void CCIDiagram::UpdateTimeBand()
{
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		CCIChart *polChart = (CCIChart *)omPtrArray[ilLc];
		if (polChart)
		{
			CciGantt *polLeftGantt = polChart->GetLeftGanttPtr();
			if (polLeftGantt)
				polLeftGantt->SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
			CciGantt *polRightGantt = polChart->GetRightGanttPtr();
			if (polRightGantt)
				polRightGantt->SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
		}
	}
}

long CCIDiagram::CalcTotalMinutes(void)
{
	long llTotalMinutes = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	if(llTotalMinutes <= 0)
	{
		llTotalMinutes = 1; // prevent divide by zero error
	}
	return llTotalMinutes;
}

// scroll prev/next charts when dragging+dropping over the prev/next buttons
LONG CCIDiagram::OnDragOver(UINT wParam, LONG lParam)
{
	CPoint olDropPosition;
	::GetCursorPos(&olDropPosition);

	CRect olRect;
	CWnd *polWnd;

	polWnd = GetDlgItem(IDC_NEXT); 
	if(polWnd != NULL)
	{
		polWnd->GetWindowRect(&olRect);
		if(olRect.PtInRect(olDropPosition))
		{
			AutoScroll(AUTOSCROLL_INITIAL_SPEED);
			return -1L;
		}
	}

	polWnd = GetDlgItem(IDC_PREV);
	if(polWnd != NULL)
	{
		polWnd->GetWindowRect(&olRect);
		if(olRect.PtInRect(olDropPosition))
		{
			AutoScroll(AUTOSCROLL_INITIAL_SPEED);
			return -1L;
		}
	}
	return -1L;	// cannot accept this object
}


// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void CCIDiagram::AutoScroll(UINT ipInitialScrollSpeed /* = AUTOSCROLL_INITIAL_SPEED*/)
{
	// if not already scrolling automatically...
	if(!bmScrolling)
	{
		// ... after a short pause (ipInitialScrollSpeed), start scrolling
		bmScrolling = true;
		SetTimer(AUTOSCROLL_TIMER_EVENT, (UINT) ipInitialScrollSpeed, NULL);
	}
}

// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void CCIDiagram::OnAutoScroll(void)
{
	bmScrolling = false;

	// if standard cursor then not currently dragging
	if(GetCursor() != AfxGetApp()->LoadStandardCursor(IDC_ARROW))
	{
		// check if the cursor is in the automatic scrolling region
		CPoint olDropPosition;
		::GetCursorPos(&olDropPosition);

		CRect olRect;
		CWnd *polWnd;

		polWnd = GetDlgItem(IDC_NEXT); 
		if(polWnd != NULL)
		{
			polWnd->GetWindowRect(&olRect);
			if(olRect.PtInRect(olDropPosition))
			{
				OnPrevChart();
				bmScrolling = true;
			}
		}

		if(!bmScrolling)
		{
			polWnd = GetDlgItem(IDC_PREV);
			if(polWnd != NULL)
			{
				polWnd->GetWindowRect(&olRect);
				if(olRect.PtInRect(olDropPosition))
				{
					OnNextChart();
					bmScrolling = true;
				}
			}
		}
	}

	if(bmScrolling)
	{
		SetTimer(AUTOSCROLL_TIMER_EVENT, (UINT) AUTOSCROLL_SPEED, NULL);
	}
	else
	{
		// no longer in the scrolling region or left mouse button released
		KillTimer(AUTOSCROLL_TIMER_EVENT);
	}
}

//PRF 8712
void CCIDiagram::OnMove(int x, int y)
{	
	CFrameWnd::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}