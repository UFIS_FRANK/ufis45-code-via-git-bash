// CedaPdaData.h

#ifndef __CedaPdaData__
#define __CedaPdaData__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct PdaDataStruct 
{
	char 	 Pdid[7]; 	// PDA ID
	char 	 Ipad[16]; 	// IP of PDA
	CTime 	 Cdat; 		// Erstellungsdatum
	CTime 	 Lstu; 		// Datum letzte �nderung
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime 	 Vafr; 		// G�ltig von
	CTime 	 Vato; 		// G�ltig bis
	CTime 	 Nafr; 		// G�ltig von
	CTime 	 Nato; 		// G�ltig bis
	long	 Ustf;		// STFTAB.URNO - assigend employee 
	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	PdaDataStruct(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
		Vafr		=	TIMENULL;
		Vato		=	TIMENULL;
		Nafr		=	TIMENULL;
		Nato		=	TIMENULL;
	}
	
	// lli: create deep copy and compare operators for deep copy purpose
	PdaDataStruct& PdaDataStruct::operator=(const PdaDataStruct& rhs)
	{
		if (&rhs != this)
		{
			strcpy(Pdid,rhs.Pdid);
			strcpy(Ipad,rhs.Ipad);
			Cdat = rhs.Cdat;
			Lstu = rhs.Lstu;
			Urno = rhs.Urno;
			strcpy(Usec,rhs.Usec);
			strcpy(Useu,rhs.Useu);
			Vafr = rhs.Vafr;
			Vato = rhs.Vato;
			Nafr = rhs.Nafr;
			Nato = rhs.Nato;
			Ustf = rhs.Ustf;
			IsChanged = rhs.IsChanged;
		}		
		return *this;
	}

	friend bool operator==(const PdaDataStruct& rrp1,const PdaDataStruct& rrp2)
	{
		return
			!strcmp(rrp1.Pdid,rrp2.Pdid) &&
			!strcmp(rrp1.Ipad,rrp2.Ipad) &&
			rrp1.Cdat == rrp2.Cdat &&
			rrp1.Lstu == rrp2.Lstu &&
			rrp1.Urno == rrp2.Urno &&
			!strcmp(rrp1.Usec,rrp2.Usec) &&
			!strcmp(rrp1.Useu,rrp2.Useu) &&
			rrp1.Vafr == rrp2.Vafr &&
			rrp1.Vato == rrp2.Vato &&
			rrp1.Nafr == rrp2.Nafr &&
			rrp1.Nato == rrp2.Nato &&
			rrp1.Ustf == rrp2.Ustf &&
			rrp1.IsChanged == rrp2.IsChanged;
	}

	friend bool operator!=(const PdaDataStruct& rrp1,const PdaDataStruct& r2)
	{
		return !operator==(rrp1,r2);
	}

}; // end AadDataStrukt

typedef struct PdaDataStruct PDADATA;

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaPdaData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapPtrToPtr omUstfMap;

    CCSPtrArray<PDADATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaPdaData();
	~CedaPdaData();
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(PDADATA *prpPda);
	bool InsertInternal(PDADATA *prpPda);
	bool Update(PDADATA *prpAad);
	bool UpdateInternal(PDADATA *prpPda);
	bool Delete(long lpUrno);
	bool RemoveFromStaff(long Ustf);
	bool RemoveFromAllPda(long lpUstf);
	bool DeleteInternal(PDADATA *prpPda);
	bool ReadSpecialData(CCSPtrArray<PDADATA> *popPda,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool Save(PDADATA *prpPda);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	PDADATA  *GetPdaByUrno(long lpUrno);
	PDADATA  *GetPdaByUstf(long lpUrno);
	void CedaPdaData::GetAllPdas(CCSPtrArray<PDADATA> &ropPdas);
	// Private methods
private:
    void PrepareAadData(PDADATA *prpPdaData);

};

extern CedaPdaData ogPdaData;

//---------------------------------------------------------------------------------------------------------

#endif //__CedaPdaData__
