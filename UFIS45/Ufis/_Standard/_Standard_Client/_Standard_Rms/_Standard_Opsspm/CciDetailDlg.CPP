// CciDD.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <ccsglobl.h>

#include <CCSPtrArray.h>
#include <CedaDemandData.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaShiftData.h>
#include <AllocData.h>
#include <CedaCicData.h>

#include <cviewer.h>
#include <CCSDragDropCtrl.h>

#include <CciDetailDlg.h>
#include <CciDetailWindow.h>

#include <BasicData.h>
#include <CCSTime.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CciDetailDialog dialog


CciDetailDialog::CciDetailDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CciDetailDialog::IDD, pParent)
{
	pcmAlocAlid = NULL;

	//{{AFX_DATA_INIT(CciDetailDialog)
	m_Alid = _T("");
	m_Type = _T("");
	m_Comm = _T("");
	m_Teln = _T("");
	m_Nafr = ogBasicData.GetTime();
	m_Nato = ogBasicData.GetTime();
	//}}AFX_DATA_INIT
}


void CciDetailDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CciDetailDialog)
	DDX_Text(pDX, IDC_ALID, m_Alid);
	DDX_Text(pDX, IDC_TYPE, m_Type);
	DDX_Text(pDX, IDC_COMM, m_Comm);
	DDX_Text(pDX, IDC_TELN, m_Teln);
//	DDX_CCSddmmyy(pDX, IDC_NAFRD, m_Nafr);
//	DDX_CCSTime(pDX, IDC_NAFRT, m_Nafr);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CciDetailDialog, CDialog)
	//{{AFX_MSG_MAP(CciDetailDialog)
	ON_BN_CLICKED(IDC_SCHLIEBEN, OnSchlieben)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CciDetailDialog message handlers

void CciDetailDialog::OnOK()
{
}

void CciDetailDialog::OnCancel()
{
}


void CciDetailDialog::OnSchlieben() 
{
	// TODO: Add your control notification handler code here
	
//	UpdateData();
    // fetch something

    // save data

    //delete (CciDetailWindow *) GetParent();
	GetParent()->DestroyWindow();
}

//static METAALLOCDATA *GetMetaAllocUnit(char *pcpAlid, ALLOCDATA *prpAllocData)
//{
//	for (int i = 0; i < ogCciAreas.omMetaAllocUnitList.GetSize(); i++)
//	{
//		//TRACE("ogCciAreas [%s] : ", ogCciAreas.omMetaAllocUnitList[i].Alid);
//		for (int j = 0; j < ogCciAreas.omMetaAllocUnitList[i].AllocationUnits.GetSize(); j++)
//		{
//			//TRACE("[%s] ", ogCciAreas.omMetaAllocUnitList[i].AllocationUnits[j].Alid);
//			if (strcmp(ogCciAreas.omMetaAllocUnitList[i].AllocationUnits[j].Alid, pcpAlid) == 0)  // found?
//			{
//				*prpAllocData = ogCciAreas.omMetaAllocUnitList[i].AllocationUnits[j];
//				return &ogCciAreas.omMetaAllocUnitList[i];
//			}
//		}
//		//TRACE("\n");
//	}
//	
//	return NULL;
//}

BOOL CciDetailDialog::OnInitDialog() 
{
	if (pcmAlocAlid != NULL)
	{
//		ALLOCDATA rlAllocData;
//		const METAALLOCDATA *prlMeta;
//		prlMeta = GetMetaAllocUnit(pcmAlocAlid, &rlAllocData);
//		const METAALLOCDATA *prlMA;
//		prlMA = ogCciAreas.GetSingleAllocUnitByAlid(pcmAlocAlid);
//
//		if (prlMA != NULL)
//		{
//			m_Alid = pcmAlocAlid;
//			m_Type = ogBasicData.GetTextById(CString(rlAllocData.Meta[4]));
//			m_Comm = prlMA->Comm;
//			m_Teln = prlMA->Teln;
//			m_Nafr = prlMA->Nafr;
//			m_Nato = prlMA->Nato;

		CICDATA *prlCic = ogCicData.GetCicByName(pcmAlocAlid);
		if(prlCic != NULL)
		{
			m_Alid = pcmAlocAlid;
			//m_Type = ogBasicData.GetTextById(CString(rlAllocData.Meta[4]));
			//???m_Type = ogBasicData.GetTextById(CString(rlAllocData.Meta[4]));
			//???m_Comm = prlCic->Comm;
			m_Teln = prlCic->Tele;
			m_Nafr = prlCic->Nafr;
			m_Nato = prlCic->Nato;
		}
		else
		{
			m_Alid = _T("");
			m_Type = _T("");
			m_Comm = _T("");
			m_Teln = _T("");
			m_Nafr = ogBasicData.GetTime();
			m_Nato = ogBasicData.GetTime();
		}
	}

	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CWnd *polWnd = GetDlgItem(IDC_SCHLIEBEN); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32882));
	}
	polWnd = GetDlgItem(IDC_DESK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32883));
	}
	polWnd = GetDlgItem(IDC_CCITYPE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32884));
	}
	polWnd = GetDlgItem(IDC_REMARK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32885));
	}
	polWnd = GetDlgItem(IDC_PHONE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32886));
	}
	polWnd = GetDlgItem(IDC_AVAILAB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32887));
	}
	polWnd = GetDlgItem(IDC_BELEGUNG); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32869));
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
