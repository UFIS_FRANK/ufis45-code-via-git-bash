// ReleaseDialog.cpp : implementation file
//
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CedaJobData.h>
#include <ReleaseDialog.h>
#include <CCSCedaData.h>
#include <BasicData.h>
#include <UndoManager.h>
#include <UndoClasses.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern UndoManager ogUndoManager;
/////////////////////////////////////////////////////////////////////////////
// ReleaseDialog dialog

ReleaseDialog::ReleaseDialog(CWnd* pParent /*=NULL*/)
	: CDialog(ReleaseDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(ReleaseDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

void ReleaseDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ReleaseDialog)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(ReleaseDialog, CDialog)
	//{{AFX_MSG_MAP(ReleaseDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ReleaseDialog message handlers

BOOL ReleaseDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetWindowText(GetString(IDS_STRING33022));
	CWnd *polWnd = GetDlgItem(IDC_SELECTALL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33023));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}
	
	CCSPtrArray <OFFLINE_DESCRIPTION> olOfflineDescriptions;
	CStringArray olErrors;
	ogOfflineDescription.GetOfflineDescriptions(olOfflineDescriptions, olErrors);
	int ilNumErrors = olErrors.GetSize();
	for(int ilE = 0; ilE < ilNumErrors; ilE++)
	{
		m_List.AddString(olErrors[ilE]);
	}
	int ilNumDescriptions = olOfflineDescriptions.GetSize();
	for(int ilD = 0; ilD < ilNumDescriptions; ilD++)
	{
		m_List.AddString(olOfflineDescriptions[ilD].Description);
	}
//	CStringArray olJobInfo;
//	ogUndoManager.GetUndoList(olJobInfo);
////	ogJobData.GetJobsChangedOffline(olJobInfo);
//	for(int i = 0; i < olJobInfo.GetSize(); i++)
//	{
//		m_List.AddString(olJobInfo[i]);
//	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ReleaseDialog::OnOK() 
{
	ogOfflineDescription.ClearAll();
	CDialog::OnOK();
}
