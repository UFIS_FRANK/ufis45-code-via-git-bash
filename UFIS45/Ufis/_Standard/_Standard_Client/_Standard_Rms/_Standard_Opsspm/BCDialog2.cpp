// BCDialog2.cpp : implementation file
//

#include "stdafx.h"
#include "opsspm.h"
#include "BCDialog2.h"
#include <CCSGlobl.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>

/*
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = "ReadMsgFromSock";
#endif
*/
ofstream of;

extern CCSBcHandle ogBcHandle;
static BCDialog *myThis = NULL;
/////////////////////////////////////////////////////////////////////////////
// BCDialog dialog

BCDialog::BCDialog(CWnd* pParent /*=NULL*/)
	: CDialog(BCDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(BCDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	myThis = this;
	lmMaxBCRecords=0;
	lmBCSpoolTimer=100;
	pfmStartBroadcastCallback = NULL;
	pfmEndBroadcastCallback = NULL;
	this->pomWorkerThread = NULL;
	bmCedaSenderConnected = false;
	bmTimerIsSet = false;
	char pclConfigPath[256];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	lmCurrWriteCount=0;
	lmCurrFileNo=0;
	CString olFileName;
	olFileName.Format(CCSLog::GetTmpPath("\\OpssPm_BC%ld.log"), lmCurrFileNo);

	omCurrFileName = olFileName;

	lmMaxBCRecords = GetPrivateProfileInt("GLOBAL", "EXTBCLOG", 0, pclConfigPath);
	lmBCSpoolTimer = GetPrivateProfileInt("GLOBAL", "BCSPOOLTIMER", 100, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "HOSTNAME", "", pcmHostName, sizeof pcmHostName, pclConfigPath);

	if(lmMaxBCRecords > 0)
	{
		of.open(olFileName.GetBuffer(0), ios::out);
	}
}

BCDialog::~BCDialog()
{
	if (this->pomWorkerThread != NULL)
	{
		this->pomWorkerThread->SuspendThread();
		delete this->pomWorkerThread;
		this->pomWorkerThread = NULL;
	}

	if(lmMaxBCRecords > 0)
	{
		of.close();
	}
	mapFilter.RemoveAll();
}

void BCDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BCDialog)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BCDialog, CDialog)
	//{{AFX_MSG_MAP(BCDialog)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_SIMULATE, OnSimulate)
	ON_BN_CLICKED(IDC_ABOUTBUTTON, OnAboutButton)
    ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_THREAD_MESSAGE(WM_THREAD_LOST_BC,OnThreadLostBc)
	ON_MESSAGE(WM_THREAD_LOST_BC,OnMessageLostBc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BCDialog message handlers

BEGIN_EVENTSINK_MAP(BCDialog, CDialog)
    //{{AFX_EVENTSINK_MAP(BCDialog)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()


void BCDialog::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	//ShowWindow(SW_HIDE);	
	// Do not call CDialog::OnPaint() for painting messages
}

BOOL BCDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int BCDialog::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}

void BCDialog::OnThreadLostBc(WPARAM wParam,LPARAM lParam)
{
	CString olMsg;
	switch(wParam)
	{
	case 0:
		olMsg.Format("Broadcast thread started!");
		AfxMessageBox(olMsg);
	break;
	case 1:
		olMsg.Format("Broadcast lost due to socket error %ld!\n Continue ?",lParam);
		if (AfxMessageBox(olMsg,MB_RETRYCANCEL) == IDCANCEL)
		{
			this->pomWorkerThread->SuspendThread();
		}
	break;
	case 2:
		olMsg.Format("Broadcast lost due to exception %s!\n Continue ?",(char *)lParam);
		if (AfxMessageBox(olMsg,MB_RETRYCANCEL) == IDCANCEL)
		{
			this->pomWorkerThread->SuspendThread();
		}
	break;
	}
}

LONG BCDialog::OnMessageLostBc(WPARAM wParam,LPARAM lParam)
{
	CString olMsg;
	switch(wParam)
	{
	case 0:
		olMsg.Format("Broadcast thread started!");
		AfxMessageBox(olMsg);
	break;
	case 1:
		olMsg.Format("Broadcast lost due to socket error %ld!\n Continue ?",lParam);
		if (AfxMessageBox(olMsg,MB_RETRYCANCEL) == IDCANCEL)
		{
			this->pomWorkerThread->SuspendThread();
		}
	break;
	case 2:
		olMsg.Format("Broadcast lost due to exception %s!\n Continue ?",(char *)lParam);
		if (AfxMessageBox(olMsg,MB_RETRYCANCEL) == IDCANCEL)
		{
			this->pomWorkerThread->SuspendThread();
		}
	break;
	}

	return 0L;
}

void BCDialog::OnSimulate() 
{
	CFileException olException;
	CString omErr;
	CStdioFile olFile;
	CString olFileName = CCSLog::GetUfisSystemPath("\\TestBcCom.txt");
	if( ! olFile.Open(olFileName,CFile::modeRead,&olException) )
	{
		if( olException.m_cause == CFileException::fileNotFound )
			// file not fouund
			omErr.Format("Error opening: \"%s\" - file not found",olFileName);
		else
			// other error
			omErr.Format("Error opening \"%s\" - error code %u",olFileName,olException.m_cause);
		AfxMessageBox(omErr);
	}
	else
	{
		int ilLineNo = 0;
		CString olReadBuf, olSelection, olFields, olData, olAttachment, olCmd, olTable;
//		CStringArray olHeader;
		while(olFile.ReadString(olReadBuf))
		{
//0			message expected in the following format
//			0     1     2     3   4      5   6   7   8
//1			ReqId,Dest1,Dest2,Cmd,Object,Seq,Tws,Twe,BcNum
//2			Selection
//3			Fields
//4			Data
//5			Attachment1
//:			: : : : : :
//N			AttachmentN

//			Example
//			ReqId,	"wks8u"
//			Dest1,	"bch"
//			Dest2,	"wks8u"
//			Cmd,	"URT"
//			Object,	"DRRTAB"
//			Seq,	""
//			Tws,	"0"
//			Twe,	"OPSSPM,,203,0,0"
//			BcNum	"27780"

			switch(ilLineNo++)
			{
			case 0:
//				ogBasicData.ExtractItemList(olReadBuf, &olHeader);
				olCmd = olReadBuf;
				break;
			case 1:
				olTable = olReadBuf;
				break;
			case 2:
				olSelection = olReadBuf;
				break;
			case 3:
				olFields = olReadBuf;
				break;
			case 4:
				olData = olReadBuf;
				break;
			default:
				if(!olAttachment.IsEmpty())
					olAttachment += "\n";
				olAttachment += olReadBuf;
				break;
			}

			if(olReadBuf.IsEmpty())
			{
				olAttachment.Replace('�','\n');
				// end of message
				//ogBcHandle.ProcessBroadcast(ReqId, Dest1, Dest2, Cmd, Object, Seq, Tws, Twe, Selection, Fields, Data, BcNum, Attachment, Additional);
				if(!olData.IsEmpty())
				{
					ogBcHandle.ProcessBroadcast("wksXX", ogUsername, "wksXX", olCmd, olTable, "", "0", "OPSSPM,,203,0,0", 
												olSelection, olFields, olData, "0", olAttachment, "");
				}
				ilLineNo = 0;
//				olHeader.RemoveAll();
				olCmd.Empty();
				olTable.Empty();
				olData.Empty();
				olSelection.Empty();
				olFields.Empty();
				olAttachment.Empty();
			}
		}
		if(!olData.IsEmpty())
		{
			olAttachment.Replace('�','\n');
			ogBcHandle.ProcessBroadcast("wksXX", ogUsername, "wksXX", olCmd, olTable, "", "0", "OPSSPM,,203,0,0", 
										olSelection, olFields, olData, "0", olAttachment, "");
		}
		olFile.Close();
	}
}

void BCDialog::OnAboutButton() 
{
	CString olMsg = "The Simulate Broadcasts button reads data from c:\\Ufis\\System\\TestBcCom.txt\n\nFormat:\nCOMMAND\nTABLE\nFIELD LIST\nDATA\nATTACHMENT LINE1\n: : : : : : : :\nATTACHMENT LINEn\nblank line\nnext COMMAND etc.\n\nNOTES: The ATTACHMENT lines are optional. The newline record seperators in the attachment are replaced with the following character �";
	MessageBox(olMsg,"Simulate Broadcasts",MB_ICONINFORMATION);
}


void BCDialog::SetStartBroadcastCallback(TCPIP_TRAFFIC_LIGHT_CALLBACK pfpStartBroadcastCallback)
{
	pfmStartBroadcastCallback = pfpStartBroadcastCallback;
	SetTimer(0, (UINT) lmBCSpoolTimer, NULL);
}

void BCDialog::SetEndBroadcastCallback(TCPIP_TRAFFIC_LIGHT_CALLBACK pfpEndBroadcastCallback)
{
	pfmEndBroadcastCallback = pfpEndBroadcastCallback;
}

CAttachment::CAttachment(CString &ropAttachment)
{
	bmAttachmentValid = true;

	CString olFieldsToken = "{=FIELDS=}", olFieldsEndToken  = "{=\\FIELDS=}";
	char *pclData = strstr(ropAttachment.GetBuffer(0), olFieldsToken);

	if(pclData == NULL)
		bmAttachmentValid = false; // invalid field list

	char *pclEnd = NULL;

	if(bmAttachmentValid)
	{
		pclData += strlen(olFieldsToken);
		if((pclEnd = strstr(pclData, olFieldsEndToken)) == NULL)
			pclEnd = pclData + strlen(pclData);
		omFieldList.Format("%.*s", (int) (pclEnd - pclData), pclData);
		pclData = pclEnd;
	}
	Trace("Fields: %s\n", omFieldList);

	if(bmAttachmentValid && (pclData = GetAttachmentData(pclData, "{=INSERT=}", omInsertDataList, "\n")) == NULL)
		bmAttachmentValid = false;
	if(bmAttachmentValid && (pclData = GetAttachmentData(pclData, "{=UPDATE=}", omUpdateDataList, "\n")) == NULL)
		bmAttachmentValid = false;
	if(bmAttachmentValid && (pclData = GetAttachmentData(pclData, "{=DELETE=}", omDeleteDataList, ",")) == NULL)
		bmAttachmentValid = false;

	int i = 0;
	Trace("{=INSERT=}\n");
	for(i = 0; i < omInsertDataList.GetSize(); i++)
		Trace("%s\n", omInsertDataList[i]);
	Trace("{=UPDATE=}\n");
	for(i = 0; i < omUpdateDataList.GetSize(); i++)
		Trace("%s\n", omUpdateDataList[i]);
	Trace("{=DELETE=}\n");
	for(i = 0; i < omDeleteDataList.GetSize(); i++)
		Trace("%s\n", omDeleteDataList[i]);
}

char *CAttachment::GetAttachmentData(char *pcpData, char *pcpToken, CStringArray &ropDataList, char *pcpRecSeparator)
{
	char *pclData = strstr(pcpData, pcpToken);
	if(pclData == NULL)
		return pcpData; // pcpToken not found

	if((pclData = strstr(pclData, DATA_TOKEN)) == NULL)
		return NULL; // error DATA_TOKEN not found

	pclData += strlen(DATA_TOKEN);
	char *pclEnd = strstr(pclData, DATA_ENDTOKEN);

	CString olData;	
	olData.Format("%.*s", (pclEnd != NULL) ? pclEnd - pclData : strlen(pclData), pclData);
	if(!olData.IsEmpty())
		ogBasicData.ExtractItemList(olData, &ropDataList, *pcpRecSeparator);

	return (pclEnd != NULL) ? pclEnd : pclData;
}
/*
char *CAttachment::GetAttachmentData(char *pcpData, char *pcpToken, CStringArray &ropDataList, char *pcpRecSeparator)
{
	char *pclData = pcpData;

	if((pclData = strstr(pclData, pcpToken)) == NULL)
		return pcpData; // token not found

	char *pclEnd = NULL;
	CString olDataToken = "{=DATA=}", olEndDataToken = "{=\\DATA=}";
	CString olCountToken = "{=COUNT=}";

	if((pclData = strstr(pclData, olCountToken)) == NULL)
		return NULL; // error (=COUNT=) not found
	pclData += strlen(olCountToken);
	int ilNumRecs = atoi(pclData);
	if(ilNumRecs <= 0)
		return pclData; // no records

	if((pclData = strstr(pclData, olDataToken)) == NULL)
		return NULL; // error (=DATA=) not found
	pclData += olDataToken.GetLength();

	int ilDataLen = 0;
	for(int ilRec = 0; ilRec < ilNumRecs; ilRec++)
	{
		int ilTokenLen = 1;
		if((pclEnd = strstr(pclData, pcpRecSeparator)) == NULL) // search for record separator
		{
			pclEnd = strstr(pclData, olEndDataToken); // or the end token
			ilTokenLen = olEndDataToken.GetLength();
		}

		ilDataLen = (pclEnd != NULL) ? pclEnd - pclData : strlen(pclData);
		CString olRec;	olRec.Format("%.*s",ilDataLen, pclData);
		ropDataList.Add(olRec);
		Trace("%s %d %d %s\n", pcpToken, ilNumRecs, ilDataLen, olRec);
		pclData = (pclEnd != NULL) ? pclEnd + ilTokenLen : pclData;
	}

	return pclData;
}*/
//		olRec.Format("%.*s",ilDataLen, pclData);
//		char *pclDataList = new char[ilDataLen + 10];
//		strncpy(pclDataList, pclData, ilDataLen);
//		pclDataList[ilDataLen] = 0x00; 
//		ropDataList.Add(pclDataList);
//		delete [] pclDataList;

// this is a safe version of TRACE (TRACE crashes when there are > 512 characters)
void CAttachment::Trace(char *pcpFormatList, ...)
{
	char pclText[512];
	memset(pclText,'\0',512);
	va_list args;
	va_start(args, pcpFormatList);
	_vsnprintf( pclText, 511, pcpFormatList, args);
	TRACE(pclText);
}


//MWO========================
char* BCDialog::GetKeyItem(char *pcpResultBuff, long *plpResultSize, 
				 char *pcpTextBuff, char *pcpKeyWord, 
				 char *pcpItemEnd, bool bpCopyData) 
{ 
	long llDataSize = 0L; 
	char *pclDataBegin = NULL; 
	char *pclDataEnd = NULL; 
	pclDataBegin = strstr(pcpTextBuff, pcpKeyWord); 
	/* Search the keyword */ 
	if (pclDataBegin != NULL) 
	{ 
		/* Did we find it? Yes. */ 
		pclDataBegin += strlen(pcpKeyWord); 
		/* Skip behind the keyword */ 
		pclDataEnd = strstr(pclDataBegin, pcpItemEnd); 
		/* Search end of data */ 
		if (pclDataEnd == NULL) 
		{ 
			/* End not found? */ 
			pclDataEnd = pclDataBegin + strlen(pclDataBegin); 
			/* Take the whole string */ 
		} /* end if */ 
		llDataSize = pclDataEnd - pclDataBegin; 
		/* Now calculate the length */ 
		if (bpCopyData == true) 
		{ 
			/* Shall we copy? */ 
			strncpy(pcpResultBuff, pclDataBegin, llDataSize); 
			/* Yes, strip out the data */ 
		} /* end if */ 
	} /* end if */ 
	if (bpCopyData == true) 
	{ 
		/* Allowed to set EOS? */ 
		pcpResultBuff[llDataSize] = 0x00; 
		/* Yes, terminate string */ 
	} /* end if */ 
	*plpResultSize = llDataSize; 
	/* Pass the length back */ 
	return pclDataBegin; 
	/* Return the data's begin */ 
} /* end GetKeyItem */ 


/////////////////////////////////////////////////////////////////////////////
// CBC_SimpleCtrl message handlers
void BCDialog::Message(LPCTSTR lpszMessage)
{
	//TO DO
/*	m_BCList.InsertString(0, lpszMessage);
	int ilCount = m_BCList.GetCount();
 	if (ilCount >= 500)
	{
		m_BCList.DeleteString (ilCount - 1);
	}
*/
}

void BCDialog::SendBCData(char *pclBuf)
{
	omSpooler.Add(pclBuf);

	BC_To_Client();
}

 
void BCDialog::BC_To_Client()
{
	char *pclDataBegin;
	char pclKeyWord[100];
	char pclResult[25000];
	char *pclAttach=NULL;
	long llAttachSize;
	char *pclData=NULL;
	long llDataSize;	
	long llSize;
	CString ReqId,  
			Dest1,  
			Dest2,  
			Cmd,  
			Object,  
			Seq,  
			Tws,  
			Twe,  
			Selection,  
			Fields,  
			Data,  
			BcNum,
			Queue,
			olAttach;

	CString myBCData;
	CSingleLock lock(&omSection);
	if (lock.Lock())
	{
		if(omSpooler.GetSize() > 0)
		{
			myBCData = omSpooler[0];
			omSpooler.RemoveAt(0);
		}
		lock.Unlock();
	}

	if (myBCData.GetLength() == 0)
	{
		return;
	}

	strcpy(pclKeyWord,"{=QUE=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Queue = CString(pclResult);
		m_bCQueueID = Queue;
	}
	strcpy(pclKeyWord,"{=USR=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		ReqId = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=USR=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Dest1  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=WKS=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Dest2  = CString(pclResult);
		ReqId = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=CMD=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Cmd  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=TBL=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Object  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=XXX=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Seq  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=TWS=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Tws  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=TWE=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Twe  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=WHE=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Selection  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=FLD=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Fields  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=DAT=}"); 
	pclData = GetKeyItem("", &llDataSize, myBCData.GetBuffer(0), pclKeyWord, "{=",false); 
	if (pclData != NULL) 
	{
		char tmp = pclData[llDataSize];
		pclData[llDataSize] = '\0';
		Data  = CString(pclData);
		pclData[llDataSize] = tmp;
	}
	strcpy(pclKeyWord,"{=BCNUM=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		BcNum  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=ATTACH=}"); 
	pclAttach = GetKeyItem("", &llAttachSize, myBCData.GetBuffer(0), pclKeyWord, "{=\\ATTACH=}", false); 
	if (pclAttach != NULL) 
	{ 
		pclAttach[llAttachSize] = '\0';
		olAttach = CString(pclAttach);
	}

	if (Cmd == "BCOUT" && !Queue.IsEmpty())
	{
		//m_bCQueueID = Queue;
		m_amIConnected = TRUE;
	}
	else
	{
		CString myBC;
		myBC = CString(myBCData.GetBuffer(0));
		//myBC.Replace(",", "\017");
		//MWO Dont log, becaus this makes no sense in this control.
		if(lmMaxBCRecords > 0)
		{
			of << myBCData.GetBuffer(0) << endl;
		}

		lmCurrWriteCount++;
		if(lmCurrWriteCount == lmMaxBCRecords)
		{
			lmCurrWriteCount=0;
			lmCurrFileNo++;
			CString olFileName;
			olFileName.Format(CCSLog::GetTmpPath("\\OpssPm_BC%ld.log"), lmCurrFileNo);
			if(lmMaxBCRecords > 0)
			{
				of.close();
				of.open(olFileName.GetBuffer(0), ios::out);
			}
		}

		//fire out
		FireEvent(ReqId, Dest1, Dest2, Cmd, Object, Seq, Tws, Twe, Selection, Fields, Data, BcNum, olAttach, "");
		//FireOnBc( ReqId, Dest1, Dest2, Cmd, Object, Seq, Tws, Twe, Selection, Fields, Data, BcNum, olAttach, "");
	}
}


void BCDialog::ShutDownSocket(CString opWho)
{

}

void BCDialog::Connect()
{
	if (CreateThread())
	{
		this->pomWorkerThread->ResumeThread();
	}
	// main thread must wait until worker thread has got the queue id !
	Sleep(1000);
}

void BCDialog::TransmitFilter() 
{
	CString olNewFilter;
	int i =0;

	for (POSITION pos2 = mapFilter.GetStartPosition(); pos2 != NULL;)
	{
		CString olString;
		CString *p = NULL;
		mapFilter.GetNextAssoc(pos2,olString,(void *&)p);
		if (!olString.IsEmpty())
			olNewFilter += olString + ";";
	}

	if(olNewFilter.GetLength() > 0)
	{
		olNewFilter = olNewFilter.Left(olNewFilter.GetLength()-1);
	}


	CBlockingSocket sock;
	CSockAddr addr;
	try
	{

		sock.Create();

		addr = sock.GetHostByName(this->pcmHostName,3358);

		sock.Connect(addr);

		int ilLen = this->m_bCQueueID.GetLength();
		int ilFilterLen = olNewFilter.GetLength();
		char pclS[1000]="";
		sprintf(pclS, "{=TOT=}%09d{=CMD=}BCKEY{=QUE=}%s{=DAT=}%s", ilLen + 42 + ilFilterLen, this->m_bCQueueID.GetBuffer(0), olNewFilter.GetBuffer(0));
		this->Message(CString(pclS));
		int err = sock.Send(pclS, ilLen + 42 + ilFilterLen);

	}
	catch(const char* e)
	{
		CString olMsg;
		int ilErr = sock.GetLastError();
		if(ilErr != 0)
		{
			olMsg.Format("Transmit Filter: CEDA-Socket Error: %d", ilErr);
			AfxMessageBox(olMsg);
		}
		else
		{
			olMsg.Format("Transmit Filter: Exception: %s occured", e);
			AfxMessageBox(olMsg);
		}
	}

	sock.Cleanup();

}
void BCDialog::SetFilter(char* strTable, char* strCommand, long bOwnBC, char* application) 
{
	myThis->mapFilter.SetAt(strTable, (void*)strTable);
}

void BCDialog::FireEvent(LPCTSTR ReqId, LPCTSTR Dest1, LPCTSTR Dest2, LPCTSTR Cmd, LPCTSTR Object, LPCTSTR Seq, LPCTSTR Tws, LPCTSTR Twe, LPCTSTR Selection, LPCTSTR Fields, LPCTSTR Data, LPCTSTR BcNum, LPCTSTR Attachment, LPCTSTR Additional)
{
	if(pfmStartBroadcastCallback != NULL)
		pfmStartBroadcastCallback();
	ogBcHandle.ProcessBroadcast(ReqId, Dest1, Dest2, Cmd, Object, Seq, Tws, Twe, Selection, Fields, Data, BcNum, Attachment, Additional);
	if(pfmEndBroadcastCallback != NULL)
		pfmEndBroadcastCallback();
}

void BCDialog::OnTimer(UINT nIDEvent)
{
	static bool smNoUpdatesNow = false;

	if (smNoUpdatesNow == false)
	{
		smNoUpdatesNow = true;
		BC_To_Client();
		smNoUpdatesNow = false;
	}
}

void BCDialog::OnClose() 
{
	KillTimer(0);
}

bool BCDialog::CreateThread()
{
	this->pomWorkerThread = AfxBeginThread(WorkerFunction,this,THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED,NULL);
	if (this->pomWorkerThread != NULL)
		return true;
	else
		return false;
}


/*UINT BCDialog::WorkerFunction( LPVOID pParam )
{
	bool blRet = true;
	int	ilErr = 0;
	CString olMsg;

	char *pclReturnBuffer;
	char pclRetFirst[100];
	char pclKeyWord[100];
	char pclResult[25000];
	char *pclDataBegin;
	char *pclCurrPtr = NULL;
	char *pclTotBufBegin;
	char *pclTotalDataBuffer = NULL;
	int  ilBytes;
	long llSize;
	long llTransferBytes = 0;
	long llCurrentBytes;

	BCDialog *polDlg = (BCDialog *)pParam;

	CBlockingSocket sock;
	CSockAddr addr;


	try
	{
		sock.Create();

		addr = sock.GetHostByName(polDlg->pcmHostName,3358);

		sock.Connect(addr);

		char pclS[60000]="{=TOT=}       28{=CMD=}BCOUT";
		int err = sock.Send(pclS, 28);
		memset((void*)pclRetFirst, 0x00, 100);
		int len = sock.Receive(pclRetFirst, 99,2000);
		sock.Send("{=TOT=}       27{=ACK=}NEXT", 27);
		
	}
	catch(const char* e)
	{
		ilErr = sock.GetLastError();
		if(ilErr != 0)
		{
			olMsg.Format("CEDA-Socket Error: %d", ilErr);
			AfxMessageBox(olMsg);
			return false;
		}
		else
		{
			AfxMessageBox(e);
			return false;
		}
	}

	while(true)
	{
		try
		{
			memset((void*)pclRetFirst, 0x00, 100);
			int len = sock.Receive(pclRetFirst, 16,2000);
		}
		catch(const char* e)
		{
			ilErr = sock.GetLastError();
			if(ilErr != 0)
			{
				olMsg.Format("CEDA-Socket Error: %d", ilErr);
				AfxMessageBox(olMsg);
				return false;
			}
			else
			{
				AfxMessageBox(e);
				return false;
			}
		}

		if(strcmp(pclRetFirst, "") == 0)
			return false;

		strcpy(pclKeyWord,"{=TOT=}"); 
		pclDataBegin = polDlg->GetKeyItem(pclResult, &llSize, pclRetFirst, pclKeyWord, "{=", true); 
		if (pclDataBegin == NULL)
		{
			int ilShit = 99;
		}

		long llTotal = atol(pclResult);
		llTotal -= 16;
		if (llTotal < 0) //take care that it is not negative
		{
			llTotal = 0;
		}
		if(llTotal > 8000)
		{
			llTotal = llTotal;
		}
		pclReturnBuffer = (char*)malloc((size_t)llTotal+1);
		memset((void*)pclReturnBuffer, 0x00, llTotal+1);
		if (pclTotalDataBuffer == NULL)
		{
			pclTotalDataBuffer = (char*)malloc((size_t)llTotal+1);
		}

		memset((void*)pclTotalDataBuffer, 0x00, llTotal+1);

		try
		{
			ilBytes = sock.Receive(pclTotalDataBuffer, llTotal, 2000);
			if(strcmp(pclTotalDataBuffer, "") == 0)
				return false;
		}
		catch(const char* e)
		{
			ilErr = sock.GetLastError();
			if (ilErr != 0)
			{
				olMsg.Format("CEDA-Socket Error: %d", ilErr);
				AfxMessageBox(olMsg);
				return false;
			}
			else
			{
				AfxMessageBox(e);
				return false;
			}
		}

		pclTotBufBegin = pclTotalDataBuffer;
		pclTotalDataBuffer = pclTotalDataBuffer + ilBytes;
		llCurrentBytes = strlen(pclReturnBuffer);
		llTransferBytes += ilBytes;
		struct linger rmLinger;
		rmLinger.l_onoff = 0;
		rmLinger.l_linger = 2;
		while (llTransferBytes < llTotal)
		{
			try
			{
				ilBytes = sock.Receive(pclTotalDataBuffer, (llTotal-llTransferBytes), 2000);
				ilErr = sock.GetLastError();
				if(ilErr != 0)
				{
					olMsg.Format("CEDA-Socket Error: %d", ilErr);
					AfxMessageBox(olMsg);
					return false;
				}
				pclTotalDataBuffer = pclTotalDataBuffer + ilBytes;
				llTransferBytes += ilBytes;
			}
			catch(const char* e)
			{
				ilErr = sock.GetLastError();
				if(ilErr != 0)
				{
					olMsg.Format("CEDA-Socket Error: %d", ilErr);
					AfxMessageBox(olMsg);
					return false;
				}
				else
				{
					AfxMessageBox(e);
					return false;
				}
			}
		}

//		sock.Send("{=TOT=}       27{=ACK=}NEXT", 27);

		pclTotalDataBuffer = pclTotBufBegin;

		free(pclReturnBuffer);
		pclReturnBuffer = NULL;
		CSingleLock lock(&polDlg->omSection);
		if (lock.Lock())
		{
			polDlg->omSpooler.Add(pclTotalDataBuffer);
			lock.Unlock();
		}
		free(pclTotalDataBuffer);
		pclTotalDataBuffer = NULL;
	}		
	sock.Cleanup();
	return 0;
}
*/


UINT BCDialog::WorkerFunction( LPVOID pParam )
{
	BCDialog *polDlg = (BCDialog *)pParam;

	CString olMsg;
	char pclRetFirst[100];
	int	ilErr = 0;
	CBlockingSocket sock;
	CSockAddr addr;
	STR_DESC rlMsg;
	rlMsg.Value = NULL;
	rlMsg.AllocatedSize = 0;
	rlMsg.UsedLen = 0;

	FILE *fp = NULL;

	char pclConfigPath[256];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

resume:
	try
	{
//		polDlg->PostMessage(WM_THREAD_LOST_BC,0,0L);

		char pclTmpText[512];
		GetPrivateProfileString("GLOBAL","THREADBCLOG","",pclTmpText, sizeof pclTmpText, pclConfigPath);
		if (strlen(pclTmpText) > 0)
		{
			fp = fopen(pclTmpText,"wt");
		}

		sock.Create();

		addr = sock.GetHostByName(polDlg->pcmHostName,3358);

		sock.Connect(addr);

		char pclS[60000]="{=TOT=}       28{=CMD=}BCOUT";
		int err = sock.Send(pclS, 28);

		memset((void*)pclRetFirst, 0x00, 100);
		int len = sock.Receive(pclRetFirst, 99,2000);

		char pclResult[256];
		long llSize = 0;
		if (polDlg->GetKeyItem(pclResult, &llSize, pclRetFirst, "{=QUE=}", "{=", true) != NULL)
		{
			polDlg->m_bCQueueID = CString(pclResult);
		}

		sock.Send("{=TOT=}       27{=ACK=}NEXT", 27);

		while(true)
		{
			if (ReadMsgFromSock(fp,polDlg, sock, &rlMsg, FALSE) == FALSE)
			{
				CSingleLock lock(&polDlg->omSection);
				if (lock.Lock())
				{
					polDlg->omSpooler.Add(rlMsg.Value);
					lock.Unlock();
				}
			}
		}

	}
	catch(const char* e)
	{
		ilErr = sock.GetLastError();
		if(ilErr != 0)
		{
			olMsg.Format("CEDA-Socket Error: %d", ilErr);
			polDlg->PostMessage(WM_THREAD_LOST_BC,1,ilErr);
			if (fp != NULL)
			{
				fclose(fp);
				fp = NULL;
			}
			sock.Cleanup();
			goto resume;
		}
		else
		{
			polDlg->PostMessage(WM_THREAD_LOST_BC,2,(long)e);
			if (fp != NULL)
			{
				fclose(fp);
				fp = NULL;
			}
			sock.Cleanup();
			goto resume;
		}

	}

	sock.Cleanup();
	if (fp != NULL)
		fclose(fp);

	return 0;
}
/* *************************************************************** */
/* *************************************************************** */
BOOL BCDialog::ReadMsgFromSock(FILE *fp,BCDialog *popDlg, CBlockingSocket &ropSock, STR_DESC *rpMsg, BOOL ipGetAck)
{
  int ilStep = 0;
  int ilRcvNow = 0;
  int ilGotNow = 0;
  int ilRcvLen = 0;
  BOOL blBreakOut = FALSE;
  long llDatLen = 0;
  char pclKeyVal[32];
  char *pclPtr = NULL;

  blBreakOut = FALSE;
  ilRcvNow = 16;
  rpMsg->UsedLen = 0;
  while ((ilStep < 2) && (blBreakOut == FALSE)) 
  {
    ilStep++;
    ilRcvLen = ilRcvNow + 16;
    if (rpMsg->AllocatedSize < ilRcvLen)
    {
      rpMsg->Value = (char*)realloc(rpMsg->Value, ilRcvLen+16+1);
      rpMsg->AllocatedSize = ilRcvLen;
    }
    pclPtr = rpMsg->Value + rpMsg->UsedLen;
    *pclPtr = 0x00;

	if (fp != NULL)
	    fprintf(fp,"%s READING %d BYTES FROM SOCKET\n", "ReadMsgFromSock 1013", ilRcvNow);

    ilRcvLen = 0;
    while ((ilRcvLen < ilRcvNow) && (blBreakOut == FALSE))
    {
		if (fp != NULL)
			fprintf(fp,"%s TRYING TO RECEIVE %d BYTES\n", "ReadMsgFromSock 1017", ilRcvNow-ilRcvLen);
		ilGotNow = ropSock.Receive(pclPtr, ilRcvNow-ilRcvLen,2000);

        if (ilGotNow > 0)
        {
          ilRcvLen += ilGotNow;
          pclPtr += ilGotNow;
          rpMsg->UsedLen += ilGotNow;
          if (ilRcvLen < ilRcvNow)
          {
			if (fp != NULL)
				fprintf(fp,"%s RECEIVED THIS TIME %d BYTES\n", "ReadMsgFromSock 1026", ilGotNow);
            *pclPtr = 0x00;
            if (strcmp(rpMsg->Value,"{=ACK=}") == 0)
            {
				if (fp != NULL)
					fprintf(fp,"%s RECEIVED <%s> FROM CLIENT\n", "ReadMsgFromSock 1030",rpMsg->Value);
				blBreakOut = TRUE;
            }
          }
        }
        if (ilGotNow < 0)
        {
			if (fp != NULL)
				fprintf(fp,"%s SOCKET ERROR (%d) DETECTED\n", "ReadMsgFromSock 1037", ilGotNow);
			blBreakOut = TRUE;
        }
        if (ilGotNow == 0)
        {
			if (fp != NULL)
				fprintf(fp,"%s LOST SERVERCONNECTION DETECTED\n", "ReadMsgFromSock 1042");
			blBreakOut = TRUE;
        }
    } /* end while read  */
    *pclPtr = 0x00;
    
    if (blBreakOut == FALSE)
    {
      if (ilStep == 1)
      {
        popDlg->GetKeyItem(pclKeyVal,&llDatLen,rpMsg->Value,"{=TOT=}","{=", TRUE);
        ilRcvNow = atoi(pclKeyVal) - 16;
        if (ilRcvNow <=0)
        {
			if (fp != NULL)
			{
				fprintf(fp,"%s RECEIVED <%s> FROM CLIENT\n", "ReadMsgFromSock 1056",rpMsg->Value);
				fprintf(fp,"%s NO FURTHER DATA EXPECTED\n", "ReadMsgFromSock 1057");
			}
          blBreakOut = TRUE;
        }
      }
    }
  } /* end while ilStep < 2*/
  *pclPtr = 0x00;

  if ((blBreakOut == FALSE) && (ipGetAck == TRUE))
  {
    popDlg->GetKeyItem(pclKeyVal,&llDatLen,rpMsg->Value,"{=ACK=}","{=", TRUE);
	if (fp != NULL)
		fprintf(fp,"%s RECEIVED %s <%s> LEN=%d\n", "ReadMsgFromSock 1068","{=ACK=}", pclKeyVal, llDatLen);
    if (llDatLen > 0)
    {
/*      if (strcmp(pclKeyVal,"CLOSE") == 0)
      {
        blBreakOut = TRUE;
      }
      if (strcmp(pclKeyVal,"NEXT") == 0)
      {
        igTmpAlive = TRUE;
      }
      if (strcmp(pclKeyVal,"KEEP") == 0)
      {
        igTmpAlive = TRUE;
      }
      if (strcmp(pclKeyVal,"ALIVE") == 0)
      {
        igTmpAlive = TRUE;
      }
*/
    }
    else
    {
		if (fp != NULL)
			fprintf(fp,"%s EXPECTED ACK NOT FOUND IN <%s>\n","ReadMsgFromSock 1091",rpMsg->Value);
		blBreakOut = TRUE;
    }

	if (fp != NULL)
	{
		fprintf(fp,"Broadcast completed\n");
		fflush(fp);
	}
  }

  return blBreakOut;
}

int BCDialog::SpooledBroadcasts()
{
	if (myThis != NULL)
	{
		int ilSize = 0;
		CSingleLock lock(&myThis->omSection);
		if (lock.Lock())
		{
			ilSize = myThis->omSpooler.GetSize();
			lock.Unlock();
		}

		return ilSize;
	}
	else
	{
		return 0;
	}
}