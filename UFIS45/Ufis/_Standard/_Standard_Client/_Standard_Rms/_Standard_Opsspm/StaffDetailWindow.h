// StaffDetailWindow.h : header file
//


#ifndef _STAFFDW_
#define _STAFFDW_

#include <StaffDetailDlg.h>
#include <StaffDetailViewer.h>
#include <StaffDetailGantt.h>

#define StaffData  STAFFDATA

/////////////////////////////////////////////////////////////////////////////
// StaffDetailWindow frame

class StaffDetailWindow : public CFrameWnd
{
	DECLARE_DYNCREATE(StaffDetailWindow)

protected:
    StaffDetailWindow();

// Attributes
public:

// Operations
public:
	StaffDetailWindow(CWnd *popParent, long lpJobPrimaryKey);
	virtual ~StaffDetailWindow();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(StaffDetailWindow)
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(StaffDetailWindow)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
    long lmJobPrimaryKey;

	EMPDATA *pomEmp;
	SHIFTDATA *pomShift;
	CCSPtrArray<JOBDATA> *pomJobs;

    StaffDetailDialog omStaffDetail;
    CTimeScale omTimeScale;
    CStatusBar omStatusBar;

    
    int imStartTimeScalePos;
    int imBottomPos;

    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;

	
	StaffDetailViewer omViewer;
public:
	StaffDetailGantt omGantt;

private:      
    static CPoint omMaxTrackSize;
    static CPoint omMinTrackSize;
};

/////////////////////////////////////////////////////////////////////////////

#endif // _STAFFDW_

