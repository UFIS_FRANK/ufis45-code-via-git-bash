// CedaHagData.cpp - Class for Handling Agents
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaHagData.h>
#include <BasicData.h>


CedaHagData::CedaHagData()
{                  
    BEGIN_CEDARECINFO(HAGDATA, HagDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Faxn,"FAXN")
		FIELD_CHAR_TRIM(Hnam,"HNAM")
		FIELD_CHAR_TRIM(Hsna,"HSNA")
		FIELD_CHAR_TRIM(Prfl,"PRFL")
		FIELD_CHAR_TRIM(Tele,"TELE")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(HagDataRecInfo)/sizeof(HagDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&HagDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"HAGTAB");
	strcpy(clFieldList, "URNO,FAXN,HNAM,HSNA,PRFL,TELE");
    pcmFieldList = clFieldList;
}
 
CedaHagData::~CedaHagData()
{
	TRACE("CedaHagData::~CedaHagData called\n");
	ClearAll();
}

void CedaHagData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omHnamMap.RemoveAll();
	omHsnaMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaHagData::ReadHagData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];

    
	strcpy(clFieldList,"URNO,FAXN,HNAM,HSNA,PRFL,TELE");


	strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaHagData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		HAGDATA rlHagData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlHagData)) == RCSuccess)
			{
				AddHagInternal(rlHagData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaHagData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(HAGDATA), pclWhere);
    return ilRc;
}


void CedaHagData::AddHagInternal(HAGDATA &rrpHag)
{
	HAGDATA *prlHag = new HAGDATA;
	*prlHag = rrpHag;
	omData.Add(prlHag);
	omUrnoMap.SetAt((void *)prlHag->Urno,prlHag);
	omHnamMap.SetAt(prlHag->Hnam,prlHag);
	omHsnaMap.SetAt(prlHag->Hsna,prlHag);
}


HAGDATA* CedaHagData::GetHagByUrno(long lpUrno)
{
	HAGDATA *prlHag = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlHag);
	return prlHag;
}

HAGDATA* CedaHagData::GetHagByName(const CString& ropName)
{
	HAGDATA *prlHag = NULL;
	omHnamMap.Lookup(ropName, (void *&) prlHag);
	return prlHag;
}

HAGDATA* CedaHagData::GetHagByHsna(const CString& ropName)
{
	HAGDATA *prlHag = NULL;
	omHsnaMap.Lookup(ropName, (void *&) prlHag);
	return prlHag;
}

CString CedaHagData::GetTableName(void)
{
	return CString(pcmTableName);
}

int CedaHagData::GetCountOfRecords()
{
	return omData.GetSize();
}

int	CedaHagData::GetNameList(CStringArray& ropNames)
{
	for (int i = 0;i < omData.GetSize(); i++)
	{
		ropNames.Add(omData[i].Hnam);		
	}

	return omData.GetSize();
}

int	CedaHagData::GetShortNameList(CStringArray& ropNames)
{
	for (int i = 0;i < omData.GetSize(); i++)
	{
		ropNames.Add(omData[i].Hsna);		
	}

	return omData.GetSize();
}

bool CedaHagData::GetNameList(const CStringArray& ropShortNames,CStringArray& ropNames)
{
	for (int i = 0; i < ropShortNames.GetSize(); i++)
	{
		if (ropShortNames[i] == GetString(IDS_ALLSTRING))
			ropNames.Add(GetString(IDS_ALLSTRING));
		else if (ropShortNames[i] == GetString(IDS_NOAGENT))
			ropNames.Add(GetString(IDS_NOAGENT));
		else
		{
			HAGDATA *prlHag = NULL;
			if (!omHsnaMap.Lookup(ropShortNames[i],(void *&)prlHag) || !prlHag)
				return false;
			ropNames.Add(prlHag->Hnam);
		}
	}

	return true;
}

bool CedaHagData::GetShortNameList(const CStringArray& ropNames,CStringArray& ropShortNames)
{
	for (int i = 0; i < ropNames.GetSize(); i++)
	{
		if (ropNames[i] == GetString(IDS_ALLSTRING))
			ropShortNames.Add(GetString(IDS_ALLSTRING));
		else if (ropNames[i] == GetString(IDS_NOAGENT))
			ropShortNames.Add(GetString(IDS_NOAGENT));
		else
		{
			HAGDATA *prlHag = NULL;
			if (!omHnamMap.Lookup(ropNames[i],(void *&)prlHag) || !prlHag)
				return false;
			ropShortNames.Add(prlHag->Hsna);
		}
	}

	return true;
}
