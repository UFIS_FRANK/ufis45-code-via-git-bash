// Meta Groups
#ifndef _CEDASGRDATA_H_
#define _CEDASGRDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct SgrDataStruct
{
	long	Urno;		// Unique Record Number
	char	Grpn[33];	// Group Name eg "A01-A03"
	char	Grds[65];	// Group Description
	char	Tabn[7];	// Table name with further info about this group
	long	Ugty;		// Urno of Group Type (ALOTAB) eg "GATEBEREICHE"
};

typedef struct SgrDataStruct SGRDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaSgrData: public CCSCedaData
{
public:
    CCSPtrArray <SGRDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omGroupNameMap;
	CString GetTableName(void);
	void ProcessSgrBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CedaSgrData();
	~CedaSgrData();
	bool ReadSgrData();
	SGRDATA* GetSgrByUrno(long lpUrno);
	void GetSgrByUgty(long lpUalo, CCSPtrArray <SGRDATA> &ropSgrList, bool bpReset = true);
	bool ReadSpecialData(CCSPtrArray<SGRDATA> *popSgr,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	long GetSgrUrnoByName(const char *pcpName);

private:
	SGRDATA *AddSgrInternal(SGRDATA &rrpSgr);
	void DeleteSgrInternal(long lpUrno);
	void ClearAll();
};


extern CedaSgrData ogSgrData;
#endif _CEDASGRDATA_H_
