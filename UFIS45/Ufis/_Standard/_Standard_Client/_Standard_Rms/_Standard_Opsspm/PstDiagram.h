// PstDiagram.h : header file
//

#ifndef _PST_DIAGRAM_
#define _PST_DIAGRAM_

#include <clientwn.h>
#include <tscale.h>
#include <PstViewer.h>
#include <CCSButtonCtrl.h>

#define AUTOSCROLL_TIMER_EVENT 2
#define AUTOSCROLL_INITIAL_SPEED 10 // millisecs
#define AUTOSCROLL_SPEED 1000 // millisecs

/////////////////////////////////////////////////////////////////////////////
// PstDiagram frame

class PstDiagram : public CFrameWnd
{
    DECLARE_DYNCREATE(PstDiagram)

public:


    PstDiagram();         // protected constructor used by dynamic creation
    PstDiagram(BOOL bpPrePlanMode,CTime opPrePlanTime);    

// Attributes
public:

private:
	BOOL bmIsViewOpen;
	CRect omWindowRect; //PRF 8712

// Operations 
public:

	

	CPtrArray		omFlightPlanArray;
	CTime omTimeBandStartTime, omTimeBandEndTime;
	void SetTimeBand(CTime opStartTime, CTime opEndTime);
	void UpdateTimeBand();
    void PositionChild();
    void SetTSStartTime(CTime opTSStartTime);
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName);
	void OnUpdatePrevNext(void);
	void PrePlanMode(BOOL bpToSet,CTime opPrePlanTime);
	void SetCaptionText(void);
	BOOL DestroyWindow() ;
	CTime GetTsStartTime();
    CTimeSpan GetTsDuration(void);
    void OnFirstChart();
    void OnLastChart();
	BOOL bmNoUpdatesNow;

// Internal used only
// Id 25-Sep-96 -- add keyboard handling to the diagram
private:
	CListBox *GetBottomMostGantt();

// Overrides
public:
	void SetPstAreaButtonColor(int ipGroupno);
	void SetAllPstAreaButtonsColor(void);

// Implementation
protected:
    virtual ~PstDiagram();

    // Generated message map functions
    //{{AFX_MSG(PstDiagram)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnMove(int x, int y); //PRF 8712
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnNextChart();
    afx_msg void OnPrevChart();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
    afx_msg void OnArrival();
    afx_msg void OnDeparture();
    afx_msg void OnAnsicht();
	afx_msg void OnViewSelChange();
	afx_msg void OnCloseupView();
    afx_msg void OnMabstab();
    afx_msg void OnZeit();
	afx_msg void OnPrint();
	afx_msg void OnAssign();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
    afx_msg LONG OnDragOver(UINT, LONG); 
	afx_msg void OnUpdateUIZeit(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIMabstab(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIPrint(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIAnsicht(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIView(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIAssign(CCmdUI *pCmdUI);
    afx_msg LONG OnSelectDiagram(WPARAM wParam, LPARAM lParam);

	afx_msg void OnFilterFlight();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

protected:


    CDialogBar omDialogBar;
    C2StateButton omArrival;
    C2StateButton omDeparture;
    C3DStatic omTime;
    C3DStatic omDate;
    C3DStatic omTSDate;
	CCSButtonCtrl *pomAssignButton;
    
    CTimeScale omTimeScale;
    CBitmapButton omBB1, omBB2;

	long CalcTotalMinutes(void);
    
    CClientWnd omClientWnd;
    CStatusBar omStatusBar;
    
    CTime omStartTime;
    CTimeSpan omDuration;
    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
    
    CPtrArray omPtrArray;
    int imFirstVisibleChart;
    int imStartTimeScalePos;
    
    CTime omPrePlanTime;
	BOOL omPrePlanMode;
	CString omCaptionText;

    PstDiagramViewer omViewer;

// Redisplay all methods for DDX call back function
public:
	void RedisplayAll();

	void HandleGlobalDateUpdate(CTime opDate);
	void ProcessEndAssignment();
	void ProcessStartAssignment();

	bool bmScrolling;
	void AutoScroll(UINT ipInitialScrollSpeed = AUTOSCROLL_INITIAL_SPEED);
	void OnAutoScroll(void);
	CCSDragDropCtrl m_DragDropTarget;

private:
    static CPoint omMaxTrackSize;
    static CPoint omMinTrackSize;
    static COLORREF lmBkColor;
    static COLORREF lmTextColor;
    static COLORREF lmHilightColor;
};

/////////////////////////////////////////////////////////////////////////////

#endif // _PST_DIAGRAM_
