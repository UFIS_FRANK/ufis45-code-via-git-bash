// StaffDetailDlg.h : header file
//  

#ifndef _STAFFDD_
#define _STAFFDD_

#include <CedaSprData.h>
#define STAFFDATA	EMPDATA
#define StaffData	STAFFDATA

/////////////////////////////////////////////////////////////////////////////
// StaffDetailDialog dialog

class StaffDetailDialog : public CDialog
{
// Construction
public:
	StaffDetailDialog(CWnd* pParent = NULL);   // standard constructor
	~StaffDetailDialog();
    void SetStaffPtr(StaffData *popStaff) { pomStaff = popStaff; }
	void SetShiftPtr(SHIFTDATA *popShift) { pomShift = popShift; }

	void ProcessShiftUpdate(SHIFTDATA *prpShift);
	void ProcessShiftDelete(long lpShiftUrno);
	void ProcessSprUpdate(SPRDATA *prpSpr);
	void ProcessSprDelete();
	void UpdateClockingValues();
	void SetEditFieldsStat(void);
	void SaveChanges(void);
	void CheckOvertimeValue(UINT ipId);

// Dialog Data
	//{{AFX_DATA(StaffDetailDialog)
	enum { IDD = IDD_STAFF_DETAIL };
	CEdit	m_CompleteCtrl;
	CEdit	m_AfterCtrl;
	CEdit	m_BeforeCtrl;
	CComboBox	m_Sub1Sub2Ctrl;
	CEdit	m_AcfrtCtrl;
	CEdit	m_ActotCtrl;
	CEdit	m_ActodCtrl;
	CEdit	m_AcfrdCtrl;
	CString m_Disp;
	CString m_Rank;
	CString m_MultiFunctions;
	CString m_Qualifications;
	CString	m_Name;
	CString	m_Finm;
	CString	m_Lanm;
	CString	m_Nickname;
	CString	m_Initials;
	CString m_Pkno;
	CString m_Tmid;
	CString m_Stid;
	CString m_Sti2;
	CTime	m_Acfrd;
	CTime	m_Acfrt;
	CTime	m_Actod;
	CTime	m_Actot;
	CString	m_ClockOnDate;
	CString	m_ClockOnTime;
	CString	m_ClockOffDate;
	CString	m_ClockOffTime;
	CString m_Text;
	CString m_Text2;
	CString	m_Sub1Sub2;
	CString	m_Before;
	CString	m_After;
	CString	m_Complete;
	CString m_Tpno;
	//}}AFX_DATA

protected:
    StaffData	*pomStaff;
	SHIFTDATA	*pomShift;
	CTime		omClockOn;
	CTime		omClockOff;
	CString		omOldText;
	CTime		omAvfr;
	CTime		omAvto;
	CString		omOldBefore, omOldAfter, omOldComplete;

	CString omOldAcfrt, omOldActot, omOldActod, omOldAcfrd;
	bool bmChanged;
	void SetSub1Sub2();
	CString omOldSub1Sub2;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(StaffDetailDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(StaffDetailDialog)
	afx_msg void OnSchlieben();
	virtual BOOL OnInitDialog();
	afx_msg void OnReset();
	afx_msg void OnKillfocusClockOnDate();
	afx_msg void OnKillfocusClockOnTime();
	afx_msg void OnKillfocusClockOffDate();
	afx_msg void OnKillfocusClockOffTime();
	afx_msg void OnDestroy();
	afx_msg void OnKillfocusAcfrd();
	afx_msg void OnKillfocusAcfrt();
	afx_msg void OnKillfocusActod();
	afx_msg void OnKillfocusActot();
	afx_msg void OnChangeEZcode();
	afx_msg void OnChangeEZcode2();
	afx_msg void OnChangeEZcode3();
	afx_msg void OnKillfocusEZcode();
	afx_msg void OnKillfocusEZcode2();
	afx_msg void OnKillfocusEZcode3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	BOOL OnCommand(WPARAM wParam ,LPARAM lParam);

private:
// Prototypes
	static void StaffDetailDialogCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
};

#endif
