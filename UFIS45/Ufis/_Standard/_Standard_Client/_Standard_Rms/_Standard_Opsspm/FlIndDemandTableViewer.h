#ifndef __FIDVIEWER_H__
#define __FIDVIEWER_H__

#include <cviewer.h>
#include <table.h>
#include <ccsprint.h>

struct FLINDEM_LINE
{
	long		DemandUrno;
	CString		Name;
	CString		Code;
	CString		Dety;
	CString		Function;
	CString		Permit;
	CTime		From;
	CTime		Until;
	CTimeSpan	Duration;
	CStringArray Jobs;
	bool InvalidDemand; // display invalid demands in red

	FLINDEM_LINE(void)
	{
		InvalidDemand = false;
	}

	FLINDEM_LINE(const FLINDEM_LINE& rhs)
	{
		*this = rhs;
	}

	FLINDEM_LINE& FLINDEM_LINE::operator=(const FLINDEM_LINE& rhs)
	{
		if (&rhs != this)
		{
			DemandUrno = rhs.DemandUrno;
			Name = rhs.Name;
			Code = rhs.Code;
			Dety = rhs.Dety;
			Function = rhs.Function;
			Permit = rhs.Permit;
			From = rhs.From;
			Until = rhs.Until;
			Duration = rhs.Duration;
			InvalidDemand = rhs.InvalidDemand;
			Jobs.RemoveAll();
			for(int ilJ = 0; ilJ < rhs.Jobs.GetSize(); ilJ++)
			{
				Jobs.Add(rhs.Jobs[ilJ]);
			}
		}		
		return *this;
	}
};

class FlIndDemandTableViewer : public CViewer
{
// Constructions
public:
    FlIndDemandTableViewer();
    ~FlIndDemandTableViewer();

    void Attach(CTable *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName, CString opDate);
    void ChangeViewTo(const char *pcpViewName);

	void SetStartTime(CTime opStartTime);
	void SetEndTime(CTime opEndTime);
	CString GetAssignmentStringForJob(JOBDATA *prpJob);

private:
	static void FlIndDemandTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void PrepareFilter();
	void PrepareSorter();

	BOOL IsPassFilter(const char *pcpName, const char *pcpCode, DEMANDDATA *prpDem);
	int  CompareDem(FLINDEM_LINE *prpDem1, FLINDEM_LINE *prpDem2);

	void MakeLines();
	int MakeLine(DEMANDDATA *prpGate);
	bool FormatLine(FLINDEM_LINE &ropLine, DEMANDDATA *prpDem);
	BOOL FindAssignment(long lpJobUrno, int &ripLineno);

	void DeleteAll();
	int  CreateLine(FLINDEM_LINE *prpLine);
	void DeleteLine(int ipLineno);

	void UpdateDisplay();
	CString Format(FLINDEM_LINE *prpLine);
	int imJobCount;
	int imMaxJobsPerLine;

// Attributes used for filtering condition
private:
    int				omGroupBy;              // enumerated value -- define in "flIndDemandviewer.cpp" (use first sorting)
    CWordArray		omSortOrder;
	CMapStringToPtr omCMapForName;
	CMapStringToPtr omCMapForCode;
	bool			bmUseAllNames;
	bool			bmUseAllCodes;
	int				imDemandTypes; // personnel/location/equipment
	bool	bmSortDemStartAscending;
	bool	bmSortDemEndAscending;
	bool    bmSortDemTerminalAscending;
	CString omTerminal; //Singapore

// Attributes
public:
    CTable *pomTable;
    CCSPtrArray<FLINDEM_LINE> omLines;
	CTime omStartTime;
	CTime omEndTime;
    CString omDate;

	FLINDEM_LINE*	GetLine(DEMANDDATA *prpDem);

	void ProcessDemandDelete(DEMANDDATA *prpDemand, bool bpDisplayTable = true);
	void ProcessDemandChange(DEMANDDATA *prpDemand);
	void ProcessJobChange(JOBDATA *prpJob);
	void ProcessJodChange(JODDATA *prpJod);
	DEMANDDATA *GetDemandForJob(long lpJobUrno);
	int FindDemandLine(long lpDemandUrno);
	
	CString GetFunctionForDemand(DEMANDDATA *prpDemand);
	CString GetPermitForDemand(DEMANDDATA *prpDemand);

	void SetTerminal(const CString& ropTerminal){omTerminal = ropTerminal;} //Singapore
	CString GetTerminal() const {return omTerminal;} //Singapore
	inline void GetLines(CCSPtrArray<FLINDEM_LINE>& opLines) const{opLines = omLines;} //Singapore
	CCSPtrArray<FLINDEM_LINE>& GetLines(){return omLines;} //Singapore

// Printing functions
private:
	BOOL PrintFidLine(FLINDEM_LINE *prpLine,BOOL bpIsLastLine);
	BOOL PrintFidHeader(CCSPrint *pomPrint);
	CCSPrint *pomPrint;
public:
	void PrintView();
};

#endif //__GTVIEWER_H__