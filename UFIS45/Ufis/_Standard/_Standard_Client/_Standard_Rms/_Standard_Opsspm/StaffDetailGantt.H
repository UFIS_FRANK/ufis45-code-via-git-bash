// StaffDetailGantt.h : header file
//

#ifndef _STAFFDG_H_
#define _STAFFDG_H_

#include <tscale.h>

// Special time value definition
#ifndef TIMENULL
#define TIMENULL    CTime((time_t)-1)
#endif

/////////////////////////////////////////////////////////////////////////////
// StaffDetailGantt window

class StaffDetailGantt: public CListBox
{
// Operations
public:
	StaffDetailGantt::StaffDetailGantt(StaffDetailViewer *popViewer = NULL,
        int ipVerticalScaleWidth = 0, int ipVerticalScaleIndent = 15,
        CFont *popVerticalScaleFont = NULL, CFont *popGanttChartFont = NULL,
        int ipGutterHeight = 2, int ipOverlapHeight = 4,
        COLORREF lpVerticalScaleTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightVerticalScaleTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW),
        COLORREF lpGanttChartTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightGanttChartTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW));
	~StaffDetailGantt();
    void SetViewer(StaffDetailViewer *popViewer);
    void SetVerticalScaleWidth(int ipWidth);
	void SetVerticalScaleIndent(int ipIndent);
    void SetFonts(CFont *popVerticalScaleFont, CFont *popGanttChartFont);
    void SetGutters(int ipGutterHeight, int ipOverlapHeight);
    void SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);
    void SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);

    int GetGanttChartHeight();
    int GetLineHeight(int ipMaxOverlapLevel);

    BOOL Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID = 0);
    void SetStatusBar(CStatusBar *popStatusBar);
	void SetTimeScale(CTimeScale *popTimeScale);
    void SetBorderPrecision(int ipBorderPrecision);

    // This group of function will automatically repaint the window
    void SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling = TRUE);
    void SetDisplayStart(CTime opDisplayStart);
    void SetCurrentTime(CTime opCurrentTime);
    void SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd);

    void RepaintVerticalScale(int ipLineno = -1, BOOL bpErase = TRUE);
    void RepaintGanttChart(int ipLineno = -1,
            CTime opStartTime = TIMENULL, CTime opEndTime = TIMENULL, BOOL bpErase = TRUE);
    void RepaintItemHeight(int ipLineno);

// Attributes
private:
    StaffDetailViewer *pomViewer;
    int imVerticalScaleWidth;
	int imVerticalScaleIndent;	// number of pixels used to indent text in VerticalScale
    CFont *pomVerticalScaleFont;
    CFont *pomGanttChartFont;
    int imBarHeight;            // calculated when SetGanttChartFont()
    int imGutterHeight;         // space between the GanttLine and the topmost-level bar
    int imLeadingHeight;        // space between the bar and the border
    int imOverlapHeight;        // space between the overlapped bar
    COLORREF lmVerticalScaleTextColor;
    COLORREF lmVerticalScaleBackgroundColor;
    COLORREF lmHighlightVerticalScaleTextColor;
    COLORREF lmHighlightVerticalScaleBackgroundColor;
    COLORREF lmGanttChartTextColor;
    COLORREF lmGanttChartBackgroundColor;
    COLORREF lmHighlightGanttChartTextColor;
    COLORREF lmHighlightGanttChartBackgroundColor;

    CTime omDisplayStart;
    CTime omDisplayEnd;
    int imWindowWidth;          // in pixels, for pixel/time ratio (updated by WM_SIZE)
    BOOL bmIsFixedScaling;
        // There are two mode for updating the display window when WM_SIZE was sent,
        // variabled-scaling and fixed-scaling. The mode of this operation will be
        // given in the method SetDisplayWindow().

    CTime omCurrentTime;                    // the red vertical time line
    CTime omMarkTimeStart, omMarkTimeEnd;   // two yellow vertical time lines

    CStatusBar *pomStatusBar;   // the status bar where the notification message goes
	CTimeScale *pomTimeScale;	// the time scale for top scale indicators display

    BOOL bmIsMouseInWindow;
    BOOL bmIsControlKeyDown;
    int imHighlightLine;        // the current line that painted with highlight color
    int imCurrentBar;           // the current bar that the user places mouse over

    UINT umResizeMode;          // HTNOWHERE, HTCAPTION (moving), HTLEFT, or HTRIGHT
    CPoint omPointResize;       // the point where moving/resizing begin
    CRect omRectResize;         // current focus rectangle for moving/resizing
    int imBorderPreLeft;        // define the sensitivity when user detect bar border
    int imBorderPreRight;       // define the sensitivity when user detect bar border
	STAFFDETAIL_BARDATA rmActiveBar;          // Bar which is currently moved or sized
	BOOL bmActiveBarSet;
	long lmFastLinkJobUrno;

	int imContextItem;
	int imContextBarNo;
	STAFFDETAIL_BARDATA rmContextBar;  // Save bar for Context Menu handling
	BOOL bmContextBarSet;


// Implementation
public:
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
    virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);

private:
    void DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem);
    void DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
    void DrawTimeLines(CDC *pDC, int top, int bottom);
	void DrawFocusRect(CDC *pDC, const CRect &rect);

protected:
    // Generated message map functions
    //{{AFX_MSG(StaffDetailGantt)
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMenuStaffConfirm();
    afx_msg void OnMenuStaffFinish();
    afx_msg void OnMenuStaffWorkOn();
    afx_msg void OnMenuStaffDelete();
    afx_msg void OnMenuStaffBreak();
	afx_msg void OnMenuBreakUnpayed();
    afx_msg void OnMenuStaffAcceptConflict(UINT);
    afx_msg void OnMenuStaffInform();
	afx_msg void OnMenuEditFastLink();
	afx_msg void OnMenuDeleteFastLink();
	afx_msg void OnMenuLastJobInfo(); //PRF 8998
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
	CMapPtrToPtr omAcceptConflictsMap;
    void UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown);
    void UpdateBarStatus(int ipLineno, int ipBarno, int ipFlBkBarno,  int ipTaBkBarno);
    BOOL BeginMovingOrResizing(CPoint point);
    BOOL OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown);
    int GetItemFromPoint(CPoint point);
    int GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1);
        // Precision is measured in pixel.
        // The purpose of the precision is for detecting more pixel when user want to resize.
        // The acceptable period of time is between [point.x - ipPreLeft, point.x + ipPreRight]
    UINT HitTest(int ipLineno, int ipBarno, CPoint point);
        // HitTest will use the precision defined in "imBorderPrecision"
	BOOL IsStairCase(const CString &ropGate);

//Pichate
protected:
	void DrawDotHorizontalLine(CDC *pDC);
	void DrawVerticalLine(CDC *pDC);
	void DrawBkBars(CDC *pDC);
	int GetBkBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1);
	int GetFastLinkBkBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1);
	int GetTempAbsenceBkBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1);
	CString GetTempAbsenceTextFromPoint(CPoint point, int ipPreLeft = 0, int ipPreRight = 1);
	void DrawFastLinkBkBars(CDC *pDC);
	void DrawTempAbsenceBkBars(CDC *pDC);
	void DrawPoolJobBars(CDC *pDC);

// drag-and-drop implementation
private:
	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;
	CPoint omDropPosition;
	void DragDutyBarBegin();
	LONG ProcessDropFlightBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropFlight(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropDemandBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropAloc(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect, CString opJobType, CString opAloc);
	LONG ProcessDropFIDTable(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropEquipment(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
};

/////////////////////////////////////////////////////////////////////////////

#endif
