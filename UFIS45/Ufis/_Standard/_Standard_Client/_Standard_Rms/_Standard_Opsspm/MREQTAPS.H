// RequestTablePropertySheet.h : header file
//
#ifndef _RequestTablePROPERTYSHEET_H_
#define _RequestTablePROPERTYSHEET_H_
/////////////////////////////////////////////////////////////////////////////
// RequestTablePropertySheet

class RequestTablePropertySheet : public BasePropertySheet
{
// Construction
public:
	RequestTablePropertySheet(CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:

	RequestTableSortPage m_pageSort;
//	ZeitPage m_pageZeitVald;
//	ZeitPage m_pageZeitCrat;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();
};

#endif // _RequestTablePROPERTYSHEET_H_
/////////////////////////////////////////////////////////////////////////////

