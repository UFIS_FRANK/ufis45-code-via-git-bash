#ifndef _CCSERROR_H_
#define _CCSERROR_H_

#include <ccsdefs.h>

class ostream;

//@Man:
//@See: Returns
//@Memo:Return code class
/*@Doc: Simple class for #int#-like error codes. Objects can be assigned,
  compared and printed.
*/
class CCSReturnCode
{
  public:

    //@ManDoc: constructor
    CCSReturnCode(int c = 0, const char *s = "Not set");

    //@ManDoc: #int# type conversion operator
    operator int() const;

    //@ManDoc: assignment operator
    CCSReturnCode& operator=(const CCSReturnCode& other);

    //@ManDoc: test on equality operator
    BOOL operator==(const CCSReturnCode& other) const;

    //@ManDoc: output to stream
    friend ostream& operator<<(ostream&, const CCSReturnCode&);

  private:

    const char *pcmName; // name (for debugging)
    int         imCode;  // integer code
};

//@Man:  Returns
//@Type: Predefined
//@See:  CCSReturnCode
//@{
//@ManMemo: indicates success, good, ok, ...
extern const CCSReturnCode RCSuccess;

//@ManMemo: indicates failure, bad, error, ...
extern const CCSReturnCode RCFailure;
//@}

#endif

