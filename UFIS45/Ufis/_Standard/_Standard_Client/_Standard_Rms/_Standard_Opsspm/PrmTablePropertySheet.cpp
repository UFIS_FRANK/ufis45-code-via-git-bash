// PRMTablePropertySheet.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CedaAltData.h>
#include <CedaSerData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <PrmTablePropertySheet.h>
#include <CedaFlightData.h>
#include <conflict.h>
#include <PrmTableViewer.h>
#include <GateViewer.h> //Singapore

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PRMTablePropertySheet
//
// lli: Use the correct string to PropertySheet (IDS_STRING61422)
PRMTablePropertySheet::PRMTablePropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_STRING61422), pParentWnd, popViewer, iSelectPage)
{

	AddPage(&m_pageSort);
	// lli: load PRM-AIRLINEFILTER setting from ceda.ini, default is no airline filter
	char pclTmpText[512];
	char pclConfigPath[512];
	char pclConfigCopy[512];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	//Terminal Tab
	GetPrivateProfileString(pcgAppName, "PRM_TERMINALFILTER",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (!strcmp(pclTmpText,"YES")) {
		AddPage(&m_pageTerminal);
		m_pageTerminal.SetCaption(GetString(IDS_STRING62531));
	
		GetPrivateProfileString(pcgAppName, "FLIGHT_TERMINALS", "",pclTmpText, sizeof (pclTmpText), pclConfigPath);
		strcpy(pclConfigCopy,pclTmpText);
		char *pclToken  = strtok(pclConfigCopy,",");
		while (pclToken)
		{
			if(strcmp(pclToken, " ") != 0)
			{
				m_pageTerminal.omPossibleItems.Add(pclToken);
			}
			pclToken = strtok(NULL,",");
		}
		m_pageTerminal.omPossibleItems.Add( GetString(IDS_STRINGWTFLIGHTS));
		m_pageTerminal.bmSelectAllEnabled = true;
		
	}  
	

	
	///////////////////////

	GetPrivateProfileString(pcgAppName, "PRM-AIRLINEFILTER",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (!strcmp(pclTmpText,"YES")) {
		AddPage(&m_pageAirline);
		m_pageAirline.SetCaption(GetString(IDS_STRING61597));
		
		// airline codes format ALC2/ALC3
		ogAltData.GetAlc2Alc3Strings(m_pageAirline.omPossibleItems);
		m_pageAirline.bmSelectAllEnabled = true;
	}
	
	// lli: load PRM-SERVICEFILTER setting from ceda.ini, default is no service filter
	GetPrivateProfileString(pcgAppName, "PRM-SERVICEFILTER",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (!strcmp(pclTmpText,"YES")) {
		AddPage(&m_pageService);
		m_pageService.SetCaption(GetString(IDS_SERVICECODES));
		
		ogSerData.GetAllServiceCodes(m_pageService.omPossibleItems, TRUE);
		ogSerData.GetAllServiceCodes(m_pageService.omPossibleItems, FALSE);
		m_pageService.bmSelectAllEnabled = true;
	}
	
	// lli: load PRM-PRMTFILTER setting from ceda.ini, default is no prmt filter
	GetPrivateProfileString(pcgAppName, "PRM-PRMTFILTER",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (!strcmp(pclTmpText,"YES")) {
		AddPage(&m_pagePrmt);
		m_pagePrmt.SetCaption(CString("PRM Types"));
		GetPrivateProfileString(pcgAppName, "PRM-TYPES", "OWHC",pclTmpText, sizeof (pclTmpText), pclConfigPath);
		strcpy(pclConfigCopy,pclTmpText);
		char *pclToken  = strtok(pclConfigCopy,",");
		while (pclToken)
		{
			m_pagePrmt.omPossibleItems.Add(pclToken);
			pclToken = strtok(NULL,",");
		}
		m_pagePrmt.bmSelectAllEnabled = true;
	}
	
	//added bu MAX
	GetPrivateProfileString(pcgAppName, "PRM-PAXLOCFILTER",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (!strcmp(pclTmpText,"YES")) {
		AddPage(&m_pageArrivalLocation);
		m_pageArrivalLocation.SetCaption(CString("Pax Loc"));
		GetPrivateProfileString(pcgAppName, "PRM-ARRIVAL-LOCATION", ",HALL,DESK,GATE,T1Lounge,T2Lounge,T3Lounge",pclTmpText, sizeof (pclTmpText), pclConfigPath);
		strcpy(pclConfigCopy,pclTmpText);
		char *pclToken  = strtok(pclConfigCopy,",");
		while (pclToken)
		{
			m_pageArrivalLocation.omPossibleItems.Add(pclToken);
			pclToken = strtok(NULL,",");
		}
		m_pageArrivalLocation.bmSelectAllEnabled = true;
	}

	// lli: load PRM-ADIDFILTER setting from ceda.ini, default is no ADID filter
	GetPrivateProfileString(pcgAppName, "PRM-ADIDFILTER",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (!strcmp(pclTmpText,"YES")) {
		AddPage(&m_pageAdid);
		m_pageAdid.SetCaption(CString("ADT"));
		m_pageAdid.omPossibleItems.Add(CString("A"));
		m_pageAdid.omPossibleItems.Add(CString("D"));
		m_pageAdid.omPossibleItems.Add(CString("T"));
		m_pageAdid.bmSelectAllEnabled = true;
	}
	
	// lli: load PRM-COLUMNFILTER setting from ceda.ini, default is no columns filter
	GetPrivateProfileString(pcgAppName, "PRM-COLUMNFILTER",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath); //2575
	if (!strcmp(pclTmpText,"YES")) {
		AddPage(&m_pageColumns);
		m_pageColumns.SetCaption(GetString(IDS_STRING61895));
	}

    //m_pageColumns.omPossibleItems.Add("");

	// get all possible columns
	CStringArray olColumns;                              
	PrmTableViewer::GetAllViewColumns(olColumns);
	

	for (int ilC = 0; ilC < olColumns.GetSize(); ilC++)       
	{
		m_pageColumns.omPossibleItems.Add(PrmTableViewer::GetViewColumnName(olColumns[ilC]));		
//		m_pageColumns.omPossibleItems.Add(olColumns[ilC]);		
	}
	
    // m_pageColumns.omPossibleItems.Add("Created by");

	m_pageColumns.SetSorted(FALSE);
}

void PRMTablePropertySheet::LoadDataFromViewer()
{
	pomViewer->GetSort(m_pageSort.omSortOrders);
	pomViewer->GetFilter("Airline",		m_pageAirline.omSelectedItems);
	pomViewer->GetFilter("Service",		m_pageService.omSelectedItems);
	pomViewer->GetFilter("Prmt",		m_pagePrmt.omSelectedItems);
	//added by MAX
	pomViewer->GetFilter("PrmAL",       m_pageArrivalLocation.omSelectedItems);
	
	pomViewer->GetFilter("Adid",		m_pageAdid.omSelectedItems);
	pomViewer->GetFilter("Terminal",	m_pageTerminal.omSelectedItems);


	CStringArray olColumns;
	pomViewer->GetFilter("Columns",olColumns);
	m_pageColumns.omSelectedItems.RemoveAll();
	for (int ilC = 0; ilC < olColumns.GetSize(); ilC++)
	{
		m_pageColumns.omSelectedItems.Add(PrmTableViewer::GetViewColumnName(olColumns[ilC]));		
//		m_pageColumns.omSelectedItems.Add(olColumns[ilC]);		
	}
}

void PRMTablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{

	pomViewer->SetSort(m_pageSort.omSortOrders);
	pomViewer->SetFilter("Airline",		m_pageAirline.omSelectedItems);
	pomViewer->SetFilter("Service",		m_pageService.omSelectedItems);
	pomViewer->SetFilter("Prmt",		m_pagePrmt.omSelectedItems);
	//added by MAX
	pomViewer->SetFilter("PrmAL",       m_pageArrivalLocation.omSelectedItems);

	pomViewer->SetFilter("Adid",		m_pageAdid.omSelectedItems);
	pomViewer->SetFilter("Terminal",	m_pageTerminal.omSelectedItems);

	CStringArray olColumns;
	for (int ilC = 0; ilC < m_pageColumns.omSelectedItems.GetSize(); ilC++)
	{
		olColumns.Add(PrmTableViewer::GetViewColumnField(m_pageColumns.omSelectedItems[ilC]));		
//		olColumns.Add(m_pageColumns.omSelectedItems[ilC]);		
	}
	pomViewer->SetFilter("Columns",olColumns);

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int PRMTablePropertySheet::QueryForDiscardChanges()
{
	CStringArray olSortOrders;
	CStringArray olSelectedAirlines;
	CStringArray olSelectedServices;
	CStringArray olSelectedPrmts;
	//added by MAX
	CStringArray olSelectedPrmLAs;

	CStringArray olSelectedAdids;
	CStringArray olSelectedColumns;
	CStringArray olSelectedTerminals;

	pomViewer->GetSort(olSortOrders);
	pomViewer->GetFilter("Airline",		olSelectedAirlines);
	pomViewer->GetFilter("Service",		olSelectedServices);
	pomViewer->GetFilter("Prmt",		olSelectedPrmts);
	//added by MAX
	pomViewer->GetFilter("PrmAL",		olSelectedPrmLAs);
	
	pomViewer->GetFilter("Adid",		olSelectedAdids);
	pomViewer->GetFilter("Columns",		olSelectedColumns);
	pomViewer->GetFilter("Terminal",	olSelectedTerminals);


	if (!IsIdentical(olSortOrders, m_pageSort.omSortOrders)
		||!IsIdentical(olSelectedAirlines,	m_pageAirline.omSelectedItems)
		||!IsIdentical(olSelectedServices, m_pageService.omSelectedItems)
		||!IsIdentical(olSelectedPrmts, m_pagePrmt.omSelectedItems)
		||!IsIdentical(olSelectedPrmLAs, m_pageArrivalLocation.omSelectedItems)
		||!IsIdentical(olSelectedAdids, m_pageAdid.omSelectedItems)
		||!IsIdentical(olSelectedColumns, m_pageColumns.omSelectedItems) 
		||!IsIdentical(olSelectedTerminals, m_pageTerminal.omSelectedItems)
		)
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}

BEGIN_MESSAGE_MAP(PRMTablePropertySheet, BasePropertySheet)
    //{{AFX_MSG_MAP(PRMTablePropertySheet)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PRMTablePropertySheet message handlers

LONG PRMTablePropertySheet::OnPrmDiagramGroupPageChanged(UINT wParam, LONG lParam)
{
	UpdateEnableFlagInFilterPages();
	return 0L;
}

/////////////////////////////////////////////////////////////////////////////
// PRMTablePropertySheet -- help routines

void PRMTablePropertySheet::UpdateEnableFlagInFilterPages()
{
}
