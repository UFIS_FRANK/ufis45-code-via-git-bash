// PstViewer.h : header file
//

#ifndef _PSTVIEWER_H_
#define _PSTVIEWER_H_

#include <cviewer.h>
#include <ccsprint.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>


struct PST_INDICATORDATA
{
	long DemandUrno;
	long JobUrno;
	// standard data for indicators in general GanttBar
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
	BOOL IsArrival;
};
struct PST_BARDATA 
{
	int  Type;
	long FlightUrno;		// from FLTFRA.URNO
	int ReturnFlightType;
	long DepartureUrno;
	int DepReturnFlightType;
	bool IsFinished;
	CString StatusText;		// long text used to be displayed at the status bar
    CString Text;
    CTime StartTime;
    CTime EndTime;
	CTimeSpan Delay;
	int IsDelayed;
    int FrameType;
	COLORREF FrameColor;
    int MarkerType;
    CBrush *MarkerBrush;
	BOOL IsShadowBar;
	BOOL IsTmo;
	BOOL NoDropOnThisBar;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	CCSPtrArray<PST_INDICATORDATA> Indicators;
	int FlightType;

	PST_BARDATA(void)
	{
		Type			= -1;
		FlightUrno		= -1;
		DepartureUrno	= -1;
		IsDelayed		= -1;
		FrameType		= -1;
		FrameColor = BLACK;
		MarkerType		= -1;
		MarkerBrush		= NULL;
		IsShadowBar		= FALSE;
		IsTmo			= FALSE;
		NoDropOnThisBar	= FALSE;
		OverlapLevel	= 0;
		ReturnFlightType = ID_NOT_RETURNFLIGHT;
		DepReturnFlightType = ID_NOT_RETURNFLIGHT;
		IsFinished = false;
	}
};
struct PST_LINEDATA 
{
	// standard data for lines in general Diagram's chart
    CString Text;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
	BOOL IsStairCasePst;
    CCSPtrArray<PST_BARDATA> Bars;		// in order of painting (leftmost is the bottommost)
    CCSPtrArray<int> TimeOrder;			// maintain overlapped bars, see below
};
struct PST_MANAGERDATA
{
	long JobUrno;			// from JOBCKI.URNO (of the job for FM/AM)
	// these data are for displaying the TopScaleText for each group in PstDiagram
	CString ShiftStid;
	CString ShiftTmid;
	CString EmpLnam;
};
struct PST_GROUPDATA 
{
	CString PstAreaId;			// Pst area's ID associated with this group
    CCSPtrArray<PST_MANAGERDATA> Managers;
	// standard data for groups in general Diagram
    CString Text;
    CCSPtrArray<PST_LINEDATA> Lines;
};

struct PST_SELECTION
{
	int imGroupno;
	int imLineno;
	int imBarno;

	PST_SELECTION(void)
	{
		imGroupno = -1;
		imLineno  = -1;
		imBarno	  = -1;
	}
};

enum enumPstBarType {BAR_PSTFLIGHT, BAR_PSTSPECIAL};
enum enumPstFlightType {PSTBAR_ARRIVAL, PSTBAR_DEPARTURE, PSTBAR_TURNAROUND};

// Attention:
// Element "TimeOrder" in the struct "PST_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array PST_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the PST_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array PST_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// PstDiagramViewer window

class PstGantt;

class PstDiagramViewer: public CViewer
{
	friend PstGantt;

// Constructions
public:
    PstDiagramViewer();
    ~PstDiagramViewer();

	void SetAdditionalPosForWithoutPosGrpMap();
	void Attach(CWnd *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName,
		BOOL bpIncludeArrivalFlights, BOOL bpIncludeDepartureFlights,
		CTime opStartTime, CTime opEndTime);
	void ChangeViewTo(void);
	void UpdateManagers(CTime opStartTime, CTime opEndTime);
	int imFlightsWithoutPstsGroup;
	void LoadDelegatedFlightUrnos();
	void AddDelegatedFlightUrno(JOBDATA *prpJob);
	void RemoveDelegatedFlightUrno(JOBDATA *prpJob);
// Internal data processing routines
private:
	static void PstDiagramCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void PrepareFilter();
	BOOL IsPassFilter(const char *pcpPstArea);
	BOOL IsPassFilter(const char *pcpPstArea,const char *pcpAlcd,const char *pcpHara, const char *pcpKeycode, const char *pcpNature, const char *pcpAct3, const char *pcpAct5, const long lpFlightUrno);
	int CompareGroup(PST_GROUPDATA *prpGroup1, PST_GROUPDATA *prpGroup2);
	int CompareLine(PST_LINEDATA *prpLine1, PST_LINEDATA *prpLine2);
	int CompareManager(PST_MANAGERDATA *prpManager1, PST_MANAGERDATA *prpManager2);

	void MakeGroupsAndLines();
	void MakeManagers();
	void MakeBars();
	void MakeShadowBar(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight);
	void MakeShadowBarWithoutDemand(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight);
	void MakeManager(int ipGroupno, JOBDATA *prpJob);
	void MakeBar(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight, bool bpIgnorePosition= false, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	void MakeBarWithoutDemand(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	CString GetBarText(FLIGHTDATA *prpFlight, bool bpIsArrival, FLIGHTDATA *prpRotation = NULL);
	CString GetStatusText(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotation);
	CString GetGateText(FLIGHTDATA *prpFlight);
	CString GetPositionText(FLIGHTDATA *prpFlight);
	void MakeSpecialBar(int ipGroupno, int ipLineno, JOBDATA *prpJob);
	void MakeIndicators(int ipGroupno, int ipLineno, int ipBarno, 
		CCSPtrArray<DEMANDDATA> &ropArrivalDemands,
		CCSPtrArray<DEMANDDATA> &ropDepartureDemands,
		FLIGHTDATA *prpArrivalFlight,FLIGHTDATA *prpDepartureFlight);
	int GetReturnFlightTypes(FLIGHTDATA *prpFlight, CUIntArray &ropReturnFlightTypes, CString opAlid = "");

	int GetDemands(long lpUrno, const char *pcpAlid, CCSPtrArray<DEMANDDATA> &ropDemands,
		CTime &rolStartTime, CTime &rolEndTime);

	CString ArrivalFlightStatus(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotationFlight);
	CString DepartureFlightStatus(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotationFlight);
	CString TurnAroundFlightStatus(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotationFlight);

	BOOL FindGroup(const char *pcpPstAreaId, int &ripGroupno);
	BOOL FindGroupAndLine(const char *pcpPstId, int &ripGroupno, int &ripLineno);
	BOOL FindManager(long lpUrno, int &ripGroupno, int &ripManagerno);
	BOOL FindFlightBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno, bool bpFindRotation = false, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	BOOL FindLine(const char *pcpPstId, int ipGroupno, int &ripLineno);

// Operations
public:
	void CheckForFinishedFlights();
    int GetGroupCount();
    PST_GROUPDATA *GetGroup(int ipGroupno);
    CString GetGroupText(int ipGroupno);
    CString GetGroupTopScaleText(int ipGroupno);
	int GetManagerCount(int ipGroupno);
    PST_MANAGERDATA *GetManager(int ipGroupno, int ipManagerno);
    int GetLineCount(int ipGroupno);
    PST_LINEDATA *GetLine(int ipGroupno, int ipLineno);
    CString GetLineText(int ipGroupno, int ipLineno);
    int GetMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetBarCount(int ipGroupno, int ipLineno);
    PST_BARDATA *GetBar(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBarText(int ipGroupno, int ipLineno, int ipBarno);
	int GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno);
	PST_INDICATORDATA *GetIndicator(int ipGroupno, int ipLineno, int ipBarno,int ipIndicatorno);
	CString GetTmo(int ipGroupno, int ipLineno);
	int GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd);
	void AllowUpdates(BOOL bpNoUpdatesNow);
	BOOL bmNoUpdatesNow;
    void DeleteAll();
    int CreateGroup(PST_GROUPDATA *prpGroup);
    void DeleteGroup(int ipGroupno);
	CCSReturnCode CreateManagersForGroup(int ipGroupNo);
	int CreateManager(int ipGroupno, PST_MANAGERDATA *prpManager);
	void DeleteManager(int ipGroupno, int ipManagerno);
    int CreateLine(int ipGroupno, PST_LINEDATA *prpLine);
    void DeleteLine(int ipGroupno, int ipLineno);
    int CreateBar(int ipGroupno, int ipLineno, PST_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
	void DeleteBarsForFlight(long lpFlightUrno);
    void DeleteBar(int ipGroupno, int ipLineno, int ipBarno);
    int CreateIndicator(int ipGroupno, int ipLineno, int ipBarno,
    	PST_INDICATORDATA *prpIndicator);
	void DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);

    int GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
        int ipOverlapLevel1, int ipOverlapLevel2);

    CMapStringToPtr* GetMapForFunctions() const; //PRF 8999
	CMapStringToPtr* GetMapForEquDemandGroups() const;

	COLORREF GetColourForDepFlight(long lpFlightUrno);
	COLORREF GetColourForArrFlight(long lpFlightUrno);
	bool DisplayMarker(CString opMarkerType);
	bool bmDisplayJobConflicts;
	bool bmOneFlightPerLine;

	int CreateLinesForFlightsWithoutPsts(FLIGHTDATA *prpSingleFlight = NULL);

	int imAllFlightsGroup;
	int CreateLinesForAllFlights(FLIGHTDATA *prpSingleFlight = NULL);
	int SortLineOfGroupAllFlights(int ipLineNo, long lpFlightUrno = 0L);
	int GetAllFlightsGroup();
	bool IsAllFlightsGroup(int ipGroupno);

	int CreateGeneralGroupLines(CCSPtrArray <FLIGHTDATA> &ropFlights, int ipGroupno);

	CWnd *pomAttachWnd;

// Private helper routines
private:
	void GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
		CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);

// Attributes used for filtering condition
private:
	CMapStringToPtr omAdditionalPosForWithoutPosGrpMap;
	CMapStringToPtr omCMapForDemandType;
	CMapStringToPtr omCMapForPstArea;
	CMapStringToPtr	omCMapForAlcd;
	CMapStringToPtr	omCMapForAgents;
	CMapStringToPtr	omCMapForKeycodes;
	CMapStringToPtr	omCMapForNatures;
	CMapStringToPtr	omCMapForAct3;
	CMapStringToPtr	omCMapForAct5;
	CMapStringToPtr	omCMapForFunctions; //PRF 8999
	CMapStringToPtr omCMapForEquDemandGroups;
	BOOL			bmUseAllAgents, bmUseAllPstAreas, bmUseAllAlcds, bmUseAllKeycodes, bmUseAllNatures, bmUseAllActs, bmUseAllFunctions, bmUseAllEquDemandGroups;
	bool			bmNoAct;
	bool bmEmptyAirlineSelected; //Singapore

	BOOL			bmIncludeArrivalFlights;
	BOOL			bmIncludeDepartureFlights;
	CTime			omStartTime;
	CTime			omEndTime;
	CMapPtrToPtr omDelegatedFlights;

public:
	int				imDemandTypes; // personnel/location/equipment

// Attributes
private:
	CCSPtrArray<PST_GROUPDATA> omGroups;

// Methods which handle changes (from Data Distributor)
private:
	void ProcessDemandChange(DEMANDDATA *prpDemand);
	void ProcessJobChange(JOBDATA *prpJob);
	void ProcessFlightDelete(long lpFlightUrno);
	void ProcessFlightChange(FLIGHTDATA *prpFlight, bool bpCheckRotation = true);
	void ProcessFlightSelect(FLIGHTDATA *prpFlight);
	void ProcessSpecialJobNew(JOBDATA * prpJob);
	void ProcessSpecialJobChange(JOBDATA * prpJob);
	void ProcessSpecialJobDelete(JOBDATA * prpJob);
	void ProcessFmgJobNew(JOBDATA *prpJob);
	void ProcessFmgJobDelete(JOBDATA *prpJob);
	// printing
public:
	void PrintDiagram(CPtrArray &opPtrArray);
	void PrintFm(CPtrArray &opPtrArray);
	void GetDemandList(CUIntArray  &ropDemandList);
	bool IsGroupWithoutPositions(int ipGroupno);
	int GetGroupWithoutPositions();
	int SortLineOfGroupWithoutPositions(int ipLineNo, long lpFlightUrno = 0L);

private:
	void PrintPstDiagramHeader(int ipGroupno);
	CCSPrint *pomPrint;
	int	igFirstGroupOnPage;
	void PrintPstArea(CPtrArray &opPtrArray,int ipGroupNo);
	void PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine);
	void PrintManagers(int ipGroupno,int ipYOffset1,int ipYOffset2);

	void PrintFmHeader(int ipGroupNo);
	void PrintOneFm(SHIFTDATA *prpShift,EMPDATA *prpEmp,JOBDATA *prpJob,BOOL bpIsFirstLine);
	void PrintFmForPstArea(CPtrArray &opPtrArray,int ipGroupNo);
	CTime GetFlightArr(FLIGHTDATA *prpFlight, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	CTime GetFlightDep(FLIGHTDATA *prpFlight, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	void CheckBarDuration(PST_BARDATA &ropBar);
	BOOL CheckDemandValidity(const DEMANDDATA*& ropDemandData) const; //PRF 8999
};

/////////////////////////////////////////////////////////////////////////////

#endif
