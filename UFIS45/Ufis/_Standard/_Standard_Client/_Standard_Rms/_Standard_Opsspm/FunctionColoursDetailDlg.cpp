// FunctionColoursDetailDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <FunctionColoursDetailDlg.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFunctionColoursDetailDlg dialog


CFunctionColoursDetailDlg::CFunctionColoursDetailDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFunctionColoursDetailDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFunctionColoursDetailDlg)
	m_ColourTitle = _T("");
	m_FctnTitle = _T("");
	m_Enabled = FALSE;
	m_FctcTitle = _T("");
	m_Fctn = _T("");
	m_Fctc = _T("");
	//}}AFX_DATA_INIT
}


void CFunctionColoursDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFunctionColoursDetailDlg)
	DDX_Text(pDX, IDC_COLOURTITLE, m_ColourTitle);
	DDX_Text(pDX, IDC_FCTNTITLE, m_FctnTitle);
	DDX_Check(pDX, IDC_STATUS, m_Enabled);
	DDX_Text(pDX, IDC_FCTCTITLE, m_FctcTitle);
	DDX_Text(pDX, IDC_FCTN, m_Fctn);
	DDX_Text(pDX, IDC_FCTC, m_Fctc);
	DDX_Control(pDX, IDC_COLOUR, omColourButton);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFunctionColoursDetailDlg, CDialog)
	//{{AFX_MSG_MAP(CFunctionColoursDetailDlg)
	ON_BN_CLICKED(IDC_COLOUR, OnColour)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFunctionColoursDetailDlg message handlers

BOOL CFunctionColoursDetailDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText(GetString(IDS_FUNCCOLOURSDETAILDLG));
	m_ColourTitle = GetString(IDS_FUNCCOLOURS_COLOURTITLE);
	m_FctcTitle = GetString(IDS_FUNCCOLOURS_CODE);
	GetDlgItem(IDC_STATUS)->SetWindowText(GetString(IDS_FUNCCOLOURS_STATUS));
	m_FctnTitle = GetString(IDS_FUNCCOLOURS_NAME);
	omColourButton.SetColors(omColour,omColour,omColour);
	
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFunctionColoursDetailDlg::SetData(CString opFctc, CString opFctn, bool bpEnabled, COLORREF opColour)
{
	m_Fctc = opFctc;
	m_Fctn = opFctn;
	m_Enabled = bpEnabled ? TRUE : FALSE;
	omColour = opColour;
}

void CFunctionColoursDetailDlg::GetData(CString &ropFctc, CString &ropFctn, bool &rbpEnabled, COLORREF &ropColour)
{
	ropFctc = m_Fctc;
	ropFctn = m_Fctn;
	rbpEnabled = m_Enabled ? true : false;
	ropColour = omColour;
}

void CFunctionColoursDetailDlg::OnOK() 
{
	if(UpdateData(TRUE))
	{
		CDialog::OnOK();
	}
}

void CFunctionColoursDetailDlg::OnColour() 
{
	CColorDialog olColorDlg(omColour);
	if (olColorDlg.DoModal() == IDOK)
	{
		omColour = olColorDlg.GetColor();
		omColourButton.SetColors(omColour,omColour,omColour);
		omColourButton.Invalidate(TRUE);
	}
}
