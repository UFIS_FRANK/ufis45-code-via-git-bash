// stviewer.cpp : implementation file
//
// Modification History:
// 19-Sep-96	Damkerng	Change many places since our database is changed.
//							In this module, the old implementation will use the field
//							PRID to keep text for a special job and an illness job.
//							Now, these has been moved to the field TEXT.
//							Check these places out by searching for "Id 19-Sep-96"

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <cviewer.h>
#include <CedaShiftData.h>
#include <CedaShiftTypeData.h>
#include <CedaJobData.h>
#include <CedaWgpData.h>
#include <CedaJtyData.h>
#include <CedaFlightData.h>
#include <CedaDemandData.h>
#include <CedaEmpData.h>
#include <CedaDraData.h>
#include <CedaOdaData.h>
#include <CedaShiftTypeData.h> //Singapore
#include <conflict.h>
#include <gantt.h>
#include <gbar.h>
#include <ccsddx.h>
#include <AllocData.h>
#include <StaffViewer.h>
#include <CCSCedaCom.h>
#include <BasicData.h>
#include <DataSet.h>
#include <CedaDlgData.h>
#include <CedaDrsData.h> // double tour data
#include <CedaEquData.h>
#include <CedaPolData.h>
#include <CedaDelData.h>
#include <DlgSettings.h>
#include <FieldConfigDlg.h>
#include <CPrintPreviewView.h> //PRF 8704
#include <CedaDpxData.h>
#include <CedaPdaData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern CCSCedaCom ogCommHandler;
const CString StaffDiagramViewer::Terminal2 = "T2";
const CString StaffDiagramViewer::Terminal3 = "T3";

// General macros for testing a point against an interval
#define IsBetween(val, Start, end)  ((Start) <= (val) && (val) <= (end))
#define IsOverlapped(Start1, end1, Start2, end2)    ((Start1) <= (end2) && (Start2) <= (end1))
#define IsReallyOverlapped(Start1, end1, Start2, end2)	((Start1) < (end2) && (Start2) < (end1))
#define IsWithIn(Start1, end1, Start2, end2)	((Start1) >= (Start2) && (end1) <= (end2))

#define INCH 0.284
#define MMY(x)    ((int)(MulDiv((x),pomPrint->pomCdc->GetDeviceCaps(LOGPIXELSY), 72)*INCH))


static int ByType(const STAFF_BKBARDATA **pppBkBar1, const STAFF_BKBARDATA **pppBkBar2);
static int ByType(const STAFF_BKBARDATA **pppBkBar1, const STAFF_BKBARDATA **pppBkBar2)
{
   int ilWeight1 = ((**pppBkBar1).Type == JOBPOOL) ? 1 : (((**pppBkBar1).Type == JOBBREAK) ? 2 : 3);
   int ilWeight2 = ((**pppBkBar2).Type == JOBPOOL) ? 1 : (((**pppBkBar2).Type == JOBBREAK) ? 2 : 3);
   return (int) ilWeight1 - ilWeight2;
}


static int ByNewLineNumber(const POOLJOBLINE **pppPoolJobLine1, const POOLJOBLINE **pppPoolJobLine2);
static int ByNewLineNumber(const POOLJOBLINE **pppPoolJobLine1, const POOLJOBLINE **pppPoolJobLine2)
{
	return (*pppPoolJobLine1)->NewLineno - (*pppPoolJobLine2)->NewLineno;
}

static int ByLineNumberReverse(const POOLJOBLINE **pppPoolJobLine1, const POOLJOBLINE **pppPoolJobLine2);
static int ByLineNumberReverse(const POOLJOBLINE **pppPoolJobLine1, const POOLJOBLINE **pppPoolJobLine2)
{
	return (*pppPoolJobLine2)->Lineno - (*pppPoolJobLine1)->Lineno;
}

static int ByJobStartTime(const JOBDATA **pppJob1, const JOBDATA **pppJob2);
static int ByJobStartTime(const JOBDATA **pppJob1, const JOBDATA **pppJob2)
{
	return (*pppJob1)->Acfr.GetTime() - (*pppJob2)->Acfr.GetTime();
}

static int BySortString(const STAFF_LINEDATA **pppLine1, const STAFF_LINEDATA **pppLine2);
static int BySortString(const STAFF_LINEDATA **pppLine1, const STAFF_LINEDATA **pppLine2)
{
	return strcmp((*pppLine1)->SortString, (*pppLine2)->SortString);
}


#define GROUP_BY_DEFAULT	GROUP_BY_POOL	// define a bullet-proof grouping symbol

// To know which rank is higher in comparison of two staff, I provide a colon-separated
// string from the lowest rank to the highest rank. By searching for a string ":XX:"
// in "pclRanks", if XX is the given rank, we could easily see which rank is higher.
// By using the same technic, we could see if the given rank is a manager by searching
// for a string ":XX:" in "ogBasicData.omManager".
//

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramViewer
//
StaffDiagramViewer::StaffDiagramViewer()
{
	bmDisableMessaging = false;
	bmUseAllShiftCodes = false;
	bmUseAllTeams = false;
	bmUseAllPools = false;
	bmUseAllRanks = false;
	bmUseAllPermits = false;
	bmTeamView = false;
	bmHideAbsentEmps = false;
	bmHideFidEmps = false;
	bmHideStandbyEmps = false;
	bmHideCompletelyDelegatedTeams = false;
	pomAttachWnd = NULL;
	omTempAbsenceBrush.CreateSolidBrush(SILVER);
	omBkTextBrush.CreateSolidBrush(WHITE);

	omBkBrush.CreateSolidBrush(RGB(160, 160, 160));
	omBkPausenlageBrush.CreateSolidBrush(RGB(180, 180, 180));

	omConfirmedBkBrush.CreateSolidBrush(RGB(0,0,160));
	omConfirmedBkPausenlageBrush.CreateSolidBrush(RGB(0,95,200));

	omFinishedBkBrush.CreateSolidBrush(RGB(90,90,90));
	omFinishedBkPausenlageBrush.CreateSolidBrush(RGB(120,120,120));

	omAbsenceBkBrush.CreateSolidBrush(RGB(0,213,213));
	omAbsenceBkPausenlageBrush.CreateSolidBrush(RGB(0,255,255));

	omDelegationBrush.CreateSolidBrush(RGB(4,4,255));
	omWIFBkBrush.CreateSolidBrush(OLIVE);
	
	ogCCSDdx.Register(this, JOB_NEW,CString("STVIEWER"), CString("Job New"), StaffDiagramCf);
	ogCCSDdx.Register(this, PRM_JOB_CHANGE,CString("STVIEWER"), CString("PRM Job Changed"), StaffDiagramCf);
	ogCCSDdx.Register(this, JOB_CHANGE,CString("STVIEWER"), CString("Job Changed"), StaffDiagramCf);
	ogCCSDdx.Register(this, JOB_DELETE,CString("STVIEWER"), CString("Job Deleted"), StaffDiagramCf);
	ogCCSDdx.Register(this, JOD_CHANGE, CString("STVIEWER"),CString("Jod Changed"), StaffDiagramCf);
	ogCCSDdx.Register(this, JOD_DELETE, CString("STVIEWER"),CString("Jod Changed"), StaffDiagramCf);
	ogCCSDdx.Register(this, JOD_NEW, CString("STVIEWER"),CString("'New Jod"), StaffDiagramCf);
	ogCCSDdx.Register(this, SHIFT_CHANGE,CString("STVIEWER"), CString("Shift Changed"), StaffDiagramCf);
	ogCCSDdx.Register(this, UPDATE_CONFLICT_SETUP,CString("STVIEWER"), CString("Update Conflict Setup"), StaffDiagramCf);
	ogCCSDdx.Register(this, SHIFT_SELECT,CString("STVIEWER"), CString("Shift Selected"), StaffDiagramCf);
	ogCCSDdx.Register(this, DRS_CHANGE,CString("STVIEWER"), CString("Drs Changed"), StaffDiagramCf);
	ogCCSDdx.Register(this, DRS_DELETE,CString("STVIEWER"), CString("Drs Deleted"), StaffDiagramCf);
//	ogCCSDdx.Register(this, SWG_INSERT,CString("STVIEWER"), CString("Swg New"), StaffDiagramCf);
//	ogCCSDdx.Register(this, SWG_UPDATE,CString("STVIEWER"), CString("Swg Changed"), StaffDiagramCf);
//	ogCCSDdx.Register(this, SWG_DELETE,CString("STVIEWER"), CString("Swg Deleted"), StaffDiagramCf);
//	ogCCSDdx.Register(this, DRG_INSERT,CString("STVIEWER"), CString("Drg New"), StaffDiagramCf);
//	ogCCSDdx.Register(this, DRG_UPDATE,CString("STVIEWER"), CString("Drg Changed"), StaffDiagramCf);
//	ogCCSDdx.Register(this, DRG_DELETE,CString("STVIEWER"), CString("Drg Deleted"), StaffDiagramCf);
	ogCCSDdx.Register(this, PDA_CHANGE, CString("STVIEWER"),CString("PDA Changed"), StaffDiagramCf);

	bool blShowCornerMarker = IsPrivateProfileOn("SHOW_STAFF_MARKER_FILTER","NO");
	if (blShowCornerMarker == TRUE)
	{
		ogCCSDdx.Register(this, FLIGHT_CHANGE,CString("STVIEWER"), CString("Flight Change"), StaffDiagramCf);
	}



	bmTeamLeaderColourDefined = false;
	imTeamLeaderColour = BLUE;
	pomPrint = NULL; //PRF 8704
}

StaffDiagramViewer::~StaffDiagramViewer()
{
    ogCCSDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	DeleteAllTeams();
}

void StaffDiagramViewer::Attach(CWnd *popAttachWnd)
{
    pomAttachWnd = popAttachWnd;
}

void StaffDiagramViewer::StaffDiagramCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    StaffDiagramViewer *polViewer = (StaffDiagramViewer *)popInstance;

	if (polViewer->bmNoUpdatesNow == FALSE)
	{
		//ogBasicData.Trace("StaffDiagramViewer Start DDX ===========================================================\n");
		if (ipDDXType == JOB_NEW)
		{
			polViewer->ProcessJobChange((JOBDATA *)vpDataPointer);
		}
		else if (ipDDXType == JOB_CHANGE)
		{
			polViewer->ProcessJobChange((JOBDATA *)vpDataPointer);
		}
		else if (ipDDXType == PRM_JOB_CHANGE)
		{
			polViewer->ProcessJobChange((JOBDATA *)vpDataPointer);
		}
		else if (ipDDXType == JOB_DELETE)
		{
			polViewer->SelectedJobDeleted((JOBDATA *)vpDataPointer);
			polViewer->ProcessJobDelete((JOBDATA *)vpDataPointer);
		}
		else if (ipDDXType == JOD_NEW || ipDDXType == JOD_CHANGE || ipDDXType == JOD_DELETE)
		{
			polViewer->ProcessJodChange((JODDATA *)vpDataPointer);
		}
		else if (ipDDXType == SHIFT_CHANGE)
		{
			polViewer->ProcessShiftChange((SHIFTDATA *)vpDataPointer);
		}
		else if (ipDDXType == PDA_CHANGE)
		{
			polViewer->ProcessPdaChange((PDADATA *)vpDataPointer);
		}
		else if (ipDDXType == REDISPLAY_ALL || ipDDXType == UPDATE_CONFLICT_SETUP)
		{
			polViewer->ChangeViewTo(polViewer->SelectView(),polViewer->omStartTime,polViewer->omEndTime);
			polViewer->pomAttachWnd->Invalidate();
		}
		else if (ipDDXType == SHIFT_SELECT)
		{
			polViewer->ProcessShiftSelect((SHIFTDATA *)vpDataPointer);
		}
		else if (ipDDXType == DRS_CHANGE)
		{
			polViewer->ProcessDrsChange((DRSDATA *)vpDataPointer);
		}
		else if (ipDDXType == DRS_DELETE)
		{
			polViewer->ProcessDrsDelete((long)vpDataPointer);
		}
		else if (ipDDXType == SWG_INSERT || ipDDXType == SWG_UPDATE || ipDDXType == SWG_DELETE)
		{
			polViewer->ProcessSwgChange((SWGDATA *)vpDataPointer);
		}
		else if (ipDDXType == DRG_INSERT || ipDDXType == DRG_CHANGE || ipDDXType == DRG_DELETE)
		{
			polViewer->ProcessDrgChange((DRGDATA *)vpDataPointer);
		}
		else if( ipDDXType == FLIGHT_CHANGE)
		{
			polViewer->ProcessRefresh();
		}
		//ogBasicData.Trace("StaffDiagramViewer End DDX ===========================================================\n");
	}
}


void StaffDiagramViewer::ProcessRefresh(void)
{
	if(MessagingActivated())
	{
		pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINEHEIGHT, 0);
	}
}


void StaffDiagramViewer::ProcessJodChange(JODDATA *prpJod)
{
	JOBDATA *prlJob = NULL;
	if(prpJod != NULL && (prlJob = ogJobData.GetJobByUrno(prpJod->Ujob)) != NULL)
	{
		ProcessJobChange(prlJob);
	}
}

void StaffDiagramViewer::ProcessSwgChange(SWGDATA *prpSwg)
{
	SHIFTDATA *prlShift = NULL;
	if(prpSwg != NULL)
	{
		CCSPtrArray <SHIFTDATA> olShifts;
		int ilNumShifts = ogShiftData.GetShiftsByUstf(prpSwg->Surn, olShifts);
		for(int ilS = 0; ilS < ilNumShifts; ilS++)
		{
			ProcessShiftChange(&olShifts[ilS]);
		}
	}
}

void StaffDiagramViewer::ProcessDrgChange(DRGDATA *prpDrg)
{
	SHIFTDATA *prlShift = NULL;
	if(prpDrg != NULL)
	{
		CCSPtrArray <SHIFTDATA> olShifts;
		int ilNumShifts = ogShiftData.GetShiftsByUstf(prpDrg->Stfu, olShifts);
		for(int ilS = 0; ilS < ilNumShifts; ilS++)
		{
			ProcessShiftChange(&olShifts[ilS]);
		}
	}
}

void StaffDiagramViewer::ProcessDrsChange(DRSDATA *prpDrs)
{
	if (prpDrs == NULL)
		return;

	SHIFTDATA *prlShift = NULL;
	prlShift = ogShiftData.GetShiftByUrno(prpDrs->Drru);
	if(prlShift != NULL)
	{
		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlShift->Peno);
		if (prlEmp != NULL)
		{
			// get pool jobs for this shift and update the pool jobs
			CCSPtrArray <JOBDATA> olPoolJobs;
			ogJobData.GetJobsByShur(olPoolJobs,prlShift->Urno,true);
			int ilNumPoolJobs = olPoolJobs.GetSize();
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				JOBDATA *prlJob = &olPoolJobs[ilPoolJob];
				if (!prlJob)
					continue;

				CStringArray olPermits;
				CString olFunction = ogBasicData.GetFunctionForPoolJob(prlJob);
				ogBasicData.GetPermitsForPoolJob(prlJob,olPermits);
				CString olGroupName = ogBasicData.GetTeamForPoolJob(prlJob);

				int ilGroupno;
//				if (!IsPassFilter(prlJob->Alid,olGroupName,prlShift->Sfca,olFunction,ogShiftData.IsAbsent(prlShift),olPermits) ||
//					!IsInTimeframe(prlJob) ||
//					(ilGroupno = FindGroup(prlJob->Alid,olGroupName,prlShift->Sfca)) == -1 ||
//					(bmHideCompletelyDelegatedTeams && IsCompletelyDelegatedPoolJob(prlJob)) ||
//					(bmHideAbsentEmps && ogJobData.IsAbsent(prlJob)) ||
//					(bmHideStandbyEmps && ogJobData.IsStandby(prlJob)) ||
//					(bmHideFidEmps && ogJobData.PoolJobHasFidJobs(prlJob->Urno)))
//					continue;
				if(!IsPassPoolJobFilter(prlJob, prlShift, prlEmp, olGroupName, olFunction, olPermits) ||
					(ilGroupno = FindGroup(prlJob->Alid,olGroupName,prlShift->Sfca)) == -1)
					continue;

				if (!ogBasicData.IsManager(olFunction))
				{
					int ilLineno;
					if (FindDutyBar(prlJob->Urno,ilGroupno,ilLineno))
					{
						STAFF_LINEDATA *prlLine = GetLine(ilGroupno,ilLineno);
						if (!prlLine)
							continue;

						// don't display a student that is part of a double tour
						if(ogDrsData.IsStudent(prlShift->Urno))
						{
							// we do nothing here, because polhdl will delete the pool job for us
							// ProcessJobDelete(prlJob);

						}
						else
						{
							// if this emp has a student accompanying him - get the student name
							EMPDATA *prlStudent = ogEmpData.GetEmpByUrno(ogDrsData.GetStudentForTeacher(prlShift->Urno));
							if(prlStudent != NULL)
							{
								prlLine->DoubleTourStudent.Format("%s %s",GetString(IDS_DOUBLETOURSTUDENT),ogEmpData.GetEmpName(prlStudent));
							}
							else
								prlLine->DoubleTourStudent = "";

							if(MessagingActivated())
							{
								LONG lParam = MAKELONG(ilLineno,ilGroupno);
								pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,UD_UPDATELINE,lParam);
							}
						}				
					}
				}
			}
		}
	}
}

void StaffDiagramViewer::ProcessDrsDelete(long lpDrru)
{
	SHIFTDATA *prlShift = NULL;
	prlShift = ogShiftData.GetShiftByUrno(lpDrru);
	if(prlShift != NULL)
	{
		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlShift->Peno);
		if (prlEmp != NULL)
		{
			// get pool jobs for this shift and update the pool jobs
			CCSPtrArray <JOBDATA> olPoolJobs;
			ogJobData.GetJobsByShur(olPoolJobs,prlShift->Urno,true);
			int ilNumPoolJobs = olPoolJobs.GetSize();
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				JOBDATA *prlJob = &olPoolJobs[ilPoolJob];
				if (!prlJob)
					continue;

				CStringArray olPermits;
				CString olFunction = ogBasicData.GetFunctionForPoolJob(prlJob);
				ogBasicData.GetPermitsForPoolJob(prlJob,olPermits);
				CString olGroupName = ogBasicData.GetTeamForPoolJob(prlJob);

				int ilGroupno;
//				if (!IsPassFilter(prlJob->Alid,olGroupName,prlShift->Sfca,olFunction,ogShiftData.IsAbsent(prlShift),olPermits) ||
//					!IsInTimeframe(prlJob) ||
//					(ilGroupno = FindGroup(prlJob->Alid,olGroupName,prlShift->Sfca)) == -1 ||
//					(bmHideCompletelyDelegatedTeams && IsCompletelyDelegatedPoolJob(prlJob)) ||
//					(bmHideAbsentEmps && ogJobData.IsAbsent(prlJob)) ||
//					(bmHideStandbyEmps && ogJobData.IsStandby(prlJob)) ||
//					(bmHideFidEmps && ogJobData.PoolJobHasFidJobs(prlJob->Urno)))
//					continue;
				if(!IsPassPoolJobFilter(prlJob, prlShift, prlEmp, olGroupName, olFunction, olPermits) ||
					(ilGroupno = FindGroup(prlJob->Alid,olGroupName,prlShift->Sfca)) == -1)
					continue;

				if (!ogBasicData.IsManager(olFunction))
				{
					int ilLineno;
					if (FindDutyBar(prlJob->Urno,ilGroupno,ilLineno))
					{
						STAFF_LINEDATA *prlLine = GetLine(ilGroupno,ilLineno);
						if (!prlLine)
							continue;

						prlLine->DoubleTourStudent = "";

						if(MessagingActivated())
						{
							LONG lParam = MAKELONG(ilLineno,ilGroupno);
							pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,UD_UPDATELINE,lParam);
						}
						
					}
				}
			}
		}
	}
}

void StaffDiagramViewer::ProcessShiftSelect(SHIFTDATA *prpShift)
{
	if(pomAttachWnd != NULL && pomAttachWnd->GetSafeHwnd())
	{
		bool blFound = false;
		if(prpShift != NULL)
		{
			// get pool jobs for this shift and update the pool jobs
			CCSPtrArray <JOBDATA> olPoolJobs;
			ogJobData.GetJobsByShur(olPoolJobs,prpShift->Urno,true);
			JOBDATA *prlFirstPoolJob = NULL;
			int ilNumPoolJobs = olPoolJobs.GetSize();
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				if (!prlFirstPoolJob) 
					prlFirstPoolJob = &olPoolJobs[ilPoolJob];
				else if (olPoolJobs[ilPoolJob].Acfr < prlFirstPoolJob->Acfr)
					prlFirstPoolJob = &olPoolJobs[ilPoolJob];
			}

			if(prlFirstPoolJob)
			{
				// check, whether this pool job is in range of data currently displayed
				if (!IsReallyOverlapped(prlFirstPoolJob->Acfr,prlFirstPoolJob->Acto,omStartTime,omEndTime))
				{
					ChangeViewTo(SelectView(),prlFirstPoolJob->Acfr,prlFirstPoolJob->Acto);					
				}

				STAFF_SELECTION rolSelection;
				if(FindDutyBar(prlFirstPoolJob->Urno,rolSelection.imGroupno,rolSelection.imLineno,false,true))
				{
					rolSelection.lmUserData = prlFirstPoolJob->Urno;
					LONG lParam = reinterpret_cast<LONG>(&rolSelection);
					pomAttachWnd->SendMessage(WM_SELECTDIAGRAM,UD_SELECTLINE, lParam);
					blFound = true;
				}
			}
		}
		if(!blFound)
		{
			pomAttachWnd->MessageBox(GetString(IDS_SV_NOTFOUND),GetString(IDS_SV_NOTFOUNDTITLE),MB_ICONEXCLAMATION );
		}
	}
}

				

void StaffDiagramViewer::ProcessShiftChange(SHIFTDATA *prpShift)
{
	if(prpShift != NULL)
	{
		// get pool jobs for this shift and update the pool jobs
		CCSPtrArray <JOBDATA> olPoolJobs;
		ogJobData.GetJobsByShur(olPoolJobs,prpShift->Urno,true);
		int ilNumPoolJobs = olPoolJobs.GetSize();
		for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
		{
			ProcessJobChange(&olPoolJobs[ilPoolJob]);
		}
	}
}
void StaffDiagramViewer::ProcessPdaChange(PDADATA *prpPda)
{
	if(prpPda != NULL)
	{
		if (prpPda->Ustf != 0L)
		{
			// get pool jobs for this staff and update the pool jobs
			CCSPtrArray <JOBDATA> olPoolJobs;
			ogJobData.GetJobsByUstf(olPoolJobs,prpPda->Ustf,true);
			int ilNumPoolJobs = olPoolJobs.GetSize();
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				ProcessJobChange(&olPoolJobs[ilPoolJob]);
			}
		}
	}
}

bool StaffDiagramViewer::ProcessPoolJobUpdate(JOBDATA *prpPoolJob)
{
	if(prpPoolJob == NULL)
		return false;

	TEAM_DATA *prlSourceTeam = GetTeamByPoolJobUrno(prpPoolJob->Urno);
	TEAM_DATA *prlTargetTeam = GetMatchingTeam(prpPoolJob, ogBasicData.GetTeamForPoolJob(prpPoolJob));
	bool blSourceTeamIsCompressed = (prlSourceTeam != NULL && prlSourceTeam->IsCompressed && prlSourceTeam->PoolJobs.GetSize() > 1) ? true : false;
	bool blTargetTeamIsCompressed = blSourceTeamIsCompressed;

	// prevent DDX updates
	bool blOldDisableMessaging = bmDisableMessaging;
	bmDisableMessaging = true;

	// collect team lines for sending updates
	CCSPtrArray <POOLJOBLINE> olPoolJobLines;
	AddTeamLinesToPoolJobLines(prlSourceTeam, olPoolJobLines);
	if(prlTargetTeam != NULL && prlSourceTeam != prlTargetTeam)
	{
		blTargetTeamIsCompressed = prlTargetTeam->IsCompressed;
		AddTeamLinesToPoolJobLines(prlTargetTeam, olPoolJobLines);
	}

	// set flags for the original pool job indicating the compressed state of the target and source teams
	// if both source + target are compress - do nothing
	// if source is compressed and target not - insert line only
	// if target is compressed but the source not - delete line only
	bool blFoundInTeam = false;
	POOLJOBLINE *prlPJL = NULL;
	for(int i = 0; i < olPoolJobLines.GetSize(); i++)
	{
		if(olPoolJobLines[i].PoolJobUrno == prpPoolJob->Urno)
		{
			blFoundInTeam = true;
			break;
		}
	}

	int ilGroupno = -1, ilLineno = -1;
	BOOL blLineWasDisplayed = FindDutyBar(prpPoolJob->Urno, ilGroupno, ilLineno, false, true);

	if(!blFoundInTeam)
		AddPoolJobToPoolJobLines(prpPoolJob->Urno, olPoolJobLines, ilGroupno, ilLineno);
	
	
	if(blSourceTeamIsCompressed || blTargetTeamIsCompressed)
	{
		for(int i = 0; i < olPoolJobLines.GetSize(); i++)
		{
			prlPJL = &olPoolJobLines[i];
			if(prlPJL->PoolJobUrno == prpPoolJob->Urno)
			{
				prlPJL->SourceTeamIsCompressed = blSourceTeamIsCompressed;
				prlPJL->TargetTeamIsCompressed = blTargetTeamIsCompressed;
				break;
			}
		}
	}

	// delete the job from its team
	if(prlSourceTeam != NULL)
		prlSourceTeam = DeletePoolJobFromTeam(prlSourceTeam, prpPoolJob->Urno, false);

	// 2 man team was compressed: DeletePoolJobFromTeam() caused IsCompressed flag to be set to false
	// if the removed emp was re-added then reset the IsCompressed flag 
	if(prlSourceTeam != NULL && prlSourceTeam->PoolJobs.GetSize() > 0)
		prlSourceTeam->IsCompressed = blSourceTeamIsCompressed;

	// delete the line if it was displayed and the team line wasn't compressed
	if(blLineWasDisplayed && !blSourceTeamIsCompressed)
		DeleteLine(ilGroupno, ilLineno);

	// add the line
	if(IsPassPoolJobFilter(prpPoolJob))
		ProcessJobNew(prpPoolJob);

	// check for 2nd to last pool job deleted from compressed team in which case the team is no longer compressed
	if(prlSourceTeam != NULL && prlSourceTeam->PoolJobs.GetSize() <= 1)
		prlSourceTeam->IsCompressed = false;

	// reactivate DDX updates
	bmDisableMessaging = blOldDisableMessaging;

	// recreate team lines so that they are sorted correctly
	for(int l = 0; l < olPoolJobLines.GetSize(); l++)
		ResortDisplayLines(olPoolJobLines[l].PoolJobUrno);

	// redisplay lines that may now be in a different order due to being resorted
	UpdateDisplayLinesForPoolJobs(olPoolJobLines);

	olPoolJobLines.DeleteAll();
	return true;
}

void StaffDiagramViewer::AddTeamLinesToPoolJobLines(TEAM_DATA *prpTeam, CCSPtrArray <POOLJOBLINE> &ropPoolJobLines)
{
	if(prpTeam != NULL)
	{
		int ilGroupno, ilLineno;
		if(prpTeam->IsCompressed)
		{
			if(FindCompressedDutyBar(prpTeam->FirstEmpPoolJobUrno, ilGroupno, ilLineno))
			{
				for(int ilPJ = 0; ilPJ < prpTeam->PoolJobs.GetSize(); ilPJ++)
				{
					POOLJOBLINE *prlPJL = new POOLJOBLINE;
					prlPJL->PoolJobUrno = prpTeam->PoolJobs[ilPJ].Urno;
					prlPJL->Groupno = ilGroupno;
					prlPJL->Lineno = ilLineno;
					ropPoolJobLines.Add(prlPJL);
				}
			}
		}
		else
		{
			for(int i = 0; i < prpTeam->PoolJobs.GetSize(); i++)
			{
				AddPoolJobToPoolJobLines(prpTeam->PoolJobs[i].Urno, ropPoolJobLines);
			}
		}
	}
}

void StaffDiagramViewer::AddPoolJobToPoolJobLines(long lpPoolJobUrno, CCSPtrArray <POOLJOBLINE> &ropPoolJobLines, int ipGroupno /* = -1 */, int ipLineno /* = -1 */)
{
	int ilGroupno, ilLineno;
	if(ipGroupno != -1 && ipLineno != -1)
	{
		ilGroupno = ipGroupno;
		ilLineno = ipLineno;
	}
	else
	{
		FindDutyBar(lpPoolJobUrno, ilGroupno, ilLineno);
	}

	POOLJOBLINE *prlPJL = new POOLJOBLINE;
	prlPJL->PoolJobUrno = lpPoolJobUrno;
	prlPJL->Groupno = ilGroupno;
	prlPJL->Lineno = ilLineno;
	ropPoolJobLines.Add(prlPJL);
}

// removes the line and recreates it in the array of lines
// this may be necessary when a pool job is added/removed/changed
// in a team, as the change may result in a different sort
// condition for the team, so it needs to be resorted
void StaffDiagramViewer::ResortDisplayLines(long lpPoolJobUrno)
{
	int ilGroupno, ilLineno;
	if(FindDutyBar(lpPoolJobUrno, ilGroupno, ilLineno, false, true))
	{
		STAFF_LINEDATA *prlLine = &omGroups[ilGroupno].Lines[ilLineno];
		int ilMaxOverlapLevel = prlLine->MaxOverlapLevel;
		omGroups[ilGroupno].Lines.RemoveAt(ilLineno);
		CreateLine(ilGroupno, prlLine); // results in the line being resorted
		prlLine->MaxOverlapLevel = ilMaxOverlapLevel;
	}
}

void StaffDiagramViewer::UpdateDisplayLinesForPoolJobs(CCSPtrArray <POOLJOBLINE> &ropPoolJobLines)
{
	CCSPtrArray <POOLJOBLINE> olLines;
	POOLJOBLINE *prlPJL = NULL;
	LONG llLine;

	// get new line numbers
	for(int l = 0; l < ropPoolJobLines.GetSize(); l++)
	{
		prlPJL = &ropPoolJobLines[l];
		FindDutyBar(prlPJL->PoolJobUrno, prlPJL->NewGroupno, prlPJL->NewLineno, false, true);
		//TRACE("OldLineno %d  NewLineno %d  SourceTeamIsCompressed %s TargetTeamIsCompressed %s\n", prlPJL->Lineno,prlPJL->NewLineno,prlPJL->SourceTeamIsCompressed ? "true" : "false",prlPJL->TargetTeamIsCompressed ? "true" : "false");
	}


	// perform updates
	CDWordArray olUpdatedLines;
	for(l = 0; l < ropPoolJobLines.GetSize(); l++)
	{
		prlPJL = &ropPoolJobLines[l];
		if(prlPJL->Groupno != -1 && prlPJL->Lineno != -1)
		{
			llLine = MAKELONG(prlPJL->NewLineno, prlPJL->NewGroupno);

			bool blNotAlreadyUpdated = true;
			for(int ilDL = 0; ilDL < olUpdatedLines.GetSize(); ilDL++)
			{
				if(llLine == (long) olUpdatedLines[ilDL])
				{
					blNotAlreadyUpdated = false;
					break;
				}
			}

			if(blNotAlreadyUpdated)
			{
				if(prlPJL->Groupno == prlPJL->NewGroupno && prlPJL->Lineno == prlPJL->NewLineno)
				{
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINEHEIGHT, llLine);
					olUpdatedLines.Add((DWORD) llLine);
				}
			}
		}
	}

	// perform deletes
	CDWordArray olDeletedLines;
	ropPoolJobLines.Sort(ByLineNumberReverse); // delete lines in reverse order
	for(l = 0; l < ropPoolJobLines.GetSize(); l++)
	{
		prlPJL = &ropPoolJobLines[l];
		if(prlPJL->Groupno != -1 && prlPJL->Lineno != -1 && !prlPJL->SourceTeamIsCompressed)
		{
			llLine = MAKELONG(prlPJL->Lineno, prlPJL->Groupno);

			bool blNotAlreadyDeleted = true;
			for(int ilDL = 0; ilDL < olDeletedLines.GetSize(); ilDL++)
			{
				if(llLine == (long) olDeletedLines[ilDL])
				{
					blNotAlreadyDeleted = false;
					break;
				}
			}

			if(blNotAlreadyDeleted)
			{
				if((!prlPJL->SourceTeamIsCompressed && prlPJL->TargetTeamIsCompressed) || 
					prlPJL->NewGroupno == -1 || prlPJL->NewLineno == -1 || prlPJL->Groupno != prlPJL->NewGroupno || prlPJL->Lineno != prlPJL->NewLineno)
				{
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_DELETELINE, llLine);
					olDeletedLines.Add((DWORD) llLine);
					//TRACE("DELETED line %d\n", prlPJL->Lineno);
				}
			}
		}
	}

	// perform inserts
	CDWordArray olInsertedLines;
	ropPoolJobLines.Sort(ByNewLineNumber); // insert lines in line order
	for(l = 0; l < ropPoolJobLines.GetSize(); l++)
	{
		prlPJL = &ropPoolJobLines[l];
		if(prlPJL->NewGroupno != -1 && prlPJL->NewLineno != -1 && !prlPJL->TargetTeamIsCompressed)
		{
			llLine = MAKELONG(prlPJL->NewLineno, prlPJL->NewGroupno);

			bool blNotAlreadyInserted = true;
			for(int ilIL = 0; ilIL < olInsertedLines.GetSize(); ilIL++)
			{
				if(llLine == (long) olInsertedLines[ilIL])
				{
					blNotAlreadyInserted = false;
					break;
				}
			}

			if(blNotAlreadyInserted)
			{
				if((prlPJL->SourceTeamIsCompressed && !prlPJL->TargetTeamIsCompressed) || 
					prlPJL->NewGroupno != -1 && prlPJL->NewLineno != -1 && (prlPJL->Groupno != prlPJL->NewGroupno || prlPJL->Lineno != prlPJL->NewLineno))
				{
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, llLine);
					olInsertedLines.Add((DWORD) llLine);
					//TRACE("INSERTED line %d\n", prlPJL->NewLineno);
				}
			}
		}
	}
}

void StaffDiagramViewer::ProcessJobChange(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		if(!strcmp(prpJob->Jtco,JOBEQUIPMENTFASTLINK) && ogBasicData.bmDisplayEquipment)
		{
			ProcessEquipmentFastLinkUpdate(prpJob);
			return;
		}

		//ogBasicData.Trace("ProcessJobChange() Job Received URNO <%ld>\n",prpJob->Urno);
		int ilGroupno, ilLineno, ilBarno;
		if(!strcmp(prpJob->Jtco,JOBPOOL))
		{
			if(ogBasicData.IsManager(ogBasicData.GetFunctionForPoolJob(prpJob)))
			{
				int ilManagerno;
				if(FindManager(prpJob->Urno, ilGroupno, ilManagerno))
				{
					DeleteManager(ilGroupno, ilManagerno);
					if(MessagingActivated())
					{
						LONG lParam = MAKELONG(0, ilGroupno);
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATEGROUP, lParam);
					}
				}
				ProcessJobNew(prpJob);
			}
			else
			{
				ProcessPoolJobUpdate(prpJob);
			}
//			if(!IsPassPoolJobFilter(prpJob))
//			{
//				ProcessPoolJobUpdate(prpJob);
//				//ProcessJobDelete(prpJob);
//			}
//			else if(ogBasicData.IsManager(ogBasicData.GetFunctionForPoolJob(prpJob)))
//			{
//				int ilManagerno;
//				// is a flight manager
//				if(FindManager(prpJob->Urno, ilGroupno, ilManagerno))
//				{
//					DeleteManager(ilGroupno, ilManagerno);
//					if(MessagingActivated())
//					{
//						LONG lParam = MAKELONG(0, ilGroupno);
//						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATEGROUP, lParam);
//					}
//				}
//				ProcessJobNew(prpJob);
//			}
//			else
//			{
//				if(!FindDutyBar(prpJob->Urno, ilGroupno, ilLineno) &&  !FindCompressedDutyBar(prpJob->Urno, ilGroupno, ilLineno))
//				{
//					ProcessJobNew(prpJob);
//				}
//				else
//				{
//					if(!ProcessPoolJobUpdate(prpJob))
//					{
//						bool blOldDisableMessaging = bmDisableMessaging;
//						bmDisableMessaging = true;
//						DeleteLine(ilGroupno, ilLineno);
//						ProcessJobNew(prpJob);
//
//						bmDisableMessaging = blOldDisableMessaging;
//						if(MessagingActivated())
//						{
//							LONG lParam = MAKELONG(ilLineno, ilGroupno);
//							int ilNewGroupno = -1, ilNewLineno = -1;
//							if(!FindDutyBar(prpJob->Urno, ilNewGroupno, ilNewLineno))
//							{
//								pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_DELETELINE, lParam);
//							}
//							else if(ilGroupno != ilNewGroupno || ilLineno != ilNewLineno)
//							{
//								pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_DELETELINE, lParam);
//								lParam = MAKELONG(ilNewLineno, ilNewGroupno);
//								pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, lParam);
//							}
//							else
//							{
//								pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINE, lParam);
//							}
//						}
//					}
//				}
//			}
		}
		else if(!strcmp(prpJob->Jtco,JOBTEMPABSENCE))
		{
			STAFF_LINEDATA *prlLine;
			for(int ilGroupno = 0; ilGroupno < GetGroupCount(); ilGroupno++)
			{
				for(int ilLineno = 0; ilLineno < GetLineCount(ilGroupno); ilLineno++)
				{
					prlLine = GetLine(ilGroupno, ilLineno);
					if(prlLine->StfUrno == prpJob->Ustf)
					{
						DeleteBkBarsByJob(ilGroupno,ilLineno,prpJob->Urno);
						
							
		 
						
						MakeTempAbsenceBkBar(prlLine, prpJob);
						prlLine->BkBars.Sort(ByType);

						// Notifies the attached window (if exist)
						if(MessagingActivated())
						{
							LONG lParam = MAKELONG(ilLineno, ilGroupno);
							pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINEHEIGHT, lParam);
						}
					}
				}
			}
		}
		else // NON POOL JOB
		{
			bool blHiddenPoolJob = false;
			JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(prpJob->Jour);
			if(prlPoolJob != NULL)
			{
				if((!strcmp(prpJob->Jtco,JOBTEAMDELEGATION) && bmHideCompletelyDelegatedTeams && IsCompletelyDelegatedPoolJob(prlPoolJob)) ||
					(bmHideAbsentEmps && ogJobData.IsAbsent(prlPoolJob)) ||
					(bmHideStandbyEmps && ogJobData.IsStandby(prlPoolJob)) ||
					(bmHideFidEmps && ogJobData.PoolJobHasFidJobs(prlPoolJob->Urno)))
				{
					blHiddenPoolJob = true;
					int ilGroupno, ilLineno;
					if(FindDutyBar(prlPoolJob->Urno, ilGroupno, ilLineno))
					{
						DeleteLine(ilGroupno, ilLineno);
						if(MessagingActivated())
						{
							// pool job was displayed but is now hidden so remove
							LONG lParam = MAKELONG(ilLineno, ilGroupno);
							pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_DELETELINE, lParam);
						}
					}
				}
			}

			if(!blHiddenPoolJob)
			{
				//ogBasicData.Trace("Not a Pool Job - ");
				TEAM_DATA *prlTeam = GetTeamByJobUrno(prpJob->Urno);
				TEAM_DATA *prlNewTeam = GetTeamByPoolJobUrno(prpJob->Jour);
				if((prlTeam != NULL && prlTeam->IsCompressed) || (prlNewTeam != NULL && prlNewTeam->IsCompressed))
				{
					if(prlTeam == NULL)
					{
						ProcessJobDelete(prpJob);
					}
					else if(!prlTeam->IsCompressed)
					{
						ProcessJobDelete(prpJob);
						ProcessJobNew(prpJob);
					}
					else
					{
						MakeCompressedJobsForTeam(prlTeam);
					}

					if(prlNewTeam != NULL && prlTeam != prlNewTeam)
					{
						if(prlNewTeam->IsCompressed)
						{
							MakeCompressedJobsForTeam(prlNewTeam);
						}
						else
						{
							ProcessJobNew(prpJob);
						}
					}
				}
				else
				{
					if(FindBar(prpJob->Urno, ilGroupno, ilLineno, ilBarno))
					{
						//ogBasicData.Trace("Job Bar found - ");

						if(!JobBelongsToThisLine(GetLine(ilGroupno, ilLineno),prpJob->Jour))
						{
							// the Jour (pool job URNO) of the received job no longer matches
							// the pool job URNO of the line where it was display (ie job moved
							// to another line) - recreate it on a new line
							//ogBasicData.Trace("Jour (PoolJobUrno) changed so delete the bar and recreate on a new line.\n");
							ProcessJobDelete(prpJob);
							ProcessJobNew(prpJob);
						}
						else
						{
							//ogBasicData.Trace("Update the bar\n");
							STAFF_BARDATA *prlBar = GetBar(ilGroupno, ilLineno, ilBarno);
							MakeBarData(prlBar,prpJob);
							STAFF_BARDATA rlBar = *prlBar;

							rlBar.Indicators.RemoveAll();	// remove indicators, this just another copy of pointers
								
							// We also have to create a new indicator for showing the range of time on TimeScale,
							// and there is only one indicator in Staff Diagram
							rlBar.Indicators.New();
							rlBar.Indicators[0].StartTime = rlBar.StartTime;
							rlBar.Indicators[0].EndTime = rlBar.EndTime;
							//rlBar.Indicators[0].Color = ogColors[prpJob->ColorIndex];
							rlBar.Indicators[0].Color = ogColors[ogConflicts.GetJobConflictColor(prpJob)];

							int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
							DeleteBar(ilGroupno, ilLineno, ilBarno);
							CreateBar(ilGroupno, ilLineno, &rlBar, TRUE);

							// Notifies the attached window (if exist)
							if(MessagingActivated())
							{
								LONG lParam = MAKELONG(ilLineno, ilGroupno);
								if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
									pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
								else
									pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
							}
						}
					}
					else // job not found
					{
						int ilGroupno, ilLineno;
						bool blUpdated = false;

						if(prlPoolJob != NULL)
						{
							if(IsPassPoolJobFilter(prlPoolJob) && !strcmp(prlPoolJob->Jtco,JOBPOOL) && !FindDutyBar(prlPoolJob->Urno, ilGroupno, ilLineno) &&  !FindCompressedDutyBar(prlPoolJob->Urno, ilGroupno, ilLineno))
							{
								blUpdated = true;
								// required when flight independent job set to finished
								// emps who were filtered out need to be inserted
								ProcessJobNew(prlPoolJob);
								CCSPtrArray<JOBDATA> olJobs;
								ogJobData.GetJobsByJour(olJobs, prlPoolJob->Urno);
								for(int ilJ = 0; ilJ < olJobs.GetSize(); ilJ++)
								{
									JOBDATA *prlJob = &olJobs[ilJ];
									int ilJGroup, ilJLine, ilJBar;
									if(!FindBar(prlJob->Urno, ilJGroup, ilJLine, ilJBar))
										ProcessJobNew(prlJob);
								}
							}
						}
						if(!blUpdated)
						{
							ProcessJobNew(prpJob);
						}
					}
				}
			}
		}
	}
}


void StaffDiagramViewer::ProcessEquipmentFastLinkUpdate(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		// update all pool jobs for the employee that has this fast link
		CCSPtrArray<JOBDATA> olJobs;
		ogJobData.GetJobsByUstf(olJobs, prpJob->Ustf);
		int ilNumJobs = olJobs.GetSize();
		for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
		{
			JOBDATA *prlJob = &olJobs[ilJob];
			if(!strcmp(prlJob->Jtco,JOBPOOL))
			{
				ProcessJobChange(prlJob);
			}
		}
	}
}

void StaffDiagramViewer::ProcessJobNew(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		if(!strcmp(prpJob->Jtco,JOBEQUIPMENTFASTLINK) && ogBasicData.bmDisplayEquipment)
		{
			ProcessEquipmentFastLinkUpdate(prpJob);
			return;
		}

		int ilGroupno, ilManagerno, ilLineno;

		if(!strcmp(prpJob->Jtco,JOBPOOL))
		{
			// pool job
			//ogBasicData.Trace("Pool Job - ");
			bool blNewLineCreated = MakePoolJob(prpJob);
			if (FindManager(prpJob->Urno, ilGroupno, ilManagerno))	// a flight manager?
			{
				//ogBasicData.Trace("New Flight Manager Created\n");
				if(MessagingActivated())
				{
					LONG lParam = MAKELONG(0, ilGroupno);
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATEGROUP, lParam);
				}
			}
//			else if (FindDutyBar(prpJob->Urno, ilGroupno, ilLineno) ||
//				FindCompressedDutyBar(prpJob->Urno, ilGroupno, ilLineno))
//			{
//				//ogBasicData.Trace("New Pool Job Shift Created\n");
//				MakeBarsForPoolJob(ilGroupno, ilLineno, prpJob->Urno);
//				if(MessagingActivated())
//				{
//					LONG lParam = MAKELONG(ilLineno, ilGroupno);
//					if(blNewLineCreated)
//					{
//						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, lParam);
//					}
//					else
//					{
//						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINE, lParam);
//					}
//				}
//			}
			else
			{
				BOOL blSendUpdate = FALSE;
				ilLineno = -1;
				TEAM_DATA *prlTeam = NULL;
				if((prlTeam = GetTeamByPoolJobUrno(prpJob->Urno)) != NULL && prlTeam->IsCompressed)
				{
					MakeCompressedJobsForTeam(prlTeam);
					blSendUpdate = FindCompressedDutyBar(prpJob->Urno, ilGroupno, ilLineno);
				}
				else
				{
					if((blSendUpdate = FindDutyBar(prpJob->Urno, ilGroupno, ilLineno)) == TRUE)
					{
						MakeBkBarsForPoolJob(ilGroupno, ilLineno, prpJob->Urno);
						MakeBarsForPoolJob(ilGroupno, ilLineno, prpJob->Urno);
					}
				}
				if(blSendUpdate && MessagingActivated())
				{
					LONG lParam = MAKELONG(ilLineno, ilGroupno);
					if(blNewLineCreated)
					{
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, lParam);
					}
					else
					{
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINE, lParam);
					}
				}
			}
		}
		else
		{
			bool blIsPause = !strcmp(prpJob->Jtco, JOBBREAK) ? true : false;
			if(FindDutyBar(prpJob->Jour, ilGroupno, ilLineno, blIsPause))
			{
				// normal job
				int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
				MakeBar(ilGroupno, ilLineno, prpJob);
				STAFF_LINEDATA *prlLine = GetLine(ilGroupno,ilLineno);
				if(prlLine != NULL)	prlLine->BkBars.Sort(ByType);
				//ogBasicData.Trace("New Job Bar Created for pool URNO <%ld>\n",prpJob->Jour);
				if(MessagingActivated())
				{
					LONG lParam = MAKELONG(ilLineno, ilGroupno);
					if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINE, lParam);
					else
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINEHEIGHT, lParam);
				}
			}
		}
	}
}


void StaffDiagramViewer::ProcessJobDelete(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		if(!strcmp(prpJob->Jtco,JOBEQUIPMENTFASTLINK) && ogBasicData.bmDisplayEquipment)
		{
			ProcessEquipmentFastLinkUpdate(prpJob);
			return;
		}

		int ilGroupno, ilManagerno, ilLineno, ilBarno;

		if(!strcmp(prpJob->Jtco, JOBPOOL))
		{
			// Pool Job
			//ogBasicData.Trace("Pool Job - ");
			TEAM_DATA *prlTeam = GetTeamByJobUrno(prpJob->Urno);
			if(prlTeam != NULL)
			{
				// NOTE: deletion of pool jobs from team is now handled in StaffDiagram
				// because the whole chart needs to be redisplayed in case the team makeup
				// has changed comppletely (ie deletion of the first member in a team)
//				DeletePoolJobFromTeam(prlTeam, prpJob->Urno);
//				ChangeViewTo(SelectView(),omStartTime,omEndTime);
//				pomAttachWnd->Invalidate();
			}
			else if(FindManager(prpJob->Urno, ilGroupno, ilManagerno))
			{
				// flight manager
				//ogBasicData.Trace("Flight Manager Pool Job Deleted\n");
				DeleteManager(ilGroupno, ilManagerno);
				if(MessagingActivated())
				{
					LONG lParam = MAKELONG(0, ilGroupno);
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATEGROUP, lParam);
				}
			}
			else if(FindDutyBar(prpJob->Urno, ilGroupno, ilLineno))
			{
				// normal shift
				//ogBasicData.Trace("Pool Job Shift Deleted\n");
				DeleteLine(ilGroupno, ilLineno);
				if(MessagingActivated())
				{
					LONG lParam = MAKELONG(ilLineno, ilGroupno);
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_DELETELINE, lParam);
				}
			}
			else
			{
				//ogBasicData.Trace("Line for Pool Job Shift Not Found\n");
			}
		}
		else if(!strcmp(prpJob->Jtco, JOBTEMPABSENCE))
		{
			STAFF_LINEDATA *prlLine;
			
			for(int ilGroupno = 0; ilGroupno < GetGroupCount(); ilGroupno++)
			{
				for(int ilLineno = 0; ilLineno < GetLineCount(ilGroupno); ilLineno++)
				{
					prlLine = GetLine(ilGroupno, ilLineno);
					if(prlLine->StfUrno == prpJob->Ustf)
					{
						DeleteBkBarsByJob(ilGroupno,ilLineno,prpJob->Urno);

						// Notifies the attached window (if exist)
						if(MessagingActivated())
						{
							LONG lParam = MAKELONG(ilLineno, ilGroupno);
							pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINE, lParam);
						}
					}
				}
			}
		}
		else
		{
			TEAM_DATA *prlTeam = GetTeamByJobUrno(prpJob->Urno);
			if(prlTeam != NULL && prlTeam->IsCompressed)
			{
				MakeCompressedJobsForTeam(prlTeam);
			}
			else if(FindBar(prpJob->Urno, ilGroupno, ilLineno, ilBarno))
			{
				int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
				DeleteBar(ilGroupno, ilLineno, ilBarno);

				// Notifies the attached window (if exist)
				if(MessagingActivated())
				{
					LONG lParam = MAKELONG(ilLineno, ilGroupno);
					if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
					else
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
				}
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramViewer -- Filtering, Sorting, and Grouping
//
// Requirements:
// The StaffDiagram is the most complex diagram for implement the filtering, sorting and
// grouping. This diagram could be grouped by Pool, grouped by Team, and grouped by ShiftCode.
// However, no matter what the groupping is, each GanttLine in this diagram will represent
// a duty job (job type JOBPOOL). Sorting may be a combination of ShiftBegin, ShiftEnd,
// ShiftCode, Team, Name and Rank. Filtering may be a combination of Pool, Team, ShiftCode,
// and Rank.
//
// Methods for change the view:
// ChangeViewTo		Change view, reload everything from Ceda????Data
//
// PrepareGroupping	Prepare the enumerate group value
// PrepareFilter	Prepare CMapStringToPtr or CMapPtrToPtr for filtering
// PrepareSorter	Prepare sort criterias before sorting take place
//
// IsPassFilter		Return TRUE if the given record satisfies the filter
// CompareGroup		Return 0 or -1 or 1 as the result of comparison of two groups
// CompareLine		Return 0 or -1 or 1 as the result of comparison of two lines
// CompareManager	Return 0 or -1 or 1 as the result of comparison of two managers
//
//
// Programmer notes:
// At the program Start up, we will create a default view for each dialog. This will
// overwrite the <default> view saved in the last time execution.
//
// When the viewer is constructed or everytimes the view changes, we will clear
// everything from the viewer buffer and do the insertion sorting. We will insert
// the record only if it satisfies our filter. Conceptually we will filter the record
// first, then sort in (by insertion sort), and group in.
//
// To let the filter work fast, we will implement a CMapStringToPtr for every filter
// condition. For example, let's say that we have a CStringArray for every possible
// values of a filter. We will have a CMapStringToPtr which could tell us the given
// key value is selected in the filter or not.
//
void StaffDiagramViewer::ChangeViewTo(const char *pcpViewName,
	CTime opStartTime, CTime opEndTime)
{
	DeleteAll();	// remove everything

	bmTeamLeaderColourDefined = false;
	imTeamLeaderColour = BLUE;
	CString olColour;
	if(ogDlgSettings.GetValue("FUNCTIONCOLOURS", GetString(IDS_FUNCCOLOURS_TEAMLEADER1), olColour))
	{
		imTeamLeaderColour = (COLORREF) atol(olColour);
		bmTeamLeaderColourDefined = true;
	}

	CString olPrevView = GetViewName();

	ogCfgData.rmUserSetup.STCV = pcpViewName;
	SelectView(pcpViewName);        

	if(olPrevView != GetViewName())
	{
		// for compatibility with version 1.19
		CString olHideCompletelyDelegatedTeams = GetUserData("HIDEDELEG");
		if (olHideCompletelyDelegatedTeams.IsEmpty())
			bmHideCompletelyDelegatedTeams = false;
		else
			bmHideCompletelyDelegatedTeams = olHideCompletelyDelegatedTeams == "YES";
	}

	PrepareGrouping();
	PrepareSorter();
	PrepareFilter();
	omStartTime = opStartTime;
	omEndTime = opEndTime;

	bmDisableMessaging = true; // prevent single insert/update/delete of lines/bars sent to the diagram

	MakeGroups();
	MakeLinesAndManagers();
	MakeBars();

	bmDisableMessaging = false; // allow single insert/update/delete of lines/bars sent to the diagram
}

void StaffDiagramViewer::PrepareGrouping()
{
	CString olGroupBy = CViewer::GetGroup();
	if (olGroupBy == "Pool")
		omGroupBy = GROUP_BY_POOL;
	else if (olGroupBy == "Team")
		omGroupBy = GROUP_BY_TEAM;
	else if (olGroupBy == "Schichtcode")
		omGroupBy = GROUP_BY_SHIFTCODE;
	else
		omGroupBy = GROUP_BY_DEFAULT;

	bmTeamView = ogBasicData.bmDisplayTeams && GetUserData("GROUPVIEW") == "YES";
	bmHideAbsentEmps = GetUserData("HIDEABSENTEMPS") == "YES";
	bmHideStandbyEmps = GetUserData("HIDESTANDBYEMPS") == "YES";
	bmHideFidEmps = GetUserData("HIDEFIDEMPS") == "YES";
}

void StaffDiagramViewer::PrepareFilter()
{
	bmUseAllPools = SetFilterMap("Pool", omCMapForPool, GetString(IDS_ALLSTRING));

	bmUseAllTeams = SetFilterMap("Team", omCMapForTeam, GetString(IDS_ALLSTRING));
	bmEmptyTeamSelected = CheckForEmptyValue(omCMapForTeam, GetString(IDS_NOWORKGROUP));

	bmUseAllShiftCodes = SetFilterMap("Schichtcode", omCMapForShiftCode, GetString(IDS_ALLSTRING));
	bmUseAllRanks = SetFilterMap("Dienstrang", omCMapForRank, GetString(IDS_ALLSTRING));
	bmUseAllPermits = SetFilterMap("Permits", omCMapForPermits, GetString(IDS_ALLSTRING));
	bmUseAllShiftCodes = SetFilterMap("Schichtcode", omCMapForShiftCode, GetString(IDS_ALLSTRING));
	bmUseAllRanks = SetFilterMap("Dienstrang", omCMapForRank, GetString(IDS_ALLSTRING));
	bmUseAllPermits = SetFilterMap("Permits", omCMapForPermits, GetString(IDS_ALLSTRING));
}

void StaffDiagramViewer::PrepareSorter()
{
	// Get sort orders from the registry database
	GetSort(omSortOrder);
	// Even if there is no sorting specified, we will sort each duty (GanttLine) by the beginning time
	omSortOrder.Add("JobPool");
}

bool StaffDiagramViewer::IsPassFilter(const char *pcpPoolId, const char *pcpTeamId,
	const char *pcpShiftCode, const char *pcpRank, bool bpIsAbsent, CStringArray &ropPermits)
{
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //
	// if this filter is updated then the filter in AvailableEmpsViewer must also be changed. //
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //

	void *p;
	BOOL blIsPoolOk = bmUseAllPools || omCMapForPool.Lookup(CString(pcpPoolId), p);
	BOOL blIsTeamOk = bmUseAllTeams || (strlen(pcpTeamId) == 0 && bmEmptyTeamSelected) || omCMapForTeam.Lookup(CString(pcpTeamId), p);
	BOOL blIsShiftCodeOk = bmUseAllShiftCodes || omCMapForShiftCode.Lookup(CString(pcpShiftCode), p);
	BOOL blIsRankOk = bmUseAllRanks || omCMapForRank.Lookup(CString(pcpRank), p);
	BOOL blIsPermitOk = bmUseAllPermits;
	int ilNumPermits = ropPermits.GetSize();
	for(int ilPermit = 0; !blIsPermitOk && ilPermit < ilNumPermits; ilPermit++)
	{
		blIsPermitOk = omCMapForPermits.Lookup(ropPermits[ilPermit], p);
	}
	if (bpIsAbsent)
	{
		blIsShiftCodeOk = TRUE;
	}
	return (blIsPoolOk && blIsTeamOk && blIsShiftCodeOk && blIsRankOk && blIsPermitOk);
}


// check if the whole shift or all the pool jobs within the shift are covered by a team-delegation-job
bool StaffDiagramViewer::IsCompletelyDelegatedPoolJob(JOBDATA *prpPoolJob)
{
	bool blIsCompletelyDelegatedPoolJob = false;
	if(prpPoolJob != NULL)
	{
		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetTeamDelegationsForPoolJob(prpPoolJob->Urno, olJobs);
		olJobs.Sort(ByJobStartTime);

		CTime olFrom = TIMENULL, olTo = TIMENULL;

		int ilNumJobs = olJobs.GetSize();
		for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
		{
			JOBDATA *prlJob = &olJobs[ilJob];
			if(!strcmp(prlJob->Jtco, JOBTEAMDELEGATION))
			{
				if(olFrom == TIMENULL)
				{
					olFrom = prlJob->Acfr;
					olTo = prlJob->Acto;
				}
				else
				{
					if(IsOverlapped(olFrom,olTo,prlJob->Acfr,prlJob->Acto))
					{
						if(prlJob->Acfr < olFrom)
						{
							olFrom = prlJob->Acfr;
						}
						if(prlJob->Acto > olTo)
						{
							olTo = prlJob->Acto;
						}
					}
				}
			}
		}

		if(olFrom != TIMENULL && olFrom <= prpPoolJob->Acfr && olTo >= prpPoolJob->Acto)
		{
			blIsCompletelyDelegatedPoolJob = true;
		}
	}

	return blIsCompletelyDelegatedPoolJob;
}

bool StaffDiagramViewer::IsInTimeframe(JOBDATA *prpPoolJob)
{
	bool blIsInTimeframe = false;
	if(prpPoolJob != NULL)
	{
		if(!(blIsInTimeframe = IsOverlapped(prpPoolJob->Acfr, prpPoolJob->Acto, omStartTime, omEndTime)))
		{
			// the pool job itself doesn't overlap the timeframe so check if there are any
			// jobs for this pool job whicj do overlap the timeframe
			CCSPtrArray <JOBDATA> olJobs;
			ogJobData.GetJobsByJour(olJobs, prpPoolJob->Urno);
			int ilNumJobs = olJobs.GetSize();
			for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
			{
				if((blIsInTimeframe = IsOverlapped(olJobs[ilJ].Acfr, olJobs[ilJ].Acto, omStartTime, omEndTime)))
				{
					break;
				}
			}
		}		
	}

	return blIsInTimeframe;
}

int StaffDiagramViewer::CompareGroup(STAFF_GROUPDATA *prpGroup1, STAFF_GROUPDATA *prpGroup2)
{
	// Groups in StaffDiagram always ordered by TeamId, so let's compare them.
	CString &s1 = prpGroup1->GroupId;
	CString &s2 = prpGroup2->GroupId;
	return (s1 == s2)? 0: (s1 > s2)? 1: -1;
}

CString StaffDiagramViewer::MakeSortString(STAFF_LINEDATA *prpLine)
{
	CString olSortString;

	CString olWorkGroupTypeSortOrder = "ZZ9999"; // not team leader
	if(bmTeamView)
	{
		TEAM_DATA *prlTeam = GetTeamByPoolJobUrno(prpLine->JobUrno);
		if(prlTeam != NULL)
		{
			// for teams the sorting goes on within the team, so make the fisrt
			// sort conditions the team start time plus the team name
			CTime olTeamStartTime = (prlTeam->SortStartDate != NULL) ? prlTeam->SortStartDate : prpLine->ShiftAcfr;
			CTime olTeamEndTime = (prlTeam->SortEndDate != NULL) ? prlTeam->SortEndDate : prpLine->ShiftActo;
			olSortString.Format("%s%s%s",olTeamStartTime.Format("%Y%m%d%H%M%S"),olTeamEndTime.Format("%Y%m%d%H%M%S"),prpLine->ShiftTmid);

			// GroupFunctionWeight = numerical order of the emp's group type function
			if(prlTeam->TeamLeaderPoolJobUrno == prpLine->JobUrno)
			{
				// "TL" -> trick to cause team leader to be displayed first
				olWorkGroupTypeSortOrder.Format("TL%04d",prpLine->GroupFunctionWeight); 
			}
			else // "ZZ" -> not team leader
			{
				olWorkGroupTypeSortOrder.Format("ZZ%04d",prpLine->GroupFunctionWeight); 
			}
		}
		else
		{
			olSortString.Format("%s%s%s",prpLine->ShiftAcfr.Format("%Y%m%d%H%M%S"),prpLine->ShiftActo.Format("%Y%m%d%H%M%S"),prpLine->ShiftTmid);
		}
	}

	int n = omSortOrder.GetSize();
	for (int i = 0; i < n; i++)
	{
		int ilCompareResult = 0;

		if (omSortOrder[i] == "Schichtbegin")
			olSortString += prpLine->ShiftAcfr.Format("%Y%m%d%H%M%S");
		else if (omSortOrder[i] == "Schichtend")
			olSortString += prpLine->ShiftActo.Format("%Y%m%d%H%M%S");
		else if (omSortOrder[i] == "Schichtcode")
			olSortString += prpLine->ShiftStid;
		else if (omSortOrder[i] == "Team")
			olSortString += prpLine->ShiftTmid;
		else if (omSortOrder[i] == "Name")
			olSortString += prpLine->EmpName;
		else if (omSortOrder[i] == "Dienstrang")
			olSortString += prpLine->EmpRank;
		else if (omSortOrder[i] == "JobPool")
			olSortString += prpLine->BkBars[0].StartTime.Format("%Y%m%d%H%M%S");
		else if (omSortOrder[i] == "WorkGroupType")
			olSortString += olWorkGroupTypeSortOrder;
	}

	// allow for 2 emps with identical sort values i.e. if only workgroup was specified as a sort condition
	// adding the staff urno at the end causes the employees with identical sort values to always be sorted
	// in the same way, otherwise when a pool job update for one employee comes, the emp will be deleted
	// and re-inserted on a different line. Deletion+re-insertion is necessary when emp details e.g. workgroup
	// or function changes, then the emp will need to be displayed on a different line
	CString olStfu;
	olStfu.Format("(%ld)", prpLine->StfUrno);
	olSortString += olStfu;

	return olSortString;
}


int StaffDiagramViewer::CompareManager(STAFF_MANAGERDATA *prpManager1, STAFF_MANAGERDATA *prpManager2)
{
	// Compare manager -- we will sort them by name
	return  (prpManager1->EmpLnam == prpManager2->EmpLnam)? 0:
		(prpManager1->EmpLnam > prpManager2->EmpLnam)? 1: -1;
}


/////////////////////////////////////////////////////////////////////////////
// StaffDiagramViewer -- Data for displaying graphical objects
//
// The related graphical class: StaffDiagram, StaffChart, and StaffGantt will use this
// StaffViewer as the source of data for displaying GanttLine and GanttBar objects.
//
// We let the previous section handles all filtering, sorting, and grouping. In this
// section, we will provide a set of methods which will help us to create the data
// represent the graphical GanttLine and GanttChart as easiest as possible.
//
// Before we can display the data correctly, we have to understand the structure of
// the JOBCKI database table. Actually, this table (JOBCKI) is the core of the application.
// We stores every jobs of every job types in it. There are many linking fields across all
// of these possible job types. For better understanding, we will describe them type by
// type. Remember that JOBCKI.URNO will be always unique.
//
//	JOBPOOL		Basically, this job will be created by dragging staff shifts from the
//				PrePlanTable and drop them into anywhere (except TopScaleText) in the
//				StaffDiagram. (Remember that if the PrePlanTable is opened, the StaffDiagram
//				has to work in the "PrePlan" mode.) However, the PDI operators may drag
//				flight managers and drop them to the GateArea button in GateDiagram or to the
//				CciArea button in CciDiagram also.
//
//				This JobPool is the basic job of the employee. The other jobs have to refer
//				to this job with the field JOBCKI.JOUR.
//
//				This job type could be identified by JCTO == "POL". Normally, its JOUR should
//				be zero. Its ALID should be PoolId. We use the field FLUR for URNO of the
//				assigned shift (from PrePlanTable). We also copy PKNO from the shift to this
//				JobPool also. Its PRID should be blanks. Now, special cases, if the user drag
//				flight managers from PrePlanTable and drop them to the GateArea button in
//				GateDiagram, we will store the GateAreaId into the field PRID. If the user
//				drag flight managers from PrePlanTable and drop them to the CciArea button in
//				the CciDiagram, we will store the CciAreaId into the field PRID. (In this
//				application, we can assume that there is no duplicate of URNO in every
//				tables). For the JobPool which created from the reassignment (JOBDETACH), we
//				will store URNO of the previous job to the field JOUR.
//
//	JOBFLIGHT	Basically, this job will be created by dragging staff shifts from the
//				StaffDiagram and drop them into a FlightBar in the GateDiagram.
//				
//				This job type could be identified by JCTO == "FLT". Its JOUR will be the
//				URNO of its JobPool. Its ALID will be the Gate of that flight. Its FLUR will
//				be the URNO of the associated flight. We also store employee ID into the field
//				PKNO. Its PRID must be blank.
//
//				However, there is a special kind of JobFlight. For the flight arrived in the
//				morning (let's say before the early shift), the user may drag some staff from
//				the PrePlanTable and drop them into	a FlightBar directly. This will create a
//				JobFlight also, but without the	associated JobPool. This special JobFlight will
//				not be displayed in the	StaffDiagram but will be displayed as an indicator in
//				the GateDiagram, and will be displayed in the PrePlanTable also.
//
//				For this special JobFlight, its PRID will be not empty (actually should be
//				"Special"). The field FLUR of these special JobFlight will be the URNO of the
//				associated Shift (not the associated JobPool, since we know nothing about it).
//
//	JOBMANAGER	Basically, this job will be created by dragging a flight from the Gate Detail
//				Window and drop it to the TopScaleText in StaffDiagram or GateDiagram. The
//				prerequisites before this type of job creation is that flight manager must
//				has already a JobGateArea assigned to the gate area for that flight.
//
//				This job type could be identified by JCTO == "FMJ". Its JOUR will be the URNO
//				of its JobGateArea. Its ALID will be the Gate of that flight. Its FLUR will
//				be the URNO of the associated flight. We also store employee ID into the field
//				PKNO. Its PRID must be blank.
//
//	JOBCCI		Basically, this job will be created by dragging staff shifts from the
//				StaffDiagram and drop them into the area of GanttChart in the CciDiagram (may
//				be in the vertical scale or in the GanttLine).
//
//				This job type could be identified by JCTO == "CCI". Its JOUR will be the
//				URNO of its JobPool. Its ALID will be the CCI-Desk which the employee was
//				assigned to. We also store employee ID into the field PKNO.
//
//	JOBGATE		Basically, this job will be created by dragging from the StaffDiagram to the
//				vertical scale in GateDiagram. This will assign the employees who are not
//				flight managers to work at the specified gate. (Remember that if the user does
//				this, we will have to perform automatic assignment -- for resolving demands
//				left on that gate.)
//
//				This job type could be identified by JTCO == "GAT". Its JOUR will be the
//				URNO of its JobPool. Its ALID will be the Gate which the employee was assigned
//				to. We also store employee ID into the field PKNO.
//
//	JOBGATEAREA	Basically, this job will be created by dragging from the StaffDiagram to the
//				GateArea button in GateDiagram. This will assign the flight managers to work
//				at the specified gate area.
//
//				This job type could be identified by JTCO == "GTA". Its JOUR will be the URNO
//				of its JobPool. Its ALID will be the GateArea which the employee was assigned
//				to. We also store employee ID into the field PKNO.
//
//	JOBDETACH	Basically, this job will be created by reassignment some part of the duty bar
//				(JobPool) to one of the other Pools. When this reassignment happened, we will
//				create two jobs, one JobDetach for displaying on the old JobPool, and another
//				JobPool for displaying on the new pool.
//
//				This job type could be identified by JTCO == "DET". Its JOUR will be the URNO
//				of its JobPool. Its ALID will be the new PoolId which the employee was re-
//				-assigned to. Its FLUR will be the URNO of the original shift of that
//				reassignment. We also store employee ID into the field PKNO.
//
//	JOBBREAK	Basically, we can think of all these jobs in the same way. JobBreak will be
//	JOBSPECIAL	used for showing a break of an employee during a day. It will be displayed
//	JOBILLNESS	as a white-red stripes pattern bar in the StaffDiagram. JobSpecial is a general
//				purpose job for displaying jobs which not defined before (such as Meeting or
//				Training). JobIllness is just a special kind of JobSpecial. These jobs will
//				have thier JOUR as the URNO of its JobPool. Its PKNO will be the employee ID.
//				For JobSpecial and JobIllness, we will displayed the text which is saved in
//				the JOBCKI.PRID when displaying the bar.
//
// Methods for creating graphical objects:
// MakeGroups			Create groups that has to be displayed.
// MakeLinesAndManagers	Create lines for all pre-created groups (by scanning thru all JobPool)
// MakeBars				Create bars for all lines in all groups (rescanning -- 2nd round)
// MakePoolJob	Create a line or a manager depending on the employee's rank
// MakeBar				Create a job bar (which is not a JobPool)
//
// GroupText			Return a string displayed in the chart button
// LineText				Return a string displayed in the left scale text
// BarText				Return a string displayed in the bar of the specified job
//
void StaffDiagramViewer::MakeGroups()
{
	CStringArray olGroups;
	CString olGroupType = CViewer::GetGroup();

	if(olGroupType == "Pool")
	{
		if(bmUseAllPools)
		{
			CStringArray olPools;
			ogPolData.GetPoolNames(olPools);
			int ilNumPools = olPools.GetSize();
			for(int ilPool = 0; ilPool < ilNumPools; ilPool++)
			{
				olGroups.Add(olPools[ilPool]);
			}
		}
		else
		{
			GetFilter(CViewer::GetGroup(), olGroups);
		}
	}
	else if(olGroupType == "Team")
	{
		if(bmUseAllTeams)
		{
			ogWgpData.GetAllTeams(olGroups);
		}
		else
		{
			GetFilter(CViewer::GetGroup(), olGroups);
		}
	}
	else if(olGroupType == "Schichtcode")
	{
		if(bmUseAllShiftCodes)
		{
			ogShiftTypes.GetAllShiftTypes(olGroups);
		}
		else
		{
			GetFilter(CViewer::GetGroup(), olGroups);
		}
	}


	// Create a group for each value found in the corresponding filter
	for (int i = 0; i < olGroups.GetSize(); i++)
	{                                            
		STAFF_GROUPDATA rlGroup;
		rlGroup.GroupId = olGroups[i];
		rlGroup.Text = GroupText(rlGroup.GroupId);
		if(ogBasicData.IsPaxServiceT2() == true) //Singapore
		{
			if(rlGroup.Text.Mid(0,2) == Terminal2)
			{
				STAFF_GROUPDATA olGroup;
				olGroup.GroupId = rlGroup.GroupId;
				olGroup.Text = rlGroup.Text;
				olGroup.Text.Replace(Terminal2,Terminal3);

				int ilGroupnoParent = CreateGroup(&rlGroup);
				int ilGroupnoChild  = CreateGroup(&olGroup);

				void* polVoid;
				if(omMapGroupNoToNull.Lookup(ilGroupnoChild,polVoid) == FALSE)
				{
					omMapGroupNoToNull.SetAt(ilGroupnoChild,NULL);
				}

				STAFF_GROUPDATA* prlGroup = GetGroup(ilGroupnoParent);
				STAFF_GROUPDATA* polGroup = GetGroup(ilGroupnoChild);
				
				polGroup->pomParentGroupData = GetGroup(ilGroupnoParent);
				prlGroup->pomChildGroupData  = GetGroup(ilGroupnoChild);

				omGroups.SetAt(ilGroupnoParent,prlGroup);
				omGroups.SetAt(ilGroupnoChild, polGroup);
			}
			else if(rlGroup.Text.IsEmpty() == TRUE)
			{
				continue;
			}
			else
			{
		        CreateGroup(&rlGroup);
			}
		}
		else
		{
			CreateGroup(&rlGroup);
		}
	}

	for (i = 0; i < omGroups.GetSize(); i++)
	{
		STAFF_GROUPDATA* polGroupData = GetGroup(i);
	}
}

void StaffDiagramViewer::MakeLinesAndManagers()
{
	CCSPtrArray <JOBDATA> olPoolJobs;
	CCSPtrArray <JOBDATA> olTempAbsences;

	JOBDATA *prlJob = NULL;
	int ilNumJobs = ogJobData.omData.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		prlJob = &ogJobData.omData[ilJ];
		if(!strcmp(prlJob->Jtco, JOBPOOL))
		{
			olPoolJobs.Add(prlJob);
		}
		else if(!strcmp(prlJob->Jtco, JOBTEMPABSENCE))
		{
			olTempAbsences.Add(prlJob);
		}
	}

	olPoolJobs.Sort(ByJobStartTime);

	RememberCompressedTeams(); // make a list of which teams are currently compressed (ie only team leader displayed)
	DeleteAllTeams();

	omUstfMap.RemoveAll();
	int ilNumPoolJobs = olPoolJobs.GetSize();
	for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
	{
		MakePoolJob(&olPoolJobs[ilPoolJob], false);
	}


	int ilNumGroups = omGroups.GetSize();
	for(int ilG = 0; ilG < ilNumGroups; ilG++)
	{
		if(bmTeamView)
		{
			// it is only after all pool jobs have been added to a team that we
			// know the start and end times of the team - after all teams have
			// been created recalculate the sort string using the start and end times
			int ilNumLines = omGroups[ilG].Lines.GetSize();
			for(int ilL = 0; ilL < ilNumLines; ilL++)
			{
				STAFF_LINEDATA *prlLine = &omGroups[ilG].Lines[ilL];
				prlLine->SortString = MakeSortString(prlLine);
			}
		}
		omGroups[ilG].Lines.Sort(BySortString);
	}

	int ilNumTempAbsences = olTempAbsences.GetSize();
	for(int ilTempAbsence = 0; ilTempAbsence < ilNumTempAbsences; ilTempAbsence++)
	{
		MakeTempAbsence(&olTempAbsences[ilTempAbsence]);
	}

}


void StaffDiagramViewer::MakeBars()
{
	int ilNumGroups = omGroups.GetSize();
	for(int ilG = 0; ilG < ilNumGroups; ilG++)
	{
		int ilNumLines = omGroups[ilG].Lines.GetSize();
		for(int ilL = 0; ilL < ilNumLines; ilL++)
		{
			int ilNumBkBars = omGroups[ilG].Lines[ilL].BkBars.GetSize();
			for(int ilB = 0; ilB < ilNumBkBars; ilB++)
			{
				long ilTmpJobUrno = omGroups[ilG].Lines[ilL].BkBars[ilB].JobUrno;
				CString ilType = omGroups[ilG].Lines[ilL].BkBars[ilB].Type;
				if(omGroups[ilG].Lines[ilL].BkBars[ilB].Type == JOBPOOL)
				{
					MakeBarsForPoolJob(ilG, ilL, omGroups[ilG].Lines[ilL].BkBars[ilB].JobUrno);
				}
			}
		}
	}

	if(bmTeamView)
	{
		// loop through teams, displaying jobs for those teams that are compressed (only the team leader is displayed)
		int ilNumTeams = omTeams.GetSize();
		for(int ilTeam = 0; ilTeam < ilNumTeams; ilTeam++)
		{
			MakeCompressedJobsForTeam(&omTeams[ilTeam]);
		}
	}
}

void StaffDiagramViewer::MakeBkBarsForPoolJob(int ipGroup, int ipLine, long lpPoolJobUrno)
{
	STAFF_LINEDATA *prlLine = GetLine(ipGroup, ipLine);
	if(prlLine == NULL)
		return;

	CCSPtrArray <JOBDATA> olJobs;
	ogJobData.GetJobsByJour(olJobs, lpPoolJobUrno);
	for(int i = 0; i < olJobs.GetSize(); i++)
	{
	
	
		
	
		JOBDATA *prlJob = &olJobs[i];
		if(!strcmp(prlJob->Jtco,JOBTEMPABSENCE))
			MakeTempAbsenceBkBar(prlLine, prlJob);
	}
	prlLine->BkBars.Sort(ByType);
}

void StaffDiagramViewer::MakeBarsForPoolJob(int ipGroup, int ipLine, long lpPoolJobUrno)
{
	if(!bmTeamView || !IsTeamCompressed(lpPoolJobUrno))
	{
		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetJobsByJour(olJobs, lpPoolJobUrno);
		int ilNumJobs = olJobs.GetSize();
		for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
		{
			JOBDATA *prlJob = &olJobs[ilJ];
			if (!strcmp(prlJob->Jtco, JOBPOOL) || !strcmp(prlJob->Jtco, JOBTEMPABSENCE) || !strcmp(prlJob->Jtco, JOBEQUIPMENTFASTLINK))
				continue;

			MakeBar(ipGroup, ipLine, prlJob);
		}
	}
}

bool StaffDiagramViewer::IsPassPoolJobFilter(JOBDATA *prpPoolJob)
{
	if(prpPoolJob == NULL)
		return false;

	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpPoolJob->Shur);
	if(prlShift == NULL)
		return false;

	CString olTeamName = ogBasicData.GetTeamForPoolJob(prpPoolJob);
	CString olFunction = ogBasicData.GetFunctionForPoolJob(prpPoolJob);
	CStringArray olPermits;
	ogBasicData.GetPermitsForPoolJob(prpPoolJob,olPermits);

	return IsPassPoolJobFilter(prpPoolJob, prlShift, ogEmpData.GetEmpByUrno(prlShift->Stfu), olTeamName, olFunction, olPermits);
}

bool StaffDiagramViewer::IsPassPoolJobFilter(JOBDATA *prpPoolJob, SHIFTDATA *prpShift, EMPDATA *prpEmp, 
											 CString &ropTeamName, CString &ropFunction, CStringArray &ropPermits)
{
	if(prpPoolJob == NULL || strcmp(prpPoolJob->Jtco, JOBPOOL))
		return false;

	if(prpShift == NULL)
		return false;

	if(prpEmp == NULL)
		return false;

	if(!IsInTimeframe(prpPoolJob))
		return false;

	if(bmHideCompletelyDelegatedTeams && IsCompletelyDelegatedPoolJob(prpPoolJob))
		return false;

	if(bmHideAbsentEmps && ogJobData.IsAbsent(prpPoolJob))
		return false;

	if(bmHideStandbyEmps && ogJobData.IsStandby(prpPoolJob))
		return false;

	if(bmHideFidEmps && ogJobData.PoolJobHasFidJobs(prpPoolJob->Urno))
		return false;

	// don't display a student that is part of a double tour
	if(ogDrsData.IsStudent(prpShift->Urno))
		return false;

	void *p;
	BOOL blIsPoolOk = bmUseAllPools || omCMapForPool.Lookup(prpPoolJob->Alid, p);
	BOOL blIsTeamOk = bmUseAllTeams || (ropTeamName.IsEmpty() && bmEmptyTeamSelected) || omCMapForTeam.Lookup(ropTeamName, p);
	BOOL blIsShiftCodeOk = bmUseAllShiftCodes || omCMapForShiftCode.Lookup(prpShift->Sfca, p);
	BOOL blIsRankOk = bmUseAllRanks || omCMapForRank.Lookup(ropFunction, p);
	BOOL blIsPermitOk = bmUseAllPermits;
	int ilNumPermits = ropPermits.GetSize();
	for(int ilPermit = 0; !blIsPermitOk && ilPermit < ilNumPermits; ilPermit++)
		blIsPermitOk = omCMapForPermits.Lookup(ropPermits[ilPermit], p);

	if(ogShiftData.IsAbsent(prpShift))
		blIsShiftCodeOk = TRUE;

	return (blIsPoolOk && blIsTeamOk && blIsShiftCodeOk && blIsRankOk && blIsPermitOk);
}

// return true if new line is created
bool StaffDiagramViewer::MakePoolJob(JOBDATA *prpPoolJob, bool bpSort /* = true*/)
{
	bool blNewLineCreated = false;

	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpPoolJob->Shur);
	if(prlShift == NULL)
		return blNewLineCreated;

	EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlShift->Stfu);
	CString olTeamName = ogBasicData.GetTeamForPoolJob(prpPoolJob);
	CString olFunction = ogBasicData.GetFunctionForPoolJob(prpPoolJob);
	CStringArray olPermits;
	ogBasicData.GetPermitsForPoolJob(prpPoolJob,olPermits);

	if(!IsPassPoolJobFilter(prpPoolJob, prlShift, prlEmp, olTeamName, olFunction, olPermits))
		return blNewLineCreated;
	
	int ilGroupno = FindGroup(prpPoolJob->Alid, olTeamName, prlShift->Sfca);
	if(ilGroupno == -1)
		return blNewLineCreated;

	ogBasicData.GetFunctionForPoolJob(prpPoolJob);

	if(ogBasicData.IsManager(olFunction))
	{
		// Create a new manager in found group
		STAFF_MANAGERDATA rlManager;
		rlManager.JobUrno = prpPoolJob->Urno;

		rlManager.ShiftStid = prlShift != NULL ? prlShift->Sfca : "";
		rlManager.ShiftTmid = prlShift != NULL ? olTeamName : "";
		rlManager.EmpLnam = CString(prlEmp != NULL ? prlEmp->Lanm : "");
		if (prlShift != NULL)
		{
			if ((prlShift->Avfa < prpPoolJob->Acfr) || (prlShift->Avta > prpPoolJob->Acto))
			{
				rlManager.ShiftStid += "'";
			}
		}
		CreateManager(ilGroupno, &rlManager);
	}
	else
	{
		// check if a line already exists for this pool job (employees with Abordnungen can have several pool jobs)
		STAFF_LINEDATA *prlLine = NULL;
		bool blNotFound = true;

		void *pvlDummy;
		if(omUstfMap.Lookup((void *)prpPoolJob->Ustf, (void *&) pvlDummy))
		{
			for(int ilLineno = 0; ilLineno < GetLineCount(ilGroupno); ilLineno++)
			{
				prlLine = GetLine(ilGroupno, ilLineno);
				if(prlLine->StfUrno == prpPoolJob->Ustf && prlLine->ShiftTmid == olTeamName &&
					IsOverlapped(prpPoolJob->Acfr, prpPoolJob->Acto, prlLine->ShiftAcfr, prlLine->ShiftActo))
				{
					blNotFound = false;
					break;
				}
			}
			if(blNotFound == true)
			{
				if(ogBasicData.IsPaxServiceT2() == true && GetGroup(ilGroupno) != NULL 
					&& GetGroup(ilGroupno)->pomChildGroupData != NULL)
				{					
					if(GetGroup(ilGroupno + 1) != NULL)
					{
						ilGroupno++;
						for(int ilLineno = 0; ilLineno < GetLineCount(ilGroupno); ilLineno++)
						{
							prlLine = GetLine(ilGroupno, ilLineno);
							if(prlLine->StfUrno == prpPoolJob->Ustf && prlLine->ShiftTmid == olTeamName &&
								IsOverlapped(prpPoolJob->Acfr, prpPoolJob->Acto, prlLine->ShiftAcfr, prlLine->ShiftActo))
							{
								blNotFound = false;
								break;
							}
						}
					}
				}
			}
		}
		if(blNotFound)
		{
            //omUstfMap.SetAt((void *) prpPoolJob, NULL);
			omUstfMap.SetAt((void *) prpPoolJob->Ustf, NULL);
		}

		if(bmTeamView)
		{
			// add information about the team
			AddToTeam(prpPoolJob, olTeamName);
		}

		if(bmTeamView && IsTeamCompressed(prpPoolJob->Urno))
		{
			// if this is a team view and the team is compressed then make a compressed pool job
			if(AddCompressedPoolJobToTeam(prpPoolJob) != NULL)
			{
				blNewLineCreated = true;
			}
		}
		else if(blNotFound)
		{
			// create a new line for the pool job
			STAFF_LINEDATA *prlNewLine = new STAFF_LINEDATA;
			prlNewLine->JobUrno = prpPoolJob->Urno;
			prlNewLine->StfUrno = prpPoolJob->Ustf;
			prlNewLine->ShiftAcfr = prlShift->Avfa;
			prlNewLine->ShiftActo = prlShift->Avta;
			prlNewLine->ShiftStid = prlShift->Sfca;
			prlNewLine->ShiftTmid = olTeamName;
			prlNewLine->EmpName = ogEmpData.GetEmpName(prlEmp);
			prlNewLine->EmpRank = olFunction;
			prlNewLine->GroupFunctionWeight = ogBasicData.GetFunctionWeightWithinWorkgroup(prpPoolJob);

			CString olColour;
			if(ogDlgSettings.GetValue("FUNCTIONCOLOURS", olFunction, olColour))
			{
				prlNewLine->TextColor = (COLORREF) atol(olColour);
				prlNewLine->TextColorDefined = true;
			}

			// if this emp has a student accompanying him - get the student name
			EMPDATA *prlStudent = ogEmpData.GetEmpByUrno(ogDrsData.GetStudentForTeacher(prlShift->Urno));
			if(prlStudent != NULL)
			{
				prlNewLine->DoubleTourStudent.Format("%s %s",GetString(IDS_DOUBLETOURSTUDENT),ogEmpData.GetEmpName(prlStudent));
			}

			prlNewLine->Text = LineText(prpPoolJob, prlShift, prlEmp);
			prlNewLine->StatusText = prpPoolJob->Text;
			prlNewLine->MultiFunctions = GetString(IDS_MULTIFUNC) + ogBasicData.GetMultiFunctionText(prlEmp->Urno);
			MakeBkBars(prlNewLine, prpPoolJob, prlShift);
			prlNewLine->BkBars.Sort(ByType);

			if(CreateLine(ilGroupno, prlNewLine, bpSort) != NULL)
			{
				blNewLineCreated = true;
			}
		}
		else if(prlLine != NULL)
		{
			// there is an existing line for this pool job (employees with Abordnungen can have several pool jobs)
			MakeBkBars(prlLine, prpPoolJob, prlShift);
			prlLine->BkBars.Sort(ByType);
		}

	}

	return blNewLineCreated;
}

COLORREF StaffDiagramViewer::GetTextColour(int ipGroupno, int ipLineno)
{
	COLORREF ilColour = BLACK;
	if(ipGroupno >= 0 && ipGroupno < omGroups.GetSize() && ipLineno >= 0 && ipLineno < omGroups[ipGroupno].Lines.GetSize())
	{
		bool blIsTeamLeader = IsTeamLeader(omGroups[ipGroupno].Lines[ipLineno].JobUrno);
		if(blIsTeamLeader && bmTeamLeaderColourDefined)
		{
			ilColour = imTeamLeaderColour;
		}
		else if(omGroups[ipGroupno].Lines[ipLineno].TextColorDefined)
		{
			ilColour = omGroups[ipGroupno].Lines[ipLineno].TextColor;
		}
		else if(blIsTeamLeader)
		{
			ilColour = BLUE;
		}
	}
	return ilColour;
}

void StaffDiagramViewer::MakeCompressedBkBars(STAFF_LINEDATA *prpLine, COMPRESSED_JOBDATA *prpCompressedJob, SHIFTDATA *prpShift)
{
	JOBDATA *prlJob = &prpCompressedJob->Data;

	STAFF_BKBARDATA *prlBkBar = MakeBkBars(prpLine, prlJob, prpShift);

	prlBkBar->OfflineBar = false;
	if(!bgOnline)
	{
		int ilNumJobs = prpCompressedJob->OriginalJobs.GetSize();
		for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
		{
			prlJob = &prpCompressedJob->OriginalJobs[ilJ];
			if(ogJobData.JobChangedOffline(prlJob) || ogJodData.JodChangedOffline(prlJob->Urno))
			{
				prlBkBar->OfflineBar = true;
				break;
			}
		}
	}
}

STAFF_BKBARDATA *StaffDiagramViewer::MakeBkBars(STAFF_LINEDATA *prpLine, JOBDATA *prpJob, SHIFTDATA *prpShift)
{
	STAFF_BKBARDATA *prlBkBar = new STAFF_BKBARDATA;

	CString olEquipmentFastLinkText, olEquipmentFastLinkStatusText, olTmp;
	if(ogBasicData.bmDisplayEquipment)
	{
		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetEquipmentFastLinkJobsByPoolJob(olJobs,prpJob->Urno);
		int ilNumJobs = olJobs.GetSize();
		for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
		{
			JOBDATA *prlJob = &olJobs[ilJ];
			EQUDATA *prlEqu = ogEquData.GetEquByUrno(prlJob->Uequ);
			JTYDATA *prlJty = ogJtyData.GetJtyByUrno(prlJob->Ujty);
			if(prlEqu != NULL && prlJty != NULL)
			{
				olTmp.Format("%s: %s %s-%s",prlJty->Dscr,prlEqu->Enam,prpJob->Acfr.Format("%H%M"),prpJob->Acto.Format("%H%M"));
				if(olEquipmentFastLinkText.IsEmpty())
				{
					olEquipmentFastLinkText = prlEqu->Enam;
					olEquipmentFastLinkStatusText = olTmp;
				}
				else
				{
					olEquipmentFastLinkText += CString(", ") + prlEqu->Enam;
					olEquipmentFastLinkStatusText += CString(", ") + olTmp;
				}
			}
		}
	}

	prlBkBar->Text = olEquipmentFastLinkText;
	prlBkBar->TextColor = WHITE;
	prlBkBar->Type = prpJob->Jtco;
	prlBkBar->StartTime = prpJob->Acfr;
	prlBkBar->EndTime = prpJob->Acto;
	prlBkBar->FrameType = FRAMERECT;
	prlBkBar->FrameColor = (*prpJob->Ignr == '1') ? WHITE : BLACK;
	prlBkBar->MarkerBrush = NULL;
	prlBkBar->JobUrno = prpJob->Urno;
	prlBkBar->StatusText = ogDataSet.JobBarText(prpJob) + CString("  ") + StatusText(prpJob) + CString(" ") + olEquipmentFastLinkStatusText;
	prlBkBar->OfflineBar = (!bgOnline && (ogJobData.JobChangedOffline(prpJob) || ogJodData.JodChangedOffline(prpJob->Urno))) ? true : false;

	CBrush *polPausenlageBrush = NULL;
	CBrush *polPoolJobBrush = NULL;

	if (prlBkBar->MarkerBrush == NULL)
	{
		if(prpJob->Infm)
		{
			prlBkBar->MarkerBrush = &omAbsenceBkBrush;
			polPausenlageBrush = &omAbsenceBkPausenlageBrush;
		}
		else if(*prpJob->Stat == 'C')
		{
			prlBkBar->MarkerBrush = &omConfirmedBkBrush;
			polPausenlageBrush = &omConfirmedBkPausenlageBrush;
		}
		else if(*prpJob->Stat == 'F')
		{
			prlBkBar->MarkerBrush = &omFinishedBkBrush;
			polPausenlageBrush = &omFinishedBkPausenlageBrush;
		}
		else
		{
			prlBkBar->MarkerBrush = &omBkBrush;
			polPausenlageBrush = &omBkPausenlageBrush;
		}
	}

	prpLine->BkBars.Add(prlBkBar);

	if(polPausenlageBrush != NULL && prpShift->Sbfr != TIMENULL && prpShift->Sbto != TIMENULL)
	{
		// draw the Pausenlage for if the Pausenlage is within the current pool job
		if(IsOverlapped(prpShift->Sbfr, prpShift->Sbto, prpJob->Acfr, prpJob->Acto))
		{
			STAFF_BKBARDATA *prlPausenlageBar = new STAFF_BKBARDATA;
			*prlPausenlageBar = *prlBkBar; // init

			prlPausenlageBar->Text.Empty();
			prlPausenlageBar->MarkerBrush = polPausenlageBrush;
			prlPausenlageBar->Type = JOBBREAK;
			prlPausenlageBar->StartTime = (prpShift->Sbfr > prpJob->Acfr) ? prpShift->Sbfr : prpJob->Acfr;
			prlPausenlageBar->EndTime = (prpShift->Sbto < prpJob->Acto) ? prpShift->Sbto : prpJob->Acto;
			prlPausenlageBar->OfflineBar = false;
			prpLine->BkBars.Add(prlPausenlageBar);
		}
	}
	
	return prlBkBar;
}


bool StaffDiagramViewer::IsNearlyWithIn(CTime opStart1, CTime opEnd1, CTime opStart2, CTime opEnd2)
{
	CTimeSpan olOneMinute(0,0,1,0);
	return (opStart1 >= (opStart2-olOneMinute) && opEnd1 <= (opEnd2+olOneMinute));
}

bool StaffDiagramViewer::IsNotShiftDelegation(JOBDATA *prpJob)
{
	return (prpJob != NULL && prpJob->Udel == 0L);
}

bool StaffDiagramViewer::PausenlageNotAlreadyDrawn(STAFF_LINEDATA *prpLine)
{
	bool blPausenlageNotAlreadyDrawn = true;
	if(prpLine != NULL)
	{
		int ilNumBkBars = prpLine->BkBars.GetSize();
		for(int ilBkBar = 0; blPausenlageNotAlreadyDrawn && ilBkBar < ilNumBkBars; ilBkBar++)
		{
			if(prpLine->BkBars[ilBkBar].Type == JOBBREAK)
			{
				blPausenlageNotAlreadyDrawn = false;
			}
		}
	}

	return blPausenlageNotAlreadyDrawn;
}

void StaffDiagramViewer::MakeTempAbsence(JOBDATA *prpJob)
{
	if(prpJob == NULL || strcmp(prpJob->Jtco, JOBTEMPABSENCE) != 0)
		return;

//	SHIFTDATA *prlShift;
//	EMPDATA *prlEmp;
//	if ((prlShift = ogShiftData.GetShiftByUrno(prpJob->Shur)) == NULL ||
//		(prlEmp = ogEmpData.GetEmpByPeno(prlShift->Peno)) == NULL ||
//		!IsPassFilter(prpJob->Alid, prlShift->Egrp, prlShift->Sfca, prlShift->Efct,ogShiftData.IsAbsent(prlShift)) ||
//		!IsOverlapped(prpJob->Acfr, prpJob->Acto, omStartTime, omEndTime))
//		return;
//
//	int ilGroupno = FindGroup(prpJob->Alid, prlShift->Egrp, prlShift->Sfca);
//	if(ilGroupno == -1)
//	{
//		// no group found for the pool job's Alid
//		ogBasicData.Trace("StaffDiagramViewer::MakeTempAbsence() TABS Job contains unknown Alid <%s>\n",prpJob->Alid);
//		return;
//	}
//
//	BOOL blIsFlightManager = ogBasicData.IsManager(prlShift->Efct);
//	if (blIsFlightManager)
//	{
//		// Create a new manager in found group
//		STAFF_MANAGERDATA rlManager;
//		rlManager.JobUrno = prpJob->Urno;
//
//		rlManager.ShiftStid = prlShift != NULL ? prlShift->Sfca : "";
//		rlManager.ShiftTmid = prlShift != NULL ? prlShift->Egrp : "";
//		rlManager.EmpLnam = CString(prlEmp != NULL ? prlEmp->Lanm : "");
//		if (prlShift != NULL)
//		{
//			if ((prlShift->Avfa < prpJob->Acfr) || (prlShift->Avta > prpJob->Acto))
//			{
//				rlManager.ShiftStid += "'";
//			}
//		}
//		CreateManager(ilGroupno, &rlManager);
//	}
//	else


	STAFF_LINEDATA *prlLine;
	bool blNotFound = true;
	for(int ilGroupno = 0; blNotFound && ilGroupno < GetGroupCount(); ilGroupno++)
	{
		for(int ilLineno = 0; blNotFound && ilLineno < GetLineCount(ilGroupno); ilLineno++)
		{
			prlLine = GetLine(ilGroupno, ilLineno);

//			 if(prlLine->StfUrno == prpJob->Ustf)
			if(prlLine->StfUrno == prpJob->Ustf && IsOverlapped(prpJob->Acfr, prpJob->Acto, prlLine->ShiftAcfr, prlLine->ShiftActo))
			{
			// QDOhere
				
			    MakeTempAbsenceBkBar(prlLine, prpJob);
				blNotFound = false;
			}
		}
	}
}

void StaffDiagramViewer::MakeTempAbsenceBkBar(STAFF_LINEDATA *prpLine, JOBDATA *prpJob)
{

	
	
	if(prpLine != NULL && prpJob != NULL)
	{
		int ilBkBarCount = prpLine->BkBars.GetSize();

		STAFF_BKBARDATA rlBkBar;
		rlBkBar.Text = "Absence Code Not Found";
		CString olCode;




		DRADATA *prlDra = NULL;
		DELDATA *prlDel = NULL;
		if((prlDra = ogDraData.GetDraByUrno(prpJob->Shur)) != NULL)
		{
			olCode = prlDra->Sdac;
		}
		else if((prlDel = ogDelData.GetDelByUrno(prpJob->Udel)) != NULL)
		{
			olCode = prlDel->Sdac;
		}

		ODADATA *prlOda = ogOdaData.GetOdaByType(olCode);
		if(prlOda != NULL)
		{
			rlBkBar.Text = prlOda->Sdan;
		}

		rlBkBar.Type = prpJob->Jtco;
		rlBkBar.StartTime = (prpLine->ShiftAcfr > prpJob->Acfr) ? prpLine->ShiftAcfr : prpJob->Acfr;
		rlBkBar.EndTime = (prpLine->ShiftActo < prpJob->Acto) ? prpLine->ShiftActo : prpJob->Acto;
		rlBkBar.FrameType = FRAMERECT;
		rlBkBar.JobUrno = prpJob->Urno;
		rlBkBar.MarkerBrush = &omTempAbsenceBrush;
		prpLine->BkBars.NewAt(ilBkBarCount++, rlBkBar);
	}
}

void StaffDiagramViewer::MakeBarData(STAFF_BARDATA *prlBar, JOBDATA *prpJob)
{
	prlBar->JobUrno = prpJob->Urno;
	if(!strcmp(prpJob->Jtco, JOBBREAK))
	{
		prlBar->Text = "";
		prlBar->StatusText = ogDataSet.JobBarText(prpJob) + CString(" ") + StatusText(prpJob);
		prlBar->IsPause = true;
		prlBar->PauseColour = ogColors[ogConflicts.GetJobConflictColor(prpJob)];
		prlBar->MarkerBrush = ogBrushs[21]; // break bitmap whose colour depends on prlBar->PauseColour
	}
	else
	{
		CString olText = "";
		if (bgIsPrm)
		{
			DPXDATA *prlDpx = ogDpxData.GetDpxByUrno(prpJob->Uprm);
			if (prlDpx != NULL)
			{
				if (bgPrmShowFlightNumber)
				{
					olText = prlDpx->FlnuFlno + CString("-") + prlDpx->TfluFlno + CString("(")+prlDpx->Prmt+CString(")");
					prlBar->Text = olText;
					prlBar->StatusText = prlBar->Text + CString("  ") + prpJob->Acfr.Format("%H%M")  + "-" + prpJob->Acto.Format("%H%M");
					prlBar->StatusText = prlBar->StatusText + CString("  ") + prlDpx->Name + CString("  ") + prlDpx->Seat;
				}
				else
				{
					olText = prlDpx->Name;
					prlBar->Text = olText;
					prlBar->StatusText = prlBar->Text + CString(" ") + StatusText(prpJob);
				}
			}
			else
			{
				if(olText.IsEmpty())
					olText = ogDataSet.JobBarText(prpJob);
				prlBar->Text = olText;
				prlBar->StatusText = prlBar->Text + CString(" ") + StatusText(prpJob);
				if (bgPrmShowFlightNumber)
					prlBar->StatusText = prlBar->StatusText + CString("  ") + CString("     ") + CString("-") + CString("     ");
			}
		}
		else
		{

			if(strcmp(prpJob->Jtco,JOBDELEGATEDFLIGHT) == 0)
				olText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_DFJ, "%s%s", "", ogDataSet.GetFlightJobBarText(prpJob));
			else if(strcmp(prpJob->Jtco,JOBRESTRICTEDFLIGHT) == 0)
				olText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_RFJ, "%s%s", "", ogDataSet.GetFlightJobBarText(prpJob));

			if(IsPrivateProfileOn("SHOW_STAFF_MARKER_FILTER","NO"))
			{
				FLIGHTDATA *prlFlt = ogFlightData.GetFlightByUrno(prpJob->Flur);
				if (prlFlt != NULL)
				{
					prlBar->FlightUrno = prlFlt->Urno;
					if(ogJobData.IsTurnaroundJob(prpJob))
					{
						prlBar->FlightType = STFBAR_TURNAROUND;						
					}
					else if(strcmp((char*)prlFlt->Adid, "A") == 0)
					{
						prlBar->FlightType = STFBAR_ARRIVAL;
					}
					else if (strcmp((char*)prlFlt->Adid, "D") == 0)
					{
						prlBar->FlightType = STFBAR_DEPARTURE;
					}	
				}
			}


			if(olText.IsEmpty())
				olText = ogDataSet.JobBarText(prpJob);
			prlBar->Text = olText;
			prlBar->StatusText = prlBar->Text + CString(" ") + StatusText(prpJob);
		}  
		prlBar->MarkerBrush = ogBrushs[ogConflicts.GetJobConflictColor(prpJob)];
	}
	prlBar->StartTime = prpJob->Acfr;
	prlBar->EndTime = prpJob->Acto;
	prlBar->FrameType = FRAMERECT;
	if(strcmp(prpJob->Jtco,JOBDELEGATEDFLIGHT) == 0)
	{
		prlBar->FrameColor = ORANGE;
	}
	else
	{
		prlBar->FrameColor = (*prpJob->Ignr == '1') ? WHITE : BLACK;
	}
	prlBar->MarkerType = (prpJob->Stat[0] == 'P')? MARKLEFT: MARKFULL;
	prlBar->OfflineBar = (!bgOnline && (ogJobData.JobChangedOffline(prpJob) || ogJodData.JodChangedOffline(prpJob->Urno))) ? true : false;
	prlBar->Infm = (prpJob->Stat[0] == 'P' && prpJob->Infm) ? true : false;
	prlBar->Ackn = prpJob->Ackn;
	prlBar->DifferentFunction = ogBasicData.JobAndDemHaveDiffFuncs(prpJob);
	
}


void StaffDiagramViewer::MakeBar(int ipGroupno, int ipLineno, JOBDATA *prpJob)
{
	STAFF_BARDATA rlBar;

	MakeBarData(&rlBar,prpJob);
	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);

	// We also have to create an indicator for showing the range of time on TimeScale
	STAFF_INDICATORDATA rlIndicator;
	rlIndicator.StartTime = rlBar.StartTime;
	rlIndicator.EndTime = rlBar.EndTime;
	//rlIndicator.Color = ogColors[prpJob->ColorIndex];
	rlIndicator.Color = ogColors[ogConflicts.GetJobConflictColor(prpJob)];
	CreateIndicator(ipGroupno, ipLineno, ilBarno, &rlIndicator);	
}

CString StaffDiagramViewer::GroupText(const char *pcpTeamId)
{
	ALLOCUNIT *prlPoolGroup;
	switch (omGroupBy)
	{
	case GROUP_BY_POOL:
		if((prlPoolGroup = ogAllocData.GetGroupByName(ALLOCUNITTYPE_POOLGROUP,pcpTeamId)) != NULL)
			return prlPoolGroup->Name;
//		const METAALLOCDATA *prlMetaAlloc;
//		if ((prlMetaAlloc = ogPools.GetMetaAllocUnit(pcpTeamId)) != NULL)
//			return prlMetaAlloc->Alfn;
	case GROUP_BY_TEAM:
		return pcpTeamId;	// no Team lookup implementation, just return Team ID
		break;
	case GROUP_BY_SHIFTCODE:
		return pcpTeamId;	// no ShiftCode lookup implementation, just return Team ID
		break;
	}

	return "";	// error, just simply return an empty string
}

CString StaffDiagramViewer::LineText(JOBDATA *prpJob, SHIFTDATA *prpShift, EMPDATA *prpEmp)
{
	char clPDA[24] = "  ";

	CString olFunction = ogBasicData.GetFunctionForPoolJob(prpJob);
	CString olText;
	if (bgIsPrm)
	{
		PDADATA *prlPda = ogPdaData.GetPdaByUstf(prpShift->Stfu);
		if (prlPda != NULL)
		{
			strcpy(clPDA,"P ");
		}
	}

	char clBay[6] = " ";
	if(IsPrivateProfileOn("ONE_GLANCE_DEMO","NO"))
	{
		
		CString randBay;
		if(prpJob != NULL)
		{
			CTime olLocalCurrTime = CTime::GetCurrentTime();
			CString temp = olLocalCurrTime.Format("%Y%m%d%H%M%S");
			CString temp1 = prpJob->Acfr.Format("%Y%m%d%H%M%S");
			CString temp2 = prpJob->Acto.Format("%Y%m%d%H%M%S");

			if(olLocalCurrTime > prpJob->Acfr && olLocalCurrTime < prpJob->Acto)
			{
				if(prpJob->Stat[0] == 'C')
				{
					//strcpy(clBay, prpJob->Posi);

					//if(prpJob->Posi[0] == ' ')
					{
						CCSPtrArray<JOBDATA> olStfJobs;
						
						ogJobData.GetJobsByUstf(olStfJobs,prpJob->Ustf);
						

						int ilNumStfJobs = olStfJobs.GetSize();
						
						CString tempUrnos ;
						CTimeSpan ilNewNearestTime(0);
						CTimeSpan ilOldNearestTime(1,0,0,0);

						for(int ilStfJob = 0; ilStfJob < ilNumStfJobs; ilStfJob++)
						{
							JOBDATA *prlJob = &olStfJobs[ilStfJob];
							 
							if(prlJob->Flur > 0)
							{
								ilNewNearestTime = olLocalCurrTime - prlJob->Acfr;
									
								if(olLocalCurrTime > prlJob->Acfr  && ilNewNearestTime < ilOldNearestTime)
								{
									ilOldNearestTime = ilNewNearestTime;
									strcpy(clBay, prlJob->Alid);
									/*
									FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlJob->Flur);
									if( prlFlight !=NULL)
									{
										if(prlFlight->Adid[0] == 'A')
										{
											strcpy(clBay, prlFlight->Psta);
										}
										else
										{
											strcpy(clBay, prlFlight->Pstd);
										}
									}
									*/
								}

							}
							
						}
						
					}

					if(clBay[0] == ' ' || strlen(clBay) == 0)
					{					
						randBay.Format("A%d", (rand() % 10 + 1));
						strcpy(clBay, randBay);
					}
				}
			
			}		
		}
	}
		
	


	olText.Format("%s %s %s %s %s %s %s", clPDA, clBay, ogBasicData.GetTeamForPoolJob(prpJob), prpShift->Sfca,
		olFunction, ogEmpData.GetEmpName(prpEmp),  prpJob->Text);
    //olText.TrimLeft();
	return olText;
}


CString StaffDiagramViewer::BarText(JOBDATA *prpJob)
{
	CString olBarText;

// Id 19-Sep-96
	if (strcmp(prpJob->Jtco, JOBFLIGHT) == 0)
// end of Id 19-Sep-96
	{
		FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prpJob);
		if (prlFlight != NULL)
		{
			CString olAlidText = prpJob->Alid;
			if(!strcmp(prpJob->Aloc,ALLOCUNITTYPE_GATE) && strcmp(prpJob->Alid,prpJob->Gate))
			{
				olAlidText = CString(prpJob->Gate) + " -> " + CString(prpJob->Alid);
			}
			else if(!strcmp(prpJob->Aloc,ALLOCUNITTYPE_PST) && strcmp(prpJob->Alid,prpJob->Posi))
			{
				olAlidText = CString(prpJob->Posi) + " -> " + CString(prpJob->Alid);
			}
			olBarText.Format("%s, %s: %s",prlFlight->Fnum,prpJob->Aloc,olAlidText);
		}
		else
		{
			char pclBuf[512];
			sprintf(pclBuf, GetString(IDS_STRING33140), ogBasicData.GetFlightUrnoForJob(prpJob));
			olBarText = CString(pclBuf);
		}
	}
   	else if (strcmp(prpJob->Jtco, JOBCCI) == 0)
	{
		olBarText = GetString(IDS_STRING61651) + ", " + prpJob->Alid;
	}
	else if (strcmp(prpJob->Jtco, JOBGATE) == 0)
	{
		//olBarText =  GetString(IDS_STRING61644) + ", " + prpJob->Alid;
		olBarText = prpJob->Alid;
	}
	else if (strcmp(prpJob->Jtco, JOBGATEAREA) == 0)
	{
		ALLOCUNIT *prlGateArea = ogAllocData.GetGroupByName(ALLOCUNITTYPE_GATEGROUP,prpJob->Alid);
		if(prlGateArea != NULL)
			return CString(prlGateArea->Name);
	}
	else if (strcmp(prpJob->Jtco, JOBDETACH) == 0)
	{
		//"Abgeordnet nach: "
		olBarText = GetString(IDS_STRING61653) + CString(prpJob->Alid);
	}
	else if (strcmp(prpJob->Jtco, JOBTEAMDELEGATION) == 0)
	{
		CString olGroup, olFunction;
		DLGDATA *prlDlg = ogDlgData.GetDlgByUjob(prpJob->Urno);
		if(prlDlg != NULL)
		{
			olGroup = prlDlg->Wgpc;
			olFunction = prlDlg->Fctc;
		}
		//"Abgeordnet nach: "
		olBarText.Format("%s %s/%s/%s",GetString(IDS_STRING61653),olGroup,prpJob->Alid,olFunction);
	}
	else if (strcmp(prpJob->Jtco, JOBSPECIAL) == 0 || strcmp(prpJob->Jtco, JOBILLNESS) == 0)
	{
		olBarText = CString(prpJob->Text);
	}
	else if (strcmp(prpJob->Jtco, JOBSTAIRCASE) == 0 )
	{
		olBarText =  GetString(IDS_STRING61646) + ", " + prpJob->Alid;
	}

	return olBarText;
}

CString StaffDiagramViewer::StatusText(JOBDATA *prpJob)
{
	CString olStatusText;
	SHIFTDATA *prlShift = NULL;
	if (prpJob != NULL)
	{
		prlShift = ogShiftData.GetShiftByUrno(prpJob->Shur);
	}

	if(prlShift != NULL)
	{
		olStatusText = CString("  ") + prlShift->Sfca;
		olStatusText += CString("  ") + prpJob->Acfr.Format("%H%M")  + " - " + prpJob->Acto.Format("%H%M");
		if(!strcmp(prpJob->Jtco,JOBPOOL))
		{
			// "Func: "
			olStatusText += CString("  ") + GetString(IDS_FUNCTIONTEXT) + ogBasicData.GetFunctionForPoolJob(prpJob);
			// "Org: "
			//olStatusText += CString("  ") + GetString(IDS_ORGTEXT) + ogBasicData.GetOrgForPoolJob(prpJob);
		}
	}
	return olStatusText;

}

int StaffDiagramViewer::FindGroup(const char *pcpPoolId, const char *pcpTeamId,
	const char *pcpShiftCode)
{
	bool blIsWithoutGroup = false;
	CString olWithoutGroupName = GetString(IDS_NOWORKGROUP);

	// Prepare the group ID value corresponding to the current groupping
	CString olTeamId;
	switch (omGroupBy)
	{
	case GROUP_BY_POOL:
		olTeamId = pcpPoolId;
		break;
	case GROUP_BY_TEAM:
		olTeamId = pcpTeamId;
		if(olTeamId.IsEmpty())
			blIsWithoutGroup = true;
		break;
	case GROUP_BY_SHIFTCODE:
		olTeamId = pcpShiftCode;
	}

	// Search for the given group code, return group index if found
	CString olGroupName;
	int n = GetGroupCount();
	for (int i = 0; i < n; i++)
	{
		if(ogBasicData.IsPaxServiceT2() == true) //Singapore
		{	
		olGroupName = GetGroup(i)->GroupId;
			if(GetGroup(i)->pomParentGroupData != NULL)
			{
				olGroupName = GetGroup(i)->pomParentGroupData->GroupId;
		       if(olTeamId == olGroupName || (blIsWithoutGroup && olGroupName == olWithoutGroupName))
			       return i;
			}
			else if(olTeamId == olGroupName || (blIsWithoutGroup && olGroupName == olWithoutGroupName))
			{
				return i;
			}
		}
		else
		{
			olGroupName = GetGroup(i)->GroupId;
			if(olTeamId == olGroupName || (blIsWithoutGroup && olGroupName == olWithoutGroupName))
				return i;
		}
	}
	return -1;
}

BOOL StaffDiagramViewer::FindManager(long lpUrno, int &ripGroupno, int &ripManagerno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		for (ripManagerno = 0; ripManagerno < GetManagerCount(ripGroupno); ripManagerno++)
			if (GetManager(ripGroupno, ripManagerno)->JobUrno == lpUrno)
				return TRUE;

	return FALSE;
}

BOOL StaffDiagramViewer::FindDutyBar(long lpUrno, int &ripGroupno, int &ripLineno, bool bpIsPause /*false*/, bool bpSearchWithinTeam /*false*/)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
	{
		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
		{
			STAFF_LINEDATA *prlLine = GetLine(ripGroupno, ripLineno);
			if (JobBelongsToThisLine(prlLine,lpUrno))
			{
				return TRUE;
			}
		}
	}
	if(bpSearchWithinTeam)
		return FindCompressedDutyBar(lpUrno, ripGroupno, ripLineno, bpIsPause);

	ripGroupno = ripLineno = -1;
	return FALSE;
}


BOOL StaffDiagramViewer::FindCompressedDutyBar(long lpUrno, int &ripGroupno, int &ripLineno, bool blIsPause /*false*/)
{
	long llPoolJobUrno = lpUrno;

	TEAM_DATA *prlTeam = GetTeamByPoolJobUrno(llPoolJobUrno);
	if(prlTeam != NULL && prlTeam->IsCompressed)
	{
		llPoolJobUrno = HasPlusMinusButton(prlTeam);
	}

	return FindDutyBar(llPoolJobUrno, ripGroupno, ripLineno, blIsPause);
}


BOOL StaffDiagramViewer::FindGroupAndLine(long lpUrno,int& ripGroupno,int& ripLineno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
	{
		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
		{
			STAFF_LINEDATA *prlLine = GetLine(ripGroupno, ripLineno);
			if(prlLine->JobUrno == lpUrno)
			{
				return TRUE;
			}
		}
	}

	return FALSE;

}

BOOL StaffDiagramViewer::FindLine(long lpUrno,int ipGroupno,int& ripLineno)
{
	if (ipGroupno >= 0 && ipGroupno < GetGroupCount())
	{
		for (ripLineno = 0; ripLineno < GetLineCount(ipGroupno); ripLineno++)
		{
			STAFF_LINEDATA *prlLine = GetLine(ipGroupno, ripLineno);
			if(prlLine->JobUrno == lpUrno)
			{
				return TRUE;
			}
		}
	}

	return FALSE;

}

bool StaffDiagramViewer::ContainsPausenlage(STAFF_LINEDATA *prpLine)
{
	int ilNumBkBars = prpLine->BkBars.GetSize();
	for(int ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
	{
		if(prpLine->BkBars[ilBkBar].Type == JOBBREAK)
			return true;
	}
	return false;
}

bool StaffDiagramViewer::JobBelongsToThisLine(STAFF_LINEDATA *prpLine, long lpPoolJobUrno)
{
	bool blJobBelongsToThisLine = false;
	if(prpLine != NULL)
	{
		int ilNumBkBars = prpLine->BkBars.GetSize();
		for(int ilBkBar = 0; !blJobBelongsToThisLine && ilBkBar < ilNumBkBars; ilBkBar++)
		{
			if(prpLine->BkBars[ilBkBar].Type == JOBPOOL && prpLine->BkBars[ilBkBar].JobUrno == lpPoolJobUrno)
			{
				blJobBelongsToThisLine = true;
			}
		}
	}
	return blJobBelongsToThisLine;
}

BOOL StaffDiagramViewer::FindBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
			for (ripBarno = 0; ripBarno < GetBarCount(ripGroupno, ripLineno); ripBarno++)
				if (GetBar(ripGroupno, ripLineno, ripBarno)->JobUrno == lpUrno)
					return TRUE;

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramViewer -- Viewer basic operations
//
int StaffDiagramViewer::GetGroupCount()
{
    return omGroups.GetSize();
}

STAFF_GROUPDATA *StaffDiagramViewer::GetGroup(int ipGroupno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize()) return NULL;
    return &omGroups[ipGroupno];
}

CString StaffDiagramViewer::GetGroupText(int ipGroupno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize()) return CString("");
    return omGroups[ipGroupno].Text;
}

CString StaffDiagramViewer::GetGroupTopScaleText(int ipGroupno)
{
	CString s;
	for (int i = 0; i < GetManagerCount(ipGroupno); i++)
	{
		STAFF_MANAGERDATA *prlManager = GetManager(ipGroupno, i);
		s += s.IsEmpty()? "": " / ";
		s += CString(prlManager->EmpLnam) + " " + prlManager->ShiftTmid + " " + prlManager->ShiftStid;
	}
	return s;
}

int StaffDiagramViewer::GetManagerCount(int ipGroupno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize()) return -1;
    return omGroups[ipGroupno].Managers.GetSize();
}

STAFF_MANAGERDATA *StaffDiagramViewer::GetManager(int ipGroupno, int ipManagerno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipManagerno >= omGroups[ipGroupno].Managers.GetSize()) return NULL;
    return &omGroups[ipGroupno].Managers[ipManagerno];
}

int StaffDiagramViewer::GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd)
{
	int ilColorIndex = FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2);
	CBrush *prlBrush = ogBrushs[ilColorIndex];
	int ilLineCount = GetLineCount(ipGroupno);
	for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
	{
		for (int ilBarno = 0; ilBarno < GetBarCount(ipGroupno, ilLineno); ilBarno++)
		{
			STAFF_BARDATA *prlBar;
			if ((prlBar = GetBar(ipGroupno, ilLineno, ilBarno)) != NULL)
			{
				if (IsOverlapped(opStart, opEnd, prlBar->StartTime, prlBar->EndTime))
				{
					JOBDATA *prlJob = ogJobData.GetJobByUrno(prlBar->JobUrno);
					if (prlJob != NULL)
					{
						if ((prlJob->ConflictType !=  CFI_NOCONFLICT) && (!prlJob->ConflictConfirmed))
						{
							return 4; // RED
							//return FIRSTCONFLICTCOLOR+(CFI_NOTCOVERED*2);
						}
					}
				}
			}
		}
	}
	return ilColorIndex;
}

int StaffDiagramViewer::GetLineCount(int ipGroupno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize()) return -1;
    return omGroups[ipGroupno].Lines.GetSize();
}


STAFF_LINEDATA *StaffDiagramViewer::GetLine(int ipGroupno, int ipLineno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize()) return NULL;
    return &omGroups[ipGroupno].Lines[ipLineno];
}

CString StaffDiagramViewer::GetLineText(int ipGroupno, int ipLineno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize()) return -1;
    return omGroups[ipGroupno].Lines[ipLineno].Text;
}

CString StaffDiagramViewer::GetLineStatusText(int ipGroupno, int ipLineno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize()) return -1;
    return omGroups[ipGroupno].Lines[ipLineno].StatusText;
}

bool StaffDiagramViewer::LineIsBottomOfTeam(int ipGroupno, int ipLineno)
{
	bool blIsBottomOfTeam = false;
	if(ipGroupno >= 0 && ipGroupno < omGroups.GetSize() && ipLineno < omGroups[ipGroupno].Lines.GetSize())
	{
		if (bmTeamView && (ipLineno >= (GetLineCount(ipGroupno)-1) || !AreInSameTeam(GetLine(ipGroupno,ipLineno)->JobUrno, GetLine(ipGroupno,ipLineno+1)->JobUrno)))
		{
			blIsBottomOfTeam = true;
		}
	}
	return blIsBottomOfTeam;
}

bool StaffDiagramViewer::LineIsTopOfTeam(int ipGroupno, int ipLineno)
{
	bool blIsTopOfTeam = false;
	if(ipGroupno >= 0 && ipGroupno < omGroups.GetSize() && ipLineno < omGroups[ipGroupno].Lines.GetSize())
	{
		if(bmTeamView)
		{
			if(IsTeamCompressed(ipGroupno, ipLineno))
			{
				blIsTopOfTeam = true;
			}
			else if(ipLineno < (GetLineCount(ipGroupno)-1)) // not the last line in this group
			{
				// is part of a team
				if(AreInSameTeam(GetLine(ipGroupno,ipLineno)->JobUrno, GetLine(ipGroupno,ipLineno+1)->JobUrno)) // same team a the next line
				{
					// is either the top line or different team to the previous line
					if(ipLineno <= 0 || !AreInSameTeam(GetLine(ipGroupno,ipLineno-1)->JobUrno, GetLine(ipGroupno,ipLineno)->JobUrno))
					{
						blIsTopOfTeam = true;
					}
				}
			}
		}
	}
	return blIsTopOfTeam;
}

// true if this emp is a trainer and accompanies a trainee
bool StaffDiagramViewer::IsDoubleTourTeacher(int ipGroupno, int ipLineno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize()) return false;
    return (omGroups[ipGroupno].Lines[ipLineno].DoubleTourStudent.IsEmpty()) ? false : true;
}

// true if this team has a team leader present
bool StaffDiagramViewer::IsTeamLeader(int ipGroupno, int ipLineno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize()) return false;
    return IsTeamLeader(omGroups[ipGroupno].Lines[ipLineno].JobUrno);
}

bool StaffDiagramViewer::IsTeamCompressed(int ipGroupno, int ipLineno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize()) return false;
    return IsTeamCompressed(omGroups[ipGroupno].Lines[ipLineno].JobUrno);
}

int StaffDiagramViewer::GetMaxOverlapLevel(int ipGroupno, int ipLineno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize()) return 0;
    return omGroups[ipGroupno].Lines[ipLineno].MaxOverlapLevel;
}

int StaffDiagramViewer::GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno)
{
	// For max level 0,1,2 we will use 2 levels, 3,4,5 we will use 5 levels, 6,7,8 we use 8 levels, and so on ...
	return GetMaxOverlapLevel(ipGroupno, ipLineno) / 3 * 3 + 2;
}

int StaffDiagramViewer::GetBkBarCount(int ipGroupno, int ipLineno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize()) return 0;
    return omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize();
}

STAFF_BKBARDATA *StaffDiagramViewer::GetBkBar(int ipGroupno, int ipLineno, int ipBkBarno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize() || ipBkBarno >= omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize()) return NULL;
    return &omGroups[ipGroupno].Lines[ipLineno].BkBars[ipBkBarno];
}

STAFF_BKBARDATA *StaffDiagramViewer::GetBkBarByJobUrno(int ipGroupno, int ipLineno, long lpJobUrno)
{
	STAFF_BKBARDATA *prlBkBar = NULL;

	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize()) return NULL;

	int ilNumBkBars = omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize();
	for(int ilBkBar = 0; prlBkBar == NULL && ilBkBar < ilNumBkBars; ilBkBar++)
	{
		if(omGroups[ipGroupno].Lines[ipLineno].BkBars[ilBkBar].JobUrno == lpJobUrno)
		{
			prlBkBar = &omGroups[ipGroupno].Lines[ipLineno].BkBars[ilBkBar];
		}
	}

	return prlBkBar;
}

CString StaffDiagramViewer::GetBkBarText(int ipGroupno, int ipLineno, int ipBkBarno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize() || ipBkBarno >= omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize()) return CString("");
    return omGroups[ipGroupno].Lines[ipLineno].BkBars[ipBkBarno].Text;
}

int StaffDiagramViewer::GetBarCount(int ipGroupno, int ipLineno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize()) return 0;
    return omGroups[ipGroupno].Lines[ipLineno].Bars.GetSize();
}

STAFF_BARDATA *StaffDiagramViewer::GetBar(int ipGroupno, int ipLineno, int ipBarno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize() || ipBarno >= omGroups[ipGroupno].Lines[ipLineno].Bars.GetSize()) return NULL;
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno];
}

CString StaffDiagramViewer::GetBarText(int ipGroupno, int ipLineno, int ipBarno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize() || ipBarno >= omGroups[ipGroupno].Lines[ipLineno].Bars.GetSize()) return CString("");
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Text;
}

CString StaffDiagramViewer::GetStatusBarText(int ipGroupno, int ipLineno, int ipBarno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize() || ipBarno >= omGroups[ipGroupno].Lines[ipLineno].Bars.GetSize()) return CString("");
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].StatusText;
}

CString StaffDiagramViewer::GetStatusPoolJobBkBarText(int ipGroupno, int ipLineno, int ipPoolJobBkBarno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize() || ipPoolJobBkBarno >= omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize()) return CString("");
    return omGroups[ipGroupno].Lines[ipLineno].BkBars[ipPoolJobBkBarno].StatusText;
}

int StaffDiagramViewer::GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize() || ipBarno >= omGroups[ipGroupno].Lines[ipLineno].Bars.GetSize()) return 0;
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize();
}

STAFF_INDICATORDATA *StaffDiagramViewer::GetIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize() || ipBarno >= omGroups[ipGroupno].Lines[ipLineno].Bars.GetSize() || ipIndicatorno >= omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize()) return NULL;
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators[ipIndicatorno];
}


/////////////////////////////////////////////////////////////////////////////
// StaffDiagramViewer - STAFF_BARDATA array maintenance (overlapping version)
//
void StaffDiagramViewer::DeleteAll()
{
    while (GetGroupCount() > 0)
        DeleteGroup(0);
	
	ClearMapPageNoToPreviewDataList();
}

int StaffDiagramViewer::CreateGroup(STAFF_GROUPDATA *prpGroup)
{
    int ilGroupCount = omGroups.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
//#ifndef SCANBACKWARD
//    // Search for the position which we want to insert this new bar
//    for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
//		if (CompareGroup(prpGroup, GetGroup(ilGroupno)) <= 0)
//            break;  // should be inserted before Groups[ilGroupno]
//#else
//    // Search for the position which we want to insert this new bar
//    for (int ilGroupno = ilGroupCount; ilGroupno > 0; ilGroupno--)
//		if (CompareGroup(prpGroup, GetGroup(ilGroupno-1)) >= 0)
//            break;  // should be inserted after Groups[ilGroupno-1]
//#endif
//    
//    omGroups.NewAt(ilGroupno, *prpGroup);
//	return ilGroupno;
    omGroups.NewAt(ilGroupCount, *prpGroup);
	return ilGroupCount;
}

void StaffDiagramViewer::DeleteGroup(int ipGroupno)
{
    while (GetLineCount(ipGroupno) > 0)
        DeleteLine(ipGroupno, 0);
    while (GetManagerCount(ipGroupno) > 0)
        DeleteManager(ipGroupno, 0);

    omGroups.DeleteAt(ipGroupno);
}

int StaffDiagramViewer::CreateManager(int ipGroupno, STAFF_MANAGERDATA *prpManager)
{
    int ilManagerCount = omGroups[ipGroupno].Managers.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilManagerno = 0; ilManagerno < ilManagerCount; ilManagerno++)
		if (CompareManager(prpManager, GetManager(ipGroupno, ilManagerno)) <= 0)
            break;  // should be inserted before Lines[ilManagerno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilManagerno = ilManagerCount; ilManagerno > 0; ilManagerno--)
		if (CompareManager(prpManager, GetManager(ipGroupno, ilManagerno-1)) >= 0)
            break;  // should be inserted after Lines[ilManagerno-1]
#endif

    omGroups[ipGroupno].Managers.NewAt(ilManagerno, *prpManager);
    STAFF_MANAGERDATA *prlManager = &omGroups[ipGroupno].Managers[ilManagerno];
    return ilManagerno;
}

void StaffDiagramViewer::DeleteManager(int ipGroupno, int ipManagerno)
{
    omGroups[ipGroupno].Managers.DeleteAt(ipManagerno);
}

STAFF_LINEDATA *StaffDiagramViewer::CreateLine(int ipGroupno, STAFF_LINEDATA *prpLine, bool bpSort /* = true */)
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		//Implemented for singapore airport
		CString olGroupText = GetGroupText(ipGroupno);
		SHIFTTYPEDATA* polShiftType = ogShiftTypes.GetShiftTypeByCode(prpLine->ShiftStid.GetBuffer(0));
		CString olEmpTerminal = prpLine->ShiftStid.Mid(0,2);
		if(polShiftType != NULL)
		{
			olEmpTerminal = strlen(polShiftType->Bsds) == 0 ? olEmpTerminal : polShiftType->Bsds;
		}
		if((olGroupText.Mid(0,2).CompareNoCase(Terminal2) == 0) && (olEmpTerminal.CompareNoCase(Terminal2) != 0))
		{
			STAFF_GROUPDATA* polGroupData = GetGroup(ipGroupno);
			if(polGroupData != NULL && polGroupData->pomChildGroupData != NULL)
			{
				ipGroupno++;
			}
			else 
			{
				return NULL;
			}
		}
		else if((olGroupText.Mid(0,2).CompareNoCase(Terminal3) == 0) && (olEmpTerminal.CompareNoCase(Terminal2) == 0))
		{	
			STAFF_GROUPDATA* polGroupData = GetGroup(ipGroupno);
			if(polGroupData != NULL && polGroupData->pomParentGroupData != NULL)
			{
				ipGroupno--;
			}
			else 
			{
				return NULL;
			}
		}
	}

	prpLine->SortString = MakeSortString(prpLine);
	prpLine->MaxOverlapLevel = 0;

	if(bpSort)
	{
		int ilNumLines = omGroups[ipGroupno].Lines.GetSize();
		for(int ilL = 0; ilL < ilNumLines; ilL++)
		{
			if(strcmp(prpLine->SortString, omGroups[ipGroupno].Lines[ilL].SortString) <= 0)
			{
				break;
			}
		}

		omGroups[ipGroupno].Lines.InsertAt(ilL, prpLine);
	}
	else
	{
	    omGroups[ipGroupno].Lines.Add(prpLine);
	}

    return prpLine;
}

void StaffDiagramViewer::DeleteLine(int ipGroupno, int ipLineno)
{
	STAFF_LINEDATA *prlLine = &omGroups[ipGroupno].Lines[ipLineno];
	DeleteBarsForLine(prlLine);
    omGroups[ipGroupno].Lines.DeleteAt(ipLineno);
}

void StaffDiagramViewer::DeleteBarsForLine(STAFF_LINEDATA *prpLine)
{
	if(prpLine != NULL)
	{
		while (prpLine->Bars.GetSize() > 0)
		{
			prpLine->Bars[0].Indicators.DeleteAll();
			prpLine->Bars.DeleteAt(0);
		}
		prpLine->BkBars.DeleteAll();
	}
}

int StaffDiagramViewer::CreateBkBar(int ipGroupno, int ipLineno, STAFF_BKBARDATA *prpBkBar)
{
    int ilBkBarCount = omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
// (2.89 seconds vs. 4.21 seconds) for 3000 bars creation.
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilBkBarno = 0; ilBkBarno < ilBarCount; ilBkBarno++)
        if (prpBar->StartTime <= GetBkBar(ipGroupno,ipLineno,ilBkBarno)->StartTime)
            break;  // should be inserted before Bars[ilBkBarno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilBkBarno = ilBkBarCount; ilBkBarno > 0; ilBkBarno--)
        if (prpBkBar->StartTime >= GetBkBar(ipGroupno,ipLineno,ilBkBarno-1)->StartTime)
            break;  // should be inserted after Bars[ilBkBarno-1]
#endif

    omGroups[ipGroupno].Lines[ipLineno].BkBars.NewAt(ilBkBarno, *prpBkBar);
    return ilBkBarno;
}

void StaffDiagramViewer::DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno)
{
    omGroups[ipGroupno].Lines[ipLineno].BkBars.DeleteAt(ipBkBarno);
}

void StaffDiagramViewer::DeleteBkBarsByJob(int ipGroupno, int ipLineno, long lpJobUrno)
{
	int ilNumBkBars = omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize();
	for(int ilBkBar = (ilNumBkBars-1); ilBkBar >= 0; ilBkBar--)
	{
		if(omGroups[ipGroupno].Lines[ipLineno].BkBars[ilBkBar].JobUrno == lpJobUrno)
		{
			omGroups[ipGroupno].Lines[ipLineno].BkBars.DeleteAt(ilBkBar);
		}
	}
}

int StaffDiagramViewer::CreateBar(int ipGroupno, int ipLineno, STAFF_BARDATA *prpBar, BOOL bpTopBar)
{
	CheckSelectBar(prpBar);

    STAFF_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();



	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of insertion:
	//
	// Case A: Insert a new bar to be the topmost bar
	//		We will scan every bars, and for each bar which is in the same overlap group
	//		with the new bar, we will shift it downward one level, and update the field
	//		Line.MaxOverlapLevel. This new bar will be the last bar in painting order
	//		(for being topmost), and has overlapping level = 0.
	//
	// Case B: Insert a new bar to be the bottommost bar
	//		We will scan every bars for finding the maximum overlap level of bars which
	//		are in the same overlap group with this new bar (use level -1 if not found).
	//		Then use this maximum overlap level + 1 as overlap level of this new bar.
	//		This new bar will be the first bar in painting order (for being bottommost).
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipLineno,
		prpBar->StartTime, prpBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	if (bpTopBar)
	{
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			STAFF_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			int ilNewOverlapLevel = ++prlBar->OverlapLevel;
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewOverlapLevel);
		}
		prlLine->Bars.NewAt(ilBarCount, *prpBar);	// to be the last bar in painting order
		prlLine->Bars[ilBarCount].OverlapLevel = 0;
		return ilBarCount;
	}
	else
	{
		int ilNewBarLevel = -1;
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			STAFF_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			ilNewBarLevel = max(ilNewBarLevel, prlBar->OverlapLevel + 1);
				// the +1 means that we have to insert the new bar below this bar
		}
		prlLine->Bars.NewAt(0, *prpBar);	// to be the first bar in painting order
		prlLine->Bars[0].OverlapLevel = ilNewBarLevel;
		prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewBarLevel);
		return 0;
	}
}

void StaffDiagramViewer::DeleteBar(int ipGroupno, int ipLineno, int ipBarno)
{
	// Delete all indicators of this bar
	while (GetIndicatorCount(ipGroupno, ipLineno, ipBarno) > 0)
		DeleteIndicator(ipGroupno, ipLineno, ipBarno, 0);

    STAFF_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
	STAFF_BARDATA *prlBar = &prlLine->Bars[ipBarno];

	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of deletion:
	//
	// We will scan every bars, and for each bar which is in the same overlap group with
	// the new bar, we will back up them to a temporary array. Then we remove every bars
	// in this overlapping group, and insert these bar from the temporary array except
	// the one which has to be deleted back to the viewer.
	//
	// In some situation, if this overlapping group controls the maximum overlap level of
	// the line, we may have to rescan every bars in this line to make sure that the
	// maximum overlap level still be correct.
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipLineno,
		prlBar->StartTime, prlBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	// Save the bar in this overlapping group to a temporary array
	CCSPtrArray<STAFF_BARDATA> olSavedBars;
	BOOL blMustRecomputeMaxOverlapLevel = FALSE;
	for (int ilLc = 0; ilLc < ilListCount; ilLc++)
	{
		int ilBarno = olBarnoList[ilLc];
		STAFF_BARDATA *prlBar = &prlLine->Bars[ilBarno];
		blMustRecomputeMaxOverlapLevel |= (prlBar->OverlapLevel == prlLine->MaxOverlapLevel);
			// check if deletion of this bar may change the line height
		if (ilBarno != ipBarno)	// must we save this bar?
			olSavedBars.NewAt(olSavedBars.GetSize(), prlLine->Bars[ilBarno]);
	}
	// Delete all bars (which are already backed up in a temporary array) from the viewer
	while (ilListCount-- > 0)
	{
		int ilBarno = olBarnoList[ilListCount];
		prlLine->Bars.DeleteAt(ilBarno);
	}
	// Then, we insert those bars back to the Gantt chart, except the specified one
	int ilSavedBarCount = olSavedBars.GetSize();
	for (int ilSavedBarno = 0; ilSavedBarno < ilSavedBarCount; ilSavedBarno++)
	{
		CreateBar(ipGroupno, ipLineno, &olSavedBars[ilSavedBarno]);
	}
	olSavedBars.DeleteAll();
	// Then recompute the MaxOverlapLevel if it's necessary
	if (blMustRecomputeMaxOverlapLevel)
	{
		prlLine->MaxOverlapLevel = 0;
	    int ilBarCount = prlLine->Bars.GetSize();
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			STAFF_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, prlBar->OverlapLevel);
		}
	}
}

int StaffDiagramViewer::CreateIndicator(int ipGroupno, int ipLineno, int ipBarno, STAFF_INDICATORDATA *prpIndicator)
{
	if(ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize() || ipBarno >= omGroups[ipGroupno].Lines[ipLineno].Bars.GetSize()) return -1;
    int ilIndicatorCount = omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize();

#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
        if (prpIndicator->StartTime <= GetIndicator(ipGroupno,ipLineno,ipBarno,ilIndicatorno)->StartTime)
            break;  // should be inserted before Indicators[ilBarno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilIndicatorno = ilIndicatorCount; ilIndicatorno > 0; ilIndicatorno--)
        if (prpIndicator->StartTime >= GetIndicator(ipGroupno,ipLineno,ipBarno,ilIndicatorno-1)->StartTime)
            break;  // should be inserted before Indicators[ilBarno]
#endif

    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.
		NewAt(ilIndicatorno, *prpIndicator);
    return ilIndicatorno;
}

void StaffDiagramViewer::DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
	if(ipGroupno >= omGroups.GetSize() || ipLineno >= omGroups[ipGroupno].Lines.GetSize() || ipBarno >= omGroups[ipGroupno].Lines[ipLineno].Bars.GetSize() || ipIndicatorno >= omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize()) return;
    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.DeleteAt(ipIndicatorno);
}


int StaffDiagramViewer::PoolJobCount(int ipGroupno, int ipLineno)
{
	int ilPoolJobCount = 0;

	if(ipGroupno < omGroups.GetSize() && ipLineno < omGroups[ipGroupno].Lines.GetSize())
	{
		int ilNumBkBars = omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize();
		for(int ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
		{
			if(omGroups[ipGroupno].Lines[ipLineno].BkBars[ilBkBar].Type == JOBPOOL)
			{
				ilPoolJobCount++;
			}
		}
	}
	return ilPoolJobCount;
}

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramViewer - STAFF_BARDATA calculation routines

// Return the bar number of the list box based on the given period [time1, time2]
int StaffDiagramViewer::GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
    int ipOverlapLevel1, int ipOverlapLevel2)
{
    for (int i = GetBarCount(ipGroupno, ipLineno)-1; i >= 0; i--)
    {
        STAFF_BARDATA *prlBar = GetBar(ipGroupno, ipLineno, i);
        if (prlBar != NULL && opTime1 <= prlBar->EndTime && prlBar->StartTime <= opTime2 &&
            ipOverlapLevel1 <= prlBar->OverlapLevel && prlBar->OverlapLevel <= ipOverlapLevel2)
            return i;
    }
    return -1;
}

// Return the background bar number of the list box based on the given period [time1, time2]
int StaffDiagramViewer::GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2)
{
    for (int i = GetBkBarCount(ipGroupno, ipLineno)-1; i >= 0; i--)
    {
        STAFF_BKBARDATA *prlBkBar = GetBkBar(ipGroupno, ipLineno, i);
        if (prlBkBar != NULL && opTime1 <= prlBkBar->EndTime && prlBkBar->StartTime <= opTime2)
            return i;
    }
    return -1;
}

// Return the pool job background bar number of the list box based on the given period [time1, time2]
int StaffDiagramViewer::GetPoolJobFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2)
{
    for (int i = GetBkBarCount(ipGroupno, ipLineno)-1; i >= 0; i--)
    {
        STAFF_BKBARDATA *prlBkBar = GetBkBar(ipGroupno, ipLineno, i);
        if (prlBkBar != NULL && prlBkBar->Type == JOBPOOL && opTime1 <= prlBkBar->EndTime && prlBkBar->StartTime <= opTime2)
            return i;
    }
    return -1;
}


/////////////////////////////////////////////////////////////////////////////
// StaffDiagramViewer private helper methods

void StaffDiagramViewer::GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
	CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList)
{
    STAFF_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	// Prepare the empty array of the involvement flag for each bar
	CCSPtrArray<BOOL> IsInvolvedBar;
	for (int i = 0; i < ilBarCount; i++)
		IsInvolvedBar.NewAt(i, FALSE);

	// We will loop again and again until there is no more bar in this overlapping found.
	// The overlapping group is at first determine by the given "opTime1" and "opTime2".
	// But if the bar we find is exceed this period of time, we will extend searching
	// to both left side and right side in the next round.
	//
	int nBarsFoundThisRound = -1;
	while (nBarsFoundThisRound != 0)
	{
		nBarsFoundThisRound = 0;
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			CTime olStartTime = prlLine->Bars[ilBarno].StartTime;
			CTime olEndTime = prlLine->Bars[ilBarno].EndTime;
			BOOL blIsOverlapped = IsReallyOverlapped(olStartTime, olEndTime, opTime1, opTime2);
			BOOL blIsWithIn = IsWithIn(olStartTime, olEndTime, opTime1, opTime2);
				// this IsWithIn() fix the real thin bar bug
			BOOL blIsInvolved = (blIsOverlapped || blIsWithIn);

			if (!IsInvolvedBar[ilBarno] && blIsInvolved)	// some more new involved bars?
			{
				IsInvolvedBar[ilBarno] = TRUE;
				nBarsFoundThisRound++;
				opTime1 = min(opTime1, prlLine->Bars[ilBarno].StartTime);
				opTime2 = max(opTime2, prlLine->Bars[ilBarno].EndTime);
			}
		}
	}

	// Create the list of involved bars, then store them to "ropBarnoList"
	ropBarnoList.RemoveAll();
	for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
	{
		if (IsInvolvedBar[ilBarno])
		{
			ropBarnoList.Add(ilBarno);
		}
	}
	IsInvolvedBar.DeleteAll();
}


/////////////////////////////////////////////////////////////////////////////
// StaffDiagramViewer Jobs creation methods

void StaffDiagramViewer::AllowUpdates(BOOL bpNoUpdatesNow)
{
	bmNoUpdatesNow = bpNoUpdatesNow;
}



///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines

void StaffDiagramViewer::PrintStaffDiagramHeader(int ipGroupno)
{
	CString olPoolName = GetGroupText(ipGroupno);
	if (omGroupBy != GROUP_BY_POOL)
	{
		olPoolName.Empty();
	}


    CTime olActStart(omStartTime.GetYear(),omStartTime.GetMonth(),omStartTime.GetDay(),
		omStartTime.GetHour(),0,0);
	//  add one hour if we are in the second half of the actual hour
	if (omStartTime.GetMinute() > 30)
	{
		olActStart += CTimeSpan(0,1,0,0);
	}

    CTimeSpan olDuration = omEndTime - omStartTime;
	int ilHours = olDuration.GetTotalHours();
	CTime olActEnd = olActStart + CTimeSpan(0,ilHours,0,0);

	CString omTarget;
	omTarget.Format(GetString(IDS_STRING61285),olPoolName,olActStart.Format("%d.%m.%Y %H:%M"),olActEnd.Format("%d.%m.%Y  %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");

	pomPrint->pomCdc->StartPage(); //PRF 8704
	pomPrint->PrintHeader(GetString(IDS_STAFFCHART),olPrintDate,omTarget);
	pomPrint->PrintTimeScale(700,2800,omStartTime,omEndTime,olPoolName);
}


void StaffDiagramViewer::PrintPrepareLineData(int ipGroupNo,int ipLineno,
	CCSPtrArray<PRINTBARDATA> &ropPrintLine,CCSPtrArray<PRINTBARDATA> &ropBkBars)
{

	if (ipLineno < omGroups[ipGroupNo].Lines.GetSize())
	{
		STAFF_LINEDATA *prlLine = &omGroups[ipGroupNo].Lines[ipLineno];
		ropPrintLine.DeleteAll();
		int ilBarCount = prlLine->Bars.GetSize();
		for( int i = 0; i < ilBarCount; i++)
		{
			if (IsOverlapped(prlLine->Bars[i].StartTime,prlLine->Bars[i].EndTime,
				omStartTime,omEndTime))
			{
				PRINTBARDATA rlPrintBar;
				rlPrintBar.Text = prlLine->Bars[i].Text;
				rlPrintBar.StartTime = prlLine->Bars[i].StartTime;
				rlPrintBar.EndTime = prlLine->Bars[i].EndTime;
				rlPrintBar.FrameType = prlLine->Bars[i].FrameType;
				rlPrintBar.MarkerType = prlLine->Bars[i].MarkerType;
				rlPrintBar.IsShadowBar = FALSE;
				rlPrintBar.OverlapLevel = 0;
				rlPrintBar.IsBackGroundBar = FALSE;
				ropPrintLine.NewAt(ropPrintLine.GetSize(),rlPrintBar);
			}
		}
		ilBarCount = prlLine->BkBars.GetSize();
		for( i = 0; i < ilBarCount; i++)
		{
			if (IsOverlapped(prlLine->BkBars[i].StartTime,prlLine->BkBars[i].EndTime,
				omStartTime,omEndTime))
			{
				PRINTBARDATA rlPrintBar;
				if (prlLine->BkBars[i].Type == JOBTEMPABSENCE)
				{
					rlPrintBar.Text = prlLine->BkBars[i].Text;
					rlPrintBar.StartTime = prlLine->BkBars[i].StartTime;
					rlPrintBar.EndTime = prlLine->BkBars[i].EndTime;
					rlPrintBar.FrameType = FRAMERECT;
					rlPrintBar.MarkerType = MARKNONE;
					rlPrintBar.IsShadowBar = FALSE;
					rlPrintBar.OverlapLevel = 0;
					rlPrintBar.IsBackGroundBar = TRUE;
				}
				else
				{
					rlPrintBar.Text = prlLine->BkBars[i].Text;
					rlPrintBar.TextAlign = TA_LEFT;
					rlPrintBar.StartTime = prlLine->BkBars[i].StartTime;
					rlPrintBar.EndTime = prlLine->BkBars[i].EndTime;
					rlPrintBar.FrameType = FRAMENONE;
					rlPrintBar.MarkerType = MARKFULL;
					rlPrintBar.IsShadowBar = FALSE;
					rlPrintBar.OverlapLevel = 0;
					rlPrintBar.IsBackGroundBar = TRUE;
				}

				ropBkBars.NewAt(ropBkBars.GetSize(),rlPrintBar);
			}
		}
	}
	else
	{
		ropPrintLine.DeleteAll();
	}
}


void StaffDiagramViewer::PrintManagers(int ipGroupno)
{
	CString olAllManagers;

	POSITION rlPos;
	for ( rlPos = ogJobData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		JOBDATA *prlJob;
		ogJobData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlJob);
		if (strcmp(prlJob->Jtco, JOBPOOL) != 0)	// uninterest non-duty bars
			continue;

		CString olFunction = ogBasicData.GetFunctionForPoolJob(prlJob);

		CStringArray olPermits;
		ogBasicData.GetPermitsForPoolJob(prlJob,olPermits);
		CString olGroupName = ogBasicData.GetTeamForPoolJob(prlJob);

		// Check if this duty is in the filter and has some part visible in the display window
		SHIFTDATA *prlShift;
		EMPDATA *prlEmp;
		if ((prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur)) == NULL ||
			(prlEmp = ogEmpData.GetEmpByPeno(prlShift->Peno)) == NULL ||
			!IsPassPoolJobFilter(prlJob, prlShift, prlEmp, olGroupName, olFunction, olPermits))
			continue;

//		if ((prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur)) == NULL ||
//			(prlEmp = ogEmpData.GetEmpByPeno(prlShift->Peno)) == NULL ||
//			!IsPassFilter(prlJob->Alid, olGroupName, prlShift->Sfca, olFunction,ogShiftData.IsAbsent(prlShift),olPermits) ||
//			!IsInTimeframe(prlJob) ||
//			(bmHideCompletelyDelegatedTeams && IsCompletelyDelegatedPoolJob(prlJob)) ||
//			(bmHideAbsentEmps && ogJobData.IsAbsent(prlJob)) ||
//			(bmHideStandbyEmps && ogJobData.IsStandby(prlJob)) ||
//			(bmHideFidEmps && ogJobData.PoolJobHasFidJobs(prlJob->Urno)))
//			continue;


		int ilGroupno;
		if ((ilGroupno = FindGroup(prlJob->Alid, olGroupName, prlShift->Sfca)) != ipGroupno)
			continue;	// manager is not in this group

		BOOL blIsFlightManager = ogBasicData.IsManager(olFunction);
		if (blIsFlightManager)
		{
			if (olAllManagers.IsEmpty() == FALSE)
			{
				olAllManagers += "   /   ";
			}
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur);
			olAllManagers += CString((prlShift != NULL ? prlShift->Sfca : "")) + ", ";
			olAllManagers += olGroupName + ", ";
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);
			olAllManagers += CString(prlEmp != NULL ? prlEmp->Lanm : "");
		}
	}
	if (olAllManagers.IsEmpty() == FALSE)
	{
		int ilYOffset = 0;
		if (bmIsFirstGroupOnPage == FALSE)
		{
			pomPrint->PrintText(720,0,1100,40,PRINT_SMALLBOLD,"Flightmanager:",TRUE);
			pomPrint->PrintText(720,40,0,130,PRINT_SMALL,olAllManagers,TRUE);
			pomPrint->PrintText(720,0,0,50,PRINT_SMALL,"");
		}
		else
		{
			pomPrint->PrintText(720,380,1100,420,PRINT_SMALLBOLD,"Flightmanager:");
			pomPrint->PrintText(720,420,0,510,PRINT_SMALL,olAllManagers);
		}  
		bmIsFirstGroupOnPage = FALSE;
	}
} 

void StaffDiagramViewer::PrintPool(CPtrArray &opPtrArray,int ipGroupNo)
{
	if( pomPrint->imLineNo > (pomPrint->imMaxLines - ogBasicData.imFreeBottomLines))
	{
		if ( pomPrint->imPageNo > 0)
		{
			pomPrint->PrintGanttBottom();
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			bmIsFirstGroupOnPage = TRUE;
		//	pomPrint->imLineNo = 999;
		}
		PrintManagers(ipGroupNo);
		PrintStaffDiagramHeader(ipGroupNo);
	}
	else
	{
		//PRF 8704, Added for handling empty pools
		//////////////////////////
		if(!((pomPrint->imYOffset) < (pomPrint->pomCdc->GetDeviceCaps(VERTRES)-MMY(100))))
		{
			if ( pomPrint->imPageNo > 0)
			{
				pomPrint->PrintGanttBottom();
				pomPrint->PrintFooter("","");
				pomPrint->omCdc.EndPage();
			}
			bmIsFirstGroupOnPage = TRUE;
			PrintStaffDiagramHeader(ipGroupNo);
			pomPrint->imLineNo = 0;
			bmIsFirstGroupOnPage = TRUE;
		}
		///////////////////////////
		else
		{
			PrintManagers(ipGroupNo);
			CString olPoolName = GetGroupText(ipGroupNo);
			pomPrint->PrintGanttHeader(700,2800,olPoolName);
		}		
	}


	CCSPtrArray<PRINTBARDATA> ropPrintLine;
	CCSPtrArray<PRINTBARDATA> ropBkBars;
	int ilLineCount = GetLineCount(ipGroupNo);
	/*
	if (ilLineCount == 0)
	{
		ropBkBars.RemoveAll();
		ropPrintLine.DeleteAll();
		for(int ilLc = 0; ilLc < 5; ilLc++)
		{
//	if (pomPrint->PrintGanttLine(" ",ropPrintLine,ropBkBars) != TRUE)
			pomPrint->PrintGanttLine(" ",ropPrintLine,ropBkBars);
		}
	
		// page full ?
		if ( pomPrint->imPageNo > 0)
		{
			pomPrint->PrintGanttBottom();
			pomPrint->PrintFooter("Dispositions Arbeitsplatz:    ","Mitarbeiterdiagramm");
			pomPrint->omCdc.EndPage();
			bmIsFirstGroupOnPage = TRUE;
		}
		PrintStaffDiagramHeader(ipGroupNo);
		bmIsFirstGroupOnPage = TRUE;

		bmIsFirstGroupOnPage = TRUE;
		pomPrint->imLineNo = 0;
		bmIsFirstGroupOnPage = TRUE;
		PrintManagers(ipGroupNo);
		PrintStaffDiagramHeader(ipGroupNo);
		// try it again, but don't continue with this line if it don't fit on page again!
		pomPrint->PrintGanttLine(" ",ropPrintLine,ropBkBars);

	}
	else*/
    for( int ilLineno = 0; ilLineno < (ilLineCount); ilLineno++)
	{
		PrintPrepareLineData(ipGroupNo,ilLineno,ropPrintLine,ropBkBars);
		CString olLineText;
		if (ilLineno < ilLineCount)
		{
			olLineText = GetLineText(ipGroupNo, ilLineno);
		}
		else
		{
			olLineText.Empty();
		}
		if (pomPrint->PrintGanttLine(olLineText,ropPrintLine,ropBkBars) != TRUE)
		{
			if ( pomPrint->imPageNo > 0)
			{
				pomPrint->PrintGanttBottom();
				pomPrint->PrintFooter("","");
				pomPrint->omCdc.EndPage();
			}
			bmIsFirstGroupOnPage = TRUE;
			PrintStaffDiagramHeader(ipGroupNo);
			pomPrint->imLineNo = 0;
			bmIsFirstGroupOnPage = TRUE;
			PrintManagers(ipGroupNo);
			//PrintStaffDiagramHeader(ipGroupNo);
			// try it again, but don't continue with this line if it don't fit on page again!
			if (ilLineno < ilLineCount)
			{
				pomPrint->PrintGanttLine(GetLineText(ipGroupNo, ilLineno),ropPrintLine,ropBkBars);
			}
		}
	}
	pomPrint->PrintGanttBottom();
//	pomPrint->imLineNo = 999;
}

void StaffDiagramViewer::PrintDiagram(CPtrArray &opPtrArray)
{
	CString omTarget = CString("Anzeigebegin: ") + CString(omStartTime.Format("%d.%m.%Y"))
		+  "     Ansicht: " + ogCfgData.rmUserSetup.STCV;
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomAttachWnd,PRINT_LANDSCAPE,80,500,200,
		CString("Mitarbeiterdiagramm"),olPrintDate,omTarget);
	if (pomPrint != NULL)
	{
		pomPrint->EnablePageSelection();
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetupPrintPageInfo(CCSPrint::PRINT_PRINTER); //PRF 8704
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = "Mitarbeiterdiagramm";	
			pomPrint->omCdc.StartDoc( &rlDocInfo );

			if(pomPrint->IsPageSelected() == TRUE)
			{
				for(int i = pomPrint->GetFromPage(); i <= pomPrint->GetToPage(); i++)
				{
					PrintPageData(pomPrint,i);
				}
			}
			else
			{
				int ilGroupCount = GetGroupCount();
				bmIsFirstGroupOnPage = TRUE;
				for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
				{
					PrintPool(opPtrArray,ilGroupno);
				}
				pomPrint->PrintFooter("","");
				pomPrint->omCdc.EndPage();
				bmIsFirstGroupOnPage = TRUE;/*
				for(int i = 1 ; i <= omMapPageNoToPreviewDataList.GetCount(); i++)
				{
					PrintPageData(pomPrint,i);
				}*/

			}

			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
	if(pomPrint != NULL)
	{
		delete pomPrint;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  team/Gruppen- dispo                                                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void StaffDiagramViewer::DeleteAllTeams()
{
	int ilNumTeams = omTeams.GetSize();
	for(int ilTeam = (ilNumTeams-1); ilTeam >= 0; ilTeam--)
	{
		DeleteTeam(&omTeams[ilTeam],false,false);
	}
	omTeams.DeleteAll();
	omJobUrnoToTeamMap.RemoveAll();
}

void StaffDiagramViewer::DeleteTeam(TEAM_DATA *prpTeam, bool bpDeleteBars /* = true */, bool bpUpdateLine /* = true */)
{
	if(prpTeam != NULL)
	{
		CString olTeamName = prpTeam->TeamName;
		int ilGroupNo = prpTeam->GroupNo;
		CTime olBegin = prpTeam->Begin;
		CTime olEnd = prpTeam->End;

		DeleteCompressedJobsForTeam(prpTeam,bpDeleteBars,bpUpdateLine);
		DeleteCompressedPoolJobsForTeam(prpTeam,bpDeleteBars,bpUpdateLine);
		DeletePoolJobsForTeam(prpTeam,bpDeleteBars,bpUpdateLine);
		int ilNumTeams = omTeams.GetSize();
		for(int ilTeam = (ilNumTeams-1); ilTeam >= 0; ilTeam--)
		{
			TEAM_DATA *prlTmpTeam = &omTeams[ilTeam];
			if(prlTmpTeam->TeamName == olTeamName && prlTmpTeam->GroupNo == ilGroupNo && IsOverlapped(prlTmpTeam->Begin,prlTmpTeam->End,olBegin,olEnd))
			{
				omTeams.DeleteAt(ilTeam);
				break;
			}
		}
	}
}

void StaffDiagramViewer::DeletePoolJobsForTeam(TEAM_DATA *prpTeam, bool bpDeleteBars /* = true */, bool bpUpdateLine /* = true */)
{
	if(prpTeam != NULL)
	{
		int ilNumPoolJobs = prpTeam->PoolJobs.GetSize();
		for(int ilPoolJob = (ilNumPoolJobs-1); ilPoolJob >= 0; ilPoolJob--)
		{
			JOBDATA *prlPoolJob = &prpTeam->PoolJobs[ilPoolJob];
			int ilGroupno, ilLineno;
			if(bpDeleteBars && FindDutyBar(prlPoolJob->Urno, ilGroupno, ilLineno))
			{
				// normal shift
				//ogBasicData.Trace("Pool Job Shift Deleted\n");
				DeleteLine(ilGroupno, ilLineno);
				if(MessagingActivated())
				{
					LONG lParam = MAKELONG(ilLineno, ilGroupno);
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_DELETELINE, lParam);
				}
			}
			omJobUrnoToTeamMap.RemoveKey((void *)prlPoolJob->Urno);
		}
		prpTeam->PoolJobs.RemoveAll();
	}
}

void StaffDiagramViewer::DeleteCompressedPoolJobsForTeam(TEAM_DATA *prpTeam, bool bpDeleteBars /* = true */, bool bpUpdateLine /* = true */)
{
	if(prpTeam != NULL)
	{
		int ilNumCompressedPoolJobs = prpTeam->CompressedPoolJobs.GetSize();
		for(int ilCPJ = 0; ilCPJ < ilNumCompressedPoolJobs; ilCPJ++)
		{
			COMPRESSED_JOBDATA *prlCompressedJob = &prpTeam->CompressedPoolJobs[ilCPJ];

			int ilNumOriginalJobs = prlCompressedJob->OriginalJobs.GetSize();
			for(int ilOJ = 0; ilOJ < ilNumOriginalJobs; ilOJ++)
			{
				JOBDATA *prlOriginalJob = &prlCompressedJob->OriginalJobs[ilOJ];
				omJobUrnoToTeamMap.RemoveKey((void *)prlOriginalJob->Urno);
			}
			prlCompressedJob->OriginalJobs.RemoveAll();

			int ilGroupno, ilLineno;
			if(bpDeleteBars && FindDutyBar(prlCompressedJob->Data.Urno, ilGroupno, ilLineno))
			{
				DeleteLine(ilGroupno, ilLineno);
				if(MessagingActivated())
				{
					LONG lParam = MAKELONG(ilLineno, ilGroupno);
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_DELETELINE, lParam);
				}
			}
		}
		prpTeam->CompressedPoolJobs.DeleteAll();
		prpTeam->CompressedTempAbsences.DeleteAll();
	}
}

// deletes the pool job specified from the team
TEAM_DATA *StaffDiagramViewer::DeletePoolJobFromTeam(TEAM_DATA *prpTeam, long lpPoolJobUrno, bool bpUpdateLine /* = true */)
{
	TEAM_DATA *prlTeam = prpTeam;
	if(prpTeam != NULL)
	{
		int ilNumPoolJobs = prpTeam->PoolJobs.GetSize();
		if(ilNumPoolJobs == 1 && prpTeam->PoolJobs[0].Urno == lpPoolJobUrno)
		{
			// only pool job in the team, so delete the team
			DeleteTeam(prpTeam, bpUpdateLine, bpUpdateLine);
			prlTeam = NULL;
		}
		else
		{
			omJobUrnoToTeamMap.RemoveKey((void *)lpPoolJobUrno);

			int ilNumCompressedPoolJobs = prpTeam->CompressedPoolJobs.GetSize();
			for(int ilCPJ = 0; ilCPJ < ilNumCompressedPoolJobs; ilCPJ++)
			{
				int ilNumOriginalJobs = prpTeam->CompressedPoolJobs[ilCPJ].OriginalJobs.GetSize();
				for(int ilOJ = (ilNumOriginalJobs-1); ilOJ >= 0; ilOJ--)
				{
					if(prpTeam->CompressedPoolJobs[ilCPJ].OriginalJobs[ilOJ].Urno == lpPoolJobUrno)
					{
						prpTeam->CompressedPoolJobs[ilCPJ].OriginalJobs.RemoveAt(ilOJ);
					}
				}
			}
			int ilNumPoolJobs = prpTeam->PoolJobs.GetSize();
			for(int ilPJ = (ilNumPoolJobs-1); ilPJ >= 0; ilPJ--)
			{
				if(prpTeam->PoolJobs[ilPJ].Urno == lpPoolJobUrno)
				{
					prpTeam->PoolJobs.RemoveAt(ilPJ);
				}
			}


			//long llOldTeamUrno = prpTeam->TeamLeaderPoolJobUrno;
			ResetTeamInfo(prpTeam);
			if(prpTeam->IsCompressed)
			{
				//MakeCompressedPoolJobsForTeam(prpTeam, false, llOldTeamUrno);
				MakeCompressedPoolJobsForTeam(prpTeam, false);
//				if(prpTeam->PoolJobs.GetSize() <= 1)
//				{
//					prpTeam->IsCompressed = false;
//					prpTeam->CompressedPoolJobs.DeleteAll();
//					prpTeam->CompressedTempAbsences.DeleteAll();
//					prpTeam->CompressedJobs.DeleteAll();
//				}
			}
		}
	}

	return prlTeam; // set to NULL if the team was deleted
}

void StaffDiagramViewer::DeleteCompressedJobsForTeam(TEAM_DATA *prpTeam, bool bpDeleteBars /* = true */, bool bpUpdateLine /* = true */)
{
	if(prpTeam != NULL)
	{
		int ilNumCompressedJobs = prpTeam->CompressedJobs.GetSize();
		for(int ilCompressedJob = (ilNumCompressedJobs-1); ilCompressedJob >= 0; ilCompressedJob--)
		{
			COMPRESSED_JOBDATA *prlCompressedJob = &prpTeam->CompressedJobs[ilCompressedJob];
			DeleteCompressedJob(prlCompressedJob, bpDeleteBars, bpUpdateLine);
		}
		prpTeam->CompressedJobs.DeleteAll();
	}
}

void StaffDiagramViewer::DeleteCompressedJob(COMPRESSED_JOBDATA *prpCompressedJob, bool bpDeleteBars /* = true */, bool bpUpdateLine /* = true */)
{
	int ilNumOriginalJobs = prpCompressedJob->OriginalJobs.GetSize();
	for(int ilOJ = 0; ilOJ < ilNumOriginalJobs; ilOJ++)
	{
		JOBDATA *prlOriginalJob = &prpCompressedJob->OriginalJobs[ilOJ];
		omJobUrnoToTeamMap.RemoveKey((void *)prlOriginalJob->Urno);
	}
	prpCompressedJob->OriginalJobs.RemoveAll();

	int ilGroupno, ilLineno, ilBarno;
	if(bpDeleteBars && FindBar(prpCompressedJob->Data.Urno, ilGroupno, ilLineno, ilBarno))
	{
		int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
		DeleteBar(ilGroupno, ilLineno, ilBarno);
		if(MessagingActivated())
		{
			LONG lParam = MAKELONG(ilLineno, ilGroupno);
			if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
			else
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
		}
	}
}

TEAM_DATA *StaffDiagramViewer::GetMatchingTeam(JOBDATA *prpPoolJob, CString opTeamName)
{
	TEAM_DATA *prlTeam = NULL;

	if(prpPoolJob != NULL)
	{
		if(opTeamName.IsEmpty())
		{
			opTeamName = ogBasicData.GetTeamForPoolJob(prpPoolJob);
		}

		// need to test if the shift overlaps and not the pool job because if the shift has an abordnung
		// the second pool job in the shift messes up the calculations
		SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpPoolJob->Shur);
		if(prlShift != NULL && !opTeamName.IsEmpty())
		{
			int ilGroupNo = FindGroup(prpPoolJob->Alid, opTeamName, prlShift->Sfca);
			if(ilGroupNo != -1)
			{
				int ilNumTeams = omTeams.GetSize();
				for(int ilTeam = 0; prlTeam == NULL && ilTeam < ilNumTeams; ilTeam++)
				{
					TEAM_DATA *prlTmpTeam = &omTeams[ilTeam];
					if(prlTmpTeam->TeamName == opTeamName && prlTmpTeam->GroupNo == ilGroupNo &&
						(IsOverlapped(prlShift->Avfa,prlShift->Avta,prlTmpTeam->Begin,prlTmpTeam->End) || 
						 IsOverlapped(prpPoolJob->Acfr,prpPoolJob->Acto,prlTmpTeam->Begin,prlTmpTeam->End)))
					{
						prlTeam = prlTmpTeam;
					}
				}
			}
		}
	}

	return prlTeam;
}

TEAM_DATA *StaffDiagramViewer::GetTeamByPoolJobUrno(long lpPoolJobUrno)
{
	TEAM_DATA *prlTeam = NULL;
	omJobUrnoToTeamMap.Lookup((void *) lpPoolJobUrno, (void *&) prlTeam);
	return prlTeam;
}

TEAM_DATA *StaffDiagramViewer::GetTeamByJobUrno(long lpJobUrno)
{
	TEAM_DATA *prlTeam = NULL;
	omJobUrnoToTeamMap.Lookup((void *) lpJobUrno, (void *&) prlTeam);
	return prlTeam;
}

TEAM_DATA *StaffDiagramViewer::AddToTeam(JOBDATA *prpPoolJob, CString opTeamName)
{
	TEAM_DATA *prlTeam = NULL;
	if(prpPoolJob != NULL && GetTeamByPoolJobUrno(prpPoolJob->Urno) == NULL)
	{
		// if not already in the group (can happen when the group is expanded and the pool job is reinserted)
		if(opTeamName.IsEmpty())
		{
			opTeamName = ogBasicData.GetTeamForPoolJob(prpPoolJob);
		}

		if(!opTeamName.IsEmpty())
		{
			prlTeam = GetMatchingTeam(prpPoolJob, opTeamName);
			if(prlTeam != NULL)
			{
				AddPoolJobToTeam(prlTeam,prpPoolJob);
			}
			else
			{
				AddTeam(prpPoolJob, opTeamName);
			}
		}
	}

	return prlTeam;
}

void StaffDiagramViewer::AddPoolJobToTeam(TEAM_DATA *prpTeam, JOBDATA *prpPoolJob)
{
	if(prpTeam != NULL && prpPoolJob != NULL)
	{
		if(SetTeamInfoForPoolJob(prpTeam, prpPoolJob))
		{
			prpTeam->PoolJobs.Add(prpPoolJob);
			omJobUrnoToTeamMap.SetAt((void *) prpPoolJob->Urno, prpTeam);
		}
	}
}

void StaffDiagramViewer::ResetTeamInfo(TEAM_DATA *prpTeam)
{
	if(prpTeam != NULL)
	{
		long llOldUrno = HasPlusMinusButton(prpTeam);

		prpTeam->OverStaffed = false;
		prpTeam->UnderStaffed = false;
		prpTeam->TeamLeaderPoolJobUrno = 0L;
		prpTeam->TeamLeaderSortString.Empty();
		prpTeam->NameOfFirstEmp.Empty();
		prpTeam->FirstEmpPoolJobUrno = 0L;
		prpTeam->Begin = TIMENULL;
		prpTeam->End = TIMENULL;
		prpTeam->SortStartDate = TIMENULL;
		prpTeam->SortEndDate = TIMENULL;
		for(int i = 0; i < prpTeam->PoolJobs.GetSize(); i++)
		{
			SetTeamInfoForPoolJob(prpTeam, &prpTeam->PoolJobs[i]);
		}

		UpdateDisplayLineForUrnoChange(llOldUrno, prpTeam);
	}
}

// TeamLeaderPoolJobUrno may have changed, reset the display line's URNO in order to be able to find it again
void StaffDiagramViewer::UpdateDisplayLineForUrnoChange(long lpOldUrno, TEAM_DATA *prpTeam)
{
	if(prpTeam->IsCompressed)
	{
		long llNewUrno = HasPlusMinusButton(prpTeam);
		if(lpOldUrno != llNewUrno)
		{
			STAFF_LINEDATA *prlLine = NULL;
			int ilGroupno = -1, ilLineno = -1;
			if(FindGroupAndLine(lpOldUrno, ilGroupno, ilLineno))
			{
				prlLine = GetLine(ilGroupno,ilLineno);
				prlLine->JobUrno = llNewUrno;
			}
		}
	}
}

bool StaffDiagramViewer::SetTeamInfoForPoolJob(TEAM_DATA *prpTeam, JOBDATA *prpPoolJob)
{
	bool blRc = false;
	if(prpTeam != NULL && prpPoolJob != NULL)
	{
		long llOldUrno = HasPlusMinusButton(prpTeam);

		SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpPoolJob->Shur);
		EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prpPoolJob->Ustf);
		TEAM_DATA *prlExistingTeam = NULL; //GetTeamByPoolJobUrno(prpPoolJob->Urno);
		if(prlEmp != NULL && prlShift != NULL && prlExistingTeam == NULL)
		{
			blRc = true;
			if(ogJobData.PoolJobIsDelegation(prpPoolJob))
			{
				prpTeam->OverStaffed = true;
			}
			else if(ogJobData.PoolJobIsDelegated(prpPoolJob->Urno) || (prpTeam->Begin != TIMENULL && (prpTeam->Begin != prlShift->Avfa || prpTeam->End != prlShift->Avta)))
			{
				// if all team members don't have the same time then a '-' is displayed for a compressed team leader
				prpTeam->UnderStaffed = true;
			}

			CString olName;
			olName.Format("%s",ogEmpData.GetEmpName(prlEmp));

			if(ogBasicData.IsGroupLeader(prpPoolJob))
			{
				CString olTeamLeaderSortString;
				olTeamLeaderSortString.Format("%s%s%s", prpPoolJob->Acfr.Format("%Y%m%d%H%M%S"), prpPoolJob->Acto.Format("%Y%m%d%H%M%S"), olName);
				if(prpTeam->TeamLeaderPoolJobUrno == 0L || prpTeam->TeamLeaderSortString > olTeamLeaderSortString)
				{
					prpTeam->TeamLeaderPoolJobUrno = prpPoolJob->Urno; // results in the emp being displayed in blue
					prpTeam->TeamLeaderSortString = olTeamLeaderSortString;
				}
			}

			// either this is the employee with the earliest shift or if the shift is the same then the emp is first alphabetically
			if(prpTeam->FirstEmpPoolJobUrno == 0L || (prpTeam->Begin > prlShift->Avfa || (prpTeam->Begin == prlShift->Avfa && olName < prpTeam->NameOfFirstEmp)))
			{
				// this emp will have the +/- button even if they are not really the team leader (ie a team without team leader can still be compressed)
				prpTeam->FirstEmpPoolJobUrno = prpPoolJob->Urno;
				prpTeam->NameOfFirstEmp = olName;
				prpTeam->Begin = prlShift->Avfa;
				prpTeam->End = prlShift->Avta;
			}
			if(prpTeam->SortStartDate == TIMENULL || prpTeam->SortStartDate > prpPoolJob->Acfr)
			{
				prpTeam->SortStartDate = prpPoolJob->Acfr;
			}
			if(prpTeam->SortEndDate == TIMENULL || prpTeam->SortEndDate < prpPoolJob->Acto)
			{
				prpTeam->SortEndDate = prpPoolJob->Acto;
			}
		}

		UpdateDisplayLineForUrnoChange(llOldUrno, prpTeam);
	}


	return blRc;
}

TEAM_DATA *StaffDiagramViewer::AddTeam(JOBDATA *prpPoolJob, CString opTeamName)
{
	TEAM_DATA *prlTeam = NULL;
	if(prpPoolJob != NULL)
	{
		if(opTeamName.IsEmpty())
		{
			opTeamName = ogBasicData.GetTeamForPoolJob(prpPoolJob);
		}

		if(!opTeamName.IsEmpty())
		{
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpPoolJob->Shur);
			if(prlShift != NULL)
			{
				int ilGroupNo = FindGroup(prpPoolJob->Alid, opTeamName, prlShift->Sfca);
				if(ilGroupNo != -1)
				{
					prlTeam = new TEAM_DATA;
					prlTeam->TeamName = opTeamName;
					prlTeam->IsCompressed = TeamWasCompressed(prpPoolJob);
					prlTeam->GroupNo = ilGroupNo;
					AddPoolJobToTeam(prlTeam,prpPoolJob);
					omTeams.Add(prlTeam);
				}
			}
		}
	}

	return prlTeam;
}

// on receiving a new pool job, check it is really
// new, if so recreate the compressed pool job for this team
STAFF_LINEDATA *StaffDiagramViewer::AddCompressedPoolJobToTeam(JOBDATA *prpPoolJob, bool bpAddOnlyWhenNotFound /*true*/)
{
	STAFF_LINEDATA *prlLine = NULL;

	if(prpPoolJob != NULL)
	{
		TEAM_DATA *prlTeam = GetTeamByPoolJobUrno(prpPoolJob->Urno);
		if(prlTeam != NULL)
		{
			if(!bpAddOnlyWhenNotFound || !PoolJobIsInCompressedTeam(prlTeam,prpPoolJob->Urno))
			{
				prlLine = MakeCompressedPoolJobsForTeam(prlTeam);
			}
		}
	}

	return prlLine;
}

bool StaffDiagramViewer::PoolJobIsInCompressedTeam(TEAM_DATA *prpTeam, long lpPoolJobUrno)
{
	bool blPoolJobIsInCompressedTeam = false;
	if(prpTeam != NULL)
	{
		int ilNumCompressedPoolJobs = prpTeam->CompressedPoolJobs.GetSize();
		for(int ilCPJ = 0; ilCPJ < ilNumCompressedPoolJobs; ilCPJ++)
		{
			int ilNumOriginalJobs = prpTeam->CompressedPoolJobs[ilCPJ].OriginalJobs.GetSize();
			for(int ilOJ = 0; ilOJ < ilNumOriginalJobs; ilOJ++)
			{
				if(prpTeam->CompressedPoolJobs[ilCPJ].OriginalJobs[ilOJ].Urno == lpPoolJobUrno)
				{
					blPoolJobIsInCompressedTeam = true;
					break;
				}
			}
		}
	}
	return blPoolJobIsInCompressedTeam;
}

// lpJobUrno - either a job or a pool job
bool StaffDiagramViewer::IsTeamCompressed(long lpJobUrno)
{
	bool blIsCompressed = false;
	TEAM_DATA *prlTeam = GetTeamByPoolJobUrno(lpJobUrno);
	if(prlTeam == NULL)
	{
		prlTeam = GetTeamByJobUrno(lpJobUrno);
	}

	if(prlTeam != NULL)
	{
		blIsCompressed = prlTeam->IsCompressed;
	}

	return blIsCompressed;
}


bool StaffDiagramViewer::GetJobsForCompressedJob(long lpJobUrno, CCSPtrArray <JOBDATA> &ropJobs)
{
	TEAM_DATA *prlTeam = GetTeamByJobUrno(lpJobUrno);
	if(prlTeam != NULL && prlTeam->IsCompressed)
	{
		COMPRESSED_JOBDATA *prlCompressedJob = FindCompressedJob(prlTeam, lpJobUrno);
		if(prlCompressedJob != NULL)
		{
			int ilNumJobs = prlCompressedJob->OriginalJobs.GetSize();
			for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
			{
				ropJobs.Add(&prlCompressedJob->OriginalJobs[ilJob]);
			}
		}
	}

	return ropJobs.GetSize() > 0 ? true : false;
}

bool StaffDiagramViewer::GetPoolJobsForCompressedPoolJob(long lpPoolJobUrno, CCSPtrArray <JOBDATA> &ropPoolJobs)
{
	TEAM_DATA *prlTeam = GetTeamByPoolJobUrno(lpPoolJobUrno);
	if(prlTeam != NULL && prlTeam->IsCompressed)
	{
		int ilNumCompressedPoolJobs = prlTeam->CompressedPoolJobs.GetSize();
		for(int ilCPJ = 0; ilCPJ < ilNumCompressedPoolJobs; ilCPJ++)
		{
			COMPRESSED_JOBDATA *prlCompressedPoolJob = &prlTeam->CompressedPoolJobs[ilCPJ];
			int ilNumOrigJobs = prlCompressedPoolJob->OriginalJobs.GetSize();
			for(int ilOrigJob = 0; ilOrigJob < ilNumOrigJobs; ilOrigJob++)
			{
				ropPoolJobs.Add(&prlCompressedPoolJob->OriginalJobs[ilOrigJob]);
			}
		}
	}

	return ropPoolJobs.GetSize() > 0 ? true : false;
}

// true if this has a team leader present
bool StaffDiagramViewer::IsTeamLeader(long lpPoolJobUrno)
{
	bool blIsTeamLeader = false;
	TEAM_DATA *prlTeam = GetTeamByPoolJobUrno(lpPoolJobUrno);
	if(prlTeam != NULL && prlTeam->TeamLeaderPoolJobUrno == lpPoolJobUrno)
	{
		blIsTeamLeader = true;
	}

	return blIsTeamLeader;
}


bool StaffDiagramViewer::AreInSameTeam(long lpPoolJobUrno1, long lpPoolJobUrno2)
{
	bool blAreInSameTeam = false;
	TEAM_DATA *prlTeam = GetTeamByPoolJobUrno(lpPoolJobUrno1);
	if(prlTeam != NULL)
	{
		int ilNumPoolJobs = prlTeam->PoolJobs.GetSize();
		for(int ilPoolJob = 0; !blAreInSameTeam && ilPoolJob < ilNumPoolJobs; ilPoolJob++)
		{
			if(prlTeam->PoolJobs[ilPoolJob].Urno == lpPoolJobUrno2)
			{
				blAreInSameTeam = true;
			}
		}
	}

	return blAreInSameTeam;
}

void StaffDiagramViewer::GetTeamSortTimes(long lpPoolJobUrno, CTime &ropStartTime, CTime &ropEndTime)
{
	ropStartTime = TIMENULL;
	ropEndTime = TIMENULL;
	TEAM_DATA *prlTeam = GetTeamByPoolJobUrno(lpPoolJobUrno);
	if(prlTeam != NULL)
	{
		ropStartTime = prlTeam->SortStartDate;
		ropEndTime = prlTeam->SortEndDate;
	}
}

// loop through all lines remembering the compressed/expanded state of the groups - so that when the list is recreated
// the group will be displayed as before
void StaffDiagramViewer::RememberCompressedTeams()
{
	omCompressedTeamMap.RemoveAll();

	int ilNumTeams = omTeams.GetSize();
	for(int ilTeam = 0; ilTeam < ilNumTeams; ilTeam++)
	{
		TEAM_DATA *prlTeam = &omTeams[ilTeam];
		if(prlTeam->IsCompressed)
		{
			CString olShiftAndPoolKey;
			int ilNumPoolJobs = prlTeam->PoolJobs.GetSize();
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				// only a combination of shift (DRR.URNO) and pool (POLTAB.URNO) make up a valid key
				JOBDATA *prlPoolJob = &prlTeam->PoolJobs[ilPoolJob];
				olShiftAndPoolKey.Format("<%d><%d><%s>",prlPoolJob->Shur,prlPoolJob->Uaid,ogBasicData.GetTeamForPoolJob(prlPoolJob));
				omCompressedTeamMap.SetAt(olShiftAndPoolKey, (void *) NULL);
			}
		}
	}
}

bool StaffDiagramViewer::TeamWasCompressed(JOBDATA *prpPoolJob)
{
	bool blTeamWasCompressed = false;
	if(prpPoolJob != NULL)
	{
		CString olShiftAndPoolKey;
		void *pvlDummy;
		olShiftAndPoolKey.Format("<%d><%d><%s>",prpPoolJob->Shur,prpPoolJob->Uaid,ogBasicData.GetTeamForPoolJob(prpPoolJob));
		blTeamWasCompressed = omCompressedTeamMap.Lookup(olShiftAndPoolKey, (void *&) pvlDummy) ? true : false;
	}
	return blTeamWasCompressed;
}

void StaffDiagramViewer::ExpandOrCompressTeam(int ipGroup, int ipLine)
{
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		pomAttachWnd->SendMessage(WM_REMEMBERLISTPOSITIONS, 0, 0);
	}

	int ilGroupCount = omGroups.GetSize();
	if(ipGroup < ilGroupCount)
	{
		int ilLineCount = omGroups[ipGroup].Lines.GetSize();
		if(ipLine < ilLineCount)
		{
			TEAM_DATA *prlTeam = GetTeamByPoolJobUrno(omGroups[ipGroup].Lines[ipLine].JobUrno);
			if(prlTeam != NULL)
			{
				prlTeam->IsCompressed = !prlTeam->IsCompressed;

				if(prlTeam->IsCompressed)
				{
					// delete existing lines and create a compressed pool job with compressed jobs
					int ilNumPoolJobs = prlTeam->PoolJobs.GetSize();
					for(int ilPoolJob = (ilNumPoolJobs-1); ilPoolJob >= 0; ilPoolJob--)
					{
						JOBDATA *prlPoolJob = &prlTeam->PoolJobs[ilPoolJob];
						int ilLineno, ilGroupno;
						if(FindDutyBar(prlPoolJob->Urno, ilGroupno, ilLineno))
						{
//						int ilLine = FindPoolJobLine(prlPoolJob->Urno, ipGroup);
//						if(ilLine != -1)
//						{
							if(ogBasicData.IsPaxServiceT2() == true)
							{
								if(ipGroup == ilGroupno)
								{
							        DeleteLine(ilGroupno, ilLineno);
								}
							}
							else
							{
								DeleteLine(ilGroupno, ilLineno);
							}
							if(MessagingActivated())
							{
								LONG lParam = MAKELONG(ilLineno, ilGroupno);
								pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_DELETELINE, lParam);
							}
						}
					}

					if(ilNumPoolJobs > 0)
					{
						// prlTeam->IsCompressed is true, so MakePoolJob() will make a compressed pool job
						// although this function call is within a loop, it will create the compressed pool
						// job on the first call only, on subsequent calls it checks if the compressed pool
						// job already exists and if so does nothing
						MakePoolJob(&prlTeam->PoolJobs[0]);
					}

					int ilGroupno, ilLineno;
					long llCompressedEmpPoolJobUrno = HasPlusMinusButton(prlTeam);

					if(FindDutyBar(llCompressedEmpPoolJobUrno, ilGroupno, ilLineno))
					{
						if(MessagingActivated())
						{
							LONG lParam = MAKELONG(ilLineno, ilGroupno);
							pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, lParam);
						}
					}

					MakeCompressedJobsForTeam(prlTeam);
				}
				else
				{
					DeleteCompressedJobsForTeam(prlTeam);
					DeleteCompressedPoolJobsForTeam(prlTeam);
					CCSPtrArray <JOBDATA> olPoolJobs;
					int ilNumPoolJobs = prlTeam->PoolJobs.GetSize(), ilPoolJob;
					for(ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
					{
						olPoolJobs.Add(&prlTeam->PoolJobs[ilPoolJob]);
					}
					prlTeam->PoolJobs.RemoveAll(); // will be recreated below in MakePool()

					for(ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
					{
						JOBDATA *prlPoolJob = &olPoolJobs[ilPoolJob];
						MakePoolJob(prlPoolJob);
						int ilInsertedLine = FindPoolJobLine(prlPoolJob->Urno, ipGroup);
						//int ilInsertedLine;
						//FindDutyBar(prlPoolJob->Urno, ilGroupno, ilInsertedLine);

						CCSPtrArray <JOBDATA> olJobs;
						ogJobData.GetJobsByPoolJob(olJobs,prlPoolJob->Urno);
						int ilNumJobs = olJobs.GetSize();
						for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
						{
							JOBDATA *prlJob = &olJobs[ilJob];
							bool blIsPause = !strcmp(prlJob->Jtco, JOBBREAK) ? true : false;
							int ilGroupno, ilLineno;
							if(FindDutyBar(prlJob->Jour, ilGroupno, ilLineno, blIsPause))	// corresponding duty bar found?
							{
								if(ilGroupno == ipGroup && (ilInsertedLine == -1 || ilLineno == ilInsertedLine))
								{
									MakeBar(ilGroupno, ilLineno, prlJob);
								}
							}
						}
						if(ilInsertedLine != -1)
						{
							STAFF_LINEDATA *prlLine = GetLine(ipGroup, ilInsertedLine);
							bool blTAFound = false;
							CCSPtrArray <JOBDATA> olTempAbsences;
							ogJobData.GetJobsByPkno(olTempAbsences,prlPoolJob->Peno,JOBTEMPABSENCE);
							int ilNumTA = olTempAbsences.GetSize();
						
		
							for(int ilTA = 0; ilTA < ilNumTA; ilTA++)
							{
								MakeTempAbsenceBkBar(prlLine, &olTempAbsences[ilTA]);
								blTAFound = true;
							}
							if(blTAFound)
							{
								prlLine->BkBars.Sort(ByType);
							}

							if(MessagingActivated())
							{
								LONG lParam = MAKELONG(ilInsertedLine, ipGroup);
								pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, lParam);
							}
						}
					}
				}
			}
		}
	}
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		pomAttachWnd->SendMessage(WM_RESTORELISTPOSITIONS, 0, 0);
	}
}

int StaffDiagramViewer::FindPoolJobLine(long lpPoolJobUrno, int ipGroup)
{
	int ilLine = -1;
	int ilNumLines = GetLineCount(ipGroup);
	for (int ilLineno = 0; ilLine == -1 && ilLineno < ilNumLines; ilLineno++)
	{
		STAFF_LINEDATA *prlLine = GetLine(ipGroup, ilLineno);
		if(prlLine->JobUrno == lpPoolJobUrno)
		{
			ilLine = ilLineno;
		}
	}
	return ilLine;
}

// loop through team members, displaying jobs for those teams that are compressed (only the team leader is displayed)
void StaffDiagramViewer::MakeCompressedJobsForTeam(TEAM_DATA *prpTeam)
{
	// map of aloc + alids   eg  key is "<GAT><A01>"
//	CMapStringToPtr olAlocAlidMap;
//	CString olAlocAlidKey;
	int ilGroupno, ilLineno;

	if(prpTeam != NULL && prpTeam->IsCompressed)
	{
		DeleteCompressedJobsForTeam(prpTeam);

		long llCompressedEmpPoolJobUrno = HasPlusMinusButton(prpTeam);
		if(FindDutyBar(llCompressedEmpPoolJobUrno, ilGroupno, ilLineno))
		{
			CCSPtrArray <JOBDATA> olJobs;

			int ilNumPoolJobs = prpTeam->PoolJobs.GetSize();
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				JOBDATA *prlPoolJob = &prpTeam->PoolJobs[ilPoolJob];
				ogJobData.GetJobsByPoolJob(olJobs, prlPoolJob->Urno, false);
			}

			olJobs.Sort(ByJobStartTime);
			int ilNumJobs = olJobs.GetSize();
			for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
			{
				AddCompressedJob(prpTeam,&olJobs[ilJob]);
			}
		}

		int ilNumCompressedJobs = prpTeam->CompressedJobs.GetSize();
		for(int ilCompressedJob = 0; ilCompressedJob < ilNumCompressedJobs; ilCompressedJob++)
		{
			COMPRESSED_JOBDATA *prlCompressedJob = &prpTeam->CompressedJobs[ilCompressedJob];
			bool blIsPause = !strcmp(prlCompressedJob->Data.Jtco, JOBBREAK) ? true : false;
			int ilGroupno, ilLineno;
			if(FindDutyBar(llCompressedEmpPoolJobUrno, ilGroupno, ilLineno, blIsPause))	// corresponding duty bar found?
			{
				MakeCompressedBar(ilGroupno, ilLineno, prlCompressedJob);
			}
		}
		if(FindDutyBar(llCompressedEmpPoolJobUrno, ilGroupno, ilLineno) && MessagingActivated())
		{
			LONG lParam = MAKELONG(ilLineno, ilGroupno);
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINEHEIGHT, lParam);
		}
	}
}


// loop through team members, displaying a single pool job (or pool jobs if there are Abordnungen)
// for those teams that are compressed (only the team leader is displayed)
// lpOldTeamUrno - required when the original pool job that was the main URNO is deleted from the team - required for line updates
STAFF_LINEDATA *StaffDiagramViewer::MakeCompressedPoolJobsForTeam(TEAM_DATA *prpTeam, bool bpUpdate /*=true*/, long lpOldTeamUrno /* = 0L */)
{
	STAFF_LINEDATA *prlLine = NULL;

	if(prpTeam != NULL && prpTeam->IsCompressed)
	{
		// reset the compressed data
		DeleteCompressedPoolJobsForTeam(prpTeam,bpUpdate,bpUpdate);

		// loop through all pool jobs, creating a new single pool job
		int ilNumPoolJobs = prpTeam->PoolJobs.GetSize();
		for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
		{
			JOBDATA *prlPoolJob = &prpTeam->PoolJobs[ilPoolJob];
			AddCompressedPoolJob(prpTeam,prlPoolJob);

			CCSPtrArray <JOBDATA> olTempAbsences;
			ogJobData.GetJobsByPkno(olTempAbsences,prlPoolJob->Peno,JOBTEMPABSENCE);
			int ilNumTA = olTempAbsences.GetSize();
			for(int ilTA = 0; ilTA < ilNumTA; ilTA++)
			{
				AddCompressedTempAbsence(prpTeam,&olTempAbsences[ilTA]);
			}
		}

		int ilNumCompressedPoolJobs = prpTeam->CompressedPoolJobs.GetSize();
		if(ilNumCompressedPoolJobs > 0)
		{
			long llCompressedEmpPoolJobUrno = HasPlusMinusButton(prpTeam);

			// count how many emps are in this group and get the first pool job for the team leader
			JOBDATA *prlTeamLeaderPoolJob = NULL;
			CString olUstfList, olUstf;
			int ilNumEmpsInThisTeam = 0;
			bool blAllShiftsAreSame = true;
			for(int ilCPJ = 0; ilCPJ < ilNumCompressedPoolJobs; ilCPJ++)
			{
				int ilNumOrigJobs = prpTeam->CompressedPoolJobs[ilCPJ].OriginalJobs.GetSize();
				for(int ilOrigJob = 0; ilOrigJob < ilNumOrigJobs; ilOrigJob++)
				{
					JOBDATA *prlPoolJob = &prpTeam->CompressedPoolJobs[ilCPJ].OriginalJobs[ilOrigJob];

					// get the team leader pool job
					if(prlTeamLeaderPoolJob == NULL && prlPoolJob->Urno == llCompressedEmpPoolJobUrno)
					{
						prlTeamLeaderPoolJob = prlPoolJob;
					}


					// count how many emps there are in this compressed team
					olUstf.Format("+%d+",prlPoolJob->Ustf);
					if(olUstfList.Find(olUstf) == -1)
					{
						olUstfList += olUstf;
						ilNumEmpsInThisTeam++;
					}
				}
			}

			if(lpOldTeamUrno == 0L)
			{
				lpOldTeamUrno = llCompressedEmpPoolJobUrno;
			}

			// create the line for the team leader to display the compressed pool job
			if(prlTeamLeaderPoolJob != NULL)
			{
				SHIFTDATA *prlShift = NULL;
				EMPDATA *prlEmp = NULL;
				prlShift = ogShiftData.GetShiftByUrno(prlTeamLeaderPoolJob->Shur);
				if(prlShift != NULL)
				{
					prlEmp = ogEmpData.GetEmpByPeno(prlShift->Peno);
				}

				if(prlEmp != NULL)
				{
					CString olTeamName = ogBasicData.GetTeamForPoolJob(prlTeamLeaderPoolJob);
					CString olFunction = ogBasicData.GetFunctionForPoolJob(prlTeamLeaderPoolJob);

					CStringArray olPermits;
					ogBasicData.GetPermitsForPoolJob(prlTeamLeaderPoolJob,olPermits);

					STAFF_LINEDATA *prlNewLine = new STAFF_LINEDATA;
					prlNewLine->JobUrno = llCompressedEmpPoolJobUrno;
					prlNewLine->StfUrno = prlTeamLeaderPoolJob->Ustf;
					prlNewLine->ShiftAcfr = prlShift->Avfa;
					prlNewLine->ShiftActo = prlShift->Avta;
					prlNewLine->ShiftStid = prlShift->Sfca;
					prlNewLine->ShiftTmid = olTeamName;
					prlNewLine->EmpName = ogEmpData.GetEmpName(prlEmp);
					prlNewLine->EmpRank = olFunction;
					CString olColour;
					if(ogDlgSettings.GetValue("FUNCTIONCOLOURS", olFunction, olColour))
					{
						prlNewLine->TextColor = (COLORREF) atol(olColour);
						prlNewLine->TextColorDefined = true;
					}
					CString olTeamStrength;
					// if all team members don't have the same shift time or are delegated elsewhere then a '-' is displayed for a compressed team leader
					if(prpTeam->UnderStaffed)
					{
						olTeamStrength += "-";
					}
					// if one or more team members are loaned to this team from another team then display a '+'
					if(prpTeam->OverStaffed)
					{
						if(!olTeamStrength.IsEmpty())
						{
							olTeamStrength += "/";
						}
						olTeamStrength += "+";
					}
					if(!olTeamStrength.IsEmpty())
					{
						olTeamStrength += " ";
					}

					prlNewLine->Text.Format("%s / %d %s%s", olTeamName, ilNumEmpsInThisTeam, olTeamStrength, ogEmpData.GetEmpName(prlEmp));

					for(int ilCPJ = 0; ilCPJ < ilNumCompressedPoolJobs; ilCPJ++)
					{
						MakeCompressedBkBars(prlNewLine, &prpTeam->CompressedPoolJobs[ilCPJ], prlShift);
					}

					int ilNumCompressedTA = prpTeam->CompressedTempAbsences.GetSize();
					for(int ilCTA = 0; ilCTA < ilNumCompressedTA; ilCTA++)
					{
						MakeTempAbsenceBkBar(prlNewLine, &prpTeam->CompressedTempAbsences[ilCTA].Data);
					}
					prlNewLine->BkBars.Sort(ByType);

					int ilLineno = 0, ilGroupno = 0;
					long llUrno = HasPlusMinusButton(prpTeam);
					if(FindGroupAndLine(llUrno, ilGroupno, ilLineno))
					{
						prlLine = GetLine(ilGroupno,ilLineno);
						*prlLine = *prlNewLine;
						DeleteBarsForLine(prlNewLine);
						delete prlNewLine;
						MakeCompressedJobsForTeam(prpTeam);
					}
					else if((ilGroupno = FindGroup(prlTeamLeaderPoolJob->Alid, olTeamName, prlShift->Sfca)) > -1)
					{
						prlLine = CreateLine(ilGroupno, prlNewLine);
					}
				}
			}
		}
	}

	return prlLine;
}

void StaffDiagramViewer::MakeCompressedBarData(STAFF_BARDATA *prlBar, COMPRESSED_JOBDATA *prpCompressedJob)
{
	JOBDATA *prlJob = &prpCompressedJob->Data;

	prlBar->JobUrno = prlJob->Urno;
	if(!strcmp(prlJob->Jtco, JOBBREAK))
	{
		prlBar->Text = "";
		prlBar->StatusText = ogDataSet.JobBarText(prlJob) + CString(" ") + StatusText(prlJob);
		prlBar->IsPause = true;
		prlBar->PauseColour = ogColors[ogConflicts.GetJobConflictColorForCompressedTeam(prpCompressedJob->OriginalJobs)];
		prlBar->MarkerBrush = ogBrushs[21]; // break bitmap whose colour depends on prlBar->PauseColour
	}
	else
	{
		prlBar->Text = ogDataSet.JobBarText(prlJob);
		prlBar->StatusText = prlBar->Text + CString(" ") + StatusText(prlJob);
		prlBar->MarkerBrush = ogBrushs[ogConflicts.GetJobConflictColorForCompressedTeam(prpCompressedJob->OriginalJobs)];
		if(IsPrivateProfileOn("SHOW_STAFF_MARKER_FILTER","NO"))
			{
				FLIGHTDATA *prlFlt = ogFlightData.GetFlightByUrno(prlJob->Flur);
				if (prlFlt != NULL)
				{
					prlBar->FlightUrno = prlFlt->Urno;
					if(ogJobData.IsTurnaroundJob(prlJob))
					{
						prlBar->FlightType = STFBAR_TURNAROUND;						
					}
					else if(strcmp((char*)prlFlt->Adid, "A") == 0)
					{
						prlBar->FlightType = STFBAR_ARRIVAL;
					}
					else if (strcmp((char*)prlFlt->Adid, "D") == 0)
					{
						prlBar->FlightType = STFBAR_DEPARTURE;
					}	
				}
			}

	}
	prlBar->StartTime = prlJob->Acfr;
	prlBar->EndTime = prlJob->Acto;
	prlBar->FrameType = FRAMERECT;
	prlBar->FrameColor = (*prlJob->Ignr == '1') ? WHITE : BLACK;
	prlBar->MarkerType = (prlJob->Stat[0] == 'P')? MARKLEFT: MARKFULL;
	prlBar->Infm = (prlJob->Stat[0] == 'P' && prlJob->Infm) ? true : false;

	if(!bgOnline)
	{
		int ilNumJobs = prpCompressedJob->OriginalJobs.GetSize();
		for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
		{
			JOBDATA *prlJob = &prpCompressedJob->OriginalJobs[ilJ];
			if(ogJobData.JobChangedOffline(prlJob) || ogJodData.JodChangedOffline(prlJob->Urno))
			{
				prlBar->OfflineBar = true;
				break;
			}
		}
	}
}


void StaffDiagramViewer::MakeCompressedBar(int ipGroupno, int ipLineno, COMPRESSED_JOBDATA *prpCompressedJob)
{
	JOBDATA *prlJob = &prpCompressedJob->Data;
	STAFF_BARDATA rlBar;

	MakeCompressedBarData(&rlBar,prpCompressedJob);
	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);

	// We also have to create an indicator for showing the range of time on TimeScale
	int ilNumJobs = prpCompressedJob->OriginalJobs.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		JOBDATA *prlJob = &prpCompressedJob->OriginalJobs[ilJob];
		STAFF_INDICATORDATA rlIndicator;
		rlIndicator.StartTime = prlJob->Acfr;
		rlIndicator.EndTime = prlJob->Acto;
		rlIndicator.Color = ogColors[ogConflicts.GetJobConflictColor(prlJob->Urno)];
		CreateIndicator(ipGroupno, ipLineno, ilBarno, &rlIndicator);
	}
}


void StaffDiagramViewer::AddCompressedJob(TEAM_DATA *prpTeam, JOBDATA *prpJob)
{
	if(prpTeam != NULL && prpJob != NULL)
	{
		COMPRESSED_JOBDATA *prlCompressedJob = FindCompressedJobMatch(prpTeam, prpJob);
		if(prlCompressedJob == NULL)
		{
			// create a new job
			prlCompressedJob = new COMPRESSED_JOBDATA;
			prpTeam->CompressedJobs.Add(prlCompressedJob);
			prlCompressedJob->Data = *prpJob;
		}
		else
		{
			UpdateCompressedJob(prlCompressedJob,prpJob);
		}

		prlCompressedJob->OriginalJobs.Add(prpJob);

		// add to map to help find a compressed team if a job update is received
		omJobUrnoToTeamMap.SetAt((void *)prpJob->Urno,prpTeam);
	}
}

void StaffDiagramViewer::AddCompressedPoolJob(TEAM_DATA *prpTeam, JOBDATA *prpPoolJob)
{
	if(prpTeam != NULL && prpPoolJob != NULL)
	{
		COMPRESSED_JOBDATA *prlCompressedPoolJob = NULL;

		int ilNumCompressedPoolJobs = prpTeam->CompressedPoolJobs.GetSize(); // can be many for shift Abordnungen
		int ilCPJ = 0;
		bool blMakeNew = true;

		// find the correct overlapping pool job
		for(ilCPJ = 0; ilCPJ < ilNumCompressedPoolJobs; ilCPJ++)
		{
			if(prpPoolJob->Acfr <= prpTeam->CompressedPoolJobs[ilCPJ].Data.Acto)
			{
				blMakeNew = false;
				break;
			}
		}

		if(blMakeNew)
		{
			COMPRESSED_JOBDATA *prlCompressedPoolJob = new COMPRESSED_JOBDATA;
			prpTeam->CompressedPoolJobs.Add(prlCompressedPoolJob);
			prlCompressedPoolJob->Data = *prpPoolJob;
			prlCompressedPoolJob->Data.Urno = HasPlusMinusButton(prpTeam);
		}
		else
		{
			UpdateCompressedJob(&prpTeam->CompressedPoolJobs[ilCPJ],prpPoolJob);
		}

		prpTeam->CompressedPoolJobs[ilCPJ].OriginalJobs.Add(prpPoolJob);

		// add to map to help find a compressed team if a job update is received
		omJobUrnoToTeamMap.SetAt((void *)prpPoolJob->Urno,prpTeam);

	}
}

void StaffDiagramViewer::AddCompressedTempAbsence(TEAM_DATA *prpTeam, JOBDATA *prpTempAbsence)
{
	if(prpTeam != NULL && prpTempAbsence != NULL)
	{
		COMPRESSED_JOBDATA *prlCompressedTA = FindCompressedJobMatch(prpTeam, prpTempAbsence);
		if(prlCompressedTA == NULL)
		{
			// create a new job
			prlCompressedTA = new COMPRESSED_JOBDATA;
			prpTeam->CompressedTempAbsences.Add(prlCompressedTA);
			prlCompressedTA->Data = *prpTempAbsence;
		}
		else
		{
			UpdateCompressedJob(prlCompressedTA,prpTempAbsence);
		}

		strcpy(prlCompressedTA->Data.Text,"Absent");

		prlCompressedTA->OriginalJobs.Add(prpTempAbsence);

		// add to map to help find a compressed team if a job update is received
		omJobUrnoToTeamMap.SetAt((void *)prpTempAbsence->Urno,prpTeam);
	}
}

void StaffDiagramViewer::UpdateCompressedJob(COMPRESSED_JOBDATA *prpCompressedJob, JOBDATA *prpJob)
{
	if(prpCompressedJob != NULL && prpJob != NULL)
	{
		JOBDATA *prlExistingJob = &prpCompressedJob->Data;

		if(prlExistingJob->Acfr == TIMENULL || prlExistingJob->Acfr > prpJob->Acfr)
		{
			prlExistingJob->Acfr = prpJob->Acfr;
		}
		if(prlExistingJob->Acto == TIMENULL || prlExistingJob->Acto < prpJob->Acto)
		{
			prlExistingJob->Acto = prpJob->Acto;
		}
		strcpy(prlExistingJob->Act3,prpJob->Act3);
		strcpy(prlExistingJob->Alid,prpJob->Alid);
		strcpy(prlExistingJob->Aloc,prpJob->Aloc);
		strcpy(prlExistingJob->Chng,*prlExistingJob->Chng == '1' ? prlExistingJob->Chng : prpJob->Chng);
		// 'P' > 'C' > 'F'
		if(*prlExistingJob->Conf != 'P')
		{
			if(*prpJob->Conf != 'F')
			{
				strcpy(prlExistingJob->Conf,prpJob->Conf);
			}
		}
		// 'P' > 'C' > 'F'
		if(prlExistingJob->Stat[0] != 'P')
		{
			if(prpJob->Stat[0] != 'F')
			{
				prlExistingJob->Stat[0] = prpJob->Stat[0];
			}
		}
		prlExistingJob->Flur = ogBasicData.GetFlightUrnoForJob(prpJob);
		strcpy(prlExistingJob->Gate,prpJob->Gate);
		strcpy(prlExistingJob->Ignr,*prlExistingJob->Ignr == '1' ? prlExistingJob->Ignr : prpJob->Ignr);
		strcpy(prlExistingJob->Jtco,prpJob->Jtco);
		strcpy(prlExistingJob->Posi,prpJob->Posi);
		strcpy(prlExistingJob->Regn,prpJob->Regn);

		if(!strcmp(prpJob->Jtco,JOBPOOL))
		{
			// if one pool job is marked as absent then all are absent
			if(prpJob->Infm)
			{
				prlExistingJob->Infm = true;
			}
		}
		else if(!prpJob->Infm)
		{
			// non-pool jobs are EmpInformed only if all jobs are EmpInformed
			prlExistingJob->Infm = false;
		}
//		prlExistingJob->WhatIfUrno = (prlExistingJob->WhatIfUrno != 0L) ? prlExistingJob->WhatIfUrno : prpJob->WhatIfUrno;
	}
}


COMPRESSED_JOBDATA *StaffDiagramViewer::FindCompressedJobMatch(TEAM_DATA *prpTeam, JOBDATA *prpJob)
{
	COMPRESSED_JOBDATA *prlCompressedJob = NULL;
	if(prpTeam != NULL && prpJob != NULL)
	{
		int ilNumCompressedJobs = prpTeam->CompressedJobs.GetSize();
		for(int ilCompressedJob = 0; ilCompressedJob < ilNumCompressedJobs; ilCompressedJob++)
		{
			JOBDATA *prlExistingJob = &prpTeam->CompressedJobs[ilCompressedJob].Data;

			if(IsReallyOverlapped(prlExistingJob->Acfr, prlExistingJob->Acto, prpJob->Acfr, prpJob->Acto))
			{
				if(!strcmp(prpJob->Jtco,JOBBREAK))
				{
					// break jobs are combined into a compressed job if they have the same time
					if(!strcmp(prlExistingJob->Jtco,JOBBREAK) && prlExistingJob->Acfr == prpJob->Acfr && prlExistingJob->Acto == prpJob->Acto)
					{
						prlCompressedJob = &prpTeam->CompressedJobs[ilCompressedJob];
						break;
					}
				}
				else if(!strcmp(prlExistingJob->Aloc,prpJob->Aloc) && !strcmp(prlExistingJob->Jtco,prpJob->Jtco) &&	prlExistingJob->Flur == ogBasicData.GetFlightUrnoForJob(prpJob))
				{
					prlCompressedJob = &prpTeam->CompressedJobs[ilCompressedJob];
					break;
				}
			}
		}
	}

	return prlCompressedJob;
}

COMPRESSED_JOBDATA *StaffDiagramViewer::FindCompressedJob(TEAM_DATA *prpTeam, long lpJobUrno)
{
	COMPRESSED_JOBDATA *prlCompressedJob = NULL;
	if(prpTeam != NULL)
	{
		int ilNumCompressedJobs = prpTeam->CompressedJobs.GetSize();
		for(int ilCompressedJob = 0; prlCompressedJob == NULL && ilCompressedJob < ilNumCompressedJobs; ilCompressedJob++)
		{
			int ilNumJobs = prpTeam->CompressedJobs[ilCompressedJob].OriginalJobs.GetSize();
			for(int ilJob = 0; prlCompressedJob == NULL && ilJob < ilNumJobs; ilJob++)
			{
				if(prpTeam->CompressedJobs[ilCompressedJob].OriginalJobs[ilJob].Urno == lpJobUrno)
				{
					prlCompressedJob = &prpTeam->CompressedJobs[ilCompressedJob];
				}
			}
		}
	}

	return prlCompressedJob;
}

COMPRESSED_JOBDATA *StaffDiagramViewer::FindCompressedPoolJob(TEAM_DATA *prpTeam, long lpPoolJobUrno)
{
	COMPRESSED_JOBDATA *prlCompressedPoolJob = NULL;
	if(prpTeam != NULL)
	{
		int ilNumCompressedPoolJobs = prpTeam->CompressedPoolJobs.GetSize();
		for(int ilCompressedPoolJob = 0; prlCompressedPoolJob == NULL && ilCompressedPoolJob < ilNumCompressedPoolJobs; ilCompressedPoolJob++)
		{
			int ilNumPoolJobs = prpTeam->CompressedPoolJobs[ilCompressedPoolJob].OriginalJobs.GetSize();
			for(int ilPoolJob = 0; prlCompressedPoolJob == NULL && ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				if(prpTeam->CompressedPoolJobs[ilCompressedPoolJob].OriginalJobs[ilPoolJob].Urno == lpPoolJobUrno)
				{
					prlCompressedPoolJob = &prpTeam->CompressedPoolJobs[ilCompressedPoolJob];
				}
			}
		}
	}

	return prlCompressedPoolJob;
}

JOBDATA *StaffDiagramViewer::GetCompressedJobByNormalJobUrno(long lpJobUrno)
{
	JOBDATA *prlCompressedJob = NULL;
	TEAM_DATA *prlTeam = GetTeamByJobUrno(lpJobUrno);
	if(prlTeam != NULL)
	{
		COMPRESSED_JOBDATA *prlCompressedJobData = FindCompressedJob(prlTeam, lpJobUrno);
		if(prlCompressedJobData != NULL)
		{
			prlCompressedJob = &prlCompressedJobData->Data;
		}
	}

	return prlCompressedJob;
}

JOBDATA *StaffDiagramViewer::GetCompressedPoolJobByNormalPoolJobUrno(long lpPoolJobUrno)
{
	JOBDATA *prlCompressedPoolJob = NULL;
	TEAM_DATA *prlTeam = GetTeamByPoolJobUrno(lpPoolJobUrno);
	if(prlTeam != NULL)
	{
		COMPRESSED_JOBDATA *prlCompressedPoolJobData = FindCompressedPoolJob(prlTeam, lpPoolJobUrno);
		if(prlCompressedPoolJobData != NULL)
		{
			prlCompressedPoolJob = &prlCompressedPoolJobData->Data;
		}
	}

	return prlCompressedPoolJob;
}

BOOL StaffDiagramViewer::FindCompressedBarByJobUrno(long lpJobUrno, int &ripGroupno, int &ripLineno, int &ripBarno)
{
	BOOL blFound = FALSE;
	TEAM_DATA *prlTeam = GetTeamByJobUrno(lpJobUrno);
	if(prlTeam != NULL)
	{
		COMPRESSED_JOBDATA *prlCompressedJob = FindCompressedJob(prlTeam, lpJobUrno);
		if(prlCompressedJob != NULL)
		{
			blFound = FindBar(prlCompressedJob->Data.Urno, ripGroupno, ripLineno, ripBarno);
		}
	}
	return blFound;
}


// when the team is compressed only one emp is displayed
// if the team has a team leader then return the pool job URNO of the team leader
// otherwise return the pool job URNO of the first emnp displayed
long StaffDiagramViewer::HasPlusMinusButton(TEAM_DATA *prpTeam)
{
	long llCompressedEmpPoolJobUrno = 0L;
	if(prpTeam != NULL)
	{
		if(prpTeam->IsCompressed && prpTeam->TeamLeaderPoolJobUrno != 0L)
		{
			llCompressedEmpPoolJobUrno = prpTeam->TeamLeaderPoolJobUrno;
		}
		else
		{
			llCompressedEmpPoolJobUrno = prpTeam->FirstEmpPoolJobUrno;
		}
	}

	return llCompressedEmpPoolJobUrno;
}


CString StaffDiagramViewer::DumpTeamData(long lpPoolJobUrno)
{
	CString olText;
	TEAM_DATA *prlTeam = GetTeamByPoolJobUrno(lpPoolJobUrno);
	if(prlTeam != NULL)
	{
		olText.Format("Team Data 0x%x: TeamName <%s> TeamLeaderPoolJobUrno <%d> FirstEmpPoolJobUrno <%d> Begin <%s> End <%s> NameOfFirstEmp <%s> SortStartDate <%s> SortEndDate <%s>",
			prlTeam,prlTeam->TeamName,prlTeam->TeamLeaderPoolJobUrno,prlTeam->FirstEmpPoolJobUrno,prlTeam->Begin.Format("%H%M/%d"),prlTeam->End.Format("%H%M/%d"),prlTeam->NameOfFirstEmp,prlTeam->SortStartDate.Format("%Y%m%d%H%M%S"),prlTeam->SortEndDate.Format("%Y%m%d%H%M%S"));

		int ilNumJobs, ilJob, ilNumJobs2, ilJob2;

		ilNumJobs = prlTeam->PoolJobs.GetSize();
		if(ilNumJobs <= 0)
		{
			olText += "\nTeam has no pool jobs !";
		}
		else
		{
			olText += "\nTeam Pool Jobs:";
			for(ilJob = 0; ilJob < ilNumJobs; ilJob++)
			{
				olText += "\n-> " + ogJobData.Dump(prlTeam->PoolJobs[ilJob].Urno);
			}
		}

		if(prlTeam->IsCompressed)
		{
			ilNumJobs = prlTeam->CompressedPoolJobs.GetSize();
			if(ilNumJobs <= 0)
			{
				olText += "\nTeam has no compressed pool jobs !";
			}
			else
			{
				olText += "\nTeam Compressed Pool Jobs:";
				for(ilJob = 0; ilJob < ilNumJobs; ilJob++)
				{
					olText += "\n-> Data: " + ogJobData.Dump(prlTeam->CompressedPoolJobs[ilJob].Data.Urno);
					ilNumJobs2 = prlTeam->CompressedPoolJobs[ilJob].OriginalJobs.GetSize();
					for(ilJob2 = 0; ilJob2 < ilNumJobs2; ilJob2++)
					{
						olText += "\n--> OriginalJob: " + ogJobData.Dump(prlTeam->CompressedPoolJobs[ilJob].OriginalJobs[ilJob2].Urno);
					}
				}
			}
			ilNumJobs = prlTeam->CompressedJobs.GetSize();
			if(ilNumJobs <= 0)
			{
				olText += "\nTeam has no compressed jobs !";
			}
			else
			{
				olText += "\nTeam Compressed Jobs:";
				for(ilJob = 0; ilJob < ilNumJobs; ilJob++)
				{
					olText += "\n-> Data: " + ogJobData.Dump(prlTeam->CompressedJobs[ilJob].Data.Urno);
					ilNumJobs2 = prlTeam->CompressedJobs[ilJob].OriginalJobs.GetSize();
					for(ilJob2 = 0; ilJob2 < ilNumJobs2; ilJob2++)
					{
						olText += "\n--> OriginalJob: " + ogJobData.Dump(prlTeam->CompressedJobs[ilJob].OriginalJobs[ilJob2].Urno);
					}
				}
			}
		}
	}

	return olText;
}

void StaffDiagramViewer::CompressTeams(bool bpCompress)
{
	int ilNumTeams = omTeams.GetSize();
	for(int ilTeam = 0; ilTeam < ilNumTeams; ilTeam++)
	{
		TEAM_DATA *prlTeam = &omTeams[ilTeam];
		if(prlTeam->PoolJobs.GetSize() > 1)
			prlTeam->IsCompressed = bpCompress;
	}
//	int ilGroupCount = omGroups.GetSize();
//	for(int ilGroup = 0; ilGroup < ilGroupCount; ilGroup++)
//	{
//		int ilLineCount = omGroups[ilGroup].Lines.GetSize();
//		for(int ilLine = 0; ilLine < ilLineCount; ilLine++)
//		{
//			TEAM_DATA *prlTeam = GetTeamByPoolJobUrno(omGroups[ilGroup].Lines[ilLine].JobUrno);
//			if(prlTeam != NULL)
//			{
//				if(prlTeam->IsCompressed != bpCompress)
//				{
////					prlTeam->IsCompressed = !bpCompress;
//					ExpandOrCompressTeam(ilGroup, ilLine);
//				}
//			}
//		}
//	}
}

// given a pool job URNO -> find the relevant team and return the other pool jobs within the team
void StaffDiagramViewer::GetPoolJobsInTeam(long lpPoolJobUrno, CCSPtrArray <JOBDATA> &ropTeamPoolJobs, bool blReturnFirstPoolJobOnlyForEachEmp /*=false*/)
{
	TEAM_DATA *prlTeam = GetTeamByPoolJobUrno(lpPoolJobUrno);
	if(prlTeam != NULL)
	{
		CMapPtrToPtr olEmpPoolJobMap;
		JOBDATA *prlExistingPoolJob, *prlPoolJob;

		int ilNumPoolJobs = prlTeam->PoolJobs.GetSize();
		for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
		{
			prlPoolJob = &prlTeam->PoolJobs[ilPoolJob];

			// it is possible that each emp has more than one pool job in a team (ie for Abordnungen)
			if(blReturnFirstPoolJobOnlyForEachEmp)
			{
				// return only the first (earliest) pool job for each emp
				if(!olEmpPoolJobMap.Lookup((void *) prlPoolJob->Ustf, (void *&) prlExistingPoolJob) || prlExistingPoolJob->Acfr > prlPoolJob->Acfr)
				{
					olEmpPoolJobMap.SetAt((void *) prlPoolJob->Ustf, prlPoolJob);
				}
			}
			else
			{
				ropTeamPoolJobs.Add(prlPoolJob);
			}
		}

		if(blReturnFirstPoolJobOnlyForEachEmp)
		{
			long llUstf;
			for(POSITION rlPos = olEmpPoolJobMap.GetStartPosition(); rlPos != NULL; )
			{
				olEmpPoolJobMap.GetNextAssoc(rlPos, (void *&)llUstf, (void *&)prlPoolJob);
				ropTeamPoolJobs.Add(prlPoolJob);
			}
		}
	}
}

//void StaffDiagramViewer::ProcessTeamUpdate(JOBDATA *prpPoolJob)
//{
//	if(prpPoolJob != NULL)
//	{
//		TEAM_DATA *prlTeam = GetTeam
//	}
//}


void StaffDiagramViewer::SelectBar(STAFF_BARDATA *prpBar, bool bpSelect /*=true*/)
{
	if(prpBar != NULL)
	{
		prpBar->Selected = bpSelect;
		if(bpSelect)
		{
			omSelectedBars.SetAt((void *) prpBar->JobUrno, NULL);
		}
		else
		{
			omSelectedBars.RemoveKey((void *) prpBar->JobUrno);
		}
	}
}

// check that the fram colour of the bar matches its select status in the map
void StaffDiagramViewer::CheckSelectBar(STAFF_BARDATA *prpBar)
{
	void *pvlDummy;
	if(omSelectedBars.Lookup((void *) prpBar->JobUrno, (void *&) pvlDummy))
	{
		if(!prpBar->Selected)
		{
			SelectBar(prpBar, true);
		}
	}
	else if(prpBar->Selected)
	{
		SelectBar(prpBar, false);
	}
}

void StaffDiagramViewer::DeselectAllBars()
{
	void *pvlDummy;
	bool blUpdateLine = false;
	int ilNumGroups = omGroups.GetSize();
	for(int ilG = 0; ilG < ilNumGroups; ilG++)
	{
		int ilNumLines = omGroups[ilG].Lines.GetSize();
		for(int ilL = 0; ilL < ilNumLines; ilL++)
		{
			blUpdateLine = false;
			int ilNumBars = omGroups[ilG].Lines[ilL].Bars.GetSize();
			for(int ilB = 0; ilB < ilNumBars; ilB++)
			{
				STAFF_BARDATA *prlBar = &omGroups[ilG].Lines[ilL].Bars[ilB];
				if(omSelectedBars.Lookup((void *) prlBar->JobUrno, (void *&) pvlDummy))
				{
					SelectBar(prlBar, false);
					blUpdateLine = true;
				}
			}

			if(blUpdateLine)
			{
				if(MessagingActivated())
				{
					LONG lParam = MAKELONG(ilL, ilG);
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINE, lParam);
				}
			}
		}
	}
	omSelectedBars.RemoveAll();
}

void StaffDiagramViewer::SelectedJobDeleted(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		SelectedJobDeleted(prpJob->Urno);
	}
}

void StaffDiagramViewer::SelectedJobDeleted(long lpJobUrno)
{
	omSelectedBars.RemoveKey((void *) lpJobUrno);
}

int StaffDiagramViewer::GetSelectedJobs(CCSPtrArray <JOBDATA> &ropJobs)
{
	long llJobUrno = 0L;
	void *pvlDummy = NULL;
	JOBDATA *prlJob = NULL;

	for(POSITION rlPos = omSelectedBars.GetStartPosition(); rlPos != NULL; )
	{
		omSelectedBars.GetNextAssoc(rlPos, (void *&)llJobUrno, (void *&)pvlDummy);
		if((prlJob = ogJobData.GetJobByUrno(llJobUrno)) != NULL)
		{
			ropJobs.Add(prlJob);
		}
	}

	return ropJobs.GetSize();
}

bool StaffDiagramViewer::MessagingActivated(void)
{
	return (!bmDisableMessaging && ::IsWindow(pomAttachWnd->GetSafeHwnd()));
}

//Singapore
CString StaffDiagramViewer::GetPool(int ipGroupno) const
{	
	if(ipGroupno < 0 || ipGroupno >= omGroups.GetSize()) return CString("");
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		void* polVoid = NULL;
		if(omMapGroupNoToNull.Lookup(ipGroupno,polVoid) == TRUE)
		{
			if(omGroups[ipGroupno].pomParentGroupData != NULL)
			{
				return omGroups[ipGroupno].pomParentGroupData->Text;
			}
		}
	}
    return omGroups[ipGroupno].Text;
}

//PRF 8704
/*This function is tightly coupled with the Print functionalities in the CCSPrint class for gantt chart.
  It has to be/should be modified in case CCSPrint class is enhanced or modified for any reason.
  
  This function tries to replicate the functionality of the PrintPool(...) function.
  Since for the PrintPreview we need to have the information regarding no of pages, we have to store the relevant
  print page information inside the omMapPageNoToPreviewDataList member variable.
*/
void StaffDiagramViewer::SetupPrintPageInfo(CCSPrint::PRINT_OPTION epPrintOption)
{
	ClearMapPageNoToPreviewDataList();
	CList<PRINT_PREVIEW_PAGEDATA*,PRINT_PREVIEW_PAGEDATA*&>* polPoolListPreviewData = NULL;
	PRINT_PREVIEW_PAGEDATA* polPrintPreviewPageData = NULL;

	int ilGroupCount = GetGroupCount();
	int ilLineNo = 999;
	int ilPageNo = 0;
	int ilYOffset = MMY(500);
	int ilGanttEndY = 0;
	int ilTsHeight;
	int ilStartY;
	int ilEndY;

	CCSPtrArray<PRINTBARDATA> ropPrintLine;
	CCSPtrArray<PRINTBARDATA> ropBkBars;

	BOOL blIsFirstGroupOnPage = TRUE;
	CString olAllManagers;

	BOOL blNewPage = TRUE;

	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		polPrintPreviewPageData = new PRINT_PREVIEW_PAGEDATA();		
		polPrintPreviewPageData->imGroupNo = ilGroupno;
		polPrintPreviewPageData->omTeamName = GetGroupText(ilGroupno);
		polPrintPreviewPageData->imStartLineNo = 0;
		polPrintPreviewPageData->imEndLineNo = 0;

		//Print GanttLine //imGanttEndY = imYOffset;		
   		if( ilLineNo > (pomPrint->imMaxLines - ogBasicData.imFreeBottomLines))
		//if(blNewPage == TRUE)
		{
			blNewPage = FALSE;
			if ( ilPageNo > 0)
			{
				//pomPrint->PrintGanttBottom();
				//ilYOffset = ilGanttEndY + MMY(40); 
				ilYOffset = MMY(500);

				//bmIsFirstGroupOnPage = TRUE;
				blIsFirstGroupOnPage = TRUE;
			}
	
			//PrintManagers(ipGroupNo);-->pomPrint->PrintText(720,0,0,50,PRINT_SMALL,"");
			if (GetAllManagers(ilGroupno,olAllManagers) == FALSE)
			{
				if(blIsFirstGroupOnPage == FALSE)
				{
					//PrintManagers(ipGroupNo);-->pomPrint->PrintText(720,0,0,50,PRINT_SMALL,"");
					ilYOffset += MMY(50);
					blIsFirstGroupOnPage = FALSE;
				}
				blIsFirstGroupOnPage = FALSE;
			}

			polPoolListPreviewData = new CList<PRINT_PREVIEW_PAGEDATA*,PRINT_PREVIEW_PAGEDATA*&>;
			polPoolListPreviewData->AddTail(polPrintPreviewPageData);

			//PrintStaffDiagramHeader(ipGroupNo); 
			ilPageNo++;
			ilLineNo = 0;

			omMapPageNoToPreviewDataList.SetAt(ilPageNo,polPoolListPreviewData);
			polPrintPreviewPageData->imPageNo = ilPageNo;
			polPrintPreviewPageData->omTeamName = GetGroupText(ilGroupno);

			//pomPrint->PrintTimeScale(700,2800,omStartTime,omEndTime,olPoolName);
			int ilHours = ((CTimeSpan)(omEndTime-omStartTime)).GetTotalHours();
			if (ilHours < 9)
			{
				ilTsHeight = MMY(100);
				ilStartY = ilYOffset;
				ilEndY   = ilStartY+ilTsHeight; 
			}
			else
			{
				ilTsHeight = MMY(83);
				ilStartY = ilYOffset;
				ilEndY   = ilStartY+ilTsHeight; 				
			}

			ilGanttEndY = ilEndY;
			ilYOffset = ilEndY  + MMY(10);

		}
		else
		{
			if(!(ilYOffset < (pomPrint->pomCdc->GetDeviceCaps(VERTRES) - MMY(200))))
			{
				if (ilPageNo  > 0)//pomPrint->imPageNo
				{
					//pomPrint->PrintGanttBottom();
					ilYOffset = ilGanttEndY + MMY(40);
				}
							
				//bmIsFirstGroupOnPage = TRUE;
				blIsFirstGroupOnPage = TRUE;

				//PrintStaffDiagramHeader(ipGroupNo);
				ilPageNo++;
				ilLineNo = 0;

				polPoolListPreviewData = new CList<PRINT_PREVIEW_PAGEDATA*,PRINT_PREVIEW_PAGEDATA*&>;
				polPrintPreviewPageData->imGroupNo = ilGroupno;
				polPrintPreviewPageData->omTeamName = GetGroupText(ilGroupno);
				polPoolListPreviewData->AddTail(polPrintPreviewPageData);
				omMapPageNoToPreviewDataList.SetAt(ilPageNo,polPoolListPreviewData);

				ilYOffset = MMY(500);
				//pomPrint->PrintTimeScale(700,2800,omStartTime,omEndTime,olPoolName);
				int ilHours = ((CTimeSpan)(omEndTime-omStartTime)).GetTotalHours();
				if (ilHours < 9)
				{
					ilTsHeight = MMY(100);;
					ilStartY = ilYOffset;
					ilEndY   = ilStartY+ilTsHeight; 
				}
				else
				{
					ilTsHeight = MMY(83);
					ilStartY = ilYOffset;
					ilEndY   = ilStartY+ilTsHeight; 										
				}
				ilGanttEndY = ilEndY;
				ilYOffset = ilEndY  + MMY(10);

				//bmIsFirstGroupOnPage = TRUE;
				blIsFirstGroupOnPage = TRUE;
			}				
			else
			{
				polPrintPreviewPageData->imPageNo = ilPageNo;
				polPoolListPreviewData->AddTail(polPrintPreviewPageData);

				if (GetAllManagers(ilGroupno,olAllManagers) == FALSE)
				{
					if(blIsFirstGroupOnPage == FALSE)
					{
						//PrintManagers(ipGroupNo);--> pomPrint->PrintText(720,0,0,50,PRINT_SMALL,"");
						ilYOffset += MMY(50);
						blIsFirstGroupOnPage = FALSE;
					}
					blIsFirstGroupOnPage = FALSE;
				}
				
				//pomPrint->PrintGanttHeader(700,2800,olPoolName); 
				ilTsHeight = MMY(83); //int
				ilStartY = ilYOffset; //int 
				ilEndY   = ilStartY+ilTsHeight; //int
				ilYOffset = ilEndY  + MMY(10);
				ilGanttEndY = ilEndY;
			}
		}

		
		int ilLineCount = GetLineCount(ilGroupno);
		for(int ilLine = 0; ilLine < (ilLineCount); ilLine++)
		{
			ilLineNo++;
			PrintPrepareLineData(ilGroupno,ilLine,ropPrintLine,ropBkBars);
			int ilMaxOverlapLevel = GetMaxOverlapLevel(ropPrintLine,ropBkBars);
			int ilHeightOfThisLine = ilMaxOverlapLevel * pomPrint->imGanttLineHeight;

			int test = pomPrint->pomCdc->GetDeviceCaps(VERTRES);
			int testt = pomPrint->pomCdc->GetDeviceCaps(HORZRES);
			int testtt = pomPrint->pomCdc->GetDeviceCaps(VERTSIZE) * 10;
			int ilOffset = MMY(100);
			if(epPrintOption == CCSPrint::PRINT_PRINTER)
			{
				ilOffset = MMY(100);
			}

			if(!((ilYOffset + ilHeightOfThisLine) < (pomPrint->pomCdc->GetDeviceCaps(VERTRES) - ilOffset)))
			{
				polPrintPreviewPageData->imEndLineNo = ilLine - 1;

				if (ilPageNo  > 0)//pomPrint->imPageNo
				{
					//pomPrint->PrintGanttBottom();
					ilYOffset = ilGanttEndY + MMY(40);
				}
							
				//bmIsFirstGroupOnPage = TRUE;
				blIsFirstGroupOnPage = TRUE;

				//PrintStaffDiagramHeader(ipGroupNo);
				ilPageNo++;
				ilLineNo = 0;

				polPoolListPreviewData = new CList<PRINT_PREVIEW_PAGEDATA*,PRINT_PREVIEW_PAGEDATA*&>;
				polPrintPreviewPageData = new PRINT_PREVIEW_PAGEDATA();
				polPrintPreviewPageData->imGroupNo = ilGroupno;
				polPrintPreviewPageData->imPageNo = ilPageNo;
				polPrintPreviewPageData->omTeamName = GetGroupText(ilGroupno);

				polPoolListPreviewData->AddTail(polPrintPreviewPageData);
				polPrintPreviewPageData->imStartLineNo = ilLine;
				polPrintPreviewPageData->imEndLineNo = ilLine;
				omMapPageNoToPreviewDataList.SetAt(ilPageNo,polPoolListPreviewData);

				ilYOffset = MMY(500);
				//pomPrint->PrintTimeScale(700,2800,omStartTime,omEndTime,olPoolName);
				int ilHours = ((CTimeSpan)(omEndTime-omStartTime)).GetTotalHours();
				if (ilHours < 9)
				{
					ilTsHeight = MMY(100);;
					ilStartY = ilYOffset;
					ilEndY   = ilStartY+ilTsHeight; 
				}
				else
				{
					ilTsHeight = MMY(83);
					ilStartY = ilYOffset;
					ilEndY   = ilStartY+ilTsHeight; 										
				}
				ilGanttEndY = ilEndY;
				ilYOffset = ilEndY  + MMY(10);

				//bmIsFirstGroupOnPage = TRUE;
				blIsFirstGroupOnPage = TRUE;
				
				//PrintManagers(ipGroupNo);
				if (GetAllManagers(ilGroupno,olAllManagers) == FALSE)
				{
					if(blIsFirstGroupOnPage == FALSE)
					{
						//PrintManagers(ipGroupNo); //pomPrint->PrintText(720,0,0,50,PRINT_SMALL,"");
						ilYOffset += MMY(50);

						blIsFirstGroupOnPage = FALSE;
					}
					blIsFirstGroupOnPage = FALSE;
				}
				
				if (ilLine < ilLineCount)
				{
					//pomPrint->PrintGanttLine(GetLineText(ipGroupNo, ilLineno),ropPrintLine,ropBkBars);
					int ilMaxOverlapLevel = GetMaxOverlapLevel(ropPrintLine,ropBkBars);
					int ilHeightOfThisLine = ilMaxOverlapLevel * pomPrint->imGanttLineHeight;
					if((ilYOffset + ilHeightOfThisLine) < (pomPrint->pomCdc->GetDeviceCaps(VERTRES)-ilOffset))
					{
						int ilNewYOffset = ilYOffset + ((ilMaxOverlapLevel) * pomPrint->imGanttLineHeight);
						ilYOffset = ilNewYOffset + MMY(10);
						ilGanttEndY = ilYOffset;
					}					
				}
			}
			else
			{
				//pomPrint->PrintGanttLine(GetLineText(ipGroupNo, ilLineno),ropPrintLine,ropBkBars);
				polPrintPreviewPageData->imEndLineNo = ilLine;
				int ilNewYOffset = ilYOffset + ((ilMaxOverlapLevel) * pomPrint->imGanttLineHeight);
				ilYOffset = ilNewYOffset + MMY(10);
				ilGanttEndY = ilYOffset;
			}
		}
		//pomPrint->PrintGanttBottom();
		ilYOffset = ilGanttEndY + MMY(40);
	}
}

//PRF 8704
BOOL StaffDiagramViewer::GetAllManagers(const int ipGroupno,CString& ropAllManagers)
{
	ropAllManagers = "";
	POSITION rlPos;
	for ( rlPos = ogJobData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		JOBDATA *prlJob;
		ogJobData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlJob);
		if (strcmp(prlJob->Jtco, JOBPOOL) != 0)	// uninterest non-duty bars
			continue;

		CString olFunction = ogBasicData.GetFunctionForPoolJob(prlJob);

		CStringArray olPermits;
		ogBasicData.GetPermitsForPoolJob(prlJob,olPermits);
		CString olGroupName = ogBasicData.GetTeamForPoolJob(prlJob);

		// Check if this duty is in the filter and has some part visible in the display window
		SHIFTDATA *prlShift;
		EMPDATA *prlEmp;
		if ((prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur)) == NULL ||
			(prlEmp = ogEmpData.GetEmpByPeno(prlShift->Peno)) == NULL ||
			!IsPassPoolJobFilter(prlJob, prlShift, prlEmp, olGroupName, olFunction, olPermits))
			continue;

		int ilGroupno;
		if ((ilGroupno = FindGroup(prlJob->Alid, olGroupName, prlShift->Sfca)) != ipGroupno)
			continue;	// manager is not in this group

		BOOL blIsFlightManager = ogBasicData.IsManager(olFunction);
		if (blIsFlightManager)
		{
			if (ropAllManagers.IsEmpty() == FALSE)
			{
				ropAllManagers += "   /   ";
			}
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur);
			ropAllManagers += CString((prlShift != NULL ? prlShift->Sfca : "")) + ", ";
			ropAllManagers += olGroupName + ", ";
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);
			ropAllManagers += CString(prlEmp != NULL ? prlEmp->Lanm : "");
		}
	}
	return ropAllManagers.IsEmpty();
}

//PRF 8704
static int ComparePrintBar(const PRINTBARDATA **e1, const PRINTBARDATA **e2)
{
	return (int)((**e1).StartTime.GetTime() - (**e2).StartTime.GetTime());
}

//PRF 8704
int StaffDiagramViewer::GetMaxOverlapLevel(CCSPtrArray<PRINTBARDATA>& ropPrintLine,
							               CCSPtrArray<PRINTBARDATA>& ropBkBars) const
{
	int i,j,k;
	BOOL blFound;
	BOOL blDontOverlap;

	// first calculate max overlaplevel and line height
	ropPrintLine.Sort(ComparePrintBar); // sort for time
	// calculate overlaplevel
	int ilBarCount = ropPrintLine.GetSize();
	int ilMaxOverlapLevel = 0;
	// start the loop with 1, the first bar can't be overlapped
	for ( i = 1; i < ilBarCount; i++)
	{
		if (ropPrintLine[i-1].EndTime > ropPrintLine[i].StartTime )
		{
			// found an overlapping bar, check if we can it display at lower overlap level
			
			blDontOverlap = FALSE;
			for (j = 0; (j <= ilMaxOverlapLevel) && (blDontOverlap == FALSE); j++)
			{
				blFound = FALSE;
				for( k = 0; (k < ilBarCount) && (blFound == FALSE) && (k < i); k++)
				{
					if (ropPrintLine[k].OverlapLevel == j)
					{
						if (ropPrintLine[k].EndTime >= ropPrintLine[i].StartTime)
						{
							blFound = TRUE;
						}
					}
				}
				if (blFound == FALSE)
				{
					// no overlapping at this line, so we can use it
					ropPrintLine[i].OverlapLevel = j;
					blDontOverlap = TRUE;
				}
			}
			if (blDontOverlap == FALSE)
			{
				// we have not found a free line, extend overlaplevel
				ilMaxOverlapLevel++;
				ropPrintLine[i].OverlapLevel = ilMaxOverlapLevel;
			}
		}
	}

	ilMaxOverlapLevel++; 
	return ilMaxOverlapLevel;
}

//PRF 8704
void StaffDiagramViewer::ClearMapPageNoToPreviewDataList()
{
	POSITION olMapPos = omMapPageNoToPreviewDataList.GetStartPosition();
	CList<PRINT_PREVIEW_PAGEDATA*,PRINT_PREVIEW_PAGEDATA*&>* polPoolListPreviewData = NULL;
	WORD dWord;
	while(olMapPos != NULL)
	{
		omMapPageNoToPreviewDataList.GetNextAssoc(olMapPos,dWord,(void*&)polPoolListPreviewData);
		if(polPoolListPreviewData  != NULL)
		{
			PRINT_PREVIEW_PAGEDATA* polPrintPreviewPageData;
			POSITION olListPos = polPoolListPreviewData->GetHeadPosition();
			while(olListPos != NULL)
			{
				polPrintPreviewPageData = polPoolListPreviewData->GetNext(olListPos);
				if(polPrintPreviewPageData != NULL)
				{
					delete polPrintPreviewPageData;				
				}
			}			
			polPoolListPreviewData->RemoveAll();
			delete polPoolListPreviewData;
		}
	}
	omMapPageNoToPreviewDataList.RemoveAll();
}

//PRF 8704
void StaffDiagramViewer::PrintPageData(CCSPrint* pomPrint, const int& ripPageNo)
{
	CList<PRINT_PREVIEW_PAGEDATA*,PRINT_PREVIEW_PAGEDATA*&>* polPoolListPreviewData = NULL;
	PRINT_PREVIEW_PAGEDATA* polPrintPreviewPageData = NULL;
	BOOL blPrintStaffDiagramHeader = FALSE;
	
	CCSPtrArray<PRINTBARDATA> olPrintLine;
	CCSPtrArray<PRINTBARDATA> olBkBars;

	if(omMapPageNoToPreviewDataList.Lookup(ripPageNo,(void*&)polPoolListPreviewData) != 0
		&& polPoolListPreviewData != NULL && polPoolListPreviewData->GetCount() > 0)
	{
		POSITION olListPos = polPoolListPreviewData->GetHeadPosition();
		while(olListPos != NULL)
		{
			int ilGroupNo = -1;
			polPrintPreviewPageData = polPoolListPreviewData->GetNext(olListPos);
			if(polPrintPreviewPageData == NULL || polPrintPreviewPageData->imGroupNo == -1
				|| polPrintPreviewPageData->imStartLineNo > polPrintPreviewPageData->imEndLineNo)
			{
				break;
			}
			ilGroupNo = polPrintPreviewPageData->imGroupNo;
			if(blPrintStaffDiagramHeader == FALSE)
			{
				PrintManagers(ilGroupNo);
				PrintStaffDiagramHeader(ilGroupNo);
				bmIsFirstGroupOnPage = TRUE;
				blPrintStaffDiagramHeader = TRUE;
			}
			else
			{
				PrintManagers(ilGroupNo);
				CString olPoolName = GetGroupText(ilGroupNo);
				pomPrint->PrintGanttHeader(700,2800,olPoolName);
			}			
			pomPrint->imPageNo = ripPageNo;	
			if(GetLineCount(ilGroupNo) == 0)
			{
				continue;
			}
			for(int i = polPrintPreviewPageData->imStartLineNo ; i <= polPrintPreviewPageData->imEndLineNo; i++)
			{
				PrintPrepareLineData(ilGroupNo,i,olPrintLine,olBkBars);
				CString olLineText;
				olLineText = GetLineText(ilGroupNo, i);
				pomPrint->PrintGanttLine(olLineText,olPrintLine,olBkBars);
			}
			pomPrint->PrintGanttBottom();
		}
		if(blPrintStaffDiagramHeader == TRUE)
		{
			pomPrint->PrintFooter("","");
			pomPrint->pomCdc->EndPage();
		}
	}
	pomPrint->imLineTop = pomPrint->imFirstLine;
	pomPrint->imLineNo = 0;
	bmIsFirstGroupOnPage = TRUE;
}

void StaffDiagramViewer::ExportToExcel(BOOL bpIsPageBreakEnabled, BOOL bpIsColorEnabled)
{
	CoInitialize(NULL);
	CString strFilename = "OPSS_STAFFCHART_";
	strFilename += CTime::GetCurrentTime().Format("%Y%m%d-%H%M%S");
	strFilename += ".xls";

	VARIANT varFileName;
	varFileName.vt = VT_BSTR;
	varFileName.bstrVal = strFilename.AllocSysString();

	omXlPgDmns.bmReset = TRUE;
	VARIANT varSave;
      varSave.vt = VT_BOOL;
      varSave.boolVal = VARIANT_FALSE;

	VARIANT varEmpty;
      varEmpty.vt = VT_ERROR;
      varEmpty.scode = DISP_E_PARAMNOTFOUND;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	_Application appExcel = NULL;
	appExcel.CreateDispatch("Excel.Application");

    // Open the workbook in the strFilename
	Workbooks workBooks = appExcel.GetWorkbooks();	
	_Workbook workBook  = workBooks.Add(varEmpty);
	
	Worksheets workSheets = workBook.GetWorksheets();
	_Worksheet workSheet = workSheets.GetItem(COleVariant((short)1));
	Shapes shapes= workSheet.GetShapes();	
	
	int ilStartRow = 10;
	VARIANT varDimension;
	VariantInit(&varDimension);
	varDimension.vt = VT_R8;	

	Range rangeCell = workSheet.GetRange(COleVariant("A01"),COleVariant("A01"));	
	varDimension = rangeCell.GetWidth();	
	omXlPgDmns.fmCellWidth = (float)varDimension.dblVal;	
	varDimension = rangeCell.GetHeight();
	omXlPgDmns.fmCellHeight = (float)varDimension.dblVal;
	rangeCell.ReleaseDispatch();

	omXlPgDmns.fmPoolPrintStartHeight = ilStartRow * omXlPgDmns.fmCellHeight;
	omXlPgDmns.fmPrintPageWidth = omXlPgDmns.fmCellWidth * 21 - 1;   //This dimension is meant for paper size A3-LandScape
	omXlPgDmns.fmPrintPageHeight = 56 * omXlPgDmns.fmCellHeight;     //This dimension is meant for paper size A3-LandScape.
	omXlPgDmns.fmEmployeePoolPrintWidth = omXlPgDmns.fmCellWidth * 3;
	omXlPgDmns.fmPoolJobPrintWidth = omXlPgDmns.fmPrintPageWidth - omXlPgDmns.fmEmployeePoolPrintWidth;
	omXlPgDmns.fmGanttLineHeight = 2 * omXlPgDmns.fmCellHeight;
	omXlPgDmns.fmBarHeight = omXlPgDmns.fmCellHeight;

      ExcelExportPrintHeader(shapes,0,0,bpIsColorEnabled);
	
	/*************************Export Employee names**********************************************/
	FillFormat fillFormat = NULL;
	ColorFormat newColorFormat = NULL;
	int ilGroupCount = GetGroupCount();
	float flNextStartOffset = omXlPgDmns.fmPoolPrintStartHeight - omXlPgDmns.fmTimeLongLineHeight;		
	float flNextStartOffsetCheck = flNextStartOffset;
	for (int ilGroupNo = 0; ilGroupNo < ilGroupCount; ilGroupNo++)
	{	
		if(bpIsPageBreakEnabled == TRUE && (flNextStartOffsetCheck + omXlPgDmns.fmGanttLineHeight) > omXlPgDmns.fmPrintPageHeight)// && GetLineCount(ilGroupNo) > 0)
		{
			flNextStartOffsetCheck = ilStartRow * omXlPgDmns.fmCellHeight - omXlPgDmns.fmTimeLongLineHeight;
			omXlPgDmns.fmPoolPrintStartHeight = ilStartRow * omXlPgDmns.fmCellHeight + omXlPgDmns.fmPrintPageHeight * (int)((flNextStartOffset + omXlPgDmns.fmGanttLineHeight)/omXlPgDmns.fmPrintPageHeight);
			ExcelExportPrintHeader(shapes,(int)((flNextStartOffset + omXlPgDmns.fmGanttLineHeight)/omXlPgDmns.fmPrintPageHeight),ilGroupNo,bpIsColorEnabled);
			flNextStartOffset = omXlPgDmns.fmPrintPageHeight * (int)((flNextStartOffset + omXlPgDmns.fmGanttLineHeight)/omXlPgDmns.fmPrintPageHeight) + (ilStartRow * omXlPgDmns.fmCellHeight - omXlPgDmns.fmTimeLongLineHeight);
			
			/*if(GetLineCount(ilGroupNo) == 0)
			{
				ExcelExportPrintPoolHeader(shapes,flNextStartOffset,ilGroupNo,bpIsColorEnabled);
				flNextStartOffset += omXlPgDmns.fmGanttLineHeight;
				flNextStartOffsetCheck += omXlPgDmns.fmGanttLineHeight;
				continue;
			}*/
		}
		
		if(GetLineCount(ilGroupNo) == 0)
		{
			ExcelExportPrintPoolHeader(shapes,flNextStartOffset,ilGroupNo,bpIsColorEnabled);
			flNextStartOffset += omXlPgDmns.fmGanttLineHeight;
			flNextStartOffsetCheck += omXlPgDmns.fmGanttLineHeight;
		}

		/*if(bpIsPageBreakEnabled == TRUE && flNextStartOffsetCheck > omXlPgDmns.fmPrintPageHeight)
		{			
			flNextStartOffsetCheck = ilStartRow * omXlPgDmns.fmCellHeight - omXlPgDmns.fmTimeLongLineHeight;
			omXlPgDmns.fmPoolPrintStartHeight = ilStartRow * omXlPgDmns.fmCellHeight + omXlPgDmns.fmPrintPageHeight * (int) (flNextStartOffset/omXlPgDmns.fmPrintPageHeight);
			ExcelExportPrintHeader(shapes,(int)(flNextStartOffset/omXlPgDmns.fmPrintPageHeight),ilGroupNo,bpIsColorEnabled);
			flNextStartOffset = omXlPgDmns.fmPrintPageHeight * (int)(flNextStartOffset/omXlPgDmns.fmPrintPageHeight) + (ilStartRow * omXlPgDmns.fmCellHeight - omXlPgDmns.fmTimeLongLineHeight);			
		}*/

		float flPoolJobsStartHeight = flNextStartOffset;

		CCSPtrArray<PRINTBARDATA> ropPrintLine;
		CCSPtrArray<PRINTBARDATA> ropBkBars;
		int ilLineCount = GetLineCount(ilGroupNo);

		for( int ilLineno = 0; ilLineno < (ilLineCount); ilLineno++)
		{			
			ropPrintLine.DeleteAll();
			ropBkBars.DeleteAll();
			PrintPrepareLineData(ilGroupNo,ilLineno,ropPrintLine,ropBkBars);
			CString olLineText;
			if (ilLineno < ilLineCount)
			{
				olLineText = GetLineText(ilGroupNo, ilLineno);
				olLineText.TrimLeft();
				olLineText.TrimRight();
			}
			else
			{
				olLineText.Empty();
			}
	
			/********************************For Finding the overlap level********************************/
			BOOL blFound;
			BOOL blDontOverlap;
			// first calculate max overlaplevel and line height
			ropPrintLine.Sort(ComparePrintBar); // sort for time
			// calculate overlaplevel
			int ilBarCount = ropPrintLine.GetSize();
			int ilMaxOverlapLevel = 0;
			// start the loop with 1, the first bar can't be overlapped
			for (int i = 1; i < ilBarCount; i++)
			{
				if (ropPrintLine[i-1].EndTime > ropPrintLine[i].StartTime )
				{
					// found an overlapping bar, check if we can it display at lower overlap level
					
					blDontOverlap = FALSE;
					for (int j = 0; (j <= ilMaxOverlapLevel) && (blDontOverlap == FALSE); j++)
					{
						blFound = FALSE;
						for(int k = 0; (k < ilBarCount) && (blFound == FALSE) && (k < i); k++)
						{
							if (ropPrintLine[k].OverlapLevel == j)
							{
								if (ropPrintLine[k].EndTime >= ropPrintLine[i].StartTime)
								{
									blFound = TRUE;
								}
							}
						}
						if (blFound == FALSE)
						{
							// no overlapping at this line, so we can use it
							ropPrintLine[i].OverlapLevel = j;
							blDontOverlap = TRUE;
						}
					}
					if (blDontOverlap == FALSE)
					{
						// we have not found a free line, extend overlaplevel
						ilMaxOverlapLevel++;
						ropPrintLine[i].OverlapLevel = ilMaxOverlapLevel;
					}
				}
			}
			ilMaxOverlapLevel++; 
			/****************************************************************************************/

			float flBkBarLineHeight = ilMaxOverlapLevel * omXlPgDmns.fmGanttLineHeight;
			if(bpIsPageBreakEnabled == TRUE && (flNextStartOffsetCheck +flBkBarLineHeight) > omXlPgDmns.fmPrintPageHeight)
			{
				if(flNextStartOffset > flPoolJobsStartHeight)
				{
					for(int j = 0; j < omXlPgDmns.imTimeScaleBreakUpSteps / 3; j++ )
					{			
						Shape shapeLine = shapes.AddLine(omXlPgDmns.fmEmployeePoolPrintWidth + j * 30 * omXlPgDmns.fmTimeOneMinStepWidth,flPoolJobsStartHeight,omXlPgDmns.fmEmployeePoolPrintWidth + j * 30 * omXlPgDmns.fmTimeOneMinStepWidth,flNextStartOffset);
						LineFormat lineFormat = shapeLine.GetLine();
						lineFormat.SetDashStyle(4);
						lineFormat.SetVisible(-1);
						lineFormat.ReleaseDispatch();
						shapeLine.ReleaseDispatch();
					}
				}

				flNextStartOffsetCheck = ilStartRow * omXlPgDmns.fmCellHeight - omXlPgDmns.fmTimeLongLineHeight;
				omXlPgDmns.fmPoolPrintStartHeight = ilStartRow * omXlPgDmns.fmCellHeight + omXlPgDmns.fmPrintPageHeight * (int)((flNextStartOffset + flBkBarLineHeight)/omXlPgDmns.fmPrintPageHeight);
				ExcelExportPrintHeader(shapes,(int)((flNextStartOffset + flBkBarLineHeight)/omXlPgDmns.fmPrintPageHeight),ilGroupNo,bpIsColorEnabled);
    			      flNextStartOffset = omXlPgDmns.fmPrintPageHeight * (int)((flNextStartOffset+flBkBarLineHeight)/omXlPgDmns.fmPrintPageHeight) + (ilStartRow * omXlPgDmns.fmCellHeight - omXlPgDmns.fmTimeLongLineHeight);
				
				ExcelExportPrintPoolHeader(shapes,flNextStartOffset,ilGroupNo,bpIsColorEnabled);
				flNextStartOffset += omXlPgDmns.fmGanttLineHeight;
				flNextStartOffsetCheck += omXlPgDmns.fmGanttLineHeight;
				flPoolJobsStartHeight = flNextStartOffset;
			}
			else if(ilLineno == 0)
			{
				ExcelExportPrintPoolHeader(shapes,flNextStartOffset,ilGroupNo,bpIsColorEnabled);
				flNextStartOffset += omXlPgDmns.fmGanttLineHeight;
				flNextStartOffsetCheck += omXlPgDmns.fmGanttLineHeight;
				flPoolJobsStartHeight = flNextStartOffset;
			}

			Shape shapeTextBox = shapes.AddTextbox(1,0,flNextStartOffset,omXlPgDmns.fmEmployeePoolPrintWidth,flBkBarLineHeight);
			shapeTextBox.SetAutoShapeType(1); //MsoAutoShapeType
			TextFrame textFrame = shapeTextBox.GetTextFrame();	
			Characters character = textFrame.Characters(varEmpty,varEmpty);
			character.SetText(olLineText);
			character.ReleaseDispatch();
			textFrame.ReleaseDispatch();			

                  if(bpIsColorEnabled)
			{
				fillFormat = shapeTextBox.GetFill();
				newColorFormat = fillFormat.GetForeColor();			
				newColorFormat.SetRgb(RGB(190,205,225));					
				newColorFormat.ReleaseDispatch();
				fillFormat.ReleaseDispatch();
			}
			shapeTextBox.ReleaseDispatch();

			Shape shapeRect = shapes.AddShape(1,omXlPgDmns.fmEmployeePoolPrintWidth, flNextStartOffset,omXlPgDmns.fmPoolJobPrintWidth,flBkBarLineHeight);
                  if(bpIsColorEnabled)
			{
				fillFormat = shapeRect.GetFill();
				newColorFormat = fillFormat.GetForeColor();			
				newColorFormat.SetRgb(RGB(0,100,100));					
				newColorFormat.ReleaseDispatch();
				fillFormat.ReleaseDispatch();
			}
			shapeRect.ReleaseDispatch();
			
			int ilBkBarSize = ropBkBars.GetSize();
			float flBkBarLeft = 0;
			float flBkBarRight = omXlPgDmns.fmPrintPageWidth;
			
			CTime bkBarMinStartTime = TIMENULL;
			CTime bkBarMaxEndTime = TIMENULL;

			for ( i = 0; i < ilBkBarSize; i++)
			{				
				PRINTBARDATA ropBkBar = ropBkBars[i];
				if(ropBkBar.EndTime < omXlPgDmns.omActualStartTime)
				{
					continue;
				}
				if(bkBarMinStartTime == TIMENULL)
				{
					bkBarMinStartTime = ropBkBar.StartTime;
				}
				if(bkBarMaxEndTime == TIMENULL)
				{					
					bkBarMaxEndTime = ropBkBar.EndTime;
				}
				if(ropBkBar.StartTime < bkBarMinStartTime)
				{
					bkBarMinStartTime = ropBkBar.StartTime;
				}
				if(ropBkBar.EndTime > bkBarMaxEndTime)
				{
					bkBarMaxEndTime = ropBkBar.EndTime;
				}
			}
			
			if(bkBarMinStartTime <= omXlPgDmns.omActualStartTime)
			{
				flBkBarLeft = omXlPgDmns.fmEmployeePoolPrintWidth;
			}
			else
			{
				CTimeSpan olDiff = bkBarMinStartTime - omXlPgDmns.omActualStartTime;
				LONG lOffsetMin = olDiff.GetTotalMinutes();
				flBkBarLeft = lOffsetMin * omXlPgDmns.fmTimeOneMinStepWidth + omXlPgDmns.fmEmployeePoolPrintWidth;
			}
			if(bkBarMaxEndTime >= omXlPgDmns.omActualEndTime)
			{
				flBkBarRight = omXlPgDmns.fmPrintPageWidth;
			}
			else
			{				
				CTimeSpan olDiff = bkBarMaxEndTime - omXlPgDmns.omActualStartTime;
				LONG lOffsetMin = olDiff.GetTotalMinutes();
				flBkBarRight = lOffsetMin * omXlPgDmns.fmTimeOneMinStepWidth + omXlPgDmns.fmEmployeePoolPrintWidth;
			}
			
			if(flBkBarLeft < flBkBarRight)
			{
				Shape shape = shapes.AddShape(1,flBkBarLeft,flNextStartOffset,flBkBarRight - flBkBarLeft, flBkBarLineHeight);
				fillFormat = shape.GetFill();
				newColorFormat = fillFormat.GetForeColor();
				if(bpIsColorEnabled)
				{
					newColorFormat.SetRgb(RGB(100,100,225));
				}
				else
				{
					newColorFormat.SetRgb(RGB(225,225,225));
				}	
				newColorFormat.ReleaseDispatch();
				fillFormat.ReleaseDispatch();
				shape.ReleaseDispatch();
			}
			
			float flJobBarLeft = 0;
			float flJobBarRight = 0;

			for ( i = 0; i < ilBarCount; i++)
			{
				PRINTBARDATA ropJobBar = ropPrintLine[i];
				if(ropJobBar.StartTime >= omXlPgDmns.omActualEndTime)
				{
					continue;
				}
				if(ropJobBar.StartTime <= omXlPgDmns.omActualStartTime)
				{
					flJobBarLeft = omXlPgDmns.fmEmployeePoolPrintWidth;
				}
				else
				{
					CTimeSpan olDiff = ropJobBar.StartTime - omXlPgDmns.omActualStartTime;
					LONG lOffsetMin = olDiff.GetTotalMinutes();
					flJobBarLeft = lOffsetMin * omXlPgDmns.fmTimeOneMinStepWidth + omXlPgDmns.fmEmployeePoolPrintWidth;
				}
				if(ropJobBar.EndTime >= omXlPgDmns.omActualEndTime)
				{
					flJobBarRight = omXlPgDmns.fmPrintPageWidth;
				}
				else
				{	
					CTimeSpan olDiff = ropJobBar.EndTime - omXlPgDmns.omActualStartTime;
					LONG lOffsetMin = olDiff.GetTotalMinutes();
					flJobBarRight = lOffsetMin * omXlPgDmns.fmTimeOneMinStepWidth + omXlPgDmns.fmEmployeePoolPrintWidth;					
				}

				if(!(ropJobBar.Text.IsEmpty()))
				{
					Shape shapeTextBox = shapes.AddTextbox(1,flJobBarLeft,flNextStartOffset + (omXlPgDmns.fmGanttLineHeight - omXlPgDmns.fmBarHeight)/2  + ropJobBar.OverlapLevel * omXlPgDmns.fmGanttLineHeight,flJobBarRight - flJobBarLeft,omXlPgDmns.fmBarHeight);
					//shapeTextBox.SetAutoShapeType(5); //MsoAutoShapeType
					TextFrame textFrame = shapeTextBox.GetTextFrame();
					Characters character = textFrame.Characters(varEmpty,varEmpty);
					Font font = character.GetFont();
					textFrame.SetHorizontalAlignment(2);  //MsoTextEffectAlignment
					font.SetSize(COleVariant((short)6));					
					character.SetText(ropJobBar.Text);
					font.ReleaseDispatch();
					character.ReleaseDispatch();
					textFrame.ReleaseDispatch();
                
					fillFormat = shapeTextBox.GetFill();
					newColorFormat = fillFormat.GetForeColor();
					if(bpIsColorEnabled)
					{
						newColorFormat.SetRgb(RGB(100,200,255));
					}
					else
					{
						newColorFormat.SetRgb(RGB(100,115,125));
					}
					newColorFormat.ReleaseDispatch();
					fillFormat.ReleaseDispatch();
					shapeTextBox.ReleaseDispatch();
				}
				else
				{
					Shape shape = shapes.AddShape(1,flJobBarLeft,flNextStartOffset + (omXlPgDmns.fmGanttLineHeight - omXlPgDmns.fmBarHeight)/2 + ropJobBar.OverlapLevel * omXlPgDmns.fmGanttLineHeight,flJobBarRight - flJobBarLeft, omXlPgDmns.fmBarHeight);
					fillFormat = shape.GetFill();
					ColorFormat newColorFormat = fillFormat.GetForeColor();
					if(bpIsColorEnabled)
					{
						newColorFormat.SetRgb(RGB(100,150,255));
					}
					else
					{
						newColorFormat.SetRgb(RGB(200,200,200));
					}
					newColorFormat.ReleaseDispatch();
					fillFormat.ReleaseDispatch();
					shape.ReleaseDispatch();
				}			

				if(i < (ilMaxOverlapLevel - 1))
				{
					Shape shapeLine = shapes.AddLine(omXlPgDmns.fmEmployeePoolPrintWidth,flNextStartOffset + (i+1)* omXlPgDmns.fmGanttLineHeight,omXlPgDmns.fmPrintPageWidth,flNextStartOffset + (i+1)* omXlPgDmns.fmGanttLineHeight);
					LineFormat lineFormat = shapeLine.GetLine();
					lineFormat.SetDashStyle(4);
					lineFormat.SetVisible(-1);
					lineFormat.ReleaseDispatch();
					shapeLine.ReleaseDispatch();
				}

			}
			
			flNextStartOffset += flBkBarLineHeight;
			flNextStartOffsetCheck += flBkBarLineHeight;
		}
		if(flNextStartOffset > flPoolJobsStartHeight)
		{
			for(int j = 0; j < omXlPgDmns.imTimeScaleBreakUpSteps / 3; j++ )
			{			
				Shape shapeLine = shapes.AddLine(omXlPgDmns.fmEmployeePoolPrintWidth + j * 30 * omXlPgDmns.fmTimeOneMinStepWidth,flPoolJobsStartHeight,omXlPgDmns.fmEmployeePoolPrintWidth + j * 30 * omXlPgDmns.fmTimeOneMinStepWidth,flNextStartOffset);
				LineFormat lineFormat = shapeLine.GetLine();
				lineFormat.SetDashStyle(4);
				lineFormat.SetVisible(-1);
				lineFormat.ReleaseDispatch();
				shapeLine.ReleaseDispatch();
			}
		}
	}
	/***************************************************************************************/
	
	try
	{
		/*************This piece of code within this bracket should be kept here only, otherwise performance is pathetic!*/
		if(bpIsPageBreakEnabled == FALSE)
		{
			Range range = workSheet.GetRange(COleVariant("A09"),COleVariant("U09"));
			range.Activate();
			Window window;
			window.m_lpDispatch = appExcel.GetActiveWindow();
			window.SetFreezePanes(TRUE);
			window.ReleaseDispatch();
			range.ReleaseDispatch();
		}
		shapes.ReleaseDispatch();
		/************************************************************************************/
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		CFileDialog* polFileDialog = new CFileDialog(FALSE,NULL,strFilename,OFN_HIDEREADONLY,"Microsoft Office Excel Workbook(*.xls)|*.xls|");
		CString olFileName;
		if(polFileDialog->DoModal() == IDOK)
		{
			olFileName = polFileDialog->GetPathName();
			varFileName.bstrVal = olFileName.AllocSysString();			
			workBook.SaveAs(varFileName,varEmpty,varEmpty,varEmpty,varEmpty,varEmpty,3,varEmpty,varEmpty,varEmpty,varEmpty);
			//AfxMessageBox("Excel export completed");
		}
		else
		{
			workBook.Close(varSave,varEmpty,varEmpty);
		}
	}
	catch(CException ex)
	{		
		AfxMessageBox("Unknown error",MB_ICONERROR,NULL);
	}
	workSheet.ReleaseDispatch();
	workSheets.ReleaseDispatch();
	workBook.ReleaseDispatch();
	workBooks.ReleaseDispatch();
	appExcel.Quit();
	appExcel.ReleaseDispatch();	
	CoUninitialize();
}

void StaffDiagramViewer::ExcelExportPrintHeader(Shapes& ropShapes, const int& ipPageNo,
												const int& ipGroupNo,BOOL bpIsColorEnabled)
{
	/*************************Export Header Implemented*******************************/
	Shape shapeRect = ropShapes.AddShape(1,0,omXlPgDmns.fmPrintPageHeight * ipPageNo,omXlPgDmns.fmPrintPageWidth,8*omXlPgDmns.fmCellHeight);
      FillFormat fillFormat = shapeRect.GetFill();
	fillFormat.PresetTextured(10);	
	fillFormat.ReleaseDispatch();
	shapeRect.ReleaseDispatch();

	shapeRect = ropShapes.AddShape(1,omXlPgDmns.fmEmployeePoolPrintWidth,omXlPgDmns.fmCellHeight + omXlPgDmns.fmPrintPageHeight * ipPageNo,omXlPgDmns.fmPoolJobPrintWidth,4*omXlPgDmns.fmCellHeight);
      if(bpIsColorEnabled)
	{
		FillFormat fillFormat = shapeRect.GetFill();
		fillFormat.PresetTextured(5); //msoTextureWaterDroplets
		ColorFormat newColorFormat = fillFormat.GetForeColor();
		newColorFormat.SetRgb(RGB(225,225,225));
		newColorFormat.ReleaseDispatch();		
		fillFormat.ReleaseDispatch();
	}
	shapeRect.ReleaseDispatch();
	
	ropShapes.AddLine(omXlPgDmns.fmEmployeePoolPrintWidth,omXlPgDmns.fmPrintPageHeight*ipPageNo + 3*omXlPgDmns.fmCellHeight,omXlPgDmns.fmPrintPageWidth,omXlPgDmns.fmPrintPageHeight*ipPageNo + 3*omXlPgDmns.fmCellHeight)->Release();	
	ropShapes.AddTextEffect(0,GetString(IDS_STAFFCHART),"Courier",12,-1,0,omXlPgDmns.fmEmployeePoolPrintWidth + 10,omXlPgDmns.fmPrintPageHeight*ipPageNo + omXlPgDmns.fmCellHeight + 5)->Release();
    
	CString olPoolName = GetGroupText(ipGroupNo);
	if (omGroupBy != GROUP_BY_POOL)
	{
		olPoolName.Empty();
	}

      CTime olActStart(omStartTime.GetYear(),omStartTime.GetMonth(),omStartTime.GetDay(),
	                 omStartTime.GetHour(),0,0);
	//  add one hour if we are in the second half of the actual hour
	if (omStartTime.GetMinute() > 30)
	{
		olActStart += CTimeSpan(0,1,0,0);
	}

      CTimeSpan olDuration = omEndTime - omStartTime;
	int ilHours = olDuration.GetTotalHours();
	CTime olActEnd = olActStart + CTimeSpan(0,ilHours,0,0);
	omXlPgDmns.omActualStartTime = olActStart;
	omXlPgDmns.omActualEndTime = olActEnd;

	CString olTarget;
	olTarget.Format(GetString(IDS_STRING61285),olPoolName,olActStart.Format("%d.%m.%Y %H:%M"),olActEnd.Format("%d.%m.%Y  %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");

	ropShapes.AddTextEffect(0, olTarget,"Courier",12,0,0,omXlPgDmns.fmEmployeePoolPrintWidth + 10,omXlPgDmns.fmPrintPageHeight * ipPageNo + 3 * omXlPgDmns.fmCellHeight + 5)->Release();
	ropShapes.AddTextEffect(0, olPrintDate, "Courier",8,-1,0,omXlPgDmns.fmPrintPageWidth - 100,omXlPgDmns.fmPrintPageHeight * ipPageNo + omXlPgDmns.fmCellHeight + 5)->Release();	
    
	// loading of the header bitmap from ceda.ini defined resource
	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(pcgAppName, "LOGO","",
		pclTmpText, sizeof pclTmpText, pclConfigPath);
	if(strlen(pclTmpText) != 0)
	{
		try
		{
			ropShapes.AddPicture(pclTmpText,-1,-1,omXlPgDmns.fmCellWidth/2,omXlPgDmns.fmPrintPageHeight * ipPageNo + omXlPgDmns.fmCellHeight,2*omXlPgDmns.fmCellWidth,6*omXlPgDmns.fmCellHeight)->Release();
		}
		catch(...)
		{			
			ropShapes.AddTextEffect(0, "Your logo here", "Courier",10,-1,0,10,4* omXlPgDmns.fmCellHeight)->Release();
		}
	}
	else
	{
		ropShapes.AddTextEffect(0, "Your logo here", "Courier",10,-1,0,10,omXlPgDmns.fmCellHeight)->Release();
	}
	
    /***************************************************************************************/

	/*************************Export TimeScale**********************************************/
	if(omXlPgDmns.bmReset == TRUE)
	{
		omXlPgDmns.bmReset = FALSE;

		omXlPgDmns.imTimeScaleBreakUpSteps = ilHours * 6;
		omXlPgDmns.fmTimeTenMinStepWidth = (float)(omXlPgDmns.fmPoolJobPrintWidth) / omXlPgDmns.imTimeScaleBreakUpSteps;	
		omXlPgDmns.fmTimeOneMinStepWidth = omXlPgDmns.fmTimeTenMinStepWidth / 10;

		omXlPgDmns.fmTimeLongLineHeight = 2 * omXlPgDmns.fmCellHeight;
		omXlPgDmns.fmTimeMediumLineHeight = (float) 1.5* omXlPgDmns.fmCellHeight;
		omXlPgDmns.fmTimeShortLineHeight =(float) omXlPgDmns.fmCellHeight;
	}

	float flTimeScaleTop = omXlPgDmns.fmPoolPrintStartHeight - omXlPgDmns.fmTimeLongLineHeight;
	float flTimeLongLineTop = flTimeScaleTop - omXlPgDmns.fmTimeLongLineHeight;
	float flTimeMediumLineTop = flTimeScaleTop - omXlPgDmns.fmTimeMediumLineHeight;
	float flTimeShortLineTop = flTimeScaleTop - omXlPgDmns.fmTimeShortLineHeight;

	//We need to draw one straight horizontal line for time scale
	ropShapes.AddShape(19,omXlPgDmns.fmEmployeePoolPrintWidth,flTimeScaleTop,omXlPgDmns.fmPoolJobPrintWidth,0)->Release();
	
	//For drawing the line for starting hour
	ropShapes.AddShape(19,omXlPgDmns.fmEmployeePoolPrintWidth,flTimeLongLineTop,0,omXlPgDmns.fmTimeLongLineHeight)->Release();
	CString olText = olActStart.Format("%H:%M");
	ropShapes.AddTextEffect(0,olText,"Courier",8,0,0,omXlPgDmns.fmEmployeePoolPrintWidth - 15,flTimeLongLineTop - 2)->Release();
	
	CTime olHour = olActStart;
	for(int i = 1 ; i < omXlPgDmns.imTimeScaleBreakUpSteps +1; i++)
	{
		if((i % 6) == 0)
		{
			olHour += CTimeSpan(0,1,0,0);
			if(i < omXlPgDmns.imTimeScaleBreakUpSteps)
			{
				olText = olHour.Format("%H:%M");
			}
			else
			{
				olText = olHour.Format("%H");
			}
			ropShapes.AddShape(19,omXlPgDmns.fmEmployeePoolPrintWidth + i * omXlPgDmns.fmTimeTenMinStepWidth, flTimeLongLineTop,0, omXlPgDmns.fmTimeLongLineHeight)->Release();
			ropShapes.AddTextEffect(0,olText,"Courier",8,0,0,omXlPgDmns.fmEmployeePoolPrintWidth - 15  + i * omXlPgDmns.fmTimeTenMinStepWidth,flTimeLongLineTop - 2)->Release();
		}
		else
		{
			if((i % 3) == 0)
			{
				ropShapes.AddShape(19,omXlPgDmns.fmEmployeePoolPrintWidth + i * omXlPgDmns.fmTimeTenMinStepWidth,flTimeMediumLineTop, 0 ,omXlPgDmns.fmTimeMediumLineHeight)->Release();
			}
			else
			{
				ropShapes.AddShape(19,omXlPgDmns.fmEmployeePoolPrintWidth + i * omXlPgDmns.fmTimeTenMinStepWidth,flTimeShortLineTop, 0 , omXlPgDmns.fmTimeShortLineHeight)->Release();
			}
		}
	}
	/***************************************************************************************/
}

void StaffDiagramViewer::ExcelExportPrintPoolHeader(Shapes& ropShapes, const float& fpNextStartOffset, const int& ipGroupNo, BOOL bpIsColorEnabled)
{
	VARIANT varEmpty;
      varEmpty.vt = VT_ERROR;
      varEmpty.scode = DISP_E_PARAMNOTFOUND;

	FillFormat fillFormat  = NULL;
	ColorFormat newColorFormat = NULL;
	/****************Creating rectangle for containing group text and group lines******/
	Shape shapeRect = ropShapes.AddShape(125,0,fpNextStartOffset, omXlPgDmns.fmEmployeePoolPrintWidth ,omXlPgDmns.fmGanttLineHeight);
    	if(bpIsColorEnabled)
	{
		fillFormat = shapeRect.GetFill();
		newColorFormat = fillFormat.GetForeColor();
		newColorFormat.SetRgb(RGB(225,225,225));
		newColorFormat.ReleaseDispatch();	
		fillFormat.ReleaseDispatch();
	}
	shapeRect.ReleaseDispatch();

	shapeRect = ropShapes.AddShape(125,omXlPgDmns.fmEmployeePoolPrintWidth, fpNextStartOffset,omXlPgDmns.fmPoolJobPrintWidth,omXlPgDmns.fmGanttLineHeight);
    	if(bpIsColorEnabled)
	{			
		fillFormat = shapeRect.GetFill();
		newColorFormat = fillFormat.GetForeColor();		
		newColorFormat.SetRgb(RGB(225,225,225));
		newColorFormat.ReleaseDispatch();		
		fillFormat.ReleaseDispatch();
	}
	shapeRect.ReleaseDispatch();

	shapeRect = ropShapes.AddShape(125,omXlPgDmns.fmEmployeePoolPrintWidth + 5,fpNextStartOffset + omXlPgDmns.fmCellHeight / 2,omXlPgDmns.fmPoolJobPrintWidth - 10,omXlPgDmns.fmCellHeight);
    	if(bpIsColorEnabled)
	{
		fillFormat = shapeRect.GetFill();
		newColorFormat = fillFormat.GetForeColor();
		newColorFormat.SetRgb(RGB(225,225,225));
		newColorFormat.ReleaseDispatch();
		fillFormat.ReleaseDispatch();
	}
	shapeRect.ReleaseDispatch();

	/************Creating TextBox for filling Group text***************/
	Shape shapeTextBox = ropShapes.AddTextbox(1,5,fpNextStartOffset + 5, omXlPgDmns.fmEmployeePoolPrintWidth - 65 ,omXlPgDmns.fmGanttLineHeight - 10);
	shapeTextBox.SetAutoShapeType(5); //MsoAutoShapeType
	TextFrame textFrame = shapeTextBox.GetTextFrame();	
	Characters character = textFrame.Characters(varEmpty,varEmpty);
	textFrame.SetHorizontalAlignment(2); //MsoTextEffectAlignment
	character.SetText(GetGroupText(ipGroupNo));
	
	character.ReleaseDispatch();
	textFrame.ReleaseDispatch();

    	if(bpIsColorEnabled)
	{
		fillFormat = shapeTextBox.GetFill();
		newColorFormat = fillFormat.GetForeColor();		
		newColorFormat.SetRgb(RGB(225,225,225));
		newColorFormat.ReleaseDispatch();
		fillFormat.ReleaseDispatch();
	}
	shapeTextBox.ReleaseDispatch();
	
	/**************Creating TextBox for filling line count***************/
	Shape shapeLineCountTextBox = ropShapes.AddTextbox(1,omXlPgDmns.fmEmployeePoolPrintWidth - 50,fpNextStartOffset + 5, 45 ,omXlPgDmns.fmGanttLineHeight - 10);
	shapeLineCountTextBox.SetAutoShapeType(5); //MsoAutoShapeType
	textFrame = shapeLineCountTextBox.GetTextFrame();	
	textFrame.SetHorizontalAlignment(2); //MsoTextEffectAlignment
	character = textFrame.Characters(varEmpty,varEmpty);
	char chLines[10];
	character.SetText(itoa(GetLineCount(ipGroupNo),chLines,10));

	textFrame.ReleaseDispatch();
	character.ReleaseDispatch();

    	if(bpIsColorEnabled)
	{
		fillFormat = shapeLineCountTextBox.GetFill();
		newColorFormat = fillFormat.GetForeColor();		
		newColorFormat.SetRgb(RGB(200,200,225));
		newColorFormat.ReleaseDispatch();
		fillFormat.ReleaseDispatch();
	}
	shapeLineCountTextBox.ReleaseDispatch();
	/***************************************************/	
}

COLORREF StaffDiagramViewer::GetColourForArrFlight(long lpFlightUrno)
{
	COLORREF rlColour = BLACK;
	FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(lpFlightUrno);
	if(!ogFlightData.IsArrivalOrBoth(prlFlight))
	{
		prlFlight = ogFlightData.GetRotationFlight(prlFlight);
	}

	if(prlFlight != NULL)
	{
		int ilReturnFlightType = ogFlightData.IsReturnFlight(prlFlight) ? ID_ARR_RETURNFLIGHT : ID_NOT_RETURNFLIGHT;
		FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight,ilReturnFlightType);
		if(prlFlight->Onbl != TIMENULL)
		{
			rlColour = atol(GetUserData("ONBL"));
		}
		else if(prlFlight->Land != TIMENULL)
		{
			rlColour = atol(GetUserData("LAND"));
		}
		else if(prlRotation != NULL && (prlRotation->Ofbl != TIMENULL || prlRotation->Airb != TIMENULL))
		{
			// departure already offblock or airborne so this must be onbl
			rlColour = atol(GetUserData("ONBL"));
		}
		else if(prlFlight->Tmoa != TIMENULL)
		{
			rlColour = atol(GetUserData("TMOA"));
		}
		else if(prlFlight->Etoa != TIMENULL)
		{
			rlColour = atol(GetUserData("ETOA"));
		}
		else if(prlFlight->Stoa != TIMENULL)
		{
			rlColour = atol(GetUserData("STOA"));
		}
	}

	return rlColour;
}

COLORREF StaffDiagramViewer::GetColourForDepFlight(long lpFlightUrno)
{
	COLORREF rlColour = BLACK;
	FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(lpFlightUrno);
	if(!ogFlightData.IsDepartureOrBoth(prlFlight))
	{
		prlFlight = ogFlightData.GetRotationFlight(prlFlight);
	}

	if(prlFlight != NULL)
	{
		if(prlFlight->Airb != TIMENULL)
		{
			rlColour = atol(GetUserData("AIRB"));
		}
		else if(prlFlight->Ofbl != TIMENULL)
		{
			rlColour = atol(GetUserData("OFBL"));
		}
		else if(prlFlight->Slot != TIMENULL && IsPrivateProfileOn("SHOW_GT_SLOT_MARKER","NO") )
		{
 			rlColour = atol(GetUserData("SLOT"));
		}
		else if(prlFlight->Etod != TIMENULL)
		{
			rlColour = atol(GetUserData("ETOD"));
		}
		else if(prlFlight->Stod != TIMENULL)
		{
			rlColour = atol(GetUserData("STOD"));
		}
	}

	return rlColour;
}

bool StaffDiagramViewer::DisplayMarker(CString opMarkerType)
{
	return (GetUserData(opMarkerType) == "YES") ? true : false;
}