// CedaActData.cpp - Class for gates
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaActData.h>
#include <BasicData.h>

CedaActData::CedaActData()
{                  
    BEGIN_CEDARECINFO(ACTDATA, ActDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Act3,"ACT3")
		FIELD_CHAR_TRIM(Act5,"ACT5")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(ActDataRecInfo)/sizeof(ActDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ActDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"ACTTAB");
    pcmFieldList = "URNO,ACT3,ACT5";
}
 
CedaActData::~CedaActData()
{
	TRACE("CedaActData::~CedaActData called\n");
	ClearAll();
}

void CedaActData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaActData::ReadActData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaActData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		ACTDATA rlActData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlActData)) == RCSuccess)
			{
				AddActInternal(rlActData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaActData Read %d Records (Rec Size %d) %s",omData.GetSize(),sizeof(ACTDATA), pclWhere);
    return ilRc;
}


void CedaActData::AddActInternal(ACTDATA &rrpAct)
{
	ACTDATA *prlAct = new ACTDATA;
	*prlAct = rrpAct;
	omData.Add(prlAct);
	omUrnoMap.SetAt((void *)prlAct->Urno,prlAct);
}


ACTDATA* CedaActData::GetActByUrno(long lpUrno)
{
	ACTDATA *prlAct = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlAct);
	return prlAct;
}

void CedaActData::GetAct3Names(CStringArray &ropActNames)
{
	int ilNumActs = omData.GetSize();
	for(int ilAct = 0; ilAct < ilNumActs; ilAct++)
	{
		ropActNames.Add(omData[ilAct].Act3);
	}
}

void CedaActData::GetAct5Names(CStringArray &ropActNames)
{
	int ilNumActs = omData.GetSize();
	for(int ilAct = 0; ilAct < ilNumActs; ilAct++)
	{
		ropActNames.Add(omData[ilAct].Act5);
	}
}

void CedaActData::GetFormattedActNames(CStringArray &ropActNames)
{
	CString olAct;
	int ilNumActs = omData.GetSize();
	for(int ilAct = 0; ilAct < ilNumActs; ilAct++)
	{
		olAct.Format("%s/%s", omData[ilAct].Act3, omData[ilAct].Act5);
		ropActNames.Add(olAct);
	}
}

void CedaActData::GetAct3AndAct5FromFormattedName(const char *pcpFormattedName, CString &ropAct3, CString &ropAct5)
{
	CStringArray olActs;
	ogBasicData.ExtractItemList(pcpFormattedName, &olActs,'/');
	int ilCount = olActs.GetSize();
	if(ilCount >= 1)
		ropAct3 = olActs[0];
	if(ilCount >= 2)
		ropAct5 = olActs[1];
}

CString CedaActData::GetTableName(void)
{
	return CString(pcmTableName);
}