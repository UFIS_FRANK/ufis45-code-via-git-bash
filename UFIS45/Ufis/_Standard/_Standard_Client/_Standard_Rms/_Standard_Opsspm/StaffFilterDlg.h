// StaffFilterDlg.h : header file
//
#ifndef _CSTAFFFOLTERDIALOG_H_
#define _CSTAFFFILTERDIALOG_H_

/////////////////////////////////////////////////////////////////////////////
// CStaffFilterDialog dialog

class CStaffFilterDialog : public CDialog
{
// Construction
public:
    CStaffFilterDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
    //{{AFX_DATA(CStaffFilterDialog)
    enum { IDD = IDD_STAFF_FILTER };
    BOOL    m_bEarlyShift;
    BOOL    m_bLateShift;
    BOOL    m_bStationA;
    BOOL    m_bStationB;
    //}}AFX_DATA

// Implementation
protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    // Generated message map functions
    //{{AFX_MSG(CStaffFilterDialog)
    virtual BOOL OnInitDialog();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};
#endif // _CSTAFFFOLTERDIALOG_H_
