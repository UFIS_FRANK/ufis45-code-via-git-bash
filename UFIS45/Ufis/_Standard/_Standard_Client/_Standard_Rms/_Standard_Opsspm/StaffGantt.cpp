// stgantt.cpp : implementation file
//  

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CLIENTWN.H>
#include <CCSCedaData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <tscale.h>
#include <cviewer.h>
#include <CCSDragDropCtrl.h>
#include <gbar.h>
#include <cxbutton.h>
#include <PrePlanTable.h>
#include <CCITable.h>
#include <GateTable.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <CciDiagram.h>
#include <StartDate.h>
#include <ConflictConfigTable.h>
#include <ufis.h>
#include <CCSBcHandle.h>
#include <conflict.h>
#include <ConflictTable.h>
#include <ButtonList.h>
#include <ccsddx.h>
#include <StaffViewer.h>
#include <StaffDetailWindow.h>
#include <StaffGantt.h>
#include <dataset.h>
#include <BasicData.h>
#include <TeamDelegationDlg.h>
#include <AvailableEmpsDlg.h>
#include <CciDemandDetailDlg.h>
#include <CedaSerData.h>

#include <AssignDlg.h>
#include <AssignJobDetachDlg.h>
#include <AssignFromPrePlanTableDlg.h>
#include <dbreak.h>

#include <TimePacket.h>
#include <CciDeskDlg.h>
#include <dgatejob.h>
#include <FlightDetailWindow.h>
#include <DJobChange.h>
#include <DChange.h>
#include <JobTimeHandler.h>

#include <AllocDlg.h>
#include <CedaEquData.h>
#include <SplitJobDlg.h>
#include <FieldConfigDlg.h>
#include <CedaShiftTypeData.h>
#include <CedaSprData.h>
#include <LastJobInfo.h> //PRF 8998
#include <PrmJobWithoutDemDlg.h>
#include <PrmAssignPdaDlg.h>
#include <CedaPdaData.h>

#include <CedaSpeData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsTotallyInside(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))
extern bool check;

bool dropfrom=false;

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This block of code is used for substituing the scale calculation
// since the TimeScale has been implemented with a different pixel
// calculation algorithm (Pichet used the floating point calculation,
// which introduce +1 or -1 rounding effect. Furthermore, he still has
// some adjustments may be 2 or 3 pixels for makeing the TimeScale
// display nicely :-). Besides, there are still more few pixels which
// display inappropriately which introduced from the border of the
// GateChart or the other classes of the same kind). To fix all of these
// things, the only fastest way is just adopt the scaling algorithm
// written in the TimeScale and come back to clean up this sometime later.
//
// After a few though, just replace the macro GetX(time) and GetTime(x)
// should be enough, still we had to update the "omDisplayStart" and
// "omDisplayEnd" everytime these value has been changed.
#define	FIX_TIMESCALE_ROUNDING_ERROR
#ifdef	FIX_TIMESCALE_ROUNDING_ERROR
#define GetX(time)	(pomTimeScale->GetXFromTime(time) + imVerticalScaleWidth)
#define GetCTime(x)	(pomTimeScale->GetTimeFromX((x) - imVerticalScaleWidth))
#else
////////////////////////////////////////////////////////////////////////

// Macro definition for get X-coordinate from the given time
#define GetX(time)  (omDisplayStart == omDisplayEnd? -1: \
            (imVerticalScaleWidth + \
            (((time) - omDisplayStart).GetTotalSeconds() * \
            (imWindowWidth - imVerticalScaleWidth) / \
            (omDisplayEnd - omDisplayStart).GetTotalSeconds())))

// Macro definition for get time from the given X-coordinate
#define GetCTime(x)  ((imWindowWidth - imVerticalScaleWidth) == 0? TIMENULL: \
            (omDisplayStart + \
            (time_t)(((x) - imVerticalScaleWidth) * \
            (omDisplayEnd - omDisplayStart).GetTotalSeconds() / \
            (imWindowWidth - imVerticalScaleWidth))))

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
#endif
////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// StaffGantt
CDWordArray StaffGantt::omSelectedEmployees;

StaffGantt::StaffGantt(StaffDiagramViewer *popViewer, int ipGroupno,
    int ipVerticalScaleWidth, int ipVerticalScaleIndent,
    CFont *popVerticalScaleFont, CFont *popGanttChartFont,
    int ipGutterHeight, int ipOverlapHeight,
    COLORREF lpVerticalScaleTextColor, COLORREF lpVerticalScaleBackgroundColor,
    COLORREF lpHighlightVerticalScaleTextColor, COLORREF lpHighlightVerticalScaleBackgroundColor,
    COLORREF lpGanttChartTextColor, COLORREF lpGanttChartBackgroundColor,
    COLORREF lpHighlightGanttChartTextColor, COLORREF lpHighlightGanttChartBackgroundColor)
{
    SetViewer(popViewer, ipGroupno);
    SetVerticalScaleWidth(ipVerticalScaleWidth);
	if((ogStaffIndexes.Chart == MS_SANS12) || (ogStaffIndexes.Chart == MS_SANS8))
	{
		ipVerticalScaleIndent = 1;
	}
    SetVerticalScaleIndent(ipVerticalScaleIndent);
	SetFonts(ogStaffIndexes.VerticalScale, ogStaffIndexes.Chart);
    SetGutters(ipGutterHeight, ipOverlapHeight);
    SetVerticalScaleColors(lpVerticalScaleTextColor, lpVerticalScaleBackgroundColor,
        lpHighlightVerticalScaleTextColor, lpHighlightVerticalScaleBackgroundColor);
    SetGanttChartColors(lpGanttChartTextColor, lpGanttChartBackgroundColor,
        lpHighlightGanttChartTextColor, lpHighlightGanttChartBackgroundColor);

    // Initialize default values
    omDisplayStart = TIMENULL;
    omDisplayEnd = TIMENULL;
    imWindowWidth = 0;          // unessential -- WM_SIZE will initialize this
    bmIsFixedScaling = TRUE;    // unessential -- will be set in method SetDisplayWindow()
    omCurrentTime = TIMENULL;
    omMarkTimeStart = TIMENULL;
    omMarkTimeEnd = TIMENULL;
    pomStatusBar = NULL;
	pomTimeScale = NULL;
    bmIsMouseInWindow = FALSE;
    bmIsControlKeyDown = FALSE;
    imHighlightLine = -1;
    imCurrentBar = -1;
	lmActiveUrno  = -1;
	bmActiveBar   = FALSE;
	bmActiveBkBar = FALSE;

	// Id 1-Oct-96
	// This will help us create a virtual double click on a double click after SetCursorPos().
	omLastClickedPosition = CPoint(-1, -1);

    // Required only if you allow moving/resizing
    SetBorderPrecision(15);
    umResizeMode = HTNOWHERE;

	omBlackSolidPen1.CreatePen(PS_SOLID, 1, BLACK);
	omWhiteSolidPen1.CreatePen(PS_SOLID, 1, WHITE);
	omBlackSolidPen2.CreatePen(PS_SOLID, 2, BLACK);
	omYellowSolidPen1.CreatePen(PS_SOLID, 2, YELLOW);
}

StaffGantt::~StaffGantt()
{
	pogButtonList->UnRegisterTimer(this);    // install the timer
}

void StaffGantt::SetViewer(StaffDiagramViewer *popViewer, int ipGroupno)
{
    pomViewer = popViewer;
    imGroupno = ipGroupno;
}

void StaffGantt::SetVerticalScaleWidth(int ipWidth)
{
    imVerticalScaleWidth = ipWidth;
}

void StaffGantt::SetVerticalScaleIndent(int ipIndent)
{
    imVerticalScaleIndent = ipIndent;
}

void StaffGantt::SetFonts(int ipIndex1, int ipIndex2)
{
	LOGFONT rlLogFont;
    pomVerticalScaleFont = &ogScalingFonts[min(ipIndex1,1)];//popVerticalScaleFont;
    pomGanttChartFont = &ogScalingFonts[ipIndex2];//&ogSmallFonts_Regular_4;//popGanttChartFont;
	pomWIFBarFont = &ogScalingFonts[ipIndex2 < 3 ? ipIndex2+1 : ipIndex2];
	pomGanttChartFont->GetLogFont(&rlLogFont);

    // Calculate the normal height of a bar
    CDC dc;
    dc.CreateCompatibleDC(NULL);
    if (pomGanttChartFont)
        dc.SelectObject(pomGanttChartFont);
    TEXTMETRIC tm;
    dc.GetTextMetrics(&tm);
    imBarHeight = tm.tmHeight + tm.tmExternalLeading + 2;
    imLeadingHeight = (tm.tmExternalLeading + 2) / 2;
    dc.DeleteDC();
}

void StaffGantt::SetGutters(int ipGutterHeight, int ipOverlapHeight)
{
    imGutterHeight = ipGutterHeight;
    imOverlapHeight = ipOverlapHeight;
}

void StaffGantt::SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
    lmVerticalScaleTextColor = lpTextColor;
    lmVerticalScaleBackgroundColor = lpBackgroundColor;
    lmHighlightVerticalScaleTextColor = lpHighlightTextColor;
    lmHighlightVerticalScaleBackgroundColor = lpHighlightBackgroundColor;
}

void StaffGantt::SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
    lmGanttChartTextColor = lpTextColor;
    lmGanttChartBackgroundColor = lpBackgroundColor;
    lmHighlightGanttChartTextColor = lpHighlightTextColor;
    lmHighlightGanttChartBackgroundColor = lpHighlightBackgroundColor;
}

// Attentions:
// Return the height that it's expected for being displayed
// Be careful, this one may be called before the CListBox was created.
//
int StaffGantt::GetGanttChartHeight()
{
    int ilHeight = 0;
    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(imGroupno); ilLineno++)
        ilHeight += GetLineHeight(ilLineno);

    return ilHeight;
}

int StaffGantt::GetLineHeight(int ilLineno)
{
	int ilVisualMaxOverlapLevel = pomViewer->GetVisualMaxOverlapLevel(imGroupno, ilLineno);
	return (2 * imGutterHeight) + imBarHeight + (ilVisualMaxOverlapLevel * imOverlapHeight);
}		

BOOL StaffGantt::Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID)
{
    // Make sure that the viewer is already given
    if (pomViewer == NULL)
        return FALSE;

    // Make sure that all essential style is defined
    // WS_CLIPSIBLINGS is very important for the ListBox so it does not paint outside its window
    dwStyle |= WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_CLIPSIBLINGS;
    dwStyle |= LBS_NOINTEGRALHEIGHT | LBS_OWNERDRAWVARIABLE;
    dwStyle |= LBS_MULTIPLESEL;

    // Create the window
    if (CListBox::Create(dwStyle, rect, pParentWnd, nID) == FALSE)
        return FALSE;

    // Create items according to what's in the Viewer
    ResetContent();
    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(imGroupno); ilLineno++)
    {
		AddString("");
		SetItemHeight(ilLineno, GetLineHeight(ilLineno));
    }

    return TRUE;
}

void StaffGantt::SetStatusBar(CStatusBar *popStatusBar)
{
    pomStatusBar = popStatusBar;
}

void StaffGantt::SetTimeScale(CTimeScale *popTimeScale)
{
    pomTimeScale = popTimeScale;
}

void StaffGantt::SetBorderPrecision(int ipBorderPrecision)
{
    imBorderPreLeft = ipBorderPrecision / 2;
    imBorderPreRight = (ipBorderPrecision+1) / 2;
}

void StaffGantt::SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling)
{
    omDisplayStart = opDisplayStart;
    omDisplayEnd = opDisplayEnd;
    bmIsFixedScaling = bpFixedScaling;

    if (m_hWnd == NULL)
        imWindowWidth = 0;  // window is still not opened
    else
    {
        CRect rect;
        GetWindowRect(&rect);   // can't use client rect (vertical scroll may less its width)
        imWindowWidth = rect.Width();   // however, this is correct only if window has no border
        RepaintGanttChart();    // redraw the entire screen
    }
}

void StaffGantt::SetDisplayStart(CTime opDisplayStart)
{
    if (bmIsFixedScaling)   // must shift both start/end time while keeping the old time span
        omDisplayEnd = opDisplayStart + (omDisplayEnd - omDisplayStart).GetTotalSeconds();
    omDisplayStart = opDisplayStart;
    RepaintGanttChart();    // redraw the entire screen
}

void StaffGantt::SetCurrentTime(CTime opCurrentTime)
{                               
    RepaintGanttChart(-1, omCurrentTime, opCurrentTime);
    omCurrentTime = opCurrentTime;
}

void StaffGantt::SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd)
{
	// There are two reasons for these min/max.
	// First, we have to make sure that the start time parameter for RepaintGanttChart()
	// should not be greater than the end time parameter.
	// Second, we should not refresh markers in the vertical scale, since this will
	// disturb the repainting of the vertical scale when we are drawing these marker lines
	// together with the focus rectangle when moving/resizing a bar.
    RepaintGanttChart(-1,
		max(omDisplayStart, min(omMarkTimeStart, opMarkTimeStart)),
		max(omDisplayStart, max(omMarkTimeStart, opMarkTimeStart)));
    omMarkTimeStart = opMarkTimeStart;
    RepaintGanttChart(-1,
		max(omDisplayStart, min(omMarkTimeEnd, opMarkTimeEnd)),
		max(omDisplayStart, max(omMarkTimeEnd, opMarkTimeEnd)));
    omMarkTimeEnd = opMarkTimeEnd;
}

void StaffGantt::RepaintVerticalScale(int ipLineno, BOOL bpErase)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcPaint;
    if (ipLineno == -1) // repaint the whole chart?
        GetClientRect(&rcPaint);
    else
        GetItemRect(ipLineno, &rcPaint);

	rcPaint.right = imVerticalScaleWidth - 1;
	InvalidateRect(&rcPaint, bpErase);
}

void StaffGantt::RepaintGanttChart(int ipLineno, CTime opStartTime, CTime opEndTime, BOOL bpErase)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcPaint;
    if (ipLineno == -1) // repaint the whole chart?
        GetClientRect(&rcPaint);
    else
        GetItemRect(ipLineno, &rcPaint);

    // Update the GanttChart body
    if (opStartTime <= omDisplayStart)
        rcPaint.left = imVerticalScaleWidth;
    else
        rcPaint.left = (int)GetX(opStartTime);

    // Use the client width if user want to repaint to the end of time
    if (opEndTime != TIMENULL && opEndTime <= omDisplayEnd)
        rcPaint.right = (int)GetX(opEndTime) + 1;

    InvalidateRect(&rcPaint, bpErase);
}

void StaffGantt::RepaintItemHeight(int ipLineno)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    // This repaint method will recalculate the item height
    SetItemHeight(ipLineno, GetLineHeight(ipLineno));

    CRect rcItem, rcPaint;
    GetClientRect(&rcPaint);
    GetItemRect(ipLineno, &rcItem);
    rcPaint.top = rcItem.top;
    InvalidateRect(&rcPaint, TRUE);
}


/////////////////////////////////////////////////////////////////////////////
// StaffGantt implementation

void StaffGantt::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
    // We have to define MeasureItem() here since MFC places an ASSERT inside
    // the default MeasureItem() of an owner-drawn CListBox.
}

void StaffGantt::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// To let the GanttChart know automatically those changes in TimeScale.
// We had to reset the value of omDisplayStart and omDisplayEnd everytime.
	omDisplayStart = GetCTime(imVerticalScaleWidth);
	omDisplayEnd = GetCTime(imWindowWidth);
////////////////////////////////////////////////////////////////////////
    
	int itemID = lpDrawItemStruct->itemID;

    // Attention:
    // This optimization will the speed of displaying GanttChart very a lot.
    // However, this works because we hadn't display any selected or focus items.
    // (We use the "imCurrentItem" variable instead.)
    // So, be careful since this assumption may be changed in the future.
    //
    if (itemID == -1 || itemID > pomViewer->GetLineCount(imGroupno)-1)
        return;
    if (!(lpDrawItemStruct->itemAction & ODA_DRAWENTIRE))
        return; // nothing more to do if listbox just change its selection and/or focus item

    // Drawing routines start here
    CDC dc;
    dc.Attach(lpDrawItemStruct->hDC);
    CRect rcItem(&lpDrawItemStruct->rcItem);
    CRect rcClip;
    dc.GetClipBox(&rcClip);


	if(!pomViewer->LineIsBottomOfTeam(imGroupno, itemID))
	{
		DrawDottedLines(&dc, itemID, rcItem);
	}

    // Draw time lines and bars if necessary
    if (imVerticalScaleWidth <= rcClip.right)
    {
        CRgn rgn;
        CRect rect(imVerticalScaleWidth, rcItem.top, rcItem.right, rcItem.bottom);
        rgn.CreateRectRgnIndirect(&rect);
        dc.SelectClipRgn(&rgn);
		DrawBackgroundBars(&dc, itemID, rcItem, rcClip);
        DrawTimeLines(&dc, rcItem.top, rcItem.bottom);
        DrawGanttChart(&dc, itemID, rcItem, rcClip);
        if (umResizeMode != HTNOWHERE)
            DrawFocusRect(&dc, &omRectResize);
        dc.SelectClipRgn(NULL);
    }
    // Draw vertical scale if necessary
    if (rcClip.left < imVerticalScaleWidth)
	{
        DrawVerticalScale(&dc, itemID, rcItem);
	}

	if(pomViewer->LineIsBottomOfTeam(imGroupno, itemID))
	{
		Draw3dLines(&dc, itemID, rcItem);
	}

    dc.Detach();
}


/////////////////////////////////////////////////////////////////////////////
// StaffGantt implementation helper functions


void StaffGantt::DrawDottedLines(CDC *pDC, int itemID, const CRect &rcItem)
{
    CBitmap Bitmap;
	CTime olTime, olTmpTime;

	// select dotted pen
	CPen olDottedLinePen(PS_DOT, 1, BLACK);
	COLORREF olOldBkColor = pDC->SetBkColor(SILVER);
	CPen *pOldPen;

	// dotted horizontal lines
	CRect olRect(rcItem);
	int ilHeight = 0;
	CRect olTmpRect = olRect;
	olRect.bottom = olRect.top + GetLineHeight(itemID);
	int ilLeft;

	pOldPen = pDC->SelectObject(&olDottedLinePen);
	ilLeft = olRect.left + imVerticalScaleWidth + imVerticalScaleIndent;
	pDC->MoveTo(ilLeft, olRect.bottom - 1);
	pDC->LineTo(olRect.right, olRect.bottom - 1);
	
	olRect.top += GetLineHeight(itemID);
                     
	pDC->SelectObject(pOldPen);	
	pDC->SetBkColor(olOldBkColor);
}


void StaffGantt::Draw3dLines(CDC *pDC, int itemID, const CRect &rcItem)
{
    CBitmap Bitmap;
	CTime olTime, olTmpTime;

	// select dotted pen
	COLORREF olOldBkColor = pDC->SetBkColor(SILVER);
	CPen *pOldPen;

	// dotted horizontal lines
	CRect olRect(rcItem);
	int ilHeight = 0;
	CRect olTmpRect = olRect;
	olRect.bottom = olRect.top + GetLineHeight(itemID);
	int ilLeft;

	ilLeft = olRect.left + imVerticalScaleWidth;

	CPen olSolidLinePen1(PS_SOLID, 1, BLACK);
	pOldPen = pDC->SelectObject(&olSolidLinePen1);
	pDC->MoveTo(ilLeft, olRect.bottom-2);
	pDC->LineTo(olRect.right, olRect.bottom-2);

	CPen olSolidLinePen2(PS_SOLID, 1, WHITE);
	pDC->SelectObject(&olSolidLinePen2);
	pDC->MoveTo(ilLeft, olRect.bottom-1);
	pDC->LineTo(olRect.right, olRect.bottom-1);
	
	olRect.top += GetLineHeight(itemID);
                     
	pDC->SelectObject(pOldPen);	
	pDC->SetBkColor(olOldBkColor);
}

void StaffGantt::DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem)
{
	CPoint point;
    ::GetCursorPos(&point);
    ScreenToClient(&point);

    // Check if we need to change the highlight text in VerticalScale
    BOOL blHighlight;
    if(!bmIsMouseInWindow || !rcItem.PtInRect(point))
	{
        blHighlight = FALSE;
	}
    else
    {
        blHighlight = TRUE;
        if(imHighlightLine != itemID)
		{
            RepaintVerticalScale(imHighlightLine, FALSE);
		}
        imHighlightLine = itemID;
    }

    // Drawing routines start here
    CString olLineText = pomViewer->GetLineText(imGroupno, itemID);
    CFont *pOldFont = pDC->SelectObject(pomVerticalScaleFont);
	//COLORREF nOldTextColor = pDC->SetTextColor(blHighlight ? lmHighlightVerticalScaleTextColor : lmVerticalScaleTextColor);

	COLORREF nOldTextColor = pDC->SetTextColor((blHighlight) ? BLACK : pomViewer->GetTextColour(imGroupno, itemID));

    COLORREF nOldBackgroundColor = pDC->SetBkColor(blHighlight ? lmHighlightVerticalScaleBackgroundColor : lmVerticalScaleBackgroundColor);

    CRect olRect(rcItem);
    int ilLeft = olRect.left + imVerticalScaleIndent;
	olRect.right = imVerticalScaleWidth - 2;

	if(pomViewer->LineIsTopOfTeam(imGroupno, itemID))
	{
		// group leaders can be clicked to expand/contract their groups, so add a +/- button

		// text may need to be shifted to the right to accomodate the +/- button
		pDC->ExtTextOut(olRect.left+max(imVerticalScaleIndent,olRect.left+15), olRect.top, ETO_CLIPPED | ETO_OPAQUE, &olRect, olLineText, lstrlen(olLineText), NULL);

		if(ogBasicData.IsPaxServiceT2() == false) //Singapore
		{
		    // draw the frame for the +/- button
		    pDC->SelectObject(&omBlackSolidPen1);
		    pDC->Rectangle(olRect.left+1,olRect.top+1,olRect.left+14,olRect.top+14);
		    pDC->FillSolidRect(olRect.left+2,olRect.top+2,11,11,WHITE);

		
		if(pomViewer->IsTeamCompressed(imGroupno, itemID))
		{
			// draw the + button
			pDC->MoveTo(olRect.left+4, olRect.top+7);
			pDC->LineTo(olRect.left+11, olRect.top+7);
			pDC->MoveTo(olRect.left+7, olRect.top+4);
			pDC->LineTo(olRect.left+7, olRect.top+11);
		}
		else
		{
			// draw the - button
			pDC->MoveTo(olRect.left+5, olRect.top+7);
			pDC->LineTo(olRect.left+10, olRect.top+7);
		}
	}
	}
	else
	{
		if(pomViewer->IsDoubleTourTeacher(imGroupno, itemID))
		{
			// text may need to be shifted to the right to accomodate the +/- button
			pDC->ExtTextOut(olRect.left+max(imVerticalScaleIndent,olRect.left+15), olRect.top, ETO_CLIPPED | ETO_OPAQUE, &olRect, olLineText, lstrlen(olLineText), NULL);
			// draw the frame for the +/- button
			pDC->SelectObject(&omBlackSolidPen1);
			pDC->Rectangle(olRect.left+1,olRect.top+1,olRect.left+14,olRect.top+14);
			pDC->FillSolidRect(olRect.left+2,olRect.top+2,11,11,AQUA);
			// draw the 'T' for teacher
			pDC->MoveTo(olRect.left+5, olRect.top+4); // top line
			pDC->LineTo(olRect.left+10, olRect.top+4); // top line
			pDC->MoveTo(olRect.left+7, olRect.top+4); // vertical
			pDC->LineTo(olRect.left+7, olRect.top+11); // vertical
		}
		else
		{
			pDC->ExtTextOut(ilLeft, olRect.top, ETO_CLIPPED | ETO_OPAQUE, &olRect, olLineText, lstrlen(olLineText), NULL);
		}
	}


    
	if(pomViewer->LineIsBottomOfTeam(imGroupno, itemID))
	{
		// draw the 3D seperator, seperating teams/groups
		pDC->SelectObject(&omBlackSolidPen2);
		pDC->MoveTo(olRect.left, olRect.bottom-1);
		pDC->LineTo(olRect.right, olRect.bottom-1);

		pDC->SelectObject(&omWhiteSolidPen1);
		pDC->MoveTo(olRect.left, olRect.bottom-1);
		pDC->LineTo(olRect.right, olRect.bottom-1);
	}

	if(EmployeeIsSelected(imGroupno, itemID))
	{
		pDC->SelectObject(&omYellowSolidPen1);
		pDC->MoveTo(olRect.left+2, olRect.top+1);
		pDC->LineTo(olRect.right-1, olRect.top+1);
		pDC->LineTo(olRect.right-1, olRect.bottom-10);
		pDC->LineTo(olRect.left+2, olRect.bottom-10);
		pDC->LineTo(olRect.left+2, olRect.top+1);
	}

	
	pDC->SetBkColor(nOldBackgroundColor);
    pDC->SetTextColor(nOldTextColor);
    pDC->SelectObject(pOldFont);
}

void StaffGantt::DrawBackgroundBars(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
	int ilPixel = 1;
	if (omDisplayStart >= omDisplayEnd) // no space to display?
		return;

	// Draw each bar if it in the range of the clipped box
	for (int i = 0; i < pomViewer->GetBkBarCount(imGroupno, itemID); i++)
	{
		STAFF_BKBARDATA *prlBkBar = pomViewer->GetBkBar(imGroupno, itemID, i);

		if (!IsOverlapped(prlBkBar->StartTime, prlBkBar->EndTime, omDisplayStart, omDisplayEnd))
			continue;   // skip bar which is out of time range

		int left = (int)GetX(prlBkBar->StartTime);
		int right = (int)GetX(prlBkBar->EndTime);
		int top = rcItem.top;
		int bottom = rcItem.bottom;

		if (!IsOverlapped(left, right, rcClip.left, rcClip.right))
			continue;   // skip bar which is out of clipping region

		CCSPtrArray<BARDECO> olDecoData;
		if(prlBkBar->OfflineBar)
			GanttBar::MakeOfflineSymbol(olDecoData, left, top, right, bottom);

		GanttBar paintbar(pDC, CRect(left, top, right, bottom),
			prlBkBar->FrameType, MARKFULL, prlBkBar->MarkerBrush,
			prlBkBar->Text, pomGanttChartFont, 
			prlBkBar->TextColor != 0L ? prlBkBar->TextColor : lmGanttChartTextColor, ilPixel,
			FALSE,CRect(0,0,0,0),NULL,&olDecoData,TA_LEFT,NULL,0,prlBkBar->FrameColor);
	}
}

void StaffGantt::DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
	
	
	int ilPixel = 1;

    if (omDisplayStart >= omDisplayEnd) // no space to display?
        return;

	bool blDisplayArrivalMarker = pomViewer->DisplayMarker("ARRIVAL");
	bool blDisplayDepartureMarker = pomViewer->DisplayMarker("DEPARTURE");
	bool blDisplayTurnaroundMarker = pomViewer->DisplayMarker("TURNAROUND");
	bool blShowCornerMarker = IsPrivateProfileOn("SHOW_STAFF_MARKER_FILTER","NO");

    // Draw each bar if it in the range of the clipped box
    
	JobDataStruct *prlJob;
	
	for (int i = 0; i < pomViewer->GetBarCount(imGroupno, itemID); i++)
    {
        
       STAFF_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, i);

		   
	   
	 if ( prlBar && ((prlJob = ogJobData.GetJobByUrno(prlBar->JobUrno)) != NULL))
	 {
        
	          if (strcmp(prlJob->Jtco,JOBCCC) == 0)
			{
			     DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prlJob);
			
			     if(prlDemand && prlDemand->Dety[0] == CCI_DEMAND)
				 {
                     
					 RUDDATA *prlRudData = ogRudData.GetRudByUrno(prlDemand->Urud);
		             if(prlRudData != NULL)
					 {

                       	  SERDATA *prlSer = ogSerData.GetSerByUrno(prlRudData->Ughs);

						  char posi[26];	
					
                            CString pclConfigPath =ogBasicData.GetConfigFileName();
                            GetPrivateProfileString(pcgAppName, "CCI_JOB_BAR_DESC","FALSE",posi, sizeof posi, pclConfigPath);
	            
               				
							if (strcmp(posi,"SERVICE_NAME")==0)  // ceda
							 {
						  		CString s;
								s.Format("%s,%s",prlSer->Snam,prlDemand->Alid);
							    prlBar->Text=s;
							} 

							 else if (strcmp(posi,"SERVICE_CODE")==0)
							{
			   					 CString s;
								 s.Format("%s,%s",prlSer->Seco,prlDemand->Alid);
								 prlBar->Text=s;
			   				 }

			 
					 }


				 }


			  }
	
	 }
	   
	   
	   
	   	     
	   if (!IsOverlapped(prlBar->StartTime, prlBar->EndTime, omDisplayStart, omDisplayEnd))
            continue;   // skip bar which is out of time range

        int left = (int)GetX(prlBar->StartTime);
        int right = (int)GetX(prlBar->EndTime);
        int top = rcItem.top + imGutterHeight + (prlBar->OverlapLevel * imOverlapHeight);
        int bottom = top + imBarHeight;

        if (!IsOverlapped(left, right, rcClip.left, rcClip.right))
            continue;   // skip bar which is out of clipping region

		CCSPtrArray<BARDECO> olDecoData;  //For additional decoration
		olDecoData.RemoveAll();

		if(prlBar->Infm)
			GanttBar::MakeEmpInformedSymbol(olDecoData, left, top, right, bottom, (!prlBar->Ackn ) ? RGB(0,0,0) : RGB(255,255,255));

		if(prlBar->DifferentFunction)
			GanttBar::MakeFunctionSymbol(olDecoData, left, top, right, bottom);

		if(prlBar->OfflineBar)
			GanttBar::MakeOfflineSymbol(olDecoData, left, top, right, bottom);

		COLORREF llFrameColor = (prlBar->Selected) ? YELLOW : prlBar->FrameColor;
		ilPixel = (ogStaffIndexes.Chart == MS_SANS12 || prlBar->Selected) ? 2 : 1;

		int ilDecoCntBefMarker = olDecoData.GetSize();

		//For additional decoration like corner marker
		if(prlBar->FlightUrno != 0 && blShowCornerMarker)
		{
			CPoint olPoints[3];
			int ilLeft = (int)GetX(prlBar->StartTime);
			int ilRight = (int)GetX(prlBar->EndTime);
			int ilTop = rcItem.top + (prlBar->OverlapLevel * imOverlapHeight)+2;
			int ilBottom = ilTop + imBarHeight;

			if(	(blDisplayArrivalMarker && prlBar->FlightType == STFBAR_ARRIVAL) ||
				(blDisplayTurnaroundMarker && prlBar->FlightType == STFBAR_TURNAROUND))
			{
				// create a black background triangle slightly bigger than the coloured
				// triangle created below so that a blak diagonal line is displayed
				olPoints[0].x = ilLeft;
				olPoints[0].y = ilTop;

				olPoints[1].x = ilLeft; 
				olPoints[1].y = ilBottom;
				
				olPoints[2].x = ilLeft+(ilBottom-ilTop);
				olPoints[2].y = ilTop;

				BARDECO *prlDeco = new BARDECO;
				prlDeco->type = BD_REGION;
				prlDeco->Color = BLACK;

				CRgn *polRgn = new CRgn;
				polRgn->CreatePolygonRgn( olPoints, 3,  WINDING);
				prlDeco->Region = polRgn;

				olDecoData.Add(prlDeco);

				// left triangle
				olPoints[0].x = ilLeft+1;
				olPoints[0].y = ilTop+1;

				olPoints[1].x = ilLeft+1; 
				olPoints[1].y = ilBottom-2;
				
				olPoints[2].x = ilLeft+(ilBottom-ilTop-2);
				olPoints[2].y = ilTop+1;

				prlDeco = new BARDECO;
				prlDeco->type = BD_REGION;
				prlDeco->Color = pomViewer->GetColourForArrFlight(prlBar->FlightUrno);

				polRgn = new CRgn;
				polRgn->CreatePolygonRgn( olPoints, 3,  WINDING);
				prlDeco->Region = polRgn;

				olDecoData.Add(prlDeco);
			}
			if(	(blDisplayDepartureMarker && prlBar->FlightType == STFBAR_DEPARTURE) ||
				(blDisplayTurnaroundMarker && prlBar->FlightType == STFBAR_TURNAROUND))
			{
				ilRight++;
				// create a black background triangle slightly bigger than the coloured
				// triangle created below so that a blak diagonal line is displayed
				olPoints[0].x = ilRight;
				olPoints[0].y = ilTop;

				olPoints[1].x = ilRight-(ilBottom-ilTop); 
				olPoints[1].y = ilBottom;
				
				olPoints[2].x = ilRight;
				olPoints[2].y = ilBottom;

				BARDECO *prlDeco = new BARDECO;
				prlDeco->type = BD_REGION;
				prlDeco->Color = BLACK;

				CRgn *polRgn = new CRgn;
				polRgn->CreatePolygonRgn( olPoints, 3,  WINDING);
				prlDeco->Region = polRgn;

				olDecoData.Add(prlDeco);

				// right triangle
				olPoints[0].x = ilRight-1;
				olPoints[0].y = ilTop+2;

				olPoints[1].x = ilRight-(ilBottom-ilTop-2); 
				olPoints[1].y = ilBottom-1;
				
				olPoints[2].x = ilRight-1;
				olPoints[2].y = ilBottom-1;

				prlDeco = new BARDECO;
				prlDeco->type = BD_REGION;
				prlDeco->Color = pomViewer->GetColourForDepFlight(prlBar->FlightUrno);

				polRgn = new CRgn;
				polRgn->CreatePolygonRgn( olPoints, 3,  WINDING);
				prlDeco->Region = polRgn;

				olDecoData.Add(prlDeco);
			}
		}
			


        GanttBar paintbar(pDC, CRect(left, top, right, bottom), prlBar->FrameType, 
			prlBar->MarkerType, prlBar->MarkerBrush, prlBar->Text, pomGanttChartFont, 
			prlBar->IsPause ? prlBar->PauseColour : lmGanttChartTextColor,ilPixel,
			FALSE, CRect(0,0,0,0), NULL, &olDecoData, TA_CENTER,NULL,0,llFrameColor);

		int ilNumDeco = olDecoData.GetSize();
		//Start from ilDecoCntBefMarker, since there are no region added.
		for(int ilDeco = ilDecoCntBefMarker ; ilDeco < ilNumDeco ; ilDeco++)
		{
			delete olDecoData[ilDeco].Region;			
		}

		olDecoData.DeleteAll();
    }
}

void StaffGantt::DrawTimeLines(CDC *pDC, int top, int bottom)
{
	int ilPixel = 1;
    // Draw current time

    if (omCurrentTime != TIMENULL && IsBetween(omCurrentTime, omDisplayStart, omDisplayEnd))
    {
        CPen penRed(PS_SOLID, 0, RGB(255, 0, 0));
        CPen *pOldPen = pDC->SelectObject(&penRed);
        int x = (int)GetX(omCurrentTime);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw marked time start
    if (omMarkTimeStart != TIMENULL && IsBetween(omMarkTimeStart, omDisplayStart, omDisplayEnd))
    {
        CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
        CPen *pOldPen = pDC->SelectObject(&penYellow);
        int x = (int)GetX(omMarkTimeStart);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw marked time end
    if (omMarkTimeEnd != TIMENULL && IsBetween(omMarkTimeEnd, omDisplayStart, omDisplayEnd))
    {
        CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
        CPen *pOldPen = pDC->SelectObject(&penYellow);
        int x = (int)GetX(omMarkTimeEnd);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }
}

void StaffGantt::DrawFocusRect(CDC *pDC, const CRect &rect)
{
	CRect rcFocus = rect;
	rcFocus.right++;	// extra pixel to help DrawFocusRect() work correctly
	pDC->DrawFocusRect(&rcFocus);

	// Before finish: please insert the correct code for status bar while moving/resizing
    if (pomStatusBar != NULL)
	{
		CString s = GetCTime(rcFocus.left).Format("%H%M") + " - "
			+ GetCTime(rcFocus.right - 1).Format("%H%M");
		pomStatusBar->SetPaneText(0, s);
	}
}


/////////////////////////////////////////////////////////////////////////////
// StaffGantt message handlers

BEGIN_MESSAGE_MAP(StaffGantt, CWnd)
	//{{AFX_MSG_MAP(StaffGantt)
	ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
	ON_WM_SETCURSOR()
    ON_WM_MOUSEMOVE()
    ON_WM_TIMER()
    ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
    ON_MESSAGE(WM_USERKEYDOWN, OnUserKeyDown)
    ON_MESSAGE(WM_USERKEYUP, OnUserKeyUp)
	ON_COMMAND(FIXJOB_MENUITEM, OnMenuStaffBarFix)
	ON_COMMAND(INFORMJOB_MENUITEM, OnMenuStaffInform)
	ON_COMMAND(FREEEMPSLIST_MENUITEM, OnMenuFreeEmployees)
	ON_COMMAND(EDITDLG_MENUITEM, OnMenuStaffEdit)
	ON_COMMAND(ALLOCDLG_MENUITEM, OnMenuStaffAssign)
	ON_COMMAND(PRMASSIGNPDA_MENUITEM, OnMenuPdaAssign) 
	ON_COMMAND(PRMREMOVEPDA_MENUITEM, OnMenuPdaRemove)
    ON_COMMAND(PAUSEDLG_MENUITEM, OnMenuStaffBreak)          
	ON_COMMAND(CONFIRMJOB_MENUITEM, OnMenuStaffConfirm)
	ON_COMMAND(ENDJOB_MENUITEM, OnMenuStaffBarEnd)
	ON_COMMAND(CHANGEJOB_MENUITEM, OnMenuChangeJob)
	ON_COMMAND(DELETEJOB_MENUITEM, OnMenuStaffBarDelete)
	ON_COMMAND(SPLITJOB_MENUITEM, OnMenuStaffSplitJob)
	ON_COMMAND(COPYJOB_MENUITEM, OnMenuStaffCopyJob)
	ON_COMMAND(45, OnMenuStaffBarBreak)
	ON_COMMAND(STANDBY_MENUITEM, OnMenuPoolJobStandby)
	ON_COMMAND(ABSENCE_MENUITEM, OnMenuPoolJobAbsence)
	ON_COMMAND(UNPAIDBREAK_MENUITEM, OnMenuBreakUnpayed)
	ON_COMMAND(LASTJOBINFO_MENUITEM, OnMenuLastJobInfo) //PRF 8998
	ON_COMMAND_RANGE(ACCEPT_ALL_CONFLICTS, (ACCEPT_ALL_CONFLICTS+MAXMENUITEM), OnMenuStaffBarAcceptConflict)
	ON_COMMAND(DEBUGINFO_MENUITEM, OnMenuDebugInfo)
	ON_COMMAND_RANGE(EDITFASTLINK_MENUITEM, (EDITFASTLINK_MENUITEM+MAXMENUITEM), OnMenuEditFastLink)
	ON_COMMAND_RANGE(DELETEFASTLINK_MENUITEM, (DELETEFASTLINK_MENUITEM+MAXMENUITEM), OnMenuDeleteFastLink)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_WM_VSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

int StaffGantt::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	EnableToolTips (TRUE);
    m_DragDropTarget.RegisterTarget(this, this);


	return 0;
}

void StaffGantt::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	m_DragDropTarget.Revoke();
	CWnd::OnDestroy();
}

void StaffGantt::OnSize(UINT nType, int cx, int cy)
{
    CListBox::OnSize(nType, cx, cy);

    CRect rect;
    GetWindowRect(&rect);   // don't use cx (vertical scroll may less the width of client rect)

    if (bmIsFixedScaling)   // fixed-scaling mode?
    {
        if (imWindowWidth == 0) // first time initialization?
            imWindowWidth = rect.Width();
        omDisplayEnd = GetCTime(rect.Width());   // has to recalculate the right most boundary?
    }
    else    // must be automatic variable-scaling mode
    {
        if (rect.Width() != imWindowWidth) // display window changed?
            InvalidateRect(NULL);   // repaint whole GanttChart
    }
    imWindowWidth = rect.Width();
}

// Attention:
// Be careful, this may not work if you try to develop horizontal-scrolling in GanttChart.
// Generally, if the ListBox need to be repainted, it will send the message WM_ERASEBKGND.
// But if you allow horizontal-scrolling, WM_ERASEBKGND will be called before the new
// horizontal position could be detected by dc.GetWindowOrg().x in DrawItem().
// However, this version work fine since there's no such scrolling.
// Then, we can assume that the beginning offset will be 0 all the time.
//
BOOL StaffGantt::OnEraseBkgnd(CDC* pDC)
{
    CRect rectErase;
    pDC->GetClipBox(&rectErase);

    // Draw the VerticalScale if the user specify it to be displayed
    if (imVerticalScaleWidth > 0)
    {
        CBrush brush(lmVerticalScaleBackgroundColor);
        CRect rect(0, rectErase.top, imVerticalScaleWidth, rectErase.bottom);
        pDC->FillRect(&rect, &brush);

        // Draw seperator line between VerticalScale and the body of GanttChart
        CPen penBlack(PS_SOLID, 0, ::GetSysColor(COLOR_WINDOWFRAME));
        CPen *pOldPen = pDC->SelectObject(&penBlack);
        pDC->MoveTo(imVerticalScaleWidth-2, rectErase.top);
        pDC->LineTo(imVerticalScaleWidth-2, rectErase.bottom);
        CPen penWhite(PS_SOLID, 0, ::GetSysColor(COLOR_BTNHIGHLIGHT));
        pDC->SelectObject(&penWhite);
        pDC->MoveTo(imVerticalScaleWidth-1, rectErase.top);
        pDC->LineTo(imVerticalScaleWidth-1, rectErase.bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw the background of the body of GanttChart
    CBrush brush(lmGanttChartBackgroundColor);
    CRect rect(imVerticalScaleWidth, rectErase.top, rectErase.right, rectErase.bottom);
    pDC->FillRect(&rect, &brush);

    // Draw the vertical current time and marked time lines
    DrawTimeLines(pDC, rectErase.top, rectErase.bottom);

    // Tell Windows there's nothing more to do
    return TRUE;
}

BOOL StaffGantt::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	// Id 2-Oct-96
	// This override cursor setting will remove cursor flickering when the user press
	// control key but still not begin the moving/resizing yet.
	//
    if (bmIsControlKeyDown && imHighlightLine != -1 && imCurrentBar != -1)
		return TRUE;	// override the default processing

	return CWnd::OnSetCursor(pWnd, nHitTest, message);
}

void StaffGantt::OnMouseMove(UINT nFlags, CPoint point)
{
    if (umResizeMode != HTNOWHERE)  // in SetCapture()?
    {
        OnMovingOrResizing(point, nFlags & MK_LBUTTON);
        return;
    }

    CListBox::OnMouseMove(nFlags, point);
    UpdateGanttChart(point, nFlags & MK_CONTROL);
}

void StaffGantt::OnTimer(UINT nIDEvent)
{
    if (umResizeMode != HTNOWHERE)  // in SetCapture()?
        return;

    if (nIDEvent == 0)  // the last timer message, clear everything
    {
		bmIsMouseInWindow = FALSE;
        RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = -1;       // no more selected line
        UpdateBarStatus(-1, -1, -1);    // no more status if mouse is outside of GanttChart
        return;
    }

    CPoint point;
    ::GetCursorPos(&point);
    ScreenToClient(&point);
    UpdateGanttChart(point, ::GetKeyState(VK_CONTROL) & 0x8080);
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.
}

void StaffGantt::OnLButtonDown(UINT nFlags, CPoint point)
{
    if(!(nFlags & MK_LBUTTON && nFlags & MK_SHIFT))
	{
		// shift + left mouse means mult-select, if not pressed then deselect all bars
		pomViewer->DeselectAllBars();
	}

	// This will help us create a virtual double click on a double click after SetCursorPos().
	CPoint olLastClickedPosition = omLastClickedPosition;
	omLastClickedPosition = point;
	if (olLastClickedPosition == point)
	{
		OnLButtonDblClk(nFlags, point);
		omLastClickedPosition = CPoint(-1, -1);
		return;
	}

    // Check if the user starts moving/resizing mouse action
    if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL) && BeginMovingOrResizing(point))
	{
        return; // not call the default after SetCapture()
	}

	// Check dragging from the vertical scale (dragging DutyBar)
	if((imHighlightLine = GetItemFromPoint(point)) != -1)
	{
		if(!(nFlags & MK_LBUTTON && nFlags & MK_SHIFT))
		{
			if(!EmployeeIsSelected(imGroupno, imHighlightLine))
			{
				DeselectEmployees();
			}
		}

		if (IsBetween(point.x, 0, imVerticalScaleWidth-2))	// from VerticalScale?
		{
		    if(nFlags & MK_LBUTTON && nFlags & MK_SHIFT)
			{
				SelectEmployee(imGroupno, imHighlightLine);
			}

			CRect olLineRect;
	        GetItemRect(imHighlightLine, &olLineRect);
			if(pomViewer->LineIsTopOfTeam(imGroupno, imHighlightLine) && IsBetween(point.x, 2, 13) && IsBetween(point.y, olLineRect.top+2, olLineRect.top+13))
			{
				omLastClickedPosition = CPoint(-1, -1);
				if(ogBasicData.IsPaxServiceT2() == false) //Singapore
				{
				pomViewer->ExpandOrCompressTeam(imGroupno, imHighlightLine);
				}
				return;
			}
			else
			{
				// if this emp has more than one pool job then dragging needs to
				// be initiated from the pool job itself and not from the vertical scale
				// No more time band
				TIMEPACKET olTimePacket;
				olTimePacket.StartTime = TIMENULL;
				olTimePacket.EndTime = TIMENULL;
				ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);

				STAFF_LINEDATA *prlActiveLine = pomViewer->GetLine(imGroupno, imHighlightLine);
				if(prlActiveLine != NULL)
				{
					long llPoolJobUrno = prlActiveLine->JobUrno;

					CCSPtrArray <JOBDATA> olPoolJobs;
					bool blTeamAlloc = false;
					if(pomViewer->bmTeamView && pomViewer->IsTeamCompressed(llPoolJobUrno))
					{
						// display all emps in the same team if the team is compressed
						pomViewer->GetPoolJobsInTeam(llPoolJobUrno, olPoolJobs, false);
						blTeamAlloc = true;
					}
					else
					{
						JOBDATA *prlPoolJob;
						int ilNumEmps = omSelectedEmployees.GetSize();
						if(ilNumEmps > 0)
						{
							// multi-select
							for(int ilPJ = 0; ilPJ < ilNumEmps; ilPJ++)
							{
								if((prlPoolJob = ogJobData.GetJobByUrno(omSelectedEmployees[ilPJ])) != NULL)
								{
									olPoolJobs.Add(prlPoolJob);
								}
							}
						}
						else
						{
							int ilNumBkBars = prlActiveLine->BkBars.GetSize();
							for(int ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
							{
								STAFF_BKBARDATA *prlBkBar = &prlActiveLine->BkBars[ilBkBar];
								if(!strcmp(prlBkBar->Type,JOBPOOL))
								{
									if((prlPoolJob = ogJobData.GetJobByUrno(prlBkBar->JobUrno)) != NULL)
									{
										olPoolJobs.Add(prlPoolJob);
									}
								}
							}
						}
					}

					if(olPoolJobs.GetSize() > 0)
					{
						DragPoolJobBegin(olPoolJobs,blTeamAlloc);
					}
				}

				return;	// not call the default after dragging;
			}
		}
	}

    // Bring bar to front / Send bar to back
    imCurrentBar = GetBarnoFromPoint(imHighlightLine, point);
    if (imCurrentBar == -1)
	{
		int ilBkBarno = GetPoolJobFromPoint(imHighlightLine, point);
		if(ilBkBarno != -1)	// Dragging from pool job
		{
			long llPoolJobUrno = pomViewer->GetBkBar(imGroupno, imHighlightLine, ilBkBarno)->JobUrno;
			if(pomViewer->IsTeamCompressed(llPoolJobUrno))
			{
				CCSPtrArray <JOBDATA> olTeamPoolJobs;
				pomViewer->GetPoolJobsInTeam(llPoolJobUrno, olTeamPoolJobs);
				DragPoolJobBegin(olTeamPoolJobs,true);
			}
			else
			{
				DragPoolJobBegin(llPoolJobUrno);
			}
			return;	// not call the default after dragging;
		}
	}
	else
	{
		STAFF_BARDATA *prlBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
		int ilOldOverlapLevel = prlBar->OverlapLevel;

		imCurrentBar = SelectBar(imGroupno, imHighlightLine, imCurrentBar);

		// Id 22-Sep-96
		// To make drag-and-drop operation works properly, we have to change the position
		// of the mouse cursor also, since the drag-and-drop will use the current position
		// as a basis for selecting a bar.
		prlBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
			// reload the bar again (it may be already moved to some other position)
		int ilNewOverlapLevel = prlBar->OverlapLevel;
		point.Offset(0, (ilNewOverlapLevel - ilOldOverlapLevel) * imOverlapHeight);
    }

	// Update the cursor position to make sure that the cursor will always be over
	// the bar we just click on.
	CPoint olMousePosition = point;
	ClientToScreen(&olMousePosition);
	::SetCursorPos(olMousePosition.x, olMousePosition.y);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	// Id 1-Oct-96
	// This will help us create a virtual double click on a double click after SetCursorPos().
	omLastClickedPosition = point;

	// Checking for dragging a job bar?
	// Notes: Here's the best place for checking for dragging from job bar.
	// We cannot simply regard OnLButtonDown() as the beginning of dragging,
	// since we defined OnLButtonDown() for bringing bar to front or to back.
	//
	// Id 25-Sep-96
	// All of the above are wrong. Try to move it back and you will see.
	// If we place this in OnMouseMove(), we can initiate the drag operation by
	// holding on the left mouse from some place which does not allow dragging,
	// then move the mouse over some bars, the drag operation will start when the
	// cursor is on some bar. This is weird and should not happened.
	if ((nFlags & MK_LBUTTON) && imHighlightLine != -1 && imCurrentBar != -1)
	{
		DragJobBarBegin(imHighlightLine, imCurrentBar);
		return;
	}

	// Id 2-Oct-96
	// Fix our secret 4 mouse click which crashes our program..
	// I found that if we pass OnLButtonDown() to CListBox and initiate dragging
	// (which is in DragJobBarBegin). The window behavior will be very strange
	// when we click on the scroll bar, and it is the reason of our crash.
	// I have found out that we are in luck that this kind of bug can be fixed very
	// easily (but it very difficult to realize that what happened).
	// However, I document this since may be we can use it to remind this kind of
	// symptom again if we find it the next time.
    CListBox::OnLButtonDown(nFlags, point);
}

int StaffGantt::SelectBar(int ipGroupno, int ipLineno, int ipBarno)
{

	
	
	STAFF_BARDATA *prlBar = pomViewer->GetBar(ipGroupno, ipLineno, ipBarno);
	TIMEPACKET olTimePacket;
	olTimePacket.StartTime = prlBar->StartTime;
	olTimePacket.EndTime = prlBar->EndTime;
	ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);

	// Bring bar to front / Send bar to back
	int ilOldLineHeight = GetLineHeight(ipLineno);
	STAFF_BARDATA rlBar = *prlBar;	// copy all data of this bar
	rlBar.Indicators.RemoveAll();	// remove indicators, will be dealloc in DeleteBar()
	for (int i = 0; i < prlBar->Indicators.GetSize(); i++)
		rlBar.Indicators.NewAt(i, prlBar->Indicators[i]);
	pomViewer->DeleteBar(ipGroupno, ipLineno, ipBarno);
	//BOOL blBringBarToFront = !(nFlags & MK_SHIFT);
	pomViewer->SelectBar(&rlBar, !rlBar.Selected);
    ipBarno = pomViewer->CreateBar(ipGroupno, ipLineno, &rlBar, TRUE);
		// unnecessary to delete the NewAt() bars since create bar automatic copy it
	// Update the display, also check if line height was changed
	if (GetLineHeight(ipLineno) == ilOldLineHeight)
	{
		RepaintGanttChart(ipLineno, TIMENULL, TIMENULL, TRUE);
	}
	else
	{
		//RepaintItemHeight(ipLineno);
		LONG lParam = MAKELONG(ipLineno, ipGroupno);
		pomViewer->pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINEHEIGHT, lParam);
	}

	return ipBarno;
}

void StaffGantt::OnLButtonUp(UINT nFlags, CPoint point) 
{
    if (umResizeMode != HTNOWHERE)  // in SetCapture()?
    {
		// This allow a fast way for terminating moving or resizing mode
        OnMovingOrResizing(point, nFlags & MK_LBUTTON);
        return;
    }

	CListBox::OnLButtonUp(nFlags, point);
}

void StaffGantt::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// Pop-up detailed window
	if (imHighlightLine != -1)
	{
		CRect olLineRect;
	    GetItemRect(imHighlightLine, &olLineRect);
		if(pomViewer->LineIsTopOfTeam(imGroupno, imHighlightLine) && IsBetween(point.x, 2, 13) && IsBetween(point.y, olLineRect.top+2, olLineRect.top+13))
		{
			OnLButtonDown(nFlags, point);
			return;
		}

		if(imCurrentBar != -1 && point.x >= imVerticalScaleWidth)	// bar?
		{
			JOBDATA *prlJob;
		
			
			STAFF_BARDATA *prlActiveBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
		
			
		
			 
			
			if ( prlActiveBar && ((prlJob = ogJobData.GetJobByUrno(prlActiveBar->JobUrno)) != NULL))
			{
				if (strcmp(prlJob->Jtco, JOBFLIGHT) == 0 || strcmp(prlJob->Jtco, JOBRESTRICTEDFLIGHT) == 0)
				{
					long llArrUrno, llDepUrno;
					ogBasicData.GetFlightUrnoForJob(prlJob, &llArrUrno, &llDepUrno);
					new FlightDetailWindow(this, llArrUrno, llDepUrno, FALSE, prlJob->Aloc, NULL, PERSONNELDEMANDS, prlJob->Acfr);

					// Make the time band lines appear in the StaffDetailWindow also
					TIMEPACKET olTimePacket;
					olTimePacket.StartTime = omMarkTimeStart;
					olTimePacket.EndTime = omMarkTimeEnd;
					ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);

					return;
				}
				else if (strcmp(prlJob->Jtco,JOBCCC) == 0)
				{
					DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prlJob);
					if(prlDemand && prlDemand->Dety[0] == CCI_DEMAND)
					{
						new CciDemandDetailDlg(this,prlDemand->Urno);
					}
					else
					{
						DJobChange olJobChangeDialog(this,prlActiveBar->Text,prlJob->Acfr,prlJob->Acto);
						if(olJobChangeDialog.DoModal() == IDOK)
						{
							JOBDATA rlOldJob = *prlJob;
							prlJob->Acfr = olJobChangeDialog.m_Acfr;
							prlJob->Acto = olJobChangeDialog.m_Acto;
							ogJobData.ChangeJobData(prlJob, &rlOldJob, "CicEdt");
						}
					}
					return;
				}
			}
		}
		//  kein Doppelklick auf Flugbezogenen job
		STAFF_LINEDATA *prlLine = pomViewer->GetLine(imGroupno, imHighlightLine);
		
	
		new StaffDetailWindow(this,prlLine->JobUrno);

		// Make the time band lines appear in the StaffDetailWindow also
		TIMEPACKET olTimePacket;
		olTimePacket.StartTime = omMarkTimeStart;
		olTimePacket.EndTime = omMarkTimeEnd;
		ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);
	}
}

void StaffGantt::OnRButtonDown(UINT nFlags, CPoint point) 
{
    if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL) &&
        BeginMovingOrResizing(point))
        return; // not call the default after SetCapture()

	int ilBkBarno;
	if((imHighlightLine = GetItemFromPoint(point)) == -1)
		return;		// not click in the Gantt window

	lmActiveUrno  = -1;
	bmActiveBar   = FALSE;
	bmActiveBkBar = FALSE;
	DeselectEmployees();

	STAFF_LINEDATA *prlActiveLine = pomViewer->GetLine(imGroupno, imHighlightLine);
	if(prlActiveLine != NULL)
	{
		lmActiveUrno = prlActiveLine->JobUrno;

		if(point.x < imVerticalScaleWidth - 2) // on VerticalScale
		{
			pomViewer->DeselectAllBars();

			CMenu olMenu;
			olMenu.CreatePopupMenu();
			for (int i = olMenu.GetMenuItemCount(); i > 0; i--)
				 olMenu.RemoveMenu(i, MF_BYPOSITION);
			JOBDATA *prlJob = GetJob(lmActiveUrno);

			if(prlJob != NULL)
			{
				if(*prlJob->Stat == 'P')
				{
					olMenu.AppendMenu(MF_STRING|MF_UNCHECKED,CONFIRMJOB_MENUITEM, GetString(IDS_STRING61353)); // confirm
					olMenu.AppendMenu(MF_STRING|MF_UNCHECKED|MF_GRAYED,ENDJOB_MENUITEM, GetString(IDS_STRING61354)); // end
				}
				else if(*prlJob->Stat == 'C')
				{
					olMenu.AppendMenu(MF_STRING|MF_CHECKED,CONFIRMJOB_MENUITEM, GetString(IDS_STRING61353)); // confirm
					olMenu.AppendMenu(MF_STRING|MF_UNCHECKED,ENDJOB_MENUITEM, GetString(IDS_STRING61354)); // end
				}
				else if(*prlJob->Stat == 'F')
				{
					olMenu.AppendMenu(MF_STRING|MF_UNCHECKED|MF_GRAYED,CONFIRMJOB_MENUITEM, GetString(IDS_STRING61353)); // confirm
					olMenu.AppendMenu(MF_STRING|MF_CHECKED,ENDJOB_MENUITEM, GetString(IDS_STRING61354)); // end
				}
			}
			olMenu.AppendMenu(MF_STRING,EDITDLG_MENUITEM, GetString(IDS_STRING61352)); // Bearbeiten
			olMenu.AppendMenu(MF_STRING,ALLOCDLG_MENUITEM, GetString(IDS_STRING61388)); // Einteilen Dlg
			if (bgIsPrm)
			{
				olMenu.AppendMenu(MF_STRING,PRMASSIGNPDA_MENUITEM, GetString(IDS_STRING2009)); // Assign PDA
				olMenu.AppendMenu(MF_STRING,PRMREMOVEPDA_MENUITEM, GetString(IDS_STRING2010)); // Remove PDA
			}
			olMenu.AppendMenu(MF_STRING,PAUSEDLG_MENUITEM, GetString(IDS_STRING61385)); // Pause

			if(prlJob != NULL)
			{
				if(*prlJob->Ignr == '1')
				{
					olMenu.AppendMenu(MF_STRING|MF_CHECKED,STANDBY_MENUITEM, GetString(IDS_STRING61656));// Ignore pooljob during allocation
				}
				else
				{
					olMenu.AppendMenu(MF_STRING|MF_UNCHECKED,STANDBY_MENUITEM, GetString(IDS_STRING61656));// Don't ignore pooljob during allocation
				}

				if(prlJob->Infm)
				{
					olMenu.AppendMenu(MF_STRING|MF_CHECKED,ABSENCE_MENUITEM, GetString(IDS_EMPABSENCE));// Employee is absent
				}
				else
				{
					olMenu.AppendMenu(MF_STRING|MF_UNCHECKED,ABSENCE_MENUITEM, GetString(IDS_EMPABSENCE));// Employee is present
				}
				if(*prlJob->Stat == 'F')
				{
					olMenu.EnableMenuItem(ABSENCE_MENUITEM,MF_BYCOMMAND || MF_GRAYED);
				}
			}

//			if(pomViewer->PoolJobCount(imGroupno, imHighlightLine) > 1)
//			{
//				// if emp has > 1 pool job then alloc dlg is disabled
//				olMenu.EnableMenuItem(ALLOCDLG_MENUITEM,MF_BYCOMMAND || MF_GRAYED);
//			}


			if(ogSprData.bmNoChangesAllowedAfterClockout && prlJob != NULL)
			{
				CCSPtrArray <JOBDATA> olPoolJobs;
				if(pomViewer->GetPoolJobsForCompressedPoolJob(lmActiveUrno, olPoolJobs))
				{
					if(ogSprData.ClockedOut(olPoolJobs))
						ogBasicData.DisableAllMenuItems(olMenu);
				}
				else
				{
					if(ogSprData.ClockedOut(prlJob->Shur))
						ogBasicData.DisableAllMenuItems(olMenu);
				}
			}

			if(ogBasicData.DisplayDebugInfo())
			{
				olMenu.AppendMenu(MF_STRING,DEBUGINFO_MENUITEM, "Debug Info");
			}

			ClientToScreen(&point);
			olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
			return;
		}
		else if ((imCurrentBar = GetBarnoFromPoint(imHighlightLine, point)) != -1) // on a bar
		{
			JOBDATA *prlJob;
			STAFF_BARDATA *prlActiveBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
			if(prlActiveBar != NULL && (prlJob = GetJob(prlActiveBar->JobUrno)) != NULL)
			{
				CCSPtrArray <JOBDATA> olSelJobs;
				int ilNumJobs = pomViewer->GetSelectedJobs(olSelJobs);

				bool blCurrBarSelected = false;
				for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
				{
					if(olSelJobs[ilJob].Urno == prlJob->Urno)
					{
						blCurrBarSelected = true;
						break;
					}
				}
				if(!blCurrBarSelected)
				{
					pomViewer->DeselectAllBars();
					imCurrentBar = SelectBar(imGroupno, imHighlightLine, imCurrentBar);
					olSelJobs.RemoveAll();
					ilNumJobs = pomViewer->GetSelectedJobs(olSelJobs);
					prlActiveBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
				}

				UINT ulPosConfirm = 0;
				UINT ulPosEnded	  = 1;

				rmActiveBar = *prlActiveBar;
				bmActiveBar = TRUE;

				CMenu olMenu;
				olMenu.CreatePopupMenu();
				for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
					olMenu.RemoveMenu(i, MF_BYPOSITION);

				bool blDFJ = (!strcmp(prlJob->Jtco,JOBDELEGATEDFLIGHT)) ? true : false;
				bool blRFJ = (!strcmp(prlJob->Jtco,JOBRESTRICTEDFLIGHT)) ? true : false;
				if(ilNumJobs > 1)
				{
					bool blHasPlannedJobs = false, blHasStartedJobs = false, blAllEmpsInformed = true;
					for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
					{
						JOBDATA *prlJob = &olSelJobs[ilJ];

						CCSPtrArray <JOBDATA> olCompressedJobs;
						if(pomViewer->GetJobsForCompressedJob(prlJob->Urno, olCompressedJobs))
						{
							int ilNumCompressedJobs = olCompressedJobs.GetSize();
							for(int ilCJ = 0; ilCJ < ilNumCompressedJobs; ilCJ++)
							{
								JOBDATA *prlCJ = &olCompressedJobs[ilCJ];
								if(*prlCJ->Stat == 'P')
								{
									blHasPlannedJobs = true;
								}
								else if(*prlCJ->Stat == 'C')
								{
									blHasStartedJobs = true;
								}
								if(!prlCJ->Infm)
								{
									blAllEmpsInformed = false;
								}

								if (strcmp(prlCJ->Jtco,JOBDELEGATEDFLIGHT) == 0)
									blDFJ = true;
								else if (strcmp(prlCJ->Jtco,JOBRESTRICTEDFLIGHT) == 0)
									blRFJ = true;
							}
						}
						else
						{
							if(*prlJob->Stat == 'P')
							{
								blHasPlannedJobs = true;
							}
							else if(*prlJob->Stat == 'C')
							{
								blHasStartedJobs = true;
							}
							if(!prlJob->Infm)
							{
								blAllEmpsInformed = false;
							}

							if (strcmp(prlJob->Jtco,JOBDELEGATEDFLIGHT) == 0)
								blDFJ = true;
							else if (strcmp(prlJob->Jtco,JOBRESTRICTEDFLIGHT) == 0)
								blRFJ = true;
						}
					}
					olMenu.AppendMenu(MF_STRING,INFORMJOB_MENUITEM, GetString(IDS_INFORMJOB));
					olMenu.AppendMenu(MF_STRING,CONFIRMJOB_MENUITEM, GetString(IDS_STRING61353));
					olMenu.AppendMenu(MF_STRING,ENDJOB_MENUITEM, GetString(IDS_STRING61354));

					if(!blHasPlannedJobs)
					{
						olMenu.EnableMenuItem(INFORMJOB_MENUITEM,MF_BYCOMMAND || MF_GRAYED);
						olMenu.EnableMenuItem(CONFIRMJOB_MENUITEM,MF_BYCOMMAND || MF_GRAYED);
					}
					else if(blAllEmpsInformed)
					{
						olMenu.CheckMenuItem(INFORMJOB_MENUITEM, MF_CHECKED);
					}

					if(!blHasStartedJobs)
					{
						olMenu.EnableMenuItem(ENDJOB_MENUITEM,MF_BYCOMMAND || MF_GRAYED);
					}

					if(blHasPlannedJobs)
					{
						ulPosConfirm++;
						ulPosEnded++;
					}

					JobTimeHandler hdl(olSelJobs,&olMenu,ulPosConfirm,ulPosEnded);
					ClientToScreen(&point);
					hdl.TrackPopupMenu(point,this);

					if(ogSprData.bmNoChangesAllowedAfterClockout && ogSprData.ClockedOut(olSelJobs))
						ogBasicData.DisableAllMenuItems(olMenu);

					//ClientToScreen(&point);
					//olMenu.TrackPopupMenu(TPM_LEFTBUTTON|TPM_RIGHTBUTTON,point.x,point.y,this);
					return;
				}


				olMenu.AppendMenu(MF_STRING,CONFIRMJOB_MENUITEM, GetString(IDS_STRING61353));// Best�tigung
				olMenu.AppendMenu(MF_STRING,ENDJOB_MENUITEM, GetString(IDS_STRING61354));// Beenden
				olMenu.AppendMenu(MF_STRING,CHANGEJOB_MENUITEM, GetString(IDS_STRING61360));// �ndern
				olMenu.AppendMenu(MF_STRING,EDITDLG_MENUITEM, GetString(IDS_STRING61352));// Bearbeiten
				olMenu.AppendMenu(MF_STRING,DELETEJOB_MENUITEM, GetString(IDS_STRING61210));// L�schen
				olMenu.AppendMenu(MF_STRING,SPLITJOB_MENUITEM, GetString(IDS_EG_SPLITJOB));// Split
				olMenu.AppendMenu(MF_STRING,COPYJOB_MENUITEM, GetString(IDS_EG_COPYJOB));// Copy

				if(!strcmp(prlJob->Jtco, JOBBREAK))
				{
					// pause
					olMenu.AppendMenu(MF_STRING|((*prlJob->Ignr == '1') ? MF_CHECKED : MF_UNCHECKED),UNPAIDBREAK_MENUITEM, GetString(IDS_STRING61206));  // Coffee Break

					if(*prlJob->Stat == 'P')
					{
						// emp has been informed about the job but it hasn't yet started
						if(prlJob->Infm)
						{
							olMenu.InsertMenu(0,MF_BYPOSITION|MF_STRING|MF_CHECKED,INFORMJOB_MENUITEM, GetString(IDS_INFORMJOB)); 
						}
						else
						{
							olMenu.InsertMenu(0,MF_BYPOSITION|MF_STRING|MF_UNCHECKED,INFORMJOB_MENUITEM, GetString(IDS_INFORMJOB));
						}

						++ulPosConfirm;
						++ulPosEnded;
					}
				}
				else
				{
					// normal job
					if(*prlJob->Ignr == '1')
					{
						olMenu.AppendMenu(MF_STRING|MF_CHECKED,FIXJOB_MENUITEM, GetString(IDS_FIXMENUITEM));// Don't Ignore job during automatic allocation
					}
					else
					{
						olMenu.AppendMenu(MF_STRING|MF_UNCHECKED,FIXJOB_MENUITEM, GetString(IDS_FIXMENUITEM));// Ignore job during automatic allocation
					}

					if (strcmp(prlJob->Jtco,JOBTEAMDELEGATION) == 0 || strcmp(prlJob->Jtco,JOBDETACH) == 0)
					{
						olMenu.EnableMenuItem(FIXJOB_MENUITEM,MF_BYCOMMAND || MF_GRAYED);
					}

					if(*prlJob->Stat == 'P')
					{
						// emp has been informed about the job but it hasn't yet started
						if(prlJob->Infm)
						{
							olMenu.InsertMenu(0,MF_BYPOSITION|MF_STRING|MF_CHECKED,INFORMJOB_MENUITEM, GetString(IDS_INFORMJOB)); 
						}
						else
						{
							olMenu.InsertMenu(0,MF_BYPOSITION|MF_STRING|MF_UNCHECKED,INFORMJOB_MENUITEM, GetString(IDS_INFORMJOB));
						}

						++ulPosConfirm;
						++ulPosEnded;
					}
					olMenu.AppendMenu(MF_STRING,PAUSEDLG_MENUITEM, GetString(IDS_STRING61385));// Pause
					olMenu.AppendMenu(MF_STRING,FREEEMPSLIST_MENUITEM, GetString(IDS_STRING61395));// Freie Mitarbeiter

					if(!strcmp(prlJob->Jtco,JOBTEAMDELEGATION) || !strcmp(prlJob->Jtco,JOBDETACH))
					{
						olMenu.EnableMenuItem(FREEEMPSLIST_MENUITEM,MF_BYCOMMAND || MF_GRAYED);
						olMenu.EnableMenuItem(COPYJOB_MENUITEM, MF_BYCOMMAND || MF_GRAYED);
						olMenu.EnableMenuItem(SPLITJOB_MENUITEM, MF_BYCOMMAND || MF_GRAYED);
					}
					else if(pomViewer->IsTeamCompressed(prlJob->Urno))
					{
						olMenu.EnableMenuItem(COPYJOB_MENUITEM, MF_BYCOMMAND || MF_GRAYED);
						olMenu.EnableMenuItem(LASTJOBINFO_MENUITEM, MF_BYCOMMAND || MF_GRAYED); //PRF 8998
					}
				}

				if(blDFJ)
				{
					ogBasicData.DisableAllMenuItems(olMenu);
					if(ogBasicData.IsSinApron1() == true || ogBasicData.IsSinApron2() == true)
					{
						olMenu.EnableMenuItem(INFORMJOB_MENUITEM, MF_BYCOMMAND||MF_ENABLED);
						olMenu.EnableMenuItem(DELETEJOB_MENUITEM, MF_BYCOMMAND||MF_ENABLED);
						olMenu.EnableMenuItem(CONFIRMJOB_MENUITEM, MF_BYCOMMAND||MF_ENABLED);
						olMenu.EnableMenuItem(ENDJOB_MENUITEM, MF_BYCOMMAND||MF_ENABLED);
					}
					else
					{
						olMenu.EnableMenuItem(DELETEJOB_MENUITEM, MF_BYCOMMAND||MF_ENABLED);
					}
				}
				else if(blRFJ)
				{
					ogBasicData.DisableAllMenuItems(olMenu);
					if(ogBasicData.IsSinApron1() == true || ogBasicData.IsSinApron2() == true)
					{
						olMenu.EnableMenuItem(INFORMJOB_MENUITEM, MF_BYCOMMAND||MF_ENABLED);
					}
					olMenu.EnableMenuItem(DELETEJOB_MENUITEM, MF_BYCOMMAND||MF_ENABLED);
					olMenu.EnableMenuItem(CONFIRMJOB_MENUITEM, MF_BYCOMMAND||MF_ENABLED);
					olMenu.EnableMenuItem(ENDJOB_MENUITEM, MF_BYCOMMAND||MF_ENABLED);
				}



				// display a list of conflicts
				CString olText;
				CCSPtrArray <CONFLICTDATA> olConflicts;

				if(pomViewer->IsTeamCompressed(prlJob->Urno))
				{

					// get all conflicts for the jobs that make up this compressed job
					TEAM_DATA *prlTeam = pomViewer->GetTeamByJobUrno(prlJob->Urno);
					if(prlTeam != NULL)
					{
						COMPRESSED_JOBDATA *prlCompressedJob = pomViewer->FindCompressedJob(prlTeam,prlJob->Urno);
						if(prlCompressedJob != NULL)
						{
							int ilConflNum = ACCEPT_ALL_CONFLICTS;
							int ilNumUnacceptedConflicts = 0;

							omAcceptConflictsMap.RemoveAll();
							omConflictToJobMap.RemoveAll();
							int ilNumJobs = prlCompressedJob->OriginalJobs.GetSize();
							for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
							{
								JOBDATA *prlJob = &prlCompressedJob->OriginalJobs[ilJob];

								ogConflicts.GetJobConflicts(olConflicts,prlJob->Urno);
								int ilNumConflicts = olConflicts.GetSize();

								// display employee name for compressed job conflicts
								EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlJob->Ustf);
								CString olName;
								if(prlEmp == NULL)
								{
									olName.Format("Name not found (Invalid JOB,USTF <%d> for JOB.URNO <%d>",prlJob->Ustf,prlJob->Urno);
								}
								else
								{
									olName.Format("%s   %s - %s (%s)",ogEmpData.GetEmpName(prlEmp),prlJob->Acfr.Format("%H%M/%d"),prlJob->Acto.Format("%H%M/%d"),ogJobData.GetJobStatusString(prlJob));
								}
								olMenu.AppendMenu(MF_SEPARATOR);
								olMenu.AppendMenu(MF_STRING|MF_DISABLED, 0, olName);

								if(ilNumConflicts > 0)
								{
									for(int ilConfl = 0; ilConfl < ilNumConflicts; ilConfl++)
									{
										ilConflNum++;
										//olMenu.AppendMenu(MF_STRING,25, "Konflikt akzeptieren");	// accept conflict
										olText.Format("%s %s",GetString(IDS_STRING61357),ogConflicts.GetConflictTextByType(olConflicts[ilConfl].Type));
										olText += CString(" ") + ogConflicts.GetAdditionalConflictText(olConflicts[ilConfl].Type,ogBasicData.GetFlightForJob(prlJob),prlJob);
										if(olConflicts[ilConfl].Confirmed)
										{
											olMenu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
										}
										else
										{
											olMenu.AppendMenu(MF_STRING, ilConflNum, olText);
											ilNumUnacceptedConflicts++;
										}
										omAcceptConflictsMap.SetAt((void *) ilConflNum, &olConflicts[ilConfl]);
										omConflictToJobMap.SetAt((void *) ilConflNum, prlJob);
									}
									olConflicts.RemoveAll();
								}
							}
							if(ilConflNum > ACCEPT_ALL_CONFLICTS)
							{
								olMenu.AppendMenu(MF_SEPARATOR);
							}
							if(ilNumUnacceptedConflicts > 1)
							{
								olMenu.AppendMenu(MF_STRING, ACCEPT_ALL_CONFLICTS, GetString(IDS_STRING61378));
							}
						}
					}
				}
				else
				{
					ogConflicts.GetJobConflicts(olConflicts,prlJob->Urno);
					int ilNumConflicts = olConflicts.GetSize();
					if(ilNumConflicts > 0)
					{
						int ilConflNum;
						int ilNumUnacceptedConflicts = 0;
						omAcceptConflictsMap.RemoveAll();
						omConflictToJobMap.RemoveAll();
						for(int ilConfl = 0; ilConfl < ilNumConflicts; ilConfl++)
						{
							//olMenu.AppendMenu(MF_STRING,25, "Konflikt akzeptieren");	// accept conflict
							ilConflNum = 16 + ilConfl;
							olText.Format("%s: %s",GetString(IDS_STRING61357),ogConflicts.GetConflictTextByType(olConflicts[ilConfl].Type));
							olText += CString(" ") + ogConflicts.GetAdditionalConflictText(olConflicts[ilConfl].Type,ogBasicData.GetFlightForJob(prlJob),prlJob);
							if(olConflicts[ilConfl].Confirmed)
							{
								olMenu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
							}
							else
							{
								olMenu.AppendMenu(MF_STRING, ilConflNum, olText);
								ilNumUnacceptedConflicts++;
							}
							omAcceptConflictsMap.SetAt((void *) ilConflNum, &olConflicts[ilConfl]);
							omConflictToJobMap.SetAt((void *) ilConflNum, prlJob);
						}
						olConflicts.RemoveAll();
						if(ilNumUnacceptedConflicts > 1)
						{
							olMenu.AppendMenu(MF_STRING, ACCEPT_ALL_CONFLICTS, GetString(IDS_STRING61378));
						}
					}
				}


				// disable some olMenu entries which are not needed
				if (*prlJob->Stat != 'P')
				{
					olMenu.EnableMenuItem(CONFIRMJOB_MENUITEM,MF_BYCOMMAND || MF_GRAYED);
				}
				if ((*prlJob->Stat != 'C') || (prlJob->Acfr > ogBasicData.GetTime()))
				{
					olMenu.EnableMenuItem(ENDJOB_MENUITEM,MF_BYCOMMAND || MF_GRAYED);
				}
				if (*prlJob->Stat == 'F')
				{
					olMenu.EnableMenuItem(DELETEJOB_MENUITEM,MF_BYCOMMAND || MF_GRAYED);
				}

				CCSPtrArray <JOBDATA> olJobs;
				if(!pomViewer->GetJobsForCompressedJob(prlJob->Urno, olJobs))
				{
					olJobs.Add(prlJob);
				}
				JobTimeHandler hdl(olJobs,&olMenu,ulPosConfirm,ulPosEnded);

				if(ogSprData.bmNoChangesAllowedAfterClockout && ogSprData.ClockedOut(olJobs))
					ogBasicData.DisableAllMenuItems(olMenu);

				if(ogBasicData.DisplayDebugInfo())
				{
					olMenu.AppendMenu(MF_STRING,DEBUGINFO_MENUITEM, "Debug Info");
				}
				
				olMenu.AppendMenu(MF_STRING,LASTJOBINFO_MENUITEM, GetString(IDS_LAST_JOB_INFO));//PRF 8998
				if(pomViewer->IsTeamCompressed(prlJob->Urno)) //PRF 8998
				{
					olMenu.EnableMenuItem(LASTJOBINFO_MENUITEM, MF_BYCOMMAND || MF_GRAYED); //PRF 8998
				}

				ClientToScreen(&point);
				hdl.TrackPopupMenu(point,this);
			}
			return;
		}
		else if(((ilBkBarno = GetBkBarnoFromPoint(imHighlightLine, point)) != -1))
		{
			// on a background bar (pool job)
			STAFF_BKBARDATA *prlActiveBkBar = pomViewer->GetBkBar(imGroupno, imHighlightLine, ilBkBarno);
			if(prlActiveBkBar != NULL)
			{
				rmActiveBkBar = *prlActiveBkBar;
				bmActiveBkBar = TRUE;

				CMenu olMenu;
				olMenu.CreatePopupMenu();
				for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
					olMenu.RemoveMenu(i, MF_BYPOSITION);

				// allow edit/delete of equipment fast link assignments
				if(ogBasicData.bmDisplayEquipment)
				{
					omMenuItemToEquFastLinkMap.RemoveAll();

					CString olText;
					int ilMenuItem;

					CCSPtrArray <JOBDATA> olEquipmentFastLinkJobs;
					ogJobData.GetEquipmentFastLinkJobsByPoolJob(olEquipmentFastLinkJobs,prlActiveBkBar->JobUrno);
					int ilNumJobs = olEquipmentFastLinkJobs.GetSize();
					for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
					{
						JOBDATA *prlEFLJob = &olEquipmentFastLinkJobs[ilJ];
						EQUDATA *prlEqu = ogEquData.GetEquByUrno(prlEFLJob->Uequ);
						if(prlEqu != NULL)
						{
							ilMenuItem = EDITFASTLINK_MENUITEM + ilJ;
							if(ilMenuItem < (EDITFASTLINK_MENUITEM + MAXMENUITEM))
							{
								olText.Format("%s%s",GetString(IDS_EDITFASTLINK),prlEqu->Enam);
								olMenu.AppendMenu(MF_STRING, ilMenuItem, olText);
								omMenuItemToEquFastLinkMap.SetAt((void *) ilMenuItem, prlEFLJob);
							}
							ilMenuItem = DELETEFASTLINK_MENUITEM + ilJ;
							if(ilMenuItem < (DELETEFASTLINK_MENUITEM + MAXMENUITEM))
							{
								olText.Format("%s%s",GetString(IDS_DELETEFASTLINK),prlEqu->Enam);
								olMenu.AppendMenu(MF_STRING, ilMenuItem, olText);
								omMenuItemToEquFastLinkMap.SetAt((void *) ilMenuItem, prlEFLJob);
							}
						}
					}
				}

				JOBDATA *prlTmpJob = ogJobData.GetJobByUrno(prlActiveBkBar->JobUrno);
				if(prlTmpJob != NULL)
				{
					if(ogSprData.bmNoChangesAllowedAfterClockout && ogSprData.ClockedOut(prlTmpJob))
						ogBasicData.DisableAllMenuItems(olMenu);
				}
				if(ogBasicData.DisplayDebugInfo())
				{
					olMenu.AppendMenu(MF_STRING,DEBUGINFO_MENUITEM, "Debug Info");
				}

				if(olMenu.GetMenuItemCount() > 0)
				{
					ClientToScreen(&point);
					olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
				}
			}
			return;
		}
	}
	
	CWnd::OnRButtonDown(nFlags, point);
}

void StaffGantt::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

void StaffGantt::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

LONG StaffGantt::OnUserKeyDown(UINT wParam, LONG lParam)
{
	imTopIndex = GetTopIndex();

	switch (wParam) {
	case VK_UP:
		imTopIndex--;
		imTopIndex = max(0,imTopIndex);
		CListBox::SetTopIndex(imTopIndex);
		break;
	case VK_DOWN:
		imTopIndex++;
		imTopIndex = min(imTopIndex,GetCount()-1);
		CListBox::SetTopIndex(imTopIndex);
		break;
	}

	return 0L;
}

LONG StaffGantt::OnUserKeyUp(UINT wParam, LONG lParam)
{
	return 0L;
}

/////////////////////////////////////////////////////////////////////////////
// StaffGantt context menus helper functions

void StaffGantt::OnMenuFreeEmployees()
{
	if(bmActiveBar)
	{
		JOBDATA *prlJob = ogJobData.GetJobByUrno(rmActiveBar.JobUrno);
		if(prlJob != NULL)
		{
			DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prlJob);
			if(prlDemand != NULL)
			{
				CString olAloc = prlDemand->Aloc;
				CString olAlid = prlDemand->Alid;

				CAvailableEmpsDlg olAvailableEmpsDlg;
				olAvailableEmpsDlg.SetAllocUnit(olAloc, olAlid);

				CString olCaption;
				olCaption.Format(GetString(IDS_REASSIGNJOB),prlJob->Acfr.Format("%H%M"),prlJob->Acto.Format("%H%M"));
				olAvailableEmpsDlg.SetCaptionObject(olCaption);

				int ilNumEmps = olAvailableEmpsDlg.GetAvailableEmployeesForDemand(prlDemand, true);
				olAvailableEmpsDlg.AddDemand(prlDemand);

				if(ilNumEmps <= 0)
				{
					// no employees available for the demands
					MessageBox(GetString(IDS_STRING61266), GetString(IDS_AVAILEMPSCAPTION), MB_ICONEXCLAMATION);
				}
				else
				{
					if(olAvailableEmpsDlg.DoModal() == IDOK && olAvailableEmpsDlg.omSelectedPoolJobUrnos.GetSize() > 0)
					{
						ogDataSet.ReassignJob(this, prlJob->Urno, olAvailableEmpsDlg.omSelectedPoolJobUrnos[0]);
					}
				}
			}
		}
	}
}

void StaffGantt::OnMenuStaffEdit()
{
	if(bmActiveBar)
	{
		JOBDATA *prlJob = GetJob(rmActiveBar.JobUrno);
		if(prlJob != NULL)
		{
			if (strcmp(prlJob->Jtco,JOBFLIGHT) == 0)
			{
				long llArrUrno, llDepUrno;
				ogBasicData.GetFlightUrnoForJob(prlJob, &llArrUrno, &llDepUrno);
				new FlightDetailWindow(this, llArrUrno, llDepUrno, FALSE, prlJob->Aloc, NULL, PERSONNELDEMANDS, prlJob->Acfr);
				//new FlightDetailWindow(this, ogBasicData.GetFlightUrnoForJob(prlJob),0, FALSE, prlJob->Aloc, NULL, PERSONNELDEMANDS);
				return;
			}
			else if (strcmp(prlJob->Jtco,JOBCCC) == 0)
			{
				DJobChange olJobChangeDialog(this,rmActiveBar.Text,prlJob->Acfr,prlJob->Acto);
				if(olJobChangeDialog.DoModal() == IDOK)
				{
					JOBDATA rlOldJob = *prlJob;
					prlJob->Acfr = olJobChangeDialog.m_Acfr;
					prlJob->Acto = olJobChangeDialog.m_Acto;
					ogJobData.ChangeJobData(prlJob, &rlOldJob, "CicEdt");
				}

				return;
			}
		}
	}

	if(lmActiveUrno != -1)
	{
		new StaffDetailWindow(this,lmActiveUrno);
	}
}

void StaffGantt::OnMenuStaffAssign()
{

	if(lmActiveUrno != -1)
	{
		CAllocDlg olAllocDlg;
		STAFF_GROUPDATA *prlGroup = pomViewer->GetGroup(imGroupno);
		//olAllocDlg.omPool = prlGroup->Text; //Commented Singapore
		olAllocDlg.omPool = pomViewer->GetPool(imGroupno); //Added Singapore

		if(pomViewer->bmTeamView && pomViewer->IsTeamCompressed(lmActiveUrno))
		{
			// display all emps in the same team if the team is compressed
			CCSPtrArray <JOBDATA> olTeamPoolJobs;
			pomViewer->GetPoolJobsInTeam(lmActiveUrno, olTeamPoolJobs, false);
			int ilNumPoolJobs = olTeamPoolJobs.GetSize();
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				olAllocDlg.AddPoolJobUrno(olTeamPoolJobs[ilPoolJob].Urno);
				olAllocDlg.bmTeamAlloc = true;
			}
		}
		else
		{
			int ilLineno = -1;
			if (pomViewer->FindLine(lmActiveUrno,imGroupno,ilLineno))
			{
				STAFF_LINEDATA *prlActiveLine = pomViewer->GetLine(imGroupno,ilLineno);
				if (prlActiveLine)
				{
					int ilNumBkBars = prlActiveLine->BkBars.GetSize();
					for(int ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
					{
						STAFF_BKBARDATA *prlBkBar = &prlActiveLine->BkBars[ilBkBar];
						if(!strcmp(prlBkBar->Type,JOBPOOL))
						{
							olAllocDlg.AddPoolJobUrno(prlBkBar->JobUrno);
						}
					}
				}
			}
		}

		if (bgIsPrm)
		{
			int ilLineno = -1;
			CTime olStart = TIMENULL; 
			CTime olEnd = TIMENULL;
			long llPooljobUrno = 0L;

			if (pomViewer->FindLine(lmActiveUrno,imGroupno,ilLineno))
			{
				STAFF_LINEDATA *prlActiveLine = pomViewer->GetLine(imGroupno,ilLineno);
				if (prlActiveLine)
				{
					int ilNumBkBars = prlActiveLine->BkBars.GetSize();
					for(int ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
					{
						STAFF_BKBARDATA *prlBkBar = &prlActiveLine->BkBars[ilBkBar];
						if(!strcmp(prlBkBar->Type,JOBPOOL))
						{
							llPooljobUrno = prlBkBar->JobUrno;
						}
					}

					olStart = prlActiveLine->ShiftAcfr;
					olEnd = olStart + 1800;
					PrmJobWithoutDemDlg olPrmJobWithoutDemDlg;

					olPrmJobWithoutDemDlg.InitDefaultValues(prlActiveLine->StfUrno, llPooljobUrno, olStart,olEnd);
					olPrmJobWithoutDemDlg.DoModal();
				}
			}


		}
		else
		{
			olAllocDlg.DoModal();
		}
		return;
	}

//	if(lmActiveUrno != NULL)
//	{
//		CCSPtrArray <long> olUrnoList;
//		olUrnoList.Add(&lmActiveUrno->JobUrno);
//
//		CAssignmentJobDialog dialog(this, &olUrnoList,
//			pomViewer->GetGroup(imGroupno)->GroupId,
//			pomViewer->GetGroup(imGroupno)->Text);
//		dialog.DoModal();
//
//	}

}


void StaffGantt::OnMenuPdaAssign()
{

	if(lmActiveUrno != -1)
	{
		CAllocDlg olAllocDlg;
		STAFF_GROUPDATA *prlGroup = pomViewer->GetGroup(imGroupno);
		//olAllocDlg.omPool = prlGroup->Text; //Commented Singapore
		olAllocDlg.omPool = pomViewer->GetPool(imGroupno); //Added Singapore

		if(pomViewer->bmTeamView && pomViewer->IsTeamCompressed(lmActiveUrno))
		{
			// display all emps in the same team if the team is compressed
			CCSPtrArray <JOBDATA> olTeamPoolJobs;
			pomViewer->GetPoolJobsInTeam(lmActiveUrno, olTeamPoolJobs, false);
			int ilNumPoolJobs = olTeamPoolJobs.GetSize();
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				olAllocDlg.AddPoolJobUrno(olTeamPoolJobs[ilPoolJob].Urno);
				olAllocDlg.bmTeamAlloc = true;
			}
		}
		else
		{
			int ilLineno = -1;
			if (pomViewer->FindLine(lmActiveUrno,imGroupno,ilLineno))
			{
				STAFF_LINEDATA *prlActiveLine = pomViewer->GetLine(imGroupno,ilLineno);
				if (prlActiveLine)
				{
					int ilNumBkBars = prlActiveLine->BkBars.GetSize();
					for(int ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
					{
						STAFF_BKBARDATA *prlBkBar = &prlActiveLine->BkBars[ilBkBar];
						if(!strcmp(prlBkBar->Type,JOBPOOL))
						{
							olAllocDlg.AddPoolJobUrno(prlBkBar->JobUrno);
						}
					}
				}
			}
		}

		if (bgIsPrm)
		{
			int ilLineno = -1;
			CTime olStart = TIMENULL; 
			CTime olEnd = TIMENULL;
			long llPooljobUrno = 0L;

			if (pomViewer->FindLine(lmActiveUrno,imGroupno,ilLineno))
			{
				STAFF_LINEDATA *prlActiveLine = pomViewer->GetLine(imGroupno,ilLineno);
				if (prlActiveLine)
				{
					int ilNumBkBars = prlActiveLine->BkBars.GetSize();
					for(int ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
					{
						STAFF_BKBARDATA *prlBkBar = &prlActiveLine->BkBars[ilBkBar];
						if(!strcmp(prlBkBar->Type,JOBPOOL))
						{
							llPooljobUrno = prlBkBar->JobUrno;
						}
					}

					olStart = prlActiveLine->ShiftAcfr;
					olEnd = olStart + 1800;
					PrmAssignPdaDlg olPrmAssignPdaDlg;

					olPrmAssignPdaDlg.InitDefaultValues(prlActiveLine->StfUrno);
					if (olPrmAssignPdaDlg.DoModal() != IDCANCEL)
					{
					pomViewer->ChangeViewTo(pomViewer->SelectView(),pomViewer->omStartTime,pomViewer->omEndTime);
					pomViewer->pomAttachWnd->Invalidate();
					}

				}
			}


		}
		return;
	}

//	if(lmActiveUrno != NULL)
//	{
//		CCSPtrArray <long> olUrnoList;
//		olUrnoList.Add(&lmActiveUrno->JobUrno);
//
//		CAssignmentJobDialog dialog(this, &olUrnoList,
//			pomViewer->GetGroup(imGroupno)->GroupId,
//			pomViewer->GetGroup(imGroupno)->Text);
//		dialog.DoModal();
//
//	}

}


 

void StaffGantt::OnMenuPdaRemove()
{

	if(lmActiveUrno != -1)
	{
		CAllocDlg olAllocDlg;
		STAFF_GROUPDATA *prlGroup = pomViewer->GetGroup(imGroupno);
		//olAllocDlg.omPool = prlGroup->Text; //Commented Singapore
		olAllocDlg.omPool = pomViewer->GetPool(imGroupno); //Added Singapore

		if(pomViewer->bmTeamView && pomViewer->IsTeamCompressed(lmActiveUrno))
		{
			// display all emps in the same team if the team is compressed
			CCSPtrArray <JOBDATA> olTeamPoolJobs;
			pomViewer->GetPoolJobsInTeam(lmActiveUrno, olTeamPoolJobs, false);
			int ilNumPoolJobs = olTeamPoolJobs.GetSize();
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				olAllocDlg.AddPoolJobUrno(olTeamPoolJobs[ilPoolJob].Urno);
				olAllocDlg.bmTeamAlloc = true;
			}
		}
		else
		{
			int ilLineno = -1;
			if (pomViewer->FindLine(lmActiveUrno,imGroupno,ilLineno))
			{
				STAFF_LINEDATA *prlActiveLine = pomViewer->GetLine(imGroupno,ilLineno);
				if (prlActiveLine)
				{
					int ilNumBkBars = prlActiveLine->BkBars.GetSize();
					for(int ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
					{
						STAFF_BKBARDATA *prlBkBar = &prlActiveLine->BkBars[ilBkBar];
						if(!strcmp(prlBkBar->Type,JOBPOOL))
						{
							olAllocDlg.AddPoolJobUrno(prlBkBar->JobUrno);
						}
					}
				}
			}
		}

		if (bgIsPrm)
		{
			int ilLineno = -1;
			CTime olStart = TIMENULL; 
			CTime olEnd = TIMENULL;
			long llPooljobUrno = 0L;

			if (pomViewer->FindLine(lmActiveUrno,imGroupno,ilLineno))
			{
				STAFF_LINEDATA *prlActiveLine = pomViewer->GetLine(imGroupno,ilLineno);
				if (prlActiveLine)
				{
					int ilNumBkBars = prlActiveLine->BkBars.GetSize();
					for(int ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
					{
						STAFF_BKBARDATA *prlBkBar = &prlActiveLine->BkBars[ilBkBar];
						if(!strcmp(prlBkBar->Type,JOBPOOL))
						{
							llPooljobUrno = prlBkBar->JobUrno;
						}
					}

					ogPdaData.RemoveFromStaff(prlActiveLine->StfUrno);
					pomViewer->ChangeViewTo(pomViewer->SelectView(),pomViewer->omStartTime,pomViewer->omEndTime);
					pomViewer->pomAttachWnd->Invalidate();

					/***
					ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prlActiveLine->s); //Update Viewer
					PDADATA *prlPda = ogPdaData.GetPdaByUstf(prlActiveLine->StfUrno);
					if (prlPda != NULL)
					{
						ogCCSDdx.DataChanged((void *)this,PDA_CHANGE,(void *)prlPda); //Update Viewer
					}
					***/
				}
			}


		}
		return;
	}
}
void StaffGantt::OnMenuStaffBreak()
{
	if(lmActiveUrno != -1)
	{
		CString olEmpName;
		EMPDATA *prlEmp = NULL;
		SHIFTDATA *prlShift = NULL;
		CTime olBreakStart = TIMENULL;
		JOBDATA *prlJob = NULL;

		CCSPtrArray <JOBDATA> olTeamPoolJobs;
		if(pomViewer->bmTeamView && pomViewer->IsTeamCompressed(lmActiveUrno))
		{
			JOBDATA *prlPoolJob = NULL;
			CString olTmp;
			// breaks for a team
			pomViewer->GetPoolJobsInTeam(lmActiveUrno, olTeamPoolJobs, false);
			int ilNumPoolJobs = olTeamPoolJobs.GetSize();
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				JOBDATA *prlPoolJob = &olTeamPoolJobs[ilPoolJob];
				if((prlEmp = ogEmpData.GetEmpByPeno(prlPoolJob->Peno)) != NULL)
				{
					if(!olEmpName.IsEmpty())
					{
						olEmpName += "\r\n";
					}
					olEmpName += ogEmpData.GetEmpName(prlEmp);
				}
			}
		}
		if ((prlJob = ogJobData.GetJobByUrno(lmActiveUrno)) != NULL &&
			(prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno)) != NULL &&
			(prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur)) != NULL)
		{
			CTime olNow = ogBasicData.GetTime() + CTimeSpan(0, 0, 2, 0);
			if(prlShift->Sbfr != TIMENULL && olNow <= prlShift->Sbfr)
			{
				// there is a break period and it is in the future so set the break start to the start of the break period
				olBreakStart = prlShift->Sbfr;
			}
			else if(olNow >= prlShift->Avfa && olNow <= prlShift->Avta)
			{
				// no break period so use the current time if it is within the shift
				olBreakStart = olNow;
			}
			else
			{
				// default -- set the break start to the start of the shift
				olBreakStart = prlShift->Avfa;
			}
			if(olEmpName.IsEmpty())
			{
				olEmpName = ogEmpData.GetEmpName(prlEmp);
			}
		}

		if(!olEmpName.IsEmpty() && olBreakStart != TIMENULL)
		{
			CBreakJobDialog olBreakDlg(this,olBreakStart);
			olBreakDlg.m_Name = olEmpName;
			olBreakDlg.m_CoffeeBreak = false;
			if (olBreakDlg.DoModal() == IDOK)
			{
				CTimeSpan olDuration(0, olBreakDlg.m_Duration / 60, olBreakDlg.m_Duration % 60, 0);
				CTime olStart = olBreakDlg.m_StartTime;
				CTime olEnd = olBreakDlg.m_StartTime + olDuration;
				bool blCoffeeBreak = olBreakDlg.m_CoffeeBreak ? true : false;

				if(olTeamPoolJobs.GetSize() > 0)
				{
					int ilNumPoolJobs = olTeamPoolJobs.GetSize();
					for(int ilPoolJob = (ilNumPoolJobs-1); ilPoolJob >= 0; ilPoolJob--)
					{
						JOBDATA *prlPoolJob = &olTeamPoolJobs[ilPoolJob];
						if(!IsWithIn(olStart,olEnd,prlPoolJob->Acfr,prlPoolJob->Acto))
						{
							olTeamPoolJobs.RemoveAt(ilPoolJob);
						}
					}

					if(olTeamPoolJobs.GetSize() > 0)
					{
						ogDataSet.CreateJobBreak(this,olTeamPoolJobs,olStart,olEnd,blCoffeeBreak);
					}
				}
				else
				{
					ogDataSet.CreateJobBreak(this,lmActiveUrno,olStart,olEnd,blCoffeeBreak);
				}
			}
		}
	}
}

// set pool job to ignore for automatische Einteilung (Job.Ignr='1') or to '0'
void StaffGantt::OnMenuPoolJobStandby()
{
	if(lmActiveUrno != -1)
	{
		JOBDATA *prlJob = GetJob(lmActiveUrno);
		if(prlJob != NULL)
		{
			CCSPtrArray <JOBDATA> olPoolJobs;
			if(!pomViewer->GetPoolJobsForCompressedPoolJob(prlJob->Urno, olPoolJobs)) // confirm all pool jobs for the compressed team
			{
				ogJobData.GetJobsByShur(olPoolJobs,prlJob->Shur); // confirm all pool jobs for this emp
			}
			int ilNumPoolJobs = olPoolJobs.GetSize();
			if(ilNumPoolJobs > 1)
			{
				ogJobData.StartTransaction();
				ogJodData.StartTransaction();
			}
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				ogDataSet.SetJobStandby(this,&olPoolJobs[ilPoolJob]);
			}
			if(ilNumPoolJobs > 1)
			{
				ogJobData.CommitTransaction();
				ogJodData.CommitTransaction();
			}
		}
	}
}

// set pool job to ignore for automatische Einteilung (Job.Ignr='1') or to '0'
void StaffGantt::OnMenuPoolJobAbsence()
{
	if(lmActiveUrno != -1)
	{
		JOBDATA *prlJob = GetJob(lmActiveUrno);
		if(prlJob != NULL)
		{
			CCSPtrArray <JOBDATA> olPoolJobs;
			if(!pomViewer->GetPoolJobsForCompressedPoolJob(prlJob->Urno, olPoolJobs)) // confirm all pool jobs for the compressed team
			{
				ogJobData.GetJobsByShur(olPoolJobs,prlJob->Shur); // confirm all pool jobs for this emp
			}
			int ilNumPoolJobs = olPoolJobs.GetSize();
			if(ilNumPoolJobs > 1)
			{
				ogJobData.StartTransaction();
				ogJodData.StartTransaction();
			}
			JOBDATA *prlPJ = NULL;
			bool blAbsent = true;
			int ilPoolJob;
			for(ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				prlPJ = &olPoolJobs[ilPoolJob];
				if(!prlPJ->Infm)
				{
					blAbsent = false;
					break;
				}
			}
			for(ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				prlPJ = &olPoolJobs[ilPoolJob];
				prlPJ->Infm = blAbsent;
				ogDataSet.EmpAbsent(prlPJ);
			}
			if(ilNumPoolJobs > 1)
			{
				ogJobData.CommitTransaction();
				ogJodData.CommitTransaction();
			}
		}
	}
}

// set break to NormalBreak (Job.Ignr='0') or CoffeeBreak (Job.Ignr='1')
void StaffGantt::OnMenuBreakUnpayed()
{
	if(bmActiveBar)
	{
		JOBDATA *prlJob = GetJob(rmActiveBar.JobUrno);
		if(prlJob != NULL && !strcmp(prlJob->Jtco, JOBBREAK))
		{
			CCSPtrArray <JOBDATA> olJobs;
			if(pomViewer->GetJobsForCompressedJob(prlJob->Urno, olJobs))
			{
				CString olCompressedJobIgnr("0");
				JOBDATA *prlCompressedJob = pomViewer->GetCompressedJobByNormalJobUrno(rmActiveBar.JobUrno);
				if(prlCompressedJob != NULL)
				{
					olCompressedJobIgnr = CString(prlCompressedJob->Ignr);
				}

				int ilNumJobs = olJobs.GetSize();
				if(ilNumJobs > 1)
				{
					ogJobData.StartTransaction();
					ogJodData.StartTransaction();
				}

				for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
				{
					JOBDATA *prlBreak = &olJobs[ilJob];
					if(!strcmp(prlBreak->Jtco, JOBBREAK) && !strcmp(olCompressedJobIgnr,prlBreak->Ignr))
					{
						ogDataSet.SetCoffeeBreak(this,prlBreak);
					}
				}
				if(ilNumJobs > 1)
				{
					ogJobData.CommitTransaction();
					ogJodData.CommitTransaction();
				}
			}
			else
			{
				ogDataSet.SetCoffeeBreak(this,prlJob);
			}
		}
	}
}

void StaffGantt::OnMenuStaffSplitJob()
{
	long llJobUrno = 0L;
	if(bmActiveBar)
	{
		// confirm a normal job
		llJobUrno = rmActiveBar.JobUrno;
	}
	else if(lmActiveUrno != -1)
	{
		llJobUrno = lmActiveUrno;
	}

	if(llJobUrno != 0L)
	{
		JOBDATA *prlJob = GetJob(llJobUrno);
		if(prlJob != NULL)
		{
			CString olTitle;
			CTime olSplitTime = TIMENULL;
			bool blCanChangeStartTime = true, blCanChangeEndTime = true;
			CTime olStart = prlJob->Acfr, olEnd = prlJob->Acto;
			CCSPtrArray <JOBDATA> olJobs;

			if(pomViewer->GetJobsForCompressedJob(prlJob->Urno, olJobs))
			{
				int ilNumJobs = olJobs.GetSize();
				for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
				{
					if(olJobs[ilJ].Acfr != olStart)
					{
						blCanChangeStartTime = false;
						if(olJobs[ilJ].Acfr < olStart)
						{
							olStart = olJobs[ilJ].Acfr;
						}
					}
					if(olJobs[ilJ].Acto != olEnd)
					{
						blCanChangeEndTime = false;
						if(olJobs[ilJ].Acto > olEnd)
						{
							olEnd = olJobs[ilJ].Acto;
						}
					}
				}
			}
			else
			{
				olJobs.Add(prlJob);

				EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlJob->Ustf);
				if(prlEmp != NULL)
				{
					olTitle.Format("%s %s-%s", ogEmpData.GetEmpName(prlEmp), prlJob->Acfr.Format("%H%M"), prlJob->Acto.Format("%H%M"));
				}

				JOBDATA *prlPJ = ogJobData.GetJobByUrno(prlJob->Jour);
				if(prlPJ != NULL)
				{
					if(IsBetween(prlPJ->Acto, prlJob->Acfr, prlJob->Acto))
					{
						olSplitTime = prlPJ->Acto;
					}
				}
			}

			CSplitJobDlg olSplitJobDlg;
			olSplitJobDlg.InitJobTimes(olTitle, olStart, olEnd, olSplitTime, blCanChangeStartTime, blCanChangeEndTime);
			if(olSplitJobDlg.DoModal() == IDOK)
			{
				CTime olJob1Start, olJob1End, olJob2Start, olJob2End;
				olSplitJobDlg.GetJobTimes(olJob1Start, olJob1End, olJob2Start, olJob2End);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				int ilNumJobs = olJobs.GetSize();
				if(ilNumJobs > 1)
				{
					ogJobData.StartTransaction();
					ogJodData.StartTransaction();
				}
				for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
				{
					JOBDATA *prlJ = &olJobs[ilJ];

					if(IsBetween(olJob2Start,prlJ->Acfr,prlJ->Acto))
					{
						JODDATA *prlJod = NULL;
						CCSPtrArray<JODDATA> olJods;
						ogJodData.GetJodsByJob(olJods, prlJ->Urno);
						if(olJods.GetSize() > 0)
						{
							prlJod = &olJods[0];
						}
						ogDataSet.CopyAssignment(this, prlJ, olJob2Start, (blCanChangeEndTime) ? olJob2End : prlJ->Acto, prlJod);
					}

					if(IsBetween(olJob1End,prlJ->Acfr,prlJ->Acto))
					{
						ogDataSet.ChangeTimeOfJob(prlJ->Urno, (blCanChangeStartTime) ? olJob1Start : prlJ->Acfr, olJob1End, FALSE);
					}
				}
				if(ilNumJobs > 1)
				{
					ogJobData.CommitTransaction();
					ogJodData.CommitTransaction();
				}
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
			}
		}
	}
}

void StaffGantt::OnMenuStaffCopyJob()
{
	long llJobUrno = 0L;
	if(bmActiveBar)
	{
		// confirm a normal job
		llJobUrno = rmActiveBar.JobUrno;
	}
	else if(lmActiveUrno != -1)
	{
		llJobUrno = lmActiveUrno;
	}

	if(llJobUrno != 0L)
	{
		JOBDATA *prlJob = GetJob(llJobUrno);
		if(prlJob != NULL)
		{

			JODDATA *prlJod = NULL;
			CCSPtrArray<JODDATA> olJods;
			ogJodData.GetJodsByJob(olJods, prlJob->Urno);
			if(olJods.GetSize() > 0)
			{
				prlJod = &olJods[0];
			}
			ogDataSet.CopyAssignment(this, prlJob, prlJob->Acfr, prlJob->Acto, prlJod);
		}
	}
}

void StaffGantt::OnMenuStaffConfirm()
{
	// get a list of selected jobs
	int ilNumSelectedJobs = 0;
	JOBDATA *prlSelectedJob = NULL;
	CCSPtrArray <JOBDATA> olSelectedJobs;
	if((ilNumSelectedJobs = pomViewer->GetSelectedJobs(olSelectedJobs)) <= 0)
	{
		long llJobUrno = 0L;
		if(bmActiveBar)
		{
			// confirm a normal job
			llJobUrno = rmActiveBar.JobUrno;
		}
		else if(lmActiveUrno != -1)
		{
			llJobUrno = lmActiveUrno;
		}
		if((prlSelectedJob = ogJobData.GetJobByUrno(llJobUrno)) != NULL)
		{
			// single job selected
			olSelectedJobs.Add(prlSelectedJob);
			ilNumSelectedJobs = 1;
		}
	}

	bool blTransaction = false;
	if(ilNumSelectedJobs > 1)
	{
		ogJobData.StartTransaction();
		ogJodData.StartTransaction();
		blTransaction = true;
	}

	int ilNumJobs = 0;
	JOBDATA *prlJob = NULL;
	CCSPtrArray <JOBDATA> olJobs;
	for(int ilSJ = 0; ilSJ < ilNumSelectedJobs; ilSJ++)
	{
		prlSelectedJob = &olSelectedJobs[ilSJ];

		CCSPtrArray <JOBDATA> olJobs;
		if(!strcmp(prlSelectedJob->Jtco,JOBPOOL))
		{
			// for pool jobs the status can be toggled between confirmed and planned
			CString olStatus = (*prlSelectedJob->Stat == 'P') ? "C" : "P";

			if(!pomViewer->GetPoolJobsForCompressedPoolJob(prlSelectedJob->Urno, olJobs)) // confirm all pool jobs for the compressed team
			{
				ogJobData.GetJobsByShur(olJobs,prlSelectedJob->Shur); // confirm all pool jobs for this emp
			}
			if(olJobs.GetSize() > 1 && !blTransaction)
			{
				ogJobData.StartTransaction();
				ogJodData.StartTransaction();
				blTransaction = true;
			}
			ogDataSet.ConfirmJob(this,olJobs,olStatus);
		}
		else
		{
			if(pomViewer->GetJobsForCompressedJob(prlSelectedJob->Urno, olJobs))
			{
				if(olJobs.GetSize() > 1 && !blTransaction)
				{
					ogJobData.StartTransaction();
					ogJodData.StartTransaction();
					blTransaction = true;
				}
				ogDataSet.ConfirmJob(this,olJobs); // confirm all jobs that made up this compressed job for a team
			}
			else
			{
				ogDataSet.ConfirmJob(this,prlSelectedJob); // confirm a single job
			}
		}
	}

	if(blTransaction)
	{
		ogJobData.CommitTransaction();
		ogJodData.CommitTransaction();
	}
}

void StaffGantt::OnMenuStaffBarEnd()
{
	int ilNumSelectedJobs = 0;
	JOBDATA *prlSelectedJob = NULL;
	CCSPtrArray <JOBDATA> olSelectedJobs;
	if((ilNumSelectedJobs = pomViewer->GetSelectedJobs(olSelectedJobs)) <= 0)
	{
		long llJobUrno = 0L;
		if(bmActiveBar)
		{
			// confirm a normal job
			llJobUrno = rmActiveBar.JobUrno;
		}
		else if(lmActiveUrno != -1)
		{
			llJobUrno = lmActiveUrno;
		}
		if((prlSelectedJob = ogJobData.GetJobByUrno(llJobUrno)) != NULL)
		{
			// single job selected
			olSelectedJobs.Add(prlSelectedJob);
			ilNumSelectedJobs = 1;
		}
	}

	bool blTransaction = false;
	if(ilNumSelectedJobs > 1)
	{
		ogJobData.StartTransaction();
		ogJodData.StartTransaction();
		blTransaction = true;
	}
	int ilNumJobs = 0;
	JOBDATA *prlJob = NULL;
	CCSPtrArray <JOBDATA> olJobs;
	for(int ilSJ = 0; ilSJ < ilNumSelectedJobs; ilSJ++)
	{
		prlSelectedJob = &olSelectedJobs[ilSJ];

		CCSPtrArray <JOBDATA> olJobs;
		if(!strcmp(prlSelectedJob->Jtco,JOBPOOL))
		{
			if(!pomViewer->GetPoolJobsForCompressedPoolJob(prlSelectedJob->Urno, olJobs)) // confirm all pool jobs for the compressed team
			{
				ogJobData.GetJobsByShur(olJobs,prlSelectedJob->Shur); // confirm all pool jobs for this emp
			}
			int ilNumJobs = olJobs.GetSize();
			if(ilNumJobs > 1 && !blTransaction)
			{
				ogJobData.StartTransaction();
				ogJodData.StartTransaction();
				blTransaction = true;
			}
			for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
			{
				JOBDATA *prlJob = &olJobs[ilJ];

				if(*prlJob->Stat == 'F')
				{
					ogDataSet.ConfirmJob(this, prlJob, "C");
				}
				else
				{
					ogDataSet.FinishJob(prlJob, prlJob->Acto);
				}
			}
		}
		else
		{
			if(pomViewer->GetJobsForCompressedJob(prlSelectedJob->Urno, olJobs))
			{
				int ilNumJobs = olJobs.GetSize();
				if(ilNumJobs > 1 && !blTransaction)
				{
					ogJobData.StartTransaction();
					ogJodData.StartTransaction();
					blTransaction = true;
				}
				ogDataSet.FinishJob(olJobs);
			}
			else
			{
				ogDataSet.FinishJob(prlSelectedJob);
			}
		}
	}

	if(blTransaction)
	{
		ogJobData.CommitTransaction();
		ogJodData.CommitTransaction();
	}
}


void StaffGantt::OnMenuChangeJob()
{
	if(bmActiveBar)
	{
		JOBDATA *prlJob = GetJob(rmActiveBar.JobUrno);
		if(prlJob != NULL)
		{
			DJobChange olJobChangeDialog(this,prlJob);
			if(olJobChangeDialog.DoModal() == IDOK)
			{
				DataSet::ChangeTimeOfJob(rmActiveBar.JobUrno,olJobChangeDialog.m_Acfr,olJobChangeDialog.m_Acto,TRUE);
			}
		}
	}
}


void StaffGantt::OnMenuStaffBarDelete()
{
	if(bmActiveBar)
	{
		JOBDATA *prlJob = GetJob(rmActiveBar.JobUrno);
		if(prlJob != NULL)
		{
			CCSPtrArray <JOBDATA> olJobs;
			if(pomViewer->GetJobsForCompressedJob(prlJob->Urno, olJobs))
			{
				int ilNumJobs = olJobs.GetSize();
				if(ilNumJobs > 1)
				{
					ogJobData.StartTransaction();
					ogJodData.StartTransaction();
				}
				ogDataSet.DeleteJob(this, olJobs);
				if(ilNumJobs > 1)
				{
					ogJobData.CommitTransaction();
					ogJodData.CommitTransaction();
				}
			}
			else
			{
				ogDataSet.DeleteJob(this, prlJob);
			}
		}
	}
}

void StaffGantt::OnMenuStaffBarAcceptConflict(UINT ipId)
{
	if(bmActiveBar)
	{
		JOBDATA *prlJob;
		if(ipId == ACCEPT_ALL_CONFLICTS)
		{
			// accept all conflicts
			long ilId;
			for(POSITION rlPos = omConflictToJobMap.GetStartPosition(); rlPos != NULL; )
			{
				omConflictToJobMap.GetNextAssoc(rlPos, (void *&)ilId, (void *&)prlJob);
				ogConflicts.AcceptJobConflicts(prlJob->Urno);
			}
		}
		else
		{
			// accept a single conflict
			CONFLICTDATA *prlConflict;
			omAcceptConflictsMap.Lookup((void *) ipId, (void *&) prlConflict);
			omConflictToJobMap.Lookup((void *) ipId, (void *&) prlJob);
			ogConflicts.AcceptOneJobConflict(prlJob->Urno, prlConflict->Type);
		}
	}
}

void StaffGantt::OnMenuEditFastLink(UINT ipId)
{
	JOBDATA *prlFastLinkJob = NULL;
	omMenuItemToEquFastLinkMap.Lookup((void *) ipId, (void *&) prlFastLinkJob);
	if(prlFastLinkJob != NULL)
	{
		CString olText;
		EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlFastLinkJob->Ustf);
		EQUDATA *prlEqu = ogEquData.GetEquByUrno(prlFastLinkJob->Uequ);
		if(prlEmp != NULL && prlEqu != NULL)
		{
			olText.Format("%s - %s%s",ogEmpData.GetEmpName(prlEmp), GetString(IDS_EDITFASTLINK), prlEqu->Enam);
		}
		DJobChange olJobChangeDialog(this,olText,prlFastLinkJob->Acfr,prlFastLinkJob->Acto);
		if(olJobChangeDialog.DoModal() == IDOK)
		{
			JOBDATA rlOldJob = *prlFastLinkJob;
			prlFastLinkJob->Acfr = olJobChangeDialog.m_Acfr;
			prlFastLinkJob->Acto = olJobChangeDialog.m_Acto;
			ogJobData.ChangeJobData(prlFastLinkJob, &rlOldJob, "EFLEdt");
		}
	}
}

void StaffGantt::OnMenuDeleteFastLink(UINT ipId)
{
	JOBDATA *prlFastLinkJob = NULL;
	omMenuItemToEquFastLinkMap.Lookup((void *) ipId, (void *&) prlFastLinkJob);
	if(prlFastLinkJob != NULL)
	{
		CString olText;
		EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlFastLinkJob->Ustf);
		EQUDATA *prlEqu = ogEquData.GetEquByUrno(prlFastLinkJob->Uequ);
		if(prlEmp != NULL && prlEqu != NULL)
		{
			olText.Format("%s - %s %s",ogEmpData.GetEmpName(prlEmp), GetString(IDS_CONFIRMDELETEFASTLINK), prlEqu->Enam);
		}
		if(MessageBox(olText, GetString(IDS_STRING61210), MB_ICONQUESTION|MB_YESNO) == IDYES)
		{
			ogDataSet.DeleteJob(this, prlFastLinkJob);
		}
	}
}

// toggle between:
// (Job.Ignr='0') job is used in automatic assignment (jobhdl) and
// (Job.Ignr='1') job is not used in automatic assignment
void StaffGantt::OnMenuStaffBarFix()
{
	if(bmActiveBar)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		JOBDATA *prlJob = NULL;
		CCSPtrArray <JOBDATA> olJobs;
		if(pomViewer->GetJobsForCompressedJob(rmActiveBar.JobUrno, olJobs))
		{
			CString olCompressedJobIgnr("0");
			JOBDATA *prlCompressedJob = pomViewer->GetCompressedJobByNormalJobUrno(rmActiveBar.JobUrno);
			if(prlCompressedJob != NULL)
			{
				olCompressedJobIgnr = CString(prlCompressedJob->Ignr);
			}

			int ilNumJobs = olJobs.GetSize();
			if(ilNumJobs > 1)
			{
				ogJobData.StartTransaction();
				ogJodData.StartTransaction();
			}
			for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
			{
				prlJob = &olJobs[ilJob];
				if(!strcmp(olCompressedJobIgnr,prlJob->Ignr))
				{
					ogDataSet.SetJobStandby(this,prlJob);
				}
			}
			if(ilNumJobs > 1)
			{
				ogJobData.CommitTransaction();
				ogJodData.CommitTransaction();
			}
		}
		else if((prlJob = ogJobData.GetJobByUrno(rmActiveBar.JobUrno)) != NULL)
		{
			ogDataSet.SetJobStandby(this,prlJob);
		}
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}

void StaffGantt::OnMenuStaffInform()
{
	if(bmActiveBar)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

		// get a list of selected jobs
		int ilNumSelectedJobs = 0;
		JOBDATA *prlSelectedJob = NULL;
		CCSPtrArray <JOBDATA> olSelectedJobs;
		if((ilNumSelectedJobs = pomViewer->GetSelectedJobs(olSelectedJobs)) <= 0)
		{
			if((prlSelectedJob = ogJobData.GetJobByUrno(rmActiveBar.JobUrno)) != NULL)
			{
				// single job selected
				olSelectedJobs.Add(prlSelectedJob);
				ilNumSelectedJobs = 1;
			}
		}

		bool blTransaction = false;
		if(ilNumSelectedJobs > 1)
		{
			ogJobData.StartTransaction();
			blTransaction = true;
		}

		int ilNumJobs = 0;
		JOBDATA *prlJob = NULL;
		CCSPtrArray <JOBDATA> olJobs;
		for(int ilSJ = 0; ilSJ < ilNumSelectedJobs; ilSJ++)
		{
			prlSelectedJob = &olSelectedJobs[ilSJ];

			if(pomViewer->GetJobsForCompressedJob(prlSelectedJob->Urno, olJobs))
			{
				// compressed job selected
				bool blEmpInformed = false;
				JOBDATA *prlCompressedJob = pomViewer->GetCompressedJobByNormalJobUrno(rmActiveBar.JobUrno);
				if(prlCompressedJob != NULL)
				{
					blEmpInformed = prlCompressedJob->Infm;
				}
				ilNumJobs = olJobs.GetSize();
				if(ilNumJobs > 1 && !blTransaction)
				{
					ogJobData.StartTransaction();
					blTransaction = true;
				}
				for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
				{
					prlJob = &olJobs[ilJob];
					if(prlJob->Infm == blEmpInformed)
					{
						ogDataSet.EmpInformedOfJob(prlJob);
					}
				}
			}
			else
			{
				ogDataSet.EmpInformedOfJob(prlSelectedJob);
			}
		}
		if(blTransaction)
		{
			ogJobData.CommitTransaction();
		}

        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}

void StaffGantt::OnMenuStaffBarBreak()
{
	if(lmActiveUrno != -1 && bmActiveBar)
	{
		// checking for valid job and employee data
		JOBDATA *prlJob;
		EMPDATA *prlEmp;
		SHIFTDATA *prlShift;
		if ((prlJob = ogJobData.GetJobByUrno(rmActiveBar.JobUrno)) == NULL ||
			(prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno)) == NULL ||
			(prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur)) == NULL)
			return;

		CBreakJobDialog dialog(this,prlShift->Sbfr);
		dialog.m_Name = ogEmpData.GetEmpName(prlEmp);
		dialog.m_CoffeeBreak = false;
		//dialog.m_StartTime = prlShift->Sbfr;
		if (dialog.DoModal() == IDOK)
		{
			CTimeSpan omDuration(0, dialog.m_Duration / 60, dialog.m_Duration % 60, 0);
			ogDataSet.CreateJobBreak(this, lmActiveUrno,dialog.m_StartTime, dialog.m_StartTime + omDuration,dialog.m_CoffeeBreak);
		}
	}
}

void StaffGantt::OnMenuDebugInfo()
{
	if(bmActiveBar)
	{
		// display debug info for a single job
		CString olText = "Job:\n" + ogJobData.Dump(rmActiveBar.JobUrno);
		ogBasicData.DebugInfoMsgBox(olText);
	}
	else if(bmActiveBkBar)
	{
		// display debug info for a background bar (pool job/temp absence etc)
		CString olText = "Job:\n" + ogJobData.Dump(rmActiveBkBar.JobUrno);
		ogBasicData.DebugInfoMsgBox(olText);
	}
	else if(lmActiveUrno != -1)
	{
		// display debug info for employee
		CString olText;
		JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(lmActiveUrno);
		if(prlPoolJob != NULL)
		{
			CCSPtrArray <JOBDATA> olPoolJobs;
			ogJobData.GetJobsByShur(olPoolJobs,prlPoolJob->Shur);
			int ilNumPoolJobs = olPoolJobs.GetSize();
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				prlPoolJob = &olPoolJobs[ilPoolJob];
				olText += "Pool Job:\n" + ogJobData.Dump(prlPoolJob->Urno) + "\n";
				SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prlPoolJob->Shur);
				if(prlShift == NULL)
				{
					olText += "No shift data found for the pool job (Ustf)\n";
				}
				else
				{
					olText += "Shift Record:\n" + ogShiftData.Dump(prlPoolJob->Shur) + "\n";
					EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlShift->Stfu);
					if(prlEmp == NULL)
					{
						olText += "No emp data found for the shift (Stfu)\n";
					}
					else
					{
						olText += "Emp Record:\n" + ogEmpData.Dump(prlPoolJob->Ustf) + "\n";
					}
					DRGDATA *prlDrg = ogDrgData.GetDrgByShift(prlShift);
					if(prlDrg != NULL)
						olText += "DRG Group Record (DRG overrides SWG record):\n" + ogDrgData.Dump(prlDrg->Urno) + "\n";
					else
						olText += "No DRG group data found for the shift (Stfu/Sday)\n";
					olText += "Swg Group Records:\n" + ogSwgData.DumpForUstf(prlShift->Stfu, prlShift->Avfa);

					SPRDATA* prlSpr = NULL;
					if((prlSpr = ogSprData.GetSprByUstf(prlShift->Stfu, SPR_STARTSHIFT, prlShift->Avfa, prlShift->Avta)) != NULL)
					{
						olText += "Spr Record (clock-on):\n" + ogSprData.Dump(prlSpr->Urno) + "\n";
					}
					if((prlSpr = ogSprData.GetSprByUstf(prlShift->Stfu, SPR_ENDSHIFT, prlShift->Avfa, prlShift->Avta)) != NULL)
					{
						olText += "Spr Record (clock-off):\n" + ogSprData.Dump(prlSpr->Urno) + "\n";
					}
				}
				CCSPtrArray <JOBDATA> olJobs;
				ogJobData.GetJobsByPoolJob(olJobs, prlPoolJob->Urno);
				int ilNumJobs = olJobs.GetSize();
				if(ilNumJobs <= 0)
				{
					olText += "No Jobs found for this pool job\n";
				}
				else
				{
					for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
					{
						olText += "Job:\n" + ogJobData.Dump(olJobs[ilJob].Urno) + "\n";
					}
				}
			}
		}
		else
		{
			 olText += "Pool Job Not Found";
		}
			
		olText += "\n" + pomViewer->DumpTeamData(lmActiveUrno);
		int ilLineno = -1;
		if (pomViewer->FindLine(lmActiveUrno,imGroupno,ilLineno))
		{
			CString olTxt;
			STAFF_LINEDATA *prlActiveLine = pomViewer->GetLine(imGroupno,ilLineno);
			if(prlActiveLine)
			{
//				olTxt.Format("\n************** STAFF_LINEDATA Groupno <%d> Lineno <%d> **************\nJobUrno <%ld> StfUrno <%ld> ShiftAcfr <%s> ShiftActo <%s> "
//								"ShiftStid <%s> ShiftTmid <%s> EmpName <%s> EmpRank <%s> EmpFunction <%s> "
//								"DoubleTourStudent <%s> SortString <%s> TeamFound <%s> Text <%s> "
//								"MaxOverlapLevel <%d> GroupFunctionWeight <%d>",
//					prlActiveLine->JobUrno, prlActiveLine->StfUrno, prlActiveLine->ShiftAcfr.Format("%d.%m.%Y %H:%M"), prlActiveLine->ShiftActo.Format("%d.%m.%Y %H:%M"), 
//					prlActiveLine->ShiftStid, prlActiveLine->ShiftTmid, prlActiveLine->EmpName, prlActiveLine->EmpRank, prlActiveLine->EmpFunction, 
//					prlActiveLine->DoubleTourStudent, prlActiveLine->SortString, prlActiveLine->TeamFound ? "true" : "false", prlActiveLine->Text, 
//					prlActiveLine->MaxOverlapLevel, prlActiveLine->GroupFunctionWeight);
				olTxt.Format("\n-> STAFF_LINEDATA Groupno <%d> Lineno <%d> ", imGroupno,ilLineno);
				olTxt += DumpField("JobUrno", prlActiveLine->JobUrno);
				olTxt += DumpField("StfUrno",prlActiveLine->StfUrno);
				olTxt += DumpField("ShiftAcfr",prlActiveLine->ShiftAcfr);
				olTxt += DumpField("ShiftActo",prlActiveLine->ShiftActo);
				olTxt += DumpField("ShiftStid",prlActiveLine->ShiftStid);
				olTxt += DumpField("ShiftTmid",prlActiveLine->ShiftTmid);
				olTxt += DumpField("EmpName",prlActiveLine->EmpName);
				olTxt += DumpField("EmpRank",prlActiveLine->EmpRank);
				olTxt += DumpField("EmpFunction",prlActiveLine->EmpFunction);
				olTxt += DumpField("DoubleTourStudent",prlActiveLine->DoubleTourStudent);
				olTxt += DumpField("SortString",prlActiveLine->SortString);
				olTxt += DumpField("TeamFound",prlActiveLine->TeamFound);
				olTxt += DumpField("Text",prlActiveLine->Text);
				olTxt += DumpField("MaxOverlapLevel",(long) prlActiveLine->MaxOverlapLevel);
				olTxt += DumpField("GroupFunctionWeight",(long) prlActiveLine->GroupFunctionWeight);

				olText += olTxt;

				for(int bb = 0; bb < prlActiveLine->BkBars.GetSize(); bb++)
				{
					STAFF_BKBARDATA *prlBkBar = &prlActiveLine->BkBars[bb];
					olTxt.Format("\n--> STAFF_BKBARDATA <%d> ", bb);
					olTxt += DumpField("Type", prlBkBar->Type);
					olTxt += DumpField("Text", prlBkBar->Text);
					olTxt += DumpField("StatusText", prlBkBar->StatusText);
					olTxt += DumpField("StartTime", prlBkBar->StartTime);
					olTxt += DumpField("EndTime", prlBkBar->EndTime);
					olTxt += DumpField("JobUrno", prlBkBar->JobUrno);
					olTxt += DumpField("OfflineBar", prlBkBar->OfflineBar);
					olText += olTxt;
				}

				for(int b = 0; b < prlActiveLine->Bars.GetSize(); b++)
				{
					STAFF_BARDATA *prlBar = &prlActiveLine->Bars[b];
					olTxt.Format("\n--> STAFF_BARDATA <%d> ", b);
					olTxt += DumpField("JobUrno", prlBar->JobUrno);
					olTxt += DumpField("Text", prlBar->Text);
					olTxt += DumpField("StatusText", prlBar->StatusText);
					olTxt += DumpField("StartTime", prlBar->StartTime);
					olTxt += DumpField("EndTime", prlBar->EndTime);
					olTxt += DumpField("Selected", prlBar->Selected);
					olTxt += DumpField("OfflineBar", prlBar->OfflineBar);
					olTxt += DumpField("OverlapLevel", (long) prlBar->OverlapLevel);
					olTxt += DumpField("Infm", prlBar->Infm);
					olTxt += DumpField("IsPause", prlBar->IsPause);
					olTxt += DumpField("DifferentFunction", prlBar->DifferentFunction);
					olText += olTxt;
				}
			}
		}
		ogBasicData.DebugInfoMsgBox(olText);
		//MessageBox(olText,"Debug Info",MB_ICONINFORMATION);
	}
}

//PRF 8998
void StaffGantt::OnMenuLastJobInfo()
{
	if(bmActiveBar)
	{
		LastJobInfo olLastJobInfo(rmActiveBar.JobUrno,this);
		olLastJobInfo.DoModal();
	}	
}

CString StaffGantt::DumpField(CString opFieldName, CString opFieldValue)
{
	CString olField;
	olField.Format("%s <%s> ", opFieldName, opFieldValue);
	return olField;
}

CString StaffGantt::DumpField(CString opFieldName, bool bpFieldValue)
{
	CString olField;
	olField.Format("%s <%s> ", opFieldName, bpFieldValue ? "true" : "false");
	return olField;
}

CString StaffGantt::DumpField(CString opFieldName, CTime opFieldValue)
{
	CString olField;
	olField.Format("%s <%s> ", opFieldName, opFieldValue.Format("%d.%m.%Y %H:%M"));
	return olField;
}

CString StaffGantt::DumpField(CString opFieldName, long lpFieldValue)
{
	CString olField;
	olField.Format("%s <%d> ", opFieldName, lpFieldValue);
	return olField;
}

/////////////////////////////////////////////////////////////////////////////
// StaffGantt message handlers helper functions

// Update every necessary GUI elements for each mouse/timer event
void StaffGantt::UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown)
{
    int itemID = GetItemFromPoint(point);

    // Check if we have to change the highlight line in VerticalScale
	CPoint ptScreen = point;
	ClientToScreen(&ptScreen);
	if (WindowFromPoint(ptScreen) == this)	// moving inside GanttChart?
    {
        if (!bmIsMouseInWindow) // first time?
        {
            bmIsMouseInWindow = TRUE;
            pogButtonList->RegisterTimer(this);    // install the timer
        }

        if (itemID != imHighlightLine)  // move to new bar?
		{
            RepaintVerticalScale(itemID, FALSE);
		}
        if (IsBetween(point.x, 0, imVerticalScaleWidth-1))
		{
            UpdateBarStatus(itemID, -1, -1);    // moving on vertical scale, not a bar
		}
        else
        {
            int ilBarno;
            if (!bpIsControlKeyDown)
                ilBarno = GetBarnoFromPoint(itemID, point);
            else
                ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);

			int ilPoolJobBkBar = GetPoolJobFromPoint(itemID, point);
            
			UpdateBarStatus(itemID, ilBarno, ilPoolJobBkBar);
        }
    }
    else if (bmIsMouseInWindow) // moving out of GanttChart? (first time only)
    {
        bmIsMouseInWindow = FALSE;
        RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = -1;       // no more selected line
        UpdateBarStatus(-1, -1, -1);    // no more status if mouse is outside of GanttChart
    }

    // Check if we have to change the icon (when user want to move/resize a bar)
	if (GetCapture() != NULL)
		;	// don't set cursor on drag-and-drop
    else if (bmIsControlKeyDown && !bpIsControlKeyDown)			// no more Ctrl-key?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    else if (!bmIsMouseInWindow || !bpIsControlKeyDown || imCurrentBar == -1)
        ;	// do nothing
    else if (HitTest(itemID, imCurrentBar, point) == HTCAPTION) // body?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
    else                                                        // left/right border?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));

    // Update control key state
    bmIsControlKeyDown = bpIsControlKeyDown;
}

// Update the frame window's status bar
void StaffGantt::UpdateBarStatus(int ipLineno, int ipBarno, int ipPoolJobBkBar)
{
    CString olStatusText;

    if (ipLineno == imHighlightLine && ipBarno == imCurrentBar && ipPoolJobBkBar == imPoolJobBkBar)	// user still be on the old bar?
		return;

    if (ipLineno == -1)	// there is no more bar status?
	{
        olStatusText = "";
	}
    else if (ipBarno == -1 && ipPoolJobBkBar == -1)	// user move outside the bar?
	{
        olStatusText = pomViewer->GetLineText(imGroupno, ipLineno) + CString("     ") + pomViewer->GetLineStatusText(imGroupno, ipLineno);;
	}
	else if(ipBarno == -1 && ipPoolJobBkBar != -1)
	{
		olStatusText = pomViewer->GetLineText(imGroupno, ipLineno) + CString("     ") + pomViewer->GetStatusPoolJobBkBarText(imGroupno, ipLineno, ipPoolJobBkBar);
	}
	else
	{
        olStatusText = pomViewer->GetLineText(imGroupno, ipLineno) + CString("     ") + pomViewer->GetStatusBarText(imGroupno, ipLineno, ipBarno);
	}

	// display status bar
    if (pomStatusBar != NULL)
	{
        pomStatusBar->SetPaneText(0, (LPCSTR)olStatusText);
	}

	// display top scale indicator
	if (pomTimeScale != NULL)
	{
		if (ipLineno == -1 ||	// mouse moved out of every lines?
			ipBarno == -1 && imCurrentBar != -1)	// mouse just leave a bar?
			pomTimeScale->RemoveAllTopScaleIndicator();
		else if (ipBarno != imCurrentBar)
		{
			pomTimeScale->RemoveAllTopScaleIndicator();
			int ilIndicatorCount = pomViewer->GetIndicatorCount(imGroupno,
				ipLineno, ipBarno);
			for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
			{
				STAFF_INDICATORDATA *prlIndicator = pomViewer->GetIndicator(imGroupno,
					ipLineno, ipBarno, ilIndicatorno);
				pomTimeScale->AddTopScaleIndicator(prlIndicator->StartTime,
					prlIndicator->EndTime, prlIndicator->Color);
			}
			pomTimeScale->DisplayTopScaleIndicator();
		}
	}

    // remember the bar and update the status bar (if exist)
    imCurrentBar = ipBarno;
	imPoolJobBkBar = ipPoolJobBkBar;
}

// Start moving/resizing
BOOL StaffGantt::BeginMovingOrResizing(CPoint point)
{
	bmActiveBar = FALSE;

    int itemID = GetItemFromPoint(point);
    int ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
	int ilPoolJobBkBar = GetPoolJobFromPoint(itemID, point);
    UpdateBarStatus(itemID, ilBarno, ilPoolJobBkBar);

	umResizeMode = HTNOWHERE;
    if (ilBarno != -1)
        umResizeMode = HitTest(itemID, ilBarno, point);

    if (umResizeMode != HTNOWHERE)
    {
        SetCapture();
        if (umResizeMode == HTCAPTION)
            SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
        else
            SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));

        // Calculate size of current bar
        STAFF_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, ilBarno);
		if (prlBar)
		{
			CRect rcItem;
			GetItemRect(itemID, &rcItem);
			int left = (int)GetX(prlBar->StartTime);
			int right = (int)GetX(prlBar->EndTime);
			int top = rcItem.top + imGutterHeight + (prlBar->OverlapLevel * imOverlapHeight);
			int bottom = top + imBarHeight;

			rmActiveBar = *prlBar;
			bmActiveBar = TRUE;

			omPointResize = point;
			omRectResize = CRect(left, top, right, bottom);
			CClientDC dc(this);

			// Update the time band (two vertical yellow lines)
			TIMEPACKET olTimePacket;
			olTimePacket.StartTime = GetCTime(omRectResize.left);
			olTimePacket.EndTime = GetCTime(omRectResize.right);
			ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);
			DrawFocusRect(&dc, &omRectResize);	// first time focus rectangle
		}
    }

    return (umResizeMode != HTNOWHERE);
}

// Update the resizing/moving rectangle
BOOL StaffGantt::OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown)
{
    CClientDC dc(this);

    if (bpIsLButtonDown)    // still resizing/moving?
    {
		CRect client;
		GetClientRect(client);
		client.left = imVerticalScaleWidth;
		if (!client.PtInRect(point))
		{
	        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_NO));
			return umResizeMode != HTNOWHERE;
		}
		
		// reset to previous cursor
		if (GetCursor() == AfxGetApp()->LoadStandardCursor(IDC_NO))
		{
			if (umResizeMode == HTCAPTION)
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
			else
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
		}

        DrawFocusRect(&dc, &omRectResize);	// delete previous focus rectangle
        switch (umResizeMode)
        {
        case HTCAPTION: // moving mode
            omRectResize.OffsetRect(point.x - omPointResize.x, 0);
            break;
        case HTLEFT:    // resize left border
            omRectResize.left = min(point.x, omRectResize.right);
            break;
        case HTRIGHT:   // resize right border
            omRectResize.right = max(point.x, omRectResize.left);
            break;
        }
        omPointResize = point;

		// Update the time band (two vertical yellow lines)
		TIMEPACKET olTimePacket;
		olTimePacket.StartTime = GetCTime(omRectResize.left);
		olTimePacket.EndTime = GetCTime(omRectResize.right);
		ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);
	    DrawFocusRect(&dc, &omRectResize);

		// Update the indicator also while moving the bar
		// Be careful, I assume that there will be one and only one indicator for a bar.
		if(bmActiveBar)
		{
			pomTimeScale->RemoveAllTopScaleIndicator();
			pomTimeScale->AddTopScaleIndicator(GetCTime(omRectResize.left),
				GetCTime(omRectResize.right), rmActiveBar.Indicators[0].Color);
			pomTimeScale->DisplayTopScaleIndicator();
		}
    }
    else
    {
		CRect client;
		GetClientRect(client);
		client.left = imVerticalScaleWidth;
		umResizeMode = HTNOWHERE;   // no more moving/resizing
		if (client.PtInRect(point))
		{
			if(bmActiveBar)
			{
				// Also change the associated JobPool if exist (only for JobDetach)
				// this has to be done before changing the detach job times because
				// the pool job in the new pool will be detected by job times

				CTime olNewStart = GetCTime(omRectResize.left);
				CTime olNewEnd = GetCTime(omRectResize.right);
				if(olNewStart != rmActiveBar.StartTime && olNewEnd != rmActiveBar.EndTime)
				{
					// trick to ensure that when the whole bar is shifted, the duration of the bar always remains the same.
					CTimeSpan olDuration = rmActiveBar.EndTime - rmActiveBar.StartTime;
					olNewEnd = olNewStart + olDuration;
				}
				bool blUpdatedGroup = false;
				JOBDATA *prlJob = ogJobData.GetJobByUrno(rmActiveBar.JobUrno);
				if(prlJob != NULL)
				{
					if(pomViewer->IsTeamCompressed(prlJob->Urno))
					{
						// get all conflicts for the jobs that make up this compressed job
						TEAM_DATA *prlTeam = pomViewer->GetTeamByJobUrno(prlJob->Urno);
						if(prlTeam != NULL)
						{
							COMPRESSED_JOBDATA *prlCompressedJob = pomViewer->FindCompressedJob(prlTeam,prlJob->Urno);
							if(prlCompressedJob != NULL)
							{
								int ilNumJobs = prlCompressedJob->OriginalJobs.GetSize();
								if(ilNumJobs > 1)
								{
									blUpdatedGroup = ogDataSet.ChangeGroupJobTimes(rmActiveBar.StartTime, 
																					rmActiveBar.EndTime,
																					olNewStart, 
																					olNewEnd, 
																					prlCompressedJob->OriginalJobs);
								}
							}
						}
					}
				}
				if(!blUpdatedGroup)
				{
					DataSet::ChangeTimeOfJob(rmActiveBar.JobUrno, olNewStart, olNewEnd, TRUE);
				}
				bmActiveBar = FALSE;
				pomTimeScale->RemoveAllTopScaleIndicator();
				imCurrentBar = -1;
			}
		}
		else
		{
			if (bmActiveBar)
			{
		        DrawFocusRect(&dc, &omRectResize);	// delete previous focus rectangle

				// Update the time band (two vertical yellow lines)
				TIMEPACKET olTimePacket;
				olTimePacket.StartTime = rmActiveBar.StartTime;
				olTimePacket.EndTime   = rmActiveBar.EndTime;
				ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);

				bmActiveBar  = FALSE;
				imCurrentBar = -1;
			}
		}

        ReleaseCapture();
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		//SetMarkTime(TIMENULL, TIMENULL);
    }

    return (umResizeMode != HTNOWHERE);
}

// return the item ID of the list box based on the given "point"
int StaffGantt::GetItemFromPoint(CPoint point) const
{
    CRect rcItem;
    for (int itemID = GetTopIndex(); itemID < GetCount(); itemID++)
    {
        GetItemRect(itemID, &rcItem);
        if (rcItem.PtInRect(point))
            return itemID;
    }
    return -1;
}

// Return the bar number of the list box based on the given point
int StaffGantt::GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft, int ipPreRight) const
{
    if (ipLineno == -1) // this checking was added after some bugs found
        return -1;

    // Calculate possible levels for vertical position
    CRect rcItem;
    GetItemRect(ipLineno, &rcItem);
    point.y -= rcItem.top + imGutterHeight;
    int level1 = (point.y < imBarHeight)? 0: ((point.y - imBarHeight) / imOverlapHeight) + 1;
    int level2 = (point.y < 0)? -1: point.y / imOverlapHeight;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-ipPreLeft);
    CTime time2 = GetCTime(point.x+ipPreRight);

    return pomViewer->GetBarnoFromTime(imGroupno, ipLineno, time1, time2, level1, level2);
}

// Return the background bar number of the list box based on the given point
int StaffGantt::GetBkBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft, int ipPreRight) const
{
    if (ipLineno == -1) // this checking was added after some bugs found
        return -1;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-ipPreLeft);
    CTime time2 = GetCTime(point.x+ipPreRight);

    return pomViewer->GetBkBarnoFromTime(imGroupno, ipLineno, time1, time2);
}

// Return the background bar number of the list box based on the given point
int StaffGantt::GetPoolJobFromPoint(int ipLineno, CPoint point, int ipPreLeft, int ipPreRight) const
{
    if (ipLineno == -1) // this checking was added after some bugs found
        return -1;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-ipPreLeft);
    CTime time2 = GetCTime(point.x+ipPreRight);

    return pomViewer->GetPoolJobFromTime(imGroupno, ipLineno, time1, time2);
}

// If dropping directly on a pool job then get the pool job urno
// directly from the pool job background bar, otherwise get the
// pool job urno from the line data. This is required because an emp
// can have several pool jobs.
long StaffGantt::GetPoolJobUrnoFromPoint(CPoint opPoint) const
{
	long llPoolJobUrno = 0L;
	int ilLineno = GetItemFromPoint(opPoint);
	int ilPoolJobBkBarno = -1;
	if(ilLineno != -1)
	{
		if((ilPoolJobBkBarno = GetPoolJobFromPoint(ilLineno, opPoint)) != -1)
		{
			llPoolJobUrno = pomViewer->GetBkBar(imGroupno, ilLineno, ilPoolJobBkBarno)->JobUrno;
		}
		else
		{
			llPoolJobUrno = pomViewer->GetLine(imGroupno, ilLineno)->JobUrno;
		}
	}

	return llPoolJobUrno;
}

bool StaffGantt::GetPoolJobUrnosFromPoint(CPoint opPoint, CDWordArray &ropPoolJobUrnos)
{
	bool blTeamAlloc = false;
	long llPoolJobUrno = GetPoolJobUrnoFromPoint(opPoint);

	TEAM_DATA *prlTeam = pomViewer->GetTeamByPoolJobUrno(llPoolJobUrno);
	if(prlTeam != NULL && prlTeam->IsCompressed)
	{
		// an emp can have more than one pool job, for each emp we need to get only
		// the pool job that is closest to the point where the duty was dropped
		CMapPtrToPtr olUstfList;
		CPoint olDropPoint = (CPoint) opPoint;
	    CTime olDropTime = GetCTime(olDropPoint.x);

		JOBDATA *prlTmpJob, *prlJob;
		int ilNumPoolJobs = prlTeam->PoolJobs.GetSize();
		for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
		{
			prlJob = &prlTeam->PoolJobs[ilPoolJob];
			if(olUstfList.Lookup((void *) prlJob->Ustf, (void *&) prlTmpJob))
			{
				// this job is closer to the drop point than the previous
				if(!IsBetween(olDropTime, prlTmpJob->Acfr, prlTmpJob->Acto) &&
					IsBetween(olDropTime, prlJob->Acfr, prlJob->Acto))
				{
					olUstfList.SetAt((void *) prlJob->Ustf, (void *) prlJob);
				}
			}
			else
			{
				olUstfList.SetAt((void *) prlJob->Ustf, (void *) prlJob);
			}
		}
		for(POSITION rlPos = olUstfList.GetStartPosition(); rlPos != NULL; )
		{
			long llTmpUstf;
			olUstfList.GetNextAssoc(rlPos, (void *&) llTmpUstf, (void *&)prlJob);
			ropPoolJobUrnos.Add(prlJob->Urno);
		}
		blTeamAlloc = true;
	}
	else
	{
		ropPoolJobUrnos.Add(llPoolJobUrno);
	}

	return blTeamAlloc;
}

// Return the hit test area code (determine the location of the cursor).
// Possible codes are HTLEFT, HTRIGHT, HTNOWHERE (out-of-bar), HTCAPTION (bar body)
// 
UINT StaffGantt::HitTest(int ipLineno, int ipBarno, CPoint point)
{
    if (ipLineno == -1 || ipBarno == -1)    // this checking was added after some bugs found
        return HTNOWHERE;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-imBorderPreLeft);
    CTime time2 = GetCTime(point.x+imBorderPreRight);

    // Check against each possible case of hit test areas.
    // Since most people like to drag things to the right, we check right border before left.
    // Be careful, if the bar is very small, the user may not be able to get HTCAPTION or HTLEFT.
    //
    STAFF_BARDATA *prlBar = pomViewer->GetBar(imGroupno, ipLineno, ipBarno);
    if (IsBetween(prlBar->EndTime, time1, time2))
        return HTRIGHT;
    else if (IsBetween(prlBar->StartTime, time1, time2))
        return HTLEFT;
    else if (IsOverlapped(time1, time2, prlBar->StartTime, prlBar->EndTime))
        return HTCAPTION;

    return HTNOWHERE;
}

// Return true if the mouse position in on the vertical scale or a duty bar.
// Also remember the place where the drop happened in "imDropLineno".
BOOL StaffGantt::IsDropOnPoolJob(CPoint point)
{
	int ilLineno = GetItemFromPoint(point);

	if(ilLineno == -1)
	{
		return FALSE;	// cannot find a line of this diagram
	}

	int ilPoolJobBkBarno = GetPoolJobFromPoint(ilLineno, point);

	if(point.x < imVerticalScaleWidth - 2)
	{
		if(pomViewer->PoolJobCount(imGroupno, ilLineno) > 1 && ilPoolJobBkBarno == -1)
		{
			// if emp has more than on pool job, can only drop on a specific pool job
			return FALSE;
		}
		return TRUE;	// mouse is on the vertical scale
	}

	if(ilPoolJobBkBarno != -1)
	{
		return TRUE;	// mouse is on a background bar
	}

	// If it reaches here, mouse is on the background of the GanttChart.
	return FALSE;
}

// Return true if this gate is a stair-case
BOOL StaffGantt::IsStairCase(const CString &ropGate)
{
//	// scan thru all of the meta alloc units
//	int n = ogGateAreas.omMetaAllocUnitList.GetSize();
//    for (int i = 0; i < n; i++)
//    {
//		METAALLOCDATA *prlMeta = &ogGateAreas.omMetaAllocUnitList[i];
//
//		// scan thru all of allocation unit of this meta allocation unit
//        int n = prlMeta->AllocationUnits.GetSize();
//		for (int i = 0; i < n; i++)
//		{
//			ALLOCDATA *prlAlloc = &prlMeta->AllocationUnits[i];
//			if (ropGate == prlAlloc->Alid)
//				return prlAlloc->IsStairCaseGate;
//		}
//	}
	return FALSE;	// not found
}

/////////////////////////////////////////////////////////////////////////////
// StaffGantt -- drag-and-drop functionalities
//
// The user may drag from:
//	- VerticalScale or the DutyBar for create non-duty jobs.
//	- the JobBar for reassignment a job.
//
// and the user will have opportunities to:
//	-- drop from PrePlanTable onto any places in StaffGantt.
//		If the user also press Ctrl, we will open a dialog and confirm before.
//	-- drop from DutyBar from other pools for create job detach.
//		We will ask the user for the period of time and create one JobDetach and another
//		JobPool for the new pool for the specified period of time.
//	-- drop from JobBar in the same pool (reassign job)
//		(Just for JobFlight, JobCciDesk, and JobSpecial.)
//		We will change the field JOUR and the field PKNO to the new staff.
//	-- drop from FlightBar onto the Vertical Scale, the duty bar, or the job bars.
//		(Check the correct GateArea-Pool combination before dropping)
//		We will create a normal flight job for that staff.
//	-- drop from the background area from FlightDetailWindow onto the Vertical Scale,
//		the duty bar, or the job bars.
//		(Check the correct GateArea-Pool combination before dropping)
//		We will create a normal flight job (without demand) for that staff.
//	-- drop from DemandBar onto the Vertical Scale, the duty bar, or the job bars.
//		(Check the correct GateArea-Pool combination before dropping)
//		We will create a normal flight job for that staff to resolve that demand.
//	-- drop from gate (vertical scale) in GateDiagram onto any places in the gantt chart.
//		We will ask the user for the period of time and create many flight jobs for
//		this employees for demands in that Gate.
//	-- drop from CCI-Desk (vertical scale) in CciDiagram (to create JobCci)
//		We will ask the user for the period of time and create a JobCciDesk for
//		this employee. (use the default time as the default).
//
void StaffGantt::DragPoolJobBegin(long lpPoolJobUrno)
{
	int ilNumEmps = omSelectedEmployees.GetSize();
	if(ilNumEmps > 0)
	{
		m_DragDropSource.CreateDWordData(DIT_DUTYBAR, ilNumEmps+1);
		// multi-select
		for(int ilEmp = 0; ilEmp < ilNumEmps; ilEmp++)
		{
			m_DragDropSource.AddDWord(omSelectedEmployees[ilEmp]);
		}
	}
	else
	{

		m_DragDropSource.CreateDWordData(DIT_DUTYBAR, 2);
		m_DragDropSource.AddDWord(lpPoolJobUrno);
	}
	m_DragDropSource.AddDWord(0); // team allocation is false
	m_DragDropSource.BeginDrag();
}

void StaffGantt::DragPoolJobBegin(CCSPtrArray <JOBDATA> &ropPoolJobs, bool bpTeamAlloc)
{
	int ilNumPoolJobs = ropPoolJobs.GetSize();
	if(ilNumPoolJobs > 0)
	{
		m_DragDropSource.CreateDWordData(DIT_DUTYBAR, ilNumPoolJobs+1);
		for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
		{
			m_DragDropSource.AddDWord(ropPoolJobs[ilPoolJob].Urno);
		}
		m_DragDropSource.AddDWord(bpTeamAlloc ? 1 : 0); // team allocation
		m_DragDropSource.BeginDrag();
	}
}

void StaffGantt::DragJobBarBegin(int ipLineno, int ipBarno)
{
	m_DragDropSource.CreateDWordData(DIT_JOBBAR, 1);

	long llJobUrno = pomViewer->GetBar(imGroupno, ipLineno, ipBarno)->JobUrno;
	m_DragDropSource.AddDWord(llJobUrno);
	m_DragDropSource.BeginDrag();
}

LONG StaffGantt::OnDragOver(UINT wParam, LONG lParam)
{
    ::GetCursorPos(&omDropPosition);
	ScreenToClient(&omDropPosition);    
    UpdateGanttChart(omDropPosition, FALSE);

	CCSDragDropCtrl *pomDragDropCtrl = &m_DragDropTarget;

	// Perform a validation on dragged object
	JOBDATA *prlJob;
	EQUDATA *prlEqu;
	JOBDATA *prlJobOfDutyBar;
	FLIGHTDATA *prlFlight;
	DEMANDDATA *prlDemand;
	int ilLineno = -1;

	switch (pomDragDropCtrl->GetDataClass())
	{
	case DIT_SHIFT:
		if (((CViewer *)pomViewer)->GetGroup() == "Pool")
			return 0L;	// drop from PrePlanTable onto any places in StaffChart
		break;
	case DIT_DUTYBAR:
		if ((ilLineno = GetItemFromPoint(omDropPosition)) != -1 &&
			(prlJobOfDutyBar = ogJobData.GetJobByUrno(pomViewer->GetLine(imGroupno, ilLineno)->JobUrno)) != NULL &&
			(prlJob = ogJobData.GetJobByUrno(pomDragDropCtrl->GetDataDWord(0))) != NULL &&
			(CString(prlJob->Alid) != prlJobOfDutyBar->Alid ||
			(ogBasicData.bmDisplayTeams && ogBasicData.GetTeamForPoolJob(prlJob) != ogBasicData.GetTeamForPoolJob(prlJobOfDutyBar))))
			return 0L;	// drop from DutyBar from other pools for create job detach
		break;
	case DIT_JOBBAR:
		if (m_DragDropTarget.GetDataCount() == 1 &&
			IsDropOnPoolJob(omDropPosition) &&
			(ilLineno = GetItemFromPoint(omDropPosition)) != -1 &&
			(prlJobOfDutyBar = ogJobData.GetJobByUrno(pomViewer->GetLine(imGroupno, ilLineno)->JobUrno)) != NULL &&
			(prlJob = ogJobData.GetJobPoolByUrno(pomDragDropCtrl->GetDataDWord(0))) != NULL &&
			(strlen(prlJob->Alid) <= 0 || ogBasicData.IsInPool(prlJob->Aloc,prlJob->Alid,prlJobOfDutyBar->Alid)) &&
			(pomViewer->GetLine(imGroupno, ilLineno)->JobUrno != pomDragDropCtrl->GetDataDWord(1)))
			return 0L;	// drop from JobBar in the same pool (reassign job)
		break;
	case DIT_FLIGHTBAR:
		if (IsDropOnPoolJob(omDropPosition) &&
			(ilLineno = GetItemFromPoint(omDropPosition)) != -1 &&
			(prlJobOfDutyBar = ogJobData.GetJobByUrno(pomViewer->GetLine(imGroupno, ilLineno)->JobUrno)) != NULL &&
			(prlFlight = ogFlightData.GetFlightByUrno(pomDragDropCtrl->GetDataDWord(0))) != NULL)
		{
			CString olAloc = pomDragDropCtrl->GetDataString(2);
			CString olAlid = pomDragDropCtrl->GetDataString(3);

			if(olAlid.IsEmpty() ||
			 ogBasicData.IsInPool(olAloc,olAlid,prlJobOfDutyBar->Alid))
				return 0L;	// drop from FlightBar onto the Vertical Scale, the duty bar, or the job bars.
		}
		break;
	case DIT_FLIGHT:
		if (IsDropOnPoolJob(omDropPosition) &&
			(ilLineno = GetItemFromPoint(omDropPosition)) != -1 &&
			(prlJobOfDutyBar = ogJobData.GetJobByUrno(pomViewer->GetLine(imGroupno, ilLineno)->JobUrno)) != NULL &&
			(prlDemand = ogDemandData.GetDemandByUrno(pomDragDropCtrl->GetDataDWord(0))) != NULL &&
			(strlen(prlDemand->Alid) <= 0 || ogBasicData.IsInPool(prlDemand->Aloc,prlDemand->Alid,prlJobOfDutyBar->Alid)))
			return 0L;	// drop from the background area from FlightDetailWindow
		break;
	case DIT_DEMANDBAR:
		{
		bool blValidDemandFound = false;
		for(int ilD = 0; ilD < m_DragDropTarget.GetDataCount(); ilD++)
		{
			if (IsDropOnPoolJob(omDropPosition) &&
				(ilLineno = GetItemFromPoint(omDropPosition)) != -1 &&
				(prlJobOfDutyBar = ogJobData.GetJobByUrno(pomViewer->GetLine(imGroupno, ilLineno)->JobUrno)) != NULL &&
				(prlDemand = ogDemandData.GetDemandByUrno(pomDragDropCtrl->GetDataDWord(ilD))) != NULL &&
				 (strlen(prlDemand->Alid) <= 0 || ogBasicData.IsInPool(prlDemand->Aloc,prlDemand->Alid,prlJobOfDutyBar->Alid)) &&
				 ogRudData.DemandIsOfType(prlDemand->Urud, PERSONNELDEMANDS))
			return 0L;	// drop from DemandBar onto the Vertical Scale, the duty bar, or the job bars.
		}
		break;
		}
	case DIT_GATE:
		if (IsDropOnPoolJob(omDropPosition) &&
			(ilLineno = GetItemFromPoint(omDropPosition)) != -1 &&
			(prlJobOfDutyBar = ogJobData.GetJobByUrno(pomViewer->GetLine(imGroupno, ilLineno)->JobUrno)) != NULL &&
			 ogBasicData.IsInPool(ALLOCUNITTYPE_GATE,pomDragDropCtrl->GetDataString(0),prlJobOfDutyBar->Alid))
			return 0L;	// drop from gate (vertical scale) in GateDiagram
		break;
	case DIT_EQUIPMENT:
		if ((ilLineno = GetItemFromPoint(omDropPosition)) != -1 &&
			(prlJobOfDutyBar = ogJobData.GetJobByUrno(pomViewer->GetLine(imGroupno, ilLineno)->JobUrno)) != NULL &&
			(prlEqu = ogEquData.GetEquByUrno(pomDragDropCtrl->GetDataDWord(0))) != NULL &&
			 ogBasicData.IsInPool(ALLOCUNITTYPE_EQUIPMENT,prlEqu->Enam,prlJobOfDutyBar->Alid))
			return 0L;
		break;
	case DIT_CCIDESK:
		if (IsDropOnPoolJob(omDropPosition) &&
			(ilLineno = GetItemFromPoint(omDropPosition)) != -1 &&
			(prlJobOfDutyBar = ogJobData.GetJobByUrno(pomViewer->GetLine(imGroupno, ilLineno)->JobUrno)) != NULL &&
			 ogBasicData.IsInPool(ALLOCUNITTYPE_CIC,pomDragDropCtrl->GetDataString(0),prlJobOfDutyBar->Alid))
			return 0L;	// drop from CCI-Desk (vertical scale) in CciDiagram (to create JobCci)
		break;
	case DIT_FID :
		if (IsDropOnPoolJob(omDropPosition) &&
			(ilLineno = GetItemFromPoint(omDropPosition)) != -1 &&
			(prlJobOfDutyBar = ogJobData.GetJobByUrno(pomViewer->GetLine(imGroupno, ilLineno)->JobUrno)) != NULL &&
			(prlDemand = ogDemandData.GetDemandByUrno(pomDragDropCtrl->GetDataDWord(0))) != NULL &&
			 (strlen(prlDemand->Alid) <= 0 || ogBasicData.IsInPool(prlDemand->Aloc,prlDemand->Alid,prlJobOfDutyBar->Alid)))
			return 0L;	// drop from FID - table onto the Vertical Scale, the duty bar, or the job bars.
		break;
	case DIT_CICDEMANDBAR:
		if (IsDropOnPoolJob(omDropPosition) &&
			(ilLineno = GetItemFromPoint(omDropPosition)) != -1 &&
			(prlJobOfDutyBar = ogJobData.GetJobByUrno(pomViewer->GetLine(imGroupno, ilLineno)->JobUrno)) != NULL &&
			(prlDemand = ogDemandData.GetDemandByUrno(pomDragDropCtrl->GetDataDWord(0))) != NULL &&
			 (strlen(prlDemand->Alid) <= 0 || ogBasicData.IsInPool(prlDemand->Aloc,prlDemand->Alid,prlJobOfDutyBar->Alid)))
			return 0L;	// drop from DemandBar onto the Vertical Scale, the duty bar, or the job bars.
		break;
	}
	return -1L;	// cannot accept this object
//----------------------------------------------------------------------
}

LONG StaffGantt::OnDrop(UINT wParam, LONG lParam)
{
	CCSDragDropCtrl *pomDragDropCtrl = &m_DragDropTarget;

	// Perform job creation on dragged object
	DROPEFFECT lpDropEffect = wParam;
	switch (pomDragDropCtrl->GetDataClass())
	{
	case DIT_SHIFT:	// drop from PrePlanTable
		return ProcessDropShifts(pomDragDropCtrl, lpDropEffect);
	case DIT_DUTYBAR:	// drop from DutyBar
		return ProcessDropPoolJob(pomDragDropCtrl, lpDropEffect);
	case DIT_JOBBAR:	// drop from JobBar
		return ProcessDropJobBar(pomDragDropCtrl, lpDropEffect);
	case DIT_FLIGHTBAR:	// drop from FlightBar
		return ProcessDropFlightBar(pomDragDropCtrl, lpDropEffect);
	case DIT_FLIGHT:	// drop from flight (from background of FlightDetailWindow)
		return ProcessDropFlight(pomDragDropCtrl, lpDropEffect);
	case DIT_DEMANDBAR:	// drop from DemandBar
		return ProcessDropDemandBar(pomDragDropCtrl, lpDropEffect);
	
	case DIT_GATE:		// drop from gate (VerticalScale in GateDiagram)
		return ProcessDropAloc(pomDragDropCtrl, lpDropEffect, JOBGATE, ALLOCUNITTYPE_GATE);
		//return ProcessDropGate(pomDragDropCtrl, lpDropEffect);
	case DIT_CCIDESK:	// drop from CCI-Desk (VerticalScale in CciDiagram)
		return ProcessDropAloc(pomDragDropCtrl, lpDropEffect, JOBCCI, ALLOCUNITTYPE_CIC);
		//return ProcessDropCciDesk(pomDragDropCtrl, lpDropEffect);
	case DIT_PST:	// drop from CCI-Desk (VerticalScale in CciDiagram)
		return ProcessDropAloc(pomDragDropCtrl, lpDropEffect, JOBPST, ALLOCUNITTYPE_PST);
	case DIT_FID:		// frop from FID-Table
		return ProcessDropFIDTable(pomDragDropCtrl,lpDropEffect);
	case DIT_CICDEMANDBAR:	// drop from Checkin Counter DemandBar
		return ProcessDropDemandBar(pomDragDropCtrl, lpDropEffect);
	
	
	case DIT_EQUIPMENT:	// drop from Checkin Counter DemandBar
		return ProcessDropEquipment(pomDragDropCtrl, lpDropEffect);
	}

    return -1L;
//----------------------------------------------------------------------
}

LONG StaffGantt::ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	if (lpDropEffect == DROPEFFECT_COPY)	// holding the Control key?
	{
		CAssignmentFromPrePlanTableToPoolDialog dialog(this, popDragDropCtrl,
			pomViewer->GetGroup(imGroupno)->GroupId,
			pomViewer->GetGroup(imGroupno)->Text);
		dialog.DoModal();
	}
	else if (lpDropEffect == DROPEFFECT_MOVE)
	{
		CDWordArray olShiftUrnos;
		for (int ilIndex = 0; ilIndex < popDragDropCtrl->GetDataCount(); ilIndex++)
			olShiftUrnos.Add(popDragDropCtrl->GetDataDWord(ilIndex));
		CString olPool = pomViewer->GetGroup(imGroupno)->GroupId;
		ogDataSet.CreateJobPool(this, olShiftUrnos, olPool);
	}
	return 0L;
}

LONG StaffGantt::ProcessDropPoolJob(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	CTeamDelegationDlg olTeamDelegationDlg;

	CDWordArray olPoolJobUrnos;
	JOBDATA *prlPoolJob;
	EMPDATA *prlEmp;
	CString olEmp, olFunction, olPool, olTeam;
	CTime olStartTime = TIMENULL, olEndTime = TIMENULL;

//	CMapPtrToPtr olEmpsAlreadyAdded;
	int ilNumPoolJobs = popDragDropCtrl->GetDataCount();
	for(int llPoolJob = 0; llPoolJob < ilNumPoolJobs; llPoolJob++)
	{
		if((prlPoolJob = ogJobData.GetJobByUrno(popDragDropCtrl->GetDataDWord(llPoolJob))) == NULL)
		{
			continue;
		}
		if((prlEmp = ogEmpData.GetEmpByPeno(prlPoolJob->Peno)) == NULL)
		{
			continue;
		}

//		void *pvlDummy;
//		if(!olEmpsAlreadyAdded.Lookup((void *)prlEmp->Urno, (void *&) pvlDummy))
		{
			// don't add the emp more than once if they have >1 pool jobs
//			olEmpsAlreadyAdded.SetAt((void *)prlEmp->Urno, NULL);
			olPoolJobUrnos.Add(prlPoolJob->Urno);

			olEmp = ogEmpData.GetEmpName(prlEmp);
			olTeamDelegationDlg.omEmpList.Add(olEmp);

			olFunction = ogBasicData.GetFunctionForPoolJob(prlPoolJob);
			olTeamDelegationDlg.omFunctionList.Add(olFunction);
		}

		if(olStartTime == TIMENULL || olStartTime > prlPoolJob->Acfr)
		{
			olStartTime = prlPoolJob->Acfr;
		}
		if(olEndTime == TIMENULL || olEndTime < prlPoolJob->Acto)
		{
			olEndTime = prlPoolJob->Acto;
		}

		// initialize to the pool and team of the dragged pool job(s)
		olPool = prlPoolJob->Alid;
		olTeam = ogBasicData.GetTeamForPoolJob(prlPoolJob);
	}
	if(olPoolJobUrnos.GetSize() > 0)
	{
		if(pomViewer->omGroupBy == GROUP_BY_POOL)
		{
			olPool = pomViewer->GetGroup(imGroupno)->GroupId;
		}
		else if(pomViewer->omGroupBy == GROUP_BY_TEAM)
		{
			olTeam = pomViewer->GetGroup(imGroupno)->GroupId;
		}

		JOBDATA *prlTargetPoolJob = NULL;
		if((prlTargetPoolJob = ogJobData.GetJobByUrno(GetPoolJobUrnoFromPoint(omDropPosition))) != NULL)
		{
			// dropped directly onto a group button
			if(pomViewer->omGroupBy == GROUP_BY_POOL)
			{
				olPool = pomViewer->GetGroup(imGroupno)->GroupId;
				olTeam = ogBasicData.GetTeamForPoolJob(prlTargetPoolJob);
			}
			else if(pomViewer->omGroupBy == GROUP_BY_TEAM)
			{
				olTeam = pomViewer->GetGroup(imGroupno)->GroupId;
				olPool = "";
			}
		}
		else
		{
			// dropped onto an employee (vertical scale)
			int ilLineno;
			if ((ilLineno = GetItemFromPoint(omDropPosition)) != -1 &&
				(prlTargetPoolJob = ogJobData.GetJobByUrno(pomViewer->GetLine(imGroupno, ilLineno)->JobUrno)) != NULL)
			{
				olTeam = ogBasicData.GetTeamForPoolJob(prlTargetPoolJob);
				olPool = prlTargetPoolJob->Alid;
			}
		}

		olTeamDelegationDlg.omPool = olPool;
		olTeamDelegationDlg.omTeam = olTeam;
		olTeamDelegationDlg.omFrom = olStartTime;
		olTeamDelegationDlg.omTo = olEndTime;

		if(olTeamDelegationDlg.DoModal() == IDOK)
		{
			if(ogBasicData.bmDisplayTeams)
			{
				ogDataSet.CreateGroupDelegation(this,olTeamDelegationDlg.omTeam,olTeamDelegationDlg.omPool,
													olPoolJobUrnos,olTeamDelegationDlg.omFunctionList,
													olTeamDelegationDlg.omFrom,olTeamDelegationDlg.omTo);
			}
			else
			{
				ogDataSet.CreateJobDetachFromDuty(this,olPoolJobUrnos,olTeamDelegationDlg.omPool,olTeamDelegationDlg.omFrom,olTeamDelegationDlg.omTo);
			}
		}
	}
	return 0L;
}

LONG StaffGantt::ProcessDropJobBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	long llJobUrno = popDragDropCtrl->GetDataDWord(0);
	long llPoolJobUrno = GetPoolJobUrnoFromPoint(omDropPosition);

	CCSPtrArray <JOBDATA> olNewPoolJobs;
	bool blHasCompressedPoolJobs = pomViewer->GetPoolJobsForCompressedPoolJob(llPoolJobUrno, olNewPoolJobs);

	CCSPtrArray <JOBDATA> olCurrentJobs;
	if(pomViewer->GetJobsForCompressedJob(llJobUrno, olCurrentJobs))
	{
		if(blHasCompressedPoolJobs)
		{
			bool blCanReassign = true;
			JOBDATA *prlJob = NULL;
			int ilNumJobs = olCurrentJobs.GetSize();
			for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
			{
				prlJob = &olCurrentJobs[ilJ];
				if(!strcmp(prlJob->Jtco,JOBBREAK))
				{
					// error - can't drop compressed break jobs
					MessageBox(GetString(IDS_COMPRESSEDBREAK),GetString(IDS_REASSIGNTEAMERR),MB_ICONSTOP);
					blCanReassign = false;
					break;
				}
			}

			if(blCanReassign)
			{
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				ogDataSet.ReassignFlightForGroup(this, olCurrentJobs, olNewPoolJobs);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
			}
		}
		else
		{
			// error - can't drop compressed jobs onto an uncompressed pool job
			MessageBox(GetString(IDS_UNCOMPRESSEDTARGET),GetString(IDS_REASSIGNTEAMERR),MB_ICONSTOP);
		}
	}
	else if(blHasCompressedPoolJobs)
	{
		// error - can't drop single job onto a compressed pool job - or select emp ?
		MessageBox(GetString(IDS_UNCOMPRESSEDSOURCE),GetString(IDS_REASSIGNTEAMERR),MB_ICONSTOP);
	}
	else
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		ogDataSet.ReassignJob(this, llJobUrno, llPoolJobUrno);
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
	return 0L;
}

LONG StaffGantt::ProcessDropEquipment(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	
	
	
	LONG llRc = -1;
	long llEquUrno = popDragDropCtrl->GetDataDWord(0);
	EQUDATA *prlEqu = ogEquData.GetEquByUrno(llEquUrno);
	if(prlEqu != NULL)
	{
		CDWordArray olPoolJobUrnos;
		GetPoolJobUrnosFromPoint(omDropPosition, olPoolJobUrnos);

		int ilNumPJ = olPoolJobUrnos.GetSize();
		if(ilNumPJ > 0)
		{
			CTime olBegin = TIMENULL, olEnd = TIMENULL;
			CString olEmpList;
			for(int ilPJ = 0; ilPJ < ilNumPJ; ilPJ++)
			{
				JOBDATA *prlPJ = ogJobData.GetJobByUrno(olPoolJobUrnos[ilPJ]);
				if(prlPJ != NULL)
				{
					if(olBegin == TIMENULL || prlPJ->Acfr < olBegin)
					{
						olBegin = prlPJ->Acfr;
					}
					if(olEnd == TIMENULL || prlPJ->Acto > olEnd)
					{
						olEnd = prlPJ->Acto;
					}

					EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlPJ->Ustf);
					CString olName;
					if(prlEmp != NULL)
					{
						olName = ogEmpData.GetEmpName(prlEmp);
						olEmpList += olName + CString("\n");
					}
				}
			}

			// get the duration of a fast link job
			DChange olJobChangeDialog(this,olEmpList,olBegin,olEnd);
			olJobChangeDialog.omCaption.Format("%s: %s",GetString(IDS_FASTLINKCAPTION),prlEqu->Enam);
			if(olJobChangeDialog.DoModal() == IDOK)
			{
				ogDataSet.CreateEquipmentFastLink(this, olPoolJobUrnos, llEquUrno, olJobChangeDialog.m_Acfr, olJobChangeDialog.m_Acto);
				llRc = 0L;
			}
		}	
	}
    return llRc;
}

EMPDATA *prlEmp;

// flight dropped from gate gantt
LONG StaffGantt::ProcessDropFlightBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	
	
	prlEmp=NULL;
	
	long llPoolJobUrno = GetPoolJobUrnoFromPoint(omDropPosition);
 	JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(llPoolJobUrno);

	
	if(prlPoolJob == NULL)
		return -1L;

	
	prlEmp = ogEmpData.GetEmpByPeno(prlPoolJob->Peno);
	
	
	long llFlightUrno = popDragDropCtrl->GetDataDWord(0);
	long llRotationUrno = popDragDropCtrl->GetDataDWord(1);
	CString olAloc = popDragDropCtrl->GetDataString(2);
	CString olAlid = popDragDropCtrl->GetDataString(3);

	CDWordArray olPoolJobUrnos;
	bool blTeamAlloc = GetPoolJobUrnosFromPoint(omDropPosition, olPoolJobUrnos);
	
	if(olAloc == ALLOCUNITTYPE_REGN && !ogFlightData.IsThirdPartyFlight(ogFlightData.GetFlightByUrno(llFlightUrno)))
	{
		// registration demand
		ogDataSet.CreateJobRegnFromDuty(this,olPoolJobUrnos,ogFlightData.GetFlightByUrno(llFlightUrno));
	}
	else
	{
		int ilReturnFlightType = (popDragDropCtrl->GetDataCount() >= 6) ? (int) popDragDropCtrl->GetDataDWord(5) : ID_NOT_RETURNFLIGHT;
		ogDataSet.CreateJobFlightFromDuty(this, olPoolJobUrnos, llFlightUrno, olAloc, olAlid, blTeamAlloc, NULL, 
			TIMENULL, TIMENULL, true, llRotationUrno, true, false, 0L, ilReturnFlightType);
	}
	return 0L;
}

LONG StaffGantt::ProcessDropFlight(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
//	long llDemandUrno = popDragDropCtrl->GetDataDWord(0);
//	long llPoolJobUrno = GetPoolJobUrnoFromPoint(omDropPosition);
//	ogDataSet.CreateJobFlightWithoutDemandFromDuty(this, llPoolJobUrno, llPoolJobUrno);
	return 0L;
}




LONG StaffGantt::ProcessDropDemandBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	
   dropfrom =true;

	prlEmp=NULL;
	
	long llPoolJobUrno = GetPoolJobUrnoFromPoint(omDropPosition);
 	JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(llPoolJobUrno);

	
	if(prlPoolJob == NULL)
		return -1L;

	
	prlEmp = ogEmpData.GetEmpByPeno(prlPoolJob->Peno);
	
	
	CDWordArray olPoolJobUrnos;
	bool blTeamAlloc = GetPoolJobUrnosFromPoint(omDropPosition, olPoolJobUrnos);
	CMapPtrToPtr olFlightMap;

	CCSPtrArray <DEMANDDATA> olFlightDemands;
	DEMANDDATA *prlDemand = NULL;
	int ilNumDemands = popDragDropCtrl->GetDataCount();
	if(ilNumDemands > 1 && olPoolJobUrnos.GetSize() == 1)
	{
		// many demands dropped on a single employee
		for(int ilD = 0; ilD < ilNumDemands; ilD++)
		{
			if ((prlDemand = ogDemandData.GetDemandByUrno(popDragDropCtrl->GetDataDWord(ilD))) != NULL &&
				 (strlen(prlDemand->Alid) <= 0 || ogBasicData.IsInPool(prlDemand->Aloc,prlDemand->Alid,prlPoolJob->Alid)) &&
				 ogRudData.DemandIsOfType(prlDemand->Urud, PERSONNELDEMANDS))
			{
				olFlightDemands.Add(prlDemand);
			}
		}
	}

	if(olFlightDemands.GetSize() > 0)
	{
		// many demands dropped on a single employee
		ogDataSet.CreateMultiJobsForSingleEmp(this, olPoolJobUrnos[0], olFlightDemands);
	}
	else
	{
		// either many demands dropped on many emps (team) or single demand dropped on single emp
		for(int ilD = 0; ilD < ilNumDemands; ilD++)
		{
			if ((prlDemand = ogDemandData.GetDemandByUrno(popDragDropCtrl->GetDataDWord(ilD))) != NULL &&
				 (strlen(prlDemand->Alid) <= 0 || ogBasicData.IsInPool(prlDemand->Aloc,prlDemand->Alid,prlPoolJob->Alid)) &&
				 ogRudData.DemandIsOfType(prlDemand->Urud, PERSONNELDEMANDS))
			{
				if (prlDemand->Dety[0] == CCI_DEMAND)
				{
					
					
					ogDataSet.CreateCommonCheckinCounterJobs(this,olPoolJobUrnos,prlDemand->Urno);
				}
				else if(!strcmp(prlDemand->Aloc,ALLOCUNITTYPE_REGN) && !ogFlightData.IsThirdPartyFlight(ogFlightData.GetFlightByUrno(ogDemandData.GetFlightUrno(prlDemand))))
				{
					ogDataSet.CreateJobRegnFromDuty(this,olPoolJobUrnos,NULL,NULL,prlDemand);
				}
				else if (ogDemandData.IsFlightDependingDemand(prlDemand->Dety))
				{
					olFlightDemands.Add(prlDemand);
				}
				else
				{
					ogDataSet.CreateFlightIndependentJobs(this,olPoolJobUrnos,prlDemand->Urno);
				}
			}
		}

		if(olFlightDemands.GetSize() > 0)
		{
			ogDataSet.CreateJobFlightFromDuty(this, olPoolJobUrnos, 0L, "", "", blTeamAlloc, &olFlightDemands);
		}
	}	
	return 0L;
}

LONG StaffGantt::ProcessDropAloc(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect, CString opJobType, CString opAloc)
{
	CString olAlid = popDragDropCtrl->GetDataString(0);

	CDWordArray olPoolJobUrnos;
	long llPoolJobUrno = GetPoolJobUrnoFromPoint(omDropPosition);

	bool blTeamAlloc = false;
	TEAM_DATA *prlTeam = pomViewer->GetTeamByPoolJobUrno(llPoolJobUrno);
	if(prlTeam != NULL && prlTeam->IsCompressed)
	{
		// an emp can have more than one pool job, for each emp we need to get only
		// the pool job that is closest to the point where the duty was dropped
		CMapPtrToPtr olUstfList;
		CPoint olDropPoint = (CPoint) omDropPosition;
	    CTime olDropTime = GetCTime(olDropPoint.x);

		JOBDATA *prlTmpJob, *prlJob;
		int ilNumPoolJobs = prlTeam->PoolJobs.GetSize();
		for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
		{
			prlJob = &prlTeam->PoolJobs[ilPoolJob];
			if(olUstfList.Lookup((void *) prlJob->Ustf, (void *&) prlTmpJob))
			{
				// this job is closer to the drop point than the previous
				if(!IsBetween(olDropTime, prlTmpJob->Acfr, prlTmpJob->Acto) &&
					IsBetween(olDropTime, prlJob->Acfr, prlJob->Acto))
				{
					olUstfList.SetAt((void *) prlJob->Ustf, (void *) prlJob);
				}
			}
			else
			{
				olUstfList.SetAt((void *) prlJob->Ustf, (void *) prlJob);
			}
		}
		for(POSITION rlPos = olUstfList.GetStartPosition(); rlPos != NULL; )
		{
			long llTmpUstf;
			olUstfList.GetNextAssoc(rlPos, (void *&) llTmpUstf, (void *&)prlJob);
			olPoolJobUrnos.Add(prlJob->Urno);
		}
		blTeamAlloc = true;
	}
	else
	{
		olPoolJobUrnos.Add(llPoolJobUrno);
	}

	int ilNumPoolJobs = olPoolJobUrnos.GetSize();
	if(ilNumPoolJobs > 0)
	{
		CTime olBegin = TIMENULL, olEnd = TIMENULL;
		CStringArray olEmpList;

		JOBDATA *prlJob = NULL;
		for(int ilC = 0; ilC < ilNumPoolJobs; ilC++)
		{
			if((prlJob = ogJobData.GetJobByUrno(olPoolJobUrnos[ilC])) != NULL)
			{
				if(olBegin == TIMENULL || prlJob->Acfr < olBegin)
				{
					olBegin = prlJob->Acfr;
				}
				if(olEnd == TIMENULL || prlJob->Acto > olEnd)
				{
					olEnd = prlJob->Acto;
				}
				EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);
				if (prlEmp != NULL) 
				{
					olEmpList.Add(ogEmpData.GetEmpName(prlEmp));
				}
			}
		}

		CString olEmpName = olEmpList.GetSize() > 0 ? olEmpList[0] : "";
		DGateJob olGateJobDialog(this,opAloc,olAlid,olEmpName,olBegin);
		int ilDuration;
		bgModal++;
		if (olGateJobDialog.DoModal(olBegin,ilDuration) == IDOK)
		{
			olBegin = olGateJobDialog.m_Hour;
			ilDuration = olGateJobDialog.m_Duration;
			ogDataSet.CreateNormalJobs(this, opJobType, olPoolJobUrnos, opAloc, olAlid, olBegin,olBegin+(ilDuration*60), blTeamAlloc);
		}
		bgModal--;
	}
	return 0L;
}


//LONG StaffGantt::ProcessDropGate(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
//{
//	CString olGate = m_DragDropTarget.GetDataString(0);
//	long llDutyJobUrno = GetPoolJobUrnoFromPoint(omDropPosition);
//	JOBDATA *prlJob = ogJobData.GetJobByUrno(llDutyJobUrno);
//	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);
//	if (prlEmp != NULL) 
//	{
//		CWnd *polWnd = GetParent()->GetParent()->GetParent();
//		CString olEmpNam = ogEmpData.GetEmpName(prlEmp);
//		CTime olStartTime = ogDataSet.GetTimeForNextJob(llDutyJobUrno);
//		DGateJob olGateJobDialog(polWnd, olGate, olEmpNam, olStartTime);
//		int ilDuration;
//		if (olGateJobDialog.DoModal(olStartTime,ilDuration) == IDOK)
//		{
//			olStartTime = olGateJobDialog.m_Hour;
//			ilDuration = olGateJobDialog.m_Duration;
//			if (IsStairCase(olGate))
//			{
//				ogDataSet.CreateJobStairCaseGateFromDuty(this, olGate, llDutyJobUrno,
//					olStartTime, ilDuration);
//			}
//			else	// create normal flight jobs for all flights in timeframe
//			{
//				CDWordArray olPoolJobUrnos;
//				olPoolJobUrnos.Add(llDutyJobUrno);
//				ogDataSet.CreateNormalJobs(this, JOBGATE, olPoolJobUrnos, ALLOCUNITTYPE_GATE, olGate, olStartTime,olStartTime+(ilDuration*60), bmTeamAlloc);
//				//ogDataSet.CreateGateJob(this, olPoolJobUrnos, olStartTime,olStartTime+(ilDuration*60), olGate);
//			}
//		}
//	}
//
//	return 0L;
//}
//
//LONG StaffGantt::ProcessDropCciDesk(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
//{
//	CString olCciDesk = m_DragDropTarget.GetDataString(0);
//	CDWordArray olPoolJobUrnos;
//	olPoolJobUrnos.Add(GetPoolJobUrnoFromPoint(omDropPosition));
//	JOBDATA *prlJob = ogJobData.GetJobByUrno(olPoolJobUrnos[0]);
//	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);
//	
//	BOOL blIsInVerticalScale = (omDropPosition.x < imVerticalScaleWidth - 2);
//
//	// Confirm this assignment from the user with the CCI-Desk dialog
//	CCciDeskDialog dialog(this, omDisplayStart);
//	dialog.m_CciName = ogEmpData.GetEmpName(prlEmp);
//	dialog.m_Desk = olCciDesk;
//	if (!blIsInVerticalScale)	// change begin time to the place where mouse release
//		dialog.m_Hour = GetCTime(omDropPosition.x);
//	CCSPtrArray<JOBDATA> olJobs;
//	ogJobData.GetJobsByAlid(olJobs, olCciDesk, ALLOCUNITTYPE_CIC);
//	for (int ilJobno = 0; ilJobno < olJobs.GetSize(); ilJobno++)
//	{
//		CTime olEndTime = olJobs[ilJobno].Acto;
//		dialog.m_Hour = max(dialog.m_Hour, olEndTime);
//	}
//	dialog.m_Min = 120;			// default time is 2 hours
//	dialog.m_Duration = 2;
//	if (dialog.DoModal() != IDOK)
//		return -1L;
//
//	CTimeSpan olDuration = CTimeSpan(0, dialog.m_Min / 60, dialog.m_Min % 60, 0);
//	ogDataSet.CreateJobCciFromDuty(this, olPoolJobUrnos, olCciDesk,
//		dialog.m_Hour, dialog.m_Hour + olDuration);
//
//	return 0L;
//}

LONG StaffGantt::ProcessDropFIDTable(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	long llDemandUrno = popDragDropCtrl->GetDataDWord(0);
	long llPoolJobUrno = GetPoolJobUrnoFromPoint(omDropPosition);

	DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(llDemandUrno);
	if(prlDem == NULL)
		return -1L;
 	JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(llPoolJobUrno);
	if(prlPoolJob == NULL)
		return -1L;
	if(strlen(prlDem->Alid) > 0 && !ogBasicData.IsInPool(prlDem->Aloc,prlDem->Alid,prlPoolJob->Alid))
		return -1L;

	CDWordArray olPoolJobUrnos;
	olPoolJobUrnos.Add(llPoolJobUrno);
	ogDataSet.CreateFlightIndependentJobs(this, olPoolJobUrnos, llDemandUrno);
	
	return 0L;
}

void StaffGantt::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CWnd::OnVScroll(nSBCode, nPos, pScrollBar);
	imTopIndex = GetTopIndex();
}

void StaffGantt::SetTopIndex(int ipGanttTopIndex) 
{
	imTopIndex = ipGanttTopIndex;
	if (GetTopIndex() != imTopIndex)
		CListBox::SetTopIndex(imTopIndex);
}


int StaffGantt::OnToolHitTest(CPoint point, TOOLINFO* pTI) const
{
	static int   ilPrevLine =-1;
	int	ilLine = GetItemFromPoint(point);
	STAFF_LINEDATA *prlLine = NULL;

	static bool blFirst = true;

	if(ilLine > -1 && ilLine < pomViewer->GetLineCount(imGroupno) && 
		point.x < imVerticalScaleWidth && 
		(prlLine = pomViewer->GetLine(imGroupno, ilLine)) != NULL)
	{
		int ilDoubleTourRectOffset = 0;
		CString olText;
		CRect olLineRect;
	    GetItemRect(imHighlightLine, &olLineRect);
		if(!prlLine->DoubleTourStudent.IsEmpty() && IsBetween(point.x, ilDoubleTourRectOffset+2, ilDoubleTourRectOffset+13) && IsBetween(point.y, olLineRect.top+2, olLineRect.top+13))
		{
			if (blFirst)
			{
				blFirst = false;
				ilPrevLine = -1;
				return -1;
			}
			olText = prlLine->DoubleTourStudent;
			pTI->lpszText = new char[olText.GetLength()+1];
			pTI->uFlags = TTF_NOTBUTTON | TTF_IDISHWND;
			strcpy(pTI->lpszText, olText);
			pTI->uId = (UINT)m_hWnd;
			pTI->hwnd = ::GetParent(m_hWnd);
			return 1;
		}
		else
		{
			blFirst = true;
			if (ilLine != ilPrevLine)
			{
				ilPrevLine = ilLine;
				return -1;
			}

			SHIFTTYPEDATA *prlShiftType = ogShiftTypes.GetShiftTypeByCode(prlLine->ShiftStid);
			olText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_STAFFTOOLTIP, "%s%s%s%s%s%s", 
											prlLine->EmpName, prlLine->ShiftStid, prlLine->EmpRank, prlLine->ShiftTmid, 
											(prlShiftType != NULL) ? prlShiftType->Bsdn : "", prlLine->MultiFunctions);
			if(olText.IsEmpty())
				olText = prlLine->Text + " " + prlLine->MultiFunctions;

			pTI->lpszText = new char[olText.GetLength()+1];
			pTI->uFlags = TTF_NOTBUTTON | TTF_IDISHWND;
			strcpy(pTI->lpszText, olText);
			pTI->uId = (UINT)m_hWnd;
			pTI->hwnd = ::GetParent(m_hWnd);
			return 1;
		}
	}
	else
	{
		CString olText;
		blFirst = true;
		if (ilLine != ilPrevLine)
		{
			ilPrevLine = ilLine;
			return -1;
		}
		int ilBarno = GetBarnoFromPoint(ilLine, point, imBorderPreLeft, imBorderPreRight);
		int ilPoolJobBkBar = GetPoolJobFromPoint(ilLine, point);
		if (ilLine!=-1&&ilBarno!=-1&&ilPoolJobBkBar!=-1)
		{
			olText = pomViewer->GetStatusBarText(imGroupno, ilLine, ilBarno);
			if(olText.IsEmpty())
				olText = prlLine->Text + " " + prlLine->MultiFunctions;

			pTI->lpszText = new char[olText.GetLength()+1];
			pTI->uFlags = TTF_NOTBUTTON | TTF_IDISHWND;
			strcpy(pTI->lpszText, olText);
			pTI->uId = (UINT)m_hWnd;
			pTI->hwnd = ::GetParent(m_hWnd);
			return 1;
		}
	}
	return -1;
}

JOBDATA *StaffGantt::GetJob(long lpJobUrno)
{
	JOBDATA *prlJob = ogJobData.GetJobByUrno(lpJobUrno);
	if(prlJob != NULL && pomViewer->IsTeamCompressed(lpJobUrno))
	{
		if(!strcmp(prlJob->Jtco,JOBPOOL))
		{
			prlJob = pomViewer->GetCompressedPoolJobByNormalPoolJobUrno(lpJobUrno);
		}
		else
		{
			prlJob = pomViewer->GetCompressedJobByNormalJobUrno(lpJobUrno);
		}
	}
	return prlJob;
}

void StaffGantt::DeselectEmployees()
{
	if(omSelectedEmployees.GetSize() > 0)
	{
		omSelectedEmployees.RemoveAll();

		// need to update all gantt chart vertical scales not just this one
		LONG llParam = 0L;
		pomViewer->pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_REPAINTVERTICALSCALES, llParam);
		//RepaintVerticalScale(-1,true);
	}
}

void StaffGantt::SelectEmployee(int ipGroupno, int ipHighlightLine)
{
	if(!EmployeeIsSelected(ipGroupno, ipHighlightLine))
	{
		STAFF_LINEDATA *prlActiveLine = pomViewer->GetLine(ipGroupno, ipHighlightLine);
		if(prlActiveLine != NULL)
		{
			omSelectedEmployees.Add(prlActiveLine->JobUrno);
		}
	}
}

bool StaffGantt::EmployeeIsSelected(int ipGroupno, int ipHighlightLine)
{
	bool blEmployeeIsSelected = false;
	STAFF_LINEDATA *prlActiveLine = pomViewer->GetLine(ipGroupno, ipHighlightLine);
	if(prlActiveLine != NULL)
	{
		int ilNumEmps = omSelectedEmployees.GetSize();
		for(int ilEmp = 0; ilEmp < ilNumEmps; ilEmp++)
		{
			if(omSelectedEmployees[ilEmp] ==  prlActiveLine->JobUrno)
			{
				blEmployeeIsSelected = true;
				break;
			}
		}
	}

	return blEmployeeIsSelected;
}


