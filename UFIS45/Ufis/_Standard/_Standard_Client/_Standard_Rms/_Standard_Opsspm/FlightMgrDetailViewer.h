// FlightMgrDetailViewer.h : header file
//

#ifndef _FMDV_H_
#define _FMDV_H_



struct FMDW_ASSIGNMENT  // each assignment record
{
    CString Fnum;   // assigned to flightnumber
	long FlightUrno;      // flight Urno 
	long JobUrno;
	CTime Acfr;
};

struct FMDW_LINEDATA
{
    long ShiftUrno; // from SHFCKI.URNO
    long DutyUrno;  // from JOBPOOL
    CString Peno;   // staff ID of this record
    CString Name;   // staff name (both Lnam, Fnam)
    CString Tmid;   // the team that this staff is in
    CString Sfca;   // ShiftCode for this staff
	CTime	Acfr;	// Actual begin of job
	CTime	Acto;   // Actual end of job

    CCSPtrArray<FMDW_ASSIGNMENT> Assignment;
};

/////////////////////////////////////////////////////////////////////////////
// FlightMgrDetailViewer

class FlightMgrDetailViewer: public CViewer
{
// Constructions
public:
    FlightMgrDetailViewer();
    ~FlightMgrDetailViewer();

    void Attach(CTable *popTable,CTime opStartTime,CTime opEndTime,CString pcpAlid);

// Internal data processing routines
private:
    BOOL IsPassFilter(CTime opAcfr,CTime opActo);

    void MakeLines();
    void MakeAssignments();
    void MakeLine(JOBDATA *prpJob);
	int CompareLine(FMDW_LINEDATA &rrpLine1,FMDW_LINEDATA &rrpLine2);


// Operations
public:
    void DeleteAll();
    int CreateLine(FMDW_LINEDATA &rrpLine);
    void DeleteLine(int ipLineno);
    int CreateAssignment(int ipLineno, FMDW_ASSIGNMENT *prpAssignment);
	void DeleteAssignmentJob(long lpJobUrno);
    void DeleteAssignment(int ipLineno, int ipAssignmentno);

	// Window refreshing routines
public:
    void UpdateDisplay();
    CString Format(FMDW_LINEDATA *prpLine);
	void ProcessJobDelete(JOBDATA *prpJob);
	BOOL FindItemByUrno(JOBDATA *prpJob,int& ripItem, int& ripAssignment);
	BOOL FindLine(CString opPkno, int &rilLineno);
    // Attributes used for filtering condition
private:
public:
	CTime omStartTime;
	CTime omEndTime;
	CString pcmAlid;

// Attributes
public:
    CTable *pomTable;
    CCSPtrArray<FMDW_LINEDATA> omLines;

// Methods which handle changes (from Data Distributor)
public:
    void ProcessJobNew(JOBDATA *prpJob);
};

#endif  // __PPVIEWER_H_
