#if !defined(AFX_ABSENCE_H__C388A214_4856_4D6D_8235_9D83F12C0960__INCLUDED_)
#define AFX_ABSENCE_H__C388A214_4856_4D6D_8235_9D83F12C0960__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Absence.h : header file


/////////////////////////////////////////////////////////////////////////////
// Absence dialog

class Absence : public CDialog
{
// Construction
public:
	Absence(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Absence)
	enum { IDD = IDD_DIALOG1 };
	CEdit	m_rema;
	CListBox	m_list;
	CEdit	m_edit4;
	CEdit	m_edit3;
	CEdit	m_edit2;
	CEdit	m_edit1;
	CComboBox	m_com;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Absence)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Absence)
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual void OnOK();
	afx_msg void OnSelchangeList1();
	afx_msg void OnDel();
	afx_msg void OnUpdate();
	afx_msg void OnDblclkList1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ABSENCE_H__C388A214_4856_4D6D_8235_9D83F12C0960__INCLUDED_)
