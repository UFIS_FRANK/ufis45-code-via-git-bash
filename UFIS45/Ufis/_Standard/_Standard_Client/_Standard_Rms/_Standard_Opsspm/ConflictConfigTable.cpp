// ConflictConfigTable.cpp - Class for configuring the conflict checking
//
// Written by:
// Manfred Cuntz 09. June 1996

#include <time.h>
#include <stdlib.h>
#include <stdafx.h>
#include <CCSPtrArray.h>
#include <ccsglobl.h>
#include <CCSCedaData.h>
#include <ufis.h>
#include <ccsddx.h>
#include <ccsobj.h>
#include <resource.h>
#include <CCSCedaCom.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <table.h>
#include <ConflictConfigTable.h>
#include <BasicData.h>
#include <CedaCfgData.h>
#include <conflict.h>

extern bool BGS;

CCSPtrArray <CONFLICTCONFDATA>  ogConflictConfData;

/////////////////////////////////////////////////////////////////////////////
// ConfConflicts dialog

ConfConflicts::ConfConflicts(CWnd* pParent /*=NULL*/)
	: CDialog(ConfConflicts::IDD, pParent)
{
	//{{AFX_DATA_INIT(ConfConflicts)
	m_DisplayEnabledFirst = FALSE;
	//}}AFX_DATA_INIT

	// save current conflict values
	int ilSize = ogConflictConfData.GetSize();
	for (int ilC = 0; ilC < ilSize; ilC++)
	{
		omConflictConfData.NewAt(ilC,ogConflictConfData.ElementAt(ilC));
	}

	// use single selection
	omTable.SetSelectMode(0);
}

ConfConflicts::~ConfConflicts(void)
{
	TRACE("ConfConflicts::~ConfConflicts\n");
	// delete saved conflict values
	omConflictConfData.DeleteAll();
}

void ConfConflicts::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ConfConflicts)
	DDX_Check(pDX, IDC_ENABLEDFIRST, m_DisplayEnabledFirst);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(ConfConflicts, CDialog)
	//{{AFX_MSG_MAP(ConfConflicts)
	ON_BN_CLICKED(IDC_UPDATE, OnUpdate)
	ON_BN_CLICKED(IDC_SAVE, OnSave)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnLButtonDblClk)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDC_LOADFROMDEFAULT, OnLoadFromDefault)
	ON_BN_CLICKED(IDC_SAVEASDEFAULT, OnSaveAsDefault)
	ON_BN_CLICKED(IDC_ENABLEDFIRST, OnDisplayEnabledFirst)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
 

// ConfConflicts: Load conflict checking configuration data
void ConfConflicts::UpdateDisplay()
{
	
	
	// read conflict configuration data from DOS file
	// will be changed to database later and moved 
	// that it will be read at program start.

	// Load filtered and sorted data into the table content
	omTable.ResetContent();
	omTable.SetFieldNames("STATUS,TYPE,NAME,WEIGHT,TIME,NCOL,CCOL,DSCR");
	//omTable.SetHeaderFields("Status|Konflict Art|Min.|Gew.|Color 1|Color 2|Beschreibung");
	
	if (BGS)
    	omTable.SetHeaderFieldsBGS(GetString(IDS_CONFLICTTABHEADER));

	else
    	omTable.SetHeaderFields(GetString(IDS_CONFLICTTABHEADER));



	omTable.SetFormatList("3|30|4|4|4|8|8|80");
	omTable.SetTypeList("TEXT|TEXT|TEXT|TEXT|TEXT|COLOR|COLOR|TEXT");

	CStringArray olNonEnabledConflicts;
	CString olConflict;
	int ilLc, ilCount = 0;

	for(ilLc = 0; ilLc < omConflictConfData.GetSize(); ilLc++)
	{
		CONFLICTCONFDATA *prlConflictConfData = &omConflictConfData[ilLc];
		
		olConflict = Format(prlConflictConfData);

		if(prlConflictConfData->Enabled || !m_DisplayEnabledFirst)
		{
			omTable.AddTextLine(olConflict, (void *)prlConflictConfData);
			if(prlConflictConfData->Enabled)
				omTable.SetTextLineColor(ilCount, BLACK, WHITE);
			else
				omTable.SetTextLineColor(ilCount, GRAY, SILVER);
			ilCount++;
		}
	}
	if(m_DisplayEnabledFirst)
	{
		for(ilLc = 0; ilLc < omConflictConfData.GetSize(); ilLc++)
		{
			CONFLICTCONFDATA *prlConflictConfData = &omConflictConfData[ilLc];

			if(!prlConflictConfData->Enabled)
			{
				olConflict = Format(prlConflictConfData);
				omTable.AddTextLine(olConflict, (void *)prlConflictConfData);
				omTable.SetTextLineColor(ilCount++, GRAY, SILVER);
			}
		}
	}

    // Update the table content in the display
    omTable.DisplayTable(); 

	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
    omTable.GetCTableListBox()->SetFocus();
}

// Format: Format conflict configiguration data into vertical bar-separated string
CString ConfConflicts::Format(CONFLICTCONFDATA *prpConflictConfData)
{
    CString olText;
	char pclTmp[24];

	olText.Format("%03d|",prpConflictConfData->Index);
    olText += CString(prpConflictConfData->Name);

	if (UseOverlappedTime(prpConflictConfData->Type))
		olText += CString("|") + CString(itoa(prpConflictConfData->Time,pclTmp,10));
	else
		olText += CString("|") + CString("          ");

	if (UseConflictTime(prpConflictConfData->Type))
		olText += CString("|") + CString(itoa(prpConflictConfData->Time,pclTmp,10));
	else
		olText += CString("|") + CString("          ");

	olText += CString("|") + CString(itoa(prpConflictConfData->Weight,pclTmp,10));
    olText += CString("|") + CString(ltoa(prpConflictConfData->NCol,pclTmp,10));
    olText += CString("|") + CString(ltoa(prpConflictConfData->CCol,pclTmp,10));
    olText += CString("|") + CString(prpConflictConfData->Dscr);
    return olText;
}

/////////////////////////////////////////////////////////////////////////////
// ConfConflicts message handlers

BOOL ConfConflicts::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CWnd *polWnd = GetDlgItem(IDC_UPDATE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61352));
	}
	polWnd = GetDlgItem(IDC_SAVE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61282));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}
	
	polWnd = GetDlgItem(IDC_LOADFROMDEFAULT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61856));
	}

	polWnd = GetDlgItem(IDC_SAVEASDEFAULT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61857));
	}

	polWnd = GetDlgItem(IDC_ENABLEDFIRST); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_ENABLEDFIRST));
	}

	ogBasicData.SetWindowStat("CONFLICT IDC_LOADFROMDEFAULT",GetDlgItem(IDC_LOADFROMDEFAULT));
	ogBasicData.SetWindowStat("CONFLICT IDC_SAVEASDEFAULT",GetDlgItem(IDC_SAVEASDEFAULT));

	// calculate the window height
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

    // extend dialog window to current screen width
    rect.left = 10;
    rect.top = 56;

    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 10;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 10;

	// limit the dialog to a single monitor ie. not stretched across several monitors
	CRect olRect2;
	ogBasicData.GetWindowPosition(olRect2,"L","1");
	rect.right = olRect2.right;

    MoveWindow(&rect);

	// Display table content
    CRect olRectTab;
    GetClientRect(&olRectTab);
    olRectTab.OffsetRect(0, m_nDialogBarHeight);
    olRectTab.InflateRect(1, 1);     // hiding the CTable window border
    omTable.SetTableData(this, olRectTab.left, olRectTab.right, olRectTab.top, olRectTab.bottom);
    UpdateDisplay();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ConfConflicts::OnCancel() 
{
	AfxGetMainWnd()->SendMessage(WM_SETUPTABLE_EXIT);
	CDialog::OnCancel();
}

void ConfConflicts::OnUpdate() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
    omTable.GetCTableListBox()->SetFocus();

	int ilCurrLine = omTable.GetCurrentLine();
	if(ilCurrLine != -1)
	{
		ConflictConfigDlg olDialog;
		CONFLICTCONFDATA *prlData = (CONFLICTCONFDATA *) omTable.GetTextLineData(ilCurrLine);
		CONFLICTCONFDATA rlOldData = *prlData;

		olDialog.m_prpData = prlData;
		//olDialog.m_szTitle = CString("Konfiguration Konfliktart: ") + CString(prlData->Name);
		olDialog.m_szTitle = GetString(IDS_STRING61634) + CString(prlData->Name);
		if ( olDialog.DoModal() == IDOK)
		{
			omTable.ChangeTextLine( ilCurrLine, Format(prlData), (void *)prlData );
			if(prlData->Enabled)
			{
				omTable.SetTextLineColor(ilCurrLine, BLACK, WHITE);
			}
			else
			{
				omTable.SetTextLineColor(ilCurrLine, GRAY, SILVER);
			}
			omTable.DisplayTable();
			if(*prlData != rlOldData)
			{
				omLinesChangedMap.SetAt((void *)prlData->Type,prlData);
			}
		}
	}
}

void ConfConflicts::OnSave() 
{
	CCS_TRY;
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
    omTable.GetCTableListBox()->SetFocus();
	
	CWaitCursor wait;
	CONFLICTCONFDATA *prlData = NULL;
	
    for (int ilLc = 0; ilLc < omConflictConfData.GetSize(); ilLc++)
	{
		CFGDATA rlCfg;

		//added by MAX
		int maxNum=NUMCONFLICTS;//max conflict is 56
		if (ogBasicData.myNewConflict==false)
		{
			maxNum--; //max conflict is 55
		}
				
		if(ilLc < maxNum && omLinesChangedMap.Lookup((void *)omConflictConfData[ilLc].Type, (void *& ) prlData))
		{
			rlCfg.Urno = ogBasicData.GetNextUrno();
			sprintf(rlCfg.Ctyp, "CONFLICT%02d", ilLc);
			sprintf(rlCfg.Pkno, ogUsername);
			sprintf(rlCfg.Text,"%d;%ld;%s;%d;%d;%c;%c;%ld;%ld;%s;%d",
			ilLc,
			omConflictConfData[ilLc].Type,
			omConflictConfData[ilLc].Name,
			omConflictConfData[ilLc].Weight,
			omConflictConfData[ilLc].Time,
			omConflictConfData[ilLc].NSymbol,
			omConflictConfData[ilLc].CSymbol,
			omConflictConfData[ilLc].NCol,
			omConflictConfData[ilLc].CCol,
			omConflictConfData[ilLc].Dscr,
			omConflictConfData[ilLc].Enabled ? 1 : 0);

			//add by MAX
			if (ilLc==55)
			{
				sprintf(rlCfg.Text,"%d;%ld;%s;%d;%d;%c;%c;%ld;%ld;%s;%d;%d",
			ilLc,
			omConflictConfData[ilLc].Type,
			omConflictConfData[ilLc].Name,
			omConflictConfData[ilLc].Weight,
			omConflictConfData[ilLc].Time,
			omConflictConfData[ilLc].NSymbol,
			omConflictConfData[ilLc].CSymbol,
			omConflictConfData[ilLc].NCol,
			omConflictConfData[ilLc].CCol,
			omConflictConfData[ilLc].Dscr,
			omConflictConfData[ilLc].Enabled ? 1 : 0,
			omConflictConfData[ilLc].Coverage);
				
			}
			//Save the data
			ogCfgData.SaveConflictData(ogUsername,&rlCfg);

			ogConflictConfData[ilLc] = omConflictConfData[ilLc];

			ogColors[FIRSTCONFLICTCOLOR+(ilLc*2)]   = omConflictConfData[ilLc].NCol;
			ogColors[FIRSTCONFLICTCOLOR+(ilLc*2)+1] = omConflictConfData[ilLc].CCol;

 			CBrush *olBrush = ogBrushs[FIRSTCONFLICTCOLOR+(ilLc*2)];
			delete olBrush;
			ogBrushs[FIRSTCONFLICTCOLOR+(ilLc*2)] = new CBrush(ogColors[FIRSTCONFLICTCOLOR+(ilLc*2)]);

 			olBrush = ogBrushs[FIRSTCONFLICTCOLOR+(ilLc*2)+1];
			delete olBrush;
			ogBrushs[FIRSTCONFLICTCOLOR+(ilLc*2)+1] = new CBrush(ogColors[FIRSTCONFLICTCOLOR+(ilLc*2)+1]);
		}
	}
	omLinesChangedMap.RemoveAll();

	CDialog::OnOK();
	CCS_CATCH_ALL;
}

void ConfConflicts::OnPrint() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
    omTable.GetCTableListBox()->SetFocus();
}

LONG ConfConflicts::OnLButtonDblClk(UINT wParam, LONG lParam)
{
	OnUpdate();
	return 0L;
}

void ConfConflicts::OnLoadFromDefault() 
{
	// TODO: Add your control notification handler code here
    CString olRecord;

	int ilRecCount = 0;

	CStringArray olLines;
	// read default values, if there are none, use hard coded values
	if (ogCfgData.ReadConflicts(ogCfgData.pomDefaultName,olLines) == RCFailure)
	{
		ogCfgData.GetDefaultConflictSetup(olLines);
	}

	for(int i = 0; i < olLines.GetSize(); i++)
	{
		CString olRecord = olLines.GetAt(i);
		int ilMaxFields = 11;
		int ilMaxBytes  = olRecord.GetLength();
		CONFLICTCONFDATA *prlConflictConfData = new CONFLICTCONFDATA;

		for (int ilFieldCount = 0, ilByteCount = 0; ilFieldCount < ilMaxFields; ilFieldCount++)
    	{
        // Extract next field in the specified text-line
			for (int ilFirstByteInField = ilByteCount; ilByteCount < ilMaxBytes && olRecord[ilByteCount] != ';'; ilByteCount++)
				;
			if(ilByteCount <= ilMaxBytes)
			{
				CString olField = olRecord.Mid(ilFirstByteInField,ilByteCount-ilFirstByteInField);
				ilByteCount++;
				switch(ilFieldCount)
				{
				case 0 : prlConflictConfData->Index = atol((LPCSTR)olField);
					break;
				case 1 : prlConflictConfData->Type = atol((LPCSTR)olField);
					break;
				case 2 : strncpy(prlConflictConfData->Name,(LPCSTR)olField,sizeof(prlConflictConfData->Name));
						 prlConflictConfData->Name[sizeof(prlConflictConfData->Name)-1] = '\0';
					break;
				case 3 : prlConflictConfData->Weight = atoi((LPCSTR)olField);
					break;
				case 4 : prlConflictConfData->Time   = atoi((LPCSTR)olField);
					break;
				case 5 : prlConflictConfData->NSymbol = (int)olField[0];
					break;
				case 6 : prlConflictConfData->CSymbol = (int)olField[0];
					break;
				case 7 : prlConflictConfData->NCol = (COLORREF) atol((LPCSTR)olField);
					break;
				case 8 : prlConflictConfData->CCol = (COLORREF) atol((LPCSTR)olField);
					break;
				case 9 : strncpy(prlConflictConfData->Dscr,(LPCSTR)olField,sizeof(prlConflictConfData->Dscr));
						 prlConflictConfData->Dscr[sizeof(prlConflictConfData->Dscr)-1] = '\0';
					break;
				case 10 : prlConflictConfData->Enabled = (olField == "1") ? true : false;
					break;
				case 11 : prlConflictConfData->Coverage = (int)olField[0]; //add by MAX
					break;
				}
			}
		}

		// check, if an update is necessary
		if (i < omConflictConfData.GetSize())
		{
			CONFLICTCONFDATA *prlOldConfData = &omConflictConfData[i];
			if (*prlOldConfData != *prlConflictConfData)
			{
				delete prlOldConfData;
				omConflictConfData.SetAt(i,prlConflictConfData);
				ilRecCount++;
				omLinesChangedMap.SetAt((void *)prlConflictConfData->Type,prlConflictConfData);
			}
		}
		else
		{
			omConflictConfData.Add(prlConflictConfData);
			ilRecCount++;
			omLinesChangedMap.SetAt((void *)prlConflictConfData->Type,prlConflictConfData);
		}
	}

	UpdateDisplay();		
}

void ConfConflicts::OnSaveAsDefault() 
{
	// TODO: Add your control notification handler code here
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
    omTable.GetCTableListBox()->SetFocus();
	
	CWaitCursor wait;
	
    for (int ilLc = 0; ilLc < omConflictConfData.GetSize(); ilLc++)
	{
		CFGDATA rlCfg;
		rlCfg.Urno = ogBasicData.GetNextUrno();
		sprintf(rlCfg.Ctyp, "CONFLICT%02d", ilLc);
		sprintf(rlCfg.Pkno, ogCfgData.pomDefaultName);
		sprintf(rlCfg.Text,"%d;%ld;%s;%d;%d;%c;%c;%ld;%ld;%s;%d",
		ilLc,
		omConflictConfData[ilLc].Type,
		omConflictConfData[ilLc].Name,
		omConflictConfData[ilLc].Weight,
		omConflictConfData[ilLc].Time,
		omConflictConfData[ilLc].NSymbol,
		omConflictConfData[ilLc].CSymbol,
		omConflictConfData[ilLc].NCol,
		omConflictConfData[ilLc].CCol,
		omConflictConfData[ilLc].Dscr,
		omConflictConfData[ilLc].Enabled ? 1 : 0);
		//Save the data
		ogCfgData.SaveConflictData(ogCfgData.pomDefaultName,&rlCfg);

	}
}

BOOL ConfConflicts::UseConflictTime(int ipType)
{
	BOOL blResult;

	switch(ipType)
	{
		case CFI_NOCONFLICT :	// no time usage
		case CFI_CANCELLED :
		case CFI_NOOPERATION :
		case CFI_DIVERTED:
		case CFI_RETURNFLIGHT:
		case CFI_RETURNFROMTAXI:
		case CFI_GATECHANGE :
		case CFI_EQUIPMENTCHANGE :
		case CFI_POSITIONCHANGE :
		case CFI_AIRCRAFTCHANGE :
		case CFI_GAT_NODEMAND :
		case CFI_CIC_NODEMAND :
		case CFI_PST_NODEMAND:
		case CFI_REGN_NODEMAND:
		case CFI_FIRST_OVERBOOKED:
		case CFI_BUSINESS_OVERBOOKED:
		case CFI_ECONOMY_OVERBOOKED:
		case CFI_OVERBOOKED :
		case CFI_NOTINPOOL :
			blResult = FALSE;
			break;

		case CFI_INVALIDJOBDEMAND:
		case CFI_GAT_NOTCOVERED :	// conflict time
		case CFI_GAT_VLOWCOVERED :
		case CFI_GAT_LOWCOVERED :
		case CFI_GAT_OVERCOVERED :
		case CFI_CIC_NOTCOVERED :
		case CFI_CIC_VLOWCOVERED:
		case CFI_CIC_LOWCOVERED:
		case CFI_CIC_OVERCOVERED:
		case CFI_PST_NOTCOVERED:
		case CFI_PST_VLOWCOVERED:
		case CFI_PST_LOWCOVERED:
		case CFI_PST_OVERCOVERED:
		case CFI_REGN_NOTCOVERED:
		case CFI_REGN_VLOWCOVERED:
		case CFI_REGN_LOWCOVERED:
		case CFI_REGN_OVERCOVERED:
		case CFI_EQU_NOTCOVERED:
		case CFI_EQU_VLOWCOVERED:
		case CFI_EQU_LOWCOVERED:
		case CFI_EQU_OVERCOVERED:
		case CFI_EMPLOYEEABSENT:
		case CFI_DEMAND_JOB_COVERAGE:
			blResult = TRUE;
			break;

		case CFI_OVERLAPPING :	// overlap time
		case CFI_DELAYED :		
		case CFI_NOTCONFIRMED :
		case CFI_NOTENDED :
		case CFI_SHIFTNOTSTARTED:
			blResult = FALSE;
			break;

		case CFI_NEXTINFOTIME:
			blResult = FALSE;
			break;

		default :
			blResult = FALSE;
			break;
	}

	return blResult;
}

BOOL ConfConflicts::UseOverlappedTime(int ipType)
{
	BOOL blResult;

	switch(ipType)
	{
	case CFI_NOCONFLICT :	// no time usage
	case CFI_CANCELLED :
	case CFI_NOOPERATION :
	case CFI_DIVERTED:
	case CFI_GATECHANGE :
	case CFI_EQUIPMENTCHANGE :
	case CFI_POSITIONCHANGE :
	case CFI_AIRCRAFTCHANGE :
	case CFI_GAT_NODEMAND :
	case CFI_CIC_NODEMAND :
	case CFI_PST_NODEMAND:
	case CFI_REGN_NODEMAND:
	case CFI_FIRST_OVERBOOKED:
	case CFI_BUSINESS_OVERBOOKED:
	case CFI_ECONOMY_OVERBOOKED:
	case CFI_OVERBOOKED :
	case CFI_NOTINPOOL :
	case CFI_INVALIDJOBDEMAND:
		blResult = FALSE;
	break;

	case CFI_GAT_NOTCOVERED :	// conflict time
	case CFI_GAT_VLOWCOVERED :
	case CFI_GAT_LOWCOVERED :
	case CFI_GAT_OVERCOVERED :
	case CFI_CIC_NOTCOVERED :
	case CFI_CIC_VLOWCOVERED:
	case CFI_CIC_LOWCOVERED:
	case CFI_CIC_OVERCOVERED:
	case CFI_PST_NOTCOVERED:
	case CFI_PST_VLOWCOVERED:
	case CFI_PST_LOWCOVERED:
	case CFI_PST_OVERCOVERED:
	case CFI_REGN_NOTCOVERED:
	case CFI_REGN_VLOWCOVERED:
	case CFI_REGN_LOWCOVERED:
	case CFI_REGN_OVERCOVERED:
		blResult = FALSE;
	break;

	case CFI_OVERLAPPING :	// overlap time
	case CFI_DELAYED :		
	case CFI_NOTCONFIRMED :
	case CFI_NOTENDED :
	case CFI_SHIFTNOTSTARTED:
		blResult = TRUE;
	break;
	case CFI_NEXTINFOTIME:
		blResult = FALSE;
	break;
	default:
		blResult = FALSE;
	}

	return blResult;
}

void ConfConflicts::OnDisplayEnabledFirst() 
{
	UpdateData(TRUE);	
	UpdateDisplay();		
}

/////////////////////////////////////////////////////////////////////////////
// ConflictConfigDlg dialog

ConflictConfigDlg::ConflictConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ConflictConfigDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ConflictConfigDlg)
	m_Dscr = _T("");
	m_OverlappedTime = 0;
	m_ConflictTime	 = 0;
	m_Weight = 0;
	m_Enabled = FALSE;
	m_Coverage = 0;
	//}}AFX_DATA_INIT
}

void ConflictConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ConflictConfigDlg)
	DDX_Text(pDX, IDC_DSCR, m_Dscr);
	DDV_MaxChars(pDX,m_Dscr,sizeof(m_prpData->Dscr)-1);
	DDX_Text(pDX, IDC_OVERLAPPED_TIME, m_OverlappedTime);
	DDX_Text(pDX, IDC_CONFLICT_TIME, m_ConflictTime);
	DDX_Text(pDX, IDC_WEIGHT, m_Weight);
	DDX_Check(pDX, IDC_ENABLED, m_Enabled);
	DDX_Text(pDX,IDC_TYPE56_EDIT, m_Coverage);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(ConflictConfigDlg, CDialog)
	//{{AFX_MSG_MAP(ConflictConfigDlg)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDCCOL, OnCcol)
	ON_BN_CLICKED(IDNCOL, OnNcol)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ConflictConfigDlg message handlers

BOOL ConflictConfigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetWindowText(GetString(IDS_STRING32905));

	CWnd *polWnd = GetDlgItem(IDC_EDIT_OVERLAPPED_TIME); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61843));
	}

	polWnd = GetDlgItem(IDC_EDIT_CONFLICT_TIME); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61927));
	}

	polWnd = GetDlgItem(IDC_EDITCOL1); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32906));
	}
	polWnd = GetDlgItem(IDC_EDITCOL2); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32907));
	}
	polWnd = GetDlgItem(IDC_EDITWEIGHT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32908));
	}
	polWnd = GetDlgItem(IDC_EDITDESCRIPTION); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32909));
	}
	polWnd = GetDlgItem(IDNCOL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32910));
	}
	polWnd = GetDlgItem(IDCCOL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32911));
	}
	polWnd = GetDlgItem(IDC_UNCONFLICT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32912));
	}
	polWnd = GetDlgItem(IDC_CONFLICT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32913));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}
	polWnd = GetDlgItem(IDC_OVERLAPPEDTIMETEXT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61929));
	}
	polWnd = GetDlgItem(IDC_CONFLICTTIMETEXT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61929));
	}
	polWnd = GetDlgItem(IDC_ENABLED); 
	if(polWnd != NULL)
	{
		m_Enabled = m_prpData->Enabled ? TRUE : FALSE;
		polWnd->SetWindowText(GetString(IDS_STRING61928));
	}

	polWnd = GetDlgItem(IDC_OVERLAPPED_TIME); 
	if(polWnd != NULL)
	{
		if (!ConfConflicts::UseOverlappedTime(m_prpData->Type))
			polWnd->EnableWindow(FALSE);
		else
			polWnd->EnableWindow(TRUE);
	}

	polWnd = GetDlgItem(IDC_CONFLICT_TIME); 
	if(polWnd != NULL)
	{
		if (!ConfConflicts::UseConflictTime(m_prpData->Type))
			polWnd->EnableWindow(FALSE);
		else
			polWnd->EnableWindow(TRUE);
	}
	
	//add by MAX
	if (m_prpData->Type==55)
	{
		polWnd = GetDlgItem(IDC_TYPE56_JOBCOVE); 
	    if(polWnd != NULL)
		{
			polWnd->SetWindowText(GetString(ID_61927));
			polWnd->ShowWindow(SW_SHOW);
		}
		
		polWnd = GetDlgItem(IDC_TYPE56_EDIT); 
	    if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_SHOW);
			m_Coverage = m_prpData->Coverage;
		}

		polWnd = GetDlgItem(IDC_JobCoverage); 
	    if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_SHOW);
		}
	}

    SetWindowText(m_szTitle);
	
	if (ConfConflicts::UseConflictTime(m_prpData->Type))
	{
		m_ConflictTime   = m_prpData->Time;
	}
	else if (ConfConflicts::UseOverlappedTime(m_prpData->Type))
	{
		m_OverlappedTime = m_prpData->Time;
	}
	

	m_NCol = m_prpData->NCol;
	m_CCol = m_prpData->CCol;
	m_Dscr = m_prpData->Dscr;
	m_Weight = m_prpData->Weight;

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

HBRUSH ConflictConfigDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = 0;
	long llRc;

	CWnd *polWnd = GetDlgItem(IDNCOL);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(m_NCol);
		llRc = pDC->SetTextColor(m_NCol);

		hbr = CreateSolidBrush(m_NCol);

		RECT rpRect;
		pWnd->GetClientRect(&rpRect);

		CBrush ColorBkBrush;    //  brush to fill background of color field
		ColorBkBrush.CreateSolidBrush(m_NCol);
		pDC->FillRect(&rpRect, &ColorBkBrush);

		return hbr;
	}
	polWnd = GetDlgItem(IDCCOL);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(m_CCol);
		llRc = pDC->SetTextColor(m_CCol);
		hbr = CreateSolidBrush(m_CCol);

		RECT rpRect;
		pWnd->GetClientRect(&rpRect);

		CBrush ColorBkBrush;    //  brush to fill background of color field
		ColorBkBrush.CreateSolidBrush(m_CCol);
		pDC->FillRect(&rpRect, &ColorBkBrush);
		
		return hbr;
	}
	// TODO: Return a different brush if the default is not desired
	return CDialog::OnCtlColor(pDC,pWnd,nCtlColor);
}

void ConflictConfigDlg::OnCcol() 
{
	// TODO: Add your control notification handler code here
	CColorDialog olColorDlg(m_CCol);

	if (olColorDlg.DoModal() == IDOK)
	{
		m_CCol = olColorDlg.GetColor();
		CWnd *polWnd = GetDlgItem(IDCCOL);
		polWnd->Invalidate(TRUE);
		CDC *polDC;
		CRect rlRect;

		polDC = polWnd->GetDC();
		polDC->SetBkMode(OPAQUE);
		polDC->SetBkColor(m_CCol);
		ReleaseDC(polDC);
	}
}

void ConflictConfigDlg::OnNcol() 
{
	// TODO: Add your control notification handler code here
	CColorDialog olColorDlg(m_NCol);

	if (olColorDlg.DoModal() == IDOK)
	{
		m_NCol = olColorDlg.GetColor();
		CWnd *polWnd = GetDlgItem(IDNCOL);
		polWnd->Invalidate(TRUE);
		CDC *polDC;
		CRect rlRect;

		polDC = polWnd->GetDC();
		polDC->SetBkMode(OPAQUE);
		polDC->SetBkColor(m_NCol);
		ReleaseDC(polDC);
	}
}

void ConflictConfigDlg::OnOK() 
{
	UpdateData(TRUE);

	if (ConfConflicts::UseConflictTime(m_prpData->Type))
		m_prpData->Time   = m_ConflictTime;
	else if (ConfConflicts::UseOverlappedTime(m_prpData->Type))
		m_prpData->Time = m_OverlappedTime;
	else
		m_prpData->Time	  = 0;

	strcpy(m_prpData->Dscr,(LPCSTR)m_Dscr);
	m_prpData->CCol = m_CCol;
	m_prpData->NCol = m_NCol;
	m_prpData->Weight = m_Weight;
	m_prpData->Enabled = m_Enabled ? true : false;

	//add by MAX
	m_prpData->Coverage = m_Coverage;
	
	CDialog::OnOK();
}

