// EquipmentGantt.cpp : implementation file
//  

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CLIENTWN.H>
#include <CCSCedaData.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <tscale.h>
#include <cviewer.h>
#include <CCSDragDropCtrl.h>
#include <gbar.h>
#include <cxbutton.h>
#include <conflict.h>
#include <dataset.h>
#include <ufis.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <ButtonList.h>
#include <FlightDetailWindow.h>
#include <EquipmentDetailWindow.h>
#include <ccsddx.h>
#include <EquipmentViewer.h>
#include <EquipmentGantt.h>
#include <dgatejob.h>
#include <AvailableEmpsDlg.h>
#include <DChange.h>
#include <DJobChange.h>
#include <TimePacket.h>
#include <LastJobInfo.h> //PRF 8998

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsTotallyInside(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))


#define	FIX_TIMESCALE_ROUNDING_ERROR
#ifdef	FIX_TIMESCALE_ROUNDING_ERROR
#define GetX(time)	(pomTimeScale->GetXFromTime(time) + imVerticalScaleWidth)
#define GetCTime(x)	(pomTimeScale->GetTimeFromX((x) - imVerticalScaleWidth))
#else
////////////////////////////////////////////////////////////////////////

// Macro definition for get X-coordinate from the given time
#define GetX(time)  (omDisplayStart == omDisplayEnd? -1: \
            (imVerticalScaleWidth + \
            (((time) - omDisplayStart).GetTotalSeconds() * \
            (imWindowWidth - imVerticalScaleWidth) / \
            (omDisplayEnd - omDisplayStart).GetTotalSeconds())))

// Macro definition for get time from the given X-coordinate
#define GetCTime(x)  ((imWindowWidth - imVerticalScaleWidth) == 0? TIMENULL: \
            (omDisplayStart + \
            (time_t)(((x) - imVerticalScaleWidth) * \
            (omDisplayEnd - omDisplayStart).GetTotalSeconds() / \
            (imWindowWidth - imVerticalScaleWidth))))

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
#endif
////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// EquipmentGantt

EquipmentGantt::EquipmentGantt(EquipmentDiagramViewer *popViewer, int ipGroupno,
    int ipVerticalScaleWidth, int ipVerticalScaleIndent,
    CFont *popVerticalScaleFont, CFont *popGanttChartFont,
    int ipGutterHeight, int ipOverlapHeight,
    COLORREF lpVerticalScaleTextColor, COLORREF lpVerticalScaleBackgroundColor,
    COLORREF lpHighlightVerticalScaleTextColor, COLORREF lpHighlightVerticalScaleBackgroundColor,
    COLORREF lpGanttChartTextColor, COLORREF lpGanttChartBackgroundColor,
    COLORREF lpHighlightGanttChartTextColor, COLORREF lpHighlightGanttChartBackgroundColor)
{
    SetViewer(popViewer, ipGroupno);
    SetVerticalScaleWidth(ipVerticalScaleWidth);
    SetVerticalScaleIndent(ipVerticalScaleIndent);
    SetGutters(ipGutterHeight, ipOverlapHeight);
	SetFonts(ogEquipmentIndexes.VerticalScale, ogEquipmentIndexes.Chart);
    SetVerticalScaleColors(lpVerticalScaleTextColor, lpVerticalScaleBackgroundColor,
        lpHighlightVerticalScaleTextColor, lpHighlightVerticalScaleBackgroundColor);
    SetGanttChartColors(lpGanttChartTextColor, lpGanttChartBackgroundColor,
        lpHighlightGanttChartTextColor, lpHighlightGanttChartBackgroundColor);

    // Initialize default values
    omDisplayStart = TIMENULL;
    omDisplayEnd = TIMENULL;
    imWindowWidth = 0;          // unessential -- WM_SIZE will initialize this
    bmIsFixedScaling = TRUE;    // unessential -- will be set in method SetDisplayWindow()
    omCurrentTime = TIMENULL;
    omMarkTimeStart = TIMENULL;
    omMarkTimeEnd = TIMENULL;
    pomStatusBar = NULL;
	pomTimeScale = NULL;
    bmIsMouseInWindow = FALSE;
    bmIsControlKeyDown = FALSE;
    imHighlightLine = -1;
    imCurrentBar = -1;
	imContextItem = -1;
	bmContextBarSet = FALSE;
	imContextBarNo = -1;
	bmActiveLineSet = FALSE;  // There is a selected line ?
	lmActiveUrno = -1;	// Urno of last selected line

	// Id 1-Oct-96
	// This will help us create a virtual double click on a double click after SetCursorPos().
	omLastClickedPosition = CPoint(-1, -1);

    // Required only if you allow moving/resizing
    SetBorderPrecision(5);
    umResizeMode = HTNOWHERE;

	m_pchTip = NULL;
	m_pwchTip = NULL;
}

EquipmentGantt::~EquipmentGantt()
{
    pogButtonList->UnRegisterTimer(this);    // install the timer
	if(m_pwchTip != NULL)
	{
		delete 	m_pwchTip;
	}
	if(m_pchTip != NULL)
	{
		delete m_pchTip;
	}
}


void EquipmentGantt::SetViewer(EquipmentDiagramViewer *popViewer, int ipGroupno)
{
    pomViewer = popViewer;
    imGroupno = ipGroupno;
}

void EquipmentGantt::SetVerticalScaleWidth(int ipWidth)
{
    imVerticalScaleWidth = ipWidth;
}

void EquipmentGantt::SetVerticalScaleIndent(int ipIndent)
{
    imVerticalScaleIndent = ipIndent;
}

void EquipmentGantt::SetFonts(int ipIndex1, int ipIndex2)
{
	LOGFONT rlLogFont;
    pomVerticalScaleFont = &ogScalingFonts[ipIndex1];//popVerticalScaleFont;
    pomGanttChartFont = &ogScalingFonts[ipIndex2];//&ogSmallFonts_Regular_4;//popGanttChartFont;
	pomGanttChartFont->GetLogFont(&rlLogFont);

	switch(ipIndex2)
	{	case 0 : SetGutters(imGutterHeight, 2);

		break;
	case 1 : SetGutters(imGutterHeight, 4);
		break;
	case 2 : SetGutters(imGutterHeight, 8);
		break;
	}
    // Calculate the normal height of a bar
    CDC dc;
    dc.CreateCompatibleDC(NULL);
    if (pomGanttChartFont)
        dc.SelectObject(pomGanttChartFont);
    TEXTMETRIC tm;
    dc.GetTextMetrics(&tm);
    imBarHeight = tm.tmHeight + tm.tmExternalLeading + 2;
    imLeadingHeight = (tm.tmExternalLeading + 2) / 2;
    dc.DeleteDC();
}

void EquipmentGantt::SetGutters(int ipGutterHeight, int ipOverlapHeight)
{
    imGutterHeight = ipGutterHeight;
    imOverlapHeight = ipOverlapHeight;
}

void EquipmentGantt::SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
    lmVerticalScaleTextColor = lpTextColor;
    lmVerticalScaleBackgroundColor = lpBackgroundColor;
    lmHighlightVerticalScaleTextColor = lpHighlightTextColor;
    lmHighlightVerticalScaleBackgroundColor = lpHighlightBackgroundColor;
}

void EquipmentGantt::SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
    lmGanttChartTextColor = lpTextColor;
    lmGanttChartBackgroundColor = lpBackgroundColor;
    lmHighlightGanttChartTextColor = lpHighlightTextColor;
    lmHighlightGanttChartBackgroundColor = lpHighlightBackgroundColor;
}

// Attentions:
// Return the height that it's expected for being displayed
// Be careful, this one may be called before the CListBox was created.
//
int EquipmentGantt::GetGanttChartHeight()
{
    int ilHeight = 0;
    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(imGroupno); ilLineno++)
        ilHeight += GetLineHeight(ilLineno);

    return ilHeight;
}

int EquipmentGantt::GetLineHeight(int ilLineno)
{
	int ilVisualMaxOverlapLevel = pomViewer->GetVisualMaxOverlapLevel(imGroupno, ilLineno);
	return (2 * imGutterHeight) + imBarHeight + (ilVisualMaxOverlapLevel * imOverlapHeight);
}

BOOL EquipmentGantt::Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID)
{
    // Make sure that the viewer is already given
    if (pomViewer == NULL)
        return FALSE;

    // Make sure that all essential style is defined
    // WS_CLIPSIBLINGS is very important for the ListBox so it does not paint outside its window
    dwStyle |= WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_CLIPSIBLINGS;
    dwStyle |= LBS_NOINTEGRALHEIGHT | LBS_OWNERDRAWVARIABLE;
    dwStyle |= LBS_MULTIPLESEL;

    // Create the window
    if (CListBox::Create(dwStyle, rect, pParentWnd, nID) == FALSE)
        return FALSE;

    // Create items according to what's in the Viewer
    ResetContent();
    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(imGroupno); ilLineno++)
    {
        AddString("");
        SetItemHeight(ilLineno, GetLineHeight(ilLineno));
    }

    return TRUE;
}

void EquipmentGantt::SetStatusBar(CStatusBar *popStatusBar)
{
    pomStatusBar = popStatusBar;
}

void EquipmentGantt::SetTimeScale(CTimeScale *popTimeScale)
{
    pomTimeScale = popTimeScale;
}

void EquipmentGantt::SetBorderPrecision(int ipBorderPrecision)
{
    imBorderPreLeft = ipBorderPrecision / 2;
    imBorderPreRight = (ipBorderPrecision+1) / 2;
}

void EquipmentGantt::SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling)
{
    omDisplayStart = opDisplayStart;
    omDisplayEnd = opDisplayEnd;
    bmIsFixedScaling = bpFixedScaling;

    if (m_hWnd == NULL)
        imWindowWidth = 0;  // window is still not opened
    else
    {
        CRect rect;
        GetWindowRect(&rect);   // can't use client rect (vertical scroll may less its width)
        imWindowWidth = rect.Width();   // however, this is correct only if window has no border
        RepaintGanttChart();    // redraw the entire screen
    }
}

void EquipmentGantt::SetDisplayStart(CTime opDisplayStart)
{
    if (bmIsFixedScaling)   // must shift both start/end time while keeping the old time span
        omDisplayEnd = opDisplayStart + (omDisplayEnd - omDisplayStart).GetTotalSeconds();
    omDisplayStart = opDisplayStart;
    RepaintGanttChart();    // redraw the entire screen
}

void EquipmentGantt::SetCurrentTime(CTime opCurrentTime)
{                               
    RepaintGanttChart(-1, omCurrentTime, opCurrentTime);
    omCurrentTime = opCurrentTime;
}

void EquipmentGantt::SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd)
{
	// There are two reasons for these min/max.
	// First, we have to make sure that the start time parameter for RepaintGanttChart()
	// should not be greater than the end time parameter.
	// Second, we should not refresh markers in the vertical scale, since this will
	// disturb the repainting of the vertical scale when we are drawing these marker lines
	// together with the focus rectangle when moving/resizing a bar.
    RepaintGanttChart(-1,
		max(omDisplayStart, min(omMarkTimeStart, opMarkTimeStart)),
		max(omDisplayStart, max(omMarkTimeStart, opMarkTimeStart)));
    omMarkTimeStart = opMarkTimeStart;
    RepaintGanttChart(-1,
		max(omDisplayStart, min(omMarkTimeEnd, opMarkTimeEnd)),
		max(omDisplayStart, max(omMarkTimeEnd, opMarkTimeEnd)));
    omMarkTimeEnd = opMarkTimeEnd;
}

void EquipmentGantt::RepaintVerticalScale(int ipLineno, BOOL bpErase)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcPaint;
    if (GetItemRect(ipLineno, &rcPaint) != LB_ERR)  // valid item in list box?
    {
        rcPaint.right = imVerticalScaleWidth - 1;
        InvalidateRect(&rcPaint, bpErase);
    }
}

void EquipmentGantt::RepaintGanttChart(int ipLineno, CTime opStartTime, CTime opEndTime, BOOL bpErase)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcPaint;
    if (ipLineno == -1) // repaint the whole chart?
        GetClientRect(&rcPaint);
    else
        GetItemRect(ipLineno, &rcPaint);

    // Update the GanttChart body
    if (opStartTime <= omDisplayStart)
        rcPaint.left = imVerticalScaleWidth;
    else
        rcPaint.left = (int)GetX(opStartTime);

    // Use the client width if user want to repaint to the end of time
    if (opEndTime != TIMENULL && opEndTime <= omDisplayEnd)
        rcPaint.right = (int)GetX(opEndTime) + 1;

    InvalidateRect(&rcPaint, bpErase);
}

void EquipmentGantt::RepaintItemHeight(int ipLineno)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    // This repaint method will recalculate the item height
    SetItemHeight(ipLineno, GetLineHeight(ipLineno));

    CRect rcItem, rcPaint;
    GetClientRect(&rcPaint);
    GetItemRect(ipLineno, &rcItem);
    rcPaint.top = rcItem.top;
    InvalidateRect(&rcPaint, TRUE);
}


/////////////////////////////////////////////////////////////////////////////
// EquipmentGantt implementation

void EquipmentGantt::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
    // We have to define MeasureItem() here since MFC places an ASSERT inside
    // the default MeasureItem() of an owner-drawn CListBox.
}

void EquipmentGantt::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// To let the GanttChart know automatically those changes in TimeScale.
// We had to reset the value of omDisplayStart and omDisplayEnd everytime.
	omDisplayStart = GetCTime(imVerticalScaleWidth);
	omDisplayEnd = GetCTime(imWindowWidth);
////////////////////////////////////////////////////////////////////////
    
	int itemID = lpDrawItemStruct->itemID;

    // Attention:
    // This optimization will the speed of displaying GanttChart very a lot.
    // However, this works because we hadn't display any selected or focus items.
    // (We use the "imCurrentItem" variable instead.)
    // So, be careful since this assumption may be changed in the future.
    //
    if (itemID == -1)
        return;
    if (!(lpDrawItemStruct->itemAction & ODA_DRAWENTIRE))
        return; // nothing more to do if listbox just change its selection and/or focus item

    // Drawing routines start here
    CDC dc;
    dc.Attach(lpDrawItemStruct->hDC);
    CRect rcItem(&lpDrawItemStruct->rcItem);
    CRect rcClip;
    dc.GetClipBox(&rcClip);

    // Draw vertical scale if necessary
    if (rcClip.left < imVerticalScaleWidth)
        DrawVerticalScale(&dc, itemID, rcItem);

	DrawDottedLines(&dc, itemID, rcItem);

    // Draw time lines and bars if necessary
    if (imVerticalScaleWidth <= rcClip.right)
    {
        CRgn rgn;
        CRect rect(imVerticalScaleWidth, rcItem.top, rcItem.right, rcItem.bottom);
        rgn.CreateRectRgnIndirect(&rect);
        dc.SelectClipRgn(&rgn);
		DrawBackgroundBars(&dc, itemID, rcItem, rcClip);
        DrawTimeLines(&dc, rcItem.top, rcItem.bottom);
        DrawGanttChart(&dc, itemID, rcItem, rcClip);
        if (umResizeMode != HTNOWHERE)
            DrawFocusRect(&dc, &omRectResize);
        dc.SelectClipRgn(NULL);
    }

    dc.Detach();
}


/////////////////////////////////////////////////////////////////////////////
// EquipmentGantt implementation helper functions
void EquipmentGantt::DrawBackgroundBars(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
	int ilPixel = 1;
	if (omDisplayStart >= omDisplayEnd) // no space to display?
		return;

	int ilBkBarCount = pomViewer->GetBkBarCount(imGroupno, itemID);
	for (int ilBkBar = 0; ilBkBar < ilBkBarCount; ilBkBar++)
	{
		EQUIPMENT_BKBARDATA *prlBkBar = pomViewer->GetBkBar(imGroupno, itemID, ilBkBar);
		if (!IsOverlapped(prlBkBar->StartTime, prlBkBar->EndTime, omDisplayStart, omDisplayEnd))
			continue;   // skip bar which is out of time range

		int left = (int)GetX(prlBkBar->StartTime);
		int right = (int)GetX(prlBkBar->EndTime);
		int top = rcItem.top;
		int bottom = rcItem.bottom;

		lmGanttChartTextColor = BLACK;

		if (!IsOverlapped(left, right, rcClip.left, rcClip.right))
			continue;   // skip bar which is out of clipping region

		CCSPtrArray <BARDECO> olDecoData;  //For additional decoration
		if (prlBkBar->OfflineBar == TRUE)
		{
			GanttBar::MakeOfflineSymbol(olDecoData, left, top, right, bottom);
		}

		GanttBar paintbar(pDC, CRect(left, top, right, bottom),	prlBkBar->FrameType, 
			MARKFULL, prlBkBar->MarkerBrush, prlBkBar->Text, pomGanttChartFont, 
			prlBkBar->TextColor != 0L ? prlBkBar->TextColor : lmGanttChartTextColor, ilPixel,
			FALSE, CRect(0,0,0,0), NULL, &olDecoData);
	}
}

void EquipmentGantt::DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem)
{
    CPoint point;
    ::GetCursorPos(&point);
    ScreenToClient(&point);

    // Check if we need to change the highlight text in VerticalScale
    BOOL blHighlight;
    if (!bmIsMouseInWindow || !rcItem.PtInRect(point))
        blHighlight = FALSE;
    else
    {
        blHighlight = TRUE;
        if (imHighlightLine != itemID)
            RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = itemID;
    }

    // Drawing routines start here
    CString s = pomViewer->GetLineText(imGroupno, itemID);
//	s += pomViewer->GetTmo(imGroupno, itemID);
    CFont *pOldFont = pDC->SelectObject(pomVerticalScaleFont);
    COLORREF nOldTextColor = pDC->SetTextColor(blHighlight?
        lmHighlightVerticalScaleTextColor: lmVerticalScaleTextColor);
    COLORREF nOldBackgroundColor = pDC->SetBkColor(blHighlight?
        lmHighlightVerticalScaleBackgroundColor: lmVerticalScaleBackgroundColor);
    CRect rect(rcItem);
    int left = rect.left + imVerticalScaleIndent;
	rect.right = imVerticalScaleWidth - 2;
    pDC->ExtTextOut(left, rect.top, ETO_CLIPPED | ETO_OPAQUE, &rect, s, lstrlen(s), NULL);
    pDC->SetBkColor(nOldBackgroundColor);
    pDC->SetTextColor(nOldTextColor);
    pDC->SelectObject(pOldFont);
}

void EquipmentGantt::DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
	int ilPixel = 1;

    if (omDisplayStart >= omDisplayEnd) // no space to display?
        return;

//	bool blDisplayArrivalMarker = (ogBasicData.omDepArrMarkerEquipment.Find("ARRIVAL") == -1) ? false : true;
//	bool blDisplayDepartureMarker = (ogBasicData.omDepArrMarkerEquipment.Find("DEPARTURE") == -1) ? false : true;
//	bool blDisplayTurnaroundMarker = (ogBasicData.omDepArrMarkerEquipment.Find("TURNAROUND") == -1) ? false : true;

    // Draw each bar if it in the range of the clipped box
    for (int i = 0; i < pomViewer->GetBarCount(imGroupno, itemID); i++)
    {
        EQUIPMENT_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, i);

		if(ogEquipmentIndexes.Chart == MS_SANS12)
		{
		    ilPixel = 2;
		}
		else
		{
			ilPixel = 1;
		}

		CTime olEnd = prlBar->EndTime;

        int left = (int)GetX(prlBar->StartTime);
        int right = (int)GetX(olEnd);
        int top = rcItem.top + imGutterHeight + (prlBar->OverlapLevel * imOverlapHeight);
        int bottom = top + imBarHeight;

        if (!IsOverlapped(left, right, rcClip.left, rcClip.right))
            continue;   // skip bar which is out of clipping region


		CCSPtrArray<BARDECO> olDecoData;  //For additional decoration

		if(prlBar->DifferentFunction)
			GanttBar::MakeFunctionSymbol(olDecoData, left, top, right, bottom);

		if(prlBar->OfflineBar)
			GanttBar::MakeOfflineSymbol(olDecoData, left, top, right, bottom);
			
		GanttBar paintbar(pDC, CRect(left, top, right, bottom),
			prlBar->FrameType, prlBar->MarkerType, prlBar->MarkerBrush,
			prlBar->Text, pomGanttChartFont, lmGanttChartTextColor, ilPixel,
			FALSE, CRect(0,0,0,0), NULL, &olDecoData);

		int ilNumDeco = olDecoData.GetSize();
		for(int ilDeco = 0; ilDeco < ilNumDeco; ilDeco++)
		{
			delete olDecoData[ilDeco].Region;
		}
		olDecoData.DeleteAll();
    }
}

void EquipmentGantt::DrawTimeLines(CDC *pDC, int top, int bottom)
{
    // Draw current time
    if (omCurrentTime != TIMENULL && IsBetween(omCurrentTime, omDisplayStart, omDisplayEnd))
    {
        CPen penRed(PS_SOLID, 0, RGB(255, 0, 0));
        CPen *pOldPen = pDC->SelectObject(&penRed);
        int x = (int)GetX(omCurrentTime);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw marked time start
    if (omMarkTimeStart != TIMENULL && IsBetween(omMarkTimeStart, omDisplayStart, omDisplayEnd))
    {
        CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
        CPen *pOldPen = pDC->SelectObject(&penYellow);
        int x = (int)GetX(omMarkTimeStart);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw marked time end
    if (omMarkTimeEnd != TIMENULL && IsBetween(omMarkTimeEnd, omDisplayStart, omDisplayEnd))
    {
        CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
        CPen *pOldPen = pDC->SelectObject(&penYellow);
        int x = (int)GetX(omMarkTimeEnd);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }
}

void EquipmentGantt::DrawFocusRect(CDC *pDC, const CRect &rect)
{
	CRect rcFocus = rect;
	rcFocus.right++;	// extra pixel to help DrawFocusRect() work correctly
	pDC->DrawFocusRect(&rcFocus);

	// Before finish: please insert the correct code for status bar while moving/resizing
    if (pomStatusBar != NULL)
	{
		CString s = GetCTime(rcFocus.left).Format("%H%M") + " - "
			+ GetCTime(rcFocus.right - 1).Format("%H%M");
		pomStatusBar->SetPaneText(0, s);
	}
}


/////////////////////////////////////////////////////////////////////////////
// EquipmentGantt message handlers

BEGIN_MESSAGE_MAP(EquipmentGantt, CWnd)
    //{{AFX_MSG_MAP(EquipmentGantt)
	ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
	ON_WM_SETCURSOR()
    ON_WM_MOUSEMOVE()
    ON_WM_TIMER()
    ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
    ON_MESSAGE(WM_USERKEYDOWN, OnUserKeyDown)
    ON_MESSAGE(WM_USERKEYUP, OnUserKeyUp)
	ON_COMMAND(EQ_DISPALYDETAILDLG, OnMenuDesplayDetailDlg)
	ON_COMMAND(EQ_DEBUGINFO_MENUITEM, OnMenuDebugInfo)
	ON_COMMAND(EQ_EDITJOB_MENUITEM, OnMenuEditJob)
	ON_COMMAND(EQ_DELETEJOB_MENUITEM, OnMenuDeleteJob)
	ON_COMMAND(EQ_LASTJOBINFO_MENUITEM,OnMenuLastJobInfo) //PRF 8998
	ON_COMMAND_RANGE(EQ_ACCEPT_ALL_CONFLICTS, (EQ_ACCEPT_ALL_CONFLICTS+EQ_MAXNUMCONFLICTS), OnMenuAcceptConflict)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_COMMAND_RANGE(EQ_EDITFASTLINK, (EQ_EDITFASTLINK+EQ_MAXFASTLINKS), OnMenuEditFastLink)
	ON_COMMAND_RANGE(EQ_DELETEFASTLINK, (EQ_DELETEFASTLINK+EQ_MAXFASTLINKS), OnMenuDeleteFastLink)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

int EquipmentGantt::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	EnableToolTips (TRUE); //Singapore
    m_DragDropTarget.RegisterTarget(this, this);
	
	return 0;
}

void EquipmentGantt::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	m_DragDropTarget.Revoke();
	CWnd::OnDestroy();
}

void EquipmentGantt::OnSize(UINT nType, int cx, int cy)
{
    CListBox::OnSize(nType, cx, cy);

    CRect rect;
    GetWindowRect(&rect);   // don't use cx (vertical scroll may less the width of client rect)

    if (bmIsFixedScaling)   // fixed-scaling mode?
    {
        if (imWindowWidth == 0) // first time initialization?
            imWindowWidth = rect.Width();
        omDisplayEnd = GetCTime(rect.Width());   // has to recalculate the right most boundary?
    }
    else    // must be automatic variable-scaling mode
    {
        if (rect.Width() != imWindowWidth) // display window changed?
            InvalidateRect(NULL);   // repaint whole GanttChart
    }
    imWindowWidth = rect.Width();
}

// Attention:
// Be careful, this may not work if you try to develop horizontal-scrolling in GanttChart.
// Generally, if the ListBox need to be repainted, it will send the message WM_ERASEBKGND.
// But if you allow horizontal-scrolling, WM_ERASEBKGND will be called before the new
// horizontal position could be detected by dc.GetWindowOrg().x in DrawItem().
// However, this version work fine since there's no such scrolling.
// Then, we can assume that the beginning offset will be 0 all the time.
//
BOOL EquipmentGantt::OnEraseBkgnd(CDC* pDC)
{
    CRect rectErase;
    pDC->GetClipBox(&rectErase);

    // Draw the VerticalScale if the user specify it to be displayed
    if (imVerticalScaleWidth > 0)
    {
        CBrush brush(lmVerticalScaleBackgroundColor);
        CRect rect(0, rectErase.top, imVerticalScaleWidth, rectErase.bottom);
        pDC->FillRect(&rect, &brush);

        // Draw seperator line between VerticalScale and the body of GanttChart
        CPen penBlack(PS_SOLID, 0, ::GetSysColor(COLOR_WINDOWFRAME));
        CPen *pOldPen = pDC->SelectObject(&penBlack);
        pDC->MoveTo(imVerticalScaleWidth-2, rectErase.top);
        pDC->LineTo(imVerticalScaleWidth-2, rectErase.bottom);
        CPen penWhite(PS_SOLID, 0, ::GetSysColor(COLOR_BTNHIGHLIGHT));
        pDC->SelectObject(&penWhite);
        pDC->MoveTo(imVerticalScaleWidth-1, rectErase.top);
        pDC->LineTo(imVerticalScaleWidth-1, rectErase.bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw the background of the body of GanttChart
    CBrush brush(lmGanttChartBackgroundColor);
    CRect rect(imVerticalScaleWidth, rectErase.top, rectErase.right, rectErase.bottom);
    pDC->FillRect(&rect, &brush);

    // Draw the vertical current time and marked time lines
    DrawTimeLines(pDC, rectErase.top, rectErase.bottom);

    // Tell Windows there's nothing more to do
    return TRUE;
}

BOOL EquipmentGantt::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	// Id 2-Oct-96
	// This override cursor setting will remove cursor flickering when the user press
	// control key but still not begin the moving/resizing yet.
	//
    if (bmIsControlKeyDown && imHighlightLine != -1 && imCurrentBar != -1)
		return TRUE;	// override the default processing

	return CWnd::OnSetCursor(pWnd, nHitTest, message);
}

void EquipmentGantt::OnMouseMove(UINT nFlags, CPoint point)
{
    if (umResizeMode != HTNOWHERE)  // in SetCapture()?
    {
        OnMovingOrResizing(point, nFlags & MK_LBUTTON);
        return;
    }

    CListBox::OnMouseMove(nFlags, point);
    UpdateGanttChart(point, nFlags & MK_CONTROL);
}

void EquipmentGantt::OnTimer(UINT nIDEvent)
{
	if(::IsWindow(this->GetSafeHwnd()))
	{
		if (umResizeMode != HTNOWHERE)  // in SetCapture()?
			return;

		if (nIDEvent == 0)  // the last timer message, clear everything
		{
			bmIsMouseInWindow = FALSE;
			RepaintVerticalScale(imHighlightLine, FALSE);
			imHighlightLine = -1;       // no more selected line
			UpdateBarStatus(-1, -1, -1);    // no more status if mouse is outside of GanttChart
			return;
		}

		CPoint point;
		::GetCursorPos(&point);
		ScreenToClient(&point);    
		UpdateGanttChart(point, ::GetKeyState(VK_CONTROL) & 0x8080);
			// This statement has to be fixed for using both in Windows 3.11 and NT.
			// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
			// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.
	}
	else
	{
		ASSERT(0);
	}
}

void EquipmentGantt::OnLButtonDown(UINT nFlags, CPoint point)
{

	// This will help us create a virtual double click on a double click after SetCursorPos().
	CPoint olLastClickedPosition = omLastClickedPosition;
	omLastClickedPosition = point;
	if (olLastClickedPosition == point)
	{
		OnLButtonDblClk(nFlags, point);
		omLastClickedPosition = CPoint(-1, -1);
		return;
	}

    // Check if the user starts moving/resizing mouse action
    if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL) && BeginMovingOrResizing(point))
	{
        return; // not call the default after SetCapture()
	}

//	if(imHighlightLine != -1)
//	{
//		if((imCurrentBar = GetBarnoFromPoint(imHighlightLine, point)) != -1)
//		{
//			// on a bar
//		}
//		else if((imCurrentBar = GetBkBarnoFromPoint(imHighlightLine, point)) != -1)
//		{
//			// on a background bar
//		}
//		else
//		{
//			// anywhere other than a bar/background bar
//			DragEquipmentBegin(imHighlightLine);
//		}
//	}

	// Check dragging from the vertical scale (dragging DutyBar)
    imHighlightLine = GetItemFromPoint(point);
//	if (IsBetween(point.x, 0, imVerticalScaleWidth-2) && (imHighlightLine != -1))	// from VerticalScale?
//	{
//		DragEquipmentBegin(imHighlightLine);
//		return;	// not call the default after dragging;
//	}

    // Bring bar to front / Send bar to back
    imCurrentBar = GetBarnoFromPoint(imHighlightLine, point);
    int ilCurrentBkBar = GetBkBarnoFromPoint(imHighlightLine, point);

	if(imHighlightLine != -1 && imCurrentBar == -1)
	{
		DragEquipmentBegin(imHighlightLine);
		return;	// not call the default after dragging;
	}
    
	if (imCurrentBar != -1)
    {
		// Update the time band (two vertical yellow lines)
		EQUIPMENT_BARDATA *prlBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
		TIMEPACKET olTimePacket;
		olTimePacket.StartTime = prlBar->StartTime;
		olTimePacket.EndTime = prlBar->EndTime;
		ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);

		// Bring bar to front / Send bar to back
		int ilOldOverlapLevel = prlBar->OverlapLevel;
		int ilOldLineHeight = GetLineHeight(imHighlightLine);
        EQUIPMENT_BARDATA rlBar = *prlBar;	// copy all data of this bar
		rlBar.Indicators.RemoveAll();	// remove indicators, will be dealloc in DeleteBar()
		for (int i = 0; i < prlBar->Indicators.GetSize(); i++)
			rlBar.Indicators.NewAt(i, prlBar->Indicators[i]);
        pomViewer->DeleteBar(imGroupno, imHighlightLine, imCurrentBar);
		BOOL blBringBarToFront = !(nFlags & MK_SHIFT);
        imCurrentBar = pomViewer->CreateBar(imGroupno, imHighlightLine, &rlBar, blBringBarToFront);
			// unnecessary to delete the NewAt() bars since create bar automatic copy it

		// Update the display, also check if line height was changed
		if (GetLineHeight(imHighlightLine) == ilOldLineHeight)
		{
			RepaintGanttChart(imHighlightLine, TIMENULL, TIMENULL, TRUE);
		}
		else
		{
			//RepaintItemHeight(imHighlightLine);
			LONG lParam = MAKELONG(imHighlightLine, imGroupno);
			pomViewer->pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINEHEIGHT, lParam);
		}


		// To make drag-and-drop operation works properly, we have to change the position
		// of the mouse cursor also, since the drag-and-drop will use the current position
		// as a basis for selecting a bar.
		prlBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
			// reload the bar again (it may be already moved to some other position)
		int ilNewOverlapLevel = prlBar->OverlapLevel;
		point.Offset(0, (ilNewOverlapLevel - ilOldOverlapLevel) * imOverlapHeight);
	}
	else if(ilCurrentBkBar != -1)
    {
		// Update the time band (two vertical yellow lines)
		EQUIPMENT_BKBARDATA *prlBkBar = pomViewer->GetBkBar(imGroupno, imHighlightLine, ilCurrentBkBar);
		TIMEPACKET olTimePacket;
		olTimePacket.StartTime = prlBkBar->StartTime;
		olTimePacket.EndTime = prlBkBar->EndTime;
		ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);
	}

	// Update the cursor position to make sure that the cursor will always be over
	// the bar we just click on.
	CPoint olMousePosition = point;
	ClientToScreen(&olMousePosition);
	::SetCursorPos(olMousePosition.x, olMousePosition.y);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	// Id 1-Oct-96
	// This will help us create a virtual double click on a double click after SetCursorPos().
	omLastClickedPosition = point;

	if ((nFlags & MK_LBUTTON) && imHighlightLine != -1 && imCurrentBar != -1)
	{
		DragEquJobBegin(imHighlightLine, imCurrentBar);
		return; // do not call CListBox::OnLButtonDown after initiating drag and drop
	}

    CListBox::OnLButtonDown(nFlags, point);
}

void EquipmentGantt::DragEquipmentBegin(int ipLineno)
{
	EQUIPMENT_LINEDATA *prlLine = pomViewer->GetLine(imGroupno, ipLineno);

	m_DragDropSource.CreateDWordData(DIT_EQUIPMENT, 1);
	m_DragDropSource.AddDWord(prlLine->EquUrno);
	m_DragDropSource.BeginDrag();
}

void EquipmentGantt::DragEquJobBegin(int ipLineno, int ipBarno)
{
    EQUIPMENT_BARDATA *prlBar = pomViewer->GetBar(imGroupno, ipLineno, ipBarno);

	m_DragDropSource.CreateDWordData(DIT_JOBBAR, 1);
	m_DragDropSource.AddDWord(prlBar->JobUrno);
	m_DragDropSource.BeginDrag();
}

void EquipmentGantt::OnLButtonUp(UINT nFlags, CPoint point) 
{
    if (umResizeMode != HTNOWHERE)  // in SetCapture()?
    {
		// This allow a fast way for terminating moving or resizing mode
        OnMovingOrResizing(point, nFlags & MK_LBUTTON);
        return;
    }

	CListBox::OnLButtonUp(nFlags, point);
}

void EquipmentGantt::OnLButtonDblClk(UINT nFlags, CPoint point)   //huzimi
{
   
	
	
	// Check if the user starts moving/resizing mouse action
    if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL) &&
        BeginMovingOrResizing(point))
        return; // not call the default after SetCapture()

	if(imGroupno == -1 || imHighlightLine == -1)
		return;


	if(imCurrentBar != -1)
	{
		// on a flight bar
	    EQUIPMENT_BARDATA *prlBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
		JOBDATA *prlJob = ogJobData.GetJobByUrno(prlBar->JobUrno);
		if(prlJob != NULL)
		{
			TIMEPACKET olTimePacket;
			olTimePacket.StartTime = omMarkTimeStart;
			olTimePacket.EndTime = omMarkTimeEnd;
			ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);

			if(!strcmp(prlJob->Jtco,JOBFID))
			{
				EQUIPMENT_LINEDATA *prlLine = pomViewer->GetLine(imGroupno, imHighlightLine);
				CTime olClickedTime = GetCTime(point.x);
				CTimeSpan olDuration = pomViewer->omEndTime - pomViewer->omStartTime;
				new EquipmentDetailWindow(this, prlLine->EquUrno, olClickedTime, olDuration.GetTotalHours());
			}
			else
			{
				long llArrUrno, llDepUrno;
				ogBasicData.GetFlightUrnoForJob(prlJob, &llArrUrno, &llDepUrno);
				new FlightDetailWindow(this, llArrUrno, llDepUrno, FALSE, prlJob->Aloc, NULL, EQUIPMENTDEMANDS);
			}
		}
	}
	else
	{
		EQUIPMENT_LINEDATA *prlLine = pomViewer->GetLine(imGroupno, imHighlightLine);
		CTime olClickedTime = GetCTime(point.x);
		CTimeSpan olDuration = pomViewer->omEndTime - pomViewer->omStartTime;
		new EquipmentDetailWindow(this, prlLine->EquUrno, olClickedTime, olDuration.GetTotalHours());
	}
}

void EquipmentGantt::OnRButtonDown(UINT nFlags, CPoint point) 
{
    if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL) &&
        BeginMovingOrResizing(point))
        return; // not call the default after SetCapture()

	int itemID, ilBarno, ilBkBarno;
	if ((itemID = GetItemFromPoint(point)) == -1)
		return;	// not click in the Gantt window

	if (point.x < imVerticalScaleWidth - 2)	// in VerticalScale?
	{
		CMenu menu;
		menu.CreatePopupMenu();
		for (int i = menu.GetMenuItemCount(); i > 0; i--)
	        menu.RemoveMenu(i, MF_BYPOSITION);
	    //menu.AppendMenu(MF_STRING,11, "Bearbeiten");	// work on
	    menu.AppendMenu(MF_STRING,EQ_DISPALYDETAILDLG, GetString(IDS_STRING61352));	// work on

		ClientToScreen(&point);
		imContextItem = itemID;
        menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
		return;
	}

	if ((ilBarno = GetBarnoFromPoint(itemID, point)) != -1)	// on a bar
	{
        EQUIPMENT_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, ilBarno);
		bmContextBarSet = TRUE;	// Save bar data for handling after context menu closed
		rmContextBar = *prlBar;	// copy all data of this bar
		imContextItem = itemID;
		imContextBarNo = ilBarno;

		CMenu olMenu;
		olMenu.CreatePopupMenu();
		for (int i = olMenu.GetMenuItemCount(); i > 0; i--)
	        olMenu.RemoveMenu(i, MF_BYPOSITION);

		olMenu.AppendMenu(MF_STRING,EQ_EDITJOB_MENUITEM, GetString(IDS_STRING61360));
		olMenu.AppendMenu(MF_STRING,EQ_DELETEJOB_MENUITEM, GetString(IDS_STRING61210));

		CString olText;
		CCSPtrArray <CONFLICTDATA> olConflicts;
		ogConflicts.GetJobConflicts(olConflicts,prlBar->JobUrno);
		int ilNumConflicts = olConflicts.GetSize();
		if(ilNumConflicts > 0)
		{
			int ilConflNum = EQ_ACCEPT_ALL_CONFLICTS;
			int ilNumUnacceptedConflicts = 0;
			omAcceptConflictsMap.RemoveAll();
			for(int ilConfl = 0; ilConfl < ilNumConflicts; ilConfl++)
			{
				//olMenu.AppendMenu(MF_STRING,25, "Konflikt akzeptieren");	// accept conflict
				ilConflNum ++;
				olText.Format("%s: %s",GetString(IDS_STRING61357),ogConflicts.GetConflictTextByType(olConflicts[ilConfl].Type));
				if(olConflicts[ilConfl].Confirmed)
				{
					olMenu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
				}
				else
				{
					olMenu.AppendMenu(MF_STRING, ilConflNum, olText);
					ilNumUnacceptedConflicts++;
				}
				omAcceptConflictsMap.SetAt((void *) ilConflNum, &olConflicts[ilConfl]);
			}
			olConflicts.RemoveAll();
			if(ilNumUnacceptedConflicts > 1)
			{
				olMenu.AppendMenu(MF_STRING, EQ_ACCEPT_ALL_CONFLICTS, GetString(IDS_STRING61378));
			}
		}

		if(ogBasicData.DisplayDebugInfo())
		{
			olMenu.AppendMenu(MF_STRING,EQ_DEBUGINFO_MENUITEM, "Debug Info");
		}

		olMenu.AppendMenu(MF_STRING,EQ_LASTJOBINFO_MENUITEM, GetString(IDS_LAST_JOB_INFO));//PRF 8998

		ClientToScreen(&point);
        olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
		return;
	}
	else if(((ilBkBarno = GetBkBarnoFromPoint(itemID, point)) != -1))
	{
		// on a background bar (pool job)
		EQUIPMENT_BKBARDATA *prlActiveBkBar = pomViewer->GetBkBar(imGroupno, itemID, ilBkBarno);
		if(prlActiveBkBar != NULL)
		{
			if(prlActiveBkBar->Type == EQUIPMENT_BKBARDATA::FASTLINK)
			{
				rmActiveBkBar = *prlActiveBkBar;
				bmActiveBkBar = TRUE;

				int ilEditConflNum = EQ_EDITFASTLINK;
				int ilDeleteConflNum = EQ_EDITFASTLINK;
				omFastLinkMenuMap.RemoveAll();
				
				CMenu olMenu;
				olMenu.CreatePopupMenu();
				for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
					olMenu.RemoveMenu(i, MF_BYPOSITION);

				JOBDATA *prlJob;
				EMPDATA *prlEmp;
				CString olEmpText, olText;
				int ilNumFastLinks = prlActiveBkBar->JobUrnos.GetSize(), ilIndex;
				for(int ilFL = 0; ilFL < ilNumFastLinks && ilFL < EQ_MAXFASTLINKS; ilFL++)
				{
					prlJob = ogJobData.GetJobByUrno(prlActiveBkBar->JobUrnos[ilFL]);
					if(prlJob != NULL && (prlEmp = ogEmpData.GetEmpByUrno(prlJob->Ustf)) != NULL)
					{
						olEmpText.Empty();
						if(prlActiveBkBar->JobUrno == 0L)
						{
							// fast link bar is made of several fast links so display emp name and duty time
							olEmpText.Format(": %s %s-%s", ogEmpData.GetEmpName(prlEmp), prlJob->Acfr.Format("%H%M"), prlJob->Acto.Format("%H%M"));
						}

						// edit fast link
						ilIndex = EQ_EDITFASTLINK + ilFL;
						olText.Format("%s%s", GetString(IDS_EDIT_FASTLINK), olEmpText);
						olMenu.AppendMenu(MF_STRING,ilIndex, olText);
						omFastLinkMenuMap.SetAt((void *) ilIndex, (void *) prlJob->Urno);

						// delete fast link
						ilIndex = EQ_DELETEFASTLINK + ilFL;
						olText.Format("%s%s", GetString(IDS_DELETE_FASTLINK), olEmpText);
						olMenu.AppendMenu(MF_STRING,ilIndex, olText);
						omFastLinkMenuMap.SetAt((void *) ilIndex, (void *) prlJob->Urno);

						if(ilFL < (ilNumFastLinks-1))
						{
							olMenu.AppendMenu(MF_SEPARATOR);
						}
					}
				}
//				if(ogBasicData.DisplayDebugInfo())
//				{
//					olMenu.AppendMenu(MF_STRING,EQ_DEBUGINFO_MENUITEM, "Debug Info");
//				}
				ClientToScreen(&point);
				olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);

				return;
			}
		}
	}
	
	CWnd::OnRButtonDown(nFlags, point);
}

void EquipmentGantt::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

void EquipmentGantt::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

LONG EquipmentGantt::OnUserKeyDown(UINT wParam, LONG lParam)
{
	int nIndex = GetTopIndex();

	switch (wParam) {
	case VK_UP:
		SetTopIndex(nIndex - 1);
		break;
	case VK_DOWN:
		SetTopIndex(nIndex + 1);
		break;
	}

	return 0L;
}

LONG EquipmentGantt::OnUserKeyUp(UINT wParam, LONG lParam)
{
	return 0L;
}

/////////////////////////////////////////////////////////////////////////////
// EquipmentGantt context menus helper functions

void EquipmentGantt::OnMenuEditFastLink(UINT ipId)
{
	if(bmActiveBkBar)
	{
		// accept a single conflict
		long llJobUrno;
		omFastLinkMenuMap.Lookup((void *) ipId, (void *&) llJobUrno);
		JOBDATA *prlFastLinkJob = ogJobData.GetJobByUrno(llJobUrno);
		EMPDATA *prlEmp = NULL;
		if(prlFastLinkJob != NULL && (prlEmp = ogEmpData.GetEmpByUrno(prlFastLinkJob->Ustf)) != NULL)
		{
			CString olEmpText = ogEmpData.GetEmpName(prlEmp);
			DJobChange olJobChangeDialog(this, olEmpText, prlFastLinkJob->Acfr, prlFastLinkJob->Acto);
			if(olJobChangeDialog.DoModal() == IDOK)
			{
				JOBDATA rlOldJob = *prlFastLinkJob;
				prlFastLinkJob->Acfr = olJobChangeDialog.m_Acfr;
				prlFastLinkJob->Acto = olJobChangeDialog.m_Acto;
				ogJobData.ChangeJobData(prlFastLinkJob, &rlOldJob, "EFLEdt");
			}
		}
	}
}



void EquipmentGantt::OnMenuDeleteFastLink(UINT ipId)
{
	if(bmActiveBkBar)
	{
		long llJobUrno;
		omFastLinkMenuMap.Lookup((void *) ipId, (void *&) llJobUrno);
		JOBDATA *prlFastLinkJob = ogJobData.GetJobByUrno(llJobUrno);
		if(prlFastLinkJob != NULL)
		{
			CString olEmpName = ogEmpData.GetEmpName(prlFastLinkJob->Ustf);

			CString olConfirmText;
			olConfirmText.Format("%s %s ?",GetString(IDS_CONFIRMDELETEFASTLINK),olEmpName);
			if(MessageBox(olConfirmText, GetString(IDS_STRING61210), MB_ICONQUESTION|MB_YESNO) == IDYES)
			{
				ogDataSet.DeleteJob(this, prlFastLinkJob);
			}
		}
	}
}

void EquipmentGantt::OnMenuEditJob()
{
	if(bmContextBarSet)
	{
		JOBDATA *prlJob = ogJobData.GetJobByUrno(rmContextBar.JobUrno);
		if(prlJob != NULL)
		{
			long llArrUrno, llDepUrno;
			ogBasicData.GetFlightUrnoForJob(prlJob, &llArrUrno, &llDepUrno);
			new FlightDetailWindow(this,llArrUrno,llDepUrno,FALSE,prlJob->Aloc,NULL,EQUIPMENTDEMANDS);
			TIMEPACKET olTimePacket;
			olTimePacket.StartTime = omMarkTimeStart;
			olTimePacket.EndTime = omMarkTimeEnd;
			ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);
		}
	}
}

void EquipmentGantt::OnMenuDeleteJob()
{
	if(bmContextBarSet)
	{
		JOBDATA *prlJob = ogJobData.GetJobByUrno(rmContextBar.JobUrno);
		if(prlJob != NULL)
		{
			ogDataSet.DeleteJob(this, prlJob);
		}
	}
}

void EquipmentGantt::OnMenuDesplayDetailDlg()
{
	if(imGroupno > -1 && imContextItem > -1)
	{
		EQUIPMENT_LINEDATA *prlLine = pomViewer->GetLine(imGroupno, imContextItem);

		CTimeSpan olDuration = pomViewer->omEndTime - pomViewer->omStartTime;
		int ilDurationSecs = olDuration.GetTotalSeconds() / 2;
		CTime olClickedTime = GetCTime(imVerticalScaleWidth) + CTimeSpan(ilDurationSecs);

		new EquipmentDetailWindow(this, prlLine->EquUrno, olClickedTime, 24);
	}
}

void EquipmentGantt::OnMenuDebugInfo()
{
	if(bmContextBarSet)
	{
		// display debug info for a single job
		CString olText = "Job:\n" + ogJobData.Dump(rmContextBar.JobUrno);
		ogBasicData.DebugInfoMsgBox(olText);
	}
	else if(bmActiveBkBar)
	{
		// display debug info for a background bar (pool job/temp absence etc)
		JOBDATA *prlFastLinkJob = ogJobData.GetJobByUrno(rmActiveBkBar.JobUrno);
		if(prlFastLinkJob != NULL)
		{
			CString olText = "FastLinkJob:\n" + ogJobData.Dump(rmActiveBkBar.JobUrno);
			ogBasicData.DebugInfoMsgBox(olText);
		}
	}
}

//PRF 8998
void EquipmentGantt::OnMenuLastJobInfo()
{
	if(bmContextBarSet)
	{
		JOBDATA *prlJob = ogJobData.GetJobByUrno(rmContextBar.JobUrno);
		if(prlJob != NULL)
		{
			LastJobInfo olLastJobInfo(prlJob->Urno,this);
			olLastJobInfo.DoModal();
		}
	}	
}

void EquipmentGantt::OnMenuAcceptConflict(UINT ipId)
{
	if(bmContextBarSet)
	{
		if(ipId == EQ_ACCEPT_ALL_CONFLICTS)
		{
			// accept all conflicts
			ogConflicts.AcceptJobConflicts(rmContextBar.JobUrno);
		}
		else
		{
			// accept a single conflict
			CONFLICTDATA *prlConflict;
			omAcceptConflictsMap.Lookup((void *) ipId, (void *&) prlConflict);
			ogConflicts.AcceptOneJobConflict(rmContextBar.JobUrno, prlConflict->Type);
		}
	}
	bmContextBarSet = -1;
}




/////////////////////////////////////////////////////////////////////////////
// EquipmentGantt message handlers helper functions

// Update every necessary GUI elements for each mouse/timer event
void EquipmentGantt::UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown)
{
    int itemID = GetItemFromPoint(point);

    // Check if we have to change the highlight line in VerticalScale
	CPoint ptScreen = point;
	ClientToScreen(&ptScreen);
	if (WindowFromPoint(ptScreen) == this)	// moving inside GanttChart?
    {
        if (!bmIsMouseInWindow) // first time?
        {
            bmIsMouseInWindow = TRUE;
            pogButtonList->RegisterTimer(this);    // install the timer
        }

        if (itemID != imHighlightLine)  // move to new bar?
            RepaintVerticalScale(itemID, FALSE);
        if (IsBetween(point.x, 0, imVerticalScaleWidth-1))
            UpdateBarStatus(itemID, -1, -1);    // moving on vertical scale, not a bar
        else
        {
            int ilBarno;
            if (!bpIsControlKeyDown)
                ilBarno = GetBarnoFromPoint(itemID, point);
            else
                ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);

			int ilBkBarno = GetBkBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
            UpdateBarStatus(itemID, ilBarno, ilBkBarno);
        }
    }
    else if (bmIsMouseInWindow) // moving out of GanttChart? (first time only)
    {
        bmIsMouseInWindow = FALSE;
        RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = -1;       // no more selected line
        UpdateBarStatus(-1, -1, -1);    // no more status if mouse is outside of GanttChart
    }

    // Check if we have to change the icon (when user want to move/resize a bar)
	if (GetCapture() != NULL)
		;	// don't set cursor on drag-and-drop
    else if (bmIsControlKeyDown && !bpIsControlKeyDown)              // no more Ctrl-key?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    else if (!bmIsMouseInWindow || !bpIsControlKeyDown || imCurrentBar == -1)
        ;   // do noting
    else if (HitTest(itemID, imCurrentBar, point) == HTCAPTION) // body?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));	//hag20000613
    else                                                        // left/right border?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));


    // Update control key state
    bmIsControlKeyDown = bpIsControlKeyDown;
}

void EquipmentGantt::UpdateBarStatus(int ipLineno, int ipBarno, int ipBkBarno)
{
    CString olStatusText;

    if (ipLineno == imHighlightLine && ipBarno == imCurrentBar && ipBkBarno == imBkBarno)	// user still be on the old bar?
		return;

    if (ipLineno == -1)	// there is no more bar status?
	{
        olStatusText = "";
	}
    else if (ipBarno == -1 && ipBkBarno == -1)	// user move outside the bar?
	{
		// over vertical scale
        olStatusText = pomViewer->GetLineStatusText(imGroupno, ipLineno);
	}
	else if(ipBarno == -1 && ipBkBarno != -1)
	{
		// over background bar
		olStatusText = pomViewer->GetBkBarStatusText(imGroupno, ipLineno, ipBkBarno);
	}
	else
	{
		// over bar
		olStatusText = pomViewer->GetBarStatusText(imGroupno, ipLineno, ipBarno);
	}

	// display status bar
    if (pomStatusBar != NULL)
	{
        pomStatusBar->SetPaneText(0, (LPCSTR)olStatusText);
	}


    // remember the bar and update the status bar (if exist)
	imCurrentBar = ipBarno;
	imBkBarno = ipBkBarno;
}

// Start moving/resizing
BOOL EquipmentGantt::BeginMovingOrResizing(CPoint point)
{
	bmContextBarSet = FALSE;

    int itemID = GetItemFromPoint(point);
    int ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
	int ilBkBar = GetBkBarnoFromPoint(itemID, point);
    UpdateBarStatus(itemID, ilBarno, ilBkBar);

	umResizeMode = HTNOWHERE;
    if (ilBarno != -1)
        umResizeMode = HitTest(itemID, ilBarno, point);

    if (umResizeMode != HTNOWHERE)
    {
        SetCapture();
        if (umResizeMode == HTCAPTION)
            SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
        else
            SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));

        // Calculate size of current bar
        EQUIPMENT_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, ilBarno);
		if (prlBar)
		{
			CRect rcItem;
			GetItemRect(itemID, &rcItem);
			int left = (int)GetX(prlBar->StartTime);
			int right = (int)GetX(prlBar->EndTime);
			int top = rcItem.top + imGutterHeight + (prlBar->OverlapLevel * imOverlapHeight);
			int bottom = top + imBarHeight;

			rmContextBar = *prlBar;
			bmContextBarSet = TRUE;

			omPointResize = point;
			omRectResize = CRect(left, top, right, bottom);
			CClientDC dc(this);

			// Update the time band (two vertical yellow lines)
			TIMEPACKET olTimePacket;
			olTimePacket.StartTime = GetCTime(omRectResize.left);
			olTimePacket.EndTime = GetCTime(omRectResize.right);
			ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);
			DrawFocusRect(&dc, &omRectResize);	// first time focus rectangle
		}
    }

    return (umResizeMode != HTNOWHERE);
}

// Update the resizing/moving rectangle
BOOL EquipmentGantt::OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown)
{
    CClientDC dc(this);

    if (bpIsLButtonDown)    // still resizing/moving?
    {
		CRect client;
		GetClientRect(client);
		client.left = imVerticalScaleWidth;
		if (!client.PtInRect(point))
		{
	        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_NO));
			return umResizeMode != HTNOWHERE;
		}
		
		// reset to previous cursor
		if (GetCursor() == AfxGetApp()->LoadStandardCursor(IDC_NO))
		{
			if (umResizeMode == HTCAPTION)
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
			else
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
		}

        DrawFocusRect(&dc, &omRectResize);	// delete previous focus rectangle
        switch (umResizeMode)
        {
        case HTCAPTION: // moving mode
            omRectResize.OffsetRect(point.x - omPointResize.x, 0);
            break;
        case HTLEFT:    // resize left border
            omRectResize.left = min(point.x, omRectResize.right);
            break;
        case HTRIGHT:   // resize right border
            omRectResize.right = max(point.x, omRectResize.left);
            break;
        }
        omPointResize = point;

		// Update the time band (two vertical yellow lines)
		TIMEPACKET olTimePacket;
		olTimePacket.StartTime = GetCTime(omRectResize.left);
		olTimePacket.EndTime = GetCTime(omRectResize.right);
		ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);
	    DrawFocusRect(&dc, &omRectResize);

		// Update the indicator also while moving the bar
		// Be careful, I assume that there will be one and only one indicator for a bar.
		if(bmContextBarSet)
		{
			pomTimeScale->RemoveAllTopScaleIndicator();
			pomTimeScale->AddTopScaleIndicator(GetCTime(omRectResize.left),
				GetCTime(omRectResize.right), rmContextBar.Indicators[0].Color);
			pomTimeScale->DisplayTopScaleIndicator();
		}
    }
    else
    {
		CRect client;
		GetClientRect(client);
		client.left = imVerticalScaleWidth;
		umResizeMode = HTNOWHERE;   // no more moving/resizing
		if (client.PtInRect(point))
		{
			if(bmContextBarSet)
			{
				DataSet::ChangeTimeOfJob(rmContextBar.JobUrno,GetCTime(omRectResize.left), GetCTime(omRectResize.right),TRUE);
				bmContextBarSet = FALSE;
				pomTimeScale->RemoveAllTopScaleIndicator();
				imCurrentBar = -1;
			}
		}
		else
		{
			if (bmContextBarSet)
			{
		        DrawFocusRect(&dc, &omRectResize);	// delete previous focus rectangle

				// Update the time band (two vertical yellow lines)
				TIMEPACKET olTimePacket;
				olTimePacket.StartTime = rmContextBar.StartTime;
				olTimePacket.EndTime   = rmContextBar.EndTime;
				ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);

				bmContextBarSet  = FALSE;
				imCurrentBar = -1;
			}
		}

        ReleaseCapture();
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		//SetMarkTime(TIMENULL, TIMENULL);
    }

    DrawFocusRect(&dc, &omRectResize);
    return (umResizeMode != HTNOWHERE);
}

// return the item ID of the list box based on the given "point"
int EquipmentGantt::GetItemFromPoint(CPoint point) const
{
    CRect rcItem;
    for (int itemID = GetTopIndex(); itemID < GetCount(); itemID++)
    {
        GetItemRect(itemID, &rcItem);
        if (rcItem.PtInRect(point))
            return itemID;
    }
    return -1;
}

// Return the bar number of the list box based on the given point
int EquipmentGantt::GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft, int ipPreRight)
{
    if (ipLineno == -1) // this checking was added after some bugs found
        return -1;

    // Calculate possible levels for vertical position
    CRect rcItem;
    GetItemRect(ipLineno, &rcItem);
    point.y -= rcItem.top + imGutterHeight;
    int level1 = (point.y < imBarHeight)? 0: ((point.y - imBarHeight) / imOverlapHeight) + 1;
    int level2 = (point.y < 0)? -1: point.y / imOverlapHeight;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-ipPreLeft);
    CTime time2 = GetCTime(point.x+ipPreRight);

    return pomViewer->GetBarnoFromTime(imGroupno, ipLineno, time1, time2, level1, level2);
}

// Return the background bar number of the list box based on the given point
int EquipmentGantt::GetBkBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft, int ipPreRight)
{
    if (ipLineno == -1) // this checking was added after some bugs found
        return -1;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-ipPreLeft);
    CTime time2 = GetCTime(point.x+ipPreRight);

    return pomViewer->GetBkBarnoFromTime(imGroupno, ipLineno, time1, time2);
}


// Return the hit test area code (determine the location of the cursor).
// Possible codes are HTLEFT, HTRIGHT, HTNOWHERE (out-of-bar), HTCAPTION (bar body)
// 
UINT EquipmentGantt::HitTest(int ipLineno, int ipBarno, CPoint point)
{
    if (ipLineno == -1 || ipBarno == -1)    // this checking was added after some bugs found
        return HTNOWHERE;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-imBorderPreLeft);
    CTime time2 = GetCTime(point.x+imBorderPreRight);

    // Check against each possible case of hit test areas.
    // Since most people like to drag things to the right, we check right border before left.
    // Be careful, if the bar is very small, the user may not be able to get HTCAPTION or HTLEFT.
    //
    EQUIPMENT_BARDATA *prlBar = pomViewer->GetBar(imGroupno, ipLineno, ipBarno);
    if (IsBetween(prlBar->EndTime, time1, time2))
        return HTRIGHT;
    else if (IsBetween(prlBar->StartTime, time1, time2))
        return HTLEFT;
    else if (IsOverlapped(time1, time2, prlBar->StartTime, prlBar->EndTime))
        return HTCAPTION;

    return HTNOWHERE;
}


LONG EquipmentGantt::OnDragOver(UINT i, LONG l)
{
	CPoint point;
    ::GetCursorPos(&point);
    ScreenToClient(&point);    
    UpdateGanttChart(point, FALSE);

	int itemID;
	if(imGroupno == -1 || (itemID = GetItemFromPoint(point)) == -1)
		return -1L; // group or line not found

	CString olEquipment = pomViewer->GetLineText(imGroupno, itemID);
	EQUIPMENT_LINEDATA *prlLine = pomViewer->GetLine(imGroupno, itemID);
	
	// check if the pool/emp can be dropped on this equipment
	switch(m_DragDropTarget.GetDataClass())
	{
		case DIT_DUTYBAR:
		{
			JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(m_DragDropTarget.GetDataDWord(0));
			if(prlPoolJob == NULL)
			{
				DisplayTextOnStatusBar("Error - Pool job not found");
				return -1;
			}
			else if(!ogBasicData.IsInPool(ALLOCUNITTYPE_EQUIPMENT,olEquipment,prlPoolJob->Alid))
			{
				CString olWarning;
				olWarning.Format("The equipment <%s> is not in the pool <%s>",olEquipment,prlPoolJob->Alid);
				DisplayTextOnStatusBar(olWarning);
				return -1L;
			}
			break;
		}
		case DIT_DEMANDBAR:
		{
			bool blEquDemFound = false;
			DEMANDDATA *prlDem = NULL;
			for(int ilD = 0; ilD < m_DragDropTarget.GetDataCount(); ilD++)
			{
				if((prlDem = ogDemandData.GetDemandByUrno(m_DragDropTarget.GetDataDWord(ilD))) != NULL && 
					ogDemandData.DemandIsOfType(prlDem, EQUIPMENTDEMANDS))
					blEquDemFound = true;
			}
			if(!blEquDemFound)
			{
				DisplayTextOnStatusBar("Demand is not an equipment demand");
				return -1;
			}
			break;
		}
		case DIT_FLIGHTBAR:
		{
			break;
		}
		case DIT_JOBBAR:
		{
			JOBDATA *prlJob = ogJobData.GetJobByUrno(m_DragDropTarget.GetDataDWord(0));
			if(prlJob == NULL)
			{
				DisplayTextOnStatusBar("Job not found");
				return -1;
			}
			else if(strcmp(prlJob->Jtco,JOBEQUIPMENT) && strcmp(prlJob->Jtco,JOBFID))
			{
				DisplayTextOnStatusBar("Incorrect job type");
				return -1;
			}
			else if(prlJob->Uequ == prlLine->EquUrno)
			{
				DisplayTextOnStatusBar("Can reassign the job to the same equipment!");
				return -1;
			}
			break;
		}
		case DIT_FID:
		{
			DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(m_DragDropTarget.GetDataDWord(0));
			if(prlDemand == NULL)
			{
				DisplayTextOnStatusBar("Error - Demand not found");
				return -1;
			}
			else if(!ogDemandData.DemandIsOfType(prlDemand, EQUIPMENTDEMANDS))
			{
				DisplayTextOnStatusBar("Flight Independent Demand is not an equipment demand");
				return -1;
			}
			break;
		}
		default:
			DisplayTextOnStatusBar("Drop Object Type not handled here!");
			return -1;
			break;
	}

	return 0L;	// drop on equipment
}

void EquipmentGantt::DisplayTextOnStatusBar(CString opText)
{
	if(pomStatusBar != NULL)
	{
		pomStatusBar->SetPaneText(0, (LPCSTR) opText);
	}
}

LONG EquipmentGantt::OnDrop(UINT wParam, LONG lParam)
{
	LONG llRc = -1;
    int ilClass = m_DragDropTarget.GetDataClass(); 
	switch(ilClass)
	{
		case DIT_DUTYBAR:
			llRc = ProcessDropEmployee(&m_DragDropTarget, wParam);
			break;
		case DIT_FLIGHTBAR:
			llRc = ProcessDropFlight(&m_DragDropTarget, wParam);
			break;
		case DIT_DEMANDBAR:
			llRc = ProcessDropDemand(&m_DragDropTarget, wParam);
			break;
		case DIT_JOBBAR:
			llRc = ProcessDropEquJob(&m_DragDropTarget, wParam);
			break;
		case DIT_FID:
			llRc = ProcessDropFidDemand(&m_DragDropTarget, wParam);
			break;
		default:
			break;
	}

	return llRc;
}

EQUIPMENT_LINEDATA *EquipmentGantt::GetLineFromPoint()
{
	EQUIPMENT_LINEDATA *prlLine = NULL;
	CPoint olPoint;
    ::GetCursorPos(&olPoint);
    ScreenToClient(&olPoint);    

	int ilLineno = GetItemFromPoint(olPoint);
	if(ilLineno != -1)
	{
		prlLine = pomViewer->GetLine(imGroupno, ilLineno);
	}
	return prlLine;
}

LONG EquipmentGantt::ProcessDropDemand(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	EQUIPMENT_LINEDATA *prlLine = GetLineFromPoint();
	if(prlLine == NULL)
		return -1L;

	DEMANDDATA *prlDem = NULL;
	for(int ilD = 0; ilD < popDragDropCtrl->GetDataCount(); ilD++)
	{
		if((prlDem = ogDemandData.GetDemandByUrno(popDragDropCtrl->GetDataDWord(ilD))) != NULL && 
			ogDemandData.DemandIsOfType(prlDem, EQUIPMENTDEMANDS))
		{
			ogDataSet.CreateEquipmentJob(this, prlLine->EquUrno, prlDem->Urno);
		}
	}

	return 0L;
}

LONG EquipmentGantt::ProcessDropFidDemand(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	EQUIPMENT_LINEDATA *prlLine = GetLineFromPoint();
	if(prlLine == NULL)
		return -1L;


	long llDemandUrno = popDragDropCtrl->GetDataDWord(0);
	DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(llDemandUrno);
	if(prlDem == NULL)
		return -1L;

	ogDataSet.CreateFidEquipmentJob(GetParent(), prlLine->EquUrno, llDemandUrno);

	return 0L;
}

LONG EquipmentGantt::ProcessDropEquJob(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	EQUIPMENT_LINEDATA *prlLine = GetLineFromPoint();
	if(prlLine == NULL)
		return -1L;

	JOBDATA *prlEquJob = ogJobData.GetJobByUrno(popDragDropCtrl->GetDataDWord(0));
	if(prlEquJob == NULL)
		return -1L;

	ogDataSet.ReassignEquipmentJob(GetParent(), prlEquJob->Urno, prlLine->EquUrno);

	return 0L;
}

// flight dropped from gate gantt
LONG EquipmentGantt::ProcessDropFlight(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	EQUIPMENT_LINEDATA *prlLine = GetLineFromPoint();
	if(prlLine == NULL)
		return -1L;

	long llFlightUrno = popDragDropCtrl->GetDataDWord(0);
	long llRotationUrno = popDragDropCtrl->GetDataDWord(1);
	CString olAloc = popDragDropCtrl->GetDataString(2);
	CString olAlid = popDragDropCtrl->GetDataString(3);

	ogDataSet.CreateEquipmentJob(GetParent(), prlLine->EquUrno, 0L, llFlightUrno, llRotationUrno, olAloc, olAlid);

	return 0L;
}

// employee dropped onto a piece of equipment so create a fast link between employee and equipment
LONG EquipmentGantt::ProcessDropEmployee(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	EQUIPMENT_LINEDATA *prlLine = GetLineFromPoint();
	if(prlLine == NULL)
		return -1L;

	long llRc = -1L;

	// list of employees
	CString olEmpList;
	CTime olBegin = TIMENULL, olEnd = TIMENULL;
	JOBDATA *prlJob;
	CDWordArray olPoolJobUrnos;
	int ilDataCount = popDragDropCtrl->GetDataCount() - 1;
	for(int ilC = 0; ilC < ilDataCount; ilC++)
	{
		if((prlJob = ogJobData.GetJobByUrno(popDragDropCtrl->GetDataDWord(ilC))) != NULL)
		{
			olPoolJobUrnos.Add(prlJob->Urno);
			if(olBegin == TIMENULL || prlJob->Acfr < olBegin)
			{
				olBegin = prlJob->Acfr;
			}
			if(olEnd == TIMENULL || prlJob->Acto > olEnd)
			{
				olEnd = prlJob->Acto;
			}
			CString olEmpName = ogEmpData.GetEmpName(prlJob->Ustf);
			if(olEmpList.IsEmpty())
			{
				olEmpList = olEmpName;
			}
			else
			{
				olEmpList += CString("\n") + olEmpName;
			}
		}
	}
	if(olPoolJobUrnos.GetSize() > 0)
	{
		// get the duration of a fast link job
		DChange olJobChangeDialog(this,olEmpList,olBegin,olEnd);
		olJobChangeDialog.omCaption.Format("%s: %s",GetString(IDS_FASTLINKCAPTION),prlLine->Text);
		if(olJobChangeDialog.DoModal() == IDOK)
		{
			ogDataSet.CreateEquipmentFastLink(this, olPoolJobUrnos, prlLine->EquUrno, olJobChangeDialog.m_Acfr, olJobChangeDialog.m_Acto);
			llRc = 0L;
		}
	}	

    return llRc;
}

void EquipmentGantt::DrawDottedLines(CDC *pDC, int itemID, const CRect &rcItem)
{
    CBitmap Bitmap;
	CTime olTime, olTmpTime;

	// select dotted pen
	CPen SepPen(PS_DOT, 1, BLACK);
	COLORREF olOldBkColor = pDC->SetBkColor(SILVER);
	CPen *pOldPen = pDC->SelectObject(&SepPen);
	
	// dotted horizontal lines
	CRect olRect(rcItem);
	int ilHeight = 0;
	CRect olTmpRect = olRect;
	{
		olRect.bottom = olRect.top + GetLineHeight(itemID);
        pDC->MoveTo(olRect.left + imVerticalScaleWidth + imVerticalScaleIndent, olRect.bottom - 1);
        pDC->LineTo(olRect.right, olRect.bottom - 1);
		olRect.top += GetLineHeight(itemID);
	}    
                     
	pDC->SelectObject(pOldPen);	
	pDC->SetBkColor(olOldBkColor);
}

int EquipmentGantt::OnToolHitTest(CPoint point, TOOLINFO* pTI) const
{
	static int   ilPrevLine =-1;
	int	ilLine = GetItemFromPoint(point);
	EQUIPMENT_LINEDATA *prlLine = NULL;

	if(ilLine > -1 && point.x < imVerticalScaleWidth && 
		(prlLine = pomViewer->GetLine(imGroupno, ilLine)) != NULL)
	{
		
		if (ilLine != ilPrevLine)
		{
			ilPrevLine = ilLine;
			return -1;
		}


		CRect olRect;
		GetItemRect(ilLine,&olRect);
		olRect.right = imVerticalScaleWidth;
		pTI->hwnd = m_hWnd;
		pTI->uId = (UINT)(ilLine + 1);		
		pTI->rect = olRect;	
		
		EQUDATA *polEquipmentData = ogEquData.GetEquByUrno(prlLine->EquUrno);
		CCSPtrArray <EQADATA> olEqaList;
		EQADATA* polEqaData;
		ogEqaData.GetEqasByUequ(prlLine->EquUrno, olEqaList);
		
		if(polEquipmentData == NULL)
		{
			return -1;
		}

		CString olEquType;
		EQTDATA* polEqtData = ogEqtData.GetEqtByUrno(polEquipmentData->Gkey);
		if(polEqtData != NULL)
		{
			olEquType = CString(polEqtData->Code);
		}

		CString olText = CString("Code :") + CString(polEquipmentData->Gcde) + _T('\n');
		olText += CString("Type :") + olEquType + _T('\n');
		olText += CString("Qualification :") + CString(polEquipmentData->Crqu) + _T('\n');
		olText += CString("Remark :") + CString(polEquipmentData->Rema) + _T('\n');
		olText += CString("Parking Stand :") + CString(polEquipmentData->Eqps) + _T('\n') + _T('\n') + CString("Attributes:");
		for(int i = 0 ; i < olEqaList.GetSize() ; i++)
		{
			polEqaData = &olEqaList[i];
			if(polEqaData != NULL)
			{
				olText += _T('\n') + CString(polEqaData->Name) + CString(": ") + CString(polEqaData->Valu);
			}
		}

		_AFX_THREAD_STATE* pThreadState = AfxGetThreadState();
	    CToolTipCtrl *pToolTip = pThreadState->m_pToolTip;
	    pToolTip->SetMaxTipWidth(1200);

		pToolTip->SendMessage(TTM_SETDELAYTIME,TTDT_AUTOPOP,30000);
		pToolTip->SendMessage(TTM_SETDELAYTIME,TTDT_INITIAL,100);		
		pTI->lpszText = new char[olText.GetLength()+1];
		strcpy(pTI->lpszText,olText);
		return 1;
	}
	return -1;
}
