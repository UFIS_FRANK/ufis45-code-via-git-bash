// PrepPrn.cpp : implementation file
//

#include <stdafx.h>
#include <resource.h>
#include <PrePlanTeamPrintDlg.h>
#include <Ccsglobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PrePlanTeamPrintDialog dialog


PrePlanTeamPrintDialog::PrePlanTeamPrintDialog(CWnd* pParent /*=NULL*/)
	: CDialog(PrePlanTeamPrintDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(PrePlanTeamPrintDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

PrePlanTeamPrintDialog::~PrePlanTeamPrintDialog()
{
}

void PrePlanTeamPrintDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PrePlanTeamPrintDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PrePlanTeamPrintDialog, CDialog)
	//{{AFX_MSG_MAP(PrePlanTeamPrintDialog)
	ON_BN_CLICKED(IDC_ADDTEAM, OnAddteam)
	ON_BN_CLICKED(IDC_REMOVETEAM, OnRemoveteam)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PrePlanTeamPrintDialog message handlers

BOOL PrePlanTeamPrintDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING33009)); 

	CWnd *polWnd = GetDlgItem(IDC_PRINTTEAM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33010));
	}
	polWnd = GetDlgItem(IDC_ADDTEAM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32946));
	}
	polWnd = GetDlgItem(IDC_REMOVETEAM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32947));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}

	// TODO: Add extra initialization here
	CListBox *lbTeam = (CListBox*)GetDlgItem(IDC_TEAMS);

	lbTeam->ResetContent();
	for(int i = 0; i < m_TeamArray->GetSize(); i++ ) {
		lbTeam->AddString( m_TeamArray->GetAt(i) );
	}
	lbTeam->SetSel( 0, TRUE );

	CenterWindow();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void PrePlanTeamPrintDialog::OnAddteam() 
{
	// TODO: Add your control notification handler code here
	int nSelectCount;
	CListBox *lbTeam = (CListBox*)GetDlgItem(IDC_TEAMS);
	CListBox *lbTeamSel = (CListBox*)GetDlgItem(IDC_TEAMSELECT);

	nSelectCount = lbTeam->GetSelCount();
	LPINT rgIndex = (LPINT)malloc((sizeof(int) * nSelectCount));
	LPTSTR lpszBuffer = (LPTSTR)malloc( 20 );
	nSelectCount = lbTeam->GetSelItems( nSelectCount, rgIndex );

	for(; nSelectCount > 0; nSelectCount-- ) {
		lbTeam->GetText( rgIndex[nSelectCount - 1], lpszBuffer );
		lbTeamSel->AddString( lpszBuffer );
		lbTeam->DeleteString( rgIndex[nSelectCount - 1] );		
	}
	lbTeam->SetSel( 0, TRUE );

	free( lpszBuffer );
	free( rgIndex );

}

void PrePlanTeamPrintDialog::OnRemoveteam() 
{
	// TODO: Add your control notification handler code here
	int nSelectCount;
	CListBox *lbTeam = (CListBox*)GetDlgItem(IDC_TEAMS);
	CListBox *lbTeamSel = (CListBox*)GetDlgItem(IDC_TEAMSELECT);

	nSelectCount = lbTeamSel->GetSelCount();
	LPINT rgIndex = (LPINT)malloc((sizeof(int) * nSelectCount));
	LPTSTR lpszBuffer = (LPTSTR)malloc( 20 );
	nSelectCount = lbTeamSel->GetSelItems( nSelectCount, rgIndex );

	for(; nSelectCount > 0; nSelectCount-- ) {
		lbTeamSel->GetText( rgIndex[nSelectCount - 1], lpszBuffer );
		lbTeam->AddString( lpszBuffer );
		lbTeamSel->DeleteString( rgIndex[nSelectCount - 1] );		
	}
	lbTeamSel->SetSel( 0, TRUE );

	free( lpszBuffer );
	free( rgIndex );
}

void PrePlanTeamPrintDialog::OnOK() 
{
	// TODO: Add extra validation here
	int nCount;
	m_TeamSelect.RemoveAll();
	CListBox *lbTeamSel = (CListBox*)GetDlgItem(IDC_TEAMSELECT);
	LPTSTR lpszBuffer = (LPTSTR)malloc( 20 );
	nCount = lbTeamSel->GetCount();
	for(; nCount > 0; nCount-- ) {
		lbTeamSel->GetText( nCount - 1, lpszBuffer );
		m_TeamSelect.Add( lpszBuffer );
	}
	free( lpszBuffer );

	CDialog::OnOK();
}
