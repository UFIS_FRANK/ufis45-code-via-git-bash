/////////////////////////////////////////////////////////////
// File: Search.h
//
// Class: Search
//
// Author: MWO
//

#ifndef _SEARCH_H_
#define _SEARCH_H_

#include <CedaFlightData.h>

class Search
{
public:
	Search();
	~Search();

	CCSPtrArray <FLIGHTDATA> omFlights;
	CCSPtrArray <EMPDATA> omEmps;
	CCSPtrArray <SHIFTDATA> omShifts;
	CMapPtrToPtr omFlightUrnoMap;

	int FilterEmployees();
	int FilterFlights();
	BOOL IsFlightOk(const char *pcpFlight, char *pcpFilter);
	CTime GetFlightDay(FLIGHTDATA &ropFlight);

	BOOL CompareName(const char *pcpName1,const char *pcpName2);
	BOOL StrFind(const char *pcpMask,const char*pcpValue) const; 
	bool AddFlight(FLIGHTDATA *prpFlight);
};

extern Search ogSearch;
#endif //_SEARCH_H_