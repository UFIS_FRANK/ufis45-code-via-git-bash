// EquipmentGantt.h : header file
//

#ifndef __EQUIPMENTGANTT_H__
#define __EQUIPMENTGANTT_H__

// Special time value definition
#ifndef TIMENULL
#define TIMENULL    CTime((time_t)-1)
#endif

/////////////////////////////////////////////////////////////////////////////
// EquipmentGantt window
// IDs 15-30 are used for conflict confirmation !!
#define EQ_DISPALYDETAILDLG 16
#define EQ_DEBUGINFO_MENUITEM 17
#define EQ_EDITFASTLINK_MENUITEM 18
#define EQ_DELETEFASTLINK_MENUITEM 19
#define EQ_EDITJOB_MENUITEM 20
#define EQ_DELETEJOB_MENUITEM 21

#define EQ_ACCEPT_ALL_CONFLICTS 22
#define EQ_MAXNUMCONFLICTS 20

#define EQ_LASTJOBINFO_MENUITEM 23

#define EQ_EDITFASTLINK (EQ_ACCEPT_ALL_CONFLICTS + EQ_MAXNUMCONFLICTS + 1)
#define EQ_DELETEFASTLINK (EQ_EDITFASTLINK + EQ_MAXFASTLINKS + 1)
#define EQ_MAXFASTLINKS 20

class EquipmentGantt: public CListBox
{
// Operations
public:
	EquipmentGantt::EquipmentGantt(EquipmentDiagramViewer *popViewer = NULL, int ipGroup = 0,
        int ipVerticalScaleWidth = 0, int ipVerticalScaleIndent = 15,
        CFont *popVerticalScaleFont = NULL, CFont *popGanttChartFont = NULL,
        int ipGutterHeight = 2, int ipOverlapHeight = 4,
        COLORREF lpVerticalScaleTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightVerticalScaleTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW),
        COLORREF lpGanttChartTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightGanttChartTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW));
	~EquipmentGantt();

    void SetViewer(EquipmentDiagramViewer *popViewer, int ipGroup);
    void SetVerticalScaleWidth(int ipWidth);
	void SetVerticalScaleIndent(int ipIndent);
    void SetFonts(int index1, int index2);
    void SetGutters(int ipGutterHeight, int ipOverlapHeight);
    void SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);
    void SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);

    int GetGanttChartHeight();
    int GetLineHeight(int ipMaxOverlapLevel);

    BOOL Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID = 0);
    void SetStatusBar(CStatusBar *popStatusBar);
	void SetTimeScale(CTimeScale *popTimeScale);
    void SetBorderPrecision(int ipBorderPrecision);

    // This group of function will automatically repaint the window
    void SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling = TRUE);
    void SetDisplayStart(CTime opDisplayStart);
    void SetCurrentTime(CTime opCurrentTime);
    void SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd);

    void RepaintVerticalScale(int ipLineno = -1, BOOL bpErase = TRUE);
    void RepaintGanttChart(int ipLineno = -1,
            CTime opStartTime = TIMENULL, CTime opEndTime = TIMENULL, BOOL bpErase = TRUE);
    void RepaintItemHeight(int ipLineno);
	int OnToolHitTest(CPoint point, TOOLINFO* pTI) const;

// Attributes
private:
    EquipmentDiagramViewer *pomViewer;
    int imGroupno;              // index to a group in the viewer
    int imVerticalScaleWidth;
	int imVerticalScaleIndent;	// number of pixels used to indent text in VerticalScale
    CFont *pomVerticalScaleFont;
    CFont *pomGanttChartFont;
    int imBarHeight;            // calculated when SetGanttChartFont()
    int imGutterHeight;         // space between the GanttLine and the topmost-level bar
    int imLeadingHeight;        // space between the bar and the border
    int imOverlapHeight;        // space between the overlapped bar
    COLORREF lmVerticalScaleTextColor;
    COLORREF lmVerticalScaleBackgroundColor;
    COLORREF lmHighlightVerticalScaleTextColor;
    COLORREF lmHighlightVerticalScaleBackgroundColor;
    COLORREF lmGanttChartTextColor;
    COLORREF lmGanttChartBackgroundColor;
    COLORREF lmHighlightGanttChartTextColor;
    COLORREF lmHighlightGanttChartBackgroundColor;

    CTime omDisplayStart;
    CTime omDisplayEnd;
    int imWindowWidth;          // in pixels, for pixel/time ratio (updated by WM_SIZE)
    BOOL bmIsFixedScaling;
        // There are two mode for updating the display window when WM_SIZE was sent,
        // variabled-scaling and fixed-scaling. The mode of this operation will be
        // given in the method SetDisplayWindow().

    CTime omCurrentTime;                    // the red vertical time line
    CTime omMarkTimeStart, omMarkTimeEnd;   // two yellow vertical time lines

    CStatusBar *pomStatusBar;   // the status bar where the notification message goes
	CTimeScale *pomTimeScale;	// the time scale for top scale indicators display

    BOOL bmIsMouseInWindow;
    BOOL bmIsControlKeyDown;
    int imHighlightLine;        // the current line that painted with highlight color
    int imCurrentBar;           // the current bar that the user places mouse over
	int imBkBarno;

	int imContextItem;
	int imContextBarNo;
	EQUIPMENT_BARDATA rmContextBar;  // Save bar for Context Menu handling
	BOOL bmContextBarSet;
	BOOL bmActiveLineSet;		// There is a selected line ?
	long lmActiveUrno;			// Urno of last selected line
	EQUIPMENT_BKBARDATA rmActiveBkBar;
	BOOL			bmActiveBkBar;

	// Id 1-Oct-96
	// This will help us create a virtual double click on a double click after SetCursorPos().
	CPoint omLastClickedPosition;

    UINT umResizeMode;          // HTNOWHERE, HTCAPTION (moving), HTLEFT, or HTRIGHT
    CPoint omPointResize;       // the point where moving/resizing begin
    CRect omRectResize;         // current focus rectangle for moving/resizing
    int imBorderPreLeft;        // define the sensitivity when user detect bar border
    int imBorderPreRight;       // define the sensitivity when user detect bar border

	WCHAR *m_pwchTip;
	TCHAR *m_pchTip;

// Implementation
public:
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
    virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);

private:
	void DrawBackgroundBars(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
    void DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem);
    void DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
    void DrawTimeLines(CDC *pDC, int top, int bottom);
	void DrawFocusRect(CDC *pDC, const CRect &rect);
	void DrawDottedLines(CDC *pDC, int itemID, const CRect &rcItem);

protected:
    // Generated message map functions
    //{{AFX_MSG(EquipmentGantt)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg LONG OnUserKeyDown(UINT wParam, LONG lParam);
	afx_msg LONG OnUserKeyUp(UINT wParam, LONG lParam);
	afx_msg void OnMenuDesplayDetailDlg();
	afx_msg void OnMenuEditFastLink(UINT);
	afx_msg void OnMenuDeleteFastLink(UINT);
    afx_msg void OnMenuDebugInfo();
    afx_msg void OnMenuAcceptConflict(UINT);
	afx_msg void OnMenuEditJob();
	afx_msg void OnMenuDeleteJob();
	afx_msg void OnMenuLastJobInfo(); //PRF 8998
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()


private:
	CMapPtrToPtr omAcceptConflictsMap, omAcceptDepConflictsMap;
	CMapPtrToPtr omFastLinkMenuMap;
    void UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown);
    void UpdateBarStatus(int ipLineno, int ipBarno, int ipBkBarno);
    BOOL BeginMovingOrResizing(CPoint point);
    BOOL OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown);
    int GetItemFromPoint(CPoint point) const;
    int GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1);
    int GetBkBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1);
    UINT HitTest(int ipLineno, int ipBarno, CPoint point);
        // HitTest will use the precision defined in "imBorderPrecision"
	void DragEquipmentBegin(int ipLineno);
	void DragEquJobBegin(int ipLineno, int ipBarno);

private:
	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;
	EQUIPMENT_LINEDATA *GetLineFromPoint();
	LONG ProcessDropEmployee(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropDemand(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropFidDemand(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropFlight(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropEquJob(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	void DisplayTextOnStatusBar(CString opText);
};

/////////////////////////////////////////////////////////////////////////////

#endif
