// ppviewer.cpp : implementation file
//
// Modification History:
// 19-Sep-96	Damkerng	Change many places since our database is changed.
//							On this change, special flight job was changed from type
//							FLT to FLS. The field PRID which contains a word "SPECIAL"
//							is not used any more. The field FLUR which was used for
//							storing shift URNO now moved to the field SHUR.
//							Check these places out by searching for "Id 19-Sep-96"

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <cviewer.h>
#include <table.h>
#include <CedaShiftData.h>
#include <CedaShiftTypeData.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <CedaRnkData.h>
#include <ccsddx.h>
#include <AllocData.h>
#include <BasicData.h>
#include <PrePlanTableViewer.h>
#include <dataset.h>
#include <CedaOdaData.h>
#include <Search.h>
#include <CedaSpeData.h>
#include <PrePlanTable.h> //Singapore
#include <CedaShiftTypeData.h>  //Singapore
extern bool BGS;

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))


// Grouping and Sorting criterias definition:
// This will make the program faster a little bit because it will replace string comparison.
enum {
    BY_SHIFTBEGIN,
    BY_SHIFTEND,
    BY_SHIFTCODE,
    BY_TEAM,
    BY_NAME,
    BY_RANK,
    BY_NONE
};

// To know which rank is higher in comparison of two staff, I provide a colon-separated
// string from the lowest rank to the highest rank. By searching for a string ":XX:"
// in "pclRanks", if XX is the given rank, we could easily see which rank is higher.
// By using the same technic, we could see if the given rank is a manager by searching
// for a string ":XX:" in "ogBasicData.omManager".
//

/////////////////////////////////////////////////////////////////////////////
// PrePlanTableViewer
//
// Requirements:
// In the PrePlanTable we have 3 filters, by Rank, Team, and ShiftCode.
// (May be also Pools in the future. For the default pools of each employee,
// we have to look in the field EMPCKI.DISP.)
// It may be sorted by a combination of ShiftBegin, ShiftEnd, ShiftCode, Team, Name,
// Rank, and none.
// It will be always groupped by the first sorting criteria.
//
// Implementation:
// At the program start up, we will create a default view for each table if the view
// named <default> does not exist before.
//
// When the viewer is constructed or everytimes the view changes, we will clear
// everything from the viewer buffer and do the insertion sorting. We will insert
// the record only if it satisfies our filter. Conceptually we will filter the record
// first, then sort it (by insertion sort), and group it.
//
// To let the filter work fast, we will implement a CMapStringToPtr for every filter
// condition. For example, let's say that we have a CStringArray for every possible
// values of a filter. We will have a CMapStringToPtr which could tell us the given
// key value is selected in the filter or not.
//
// Methods for change the view:
// ChangeViewTo         Change view, reload everything from Ceda????Data
// PrepareGroupping     Prepare the enumerate group value
// PrepareFilter        Prepare CMapStringToPtr or CMapPtrToPtr for filtering
// PrepareSorter        Prepare sort criterias before sorting take place
// IsPassFilter         Return TRUE if the given record satisfies the filter
// CompareGroup         Return 0 or -1 or 1 as the result of comparison of two groups
// CompareShift         Return 0 or -1 or 1 as the result of comparison of two lines

PrePlanTableViewer::PrePlanTableViewer()
{
    SetViewerKey("PPTab");
	
    pomTable = NULL;
    ogCCSDdx.Register(this, JOB_NEW, CString("PPVIEWER"), CString("Job New"), PrePlanTableCf);
    ogCCSDdx.Register(this, JOB_CHANGE, CString("PPVIEWER"), CString("Job Change"), PrePlanTableCf);
    ogCCSDdx.Register(this, JOB_DELETE, CString("PPVIEWER"), CString("Job Delete"), PrePlanTableCf);
    ogCCSDdx.Register(this, SHIFT_NEW, CString("PPVIEWER"), CString("Shift New"), PrePlanTableCf);
    ogCCSDdx.Register(this, SHIFT_CHANGE, CString("PPVIEWER"), CString("Shift Change"), PrePlanTableCf);
    ogCCSDdx.Register(this, SHIFT_DELETE, CString("PPVIEWER"), CString("Shift Delete"), PrePlanTableCf);
	ogCCSDdx.Register(this, SHIFT_SELECT_PREPLAN, CString("PPVIEWER"), CString("Shift Select"), PrePlanTableCf);
	bmIsFromSearch = FALSE;
	bmUseAllShiftCodes = false;
	bmUseAllRanks = false;
	bmUseAllPermits = false;
	bmUseAllTeams = false;

	CTime olDay = ogBasicData.GetDisplayDate();
	omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omEndTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);
	omSday = omStartTime.Format("%Y%m%d");
}

PrePlanTableViewer::~PrePlanTableViewer()
{
    ogCCSDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void PrePlanTableViewer::Attach(CTable *popTable,BOOL bpIsFromSearch)
{
    pomTable = popTable;
	bmIsFromSearch = bpIsFromSearch;
}

CString PrePlanTableViewer::ChangeViewTo(const char *pcpViewName, BOOL bpIsCompleteTeam,BOOL bpAbsent)
{
	ogCfgData.rmUserSetup.VPLV = pcpViewName;
    SelectView(pcpViewName);
    bmIsCompleteTeam = bpIsCompleteTeam;
    bmDisplayAbsentEmps = bpAbsent;
    PrepareGrouping();
    PrepareSorter();
    PrepareFilter();

    DeleteAll();    // remove everything
    MakeLines();
    MakeAssignments();

    UpdateDisplay();
	return omDate;
}

CString PrePlanTableViewer::ChangeView()
{
    bmIsCompleteTeam = FALSE;
    DeleteAll();    // remove everything

	CTime olT = ogBasicData.GetTime();
	omStartTime = CTime(olT.GetYear(),olT.GetMonth(),olT.GetDay(),0,0,0);
	omEndTime  = CTime(olT.GetYear(),olT.GetMonth(),olT.GetDay(),23,59,0);
	omSday = omStartTime.Format("%Y%m%d");

    int ilShiftCount = ogSearch.omShifts.GetSize();
    for (int ilLc = 0; ilLc < ilShiftCount; ilLc++)
    {
        SHIFTDATA *prlShiftData = &ogSearch.omShifts[ilLc];
        MakeLine(prlShiftData);
    }
	if (bmIsFromSearch == TRUE && omLines.GetSize() < 1)
	{
		//MessageBox(NULL,"Keine passenden Mitarbeiter gefunden!","MA-Suche",MB_OK);
		MessageBox(NULL,GetString(IDS_STRING61499),GetString(IDS_STRING61500),MB_OK);
	}

    MakeAssignments();
    UpdateDisplay();
	return omDate;
}


void PrePlanTableViewer::PrepareGrouping()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);  // we'll always use the first sorting criteria for grouping
	BOOL blIsGrouped = *CViewer::GetGroup() - '0';

    omGroupBy = BY_NONE;
    if (olSortOrder.GetSize() > 0 && blIsGrouped)
    {
        if (olSortOrder[0] == "Schichtbegin")
            omGroupBy = BY_SHIFTBEGIN;
        else if (olSortOrder[0] == "Schichtend")
            omGroupBy = BY_SHIFTEND;
        else if (olSortOrder[0] == "Schichtcode")
            omGroupBy = BY_SHIFTCODE;
        else if (olSortOrder[0] == "Team")
            omGroupBy = BY_TEAM;
        else if (olSortOrder[0] == "Name")
            omGroupBy = BY_NAME;
        else if (olSortOrder[0] == "Dienstrang")
            omGroupBy = BY_RANK;
    }
}

void PrePlanTableViewer::PrepareFilter()
{
    CStringArray olFilterValues;
    int i, n;

    GetFilter("Dienstrang", olFilterValues);
    omCMapForRank.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllRanks = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllRanks = true;
		else
			for (i = 0; i < n; i++)
				omCMapForRank[olFilterValues[i]] = NULL;
	}
    //for (i = 0; i < n; i++)
    //    omCMapForRank[olFilterValues[i]] = NULL;
            // we can use NULL here since what the map mapped to is unimportant


    GetFilter("Permits", olFilterValues);
    omCMapForPermits.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllPermits = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllPermits = true;
		else
			for (i = 0; i < n; i++)
				omCMapForPermits[olFilterValues[i]] = NULL;
	}

    GetFilter("Team", olFilterValues);
    omCMapForTeam.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllTeams = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllTeams = true;
		else
			for (i = 0; i < n; i++)
				omCMapForTeam[olFilterValues[i]] = NULL;
	}
	bmEmptyTeamSelected = CheckForEmptyValue(omCMapForTeam, GetString(IDS_NOWORKGROUP));

    GetFilter("Schichtcode", olFilterValues);
    omCMapForShiftCode.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllShiftCodes = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllShiftCodes = true;
		else
			for (i = 0; i < n; i++)
				omCMapForShiftCode[olFilterValues[i]] = NULL;
	}
    //for (i = 0; i < n; i++)
    //    omCMapForShiftCode[olFilterValues[i]] = NULL;

//	GetFilter("Zeit", olFilterValues);
//    int ilCount = olFilterValues.GetSize();
//	if (ilCount != 3)
//	{
//		CTime olT = ogBasicData.GetTime();
//	    omStartTime = CTime(olT.GetYear(),olT.GetMonth(),olT.GetDay(),0,0,0);
//		omEndTime  = CTime(olT.GetYear(),olT.GetMonth(),olT.GetDay(),23,59,0);
//	}
//	else
//	{
//		CString olHours = olFilterValues[0].Left(2);
//		CString olMinutes = olFilterValues[0].Mid(2,2);
//		int ilOffset = olFilterValues[2][0] - '0';
//		CTime olNow = ogBasicData.GetTime();
//		CTime olToday = CTime(olNow.GetYear(), olNow.GetMonth(), olNow.GetDay(), 0, 0, 0);
//		omStartTime = olToday + CTimeSpan(ilOffset, atoi(olHours), atoi(olMinutes), 0);
//		olHours = olFilterValues[1].Left(2);
//		olMinutes = olFilterValues[1].Mid(2,2);
//		omEndTime = olToday + CTimeSpan(ilOffset, atoi(olHours), atoi(olMinutes), 0);
//	}

	omDate = omStartTime.Format("%Y%m%d");
}

void PrePlanTableViewer::PrepareSorter()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);

    // Create an array of sort order (but using enumerated value for speed up the performance)
    omSortOrder.RemoveAll();
    for (int i = 0; i < olSortOrder.GetSize(); i++)
    {
        int ilSortOrderEnumeratedValue;
        if (olSortOrder[i] == "Schichtbegin")
            ilSortOrderEnumeratedValue = BY_SHIFTBEGIN;
        else if (olSortOrder[i] == "Schichtend")
            ilSortOrderEnumeratedValue = BY_SHIFTEND;
        else if (olSortOrder[i] == "Schichtcode")
            ilSortOrderEnumeratedValue = BY_SHIFTCODE;
        else if (olSortOrder[i] == "Team")
            ilSortOrderEnumeratedValue = BY_TEAM;
        else if (olSortOrder[i] == "Name")
            ilSortOrderEnumeratedValue = BY_NAME;
        else if (olSortOrder[i] == "Dienstrang")
            ilSortOrderEnumeratedValue = BY_RANK;
        else
            ilSortOrderEnumeratedValue = BY_NONE;
        omSortOrder.Add(ilSortOrderEnumeratedValue);
    }
}

BOOL PrePlanTableViewer::IsPassFilter(const char *pcpRank, const char *pcpTeamId,
					    const char *pcpShiftCode, CTime opAcfr, CTime opActo, bool bpEmpIsAbsent,
						char *pcpDdat, CStringArray &ropPermits)
{
    void *p;
	BOOL blIsAbsentOK = (bmDisplayAbsentEmps || !bpEmpIsAbsent);
    BOOL blIsRankOk = bmUseAllRanks ||
					  omCMapForRank.Lookup(CString(pcpRank), p);
    BOOL blIsTeamOk = bmUseAllTeams || (strlen(pcpTeamId) == 0 && bmEmptyTeamSelected) || omCMapForTeam.Lookup(CString(pcpTeamId), p);
    BOOL blIsShiftCodeOk = strlen(pcpShiftCode) > 0 && (bmUseAllShiftCodes ||
						   omCMapForShiftCode.Lookup(CString(pcpShiftCode), p)); // shift code is set to blank if the shift is deleted in Rostering

//	BOOL blIsTimeOk = (opActo >= omStartTime && opAcfr <= omEndTime);
	BOOL blIsTimeOk = (!strcmp(omSday,pcpDdat)) ? TRUE : FALSE;
	if (bmDisplayAbsentEmps  && bpEmpIsAbsent)
	{
		blIsShiftCodeOk = TRUE;
		if (strcmp(pcpDdat,omDate) == 0)
		{
			blIsTimeOk = TRUE;
		}
	}
	
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		SHIFTTYPEDATA* polShiftType = ogShiftTypes.GetShiftTypeByCode(const_cast<char*>(pcpShiftCode));		
		CString olEmpTerminal = CString(pcpShiftCode).Mid(0,2);
		if(polShiftType != NULL)
		{
			olEmpTerminal = strlen(polShiftType->Bsds) == 0 ? olEmpTerminal : polShiftType->Bsds;
		}
		if((omTerminal == PrePlanTable::Terminal2) && olEmpTerminal.CompareNoCase(PrePlanTable::Terminal2) != 0)
		{
			blIsShiftCodeOk = FALSE;
		}
		else if((omTerminal == PrePlanTable::Terminal3) && olEmpTerminal.CompareNoCase(PrePlanTable::Terminal2) == 0)
		{
			blIsShiftCodeOk = FALSE;
		}
	}

	BOOL blIsPermitOk = bmUseAllPermits;
	int ilNumPermits = ropPermits.GetSize();
	for(int ilPermit = 0; !blIsPermitOk && ilPermit < ilNumPermits; ilPermit++)
	{
		blIsPermitOk = omCMapForPermits.Lookup(ropPermits[ilPermit], p);
	}

    return (blIsRankOk && blIsTeamOk && blIsShiftCodeOk && 
			blIsTimeOk && blIsAbsentOK && blIsPermitOk);
}

int PrePlanTableViewer::CompareShift(PREPLANT_SHIFTDATA *prpShift1,
	PREPLANT_SHIFTDATA *prpShift2)
{
    // Compare in the sort order, from the outermost to the innermost
    int n = omSortOrder.GetSize();
    for (int i = 0; i < n; i++)
    {
        int ilCompareResult = 0;

        switch (omSortOrder[i])
        {
        case BY_SHIFTBEGIN:
            ilCompareResult = (prpShift1->Acfr == prpShift2->Acfr)? 0:
                (prpShift1->Acfr > prpShift2->Acfr)? 1: -1;
            break;
        case BY_SHIFTEND:
            ilCompareResult = (prpShift1->Acto == prpShift2->Acto)? 0:
                (prpShift1->Acto > prpShift2->Acto)? 1: -1;
            break;
        case BY_SHIFTCODE:
            ilCompareResult = (prpShift1->Sfca == prpShift2->Sfca)? 0:
                (prpShift1->Sfca > prpShift2->Sfca)? 1: -1;
            break;
        case BY_TEAM:
			if(prpShift1->Tmid.GetLength() <= 0 || prpShift2->Tmid.GetLength() <= 0)
			{
				// always display emps with groups before those without groups (PRF 3452)
				ilCompareResult = prpShift2->Tmid.GetLength() - prpShift1->Tmid.GetLength();
			}
			else
			{
				ilCompareResult = (prpShift1->Tmid == prpShift2->Tmid) ? 0 : (prpShift1->Tmid > prpShift2->Tmid)? 1: -1;
			}
            break;
        case BY_NAME:
            ilCompareResult = (prpShift1->Name == prpShift2->Name)? 0:
                (prpShift1->Name > prpShift2->Name)? 1: -1;
            break;
        case BY_RANK:
			ilCompareResult = strcmp(prpShift1->Rank,prpShift2->Rank);
            break;
        // more sorting condition should be here, as case ...
        }

        // Check the result of this sorting order, return if unequality is found
        if (ilCompareResult != 0)
            return ilCompareResult;
    }

    return 0;   // we can say that these two group are eqaul
}

BOOL PrePlanTableViewer::IsSameGroup(PREPLANT_SHIFTDATA *prpShift1,
	PREPLANT_SHIFTDATA *prpShift2)
{
    // Compare in the sort order, from the outermost to the innermost
    switch (omGroupBy)
    {
    case BY_SHIFTBEGIN:
        return (prpShift1->Acfr == prpShift2->Acfr);
    case BY_SHIFTEND:
        return (prpShift1->Acto == prpShift2->Acto);
    case BY_SHIFTCODE:
        return (prpShift1->Sfca == prpShift2->Sfca);
    case BY_TEAM:
        return (prpShift1->Tmid == prpShift2->Tmid);
    case BY_NAME:
        return (prpShift1->Name == prpShift2->Name);
    case BY_RANK:
        return (prpShift1->Rank == prpShift2->Rank);
    }

    return TRUE;    // assume there's no grouping at all
}


/////////////////////////////////////////////////////////////////////////////
// PrePlanTableViewer -- code specific to this class

void PrePlanTableViewer::MakeLines()
{
    int ilShiftCount = ogShiftData.omData.GetSize();
    for (int ilLc = 0; ilLc < ilShiftCount; ilLc++)
    {
        SHIFTDATA *prlShiftData = &ogShiftData.omData[ilLc];
        MakeLine(prlShiftData);
    }
}

void PrePlanTableViewer::MakeAssignments()
{
    POSITION rlPos;
    for ( rlPos = ogJobData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
    {
        long llUrno;
        JOBDATA *prlJob;
        ogJobData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlJob);
		MakeAssignment(prlJob);
	}
}

void PrePlanTableViewer::MakeAssignment(JOBDATA *prpJob)
{
	BOOL blIsJobPool = CString(prpJob->Jtco) == JOBPOOL;
	BOOL blIsJobFlightSpecial = ((CString(prpJob->Jtco) == JOBFLIGHT) && 
		(prpJob->DisplayInPrePlanTable == TRUE));
	BOOL blIsJobCciSpecial = ((CString(prpJob->Jtco) == JOBCCI) && 
		(prpJob->DisplayInPrePlanTable == TRUE));

	if (!(blIsJobPool || blIsJobFlightSpecial || blIsJobCciSpecial))
		return;

	long llShiftUrno = 0L, llAlidUrno = 0L;
	CString olAssignmentText;

	if (blIsJobPool)
	{
		llShiftUrno = prpJob->Shur;
		olAssignmentText = prpJob->Alid;
		llAlidUrno = prpJob->Urno;
	}
	else
	{
		if (blIsJobFlightSpecial)
		{
			llShiftUrno = prpJob->Shur;
			FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prpJob);
			olAssignmentText = (prlFlight == NULL)? "ERR": prlFlight->Fnum;
			llAlidUrno = prpJob->Urno;
			//TRACE("%s\n",olAssignmentText);
		}
		else
		{
			if (blIsJobCciSpecial)
			{
				llShiftUrno = prpJob->Shur;
				olAssignmentText = prpJob->Alid;
				llAlidUrno = prpJob->Urno;
			}
			else
			{
				olAssignmentText = prpJob->Text;
			}
		}
	}

    PREPLANT_ASSIGNMENT rlAssignment;
    rlAssignment.Alid = olAssignmentText;
	rlAssignment.Urno = llAlidUrno;
    rlAssignment.Acfr = prpJob->Acfr;
    rlAssignment.Acto = prpJob->Acto;
	rlAssignment.IsSpecialJob = (blIsJobPool == FALSE); 
    int ilLineno = -1;
    if(FindShift(llShiftUrno, ilLineno))
    {
        CreateAssignment(ilLineno, &rlAssignment);
        RecalcAssignmentStatus(&omLines[ilLineno].Line);
    }

    int ilTeamMemberno = 0;
    if(bmIsCompleteTeam && FindTeamMember(llShiftUrno, ilLineno, ilTeamMemberno))
    {
        CreateTeamMemberAssignment(ilLineno, ilTeamMemberno, &rlAssignment);
        RecalcAssignmentStatus(&omLines[ilLineno].TeamMember[ilTeamMemberno]);
        RecalcTeamAssignmentStatus(ilLineno);
    }
}

void PrePlanTableViewer::MakeLine(SHIFTDATA *prpShift)
{
    // Check if this shift is in the specified date and satisfies the filter
    //if (strcmp(prpShift->Sday, (const char *)omDate) != 0)
   //    return;
    EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpShift->Peno);
    if (prlEmp == NULL)
		return;

	CString olTeam = ogBasicData.GetTeamForShift(prpShift);
	CString olFunction = ogBasicData.GetFunctionForShift(prpShift);
	CStringArray olPermits;
	ogSpeData.GetPermitsBySurn(prpShift->Stfu,olPermits);

	if (bmIsFromSearch == FALSE)
	{
		if(!IsPassFilter(olFunction, olTeam, prpShift->Sfca, prpShift->Avfa, prpShift->Avta,ogShiftData.IsAbsent(prpShift),prpShift->Sday, olPermits))
			return;
	}

    // Update viewer data for this shift record
    PREPLANT_SHIFTDATA rlShift;
    rlShift.ShiftUrno = prpShift->Urno;
    rlShift.Peno = prpShift->Peno;
    rlShift.Name = ogEmpData.GetEmpName(prlEmp);
	
	rlShift.Rank = olFunction;
    rlShift.Tmid = olTeam;
    rlShift.Sfca = prpShift->Sfca;
    rlShift.Acfr = prpShift->Avfa;
    rlShift.Acto = prpShift->Avta;
    rlShift.AssignmentStatus = UNASSIGNED;

	if (ogShiftData.IsAbsent(prpShift))
	{
		rlShift.Acfr = TIMENULL;
		rlShift.Acto = TIMENULL;
	}

	SHIFTTYPEDATA *prlShiftType = ogShiftTypes.GetShiftTypeById(prpShift->Sfcs);
	if (prlShiftType != NULL)
	{
		rlShift.IsTimeChanged = (prpShift->Avfa.Format("%H%M") != prlShiftType->Esbg ||
									prpShift->Avta.Format("%H%M") != prlShiftType->Lsen);
	}
	else
	{
		rlShift.IsTimeChanged = FALSE;
	}

    if (!bmIsCompleteTeam)
	{
        CreateLine(&rlShift);
	}
    else
    {
        int ilLineno;
        if (!FindTeam(prpShift, ilLineno, rlShift.Tmid))  // this team already exist?
            CreateLine(&rlShift);
        else
            CreateTeamMember(ilLineno, &rlShift);
    }
}

BOOL PrePlanTableViewer::FindTeam(SHIFTDATA *prpShift, int &rilLineno, CString opTeam)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
		CTime x = omLines[rilLineno].Line.Acfr, y = omLines[rilLineno].Line.Acto;
        if(omLines[rilLineno].Line.Tmid == opTeam && IsOverlapped(prpShift->Avfa,prpShift->Avta,omLines[rilLineno].Line.Acfr,omLines[rilLineno].Line.Acto))
		{
            return TRUE;
		}
	}
    return FALSE;
}


void PrePlanTableViewer::GetTeams(CStringArray &ropTeams)
{
	CMapStringToPtr olTeamsAlreadyAdded;
	void *pvlDummy;

	int ilNumLines = omLines.GetSize();
	for(int ilLine = 0; ilLine < ilNumLines; ilLine++)
	{
		CString olTeam = omLines[ilLine].Line.Tmid;
		if(!olTeamsAlreadyAdded.Lookup(olTeam, pvlDummy))
		{
			ropTeams.Add(olTeam);
			olTeamsAlreadyAdded.SetAt(olTeam, (void *) NULL);
		}
	}
}

//Singapore
void PrePlanTableViewer::GetTeams(CStringList &ropTeams)
{
	CMapStringToPtr olTeamsAlreadyAdded;
	void *pvlDummy;

	int ilNumLines = omLines.GetSize();
	for(int ilLine = 0; ilLine < ilNumLines; ilLine++)
	{
		CString olTeam = omLines[ilLine].Line.Tmid;
		if(!olTeamsAlreadyAdded.Lookup(olTeam, pvlDummy))
		{
			ropTeams.AddTail(olTeam);
			olTeamsAlreadyAdded.SetAt(olTeam, (void *) NULL);
		}
	}
}

BOOL PrePlanTableViewer::FindShift(long lpShiftUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
        if (omLines[rilLineno].Line.ShiftUrno == lpShiftUrno)
            return TRUE;
    return FALSE;
}

BOOL PrePlanTableViewer::FindTeamMember(long lpShiftUrno, int &rilLineno, int &rilTeamMemberno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
    {
        // we have to scan every members in this team
        int n = omLines[rilLineno].TeamMember.GetSize();
        for (rilTeamMemberno = 0; rilTeamMemberno < n; rilTeamMemberno++)
            if (omLines[rilLineno].TeamMember[rilTeamMemberno].ShiftUrno == lpShiftUrno)
                return TRUE;
    }
    return FALSE;
}

int PrePlanTableViewer::RecalcAssignmentStatus(PREPLANT_SHIFTDATA *prpShift)
{
    int n = prpShift->Assignment.GetSize();
    if (n == 0)
		return (prpShift->AssignmentStatus = UNASSIGNED);

	if (prpShift->Assignment[0].Acfr > prpShift->Acto)	// nothing in the range of shift at all?
		return (prpShift->AssignmentStatus = UNASSIGNED);

	CTime olLastTime = min(prpShift->Acfr, prpShift->Assignment[0].Acfr);
    for (int i = 0; i < n; i++) // scan through all assignments
    {
		if (prpShift->Assignment[i].IsSpecialJob == FALSE)
		{
			if (prpShift->Assignment[i].Acfr > olLastTime)	// first hole detected?
				return (prpShift->AssignmentStatus = PARTIAL_ASSIGNED);
			if (prpShift->Assignment[i].Acto >= prpShift->Acto)	// cover beyond the end of shift?
				return (prpShift->AssignmentStatus = FULLY_ASSIGNED);
			if (olLastTime < prpShift->Assignment[i].Acto)
				olLastTime = prpShift->Assignment[i].Acto;
		}
    }

	return (prpShift->AssignmentStatus = PARTIAL_ASSIGNED);
}

void PrePlanTableViewer::RecalcTeamAssignmentStatus(int ipLineno)
{
    PREPLANT_LINEDATA *prlLine = &omLines[ipLineno];
    prlLine->Line.AssignmentStatus = UNASSIGNED;	// clear current status

    int ilTeamMemberCount = prlLine->TeamMember.GetSize();
    int ilAssignedMemberCount = 0;
    for (int ilTeamMemberno = 0; ilTeamMemberno < ilTeamMemberCount; ilTeamMemberno++)
    {
        switch (prlLine->TeamMember[ilTeamMemberno].AssignmentStatus)
        {
        case PARTIAL_ASSIGNED:
            prlLine->Line.AssignmentStatus = PARTIAL_ASSIGNED;
				// some partial assignment found
            break;
        case FULLY_ASSIGNED:
            ilAssignedMemberCount++;
            break;
        }
    }

    // change color coding for the whole line
    if (ilAssignedMemberCount == ilTeamMemberCount)
        prlLine->Line.AssignmentStatus = FULLY_ASSIGNED;
    else if (ilAssignedMemberCount > 0)
        prlLine->Line.AssignmentStatus = PARTIAL_ASSIGNED;
}


/////////////////////////////////////////////////////////////////////////////
// PrePlanTableViewer - PREPLANT_LINEDATA array maintenance

void PrePlanTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int PrePlanTableViewer::CreateLine(PREPLANT_SHIFTDATA *prpShift)
{
    int ilLineCount = omLines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareShift(prpShift, &omLines[ilLineno].Line) <= 0)
            break;  // should be inserted before Lines[ilLineno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareShift(prpShift, &omLines[ilLineno-1].Line) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
#endif

	//Singapore
    PREPLANT_LINEDATA rlLine;
    rlLine.Line = *prpShift;
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		SHIFTTYPEDATA* polShiftType = ogShiftTypes.GetShiftTypeByCode(prpShift->Sfca.GetBuffer(0));
		CString olEmpTerminal = prpShift->Sfca.Mid(0,2);
		if(polShiftType != NULL)
		{
			olEmpTerminal = strlen(polShiftType->Bsds) == 0 ? olEmpTerminal : polShiftType->Bsds;
		}
		if(GetTerminal() == PrePlanTable::Terminal2)
		{
			if(olEmpTerminal.CompareNoCase(PrePlanTable::Terminal2) != 0)
				return -1;
		}
		else if(GetTerminal() == PrePlanTable::Terminal3)
		{
			if(olEmpTerminal.CompareNoCase(PrePlanTable::Terminal2) == 0)
				return -1;
		}
        omLines.NewAt(ilLineno, rlLine);
		
	}
	else
	{
		omLines.NewAt(ilLineno, rlLine);
	}

    // Prepare data for complete team processing if necessary
    if (bmIsCompleteTeam)
    {
        CreateTeamMember(ilLineno, prpShift);
        BOOL blIsFlightManager = ogBasicData.IsManager(prpShift->Rank);
        omLines[ilLineno].FmCount = (blIsFlightManager)? 1: 0;
        omLines[ilLineno].EmpCount = (!blIsFlightManager)? 1: 0;
    }

    return ilLineno;
}

void PrePlanTableViewer::DeleteLine(int ipLineno)
{
	if (ipLineno < omLines.GetSize())
	{
		while (omLines[ipLineno].Line.Assignment.GetSize() > 0)
			DeleteAssignment(ipLineno, 0);
		while (omLines[ipLineno].TeamMember.GetSize() > 0)
			DeleteTeamMember(ipLineno, 0);

		omLines.DeleteAt(ipLineno);
	}
}

int PrePlanTableViewer::CreateAssignment(int ipLineno, PREPLANT_ASSIGNMENT *prpAssignment)
{
    int ilAssignmentCount = omLines[ipLineno].Line.Assignment.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
    // Search for the position which we want to insert this new bar
    for (int ilAssignmentno = ilAssignmentCount; ilAssignmentno > 0; ilAssignmentno--)
	{
        if (prpAssignment->IsSpecialJob == FALSE && 
				omLines[ipLineno].Line.Assignment[ilAssignmentno-1].IsSpecialJob == TRUE)
            break;  // should be inserted after Lines[ilAssignmentno-1]
        if (prpAssignment->IsSpecialJob == TRUE && 
				omLines[ipLineno].Line.Assignment[ilAssignmentno-1].IsSpecialJob == FALSE)
            continue;  // special jobs should be inserted before pool jobs

        if (prpAssignment->Acfr >= omLines[ipLineno].Line.Assignment[ilAssignmentno-1].Acfr)
            break;  // should be inserted after Lines[ilAssignmentno-1]
	}
    omLines[ipLineno].Line.Assignment.NewAt(ilAssignmentno, *prpAssignment);
    return ilAssignmentno;
}

int PrePlanTableViewer::DeleteAssignmentJob(long lpJobUrno)
{
	BOOL ilRc = TRUE;

	JOBDATA *prlJob = ogJobData.GetJobByUrno(lpJobUrno);
	if (prlJob != NULL)
	{
		ilRc = ogDataSet.DeleteJob(pomTable, lpJobUrno);
	}
	return ilRc;
}

void PrePlanTableViewer::DeleteAssignment(int ipLineno, int ipAssignmentno)
{
	if (ipLineno < omLines.GetSize())
	{
		if (omLines[ipLineno].Line.Assignment.GetSize() > ipAssignmentno)
			omLines[ipLineno].Line.Assignment.DeleteAt(ipAssignmentno);
		RecalcAssignmentStatus(&omLines[ipLineno].Line);

		CString olRank = omLines[ipLineno].Line.Rank;
		BOOL blIsFlightManager = ogBasicData.IsManager(olRank);
		if (blIsFlightManager)
		{
//			pomTable->SetTextLineFont(ipLineno, &ogCourier_Bold_10);
		//	pomTable->SetTextLineColor(ipLineno, BLUE, WHITE);
		}

		if (ogOdaData.GetOdaByType(omLines[ipLineno].Line.Sfca) != NULL)
		{
			pomTable->SetTextLineColor(ipLineno, RED, WHITE);
		}
		else
		{
			// Update Display 
			switch (omLines[ipLineno].Line.AssignmentStatus)
			{
			case PARTIAL_ASSIGNED:
				pomTable->SetTextLineColor(ipLineno, (blIsFlightManager? BLUE: BLACK), SILVER);
				pomTable->SetTextLineDragEnable(ipLineno, TRUE);
				break;
			case FULLY_ASSIGNED:
				pomTable->SetTextLineColor(ipLineno, (blIsFlightManager? BLUE: WHITE), GRAY);
				pomTable->SetTextLineDragEnable(ipLineno, FALSE);
				break;
			case UNASSIGNED:
				pomTable->SetTextLineColor(ipLineno, (blIsFlightManager? BLUE: BLACK), WHITE);
				pomTable->SetTextLineDragEnable(ipLineno, TRUE);
				break;
			}
		}
		pomTable->ChangeTextLine(ipLineno, Format(&omLines[ipLineno]), &omLines[ipLineno]);
	}
}

int PrePlanTableViewer::CreateTeamMember(int ipLineno, PREPLANT_SHIFTDATA *prpShift)
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		SHIFTTYPEDATA* polShiftType = ogShiftTypes.GetShiftTypeByCode(prpShift->Sfca.GetBuffer(0));
		CString olEmpTerminal = prpShift->Sfca.Mid(0,2);
		if(polShiftType != NULL)
		{
			olEmpTerminal = strlen(polShiftType->Bsds) == 0 ? olEmpTerminal : polShiftType->Bsds;
		}
		
		polShiftType = NULL;
		polShiftType = ogShiftTypes.GetShiftTypeByCode(omLines[ipLineno].Line.Sfca.GetBuffer(0));
		CString olLineTerminal = omLines[ipLineno].Line.Sfca.Mid(0,2);
		if(polShiftType != NULL)
		{
			olEmpTerminal = strlen(polShiftType->Bsds) == 0 ? olLineTerminal : polShiftType->Bsds;
		}

		if(GetTerminal() == PrePlanTable::Terminal2)
		{
			if((olEmpTerminal.CompareNoCase(PrePlanTable::Terminal2) != 0)
				|| (olLineTerminal.CompareNoCase(PrePlanTable::Terminal2) != 0))
			{
				return -1;
			}
		}
		else if(GetTerminal() == PrePlanTable::Terminal3)
		{
			if((olEmpTerminal.CompareNoCase(PrePlanTable::Terminal2) == 0)
				|| (olLineTerminal.CompareNoCase(PrePlanTable::Terminal2) == 0))
			{
				return -1;
			}
		}
	}

    int ilTeamMemberno = omLines[ipLineno].TeamMember.New();
    omLines[ipLineno].TeamMember[ilTeamMemberno] = *prpShift;

    // Update information in the team record
    BOOL blIsFlightManager = ogBasicData.IsManager(prpShift->Rank);
    omLines[ipLineno].FmCount += (blIsFlightManager? 1: 0);
    omLines[ipLineno].EmpCount += (!blIsFlightManager? 1: 0);

    if (omLines[ipLineno].Line.Assignment.GetSize() == 1)	// Is this the first member in this team?
        omLines[ipLineno].Line.AssignmentStatus = prpShift->AssignmentStatus;
    else
    {
        if (omLines[ipLineno].Line.AssignmentStatus != prpShift->AssignmentStatus)
            omLines[ipLineno].Line.AssignmentStatus = PARTIAL_ASSIGNED;
    }

    // Check if it necessary to change the team leader
//    int ilTeamLeaderRank = ogRnkData.GetRankWeight(omLines[ipLineno].Line.Rank);
//    int ilShiftRank = ogRnkData.GetRankWeight(prpShift->Rank);
//    if (ilShiftRank > ilTeamLeaderRank)

	if(!ogBasicData.IsGroupLeader(ogShiftData.GetShiftByUrno(omLines[ipLineno].Line.ShiftUrno)) &&
		ogBasicData.IsGroupLeader(ogShiftData.GetShiftByUrno(prpShift->ShiftUrno)))
    {
        omLines[ipLineno].Line = *prpShift;
        PREPLANT_LINEDATA *prlLine = &omLines[ipLineno];
        omLines.RemoveAt(ipLineno);
        int ilLineCount = omLines.GetSize();
// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
        // Search for the position which we want to insert this new bar
        for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
            if (CompareShift(&prlLine->Line, &omLines[ilLineno].Line) <= 0)
                break;  // should be inserted before Lines[ilLineno]
#else
        // Search for the position which we want to insert this new bar
        for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
            if (CompareShift(&prlLine->Line, &omLines[ilLineno-1].Line) >= 0)
                break;  // should be inserted after Lines[ilLineno-1]
#endif
        omLines.InsertAt(ilLineno, (void *)prlLine);
			// put the team back to the correct order
    }

    return ilTeamMemberno;
}

void PrePlanTableViewer::DeleteTeamMember(int ipLineno, int ipTeamMemberno)
{
    while (omLines[ipLineno].TeamMember[ipTeamMemberno].Assignment.GetSize() > 0)
        DeleteTeamMemberAssignment(ipLineno, ipTeamMemberno, 0);

    omLines[ipLineno].TeamMember.DeleteAt(ipTeamMemberno);
}

int PrePlanTableViewer::CreateTeamMemberAssignment(int ipLineno, int ipTeamMemberno,
    PREPLANT_ASSIGNMENT *prpAssignment)
{
    int ilAssignmentCount = omLines[ipLineno].TeamMember[ipTeamMemberno].Assignment.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilAssignmentno = 0; ilAssignmentno < ilAssignmentCount; ilAssignmentno++)
        if (prpAssignment->Acfr <= omLines[ipLineno].TeamMember[ipTeamMemberno].Assignment[ilAssignmentno].Acfr)
            break;  // should be inserted before Lines[ilAssignmentno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilAssignmentno = ilAssignmentCount; ilAssignmentno > 0; ilAssignmentno--)
        if (prpAssignment->Acfr >= omLines[ipLineno].TeamMember[ipTeamMemberno].Assignment[ilAssignmentno-1].Acfr)
            break;  // should be inserted after Lines[ilAssignmentno-1]
#endif

    omLines[ipLineno].TeamMember[ipTeamMemberno].Assignment.NewAt(ilAssignmentno, *prpAssignment);
    return ilAssignmentno;
}

void PrePlanTableViewer::DeleteTeamMemberAssignment(int ipLineno, int ipTeamMemberno,
    int ipAssignmentno)
{
    omLines[ipLineno].TeamMember[ipTeamMemberno].Assignment.DeleteAt(ipAssignmentno);
}


/////////////////////////////////////////////////////////////////////////////
// PrePlanTableViewer - display drawing routine

// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void PrePlanTableViewer::UpdateDisplay()
{
    // Set table header and format
    if (!bmIsCompleteTeam)  // display each staff?
    {
        pomTable->SetFieldNames("NAME,RANK,TEAM,TYPE,ACFR,ACTO,"
            "ALO1,TIL1,ALO2,TIL2,ALO3,TIL3,ALO4,ALO4");
        //pomTable->SetHeaderFields("Name|Rng|Team|SFT|Anwesend||Zugeteilt|von-bis|Zugeteilt|von-bis|Zugeteilt|von-bis|Zugeteilt|von-bis");
       
		if (BGS)  	pomTable->SetHeaderFieldsBGS(GetString(IDS_STRING61572));
		else        pomTable->SetHeaderFields(GetString(IDS_STRING61572));
        
		pomTable->SetFormatList("24|5|7|8|7|7|10|9|10|9|10|9|10|9");
    }
    else
    {
        pomTable->SetFieldNames("NAME,RANK,TEAM,TYPE,ACFR,ACTO,FMCOUNT,MEMBERCOUNT,"
            "ALO1,TIL1,ALO2,TIL2,ALO3,TIL3,ALO4,ALO4");
        //pomTable->SetHeaderFields("Name|Rng|Team|SFT|Anwesend||FM|Emp|Zugeteilt|von-bis|Zugeteilt|von-bis|Zugeteilt|von-bis|Zugeteilt|von-bis");
        if (BGS)  	pomTable->SetHeaderFieldsBGS(GetString(IDS_STRING61573));
		else        pomTable->SetHeaderFields(GetString(IDS_STRING61573));
        
        
		
		
		
		pomTable->SetFormatList("24|5|7|8|7|7|3|3|10|9|10|9|10|9|10|9");
    }

    // Load filtered and sorted data into the table content
    pomTable->ResetContent();
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
    {
        pomTable->AddTextLine(Format(&omLines[ilLc]), &omLines[ilLc]);
//		pomTable->SetTextLineFont(ilLc, &ogCourier_Bold_10);

		// Display bold font for the managers
		CString olRank = omLines[ilLc].Line.Rank;
        BOOL blIsFlightManager = ogBasicData.IsManager(olRank);
		if (blIsFlightManager)
		{
//			pomTable->SetTextLineFont(ilLc, &ogCourier_Bold_10);
			pomTable->SetTextLineColor(ilLc, BLUE, WHITE);
		}
		/*
		if ((*omLines[ilLc].Line.Sfca == 'K') ||
			(*omLines[ilLc].Line.Sfca == 'U') ||
			(*omLines[ilLc].Line.Sfca == 'L'))
			**/
		if (ogOdaData.GetOdaByType(omLines[ilLc].Line.Sfca) != NULL)
		{
//			pomTable->SetTextLineFont(ilLc, &ogCourier_Bold_10);
			pomTable->SetTextLineColor(ilLc, RED, WHITE);
		}

        // Display grouping effect
        if (ilLc == omLines.GetSize()-1 ||
			!IsSameGroup(&omLines[ilLc].Line, &omLines[ilLc+1].Line))
            pomTable->SetTextLineSeparator(ilLc, ST_THICK);

        switch (omLines[ilLc].Line.AssignmentStatus)
        {
        case PARTIAL_ASSIGNED:
            pomTable->SetTextLineColor(ilLc, (blIsFlightManager? BLUE: BLACK), SILVER);
            break;
        case FULLY_ASSIGNED:
            pomTable->SetTextLineColor(ilLc, (blIsFlightManager? BLUE: WHITE), GRAY);
            pomTable->SetTextLineDragEnable(ilLc, FALSE);
            break;
        }
    }

    // Update the table content in the display
    pomTable->DisplayTable();
}

CString PrePlanTableViewer::Format(PREPLANT_LINEDATA *prpLine)
{
    CString s = CString(prpLine->Line.Name) + "|"
        + prpLine->Line.Rank + "|"
        + prpLine->Line.Tmid + "|"
        + prpLine->Line.Sfca + (prpLine->Line.IsTimeChanged == TRUE ? "'" : "") + "|"
        + prpLine->Line.Acfr.Format("%d/%H%M") + "|"
        + prpLine->Line.Acto.Format("%d/%H%M");

    if (bmIsCompleteTeam)
    {
        char buf[512];
        sprintf(buf, "|%3d|%3d", prpLine->FmCount, prpLine->EmpCount);
        s += buf;
    }

    for (int ilLc = 0; ilLc < prpLine->Line.Assignment.GetSize(); ilLc++)
    {
        PREPLANT_ASSIGNMENT *prlAssignment = &prpLine->Line.Assignment[ilLc];
        s += "|" + prlAssignment->Alid;
		s += "|" + prlAssignment->Acfr.Format("%H%M") + "-";
		s += prlAssignment->Acto.Format("%H%M");
    }
    return s;
}

CString PrePlanTableViewer::GetPoolName(const char *pcpPoolId)
{
	CString olPoolGroupName;
	ALLOCUNIT *prlPoolGroup = ogAllocData.GetGroupByName(ALLOCUNITTYPE_POOLGROUP,pcpPoolId);
	if(prlPoolGroup != NULL)
	{
		olPoolGroupName = prlPoolGroup->Name;
	}
	return olPoolGroupName;
//	int n = ogPools.omMetaAllocUnitList.GetSize();
//    for (int i = 0; i < n; i++)
//	{
//		if (CString(ogPools.omMetaAllocUnitList[i].Alid) == pcpPoolId)
//			return ogPools.omMetaAllocUnitList[i].Alfn;
//	}
//
//	return "";
}

CString PrePlanTableViewer::GetGateAreaOrCCIAreaName(const char *pcpAlid)
{
	CString olGroupName;

	CCSPtrArray <ALLOCUNIT> olGroups;
	ogAllocData.GetGroupsByGroupType(ALLOCUNITTYPE_GATEGROUP,olGroups);
	int ilNumGroups = olGroups.GetSize();
	for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
	{
		ALLOCUNIT *prlGroup = &olGroups[ilGroup];
		if(!strcmp(prlGroup->Name,pcpAlid))
		{
			olGroupName = prlGroup->Name;
		}
	}
	if(olGroupName.IsEmpty())
	{
		CCSPtrArray <ALLOCUNIT> olGroups;
		ogAllocData.GetGroupsByGroupType(ALLOCUNITTYPE_CICGROUP,olGroups);
		int ilNumGroups = olGroups.GetSize();
		for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
		{
			ALLOCUNIT *prlGroup = &olGroups[ilGroup];
			if(!strcmp(prlGroup->Name,pcpAlid))
			{
				olGroupName = prlGroup->Name;
			}
		}
	}
	return olGroupName;
//	int i, n;
//
//	n = ogGateAreas.omMetaAllocUnitList.GetSize();
//    for (i = 0; i < n; i++)
//	{
//		if (CString(ogGateAreas.omMetaAllocUnitList[i].Alid) == pcpAlid)
//			return ogGateAreas.omMetaAllocUnitList[i].Alfn;
//	}
//
//	// basic data module already provides methods for lookup a CCI Area ID
//	return ogBasicData.GetTextById(pcpAlid);
}

/////////////////////////////////////////////////////////////////////////////
// PrePlanTableViewer Jobs creation methods

void PrePlanTableViewer::PrePlanTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    PrePlanTableViewer *polViewer = (PrePlanTableViewer *)popInstance;

    if (ipDDXType == JOB_NEW)
	{
        polViewer->ProcessJobChange((JOBDATA *)vpDataPointer);
	}
	else if (ipDDXType == JOB_CHANGE)
	{
        polViewer->ProcessJobDelete((JOBDATA *)vpDataPointer);
        polViewer->ProcessJobNew((JOBDATA *)vpDataPointer);
	}
	else if (ipDDXType == JOB_DELETE)
	{
        polViewer->ProcessJobDelete((JOBDATA *)vpDataPointer);
	}
	else if (ipDDXType == SHIFT_CHANGE)
	{
        polViewer->ProcessShiftChange((SHIFTDATA *)vpDataPointer);
	}
	else if (ipDDXType == SHIFT_NEW)
	{
        polViewer->ProcessShiftChange((SHIFTDATA *)vpDataPointer);
	}
	else if (ipDDXType == SHIFT_DELETE)
	{
        polViewer->ProcessShiftDelete((long)vpDataPointer);
	}
    else if (ipDDXType == SHIFT_SELECT_PREPLAN)
	{
        polViewer->ProcessShiftSelect((SHIFTDATA *)vpDataPointer);
	}

	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{	
		if(polViewer->pomTable != NULL && polViewer->pomTable->m_hWnd != NULL)
		{	
			CWnd* polWnd = polViewer->pomTable->GetParent();
			(PrePlanTable*)(polWnd)->SendMessage(WM_TABLE_UPDATE_DATACOUNT,0,0);
		}
	}
}

void PrePlanTableViewer::ProcessShiftSelect(SHIFTDATA *prpShift)
{
	int ilItem;
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		igPreplanSearchCount++;
	}

	if (FindShift(prpShift->Urno,ilItem))
	{
		CListBox *polListBox = pomTable->GetCTableListBox();
		if (polListBox)
		{
			polListBox->SetSel(ilItem+1);
			polListBox->SetSel(ilItem+1, FALSE);
			polListBox->SetSel(ilItem);
		}
		igPreplanSearchCount = 0;		
	}
	else if(ogBasicData.IsPaxServiceT2() == true)
	{
		if(igPreplanSearchCount == 2)
		{
			MessageBox(NULL,GetString(IDS_SV_NOTFOUND),GetString(IDS_SV_NOTFOUNDTITLE),MB_ICONEXCLAMATION );
			igPreplanSearchCount = 0;
		}
	}
	else
	{
		MessageBox(NULL,GetString(IDS_SV_NOTFOUND),GetString(IDS_SV_NOTFOUNDTITLE),MB_ICONEXCLAMATION );
	}
}


BOOL PrePlanTableViewer::FindItemByUrno(JOBDATA *prpJob,int& ripItem, int& ripAssignment)
{
	int ilCount = omLines.GetSize();
    for (int ilLc = 0; ilLc < ilCount; ilLc++)
    {
		int ilJobCount = omLines[ilLc].Line.Assignment.GetSize();
		for ( int ilJc = 0; ilJc < ilJobCount; ilJc++)
		{
			if (omLines[ilLc].Line.Assignment[ilJc].Urno == prpJob->Urno)
			{
				ripAssignment = ilJc;
				ripItem = ilLc;
				return TRUE;
			}
		}
	}
	return FALSE;
}

void PrePlanTableViewer::ProcessJobChange(JOBDATA *prpJob)
{
// Id 19-Sep-96
    // If it's not a job pool or special flight job, ignore it
	BOOL blIsJobPool = CString(prpJob->Jtco) == JOBPOOL;
	BOOL blIsJobFlightSpecial = CString(prpJob->Jtco) == JOBFLIGHT;
	BOOL blIsJobCciSpecial = CString(prpJob->Jtco) == JOBCCI;
	if (!(blIsJobPool || blIsJobFlightSpecial || blIsJobCciSpecial))
		return;
// end of Id 19-Sep-96

	int ilItem;
	int ilAssignment;
	if (FindItemByUrno(prpJob,ilItem,ilAssignment))
	{
 		DeleteAssignment(ilItem,ilAssignment);
	}
    ProcessJobNew(prpJob);
}

void PrePlanTableViewer::ProcessJobDelete(JOBDATA *prpJob)
{
// Id 19-Sep-96
    // If it's not a job pool or special flight job, ignore it
	BOOL blIsJobPool = CString(prpJob->Jtco) == JOBPOOL;
	BOOL blIsJobFlightSpecial = CString(prpJob->Jtco) == JOBFLIGHT;
	BOOL blIsJobCciSpecial = CString(prpJob->Jtco) == JOBCCI;
	if (!(blIsJobPool || blIsJobFlightSpecial || blIsJobCciSpecial))
		return;
// end of Id 19-Sep-96

	int ilItem;
	int ilAssignment;
	if (FindItemByUrno(prpJob,ilItem,ilAssignment))
	{
//		if (blIsJobPool)
		{
			DeleteAssignment(ilItem,ilAssignment);
		}
	}
}


void PrePlanTableViewer::ProcessJobNew(JOBDATA *prpJob)
{
// Id 19-Sep-96
// Old algorithm:
//		We search for pool jobs and flight jobs. We use a special technique to
//		display the proper gate area instead of pool for a flight manager.
//		If PRID is blank, we display the pool, otherwise we display the gate area.
//		This must be handshaken with ogDataSet.CreateJobPool().
// New algorithm:
//		We still search for pool jobs and flight jobs. But now we can look at
//		JTCO directly since we have another separate type FLS for speical flight
//		job. For a pool job, if the field TEXT is not empty, we will assume that
//		the field TEXT is a gate area ID and display it, otherwise we will display
//		the pool using the value in the field ALID.
//
    // If it's not a job pool or special flight job, ignore it
	BOOL blIsJobPool = CString(prpJob->Jtco) == JOBPOOL;
	BOOL blIsJobFlightSpecial = ((CString(prpJob->Jtco) == JOBFLIGHT) && 
		(prpJob->DisplayInPrePlanTable == TRUE));
	BOOL blIsJobCciSpecial = ((CString(prpJob->Jtco) == JOBCCI) && 
		(prpJob->DisplayInPrePlanTable == TRUE));
	/*
	if (!(blIsJobPool || blIsJobFlightSpecial || blIsJobCciSpecial))
		return;
	*/

	// Prepare shift URNO and displayed text for this assignment
	long llShiftUrno;
	long llAlidUrno;
	CString olAssignmentText;

	if (blIsJobPool)
	{
		llShiftUrno = prpJob->Shur;
		//olAssignmentText = (prpJob->Text[0] != '\0') ? GetGateAreaOrCCIAreaName(prpJob->Text) : prpJob->Alid;
		olAssignmentText = prpJob->Alid;
		llAlidUrno = prpJob->Urno;
	}
	else
	{
		if (blIsJobFlightSpecial)
		{
			llShiftUrno = prpJob->Shur;
			FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prpJob);
			olAssignmentText = (prlFlight == NULL)? "ERR": prlFlight->Fnum;
			llAlidUrno = prpJob->Urno;
		}
		else
		{
			if (blIsJobCciSpecial)
			{
				llShiftUrno = prpJob->Shur;
				olAssignmentText = prpJob->Alid;
				llAlidUrno = prpJob->Urno;
			}
			else
				olAssignmentText = prpJob->Text;
		}
	}
// end of Id 19-Sep-96

    // Create a new assignment
    BOOL blHasToUpdateLine = FALSE;
    PREPLANT_ASSIGNMENT rlAssignment;
    rlAssignment.Alid = olAssignmentText;
    rlAssignment.Urno = llAlidUrno;
	rlAssignment.Acfr = prpJob->Acfr;
    rlAssignment.Acto = prpJob->Acto;
    int ilLineno;
	rlAssignment.IsSpecialJob = (blIsJobPool == FALSE);
    if (FindShift(llShiftUrno, ilLineno))  // corresponding shift found?
    {   // (for some reason, we have saved SHFCKI.URNO in JOBCKI.FLUR)
        CreateAssignment(ilLineno, &rlAssignment);
        RecalcAssignmentStatus(&omLines[ilLineno].Line);
        blHasToUpdateLine = TRUE;
    }

    int ilTeamMemberno;
    if (bmIsCompleteTeam && FindTeamMember(llShiftUrno, ilLineno, ilTeamMemberno))
    {
        CreateTeamMemberAssignment(ilLineno, ilTeamMemberno, &rlAssignment);
        RecalcAssignmentStatus(&omLines[ilLineno].TeamMember[ilTeamMemberno]);
        RecalcTeamAssignmentStatus(ilLineno);
        blHasToUpdateLine = TRUE;
    }

    // Update the table window if necessary
    if (blHasToUpdateLine)
    {
		CString olRank = omLines[ilLineno].Line.Rank;
        BOOL blIsFlightManager = ogBasicData.IsManager(olRank);

		/*
		if ((*omLines[ilLineno].Line.Sfca == 'K') ||
			(*omLines[ilLineno].Line.Sfca == 'U') ||
			(*omLines[ilLineno].Line.Sfca == 'L'))
			**/
		if (ogOdaData.GetOdaByType(omLines[ilLineno].Line.Sfca) != NULL)
		{
//			pomTable->SetTextLineFont(ilLineno, &ogCourier_Bold_10);
			pomTable->SetTextLineColor(ilLineno, RED, WHITE);
		}

		// Display grouping effect
        switch (omLines[ilLineno].Line.AssignmentStatus)
        {
        case PARTIAL_ASSIGNED:
            pomTable->SetTextLineColor(ilLineno, (blIsFlightManager? BLUE: BLACK), SILVER);
            break;
        case FULLY_ASSIGNED:
            pomTable->SetTextLineColor(ilLineno, (blIsFlightManager? BLUE: WHITE), GRAY);
            pomTable->SetTextLineDragEnable(ilLineno, FALSE);
            break;
        }

        pomTable->ChangeTextLine(ilLineno, Format(&omLines[ilLineno]), &omLines[ilLineno]);
	}
}

void PrePlanTableViewer::ProcessShiftDelete(long lpUrno)
{
	int ilLine = -1;
	if (FindShift(lpUrno,ilLine))
	{
		DeleteLine(ilLine);
		pomTable->DeleteTextLine(ilLine);
		pomTable->DisplayTable();
	}
}

void PrePlanTableViewer::ProcessShiftChange(SHIFTDATA *prpShift)
{
	if(prpShift == NULL)
		return;

	int ilItem;

	if(strlen(prpShift->Sfca) <= 0 || prpShift->Bsdu == 0)
	{
		// the shift has been deleted in rostering (update received with SCOD="" and BSDU=0)
		// delete the shift entry in the preplanning table
		ProcessShiftDelete(prpShift->Urno);
	}
	else if (FindShift(prpShift->Urno,ilItem))
	{
        PREPLANT_SHIFTDATA *prlPpShift = &omLines[ilItem].Line;
	    EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpShift->Peno);
		if (prlEmp == NULL)
			return;

		CString olTeam = ogBasicData.GetTeamForShift(prpShift);
		CString olFunction = ogBasicData.GetFunctionForShift(prpShift);
		CStringArray olPermits;
		ogSpeData.GetPermitsBySurn(prpShift->Stfu,olPermits);

		if(!IsPassFilter(olFunction, olTeam, prpShift->Sfca, prpShift->Avfa, prpShift->Avta,ogShiftData.IsAbsent(prpShift),prpShift->Sday, olPermits))
		{
			ProcessShiftDelete(prpShift->Urno);
			return;
		}

		prlPpShift->ShiftUrno = prpShift->Urno;
		prlPpShift->Peno = prpShift->Peno;
		prlPpShift->Name = ogEmpData.GetEmpName(prlEmp);
		prlPpShift->Rank = olFunction;
		prlPpShift->Tmid = olTeam;
		prlPpShift->Sfca = prpShift->Sfca;
		prlPpShift->Acfr = prpShift->Avfa;
		prlPpShift->Acto = prpShift->Avta;

		if (ogShiftData.IsAbsent(prpShift))
		{
			prlPpShift->Acfr = TIMENULL;
			prlPpShift->Acto = TIMENULL;
		}
		SHIFTTYPEDATA *prlShiftType = ogShiftTypes.GetShiftTypeById(prpShift->Sfcs);
		if (prlShiftType != NULL)
		{
			prlPpShift->IsTimeChanged = (prpShift->Avfa.Format("%H%M") != prlShiftType->Esbg ||
										prpShift->Avta.Format("%H%M") != prlShiftType->Lsen);
		}
		else
		{
			prlPpShift->IsTimeChanged = FALSE;
		}

		RecalcAssignmentStatus(prlPpShift);
		SetLineColour(ilItem);
	}
	else
	{
		if (bmIsFromSearch == FALSE)
		{
			MakeLine(prpShift);
			if(FindShift(prpShift->Urno, ilItem))
			{
				pomTable->InsertTextLine(ilItem, Format(&omLines[ilItem]), &omLines[ilItem]);

				CCSPtrArray<JOBDATA> olJobs;
				ogJobData.GetJobsByShur(olJobs, prpShift->Urno);
				for(int ilJ = 0; ilJ < olJobs.GetSize(); ilJ++)
					MakeAssignment(&olJobs[ilJ]);

				SetLineColour(ilItem);
			}
		}
	}
}

void PrePlanTableViewer::SetLineColour(int ipLineno)
{
	CString olRank = omLines[ipLineno].Line.Rank;
	BOOL blIsFlightManager = ogBasicData.IsManager(olRank);

	switch (omLines[ipLineno].Line.AssignmentStatus)
	{
	case PARTIAL_ASSIGNED:
		pomTable->SetTextLineColor(ipLineno, (blIsFlightManager? BLUE: BLACK), SILVER);
		pomTable->SetTextLineDragEnable(ipLineno, TRUE);
		break;
	case FULLY_ASSIGNED:
		pomTable->SetTextLineColor(ipLineno, (blIsFlightManager? BLUE: WHITE), GRAY);
		pomTable->SetTextLineDragEnable(ipLineno, FALSE);
		break;
	case UNASSIGNED:
		pomTable->SetTextLineColor(ipLineno, (blIsFlightManager? BLUE: BLACK), WHITE);
		pomTable->SetTextLineDragEnable(ipLineno, TRUE);
		break;
	}

	if (ogOdaData.GetOdaByType(omLines[ipLineno].Line.Sfca) != NULL)
	{
		if(bmDisplayAbsentEmps)
		{
			pomTable->SetTextLineColor(ipLineno, RED, WHITE);
		}
		else
		{
			DeleteLine(ipLineno);
			pomTable->DeleteTextLine(ipLineno);
			pomTable->DisplayTable();
			return;
		}
	}

	pomTable->ChangeTextLine(ipLineno, Format(&omLines[ipLineno]), &omLines[ipLineno]);
	pomTable->DisplayTable();
}

CTime PrePlanTableViewer::StartTime() const
{
	return omStartTime;
}

CTime PrePlanTableViewer::EndTime() const
{
	return omEndTime;
}

void PrePlanTableViewer::SetStartTime(CTime opStartTime)
{
	omStartTime = opStartTime;
	omSday = opStartTime.Format("%Y%m%d");
}
 
void PrePlanTableViewer::SetEndTime(CTime opEndTime)
{
	omEndTime = opEndTime;
}

int PrePlanTableViewer::Lines() const
{
	return omLines.GetSize();
}

PREPLANT_LINEDATA* PrePlanTableViewer::GetLine(int ipLineNo)
{
	if (ipLineNo >= 0 && ipLineNo < omLines.GetSize())
		return &omLines[ipLineNo];
	else
		return NULL;
}

void PrePlanTableViewer::DeleteAllAssignments(int ipLineNo)
{
	// validity check
	if (ipLineNo >= 0 && ipLineNo < omLines.GetSize())
	{
		for (int i = omLines[ipLineNo].Line.Assignment.GetSize() - 1; i >= 0; i--)
		{
			DeleteAssignmentJob(omLines[ipLineNo].Line.Assignment[i].Urno);
		}
	}
}