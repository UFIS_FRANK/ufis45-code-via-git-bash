

#include <stdafx.h>
#include <ccsglobl.h>
#include <resource.h>
#include <CCSPrint.h>
#include <gbar.h>
#include <WINSPOOL.H>
#include <BasicData.h>
#include <CPrintPageDialog.h>
 

//#define INCH 0.254
#define INCH 0.284

#define MMX(x)    ((int)(MulDiv((x),imLogPixelsX, 72)*INCH))
#define MMY(x)    ((int)(MulDiv((x),imLogPixelsY, 72)*INCH))


CCSPtrArray <PRINTBARDATA> omDefBkBars;

CCSPrint::CCSPrint(CWnd *opParent)
{
	pomParent = opParent;
	imLineNo = 999;
	imPageNo = 0;
	imMaxLines = 14;
	hmBitmap = NULL;
	hmPalette= NULL;
	emPrintOption = PRINT_PRINTER;
	omHatchedBrush.CreateHatchBrush( HS_BDIAGONAL, RGB(255,0,0)	 );
	smPaperSize = DMPAPER_A4;

	bmIsPageSelected = FALSE; //Singapore
	bmEnablePageSelection = FALSE; //Singapore
	imFromPage = -1; //Singapore
	imToPage = -1; //Singapore
	imTotalPageCount = 0; //Singapore
	InitializeDevicePointer(); //Singapore	
}


CCSPrint::CCSPrint(CWnd *opParent,int ipOrientation,
		int ipLineHeight,int ipFirstLine,int ipLeftOffset,
		  CString opHeader1,CString opHeader2,CString opHeader3,CString opHeader4,
		CString opFooter1,CString opFooter2,CString opFooter3)
{
	bmIsInitialized = FALSE;

	imLineHeight = ipLineHeight;
	imFirstLine  = ipFirstLine;
	imLineTop = imFirstLine;
	imLeftOffset = ipLeftOffset;
	imHourPixels = 0;
	imHours = 0;
	emPrintOption = PRINT_PRINTER;
	InitializeDevicePointer();

	pomParent = opParent;
	imOrientation = ipOrientation;
	omHeader.Add(opHeader1);
	omHeader.Add(opHeader2);
	omHeader.Add(opHeader3);
	omHeader.Add(opHeader4);
	omHeader.Add(opFooter1);
	omHeader.Add(opFooter2);
	omHeader.Add(opFooter3);



	imLineNo = 999;
	imPageNo = 0;
	if (imOrientation == PRINT_LANDSCAPE)
	{
		imMaxLines = 18;
	}
	else
	{
		imMaxLines = 24;
	}

	hmBitmap = NULL;
	hmPalette= NULL;
	CBitmap olBitmap;
	omHatchedBrush.CreateHatchBrush( HS_BDIAGONAL, RGB(255,0,0)	 );

	bmIsPageSelected = FALSE; //Singapore
	imFromPage = -1; //Singapore
	imToPage = -1; //Singapore
	imTotalPageCount = 0; //Singapore	
	//bmEnablePageSelection = FALSE; //Singapore
	bmEnablePageSelection = TRUE; 
}


CCSPrint::~CCSPrint()
{
	if (bmIsInitialized == TRUE)
	{
		pomCdc->DeleteDC();
		ThinPen.DeleteObject();
		MediumPen.DeleteObject();
		ThickPen.DeleteObject();
		DottedPen.DeleteObject();
		omRgn.DeleteObject();

		omSmallFont_Regular.DeleteObject();
		omMediumFont_Regular.DeleteObject();
		omLargeFont_Regular.DeleteObject();
		omSmallFont_Bold.DeleteObject();
		omSmallFont_UnderlineBold.DeleteObject();
		omMediumFont_Bold.DeleteObject();
		omLargeFont_Bold.DeleteObject();
		omCourierNormal10.DeleteObject();
		omCourierNormal7.DeleteObject();
		omCCSFont.DeleteObject();

	}

	if (hmBitmap)
		::DeleteObject(hmBitmap);
	if (hmPalette)
		::DeleteObject(hmPalette);
}

BOOL CCSPrint::InitializePrinter(int ipOrientation,char *pcpFormName,int ipPaperSize)
{
	int ilRc;

	HDC hlHdc;


	/*
	char pclDevices[182];

	::GetProfileString("windows", "device","",pclDevices,180);
	CString olDevices = pclDevices;
	CString olDeviceFile
	int ilKomma1 = olDevices.Find(',');
	int ilKomma2 = olDevices.ReverseFind(',');
	if ((ilKomma1 > -1) && (ilKomma2 > -1))
	{	
		increment ilKomma1;
		olDeviceFile = olDevices.Mid(ilKomma1,ilKomma2-ilKomma1);
	}
	olDeviceFile += ".drv"
	
	HMODULE hlModule = 0;
	if (olDeviceFile.IsEmpty() == FALSE)
	{
		hlModule = GetModuleHandle(olDeviceFile);
		if (hlModule == NULL)
		{
			LoadLibrary(olDeviceFile);
		}
	}
	if (hlModule != 0)
	{
	}

  *****/

	char pclDevices[182];

	::GetProfileString("windows", "device","",pclDevices,180);
	CString olDevices = pclDevices;
	CString olDeviceName;
	int ilKomma = olDevices.Find(',');
	if (ilKomma > -1)
	{
		olDeviceName = olDevices.Left(ilKomma);
	}


	char pclDeviceName[182];
	strcpy(pclDeviceName,olDeviceName);

	HANDLE  hlPrinter = 0;
	static HANDLE hDevMode = 0;
	
//	if (hDevMode == 0)
	{
		if (OpenPrinter(pclDeviceName,&hlPrinter,NULL) == TRUE)
		{
		//	if (GetPrinter(hlPrinter,2,(unsigned char *)&rlPrinterInfo,sizeof(rlPrinterInfo),&llBytesReceived) == TRUE)
			{
				LONG llDevModeLen = DocumentProperties(NULL,hlPrinter,pclDeviceName,NULL,NULL,0);
				if (llDevModeLen > 0)
				{
					hDevMode = GlobalAlloc(GMEM_MOVEABLE,llDevModeLen);
					DEVMODE *prlDevMode = (DEVMODE *) GlobalLock(hDevMode);
					DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,NULL,DM_OUT_BUFFER);
					DWORD dm_orientation =  DM_ORIENTATION;
					prlDevMode->dmFields = DM_ORIENTATION | DM_FORMNAME | DM_PAPERSIZE ;
					//prlDevMode->dmFormName = "A3";
					for (int ilLc = 0; pcpFormName[ilLc] != '\0' && ilLc < 30; ilLc++)
					{
						prlDevMode->dmFormName[ilLc] = pcpFormName[ilLc];
					}
					prlDevMode->dmFormName[ilLc] = '\0';

					prlDevMode->dmPaperSize = ipPaperSize;
					//strcpy(&prlDevMode->dmFormName,"A3");
					switch (ipOrientation)
					{
					case PRINT_PORTRAIT :
						prlDevMode->dmOrientation = DMORIENT_PORTRAIT;
						break;
					case PRINT_LANDSCAPE:
					default:
						prlDevMode->dmOrientation = DMORIENT_LANDSCAPE;
					}
					if (bgIsPrm)
					{
						char tmpFormname[24];

						prlDevMode->dmPaperSize = DMPAPER_A4;
//						prlDevMode->dmOrientation = DMORIENT_LANDSCAPE;
						strcpy(tmpFormname,"A4");
						for (int ilLc = 0; pcpFormName[ilLc] != '\0' && ilLc < 24; ilLc++)
						{
							prlDevMode->dmFormName[ilLc] = tmpFormname[ilLc];
						}
						prlDevMode->dmFormName[ilLc] = 0;
					}
					DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,prlDevMode,DM_IN_BUFFER);
					GlobalUnlock(hDevMode);
				}
			}
			ClosePrinter(hlPrinter);
		}
	}
   
	
   
  
	imPageNo = 0;

	CPrintPageDialog *polPrintDialog = new CPrintPageDialog(
		  FALSE,PD_ALLPAGES|PD_PAGENUMS|PD_NOSELECTION|PD_USEDEVMODECOPIESANDCOLLATE,bmEnablePageSelection,
		  pomParent);

	if (hDevMode != 0)
	{
		polPrintDialog->m_pd.hDevMode = hDevMode;
	}
	LPDEVMODE prlOldDevMode = polPrintDialog->GetDevMode( );

//	prlOldDevMode->dmOrientation = 2;

	imFromPage = -1;
	imToPage = -1;

	ilRc = polPrintDialog->DoModal();
	if (ilRc != IDCANCEL )
	{
		bmIsPageSelected = polPrintDialog->IsPageSelected();
		if(bmIsPageSelected && polPrintDialog->IsPageNoValid() == TRUE)
		{
			imFromPage = polPrintDialog->m_pd.nFromPage;
			imToPage = polPrintDialog->m_pd.nToPage;
		}

		LPDEVMODE prlDevMode = polPrintDialog->GetDevMode( );

		hlHdc = polPrintDialog->GetPrinterDC();
		if (!prlDevMode || !hlHdc)
		{
			return FALSE; 
		}

		smPaperSize = prlDevMode->dmPaperSize;
		imOrientation = (prlDevMode->dmOrientation == DMORIENT_PORTRAIT) ? PRINT_PORTRAIT : PRINT_LANDSCAPE;
		omCdc.Attach(hlHdc);
		//pomCdc->SetMapMode(MM_TWIPS);
		//pomCdc->SetMapMode(MM_HIMETRIC);
		InitializePrintSetup();
	}

	return ilRc;
}

BOOL CCSPrint::InitializePrintSetup(PRINT_OPTION epPrintOption)
{	
	int ilRc = TRUE;
	emPrintOption = epPrintOption;
	InitializeDevicePointer();

	if(emPrintOption == PRINT_PRINTER)
	{		
		pomCdc->SetMapMode(MM_TEXT);
	
		imLogPixelsY = pomCdc->GetDeviceCaps(LOGPIXELSY);
		imLogPixelsX = pomCdc->GetDeviceCaps(LOGPIXELSX);

		imWidth = pomCdc->GetDeviceCaps(HORZSIZE) * 10;
		imHeight = pomCdc->GetDeviceCaps(VERTSIZE)* 10;
	}
	else if(emPrintOption == PRINT_PREVIEW)
	{
		imLogPixelsY = pomCdc->GetDeviceCaps(LOGPIXELSY);
		imLogPixelsX = pomCdc->GetDeviceCaps(LOGPIXELSX);

		imWidth = pomCdc->GetDeviceCaps(HORZSIZE) * 10;
		imHeight = pomCdc->GetDeviceCaps(VERTSIZE) * 10;
	}
	
	if (imLineHeight == 0)
		imLineHeight = 85;
	imMaxLines = (imHeight-700) / imLineHeight;
	imYOffset = MMY(400);

	ThinPen.DeleteObject();
	MediumPen.DeleteObject();
	ThickPen.DeleteObject();
	DottedPen.DeleteObject();
	omRgn.DeleteObject();

	omRgn.DeleteObject();
	omRgn.CreateRectRgn(0,0,pomCdc->GetDeviceCaps(HORZRES),pomCdc->GetDeviceCaps(VERTRES));
	pomCdc->SelectClipRgn(&omRgn);

	TRACE("X = %d, Y = %d \n",imLogPixelsX,imLogPixelsY);
	LOGFONT rlLf;
	memset(&rlLf, 0, sizeof(LOGFONT));

	omSmallFont_Regular.DeleteObject();
	omMediumFont_Regular.DeleteObject();
	omLargeFont_Regular.DeleteObject();
	omSmallFont_Bold.DeleteObject();
	omSmallFont_UnderlineBold.DeleteObject();
	omMediumFont_Bold.DeleteObject();
	omLargeFont_Bold.DeleteObject();
	omCourierNormal10.DeleteObject();
	omCourierNormal7.DeleteObject();
	omCCSFont.DeleteObject();

	/*omSmallFont_Regular.Detach();
	omMediumFont_Regular.Detach();
	omLargeFont_Regular.Detach();
	omSmallFont_Bold.Detach();
	omSmallFont_UnderlineBold.Detach();
	omMediumFont_Bold.Detach();
	omLargeFont_Bold.Detach();
	omCourierNormal10.Detach();
	omCourierNormal7.Detach();
	omCCSFont.Detach();*/

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
	rlLf.lfWeight = FW_NORMAL;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
	lstrcpy(rlLf.lfFaceName, "Arial");
	ilRc = omSmallFont_Regular.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
	ilRc = omMediumFont_Regular.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
	ilRc = omLargeFont_Regular.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
	rlLf.lfWeight = FW_BOLD;
	ilRc = omSmallFont_Bold.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
	rlLf.lfWeight = FW_BOLD;
	rlLf.lfUnderline = TRUE;
	ilRc = omSmallFont_UnderlineBold.CreateFontIndirect(&rlLf);

	rlLf.lfUnderline = FALSE;
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
	ilRc = omMediumFont_Bold.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
	ilRc = omLargeFont_Bold.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(10, imLogPixelsY, 72);
	rlLf.lfWeight = FW_NORMAL;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
	lstrcpy(rlLf.lfFaceName, "Courier New");
	ilRc = omCourierNormal10.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(6, imLogPixelsY, 72);
	rlLf.lfWeight = FW_NORMAL;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
	lstrcpy(rlLf.lfFaceName, "Courier New");
	ilRc = omCourierNormal7.CreateFontIndirect(&rlLf);

	rlLf.lfHeight = - MulDiv(10, imLogPixelsY, 72);
	rlLf.lfPitchAndFamily = 2;
	rlLf.lfCharSet = SYMBOL_CHARSET;
	rlLf.lfWeight = 400;
	lstrcpy(rlLf.lfFaceName, "ccs");
	ilRc = omCCSFont.CreateFontIndirect(&rlLf);

	ThinPen.DeleteObject();
	MediumPen.DeleteObject();
	ThickPen.DeleteObject();
	DottedPen.DeleteObject();

	ThinPen.CreatePen(PS_SOLID, MulDiv(2, imLogPixelsX, 254), RGB(0,0,0));
	MediumPen.CreatePen(PS_SOLID, MulDiv(4, imLogPixelsX, 254), RGB(0,0,0));
	ThickPen.CreatePen(PS_SOLID, MulDiv(8, imLogPixelsX, 254), RGB(0,0,0));
	DottedPen.CreatePen(PS_DOT,1, RGB(0,0,0));

	// loading of the header bitmap from ceda.ini defined resource
	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(pcgAppName, "LOGO","",
		pclTmpText, sizeof pclTmpText, pclConfigPath);

	// userdefined "logo" available ?
	if (strlen(pclTmpText))
	{
#if	0
		// load userdefined logo from file and resize it to huge scale
//			HBITMAP hlBitmap = (HBITMAP)::LoadImage(NULL,pclTmpText,IMAGE_BITMAP,0,0,LR_LOADFROMFILE|LR_CREATEDIBSECTION);
		HBITMAP hlBitmap = (HBITMAP)::LoadImage(NULL,pclTmpText,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
		if (hlBitmap)
		{
			if(omBitmap.m_hObject != NULL)
			{
				BOOL blResult = omBitmap.Attach(hlBitmap);
				VERIFY(blResult);
			}

			CBitmap olBitmap;
			olBitmap.LoadBitmap(IDB_OPSSPM);
			BITMAP rlHugeBitmap;
			olBitmap.GetBitmap(&rlHugeBitmap);

			BITMAP rlCurrBitmap;
			omBitmap.GetBitmap(&rlCurrBitmap);

			CDC olMemDc;
			olMemDc.CreateCompatibleDC(pomCdc);
			olMemDc.SelectObject(&omBitmap);

			
			CPoint rlHugeSize(rlHugeBitmap.bmWidth,rlHugeBitmap.bmHeight);
			olMemDc.DPtoLP(&rlHugeSize);

			CPoint rlCurrSize(rlCurrBitmap.bmWidth,rlCurrBitmap.bmHeight);
			omMemDc.DeleteDC();
			omMemDc.CreateCompatibleDC(pomCdc);
			omMemDc.DPtoLP(&rlCurrSize);
			blResult = omMemDc.StretchBlt(0,0,rlHugeSize.x,rlHugeSize.y,&olMemDc,0,0,rlCurrSize.x,rlCurrSize.y,SRCCOPY);

			if (!blResult)
			{
				omBitmap.DeleteObject();
				omBitmap.LoadBitmap(IDB_OPSSPM);
				omMemDc.SelectObject(&omBitmap);
			}
		}
		else
		{
			DWORD ulError = ::GetLastError();
			omBitmap.DeleteObject();
			omBitmap.LoadBitmap(IDB_OPSSPM);
			omMemDc.DeleteDC();
			omMemDc.CreateCompatibleDC(pomCdc);
			omMemDc.SelectObject(&omBitmap);
		}
#else

		if (!LoadBitmapFromBMPFile(pclTmpText,&hmBitmap,&hmPalette))
		{
			hmBitmap = NULL;
			hmPalette= NULL;
			omBitmap.DeleteObject();
			omBitmap.LoadBitmap(IDB_OPSSPM);			
			omMemDc.DeleteDC();
			omMemDc.CreateCompatibleDC(pomCdc);
			omMemDc.SelectObject(&omBitmap);
		}
#endif

	}
	else
	{
		omBitmap.DeleteObject();
		omBitmap.LoadBitmap(IDB_OPSSPM);
		omMemDc.DeleteDC();
		omMemDc.CreateCompatibleDC(pomCdc);
		omMemDc.SelectObject(&omBitmap);
	}

	omCcsBitmap.DeleteObject();
	omCcsBitmap.LoadBitmap(IDB_CCS);
	omCcsMemDc.DeleteDC();
	omCcsMemDc.CreateCompatibleDC(pomCdc);
	omCcsMemDc.SelectObject(&omCcsBitmap);

	bmIsInitialized = TRUE;

	TEXTMETRIC olTm;
	pomCdc->GetTextMetrics(&olTm);

	int  ilHeight = olTm.tmHeight + olTm.tmExternalLeading;
	imBarHeight = (olTm.tmHeight + olTm.tmExternalLeading);
	imGanttLineHeight = (int) (imBarHeight * 1.2);

	// calculate vertical ofset of text in a bar
	
	CFont *polOldFont = pomCdc->SelectObject(&omSmallFont_Regular);
	CSize olSize = pomCdc->GetTextExtent("LH 4711");	
	imBarVerticalTextOffset = (int) ((imBarHeight-olSize.cy)/2);
	pomCdc->SelectObject(polOldFont);	 
	return ilRc;
}

BOOL CCSPrint::PrintHeader()
{
	PrintHeader(omHeader[0],omHeader[1],omHeader[2],omHeader[3]);
	return TRUE;
}


BOOL CCSPrint::PrintHeader(CString opHeader1,CString opHeader2,
					  CString opHeader3,CString opHeader4)
{	
	CFont *polOldFont;
	CPen  *polOldPen; 

	imPageNo++;
	imLineNo = 0;

	int ilBitmapWidth = 0, ilBitmapHeight = 0;
	ReadBitmapDimensions(ilBitmapWidth, ilBitmapHeight);


	if (hmBitmap)	// user defined logo bitmap available ?
	{
		HBITMAP  hlOldBitmap;
		HPALETTE hlOldPalette;

		BITMAP rlBitmap;
		HDC	 hlMemDC;

		::GetObject(hmBitmap,sizeof(BITMAP),&rlBitmap);	
		hlMemDC = ::CreateCompatibleDC(pomCdc->m_hDC);
		hlOldBitmap = (HBITMAP)::SelectObject(hlMemDC,hmBitmap);
		hlOldPalette= ::SelectPalette(pomCdc->m_hDC,hmPalette,FALSE);
		::RealizePalette(pomCdc->m_hDC);

		CSize rlSize(rlBitmap.bmWidth,rlBitmap.bmHeight);
		pomCdc->DPtoLP(&rlSize);
		BOOL bResult;
		bResult= ::StretchBlt(pomCdc->m_hDC,MMX(100),MMY(150),MMX(ilBitmapWidth),MMY(ilBitmapHeight),hlMemDC,0,0,rlSize.cx,rlSize.cy,SRCCOPY);
		
		::SelectObject(hlMemDC,hlOldBitmap);
		::SelectPalette(pomCdc->m_hDC,hlOldPalette,FALSE);
	}
	else
	{
		pomCdc->StretchBlt(MMX(100),MMY(150),MMX(ilBitmapWidth),MMY(ilBitmapHeight),&omMemDc,0,0,519,519, SRCCOPY);
	}
	
    polOldPen = pomCdc->SelectObject(&ThinPen);

	pomCdc->Rectangle(MMX(ilBitmapWidth+130),MMY(150),MMX(imWidth-50),MMY(330));
    pomCdc->MoveTo(MMX(ilBitmapWidth+150),MMY(250));
    pomCdc->LineTo(MMX(imWidth-80),MMY(250));

    polOldFont = pomCdc->SelectObject(&omLargeFont_Regular);
	pomCdc->TextOut(MMX(ilBitmapWidth+160),MMY(170), opHeader1, strlen(opHeader1) );

    pomCdc->SelectObject(&omSmallFont_Bold);
	pomCdc->TextOut(MMX(imWidth-360),MMY(210), opHeader2, strlen(opHeader2) );

    pomCdc->SelectObject(&omMediumFont_Regular);
	pomCdc->TextOut(MMX(ilBitmapWidth+160),MMY(270), opHeader3, strlen(opHeader3) );

	pomCdc->SelectObject(polOldFont);
	pomCdc->SelectObject(polOldPen);

	return TRUE;
}

void CCSPrint::ReadBitmapDimensions(int &ripBitmapWidth, int &ripBitmapHeight)
{
	ripBitmapWidth = 270;
	ripBitmapHeight = 270;

	char pclTmpText[512];
	GetPrivateProfileString(pcgAppName, "PRINTOUT_BITMAPSIZE", "270,270", pclTmpText, sizeof pclTmpText, ogBasicData.GetConfigFileName());
	if(strlen(pclTmpText) > 0 && strchr(pclTmpText,',') != NULL)
	{
		CStringArray olTmp;
		ogBasicData.ExtractItemList(pclTmpText, &olTmp, ',');
		ripBitmapWidth = atoi(olTmp[0]);
		ripBitmapHeight = atoi(olTmp[1]);
	}
}

BOOL CCSPrint::PrintFooter()
{
	return TRUE;

}


BOOL CCSPrint::PrintFooter(CString opFooter1,CString opFooter2)
{
	CFont *polOldFont;
	CPen  *polOldPen; 
	int ilXOffset = MMX(200);
	int ilYOffset = pomCdc->GetDeviceCaps(VERTRES)-MMY(50);
	CSize olSize;

    polOldPen = pomCdc->SelectObject(&ThinPen);
	
	 /*********
    pomCdc->MoveTo(MMX(180),ilYOffset-MMY(50));
    pomCdc->LineTo(MMX(2850),ilYOffset-MMY(50));
	****************/

    pomCdc->MoveTo(MMX(180),ilYOffset-MMY(50));
    pomCdc->LineTo(MMX(imWidth-50),ilYOffset-MMY(50));
	 
/*
    polOldFont = pomCdc->SelectObject(&omCCSFont);
	olSize = pomCdc->GetTextExtent("CCS ");
	pomCdc->TextOut(ilXOffset,ilYOffset-olSize.cy, "CCS ", 3 );
 	ilXOffset += olSize.cx;
*/

	//pomCdc->StretchBlt(ilXOffset,ilYOffset-MMY(40),MMX(120),MMY(40),&omCcsMemDc,
	//	0,0,261,78, SRCCOPY);

	ilXOffset += 200;

    polOldFont = pomCdc->SelectObject(&omSmallFont_Regular);
	olSize = pomCdc->GetTextExtent(opFooter1);
	pomCdc->TextOut(ilXOffset,ilYOffset-olSize.cy,opFooter1);
 	ilXOffset += olSize.cx;

    pomCdc->SelectObject(&omSmallFont_Bold);
	olSize = pomCdc->GetTextExtent(opFooter2);
	pomCdc->TextOut(ilXOffset,ilYOffset-olSize.cy+5,opFooter2);


 	ilXOffset = MMX(imWidth - 200);
    pomCdc->SelectObject(&omSmallFont_Regular);
	CString olPage = GetString(IDS_PRINTPAGE);
	olSize = pomCdc->GetTextExtent(olPage);
	pomCdc->TextOut(ilXOffset,ilYOffset-olSize.cy,olPage,olPage.GetLength());
 	ilXOffset += olSize.cx;

	char olPageNo[24];
	sprintf(olPageNo,"%3d",imPageNo);
    pomCdc->SelectObject(&omSmallFont_Bold);
	olSize = pomCdc->GetTextExtent(olPageNo);
	pomCdc->TextOut(ilXOffset,ilYOffset-olSize.cy,olPageNo,strlen(olPageNo));


	pomCdc->SelectObject(polOldFont);
	pomCdc->SelectObject(polOldPen);

	/************** MCU inserted to newlines 17.11 *****************/
//	imLineNo++;
//	imLineNo++;

	return TRUE;
}


BOOL CCSPrint::SelectFramePen(int ipFrameType)
{
	int ilRc = TRUE; 
	switch (ipFrameType)
	{
	case PRINT_FRAMETHIN: 
		pomCdc->SelectObject(&ThinPen);
		break;
	case PRINT_FRAMEMEDIUM: 
		pomCdc->SelectObject(&MediumPen);
		break;
	case PRINT_FRAMETHICK: 
		pomCdc->SelectObject(&ThickPen);
		break;
	default: ilRc = FALSE;

	}
	return ilRc;
}

void CCSPrint::PrintLeft(CRect opRect,CString opText)
{

	TEXTMETRIC olTm;

	// insert a small space at the left side
	opRect.left += MMX(10);
	pomCdc->GetTextMetrics(&olTm);
	int  ilHeight = olTm.tmHeight + olTm.tmExternalLeading;
	if(emPrintOption == PRINT_PRINTER)
	{
		pomCdc->TextOut(opRect.left,opRect.bottom-ilHeight,opText);
	}
	else if(emPrintOption == PRINT_PREVIEW)
	{
		CRect olRect = opRect;
		olRect.top = opRect.bottom - ilHeight;
		pomCdc->DrawText(opText,olRect,DT_LEFT);
	}
	
}

void CCSPrint::PrintRight(CRect opRect,CString opText)
{

	CSize olSize = pomCdc->GetTextExtent(opText);

	pomCdc->TextOut(opRect.right-olSize.cx,opRect.bottom-olSize.cy,opText);
	
}

void CCSPrint::PrintCenter(CRect opRect,CString opText)
{

	CSize olSize = pomCdc->GetTextExtent(opText);
	
	int ilOffset = max(0,(int)(opRect.right-opRect.left-olSize.cx)/2);
	pomCdc->TextOut(opRect.left+ilOffset,opRect.bottom-olSize.cy,opText);
	
}

BOOL CCSPrint::PrintLine(CCSPtrArray <PRINTELEDATA> &ropPrintLine)
{
	CPen *polOldPen;
	
	int ilLeftPos = imLeftOffset;
	int ilLineTop;
	if (imLineTop == 0)
	{
		ilLineTop = imFirstLine;
	}
	else
	{
		ilLineTop = imLineTop;
	}
	imLineTop += imLineHeight;

	//imFirstLine + (imLineHeight*imLineNo);

	polOldPen = pomCdc->SelectObject(&ThinPen);


	
	
	for(int i = 0; i < ropPrintLine.GetSize(); i++)
	{
		PRINTELEDATA *prlEle = &ropPrintLine[i];
		
		int ilLineOffset = imLineNo * imLineHeight;

		if (SelectFramePen(prlEle->FrameLeft) == TRUE)
		{
			pomCdc->MoveTo(MMX(ilLeftPos),MMY(ilLineTop));
			pomCdc->LineTo(MMX(ilLeftPos),MMY(ilLineTop+imLineHeight));
		}
		if (SelectFramePen(prlEle->FrameTop) == TRUE)
		{
			pomCdc->MoveTo(MMX(ilLeftPos),MMY(ilLineTop));
			pomCdc->LineTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop));
		}
		if (SelectFramePen(prlEle->FrameBottom) == TRUE)
		{
			pomCdc->MoveTo(MMX(ilLeftPos),MMY(ilLineTop+imLineHeight));
			pomCdc->LineTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight));
		}
		if (SelectFramePen(prlEle->FrameRight) == TRUE)
		{
			pomCdc->MoveTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop));
			pomCdc->LineTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight));
		}
		if (prlEle->Text.IsEmpty() == FALSE)
		{
		
			
			
			CRgn rlRgn;

			rlRgn.CreateRectRgn(MMX(ilLeftPos),MMY(ilLineTop+2),
				MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight-2));
		
			
			if(emPrintOption == PRINT_PRINTER)
			{
				pomCdc->SelectClipRgn(&rlRgn);
			}
			else if(emPrintOption == PRINT_PREVIEW)
			{
				pomCdc->SelectClipRgn(&rlRgn,RGN_OR);
			}
			
			CFont *polOldFont = pomCdc->SelectObject(prlEle->pFont);
			CRect olRect(MMX(ilLeftPos+2),MMY(ilLineTop+2),
				MMX(ilLeftPos+prlEle->Length-2),MMY(ilLineTop+imLineHeight-2));
			switch (prlEle->Alignment)
			{
			case PRINT_LEFT: PrintLeft(olRect,prlEle->Text);
				break;
			case PRINT_RIGHT: PrintRight(olRect,prlEle->Text);
				break;
			case PRINT_CENTER: PrintCenter(olRect,prlEle->Text);
				break;
			}
			pomCdc->SelectClipRgn(&omRgn);
			rlRgn.DeleteObject();
		}
		ilLeftPos += prlEle->Length;

	}
	ropPrintLine.DeleteAll();
	imLineNo++;
	return TRUE;
}



BOOL CCSPrint::PrintTimeScale(int ipStartX,int ipEndX,CTime opStartTime,CTime opEndTime,CString opText)
{
	imYOffset = MMY(imFirstLine);
	CFont *polTextFont;
	CPen *polPen;
	int ilSteps;
	int ilPixels;
	int ilHourPixels;
	int i;
	

	int ilTsHeight;
	int ilLongLine;
	int ilMediumLine;
	int ilShortLine;
	int ilStartX;
	int ilStartY;
	int ilEndX;
	int ilEndY; 
	int ilActX;
	int ilActY;
	int ilTextStartY;


	//imMaxLines = 900;
	ilStartX = MMX(ipStartX);
	ilEndX   = MMX(imWidth-50);
	ilActX   = ilStartX;

	imGanttStartX = ilStartX;

	CPen  *polOldPen  = pomCdc->SelectObject(&MediumPen);
	CFont *polOldFont = pomCdc->SelectObject(&omSmallFont_Bold);
    
	//  rounding to full hour
    CTime olActHour(opStartTime.GetYear(),opStartTime.GetMonth(),opStartTime.GetDay(),
		opStartTime.GetHour(),0,0);
	//  add one hour if we are in the second half of the actual hour
	if (opStartTime.GetMinute() > 30)
	{
		olActHour += CTimeSpan(0,1,0,0);
	}
	omStartTime = olActHour;
    CTimeSpan olDuration = opEndTime - omStartTime;
	int ilHours = olDuration.GetTotalHours();
	imHours = ilHours;
	omEndTime = omStartTime + CTimeSpan(0,ilHours,0,0);

    
	ilSteps = ilHours * 6;
	ilHourPixels = (ilEndX - ilStartX) / ilHours;
	ilPixels = (int) ((int)ilHourPixels / 0.6);
	if ((ilPixels % 10) > 5)
	{
		ilPixels += 10;
	//	ilHourPixels += 1;
	}
	ilPixels = (int) ilPixels / 10;
	imHourPixels = ilHourPixels;

	if (ilHours < 9)
	{
		ilTsHeight = MMY(100);
		ilLongLine = MMY(35);
		ilMediumLine = MMY(60);
		ilShortLine  = MMY(75);
		ilStartY = imYOffset;
		ilEndY   = ilStartY+ilTsHeight; 
		ilActY   = ilStartY;
		ilTextStartY = imYOffset - MMY(20);
		polTextFont = &omMediumFont_Bold;
		polPen = &MediumPen;
	}
	else
	{
		ilTsHeight = MMY(83);
		ilLongLine = MMY(45);
		ilMediumLine = MMY(55);
		ilShortLine  = MMY(70);
		ilStartY = imYOffset;
		ilEndY   = ilStartY+ilTsHeight; 
		ilActY   = ilStartY;
		ilTextStartY = imYOffset+MMY(10);
		polTextFont = &omSmallFont_Bold;
		polPen = &ThinPen;
	}

	// save the Y position of timescale
	imGanttStartY = ilEndY;
	imGanttEndY = ilEndY;

	// first draw a full height line  with medium pen
	pomCdc->MoveTo(ilStartX,ilStartY);
	pomCdc->LineTo(ilStartX,ilEndY);

	// set the pen for the timescale lines
	pomCdc->SelectObject(polPen);

	for ( i = 1; i  < ilSteps; i++)
	{
		ilActX += ilPixels;
		if ( i % 6 == 0)
		{
			// draw long line every hour
			pomCdc->MoveTo(ilActX,ilEndY);
			pomCdc->LineTo(ilActX,ilStartY+ilLongLine);
			
		}
		else
		{
			if ( i % 3 == 0)
			{
				// draw medium line every half hour
				pomCdc->MoveTo(ilActX,ilEndY);
				pomCdc->LineTo(ilActX,ilStartY+ilMediumLine);
			}
			else
			{
				// draw short line every 15 minutes
				pomCdc->MoveTo(ilActX,ilEndY);
				pomCdc->LineTo(ilActX,ilStartY+ilShortLine);
			}
		}
	}
	ilActX += ilPixels;

	// new end of gantt chart
	imGanttEndX   = ilActX;

	// calculate the factor for GetXFromTime
    int ilTotalMin = ilHours*60;
	int ilTotalPixels = imGanttEndX-imGanttStartX;
	dmXFactor = (double) ilTotalPixels / ilTotalMin;

	// at the end draw another full height line with medium pen
	pomCdc->SelectObject(&MediumPen);
	pomCdc->MoveTo(imGanttEndX,ilStartY);
	pomCdc->LineTo(imGanttEndX,ilEndY);
	
	// underline the whole timescale
	pomCdc->MoveTo(imLeftOffset,ilEndY);
	pomCdc->LineTo(imGanttEndX,ilEndY);


	// write headers every hour
	polOldFont = pomCdc->SelectObject(polTextFont);
	ilActX = ilStartX;
	for ( i = 1; i < ilHours; i++)
	{
		ilActX += ilHourPixels;
		olActHour += CTimeSpan(0,1,0,0);
		CString olText = olActHour.Format("%H:%M");
		CSize olSize = pomCdc->GetTextExtent(olText);
		int ilOffset = (int) olSize.cx / 2;
		pomCdc->TextOut(ilActX-ilOffset,ilTextStartY,olText);
	}

	
	if (opText.IsEmpty() == FALSE)
	{
		// print chart name left of timescale
		pomCdc->SelectObject(&omMediumFont_Bold);
//hag20020524		pomCdc->SelectObject(&omLargeFont_Bold);
		CSize olSize = pomCdc->GetTextExtent(opText);
		if (olSize.cx > (imGanttStartX-imLeftOffset-MMX(20)))
		{
			pomCdc->TextOut(imGanttStartX-olSize.cx-MMX(10),imGanttStartY-olSize.cy-MMY(15),opText);
		}
		else
		{
			// there is space enough, we can center the text
			int ilOffset = (int) ((imGanttStartX-imLeftOffset-olSize.cx) / 2);
			pomCdc->TextOut(imLeftOffset+ilOffset,imGanttStartY-olSize.cy-MMY(15),opText);
		}
	}
	imYOffset = ilEndY  + MMY(10);
    
    pomCdc->SelectObject(polOldPen);
    pomCdc->SelectObject(polOldFont);
	return TRUE;
}



BOOL CCSPrint::PrintGanttHeader(int ipStartX,int ipEndX,CString opText)
{
	CFont *polTextFont;
	CPen *polPen;

	int ilTsHeight;
	int ilStartX;
	int ilStartY;
	int ilEndX;
	int ilEndY; 
	int ilActX;
	int ilActY;
	int ilTextStartY;


	ilStartX = MMX(ipStartX);
//	ilEndX   = MMX(ipEndX);
	ilEndX   = MMX(imWidth-100);
	ilActX   = ilStartX;

	imGanttStartX = ilStartX;

	CPen  *polOldPen  = pomCdc->SelectObject(&MediumPen);
	CFont *polOldFont = pomCdc->SelectObject(&omSmallFont_Bold);
    
//	imYOffset += MMY(40);
    
	ilTsHeight = MMY(83);
	ilStartY = imYOffset;
	ilEndY   = ilStartY+ilTsHeight; 
	ilActY   = ilStartY;
	ilTextStartY = imYOffset;
	polTextFont = &omSmallFont_Bold;
	polPen = &ThinPen;

	// save the Y position of timescale
	imGanttStartY = ilEndY;
	imGanttEndY = ilEndY;

	// first draw a full height line  with medium pen
	pomCdc->MoveTo(ilStartX,ilStartY);
	pomCdc->LineTo(ilStartX,ilEndY);

	// at the end draw another full height line with medium pen
	pomCdc->MoveTo(imGanttEndX,ilStartY);
	pomCdc->LineTo(imGanttEndX,ilEndY);
	
	// underline the whole timescale
	pomCdc->MoveTo(imLeftOffset,ilEndY);
	pomCdc->LineTo(imGanttEndX,ilEndY);

	
	if (opText.IsEmpty() == FALSE)
	{
		// print chart name left of timescale
		pomCdc->SelectObject(&omMediumFont_Bold);
//hag20020524		pomCdc->SelectObject(&omLargeFont_Bold);
		
		CSize olSize = pomCdc->GetTextExtent(opText);
		if (olSize.cx > (imGanttStartX-imLeftOffset-MMX(20)))
		{
			pomCdc->TextOut(imGanttStartX-olSize.cx-MMX(10),imGanttStartY-olSize.cy-MMY(15),opText);
		}
		else
		{
			// there is space enough, we can center the text
			int ilOffset = (int) ((imGanttStartX-imLeftOffset-olSize.cx) / 2);
			pomCdc->TextOut(imLeftOffset+ilOffset,imGanttStartY-olSize.cy-MMY(15),opText);
		}
	}
	imYOffset = ilEndY  + MMY(10);
	imGanttEndY = ilEndY;	/* to avaid wrong painting if gantt is empty */

    pomCdc->SelectObject(polOldPen);
    pomCdc->SelectObject(polOldFont);
	return TRUE;
}

static int ComparePrintBar(const PRINTBARDATA **e1, const PRINTBARDATA **e2)
{
	return (int)((**e1).StartTime.GetTime() - (**e2).StartTime.GetTime());
}

BOOL CCSPrint::PrintGanttLine(CString opText,CCSPtrArray<PRINTBARDATA> &ropPrintLine,
							  CCSPtrArray<PRINTBARDATA> &ropBkBars)
{
	int i,j,k;
	BOOL blFound;
	BOOL blDontOverlap;
	CPen *polOldPen;

	// first calculate max overlaplevel and line height
	ropPrintLine.Sort(ComparePrintBar); // sort for time
	// calculate overlaplevel
	int ilBarCount = ropPrintLine.GetSize();
	int ilMaxOverlapLevel = 0;
	// start the loop with 1, the first bar can't be overlapped
	for ( i = 1; i < ilBarCount; i++)
	{
		if (ropPrintLine[i-1].EndTime > ropPrintLine[i].StartTime )
		{
			// found an overlapping bar, check if we can it display at lower overlap level
			
			blDontOverlap = FALSE;
			for (j = 0; (j <= ilMaxOverlapLevel) && (blDontOverlap == FALSE); j++)
			{
				blFound = FALSE;
				for( k = 0; (k < ilBarCount) && (blFound == FALSE) && (k < i); k++)
				{
					if (ropPrintLine[k].OverlapLevel == j)
					{
						if (ropPrintLine[k].EndTime >= ropPrintLine[i].StartTime)
						{
							blFound = TRUE;
						}
					}
				}
				if (blFound == FALSE)
				{
					// no overlapping at this line, so we can use it
					ropPrintLine[i].OverlapLevel = j;
					blDontOverlap = TRUE;
				}
			}
			if (blDontOverlap == FALSE)
			{
				// we have not found a free line, extend overlaplevel
				ilMaxOverlapLevel++;
				ropPrintLine[i].OverlapLevel = ilMaxOverlapLevel;
			}
		}
	}

	int iltest = pomCdc->GetDeviceCaps(VERTRES);
	ilMaxOverlapLevel++;  
	int ilHeightOfThisLine = ilMaxOverlapLevel * imGanttLineHeight;
	if ((imYOffset + ilHeightOfThisLine) < (pomCdc->GetDeviceCaps(VERTRES)-MMY(100)))
	{
		// this line fit on page
		// now we know all overlaplevels and start printing bars
		CRgn rlRgn;

		rlRgn.CreateRectRgn(imGanttStartX,imGanttStartY,imGanttEndX,pomCdc->GetDeviceCaps(VERTRES)-MMY(100));

		if(emPrintOption == PRINT_PRINTER)
		{
			pomCdc->SelectClipRgn(&rlRgn);
		}		
		else if(emPrintOption == PRINT_PREVIEW)
		{
			pomCdc->SelectClipRgn(&rlRgn,RGN_OR);
		}

		// print background bars first
		for ( i = 0; i < ropBkBars.GetSize(); i++)
		{
			PrintBkBar(ropBkBars[i],ilHeightOfThisLine);
		}

		for ( i = 0; i < ilBarCount; i++)
		{
			PrintGanttBar(ropPrintLine[i]);
		}

		if(emPrintOption == PRINT_PRINTER)
		{
			pomCdc->SelectClipRgn(&omRgn);
		}		
		else if(emPrintOption == PRINT_PREVIEW)
		{
			pomCdc->SelectClipRgn(&omRgn,RGN_OR);
		}
		rlRgn.DeleteObject();

		int ilNewYOffset = imYOffset + ((ilMaxOverlapLevel) * imGanttLineHeight);


		// also print the line text in vertical scale

		rlRgn.CreateRectRgn(MMX(imLeftOffset),imYOffset,imGanttStartX-MMX(10),ilNewYOffset);
		if(emPrintOption == PRINT_PRINTER)
		{
			pomCdc->SelectClipRgn(&rlRgn);
		}		
		else if(emPrintOption == PRINT_PREVIEW)
		{
			pomCdc->SelectClipRgn(&rlRgn,RGN_OR);
		}
		CFont *polOldFont = pomCdc->SelectObject(&omSmallFont_Bold);
		CSize olSize = pomCdc->GetTextExtent(opText);
		int ilTextOffset = ((ilNewYOffset - imYOffset) / 2) - (olSize.cy/2);
		if(emPrintOption == PRINT_PRINTER)
		{
			pomCdc->TextOut(MMX(imLeftOffset+4),imYOffset+ilTextOffset, opText);
		}
		else if(emPrintOption == PRINT_PREVIEW)
		{
			CRect olRect(MMX(imLeftOffset+4),imYOffset,imGanttStartX-MMX(10),ilNewYOffset);			
			pomCdc->DrawText(opText,olRect,DT_LEFT);
		}
		pomCdc->SelectObject(polOldFont);
		if(emPrintOption == PRINT_PRINTER)
		{
			pomCdc->SelectClipRgn(&omRgn);
		}		
		else if(emPrintOption == PRINT_PREVIEW)
		{
			pomCdc->SelectClipRgn(&omRgn,RGN_OR);
		}
		rlRgn.DeleteObject();


		// draw a horizontal line between gate lines
		polOldPen = pomCdc->SelectObject(&DottedPen);
		pomCdc->MoveTo(MMX(imLeftOffset),ilNewYOffset);
		pomCdc->LineTo(imGanttEndX,ilNewYOffset);
		pomCdc->SelectObject(polOldPen);

		// save new Y position 
		imYOffset = ilNewYOffset + MMY(10);
		imGanttEndY = imYOffset;

		ropPrintLine.DeleteAll();
		ropBkBars.DeleteAll();
		
		imLineNo++;
		return TRUE;
	}
	// line don't fit on page
	return FALSE;
	}


BOOL CCSPrint::PrintGanttBottom(void)
{
	CPen *polOldPen = pomCdc->SelectObject(&MediumPen);
	// rectangle around gantt chart
    pomCdc->MoveTo(imGanttStartX,imGanttStartY);
    pomCdc->LineTo(imGanttStartX,imGanttEndY);
    pomCdc->LineTo(imGanttEndX,imGanttEndY);
    pomCdc->LineTo(imGanttEndX,imGanttStartY);
	
	// line left of vertical scale
    pomCdc->MoveTo(imLeftOffset,imGanttStartY);
    pomCdc->LineTo(imLeftOffset,imGanttEndY);
    pomCdc->LineTo(imGanttStartX,imGanttEndY);

	pomCdc->SelectObject(polOldPen);

	imYOffset = imGanttEndY + MMY(40);

//#if 0
	if (imHourPixels != 0)
	{
		int ilPixel = (int) imHourPixels / 2;
		CPen *polOldPen = pomCdc->SelectObject(&DottedPen);

		for ( int ilStep = 1; ilStep < (imHours*2); ilStep++)
		{
			pomCdc->MoveTo(imGanttStartX + (ilPixel*ilStep) + ilStep,imGanttStartY);
			pomCdc->LineTo(imGanttStartX + (ilPixel*ilStep) + ilStep,imGanttEndY);
		}
		pomCdc->SelectObject(polOldPen);
	}
//#endif
	return TRUE;
}


int CCSPrint::GetXFromTime(CTime opTime)
{
    long llMinutes = (opTime - omStartTime).GetTotalMinutes();
	return (int) ((llMinutes * dmXFactor) + imGanttStartX);

}

BOOL CCSPrint::PrintGanttBar(PRINTBARDATA &ropBar)
{
	int ilStartX = GetXFromTime(ropBar.StartTime);
	int ilEndX   = GetXFromTime(ropBar.EndTime);
	int ilStartY = imYOffset + (ropBar.OverlapLevel * imGanttLineHeight);
	int ilEndY   = ilStartY + imBarHeight;

	//PRF 8704
	if(emPrintOption == PRINT_PREVIEW)
	{
		ilEndX = ilEndX > imGanttEndX ? imGanttEndX + 2: ilEndX;
		ilStartX = ilStartX < imGanttStartX ? imGanttStartX -2 : ilStartX;
	}

    CRect rcBackground(ilStartX,ilStartY,ilEndX,ilEndY); // for drawing bar background
    CRect rcMarker(rcBackground);     // for drawing bar marker
    CRect rcBar(rcBackground);        // for drawing frame and text
    CBrush brWhite(RGB(255, 255, 255));

	CBrush brMarkerBrush(RGB(220,220,220));

    // draw bar background and marker
    switch (ropBar.MarkerType)
    {
    case MARKNONE:
        rcBackground.right++;   // extra pixel to help FillRect() work correctly
        pomCdc->FillRect(&rcBackground, &brWhite);
        break;
    case MARKLEFT:
        rcMarker.right = (rcMarker.left + rcMarker.right) / 2;
        rcMarker.right++;       // extra pixel to help FillRect() work correctly
        pomCdc->FillRect(&rcMarker, &brMarkerBrush);
        rcBackground.left = rcMarker.right;
        rcBackground.right++;   // extra pixel to help FillRect() work correctly
        pomCdc->FillRect(&rcBackground, &brWhite);
        break;
    case MARKRIGHT:
        rcBackground.right = (rcBackground.left + rcBackground.right) / 2;
        rcBackground.right++;   // extra pixel to help FillRect() work correctly
        pomCdc->FillRect(&rcBackground, &brWhite);
        rcMarker.left = rcBackground.right;
        rcMarker.right++;       // extra pixel to help FillRect() work correctly
        pomCdc->FillRect(&rcMarker, &brMarkerBrush);
        break;
    case MARKFULL:
        rcMarker.right++;       // extra pixel to help FillRect() work correctly
        pomCdc->FillRect(&rcMarker, &brMarkerBrush);
        break;
    }

	/*
	if (isRightMarker)
	{
        pomCdc->FillRect(&opRightMarkerRect, popRightMarkerBrush);
	}
	*/
    // extra pixel to help ExtTextOut() work correctly
    rcBar.right++;

    // draw bar text (with clipping)
    CFont *pOldFont = pomCdc->SelectObject(&omSmallFont_Regular);
  //  COLORREF nOldTextColor = pomCdc->SetTextColor(lpTextColor);
    int nBkMode = pomCdc->SetBkMode(TRANSPARENT);
    UINT nTextAlign = pomCdc->SetTextAlign(TA_CENTER);
    int x = (ilStartX + ilEndX) / 2;

    pomCdc->ExtTextOut(x, ilStartY + imBarVerticalTextOffset, ETO_CLIPPED, &rcBar, ropBar.Text, lstrlen(ropBar.Text), NULL);
    pomCdc->SetTextAlign(nTextAlign);
    pomCdc->SetBkMode(nBkMode);
  //  pomCdc->SetTextColor(nOldTextColor);
    pomCdc->SelectObject(pOldFont);

    // draw frame over bar if necessary
    if (ropBar.FrameType == FRAMERECT)
    {   
		CBrush brBlack(RGB(0, 0, 0));
		for(int i = 0; i < 4; i++)
		{
			pomCdc->FrameRect(&rcBar, &brBlack);
			rcBar.top++;
			rcBar.bottom--;
			rcBar.left++;
			rcBar.right--;
		}
    }
	if (ropBar.FrameType == FRAMEBACKGROUND)
	{
		CBrush brBackground(RGB(147,147,147));
		pomCdc->FrameRect(&rcBar, &brBackground);
	}
	/* no such thing for the moment 
	// paint additional decoration into the bar
	if(ropDeco != NULL)
	{
		int ilCount = ropDeco->GetSize();
		for(int ili = 0; ili < ilCount; ili++)
		{
			BARDECO prlDeco = ropDeco->GetAt(ili);
			CBrush brFillBrush(prlDeco.Color);
			switch(prlDeco.type)
			{
			case BD_BAR:
				pomCdc->FillRect(&prlDeco.Rect, &brFillBrush);
				break;
			case BD_STRING:
				pOldFont = pomCdc->SelectObject(popTextFont);
				nOldTextColor = pomCdc->SetTextColor(prlDeco.Color);
				nBkMode = pomCdc->SetBkMode(TRANSPARENT);
				nTextAlign = pomCdc->SetTextAlign(TA_CENTER);
				x = (prlDeco.Rect.left + prlDeco.Rect.right) / 2;
				pomCdc->ExtTextOut(x, prlDeco.Rect.top + 1, ETO_CLIPPED, 
							      &prlDeco.Rect, prlDeco.Value, 
								  prlDeco.Value.GetLength(), NULL);
				pomCdc->SetTextAlign(nTextAlign);
				pomCdc->SetBkMode(nBkMode);
				pomCdc->SetTextColor(nOldTextColor);
				pomCdc->SelectObject(pOldFont);
				break;
			}
		}
		ropDeco++;
	}
	*/
	return TRUE;
}



BOOL CCSPrint::PrintBkBar(PRINTBARDATA &ropBar, int ipHeightOfThisLine)
{
	int ilStartX = GetXFromTime(ropBar.StartTime);
	int ilEndX   = GetXFromTime(ropBar.EndTime);
	int ilStartY = imYOffset;
	int ilEndY   = ilStartY + ipHeightOfThisLine;

	//PRF 8704
	if(emPrintOption == PRINT_PREVIEW)
	{
		ilEndX = ilEndX > imGanttEndX ? imGanttEndX: ilEndX;
		ilStartX = ilStartX < imGanttStartX ? imGanttStartX : ilStartX;
	}

    CRect rcBackground(ilStartX,ilStartY,ilEndX,ilEndY); // for drawing bar background
	CRect rcMarker(rcBackground);						 // for drawing bar marker
	CRect rcBar(rcBackground);							 // for drawing frame and text
	

	CBrush brWhite(RGB(255,255,255));
	CBrush brMarkerBrush(RGB(240,240,240));
	CBrush brMarkerNoneBrush(RGB(250,250,250));


	// draw bar background and marker
    switch (ropBar.MarkerType)
    {
    case MARKNONE:
        rcBackground.right++;   // extra pixel to help FillRect() work correctly
        pomCdc->FillRect(&rcBackground, &brMarkerNoneBrush);
        break;
    case MARKLEFT:
        rcMarker.right = (rcMarker.left + rcMarker.right) / 2;
        rcMarker.right++;       // extra pixel to help FillRect() work correctly
        pomCdc->FillRect(&rcMarker, &brMarkerBrush);
        rcBackground.left = rcMarker.right;
        rcBackground.right++;   // extra pixel to help FillRect() work correctly
        pomCdc->FillRect(&rcBackground, &brWhite);
        break;
    case MARKRIGHT:
        rcBackground.right = (rcBackground.left + rcBackground.right) / 2;
        rcBackground.right++;   // extra pixel to help FillRect() work correctly
        pomCdc->FillRect(&rcBackground, &brWhite);
        rcMarker.left = rcBackground.right;
        rcMarker.right++;       // extra pixel to help FillRect() work correctly
        pomCdc->FillRect(&rcMarker, &brMarkerBrush);
        break;
    case MARKFULL:
        rcMarker.right++;       // extra pixel to help FillRect() work correctly
        pomCdc->FillRect(&rcMarker, &brMarkerBrush);
        break;
	case MARKHATCHED:
        rcBackground.right++;   // extra pixel to help FillRect() work correctly
        pomCdc->FillRect(&rcBackground, &brMarkerNoneBrush);
        pomCdc->FillRect(&rcBackground, &omHatchedBrush);
    }

    // extra pixel to help ExtTextOut() work correctly
    rcBar.right++;

    // draw bar text (with clipping)
	if (ropBar.Text.GetLength())
	{
		// MCU 29.09.98 using smaller font for gantt-bar
		CFont *pOldFont = pomCdc->SelectObject(&omSmallFont_Regular);
		//CFont *pOldFont = pomCdc->SelectObject(&omSmallFont_Regular);
	  //  COLORREF nOldTextColor = pomCdc->SetTextColor(lpTextColor);
		int nBkMode = pomCdc->SetBkMode(TRANSPARENT);
		//  UINT nTextAlign = pomCdc->SetTextAlign(TA_CENTER);
		UINT nTextAlign = pomCdc->SetTextAlign(ropBar.TextAlign);
		int x ;
		if( ropBar.TextAlign==TA_LEFT)
			x = max (imGanttStartX,ilStartX ) + MMX(5);
		else
			x =  (ilStartX + ilEndX) / 2;

		pomCdc->ExtTextOut(x, ilStartY + imBarVerticalTextOffset, ETO_CLIPPED, &rcBar, ropBar.Text, lstrlen(ropBar.Text), NULL);
		pomCdc->SetTextAlign(nTextAlign);
		pomCdc->SetBkMode(nBkMode);
	  //  pomCdc->SetTextColor(nOldTextColor);
		pomCdc->SelectObject(pOldFont);
	}

    // draw frame over bar if necessary
    if (ropBar.FrameType == FRAMERECT)
    {   
		CBrush brBlack(RGB(0, 0, 0));
		for(int i = 0; i < 4; i++)
		{
			pomCdc->FrameRect(&rcBar, &brBlack);
			rcBar.top++;
			rcBar.bottom--;
			rcBar.left++;
			rcBar.right--;
		}
    }
	if (ropBar.FrameType == FRAMEBACKGROUND)
	{
		CBrush brBackground(RGB(147,147,147));
		pomCdc->FrameRect(&rcBar, &brBackground);
	}
	
	return TRUE;
}


BOOL CCSPrint::PrintText(int ipStartX,int ipStartY,int ipEndX,int ipEndY,int ipType,CString opText,BOOL ipUseOffset)
{
    CFont *polOldFont;
	int ilStartY;
	int ilEndY;

	switch ( ipType )
	{
	case PRINT_SMALLBOLD:
		polOldFont = pomCdc->SelectObject(&omSmallFont_Bold);
		break;
	case PRINT_MEDIUMBOLD:
		polOldFont = pomCdc->SelectObject(&omMediumFont_Bold);
		break;
	case PRINT_LARGEBOLD:
		polOldFont = pomCdc->SelectObject(&omLargeFont_Bold);
		break;
	case PRINT_MEDIUM:
		polOldFont = pomCdc->SelectObject(&omMediumFont_Regular);
		break;
	case PRINT_LARGE:
		polOldFont = pomCdc->SelectObject(&omLargeFont_Regular);
		break;
	case PRINT_SMALL:
	default:
		// if we find no Type, use small Font as in PRINT_SMALL
		polOldFont = pomCdc->SelectObject(&omSmallFont_Regular);
		break;
	}

	if (ipUseOffset == TRUE)
	{
			ilStartY = imYOffset + MMY(ipStartY);
		//	imYOffset += MMY(ipEndY);
			ilEndY = ilStartY + MMY(ipEndY); 
	}
	else 
	{
		if (ipStartY == 0)
		{
			ilStartY = imYOffset;
			imYOffset += MMY(ipEndY);
			ilEndY = imYOffset;
		}
		else
		{
			ilStartY = MMY(ipStartY);
			ilEndY = MMY(ipEndY);
		}
	}

	if (ipEndX == 0)
	{
		ipEndX   = imWidth-100;
	}

	CRect olRect(MMX(ipStartX),ilStartY,MMX(ipEndX),ilEndY);
	pomCdc->DrawText(opText,olRect,DT_LEFT|DT_WORDBREAK);
	pomCdc->SelectObject(polOldFont);

	return TRUE;

}

BOOL CCSPrint::LoadBitmapFromBMPFile( LPTSTR szFileName, HBITMAP *phBitmap,HPALETTE *phPalette )
{
     BITMAP  bm;

     *phBitmap = NULL;
     *phPalette = NULL;

     // Use LoadImage() to get the image loaded into a DIBSection
     *phBitmap = (HBITMAP)LoadImage( NULL, szFileName, IMAGE_BITMAP, 0, 0,
                 LR_CREATEDIBSECTION | LR_LOADFROMFILE );
     if( *phBitmap == NULL )
       return FALSE;

     // Get the color depth of the DIBSection
     GetObject(*phBitmap, sizeof(BITMAP), &bm );
     // If the DIBSection is 256 color or less, it has a color table
     if( ( bm.bmBitsPixel * bm.bmPlanes ) <= 8 )
     {
       HDC           hMemDC;
       HBITMAP       hOldBitmap;
       RGBQUAD       rgb[256];
       LPLOGPALETTE  pLogPal;
       WORD          i;

       // Create a memory DC and select the DIBSection into it
       hMemDC = CreateCompatibleDC( NULL );
       hOldBitmap = (HBITMAP)SelectObject( hMemDC, *phBitmap );
       // Get the DIBSection's color table
       GetDIBColorTable( hMemDC, 0, 256, rgb );
       // Create a palette from the color table
       pLogPal = (LOGPALETTE*)malloc( sizeof(LOGPALETTE) + (256*sizeof(PALETTEENTRY)) );
       pLogPal->palVersion = 0x300;
       pLogPal->palNumEntries = 256;
       for(i=0;i<256;i++)
       {
         pLogPal->palPalEntry[i].peRed = rgb[i].rgbRed;
         pLogPal->palPalEntry[i].peGreen = rgb[i].rgbGreen;
         pLogPal->palPalEntry[i].peBlue = rgb[i].rgbBlue;
         pLogPal->palPalEntry[i].peFlags = 0;
       }
       *phPalette = CreatePalette( pLogPal );
       // Clean up
       free( pLogPal );
       SelectObject( hMemDC, hOldBitmap );
       DeleteDC( hMemDC );
     }
     else   // It has no color table, so use a halftone palette
     {
       HDC    hRefDC;

       hRefDC = GetDC( NULL );
       *phPalette = CreateHalftonePalette( hRefDC );
       ReleaseDC( NULL, hRefDC );
     }
     return TRUE;
}

void CCSPrint::InitializeDevicePointer()
{		
	if(emPrintOption == PRINT_PRINTER)
	{		
		pomCdc = &omCdc;
	}
}