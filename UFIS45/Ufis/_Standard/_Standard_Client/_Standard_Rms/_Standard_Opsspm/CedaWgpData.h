// Employee Groups (Teams)
#ifndef _CEDAWGPDATA_H_
#define _CEDAWGPDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct WgpDataStruct
{
	long	Urno;		// Unique Record Number
	char	Wgpc[6];	// Group Code
	char	Wgpn[41];	// Group Description
	long	Pgpu;		// Planning Group URNO (PGPTAB)
};

typedef struct WgpDataStruct WGPDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaWgpData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <WGPDATA> omData;
	CMapStringToPtr omWgpcMap;
	CString GetTableName(void);

// Operations
public:
	CedaWgpData();
	~CedaWgpData();

	CCSReturnCode ReadWgpData();
	void GetAllTeams(CStringArray &ropTeams);
	WGPDATA* GetWgpByWgpc(const char *pcpWgpc);

private:
	void AddWgpInternal(WGPDATA &rrpWgp);
	void ClearAll();
};


extern CedaWgpData ogWgpData;
#endif _CEDAWGPDATA_H_
