// FlightScheduleViewer.h
//
#ifndef __FLVIEWER_H__
#define __FLVIEWER_H__

#include <CCSPtrArray.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <cviewer.h>
#include <table.h>
#include <ccsprint.h>
#include <FlightDetailWindow.h>

struct FLIGHTSCHEDULE_LINEDATA
{
	CString		Regn;
	CString		Actl;
	CString		Alcd;

	CString		Fnuma;
	CString		Rou1a;
	CString		Rou2a;
	CTime		Stoa;
	CTime		Etoa;
	CTime		Tmot;
	CTime       Land;
	CTime       Onbl;
	CString		Posia;
	CString		Alida;

	int			ArrReturnFlightType;
	int			DepReturnFlightType;

	CString		Fnumd;
	CString		Rou1d;
	CString		Rou2d;
	CTime		Stod;
	CTime		Etod;
	CTime		Ofbl;
	CTime	    Airb;
	CString		Posid;
	CString		Alidd;
	CString		VaArr;		// Nature Arrival
	CString		VaDep;		// Nature Departure
	CString		Blt1;		// Baggage belt 1
	CString		Cki;		// Checkin counter range eg "321-323"
	CString		Pax;
	long		DepUrno;
	long		ArrUrno;

	FLIGHTSCHEDULE_LINEDATA(void)
	{
		ArrReturnFlightType = ID_NOT_RETURNFLIGHT;
		DepReturnFlightType = ID_NOT_RETURNFLIGHT;
	};
};

enum ogIofl{IS_ARRIVAL,IS_DEPARTURE,IS_ROTATION};

class FlightScheduleViewer: public CViewer 
{
// Constructions
public:
    FlightScheduleViewer();
    ~FlightScheduleViewer();

     //QDO
	//void DeleteLine(int ipLineno);
   // void DeleteAll();
	
	void Attach(CTable *popAttachWnd);
    void ChangeViewTo(const char *pcpViewName);
	void SetIsFromSearchMode(BOOL bpIsFromSearch);
	void AllowUpdates(BOOL bpNoUpdatesNow);

	BOOL bmNoUpdatesNow;

	void DeleteLine(int ipLineno);

	CString Format(FLIGHTSCHEDULE_LINEDATA *prpLine);

// Internal data processing routines
private:
    void PrepareFilter();
	void PrepareSorter();
	void PrepareGrouping();
	BOOL IsSameGroup(FLIGHTSCHEDULE_LINEDATA *prpFlight1,
	FLIGHTSCHEDULE_LINEDATA *prpFlight2);

    void MakeLines();
	void MakeLine(FLIGHTDATA *prpFlight1, bool bpUpdateTable = false);
	void MakeLine(FLIGHTDATA  *prpFlightArr, FLIGHTDATA  *prpFlightDep, bool bpUpdateTable = false);
	BOOL IsPassFilter(FLIGHTDATA *prpFlightArr, FLIGHTDATA *prpFlightDep);
	int CreateLine(FLIGHTSCHEDULE_LINEDATA *prpFlight);
	int CompareFlight(FLIGHTSCHEDULE_LINEDATA *prpFlight1, 
					  FLIGHTSCHEDULE_LINEDATA *prpFlight2);

	void DeleteAll();
	//void DeleteLine(int ipLineno);

private:
	void UpdateDisplay();
	//CString Format(FLIGHTSCHEDULE_LINEDATA *prpLine);


// Attributes used for filtering condition
private:
    int omGroupBy;              // enumerated value -- define in "flviewer.cpp" (use first sorting)
    CWordArray omSortOrder;     // array of enumerated value -- defined in "flviewer.cpp"
	CMapStringToPtr omCMapForGata;		// Gate of arrival flight
	CMapStringToPtr omCMapForGatd;		// Gate of departure flight
	CMapStringToPtr omCMapForPsta;		// Position of arrival flight
	CMapStringToPtr omCMapForPstd;		// Position of departure flight
    CMapStringToPtr omCMapForAlcd;		// Aircraft type
    CString omDate;
	CString m_StartDay;

// Attributes
public:
    CTable *pomTable;
    CCSPtrArray<FLIGHTSCHEDULE_LINEDATA> omLines;
	BOOL bmDisplayOutbound;
	BOOL bmDisplayInbound;
	BOOL bmDisplayPax;
	BOOL bmOutboundOnly;
	BOOL bmInboundOnly;
	BOOL bmIsFromSearch;
	CTime omStartTime;
	CTime omEndTime;

// Methods which handle changes (from Data Distributor)
public:
	void ProcessFlightChange(FLIGHTDATA *prpFlight);
	void ProcessFlightDelete(long lpFlightUrno);
	void ProcessFlightSelect(FLIGHTDATA *prpFlight);
	BOOL FindInboundFlight(long lpFlightUrno,int& ilItem);
	BOOL FindOutboundFlight(long lpFlightUrno,int& ilItem);
	int FindFlightLine(long lpFlightUrno);
	bool bmUseAllAirlines, bmUseAllArrGates, bmUseAllDepGates, bmUseAllArrPositions, bmUseAllDepPositions;
	CMapPtrToPtr omArrMap, omDepMap;

	//inline int Lines() const {return omLines.GetSize();} //Singapore
	//inline void SetTerminal(const CString& ropTerminal){omTerminal = ropTerminal;} //Singapore
	//inline CString GetTerminal() const {return omTerminal;} //Singapore
//    inline void GetLines(CCSPtrArray<STAFFTABLE_LINEDATA>& opLines) const{opLines = omLines;} //Singapore
	CCSPtrArray<FLIGHTSCHEDULE_LINEDATA>& GetLines(){return omLines;} //Singapore
	CCSPrint*& GetCCSPrinter() {return pomPrint;}


////////////////////////////////////////////////////////////////////////////////////////////
// Printing functions

private:
	BOOL PrintFlightScheduleLine(FLIGHTSCHEDULE_LINEDATA *prpLine,BOOL bpIsLastLine);
	BOOL PrintFlightScheduleLineInbound(FLIGHTSCHEDULE_LINEDATA *prpLine,BOOL bpIsLastLine);
	BOOL PrintFlightScheduleLineOutbound(FLIGHTSCHEDULE_LINEDATA *prpLine,BOOL bpIsLastLine);
	BOOL PrintFlightScheduleLineTurnaround(FLIGHTSCHEDULE_LINEDATA *prpLine,BOOL bpIsLastLine);
	BOOL PrintFlightScheduleHeader(CCSPrint *pomPrint);
	BOOL PrintPreviewFlightScheduleHeader(CCSPrint *pomPrint);
	CCSPrint *pomPrint;
public:
	void PrintView();

	void PrintPreview(const int& ripPageNo); //Singapore
};

#endif //__FLVIEWER_H__
