// PstDiagramPropSheet.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaHagData.h>
#include <CedaAltData.h>
#include <CedaNatData.h>
#include <CedaActData.h>
#include <CedaPfcData.h>
#include <Conflict.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <BasePropertySheet.h>
#include <PstDiagramPropSheet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PstDiagramPropertySheet
//

//PstDiagramPropertySheet::PstDiagramPropertySheet(CWnd* pParentWnd,
//	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
//	(ID_SHEET_PST_DIAGRAM, pParentWnd, popViewer, iSelectPage)
PstDiagramPropertySheet::PstDiagramPropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_STRING61933), pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_pagePstArea);
	AddPage(&m_pageAirline);
	if (ogBasicData.HandlingAgentsSupported())
		AddPage(&m_pageAgents);
	if(ogBasicData.bmDisplayEquipment)
		AddPage(&m_pageDemandTypes);
	AddPage(&m_pageArrDep);
	AddPage(&m_pageKeycodes);
	AddPage(&m_pageNatures);
	AddPage(&m_pageAct);
	AddPage(&m_pageFunctions); //PRF 8999
	AddPage(&m_pageEquDemandGroups);
//	AddPage(&m_pageConflicts);
//	AddPage(&m_pageSetup);

	// Change the caption in tab control of the PropertySheet
	//m_pagePstArea.SetCaption(ID_PAGE_FILTER_PSTAREA);
	m_pagePstArea.SetCaption(GetString(IDS_STRING61932));

	//m_pageAirline.SetCaption(ID_PAGE_FILTER_AIRLINE);
	m_pageAirline.SetCaption(GetString(IDS_STRING61597));

	m_pageAgents.SetCaption(GetString(IDS_STRING61954));

	m_pageDemandTypes.SetCaption(GetString(IDS_DEMANDTYPESVIEW));

	m_pageKeycodes.SetCaption(GetString(IDS_KEYCODESVIEW));

	m_pageNatures.SetCaption(GetString(IDS_STRING632));

	m_pageAct.SetCaption(GetString(IDS_AIRCRAFTTYPE));
//	m_pageConflicts.SetCaption(GetString(IDS_PST_CONFLICTS));

	m_pageFunctions.SetCaption(GetString(IDS_FUNCTION_CODE)); ///PRF 8999
	m_pageEquDemandGroups.SetCaption(GetString(IDS_EQU_DEM_GROUPS));

	// Prepare possible values for each PropertyPage
	CCSPtrArray <ALLOCUNIT> olPstAreas;
	ogAllocData.GetGroupsByGroupType(ALLOCUNITTYPE_PSTGROUP,olPstAreas);
	int ilNumPstAreas = olPstAreas.GetSize();
	for(int ilPstArea = 0; ilPstArea < ilNumPstAreas; ilPstArea++)
	{
		ALLOCUNIT *prlPstArea = &olPstAreas[ilPstArea];
		m_pagePstArea.omPossibleItems.Add(prlPstArea->Name);
	}
	m_pagePstArea.omPossibleItems.Add(GetString(IDS_NO_POSITION));
	m_pagePstArea.omPossibleItems.Add(GetString(IDS_ALL_FLIGHTS));
	m_pagePstArea.bmSelectAllEnabled = true;

	// airline codes format ALC2/ALC3
	ogAltData.GetAlc2Alc3Strings(m_pageAirline.omPossibleItems);
	m_pageAirline.omPossibleItems.Add(GetString(IDS_NO_AIRLINE));
	m_pageAirline.bmSelectAllEnabled = true;

	ogNatData.GetAllNatures(m_pageNatures.omPossibleItems);
	m_pageNatures.omPossibleItems.Add(GetString(IDS_NONATURE));
	m_pageNatures.bmSelectAllEnabled = true;

	ogActData.GetFormattedActNames(m_pageAct.omPossibleItems);
	m_pageAct.omPossibleItems.Add(GetString(IDS_NOACT));
	m_pageAct.bmSelectAllEnabled = true;

	ogPfcData.GetAllFunctions(m_pageFunctions.omPossibleItems);
	m_pageFunctions.bmSelectAllEnabled = true;

	FillEquipmentDemandGroups();
	m_pageEquDemandGroups.bmSelectAllEnabled = true;

	// handling agents
	CStringArray rolNames;
	int n = ogHagData.GetNameList(rolNames);
	for (int i = 0; i < n; i++)
	{
		m_pageAgents.omPossibleItems.Add(rolNames[i]);
	}
	m_pageAgents.omPossibleItems.Add(GetString(IDS_NOAGENT));
	m_pageAgents.bmSelectAllEnabled = true;

	// key codes
	CStringArray olKeycodes;
	ogFlightData.GetKeycodes(m_pageKeycodes.omPossibleItems);
	m_pageKeycodes.bmSelectAllEnabled = true;

	m_pageDemandTypes.omPossibleItems.Add(GetString(IDS_PERSONNEL_DEM));
	m_pageDemandTypes.omPossibleItems.Add(GetString(IDS_EQUIPMENT_DEM));
//	m_pageDemandTypes.omPossibleItems.Add(GetString(IDS_LOCATION_DEM));
	m_pageDemandTypes.bmSelectAllEnabled = true;

//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_OVERLAPPING));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_NOTCONFIRMED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_NOTENDED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_NOTINPOOL));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_INVALIDJOBDEMAND));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_SHIFTNOTSTARTED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_EMPLOYEEABSENT));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_EQU_UNAVAILABLE));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_DELAYED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_CANCELLED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_DIVERTED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_EQUIPMENTCHANGE));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_OVERBOOKED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_POSITIONCHANGE));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_AIRCRAFTCHANGE));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_PST_NODEMAND));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_PST_NOTCOVERED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_PST_VLOWCOVERED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_PST_LOWCOVERED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_PST_OVERCOVERED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_FIRST_OVERBOOKED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_BUSINESS_OVERBOOKED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_ECONOMY_OVERBOOKED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_NEXTINFOTIME));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_EQU_NODEMAND));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_EQU_NOTCOVERED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_EQU_VLOWCOVERED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_EQU_LOWCOVERED));
//	m_pageConflicts.omPossibleItems.Add(ogConflicts.GetConflictTextByType(CFI_EQU_OVERCOVERED));
//	m_pageConflicts.bmSelectAllEnabled = true;
}

void PstDiagramPropertySheet::LoadDataFromViewer()
{
	if(!pomViewer->GetFilter("Pstbereich", m_pagePstArea.omSelectedItems))
	{
		m_pagePstArea.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("Airline", m_pageAirline.omSelectedItems))
	{
		m_pageAirline.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("Natures", m_pageNatures.omSelectedItems))
	{
		m_pageNatures.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("Act", m_pageAct.omSelectedItems))
	{
		m_pageAct.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	CStringArray olShortNames;
	if(pomViewer->GetFilter("Agents",olShortNames))
	{
		m_pageAgents.omSelectedItems.RemoveAll();
		ogHagData.GetNameList(olShortNames,m_pageAgents.omSelectedItems);
	}
	else
	{
		m_pageAgents.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("DemandType",	m_pageDemandTypes.omSelectedItems))
	{
		m_pageDemandTypes.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("Keycodes",	m_pageKeycodes.omSelectedItems))
	{
		m_pageKeycodes.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

//	if(!pomViewer->GetFilter("Conflicts", m_pageConflicts.omSelectedItems))
//	{
//		m_pageConflicts.omSelectedItems.Add(GetString(IDS_ALLSTRING));
//	}

	if(!pomViewer->GetFilter("Functions", m_pageFunctions.omSelectedItems))
	{
		m_pageFunctions.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}
	if(!pomViewer->GetFilter("EquDemandGroups", m_pageEquDemandGroups.omSelectedItems))
	{
		m_pageEquDemandGroups.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}



	CString olVal;

	olVal = pomViewer->GetUserData("STOA");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmStoaColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmStoaColour = SILVER;
	}

	olVal = pomViewer->GetUserData("ETOA");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmEtoaColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmEtoaColour = WHITE;
	}

	olVal = pomViewer->GetUserData("TMOA");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmTmoaColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmTmoaColour = YELLOW;
	}

	olVal = pomViewer->GetUserData("LAND");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmLandColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmLandColour = GREEN;
	}

	olVal = pomViewer->GetUserData("ONBL");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmOnblColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmOnblColour = LIME;
	}

	olVal = pomViewer->GetUserData("STOD");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmStodColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmStodColour = SILVER;
	}

	olVal = pomViewer->GetUserData("ETOD");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmEtodColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmEtodColour = WHITE;
	}

	olVal = pomViewer->GetUserData("SLOT");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmSlotColour= atol(olVal);
	}
	else
	{
		m_pageArrDep.lmSlotColour = YELLOW;
	}

	olVal = pomViewer->GetUserData("OFBL");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmOfblColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmOfblColour = LIME;
	}

	olVal = pomViewer->GetUserData("AIRB");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.lmAirbColour = atol(olVal);
	}
	else
	{
		m_pageArrDep.lmAirbColour = GREEN;
	}

	m_pageArrDep.SetColours();

	m_pageArrDep.m_DisplayJobConflicts = (pomViewer->GetUserData("DISPLAYJOBCONFLICTS") == "YES") ? TRUE : FALSE;
	m_pageArrDep.m_OneFlightPerLine = (pomViewer->GetUserData("ONEFLIGHTPERLINE") == "YES") ? TRUE : FALSE;


	olVal = pomViewer->GetUserData("ARRIVAL");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.bmArrCheckBox = (olVal == "YES") ? TRUE : FALSE;
	}
	else
	{
		m_pageArrDep.bmArrCheckBox = (ogBasicData.omDepArrMarkerPst.Find("ARRIVAL") != -1) ? TRUE : FALSE;
	}

	olVal = pomViewer->GetUserData("DEPARTURE");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.bmDepCheckBox = (olVal == "YES") ? TRUE : FALSE;
	}
	else
	{
		m_pageArrDep.bmDepCheckBox = (ogBasicData.omDepArrMarkerPst.Find("DEPARTURE") != -1) ? TRUE : FALSE;
	}

	olVal = pomViewer->GetUserData("TURNAROUND");
	if(!olVal.IsEmpty())
	{
		m_pageArrDep.bmTurnaroundCheckBox = (olVal == "YES") ? TRUE : FALSE;
	}
	else
	{
		m_pageArrDep.bmTurnaroundCheckBox = (ogBasicData.omDepArrMarkerPst.Find("TURNAROUND") != -1) ? TRUE : FALSE;
	}
}

void PstDiagramPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("Pstbereich",	m_pagePstArea.omSelectedItems);
	pomViewer->SetFilter("Airline",		m_pageAirline.omSelectedItems);
	pomViewer->SetFilter("Natures",		m_pageNatures.omSelectedItems);
	pomViewer->SetFilter("Act",			m_pageAct.omSelectedItems);

	CStringArray olShortNames;
	ogHagData.GetShortNameList(m_pageAgents.omSelectedItems,olShortNames);
	pomViewer->SetFilter("Agents",		olShortNames);

	pomViewer->SetFilter("DemandType",	m_pageDemandTypes.omSelectedItems);
	pomViewer->SetFilter("Keycodes",	m_pageKeycodes.omSelectedItems);

	pomViewer->SetFilter("Functions",   m_pageFunctions.omSelectedItems);
	pomViewer->SetFilter("EquDemandGroups", m_pageEquDemandGroups.omSelectedItems);

//	pomViewer->SetFilter("Conflicts", m_pageConflicts.omSelectedItems);

	CString olColour;
	olColour.Format("%ld",m_pageArrDep.lmStoaColour);	pomViewer->SetUserData("STOA",olColour);
	olColour.Format("%ld",m_pageArrDep.lmEtoaColour);	pomViewer->SetUserData("ETOA",olColour);
	olColour.Format("%ld",m_pageArrDep.lmTmoaColour);	pomViewer->SetUserData("TMOA",olColour);
	olColour.Format("%ld",m_pageArrDep.lmLandColour);	pomViewer->SetUserData("LAND",olColour);
	olColour.Format("%ld",m_pageArrDep.lmOnblColour);	pomViewer->SetUserData("ONBL",olColour);
	olColour.Format("%ld",m_pageArrDep.lmStodColour);	pomViewer->SetUserData("STOD",olColour);
	olColour.Format("%ld",m_pageArrDep.lmEtodColour);	pomViewer->SetUserData("ETOD",olColour);
	olColour.Format("%ld",m_pageArrDep.lmSlotColour);	pomViewer->SetUserData("SLOT",olColour);
	olColour.Format("%ld",m_pageArrDep.lmOfblColour);	pomViewer->SetUserData("OFBL",olColour);
	olColour.Format("%ld",m_pageArrDep.lmAirbColour);	pomViewer->SetUserData("AIRB",olColour);

	pomViewer->SetUserData("DISPLAYJOBCONFLICTS",m_pageArrDep.m_DisplayJobConflicts ? "YES" : "NO");
	pomViewer->SetUserData("ONEFLIGHTPERLINE",m_pageArrDep.m_OneFlightPerLine ? "YES" : "NO");
	pomViewer->SetUserData("ARRIVAL",m_pageArrDep.bmArrCheckBox ? "YES" : "NO");
	pomViewer->SetUserData("DEPARTURE",m_pageArrDep.bmDepCheckBox ? "YES" : "NO");
	pomViewer->SetUserData("TURNAROUND",m_pageArrDep.bmTurnaroundCheckBox ? "YES" : "NO");

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int PstDiagramPropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedPstAreas;
	CStringArray olSelectedAirlines;
	CStringArray olSelectedNatures;
	CStringArray olSelectedAct;
	CStringArray olSelectedAgents;
	CStringArray olSelectedDemandTypes;
	CStringArray olSelectedKeycodes;
	CStringArray olSelectedConflicts;
	CStringArray olSelectedFunctions; //PRF 8999
	CStringArray olSelectedEquDemandGroups;
	
	pomViewer->GetFilter("Pstbereich", olSelectedPstAreas);
	pomViewer->GetFilter("Airline",		olSelectedAirlines);
	pomViewer->GetFilter("Natures",		olSelectedNatures);
	pomViewer->GetFilter("Act",		olSelectedAct);
	pomViewer->GetFilter("Agents",		olSelectedAgents);
	pomViewer->GetFilter("DemandType",		olSelectedDemandTypes);
	pomViewer->GetFilter("Keycodes",		olSelectedKeycodes);
	pomViewer->GetFilter("Functions", olSelectedFunctions); //PRF 8999
	pomViewer->GetFilter("EquDemandGroups", olSelectedEquDemandGroups);
//	pomViewer->GetFilter("Conflicts", olSelectedConflicts);
	
	CStringArray olShortNames;
	ogHagData.GetShortNameList(m_pageAgents.omSelectedItems,olShortNames);

	if (!IsIdentical(olSelectedPstAreas, m_pagePstArea.omSelectedItems) ||
		!IsIdentical(olSelectedAirlines, m_pageAirline.omSelectedItems) ||
		!IsIdentical(olSelectedNatures, m_pageNatures.omSelectedItems) ||
		!IsIdentical(olSelectedAct, m_pageAct.omSelectedItems) ||
		!IsIdentical(olSelectedDemandTypes, m_pageDemandTypes.omSelectedItems) ||
		!IsIdentical(olSelectedKeycodes, m_pageKeycodes.omSelectedItems) ||
//		!IsIdentical(olSelectedConflicts, m_pageConflicts.omSelectedItems) ||
		!IsIdentical(olSelectedAgents, olShortNames) ||
		!IsIdentical(olSelectedFunctions, m_pageFunctions.omSelectedItems) /*PRF 8999*/ ||
		!IsIdentical(olSelectedEquDemandGroups, m_pageFunctions.omSelectedItems))
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}

void PstDiagramPropertySheet::FillEquipmentDemandGroups()
{
	char chWhere[200];
	CDWordArray wordTplUrnos;
	CCSPtrArray<SGRDATA> olSgrData;
	ogBasicData.GetTemplateFilters(wordTplUrnos);
	CString olUrnos;
	CString olUrno;	
	for(int i = 0 ; i < wordTplUrnos.GetSize(); i++)
	{
		olUrno.Format("'%ld'",wordTplUrnos.GetAt(i));
		olUrnos += olUrno;
		olUrnos += i < (wordTplUrnos.GetSize() - 1) ? "," : "";
	}
	sprintf(chWhere,"WHERE UTPL IN(%s) AND TABN='EQU'",olUrnos);
	if(ogSgrData.ReadSpecialData(&olSgrData,chWhere,"URNO,GRPN",false) == true)
	{
		int ilNumSgr = olSgrData.GetSize();
		for(i = 0 ; i < ilNumSgr; i++)
		{
			SGRDATA* polSgrData = &olSgrData[i];
			if(polSgrData != NULL)
			{
				m_pageEquDemandGroups.omPossibleItems.Add(polSgrData->Grpn);
			}
		}
	}
}