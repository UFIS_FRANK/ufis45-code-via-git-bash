//
// CedaPerData.h - Permit Stammdaten (aka Qualifications)
//
#ifndef _CEDAPERDATA_H_
#define _CEDAPERDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct PerDataStruct
{
	long	Urno;
	char	Prmc[21];	// Permit Code
	char	Prmn[41];	// Description
};

typedef struct PerDataStruct PERDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaPerData: public CCSCedaData
{
public:
    CCSPtrArray <PERDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);
	CedaPerData();
	~CedaPerData();
	void ProcessPerBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool ReadPerData();
	PERDATA *GetPerByUrno(long lpUrno);
	void GetAllPermits(CStringArray &ropPermits);

private:
	PERDATA *AddPerInternal(PERDATA &rrpPer);
	void DeletePerInternal(long lpUrno);
	void ClearAll();
};


extern CedaPerData ogPerData;
#endif _CEDAPERDATA_H_
