// GateViewer.h : header file
//

#ifndef _GAVIEWER_H_
#define _GAVIEWER_H_

#include <cviewer.h>
#include <ccsprint.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>


struct GATE_INDICATORDATA
{
	long DemandUrno;
	long JobUrno;
	// standard data for indicators in general GanttBar
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
	BOOL IsArrival;

	GATE_INDICATORDATA(void)
	{
		DemandUrno = -1;
		JobUrno	   = -1;
		IsArrival  = FALSE;
		Color	   = 0;
	}
};



struct GATE_BARDATA 
{
	int  Type;
	long FlightUrno;		// from FLTFRA.URNO
	int ReturnFlightType;
	long DepartureUrno;
	int DepReturnFlightType;
	bool IsFinished;
	CString StatusText;		// long text used to be displayed at the status bar
    // standard data for bars in general GanttLine
    CString Text;
    CTime StartTime;
    CTime EndTime;
	CTimeSpan Delay;
	int IsDelayed;
    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
	COLORREF FrameColor;
	BOOL IsShadowBar;
	BOOL IsTmo;
	BOOL NoDropOnThisBar;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	CCSPtrArray<GATE_INDICATORDATA> Indicators;
	int FlightType;

	GATE_BARDATA(void)
	{
		FrameColor      = BLACK;
		Type			= -1;
		FlightUrno		= -1;
		DepartureUrno	= -1;
		IsDelayed		= -1;
		FrameType		= -1;
		MarkerType		= -1;
		MarkerBrush		= NULL;
		IsShadowBar		= FALSE;
		IsTmo			= FALSE;
		NoDropOnThisBar	= FALSE;
		OverlapLevel	= 0;
		ReturnFlightType = ID_NOT_RETURNFLIGHT;
		DepReturnFlightType = ID_NOT_RETURNFLIGHT;
		IsFinished = false;
	}
};

struct GATE_LINEDATA 
{
	// standard data for lines in general Diagram's chart
    CString Text;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
	BOOL IsStairCaseGate;
    CCSPtrArray<GATE_BARDATA> Bars;		// in order of painting (leftmost is the bottommost)
    CCSPtrArray<int> TimeOrder;			// maintain overlapped bars, see below

	GATE_LINEDATA(void)
	{
		MaxOverlapLevel = 0;
	}
};
struct GATE_MANAGERDATA
{
	long JobUrno;			// from JOBCKI.URNO (of the job for FM/AM)
	// these data are for displaying the TopScaleText for each group in GateDiagram
	CString ShiftStid;
	CString ShiftTmid;
	CString EmpLnam;

	GATE_MANAGERDATA(void)
	{
		JobUrno = -1;
	}
};

struct GATE_GROUPDATA 
{
	CString GateAreaId;			// Gate area's ID associated with this group
    CCSPtrArray<GATE_MANAGERDATA> Managers;
	// standard data for groups in general Diagram
    CString Text;
    CCSPtrArray<GATE_LINEDATA> Lines;
};

struct GATE_SELECTION
{
	int imGroupno;
	int imLineno;
	int imBarno;

	GATE_SELECTION(void)
	{
		imGroupno = -1;
		imLineno  = -1;
		imBarno	  = -1;
	}
};

enum enumBarType {BAR_FLIGHT, BAR_SPECIAL};
enum enumGateFlightType {GATEBAR_ARRIVAL, GATEBAR_DEPARTURE, GATEBAR_TURNAROUND};

// Attention:
// Element "TimeOrder" in the struct "GATE_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array GATE_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the GATE_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array GATE_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// GateDiagramViewer window

class GateGantt;

class GateDiagramViewer: public CViewer
{
	friend GateGantt;

// Constructions
public:
    GateDiagramViewer();
    ~GateDiagramViewer();

	void Attach(CWnd *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName,
		BOOL bpIncludeArrivalFlights, BOOL bpIncludeDepartureFlights,
		CTime opStartTime, CTime opEndTime);
	void ChangeViewTo(void);
	void UpdateManagers(CTime opStartTime, CTime opEndTime);
// Internal data processing routines
private:
	static void GateDiagramCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void PrepareFilter();
	BOOL IsPassFilter(const char *pcpGateArea);
	BOOL IsPassFilter(const char *pcpGateArea, FLIGHTDATA *prpFlight);
	int CompareGroup(GATE_GROUPDATA *prpGroup1, GATE_GROUPDATA *prpGroup2);
	int CompareLine(GATE_LINEDATA *prpLine1, GATE_LINEDATA *prpLine2);
	int CompareManager(GATE_MANAGERDATA *prpManager1, GATE_MANAGERDATA *prpManager2);

	void MakeGroupsAndLines();
	void MakeManagers();
	void MakeBars();
	void MakeShadowBar(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight);
	void MakeShadowBarWithoutDemand(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight);
	void MakeManager(int ipGroupno, JOBDATA *prpJob);
	void MakeBar(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight, bool bpIgnoreGate = false, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	void MakeBarWithoutDemand(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	void MakeSpecialBar(int ipGroupno, int ipLineno, JOBDATA *prpJob);
	void MakeIndicators(int ipGroupno, int ipLineno, int ipBarno, 
		CCSPtrArray<DEMANDDATA> &ropArrivalDemands,
		CCSPtrArray<DEMANDDATA> &ropDepartureDemands,
		FLIGHTDATA *prpArrivalFlight,FLIGHTDATA *prpDepartureFlight);

	int GetReturnFlightTypes(FLIGHTDATA *prpFlight, CUIntArray &ropReturnFlightTypes, CString opAlid = "");

	int GetDemands(long lpUrno, const char *pcpAlid, CCSPtrArray<DEMANDDATA> &ropDemands,
		CTime &rolStartTime, CTime &rolEndTime);

	CString ArrivalFlightStatus(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotationFlight);
	CString DepartureFlightStatus(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotationFlight);
	CString TurnAroundFlightStatus(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotationFlight);

	BOOL FindGroup(const char *pcpGateAreaId, int &ripGroupno);
	BOOL FindGroupAndLine(const char *pcpGateId, int &ripGroupno, int &ripLineno);
	BOOL FindManager(long lpUrno, int &ripGroupno, int &ripManagerno);
	BOOL FindFlightBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno, bool bpFindRotation = false, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	BOOL FindLine(const char *pcpGateId, int ipGroupno, int &ripLineno);

// Operations
public:
	void CheckForFinishedFlights();
    int GetGroupCount();
    GATE_GROUPDATA *GetGroup(int ipGroupno);
    CString GetGroupText(int ipGroupno);
    CString GetGroupTopScaleText(int ipGroupno);
	int GetManagerCount(int ipGroupno);
    GATE_MANAGERDATA *GetManager(int ipGroupno, int ipManagerno);
    int GetLineCount(int ipGroupno);
    GATE_LINEDATA *GetLine(int ipGroupno, int ipLineno);
    CString GetLineText(int ipGroupno, int ipLineno);
    int GetMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetBarCount(int ipGroupno, int ipLineno);
    GATE_BARDATA *GetBar(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBarText(int ipGroupno, int ipLineno, int ipBarno);
	CString GetBarText(FLIGHTDATA *prpFlight, bool bpIsArrival, FLIGHTDATA *prpRotation = NULL);//wny
	CString GetStatusText(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotation);//wny
	CString GetGateText(FLIGHTDATA *prpFlight);//wny
	CString GetPositionText(FLIGHTDATA *prpFlight);//wny
	int GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno);
	GATE_INDICATORDATA *GetIndicator(int ipGroupno, int ipLineno, int ipBarno,int ipIndicatorno);
	CString GetTmo(int ipGroupno, int ipLineno);
	int GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd);
	void AllowUpdates(BOOL bpNoUpdatesNow);
	BOOL bmNoUpdatesNow;
    void DeleteAll();
    int CreateGroup(GATE_GROUPDATA *prpGroup);
    void DeleteGroup(int ipGroupno);
	CCSReturnCode CreateManagersForGroup(int ipGroupNo);
	int CreateManager(int ipGroupno, GATE_MANAGERDATA *prpManager);
	void DeleteManager(int ipGroupno, int ipManagerno);
    int CreateLine(int ipGroupno, GATE_LINEDATA *prpLine);
    void DeleteLine(int ipGroupno, int ipLineno);
    int CreateBar(int ipGroupno, int ipLineno, GATE_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
	void DeleteBarsForFlight(long lpFlightUrno);
    void DeleteBar(int ipGroupno, int ipLineno, int ipBarno);
    int CreateIndicator(int ipGroupno, int ipLineno, int ipBarno,
    	GATE_INDICATORDATA *prpIndicator);
	void DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);

    int GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
        int ipOverlapLevel1, int ipOverlapLevel2);

	COLORREF GetColourForDepFlight(long lpFlightUrno);
	COLORREF GetColourForArrFlight(long lpFlightUrno);
	bool DisplayMarker(CString opMarkerType);
	void GetTerminalList(CStringList& ropTerminalList); //Singapore

	CWnd *pomAttachWnd;

	static const CString omTerminal1;
	static const CString omTerminal2;
	static const CString omTerminal3;
	static const CString omTerminal4;
	static const CString omTerminalB;	

// Private helper routines
private:
	void GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
		CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);

// Attributes used for filtering condition
private:
	CMapStringToPtr omCMapForGateArea;
	CMapStringToPtr	omCMapForAlcd;
	CMapStringToPtr	omCMapForAgents;
	BOOL bmUseAllAgents, bmUseAllGateAreas, bmUseAllAlcds;

	BOOL bmIncludeArrivalFlights;
	BOOL bmIncludeDepartureFlights;
	CTime omStartTime;
	CTime omEndTime;

// Attributes
private:
	CCSPtrArray<GATE_GROUPDATA> omGroups;

// Methods which handle changes (from Data Distributor)
private:
	void ProcessDemandChange(DEMANDDATA *prpDemand);
	void ProcessJobChange(JOBDATA *prpJob);
	void ProcessFlightChange(FLIGHTDATA *prpFlight, bool bpCheckRotation = true);
	void ProcessFlightDelete(long lpFlightUrno);
	void ProcessFlightSelect(FLIGHTDATA *prpFlight);
	void ProcessSpecialJobNew(JOBDATA * prpJob);
	void ProcessSpecialJobChange(JOBDATA * prpJob);
	void ProcessSpecialJobDelete(JOBDATA * prpJob);
	void ProcessFmgJobNew(JOBDATA *prpJob);
	void ProcessFmgJobDelete(JOBDATA *prpJob);

	int imFlightsWithoutGatesGroup;
	int CreateLinesForFlightsWithoutGates(FLIGHTDATA *prpSingleFlight = NULL);
	bool IsGroupWithoutGates(int ipGroupno);
	int GetGroupWithoutGates();
	int SortLineOfGroupWithoutGates(int ipLineNo, long lpFlightUrno = 0L);

	int imAllFlightsGroup;
	int CreateLinesForAllFlights(FLIGHTDATA *prpSingleFlight = NULL);
	int SortLineOfGroupAllFlights(int ipLineNo, long lpFlightUrno = 0L);
	int GetAllFlightsGroup();
	bool IsAllFlightsGroup(int ipGroupno);

	int CreateGeneralGroupLines(CCSPtrArray <FLIGHTDATA> &ropFlights, int ipGroupno);
	
	void CreateTerminalList(); //Singapore
	void CreateTerminalGroups(); //Singapore
	int SortLineOfGroupFlights(int ipGroupNo,int ipLineNo, long lpFlightUrno = 0L); //Singapore
	int CreateLinesTerminalFlights(int ipGroupNo,FLIGHTDATA *prpSingleFlight = NULL); //Singapore
	BOOL IsTerminalFlightToAdd(int ipGroupNo,FLIGHTDATA *prpSingleFlight); //Singapore

	int imT1FlightsGroup;
	int imT2FlightsGroup;
	int imT3FlightsGroup;
	int imT4FlightsGroup;
	int imTBFlightsGroup;
	int imWTFlightsGroup;
	int CreateLinesForWTFlights(FLIGHTDATA *prpSingleFlight = NULL);
	void UpdateDiagramTerminals(FLIGHTDATA *prpFlight);
	CStringList omListTerminalGroupName;
	CMapStringToString omMapTerminalGroupNameToNo;

	// printing
public:
	void PrintDiagram(CPtrArray &opPtrArray);
	void PrintFm(CPtrArray &opPtrArray);
	void GetDemandList(CUIntArray  &ropDemandList);

private:
	void PrintGateDiagramHeader(int ipGroupno);
	CCSPrint *pomPrint;
	int	igFirstGroupOnPage;
	void PrintGateArea(CPtrArray &opPtrArray,int ipGroupNo);
	void PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine);
	void PrintManagers(int ipGroupno,int ipYOffset1,int ipYOffset2);

	void PrintFmHeader(int ipGroupNo);
	void PrintOneFm(SHIFTDATA *prpShift,EMPDATA *prpEmp,JOBDATA *prpJob,BOOL bpIsFirstLine);
	void PrintFmForGateArea(CPtrArray &opPtrArray,int ipGroupNo);
	CTime GetFlightArr(FLIGHTDATA *prpFlight, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	CTime GetFlightDep(FLIGHTDATA *prpFlight, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	void CheckBarDuration(GATE_BARDATA &ropBar);
};

/////////////////////////////////////////////////////////////////////////////

#endif
