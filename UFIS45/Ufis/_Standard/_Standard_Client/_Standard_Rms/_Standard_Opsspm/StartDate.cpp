// dstartd.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <StartDate.h>
#include <BasicData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// StartDate dialog


StartDate::StartDate(CWnd* pParent /*=NULL*/)
    : CDialog(StartDate::IDD, pParent)
{
    //{{AFX_DATA_INIT(StartDate)

    m_DDMM = "";
    m_YYYY = "";
    //}}AFX_DATA_INIT
}

void StartDate::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(StartDate)
    DDX_Text(pDX, IDC_DDMM, m_DDMM);
    DDX_Text(pDX, IDC_YYYY, m_YYYY);
    //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(StartDate, CDialog)
    //{{AFX_MSG_MAP(StartDate)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// StartDate message handlers

BOOL StartDate::OnInitDialog()
{
    CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING33062));
	CWnd *polWnd = GetDlgItem(IDC_STAFF_SHIFT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33063));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}
    
    // TODO: Add extra initialization here
    CTime t = ogBasicData.GetTime();    

    m_DDMM = t.Format("%d%m");
    m_YYYY = t.Format("%Y");
    UpdateData ( FALSE );
    
    CenterWindow();
    
    return TRUE;  // return TRUE  unless you set the focus to a control
}
