// FlightMgrDetailWindow.h : header file
//


#ifndef _FMDW_
#define _FMDW_

#include <FlightMgrDetailViewer.h>

/////////////////////////////////////////////////////////////////////////////
// FlightMgrDetailWindow dialog

class FlightMgrDetailWindow : public CDialog
{
// Construction
public:
	FlightMgrDetailWindow(CWnd* pParent, char *pcpAlid,
	BOOL bpArrival, BOOL bpDeparture, CTime opStartTime, CTime opEndTime);
	
	~FlightMgrDetailWindow();

	FlightMgrDetailViewer omViewer;
	CCSDragDropCtrl omDragDropObject;

// Dialog Data
	//{{AFX_DATA(FlightMgrDetailWindow)
	enum { IDD = IDD_FLIGHTMANAGER_DETAIL };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FlightMgrDetailWindow)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnCancel();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FlightMgrDetailWindow)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnMenuDelete();
    afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
	char cmAlid[255];
	char *pcmAlid;
	BOOL bmArrival;
	BOOL bmDeparture;
	CTime omStartTime;
	CTime omEndTime;

	int m_nDialogBarHeight;
	CTable omTable;
	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;
	int omContextItem;
	long omContextJobUrno;
	
    static int imCount;             // number of instances created
};

#endif // _FMDW_

