// RegnDetailViewer.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <RegnDetailViewer.h>
#include <PrintControl.h>
extern bool BGS;

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void RegnDetailCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// RegnDetailViewer
//

RegnDetailViewer::RegnDetailViewer()
{
    pomTable = NULL;
}

RegnDetailViewer::~RegnDetailViewer()
{
	ogCCSDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}


void RegnDetailViewer::Attach(CTable *popTable,CTime opStartTime,CTime opEndTime,CString opAlid)
{
    pomTable = popTable;
	omAlid = opAlid;
	omStartTime = opStartTime;
	omEndTime = opEndTime;
    ogCCSDdx.Register(this, FLIGHT_CHANGE, CString("GDVIEWER"), CString("Shift Change"), RegnDetailCf);

}

void RegnDetailViewer::ChangeView()
{

    DeleteAll();    
    MakeLines();
//	MakeAssignments();

	UpdateDisplay();
}



BOOL RegnDetailViewer::IsPassFilter(CTime opAcfr)
{
	BOOL blIsTimeOk = (omStartTime <= opAcfr && opAcfr <= omEndTime);

    return (blIsTimeOk);
}

/////////////////////////////////////////////////////////////////////////////
// RegnDetailViewer -- code specific to this class

void RegnDetailViewer::MakeLines()
{
	CCSPtrArray<FLIGHTDATA> olFlights;
	ogFlightData.GetFlightsByRegn(olFlights, omAlid);
    int ilFlightCount = olFlights.GetSize();
    for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
    {
        FLIGHTDATA *prlFlight = &olFlights[ilLc];
        MakeLine(prlFlight);
    }
}


void RegnDetailViewer::MakeAssignments(long lpFlightUrno,REGNDETAIL_LINEDATA &rrpFlightLine)
{
	CCSPtrArray <JOBDATA> olJobs;
	ogJobData.GetJobsByFlur(olJobs,lpFlightUrno,FALSE); // don't like JOBFM);

    for ( int ilLc = 0; ilLc < olJobs.GetSize(); ilLc++ )
    {
        JOBDATA *prlJob = &olJobs[ilLc];
		
		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);

	    // Create a new assignment
        REGNDETAIL_ASSIGNMENT rlAssignment;
        rlAssignment.Name = ogEmpData.GetEmpName(prlEmp);
		rlAssignment.Urno = prlJob->Urno;
        rlAssignment.Acfr = prlJob->Acfr;
        rlAssignment.Acto = prlJob->Acto;
        CreateAssignment(rrpFlightLine, &rlAssignment);
    }
}

void RegnDetailViewer::MakeLine(FLIGHTDATA *prpFlight)
{

    // Update viewer data for this flight record
    REGNDETAIL_LINEDATA rlFlightLine;
	rlFlightLine.FlightTime = (ogFlightData.IsArrival(prpFlight) ? prpFlight->Stoa : prpFlight->Stod);
	
	if (IsPassFilter(rlFlightLine.FlightTime) == TRUE)
	{
		rlFlightLine.FlightUrno = prpFlight->Urno;
		rlFlightLine.Fnum = prpFlight->Fnum;
		rlFlightLine.Rou1 = (ogFlightData.IsArrival(prpFlight) ? prpFlight->Org3 : prpFlight->Des3);
		rlFlightLine.Iofl = prpFlight->Adid;

		MakeAssignments(prpFlight->Urno,rlFlightLine);
		CreateLine(&rlFlightLine);
	}
}

BOOL RegnDetailViewer:: FindFlight(long lpFlightUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
		long x = omLines[rilLineno].FlightUrno;
        if (omLines[rilLineno].FlightUrno == lpFlightUrno)
            return TRUE;
	}
    return FALSE;
}

int RegnDetailViewer::CreateAssignment(REGNDETAIL_LINEDATA &rrpLine, REGNDETAIL_ASSIGNMENT *prpAssignment)
{
    int ilAssignmentCount = rrpLine.Assignment.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
    // Search for the position which we want to insert this new bar
    for (int ilAssignmentno = ilAssignmentCount; ilAssignmentno > 0; ilAssignmentno--)
        if (prpAssignment->Acfr >= rrpLine.Assignment[ilAssignmentno-1].Acfr)
            break;  // should be inserted after Lines[ilAssignmentno-1]

    rrpLine.Assignment.NewAt(ilAssignmentno, *prpAssignment);

    return ilAssignmentno;
}


/////////////////////////////////////////////////////////////////////////////
// RegnDetailViewer - REGNDETAIL_LINEDATA array maintenance

void RegnDetailViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int RegnDetailViewer::CreateLine(REGNDETAIL_LINEDATA *prpFlightLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
    if (prpFlightLine->FlightTime <= omLines[ilLineno].FlightTime)
        break;  // should be inserted after Lines[ilLineno-1]

	REGNDETAIL_LINEDATA rlFlightLine;
	rlFlightLine = *prpFlightLine;
    omLines.NewAt(ilLineno, rlFlightLine);

    return ilLineno;
}

void RegnDetailViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}


/////////////////////////////////////////////////////////////////////////////
// RegnDetailViewer - display drawing routine

// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void RegnDetailViewer::UpdateDisplay()
{
    //pomTable->SetHeaderFields("Flugnummer|Apt||Sta/d|Name|Name|Name|Name|Name|Name");
    if (BGS) pomTable->SetHeaderFieldsBGS(GetString(IDS_STRING61568));
	else	 pomTable->SetHeaderFields(GetString(IDS_STRING61568));
  
	
	
	pomTable->SetFormatList("10|4|1|4|22|22|22|22|22|22");


    // Load filtered and sorted data into the table content
	pomTable->ResetContent();
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		pomTable->AddTextLine(Format(&omLines[ilLc]), &omLines[ilLc]);
    }                                           

    // Update the table content in the display
    pomTable->DisplayTable();
}

CString RegnDetailViewer::Format(REGNDETAIL_LINEDATA *prpLine)
{

	CString olText;
	olText = prpLine->Fnum;
	olText += "|" + prpLine->Rou1;
	olText += "|" + prpLine->Iofl;
	olText += "|" + prpLine->FlightTime.Format("%H%M");

    for (int ilLc = 0; ilLc < prpLine->Assignment.GetSize(); ilLc++)
    {
        REGNDETAIL_ASSIGNMENT *prlAssignment = &prpLine->Assignment[ilLc];
        olText += "|" + prlAssignment->Name;
    }
    return olText;
}


static void RegnDetailCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RegnDetailViewer *polViewer = (RegnDetailViewer *)popInstance;

    if (ipDDXType == FLIGHT_CHANGE)
        polViewer->ProcessFlightChange((FLIGHTDATA *)vpDataPointer);

}


void RegnDetailViewer::ProcessFlightChange(FLIGHTDATA *prpFlight)
{

	int ilItem;

	if (FindFlight(prpFlight->Urno,ilItem))
	{
        REGNDETAIL_LINEDATA *prlLine = &omLines[ilItem];

		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetJobsByFlur(olJobs,prpFlight->Urno,FALSE); // don't like JOBFM);

		// Update viewer data for this shift record
		prlLine->FlightUrno = prpFlight->Urno;
		prlLine->Fnum = prpFlight->Fnum;
		prlLine->Rou1 = (ogFlightData.IsArrival(prpFlight) ? prpFlight->Org3 : prpFlight->Des3);
		prlLine->Iofl = prpFlight->Adid;
		prlLine->FlightTime = (ogFlightData.IsArrival(prpFlight) ? prpFlight->Stoa : prpFlight->Stod);

		prlLine->Assignment.RemoveAll();
		MakeAssignments(prpFlight->Urno,*prlLine);

        pomTable->ChangeTextLine(ilItem, Format(&omLines[ilItem]), &omLines[ilItem]);
		pomTable->DisplayTable();
	}
	else
	{
		if (omAlid == prpFlight->Regn)
		{
			MakeLine(prpFlight);
			if (FindFlight(prpFlight->Urno,ilItem))
			{
				pomTable->InsertTextLine(ilItem,Format(&omLines[ilItem]), &omLines[ilItem]);
				pomTable->DisplayTable();
			}
		}
	}
}





