// ccichrt.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <OpssPm.h>
#include <cxbutton.h>
#include <clientwn.h>
#include <tscale.h>
#include <CCSDragDropCtrl.h>
#include <CedaJobData.h>
#include <cviewer.h>
#include <CciViewer.h>
#include <CciGantt.h>
#include <CciDiagram.h>

#include <AssignDlg.h>
#include <AssignFromPrePlanTableDlg.h>	// for CAssignmentFromPrePlanTableDialog

#include <CciChart.h>
#include <dataset.h>
#include <AllocData.h>
#include <BasicData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////
// CCIChart

IMPLEMENT_DYNCREATE(CCIChart, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CCIChart::CCIChart()
{
    pomLeftTimeScale = NULL;
	pomRightTimeScale = NULL;
    pomStatusBar = NULL;
    pomViewer = NULL;
	bmScrolling = false;
    pomTopScaleText = NULL;
}

CCIChart::~CCIChart()
{
    if (pomTopScaleText != NULL)
        delete  pomTopScaleText;
}

BEGIN_MESSAGE_MAP(CCIChart, CFrameWnd)
    //{{AFX_MSG_MAP(CCIChart)
    ON_WM_CREATE()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_PAINT()
    ON_BN_CLICKED(IDC_CHARTBUTTON, OnChartButton)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)  
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_WM_KEYUP()
	ON_WM_KEYDOWN()
	ON_WM_DESTROY()
    ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


int CCIChart::GetHeight()
{
    if (imState == Minimized)
        return imStartVerticalScalePos; // height of ChartButtton/TopScaleText
    else                                // (imState == Normal) || (imState == Maximized)
        return imStartVerticalScalePos + imHeight + 7;
}

void CCIChart::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

void CCIChart::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

/////////////////////////////////////////////////////////////////////////////
// CCIChart message handlers

#include <clientwn.h>

int CCIChart::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
        return -1;
    
    // TODO: Add your specialized creation code here
    CString olStr; GetWindowText(olStr);
    omButton.Create(olStr, BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, CRect(5, 4, 110, 4 + 20), this, IDC_CHARTBUTTON);
    omButton.SetFont(&ogMSSansSerif_Bold_8, FALSE);
    
  
    CRect olRect; GetClientRect(&olRect);

    pomTopScaleText = new C3DStatic(TRUE);
    pomTopScaleText->Create("Top Scale Text", WS_CHILD | WS_VISIBLE,
        CRect(imStartTopScaleTextPos + 6, 4 + 1, olRect.right - 6, (4 + 20) - 1), this);

    m_TopScaleTextDragDrop.RegisterTarget(pomTopScaleText, this);
	m_ChartWindowDragDrop.RegisterTarget(this, this);
    m_ChartButtonDragDrop.RegisterTarget(&omButton, this);
    
    // read all data
    SetState(Maximized);
    
    omLeftGantt.SetTimeScale(GetLeftTimeScale());
    omLeftGantt.SetViewer(GetViewer(), GetGroupNo(), 0);
    omLeftGantt.SetStatusBar(GetStatusBar());
    omLeftGantt.SetDisplayWindow(GetStartTime(), GetStartTime() + GetInterval());
    //omLeftGantt.SetVerticalScaleWidth(imStartTopScaleTextPos);
	omLeftGantt.SetVerticalScaleWidth(/*MAWO 40*/50);
	omLeftGantt.SetVerticalScaleIndent(3);
	//MWO 12.09.1996
omLeftGantt.SetVerticalScaleWidth(/*MWO 39*/49);
omLeftGantt.SetVerticalScaleIndent(3);
    //omLeftGantt.SetFonts(&ogMSSansSerif_Bold_8, &ogSmallFonts_Regular_6);
    omLeftGantt.SetFonts(ogCCiIndexes.VerticalScale, ogCCiIndexes.Chart);
	//MWO END
    //omLeftGantt.SetVerticalScaleColors(NAVY, SILVER, YELLOW, SILVER);
    omLeftGantt.SetVerticalScaleColors(NAVY, SILVER, BLACK, YELLOW);
    omLeftGantt.SetGanttChartColors(BLACK, SILVER, BLACK, SILVER);
//    omLeftGantt.Create(0, CRect(olRect.left, imStartVerticalScalePos,((olRect.right-(36+2))/2), olRect.bottom), this);
omLeftGantt.Create(WS_VSCROLL, CRect(olRect.left, imStartVerticalScalePos,((olRect.right-(36+2))/2), olRect.bottom), this);

    omRightGantt.SetTimeScale(GetRightTimeScale());
    omRightGantt.SetViewer(GetViewer(), GetGroupNo(), 1);
    omRightGantt.SetStatusBar(GetStatusBar());
    omRightGantt.SetDisplayWindow(GetStartTime(), GetStartTime() + GetInterval());
	//MWO 12.09.1996
	omRightGantt.SetVerticalScaleWidth(/*MWO 39*/49);
	omRightGantt.SetVerticalScaleIndent(3);
    //omRightGantt.SetFonts(&ogMSSansSerif_Bold_8, &ogSmallFonts_Regular_6);
    omRightGantt.SetFonts(ogCCiIndexes.VerticalScale, ogCCiIndexes.Chart);
	//MWO END
    //omRightGantt.SetVerticalScaleColors(NAVY, SILVER, YELLOW, SILVER);
    omRightGantt.SetVerticalScaleColors(NAVY, SILVER, BLACK, YELLOW);
    omRightGantt.SetGanttChartColors(BLACK, SILVER , BLACK, SILVER);
    omRightGantt.Create(WS_VSCROLL, CRect(((olRect.right-(36+2))/2)+2, imStartVerticalScalePos, olRect.right, olRect.bottom), this);

	// link left and right GanttCharts together
	omLeftGantt.SetSiblingGanttChart(&omRightGantt);
	omRightGantt.SetSiblingGanttChart(&omLeftGantt);

    olStr = GetViewer()->GetGroupText(GetGroupNo());    
    omButton.SetWindowText(olStr);
    olStr = GetViewer()->GetGroupTopScaleText(GetGroupNo());
    pomTopScaleText->SetWindowText(olStr);
    
    char clBuf[255]; pomTopScaleText->GetWindowText(clBuf, sizeof(clBuf));
    //TRACE("pomTopScaleText: [%s][%d]\n", clBuf, strlen(clBuf));
    
    imHeight = max(omLeftGantt.GetGanttChartHeight(),omRightGantt.GetGanttChartHeight());
    //TRACE("imHeight=%d\n", imHeight);
    //
    return 0;
}

void CCIChart::OnDestroy() 
{
	if (bgModal == TRUE)
		return;

	// Id 30-Sep-96
	// This will remove a lot of warning message when the user change view.
	// If we just delete a staff chart, MFC will produce two warning message.
	// First, Revoke not called before the destructor.
	// Second, calling DestroyWindow() in CWnd::~CWnd.

	m_ChartButtonDragDrop.Revoke();
	m_TopScaleTextDragDrop.Revoke();
	m_ChartWindowDragDrop.Revoke();

	CFrameWnd::OnDestroy();
}

void CCIChart::OnSize(UINT nType, int cx, int cy) 
{
    // TODO: Add your message handler code here
    CFrameWnd::OnSize(nType, cx, cy);
    
    CRect olClientRect; GetClientRect(&olClientRect);
    //TRACE("CCIChart OnSize: client rect [top=%d, bottom+%d]\n", olClientRect.top, olClientRect.bottom);
    CRect olRect(imStartTopScaleTextPos + 6, 4 + 1, olClientRect.right - 6, (4 + 20) - 1);
    pomTopScaleText->MoveWindow(&olRect, FALSE);

    olRect.SetRect(olClientRect.left, imStartVerticalScalePos, ((olClientRect.right-(36+2))/2), olClientRect.bottom);
    //TRACE("CCIChart SetRect: client rect [top=%d, bottom+%d]\n", imStartVerticalScalePos, olClientRect.bottom);
    omLeftGantt.MoveWindow(&olRect, FALSE);

    olRect.SetRect(((olClientRect.right-(36+2))/2)+2, imStartVerticalScalePos, olClientRect.right, olClientRect.bottom);
    //TRACE("CCIChart SetRect: client rect [top=%d, bottom+%d]\n", imStartVerticalScalePos, olClientRect.bottom);
    omRightGantt.MoveWindow(&olRect, FALSE);
}

BOOL CCIChart::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void CCIChart::OnPaint()
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    
    // Do not call CFrameWnd::OnPaint() for painting messages
    CPen *polOldPen = (CPen *) dc.SelectStockObject(BLACK_PEN);

    CRect olClientRect;
    GetClientRect(&olClientRect);
    
#define imHorizontalPos imStartVerticalScalePos
#define imVerticalPos imStartTopScaleTextPos

    // draw horizontal seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 2);
    dc.LineTo(olClientRect.right, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 1);
    dc.LineTo(olClientRect.right, imHorizontalPos - 1);

    // draw vertical seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(imVerticalPos - 2, olClientRect.top);
    //dc.LineTo(imVerticalPos - 2, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 2, imHorizontalPos - 2);

    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(imVerticalPos - 1, olClientRect.top);
    //dc.LineTo(imVerticalPos - 1, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 1, imHorizontalPos - 2);

    // draw vertical line (CCI)
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(((olClientRect.right - (36 + 2)) / 2), imHorizontalPos);
    dc.LineTo(((olClientRect.right - (36 + 2)) / 2), olClientRect.bottom);

    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(((olClientRect.right - (36 + 2)) / 2) + 1, imHorizontalPos);
    dc.LineTo(((olClientRect.right - (36 + 2)) / 2) + 1, olClientRect.bottom);
    
    dc.SelectObject(polOldPen);
}

void CCIChart::OnChartButton()
{
    if (imState == Minimized)
        SetState(Maximized);
    else
        SetState(Minimized);

    GetParent() -> GetParent() -> SendMessage(WM_POSITIONCHILD, 0, 0L);
}

//***** change everything below this line
/////////////////////////////////////////////////////////////////////////////
// CciChart -- drag-and-drop functionalities
//
// In a CciChart, the user will have opportunities to:
//	-- drop from PrePlanTable onto the CciArea button.
//		Create a JobPool for every staff. If the staff is a flight manager,
//		we will store the CciArea in the field PRID also. If the user press
//		Ctrl button, we have to open a dialog for asking the period of time.
//		The user must group the CciDiagram by Terminal only.
//
LONG CCIChart::OnDragOver(UINT wParam, LONG lParam)
{
	CCSDragDropCtrl *pomDragDropCtrl;
	bool blAutoScroll = false;
	long llRc = 0L;

	// First we have to find the correct drag-and-drop control object
	if ((CWnd *)lParam == this)
	{
		pomDragDropCtrl = &m_ChartWindowDragDrop;
	}
	else if ((CWnd *)lParam == &omButton)
	{
		pomDragDropCtrl = &m_ChartButtonDragDrop;
	}
	else if ((CWnd *)lParam == pomTopScaleText)
	{
		pomDragDropCtrl = &m_TopScaleTextDragDrop;
	}
	else
	{
		llRc = -1L;
	}


	if(llRc == 0L)
	{
		llRc = -1L;
		switch (pomDragDropCtrl->GetDataClass())
		{
		case DIT_SHIFT:
			llRc = 0L;	// drop from FlightBar onto any places in StaffChart
		default:
			blAutoScroll = true;
			llRc = -1L;	// drop from FlightBar onto any places in StaffChart
			break;
		}
	}

	if(blAutoScroll)
	{
		AutoScroll(750);
	}
	else
	{
		AutoScroll(); // end scrolling
	}

	return llRc;
}

// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void CCIChart::AutoScroll(UINT ipInitialScrollSpeed /*200*/)
{
	// if not already scrolling automatically...
	if(!bmScrolling)
	{
		// ... after a short pause (ipInitialScrollSpeed), start scrolling
		bmScrolling = true;
		imScrollSpeed = ipInitialScrollSpeed;
		SetTimer(1, (UINT) imScrollSpeed, NULL);
	}
}

// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void CCIChart::OnAutoScroll(void)
{
	bmScrolling = false;

	// if standard cursor then not currently dragging
	if(GetCursor() != AfxGetApp()->LoadStandardCursor(IDC_ARROW))
	{
		// check if the cursor is in the automatic scrolling region
		CPoint olPoint;
		::GetCursorPos(&olPoint);

		CRect olLeftGanttRect;
		omLeftGantt.GetWindowRect(&olLeftGanttRect);

		if(olPoint.y <= olLeftGanttRect.top && olPoint.y > (olLeftGanttRect.top - 30))
		{
			// above the LeftGantt so scroll up
			omLeftGantt.SetTopIndex(omLeftGantt.GetTopIndex()-1);
			bmScrolling = true;
		}
		else if(olPoint.y >= olLeftGanttRect.bottom && olPoint.y < (olLeftGanttRect.bottom + 30))
		{
			// below the LeftGantt so scroll down
			omLeftGantt.SetTopIndex(omLeftGantt.GetTopIndex()+1);
			bmScrolling = true;
		}

		CRect olRightGanttRect;
		omRightGantt.GetWindowRect(&olRightGanttRect);

		if(olPoint.y <= olRightGanttRect.top && olPoint.y > (olRightGanttRect.top - 30))
		{
			// above the RightGantt so scroll up
			omRightGantt.SetTopIndex(omRightGantt.GetTopIndex()-1);
			bmScrolling = true;
		}
		else if(olPoint.y >= olRightGanttRect.bottom && olPoint.y < (olRightGanttRect.bottom + 30))
		{
			// below the RightGantt so scroll down
			omRightGantt.SetTopIndex(omRightGantt.GetTopIndex()+1);
			bmScrolling = true;
		}
	}

	if(bmScrolling)
	{
		if(imScrollSpeed != 50)
		{
			imScrollSpeed = 50;
			SetTimer(1, (UINT) 50, NULL);
		}
	}
	else
	{
		// no longer in the scrolling region or left mouse button released
		KillTimer(1);
	}
}

void CCIChart::OnTimer(UINT nIDEvent)
{
	if(nIDEvent == 1)
	{
		// called during drag&drop, does automatic scrolling when the cursor
		// is outside the main gantt chart
		OnAutoScroll();
		CFrameWnd::OnTimer(nIDEvent);
	}
}

LONG CCIChart::OnDrop(UINT wParam, LONG lParam)
{
	CCSDragDropCtrl *pomDragDropCtrl;

	if ((CWnd *)lParam == this)
		pomDragDropCtrl = &m_ChartWindowDragDrop;
	else if ((CWnd *)lParam == &omButton)
		pomDragDropCtrl = &m_ChartButtonDragDrop;
	else if ((CWnd *)lParam == pomTopScaleText)
		pomDragDropCtrl = &m_TopScaleTextDragDrop;
	else
		return -1L;

	// Don't allow drop to CCI Diagram when not groupped by Terminal
	if (!pomViewer->IsGrouppedByTerminal())
		return -1L;

	ProcessDropShifts(pomDragDropCtrl, wParam);
    return 0L;
}

LONG CCIChart::ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	if (lpDropEffect == DROPEFFECT_COPY)	// holding the Control key?
	{
		CString olPool = ogBasicData.GetPool(this, ALLOCUNITTYPE_CICGROUP, pomViewer->GetGroup(imGroupNo)->Alid);
		if(!olPool.IsEmpty())
		{
			CAssignmentFromPrePlanTableToCciAreaDialog olDlg(this, popDragDropCtrl,
									olPool,pomViewer->GetGroup(imGroupNo)->Alid,
									pomViewer->GetGroup(imGroupNo)->Text);
			olDlg.DoModal();
		}
	}
	else if (lpDropEffect == DROPEFFECT_MOVE)
	{
		CString olPool = ogBasicData.GetPool(this, ALLOCUNITTYPE_CICGROUP, pomViewer->GetGroup(imGroupNo)->Alid);
		if(!olPool.IsEmpty())
		{
			CDWordArray olShiftUrnos;
			for (int ilIndex = 0; ilIndex < popDragDropCtrl->GetDataCount(); ilIndex++)
				olShiftUrnos.Add(popDragDropCtrl->GetDataDWord(ilIndex));
			CString olCciArea = pomViewer->GetGroup(imGroupNo)->Alid;
			ogDataSet.CreateJobPool(this, olShiftUrnos, olPool, olCciArea);
		}
	}

	return 0L;
}
