// CciDiagramGroupPage.h : header file
//

#ifndef _CCIDIAGR_H_
#define _CCIDIAGR_H_

// Message for notify the CCIDiagramPropertySheet
#define WM_CCIDIAGRAM_GROUPPAGE_CHANGED	(WM_USER + 320)

/////////////////////////////////////////////////////////////////////////////
// CCIDiagramGroupPage dialog

class CCIDiagramGroupPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CCIDiagramGroupPage)

// Construction
public:
	CCIDiagramGroupPage();
	~CCIDiagramGroupPage();

// Dialog Data
	CString omGroupBy;

	//{{AFX_DATA(CCIDiagramGroupPage)
	enum { IDD = IDD_CCIDIAGRAM_GROUP_PAGE };
	int		m_GroupBy;
	//}}AFX_DATA
	CString omTitle;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CCIDiagramGroupPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCIDiagramGroupPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetGroupId(const char *pcpGroupKey);
	CString GetGroupKey(int ipGroupId);
};

#endif // _CCIDIAGR_H_
