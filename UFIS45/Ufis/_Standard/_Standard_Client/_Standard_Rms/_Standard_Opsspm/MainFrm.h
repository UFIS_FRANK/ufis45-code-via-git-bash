// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__6978E8D8_E90E_11D3_9387_0050DA1CABCB__INCLUDED_)
#define AFX_MAINFRM_H__6978E8D8_E90E_11D3_9387_0050DA1CABCB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
public:
	void SetWindowTitle();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CReBar      m_wndReBar;
	CDialogBar  m_wndDlgBar;

private:
	CRect omWindowRect; //PRF 8712

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnSize(UINT nType, int cx, int cy); //PRF 8712
	afx_msg void OnMove(int x, int y); //PRF 8712
	afx_msg void OnDestroy(); //PRF 8712
	afx_msg void OnAppAbout();
	afx_msg void OnChartCci();
	afx_msg void OnChartGate();
	afx_msg void OnChartStaff();
	afx_msg void OnChartRegn();
	afx_msg void OnListGate();
	afx_msg void OnFlightJobs();
	afx_msg void OnListCci();
	afx_msg void OnListStaff();
	afx_msg void OnListPreplan();
	afx_msg void OnListFlight();
	afx_msg void OnListConflict();
	afx_msg void OnListAttention();
	afx_msg void OnListDemands();
	afx_msg void OnChartPos();
	afx_msg void OnChartEqu();
	afx_msg void OnListCloseAllSchedules();
	afx_msg void OnHelpMenuItem();
	afx_msg void OnFilePrint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	bool TranslateMenu ();
	BOOL SetMenu(HMENU opMenu, const char *pcpKey, const char *pcpText, UINT ipMenuId, bool bpEnabledInCedaIni = true);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__6978E8D8_E90E_11D3_9387_0050DA1CABCB__INCLUDED_)
