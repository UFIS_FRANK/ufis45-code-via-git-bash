// PrintOrExportToExcel.cpp : implementation file
//

#include "stdafx.h"
#include "opsspm.h"
#include "PrintOrExportToExcel.h"
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintOrExportToExcel dialog


CPrintOrExportToExcel::CPrintOrExportToExcel(CWnd* pParent /*=NULL*/)
	: CDialog(CPrintOrExportToExcel::IDD, pParent)
{
	bmIsExportToExcel = TRUE;
	bmIsPageBreakEnabled = FALSE;
	//{{AFX_DATA_INIT(CPrintOrExportToExcel)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CPrintOrExportToExcel::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX,IDC_RADIO_EXPORT_TO_EXCEL, omRadioButton);
	DDX_Control(pDX,IDC_PAGE_BREAK,omCheckBoxPageBreak);
	DDX_Control(pDX,IDC_CHECK_COLOR,omCheckBoxColorEnabled);
	//{{AFX_DATA_MAP(CPrintOrExportToExcel)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BOOL CPrintOrExportToExcel::OnInitDialog()
{
	CDialog::OnInitDialog();	
	CButton* polButton = (CButton*)GetDlgItem(IDC_RADIO_EXPORT_TO_EXCEL);
	if(polButton != NULL)
	{
		polButton->SetCheck(1);
	}


   SetWindowText(GetString(IDS_61979));
   SetDlgItemText(IDC_STATIC1, GetString(IDS_61980));
SetDlgItemText(IDC_RADIO_EXPORT_TO_EXCEL, GetString(IDS_61981));
SetDlgItemText(IDC_PAGE_BREAK, GetString(IDS_61982));
  SetDlgItemText(IDC_CHECK_COLOR, GetString(IDS_61983));
SetDlgItemText(IDC_RADIO_PRINT, GetString(IDS_61984));
SetDlgItemText(IDOK, GetString(IDS_61985));
SetDlgItemText(IDCANCEL, GetString(IDS_61986));

	return TRUE;
}

BOOL CPrintOrExportToExcel::IsExportToExcel()
{	
	return bmIsExportToExcel;
}

BOOL CPrintOrExportToExcel::IsPageBreakEnabled()
{
	return bmIsPageBreakEnabled;
}

BOOL CPrintOrExportToExcel::IsColorEnabled()
{
	return bmIsColorEnabled;
}

void CPrintOrExportToExcel::OnOK()
{
	UpdateData();
	bmIsExportToExcel = omRadioButton.GetCheck() == 1 ? TRUE : FALSE;
	bmIsPageBreakEnabled = omCheckBoxPageBreak.GetCheck() == 1 ? TRUE : FALSE;
	bmIsColorEnabled = omCheckBoxColorEnabled.GetCheck() == 1 ? TRUE : FALSE;
	CDialog::OnOK();
}

BEGIN_MESSAGE_MAP(CPrintOrExportToExcel, CDialog)
	//{{AFX_MSG_MAP(CPrintOrExportToExcel)
			// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintOrExportToExcel message handlers
