// gatechrt.cpp : implementation file
//


#include <stdafx.h>
#include <CCSGlobl.h>
#include <resource.h>
#include <OpssPm.h>
#include <cxbutton.h>
#include <clientwn.h>
#include <tscale.h>
#include <CCSDragDropCtrl.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <PstDiagram.h>
#include <clientwn.h>
#include <tscale.h>
#include <CCSDragDropCtrl.h>
#include <PstViewer.h>
#include <PstGantt.h>
#include <dataset.h>
#include <BasicData.h>
#include <AllocData.h>

#include <AssignDlg.h>
#include <AssignFromPrePlanTableDlg.h>	// for CAssignmentFromPrePlanTableDialog

#include <PstChart.h>
#include <table.h>
#include <FlightMgrDetailWindow.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PstChart

IMPLEMENT_DYNCREATE(PstChart, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

PstChart::PstChart()
{
    pomTimeScale = NULL;
    pomStatusBar = NULL;
    pomViewer = NULL;
    
    pomTopScaleText = NULL;
}

PstChart::~PstChart()
{
    if (pomTopScaleText != NULL)
        delete  pomTopScaleText;
}

BEGIN_MESSAGE_MAP(PstChart, CFrameWnd)
    //{{AFX_MSG_MAP(PstChart)
    ON_WM_CREATE()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_PAINT()
    ON_BN_CLICKED(IDC_CHARTBUTTON, OnChartButton)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)  
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_WM_LBUTTONDBLCLK()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

int PstChart::GetHeight()
{
    if (imState == Minimized)
        return imStartVerticalScalePos; // height of ChartButtton/TopScaleText
    else                                // (imState == Normal) || (imState == Maximized)
/////////////////////////////////////////////////////////////////////////////
// Id 18 Sep - Pichet's version uses "imHeight" which will be fixed by OnCreated().
// That's not correct, and it's the reason for scroll bars happended in
// non-bottommost gantt chart of a diagram.
        //return imStartVerticalScalePos + imHeight + 2;
		return imStartVerticalScalePos + omGantt.GetGanttChartHeight() + 7;
/////////////////////////////////////////////////////////////////////////////
}

/////////////////////////////////////////////////////////////////////////////
// PstChart message handlers

#include <clientwn.h>

int PstChart::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
        return -1;
    
    // TODO: Add your specialized creation code here
    CString olStr; GetWindowText(olStr);
    omButton.Create(olStr, BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, CRect(5, 4, 110, 4 + 20), this, IDC_CHARTBUTTON);
    omButton.SetFont(&ogMSSansSerif_Bold_8, FALSE);
    
    m_ChartButtonDragDrop.RegisterTarget(&omButton, this);
    
    CRect olRect; GetClientRect(&olRect);

    pomTopScaleText = new C3DStatic(TRUE);
    pomTopScaleText->Create("Top Scale Text", WS_CHILD | WS_VISIBLE,
        CRect(imStartTopScaleTextPos + 6, 4 + 1, olRect.right - 6, (4 + 20) - 1), this);

    m_TopScaleTextDragDrop.RegisterTarget(pomTopScaleText, this);

    ////
    // read all data
    SetState(Maximized);
    
    omGantt.SetTimeScale(GetTimeScale());
    omGantt.SetViewer(GetViewer(), GetGroupNo());
    omGantt.SetStatusBar(GetStatusBar());
    omGantt.SetDisplayWindow(GetStartTime(), GetStartTime() + GetInterval());
    omGantt.SetVerticalScaleWidth(imStartTopScaleTextPos);
    omGantt.SetFonts(ogPstIndexes.VerticalScale, ogPstIndexes.Chart);
    omGantt.SetVerticalScaleColors(NAVY, SILVER, BLACK, YELLOW);
    omGantt.Create(0, CRect(olRect.left, imStartVerticalScalePos, olRect.right, olRect.bottom), this);

    olStr = GetViewer()->GetGroupText(GetGroupNo());    
    omButton.SetWindowText(olStr);
    olStr = GetViewer()->GetGroupTopScaleText(GetGroupNo());
    pomTopScaleText->SetWindowText(olStr);
    char clBuf[255];
	pomTopScaleText->GetWindowText(clBuf, sizeof(clBuf));
    
    imHeight = omGantt.GetGanttChartHeight();
    return 0;
}

void PstChart::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	// Id 30-Sep-96
	// This will remove a lot of warning message when the user change view.
	// If we just delete a staff chart, MFC will produce two warning message.
	// First, Revoke not called before the destructor.
	// Second, calling DestroyWindow() in CWnd::~CWnd.

    m_ChartButtonDragDrop.Revoke();
    m_TopScaleTextDragDrop.Revoke();

	CFrameWnd::OnDestroy();
}

void PstChart::OnSize(UINT nType, int cx, int cy) 
{
    // TODO: Add your message handler code here
    CFrameWnd::OnSize(nType, cx, cy);
    
    CRect olClientRect; GetClientRect(&olClientRect);
    //TRACE("PstChart OnSize: client rect [top=%d, bottom+%d]\n", olClientRect.top, olClientRect.bottom);
    CRect olRect(imStartTopScaleTextPos + 6, 4 + 1, olClientRect.right - 6, (4 + 20) - 1);
    pomTopScaleText->MoveWindow(&olRect, FALSE);

    olRect.SetRect (olClientRect.left, imStartVerticalScalePos, olClientRect.right, olClientRect.bottom);
    //TRACE("PstChart SetRect: client rect [top=%d, bottom+%d]\n", imStartVerticalScalePos, olClientRect.bottom);
    omGantt.MoveWindow(&olRect, FALSE);
}

BOOL PstChart::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
}

void PstChart::OnPaint()
{
    CPaintDC dc(this); // device context for painting
    
    // Do not call CFrameWnd::OnPaint() for painting messages
    CPen *polOldPen = (CPen *) dc.SelectStockObject(BLACK_PEN);

    CRect olClientRect;
    GetClientRect(&olClientRect);
    
#define imHorizontalPos imStartVerticalScalePos
#define imVerticalPos imStartTopScaleTextPos

    // draw horizontal seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 2);
    dc.LineTo(olClientRect.right, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 1);
    dc.LineTo(olClientRect.right, imHorizontalPos - 1);
    //

    // draw vertical seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(imVerticalPos - 2, olClientRect.top);
    //dc.LineTo(imVerticalPos - 2, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 2, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(imVerticalPos - 1, olClientRect.top);
    //dc.LineTo(imVerticalPos - 1, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 1, imHorizontalPos - 2);
    //

    dc.SelectObject(polOldPen);
}

void PstChart::OnChartButton()
{
    if (imState == Minimized)
        SetState(Maximized);
    else
        SetState(Minimized);
        
    GetParent() -> GetParent() -> SendMessage(WM_POSITIONCHILD, 0, 0L);
}

void PstChart::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CFrameWnd::OnLButtonDblClk(nFlags, point);

	if (pomTopScaleText != NULL)
	{
		CRect olRect;
		pomTopScaleText->GetWindowRect(&olRect);
		ScreenToClient(&olRect);
		if (olRect.PtInRect(point) == TRUE)
		{
			CString s = pomViewer->GetGroup(imGroupNo)->PstAreaId;
			char clBuf[255]; strcpy(clBuf, s);
			BOOL blArrival = true, blDeparture = true;
			PstDiagram *polPstDiagram = (PstDiagram *)GetParent() -> GetParent();
			CTime olStartTime;
			CTime olEndTime;
			if (polPstDiagram != NULL)
			{
				olStartTime = polPstDiagram->GetTsStartTime();
				olEndTime = olStartTime + polPstDiagram->GetTsDuration();
			}
			else
			{
				return;
			}
			CWnd *polWnd = GetParent()->GetParent();
			new FlightMgrDetailWindow(polWnd, clBuf, blArrival, blDeparture, olStartTime, olEndTime);
		}
	}
}

void PstChart::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

void PstChart::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

//***** change everything below this line
/////////////////////////////////////////////////////////////////////////////
// PstChart -- drag-and-drop functionalities
//
// In a PstChart, the user will have opportunities to:
//	-- drop from PrePlanTable onto the PstArea button.
//		Create a JobPool for every staff. If the staff is a flight manager,
//		we will store the PstArea in the field PRID also. If the user press
//		Ctrl button, we have to open a dialog for asking the period of time.
//	-- drop from StaffTable onto the PstArea button.
//		Create a JobPstArea for every staff. If the staff is a flight manager,
//		set PRID if it not already set, and complain if it set to another PstArea.
//	-- drop from DutyBar onto the PstArea button.
//		Create a JobPstArea for that staff. We have to open a dialog for asking
//		the period of time.
//
LONG PstChart::OnDragOver(UINT wParam, LONG lParam)
{
	CCSDragDropCtrl *pomDragDropCtrl;

	// First we have to find the correct drag-and-drop control object
	// drag only on PstArea button
	if ((CWnd *)lParam == &omButton)
		pomDragDropCtrl = &m_ChartButtonDragDrop;
	else
		return -1L;

	// For now we have shifts from PrePlanTable only
    int ilClass = pomDragDropCtrl->GetDataClass(); 
	if (ilClass != DIT_SHIFT)
		return -1L;	// cannot interpret this object
	return 0;
}

LONG PstChart::OnDrop(UINT wParam, LONG lParam)
{
	CCSDragDropCtrl *pomDragDropCtrl;

	// First we have to find the correct drag-and-drop control object
	// drag only on PstArea button
	if ((CWnd *)lParam == &omButton)
		pomDragDropCtrl = &m_ChartButtonDragDrop;
	else
		return -1L;

	ProcessDropShifts(pomDragDropCtrl, wParam);
    return 0L;
}

LONG PstChart::ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	if (lpDropEffect == DROPEFFECT_COPY)	// holding the Control key?
	{
		CString olPool = ogBasicData.GetPool(this, ALLOCUNITTYPE_PSTGROUP, pomViewer->GetGroup(imGroupNo)->PstAreaId);
		if(!olPool.IsEmpty())
		{
			CAssignmentFromPrePlanTableToGateAreaDialog olDlg(this, popDragDropCtrl,
								olPool,pomViewer->GetGroup(imGroupNo)->PstAreaId,
								pomViewer->GetGroup(imGroupNo)->Text);
			olDlg.DoModal();
		}
	}
	else if (lpDropEffect == DROPEFFECT_MOVE)
	{
		// create one or many flight managers dragged from pre-planning.
		// ogBasicData.GetPool() returns the pool assigned to the gate group
		// - there are more than one pools then the user will be asked to select
		// one of them
		CDWordArray olShiftUrnos;
		for (int ilIndex = 0; ilIndex < popDragDropCtrl->GetDataCount(); ilIndex++)
			olShiftUrnos.Add(popDragDropCtrl->GetDataDWord(ilIndex));
		CString olPstArea = pomViewer->GetGroup(imGroupNo)->PstAreaId;
		CString olPool = ogBasicData.GetPool(this, ALLOCUNITTYPE_PSTGROUP, olPstArea);
		if(!olPool.IsEmpty())
		{
			ogDataSet.CreateJobPool(this, olShiftUrnos, olPool, olPstArea);
		}
	}

	return 0L;
}
