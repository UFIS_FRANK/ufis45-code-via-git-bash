// LoadTimeSpanDlg.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <LoadTimeSpanDlg.h>
#include <BasicData.h>
#include <CedaTplData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoadTimeSpanDlg dialog


CLoadTimeSpanDlg::CLoadTimeSpanDlg(CWnd* pParent ,CStringArray &ropValues,bool bpAbsFlag, bool bpAllFieldsDisabled)
	: CDialog(CLoadTimeSpanDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLoadTimeSpanDlg)
	m_AbsTimeVal = -1;
	//}}AFX_DATA_INIT

	omValues.RemoveAll();
	for(int i = 0; i < ropValues.GetSize(); i++)
	{
		omValues.Add(ropValues[i]);
	}
	m_AbsTimeVal = bpAbsFlag ? 1 : 0;
	omStartTime = TIMENULL;
	omEndTime = TIMENULL;
	bmAllFieldsDisabled = bpAllFieldsDisabled;

}

CLoadTimeSpanDlg::~CLoadTimeSpanDlg()
{	
	omValues.RemoveAll();
}

void CLoadTimeSpanDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLoadTimeSpanDlg)
	DDX_Control(pDX, IDC_TOTIME, m_ToTime);
	DDX_Control(pDX, IDC_TODATE, m_ToDate);
	DDX_Control(pDX, IDC_RELTO, m_RelTo);
	DDX_Control(pDX, IDC_FROMTIME, m_FromTime);
	DDX_Control(pDX, IDC_RELFROM, m_RelFrom);
	DDX_Control(pDX, IDC_FROMDATE, m_FromDate);
	DDX_Radio(pDX, IDC_ABSTIME, m_AbsTimeVal);
	DDX_Control(pDX, IDC_LIST1, m_PossibleList);
	DDX_Control(pDX, IDC_LIST2, m_SelectedList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLoadTimeSpanDlg, CDialog)
	//{{AFX_MSG_MAP(CLoadTimeSpanDlg)
	ON_BN_CLICKED(IDC_ABSTIME, OnAbstime)
	ON_BN_CLICKED(IDC_RELTIME, OnReltime)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DEBUGINFO, OnDebugInfo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLoadTimeSpanDlg message handlers
BOOL CLoadTimeSpanDlg::GetData()
{
	omValues.RemoveAll();
	CStringArray olTmpValues;
	if(GetData(olTmpValues) == FALSE)
	{
		return FALSE;
	}
	for(int i = 0; i < olTmpValues.GetSize(); i++)
	{
		CString olTmp = olTmpValues[i];
		omValues.Add(olTmpValues[i]);
	}
	return TRUE;
}
BOOL CLoadTimeSpanDlg::GetData(CStringArray &ropValues)
{
	CString olT1, olT2, olT3, olT4;
	CString olResult;
	CTime olDate, olTime;
	m_FromDate.GetWindowText(olT1);// TIFA From || TIFD From; Date
	m_FromTime.GetWindowText(olT2);
	m_ToDate.GetWindowText(olT3);
	m_ToTime.GetWindowText(olT4);
	//Erst mal einen precheck und was leer ist wird 
	//entsprechend gesetzt

	if(olT1.IsEmpty() && olT2.IsEmpty() && olT3.IsEmpty() && olT4.IsEmpty())
	{
		;
	}
	else
	{
		if(olT2.IsEmpty())
		{
			olT2 = CString("00:00");
			m_FromTime.SetWindowText(olT2);
		}
		if(olT4.IsEmpty())
		{
			olT4 = CString("23:59");
			m_ToTime.SetWindowText(olT4);
		}
		if(olT1.IsEmpty() && olT3.IsEmpty())
		{
			olT1 = CTime::GetCurrentTime().Format("%d.%m.%Y");
			olT3 = olT1;
			m_FromDate.SetWindowText(olT1);
			m_ToDate.SetWindowText(olT3);
		}
		if(!olT1.IsEmpty() && olT3.IsEmpty())
		{
			olT3 = olT1;
			m_ToDate.SetWindowText(olT3);
		}
		if(olT1.IsEmpty() && !olT3.IsEmpty())
		{
			olT1 = olT3;
			m_FromDate.SetWindowText(olT1);
		}
	}
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateStringToDate(olT1);
		if(olDate != TIMENULL)
			olT1 = olDate.Format("%Y%m%d");
		else
		{
			CString olText;
			olText = GetString(IDS_STRING61346); // invalid date
			MessageBox(olText, "", MB_OK);
			return FALSE;//olT1 = " ";
		}
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(olT2, olDate);
			if(olTime != TIMENULL)
				olT1 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
			{
				CString olText;
				olText = GetString(IDS_STRING61346); // invalid date
				MessageBox(olText, "", MB_OK);
				return FALSE;//olT1 = " ";
			}
		}
	}
	else
	{
		olT1 = " ";
		olDate = TIMENULL;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

    m_ToDate.GetWindowText(olT1);// TIFA To || TIFD To; Date
	m_ToTime.GetWindowText(olT2);
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateStringToDate(olT1);
		if(olDate != TIMENULL)
			olT1 = olDate.Format("%Y%m%d");
		else
		{
			CString olText;
			olText = GetString(IDS_STRING61346);// invalid date
			MessageBox(olText, "", MB_OK);
			return FALSE;//olT1 = " ";
		}
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(olT2, olDate);
			if(olTime != TIMENULL)
				olT1 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
			{
				CString olText;
				olText = GetString(IDS_STRING61346); // invalid date
				MessageBox(olText, "", MB_OK);
				return FALSE;//olT1 = " ";
			}
		}
	}
	else
	{
		olT1 = " ";
		olDate = TIMENULL;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_RelFrom.GetWindowText(olT1);// RelHBefore
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_RelTo.GetWindowText(olT1);// RelHAfter
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	return true;
}

void CLoadTimeSpanDlg::SetData()
{
	SetData(omValues);
}

void CLoadTimeSpanDlg::SetData(CStringArray &ropValues)
{
	m_FromDate.SetWindowText("");// TIFA From || TIFD From; Date
    m_FromTime.SetWindowText("");// TIFA From || TIFD From; Time
    m_ToDate.SetWindowText("");// TIFA To || TIFD To; Date
    m_ToTime.SetWindowText("");// TIFA To || TIFD To; Time
    m_RelFrom.SetWindowText("");// RelHBefore
    m_RelTo.SetWindowText("");// RelHAfter
 	bool blFlag = false;
	for(int i = 0; i < ropValues.GetSize(); i++)
	{
		CTime olDate = TIMENULL;
		CString olV = ropValues[i];
		if(!olV.IsEmpty())
			blFlag = true;
		switch(i)
		{
		case 0:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				m_FromDate.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_FromTime.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_FromDate.SetWindowText(CString(""));
				m_FromTime.SetWindowText(CString(""));
			}
			break;
		case 1:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				m_ToDate.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_ToTime.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_ToDate.SetWindowText(CString(""));
				m_ToTime.SetWindowText(CString(""));
			}
			break;
		case 2:
			if(ropValues[i] != CString(" "))
				m_RelFrom.SetWindowText(ropValues[i]);
			break;
		case 3:
			if(ropValues[i] != CString(" "))
				m_RelTo.SetWindowText(ropValues[i]);
			break;

		}
	}
}

BOOL CLoadTimeSpanDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CWnd *polWnd = GetDlgItem(IDCANCEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}
	polWnd = GetDlgItem(IDOK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDC_ZEITRAUMVON);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32989));
	}
	polWnd = GetDlgItem(IDC_BIS);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32990));
	}
	polWnd = GetDlgItem(IDC_ZEITRAUMREL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32991));
	}
	polWnd = GetDlgItem(IDC_STDVOR);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32992));
	}
	polWnd = GetDlgItem(IDC_SDTNACH);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32993));
	}

	if((polWnd = GetDlgItem(IDC_DEBUGINFO)) != NULL)
	{
		if(!ogBasicData.DisplayDebugInfo())
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}


	if(bmAllFieldsDisabled)
	{
		if((polWnd = GetDlgItem(IDC_ABSTIME)) != NULL)
		{
			polWnd->EnableWindow(FALSE);
		}
		if((polWnd = GetDlgItem(IDC_RELTIME)) != NULL)
		{
			polWnd->EnableWindow(FALSE);
		}
		if((polWnd = GetDlgItem(IDC_FROMDATE)) != NULL)
		{
			polWnd->EnableWindow(FALSE);
		}
		if((polWnd = GetDlgItem(IDC_FROMTIME)) != NULL)
		{
			polWnd->EnableWindow(FALSE);
		}
		if((polWnd = GetDlgItem(IDC_TODATE)) != NULL)
		{
			polWnd->EnableWindow(FALSE);
		}
		if((polWnd = GetDlgItem(IDC_TOTIME)) != NULL)
		{
			polWnd->EnableWindow(FALSE);
		}
		if((polWnd = GetDlgItem(IDC_RELFROM)) != NULL)
		{
			polWnd->EnableWindow(FALSE);
		}
		if((polWnd = GetDlgItem(IDC_RELTO)) != NULL)
		{
			polWnd->EnableWindow(FALSE);
		}
	}


	SetWindowText(GetString(IDS_STRING32994));

	SetData();
	
	if(m_AbsTimeVal == 0)
	{
		OnAbstime();
	}
	else
	{
		OnReltime();
	}

	char pclConfigPath[256];
	char pclTemplateFilter[512];
	char pclTemplateCopy[512];

    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(pcgAppName, "TEMPLATEFILTER", "ALL",pclTemplateFilter, sizeof pclTemplateFilter, pclConfigPath);
	
	bool blUseAllTemplates = stricmp(pclTemplateFilter,"ALL") == 0;

	if (ogTplData.omData.GetSize() == 0)
		ogTplData.ReadTplData();	

	for (int ilC = 0; ilC < ogTplData.omData.GetSize(); ilC++)
	{
		TPLDATA *polTplData = &ogTplData.omData[ilC];
		if (!polTplData)
			continue;
		if (polTplData->Tpst[0] == '0')
			continue;

		if (blUseAllTemplates)
		{
				int ind = m_SelectedList.FindStringExact(-1,polTplData->Tnam);
				if (ind < 0)
				{
					ind = m_SelectedList.AddString(polTplData->Tnam);
					if (ind < 0)
						continue;
					m_SelectedList.SetItemData(ind,ilC);
				}
		}
		else 
		{
			bool blSelected = false;
			strcpy(pclTemplateCopy,pclTemplateFilter);
			char *pclToken  = strtok(pclTemplateCopy,",");
			while (pclToken)
			{
				if (stricmp(pclToken,polTplData->Tnam) == 0)
				{
					blSelected = true;
					break;
				}

				pclToken = strtok(NULL,",");
			}

			if (blSelected)
			{
				int ind = m_SelectedList.FindStringExact(-1,polTplData->Tnam);
				if (ind < 0)
				{
					ind = m_SelectedList.AddString(polTplData->Tnam);
					if (ind < 0)
						continue;
					m_SelectedList.SetItemData(ind,ilC);
				}
			}
			else
			{
				int ind = m_PossibleList.FindStringExact(-1,polTplData->Tnam);
				if (ind < 0)
				{
					ind = m_PossibleList.AddString(polTplData->Tnam);
					if (ind < 0)
						continue;
					m_PossibleList.SetItemData(ind,ilC);
				}
			}
		}
	}
	CWnd *pWnd = GetDlgItem(IDOK);
	if (pWnd)
	{
		if (m_SelectedList.GetCount() > 0)
			pWnd->EnableWindow(TRUE);
		else
			pWnd->EnableWindow(FALSE);
	}

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLoadTimeSpanDlg::OnOK() 
{
	if(GetData())
	{
		UpdateData(TRUE);
		omStartTime = CTime::GetCurrentTime() - CTimeSpan(0,1,0,0);
		omEndTime = CTime::GetCurrentTime() + CTimeSpan(0,1,0,0);
		for( int i = 0; i < omValues.GetSize(); i++)
		{
			CString olTmpVal = omValues[i];
			if(omValues[i] != CString("") && omValues[i] != CString(" "))
			{
				switch (i)
				{
				case 0: // Zeitraum absolut von
					{
						if(m_AbsTimeVal == 0)
						{
							omStartTime = DBStringToDateTime(omValues[i]);
							omEndTime = DBStringToDateTime(omValues[i+1]);
						}					
					}
					break;
				case 2: //Rel. Stunden vor akt. Zeit ==> ist eigentlich tifa und tifd
					{
						if(m_AbsTimeVal == 1)
						{
							CTime olTime = CTime::GetCurrentTime();
							char pclHours[10]="";
							sprintf(pclHours, "%s", omValues[i]);

							olTime -= CTimeSpan((atoi(pclHours)*60*60));
							omStartTime = olTime;
						}
					}
					break;
				case 3: //Rel. Stunden nach akt. Zeit
					{
						if(m_AbsTimeVal == 1)
						{
							CTime olTime = CTime::GetCurrentTime();
							char pclHours[10]="";
							sprintf(pclHours, "%s", omValues[i]);

							olTime += CTimeSpan((atoi(pclHours)*60*60));
							omEndTime = olTime;
						}
					}
					break;
				}
			}
		}

		if(omEndTime <= omStartTime)
		{
			MessageBox(GetString(IDS_STRING61584), GetString(IDS_STRING32994), MB_OK);
		}
		else
		{
			for (i = 0; i < ogTplData.omData.GetSize(); i++)
			{
				TPLDATA *polTplData = &ogTplData.omData[i];
				if (!polTplData)
					continue;
				if (polTplData->Tpst[0] == '0')
					continue;

				if (m_SelectedList.FindStringExact(-1,polTplData->Tnam) < 0)
					continue;

				omTplUrnos.Add(polTplData->Urno);
			}

			// template was selected OR "You have not selected a template, do you want to continue?"
			if(omTplUrnos.GetSize() > 0 ||
				MessageBox(GetString(IDS_NOTEMPLATESELECTED),GetString(IDS_STRING32994),MB_ICONQUESTION|MB_YESNO) == IDYES)
			{
				CDialog::OnOK();
			}
		}
	}
}


void CLoadTimeSpanDlg::OnAbstime() 
{
	if(!bmAllFieldsDisabled)
	{
		m_ToTime.EnableWindow(TRUE);	
		m_ToDate.EnableWindow(TRUE);	
		m_FromDate.EnableWindow(TRUE);	
		m_FromTime.EnableWindow(TRUE);	
		m_RelFrom.EnableWindow(FALSE);	
		m_RelTo.EnableWindow(FALSE);	
	}
}

void CLoadTimeSpanDlg::OnReltime() 
{
	if(!bmAllFieldsDisabled)
	{
		m_ToTime.EnableWindow(FALSE);	
		m_ToDate.EnableWindow(FALSE);	
		m_FromDate.EnableWindow(FALSE);	
		m_FromTime.EnableWindow(FALSE);	
		m_RelFrom.EnableWindow(TRUE);	
		m_RelTo.EnableWindow(TRUE);	
	}
}

void CLoadTimeSpanDlg::OnRemove() 
{
	int ilnC = m_SelectedList.GetSelCount();
	if (ilnC > 0)
	{
		int	*ilSelItems	= new int[ilnC];
		m_SelectedList.GetSelItems(ilnC,ilSelItems);
		for (int ilC = ilnC-1; ilC >= 0; ilC--)
		{
			CString olName;
			m_SelectedList.GetText(ilSelItems[ilC],olName);			
			int ind = m_PossibleList.AddString(olName);
			m_PossibleList.SetItemData(ind,m_SelectedList.GetItemData(ilSelItems[ilC]));
			m_SelectedList.DeleteString(ilSelItems[ilC]);
		}
		delete [] ilSelItems;
	} 

	CWnd *pWnd = GetDlgItem(IDOK);
	if (pWnd)
	{
		if (m_SelectedList.GetCount() > 0)
			pWnd->EnableWindow(TRUE);
		else
			pWnd->EnableWindow(FALSE);
	}
}

void CLoadTimeSpanDlg::OnAdd() 
{
	int ilnC = m_PossibleList.GetSelCount();
	if (ilnC > 0)
	{
		int	*ilSelItems	= new int[ilnC];
		m_PossibleList.GetSelItems(ilnC,ilSelItems);
		for (int ilC = ilnC-1; ilC >= 0; ilC--)
		{
			CString olName;
			m_PossibleList.GetText(ilSelItems[ilC],olName);			
			int ind = m_SelectedList.AddString(olName);
			m_SelectedList.SetItemData(ind,m_PossibleList.GetItemData(ilSelItems[ilC]));
			m_PossibleList.DeleteString(ilSelItems[ilC]);
		}
		delete [] ilSelItems;
	}

	CWnd *pWnd = GetDlgItem(IDOK);
	if (pWnd)
	{
		if (m_SelectedList.GetCount() > 0)
			pWnd->EnableWindow(TRUE);
		else
			pWnd->EnableWindow(FALSE);
	}
}


void CLoadTimeSpanDlg::OnDebugInfo() 
{
	if(MessageBox("This function counts jobs, shifts and demands for each day and displays the results in a list.\nThis will take a minute, do you want to continue?","Display Debug Info",MB_ICONASTERISK|MB_YESNO) == IDYES)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		ogBasicData.GetDebugData();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}
