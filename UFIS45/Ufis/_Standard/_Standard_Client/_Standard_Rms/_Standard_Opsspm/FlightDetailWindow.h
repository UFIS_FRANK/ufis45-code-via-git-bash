// FlightDetailWindow.h : header file
//


#ifndef _FLIGHTDW_
#define _FLIGHTDW_

#include <FlightDetailDlg.h>
#include <FlightDetailViewer.h>
#include <FlightDetailGantt.h>
#include <CedaAcrData.h>
#include <CedaDemandData.h>

#define FlightData  FLIGHTDATA
#define FDD_KEY		"FlightDetDlg"
#define FDD_SCREEN_HEIGHT "ScreenHeight"

/////////////////////////////////////////////////////////////////////////////
// FlightDetailWindow frame
class FlightDetailWindow : public CFrameWnd
{
	DECLARE_DYNCREATE(FlightDetailWindow)

protected:
    FlightDetailWindow();

// Attributes
public:

// Operations
public:
	FlightDetailWindow(CWnd* popParent, long lpFlightPrimaryKey, long lpFlightSecondaryKey, BOOL bpShadowBar = FALSE,
						const char *pcpAllocUnitType="", ACRDATA *prpAcr = NULL, int ipDemandType = ALLDEMANDS, 
						CTime opMarkTimeStart = TIMENULL, CMapStringToPtr* prpMapFunctionCode = NULL, CMapStringToPtr* prpMapEquDemandGroups = NULL);
	virtual ~FlightDetailWindow();

	void ProcessRefresh();
	void ProcessFlightDelete(long lpFlightUrno);
	void CheckFlightPtrs(FLIGHTDATA *prpArr, FLIGHTDATA *prpDep);
	void SetFlightPtrs();
	void Initialize(void);
	void OnSelchangeAllocTypes();
	CString GetCurrentAllocType();
	bool HasTurnaroundDemands();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FlightDetailWindow)
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(FlightDetailWindow)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnDestroy();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
    long lmFlightPrimaryKey;
	long lmFlightSecondaryKey;

	CString omCaption;
	void SetCaption();

    FLIGHTDATA	*pomAF;
    FLIGHTDATA	*pomDF;
	ACRDATA		*prmAcr;

	CCSPtrArray <DEMANDDATA> omDemands;
	CCSPtrArray <JOBDATA> omJobs;
	CCSPtrArray <JOBDATA> omJobsWithoutDemands;

	CTime omMinStartTime;
	CTime omMaxEndTime;

	int imDemandType; // { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
	int RemoveJobsWithIncorrectType(CCSPtrArray <JOBDATA> &ropJobs, int ipDemandType);

	CString omAllocUnitType;

    FlightDetailDialog omFlightDetail;
    CTimeScale omTimeScale;
    CStatusBar omStatusBar;
    BOOL bmSelchange;
    
    int imStartTimeScalePos;
    int imBottomPos;

    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
	void SetGanttTimes(CTime &ropTSStartTime, CTimeSpan &ropTSDuration);
	void UpdateGanttChart(int ipType, int ipLine, int ipBar);
	void GetDisplayedDemands(CCSPtrArray <DEMANDDATA> &ropDemands);
	void GetDisplayedJobsWithoutDemands(CCSPtrArray <JOBDATA> &ropJobsWithoutDemands);
	
	FlightDetailViewer	omViewer;
	CStringArray		omAllocTypes;
	BOOL				bmIsShadowBar;
public:
	FlightDetailGantt omGantt;

public:
	bool bmScrolling;
	int  imScrollSpeed;
	void AutoScroll(UINT ipInitialScrollSpeed = 200);
	void OnAutoScroll(void);
    CCSDragDropCtrl m_FlightDetailWindowDragDrop;

protected:
    CTime omStartTime;
    CTimeSpan omDuration;
    CTime omClickedTime; // point (in time) which was clicked on the equipment chart - used to centre the display time
    int imDurationSecs; // duration of the displayed data in seconds

private:      
    static CPoint omMaxTrackSize;
    static CPoint omMinTrackSize;
	bool bmInitialized;
	int imScreenHeight, imPrevScreenHeight;
	CMapStringToPtr* pomMapFunctionCode; //PRF 8999
	CMapStringToPtr* pomMapEquDemandGropus;

public:
    CTime omMarkTimeStart;
    int imUpdatedLine;
    int imPrevScrollPos;
    void OnHorizScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    long CalcTotalMinutes(void);
    void SetTSStartTime(CTime opNewTsStartTime);

};

/////////////////////////////////////////////////////////////////////////////

#endif // _FLIGHTDW_

