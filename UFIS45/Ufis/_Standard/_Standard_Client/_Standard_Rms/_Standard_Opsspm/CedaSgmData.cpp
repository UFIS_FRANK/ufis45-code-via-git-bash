// CedaSgmData.cpp - Class Group Members (META)
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaSgmData.h>
#include <CedaGatData.h>
#include <CedaCicData.h>
#include <BasicData.h>

void ProcessSgmCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaSgmData::CedaSgmData()
{                  
    BEGIN_CEDARECINFO(SGMDATA, SgmDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Usgr,"USGR")
		FIELD_LONG(Uval,"UVAL")
		FIELD_CHAR_TRIM(Tabn,"TABN")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(SgmDataRecInfo)/sizeof(SgmDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SgmDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogCCSDdx.Register((void *)this, BC_SGM_INSERT, CString("SGMDATA"), CString("Sgm changed"),ProcessSgmCf);
	ogCCSDdx.Register((void *)this, BC_SGM_UPDATE, CString("SGMDATA"), CString("Sgm changed"),ProcessSgmCf);
	ogCCSDdx.Register((void *)this, BC_SGM_DELETE, CString("SGMDATA"), CString("Sgm deleted"),ProcessSgmCf);

    // Initialize table names and field names
    strcpy(pcmTableName,"SGMTAB");
    pcmFieldList = "URNO,USGR,UVAL,TABN";
}


void ProcessSgmCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaSgmData *)popInstance)->ProcessSgmBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaSgmData::ProcessSgmBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSgmData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlSgmData);
	SGMDATA rlSgm, *prlSgm = NULL;
	long llUrno = GetUrnoFromSelection(prlSgmData->Selection);
	GetRecordFromItemList(&rlSgm,prlSgmData->Fields,prlSgmData->Data);
	if(llUrno == 0L) llUrno = rlSgm.Urno;

	switch(ipDDXType)
	{
		case BC_SGM_INSERT:
		{
			if((prlSgm = AddSgmInternal(rlSgm)) != NULL)
			{
				ogCCSDdx.DataChanged((void *)this, SGM_INSERT, (void *)prlSgm);
			}
			break;
		}
		case BC_SGM_UPDATE:
		{
			if((prlSgm = GetSgmByUrno(llUrno)) != NULL)
			{
				GetRecordFromItemList(prlSgm,prlSgmData->Fields,prlSgmData->Data);
				//PrepareDataAfterRead(prlSgm);
				ogCCSDdx.DataChanged((void *)this, SGM_UPDATE, (void *)prlSgm);
			}
			break;
		}
		case BC_SGM_DELETE:
		{
			if((prlSgm = GetSgmByUrno(llUrno)) != NULL)
			{
				DeleteSgmInternal(prlSgm->Urno);
				ogCCSDdx.DataChanged((void *)this, SGM_DELETE, (void *)prlSgm);
			}
			break;
		}
	}
}
 
CedaSgmData::~CedaSgmData()
{
	TRACE("CedaSgmData::~CedaSgmData called\n");
	ClearAll();
}

void CedaSgmData::ClearAll()
{
	CMapPtrToPtr *polSingleMap;
	long llUrno;
	for(POSITION rlPos =  omUsgrMap.GetStartPosition(); rlPos != NULL; )
	{
		omUsgrMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUsgrMap.RemoveAll();
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

bool CedaSgmData::ReadSgmData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

    char pclWhere[512] = "";
	char pclCom[10] = "RT";
    if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaSgmData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		SGMDATA rlSgmData;
		for(int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlSgmData)) == RCSuccess)
			{
				AddSgmInternal(rlSgmData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaSgmData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(SGMDATA), pclWhere);
    return ilRc;
}

SGMDATA *CedaSgmData::AddSgmInternal(SGMDATA &rrpSgm)
{
	//TRACE("SGM URNO %d UVAL %d USGR %d\n",rrpSgm.Urno,rrpSgm.Uval,rrpSgm.Usgr);
	SGMDATA *prlSgm = new SGMDATA;
	*prlSgm = rrpSgm;
	omData.Add(prlSgm);
	omUrnoMap.SetAt((void *)prlSgm->Urno,prlSgm);

	CMapPtrToPtr *polSingleMap;
	if(omUsgrMap.Lookup((void *) prlSgm->Usgr, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prlSgm->Urno,prlSgm);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prlSgm->Urno,prlSgm);
		omUsgrMap.SetAt((void *)prlSgm->Usgr,polSingleMap);
	}

	return prlSgm;
}

void CedaSgmData::DeleteSgmInternal(long lpUrno)
{
	int ilNumSgms = omData.GetSize();
	for(int ilSgm = (ilNumSgms-1); ilSgm >= 0; ilSgm--)
	{
		if(omData[ilSgm].Urno == lpUrno)
		{
			CMapPtrToPtr *polSingleMap;
			if (omUsgrMap.Lookup((void *) omData[ilSgm].Usgr, (void *&) polSingleMap) == TRUE)
			{
				polSingleMap->RemoveKey((void * ) lpUrno);
			}

			omData.DeleteAt(ilSgm);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

SGMDATA *CedaSgmData::GetSgmByUrno(long lpUrno)
{
	SGMDATA *prlSgm = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlSgm);
	return prlSgm;
}

void CedaSgmData::GetSgmListByUsgr(long lpUsgr, CCSPtrArray <SGMDATA> &ropSgmList, bool bpReset /*true*/)
{
	if(bpReset)
	{
	    ropSgmList.RemoveAll();
	}

	CMapPtrToPtr *polSingleMap;
	long llUrno;
	SGMDATA *prlSgm;

	if(omUsgrMap.Lookup((void *)lpUsgr,(void *& )polSingleMap))
	{
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlSgm);
			ropSgmList.Add(prlSgm);
		}
	}
}

void CedaSgmData::GetSgmListByUval(long lpUval, CCSPtrArray <SGMDATA> &ropSgmList, bool bpReset /*true*/)
{
	if(bpReset)
	{
	    ropSgmList.RemoveAll();
	}
	int ilNumSgm = omData.GetSize();
	for(int ilSgm = 0; ilSgm < ilNumSgm; ilSgm++)
	{
		if(omData[ilSgm].Uval == lpUval)
		{
			ropSgmList.Add(&omData[ilSgm]);
		}
	}
}


CString CedaSgmData::GetTableName(void)
{
	return CString(pcmTableName);
}