// CedaPaxData.h - Paxding Table - Contains suplimentary data about flights
// eg num passengers, baggage handling info
#ifndef _CedaPaxData_H_
#define _CedaPaxData_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CedaFlightData.h>

#define PAX_TOTAL		0
#define PAX_FIRST		1
#define PAX_BUSINESS	2
#define PAX_ECONOMY		3
#define	PAX_PREMIUM		4

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration
// Name                            Null?    Type
// ------------------------------- -------- ----
// AFTU                                     CHAR(10)
// FDCS										CHAR(1)
// FIPD										CHAR(1)
// FKEY                                     CHAR(18)
// HOPO                                     CHAR(3)
// NOSC                                     CHAR(3)
// NOSF                                     CHAR(3)
// NOSY                                     CHAR(3)
// NOSP                                     CHAR(3)
// PAXT                                     CHAR(3)
// PBLC                                     CHAR(3)
// PBLF                                     CHAR(3)
// PBLY                                     CHAR(3)
// PBLP                                     CHAR(3)
// PBTC                                     CHAR(3)
// PBTF                                     CHAR(3)
// PBTY                                     CHAR(3)
// PBTP                                     CHAR(3)
// PCLC                                     CHAR(3)
// PCLF                                     CHAR(3)
// PCLY                                     CHAR(3)
// PCLP                                     CHAR(3)
// PCTC                                     CHAR(3)
// PCTF                                     CHAR(3)
// PCTY                                     CHAR(3)
// PCTP                                     CHAR(3)
// URNO                            NOT NULL CHAR(10)

struct PaxDataStruct
{
	long	Urno;		// Unique Record Number
	long	Aftu;		// URNO of flight in AFTTAB
	char	Fkey[19];	// flightNum + alc3 + '#' + flight day (yyyymmdd) + adid -> eg. "00721LAV#20000731D"

	char	Fdcs[2];
	char	Fipd[2];
	char	Hopo[4];

	int		Nosp;	// Number of seats Premium
	int		Nosf;	// Number of seats First
	int		Nosc;	// Number of seats Business
	int		Nosy;	// Number of seats Economy
			
	int		Pblp;	// Passengers booked local Premium
	int		Pblf;	// Passengers booked local First
	int		Pblc;	// Passengers booked local Business
	int		Pbly;	// Passengers booked local Economy
			
	int		Pbtp;	// Passengers booked transfer Premium
	int		Pbtf;	// Passengers booked transfer First
	int		Pbtc;	// Passengers booked transfer Business
	int		Pbty;	// Passengers booked transfer Economy
			
	int		Pclp;	// Passengers checked-in local Premium
	int		Pclf;	// Passengers checked-in local First
	int		Pclc;	// Passengers checked-in local Business
	int		Pcly;	// Passengers checked-in local Economy
			
	int		Pctp;	// Passengers checked-in transfer Premium
	int		Pctf;	// Passengers checked-in transfer First
	int		Pctc;	// Passengers checked-in transfer Business
	int		Pcty;	// Passengers checked-in transfer Economy

	int		Paxt;	// Passengers totally booked + checked-in

	int		Prdp;	// Passengers forcast Premium
	int		Prdf;	// Passengers forcast First
	int		Prdc;	// Passengers forcast Business
	int		Prdy;	// Passengers forcast Economy

    char  Usec[33];	// Creator
    CTime Cdat;		// Creation Date
    char  Useu[33];	// Updater
    CTime Lstu;		// Update Date

	// non DB data
	enum State
	{
		NEW,
		CHANGED,
		DELETED,
		UNCHANGED
	};

	State emState;	

	PaxDataStruct(void)
	{
		Urno	= 0L;
		Aftu	= 0L;
		strcpy(Fkey,"");
		strcpy(Fdcs," ");
		strcpy(Fipd," ");
		strcpy(Hopo,pcgHomeAirport);
		Nosp	= 0;
		Nosf	= 0;
		Nosc	= 0;
		Nosy	= 0;

		Pblp	= 0;
		Pblf	= 0;
		Pblc	= 0;
		Pbly	= 0;

		Pbtp	= 0;
		Pbtf	= 0;
		Pbtc	= 0;
		Pbty	= 0;

		Pclp	= 0;
		Pclf	= 0;
		Pclc	= 0;
		Pcly	= 0;

		Pctp	= 0;
		Pctf	= 0;
		Pctc	= 0;
		Pcty	= 0;

		Paxt	= 0;

		Prdp	= 0;
		Prdf	= 0;
		Prdc	= 0;
		Prdy	= 0;

		strcpy(Usec," ");
		Cdat	= TIMENULL;
		strcpy(Useu," ");
		Lstu	= TIMENULL;
		emState = NEW;
	}

};

typedef struct PaxDataStruct PAXDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaPaxData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <PAXDATA>	omData;
	CMapPtrToPtr			omUrnoMap;
	CMapPtrToPtr			omAftuMap; // key is AFT.URNO contains a submap with key "<TYPE><SSTP><STYP>"
	CString GetTableName(void);

// Operations
public:
	CedaPaxData();
	~CedaPaxData();

	bool			ReadPaxData();
	PAXDATA*		GetPaxByUrno(long lpUrno);
	PAXDATA*		GetPaxByAftu(long lpAftu);
	void			ProcessPaxBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	int				GetBookedPassengers(long lpAftu, int ipClass = PAX_TOTAL);
	int				GetBookedPassengers(PAXDATA *prpPax, int ipClass = PAX_TOTAL);
	void			GetBookedPassengers(long lpAftu, int &ripFirst, int &ripBusiness, int &ripEconomy, int &ripPremium, int &ripTotal);
	int				GetCheckedInPassengers(long lpAftu, int ipClass = PAX_TOTAL);
	int				GetCheckedInPassengers(PAXDATA *prpPax, int ipClass = PAX_TOTAL);
	void			GetCheckedInPassengers(long lpAftu, int &ripFirst, int &ripBusiness, int &ripEconomy, int &ripPremium, int &ripTotal);
	int				GetNumSeats(long lpAftu, int ipClass = PAX_TOTAL);
	int				GetNumSeats(PAXDATA *prpPax, int ipClass = PAX_TOTAL);
	bool			IsOverBooked(long lpAftu, int ipClass = PAX_TOTAL);
	CString			GetPaxString(long lpAftu);
	CString			DumpForFlight(long lpAftu);
	CString			Dump(long lpUrno);

	BOOL			InitNewPaxRecord(const FLIGHTDATA *prpFlight,PAXDATA& rrpData);
	BOOL			AddPaxRecord(const PAXDATA& rpData,BOOL bpSendDDX = TRUE);
	void			PrepareData(PAXDATA *prpPax);
	BOOL			UpdatePaxRecord(const PAXDATA& rpData,BOOL bpSendDDX = TRUE);
private:
	void			SendFlightChange(long lpAftu);
	PAXDATA*		AddPaxInternal(PAXDATA &rrpPax);
	void			DeletePaxInternal(long lpUrno);
	void			ClearAll();
	void			ConvertDatesToUtc(PAXDATA *prpPax);
	void			ConvertDatesToLocal(PAXDATA *prpPax);
	BOOL			SavePaxRecord(PAXDATA *prpPax,PAXDATA *prpOldPax = NULL);
	bool			CreationDateSupported();
};


extern CedaPaxData ogPaxData;
#endif _CedaPaxData_H_
