// DruckAuswahl.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <PstDiagramPrintSelection.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDruckAuswahl dialog


PstDiagramPrintSelection::PstDiagramPrintSelection(CWnd* pParent /*=NULL*/)
	: CDruckAuswahl(pParent)
{                                 
	//{{AFX_DATA_INIT(PstDiagramPrintSelection)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void PstDiagramPrintSelection::DoDataExchange(CDataExchange* pDX)
{
	CDruckAuswahl::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PstDiagramPrintSelection)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PstDiagramPrintSelection, CDruckAuswahl)
	//{{AFX_MSG_MAP(PstDiagramPrintSelection)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP() 

/////////////////////////////////////////////////////////////////////////////
// CDruckAuswahl message handlers

void PstDiagramPrintSelection::OnOK() 
{
	CDruckAuswahl::OnOK();
}

BOOL PstDiagramPrintSelection::OnInitDialog() 
{
	CDruckAuswahl::OnInitDialog();

	SetWindowText(GetString(IDS_STRING61911));

	CWnd *polWnd = GetDlgItem(IDC_GATES); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61912));
	}
	polWnd = GetDlgItem(IDC_FM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32922));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}

	CButton *polRadioButton = (CButton *) GetDlgItem(IDC_GATES);
	if (polRadioButton != NULL)
	{
		polRadioButton->SetCheck(TRUE);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
