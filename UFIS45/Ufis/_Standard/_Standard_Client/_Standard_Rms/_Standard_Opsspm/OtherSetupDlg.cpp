// OtherSetupDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <OtherSetupDlg.h>
#include <BasicData.h>
#include <CCSGlobl.h>
#include <DlgSettings.h>
#include <CedaPfcData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COtherSetupDlg dialog


COtherSetupDlg::COtherSetupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COtherSetupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COtherSetupDlg)
	m_DisplayDiffFuncSymbol = FALSE;
	m_FlightJobsExcelFunctions = _T("");
	imBreakBuffer = 0;
	//}}AFX_DATA_INIT

	lmDiffFuncColour = GRAY;
}


void COtherSetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COtherSetupDlg)
	DDX_Control(pDX, IDC_FLIGHTJOBS_EXCEL_TITLE1, m_FlightJobsExcelTitle1Ctrl);
	DDX_Control(pDX, IDC_DIFFFUNCTEXT, m_DiffFuncTextCtrl);
	DDX_Control(pDX, IDC_DIFFFUNCCOLOUR, m_DiffFuncColourCtrl);
	DDX_Control(pDX, IDC_DIFFFUNC, m_DiffFuncCtrl);
	DDX_Check(pDX, IDC_DIFFFUNC, m_DisplayDiffFuncSymbol);
	DDX_Text(pDX, IDC_FLIGHTJOBS_EXCEL, m_FlightJobsExcelFunctions);
	DDX_Control(pDX, IDC_FLIGHTJOBS_EXCEL_TITLE2, m_FlightJobsExcelTitle2Ctrl);
	DDX_Control(pDX, IDC_FLIGHTJOBS_EXCEL, m_FlightJobsExcelFunctionsCtrl);
	DDX_Text(pDX, IDC_EDIT_DEFAULT_BREAK_BUFFER, imBreakBuffer);
	DDV_MinMaxInt(pDX, imBreakBuffer, -60, 60);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COtherSetupDlg, CDialog)
	//{{AFX_MSG_MAP(COtherSetupDlg)
	ON_BN_CLICKED(IDC_DIFFFUNCCOLOUR, OnSetDiffFuncColour)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COtherSetupDlg message handlers

BOOL COtherSetupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_OTHERSETUPDLGTITLE));
	m_DiffFuncCtrl.SetWindowText(GetString(IDS_DIFFFUNCTEXT1));
	m_DiffFuncTextCtrl.SetWindowText(GetString(IDS_DIFFFUNCTEXT2));
	CString olTitle;
	olTitle.Format(GetString(IDS_FLIGHTJOBSEXCELTITLE2), GetString(IDS_DEFAULTEXCELFUNCTIONS));
	m_FlightJobsExcelTitle1Ctrl.SetWindowText(GetString(IDS_FLIGHTJOBSEXCELTITLE1));
	m_FlightJobsExcelTitle2Ctrl.SetWindowText(olTitle);

	CString olColour;
	if(ogDlgSettings.GetValue("DiffFuncColour", "DiffFuncColour", olColour))
	{
		lmDiffFuncColour = (COLORREF) atol(olColour);
		m_DisplayDiffFuncSymbol = TRUE;
	}
	else
	{
		lmDiffFuncColour = GRAY;
		m_DisplayDiffFuncSymbol = FALSE;
	}
	m_DiffFuncColourCtrl.SetColors(lmDiffFuncColour,lmDiffFuncColour,lmDiffFuncColour);

	if(!ogDlgSettings.GetValue("FlightJobsTable", "ExcelFunctions", m_FlightJobsExcelFunctions))
	{
		m_FlightJobsExcelFunctions = GetString(IDS_DEFAULTEXCELFUNCTIONS); // "A1BL,A2BL;A1BR,A2BR;A1BE,A2BE;A1BA,A2BA";
	}
	//PRF5802: Initialise Default break buffer from ogBasicData
	imBreakBuffer = ogBasicData.imDefaultBreakBuffer;

     SetDlgItemText(IDC_STATIC_DEFAULT_BREAK_BUFFER,GetString(IDS_61974));

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void COtherSetupDlg::OnOK() 
{
	CString olOldFlightJobsExcelFunctions = m_FlightJobsExcelFunctions;
	if(UpdateData())
	{
		ogDlgSettings.EnableSaveToDb("DiffFuncColour");
		ogDlgSettings.DeleteSettingsByDialog("DiffFuncColour");

		if(m_DisplayDiffFuncSymbol)
		{
			CString olColour;
			olColour.Format("%ld", (long) lmDiffFuncColour);
			ogDlgSettings.AddValue("DiffFuncColour", "DiffFuncColour", olColour);
		}
		ogBasicData.InitDiffFuncColour();

		//PRF5802: if Default Break Buffer has been changed save it to ogBasicData and ogDlgSettings
		if ( imBreakBuffer != ogBasicData.imDefaultBreakBuffer )
		{
			CString olValue;
			ogDlgSettings.EnableSaveToDb("Defaults");
			olValue.Format ( "%d", imBreakBuffer );
			ogBasicData.imDefaultBreakBuffer = imBreakBuffer ;
			ogDlgSettings.AddValue("Defaults", "Break Buffer", olValue);
		}
		bool blOk = true;
		bool blHasChanged = (olOldFlightJobsExcelFunctions != m_FlightJobsExcelFunctions) ? true : false;

		if(blHasChanged && m_FlightJobsExcelFunctions.GetLength() > 0)
		{
			CStringArray olColumns;
			CStringArray olFunctions;
			ogBasicData.ExtractItemList(m_FlightJobsExcelFunctions,&olColumns,';');
			for(int ilC = 0; ilC < olColumns.GetSize(); ilC++)
			{
				ogBasicData.ExtractItemList(olColumns[ilC],&olFunctions,',');
				for(int ilF = 0; ilF < olFunctions.GetSize(); ilF++)
				{
					if(ogPfcData.GetPfcByFctc(olFunctions[ilF]) == NULL)
					{
						CString olErr;
						olErr.Format(GetString(IDS_FLIGHTJOBSEXCELERR), olFunctions[ilF]);
						MessageBox(olErr, GetString(IDS_FLIGHTJOBSEXCELTITLE1), MB_ICONERROR);
						m_FlightJobsExcelFunctionsCtrl.SetFocus();
						blOk = false;
						break;
					}
				}
			}
		}

		if(blOk)
		{
			if(blHasChanged)
			{
				ogDlgSettings.EnableSaveToDb("FlightJobsTable");
				ogDlgSettings.DeleteSettingsByDialog("FlightJobsTable");
				ogDlgSettings.AddValue("FlightJobsTable", "ExcelFunctions", m_FlightJobsExcelFunctions);
			}

			CDialog::OnOK();
		}
	}
}

void COtherSetupDlg::OnSetDiffFuncColour() 
{
	ChangeColour(m_DiffFuncColourCtrl, lmDiffFuncColour);
}

void COtherSetupDlg::ChangeColour(CCSButtonCtrl &ropButton, COLORREF &ropColour)
{
	CColorDialog olColorDlg(ropColour, 0 , this);

	if (olColorDlg.DoModal() == IDOK)
	{
		ropColour = olColorDlg.GetColor();
		ropButton.SetColors(ropColour,ropColour,ropColour);
		ropButton.Invalidate(TRUE);
	}
}
