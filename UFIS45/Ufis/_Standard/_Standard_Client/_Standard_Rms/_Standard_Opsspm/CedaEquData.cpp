// CedaEquData.cpp - Class for Equipment
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaEquData.h>
#include <BasicData.h>

void ProcessEquCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaEquData::CedaEquData()
{                  
    BEGIN_CEDARECINFO(EQUDATA, EquDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Enam,"ENAM")
		FIELD_CHAR_TRIM(Eqps,"EQPS")
		FIELD_CHAR_TRIM(Etyp,"ETYP")
		FIELD_CHAR_TRIM(Gcde,"GCDE")
		FIELD_LONG(Gkey,"GKEY")
		FIELD_CHAR_TRIM(Ivnr,"IVNR")
		FIELD_CHAR_TRIM(Rema,"REMA")
		FIELD_CHAR_TRIM(Crqu,"CRQU")
		FIELD_CHAR_TRIM(Tele,"TELE")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(EquDataRecInfo)/sizeof(EquDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&EquDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogCCSDdx.Register((void *)this, BC_EQU_INSERT, CString("EQUDATA"), CString("Equipment changed"),ProcessEquCf);
	ogCCSDdx.Register((void *)this, BC_EQU_UPDATE, CString("EQUDATA"), CString("Equipment changed"),ProcessEquCf);
	ogCCSDdx.Register((void *)this, BC_EQU_DELETE, CString("EQUDATA"), CString("Equipment deleted"),ProcessEquCf);

    // Initialize table names and field names
    strcpy(pcmTableName,"EQUTAB");
    pcmFieldList = "URNO,ENAM,EQPS,ETYP,GCDE,GKEY,IVNR,REMA,CRQU,TELE";
}


void ProcessEquCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaEquData *)popInstance)->ProcessEquBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaEquData::ProcessEquBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlEquData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlEquData);
	EQUDATA rlEqu, *prlEqu = NULL;
	long llUrno = GetUrnoFromSelection(prlEquData->Selection);
	GetRecordFromItemList(&rlEqu,prlEquData->Fields,prlEquData->Data);
	if(llUrno == 0L) llUrno = rlEqu.Urno;

	switch(ipDDXType)
	{
		case BC_EQU_INSERT:
		{
			if((prlEqu = AddEquInternal(rlEqu)) != NULL)
			{
				ogCCSDdx.DataChanged((void *)this, EQU_INSERT, (void *)prlEqu);
			}
			break;
		}
		case BC_EQU_UPDATE:
		{
			if((prlEqu = GetEquByUrno(llUrno)) != NULL)
			{
				GetRecordFromItemList(prlEqu,prlEquData->Fields,prlEquData->Data);
				//PrepareDataAfterRead(prlEqu);
				ogCCSDdx.DataChanged((void *)this, EQU_UPDATE, (void *)prlEqu);
			}
			break;
		}
		case BC_EQU_DELETE:
		{
			if((prlEqu = GetEquByUrno(llUrno)) != NULL)
			{
				long llEquUrno = prlEqu->Urno;
				DeleteEquInternal(llEquUrno);
				ogCCSDdx.DataChanged((void *)this, EQU_DELETE, (void *)llEquUrno);
			}
			break;
		}
	}
}
 
CedaEquData::~CedaEquData()
{
	TRACE("CedaEquData::~CedaEquData called\n");
	ClearAll();
}

void CedaEquData::ClearAll()
{
	omUrnoMap.RemoveAll();
	CMapStringToPtr *polSingleMap;
	long llUrno;
	for(POSITION rlPos = omGkeyMap.GetStartPosition(); rlPos != NULL; )
	{
		omGkeyMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omGkeyMap.RemoveAll();

	omData.DeleteAll();
}

CCSReturnCode CedaEquData::ReadEquData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaEquData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		EQUDATA rlEquData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlEquData)) == RCSuccess)
			{
				AddEquInternal(rlEquData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaEquData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(EQUDATA), pclWhere);
    return ilRc;
}

EQUDATA *CedaEquData::AddEquInternal(EQUDATA &rrpEqu)
{
	EQUDATA *prlEqu = new EQUDATA;
	*prlEqu = rrpEqu;
	omData.Add(prlEqu);
	omUrnoMap.SetAt((void *)prlEqu->Urno,prlEqu);

	CMapStringToPtr *polSingleMap;
	if(omGkeyMap.Lookup((void *) prlEqu->Gkey, (void *&) polSingleMap))
	{
		polSingleMap->SetAt(prlEqu->Enam,prlEqu);
	}
	else
	{
		polSingleMap = new CMapStringToPtr;
		polSingleMap->SetAt(prlEqu->Enam,prlEqu);
		omGkeyMap.SetAt((void *)prlEqu->Gkey,polSingleMap);
	}

	return prlEqu;
}

void CedaEquData::DeleteEquInternal(long lpUrno)
{
	EQUDATA *prlEqu = NULL;
	int ilNumEqus = omData.GetSize();
	for(int ilEqu = (ilNumEqus-1); ilEqu >= 0; ilEqu--)
	{
		prlEqu = &omData[ilEqu];
		if(prlEqu->Urno == lpUrno)
		{
			omData.DeleteAt(ilEqu);

			CMapStringToPtr *polSingleMap;
			if(omGkeyMap.Lookup((void *) prlEqu->Gkey, (void *&) polSingleMap))
			{
				polSingleMap->RemoveKey(prlEqu->Enam);
			}
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

EQUDATA* CedaEquData::GetEquByUrno(long lpUrno)
{
	EQUDATA *prlEqu = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlEqu);
	return prlEqu;
}

bool CedaEquData::GetEquipmentByType(long lpGkey, CCSPtrArray <EQUDATA> &ropEquipmentList, bool bpReset /*=true*/)
{
	if(bpReset)
	{
		ropEquipmentList.RemoveAll();
	}

	CMapStringToPtr *polSingleMap;

	if(omGkeyMap.Lookup((void *)lpGkey,(void *& )polSingleMap))
	{
		EQUDATA *prlEqu = NULL;
		CString olEnam;

		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,olEnam,(void *& )prlEqu);
			ropEquipmentList.Add(prlEqu);
		}
	}

	return ropEquipmentList.GetSize() > 0 ? true : false;
}

EQUDATA *CedaEquData::GetEquipmentByTypeAndName(long lpGkey, CString opEnam)
{
	EQUDATA *prlEqu = NULL;

	CMapStringToPtr *polSingleMap;

	if(omGkeyMap.Lookup((void *)lpGkey,(void *&)polSingleMap))
	{
		polSingleMap->Lookup(opEnam, (void *&)prlEqu);
	}

	return prlEqu;
}

CString CedaEquData::GetTableName(void)
{
	return CString(pcmTableName);
}

CString CedaEquData::Dump(long lpUrno)
{
	CString olDumpStr;
	EQUDATA *prlEqu = GetEquByUrno(lpUrno);
	if(prlEqu == NULL)
	{
		olDumpStr.Format("No EQUDATA Found for EQU.URNO <%ld>",lpUrno);
	}
	else
	{
		EQUDATA rlEqu;
		memcpy(&rlEqu,prlEqu,sizeof(EQUDATA));
		CString olData;
		MakeCedaData(&omRecInfo,olData,&rlEqu);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();
		if(ilNumFields != ilNumData)
		{
			olDumpStr.Format("<%s>\n<%s>\n",pcmFieldList,olData);
		}
		else
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + "> ";
			}
		}
	}
	return olDumpStr;
}

void CedaEquData::GetEquipmentNames(CStringArray &ropEquipmentNames) const
{
	ropEquipmentNames.RemoveAll();
	int ilNumData = ogEquData.omData.GetSize();
	for(int ilLc = 0; ilLc < ilNumData; ilLc++)
	{
		EQUDATA *prlEqu = &ogEquData.omData[ilLc];
		ropEquipmentNames.Add(prlEqu->Enam);
	}
}

