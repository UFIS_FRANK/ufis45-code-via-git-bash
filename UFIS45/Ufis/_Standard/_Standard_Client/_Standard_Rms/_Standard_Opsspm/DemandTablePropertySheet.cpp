// DemandTablePropertySheet.cpp : implementation file
//
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <DemandFilterPage.h>
#include <DemandTableSortPage.h>
#include <BasePropertySheet.h>
#include <DemandTablePropertySheet.h>
#include <DemandTableViewer.h>
#include <conflict.h>

#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaRnkData.h>
#include <CedaPerData.h>
#include <CedaFlightData.h>
#include <CedaAloData.h>
#include <CedaAltData.h>
#include <CedaNatData.h>
#include <CedaCicData.h>
#include <CedaBltData.h>
#include <CedaAptData.h>
#include <CedaSerData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

 
IMPLEMENT_DYNAMIC(DemandTablePropertySheet, BasePropertySheet)

/////////////////////////////////////////////////////////////////////////////
// DemandTablePropertySheet class variables
//

/////////////////////////////////////////////////////////////////////////////
// DemandTablePropertySheet
//
DemandTablePropertySheet::DemandTablePropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_STRING61877), pParentWnd, popViewer, iSelectPage)
{
	EnableStackedTabs(FALSE);
	AddPage(&m_pageColumns);
	AddPage(&m_pageAloc);
	AddPage(&m_pageDety);
	AddPage(&m_pageRety);
	AddPage(&m_pageAirline);
	AddPage(&m_pageNature);
	AddPage(&m_pageAirport);
	AddPage(&m_pageRank);
	AddPage(&m_pageService);
	AddPage(&m_pagePermits);
	AddPage(&m_pageSort);
	AddPage(&m_pageCic);
	AddPage(&m_pageBelt1);

	// Change the caption in tab control of the PropertySheet
	m_pageAloc.SetCaption(GetString(IDS_STRING61875));

	m_pageDety.SetCaption(GetString(IDS_STRING61876));

	m_pageRety.SetCaption(GetString(IDS_RETYTABCTRL));

	m_pageAirline.SetCaption(GetString(IDS_STRING61597));

	m_pageAirport.SetCaption(GetString(IDS_DEMLIST_DES3));

	m_pageNature.SetCaption(GetString(IDS_STRING632));

	m_pageRank.SetCaption(GetString(IDS_STRING61604));

	m_pageService.SetCaption(GetString(IDS_SERVICECODES));

	m_pagePermits.SetCaption(GetString(IDS_PERMIT_TAB));

	m_pageColumns.SetCaption(GetString(IDS_STRING61895));

	m_pageCic.SetCaption(GetString(IDS_DTPS_CIC));

	m_pageBelt1.SetCaption(GetString(IDS_DTPS_BELTS));

	// Prepare possible values for each PropertyPage
//	ogBasicData.GetAllRanks(m_pageGate.omPossibleItems);
//	ogBasicData.GetAllShifts(m_pageAirline.omPossibleItems);

	for (int i = 0; i < ogDemandData.GetCountOfDemandTypes(); i++)
	{
		DEMANDDATA olDemand;
		olDemand.Dety[0] = '0' + i;
		m_pageDety.omPossibleItems.Add(ogDemandData.GetStringForDemandType(olDemand));
	}
	m_pageDety.bmSelectAllEnabled = true;

	// airline codes format ALC2/ALC3
	ogAltData.GetAlc2Alc3Strings(m_pageAirline.omPossibleItems);
	m_pageAirline.bmSelectAllEnabled = true;

	ogNatData.GetAllNatures(m_pageNature.omPossibleItems);
	m_pageNature.bmSelectAllEnabled = true;

	ogAptData.GetApc3Apc4Strings(m_pageAirport.omPossibleItems);
	m_pageAirport.bmSelectAllEnabled = true;

	ogRnkData.GetAllRanks(m_pageRank.omPossibleItems);
	m_pageRank.bmSelectAllEnabled = true;

	ogSerData.GetAllServiceCodes(m_pageService.omPossibleItems, TRUE);
	ogSerData.GetAllServiceCodes(m_pageService.omPossibleItems, FALSE);
	m_pageService.bmSelectAllEnabled = true;

	ogPerData.GetAllPermits(m_pagePermits.omPossibleItems);
	m_pagePermits.bmSelectAllEnabled = true;

	ogCicData.GetAllCics(m_pageCic.omPossibleItems);
	m_pageCic.bmSelectAllEnabled = true;

	ogBltData.GetAllBltNames(m_pageBelt1.omPossibleItems);
	m_pageBelt1.bmSelectAllEnabled = true;

	// resource type (personnel/equipment/location)
	m_pageRety.omPossibleItems.Add(GetString(IDS_PERSONNEL_DEM));
	m_pageRety.omPossibleItems.Add(GetString(IDS_EQUIPMENT_DEM));
	m_pageRety.omPossibleItems.Add(GetString(IDS_LOCATION_DEM));
	m_pageRety.bmSelectAllEnabled = true;


	// get all possible aloc's
	const char **pclAllocGroupType = DemandTableViewer::omAllocGroupTypes;
	if (*pclAllocGroupType)
	{
		m_pageAloc.omPossibleItems.Add(*pclAllocGroupType);

		// add allocation id pages
		if (!m_pageAlids.Lookup(*pclAllocGroupType,(void *&)polPage1))
		{
			polPage1 = new DemandFilterPage;
			m_pageAlids.SetAt(*pclAllocGroupType,polPage1);

			ALLOCUNIT *pclUnit = ogAllocData.GetGroupTypeByName(*pclAllocGroupType);
			if (pclUnit)
				polPage1->SetCaption(pclUnit->Desc);
			else
				polPage1->SetCaption(*pclAllocGroupType);

			CCSPtrArray <ALLOCUNIT> olAllocGroups;
			ogAllocData.GetGroupsByGroupType(*pclAllocGroupType,olAllocGroups);
			int ilNumGroups = olAllocGroups.GetSize();
			for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
			{
				ALLOCUNIT *prlGroup= &olAllocGroups[ilGroup];
				polPage1->omPossibleItems.Add(prlGroup->Name);
			}
			polPage1->bmSelectAllEnabled = true;
		}
		++pclAllocGroupType;
	}
	if (*pclAllocGroupType)
	{
		// add allocation id pages
		m_pageAloc.omPossibleItems.Add(*pclAllocGroupType);

		if (!m_pageAlids.Lookup(*pclAllocGroupType,(void *&)polPage2))
		{
			polPage2 = new DemandFilterPage;
			m_pageAlids.SetAt(*pclAllocGroupType,polPage2);

			ALLOCUNIT *pclUnit = ogAllocData.GetGroupTypeByName(*pclAllocGroupType);
			if (pclUnit)
				polPage2->SetCaption(pclUnit->Desc);
			else
				polPage2->SetCaption(*pclAllocGroupType);

			CCSPtrArray <ALLOCUNIT> olAllocGroups;
			ogAllocData.GetGroupsByGroupType(*pclAllocGroupType,olAllocGroups);
			int ilNumGroups = olAllocGroups.GetSize();
			for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
			{
				ALLOCUNIT *prlGroup= &olAllocGroups[ilGroup];
				polPage2->omPossibleItems.Add(prlGroup->Name);
			}
			polPage2->bmSelectAllEnabled = true;
		}
		++pclAllocGroupType;
	}
	if (*pclAllocGroupType)
	{
		m_pageAloc.omPossibleItems.Add(*pclAllocGroupType);

		// add allocation id pages
		if (!m_pageAlids.Lookup(*pclAllocGroupType,(void *&)polPage3))
		{
			polPage3 = new DemandFilterPage;
			m_pageAlids.SetAt(*pclAllocGroupType,polPage3);

			ALLOCUNIT *pclUnit = ogAllocData.GetGroupTypeByName(*pclAllocGroupType);
			if (pclUnit)
				polPage3->SetCaption(pclUnit->Desc);
			else
				polPage3->SetCaption(*pclAllocGroupType);

			CCSPtrArray <ALLOCUNIT> olAllocGroups;
			ogAllocData.GetGroupsByGroupType(*pclAllocGroupType,olAllocGroups);
			int ilNumGroups = olAllocGroups.GetSize();
			for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
			{
				ALLOCUNIT *prlGroup= &olAllocGroups[ilGroup];
				polPage3->omPossibleItems.Add(prlGroup->Name);
			}
			polPage3->bmSelectAllEnabled = true;
		}
		++pclAllocGroupType;
	}
	if (*pclAllocGroupType)
	{
		m_pageAloc.omPossibleItems.Add(*pclAllocGroupType);

		// add allocation id pages
		if (!m_pageAlids.Lookup(*pclAllocGroupType,(void *&)polPage4))
		{
			polPage4 = new DemandFilterPage;
			m_pageAlids.SetAt(*pclAllocGroupType,polPage4);

			ALLOCUNIT *pclUnit = ogAllocData.GetGroupTypeByName(*pclAllocGroupType);
			if (pclUnit)
				polPage4->SetCaption(pclUnit->Desc);
			else
				polPage4->SetCaption(*pclAllocGroupType);

			CCSPtrArray <ALLOCUNIT> olAllocGroups;
			ogAllocData.GetGroupsByGroupType(*pclAllocGroupType,olAllocGroups);
			int ilNumGroups = olAllocGroups.GetSize();
			for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
			{
				ALLOCUNIT *prlGroup= &olAllocGroups[ilGroup];
				polPage4->omPossibleItems.Add(prlGroup->Name);
			}
			polPage4->bmSelectAllEnabled = true;
		}
		//++pclAllocGroupType;
	}

	//added bu MAX
	
/*
	while(*pclAllocGroupType)
	{
		m_pageAloc.omPossibleItems.Add(*pclAllocGroupType);

		// add allocation id pages
		DemandFilterPage *polPage = NULL;
		if (!m_pageAlids.Lookup(*pclAllocGroupType,(void *&)polPage))
		{
			polPage = new DemandFilterPage;
			m_pageAlids.SetAt(*pclAllocGroupType,polPage);

			ALLOCUNIT *pclUnit = ogAllocData.GetGroupTypeByName(*pclAllocGroupType);
			if (pclUnit)
				polPage->SetCaption(pclUnit->Desc);
			else
				polPage->SetCaption(*pclAllocGroupType);

			CCSPtrArray <ALLOCUNIT> olAllocGroups;
			ogAllocData.GetGroupsByGroupType(*pclAllocGroupType,olAllocGroups);
			int ilNumGroups = olAllocGroups.GetSize();
			for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
			{
				ALLOCUNIT *prlGroup= &olAllocGroups[ilGroup];
				polPage->omPossibleItems.Add(prlGroup->Name);
			}
			polPage->bmSelectAllEnabled = true;
		}
		++pclAllocGroupType;
	}
*/
	m_pageAloc.bmSelectAllEnabled = true;

	// get all possible columns
	CStringArray olColumns;
	DemandTableViewer::GetAllViewColumns(olColumns);
	for (int ilC = 0; ilC < olColumns.GetSize(); ilC++)
	{
		m_pageColumns.omPossibleItems.Add(DemandTableViewer::GetViewColumnName(olColumns[ilC]));		
	}
	
	m_pageColumns.SetSorted(FALSE);

}

DemandTablePropertySheet::~DemandTablePropertySheet()
{
	POSITION pos = m_pageAlids.GetStartPosition();
	while(pos)
	{
		CString olKey;
		DemandFilterPage *polPage = NULL;
		m_pageAlids.GetNextAssoc(pos,olKey,(void *&)polPage);
		if (GetPageIndex(polPage) >= 0)
			RemovePage(polPage);
		delete polPage;
	}
	m_pageAlids.RemoveAll();
}

BEGIN_MESSAGE_MAP(DemandTablePropertySheet, BasePropertySheet)
	//{{AFX_MSG_MAP(DemandTablePropertySheet)
    ON_MESSAGE(WM_UPDATE_ALL_PAGES, OnUpdateAllPages)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void DemandTablePropertySheet::LoadDataFromViewer()
{
	// remove all previously used Alid - based pages from the sheet
	POSITION pos = m_pageAlids.GetStartPosition();
	while(pos)
	{
		CString olKey;
		DemandFilterPage *polPage = NULL;
		m_pageAlids.GetNextAssoc(pos,olKey,(void *&)polPage);
		if (GetPageIndex(polPage) >= 0)
			RemovePage(polPage);
	}

	// get allocation filter
	pomViewer->GetFilter("Aloc", m_pageAloc.omSelectedItems);
	bool flagisall=false;
	int ilC=0;
	for (ilC = 0; ilC < m_pageAloc.omSelectedItems.GetSize();ilC++)
	{
 		if (m_pageAloc.omSelectedItems[ilC]=="*All")
		{
			flagisall=true;
			break;
		}
	}
	if (flagisall==true)
	{


				for (int myi = 0; myi < m_pageAloc.omPossibleItems.GetSize();myi++)
				{
					DemandFilterPage *polPage = NULL;
					if (m_pageAlids.Lookup(m_pageAloc.omPossibleItems[myi],(void *&)polPage) && polPage != NULL)
					{
						pomViewer->GetFilter(m_pageAloc.omPossibleItems[myi],polPage->omSelectedItems);
						AddPage(polPage);
					}
				}
	}
		else
	{
		for (ilC = 0; ilC < m_pageAloc.omSelectedItems.GetSize();ilC++)
		{
			DemandFilterPage *polPage = NULL;
			if (m_pageAlids.Lookup(m_pageAloc.omSelectedItems[ilC],(void *&)polPage) && polPage != NULL)
			{
				pomViewer->GetFilter(m_pageAloc.omSelectedItems[ilC],polPage->omSelectedItems);
				AddPage(polPage);
			}
		}
	}

	pomViewer->GetFilter("Dety",m_pageDety.omSelectedItems);

	pomViewer->GetFilter("Airline",		m_pageAirline.omSelectedItems);

	pomViewer->GetFilter("Airport",		m_pageAirport.omSelectedItems);

	pomViewer->GetFilter("Nature",		m_pageNature.omSelectedItems);

	pomViewer->GetFilter("Dienstrang", m_pageRank.omSelectedItems);

	pomViewer->GetFilter("Service", m_pageService.omSelectedItems);

	pomViewer->GetFilter("Permits", m_pagePermits.omSelectedItems);

	pomViewer->GetFilter("Cic", m_pageCic.omSelectedItems);

	pomViewer->GetFilter("Belt1", m_pageBelt1.omSelectedItems);

	pomViewer->GetFilter("Rety",m_pageRety.omSelectedItems);

	CStringArray olColumns;
	pomViewer->GetFilter("Columns",olColumns);
	m_pageColumns.omSelectedItems.RemoveAll();
	for (ilC = 0; ilC < olColumns.GetSize(); ilC++)
	{
		m_pageColumns.omSelectedItems.Add(DemandTableViewer::GetViewColumnName(olColumns[ilC]));		
	}

	pomViewer->GetSort(m_pageSort.omSortOrders);
	m_pageSort.bmDemStartAscending = pomViewer->GetUserData("DEMSTARTORDER") == "YES" ? true : false;
	m_pageSort.bmDemEndAscending = pomViewer->GetUserData("DEMENDORDER") == "YES" ? true : false;

	//added by MAX
	m_pageSort.bmDemTerminalAscending = pomViewer->GetUserData("DEMTERMINALORDER") == "YES" ? true : false;
	m_pageSort.m_Group = pomViewer->GetGroup()[0] - '0';
}

void DemandTablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("Aloc", m_pageAloc.omSelectedItems);
	for (int ilC = 0; ilC < m_pageAloc.omSelectedItems.GetSize();ilC++)
	{
		if (m_pageAloc.omSelectedItems[ilC]=="*All")
		{
			for (int myi = 0; myi < m_pageAloc.omPossibleItems.GetSize();myi++)
			{
				DemandFilterPage *polPage = NULL;
				if (m_pageAlids.Lookup(m_pageAloc.omPossibleItems[myi],(void *&)polPage) && polPage != NULL)
				{
					pomViewer->SetFilter(m_pageAloc.omPossibleItems[myi],polPage->omSelectedItems);
				}
			}
 		}
		DemandFilterPage *polPage = NULL;
		if (m_pageAlids.Lookup(m_pageAloc.omSelectedItems[ilC],(void *&)polPage) && polPage != NULL)
		{
			pomViewer->SetFilter(m_pageAloc.omSelectedItems[ilC],polPage->omSelectedItems);
		}
	}

	pomViewer->SetFilter("Dety", m_pageDety.omSelectedItems);

	pomViewer->SetFilter("Rety", m_pageRety.omSelectedItems);

	pomViewer->SetFilter("Airline",		m_pageAirline.omSelectedItems);

	pomViewer->SetFilter("Airport",		m_pageAirport.omSelectedItems);

	pomViewer->SetFilter("Nature",		m_pageNature.omSelectedItems);

	pomViewer->SetFilter("Dienstrang", m_pageRank.omSelectedItems);

	pomViewer->SetFilter("Service", m_pageService.omSelectedItems);

	pomViewer->SetFilter("Permits", m_pagePermits.omSelectedItems);

	pomViewer->SetFilter("Cic", m_pageCic.omSelectedItems);

	pomViewer->SetFilter("Belt1", m_pageBelt1.omSelectedItems);

	CStringArray olColumns;
	for (ilC = 0; ilC < m_pageColumns.omSelectedItems.GetSize(); ilC++)
	{
		olColumns.Add(DemandTableViewer::GetViewColumnField(m_pageColumns.omSelectedItems[ilC]));		
	}
	pomViewer->SetFilter("Columns",olColumns);
	
	pomViewer->SetSort(m_pageSort.omSortOrders);
	pomViewer->SetUserData("DEMSTARTORDER",m_pageSort.bmDemStartAscending ? "YES" : "NO");
	pomViewer->SetUserData("DEMENDORDER",m_pageSort.bmDemEndAscending ? "YES" : "NO");

	pomViewer->SetUserData("DEMTERMINALORDER",m_pageSort.bmDemTerminalAscending ? "YES" : "NO");

	pomViewer->SetGroup(CString(m_pageSort.m_Group + '0'));
	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int DemandTablePropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedAlocs;
	CStringArray olSelectedDetys;
	CStringArray olSelectedRetys;
	CStringArray olSelectedAirlines;
	CStringArray olSelectedAirports;
	CStringArray olSelectedNatures;
	CStringArray olSelectedRanks;
	CStringArray olSelectedServices;
	CStringArray olSelectedPermits;
	CStringArray olSelectedColumns;
	CStringArray olSelectedCics;
	CStringArray olSelectedBelt1s;
	CStringArray olSortOrders;
	CString olGroupBy;
	CStringArray olSelectedTime;
	pomViewer->GetFilter("Aloc", olSelectedAlocs);
	pomViewer->GetFilter("Dety", olSelectedDetys);
	pomViewer->GetFilter("Rety", olSelectedRetys);
	pomViewer->GetFilter("Airline",		olSelectedAirlines);
	pomViewer->GetFilter("Airport",		olSelectedAirports);
	pomViewer->GetFilter("Nature",		olSelectedNatures);
	pomViewer->GetFilter("Dienstrang", olSelectedRanks);
	pomViewer->GetFilter("Service", olSelectedServices);
	pomViewer->GetFilter("Permits", olSelectedPermits);
	pomViewer->GetFilter("Cic", olSelectedCics);
	pomViewer->GetFilter("Belt1", olSelectedBelt1s);

	CStringArray olSelectedColumnsFields;
	pomViewer->GetFilter("Columns",olSelectedColumnsFields);
	for (int ilC = 0; ilC < olSelectedColumnsFields.GetSize(); ilC++)
	{
		olSelectedColumns.Add(DemandTableViewer::GetViewColumnName(olSelectedColumnsFields[ilC]));		
	}
	
	bool blDemStartAscending = pomViewer->GetUserData("DEMSTARTORDER") == "YES" ? true : false;
	bool blDemEndAscending = pomViewer->GetUserData("DEMENDORDER") == "YES" ? true : false;

	bool blDemTerminalAscending = pomViewer->GetUserData("DEMTERMINALORDER") == "YES" ? true : false;

	pomViewer->GetSort(olSortOrders);
	olGroupBy = pomViewer->GetGroup();

	if	( !IsIdentical(olSelectedAlocs, m_pageAloc.omSelectedItems) 
		||!IsIdentical(olSelectedDetys, m_pageDety.omSelectedItems) 
		||!IsIdentical(olSelectedRetys, m_pageRety.omSelectedItems) 
		||!IsIdentical(olSelectedAirlines,	m_pageAirline.omSelectedItems)
		||!IsIdentical(olSelectedAirports,	m_pageAirport.omSelectedItems)
		||!IsIdentical(olSelectedNatures,	m_pageNature.omSelectedItems)
		||!IsIdentical(olSelectedRanks, m_pageRank.omSelectedItems) 
		||!IsIdentical(olSelectedServices, m_pageService.omSelectedItems) 
		||!IsIdentical(olSelectedCics, m_pageCic.omSelectedItems) 
		||!IsIdentical(olSelectedBelt1s, m_pageBelt1.omSelectedItems) 
		||!IsIdentical(olSelectedPermits, m_pagePermits.omSelectedItems)
		||!IsIdentical(olSelectedColumns, m_pageColumns.omSelectedItems) 
		||!IsIdentical(olSortOrders, m_pageSort.omSortOrders) 
		|| olGroupBy != CString(m_pageSort.m_Group + '0')
		|| m_pageSort.bmDemStartAscending != blDemStartAscending
		|| m_pageSort.bmDemEndAscending != blDemEndAscending
		|| m_pageSort.bmDemTerminalAscending != blDemTerminalAscending)
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}

LONG DemandTablePropertySheet::OnUpdateAllPages(UINT wParam,LONG lParam)
{
	CPropertyPage *popSenderPage = reinterpret_cast<CPropertyPage *>(lParam);
	if (popSenderPage == &m_pageAloc)
	{
		bool blSelectAll = false;
		if (m_pageAloc.m_List2.GetCount() == 1)
		{
			CString olName;
			m_pageAloc.m_List2.GetText(0,olName);
			if (olName == GetString(IDS_ALLSTRING))
				blSelectAll = true;
		}

		POSITION pos = m_pageAlids.GetStartPosition();
		while(pos)
		{
			CString olName;
			CPropertyPage *polPage = NULL;
			m_pageAlids.GetNextAssoc(pos,olName,(void *&)polPage);
			if (blSelectAll && GetPageIndex(polPage) < 0)
			{
				AddPage(polPage);
			}
			else if (m_pageAloc.m_List2.FindStringExact(-1,olName) == LB_ERR)
			{
				if (GetPageIndex(polPage) >= 0)
					RemovePage(polPage);
			}
			else if (GetPageIndex(polPage) < 0)
				AddPage(polPage);
		}
	}
	return 0l;
}