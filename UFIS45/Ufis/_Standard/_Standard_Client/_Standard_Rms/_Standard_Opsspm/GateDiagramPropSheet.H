// GateDiagramPropSheet.h : header file
//

#ifndef _GATDIAPS_H_
#define _GATDIAPS_H_
#include <GateDiagramArrDepPage.h>

/////////////////////////////////////////////////////////////////////////////
// GateDiagramPropertySheet

class GateDiagramPropertySheet : public BasePropertySheet
{
// Construction
public:
	GateDiagramPropertySheet(CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	FilterPage m_pageGateArea;
	FilterPage m_pageAirline;
	FilterPage m_pageAgents;
	GateDiagramArrDepPage m_pageArrDep;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int	 QueryForDiscardChanges();
};

/////////////////////////////////////////////////////////////////////////////

#endif // _GATDIAPS_H_
