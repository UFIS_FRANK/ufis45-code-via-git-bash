// CedaGatData.cpp - Class for gates
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaGatData.h>
#include <BasicData.h>

CedaGatData::CedaGatData()
{                  
    BEGIN_CEDARECINFO(GATDATA, GatDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Gnam,"GNAM")
		FIELD_DATE(Vafr,"VAFR")
		FIELD_DATE(Vato,"VATO")
		FIELD_DATE(Nafr,"NAFR")
		FIELD_DATE(Nato,"NATO")
		FIELD_CHAR_TRIM(Rga1,"RGA1")
		FIELD_CHAR_TRIM(Rga2,"RGA2")
		FIELD_CHAR_TRIM(Tele,"TELE")
		FIELD_CHAR_TRIM(Term,"TERM") //Singpaore
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(GatDataRecInfo)/sizeof(GatDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&GatDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"GATTAB");
    pcmFieldList = "URNO,GNAM,VAFR,VATO,NAFR,NATO,RGA1,RGA2,TELE,TERM";  //Added terminal Singapore
}
 
CedaGatData::~CedaGatData()
{
	TRACE("CedaGatData::~CedaGatData called\n");
	ClearAll();
}

void CedaGatData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omNameMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaGatData::ReadGatData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaGatData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		GATDATA rlGatData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlGatData)) == RCSuccess)
			{
				AddGatInternal(rlGatData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaGatData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(GATDATA), pclWhere);
    return ilRc;
}


void CedaGatData::AddGatInternal(GATDATA &rrpGat)
{
	GATDATA *prlGat = new GATDATA;
	*prlGat = rrpGat;
	omData.Add(prlGat);
	omUrnoMap.SetAt((void *)prlGat->Urno,prlGat);
	omNameMap.SetAt(prlGat->Gnam,prlGat);
}


GATDATA* CedaGatData::GetGatByUrno(long lpUrno)
{
	GATDATA *prlGat = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlGat);
	return prlGat;
}

GATDATA* CedaGatData::GetGatByName(const char *pcpName)
{
	GATDATA *prlGat = NULL;
	omNameMap.Lookup(pcpName, (void *&) prlGat);
	return prlGat;
}

long CedaGatData::GetGatUrnoByName(const char *pcpName)
{
	long llUrno = 0L;
	GATDATA *prlGat = GetGatByName(pcpName);
	if(prlGat != NULL)
	{
		llUrno = prlGat->Urno;
	}
	return llUrno;
}


CString CedaGatData::GetTableName(void)
{
	return CString(pcmTableName);
}


void CedaGatData::GetAllGateNames(CStringArray &ropGateNames)
{
	int ilNumGats = omData.GetSize();
	for(int ilGat = 0; ilGat < ilNumGats; ilGat++)
	{
		ropGateNames.Add(omData[ilGat].Gnam);
	}
}


void CedaGatData::GetAllValidGateNames(CStringArray &ropGateNames)
{
	int ilNumGats = omData.GetSize();
	CTime olCurrTime = ogBasicData.GetTime();

	for(int ilGat = 0; ilGat < ilNumGats; ilGat++)
	{
		GATDATA *prlGat = &omData[ilGat]; 
		if( olCurrTime >= prlGat->Vafr && ( prlGat->Vato == -1 || olCurrTime <= prlGat->Vato))
		{
			ropGateNames.Add(prlGat->Gnam);
		}
	}
}