// Class for DRATAB - temporary unpaid absences
#ifndef _CEDADRADATA_H_
#define _CEDADRADATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct DraDataStruct
{
	
	CTime			Abfr;					// abwesend von
	CTime			Abto;					// abwesend bis
	char			Drrn[3]; 		// Schichtnummer (1-n pro Tag)
	char 			Rema[50];		 		// Bemerkung
//	char 			Absc[8]; 	// besondere Abwesenheitsarten
	char 			Sday[10]; 		// Tages-Schl�ssel YYYYMMDD
	long 			Stfu;					// Mitarbeiter-Urno
	long 			Bsdu;					// Urno of ODA-record
	char 			Usec[35]; 		// Anwender (Ersteller)
//	char 			Useu[35]; 		// Anwender (letzte �nderung)
	CTime			Cdat;					// Erstellungsdatum
//	CTime			Lstu;					// Datum letzte �nderung
	long			Urno;
	char			Sdac[8];	// Absence code in ODATAB

	DraDataStruct(void)
	{
		Stfu = 0L;
		Bsdu = 0L;
		Urno = 0L;
		strcpy(Sdac,"");
		strcpy(Drrn,"");
		strcpy(Rema,"");
		strcpy(Sday,"");
		strcpy(Usec,"");
		Abfr=TIMENULL;
		Cdat=TIMENULL;
		Abto=TIMENULL;
	}
};

typedef struct DraDataStruct DRADATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaDraData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <DRADATA> omData;
	CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);

// Operations
public:
	CedaDraData();
	~CedaDraData();

	CCSReturnCode ReadAllDraData(char* sday, long stfu);
	void DeleteTemp(long urno);
//	void UpdateTemp(long urno, CTime from,CTime to);

	DRADATA ReadDraByUrno(long urno);
	
	void ProcessDraBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CCSReturnCode ReadDraData();
	DRADATA* GetDraByUrno(long lpUrno);
	void PrepareData(DRADATA *prpDra);
	CCSReturnCode DeleteDraRecord(long lpUrno);
    //bool InsertDraRecord(DRADATA *prpDra);

        bool InsertDraRecord(DRADATA rlDrg);

private:
	DRADATA *AddDraInternal(DRADATA &rrpDra);
	void DeleteDraInternal(long lpUrno);
	void ClearAll();
};

extern CedaDraData ogDraData;

#endif _CEDADRADATA_H_
