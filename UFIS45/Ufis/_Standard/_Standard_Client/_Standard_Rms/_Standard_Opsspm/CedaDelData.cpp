// CedaDelData.cpp - Class for Shift Delegations
// shift delegations are extra records for a shift
// eg 'F1' to indicate that all emps that have this shift
// are delegated to other shifts - these have their own
// functions (optional) but not permits
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaDelData.h>
#include <BasicData.h>
#include <Resource.h>

extern CCSDdx ogCCSDdx;
void  ProcessDelCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaDelData::CedaDelData()
{                  
    BEGIN_CEDARECINFO(DELDATA, DelDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Fctc,"FCTC")
		FIELD_CHAR_TRIM(Sdac,"SDAC")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(DelDataRecInfo)/sizeof(DelDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DelDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"DELTAB");
    pcmFieldList = "URNO,FCTC,SDAC";

	ogCCSDdx.Register((void *)this,BC_DEL_CHANGE,CString("DELDATA"), CString("Del changed"),ProcessDelCf);
	ogCCSDdx.Register((void *)this,BC_DEL_DELETE,CString("DELDATA"), CString("Del deleted"),ProcessDelCf);
}
 
CedaDelData::~CedaDelData()
{
	TRACE("CedaDelData::~CedaDelData called\n");
	ClearAll();
	ogCCSDdx.UnRegister(this,NOTUSED);
}

void  ProcessDelCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaDelData *)popInstance)->ProcessDelBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void  CedaDelData::ProcessDelBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlDelData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlDelData->Selection);
	DELDATA *prlDel = GetDelByUrno(llUrno);
	ogBasicData.LogBroadcast(prlDelData);

	if(ipDDXType == BC_DEL_CHANGE && prlDel != NULL)
	{
		// update
		//ogBasicData.Trace("BC_DEL_CHANGE - UPDATE DELTAB URNO %ld\n",prlDel->Urno);
		GetRecordFromItemList(prlDel,prlDelData->Fields,prlDelData->Data);
	}
	else if(ipDDXType == BC_DEL_CHANGE && prlDel == NULL)
	{
		// insert
		DELDATA rlDel;
		GetRecordFromItemList(&rlDel,prlDelData->Fields,prlDelData->Data);
		//ogBasicData.Trace("BC_DEL_CHANGE - INSERT DELTAB URNO %ld\n",rlDel.Urno);
		AddDelInternal(rlDel);
	}
	else if(ipDDXType == BC_DEL_DELETE && prlDel != NULL)
	{
		// delete
		//ogBasicData.Trace("BC_DEL_DELETE - DELETE DELTAB URNO %ld\n",prlDel->Urno);
		DeleteDelInternal(prlDel->Urno);
	}

}


void CedaDelData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

bool CedaDelData::ReadDelData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512] = "";
    //sprintf(pclWhere,"WHERE APPL='%s'",pcgAppName);
	if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaDelData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		DELDATA rlDelData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlDelData)) == RCSuccess)
			{
				AddDelInternal(rlDelData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaDelData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(DELDATA), pclWhere);
    return ilRc;
}


void CedaDelData::AddDelInternal(DELDATA &rrpDel)
{
	DELDATA *prlDel = new DELDATA;
	*prlDel = rrpDel;
	omData.Add(prlDel);
	omUrnoMap.SetAt((void *)prlDel->Urno,prlDel);
}


void CedaDelData::DeleteDelInternal(long lpUrno)
{
	int ilNumDels = omData.GetSize();
	for(int ilDel = (ilNumDels-1); ilDel >= 0; ilDel--)
	{
		if(omData[ilDel].Urno == lpUrno)
		{
			omData.DeleteAt(ilDel);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}


DELDATA* CedaDelData::GetDelByUrno(long lpUrno)
{
	DELDATA *prlDel = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlDel);
	return prlDel;
}

CString CedaDelData::GetFunctionByUrno(long lpUrno)
{
	CString olFunction;
	DELDATA *prlDel = GetDelByUrno(lpUrno);
	if(prlDel != NULL)
	{
		olFunction = prlDel->Fctc;
	}
	return olFunction;
}

CString CedaDelData::GetTableName(void)
{
	return CString(pcmTableName);
}