// StaffTableFilterPage.cpp : implementation file
//

#include "stdafx.h"
#include "opsspm.h"
#include "StaffTableFilterPage.h"
#include <BasicData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStaffTableFilterPage dialog

IMPLEMENT_DYNCREATE(CStaffTableFilterPage, CPropertyPage)

CStaffTableFilterPage::CStaffTableFilterPage() : CPropertyPage(CStaffTableFilterPage::IDD)
{
	//{{AFX_DATA_INIT(CStaffTableFilterPage)
	m_HideAbsentEmps = FALSE;
	m_HideFidEmps = FALSE;
	m_HideStandbyEmps = FALSE;
	m_ShiftTime = -1;
	//}}AFX_DATA_INIT
}


void CStaffTableFilterPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CStaffTableFilterPage)
	DDX_Control(pDX, IDC_SHIFTBEGIN, m_ShiftBeginCtrl);
	DDX_Control(pDX, IDC_SHIFTEND, m_ShiftEndCtrl);
	DDX_Control(pDX, IDC_SHIFTOVERLAP, m_ShiftOverlapCtrl);
	DDX_Control(pDX, IDC_HIDEABSENTEMPS, m_HideAbsentEmpsCtrl);
	DDX_Control(pDX, IDC_HIDEFIDEMPS, m_HideFidEmpsCtrl);
	DDX_Control(pDX, IDC_HIDESTANDBYEMPS, m_HideStandbyEmpsCtrl);
	DDX_Check(pDX, IDC_HIDEABSENTEMPS, m_HideAbsentEmps);
	DDX_Check(pDX, IDC_HIDEFIDEMPS, m_HideFidEmps);
	DDX_Check(pDX, IDC_HIDESTANDBYEMPS, m_HideStandbyEmps);
	DDX_Radio(pDX, IDC_SHIFTBEGIN, m_ShiftTime);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CStaffTableFilterPage, CPropertyPage)
	//{{AFX_MSG_MAP(CStaffTableFilterPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CStaffTableFilterPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	

	m_HideAbsentEmpsCtrl.SetWindowText(GetString(IDS_HIDEABSENTEMPS));
	m_HideFidEmpsCtrl.SetWindowText(GetString(IDS_HIDEFIDEMPS));
	m_HideStandbyEmpsCtrl.SetWindowText(GetString(IDS_HIDESTANDBYEMPS));

	m_ShiftBeginCtrl.SetWindowText(GetString(IDS_STAFFTABLE_SHIFTBEGIN));
	m_ShiftEndCtrl.SetWindowText(GetString(IDS_STAFFTABLE_SHIFTEND));
	m_ShiftOverlapCtrl.SetWindowText(GetString(IDS_STAFFTABLE_SHIFTOVERLAP));
	

	

	
  
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


/////////////////////////////////////////////////////////////////////////////
// CStaffTableFilterPage message handlers
