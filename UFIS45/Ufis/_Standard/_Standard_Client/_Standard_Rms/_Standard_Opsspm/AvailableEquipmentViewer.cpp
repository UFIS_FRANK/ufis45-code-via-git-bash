// AvailableEquipmentViewer.cpp : implementation file
//
// Modification History:
// 19-Sep-96	Damkerng	Change many places since our database is changed.
//							On this change, special flight job was changed from type
//							FLT to FLS. The field PRID which contains a word "SPECIAL"
//							is not used any more. The field FLUR which was used for
//							storing shift URNO now moved to the field SHUR.
//							Check these places out by searching for "Id 19-Sep-96"

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaShiftData.h>
#include <ccsddx.h>
#include <AvailableEquipmentViewer.h>
#include <BasicData.h>
#include <AllocData.h>
#include <CedaEqtData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void AvailableStaffTableTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// AvailableEquipmentViewer
//

AvailableEquipmentViewer::AvailableEquipmentViewer(CCSPtrArray<AVAILABLE_EQU_LINEDATA> *popAvailableEquipment)
{
	pomAvailableEquipment = NULL;
	if(popAvailableEquipment != NULL)
	{
		pomAvailableEquipment = popAvailableEquipment;
	}
//	omSortOrder.Add("TIMEFROM");
//	omSortOrder.Add("TEAM");	
    pomTable = NULL;

	bmUseAllEquipmentTypes = false;
	bmUseAllEquipmentGroups = false;
	bmUseAllEquipmentNames = false;
}

AvailableEquipmentViewer::~AvailableEquipmentViewer()
{
    DeleteAll();
}

void AvailableEquipmentViewer::Attach(CTable *popTable)
{
    pomTable = popTable;
}

void AvailableEquipmentViewer::PrepareFilter()
{
	CStringArray olFilterValues;
	int i, ilNumFilterValues;

	omGroupBy = CViewer::GetGroup();

	bmUseAllEquipmentGroups = false;
	omEquipmentGroupMap.RemoveAll();
	GetFilter("EquipmentGroup", olFilterValues);
	ilNumFilterValues = olFilterValues.GetSize();
	if (ilNumFilterValues > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
		{
			bmUseAllEquipmentGroups = true;
		}
		else
		{
			for (i = 0; i < ilNumFilterValues; i++)
			{
				omEquipmentGroupMap[olFilterValues[i]] = NULL;
			}
		}
	}

	bmUseAllEquipmentTypes = false;
	omEquipmentTypeMap.RemoveAll();
	GetFilter("EquipmentType", olFilterValues);
	ilNumFilterValues = olFilterValues.GetSize();
	if (ilNumFilterValues > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
		{
			bmUseAllEquipmentTypes = true;
		}
		else
		{
			for (i = 0; i < ilNumFilterValues; i++)
			{
				omEquipmentTypeMap[olFilterValues[i]] = NULL;
			}
		}
	}

	bmUseAllEquipmentNames = false;
	omEquipmentNamesMap.RemoveAll();
	GetFilter("EquipmentName", olFilterValues);
	ilNumFilterValues = olFilterValues.GetSize();
	if (ilNumFilterValues > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
		{
			bmUseAllEquipmentNames = true;
		}
		else
		{
			for (i = 0; i < ilNumFilterValues; i++)
			{
				omEquipmentNamesMap[olFilterValues[i]] = NULL;
			}
		}
	}
}

int AvailableEquipmentViewer::CompareAvailable(AVAILABLE_EQU_LINEDATA *prpLine1, AVAILABLE_EQU_LINEDATA *prpLine2)
{
	int ilRes = 0;
	if(prpLine1->IsCorrectEquForDemand && !prpLine2->IsCorrectEquForDemand)
	{
		ilRes = -1;
	}
	else if(!prpLine1->IsCorrectEquForDemand && prpLine2->IsCorrectEquForDemand)
	{
		ilRes = 1;
	}
	else
	{
		ilRes = strcmp(prpLine1->Enam,prpLine2->Enam);
	}

	return ilRes;
}


void AvailableEquipmentViewer::UpdateView(const char *pcpViewName)
{
    SelectView(pcpViewName);
	DeleteAll();
	PrepareFilter();
	MakeLines();
	UpdateDisplay();
}




/////////////////////////////////////////////////////////////////////////////
// AvailableEquipmentViewer -- code specific to this class

void AvailableEquipmentViewer::MakeLines()
{
	int ilCount = 0;
	if(pomAvailableEquipment == NULL)
	{
		return;
	}

	ilCount = pomAvailableEquipment->GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		AVAILABLE_EQU_LINEDATA rlLine = pomAvailableEquipment->GetAt(i);
		if(IsPassFilter(&rlLine))
		{
			MakeLine(&rlLine);
		}
	}
}

bool AvailableEquipmentViewer::IsPassFilter(AVAILABLE_EQU_LINEDATA *prpLine)
{
	void *p;
	bool blIsGroupOk = false;
	bool blIsEquNameOk = false;

	EQUDATA *prlEqu = ogEquData.GetEquByUrno(prpLine->Urno);
	if(prlEqu != NULL)
	{
		if(omGroupBy == "EquipmentGroup")
		{
			ALLOCUNIT *prlEquGrp = ogAllocData.GetGroupByUnitName(ALLOCUNITTYPE_EQUIPMENTGROUP,prlEqu->Enam);
			if (prlEquGrp != NULL)
			{
				blIsGroupOk = bmUseAllEquipmentGroups || omEquipmentGroupMap.Lookup(CString(prlEquGrp->Name), p);
			}
		}
		else if(omGroupBy == "EquipmentType")
		{
			EQTDATA *prlEqt = ogEqtData.GetEqtByUrno(prlEqu->Gkey);
			if(prlEqt != NULL)
			{
				blIsGroupOk = (bmUseAllEquipmentTypes || omEquipmentTypeMap.Lookup(CString(prlEqt->Name), p)) ? true : false;
			}
		}
		void* p;
		blIsEquNameOk = bmUseAllEquipmentNames || omEquipmentNamesMap.Lookup(prlEqu->Enam,p);
	}

	return blIsGroupOk & blIsEquNameOk;
}


void AvailableEquipmentViewer::MakeLine(AVAILABLE_EQU_LINEDATA  *prpAvailable)
{
	
	//File the Line-record and Create the Line
    CreateLine(prpAvailable);
}



/////////////////////////////////////////////////////////////////////////////
// AvailableEquipmentViewer - STAFFTABLE_LINEDATA array maintenance

void AvailableEquipmentViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int AvailableEquipmentViewer::CreateLine(AVAILABLE_EQU_LINEDATA *prpAvailable)
{
    int ilLineCount = omLines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareAvailable(prpAvailable, &omLines[ilLineno]) <= 0)
            break;  // should be inserted before Lines[ilLineno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareAvailable(prpAvailable, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
#endif

    omLines.NewAt(ilLineno, *prpAvailable);

    return ilLineno;
}

void AvailableEquipmentViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}


/////////////////////////////////////////////////////////////////////////////
// AvailableEquipmentViewer - display drawing routine

// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void AvailableEquipmentViewer::UpdateDisplay()
{
	CString olHeader;
	// Name|Type|Code|Fast Links
	olHeader.Format("%s|%s|%s|%s|%s",GetString(IDS_AVEQ_NAME),GetString(IDS_AVEQ_TYPE),GetString(IDS_AVEQ_CODE),GetString(IDS_AVEQ_FASTLINKS),GetString(IDS_AVEQ_ATTRIBUTE));

	pomTable->SetHeaderFields(olHeader);

	int ilNameLen = strlen(GetString(IDS_AVEQ_NAME));
	int ilTypeLen = strlen(GetString(IDS_AVEQ_TYPE));
	int ilCodeLen = strlen(GetString(IDS_AVEQ_CODE));
	int ilFastLinkLen = strlen(GetString(IDS_AVEQ_FASTLINKS));
	int ilAttriLen = strlen(GetString(IDS_AVEQ_ATTRIBUTE));

    // Load filtered and sorted data into the table content
	pomTable->ResetContent();
	int ilNumLines = omLines.GetSize();
    for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
	{
		AVAILABLE_EQU_LINEDATA *prlLine = &omLines[ilLc];

		pomTable->AddTextLine(Format(prlLine), prlLine);
		pomTable->SetTextLineDragEnable(ilLc,FALSE);
		if(!prlLine->IsCorrectEquForDemand)
		{
			pomTable->SetTextLineColor(ilLc, RED, WHITE);
		}

		ilNameLen = max(ilNameLen,(int) strlen(prlLine->Enam));
		ilTypeLen = max(ilTypeLen,(int) strlen(prlLine->Etyp));
		ilCodeLen = max(ilCodeLen,(int) strlen(prlLine->Gcde));
		ilFastLinkLen = max(ilFastLinkLen,(int) strlen(prlLine->FastLinkEmps));
		ilAttriLen = max(ilAttriLen,(int) strlen(prlLine->EquipAttribute));
		
    }                                           

	CString olFormatList;
	olFormatList.Format("%d|%d|%d|%d|%d", ilNameLen, ilTypeLen, ilCodeLen, ilFastLinkLen,ilAttriLen);
    pomTable->SetFormatList(olFormatList);

    // Update the table content in the display
    pomTable->DisplayTable();
}

CString AvailableEquipmentViewer::Format(AVAILABLE_EQU_LINEDATA *prpLine)
{
	CString olText;
	olText.Format("%s|%s|%s|%s|%s",prpLine->Enam, prpLine->Etyp, prpLine->Gcde, prpLine->FastLinkEmps , prpLine->EquipAttribute);
    return olText;
}

static void AvailableStaffTableTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
/*    AvailableEquipmentViewer *polViewer = (AvailableEquipmentViewer *)popInstance;

	if(ipDDXType == DEMANDGANTT_SELECT)
	{
		polViewer->ProcessSelectDemand((JOBDATA *)vpDataPointer);
	}*/
}


int AvailableEquipmentViewer::GetLineCount()
{
	return omLines.GetSize();
}