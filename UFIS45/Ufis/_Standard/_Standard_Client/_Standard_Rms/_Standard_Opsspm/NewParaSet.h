// NewParaSet.h : header file
//
#ifndef _CNEWPARASET_H_
#define _CNEWPARASET_H_

/////////////////////////////////////////////////////////////////////////////
// CNewParaSet dialog

class CNewParaSet : public CDialog
{
// Construction
public:
	CNewParaSet(CWnd* pParent, char *pcpNewParaSet);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewParaSet)
	enum { IDD = IDD_NEWPARA };
	CEdit	m_ParaSet;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewParaSet)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CNewParaSet)
	afx_msg void OnKillfocusNewParaset();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	char *pcmNewParaSet;
};
#endif // _CNEWPARASET_H_