// CedaInitModuData.h

#ifndef __CEDAINITMODUDATA__
#define __CEDAINITMODUDATA__
 
#include <stdafx.h>
#include <CCSCedaData.h>

//--Class declaratino-------------------------------------------------------------------------------------------------------

//class CedaInitModuData: public CCSCedaData
class CedaInitModuData: public CCSCedaData
{
public:

// Operations
	CedaInitModuData();
	~CedaInitModuData();

	CString GetTableName(void);
	bool SendInitModu();
	CString	GetInitModuTxt();

// Variaben
	char pcmInitModuFieldList[200];
};

//---------------------------------------------------------------------------------------------------------

extern CedaInitModuData ogInitModuData;


#endif //__CEDAINITMODUDATA__
