// dbtnlist.cpp : implementation file
//


#include <stdafx.h>
#include <resource.h>
#include <ccsglobl.h>
#include <resource.h>
#include <CCSCedaCom.h>
#include <OpssPm.h>
#include <cxbutton.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <PrePlanTable.h>
#include <CCITable.h>
#include <GateTable.h>
#include <FlightJobsTable.h>
#include <CedaFlightData.h>
#include <FlightPlan.h>
#include <FlIndDemandTable.h>
#include <StaffTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <CciDiagram.h>
#include <StartDate.h>
#include <ConflictConfigTable.h>
#include <ufis.h>
#include <CCSBcHandle.h>
#include <conflict.h>
#include <ConflictTable.h>
#include <mreqtable.h>
#include <SetupDlg.h>
#include <ButtonList.h>
#include <CedaCfgData.h>
#include <dinfo.h>
#include <dataset.h>
#include <FilterData.h>
#include <SearchDlg.h>
#include <StaffDetailWindow.h>
#include <FlightDetailWindow.h>
#include <ReleaseDialog.h>
#include <ccsddx.h>
#include <BasicData.h>
#include <PrivList.h>
#include <LoadTimeSpanDlg.h>
#include <Process.h>
#include <StaffBreakViewer.h>
#include <DemandTableViewer.h>
#include <ConflictViewer.h>
#include <CedaGatData.h>
#include <CedaPstData.h>
#include <DlgSettings.h>
#include <CedaWroData.h>
#include "hinese.h"


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


extern void DeleteBrushes(void);
extern COpssPmApp theApp;
extern CCSCedaCom ogCommHandler;
extern CCSBcHandle ogBcHandle;
extern bool bgUseBCComClient;

extern bool BGS;


extern bool filter;
bool dateflag=false;
int index;

CString date;


#ifdef	IsOverlapped
#undef	IsOverlapped
#endif

template<class TYPE> bool IsOverlapped(const TYPE& start1,const TYPE& end1,const TYPE& start2,const TYPE& end2)
{
	return start1 <= end2 && start2 <= end1;	
}

/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog dialog


//extern bool filter;

CButtonListDialog::CButtonListDialog(CWnd* pParent /*=NULL*/)
    : CDialog(CButtonListDialog::IDD, pParent)
{
    //{{AFX_DATA_INIT(CButtonListDialog)
	//}}AFX_DATA_INIT
 // 	m_FlightFilter.ShowWindow(SW_HIDE);

    m_wndStaffDiagram	= NULL;
    m_wndGateDiagram	= NULL;
    m_wndPstDiagram		= NULL;
    m_wndCciDiagram		= NULL;
	m_wndPrmTable		= NULL;
	m_wndRegnDiagram	= NULL;

    m_wndStaffTable		= NULL;
    m_wndFlightPlan		= NULL;
	m_wndCCITable		= NULL;
	m_wndGateTable		= NULL;
	m_wndFlightJobsTable = NULL;
	m_wndMReqTable		= NULL;
    m_wndEquipmentDiagram	= NULL;
//	m_wndEquipmentTable		= NULL;

	m_wndFlIndDemandTable	= NULL;
    m_wndPrePlanTable		= NULL;
	m_wndPrePlanSearchTable = NULL;
	m_wndAttentionTable		= NULL;
	m_wndConflictTable		= NULL;


	m_wndConfConflictTable	= NULL;
	m_wndInfoTable			= NULL;
	m_wndSetupDlg			= NULL;
	m_wndDemandTable		= NULL;

	omPrePlanMode = FALSE;

	pomDemandTableButton = 	new CCSButtonCtrl(true);

	// initialize the timer service data members
	pomWndRequireTimer = NULL;

	omFlightPlanArray.RemoveAll();

	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDI_UFIS);

	// Preset broadcast state to not initialized
	imBroadcastState = -1;
	bmBroadcastCheckMsg = false;

	// Print breaks with reporting tool
	bmPrintBreaks = FALSE;

	// Default time span is 30 min
	omPrintBreakOffset = CTimeSpan(0,0,30,0);

	pomBreakViewer = NULL;

	pomDemandViewer = NULL;

	pomAttentionViewer = NULL;

	bmBroadcastReceived = false;

	// must be called at least because OnInitDialog will be called inside
	Create(IDD,pParent);	


	//MWO: 15.07.2004
//	ogBcHandle.AttachApplicationBCState(&imBroadcastState);
	//END MWO: 15.07.2004
}

CButtonListDialog::~CButtonListDialog(void)
{
	if (bgModal == TRUE)
		return;

	TRACE("CButtonListDialog::~CButtonListDialog\n");
	delete pomBreakViewer;
//	delete pomDemandViewer;
	delete pomAttentionViewer;
	delete pomDemandTableButton;

	//Singapore
	POSITION olPosition = NULL;
	if(pomMapDiagramToScaleDlg != NULL)
	{
		CWnd* polTimeScale = NULL;
		CString olKey;
		olPosition = pomMapDiagramToScaleDlg->GetStartPosition();
		while(olPosition != NULL)
		{
			pomMapDiagramToScaleDlg->GetNextAssoc(olPosition,olKey,(CObject*&)polTimeScale);
			if(polTimeScale != NULL && polTimeScale->m_hWnd != NULL)
			{
				polTimeScale->DestroyWindow();
				delete polTimeScale;
			}
			else if(polTimeScale != NULL && polTimeScale->m_hWnd == NULL)
			{				
				delete polTimeScale;
			}
		}
		pomMapDiagramToScaleDlg->RemoveAll();
		delete pomMapDiagramToScaleDlg;
	}
}

void CButtonListDialog::RegisterTimer(CWnd *popWnd)
{
	if (popWnd != pomWndRequireTimer)
	{
		if (::IsWindow(pomWndRequireTimer->GetSafeHwnd()))
			pomWndRequireTimer->PostMessage(WM_TIMER, 0, NULL);	
	}
	pomWndRequireTimer = popWnd;
}

void CButtonListDialog::UnRegisterTimer(CWnd *popWnd)
{
	if (popWnd == pomWndRequireTimer)
	{
		pomWndRequireTimer = NULL;
	}
}


void CButtonListDialog::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CButtonListDialog)
	DDX_Control(pDX, IDC_DATE, m_Date);
	DDX_Control(pDX, IDC_RELEASE, m_ReleaseButton);
	DDX_Control(pDX, IDC_ONLINE, m_OnlineButton);
    DDX_Control(pDX, IDC_REGNDIAGRAM, m_RegnDiagramButton);
    DDX_Control(pDX, IDC_STAFFDIAGRAM, m_StaffDiagramButton);
    DDX_Control(pDX, IDC_EQUIPMENTDIAGRAM, m_EquipmentDiagramButton);
    DDX_Control(pDX, IDC_EQUIPMENTTABLE, m_EquipmentTableButton);
    DDX_Control(pDX, IDC_GATEDIAGRAM, m_GateDiagramButton);
    DDX_Control(pDX, IDC_PSTDIAGRAM, m_PstDiagramButton);
    DDX_Control(pDX, IDC_CCIDIAGRAM, m_CciDiagramButton);
    DDX_Control(pDX, IDC_STAFFTABLE, m_StaffTableButton);
    DDX_Control(pDX, IDC_FLIGHTPLAN, m_FlightPlanButton);


    DDX_Control(pDX, IDC_FILTERF, m_FlightFilter);

	DDX_Control(pDX, IDC_FLIGHT_IND_DEMAND, m_FlIndDemandButton);
	DDX_Control(pDX, IDC_GATEBELEGUNG, m_GateTableButton);
	DDX_Control(pDX, IDC_FLIGHTJOBSTABLE, m_FlightJobsTableButton);
	DDX_Control(pDX, IDC_CCIBELEGUNG, m_CCITableButton);
    DDX_Control(pDX, IDC_REQUEST, m_RequestButton);
	DDX_Control(pDX, IDC_COVERAGE, m_CoverageButton);
    DDX_Control(pDX, IDC_PREPLANING, m_PrePlanningButton);
    DDX_Control(pDX, IDC_ATTENTION, m_AttentionTableButton);
    DDX_Control(pDX, IDC_CONFLICT, m_ConflictTableButton);
	DDX_Control(pDX, IDC_SETUP, m_SetupButton);
	DDX_Control(pDX, IDC_INFO, m_InfoButton);
    DDX_Control(pDX, IDC_HELPBUTTON, m_HelpButton);
	DDX_Control(pDX, IDC_PRINT_COMMON,m_PrintButton);
	DDX_Control(pDX, IDC_UNRESOLVED_DEMANDS,*pomDemandTableButton);
	DDX_Control(pDX, IDC_BC_STATUS, m_CB_BCStatus);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CButtonListDialog, CDialog)
    //{{AFX_MSG_MAP(CButtonListDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CREATE()
	ON_WM_DESTROY()
    //ON_WM_ERASEBKGND()
    ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_RELOAD, OnReload)
    ON_BN_CLICKED(IDC_STAFFDIAGRAM, OnStaffdiagram)
    ON_BN_CLICKED(IDC_GATEDIAGRAM, OnGatediagram)
    ON_BN_CLICKED(IDC_EQUIPMENTDIAGRAM, OnEquipmentDiagram)
    ON_BN_CLICKED(IDC_CCIDIAGRAM, OnCcidiagram)
    ON_BN_CLICKED(IDC_PSTDIAGRAM, OnPstdiagram)
    ON_BN_CLICKED(IDC_STAFFTABLE, OnStafftable)
    ON_MESSAGE(WM_STAFFTABLE_EXIT, OnStaffTableExit)
    ON_BN_CLICKED(IDC_FLIGHTPLAN, OnFlightplan)

   ON_BN_CLICKED(IDC_FILTERF, OnFilterFlight)

  
   
   
    ON_MESSAGE(WM_FLIGHTTABLE_EXIT, OnFlightplanExit)
	ON_BN_CLICKED(IDC_FLIGHT_IND_DEMAND, OnFlIndDemand)
	ON_BN_CLICKED(IDC_GATEBELEGUNG, OnGatebelegung)
	ON_MESSAGE(WM_GATETABLE_EXIT, OnGateTableExit)
	ON_BN_CLICKED(IDC_EQUIPMENTTABLE, OnEquipmentTable)
	ON_MESSAGE(WM_EQUIPMENTTABLE_EXIT, OnEquipmentTableExit)
	ON_BN_CLICKED(IDC_CCIBELEGUNG, OnCcibelegung)
	ON_MESSAGE(WM_CCITABLE_EXIT, OnCCITableExit)
	ON_BN_CLICKED(IDC_REQUEST, OnRequest)
	ON_MESSAGE(WM_REQUESTTABLE_EXIT, OnRequestTableExit)
	ON_BN_CLICKED(IDC_PEAKTABLE, OnPeaktable)
    ON_MESSAGE(WM_PEAKTABLE_EXIT, OnPeaktableExit)
	ON_BN_CLICKED(IDC_COVERAGE, OnCoverage)
    ON_MESSAGE(WM_COVERAGEEXIT, OnCoverageExit)
    ON_BN_CLICKED(IDC_PREPLANING, OnPreplaning)
    ON_MESSAGE(WM_PREPLANTABLE_EXIT, OnPrePlanTableExit)
    ON_MESSAGE(WM_PREPLANSEARCHTABLE_EXIT, OnPrePlanSearchTableExit)
	ON_BN_CLICKED(IDC_ATTENTION, OnAttention)
	ON_MESSAGE(WM_ATTENTIONTABLE_EXIT, OnAttentionTableExit)
	ON_BN_CLICKED(IDC_CONFLICT, OnConflict)
	ON_MESSAGE(WM_CONFLICTTABLE_EXIT, OnConflictTableExit)
	ON_BN_CLICKED(IDC_UNDO, OnUndo)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
	ON_BN_CLICKED(IDC_RELEASE, OnRelease)
	ON_BN_CLICKED(IDC_INFO, OnInfo)
	ON_MESSAGE(WM_INFOTABLE_EXIT, OnInfoTableExit)
	ON_BN_CLICKED(IDC_SETUP, OnSetup)
	ON_MESSAGE(WM_SETUPTABLE_EXIT, OnSetupTableExit)
    ON_BN_CLICKED(IDC_HELPBUTTON, OnHelp)
	ON_BN_CLICKED(IDC_LH_HOST, OnLhHost)
    ON_BN_CLICKED(IDC_EXIT, OnExit)
	ON_BN_CLICKED(IDC_ONLINE, OnOnline)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_REGNDIAGRAM, OnRegndiagram)
	ON_MESSAGE(WM_BCADD,OnBcAdd)
	ON_CBN_SELCHANGE(IDC_DATE, OnSelchangeDate)
	ON_BN_CLICKED(IDC_PRINT_COMMON,OnPrint)
    ON_BN_CLICKED(IDC_UNRESOLVED_DEMANDS, OnDemandTable)
	ON_MESSAGE(WM_DEMANDTABLE_EXIT, OnDemandTableExit)
	ON_BN_CLICKED(IDC_FLIGHTJOBS, OnFlightJobsTable)
	ON_MESSAGE(WM_FLIGHTJOBSTABLE_EXIT, OnFlightJobsTableExit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog message handlers

BOOL CButtonListDialog::OnInitDialog()
{
	

	
	
	static long llRealWorldUrno = 0L;
	CDialog::OnInitDialog();

	CWnd *polWnd = GetDlgItem(IDC_CHART); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32859));
	}
	polWnd = GetDlgItem(IDC_LIST); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32860));
	}
	polWnd = GetDlgItem(IDC_STAFFDIAGRAM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32861));
	}
	polWnd = GetDlgItem(IDC_STAFFTABLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32861));
	}
	polWnd = GetDlgItem(IDC_GATEDIAGRAM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32862));
	}
	polWnd = GetDlgItem(IDC_GATEBELEGUNG); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32862));
	}
	polWnd = GetDlgItem(IDC_EQUIPMENTDIAGRAM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_EQUIPMENT));
	}
	polWnd = GetDlgItem(IDC_EQUIPMENTTABLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_EQUIPMENT));
	}
	CString olTmp = GetString(IDS_STRING62866);
	int xx = IDS_STRING62866;
	olTmp = GetString(IDS_PRMTABLE);
	if (!bgIsPrm)  {
		polWnd = GetDlgItem(IDC_CCIDIAGRAM); 
		if(polWnd != NULL)
		{
			polWnd->SetWindowText(GetString(IDS_STRING32863));
		}
		polWnd = GetDlgItem(IDC_CCIBELEGUNG); 
		if(polWnd != NULL)
		{
			polWnd->SetWindowText(GetString(IDS_STRING32863));
		}
	} else
	{
		polWnd = GetDlgItem(IDC_CCIDIAGRAM); 
		if(polWnd != NULL)
		{
			if (bgIsPrm)
			{
//				polWnd->SetWindowText(GetString(IDS_STRING62866));
				polWnd->SetWindowText(GetString(IDS_PRMTABLE));
			}
			else
			{
				polWnd->SetWindowText(GetString(IDS_STRING32863));
			}
		}
	}
	polWnd = GetDlgItem(IDC_REGNDIAGRAM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32864));
	}
	polWnd = GetDlgItem(IDC_FLIGHTPLAN); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32865));
	}
	polWnd = GetDlgItem(IDC_PSTDIAGRAM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32866));
	}
	polWnd = GetDlgItem(IDC_REQUEST); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32867));
	}
	polWnd = GetDlgItem(IDC_INFO); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32868));
	}
	polWnd = GetDlgItem(IDC_COVERAGE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32869));
	}
	polWnd = GetDlgItem(IDC_PREPLANING); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32870));
	}
	polWnd = GetDlgItem(IDC_ATTENTION); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32871));
	}
	polWnd = GetDlgItem(IDC_CONFLICT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32872));
	}
	polWnd = GetDlgItem(IDC_UNDO); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32873));
	}
	polWnd = GetDlgItem(IDC_SEARCH); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32874));
	}
	polWnd = GetDlgItem(IDC_ONLINE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32875));
	}
	polWnd = GetDlgItem(IDC_RELEASE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32876));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32878));
	}
	polWnd = GetDlgItem(IDC_SETUP); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32879));
	}
	polWnd = GetDlgItem(IDC_HELPBUTTON); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32880));
	}
	polWnd = GetDlgItem(IDC_EXIT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32881));
	}
	polWnd = GetDlgItem(IDC_FLIGHT_IND_DEMAND); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33165));
	}

	polWnd = GetDlgItem(IDC_PRINT_COMMON); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}

	polWnd = GetDlgItem(IDC_UNRESOLVED_DEMANDS); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61893));
	}

	// calculate the window height
	CRect rectScreen(0, 0, ::GetSystemMetrics(SM_CXSCREEN), 56); //  ::GetSystemMetrics(SM_CYSCREEN));
	MoveWindow(&rectScreen, TRUE);

	CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;


	// moved from the constructor
    m_flagOnLine = TRUE;
    SetTimer(0, (UINT) 60 * 1000, NULL);
	SetTimer(1, 250, NULL);

	// establish watch dog ?
	char pclConfigPath[512];
	char pclBroadcastCheck[512];

    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(pcgAppName, "BROADCASTCHECK", "NO",pclBroadcastCheck, sizeof pclBroadcastCheck, pclConfigPath);
	// install watch dog as timer event 3, because timer event 2 is already used by AUTOSCROLL_TIMER_EVENT
	// we will be notified all minute
	if (stricmp(pclBroadcastCheck,"YES") == 0)
	{
		bmBroadcastCheckMsg = true;
	}
	else
	{
		bmBroadcastCheckMsg = false;
	}

	SetTimer(3, 60 * 1000,NULL);
	ogBcHandle.AddTableCommand(CString("SBCXXX"), CString("SBC"), BROADCAST_CHECK,true);
	ogCCSDdx.Register((void *)this,BROADCAST_CHECK,CString("BROADCAST_CHECK"), CString("Broadcast received"),ProcessCf);
	ogCCSDdx.Register((void *)this,DEMAND_NEW,CString("BROADCAST_CHECK"), CString("Broadcast received"),ProcessCf);
	ogCCSDdx.Register((void *)this,JOD_DELETE,CString("BROADCAST_CHECK"), CString("Broadcast received"),ProcessCf);
	imBroadcastState = 0;

	if (strcmp(ogCfgData.rmUserSetup.BKPR,"J") == 0)
	{
		bmPrintBreaks = TRUE;
		
		int ilOffset;
		if (sscanf(ogCfgData.rmUserSetup.BKPO,"%d",&ilOffset) == 1 && ilOffset >= 0)
			omPrintBreakOffset = CTimeSpan(0,0,ilOffset,0);

		pomBreakViewer = new StaffBreakDiagramViewer(omPrintBreakOffset);
		pomBreakViewer->SetViewerKey("StaffDia");
		CTime olTSStartTime = ogBasicData.GetDisplayDate() - ogBasicData.GetDisplayOffset();
		CTime olTSSEndTime  = ogBasicData.GetTimeframeEnd();
		pomBreakViewer->ChangeViewTo(ogCfgData.rmUserSetup.STCV,olTSStartTime,olTSSEndTime);
	}

	// create demand table viewer, necessary
//	if (ogBasicData.bmDisplayDemands)
//	{
//		pomDemandViewer = new DemandTableViewer();
//		pomDemandViewer->SetViewerKey("DemandTab");
//		CTime olTSStartTime = ogBasicData.GetDisplayDate() - ogBasicData.GetDisplayOffset();
//		CTime olTSSEndTime  = ogBasicData.GetTimeframeEnd();
//		pomDemandViewer->SetStartTime(olTSStartTime);
//		pomDemandViewer->SetEndTime(olTSSEndTime);
//		CString  olDemandViewName = pomDemandViewer->GetViewName();
//		if (olDemandViewName.IsEmpty() == TRUE)
//		{
//			olDemandViewName = ogCfgData.rmUserSetup.DETV;
//		}
//		
//		pomDemandViewer->ChangeViewTo(olDemandViewName);
//	}

	{
		pomAttentionViewer = new ConflictTableViewer();
		pomAttentionViewer->SetTableType(CFLTABLE_ATTENTION);
		CString  olAttentionViewName = pomAttentionViewer->GetViewName();
		if (olAttentionViewName.IsEmpty() == TRUE)
		{
			olAttentionViewName = ogCfgData.rmUserSetup.ATTV;
		}

		pomAttentionViewer->ChangeViewTo(olAttentionViewName);

	}

	CString olMainCaption;
	olMainCaption.Format(GetString(IDS_MAIN_CAPTION),pcgAppName,ogCommHandler.pcmHostName,ogCommHandler.pcmHostType);
	olMainCaption += CString("   ") + ogBasicData.GetTimeframeString();
    SetWindowText(olMainCaption);


	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon


	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	if (stricmp(pclUseBcProxy,"BCSERV") == 0)
	{
		ogCommHandler.RegisterBcWindow(this);
	}
	else
	{
		ogBcHandle.StartBc(); 
	}

	ogBcHandle.GetBc();
   
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("ButtonList"),CString("Global Date Change"), ProcessCf); // change the date of the data displayed
	if (bmPrintBreaks)
	{
		ogCCSDdx.Register(this,JOB_CHANGE,CString("Buttonlist JOB_CHANGE"), CString("JOB_CHANGE"),ProcessCf);
	}
	ogCCSDdx.Register(this,UPDATE_SETUP,CString("Buttonlist"), CString("UPDATE_SETUP"),ProcessCf);
	ogCCSDdx.Register(this,OFFLINE_CHANGES,CString("Buttonlist"), CString("Offline Changes"),ProcessCf);

	ogBasicData.SetWindowStat("BUTTONLIST IDC_STAFFDIAGRAM",&m_StaffDiagramButton);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_GATEDIAGRAM",&m_GateDiagramButton,ogBasicData.bmDisplayGates);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_GATEBELEGUNG",&m_GateTableButton,ogBasicData.bmDisplayGates);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_FLIGHTJOBSTABLE",&m_FlightJobsTableButton,ogBasicData.bmDisplayFlightJobsTable);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_EQUIPMENTDIAGRAM",&m_EquipmentDiagramButton,ogBasicData.bmDisplayEquipment);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_EQUIPMENTTABLE",&m_EquipmentTableButton,ogBasicData.bmDisplayEquipment);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_CCIDIAGRAM",&m_CciDiagramButton,ogBasicData.bmDisplayCheckins);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_CCIBELEGUNG",&m_CCITableButton,ogBasicData.bmDisplayCheckins);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_STAFFTABLE",&m_StaffTableButton);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_FLIGHTPLAN",&m_FlightPlanButton);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_PREPLANING",&m_PrePlanningButton,ogBasicData.bmDisplayPrePlanning);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_REGNDIAGRAM",&m_RegnDiagramButton,ogBasicData.bmDisplayRegistrations);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_POSITIONS",&m_PstDiagramButton,ogBasicData.bmDisplayPositions);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_UNRESOLVED_DEMANDS",pomDemandTableButton,ogBasicData.bmDisplayDemands);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_ATTENTION",&m_AttentionTableButton);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_CONFLICT",&m_ConflictTableButton);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_UNDO",GetDlgItem(IDC_UNDO));
	ogBasicData.SetWindowStat("BUTTONLIST IDC_SEARCH",GetDlgItem(IDC_SEARCH));
	ogBasicData.SetWindowStat("BUTTONLIST IDC_ONLINE",GetDlgItem(IDC_ONLINE));
	ogBasicData.SetWindowStat("BUTTONLIST IDC_ONLINE",GetDlgItem(IDC_RELEASE));
	ogBasicData.SetWindowStat("BUTTONLIST IDC_FLIGHT_IND_DEMAND",GetDlgItem(IDC_FLIGHT_IND_DEMAND));
	m_ReleaseButton.EnableWindow(FALSE); // disabled until Online/Offline button pressed
	ogBasicData.SetWindowStat("BUTTONLIST IDC_SETUP",&m_SetupButton);
	ogBasicData.SetWindowStat("BUTTONLIST IDC_HELPBUTTON",GetDlgItem(IDC_HELPBUTTON));
	ogBasicData.SetWindowStat("BUTTONLIST IDC_PRINT_COMMON",&m_PrintButton);

	if (m_PrintButton.IsWindowEnabled() || m_PrintButton.IsWindowVisible())
	{
		if (strcmp(pcgReportingPath,"NONE") == 0 || strcmp(pcgMacroPrintCommon,"NONE") == 0)
			m_PrintButton.ShowWindow(SW_HIDE);
	}


	CStringArray olTimeframeList, newlist;
	ogBasicData.GetTimeframeList(olTimeframeList);

  

	SYSTEMTIME sm = { 0 };
    GetLocalTime( &sm  );
    short year = sm.wYear; 
	

   int day= 31-(5*year/4+1)%7;
   CString s;
   s.Format("%d.10.%d",day,year);
   
  bool test=false; 
  for (int m=0;m<olTimeframeList.GetSize();m++)
  {
     if (olTimeframeList[m]==s && test==false)
	 {
	  newlist.Add(olTimeframeList[m]);
	  test=true; 
	 
	 }
   

	 if (olTimeframeList[m]==s && test==true)
	 {
	    continue;
	 
	 }

   if (olTimeframeList[m]!=s )
	 {
	  newlist.Add(olTimeframeList[m]);
	
	 
	 }
	 
  
  }


	 int ilNumDays = newlist.GetSize();
	
		
	
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(newlist[ilDay]);

		if (s==newlist[ilDay])
		{
		  dateflag=true; 
		  index=ilDay;
		 
		}
	}

    if (BGS)
	{
        SetDlgItemText(IDC_CCIBELEGUNG,GetString(IDS_61959));
        SetDlgItemText(IDC_FLIGHTJOBSTABLE,GetString(IDS_61960));
	}



	if(ilNumDays > 0)
	{
		m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
	}

	pomMapDiagramToScaleDlg = new CMapStringToOb(); //Singapore


	theApp.SetLandscape();

	m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_GREEN, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

    return TRUE;  // return TRUE  unless you set the focus to a control
}


void CButtonListDialog::OnRegndiagram() 
{

	  int nIndex = m_Date.GetCurSel();
   
     m_Date.GetLBText( nIndex, date);
	
	
	// TODO: Add your control notification handler code here
	if (m_wndRegnDiagram == NULL)  // this window didn't open yet?
	{
		m_RegnDiagramButton.Recess(TRUE);
		m_wndRegnDiagram = new RegnDiagram(omPrePlanMode,GetPrePlanTime());
	}
	else    // close window
	{
		if (m_wndRegnDiagram->DestroyWindow())
		{
			m_wndRegnDiagram = NULL;
			m_RegnDiagramButton.Recess(FALSE);
		}
	}
}

void CButtonListDialog::OnCancel()
{
    // Ignore cancel -- This makes Alt-F4 and Escape keys no effect
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CButtonListDialog::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CButtonListDialog::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

int CButtonListDialog::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	
    CRect rectScreen(0, 0, ::GetSystemMetrics(SM_CXSCREEN), 56);
    MoveWindow(&rectScreen, TRUE);
	

	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}

void CButtonListDialog::OnDestroy() 
{
	if (bgModal == TRUE)
		return;


	TRACE("CButtonListDialog::OnDestroy\n");

	// kill timers to prevent from getting broadcasts, 
	// because OnBreakPrint method will crash the application when called inside OnOnline method, 
	// due to pomViewer has no window anymore at this point
	if (::IsWindow(m_hWnd))
	{
		KillTimer(0);
		KillTimer(1);
		if (imBroadcastState != -1) // used
			KillTimer(3);
	}


	// reset the system time if CURRENTDATE is defined in ceda.ini
	if (ogSysTime.wYear != 0)
	{
		SYSTEMTIME olTmpTime;

		GetSystemTime(&olTmpTime);

		ogSysTime.wHour = olTmpTime.wHour;
		ogSysTime.wMinute = olTmpTime.wMinute;
		ogSysTime.wSecond = olTmpTime.wSecond;

		SetSystemTime(&ogSysTime);
	}


	ogCommHandler.CleanUpCom();
	ogCCSDdx.UnRegisterAll();

	CDialog::OnDestroy();
}

BOOL CButtonListDialog::OnEraseBkgnd(CDC* pDC)
{
    // determine size of the screen
    CRect rectScreen(0, 0, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));

    // determine rectangle of the updated region
    CRect rectUpdate;
    pDC->GetClipBox(&rectUpdate);

    // paint background of button-list menu area in the top of the screen
    CRect rectMenu = rectScreen;
    rectMenu.bottom = m_nDialogBarHeight;
    if (rectMenu.IntersectRect(&rectMenu, &rectUpdate))
    {
        CBrush brush;
        brush.CreateStockObject(SILVER);
        CBrush *pOldBrush = pDC->SelectObject(&brush);
        pDC->PatBlt(rectMenu.left, rectMenu.top, rectMenu.Width(), rectMenu.Height(), PATCOPY);
        pDC->SelectObject(pOldBrush);
    }

    // paint background of remaining area in the lower part of the screen
    CRect rectBody = rectScreen;
    rectBody.top = m_nDialogBarHeight;
    if (rectBody.IntersectRect(&rectBody, &rectUpdate))
    {
        CBrush brush;
        brush.CreateStockObject(SILVER);
        CBrush *pOldBrush = pDC->SelectObject(&brush);
        pDC->PatBlt(rectBody.left, rectBody.top, rectBody.Width(), rectBody.Height(), PATCOPY);
        pDC->SelectObject(pOldBrush);
    }

    return TRUE;
}

HBRUSH CButtonListDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbrush = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// Make the focus rectangle in push buttons disappeared
	//if (nCtlColor == CTLCOLOR_BTN)
	//{
	//	pDC->SetBkColor(0);
	//	pDC->SetTextColor(0);
	//}
	return hbrush;
}

void CButtonListDialog::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent == 0)	// update the timeline (every 1 minutes)
	{
		if (bgIsInitialized)
			ogConflicts.CheckAllConflicts();

		if (::IsWindow(m_wndStaffDiagram->GetSafeHwnd()))
			m_wndStaffDiagram->PostMessage(WM_TIMER, nIDEvent, 0L);
		if (! bgIsPrm ) {
		if (::IsWindow(m_wndPrmTable->GetSafeHwnd()))
			m_wndPrmTable->PostMessage(WM_TIMER, nIDEvent, 0L);
		} else {
		if (::IsWindow(m_wndCciDiagram->GetSafeHwnd()))
			m_wndCciDiagram->PostMessage(WM_TIMER, nIDEvent, 0L);
		}
		if (::IsWindow(m_wndGateDiagram->GetSafeHwnd()))
			m_wndGateDiagram->PostMessage(WM_TIMER, nIDEvent, 0L);
		if (::IsWindow(m_wndPstDiagram->GetSafeHwnd()))
			m_wndPstDiagram->PostMessage(WM_TIMER, nIDEvent, 0L);
		if (::IsWindow(m_wndRegnDiagram->GetSafeHwnd()))
			m_wndRegnDiagram->PostMessage(WM_TIMER, nIDEvent, 0L);
		if (bmPrintBreaks)
			PrintBreakJobs();			
	}
	else if (nIDEvent == 1)	// notify the registered timed window
	{
		if (::IsWindow(pomWndRequireTimer->GetSafeHwnd()))
			pomWndRequireTimer->PostMessage(WM_TIMER, nIDEvent, NULL);
	}
	else if (nIDEvent == 3)	// watch dog event
	{
		if(bmBroadcastReceived)
		{
			bmBroadcastReceived = false;
		}
		else
		{
			switch(imBroadcastState)
			{
			case 0 :	// initialized
				{
					imBroadcastState = 1;	// data sent !
					//MWO unnecessary ogBasicData.CedaAction("SBC","SBCXXX","","","","");
				}
			break;
			case 1 :	// data sent -> no broadcast message received
				{
					if(::IsWindow(m_hWnd))
						KillTimer(3);	// don't get recursive !
					m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_RED, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
					if (bmBroadcastCheckMsg)
					{
						CString olMessage(GetString(IDS_STRING61865)); 
						int ilState = AfxMessageBox(olMessage,MB_YESNO|MB_ICONSTOP);
						SetTimer(3,60 * 1000,NULL);
						if (ilState == IDYES)	// we try it again
						{
							imBroadcastState = 1;
							//MWO unnecessary ogBasicData.CedaAction("SBC","SBCXXX","","","","");
						}
						else
						{
							imBroadcastState = 1;
							//MWO unnecessary ogBasicData.CedaAction("SBC","SBCXXX","","","","");
							bmBroadcastCheckMsg = false;
						}
					}
					else	// retry !
					{
						SetTimer(3, 60 * 1000,NULL);
						imBroadcastState = 1;
						//MWO unnecessary ogBasicData.CedaAction("SBC","SBCXXX","","","","");
					}
				}
			break;
			case 2 :	// data received
				{
					imBroadcastState = 1;	// data sent !
					m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_GREEN, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
					//MWO unnecessary ogBasicData.CedaAction("SBC","SBCXXX","","","","");
				}
			break;
			case 3 :	// broadcast not received -> ignore further events
				;
			break;
			}
		}
	}

	CDialog::OnTimer(nIDEvent);
}

/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog message handlers for diagrams

void CButtonListDialog::OnStaffdiagram()
{
    
	int nIndex = m_Date.GetCurSel();
   
     m_Date.GetLBText( nIndex, date);
	
	
	
	if (m_wndStaffDiagram == NULL)  
    {
        m_StaffDiagramButton.Recess(TRUE);
		m_wndStaffDiagram = new StaffDiagram(omPrePlanMode,GetPrePlanTime());
    }
    else    // close window
    {
        if (m_wndStaffDiagram->DestroyWindow())
		{
			m_StaffDiagramButton.Recess(FALSE);
			m_wndStaffDiagram = NULL;
		}
    }
}

void CButtonListDialog::OnGatediagram()
{
    
	 int nIndex = m_Date.GetCurSel();
     m_Date.GetLBText( nIndex, date);
	
	if (m_wndGateDiagram == NULL)  // this window didn't open yet?
    {
        m_GateDiagramButton.Recess(TRUE);
		m_wndGateDiagram = new GateDiagram(omPrePlanMode,GetPrePlanTime());
    }
    else    // close window
    {
		if (m_wndGateDiagram->DestroyWindow())
		{
			m_wndGateDiagram = NULL;
			m_GateDiagramButton.Recess(FALSE);
		}
    }
}

void CButtonListDialog::OnEquipmentDiagram()
{
    
	 int nIndex = m_Date.GetCurSel();
     m_Date.GetLBText( nIndex, date);
	
	
	if (m_wndEquipmentDiagram == NULL)  // this window didn't open yet?
    {
        m_EquipmentDiagramButton.Recess(TRUE);
		m_wndEquipmentDiagram = new EquipmentDiagram(omPrePlanMode,GetPrePlanTime());
    }
    else    // close window
    {
		if (m_wndEquipmentDiagram->DestroyWindow())
		{
			m_wndEquipmentDiagram = NULL;
			m_EquipmentDiagramButton.Recess(FALSE);
		}
    }
}

void CButtonListDialog::OnCcidiagram()
{
     int nIndex = m_Date.GetCurSel();
   
     m_Date.GetLBText( nIndex, date);
	
	
	if (bgIsPrm) 
	{
		if (m_wndPrmTable == NULL)  
		{
			m_CciDiagramButton.Recess(TRUE);
			m_wndPrmTable = new PRMTable();
		}
		else    // close window
		{
		   if (m_wndPrmTable->DestroyWindow())
			{
				m_CciDiagramButton.Recess(FALSE);
				m_wndPrmTable = NULL;
			}
		}
	}
	else
	{
		if ((m_wndCciDiagram == NULL)   && (m_wndPrmTable == NULL))
		{
			m_CciDiagramButton.Recess(TRUE);
			if (!bgIsPrm) {
				m_wndCciDiagram = new CCIDiagram(omPrePlanMode,GetPrePlanTime());
			}
			else
			{
				 m_wndPrmTable = new PRMTable();
			}
		}
		else    // close window
		{
		   if (m_wndCciDiagram != NULL) 
		   {
			   if (m_wndCciDiagram->DestroyWindow())
				{
					m_CciDiagramButton.Recess(FALSE);
					m_wndCciDiagram = NULL;
				}
		   } else if (m_wndPrmTable != NULL)
		   {
			   if (m_wndPrmTable->DestroyWindow())
				{
					m_CciDiagramButton.Recess(FALSE);
					m_wndPrmTable = NULL;
				}

		   }
		}
	}
}

PstDiagram		*m_wndPstD;


void CButtonListDialog::OnPstdiagram()
{
    
     int nIndex = m_Date.GetCurSel();
   
     m_Date.GetLBText( nIndex, date);
	

	
	if (m_wndPstDiagram == NULL)  
    {
        m_PstDiagramButton.Recess(TRUE);

		
        m_wndPstDiagram = new PstDiagram(omPrePlanMode,GetPrePlanTime());

       m_wndPstD=m_wndPstDiagram;
    }
    else    // close window
    {
        if (m_wndPstDiagram->DestroyWindow())
		{
			m_PstDiagramButton.Recess(FALSE);
			m_wndPstDiagram = NULL;
			m_wndPstD=NULL;
		}
    }
}

/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog message handlers for tables

void CButtonListDialog::OnStafftable()
{
    // TODO: Add your control notification handler code here
    if (m_wndStaffTable == NULL)  // this window didn't open yet?
    {
		m_StaffTableButton.Recess(TRUE);
		m_wndStaffTable = new StaffTable();

    }
    else    // close window
    {
        if (m_wndStaffTable->DestroyWindow())
		{
			m_StaffTableButton.Recess(FALSE);
			m_wndStaffTable = NULL;
		}
    }
}

LONG CButtonListDialog::OnStaffTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
    if (m_wndStaffTable != NULL)  // this window is openning?
    {
		m_StaffTableButton.Recess(FALSE);
		m_wndStaffTable = NULL;
    }
    return 0L;
}

void CButtonListDialog::OnFlightplan()
{
//    if (m_wndFlightPlan == NULL)  // this window didn't open yet?
//    {
//		m_FlightPlanButton.Recess(TRUE);
//		m_wndFlightPlan = new FlightPlan(this,
//			ogBasicData.GetTime(), ogBasicData.GetTime());
//    }
//    else    // close window
//    {   
//		if (m_wndFlightPlan->DestroyWindow())
//		{
//			m_wndFlightPlan = NULL;
//			m_FlightPlanButton.Recess(FALSE);
//		}
//	  }

		filter=false;
	m_wndFlightPlan = new FlightPlan(this, TIMENULL, TIMENULL);
	omFlightPlanArray.Add((void *)m_wndFlightPlan);
}

void CButtonListDialog::OnDemandTable()
{
    // TODO: Add your control notification handler code here
    if (m_wndDemandTable == NULL)  // this window didn't open yet?
    {
		pomDemandTableButton->Recess(TRUE);
		m_wndDemandTable = new DemandTable(pomDemandViewer);
		SetDemandColor(::GetSysColor(COLOR_BTNFACE));

    }
    else    // close window
    {
        if (m_wndDemandTable->DestroyWindow())
		{
			pomDemandTableButton->Recess(FALSE);
			m_wndDemandTable = NULL;
		}
    }
}

LONG CButtonListDialog::OnDemandTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
    if (m_wndDemandTable != NULL)  // this window is openning?
    {
		pomDemandTableButton->Recess(FALSE);
		m_wndDemandTable = NULL;
    }

    return 0L;
}

void CButtonListDialog::OnFlIndDemand()
{
	if (m_wndFlIndDemandTable == NULL)  
	{
		m_FlIndDemandButton.Recess(TRUE);
		m_wndFlIndDemandTable = new FlIndDemandTable();
	}
	else    // close window
	{
		if (m_wndFlIndDemandTable->DestroyWindow())
		{
			m_FlIndDemandButton.Recess(FALSE);
			m_wndFlIndDemandTable = NULL;
		}
	}
	
}


void CButtonListDialog::OnCloseAllFlightPlans()
{
	for ( int ilFP = omFlightPlanArray.GetSize() - 1; ilFP >= 0; ilFP--)
	{
		FlightPlan *polFlightPlan = (FlightPlan *) omFlightPlanArray.GetAt(ilFP);
		if(polFlightPlan != NULL)
		{
			polFlightPlan->DestroyWindow();
		}
		omFlightPlanArray.RemoveAt(ilFP);
	}	
}


LONG CButtonListDialog::OnFlightplanExit(UINT /*wParam*/, LONG /*lParam*/)
{
    return 0L;
}

void CButtonListDialog::OnGatebelegung() 
{
	// TODO: Add your control notification handler code here
    if (m_wndGateTable == NULL)  
    {
        m_GateTableButton.Recess(TRUE);
        m_wndGateTable = new GateTable;
    }
    else    // close window
    {
        if (m_wndGateTable->DestroyWindow())
		{
			m_GateTableButton.Recess(FALSE);
			m_wndGateTable = NULL;
		}
    }
}

void CButtonListDialog::OnFlightJobsTable() 
{
    if (m_wndFlightJobsTable == NULL)  
    {
        m_FlightJobsTableButton.Recess(TRUE);
        m_wndFlightJobsTable = new FlightJobsTable;
    }
    else
    {
        if (m_wndFlightJobsTable->DestroyWindow())
		{
			m_FlightJobsTableButton.Recess(FALSE);
			m_wndFlightJobsTable = NULL;
		}
    }
}

void CButtonListDialog::OnEquipmentTable() 
{
	// TODO: Add your control notification handler code here
//    if (m_wndGateTable == NULL)  
//    {
//        m_EquipmentTableButton.Recess(TRUE);
//        m_wndEquipmentTable = new EquipmentTable;
//    }
//    else    // close window
//    {
//        if (m_wndEquipmentTable->DestroyWindow())
//		{
//			m_EquipmentTableButton.Recess(FALSE);
//			m_wndEquipmentTable = NULL;
//		}
//    }
}

LONG CButtonListDialog::OnGateTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
    if (m_wndGateTable != NULL)  // this window is openning?
    {
		m_GateTableButton.Recess(FALSE);
		m_wndGateTable = NULL;
    }

    return 0L;
}

LONG CButtonListDialog::OnFlightJobsTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
    if (m_wndFlightJobsTable != NULL)
    {
		m_FlightJobsTableButton.Recess(FALSE);
		m_wndFlightJobsTable = NULL;
    }

    return 0L;
}

LONG CButtonListDialog::OnEquipmentTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
//	TRACE("CButtonListDialog::OnEquipmentTableExit %x \n",m_wndEquipmentTable);
//
//    if (m_wndEquipmentTable != NULL)  // this window is openning?
//    {
//		m_EquipmentTableButton.Recess(FALSE);
//		m_wndEquipmentTable = NULL;
//    }

    return 0L;
}

void CButtonListDialog::OnCcibelegung() 
{
	// TODO: Add your control notification handler code here
    if (m_wndCCITable == NULL)  
    {
        m_CCITableButton.Recess(TRUE);
        m_wndCCITable = new CCITable(this);
    }
    else    // close window
    {
		if (m_wndCCITable->DestroyWindow())
		{
			m_CCITableButton.Recess(FALSE);
			m_wndCCITable = NULL;
		}
    }
}

LONG CButtonListDialog::OnCCITableExit(UINT /*wParam*/, LONG /*lParam*/)
{
	TRACE("CButtonListDialog::OnCCITableExit %x \n",m_wndCCITable);

    if (m_wndCCITable != NULL)  // this window is openning?
    {
		m_CCITableButton.Recess(FALSE);
		m_wndCCITable = NULL;
    }

    return 0L;
}

void CButtonListDialog::OnRequest()
{

	// TODO: Add your control notification handler code here
    if (m_wndMReqTable == NULL)  
    {
		m_RequestButton.SetColors(GetSysColor(COLOR_BTNFACE),
			::GetSysColor(COLOR_BTNSHADOW),
			::GetSysColor(COLOR_BTNHIGHLIGHT));

        m_RequestButton.Recess(TRUE);
        m_wndMReqTable = new MReqTable("R");
    }
    else    // close window
    {
		if (m_wndMReqTable->DestroyWindow())
		{
			m_RequestButton.Recess(FALSE);
			m_wndMReqTable = NULL;
			m_RequestButton.SetColors(GetSysColor(COLOR_BTNFACE),
				::GetSysColor(COLOR_BTNSHADOW),
				::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
	}	
}

LONG CButtonListDialog::OnRequestTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
    if (m_wndMReqTable != NULL)  // this window is openning?
    {
		m_RequestButton.Recess(FALSE);
		m_wndMReqTable = NULL;
    }

    return 0L;
}

void CButtonListDialog::OnPeaktable() 
{
//	ogBasicData.DumpJobs();
//
//	// TODO: Add your control notification handler code here
//    if (m_wndPeakTable == NULL)  
//    {
//		CTime opStart;
//		if (bgIsPreplanMode == TRUE)
//		{
//			 opStart = m_wndPrePlanTable->GetPrePlanTime();
//		}
//		else
//		{
//			opStart = ogBasicData.GetTime();
//		}
//
//        m_PeakTableButton.Recess(TRUE);
//        m_wndPeakTable = new PeakTable(NULL,opStart);
//    }
//    else    // close window
//    {
//		if (m_wndPeakTable->DestroyWindow())
//		{
//			m_PeakTableButton.Recess(FALSE);
//			m_wndPeakTable = NULL;
//		}
//    }
}

LONG CButtonListDialog::OnPeaktableExit(UINT /*wParam*/, LONG /*lParam*/)
{
//    if (m_wndPeakTable != NULL)  // this window is openning?
//    {
//		m_PeakTableButton.Recess(FALSE);
//		m_wndPeakTable = NULL;
//    }

    return 0L;
}

/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog message handlers for middle group buttons

void CButtonListDialog::OnCoverage()
{
//	CTime CallTime;
//	if (bgIsPreplanMode)
//	{
//		CallTime = ogBasicData.GetTime();
//		CallTime += CTimeSpan(1,0,0,0);
//	}
//	else
//	{
//		CallTime = ogBasicData.GetTime();
//	}
//
//
//		CallTime = CTime( CallTime.GetYear(),
//											CallTime.GetMonth(),
//											CallTime.GetDay(),
//											0, 0, 0);
//
//
//
//	DWORD style = WS_OVERLAPPEDWINDOW | WS_VISIBLE;
//
//    if (m_wndCoverage == NULL)  
//    {
//       m_CoverageButton.Recess(TRUE);
//       m_wndCoverage = new CCoverageDlg();
//		m_wndCoverage->SetStartTime(CallTime);
//		m_wndCoverage->Create();
//
//	//	m_wndCoverage->CovMenu.LoadMenu(MY_CCSCOV);
//	//	m_wndCoverage->Create(NULL,"Coverage",style);
//	//	m_wndCoverage->SetMenu(&m_wndCoverage->CovMenu);
//	//	m_wndCoverage->CoverageNotify(ogBasicData.GetTime(),"Pool A        ");
//		
///***
//
//		m_wndCoverage->OnAnsichtGate();
//		m_wndCoverage->OnStartzeit0600();
//		m_wndCoverage->OnBereichBereich1();
//		m_wndCoverage->OnZeitinterval15min();
// 		m_wndCoverage->UpdateCoverage();
//******************/
//    }
//    else    // close window
//    {
//        if (m_wndCoverage->DestroyWindow())
//		{
//			m_wndCoverage = NULL;
//	        m_CoverageButton.Recess(FALSE);
//		}
//    }
}

LONG CButtonListDialog::OnCoverageExit(UINT /*wParam*/, LONG /*lParam*/)
{
//	TRACE("CButtonListDialog::OnCoverageExit %x \n",m_wndCoverage);
//
//    if (m_wndCoverage != NULL)  // this window is openning?
//    {
//        m_CoverageButton.Recess(FALSE);
//        m_wndCoverage = NULL;
//    }

    return 0L;
}

CTime CButtonListDialog::GetPrePlanTime()
{
	CTime olPrePlanTime = TIMENULL;

	if (m_wndPrePlanTable != NULL && ::IsWindow(m_wndPrePlanTable->GetSafeHwnd()))
	{
		olPrePlanTime = m_wndPrePlanTable->GetPrePlanTime();
	}

	return olPrePlanTime;
}

void CButtonListDialog::OnPreplaning()
{
    if (m_wndPrePlanTable == NULL)  // this window didn't open yet?
    {
        m_PrePlanningButton.Recess(TRUE);
        m_wndPrePlanTable = new PrePlanTable( "" );
//		omPrePlanMode = TRUE;
//		bgIsPreplanMode = TRUE;
//		CWnd *polWnd = GetDlgItem(IDC_STAFFDIAGRAM); 
//		if(polWnd != NULL && polWnd->IsWindowEnabled())
//		{
//			if (::IsWindow(m_wndStaffDiagram->GetSafeHwnd()))
//			{
//				m_wndStaffDiagram->PrePlanMode(TRUE,GetPrePlanTime());
//			}
//		}
//		if(ogBasicData.bmDisplayGates)
//		{
//			polWnd = GetDlgItem(IDC_GATEDIAGRAM); 
//			if(polWnd != NULL && polWnd->IsWindowEnabled())
//			{
//				if (::IsWindow(m_wndGateDiagram->GetSafeHwnd()))
//				{
//					m_wndGateDiagram->PrePlanMode(TRUE,GetPrePlanTime());
//				}
//			}
//		}
//		if(ogBasicData.bmDisplayCheckins)
//		{
//			polWnd = GetDlgItem(IDC_CCIDIAGRAM); 
//			if(polWnd != NULL && polWnd->IsWindowEnabled())
//			{
//				if (::IsWindow(m_wndCciDiagram->GetSafeHwnd()))
//				{
//					m_wndCciDiagram->PrePlanMode(TRUE,GetPrePlanTime());
//				}
//			}
//		}
//		if(ogBasicData.bmDisplayRegistrations)
//		{
//			polWnd = GetDlgItem(IDC_REGNDIAGRAM); 
//			if(polWnd != NULL && polWnd->IsWindowEnabled())
//			{
//				if (::IsWindow(m_wndRegnDiagram->GetSafeHwnd()))
//				{
//					m_wndRegnDiagram->PrePlanMode(TRUE,GetPrePlanTime());
//				}
//			}
//		}
//		if(ogBasicData.bmDisplayPositions)
//		{
//			polWnd = GetDlgItem(IDC_PSTDIAGRAM); 
//			if(polWnd != NULL && polWnd->IsWindowEnabled())
//			{
//				if (::IsWindow(m_wndPstDiagram->GetSafeHwnd()))
//				{
//					m_wndPstDiagram->PrePlanMode(TRUE,GetPrePlanTime());
//				}
//			}
//		}
    }
    else    // close window
    {
        if (m_wndPrePlanTable->DestroyWindow())
		{
			CleanupOnPrePlanExit();
		}
    }
}

LONG CButtonListDialog::OnPrePlanTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
	TRACE("CButtonListDialog::OnPrePlanTableExit %x \n",m_wndPrePlanTable);

    if (m_wndPrePlanTable != NULL)  // this window is open?
    {
		CleanupOnPrePlanExit();
    }

    return 0L;
}

void CButtonListDialog::CleanupOnPrePlanExit()
{
	m_wndPrePlanTable = NULL;
	m_PrePlanningButton.Recess(FALSE);
	omPrePlanMode = FALSE;
//	if (::IsWindow(m_wndStaffDiagram->GetSafeHwnd()))
//	{
//		m_wndStaffDiagram->PrePlanMode(FALSE,ogBasicData.GetTime());
//	}
//	if (::IsWindow(m_wndGateDiagram->GetSafeHwnd()))
//	{
//		m_wndGateDiagram->PrePlanMode(FALSE,ogBasicData.GetTime());
//	}
//	if (::IsWindow(m_wndCciDiagram->GetSafeHwnd()))
//	{
//		m_wndCciDiagram->PrePlanMode(FALSE,ogBasicData.GetTime());
//	}
//	if (::IsWindow(m_wndRegnDiagram->GetSafeHwnd()))
//	{
//		m_wndRegnDiagram->PrePlanMode(FALSE,ogBasicData.GetTime());
//	}
//	if (::IsWindow(m_wndPstDiagram->GetSafeHwnd()))
//	{
//		m_wndPstDiagram->PrePlanMode(FALSE,ogBasicData.GetTime());
//	}
}

LONG CButtonListDialog::OnPrePlanSearchTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
	TRACE("CButtonListDialog::OnPrePlanSearchTableExit %x \n",m_wndPrePlanSearchTable);
	m_wndPrePlanSearchTable = NULL;
    return 0L;
}


void CButtonListDialog::OnAttention() 
{
    if (m_wndAttentionTable == NULL)  
    {
        m_AttentionTableButton.Recess(TRUE);
        m_wndAttentionTable = new ConflictTable(CFLTABLE_ATTENTION,pomAttentionViewer);

		SetAttentionColor(::GetSysColor(COLOR_BTNFACE));

    }
    else    // close window
    {
		if (m_wndAttentionTable->DestroyWindow())
		{
			m_AttentionTableButton.Recess(FALSE);
			m_wndAttentionTable = NULL;
		}
    }
}

LONG CButtonListDialog::OnAttentionTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
    if (m_wndAttentionTable != NULL)  // this window is openning?
    {
		m_AttentionTableButton.Recess(FALSE);
		m_wndAttentionTable = NULL;
    }

    return 0L;
}

void CButtonListDialog::OnConflict() 
{
    if (m_wndConflictTable == NULL)  
    {
        m_ConflictTableButton.Recess(TRUE);
        m_wndConflictTable = new ConflictTable(CFLTABLE_CONFLICT);
    }
    else    // close window
    {
        if (m_wndConflictTable->DestroyWindow())
		{
			m_wndConflictTable = NULL;
			m_ConflictTableButton.Recess(FALSE);
		}
    }
}

LONG CButtonListDialog::OnConflictTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
    if (m_wndConflictTable != NULL)  // this window is openning?
    {
		m_ConflictTableButton.Recess(FALSE);
		m_wndConflictTable = NULL;
    }

    return 0L;
}

/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog message handlers for other buttons


void CButtonListDialog::OnUndo()
{
	ogUndoManager.DoModalUndoDialog(this);
}


void CButtonListDialog::OnSearch() 
{
	if (BGS)
	{
		AfxGetApp()->DoWaitCursor(1);
	CSearchDlg olSearchDlg;
	CString olWhat;
	CString olText, olPosition, olGate;

//	olWhat = GetString(IDS_SDKEY_GATES); // "Gates"
//	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_GATE), 10);
//	olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_GATEGANTT));
//	olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_GATELIST));
//	int ilNumGates = ogGatData.omData.GetSize();
//	for(int ilG = 0; ilG < ilNumGates; ilG++)
//	{
//		GATDATA *prlGat = &ogGatData.omData[ilG];
//		olSearchDlg.AddData(olWhat, prlGat->Gnam, prlGat->Gnam, prlGat->Urno);
//	}
//
//	olWhat = GetString(IDS_SDKEY_POSITIONS); // "Positions"
//	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_POSITION), 10);
//	olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_PSTGANTT));
//	int ilNumPositions = ogPstData.omData.GetSize();
//	for(int ilP = 0; ilP < ilNumPositions; ilP++)
//	{
//		PSTDATA *prlPst = &ogPstData.omData[ilP];
//		olSearchDlg.AddData(olWhat, prlPst->Pnam, prlPst->Pnam, prlPst->Urno);
//	}

	if(ogBasicData.bmDisplayEquipment)
	{
		CStringArray olEquFields;
		olWhat = "Equipment"; // "Equipment"
		olSearchDlg.AddSearchField(olWhat, "Name", 8);
		olSearchDlg.AddSearchField(olWhat, "Code", 8);
		olSearchDlg.AddSearchField(olWhat, "Type", 8);
		olSearchDlg.AddWhere(olWhat, "Equipment Chart");
		int ilNumEqu = ogEquData.omData.GetSize();
		for(int ilEqu = 0; ilEqu < ilNumEqu; ilEqu++)
		{
			EQUDATA *prlEqu = &ogEquData.omData[ilEqu];
			olEquFields.RemoveAll();
			olEquFields.Add(prlEqu->Enam);
			olEquFields.Add(prlEqu->Gcde);
			olEquFields.Add(prlEqu->Etyp);
			olText.Format("%s / %s / %s",prlEqu->Enam,prlEqu->Gcde,prlEqu->Etyp);
			olSearchDlg.AddData(olWhat, olEquFields, olText, prlEqu->Urno);
		}
	}

	olWhat = "Flights"; // "Flights"
	olSearchDlg.AddSearchField(olWhat, "Code", 3);
	olSearchDlg.AddSearchField(olWhat, "Number", 6);
	olSearchDlg.AddSearchField(olWhat, "Suffix", 2);
	olSearchDlg.AddSearchField(olWhat, "Callsign", 9);
	olSearchDlg.AddSearchField(olWhat, "Gate", 4);
	olSearchDlg.AddSearchField(olWhat, "Position", 4);
	if(ogBasicData.bmDisplayGates)
	{
		olSearchDlg.AddWhere(olWhat, "Gate Gantt");
	}
	if(ogBasicData.bmDisplayPositions)
	{
		olSearchDlg.AddWhere(olWhat, "Position Gantt");
	}
	if(ogBasicData.bmDisplayFlightJobsTable)
	{
		olSearchDlg.AddWhere(olWhat, "Flight Related Jobs List");
	}

	CMapPtrToPtr olArrReturnFlightIds, olDepReturnFlightIds;
	int ilNextReturnFlightId = -1;
	long llFlur = 0L;

	olSearchDlg.AddWhere(olWhat, "Flight List");
	olSearchDlg.AddWhere(olWhat, "Flight Detail Dialog");
	int ilNumFlights = ogFlightData.omData.GetSize();
	CStringArray olFlightFields;
	for(int ilF = 0; ilF < ilNumFlights; ilF++)
	{
		FLIGHTDATA *prlFlight = &ogFlightData.omData[ilF];

		olFlightFields.RemoveAll();
		olText = prlFlight->Alc2;
		olSearchDlg.AddSearchFieldChoice(olText, prlFlight->Alc3);
		olFlightFields.Add(olText);
		olFlightFields.Add(prlFlight->Fltn);
		olFlightFields.Add(prlFlight->Flns);
		olFlightFields.Add(prlFlight->Csgn);
		olGate = ogFlightData.GetGate(prlFlight);
		olFlightFields.Add(olGate);
		olPosition = ogFlightData.GetPosition(prlFlight);
		olFlightFields.Add(olPosition);

		CTime olFlightTime = ogFlightData.GetFlightTime(prlFlight);

		CString olFlightKey;
		if(ogFlightData.IsReturnFlight(prlFlight))
		{
			olArrReturnFlightIds.SetAt((void *) --ilNextReturnFlightId, (void *) prlFlight->Urno);
			olText.Format("%s/%s %s %s %s (Arr)  %s  G:%s P:%s", prlFlight->Alc2, prlFlight->Alc3, prlFlight->Fltn, prlFlight->Flns, prlFlight->Csgn,
							prlFlight->Tifa.Format("%d.%m.%Y %H:%M"),olGate, olPosition);
			olSearchDlg.AddData(olWhat, olFlightFields, olText, ilNextReturnFlightId, olFlightTime);

			olDepReturnFlightIds.SetAt((void *) --ilNextReturnFlightId, (void *) prlFlight->Urno);
			olText.Format("%s/%s %s %s %s (Dep)  %s  G:%s P:%s", prlFlight->Alc2, prlFlight->Alc3, prlFlight->Fltn, prlFlight->Flns,  prlFlight->Csgn,
							prlFlight->Tifd.Format("%d.%m.%Y %H:%M"),olGate, olPosition);
			olSearchDlg.AddData(olWhat, olFlightFields, olText, ilNextReturnFlightId, olFlightTime);
		}
		else
		{
			olText.Format("%s/%s %s %s %s %s  %s  G:%s P:%s", prlFlight->Alc2, prlFlight->Alc3, prlFlight->Fltn, prlFlight->Flns,  prlFlight->Csgn,
				ogFlightData.IsArrival(prlFlight) ? "(Arr)" : "(Dep)", olFlightTime.Format("%d.%m.%Y %H:%M"),olGate, olPosition);
			olSearchDlg.AddData(olWhat, olFlightFields, olText, prlFlight->Urno, olFlightTime);
		}
	}

	olWhat = "Employees"; // "Employees"
	CStringArray olEmpFields;
	CString olFunction;
	olSearchDlg.AddSearchField(olWhat, "Last Name", 6);
	olSearchDlg.AddSearchField(olWhat, "First Name", 6);
	olSearchDlg.AddSearchField(olWhat, "Nickname:", 4);
	olSearchDlg.AddSearchField(olWhat, "Initials:", 3);
	olSearchDlg.AddSearchField(olWhat, "Personnel no.", 6);
	olSearchDlg.AddSearchField(olWhat, "Shift Code", 4);
	olSearchDlg.AddSearchField(olWhat, "Function", 4);
	olSearchDlg.AddWhere(olWhat, "Employee Gantt");
	olSearchDlg.AddWhere(olWhat, "Employee List");
	olSearchDlg.AddWhere(olWhat, "Employee Detail Dialog");
	olSearchDlg.AddWhere(olWhat, "Preplanning List");
	int ilNumShifts = ogShiftData.omData.GetSize();
	for(int ilS = 0; ilS < ilNumShifts; ilS++)
	{
		olEmpFields.RemoveAll();
		SHIFTDATA *prlShift = &ogShiftData.omData[ilS];
		EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlShift->Stfu);
		if(prlEmp != NULL)
		{
			olFunction = ogBasicData.GetFunctionForShift(prlShift);
			olEmpFields.Add(prlEmp->Lanm);
			olEmpFields.Add(prlEmp->Finm);
			olEmpFields.Add(prlEmp->Shnm);
			olEmpFields.Add(prlEmp->Perc);
			olEmpFields.Add(prlEmp->Peno);
			olEmpFields.Add(prlShift->Sfca);
			olEmpFields.Add(olFunction);
			olText.Format("%s   %s   %s   %s   %s   %s   %s   %s-%s", prlEmp->Lanm, prlEmp->Finm, prlEmp->Shnm, prlEmp->Perc, prlEmp->Peno, prlShift->Sfca, olFunction, prlShift->Avfa.Format("%H:%M/%d"),  prlShift->Avta.Format("%H:%M/%d"));
			olSearchDlg.AddData(olWhat, olEmpFields, olText, prlShift->Urno, prlShift->Avfa, prlShift->Avta);
		}
	}

	AfxGetApp()->DoWaitCursor(-1);
	if(olSearchDlg.DoModal() != IDCANCEL)
	{
		CString olWhat, olWhere, olDisplayData;
		long llUserData; // eg URNO of selected object
		CTime olWhen; // day selected in the search dialog
		CStringArray olSearchStrings; // the values that were found eg. Alc2/Fltn/Flsn/Gate/Pos etc
		CTime olDate1, olDate2;

		olEmpFields.RemoveAll();
		olSearchDlg.GetSelectedData(olWhat, olWhere, olWhen, llUserData, olSearchStrings, olDisplayData, olDate1, olDate2);

	//	CString s; s.Format("%s",olWhen);
		
	

		
		if(olWhat == "Flights")
		{
			int ilReturnFlightType = ID_NOT_RETURNFLIGHT;
			if(olArrReturnFlightIds.Lookup((void *) llUserData, (void *&) llFlur))
			{
				ilReturnFlightType = ID_ARR_RETURNFLIGHT;
				llUserData = llFlur;
			}
			else if(olDepReturnFlightIds.Lookup((void *) llUserData, (void *&) llFlur))
			{
				ilReturnFlightType = ID_DEP_RETURNFLIGHT;
				llUserData = llFlur;
			}

			FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(llUserData);
			if(prlFlight == NULL)
			{
				CString olErr;
				olErr.Format("Flight for URNO <%d> not found!\n",llUserData);
				MessageBox(olErr,"Internal Error",MB_ICONERROR);
			}
			else
			{
				prlFlight->ReturnFlightType = ilReturnFlightType;

				if(olWhere == "Flight Detail Dialog")
				{
					if(ilReturnFlightType == ID_DEP_RETURNFLIGHT)
						new FlightDetailWindow(this,0L,llUserData);
					else
						new FlightDetailWindow(this,llUserData,0L);
				}
				else if(olWhere == "Flight List")
				{
				    if(omFlightPlanArray.GetSize() <= 0)
					{
						m_wndFlightPlan = new FlightPlan(this, olDate1, olDate1, FALSE, FALSE);
						omFlightPlanArray.Add((void *)m_wndFlightPlan);
					}
					else
					{
						m_wndFlightPlan->SetDate(olDate1);
					}
					ogCCSDdx.DataChanged(this,FLIGHT_SELECT_FLIGHTLIST,prlFlight);
				}
				else if(olWhere == "Position Gantt")
				{
					if (m_wndPstDiagram == NULL)  
					{
						m_PstDiagramButton.Recess(TRUE);
						m_wndPstDiagram = new PstDiagram(FALSE,olDate1);
					}
					ogCCSDdx.DataChanged(this,FLIGHT_SELECT_PSTGANTT,prlFlight);
				}
				else if(olWhere == "Flight Related Jobs List")
				{
				    if(m_wndFlightJobsTable == NULL)
					{
						m_FlightJobsTableButton.Recess(TRUE);
						m_wndFlightJobsTable = new FlightJobsTable;
					}
					else
					{
						m_wndFlightJobsTable->SetDate(olDate1);
					}
					ogCCSDdx.DataChanged(this,FLIGHT_SELECT_FLIGHTJOBSTABLE,prlFlight);
				}
				else if(olWhere == "Gate Gantt")
				{
					if (m_wndGateDiagram == NULL)  
					{
						m_GateDiagramButton.Recess(TRUE);
						m_wndGateDiagram = new GateDiagram(FALSE,olDate1);
					}
					//Singapore
					else
					{
						if(ogBasicData.IsPaxServiceT2() == true)
						{
							m_wndGateDiagram->SetDate(olDate1);
						}
					}
					ogCCSDdx.DataChanged(this,FLIGHT_SELECT_GATEGANTT,prlFlight);
				}
				ogCCSDdx.DataChanged(this,FLIGHT_SELECT,prlFlight);
			}
		}
		else if(olWhat == "Employees")
		{
			if(olWhere == "Employee Gantt")
			{
				SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(llUserData);
				if(prlShift)
				{
					
				///	m_wndStaffDiagram = new StaffDiagram(FALSE,olDate1);
					
					
					if (m_wndStaffDiagram == NULL)  
					{
					//	this->OnStaffdiagram();

                        int nIndex = m_Date.GetCurSel();
                        m_Date.GetLBText( nIndex, date);
                        m_StaffDiagramButton.Recess(TRUE);
	                 	m_wndStaffDiagram = new StaffDiagram(omPrePlanMode,GetPrePlanTime());
                        m_wndStaffDiagram->DestroyWindow();
	
					//	m_StaffDiagramButton.Recess(TRUE);
						m_wndStaffDiagram = new StaffDiagram(FALSE,olDate1);
					}
					//Singapore
					else
					{
                      
					
						if(ogBasicData.IsPaxServiceT2() == true)
						{
							m_wndStaffDiagram->SetDate(olDate1);
						}
					}
					ogCCSDdx.DataChanged(this,SHIFT_SELECT,prlShift);
				}
			}
			else if(olWhere == "Employee Detail Dialog")
			{
				CCSPtrArray <JOBDATA> olPoolJobs;
				ogJobData.GetJobsByShur(olPoolJobs,llUserData,true);
				if(olPoolJobs.GetSize() > 0)
				{
					new StaffDetailWindow(this,olPoolJobs[0].Urno);
				}
				else
				{
					CString olText;
					olText.Format("%s\n%s",olDisplayData,GetString(IDS_SD_EMPNOTINPOOL));
					MessageBox(olText,GetString(IDS_SD_TITLE),MB_ICONSTOP);
				}
			}
			else if(olWhere == "Employee List")
			{
				SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(llUserData);
				if(prlShift)
				{
					if(m_wndStaffTable == NULL)
					{
						m_StaffTableButton.Recess(TRUE);
						m_wndStaffTable = new StaffTable(prlShift->Avfa);
					}
					else
					{
						m_wndStaffTable->SetDate(prlShift->Avfa);
					}
					ogCCSDdx.DataChanged(this,SHIFT_SELECT_EMPLIST,prlShift);
				}
			}
			else if(olWhere == "Preplanning List")
			{
				SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(llUserData);
				if(prlShift)
				{
					if(m_wndPrePlanTable == NULL)
					{
						m_PrePlanningButton.Recess(TRUE);
						m_wndPrePlanTable = new PrePlanTable( "" );
					}
					else
					{
						m_wndPrePlanTable->SetDate(prlShift->Avfa);
					}
					ogCCSDdx.DataChanged(this,SHIFT_SELECT_PREPLAN,prlShift);
				}
			}
		}
		else if(olWhat == GetString(IDS_SDKEY_GATES))
		{
		}
		else if(olWhat == GetString(IDS_SDKEY_POSITIONS))
		{
		}
		else if(olWhat == "Equipment")
		{
			if(olWhere == "Equipment Chart")
			{
				EQUDATA *prlEqu = ogEquData.GetEquByUrno(llUserData);
				if(prlEqu)
				{
					if (m_wndEquipmentDiagram == NULL)  
					{
						m_EquipmentDiagramButton.Recess(TRUE);
						m_wndEquipmentDiagram = new EquipmentDiagram(FALSE,olDate1);
					}
					ogCCSDdx.DataChanged(this,EQUIPMENT_SELECT,prlEqu);
				}
			}
		}
		else
		{
			CString olErr;
			olErr.Format("The following \"What\" condition is not handled <%s>",olWhat);
			MessageBox(olErr,"Internal Error",MB_ICONERROR);
		}
	}
	
	}

	else   // not BGS
	{
	
	 AfxGetApp()->DoWaitCursor(1);
	CSearchDlg olSearchDlg;
	CString olWhat;
	CString olText, olPosition, olGate;

//	olWhat = GetString(IDS_SDKEY_GATES); // "Gates"
//	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_GATE), 10);
//	olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_GATEGANTT));
//	olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_GATELIST));
//	int ilNumGates = ogGatData.omData.GetSize();
//	for(int ilG = 0; ilG < ilNumGates; ilG++)
//	{
//		GATDATA *prlGat = &ogGatData.omData[ilG];
//		olSearchDlg.AddData(olWhat, prlGat->Gnam, prlGat->Gnam, prlGat->Urno);
//	}
//
//	olWhat = GetString(IDS_SDKEY_POSITIONS); // "Positions"
//	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_POSITION), 10);
//	olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_PSTGANTT));
//	int ilNumPositions = ogPstData.omData.GetSize();
//	for(int ilP = 0; ilP < ilNumPositions; ilP++)
//	{
//		PSTDATA *prlPst = &ogPstData.omData[ilP];
//		olSearchDlg.AddData(olWhat, prlPst->Pnam, prlPst->Pnam, prlPst->Urno);
//	}

	if(ogBasicData.bmDisplayEquipment)
	{
		CStringArray olEquFields;
		olWhat = GetString(IDS_SDKEY_EQUIPMENT); // "Equipment"
		olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_EQUIPMENTNAME), 8);
		olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_EQUIPMENTCODE), 8);
		olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_EQUIPMENTTYPE), 8);
		olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_EQUGANTT));
		int ilNumEqu = ogEquData.omData.GetSize();
		for(int ilEqu = 0; ilEqu < ilNumEqu; ilEqu++)
		{
			EQUDATA *prlEqu = &ogEquData.omData[ilEqu];
			olEquFields.RemoveAll();
			olEquFields.Add(prlEqu->Enam);
			olEquFields.Add(prlEqu->Gcde);
			olEquFields.Add(prlEqu->Etyp);
			olText.Format("%s / %s / %s",prlEqu->Enam,prlEqu->Gcde,prlEqu->Etyp);
			olSearchDlg.AddData(olWhat, olEquFields, olText, prlEqu->Urno);
		}
	}

	olWhat = GetString(IDS_SDKEY_FLIGHTS); // "Flights"
	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_CODE), 3);
	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_NUMBER), 6);
	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_SUFFIX), 2);
	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_CALLSIGN), 9);
	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_GATE), 4);
	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_POSITION), 4);
	if(ogBasicData.bmDisplayGates)
	{
		olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_GATEGANTT));
	}
	if(ogBasicData.bmDisplayPositions)
	{
		olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_PSTGANTT));
	}
	if(ogBasicData.bmDisplayFlightJobsTable)
	{
		olSearchDlg.AddWhere(olWhat, GetString(IDS_FLIGHTJOBSTABLE_TITLE));
	}

	CMapPtrToPtr olArrReturnFlightIds, olDepReturnFlightIds;
	int ilNextReturnFlightId = -1;
	long llFlur = 0L;

	olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_FLIGHTLIST));
	olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_FLIGHTDETAIL));
	int ilNumFlights = ogFlightData.omData.GetSize();
	CStringArray olFlightFields;
	for(int ilF = 0; ilF < ilNumFlights; ilF++)
	{
		FLIGHTDATA *prlFlight = &ogFlightData.omData[ilF];

		olFlightFields.RemoveAll();
		olText = prlFlight->Alc2;
		olSearchDlg.AddSearchFieldChoice(olText, prlFlight->Alc3);
		olFlightFields.Add(olText);
		olFlightFields.Add(prlFlight->Fltn);
		olFlightFields.Add(prlFlight->Flns);
		olFlightFields.Add(prlFlight->Csgn);
		olGate = ogFlightData.GetGate(prlFlight);
		olFlightFields.Add(olGate);
		olPosition = ogFlightData.GetPosition(prlFlight);
		olFlightFields.Add(olPosition);

		CTime olFlightTime = ogFlightData.GetFlightTime(prlFlight);

		CString olFlightKey;
		if(ogFlightData.IsReturnFlight(prlFlight))
		{
			olArrReturnFlightIds.SetAt((void *) --ilNextReturnFlightId, (void *) prlFlight->Urno);
			olText.Format("%s/%s %s %s %s (Arr)  %s  G:%s P:%s", prlFlight->Alc2, prlFlight->Alc3, prlFlight->Fltn, prlFlight->Flns, prlFlight->Csgn,
							prlFlight->Tifa.Format("%d.%m.%Y %H:%M"),olGate, olPosition);
			olSearchDlg.AddData(olWhat, olFlightFields, olText, ilNextReturnFlightId, olFlightTime);

			olDepReturnFlightIds.SetAt((void *) --ilNextReturnFlightId, (void *) prlFlight->Urno);
			olText.Format("%s/%s %s %s %s (Dep)  %s  G:%s P:%s", prlFlight->Alc2, prlFlight->Alc3, prlFlight->Fltn, prlFlight->Flns,  prlFlight->Csgn,
							prlFlight->Tifd.Format("%d.%m.%Y %H:%M"),olGate, olPosition);
			olSearchDlg.AddData(olWhat, olFlightFields, olText, ilNextReturnFlightId, olFlightTime);
		}
		else
		{
			olText.Format("%s/%s %s %s %s %s  %s  G:%s P:%s", prlFlight->Alc2, prlFlight->Alc3, prlFlight->Fltn, prlFlight->Flns,  prlFlight->Csgn,
				ogFlightData.IsArrival(prlFlight) ? "(Arr)" : "(Dep)", olFlightTime.Format("%d.%m.%Y %H:%M"),olGate, olPosition);
			olSearchDlg.AddData(olWhat, olFlightFields, olText, prlFlight->Urno, olFlightTime);
		}
	}

	olWhat = GetString(IDS_SDKEY_EMPLOYEES); // "Employees"
	CStringArray olEmpFields;
	CString olFunction;
	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_LASTNAME), 6);
	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_FIRSTNAME), 6);
	olSearchDlg.AddSearchField(olWhat, GetString(IDS_NCD_NICKNAME), 4);
	olSearchDlg.AddSearchField(olWhat, GetString(IDS_NCD_INITIALS), 3);
	olSearchDlg.AddSearchField(olWhat, GetString(IDS_NCD_PERSONNELNO), 6);
	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_SHIFTCODE), 4);
	olSearchDlg.AddSearchField(olWhat, GetString(IDS_SDFLD_FUNCTION), 4);
	olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_EMPGANTT));
	olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_EMPLIST));
	olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_EMPDETAIL));
	olSearchDlg.AddWhere(olWhat, GetString(IDS_SDWHERE_PREPLAN));
	int ilNumShifts = ogShiftData.omData.GetSize();
	for(int ilS = 0; ilS < ilNumShifts; ilS++)
	{
		olEmpFields.RemoveAll();
		SHIFTDATA *prlShift = &ogShiftData.omData[ilS];
		EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlShift->Stfu);
		if(prlEmp != NULL)
		{
			olFunction = ogBasicData.GetFunctionForShift(prlShift);
			olEmpFields.Add(prlEmp->Lanm);
			olEmpFields.Add(prlEmp->Finm);
			olEmpFields.Add(prlEmp->Shnm);
			olEmpFields.Add(prlEmp->Perc);
			olEmpFields.Add(prlEmp->Peno);
			olEmpFields.Add(prlShift->Sfca);
			olEmpFields.Add(olFunction);
			olText.Format("%s   %s   %s   %s   %s   %s   %s   %s-%s", prlEmp->Lanm, prlEmp->Finm, prlEmp->Shnm, prlEmp->Perc, prlEmp->Peno, prlShift->Sfca, olFunction, prlShift->Avfa.Format("%H:%M/%d"),  prlShift->Avta.Format("%H:%M/%d"));
			olSearchDlg.AddData(olWhat, olEmpFields, olText, prlShift->Urno, prlShift->Avfa, prlShift->Avta);
		}
	}

	AfxGetApp()->DoWaitCursor(-1);
	if(olSearchDlg.DoModal() != IDCANCEL)
	{
		CString olWhat, olWhere, olDisplayData;
		long llUserData; // eg URNO of selected object
		CTime olWhen; // day selected in the search dialog
		CStringArray olSearchStrings; // the values that were found eg. Alc2/Fltn/Flsn/Gate/Pos etc
		CTime olDate1, olDate2;

		olEmpFields.RemoveAll();
		olSearchDlg.GetSelectedData(olWhat, olWhere, olWhen, llUserData, olSearchStrings, olDisplayData, olDate1, olDate2);

	//	CString s; s.Format("%s",olWhen);
		
	

		
		if(olWhat == GetString(IDS_SDKEY_FLIGHTS))
		{
			int ilReturnFlightType = ID_NOT_RETURNFLIGHT;
			if(olArrReturnFlightIds.Lookup((void *) llUserData, (void *&) llFlur))
			{
				ilReturnFlightType = ID_ARR_RETURNFLIGHT;
				llUserData = llFlur;
			}
			else if(olDepReturnFlightIds.Lookup((void *) llUserData, (void *&) llFlur))
			{
				ilReturnFlightType = ID_DEP_RETURNFLIGHT;
				llUserData = llFlur;
			}

			FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(llUserData);
			if(prlFlight == NULL)
			{
				CString olErr;
				olErr.Format("Flight for URNO <%d> not found!\n",llUserData);
				MessageBox(olErr,"Internal Error",MB_ICONERROR);
			}
			else
			{
				prlFlight->ReturnFlightType = ilReturnFlightType;

				if(olWhere == GetString(IDS_SDWHERE_FLIGHTDETAIL))
				{
					if(ilReturnFlightType == ID_DEP_RETURNFLIGHT)
						new FlightDetailWindow(this,0L,llUserData);
					else
						new FlightDetailWindow(this,llUserData,0L);
				}
				else if(olWhere == GetString(IDS_SDWHERE_FLIGHTLIST))
				{
				    if(omFlightPlanArray.GetSize() <= 0)
					{
						m_wndFlightPlan = new FlightPlan(this, olDate1, olDate1, FALSE, FALSE);
						omFlightPlanArray.Add((void *)m_wndFlightPlan);
					}
					else
					{
						m_wndFlightPlan->SetDate(olDate1);
					}
					ogCCSDdx.DataChanged(this,FLIGHT_SELECT_FLIGHTLIST,prlFlight);
				}
				else if(olWhere == GetString(IDS_SDWHERE_PSTGANTT))
				{
					if (m_wndPstDiagram == NULL)  
					{
						m_PstDiagramButton.Recess(TRUE);
						m_wndPstDiagram = new PstDiagram(FALSE,olDate1);
					}
					ogCCSDdx.DataChanged(this,FLIGHT_SELECT_PSTGANTT,prlFlight);
				}
				else if(olWhere == GetString(IDS_FLIGHTJOBSTABLE_TITLE))
				{
				    if(m_wndFlightJobsTable == NULL)
					{
						m_FlightJobsTableButton.Recess(TRUE);
						m_wndFlightJobsTable = new FlightJobsTable;
					}
					else
					{
						m_wndFlightJobsTable->SetDate(olDate1);
					}
					ogCCSDdx.DataChanged(this,FLIGHT_SELECT_FLIGHTJOBSTABLE,prlFlight);
				}
				else if(olWhere == GetString(IDS_SDWHERE_GATEGANTT))
				{
					if (m_wndGateDiagram == NULL)  
					{
						m_GateDiagramButton.Recess(TRUE);
						m_wndGateDiagram = new GateDiagram(FALSE,olDate1);
					}
					//Singapore
					else
					{
						if(ogBasicData.IsPaxServiceT2() == true)
						{
							m_wndGateDiagram->SetDate(olDate1);
						}
					}
					ogCCSDdx.DataChanged(this,FLIGHT_SELECT_GATEGANTT,prlFlight);
				}
				ogCCSDdx.DataChanged(this,FLIGHT_SELECT,prlFlight);
			}
		}
		else if(olWhat == GetString(IDS_SDKEY_EMPLOYEES))
		{
			if(olWhere == GetString(IDS_SDWHERE_EMPGANTT))
			{
				SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(llUserData);
				if(prlShift)
				{
					
				///	m_wndStaffDiagram = new StaffDiagram(FALSE,olDate1);
					
					
					if (m_wndStaffDiagram == NULL)  
					{
					//	this->OnStaffdiagram();

                        int nIndex = m_Date.GetCurSel();
                        m_Date.GetLBText( nIndex, date);
                        m_StaffDiagramButton.Recess(TRUE);
	                 	m_wndStaffDiagram = new StaffDiagram(omPrePlanMode,GetPrePlanTime());
                        m_wndStaffDiagram->DestroyWindow();
	
					//	m_StaffDiagramButton.Recess(TRUE);
						m_wndStaffDiagram = new StaffDiagram(FALSE,olDate1);
					}
					//Singapore
					else
					{
                      
					
						if(ogBasicData.IsPaxServiceT2() == true)
						{
							m_wndStaffDiagram->SetDate(olDate1);
						}
					}
					ogCCSDdx.DataChanged(this,SHIFT_SELECT,prlShift);
				}
			}
			else if(olWhere == GetString(IDS_SDWHERE_EMPDETAIL))
			{
				CCSPtrArray <JOBDATA> olPoolJobs;
				ogJobData.GetJobsByShur(olPoolJobs,llUserData,true);
				if(olPoolJobs.GetSize() > 0)
				{
					new StaffDetailWindow(this,olPoolJobs[0].Urno);
				}
				else
				{
					CString olText;
					olText.Format("%s\n%s",olDisplayData,GetString(IDS_SD_EMPNOTINPOOL));
					MessageBox(olText,GetString(IDS_SD_TITLE),MB_ICONSTOP);
				}
			}
			else if(olWhere == GetString(IDS_SDWHERE_EMPLIST))
			{
				SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(llUserData);
				if(prlShift)
				{
					if(m_wndStaffTable == NULL)
					{
						m_StaffTableButton.Recess(TRUE);
						m_wndStaffTable = new StaffTable(prlShift->Avfa);
					}
					else
					{
						m_wndStaffTable->SetDate(prlShift->Avfa);
					}
					ogCCSDdx.DataChanged(this,SHIFT_SELECT_EMPLIST,prlShift);
				}
			}
			else if(olWhere == GetString(IDS_SDWHERE_PREPLAN))
			{
				SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(llUserData);
				if(prlShift)
				{
					if(m_wndPrePlanTable == NULL)
					{
						m_PrePlanningButton.Recess(TRUE);
						m_wndPrePlanTable = new PrePlanTable( "" );
					}
					else
					{
						m_wndPrePlanTable->SetDate(prlShift->Avfa);
					}
					ogCCSDdx.DataChanged(this,SHIFT_SELECT_PREPLAN,prlShift);
				}
			}
		}
		else if(olWhat == GetString(IDS_SDKEY_GATES))
		{
		}
		else if(olWhat == GetString(IDS_SDKEY_POSITIONS))
		{
		}
		else if(olWhat == GetString(IDS_SDKEY_EQUIPMENT))
		{
			if(olWhere == GetString(IDS_SDWHERE_EQUGANTT))
			{
				EQUDATA *prlEqu = ogEquData.GetEquByUrno(llUserData);
				if(prlEqu)
				{
					if (m_wndEquipmentDiagram == NULL)  
					{
						m_EquipmentDiagramButton.Recess(TRUE);
						m_wndEquipmentDiagram = new EquipmentDiagram(FALSE,olDate1);
					}
					ogCCSDdx.DataChanged(this,EQUIPMENT_SELECT,prlEqu);
				}
			}
		}
		else
		{
			CString olErr;
			olErr.Format("The following \"What\" condition is not handled <%s>",olWhat);
			MessageBox(olErr,"Internal Error",MB_ICONERROR);
		}
	}
	
	
	}

	

}


void CButtonListDialog::OnOnline() 
{
	
	HandleOnline();
}

bool CButtonListDialog::HandleOnline(void)
{
	int ilReply = IDNO;
	if(bgOnline)
	{
		bgOnline = false;
		ogJobData.SetOffline();
		ogJodData.SetOffline();
		m_OnlineButton.SetWindowText(GetString(IDS_STRING61302)); // "OFFLINE"
		m_OnlineButton.SetColors(YELLOW,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
		//ogBasicData.SetWindowStat("BUTTONLIST IDC_RELEASE",GetDlgItem(IDC_RELEASE));
	}
	else
	{
		bool blReleaseChanges = false;
		bool blChanges = false;
		if(ogJobData.JobsChangedOffline() || ogJodData.JodsChangedOffline())
		{
			blChanges = true;

			// "Do you want to save the jobs changed whilst working in offline mode?"
			if((ilReply = MessageBox(GetString(IDS_STRING61315),"",MB_ICONEXCLAMATION|MB_YESNOCANCEL)) == IDYES)
			{
				ReleaseDialog olDlg;
				if((ilReply = olDlg.DoModal()) == IDOK)
				{
					blReleaseChanges = true;
				}
			}
		}

		if(ilReply != IDCANCEL)
		{
			bgOnline = true;

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			ogJobData.SetOnline(blReleaseChanges);
			ogJodData.SetOnline(blReleaseChanges);
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			m_OnlineButton.SetWindowText(GetString(IDS_STRING61301)); // "Online"
			m_OnlineButton.SetColors(::GetSysColor(COLOR_BTNFACE),::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
			m_ReleaseButton.EnableWindow(FALSE);

			if(blChanges)
			{
				ogConflicts.CheckAllConflicts(TRUE);
				ogCCSDdx.DataChanged(this,REDISPLAY_ALL,NULL);
			}
		}
	}

	ogCCSDdx.DataChanged(this,ONLINE_STATUS_CHANGE,NULL);
	return ilReply != IDCANCEL ? true : false;
}

void CButtonListDialog::OnRelease() 
{
	bool blJobChanges = ogJobData.JobsChangedOffline();
	bool blJodChanges = ogJodData.JodsChangedOffline();
	if(blJobChanges || blJodChanges)
	{
		ReleaseDialog olDlg;
		if(olDlg.DoModal() == IDOK)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			if(blJodChanges)
				ogJodData.ReleaseJodsChangedOffline();
			if(blJobChanges)
				ogJobData.ReleaseJobsChangedOffline();
			ogConflicts.CheckAllConflicts(TRUE);
			ogCCSDdx.DataChanged(this,REDISPLAY_ALL,NULL);
			m_ReleaseButton.EnableWindow(FALSE);

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		}
	}
	else
	{
		// there where no changes
		MessageBox(GetString(IDS_NOOFFLINECHANGES),"",MB_ICONINFORMATION);
		m_ReleaseButton.EnableWindow(FALSE);
	}
	
}

void CButtonListDialog::OnInfo()
{
	// TODO: Add your control notification handler code here
    if (m_wndInfoTable == NULL)  
    {
		m_InfoButton.SetColors(GetSysColor(COLOR_BTNFACE),
			::GetSysColor(COLOR_BTNSHADOW),
			::GetSysColor(COLOR_BTNHIGHLIGHT));

        m_InfoButton.Recess(TRUE);
        m_wndInfoTable = new MReqTable("I");
    }
    else    // close window
    {
		if (m_wndInfoTable->DestroyWindow())
		{
			m_InfoButton.Recess(FALSE);
			m_wndInfoTable = NULL;
			m_InfoButton.SetColors(GetSysColor(COLOR_BTNFACE),
				::GetSysColor(COLOR_BTNSHADOW),
				::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
    }
}

LONG CButtonListDialog::OnInfoTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
    if (m_wndInfoTable != NULL)  // this window is openning?
    {
		m_InfoButton.Recess(FALSE);
		m_wndInfoTable = NULL;
    }

    return 0L;
}

/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog message handlers for the rightmost group buttons

void CButtonListDialog::OnSetup() 
{
	m_wndSetupDlg = new SetupDlg();
	m_wndSetupDlg->DoModal();
	delete m_wndSetupDlg;
	m_wndSetupDlg = NULL;
}

LONG CButtonListDialog::OnSetupTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
    if (m_wndConfConflictTable != NULL)  // this window is openning?
    {
		m_SetupButton.Recess(FALSE);
		m_wndConfConflictTable = NULL;
    }

    return 0L;
}

void CButtonListDialog::OnHelp()
{
//		ogBcHandle.GetBc();

	AfxGetApp()->WinHelp(0,HELP_CONTENTS);
}

void CButtonListDialog::OnLhHost() 
{
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));
	char pclTmpText[512];
	GetPrivateProfileString(pcgAppName, "LH-HOST", "",
		pclTmpText, sizeof pclTmpText, pclConfigPath);
	char pclTmpMode[28];
	GetPrivateProfileString(pcgAppName, "LH-HOST-MODE", "NORMAL",
		pclTmpMode, sizeof pclTmpMode, pclConfigPath);

	if (*pclTmpText != '\0')
	{
		UINT ulCmdShow;
		if (strcmp(pclTmpMode,"MINIMIZED") == 0)
			ulCmdShow = SW_SHOWMINIMIZED;
		else
		if (strcmp(pclTmpMode,"MAXIMIZED") == 0)
			ulCmdShow = SW_SHOWMAXIMIZED;
		else
			ulCmdShow = SW_SHOWNORMAL;

		WinExec(pclTmpText,ulCmdShow);
	}
}


void CButtonListDialog::OnExit()
{
	if (ExitCommitted())
	{
		ogOldSearchValues.SaveOldSearchValuesToDb();
		ogDlgSettings.SaveSettingsToDb();
		ogBasicData.DumpRereadStatistics();

		if(m_wndStaffDiagram)
		{
			m_wndStaffDiagram->DestroyWindow();
			m_wndStaffDiagram = NULL;
		}
		if(m_wndPrePlanTable)
		{
			m_wndPrePlanTable->DestroyWindow();
			m_wndPrePlanTable = NULL;
		}
		if(m_wndPrePlanSearchTable)
		{
			m_wndPrePlanSearchTable->DestroyWindow();
			m_wndPrePlanSearchTable = NULL;
		}
		if(m_wndStaffTable)
		{
			m_wndStaffTable->DestroyWindow();
			m_wndStaffTable = NULL;
		}
		if(m_wndFlIndDemandTable)
		{
			m_wndFlIndDemandTable->DestroyWindow();
			m_wndFlIndDemandTable = NULL;
		}
		if(m_wndCciDiagram)
		{
			m_wndCciDiagram->DestroyWindow();
			m_wndCciDiagram = NULL;
		}
		if(m_wndPrmTable)
		{
			m_wndPrmTable->DestroyWindow();
			m_wndPrmTable = NULL;
		}

		if(m_wndGateDiagram)
		{
			m_wndGateDiagram->DestroyWindow();
			m_wndGateDiagram = NULL;
		}
		if(m_wndEquipmentDiagram)
		{
			m_wndEquipmentDiagram->DestroyWindow();
			m_wndEquipmentDiagram = NULL;
		}
		if(m_wndPstDiagram)
		{
			m_wndPstDiagram->DestroyWindow();
			m_wndPstDiagram = NULL;
		}
		if(m_wndRegnDiagram)
		{
			m_wndRegnDiagram->DestroyWindow();
			m_wndRegnDiagram = NULL;
		}
		if(m_wndConfConflictTable)
		{
			m_wndConfConflictTable->DestroyWindow();
			m_wndConfConflictTable = NULL;
		}
		if(m_wndAttentionTable)
		{
			m_wndAttentionTable->DestroyWindow();
			m_wndAttentionTable = NULL;
		}
		if(m_wndConflictTable)
		{
			m_wndConflictTable->DestroyWindow();
			m_wndConflictTable = NULL;
		}
		if(m_wndCCITable)
		{
			m_wndCCITable->DestroyWindow();
			m_wndCCITable = NULL;
		}
		if(m_wndGateTable)
		{
			m_wndGateTable->DestroyWindow();
			m_wndGateTable = NULL;
		}
		if(m_wndFlightJobsTable)
		{
			m_wndFlightJobsTable->DestroyWindow();
			m_wndFlightJobsTable = NULL;
		}
		if(m_wndMReqTable)
		{
			m_wndMReqTable->DestroyWindow();
			m_wndMReqTable = NULL;
		}
		if(m_wndInfoTable)
		{
			m_wndInfoTable->DestroyWindow();
			m_wndInfoTable = NULL;
		}
		if(m_wndSetupDlg)
		{
			m_wndSetupDlg->DestroyWindow();
			m_wndSetupDlg = NULL;
		}
		if(m_wndDemandTable)
		{
			m_wndDemandTable->DestroyWindow();
			m_wndDemandTable = NULL;
		}

		OnCloseAllFlightPlans();

		CWnd *polMainFrame = AfxGetMainWnd();
		if (polMainFrame)
			polMainFrame->DestroyWindow();
	}
}

/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog helper routines

LONG CButtonListDialog::OnBcAdd(UINT wParam, LONG lParam)
{
	TRACE("Broadcast num %d\n",wParam);
	bmBroadcastReceived = true;
	

	m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_YELLOW, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_BCStatus.UpdateWindow();

	bool blStatus = ogBcHandle.GetBc(wParam);

	imBroadcastState = 2;
	bmBroadcastReceived = true;
	
	m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_GREEN, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_BCStatus.UpdateWindow();
			
	return TRUE;
}

void CButtonListDialog::StartBroadcastReceived(void)
{
	bmBroadcastReceived = true;
	imBroadcastState = 2;
	m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_YELLOW, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_BCStatus.UpdateWindow();
}

void CButtonListDialog::EndBroadcastReceived(void)
{
	m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_GREEN, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_BCStatus.UpdateWindow();
}

void CButtonListDialog::SetRequestColor(COLORREF lpColor)
{
	m_RequestButton.SetColors(lpColor,
		::GetSysColor(COLOR_BTNSHADOW),
		::GetSysColor(COLOR_BTNHIGHLIGHT));
}

void CButtonListDialog::SetInfoColor(COLORREF lpColor)
{
	m_InfoButton.SetColors(lpColor,
		::GetSysColor(COLOR_BTNSHADOW),
		::GetSysColor(COLOR_BTNHIGHLIGHT));
}

void CButtonListDialog::SetAttentionColor(COLORREF lpColor)
{
	m_AttentionTableButton.SetColors(lpColor,
		::GetSysColor(COLOR_BTNSHADOW),
		::GetSysColor(COLOR_BTNHIGHLIGHT));
}

void CButtonListDialog::SetConflictColor(COLORREF lpColor)
{
	m_ConflictTableButton.SetColors(lpColor,
		::GetSysColor(COLOR_BTNSHADOW),
		::GetSysColor(COLOR_BTNHIGHLIGHT));
}

void CButtonListDialog::SetDemandColor(COLORREF lpColor)
{
	pomDemandTableButton->SetColors(lpColor,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
}

void  CButtonListDialog::ProcessCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogCfgData.ProcessCfgBc(ipDDXType,vpDataPointer,ropInstanceName);

	CButtonListDialog *polButtonList = (CButtonListDialog *)vpInstance;
	if(ipDDXType == GLOBAL_DATE_UPDATE)
	{
		polButtonList->HandleGlobalDateUpdate();
	}
	else if (ipDDXType == JOB_CHANGE)
	{
		polButtonList->HandleJobChange((JOBDATA *)vpDataPointer);
	}
	else if (ipDDXType == UPDATE_SETUP)
	{
		polButtonList->HandleSetupChange();
	}
	else if (ipDDXType == BROADCAST_CHECK)
	{
		struct BcStruct *prlRequestData;
		prlRequestData = (struct BcStruct *) vpDataPointer;
		//ogBasicData.LogBroadcast(prlRequestData);
	}
	else if (ipDDXType == OFFLINE_CHANGES)
	{
		ogBasicData.SetWindowStat("BUTTONLIST IDC_RELEASE",polButtonList->GetDlgItem(IDC_RELEASE));
	}
	else if (ipDDXType == DEMAND_NEW || ipDDXType == JOD_DELETE)
	{
		if(polButtonList->m_wndDemandTable == NULL)
		{
			polButtonList->SetDemandColor(RED);
		}
	}

}

void CButtonListDialog::HandleSetupChange()
{
	if (strcmp(ogCfgData.rmUserSetup.BKPR,"J") == 0)
	{
		bmPrintBreaks = TRUE;
		
		int ilOffset;
		if (sscanf(ogCfgData.rmUserSetup.BKPO,"%d",&ilOffset) == 1 && ilOffset >= 0)
			omPrintBreakOffset = CTimeSpan(0,0,ilOffset,0);
		if (!pomBreakViewer)
		{
			pomBreakViewer = new StaffBreakDiagramViewer(omPrintBreakOffset);
			pomBreakViewer->SetViewerKey("StaffDia");
		}
		CTime olTSStartTime = ogBasicData.GetDisplayDate() - ogBasicData.GetDisplayOffset();
		CTime olTSSEndTime  = ogBasicData.GetTimeframeEnd();
		pomBreakViewer->ChangeViewTo(ogCfgData.rmUserSetup.STCV,olTSStartTime,olTSSEndTime);
	}
	else
		bmPrintBreaks = false;

		
}

void CButtonListDialog::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnClose();
}

void CButtonListDialog::OnOK() 
{
	// TODO: Add extra validation here
	
//	CDialog::OnOK();
}


void CButtonListDialog::OnReload(void)
{
	// load time spans
	char clStat = ogPrivList.GetStat("LOAD_TIMESPAN_DLG");

	if(clStat != '-') // not hidden
	{
		bool blAllFieldsDisabled = (clStat == '1' ? false : true);
		CLoadTimeSpanDlg olLoadTimeSpanDlg(NULL,ogBasicData.omLoadTimeValues,ogBasicData.bmLoadRel,blAllFieldsDisabled);
		if(olLoadTimeSpanDlg.DoModal() == IDOK)
		{
			ogBasicData.omLoadTimeValues.RemoveAll();
			for(int illc = 0; illc < olLoadTimeSpanDlg.omValues.GetSize() ;illc++)
			{
				ogBasicData.omLoadTimeValues.Add(olLoadTimeSpanDlg.omValues.GetAt(illc));
			}
			ogBasicData.bmLoadRel = (olLoadTimeSpanDlg.m_AbsTimeVal == 1) ? true : false;
			ogBasicData.SetTimeframe(olLoadTimeSpanDlg.omStartTime,olLoadTimeSpanDlg.omEndTime);
			ogBasicData.SetTemplateFilters(olLoadTimeSpanDlg.omTplUrnos);
			pogInitialLoad = new InitialLoadDlg();

			bgIsInitialized = FALSE;
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			SetLoadMessage(GetString(IDS_STRING61664)); // "Daten werden geladen"

			
			// Checkin counter jobs
			if (ogCfgData.rmUserSetup.OPTIONS.CCA != USERSETUPDATA::NONE)
			{
				ogCcaData.ReadCcaData(ogBasicData.GetTimeframeStart(),ogBasicData.GetTimeframeEnd());
				SetLoadMessage("",10); // ""
			}

			// Flight Data
			SetLoadMessage(GetString(IDS_STRING61668)); // "Fl�ge werden geladen"
			ogFlightData.ReadAllFlights(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
			SetLoadMessage("",40); // ""

			// Demands
			SetLoadMessage(GetString(IDS_STRING61669)); // "Bedarfe werden geladen"
			ogDemandData.ReadAllDemands(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc(),olLoadTimeSpanDlg.omTplUrnos);
			SetLoadMessage("",25); // ""

			// Jobs
			SetLoadMessage(GetString(IDS_STRING61671)); // "Eins�tze werden geladen"
			ogJobData.ReadAllJobs(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
			SetLoadMessage("",25); // ""

			if (bgIsPrm)
			{
				// PRM
				SetLoadMessage(GetString(IDS_STRING61679)); // "PRM Requests werden geladen"
				ogDpxData.ReadDpxData(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
				SetLoadMessage("",25); // ""
				SetLoadMessage(GetString(IDS_STRING61679)); // "Warteraeume werden geladen"
				ogWroData.ReadWroData();
				SetLoadMessage("",25); // ""

			}
			ogBasicData.CreateAlocUnitToPoolMap();
			ogConflicts.CheckAllConflicts(TRUE);


			ogConflicts.CheckAllConflicts(TRUE);

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
			bgIsInitialized = TRUE;

			if(pogInitialLoad != NULL)
			{
				pogInitialLoad->DestroyWindow();
				pogInitialLoad = NULL;
			}
		}
	}
}


void CButtonListDialog::SetLoadMessage(const char *pcpMessage, int ipProgress /*=0*/)
{
	if(pogInitialLoad != NULL)
	{
		if(strlen(pcpMessage) > 0)
		{
			pogInitialLoad->SetMessage(pcpMessage);
		}
		if(ipProgress > 0)
		{
			pogInitialLoad->SetProgress(ipProgress);
		}
	}
}

void CButtonListDialog::OnClickMenu(UINT upMenuItem)
{
	switch (upMenuItem)
	{
	case ID_CHART_CCI:
		OnCcidiagram();
		break;
	
	case ID_CHART_GATES:
		OnGatediagram();
		break;

	case ID_CHART_STAFF:
		OnStaffdiagram();
		break;

	case ID_CHART_REGN:
		OnRegndiagram();
		break;

	case ID_LIST_CCI:
		OnCcibelegung();
		break;

	case ID_LIST_STAFF:
		OnStafftable();
		break;

	case ID_LIST_GATE:
		OnGatebelegung();
		break;

	case ID_FLIGHTJOBS:
		OnFlightJobsTable();
		break;

	case ID_LIST_ATTENTION:
		OnAttention();
		break;

	case ID_LIST_CONFLICT:
		OnConflict();
		break;

	case ID_LIST_PREPLAN:
		OnPreplaning();
		break;

	case ID_LIST_FLIGHT:
		OnFlightplan();
		break;

	case ID_CHART_POS:
		OnPstdiagram();
		break;

	case ID_CHART_EQU:
		OnEquipmentDiagram();
		break;

	case ID_LIST_CLOSEALLSCHEDULES:
		OnCloseAllFlightPlans();
		break;

	case ID_LIST_DEMANDS:
		OnDemandTable();
		break;
	default:
		break;
	}

}


void CButtonListDialog::OnSelchangeDate() 
{
	// setting the time on the main frame results in the date being changed so that all other diagrams/gantts are synchronised

	
	CTime olDay = ogBasicData.GetTimeframeStart() + CTimeSpan(m_Date.GetCurSel(),0,0,0);
	
    if (dateflag && m_Date.GetCurSel()>index)
	{
	 olDay = ogBasicData.GetTimeframeStart() + CTimeSpan(m_Date.GetCurSel()+1,0,0,0);
	
	}




	
	ogBasicData.SynchronizeDisplayDates(olDay);
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void CButtonListDialog::HandleGlobalDateUpdate()
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
}

void CButtonListDialog::OnPrint()
{
	char *args[3];
	args[0] = pcgReportingPath;
	args[1] = pcgMacroPrintCommon;
	args[2] = NULL;
	
	int ilReturn = _spawnv(_P_NOWAIT,pcgReportingPath,args);
	if (ilReturn == -1)	// process not started
	{
		CString olMsg;
		olMsg.Format(GetString(IDS_STRING61867),errno);			
		AfxMessageBox(olMsg,MB_ICONSTOP|MB_OK);
	}
}

void CButtonListDialog::HandleJobChange(JOBDATA *vpJobData)
{
	if (!bmPrintBreaks)
		return;

	if (!vpJobData)
		return;

	// non printed break jobs only
	if (strcmp(vpJobData->Jtco,JOBBREAK) == 0)
	{
		CTime olStartTime = ogBasicData.GetTime();
		CTime olEndTime   = olEndTime + omPrintBreakOffset;
		if (IsOverlapped(olStartTime,olEndTime,vpJobData->Acfr,vpJobData->Acto))
			PrintBreakJob(vpJobData);				
	}
}

void CButtonListDialog::PrintBreakJobs()
{
	// check if current view matches the staff diagram view
	if (pomBreakViewer && m_wndStaffDiagram)
	{
		if (m_wndStaffDiagram->omCurrentView != pomBreakViewer->SelectView())
		{
			CTime olStartTime = ogBasicData.GetDisplayDate() - ogBasicData.GetDisplayOffset();
			CTime olEndTime   = ogBasicData.GetTimeframeEnd();
			pomBreakViewer->ChangeViewTo(m_wndStaffDiagram->omCurrentView,olStartTime,olEndTime);
		}
	}

	CCSPtrArray<JOBDATA> olBreakJobs;
	if (pomBreakViewer && pomBreakViewer->GetBreakJobsNotPrinted(olBreakJobs))
	{
		bmPrintBreaks = false;	// don't handle job changes at the moment
		for (int ilC = 0; ilC < olBreakJobs.GetSize(); ilC++)
		{
			JOBDATA *prlJobData = &olBreakJobs[ilC];
			if (!prlJobData)
				continue;

			if (!PrintBreakJob(prlJobData))
				break;
		}		
		bmPrintBreaks = true;
	}

	olBreakJobs.DeleteAll();
}

BOOL CButtonListDialog::PrintBreakJob(JOBDATA *prlJobData)
{
	if (!prlJobData)
		return FALSE;

	CString olEmployeePeno;
	olEmployeePeno.Format("STF.PKNO=%s",prlJobData->Peno);

	EMPDATA *prlEmpData = ogEmpData.GetEmpByPeno(prlJobData->Peno);
	if (prlEmpData)
	{
		ogBasicData.Trace("Name = %s\n",ogEmpData.GetEmpName(prlEmpData->Urno));
	}

	CString olStartTime;
	olStartTime.Format("JOB.ACFR=%s",prlJobData->Acfr.Format("%Y%m%d%H%M%S"));

	CTimeSpan olJobDuration = prlJobData->Acto - prlJobData->Acfr;

	CString olEndTime;
	olEndTime.Format("JOB.ACTO=%s",prlJobData->Acto.Format("%Y%m%d%H%M%S"));

	char *args[10];
	args[0] = pcgReportingPath;
	args[1] = pcgMacroPrintBreak;
	args[2] = olEmployeePeno.GetBuffer(0);
	args[3] = olStartTime.GetBuffer(0);
	args[4] = olEndTime.GetBuffer(0);
	args[5] = NULL;
			
	int ilReturn = _spawnv(_P_NOWAIT,pcgReportingPath,args);
	if (ilReturn == -1)	// process not started
	{
		CString olMsg;
		olMsg.Format(GetString(IDS_STRING61867),errno);			
		AfxMessageBox(olMsg,MB_ICONSTOP|MB_OK);
		return FALSE;
	}

	if (!prlJobData->Printed)
	{
		JOBDATA rlOldJob = *prlJobData;
		prlJobData->Printed = true;
		ogJobData.ChangeJobData(prlJobData, &rlOldJob, "PrntBrk");
	}
	return TRUE;
}

void CButtonListDialog::InitializeViewer()
{
//	if (pomDemandViewer)
//	{
//		CString  olDemandViewName = pomDemandViewer->GetViewName();
//		if (olDemandViewName.IsEmpty() == TRUE)
//		{
//			olDemandViewName = ogCfgData.rmUserSetup.DETV;
//		}
//		
//		pomDemandViewer->ChangeViewTo(olDemandViewName);
//	}

	if (pomBreakViewer)
	{
		CTime olTSStartTime = ogBasicData.GetDisplayDate() - ogBasicData.GetDisplayOffset();
		CTime olTSSEndTime  = ogBasicData.GetTimeframeEnd();
		pomBreakViewer->ChangeViewTo(ogCfgData.rmUserSetup.STCV,olTSStartTime,olTSSEndTime);
	}

	if (pomAttentionViewer)
	{
		CString  olAttentionViewName = pomAttentionViewer->GetViewName();
		if (olAttentionViewName.IsEmpty() == TRUE)
		{
			olAttentionViewName = ogCfgData.rmUserSetup.ATTV;
		}

		pomAttentionViewer->ChangeViewTo(olAttentionViewName);
		// ignore previous check results
		SetAttentionColor(::GetSysColor(COLOR_BTNFACE));
		ogConflicts.DeleteFromAttentionMap(TIMENULL);
	}
}

BOOL CButtonListDialog::ExitCommitted()
{
	if(bgPrintPreviewMode == TRUE)
	{
		return TRUE;
	}
	CString olMsg;
	olMsg.Format(GetString(IDS_STRING62500));			
	if(AfxMessageBox(olMsg,MB_ICONQUESTION|MB_YESNO) == IDNO)
	{
		return FALSE;
	}
	else
	{
		// save offline data, if any
		if (!bgOnline)
		{
			if(!HandleOnline())
				return FALSE;
		}
		return TRUE;
	}

	return FALSE;
}

void CButtonListDialog::UpdateTrafficLight(int ipState)
{
	if (::IsWindow(m_hWnd)) // after closing OPSSPM, this callback function may still be called causing crash in KillTimer()
	{
		KillTimer(3);
		if(ipState == 0)
		{
			bmBroadcastReceived = true;

			m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_YELLOW, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			m_CB_BCStatus.UpdateWindow();
		}
		else if(ipState == 1)
		{
			imBroadcastState = 2;
			m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_GREEN, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			m_CB_BCStatus.UpdateWindow();
		}

		SetTimer(3, 60 * 1000,NULL);
	}
}

void CButtonListDialog::OnFilterFlight()
{
	
	filter=true;  

	
	m_wndFlightPlan = new FlightPlan(this, TIMENULL, TIMENULL);
	omFlightPlanArray.Add((void *)m_wndFlightPlan);


}


