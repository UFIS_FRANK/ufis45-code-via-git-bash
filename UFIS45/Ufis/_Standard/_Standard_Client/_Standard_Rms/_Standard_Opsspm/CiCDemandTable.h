// CiCDemandTable.h : header file
//
#ifndef __CICDEMANDTABLE_H__
#define __CICDEMANDTABLE_H__

#include <CicDemandTableViewer.h>
#include <CCSDragDropCtrl.h>

class CciDiagramViewer;

/////////////////////////////////////////////////////////////////////////////
// DemandTable dialog

class CicDemandTable : public CDialog
{
// Construction
public:
	CicDemandTable(CWnd* pParent = NULL);   // standard constructor
	CicDemandTable(CicDemandTableViewer *popViewer,CciDiagramViewer *popParentViewer,CWnd* pParent = NULL);
	~CicDemandTable();
	void UpdateView();
	void SetCaptionText(void);

private :
    CCSTable*				pomTable; // visual object, the table content
	BOOL					bmIsViewOpen;
	CicDemandTableViewer*	pomViewer;
	int						m_nDialogBarHeight;
	CWnd*					pomParentWnd;
	CciDiagramViewer*		pomParentViewer;
private:
	static void				CicDemandTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void					HandleGlobalDateUpdate();
	void					SetViewerDate();
	void					UpdateComboBox();

// Dialog Data
	//{{AFX_DATA(CicDemandTable)
	enum { IDD = IDD_CICDEMANDTABLE };
	CComboBox	m_Date;
	//}}AFX_DATA

 
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CicDemandTable)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CicDemandTable)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSelchangeViewcombo();
	afx_msg void OnView();
	afx_msg void OnPrint();
	afx_msg void OnCloseupViewcombo();
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnClose();
	afx_msg void OnSelchangeDate();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnExpandGroup();
	afx_msg void OnCompressGroup();
	afx_msg LONG OnTableSelchange(UINT wParam,LONG lParam);
	afx_msg void OnMenuWorkOn();
	afx_msg void OnMenuFreeEmployees();
	afx_msg void OnAllocate();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Drag-and-drop implementation
private:
	CCSDragDropCtrl m_DragDropTarget;
	long			imDropDemandUrno;
};

#endif //__CICDEMANDTABLE_H__
