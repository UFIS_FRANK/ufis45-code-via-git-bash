// FlightTableBoundFilterPage.h : header file
//

#ifndef _FLPLANBO_H_
#define _FLPLANBO_H_

/////////////////////////////////////////////////////////////////////////////
// FlightTableBoundFilterPage dialog

class FlightTableBoundFilterPage : public CPropertyPage
{
	DECLARE_DYNCREATE(FlightTableBoundFilterPage)

// Construction
public:
	FlightTableBoundFilterPage();
	~FlightTableBoundFilterPage();

// Dialog Data
	//{{AFX_DATA(FlightTableBoundFilterPage)
	enum { IDD = IDD_FLIGHTTABLE_BOUNDFILTER_PAGE };
	BOOL	m_Inbound;
	BOOL	m_Outbound;
	BOOL	m_DisplayPax;
	//}}AFX_DATA
	CString omTitle;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(FlightTableBoundFilterPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(FlightTableBoundFilterPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
};

#endif // _FLPLANBO_H_
