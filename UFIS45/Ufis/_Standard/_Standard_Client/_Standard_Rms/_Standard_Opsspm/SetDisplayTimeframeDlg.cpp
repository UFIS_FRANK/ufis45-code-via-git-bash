// SetDisplayTimeframeDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <SetDisplayTimeframeDlg.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSetDisplayTimeframeDlg dialog


CSetDisplayTimeframeDlg::CSetDisplayTimeframeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSetDisplayTimeframeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSetDisplayTimeframeDlg)
	//}}AFX_DATA_INIT
}


void CSetDisplayTimeframeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSetDisplayTimeframeDlg)
	DDX_CCSddmmyy(pDX, IDC_FROMDATE, m_FromDate);
	DDX_CCSTime(pDX, IDC_FROMTIME, m_FromTime);
	DDX_CCSddmmyy(pDX, IDC_TODATE, m_ToDate);
	DDX_CCSTime(pDX, IDC_TOTIME, m_ToTime);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSetDisplayTimeframeDlg, CDialog)
	//{{AFX_MSG_MAP(CSetDisplayTimeframeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSetDisplayTimeframeDlg message handlers

BOOL CSetDisplayTimeframeDlg::OnInitDialog() 
{
	m_FromDate = omFrom;
	m_FromTime = omFrom;
	m_ToDate   = omTo;
	m_ToTime   = omTo;

	static_cast<CEdit *>(GetDlgItem(IDC_FROMDATE))->SetReadOnly(omFromReadOnly);
	static_cast<CEdit *>(GetDlgItem(IDC_TODATE))->SetReadOnly(omToReadOnly);

	CDialog::OnInitDialog();
	
	SetWindowText(GetString(IDS_SDTFDLG_TITLE));
	GetDlgItem(IDC_FROMTITLE)->SetWindowText(GetString(IDS_SDTFDLG_FROMTITLE));
	GetDlgItem(IDC_TOTITLE)->SetWindowText(GetString(IDS_SDTFDLG_TOTITLE));
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSetDisplayTimeframeDlg::OnOK() 
{
	if(UpdateData(TRUE))
	{

		CTime olFrom = CTime (m_FromDate.GetYear(),m_FromDate.GetMonth(),m_FromDate.GetDay(),
						m_FromTime.GetHour(),m_FromTime.GetMinute(),m_FromTime.GetSecond());

		CTime olTo = CTime (m_ToDate.GetYear(),m_ToDate.GetMonth(),m_ToDate.GetDay(),
						m_ToTime.GetHour(),m_ToTime.GetMinute(),m_ToTime.GetSecond());

		if(olFrom >= olTo)
		{
			MessageBox(GetString(IDS_SDTFDLG_TIMEERROR), GetString(IDS_SDTFDLG_TITLE), MB_OK|MB_ICONERROR);
		}
		else
		{
			omFrom = olFrom;
			omTo = olTo;
			CDialog::OnOK();
		}
	}
}
