// DEMAND PERMITS
// Each demand requires one or more permits (qualifications), these are
// contained in this table - the demands and permits are linked by:
// DEMTAB.URUD = RPQTAB.URUD
#ifndef _CEDARPQDATA_H_
#define _CEDARPQDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct RpqDataStruct
{
	long	Urno;
	long	Urud;		// URNO in RUDTAB (DEMTAB.URUD = RPQTAB.URUD)
	long	Uper;		// URNO in PERTAB (Permits Stammdaten)
	long	Udgr;		// Group URNO - links demands for a team (see DEM.UDGR or RUD.UDGR)
	char	Quco[21];	// Permit Code (Defined in PERTAB)
};

typedef struct RpqDataStruct RPQDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaRpqData: public CCSCedaData
{
public:
    CCSPtrArray <RPQDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omUrudMap;
	CMapPtrToPtr omUdgrMap;
	CString GetTableName(void);
	CedaRpqData();
	~CedaRpqData();
	void ProcessRpqBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CCSReturnCode ReadRpqData(CDWordArray &ropTplUrnos );
	void GetRpqsByUrud(long lpUrud, CCSPtrArray <RPQDATA> &ropRpqs, bool bpReset = true);
	void GetRpqsByUdgr(long lpUdgr, CCSPtrArray <RPQDATA> &ropRpqs, bool bpReset = true);
	RPQDATA *GetRpqByUrno(long lpUrno);

private:
	RPQDATA *AddRpqInternal(RPQDATA &rrpRpq);
	void DeleteRpqInternal(long lpUrno);
	void ClearAll();
};


extern CedaRpqData ogRpqData;
#endif _CEDARPQDATA_H_
