#if !defined(AFX_COPYDEMANDDLG_H__02FC732D_C76B_4C62_8877_C1FC20B90A0E__INCLUDED_)
#define AFX_COPYDEMANDDLG_H__02FC732D_C76B_4C62_8877_C1FC20B90A0E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CopyDemandDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCopyDemandDlg dialog

class CCopyDemandDlg : public CDialog
{
// Construction
public:
	CCopyDemandDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCopyDemandDlg)
	enum { IDD = IDD_COPYDEMANDDLG };
	CTime		m_FromDate;
	CTime		m_FromTime;
	CTime		m_ToDate;
	CTime		m_ToTime;
	CComboBox	m_CounterList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCopyDemandDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCopyDemandDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	void Init(CWnd* pParent, CString opSelectedCounter, CTime opFrom, CTime opTo);
	CTime omFrom, omTo;
	CString omSelectedCounter;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COPYDEMANDDLG_H__02FC732D_C76B_4C62_8877_C1FC20B90A0E__INCLUDED_)
