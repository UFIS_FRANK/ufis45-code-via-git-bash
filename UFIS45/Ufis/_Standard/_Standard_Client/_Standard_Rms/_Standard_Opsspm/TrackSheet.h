#ifndef _TRACKSHEET_H_
#define _TRACKSHEET_H_

#include <CCSPtrArray.h>
#include <CCSGlobl.h>
#include <CedaFlightData.h>
#include <CedaDemandData.h>
#include <CedaJobData.h>
#include <CedaDpxData.h>
#include <CedaTlxData.h>
#include <CedaFlightData.h>
#include <ccsprint.h>
#include <FieldConfigDlg.h>
#include <table.h>

struct TRACKSHEET_LINEDATA
{
	long	Udpx;	// dpx urno
	CString Name;	// passenger name
	CString Seat;	// seat number
	CString Prmt;	// prm type
	long	Tflu;	// transit flight urno
	CString Tfln;	// transit (departure) flight number
	CString Tstd;	// latest time (Etd or Std) of transit flight
	CString Rema;	// Remarks in PRM request
	CString Aloc;	// Pax Location
	CCSPtrArray<CString> Stfns;	// staff names (who handle this request)


	TRACKSHEET_LINEDATA()
	{
		Udpx = 0;
		Tflu = 0;
	}

	TRACKSHEET_LINEDATA(const TRACKSHEET_LINEDATA& rhs)
	{
		*this = rhs;		
	}

	TRACKSHEET_LINEDATA& TRACKSHEET_LINEDATA::operator=(const TRACKSHEET_LINEDATA& rhs)
	{
		if (&rhs != this)
		{
			this->Udpx = rhs.Udpx;
			this->Name = rhs.Name;
			this->Seat = rhs.Seat;
			this->Prmt = rhs.Prmt;
			this->Tflu = rhs.Tflu;
			this->Tfln = rhs.Tfln;
			this->Tstd = rhs.Tstd;
			this->Rema = rhs.Rema;
			this->Aloc = rhs.Aloc;
			this->Stfns.DeleteAll();
			CString olStaffName;
			for(int i = 0; i < rhs.Stfns.GetSize(); i++)
			{
				olStaffName = rhs.Stfns[i];
				this->Stfns.NewAt(this->Stfns.GetSize(),olStaffName);
			}
		}
		return *this;
	}
	
	~TRACKSHEET_LINEDATA()
	{
		Stfns.DeleteAll(); 
	}
};

/////////////////////////////////////////////////////////////////////////////
// TrackSheet

class TrackSheet: public CObject
{
// Constructions
public:
    TrackSheet(CWnd *opParent = NULL);
    ~TrackSheet();

public:
//	types
	enum PRINT_OPTION{PRINT_PRINTER,PRINT_PREVIEW};

//    void Attach(CTable *popAttachWnd);
	/* void ChangeViewTo(const char *pcpViewName, CString opDate); */
//    void ChangeViewTo(const char *pcpViewName);

// Internal data processing routines
private:
//	static void StaffTableTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
//	void PrepareGrouping();
//    void PrepareFilter();
//	void PrepareSorter();
//	BOOL IsPassFilter(const char *pcpRank, const char *pcpTeamId,
//		const char *pcpShiftCode, CTime opAcfr, CTime opActo, bool bpIsAbsent, CStringArray &ropPermits, 
//		bool bpHasFidJobs, bool bpPoolJobSetToAbsent, bool bpEmpStandby);
	int ComparePRM(TRACKSHEET_LINEDATA *prpPrm1, TRACKSHEET_LINEDATA *prpPrm2);
//    BOOL IsSameGroup(TRACKSHEET_LINEDATA *prpShift1, TRACKSHEET_LINEDATA *prpShift2);
//
//    void MakeLines();
//	void MakeLine(SHIFTDATA *prpShift);
//	void MakeAssignments(CCSPtrArray <JOBDATA> &ropJobs,TRACKSHEET_LINEDATA &rrpPrm);
//	int CreateAssignment(TRACKSHEET_LINEDATA &rrlPrm, TRACKSHEET_ASSIGNMENT *prpAssignment);
//	CString GetAssignmentString(JOBDATA *prpJob);


// Operations
public:
	void MakeLines(FLIGHTDATA *popFlight);
	void MakeLines();
	void DeleteAll();
	int CreateLine(TRACKSHEET_LINEDATA *prpPrm);
	void DeleteLine(int ipLineno);
//	void SetIsFromSearchMode(BOOL bpIsFromSearch);
//	BOOL FindShift(long lpShiftUrno, int &rilLineno);

// Window refreshing routines
public:
//	BOOL DeleteAssignmentJob(long lpJobUrno);
private:
//	void ProcessShiftDelete(long lpUrno);
//	void ProcessShiftChange(SHIFTDATA *prpShift);
//	void ProcessShiftSelect(SHIFTDATA *prpShift);
//	void ProcessShiftChange(JOBDATA *prpJob);
//	void ProcessJobDelete(JOBDATA *prpJob);
//	void UpdateDisplay();
//	CString Format(TRACKSHEET_LINEDATA *prpLine);

// Attributes used for filtering condition
private:
//    int omGroupBy;              // enumerated value -- define in "Sfviewer.cpp" (use first sorting)
//    CWordArray omSortOrder;     // array of enumerated value -- defined in "Sfviewer.cpp"
//	CMapStringToPtr omCMapForShiftCode;
//	CMapStringToPtr omCMapForRank;
//	CMapStringToPtr omCMapForPermits;
//    CMapStringToPtr omCMapForTeam;
//    CString omDate;
	BOOL bmPRMPrinted;
	BOOL bmCrewPrinted;
	BOOL bmArrPrinted;

	// Attributes
private:
//	bool bmUseAllShiftCodes ;
//	bool bmUseAllRanks;
//	bool bmUseAllPermits;
//	bool bmUseAllTeams;
//	bool bmEmptyTeamSelected;
//	bool bmHideAbsentEmps, bmHideFidEmps, bmHideStandbyEmps;
//	int imShiftTime;
	CCSPtrArray<TRACKSHEET_LINEDATA> omLines;
//	int	 imColumns;
//	CString omTerminal; //Singapore
	CWnd *pomParent;
    FLIGHTDATA*	pomFlight;
//	
// Methods which handle changes (from Data Distributor)
public:
	CTime omStartTime;
	CTime omEndTime;

	CFieldDataArray omHeaderConfig;
	int imNumJobsPerLine;
	int imFirstJobField;
	int imCharWidth;
	int imMaxLines;
	int imMaxJobsPerLine;
	int imMaxLineWidth;
	int imCrewAckLines;
	int imArrTeamLines;
	int imRemaLength;
	int imTotalLines;
	void PrintViewConfigurable();
	inline int Lines() const {return omLines.GetSize();} //Singapore
//	inline void SetTerminal(const CString& ropTerminal){omTerminal = ropTerminal;} //Singapore
//	inline CString GetTerminal() const {return omTerminal;} //Singapore
	inline void GetLines(CCSPtrArray<TRACKSHEET_LINEDATA>& opLines) const{opLines = omLines;} //Singapore
	CCSPtrArray<TRACKSHEET_LINEDATA>& GetLines(){return omLines;} //Singapore
	CCSPrint*& GetCCSPrinter() {return pomPrint;}

// Printing functions
private:
	BOOL PrintEmptyPrmLine();
	BOOL PrintNilCasePrmLine();
	BOOL PrintPrmLineHeader();
	BOOL PrintPrmLine(TRACKSHEET_LINEDATA *prpLine,BOOL bpIsLastLine);
	BOOL PrintPrmLine2(TRACKSHEET_LINEDATA *prpLine);
	BOOL PrintPrmLine3(TRACKSHEET_LINEDATA *prpLine);
	BOOL PrintPrmHeader(CCSPrint *pomPrint);
	BOOL PrintCrewAck(CCSPrint *pomPrint);
	BOOL PrintArrTeam(CCSPrint *pomPrint);
	BOOL PrintFlightInfo(CCSPrint *pomPrint);
	BOOL PrintTableName(CCSPrint *pomPrint,CString opTableName);
	CCSPrint *pomPrint;
//	CString GetTeamLeaderWorkGroupsForFlight(long lpFlightUrno);
//
	BOOL PrintPrmHeaderConfigurable(CCSPrint *pomPrint);
	void PrintColumnTitlesConfigurable();
	BOOL PrintPrmLineConfigurable(TRACKSHEET_LINEDATA *prpLine);
	bool ConfigureHeader(void);
	bool CheckForNewPageConfigurable();
	void PrintEndOfPageConfigurable();

	int imFuncLen2,imNameLen2,imWorkGroupLen2,imShiftLen2,imJobLen2,imRegnLen2,imArrTimeLen2,imDepTimeLen2,imGatePosLen2,imServiceLen2;
	int imFuncLen3,imBreakLen3,imNameLen3,imShiftLen3,imJobLen3,imRegnLen3,imArrTimeLen3,imDepTimeLen3,imGatePosLen3,imServiceLen3,imTotalLen3;
	void AddElement(CCSPtrArray <PRINTELEDATA> &ropPrintLine, CString &ropText, int ipLength, int ipAlignment, int ipFrameLeft, int ipFrameRight, int ipFrameTop, int ipFrameBottom, CFont &ropFont);
	void PrintEndOfPage2();
	bool PrintStartOfPage2();
	void PrintEndOfPage3();
	bool PrintStartOfPage3();
public:
	void PrintView();
	void PrintView2();
	void PrintView3();
//	void CreateExport(const char *pclFileName, const char *pclSeperator, CString opTitle);
//	CString GetJobText(JOBDATA *prpJob);
//
	void PrintPreview(const int& ripPageNo); //Singapore
};

#endif


