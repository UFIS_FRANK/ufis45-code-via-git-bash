// CedaRloData.cpp - Stammdaten: Allocation Unit Types
//

#include <stdafx.h>
#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaCom.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccslog.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <CedaRloData.h>

CedaRloData ogRloData;

CedaRloData::CedaRloData()
{
   // Create an array of CEDARECINFO for RLODATA
    BEGIN_CEDARECINFO(RLODATA, RloDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Gtab,"GTAB")
		FIELD_CHAR_TRIM(Reft,"REFT")
		FIELD_LONG(Rloc,"RLOC")
		FIELD_LONG(Urud,"URUD")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(RloDataRecInfo)/sizeof(RloDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&RloDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"RLOTAB");
    pcmFieldList = "URNO,GTAB,REFT,RLOC,URUD";
}

CedaRloData::~CedaRloData()
{
	TRACE("CedaRloData::~CedaRloData called\n");
	ClearAll();
}

void CedaRloData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omUrudMap.RemoveAll();
	omData.DeleteAll();
}

BOOL CedaRloData::ReadRloData()
{
	CCSReturnCode ilRc = RCSuccess;

	// Select data from the database
	char pclWhere[100] = "";
	char pclCom[10] = "RT";
	if (CedaAction2(pclCom, pclWhere) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaRloData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			RLODATA *prlRlo = new RLODATA;
			if ((ilRc = GetBufferRecord2(ilLc,prlRlo)) == RCSuccess)
			{
				AddRloInternal(prlRlo);
			}
			else
			{
				delete prlRlo;
			}
		}
	}
	ogBasicData.Trace("CedaRloData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(RLODATA), "");
	return TRUE;
}


BOOL CedaRloData::AddRloInternal(RLODATA *prpRlo)
{
	omData.Add(prpRlo);
	omUrnoMap.SetAt((void *)prpRlo->Urno,prpRlo);
	omUrudMap.SetAt((void *)prpRlo->Urud,prpRlo);
	return RCSuccess;
}

RLODATA *CedaRloData::GetRloByUrno(long lpUrno)
{
	RLODATA *prlRlo = NULL;
	omUrnoMap.Lookup((void *)lpUrno,(void *& )prlRlo);
	return prlRlo;
}

RLODATA *CedaRloData::GetRloByUrud(long lpUrud)
{
	RLODATA *prlRlo = NULL;
	omUrudMap.Lookup((void *)lpUrud,(void *& )prlRlo);
	return prlRlo;
}


CString CedaRloData::GetTableName(void)
{
	return CString(pcmTableName);
}

int CedaRloData::GetCountOfRecords()
{
	return omData.GetSize();
}