// flplanbo.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <FlightTableBoundFilterPage.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Change this key identifiers for name used in the registry database

/////////////////////////////////////////////////////////////////////////////
// FlightTableBoundFilterPage property page

IMPLEMENT_DYNCREATE(FlightTableBoundFilterPage, CPropertyPage)

FlightTableBoundFilterPage::FlightTableBoundFilterPage() : CPropertyPage(FlightTableBoundFilterPage::IDD)
{
	//{{AFX_DATA_INIT(FlightTableBoundFilterPage)
	m_Inbound = FALSE;
	m_Outbound = FALSE;
	m_DisplayPax = FALSE;
	//}}AFX_DATA_INIT
	omTitle = GetString(IDS_STRING32977);
	m_psp.pszTitle = omTitle;
	m_psp.dwFlags |= PSP_USETITLE;
}

FlightTableBoundFilterPage::~FlightTableBoundFilterPage()
{
}

void FlightTableBoundFilterPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FlightTableBoundFilterPage)
	DDX_Check(pDX, IDC_CHECK1, m_Inbound);
	DDX_Check(pDX, IDC_CHECK2, m_Outbound);
	DDX_Check(pDX, IDC_DISPLAYPAX, m_DisplayPax);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FlightTableBoundFilterPage, CPropertyPage)
	//{{AFX_MSG_MAP(FlightTableBoundFilterPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FlightTableBoundFilterPage message handlers

BOOL FlightTableBoundFilterPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	if (HIWORD(wParam) == BN_CLICKED)
		CancelToClose();
	return CPropertyPage::OnCommand(wParam, lParam);
}

BOOL FlightTableBoundFilterPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
//	SetWindowText(GetString(IDS_STRING32977)); 

	CWnd *polWnd = GetDlgItem(IDC_CHECK1); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61383));
	}
	polWnd = GetDlgItem(IDC_CHECK2); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61384));
	}
	polWnd = GetDlgItem(IDC_DISPLAYPAX); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_DISPLAYPAX));
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
