#if !defined(AFX_TRANSACTIONMANAGER_H__BF466F25_375F_11D7_8206_00010215BFE5__INCLUDED_)
#define AFX_TRANSACTIONMANAGER_H__BF466F25_375F_11D7_8206_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TransactionManager.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTransactionManager window

class CTransactionManager
{
public:
	CTransactionManager();
	void Start(void);
	void End(void);
};

extern CTransactionManager ogTransactionManager;

#endif // !defined(AFX_TRANSACTIONMANAGER_H__BF466F25_375F_11D7_8206_00010215BFE5__INCLUDED_)
