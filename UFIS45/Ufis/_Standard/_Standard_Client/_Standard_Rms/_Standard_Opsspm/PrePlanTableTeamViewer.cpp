// ppviewer.cpp : implementation file
//
// Modification History:
// 19-Sep-96	Damkerng	Change many places since our database is changed.
//							On this change, special flight job was changed from type
//							FLT to FLS. The field PRID which contains a word "SPECIAL"
//							is not used any more. The field FLUR which was used for
//							storing shift URNO now moved to the field SHUR.
//							Check these places out by searching for "Id 19-Sep-96"

#include <stdafx.h> 
#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <cviewer.h>
#include <table.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaRnkData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <ccsddx.h>
#include <AllocData.h>
#include <BasicData.h>
#include <PrePlanTableViewer.h>
#include <PrePlanTableTeamViewer.h>
#include <dataset.h>
extern bool BGS;

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))


// Grouping and Sorting criterias definition:
// This will make the program faster a little bit because it will replace string comparison.
enum {
    BY_SHIFTBEGIN,
    BY_SHIFTEND,
    BY_SHIFTCODE,
    BY_TEAM,
    BY_NAME,
    BY_RANK,
    BY_NONE
};

// To know which rank is higher in comparison of two staff, I provide a colon-separated
// string from the lowest rank to the highest rank. By searching for a string ":XX:"
// in "pclRanks", if XX is the given rank, we could easily see which rank is higher.
// By using the same technic, we could see if the given rank is a manager by searching
// for a string ":XX:" in "ogBasicData.omManager".
//

/////////////////////////////////////////////////////////////////////////////
// PrePlanTableTeamViewer
//
// Requirements:
// In the PrePlanTable we have 3 filters, by Rank, Team, and ShiftCode.
// (May be also Pools in the future. For the default pools of each employee,
// we have to look in the field EMPCKI.DISP.)
// It may be sorted by a combination of ShiftBegin, ShiftEnd, ShiftCode, Team, Name,
// Rank, and none.
// It will be always groupped by the first sorting criteria.
//
// Implementation:
// At the program start up, we will create a default view for each table if the view
// named <default> does not exist before.
//
// When the viewer is constructed or everytimes the view changes, we will clear
// everything from the viewer buffer and do the insertion sorting. We will insert
// the record only if it satisfies our filter. Conceptually we will filter the record
// first, then sort it (by insertion sort), and group it.
//
// To let the filter work fast, we will implement a CMapStringToPtr for every filter
// condition. For example, let's say that we have a CStringArray for every possible
// values of a filter. We will have a CMapStringToPtr which could tell us the given
// key value is selected in the filter or not.
//
// Methods for change the view:
// ChangeViewTo         Change view, reload everything from Ceda????Data
// PrepareGroupping     Prepare the enumerate group value
// PrepareFilter        Prepare CMapStringToPtr or CMapPtrToPtr for filtering
// PrepareSorter        Prepare sort criterias before sorting take place
// IsPassFilter         Return TRUE if the given record satisfies the filter
// CompareGroup         Return 0 or -1 or 1 as the result of comparison of two groups
// CompareShift         Return 0 or -1 or 1 as the result of comparison of two lines

PrePlanTableTeamViewer::PrePlanTableTeamViewer()
{
    SetViewerKey("PPTab");
    pomTable = NULL;
	omSortOrder.RemoveAll();
	omSortOrder.Add("RANK");
	omSortOrder.Add("NAME");
	omTeamID = CString("");
    ogCCSDdx.Register(this, JOB_NEW, CString("PPVIEWER"), CString("Job New"), PrePlanTableCf);
    ogCCSDdx.Register(this, JOB_DELETE, CString("PPVIEWER"), CString("Job Delete"), PrePlanTableCf);
    ogCCSDdx.Register(this, SHIFT_NEW, CString("PPVIEWER"), CString("Shift New"), PrePlanTableCf);
    ogCCSDdx.Register(this, SHIFT_CHANGE, CString("PPVIEWER"), CString("Shift Change"), PrePlanTableCf);
}

PrePlanTableTeamViewer::~PrePlanTableTeamViewer()
{
    ogCCSDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void PrePlanTableTeamViewer::SetTeamID(CString opTeamID)
{
	omTeamID = opTeamID;
}

void PrePlanTableTeamViewer::Attach(CTable *popTable)
{
    pomTable = popTable;
}



/////////////////////////////////////////////////////////////////////////////
// PrePlanTableTeamViewer -- code specific to this class

void PrePlanTableTeamViewer::MakeLines(CCSPtrArray<PREPLANT_SHIFTDATA> *polTeamMember)
{
    int ilShiftCount = polTeamMember->GetSize();
    for (int ilLc = 0; ilLc < ilShiftCount; ilLc++)
    {
			PREPLANT_SHIFTDATA rlShiftData = polTeamMember->GetAt(ilLc);
			MakeLine(&rlShiftData);
    }

}

void PrePlanTableTeamViewer::MakeAssignments()
{
    POSITION rlPos;
    for ( rlPos = ogJobData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
    {
        long llUrno;
        JOBDATA *prlJob;
        ogJobData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlJob);

// Id 19-Sep-96
// Old algorithm:
//		We search for pool jobs and flight jobs. We use a special technique to
//		display the proper gate area instead of pool for a flight manager.
//		If PRID is blank, we display the pool, otherwise we display the gate area.
//		This must be handshaken with ogDataSet.CreateJobPool().
// New algorithm:
//		We still search for pool jobs and flight jobs. But now we can look at
//		JTCO directly since we have another separate type FLS for speical flight
//		job. For a pool job, if the field TEXT is not empty, we will assume that
//		the field TEXT is a gate area ID and display it, otherwise we will display
//		the pool using the value in the field ALID.
//
	    // If it's not a job pool or special flight job, ignore it
		BOOL blIsJobPool = CString(prlJob->Jtco) == JOBPOOL;
		BOOL blIsJobFlightSpecial = ((CString(prlJob->Jtco) == JOBFLIGHT) && 
			(prlJob->DisplayInPrePlanTable == TRUE));
		BOOL blIsJobCciSpecial = ((CString(prlJob->Jtco) == JOBCCI) && 
			(prlJob->DisplayInPrePlanTable == TRUE));
		if (!(blIsJobPool || blIsJobFlightSpecial || blIsJobCciSpecial))
			continue;

		// Prepare shift URNO and displayed text for this assignment
		long llShiftUrno;
		long llAlidUrno;
		CString olAssignmentText;

		if (blIsJobPool)
		{
			llShiftUrno = prlJob->Shur;
			olAssignmentText = (prlJob->Text[0] != '\0')?
				GetGateAreaName(prlJob->Text): prlJob->Alid;
			llAlidUrno = prlJob->Urno;
		}
		else
		{
			llShiftUrno = prlJob->Shur;
			FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prlJob);
			olAssignmentText = (prlFlight == NULL)? "ERR": prlFlight->Fnum;
			llAlidUrno = prlJob->Urno;
		}
// end of Id 19-Sep-96

	    // Create a new assignment
        PREPLANT_ASSIGNMENT rlAssignment;
        rlAssignment.Alid = olAssignmentText;
		rlAssignment.Urno = llAlidUrno;
        rlAssignment.Acfr = prlJob->Acfr;
        rlAssignment.Acto = prlJob->Acto;
        int ilLineno;
        if (FindShift(llShiftUrno, ilLineno))  // corresponding shift found?
        {   // (for some reason, we have saved SHFCKI.URNO in JOBCKI.FLUR)
            CreateAssignment(ilLineno, &rlAssignment);
            RecalcAssignmentStatus(&omLines[ilLineno].Line);
        }

        int ilTeamMemberno;
        if (bmIsCompleteTeam && FindTeamMember(llShiftUrno, ilLineno, ilTeamMemberno))
        {
            CreateTeamMemberAssignment(ilLineno, ilTeamMemberno, &rlAssignment);
            RecalcAssignmentStatus(&omLines[ilLineno].TeamMember[ilTeamMemberno]);
            RecalcTeamAssignmentStatus(ilLineno);
        }
    }
}

void PrePlanTableTeamViewer::MakeLine(PREPLANT_SHIFTDATA *prpShift)
{
	PREPLANT_SHIFTDATA rlShiftLine;
	rlShiftLine.ShiftUrno = prpShift->ShiftUrno;
	rlShiftLine.Peno = prpShift->Peno;
	rlShiftLine.Name = prpShift->Name;
	rlShiftLine.Rank = prpShift->Rank;
	rlShiftLine.Tmid = prpShift->Tmid;
	rlShiftLine.Sfca = prpShift->Sfca;
	rlShiftLine.Acfr = prpShift->Acfr;
	rlShiftLine.Acto = prpShift->Acto;
	rlShiftLine.AssignmentStatus = UNASSIGNED;
    CreateLine(&rlShiftLine);
}

BOOL PrePlanTableTeamViewer::FindTeam(SHIFTDATA *prpShift, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
        if (omLines[rilLineno].Line.Tmid == prpShift->Egrp &&
            omLines[rilLineno].Line.Acfr == prpShift->Avfa &&
            omLines[rilLineno].Line.Acto == prpShift->Avta)
            return TRUE;
    return FALSE;
}

BOOL PrePlanTableTeamViewer::FindShift(long lpShiftUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
        if (omLines[rilLineno].Line.ShiftUrno == lpShiftUrno)
            return TRUE;
    return FALSE;
}

BOOL PrePlanTableTeamViewer::FindTeamMember(long lpShiftUrno, int &rilLineno, int &rilTeamMemberno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
    {
        // we have to scan every members in this team
        int n = omLines[rilLineno].TeamMember.GetSize();
        for (rilTeamMemberno = 0; rilTeamMemberno < n; rilTeamMemberno++)
            if (omLines[rilLineno].TeamMember[rilTeamMemberno].ShiftUrno == lpShiftUrno)
                return TRUE;
    }
    return FALSE;
}

void PrePlanTableTeamViewer::RecalcTeamAssignmentStatus(int ipLineno)
{
    PREPLANT_LINEDATA *prlLine = &omLines[ipLineno];
    prlLine->Line.AssignmentStatus = UNASSIGNED;	// clear current status

    int ilTeamMemberCount = prlLine->TeamMember.GetSize();
    int ilAssignedMemberCount = 0;
    for (int ilTeamMemberno; ilTeamMemberno < ilTeamMemberCount; ilTeamMemberno++)
    {
        switch (prlLine->TeamMember[ilTeamMemberno].AssignmentStatus)
        {
        case PARTIAL_ASSIGNED:
            prlLine->Line.AssignmentStatus = PARTIAL_ASSIGNED;
				// some partial assignment found
            break;
        case FULLY_ASSIGNED:
            ilAssignedMemberCount++;
            break;
        }
    }

    // change color coding for the whole line
    if (ilAssignedMemberCount == ilTeamMemberCount)
        prlLine->Line.AssignmentStatus = FULLY_ASSIGNED;
    else if (ilAssignedMemberCount > 0)
        prlLine->Line.AssignmentStatus = PARTIAL_ASSIGNED;
}


int PrePlanTableTeamViewer::RecalcAssignmentStatus(PREPLANT_SHIFTDATA *prpShift)
{
    int n = prpShift->Assignment.GetSize();
    if (n == 0)
		return (prpShift->AssignmentStatus = UNASSIGNED);

	if (prpShift->Assignment[0].Acfr > prpShift->Acto)	// nothing in the range of shift at all?
		return (prpShift->AssignmentStatus = UNASSIGNED);

	CTime olLastTime = min(prpShift->Acfr, prpShift->Assignment[0].Acfr);
    for (int i = 0; i < n; i++) // scan through all assignments
    {
        if (prpShift->Assignment[i].Acfr > olLastTime)	// first hole detected?
			return (prpShift->AssignmentStatus = PARTIAL_ASSIGNED);
        if (prpShift->Assignment[i].Acto >= prpShift->Acto)	// cover beyond the end of shift?
			return (prpShift->AssignmentStatus = FULLY_ASSIGNED);
        if (olLastTime < prpShift->Assignment[i].Acto)
			olLastTime = prpShift->Assignment[i].Acto;
    }

	return (prpShift->AssignmentStatus = PARTIAL_ASSIGNED);
}



/////////////////////////////////////////////////////////////////////////////
// PrePlanTableTeamViewer - PREPLANT_LINEDATA array maintenance

void PrePlanTableTeamViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int PrePlanTableTeamViewer::CreateLine(PREPLANT_SHIFTDATA *prpShift)
{
    int ilLineCount = omLines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
		//MWO: Compare by Name        
		if (CompareShift(prpShift, &omLines[ilLineno].Line) <= 0)
            break;  // should be inserted before Lines[ilLineno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        //MWO: Compare by Name		
		if (CompareShift(prpShift, &omLines[ilLineno-1].Line) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
#endif

    PREPLANT_LINEDATA rlLine;

    rlLine.Line = *prpShift;
    omLines.NewAt(ilLineno, rlLine);

    return ilLineno;
}

int PrePlanTableTeamViewer::CompareShift(PREPLANT_SHIFTDATA *prpShift1, 
										 PREPLANT_SHIFTDATA *prpShift2)
{
	int ilCompareResult = 0;
	int ilCount = omSortOrder.GetSize();
	for (int ili = 0; ili < ilCount; ili++)
	{
		ilCompareResult = 0;
		if(strcmp(omSortOrder[ili], "RANK") == 0)
		{
            ilCompareResult = (ogRnkData.GetRankWeight(prpShift1->Rank) == ogRnkData.GetRankWeight(prpShift2->Rank))? 0:
                (ogRnkData.GetRankWeight(prpShift1->Rank) > ogRnkData.GetRankWeight(prpShift2->Rank))? 1: -1;
		}
		else if(strcmp(omSortOrder[ili], "NAME") == 0)
		{
			ilCompareResult = (prpShift1->Name == prpShift2->Name)? 0:
				(prpShift1->Name > prpShift2->Name)? 1: -1;
		}
        if (ilCompareResult != 0)
            return ilCompareResult;
	}
	return 0;
}

void PrePlanTableTeamViewer::DeleteLine(int ipLineno)
{
	if (ipLineno < omLines.GetSize())
	{
		while (omLines[ipLineno].Line.Assignment.GetSize() > 0)
			DeleteAssignment(ipLineno, 0);
		while (omLines[ipLineno].TeamMember.GetSize() > 0)
			DeleteTeamMember(ipLineno, 0);

		omLines.DeleteAt(ipLineno);
	}
}

int PrePlanTableTeamViewer::CreateAssignment(int ipLineno, PREPLANT_ASSIGNMENT *prpAssignment)
{
    int ilAssignmentCount = omLines[ipLineno].Line.Assignment.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilAssignmentno = 0; ilAssignmentno < ilAssignmentCount; ilAssignmentno++)
        if (prpAssignment->Acfr <= omLines[ipLineno].Line.Assignment[ilAssignmentno].Acfr)
            break;  // should be inserted before Lines[ilAssignmentno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilAssignmentno = ilAssignmentCount; ilAssignmentno > 0; ilAssignmentno--)
        if (prpAssignment->Acfr >= omLines[ipLineno].Line.Assignment[ilAssignmentno-1].Acfr)
            break;  // should be inserted after Lines[ilAssignmentno-1]
#endif

    omLines[ipLineno].Line.Assignment.NewAt(ilAssignmentno, *prpAssignment);

    return ilAssignmentno;
}

void PrePlanTableTeamViewer::DeleteAssignmentJob(long lpJobUrno)
{
	JOBDATA *prlJob = ogJobData.GetJobByUrno(lpJobUrno);
	if (prlJob != NULL)
	{
		ogDataSet.DeleteJob(pomTable, lpJobUrno);
	}
}

void PrePlanTableTeamViewer::DeleteAssignment(int ipLineno, int ipAssignmentno)
{
	if (ipLineno < omLines.GetSize())
	{
		if (omLines[ipLineno].Line.Assignment.GetSize() > ipAssignmentno)
			omLines[ipLineno].Line.Assignment.DeleteAt(ipAssignmentno);
		RecalcAssignmentStatus(&omLines[ipLineno].Line);
		// Update Display 
		switch (omLines[ipLineno].Line.AssignmentStatus)
		{
		case PARTIAL_ASSIGNED:
			pomTable->SetTextLineColor(ipLineno, BLACK, SILVER);
			pomTable->SetTextLineDragEnable(ipLineno, TRUE);
			break;
		case FULLY_ASSIGNED:
			pomTable->SetTextLineColor(ipLineno, WHITE, GRAY);
			pomTable->SetTextLineDragEnable(ipLineno, FALSE);
			break;
		case UNASSIGNED:
			pomTable->SetTextLineColor(ipLineno, BLACK, WHITE);
			pomTable->SetTextLineDragEnable(ipLineno, TRUE);
			break;
		}

		pomTable->ChangeTextLine(ipLineno, Format(&omLines[ipLineno]), &omLines[ipLineno]);
	}
}

int PrePlanTableTeamViewer::CreateTeamMember(int ipLineno, PREPLANT_SHIFTDATA *prpShift)
{
    int ilTeamMemberno = omLines[ipLineno].TeamMember.New();
    omLines[ipLineno].TeamMember[ilTeamMemberno] = *prpShift;

    // Update information in the team record
    BOOL blIsFlightManager = ogBasicData.IsManager(prpShift->Rank);
    omLines[ipLineno].FmCount += (blIsFlightManager? 1: 0);
    omLines[ipLineno].EmpCount += (!blIsFlightManager? 1: 0);

    if (omLines[ipLineno].Line.Assignment.GetSize() == 1)	// Is this the first member in this team?
        omLines[ipLineno].Line.AssignmentStatus = prpShift->AssignmentStatus;
    else
    {
        if (omLines[ipLineno].Line.AssignmentStatus != prpShift->AssignmentStatus)
            omLines[ipLineno].Line.AssignmentStatus = PARTIAL_ASSIGNED;
    }

    // Check if it necessary to change the team leader
    int ilTeamLeaderRank = ogRnkData.GetRankWeight(omLines[ipLineno].Line.Rank);
    int ilShiftRank = ogRnkData.GetRankWeight(prpShift->Rank);
    if (ilShiftRank > ilTeamLeaderRank)
    {
        omLines[ipLineno].Line = *prpShift;
        PREPLANT_LINEDATA *prlLine = &omLines[ipLineno];
        omLines.RemoveAt(ipLineno);
        int ilLineCount = omLines.GetSize();
// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
        // Search for the position which we want to insert this new bar
        for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
            // MWO: Compare by name  if (CompareShift(&prlLine->Line, &omLines[ilLineno].Line) <= 0)
                break;  // should be inserted before Lines[ilLineno]
#else
        // Search for the position which we want to insert this new bar
        for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
            // MWO: Compare by name  if (CompareShift(&prlLine->Line, &omLines[ilLineno-1].Line) >= 0)
                break;  // should be inserted after Lines[ilLineno-1]
#endif
        omLines.InsertAt(ilLineno, (void *)prlLine);
			// put the team back to the correct order
    }

    return ilTeamMemberno;
}

void PrePlanTableTeamViewer::DeleteTeamMember(int ipLineno, int ipTeamMemberno)
{
    while (omLines[ipLineno].TeamMember[ipTeamMemberno].Assignment.GetSize() > 0)
        DeleteTeamMemberAssignment(ipLineno, ipTeamMemberno, 0);

    omLines[ipLineno].TeamMember.DeleteAt(ipTeamMemberno);
}

int PrePlanTableTeamViewer::CreateTeamMemberAssignment(int ipLineno, int ipTeamMemberno,
    PREPLANT_ASSIGNMENT *prpAssignment)
{
    int ilAssignmentno = omLines[ipLineno].TeamMember[ipTeamMemberno].Assignment.New();
    omLines[ipLineno].TeamMember[ipTeamMemberno].Assignment[ilAssignmentno] = *prpAssignment;

    return ilAssignmentno;
}

void PrePlanTableTeamViewer::DeleteTeamMemberAssignment(int ipLineno, int ipTeamMemberno,
    int ipAssignmentno)
{
    omLines[ipLineno].TeamMember[ipTeamMemberno].Assignment.DeleteAt(ipAssignmentno);
}


/////////////////////////////////////////////////////////////////////////////
// PrePlanTableTeamViewer - display drawing routine

// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void PrePlanTableTeamViewer::UpdateDisplay(CCSPtrArray<PREPLANT_SHIFTDATA> *polTeamMember)
{
    // Set table header and format
    DeleteAll();    // remove everything
    MakeLines(polTeamMember);
	MakeAssignments();
	bmIsCompleteTeam = FALSE;
    if (!bmIsCompleteTeam)  // display each staff?
    {
        pomTable->SetFieldNames("NAME,RANK,TEAM,TYPE,ACFR,ACHTO,"
            "ALO1,TIL1,ALO2,TIL2,ALO3,TIL3,ALO4,ALO4");
        //pomTable->SetHeaderFields("Name|Rng|Team|SFT|Anwesend||Zugeteilt|von-bis|Zugeteilt|von-bis|Zugeteilt|von-bis|Zugeteilt|von-bis");

        if (BGS) 	pomTable->SetHeaderFieldsBGS(GetString(IDS_STRING61570));
		
		else		pomTable->SetHeaderFields(GetString(IDS_STRING61570));
        
		pomTable->SetFormatList("24|3|4|3|7|7|10|9|10|9|10|9|10|9");
    }
    else
    {
        pomTable->SetFieldNames("NAME,RANK,TEAM,TYPE,ACFR,ACTO,FMCOUNT,MEMBERCOUNT,"
            "ALO1,TIL1,ALO2,TIL2,ALO3,TIL3,ALO4,ALO4");
        //pomTable->SetHeaderFields("Name|Rng|Team|SFT|Anwesend||FM|Emp|Zugeteilt|von-bis|Zugeteilt|von-bis|Zugeteilt|von-bis|Zugeteilt|von-bis");
        
		if (BGS) 	pomTable->SetHeaderFieldsBGS(GetString(IDS_STRING61571));
		else		pomTable->SetHeaderFields(GetString(IDS_STRING61571));
        
		pomTable->SetFormatList("24|3|4|3|7|7|3|3|10|9|10|9|10|9|10|9");
    }

    // Load filtered and sorted data into the table content
    pomTable->ResetContent();
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
    {
        pomTable->AddTextLine(Format(&omLines[ilLc]), &omLines[ilLc]);

		// Display bold font for the managers
		CString olRank = omLines[ilLc].Line.Rank;
        BOOL blIsFlightManager = ogBasicData.IsManager(olRank);
		if (blIsFlightManager)
		{
			pomTable->SetTextLineFont(ilLc, &ogCourier_Bold_10);
			pomTable->SetTextLineColor(ilLc, BLUE, WHITE);
		}

        // Display grouping effect
        if (ilLc == omLines.GetSize()-1) //||
			//!IsSameGroup(&omLines[ilLc].Line, &omLines[ilLc+1].Line))
            pomTable->SetTextLineSeparator(ilLc, ST_THICK);

        switch (omLines[ilLc].Line.AssignmentStatus)
        {
        case PARTIAL_ASSIGNED:
            pomTable->SetTextLineColor(ilLc, (blIsFlightManager? BLUE: BLACK), SILVER);
            break;
        case FULLY_ASSIGNED:
            pomTable->SetTextLineColor(ilLc, (blIsFlightManager? BLUE: WHITE), GRAY);
            pomTable->SetTextLineDragEnable(ilLc, FALSE);
            break;
        }
    }

    // Update the table content in the display
    pomTable->DisplayTable();
}

CString PrePlanTableTeamViewer::Format(PREPLANT_LINEDATA *prpLine)
{
    CString s = CString(prpLine->Line.Name) + "|"
        + prpLine->Line.Rank + "|"
        + prpLine->Line.Tmid + "|"
        + prpLine->Line.Sfca + "|"
        + prpLine->Line.Acfr.Format("%d/%H%M") + "|"
        + prpLine->Line.Acto.Format("%d/%H%M");

    if (bmIsCompleteTeam)
    {
        char buf[512];
        sprintf(buf, "|%3d|%3d", prpLine->FmCount, prpLine->EmpCount);
        s += buf;
    }

    for (int ilLc = 0; ilLc < prpLine->Line.Assignment.GetSize(); ilLc++)
    {
        PREPLANT_ASSIGNMENT *prlAssignment = &prpLine->Line.Assignment[ilLc];
        s += "|" + prlAssignment->Alid;
		s += "|" + prlAssignment->Acfr.Format("%H%M") + "-";
		s += prlAssignment->Acto.Format("%H%M");
    }
    return s;
}

CString PrePlanTableTeamViewer::GetPoolName(const char *pcpPoolId)
{
	CString olPoolGroupName;
	ALLOCUNIT *prlPoolGroup = ogAllocData.GetGroupByName(ALLOCUNITTYPE_POOLGROUP,pcpPoolId);
	if(prlPoolGroup != NULL)
	{
		olPoolGroupName = prlPoolGroup->Desc;
	}
	return olPoolGroupName;
//	int n = ogPools.omMetaAllocUnitList.GetSize();
//    for (int i = 0; i < n; i++)
//	{
//		if (CString(ogPools.omMetaAllocUnitList[i].Alid) == pcpPoolId)
//			return ogPools.omMetaAllocUnitList[i].Alfn;
//	}
//
//	return "";
}

CString PrePlanTableTeamViewer::GetGateAreaName(const char *pcpGateAreaId)
{
	CString olGateAreaName;

	CCSPtrArray <ALLOCUNIT> olGateAreas;
	ogAllocData.GetGroupsByGroupType(ALLOCUNITTYPE_GATEGROUP,olGateAreas);
	int ilNumGateAreas = olGateAreas.GetSize();
	for(int ilGateArea = 0; ilGateArea < ilNumGateAreas; ilGateArea++)
	{
		ALLOCUNIT *prlGateArea = &olGateAreas[ilGateArea];
		if(!strcmp(prlGateArea->Name,pcpGateAreaId))
		{
			olGateAreaName = prlGateArea->Desc;
		}
	}
//
//	int n = ogGateAreas.omMetaAllocUnitList.GetSize();
//    for (int i = 0; i < n; i++)
//	{
//		if (CString(ogGateAreas.omMetaAllocUnitList[i].Alid) == pcpGateAreaId)
//			return ogGateAreas.omMetaAllocUnitList[i].Alfn;
//	}
//
//	return "";
	return olGateAreaName;
}


/////////////////////////////////////////////////////////////////////////////
// PrePlanTableTeamViewer Jobs creation methods

void PrePlanTableTeamViewer::PrePlanTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    PrePlanTableTeamViewer *polViewer = (PrePlanTableTeamViewer *)popInstance;

    if (ipDDXType == JOB_NEW)
        polViewer->ProcessJobNew((JOBDATA *)vpDataPointer);
	else
    if (ipDDXType == JOB_DELETE)
        polViewer->ProcessJobDelete((JOBDATA *)vpDataPointer);
	else
    if (ipDDXType == SHIFT_CHANGE)
        polViewer->ProcessShiftChange((SHIFTDATA *)vpDataPointer);
	else
    if (ipDDXType == SHIFT_NEW)
        polViewer->ProcessShiftChange((SHIFTDATA *)vpDataPointer);


}


BOOL PrePlanTableTeamViewer::FindItemByUrno(JOBDATA *prpJob,int& ripItem, int& ripAssignment)
{
	int ilCount = omLines.GetSize();
    for (int ilLc = 0; ilLc < ilCount; ilLc++)
    {
		int ilJobCount = omLines[ilLc].Line.Assignment.GetSize();
		for ( int ilJc = 0; ilJc < ilJobCount; ilJc++)
		{
			if (omLines[ilLc].Line.Assignment[ilJc].Urno == prpJob->Urno)
			{
				ripAssignment = ilJc;
				ripItem = ilLc;
				return TRUE;
			}
		}
	}
	return FALSE;
}

void PrePlanTableTeamViewer::ProcessJobDelete(JOBDATA *prpJob)
{
	if(CString(prpJob->Jtco) != JOBPOOL)
		return;

	int ilItem;
	int ilAssignment;
	if (FindItemByUrno(prpJob,ilItem,ilAssignment))
	{
//		if (blIsJobPool)
		{
			DeleteAssignment(ilItem,ilAssignment);
		}
	}
}

void PrePlanTableTeamViewer::ProcessJobNew(JOBDATA *prpJob)
{
// Id 19-Sep-96
// Old algorithm:
//		We search for pool jobs and flight jobs. We use a special technique to
//		display the proper gate area instead of pool for a flight manager.
//		If PRID is blank, we display the pool, otherwise we display the gate area.
//		This must be handshaken with ogDataSet.CreateJobPool().
// New algorithm:
//		We still search for pool jobs and flight jobs. But now we can look at
//		JTCO directly since we have another separate type FLS for speical flight
//		job. For a pool job, if the field TEXT is not empty, we will assume that
//		the field TEXT is a gate area ID and display it, otherwise we will display
//		the pool using the value in the field ALID.
//
    // If it's not a job pool or special flight job, ignore it
		BOOL blIsJobPool = CString(prpJob->Jtco) == JOBPOOL;
		BOOL blIsJobFlightSpecial = ((CString(prpJob->Jtco) == JOBFLIGHT) && 
			(prpJob->DisplayInPrePlanTable == TRUE));
		BOOL blIsJobCciSpecial = ((CString(prpJob->Jtco) == JOBCCI) && 
			(prpJob->DisplayInPrePlanTable == TRUE));
		if (!(blIsJobPool || blIsJobFlightSpecial || blIsJobCciSpecial))
			return;

	// Prepare shift URNO and displayed text for this assignment
	long llShiftUrno;
	long llAlidUrno;
	CString olAssignmentText;

	if (blIsJobPool)
	{
		llShiftUrno = prpJob->Shur;
		olAssignmentText = (prpJob->Text[0] != '\0')?
			GetGateAreaName(prpJob->Text): prpJob->Alid;
		llAlidUrno = prpJob->Urno;
	}
	else
	{
		llShiftUrno = prpJob->Shur;
		FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prpJob);
		olAssignmentText = (prlFlight == NULL)? "ERR": prlFlight->Fnum;
		llAlidUrno = prpJob->Urno;
	}
// end of Id 19-Sep-96

    // Create a new assignment
    BOOL blHasToUpdateLine = FALSE;
    PREPLANT_ASSIGNMENT rlAssignment;
    rlAssignment.Alid = olAssignmentText;
    rlAssignment.Urno = llAlidUrno;
	rlAssignment.Acfr = prpJob->Acfr;
    rlAssignment.Acto = prpJob->Acto;
    int ilLineno;
    if (FindShift(llShiftUrno, ilLineno))  // corresponding shift found?
    {   // (for some reason, we have saved SHFCKI.URNO in JOBCKI.FLUR)
        CreateAssignment(ilLineno, &rlAssignment);
        RecalcAssignmentStatus(&omLines[ilLineno].Line);
        blHasToUpdateLine = TRUE;
    }

    int ilTeamMemberno;
    if (bmIsCompleteTeam && FindTeamMember(llShiftUrno, ilLineno, ilTeamMemberno))
    {
        CreateTeamMemberAssignment(ilLineno, ilTeamMemberno, &rlAssignment);
        RecalcAssignmentStatus(&omLines[ilLineno].TeamMember[ilTeamMemberno]);
        RecalcTeamAssignmentStatus(ilLineno);
        blHasToUpdateLine = TRUE;
    }

    // Update the table window if necessary
    if (blHasToUpdateLine)
    {
		CString olRank = omLines[ilLineno].Line.Rank;
        BOOL blIsFlightManager = ogBasicData.IsManager(olRank);

        // Display grouping effect
        switch (omLines[ilLineno].Line.AssignmentStatus)
        {
        case PARTIAL_ASSIGNED:
            pomTable->SetTextLineColor(ilLineno, (blIsFlightManager? BLUE: BLACK), SILVER);
            break;
        case FULLY_ASSIGNED:
            pomTable->SetTextLineColor(ilLineno, (blIsFlightManager? BLUE: WHITE), GRAY);
            pomTable->SetTextLineDragEnable(ilLineno, FALSE);
            break;
        }

        pomTable->ChangeTextLine(ilLineno, Format(&omLines[ilLineno]), &omLines[ilLineno]);
	}
}

void PrePlanTableTeamViewer::ProcessShiftChange(SHIFTDATA *prpShift)
{

	int ilItem;

	if (FindShift(prpShift->Urno,ilItem))
	{
        PREPLANT_SHIFTDATA *prlPpShift = &omLines[ilItem].Line;
	    EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpShift->Peno);

		prlPpShift->ShiftUrno = prpShift->Urno;
		prlPpShift->Peno = prpShift->Peno;
		prlPpShift->Name = ogEmpData.GetEmpName(prlEmp);
	//	prlPpShift->Rank = prlEmp != NULL ? prlEmp->Rank : "";
		prlPpShift->Tmid = prpShift->Egrp;
		prlPpShift->Sfca = prpShift->Sfca;
		prlPpShift->Acfr = prpShift->Avfa;
		prlPpShift->Acto = prpShift->Avta;

		if (ogShiftData.IsAbsent(prpShift))
		{
			prlPpShift->Acfr = TIMENULL;
			prlPpShift->Acto = TIMENULL;
		}

        pomTable->ChangeTextLine(ilItem, Format(&omLines[ilItem]), &omLines[ilItem]);

	}
	else
	{

		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpShift->Peno);
		if (prlEmp == NULL) 
			return;

		// Update viewer data for this shift record
		PREPLANT_SHIFTDATA rlShift;
		rlShift.ShiftUrno = prpShift->Urno;
		rlShift.Peno = prpShift->Peno;
		rlShift.Name = ogEmpData.GetEmpName(prlEmp);
	//	rlShift.Rank = prlEmp->Rank;
		rlShift.Tmid = prpShift->Egrp;
		rlShift.Sfca = prpShift->Sfca;
		rlShift.Acfr = prpShift->Avfa;
		rlShift.Acto = prpShift->Avta;
		rlShift.AssignmentStatus = UNASSIGNED;

		if (ogShiftData.IsAbsent(prpShift))
		{
//			rlShift.Sfca = CString(prpShift->Tpid);
			rlShift.Acfr = TIMENULL;
			rlShift.Acto = TIMENULL;
		}
		MakeLine(&rlShift);
		if (FindShift(prpShift->Urno,ilItem))
		{
			pomTable->InsertTextLine(ilItem,Format(&omLines[ilItem]), &omLines[ilItem]);
			pomTable->DisplayTable();

		//	pomTable->ChangeTextLine(ilItem, Format(&omLines[ilItem]), &omLines[ilItem]);
		}
	}
}

CTime PrePlanTableTeamViewer::StartTime() const
{
	return omStartTime;
}

CTime PrePlanTableTeamViewer::EndTime() const
{
	return omEndTime;
}

int PrePlanTableTeamViewer::Lines() const
{
	return omLines.GetSize();
}

void PrePlanTableTeamViewer::DeleteAssignmentJob(int ipLineNo,int ipIndex)
{
	if (ipLineNo >= 0 && ipLineNo < omLines.GetSize())
	{
		if (ipIndex >= 0 && ipIndex < omLines[ipLineNo].Line.Assignment.GetSize())
		{
			DeleteAssignmentJob(omLines[ipLineNo].Line.Assignment[ipIndex].Urno);
		}
	}
}