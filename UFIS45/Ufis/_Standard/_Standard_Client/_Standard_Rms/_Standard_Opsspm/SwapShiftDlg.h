#if !defined(AFX_SWAPSHIFTDLG_H__6822FD38_460C_487B_B408_C536174E33FA__INCLUDED_)
#define AFX_SWAPSHIFTDLG_H__6822FD38_460C_487B_B408_C536174E33FA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SwapShiftDlg.h : header file
//
#include <CedaShiftData.h>

/////////////////////////////////////////////////////////////////////////////
// CSwapShiftDlg dialog

class CSwapShiftDlg : public CDialog
{
// Construction
public:
	CSwapShiftDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSwapShiftDlg)
	enum { IDD = IDD_SWAPSHIFTDLG };
	CButton	m_OkCtrl;
	CEdit	m_Pkno2Ctrl;
	CEdit	m_Pkno1Ctrl;
	CComboBox	m_WorkGroup2Ctrl;
	CComboBox	m_WorkGroup1Ctrl;
	CComboBox	m_Sub1Sub22Ctrl;
	CComboBox	m_Sub1Sub21Ctrl;
	CComboBox	m_Function2Ctrl;
	CComboBox	m_Function1Ctrl;
	CString	m_FunctionTitle;
	CString	m_NameTitle;
	CString	m_NewShiftTitle;
	CString	m_OldShiftTitle;
	CString	m_PknoTitle;
	CString	m_Sub1Sub2Title;
	CString	m_WorkGroupTitle;
	CString	m_Name1;
	CString	m_Name2;
	CString	m_NewShift1;
	CString	m_NewShift2;
	CString	m_OldShift1;
	CString	m_OldShift2;
	CString	m_Pkno1;
	CString	m_Pkno2;
	BOOL	m_DeleteJobs;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSwapShiftDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSwapShiftDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelemp1();
	afx_msg void OnSelemp2();
	afx_msg void OnChangePkno1();
	afx_msg void OnChangePkno2();
	virtual void OnOK();
	afx_msg void OnSelChangeFunction1();
	afx_msg void OnSelChangeFunction2();
	afx_msg void OnSelChangeWorkgroup1();
	afx_msg void OnSelChangeWorkgroup2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
//	CStringArray omFunctions;
//	CStringArray omTeams;
	bool bmInitialized;
	CString omSday;
	SHIFTDATA *prmOriginalShift1, *prmOriginalShift2, omShift1, omShift2;
	void InitializeShift1Fields();
	void InitializeShift2Fields();
	void InitSub1Sub2ComboBox(CComboBox &ropSub1Sub2Ctrl, SHIFTDATA *prpShift);
	void SetSub1Sub2FieldsIfThereAreChanges();
	CString GetComboBoxSelection(CComboBox &ropComboBox);
	void SetComboBoxSelection(CComboBox &ropComboBox, CString olSelection);
	bool CheckForDeviationOrAbsence(SHIFTDATA *prpShift1, SHIFTDATA *prpShift2);
	bool CheckForShadowShift(SHIFTDATA *prpShift1, SHIFTDATA *prpShift2);
	CMapStringToPtr omFunctionMap, omWorkgroupMap;
	CString omFunction1, omFunction2, omWorkgroup1, omWorkgroup2;

public:
	void SetShifts(SHIFTDATA *prpShift1, SHIFTDATA *prpShift2);
	void SetSday(CString opSday);
	CCSPtrArray <SHIFTDATA> omShifts;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SWAPSHIFTDLG_H__6822FD38_460C_487B_B408_C536174E33FA__INCLUDED_)
