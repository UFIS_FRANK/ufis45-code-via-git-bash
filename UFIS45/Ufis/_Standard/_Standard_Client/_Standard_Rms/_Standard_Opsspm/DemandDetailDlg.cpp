// DemandDetailDlg.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <OpssPm.h>
#include <CCSCedaData.h>
#include <CedaJobData.h>
#include <CedaRudData.h>
#include <CedaRueData.h>
#include <CedaSerData.h>
#include <CedaRpfData.h>
#include <CedaRpqData.h>
#include <CedaSgmData.h>
#include <CedaPerData.h>
#include <CedaFlightData.h>
#include <CedaBasicData.h>
#include <BasicData.h>
#include <CedaTplData.h>
#include <cviewer.h>
#include <FlightDetailViewer.h>
#include <DemandDetailDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DemandDetailDlg dialog


DemandDetailDlg::DemandDetailDlg(CWnd* pParent /*=NULL*/)
	: CDialog(DemandDetailDlg::IDD, pParent)
{
	lmTplUrno = 0L;
	lmRudUrno = 0L;
	lmCurrRueUrno = 0L;

	pomFltViewer = NULL;

	pomParent = pParent;

	//{{AFX_DATA_INIT(DemandDetailDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

DemandDetailDlg::DemandDetailDlg(FlightDetailViewer* pViewer,CWnd* pParent /*=NULL*/)
	: CDialog(DemandDetailDlg::IDD, pParent)
{

	lmTplUrno = 0L;
	lmRudUrno = 0L;
	lmCurrRueUrno = 0L;
	pomFltViewer = pViewer;

	pomParent = pParent;

	//{{AFX_DATA_INIT(DemandDetailDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DemandDetailDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Control(pDX, IDOK, m_Btn_OK);
	DDX_Control(pDX, IDCANCEL, m_Btn_Cancel);
	DDX_Control(pDX, DEMANDDETAIL_COB_TEMPLATE, m_Cob_Template);
	DDX_Control(pDX, DEMANDDETAIL_COB_RULE, m_Cob_Rule);
	DDX_Control(pDX, DEMANDDETAIL_COB_SERVICE, m_Cob_Service);
	DDX_Control(pDX, DEMDETAIL_EDT_SHORTNAME, m_Edt_ShortName);
	DDX_Control(pDX, DEMDETAIL_EDT_NAME, m_Edt_Name);
	DDX_Control(pDX, DEMANDDETAIL_EDT_TIME1, m_Edt_Time1);
	DDX_Control(pDX, DEMANDDETAIL_EDT_TIME2, m_Edt_Time2);
	DDX_Control(pDX, DEMANDDETAIL_EDT_TIME3, m_Edt_Time3);
	DDX_Control(pDX, DEMANDDETAIL_EDT_TIME4, m_Edt_Time4);
	DDX_Control(pDX, DEMANDDETAIL_EDT_TIME5, m_Edt_Time5);
	DDX_Control(pDX, DEMANDDETAIL_EDT_FUNC, m_Edt_Func);
	DDX_Control(pDX, DEMANDDETAIL_EDT_QUA, m_Edt_Qua);
	DDX_Control(pDX, DEMANDDETAIL_EDT_REF1, m_Edt_Ref1);
	DDX_Control(pDX, DEMANDDETAIL_EDT_REF2, m_Edt_Ref2);
	DDX_Control(pDX, DEMANDDETAIL_EDT_REF3, m_Edt_Ref3);
	DDX_Control(pDX, DEMANDDETAIL_EDT_REF4, m_Edt_Ref4);
	DDX_Control(pDX, DEMANDDETAIL_EDT_ALO, m_Edt_Alo);
	DDX_Control(pDX, DEMANDDETAIL_EDT_REF, m_Edt_Ref);
	DDX_Control(pDX, DEMDETAIL_RBT_INBOUND, m_RBt_Inbound);
	DDX_Control(pDX, DEMDETAIL_RBT_OUTBOUND, m_RBt_Outbound);
	DDX_Control(pDX, DEMDETAIL_RBT_TURNAROUND, m_RBt_Turnaround);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DemandDetailDlg, CDialog)
	//{{AFX_MSG_MAP(DemandDetailDlg)
	ON_CBN_EDITCHANGE(DEMANDDETAIL_COB_RULE, OnEditchangeComboRule)
	ON_CBN_EDITCHANGE(DEMANDDETAIL_COB_SERVICE, OnEditchangeComboService)
	ON_WM_DESTROY()
	ON_BN_CLICKED(DEMDETAIL_RBT_INBOUND, OnInbound)
	ON_BN_CLICKED(DEMDETAIL_RBT_OUTBOUND, OnOutbound)
	ON_BN_CLICKED(DEMDETAIL_RBT_TURNAROUND, OnTurnaround)
	ON_CBN_EDITCHANGE(DEMANDDETAIL_COB_TEMPLATE, OnEditchangeCobTemplate)
	ON_CBN_SELCHANGE(DEMANDDETAIL_COB_TEMPLATE, OnSelchangeCobTemplate)
	ON_CBN_SELCHANGE(DEMANDDETAIL_COB_RULE, OnSelchangeCobRule)
	ON_BN_CLICKED(IDOK, OnOK)
	ON_CBN_SELCHANGE(DEMANDDETAIL_COB_SERVICE, OnSelchangeCobService)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DemandDetailDlg message handlers

void DemandDetailDlg::OnOK() 
{
	// TODO: Add extra validation here
	SetTimeValues();
	CDialog::OnOK();
}

void DemandDetailDlg::OnEditchangeComboRule() 
{
	// TODO: Add your control notification handler code here
	
}

void DemandDetailDlg::OnEditchangeComboService() 
{
	// TODO: Add your control notification handler code here
	
}

//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
}

void DemandDetailDlg::OnInbound() 
{
	// TODO: Add your control notification handler code here
	
}

void DemandDetailDlg::OnOutbound() 
{
	// TODO: Add your control notification handler code here
	
}

void DemandDetailDlg::OnTurnaround() 
{
	// TODO: Add your control notification handler code here
	
}

BOOL DemandDetailDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	m_Cob_Template.ResetContent();

	int ilIndex = -1;
	if(lmTplUrno == 0L)
	{
		void *pvlDummy;
		CMapStringToPtr olTemplatesSoFar; 
		CDWordArray olTplUrnos;
		ogBasicData.GetTemplateFilters(olTplUrnos);
		int ilNumTpls = olTplUrnos.GetSize();
		for(int ilTpl = 0; ilTpl < ilNumTpls; ilTpl++)
		{
			TPLDATA *prlTpl = ogTplData.GetTplByUrno(olTplUrnos[ilTpl]);
			if(prlTpl != NULL && !olTemplatesSoFar.Lookup(prlTpl->Tnam,(void *&) pvlDummy))
			{
				if((ilIndex = m_Cob_Template.AddString(prlTpl->Tnam)) != CB_ERR)
				{
					m_Cob_Template.SetItemData(ilIndex, prlTpl->Urno);
					olTemplatesSoFar.SetAt(prlTpl->Tnam,NULL);
				}
			}
		}
	}
	else
	{
		TPLDATA *prlTpl = ogTplData.GetTplByUrno(lmTplUrno);
		if(prlTpl != NULL)
			if((ilIndex = m_Cob_Template.AddString(prlTpl->Tnam)) != CB_ERR)
				m_Cob_Template.SetItemData(ilIndex, prlTpl->Urno);
	}

	if(m_Cob_Template.GetCount() > 0)
	{
		m_Cob_Template.SetCurSel(0);
		lmTplUrno = (long) m_Cob_Template.GetItemData(0);
		SetRules();
	}

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::InitData(FlightDetailViewer* pViewer)
{
	pomFltViewer = pViewer;
}
//-----------------------------------------------------------------------------------------------------------------------


CTime DemandDetailDlg::GetTimeFromFlight(FLIGHTDATA *opAF, CString opField)
{
	CTime opTime;
	if(opAF!= NULL)
	{
		if (opAF->Adid[0] == 'D')
		{
			if (opAF->Tifd != TIMENULL)
				opTime = opAF->Tifd;
			if (!opField.CompareNoCase("TIFD"))
				return opTime;
			if (opAF->Stod != TIMENULL)
				opTime = opAF->Stod;
			if (!opField.CompareNoCase("STOD"))
				return opTime;
			if (opAF->Etod != TIMENULL)
				opTime = opAF->Etod;
			if (!opField.CompareNoCase("ETDI"))
				return opTime;
			if (opAF->Etod != TIMENULL)
				opTime = opAF->Etod;
			if (!opField.CompareNoCase("ETOD"))
				return opTime;
			if (opAF->Ofbl != TIMENULL)
				opTime = opAF->Ofbl;
			if (!opField.CompareNoCase("OFBL"))
				return opTime;
			if (opAF->Airb != TIMENULL)
				opTime = opAF->Airb;
			if (!opField.CompareNoCase("AIRB"))
				return opTime;
		}
		else if (opAF->Adid[0] == 'A')
		{
			if (opAF->Tifa != TIMENULL)
				opTime = opAF->Tifa;
			if (!opField.CompareNoCase("TIFA"))
				return opTime;
			if (opAF->Stoa != TIMENULL)
				opTime = opAF->Stoa;
			if (!opField.CompareNoCase("STOA"))
				return opTime;
			if (opAF->Etoa != TIMENULL)
				opTime = opAF->Etoa;
			if (!opField.CompareNoCase("ETAI"))
				return opTime;
			if (opAF->Etoa != TIMENULL)
				opTime = opAF->Etoa;
			if (!opField.CompareNoCase("ETOA"))
				return opTime;
			if (opAF->Tmoa != TIMENULL)
				opTime = opAF->Tmoa;
			if (!opField.CompareNoCase("TMOA"))
				return opTime;
			if (opAF->Land != TIMENULL)
				opTime = opAF->Land;
			if (!opField.CompareNoCase("LAND"))
				return opTime;
			if (opAF->Onbe != TIMENULL)
				opTime = opAF->Onbe;
			if (!opField.CompareNoCase("ONBE"))
				return opTime;
			if (opAF->Onbl != TIMENULL)
				opTime = opAF->Onbl;
			if (!opField.CompareNoCase("ONBL"))
				return opTime;
		}
	}
	return opTime;
}



//-----------------------------------------------------------------------------------------------------------------------
//					set methods
//-----------------------------------------------------------------------------------------------------------------------
void DemandDetailDlg::SetTimeValues()
{
	long	llDiff;                                                                          
	long	llMinDedu;                                                                       
	CTime	tlDebe;                                                                        
	CTime	tlDeen;                                                                        
	CTime tlTime1;
	CTime tlTime2;
	CTimeSpan tlSpan;

	RUDDATA *blpRudData;

	if (lmRudUrno != 0L)
	{
		blpRudData = ogRudData.GetRudByUrno(lmRudUrno);
	}

	/************ calculate demand begin ****/                                           
	if ( (blpRudData->Drty[0] == '1') || (blpRudData->Drty[0] == '0') )                                        
	{	/*** calculate times for INBOUND-related demand ***************/                   
		if ( strcmp (blpRudData->Rtdb,"") != 0)                                                            
		{                                                                                  
			/************** duty reference time ******************/                          
			tlTime1 = GetTimeFromFlight(pomFltViewer->pomAF,blpRudData->Rtdb);
			tlTime1 += blpRudData->Tsdb;
		}                                                                                  
	}                                                                                    
	/************ calculate demand end ****/                                             
	if ( (blpRudData->Drty[0] == '2') || (blpRudData->Drty[0] == '0') )                                        
	{	/*** calculate times for OUTBOUND-related demand ***************/                  
		if ( strcmp (blpRudData->Rtde,"") != 0)                                                            
		{                                                                                  
			/************** duty reference time ******************/                          
			tlTime2 = GetTimeFromFlight(pomFltViewer->pomDF,blpRudData->Rtde);
			tlTime2 += blpRudData->Tsde;
		}                                                                                  
	}                                                                                    
	if ( blpRudData->Drty[0] == '1' )                                                               
	{                                                                                    
		/********* real duty begin + duty duration = duty end *************/ 
		tlTime2 = tlTime1 + blpRudData->Dedu;
	}                                                                                    
	if ( blpRudData->Drty[0] == '2' )                                                               
	{                                                                                    
		/********* real duty end - duty duration = duty begin *************/               
		tlTime1 = tlTime2 - CTimeSpan(blpRudData->Dedu);
	}                                                                                   
                                                                                     
	/*********** substract setup time **********************/                            
	tlTime1 = tlTime1 - CTimeSpan(blpRudData->Suti);
	/*********** substract time to go to ***************/                                
	tlTime1 = tlTime1 - CTimeSpan(blpRudData->Ttgt);
		                                                                                 
	/*********** substract setup time **********************/                            
	tlTime2 = tlTime2 + CTimeSpan(blpRudData->Sdti);
	/*********** substract time to go from ***************/                              
	tlTime2 = tlTime2 + CTimeSpan(blpRudData->Ttgf);
                                                                                     
	/*  for turnaround demands check min. demand length */                               
	if (blpRudData->Drty[0] == '0')                                                                 
	{                                                                                    
		tlSpan = tlTime2 - tlTime1;                                                         
		llDiff = tlSpan.GetTotalSeconds();
		if (llDiff < 1800)                                                            
		{   
			tlTime2 = tlTime1 + CTimeSpan(1800);
		}                                                                                  
	}                                                                                    
	omDebe = tlTime1;                                                        
	omDeen = tlTime2;
	tlSpan = tlTime2 - tlTime1; 
	lmDedu = tlSpan.GetTotalSeconds();
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::SetRules()
{
	m_Cob_Rule.ResetContent();
	if (lmTplUrno != 0L)
	{
		CCSPtrArray <RUEDATA> olRules;
		ogRueData.GetRuesByTemplate(olRules,lmTplUrno, true);

		int ilNumRues = olRules.GetSize();
		RUEDATA *blpRueData;
		int ilIndex;
		int ilGeneric = 0;
		for(int ilD = 0; ilD < ilNumRues; ilD++)
		{
			blpRueData = &olRules[ilD];
			ilIndex = m_Cob_Rule.AddString(blpRueData->Runa);
			m_Cob_Rule.SetItemData(ilIndex,blpRueData->Urno);
			CString olShortName = CString("") + blpRueData->Rusn;
			if (olShortName.Right(5).Compare("ADHOC")==0)
			{
				ilGeneric = ilIndex;
			}
		}
		if (m_Cob_Rule.GetCount() > 0)
		{
			m_Cob_Rule.SetCurSel(ilGeneric);
			lmCurrRueUrno = m_Cob_Rule.GetItemData(ilGeneric);
			SetServices();
		}
	}

}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::SetServices()
{
	m_Cob_Service.ResetContent();
	if (lmCurrRueUrno != 0L)
	{
		CCSPtrArray <RUDDATA> olRuds;
		ogRudData.GetRudsByUrue(lmCurrRueUrno,olRuds);

		int ilNumRuds = olRuds.GetSize();
		RUDDATA *blpRudData;
		int ilIndex;
		CString olString;
		for(int ilD = 0; ilD < ilNumRuds; ilD++)
		{
			CString func = "";
			blpRudData = &olRuds[ilD];
			SERDATA *blpSerData = ogSerData.GetSerByUrno(blpRudData->Ughs);
			RPFDATA *blpRpfData = ogRpfData.GetFunctionByUrud(blpRudData->Urno);
			if (blpRpfData)
			{
				func = blpRpfData->Fcco;
			}
			if (blpSerData)
			{
				olString =  blpSerData->Snam + CString("(") + func + CString(")");
			}
			else
			{
				olString = blpRudData->Ughs;
			}
			if((ilIndex = m_Cob_Service.AddString(olString)) != CB_ERR)
				m_Cob_Service.SetItemData(ilIndex,blpRudData->Urno);
		}
		if (m_Cob_Service.GetCount() > 0)
		{
			m_Cob_Service.SetCurSel(0);
			lmRudUrno = m_Cob_Service.GetItemData(0);
			SetControls();
		}
	}

}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::SetControls()
{
	RUDDATA *blpRudData;
	RUEDATA *blpRueData; 
	SERDATA *blpSerData;

	if (lmRudUrno != 0L)
	{
		blpRudData = ogRudData.GetRudByUrno(lmRudUrno);
		omRud = blpRudData;

		blpSerData = ogSerData.GetSerByUrno(blpRudData->Ughs);
	}

	if (lmCurrRueUrno != 0L)
	{
		blpRueData = ogRueData.GetRueByUrno(lmCurrRueUrno);
		omRue = blpRueData;
	}


	if (blpRudData != NULL && blpRueData != NULL && blpSerData != NULL)
	{
		
		CString olTmp;

		olTmp= CString(blpSerData->Snam);
		m_Edt_Name.SetWindowText(olTmp); 
		m_Edt_Name.EnableWindow(TRUE);

		olTmp = CString(blpSerData->Seco);
		m_Edt_ShortName.SetWindowText(olTmp); 
		m_Edt_ShortName.EnableWindow(TRUE);

		olTmp.Format("%d",blpRudData->Dedu / 60);
		m_Edt_Time1.SetWindowText(olTmp); 
		m_Edt_Time1.EnableWindow(TRUE);

		olTmp.Format("%d",blpRudData->Suti / 60);
		m_Edt_Time2.SetWindowText(olTmp); 
		m_Edt_Time2.EnableWindow(TRUE);

		olTmp.Format("%d",blpRudData->Sdti / 60);
		m_Edt_Time3.SetWindowText(olTmp); 
		m_Edt_Time3.EnableWindow(TRUE);

		olTmp.Format("%d",blpRudData->Ttgf / 60);
		m_Edt_Time4.SetWindowText(olTmp); 
		m_Edt_Time4.EnableWindow(TRUE);

		olTmp.Format("%d",blpRudData->Ttgt / 60);
		m_Edt_Time5.SetWindowText(olTmp); 
		m_Edt_Time5.EnableWindow(TRUE);
		
		RPFDATA *blpRpfData = ogRpfData.GetFunctionByUrud(blpRudData->Urno);
		if (blpRpfData != NULL)
		{
			olTmp = blpRpfData->Fcco;
		}
		else
		{
			olTmp = CString("");
		}
		m_Edt_Func.SetWindowText(olTmp); 
		m_Edt_Func.EnableWindow(TRUE);

		olTmp = CString("");
		CCSPtrArray<RPQDATA> olPermits;
		ogRpqData.GetRpqsByUrud(blpRudData->Urno,olPermits);
		for (int ilC = 0; ilC < olPermits.GetSize(); ilC++)
		{
			RPQDATA *prlPermit = &olPermits[ilC];
			if (prlPermit)
			{
				if (strcmp(prlPermit->Quco,"") == 0)
				{
					olTmp = "";
					// this is a header for a group of permits, one of which the emp must have
					CCSPtrArray <SGMDATA> olSgms;
					ogSgmData.GetSgmListByUsgr(prlPermit->Uper,olSgms);
					int ilNumSgms = olSgms.GetSize();
					for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
					{
						PERDATA *prlPer = ogPerData.GetPerByUrno(olSgms[ilSgm].Uval);
						if(prlPer != NULL)
						{
							if (!olTmp.IsEmpty())
								olTmp += ',';
							olTmp += prlPer->Prmc;
						}
					}
				}
				else	// single permit
					olTmp = prlPermit->Quco;
			}
		}
		m_Edt_Qua.SetWindowText(olTmp); 
		m_Edt_Qua.EnableWindow(TRUE);

		olTmp = blpRudData->Rtdb;
		m_Edt_Ref1.SetWindowText(olTmp); 
		m_Edt_Ref1.EnableWindow(TRUE);

		olTmp.Format("%d",blpRudData->Tsdb / 60);
		m_Edt_Ref2.SetWindowText(olTmp); 
		m_Edt_Ref2.EnableWindow(TRUE);

		olTmp = blpRudData->Rtde;
		m_Edt_Ref3.SetWindowText(olTmp); 
		m_Edt_Ref3.EnableWindow(TRUE);

		olTmp.Format("%d",blpRudData->Tsde / 60);
		m_Edt_Ref4.SetWindowText(olTmp); 
		m_Edt_Ref4.EnableWindow(TRUE);

		olTmp = blpRudData->Aloc;
		m_Edt_Alo.SetWindowText(olTmp); 
		m_Edt_Alo.EnableWindow(TRUE);

		m_Edt_Ref.SetWindowText(olTmp); 
		m_Edt_Ref.EnableWindow(FALSE);
		SetIOTRadioButtons();

	}
}
//-----------------------------------------------------------------------------------------------------------------------
void DemandDetailDlg::SetIOTRadioButtons()
{
	RUDDATA *blpRudData;
	if (lmRudUrno != 0L)
	{
		blpRudData = ogRudData.GetRudByUrno(lmRudUrno);
	}

	CString olBarType;
	olBarType = blpRudData->Drty;

	// check appropriate radio button
	m_RBt_Turnaround.SetCheck(0);
	m_RBt_Inbound.SetCheck(0);
	m_RBt_Outbound.SetCheck(0);


	if (olBarType == "0")	// turnaround
	{
		m_RBt_Turnaround.SetCheck(1);
	}
	else if(olBarType == "1")	// inbound
	{
		m_RBt_Inbound.SetCheck(1);
	}
	else if(olBarType == "2" )		// outbound
	{
		m_RBt_Outbound.SetCheck(1);
	}
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::EnableIOTRadioButtons(CString opEvty)
{
}

void DemandDetailDlg::OnEditchangeCobTemplate() 
{
	// TODO: Add your control notification handler code here
	int ilIndex = m_Cob_Template.GetCurSel();
	if(ilIndex != CB_ERR)
	{
		lmTplUrno = (long) m_Cob_Template.GetItemData(ilIndex);
		SetRules();
	}
}

void DemandDetailDlg::OnSelchangeCobTemplate() 
{
	// TODO: Add your control notification handler code here
	int ilIndex = m_Cob_Template.GetCurSel();
	if(ilIndex != CB_ERR)
	{
		lmTplUrno = (long) m_Cob_Template.GetItemData(ilIndex);
		SetRules();
	}
}

void DemandDetailDlg::OnSelchangeCobRule() 
{
	// TODO: Add your control notification handler code here
	int ilIndex = m_Cob_Rule.GetCurSel();
	if(ilIndex != CB_ERR)
	{
		lmCurrRueUrno = (long) m_Cob_Rule.GetItemData(ilIndex);
		SetServices();
	}
}

void DemandDetailDlg::OnSelchangeCobService() 
{
	// TODO: Add your control notification handler code here
	int ilIndex = m_Cob_Service.GetCurSel();
	if(ilIndex != CB_ERR)
	{
		lmRudUrno = (long) m_Cob_Service.GetItemData(ilIndex);
		SetControls();
	}
}
