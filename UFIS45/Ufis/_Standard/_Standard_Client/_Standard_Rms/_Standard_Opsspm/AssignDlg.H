// AssignDlg.h : header file
// 
#ifndef _ASSIGNDIALOG_
#define _ASSIGNDIALOG_

// Constants for reference to each job type
#define FIRSTRADIO	(RADIOFLUG)
#define LASTRADIO	(RADIOSONSTINGES)
#define ASSIGNMENT_JOBFLIGHT	(RADIOFLUG - FIRSTRADIO)
#define ASSIGNMENT_JOBCCI		(RADIOCCIDESK - FIRSTRADIO)
#define ASSIGNMENT_JOBPOOL		(RADIOPOOL - FIRSTRADIO)
#define ASSIGNMENT_JOBGATE		(RADIOGATE - FIRSTRADIO)
#define ASSIGNMENT_JOBGATEAREA	(RADIOGATEBEREICH - FIRSTRADIO)
#define ASSIGNMENT_JOBSPECIAL	(RADIOSONSTINGES - FIRSTRADIO)

/////////////////////////////////////////////////////////////////////////////
// CAssignmentDialog dialog

class CAssignmentDialog : public CDialog
{
// Construction
public:
	CAssignmentDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	BOOL bmHalfWidth;
	CTime omStartTime;
	CTime omEndTime;
	CStringArray omStaffList;
	CString omFirstStaffName;

	//{{AFX_DATA(CAssignmentDialog)
	enum { IDD = IDD_ASSIGNMENT };
	int			m_JobType;
	CString		m_Description;
	CTime		m_EndDate;
	CTime		m_EndTime;
	CTime		m_StartDate;
	CTime		m_StartTime;
	CListBox	m_List;
	//}}AFX_DATA

// Output data member
public:
	// The caller may check this array for each selected item on return
	CWordArray omSelectedItems;

public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAssignmentDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CRect	m_rcDescription;
	int		m_yTopOfFirstRadio;

	// Generated message map functions
	//{{AFX_MSG(CAssignmentDialog)
	virtual BOOL OnInitDialog();
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	void SetEditControlPos();
};
#endif // _ASSIGNDIALOG_
