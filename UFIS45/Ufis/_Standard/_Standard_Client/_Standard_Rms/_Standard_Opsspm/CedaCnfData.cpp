// CedaCnfData.cpp - Class for conflict data
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaCnfData.h>
#include <BasicData.h>

CedaCnfData::CedaCnfData()
{                  
    BEGIN_CEDARECINFO(CNFDATA, CnfDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Utyp,"UTYP")
		FIELD_DATE(Coti,"COTI")
		FIELD_INT(Stat,"STAT")
		FIELD_CHAR_TRIM(Olva,"OLVA")
		FIELD_LONG(Ourn,"OURN")
		FIELD_DATE(Lstu,"LSTU")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CnfDataRecInfo)/sizeof(CnfDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CnfDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"CNFCKI");
    pcmFieldList = "URNO,UTYP,COTI,STAT,OLVA,OURN,LSTU";
}
 
CedaCnfData::~CedaCnfData()
{
	TRACE("CedaCnfData::~CedaCnfData called\n");
	ClearAll();
}

void CedaCnfData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	for( POSITION ilPos =  omOurnMap.GetStartPosition(); ilPos != NULL; )
	{
		long llOurn;
		CMapPtrToPtr *polSubOurnMap;
		omOurnMap.GetNextAssoc( ilPos, (void *&)llOurn, (void *&)polSubOurnMap);
		polSubOurnMap->RemoveAll();
		delete polSubOurnMap;
	}
	omOurnMap.RemoveAll();
}

CCSReturnCode CedaCnfData::ReadCnfData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaCnfData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		CNFDATA rlCnfData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlCnfData)) == RCSuccess)
			{
				AddCnfInternal(rlCnfData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaCnfData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(CNFDATA), pclWhere);
    return ilRc;
}


void CedaCnfData::AddCnfInternal(CNFDATA &rrpCnf)
{
	CNFDATA *prlCnf = new CNFDATA;
	*prlCnf = rrpCnf;
	omData.Add(prlCnf);
	omUrnoMap.SetAt((void *)prlCnf->Urno,prlCnf);
}

// OURN is the URNO of the object that caused the conflict eg. flight URNO
// create a map of the format
// OURN1 {cnf1,cnf2...cnfN} --- OURN2 {cnf1,cnf2...cnfN} ..... OURNN  {cnf1,cnf2...cnfN}
bool CedaCnfData::AddCnfToOurnMap(CNFDATA &rrpCnf)
{
	bool blAdded = false;

	CMapPtrToPtr *pomSubOurnMap = NULL;

	if(omOurnMap.Lookup((void *)rrpCnf.Ourn, (void *&) pomSubOurnMap))
	{
		pomSubOurnMap->SetAt((void *)rrpCnf.Urno,&rrpCnf);
		blAdded = true;
	}
	else
	{
		pomSubOurnMap = new CMapPtrToPtr;
		pomSubOurnMap->SetAt((void *)rrpCnf.Urno,&rrpCnf);
		omOurnMap.SetAt((void *)rrpCnf.Ourn,pomSubOurnMap);
		blAdded = true;
	}

	return blAdded;
}

// get list of conflicts for the OURN eg. for a flight URNO
// return true if something found
bool CedaCnfData::GetCnfByOurn(long lpOurn, CCSPtrArray <CNFDATA> &ropOurnCnfList, bool bpReset /*true*/)
{
	bool blFound = false;

	if(bpReset)
	{
		ropOurnCnfList.RemoveAll();
	}
	
	CMapPtrToPtr *prlSubOurnMap;
	if(omOurnMap.Lookup( (void *)lpOurn, (void *&) prlSubOurnMap))
	{
		for(POSITION ilPos = prlSubOurnMap->GetStartPosition(); ilPos != NULL; )
		{
			long llUrno; CNFDATA *prlCnf;
			prlSubOurnMap->GetNextAssoc(ilPos, (void *&)llUrno, (void *&)prlCnf);
			ropOurnCnfList.Add(prlCnf);
			blFound = true;
		}
	}

	return blFound;
}

// get the old value for the Ourn (eg. Flight Urno) specified and the conflict
// type
bool CedaCnfData::GetOldValueByOurn(long lpOurn, long lpUtyp, char *pcpOldValue)
{
	bool blFound = false;
	CTime olNewest = TIMENULL;

	CCSPtrArray <CNFDATA> olOurnCnfList;
	if(GetCnfByOurn(lpOurn, olOurnCnfList))
	{
		int ilNumCnf = olOurnCnfList.GetSize();
		for(int ilCnf = 0; ilCnf < ilNumCnf; ilCnf++)
		{
			CNFDATA *prlCnf = &olOurnCnfList[ilCnf];
			if(prlCnf->Utyp == lpUtyp && (olNewest == TIMENULL || prlCnf->Lstu > olNewest))
			{
				strcpy(pcpOldValue,prlCnf->Olva);
				blFound = true;
			}
		}
	}

	return blFound;
}


CString CedaCnfData::GetTableName(void)
{
	return CString(pcmTableName);
}