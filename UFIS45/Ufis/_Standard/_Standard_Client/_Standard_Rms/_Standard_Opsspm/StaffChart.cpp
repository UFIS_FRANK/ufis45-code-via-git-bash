// stafchrt.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <resource.h>
#include <OpssPm.h>
#include <cxbutton.h>
#include <clientwn.h>
#include <tscale.h>
#include <CCSDragDropCtrl.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <StaffViewer.h>
#include <StaffGantt.h>
#include <StaffDiagram.h>
#include <PrePlanTable.h>
#include <dataset.h>
//-DTT Jul.22-----------------------------------------------------------
#include <AssignDlg.h>
#include <AssignFromPrePlanTableDlg.h>	// for CAssignmentFromPrePlanTableDialog
#include <AssignJobDetachDlg.h>	// for CAssignmentJobDetachDialog
#include <SelectPoolStaffDlg.h>
#include <SelectPoolStaffForJobFlightDlg.h>	// for CSelectPoolStaffForJobFlightDialog
#include <TeamDelegationDlg.h>
//----------------------------------------------------------------------

#include <StaffChart.h>
#include <BasicData.h>
#include <AllocData.h>
#include <PrmAssignPdaDlg.h>
#include <AllocDlg.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static int ByName(const CString **ppp1, const CString **ppp2)
{
	int ilCompare = (*ppp1)->Compare(**ppp2);
	return ilCompare;
}

static int ByName2(const CString **ppp1, const CString **ppp2)
{
	if ((*ppp1)->GetLength() < (*ppp2)->GetLength())
		return -1;
	else if ((*ppp1)->GetLength() > (*ppp2)->GetLength())
		return 1;

	int ilLen = (*ppp1)->GetLength();
	for (int ilC = 0; ilC < ilLen; ilC++)
	{
		if ((*ppp1)->GetAt(ilC) < (*ppp2)->GetAt(ilC))
			return -1;
		else if ((*ppp1)->GetAt(ilC) > (*ppp2)->GetAt(ilC))
			return 1;
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// StaffChart

IMPLEMENT_DYNCREATE(StaffChart, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

StaffChart::StaffChart()
{
    pomTimeScale = NULL;
    pomStatusBar = NULL;
    pomViewer = NULL;

	bmScrolling = false;
    
    pomTopScaleText = NULL;
	pomCountText = NULL;
}

StaffChart::~StaffChart()
{
    if (pomTopScaleText != NULL)
        delete pomTopScaleText;
	if (pomCountText != NULL)
		delete pomCountText;
}

BEGIN_MESSAGE_MAP(StaffChart, CFrameWnd)
    //{{AFX_MSG_MAP(StaffChart)
    ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_PAINT()
    ON_BN_CLICKED(IDC_CHARTBUTTON, OnChartButton)
    ON_MESSAGE(WM_CCSBUTTON_RBUTTONDOWN, OnChartButtonRButtonDown)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_COMMAND(31, OnMenuAssign)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
    ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


int StaffChart::GetHeight()
{
    if (imState == Minimized)
        return imStartVerticalScalePos; // height of ChartButtton/TopScaleText
    else                                // (imState == Normal) || (imState == Maximized)
		return imStartVerticalScalePos + omGantt.GetGanttChartHeight() + 7;
        //return imStartVerticalScalePos + imHeight + 2;
}

/////////////////////////////////////////////////////////////////////////////
// StaffChart message handlers

#include <clientwn.h>

int StaffChart::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
        return -1;
    
    // TODO: Add your specialized creation code here
    CString olStr; GetWindowText(olStr);
    omButton.Create(olStr, BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, CRect(5, 4, 130, 4 + 20), this, IDC_CHARTBUTTON);
    omButton.SetFont(&ogMSSansSerif_Bold_8, FALSE);
   
	//
    pomCountText = new C3DStatic(TRUE);
    pomCountText->Create("999", WS_CHILD | WS_VISIBLE,
        CRect(130 + 10, 4 + 1, imStartTopScaleTextPos - 10, (4 + 20) - 1), this);
	//
    
	CRect olRect; GetClientRect(&olRect);

    pomTopScaleText = new C3DStatic(TRUE);
    pomTopScaleText->Create("Top Scale Text", WS_CHILD | WS_VISIBLE,
        CRect(imStartTopScaleTextPos + 6, 4 + 1, olRect.right - 6, (4 + 20) - 1), this);

	m_ChartWindowDragDrop.RegisterTarget(this, this);
    m_ChartButtonDragDrop.RegisterTarget(&omButton, this);
    m_CountTextDragDrop.RegisterTarget(pomCountText, this);
    m_TopScaleTextDragDrop.RegisterTarget(pomTopScaleText, this);

    ////
    // read all data
    //SetState(Maximized);
    
    omGantt.SetTimeScale(GetTimeScale());
    omGantt.SetViewer(GetViewer(), GetGroupNo());
    omGantt.SetStatusBar(GetStatusBar());
    omGantt.SetDisplayWindow(GetStartTime(), GetStartTime() + GetInterval());
    omGantt.SetVerticalScaleWidth(imStartTopScaleTextPos);
    omGantt.SetFonts(ogStaffIndexes.VerticalScale, ogStaffIndexes.Chart);
    omGantt.SetVerticalScaleColors(NAVY, SILVER, BLACK, YELLOW);
//    omGantt.SetVerticalScaleColors(NAVY, SILVER, YELLOW, SILVER);
    omGantt.Create(0, CRect(olRect.left, imStartVerticalScalePos, olRect.right, olRect.bottom), this);

    olStr = GetViewer()->GetGroupText(GetGroupNo());
    omButton.SetWindowText(olStr);

#ifndef	PICHIT_FIXED_THE_COUNTER
	char clBuf[255];
	int ilCount = GetViewer()->GetLineCount(GetGroupNo());
	sprintf(clBuf, "%d", ilCount);
	pomCountText->SetWindowText(clBuf);
#else
	char clBuf[255];
	int ilCount = GetViewer()->GetLineCount(GetGroupNo());
	int ilManagerCount = GetViewer()->GetManagerCount(GetGroupNo());
	sprintf(clBuf, "%d %d", ilCount, ilManagerCount);
	pomCountText->SetWindowText(clBuf);
#endif

    olStr = GetViewer()->GetGroupTopScaleText(GetGroupNo());
    pomTopScaleText->SetWindowText(olStr);
    
    pomTopScaleText->GetWindowText(clBuf, sizeof(clBuf));
    //TRACE("pomTopScaleText: [%s][%d]\n", clBuf, strlen(clBuf));
    
    ////imHeight = omGantt.GetGanttChartHeight();
    //TRACE("imHeight=%d\n", imHeight);
    //
    return 0;
}

void StaffChart::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	// Id 30-Sep-96
	// This will remove a lot of warning message when the user change view.
	// If we just delete a staff chart, MFC will produce two warning message.
	// First, Revoke not called before the destructor.
	// Second, calling DestroyWindow() in CWnd::~CWnd.

	m_ChartWindowDragDrop.Revoke();
    m_ChartButtonDragDrop.Revoke();
    m_CountTextDragDrop.Revoke();
    m_TopScaleTextDragDrop.Revoke();

	CFrameWnd::OnDestroy();
}

void StaffChart::OnSize(UINT nType, int cx, int cy) 
{
    // TODO: Add your message handler code here
    CFrameWnd::OnSize(nType, cx, cy);
    
    CRect olClientRect; GetClientRect(&olClientRect);
    //TRACE("StaffChart OnSize: client rect [top=%d, bottom+%d]\n", olClientRect.top, olClientRect.bottom);
    CRect olRect(imStartTopScaleTextPos + 6, 4 + 1, olClientRect.right - 6, (4 + 20) - 1);
    pomTopScaleText->MoveWindow(&olRect, FALSE);

    olRect.SetRect (olClientRect.left, imStartVerticalScalePos, olClientRect.right, olClientRect.bottom);
    //TRACE("StaffChart SetRect: client rect [top=%d, bottom+%d]\n", imStartVerticalScalePos, olClientRect.bottom);
    omGantt.MoveWindow(&olRect, FALSE);
}

BOOL StaffChart::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void StaffChart::OnPaint()
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    
    // Do not call CFrameWnd::OnPaint() for painting messages
    CPen *polOldPen = (CPen *) dc.SelectStockObject(BLACK_PEN);

    CRect olClientRect;
    GetClientRect(&olClientRect);
    
#define imHorizontalPos imStartVerticalScalePos
#define imVerticalPos imStartTopScaleTextPos

    // draw horizontal seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 2);
    dc.LineTo(olClientRect.right, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 1);
    dc.LineTo(olClientRect.right, imHorizontalPos - 1);
    //

    // draw vertical seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(imVerticalPos - 2, olClientRect.top);
    //dc.LineTo(imVerticalPos - 2, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 2, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(imVerticalPos - 1, olClientRect.top);
    //dc.LineTo(imVerticalPos - 1, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 1, imHorizontalPos - 2);
    //

    dc.SelectObject(polOldPen);
}

void StaffChart::OnChartButton()
{
    if (imState == Minimized)
        SetState(Maximized);
    else
        SetState(Minimized);

	GetParent() -> GetParent() -> SendMessage(WM_POSITIONCHILD, 0, 0L);
}

LONG StaffChart::OnChartButtonRButtonDown(UINT wParam, LONG lParam)
{
	CMenu olMenu;
	olMenu.CreatePopupMenu();
	for (int i = olMenu.GetMenuItemCount(); i > 0; i--)
	     olMenu.RemoveMenu(i, MF_BYPOSITION);

	if(pomViewer->GetLineCount(imGroupNo) > 0)
	{
	    olMenu.AppendMenu(MF_STRING,31, GetString(IDS_STRING61388)); // "Assign"
	}

	//CString olPool = GetViewer()->GetGroupText(GetGroupNo()); //Commented Singapore
	CString olPool = GetViewer()->GetPool(GetGroupNo()); //Added Singapore
	CMenu olCicMenu, olGateMenu, olRegnMenu, olPosiMenu;
	UINT ilFlags;

	if(ogBasicData.bmDisplayCheckins)
	{
		olCicMenu.CreatePopupMenu();
		CStringArray olAlocUnits;
		int ilUnit, ilNumUnits = ogBasicData.GetAlocUnitsByPool(ALLOCUNITTYPE_CIC, olPool, olAlocUnits);
		if(ilNumUnits > 0)
		{
			CCSPtrArray<CString> olSortedAlocUnits;
			for(ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
			{
				olSortedAlocUnits.New(olAlocUnits[ilUnit]);
			}

			olSortedAlocUnits.Sort(ByName);

			for(ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
			{
				ilFlags = ((ilUnit+1) % 30) ? MF_STRING|MF_DISABLED : MF_STRING|MF_DISABLED|MF_MENUBARBREAK;
				CString olCic = olSortedAlocUnits[ilUnit];
				olCicMenu.AppendMenu(ilFlags, 0, olCic);
			}

			olSortedAlocUnits.DeleteAll();
		}
		else
		{
			olCicMenu.AppendMenu(MF_STRING|MF_DISABLED, 0, GetString(IDS_STRING61328)); // "None"
		}

		olMenu.AppendMenu(MF_STRING|MF_POPUP|MF_ENABLED,(UINT)olCicMenu.m_hMenu,GetString(IDS_STRING61329)); // "Available Checkin Counters"
	}

	if(ogBasicData.bmDisplayGates)
	{
		olGateMenu.CreatePopupMenu();
		CStringArray olAlocUnits;
		int ilUnit, ilNumUnits = ogBasicData.GetAlocUnitsByPool(ALLOCUNITTYPE_GATE, olPool, olAlocUnits);
		if(ilNumUnits > 0)
		{
			CCSPtrArray<CString> olSortedAlocUnits;
			for(ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
			{
				olSortedAlocUnits.New(olAlocUnits[ilUnit]);
			}

			olSortedAlocUnits.Sort(ByName2);

			for(ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
			{
				olGateMenu.AppendMenu(MF_STRING|MF_DISABLED, 0, olSortedAlocUnits[ilUnit]);
			}

			olSortedAlocUnits.DeleteAll();
		}
		else
		{
			olGateMenu.AppendMenu(MF_STRING|MF_DISABLED, 0, GetString(IDS_STRING61328)); // "None"
		}

		olMenu.AppendMenu(MF_STRING|MF_POPUP|MF_ENABLED,(UINT)olGateMenu.m_hMenu,GetString(IDS_STRING61332)); // "Available Gates"
	}

	if(ogBasicData.bmDisplayPositions)
	{
		olPosiMenu.CreatePopupMenu();
		CStringArray olAlocUnits;
		int ilUnit, ilNumUnits = ogBasicData.GetAlocUnitsByPool(ALLOCUNITTYPE_PST, olPool, olAlocUnits);
		if(ilNumUnits > 0)
		{
			CCSPtrArray<CString> olSortedAlocUnits;
			for(ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
			{
				olSortedAlocUnits.New(olAlocUnits[ilUnit]);
			}

			olSortedAlocUnits.Sort(ByName2);

			for(ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
			{
				olPosiMenu.AppendMenu(MF_STRING|MF_DISABLED, 0, olSortedAlocUnits[ilUnit]);
			}

			olSortedAlocUnits.DeleteAll();
		}
		else
		{
			olPosiMenu.AppendMenu(MF_STRING|MF_DISABLED, 0, GetString(IDS_STRING61328)); // "None"
		}

		olMenu.AppendMenu(MF_STRING|MF_POPUP|MF_ENABLED,(UINT)olPosiMenu.m_hMenu,GetString(IDS_AVAILABLE_POSITIONS)); // "Available Positions"
	}

	if(ogBasicData.bmDisplayRegistrations)
	{
		olRegnMenu.CreatePopupMenu();
		CStringArray olAlocUnits;
		int ilUnit, ilNumUnits = ogBasicData.GetAlocUnitsByPool(ALLOCUNITTYPE_REGN, olPool, olAlocUnits);
		if(ilNumUnits > 0)
		{
			CCSPtrArray<CString> olSortedAlocUnits;
			for(ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
			{
				olSortedAlocUnits.New(olAlocUnits[ilUnit]);
			}

			olSortedAlocUnits.Sort(ByName2);

			for(ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
			{
				olRegnMenu.AppendMenu(MF_STRING|MF_DISABLED, 0, olSortedAlocUnits[ilUnit]);
			}

			olSortedAlocUnits.DeleteAll();
		}
		else
		{
			olRegnMenu.AppendMenu(MF_STRING|MF_DISABLED, 0, GetString(IDS_STRING61328)); // "None"
		}

		olMenu.AppendMenu(MF_STRING|MF_POPUP|MF_ENABLED,(UINT)olRegnMenu.m_hMenu,GetString(IDS_STRING61333)); // "Available Registrations"
	}

	CPoint olPoint(LOWORD(lParam),HIWORD(lParam));
	ClientToScreen(&olPoint);
    olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y, this, NULL);
	
	// see HELP: "Building a Dynamic Menu for TrackPopupMenu" as to why I do the following
	if(ogBasicData.bmDisplayCheckins) olCicMenu.Detach();
	if(ogBasicData.bmDisplayGates) olGateMenu.Detach();
	if(ogBasicData.bmDisplayRegistrations) olRegnMenu.Detach();
	if(ogBasicData.bmDisplayPositions) olPosiMenu.Detach();


	return 0L;
}



void StaffChart::OnMenuAssign()
{
	CAllocDlg olAllocDlg;
	STAFF_GROUPDATA *prlGroup = pomViewer->GetGroup(imGroupNo);
	//olAllocDlg.omPool = prlGroup->Text; //Commented Singapore
	olAllocDlg.omPool = pomViewer->GetPool(imGroupNo); //Added Singapore
	int ilNumLines = pomViewer->GetLineCount(imGroupNo);
	if(ilNumLines > 0)
	{
		for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
		{
			STAFF_LINEDATA *prlLine = pomViewer->GetLine(imGroupNo,ilLc);
			if (prlLine != NULL)
			{
				olAllocDlg.AddPoolJobUrno(prlLine->JobUrno);
			}
		}
		olAllocDlg.DoModal();
	}
	
//	CCSPtrArray <long> olUrnoList;
//
//	for (int ilLc = 0; ilLc < pomViewer->GetLineCount(imGroupNo); ilLc++)
//	{
//		STAFF_LINEDATA *prlLine = pomViewer->GetLine(imGroupNo,ilLc);
//		if (prlLine != NULL)
//		{
//			olUrnoList.Add(&prlLine->JobUrno);
//		}
//	}
//	
//	CAssignmentJobDialog dialog(this, &olUrnoList,
//		pomViewer->GetGroup(imGroupNo)->GroupId,
//		pomViewer->GetGroup(imGroupNo)->Text);
//	dialog.DoModal();

}

void StaffChart::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

void StaffChart::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}


/////////////////////////////////////////////////////////////////////////////
// StaffChart -- drag-and-drop functionalities
//
// In a StaffChart, the user will have opportunities to:
//	-- drop from PrePlanTable onto any places in StaffChart.
//		(If the user also press Ctrl, we will open the dialog and confirm before.)
//	-- drop from DutyBar from other pools for create job detach.
//		We will ask the user for the period of time and create one JobDetach and another
//		JobPool for the new pool for the specified period of time.
//	-- drop from FlightBar onto any places in StaffChart.
//		We will open a dialog and let user select employees for assign flight jobs.
//
LONG StaffChart::OnDragOver(UINT wParam, LONG lParam)
{

	
	
	CCSDragDropCtrl *pomDragDropCtrl;
	LONG llRc = 0L;
	bool blAutoScroll = false;

	// First we have to find the correct drag-and-drop control object
	if ((CWnd *)lParam == this)
	{
		pomDragDropCtrl = &m_ChartWindowDragDrop;
	}
	else if ((CWnd *)lParam == &omButton)
	{
		pomDragDropCtrl = &m_ChartButtonDragDrop;
	}
	else if ((CWnd *)lParam == pomCountText)
	{
		pomDragDropCtrl = &m_CountTextDragDrop;
	}
	else if ((CWnd *)lParam == pomTopScaleText)
	{
		pomDragDropCtrl = &m_TopScaleTextDragDrop;
	}
	else
	{
		llRc = -1L;
	}


	if(llRc == 0L)
	{
		blAutoScroll = true;
		llRc = -1L;
		// Perform a validation on dragged object
		JOBDATA *prlJob;
		switch (pomDragDropCtrl->GetDataClass())
		{
		case DIT_SHIFT:
			if (((CViewer *)pomViewer)->GetGroup() == "Pool")
			{
				llRc = 0L;	// drop from PrePlanTable onto any places in StaffChart
			}
			break;
		case DIT_DUTYBAR:
			if ((prlJob = ogJobData.GetJobByUrno(pomDragDropCtrl->GetDataDWord(0))) != NULL &&
				prlJob->Alid != pomViewer->GetGroup(imGroupNo)->GroupId)
			{
				llRc = 0L;	// drop from DutyBar from other pools for create job detach
			}
			break;
		case DIT_FLIGHTBAR:
			llRc = 0L;	// drop from FlightBar onto any places in StaffChart
			break;
		}
	}

	if(blAutoScroll)
	{
		AutoScroll(750);
	}
	else
	{
		AutoScroll(); // end scrolling
	}

	return llRc;
//----------------------------------------------------------------------
}

LONG StaffChart::OnDrop(UINT wParam, LONG lParam)
{
  
	
	CCSDragDropCtrl *pomDragDropCtrl;

	// First we have to find the correct drag-and-drop control object
	if ((CWnd *)lParam == this)
		pomDragDropCtrl = &m_ChartWindowDragDrop;
	else if ((CWnd *)lParam == &omButton)
		pomDragDropCtrl = &m_ChartButtonDragDrop;
	else if ((CWnd *)lParam == pomCountText)
		pomDragDropCtrl = &m_CountTextDragDrop;
	else if ((CWnd *)lParam == pomTopScaleText)
		pomDragDropCtrl = &m_TopScaleTextDragDrop;
	else
		return -1L;

//-DTT Jul.22-----------------------------------------------------------
	// Perform job creation on dragged object
	DROPEFFECT lpDropEffect = wParam;
	switch (pomDragDropCtrl->GetDataClass())
	{
	case DIT_SHIFT:	// drop from PrePlanTable
		return ProcessDropShifts(pomDragDropCtrl, lpDropEffect);
	case DIT_DUTYBAR:	// drop from DutyBar
		return ProcessDropDutyBar(pomDragDropCtrl, lpDropEffect);
	case DIT_FLIGHTBAR:	// drop from FlightBar
		return ProcessDropFlightBar(pomDragDropCtrl, lpDropEffect);
	}

    return -1L;
//----------------------------------------------------------------------
}

LONG StaffChart::ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	if (lpDropEffect == DROPEFFECT_COPY)	// holding the Control key?
	{
//-DTT Jul.22-----------------------------------------------------------
// No more routine ProcessPartTimeAssignment(), we use a new dialog in
// file "AssignFromPrePlanTableDlg.h" instead.
//
		CAssignmentFromPrePlanTableToPoolDialog dialog(this, popDragDropCtrl,
			pomViewer->GetGroup(imGroupNo)->GroupId,
			pomViewer->GetGroup(imGroupNo)->Text);
		dialog.DoModal();
//----------------------------------------------------------------------
	}
	else if (lpDropEffect == DROPEFFECT_MOVE)
	{
		CDWordArray olShiftUrnos;
		for (int ilIndex = 0; ilIndex < popDragDropCtrl->GetDataCount(); ilIndex++)
			olShiftUrnos.Add(popDragDropCtrl->GetDataDWord(ilIndex));
		CString olPool = pomViewer->GetGroup(imGroupNo)->GroupId;
		ogDataSet.CreateJobPool(this, olShiftUrnos, olPool);
	}
	return 0L;
}

//-DTT Jul.23-----------------------------------------------------------
LONG StaffChart::ProcessDropDutyBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	CTeamDelegationDlg olTeamDelegationDlg;

	CDWordArray olPoolJobUrnos;
	JOBDATA *prlPoolJob;
	EMPDATA *prlEmp;
	CString olEmp, olFunction, olPool, olTeam;
	CTime olStartTime = TIMENULL, olEndTime = TIMENULL;

	int ilNumPoolJobs = popDragDropCtrl->GetDataCount();
	for(int llPoolJob = 0; llPoolJob < ilNumPoolJobs; llPoolJob++)
	{
		prlPoolJob = ogJobData.GetJobByUrno(popDragDropCtrl->GetDataDWord(llPoolJob));
		if(prlPoolJob != NULL)
		{
			olPoolJobUrnos.Add(prlPoolJob->Urno);

			if((prlEmp = ogEmpData.GetEmpByPeno(prlPoolJob->Peno)) == NULL)
			{
				continue;
			}

			olEmp = ogEmpData.GetEmpName(prlEmp);
			olTeamDelegationDlg.omEmpList.Add(olEmp);

			olFunction = ogBasicData.GetFunctionForPoolJob(prlPoolJob);
			olTeamDelegationDlg.omFunctionList.Add(olFunction);

			if(olStartTime == TIMENULL || olStartTime > prlPoolJob->Acfr)
			{
				olStartTime = prlPoolJob->Acfr;
			}
			if(olEndTime == TIMENULL || olEndTime < prlPoolJob->Acto)
			{
				olEndTime = prlPoolJob->Acto;
			}

			// initialize to the pool and team of the dragged pool job(s)
			olPool = prlPoolJob->Alid;
			olTeam = ogBasicData.GetTeamForPoolJob(prlPoolJob);
		}
	}
	if(olPoolJobUrnos.GetSize() > 0)
	{
		// dropped onto a group button
		//olPool = pomViewer->GetGroup(imGroupNo)->GroupId;
		if(pomViewer->omGroupBy == GROUP_BY_POOL)
		{
			olPool = pomViewer->GetGroup(imGroupNo)->GroupId;
		}
		else if(pomViewer->omGroupBy == GROUP_BY_TEAM)
		{
			olTeam = pomViewer->GetGroup(imGroupNo)->GroupId;
		}

		olTeamDelegationDlg.omPool = olPool;
		olTeamDelegationDlg.omTeam = olTeam;
		olTeamDelegationDlg.omFrom = olStartTime;
		olTeamDelegationDlg.omTo = olEndTime;

		if(olTeamDelegationDlg.DoModal() == IDOK)
		{
			if(ogBasicData.bmDisplayTeams)
			{
				ogDataSet.CreateGroupDelegation(this,olTeamDelegationDlg.omTeam,olTeamDelegationDlg.omPool,
													olPoolJobUrnos,olTeamDelegationDlg.omFunctionList,
													olTeamDelegationDlg.omFrom,olTeamDelegationDlg.omTo);
			}
			else
			{
				ogDataSet.CreateJobDetachFromDuty(this,olPoolJobUrnos,olTeamDelegationDlg.omPool,olTeamDelegationDlg.omFrom,olTeamDelegationDlg.omTo);
			}
		}
	}
	return 0L;
//	CAssignmentJobDetachDialog dialog(this, popDragDropCtrl,
//		pomViewer->GetGroup(imGroupNo)->GroupId,
//		pomViewer->GetGroup(imGroupNo)->Text);
//	dialog.DoModal();
//	return 0L;
}

LONG StaffChart::ProcessDropFlightBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	long llFlightUrno = popDragDropCtrl->GetDataDWord(0);
	long llRotationUrno = popDragDropCtrl->GetDataDWord(1);
	FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(llFlightUrno);
	if (prlFlight != NULL)
	{
		CSelectPoolStaffForJobFlightDialog olDlg(this, pomViewer, imGroupNo, llFlightUrno);

		olDlg.omAloc = popDragDropCtrl->GetDataString(2);
		olDlg.omAlid = popDragDropCtrl->GetDataString(3);

		//olDlg.SetCaptionText(CString("Zuordnung zu Flug: ") + prlFlight->Fnum); 
		olDlg.SetCaptionText(GetString(IDS_STRING61324) + prlFlight->Fnum); 
		olDlg.DoModal();
	}
	return 0L;
}
//----------------------------------------------------------------------

// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void StaffChart::AutoScroll(UINT ipInitialScrollSpeed /*200*/)
{
	// if not already scrolling automatically...
	if(!bmScrolling)
	{
		// ... after a short pause (ipInitialScrollSpeed), start scrolling
		bmScrolling = true;
		imScrollSpeed = ipInitialScrollSpeed;
		SetTimer(1, (UINT) imScrollSpeed, NULL);
	}
}

// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void StaffChart::OnAutoScroll(void)
{
	bmScrolling = false;

	// if standard cursor then not currently dragging
	if(GetCursor() != AfxGetApp()->LoadStandardCursor(IDC_ARROW))
	{
		// check if the cursor is in the automatic scrolling region
		CPoint olPoint;
		::GetCursorPos(&olPoint);

		CRect olGanttRect;
		omGantt.GetWindowRect(&olGanttRect);

		if(olPoint.y <= olGanttRect.top && olPoint.y > (olGanttRect.top - 30))
		{
			// above the gantt so scroll up
			omGantt.SetTopIndex(omGantt.GetTopIndex()-1);
			bmScrolling = true;
		}
		else if(olPoint.y >= olGanttRect.bottom && olPoint.y < (olGanttRect.bottom + 30))
		{
			// below the gantt so scroll down
			omGantt.SetTopIndex(omGantt.GetTopIndex()+1);
			bmScrolling = true;
		}
	}

	if(bmScrolling)
	{
		if(imScrollSpeed != 50)
		{
			imScrollSpeed = 50;
			SetTimer(1, (UINT) 50, NULL);
		}
	}
	else
	{
		// no longer in the scrolling region or left mouse button released
		KillTimer(1);
	}
}

void StaffChart::OnTimer(UINT nIDEvent)
{
	if(nIDEvent == 1)
	{
		// called during drag&drop, does automatic scrolling when the cursor
		// is outside the main gantt chart
		OnAutoScroll();
		CFrameWnd::OnTimer(nIDEvent);
	}
}
