// StaffDV.cpp : implementation file
//  
// Modification History:
// 19-Sep-96	Damkerng	Change many places since our database is changed.
//							In this module, the old implementation will use the field
//							PRID to keep text for a special job and an illness job.
//							Now, these has been moved to the field TEXT.
//							Check these places out by searching for "Id 19-Sep-96"

#include <stdafx.h>
#include <CCSGlobl.h>
#include <resource.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CCSDragDropCtrl.h>
#include <CedaJobData.h>
#include <CedaJtyData.h>
#include <CedaEmpData.h>
#include <CedaEquData.h>
#include <CedaFlightData.h>
#include <CedaShiftData.h>
#include <gantt.h>
#include <gbar.h>
#include <CedaDemandData.h>
#include <conflict.h>
#include <ccsddx.h>
#include <cviewer.h>
#include <StaffDetailWindow.h>
#include <StaffViewer.h>
#include <StaffDetailViewer.h>
#include <AllocData.h>
#include <CedaJodData.h>
#include <DataSet.h>
#include <BasicData.h>
#include <CedaDraData.h>
#include <CedaDelData.h>
#include <CedaOdaData.h>
#include <FieldConfigDlg.h>

#ifdef _DEBUG
#undef THIS_FILE
#define new DEBUG_NEW
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))

// Local function prototype
static void StaffDetailCf(void *popInstance, int ipDDXType,
	void *vpDataPointer, CString &ropInstanceName);


//// General purpose
static BOOL GetMaxEndTime(CCSPtrArray<DEMANDDATA> &ropDemands, CTime &ropMaxEndTime)
{
	int ilSize = ropDemands.GetSize();
	if (ilSize == 0)
		return FALSE;

	CTime olMaxTime = ropDemands[0].Deen;
	CTime olTime;
	for (int ilIndex = 1; ilIndex < ilSize; ilIndex++)
	{
		olTime = ropDemands[ilIndex].Deen;
		if (olMaxTime < olTime)
			olMaxTime = olTime;
	}
	ropMaxEndTime = olMaxTime;
	return TRUE;
}

static BOOL GetMinStartTime(CCSPtrArray<DEMANDDATA> &ropDemands, CTime &ropMinStartTime)
{
	int ilSize = ropDemands.GetSize();
	if (ilSize == 0)
		return FALSE;

	CTime olMinTime = ropDemands[0].Debe;
	CTime olTime;
	for (int ilIndex = 1; ilIndex < ilSize; ilIndex++)
	{
		olTime = ropDemands[ilIndex].Debe;
		if (olMinTime > olTime)
			olMinTime = olTime;
	}
	ropMinStartTime = olMinTime; 
	return TRUE;
}

StaffDetailViewer::StaffDetailViewer()
{
	pomAttachWnd = NULL;
	bmIsFirstTime = TRUE;

	omFastLinkBrush.CreateSolidBrush(OLIVE);
	omTempAbsenceBrush.CreateSolidBrush(SILVER);

	ogCCSDdx.Register((void *)this,JOB_CHANGE,CString("StaffDETAIL"), CString("Job changed"),StaffDetailCf);
	ogCCSDdx.Register((void *)this,JOB_DELETE,CString("StaffDETAIL"), CString("Job changed"),StaffDetailCf);
	ogCCSDdx.Register((void *)this,JOB_NEW,CString("StaffDETAIL"), CString("Fob changed"),StaffDetailCf);
	ogCCSDdx.Register(this, REDISPLAY_ALL,CString("StaffDETAIL"), CString("Global Update"), StaffDetailCf);
	ogCCSDdx.Register(this, UPDATE_CONFLICT_SETUP,CString("StaffDETAIL"), CString("Global Update"), StaffDetailCf);
}

StaffDetailViewer::~StaffDetailViewer()
{
	RemoveAll();
	ogCCSDdx.UnRegister(this, NOTUSED);
}

void StaffDetailViewer::RemoveAll()
{
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		StaffDetailGantt *polGantt = &((StaffDetailWindow *)pomAttachWnd)->omGantt;
		if (::IsWindow(polGantt->GetSafeHwnd()))
		{
			polGantt->ResetContent();
		}
	}

	STAFFDETAIL_LINEDATA *prlLine;
	for(int i = 0; i < omStaffLines.GetSize(); i++)
	{
		prlLine = &omStaffLines[i];
		prlLine->Bars.DeleteAll();
		prlLine->BkBars.DeleteAll();
		prlLine->TempAbsenceBkBars.DeleteAll();
		prlLine->FastLinkBkBars.DeleteAll();
	}
	omStaffLines.DeleteAll();

	omTempAbsences.RemoveAll();
	omJobs.RemoveAll();

}

void StaffDetailViewer::Attach(CWnd *popAttachWnd)
{
	pomAttachWnd = popAttachWnd;
}

static int CompareJobStartTime(const JOBDATA **e1, const JOBDATA **e2)
{
	return (int)((**e1).Acfr.GetTime() - (**e2).Acfr.GetTime());
}

void StaffDetailViewer::Init(long lpJobPrimaryKey,BOOL bpIsFirstTime)
{
	if (bpIsFirstTime)
	{
		JOBDATA *prlJob = ogJobData.GetJobByUrno(lpJobPrimaryKey);
		lmJobPrimaryKey = lpJobPrimaryKey;
		bmIsFirstTime = FALSE;
		if (prlJob != NULL)
		{
			strcpy(lmPknoKey,prlJob->Peno);
		}
		else
		{
			strcpy(lmPknoKey,"");
		}
	}
	else
	{
		JOBDATA *prlJob = ogJobData.GetJobByUrno(lpJobPrimaryKey);
		if (prlJob != NULL)
		{
			if (strcmp(lmPknoKey,prlJob->Peno) != 0)
			{
				return;
			}
		}
	}


	RemoveAll();
	
	JOBDATA olJob;
	JOBDATA *polJob = ogJobData.GetJobByUrno(lmJobPrimaryKey);
	if (polJob != NULL)
	{
		olJob = *polJob;
	}
	else
	{
		return;
	}

	EMPDATA *polEmp = ogEmpData.GetEmpByPeno(olJob.Peno);
	if (polEmp != NULL)
	{
		omEmp = *polEmp;
	}
	else
	{
		return;
	}

	SHIFTDATA *polShift = ogShiftData.GetShiftByUrno(olJob.Shur);
	if (polShift != NULL)
	{
		omShift = *polShift;
	}
	else
	{
		return;
	}

	// get temporary absences so that when the emp is absent the background will
	// indicate the absence
	//ogJobData.GetJobsByPkno(omTempAbsences,olJob.Peno,JOBTEMPABSENCE);

	char clBuf[255];
	STAFFDETAIL_BARDATA *prlBar = NULL;
	STAFFDETAIL_LINEDATA *prlLine = NULL;

	CCSPtrArray<JOBDATA> olPoolJobs;
	ogJobData.GetJobsByShur(olPoolJobs,olJob.Shur,TRUE);
	olPoolJobs.Sort(CompareJobStartTime);
	for (int i = 0; i < olPoolJobs.GetSize(); i++)
	{
		JOBDATA *prlPJ = &olPoolJobs[i];

		prlLine = new STAFFDETAIL_LINEDATA;
		prlLine->Text.Format("%s - %s  %s  %s", prlPJ->Acfr.Format("%H%M/%d"), prlPJ->Acto.Format("%H%M/%d"), prlPJ->Alid, ogBasicData.GetFunctionForPoolJob(prlPJ));
		prlLine->MaxOverlapLevel = 0;
		prlLine->StartTime = prlPJ->Acfr;
		prlLine->EndTime = prlPJ->Acto;
		prlLine->PoolJobUrno = prlPJ->Urno;
		prlLine->OfflineBar = (!bgOnline && (ogJobData.JobChangedOffline(prlPJ) || ogJodData.JodChangedOffline(prlPJ->Urno)));


		CCSPtrArray<JOBDATA> olJobs;
		ogJobData.GetJobsByPoolJobAndType(olJobs, prlPJ->Urno, JOBTEMPABSENCE);
		for(int x = 0; x < olJobs.GetSize(); x++)
		{
			MakeTempAbsenceBkBar(prlLine, &olJobs[x]);
		}
		
		ogJobData.GetJobsByPoolJob(olJobs, prlPJ->Urno, true);
		olJobs.Sort(CompareJobStartTime);
		omJobs.Append(olJobs);
			
		int ilLine = omStaffLines.Add(prlLine);
		VERIFY(ilLine == i);

		for (int j = 0; j < olJobs.GetSize(); j++)
		{
			JOBDATA *prlJob = &olJobs[j];

			// Pichate CreateBar
			prlBar = new STAFFDETAIL_BARDATA;
			prlBar->StaffUrno = ogBasicData.GetFlightUrnoForJob(prlJob);
			prlBar->Urno = prlJob->Urno;

			CString olAcfr = prlJob->Acfr.Format("%H%M/%d");
			CString olActo = prlJob->Acto.Format("%H%M/%d");
			if(!strcmp(prlJob->Jtco, JOBBREAK))
			{
				prlBar->Text  = "";
				sprintf(clBuf, "%s - %s %s",olAcfr,olActo,	ogDataSet.JobBarText(prlJob));
				prlBar->IsPause = true;
				prlBar->PauseColour = ogColors[ogConflicts.GetJobConflictColor(prlJob)];
				prlBar->MarkerBrush = ogBrushs[21];
			}
			else
			{
				CString olText = "";
				if(strcmp(prlJob->Jtco,JOBDELEGATEDFLIGHT) == 0)
					olText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_DFJ, "%s%s", "", ogDataSet.GetFlightJobBarText(prlJob));
				else if(strcmp(prlJob->Jtco,JOBRESTRICTEDFLIGHT) == 0)
					olText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_RFJ, "%s%s", "", ogDataSet.GetFlightJobBarText(prlJob));

				if(olText.IsEmpty())
					olText = ogDataSet.JobBarText(prlJob);

				prlBar->Text  = olText;
				//prlBar->Text  = ogDataSet.JobBarText(prlJob);
				sprintf(clBuf, "%s - %s %s",olAcfr,olActo,	prlBar->Text);
				prlBar->MarkerBrush = ogBrushs[ogConflicts.GetJobConflictColor(prlJob)];
			}

			prlBar->FrameType = FRAMERECT;
			if(strcmp(prlJob->Jtco,JOBDELEGATEDFLIGHT) == 0)
			{
				prlBar->FrameColor = ORANGE;
			}
			else
			{
				prlBar->FrameColor = (!strcmp(prlJob->Jtco, JOBBREAK) && *prlJob->Ignr == '1') ? WHITE : BLACK;
			}

			prlBar->StatusText = clBuf;
			prlBar->StartTime = prlJob->Acfr;
			prlBar->EndTime = prlJob->Acto;
			prlBar->MarkerType = (prlJob->Stat[0] == 'P')? MARKLEFT: MARKFULL;
			//prlBar->MarkerBrush = ogBrushs[olJob.ColorIndex];
			prlBar->OverlapLevel = 0;	// zero for the top-most level, each level lowered by 4 pixels
			prlBar->OfflineBar = !bgOnline && (ogJobData.JobChangedOffline(prlJob) || ogJodData.JodChangedOffline(prlJob->Urno));
			prlBar->DifferentFunction = ogBasicData.JobAndDemHaveDiffFuncs(prlJob);

			if(prlJob->Stat[0] == 'P' && prlJob->Infm)
			{
				prlBar->Infm = true;
			}
			else
			{
				prlBar->Infm = false;
			}


			CreateBar(i,prlBar,TRUE);
		}

		if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
		{
			StaffDetailGantt *polGantt = &((StaffDetailWindow *)pomAttachWnd)->omGantt;
			if (::IsWindow(polGantt->GetSafeHwnd()))
			{
				polGantt->AddString("");
				polGantt->SetItemHeight(i, polGantt->GetLineHeight(i));

			}
		}
	}

	if(ogBasicData.bmDisplayEquipment)
	{
		CCSPtrArray <JOBDATA> olEquipmentFastLinkJobs;
		ogJobData.GetEquipmentFastLinkJobsByPoolJob(olEquipmentFastLinkJobs,olPoolJobs);
		olEquipmentFastLinkJobs.Sort(CompareJobStartTime);

		bool blLineAdded;
		STAFFDETAIL_LINEDATA *prlFreeLine;
		int ilNumFastLinks = olEquipmentFastLinkJobs.GetSize();
		for(int ilFL = 0; ilFL < ilNumFastLinks; ilFL++)
		{
			JOBDATA *prlFastLinkJob = &olEquipmentFastLinkJobs[ilFL];
			blLineAdded = false;
			if((prlFreeLine = GetFreeLineForTime(prlFastLinkJob->Acfr,prlFastLinkJob->Acto)) == NULL)
			{
				prlFreeLine = new STAFFDETAIL_LINEDATA;
				prlFreeLine->Text.Empty();
				prlFreeLine->MaxOverlapLevel = 0;
				omStaffLines.Add(prlFreeLine);
				blLineAdded = true;
			}
			if(prlFreeLine != NULL)
			{
				MakeFastLinkBkBar(prlFreeLine, prlFastLinkJob);
			}
			if(blLineAdded)
			{
				StaffDetailGantt *polGantt = &((StaffDetailWindow *)pomAttachWnd)->omGantt;
				if (::IsWindow(polGantt->GetSafeHwnd()))
				{
					polGantt->AddString("");
					polGantt->SetItemHeight(i, polGantt->GetLineHeight(i));
				}
			}
		}
	}

	// fill up with blank lines if necessary	
	for (;i < 8; i++)
	{
		prlLine = new STAFFDETAIL_LINEDATA;
		prlLine->Text.Empty();
		prlLine->MaxOverlapLevel = 0;
		
		omStaffLines.Add(prlLine);
		if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
		{
			StaffDetailGantt *polGantt = &((StaffDetailWindow *)pomAttachWnd)->omGantt;
			if (::IsWindow(polGantt->GetSafeHwnd()))
			{
				polGantt->AddString("");
				polGantt->SetItemHeight(i, polGantt->GetLineHeight(i));
			}
		}
	}

}


STAFFDETAIL_LINEDATA *StaffDetailViewer::GetFreeLineForTime(CTime opBarStart, CTime opBarEnd)
{
	STAFFDETAIL_LINEDATA *prlLine = NULL;
	int ilNumLines = omStaffLines.GetSize();
	for(int ilLine = 0; prlLine == NULL && ilLine < ilNumLines; ilLine++)
	{
		STAFFDETAIL_LINEDATA *prlTmpLine = &omStaffLines[ilLine];
		bool blNoOverlaps = true;
		int ilNumBars = prlTmpLine->FastLinkBkBars.GetSize();
		for(int ilBar = 0; blNoOverlaps && ilBar < ilNumBars; ilBar++)
		{
			if(IsOverlapped(prlTmpLine->FastLinkBkBars[ilBar].StartTime, prlTmpLine->FastLinkBkBars[ilBar].EndTime, opBarStart, opBarEnd))
			{
				blNoOverlaps = false;
			}
		}
		if(blNoOverlaps)
		{
			// free line found
			prlLine = prlTmpLine;
		}
	}

	return prlLine;
}

int StaffDetailViewer::GetLineCount()
{
	return omStaffLines.GetSize();
}

STAFFDETAIL_LINEDATA *StaffDetailViewer::GetLine(int ipLineno)
{
	STAFFDETAIL_LINEDATA *prlLine = NULL;
	if(ipLineno >= 0 && ipLineno < omStaffLines.GetSize())
	{
		prlLine = &omStaffLines[ipLineno];
	}
	return prlLine;
}

CString StaffDetailViewer::GetLineText(int ipLineno)
{
	static char clBuf[255];
	sprintf(clBuf, "[%d]", ipLineno);
	return CString(clBuf);
}

int StaffDetailViewer::GetOverlappingBars(STAFFDETAIL_LINEDATA *prpLine, CTime opStartTime, CTime opEndTime, CCSPtrArray <STAFFDETAIL_BARDATA> &ropOverlappingBars)
{
	if(prpLine != NULL)
	{
		int ilNumBars = prpLine->Bars.GetSize();
		for(int ilBar = 0; ilBar < ilNumBars; ilBar++)
		{
			STAFFDETAIL_BARDATA *prlBar = &prpLine->Bars[ilBar];
			if(IsReallyOverlapped(opStartTime, opEndTime, prlBar->StartTime, prlBar->EndTime))
			{
				ropOverlappingBars.Add(prlBar);
			}
		}
	}

	return ropOverlappingBars.GetSize();
}

// Foreground Bar
int StaffDetailViewer::CreateBar(int ipLineno, STAFFDETAIL_BARDATA *prpNewBar, BOOL bpFrontBar)
{
	int ilBarno = -1;
    STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		prpNewBar->OverlapLevel = 0;

		CCSPtrArray <STAFFDETAIL_BARDATA> olOverlappingBars;
		int ilNumOverlappingBars = GetOverlappingBars(prlLine, prpNewBar->StartTime, prpNewBar->EndTime, olOverlappingBars);
		for(int ilBar = 0; ilBar < ilNumOverlappingBars; ilBar++)
		{
			STAFFDETAIL_BARDATA *prlBar = &olOverlappingBars[ilBar];
			if(prpNewBar->OverlapLevel == prlBar->OverlapLevel)
			{
				prpNewBar->OverlapLevel += 4;
			}

			if(prpNewBar->OverlapLevel > prlLine->MaxOverlapLevel)
			{
				prlLine->MaxOverlapLevel = prpNewBar->OverlapLevel;
			}
		}
		ilBarno = prlLine->Bars.GetSize();
		prlLine->Bars.Add(prpNewBar);
	}
	return ilBarno;
}

void StaffDetailViewer::DeleteBar(int ipLineno, int ipBarno)
{
    STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		prlLine->Bars.DeleteAt(ipBarno);
	}
}

int StaffDetailViewer::GetBarCount(int ipLineno)
{
	int ilBarCount = 0;
	STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		ilBarCount = prlLine->Bars.GetSize();
	}
	return ilBarCount;
}

STAFFDETAIL_BARDATA *StaffDetailViewer::GetBar(int ipLineno, int ipBarno)
{
	STAFFDETAIL_BARDATA *prlBar = NULL;
	STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		if(ipBarno >= 0 && ipBarno < prlLine->Bars.GetSize())
		{
			prlBar = &prlLine->Bars[ipBarno];
		}
	}

	return prlBar;
}

int StaffDetailViewer::GetBarnoFromTime(int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2)
{
	STAFFDETAIL_LINEDATA *prlLine = &omStaffLines[ipLineno];
	int ilBarCount = prlLine->Bars.GetSize();
	for(int i = (ilBarCount-1); i >= 0; i--)
	{
		STAFFDETAIL_BARDATA *prlBar = &prlLine->Bars[i];
        if (prlBar != NULL && opTime1 <= prlBar->EndTime && prlBar->StartTime <= opTime2 &&
            ipOverlapLevel1 <= prlBar->OverlapLevel && prlBar->OverlapLevel <= ipOverlapLevel2)
            return i;
	}

	return i;
}

int StaffDetailViewer::GetBkBarCount(int ipLineno)
{
	int ilBarCount = 0;
	STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		ilBarCount = prlLine->BkBars.GetSize();
	}
	return ilBarCount;
}

int StaffDetailViewer::GetFastLinkBkBarCount(int ipLineno)
{
	int ilBarCount = 0;
	STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		ilBarCount = prlLine->FastLinkBkBars.GetSize();
	}
	return ilBarCount;
}

int StaffDetailViewer::GetTempAbsenceBkBarCount(int ipLineno)
{
	int ilBarCount = 0;
	STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		ilBarCount = prlLine->TempAbsenceBkBars.GetSize();
	}
	return ilBarCount;
}

STAFFDETAIL_BARDATA *StaffDetailViewer::GetBkBar(int ipLineno, int ipBkBarno)
{
	STAFFDETAIL_BARDATA *prlBkBar = NULL;

	STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		if(ipBkBarno >= 0 && ipBkBarno < prlLine->BkBars.GetSize())
		{
			prlBkBar = &prlLine->BkBars[ipBkBarno];
		}
	}

	return prlBkBar;
}

STAFFDETAIL_FASTLINKBKBARDATA *StaffDetailViewer::GetFastLinkBkBar(int ipLineno, int ipFastLinkBkBarno)
{
	STAFFDETAIL_FASTLINKBKBARDATA *prlFastLinkBkBar = NULL;

	STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		if(ipFastLinkBkBarno >= 0 && ipFastLinkBkBarno < prlLine->FastLinkBkBars.GetSize())
		{
			prlFastLinkBkBar = &prlLine->FastLinkBkBars[ipFastLinkBkBarno];
		}
	}

	return prlFastLinkBkBar;
}

STAFFDETAIL_TEMPABSENCEBKBARDATA *StaffDetailViewer::GetTempAbsenceBkBar(int ipLineno, int ipTempAbsenceBkBarno)
{
	STAFFDETAIL_TEMPABSENCEBKBARDATA *prlTempAbsenceBkBar = NULL;

	STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		if(ipTempAbsenceBkBarno >= 0 && ipTempAbsenceBkBarno < prlLine->TempAbsenceBkBars.GetSize())
		{
			prlTempAbsenceBkBar = &prlLine->TempAbsenceBkBars[ipTempAbsenceBkBarno];
		}
	}

	return prlTempAbsenceBkBar;
}

int StaffDetailViewer::GetBkBarnoFromTime(int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2)
{
	int ilBkBarno = -1;
	STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		int ilBkBarCount = prlLine->BkBars.GetSize();
		for(int i = (ilBkBarCount-1); i >= 0; i--)
		{
			if(IsOverlapped(prlLine->BkBars[i].StartTime, prlLine->BkBars[i].EndTime, opTime1, opTime2))
			{
				ilBkBarno = i;
				break;
			}
		}
	}

	return ilBkBarno;
}

int StaffDetailViewer::GetFastLinkBkBarnoFromTime(int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2)
{
	int ilFastLinkBkBarno = -1;
	STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		int ilFastLinkBkBarCount = prlLine->FastLinkBkBars.GetSize();
		for(int i = (ilFastLinkBkBarCount-1); i >= 0; i--)
		{
			if(IsOverlapped(prlLine->FastLinkBkBars[i].StartTime, prlLine->FastLinkBkBars[i].EndTime, opTime1, opTime2))
			{
				ilFastLinkBkBarno = i;
				break;
			}
		}
	}
	return ilFastLinkBkBarno;
}

int StaffDetailViewer::GetTempAbsenceBkBarnoFromTime(int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2)
{
	int ilTempAbsenceBkBarno = -1;
	STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		int ilTempAbsenceBkBarCount = prlLine->TempAbsenceBkBars.GetSize();
		for(int i = (ilTempAbsenceBkBarCount-1); i >= 0; i--)
		{
			if(IsOverlapped(prlLine->TempAbsenceBkBars[i].StartTime, prlLine->TempAbsenceBkBars[i].EndTime, opTime1, opTime2))
			{
				ilTempAbsenceBkBarno = i;
				break;
			}
		}
	}
	return ilTempAbsenceBkBarno;
}

CString StaffDetailViewer::GetTempAbsenceTextFromTime(CTime opTime1, CTime opTime2)
{
	CString olText = "";

	int ilCnt = omTempAbsences.GetSize();
	for(int i = 0; i < ilCnt; i++)
	{
		JOBDATA *prlTA = &omTempAbsences[i];
		if(IsOverlapped(prlTA->Acfr, prlTA->Acto, opTime1, opTime2))
		{
			CString olCode;
			DRADATA *prlDra = NULL;
			DELDATA *prlDel = NULL;
			if((prlDra = ogDraData.GetDraByUrno(prlTA->Shur)) != NULL)
			{
				olCode = prlDra->Sdac;
			}
			else if((prlDel = ogDelData.GetDelByUrno(prlTA->Udel)) != NULL)
			{
				olCode = prlDel->Sdac;
			}

			ODADATA *prlOda = ogOdaData.GetOdaByType(olCode);
			if(prlOda != NULL)
			{
				olText = prlOda->Sdan;
			}
		}
	}

	return olText;
}
	
static void StaffDetailCf(void *popInstance, int ipDDXType,
	void *vpDataPointer, CString &ropInstanceName)
{
    StaffDetailViewer *polViewer = (StaffDetailViewer *)popInstance;

	if(ipDDXType == JOB_CHANGE || ipDDXType == JOB_NEW || ipDDXType == JOB_DELETE)
	{
		polViewer->ProcessJobChange((JOBDATA *)vpDataPointer);
	}
	else if(ipDDXType == REDISPLAY_ALL || ipDDXType == UPDATE_CONFLICT_SETUP)
	{
		polViewer->ProcessRefresh();
	}
}

void StaffDetailViewer::ProcessRefresh(void)
{
	if (!::IsWindow(pomAttachWnd->GetSafeHwnd()))
		return;

	Init(lmJobPrimaryKey,FALSE);
}


void StaffDetailViewer::ProcessStaffChange(STAFFDATA *prpStaff)
{
	if (!::IsWindow(pomAttachWnd->GetSafeHwnd()))
		return;
}

void StaffDetailViewer::ProcessJobChange(JOBDATA *prpJob)
{
	if (!::IsWindow(pomAttachWnd->GetSafeHwnd()))
		return;

	if(prpJob != NULL)
	{
		if(!strcmp(prpJob->Jtco,JOBPOOL) || !strcmp(prpJob->Jtco,JOBTEMPABSENCE))
		{
			prpJob = ogJobData.GetJobByUrno(prpJob->Jour);
		}
		else
		{
			Init(prpJob->Urno,FALSE);
		}
	}
}

void StaffDetailViewer::GetOverlappedBarsFromTime(int ipLineno, CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList)
{
    STAFFDETAIL_LINEDATA *prlLine = GetLine(ipLineno);
	if(prlLine != NULL)
	{
		int ilBarCount = prlLine->Bars.GetSize();

		// Prepare the empty array of the involvement flag for each bar
		CCSPtrArray <BOOL> IsInvolvedBar;
		for (int i = 0; i < ilBarCount; i++)
			IsInvolvedBar.NewAt(i, FALSE);

		// We will loop again and again until there is no more bar in this overlapping found.
		// The overlapping group is at first determine by the given "opTime1" and "opTime2".
		// But if the bar we find is exceed this period of time, we will extend searching
		// to both left side and right side in the next round.
		//
		int nBarsFoundThisRound = -1;
		while (nBarsFoundThisRound != 0)
		{
			nBarsFoundThisRound = 0;
			for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
			{
				STAFFDETAIL_BARDATA *prlBar = &prlLine->Bars[ilBarno];
				CTime olStartTime = prlBar->StartTime;
				CTime olEndTime = prlBar->EndTime;
				BOOL blIsOverlapped = IsReallyOverlapped(olStartTime, olEndTime, opTime1, opTime2);
				BOOL blIsWithIn = IsWithIn(olStartTime, olEndTime, opTime1, opTime2);
					// this IsWithIn() fix the real thin bar bug
				BOOL blIsInvolved = (blIsOverlapped || blIsWithIn);

				if (!IsInvolvedBar[ilBarno] && blIsInvolved)	// some more new involved bars?
				{
					IsInvolvedBar[ilBarno] = TRUE;
					nBarsFoundThisRound++;
					opTime1 = min(opTime1, prlBar->StartTime);
					opTime2 = max(opTime2, prlBar->EndTime);
				}
			}
		}

		// Create the list of involved bars, then store them to "ropBarnoList"
		ropBarnoList.RemoveAll();
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			if (IsInvolvedBar[ilBarno])
			{
				ropBarnoList.Add(ilBarno);
			}
		}
		IsInvolvedBar.DeleteAll();
	}
}


STAFFDETAIL_FASTLINKBKBARDATA* StaffDetailViewer::MakeFastLinkBkBar(STAFFDETAIL_LINEDATA *prpLine, JOBDATA *prpJob)
{
	STAFFDETAIL_FASTLINKBKBARDATA *prlBkBar = NULL;

	if(prpLine != NULL && prpJob != NULL)
	{
		EQUDATA *prlEqu = ogEquData.GetEquByUrno(prpJob->Uequ);
		JTYDATA *prlJty = ogJtyData.GetJtyByUrno(prpJob->Ujty);
		if(prlEqu != NULL && prlJty != NULL)
		{
			prlBkBar = new STAFFDETAIL_FASTLINKBKBARDATA;
			prlBkBar->Text.Format("%s",prlEqu->Enam);
			prlBkBar->TextColor = WHITE;
			prlBkBar->StatusText.Format("%s %s - %s %s",prlJty->Dscr,prpJob->Acfr.Format("%d/%H%M"),prpJob->Acto.Format("%d/%H%M"),prlEqu->Enam);
			prlBkBar->StartTime = prpJob->Acfr;
			prlBkBar->EndTime	= prpJob->Acto;
			prlBkBar->MarkerBrush = &omFastLinkBrush;
			prlBkBar->MarkerType = MARKFULL;
			prlBkBar->FrameType = FRAMERECT;
			prlBkBar->JobUrno = prpJob->Urno;
			prlBkBar->OfflineBar = !bgOnline && (ogJobData.JobChangedOffline(prpJob) || ogJodData.JodChangedOffline(prpJob->Urno));

			prpLine->FastLinkBkBars.Add(prlBkBar);
		}
	}

	return prlBkBar;
}


STAFFDETAIL_TEMPABSENCEBKBARDATA* StaffDetailViewer::MakeTempAbsenceBkBar(STAFFDETAIL_LINEDATA *prpLine, JOBDATA *prpJob)
{
	STAFFDETAIL_TEMPABSENCEBKBARDATA *prlBkBar = NULL;

	if(prpLine != NULL && prpJob != NULL)
	{
		CString olCode, olText = "Absence Code Not Found";

		DRADATA *prlDra = NULL;
		DELDATA *prlDel = NULL;
		if((prlDra = ogDraData.GetDraByUrno(prpJob->Shur)) != NULL)
		{
			olCode = prlDra->Sdac;
		}
		else if((prlDel = ogDelData.GetDelByUrno(prpJob->Udel)) != NULL)
		{
			olCode = prlDel->Sdac;
		}

		ODADATA *prlOda = ogOdaData.GetOdaByType(olCode);
		if(prlOda != NULL)
		{
			olText = prlOda->Sdan;
		}

		prlBkBar = new STAFFDETAIL_TEMPABSENCEBKBARDATA;
		prlBkBar->Text = olText;
		prlBkBar->TextColor = BLACK;
		prlBkBar->StatusText.Format("%s - %s %s %s", prpJob->Acfr.Format("%H%M/%d"), prpJob->Acto.Format("%H%M/%d"), olCode, olText);
		prlBkBar->StartTime = prpJob->Acfr;
		prlBkBar->EndTime	= prpJob->Acto;
		prlBkBar->MarkerBrush = &omTempAbsenceBrush;
		prlBkBar->MarkerType = MARKFULL;
		prlBkBar->FrameType = FRAMERECT;
		prlBkBar->JobUrno = prpJob->Urno;

		prpLine->TempAbsenceBkBars.Add(prlBkBar);
	}

	return prlBkBar;
}
