// CedaOptData.cpp
#include <stdafx.h>
#include <ccsglobl.h> 
#include <resource.h>
#include <CCSCedaCom.h>
#include <OpssPm.h>
#include <ccsddx.h>
#include <CCSCedaData.h>
#include <cxbutton.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <PrePlanTable.h>
#include <CCITable.h>
#include <GateTable.h>
#include <CedaFlightData.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <CciDiagram.h>
#include <StartDate.h>
#include <ConflictConfigTable.h>
#include <ufis.h>
#include <CCSBcHandle.h>
#include <ConflictConfigTable.h>
#include <conflict.h>
#include <ConflictTable.h>
#include <mreqtable.h>
#include <ButtonList.h>
#include <CedaDprData.h>
#include <CedaOptData.h>

#define VORPLANUNG				1
#define EINSATZPLANUNG			2
#define CCI_PLANUNG				3

void  ProcessOptCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaOptData::CedaOptData()
{                  
    // Create an array of CEDARECINFO for OPTDATA
    BEGIN_CEDARECINFO(OPTDATA, OptDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_CHAR_TRIM(Appn,"APPN")
        FIELD_CHAR_TRIM(Ctyp,"CTYP")
        FIELD_CHAR_TRIM(Ckey,"CKEY")
        FIELD_CHAR_TRIM(Erst,"ERST")
        FIELD_DATE(Erda,"ERDA")
        FIELD_CHAR_TRIM(Para,"PARA")
        FIELD_DATE(Vafr,"VAFR")
        FIELD_DATE(Vato,"VATO")
        FIELD_DATE(Expr,"EXPR")
        FIELD_CHAR_TRIM(Peno,"PKNO")
        FIELD_CHAR_TRIM(Text,"TEXT")
        FIELD_CHAR_TRIM(Ftxt,"FTXT")
        FIELD_CHAR_TRIM(Lis1,"LIS1")
        FIELD_CHAR_TRIM(Lis2,"LIS2")
        FIELD_INT(Kind,"KIND")
        FIELD_INT(Mafm,"MAFM")
        FIELD_INT(Aufm,"AUFM")
        FIELD_INT(Mapo,"MAPO")
        FIELD_INT(Aupo,"AUPO")
        FIELD_INT(Mafl,"MAFL")
        FIELD_INT(Aufl,"AUFL")
        FIELD_INT(Macc,"MACC")
        FIELD_INT(Aucc,"AUCC")
        FIELD_INT(Mabr,"MABR")
        FIELD_INT(Aubr,"AUBR")
        FIELD_INT(Mazb,"MAZB")
        FIELD_INT(Auzb,"AUZB")
        FIELD_INT(Smrt,"SMRT")
        FIELD_INT(Saop,"SAOP")
        FIELD_INT(Cemf,"CEMF")
        FIELD_INT(Cfmf,"CFMF")
        FIELD_INT(Sdis,"SDIS")
        FIELD_INT(Shis,"SHIS")
        FIELD_INT(Sjmp,"SJMP")
        FIELD_INT(Swol,"SWOL")
        FIELD_INT(Svmf,"SVMF")
        FIELD_INT(Sbre,"SBRE")
        FIELD_INT(Squa,"SQUA")
        FIELD_INT(Sdtl,"SDTL")
        FIELD_INT(Stec,"STEC")
        FIELD_INT(Solp,"SOLP")
        FIELD_INT(Srmx,"SRMX")
        FIELD_INT(Spra,"SPRA")
        FIELD_INT(Snea,"SNEA")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(OptDataRecInfo)/sizeof(OptDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&OptDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"OPTCKI");
    pcmFieldList = 
					"URNO,APPN,CTYP,CKEY,ERST,ERDA,PARA,VAFR,VATO,EXPR,PKNO,TEXT,FTXT,LIS1,LIS2,KIND,"
				   "MAFM,AUFM,MAPO,AUPO,MAFL,AUFL,MACC,AUCC,MABR,AUBR,MAZB,AUZB,SMRT,SAOP,CEMF,"
				   "CFMF,SDIS,SHIS,SJMP,SWOL,SVMF,SBRE,SQUA,SDTL,STEC,SOLP,SRMX,SPRA,SNEA";
	ogCCSDdx.Register((void *)this,BC_OPT_CHANGE,CString("OPTDATA"), CString("Opt changed"),ProcessOptCf);
}
 
CedaOptData::~CedaOptData()
{
	TRACE("CedaOptData::~CedaOptData called\n");
	ogCCSDdx.UnRegister(this, NOTUSED);
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaOptData::ReadOptData()
{
    char pclSelection[512];
	CCSReturnCode ilRc = RCSuccess;
	int ilLc;

    // Initialize table names and field names
    strcpy(pcmTableName,"OPTCKI");

	CTime olTime = ogBasicData.GetTime();
	sprintf(pclSelection,"");
    if (CedaAction("RT",pclSelection) == RCFailure)
        return RCFailure;
    omData.DeleteAll();

	int ilCount = 0;
    for (ilLc = 0; ilRc == RCSuccess; ilLc++)
    {
		OPTDATA *prlOptData = new OPTDATA;

		if ((ilRc = GetBufferRecord(ilLc,prlOptData)) == RCSuccess)
		{
			if (AddOptInternal(prlOptData) == RCSuccess)
			{
				ilCount++;
			}
			else
			{
				delete prlOptData;
			}
		}
		else
		{
			delete prlOptData;
		}
	}

//    RC(RCSuccess);
    return RCSuccess;
}

CCSReturnCode CedaOptData::AddOpt(OPTDATA * prpOpt)
{
	OPTDATA *prlOpt = new OPTDATA;
	memcpy(prlOpt,prpOpt,sizeof(OPTDATA));

	prlOpt->IsChanged = DATA_NEW;
	AddOptInternal(prlOpt);
	ogCCSDdx.DataChanged((void *)this,OPT_CHANGE,(void *)prlOpt);
	SaveOpt(prlOpt);
//    RC(RCSuccess);
    return RCSuccess;
}

CCSReturnCode CedaOptData::ChangeOpt(OPTDATA *prpOpt)
{
	OPTDATA *prlOpt = new OPTDATA;
	memcpy(prlOpt,prpOpt,sizeof(OPTDATA));

	if (prlOpt->IsChanged == DATA_UNCHANGED)
	{
		prlOpt->IsChanged = DATA_CHANGED;
	}
	ogCCSDdx.DataChanged((void *)this,OPT_CHANGE,(void *)prlOpt);
	SaveOpt(prlOpt);

//    RC(RCSuccess);
    return RCSuccess;
}


CCSReturnCode CedaOptData::AddOptInternal(OPTDATA * prpOpt)
{

	//if ( strcmp(prpOpt->Ckey, "Real-World") == 0)
	if ( strcmp(prpOpt->Ckey, GetString(IDS_STRING61406)) == 0)
	{
		prpOpt->Urno = 0L;
		return RCFailure;
	}
	else
	{
		omData.Add(prpOpt);
		omUrnoMap.SetAt((void *)prpOpt->Urno,prpOpt);
	}
//	RC(RCSuccess);
	return RCSuccess;
}

CCSReturnCode CedaOptData::SaveOpt(OPTDATA * prpOpt)
{

	CCSReturnCode olRc = RCSuccess;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];


	if ((prpOpt->IsChanged == DATA_UNCHANGED) || (! bgOnline))
	{
		return RCSuccess; // no change, nothing to do
	}

	switch(prpOpt->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(olListOfData,prpOpt);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpOpt->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpOpt->Urno);
		MakeCedaData(olListOfData,prpOpt);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpOpt->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpOpt->Urno);
		olRc = CedaAction("DRT",pclSelection);
		if (olRc == RCSuccess)
		{
			for (int ilLc = 0; ilLc < omData.GetSize(); ilLc++)
			{
				if (&omData[ilLc] == prpOpt)
				{
					omData.RemoveAt(ilLc);
					delete prpOpt;
					break;
				}
			}
		}
		break;
	}
	
//	RC(RCSuccess);
    return RCSuccess;
}


CCSReturnCode CedaOptData::CreateCkoRequest(OPTDATA * prpOpt)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	char clPrg;
	if (prpOpt->Kind == VORPLANUNG)
		clPrg = 'P';
	else
	if (prpOpt->Kind == EINSATZPLANUNG)
		clPrg = 'O';
	else
	if (prpOpt->Kind == CCI_PLANUNG)
		clPrg = 'C';

	CString olAlos;
	olAlos.Empty();
	char pclList[2048];
	char pclElo[2048];
	char pclAlo[2048];
	char pclTmp[68];
	char *pclToken;


	*pclElo = '\0';
	strcpy(pclList,prpOpt->Lis1);
	pclToken = strtok(pclList," ");
	while (pclToken != NULL)
	{
		sprintf(pclTmp,";ELO=%s",pclToken);
		pclToken = strtok(NULL," ");
		strcat(pclElo,pclTmp);
	}


	*pclAlo = '\0';
	strcpy(pclList,prpOpt->Lis2);
	pclToken = strtok(pclList," ");
	while (pclToken != NULL)
	{
		sprintf(pclTmp,";ALO=%s",pclToken);
		pclToken = strtok(NULL," ");
		strcat(pclAlo,pclTmp);
	}
	if (*pclAlo == ';')
		strcat(pclAlo,";");

	if (*pclElo == ';' && *pclAlo != ';')
		strcat(pclElo,";");

/*******************
	char pclParameter[4096];
	sprintf(pclParameter,"PRG=%c%s%sOFR=%s;OTL=%s;MRT=%d;AOP=%d;EMF=%d;FMF=%d;"
		"DIS=%d;HIS=%d;JMP=%d;WOL=%d;VMF=%d;BRE=%d;QUA=%d;DTL=%d;TEC=%d;"
		"OLP=%d;RMX=%d;PRA=%d;NEA=%d;WID=%s;"
		"BM0=%d;BM1=%d;BM2=%d;BM3=%d;BM4=%d;BM5=%d;"
		"BO0=%d;BO1=%d;BO2=%d;BO3=%d;BO4=%d;BO5=%d;"

			,clPrg,pclElo,pclAlo,prpOpt->Vafr.Format("%Y%m%d%H%M00"),
			prpOpt->Vato.Format("%Y%m%d%H%M00"),

			
			prpOpt->Smrt,prpOpt->Saop,
			prpOpt->Cemf,prpOpt->Cfmf,prpOpt->Sdis,prpOpt->Shis,prpOpt->Sjmp,
			prpOpt->Swol,prpOpt->Svmf,prpOpt->Sbre,prpOpt->Squa,prpOpt->Sdtl,
			prpOpt->Stec,prpOpt->Solp,prpOpt->Srmx,prpOpt->Spra,prpOpt->Snea,
			prpOpt->Ckey,
			prpOpt->Mafm,prpOpt->Mapo,prpOpt->Mafl,prpOpt->Macc,prpOpt->Mabr,prpOpt->Mazb,
			prpOpt->Aufm,prpOpt->Aupo,prpOpt->Aufl,prpOpt->Aucc,prpOpt->Aubr,prpOpt->Auzb
			);
			
**************************/
	char pclParameter[4096];
	sprintf(pclParameter,"PRG=%c%s%sDAT=%s;TMF=%s;TMT=%s;MRT=%d;AOP=%d;EMF=%d;FMF=%d;"
		"DIS=%d;HIS=%d;JMP=%d;WOL=%d;VMF=%d;BRE=%d;QUA=%d;DTL=%d;TEC=%d;"
		"OLP=%d;RMX=%d;PRA=%d;NEA=%d;WID=%s;"
		"BM0=%d;BM1=%d;BM2=%d;BM3=%d;BM4=%d;BM5=%d;"
		"BO0=%d;BO1=%d;BO2=%d;BO3=%d;BO4=%d;BO5=%d;"

			,clPrg,pclElo,pclAlo,
			prpOpt->Vafr.Format("%Y%m%d"),
			prpOpt->Vafr.Format("%H%M"),
			prpOpt->Vato.Format("%H%M"),

			
			prpOpt->Smrt,prpOpt->Saop,
			prpOpt->Cemf,prpOpt->Cfmf,prpOpt->Sdis,prpOpt->Shis,prpOpt->Sjmp,
			prpOpt->Swol,prpOpt->Svmf,prpOpt->Sbre,prpOpt->Squa,prpOpt->Sdtl,
			prpOpt->Stec,prpOpt->Solp,prpOpt->Srmx,prpOpt->Spra,prpOpt->Snea,
			prpOpt->Ckey,
			prpOpt->Mafm,prpOpt->Mapo,prpOpt->Mafl,prpOpt->Macc,prpOpt->Mabr,prpOpt->Mazb,
			prpOpt->Aufm,prpOpt->Aupo,prpOpt->Aufl,prpOpt->Aucc,prpOpt->Aubr,prpOpt->Auzb
			);
			

// update Cfgl data in table "SHFCKI"
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[6000]; // SHFCKI ~ 140 byte
	
	char pclSelection[64];
    sprintf(CfgData, "%s,%s,%s,%s,%s",
		prpOpt->Ctyp,
		prpOpt->Ckey,
		prpOpt->Vafr.Format("%Y%m%d%H%M%S"),
		prpOpt->Vato.Format("%Y%m%d%H%M%S"),
		pclParameter);
	sprintf(pclSelection,"'%s'",prpOpt->Ckey);
	CCSReturnCode olRc = CedaAction("CKO", "CFGCKI", CfgFields, pclSelection, "", CfgData);
//	CCSReturnCode olRc;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	if (olRc == RCSuccess )
	{
		if (strncmp("Start Optimierung",CfgData,strlen(CfgData)) != 0)
		{
			//MessageBox(NULL,CfgData,"Optimiereraufruf",MB_OK);
			MessageBox(NULL,CfgData,GetString(IDS_STRING61228),MB_OK);
		}
	}
	else
	{
		//MessageBox(NULL,"Keine Verbindung zu Optimierer m�glich","Optimiereraufruf",MB_OK);
		MessageBox(NULL,GetString(IDS_STRING61227),GetString(IDS_STRING61228),MB_OK);
	}
//	ogCCSDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfgData);
	return olRc;
}


CCSReturnCode CedaOptData::SaveAllOpts(void)
{
	CCSReturnCode olRc = RCSuccess;
	int ilOptCount = omData.GetSize();
	for ( int ilLc = 0; ilLc < ilOptCount; ilLc++)
	{
		SaveOpt(&omData[ilLc]);
	}
// RC(olRc);
 return(olRc);
}

CCSReturnCode CedaOptData::DeleteOpt(long lpUrno)
{

	OPTDATA *prpOpt = GetOptByUrno(lpUrno);
	if (prpOpt != NULL)
	{
		prpOpt->IsChanged = DATA_DELETED;

		omUrnoMap.RemoveKey((void *)lpUrno);

		ogCCSDdx.DataChanged((void *)this,OPT_DELETE,(void *)prpOpt);
	//	ogJodData.DeleteJodsByWiid(prpOpt->Ckey);
	//	ogJobData.DeleteJobsByWiid(prpOpt->Ckey);
		SaveOpt(prpOpt);
	}
//    RC(RCSuccess);
    return RCSuccess;
}

CCSReturnCode CedaOptData::UseOpt(long lpUrno)
{
	OPTDATA *prpOpt = GetOptByUrno(lpUrno);
	if (prpOpt != NULL)
	{
		ogCCSDdx.DataChanged((void *)this,OPT_USE,(void *)prpOpt);
	}
//    RC(RCSuccess);
    return RCSuccess;
}

OPTDATA  *CedaOptData::GetOptByUrno(long lpUrno)
{
	OPTDATA  *prlOpt;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlOpt) == TRUE)
	{
		return prlOpt;
	}
	return NULL;
}


long  CedaOptData::GetUrnoById(char *pclWiid)
{
	int ilWhatifCount = omData.GetSize();
	for ( int i = 0; i < ilWhatifCount; i++)
	{
		if (strcmp(omData[i].Ckey,pclWiid) == 0)
		{
			return omData[i].Urno;
		}
	}
	return 0L;
}



void  ProcessOptCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_OPT_CHANGE :
		((CedaOptData *)popInstance)->ProcessOptBc(ipDDXType,
								vpDataPointer,ropInstanceName);
		break;
	}
}


void  CedaOptData::ProcessOptBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	if (ipDDXType == BC_OPT_CHANGE)
	{
		OPTDATA *prpOpt;
		struct BcStruct *prlOptData;

		prlOptData = (struct BcStruct *) vpDataPointer;
		ogBasicData.LogBroadcast(prlOptData);
		long llUrno;

		if (strstr(prlOptData->Selection,"WHERE") != NULL)
		{
			llUrno = GetUrnoFromSelection(prlOptData->Selection);
		}
		else
		{
			llUrno = atol(prlOptData->Selection);
		}
		if (omUrnoMap.Lookup((void *)llUrno,(void *& )prpOpt) == TRUE)
		{
			GetRecordFromItemList(prpOpt,
				prlOptData->Fields,prlOptData->Data);
		}
		else
		{
			prpOpt = new OPTDATA;
			GetRecordFromItemList(prpOpt,
				prlOptData->Fields,prlOptData->Data);

			AddOptInternal(prpOpt);
		}
		ogCCSDdx.DataChanged((void *)this,OPT_CHANGE,(void *)prpOpt);
	}
}

CString CedaOptData::GetTableName(void)
{
	return CString(pcmTableName);
}