// PrnCtrl.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>

#include <resource.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <PrePlanTeamPrintDlg.h>

#include <StaffTableViewer.h>
#include <FlightScheduleViewer.h>
#include <PrmTableViewer.h>

#include <PrintControl.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintCtrl

IMPLEMENT_DYNAMIC(CPrintCtrl, CPrintDialog)

CPrintCtrl::CPrintCtrl(BOOL bPrintSetupOnly, DWORD dwFlags, CWnd* pParentWnd) :
	CPrintDialog(bPrintSetupOnly, dwFlags, pParentWnd)
{
	TRACE("CPrintCtrl: Create Nowwwwwwww.........\n");
	m_nStartPageX = 100;
	m_nStartPageY = 100;
}

CPrintCtrl::~CPrintCtrl()
{
	TRACE("CPrintCtrl: Delete Nowwwwwwww.........\n");
}

BEGIN_MESSAGE_MAP(CPrintCtrl, CPrintDialog)
	//{{AFX_MSG_MAP(CPrintCtrl)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// MCU 07.07 new parameter pcpDutyDay 
void CPrintCtrl::PreplanPrint( CedaShiftData *shfData,char *pcpDutyDay)
{
	SHIFTDATA *polShift;
	int i, j;
	CStringArray	TeamArray;
	PrePlanTeamPrintDialog ppTeamDlg;
	BOOL	bMatch;
	
	for( i = 0; i < shfData->omData.GetSize(); i++ ) {
		polShift = &shfData->omData[i];
		if( TeamArray.GetSize() != 0 ) {
			bMatch = FALSE;
			for( j = TeamArray.GetSize(); j > 0; j-- ) {
				if( strcmp( TeamArray.GetAt(j - 1), polShift->Egrp ) == 0 ) {
					bMatch = TRUE;
					break;
				}
			}
			if( !bMatch ) {
				TeamArray.Add( polShift->Egrp );				
			}
		}else {
			TeamArray.Add( polShift->Egrp );
		}
	}
	ppTeamDlg.m_TeamArray = &TeamArray;
	if( ppTeamDlg.DoModal() == IDCANCEL ) return;

	HDC		hdc;
	DOCINFO	docInfo;

	if( CPrintDialog::DoModal() == IDCANCEL ) return;
	hdc = GetPrinterDC();

	memset(&docInfo, 0, sizeof(DOCINFO));
	docInfo.cbSize = sizeof( DOCINFO );
	docInfo.lpszDocName = "Preplan Table Report";	
	::StartDoc( hdc, &docInfo );
	
	for( i = 0; i < ppTeamDlg.m_TeamSelect.GetSize(); i++ ) {
		PrintPreplanTeam( hdc, ppTeamDlg.m_TeamSelect[i], shfData,pcpDutyDay );
	}
	
	::EndDoc( hdc );
	::DeleteDC( hdc );
}

void CPrintCtrl::PrintPreplanTeam( HDC hdc, CString strTeam, 
					CedaShiftData *shfData,char *pcpDutyDay )
{
	SHIFTDATA *polShift;
	EMPDATA *prlEmp;
//	FLIGHTDATA  *prlFlight;

	PREPLANDETAIL	ppDT;
	PREPLANHEADER	ppHD;

	::StartPage( hdc );

	strcpy( ppHD.Team, strTeam );
	strcpy( ppHD.Date, "" );
	PrintPreplanHeader( hdc, &ppHD );	

	int i, nLineCount;
	char TempStr[50];
	_strset(TempStr, '\0' );

	
	for( nLineCount = 0, i = 0; i < shfData->omData.GetSize(); i++ ) 
	{

		// MCU 07.07 print only shifts of current duty date
		polShift = &shfData->omData[i];
		if (strcmp(polShift->Sday,pcpDutyDay) != 0)
		{
			continue;
		}
		if( nLineCount > 62 )
		{
			::EndPage( hdc );
			::StartPage( hdc );
			strcpy( ppHD.Team, strTeam );
			strcpy( ppHD.Date, "" );
			PrintPreplanHeader( hdc, &ppHD );
			nLineCount = 0;
		}
		
		if( strcmp( strTeam, polShift->Egrp ) == 0 ) {
			prlEmp = ogEmpData.GetEmpByPeno(polShift->Peno);
			strcpy( ppDT.Rng, polShift->Efct );
			_strset(TempStr, '\0' );
			strcpy( TempStr, ogEmpData.GetEmpName(prlEmp) );
			strcpy( ppDT.Name, TempStr );
			strcpy( ppDT.Sft, polShift->Sfca);

			strcpy( ppDT.Anwesend1, polShift->Avfa.Format("%H%M"));
			strcpy( ppDT.Anwesend2, polShift->Avta.Format("%H%M"));

//		prlFlight = ogFlightData.GetFlightByUrno(polShift->Urno);

			strcpy( ppDT.Flua_CCI, "");
			strcpy( ppDT.Zugeorduetbis1, "");
			strcpy( ppDT.Zugeorduetbis2, "");
			strcpy( ppDT.Zugeorduetbis3, "");
			strcpy( ppDT.Zugeorduetbis4, "");
			strcpy( ppDT.Zugeorduetbis5, "");
			strcpy( ppDT.Zugeorduetbis6, "");
			strcpy( ppDT.Unterschrift, "");
			PrintPreplanDetail( hdc, &ppDT, nLineCount );
			nLineCount++;
		}
	}
	::EndPage( hdc );
}

void CPrintCtrl::PrintPreplanHeader( HDC hdc, PPREPLANHEADER pPPHeader )
{
	HFONT hNewFont, hOldFont;
	LOGFONT  lf;
	TEXTMETRIC  tm;
	char cTempBuff[100];
	int x, y;

	lf.lfHeight				= 60;
	lf.lfWidth				= 0; 
	lf.lfEscapement			= 0; 
	lf.lfOrientation		= 0; 
	lf.lfWeight				= FW_EXTRABOLD; 
	lf.lfItalic				= 0; 
	lf.lfUnderline			= 0; 
	lf.lfStrikeOut			= 0; 
	lf.lfCharSet			= DEFAULT_CHARSET; 
	lf.lfOutPrecision		= OUT_CHARACTER_PRECIS;
	lf.lfClipPrecision		= CLIP_DEFAULT_PRECIS; 
	lf.lfQuality			= DEFAULT_QUALITY; 
	lf.lfPitchAndFamily		= DEFAULT_PITCH; 
	strcpy( lf.lfFaceName, "Courier New");
	hNewFont = CreateFontIndirect( &lf );
	
  
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );

	GetTextMetrics( hdc, &tm );

	x = m_nStartPageX;
	y = m_nStartPageY;

	strcpy( cTempBuff, "Team" );
	::TextOut( hdc, x, y, cTempBuff, strlen( cTempBuff ) );
	::TextOut( hdc, x + 200, y, pPPHeader->Team, strlen( pPPHeader->Team ) );
	strcpy( cTempBuff, "Datum:" );
	::TextOut( hdc, x + 1800, y, cTempBuff, strlen( cTempBuff ) );
	::TextOut( hdc, x + 2000, y, pPPHeader->Date, strlen( pPPHeader->Date ) );
	::SelectObject( hdc, hOldFont );	
	DeleteObject( hNewFont );

	lf.lfHeight				= 50;
	hNewFont = CreateFontIndirect( &lf );
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );

	 y += (tm.tmHeight + 5);
	strcpy( cTempBuff, "Rng" );
	::TextOut( hdc, x, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Name" );
	::TextOut( hdc, x + 100, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "SFT" );
	::TextOut( hdc, x + 600, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Anwesend" );
	::TextOut( hdc, x + 700, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Flua/CCI" );
	::TextOut( hdc, x + 950, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Zugeordnetbis" );
	::TextOut( hdc, x + 1195, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Unterschrift" );
	::TextOut( hdc, x + 1990, y, cTempBuff, strlen( cTempBuff ) );

	::SelectObject( hdc, hOldFont );	
	DeleteObject( hNewFont );

}

void CPrintCtrl::PrintPreplanDetail( HDC hdc, PPREPLANDETAIL pPPDetail, int nLineNo )
{
	HFONT hNewFont, hOldFont;
	LOGFONT  lf;
	TEXTMETRIC  tm;
	int x, y;

	lf.lfHeight				= 40;
	lf.lfWidth				= 0; 
	lf.lfEscapement			= 0; 
	lf.lfOrientation		= 0; 
	lf.lfWeight				= FW_EXTRABOLD; 
	lf.lfItalic				= 0; 
	lf.lfUnderline			= 0; 
	lf.lfStrikeOut			= 0; 
	lf.lfCharSet			= DEFAULT_CHARSET; 
	lf.lfOutPrecision		= OUT_CHARACTER_PRECIS;
	lf.lfClipPrecision		= CLIP_DEFAULT_PRECIS; 
	lf.lfQuality			= DEFAULT_QUALITY; 
	lf.lfPitchAndFamily		= DEFAULT_PITCH; 
	strcpy( lf.lfFaceName, "Courier New");
	hNewFont = CreateFontIndirect( &lf );
	
  
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );

	GetTextMetrics( hdc, &tm );
	x = m_nStartPageX;
	y = (m_nStartPageY + 120) + ( 45 * nLineNo );

	::TextOut( hdc, x, y, pPPDetail->Rng, strlen( pPPDetail->Rng ) );
	::TextOut( hdc, x + 100, y, pPPDetail->Name, strlen( pPPDetail->Name ) );
	::TextOut( hdc, x + 600, y, pPPDetail->Sft, strlen( pPPDetail->Sft ) );
	::TextOut( hdc, x + 700, y, pPPDetail->Anwesend1, strlen( pPPDetail->Anwesend1 ) );
	::TextOut( hdc, x + 820, y, pPPDetail->Anwesend2, strlen( pPPDetail->Anwesend2 ) );
	::TextOut( hdc, x + 950, y, pPPDetail->Flua_CCI, strlen( pPPDetail->Flua_CCI ) );

	::TextOut( hdc, x + 1195, y, pPPDetail->Zugeorduetbis1, strlen( pPPDetail->Zugeorduetbis1 ) );
	::TextOut( hdc, x + 1355, y, pPPDetail->Zugeorduetbis2, strlen( pPPDetail->Zugeorduetbis2 ) );

	::TextOut( hdc, x + 1465, y, pPPDetail->Zugeorduetbis3, strlen( pPPDetail->Zugeorduetbis3 ) );
	::TextOut( hdc, x + 1625, y, pPPDetail->Zugeorduetbis4, strlen( pPPDetail->Zugeorduetbis4 ) );	

	::TextOut( hdc, x + 1735, y, pPPDetail->Zugeorduetbis5, strlen( pPPDetail->Zugeorduetbis5 ) );
	::TextOut( hdc, x + 1900, y, pPPDetail->Zugeorduetbis6, strlen( pPPDetail->Zugeorduetbis6 ) );

	::TextOut( hdc, x + 1995, y, pPPDetail->Unterschrift, strlen( pPPDetail->Unterschrift ) );

	::SelectObject( hdc, hOldFont );	
	DeleteObject( hNewFont );

}


void CPrintCtrl::StaffTablePrint()
{
//	HDC		hdc;
//	DOCINFO	docInfo;
//
//	if( CPrintDialog::DoModal() == IDCANCEL ) return;
//	hdc = GetPrinterDC();
//
//	memset(&docInfo, 0, sizeof(DOCINFO));
//	docInfo.cbSize = sizeof( DOCINFO );
//	docInfo.lpszDocName = "StaffTable Report";	
//	::StartDoc( hdc, &docInfo );
//
//	
//	::StartPage( hdc );
//	PrintStaffTableHeader( hdc );
//
//	int i, nLineCount;
//	STAFFTABLE_LINEDATA	*pLineData;
//
//	
//	for( nLineCount = 0, i = 0; i < omStaffTableLine.GetSize(); i++ ) 
//	{
//		pLineData = &omStaffTableLine[i];
//		if( nLineCount > 62 )
//		{
//			::EndPage( hdc );
//			::StartPage( hdc );
//			PrintStaffTableHeader( hdc );
//			nLineCount = 0;
//		}
//
//		PrintStaffTableDetail( hdc, pLineData, nLineCount );
//		nLineCount++;
//	}
//
//
//
//	::EndPage( hdc );
//
//	
//	::EndDoc( hdc );
//	::DeleteDC( hdc );

}

void CPrintCtrl::PrintStaffTableHeader( HDC hdc )
{
	HFONT hNewFont, hOldFont;
	LOGFONT  lf;
	TEXTMETRIC  tm;
	char cTempBuff[100];
	int x, y;

	lf.lfHeight				= 60;
	lf.lfWidth				= 0; 
	lf.lfEscapement			= 0; 
	lf.lfOrientation		= 0; 
	lf.lfWeight				= FW_EXTRABOLD; 
	lf.lfItalic				= 0; 
	lf.lfUnderline			= 0; 
	lf.lfStrikeOut			= 0; 
	lf.lfCharSet			= DEFAULT_CHARSET; 
	lf.lfOutPrecision		= OUT_CHARACTER_PRECIS;
	lf.lfClipPrecision		= CLIP_DEFAULT_PRECIS; 
	lf.lfQuality			= DEFAULT_QUALITY; 
	lf.lfPitchAndFamily		= DEFAULT_PITCH; 
	strcpy( lf.lfFaceName, "Courier New");
	hNewFont = CreateFontIndirect( &lf );
	  
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );

	GetTextMetrics( hdc, &tm );

	x = m_nStartPageX;
	y = m_nStartPageY;

	lf.lfHeight				= 50;
	hNewFont = CreateFontIndirect( &lf );
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );

	strcpy( cTempBuff, "RA" );
	::TextOut( hdc, x, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Name" );
	::TextOut( hdc, x + 100, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Team" );
	::TextOut( hdc, x + 600, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "SFT" );
	::TextOut( hdc, x + 720, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "SFT" );
	::TextOut( hdc, x + 820, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Von" );
	::TextOut( hdc, x + 920, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Bin" );
	::TextOut( hdc, x + 1100, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Sprachen" );
	::TextOut( hdc, x + 1250, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Zuordnung" );
	::TextOut( hdc, x + 1530, y, cTempBuff, strlen( cTempBuff ) );
	
	::SelectObject( hdc, hOldFont );	
	DeleteObject( hNewFont );
}

void CPrintCtrl::PrintStaffTableDetail( HDC hdc, STAFFTABLE_LINEDATA *pData, int nLineNo )
{
//	HFONT hNewFont, hOldFont;
//	LOGFONT  lf;
//	TEXTMETRIC  tm;
//	int x, y;
//
//	lf.lfHeight				= 40;
//	lf.lfWidth				= 0; 
//	lf.lfEscapement			= 0; 
//	lf.lfOrientation		= 0; 
//	lf.lfWeight				= FW_EXTRABOLD; 
//	lf.lfItalic				= 0; 
//	lf.lfUnderline			= 0; 
//	lf.lfStrikeOut			= 0; 
//	lf.lfCharSet			= DEFAULT_CHARSET; 
//	lf.lfOutPrecision		= OUT_CHARACTER_PRECIS;
//	lf.lfClipPrecision		= CLIP_DEFAULT_PRECIS; 
//	lf.lfQuality			= DEFAULT_QUALITY; 
//	lf.lfPitchAndFamily		= DEFAULT_PITCH; 
//	strcpy( lf.lfFaceName, "Courier New");
//	hNewFont = CreateFontIndirect( &lf );
//	
//	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );
//
//	GetTextMetrics( hdc, &tm );
//	x = m_nStartPageX;
//	y = (m_nStartPageY + 100) + ( 45 * nLineNo );
//
//	char tmp[30];
//	CString str;
//	
//	_strset(tmp, '\0' );
//	strcpy(tmp, pData->Peno);
//	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(tmp);
//	_strset(tmp, '\0' );
//	strcpy(tmp, polShift->Rank);
//	::TextOut( hdc, x, y, tmp, strlen( tmp ) );
//
//	_strset(tmp, '\0' );
//	strcpy(tmp, pData->Name);
//	::TextOut( hdc, x + 100, y, tmp, strlen( tmp ) );
//
//	_strset(tmp, '\0' );
//	strcpy(tmp, pData->Tmid);
//	::TextOut( hdc, x + 600, y, tmp, strlen( tmp ) );
//
//	_strset(tmp, '\0' );
//	strcpy(tmp, pData->Sfca);
//	::TextOut( hdc, x + 720, y, tmp, strlen( tmp ) );
//	
//	_strset(tmp, '\0' );
//	strcpy(tmp, pData->Sti2);
//	::TextOut( hdc, x + 820, y, tmp, strlen( tmp ) );
//
//	_strset(tmp, '\0' );
//	str = CString(pData->Acfr.Format("%d/%H%M"));
//	strcpy( tmp, str );
//	::TextOut( hdc, x + 920, y, tmp, strlen( tmp ) );
//
//	_strset(tmp, '\0' );
//	str = CString(pData->Acto.Format("%d/%H%M"));
//	strcpy( tmp, str );
//	::TextOut( hdc, x + 1100, y, tmp, strlen( tmp ) );
//
//
//	::SelectObject( hdc, hOldFont );	
//	DeleteObject( hNewFont );
}

void CPrintCtrl::FlightSchedulePrint()
{
	HDC		hdc;
	DOCINFO	docInfo;

	if( CPrintDialog::DoModal() == IDCANCEL ) return;
	hdc = GetPrinterDC();

	memset(&docInfo, 0, sizeof(DOCINFO));
	docInfo.cbSize = sizeof( DOCINFO );
	docInfo.lpszDocName = "FlightSchedule Report";	
	::StartDoc( hdc, &docInfo );

	
	::StartPage( hdc );
	PrintFlightScheduleHeader( hdc );

	int i, nLineCount;
	FLIGHTSCHEDULE_LINEDATA	*pLineData;
	
	for( nLineCount = 0, i = 0; i < omFlightScheduleLines.GetSize(); i++ ) 
	{
		pLineData = &omFlightScheduleLines[i];
		if( nLineCount > 40 )
		{
			::EndPage( hdc );
			::StartPage( hdc );
			PrintFlightScheduleHeader( hdc );
			nLineCount = 0;
		}

		PrintFlightScheduleDetail( hdc, pLineData, nLineCount );
		nLineCount++;
	}

	::EndPage( hdc );

	
	::EndDoc( hdc );
	::DeleteDC( hdc );

}


void CPrintCtrl::PrintFlightScheduleHeader( HDC hdc )
{
	HFONT hNewFont, hOldFont;
	LOGFONT  lf;
	TEXTMETRIC  tm;
	char cTempBuff[100];
	int x, y;

	lf.lfHeight				= 60;
	lf.lfWidth				= 0; 
	lf.lfEscapement			= 0; 
	lf.lfOrientation		= 0; 
	lf.lfWeight				= FW_EXTRABOLD; 
	lf.lfItalic				= 0; 
	lf.lfUnderline			= 0; 
	lf.lfStrikeOut			= 0; 
	lf.lfCharSet			= DEFAULT_CHARSET; 
	lf.lfOutPrecision		= OUT_CHARACTER_PRECIS;
	lf.lfClipPrecision		= CLIP_DEFAULT_PRECIS; 
	lf.lfQuality			= DEFAULT_QUALITY; 
	lf.lfPitchAndFamily		= DEFAULT_PITCH; 
	strcpy( lf.lfFaceName, "Courier New");
	hNewFont = CreateFontIndirect( &lf );
	  
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );

	GetTextMetrics( hdc, &tm );

	x = m_nStartPageX;
	y = m_nStartPageY;

	lf.lfHeight				= 50;
	hNewFont = CreateFontIndirect( &lf );
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );

	y -= 50;
	
	strcpy( cTempBuff, "Regist" );
	::TextOut( hdc, x, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "A/C" );
	::TextOut( hdc, x + 180, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Flight No." );
	::TextOut( hdc, x + 280, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Ori" );
	::TextOut( hdc, x + 550, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Via" );
	::TextOut( hdc, x + 670, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Sched" );
	::TextOut( hdc, x + 770, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Etoa" );
	::TextOut( hdc, x + 950, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Actual" );
	::TextOut( hdc, x + 1130, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Pos" );
	::TextOut( hdc, x + 1300, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Gate" );
	::TextOut( hdc, x + 1400, y, cTempBuff, strlen( cTempBuff ) );
	

	strcpy( cTempBuff, "Flight No." );
	::TextOut( hdc, x + 1520, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Des" );
	::TextOut( hdc, x + 1800, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Via" );
	::TextOut( hdc, x + 1920, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Sched" );
	::TextOut( hdc, x + 2020, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Estd" );
	::TextOut( hdc, x + 2200, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Ofbl" );
	::TextOut( hdc, x + 2370, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Pos" );
	::TextOut( hdc, x + 2600, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Gate" );
	::TextOut( hdc, x + 2700, y, cTempBuff, strlen( cTempBuff ) );

	::SelectObject( hdc, hOldFont );	
	DeleteObject( hNewFont );


}

void CPrintCtrl::PrintFlightScheduleDetail( HDC hdc, 
											FLIGHTSCHEDULE_LINEDATA *pData, 
											int nLineNo )
{
	HFONT hNewFont, hOldFont;
	LOGFONT  lf;
	TEXTMETRIC  tm;
	int x, y;

	lf.lfHeight				= 40;
	lf.lfWidth				= 0; 
	lf.lfEscapement			= 0; 
	lf.lfOrientation		= 0; 
	lf.lfWeight				= FW_EXTRABOLD; 
	lf.lfItalic				= 0; 
	lf.lfUnderline			= 0; 
	lf.lfStrikeOut			= 0; 
	lf.lfCharSet			= DEFAULT_CHARSET; 
	lf.lfOutPrecision		= OUT_CHARACTER_PRECIS;
	lf.lfClipPrecision		= CLIP_DEFAULT_PRECIS; 
	lf.lfQuality			= DEFAULT_QUALITY; 
	lf.lfPitchAndFamily		= DEFAULT_PITCH; 
	strcpy( lf.lfFaceName, "Courier New");
	hNewFont = CreateFontIndirect( &lf );
	
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );

	GetTextMetrics( hdc, &tm );
	x = m_nStartPageX;
	y = (m_nStartPageY + 50) + ( 45 * nLineNo );

	char tmp[30];
	CString str;

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Regn);
	::TextOut( hdc, x , y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Actl);
	::TextOut( hdc, x + 180, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Fnuma);
	::TextOut( hdc, x + 280, y, tmp, strlen( tmp ) );
	
	_strset(tmp, '\0' );
	strcpy(tmp, pData->Rou1a);
	::TextOut( hdc, x + 550, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Rou2a);
	::TextOut( hdc, x + 670, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	str = CString(pData->Stoa.Format("%d/%H%M"));
	strcpy( tmp, str );
	::TextOut( hdc, x + 770, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	str = CString(pData->Etoa.Format("%d/%H%M"));
	strcpy( tmp, str );
	::TextOut( hdc, x + 950, y, tmp, strlen( tmp ) );


	_strset(tmp, '\0' );
	strcpy(tmp, pData->Posia);
	::TextOut( hdc, x + 1300, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Alida);
	::TextOut( hdc, x + 1400, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Fnumd);
	::TextOut( hdc, x + 1520, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Rou1d);
	::TextOut( hdc, x + 1800, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Rou2d);
	::TextOut( hdc, x + 1920, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	str = CString(pData->Stod.Format("%d/%H%M"));
	strcpy( tmp, str );
	::TextOut( hdc, x + 2020, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	str = CString(pData->Etod.Format("%d/%H%M"));
	strcpy( tmp, str );
	::TextOut( hdc, x + 2200, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	str = CString(pData->Ofbl.Format("%d/%H%M"));
	strcpy( tmp, str );
	::TextOut( hdc, x + 2370, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Posid);
	::TextOut( hdc, x + 2600, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Alidd);
	::TextOut( hdc, x + 2700, y, tmp, strlen( tmp ) );


	::SelectObject( hdc, hOldFont );	
	DeleteObject( hNewFont );

}



void CPrintCtrl::GateTablePrint()
{
	HDC		hdc;
	DOCINFO	docInfo;

	if( CPrintDialog::DoModal() == IDCANCEL ) return;
	hdc = GetPrinterDC();

	memset(&docInfo, 0, sizeof(DOCINFO));
	docInfo.cbSize = sizeof( DOCINFO );
	docInfo.lpszDocName = "StaffTable Report";	
	::StartDoc( hdc, &docInfo );

	
	::StartPage( hdc );
	PrintGateTableHeader( hdc );

	int i, nLineCount;
	GATEDATA_LINE	*pLineData;
	int nGateDataLine = omGateTableLines.GetSize();
	
	for( nLineCount = 0, i = 0; i < nGateDataLine; i++ ) 
	{
		pLineData = &omGateTableLines[i];
		if( nLineCount > 45 )
		{
			::EndPage( hdc );
			::StartPage( hdc );
			PrintGateTableHeader( hdc );
			nLineCount = 0;
		}

		PrintGateTableDetail( hdc, pLineData, nLineCount );
		nLineCount++;
	}


	::EndPage( hdc );

	
	::EndDoc( hdc );

	::DeleteDC( hdc );

}


void CPrintCtrl::PrintGateTableHeader( HDC hdc )
{
	HFONT hNewFont, hOldFont;
	LOGFONT  lf;
	TEXTMETRIC  tm;
	char cTempBuff[100];
	int x, y;

	lf.lfHeight				= 60;
	lf.lfWidth				= 0; 
	lf.lfEscapement			= 0; 
	lf.lfOrientation		= 0; 
	lf.lfWeight				= FW_EXTRABOLD; 
	lf.lfItalic				= 0; 
	lf.lfUnderline			= 0; 
	lf.lfStrikeOut			= 0; 
	lf.lfCharSet			= DEFAULT_CHARSET; 
	lf.lfOutPrecision		= OUT_CHARACTER_PRECIS;
	lf.lfClipPrecision		= CLIP_DEFAULT_PRECIS; 
	lf.lfQuality			= DEFAULT_QUALITY; 
	lf.lfPitchAndFamily		= DEFAULT_PITCH; 
	strcpy( lf.lfFaceName, "Courier New");
	hNewFont = CreateFontIndirect( &lf );
	  
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );

	GetTextMetrics( hdc, &tm );

	x = m_nStartPageX;
	y = m_nStartPageY;

	lf.lfHeight				= 50;
	hNewFont = CreateFontIndirect( &lf );
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );
	strcpy( cTempBuff, GetString(IDS_STRING61637) );
	::TextOut( hdc, x, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, GetString(IDS_STRING61639) );
	::TextOut( hdc, x + 150, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, GetString(IDS_STRING32823) );
	::TextOut( hdc, x + 350, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, GetString(IDS_STRING33150) );
	::TextOut( hdc, x + 850, y, cTempBuff, strlen( cTempBuff ) );
	::SelectObject( hdc, hOldFont );	
	DeleteObject( hNewFont );
}



void CPrintCtrl::PrintGateTableDetail( HDC hdc, GATEDATA_LINE *pData, int nLineNo )
{
#if 0	// not used anymore
	HFONT hNewFont, hOldFont;
	LOGFONT  lf;
	TEXTMETRIC  tm;
	int x, y;

	lf.lfHeight				= 40;
	lf.lfWidth				= 0; 
	lf.lfEscapement			= 0; 
	lf.lfOrientation		= 0; 
	lf.lfWeight				= FW_EXTRABOLD; 
	lf.lfItalic				= 0; 
	lf.lfUnderline			= 0; 
	lf.lfStrikeOut			= 0; 
	lf.lfCharSet			= DEFAULT_CHARSET; 
	lf.lfOutPrecision		= OUT_CHARACTER_PRECIS;
	lf.lfClipPrecision		= CLIP_DEFAULT_PRECIS; 
	lf.lfQuality			= DEFAULT_QUALITY; 
	lf.lfPitchAndFamily		= DEFAULT_PITCH; 
	strcpy( lf.lfFaceName, "Courier New");
	hNewFont = CreateFontIndirect( &lf );
	
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );

	GetTextMetrics( hdc, &tm );
	x = m_nStartPageX;
	y = (m_nStartPageY + 100) + ( 45 * nLineNo );

	char tmp[30];
	CString str;
	
	_strset(tmp, '\0' );
	strcpy(tmp, pData->Alid);
	::TextOut( hdc, x, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Fnum);
	::TextOut( hdc, x + 150, y, tmp, strlen( tmp ) );

	int nJobLineCount = pData->JobData.GetSize();
	for(int i = 0; i < nJobLineCount; i++) {
		_strset(tmp, '\0' );
		strcpy(tmp, pData->JobData[i].Name);
		::TextOut( hdc, (x + 350) + (i * 700), y, tmp, strlen( tmp ) );
		_strset(tmp, '\0' );
		CString tmpTime;
		tmpTime = pData->JobData[i].Acfr.Format("%H%M") + CString("-");
		tmpTime += pData->JobData[i].Acto.Format("%H%M");
		strcpy(tmp, tmpTime);
		::TextOut( hdc, (x + 850) + (i * 700), y, tmp, strlen( tmp ) );
	}


	::SelectObject( hdc, hOldFont );	
	DeleteObject( hNewFont );
#endif
}


void CPrintCtrl::PrmTablePrint()
{
	HDC		hdc;
	DOCINFO	docInfo;

	if( CPrintDialog::DoModal() == IDCANCEL ) return;
	hdc = GetPrinterDC();

	memset(&docInfo, 0, sizeof(DOCINFO));
	docInfo.cbSize = sizeof( DOCINFO );
	docInfo.lpszDocName = "PRMRequests";	
	::StartDoc( hdc, &docInfo );

	
	::StartPage( hdc );
	PrintPrmTableHeader( hdc );

	int i, nLineCount;
	PRMDATA_LINE *pLineData;
	
	for( nLineCount = 0, i = 0; i < omPrmTableLines.GetSize(); i++ ) 
	{
		pLineData = &omPrmTableLines[i];
		if( nLineCount > 40 )
		{
			::EndPage( hdc );
			::StartPage( hdc );
			PrintPrmTableHeader( hdc );
			nLineCount = 0;
		}

	//	PrintFlightScheduleDetail( hdc, pLineData, nLineCount );
		nLineCount++;
	}

	::EndPage( hdc );

	
	::EndDoc( hdc );
	::DeleteDC( hdc );

}


void CPrintCtrl::PrintPrmTableHeader( HDC hdc )
{
	HFONT hNewFont, hOldFont;
	LOGFONT  lf;
	TEXTMETRIC  tm;
	char cTempBuff[100];
	int x, y;

	lf.lfHeight				= 60;
	lf.lfWidth				= 0; 
	lf.lfEscapement			= 0; 
	lf.lfOrientation		= 0; 
	lf.lfWeight				= FW_EXTRABOLD; 
	lf.lfItalic				= 0; 
	lf.lfUnderline			= 0; 
	lf.lfStrikeOut			= 0; 
	lf.lfCharSet			= DEFAULT_CHARSET; 
	lf.lfOutPrecision		= OUT_CHARACTER_PRECIS;
	lf.lfClipPrecision		= CLIP_DEFAULT_PRECIS; 
	lf.lfQuality			= DEFAULT_QUALITY; 
	lf.lfPitchAndFamily		= DEFAULT_PITCH; 
	strcpy( lf.lfFaceName, "Courier New");
	hNewFont = CreateFontIndirect( &lf );
	  
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );

	GetTextMetrics( hdc, &tm );

	x = m_nStartPageX;
	y = m_nStartPageY;

	lf.lfHeight				= 50;
	hNewFont = CreateFontIndirect( &lf );
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );

	y -= 50;
	
	strcpy( cTempBuff, "ID" );
	::TextOut( hdc, x, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Name" );
	::TextOut( hdc, x + 180, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Type" );
	::TextOut( hdc, x + 280, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Gender" );
	::TextOut( hdc, x + 550, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Arr.Time" );
	::TextOut( hdc, x + 670, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Flight" );
	::TextOut( hdc, x + 770, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "T-Flight" );
	::TextOut( hdc, x + 950, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Lang." );
	::TextOut( hdc, x + 1130, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Natl." );
	::TextOut( hdc, x + 1300, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Jobs" );
	::TextOut( hdc, x + 1400, y, cTempBuff, strlen( cTempBuff ) );
	
	/***
	strcpy( cTempBuff, "Flight No." );
	::TextOut( hdc, x + 1520, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Des" );
	::TextOut( hdc, x + 1800, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Via" );
	::TextOut( hdc, x + 1920, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Sched" );
	::TextOut( hdc, x + 2020, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Estd" );
	::TextOut( hdc, x + 2200, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Ofbl" );
	::TextOut( hdc, x + 2370, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Pos" );
	::TextOut( hdc, x + 2600, y, cTempBuff, strlen( cTempBuff ) );

	strcpy( cTempBuff, "Gate" );
	::TextOut( hdc, x + 2700, y, cTempBuff, strlen( cTempBuff ) );
	**/

	::SelectObject( hdc, hOldFont );	
	DeleteObject( hNewFont );


}

void CPrintCtrl::PrintPrmTableDetail( HDC hdc, 
											PRMDATA_LINE *pData, 
											int nLineNo )
{
	HFONT hNewFont, hOldFont;
	LOGFONT  lf;
	TEXTMETRIC  tm;
	int x, y;

	lf.lfHeight				= 40;
	lf.lfWidth				= 0; 
	lf.lfEscapement			= 0; 
	lf.lfOrientation		= 0; 
	lf.lfWeight				= FW_EXTRABOLD; 
	lf.lfItalic				= 0; 
	lf.lfUnderline			= 0; 
	lf.lfStrikeOut			= 0; 
	lf.lfCharSet			= DEFAULT_CHARSET; 
	lf.lfOutPrecision		= OUT_CHARACTER_PRECIS;
	lf.lfClipPrecision		= CLIP_DEFAULT_PRECIS; 
	lf.lfQuality			= DEFAULT_QUALITY; 
	lf.lfPitchAndFamily		= DEFAULT_PITCH; 
	strcpy( lf.lfFaceName, "Courier New");
	hNewFont = CreateFontIndirect( &lf );
	
	hOldFont = (HFONT)::SelectObject( hdc, hNewFont );

	GetTextMetrics( hdc, &tm );
	x = m_nStartPageX;
	y = (m_nStartPageY + 50) + ( 45 * nLineNo );

	char tmp[30];
	CString str;


	_strset(tmp, '\0' );
	strcpy(tmp, pData->dpx->Prid);
	::TextOut( hdc, x , y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->dpx->Name);
	::TextOut( hdc, x + 180, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->dpx->Prmt);
	::TextOut( hdc, x + 280, y, tmp, strlen( tmp ) );
	
	_strset(tmp, '\0' );
	strcpy(tmp, pData->dpx->Gend);
	::TextOut( hdc, x + 550, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->dpx->Sati.Format("%d.%m %H:%M"));
	::TextOut( hdc, x + 670, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	str = CString(pData->dpx->FlnuFlno);
	strcpy( tmp, str );
	::TextOut( hdc, x + 770, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	str = CString(pData->dpx->TfluFlno);
	strcpy( tmp, str );
	::TextOut( hdc, x + 950, y, tmp, strlen( tmp ) );


	_strset(tmp, '\0' );
	strcpy(tmp, pData->dpx->Lang);
	::TextOut( hdc, x + 1300, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->dpx->Natl);
	::TextOut( hdc, x + 1400, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->HasJobs);
	::TextOut( hdc, x + 1520, y, tmp, strlen( tmp ) );

	/**************
	_strset(tmp, '\0' );
	strcpy(tmp, pData->Rou1d);
	::TextOut( hdc, x + 1800, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Rou2d);
	::TextOut( hdc, x + 1920, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	str = CString(pData->Stod.Format("%d/%H%M"));
	strcpy( tmp, str );
	::TextOut( hdc, x + 2020, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	str = CString(pData->Etod.Format("%d/%H%M"));
	strcpy( tmp, str );
	::TextOut( hdc, x + 2200, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	str = CString(pData->Ofbl.Format("%d/%H%M"));
	strcpy( tmp, str );
	::TextOut( hdc, x + 2370, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Posid);
	::TextOut( hdc, x + 2600, y, tmp, strlen( tmp ) );

	_strset(tmp, '\0' );
	strcpy(tmp, pData->Alidd);
	::TextOut( hdc, x + 2700, y, tmp, strlen( tmp ) );
**/

	::SelectObject( hdc, hOldFont );	
	DeleteObject( hNewFont );

}


