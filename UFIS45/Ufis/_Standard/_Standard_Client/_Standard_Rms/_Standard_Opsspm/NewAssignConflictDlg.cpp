// NewAssignConflictDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <NewAssignConflictDlg.h>
#include <NewAssignConflictViewer.h>
#include <NewAssignConflictGantt.h>
#include <CedaRudData.h>
#include <CedaSerData.h>
#include <JobWithoutDemDlg.h>
#include <CedaAloData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern bool conflict;

extern bool newconfi;
extern CString name;
/////////////////////////////////////////////////////////////////////////////
// CNewAssignConflictDlg dialog


CNewAssignConflictDlg::CNewAssignConflictDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNewAssignConflictDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewAssignConflictDlg)
	m_Explanation = _T("");
	m_DisplayNonTeamDemands = FALSE;
	//}}AFX_DATA_INIT
	bmScrolling = false;
	lmOldUdem = 0L;


}


void CNewAssignConflictDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewAssignConflictDlg)
	DDX_Control(pDX, IDC_NONTEAMDEMANDS, m_NonTeamDemandsCtrl);
	DDX_Control(pDX, IDC_STATUSBAR, m_StatusBar);
	DDX_Control(pDX, IDC_EXPLANATION, m_ExplanationCtrl);
	DDX_Text(pDX, IDC_EXPLANATION, m_Explanation);
	DDX_Check(pDX, IDC_NONTEAMDEMANDS, m_DisplayNonTeamDemands);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewAssignConflictDlg, CDialog)
	//{{AFX_MSG_MAP(CNewAssignConflictDlg)
	ON_BN_CLICKED(IDC_NONTEAMDEMANDS, OnNonTeamDemandsClicked)
	ON_WM_CREATE()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
	ON_WM_DESTROY()
    ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewAssignConflictDlg message handlers

void CNewAssignConflictDlg::OnOK() 
{
	

	
	JOBDATA *prlJob = NULL;
	int ilNumJobs = omNoDemandJobs.GetSize();
	bool blCancel = false;

	CString olAloc = "";
	long llUtpl = 0L;
	if(omDemands.GetSize() > 0)
	{
		olAloc = omDemands[0].Aloc;
		llUtpl = ogDataSet.GetUtplByDemand(&omDemands[0]);
	}

	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		prlJob = &omNoDemandJobs[ilJob];
		if(prlJob->Utpl == 0L || prlJob->Ualo == 0L)
		{
			// job was not assigned to a demand so request alloc unit and template
			JobWithoutDemDlg olDlg;
			olDlg.InitDefaultValues(prlJob->Acfr, prlJob->Acto, olAloc, llUtpl);
			if(olDlg.DoModal() == IDOK)
			{
				prlJob->Acfr = olDlg.omFrom;
				prlJob->Acto = olDlg.omTo;
				strcpy(prlJob->Aloc,olDlg.omAloc);
				prlJob->Ualo = ogAloData.GetAloUrnoByName(prlJob->Aloc);
				prlJob->Utpl = olDlg.lmUtpl;
			}
			else
			{
				blCancel = true;
				break;
			}
		}
	}

	if(!blCancel)
	{
		CDialog::OnOK();
	}
}

void CNewAssignConflictDlg::OnCancel() 
{
	if(!ogJodData.bmUseJodtab && omViewer.bmSingleJobAndDemand && omJobs.GetSize() == 1)
	{
		omJobs[0].Udem = lmOldUdem;
	}
	
	CDialog::OnCancel();
}

BOOL CNewAssignConflictDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString olDemandType = GetString(IDS_ASSIGNEMPLOYEE);
	int ilNumDemands = omDemands.GetSize();

	
	
	for(int ilD = 0; ilD < ilNumDemands; ilD++)
	{
		if(ogDemandData.DemandIsOfType(&omDemands[ilD],PERSONNELDEMANDS))
		{
			olDemandType = GetString(IDS_ASSIGNEMPLOYEE);
			break;
		}
		else if(ogDemandData.DemandIsOfType(&omDemands[ilD],EQUIPMENTDEMANDS))
		{
			olDemandType = GetString(IDS_ASSIGNEQUIPMENT);
			break;
		}
		else if(ogDemandData.DemandIsOfType(&omDemands[ilD],LOCATIONDEMANDS))
		{
			olDemandType = GetString(IDS_ASSIGNLOCATION);
			break;
		}
	}

	CString olCaption;
	if(prmRotation != NULL && prmFlight != NULL)
	{
		CString olFnumText;
		olFnumText.Format("%s -> %s",prmFlight->Fnum,prmRotation->Fnum);
		// "Error Assigning %s to Flight %s !"
		olCaption.Format(GetString(IDS_ASSIGNCONFLICTCAPTION),olDemandType,olFnumText);
	}
	else if(prmFlight != NULL)
	{
		// "Error Assigning %s to Flight %s !"
		olCaption.Format(GetString(IDS_ASSIGNCONFLICTCAPTION),olDemandType,prmFlight->Fnum);
	}
	else if(!omRegn.IsEmpty())
	{
		// "Error Assigning %s to Registration %s !"
		olCaption.Format(GetString(IDS_ASSIGNCONFLICTCAPTION2),olDemandType,omRegn);
	}
	else
	{
		
		
		
		if(ilNumDemands > 0 && omDemands[0].Dety[0] == FID_DEMAND)
		{
			RUDDATA* prlRud = ogRudData.GetRudByUrno(omDemands[0].Urud);
			if(prlRud != NULL)
			{
				SERDATA* prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
				if(prlSer != NULL)
				{
					// "Error Assigning to FID:"
					olCaption.Format("%s %s  %s - %s",GetString(IDS_ASSIGNCONFLICTCAPTION3), (const char *)prlSer->Snam, omDemands[0].Debe.Format("%H%M/%d"), omDemands[0].Deen.Format("%H%M/%d"));
				}
			}
		}
	}
	SetWindowText(olCaption);



	CWnd *polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_ASSIGNBUTTON));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_CANCELBUTTON));
	}

	m_NonTeamDemandsCtrl.SetWindowText(GetString(IDS_NONTEAMDEMANDS));
	if(omNonTeamDemands.GetSize() <= 0)
	{
		m_NonTeamDemandsCtrl.EnableWindow(FALSE);
	}
	else
	{
		m_NonTeamDemandsCtrl.SetCheck(ogBasicData.bmAssignErrorDlgShowAllDemands ? 1 : 0);
	}

	// "One or more conflicts have occured.\n\nMove the mouse over the bars below to display more information about functions, permits and conflicts."
	m_ExplanationCtrl.SetWindowText(GetString(IDS_ASSIGNCONFLICTEXPLANATION));

    CRect olRect;
    GetDlgItem(IDC_EINSALZE)->GetWindowRect(&olRect); 
	ScreenToClient(&olRect);
    olRect.InflateRect(-15, -15);

    omTSInterval = CTimeSpan(0, 0, 10, 0);
    omTimeScale.EnableDisplayCurrentTime(FALSE);
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE, CRect(olRect.left, olRect.top, olRect.right, olRect.top + 34), this, 0, NULL);
//    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,AFX_IDW_STATUS_BAR);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

	omViewer.RemoveAll();
	//omDemands.Sort(ByFlightTypeAndTime);
	omViewer.SetDemandsPtr(&omDemands);
	//omViewer.omAllocUnitType = omAllocUnitType;
	omViewer.SetJobsPtr(&omJobs);
	omViewer.SetJodsPtr(&omJods);
	omViewer.SetNoDemandJobsPtr(&omNoDemandJobs);

    omGantt.SetTimeScale(&omTimeScale);
    omGantt.SetViewer(&omViewer, 0 /* GroupNo */);
    omGantt.SetStatusBar(&m_StatusBar);
    omGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());
    omGantt.SetVerticalScaleWidth(-2);
    omGantt.SetFonts(&ogMSSansSerif_Regular_8, &ogSmallFonts_Regular_6);
    //omGantt.SetVerticalScaleColors(NAVY, SILVER, YELLOW, SILVER);
	omGantt.SetGanttChartColors(NAVY, GRAY, YELLOW, GRAY);
    omGantt.Create(0, CRect(olRect.left, olRect.top + 34, olRect.right, olRect.bottom), this);
	omGantt.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
	omViewer.Attach(this);

	DisplayGanttData();

	if (conflict)
	{
 
	  CButton *pbut =(CButton *)GetDlgItem(IDOK);
      pbut->EnableWindow(FALSE);

   	}


	if (newconfi)
	{
	  newconfi=false;
	  CButton *pbut =(CButton *)GetDlgItem(IDOK);
      pbut->EnableWindow(FALSE);

	
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
	
void CNewAssignConflictDlg::DisplayGanttData()
{
	CTime olStartTime = TIMENULL, olEndTime = TIMENULL;
	int ilCount = omDemands.GetSize(), ilLc;
	for(ilLc = 0; ilLc < ilCount; ilLc++)
	{
		if(olStartTime == TIMENULL || olStartTime > omDemands[ilLc].Debe)
		{
			olStartTime = omDemands[ilLc].Debe;
		}
		if(olEndTime == TIMENULL || olEndTime < omDemands[ilLc].Deen)
		{
			olEndTime = omDemands[ilLc].Deen;
		}
	}

	ilCount = omJobs.GetSize();
	for(ilLc = 0; ilLc < ilCount; ilLc++)
	{
		if(olStartTime == TIMENULL || olStartTime > omJobs[ilLc].Acfr)
		{
			olStartTime = omJobs[ilLc].Acfr;
		}
		if(olEndTime == TIMENULL || olEndTime < omJobs[ilLc].Acto)
		{
			olEndTime = omJobs[ilLc].Acto;
		}
	}

	ilCount = omNoDemandJobs.GetSize();
	for(ilLc = 0; ilLc < ilCount; ilLc++)
	{
		if(olStartTime == TIMENULL || olStartTime > omNoDemandJobs[ilLc].Acfr)
		{
			olStartTime = omNoDemandJobs[ilLc].Acfr;
		}
		if(olEndTime == TIMENULL || olEndTime < omNoDemandJobs[ilLc].Acto)
		{
			olEndTime = omNoDemandJobs[ilLc].Acto;
		}
	}
	if(olStartTime == TIMENULL)
	{
		olStartTime = ogBasicData.GetTime() - CTimeSpan(0, 1, 0, 0);
	}
	if(olEndTime == TIMENULL)
	{
		olEndTime = ogBasicData.GetTime() + CTimeSpan(0, 1, 0, 0);
	}

	omTSStartTime = olStartTime - CTimeSpan(0, 1, 0, 0);
	omTSDuration = (olEndTime + CTimeSpan(0, 1, 0, 0)) - omTSStartTime;
    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
	omTimeScale.Invalidate(TRUE);
    omGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());

	omViewer.Init();

	if (::IsWindow(omGantt.GetSafeHwnd()))
	{
		omGantt.ResetContent();
	    for (int ilLineno = 0; ilLineno < omViewer.omFlightLines.GetSize(); ilLineno++)
		{
			omGantt.AddString("");
			omGantt.SetItemHeight(ilLineno, omGantt.GetLineHeight(ilLineno));
		}
		omGantt.InvalidateRect(NULL);
	}
}

// ropJobs and ropDemands are linked by ropDemToJobMap 
void CNewAssignConflictDlg::SetJobsAndDemands(JOBDATA *prpJob, DEMANDDATA *prpDemand) 
{
	CCSPtrArray <JOBDATA> olJobs;
	CCSPtrArray <DEMANDDATA> olDemands;
	CCSPtrArray <JODDATA> olJods;

	if(prpJob != NULL && prpDemand != NULL)
	{
		omJobs.Add(prpJob);
		omDemands.Add(prpDemand);

		if(ogJodData.bmUseJodtab)
		{
			rmJod.Udem = prpDemand->Urno;
			rmJod.Ujob = prpJob->Urno;
			omJods.Add(&rmJod);
		}
		else
		{
			lmOldUdem = prpJob->Udem;
			prpJob->Udem = prpDemand->Urno;
		}

		omViewer.bmSingleJobAndDemand = true;
	}
}

// ropJobs and ropDemands are linked by ropDemToJobMap 
void CNewAssignConflictDlg::SetJobsAndDemands(CCSPtrArray <JOBDATA> &ropJobs, CCSPtrArray <DEMANDDATA> &ropDemands, CCSPtrArray <JODDATA> &ropJods) 
{
	CCSPtrArray <DEMANDDATA> olNonTeamDemands;
	SetJobsAndDemands(ropJobs, ropDemands, ropJods, olNonTeamDemands);
}

// ropJobs and ropDemands are linked by ropDemToJobMap 
void CNewAssignConflictDlg::SetJobsAndDemands(CCSPtrArray <JOBDATA> &ropJobs, CCSPtrArray <DEMANDDATA> &ropDemands, 
											  CCSPtrArray <JODDATA> &ropJods, CCSPtrArray <DEMANDDATA> &ropNonTeamDemands) 
{
	int ilCount = 0, ilLc;
	
	omJods.RemoveAll();
	ilCount = ropJods.GetSize();
	for(ilLc = 0; ilLc < ilCount; ilLc++)
	{
		omJods.Add(&ropJods[ilLc]);
	}

	omDemands.RemoveAll();
	ilCount = ropDemands.GetSize();
	for(ilLc = 0; ilLc < ilCount; ilLc++)
	{
		omDemands.Add(&ropDemands[ilLc]);
	}

	omTeamDemands.RemoveAll();
	ilCount = ropDemands.GetSize();
	for(ilLc = 0; ilLc < ilCount; ilLc++)
	{
		omTeamDemands.Add(&ropDemands[ilLc]);
	}

	omNonTeamDemands.RemoveAll();
	ilCount = ropNonTeamDemands.GetSize();
	for(ilLc = 0; ilLc < ilCount; ilLc++)
	{
		omNonTeamDemands.Add(&ropNonTeamDemands[ilLc]);
		if(ogBasicData.bmAssignErrorDlgShowAllDemands)
		{
			omDemands.Add(&ropNonTeamDemands[ilLc]);
		}
	}

	omJobs.RemoveAll();
	ilCount = ropJobs.GetSize();
	for(ilLc = 0; ilLc < ilCount; ilLc++)
	{
		JOBDATA *prlJob = &ropJobs[ilLc];
		if(JobIsAssigned(prlJob))
		{
			omJobs.Add(prlJob);
		}
		else
		{
			omNoDemandJobs.Add(prlJob);
		}
	}
}

void CNewAssignConflictDlg::GetJods(CCSPtrArray <JODDATA> &ropJods)
{
	ropJods.RemoveAll();
	int ilNumJods = omJods.GetSize();
	for(int ilJod = 0; ilJod < ilNumJods; ilJod++)
	{
		ropJods.Add(&omJods[ilJod]);
	}
}


void CNewAssignConflictDlg::SetFlightPtr(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotation /* = NULL */)
{
	prmFlight = prpFlight;
	prmRotation = prpRotation;
}

void CNewAssignConflictDlg::SetRegn(const char *pcpRegn)
{
	omRegn = pcpRegn;
}

bool CNewAssignConflictDlg::JobIsAssigned(JOBDATA *prpJob)
{
	bool blIsAssigned = false;
	if(prpJob != NULL)
	{
		if(ogJodData.bmUseJodtab)
		{
			int ilCount = omJods.GetSize();
			for(int ilLc = 0; ilLc < ilCount; ilLc++)
			{
				if(omJods[ilLc].Ujob == prpJob->Urno)
				{
					blIsAssigned = true;
					break;
				}
			}
		}
		else
		{
			blIsAssigned = (prpJob->Udem != 0L) ? true : false;
		}
	}
	return blIsAssigned;
}

void CNewAssignConflictDlg::OnNonTeamDemandsClicked() 
{
	UpdateData();
//	else
	{
		int ilNumTeamDems = omTeamDemands.GetSize(), ilDem;
		CDWordArray olJobUrnos;
		int ilNumJods = omJods.GetSize();
		bool blFound = false;
		for(int ilJod = 0; ilJod < ilNumJods; ilJod++)
		{
			blFound = false;
			for(ilDem = 0; ilDem < ilNumTeamDems; ilDem++)
			{
				if(omTeamDemands[ilDem].Urno == omJods[ilJod].Udem)
				{
					blFound = true;
					break;
				}
			}
			if(!blFound)
			{
				olJobUrnos.Add(omJods[ilJod].Ujob);
			}
		}
		int ilNumJobs = olJobUrnos.GetSize();
		for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
		{
			omViewer.DeleteJodsByJobUrno(olJobUrnos[ilJob]);
		}

		omDemands.RemoveAll();
		for(ilDem = 0; ilDem < ilNumTeamDems; ilDem++)
		{
			omDemands.Add(&omTeamDemands[ilDem]);
		}

	}
	if(m_DisplayNonTeamDemands)
	{
//		omDemands.RemoveAll();
		int ilNumDems = omNonTeamDemands.GetSize();
		for(int ilDem = 0; ilDem < ilNumDems; ilDem++)
		{
			omDemands.Add(&omNonTeamDemands[ilDem]);
		}
	}
	DisplayGanttData();

//	if(::IsWindow(omGantt.GetSafeHwnd()))
//	{
//		omGantt.ResetContent();
//		omViewer.Init();
//
//		for (int ilLineno = 0; ilLineno < omViewer.omFlightLines.GetSize(); ilLineno++)
//		{
//			omGantt.AddString("");
//			omGantt.SetItemHeight(ilLineno, omGantt.GetLineHeight(ilLineno));
//		}
//		omGantt.InvalidateRect(NULL);
//	}
}

LONG CNewAssignConflictDlg::OnDragOver(UINT wParam, LONG lParam)
{
	LONG llRc = -1L;

	if((CWnd *)lParam == this)
	{
		AutoScroll(200);
	}
	else
	{
		AutoScroll(); // end scrolling
	}

	return llRc;
}

// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void CNewAssignConflictDlg::AutoScroll(UINT ipInitialScrollSpeed /*200*/)
{
	// if not already scrolling automatically...
	if(!bmScrolling)
	{
		// ... after a short pause (ipInitialScrollSpeed), start scrolling
		bmScrolling = true;
		imScrollSpeed = ipInitialScrollSpeed;
		SetTimer(1, (UINT) imScrollSpeed, NULL);
	}
}

// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void CNewAssignConflictDlg::OnAutoScroll(void)
{
	bmScrolling = false;

	// if standard cursor then not currently dragging
	if(GetCursor() != AfxGetApp()->LoadStandardCursor(IDC_ARROW))
	{
		// check if the cursor is in the automatic scrolling region
		CPoint olPoint;
		::GetCursorPos(&olPoint);

		CRect olGanttRect;
		omGantt.GetWindowRect(&olGanttRect);

		if(olPoint.y <= olGanttRect.top && olPoint.y > (olGanttRect.top - 30))
		{
			// above the gantt so scroll up
			omGantt.SetTopIndex(omGantt.GetTopIndex()-1);
			bmScrolling = true;
		}
		else if(olPoint.y >= olGanttRect.bottom && olPoint.y < (olGanttRect.bottom + 30))
		{
			// below the gantt so scroll down
			omGantt.SetTopIndex(omGantt.GetTopIndex()+1);
			bmScrolling = true;
		}
	}

	if(bmScrolling)
	{
		if(imScrollSpeed != 50)
		{
			imScrollSpeed = 50;
			SetTimer(1, (UINT) 50, NULL);
		}
	}
	else
	{
		// no longer in the scrolling region or left mouse button released
		KillTimer(1);
	}
}

void CNewAssignConflictDlg::OnTimer(UINT nIDEvent)
{
	if(nIDEvent == 1)
	{
		// called during drag&drop, does automatic scrolling when the cursor
		// is outside the main gantt chart
		OnAutoScroll();
		CDialog::OnTimer(nIDEvent);
	}
}

int CNewAssignConflictDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	m_NewAssignConflictDlgDragDrop.RegisterTarget(this, this);

	
	return 0;
}

void CNewAssignConflictDlg::OnDestroy() 
{
	m_NewAssignConflictDlgDragDrop.Revoke();
	CDialog::OnDestroy();
}
