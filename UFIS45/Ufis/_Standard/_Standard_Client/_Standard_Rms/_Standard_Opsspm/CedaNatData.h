// Class for Flight nature Types
#ifndef _CEDANATDATA_H_
#define _CEDANATDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct NatDataStruct
{
	long	Urno;		// Unique Record Number
	char	Ttyp[6];	// type
	char	Tnam[32];	// name

	NatDataStruct(void)
	{
		Urno = 0L;
		strcpy(Ttyp,"");
		strcpy(Tnam,"");
	}
};

typedef struct NatDataStruct NATDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaNatData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <NATDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omTtypMap;
	CMapStringToPtr omTnamMap;
	CString GetTableName(void);

// Operations
public:
	CedaNatData();
	~CedaNatData();

	CCSReturnCode ReadNatData();
	NATDATA* GetNatByUrno(long lpUrno);

	NATDATA* GetNatByTtyp(const char *pcpTtyp);
	NATDATA* GetNatByTnam(const char *pcpTnam);
	void	 GetAllNatures(CStringArray& ropTypes);
	int		 GetCountOfRecords() { return omData.GetSize(); }
private:
	void AddNatInternal(NATDATA &rrpNat);
	void ClearAll();
};


extern CedaNatData ogNatData;
#endif _CEDAALTDATA_H_
