// CedaDpxData.cpp: Auto assignment parameters for line maintenence (Reduktionstufen)
//
//////////////////////////////////////////////////////////////////////

#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaDpxData.h>
#include <BasicData.h>
#include <PrmConfig.h>

//igu
#include <ButtonList.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
void  ProcessDpxCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaDpxData::CedaDpxData()
{                  
    BEGIN_CEDARECINFO(DPXDATA, DPXDATARecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Prid,"PRID")
		FIELD_CHAR_TRIM(Name,"NAME")
		FIELD_CHAR_TRIM(Natl,"NATL")
		FIELD_CHAR_TRIM(Lang,"LANG")
		FIELD_CHAR_TRIM(Gend,"GEND")
		FIELD_CHAR_TRIM(Mail,"MAIL")
		FIELD_CHAR_TRIM(Owhc,"OWHC")
		FIELD_LONG(Flnu,"FLNU")
		FIELD_DATE(Fdat,"FDAT")
		FIELD_LONG(Tflu,"TFLU")
		FIELD_DATE(Abdt,"ABDT")
		FIELD_CHAR_TRIM(Absr,"ABSR")
		FIELD_CHAR_TRIM(Aloc,"ALOC")
		FIELD_DATE(Sati,"SATI")
		FIELD_DATE(Aati,"AATI")
		FIELD_CHAR_TRIM(Prmt,"PRMT")
		FIELD_LONG(Tlxu,"TLXU")
		FIELD_CHAR_TRIM(Seat,"SEAT")
		FIELD_CHAR_TRIM(Usec,"USEC")
		FIELD_CHAR_TRIM(Useu,"USEU")
		FIELD_DATE(Cdat,"CDAT")
		FIELD_DATE(Lstu,"LSTU")
		FIELD_DATE(Clos,"CLOS")
		FIELD_CHAR_TRIM(Filt,"FILT")
		FIELD_CHAR_TRIM(Rema,"REMA")

    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(DPXDATARecInfo)/sizeof(DPXDATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DPXDATARecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"DPXTAB");
	if (bgShowPrmClos)
	{
		strcpy(cgFieldList,"URNO,PRID,NAME,NATL,LANG,GEND,MAIL,OWHC,FLNU,FDAT,TFLU,ABDT,ABSR,ALOC,SATI,AATI,PRMT,TLXU,SEAT,USEC,USEU,CDAT,LSTU,CLOS,FILT");
	}
	else
	{
		strcpy(cgFieldList,"URNO,PRID,NAME,NATL,LANG,GEND,MAIL,OWHC,FLNU,FDAT,TFLU,ABDT,ABSR,ALOC,SATI,AATI,PRMT,TLXU,SEAT,USEC,USEU,CDAT,LSTU,FILT");
	}
	
	// lli: add in REMA field if this field exists in DB
	if (bgPrmUseRema&&ogBasicData.DoesFieldExist("DPX", "REMA"))
	{
		strcat(cgFieldList,",REMA");
	}

	pcmFieldList = cgFieldList;

	ogCCSDdx.Register((void *)this,BC_DPX_NEW,CString("DPXDATA"), CString("BC Dpx changed"),ProcessDpxCf);
	ogCCSDdx.Register((void *)this,BC_DPX_CHANGE,CString("DPXDATA"), CString("BC Dpx changed"),ProcessDpxCf);
	ogCCSDdx.Register((void *)this,BC_DPX_DELETE,CString("DPXDATA"), CString("BC Dpx deleted"),ProcessDpxCf);
	ogCCSDdx.Register((void *)this,FLIGHT_CHANGE,CString("DPXDATA"), CString("Flight Change"),ProcessDpxCf);

}
 
CedaDpxData::~CedaDpxData()
{
	TRACE("CedaDpxData::~CedaDpxData called\n");
	ClearAll();
}

void CedaDpxData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omNameMap.RemoveAll();
	omPridMap.RemoveAll();
	omUprmMap.RemoveAll();

	POSITION rlPos;
	CMapPtrToPtr *pomFlightJobMap;
	for ( rlPos =  omFlightMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omFlightMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )pomFlightJobMap);
		pomFlightJobMap->RemoveAll();
		delete pomFlightJobMap;
	}
	omFlightMap.RemoveAll();

	omData.DeleteAll();
}

CCSReturnCode CedaDpxData::ReadDpxData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();
	CCSPtrArray <DPXDATA> olData;

	char pclCom[10] = "RT";
    char clSelection[512] = "";

	if (ogBasicData.IsPrmHandlingAgentEnabled())
	{
		sprintf(clSelection," WHERE FILT = '%s' ",ogPrmConfig.getHandlingAgentShortName());
	}
    
	if((ilRc = CedaAction2(pclCom, clSelection)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaDpxData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,clSelection);
	}
	else
	{
		DPXDATA rlDPXDATA;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlDPXDATA)) == RCSuccess)
			{
				ConvertDatesToLocal(&rlDPXDATA);
				DPXDATA* prlDPXDATA  = new DPXDATA;
				*prlDPXDATA = rlDPXDATA;
				olData.Add(prlDPXDATA);
			}
		}
		int ilNumDpxs = olData.GetSize();
		for(int ilDpx = 0; ilDpx < ilNumDpxs; ilDpx++)
		{
			DPXDATA rlDpx = olData.GetAt(ilDpx);
			AddDpxInternal(&rlDpx);
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaDpxData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(DPXDATA), clSelection);
    return ilRc;
}

CCSReturnCode CedaDpxData::ReadDpxData(CTime start, CTime end)
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();
	CCSPtrArray <DPXDATA> olData;

	char pclCom[10] = "RT";
    char pclSelection[1512];

	if (bgShowPrmClos)
	{
		strcpy(cgFieldList,"URNO,PRID,NAME,NATL,LANG,GEND,MAIL,OWHC,FLNU,FDAT,TFLU,ABDT,ABSR,ALOC,SATI,AATI,PRMT,TLXU,SEAT,USEC,USEU,CDAT,LSTU,CLOS,FILT");
	}
	else
	{
		strcpy(cgFieldList,"URNO,PRID,NAME,NATL,LANG,GEND,MAIL,OWHC,FLNU,FDAT,TFLU,ABDT,ABSR,ALOC,SATI,AATI,PRMT,TLXU,SEAT,USEC,USEU,CDAT,LSTU,FILT");
	}

	// lli: read REMA column if it exists in DB
	if (bgPrmUseRema&&ogBasicData.DoesFieldExist("DPX", "REMA"))
	{
		strcat(cgFieldList,",REMA");
	}

	if (ogBasicData.IsPrmHandlingAgentEnabled())
	{
		sprintf(pclSelection, "WHERE ((SATI between '%s' and '%s') OR (FDAT between '%s' and '%s')) AND FILT = '%s'", 
			start.Format("%Y%m%d%H%M%S"), end.Format("%Y%m%d%H%M%S"),
			start.Format("%Y%m%d%H%M%S"), end.Format("%Y%m%d%H%M%S"),
				ogPrmConfig.getHandlingAgentShortName());
		strcat(cgFieldList,",FILT");
	}
	else
	{
		sprintf(pclSelection, "WHERE (SATI between '%s' and '%s') OR (FDAT between '%s' and '%s')", start.Format("%Y%m%d%H%M%S"), end.Format("%Y%m%d%H%M%S"),start.Format("%Y%m%d%H%M%S"), end.Format("%Y%m%d%H%M%S"));
	}




    if((ilRc = CedaAction2(pclCom, pclSelection)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaDpxData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclSelection);
	}
	else
	{
		DPXDATA rlDPXDATA;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlDPXDATA)) == RCSuccess)
			{
				ConvertDatesToLocal(&rlDPXDATA);
				DPXDATA* prlDPXDATA  = new DPXDATA;
				*prlDPXDATA = rlDPXDATA;
				olData.Add(prlDPXDATA);
			}
		}
		int ilNumDpxs = olData.GetSize();
		for(int ilDpx = 0; ilDpx < ilNumDpxs; ilDpx++)
		{
			DPXDATA rlDpx = olData.GetAt(ilDpx);
			AddDpxInternal(&rlDpx);

		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaDpxData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(DPXDATA), pclSelection);
    return ilRc;
}

void CedaDpxData::AddDpxRecord(DPXDATA *prpData,BOOL bpSendDDX)
{
	if (prpData->Urno == 0L)
	{
		prpData->Urno = ogBasicData.GetNextUrno();
	}

	AddDpxInternal(prpData);
	DbInsert(prpData);
}

void CedaDpxData::ChangeDpxRecord(DPXDATA *prpData,BOOL bpSendDDX)
{

	if (prpData->Urno == 0L)
	{
		prpData->Urno = ogBasicData.GetNextUrno();
	}

	DPXDATA *prlOldDpx = GetDpxByUrno(prpData->Urno);
	char prlTmpName[34];
	char prlTmpRema[258];
	strcpy(prlTmpName,prlOldDpx->Name);
	strcpy(prlTmpRema,prlOldDpx->Rema);
	if (prlOldDpx != NULL)
	{
		UpdateMaps(prpData,prlOldDpx);
	}
	*prlOldDpx = *prpData;
	DbUpdate(prlOldDpx);
	// lli: send a PRM_JOB_CHANGE broadcast to change the display name for related PRM jobs
	if (strcmp(prlTmpName,prpData->Name))
	{
		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetJobsByUprm(olJobs, prlOldDpx->Urno);
		int ilNumJobs = olJobs.GetSize();
		for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
		{
			ogCCSDdx.DataChanged((void *)this,PRM_JOB_CHANGE,(void *)&olJobs[ilJob]); //Update Viewer
		}
	}
	// lli: after the DPX data is updated, we need to send PRM_DEMAND_CHANGE broadcast for related PRM demands
	if (strcmp(prlTmpName,prpData->Name)||strcmp(prlTmpRema,prpData->Rema))
	{
		CCSPtrArray <DEMANDDATA> olDemands;
		ogDemandData.GetDemandsByUprm(olDemands, prlOldDpx->Urno);
		int ilNumDemands = olDemands.GetSize();
		for(int ilDemand = 0; ilDemand < ilNumDemands; ilDemand++)
		{
			ogCCSDdx.DataChanged((void *)this,PRM_DEMAND_CHANGE,(void *)&olDemands[ilDemand]); //Update Viewer
		}
	}
}


void CedaDpxData::AddDpxInternal(DPXDATA *prpDpx)
{

	if(prpDpx == NULL)
	{
		return;
	}
	if(GetDpxByUrno(prpDpx->Urno) != NULL) // DPX already in mem - can happen when checking for missing demands
		return;
	DPXDATA *prlDpx = new DPXDATA;
	*prlDpx = *prpDpx;
	if ((prlDpx->Flnu != 0L))
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Flnu);
		if (prlFlight == NULL)
		{
			ogFlightData.HandleMissingFlight(prlDpx->Flnu);
			prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Flnu);
		}
		if (prlFlight != NULL)
		{
			if (*prlFlight->Adid == 'A')
			{
				prlDpx->Fdat = prlFlight->Tifa;
			}
			else
			{
				prlDpx->Fdat = prlFlight->Tifd;
			}
			strcpy ( prlDpx->FlnuFlno,prlFlight->Flno);
			if (strlen(prlFlight->Alc2) > 0)
				strcpy(prlDpx->FlnuAlc,prlFlight->Alc2);
			else
				strcpy(prlDpx->FlnuAlc,prlFlight->Alc3);
			strcpy(prlDpx->FlnuNumber,prlFlight->Fltn);
			strcpy(prlDpx->FlnuSuffix,prlFlight->Flns);
			prlDpx->FlnuAdid = *prlFlight->Adid;
			strcpy(prlDpx->FlnuPrmu,prlFlight->Prmu);
			strcpy(prlDpx->FlnuPrms,prlFlight->Prms);

			CMapPtrToPtr *polFlightJobMap;
			if (omFlightMap.Lookup((void *) prlDpx->Flnu, (void *&) polFlightJobMap) == TRUE)
			{
				polFlightJobMap->SetAt((void *)prlDpx->Urno,prlDpx);
			}
			else
			{
				polFlightJobMap = new CMapPtrToPtr;
				polFlightJobMap->SetAt((void *)prlDpx->Urno,prlDpx);
				omFlightMap.SetAt((void *)prlDpx->Flnu,polFlightJobMap);
			}
		}
	}

	if ((prlDpx->Tflu != 0L))
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Tflu);
		if (prlFlight == NULL)
		{
			ogFlightData.HandleMissingFlight(prlDpx->Tflu);
			prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Tflu);
		}
		if (prlFlight != NULL)
		{
			strcpy ( prlDpx->TfluFlno,prlFlight->Flno);
			if (strlen(prlFlight->Alc2) > 0)
				strcpy(prlDpx->TfluAlc,prlFlight->Alc2);
			else
				strcpy(prlDpx->TfluAlc,prlFlight->Alc3);
			strcpy(prlDpx->TfluNumber,prlFlight->Fltn);
			strcpy(prlDpx->TfluSuffix,prlFlight->Flns);
			prlDpx->TfluAdid = *prlFlight->Adid;
			strcpy(prlDpx->TfluPrmu,prlFlight->Prmu);
			strcpy(prlDpx->TfluPrms,prlFlight->Prms);

			CMapPtrToPtr *polFlightJobMap;
			if (omFlightMap.Lookup((void *) prlDpx->Tflu, (void *&) polFlightJobMap) == TRUE)
			{
				polFlightJobMap->SetAt((void *)prlDpx->Urno,prlDpx);
			}
			else
			{
				polFlightJobMap = new CMapPtrToPtr;
				polFlightJobMap->SetAt((void *)prlDpx->Urno,prlDpx);
				omFlightMap.SetAt((void *)prlDpx->Tflu,polFlightJobMap);
			}
		}
	}

	omData.Add(prlDpx);
	omUrnoMap.SetAt((void *)prlDpx->Urno,prlDpx);
	omNameMap.SetAt(prlDpx->Name,prlDpx);
	omPridMap.SetAt(prlDpx->Prid,prlDpx);
	if(prlDpx->Urno != 0L && strcmp(prlDpx->Absr,ogBasicData.GetConfirmedPSMEntry()) == 0)
	{
		omUprmMap.SetAt((void *)prlDpx->Urno,prlDpx);
	}
}

bool CedaDpxData::DbInsert(DPXDATA *prpDpx)
{
	CCSReturnCode ilRc = RCSuccess;

		char pclSelection[124] = "";
		CString olData;
		char pclCmd[10] = "IRT";
		char pclData[2000];
		DPXDATA *prlDpx = new DPXDATA;

		*prlDpx = *prpDpx;

		if (ogBasicData.IsPrmHandlingAgentEnabled())
		{
			strcpy(prpDpx->Filt,ogPrmConfig.getHandlingAgentShortName());
		}

		//PrepareDataForWrite(prpJob);
		ConvertDatesToUtc(prlDpx);
		//GetNewFields(prpJob, pclInsertFieldList, pclData);
		//GetAllFields(prpJob, pclInsertFieldList, pclData);

		//ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pclInsertFieldList, pclData);
		MakeCedaData(&omRecInfo,olData,prlDpx);
		strcpy(pclData,olData);
		ilRc = CedaAction(pclCmd,pclSelection ,"", pclData);
		if (ilRc != RCSuccess)
		{
			ogBasicData.LogCedaError("CedaPcxData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
		else
		{
			ogCCSDdx.DataChanged((void *)this,DPX_NEW,(void *)prpDpx);
		}


	return ilRc;
}

bool CedaDpxData::DbUpdate(DPXDATA *prpDpx)
{
	CCSReturnCode ilRc = RCSuccess;

		char pclSelection[124] = "";
		CString olData;
		char pclCmd[10] = "URT";
		char pclData[2000];
		
		DPXDATA *prlDpx = new DPXDATA;

		*prlDpx = *prpDpx;

	if (prlDpx->Flnu != 0L)
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Flnu);
		if (prlFlight == NULL)
		{
			ogFlightData.HandleMissingFlight(prlDpx->Flnu);
			prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Flnu);
		}
		if (prlFlight != NULL)
		{
			if (*prlFlight->Adid == 'A')
			{
				prlDpx->Fdat = prlFlight->Tifa;
			}
			else
			{
				prlDpx->Fdat = prlFlight->Tifd;
			}
		}
	}

		//ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pclInsertFieldList, pclData);
		ConvertDatesToUtc(prlDpx);
		sprintf(pclSelection,"WHERE URNO=%ld",prpDpx->Urno);
		MakeCedaData(&omRecInfo,olData,prlDpx);
		strcpy(pclData,olData);
		ilRc = CedaAction(pclCmd,pclSelection ,"", pclData);
		if (ilRc != RCSuccess)
		{
			ogBasicData.LogCedaError("CedaPcxData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
		else
		{
			ogCCSDdx.DataChanged((void *)this,DPX_CHANGE,(void *)prpDpx);
		}

	
	return ilRc;
}

BOOL CedaDpxData::DeleteDpxRecord(DPXDATA *prpOldDpx,BOOL IsFromBC) 
{

	BOOL blRC = TRUE;

	if (!IsFromBC)
	{
		blRC = DbDelete(prpOldDpx);
	}
	long tmpUrno = prpOldDpx->Urno;
	omPridMap.RemoveKey((void * ) prpOldDpx->Prid);
	omNameMap.RemoveKey((void * ) prpOldDpx->Name);
    omUrnoMap.RemoveKey((void * ) prpOldDpx->Urno);
	omUprmMap.RemoveKey((void * ) prpOldDpx->Urno);
	CMapPtrToPtr *pomFlightJobMap;
	if (omFlightMap.Lookup((void *) prpOldDpx->Flnu, (void *&) pomFlightJobMap) == TRUE)
	{
		pomFlightJobMap->RemoveKey((void * ) tmpUrno);
	}
	if (omFlightMap.Lookup((void *) prpOldDpx->Tflu, (void *&) pomFlightJobMap) == TRUE)
	{
		pomFlightJobMap->RemoveKey((void * ) tmpUrno);
	}
	int dpxCount = omData.GetSize();
	for (int i = 0; i < dpxCount; i++)
	{
		DPXDATA rlDpx = omData.GetAt(i);
		if (prpOldDpx->Urno == rlDpx.Urno)
		{
			omData.DeleteAt(i);
			break;
		}
	}
	// lli: send PRM_JOB_CHANGE broadcast to related PRM jobs
	CCSPtrArray <JOBDATA> olJobs;
	ogJobData.GetJobsByUprm(olJobs, tmpUrno);
	int ilNumJobs = olJobs.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		ogCCSDdx.DataChanged((void *)this,PRM_JOB_CHANGE,(void *)&olJobs[ilJob]); //Update Viewer
	}
	// lli: after the DPX data is updated, we need to send PRM_DEMAND_CHANGE broadcast for related PRM demands
	CCSPtrArray <DEMANDDATA> olDemands;
	ogDemandData.GetDemandsByUprm(olDemands, tmpUrno);
	int ilNumDemands = olDemands.GetSize();
	for(int ilDemand = 0; ilDemand < ilNumDemands; ilDemand++)
	{
		ogCCSDdx.DataChanged((void *)this,PRM_DEMAND_CHANGE,(void *)&olDemands[ilDemand]); //Update Viewer
	}
	return blRC;
}

bool CedaDpxData::DbDelete(DPXDATA *prpDpx)
{
	CCSReturnCode ilRc = RCSuccess;

		char pclSelection[124] = "";
		CString olData;
		char pclCmd[10] = "DRT";
		char pclData[2000];
		

		//ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pclInsertFieldList, pclData);
		sprintf(pclSelection,"WHERE URNO=%ld",prpDpx->Urno);
		MakeCedaData(&omRecInfo,olData,prpDpx);
		strcpy(pclData,olData);
		ilRc = CedaAction(pclCmd,pclSelection ,"", pclData);
		if (ilRc != RCSuccess)
		{
			ogBasicData.LogCedaError("CedaPcxData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
		else
		{
			ogCCSDdx.DataChanged((void *)this,DPX_DELETE,(void *)prpDpx);
		}

	
 return ilRc;
}

void CedaDpxData::UpdateMaps(DPXDATA *prpDpx, DPXDATA *prpOldDpx)
{
	if(prpDpx != NULL && prpOldDpx != NULL)
	{
		if(prpOldDpx->Prid != prpDpx->Prid)
		{

			omPridMap.RemoveKey((void * ) prpOldDpx->Prid);
			omPridMap.SetAt((void *)prpDpx->Prid,prpDpx);
		}

		if(prpOldDpx->Flnu != prpDpx->Flnu)
		{

			CMapPtrToPtr *polFlightPrmMap;

			if (omFlightMap.Lookup((void *) prpOldDpx->Flnu, (void *&) polFlightPrmMap) == TRUE)
			{
				polFlightPrmMap->RemoveKey((void * ) prpDpx->Urno);
			}
			
			if (prpDpx->Flnu!=0L)
			{
				if (omFlightMap.Lookup((void *) prpDpx->Flnu, (void *&) polFlightPrmMap) == TRUE)
				{
					polFlightPrmMap->SetAt((void *)prpDpx->Urno,prpDpx);
				}
				else
				{
					polFlightPrmMap = new CMapPtrToPtr;
					polFlightPrmMap->SetAt((void *)prpDpx->Urno,prpDpx);
					omFlightMap.SetAt((void *)prpDpx->Flnu,polFlightPrmMap);
				}
			}
		}

		if(prpOldDpx->Tflu != prpDpx->Tflu)
		{

			CMapPtrToPtr *polFlightPrmMap;

			if (omFlightMap.Lookup((void *) prpOldDpx->Tflu, (void *&) polFlightPrmMap) == TRUE)
			{
				polFlightPrmMap->RemoveKey((void * ) prpDpx->Urno);
			}
			
			if (prpDpx->Tflu!=0L)
			{
				if (omFlightMap.Lookup((void *) prpDpx->Tflu, (void *&) polFlightPrmMap) == TRUE)
				{
					polFlightPrmMap->SetAt((void *)prpDpx->Urno,prpDpx);
				}
				else
				{
					polFlightPrmMap = new CMapPtrToPtr;
					polFlightPrmMap->SetAt((void *)prpDpx->Urno,prpDpx);
					omFlightMap.SetAt((void *)prpDpx->Tflu,polFlightPrmMap);
				}
			}
		}

		if(strcmp(prpDpx->Name,prpOldDpx->Name))
		{
			omNameMap.RemoveKey((void * ) prpOldDpx->Name);
			omNameMap.SetAt((void *)prpDpx->Name,prpDpx);
		}
	}
}	

DPXDATA* CedaDpxData::GetDpxByUrno(long lpUrno)
{
	DPXDATA *prlDpx = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlDpx);
	return prlDpx;
}

DPXDATA* CedaDpxData::GetDpxByName(const char *pcpName)
{
	DPXDATA *prlDpx = NULL;
	omNameMap.Lookup((void *)pcpName, (void *&) prlDpx);
	return prlDpx;
}

long CedaDpxData::GetDpxUrnoByName(const char *pcpName)
{
	long llUrno = 0L;
	DPXDATA *prlDpx = GetDpxByName(pcpName);
	if(prlDpx != NULL)
	{
		llUrno = prlDpx->Urno;
	}
	return llUrno;
}

BOOL CedaDpxData::IsConfirmedPSM(long lpUrno)
{	
	DPXDATA* prlDpx = NULL;
	return omUprmMap.Lookup((void *)lpUrno, (void *&) prlDpx);
}

CString CedaDpxData::GetTableName(void)
{
	return CString(pcmTableName);
}

void CedaDpxData::ConvertDatesToLocal(DPXDATA *prpDpx)
{
	if(prpDpx != NULL)
	{
		ogBasicData.ConvertDateToLocal(prpDpx->Abdt);
		ogBasicData.ConvertDateToLocal(prpDpx->Sati);
		ogBasicData.ConvertDateToLocal(prpDpx->Aati);
		ogBasicData.ConvertDateToLocal(prpDpx->Cdat);
		ogBasicData.ConvertDateToLocal(prpDpx->Lstu);
		ogBasicData.ConvertDateToLocal(prpDpx->Clos);
		ogBasicData.ConvertDateToLocal(prpDpx->Fdat); // lli: need to convert the time for FDAT field as well
	}
}
void CedaDpxData::ConvertDatesToUtc(DPXDATA *prpDpx)
{
	if(prpDpx != NULL)
	{
		ogBasicData.ConvertDateToUtc(prpDpx->Abdt);
		ogBasicData.ConvertDateToUtc(prpDpx->Sati);
		ogBasicData.ConvertDateToUtc(prpDpx->Aati);
		ogBasicData.ConvertDateToUtc(prpDpx->Cdat);
		ogBasicData.ConvertDateToUtc(prpDpx->Fdat);

		CString vorher = prpDpx->Lstu.Format("%H:%M");
		ogBasicData.ConvertDateToUtc(prpDpx->Lstu);
		CString nachher = prpDpx->Lstu.Format("%H:%M");
		ogBasicData.ConvertDateToUtc(prpDpx->Clos);
	}
}

//igu	
BOOL CedaDpxData::IsViewLocked()
{
	BOOL bRet = FALSE;
	
	if (pogButtonList->m_wndPrmTable != NULL)
		bRet = (pogButtonList->m_wndPrmTable->IsViewLocked());
	
	return bRet;
}

void CedaDpxData::ProcessDpxInsert(void *vpDataPointer)
{
	struct BcStruct *prlDpxData;
	prlDpxData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlDpxData);

	DPXDATA rlDpx;
	GetRecordFromItemList(&omRecInfo,&rlDpx,prlDpxData->Fields,prlDpxData->Data);
	if (ogBasicData.IsPrmHandlingAgentEnabled())
	{
		if(strcmp(ogPrmConfig.getHandlingAgentShortName(),rlDpx.Filt) != 0)
		{
			return;
		}
	}
    

	ConvertDatesToLocal(&rlDpx);
	AddDpxInternal(&rlDpx);	
	
	DPXDATA *prlDpx = GetDpxByUrno(rlDpx.Urno);

	if (prlDpx != NULL)
	{
		//igu
		if (pogButtonList->m_wndPrmTable != NULL)
			pogButtonList->m_wndPrmTable->SetDpx(prlDpx);
		if (IsViewLocked())
			pogButtonList->m_wndPrmTable->SetDpx(NULL);
		else
		{
			if (pogButtonList->m_wndPrmTable != NULL)
				if (pogButtonList->m_wndPrmTable->IsPassFilter(prlDpx))
					ogCCSDdx.DataChanged((void *)this,DPX_NEW,(void *)prlDpx);
		}
	}
}

void CedaDpxData::ProcessDpxUpdate(void *vpDataPointer)
{
	struct BcStruct *prlDpxData;
	prlDpxData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlDpxData);

	long llUrno = GetUrnoFromSelection(prlDpxData->Selection);
	if(llUrno == 0L)
			{
		DPXDATA rlDpx;
		GetRecordFromItemList(&omRecInfo,&rlDpx,prlDpxData->Fields,prlDpxData->Data);
		llUrno = rlDpx.Urno;
		ConvertDatesToLocal(&rlDpx);
	}

	if(llUrno != 0L)
	{
		DPXDATA *prlDpx = GetDpxByUrno(llUrno);
		// lli: we need to update the FLNU and TFLU related fields when DPX data is updated
		if (prlDpx != NULL)
		{
			DPXDATA rlOldDpx = *prlDpx;
			GetRecordFromItemList(&omRecInfo,prlDpx, prlDpxData->Fields, prlDpxData->Data);
			ConvertDatesToLocal(prlDpx);
			if ((prlDpx->Flnu != 0L))
			{
				FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Flnu);
				if (prlFlight == NULL)
				{
					ogFlightData.HandleMissingFlight(prlDpx->Flnu);
					prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Flnu);
				}
				if (prlFlight != NULL)
				{
					if (*prlFlight->Adid == 'A')
					{
						prlDpx->Fdat = prlFlight->Tifa;
					}
					else
					{
						prlDpx->Fdat = prlFlight->Tifd;
					}
					strcpy ( prlDpx->FlnuFlno,prlFlight->Flno);
					if (strlen(prlFlight->Alc2) > 0)
						strcpy(prlDpx->FlnuAlc,prlFlight->Alc2);
					else
						strcpy(prlDpx->FlnuAlc,prlFlight->Alc3);
					strcpy(prlDpx->FlnuNumber,prlFlight->Fltn);
					strcpy(prlDpx->FlnuSuffix,prlFlight->Flns);
					prlDpx->FlnuAdid = *prlFlight->Adid;
					strcpy(prlDpx->FlnuPrmu,prlFlight->Prmu);
					strcpy(prlDpx->FlnuPrms,prlFlight->Prms);

				}
			}

			if ((prlDpx->Tflu != 0L))
			{
				FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Tflu);
				if (prlFlight == NULL)
				{
					ogFlightData.HandleMissingFlight(prlDpx->Tflu);
					prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Tflu);
				}
				if (prlFlight != NULL)
				{
					strcpy ( prlDpx->TfluFlno,prlFlight->Flno);
					if (strlen(prlFlight->Alc2) > 0)
						strcpy(prlDpx->TfluAlc,prlFlight->Alc2);
					else
						strcpy(prlDpx->TfluAlc,prlFlight->Alc3);
					strcpy(prlDpx->TfluNumber,prlFlight->Fltn);
					strcpy(prlDpx->TfluSuffix,prlFlight->Flns);
					prlDpx->TfluAdid = *prlFlight->Adid;
					strcpy(prlDpx->TfluPrmu,prlFlight->Prmu);
					strcpy(prlDpx->TfluPrms,prlFlight->Prms);
				}
			}
			UpdateMaps(prlDpx,&rlOldDpx);

			if (!IsViewLocked()) //igu
				ogCCSDdx.DataChanged((void *)this,DPX_CHANGE,(void *)prlDpx);

			// lli: after the DPX data is updated, we need to send PRM_JOB_CHANGE broadcast for related PRM jobs
			if (strcmp(rlOldDpx.Name,prlDpx->Name))
			{
				CCSPtrArray <JOBDATA> olJobs;
				ogJobData.GetJobsByUprm(olJobs, rlOldDpx.Urno);
				int ilNumJobs = olJobs.GetSize();
				for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
				{
					if (!IsViewLocked()) //igu
						ogCCSDdx.DataChanged((void *)this,PRM_JOB_CHANGE,(void *)&olJobs[ilJob]); //Update Viewer
				}
			}
			// lli: after the DPX data is updated, we need to send PRM_DEMAND_CHANGE broadcast for related PRM demands
			if (strcmp(rlOldDpx.Name,prlDpx->Name)||strcmp(rlOldDpx.Rema,prlDpx->Rema))
			{
				CCSPtrArray <DEMANDDATA> olDemands;
				ogDemandData.GetDemandsByUprm(olDemands, rlOldDpx.Urno);
				int ilNumDemands = olDemands.GetSize();
				for(int ilDemand = 0; ilDemand < ilNumDemands; ilDemand++)
				{
					if (!IsViewLocked()) //igu
						ogCCSDdx.DataChanged((void *)this,PRM_DEMAND_CHANGE,(void *)&olDemands[ilDemand]); //Update Viewer
				}
			}
		}
	}
}
 
void CedaDpxData::ProcessDpxDelete(void *vpDataPointer)
{

	struct BcStruct *prlDpxData;
	prlDpxData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlDpxData);

	long llUrno = GetUrnoFromSelection(prlDpxData->Selection);
	if(llUrno == 0L)
			{
		DPXDATA rlDpx;
		GetRecordFromItemList(&omRecInfo,&rlDpx,prlDpxData->Fields,prlDpxData->Data);
		llUrno = rlDpx.Urno;
	}
	
	if(llUrno != 0L)
	{
		DPXDATA *prlDpx = GetDpxByUrno(llUrno);
		if (prlDpx != NULL)
		{
			if (!IsViewLocked()) //igu				
				ogCCSDdx.DataChanged((void *)this,DPX_DELETE,(void *)prlDpx);
			DeleteDpxRecord(prlDpx,true) ;
		}
		else
		{
			// seems to be a FLNU, not a DPX.URNO
			int dpxCount = omData.GetSize();
			for (int i = dpxCount-1; i >= 0; i--)
			{
				DPXDATA rlDpx = omData.GetAt(i);
				if (llUrno == rlDpx.Flnu)
				{
					if (!IsViewLocked()) //igu
						ogCCSDdx.DataChanged((void *)this,DPX_DELETE,(void *)&rlDpx);
					DeleteDpxRecord(&rlDpx,true) ;
				}
			}

		}
	}
}

void CedaDpxData::ProcessFlightChange(FLIGHTDATA *prpFlight)
{
	if(prpFlight != NULL)
	{
		CCSPtrArray <DPXDATA> olTmpDpxs;
		ogDpxData.GetDpxsByFlight(prpFlight->Urno,olTmpDpxs,true);
		for(int ilFJ = 0; ilFJ < olTmpDpxs.GetSize(); ilFJ++)
		{
			DPXDATA *prlDpx = &olTmpDpxs[ilFJ];
			if (prlDpx != NULL)
			{
				if (prlDpx->Flnu == prpFlight->Urno)
				{
					if(strcmp(prlDpx->FlnuPrmu,prpFlight->Prmu)||strcmp(prlDpx->FlnuPrms,prpFlight->Prms))
					{
						strcpy(prlDpx->FlnuPrmu,prpFlight->Prmu);
						strcpy(prlDpx->FlnuPrms,prpFlight->Prms);

						if (!IsViewLocked()) //igu
							ogCCSDdx.DataChanged((void *)this,DPX_CHANGE,(void *)prlDpx);	
					}
				}
				else if (prlDpx->Tflu == prpFlight->Urno)
				{
					if(strcmp(prlDpx->TfluPrmu,prpFlight->Prmu)||strcmp(prlDpx->TfluPrms,prpFlight->Prms))
					{
						strcpy(prlDpx->TfluPrmu,prpFlight->Prmu);
						strcpy(prlDpx->TfluPrms,prpFlight->Prms);

						if (!IsViewLocked()) //igu
							ogCCSDdx.DataChanged((void *)this,DPX_CHANGE,(void *)prlDpx);	
					}
				}
			}
		}
	}
}


void ProcessDpxCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_DPX_NEW:
		((CedaDpxData *)popInstance)->ProcessDpxInsert(vpDataPointer);
		break;
	case BC_DPX_CHANGE:
		((CedaDpxData *)popInstance)->ProcessDpxUpdate(vpDataPointer);
		break;
	case BC_DPX_DELETE:
		((CedaDpxData *)popInstance)->ProcessDpxDelete(vpDataPointer);
		break;
	case FLIGHT_CHANGE:
		((CedaDpxData *)popInstance)->ProcessFlightChange((FLIGHTDATA *) vpDataPointer);
		break;
	default:
		break;
	}
}

void CedaDpxData::GetDpxTypesByFlight(long lpAftu , CStringArray &ropDpxTypes, bool bpReset)
{
	
	CString olDpxType;
	olDpxType.Empty();
	if (bpReset)
	{
		ropDpxTypes.RemoveAll();
	}

	CCSPtrArray <DPXDATA> olTmpDpxs;
	GetDpxsByFlight(lpAftu,olTmpDpxs,true);
	for(int ilFJ = 0; ilFJ < olTmpDpxs.GetSize(); ilFJ++)
	{
		DPXDATA *prlTmpDpx = &olTmpDpxs[ilFJ];
		if (olDpxType.Find(prlTmpDpx->Prmt)  == -1)
		{
			olDpxType.Insert(olDpxType.GetLength(),prlTmpDpx->Prmt); 
			olDpxType.Insert(olDpxType.GetLength(),+ ',');
			CString newType;
			newType.Format("%d - %s",1,prlTmpDpx->Prmt);
			ropDpxTypes.Add(newType);
		}
		else
		{
			for (int ilType = 0; ilType < ropDpxTypes.GetSize(); ilType++)
			{
				if (ropDpxTypes[ilType].Find(prlTmpDpx->Prmt) > -1)
				{
					int ilCount = atoi(ropDpxTypes[ilType]);
					CString newType;
					newType.Format("%d - %s",++ilCount,prlTmpDpx->Prmt);
					ropDpxTypes[ilType] = newType;
				}
			}
		}
	}

}

BOOL CedaDpxData::GetDpxsByFlight(long lpAftu , CCSPtrArray<DPXDATA>& ropDpxList, bool bpReset)
{
	
	DPXDATA *prlDpx;
	CMapPtrToPtr *pomFlightDpxMap;

	if (bpReset)
	{
		ropDpxList.RemoveAll();
	}

	if (omFlightMap.Lookup((void *)lpAftu,(void *& )pomFlightDpxMap) == TRUE)
	{
		for(POSITION rlPos =  pomFlightDpxMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			pomFlightDpxMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlDpx);
			ropDpxList.Add(prlDpx);
		}
	}

	return true;
}

DPXDATA CedaDpxData::ReadbyUrno(long urno)
{

   CCSReturnCode ilRc= RCSuccess;
   ClearAll();
   pcmFieldList = "USEC,USEU,CDAT,LSTU";

	char pclCom[10] = "RT";
    char pclWhere[512];

    sprintf(pclWhere,"WHERE URNO='%ld'",  urno );
  	DPXDATA dpxData;
	
	if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaDraData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
	
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&dpxData)) == RCSuccess)
			{
	                                       
               				
			}
		}

		ilRc = RCSuccess;
	}

    return dpxData;

}