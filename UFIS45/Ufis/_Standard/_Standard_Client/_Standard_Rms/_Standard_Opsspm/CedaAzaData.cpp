// AZATAB -
// CedaAzaData.cpp
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaAzaData.h>
#include <OpssPm.h>
#include <CCSDdx.h>
#include <BasicData.h>
#include <CCSBcHandle.h>

extern CCSDdx ogCCSDdx;
void  ProcessAzaCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaAzaData::CedaAzaData()
{                  
    BEGIN_CEDARECINFO(AZADATA, AzaDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Func,"FUNC") 
		FIELD_CHAR_TRIM(Squa,"SQUA")
        FIELD_CHAR_TRIM(Gqua,"GQUA")
		FIELD_DATE(Acbe,"ACBE")
		FIELD_DATE(Acen,"ACEN")
		FIELD_CHAR_TRIM(Acst,"ACST")
		FIELD_CHAR_TRIM(Act3,"ACT3")
		FIELD_CHAR_TRIM(Azso, "AZSO")
		FIELD_DATE(Cdat,"CDAT")
		FIELD_CHAR_TRIM(Usec, "USEC")
		FIELD_DATE(Lstu,"LSTU")
		FIELD_CHAR_TRIM(Useu, "USEU")
		FIELD_CHAR_TRIM(Nres,"NRES")
		FIELD_CHAR_TRIM(Poty,"POTY")
		FIELD_CHAR_TRIM(Prio,"PRIO")
		FIELD_CHAR_TRIM(Regn,"REGN")
		FIELD_CHAR_TRIM(Seco,"SECO")
		FIELD_CHAR_TRIM(Pkey,"PKEY")
		FIELD_CHAR_TRIM(Stat,"STAT")

    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(AzaDataRecInfo)/sizeof(AzaDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AzaDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"AZATAB");
    pcmFieldList = "URNO,FUNC,SQUA,GQUA,ACBE,ACEN,ACST,ACT3,AZSO,CDAT,USEC,LSTU,USEU,NRES,POTY,PRIO,REGN,SECO,PKEY,STAT";

	ogCCSDdx.Register((void *)this,BC_AZA_CHANGE,CString("AZADATA"), CString("Aza changed or inserted"),ProcessAzaCf);
	ogCCSDdx.Register((void *)this,BC_AZA_DELETE,CString("AZADATA"), CString("Aza changed or inserted"),ProcessAzaCf);
}
 
CedaAzaData::~CedaAzaData()
{
	TRACE("CedaAzaData::~CedaAzaData called\n");
	ClearAll();
	ogCCSDdx.UnRegister(this,NOTUSED);
}

void CedaAzaData::ClearAll()
{
	omUrnoMap.RemoveAll();
	int ilNumAzas = omData.GetSize();
	for(int ilAza = 0; ilAza < ilNumAzas; ilAza++)
	{
		omData[ilAza].FunctionAndPermits.DeleteAll();
	}
	omData.DeleteAll();
}

CCSReturnCode CedaAzaData::ReadAzaData(CTime opStartTime, CTime opEndTime)
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512] = "";
	sprintf(pclWhere,"WHERE ACBE <= '%s' AND ACEN >= '%s'",opEndTime.Format("%Y%m%d%H%M%S"),opStartTime.Format("%Y%m%d%H%M%S"));
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaAzaData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		AZADATA rlAzaData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlAzaData)) == RCSuccess)
			{
				AddAzaInternal(rlAzaData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaAzaData Read %d Records (Rec Size %d) %s",omData.GetSize(),sizeof(AZADATA), pclWhere);
    return ilRc;
}


AZADATA *CedaAzaData::AddAzaInternal(AZADATA &rrpAza)
{
	AZADATA *prlAza = new AZADATA;
	*prlAza = rrpAza;
//	prlAza->Urno = rrpAza.Urno;
//	strcpy(prlAza->Func,rrpAza.Func);
//	strcpy(prlAza->Squa,rrpAza.Squa);
//	strcpy(prlAza->Gqua,rrpAza.Gqua);
	PrepareData(prlAza);
	omData.Add(prlAza);
	omUrnoMap.SetAt((void *)prlAza->Urno,prlAza);
	return prlAza;
}

void CedaAzaData::PrepareData(AZADATA *prpAza)
{
	if(prpAza != NULL)
	{
		// insurance against overflow
		prpAza->Func[AZASTRLEN-1] = '\0';
		prpAza->Gqua[AZASTRLEN-1] = '\0';
		CreateFunctionAndPermitsList(prpAza);
	}
}

void CedaAzaData::CreateFunctionAndPermitsList(AZADATA *prpAza)
{
	if(prpAza != NULL)
	{
		prpAza->FunctionAndPermits.DeleteAll();

		CString olFunc, olPermits;
		char *prlFunc = prpAza->Func;
		char *prlPermits = prpAza->Squa;

		if(strlen(prlFunc) > 0)
		{
			bool blNotEndOfLine = true;
			while(blNotEndOfLine)
			{
				if(*prlFunc == STX || *prlFunc == '\0')
				{
					if(!olFunc.IsEmpty() || strlen(prlPermits) > 0)
					{
						FUNCTIONANDPERMITSDATA *prlFunctionAndPermits = new FUNCTIONANDPERMITSDATA;
						prpAza->FunctionAndPermits.Add(prlFunctionAndPermits);
						prlFunctionAndPermits->Function = olFunc;

						olFunc.Empty();
						olPermits.Empty();

						if(strlen(prlPermits) > 0)
						{
							bool blEndOfPermitsForThisFunc = false;
							while(*prlPermits != '\0' && !blEndOfPermitsForThisFunc)
							{
								if(*prlPermits == STX)
								{
									blEndOfPermitsForThisFunc = true;
								}
								else if(*prlPermits == SOH)
								{
									prlFunctionAndPermits->Permits.Add(olPermits);
									olPermits.Empty();
								}
								else
								{
									olPermits += *prlPermits;
								}
								prlPermits++;
							}
							if(!olPermits.IsEmpty())
							{
								prlFunctionAndPermits->Permits.Add(olPermits);
							}
						}
					}
				}
				else
				{
					olFunc += *prlFunc;
				}

				if(*prlFunc == '\0')
				{
					blNotEndOfLine = false;
				}
				prlFunc++;
			};
		}
	}
}

AZADATA *CedaAzaData::GetAzaByUrno(long lpUrno)
{
	AZADATA *prlAza = NULL;
	omUrnoMap.Lookup((void *)lpUrno,(void *& )prlAza);
	return prlAza;
}

void  ProcessAzaCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaAzaData *)popInstance)->ProcessAzaBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaAzaData::ProcessAzaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlAzaData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlAzaData);
	AZADATA rlAza;
	GetRecordFromItemList(&rlAza,prlAzaData->Fields,prlAzaData->Data);
	AZADATA *prlAza = GetAzaByUrno(rlAza.Urno);

	if(ipDDXType == BC_AZA_CHANGE && prlAza != NULL)
	{
		// update
		GetRecordFromItemList(prlAza,prlAzaData->Fields,prlAzaData->Data);
		PrepareData(prlAza);
	}
	else if(ipDDXType == BC_AZA_CHANGE && prlAza == NULL)
	{
		// insert
		prlAza = AddAzaInternal(rlAza);
	}
	else if(ipDDXType == BC_AZA_DELETE && prlAza != NULL)
	{
		DeleteAzaInternal(rlAza.Urno);
	}
}

CString CedaAzaData::GetTableName(void)
{
	return CString(pcmTableName);
}


bool CedaAzaData::Insert(AZADATA *prpAza)
{
	bool ilRc = true;
	CString olListOfData;
	char pclData[524];
	char pclSelection[124] = "";
	char pclCmd[10] = "IAZ";

	prpAza->Urno = ogBasicData.GetNextUrno();

	// I don't need this field but set it to the URNO because it is a unique field in DB
	sprintf(prpAza->Pkey,"%ld",prpAza->Urno);

	MakeCedaData(&omRecInfo,olListOfData,prpAza);
	strcpy(pclData,olListOfData);
	ilRc = CedaAction("IRT","","",pclData);

	if (ilRc != RCSuccess)
		return false;
	else
	{
		PrepareDataForWrite(prpAza, "P05RES");
		AddAzaInternal(*prpAza);
		// Send message to demhdl

		// send message to demand handler - requires the times in UTC
		AZADATA rlAza = *prpAza;
		ogBasicData.ConvertDateToUtc(rlAza.Acbe);
		ogBasicData.ConvertDateToUtc(rlAza.Acen);
		MakeCedaData(&omRecInfo,olListOfData,&rlAza);
		strcpy(pclData,olListOfData);

		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);
		ilRc = CedaAction(pclCmd,"","",pclData);
		if (ilRc != RCSuccess)
			return false;
		else
			return true;
	}
} 

bool CedaAzaData::InsertAzaForThirdParty(AZADATA *prpAza)
{
	bool blRc = false;
	if(prpAza != NULL)
	{
		CString olListOfData;
		char pclData[524];
		char pclSelection[124] = "";
		char pclCmd[10] = "IRT";

		prpAza->Urno = ogBasicData.GetNextUrno();

		// I don't need this field but set it to the URNO because it is a unique field in DB
		sprintf(prpAza->Pkey,"%ld",prpAza->Urno);

		PrepareDataForWrite(prpAza, "3rdParty");
		MakeCedaData(&omRecInfo,olListOfData,prpAza);
		strcpy(pclData,olListOfData);
		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);
		if((blRc = CedaAction(pclCmd,"","",pclData)) == true)
		{
			AddAzaInternal(*prpAza);
		}
	}

	return blRc;
}

void CedaAzaData::PrepareDataForWrite(AZADATA *prpAza, const char *pcpText)
{
	if(prpAza != NULL)
	{
		CTime olCurrTime = ogBasicData.GetTime();
		CString olUserText;
		olUserText.Format("%s %s %s",ogUsername,pcpText,ogBasicData.GetVersion()); // username + version number and function number (which function did the change)

		if(prpAza->Cdat == TIMENULL || strlen(prpAza->Usec) <= 0)
		{
			prpAza->Cdat = olCurrTime;
			strcpy(prpAza->Usec,olUserText.Left(31));
		}
		else
		{
			prpAza->Lstu = olCurrTime;
			strcpy(prpAza->Useu,olUserText.Left(31));
		}
	}
}

CString CedaAzaData::Dump(long lpUrno)
{
	CString olDumpStr;
	AZADATA *prlAza = GetAzaByUrno(lpUrno);
	if(prlAza == NULL)
	{
		olDumpStr.Format("No AZADATA Found for AZA.URNO <%ld>",lpUrno);
	}
	else
	{
		CString olData;
		MakeCedaData(&omRecInfo,olData,prlAza);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();
		if(ilNumFields != ilNumData)
		{
			olDumpStr.Format("<%s>\n<%s>\n",pcmFieldList,olData);
		}
		else
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + ">    ";
			}
		}
	}
	return olDumpStr;
}

bool CedaAzaData::DeleteAzaRecord(long lpAzaUrno)
{
	return DeleteAzaRecord(GetAzaByUrno(lpAzaUrno));
}

bool CedaAzaData::DeleteAzaRecord(AZADATA *prpAza)
{
	bool blRc = false;

	if(prpAza != NULL)
	{
		char pclSelection[100];
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpAza->Urno);
		char pclCmd[100] = "DRT";

		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, "", "");

		if((blRc = CedaAction(pclCmd,pclSelection)))
		{
			//AZADATA rlOriginalAza = *prpAza;
			DeleteAzaInternal(prpAza->Urno);
			//ogCCSDdx.DataChanged((void *)this,AZA_DELETE,(void *)rlOriginalAza.Urno);
		}
		else
		{
			ogBasicData.LogCedaError("CedaAzaData Error",omLastErrorMessage,pclCmd,"",pcmTableName,pclSelection);
		}
	}
	return blRc;
}


void CedaAzaData::DeleteAzaInternal(long lpUrno)
{
	int ilNumAzas = omData.GetSize();
	for(int ilDel = (ilNumAzas-1); ilDel >= 0; ilDel--)
	{
		AZADATA *prlAza = &omData[ilDel];
		if(prlAza->Urno == lpUrno)
		{
			omData.DeleteAt(ilDel);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}
