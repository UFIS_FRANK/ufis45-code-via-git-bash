// CedaRnkData.h - Formerly Ranks, now Functions in PFCTAB
// see also BasicData::CreateListOfManagers() - map of which of these functions is a manager

#ifndef _CEDARNKDATA_H_
#define _CEDARNKDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

// Data fields from table PFCTAB (sic)
struct RnkDataStruct 
{
	long Urno;
	char Fctc[6];	// Function Code
	char Fctn[41];	// Function Code Description
	char Prio[3];	// Priority where 1 > 2
};
typedef struct RnkDataStruct RNKDATA;

/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaRnkData: public CCSCedaData
{

private: 
	void ClearAll();
	BOOL AddRnkInternal(RNKDATA *prpRnk);
	void PrepareRnkData(RNKDATA *prpRnk);
	
// Attributes
public:
    CCSPtrArray<RNKDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omFctcMap;
	CString GetTableName(void);

// Operations
public:
    CedaRnkData();
    ~CedaRnkData();

	BOOL ReadRnkData();

	int GetRankWeight(const char *pcpFctc);
	RNKDATA *GetRnkByUrno(long lpUrno);
	RNKDATA *GetRnkByFctc(const char *pcpFctc);
	void GetAllRanks(CStringArray& ropRanks, bool bpReset=true);
};

extern CedaRnkData ogRnkData;
#endif
