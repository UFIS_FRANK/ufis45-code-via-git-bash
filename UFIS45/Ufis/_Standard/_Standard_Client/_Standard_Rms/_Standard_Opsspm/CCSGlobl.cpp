///////////////////////////////////////////////////////////
// CCSGlobl.cpp
//
// Global definitions for the OpssPm Project
#include <CCSGlobl.h>
#include <cedabasicdata.h>
#include <GUILng.h>

CCSLog          ogCCSLog(pcgAppName);
CCSDdx			ogDdx;
CCSCedaCom      ogCCSCommHandler(&ogCCSLog, pcgAppName);  
CCSBcHandle     ogCCSBcHandle(&ogDdx, &ogCCSCommHandler, &ogCCSLog);
//CBasicData      ogCBasicData;
CCSBasic ogCCSBasic;
CedaBasicData	ogBCD("", &ogDdx, &ogCCSBcHandle, &ogCCSCommHandler, &ogCCSBasic);


const char*		pcgReportName  = "Reporting"; 
char			pcgReportingPath[256];
char			pcgMacroPrintCommon[256];
char			pcgMacroPrintWork[256];
char			pcgMacroPrintBreak[256];
char			cgSelection[20000];


CString GetString(UINT ipStringId)
{
	CGUILng* ogGUILng = CGUILng::TheOne();
	return ogGUILng->GetString(ipStringId);
}


BOOL IsPrivateProfileOn(CString key, CString defaultValue)
{
	BOOL pclResult = FALSE;
	char pclTmpText[100];	
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
	{	
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	}
	else
	{
		strcpy(pclConfigPath, getenv("CEDA"));
	}

	
	
	
	GetPrivateProfileString(pcgAppName, key, defaultValue,pclTmpText, sizeof pclTmpText, pclConfigPath);

	if(!strcmp(pclTmpText,defaultValue))
	{
		pclResult = FALSE;
	}
	else
	{
		if(!strcmp(defaultValue,"NO"))
		{
			if(!strcmp(pclTmpText,"YES"))
			pclResult = TRUE;
		}
		else if(!strcmp(defaultValue,"FALSE"))
		{
			if(!strcmp(pclTmpText,"TRUE"))
			pclResult = TRUE;	
		}
	}
	return pclResult;
}