#include <stdafx.h>
#include <TrackSheet.h>
#include <resource.h>
#include <WINSPOOL.H>
#include <CedaJobData.h>
#include <CedaJodData.h>
#include <CedaAzaData.h>
#include <CedaEmpData.h>
#include <CedaRudData.h>
#include <CedaSerData.h>
#include <CedaTlxData.h>
#include <CedaCcaData.h>
#include <CedaRemData.h>
#include <basicdata.h>
#include <FieldConfigDlg.h>
#include <CedaCfgData.h>
#include <TelexTypeDlg.h>
#include <OpssPm.h>


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern COpssPmApp theApp;

TrackSheet::TrackSheet(CWnd *opParent)
{
	pomFlight = NULL;
	pomParent = opParent;
}

TrackSheet::~TrackSheet()
{
    DeleteAll();
}


/////////////////////////////////////////////////////////////////////////////
// TrackSheet - TRACKSHEET_LINEDATA array maintenance
void TrackSheet::MakeLines(FLIGHTDATA *popFlight)
{
	pomFlight = popFlight;
	MakeLines();
}

void TrackSheet::MakeLines()
{
	DeleteAll();
	imTotalLines = 0;
	bmPRMPrinted = FALSE;
	bmCrewPrinted = FALSE;
	bmArrPrinted = FALSE;
	imCrewAckLines = 7;
	imArrTeamLines = 6;
	imRemaLength = 28;
	if (pomFlight != NULL)
	{
		CCSPtrArray <DPXDATA> olTmpDpxs;
		ogDpxData.GetDpxsByFlight(pomFlight->Urno,olTmpDpxs,true);
		for(int ilFJ = 0; ilFJ < olTmpDpxs.GetSize(); ilFJ++)
		{
			DPXDATA *prlDpx = &olTmpDpxs[ilFJ];
			if (prlDpx != NULL)
			{
				TRACKSHEET_LINEDATA rlPrm;
				rlPrm.Udpx = prlDpx->Urno;
				rlPrm.Name = prlDpx->Name;
				rlPrm.Seat = prlDpx->Seat;
				rlPrm.Prmt = prlDpx->Prmt;
				rlPrm.Tflu = prlDpx->Tflu;
				if (prlDpx->Tflu != 0L)
				{
					if (strcmp(pomFlight->Adid, "A") == 0)
					{
						FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Tflu);
						if (prlFlight == NULL)
						{
							ogFlightData.HandleMissingFlight(prlDpx->Tflu);
							prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Tflu);
						}
						if (prlFlight != NULL)
						{
							rlPrm.Tfln = prlFlight->Flno;
							CTime Etdi = (prlFlight->Etod != TIMENULL) ? prlFlight->Etod : prlFlight->Stod;
							rlPrm.Tstd = ogBasicData.FormatDate(Etdi,pomFlight->Stoa);
						}
					}
					else
					{
						FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Flnu);
						if (prlFlight == NULL)
						{
							ogFlightData.HandleMissingFlight(prlDpx->Flnu);
							prlFlight = ogFlightData.GetFlightByUrno(prlDpx->Flnu);
						}
						if (prlFlight != NULL)
						{
							rlPrm.Tfln = prlFlight->Flno;
							CTime Etai = (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa : prlFlight->Stoa;
							rlPrm.Tstd = ogBasicData.FormatDate(Etai,pomFlight->Stod);
						}
					}
				}
				rlPrm.Rema = prlDpx->Rema;
				rlPrm.Aloc = prlDpx->Aloc;
				rlPrm.Stfns.DeleteAll( );
				CCSPtrArray <JOBDATA> olJobs;
				ogJobData.GetJobsByUprm(olJobs, rlPrm.Udpx);
				int ilNumJobs = olJobs.GetSize();
				for(int ilJob = 0; ilJob<ilNumJobs; ilJob++)
				{
					//if (olJobs[ilJob].Flur == pomFlight->Urno) igu on 26 May 2010
					//{ igu on 26 May 2010
						CString olName = ogEmpData.GetEmpName(olJobs[ilJob].Ustf);
						CCSPtrArray<JOBDATA> olPoolJobs;
						ogJobData.GetJobsByShur(olPoolJobs,olJobs[ilJob].Shur,TRUE);
						int ilNumPoolJobs = olPoolJobs.GetSize();
						for(int ilPJ = 0; ilPJ < ilNumPoolJobs; ilPJ++)
						{
							JOBDATA *prlPJ = &olPoolJobs[ilPJ];
							if(strlen(prlPJ->Text) > 0)
							{
								olName = olName + CString(" - ") + prlPJ->Text;
								break;
							}
						}
						
						if ((strcmp(pomFlight->Adid, "A") == 0))
				
					{	
						if (pomFlight->Urno == olJobs[ilJob].Flur )
						{      rlPrm.Stfns.NewAt(rlPrm.Stfns.GetSize(), olName);	}


                        int a=atoi(olJobs[ilJob].Dety);

						if (a==0)
						{						  
						  rlPrm.Stfns.NewAt(rlPrm.Stfns.GetSize(), olName);	
							  
						}
					}
						

    
                 else if ((strcmp(pomFlight->Adid, "D") == 0) &&  (pomFlight->Urno == olJobs[ilJob].Flur   ) )
						
				{	rlPrm.Stfns.NewAt(rlPrm.Stfns.GetSize(), olName);	}	
						
						
						
						
						
						//rlPrm.Stfns.NewAt(rlPrm.Stfns.GetSize(), olName);
					//}  igu on 26 May 2010
				}

				CreateLine(&rlPrm);
				int ilNumRemaLines = (rlPrm.Rema.GetLength()%imRemaLength)==0?rlPrm.Rema.GetLength()/imRemaLength:rlPrm.Rema.GetLength()/imRemaLength+1;
				if (ilNumJobs > 1 || ilNumRemaLines > 1)
					imTotalLines += ilNumJobs > ilNumRemaLines ? ilNumJobs : ilNumRemaLines;
				else
					imTotalLines += 1;
			}
		}
	}
	return;
	
}

int TrackSheet::ComparePRM(TRACKSHEET_LINEDATA *prpPrm1,
	TRACKSHEET_LINEDATA *prpPrm2)
{
//    // Compare in the sort order, from the outermost to the innermost
//    int n = omSortOrder.GetSize();
//    for (int i = 0; i < n; i++)
//    {
//        int ilCompareResult = 0;
//
//        switch (omSortOrder[i])
//        {
//        case BY_SHIFTSTART:
//			ilCompareResult = prpPrm1->Acfr.GetTime() - prpPrm2->Acfr.GetTime();
//            break;
//        case BY_SHIFTEND:
//			ilCompareResult = prpPrm1->Acto.GetTime() - prpPrm2->Acto.GetTime();
//            break;
//        case BY_SHIFTCODE:
//            ilCompareResult = strcmp(prpPrm1->Sfca,prpPrm2->Sfca);
//            break;
//        case BY_TEAM:
//            ilCompareResult = strcmp(prpPrm1->Tmid,prpPrm2->Tmid);
//            break;
//        case BY_RANK:
//			ilCompareResult = strcmp(prpPrm1->Rank,prpPrm2->Rank);
//            break;
//        case BY_NAME:
//            ilCompareResult = strcmp(prpPrm1->Name,prpPrm2->Name);
//            break;
//        }
//
//        if (ilCompareResult != 0)
//            return ilCompareResult;
//    }
//
    return strcmp(prpPrm1->Name,prpPrm2->Name);
}

void TrackSheet::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int TrackSheet::CreateLine(TRACKSHEET_LINEDATA *prpPrm)
{
	int ilLineCount = omLines.GetSize();
	
	// Search for the position which we want to insert this new bar
	for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
	    if (ComparePRM(prpPrm, &omLines[ilLineno-1]) >= 0)
	        break;  // should be inserted after Lines[ilLineno-1]

	TRACKSHEET_LINEDATA rlPrm;
	rlPrm = *prpPrm;
	omLines.NewAt(ilLineno, rlPrm);

	return ilLineno;
}

void TrackSheet::DeleteLine(int ipLineno)
{
	omLines[ipLineno].Stfns.DeleteAll();
	omLines.DeleteAt(ipLineno);
}


////////////////////////////////////////////////////////////////////////////////////////////
// Printing functions

BOOL TrackSheet::PrintPrmLine(TRACKSHEET_LINEDATA *prpLine,BOOL bpIsLastLine)
{
	int ilBottomFrame; 
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	int ilJobFieldWidth = 270;
	int ilTimeWidth = 90;
	int ilRemaLength = imRemaLength;
	if (pomPrint->imLineNo <= 5)
	{
		PrintPrmLineHeader();
	}

	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 1941;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();
	}
	else
	{
		if (prpLine == NULL)
			return FALSE;
		DPXDATA *prlDpx = ogDpxData.GetDpxByUrno(prpLine->Udpx);
		if(prlDpx == NULL)
			return FALSE;
 
		bool bpFirst = true;
		int ilNumExtraLine = ((strlen(prlDpx->Aloc)>0) || (strlen(prlDpx->Owhc)>0))? 2:1;
		int ilNumStaffs = prpLine->Stfns.GetSize();
		int ilNumRemaLines = (prpLine->Rema.GetLength()%ilRemaLength)==0?prpLine->Rema.GetLength()/ilRemaLength:prpLine->Rema.GetLength()/ilRemaLength+1;//prpLine->Rema.GetLength();
		for (int i = 0; i<ilNumExtraLine || i<ilNumStaffs || i<ilNumRemaLines||i<=1;i++)
		{
			if (pomPrint->imLineNo <= 5)
			{
				PrintTableName(pomPrint, CString("DETAILS PASSENGER HANDLING"));
				PrintPrmLineHeader();
			}

			rlElement.Alignment  = PRINT_LEFT;
			rlElement.Length     = 400;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_NOFRAME;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			if(i==0)
			{
				rlElement.FrameTop = PRINT_FRAMETHIN;
				rlElement.Text.Format("%s", prpLine->Name);
			}
			else if (i==1)
			{
				rlElement.FrameTop = PRINT_NOFRAME;
				rlElement.Text.Format((strlen(prpLine->Aloc)>0)?"-%s" : "", prpLine->Aloc);
			}
			else
			{
				rlElement.FrameTop = PRINT_NOFRAME;
				rlElement.Text = "";
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 140;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			if(i==0)
			{
				rlElement.FrameTop= PRINT_FRAMETHIN;
				rlElement.Text       = prpLine->Seat;
			}
			else
			{
				rlElement.FrameTop= PRINT_NOFRAME;
				rlElement.Text = "";
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 140;
			if(i==0)
			{
				rlElement.FrameTop= PRINT_FRAMETHIN;
				rlElement.Text       = prpLine->Prmt;
			}
			else if( i==1)
			{
				rlElement.FrameTop = PRINT_NOFRAME;
				rlElement.Text.Format("-%s", prlDpx->Owhc);
			}
			else
			{
				rlElement.FrameTop= PRINT_NOFRAME;
				rlElement.Text = "";
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 140;
			if(i==0)
			{
				rlElement.FrameTop= PRINT_FRAMETHIN;
				rlElement.Text       = prpLine->Tfln;
			}
			else
			{
				rlElement.FrameTop= PRINT_NOFRAME;
				rlElement.Text = "";
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 140;
			if(i==0)
			{
				rlElement.FrameTop= PRINT_FRAMETHIN;
				rlElement.Text       = prpLine->Tstd;
			}
			else
			{
				rlElement.FrameTop= PRINT_NOFRAME;
				rlElement.Text = "";
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 640; 
			if(i==0 || i<ilNumRemaLines)
			{
				if (i==0)
					rlElement.FrameTop= PRINT_FRAMETHIN;
				else
					rlElement.FrameTop= PRINT_NOFRAME;
				rlElement.Text       = prpLine->Rema.Mid(i*ilRemaLength,ilRemaLength);
			}
			else
			{
				rlElement.FrameTop= PRINT_NOFRAME;
				rlElement.Text = "";
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 340;
			if(i==0 || i<ilNumStaffs)
			{
				rlElement.FrameTop= PRINT_FRAMETHIN;
				if (i<ilNumStaffs)
				{
					rlElement.Text       = prpLine->Stfns[i];
				}
				else
				{
					rlElement.Text = "";
				}
			}
			else
			{
				rlElement.FrameTop= PRINT_NOFRAME;
				rlElement.Text = "";
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			if(i!=1 || i<ilNumExtraLine ||  i<ilNumRemaLines || i<ilNumStaffs || prpLine->Aloc.GetLength() >0)
				pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
			bpFirst = false;
			if( pomPrint->imLineNo > pomPrint->imMaxLines )
			{ 
				if (pomPrint->emPrintOption == CCSPrint::PRINT_PREVIEW)
				{
					return TRUE;
				}
				if ( pomPrint->imPageNo > 0)
				{
					pomPrint->PrintFooter("","");
					pomPrint->imLineNo = pomPrint->imMaxLines + 1;
					pomPrint->omCdc.EndPage();
					pomPrint->imLineTop = pomPrint->imFirstLine;
				}
				PrintPrmHeader(pomPrint);
			}
		}
	}

	return TRUE;
}

BOOL TrackSheet::PrintPrmLineHeader()
{
	//int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment  = PRINT_CENTER;
	rlElement.Length     = 400;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_FRAMEMEDIUM;
	rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
	rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->omSmallFont_Bold;
	rlElement.Text = CString("Pax Name"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 140;
	rlElement.FrameLeft  = PRINT_NOFRAME;
	rlElement.Text       = CString("SOC"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 140;
	rlElement.Text       = CString("Type"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	if (strcmp(pomFlight->Adid, "A") == 0)
	{
		rlElement.Length     = 140;
		rlElement.Text       = CString("Dep Flt"); 
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 140;
		rlElement.Text       = CString("STD"); 
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	else
	{
		rlElement.Length     = 140;
		rlElement.Text       = CString("Arr Flt"); 
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 140;
		rlElement.Text       = CString("STA"); 
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}

	rlElement.Length     = 640;
	rlElement.Text       = CString("Remarks"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 340;
	rlElement.Text       = CString("Handled by"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	return TRUE;
}

BOOL TrackSheet::PrintEmptyPrmLine()
{
	//int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment  = PRINT_CENTER;
	rlElement.Length     = 400;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_FRAMEMEDIUM;
	rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
	rlElement.FrameBottom= PRINT_NOFRAME;
	rlElement.pFont       = &pomPrint->omSmallFont_Bold;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 140;
	rlElement.FrameLeft  = PRINT_NOFRAME;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 140;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 140;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 140;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 640;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 340;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	return TRUE;
}

BOOL TrackSheet::PrintNilCasePrmLine()
{
	//int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment  = PRINT_CENTER;
	rlElement.Length     = 400;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_FRAMEMEDIUM;
	rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
	rlElement.FrameBottom= PRINT_NOFRAME;
	rlElement.pFont       = &pomPrint->omSmallFont_Bold;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);


	rlElement.Length     = 140;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

  
	rlElement.Length     = 140;
	rlElement.Text = CString("Nil Case"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 140;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 140;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 640;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 340;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	return TRUE;
}

BOOL TrackSheet::PrintCrewAck(CCSPrint *pomPrint)
{
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	bmCrewPrinted = TRUE;

	ilBottomFrame = PRINT_FRAMETHIN;
	
	//Seperator line
	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = 1941;
	rlElement.FrameLeft  = PRINT_NOFRAME;
	rlElement.FrameRight = PRINT_NOFRAME;
	rlElement.FrameTop   = PRINT_NOFRAME;
	rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->omMediumFont_Bold;
	rlElement.Text.Empty();
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	//CABIN CREW ACKNOWLEDGEMENT
	rlElement.Length     = 1941;
	rlElement.FrameBottom= PRINT_NOFRAME;
	rlElement.pFont       = &pomPrint->omMediumFont_Bold;
	rlElement.Text = CString("CABIN CREW ACKNOWLEDGEMENT");
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	rlElement.pFont       = &pomPrint->omSmallFont_Bold;
	//Name and Signature
	rlElement.Length     = 800;
	rlElement.Text       = CString("NAME OF IFS/CS/CSS:___________________________________________________"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 200;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 800;
	rlElement.Text       = CString("SIGNATURE:_________________________________________________________________"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	//CSO/CSA
	rlElement.Length     = 600;
	rlElement.Text       = CString("REPORTING OF CSO/CSA:   "); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 400;
	rlElement.Text       = CString("[  ] YES   [  ] NO"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	//Shipbag
	rlElement.Length     = 600;
	rlElement.Text       = CString("SHIPBAG ON BOARD:  "); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 400;
	rlElement.Text       = CString("[  ] YES   [  ] NO"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 940;
	rlElement.Text       = CString("Please tick accordingly in each BOX."); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	//Remarks
	rlElement.Length     = 1940;
	rlElement.Text       = CString("REMARKS:________________________________________________________________________________________________________"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	//No shipbag line
	rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
	rlElement.Length     = 1940;
	rlElement.Text       = CString("IF NO SHIPBAG - Please indicate in the REMARK column accordingly."); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	return TRUE;
}

BOOL TrackSheet::PrintArrTeam(CCSPrint *pomPrint)
{
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;
	
	//ARRIVAL TEAM
	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = 1941;
	rlElement.FrameLeft  = PRINT_NOFRAME;
	rlElement.FrameRight = PRINT_NOFRAME;
	rlElement.FrameTop   = PRINT_NOFRAME;
	rlElement.FrameBottom= PRINT_NOFRAME;
	rlElement.pFont       = &pomPrint->omMediumFont_Bold;
	if (strcmp(pomFlight->Adid, "A") == 0)
	{
		rlElement.Text = CString("ARRIVAL TEAM");
	}
	else
	{
		rlElement.Text = CString("DEPARTURE TEAM");
	}
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	rlElement.pFont       = &pomPrint->omSmallFont_Bold;
	//Duty Manager and CSO/CSA
	rlElement.Length     = 800;
	rlElement.Text       = CString("Duty Manager:_____________________________________________"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 200;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 800;
	rlElement.Text       = CString("CSO/CSA:___________________________________________________"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	//CSO/CSA
	rlElement.Length     = 800;
	rlElement.Text       = CString("CSO/CSA:__________________________________________________"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 200;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 800;
	rlElement.Text       = CString("CSO/CSA:__________________________________________________"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	//CSO/CSA
	rlElement.Length     = 800;
	rlElement.Text       = CString("CSO/CSA:__________________________________________________"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 200;
	rlElement.Text.Empty(); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 800;
	rlElement.Text       = CString("CSO/CSA:__________________________________________________"); 
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	if (strcmp(pomFlight->Adid, "A") == 0)
	{
		//Important:
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.Length     = 320;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text       = CString("IMPORTANT:"); 
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 1620;
		rlElement.pFont       = &pomPrint->omSmallFont_Regular;
		rlElement.Text       = CString("PLEASE DISPLAY THE ARRIVAL BANNER PROMINENTLY OUTSIDE THE GATE AND RETURN TO "); 
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();

		//Remarks
		rlElement.FrameTop   = PRINT_NOFRAME;
		rlElement.Length     = 1940;
		rlElement.Text       = CString("ITS ORIGINAL POSITION AFTER USE. THANK YOU."); 
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();
	}

	return TRUE;
}

BOOL TrackSheet::PrintPrmHeader(CCSPrint *pomPrint)
{
	pomPrint->pomCdc->StartPage();
	pomPrint->PrintHeader();
	return PrintFlightInfo(pomPrint);
}

BOOL TrackSheet::PrintFlightInfo(CCSPrint *pomPrint)
{
	if (pomFlight == NULL)
	{
		return FALSE;
	}
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	CString olTmpStr;

	ilBottomFrame = PRINT_FRAMETHIN;

	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = 100;
	rlElement.FrameLeft  = PRINT_NOFRAME;
	rlElement.FrameRight = PRINT_NOFRAME;
	rlElement.FrameTop   = PRINT_NOFRAME;
	rlElement.FrameBottom= PRINT_NOFRAME;
	rlElement.pFont       = &pomPrint->omSmallFont_Bold;
	rlElement.Text.Empty();
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = 400;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_NOFRAME;
	rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
	rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->omSmallFont_Bold;
	rlElement.Text = CString("Flt No: ") + pomFlight->Flno;
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 400;
	if (strcmp(pomFlight->Adid, "A") == 0)
	{
		rlElement.Text       = CString("Date: ") + pomFlight->Stoa.Format("%d-%b-%y");
	}
	else
	{
		rlElement.Text       = CString("Date: ") + pomFlight->Stod.Format("%d-%b-%y");
	}
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 600;
	if (strcmp(pomFlight->Adid, "A") == 0)
	{
		rlElement.Text       = CString("Sector: ") + pomFlight->Org3;
		olTmpStr = (pomFlight->Via3 != "") ? (CString(" / ")+pomFlight->Via3):CString(" ");
		rlElement.Text = rlElement.Text + olTmpStr;
	}
	else
	{
		rlElement.Text       = CString("Sector: ") + pomFlight->Des3;
		olTmpStr = (pomFlight->Via3 != "") ? (CString(" / ")+pomFlight->Via3):CString(" ");
		rlElement.Text = rlElement.Text + olTmpStr;
	}
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 300;
	rlElement.FrameRight  = PRINT_FRAMEMEDIUM;
	rlElement.Text       = CString("A/C Regn: ") + pomFlight->Regn;
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = 100;
	rlElement.FrameLeft  = PRINT_NOFRAME;
	rlElement.FrameRight = PRINT_NOFRAME;
	rlElement.FrameTop   = PRINT_NOFRAME;
	rlElement.FrameBottom= PRINT_NOFRAME;
	rlElement.pFont       = &pomPrint->omSmallFont_Bold;
	rlElement.Text.Empty();
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = 800;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_NOFRAME;
	rlElement.FrameTop   = PRINT_NOFRAME;
	rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->omSmallFont_Bold;
	if (strcmp(pomFlight->Adid, "A") == 0)
	{
		rlElement.Text = CString("STA / ETA / ATA: ");
		olTmpStr = (pomFlight->Stoa != TIMENULL) ? pomFlight->Stoa.Format("%H:%M"):CString("     ");
		rlElement.Text = rlElement.Text + olTmpStr;
		rlElement.Text = rlElement.Text + CString(" / ");
		olTmpStr = (pomFlight->Etoa != TIMENULL) ? pomFlight->Etoa.Format("%H:%M"):CString("     ");
		rlElement.Text = rlElement.Text + olTmpStr;
		rlElement.Text = rlElement.Text + CString(" / ");
		olTmpStr = (pomFlight->Land != TIMENULL) ? pomFlight->Land.Format("%H:%M"):CString("     ");
		rlElement.Text = rlElement.Text + olTmpStr;
	}
	else
	{
		rlElement.Text = CString("STD / ETD / ATD: ");
		olTmpStr = (pomFlight->Stod != TIMENULL) ? pomFlight->Stod.Format("%H:%M"):CString("     ");
		rlElement.Text = rlElement.Text + olTmpStr;
		rlElement.Text = rlElement.Text + CString(" / ");
		olTmpStr = (pomFlight->Etod != TIMENULL) ? pomFlight->Etod.Format("%H:%M"):CString("     ");
		rlElement.Text = rlElement.Text + olTmpStr;
		rlElement.Text = rlElement.Text + CString(" / ");
		olTmpStr = (pomFlight->Airb != TIMENULL) ? pomFlight->Airb.Format("%H:%M"):CString("     ");
		rlElement.Text = rlElement.Text + olTmpStr;
	}
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 600;
	rlElement.Text = CString("D/Load");
	rlElement.Text = rlElement.Text + CString(" P ") + CString(" ");
	rlElement.Text = rlElement.Text + CString(" J ") + CString(" ");
	rlElement.Text = rlElement.Text + CString(" Y ") + CString(" ");
	rlElement.Text = rlElement.Text + CString(" INF ") + CString(" ");
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 300;
	rlElement.FrameRight  = PRINT_FRAMEMEDIUM;
	if (strcmp(pomFlight->Adid, "A") == 0)
	{
		rlElement.Text = CString("Gate / Bay ");
		olTmpStr = (strcmp(pomFlight->Gta1,"")) ? pomFlight->Gta1:CString("     ");
		rlElement.Text = rlElement.Text + olTmpStr;
		rlElement.Text = rlElement.Text + CString(" / ");
		olTmpStr = (strcmp(pomFlight->Psta,"")) ? pomFlight->Psta:CString("     ");
		rlElement.Text = rlElement.Text + olTmpStr;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	else
	{
		rlElement.Text = CString("Gate / Bay ");
		olTmpStr = (strcmp(pomFlight->Gtd1,"")) ? pomFlight->Gtd1:CString("     ");
		rlElement.Text = rlElement.Text + olTmpStr;
		rlElement.Text = rlElement.Text + CString(" / ");
		olTmpStr = (strcmp(pomFlight->Pstd,"")) ? pomFlight->Pstd:CString("     ");
		rlElement.Text = rlElement.Text + olTmpStr;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = 100;
	rlElement.FrameLeft  = PRINT_NOFRAME;
	rlElement.FrameRight = PRINT_NOFRAME;
	rlElement.FrameTop   = PRINT_NOFRAME;
	rlElement.FrameBottom= PRINT_NOFRAME;
	rlElement.pFont       = &pomPrint->omSmallFont_Bold;
	rlElement.Text.Empty();
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = 800;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_FRAMEMEDIUM;
	rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
	rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->omSmallFont_Bold;
	rlElement.Text = CString("FF status / Staff Name: ");
	olTmpStr = (strcmp(pomFlight->Prms,"")) ? pomFlight->Prms:CString("     ");
	rlElement.Text = rlElement.Text + olTmpStr;
	rlElement.Text = rlElement.Text + CString(" / ");
	if (strcmp(pomFlight->Prmu,""))
	{
		olTmpStr = pomFlight->Prmu;
		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(olTmpStr);
		if (prlEmp != NULL)
		{
			 olTmpStr = olTmpStr + CString("-") + prlEmp->Finm;
		}
	}
	else
	{
		olTmpStr = CString("     ");
	}
	rlElement.Text = rlElement.Text + olTmpStr;
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return TRUE;
}

BOOL TrackSheet::PrintTableName(CCSPrint *pomPrint,CString opTableName)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = 100;
	rlElement.FrameLeft  = PRINT_NOFRAME;
	rlElement.FrameRight = PRINT_NOFRAME;
	rlElement.FrameTop   = PRINT_NOFRAME;
	rlElement.FrameBottom= PRINT_NOFRAME;
	rlElement.pFont       = &pomPrint->omSmallFont_Bold;
	rlElement.Text.Empty();
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = 1600;
	rlElement.FrameLeft  = PRINT_NOFRAME;
	rlElement.FrameRight = PRINT_NOFRAME;
	rlElement.FrameTop   = PRINT_NOFRAME;
	rlElement.FrameBottom= PRINT_NOFRAME; 
	rlElement.pFont       = &pomPrint->omMediumFont_Bold;
	rlElement.Text = opTableName;
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return TRUE;
}

void TrackSheet::PrintView()
{  
	CString olTarget = CString("");
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y %H:%M");
	CString olReportName;
	int ilLineNo = 0;
	if (strcmp(pomFlight->Adid, "A") == 0)
	{
		olReportName = GetString(IDS_ARRIVALSHEET);
	}
	else
	{
		olReportName = GetString(IDS_DEPARTURESHEET);
	}
	theApp.SetPortrait();
	pomPrint = new CCSPrint(pomParent,PRINT_PORTRAIT,60,340,50,olReportName,olPrintDate,olTarget);
	pomPrint->imMaxLines = 42;

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_PORTRAIT,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;

			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			pomPrint->imLineHeight = 62;
			rlDocInfo.cbSize = sizeof( DOCINFO );

			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STAFFJOBS));
			rlDocInfo.lpszDocName = pclDocName;	

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			PrintPrmHeader(pomPrint);
			pomPrint->imPageNo = 1;
			PrintTableName(pomPrint, CString("DETAILS PASSENGER HANDLING"));
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				bmPRMPrinted = TRUE;
				bmCrewPrinted = FALSE;
				PrintPrmLine(&omLines[i],FALSE);
				ilLineNo ++;
				if( pomPrint->imLineNo > pomPrint->imMaxLines )
				{
					if ( pomPrint->imPageNo > 0)
					{
						PrintPrmLine(NULL,TRUE);
						pomPrint->PrintFooter("","");
						pomPrint->imLineNo = pomPrint->imMaxLines + 1;
						pomPrint->omCdc.EndPage();
						pomPrint->imLineTop = pomPrint->imFirstLine;
					}
					PrintPrmHeader(pomPrint);
				}
			}
			if (pomPrint->imLineNo + imCrewAckLines + imArrTeamLines <= pomPrint->imMaxLines)
			{
				if (!bmPRMPrinted || ilLineNo > 0)
				{
					bmCrewPrinted = FALSE;
					PrintPrmLine(NULL,FALSE);	
					//Print Nil Case Line;
					if(ilLineNo ==0)
					PrintNilCasePrmLine();

					for(i = pomPrint->imLineNo;i<pomPrint->imMaxLines-imCrewAckLines-imArrTeamLines;i++)
					{
						PrintEmptyPrmLine();
					}
				}
				PrintPrmLine(NULL,TRUE);
				if (!bmCrewPrinted)
					PrintCrewAck(pomPrint);
				PrintArrTeam(pomPrint);
			}
			else if (pomPrint->imLineNo + imCrewAckLines<= pomPrint->imMaxLines)
			{
				for(i = pomPrint->imLineNo;i<pomPrint->imMaxLines-imCrewAckLines;i++)
				{
					PrintEmptyPrmLine();
				}
				PrintPrmLine(NULL,TRUE);		
				PrintCrewAck(pomPrint);
			}
			else if (pomPrint->imLineNo <= pomPrint->imMaxLines)
			{
				for(i = pomPrint->imLineNo;i<pomPrint->imMaxLines;i++)
				{
					PrintEmptyPrmLine();
				}
				PrintPrmLine(NULL,TRUE);		
			}
			PrintPrmLine(NULL,TRUE);
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
	theApp.SetLandscape();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
void TrackSheet::PrintView2()
{  
	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) + CString(omEndTime.Format("%d.%m.%Y %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y %H:%M");
	pomPrint = new CCSPrint(pomParent,PRINT_LANDSCAPE,60,500,40,GetString(IDS_STAFFJOBS),olPrintDate,omTarget);
	pomPrint->imMaxLines = 20;

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = 0;

			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			pomPrint->imLineHeight = 62;
			rlDocInfo.cbSize = sizeof( DOCINFO );
			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STAFFJOBS));
			rlDocInfo.lpszDocName = pclDocName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				PrintPrmLine2(&omLines[i]);
			}
			if(pomPrint->imLineNo != 0)
			{
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				PrintEndOfPage2();
			}
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

bool TrackSheet::PrintStartOfPage2()
{
	bool blStartOfPage = false;
	if(pomPrint->imLineNo == 0)
	{
		blStartOfPage = true;

		PrintPrmHeader(pomPrint);

		CCSPtrArray <PRINTELEDATA> rlPrintLine;
		PRINTELEDATA rlElement;

		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
		rlElement.FrameRight = PRINT_FRAMEMEDIUM;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
		rlElement.pFont		 = &pomPrint->omCourierNormal10;

		rlElement.Length     = imFuncLen2;
		rlElement.Text = GetString(IDS_STC_MFCT); // "M-Fct";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imNameLen2;
		rlElement.Text = GetString(IDS_STC_NAME); // "Name";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imWorkGroupLen2;
		rlElement.Text = GetString(IDS_STC_WORKGRP); // "Work Grp";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imShiftLen2;
		rlElement.Text = GetString(IDS_STC_SHIFT); // "Shift";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imJobLen2;
		rlElement.Text = GetString(IDS_STC_JOB); // "Job";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imRegnLen2;
		rlElement.Text = GetString(IDS_STC_REGN); // "AC-Regn";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imArrTimeLen2;
		rlElement.Text = GetString(IDS_STC_ARRTIME); // "STA / ETA";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imDepTimeLen2;
		rlElement.Text = GetString(IDS_STC_DEPTIME); // "STD / ETD";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imServiceLen2;
		rlElement.Text = GetString(IDS_STC_SERVICE); // "Service";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imGatePosLen2;
		rlElement.Text = GetString(IDS_STC_GATPOS); // "Gate/Pos";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imFuncLen2;
		rlElement.Text = GetString(IDS_STC_JFCT); // "J-Fct";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();
	}

	return blStartOfPage;
}

void TrackSheet::PrintEndOfPage2()
{
	if(pomPrint->imLineNo >= pomPrint->imMaxLines &&  pomPrint->imPageNo > 0)
	{
		CCSPtrArray <PRINTELEDATA> rlPrintLine;
		PRINTELEDATA rlElement;

		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = imFuncLen2+imNameLen2+imWorkGroupLen2+imShiftLen2+imJobLen2+imRegnLen2+imArrTimeLen2+imDepTimeLen2+imGatePosLen2+imServiceLen2+imFuncLen2;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();

		PrintPrmLine(NULL,TRUE);
		pomPrint->PrintFooter("","");
		pomPrint->omCdc.EndPage();
		pomPrint->imLineNo = 0;
		pomPrint->imLineTop = pomPrint->imFirstLine;
	}
}

void TrackSheet::AddElement(CCSPtrArray <PRINTELEDATA> &ropPrintLine, CString &ropText, int ipLength, int ipAlignment, int ipFrameLeft, int ipFrameRight, int ipFrameTop, int ipFrameBottom, CFont &ropFont)
{
	PRINTELEDATA rlElement;
	rlElement.Length		= ipLength;
	rlElement.Text			= ropText;
	rlElement.Alignment		= ipAlignment;
	rlElement.FrameLeft		= ipFrameLeft;
	rlElement.FrameRight	= ipFrameRight;
	rlElement.FrameTop		= ipFrameTop;
	rlElement.FrameBottom	= ipFrameBottom;
	rlElement.pFont			= &ropFont;
	ropPrintLine.NewAt(ropPrintLine.GetSize(),rlElement);
}

BOOL TrackSheet::PrintPrmLine2(TRACKSHEET_LINEDATA *prpLine)
{
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	DPXDATA *prlDpx = ogDpxData.GetDpxByUrno(prpLine->Udpx);
	if(prlDpx == NULL)
		return FALSE;

/*
	bool blPrintEmpName = true;
	int ilNumJobs;
	int ilJob = 0;

	CCSPtrArray <JOBDATA> olPoolJobs;
	ogJobData.GetPoolJobsByUstf(olPoolJobs, prlShift->Stfu);
	int ilNumPJ = olPoolJobs.GetSize(), ilPJ;
	for(ilPJ = (ilNumPJ-1); ilPJ >= 0; ilPJ--)
	{
		JOBDATA *prlPJ = &olPoolJobs[ilPJ];
		if(prlPJ->Shur != prlShift->Urno)
		{
			olPoolJobs.RemoveAt(ilPJ);
		}
	}

	bool blNewPoolJob = true;
	CCSPtrArray <JOBDATA> olJobs;
	ilNumPJ = olPoolJobs.GetSize();
	for(ilPJ = 0; ilPJ < ilNumPJ; ilPJ++)
	{
		JOBDATA *prlPJ = &olPoolJobs[ilPJ];
		ogJobData.GetJobsByPoolJob(olJobs, prlPJ->Urno);
		ogJobData.GetJobsByPoolJobAndType(olJobs, prlPJ->Urno, JOBTEMPABSENCE, false);
		blNewPoolJob = true;
		ilJob = 0;
		ilNumJobs = olJobs.GetSize();
		
		while(ilJob < ilNumJobs || blNewPoolJob)
		{
			JOBDATA *prlJob = NULL;
			if(ilJob < ilNumJobs)
			{
				prlJob = &olJobs[ilJob];
			}

			if(PrintStartOfPage2())
			{
				blPrintEmpName = true;
				blNewPoolJob = true;
			}

			CString olText;

			// main function
			AddElement(rlPrintLine, (blNewPoolJob) ? ogBasicData.GetFunctionForPoolJob(prlPJ) : "", imFuncLen2, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, (blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,
						PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// employee name
			AddElement(rlPrintLine, (blPrintEmpName) ? prpLine->Name : "", imNameLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME,
						(blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,	PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// work group
			AddElement(rlPrintLine, (blNewPoolJob) ? ogBasicData.GetTeamForPoolJob(prlPJ) : "", imWorkGroupLen2, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, (blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,
						PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// shift time
			olText.Format("%s-%s", prlPJ->Acfr.Format("%H%M"), prlPJ->Acto.Format("%H%M"));
			AddElement(rlPrintLine, (blNewPoolJob) ? olText : "", imShiftLen2, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, (blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,
						PRINT_NOFRAME, pomPrint->omCourierNormal10);


			CString olRegn, olArrTime, olDepTime, olGatPos;
			CString olService, olDemandFunction;
			FLIGHTDATA *prlFlight = NULL, *prlRotation = NULL;
			if(prlJob != NULL)
			{
				if((prlFlight = ogFlightData.GetFlightByUrno(prlJob->Flur)) != NULL)
				{
					olRegn = prlFlight->Regn;
					if(ogFlightData.IsArrival(prlFlight))
					{
						olArrTime.Format("%s/%s", (prlFlight->Stoa != TIMENULL) ? prlFlight->Stoa.Format("%H%M") : "", (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa.Format("%H%M") : "");
						olGatPos.Format("%s/%s", prlFlight->Gta1, prlFlight->Psta);
					}
					else
					{
						olDepTime.Format("%s/%s", (prlFlight->Stod != TIMENULL) ? prlFlight->Stod.Format("%H%M") : "", (prlFlight->Etod != TIMENULL) ? prlFlight->Etod.Format("%H%M") : "");
						olGatPos.Format("%s/%s", prlFlight->Gtd1, prlFlight->Pstd);
					}
					if(ogJobData.IsTurnaroundJob(prlJob->Urno) && (prlRotation = ogFlightData.GetRotationFlight(prlFlight)) != NULL)
					{
						if(ogFlightData.IsArrival(prlRotation))
						{
							olArrTime.Format("%s/%s", (prlRotation->Stoa != TIMENULL) ? prlRotation->Stoa.Format("%H%M") : "", (prlRotation->Etoa != TIMENULL) ? prlRotation->Etoa.Format("%H%M") : "");
						}
						else
						{
							olDepTime.Format("%s/%s", (prlRotation->Stod != TIMENULL) ? prlRotation->Stod.Format("%H%M") : "", (prlRotation->Etod != TIMENULL) ? prlRotation->Etod.Format("%H%M") : "");
						}
					}
				}
				else
				{
					olArrTime.Format("%s/%s", prlJob->Acfr.Format("%H%M"), prlJob->Acto.Format("%H%M"));
				}
				DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prlJob);
				if(prlDemand != NULL)
				{
					RUDDATA *prlRud = ogRudData.GetRudByUrno(prlDemand->Urud);
					if(prlRud != NULL)
					{
						SERDATA *prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
						if(prlSer != NULL)
							olService = prlSer->Snam;
					}
					RPFDATA *prlDemandFunction = ogRpfData.GetFunctionByUrud(prlDemand->Urud);
					if(prlDemandFunction != NULL)
					{
						if(!strcmp(prlDemandFunction->Fcco,""))
						{
							SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(prlDemandFunction->Upfc);
							if(prlSgr != NULL)
							{
								olDemandFunction += CString("*") + prlSgr->Grpn;
							}
						}
						else
						{
							olDemandFunction += prlDemandFunction->Fcco;
						}
					}
				}
			}

			// flight regn
			AddElement(rlPrintLine, olRegn, imRegnLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// flight arr time
			AddElement(rlPrintLine, olArrTime, imArrTimeLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// flight dep time
			AddElement(rlPrintLine, olDepTime, imDepTimeLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// service
			AddElement(rlPrintLine, olService, imServiceLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// flight gate position
			AddElement(rlPrintLine, olGatPos, imGatePosLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// service
			AddElement(rlPrintLine, olDemandFunction, imFuncLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omCourierNormal10);

			ilJob++;
			blNewPoolJob = false;
			blPrintEmpName = false;
			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
			PrintEndOfPage2();
		}
	}
*/
	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
void TrackSheet::PrintView3()
{  
	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) + CString(omEndTime.Format("%d.%m.%Y %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y %H:%M");
//	pomPrint = new CCSPrint(pomTable,PRINT_PORTRAIT,20,400,30,GetString(IDS_STAFFJOBS),olPrintDate,omTarget);
	pomPrint = new CCSPrint(pomParent,PRINT_PORTRAIT,60,400,50,GetString(IDS_STAFFJOBS),olPrintDate,omTarget);
	pomPrint->imMaxLines = 39;

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_PORTRAIT,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = 0;

			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			pomPrint->imLineHeight = 62;
			rlDocInfo.cbSize = sizeof( DOCINFO );
			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STAFFJOBS));
			rlDocInfo.lpszDocName = pclDocName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				PrintPrmLine3(&omLines[i]);
			}
			if(pomPrint->imLineNo != 0)
			{
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				PrintEndOfPage3();
			}
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

bool TrackSheet::PrintStartOfPage3()
{
	bool blStartOfPage = false;
	if(pomPrint->imLineNo == 0)
	{
		blStartOfPage = true;

		PrintPrmHeader(pomPrint);

		CCSPtrArray <PRINTELEDATA> rlPrintLine;
		PRINTELEDATA rlElement;

		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
		rlElement.FrameRight = PRINT_FRAMEMEDIUM;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
		rlElement.pFont		 = &pomPrint->omSmallFont_Regular;

		rlElement.Length     = imFuncLen3;
		rlElement.Text = GetString(IDS_STC_MFCT); // "M-Fct";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imShiftLen3;
		rlElement.Text = GetString(IDS_STC_SHIFT); // "Shift";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imNameLen3;
		rlElement.Text = GetString(IDS_STC_NAME); // "Name";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imBreakLen3;
		rlElement.Text = GetString(IDS_STC_BREAK); // "Break";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imJobLen3;
		rlElement.Text = GetString(IDS_STC_JOB); // "Job";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imRegnLen3;
		rlElement.Text = GetString(IDS_STC_REGN); // "AC-Regn";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imArrTimeLen3;
		rlElement.Text = GetString(IDS_STC_ARRTIME); // "STA / ETA";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imDepTimeLen3;
		rlElement.Text = GetString(IDS_STC_DEPTIME); // "STD / ETD";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imServiceLen3;
		rlElement.Text = GetString(IDS_STC_SERVICE); // "Service";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imGatePosLen3;
		rlElement.Text = GetString(IDS_STC_GATPOS); // "Gate/Pos";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();
	}

	return blStartOfPage;
}

void TrackSheet::PrintEndOfPage3()
{
	if(pomPrint->imLineNo >= pomPrint->imMaxLines &&  pomPrint->imPageNo > 0)
	{
		CCSPtrArray <PRINTELEDATA> rlPrintLine;
		PRINTELEDATA rlElement;

		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = imTotalLen3;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();

		PrintPrmLine(NULL,TRUE);
		pomPrint->PrintFooter("","");
		pomPrint->omCdc.EndPage();
		pomPrint->imLineNo = 0;
		pomPrint->imLineTop = pomPrint->imFirstLine;
	}
}

BOOL TrackSheet::PrintPrmLine3(TRACKSHEET_LINEDATA *prpLine)
{
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	DPXDATA *prlDpx = ogDpxData.GetDpxByUrno(prpLine->Udpx);
	if(prlDpx == NULL)
		return FALSE;

/*
	bool blPrintEmpName = true;
	int ilNumJobs;
	int ilJob = 0;

	CCSPtrArray <JOBDATA> olPoolJobs;
	ogJobData.GetPoolJobsByUstf(olPoolJobs, prlShift->Stfu);
	int ilNumPJ = olPoolJobs.GetSize(), ilPJ;
	for(ilPJ = (ilNumPJ-1); ilPJ >= 0; ilPJ--)
	{
		JOBDATA *prlPJ = &olPoolJobs[ilPJ];
		if(prlPJ->Shur != prlShift->Urno)
		{
			olPoolJobs.RemoveAt(ilPJ);
		}
	}

	JOBDATA *prlJob = NULL, *prlBreak = NULL;
	CString olBreakText;
	bool blNewPoolJob = true;
	CCSPtrArray <JOBDATA> olJobs;
	ilNumPJ = olPoolJobs.GetSize();
	for(ilPJ = 0; ilPJ < ilNumPJ; ilPJ++)
	{
		JOBDATA *prlPJ = &olPoolJobs[ilPJ];
		ogJobData.GetJobsByPoolJob(olJobs, prlPJ->Urno);
		ogJobData.GetJobsByPoolJobAndType(olJobs, prlPJ->Urno, JOBTEMPABSENCE, false);
		blNewPoolJob = true;

		// remove breaks and set the break job pointer
		ilNumJobs = olJobs.GetSize();
		prlBreak = NULL;
		for(ilJob = (ilNumJobs-1); ilJob >= 0; ilJob--)
		{
			prlJob = &olJobs[ilJob];
			if(!strcmp(prlJob->Jtco,JOBBREAK))
			{
				if(prlBreak == NULL) prlBreak = prlJob;
				olJobs.RemoveAt(ilJob);
			}
		}
		
		ilNumJobs = olJobs.GetSize();
		ilJob = 0;
		while(ilJob < ilNumJobs || blNewPoolJob)
		{
			if(ilJob < ilNumJobs)
				prlJob = &olJobs[ilJob];
			else
				prlJob = NULL;

			if(PrintStartOfPage3())
			{
				blPrintEmpName = true;
				blNewPoolJob = true;
			}

			CString olText;

			// main function
			AddElement(rlPrintLine, (blPrintEmpName) ? ogBasicData.GetFunctionForPoolJob(prlPJ) : "", imFuncLen3, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, (blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,
						PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// shift time
			AddElement(rlPrintLine, (blPrintEmpName) ? CString(prlShift->Sfca) : "", imShiftLen3, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, (blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,
						PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// employee name
			AddElement(rlPrintLine, (blPrintEmpName) ? prpLine->Name : "", imNameLen3, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME,
						(blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,	PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// break time
			if(blPrintEmpName && prlBreak)
				olBreakText.Format("%s%s", prlBreak->Acfr.Format("%H%M"), prlBreak->Acto.Format("%H%M"));
			else
				olBreakText.Empty();
			AddElement(rlPrintLine, olBreakText, imBreakLen3, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, (blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,
						PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			CString olRegn, olArrTime, olDepTime, olGatPos;
			CString olService, olDemandFunction;
			FLIGHTDATA *prlFlight = NULL, *prlRotation = NULL;
			if(prlJob != NULL)
			{
				if((prlFlight = ogFlightData.GetFlightByUrno(prlJob->Flur)) != NULL)
				{
					olRegn = prlFlight->Regn;
					if(ogFlightData.IsArrival(prlFlight))
					{
						olArrTime.Format("%s/%s", (prlFlight->Stoa != TIMENULL) ? prlFlight->Stoa.Format("%H%M") : "", (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa.Format("%H%M") : "");
						olGatPos.Format("%s/%s", prlFlight->Gta1, prlFlight->Psta);
					}
					else
					{
						olDepTime.Format("%s/%s", (prlFlight->Stod != TIMENULL) ? prlFlight->Stod.Format("%H%M") : "", (prlFlight->Etod != TIMENULL) ? prlFlight->Etod.Format("%H%M") : "");
						olGatPos.Format("%s/%s", prlFlight->Gtd1, prlFlight->Pstd);
					}
					if(ogJobData.IsTurnaroundJob(prlJob->Urno) && (prlRotation = ogFlightData.GetRotationFlight(prlFlight)) != NULL)
					{
						if(ogFlightData.IsArrival(prlRotation))
						{
							olArrTime.Format("%s/%s", (prlRotation->Stoa != TIMENULL) ? prlRotation->Stoa.Format("%H%M") : "", (prlRotation->Etoa != TIMENULL) ? prlRotation->Etoa.Format("%H%M") : "");
						}
						else
						{
							olDepTime.Format("%s/%s", (prlRotation->Stod != TIMENULL) ? prlRotation->Stod.Format("%H%M") : "", (prlRotation->Etod != TIMENULL) ? prlRotation->Etod.Format("%H%M") : "");
						}
					}
				}
				else
				{
					olArrTime.Format("%s/%s", prlJob->Acfr.Format("%H%M"), prlJob->Acto.Format("%H%M"));
				}
				DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prlJob);
				if(prlDemand != NULL)
				{
					RUDDATA *prlRud = ogRudData.GetRudByUrno(prlDemand->Urud);
					if(prlRud != NULL)
					{
						SERDATA *prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
						if(prlSer != NULL)
							olService = prlSer->Snam;
					}
					RPFDATA *prlDemandFunction = ogRpfData.GetFunctionByUrud(prlDemand->Urud);
					if(prlDemandFunction != NULL)
					{
						if(!strcmp(prlDemandFunction->Fcco,""))
						{
							SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(prlDemandFunction->Upfc);
							if(prlSgr != NULL)
							{
								olDemandFunction += CString("*") + prlSgr->Grpn;
							}
						}
						else
						{
							olDemandFunction += prlDemandFunction->Fcco;
						}
					}
				}
			}

			// flight regn
			AddElement(rlPrintLine, olRegn, imRegnLen3, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// flight arr time
			AddElement(rlPrintLine, olArrTime, imArrTimeLen3, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// flight dep time
			AddElement(rlPrintLine, olDepTime, imDepTimeLen3, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// service
			AddElement(rlPrintLine, olService, imServiceLen3, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// flight gate position
			AddElement(rlPrintLine, olGatPos, imGatePosLen3, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			ilJob++;
			blNewPoolJob = false;
			blPrintEmpName = false;
			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
			PrintEndOfPage3();
		}
	}
*/
	return TRUE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void TrackSheet::PrintViewConfigurable()
{  
	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) + CString(omEndTime.Format("%d.%m.%Y %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y %H:%M");
//#define DEBUG_PRINTING

#ifndef DEBUG_PRINTING
	pomPrint = new CCSPrint(pomParent,PRINT_LANDSCAPE,60,400,50,GetString(IDS_STAFFJOBS),olPrintDate,omTarget);
	pomPrint->imMaxLines = 20;
#endif

	imMaxLineWidth = 2760;
	imMaxLines = 32;
#ifndef DEBUG_PRINTING
	if (pomPrint != NULL)
#endif
	{
#ifndef DEBUG_PRINTING
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
#endif
		{
#ifndef DEBUG_PRINTING
			if(pomPrint->smPaperSize == DMPAPER_A3)
			{
				if(pomPrint->imOrientation == PRINT_PORTRAIT)
				{
					imMaxLineWidth = 2760;
					imMaxLines = 58;
				}
				else
				{
					imMaxLineWidth = 3980;
					imMaxLines = 38;
				}
			}
			else
			{
				if(pomPrint->imOrientation == PRINT_PORTRAIT)
				{
					imMaxLineWidth = 1920;
					imMaxLines = 38;
				}
				else
				{
					imMaxLineWidth = 2760;
					imMaxLines = 24;
				}
			}
			pomPrint->imMaxLines = imMaxLines;
#endif

			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
#ifndef DEBUG_PRINTING
		delete pomPrint;
		pomPrint = NULL;
#endif
	}
}

bool TrackSheet::CheckForNewPageConfigurable()
{
	bool blIsNewPage = false;
#ifndef DEBUG_PRINTING
	if(pomPrint->imLineNo >= pomPrint->imMaxLines )
	{
		if(pomPrint->imPageNo > 0)
		{
			// print end of previous page
			PrintEndOfPageConfigurable();
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			pomPrint->omCdc.EndPage();
			pomPrint->imLineTop = pomPrint->imFirstLine;
		}

		// print start of new page
		PrintPrmHeaderConfigurable(pomPrint);
		blIsNewPage = true;
	}
#endif

	return blIsNewPage;
}

void TrackSheet::PrintEndOfPageConfigurable()
{
#ifndef DEBUG_PRINTING
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = imMaxLineWidth;
	rlElement.FrameLeft  = PRINT_NOFRAME;
	rlElement.FrameRight = PRINT_NOFRAME;
	rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
	rlElement.FrameBottom= PRINT_NOFRAME;
	rlElement.pFont       = &pomPrint->omMediumFont_Bold;
	rlElement.Text.Empty();
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	PrintPrmLine(NULL,TRUE);
	pomPrint->PrintFooter("","");

	pomPrint->omCdc.EndPage();
#endif
}

BOOL TrackSheet::PrintPrmHeaderConfigurable(CCSPrint *pomPrint)
{
	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader();
	PrintColumnTitlesConfigurable();
	return TRUE;
}

bool TrackSheet::ConfigureHeader(void)
{
	imCharWidth = 18;
	imFirstJobField = -1;
	imNumJobsPerLine = 0;

	omHeaderConfig.RemoveAll();
	ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, omHeaderConfig, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", "", "","","","","","","","","","","","","","","","");

	int ilNumColumns = 0;
	int ilBodyWidth = 0;
	int ilJobWidth = 0;
	for(int ilFC = 0; ilFC < omHeaderConfig.GetSize(); ilFC++)
	{
		if(omHeaderConfig[ilFC].SortOrder == 1)
		{
			ilBodyWidth += (omHeaderConfig[ilFC].NumChars * imCharWidth);
		}
		else if(omHeaderConfig[ilFC].SortOrder == 2)
		{
			if(imFirstJobField == -1)
				imFirstJobField = ilFC;
			ilJobWidth += (omHeaderConfig[ilFC].NumChars * imCharWidth);
		}
	}
	if(ilBodyWidth <= 0 && ilJobWidth <= 0)
	{
		MessageBox(NULL,"Cancelling Print Job - the print format must be configured first.","",0);
		return false;
	}
	if((ilBodyWidth + ilJobWidth) > imMaxLineWidth)
	{
		MessageBox(NULL,"Cancelling Print Job - Printout too large for the paper size.\n\nSelect a larger paper size or reconfigure the print format.","",0);
		return false;
	}
	if(ilJobWidth > 0)
	{
		while((ilBodyWidth + (ilJobWidth * (imNumJobsPerLine+1))) < imMaxLineWidth)
			imNumJobsPerLine++;
	}
	imMaxLineWidth = ilBodyWidth + (ilJobWidth * imNumJobsPerLine);

	return true;
}

void TrackSheet::PrintColumnTitlesConfigurable()
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

#ifndef DEBUG_PRINTING
	if (pomPrint->imLineNo == 0)
#endif
	{
		rlElement.Alignment  = PRINT_CENTER;
		rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
		rlElement.FrameRight = PRINT_FRAMEMEDIUM;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
#ifndef DEBUG_PRINTING
		rlElement.pFont       = &pomPrint->omSmallFont_Bold;
#endif
		
		for(int ilFC = 0; ilFC < omHeaderConfig.GetSize() && (imNumJobsPerLine == 0 || ilFC < imFirstJobField); ilFC++)
		{
			rlElement.Text = omHeaderConfig[ilFC].Field;
			rlElement.Length = omHeaderConfig[ilFC].NumChars * imCharWidth;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
		for(int ilJ = 0; ilJ < imNumJobsPerLine; ilJ++)
		{
			for(ilFC = imFirstJobField; ilFC < omHeaderConfig.GetSize(); ilFC++)
			{
				rlElement.Text = omHeaderConfig[ilFC].Field;
				rlElement.Length = omHeaderConfig[ilFC].NumChars * imCharWidth;
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			}
		}
#ifndef DEBUG_PRINTING
		pomPrint->PrintLine(rlPrintLine);
#endif
		rlPrintLine.DeleteAll();
	}
}

BOOL TrackSheet::PrintPrmLineConfigurable(TRACKSHEET_LINEDATA *prpLine)
{
	DPXDATA *prlDpx = ogDpxData.GetDpxByUrno(prpLine->Udpx);
	if(prlDpx == NULL)
		return FALSE;
/*
	bool blNewEmp = true;
	int ilNumJobs;
	int ilJob = 0;

	CCSPtrArray <JOBDATA> olPoolJobs;
	ogJobData.GetPoolJobsByUstf(olPoolJobs, prlShift->Stfu);
	int ilNumPJ = olPoolJobs.GetSize(), ilPJ;
	for(ilPJ = (ilNumPJ-1); ilPJ >= 0; ilPJ--)
	{
		JOBDATA *prlPJ = &olPoolJobs[ilPJ];
		if(prlPJ->Shur != prlShift->Urno)
		{
			olPoolJobs.RemoveAt(ilPJ);
		}
	}

	CFieldDataArray olFieldConfig;

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.Alignment  = PRINT_LEFT;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_FRAMEMEDIUM;
#ifndef DEBUG_PRINTING
	rlElement.pFont       = &pomPrint->omSmallFont_Regular;
#endif

	CCSPtrArray <JOBDATA> olJobs;
	ilNumPJ = olPoolJobs.GetSize();
	for(ilPJ = 0; ilPJ < ilNumPJ; ilPJ++)
	{
		JOBDATA *prlPJ = &olPoolJobs[ilPJ];
		ogJobData.GetJobsByPoolJob(olJobs, prlPJ->Urno);
		ogJobData.GetJobsByPoolJobAndType(olJobs, prlPJ->Urno, JOBTEMPABSENCE, false);
		bool blNewPoolJob = true;
		ilJob = 0;
		ilNumJobs = olJobs.GetSize();
		
		while(ilJob < ilNumJobs || blNewPoolJob)
		{
			CString olShiftTime, olPJTime;
			olShiftTime.Format("%s-%s", prpLine->Acfr.Format("%H%M"), prpLine->Acto.Format("%H%M"));
			olPJTime.Format("%s-%s", prlPJ->Acfr.Format("%H%M"), prlPJ->Acto.Format("%H%M"));
			olFieldConfig.RemoveAll();
			ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, olFieldConfig, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", 
										(blNewEmp) ? prpLine->Rank : "", (blNewEmp) ? prpLine->Name : "", (blNewEmp) ? prpLine->Tmid : "", 
										(blNewEmp) ? prpLine->Sfca : "", (blNewEmp) ? olShiftTime : "", 
										 (blNewPoolJob) ? prlPJ->Alid : "", (blNewPoolJob) ? olPJTime : "",
										 "","","","","","","","","","");
			rlElement.FrameTop = (blNewEmp) ? PRINT_FRAMETHIN : PRINT_NOFRAME;
			rlElement.FrameBottom= PRINT_NOFRAME;

			for(int ilBodyColumns = 0; ilBodyColumns < olFieldConfig.GetSize() && (imNumJobsPerLine == 0 || ilBodyColumns < imFirstJobField); ilBodyColumns++)
			{
				rlElement.Text = olFieldConfig[ilBodyColumns].Text;
				rlElement.Length = olFieldConfig[ilBodyColumns].NumChars * imCharWidth;
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			}

			blNewPoolJob = false;
			// end pool job

			rlElement.FrameTop   = PRINT_FRAMETHIN;
			rlElement.FrameBottom= PRINT_FRAMETHIN;

			if(imNumJobsPerLine <= 0)
			{
				ilJob = ilNumJobs;
			}
			else
			{
			}
			if(!CheckForNewPageConfigurable())
				blNewEmp = false;
#ifndef DEBUG_PRINTING
			pomPrint->PrintLine(rlPrintLine);
#endif
			rlPrintLine.DeleteAll();
		}
	}
*/
	return TRUE;
}

void TrackSheet::PrintPreview(const int& ripPageNo)
{
	if(ripPageNo <= 0)
		return;

	int ilLineNo = 0;
	ilLineNo = 0;
	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) + CString(omEndTime.Format("%d.%m.%Y %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y %H:%M");
	if (pomPrint != NULL)
	{
		pomPrint->imMaxLines = 36;	
		//pomPrint->imLineNo = (ripPageNo-1) * pomPrint->imMaxLines;
		imMaxJobsPerLine = 3;
	
		PrintPrmHeader(pomPrint);
		pomPrint->imPageNo = ripPageNo;
		PrintTableName(pomPrint, CString("DETAILS PASSENGER HANDLING"));
		int i;
		for(i = (ripPageNo-1) * pomPrint->imMaxLines; (pomPrint->imLineNo < pomPrint->imMaxLines) && i < omLines.GetSize(); i++ ) 
		//for(int i = ilLineNo ; pomPrint->imLineNo < pomPrint->imMaxLines && pomPrint->imLineNo < omLines.GetSize() ; i++)
		{
			bmPRMPrinted = TRUE;
			bmCrewPrinted = FALSE;
			PrintPrmLine(&omLines[i],FALSE);
			ilLineNo++;
		}
		if (pomPrint->imLineNo + imCrewAckLines + imArrTeamLines <= pomPrint->imMaxLines)
		{
			if (!bmPRMPrinted || ilLineNo > 0)
			{
				bmCrewPrinted = FALSE;
				PrintPrmLine(NULL,FALSE);
			
				//Print Nil Case Line;
				if(ilLineNo ==0)
				PrintNilCasePrmLine();

				for(i = pomPrint->imLineNo;i<pomPrint->imMaxLines-imCrewAckLines-imArrTeamLines;i++)
				{				
					PrintEmptyPrmLine();
				}
			}
			PrintPrmLine(NULL,TRUE);
		
			
			if (!bmCrewPrinted)
				PrintCrewAck(pomPrint);
			PrintArrTeam(pomPrint);
		}
		else if (pomPrint->imLineNo + imCrewAckLines<= pomPrint->imMaxLines)
		{
			for(i = pomPrint->imLineNo;i<pomPrint->imMaxLines-imCrewAckLines;i++)
			{
				PrintEmptyPrmLine();
			}
			PrintPrmLine(NULL,TRUE);		
			PrintCrewAck(pomPrint);
		}
		else if (pomPrint->imLineNo <= pomPrint->imMaxLines)
		{
			for(i = pomPrint->imLineNo;i<pomPrint->imMaxLines;i++)
			{
				PrintEmptyPrmLine();
			}
			PrintPrmLine(NULL,TRUE);		
		}

		pomPrint->PrintFooter("","");		
		pomPrint->pomCdc->EndPage();
		pomPrint->imLineTop = pomPrint->imFirstLine;
		pomPrint->imLineNo = 0;	
	}
}