#if !defined(AFX_DLGSETTINGS_H__656A0D5D_2930_4A73_BC98_82EBDE97C586__INCLUDED_)
#define AFX_DLGSETTINGS_H__656A0D5D_2930_4A73_BC98_82EBDE97C586__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgSettings.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgSettings dialog

class CDlgSettings
{
// Construction
public:
	CDlgSettings(CWnd* pParent = NULL);   // standard constructor
	~CDlgSettings();
private:
	CMapStringToPtr omDlgMap;
	CMapStringToPtr omSaveToDb; // list of which dialog require their settings to be saved to the DB
	CStringArray omDeletedDialogs;
public:
	CString GetFieldName(UINT ipFieldId);
	void EnableSaveToDb(const char *pcpDlgName);
	bool SaveToDbEnabled(const char *pcpDlgName);
	void LoadSettingsFromDb(const char *pcpDlgName);
	void CheckIfLoadedFromDb(const char *pcpDlgName);
	void SaveSettingsToDb(void);
	void AddValue(const char *pcpDlgName, const char *pcpFieldName, const char *pcpValue);
	bool GetValue(const char *pcpDlgName, const char *pcpFieldName, CString &ropValue);
	CString GetValue(const char *pcpDlgName, UINT ipFieldId);
	void GetAllValuesForDialog(const char *pcpDlgName, CStringArray &ropFieldNames, CStringArray &ropValues);
	int GetRadioButtonValue(const char *pcpDlgName, UINT ipFieldId);
	BOOL GetCheckBoxValue(const char *pcpDlgName, UINT ipFieldId);
	void DeleteSettingsByDialog(const char *pcpDlgName);
	void SetFieldValue(CDialog *popDlg, const char *pcpDlgName, UINT ipFieldId);
	void SetCheckboxValue(CDialog *popDlg, const char *pcpDlgName, UINT ipFieldId);
	void SaveFieldValue(CDialog *popDlg, const char *pcpDlgName, UINT ipFieldId);
	void SaveCheckboxValue(CDialog *popDlg, const char *pcpDlgName, UINT ipFieldId);
	void SaveRadioButtonValue(const char *pcpDlgName, UINT ipFieldId, int ipValue);

	static const CString CTYP_OTHER_COLOURS;
	static const CString CKEY_OTHER_COLOURS_PRM_FINALIZED;
//	static const CString CKEY_OTHER_COLOURS_PRM_LOCATION_LOUNGE;

};

extern CDlgSettings ogDlgSettings;

#endif // !defined(AFX_DLGSETTINGS_H__656A0D5D_2930_4A73_BC98_82EBDE97C586__INCLUDED_)
