// SelectShiftDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <SelectShiftDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectShiftDlg dialog


CSelectShiftDlg::CSelectShiftDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectShiftDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectShiftDlg)
	m_Sort = 0;
	//}}AFX_DATA_INIT

	prmSelectedShift = NULL;
}


void CSelectShiftDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectShiftDlg)
	DDX_Control(pDX, IDC_SHIFTLIST, m_ShiftListCtrl);
	DDX_Radio(pDX, IDC_SORTBYSHIFT, m_Sort);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectShiftDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectShiftDlg)
	ON_LBN_DBLCLK(IDC_SHIFTLIST, OnDblclkShiftlist)
	ON_BN_CLICKED(IDC_SORTBYEMP, OnSortByEmp)
	ON_BN_CLICKED(IDC_SORTBYSHIFT, OnSortByShift)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectShiftDlg message handlers

void CSelectShiftDlg::SetSday(CString opSday)
{
	omSday = opSday;
}


BOOL CSelectShiftDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_SELECTSHIFTDLG));
	DisplayShifts();
	
	return TRUE;
}

void CSelectShiftDlg::DisplayShifts()
{
	UpdateData();
	m_ShiftListCtrl.ResetContent();
	SHIFTDATA *prlShift = NULL;
	int ilIndex = LB_ERR;
	CString olText;
	int ilNumShifts = pomShifts->GetSize();
	for(int ilS = 0; ilS < ilNumShifts; ilS++)
	{
		prlShift = &((*pomShifts)[ilS]);
		if(!strcmp(prlShift->Sday, omSday) && !strcmp(prlShift->Drrn,"1"))
		{
			if(m_Sort == 0)
			{
				olText.Format("%s-%s   %s", prlShift->Avfa.Format("%d/%H%M"), prlShift->Avta.Format("%d/%H%M"), ogEmpData.GetEmpName(prlShift->Stfu));
			}
			else
			{
				olText.Format("%s  %s-%s", ogEmpData.GetEmpName(prlShift->Stfu), prlShift->Avfa.Format("%d/%H%M"), prlShift->Avta.Format("%d/%H%M"));
			}
			if((ilIndex = m_ShiftListCtrl.AddString(olText)) != LB_ERR)
			{
				m_ShiftListCtrl.SetItemDataPtr(ilIndex, prlShift);
			}
		}
	}
}

void CSelectShiftDlg::OnOK() 
{
	int ilSelLine = m_ShiftListCtrl.GetCurSel();
	if(ilSelLine != LB_ERR && ilSelLine >= 0)
	{
		prmSelectedShift = (SHIFTDATA *) m_ShiftListCtrl.GetItemDataPtr(ilSelLine);
	}
	
	CDialog::OnOK();
}

void CSelectShiftDlg::OnDblclkShiftlist() 
{
	OnOK();
}

void CSelectShiftDlg::OnSortByEmp() 
{
	DisplayShifts();
}

void CSelectShiftDlg::OnSortByShift() 
{
	DisplayShifts();
}
