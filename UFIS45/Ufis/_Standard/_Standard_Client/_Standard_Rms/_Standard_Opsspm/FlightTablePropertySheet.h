// FlightTablePropertySheet.h : header file
//

#ifndef _FLPLANPS_H_
#define _FLPLANPS_H_

/////////////////////////////////////////////////////////////////////////////
// FlightTablePropertySheet

class FlightTablePropertySheet : public BasePropertySheet
{
// Construction
public:
	FlightTablePropertySheet(CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	FilterPage m_pageAirline;
	FilterPage m_pageArrival;
	FilterPage m_pageDeparture;
	FilterPage m_pagePosArr;
	FilterPage m_pagePosDep;
	FlightTableBoundFilterPage m_pageBound;
	FlightTableSortPage m_pageSort;
//	ZeitPage m_pageZeit;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();
};

/////////////////////////////////////////////////////////////////////////////

#endif // _FLPLANPS_H_
