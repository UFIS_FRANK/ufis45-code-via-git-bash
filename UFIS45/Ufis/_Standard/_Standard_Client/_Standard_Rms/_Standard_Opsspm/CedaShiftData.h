#ifndef _CSHIFTD_H_
#define _CSHIFTD_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

#define MITARBEITER_TAGESLISTE "3"
#define LANGZEITSTUFE "L"
#define LAST_RECORD "L"

struct SHIFTDATA {

	long	Urno;			// Unique Record Number in EMPCKI

	char	Peno[21];		// Employee PK-number
	long	Stfu;			// URNO of Employee in STFTAB;

	char	Sday[9];		// Duty Date (YYYYMMDD)

	CTime	Avfa;			// Actual begin of Duty
	CTime	Avta;			// Actual end of Duty

	char	Sfca[9];		// Shift Code (Current)
	char	Sfco[9];		// Shift Code (Old)
	long	Bsdu;			// URNO of shift code in BSDTAB or absence code in ODATAB
	char	Bkdp[2];		// Break paid flag 1=paid or blank

	CTime	Sbfr;			// Pausenlage From
	CTime	Sbto;			// Pausenlage To

	char	Ctyp[2];		// What Sfca/Sfcs containa -> 'A'=Absence Code 'S'=Shift Code
	char	Egrp[6];		// Group Code (STF.STFU -> SWG.SURN -> SWG.CODE)
	char	Efct[6];		// Dienstrang eg GF,LA,FA etc
	char	Rema[258];		// Remark
	char	Rosl[2];		// 3=Mitarbeieter Tagesliste
	char	Ross[2];		// <> 'L' where L=Last ie previous stufe

	char	Sblu[5];		// pause duration
	char	Fctc[9];		// function code

	CTime	Avfs;			// Scheduled begin of Duty
	CTime	Avts;			// Shceduled end of Duty
	char	Sfcs[9];		// Schedulted Shift Code (Actual) (Also contains absence codes)

	char	Usec[33];		// User that created this record
	CTime	Cdat;			// Time of creation
	char	Useu[33];		// User that last updated this record
	CTime	Lstu;			// Time of last update

	char	Drs1[2];		// "1" = Overtime at the start of the shift
	char	Drs2[9];		// "1" = employer changed shift  "2" = employee changed shift
	char	Drs3[2];		// "1" = Overtime at the end of the shift
	char	Drs4[2];		// "1" = the complete shift is overtime

	char	Drrn[2];		// "1" = the complete shift is overtime

	SHIFTDATA(void) 
	{	
		memset(this,'\0',sizeof(*this));
		Avfa=TIMENULL;
		Avta=TIMENULL;
		Sbfr=TIMENULL;
		Sbto=TIMENULL;
		Lstu=TIMENULL;
		Avfs=TIMENULL;
		Avts=TIMENULL;
	}


	SHIFTDATA& SHIFTDATA::operator=(const SHIFTDATA& rhs)
	{
		if (&rhs != this)
		{
			Urno = rhs.Urno;
			strcpy(Peno,rhs.Peno);
			Stfu = rhs.Stfu;
			strcpy(Sday,rhs.Sday);
			Avfa = rhs.Avfa;
			Avta = rhs.Avta;
			strcpy(Sfca,rhs.Sfca);
			strcpy(Sfco,rhs.Sfco);
			Bsdu = rhs.Bsdu;
			strcpy(Bkdp,rhs.Bkdp);
			Sbfr = rhs.Sbfr;
			Sbto = rhs.Sbto;
			strcpy(Ctyp,rhs.Ctyp);
			strcpy(Egrp,rhs.Egrp);
			strcpy(Efct,rhs.Efct);
			strcpy(Rema,rhs.Rema);
			strcpy(Rosl,rhs.Rosl);
			strcpy(Ross,rhs.Ross);
			strcpy(Sblu,rhs.Sblu);
			strcpy(Fctc,rhs.Fctc);
			Avfs = rhs.Avfs;
			Avts = rhs.Avts;
			strcpy(Sfcs,rhs.Sfcs);
			strcpy(Usec,rhs.Usec);
			Cdat = rhs.Cdat;
			strcpy(Useu,rhs.Useu);
			Lstu = rhs.Lstu;
			strcpy(Drs1,rhs.Drs1);
			strcpy(Drs2,rhs.Drs2);
			strcpy(Drs3,rhs.Drs3);
			strcpy(Drs4,rhs.Drs4);
			strcpy(Drrn,rhs.Drrn);
		}		
		return *this;
	}

	friend bool operator==(const SHIFTDATA& rrp1,const SHIFTDATA& rrp2)
	{
		return
			rrp1.Urno == rrp2.Urno &&
			!strcmp(rrp1.Peno,rrp2.Peno) &&
			rrp1.Stfu == rrp2.Stfu &&
			!strcmp(rrp1.Sday,rrp2.Sday) &&
			rrp1.Avfa == rrp2.Avfa &&
			rrp1.Avta == rrp2.Avta &&
			!strcmp(rrp1.Sfca,rrp2.Sfca) &&
			!strcmp(rrp1.Sfco,rrp2.Sfco) &&
			rrp1.Bsdu == rrp2.Bsdu &&
			!strcmp(rrp1.Bkdp,rrp2.Bkdp) &&
			rrp1.Sbfr == rrp2.Sbfr &&
			rrp1.Sbto == rrp2.Sbto &&
			!strcmp(rrp1.Ctyp,rrp2.Ctyp) &&
			!strcmp(rrp1.Egrp,rrp2.Egrp) &&
			!strcmp(rrp1.Efct,rrp2.Efct) &&
			!strcmp(rrp1.Rema,rrp2.Rema) &&
			!strcmp(rrp1.Rosl,rrp2.Rosl) &&
			!strcmp(rrp1.Ross,rrp2.Ross) &&
			!strcmp(rrp1.Sblu,rrp2.Sblu) &&
			!strcmp(rrp1.Fctc,rrp2.Fctc) &&
			rrp1.Avfs == rrp2.Avfs &&
			rrp1.Avts == rrp2.Avts &&
			!strcmp(rrp1.Sfcs,rrp2.Sfcs) &&
			!strcmp(rrp1.Usec,rrp2.Usec) &&
			rrp1.Cdat == rrp2.Cdat &&
			!strcmp(rrp1.Useu,rrp2.Useu) &&
			!strcmp(rrp1.Drs1,rrp2.Drs1) &&
			!strcmp(rrp1.Drs2,rrp2.Drs2) &&
			!strcmp(rrp1.Drs3,rrp2.Drs3) &&
			!strcmp(rrp1.Drs4,rrp2.Drs4) &&
			!strcmp(rrp1.Drrn,rrp2.Drrn) &&
			rrp1.Lstu == rrp2.Lstu;
	}

	friend bool operator!=(const SHIFTDATA& rrp1,const SHIFTDATA& r2)
	{
		return !operator==(rrp1,r2);
	}
};	



//typedef struct ShiftDataStruct SHIFTDATA;

// the broadcast CallBack function, has to be outside the CedaShiftData class
void ProcessShiftCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
void ProcessShiftCf2(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaShiftData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<SHIFTDATA> omData;
    CCSPtrArray<SHIFTDATA> omDataLangzeit;
    CMapStringToPtr omPenoMap;
    CMapPtrToPtr    omUrnoMap;
	CString GetTableName(void);
	CString Dump(long lpUrno);

	CMapPtrToPtr omUstfMap; // sub map indexed by SDAY
	CMapPtrToPtr omUstfMapLangzeit; // sub map indexed by SDAY where ROSL='L' (scheduled SCOD,AVFR,AVTO)

// Operations
public:
    CedaShiftData();
	~CedaShiftData();
    
	void newinsert(SHIFTDATA *prpShiftData);
  	void DeleteShiftInternal(SHIFTDATA *prpShift);
	bool ReadAllShifts(CTime opStartTime, CTime opEndTime);
	void PrepareShiftData(SHIFTDATA *prpShift);
	void AddShift(SHIFTDATA *prpShift);
	void AddShiftInternal(SHIFTDATA *prpShift);
	bool LoadShift(SHIFTDATA *prpShift);
    CCSReturnCode UpdateShift(SHIFTDATA *prpShiftData, SHIFTDATA *prpOldShiftData);    // used in PrePlanTable only
	void ProcessShiftBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void ProcessShiftDdx(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CCSReturnCode InsertShift(SHIFTDATA *prpShiftData);
	SHIFTDATA *GetShiftByUrno(long lpUrno);
	SHIFTDATA *GetShiftByPeno(CString opPkno);
	SHIFTDATA *GetShiftByUstfAndSdayAndRosl(long lpUstf, const char *pcpSday, const char *pcpRosl, const char *pcpDrrn);
	int GetShiftsByUstf(long lpUstf, CCSPtrArray <SHIFTDATA> &ropShifts, bool bpReset = true);
	bool IsAbsent(SHIFTDATA *prpShift);
	CTime omLoadShiftsFrom, omLoadShiftsTo;
	int imNumShiftsLoaded, imNumAbsencesLoaded;
	void DeleteShiftsByUstfSdayAndRosl(CStringArray &ropStfUrnos, CStringArray &ropRoslList, CString opFrom, CString opTo, CDWordArray &ropDeletedUrnos);
	void ReloadShifts(const char *pcpStfUrnoList, CStringArray &ropRoslList);
	void ProcessDienstPlanUpdate(struct BcStruct *prpShiftData);
	bool ProcessShiftUpdate(long lpShiftUrno, char *pcpFields, char *pcpData);
	void ProcessShiftInsert(char *pcpFields, char *pcpData);
	void ProcessShiftDelete(long lpShiftUrno);
	bool ProcessAttachment(CString &ropAttachment);
	void DeleteData();
	bool SwapShifts(SHIFTDATA *prpShift1, CString opFunction1, CString opWorkGroup1, CString opSub1,
				    SHIFTDATA *prpShift2, CString opFunction2, CString opWorkGroup2, CString opSub2);
	bool ReleaseChanges(SHIFTDATA *prpShift1, SHIFTDATA *prpShift2);
	bool Release(CString opReleaseString, bool bpBroadcast = true);
	CString FormatSdayDrrn(const char *pcpSday, const char *pcpDrrn);
	int GetMaxFieldLength(const CString &ropField);

private:
    BOOL ShiftExist(long lpUrno);
    CCSReturnCode InsertShiftRecord(SHIFTDATA *prpShiftData);
  

	

    CCSReturnCode UpdateShiftRecord(SHIFTDATA *prpShiftData, SHIFTDATA *prpOldShiftData);
	bool GetChangedFields(SHIFTDATA *prpShift, SHIFTDATA *prpOldShift, char *pcpFieldList, char *pcpData);

	char pcmData[2028];
	char pcmInsertFields[200];
	char pcmUpdateFields[200];
	char pcmInsertCommand[10];
	char pcmUpdateCommand[10];
	void ConvertDatesToUtc(SHIFTDATA *prpShift);
	void ConvertDatesToLocal(SHIFTDATA *prpShift);
};

#endif
