// FlightMgrDetailWindow.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <table.h>

#include <ccsglobl.h>
#include <CCSPtrArray.h>

#include <CCSCedaData.h>
#include <CedaAloData.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CCSDragDropCtrl.h>

#include <cviewer.h>
#include <FlightMgrDetailViewer.h>
#include <FlightMgrDetailWindow.h>
#include <Dataset.h>
#include <ccsddx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

int FlightMgrDetailWindow::imCount = 0;

// Prototypes
static void FlightMgrDetailWindowCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// FlightMgrDetailWindow dialog

FlightMgrDetailWindow::FlightMgrDetailWindow(CWnd* pParent, char *pcpAlid,
	BOOL bpArrival, BOOL bpDeparture, CTime opStartTime, CTime opEndTime)
	: CDialog(FlightMgrDetailWindow::IDD, pParent)
{
	if (imCount > 0)
		return;

	imCount++;

	pcmAlid = &cmAlid[0];
	strcpy(pcmAlid, pcpAlid);
	bmArrival = bpArrival;
	bmDeparture = bmDeparture;
	omStartTime = opStartTime;
	omEndTime = opEndTime;

	Create(IDD, pParent);
	CenterWindow(pParent);

	m_nDialogBarHeight = 40;

	omTable.tempFlag = 2;
   	omViewer.Attach(&omTable,omStartTime,omEndTime,pcmAlid);

	//CString olCaption = CString("Flightmanager Information: ") + pcpAlid;
	CString olCaption = GetString(IDS_STRING61678) + pcpAlid;
	SetWindowText(olCaption);
    CRect rect;
	GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
	
	//GetDlgItem(IDC_TABLE_GATE)->GetClientRect(&rect);
    omTable.SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	omTable.SetSelectMode(0); // no multiple selection

	omViewer.UpdateDisplay();
	omTable.DisplayTable();

	//{{AFX_DATA_INIT(FlightMgrDetailWindow)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

FlightMgrDetailWindow::~FlightMgrDetailWindow()
{
	imCount--;
}

void FlightMgrDetailWindow::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FlightMgrDetailWindow)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FlightMgrDetailWindow, CDialog)
	//{{AFX_MSG_MAP(FlightMgrDetailWindow)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
	ON_COMMAND(11, OnMenuDelete)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FlightMgrDetailWindow message handlers

int FlightMgrDetailWindow::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	CWnd *polWnd = omTable.GetCTableListBox();
    m_DragDropTarget.RegisterTarget(this, this);

	// Register DDX call back function
	TRACE("FlightMgrDetailWindow: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("PPTEAMTABLE"),CString("Redisplay all from What-If"), FlightMgrDetailWindowCf);

	return 0;
}

void FlightMgrDetailWindow::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	// Unregister DDX call back function
	TRACE("FlightMgrDetailWindow: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	m_DragDropTarget.Revoke();
	CDialog::OnDestroy();
	delete this;
}

void FlightMgrDetailWindow::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	DestroyWindow();
}

void FlightMgrDetailWindow::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
    if (nType != SIZE_MINIMIZED)
		omTable.SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
}

LONG FlightMgrDetailWindow::OnTableRButtonDown(UINT ipItem, LONG lpLParam) 
{
	CMenu menu;
	CPoint olPoint(LOWORD(lpLParam),HIWORD(lpLParam));

	FMDW_LINEDATA *prlLine = (FMDW_LINEDATA *)omTable.GetTextLineData(ipItem);
	if (prlLine != NULL)
	{
		int ilLc;

		menu.CreatePopupMenu();
		for (ilLc = menu.GetMenuItemCount(); --ilLc >= 0;)
			menu.RemoveMenu(ilLc, MF_BYPOSITION);
		int ilAssignment = omTable.imCurrentColumn - 3;
		if ((ilAssignment < prlLine->Assignment.GetSize()) && (ilAssignment > -1))
		{
			//CString olMenuText = "L�schen: Flug " + prlLine->Assignment[ilAssignment].Fnum;
			CString olMenuText = GetString(IDS_STRING61368) + prlLine->Assignment[ilAssignment].Fnum;
			menu.AppendMenu(MF_STRING,11, olMenuText);	

			ClientToScreen( &olPoint);
			menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y + 55, this, NULL);
			omContextItem = ipItem;
			omContextJobUrno = prlLine->Assignment[ilAssignment].JobUrno;
		}
	}
	return 0L;
}

void FlightMgrDetailWindow::OnMenuDelete()
{
	if (omContextItem != -1)
	{
		ogDataSet.DeleteJob(this, omContextJobUrno);
	}
	omContextItem = -1;
}

////////////////////////////////////////////////////////////////////////
// FlightMgrDetailWindow -- implementation of DDX call back function

static void FlightMgrDetailWindowCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	FlightMgrDetailWindow *polTable = (FlightMgrDetailWindow *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
		polTable->DestroyWindow();
}

/////////////////////////////////////////////////////////////////////////////
// FlightMgrDetailWindow -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a JobFlightManager (JTCO = "FMJ").
//		(It's DIT_DUTYBAR when the user drag from this detail window).
//
// and the user will have opportunities to:
//	-- drop from FlightBar onto any non-empty lines.
//		(Check the correct GateArea-Pool combination before dropping)
//		We will create JobFlightManagers for that manager (assigned to that flight).
//	-- drop from the background area from FlightDetailWindow onto any non-empty lines.
//		We will create JobFlightManagers for that manager (assigned to that flight).
//	-- drop from DemandBar onto any non-empty lines.
//		We will create JobFlightManagers for that manager (assigned to that flight).
//
LONG FlightMgrDetailWindow::OnTableDragBegin(UINT wParam, LONG lParam)
{
	CListBox *pListBox = (CListBox *)lParam;
	int ilIndex = pListBox->GetCurSel();
	if (ilIndex != LB_ERR)
	{
		FMDW_LINEDATA *prlLine = (FMDW_LINEDATA *)omTable.GetTextLineData(ilIndex);
		if (prlLine != NULL)
		{
			omDragDropObject.CreateDWordData(DIT_FMDETAIL,1);
			omDragDropObject.AddDWord(prlLine->DutyUrno);
			omDragDropObject.BeginDrag();
			pListBox->SetSel(-1,FALSE);
		}
	}
    return 0L;
}

LONG FlightMgrDetailWindow::OnDragOver(UINT wParam, LONG lParam)
{
    int ilClass = m_DragDropTarget.GetDataClass(); 
	if (ilClass != DIT_FLIGHTBAR)
		return -1L;	// cannot interpret this object

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	if (omTable.GetLinenoFromPoint(olDropPosition) == -1)
		return -1L;

	return 0L;
}

LONG FlightMgrDetailWindow::OnDrop(UINT wParam, LONG lParam)
{
    int ilClass = m_DragDropTarget.GetDataClass(); 
	if (ilClass != DIT_FLIGHTBAR)
		return -1L;	// cannot interpret this object

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);

	long llFlightUrno = m_DragDropTarget.GetDataDWord(0);
	FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(llFlightUrno);
	if (prlFlight != NULL)
	{
		if (ogFlightData.IsArrival(prlFlight) && ogFlightData.IsTurnaround(prlFlight))
		{
			prlFlight = ogFlightData.GetRotationFlight(prlFlight);
			if (prlFlight != NULL)
			{
				llFlightUrno = prlFlight->Urno;
			}

		}

		int ilLineno = omTable.GetLinenoFromPoint(olDropPosition);
		if (ilLineno >= 0 && ilLineno < omViewer.omLines.GetSize())
		{
			ogDataSet.CreateSpecialFlightManagerJob(this,omViewer.omLines[ilLineno].DutyUrno,llFlightUrno);
		}
	}
	return 0L;
}

BOOL FlightMgrDetailWindow::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText(GetString(IDS_STRING32975));
	CWnd *polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32882));
	}
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
