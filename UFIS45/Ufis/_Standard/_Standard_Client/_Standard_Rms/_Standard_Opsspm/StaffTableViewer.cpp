
// Sfviewer.cpp : implementation file
//
// Modification History:
// 19-Sep-96	Damkerng	Change many places since our database is changed.
//							On this change, special flight job was changed from type
//							FLT to FLS. The field PRID which contains a word "SPECIAL"
//							is not used any more. The field FLUR which was used for
//							storing shift URNO now moved to the field SHUR.
//							Check these places out by searching for "Id 19-Sep-96"

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaRudData.h>
#include <CedaSerData.h>
#include <CedaDraData.h>
#include <CedaDelData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <StaffTableViewer.h>
#include <PrintControl.h>
#include <Dataset.h>
#include <Search.h>
// stream defs
#include <fstream.h>
#include <iomanip.h>

extern bool BGS;

#include <StaffTable.h> //Singapore
#include <CedaShiftTypeData.h>  //Singapore

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static int ByJobStartTime(const JOBDATA **pppJob1, const JOBDATA **pppJob2);
static int ByJobStartTime(const JOBDATA **pppJob1, const JOBDATA **pppJob2)
{
	if(!strcmp((**pppJob1).Jtco,JOBPOOL))
		return -1;
	if(!strcmp((**pppJob2).Jtco,JOBPOOL))
		return 1;
	return (int)((**pppJob1).Acfr.GetTime() - (**pppJob2).Acfr.GetTime());
}

// Grouping and Sorting criterias definition:
// This will make the program faster a little bit because it will replace string comparison.
enum {
	BY_SHIFTSTART,
	BY_SHIFTEND,
    BY_SHIFTCODE,
    BY_TEAM,
    BY_NAME,
    BY_RANK,
    BY_NONE
};

// To know which rank is higher in comparison of two staff, I provide a colon-separated
// string from the lowest rank to the highest rank. By searching for a string ":XX:"
// in "pclRanks", if XX is the given rank, we could easily see which rank is higher.
// By using the same technic, we could see if the given rank is a manager by searching
// for a string ":XX:" in "ogBasicData.omManager".
//

/////////////////////////////////////////////////////////////////////////////
// StaffTableViewer
//

StaffTableViewer::StaffTableViewer()
{
	bmUseAllShiftCodes = false;
	bmUseAllRanks = false;
	bmUseAllPermits = false;
	bmHideAbsentEmps = false;
	bmHideFidEmps = false;
	bmHideStandbyEmps = false;
	bmIsFromSearch = FALSE;
    SetViewerKey("StaffTab");
    pomTable = NULL;
    ogCCSDdx.Register(this, SHIFT_NEW, CString("SFVIEWER"), CString("Shift New"), StaffTableTableCf);
	ogCCSDdx.Register(this, SHIFT_CHANGE, CString("SFVIEWER"), CString("Shift Change"), StaffTableTableCf);
	ogCCSDdx.Register(this, SHIFT_DELETE, CString("SFVIEWER"), CString("Shift Delete"), StaffTableTableCf);
    ogCCSDdx.Register(this, JOB_NEW, CString("SFVIEWER"), CString("Job New"), StaffTableTableCf);
    ogCCSDdx.Register(this, JOB_CHANGE, CString("SFVIEWER"), CString("Job Change"), StaffTableTableCf);
    ogCCSDdx.Register(this, JOB_DELETE, CString("SFVIEWER"), CString("Job Delete"), StaffTableTableCf);
	ogCCSDdx.Register(this, SHIFT_SELECT_EMPLIST, CString("SFVIEWER"), CString("Shift Select"), StaffTableTableCf);

	CTime olDay = ogBasicData.GetTimeframeStart();
	omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omEndTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);
	imColumns = 4;	// initial column size


	imCharWidth = 24;
	imFuncLen2 = 6 * imCharWidth; 
	imNameLen2 = 26 * imCharWidth; 
	imWorkGroupLen2 = 9 * imCharWidth; 
	imShiftLen2 = 9 * imCharWidth; 
	imJobLen2 = 14 * imCharWidth; 
	imRegnLen2 = 7 * imCharWidth; 
	imArrTimeLen2 = 9 * imCharWidth; 
	imDepTimeLen2 = 9 * imCharWidth; 
	imGatePosLen2 = 8 * imCharWidth; 
	imServiceLen2 = 12 * imCharWidth;

	imCharWidth = 23;
	imTotalLen3 = 0;
	imFuncLen3 = 5 * imCharWidth; imTotalLen3 += imFuncLen3;
	imShiftLen3 = 7 * imCharWidth; imTotalLen3 += imShiftLen3; 
	imNameLen3 = 15 * imCharWidth; imTotalLen3 += imNameLen3; 
	imBreakLen3 = 7 * imCharWidth; imTotalLen3 += imBreakLen3; 
	imJobLen3 = 11 * imCharWidth; imTotalLen3 += imJobLen3; 
	imRegnLen3 = 6 * imCharWidth; imTotalLen3 += imRegnLen3; 
	imArrTimeLen3 = 7 * imCharWidth; imTotalLen3 += imArrTimeLen3; 
	imDepTimeLen3 = 7 * imCharWidth; imTotalLen3 += imDepTimeLen3; 
	imServiceLen3 = 12 * imCharWidth; imTotalLen3 += imServiceLen3;
	imGatePosLen3 = 6 * imCharWidth; imTotalLen3 += imGatePosLen3; 
}

StaffTableViewer::~StaffTableViewer()
{
	ogCCSDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void StaffTableViewer::Attach(CTable *popTable)
{
    pomTable = popTable;
}

void StaffTableViewer::ChangeViewTo(const char *pcpViewName)
{
	ogCfgData.rmUserSetup.STLV = pcpViewName;
    SelectView(pcpViewName);
	PrepareGrouping();
    PrepareFilter();
    PrepareSorter();

    DeleteAll();    
    MakeLines();
	if (bmIsFromSearch == TRUE && omLines.GetSize() < 1)
	{
		//MessageBox(NULL,"Alle gefundenen Mitarbeiter \nsind nicht eingeteilt!","MA-Suche",MB_OK);
		MessageBox(NULL,GetString(IDS_STRING61503),GetString(IDS_STRING61414),MB_OK);
	}

	UpdateDisplay();
}

void StaffTableViewer::PrepareGrouping()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);  // we'll always use the first sorting criteria for grouping
	BOOL blIsGrouped = *CViewer::GetGroup() - '0';

    omGroupBy = BY_NONE;
    if (olSortOrder.GetSize() > 0 && blIsGrouped)
    {
        if (olSortOrder[0] == "Schichtbegin")
            omGroupBy = BY_SHIFTSTART;
        else if (olSortOrder[0] == "Schichtend")
            omGroupBy = BY_SHIFTEND;
        else if (olSortOrder[0] == "Schichtcode")
            omGroupBy = BY_SHIFTCODE;
        else if (olSortOrder[0] == "Team")
            omGroupBy = BY_TEAM;
        else if (olSortOrder[0] == "Name")
            omGroupBy = BY_NAME;
        else if (olSortOrder[0] == "Dienstrang")
            omGroupBy = BY_RANK;
    }
}

void StaffTableViewer::PrepareFilter()
{
	bmUseAllShiftCodes = SetFilterMap("Schichtcode", omCMapForShiftCode, GetString(IDS_ALLSTRING));
	bmUseAllRanks = SetFilterMap("Dienstrang", omCMapForRank, GetString(IDS_ALLSTRING));
	bmUseAllPermits = SetFilterMap("Permits", omCMapForPermits, GetString(IDS_ALLSTRING));

	bmUseAllTeams = SetFilterMap("Team", omCMapForTeam, GetString(IDS_ALLSTRING));
	bmEmptyTeamSelected = CheckForEmptyValue(omCMapForTeam, GetString(IDS_NOWORKGROUP));

	bmHideAbsentEmps = GetUserData("HIDEABSENTEMPS") == "YES";
	bmHideFidEmps = GetUserData("HIDEFIDEMPS") == "YES";
	bmHideStandbyEmps = GetUserData("HIDESTANDBYEMPS") == "YES";
	imShiftTime = atoi(GetUserData("SHIFTTIME"));
}

void StaffTableViewer::PrepareSorter()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);

    omSortOrder.RemoveAll();
    for (int i = 0; i < olSortOrder.GetSize(); i++)
    {
        int ilSortOrderEnumeratedValue;
        if (olSortOrder[i] == "Schichtbegin")
            ilSortOrderEnumeratedValue = BY_SHIFTSTART;
        else if (olSortOrder[i] == "Schichtend")
            ilSortOrderEnumeratedValue = BY_SHIFTEND;
        else if (olSortOrder[i] == "Schichtcode")
            ilSortOrderEnumeratedValue = BY_SHIFTCODE;
        else if (olSortOrder[i] == "Team")
            ilSortOrderEnumeratedValue = BY_TEAM;
        else if (olSortOrder[i] == "Dienstrang")
            ilSortOrderEnumeratedValue = BY_RANK;
        else if (olSortOrder[i] == "Name")
            ilSortOrderEnumeratedValue = BY_NAME;
		else
            ilSortOrderEnumeratedValue = BY_NONE;
        omSortOrder.Add(ilSortOrderEnumeratedValue);
    }
}

BOOL StaffTableViewer::IsPassFilter(const char *pcpRank, const char *pcpTeamId,
    const char *pcpShiftCode, CTime opAcfr, CTime opActo, bool bpIsAbsent, CStringArray &ropPermits, 
	bool bpHasFidJobs, bool bpPoolJobSetToAbsent, bool bpEmpStandby)
{
	if(bmIsFromSearch == TRUE)
	{
		return TRUE;
	}
    void *p;
    BOOL blIsRankOk = bmUseAllRanks || omCMapForRank.Lookup(CString(pcpRank), p);
    BOOL blIsTeamOk = bmUseAllTeams || (strlen(pcpTeamId) == 0 && bmEmptyTeamSelected) || omCMapForTeam.Lookup(CString(pcpTeamId), p);
    BOOL blIsShiftCodeOk = bmUseAllShiftCodes || omCMapForShiftCode.Lookup(CString(pcpShiftCode), p);

	BOOL blIsTimeOk = FALSE;
	switch(imShiftTime)
	{
	case 0:
		blIsTimeOk = IsBetween(opAcfr,omStartTime,omEndTime);
		break;
	case 1:
		blIsTimeOk = IsBetween(opActo,omStartTime,omEndTime);
		break;
	case 2:
	default:
		blIsTimeOk = IsOverlapped(opAcfr,opActo,omStartTime,omEndTime);
		break;
	}

	if (bpIsAbsent)
	{
		blIsShiftCodeOk = TRUE;
	}
	
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		SHIFTTYPEDATA* polShiftType = ogShiftTypes.GetShiftTypeByCode(const_cast<char*>(pcpShiftCode));		
		CString olEmpTerminal = CString(pcpShiftCode).Mid(0,2);
		if(polShiftType != NULL)
		{
			olEmpTerminal = strlen(polShiftType->Bsds) == 0 ? olEmpTerminal : polShiftType->Bsds;
		}
		if((omTerminal == StaffTable::Terminal2) && olEmpTerminal.CompareNoCase(StaffTable::Terminal2) != 0)
		{
			blIsShiftCodeOk = FALSE;
		}
		else if((omTerminal == StaffTable::Terminal3) && olEmpTerminal.CompareNoCase(StaffTable::Terminal2) == 0)
		{
			blIsShiftCodeOk = FALSE;
		}
	}

	BOOL blIsPermitOk = bmUseAllPermits;
	int ilNumPermits = ropPermits.GetSize();
	for(int ilPermit = 0; !blIsPermitOk && ilPermit < ilNumPermits; ilPermit++)
	{
		blIsPermitOk = omCMapForPermits.Lookup(ropPermits[ilPermit], p);
	}
	BOOL blDontHide = TRUE;
	if((bmHideAbsentEmps && bpPoolJobSetToAbsent) || (bmHideFidEmps && bpHasFidJobs) || (bmHideStandbyEmps && bpEmpStandby))
		blDontHide = FALSE;

    return (blIsRankOk && blIsTeamOk && blIsShiftCodeOk && blIsTimeOk && blIsPermitOk && blDontHide);
}

int StaffTableViewer::CompareStaff(STAFFTABLE_LINEDATA *prpStaff1,
	STAFFTABLE_LINEDATA *prpStaff2)
{
    // Compare in the sort order, from the outermost to the innermost
    int n = omSortOrder.GetSize();
    for (int i = 0; i < n; i++)
    {
        int ilCompareResult = 0;

        switch (omSortOrder[i])
        {
        case BY_SHIFTSTART:
			ilCompareResult = prpStaff1->Acfr.GetTime() - prpStaff2->Acfr.GetTime();
            break;
        case BY_SHIFTEND:
			ilCompareResult = prpStaff1->Acto.GetTime() - prpStaff2->Acto.GetTime();
            break;
        case BY_SHIFTCODE:
            ilCompareResult = strcmp(prpStaff1->Sfca,prpStaff2->Sfca);
            break;
        case BY_TEAM:
            ilCompareResult = strcmp(prpStaff1->Tmid,prpStaff2->Tmid);
            break;
        case BY_RANK:
			ilCompareResult = strcmp(prpStaff1->Rank,prpStaff2->Rank);
            break;
        case BY_NAME:
            ilCompareResult = strcmp(prpStaff1->Name,prpStaff2->Name);
            break;
        }

        if (ilCompareResult != 0)
            return ilCompareResult;
    }

    return strcmp(prpStaff1->Name,prpStaff2->Name);
}

BOOL StaffTableViewer::IsSameGroup(STAFFTABLE_LINEDATA *prpShift1,
	STAFFTABLE_LINEDATA *prpShift2)
{
    // Compare in the sort order, from the outermost to the innermost
    switch (omGroupBy)
    {
    case BY_SHIFTCODE:
        return (prpShift1->Sfca == prpShift2->Sfca);
    case BY_TEAM:
        return (prpShift1->Tmid == prpShift2->Tmid);
    case BY_NAME:
        return (prpShift1->Name == prpShift2->Name);
    case BY_RANK:
        return (prpShift1->Rank == prpShift2->Rank);
    }

    return TRUE;    // assume there's no grouping at all
}


/////////////////////////////////////////////////////////////////////////////
// StaffTableViewer -- code specific to this class

void StaffTableViewer::MakeLines()
{
	if (bmIsFromSearch == TRUE)	// The list is a result of a search
	{
		int ilShiftCount = ogSearch.omShifts.GetSize();

		for (int ilLc = 0; ilLc < ilShiftCount; ilLc++)
		{
			SHIFTDATA *prlShiftData = &ogSearch.omShifts[ilLc];
			MakeLine(prlShiftData);
		}
	}
	else
	{
		int ilShiftCount = ogShiftData.omData.GetSize();

		for (int ilLc = 0; ilLc < ilShiftCount; ilLc++)
		{
			SHIFTDATA *prlShiftData = &ogShiftData.omData[ilLc];
			MakeLine(prlShiftData);
		}
	}
}

void StaffTableViewer::MakeAssignments(CCSPtrArray <JOBDATA> &ropJobs,STAFFTABLE_LINEDATA &rrpStaff)
{
	rrpStaff.Assignment.DeleteAll();
	ropJobs.Sort(ByJobStartTime);
    for ( int ilLc = 0; ilLc < ropJobs.GetSize(); ilLc++ )
    {
        JOBDATA *prlJob = &ropJobs[ilLc];
		STAFFTABLE_ASSIGNMENT rlAssignment;
		rlAssignment.Alid = GetJobText(prlJob);
		rlAssignment.Urno = prlJob->Urno;
		rlAssignment.Acfr = prlJob->Acfr;
		rlAssignment.Acto = prlJob->Acto;
		CreateAssignment(rrpStaff, &rlAssignment);
    }

	if (rrpStaff.Assignment.GetSize() > imColumns)
		imColumns = rrpStaff.Assignment.GetSize();
}

CString StaffTableViewer::GetJobText(JOBDATA *prpJob)
{
	CString olText = "";
	if(prpJob != NULL)
	{
		if(!strcmp(prpJob->Jtco,JOBTEMPABSENCE))
		{
			CString olCode = "Not Found";

			DRADATA *prlDra = NULL;
			DELDATA *prlDel = NULL;
			if((prlDra = ogDraData.GetDraByUrno(prpJob->Shur)) != NULL)
			{
				olCode = prlDra->Sdac;
			}
			else if((prlDel = ogDelData.GetDelByUrno(prpJob->Udel)) != NULL)
			{
				olCode = prlDel->Sdac;
			}
			olText.Format("Temp.Abs.%s", olCode);
		}
		else
		{
			olText = ogBasicData.GetJobAlid(prpJob);
		}
	}

	return olText;
}

void StaffTableViewer::MakeLine(SHIFTDATA *prpShift)
{
    // Check if this shift is in the specified date and satisfies the filter
    //if (strcmp(prpShift->Sday, (const char *)omDate) != 0)
    //    return;
	CCSPtrArray <JOBDATA> olJobs;
	ogJobData.GetJobsByPkno(olJobs,prpShift->Peno);
	BOOL blFound = FALSE;

	JOBDATA *prlPoolJob = NULL;

	/*
	for (int ilLc = 0; ilLc < olJobs.GetSize(); ilLc++)
	{
		if (olJobs[ilLc].Shur == prpShift->Urno)
		{
			blFound = TRUE;
			break;
		}
	}
	*/
	CStringArray olPermits;
	CString olFunction;
	bool blHasFidJobs = false;
	bool blPoolJobSetToAbsent = false;
	bool blEmpStandby = false;

	for (int ilLc = olJobs.GetSize()-1; ilLc >= 0; ilLc--)
	{
		JOBDATA *prlJob = &olJobs[ilLc];

		bool blTempAbsence = false;
		if(!strcmp(prlJob->Jtco,JOBTEMPABSENCE))
		{
			JOBDATA *prlPJ = ogJobData.GetJobByUrno(prlJob->Jour);
			if(prlPJ != NULL && prlPJ->Shur == prpShift->Urno)
			{
				blTempAbsence = true;
			}
		}

		if (prlJob->Shur != prpShift->Urno && !blTempAbsence)
		{
			olJobs.RemoveAt(ilLc);
		}
		else if(!strcmp(prlJob->Jtco,JOBPOOL))
		{
			prlPoolJob = prlJob;
			blFound = TRUE;
			olFunction = ogBasicData.GetFunctionForPoolJob(prlPoolJob);
			ogBasicData.GetPermitsForPoolJob(prlPoolJob,olPermits);
			if(ogJobData.PoolJobHasFidJobs(prlPoolJob->Urno))
				blHasFidJobs = true;
			if(ogJobData.IsAbsent(prlPoolJob))
				blPoolJobSetToAbsent = true;
			if(ogJobData.IsStandby(prlPoolJob))
				blEmpStandby = true;
		}
	}
	if (! blFound )
		return;  // no JOBPOOL for this shift

    EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpShift->Peno);
	if(prlEmp == NULL)
		return;

	CString olRank = ogBasicData.GetFunctionForShift(prpShift);
	CString olTmid = ogBasicData.GetTeamForShift(prpShift);

	if(!IsPassFilter(olRank, olTmid, prpShift->Sfca, prlPoolJob->Acfr, prlPoolJob->Acto,ogShiftData.IsAbsent(prpShift), 
					 olPermits, blHasFidJobs, blPoolJobSetToAbsent, blEmpStandby))
        return;

    // Update viewer data for this shift record
    STAFFTABLE_LINEDATA rlStaff;
    rlStaff.Name = ogEmpData.GetEmpName(prlEmp);
	rlStaff.Peno = prpShift->Peno;
	rlStaff.ShiftUrno = prpShift->Urno;
	rlStaff.Rank = olRank;
    rlStaff.Tmid = olTmid;
    rlStaff.Sfca = prpShift->Sfca;
    rlStaff.Sfcs = prpShift->Sfcs;
    rlStaff.Acfr = prpShift->Avfa;
    rlStaff.Acto = prpShift->Avta;
	if (ogShiftData.IsAbsent(prpShift))
	{
//		rlStaff.Sfca = CString(prpShift->Tpid);
		rlStaff.Acfr = TIMENULL;
		rlStaff.Acto = TIMENULL;
	}
	
	MakeAssignments(olJobs,rlStaff);
    CreateLine(&rlStaff);
}

BOOL StaffTableViewer::FindShift(long lpShiftUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
		long x = omLines[rilLineno].ShiftUrno;
        if (omLines[rilLineno].ShiftUrno == lpShiftUrno)
            return TRUE;
	}
    return FALSE;
}

int StaffTableViewer::CreateAssignment(STAFFTABLE_LINEDATA &rrlStaff, STAFFTABLE_ASSIGNMENT *prpAssignment)
{
//    int ilAssignmentCount = rrlStaff.Assignment.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
    // Search for the position which we want to insert this new bar
//    for (int ilAssignmentno = ilAssignmentCount; ilAssignmentno > 0; ilAssignmentno--)
//        if (prpAssignment->Acfr >= rrlStaff.Assignment[ilAssignmentno-1].Acfr)
//            break;  // should be inserted after Lines[ilAssignmentno-1]

    rrlStaff.Assignment.NewAt(rrlStaff.Assignment.GetSize(), *prpAssignment);

    return rrlStaff.Assignment.GetSize();
}


/////////////////////////////////////////////////////////////////////////////
// StaffTableViewer - STAFFTABLE_LINEDATA array maintenance

void StaffTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int StaffTableViewer::CreateLine(STAFFTABLE_LINEDATA *prpStaff)
{
    int ilLineCount = omLines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareStaff(prpStaff, &omLines[ilLineno]) <= 0)
            break;  // should be inserted before Lines[ilLineno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareStaff(prpStaff, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
#endif

	STAFFTABLE_LINEDATA rlStaff;
	rlStaff = *prpStaff;
	if(ogBasicData.IsPaxServiceT2() == true)
	{		
		SHIFTTYPEDATA* polShiftType = ogShiftTypes.GetShiftTypeByCode(prpStaff->Sfca.GetBuffer(0));
		CString olEmpTerminal = prpStaff->Sfca.Mid(0,2);
		if(polShiftType != NULL)
		{
			olEmpTerminal = strlen(polShiftType->Bsds) == 0 ? olEmpTerminal : polShiftType->Bsds;
		}
		if(GetTerminal() == StaffTable::Terminal2)
		{
			if(olEmpTerminal.CompareNoCase(StaffTable::Terminal2) != 0)
				return -1;
		}
		else if(GetTerminal() == StaffTable::Terminal3)
		{
			if(olEmpTerminal.CompareNoCase(StaffTable::Terminal2) == 0)
				return -1;
		}
		omLines.NewAt(ilLineno, rlStaff);		
	}
	else
	{
		omLines.NewAt(ilLineno, rlStaff);
	}

    return ilLineno;
}

void StaffTableViewer::DeleteLine(int ipLineno)
{
	omLines[ipLineno].Assignment.DeleteAll();
	omLines.DeleteAt(ipLineno);
}


/////////////////////////////////////////////////////////////////////////////
// StaffTableViewer - display drawing routine

// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void StaffTableViewer::UpdateDisplay()
{
    //pomTable->SetHeaderFields("RA|Name|Team|SFT|SFT|Von|Bis|Sprachen|Zuordnung");
    CString olHeaderFields = GetString(IDS_STRING61574);
    CString olFormatList("7|25|4|8|8|7|7|15|16|9|16|9|16|9|16|9");
	for (int i = 4; i < imColumns; i++)
	{
		olHeaderFields += GetString(IDS_STAFFTABLE_ASSIGNED);
		olFormatList += "|16|9";
	}

   
	if (BGS)    pomTable->SetHeaderFieldsBGS(olHeaderFields);
	else		pomTable->SetHeaderFields(olHeaderFields);
    
	
	
	
	pomTable->SetFormatList(olFormatList);

    // Load filtered and sorted data into the table content
	pomTable->ResetContent();
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		pomTable->AddTextLine(Format(&omLines[ilLc]), &omLines[ilLc]);

		// Display bold font for the managers
		CString olRank = omLines[ilLc].Rank;
        BOOL blIsFlightManager = ogBasicData.IsManager(olRank);
		if (blIsFlightManager)
		{
			pomTable->SetTextLineFont(ilLc, &ogCourier_Bold_10);
			pomTable->SetTextLineColor(ilLc, BLUE, WHITE);
		}
		if (*omLines[ilLc].Sfcs == 'K')
		{
			pomTable->SetTextLineFont(ilLc, &ogCourier_Bold_10);
			pomTable->SetTextLineColor(ilLc, RED, WHITE);
		}

        // Display grouping effect
        if (ilLc == omLines.GetSize()-1 ||
			!IsSameGroup(&omLines[ilLc], &omLines[ilLc+1]))
            pomTable->SetTextLineSeparator(ilLc, ST_THICK);
    }                                           

    // Update the table content in the display
    pomTable->DisplayTable();
}

CString StaffTableViewer::Format(STAFFTABLE_LINEDATA *prpLine)
{
    CString s = prpLine->Rank + "|"
		+ prpLine->Name + "|"
        + prpLine->Tmid + "|"
        + prpLine->Sfca + "|"
        + prpLine->Sfcs + "|"
        + prpLine->Acfr.Format("%d/%H%M") + "|"
        + prpLine->Acto.Format("%d/%H%M") + "|";

	if (prpLine->Assignment.GetSize() > imColumns)
		imColumns = prpLine->Assignment.GetSize();

    for (int ilLc = 0; ilLc < prpLine->Assignment.GetSize(); ilLc++)
    {
        STAFFTABLE_ASSIGNMENT *prlAssignment = &prpLine->Assignment[ilLc];
        s += "|" + prlAssignment->Alid;
		s += "|" + prlAssignment->Acfr.Format("%H%M") + "-";
		s += prlAssignment->Acto.Format("%H%M");
    }

    return s;
}

BOOL StaffTableViewer::DeleteAssignmentJob(long lpJobUrno)
{
	int ilRc;

	JOBDATA *prlJob = ogJobData.GetJobByUrno(lpJobUrno);
	if (prlJob != NULL)
	{
		ilRc = ogDataSet.DeleteJob(pomTable, lpJobUrno);
	}

	return ilRc;
}

void StaffTableViewer::StaffTableTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    StaffTableViewer *polViewer = (StaffTableViewer *)popInstance;
	int ilColumns = polViewer->imColumns;

    if (ipDDXType == SHIFT_CHANGE)
        polViewer->ProcessShiftChange((SHIFTDATA *)vpDataPointer);
    else if (ipDDXType == SHIFT_DELETE)
        polViewer->ProcessShiftDelete((long)vpDataPointer);
	else if (ipDDXType == SHIFT_NEW)
        polViewer->ProcessShiftChange((SHIFTDATA *)vpDataPointer);
    else if (ipDDXType == JOB_CHANGE)
        polViewer->ProcessShiftChange((JOBDATA *)vpDataPointer);
	else if (ipDDXType == JOB_NEW)
        polViewer->ProcessShiftChange((JOBDATA *)vpDataPointer);
	else if (ipDDXType == JOB_DELETE)
        polViewer->ProcessJobDelete((JOBDATA *)vpDataPointer);
    else if (ipDDXType == SHIFT_SELECT_EMPLIST)
        polViewer->ProcessShiftSelect((SHIFTDATA *)vpDataPointer);

	if (ilColumns != polViewer->imColumns)
		polViewer->UpdateDisplay();
	
	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{		
		if(polViewer->pomTable != NULL && polViewer->pomTable->m_hWnd != NULL)
		{
			CWnd* polWnd = polViewer->pomTable->GetParent();
			(StaffTable*)(polWnd)->SendMessage(WM_TABLE_UPDATE_DATACOUNT,0,0);
		}
	}
}

void StaffTableViewer::ProcessShiftDelete(long lpUrno)
{
	int ilLine = -1;
	if (FindShift(lpUrno,ilLine))
	{
		DeleteLine(ilLine);
		pomTable->DeleteTextLine(ilLine);
		pomTable->DisplayTable();
	}
}

void StaffTableViewer::ProcessShiftChange(SHIFTDATA *prpShift)
{
	if(prpShift == NULL)
		return;

	int ilItem;

	if(strlen(prpShift->Sfca) <= 0 || prpShift->Bsdu == 0)
	{
		// the shift has been deleted in rostering (update received with SCOD="" and BSDU=0)
		// delete the shift entry in the preplanning table
		ProcessShiftDelete(prpShift->Urno);
	}
	else if (FindShift(prpShift->Urno,ilItem))
	{
        STAFFTABLE_LINEDATA *prlLine = &omLines[ilItem];

		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetJobsByPkno(olJobs,prpShift->Peno);
		BOOL blFound = FALSE;
		for (int ilLc = 0; ilLc < olJobs.GetSize(); ilLc++)
		{
			if (olJobs[ilLc].Shur != prpShift->Urno)
			{
				olJobs.RemoveAt(ilLc);
			}
			else
			{
				blFound = TRUE;
			}
		}
		if (! blFound )
			return;  // no JOBPOOL for this shift

		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpShift->Peno);

		// Update viewer data for this shift record
		prlLine->Name = CString(prlEmp->Lanm) + ", " + prlEmp->Finm;
		prlLine->Peno = prpShift->Peno;
		prlLine->ShiftUrno = prpShift->Urno;
		prlLine->Rank = ogBasicData.GetFunctionForShift(prpShift);
		prlLine->Tmid = ogBasicData.GetTeamForShift(prpShift);
		prlLine->Sfca = prpShift->Sfca;
		prlLine->Sfcs = prpShift->Sfcs;
		prlLine->Acfr = prpShift->Avfa;
		prlLine->Acto = prpShift->Avta;
		if (ogShiftData.IsAbsent(prpShift))
		{
//			prlLine->Sfca = CString(prpShift->Tpid);
			prlLine->Acfr = TIMENULL;
			prlLine->Acto = TIMENULL;
		}
		MakeAssignments(olJobs,*prlLine);
        pomTable->ChangeTextLine(ilItem, Format(&omLines[ilItem]), &omLines[ilItem]);
		pomTable->DisplayTable();
	}
	else
	{
		if (bmIsFromSearch == FALSE)
		{
			MakeLine(prpShift);
			if (FindShift(prpShift->Urno,ilItem))
			{
				pomTable->InsertTextLine(ilItem,Format(&omLines[ilItem]), &omLines[ilItem]);
				pomTable->DisplayTable();
			}
		}
	}
}


void StaffTableViewer::ProcessShiftSelect(SHIFTDATA *prpShift)
{
	int ilItem;
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		igPreplanSearchCount++;
	}

	if (FindShift(prpShift->Urno,ilItem))
	{
		CListBox *polListBox = pomTable->GetCTableListBox();
		if (polListBox)
		{
			polListBox->SetCurSel(ilItem);
		}
        igPreplanSearchCount = 0;
	}
	else if(ogBasicData.IsPaxServiceT2() == true)
	{
		if(igPreplanSearchCount == 2)
		{
			MessageBox(NULL,GetString(IDS_SV_NOTFOUND),GetString(IDS_SV_NOTFOUNDTITLE),MB_ICONEXCLAMATION );
			igPreplanSearchCount = 0;
		}
	}
	else
	{
		MessageBox(NULL,GetString(IDS_SV_NOTFOUND),GetString(IDS_SV_NOTFOUNDTITLE),MB_ICONEXCLAMATION );
	}
}




void StaffTableViewer::ProcessShiftChange(JOBDATA *prpJob)
{

	int ilItem;
// Id 19-Sep-96
	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpJob->Shur);
	if (prlShift == NULL)
		return;
// end of Id 19-Sep-96

	if (FindShift(prlShift->Urno,ilItem))
	{
		if((bmHideAbsentEmps && ogJobData.IsAbsent(prpJob)) || 
			(bmHideStandbyEmps && ogJobData.IsStandby(prpJob)) || 
			(bmHideFidEmps && ogJobData.PoolJobHasFidJobs(prpJob->Jour)))
		{
			ProcessShiftDelete(prlShift->Urno);
			return;
		}

        STAFFTABLE_LINEDATA *prlLine = &omLines[ilItem];

		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetJobsByPkno(olJobs,prlShift->Peno);
		BOOL blFound = FALSE;
		for (int ilLc = 0; ilLc < olJobs.GetSize(); ilLc++)
		{
			if (olJobs[ilLc].Shur != prlShift->Urno)
			{
				olJobs.RemoveAt(ilLc);
			}
			else
			{
				blFound = TRUE;
			}
		}
		if (! blFound )
			return;  // no JOBPOOL for this shift

		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlShift->Peno);

		// Update viewer data for this shift record
		prlLine->Name = ogEmpData.GetEmpName(prlEmp);
		prlLine->Peno = prlShift->Peno;
		prlLine->ShiftUrno = prlShift->Urno;
		prlLine->Rank = ogBasicData.GetFunctionForShift(prlShift);
		prlLine->Tmid = ogBasicData.GetTeamForShift(prlShift);
		prlLine->Sfca = prlShift->Sfca;
		prlLine->Sfcs = prlShift->Sfcs;
		prlLine->Acfr = prlShift->Avfa;
		prlLine->Acto = prlShift->Avta;
		if (ogShiftData.IsAbsent(prlShift))
		{
			prlLine->Acfr = TIMENULL;
			prlLine->Acto = TIMENULL;
		}
		MakeAssignments(olJobs,*prlLine);
        pomTable->ChangeTextLine(ilItem, Format(&omLines[ilItem]), &omLines[ilItem]);
		CString olRank = prlLine->Rank;
        BOOL blIsFlightManager = ogBasicData.IsManager(olRank);
		if (blIsFlightManager)
		{
			pomTable->SetTextLineFont(ilItem, &ogCourier_Bold_10);
			pomTable->SetTextLineColor(ilItem, BLUE, WHITE);
		}
		pomTable->DisplayTable();
	}
	else
	{
		if (bmIsFromSearch == FALSE)
		{
			MakeLine(prlShift);
			if (FindShift(prlShift->Urno,ilItem))
			{
				pomTable->InsertTextLine(ilItem,Format(&omLines[ilItem]), &omLines[ilItem]);
				CString olRank = omLines[ilItem].Rank;
				BOOL blIsFlightManager = ogBasicData.IsManager(olRank);
				if (blIsFlightManager)
				{
					pomTable->SetTextLineFont(ilItem, &ogCourier_Bold_10);
					pomTable->SetTextLineColor(ilItem, BLUE, WHITE);
				}
				pomTable->DisplayTable();
			}
		}
	}
}


void StaffTableViewer::ProcessJobDelete(JOBDATA *prpJob)
{

	int ilItem;

	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpJob->Shur);
	if (prlShift == NULL)
		return;

	if (FindShift(prlShift->Urno,ilItem))
	{
		STAFFTABLE_LINEDATA *prlStaff = &omLines[ilItem];
		prlStaff->Assignment.DeleteAll();
		omLines.DeleteAt(ilItem);
        pomTable->DeleteTextLine(ilItem);
	}

	MakeLine(prlShift);
	if(FindShift(prlShift->Urno,ilItem) && omLines[ilItem].Assignment.GetSize() > 0)
	{
		// if the emp still has at least a pool job, then re-insert him
		pomTable->InsertTextLine(ilItem,Format(&omLines[ilItem]), &omLines[ilItem]);
	}
	pomTable->DisplayTable();
}


void StaffTableViewer::SetIsFromSearchMode(BOOL bpIsFromSearch)
{
	bmIsFromSearch = bpIsFromSearch;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Printing functions

BOOL StaffTableViewer::PrintStaffLine(STAFFTABLE_LINEDATA *prpLine,BOOL bpIsLastLine)
{
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	int ilJobFieldWidth = 270;
	int ilTimeWidth = 90;
	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 1280 + ((ilJobFieldWidth+ilTimeWidth+ilTimeWidth) * imMaxJobsPerLine);
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();
	}
	else
	{
		SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpLine->ShiftUrno);
		if(prlShift == NULL)
			return FALSE;

		if (pomPrint->imLineNo == 0)
		{
			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 510;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text = GetString(IDS_STAFFTAB_PRINTTITLE1); // Function/Name/Work Grp
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 140;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STAFFTAB_PRINTTITLE2); //Shift
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = ilTimeWidth;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = ilTimeWidth;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.Text       = "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			for(int ilJ = 0; ilJ < imMaxJobsPerLine+1; ilJ++)  // imMaxJobsPerLine+1 -> pool job column is one extra
			{
				rlElement.FrameRight = PRINT_NOFRAME;

				rlElement.Length     = ilJobFieldWidth;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.Text       = GetString((ilJ == 0) ? IDS_STAFFTAB_PRINTTITLE4 : IDS_STAFFTAB_PRINTTITLE3); // Pool or Assignment
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.Length     = ilTimeWidth;
				rlElement.FrameLeft  = PRINT_NOFRAME;
				rlElement.Text       = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.Length     = ilTimeWidth;
				rlElement.FrameLeft  = PRINT_NOFRAME;
				rlElement.FrameRight = PRINT_FRAMEMEDIUM;
				rlElement.Text       = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			}

			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
		}

		bool bpFirst = true;
		int ilNumJobs = prpLine->Assignment.GetSize();
		int ilJob = 0;

		CCSPtrArray <JOBDATA> olPoolJobs;
		ogJobData.GetPoolJobsByUstf(olPoolJobs, prlShift->Stfu);
		int ilNumPJ = olPoolJobs.GetSize(), ilPJ;
		for(ilPJ = (ilNumPJ-1); ilPJ >= 0; ilPJ--)
		{
			JOBDATA *prlPJ = &olPoolJobs[ilPJ];
			if(prlPJ->Shur != prlShift->Urno)
			{
				olPoolJobs.RemoveAt(ilPJ);
			}
		}
		olPoolJobs.Sort(ByJobStartTime);

		CCSPtrArray <JOBDATA> olJobs;
		ilNumPJ = olPoolJobs.GetSize();
		for(ilPJ = 0; ilPJ < ilNumPJ; ilPJ++)
		{
			JOBDATA *prlPJ = &olPoolJobs[ilPJ];
			ogJobData.GetJobsByPoolJob(olJobs, prlPJ->Urno);
			ogJobData.GetJobsByPoolJobAndType(olJobs, prlPJ->Urno, JOBTEMPABSENCE, false);
			olJobs.Sort(ByJobStartTime);
			bool blNewPoolJob = true;
			ilJob = 0;
			ilNumJobs = olJobs.GetSize();
			
			while(ilJob < ilNumJobs || blNewPoolJob)
			{
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.Length     = 510;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.FrameRight = PRINT_NOFRAME;
				rlElement.FrameBottom= PRINT_NOFRAME;
				rlElement.pFont       = &pomPrint->omSmallFont_Bold;
				if(bpFirst)
				{
					rlElement.FrameTop = PRINT_FRAMETHIN;
					rlElement.Text.Format("%s/%s/%s", prpLine->Rank, prpLine->Name, (strlen(prpLine->Tmid) > 0 ? prpLine->Tmid : "--"));
				}
				else
				{
					rlElement.FrameTop = PRINT_NOFRAME;
					rlElement.Text = "";
				}
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.Length     = 140;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.FrameTop   = PRINT_NOFRAME;
				rlElement.FrameBottom= PRINT_FRAMETHIN;
				if(bpFirst)
					rlElement.Text       = prpLine->Sfca;
				else
					rlElement.Text = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.Alignment  = PRINT_CENTER;
				rlElement.Length     = ilTimeWidth; //PRINT_CENTER
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.FrameTop   = PRINT_NOFRAME;
				if(bpFirst)
					rlElement.Text       = prpLine->Acfr.Format("%H:%M");
				else
					rlElement.Text = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.Length     = ilTimeWidth;
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.FrameTop   = PRINT_NOFRAME;
				if(bpFirst)
					rlElement.Text       = prpLine->Acto.Format("%H:%M");
				else
					rlElement.Text = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);


				// pool job
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.FrameTop   = PRINT_NOFRAME;
				rlElement.Length     = ilJobFieldWidth;
				if(blNewPoolJob)
					rlElement.Text       = prlPJ->Alid;
				else 
					rlElement.Text       = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.Alignment  = PRINT_CENTER;
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.FrameTop   = PRINT_NOFRAME;
				rlElement.Length     = ilTimeWidth;
				if(blNewPoolJob)
					rlElement.Text       = prlPJ->Acfr.Format("%H:%M");
				else
					rlElement.Text       = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.FrameTop   = PRINT_NOFRAME;
				rlElement.Length     = ilTimeWidth;
				rlElement.FrameRight = PRINT_FRAMEMEDIUM;
				if(blNewPoolJob)
					rlElement.Text       = prlPJ->Acto.Format("%H:%M");
				else
					rlElement.Text       = "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				blNewPoolJob = false;
				// end pool job

				for(int ilCurrJob = 0; ilCurrJob < imMaxJobsPerLine; ilCurrJob++, ilJob++)
				{
					JOBDATA *prlJob = NULL;
					if(ilJob < ilNumJobs)
					{
						prlJob = &olJobs[ilJob];
					}
					rlElement.Alignment  = PRINT_LEFT;
					rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
					rlElement.FrameTop   = PRINT_NOFRAME;
					rlElement.Length     = ilJobFieldWidth;
					if(ilJob < ilNumJobs)
					{
						rlElement.Text = GetJobText(prlJob);
					}
					else 
					{
						rlElement.Text = "";
					}
					rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

					rlElement.Alignment  = PRINT_CENTER;
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.FrameTop   = PRINT_NOFRAME;
					rlElement.Length     = ilTimeWidth;
					if(ilJob < ilNumJobs)
						rlElement.Text       = prlJob->Acfr.Format("%H:%M");
					else
						rlElement.Text       = "";
					rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.FrameTop   = PRINT_NOFRAME;
					rlElement.Length     = ilTimeWidth;
					rlElement.FrameRight = PRINT_FRAMEMEDIUM;
					if(ilJob < ilNumJobs)
						rlElement.Text       = prlJob->Acto.Format("%H:%M");
					else
						rlElement.Text       = "";
					rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
				}
				pomPrint->PrintLine(rlPrintLine);
				rlPrintLine.DeleteAll();
				bpFirst = false;
			}
		}
	}

	return TRUE;
}

BOOL StaffTableViewer::PrintStaffHeader(CCSPrint *pomPrint)
{
	pomPrint->pomCdc->StartPage();
	pomPrint->PrintHeader();
	return TRUE;
}

void StaffTableViewer::PrintView()
{  
	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) + CString(omEndTime.Format("%d.%m.%Y %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y %H:%M");
	pomPrint = new CCSPrint(pomTable,PRINT_LANDSCAPE,60,500,150,GetString(IDS_STAFFJOBS),olPrintDate,omTarget);
	pomPrint->imMaxLines = 20;

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			imMaxJobsPerLine = (pomPrint->smPaperSize == DMPAPER_A3) ? 5 : 3; // A3 = 5 , A4  = 3 jobs printed per line.

			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			pomPrint->imLineHeight = 62;
			rlDocInfo.cbSize = sizeof( DOCINFO );

			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STAFFJOBS));
			rlDocInfo.lpszDocName = pclDocName;	

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				if( pomPrint->imLineNo >= pomPrint->imMaxLines )
				{
					if ( pomPrint->imPageNo > 0)
					{
						PrintStaffLine(&omLines[i],TRUE);
						pomPrint->PrintFooter("","");
						pomPrint->imLineNo = pomPrint->imMaxLines + 1;
						pomPrint->omCdc.EndPage();
						pomPrint->imLineTop = pomPrint->imFirstLine;
					}
					PrintStaffHeader(pomPrint);
				}
				PrintStaffLine(&omLines[i],FALSE);
			}
			PrintStaffLine(NULL,TRUE);
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
void StaffTableViewer::PrintView2()
{  
	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) + CString(omEndTime.Format("%d.%m.%Y %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y %H:%M");
	pomPrint = new CCSPrint(pomTable,PRINT_LANDSCAPE,60,500,40,GetString(IDS_STAFFJOBS),olPrintDate,omTarget);
	pomPrint->imMaxLines = 20;

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = 0;

			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			pomPrint->imLineHeight = 62;
			rlDocInfo.cbSize = sizeof( DOCINFO );
			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STAFFJOBS));
			rlDocInfo.lpszDocName = pclDocName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				PrintStaffLine2(&omLines[i]);
			}
			if(pomPrint->imLineNo != 0)
			{
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				PrintEndOfPage2();
			}
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

bool StaffTableViewer::PrintStartOfPage2()
{
	bool blStartOfPage = false;
	if(pomPrint->imLineNo == 0)
	{
		blStartOfPage = true;

		PrintStaffHeader(pomPrint);

		CCSPtrArray <PRINTELEDATA> rlPrintLine;
		PRINTELEDATA rlElement;

		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
		rlElement.FrameRight = PRINT_FRAMEMEDIUM;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
		rlElement.pFont		 = &pomPrint->omCourierNormal10;

		rlElement.Length     = imFuncLen2;
		rlElement.Text = GetString(IDS_STC_MFCT); // "M-Fct";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imNameLen2;
		rlElement.Text = GetString(IDS_STC_NAME); // "Name";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imWorkGroupLen2;
		rlElement.Text = GetString(IDS_STC_WORKGRP); // "Work Grp";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imShiftLen2;
		rlElement.Text = GetString(IDS_STC_SHIFT); // "Shift";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imJobLen2;
		rlElement.Text = GetString(IDS_STC_JOB); // "Job";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imRegnLen2;
		rlElement.Text = GetString(IDS_STC_REGN); // "AC-Regn";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imArrTimeLen2;
		rlElement.Text = GetString(IDS_STC_ARRTIME); // "STA / ETA";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imDepTimeLen2;
		rlElement.Text = GetString(IDS_STC_DEPTIME); // "STD / ETD";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imServiceLen2;
		rlElement.Text = GetString(IDS_STC_SERVICE); // "Service";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imGatePosLen2;
		rlElement.Text = GetString(IDS_STC_GATPOS); // "Gate/Pos";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imFuncLen2;
		rlElement.Text = GetString(IDS_STC_JFCT); // "J-Fct";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();
	}

	return blStartOfPage;
}

void StaffTableViewer::PrintEndOfPage2()
{
	if(pomPrint->imLineNo >= pomPrint->imMaxLines &&  pomPrint->imPageNo > 0)
	{
		CCSPtrArray <PRINTELEDATA> rlPrintLine;
		PRINTELEDATA rlElement;

		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = imFuncLen2+imNameLen2+imWorkGroupLen2+imShiftLen2+imJobLen2+imRegnLen2+imArrTimeLen2+imDepTimeLen2+imGatePosLen2+imServiceLen2+imFuncLen2;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();

		pomPrint->PrintFooter("","");
		pomPrint->omCdc.EndPage();
		pomPrint->imLineNo = 0;
		pomPrint->imLineTop = pomPrint->imFirstLine;
	}
}

void StaffTableViewer::AddElement(CCSPtrArray <PRINTELEDATA> &ropPrintLine, CString &ropText, int ipLength, int ipAlignment, int ipFrameLeft, int ipFrameRight, int ipFrameTop, int ipFrameBottom, CFont &ropFont)
{
	PRINTELEDATA rlElement;
	rlElement.Length		= ipLength;
	rlElement.Text			= ropText;
	rlElement.Alignment		= ipAlignment;
	rlElement.FrameLeft		= ipFrameLeft;
	rlElement.FrameRight	= ipFrameRight;
	rlElement.FrameTop		= ipFrameTop;
	rlElement.FrameBottom	= ipFrameBottom;
	rlElement.pFont			= &ropFont;
	ropPrintLine.NewAt(ropPrintLine.GetSize(),rlElement);
}

BOOL StaffTableViewer::PrintStaffLine2(STAFFTABLE_LINEDATA *prpLine)
{
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpLine->ShiftUrno);
	if(prlShift == NULL)
		return FALSE;


	bool blPrintEmpName = true;
	int ilNumJobs = prpLine->Assignment.GetSize();
	int ilJob = 0;

	CCSPtrArray <JOBDATA> olPoolJobs;
	ogJobData.GetPoolJobsByUstf(olPoolJobs, prlShift->Stfu);
	int ilNumPJ = olPoolJobs.GetSize(), ilPJ;
	for(ilPJ = (ilNumPJ-1); ilPJ >= 0; ilPJ--)
	{
		JOBDATA *prlPJ = &olPoolJobs[ilPJ];
		if(prlPJ->Shur != prlShift->Urno)
		{
			olPoolJobs.RemoveAt(ilPJ);
		}
	}
	olPoolJobs.Sort(ByJobStartTime);

	bool blNewPoolJob = true;
	CCSPtrArray <JOBDATA> olJobs;
	ilNumPJ = olPoolJobs.GetSize();
	for(ilPJ = 0; ilPJ < ilNumPJ; ilPJ++)
	{
		JOBDATA *prlPJ = &olPoolJobs[ilPJ];
		ogJobData.GetJobsByPoolJob(olJobs, prlPJ->Urno);
		ogJobData.GetJobsByPoolJobAndType(olJobs, prlPJ->Urno, JOBTEMPABSENCE, false);
		olJobs.Sort(ByJobStartTime);
		blNewPoolJob = true;
		ilJob = 0;
		ilNumJobs = olJobs.GetSize();
		
		while(ilJob < ilNumJobs || blNewPoolJob)
		{
			JOBDATA *prlJob = NULL;
			if(ilJob < ilNumJobs)
			{
				prlJob = &olJobs[ilJob];
			}

			if(PrintStartOfPage2())
			{
				blPrintEmpName = true;
				blNewPoolJob = true;
			}

			CString olText;

			// main function
			AddElement(rlPrintLine, (blNewPoolJob) ? ogBasicData.GetFunctionForPoolJob(prlPJ) : "", imFuncLen2, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, (blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,
						PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// employee name
			AddElement(rlPrintLine, (blPrintEmpName) ? prpLine->Name : "", imNameLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME,
						(blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,	PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// work group
			AddElement(rlPrintLine, (blNewPoolJob) ? ogBasicData.GetTeamForPoolJob(prlPJ) : "", imWorkGroupLen2, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, (blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,
						PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// shift time
			olText.Format("%s-%s", prlPJ->Acfr.Format("%H%M"), prlPJ->Acto.Format("%H%M"));
			AddElement(rlPrintLine, (blNewPoolJob) ? olText : "", imShiftLen2, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, (blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,
						PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// job
			AddElement(rlPrintLine, GetJobText(prlJob), imJobLen2, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omCourierNormal10);

			CString olRegn, olArrTime, olDepTime, olGatPos;
			CString olService, olDemandFunction;
			FLIGHTDATA *prlFlight = NULL, *prlRotation = NULL;
			if(prlJob != NULL)
			{
				if((prlFlight = ogFlightData.GetFlightByUrno(prlJob->Flur)) != NULL)
				{
					olRegn = prlFlight->Regn;
					if(ogFlightData.IsArrival(prlFlight))
					{
						olArrTime.Format("%s/%s", (prlFlight->Stoa != TIMENULL) ? prlFlight->Stoa.Format("%H%M") : "", (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa.Format("%H%M") : "");
						olGatPos.Format("%s/%s", prlFlight->Gta1, prlFlight->Psta);
					}
					else
					{
						olDepTime.Format("%s/%s", (prlFlight->Stod != TIMENULL) ? prlFlight->Stod.Format("%H%M") : "", (prlFlight->Etod != TIMENULL) ? prlFlight->Etod.Format("%H%M") : "");
						olGatPos.Format("%s/%s", prlFlight->Gtd1, prlFlight->Pstd);
					}
					if(ogJobData.IsTurnaroundJob(prlJob->Urno) && (prlRotation = ogFlightData.GetRotationFlight(prlFlight)) != NULL)
					{
						if(ogFlightData.IsArrival(prlRotation))
						{
							olArrTime.Format("%s/%s", (prlRotation->Stoa != TIMENULL) ? prlRotation->Stoa.Format("%H%M") : "", (prlRotation->Etoa != TIMENULL) ? prlRotation->Etoa.Format("%H%M") : "");
						}
						else
						{
							olDepTime.Format("%s/%s", (prlRotation->Stod != TIMENULL) ? prlRotation->Stod.Format("%H%M") : "", (prlRotation->Etod != TIMENULL) ? prlRotation->Etod.Format("%H%M") : "");
						}
					}
				}
				else
				{
					olArrTime.Format("%s/%s", prlJob->Acfr.Format("%H%M"), prlJob->Acto.Format("%H%M"));
				}
				DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prlJob);
				if(prlDemand != NULL)
				{
					RUDDATA *prlRud = ogRudData.GetRudByUrno(prlDemand->Urud);
					if(prlRud != NULL)
					{
						SERDATA *prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
						if(prlSer != NULL)
							olService = prlSer->Snam;
					}
					RPFDATA *prlDemandFunction = ogRpfData.GetFunctionByUrud(prlDemand->Urud);
					if(prlDemandFunction != NULL)
					{
						if(!strcmp(prlDemandFunction->Fcco,""))
						{
							SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(prlDemandFunction->Upfc);
							if(prlSgr != NULL)
							{
								olDemandFunction += CString("*") + prlSgr->Grpn;
							}
						}
						else
						{
							olDemandFunction += prlDemandFunction->Fcco;
						}
					}
				}
			}

			// flight regn
			AddElement(rlPrintLine, olRegn, imRegnLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// flight arr time
			AddElement(rlPrintLine, olArrTime, imArrTimeLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// flight dep time
			AddElement(rlPrintLine, olDepTime, imDepTimeLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// service
			AddElement(rlPrintLine, olService, imServiceLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// flight gate position
			AddElement(rlPrintLine, olGatPos, imGatePosLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omCourierNormal10);

			// service
			AddElement(rlPrintLine, olDemandFunction, imFuncLen2, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omCourierNormal10);

			ilJob++;
			blNewPoolJob = false;
			blPrintEmpName = false;
			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
			PrintEndOfPage2();
		}
	}

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
void StaffTableViewer::PrintView3()
{  
	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) + CString(omEndTime.Format("%d.%m.%Y %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y %H:%M");
//	pomPrint = new CCSPrint(pomTable,PRINT_PORTRAIT,20,400,30,GetString(IDS_STAFFJOBS),olPrintDate,omTarget);
	
//	AfxMessageBox(omTarget+ olPrintDate);
	
	pomPrint = new CCSPrint(pomTable,PRINT_PORTRAIT,60,400,50,GetString(IDS_STAFFJOBS),olPrintDate,omTarget);
	pomPrint->imMaxLines = 39;

	if (pomPrint != NULL)
	{
		
		
		if (pomPrint->InitializePrinter(PRINT_PORTRAIT,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = 0;

			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			
			
			pomPrint->imLineHeight = 62;
		
			rlDocInfo.cbSize = sizeof( DOCINFO );
			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STAFFJOBS));
			
			rlDocInfo.lpszDocName = pclDocName;	
			
			pomPrint->omCdc.StartDoc( &rlDocInfo );
		
			
					
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				PrintStaffLine3(&omLines[i]);
			}
			if(pomPrint->imLineNo != 0)
			{
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				PrintEndOfPage3();
			}
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

bool StaffTableViewer::PrintStartOfPage3()
{
	bool blStartOfPage = false;
	if(pomPrint->imLineNo == 0)
	{
		blStartOfPage = true;

		PrintStaffHeader(pomPrint);

		CCSPtrArray <PRINTELEDATA> rlPrintLine;
		PRINTELEDATA rlElement;

		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
		rlElement.FrameRight = PRINT_FRAMEMEDIUM;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
		rlElement.pFont		 = &pomPrint->omSmallFont_Regular;

		rlElement.Length     = imFuncLen3;
		rlElement.Text = GetString(IDS_STC_MFCT); // "M-Fct";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imShiftLen3;
		rlElement.Text = GetString(IDS_STC_SHIFT); // "Shift";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imNameLen3;
		rlElement.Text = GetString(IDS_STC_NAME); // "Name";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imBreakLen3;
		rlElement.Text = GetString(IDS_STC_BREAK); // "Break";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imJobLen3;
		rlElement.Text = GetString(IDS_STC_JOB); // "Job";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imRegnLen3;
		rlElement.Text = GetString(IDS_STC_REGN); // "AC-Regn";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imArrTimeLen3;
		rlElement.Text = GetString(IDS_STC_ARRTIME); // "STA / ETA";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imDepTimeLen3;
		rlElement.Text = GetString(IDS_STC_DEPTIME); // "STD / ETD";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imServiceLen3;
		rlElement.Text = GetString(IDS_STC_SERVICE); // "Service";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = imGatePosLen3;
		rlElement.Text = GetString(IDS_STC_GATPOS); // "Gate/Pos";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();
	}

	return blStartOfPage;
}

void StaffTableViewer::PrintEndOfPage3()
{
	if(pomPrint->imLineNo >= pomPrint->imMaxLines &&  pomPrint->imPageNo > 0)
	{
		CCSPtrArray <PRINTELEDATA> rlPrintLine;
		PRINTELEDATA rlElement;

		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = imTotalLen3;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();

		pomPrint->PrintFooter("","");
		pomPrint->omCdc.EndPage();
		pomPrint->imLineNo = 0;
		pomPrint->imLineTop = pomPrint->imFirstLine;
	}
}

BOOL StaffTableViewer::PrintStaffLine3(STAFFTABLE_LINEDATA *prpLine)
{
	
	

	
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpLine->ShiftUrno);
	if(prlShift == NULL)
		return FALSE;


	bool blPrintEmpName = true;
	int ilNumJobs = prpLine->Assignment.GetSize();
	int ilJob = 0;

	CCSPtrArray <JOBDATA> olPoolJobs;
	ogJobData.GetPoolJobsByUstf(olPoolJobs, prlShift->Stfu);
	int ilNumPJ = olPoolJobs.GetSize(), ilPJ;
	for(ilPJ = (ilNumPJ-1); ilPJ >= 0; ilPJ--)
	{
		JOBDATA *prlPJ = &olPoolJobs[ilPJ];
		if(prlPJ->Shur != prlShift->Urno)
		{
			olPoolJobs.RemoveAt(ilPJ);
		}
	}
	olPoolJobs.Sort(ByJobStartTime);

	JOBDATA *prlJob = NULL, *prlBreak = NULL;
	CString olBreakText;
	bool blNewPoolJob = true;
	CCSPtrArray <JOBDATA> olJobs;
	ilNumPJ = olPoolJobs.GetSize();
	for(ilPJ = 0; ilPJ < ilNumPJ; ilPJ++)
	{
		JOBDATA *prlPJ = &olPoolJobs[ilPJ];
		ogJobData.GetJobsByPoolJob(olJobs, prlPJ->Urno);
		ogJobData.GetJobsByPoolJobAndType(olJobs, prlPJ->Urno, JOBTEMPABSENCE, false);
		olJobs.Sort(ByJobStartTime);
		blNewPoolJob = true;

		// remove breaks and set the break job pointer
		ilNumJobs = olJobs.GetSize();
		prlBreak = NULL;
		for(ilJob = (ilNumJobs-1); ilJob >= 0; ilJob--)
		{
			prlJob = &olJobs[ilJob];
			if(!strcmp(prlJob->Jtco,JOBBREAK))
			{
				if(prlBreak == NULL) prlBreak = prlJob;
				olJobs.RemoveAt(ilJob);
			}
		}
		
		ilNumJobs = olJobs.GetSize();
		ilJob = 0;
		while(ilJob < ilNumJobs || blNewPoolJob)
		{
			if(ilJob < ilNumJobs)
				prlJob = &olJobs[ilJob];
			else
				prlJob = NULL;

			if(PrintStartOfPage3())
			{
				blPrintEmpName = true;
				blNewPoolJob = true;
			}

			CString olText;

			// main function
			AddElement(rlPrintLine, (blPrintEmpName) ? ogBasicData.GetFunctionForPoolJob(prlPJ) : "", imFuncLen3, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, (blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,
						PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// shift time
			AddElement(rlPrintLine, (blPrintEmpName) ? CString(prlShift->Sfca) : "", imShiftLen3, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, (blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,
						PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// employee name
			AddElement(rlPrintLine, (blPrintEmpName) ? prpLine->Name : "", imNameLen3, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME,
						(blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,	PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// break time
			if(blPrintEmpName && prlBreak)
				olBreakText.Format("%s%s", prlBreak->Acfr.Format("%H%M"), prlBreak->Acto.Format("%H%M"));
			else
				olBreakText.Empty();
			AddElement(rlPrintLine, olBreakText, imBreakLen3, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, (blPrintEmpName) ? PRINT_FRAMETHIN : PRINT_NOFRAME,
						PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// job
			
			
			AddElement(rlPrintLine, GetJobText(prlJob), imJobLen3, PRINT_LEFT, 
						PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

		
			
			
			CString olRegn, olArrTime, olDepTime, olGatPos;
			CString olService, olDemandFunction;
			FLIGHTDATA *prlFlight = NULL, *prlRotation = NULL;
			if(prlJob != NULL)
			{
				if((prlFlight = ogFlightData.GetFlightByUrno(prlJob->Flur)) != NULL)
				{
					olRegn = prlFlight->Regn;
					if(ogFlightData.IsArrival(prlFlight))
					{
						olArrTime.Format("%s/%s", (prlFlight->Stoa != TIMENULL) ? prlFlight->Stoa.Format("%H%M") : "", (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa.Format("%H%M") : "");
						olGatPos.Format("%s/%s", prlFlight->Gta1, prlFlight->Psta);
					}
					else
					{
						olDepTime.Format("%s/%s", (prlFlight->Stod != TIMENULL) ? prlFlight->Stod.Format("%H%M") : "", (prlFlight->Etod != TIMENULL) ? prlFlight->Etod.Format("%H%M") : "");
						olGatPos.Format("%s/%s", prlFlight->Gtd1, prlFlight->Pstd);
					}
					if(ogJobData.IsTurnaroundJob(prlJob->Urno) && (prlRotation = ogFlightData.GetRotationFlight(prlFlight)) != NULL)
					{
						if(ogFlightData.IsArrival(prlRotation))
						{
							olArrTime.Format("%s/%s", (prlRotation->Stoa != TIMENULL) ? prlRotation->Stoa.Format("%H%M") : "", (prlRotation->Etoa != TIMENULL) ? prlRotation->Etoa.Format("%H%M") : "");
						}
						else
						{
							olDepTime.Format("%s/%s", (prlRotation->Stod != TIMENULL) ? prlRotation->Stod.Format("%H%M") : "", (prlRotation->Etod != TIMENULL) ? prlRotation->Etod.Format("%H%M") : "");
						}
					}
				}
				else
				{
					olArrTime.Format("%s/%s", prlJob->Acfr.Format("%H%M"), prlJob->Acto.Format("%H%M"));
				}
				DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prlJob);
				if(prlDemand != NULL)
				{
					RUDDATA *prlRud = ogRudData.GetRudByUrno(prlDemand->Urud);
					if(prlRud != NULL)
					{
						SERDATA *prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
						if(prlSer != NULL)
							olService = prlSer->Snam;
					}
					RPFDATA *prlDemandFunction = ogRpfData.GetFunctionByUrud(prlDemand->Urud);
					if(prlDemandFunction != NULL)
					{
						if(!strcmp(prlDemandFunction->Fcco,""))
						{
							SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(prlDemandFunction->Upfc);
							if(prlSgr != NULL)
							{
								olDemandFunction += CString("*") + prlSgr->Grpn;
							}
						}
						else
						{
							olDemandFunction += prlDemandFunction->Fcco;
						}
					}
				}
			}

			// flight regn
			AddElement(rlPrintLine, olRegn, imRegnLen3, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// flight arr time
			AddElement(rlPrintLine, olArrTime, imArrTimeLen3, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// flight dep time
			AddElement(rlPrintLine, olDepTime, imDepTimeLen3, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// service
			AddElement(rlPrintLine, olService, imServiceLen3, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_NOFRAME, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			// flight gate position
			AddElement(rlPrintLine, olGatPos, imGatePosLen3, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, PRINT_FRAMETHIN, PRINT_NOFRAME, pomPrint->omSmallFont_Regular);

			ilJob++;
			blNewPoolJob = false;
			blPrintEmpName = false;
			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
			PrintEndOfPage3();
		}
	}

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void StaffTableViewer::PrintViewConfigurable()
{  
	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) + CString(omEndTime.Format("%d.%m.%Y %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y %H:%M");
//#define DEBUG_PRINTING

#ifndef DEBUG_PRINTING
	pomPrint = new CCSPrint(pomTable,PRINT_LANDSCAPE,60,400,50,GetString(IDS_STAFFJOBS),olPrintDate,omTarget);
	pomPrint->imMaxLines = 20;
#endif

	imMaxLineWidth = 2760;
	imMaxLines = 32;
#ifndef DEBUG_PRINTING
	if (pomPrint != NULL)
#endif
	{
#ifndef DEBUG_PRINTING
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
#endif
		{
#ifndef DEBUG_PRINTING
			if(pomPrint->smPaperSize == DMPAPER_A3)
			{
				if(pomPrint->imOrientation == PRINT_PORTRAIT)
				{
					imMaxLineWidth = 2760;
					imMaxLines = 58;
				}
				else
				{
					imMaxLineWidth = 3980;
					imMaxLines = 38;
				}
			}
			else
			{
				if(pomPrint->imOrientation == PRINT_PORTRAIT)
				{
					imMaxLineWidth = 1920;
					imMaxLines = 38;
				}
				else
				{
					imMaxLineWidth = 2760;
					imMaxLines = 24;
				}
			}
			pomPrint->imMaxLines = imMaxLines;
#endif

			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			if(ConfigureHeader())
			{
#ifndef DEBUG_PRINTING
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;

				DOCINFO	rlDocInfo;
				memset(&rlDocInfo, 0, sizeof(DOCINFO));
				pomPrint->imLineHeight = 62;
				rlDocInfo.cbSize = sizeof( DOCINFO );
				rlDocInfo.lpszDocName = GetString(IDS_STAFFJOBS);	
				pomPrint->omCdc.StartDoc( &rlDocInfo );
#endif
				for(int i = 0; i < omLines.GetSize(); i++ ) 
				{
					CheckForNewPageConfigurable();
					PrintStaffLineConfigurable(&omLines[i]);
				}
#ifndef DEBUG_PRINTING
				PrintEndOfPageConfigurable();
				pomPrint->omCdc.EndDoc();
#endif
			}
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
#ifndef DEBUG_PRINTING
		delete pomPrint;
		pomPrint = NULL;
#endif
	}
}

bool StaffTableViewer::CheckForNewPageConfigurable()
{
	bool blIsNewPage = false;
#ifndef DEBUG_PRINTING
	if(pomPrint->imLineNo >= pomPrint->imMaxLines )
	{
		if(pomPrint->imPageNo > 0)
		{
			// print end of previous page
			PrintEndOfPageConfigurable();
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			pomPrint->omCdc.EndPage();
			pomPrint->imLineTop = pomPrint->imFirstLine;
		}

		// print start of new page
		PrintStaffHeaderConfigurable(pomPrint);
		blIsNewPage = true;
	}
#endif

	return blIsNewPage;
}

void StaffTableViewer::PrintEndOfPageConfigurable()
{
#ifndef DEBUG_PRINTING
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = imMaxLineWidth;
	rlElement.FrameLeft  = PRINT_NOFRAME;
	rlElement.FrameRight = PRINT_NOFRAME;
	rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
	rlElement.FrameBottom= PRINT_NOFRAME;
	rlElement.pFont       = &pomPrint->omMediumFont_Bold;
	rlElement.Text.Empty();
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	pomPrint->PrintFooter("","");

	pomPrint->omCdc.EndPage();
#endif
}

BOOL StaffTableViewer::PrintStaffHeaderConfigurable(CCSPrint *pomPrint)
{
	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader();
	PrintColumnTitlesConfigurable();
	return TRUE;
}

bool StaffTableViewer::ConfigureHeader(void)
{
	imCharWidth = 18;
	imFirstJobField = -1;
	imNumJobsPerLine = 0;

	omHeaderConfig.RemoveAll();
	ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, omHeaderConfig, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", "", "","","","","","","","","","","","","","","","");

	int ilNumColumns = 0;
	int ilBodyWidth = 0;
	int ilJobWidth = 0;
	for(int ilFC = 0; ilFC < omHeaderConfig.GetSize(); ilFC++)
	{
		if(omHeaderConfig[ilFC].SortOrder == 1)
		{
			ilBodyWidth += (omHeaderConfig[ilFC].NumChars * imCharWidth);
		}
		else if(omHeaderConfig[ilFC].SortOrder == 2)
		{
			if(imFirstJobField == -1)
				imFirstJobField = ilFC;
			ilJobWidth += (omHeaderConfig[ilFC].NumChars * imCharWidth);
		}
	}
	if(ilBodyWidth <= 0 && ilJobWidth <= 0)
	{
		MessageBox(NULL,"Cancelling Print Job - the print format must be configured first.","",0);
		return false;
	}
	if((ilBodyWidth + ilJobWidth) > imMaxLineWidth)
	{
		MessageBox(NULL,"Cancelling Print Job - Printout too large for the paper size.\n\nSelect a larger paper size or reconfigure the print format.","",0);
		return false;
	}
	if(ilJobWidth > 0)
	{
		while((ilBodyWidth + (ilJobWidth * (imNumJobsPerLine+1))) < imMaxLineWidth)
			imNumJobsPerLine++;
	}
	imMaxLineWidth = ilBodyWidth + (ilJobWidth * imNumJobsPerLine);

	return true;
}

void StaffTableViewer::PrintColumnTitlesConfigurable()
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

#ifndef DEBUG_PRINTING
	if (pomPrint->imLineNo == 0)
#endif
	{
		rlElement.Alignment  = PRINT_CENTER;
		rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
		rlElement.FrameRight = PRINT_FRAMEMEDIUM;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
#ifndef DEBUG_PRINTING
		rlElement.pFont       = &pomPrint->omSmallFont_Bold;
#endif
		
		for(int ilFC = 0; ilFC < omHeaderConfig.GetSize() && (imNumJobsPerLine == 0 || ilFC < imFirstJobField); ilFC++)
		{
			rlElement.Text = omHeaderConfig[ilFC].Field;
			rlElement.Length = omHeaderConfig[ilFC].NumChars * imCharWidth;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
		for(int ilJ = 0; ilJ < imNumJobsPerLine; ilJ++)
		{
			for(ilFC = imFirstJobField; ilFC < omHeaderConfig.GetSize(); ilFC++)
			{
				rlElement.Text = omHeaderConfig[ilFC].Field;
				rlElement.Length = omHeaderConfig[ilFC].NumChars * imCharWidth;
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			}
		}
#ifndef DEBUG_PRINTING
		pomPrint->PrintLine(rlPrintLine);
#endif
		rlPrintLine.DeleteAll();
	}
}

BOOL StaffTableViewer::PrintStaffLineConfigurable(STAFFTABLE_LINEDATA *prpLine)
{
	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpLine->ShiftUrno);
	if(prlShift == NULL)
		return FALSE;

	bool blNewEmp = true;
	int ilNumJobs = prpLine->Assignment.GetSize();
	int ilJob = 0;

	CCSPtrArray <JOBDATA> olPoolJobs;
	ogJobData.GetPoolJobsByUstf(olPoolJobs, prlShift->Stfu);
	int ilNumPJ = olPoolJobs.GetSize(), ilPJ;
	for(ilPJ = (ilNumPJ-1); ilPJ >= 0; ilPJ--)
	{
		JOBDATA *prlPJ = &olPoolJobs[ilPJ];
		if(prlPJ->Shur != prlShift->Urno)
		{
			olPoolJobs.RemoveAt(ilPJ);
		}
	}
	olPoolJobs.Sort(ByJobStartTime);

	CFieldDataArray olFieldConfig;

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.Alignment  = PRINT_LEFT;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_FRAMEMEDIUM;
#ifndef DEBUG_PRINTING
	rlElement.pFont       = &pomPrint->omSmallFont_Regular;
#endif

	CCSPtrArray <JOBDATA> olJobs;
	ilNumPJ = olPoolJobs.GetSize();
	for(ilPJ = 0; ilPJ < ilNumPJ; ilPJ++)
	{
		JOBDATA *prlPJ = &olPoolJobs[ilPJ];
		ogJobData.GetJobsByPoolJob(olJobs, prlPJ->Urno);
		ogJobData.GetJobsByPoolJobAndType(olJobs, prlPJ->Urno, JOBTEMPABSENCE, false);
		olJobs.Sort(ByJobStartTime);
		bool blNewPoolJob = true;
		ilJob = 0;
		ilNumJobs = olJobs.GetSize();
		
		while(ilJob < ilNumJobs || blNewPoolJob)
		{
			CString olShiftTime, olPJTime;
			olShiftTime.Format("%s-%s", prpLine->Acfr.Format("%H%M"), prpLine->Acto.Format("%H%M"));
			olPJTime.Format("%s-%s", prlPJ->Acfr.Format("%H%M"), prlPJ->Acto.Format("%H%M"));
			olFieldConfig.RemoveAll();
			ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, olFieldConfig, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", 
										(blNewEmp) ? prpLine->Rank : "", (blNewEmp) ? prpLine->Name : "", (blNewEmp) ? prpLine->Tmid : "", 
										(blNewEmp) ? prpLine->Sfca : "", (blNewEmp) ? olShiftTime : "", 
										 (blNewPoolJob) ? prlPJ->Alid : "", (blNewPoolJob) ? olPJTime : "",
										 "","","","","","","","","","");
			rlElement.FrameTop = (blNewEmp) ? PRINT_FRAMETHIN : PRINT_NOFRAME;
			rlElement.FrameBottom= PRINT_NOFRAME;

			for(int ilBodyColumns = 0; ilBodyColumns < olFieldConfig.GetSize() && (imNumJobsPerLine == 0 || ilBodyColumns < imFirstJobField); ilBodyColumns++)
			{
				rlElement.Text = olFieldConfig[ilBodyColumns].Text;
				rlElement.Length = olFieldConfig[ilBodyColumns].NumChars * imCharWidth;
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			}

			blNewPoolJob = false;
			// end pool job

			rlElement.FrameTop   = PRINT_FRAMETHIN;
			rlElement.FrameBottom= PRINT_FRAMETHIN;

			if(imNumJobsPerLine <= 0)
			{
				ilJob = ilNumJobs;
			}
			else
			{
				for(int ilCurrJob = 0; ilCurrJob < imNumJobsPerLine; ilCurrJob++, ilJob++)
				{
					CString olJob, olJobTime, olRegn, olArrTime, olDepTime, olService, olGate, olPos, olDemandFunction, olTeamLeaderWorkGroup;
					JOBDATA *prlJob = (ilJob < ilNumJobs) ? &olJobs[ilJob] : NULL;
					if(prlJob != NULL)
					{
						FLIGHTDATA *prlFlight = NULL, *prlRotation = NULL;
						olJob = GetJobText(prlJob);
						olJobTime.Format("%s-%s", prlJob->Acfr.Format("%H%M"), prlJob->Acto.Format("%H%M"));
						if((prlFlight = ogFlightData.GetFlightByUrno(prlJob->Flur)) != NULL)
						{
							olRegn = prlFlight->Regn;
							if(ogFlightData.IsArrival(prlFlight))
							{
								olArrTime.Format("%s/%s", (prlFlight->Stoa != TIMENULL) ? prlFlight->Stoa.Format("%H%M") : "", (prlFlight->Etoa != TIMENULL) ? prlFlight->Etoa.Format("%H%M") : "");
								olGate = prlFlight->Gta1;
								olPos = prlFlight->Psta;
							}
							else
							{
								olDepTime.Format("%s/%s", (prlFlight->Stod != TIMENULL) ? prlFlight->Stod.Format("%H%M") : "", (prlFlight->Etod != TIMENULL) ? prlFlight->Etod.Format("%H%M") : "");
								olGate = prlFlight->Gtd1;
								olPos = prlFlight->Pstd;
							}
							if(ogJobData.IsTurnaroundJob(prlJob->Urno) && (prlRotation = ogFlightData.GetRotationFlight(prlFlight)) != NULL)
							{
								if(ogFlightData.IsArrival(prlRotation))
									olArrTime.Format("%s/%s", (prlRotation->Stoa != TIMENULL) ? prlRotation->Stoa.Format("%H%M") : "", (prlRotation->Etoa != TIMENULL) ? prlRotation->Etoa.Format("%H%M") : "");
								else
									olDepTime.Format("%s/%s", (prlRotation->Stod != TIMENULL) ? prlRotation->Stod.Format("%H%M") : "", (prlRotation->Etod != TIMENULL) ? prlRotation->Etod.Format("%H%M") : "");
							}
							olTeamLeaderWorkGroup = GetTeamLeaderWorkGroupsForFlight(prlFlight->Urno);
						}
						else
						{
							olArrTime.Format("%s/%s", prlJob->Acfr.Format("%H%M"), prlJob->Acto.Format("%H%M"));
						}
						DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prlJob);
						if(prlDemand != NULL)
						{
							RUDDATA *prlRud = ogRudData.GetRudByUrno(prlDemand->Urud);
							if(prlRud != NULL)
							{
								SERDATA *prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
								if(prlSer != NULL)
									olService = prlSer->Snam;
							}
							RPFDATA *prlDemandFunction = ogRpfData.GetFunctionByUrud(prlDemand->Urud);
							if(prlDemandFunction != NULL)
							{
								if(!strcmp(prlDemandFunction->Fcco,""))
								{
									SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(prlDemandFunction->Upfc);
									if(prlSgr != NULL)
									{
										olDemandFunction += CString("*") + prlSgr->Grpn;
									}
								}
								else
								{
									olDemandFunction += prlDemandFunction->Fcco;
								}
							}
						}
					}

					olFieldConfig.RemoveAll();
					ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, olFieldConfig, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", 
													"", "", "", "", "", "", "", olJob, olJobTime, olRegn, olArrTime, olDepTime, olService, 
													olGate, olPos, olDemandFunction, olTeamLeaderWorkGroup);

					for(int ilJobColumns = imFirstJobField; ilJobColumns < olFieldConfig.GetSize(); ilJobColumns++)
					{
						rlElement.Text = olFieldConfig[ilJobColumns].Text;
						rlElement.Length = olFieldConfig[ilJobColumns].NumChars * imCharWidth;
						rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
					}
				}
			}
			if(!CheckForNewPageConfigurable())
				blNewEmp = false;
#ifndef DEBUG_PRINTING
			pomPrint->PrintLine(rlPrintLine);
#endif
			rlPrintLine.DeleteAll();
		}
	}

	return TRUE;
}

CString StaffTableViewer::GetTeamLeaderWorkGroupsForFlight(long lpFlightUrno)
{
	CString olTeamLeader;
	CString olWorkGroup;
	CMapStringToPtr olWorkGroups;
	CCSPtrArray <JOBDATA> olFlightJobs;
	JOBDATA *prlPJ = NULL;
	ogJobData.GetJobsByFlur(olFlightJobs, lpFlightUrno, FALSE, "", "", PERSONNELDEMANDS, true);
	for(int ilJ = 0; ilJ < olFlightJobs.GetSize(); ilJ++)
	{
		if((prlPJ = ogJobData.GetJobByUrno(olFlightJobs[ilJ].Jour)) != NULL)
		{
			if(ogBasicData.IsGroupLeader(prlPJ))
			{
				CString olWorkGroup = ogBasicData.GetTeamForPoolJob(prlPJ);
				if(!olWorkGroup.IsEmpty())
					olWorkGroups.SetAt(olWorkGroup, (void *) NULL);	
			}
		}
	}

	void *pvlDummy;
	for(POSITION rlPos = olWorkGroups.GetStartPosition(); rlPos != NULL; )
	{
		olWorkGroups.GetNextAssoc(rlPos, olWorkGroup, (void *&)pvlDummy);
		if(!olTeamLeader.IsEmpty())
			olTeamLeader += CString(",");
		olTeamLeader += olWorkGroup;
	}

	return olTeamLeader;
}

void StaffTableViewer::CreateExport(const char *pclFileName, const char *pclSeperator, CString opTitle)
{
	ofstream of;
	of.open( pclFileName, ios::out);

	// title
	of  << setw(0) << opTitle << endl;

	// column header
	int ilF = 0, i = 0;
    CStringArray olFields;
	CString olString = GetString(IDS_STRING61574);
	for(i = 4; i < imColumns; i++)
		olString += GetString(IDS_STAFFTABLE_ASSIGNED);
	ogBasicData.ExtractItemList(olString,&olFields,'|');
	for(ilF = 0; ilF < olFields.GetSize(); ilF++) of << setw(0) << olFields[ilF] << setw(0) << pclSeperator;
	of << endl;

	for(i = 0; i < omLines.GetSize(); i++ ) 
	{
		ogBasicData.ExtractItemList(Format(&omLines[i]), &olFields, '|');
		for(ilF = 0; ilF < olFields.GetSize(); ilF++)
			of << setw(0) << ogBasicData.FormatFieldForExport(olFields[ilF], pclSeperator) << setw(0) << pclSeperator;
		of << endl;
	}
	
	of << endl;
	of.close();
}

void StaffTableViewer::PrintPreview(const int& ripPageNo)
{  
	if(ripPageNo <= 0)
		return;

	static ilLineNo = 0;
	if(ripPageNo == 1)
	{
		ilLineNo = 0;
	}
	ilLineNo = (ripPageNo-1) * pomPrint->imMaxLines;
	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) + CString(omEndTime.Format("%d.%m.%Y %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y %H:%M");
	if (pomPrint != NULL)
	{
		pomPrint->imMaxLines = 20;	
		//pomPrint->imLineNo = (ripPageNo-1) * pomPrint->imMaxLines;
		imMaxJobsPerLine = 3;
	
		pomPrint->imPageNo = ripPageNo;
		PrintStaffHeader(pomPrint);
		for(int i = (ripPageNo-1) * pomPrint->imMaxLines; (pomPrint->imLineNo < pomPrint->imMaxLines) && i < omLines.GetSize(); i++ ) 
		//for(int i = ilLineNo ; pomPrint->imLineNo < pomPrint->imMaxLines && pomPrint->imLineNo < omLines.GetSize() ; i++)
		{
			pomPrint->imPageNo = ripPageNo;
			PrintStaffLine(&omLines[i],FALSE);
			ilLineNo++;
		}
		PrintStaffLine(NULL,TRUE);		
		pomPrint->PrintFooter("","");		
		pomPrint->pomCdc->EndPage();
		pomPrint->imLineTop = pomPrint->imFirstLine;
		pomPrint->imLineNo = 0;	
	}
}