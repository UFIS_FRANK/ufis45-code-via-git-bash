// CedaEqaData.cpp - Class for Equipment Attributes (extra info about a single piece of equipment)
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaEqaData.h>
#include <BasicData.h>

void  ProcessEqaCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaEqaData::CedaEqaData()
{                  
    BEGIN_CEDARECINFO(EQADATA, EqaDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Uequ,"UEQU")
		FIELD_CHAR_TRIM(Name,"NAME")
		FIELD_CHAR_TRIM(Valu,"VALU")
        FIELD_CHAR_TRIM(Usec,"USEC")
        FIELD_DATE(Cdat,"CDAT")
        FIELD_CHAR_TRIM(Useu,"USEU")
        FIELD_DATE(Lstu,"LSTU")
	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(EqaDataRecInfo)/sizeof(EqaDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&EqaDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogCCSDdx.Register((void *)this,BC_EQA_CHANGE,CString("EQADATA"), CString("Eqa changed"),ProcessEqaCf);
	ogCCSDdx.Register((void *)this,BC_EQA_DELETE,CString("EQADATA"), CString("Eqa deleted"),ProcessEqaCf);

    // Initialize table names and field names
    strcpy(pcmTableName,"EQATAB");
    pcmFieldList = "URNO,UEQU,NAME,VALU,USEC,CDAT,USEU,LSTU";
}
 
CedaEqaData::~CedaEqaData()
{
	ogCCSDdx.UnRegisterAll();
	TRACE("CedaEqaData::~CedaEqaData called\n");
	ClearAll();
}

void CedaEqaData::ClearAll()
{
	omUrnoMap.RemoveAll();
	CMapPtrToPtr *polSingleMap;
	long llUrno;
	for(POSITION rlPos = omUequMap.GetStartPosition(); rlPos != NULL; )
	{
		omUequMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUequMap.RemoveAll();

	omData.DeleteAll();
}

CCSReturnCode CedaEqaData::ReadEqaData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaEqaData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		EQADATA rlEqaData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlEqaData)) == RCSuccess)
			{
				AddEqaInternal(rlEqaData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaEqaData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(EQADATA), pclWhere);
    return ilRc;
}

void  ProcessEqaCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaEqaData *)popInstance)->ProcessEqaBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaEqaData::ProcessEqaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlEqaData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlEqaData);

	long llUrno = GetUrnoFromSelection(prlEqaData->Selection);
	if(llUrno == 0L && ipDDXType == BC_EQA_CHANGE)
	{
		EQADATA rlEqa;
		GetRecordFromItemList(&omRecInfo,&rlEqa,prlEqaData->Fields,prlEqaData->Data);
		llUrno = rlEqa.Urno;
	}
	
	EQADATA *prlEqa = GetEqaByUrno(llUrno);
	EQADATA rlOriginalEqa;
	if(ipDDXType == BC_EQA_CHANGE && prlEqa != NULL)
	{
		// update
		GetRecordFromItemList(prlEqa,prlEqaData->Fields,prlEqaData->Data);
		ogCCSDdx.DataChanged((void *)this,EQA_CHANGE,(void *)prlEqa);
	}
	else if(ipDDXType == BC_EQA_CHANGE && prlEqa == NULL)
	{
		// insert
		EQADATA rlEqa;
		GetRecordFromItemList(&rlEqa,prlEqaData->Fields,prlEqaData->Data);
		prlEqa = AddEqaInternal(rlEqa);
		ogCCSDdx.DataChanged((void *)this,EQA_CHANGE,(void *)prlEqa);
	}
	else if(ipDDXType == BC_EQA_DELETE && prlEqa != NULL)
	{
		// delete
		rlOriginalEqa = *prlEqa;
		DeleteEqaInternal(prlEqa);
		ogCCSDdx.DataChanged((void *)this,EQA_DELETE,(void *)rlOriginalEqa.Urno);
	}
}



EQADATA *CedaEqaData::AddEqaInternal(EQADATA &rrpEqa)
{
	EQADATA *prlEqa = new EQADATA;
	*prlEqa = rrpEqa;
	omData.Add(prlEqa);
	omUrnoMap.SetAt((void *)prlEqa->Urno,prlEqa);

	CMapPtrToPtr *polSingleMap;
	if(omUequMap.Lookup((void *) prlEqa->Uequ, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prlEqa->Urno,prlEqa);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prlEqa->Urno,prlEqa);
		omUequMap.SetAt((void *)prlEqa->Uequ,polSingleMap);
	}

	return prlEqa;
}


EQADATA* CedaEqaData::GetEqaByUrno(long lpUrno)
{
	EQADATA *prlEqa = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlEqa);
	return prlEqa;
}

bool CedaEqaData::GetEqasByUequ(long lpUequ, CCSPtrArray <EQADATA> &ropEqaList, bool bpReset /*=true*/)
{
	if(bpReset)
	{
		ropEqaList.RemoveAll();
	}

	CMapPtrToPtr *polSingleMap;

	if(omUequMap.Lookup((void *)lpUequ,(void *& )polSingleMap))
	{
		EQADATA *prlEqa = NULL;
		long llUrno;

		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlEqa);
			ropEqaList.Add(prlEqa);
		}
	}

	return ropEqaList.GetSize() > 0 ? true : false;
}

CString CedaEqaData::GetTableName(void)
{
	return CString(pcmTableName);
}

CString CedaEqaData::Dump(long lpUrno)
{
	CString olDumpStr;
	EQADATA *prlEqa = GetEqaByUrno(lpUrno);
	if(prlEqa == NULL)
	{
		olDumpStr.Format("No EQADATA Found for EQA.URNO <%ld>",lpUrno);
	}
	else
	{
		EQADATA rlEqa;
		memcpy(&rlEqa,prlEqa,sizeof(EQADATA));
		CString olData;
		MakeCedaData(&omRecInfo,olData,&rlEqa);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();
		if(ilNumFields != ilNumData)
		{
			olDumpStr.Format("<%s>\n<%s>\n",pcmFieldList,olData);
		}
		else
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + "> ";
			}
		}
	}
	return olDumpStr;
}

// conversions from new DB model to old
void CedaEqaData::PrepareDataForWrite(EQADATA *prpEqa)
{
	// set lstu/cdat etc
	prpEqa->Lstu = ogBasicData.GetTime();
	strcpy(prpEqa->Useu,ogUsername.Left(31));

	if(prpEqa->Cdat == TIMENULL || strlen(prpEqa->Usec) <= 0)
	{
		prpEqa->Cdat = prpEqa->Lstu;
		strcpy(prpEqa->Usec,prpEqa->Useu);
	}
}

EQADATA *CedaEqaData::InsertEqa(const char *pcpName, const char *pcpValu)
{
	EQADATA rlEqa, *prlEqa = NULL;

	rlEqa.Urno = ogBasicData.GetNextUrno();
	strcpy(rlEqa.Name,pcpName);
	strcpy(rlEqa.Valu,pcpValu);
	PrepareDataForWrite(&rlEqa);

	CString olListOfData;
	char pclData[500], pclCmd[10], pclSelection[100];

	MakeCedaData(&omRecInfo,olListOfData,&rlEqa);
	strcpy(pclCmd,"IRT");
	strcpy(pclSelection,"");

	bool blRc = true;
	ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);
	if(!(blRc = CedaAction(pclCmd,"","",pclData)))
	{
		ogBasicData.LogCedaError("CedaEqaData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
	}
	else
	{
		prlEqa = AddEqaInternal(rlEqa);
	}

	return prlEqa;
}

void CedaEqaData::UpdateEqa(long lpUrno, const char *pcpValu)
{
	EQADATA *prlEqa = GetEqaByUrno(lpUrno);
	if(prlEqa != NULL && strcmp(prlEqa->Valu,pcpValu))
	{
		EQADATA rlEqa = *prlEqa;
		strcpy(rlEqa.Valu,pcpValu);
		PrepareDataForWrite(&rlEqa);

		char pclFieldList[500], pclData[500], pclSelection[500], pclCmd[10];
		GetChangedFields(&rlEqa, prlEqa, pclFieldList, pclData);
		sprintf(pclSelection,"WHERE URNO = '%ld'",prlEqa->Urno);
		strcpy(pclCmd,"URT");
		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pclFieldList, pclData);
		bool blRc = true;
		if(!(blRc = CedaAction(pclCmd, pcmTableName, pclFieldList, pclSelection,"", pclData)))
		{
			ogBasicData.LogCedaError("CedaEqaData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
		else
		{
			*prlEqa = rlEqa;
		}
	}
}

// compare prpEqa with prpOldEqa and return only the fields (pcpFieldList) and data (pcpData) which have changed
bool CedaEqaData::GetChangedFields(EQADATA *prpEqa, EQADATA *prpOldEqa, char *pcpFieldList, char *pcpData)
{
	bool blChangesFound = false;

	memset(pcpFieldList,0,sizeof(pcpFieldList));
	memset(pcpData,0,sizeof(pcpData));

	if(prpEqa != NULL && prpOldEqa != NULL)
	{
		EQADATA rlEqa;
		CString olData;

		memcpy(&rlEqa,prpEqa,sizeof(EQADATA));
		MakeCedaData(&omRecInfo,olData,&rlEqa);
		CStringArray olNewFieldArray, olNewDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olNewFieldArray);
		ogBasicData.ExtractItemList(olData, &olNewDataArray);
		int ilNumNewFields = olNewFieldArray.GetSize();
		int ilNumNewData = olNewDataArray.GetSize();

		memcpy(&rlEqa,prpOldEqa,sizeof(EQADATA));
		MakeCedaData(&omRecInfo,olData,&rlEqa);
		CStringArray olOldDataArray;
		ogBasicData.ExtractItemList(olData, &olOldDataArray);
		int ilNumOldData = olOldDataArray.GetSize();

		bool blListsAreEmpty = true;
		if(ilNumNewFields == ilNumNewData && ilNumNewData == ilNumOldData)
		{
			for(int ilD = 0; ilD < ilNumNewData; ilD++)
			{
				// check if the data item has changed - always write LSTU and USEU whether they change or not
				if(olNewDataArray[ilD] != olOldDataArray[ilD] || olNewFieldArray[ilD] == "LSTU" || olNewFieldArray[ilD] == "USEU")
				{
					CString olOldData = olOldDataArray[ilD];
					CString olNewData = olNewDataArray[ilD];
					if(blListsAreEmpty)
					{
						strcpy(pcpFieldList,olNewFieldArray[ilD]);
						strcpy(pcpData,olNewDataArray[ilD]);
						blListsAreEmpty = false;
					}
					else
					{
						CString olTmp;
						olTmp.Format(",%s",olNewFieldArray[ilD]);
						strcat(pcpFieldList,olTmp);
						olTmp.Format(",%s",olNewDataArray[ilD]);
						strcat(pcpData,olTmp);
					}

					if(olNewDataArray[ilD] != olOldDataArray[ilD] && 
						olNewFieldArray[ilD] != "CDAT" && olNewFieldArray[ilD] != "USEC" &&
						olNewFieldArray[ilD] != "LSTU" && olNewFieldArray[ilD] != "USEU")
					{
						blChangesFound = true;
					}
				}
			}
		}
	}

	return blChangesFound;
}

void CedaEqaData::DeleteEqa(long lpUrno)
{
	EQADATA *prlEqa = GetEqaByUrno(lpUrno);
	if(prlEqa != NULL)
	{
		char pclSelection[500], pclCmd[10];
		sprintf(pclSelection,"WHERE URNO = '%ld'",prlEqa->Urno);
		strcpy(pclCmd,"DRT");
		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, "", "");
		bool blRc = true;
		if(!(blRc = CedaAction(pclCmd,pclSelection)))
		{
			ogBasicData.LogCedaError("CedaEqaData Error",omLastErrorMessage,pclCmd,"",pcmTableName,pclSelection);
		}
		else
		{
			DeleteEqaInternal(prlEqa);
		}
	}
}

void CedaEqaData::DeleteEqaInternal(EQADATA *prpEqa)
{
	CMapPtrToPtr *polSingleMap;
	if(omUequMap.Lookup((void *)prpEqa->Uequ,(void *& )polSingleMap))
	{
		polSingleMap->RemoveKey((void *)prpEqa->Urno);
	}

	long llUrno = prpEqa->Urno;
	int ilCount = omData.GetSize();
	for (int ilLc = ilCount-1; ilLc >= 0; ilLc--)
	{
		if (omData[ilLc].Urno == llUrno)
		{
			omUrnoMap.RemoveKey((void *)llUrno);
			omData.DeleteAt(ilLc);
		}
	}
}
