#ifndef __DEMTABLEVIEWER_H__
#define __DEMTABLEVIEWER_H__

#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaCcaData.h>
#include <CedaDpxData.h>

#include <cviewer.h>
#include <ccsprint.h>

struct DEMDATA_LINE	// each line represents one demand
{
	DPXDATA *dpx;
	long		DemUrno;
	char		Obty[11];	// demand type, eg. 'FID', 'AFT', ...	
	char		Aloc[11];	// allocation unit type, eg 'GAT', 'CIC', 'REG', 'POS'
    char		Alid[6];    // ID of Allocation Unit eg GateName,CicName,Regn,Pos
	CTime		Debe;		// demand begin
	CTime		Deen;		// demand end
	CTimeSpan	Dedu;		// demand duration
    char		Act3[4];    // Aircraft Type
	char		Regn[13];	// Registration
	char		Alc2[15];	// airline code format "ALC2/ALC3"
	char		Dest[15];	// Destination format "APC3/APC4"
	char		AFnum[15];	// flight number arrival
	char		DFnum[15];	// flight number departure
    CTime		Tifd;       // Actual Time (best qualify field) of Departure (for Outbound DEST/VIA)
    CTime		Tifa;       // Actual Time (best qualify field) of Arrival (for Outbound DEST/VIA)
	CString		Blt1;		// Baggage belt 1
	CString		Cki;		// Checkin counter range eg "321-323"
	CCSPtrArray <CCADATA> CcaList; // checkin counters
	CString		VaArr;		// Nature Arrival
	CString		VaDep;		// Nature Departure
	CString		Psta;		// position arrival
	CString		Pstd;		// position departure
	CString		Gata;		// gate arrival
	CString		Gatd;		// gate departure
	CString		Paxf;		// booked first class pax
	CString		Paxb;		// booked business class pax
	CString		Paxe;		// booked economy class pax
	CString		ConfF;		// LOATAB first class pax
	CString		ConfJ;		// LOATAB business class pax
	CString		ConfY;		// LOATAB economy class pax
    char		Tisd[2];    // Status of Actual Time
    char		Tisa[2];    // Status of Actual Time
	char		Dety[2];	// duty type 0 = Turnaround, 1 = Arrival, 2 = Departure
	CString		Rety;		// ResourceType (personnel/equipment/location)
	long		Ulnk;		// links a group of demands together
	bool		Expanded;	// checks, whether this group has been expanded or not
	CString		Fcco;		// Function code
	CString		Service;	// Service Code
	CString		Permits;	// Permits
	int			Staff;		// # of employees needed for this demand
	bool		IsTeamLine;	// represents whole team 
	bool		Deactivated;// demand is deactivated (will be displayed in grey)
	CCSPtrArray<DEMDATA_LINE>	TeamLines;	// the team lines
	CTime		OriDebe;
	CTime		OriDeen;
	CTimeSpan	OriDedu;
	CString     Term;       // Flight terminal information //Singapore
	CString     Pnam;       // lli: PRM Pax Name //Singapore
	CString     Prem;       // lli: PRM Remarks //Singapore

	DEMDATA_LINE()
	{
		DemUrno = 0;
		Obty[0] = '\0';
		Aloc[0] = '\0';
		Alid[0] = '\0';
		Act3[0] = '\0';
		Regn[0] = '\0';
		Alc2[0] = '\0';	
		Dest[0] = '\0';	
		AFnum[0]= '\0';
		DFnum[0]= '\0';
		Tifd    = TIMENULL;
		Tifa    = TIMENULL;
		Tisd[0] = '\0';
		Tisa[0] = '\0';
		Dety[0] = '\0';
		Ulnk    = 0;
		Staff	= 0;
		Expanded= true;
		IsTeamLine = false;
		Deactivated = false;
	}

	DEMDATA_LINE(const DEMDATA_LINE& rhs)
	{
		*this = rhs;		
	}

	DEMDATA_LINE& DEMDATA_LINE::operator=(const DEMDATA_LINE& rhs)
	{
		if (&rhs != this)
		{
			dpx = rhs.dpx;
			DemUrno = rhs.DemUrno;
			strcpy(Obty,rhs.Obty);
			strcpy(Aloc,rhs.Aloc);
			strcpy(Alid,rhs.Alid);
			Debe = rhs.Debe;		// demand begin
			Deen = rhs.Deen;		// demand end
			Dedu = rhs.Dedu;		// demand duration
			strcpy(Act3,rhs.Act3);
			strcpy(Regn,rhs.Regn);
			strcpy(Alc2,rhs.Alc2);
			strcpy(Dest,rhs.Dest);
			strcpy(AFnum,rhs.AFnum);
			strcpy(DFnum,rhs.DFnum);
			Tifd = rhs.Tifd;
			Tifa = rhs.Tifa;
			Blt1 = rhs.Blt1;
			Cki = rhs.Cki;
			VaArr = rhs.VaArr;
			VaDep = rhs.VaDep;
			Psta = rhs.Psta;
			Pstd = rhs.Pstd;
			Gata = rhs.Gata;
			Gatd = rhs.Gatd;
			strcpy(Tisd,rhs.Tisd);
			strcpy(Tisa,rhs.Tisa);
			strcpy(Dety,rhs.Dety);
			Rety = rhs.Rety;
			Ulnk = rhs.Ulnk;		// links a group of demands together
			Expanded = rhs.Expanded;	// checks, whether this group has been expanded or not
			Fcco = rhs.Fcco;		// Function code
			Service = rhs.Service;
			Permits = rhs.Permits;	// Permits
			Staff = rhs.Staff;		// # of employees needed for this demand
			IsTeamLine = rhs.IsTeamLine;	// represents whole team 
			Deactivated = rhs.Deactivated;

			this->TeamLines.DeleteAll();
			for (int i = 0; i < rhs.TeamLines.GetSize(); i++)
			{
				TeamLines.NewAt(i,rhs.TeamLines[i]);
			}

			OriDebe = rhs.OriDebe;
			OriDeen = rhs.OriDeen;
			OriDedu = rhs.OriDedu;

			Paxf = rhs.Paxf;
			Paxb = rhs.Paxb;
			Paxe = rhs.Paxe;

			ConfF = rhs.ConfF;
			ConfJ = rhs.ConfJ;
			ConfY = rhs.ConfY;

			Term  = rhs.Term; //Singapore
			Pnam  = rhs.Pnam; //Singapore
			Prem  = rhs.Prem; //Singapore
		}		
		return *this;
	}

	~DEMDATA_LINE()
	{
		TeamLines.DeleteAll(); 
	}
};

struct DEMDATA_FIELD
{
	CString	Field;
	CString	Name;
	int		Length;
	int		PrintLength;

	DEMDATA_FIELD(const char *pcpField,const char *pcpName,int ipLength,int ipPrintLength)
	{
		Field = pcpField;
		Name  = pcpName;
		Length= ipLength;
		PrintLength = ipPrintLength; 
	};

	DEMDATA_FIELD()
	{
		Length		= 0;
		PrintLength = 0;
	};
};

class	CCSTable;
struct	TABLE_COLUMN;

class DemandTableViewer: public CViewer
{
// Constructions
public:
					DemandTableViewer();
    ~				DemandTableViewer();

    void			Attach(CCSTable *popAttachWnd);
	void			ChangeViewTo(const char *pcpViewName, CString opDate);
    void			ChangeViewTo(const char *pcpViewName);
	CTime			StartTime() const;
	CTime			EndTime() const;
	int				Lines() const;
	DEMDATA_LINE*	GetLine(int ipLineNo);
	void			SetStartTime(CTime opStartTime);	
	void			SetEndTime(CTime opEndTime);	
	void			CompressGroups(bool bpCompress);
	DEMDATA_LINE*	GetLine(DEMANDDATA *prpDem);
	static void		GetAllViewColumns(CStringArray& ropColumnFields);
	static void		GetDefaultViewColumns(CStringArray& ropColumnFields);
	static CString	GetViewColumnName(const CString& ropColumnField);
	static CString	GetViewColumnField(const CString& ropColumnName);
	static const char*	omAllocGroupTypes[];
private:
	static void		DemandTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
    void			PrepareFilter();
	void			PrepareSorter();
	void			PrepareGrouping();
	BOOL			IsSameGroup(DEMDATA_LINE *prpDem1,DEMDATA_LINE *prpDem2);

	BOOL			IsPassFilter(DEMDATA_LINE *prpDemand);
	int				CompareDemand(DEMDATA_LINE *prpDem1, DEMDATA_LINE *prpDem2);

	void			MakeLines();
	void			MakeLine(DEMANDDATA *prpDem);
	void			MakeLine(DEMANDDATA *prpDem,DEMDATA_LINE& opLine);
	void			MakeLine(CCSPtrArray<DEMANDDATA>& ropTeamDemands);
	BOOL			FindTeamLeaderOf(DEMANDDATA *prpDem,int &ripLineno);
	BOOL			FindTeamLeaderOf(long lpDemUrno,int &ripLineno);
	BOOL			FindTeamOf(DEMANDDATA *prpDem,int &ripLineno);

	void			DeleteAll();
	int				CreateLine(DEMDATA_LINE *prpGate);
	void			DeleteLine(int ipLineno);

	void			UpdateDisplay(BOOL bpEraseAll = TRUE);
	void			Format(DEMDATA_LINE *prpLine,CCSPtrArray<TABLE_COLUMN>& rrpColumns);
	BOOL			EvaluateFlight(FLIGHTDATA *prpFlight,DEMDATA_LINE& rrpLine);
	BOOL			EvaluateTime(FLIGHTDATA *prpFlight,DEMDATA_LINE& rrpLine);
	CString			GetTifName(const char *polTis);
	BOOL            IsTerminalCorrect(DEMDATA_LINE *prpDemand) const;	 //Singapore
	BOOL            IsTerminalCorrect(DEMANDDATA  *prpDemand) const;  //Singapore
	COLORREF		GetBackgroundColorForPRM();  //Singapore
	bool			IsFinalized(DEMDATA_LINE * prpDem); 
// Attributes used for filtering condition
private:
	int				imGroupBy;
    CWordArray		omSortOrder;
	CMapStringToPtr omCMapForAloc;
	CMapStringToPtr omCMapForDety;
	CMapStringToPtr omCMapForRety;
	CMapStringToPtr	omCMapForAlids;
	CMapStringToPtr	omCMapForAlcd;
	CMapStringToPtr omCMapForRank;
	CMapStringToPtr omCMapForServices;
	CMapStringToPtr omCMapForPermits;
	CMapStringToPtr omCMapForNature;
	CMapStringToPtr omCMapForAirport;
	CMapStringToPtr	bmUseAllAlids;
	CMapStringToPtr omCMapForCic;
	CMapStringToPtr omCMapForBelt1;

	bool			bmUseAllAirlines;
	bool			bmUseAllAirports;
	bool			bmUseAllAlocs;
	bool			bmUseAllDetys;
	bool			bmUseAllRanks;
	bool			bmUseAllServices;
	bool			bmUseAllPermits;
	bool			bmUseAllNatures;
	bool			bmUseAllRetys;
	bool			bmCompressGroups;
	bool			bmUseAllCics;
	bool			bmUseAllBelt1s;
	bool	bmSortDemStartAscending;
	bool	bmSortDemEndAscending;

	//added by MAX
	bool	bmSortDemTerminalAscending;

// Attributes
private:
    CCSTable*		pomTable;
    CCSPtrArray<DEMDATA_LINE> omLines;
	CMapPtrToPtr	omMapUrnoToLine;
	CTime			omStartTime;
	CTime			omEndTime;
    CString			omDate;
	CCSPtrArray <DEMDATA_FIELD> omTableFields;
	CString			omTisa;	
	CString			omTisd;
	CString omTerminal; //Singapore

	CCSPtrArray <TABLE_COLUMN> omColumns;
	void MakeColumns(void);

// DDX callback function processing
private:
	int  ProcessJodNew(JODDATA *prlJod);
	int  ProcessJodNew(long lpUdem);
	int  ProcessJodDelete(JODDATA *prlJod);
	int  ProcessJodDelete(long lpUdem);
	void ProcessJodChange(JODDATA *prpJod);
	void ProcessJodChanges(int ipDDXType,JODDATA *prpJod);
	void ProcessDemandChange(DEMANDDATA *prpDemand);
	void ProcessDemandDelete(DEMANDDATA *prpDemand);
	void ProcessFlightChanges(int ipDDXType,FLIGHTDATA *prpFlight);

	int  DeleteLine(DEMDATA_LINE *prpLine);
	bool EvaluateTableFields();
	int  TableFieldIndex(const CString& ropField);
	int  RemoveLine(DEMDATA_LINE *prlLine);
	int  ChangeLine(DEMDATA_LINE *prlLine,BOOL bpResort = TRUE);
	int  InsertLine(DEMDATA_LINE *prpDem);
	void ExpandGroup(DEMDATA_LINE *prpGroup);
	void CompressGroup(DEMDATA_LINE *prpGroup);
	BOOL RecalculateTeamLine(DEMDATA_LINE *prlTeamLine);

	const char *AlocFromAllocGroupType(const char *pcpAllocGroupType);

// Printing functions
private:
	BOOL PrintDemandLine(DEMDATA_LINE *prpLine,BOOL bpIsLastLine);
	BOOL PrintDemandHeader(CCSPrint *pomPrint);
	CCSPrint *pomPrint;
public:
	void PrintView();
	void SetTerminal(const CString& ropTerminal){omTerminal = ropTerminal;}    //Singapore
	CString GetTerminal() const {return omTerminal;}                           //Singapore
	CString GetTerminalNumber(DEMDATA_LINE *prpLine) const;                          //Singapore
	CString GetTerminalNumber(DEMANDDATA *prpDemand) const;                          //Singapore
	int CompareTerminalNumber(DEMDATA_LINE *prpLine1, DEMDATA_LINE *prpLine2); //Singapore
	void GetLines(CCSPtrArray<DEMDATA_LINE>& opLines) const{opLines = omLines;} //Singapore
	CCSPtrArray<DEMDATA_LINE>& GetLines(){return omLines;} //Singapore
};

#endif //__DEMANDTABLEVIEWER_H__