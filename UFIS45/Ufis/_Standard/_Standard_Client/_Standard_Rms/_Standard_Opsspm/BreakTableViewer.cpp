// BreakTableViewer.cpp -- the viewer for Demand Table
//
 
#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>
#include <CedaDemandData.h>
#include <OpssPm.h>
#include <ccstable.h>
#include <CedaJobData.h>
#include <BasicData.h>
#include <DataSet.h>
#include <ccsddx.h>
#include <Buttonlist.h>
#include <BreakTableViewer.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

static struct BREAKTABLEDATA_FIELD osfields[]	= 
	{
		{	BREAKTABLEDATA_FIELD("NAME", "",		200,	400)	},
		{	BREAKTABLEDATA_FIELD("BREAKBEGIN","",	70,		100)	},
		{	BREAKTABLEDATA_FIELD("BREAKEND","",		70,		100)	},
		{	BREAKTABLEDATA_FIELD("JOBBEGIN","",		70,		100)	},
		{	BREAKTABLEDATA_FIELD("JOBEND","",		70,		100)	},
		{	BREAKTABLEDATA_FIELD("JOBSTATUS","",	100,	100)	},
		{	BREAKTABLEDATA_FIELD("JOBTEXT","",		600,	400)	},
	};

/////////////////////////////////////////////////////////////////////////////
// BreakTableViewer

BreakTableViewer::BreakTableViewer()
{
    SetViewerKey("StaffDia");
    pomTable = NULL;
	bmDisplayNonOverlappingOnly = false;

    ogCCSDdx.Register(this, JOB_CHANGE,	CString("BreakTableViewer"), CString("Job Change"),	BreakTableCf);
    ogCCSDdx.Register(this, JOB_DELETE,	CString("BreakTableViewer"), CString("Job Delete"),	BreakTableCf);
	
	CTime olDay = ogBasicData.GetTimeframeStart();
	omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omEndTime   = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);

	// must initialize names here because resource handle is not available during static initialization
	if (!osfields[0].Name.GetLength())
	{
		osfields[0].Name = GetString(IDS_BREAKTAB_NAME);
		osfields[1].Name = GetString(IDS_BREAKTAB_BREAKBEGIN);
		osfields[2].Name = GetString(IDS_BREAKTAB_BREAKEND);
		osfields[3].Name = GetString(IDS_BREAKTAB_JOBBEGIN);
		osfields[4].Name = GetString(IDS_BREAKTAB_JOBEND);
		osfields[5].Name = GetString(IDS_BREAKTAB_JOBSTATUS);
		osfields[6].Name = GetString(IDS_BREAKTAB_JOBTEXT);
	}

}

BreakTableViewer::~BreakTableViewer()
{
    ogCCSDdx.UnRegister(this, NOTUSED);
	omTableFields.DeleteAll();
	DeleteAll();
}

void BreakTableViewer::Attach(CCSTable *popTable)
{
	pomTable = popTable;
}

void BreakTableViewer::ChangeViewTo(const char *pcpViewName)
{
    SelectView(pcpViewName);
    DeleteAll();    
	PrepareFilter();
	EvaluateTableFields();
    MakeLines();
	UpdateDisplay();
}

void BreakTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int BreakTableViewer::CompareBreakStart(BREAKTABLEDATA_LINE *prpLine1, BREAKTABLEDATA_LINE *prpLine2)
{
	int ilVal = prpLine1->BreakStart.GetTime() - prpLine2->BreakStart.GetTime();
	if(ilVal == 0)
	{
		ilVal = strcmp(prpLine1->EmpName, prpLine2->EmpName);
	}

	return ilVal;
}

BOOL BreakTableViewer::IsPassFilter(JOBDATA *prpJob)
{
	BOOL blRc = FALSE;
    if(!strcmp(prpJob->Jtco,JOBBREAK) && prpJob->Acfr <= omEndTime && prpJob->Acto >= omStartTime && prpJob->Stat[0] == 'P')
	{
		CStringArray olPermits;
		CString olFunction, olGroupName;
		SHIFTDATA *prlShift = NULL;

		JOBDATA *prlPJ = ogJobData.GetJobByUrno(prpJob->Jour);
		if(prlPJ != NULL)
		{
			olFunction = ogBasicData.GetFunctionForPoolJob(prlPJ);
			ogBasicData.GetPermitsForPoolJob(prlPJ,olPermits);
			olGroupName = ogBasicData.GetTeamForPoolJob(prlPJ);
			prlShift = ogShiftData.GetShiftByUrno(prpJob->Shur);
		}

		if(prlPJ != NULL && prlShift != NULL)
		{
			blRc = IsPassFilter(prlPJ->Alid, olGroupName, prlShift->Sfca, olFunction, ogShiftData.IsAbsent(prlShift), olPermits);
		}
	}

	return blRc;
}

bool BreakTableViewer::IsPassFilter(const char *pcpPoolId, const char *pcpTeamId,
	const char *pcpShiftCode, const char *pcpRank, bool bpIsAbsent, CStringArray &ropPermits)
{
	void *p;
	BOOL blIsPoolOk = bmUseAllPools || omCMapForPool.Lookup(CString(pcpPoolId), p);
	BOOL blIsTeamOk = bmUseAllTeams || (strlen(pcpTeamId) == 0 && bmEmptyTeamSelected) || omCMapForTeam.Lookup(CString(pcpTeamId), p);
	BOOL blIsShiftCodeOk = bmUseAllShiftCodes || omCMapForShiftCode.Lookup(CString(pcpShiftCode), p);
	BOOL blIsRankOk =  bmUseAllRanks || omCMapForRank.Lookup(CString(pcpRank), p);
	BOOL blIsPermitOk = bmUseAllPermits;
	int ilNumPermits = ropPermits.GetSize();
	for(int ilPermit = 0; !blIsPermitOk && ilPermit < ilNumPermits; ilPermit++)
	{
		blIsPermitOk = omCMapForPermits.Lookup(ropPermits[ilPermit], p);
	}
	if (bpIsAbsent)
	{
		blIsShiftCodeOk = TRUE;
	}
	return (blIsPoolOk && blIsTeamOk && blIsShiftCodeOk && blIsRankOk && blIsPermitOk);
}

void BreakTableViewer::PrepareFilter()
{
	bmUseAllPools = SetFilterMap("Pool", omCMapForPool, GetString(IDS_ALLSTRING));

	bmUseAllTeams = SetFilterMap("Team", omCMapForTeam, GetString(IDS_ALLSTRING));
	bmEmptyTeamSelected = CheckForEmptyValue(omCMapForTeam, GetString(IDS_NOWORKGROUP));

	bmUseAllShiftCodes = SetFilterMap("Schichtcode", omCMapForShiftCode, GetString(IDS_ALLSTRING));
	bmUseAllRanks = SetFilterMap("Dienstrang", omCMapForRank, GetString(IDS_ALLSTRING));
	bmUseAllPermits = SetFilterMap("Permits", omCMapForPermits, GetString(IDS_ALLSTRING));
}

void BreakTableViewer::MakeLines()
{
	int ilNumJobs = ogJobData.omData.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		JOBDATA *prlJob = &ogJobData.omData[ilJob];
		if(IsPassFilter(prlJob))
		{
			MakeLine(prlJob);
		}
	}
}

void BreakTableViewer::MakeLine(JOBDATA *prpBreakJob, bool bpUpdateTable /*=false*/)
{
	BREAKTABLEDATA_LINE olLine;
	MakeLineData(prpBreakJob, olLine);

	if(!bmDisplayNonOverlappingOnly || !olLine.JobOverlapsBreak)
	{
		int ilNewLine = CreateLine(&olLine);
		if(bpUpdateTable && ilNewLine >= 0 && ilNewLine < omLines.GetSize())
		{
			BREAKTABLEDATA_LINE *prlLine = &omLines[ilNewLine];
			Format(prlLine, omColumns);
			pomTable->InsertTextLine(ilNewLine, omColumns, prlLine);

			COLORREF olTextColour = BLACK, olBackgroundColour = WHITE;
			if(prlLine->JobOverlapsBreak)
			{
				olTextColour = BLACK;
				olBackgroundColour = SILVER;
			}
			pomTable->SetTextLineColor(ilNewLine, olTextColour, olBackgroundColour);
		}
	}
}

void BreakTableViewer::MakeLineData(JOBDATA *prpBreakJob, BREAKTABLEDATA_LINE &ropLine)
{
	ropLine.ShiftUrno = prpBreakJob->Shur;
	ropLine.EmpName = ogEmpData.GetEmpName(prpBreakJob->Ustf);

	ropLine.BreakUrno = prpBreakJob->Urno;
	ropLine.BreakStart = prpBreakJob->Acfr;
	ropLine.BreakEnd = prpBreakJob->Acto;

	GetNextJobData(prpBreakJob, ropLine);
}

void BreakTableViewer::GetNextJobData(JOBDATA *prpBreakJob, BREAKTABLEDATA_LINE &ropLine)
{
	JOBDATA *prlNextJob = NULL;
	ropLine.JobOverlapsBreak = false;
	ropLine.JobFound = false;

	int ilBestDiff = 100000, ilDiff;

	CCSPtrArray <JOBDATA> olJobs;
	ogJobData.GetJobsByShur(olJobs, prpBreakJob->Shur, FALSE);
	int ilNumJobs = olJobs.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		JOBDATA *prlJob = &olJobs[ilJob];
		if(strcmp(prlJob->Jtco,JOBPOOL) && strcmp(prlJob->Jtco,JOBEQUIPMENTFASTLINK) &&
			strcmp(prlJob->Jtco,JOBEQUIPMENT) && strcmp(prlJob->Jtco,JOBBREAK))
		{
			if(IsReallyOverlapped(prlJob->Acfr, prlJob->Acto, prpBreakJob->Acfr, prpBreakJob->Acto))
			{
				prlNextJob = prlJob;
				ropLine.JobOverlapsBreak = true;
				break;
			}
			else
			{
				ilDiff = prlJob->Acfr.GetTime() - prpBreakJob->Acto.GetTime();
				if(ilDiff > 0 && ilDiff < ilBestDiff)
				{
					prlNextJob = prlJob;
					ilBestDiff = ilDiff;
				}
			}
		}
	}

	if(prlNextJob != NULL)
	{
		ropLine.JobStart = prlNextJob->Acfr;
		ropLine.JobEnd = prlNextJob->Acto;
		ropLine.JobStatus = ogJobData.GetJobStatusString(prlNextJob);
		ropLine.JobText = ogDataSet.JobBarText(prlNextJob);
		ropLine.JobFound = true;
	}
}

int BreakTableViewer::CreateLine(BREAKTABLEDATA_LINE *prpLine)
{
    int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareBreakStart(prpLine, &omLines[ilLineno]) <= 0)
            break;  
#else
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareBreakStart(prpLine, &omLines[ilLineno-1]) >= 0)
            break;  
#endif

    omLines.NewAt(ilLineno, *prpLine);
    return ilLineno;
}

void BreakTableViewer::DeleteLine(int ipLineno)
{
    omLines.DeleteAt(ipLineno);
}

void BreakTableViewer::UpdateDisplay()
{
	if(pomTable != NULL)
	{
		pomTable->ResetContent();

		CreateTableColumnsAndHeader();
		pomTable->SetHeaderFields(omHeader);

		pomTable->DisplayTable();

		int ilNumLines = omLines.GetSize();
		for (int i = 0; i < ilNumLines; i++) 
		{
			BREAKTABLEDATA_LINE *prlLine = &omLines[i];

			Format(prlLine, omColumns);
			pomTable->AddTextLine(omColumns, prlLine);

			COLORREF olTextColour = BLACK, olBackgroundColour = WHITE;
			if(prlLine->JobOverlapsBreak)
			{
				olTextColour = BLACK;
				olBackgroundColour = SILVER;
			}
			pomTable->SetTextLineColor(i, olTextColour, olBackgroundColour);
		}

		pomTable->DisplayTable();
	}
}

void BreakTableViewer::CreateTableColumnsAndHeader()
{
	omHeader.DeleteAll();
	omColumns.DeleteAll();
	int ilNumFields = omTableFields.GetSize();
	for (int ilC = 0; ilC < ilNumFields; ilC++)
	{
		omHeader.NewAt(ilC,TABLE_HEADER_COLUMN());
		omHeader[ilC].Text   = omTableFields[ilC].Name;
		omHeader[ilC].Length = omTableFields[ilC].Length;
		omColumns.NewAt(ilC,TABLE_COLUMN());
	}
}

void BreakTableViewer::Format(BREAKTABLEDATA_LINE *prpLine, CCSPtrArray<TABLE_COLUMN>& rrpColumns)
{
	if (rrpColumns.GetSize() != omTableFields.GetSize())
		return;

	SetColumn("NAME", prpLine->EmpName, rrpColumns);
	SetColumn("BREAKBEGIN", prpLine->BreakStart.Format("%H:%M/%d"), rrpColumns);
	SetColumn("BREAKEND", prpLine->BreakEnd.Format("%H:%M/%d"), rrpColumns);

	if(prpLine->JobFound)
	{
		SetColumn("JOBBEGIN", prpLine->JobStart.Format("%H:%M/%d"), rrpColumns);
		SetColumn("JOBEND", prpLine->JobEnd.Format("%H:%M/%d"), rrpColumns);
		SetColumn("JOBSTATUS", prpLine->JobStatus, rrpColumns);
		SetColumn("JOBTEXT", prpLine->JobText, rrpColumns);
	}
	else
	{
		SetColumn("JOBBEGIN", "", rrpColumns);
		SetColumn("JOBEND", "", rrpColumns);
		SetColumn("JOBSTATUS", "", rrpColumns);
		SetColumn("JOBTEXT", "", rrpColumns);
	}
}

bool BreakTableViewer::SetColumn(const char *pcpFieldName, const char *pcpFieldValue, CCSPtrArray<TABLE_COLUMN>& rrpColumns)
{
	int ilIndex = TableFieldIndex(pcpFieldName);
	if(ilIndex >= 0)
	{
		rrpColumns[ilIndex].Text = pcpFieldValue;
		return true;
	}

	return false;
}

void BreakTableViewer::BreakTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    BreakTableViewer *polViewer = (BreakTableViewer *)popInstance;

	if(ipDDXType == JOB_CHANGE || ipDDXType == JOB_DELETE)
	{
		polViewer->ProcessJobChange((JOBDATA *)vpDataPointer);
	}
}

void BreakTableViewer::ProcessJobChange(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		DeleteLinesByShiftUrno(prpJob->Shur);
		InsertLinesForShiftUrno(prpJob->Shur);
		pomTable->DisplayTable();
	}
}

void BreakTableViewer::DeleteLinesByShiftUrno(long lpShiftUrno)
{
	int ilLine = -1, ilNumLines = omLines.GetSize();
	for(ilLine = (ilNumLines - 1); ilLine >= 0; ilLine--)
	{
		if(omLines[ilLine].ShiftUrno == lpShiftUrno)
		{
			omLines.DeleteAt(ilLine);
			pomTable->DeleteTextLine(ilLine);
		}
	}
}

void BreakTableViewer::InsertLinesForShiftUrno(long lpShiftUrno)
{
	CCSPtrArray <JOBDATA> olJobs;
	ogJobData.GetJobsByShur(olJobs, lpShiftUrno, FALSE);
	int ilNumJobs = olJobs.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		JOBDATA *prlJob = &olJobs[ilJob];
		if(IsPassFilter(prlJob))
		{
			MakeLine(prlJob, true);
		}
	}
}

CTime BreakTableViewer::StartTime() const
{
	return omStartTime;
}

CTime BreakTableViewer::EndTime() const
{
	return omEndTime;
}

int BreakTableViewer::LineCount() const
{
	return omLines.GetSize();
}

BREAKTABLEDATA_LINE *BreakTableViewer::GetLine(int ipLineNo)
{
	if (ipLineNo >= 0 && ipLineNo < omLines.GetSize())
		return &omLines[ipLineNo];
	else
		return NULL;
}

void BreakTableViewer::SetStartTime(CTime opStartTime)
{
	omStartTime = opStartTime;
}

void BreakTableViewer::SetEndTime(CTime opEndTime)
{
	omEndTime = opEndTime;
}

bool BreakTableViewer::EvaluateTableFields()
{
	omTableFields.DeleteAll();
	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int i = 0; i < ilSize; i++)
	{
		omTableFields.New(osfields[i]);
	}

	return true;
}

int BreakTableViewer::TableFieldIndex(const CString& ropField)
{
	for (int i = 0; i < omTableFields.GetSize(); i++)
	{
		if (omTableFields[i].Field == ropField)
			return i;
	}

	return -1;
}

