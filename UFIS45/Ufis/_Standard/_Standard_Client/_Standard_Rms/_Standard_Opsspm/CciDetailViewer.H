// CciDetailViewer.h : header file
// 

#ifndef _CCIDV_H_
#define _CCIDV_H_


#define CCIDATA	EMPDATA
#define CciData	CCIDATA

#include <CedaCcaData.h>


struct CCIDETAIL_BKBARDATA
{
	enum	BKBARTYPE
	{
		NONE		= -1,
		BLOCKED		= 0,
		DEDICATED	= 1,
		COMMON		= 2,
		CCA			= 3,
		PERSONNEL	= 4,
	};

	BKBARTYPE	Type;
    CString		Text;
    CString		StatusText;
    CTime		StartTime;
    CTime		EndTime;
    CBrush*		MarkerBrush;	// background bar will be only displayed in FRAMENONE, MARKFULL
	char		Dety[2];
	long		Uaft;
	long		Urno;
	long		Urue;
	long		Udem;
	long		Ulnk;
};

struct CCIDETAIL_GROUPDATA 
{
	CString Alid;			// Cci area's ID associated with this group
	// standard data for groups in general Diagram
    CString Text;
    CPtrArray Lines;        // CCSPtrArray <CCIDETAIL_LINEDATA>
};

struct CCIDETAIL_LINEDATA 
{
	// standard data for lines in general Diagram's chart
    CString Text;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
    CPtrArray Bars;         // keep bars in order of painting (leftmost is the bottommost)
    //CUIntArray TimeOrder;   // used for maintain the overlapping bars, see documentation below

    CCSPtrArray<CCIDETAIL_BKBARDATA> BkBars;	// Cca (location demand) background bar
};

struct CCIDETAIL_BARDATA 
{
	long		CciUrno;		// from FLTFRA.URNO
	long		Urno;
    CString		Text;
    // standard data for bars in general GanttLine
	CString		StatusText;		// long text used to be displayed at the status bar
    CTime		StartTime;
    CTime		EndTime;
    int			FrameType;
    int			MarkerType;
    CBrush*		MarkerBrush;
    int			OverlapLevel;	// zero for the top-most level, each level lowered by 4 pixels
//	CPtrArray Indicators;		// keep top scale indicators for this bar
};

struct CCIDETAIL_INDICATORDATA
{
	long DemandUrno;
	// standard data for indicators in general GanttBar
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
};


// Attention:
// Element "TimeOrder" in the struct "CCIDETAIL_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array CCIDETAIL_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the CCIDETAIL_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array CCIDETAIL_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// CciDetailViewer window

class CciDetailViewer: public CViewer
{
// Constructions
public:
								CciDetailViewer();
    virtual	~					CciDetailViewer();
	void						Init(char *pcpAlocAlid, BOOL bmIsFirstTime);
	void						RemoveAll();
	void						Attach(CWnd *popAttachWnd);

// Attributes
public :
	// Data Pointer
	CCSPtrArray<JOBDATA>*		GetJobsPtr(void) { return &omJobs; };
	
    int							GetLineCount(int ipGroupno);
    CCIDETAIL_LINEDATA*			GetLine(int ipGroupno, int ipLineno);
    CString						GetLineText(int ipGroupno, int ipLineno);
    int							CreateBar(int ipGroupno, int ipLineno, CCIDETAIL_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void						DeleteBar(int ipGroupno, int ipLineno, int ipBarno);
    int							GetBarCount(int ipGroupno, int ipLineno);
    CCIDETAIL_BARDATA*			GetBar(int ipGroupno, int ipLineno, int ipBarno);
    int							GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);
    int							GetBkBarCount(int ipGroupno, int ipLineno);
    CCIDETAIL_BKBARDATA*		GetBkBar(int ipGroupno, int ipLineno, int ipBarno);
	int							GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);
	CCIDETAIL_BKBARDATA*		MakeCcaBkBar(CCADATA *prlCca);
	void						MakeDemBkBarsForCcaBkBar(CCIDETAIL_BKBARDATA *prpCcaBkBar);
	CCIDETAIL_BKBARDATA*		MakeDemBkBar(DEMANDDATA *prpDem);
	CCIDETAIL_LINEDATA*			FindDemandLineForJob(long lpJobUrno);
	CCIDETAIL_LINEDATA*			FindFreeSpaceOnLine(CTime opStart, CTime opEnd);
	bool						IsNearlyOverlapped(CTime opStart1, CTime opEnd1, CTime opStart2, CTime opEnd2);

	// Indicator
    int							CreateIndicator(int ipGroupno, int ipLineno, int ipBarno, CCIDETAIL_INDICATORDATA *prpIndicator);
	int							GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno);
	CCIDETAIL_INDICATORDATA*	GetIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno);
	
	// Special routine for my CciDetailGantt
	CString						GetDesk()			{ return cmAlocAlid; };
	CTime						GetTimescaleStart()	{ return omTimescaleStart; };
	CTime						GetTimescaleEnd()	{ return omTimescaleEnd; };

// helpers
private:
	static void					CciDetailCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void						ProcessCciChange(CCIDATA *prpCci);
	void						ProcessJobChange(JOBDATA *prpJob);
// implementation data
protected:
	CTime						omTimescaleStart;
	CTime						omTimescaleEnd;
	CBrush						omDedicatedCcaBkBrush;
	CBrush						omCommonCcaBkBrush;
	CBrush						omDemandBrush;
	CBrush						omBlockedBrush;
	char						lmEmpUrno[24];
	char						cmAlocAlid[24];
	CWnd*						pomAttachWnd;
	CCSPtrArray<JOBDATA>		omJobs;
	CPtrArray					omCciLines;
								
	BOOL						bmIsFirstTime;
};

/////////////////////////////////////////////////////////////////////////////

#endif
