// FlightJobsTable.h : header file
//
#ifndef __FLIGHTJOBSTABLE_H__
#define __FLIGHTJOBSTABLE_H__

#include <FlightJobsTableViewer.h>

/////////////////////////////////////////////////////////////////////////////
// FlightJobsTable dialog

class FlightJobsTable : public CDialog
{
// Construction
public:
	FlightJobsTable(CWnd* pParent = NULL);   // standard constructor
	~FlightJobsTable();
	void UpdateView();
	void SetCaptionText(void);
	void SetDate(CTime opDate);


private :
    CTable *pomTable; // visual object, the table content
	BOOL bmIsViewOpen;
	FlightJobsTableViewer omViewer;
	int m_nDialogBarHeight;
	CRect omWindowRect; //PRF 8712

private:
	static void FlightJobsTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void HandleGlobalDateUpdate();
	void SetViewerDate();
	void UpdateComboBox();

// Dialog Data
	//{{AFX_DATA(FlightJobsTable)
	enum { IDD = IDD_FLIGHTJOBSTABLE };
	CComboBox	m_Date;
	//}}AFX_DATA

 
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FlightJobsTable)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FlightJobsTable)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y); //PRF 8712
	afx_msg void OnSelchangeViewcombo();
	afx_msg void OnView();
	afx_msg void OnPrint();
	afx_msg void OnExport();
	afx_msg void OnCloseupViewcombo();
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnClose();
	afx_msg void OnSelchangeDate();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnTableUpdateDataCount(); //Singapore
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Drag-and-drop implementation
private:
	CCSDragDropCtrl m_DragDropTarget;
	long imDropFlightUrno;
	long imDropDemandUrno;
};

#endif //__FLIGHTJOBSTABLE_H__
