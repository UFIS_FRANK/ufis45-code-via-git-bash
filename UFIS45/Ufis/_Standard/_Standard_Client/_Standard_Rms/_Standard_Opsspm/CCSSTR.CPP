// This may look like C code, but it is really -*- C++ -*-

#include <stdafx.h>
#include <ccsstr.h>
#include <ccsdefs.h>

#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream.h>

static const int   default_chunk_size = 8;
static const char *empty_string = "";

// functions

#include <stdarg.h>		// to get varargs

// Description:
// Format, according to \fIfmt\fP the arguments to ASFormat() and
// into an CCSString.  Uses printf(3) style arguments.
CCSString CCSStringFormat (const char *fmt, ...)
{
  va_list arg_list;
  char	s[1024]; // bad...
    
  va_start(arg_list, fmt);
  (void) vsprintf(s, fmt, arg_list);
  va_end(arg_list);

  return s;
}

// class CCSString
//

// private methods
void CCSString::BigEnough(const int size)
 {
  if(size < 0)
  {
#if 0
    cerr << className() << "BigEnough: arg size = " << size
         << " is less than zero." << endl;
#endif    
    exit(1);
  }
    
  if(size >= 0 && mem < (size + 1))
  {
    char* tmp = contents;
    
    // have to do division before multiplication because
    // we're doing integer arithmetic and we want the division
    // step to truncate the answer
	
    int num_chunks = ((size + 1) + (Chunksize() - 1)) / Chunksize();
    mem = num_chunks * Chunksize();
    contents = new char[mem];
    contents[0] = '\0';
    
    if(tmp != NULL)
      strcpy(contents, tmp);
    delete tmp;
  }
}

void CCSString::InitVars() 
{
  Length(0);
  mem = 0;
  contents = NULL;
  Chunksize(default_chunk_size);
}

int CCSString::InRange(const long i) const 
{
  return(i >= 0 && i < Length());
}

void CCSString::Length(const int new_length)
{
  len = new_length >= 0 ? new_length : -1;
}

// protected methods

// Description:
// Called by the equality operators. Returns an integer greater than,
// equal to, or less than 0, according as the current CCSString is
// lexicographically greater than, equal to, or less than the CCSString
// argument \fIas\fP.
int CCSString::Compare(const CCSString& as) const
{
  if(this == &as)		// they're the same CCSString
    return 0;
  int asIsEmpty	  = as.Empty();
  int thisIsEmpty = Empty();
  
  if(asIsEmpty && thisIsEmpty) // both are empty
    return 0;		 // contents == as.contents
  else if(asIsEmpty || thisIsEmpty) // only one is empty
    return asIsEmpty ? 1 : -1;
  else			      // neither are empty
    return strcmp(contents, as.contents);
}

// Description:
// Called by the equality operators. Returns an integer greater than,
// equal to, or less than 0, according as the current CCSString is
// lexicographically greater than, equal to, or less than the const char*
// argument \fIs\fP.
int CCSString::Compare(const char* s) const 
{
  if(contents == s)
    return 0;		// they're the same pointer
    
  int sIsEmpty = (s == NULL ? TRUE : (strlen(s) == 0 ? TRUE : FALSE));
  int thisIsEmpty = Empty();

  if(sIsEmpty && thisIsEmpty) // both are empty
    return 0;		 // s == as.s
  else if(sIsEmpty || thisIsEmpty) // only one is empty
    // sIsEmpty ? contents > s : contents < s
    return sIsEmpty ? 1 : -1;
  else			     // neither are empty
    return strcmp(contents, s);
}

// Description:
// Appends the contents of the CCSString argument \fIas\fP to the end of
// the current CCSString.
void CCSString::Concat(const CCSString& as)
{
  if(!as.Empty())
  {
    int this_length	= Length();
    int as_length	= as.Length();
    int m		= this_length + as_length;
    BigEnough(m);		// make BigEnough before assigning pointer
    char* start_pointer = &contents[this_length];
    Length(m);

    // do strncpy (instead of strcpy) in case as is same CCSString as this
    strncpy(start_pointer, as.contents, as_length);
    contents[m] = '\0';
  }
}

// Description:
// Appends the contents of the const char* argument \fIs\fP to the end
// of the current CCSString.
void CCSString::Concat(const char* s)
{
  if(s != NULL)
  {
    int this_length	= Length();
    int s_length	= strlen(s);
    int m		= this_length + s_length;
    BigEnough(m);		// make BigEnough before assigning pointer
    char* start_pointer = &contents[this_length];
    Length(m);
    strncpy(start_pointer, s, s_length);
    contents[m] = '\0';
  }
}
	
// Description:
// Appends the const char argument \fIc\fP to the end of the current
// CCSString.
void CCSString::Concat(const char& c)
{
  int m = Length();
  BigEnough(m + 1);
  contents[m++] = c;
  contents[m] = '\0';
  Length(m);
}

// Description:
// Replaces the old contents of the current CCSString with the contents
// of the CCSString argument \fIas\fP.
void CCSString::Copy(const CCSString& as)
{
  if(this != &as)		// if they're not the same CCSString
    if(!as.Empty())
    {
      Length(as.Length());
      BigEnough(Length());
      strcpy(contents, as.contents);
    }
}

// Description:
// Replaces the old contents of the current CCSString with the contents
// of the const char* argument \fIcp\fP.
void CCSString::Copy(const char* cp)
{
  if(contents != cp)		// if they're not the same pointer
    if(cp != NULL)
    {
      Length(strlen(cp));
      BigEnough(Length());
      strcpy(contents, cp);
    }
}

// Description:
// Replaces the old contents of the current CCSString with the const
// char argument \fIc\fP.
void CCSString::Copy(const char& c)
{
  Length(1);
  BigEnough(Length());
  contents[0] = c;
  contents[1] = '\0';
}

// Description:
// Returns 1 if the variable \fIcontents\fP is equal to NULL and 0
// otherwise.
int CCSString::IsNull() const
{
  return(contents == NULL);
}

// public members

// Description:
// Default constructor.
CCSString::CCSString()
{
  InitVars();
}

// copy constructor
CCSString::CCSString(const CCSString& as)
{
  InitVars();
  Copy(as);
}

// construct CCSString from pointer
CCSString::CCSString(const char* cp)
{
  InitVars();
  Copy(cp);
}

// construct CCSString from char
CCSString::CCSString(char c)
{
  InitVars();
  Copy(c);
}

// construct CCSString from long
CCSString::CCSString(long l)
{
  char s[16];
  sprintf(s, "%ld", l);
  InitVars();
  Copy(s);
}

// construct CCSString from double
CCSString::CCSString(double d)
{
  char s[16];
  sprintf(s, "%g", d);
  InitVars();
  Copy(s);
}

CCSString::~CCSString()
{
  delete contents;
}

// Description:
// Returns the size of memory allocation hunks.
int CCSString::Chunksize() const
{
  return(chunk_size > 0 ? chunk_size : default_chunk_size);
}

// Description:
// Changes the size of memory allocation hunks.
void CCSString::Chunksize(const int new_size)
{
  if(new_size > 0)
    chunk_size = new_size;
}

// Description:
// Returns 1 if the string is empty, 0 otherwise.
int CCSString::Empty() const
{
  return (Length() == 0);
}

// Description:
// Returns the number of characters in the string.
// Always non-negative.
int CCSString::Length() const
{
  if(len == -1){
    CCSString* that = (CCSString*)(this); // cast away const-ness
    that->Length(IsNull() ? 0 : strlen(contents));
  }
  return(len);
}

// Description:
// Assigns one CCSString to another. The user doesn't have to worry
// about there being enough space to hold the new string.
CCSString& CCSString::operator=(const CCSString& as)
{
  if(this != &as){				// check for s = s
    delete contents;			// zap the current guy
    InitVars();
    Chunksize(as.Chunksize());
    Copy(as);
  }
  return *this;
}

// Description:
// Assigns a char* to an CCSString. The user doesn't have to worry
// about there being enough space to hold the new string.
CCSString& CCSString::operator=(const char* s)
{
  delete contents;			// zap the current guy
  InitVars();
  Copy(s);
  return *this;
}

// Description:
// Returns the (read-only) value of the \fIi\fPth character,
// counting from zero.
const char& CCSString::operator[](const long i) const
{
  if(!InRange(i))
  {
#if 0
    cerr << className() << "[" << i << "], i is out of range" << endl;
#endif
    exit(1);
  }
  return(contents[i]);
}

// Description:
// Add an CCSString to the end of the current CCSString.
CCSString& CCSString::operator+=(const CCSString& as)
{
  Concat(as);
  return *this;
}

// Description:
// Add a char* to the end of the current CCSString.
CCSString& CCSString::operator+=(const char* s)
{
  Concat(s);
  return *this;
}

// Description:
// Return a (read-only) character pointer to the current CCSString.
CCSString::operator const char*() const
{
  if(IsNull())
    return empty_string;
  else
    return(contents);
}

// Description:
// Return a lower-case'd version of the current CCSString.
CCSString CCSString::ToLower() const
{
  CCSString t = *this;
  for(int i = 0; i < len; i++)
    t.contents[i] = tolower(t.contents[i]);
  return t;
}

// friend functions

// Description:
// Return an CCSString formed by joining two CCSStrings.
CCSString operator+(const CCSString& as, const CCSString& bs)
{
  CCSString tmp;
  tmp.BigEnough(as.Length() + bs.Length());
  tmp.Concat(as);
  tmp.Concat(bs);
  return tmp;
}

// Description:
// Return an CCSString formed by joining an CCSString and a const char*.
CCSString operator+(const CCSString& as, const char* s)
{
  CCSString tmp;
  tmp.BigEnough(as.Length() + (s == NULL ? 0 : strlen(s)));
  tmp.Concat(as);
  tmp.Concat(s);
  return tmp;
}

// Description:
// Return an CCSString formed by joining a const char* and an CCSString.
CCSString operator+(const char* s, const CCSString& as)
{
  CCSString tmp;
  tmp.BigEnough((s == NULL ? 0 : strlen(s)) + as.Length());
  tmp.Concat(s);
  tmp.Concat(as);
  return tmp;
}

// equality
int operator==(const CCSString& as, const CCSString& bs)
{
  return(as.Compare(bs) == 0);
}

int operator==(const CCSString& as, const char* s)
{
  return(as.Compare(s) == 0);
}

int operator==(const char* s, const CCSString& as)
{
  return(as.Compare(s) == 0);
}

// inequality
int operator!=(const CCSString& as, const CCSString& bs)
{
  return(as.Compare(bs) != 0);
}

int operator!=(const CCSString& as, const char* s)
{
  return(as.Compare(s) != 0);
}

int operator!=(const char* s, const CCSString& as)
{
  return(as.Compare(s) != 0);
}

// greater than
int operator>(const CCSString& as, const CCSString& bs)
{
  return(as.Compare(bs) > 0);
}

int operator>(const CCSString& as, const char* s)
{
  return(as.Compare(s) > 0);
}

int operator>(const char* s, const CCSString& as)
{
  return(as.Compare(s) < 0);	// reverse the order 
}

// greater than or equal to
int operator>=(const CCSString& as, const CCSString& bs)
{
  return(as.Compare(bs) >= 0);
}

int operator>=(const CCSString& as, const char* s)
{
  return(as.Compare(s) >= 0);
}

int operator>=(const char* s, const CCSString& as)
{
  return(as.Compare(s) <= 0);	// reverse the order
}

// less than
int operator<(const CCSString& as, const CCSString& bs)
{
  return(as.Compare(bs) < 0);
}

int operator<(const CCSString& as, const char* s)
{
  return(as.Compare(s) < 0);
}

int operator<(const char* s, const CCSString& as)
{
  return(as.Compare(s) > 0);	// reverse the order
}

// less than or equal to
int operator<=(const CCSString& as, const CCSString& bs)
{
  return(as.Compare(bs) <= 0);
}

int operator<=(const CCSString& as, const char* s)
{
  return(as.Compare(s) <= 0);
}

int operator<=(const char* s, const CCSString& as)
{
  return(as.Compare(s) >= 0);	// reverse the order
}

