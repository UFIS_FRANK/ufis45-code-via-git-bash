// LastJobInfo.h : header file
//
#ifndef __LASTJOBINFO__H__
#define __LASTJOBINFO__H__


/////////////////////////////////////////////////////////////////////////////
// LastJobInfo dialog

class LastJobInfo : public CDialog
{
// Construction
public:
	LastJobInfo(const long& rlpJobUrno, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(LastJobInfo)
	enum { IDD = IDD_JOBINFO };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(LastJobInfo)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	long lmJobUrno;

	// Generated message map functions
	//{{AFX_MSG(LastJobInfo)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#endif
