#if !defined(AFX_ABSENCEDLG_H__161D6F91_B15F_11D3_92C3_0000B4392C49__INCLUDED_)
#define AFX_ABSENCEDLG_H__161D6F91_B15F_11D3_92C3_0000B4392C49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AbsenceDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAbsenceDlg dialog

class CAbsenceDlg : public CDialog
{
// Construction
public:
	CAbsenceDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAbsenceDlg)
	enum { IDD = IDD_ABSENCEDLG };
	CListBox	m_AbsenceList;
	//}}AFX_DATA

	CString omAbsenceCode;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAbsenceDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAbsenceDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ABSENCEDLG_H__161D6F91_B15F_11D3_92C3_0000B4392C49__INCLUDED_)
