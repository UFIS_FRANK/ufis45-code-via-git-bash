// CedaJodData.cpp - Holds information about Job/Demand connection

#include <resource.h>
#include <stdafx.h>
#include <stdlib.h>
#include <string.h>
#include <CCSCedaData.h>
#include <ccsGlobl.h>
#include <ccsddx.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaFlightData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <BasicData.h>
#include <CCSBcHandle.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <conflict.h>
#include <CedaCfgData.h>
#include <BasicData.h>

extern CCSDdx ogCCSDdx;

void  ProcessJodCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
void  ProcessJodCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	
	if(ipDDXType == BC_JOD_CHANGE)
	{
		((CedaJodData *)popInstance)->ProcessJodUpdate(vpDataPointer);
	}
	else if(ipDDXType == BC_JOD_DELETE)
	{
		((CedaJodData *)popInstance)->ProcessJodDelete(vpDataPointer);
	}
	else if(ipDDXType == BC_JOD_UPD)
	{
		((CedaJodData *)popInstance)->ProcessJodsUpdate(vpDataPointer);
	}
}

void CedaJodData::ProcessJodUpdate(void *vpDataPointer)
{
	JODDATA *prlJod;
	struct BcStruct *prlJodData;
	prlJodData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlJodData);


	long llUrno = GetUrnoFromSelection(prlJodData->Selection);
	if(llUrno == 0L)
	{
		JODDATA rlJod;
		GetRecordFromItemList(&omRecInfo,&rlJod,prlJodData->Fields,prlJodData->Data);
		llUrno = rlJod.Urno;
	}

	if(llUrno > 0L)
	{
		if (omUrnoMap.Lookup((void *)llUrno,(void *& )prlJod) == TRUE)
		{
			JODDATA rlOldJod = *prlJod;
			GetRecordFromItemList(&omRecInfo,prlJod,prlJodData->Fields,prlJodData->Data);

			RemoveJodFromJodDemMap(&rlOldJod);
			RemoveJodFromDemJodMap(&rlOldJod);

			bool blJobHasChangedDemand = (prlJod->Udem != rlOldJod.Udem) ? true : false;

			if(blJobHasChangedDemand)
			{
				ogCCSDdx.DataChanged(this,JOD_DELETE,prlJod);
			}
				
			AddJodToJodDemMap(prlJod);
			AddJodToDemJodMap(prlJod);
			UpdateOnlineData(prlJod);

			if(blJobHasChangedDemand)
			{
				ogCCSDdx.DataChanged(this,JOD_NEW,prlJod);
			}
			else
			{
				ogCCSDdx.DataChanged(this,JOD_CHANGE,prlJod);
			}

			ogJobData.HandleJobTemplateChange(prlJod->Ujob, prlJod->Udem);
		}
		else
		{
			prlJod = AllocNewJod(NULL);
			GetRecordFromItemList(&omRecInfo,prlJod,prlJodData->Fields,prlJodData->Data);

			if(!AddIncomingJod(prlJod))
			{
				delete prlJod;
				prlJod = NULL;
			}
			else
			{
				ogCCSDdx.DataChanged(this,JOD_NEW,prlJod);
			}
		}
	}
}

bool CedaJodData::ProcessJodUpdate(long lpJodUrno, char *pcpFields, char *pcpData)
{
	bool blFound = false;

	JODDATA *prlJod;
	if((prlJod = GetJodByUrno(lpJodUrno)) != NULL)
	{
		blFound = true;
		JODDATA rlOldJod = *prlJod;
		GetRecordFromItemList(&omRecInfo,prlJod, pcpFields, pcpData);

		RemoveJodFromJodDemMap(&rlOldJod);
		RemoveJodFromDemJodMap(&rlOldJod);

		bool blJobHasChangedDemand = (prlJod->Udem != rlOldJod.Udem) ? true : false;

		if(blJobHasChangedDemand)
		{
			ogCCSDdx.DataChanged(this,JOD_DELETE,prlJod);
		}
			
		AddJodToJodDemMap(prlJod);
		AddJodToDemJodMap(prlJod);
		UpdateOnlineData(prlJod);

		if(blJobHasChangedDemand)
		{
			ogCCSDdx.DataChanged(this,JOD_NEW,prlJod);
		}
		else
		{
			ogCCSDdx.DataChanged(this,JOD_CHANGE,prlJod);
		}

		ogJobData.HandleJobTemplateChange(prlJod->Ujob, prlJod->Udem);
	}

	return blFound;
}

void CedaJodData::ProcessJodInsert(char *pcpFields, char *pcpData)
{
	JODDATA *prlJod = AllocNewJod(NULL);
	GetRecordFromItemList(&omRecInfo, prlJod, pcpFields, pcpData);

	if(!AddIncomingJod(prlJod))
	{
		delete prlJod;
		prlJod = NULL;
	}
	else
	{
		ogCCSDdx.DataChanged(this,JOD_NEW,prlJod);
	}
}

void CedaJodData::ProcessJodDelete(void *vpDataPointer)
{
	struct BcStruct *prlJodData;
	prlJodData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlJodData);

	long llJodUrno = GetUrnoFromSelection(prlJodData->Selection);
	if(llJodUrno == 0L)
	{
		JODDATA rlJod;
		GetRecordFromItemList(&omRecInfo,&rlJod,prlJodData->Fields,prlJodData->Data);
		llJodUrno = rlJod.Urno;
	}

	ProcessJodDelete(llJodUrno);
}

void CedaJodData::ProcessJodDelete(long lpUrno)
{
	JODDATA *prlJod = GetJodByUrno(lpUrno);
	if(prlJod != NULL)
	{
		JODDATA rlJod = *prlJod;
		DeleteJodInternal(rlJod.Urno);
		DeleteFromOnlineData(rlJod.Urno);
		ogCCSDdx.DataChanged(this,JOD_DELETE,&rlJod);
	}		
}

// broadcast block update:
// data list format: "FROM,TO"
// jod changes released from jobhdl
void CedaJodData::ProcessJodsUpdate(void *vpDataPointer)
{
	struct BcStruct *prlJodData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlJodData);
	ProcessAttachment(prlJodData->Attachment);
}

void CedaJodData::ProcessAttachment(CString &ropAttachment)
{
	CAttachment olAttachment(ropAttachment);
	if(olAttachment.bmAttachmentValid)
	{
		for(int ilI = 0; ilI < olAttachment.omInsertDataList.GetSize(); ilI++)
		{
			ProcessJodInsert(olAttachment.omFieldList.GetBuffer(0), olAttachment.omInsertDataList[ilI].GetBuffer(0));
		}

		for(int ilU = 0; ilU < olAttachment.omUpdateDataList.GetSize(); ilU++)
		{
			JODDATA rlJod;
			GetRecordFromItemList(&omRecInfo,&rlJod, olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0));
			if(!ProcessJodUpdate(rlJod.Urno, olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0)))
			{
				ProcessJodInsert(olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0));
			}
		}

		for(int ilD = 0; ilD < olAttachment.omDeleteDataList.GetSize(); ilD++)
		{
			ProcessJodDelete(atol(olAttachment.omDeleteDataList[ilD]));
		}
	}
}

CedaJodData::CedaJodData()
{
	bmUseJodtab = true;

    // Create an array of CEDARECINFO for JOBDATA
    BEGIN_CEDARECINFO(JODDATA, JodDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_LONG(Ujob,"UJOB")
        FIELD_LONG(Udem,"UDEM")
        FIELD_CHAR_TRIM(Usec,"USEC")
        FIELD_DATE(Cdat,"CDAT")
        FIELD_CHAR_TRIM(Useu,"USEU")
        FIELD_DATE(Lstu,"LSTU")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(JodDataRecInfo)/sizeof(JodDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&JodDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"JODTAB");
    pcmFieldList = "URNO,UJOB,UDEM,USEC,CDAT,USEU,LSTU";
	bmIsOffline = false;

	omData.SetSize(0,480);
	ogCCSDdx.Register((void *)this,BC_JOD_CHANGE,CString("JODDATA"), CString("Jod changed"),ProcessJodCf);
	ogCCSDdx.Register((void *)this,BC_JOD_DELETE,CString("JODDATA"), CString("Jod deleted"),ProcessJodCf);
	ogCCSDdx.Register((void *)this,BC_JOD_UPD,CString("JODDATA"), CString("Jod multi-update"),ProcessJodCf);
}

CedaJodData::~CedaJodData()
{
	TRACE("CedaJodData::~CedaJodData\n");
	
	ClearAll();
	omRecInfo.DeleteAll();
}

void CedaJodData::ClearAll()
{
	long llUrno;
	POSITION rlPos;

	CMapPtrToPtr *polJodDemMap;
	for(rlPos = omJobuMap.GetStartPosition(); rlPos != NULL; )
	{
		omJobuMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polJodDemMap);
		polJodDemMap->RemoveAll();
		delete polJodDemMap;
	}
	omJobuMap.RemoveAll();

	CMapPtrToPtr *polDemJodMap;
	for(rlPos = omDemuMap.GetStartPosition(); rlPos != NULL; )
	{
		omDemuMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polDemJodMap);
		polDemJodMap->RemoveAll();
		delete polDemJodMap;
	}
	omDemuMap.RemoveAll();
	
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
}

void CedaJodData::ReReadJods(CUIntArray &ropAddedJobUrnos)
{
	if(bmUseJodtab)
	{
		int ilNumJobUrnos = ropAddedJobUrnos.GetSize();
		int ilNumJodsAdded = 0;

		if(ilNumJobUrnos > 0)
		{
			CString olFormattedUrnos = ogBasicData.FormatUrnoList(ropAddedJobUrnos);

			char *pclWhere = new char[olFormattedUrnos.GetLength() + 50];
			sprintf(pclWhere,"WHERE UJOB IN (%s)", olFormattedUrnos);
			char pclCom[10] = "RT";
			if(CedaAction2(pclCom, pclWhere) != true)
			{
				ogBasicData.LogCedaError("CedaJodData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
			}
			else
			{
				bool ilRc = true;
				for(int ilLc = 0; ilRc == true; ilLc++)
				{
					bool blAddJod = false;
					JODDATA *prlJod = AllocNewJod(NULL);
					if((ilRc = GetBufferRecord2(&omRecInfo,ilLc,prlJod)) == true)
					{
						blAddJod = AddIncomingJod(prlJod);
					}

					if(blAddJod)
					{
						ogCCSDdx.DataChanged(this,JOD_NEW,prlJod);
						ilNumJodsAdded++;
					}
					else
					{
						delete prlJod;
						prlJod = NULL;
					}
				}
			}
			delete pclWhere;
		}

		ogBasicData.Trace("CedaJodData::ReReadJods() %d JODS added", ilNumJodsAdded);
	}
}

//bool CedaJodData::ReadAllJods(CTime opStartTime, CTime opEndTime)
//{
//	int ilNumJodsAdded = 0;
//    char pclWhere[1000] = "";
//
//	CTimeSpan olBuffer(0, 0, 0, 0);
//	CTime olStart = opStartTime - olBuffer;
//	CTime olEnd = opEndTime + olBuffer;
//
//	// check if the JOD's demand is in the timeframe - not the job, because the jobs
//	// outside the timeframe may be loaded if their demand is within the timeframe
////    sprintf(pclWhere,"WHERE UDEM IN (SELECT URNO FROM DEMTAB WHERE DEBE <= '%s' AND DEEN >= '%s')", 
////			olEnd.Format("%Y%m%d%H%M%S"), olStart.Format("%Y%m%d%H%M%S"));
//
////    sprintf(pclWhere,"WHERE UJOB IN (SELECT URNO FROM JOBTAB WHERE ACFR <= '%s' AND ACTO >= '%s')", 
////			olEnd.Format("%Y%m%d%H%M%S"), olStart.Format("%Y%m%d%H%M%S"));
//
////	if(!ogDemandData.omWhere.IsEmpty())
////	{
////		// load jods for timeframe +/- 1 day (using demand date)
////		sprintf(pclWhere,"WHERE UDEM IN (SELECT URNO FROM DEMTAB %s)",ogDemandData.omWhere);
////	}
//
//	// select demands for timeframe plus/minus 1 day -	when jobs which were outside timeframe but are changed so that they
//	//													are inside, don't want to reread the missing JODS
//	// select demands for all templates -				when jobs which are re-assigned from a template not loaded to a loaded templated
//	//													don't want to reread the missing JODS
//	CTime olJodStart = opStartTime - CTimeSpan(1,0,0,0), olJodEnd = opEndTime + CTimeSpan(1,0,0,0);
//	sprintf(pclWhere,"WHERE UDEM IN (SELECT URNO FROM DEMTAB WHERE DEBE BETWEEN '%s' AND '%s')", olJodStart.Format("%Y%m%d%H%M%S"), olJodEnd.Format("%Y%m%d%H%M%S"));
//
//	char pclCom[10] = "RT";
//    if(CedaAction2(pclCom, pclWhere) != true)
//	{
//		ogBasicData.LogCedaError("CedaJodData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
//	}
//	else
//	{
//		ClearAll();
//		bool ilRc = true;
//		for(int ilLc = 0; ilRc == true; ilLc++)
//		{
//			bool blAddJod = false;
//			JODDATA *prlJod = AllocNewJod(NULL);
//			if((ilRc = GetBufferRecord2(&omRecInfo,ilLc,prlJod)) == true)
//			{
//				blAddJod = AddIncomingJod(prlJod);
//			}
//
//			if(blAddJod)
//			{
//				ilNumJodsAdded++;
//			}
//			else
//			{
//				delete prlJod;
//				prlJod = NULL;
//			}
//		}
//	}
//
//	ogBasicData.Trace("CedaJodData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(JODDATA), pclWhere);
//    return (ilNumJodsAdded > 0) ? true : false;
//}


bool CedaJodData::ReadAllJods(CTime opStartTime, CTime opEndTime)
{

	CTimeSpan olBuffer(0, 0, 0, 0);
	CTime olStart = opStartTime - olBuffer;
	CTime olEnd = opEndTime + olBuffer;

	CTime olJodStart = opStartTime - CTimeSpan(1,0,0,0), olJodEnd = opEndTime + CTimeSpan(1,0,0,0);
	sprintf(cgSelection,"WHERE UDEM IN (SELECT URNO FROM DEMTAB WHERE DEBE BETWEEN '%s' AND '%s')", olJodStart.Format("%Y%m%d%H%M%S"), olJodEnd.Format("%Y%m%d%H%M%S"));
	return ReadJods(cgSelection, true);
}

bool CedaJodData::ReadJods(char *pcpSelection, bool bpReset /*= false*/)
{
	int ilNumJodsAdded = 0;

	bmUseJodtab = false;
	char pclTmpText[100];
	GetPrivateProfileString(pcgAppName, "USEJODTAB", "YES",pclTmpText, sizeof pclTmpText, ogBasicData.GetConfigFileName());
	if(!stricmp(pclTmpText,"YES"))
	{
		bmUseJodtab = ogBasicData.DoesFieldExist("JOD", "URNO");
	}

	if(bmUseJodtab)
	{
		char pclCom[10] = "RT";
		if(CedaAction2(pclCom, pcpSelection) != true)
		{
			ogBasicData.LogCedaError("CedaJodData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pcpSelection);
		}
		else
		{
			if(bpReset)
			{
				ClearAll();
			}
			bool ilRc = true;
			for(int ilLc = 0; ilRc == true; ilLc++)
			{
				bool blAddJod = false;
				JODDATA *prlJod = AllocNewJod(NULL);
				if((ilRc = GetBufferRecord2(&omRecInfo,ilLc,prlJod)) == true)
				{
					blAddJod = AddIncomingJod(prlJod);
				}

				if(blAddJod)
				{
					ilNumJodsAdded++;
				}
				else
				{
					delete prlJod;
					prlJod = NULL;
				}
			}
		}

		ogBasicData.Trace("CedaJodData Read %d Records (Rec Size %d) %s", ilNumJodsAdded, sizeof(JODDATA), pcpSelection);
	}
    return (ilNumJodsAdded > 0) ? true : false;
}

// handle a jod that was either read or received as a broadcast
bool CedaJodData::AddIncomingJod(JODDATA *prpJod)
{
	bool blAddJod = false;
	//if(prpJod != NULL && ogDemandData.GetDemandByUrno(prpJod->Udem) != NULL)
	if(prpJod != NULL) // need to add jods of all templates in case a job is reassigned from one template to another
	{
		if(bmIsOffline)
		{
			if(!WasDeletedOffline(prpJod->Urno))
			{
				AddJodInternal(prpJod);
				blAddJod = true;
			}
			UpdateOnlineData(prpJod);
		}
		else
		{
			AddJodInternal(prpJod);
			blAddJod = true;
		}
	}

	return blAddJod;
}

JODDATA *CedaJodData::AllocNewJod(JODDATA *prpJod)
{
	JODDATA *prlJod = new JODDATA;
	if(prpJod != NULL)
	{
		*prlJod = *prpJod;
	}
	return prlJod;
}

void CedaJodData::AddJodInternal(JODDATA *prpJod)
{
	JODDATA *prlJod;
	if (omUrnoMap.Lookup((void *)prpJod->Urno,(void *& )prlJod) == TRUE)
	{
		*prlJod = *prpJod;
	}
	else
	{
		omData.Add(prpJod);
		omUrnoMap.SetAt((void *)prpJod->Urno,prpJod);
		AddJodToJodDemMap(prpJod);
		AddJodToDemJodMap(prpJod);
	}
}

void CedaJodData::AddJodToJodDemMap(JODDATA *prpJod)
{
	if(prpJod != NULL)
	{
		CMapPtrToPtr *polJodDemMap;
		if (omJobuMap.Lookup((void *) prpJod->Ujob, (void *&) polJodDemMap) == TRUE)
		{
			polJodDemMap->SetAt((void *)prpJod->Udem,prpJod);
		}
		else
		{
			polJodDemMap = new CMapPtrToPtr;
			polJodDemMap->SetAt((void *)prpJod->Udem,prpJod);
			omJobuMap.SetAt((void *)prpJod->Ujob,polJodDemMap);
		}
	}
}

void CedaJodData::AddJodToDemJodMap(JODDATA *prpJod)
{
	if(prpJod != NULL)
	{
		CMapPtrToPtr *polDemJodMap;
		if (omDemuMap.Lookup((void *) prpJod->Udem, (void *&) polDemJodMap) == TRUE)
		{
			polDemJodMap->SetAt((void *)prpJod->Ujob,prpJod);
		}
		else
		{
			polDemJodMap = new CMapPtrToPtr;
			polDemJodMap->SetAt((void *)prpJod->Ujob,prpJod);
			omDemuMap.SetAt((void *)prpJod->Udem,polDemJodMap);
		}
	}
}

void CedaJodData::RemoveJodFromJodDemMap(JODDATA *prpJod)
{
	if(prpJod != NULL)
	{
		CMapPtrToPtr *polJodDemMap;
		if (omJobuMap.Lookup((void *) prpJod->Ujob, (void *&) polJodDemMap) == TRUE)
		{
			polJodDemMap->RemoveKey((void *)prpJod->Udem);
		}
	}
}

void CedaJodData::RemoveJodFromDemJodMap(JODDATA *prpJod)
{
	if(prpJod != NULL)
	{
		CMapPtrToPtr *polDemJodMap;
		if (omDemuMap.Lookup((void *) prpJod->Udem, (void *&) polDemJodMap) == TRUE)
		{
			polDemJodMap->RemoveKey((void *)prpJod->Ujob);
		}
	}
}

JODDATA* CedaJodData::AddJod(long lpJobUrno, long lpDemandUrno)
{
	if(bmUseJodtab)
	{
		JODDATA olJod;
		olJod.Urno = ogBasicData.GetNextUrno();
		olJod.Ujob = lpJobUrno;
		olJod.Udem = lpDemandUrno;
		return AddJod(&olJod);
	}
	return NULL;
}

JODDATA* CedaJodData::AddJod(JODDATA *prpJod)
{
	if(bmUseJodtab)
	{
		JODDATA *prlJod = AllocNewJod(prpJod);
		AddJodInternal(prlJod);
		DbInsertJod(prlJod);
		ogCCSDdx.DataChanged(this,JOD_NEW,prlJod);
		return prlJod;
	}
	return NULL;
}

JODDATA* CedaJodData::ChangeDemu(JODDATA *prpJod, long lpNewDemu)
{
	ogCCSDdx.DataChanged(this,JOD_DELETE,prpJod);

	RemoveJodFromJodDemMap(prpJod);
	RemoveJodFromDemJodMap(prpJod);

	JODDATA rlOldJod = *prpJod;
	prpJod->Udem = lpNewDemu;

	AddJodToJodDemMap(prpJod);
	AddJodToDemJodMap(prpJod);

	DbUpdateJod(prpJod, &rlOldJod);
	ogCCSDdx.DataChanged(this,JOD_NEW,prpJod);

    return prpJod;
}

bool CedaJodData::GetJodsByDemand(CCSPtrArray<JODDATA> &ropJods,long lpDemu)
{
	JODDATA  *prlJod;
    ropJods.RemoveAll();
	CMapPtrToPtr *polDemJodMap;

	if (omDemuMap.Lookup((void *)lpDemu,(void *& )polDemJodMap) == TRUE)
	{
		POSITION rlPos;

		for ( rlPos =  polDemJodMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polDemJodMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJod);
			ropJods.Add(prlJod);
		}
	}

    return true;
}

bool CedaJodData::GetJobsByDemand(CCSPtrArray<JOBDATA> &ropJobs,long lpDemu, bool bpNoDuplicates /* = false */)
{
	if(bmUseJodtab)
	{
		JODDATA  *prlJod;
		ropJobs.RemoveAll();
		CMapPtrToPtr *polDemJodMap;
		CString olJobUrnoList;

		if (omDemuMap.Lookup((void *)lpDemu,(void *& )polDemJodMap) == TRUE)
		{
			for(POSITION rlPos =  polDemJodMap->GetStartPosition(); rlPos != NULL; )
			{
				long llUrno;
				polDemJodMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJod);
				JOBDATA *prlJob = ogJobData.GetJobByUrno(prlJod->Ujob);
				if (prlJob != NULL)
				{
//					bool blAdd = true;
					if(!bpNoDuplicates || strcmp(prlJob->Jtco,JOBRESTRICTEDFLIGHT))
					{
						ropJobs.Add(prlJob);
/*						CString olJobUrno;
						olJobUrno.Format("|%ld|", prlJob->Urno);
						if(olJobUrnoList.Find(olJobUrno) != -1)
						{
							olJobUrnoList += olJobUrno;
						}
						else
						{
							blAdd = false;
						}
						*/
					}
//					if(blAdd)
//						ropJobs.Add(prlJob);
				}
			}
		}
	}
	else
	{
		ogJobData.GetJobsByUdem(ropJobs, lpDemu);
	}

    return (ropJobs.GetSize() > 0) ? true : false;
}

bool CedaJodData::GetDemandsByJob(CCSPtrArray <DEMANDDATA> &ropDemands, long lpJobu)
{
// this function has been replaced by GetDemandForJob() as there can only be one demand per job
	ropDemands.RemoveAll();
	CMapPtrToPtr *polJodDemMap;

	if (omJobuMap.Lookup((void *)lpJobu,(void *& )polJodDemMap) == TRUE)
	{
		for (POSITION rlPos =  polJodDemMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			JODDATA *prlJod = NULL;
			polJodDemMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJod);
			DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prlJod->Udem);
			if(prlDemand != NULL)
			{
				ropDemands.Add(prlDemand);
			}
		}
	}

    return true;
// this function has been replaced by GetDemandForJob() as there can only be one demand per job
}

long CedaJodData::GetDemandUrnoForJob(JOBDATA *prpJob)
{
	long llUdem = 0L;
	if(prpJob != NULL)
	{
		if(bmUseJodtab)
		{
			CMapPtrToPtr *polJodDemMap;
			if (omJobuMap.Lookup((void *)prpJob->Urno,(void *& )polJodDemMap) == TRUE)
			{
				for (POSITION rlPos =  polJodDemMap->GetStartPosition(); rlPos != NULL; )
				{
					long llUrno;
					JODDATA *prlJod = NULL;
					polJodDemMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJod);
					llUdem = prlJod->Udem;
					break;
				}
			}
		}
		else
		{
			llUdem = prpJob->Udem;
		}
	}
	return llUdem;
}

DEMANDDATA *CedaJodData::GetDemandForJob(long lpJobUrno)
{
	return GetDemandForJob(ogJobData.GetJobByUrno(lpJobUrno));
}

DEMANDDATA *CedaJodData::GetDemandForJob(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		if(bmUseJodtab)
		{
			CCSPtrArray <DEMANDDATA> olDemands;
			GetDemandsByJob(olDemands, prpJob->Urno);
			if(olDemands.GetSize() > 0)
			{
				return &olDemands[0];
			}
		}
		else
		{
			return ogDemandData.GetDemandByUrno(prpJob->Udem);
		}
	}
	return NULL;
}

// return true if the demand referenced by lpDemu (DEM.URNO) has jobs assigned
bool CedaJodData::DemandHasJobs(long lpDemu)
{
	if(bmUseJodtab)
	{
		CMapPtrToPtr *polDemJodMap;
		if(omDemuMap.Lookup((void *)lpDemu,(void *& )polDemJodMap))
		{
			for(POSITION rlPos =  polDemJodMap->GetStartPosition(); rlPos != NULL; )
			{
				long llUrno; JODDATA *prlJod;
				polDemJodMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJod);
				if(ogJobData.GetJobByUrno(prlJod->Ujob) != NULL)
				{
					return true;
				}
			}
		}
	}
	else
	{
		return ogJobData.DemandHasJobs(lpDemu);
	}

	return false;
}

// return true if the demand referenced by lpDemu (DEM.URNO) has jobs assigned
bool CedaJodData::JobHasDemands(long lpJobu)
{
	return JobHasDemands(ogJobData.GetJobByUrno(lpJobu));
}

bool CedaJodData::JobHasDemands(JOBDATA *prpJob)
{
	bool blJobHasDemands = false;
	if(prpJob != NULL)
	{
		if(bmUseJodtab)
		{
			CMapPtrToPtr *polJodDemMap;

			if(omJobuMap.Lookup((void *)prpJob->Urno,(void *& )polJodDemMap) == TRUE)
			{
				long llUrno;
				JODDATA *prlJod = NULL;
				for (POSITION rlPos =  polJodDemMap->GetStartPosition(); rlPos != NULL; )
				{
					polJodDemMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJod);
					if(ogDemandData.GetDemandByUrno(prlJod->Udem) != NULL)
					{
						blJobHasDemands = true;
						break;
					}
				}
			}
		}
		else
		{
			blJobHasDemands = ogJobData.JobHasDemand(prpJob);
		}
	}
	return blJobHasDemands;
}


bool CedaJodData::GetJodsByJob(CCSPtrArray<JODDATA> &ropJods,long lpUrno)
{
	JODDATA  *prlJod;
    ropJods.RemoveAll();
	CMapPtrToPtr *polDemJodMap;

	if (omJobuMap.Lookup((void *)lpUrno,(void *& )polDemJodMap) == TRUE)
	{
		POSITION rlPos;

		for ( rlPos =  polDemJodMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polDemJodMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJod);
			ropJods.Add(prlJod);
		}
	}

    return true;
}

JODDATA  *CedaJodData::GetJodByUrno(long lpUrno)
{
	JODDATA  *prlJod;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlJod) == TRUE)
	{
		TRACE("GetJodByUrno %ld \n",prlJod->Urno);
		return prlJod;
	}
	return NULL;
}

long CedaJodData::GetFirstJobUrnoByDemand(long lpDemu)
{
	long  llJobu = 0L;
	if(bmUseJodtab)
	{
		JODDATA  rlJod;
		JOBDATA *prlJob = NULL;

		CMapPtrToPtr *polDemJodMap;

		if (omDemuMap.Lookup((void *)lpDemu,(void *& )polDemJodMap) == TRUE)
		{
			POSITION rlPos;

			rlPos =  polDemJodMap->GetStartPosition();
			while ((rlPos != NULL) && (prlJob == NULL))
			{
				polDemJodMap->GetNextAssoc(rlPos,(void *& ) llJobu,(void *& )rlJod);
				prlJob = ogJobData.GetJobByUrno(llJobu);
			}
		}

		if (prlJob != NULL)
		{
			llJobu = prlJob->Urno;
		}
	}
	else
	{
		llJobu = ogJobData.GetFirstJobUrnoByDemand(lpDemu);
	}

	return llJobu;
}

bool CedaJodData::DeleteJod(long lpJodUrno)
{
	bool blRc = false;

	JODDATA *prlJod = GetJodByUrno(lpJodUrno);
	if(prlJod != NULL)
	{
		JODDATA rlOldJod = *prlJod;
		DeleteJodInternal(lpJodUrno);
		if((blRc = DbDeleteJod(lpJodUrno)) == true)
		{
			ogCCSDdx.DataChanged(this,JOD_DELETE,&rlOldJod);
		}
	}
	return blRc;
}

bool CedaJodData::DeleteJodByJobu(long lpJobUrno)
{
	bool blRc = false;
	


	CCSPtrArray <JODDATA> olJods;
//	GetJodsByJob(olJods, lpJobUrno);
// load all jods for this job, 

	char pclCom[10] = "RT";
	sprintf(cgSelection, "WHERE UJOB = '%ld'", lpJobUrno);
	if (CedaAction2(pclCom, cgSelection) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaJodData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,cgSelection);
		return false;
	}
	
	int ilCount = 0;
	blRc = true;
	for(int ilLc = 0; blRc; ilLc++)
	{
		JODDATA *prlJod = new JODDATA;
		if ((blRc = GetBufferRecord2(&omRecInfo,ilLc,prlJod)) == RCSuccess)
		{
		//	ConvertDatesToLocal(prlDemand);
			olJods.Add(prlJod);
		}
	}

	int ilNumJods = olJods.GetSize();
	for(int ilJ = 0; ilJ < ilNumJods; ilJ++)
	{
		JODDATA *prlJod = &olJods[ilJ];
		if(prlJod != NULL)
		{
			JODDATA rlOldJod = *prlJod;
			DeleteJodInternal(rlOldJod.Urno);
			if((blRc = DbDeleteJod(rlOldJod.Urno)) == true)
			{
				ogCCSDdx.DataChanged(this,JOD_DELETE,&rlOldJod);
			}
		}
	}
	return blRc;
}

void CedaJodData::DeleteJodInternal(long lpJodUrno)
{
	JODDATA *prlJod = GetJodByUrno(lpJodUrno);
	if (prlJod != NULL)
	{
		omUrnoMap.RemoveKey((void *)lpJodUrno);
		RemoveJodFromJodDemMap(prlJod);
		RemoveJodFromDemJodMap(prlJod);
		for(int ilJ = (omData.GetSize() - 1); ilJ >= 0; ilJ--)
		{
			if(omData[ilJ].Urno == lpJodUrno)
			{
				omData.DeleteAt(ilJ);
			}
		}
	}
}

void CedaJodData::PrepareDataForWrite(JODDATA *prpJod)
{
	CTime olCurrTime = ogBasicData.GetTime();
	prpJod->Lstu = olCurrTime;
	strcpy(prpJod->Useu,ogUsername);
	if(prpJod->Cdat == TIMENULL || strlen(prpJod->Usec) <= 0)
	{
		prpJod->Cdat = olCurrTime;
		strcpy(prpJod->Usec,ogUsername);
	}
}

CString CedaJodData::GetTableName(void)
{
	return CString(pcmTableName);
}

bool CedaJodData::DbInsertJod(JODDATA *prpJod)
{
	bool blRc = false;

	if(prpJod != NULL)
	{
		if(bmIsOffline)
		{
			SetReleaseButton();
			blRc = true;
		}
		else
		{
			blRc = DbInsertJod2(prpJod);
		}
	}

	return blRc;
}

bool CedaJodData::DbInsertJod2(JODDATA *prpJod)
{
	bool blRc = false;

	// if transaction, then release all job changes via release handler in a block
	if(prpJod != NULL && (blRc = AddToIrtTransaction(prpJod)) == false)
	{
		char pclSelection[124] = "";
		char pclData[2000] = "";
		char pclCmd[10] = "IRT";
		CString olData;
		
		PrepareDataForWrite(prpJod);
		MakeCedaData(&omRecInfo,olData,prpJod);
		strcpy(pclData, olData);
		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);

		if(!(blRc = CedaAction(pclCmd,"","",pclData)))
		{
			ogBasicData.LogCedaError("CedaJodData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
	}

	return blRc;
}

bool CedaJodData::DbUpdateJod(JODDATA *prpJod, JODDATA *prpOldJod /*=NULL*/)
{
	bool blRc = false;

	if(prpJod != NULL)
	{
		if(bmIsOffline)
		{
			SetReleaseButton();
			blRc = true;
		}
		else
		{
			blRc = DbUpdateJod2(prpJod, prpOldJod);
		}
	}

	return blRc;
}

bool CedaJodData::DbUpdateJod2(JODDATA *prpJod, JODDATA *prpOldJod /*=NULL*/)
{
	bool blRc = false;

	// if transaction, then release all job changes via release handler in a block
	if(prpJod != NULL && (blRc = AddToUrtTransaction(prpJod)) == false)
	{
		char pclSelection[124] = "";
		char pclData[2000] = "";
		char pclCmd[10] = "URT";
		char pclFieldList[1000];

		PrepareDataForWrite(prpJod);
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpJod->Urno);

		if(prpOldJod != NULL && GetChangedFields(prpJod, prpOldJod, pclFieldList, pclData))
		{
			// we have the old record so update JODTAB only with the fields that have been changed
			ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pclFieldList, pclData);
			blRc = CedaAction(pclCmd, pcmTableName, pclFieldList, pclSelection,"", pclData);
		}
		else
		{
			// update all fields in JODTAB for this record
			CString olListOfData;
			MakeCedaData(&omRecInfo,olListOfData,prpJod);
			strcpy(pclData,olListOfData);
			ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);
			blRc = CedaAction(pclCmd,pclSelection,"",pclData);
		}
	
		if(!blRc)
		{
			ogBasicData.LogCedaError("CedaJodData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
	}

	return blRc;
}

bool CedaJodData::DbDeleteJod(long lpJodUrno)
{
	bool blRc = false;

	if(bmIsOffline)
	{
		// only add this jod to the deletion list if it wasn't inserted offline ie. the online jod exists
		if(GetOnlineJodByUrno(lpJodUrno) != NULL)
		{
			omDeletedOfflineJods.SetAt((void *) lpJodUrno, NULL);
		}
		SetReleaseButton();
		blRc = true;
	}
	else
	{
		blRc = DbDeleteJod2(lpJodUrno);
	}

	return blRc;
}

bool CedaJodData::DbDeleteJod2(long lpJodUrno)
{
	bool blRc = false;

	// if transaction, then release all job changes via release handler in a block
	if((blRc = AddToDrtTransaction(lpJodUrno)) == false)
	{
		char pclSelection[124] = "";
		char pclData[2000] = "";
		char pclCmd[10] = "DRT";

		sprintf(pclSelection,"WHERE URNO = '%ld'",lpJodUrno);
		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);

		if(!(blRc = CedaAction(pclCmd,pclSelection)))
		{
			ogBasicData.LogCedaError("CedaJodData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
	}

	return blRc;
}

// compare prpJod with prpOldJod and return only the fields (pcpFieldList) and data (pcpData) which have changed
bool CedaJodData::GetChangedFields(JODDATA *prpJod, JODDATA *prpOldJod, char *pcpFieldList, char *pcpData)
{
	bool blChangesFound = false;

	memset(pcpFieldList,0,sizeof(pcpFieldList));
	memset(pcpData,0,sizeof(pcpData));

	if(prpJod != NULL && prpOldJod != NULL)
	{
		JODDATA rlJod;
		CString olData;

		memcpy(&rlJod,prpJod,sizeof(JODDATA));
		MakeCedaData(&omRecInfo,olData,&rlJod);
		CStringArray olNewFieldArray, olNewDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olNewFieldArray);
		ogBasicData.ExtractItemList(olData, &olNewDataArray);
		int ilNumNewFields = olNewFieldArray.GetSize();
		int ilNumNewData = olNewDataArray.GetSize();

		memcpy(&rlJod,prpOldJod,sizeof(JODDATA));
		MakeCedaData(&omRecInfo,olData,&rlJod);
		CStringArray olOldDataArray;
		ogBasicData.ExtractItemList(olData, &olOldDataArray);
		int ilNumOldData = olOldDataArray.GetSize();

		bool blListsAreEmpty = true;
		if(ilNumNewFields == ilNumNewData && ilNumNewData == ilNumOldData)
		{
			for(int ilD = 0; ilD < ilNumNewData; ilD++)
			{
				// check if the data item has changed - always write LSTU and USEU whether they change or not
				if(olNewDataArray[ilD] != olOldDataArray[ilD] || olNewFieldArray[ilD] == "LSTU" || olNewFieldArray[ilD] == "USEU")
				{
					CString olOldData = olOldDataArray[ilD];
					CString olNewData = olNewDataArray[ilD];
					if(blListsAreEmpty)
					{
						strcpy(pcpFieldList,olNewFieldArray[ilD]);
						strcpy(pcpData,olNewDataArray[ilD]);
						blListsAreEmpty = false;
					}
					else
					{
						CString olTmp;
						olTmp.Format(",%s",olNewFieldArray[ilD]);
						strcat(pcpFieldList,olTmp);
						olTmp.Format(",%s",olNewDataArray[ilD]);
						strcat(pcpData,olTmp);
					}

					if(olNewDataArray[ilD] != olOldDataArray[ilD] && 
						olNewFieldArray[ilD] != "CDAT" && olNewFieldArray[ilD] != "USEC" &&
						olNewFieldArray[ilD] != "LSTU" && olNewFieldArray[ilD] != "USEU")
					{
						blChangesFound = true;
					}
				}
			}
		}
	}

	return blChangesFound;
}

// returns true if the jod has been deleted offline
// this function is required in the following situation
// 1. the jod is deleted offline
// 2. another OpssPm working online updates the jod
// 3. update jod broadcast is received by the offline OpssPm
// 4. the updated jod should not be re-inserted into the offline data, but the online data should be
bool CedaJodData::WasDeletedOffline(long lpJodUrno)
{
	if(bmUseJodtab)
	{
		JODDATA *prlJod = NULL;
		return omDeletedOfflineJods.Lookup((void *)lpJodUrno,(void *& )prlJod) ? true : false;
	}

	return false;
}

JODDATA *CedaJodData::GetOnlineJodByUrno(long lpJodUrno)
{
	JODDATA *prlJod = NULL;
	if(bmUseJodtab)
	{
		omOnlineUrnoMap.Lookup((void *)lpJodUrno,(void *& )prlJod);
	}
	return prlJod;
}

void CedaJodData::DeleteFromOnlineData(long lpJodUrno)
{
	if(bmUseJodtab && bmIsOffline)
	{
		if(GetOnlineJodByUrno(lpJodUrno) != NULL)
		{
			for(int ilJ = (omOnlineData.GetSize() - 1); ilJ >= 0; ilJ--)
			{
				if(omOnlineData[ilJ].Urno == lpJodUrno)
				{
					omOnlineData.DeleteAt(ilJ);
				}
			}
		}

		// this jod was deleted offline and has now been deleted in another instance of
		// opsspm online, so it can be deleted from the list of deleted offline jods
		JODDATA *prlJod = NULL;
		if(omDeletedOfflineJods.Lookup((void *)lpJodUrno,(void *& )prlJod))
		{
			omDeletedOfflineJods.RemoveKey((void *)lpJodUrno);
		}
	}
}

void CedaJodData::UpdateOnlineData(JODDATA *prpJod)
{
	if(bmUseJodtab && prpJod != NULL && bmIsOffline)
	{
		JODDATA *prlJod = NULL;
		if(omOnlineUrnoMap.Lookup((void *)prpJod->Urno,(void *& )prlJod))
		{
			// update
			*prlJod = *prpJod;
		}
		else
		{
			// insert
			JODDATA *prlJod = AllocNewJod(prpJod);
			omOnlineData.Add(prlJod);
			omOnlineUrnoMap.SetAt((void *)prlJod->Urno,prlJod);
		}
	}
}

// set offline flag and copy all jods into a backup array
void CedaJodData::SetOffline()
{
	if(bmUseJodtab)
	{
		bmIsOffline = true;
		CreateBackupData();
	}
}

void CedaJodData::CreateBackupData()
{
	if(bmUseJodtab)
	{
		omDeletedOfflineJods.RemoveAll();
		omOnlineUrnoMap.RemoveAll();
		omOnlineData.DeleteAll();
		bmNoOfflineChangesYet = true;

		int ilNumJods = omData.GetSize();
		for(int ilJ = 0; ilJ < ilNumJods; ilJ++)
		{
			// will insert all jods into omOnlineData
			UpdateOnlineData(&omData[ilJ]);
		}
	}
}

void CedaJodData::SetOnline(bool bpRelease)
{
	if(bmUseJodtab)
	{
		bmIsOffline = false;

		if(bpRelease)
		{
			ReleaseJodsChangedOffline();
		}
		else
		{
			RollbackJodsChangedOffline();
		}

		omDeletedOfflineJods.RemoveAll();
		omOnlineUrnoMap.RemoveAll();
		omOnlineData.DeleteAll();
	}
}

void CedaJodData::RollbackJodsChangedOffline(void)
{
	if(bmUseJodtab)
	{
		// delete offline data
		ClearAll();

		// copy the online data back to main data
		int ilNumJods = omOnlineData.GetSize();
		for(int ilJ = 0; ilJ < ilNumJods; ilJ++)
		{
			JODDATA *prlJod = AllocNewJod(&omOnlineData[ilJ]);
			AddJodInternal(prlJod);
		}
	}
}

void CedaJodData::ReleaseJodsChangedOffline(void)
{
	if(bmUseJodtab)
	{
		JODDATA *prlOfflineJod = NULL, *prlOnlineJod = NULL;
		void *pvlDummy = NULL;
		long llUrno = 0L;
		CString olNewLine("\n"), olComma(","), olUrno;
		int ilDrtCount = 0, ilIrtCount = 0, ilUrtCount = 0;

		int ilFields = ogBasicData.GetItemCount(CString(pcmFieldList)); 
		CString olIrt; olIrt.Format("*CMD*,%s,IRT,%d,%s\n", pcmTableName, ilFields, CString(pcmFieldList));
		CString olUrt; olUrt.Format("*CMD*,%s,URT,%d,%s,[URNO=:VURNO]\n", pcmTableName, ilFields, CString(pcmFieldList));
		CString olDrt; olDrt.Format("*CMD*,%s,DRT,-1,,[URNO=:VURNO]\n", pcmTableName);

		// release deleted jobs
		for(POSITION rlPos = omDeletedOfflineJods.GetStartPosition(); rlPos != NULL; )
		{
			omDeletedOfflineJods.GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )pvlDummy);
			olUrno.Format("%ld", llUrno);
			olDrt += olUrno + olNewLine;
			ilDrtCount++;
		}
		if(ilDrtCount > 0)
		{
			Release(olDrt);
		}

		// loop through normal data and check for inserts and updates
		CString olData;
		int ilNumJods = omData.GetSize();
		for(int ilJ = 0; ilJ < ilNumJods; ilJ++)
		{
			prlOfflineJod = &omData[ilJ];
			if((prlOnlineJod = GetOnlineJodByUrno(prlOfflineJod->Urno)) != NULL)
			{
				if(*prlOfflineJod != *prlOnlineJod)
				{
					olUrno.Format("%ld", prlOfflineJod->Urno);
					PrepareDataForWrite(prlOfflineJod);
					MakeCedaData(&omRecInfo,olData,prlOfflineJod);
					olUrt += olData + olComma + olUrno + olNewLine;
					ilUrtCount++;
				}
			}
			else
			{
				PrepareDataForWrite(prlOfflineJod);
				MakeCedaData(&omRecInfo,olData,prlOfflineJod);
				olIrt += olData + olNewLine;
				ilIrtCount++;
			}
		}

		if(ilUrtCount > 0)
		{
			Release(olUrt);
		}
		
		if(ilIrtCount > 0)
		{
			Release(olIrt);
		}

		// recreate the backup data - release button was pressed so the backup
		// data and the offline data should now be the same
		CreateBackupData();
	}
}

bool CedaJodData::Release(CString opReleaseString)
{
	bool blRc = false;

	if(!bmUseJodtab)
	{
		blRc = true;
	}
	else
	{
		int ilLen = opReleaseString.GetLength();
		if(ilLen > 0)
		{
			char *pclDataArea = (char *) malloc(ilLen + 1);
			strcpy(pclDataArea, opReleaseString);
			if((blRc = CedaAction("REL","QUICK","",pclDataArea)) == false)
			{
				ogBasicData.LogCedaError("CedaJodData Error",omLastErrorMessage,"REL",pcmFieldList,pcmTableName,"Release()");
			}
			free((char *)pclDataArea);
		}
	}
	return blRc;
}

bool CedaJodData::JodsChangedOffline(void)
{
	if(bmUseJodtab)
	{
		if(omDeletedOfflineJods.GetCount())
		{
			return true;
		}

		int ilNumJods = omData.GetSize();
		for(int ilJ = 0; ilJ < ilNumJods; ilJ++)
		{
			if(JodChangedOffline(&omData[ilJ]))
				return true;
		}
	}
	return false;
}

void CedaJodData::SetReleaseButton(void)
{
	if(bmUseJodtab && bmNoOfflineChangesYet)
	{
		if(JodsChangedOffline())
		{
			ogCCSDdx.DataChanged((void *)this, OFFLINE_CHANGES, NULL);
			bmNoOfflineChangesYet = false;
		}
	}
}

bool CedaJodData::JodChangedOffline(JODDATA *prpOfflineJod)
{
	if(bmUseJodtab && prpOfflineJod != NULL)
	{
		JODDATA *prlOnlineJod = GetOnlineJodByUrno(prpOfflineJod->Urno);
		if(prlOnlineJod == NULL || *prpOfflineJod != *prlOnlineJod)
		{
			return true;
		}
	}

	return false;
}

bool CedaJodData::JodChangedOffline(long lpJobUrno)
{
	if(bmUseJodtab)
	{
		CCSPtrArray <JODDATA> olJods;
		GetJodsByJob(olJods, lpJobUrno);
		int ilNumJods = olJods.GetSize();
		for(int ilJ = 0; ilJ < ilNumJods; ilJ++)
		{
			if(JodChangedOffline(&olJods[ilJ]))
				return true;
		}
	}
	return false;
}

void CedaJodData::GetJodsChangedOffline(CUIntArray &ropJodUrnos)
{
	if(bmUseJodtab)
	{
		int ilNumJods = omData.GetSize();
		for(int ilJ = 0; ilJ < ilNumJods; ilJ++)
		{
			if(JodChangedOffline(&omData[ilJ]))
			{
				ropJodUrnos.Add(omData[ilJ].Urno);
			}
		}
	}
}

// the transaction handling allows you send a block of changes to the release
// handler, to use it:
// 1) Call StartTransaction()
// 2) Do inserts/updates/deletes as usual
// 3) Call CommitTransaction() to release all changes
//
void CedaJodData::StartTransaction(void)
{
	bmTransactionActivated = true;
}

void CedaJodData::CommitTransaction(void)
{
	if(bmUseJodtab)
	{
		JODDATA *prlJod = NULL;
		void *pvlDummy = NULL;
		long llUrno = 0L;
		CString olNewLine("\n"), olComma(","), olUrno;
		int ilDrtCount = 0, ilIrtCount = 0, ilUrtCount = 0;
		POSITION rlPos;
		CString olData;

		int ilFields = ogBasicData.GetItemCount(CString(pcmFieldList)); 
		CString olIrt; olIrt.Format("*CMD*,%s,IRT,%d,%s\n", pcmTableName, ilFields, CString(pcmFieldList));
		CString olUrt; olUrt.Format("*CMD*,%s,URT,%d,%s,[URNO=:VURNO]\n", pcmTableName, ilFields, CString(pcmFieldList));
		CString olDrt; olDrt.Format("*CMD*,%s,DRT,-1,,[URNO=:VURNO]\n", pcmTableName);

		// release deleted jods
		for(rlPos = omDrtJods.GetStartPosition(); rlPos != NULL; )
		{
			omDrtJods.GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )pvlDummy);
			olUrno.Format("%ld", llUrno);
			olDrt += olUrno + olNewLine;
			ilDrtCount++;
		}
		if(ilDrtCount > 0)
		{
			Release(olDrt);
		}

		for(rlPos = omUrtJods.GetStartPosition(); rlPos != NULL; )
		{
			omUrtJods.GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJod);
			olUrno.Format("%ld", prlJod->Urno);
			PrepareDataForWrite(prlJod);
			MakeCedaData(&omRecInfo,olData,prlJod);
			olUrt += olData + olComma + olUrno + olNewLine;
			ilUrtCount++;
		}
		if(ilUrtCount > 0)
		{
			Release(olUrt);
		}

		for(rlPos = omIrtJods.GetStartPosition(); rlPos != NULL; )
		{
			omIrtJods.GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJod);
			PrepareDataForWrite(prlJod);
			MakeCedaData(&omRecInfo,olData,prlJod);
			olIrt += olData + olNewLine;
			ilIrtCount++;
		}
		if(ilIrtCount > 0)
		{
			Release(olIrt);
		}
	}
	bmTransactionActivated = false;
	omDrtJods.RemoveAll();
	omIrtJods.RemoveAll();
	omUrtJods.RemoveAll();
}

bool CedaJodData::AddToIrtTransaction(JODDATA *prpJod)
{
	bool blAdded = false;
	if(bmTransactionActivated && prpJod != NULL)
	{
		omIrtJods.SetAt((void *) prpJod->Urno, prpJod);
		blAdded = true;
	}

	return blAdded;
}

bool CedaJodData::AddToUrtTransaction(JODDATA *prpJod)
{
	bool blAdded = false;
	if(bmTransactionActivated && prpJod != NULL)
	{
		omUrtJods.SetAt((void *) prpJod->Urno, prpJod);
		blAdded = true;
	}

	return blAdded;
}

bool CedaJodData::AddToDrtTransaction(long lpJodUrno)
{
	bool blAdded = false;
	if(bmTransactionActivated)
	{
		omDrtJods.SetAt((void *) lpJodUrno, NULL);
		blAdded = true;
	}

	return blAdded;
}

void CedaJodData::CheckForMissingFlightJods(CDWordArray &ropJobUrnos)
{
	CString olData, olTmpStr;
	int ilNumFlightUrnos = ropJobUrnos.GetSize();
	for(int i = 0; i < ilNumFlightUrnos; i++)
	{
		if(!olData.IsEmpty())
			olData += ",";
		olTmpStr.Format("'%ld'", (long) ropJobUrnos[i]);
		olData += olTmpStr;

		if((i != 0 && ((i+1) % 1000) == 0) || i == (ilNumFlightUrnos-1)) // max 1000 URNOs in a WHERE clause
		{
			int ilDataLen = olData.GetLength();
			char *pclUrnoList = new char[ilDataLen + 50];
			char *pclSelection = new char[ilDataLen + 50];
			memset(pclUrnoList, 0, sizeof(pclUrnoList));
			memset(pclSelection, 0, sizeof(pclSelection));

			strcpy(pclUrnoList, olData);

			sprintf(pclSelection, "WHERE UJOB IN (%s)", pclUrnoList);
			ReadJods(pclSelection, false);
		
			delete [] pclUrnoList;
			delete [] pclSelection;
			olData.Empty();
		}
	}
}

void CedaJodData::ReadJodsForDelegatedFlight()
{
	if(bmUseJodtab)
	{
		CString olJobUrnos;
		int n = ogJobData.omData.GetSize();
		for(int i = 0; i < n; i++)
		{
			JOBDATA *prlJob = &ogJobData.omData[i];
			if(CString(prlJob->Jtco) == JOBDELEGATEDFLIGHT)
			{
				if(!olJobUrnos.IsEmpty())
					olJobUrnos += ",";
				CString olJobUrno;
				olJobUrno.Format("'%ld'", prlJob->Urno);
				olJobUrnos += olJobUrno;
			}
		}

		if(!olJobUrnos.IsEmpty())
		{
			sprintf(cgSelection,"WHERE UJOB IN (%s)", olJobUrnos);
			ReadJods(cgSelection);
		}
	}
}

void CedaJodData::LoadJodsForExtraDemands(CCSPtrArray <DEMANDDATA> &ropDemands)
{
	if(bmUseJodtab)
	{
		CString olDemandUrnos;
		int n = ropDemands.GetSize();
		for(int i = 0; i < n; i++)
		{
			DEMANDDATA *prlDemand = &ropDemands[i];
			if(!olDemandUrnos.IsEmpty())
				olDemandUrnos += ",";
			CString olDemandUrno;
			olDemandUrno.Format("'%ld'", prlDemand->Urno);
			olDemandUrnos += olDemandUrno;
		}

		if(!olDemandUrnos.IsEmpty())
		{
			sprintf(cgSelection,"WHERE UDEM IN (%s)", olDemandUrnos);
			ReadJods(cgSelection);
		}
	}
}
