// Class for Dpxes
#ifndef _CEDADPXDATA_H_
#define _CEDADPXDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CedaDemandData.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct DpxDataStruct
{
	long Urno;
	char Prid[52];	// PRM request ID
	char Name[34];	// Name of Passenger
	char Natl[18];		// Nationality
	char Lang[26];		// Language		
	char Gend[4];		// Gender M = Male F = Female
	char Mail[34];	//Email-Address
	char Owhc[4];  // Flag if wheelchair needed N=NO, O=Own, Y=require Wheelchair

	long Flnu;		// Flight Urno
	long Tkey;
	CTime Fdat;     // Flight date. TIFA for arrivals, TIFD for departures
	long Tflu;		// Transit Flight Urno
	CTime Abdt;		// Assistance Bookiing Date
	char Absr[22];
	char Aloc[22];	     // Arrival location ( Hall / Train / Bus / Taxi or Parking )
	CTime Sati;		// Scheduled arrival time of DPX
	CTime Aati;      // Actual arrival time of DPX
	char Prmt[12];	   // Type of PRMssssss
	long Tlxu;		// Telex Urno
	char Seat[11];		// Seat Number
    char Usec[34];	// Creator
    CTime Cdat;		// Creation Date
    char  Useu[34];	// Updater
    CTime Lstu;		// Update Date
	CTime Clos;     // PRM closing time
	char Filt[12];   // Filter for Handling Agent
	char Rema[256+2]; // Remarks requested by SATS

	char FlnuFlno[12];
	char FlnuAlc[6];
	char FlnuNumber[8];
	char FlnuSuffix[2];
	char FlnuAdid;
	char FlnuPrms[1+2]; // PRM Flight Status requested by SATS
	char FlnuPrmu[32+2]; // PRM Flight Status update user requested by SATS

	char TfluFlno[12];
	char TfluAlc[6];
	char TfluNumber[8];
	char TfluSuffix[2];
	char TfluAdid;
	char TfluPrms[1+2]; // PRM Flight Status requested by SATS
	char TfluPrmu[32+2]; // PRM Flight Status update user requested by SATS

	char FCsgn[9]; //Flight Callsign
	char TCsgn[9]; //Transit Flight Callsign

	// non DB data
	enum State
	{
		NEW,
		CHANGED,
		DELETED,
		UNCHANGED
	};

	State emState;	


	DpxDataStruct(void)
	{
		Urno	= 0L;
		memset(Prid,0,sizeof(Prid));
		memset(Name,0,sizeof(Name));
		memset(Natl,0,sizeof(Natl));
		memset(Lang,0,sizeof(Lang));
		memset(Usec,0,sizeof(Usec));
		memset(Gend,0,sizeof(Gend));
		memset(Mail,0,sizeof(Mail));
		memset(Owhc ,0,sizeof(Owhc));
		Flnu	= 0L;
		Tflu	= 0L;
		Tlxu	= 0L;
		Abdt    = TIMENULL;
		memset(Absr,0,sizeof(Absr));
		memset(Aloc,0,sizeof(Aloc));
		Fdat	= TIMENULL;
		Sati	= TIMENULL;
		Aati	= TIMENULL;
		memset(Prmt,0,sizeof(Prmt));
		memset(Seat,0,sizeof(Seat));
		memset(Usec,0,sizeof(Usec));
		Cdat	= TIMENULL;
		memset(Useu,0,sizeof(Useu));
		Lstu	= TIMENULL;
		Clos	= TIMENULL;
		emState = NEW;   
		memset(Filt,0,sizeof(Filt));
		memset(Rema,0,sizeof(Rema));
		
		
		memset( FlnuFlno,0,sizeof(FlnuFlno));
		memset( FlnuAlc,0,sizeof(FlnuAlc));
		memset( FlnuNumber,0,sizeof(FlnuNumber));
		memset( FlnuSuffix,0,sizeof(FlnuSuffix));
		memset( FlnuPrms,0,sizeof(FlnuPrms));
		memset( FlnuPrmu,0,sizeof(FlnuPrmu));

		memset( TfluFlno,0,sizeof(TfluFlno));
		memset( TfluAlc,0,sizeof(TfluAlc));
		memset( TfluNumber,0,sizeof(TfluNumber));
		memset( TfluSuffix,0,sizeof(TfluSuffix));
		memset( TfluPrms,0,sizeof(TfluPrms));
		memset( TfluPrmu,0,sizeof(TfluPrmu));

		memset( FCsgn,0,sizeof(FCsgn));
		memset( TCsgn,0,sizeof(TCsgn));

	}

	// lli: create operaors for deep copy and comparison
	DpxDataStruct& DpxDataStruct::operator=(const DpxDataStruct& rhs)
	{
		if (&rhs != this)
		{
			Urno = rhs.Urno;
			strcpy(Prid,rhs.Prid);
			strcpy(Name,rhs.Name);
			strcpy(Natl,rhs.Natl);
			strcpy(Lang,rhs.Lang);
			strcpy(Gend,rhs.Gend);
			strcpy(Mail,rhs.Mail);
			strcpy(Owhc,rhs.Owhc);
			Flnu = rhs.Flnu;
			Tkey = rhs.Tkey;
			Fdat = rhs.Fdat;
			Tflu = rhs.Tflu;
			Abdt = rhs.Abdt;
			strcpy(Absr,rhs.Absr);
			strcpy(Aloc,rhs.Aloc);
			Sati = rhs.Sati;
			Aati = rhs.Aati;
			strcpy(Prmt,rhs.Prmt);
			Tlxu = rhs.Tlxu;
			strcpy(Seat,rhs.Seat);
			strcpy(Usec,rhs.Usec);
			Cdat = rhs.Cdat;
			strcpy(Useu,rhs.Useu);
			Lstu = rhs.Lstu;
			Clos = rhs.Clos;
			strcpy(Filt,rhs.Filt);
			strcpy(Rema,rhs.Rema);
			strcpy(FlnuFlno,rhs.FlnuFlno);
			strcpy(FlnuAlc,rhs.FlnuAlc);
			strcpy(FlnuNumber,rhs.FlnuNumber);
			strcpy(FlnuSuffix,rhs.FlnuSuffix);
			FlnuAdid = rhs.FlnuAdid;
			strcpy(FlnuPrms,rhs.FlnuPrms);
			strcpy(FlnuPrmu,rhs.FlnuPrmu);
			strcpy(TfluFlno,rhs.TfluFlno);
			strcpy(TfluAlc,rhs.TfluAlc);
			strcpy(TfluNumber,rhs.TfluNumber);
			strcpy(TfluSuffix,rhs.TfluSuffix);
			TfluAdid = rhs.TfluAdid;
			strcpy(TfluPrms,rhs.TfluPrms);
			strcpy(TfluPrmu,rhs.TfluPrmu);
			emState = rhs.emState;

			strcpy(FCsgn,rhs.FCsgn);
			strcpy(TCsgn,rhs.TCsgn);

		}		
		return *this;
	}

	friend bool operator==(const DpxDataStruct& rrp1,const DpxDataStruct& rrp2)
	{
		return
			rrp1.Urno == rrp2.Urno &&
			!strcmp(rrp1.Prid,rrp2.Prid) &&
			!strcmp(rrp1.Name,rrp2.Name) &&
			!strcmp(rrp1.Natl,rrp2.Natl) &&
			!strcmp(rrp1.Lang,rrp2.Lang) &&
			!strcmp(rrp1.Gend,rrp2.Gend) &&
			!strcmp(rrp1.Mail,rrp2.Mail) &&
			!strcmp(rrp1.Owhc,rrp2.Owhc) &&
			rrp1.Flnu == rrp2.Flnu &&
			rrp1.Tkey == rrp2.Tkey &&
			rrp1.Fdat == rrp2.Fdat &&
			rrp1.Tflu == rrp2.Tflu &&
			rrp1.Abdt == rrp2.Abdt &&
			!strcmp(rrp1.Absr,rrp2.Absr) &&
			!strcmp(rrp1.Aloc,rrp2.Aloc) &&
			rrp1.Sati == rrp2.Sati &&
			rrp1.Aati == rrp2.Aati &&
			!strcmp(rrp1.Prmt,rrp2.Prmt) &&
			rrp1.Tlxu == rrp2.Tlxu &&
			!strcmp(rrp1.Seat,rrp2.Seat) &&
			!strcmp(rrp1.Usec,rrp2.Usec) &&
			rrp1.Cdat == rrp2.Cdat &&
			!strcmp(rrp1.Useu,rrp2.Useu) &&
			rrp1.Lstu == rrp2.Lstu &&
			rrp1.Clos == rrp2.Clos &&
			!strcmp(rrp1.Filt,rrp2.Filt) &&
			!strcmp(rrp1.Rema,rrp2.Rema) &&
			!strcmp(rrp1.FlnuFlno,rrp2.FlnuFlno) &&
			!strcmp(rrp1.FlnuAlc,rrp2.FlnuAlc) &&
			!strcmp(rrp1.FlnuNumber,rrp2.FlnuNumber) &&
			!strcmp(rrp1.FlnuSuffix,rrp2.FlnuSuffix) &&
			rrp1.FlnuAdid == rrp2.FlnuAdid &&
			!strcmp(rrp1.FlnuPrms,rrp2.FlnuPrms) &&
			!strcmp(rrp1.FlnuPrmu,rrp2.FlnuPrmu) &&
			!strcmp(rrp1.TfluFlno,rrp2.TfluFlno) &&
			!strcmp(rrp1.TfluAlc,rrp2.TfluAlc) &&
			!strcmp(rrp1.TfluNumber,rrp2.TfluNumber) &&
			!strcmp(rrp1.TfluSuffix,rrp2.TfluSuffix) &&
			rrp1.TfluAdid == rrp2.TfluAdid &&
			!strcmp(rrp1.TfluPrms,rrp2.TfluPrms) &&
			!strcmp(rrp1.TfluPrmu,rrp2.TfluPrmu) &&
			rrp1.emState == rrp2.emState &&
			!strcmp(rrp1.FCsgn,rrp2.FCsgn) &&
			!strcmp(rrp1.TCsgn,rrp2.TCsgn);
	}

	friend bool operator!=(const DpxDataStruct& rrp1,const DpxDataStruct& r2)
	{
		return !operator==(rrp1,r2);
	}

};

typedef struct DpxDataStruct DPXDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaDpxData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <DPXDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omPridMap;
	CMapPtrToPtr omNameMap;
	CMapPtrToPtr omUprmMap;
	CMapPtrToPtr omFlightMap;
	CString GetTableName(void);
private:
	char cgFieldList[1024];

// Operations
public:
	CedaDpxData();
	~CedaDpxData();

	void ProcessDpxBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	DPXDATA* GetDpxByUrno(long lpUrno);
	long CedaDpxData::GetDpxUrnoByName(const char *pcpName);
	void PrepareData(DPXDATA *prpDpx);
	DPXDATA* CedaDpxData::GetDpxByName(const char *pcpName);
	void CedaDpxData::UpdateMaps(DPXDATA *prpDpx, DPXDATA *prpOldDpx);
	bool DbInsert(DPXDATA *prpDpx);
	bool DbUpdate(DPXDATA *prpDpx);
	bool DbDelete(DPXDATA *prpDpx);
	CCSReturnCode CedaDpxData::ReadDpxData();
	CCSReturnCode ReadDpxData(CTime,CTime);
	void AddDpxRecord(DPXDATA *prpData,BOOL bpSendDDX = TRUE);
	BOOL DeleteDpxRecord(DPXDATA *prpOldDpx,BOOL IsFromBC = FALSE);
	void ChangeDpxRecord(DPXDATA *prpDpx,BOOL bpSendDDX);
	BOOL FindDpxRecords(const DEMANDDATA *prpDem,CCSPtrArray<DPXDATA>& ropDpxList);
	BOOL CreateDpxRecords(const DEMANDDATA *prpDem,CCSPtrArray<DPXDATA>& ropDpxList);
	BOOL IsConfirmedPSM(long lpUrno);

	BOOL GetDpxsByFlight(long lpAftu , CCSPtrArray<DPXDATA>& ropDpxList, bool bpReset = true);
	void GetDpxTypesByFlight(long lpAftu , CStringArray &ropDpxTypes, bool bpReset = true);
	CString GetCounterRangeForFlight(long lpFlightUrno);
	CString Dump(long lpUrno);

	void ProcessDpxInsert(void *vpDataPointer);
	void ProcessDpxUpdate(void *vpDataPointer);
	void ProcessDpxDelete(void *vpDataPointer);
	void ProcessFlightChange(FLIGHTDATA *prpFlight);
    DPXDATA ReadbyUrno(long urno);

private:
	void AddDpxInternal(DPXDATA *prpDpx);
	void DeleteDpxInternal(long lpUrno);
	void AddDpxToFlnuMap(DPXDATA *prpDpx);
	void DeleteDpxFromFlnuMap(long lpUaft, long lpDpxUrno);
	BOOL SaveDpxRecord(DPXDATA *prpDpx,DPXDATA *prpOldDpx = NULL);
	void ClearAll();
	void ConvertDatesToUtc(DPXDATA *prpDpx);
	void ConvertDatesToLocal(DPXDATA *prpDpx);

	BOOL IsViewLocked(); //igu
};

extern CedaDpxData ogDpxData;

#endif _CEDADPXDATA_H_
