// CCITablePropertySheet.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaCicData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <CciTablePropertySheet.h>
#include <CedaFlightData.h>
#include <conflict.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCITablePropertySheet
//

CCITablePropertySheet::CCITablePropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_STRING61592), pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_pageGroup);
	AddPage(&m_pageHall);
	AddPage(&m_pageRegion);
	AddPage(&m_pageLine);
	AddPage(&m_pageTerminal);
	AddPage(&m_pageSort);

	m_pageHall.SetSorted(false);
	m_pageHall.SetCaption(GetString(IDS_STRING61965));

	m_pageRegion.SetSorted(false);
	m_pageRegion.SetCaption(GetString(IDS_STRING61966));

	m_pageLine.SetSorted(false);
	m_pageLine.SetCaption(GetString(IDS_STRING61956));

	m_pageTerminal.SetSorted(false);
	m_pageTerminal.SetCaption(GetString(IDS_STRING61607));

	// Prepare possible values for each PropertyPage
	ogAllocData.GetGroupNamesByGroupType(ALLOCUNITTYPE_CICGROUP,m_pageTerminal.omPossibleItems);

	ogCicData.GetHalls(m_pageHall.omPossibleItems);
	ogCicData.GetRegions(m_pageRegion.omPossibleItems);
	ogCicData.GetLines(m_pageLine.omPossibleItems);
	
}

void CCITablePropertySheet::LoadDataFromViewer()
{
	pomViewer->GetFilter("Halle",	m_pageTerminal.omSelectedItems);
	pomViewer->GetFilter("Hall",	m_pageHall.omSelectedItems);
	pomViewer->GetFilter("Region",	m_pageRegion.omSelectedItems);
	pomViewer->GetFilter("Line",	m_pageLine.omSelectedItems);

	m_pageGroup.omGroupBy = pomViewer->GetGroup();
	UpdateEnableFlagInFilterPages();
	pomViewer->GetSort(m_pageSort.omSortOrders);
	m_pageSort.m_Group = pomViewer->GetGroup()[0] - '0';
}

void CCITablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("Halle",	m_pageTerminal.omSelectedItems);
	pomViewer->SetFilter("Hall",	m_pageHall.omSelectedItems);
	pomViewer->SetFilter("Region",	m_pageRegion.omSelectedItems);
	pomViewer->SetFilter("Line",	m_pageLine.omSelectedItems);

	pomViewer->SetGroup(m_pageGroup.omGroupBy);

	pomViewer->SetSort(m_pageSort.omSortOrders);

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int CCITablePropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedDeskTypes;
	CStringArray olSelectedTerminals;
	CStringArray olSelectedHalls;
	CStringArray olSelectedRegions;
	CStringArray olSelectedLines;
	CString olGroupBy;
	CStringArray olSortOrders;

	pomViewer->GetFilter("Halle", olSelectedTerminals);
	pomViewer->GetFilter("Hall",  olSelectedHalls);
	pomViewer->GetFilter("Region",olSelectedRegions);
	pomViewer->GetFilter("Line",  olSelectedLines);
	pomViewer->GetSort(olSortOrders);
	olGroupBy = pomViewer->GetGroup();

	if (!IsIdentical(olSelectedTerminals, m_pageTerminal.omSelectedItems) ||
		!IsIdentical(olSelectedHalls,	  m_pageHall.omSelectedItems) ||
		!IsIdentical(olSelectedRegions,	  m_pageRegion.omSelectedItems) ||
		!IsIdentical(olSelectedLines,	  m_pageLine.omSelectedItems) ||
		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
		olGroupBy != CString(m_pageGroup.omGroupBy))
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}

BEGIN_MESSAGE_MAP(CCITablePropertySheet, BasePropertySheet)
    //{{AFX_MSG_MAP(CCITablePropertySheet)
	ON_MESSAGE(WM_CCIDIAGRAM_GROUPPAGE_CHANGED, OnCciDiagramGroupPageChanged)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCITablePropertySheet message handlers

LONG CCITablePropertySheet::OnCciDiagramGroupPageChanged(UINT wParam, LONG lParam)
{
	UpdateEnableFlagInFilterPages();
	return 0L;
}

/////////////////////////////////////////////////////////////////////////////
// CCITablePropertySheet -- help routines

void CCITablePropertySheet::UpdateEnableFlagInFilterPages()
{
	m_pageTerminal.m_bEnabled = (m_pageGroup.omGroupBy == "Halle");
	m_pageHall.m_bEnabled	  = (m_pageGroup.omGroupBy == "Hall");
	m_pageRegion.m_bEnabled	  = (m_pageGroup.omGroupBy == "Region");
	m_pageLine.m_bEnabled	  = (m_pageGroup.omGroupBy == "Line");
}
