// EquipmentDW.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <CCSCedaData.h>
#include <OpssPm.h>
#include <CCSDragDropCtrl.h>
#include <tscale.h>
#include <CCSPtrArray.h>

#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <CedaDemandData.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>
#include <cviewer.h>

#include <EquipmentDetailWindow.h>

#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CLIENTWN.H>
#include <CCSCedaData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <tscale.h>
#include <cviewer.h>
#include <CCSDragDropCtrl.h>
#include <gbar.h>
#include <cxbutton.h>
#include <PrePlanTable.h>
#include <FlightPlan.h>
//#include "EquipmentTable.h"
#include <EquipmentDiagram.h>
#include <GateDiagram.h>
#include <CciDiagram.h>
#include <StartDate.h>
#include <ConflictConfigTable.h>
#include <conflict.h>
#include <dataset.h>
#include <ufis.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <ConflictTable.h>
#include <CciTable.h>
#include <GateTable.h>
#include <ButtonList.h>

#include <ccsddx.h>
#include <TimePacket.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Prototypes
static void EquipmentDetailWindowCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
/////////////////////////////////////////////////////////////////////////////
// EquipmentDetailWindow

IMPLEMENT_DYNCREATE(EquipmentDetailWindow, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

EquipmentDetailWindow::EquipmentDetailWindow()
{
}

EquipmentDetailWindow::EquipmentDetailWindow(CWnd* popParent, long lpEquUrno, CTime opClickedTime /*=TIMENULL*/, int ipDurationHours /* = 24 */)
{
	// get the equipment record
	ASSERT((prmEqu = ogEquData.GetEquByUrno(lpEquUrno)) != NULL);

	// get the equipment type
	ASSERT((prmEqt = ogEqtData.GetEqtByUrno(prmEqu->Gkey)) != NULL);

	omClickedTime = opClickedTime;

	if(ipDurationHours < 1)
		ipDurationHours = 1;
	imDurationSecs = ipDurationHours * 60 * 60;

	// get the additional info for this equipment
	ogEqaData.GetEqasByUequ(lpEquUrno, omEqaList);

	omTSStartTime = ogBasicData.GetTime();

    Create(NULL, "Equipment Detail Window", WS_OVERLAPPED | WS_CAPTION | WS_BORDER | WS_VISIBLE | WS_POPUP,
        CRect(0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN)), popParent, NULL,0,NULL);

    CenterWindow();
}

EquipmentDetailWindow::~EquipmentDetailWindow()
{
}

BEGIN_MESSAGE_MAP(EquipmentDetailWindow, CFrameWnd)
	//{{AFX_MSG_MAP(EquipmentDetailWindow)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_GETMINMAXINFO()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// EquipmentDetailWindow message handlers

int EquipmentDetailWindow::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO: Add your specialized creation code here
    //
	omViewer.Attach(this);
	omViewer.Init(prmEqu);

    omEquipmentDetail.SetEquipmentPtr(prmEqu);
    omEquipmentDetail.Create(omEquipmentDetail.IDD, this);

	CString olCaption;
	olCaption.Format("%s: %s",GetString(IDS_EQUIPMENTDETAILCAPTION),prmEqu->Enam);
	SetWindowText(olCaption);
    
    CRect olRect;
    int ilBottom = olRect.top;
    omEquipmentDetail.GetDlgItem(IDC_GANTTGRP) -> GetWindowRect(&olRect); ScreenToClient(&olRect);
    imBottomPos = olRect.bottom;
    olRect.bottom -= 10;
    
    CRect olDRect; omEquipmentDetail.GetClientRect(&olDRect);
    CRect olWRect; GetWindowRect(&olWRect);

	int ilXSize = ::GetSystemMetrics(SM_CXBORDER) * 2;

	// calculate the default size of window
	CSize olSize(olDRect.Width(),olDRect.Height());	// is dialog size
	olSize.cx += ilXSize;							// + 2 * frame border in x - direction

	olSize.cy += ::GetSystemMetrics(SM_CYBORDER);	// + frame border
	olSize.cy += ::GetSystemMetrics(SM_CYCAPTION);	// + caption
	olSize.cy += ::GetSystemMetrics(SM_CYHSCROLL)*2;// + size for status bar
	olSize.cy += ::GetSystemMetrics(SM_CYBORDER);	// + frame border

	CWnd *polParent = GetParent();
	if (polParent)
	{
		olWRect = CRect(CPoint(0,0),olSize);
		polParent->ScreenToClient(olWRect);
	}
	else
	{
		olWRect = CRect(olWRect.TopLeft(),olSize);
	}

    omMaxTrackSize = CPoint(olWRect.Width(), olWRect.Height());
    MoveWindow(olWRect.left, olWRect.top, olWRect.Width(), olWRect.Height(), FALSE);

	CTime olTime = TIMENULL;
	CTime olMinStartTime = ogBasicData.GetTimeframeStart();
	CTime olMaxEndTime = ogBasicData.GetTimeframeEnd();

	if (GetMinStartTime(&omJobList, olTime) == TRUE)
	{
		olTime -= CTimeSpan(0,1,0,0);
		if (olMinStartTime.GetTime() > olTime.GetTime())
			olMinStartTime = olTime;
	}
	if (GetMaxEndTime(&omJobList, olTime) == TRUE)
	{
		olTime += CTimeSpan(0,1,0,0);
		if (olMaxEndTime.GetTime() < olTime.GetTime())
			olMaxEndTime = olTime;
	}

    CTime olCurrentTime = ogBasicData.GetTime();
    CTime olCT = CTime(olCurrentTime.GetYear(), olCurrentTime.GetMonth(), olCurrentTime.GetDay(),olCurrentTime.GetHour(), olCurrentTime.GetMinute(), 0);

	// the current time window displayed
	int ilMidPointSecs = imDurationSecs / 2;
	omTSDuration = CTimeSpan(imDurationSecs);
	CTimeSpan olMidPoint(ilMidPointSecs); // mid-point of display is half the duration

	// intervals on the timescale
    omTSInterval = CTimeSpan(0, 0, 10, 0);

	// start time of the whole gantt chart (ie not just the time window currently displayed)
    omStartTime = olMinStartTime;

	// duration of the whole gantt chart --> minimum 1 day (+1 minute to stop divide by zero)
    omDuration = olMaxEndTime - olMinStartTime;
	if(omDuration <= omTSDuration)
	{
		omDuration = omTSDuration;
		omEquipmentDetail.m_HScrollBar.EnableWindow(FALSE);
	}

	// the display start time
	if(omClickedTime == TIMENULL)
	{
		SetTSStartTime(ogBasicData.GetDisplayDate() - ogBasicData.GetDisplayOffset());
	}
	else
	{
		// try to centre the clicked position
		CTime olStartTime = omClickedTime - olMidPoint;
		CTime olEndTime = omClickedTime + olMidPoint;
		CTime olMaxEndTime = omStartTime + omDuration;
		if(olEndTime > olMaxEndTime)
		{
			olStartTime = olMaxEndTime - omTSDuration;
		}
		if(olStartTime < omStartTime)
		{
			olStartTime = omStartTime;
		}
		SetTSStartTime(olStartTime);
	}

	if(omEquipmentDetail.m_HScrollBar.IsWindowEnabled())
	{
		long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
		long llTotalMin = CalcTotalMinutes();
		omEquipmentDetail.m_HScrollBar.SetScrollRange(0, 1000, FALSE);
		omEquipmentDetail.m_HScrollBar.SetScrollPos((int) (1000 * llTSMin / llTotalMin), FALSE);
	}

    olRect.InflateRect(-15, -15);
    omTimeScale.EnableDisplayCurrentTime(FALSE);
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE, CRect(olRect.left, olRect.top, olRect.right, olRect.top + 34),this, 0, NULL);
    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,AFX_IDW_STATUS_BAR);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);
	
    omGantt.SetTimeScale(&omTimeScale);
    omGantt.SetViewer(&omViewer, 0 /* GroupNo */);
    omGantt.SetStatusBar(&omStatusBar);
    omGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());
    omGantt.SetVerticalScaleWidth(-2);
    omGantt.SetFonts(&ogMSSansSerif_Regular_8, &ogSmallFonts_Regular_6);
    //omGantt.SetVerticalScaleColors(NAVY, SILVER, YELLOW, SILVER);
	omGantt.SetGanttChartColors(NAVY, GRAY, YELLOW, GRAY);
    omGantt.Create(0, CRect(olRect.left, olRect.top + 34, olRect.right, olRect.bottom), &omEquipmentDetail);
	omGantt.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
	
	ogCCSDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("EQUIPMENTDW"), CString("Update Time Band"), EquipmentDetailWindowCf);

	return 0;
}

BOOL EquipmentDetailWindow::GetMaxEndTime(CCSPtrArray<JOBDATA> *popJobs, CTime &ropMaxEndTime)
{
	int ilSize = popJobs->GetSize();
	if (ilSize == 0)
		return FALSE;

	CTime olMaxTime = (*popJobs)[0].Acto;
	CTime olTime;
	for (int ilIndex = 1; ilIndex < ilSize; ilIndex++)
	{
		olTime = (*popJobs)[ilIndex].Acto;
		if (olMaxTime < olTime)
			olMaxTime = olTime;
	}
	ropMaxEndTime = olMaxTime;
	return TRUE;
}

BOOL EquipmentDetailWindow::GetMinStartTime(CCSPtrArray<JOBDATA> *popJobs, CTime &ropMinStartTime)
{
	BOOL blRc = FALSE;
	if(popJobs != NULL)
	{
		int ilSize = popJobs->GetSize();
		if (ilSize != 0)
		{
			CTime olMinTime = (*popJobs)[0].Acfr;
			CTime olTime;
			for (int ilIndex = 1; ilIndex < ilSize; ilIndex++)
			{
				olTime = (*popJobs)[ilIndex].Acfr;
				if (olMinTime > olTime)
					olMinTime = olTime;
			}
			ropMinStartTime = olMinTime; 
			blRc = TRUE;
		}
	}
	return blRc;
}

void EquipmentDetailWindow::OnDestroy()
{
	if (bgModal == TRUE)
		return;
	// Unregister DDX call back function
	TRACE("EquipmentDetailWindow: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CFrameWnd::OnDestroy();
}

void EquipmentDetailWindow::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
	lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL EquipmentDetailWindow::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
    CRect olRect; GetClientRect(&olRect);
    olRect.top = imBottomPos;
    CBrush olBrush(SILVER);
    pDC->FillRect(&olRect, &olBrush);

    return TRUE;
}

////////////////////////////////////////////////////////////////////////
// EquipmentDiagram -- implementation of yellow vertical time band lines

static void EquipmentDetailWindowCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	EquipmentDetailWindow *polDetailWindow = (EquipmentDetailWindow *)popInstance;

	switch (ipDDXType)
	{
	case STAFFDIAGRAM_UPDATETIMEBAND:
		TIMEPACKET *polTimePacket = (TIMEPACKET *)vpDataPointer;
		if (::IsWindow(polDetailWindow->omGantt.GetSafeHwnd()))	// the window is still opened?
		{
			polDetailWindow->omGantt.SetMarkTime(polTimePacket->StartTime, polTimePacket->EndTime);
		}
		break;
	}
}

void EquipmentDetailWindow::OnHorizScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
    int ilPos = 0;
	long llTotalMin = 0;

	switch (nSBCode)
	{
		case SB_LINELEFT:	// scroll one hour left
	        ilPos = omEquipmentDetail.m_HScrollBar.GetScrollPos() - int (60 * 1000L / CalcTotalMinutes());
			if (ilPos <= 0)
			{
				SetTSStartTime(omStartTime);
			}
			else
			{
				SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));
			}

			omTimeScale.SetDisplayStartTime(omTSStartTime);
			omTimeScale.Invalidate(TRUE);
			omEquipmentDetail.m_HScrollBar.SetScrollPos(ilPos, TRUE);
            omGantt.Invalidate(TRUE);
			break;

		case SB_LINERIGHT:		// scroll one hour right
	        ilPos = omEquipmentDetail.m_HScrollBar.GetScrollPos() + int (60 * 1000L / CalcTotalMinutes());
			if (ilPos >= 1000)
			{
				SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (CalcTotalMinutes()), 0));
			}
			else
			{
				SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));
			}

			omTimeScale.SetDisplayStartTime(omTSStartTime);
			omTimeScale.Invalidate(TRUE);
			omEquipmentDetail.m_HScrollBar.SetScrollPos(ilPos, TRUE);
            omGantt.Invalidate(TRUE);
			break;

		case SB_PAGELEFT:
			ilPos = omEquipmentDetail.m_HScrollBar.GetScrollPos() - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / CalcTotalMinutes());
			if (ilPos <= 0)
			{
				ilPos = 0;
				SetTSStartTime(omStartTime);
			}
			else
			{
				SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
			}
			omTimeScale.SetDisplayStartTime(omTSStartTime);
			omTimeScale.Invalidate(TRUE);
			omEquipmentDetail.m_HScrollBar.SetScrollPos(ilPos, TRUE);
            omGantt.Invalidate(TRUE);
			break;

		case SB_PAGERIGHT:
			ilPos = omEquipmentDetail.m_HScrollBar.GetScrollPos() + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / CalcTotalMinutes());
			if (ilPos >= 1000)
			{
				ilPos = 1000;
				SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (CalcTotalMinutes()), 0));
			}
			else
			{
				SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
			}
			omTimeScale.SetDisplayStartTime(omTSStartTime);
			omTimeScale.Invalidate(TRUE);
			omEquipmentDetail.m_HScrollBar.SetScrollPos(ilPos, TRUE);
            omGantt.Invalidate(TRUE);
			break;

		case SB_THUMBTRACK:
			SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (CalcTotalMinutes() * nPos / 1000), 0));
			omTimeScale.SetDisplayStartTime(omTSStartTime);
			omTimeScale.Invalidate(TRUE);
			omEquipmentDetail.m_HScrollBar.SetScrollPos(nPos, TRUE);
            omGantt.Invalidate(TRUE);
			break;

		case SB_THUMBPOSITION:	// the thumb was just released?
			return;
		case SB_ENDSCROLL :
			break;
	}
}

long EquipmentDetailWindow::CalcTotalMinutes(void)
{
	long llTotalMinutes = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	if(llTotalMinutes <= 0)
	{
		llTotalMinutes = 1; // prevent divide by zero error
	}
	return llTotalMinutes;
}

void EquipmentDetailWindow::SetTSStartTime(CTime opNewTsStartTime)
{
	CString opDay = opNewTsStartTime.Format("%d/%m/%Y");
	if(opDay != omTSStartTime.Format("%d/%m/%Y"))
	{
		omEquipmentDetail.GetDlgItem(IDC_GANTTGRP)->SetWindowText(opDay);
	}
	omTSStartTime = opNewTsStartTime;
}
