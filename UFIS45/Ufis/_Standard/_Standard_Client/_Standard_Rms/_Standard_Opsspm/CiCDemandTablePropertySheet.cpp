// CiCDemandTablePropertySheet.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h> 
#include <CedaRnkData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <DemandFilterPage.h>
#include <CiCDemandTableSortPage.h>
#include <BasePropertySheet.h>
#include <CicDemandTablePropertySheet.h>
#include <CedaFlightData.h>
#include <conflict.h>
#include <CedaAloData.h>
#include <CedaAltData.h>
#include <CedaCccData.h>
#include <CiCDemandTableViewer.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

 
IMPLEMENT_DYNAMIC(CicDemandTablePropertySheet, BasePropertySheet)

/////////////////////////////////////////////////////////////////////////////
// CicDemandTablePropertySheet class variables
//

/////////////////////////////////////////////////////////////////////////////
// CicDemandTablePropertySheet
//
CicDemandTablePropertySheet::CicDemandTablePropertySheet(CStringArray& popGroupNames,
	CWnd* pParentWnd,CViewer *popViewer, UINT iSelectPage) 
	: BasePropertySheet(GetString(IDS_STRING61877), pParentWnd, popViewer, iSelectPage)
{
	EnableStackedTabs(FALSE);
	AddPage(&m_pageColumns);
	AddPage(&m_pageDety);
	AddPage(&m_pageAirline);
	AddPage(&m_pageFunctions);
	AddPage(&m_pageClasses);
	AddPage(&m_pageGroups);
	AddPage(&m_pageSort);

	m_pageColumns.SetCaption(GetString(IDS_STRING61895));

	m_pageDety.SetCaption(GetString(IDS_STRING61876));

	m_pageAirline.SetCaption(GetString(IDS_STRING61597));

	m_pageFunctions.SetCaption(GetString(IDS_STRING61604));

	m_pageClasses.SetCaption(GetString(IDS_STRING61971));

	m_pageGroups.SetCaption(GetString(IDS_STRING61972));

	for (int i = 0; i < ogDemandData.GetCountOfDemandTypes(); i++)
	{
		DEMANDDATA olDemand;
		olDemand.Dety[0] = '0' + i;
		//m_pageDety.omPossibleItems.Add(ogDemandData.GetStringForDemandAndResourceType(olDemand));
		m_pageDety.omPossibleItems.Add(ogDemandData.GetStringForDemandType(olDemand));
		if (ogDemandData.IsFlightDependingDemand(&olDemand))
		{
			m_DetyPageMap.SetAt(ogDemandData.GetStringForDemandType(olDemand),&m_pageAirline);
		}
	}

	m_pageDety.bmSelectAllEnabled = true;

/*
	// get all possible aloc's
	const char **pclAllocGroupType = DemandTableViewer::omAllocGroupTypes;
	while(*pclAllocGroupType)
	{
		m_pageAloc.omPossibleItems.Add(*pclAllocGroupType);

		// add allocation id pages
		DemandFilterPage *polPage = NULL;
		if (!m_pageAlids.Lookup(*pclAllocGroupType,(void *&)polPage))
		{
			polPage = new DemandFilterPage;
			m_pageAlids.SetAt(*pclAllocGroupType,polPage);

			ALLOCUNIT *pclUnit = ogAllocData.GetGroupTypeByName(*pclAllocGroupType);
			if (pclUnit)
				polPage->SetCaption(pclUnit->Desc);
			else
				polPage->SetCaption(*pclAllocGroupType);

			CCSPtrArray <ALLOCUNIT> olAllocGroups;
			ogAllocData.GetGroupsByGroupType(*pclAllocGroupType,olAllocGroups);
			int ilNumGroups = olAllocGroups.GetSize();
			for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
			{
				ALLOCUNIT *prlGroup= &olAllocGroups[ilGroup];
				polPage->omPossibleItems.Add(prlGroup->Name);
			}
			polPage->bmSelectAllEnabled = true;
		}
		++pclAllocGroupType;
	}
	m_pageAloc.bmSelectAllEnabled = true;
*/

	// airline codes format ALC2/ALC3
	ogAltData.GetAlc2Alc3Strings(m_pageAirline.omPossibleItems);
	m_pageAirline.bmSelectAllEnabled = true;

	ogRnkData.GetAllRanks(m_pageFunctions.omPossibleItems);
	m_pageFunctions.bmSelectAllEnabled = true;


	CStringArray rolNames;
	int n = ogCccData.GetClassNameList(rolNames);
	for (i = 0; i < n; i++)
	{
		m_pageClasses.omPossibleItems.Add(rolNames[i]);
	}

	m_pageClasses.omPossibleItems.Add(GetString(IDS_UNDEFINED));

	m_pageClasses.bmSelectAllEnabled = true;

	for (i = 0; i < popGroupNames.GetSize(); i++)
	{
		m_pageGroups.omPossibleItems.Add(popGroupNames[i]);
	}
	m_pageGroups.bmSelectAllEnabled = true;

	// get all possible columns
	CStringArray olColumns;
	CicDemandTableViewer::GetAllViewColumns(olColumns);
	for (int ilC = 0; ilC < olColumns.GetSize(); ilC++)
	{
		m_pageColumns.omPossibleItems.Add(CicDemandTableViewer::GetViewColumnName(olColumns[ilC]));		
	}
	
	m_pageColumns.SetSorted(FALSE);

}

CicDemandTablePropertySheet::~CicDemandTablePropertySheet()
{
/*
	POSITION pos = m_pageAlids.GetStartPosition();
	while(pos)
	{
		CString olKey;
		DemandFilterPage *polPage = NULL;
		m_pageAlids.GetNextAssoc(pos,olKey,(void *&)polPage);
		if (GetPageIndex(polPage) >= 0)
			RemovePage(polPage);
		delete polPage;
	}
	m_pageAlids.RemoveAll();
*/
}

BEGIN_MESSAGE_MAP(CicDemandTablePropertySheet, BasePropertySheet)
	//{{AFX_MSG_MAP(CicDemandTablePropertySheet)
    ON_MESSAGE(WM_UPDATE_ALL_PAGES, OnUpdateAllPages)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CicDemandTablePropertySheet::LoadDataFromViewer()
{

	// remove all previously used Alid - based pages from the sheet
/*	POSITION pos = m_DetyPageMap.GetStartPosition();
	while(pos)
	{
		CString olKey;
		CPropertyPage *polPage = NULL;
		m_DetyPageMap.GetNextAssoc(pos,olKey,(void *&)polPage);
		if (GetPageIndex(polPage) >= 0)
			RemovePage(polPage);
	}
*/
	// get demand type filter
	pomViewer->GetFilter("Dety", m_pageDety.omSelectedItems);

	bool blSelectAll = false;
	if (m_pageDety.omSelectedItems.GetSize() == 1)
	{
		if (m_pageDety.omSelectedItems[0] == GetString(IDS_ALLSTRING))
		{
			blSelectAll = true;
		}
	}

	if (blSelectAll)
	{
		POSITION pos = m_DetyPageMap.GetStartPosition();
		while(pos)
		{
			CString olName;
			CPropertyPage *polPage = NULL;
			m_DetyPageMap.GetNextAssoc(pos,olName,(void *&)polPage);
			if (GetPageIndex(polPage) < 0)
			{
				AddPage(polPage);
			}
		}
	}
	else
	{

		for (int ilC = 0; ilC < m_pageDety.omSelectedItems.GetSize();ilC++)
		{
			CPropertyPage *polPage = NULL;
			if (m_DetyPageMap.Lookup(m_pageDety.omSelectedItems[ilC],(void *&)polPage) && polPage != NULL)
			{
				if (GetPageIndex(polPage) < 0)
				{
					AddPage(polPage);
				}
			}
		}
	}

	pomViewer->GetFilter("Airline",		m_pageAirline.omSelectedItems);

	pomViewer->GetFilter("Functions", m_pageFunctions.omSelectedItems);

	CStringArray olShortNames;
	pomViewer->GetFilter("Classes",		olShortNames);
	ogCccData.GetClassNameList(olShortNames,m_pageClasses.omSelectedItems);

	pomViewer->GetFilter("Groups",m_pageGroups.omSelectedItems);

	CStringArray olColumns;
	pomViewer->GetFilter("Columns",olColumns);
	m_pageColumns.omSelectedItems.RemoveAll();
	for (int ilC = 0; ilC < olColumns.GetSize(); ilC++)
	{
		m_pageColumns.omSelectedItems.Add(CicDemandTableViewer::GetViewColumnName(olColumns[ilC]));		
	}

	pomViewer->GetSort(m_pageSort.omSortOrders);
}

void CicDemandTablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
/*
	pomViewer->SetFilter("Aloc", m_pageAloc.omSelectedItems);
	for (int ilC = 0; ilC < m_pageAloc.omSelectedItems.GetSize();ilC++)
	{
		DemandFilterPage *polPage = NULL;
		if (m_pageAlids.Lookup(m_pageAloc.omSelectedItems[ilC],(void *&)polPage) && polPage != NULL)
		{
			pomViewer->SetFilter(m_pageAloc.omSelectedItems[ilC],polPage->omSelectedItems);
		}
	}


*/
	pomViewer->SetFilter("Dety",		m_pageDety.omSelectedItems);

	pomViewer->SetFilter("Airline",		m_pageAirline.omSelectedItems);

	pomViewer->SetFilter("Functions", m_pageFunctions.omSelectedItems);

	CStringArray olShortNames;
	ogCccData.GetClassList(m_pageClasses.omSelectedItems,olShortNames);
	pomViewer->SetFilter("Classes",		olShortNames);

	pomViewer->SetFilter("Groups",		m_pageGroups.omSelectedItems);

	CStringArray olColumns;
	for (int ilC = 0; ilC < m_pageColumns.omSelectedItems.GetSize(); ilC++)
	{
		olColumns.Add(CicDemandTableViewer::GetViewColumnField(m_pageColumns.omSelectedItems[ilC]));		
	}
	pomViewer->SetFilter("Columns",olColumns);

	pomViewer->SetSort(m_pageSort.omSortOrders);

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int CicDemandTablePropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedDetys;
	CStringArray olSelectedAirlines;
	CStringArray olSelectedFunctions;
	CStringArray olSelectedClasses;
	CStringArray olSelectedGroups;
	CStringArray olSelectedColumns;
	CStringArray olSortOrders;
	CString olGroupBy;

	pomViewer->GetFilter("Dety",		olSelectedDetys);
	pomViewer->GetFilter("Airline",		olSelectedAirlines);
	pomViewer->GetFilter("Functions",	olSelectedFunctions);

	pomViewer->GetFilter("Classes",		olSelectedClasses);
	CStringArray olClasses;
	ogCccData.GetClassList(m_pageClasses.omSelectedItems,olClasses);

	pomViewer->GetFilter("Groups",		olSelectedGroups);

	CStringArray olSelectedColumnsFields;
	pomViewer->GetFilter("Columns",olSelectedColumnsFields);
	for (int ilC = 0; ilC < olSelectedColumnsFields.GetSize(); ilC++)
	{
		olSelectedColumns.Add(CicDemandTableViewer::GetViewColumnName(olSelectedColumnsFields[ilC]));	//QDO 	
	}
	

	pomViewer->GetSort(olSortOrders);

	if	( !IsIdentical(olSelectedDetys,		m_pageDety.omSelectedItems) 
		||!IsIdentical(olSelectedAirlines,	m_pageAirline.omSelectedItems)
		||!IsIdentical(olSelectedFunctions,	m_pageFunctions.omSelectedItems)
		||!IsIdentical(olSelectedClasses,	olClasses)
		||!IsIdentical(olSelectedGroups,	m_pageGroups.omSelectedItems) 
		||!IsIdentical(olSelectedColumns,	m_pageColumns.omSelectedItems) 
		||!IsIdentical(olSortOrders,		m_pageSort.omSortOrders) 
		)
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}

LONG CicDemandTablePropertySheet::OnUpdateAllPages(UINT wParam,LONG lParam)
{
	CPropertyPage *popSenderPage = reinterpret_cast<CPropertyPage *>(lParam);
	if (popSenderPage == &m_pageDety)
	{
		bool blSelectAll = false;
		if (m_pageDety.m_List2.GetCount() == 1)
		{
			CString olName;
			m_pageDety.m_List2.GetText(0,olName);
			if (olName == GetString(IDS_ALLSTRING))
				blSelectAll = true;
		}

		POSITION pos = m_DetyPageMap.GetStartPosition();
		while(pos)
		{
			CString olName;
			CPropertyPage *polPage = NULL;
			m_DetyPageMap.GetNextAssoc(pos,olName,(void *&)polPage);
			if (blSelectAll && GetPageIndex(polPage) < 0)
			{
				AddPage(polPage);
			}
			else if (m_pageDety.m_List2.FindStringExact(-1,olName) == LB_ERR)
			{
			/*	if (GetPageIndex(polPage) >= 0)
					RemovePage(polPage);*/
			}
			else if (GetPageIndex(polPage) < 0)
				AddPage(polPage);
		}
	}

	return 0l;
}