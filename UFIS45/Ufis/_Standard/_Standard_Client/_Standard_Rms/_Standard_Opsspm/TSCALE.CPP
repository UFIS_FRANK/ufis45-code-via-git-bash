// tscale.cpp : implementation file
// 

#include <stdafx.h>
#include <OpssPm.h>
#include <tscale.h>
#include <CCSGlobl.h>
#include <BasicData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTimeScale

CTimeScale::CTimeScale()
{
    bmDisplayCurrentTime = TRUE;
	omCurrentTime = ogBasicData.GetTime();
	//omOldCurrentTime = omCurrentTime;
}

CTimeScale::~CTimeScale()
{
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This code has been written to avoid AfxAssert() failed(). The situation
// is ~GateDagram() is called the ~CTimeScale() is called but there is no
// more time scale window opened.
    for (int ilIndex = 0; ilIndex < omTSIArray.GetSize(); ilIndex++)
    {
        delete (CTopScaleIndicator *) omTSIArray[ilIndex];
    }

    omTSIArray.RemoveAll();
/*
    while (omTSIArray.GetSize() > 0)
	{
		CTopScaleIndicator *p = (CTopScaleIndicator *)omTSIArray[0];
		delete p;
	}

    omTSIArray.RemoveAll();
*/
////////////////////////////////////////////////////////////////////////
}

BEGIN_MESSAGE_MAP(CTimeScale, CWnd)
    //{{AFX_MSG_MAP(CTimeScale)
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()


void CTimeScale::EnableDisplayCurrentTime(BOOL bpDisplayCurrentTime)
{
    bmDisplayCurrentTime = bpDisplayCurrentTime;
}

void CTimeScale::SetDisplayTimeFrame(CTime opDisplayStart, CTimeSpan opDuration, CTimeSpan opInterval)
{
	CTime olDisplayStart = opDisplayStart != TIMENULL ? opDisplayStart : CTime::GetCurrentTime();

	SetDisplayStartTime(olDisplayStart);
    SetTimeInterval(opInterval);
    
    CRect olClientRect; GetClientRect(&olClientRect);

    imP0 = olClientRect.Height() / 2;
    imP1 = imP0 * 125 / 100;
    imP2 = imP0 * 150 / 100;
    imP3 = olClientRect.bottom;

    int ilSlotCount = int (opDuration.GetTotalMinutes() / opInterval.GetTotalMinutes());
    // for the right most pixel
    fmIntervalWidth = (double) (olClientRect.Width() - 1) / ilSlotCount;
	//TRACE("number of slot %d, width %g", ilSlotCount, fmIntervalWidth);
}

void CTimeScale::SetDisplayStartTime(CTime opDisplayStart)
{
	CTime olDisplayStart = opDisplayStart != TIMENULL ? opDisplayStart : CTime::GetCurrentTime();

    // DisplayStart Time must begin at 0 seconds
    omDisplayStart = CTime(
        olDisplayStart.GetYear(), olDisplayStart.GetMonth(),
        olDisplayStart.GetDay(), olDisplayStart.GetHour(),
        olDisplayStart.GetMinute(), 0
    );

    
    //omDisplayStart = opDisplayStart;
}

void CTimeScale::SetTimeInterval(CTimeSpan opInterval)
{
    omInterval = opInterval;
}

CTimeSpan CTimeScale::GetDisplayDuration(void)
{
    CRect olClientRect; GetClientRect(&olClientRect);

    return CTimeSpan(0, 0, int (omInterval.GetTotalMinutes() * (olClientRect.Width() - 1) / fmIntervalWidth), 0);
}

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This block of code is used for make the TimeScale be able to help
// the other classes which desire to calculate the time backward from the
// given point.
CTime CTimeScale::GetTimeFromX(int ipX)	// offset of ipX is zero
{
    /*
	long llSeconds = (long)((ipX) * omInterval.GetTotalSeconds() / (fmIntervalWidth * 60));
        // llSeconds is measured from omDisplayStart
        // fmIntervalWidth keep number of pixel per each interval
    long llMinutes = llSeconds / 60;
    llSeconds %= 60;
    */
    long llSeconds = 0;
    long llMinutes = (long)((ipX) * omInterval.GetTotalMinutes() / fmIntervalWidth);

// DTT Jul.29 -- I've found some bug, the GetTimeFromX() and GetXFromTime()
// cannot work together correctly, i.e., they are not invert function of each other.
// So when we convert X to time back-and-forth again-and-again, some error will
// happened. I still unsure about what the real reason of this. But I think we
// may very ugly and dirty to fix this bug by plus one minute.
	llMinutes++;
////////////////

    long llHours = llMinutes / 60;
    llMinutes %= 60;
    return (omDisplayStart + CTimeSpan(0, llHours, llMinutes, llSeconds));
}
////////////////////////////////////////////////////////////////////////

int CTimeScale::GetXFromTime(CTime opTime)
{
    long llTotalMin = (opTime - omDisplayStart).GetTotalMinutes();
    int ilRet = int (fmIntervalWidth * llTotalMin / omInterval.GetTotalMinutes());
	/* TRACE("CTimeScale: GetXFromTime() return %03d (%7.3f %ld)\n",
        ilRet,
        fmIntervalWidth,
        (opTime - omDisplayStart).GetTotalMinutes()
    ); */
	return ilRet;
}

void CTimeScale::UpdateCurrentTimeLine(void)
{
	CTime olTime = ogBasicData.GetTime();
	UpdateCurrentTimeLine(olTime);
}

void CTimeScale::UpdateCurrentTimeLine(CTime opTime)
{
    int ilX = GetXFromTime(opTime);
    //int ilOldX = GetXFromTime(omOldCurrentTime);
	int ilOldX = GetXFromTime(omCurrentTime);
    if (ilX != ilOldX)
    {
		omCurrentTime = opTime;

        CRect olTSRect; GetClientRect(&olTSRect);
        olTSRect.left = ilOldX;

        olTSRect.right = ilX + 1;       // must be +1
		// old current time should be the left of current time so comment next line
        //olTSRect.NormalizeRect();
        InvalidateRect(&olTSRect, TRUE);
    }
}

void CTimeScale::AddTopScaleIndicator(CTime opStartTime, CTime opEndTime, COLORREF lpColor)
{
    CTopScaleIndicator *polTSI = new CTopScaleIndicator;
    polTSI->omStart = opStartTime;
    polTSI->omEnd = opEndTime;
    polTSI->lmColor = lpColor;
    
    AddTopScaleIndicator(polTSI);
}

void CTimeScale::DisplayTopScaleIndicator(CDC *popDC)
{
    CPen *polOldPen = (CPen *) popDC->SelectStockObject(BLACK_PEN);
    
    CPen olPen;
    CTopScaleIndicator *polTSI;
    for (int ilIndex = 0; ilIndex < omTSIArray.GetSize(); ilIndex++)
    {
        polTSI = (CTopScaleIndicator *) omTSIArray.GetAt(ilIndex);

        olPen.CreatePen(PS_SOLID, 1, polTSI->lmColor);
                
        popDC->SelectObject(&olPen);
        
        popDC->MoveTo(GetXFromTime(polTSI->omStart), (imP3 - 3) - (ilIndex * (2 + 2)));
        popDC->LineTo(GetXFromTime(polTSI->omEnd) + 1, (imP3 - 3) - (ilIndex * (2 + 2)));
        
        popDC->MoveTo(GetXFromTime(polTSI->omStart), (imP3 - 3) - 1 - (ilIndex * (2 + 2)));
        popDC->LineTo(GetXFromTime(polTSI->omEnd) + 1, (imP3 - 3) - 1 - (ilIndex * (2 + 2)));


        /* TRACE("Display TSI: [%ld][%03d]\n",
            (polTSI->omStart - omDisplayStart).GetTotalMinutes(),
            GetXFromTime(polTSI->omStart)
        ); */

        
        popDC->SelectObject(polOldPen);
        
        olPen.DeleteObject();
    }
    
    popDC->SelectObject(polOldPen);
}

BOOL CTimeScale::TSIPos(int *ipLeft, int *ipRight)
{
    BOOL blRet = TRUE;
    int ilTSISize = omTSIArray.GetSize();
    if (ilTSISize == 0)
        blRet = FALSE;
    else
    {
        int ilMinLeft, ilMaxRight;
        CTopScaleIndicator *polTSI = (CTopScaleIndicator *) omTSIArray.GetAt(0);

        if (ilTSISize == 1)
        {
            *ipLeft = GetXFromTime(polTSI->omStart);
            *ipRight = GetXFromTime(polTSI->omEnd);
        }
        else
        {
            ilMinLeft = GetXFromTime(polTSI->omStart);
            ilMaxRight = GetXFromTime(polTSI->omEnd);
        
            int ilLeft, ilRight;
            for (int ilIndex = 1; ilIndex < ilTSISize; ilIndex++)
            {
                polTSI = (CTopScaleIndicator *) omTSIArray.GetAt(ilIndex);


                ilLeft = GetXFromTime(polTSI->omStart);
                if (ilLeft < ilMinLeft)
                    ilMinLeft = ilLeft;
            
                ilRight = GetXFromTime(polTSI->omEnd);
                if (ilRight > ilMaxRight)
                    ilMaxRight = ilRight;
            }
            *ipLeft = ilMinLeft;
            *ipRight = ilMaxRight;
        }
	    (*ipRight)++; // InvalidateRect()'s rule of thumb
    }

    return blRet;
}

void CTimeScale::DisplayTopScaleIndicator(void)
{
    // find the right place to InvalidateRect
    int ilLeft, ilRight;
    if (TSIPos(&ilLeft, &ilRight))
    {
        CRect olClientRect; GetClientRect(&olClientRect);
        CRect olRect(ilLeft, olClientRect.top, ilRight, olClientRect.bottom);
        
        InvalidateRect(&olRect, FALSE);
    }
}
 
void CTimeScale::RemoveAllTopScaleIndicator(void)
{
    // find the right place to InvalidateRect
    int ilLeft, ilRight;
    if (TSIPos(&ilLeft, &ilRight))
    {
        CRect olClientRect; GetClientRect(&olClientRect);
        CRect olRect(ilLeft, olClientRect.top, ilRight, olClientRect.bottom);
        
        InvalidateRect(&olRect, TRUE);

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This code has been written to avoid AfxAssert() failed(). The situation
// is ~GateDagram() is called the ~CTimeScale() is called but there is no
// more time scale window opened.
/*		while (omTSIArray.GetSize() > 0)
		{
			CTopScaleIndicator *p = (CTopScaleIndicator *)omTSIArray[0];
			delete p;
		}
*/

        for (int ilIndex = 0; ilIndex < omTSIArray.GetSize(); ilIndex++)
        {
            delete (CTopScaleIndicator *) omTSIArray[ilIndex];
        }

        omTSIArray.RemoveAll();
////////////////////////////////////////////////////////////////////////
    }
}
 
/////////////////////////////////////////////////////////////////////////////
// CTimeScale message handlers


BOOL CTimeScale::OnEraseBkgnd(CDC* pDC)
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    

    return TRUE;

    //return CWnd::OnEraseBkgnd(pDC);
}

void CTimeScale::OnPaint()
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    
    // Do not call CWnd::OnPaint() for painting messages
    CRect olClientRect; GetClientRect(&olClientRect);

    
    CFont *polOldFont = dc.SelectObject(omFont);
    CPen olPen(PS_SOLID, 1, lmTextColor);
    CPen *polOldPen = dc.SelectObject(&olPen);
    dc.SetBkMode(TRANSPARENT);
    dc.SetTextColor(lmTextColor);
    

    /*
    // dump time
    char clBuf[64];
    CTime olTime;
    
    olTime = omDisplayStart;
    wsprintf(clBuf, "omDisplayStart: [%d %d %d] [%d:%d:%d]\n\r",
        olTime.GetYear(), olTime.GetMonth(), olTime.GetDay(),
        olTime.GetHour(), olTime.GetMinute(), olTime.GetSecond());
    OutputDebugString(clBuf);

    olTime = omDisplayEnd;
    wsprintf(clBuf, "omDisplayEnd: %d %d %d %d %d %d \n\r",
        olTime.GetYear(), olTime.GetMonth(), olTime.GetDay(),
        olTime.GetHour(), olTime.GetMinute(), olTime.GetSecond());
    OutputDebugString(clBuf);
    */


    char clHeader[8];
    int ilDiffMin = omDisplayStart.GetMinute() % (int) omInterval.GetTotalMinutes();
    
    double flCurX = 0.0;
    CTime olCurTime = omDisplayStart;
    if (ilDiffMin > 0)
    {
        // for the left most pixel
        
    
        ilDiffMin = (int) omInterval.GetTotalMinutes() - ilDiffMin;
        
        flCurX = ilDiffMin * fmIntervalWidth / omInterval.GetTotalMinutes();
        olCurTime += CTimeSpan(0, 0, ilDiffMin, 0);
    }
    //TRACE("Display First Time: %03d %07.3f\n", ilDiffMin, flCurX);
    
    while (flCurX <= olClientRect.right)
    {
        dc.MoveTo((int) flCurX, imP3);
        
        if ((olCurTime.GetMinute() % 60) == 0)
        {
            dc.LineTo((int) flCurX, imP0);

            sprintf(clHeader, "%02d00", olCurTime.GetHour());
            dc.TextOut((int) flCurX - (dc.GetTextExtent(clHeader, strlen (clHeader)).cx / 2), 2,
                clHeader, strlen (clHeader));

            //TRACE("Display Time: %s %03d\n", clHeader, (int) flCurX);
        }                      
        else if ((olCurTime.GetMinute() % 30) == 0)
            dc.LineTo((int) flCurX, imP1);
        else
            dc.LineTo((int) flCurX, imP2);
        
        
        olCurTime += omInterval;
        flCurX += fmIntervalWidth;
    }
    //


    DisplayTopScaleIndicator((CDC *) &dc);

    
    // display current time 
    if (bmDisplayCurrentTime)
    {
        //CTime olLTime = ogBasicData.GetTime();
        //int ilX = GetXFromTime(olLTime);
		int ilX = GetXFromTime(omCurrentTime);

        CPen olHPen(PS_SOLID, 1, lmHilightColor);
        dc.SelectObject(&olHPen);
    
        dc.MoveTo(ilX, olClientRect.top);
        dc.LineTo(ilX, imP3);
    
        //omOldCurrentTime = olLTime;
		//omOldCurrentTime = omCurrentTime;
    }
    //

    
    dc.SelectObject(polOldPen);
    dc.SelectObject(polOldFont);
}


