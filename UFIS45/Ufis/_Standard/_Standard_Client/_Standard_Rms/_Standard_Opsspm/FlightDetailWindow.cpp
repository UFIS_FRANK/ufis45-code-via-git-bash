// FlightDetailWindow.cpp : implementation file
//

#include <stdafx.h>
#include <DataSet.h>
#include <ccsglobl.h>
#include <CCSCedaData.h>
#include <OpssPm.h>
#include <CCSDragDropCtrl.h>
#include <tscale.h>
#include <CCSPtrArray.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <CedaDemandData.h>
#include <GateViewer.h>
#include <FlightDetailWindow.h>
#include <CCSPtrArray.h>
#include <CLIENTWN.h>
#include <CCSCedaData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <tscale.h>
#include <cviewer.h>
#include <CCSDragDropCtrl.h>
#include <gbar.h>
#include <cxbutton.h>
#include <PrePlanTable.h>
#include <CCITable.h>
#include <GateTable.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <CciDiagram.h>
#include <StartDate.h>
#include <ConflictConfigTable.h>
#include <conflict.h>
#include <dataset.h>
#include <ufis.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <ConflictTable.h>
#include <ButtonList.h>
#include <ccsddx.h>
#include <CedaAcrData.h>
#include <CedaRudData.h>
#include <CedaRueData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Prototypes
static void FlightDetailWindowCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// FlightDetailWindow

IMPLEMENT_DYNCREATE(FlightDetailWindow, CFrameWnd)

/////////////////////////////////////////////////////////////////////////////
// FlightDetailWindow

FlightDetailWindow::FlightDetailWindow()
{
}

// bpUseKeysAsSpecified - indicates that the keys shouldn't be swapped over if the primary is a departure and
// the secondary is an arrival - this is useful in the case of return flights
FlightDetailWindow::FlightDetailWindow(CWnd* popParent, long lpFlightPrimaryKey, long lpFlightSecondaryKey, BOOL bpShadowBar /*FALSE*/, 
									   const char *pcpAllocUnitType /*""*/, ACRDATA *prpAcr /*NULL*/, int ipDemandType /*=ALLDEMANDS*/,
									   CTime opMarkTimeStart /*TIMENULL*/, CMapStringToPtr* prpMapFunctionCode /*NULL*/,CMapStringToPtr* prpMapEquDemandGroups/* = NULL*/)
{
    

	
	imStartTimeScalePos = 150;
	bmInitialized = false;
	imScreenHeight = 0;
	imPrevScreenHeight = 0;
	bmScrolling = false;
	imUpdatedLine = 0;
	omMarkTimeStart = opMarkTimeStart;

	prmAcr = prpAcr;
	omAllocUnitType = CString(pcpAllocUnitType);
	imDemandType = ipDemandType;

	lmFlightPrimaryKey = lpFlightPrimaryKey;
	lmFlightSecondaryKey = lpFlightSecondaryKey;

	SetFlightPtrs();

	omTSStartTime = ogBasicData.GetTime();
	imPrevScrollPos = 0;
	bmSelchange= FALSE;
	pomMapFunctionCode = prpMapFunctionCode; //PRF 8999
	pomMapEquDemandGropus = prpMapEquDemandGroups;

	Initialize();

    // preset to huge size, will be changed in OnCreate method
	CRect olRect(0,0,::GetSystemMetrics(SM_CXSCREEN),::GetSystemMetrics(SM_CYSCREEN));
    //Create(	NULL, omCaption , WS_OVERLAPPED | WS_CAPTION | WS_BORDER | WS_VISIBLE | WS_POPUP | WS_SYSMENU,	olRect,popParent, NULL,0,NULL);
    Create(	NULL, omCaption , WS_OVERLAPPED | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_POPUP | WS_SYSMENU,	olRect,popParent, NULL,0,NULL);
    CenterWindow();

	// initialize Allocation types
	CComboBox *polAllocTypes = static_cast<CComboBox *>(omFlightDetail.GetDlgItem(IDC_ALLOCTYPES));
	if (polAllocTypes)
	{
		polAllocTypes->ResetContent();
		omAllocTypes.RemoveAll();

		CString olAllocName;
		int ilIndex = -1;

		if (ogBasicData.bmDisplayCheckins)
		{
			olAllocName = ogBasicData.GetStringForAllocUnitType(ALLOCUNITTYPE_CIC);
			ilIndex = polAllocTypes->AddString(olAllocName);
			polAllocTypes->SetItemData(ilIndex,omAllocTypes.Add(ALLOCUNITTYPE_CIC));
		}

		if (ogBasicData.bmDisplayGates)
		{
			olAllocName = ogBasicData.GetStringForAllocUnitType(ALLOCUNITTYPE_GATE);
			ilIndex = polAllocTypes->AddString(olAllocName);
			polAllocTypes->SetItemData(ilIndex,omAllocTypes.Add(ALLOCUNITTYPE_GATE));
		}

		if (ogBasicData.bmDisplayPositions)
		{
			olAllocName = ogBasicData.GetStringForAllocUnitType(ALLOCUNITTYPE_PST);
			ilIndex = polAllocTypes->AddString(olAllocName);
			polAllocTypes->SetItemData(ilIndex,omAllocTypes.Add(ALLOCUNITTYPE_PST));
		}

		if (ogBasicData.bmDisplayRegistrations)
		{
			olAllocName = ogBasicData.GetStringForAllocUnitType(ALLOCUNITTYPE_REGN);
			ilIndex = polAllocTypes->AddString(olAllocName);
			polAllocTypes->SetItemData(ilIndex,omAllocTypes.Add(ALLOCUNITTYPE_REGN));
		}

		if (polAllocTypes->GetCount() > 1)
		{
			ilIndex = polAllocTypes->AddString(GetString(IDS_ALLSTRING));
			polAllocTypes->SetItemData(ilIndex,omAllocTypes.Add(GetString(IDS_ALLSTRING)));
			polAllocTypes->SetCurSel(ilIndex);
			
				
			for (int ilC = 0; ilC < omAllocTypes.GetSize();ilC++)
			{
				CString olAlloc = omAllocTypes[ilC];
				if (olAlloc == omAllocUnitType)
				{
					for (int j = 0; j < polAllocTypes->GetCount(); j++)
					{
						if (int(polAllocTypes->GetItemData(j)) == ilC)
						{
							polAllocTypes->SetCurSel(j);
							break;
						}
					}
					break;
				}
			}
		}
		else if (polAllocTypes->GetCount() == 1)
			polAllocTypes->SetCurSel(0);
		else	// if (polAllocTypes->GetCount() == 0)
		{
			CWnd *polWnd = omFlightDetail.GetDlgItem(IDC_DEMANDS);
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
			polAllocTypes->ShowWindow(SW_HIDE);
		}
	}
	OnSelchangeAllocTypes() ;
}

void FlightDetailWindow::CheckFlightPtrs(FLIGHTDATA *prpArr, FLIGHTDATA *prpDep)
{
	if(pomAF != prpArr || pomDF != prpDep)
	{
		// force new rotation to be displayed for the primary key - the rotation
		// will not be found this forcing SetFlightPtrs() to get it
		lmFlightSecondaryKey = 0L; 
		SetFlightPtrs();
		Initialize();
	}
}

void FlightDetailWindow::SetFlightPtrs()
{
	pomAF = ogFlightData.GetFlightByUrno(lmFlightPrimaryKey);
	pomDF = ogFlightData.GetFlightByUrno(lmFlightSecondaryKey);

	if(ogFlightData.IsReturnFlight(pomAF) || ogFlightData.IsReturnFlight(pomDF))
	{
		FLIGHTDATA *prlRotation = NULL;
		if(pomAF == NULL)
		{
			if(ogFlightData.IsArrivalOrBoth((prlRotation = ogFlightData.GetRotationFlight(pomDF))))
				pomAF = prlRotation;
		}
		else if(pomDF == NULL)
		{
			if(ogFlightData.IsDepartureOrBoth((prlRotation = ogFlightData.GetRotationFlight(pomAF))))
				pomDF = prlRotation;
		}
	}
	else
	{
		// swap the keys if necessary
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(lmFlightPrimaryKey);
		if(prlFlight == NULL) prlFlight = ogFlightData.GetFlightByUrno(lmFlightSecondaryKey);
		if(prlFlight != NULL)
		{
			pomAF = NULL;
			pomDF = NULL;

			FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight);
			if(ogFlightData.IsArrival(prlFlight))
			{
				pomAF = prlFlight;
				pomDF = prlRotation;
			}
			else
			{
				pomAF = prlRotation;
				pomDF = prlFlight;
			}
		}
	}
	lmFlightPrimaryKey = 0L;
	lmFlightSecondaryKey = 0L;
	if(pomAF != NULL)
	{
		lmFlightPrimaryKey = pomAF->Urno;
		if(pomDF != NULL)
		{
			lmFlightSecondaryKey = pomDF->Urno;
		}
	}
	else if(pomDF != NULL)
	{
		lmFlightPrimaryKey = pomDF->Urno;
	}
}

void FlightDetailWindow::SetCaption()
{
	CString leftCaption, rightCaption;
	CString leftPrm, rightPrm;
	CStringArray olDpxTypes;
	
	leftPrm.Empty();
	rightPrm.Empty();



	if (bgIsPrm && pomAF != NULL)
	{
		ogDpxData.GetDpxTypesByFlight(pomAF->Urno,olDpxTypes,true);
		if (olDpxTypes.GetSize() > 0)
		{
			for (int i = 0; i < olDpxTypes.GetSize(); i++)
			{
				leftPrm.Insert(leftPrm.GetLength(),olDpxTypes[i]);
				leftPrm.Insert(leftPrm.GetLength(),", ");
			}
		}
	}

	if (bgIsPrm && pomDF != NULL)
	{
		ogDpxData.GetDpxTypesByFlight(pomDF->Urno,olDpxTypes,true);
		if (olDpxTypes.GetSize() > 0)
		{
			for (int i = 0; i < olDpxTypes.GetSize(); i++)
			{
				rightPrm.Insert(rightPrm.GetLength(),olDpxTypes[i]);
				rightPrm.Insert(rightPrm.GetLength(),", ");
			}
		}
	}

	if(pomAF != NULL && pomDF != NULL)
	{
		if (!bgIsPrm)
		{
			leftCaption = pomAF->Fnum;
			rightCaption = pomDF->Fnum;
		}
		else
		{
			leftCaption = pomAF->Fnum + CString("   ") + leftPrm;
			rightCaption = pomDF->Fnum + CString("   ") + rightPrm;
		}
		omCaption.Format(GetString(IDS_STRING61539), leftCaption, rightCaption);
	}
	if(pomAF != NULL && pomDF == NULL)
	{
		if (!bgIsPrm)
		{
			leftCaption = pomAF->Fnum;
		}
		else
		{
			leftCaption = pomAF->Fnum + CString("   ") + leftPrm;
		}
		omCaption.Format(GetString(IDS_STRING61538), leftCaption);
	}
	if(pomDF != NULL && pomAF == NULL)
	{
		if (!bgIsPrm)
		{
			rightCaption = pomDF->Fnum;
		}
		else
		{
			rightCaption = pomDF->Fnum + CString("   ") + rightPrm;
		}
		omCaption.Format(GetString(IDS_STRING61538), rightCaption);
	}

	if(omAllocUnitType == ALLOCUNITTYPE_REGN)
	{
		long llRegnUrno = 0L;
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(lmFlightPrimaryKey);
		if(prlFlight != NULL)
		{
			ACRDATA *prlAcr = ogAcrData.GetAcrByName(prlFlight->Regn);
			if(prlAcr != NULL)
			{
				llRegnUrno = prlAcr->Urno;
			}
		}
		if(llRegnUrno == 0L && prmAcr != NULL)
		{
			// this is a regn without a flight
			omCaption.Format("%s",prmAcr->Regn);
		}
	}
}

void FlightDetailWindow::SetGanttTimes(CTime &ropTSStartTime, CTimeSpan &ropTSDuration)
{
	CTime olStartTime = TIMENULL; // time of earliest job/demand begin
	CTime olEndTime = TIMENULL; // time of latest job/demand end

	omViewer.GetGanttTimes(olStartTime, olEndTime);
	if(ogFlightData.IsTurnaround(ogFlightData.GetFlightByUrno(lmFlightPrimaryKey)))
	{
		if (olStartTime == TIMENULL)
		{
			if (pomAF != NULL)
			{
				olStartTime = pomAF->Stoa;
			}
			else
			{
				if (pomDF != NULL)
				{
					olStartTime = pomDF->Stod;
				}
				else
				{
					olStartTime = ogBasicData.GetTime();
				}
			}
		}

		if(olStartTime == TIMENULL)
		{
			olStartTime = CTime::GetCurrentTime();
		}
	
		if (olEndTime == TIMENULL)
		{
			ropTSDuration = (ropTSStartTime + CTimeSpan(0, 3, 0, 0)) - ropTSStartTime;
		}
		else
		{
			ropTSDuration = (olEndTime + CTimeSpan(0, 1, 0, 0)) - ropTSStartTime;
		}
		
	}
	else if (ogFlightData.IsArrival(ogFlightData.GetFlightByUrno(lmFlightPrimaryKey)))
	{
		if (olStartTime == TIMENULL)
		{
			if (pomAF != NULL)
			{
				olStartTime = pomAF->Stoa;
			}
			else
			{
				if (pomDF != NULL)
				{
					olStartTime = pomDF->Stod;
				}
				else
				{
					olStartTime = ogBasicData.GetTime();
				}
			}
		}

		if(olStartTime == TIMENULL)
		{
			olStartTime = CTime::GetCurrentTime();
		}
		
		if(olEndTime == TIMENULL)
		{
			ropTSDuration = (ropTSStartTime + CTimeSpan(0, 3, 0, 0)) - ropTSStartTime;
		}
		else
		{
			ropTSDuration = (olEndTime + CTimeSpan(0, 1, 0, 0)) - ropTSStartTime;
		}
	}
	else
	{
		if (olStartTime == TIMENULL)
		{
			if (pomAF != NULL)
			{
				olStartTime = pomAF->Stoa;
			}
			else
			{
				if (pomDF != NULL)
				{
					olStartTime = pomDF->Stod;
				}
				else
				{
					olStartTime = ogBasicData.GetTime();
				}
			}
		}
		if(olStartTime == TIMENULL)
		{
			olStartTime = CTime::GetCurrentTime();
		}

		if (olEndTime == TIMENULL)
		{
			ropTSDuration = (ropTSStartTime + CTimeSpan(0, 3, 0, 0)) - ropTSStartTime;
		}
		else
		{
			ropTSDuration = (olEndTime + CTimeSpan(0, 1, 0, 0)) - ropTSStartTime;
		}
	}

	// when job change
	if(omViewer.bmJob_Moved_Added_Deleted_Flag)
	{
		if(imUpdatedLine >= 0 && imUpdatedLine < omViewer.omFlightLines.GetSize())
		{
			CTime olTempMinTime = 0,olTempMaxTime = 0;
			FLIGHT_LINEDATA *prlFL = &omViewer.omFlightLines[imUpdatedLine];
			if(prlFL->JobBars.GetSize() > 0)
			{
				olTempMinTime = ((FLIGHT_BARDATA)prlFL->JobBars[0]).StartTime;
				olTempMaxTime = ((FLIGHT_BARDATA)prlFL->JobBars[0]).EndTime;
				if(((olTempMinTime + CTimeSpan(0, 3, 0, 0)) > omViewer.omMaxEndTime) && (omViewer.omMaxEndTime > 0))
				{
					omTSStartTime = omViewer.omMaxEndTime - CTimeSpan(0, 4, 0, 0);
				}
				else
				{
					omTSStartTime = olTempMinTime - CTimeSpan(0, 1, 0, 0);
				}
			}
			else if(prlFL->DemandBars.GetSize() > 0)
			{
				olTempMinTime = ((FLIGHT_BARDATA)prlFL->DemandBars[0]).StartTime;
				olTempMaxTime = ((FLIGHT_BARDATA)prlFL->DemandBars[0]).EndTime;
				if(((olTempMinTime + CTimeSpan(0, 3, 0, 0)) > omViewer.omMaxEndTime) && (omViewer.omMaxEndTime > 0))
				{
					omTSStartTime = omViewer.omMaxEndTime - CTimeSpan(0, 4, 0, 0);
				}
				else
				{
					omTSStartTime = olTempMinTime - CTimeSpan(0, 1, 0, 0);
				}
			}
			else
			{
				if(omViewer.omMinStartTime > 0)
				{
					omTSStartTime = omViewer.omMinStartTime;
				}
			}
		}
	}
	else if(bmSelchange)
	{
		ropTSStartTime = omTSStartTime;
	}
	else
	{
		ropTSStartTime = olStartTime - CTimeSpan(0, 1, 0, 0);
	}
	
	// hardcoded to fixed timeframe
	ropTSDuration = CTimeSpan(0, 4, 0, 0);

}
	
void FlightDetailWindow::Initialize(void)
{
	omViewer.RemoveAll();
	omViewer.omAllocUnitType = omAllocUnitType;
	omViewer.imDemandType = imDemandType;
	omViewer.lmFlightPrimaryKey = lmFlightPrimaryKey;
	omViewer.lmFlightSecondaryKey = lmFlightSecondaryKey;

	omViewer.prmAcr = prmAcr;
	
	//pointer pass from flightplan
	omViewer.pomAF = pomAF;
	omViewer.pomDF = pomDF;

	
	omViewer.SetFunctionCodeMap(pomMapFunctionCode); //PRF 8999
	omViewer.SetEquDemandGroupMap(pomMapEquDemandGropus);
	
	omViewer.Init();

	SetCaption();
	SetGanttTimes(omTSStartTime,omTSDuration);

	omFlightDetail.bmDisplayStaffDemands = (imDemandType & PERSONNELDEMANDS) ? true : false;
	omFlightDetail.bmDisplayEquipmentDemands = (imDemandType & EQUIPMENTDEMANDS) ? true : false;
	omFlightDetail.bmDisplayLocationDemands = (imDemandType & LOCATIONDEMANDS) ? true : false;
    omFlightDetail.SetFlightPtr(pomAF, pomDF, prmAcr);
    omFlightDetail.SetFlightData();


	
	
	if (::IsWindow(omGantt.GetSafeHwnd()))
	{
		omFlightDetail.SetRemarkFields();

		//omGantt.LockWindowUpdate();
		omTimeScale.SetDisplayTimeFrame(omTSStartTime,omTSDuration, omTSInterval);
        omTimeScale.Invalidate(TRUE);
	    omGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());
		omGantt.SetMarkTime(ogBasicData.omTimebandStart, ogBasicData.omTimebandEnd);

		
	
		//omViewer.omFlightLines.GetSize()     number of demands
	
		
		
		for (int ilLineno = 0; ilLineno < omViewer.omFlightLines.GetSize(); ilLineno++)
		{
			omGantt.AddString("");
			omGantt.SetItemHeight(ilLineno, omGantt.GetLineHeight(ilLineno));
		}
	}

	
}

//XXX // ipDemandType - { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
//int FlightDetailWindow::RemoveJobsWithIncorrectType(CCSPtrArray <JOBDATA> &ropJobs, int ipDemandType)
//{
//	int ilNumJobs = ropJobs.GetSize();
//	for(int ilJob = (ilNumJobs-1); ilJob >= 0; ilJob--)
//	{
//		if(!ogBasicData.JobIsOfType(ropJobs[ilJob].Urno, ipDemandType))
//		{
//			ropJobs.RemoveAt(ilJob);
//		}
//	}
//
//	return ropJobs.GetSize();
//}


FlightDetailWindow::~FlightDetailWindow()
{
}

BEGIN_MESSAGE_MAP(FlightDetailWindow, CFrameWnd)
	//{{AFX_MSG_MAP(FlightDetailWindow)
	ON_WM_CREATE()
	ON_WM_GETMINMAXINFO()
	ON_WM_ERASEBKGND()
	ON_WM_DESTROY()
	ON_WM_KEYDOWN()
	ON_WM_SIZE()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// FlightDetailWindow message handlers

int FlightDetailWindow::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

    omFlightDetail.SetFlightPtr(pomAF, pomDF, prmAcr);
//	omFlightDetail.SetJobsPtr(&omJobs);
    omFlightDetail.Create(omFlightDetail.IDD, this);

	m_FlightDetailWindowDragDrop.RegisterTarget(this, this);
    
    CRect olRect;
    omFlightDetail.GetDlgItem(IDC_EINSALZE) -> GetWindowRect(&olRect); 
	ScreenToClient(&olRect);


    imBottomPos = olRect.bottom;
    
    CRect olDRect; omFlightDetail.GetClientRect(&olDRect);
    CRect olWRect; GetWindowRect(&olWRect);

	int ilXSize = ::GetSystemMetrics(SM_CXBORDER) * 2;



	// calculate the default size of window
	CSize olSize(olDRect.Width(),olDRect.Height());	// is dialog size
	olSize.cx += ilXSize;							// + 2 * frame border in x - direction

	olSize.cy += ::GetSystemMetrics(SM_CYBORDER);	// + frame border
	olSize.cy += ::GetSystemMetrics(SM_CYCAPTION);	// + caption
	olSize.cy += ::GetSystemMetrics(SM_CYHSCROLL)*2;// + size for status bar
	olSize.cy += ::GetSystemMetrics(SM_CYBORDER);	// + frame border

	CWnd *polParent = GetParent();
	if (polParent)
	{
		olWRect = CRect(CPoint(0,0),olSize);
		polParent->ScreenToClient(olWRect);
	}
	else
	{
		olWRect = CRect(olWRect.TopLeft(),olSize);
	}
	int ilMaxScreenHeight = ::GetSystemMetrics(SM_CYSCREEN);

//    omMaxTrackSize = CPoint(olWRect.Width(), olWRect.Height());
    omMaxTrackSize = CPoint(olWRect.Width(), ilMaxScreenHeight);

    MoveWindow(olWRect.left, olWRect.top,olWRect.Width(), olWRect.Height(), FALSE);

    CTime olCurrentTime = ogBasicData.GetTime();
    CTime olCT = CTime(
        olCurrentTime.GetYear(), olCurrentTime.GetMonth(), olCurrentTime.GetDay(),
        olCurrentTime.GetHour(), olCurrentTime.GetMinute(), 0
    );

    // PRF 4596
    // start time of the whole gantt chart (ie not just the time window currently displayed)
    if(omViewer.omMinStartTime > 0)
    {
        omStartTime = omViewer.omMinStartTime;
    }

    // duration of the whole gantt chart --> minimum 1 day (+1 minute to stop divide by zero)
    if(omViewer.omMaxEndTime > 0)
    {
        omDuration = omViewer.omMaxEndTime - omStartTime;
    }
    if(omDuration <= omTSDuration)
    {
	    omDuration = omTSDuration;
	    omFlightDetail.m_HScrollBar.EnableWindow(FALSE);
    }

    if((omMarkTimeStart != TIMENULL))
	{
		if((omViewer.omMinStartTime <= omMarkTimeStart) && (omViewer.omMaxEndTime > omMarkTimeStart) && (omViewer.omMinStartTime > 0) && ( omViewer.omMaxEndTime > 0) )
	    {
			if( (omMarkTimeStart + CTimeSpan(0,3,0,0)) > omViewer.omMaxEndTime)
			{
				omTSStartTime = omViewer.omMaxEndTime - CTimeSpan(0,4,0,0);
			}
			else 
			{
				omTSStartTime = omMarkTimeStart - CTimeSpan(0,1,0,0);
			}
		}
    }

    if(omFlightDetail.m_HScrollBar.IsWindowEnabled())
    {
        long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
        long llTotalMin = CalcTotalMinutes();
        omFlightDetail.m_HScrollBar.SetScrollRange(0, 1000, FALSE);
        int ilPos = (int) (1000 * llTSMin / llTotalMin);
        if(ilPos <= 0 )
        {
            ilPos = 0;
            imPrevScrollPos = 0;
        }
        else if(ilPos >= 1000)
        {
            ilPos = 1000;
            imPrevScrollPos = 1000;
        }
        else
        {
            imPrevScrollPos = ilPos;
        }
        omFlightDetail.m_HScrollBar.SetScrollPos(ilPos, FALSE);
    }


    omTSInterval = CTimeSpan(0, 0, 10, 0);
    olRect.InflateRect(-15, -15);
    omTimeScale.EnableDisplayCurrentTime(FALSE);
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,CRect(imStartTimeScalePos+26, olRect.top, olRect.right-10, olRect.top + 34),this, 0, NULL);
    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omGantt.SetMarkTime(ogBasicData.omTimebandStart, ogBasicData.omTimebandEnd);

    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,AFX_IDW_STATUS_BAR);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    omGantt.SetTimeScale(&omTimeScale);
    omGantt.SetViewer(&omViewer, 0 /* GroupNo */);
    omGantt.SetStatusBar(&omStatusBar);
	CTime olDisplayStart = omTSStartTime, olDisplayEnd = omTSStartTime + omTimeScale.GetDisplayDuration();
    omGantt.SetDisplayWindow(olDisplayStart, olDisplayEnd);
    SetTSStartTime(omTSStartTime);
    omGantt.SetVerticalScaleWidth(150);
    omGantt.SetFonts(&ogSmallFonts_Regular_6, &ogSmallFonts_Regular_6);
    //omGantt.SetVerticalScaleColors(NAVY, SILVER, YELLOW, SILVER);
	omGantt.SetGanttChartColors(NAVY, GRAY, YELLOW, GRAY);
    omGantt.Create(0, CRect(olRect.left, olRect.top + 34, olRect.right, olRect.bottom), &omFlightDetail);
	omGantt.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
	//omGantt.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOACTIVATE);
	omViewer.Attach(this);

	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("FLIGHTDW"),	CString("Redisplay"), FlightDetailWindowCf);
	ogCCSDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("FLIGHTDW"),CString("Update Time Band"), FlightDetailWindowCf);
	ogCCSDdx.Register(this, FLIGHT_DELETE, CString("FLIGHTDW"),CString("Flight Delete"), FlightDetailWindowCf);
	ogCCSDdx.Register(this, UPDATE_CONFLICT_SETUP,CString("FLIGHTDW"), CString("Update Conflict Setup"), FlightDetailWindowCf);


	bmInitialized = true;

	// set the dialog to the height saved for this user
	imScreenHeight = 0;
	imPrevScreenHeight = 0;
	CFGDATA *prlCfg = ogCfgData.GetCfgByCtypAndCkey(FDD_KEY, FDD_SCREEN_HEIGHT);
	if(prlCfg != NULL)
	{
		imScreenHeight = atoi(prlCfg->Text);
		imPrevScreenHeight = imScreenHeight;
	}
	if(imScreenHeight > 50 && imScreenHeight <= ilMaxScreenHeight)
	{
		GetWindowRect(&olRect); 
		ScreenToClient(&olRect);
		olRect.bottom = olRect.top + imScreenHeight;
		MoveWindow(olRect, FALSE);
	}


	// for departures may need to scroll down to display the demands otherwise
	int ilTopPos = omViewer.GetFirstDisplayLine(olDisplayStart, olDisplayEnd); 
	int ilNumLines = omGantt.GetCount();
	if(ilTopPos >= ilNumLines)
		ilTopPos = ilNumLines - 1;
	if(ilTopPos > 0)
		omGantt.SetTopIndex(ilTopPos);
	
	return 0;
}

void FlightDetailWindow::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	// Unregister DDX call back function
	TRACE("FlightDetailWindow: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	m_FlightDetailWindowDragDrop.Revoke();

	if(imScreenHeight != imPrevScreenHeight && imScreenHeight > 50 && imScreenHeight <= ::GetSystemMetrics(SM_CYSCREEN))
	{
		CString olScreenHeight;
		olScreenHeight.Format("%d", imScreenHeight);
		ogCfgData.DeleteByCtyp(FDD_KEY);
		ogCfgData.CreateCfg(FDD_KEY, FDD_SCREEN_HEIGHT, olScreenHeight);
	}

	CFrameWnd::OnDestroy();
}

void FlightDetailWindow::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);
    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    omMinTrackSize.x = omMaxTrackSize.x;  // fix the frame window width
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL FlightDetailWindow::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
    CRect olRect; GetClientRect(&olRect);
    olRect.top = imBottomPos;
    CBrush olBrush(SILVER);
    pDC->FillRect(&olRect, &olBrush);

    return TRUE;
}

void FlightDetailWindow::OnSelchangeAllocTypes() 
{
	

	
	
	CComboBox *polAllocTypes = static_cast<CComboBox *>(omFlightDetail.GetDlgItem(IDC_ALLOCTYPES));
	CButton *polStaffDemandsButton = static_cast<CButton *>(omFlightDetail.GetDlgItem(IDC_STAFFDEMANDS));
	CButton *polEquipmentDemandsButton = static_cast<CButton *>(omFlightDetail.GetDlgItem(IDC_EQUIPMENTDEMANDS));
	CButton *polLocationDemandsButton = static_cast<CButton *>(omFlightDetail.GetDlgItem(IDC_LOCATIONDEMANDS));
	CButton *polSortButton = static_cast<CButton *>(omFlightDetail.GetDlgItem(IDC_SORTASRULE));
	if (polAllocTypes && polEquipmentDemandsButton && polStaffDemandsButton && polLocationDemandsButton && polSortButton)
	{
		int ilIndex = polAllocTypes->GetCurSel();
		if (ilIndex != CB_ERR)
		{
			int ilOldDemandType = imDemandType;
			imDemandType = NODEMANDS; // init to 000
		
			if(polStaffDemandsButton->GetCheck() == 1)
			{
				imDemandType |= PERSONNELDEMANDS;
			}
			if(polEquipmentDemandsButton->GetCheck() == 1)
			{
				imDemandType |= EQUIPMENTDEMANDS;
			}
			if(polLocationDemandsButton->GetCheck() == 1)
			{
				imDemandType |= LOCATIONDEMANDS;
			}

			bool blOldUseDemandSortValues = omViewer.bmUseDemandSortValues;
			omViewer.bmUseDemandSortValues = (polSortButton->GetCheck() == 1) ? true : false;

			bool blInitialize = false;
			
			CString olAllocType = omAllocTypes[polAllocTypes->GetItemData(ilIndex)];
			if (olAllocType == GetString(IDS_ALLSTRING))
			{
				if (omAllocUnitType != "" || imDemandType != ilOldDemandType)
				{
					omAllocUnitType = "";
					blInitialize = true;
				}
			}
			else if (olAllocType != omAllocUnitType || imDemandType != ilOldDemandType)
			{
				omAllocUnitType = olAllocType;
				blInitialize = true;
			}
			
			if(blOldUseDemandSortValues != omViewer.bmUseDemandSortValues)
			{
				blInitialize = true;
			}
            
			if(blInitialize)
			{
				bmSelchange = TRUE;
				Initialize();
				bmSelchange = FALSE;
			}

		}

		// enable scrollbar
		omStartTime = omViewer.omMinStartTime;
		omDuration = omViewer.omMaxEndTime - omStartTime;
		if(omDuration > omTSDuration)
		{
			omFlightDetail.m_HScrollBar.EnableWindow(TRUE);
		}
		else
		{
   			omFlightDetail.m_HScrollBar.EnableWindow(FALSE);
		}

		if(omFlightDetail.m_HScrollBar.IsWindowEnabled())
		{
			long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
			long llTotalMin = CalcTotalMinutes();
			omFlightDetail.m_HScrollBar.SetScrollRange(0, 1000, FALSE);
			int ilPos = (int) (1000 * llTSMin / llTotalMin);
			if(ilPos <= 0 )
			{
				ilPos = 0;
				imPrevScrollPos = 0;
			}
			else if(ilPos >= 1000)
			{
				ilPos = 1000;
				imPrevScrollPos = 1000;
			}
			else
			{
				imPrevScrollPos = ilPos;
			}
			omFlightDetail.m_HScrollBar.SetScrollPos(ilPos, TRUE);
		}
	}
}

CString FlightDetailWindow::GetCurrentAllocType()
{
	CString olAllocType;	
	CComboBox *polAllocTypes = static_cast<CComboBox *>(omFlightDetail.GetDlgItem(IDC_ALLOCTYPES));
	if (polAllocTypes)
	{
		int ilIndex = polAllocTypes->GetCurSel();
		if (ilIndex != CB_ERR)
		{
			olAllocType = omAllocTypes[polAllocTypes->GetItemData(ilIndex)];
		}
	}

	return olAllocType;
}


bool FlightDetailWindow::HasTurnaroundDemands()
{
	bool blHasTurnaroundDemands = false;

	int ilNumDems = omDemands.GetSize();
	for(int ilD = 0; ilD < ilNumDems; ilD++)
	{
		if(ogDemandData.IsTurnaroundDemand(&omDemands[ilD]))
		{
			blHasTurnaroundDemands = true;
			break;
		}
	}

	return blHasTurnaroundDemands;
}

////////////////////////////////////////////////////////////////////////
// FlightDetailWindow -- implementation of yellow vertical time band lines

static void FlightDetailWindowCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	FlightDetailWindow *polDetailWindow = (FlightDetailWindow *)popInstance;

	if(ipDDXType == FLIGHT_DELETE)
	{
		polDetailWindow->ProcessFlightDelete((long)vpDataPointer);
	}
	else if(ipDDXType == STAFFDIAGRAM_UPDATETIMEBAND)
	{
		TIMEPACKET *polTimePacket = (TIMEPACKET *)vpDataPointer;
		if (::IsWindow(polDetailWindow->omGantt.GetSafeHwnd()))	// the window is still opened?
		{
			polDetailWindow->omGantt.SetMarkTime(polTimePacket->StartTime, polTimePacket->EndTime);
		}
	}
	else if(ipDDXType == REDISPLAY_ALL || ipDDXType == UPDATE_CONFLICT_SETUP)
	{
		polDetailWindow->ProcessRefresh();
	}
}

void FlightDetailWindow::ProcessRefresh(void)
{
	Initialize();
}

void FlightDetailWindow::ProcessFlightDelete(long lpFlightUrno)
{
	if(lpFlightUrno == lmFlightPrimaryKey || lpFlightUrno == lmFlightSecondaryKey)
	{
		CFrameWnd::OnClose();
	}
}

void FlightDetailWindow::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch (nChar)
	{
	case VK_ESCAPE:
	    CFrameWnd::OnClose();
		break;
	default:
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void FlightDetailWindow::UpdateGanttChart(int ipType, int ipLine, int ipBar)
{
	switch(ipType)
	{
        case UD_INSERTLINE:
		{
            omGantt.InsertString(ipLine, "");
			omGantt.RepaintItemHeight(ipLine);
	        break;
		}
        case UD_UPDATELINE:
		{
	        omGantt.RepaintVerticalScale(ipLine);
		    omGantt.RepaintGanttChart(ipLine);
	        break;
		}
        case UD_DELETELINE:
		{
			int ilTopPos = omGantt.GetTopIndex();		
            if(omGantt.DeleteString(ipLine) != LB_ERR)
			{
				int ilNumLines = omGantt.GetCount();
				if(ilTopPos >= ilNumLines)
				{
					ilTopPos = ilNumLines - 1;
				}
				omGantt.SetTopIndex(ilTopPos);
			}
	        break;
		}
        case UD_UPDATELINEHEIGHT:
		{
			omGantt.RepaintVerticalScale(ipLine);
			omGantt.RepaintItemHeight(ipLine);
	        break;
		}
	}

    CTime olTSStartTime;
    CTimeSpan olTSDuration;
	CTime olPrevTSStartTime = omTSStartTime;
	omViewer.CaleculateMinMaxDisplayTime();
	SetGanttTimes(olTSStartTime, olTSDuration);
	if(olTSStartTime != omTSStartTime || olTSDuration != omTSDuration)
	{	
		imUpdatedLine = ipLine;
		SetGanttTimes(omTSStartTime, omTSDuration);
		// To avoid gantt movement at broadcast reception
		if( omGantt.pmBroadCastInstanceCaller != omViewer.pmBroadCastInstanceCallerFromFDViewer)
		{
			if((omViewer.omMinStartTime >= olPrevTSStartTime ) && (omViewer.omMinStartTime > 0))
			{
				omTSStartTime = omViewer.omMinStartTime;
			}
			else if(((omViewer.omMaxEndTime - CTimeSpan(0,4,0,0)) < olPrevTSStartTime) && (omViewer.omMaxEndTime > 0))
			{
				omTSStartTime = omViewer.omMaxEndTime - CTimeSpan(0,4,0,0);
			}
			else if((omViewer.omMinStartTime < olPrevTSStartTime) && (omViewer.omMinStartTime > 0))
			{
				omTSStartTime = olPrevTSStartTime;
			}
		}
		omGantt.pmBroadCastInstanceCaller = NULL;

		omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
		omTimeScale.Invalidate(TRUE);

		SetTSStartTime(omTSStartTime);
		if(omViewer.omMinStartTime > 0)
		{
			omStartTime = omViewer.omMinStartTime;
		}
		if(omViewer.omMaxEndTime > 0)
		{
			omDuration  = omViewer.omMaxEndTime - omStartTime;
		}

		// enable scrollbar
		if(omDuration > omTSDuration)
		{
			omFlightDetail.m_HScrollBar.EnableWindow(TRUE);
		}
		else
		{
   			omFlightDetail.m_HScrollBar.EnableWindow(FALSE);
		}

		if(omFlightDetail.m_HScrollBar.IsWindowEnabled())
		{
			long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
			long llTotalMin = CalcTotalMinutes();
			omFlightDetail.m_HScrollBar.SetScrollRange(0, 1000, FALSE);
			int ilPos = (int) (1000 * llTSMin / llTotalMin);
			if(ilPos <= 0 )
			{
				ilPos = 0;
				imPrevScrollPos = 0;
			}
			else if(ilPos >= 1000)
			{
				ilPos = 1000;
				imPrevScrollPos = 1000;
			}
			else
			{
				imPrevScrollPos = ilPos;
			}
			omFlightDetail.m_HScrollBar.SetScrollPos((int) (1000 * llTSMin / llTotalMin), TRUE);
		}

		omGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());
		omGantt.Invalidate(TRUE);
		omGantt.RepaintGanttChart(-1,omTSStartTime, omTSStartTime+omTSDuration, TRUE);
//		// QUICK FIX: without the following, RepaintGanttChart doesn't work properly, don't know why
//		MSG olMsg;
//		while(::PeekMessage(&olMsg,NULL,0,0,FALSE))
//		{
//			::GetMessage(&olMsg,NULL,0,0);
//			::TranslateMessage(&olMsg);
//			::DispatchMessage(&olMsg);
//		}
	}
}

void FlightDetailWindow::GetDisplayedDemands(CCSPtrArray <DEMANDDATA> &ropDemands)
{
	omViewer.GetDisplayedDemands(ropDemands);
}

void FlightDetailWindow::GetDisplayedJobsWithoutDemands(CCSPtrArray <JOBDATA> &ropJobsWithoutDemands)
{
	omViewer.GetJobsWithoutDemands(ropJobsWithoutDemands);
}


void FlightDetailWindow::OnSize(UINT nType, int cx, int cy) 
{
	CFrameWnd::OnSize(nType, cx, cy);
	
	if(bmInitialized)
	{
		CRect olRect;
		omFlightDetail.GetWindowRect(&olRect);
		ScreenToClient(&olRect);
		olRect.bottom = cy - 18;
		omFlightDetail.MoveWindow(olRect, TRUE);

		CWnd *polBorder = omFlightDetail.GetDlgItem(IDC_EINSALZE);
		if(polBorder != NULL)
		{
			polBorder->GetWindowRect(&olRect); 
			ScreenToClient(&olRect);
			olRect.bottom = cy - 35;
			polBorder->MoveWindow(olRect, TRUE);
		}

		omGantt.GetWindowRect(&olRect); 
		ScreenToClient(&olRect);
		olRect.bottom = cy - 40;
		omGantt.MoveWindow(olRect, TRUE);
		
		GetWindowRect(&olRect);
		ScreenToClient(&olRect);
		imScreenHeight = olRect.bottom - olRect.top;

		int ilcy = cy-36;
		omFlightDetail.m_HScrollBar.SetWindowPos(NULL,172,ilcy,NULL,NULL,TRUE);
		omFlightDetail.m_HScrollBar.Invalidate(TRUE);

    }
}

LONG FlightDetailWindow::OnDragOver(UINT wParam, LONG lParam)
{
	LONG llRc = -1L;

	if((CWnd *)lParam == this)
	{
		AutoScroll(750);
	}
	else
	{
		AutoScroll(); // end scrolling
	}

	return llRc;
}

// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void FlightDetailWindow::AutoScroll(UINT ipInitialScrollSpeed /*200*/)
{
	// if not already scrolling automatically...
	if(!bmScrolling)
	{
		// ... after a short pause (ipInitialScrollSpeed), start scrolling
		bmScrolling = true;
		imScrollSpeed = ipInitialScrollSpeed;
		SetTimer(1, (UINT) imScrollSpeed, NULL);
	}
}

// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void FlightDetailWindow::OnAutoScroll(void)
{
	bmScrolling = false;

	// if standard cursor then not currently dragging
	if(GetCursor() != AfxGetApp()->LoadStandardCursor(IDC_ARROW))
	{
		// check if the cursor is in the automatic scrolling region
		CPoint olPoint;
		::GetCursorPos(&olPoint);

		CRect olGanttRect;
		omGantt.GetWindowRect(&olGanttRect);

		if(olPoint.y <= olGanttRect.top && olPoint.y > (olGanttRect.top - 30))
		{
			// above the gantt so scroll up
			omGantt.SetTopIndex(omGantt.GetTopIndex()-1);
			bmScrolling = true;
		}
		else if(olPoint.y >= olGanttRect.bottom && olPoint.y < (olGanttRect.bottom + 30))
		{
			// below the gantt so scroll down
			omGantt.SetTopIndex(omGantt.GetTopIndex()+1);
			bmScrolling = true;
		}
	}

	if(bmScrolling)
	{
		if(imScrollSpeed != 50)
		{
			imScrollSpeed = 50;
			SetTimer(1, (UINT) 50, NULL);
		}
	}
	else
	{
		// no longer in the scrolling region or left mouse button released
		KillTimer(1);
	}
}

void FlightDetailWindow::OnTimer(UINT nIDEvent)
{
	if(nIDEvent == 1)
	{
		// called during drag&drop, does automatic scrolling when the cursor
		// is outside the main gantt chart
		OnAutoScroll();
		CFrameWnd::OnTimer(nIDEvent);
	}
}

void FlightDetailWindow::OnHorizScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
    int ilPos = 0;
    long llTotalMin = 0;
    switch (nSBCode)
    {
        case SB_LINELEFT:	// scroll one hour left
            ilPos = omFlightDetail.m_HScrollBar.GetScrollPos() - int (60 * 1000L / CalcTotalMinutes());
            if(!((imPrevScrollPos == 0) && ( ilPos < 0)))
            {
	            if(ilPos < 0)
		            imPrevScrollPos = 0;
	            else
		            imPrevScrollPos = ilPos;
            }
            else
            {
	            return;
            }

            if (ilPos <= 0)
            {   
	            ilPos = 0;
	            imPrevScrollPos = 0;
	            SetTSStartTime(omStartTime);
            }
            else
            {
	            SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));
            }

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
            omFlightDetail.m_HScrollBar.SetScrollPos(ilPos, TRUE);
            omGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());
            omGantt.Invalidate(TRUE);
            break;

        case SB_LINERIGHT:		// scroll one hour right
            ilPos = omFlightDetail.m_HScrollBar.GetScrollPos() + int (60 * 1000L / CalcTotalMinutes());
            if(!((imPrevScrollPos == 1000) && ( ilPos > 1000)))
            {
	            if(ilPos > 1000)
		            imPrevScrollPos = 1000;
	            else
		            imPrevScrollPos = ilPos;
            }
            else
            {
	            return;
            }

            if (ilPos >= 1000)
            {
	            ilPos = 1000;
	            imPrevScrollPos = 1000;
	            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (CalcTotalMinutes()), 0));
            }
            else
            {
	            SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));
            }

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
            omFlightDetail.m_HScrollBar.SetScrollPos(ilPos, TRUE);
            omGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());
            omGantt.Invalidate(TRUE);
            break;

        case SB_PAGELEFT:
            ilPos = omFlightDetail.m_HScrollBar.GetScrollPos() - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / CalcTotalMinutes());
            if (ilPos <= 0)
            {
	            ilPos = 0;
	            imPrevScrollPos = 0;
	            SetTSStartTime(omStartTime);
            }
            else
            {
	            imPrevScrollPos = ilPos;
	            SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
            }
            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
            omFlightDetail.m_HScrollBar.SetScrollPos(ilPos, TRUE);
            omGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());
            omGantt.Invalidate(TRUE);
            break;

        case SB_PAGERIGHT:
            ilPos = omFlightDetail.m_HScrollBar.GetScrollPos() + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / CalcTotalMinutes());
            if (ilPos >= 1000)
            {
	            ilPos = 1000;
	            imPrevScrollPos = 1000;
	            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (CalcTotalMinutes()), 0));
            }
            else
            {
	            imPrevScrollPos = ilPos; 
	            SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
            }
            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
            omFlightDetail.m_HScrollBar.SetScrollPos(ilPos, TRUE);
            omGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());
            omGantt.Invalidate(TRUE);
            break;

        case SB_THUMBTRACK:
            if(nPos > 1000)
            {
                nPos = 1000;
                imPrevScrollPos = 1000;
            }
            else if(nPos < 0)
            {
                nPos = 0;
                imPrevScrollPos = 0;
            }
            else
            {
                imPrevScrollPos = nPos;;
            }

            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (CalcTotalMinutes() * nPos / 1000), 0));
            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
            omFlightDetail.m_HScrollBar.SetScrollPos(nPos, TRUE);
            omGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());
            omGantt.Invalidate(TRUE);
            break;

        case SB_THUMBPOSITION:	// the thumb was just released?
	        return;
        case SB_ENDSCROLL :
	        break;
	}
}

long FlightDetailWindow::CalcTotalMinutes(void)
{
    omViewer.CaleculateMinMaxDisplayTime();
    if((omViewer.omMaxEndTime > 0) && (omViewer.omMinStartTime > 0) && (omViewer.omMaxEndTime > omViewer.omMinStartTime))
    omDuration = omViewer.omMaxEndTime - omViewer.omMinStartTime ;
    long llTotalMinutes = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
    if(llTotalMinutes <= 0)
    {
	    llTotalMinutes = 1; // prevent divide by zero error
    }

    return llTotalMinutes;
}

void FlightDetailWindow::SetTSStartTime(CTime opNewTsStartTime)
{
    if(opNewTsStartTime >= 0)
    {
        CString opDay = opNewTsStartTime.Format("%d/%m/%Y");
        omFlightDetail.GetDlgItem(IDC_EINSALZE)->SetWindowText(opDay);
        omTSStartTime = opNewTsStartTime;
    }
}
