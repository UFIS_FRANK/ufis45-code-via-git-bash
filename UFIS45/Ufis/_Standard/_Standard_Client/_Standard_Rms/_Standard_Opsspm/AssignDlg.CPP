// dassign.cpp : implementation file
// 

#include <stdafx.h>
#include <OpssPm.h>
#include <CCSTime.h>
#include <AssignDlg.h>
#include <CCSGlobl.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAssignmentDialog dialog

CAssignmentDialog::CAssignmentDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CAssignmentDialog::IDD, pParent)
{
	bmHalfWidth = FALSE;
	omStartTime = ogBasicData.GetTime();
	omEndTime = ogBasicData.GetTime();
	omStaffList.RemoveAll();
	omSelectedItems.RemoveAll();

	//{{AFX_DATA_INIT(CAssignmentDialog)
	m_JobType = -1;
	m_Description = _T("");
	m_StartDate = NULL;
	m_StartTime = NULL;
	m_EndDate = NULL;
	m_EndTime = NULL;
	//}}AFX_DATA_INIT
}

void CAssignmentDialog::DoDataExchange(CDataExchange* pDX)
{
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		m_StartDate = omStartTime;
		m_StartTime = omStartTime;
		m_EndDate = omEndTime;
		m_EndTime = omEndTime;
	}

	// Standard data exchange (provided by ClassWizard)
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAssignmentDialog)
	DDX_Radio(pDX, RADIOFLUG, m_JobType);
	DDX_Text(pDX, IDC_DESCRIPTION, m_Description);
	DDV_MaxChars(pDX, m_Description, 30);
	DDX_CCSddmmyy(pDX, IDC_ENDDATE, m_EndDate);
	DDX_CCSTime(pDX, IDC_ENDTIMES, m_EndTime);
	DDX_CCSddmmyy(pDX, IDC_STARTDATE, m_StartDate);
	DDX_CCSTime(pDX, IDC_STARTTIMES, m_StartTime);
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP

	// Extended data exchange -- for member variables with Control type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		m_List.ResetContent();
		for (int i = 0; i < omStaffList.GetSize(); i++)
			m_List.AddString(omStaffList[i]);

		if (omStaffList.GetSize() == 1)
			m_List.SetSel(0, TRUE);
		else
			m_List.SelItemRange(TRUE, 0, m_List.GetCount()-1);
	}

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		// Convert the date and time back
		omStartTime = CTime(m_StartDate.GetYear(), m_StartDate.GetMonth(),
			m_StartDate.GetDay(), m_StartTime.GetHour(), m_StartTime.GetMinute(),
			m_StartTime.GetSecond());
		omEndTime = CTime(m_EndDate.GetYear(), m_EndDate.GetMonth(),
			m_EndDate.GetDay(),	m_EndTime.GetHour(), m_EndTime.GetMinute(),
			m_EndTime.GetSecond());
		if (omEndTime <= omStartTime)	// don't allow end time greater than start time
		{
			//CString prompt = "Ung�ltige Zeit";
			CString prompt = GetString(IDS_STRING61213);
			MessageBox(prompt, NULL, MB_ICONEXCLAMATION);
			pDX->PrepareEditCtrl(IDC_ENDTIMES);
			pDX->Fail();
		}

		// Copy the selected staff back for the caller.
		// The caller could use the "omSelectedItems" and "omStaffList" to get the result
		int n = m_List.GetSelCount();
		if(n > 0)
		{
			int *itemId = new int[n];
			m_List.GetSelItems(n, itemId);
			omStaffList.RemoveAll();
			omSelectedItems.RemoveAll();
			for (int i = 0; i < n; i++)
			{
				CString s;
				m_List.GetText(itemId[i], s);
				omStaffList.Add(s);
				omSelectedItems.Add(itemId[i]);
			}
			delete itemId;
		}
	}
}


BEGIN_MESSAGE_MAP(CAssignmentDialog, CDialog)
	//{{AFX_MSG_MAP(CAssignmentDialog)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAssignmentDialog message handlers

BOOL CAssignmentDialog::OnInitDialog() 
{
	SetWindowText(GetString(IDS_STRING32837)); 

	CWnd *polWnd = GetDlgItem(IDC_ALLOCATION); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32838));
	}
	polWnd = GetDlgItem(RADIOFLUG); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32839));
	}
	polWnd = GetDlgItem(RADIOCCIDESK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32840));
	}
	polWnd = GetDlgItem(RADIOPOOL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32841));
	}
	polWnd = GetDlgItem(RADIOGATE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32842));
	}
	polWnd = GetDlgItem(RADIOGATEBEREICH); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32843));
	}
	polWnd = GetDlgItem(RADIOSONSTINGES); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32844));
	}
	polWnd = GetDlgItem(IDC_FROM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32845));
	}
	polWnd = GetDlgItem(IDC_TO); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32846));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}
	polWnd = GetDlgItem(IDC_EMPLOYEE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32847));
	}

	// Truncate to 10 characters
	m_Description = m_Description.Left(10);

	// Round times to 5 minutes
	int ilRoundedMinute;
	ilRoundedMinute = (omStartTime.GetMinute() + 2) / 5 * 5;
	omStartTime += CTimeSpan(0, 0, ilRoundedMinute - omStartTime.GetMinute(), 0);
	ilRoundedMinute = (omEndTime.GetMinute()/* + 2*/) / 5 * 5;
	omEndTime += CTimeSpan(0, 0, ilRoundedMinute - omEndTime.GetMinute(), 0);

	// InitDialog and prepare data member for moving the description field
	CDialog::OnInitDialog();
	CRect rcFirstRadio;
	GetDlgItem(FIRSTRADIO)->GetWindowRect(rcFirstRadio);
	ScreenToClient(rcFirstRadio);
	m_yTopOfFirstRadio = rcFirstRadio.top;
	GetDlgItem(IDC_DESCRIPTION)->GetWindowRect(m_rcDescription);
	ScreenToClient(m_rcDescription);
	SetEditControlPos();

	// Check if we need to remove the right part of the dialog (staff listbox)
	if (bmHalfWidth || omStaffList.GetSize() <= 1)
	{
		CRect rect;
		GetWindowRect(rect);
		ScreenToClient(rect);
		rect.right -= 300;
	    MoveWindow(rect, FALSE);
		GetDlgItem(IDC_LIST)->EnableWindow(FALSE);	// disable list box also

		CString olWindowTitle;
		GetWindowText(olWindowTitle);
		SetWindowText(olWindowTitle +
			(omFirstStaffName.IsEmpty()? "": CString(" - ") + omFirstStaffName));
	}
	CenterWindow();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CAssignmentDialog::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	if (HIWORD(wParam) == BN_CLICKED)
		SetEditControlPos();
	
	return CDialog::OnCommand(wParam, lParam);
}

/////////////////////////////////////////////////////////////////////////////
// CAssignmentDialog -- helper routines

void CAssignmentDialog::SetEditControlPos()
{
	int nRadio = GetCheckedRadioButton(FIRSTRADIO, LASTRADIO);
	if (nRadio == 0)
		GetDlgItem(IDC_DESCRIPTION)->ShowWindow(SW_HIDE);
	else
	{
		CRect rcRadio;
		GetDlgItem(nRadio)->GetWindowRect(rcRadio);
		ScreenToClient(rcRadio);
		CRect rcDescription = m_rcDescription;
		rcDescription.OffsetRect(0, rcRadio.top - m_yTopOfFirstRadio);
		GetDlgItem(IDC_DESCRIPTION)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_DESCRIPTION)->MoveWindow(rcDescription);
		GetDlgItem(IDC_DESCRIPTION)->SetFocus();
	}
}
