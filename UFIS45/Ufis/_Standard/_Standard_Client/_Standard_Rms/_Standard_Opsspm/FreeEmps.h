// FreeEmps.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FreeEmps dialog

#ifndef _FREE_EMPSDLG_
#define _FREE_EMPSDLG_

#include <FreeEmpViewer.h>

class FreeEmps : public CDialog
{
// Construction
public:
	FreeEmps(CWnd* pParent=NULL, CCSPtrArray<FREEEMPS_LINEDATA> *ropEmps=NULL, 
			 CRect *popParentRect=NULL, CString opCaller = CString(""), FREEEMPS_ADDITIONALS *prpAddi = NULL);   // standard constructor
	~FreeEmps();
	CCSPtrArray<FREEEMPS_LINEDATA> *pomFreeEmps;
	CDWordArray omDutyUrnos;
	void UpdateComboBox();

private:
	CTable *pomFreeEmpTable;
	CTime omTimeFrom;
	CTime omTimeTo;
	CRect *pomParentRect;
	CString omCaller;
	FREEEMPS_ADDITIONALS rmAddi;

	FreeEmpViewer *pomViewer;
	CCSDragDropCtrl omDragDropObject;

public:
// Dialog Data
	//{{AFX_DATA(FreeEmps)
	enum { IDD = IDD_FREE_EMPS };
		// NOTE: the ClassWizard will add data members here
	BOOL	bmTeamAlloc;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FreeEmps)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FreeEmps)
	virtual BOOL OnInitDialog();
    afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
	virtual void OnOK();
	afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void UpdateView();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
  
#endif // _FREE_EMPSDLG_
