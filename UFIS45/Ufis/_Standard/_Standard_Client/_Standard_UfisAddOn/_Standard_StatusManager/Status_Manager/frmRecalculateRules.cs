using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;

namespace Ufis.Status_Manager.UI
{
	/// <summary>
	/// Summary description for frmRecalculateRules.
	/// </summary>
	public class frmRecalculateRules : System.Windows.Forms.Form
	{
		public System.Windows.Forms.DateTimePicker dtTo;
		public System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.TextBox txtALC;
		private System.Windows.Forms.TextBox txtFLNO;
		private System.Windows.Forms.TextBox txtORIG;
		private System.Windows.Forms.TextBox txtDEST;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblFrom;
		private System.Windows.Forms.Label lblTo;
		private System.Windows.Forms.Label lblFlno;
		private System.Windows.Forms.Label lblOrig;
		private System.Windows.Forms.Label lblDest;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.TextBox txtSection;
		private System.Windows.Forms.Button btnALT;
		private System.Windows.Forms.TextBox txtHSSU;
		private System.Windows.Forms.Label lblSection;
		private System.Windows.Forms.TextBox txtSuffix;
		private System.Windows.Forms.ImageList imageList1;
        private ImageList imageList2;
		private System.ComponentModel.IContainer components;

		public frmRecalculateRules()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRecalculateRules));
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtALC = new System.Windows.Forms.TextBox();
            this.txtFLNO = new System.Windows.Forms.TextBox();
            this.txtORIG = new System.Windows.Forms.TextBox();
            this.txtDEST = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFlno = new System.Windows.Forms.Label();
            this.lblOrig = new System.Windows.Forms.Label();
            this.lblDest = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtSection = new System.Windows.Forms.TextBox();
            this.btnALT = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.txtHSSU = new System.Windows.Forms.TextBox();
            this.lblSection = new System.Windows.Forms.Label();
            this.txtSuffix = new System.Windows.Forms.TextBox();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            resources.ApplyResources(this.dtTo, "dtTo");
            this.dtTo.Name = "dtTo";
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            resources.ApplyResources(this.dtFrom, "dtFrom");
            this.dtFrom.Name = "dtFrom";
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // txtALC
            // 
            this.txtALC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            resources.ApplyResources(this.txtALC, "txtALC");
            this.txtALC.Name = "txtALC";
            // 
            // txtFLNO
            // 
            resources.ApplyResources(this.txtFLNO, "txtFLNO");
            this.txtFLNO.Name = "txtFLNO";
            // 
            // txtORIG
            // 
            this.txtORIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            resources.ApplyResources(this.txtORIG, "txtORIG");
            this.txtORIG.Name = "txtORIG";
            // 
            // txtDEST
            // 
            this.txtDEST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            resources.ApplyResources(this.txtDEST, "txtDEST");
            this.txtDEST.Name = "txtDEST";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Name = "label1";
            // 
            // lblFrom
            // 
            this.lblFrom.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblFrom, "lblFrom");
            this.lblFrom.Name = "lblFrom";
            // 
            // lblTo
            // 
            this.lblTo.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblTo, "lblTo");
            this.lblTo.Name = "lblTo";
            // 
            // lblFlno
            // 
            this.lblFlno.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblFlno, "lblFlno");
            this.lblFlno.Name = "lblFlno";
            // 
            // lblOrig
            // 
            this.lblOrig.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblOrig, "lblOrig");
            this.lblOrig.Name = "lblOrig";
            // 
            // lblDest
            // 
            this.lblDest.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblDest, "lblDest");
            this.lblDest.Name = "lblDest";
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.Transparent;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // txtSection
            // 
            this.txtSection.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtSection, "txtSection");
            this.txtSection.Name = "txtSection";
            this.txtSection.ReadOnly = true;
            // 
            // btnALT
            // 
            this.btnALT.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.btnALT, "btnALT");
            this.btnALT.ImageList = this.imageList1;
            this.btnALT.Name = "btnALT";
            this.btnALT.UseVisualStyleBackColor = false;
            this.btnALT.Click += new System.EventHandler(this.btnALT_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.White;
            this.imageList1.Images.SetKeyName(0, "");
            // 
            // txtHSSU
            // 
            this.txtHSSU.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtHSSU, "txtHSSU");
            this.txtHSSU.Name = "txtHSSU";
            this.txtHSSU.ReadOnly = true;
            // 
            // lblSection
            // 
            this.lblSection.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblSection, "lblSection");
            this.lblSection.Name = "lblSection";
            // 
            // txtSuffix
            // 
            this.txtSuffix.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            resources.ApplyResources(this.txtSuffix, "txtSuffix");
            this.txtSuffix.Name = "txtSuffix";
            // 
            // imageList2
            // 
            this.imageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            resources.ApplyResources(this.imageList2, "imageList2");
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // frmRecalculateRules
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.txtSuffix);
            this.Controls.Add(this.txtHSSU);
            this.Controls.Add(this.txtSection);
            this.Controls.Add(this.txtDEST);
            this.Controls.Add(this.txtORIG);
            this.Controls.Add(this.txtFLNO);
            this.Controls.Add(this.txtALC);
            this.Controls.Add(this.lblSection);
            this.Controls.Add(this.btnALT);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblDest);
            this.Controls.Add(this.lblOrig);
            this.Controls.Add(this.lblFlno);
            this.Controls.Add(this.lblTo);
            this.Controls.Add(this.lblFrom);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtTo);
            this.Controls.Add(this.dtFrom);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frmRecalculateRules";
            this.Load += new System.EventHandler(this.frmRecalculateRules_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmRecalculateRules_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke, Color.Gray);			
		}

		private void frmRecalculateRules_Load(object sender, System.EventArgs e)
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy";// - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";

			lblFrom.Parent = pictureBox1;
			lblTo.Parent = pictureBox1;
			lblFlno.Parent = pictureBox1;
			lblOrig.Parent = pictureBox1;
			lblDest.Parent = pictureBox1;
			lblSection.Parent = pictureBox1;

		}

		private void btnALT_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.lblCaption.Text = "Status Sections";
			olDlg.currTable = "HSS";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtHSSU.Text = strUrno;
				txtSection.Text = olDlg.tabList.GetColumnValue(sel,1);
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strMessage="";
			strMessage = Check();
			if(strMessage != "")
			{
				MessageBox.Show(this, strMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}
		private string  Check()
		{
			string strMessage="";
			string strValue = txtALC.Text;
			IDatabase myDB = UT.GetMemDB();
			if(strValue != "")
			{
				ITable myTab = myDB["ALT"];
				bool blFound = false;
				for(int i = 0; i < myTab.Count && blFound == false; i++)
				{
					if(strValue == myTab[i]["ALC2"] || strValue == myTab[i]["ALC3"])
					{
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strMessage += "Airline code <" + strValue + "> does not exist\n";
				}
			}
			strValue = txtORIG.Text;
			if(strValue != "")
			{
				ITable myTab = myDB["APT"];
				bool blFound = false;
				for(int i = 0; i < myTab.Count && blFound == false; i++)
				{
					if(strValue == myTab[i]["APC3"] || strValue == myTab[i]["APC4"])
					{
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strMessage += "Origin <" + strValue + "> does not exist\n";
				}
			}
			strValue = txtDEST.Text;
			if(strValue != "")
			{
				ITable myTab = myDB["APT"];
				bool blFound = false;
				for(int i = 0; i < myTab.Count && blFound == false; i++)
				{
					if(strValue == myTab[i]["APC3"] || strValue == myTab[i]["APC4"])
					{
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strMessage += "Destination <" + strValue + "> does not exist\n";
				}
			}
//			if(dtFrom.Value >= dtTo.Value)
//			{
//				strMessage += "Date From > Date To\n";
//			}
			return strMessage;
		}

		private void frmRecalculateRules_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strMessage = "";
			string strSQL = "";
			strMessage = Check();
			if(this.DialogResult == DialogResult.OK)
			{
				if(strMessage != "")
				{
					e.Cancel = true;
				}
				else
				{
					//Build SQL string
					strSQL = BuildSQLString();
					IUfisComWriter aUfis = UT.GetUfisCom();
					int ilRet = aUfis.CallServer("CFS", "AFTTAB", "", "", strSQL, "230");
					if(ilRet != 0)
					{
						string strERR = aUfis.LastErrorMessage;
						MessageBox.Show(this, strERR);
						//e.Cancel = true;//TO DO remove this
					}
				}
			}
		}
		private string BuildSQLString()
		{
			string strSQL = "";
			string strDateFrom = "";
			string strDateTo = "";
			string strALC = "";
			string strORIG = "";
			string strFLNO = "";
			string strDEST = "";
			string strTmpFlno = "";

			DateTime datFrom = dtFrom.Value;
			DateTime datTo;
			datFrom = new DateTime(datFrom.Year, datFrom.Month, datFrom.Day, 0, 0, 0);
			datTo = new DateTime(datFrom.Year, datFrom.Month, datFrom.Day, 23, 59, 0);
			strDateFrom = UT.DateTimeToCeda(datFrom);
			strDateTo = UT.DateTimeToCeda(datTo);

			strSQL  = "WHERE ((TIFA BETWEEN '" + strDateFrom +"' AND '" + strDateTo + "') OR";
			strSQL += " (TIFD BETWEEN '" + strDateFrom +"' AND '" + strDateTo + "')) ";

			if(txtALC.Text != "" && txtFLNO.Text != "") //Build the flight number
			{
				if(txtALC.Text.Length == 2)
				{
					strALC = txtALC.Text + " ";
				}
				if(txtALC.Text.Length == 3)
				{
					strALC = txtALC.Text;
				}
				strTmpFlno = txtFLNO.Text;
				if(strTmpFlno.Length < 3)
				{
					strTmpFlno = strTmpFlno.PadLeft(3, '0');
				}
				if(txtSuffix.Text!= "")
				{
					strTmpFlno = strTmpFlno.PadRight(5, ' ') + txtSuffix.Text;
					strFLNO = " FLNO='" + strALC + strTmpFlno + "'";
					strSQL += "AND " + strFLNO;
				}
				else
				{
					strFLNO = " FLNO='" + strALC + strTmpFlno + "'";
					strSQL += "AND " + strFLNO;
				}
			}
			else if(txtALC.Text != "")
			{
				if(txtALC.Text.Length == 2)
				{
					strFLNO = " ALC2='"  + txtALC.Text + "' ";
					strSQL += "AND " + strFLNO;
				}
				if(txtALC.Text.Length == 3)
				{
					strFLNO = " ALC3='" + txtALC.Text + "' ";
					strSQL += "AND " + strFLNO;
				}
			}
			if(txtDEST.Text != "")
			{
				strDEST = " DES3='" + txtDEST.Text + "' ";
				strSQL += "AND " + strDEST;
			}
			if(txtORIG.Text != "")
			{
				strORIG = " ORG3='" + txtORIG.Text + "' ";
				strSQL += "AND " + strORIG;
			}
			if(strSQL != "" && txtHSSU.Text != "")
			{
				strSQL += "|" + txtHSSU.Text;
			}
			return strSQL;
		}
	}
}
