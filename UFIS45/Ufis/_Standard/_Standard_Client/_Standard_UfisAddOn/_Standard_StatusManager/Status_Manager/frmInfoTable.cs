using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using System.Threading;

using Ufis.Utils;
using Ufis.Data;

using Ufis.Status_Manager.Ctrl;

namespace Ufis.Status_Manager.UI
{
	/// <summary>
	/// Summary description for frmInfoTable.
	/// </summary>
	public class frmInfoTable : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panelHeader;
		private System.Windows.Forms.Panel panelBody;
		public AxTABLib.AxTAB tabInfo;
		public System.Windows.Forms.Label lblCaption;
		private System.Windows.Forms.Panel panelArrival;
		private System.Windows.Forms.Panel panelDeparture;
		public AxTABLib.AxTAB tabDeparture;
		private System.Windows.Forms.Panel panel2;
		public System.Windows.Forms.Label lblDeparture;
		public System.Windows.Forms.TextBox txtArrival;
		public System.Windows.Forms.TextBox txtDeparture;
		public System.Windows.Forms.TextBox txtDepLastAction;
		public System.Windows.Forms.TextBox txtArrLastAction;
		private AxTABLib.AxTAB tabStatusForFlight;
        private TextBox txtArrJob;
        private TextBox txtDepJob;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmInfoTable()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
            InitDbTableAndEvent();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
                if (!_disposed)
                {
                    UT.LogMsg("InfoTable: Closing");
                    //DE.OnOST_Changed -= delegateOstChanged;
                    //DE.OnUpdateFlightData -= delegateUpdateFlightData;
                    DE.OnOST_ChangedForAFlight -= delegateOstChangedForAFlight;
                    if (delegateJobTemplateChanged != null)
                        DE.OnJobTemplateChanged -= delegateJobTemplateChanged;
                    if (delegateJobChanged != null)
                        DE.OnJob_Changed -= delegateJobChanged;
                    UT.LogMsg("InfoTable: Closed");
                    _disposed = true;
                }
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInfoTable));
            this.panelHeader = new System.Windows.Forms.Panel();
            this.lblCaption = new System.Windows.Forms.Label();
            this.panelBody = new System.Windows.Forms.Panel();
            this.panelDeparture = new System.Windows.Forms.Panel();
            this.tabStatusForFlight = new AxTABLib.AxTAB();
            this.txtDepLastAction = new System.Windows.Forms.TextBox();
            this.txtDeparture = new System.Windows.Forms.TextBox();
            this.txtDepJob = new System.Windows.Forms.TextBox();
            this.tabDeparture = new AxTABLib.AxTAB();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblDeparture = new System.Windows.Forms.Label();
            this.panelArrival = new System.Windows.Forms.Panel();
            this.txtArrLastAction = new System.Windows.Forms.TextBox();
            this.txtArrival = new System.Windows.Forms.TextBox();
            this.txtArrJob = new System.Windows.Forms.TextBox();
            this.tabInfo = new AxTABLib.AxTAB();
            this.panelHeader.SuspendLayout();
            this.panelBody.SuspendLayout();
            this.panelDeparture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabStatusForFlight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabDeparture)).BeginInit();
            this.panel2.SuspendLayout();
            this.panelArrival.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelHeader.Controls.Add(this.lblCaption);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(248, 28);
            this.panelHeader.TabIndex = 1;
            // 
            // lblCaption
            // 
            this.lblCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.ForeColor = System.Drawing.Color.Blue;
            this.lblCaption.Location = new System.Drawing.Point(0, 0);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(244, 24);
            this.lblCaption.TabIndex = 0;
            // 
            // panelBody
            // 
            this.panelBody.Controls.Add(this.panelDeparture);
            this.panelBody.Controls.Add(this.panelArrival);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 0);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(496, 550);
            this.panelBody.TabIndex = 2;
            // 
            // panelDeparture
            // 
            this.panelDeparture.Controls.Add(this.tabStatusForFlight);
            this.panelDeparture.Controls.Add(this.txtDepLastAction);
            this.panelDeparture.Controls.Add(this.txtDeparture);
            this.panelDeparture.Controls.Add(this.txtDepJob);
            this.panelDeparture.Controls.Add(this.tabDeparture);
            this.panelDeparture.Controls.Add(this.panel2);
            this.panelDeparture.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelDeparture.Location = new System.Drawing.Point(248, 0);
            this.panelDeparture.Name = "panelDeparture";
            this.panelDeparture.Size = new System.Drawing.Size(248, 550);
            this.panelDeparture.TabIndex = 2;
            // 
            // tabStatusForFlight
            // 
            this.tabStatusForFlight.Location = new System.Drawing.Point(92, 4);
            this.tabStatusForFlight.Name = "tabStatusForFlight";
            this.tabStatusForFlight.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabStatusForFlight.OcxState")));
            this.tabStatusForFlight.Size = new System.Drawing.Size(128, 28);
            this.tabStatusForFlight.TabIndex = 5;
            this.tabStatusForFlight.Visible = false;
            // 
            // txtDepLastAction
            // 
            this.txtDepLastAction.AcceptsReturn = true;
            this.txtDepLastAction.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtDepLastAction.ForeColor = System.Drawing.Color.Blue;
            this.txtDepLastAction.Location = new System.Drawing.Point(0, 161);
            this.txtDepLastAction.Multiline = true;
            this.txtDepLastAction.Name = "txtDepLastAction";
            this.txtDepLastAction.Size = new System.Drawing.Size(248, 32);
            this.txtDepLastAction.TabIndex = 4;
            // 
            // txtDeparture
            // 
            this.txtDeparture.AcceptsReturn = true;
            this.txtDeparture.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtDeparture.ForeColor = System.Drawing.Color.Red;
            this.txtDeparture.Location = new System.Drawing.Point(0, 193);
            this.txtDeparture.Multiline = true;
            this.txtDeparture.Name = "txtDeparture";
            this.txtDeparture.Size = new System.Drawing.Size(248, 132);
            this.txtDeparture.TabIndex = 3;
            // 
            // txtDepJob
            // 
            this.txtDepJob.AcceptsReturn = true;
            this.txtDepJob.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtDepJob.Location = new System.Drawing.Point(0, 325);
            this.txtDepJob.Multiline = true;
            this.txtDepJob.Name = "txtDepJob";
            this.txtDepJob.Size = new System.Drawing.Size(248, 225);
            this.txtDepJob.TabIndex = 6;
            this.txtDepJob.Visible = false;
            // 
            // tabDeparture
            // 
            this.tabDeparture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabDeparture.Location = new System.Drawing.Point(0, 28);
            this.tabDeparture.Name = "tabDeparture";
            this.tabDeparture.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabDeparture.OcxState")));
            this.tabDeparture.Size = new System.Drawing.Size(248, 522);
            this.tabDeparture.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.lblDeparture);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(248, 28);
            this.panel2.TabIndex = 1;
            // 
            // lblDeparture
            // 
            this.lblDeparture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDeparture.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeparture.ForeColor = System.Drawing.Color.Blue;
            this.lblDeparture.Location = new System.Drawing.Point(0, 0);
            this.lblDeparture.Name = "lblDeparture";
            this.lblDeparture.Size = new System.Drawing.Size(244, 24);
            this.lblDeparture.TabIndex = 0;
            // 
            // panelArrival
            // 
            this.panelArrival.Controls.Add(this.txtArrLastAction);
            this.panelArrival.Controls.Add(this.txtArrival);
            this.panelArrival.Controls.Add(this.txtArrJob);
            this.panelArrival.Controls.Add(this.tabInfo);
            this.panelArrival.Controls.Add(this.panelHeader);
            this.panelArrival.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelArrival.Location = new System.Drawing.Point(0, 0);
            this.panelArrival.Name = "panelArrival";
            this.panelArrival.Size = new System.Drawing.Size(248, 550);
            this.panelArrival.TabIndex = 1;
            // 
            // txtArrLastAction
            // 
            this.txtArrLastAction.AcceptsReturn = true;
            this.txtArrLastAction.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtArrLastAction.ForeColor = System.Drawing.Color.Blue;
            this.txtArrLastAction.Location = new System.Drawing.Point(0, 161);
            this.txtArrLastAction.Multiline = true;
            this.txtArrLastAction.Name = "txtArrLastAction";
            this.txtArrLastAction.Size = new System.Drawing.Size(248, 32);
            this.txtArrLastAction.TabIndex = 5;
            // 
            // txtArrival
            // 
            this.txtArrival.AcceptsReturn = true;
            this.txtArrival.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtArrival.ForeColor = System.Drawing.Color.Red;
            this.txtArrival.Location = new System.Drawing.Point(0, 193);
            this.txtArrival.Multiline = true;
            this.txtArrival.Name = "txtArrival";
            this.txtArrival.Size = new System.Drawing.Size(248, 132);
            this.txtArrival.TabIndex = 2;
            // 
            // txtArrJob
            // 
            this.txtArrJob.AcceptsReturn = true;
            this.txtArrJob.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtArrJob.Location = new System.Drawing.Point(0, 325);
            this.txtArrJob.Multiline = true;
            this.txtArrJob.Name = "txtArrJob";
            this.txtArrJob.Size = new System.Drawing.Size(248, 225);
            this.txtArrJob.TabIndex = 3;
            this.txtArrJob.Visible = false;
            // 
            // tabInfo
            // 
            this.tabInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabInfo.Location = new System.Drawing.Point(0, 28);
            this.tabInfo.Name = "tabInfo";
            this.tabInfo.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabInfo.OcxState")));
            this.tabInfo.Size = new System.Drawing.Size(248, 522);
            this.tabInfo.TabIndex = 0;
            // 
            // frmInfoTable
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(496, 550);
            this.Controls.Add(this.panelBody);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmInfoTable";
            this.Opacity = 0.8;
            this.Text = "Info Table <X>";
            this.Load += new System.EventHandler(this.frmInfoTable_Load);
            this.Closed += new System.EventHandler(this.frmInfoTable_Closed);
            this.VisibleChanged += new System.EventHandler(this.frmInfoTable_VisibleChanged);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmInfoTable_FormClosed);
            this.panelHeader.ResumeLayout(false);
            this.panelBody.ResumeLayout(false);
            this.panelDeparture.ResumeLayout(false);
            this.panelDeparture.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabStatusForFlight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabDeparture)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panelArrival.ResumeLayout(false);
            this.panelArrival.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabInfo)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

        private void frmInfoTable_Closed(object sender, System.EventArgs e)
        {
        }

        private IDatabase myDB = null;
        private ITable myAFT = null;
        private ITable myOST = null;

        private void frmInfoTable_Load(object sender, System.EventArgs e)
        {
            tabInfo.ResetContent();
            tabInfo.HeaderString = "Item,Value";
            tabInfo.HeaderLengthString = "80,170";
            tabInfo.LogicalFieldList = tabInfo.HeaderString;
            tabInfo.LifeStyle = true;
            tabInfo.FontSize = 12;
            tabInfo.LineHeight = 14;
            tabInfo.SetTabFontBold(true);

            tabDeparture.ResetContent();
            tabDeparture.HeaderString = "Item,Value";
            tabDeparture.HeaderLengthString = "80,170";
            tabDeparture.LogicalFieldList = tabInfo.HeaderString;
            tabDeparture.LifeStyle = true;
            tabDeparture.FontSize = 12;
            tabDeparture.LineHeight = 14;
            tabDeparture.SetTabFontBold(true);
            ShowJobInfo();
        }

        private void ShowJobInfo()
        {
            if (!this.Visible) return;
            if (this.CurAftRkey == "") return;
            if (this.InvokeRequired)
            {//AM 20080724 - Make as thread safe
                CallbackMethod0 d = new CallbackMethod0(ShowJobInfo);
                this.Invoke(d);
            }
            else
            {
                CtrlJob ctrlJob = CtrlJob.GetInstance();
                //Set to show the job related information

                if (ctrlJob.ShowJob)
                {
                    txtDepJob.Visible = true;
                    txtArrJob.Visible = true;
                    this.Height = 560;

                    string stArrJobInfo = "";
                    string stDepJobInfo = "";
                    ctrlJob.GetJobInfo(this.ArrAftUrno, this.DepAftUrno,
                        out stArrJobInfo, out stDepJobInfo);
                    txtArrJob.Text = stArrJobInfo;
                    txtDepJob.Text = stDepJobInfo;

                }
                else
                {
                    txtDepJob.Visible = false;
                    txtArrJob.Visible = false;
                    this.Height = 335;
                }
            }
        }

        //AM 20080319 - To place the flight information in this form

        //private DE.OST_Changed delegateOstChanged = null;
        //private DE.UpdateFlightData delegateUpdateFlightData = null;
        private DE.OST_ChangedForAFlight delegateOstChangedForAFlight = null;
        private DE.JobTemplateChanged delegateJobTemplateChanged = null;
        private DE.Job_Changed delegateJobChanged = null;

        private void InitDbTableAndEvent()
        {
            myDB = UT.GetMemDB();
            myAFT = myDB["AFT"];
            myOST = myDB["OST"];

            //delegateOstChanged = new Status_Manager.DE.OST_Changed(DE_OnOST_Changed);
            delegateOstChangedForAFlight = new DE.OST_ChangedForAFlight(DE_OnOST_ChangedForAFlight);
            //delegateUpdateFlightData = new Status_Manager.DE.UpdateFlightData(DE_OnUpdateFlightData);

            //DE.OnOST_Changed += delegateOstChanged;
            //DE.OnUpdateFlightData += delegateUpdateFlightData;

            DE.OnOST_ChangedForAFlight += delegateOstChangedForAFlight;
            delegateJobTemplateChanged = new DE.JobTemplateChanged(DE_OnJobTemplateChanged);
            DE.OnJobTemplateChanged += delegateJobTemplateChanged;

            delegateJobChanged = new DE.Job_Changed(DE_OnJobChanged);
            DE.OnJob_Changed += delegateJobChanged;

        }

        private string _strCurAftRkey = "";
        private bool _firstTime = true;

        /// <summary>
        /// Display the information for given flight Urno
        /// </summary>
        /// <param name="aftUrno"></param>
        public bool DisplayFlightInfoScreen(string aftRkey)
        {
            bool hasFlight = false;
            try
            {
                if ((aftRkey == null) || (aftRkey.Trim() == ""))
                {
                    this.CurAftRkey = "";
                    this.HideMe();
                }
                else
                {
                    if (aftRkey.Trim() != this.CurAftRkey)
                    {
                        this.CurAftRkey = aftRkey.Trim();
                        ShowFlightInfo();
                        if (_firstTime)
                        {
                            _firstTime = false;
                            ShowFlightInfo();
                        }
                    }
                 }
            }
            catch (Exception)
            {
            }
            if ((ArrAftUrno != "") || (DepAftUrno != ""))
            {
                hasFlight = true;
                if (!this.Visible)
                    this.Show();
            }
            
            return hasFlight;
        }


        public void HideMe()
        {
            this.Hide();
        }

        private string ConvDateTimeToShow(DateTime dt)
        {
            //return string.Format("{0:HH:mm / dd MMM yyyy}", dt);
            //return dt.ToShortTimeString() + "/" + dt.ToShortDateString();
            return string.Format("{0:HH:mm/dd-MMM-yyyy}", dt);
        }

        private string ConvTimeToShow(DateTime dt)
        {
            return string.Format("{0:HH:mm}", dt);
        }
        
        private void ShowArrInfo(IRow row)
        {
            this.tabInfo.ResetContent();
            this.tabInfo.ResetText();
            string strVal = "";
            DateTime dt = row.FieldAsDateTime("STOA");
            this.lblCaption.Text = "Arrival POS: " + row["PSTA"] + "  Gate: " + row["GTA1"];
            string strTime = "";
            strVal = "FLNO," + row["FLNO"] + '\n';
            strVal += "Origin," + row["ORG3"] + '\n';
            strVal += "VIA," + row["VIA3"] + '\n';
            //strVal += "STA," + row.FieldAsDateTime("STOA").ToShortTimeString() + '\n';
            strVal += "STA," + ConvDateTimeToShow(row.FieldAsDateTime("STOA")) + '\n';
            strTime = row["ETAI"].Trim();
            if (strTime != "")
            {
                //strTime = UT.CedaFullDateToDateTime(strTime).ToShortTimeString();
                strTime = ConvDateTimeToShow(UT.CedaFullDateToDateTime(strTime));
            }
            strVal += "ETA," + strTime + '\n';
            strTime = row["LAND"].Trim();
            if (strTime != "")
            {
                //strTime = UT.CedaFullDateToDateTime(strTime).ToShortTimeString();
                strTime = ConvTimeToShow(UT.CedaFullDateToDateTime(strTime));
            }
            strVal += "LAND," + strTime + '\n';
            strTime = row["ONBL"].Trim();
            if (strTime != "")
            {
                //strTime = UT.CedaFullDateToDateTime(strTime).ToShortTimeString();
                strTime = ConvTimeToShow(UT.CedaFullDateToDateTime(strTime));
            }
            strVal += "ONBL," + strTime + '\n';
            strVal += "ACT," + row["ACT3"] + '\n';
            strVal += "Regis," + row["REGN"] + '\n';
            this.tabInfo.InsertBuffer(strVal, '\n'.ToString());
            this.tabInfo.Refresh();
            this.lblCaption.Refresh();
        }

        private string _strArrAftUrno = "";
        private string _strDepAftUrno = "";

        private string CurAftRkey
        {
            get { return _strCurAftRkey; }
            set
            {
                string tmpRkey = value;
                if (tmpRkey == null)
                {
                    tmpRkey = "";
                }
                if (tmpRkey != _strCurAftRkey)
                {
                    _strCurAftRkey = tmpRkey;
                    ArrAftUrno = "";
                    DepAftUrno = "";
                }
            }
        }

        private void RefreshArrStatus()
        {
            if (string.IsNullOrEmpty(this.ArrAftUrno)) return;
            //this.txtArrival.Text = GetStatusConflicts(this.ArrAftUrno);
            //this.txtArrLastAction.Text = GetLastActionStatus(this.ArrAftUrno);
            string lastStat = "";
            string allCfl = "";

            CtrlCfl.GetInstance().GetConflictStatus(this.ArrAftUrno, 5, "\r\n", ref lastStat, ref allCfl);
            this.txtArrival.Text = allCfl ;
            this.txtArrLastAction.Text = lastStat;
            //this.txtArrival.Refresh();
            //this.txtArrLastAction.Refresh();
        }

        private void RefreshDepStatus()
        {
            if (string.IsNullOrEmpty(this.DepAftUrno)) return;
            //this.txtDeparture.Text = GetStatusConflicts(this.DepAftUrno );
            //this.txtDepLastAction.Text = GetLastActionStatus(this.DepAftUrno);
            string lastStat = "";
            string allCfl = "";
            CtrlCfl.GetInstance().GetConflictStatus(this.DepAftUrno, 5, "\r\n", ref lastStat, ref allCfl);
            this.txtDeparture.Text = allCfl;
            this.txtDepLastAction.Text = lastStat;
            //this.txtDeparture.Refresh();
            //this.txtDepLastAction.Refresh();
        }

        private string GetLastActionStatus(string aftUrno)
        {
            //string stResult = "";
            //if ((aftUrno != null) && (aftUrno != ""))
            //{
            //    stResult = CFC.GetLastActionStatusForFlight(aftUrno);
            //}
            //return stResult;
            return CtrlCfl.GetInstance().GetLastStatus(this.ArrAftUrno);
        }

        private string GetStatusConflicts(string aftUrno)
        {
            //string stResult = "";
            //if ((aftUrno != null) && (aftUrno != ""))
            //{
            //    ArrayList arrCf = CFC.GetStatusConflicsForFlight(aftUrno);
            //    for (int i = 0; i < arrCf.Count && i < 5; i++)
            //    {
            //        stResult += arrCf[i].ToString() + "\r\n";
            //    }
            //    if (arrCf.Count > 5)
            //    {
            //        int ilMore = arrCf.Count - 5;
            //        stResult += ilMore.ToString() + " more conflicts";
            //    }
            //}
            //return stResult;
            return CtrlCfl.GetInstance().GetConflictStatus(this.ArrAftUrno, 5, "\r\n");
        }

        private void ShowDepInfo(IRow row, bool blRotation)
        {
            if (blRotation == true)
            {
                this.lblDeparture.Text = "Departure POS: " + row["PSTD"] + "  Gate: " + row["GTD1"];
                this.lblDeparture.Refresh();
            }
            else
            {
                this.lblCaption.Text = "Departure POS: " + row["PSTD"] + "  Gate: " + row["GTD1"];
                this.lblCaption.Refresh();
            }
            string strTime = "";
            string strVal = "FLNO," + row["FLNO"] + '\n';
            strVal += "Dest.," + row["DES3"] + '\n';
            strVal += "VIA," + row["VIA3"] + '\n';
            //strVal += "STD," + row.FieldAsDateTime("STOD").ToShortTimeString() + '\n';
            strVal += "STD," + ConvDateTimeToShow( row.FieldAsDateTime("STOD")) + '\n';
            strTime = row["ETDI"].Trim();
            if (strTime != "")
            {
                //strTime = UT.CedaFullDateToDateTime(strTime).ToShortTimeString();
                strTime = ConvDateTimeToShow(UT.CedaFullDateToDateTime(strTime));
            }
            strVal += "ETD," + strTime + '\n';
            strTime = row["OFBL"].Trim();
            if (strTime != "")
            {
                //strTime = UT.CedaFullDateToDateTime(strTime).ToShortTimeString();
                strTime = ConvTimeToShow(UT.CedaFullDateToDateTime(strTime));
            }
            strVal += "OFBL," + strTime + '\n';
            strTime = row["AIRB"].Trim();
            if (strTime != "")
            {
                //strTime = UT.CedaFullDateToDateTime(strTime).ToShortTimeString();
                strTime = ConvTimeToShow(UT.CedaFullDateToDateTime(strTime));
            }
            strVal += "AIRB," + strTime + '\n';
            strVal += "ACT," + row["ACT3"] + '\n';
            strVal += "Regis," + row["REGN"] + '\n';
            AxTABLib.AxTAB myTab = null;
            if (blRotation == true)
            {
                
                myTab = this.tabDeparture;
            }
            else
            {
                myTab = this.tabInfo;
            }
            myTab.ResetContent();
            myTab.InsertBuffer(strVal, '\n'.ToString());
            myTab.Refresh();
        }

        private string ArrAftUrno
        {
            get
            {
                return _strArrAftUrno;
            }
            set
            {
                if (_strArrAftUrno != value)
                {
                    if (value == null) _strArrAftUrno = "";
                    else _strArrAftUrno = value.Trim();
                    this.RefreshArrStatus();
                }
            }
        }

        private string DepAftUrno
        {
            get { return _strDepAftUrno; }
            set
            {
                if (_strDepAftUrno != value)
                {
                    if (value == null) _strDepAftUrno = "";
                    else _strDepAftUrno = value;
                    this.RefreshDepStatus();
                }
            }
        }

        private void RefreshInfo(IRow[] rows)
        {
            #region Get Flight Info
            ArrAftUrno  = "";
            DepAftUrno = "";
            if (rows.Length == 0)
            {
                this.Hide();
            }
            else
            {
                bool blRotation = false;

                if (rows.Length == 2)//Check if we have a rotation
                {
                    if ((rows[0]["ADID"] == "A" && rows[1]["ADID"] == "D") ||
                        (rows[0]["ADID"] == "D" && rows[1]["ADID"] == "A"))
                    {
                        this.Width = this.tabInfo.Width * 2 + 1;
                        blRotation = true;
                    }
                    else
                    {
                        this.Width = this.tabInfo.Width + 1;
                    }
                }
                else
                {
                    this.Width = this.tabInfo.Width + 1;
                }

                for (int rc = 0; rc < rows.Length; rc++)
                {
                    String strAdid = rows[rc]["ADID"];
                    if (strAdid == "A")
                    {
                        ArrAftUrno = rows[rc]["URNO"];
                        ShowArrInfo(rows[rc]);
                    }
                    else
                    {
                        DepAftUrno = rows[rc]["URNO"];
                        ShowDepInfo(rows[rc], blRotation);
                    }

                }//End for rows.length
            }
            #endregion
        }

        delegate void CallbackMethod0();
        private object _lockObj = new object();
        private bool _Refreshing = false;

        private void ShowFlightInfo()
        {
            if (this.CurAftRkey == "")
            {
                return;
            }

            if (this.InvokeRequired)
            {//AM 20080319 - Make as thread safe
                CallbackMethod0 d = new CallbackMethod0(ShowFlightInfo);
                this.Invoke(d);
            }
            else
            {
                lock (_lockObj)
                {
                    if (!_Refreshing)
                    {
                        try
                        {
                            if (myAFT != null)
                            {
                                _Refreshing = true;
                                UT.LogMsg("InfoTable:Show:b4 acq lockAFT");
                                myAFT.Lock.AcquireReaderLock(Timeout.Infinite);
                                UT.LogMsg("InfoTable:Show:af acq lockAFT");
                                IRow[] rows = myAFT.RowsByIndexValue("RKEY", this.CurAftRkey);

                                if ((rows == null) || (rows.Length == 0))
                                {
                                    this.Hide();
                                }
                                else
                                {
                                    RefreshInfo(rows);
                                    this.Show();
                                    //this.Refresh();
                                }
                            }
                            else
                            {
                                UT.LogMsg("frmInfoTable:ShowFlightInfo:Err:No Aft Info.");
                            }
                        }
                        catch (Exception ex)
                        {
                            UT.LogMsg("frmInfoTable:ShowFlightInfo:Err:" + ex.Message + Environment.NewLine + ex.StackTrace );
                            MessageBox.Show("Show Flight Information Error due to : " + ex.Message);
                        }
                        finally
                        {
                            _Refreshing = false;
                            UT.LogMsg("InfoTable:Show:b4 rel lockAFT");
                            if (myAFT.Lock.IsReaderLockHeld) myAFT.Lock.ReleaseReaderLock();
                            UT.LogMsg("InfoTable:Show:af rel lockAFT");
                        }
                        try
                        {
                            ShowJobInfo();
                        }
                        catch (Exception) { }
                    }
                }
            }
        }

        delegate void DelgObjStrState(object sender, string strOstUrno, State state);
        private void DE_OnOST_Changed(object sender, string strOstUrno, State state)
        {
            if (this.InvokeRequired)
            {
                DelgObjStrState d = new DelgObjStrState(DE_OnOST_Changed);
                this.Invoke(d, new object[] { sender, strOstUrno, state });
            }
            else
            {
                UT.LogMsg("frmInfoTable:DE_OnOST_Changed:Start");
                if (!this.Visible)
                {
                    this.CurAftRkey = "";
                    return;
                }
                switch (state)
                {
                    case State.Created:
                    case State.Modified:
                        string strAftUrno = "";
                        try
                        {
                            UT.LogMsg("frmInfoTable:DE_OnOST_Changed: b4 OST acq Reader Lock");
                            myOST.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
                            UT.LogMsg("frmInfoTable:DE_OnOST_Changed: af OST acq Reader Lock");
                            IRow[] ostRows = myOST.RowsByIndexValue("URNO", strOstUrno);
                            if (ostRows.Length > 0)
                            {
                                strAftUrno = ostRows[0]["UAFT"].Trim();
                            }
                        }
                        catch (Exception ex)
                        {
                            LogExceptionErr(ex, "");
                        }
                        finally
                        {
                            UT.LogMsg("frmInfoTable:DE_OnOST_Changed: b4 OST rel Reader Lock");
                            if (myOST.Lock.IsReaderLockHeld) myOST.Lock.ReleaseReaderLock();
                            UT.LogMsg("frmInfoTable:DE_OnOST_Changed: af OST rel Reader Lock");
                        }
                        if (strAftUrno != "")
                        {
                            if (strAftUrno == this.DepAftUrno)
                            {
                                RefreshDepStatus();
                            }
                            if (strAftUrno == this.ArrAftUrno)
                            {
                                RefreshArrStatus();
                            }
                        }
                        break;
                    case State.Deleted:
                        RefreshDepStatus();
                        RefreshArrStatus();
                        break;
                }
                UT.LogMsg("frmInfoTable:DE_OnOST_Changed:Finish");
            }
        }

        delegate void DelgUpdOstChgForAFlight(string strAftUrno);

        private void UpdateOstChangesForAFlight(string strAftUrno)
        {
            if (this.InvokeRequired)
            {
                DelgUpdOstChgForAFlight d = new DelgUpdOstChgForAFlight(UpdateOstChangesForAFlight);
                this.Invoke(d, new object[] { strAftUrno });
            }
            else
            {
                try
                {
                    if (strAftUrno == this.DepAftUrno)
                    {
                        RefreshDepStatus();
                    }
                    if (strAftUrno == this.ArrAftUrno)
                    {
                        RefreshArrStatus();
                    }
                }
                catch (Exception ex)
                {
                    UT.LogMsg("frmInfoTable:UpdateOstChangesForAFlight:Uaft<" + strAftUrno + ">:Err:" + ex.Message);
                }
            }
        }

        private void DE_OnOST_ChangedForAFlight(object sender, string strAftUrno)
        {

            try
            {
                UT.LogMsg("frmInfoTable:DE_OnOST_ChangedForAFlight:Start");
                if (this.Visible)
                {
                    if ((strAftUrno == this.CurAftRkey) || (strAftUrno == ArrAftUrno) || (strAftUrno == DepAftUrno))
                    {
                        UT.LogMsg("frmRunViewPoint:DE_OnUpdateFlightData:Start");
                        Thread thUpdFlightData = new Thread(delegate() { this.UpdateOstChangesForAFlight(strAftUrno); });
                        thUpdFlightData.IsBackground = true;
                        thUpdFlightData.Start();
                        UT.LogMsg("frmRunViewPoint:DE_OnUpdateFlightData:Finish");
                    }
                }
                else
                {
                    this.CurAftRkey = "";
                }
                UT.LogMsg("frmInfoTable:DE_OnOST_ChangedForAFlight:Finish");
            }
            catch (Exception ex)
            {
                LogExceptionErr(ex, "");
            }
            
        }

        private void DE_OnJobTemplateChanged(string newUtpl)
        {
            if (this.Visible)
            {
                if (this.CurAftRkey != "")
                {
                    UT.LogMsg("frmInfoTable:DE_OnJobTemplateChanged:1Start");
                    Thread th = new Thread(new ThreadStart(ShowJobInfo));
                    th.IsBackground = true;
                    th.Start();
                    UT.LogMsg("frmInfoTable:DE_OnJobTemplateChanged:1Finish");
                }
            }
            else
            {
                this.CurAftRkey = "";
            }
        }

        private bool IsCurUaft(string uaft)
        {
            if ((uaft != "") && ((uaft == this.ArrAftUrno) || (uaft == this.DepAftUrno))) return true;
            else return false;
        }

        private void DE_OnJobChanged(object sender, string uJob, string orgUaft, string newUaft)
        {
            if (this.Visible)
            {
                if (this.CurAftRkey != "")
                {
                    //bool upd = false;
                    if (IsCurUaft(orgUaft) || IsCurUaft(newUaft))
                    {
                        UT.LogMsg("frmInfoTable:DE_OnJobChanged:Start");
                        Thread th = new Thread(new ThreadStart(ShowJobInfo));
                        th.IsBackground = true;
                        th.Start();
                        UT.LogMsg("frmInfoTable:DE_OnJobChanged:Finish");
                    }
                    else
                    {
                        UT.LogMsg(string.Format("frmInfoTable:DE_OnJobChanged:Not for current UAFT[{0},{1}] chg[{2},{3}]",
                            this.ArrAftUrno, this.DepAftUrno, orgUaft, newUaft));
                    }
                }
            }
        }

        private void DE_OnUpdateFlightData(object sender, string strAftUrno, State state)
        {
            try
            {
                UT.LogMsg("frmInfoTable:DE_OnUpdateFlightData:Start");
                if (this.Visible)
                {
                    if ((strAftUrno == this.CurAftRkey) || (strAftUrno == ArrAftUrno) || (strAftUrno == DepAftUrno))
                    {
                        UT.LogMsg("frmInfoTable:DE_OnUpdateFlightData:1Start");
                        Thread th = new Thread(new ThreadStart(ShowFlightInfo));
                        th.IsBackground = true;
                        th.Start();
                        //ShowFlightInfo();
                        UT.LogMsg("frmInfoTable:DE_OnUpdateFlightData:1Finish");
                    }
                }
                else
                {
                    this.CurAftRkey = "";
                }
                UT.LogMsg("frmInfoTable:DE_OnUpdateFlightData:Finish");
            }
            catch (Exception ex)
            {
                LogExceptionErr(ex, "");
            }
            
        }

        private void LogExceptionErr(Exception ex, string msgToDisplay)
        {
            UT.LogMsg("frmRunViewPoint:Err:" + ex.Message + Environment.NewLine + ex.StackTrace);
            if (msgToDisplay.Trim() != "")
            {
                MessageBox.Show(msgToDisplay, "Error");
            }
        }

        private void frmInfoTable_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Dispose(true);
        }
        private bool _disposed = false;

        private void frmInfoTable_VisibleChanged(object sender, EventArgs e)
        {
            //string st = e.ToString();
            //string st1 = sender.ToString();
            if (!this.Visible)
            {
                this.CurAftRkey = "";
            }
        }



	}
}
