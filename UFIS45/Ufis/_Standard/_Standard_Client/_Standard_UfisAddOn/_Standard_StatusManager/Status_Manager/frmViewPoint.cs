using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;
using Ufis.Status_Manager.Ctrl;

namespace Ufis.Status_Manager.UI
{
	/// <summary>
	/// Summary description for frmViewPoint.
	/// </summary>
	public class frmViewPoint : System.Windows.Forms.Form
	{

		private System.Windows.Forms.Panel panelToolbar;
		private System.Windows.Forms.Button button1;
		private System.ComponentModel.IContainer components;
		private int currMouseX = -1;
		private int currMouseY = -1;
		private int currMarkerX = -1;
		private int currMarkerY = -1;
		private System.Windows.Forms.Panel panelProperties;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.PictureBox picArea;
		private System.Windows.Forms.TextBox txtFileName;
		private System.Windows.Forms.Form omParent;
		private System.Windows.Forms.ListBox lbPositions;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtCurrPos;
		private System.Windows.Forms.RadioButton rbLeftTop;
		private System.Windows.Forms.RadioButton rbTop;
		private System.Windows.Forms.RadioButton rbRightTop;
		private System.Windows.Forms.RadioButton rbRight;
		private System.Windows.Forms.RadioButton rbRightBottom;
		private System.Windows.Forms.RadioButton rbBottom;
		private System.Windows.Forms.RadioButton rbLeftBottom;
		private System.Windows.Forms.RadioButton rbLeft;

		private System.Collections.ArrayList omRbs = new ArrayList(10);

		private Pen  myMarkPen = new Pen(Color.LightGreen , 5); 
		private bool imageLoaded = false;
		private String strCurrPos;
		private System.Windows.Forms.TextBox txtViewPointName;
		private String ViewPointName = "";
		private AxTABLib.AxTAB myPosDefs;
		private int myFontSize = 12;
		private System.Drawing.Font omFont;// = new Font("Arial", myFontSize, FontStyle.Bold);
		int currPosTabLine = -1;
		int oldPosTabLine = -1;
		private frmData omHiddenData;
		private String omMode;
		private System.Windows.Forms.Button btnSave;
		private System.Timers.Timer timer1; //can be "EDIT" or "NEW"
		private String omViewPoint;
		private System.Windows.Forms.CheckBox cbShowGrid; //Name of view point for "EDIT" Mode
		private int oldX=-1;
		private int oldY=-1;
		private int oldScrollX=-1;
		private int oldScrollY=-1;
		private System.Windows.Forms.RadioButton rbAsCircle;
		private System.Windows.Forms.RadioButton rbAsText;
		private System.Windows.Forms.RadioButton rbAsAircraft;

		private bool bmGridActive = false;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.NumericUpDown udImageSize;
		private System.Windows.Forms.Label lblAlignments;
		private System.Windows.Forms.Panel panelAlignments;
		private System.Windows.Forms.Label lblPositions;
		private System.Windows.Forms.Label lblFileName;
		private System.Windows.Forms.Label lblViewName;
		private System.Windows.Forms.Label lblImageSize;
		private System.Windows.Forms.GroupBox groupDrawmode;
		private System.Windows.Forms.PictureBox picProperties;
		private System.Windows.Forms.PictureBox picToolbar;
		private System.Windows.Forms.Label lblGridsize;
		private System.Windows.Forms.ImageList imageListLarge;
		private System.Windows.Forms.ImageList imageListArrows;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Label lblDefaulTimeFrame;
		private System.Windows.Forms.NumericUpDown udDefaultTimeframe;
		private System.Windows.Forms.MenuItem mnuDelete;
		private System.Windows.Forms.ContextMenu contextMenuPositions;
		private String strDrawMode = "AIRCRAFT";
		private System.Windows.Forms.Button btnDeleteAll;
		private int imCurrentAlign = 1;
		private System.Windows.Forms.CheckBox cbAutoRotate;
		private System.Windows.Forms.ImageList imageListSmarIcons;
		private IDatabase myDB = null;
		private ITable myDefs = null;
		private System.Windows.Forms.ListBox lbGates;
		private System.Windows.Forms.Label lblGates;
		private System.Windows.Forms.Label lblPosType;
		private System.Windows.Forms.Label txtType;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown udGridSize;
		private string myDBObjectString = "";
		//just a help counter

		public frmViewPoint(Form  opParent, frmData opData, String opMode, String opViewPoint)
		{
			omParent = opParent;
			omHiddenData = opData;
			omMode = opMode;
			omViewPoint = opViewPoint;
			strDrawMode = "AIRCRAFT";

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
			myMarkPen.Dispose();
			if(picArea.Image != null)
			{
				picArea.Image.Dispose();
			}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmViewPoint));
            this.panelToolbar = new System.Windows.Forms.Panel();
            this.udGridSize = new System.Windows.Forms.NumericUpDown();
            this.btnDeleteAll = new System.Windows.Forms.Button();
            this.imageListSmarIcons = new System.Windows.Forms.ImageList(this.components);
            this.cbShowGrid = new System.Windows.Forms.CheckBox();
            this.lblGridsize = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.picToolbar = new System.Windows.Forms.PictureBox();
            this.panelProperties = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.Label();
            this.lblPosType = new System.Windows.Forms.Label();
            this.lblAlignments = new System.Windows.Forms.Label();
            this.lbGates = new System.Windows.Forms.ListBox();
            this.lblGates = new System.Windows.Forms.Label();
            this.cbAutoRotate = new System.Windows.Forms.CheckBox();
            this.udDefaultTimeframe = new System.Windows.Forms.NumericUpDown();
            this.lblDefaulTimeFrame = new System.Windows.Forms.Label();
            this.lblImageSize = new System.Windows.Forms.Label();
            this.udImageSize = new System.Windows.Forms.NumericUpDown();
            this.groupDrawmode = new System.Windows.Forms.GroupBox();
            this.rbAsAircraft = new System.Windows.Forms.RadioButton();
            this.rbAsCircle = new System.Windows.Forms.RadioButton();
            this.rbAsText = new System.Windows.Forms.RadioButton();
            this.myPosDefs = new AxTABLib.AxTAB();
            this.panelAlignments = new System.Windows.Forms.Panel();
            this.rbLeftBottom = new System.Windows.Forms.RadioButton();
            this.imageListArrows = new System.Windows.Forms.ImageList(this.components);
            this.rbBottom = new System.Windows.Forms.RadioButton();
            this.txtCurrPos = new System.Windows.Forms.TextBox();
            this.rbLeft = new System.Windows.Forms.RadioButton();
            this.rbRightTop = new System.Windows.Forms.RadioButton();
            this.rbRightBottom = new System.Windows.Forms.RadioButton();
            this.rbTop = new System.Windows.Forms.RadioButton();
            this.rbRight = new System.Windows.Forms.RadioButton();
            this.rbLeftTop = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.lbPositions = new System.Windows.Forms.ListBox();
            this.lblPositions = new System.Windows.Forms.Label();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.lblFileName = new System.Windows.Forms.Label();
            this.txtViewPointName = new System.Windows.Forms.TextBox();
            this.lblViewName = new System.Windows.Forms.Label();
            this.picProperties = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.picArea = new System.Windows.Forms.PictureBox();
            this.contextMenuPositions = new System.Windows.Forms.ContextMenu();
            this.mnuDelete = new System.Windows.Forms.MenuItem();
            this.timer1 = new System.Timers.Timer();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageListLarge = new System.Windows.Forms.ImageList(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panelToolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udGridSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picToolbar)).BeginInit();
            this.panelProperties.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udDefaultTimeframe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udImageSize)).BeginInit();
            this.groupDrawmode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.myPosDefs)).BeginInit();
            this.panelAlignments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picProperties)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timer1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelToolbar
            // 
            this.panelToolbar.Controls.Add(this.udGridSize);
            this.panelToolbar.Controls.Add(this.btnDeleteAll);
            this.panelToolbar.Controls.Add(this.cbShowGrid);
            this.panelToolbar.Controls.Add(this.lblGridsize);
            this.panelToolbar.Controls.Add(this.btnSave);
            this.panelToolbar.Controls.Add(this.button1);
            this.panelToolbar.Controls.Add(this.picToolbar);
            this.panelToolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelToolbar.Location = new System.Drawing.Point(0, 0);
            this.panelToolbar.Name = "panelToolbar";
            this.panelToolbar.Size = new System.Drawing.Size(1028, 28);
            this.panelToolbar.TabIndex = 0;
            // 
            // udGridSize
            // 
            this.udGridSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.udGridSize.Location = new System.Drawing.Point(332, 4);
            this.udGridSize.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.udGridSize.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.udGridSize.Name = "udGridSize";
            this.udGridSize.Size = new System.Drawing.Size(58, 20);
            this.udGridSize.TabIndex = 29;
            this.toolTip1.SetToolTip(this.udGridSize, "Sets the size of aircraft images to scale");
            this.udGridSize.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.udGridSize.ValueChanged += new System.EventHandler(this.udGridSize_ValueChanged);
            // 
            // btnDeleteAll
            // 
            this.btnDeleteAll.BackColor = System.Drawing.Color.Transparent;
            this.btnDeleteAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDeleteAll.ImageIndex = 0;
            this.btnDeleteAll.ImageList = this.imageListSmarIcons;
            this.btnDeleteAll.Location = new System.Drawing.Point(548, 2);
            this.btnDeleteAll.Name = "btnDeleteAll";
            this.btnDeleteAll.Size = new System.Drawing.Size(104, 24);
            this.btnDeleteAll.TabIndex = 6;
            this.btnDeleteAll.Text = "&Delete All";
            this.btnDeleteAll.UseVisualStyleBackColor = false;
            this.btnDeleteAll.Click += new System.EventHandler(this.btnDeleteAll_Click);
            // 
            // imageListSmarIcons
            // 
            this.imageListSmarIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListSmarIcons.ImageStream")));
            this.imageListSmarIcons.TransparentColor = System.Drawing.Color.White;
            this.imageListSmarIcons.Images.SetKeyName(0, "");
            this.imageListSmarIcons.Images.SetKeyName(1, "");
            this.imageListSmarIcons.Images.SetKeyName(2, "");
            // 
            // cbShowGrid
            // 
            this.cbShowGrid.BackColor = System.Drawing.Color.Transparent;
            this.cbShowGrid.Checked = true;
            this.cbShowGrid.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbShowGrid.Location = new System.Drawing.Point(404, 7);
            this.cbShowGrid.Name = "cbShowGrid";
            this.cbShowGrid.Size = new System.Drawing.Size(92, 16);
            this.cbShowGrid.TabIndex = 4;
            this.cbShowGrid.Text = "Show &Grid";
            this.cbShowGrid.UseVisualStyleBackColor = false;
            this.cbShowGrid.CheckedChanged += new System.EventHandler(this.cbShowGrid_CheckedChanged);
            // 
            // lblGridsize
            // 
            this.lblGridsize.BackColor = System.Drawing.Color.Transparent;
            this.lblGridsize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGridsize.Location = new System.Drawing.Point(268, 7);
            this.lblGridsize.Name = "lblGridsize";
            this.lblGridsize.Size = new System.Drawing.Size(56, 16);
            this.lblGridsize.TabIndex = 3;
            this.lblGridsize.Text = "Grid Size:";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.ImageIndex = 1;
            this.btnSave.ImageList = this.imageListSmarIcons;
            this.btnSave.Location = new System.Drawing.Point(120, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(88, 24);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.ImageIndex = 2;
            this.button1.ImageList = this.imageListSmarIcons;
            this.button1.Location = new System.Drawing.Point(2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 24);
            this.button1.TabIndex = 0;
            this.button1.Text = "&Open Image";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // picToolbar
            // 
            this.picToolbar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picToolbar.Location = new System.Drawing.Point(0, 0);
            this.picToolbar.Name = "picToolbar";
            this.picToolbar.Size = new System.Drawing.Size(1028, 28);
            this.picToolbar.TabIndex = 5;
            this.picToolbar.TabStop = false;
            this.picToolbar.Paint += new System.Windows.Forms.PaintEventHandler(this.picToolbar_Paint);
            // 
            // panelProperties
            // 
            this.panelProperties.AutoScroll = true;
            this.panelProperties.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelProperties.Controls.Add(this.label1);
            this.panelProperties.Controls.Add(this.txtType);
            this.panelProperties.Controls.Add(this.lblPosType);
            this.panelProperties.Controls.Add(this.lblAlignments);
            this.panelProperties.Controls.Add(this.lbGates);
            this.panelProperties.Controls.Add(this.lblGates);
            this.panelProperties.Controls.Add(this.cbAutoRotate);
            this.panelProperties.Controls.Add(this.udDefaultTimeframe);
            this.panelProperties.Controls.Add(this.lblDefaulTimeFrame);
            this.panelProperties.Controls.Add(this.lblImageSize);
            this.panelProperties.Controls.Add(this.udImageSize);
            this.panelProperties.Controls.Add(this.groupDrawmode);
            this.panelProperties.Controls.Add(this.myPosDefs);
            this.panelProperties.Controls.Add(this.panelAlignments);
            this.panelProperties.Controls.Add(this.label5);
            this.panelProperties.Controls.Add(this.lbPositions);
            this.panelProperties.Controls.Add(this.lblPositions);
            this.panelProperties.Controls.Add(this.txtFileName);
            this.panelProperties.Controls.Add(this.lblFileName);
            this.panelProperties.Controls.Add(this.txtViewPointName);
            this.panelProperties.Controls.Add(this.lblViewName);
            this.panelProperties.Controls.Add(this.picProperties);
            this.panelProperties.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelProperties.Location = new System.Drawing.Point(764, 28);
            this.panelProperties.Name = "panelProperties";
            this.panelProperties.Size = new System.Drawing.Size(264, 538);
            this.panelProperties.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 16);
            this.label1.TabIndex = 38;
            this.label1.Text = "View-Point Properties";
            // 
            // txtType
            // 
            this.txtType.BackColor = System.Drawing.Color.Transparent;
            this.txtType.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtType.ForeColor = System.Drawing.Color.Aqua;
            this.txtType.Location = new System.Drawing.Point(130, 328);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(100, 23);
            this.txtType.TabIndex = 37;
            this.txtType.Text = "GATE/POS";
            this.txtType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPosType
            // 
            this.lblPosType.BackColor = System.Drawing.Color.Transparent;
            this.lblPosType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPosType.Location = new System.Drawing.Point(40, 330);
            this.lblPosType.Name = "lblPosType";
            this.lblPosType.Size = new System.Drawing.Size(80, 18);
            this.lblPosType.TabIndex = 36;
            this.lblPosType.Text = "Position Type:";
            // 
            // lblAlignments
            // 
            this.lblAlignments.BackColor = System.Drawing.Color.Transparent;
            this.lblAlignments.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlignments.Location = new System.Drawing.Point(40, 358);
            this.lblAlignments.Name = "lblAlignments";
            this.lblAlignments.Size = new System.Drawing.Size(182, 16);
            this.lblAlignments.TabIndex = 21;
            this.lblAlignments.Text = "Alignment of flight information:";
            // 
            // lbGates
            // 
            this.lbGates.BackColor = System.Drawing.Color.Silver;
            this.lbGates.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGates.Location = new System.Drawing.Point(136, 90);
            this.lbGates.Name = "lbGates";
            this.lbGates.Size = new System.Drawing.Size(114, 147);
            this.lbGates.Sorted = true;
            this.lbGates.TabIndex = 34;
            this.toolTip1.SetToolTip(this.lbGates, "All airport GATES. You can Drag & Drop them onto the airport image");
            this.lbGates.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbGates_MouseDown);
            // 
            // lblGates
            // 
            this.lblGates.BackColor = System.Drawing.Color.Transparent;
            this.lblGates.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGates.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGates.Location = new System.Drawing.Point(136, 74);
            this.lblGates.Name = "lblGates";
            this.lblGates.Size = new System.Drawing.Size(114, 16);
            this.lblGates.TabIndex = 33;
            this.lblGates.Text = "Available Gates:";
            // 
            // cbAutoRotate
            // 
            this.cbAutoRotate.BackColor = System.Drawing.Color.Transparent;
            this.cbAutoRotate.Checked = true;
            this.cbAutoRotate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAutoRotate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAutoRotate.Location = new System.Drawing.Point(22, 460);
            this.cbAutoRotate.Name = "cbAutoRotate";
            this.cbAutoRotate.Size = new System.Drawing.Size(166, 24);
            this.cbAutoRotate.TabIndex = 32;
            this.cbAutoRotate.Text = "Automatic &Rotation (180�)";
            this.cbAutoRotate.UseVisualStyleBackColor = false;
            this.cbAutoRotate.CheckedChanged += new System.EventHandler(this.cbAutoRotate_CheckedChanged);
            // 
            // udDefaultTimeframe
            // 
            this.udDefaultTimeframe.Location = new System.Drawing.Point(160, 512);
            this.udDefaultTimeframe.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.udDefaultTimeframe.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udDefaultTimeframe.Name = "udDefaultTimeframe";
            this.udDefaultTimeframe.Size = new System.Drawing.Size(66, 20);
            this.udDefaultTimeframe.TabIndex = 31;
            this.toolTip1.SetToolTip(this.udDefaultTimeframe, "When flight has no further information than scheduled, the display time start at " +
                    "STA and ends <x> min. later");
            this.udDefaultTimeframe.Value = new decimal(new int[] {
            45,
            0,
            0,
            0});
            this.udDefaultTimeframe.Visible = false;
            // 
            // lblDefaulTimeFrame
            // 
            this.lblDefaulTimeFrame.BackColor = System.Drawing.Color.Transparent;
            this.lblDefaulTimeFrame.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDefaulTimeFrame.Location = new System.Drawing.Point(28, 514);
            this.lblDefaulTimeFrame.Name = "lblDefaulTimeFrame";
            this.lblDefaulTimeFrame.Size = new System.Drawing.Size(124, 14);
            this.lblDefaulTimeFrame.TabIndex = 30;
            this.lblDefaulTimeFrame.Text = "Def. timeframe in min.:";
            this.lblDefaulTimeFrame.Visible = false;
            // 
            // lblImageSize
            // 
            this.lblImageSize.BackColor = System.Drawing.Color.Transparent;
            this.lblImageSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImageSize.Location = new System.Drawing.Point(64, 242);
            this.lblImageSize.Name = "lblImageSize";
            this.lblImageSize.Size = new System.Drawing.Size(66, 16);
            this.lblImageSize.TabIndex = 29;
            this.lblImageSize.Text = "Image Size:";
            // 
            // udImageSize
            // 
            this.udImageSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.udImageSize.Increment = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.udImageSize.Location = new System.Drawing.Point(136, 240);
            this.udImageSize.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
            this.udImageSize.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.udImageSize.Name = "udImageSize";
            this.udImageSize.Size = new System.Drawing.Size(58, 20);
            this.udImageSize.TabIndex = 28;
            this.toolTip1.SetToolTip(this.udImageSize, "Sets the size of aircraft images to scale");
            this.udImageSize.Value = new decimal(new int[] {
            56,
            0,
            0,
            0});
            this.udImageSize.ValueChanged += new System.EventHandler(this.udImageSize_ValueChanged);
            this.udImageSize.Leave += new System.EventHandler(this.udImageSize_Leave);
            // 
            // groupDrawmode
            // 
            this.groupDrawmode.BackColor = System.Drawing.Color.Transparent;
            this.groupDrawmode.Controls.Add(this.rbAsAircraft);
            this.groupDrawmode.Controls.Add(this.rbAsCircle);
            this.groupDrawmode.Controls.Add(this.rbAsText);
            this.groupDrawmode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupDrawmode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.groupDrawmode.Location = new System.Drawing.Point(4, 264);
            this.groupDrawmode.Name = "groupDrawmode";
            this.groupDrawmode.Size = new System.Drawing.Size(256, 40);
            this.groupDrawmode.TabIndex = 27;
            this.groupDrawmode.TabStop = false;
            this.groupDrawmode.Text = "Draw mode:";
            // 
            // rbAsAircraft
            // 
            this.rbAsAircraft.Checked = true;
            this.rbAsAircraft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbAsAircraft.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbAsAircraft.Location = new System.Drawing.Point(168, 20);
            this.rbAsAircraft.Name = "rbAsAircraft";
            this.rbAsAircraft.Size = new System.Drawing.Size(84, 16);
            this.rbAsAircraft.TabIndex = 26;
            this.rbAsAircraft.TabStop = true;
            this.rbAsAircraft.Text = "As &Aircraft";
            this.toolTip1.SetToolTip(this.rbAsAircraft, "Show the positions/gates as A/C images");
            this.rbAsAircraft.CheckedChanged += new System.EventHandler(this.Change_DrawMode);
            // 
            // rbAsCircle
            // 
            this.rbAsCircle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbAsCircle.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbAsCircle.Location = new System.Drawing.Point(6, 20);
            this.rbAsCircle.Name = "rbAsCircle";
            this.rbAsCircle.Size = new System.Drawing.Size(72, 16);
            this.rbAsCircle.TabIndex = 24;
            this.rbAsCircle.Text = "As &Circle";
            this.toolTip1.SetToolTip(this.rbAsCircle, "Show the positions/gates as circle symbols");
            this.rbAsCircle.CheckedChanged += new System.EventHandler(this.Change_DrawMode);
            // 
            // rbAsText
            // 
            this.rbAsText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbAsText.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbAsText.Location = new System.Drawing.Point(88, 20);
            this.rbAsText.Name = "rbAsText";
            this.rbAsText.Size = new System.Drawing.Size(66, 16);
            this.rbAsText.TabIndex = 25;
            this.rbAsText.Text = "As &Text";
            this.toolTip1.SetToolTip(this.rbAsText, "Show the position/gates as text");
            this.rbAsText.CheckedChanged += new System.EventHandler(this.Change_DrawMode);
            // 
            // myPosDefs
            // 
            this.myPosDefs.Location = new System.Drawing.Point(8, 468);
            this.myPosDefs.Name = "myPosDefs";
            this.myPosDefs.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("myPosDefs.OcxState")));
            this.myPosDefs.Size = new System.Drawing.Size(248, 48);
            this.myPosDefs.TabIndex = 22;
            this.myPosDefs.Visible = false;
            // 
            // panelAlignments
            // 
            this.panelAlignments.BackColor = System.Drawing.Color.Transparent;
            this.panelAlignments.Controls.Add(this.rbLeftBottom);
            this.panelAlignments.Controls.Add(this.rbBottom);
            this.panelAlignments.Controls.Add(this.txtCurrPos);
            this.panelAlignments.Controls.Add(this.rbLeft);
            this.panelAlignments.Controls.Add(this.rbRightTop);
            this.panelAlignments.Controls.Add(this.rbRightBottom);
            this.panelAlignments.Controls.Add(this.rbTop);
            this.panelAlignments.Controls.Add(this.rbRight);
            this.panelAlignments.Controls.Add(this.rbLeftTop);
            this.panelAlignments.Location = new System.Drawing.Point(32, 376);
            this.panelAlignments.Name = "panelAlignments";
            this.panelAlignments.Size = new System.Drawing.Size(202, 80);
            this.panelAlignments.TabIndex = 20;
            // 
            // rbLeftBottom
            // 
            this.rbLeftBottom.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbLeftBottom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbLeftBottom.ImageIndex = 6;
            this.rbLeftBottom.ImageList = this.imageListArrows;
            this.rbLeftBottom.Location = new System.Drawing.Point(8, 52);
            this.rbLeftBottom.Name = "rbLeftBottom";
            this.rbLeftBottom.Size = new System.Drawing.Size(62, 24);
            this.rbLeftBottom.TabIndex = 18;
            this.rbLeftBottom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbLeftBottom.CheckedChanged += new System.EventHandler(this.rbLeftTop_CheckedChanged);
            // 
            // imageListArrows
            // 
            this.imageListArrows.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListArrows.ImageStream")));
            this.imageListArrows.TransparentColor = System.Drawing.Color.White;
            this.imageListArrows.Images.SetKeyName(0, "");
            this.imageListArrows.Images.SetKeyName(1, "");
            this.imageListArrows.Images.SetKeyName(2, "");
            this.imageListArrows.Images.SetKeyName(3, "");
            this.imageListArrows.Images.SetKeyName(4, "");
            this.imageListArrows.Images.SetKeyName(5, "");
            this.imageListArrows.Images.SetKeyName(6, "");
            this.imageListArrows.Images.SetKeyName(7, "");
            // 
            // rbBottom
            // 
            this.rbBottom.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbBottom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbBottom.ImageIndex = 5;
            this.rbBottom.ImageList = this.imageListArrows;
            this.rbBottom.Location = new System.Drawing.Point(70, 52);
            this.rbBottom.Name = "rbBottom";
            this.rbBottom.Size = new System.Drawing.Size(62, 24);
            this.rbBottom.TabIndex = 17;
            this.rbBottom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbBottom.CheckedChanged += new System.EventHandler(this.rbLeftTop_CheckedChanged);
            // 
            // txtCurrPos
            // 
            this.txtCurrPos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrPos.Location = new System.Drawing.Point(69, 29);
            this.txtCurrPos.Name = "txtCurrPos";
            this.txtCurrPos.Size = new System.Drawing.Size(62, 20);
            this.txtCurrPos.TabIndex = 11;
            this.txtCurrPos.Text = "<POS>";
            this.txtCurrPos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // rbLeft
            // 
            this.rbLeft.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbLeft.ImageIndex = 7;
            this.rbLeft.ImageList = this.imageListArrows;
            this.rbLeft.Location = new System.Drawing.Point(8, 28);
            this.rbLeft.Name = "rbLeft";
            this.rbLeft.Size = new System.Drawing.Size(62, 24);
            this.rbLeft.TabIndex = 19;
            this.rbLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbLeft.CheckedChanged += new System.EventHandler(this.rbLeftTop_CheckedChanged);
            // 
            // rbRightTop
            // 
            this.rbRightTop.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbRightTop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbRightTop.ImageIndex = 2;
            this.rbRightTop.ImageList = this.imageListArrows;
            this.rbRightTop.Location = new System.Drawing.Point(132, 4);
            this.rbRightTop.Name = "rbRightTop";
            this.rbRightTop.Size = new System.Drawing.Size(62, 24);
            this.rbRightTop.TabIndex = 14;
            this.rbRightTop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbRightTop.CheckedChanged += new System.EventHandler(this.rbLeftTop_CheckedChanged);
            // 
            // rbRightBottom
            // 
            this.rbRightBottom.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbRightBottom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbRightBottom.ImageIndex = 4;
            this.rbRightBottom.ImageList = this.imageListArrows;
            this.rbRightBottom.Location = new System.Drawing.Point(132, 52);
            this.rbRightBottom.Name = "rbRightBottom";
            this.rbRightBottom.Size = new System.Drawing.Size(62, 24);
            this.rbRightBottom.TabIndex = 16;
            this.rbRightBottom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbRightBottom.CheckedChanged += new System.EventHandler(this.rbLeftTop_CheckedChanged);
            // 
            // rbTop
            // 
            this.rbTop.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbTop.BackColor = System.Drawing.Color.LimeGreen;
            this.rbTop.Checked = true;
            this.rbTop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTop.ImageIndex = 1;
            this.rbTop.ImageList = this.imageListArrows;
            this.rbTop.Location = new System.Drawing.Point(70, 4);
            this.rbTop.Name = "rbTop";
            this.rbTop.Size = new System.Drawing.Size(62, 24);
            this.rbTop.TabIndex = 13;
            this.rbTop.TabStop = true;
            this.rbTop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbTop.UseVisualStyleBackColor = false;
            this.rbTop.CheckedChanged += new System.EventHandler(this.rbLeftTop_CheckedChanged);
            // 
            // rbRight
            // 
            this.rbRight.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbRight.ImageIndex = 3;
            this.rbRight.ImageList = this.imageListArrows;
            this.rbRight.Location = new System.Drawing.Point(132, 28);
            this.rbRight.Name = "rbRight";
            this.rbRight.Size = new System.Drawing.Size(62, 24);
            this.rbRight.TabIndex = 15;
            this.rbRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbRight.CheckedChanged += new System.EventHandler(this.rbLeftTop_CheckedChanged);
            // 
            // rbLeftTop
            // 
            this.rbLeftTop.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbLeftTop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbLeftTop.ImageIndex = 0;
            this.rbLeftTop.ImageList = this.imageListArrows;
            this.rbLeftTop.Location = new System.Drawing.Point(8, 4);
            this.rbLeftTop.Name = "rbLeftTop";
            this.rbLeftTop.Size = new System.Drawing.Size(62, 24);
            this.rbLeftTop.TabIndex = 12;
            this.rbLeftTop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbLeftTop.Paint += new System.Windows.Forms.PaintEventHandler(this.rbLeftTop_Paint);
            this.rbLeftTop.CheckedChanged += new System.EventHandler(this.rbLeftTop_CheckedChanged);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Black;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Lime;
            this.label5.Location = new System.Drawing.Point(4, 306);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(254, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Current Position\'s Properties";
            // 
            // lbPositions
            // 
            this.lbPositions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lbPositions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPositions.Location = new System.Drawing.Point(4, 90);
            this.lbPositions.Name = "lbPositions";
            this.lbPositions.Size = new System.Drawing.Size(114, 147);
            this.lbPositions.Sorted = true;
            this.lbPositions.TabIndex = 8;
            this.toolTip1.SetToolTip(this.lbPositions, "All airport positions. You can Drag & Drop them onto the airport image");
            this.lbPositions.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbPositions_MouseDown);
            // 
            // lblPositions
            // 
            this.lblPositions.BackColor = System.Drawing.Color.Transparent;
            this.lblPositions.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPositions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPositions.Location = new System.Drawing.Point(4, 74);
            this.lblPositions.Name = "lblPositions";
            this.lblPositions.Size = new System.Drawing.Size(114, 16);
            this.lblPositions.TabIndex = 7;
            this.lblPositions.Text = "Available Positions:";
            // 
            // txtFileName
            // 
            this.txtFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFileName.Location = new System.Drawing.Point(66, 44);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(190, 20);
            this.txtFileName.TabIndex = 4;
            this.txtFileName.Text = "<None>";
            this.toolTip1.SetToolTip(this.txtFileName, "File name of the view point\'s image");
            // 
            // lblFileName
            // 
            this.lblFileName.BackColor = System.Drawing.Color.Transparent;
            this.lblFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileName.Location = new System.Drawing.Point(8, 44);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(48, 16);
            this.lblFileName.TabIndex = 3;
            this.lblFileName.Text = "File:";
            // 
            // txtViewPointName
            // 
            this.txtViewPointName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtViewPointName.Location = new System.Drawing.Point(66, 18);
            this.txtViewPointName.Name = "txtViewPointName";
            this.txtViewPointName.Size = new System.Drawing.Size(190, 20);
            this.txtViewPointName.TabIndex = 2;
            this.txtViewPointName.Text = "myView";
            this.toolTip1.SetToolTip(this.txtViewPointName, "Name of the view point");
            // 
            // lblViewName
            // 
            this.lblViewName.BackColor = System.Drawing.Color.Transparent;
            this.lblViewName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblViewName.Location = new System.Drawing.Point(8, 20);
            this.lblViewName.Name = "lblViewName";
            this.lblViewName.Size = new System.Drawing.Size(54, 16);
            this.lblViewName.TabIndex = 1;
            this.lblViewName.Text = "V-Name:";
            // 
            // picProperties
            // 
            this.picProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picProperties.Location = new System.Drawing.Point(0, 0);
            this.picProperties.Name = "picProperties";
            this.picProperties.Size = new System.Drawing.Size(260, 534);
            this.picProperties.TabIndex = 0;
            this.picProperties.TabStop = false;
            this.picProperties.Paint += new System.Windows.Forms.PaintEventHandler(this.picProperties_Paint);
            // 
            // panel1
            // 
            this.panel1.AllowDrop = true;
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.picArea);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(764, 538);
            this.panel1.TabIndex = 4;
            this.panel1.DragOver += new System.Windows.Forms.DragEventHandler(this.panel1_DragOver);
            this.panel1.DragDrop += new System.Windows.Forms.DragEventHandler(this.panel1_DragDrop);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // picArea
            // 
            this.picArea.ContextMenu = this.contextMenuPositions;
            this.picArea.Location = new System.Drawing.Point(0, 0);
            this.picArea.Name = "picArea";
            this.picArea.Size = new System.Drawing.Size(256, 60);
            this.picArea.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picArea.TabIndex = 0;
            this.picArea.TabStop = false;
            this.picArea.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picArea_MouseMove);
            this.picArea.Click += new System.EventHandler(this.picArea_Click);
            this.picArea.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picArea_MouseDown);
            this.picArea.Paint += new System.Windows.Forms.PaintEventHandler(this.picArea_Paint);
            // 
            // contextMenuPositions
            // 
            this.contextMenuPositions.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuDelete});
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 0;
            this.mnuDelete.Text = "&Delete current selected";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // timer1
            // 
            this.timer1.SynchronizingObject = this;
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Elapsed);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.White;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "");
            this.imageList1.Images.SetKeyName(6, "");
            this.imageList1.Images.SetKeyName(7, "");
            this.imageList1.Images.SetKeyName(8, "");
            this.imageList1.Images.SetKeyName(9, "");
            this.imageList1.Images.SetKeyName(10, "");
            this.imageList1.Images.SetKeyName(11, "");
            this.imageList1.Images.SetKeyName(12, "");
            // 
            // imageListLarge
            // 
            this.imageListLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLarge.ImageStream")));
            this.imageListLarge.TransparentColor = System.Drawing.Color.White;
            this.imageListLarge.Images.SetKeyName(0, "");
            this.imageListLarge.Images.SetKeyName(1, "");
            this.imageListLarge.Images.SetKeyName(2, "");
            this.imageListLarge.Images.SetKeyName(3, "");
            this.imageListLarge.Images.SetKeyName(4, "");
            this.imageListLarge.Images.SetKeyName(5, "");
            this.imageListLarge.Images.SetKeyName(6, "");
            this.imageListLarge.Images.SetKeyName(7, "");
            this.imageListLarge.Images.SetKeyName(8, "");
            this.imageListLarge.Images.SetKeyName(9, "");
            this.imageListLarge.Images.SetKeyName(10, "");
            this.imageListLarge.Images.SetKeyName(11, "");
            this.imageListLarge.Images.SetKeyName(12, "");
            // 
            // frmViewPoint
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1028, 566);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelProperties);
            this.Controls.Add(this.panelToolbar);
            this.Name = "frmViewPoint";
            this.Text = "Viewpoint: <Airport Area>";
            this.Load += new System.EventHandler(this.frmViewPoint_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmViewPoint_Closing);
            this.panelToolbar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.udGridSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picToolbar)).EndInit();
            this.panelProperties.ResumeLayout(false);
            this.panelProperties.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udDefaultTimeframe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udImageSize)).EndInit();
            this.groupDrawmode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.myPosDefs)).EndInit();
            this.panelAlignments.ResumeLayout(false);
            this.panelAlignments.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picProperties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timer1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private void button1_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "TIF-filed (*.tif)|*.tif|bmp-files (*.bmp)|*.bmp|GIF-files (*.gif)|*.gif";//"All filed (*.*)|*.*";
			if(dlg.ShowDialog() == DialogResult.OK)
			{
				picArea.Image = new Bitmap(dlg.FileName);
				imageLoaded = true;
				txtFileName.Text = dlg.FileName;
			}
		}

		private void picArea_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			Point pScrollPos = panel1.AutoScrollPosition;
			currMouseX = e.X;
			currMouseY = e.Y;
			currMarkerX = currMouseX;// + pScrollPos.X;
			currMarkerY = currMouseY;// + pScrollPos.Y;
			this.Text = "Viewpoint " + ViewPointName + " X: " + currMouseX.ToString() + " Y: " + currMouseY.ToString();
			if(e.Button == MouseButtons.Left)
			{
				if(currPosTabLine > -1)
				{
					if(bmGridActive == true)
					{
						SnapToGrid();
					}
					if((oldX != currMarkerX) || (oldY != currMarkerY)) //optimize unnecessary drawing
					{
						oldX = currMarkerX;
						oldY = currMarkerY;
						myDefs[currPosTabLine]["X"] = currMarkerX.ToString();
						myDefs[currPosTabLine]["Y"] = currMarkerY.ToString();
						picArea.Invalidate();
					}
				}
			}
		}

		private void SnapToGrid()
		{
			int gridStep = 0;
			int restX=0;
			int restY=0;
			if(udGridSize.Value.ToString() != "")
			{
				gridStep = Convert.ToInt32(udGridSize.Value);//Convert.ToInt32(txtGridSize.Text);
				restX = (currMarkerX % gridStep);
				restY = (currMarkerY % gridStep);
				if(restX != gridStep )
				{
					currMarkerX -= restX;
				}
				if(restY != gridStep)
				{
					currMarkerY -= restY;
				}
			}
		}

		private void picArea_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Rectangle myR;
			PointF pointF;
			Brush brushBlack = System.Drawing.Brushes.Black;// Brush brushBlack = new Brush();
			Brush brushAqua  = System.Drawing.Brushes.Aqua;
			Brush brushRed  = System.Drawing.Brushes.Red;
			Brush brushBlue  = System.Drawing.Brushes.Blue;
			Pen penAqua = new Pen(Brushes.Aqua, 2);
			Pen penRed = new Pen(Brushes.Red, 4);
			Pen penGray = new Pen(Brushes.Purple, 1);
			penGray.DashStyle = DashStyle.Dash;

			int scrollX = panel1.AutoScrollPosition.X;
			int scrollY = panel1.AutoScrollPosition.Y;
			Graphics g = e.Graphics;
			DrawGrid(g);
			oldScrollX = scrollX;
			oldScrollY = scrollY;
			if ((currMarkerX != -1) && (currMarkerY != -1))
			{
				for(int i = 0; i < myDefs.Count; i++)
				{
					String olPos = myDefs[i]["POS"];
					int x = -1;
					int y = -1;
					String strX = myDefs[i]["X"];
					String strY = myDefs[i]["Y"];
					String strAlign = myDefs[i]["ALIGN"];
					string strType = myDefs[i]["TYPE"];
					if(strX != "" && strY != "")
					{
						x = Convert.ToInt32(strX);
						y = Convert.ToInt32(strY);
						int iw = (int)udImageSize.Value;
						int ih = iw;
						int ix = x - (iw/2);
						int iy = y - (ih/2);
						int cx = x + (iw/2);
						int cy = y + (ih/2);
						int nx = (iw/2)+ix;
						int ny = (ih/2)+iy;
						Point [] myPoints;

						myR = new Rectangle(x - (iw/2), y - (ih/2), iw, ih);
						if(strType == "GATE")
						{
							Rectangle tmpRect = new Rectangle(myR.X+15, myR.Y+15, myR.Width-30, myR.Height-30);
							g.DrawRectangle(penGray, tmpRect);
						}
						switch (strDrawMode)
						{
							case "TEXT":
								pointF = new PointF((float)x-15, (float)y-(myFontSize/2));//-15);
								if(i == currPosTabLine)
								{
									g.DrawString(olPos, omFont, brushRed, pointF);
								}
								else
								{
									g.DrawString(olPos, omFont, brushBlue, pointF);
								}
								break;
							case "CIRCLE":
								if(i == currPosTabLine)
								{
									g.DrawEllipse(penRed, x-10, y-10, 20,20);
								}
								else
								{
									g.DrawEllipse(penAqua, x-10, y-10, 20,20);
								}
								break;
							case "AIRCRAFT":
								try
								{
									//g.DrawImage(myImageList.Images[0], myR);
									Image im;// = new Image();
								
									if(i == currPosTabLine)
									{
										if(iw > 100) im = (Image)imageListLarge.Images[1];//.Clone();
										else im = (Image)imageList1.Images[1];//.Clone();
									}
									else
									{
										if(iw > 100) im = (Image)imageListLarge.Images[0];//.Clone();
										else im = (Image)imageList1.Images[0];//.Clone();
									}
									switch(strAlign)
									{
										case "0":
											myPoints = new Point[] {new Point(nx,iy+ih+(ih/6)),
																	   new Point(ix-(iw/6), ny),
																	   new Point(ix+iw+(iw/6), ny)};
											g.DrawImage(im, myPoints);
											break;
										case "1":
											//im.RotateFlip(RotateFlipType.Rotate270FlipNone);
											myPoints = new Point[] {new Point(myR.Left, myR.Bottom),
																	new Point(myR.Left, myR.Top),
																	new Point(myR.Right,  myR.Bottom)};
											g.DrawImage(im, myPoints);
											break;
										case "2":// 315�
											myPoints = new Point[] {new Point(ix-(iw/6), ny),
																	new Point(nx,        iy-(ih/6)),
																	new Point(nx,        iy+ih+(ih/6))};
											g.DrawImage(im, myPoints);
											break;
										case "3":
											g.DrawImage(im, myR);
											break;
										case "4": //45�
											myPoints = new Point[] {new Point(nx,           iy-(ih/6)),
																	new Point(ix+iw+(iw/6), ny),
																	new Point(ix-(iw/6),    ny)};
											g.DrawImage(im, myPoints);
											break;
										case "5"://90�
											//im.RotateFlip(RotateFlipType.Rotate90FlipNone);
											myPoints = new Point[] {new Point(myR.Right, myR.Top),
																	new Point(myR.Right, myR.Bottom),
																	new Point(myR.Left,  myR.Top)};
											g.DrawImage(im, myPoints);
											break;
										case "6":
											myPoints = new Point[] {new Point(ix+iw+(iw/6), ny),
																	new Point(nx,           iy+ih+(ih/6)),
																	new Point(nx,           iy-(ih/6))};
											g.DrawImage(im, myPoints);
											break;
										case "7"://180�
											myPoints = new Point[] {new Point(myR.Right, myR.Bottom),
																	   new Point(myR.Left, myR.Bottom),
																	   new Point(myR.Right,  myR.Top)};
											g.DrawImage(im, myPoints);
											break;
										default:
											break;
									}
								}
								catch(System.Exception err)
								{
									MessageBox.Show(err.Message);
								}
								break;
							default:
								break;
						}
					}
				}
				if(strCurrPos != "") 
				{
					//myR = new Rectangle(currMarkerX - 20, currMarkerY - 20, 40,40);
					//g.DrawRectangle(myMarkPen, myR);
				}
			}
			penAqua.Dispose();
			penRed.Dispose();
			penGray.Dispose();
		}


		private void DrawGrid(Graphics g)
		{
			if(cbShowGrid.Checked == true)
			{
				int ilW = this.Width+10000; //picArea.Width;
				int ilH = this.Height+10000; //picArea.Height;
				Rectangle omR;
				int gridStep = 1;
				if(udGridSize.Value > 0)
				{
					gridStep = Convert.ToInt32(udGridSize.Value);//Convert.ToInt32(txtGridSize.Text);
				}
				if(gridStep > 2) bmGridActive = true;

				omR = new Rectangle(picArea.Left, picArea.Top, ilW, ilH);
				Size olS = new Size(gridStep, gridStep);
				ControlPaint.DrawGrid(g, omR, olS, Color.Gray);
			}
		}

		private void frmViewPoint_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			int i = 0;
			omFont = new Font("Arial", myFontSize, FontStyle.Bold);
//			this.Top = omParent.Bottom;
//			this.Left = 0;
//			this.Width = Screen.PrimaryScreen.WorkingArea.Width;
//			this.Height = Screen.PrimaryScreen.WorkingArea.Height - omParent.Bottom;
			omRbs.Add(rbLeftTop);
			omRbs.Add(rbTop);
			omRbs.Add(rbRightTop);
			omRbs.Add(rbRight);
			omRbs.Add(rbRightBottom);
			omRbs.Add(rbBottom);
			omRbs.Add(rbLeftBottom);
			omRbs.Add(rbLeft);
			
			txtType.Text = "";
			OrganizeParents();

			if(omMode == "NEW")
			{
				int cnt = omHiddenData.tabVPD.GetLineCount()+1;
				txtViewPointName.Text = "NewView" + cnt.ToString();
				myDBObjectString = "DEFS" + txtViewPointName.Text;
				myDefs = myDB.Bind(myDBObjectString, "", "POS,X,Y,ALIGN,ROTATE,TYPE", "10,10,10,1,1,1", "");
			}
			if(omMode == "EDIT")
			{
				String strFile="";
				String strGrid="";
				for( i = 0; i < omHiddenData.tabVPD.GetLineCount(); i++)
				{
					if(omHiddenData.tabVPD.GetFieldValue(i,"VIEW") == omViewPoint)
					{
						strFile = omHiddenData.tabVPD.GetFieldValue(i,"FILE");
						strGrid = omHiddenData.tabVPD.GetFieldValue(i,"GRID");
						String strDefTimeframe = omHiddenData.tabVPD.GetFieldValue(i, "DEFTIMEFR");
						String strImageSize = omHiddenData.tabVPD.GetFieldValue(i, "IMAGESIZE");
						udImageSize.Value = Convert.ToInt32(strImageSize);
						udDefaultTimeframe.Value = Convert.ToInt32(strDefTimeframe);
						String strDrawMode = omHiddenData.tabVPD.GetFieldValue(i, "DRAWMODE");
						switch(strDrawMode)
						{
							case "T": //as Text
								rbAsText.Checked = true;
								break;
							case "A": //as Aircraft
								rbAsAircraft.Checked = true;
								break;
							case "C": //as Circle
								rbAsCircle.Checked = true;
								break;
						}
						udGridSize.Value = Convert.ToInt32(strGrid);
						//txtGridSize.Text = strGrid;
						i = omHiddenData.tabVPD.GetLineCount();
					}
				}
				if(strFile != "")
				{
					try
					{
						picArea.Image = new Bitmap(strFile);
						imageLoaded = true;
						txtFileName.Text = strFile;
					}
					catch(Exception err)
					{
						string strMessage = "";
						strMessage += err.Message.ToString() + "\n\n";
						strMessage += "Bitmap file cannot be opened!!\n";
						strMessage += strFile + "\n\n";
						strMessage += "Please copy the airport bitmaps to the correct location!!!\n";
						MessageBox.Show(this.Owner, strMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        //AM 20080422 - Use 'this.Owner' instead of 'this' - To show the message in Owner Screen
						this.Close();
						return;
					}
				}
				txtViewPointName.Text = omViewPoint;
				myDBObjectString = "DEFS" + txtViewPointName.Text;
				myDefs = myDB.Bind(myDBObjectString, "", "POS,X,Y,ALIGN,ROTATE,TYPE", "10,10,10,1,1,1", "");
				for( i = 0; i < omHiddenData.tabVPL.GetLineCount(); i++)
				{
					// 0    1    2  3 4   5     6      7   8
					//URNO,VIEW,POS,X,Y,ALIGN,ROTATE,TYPE,HOPO
					if(omHiddenData.tabVPL.GetFieldValue(i, "VIEW") == omViewPoint)
					{
						String strValues = omHiddenData.tabVPL.GetFieldValues(i, "POS,X,Y,ALIGN,ROTATE,TYPE");
						myDefs.Create(strValues);
					}
				}
				currMarkerX = -200;
				currMarkerY = -200;
				picArea.Invalidate();
			}
			ReloadPositions();

		}

		public void ReloadPositions()
		{
			int i=0;
			String olS="";
			lbPositions.Items.Clear();
			ITable tabGAT = myDB["GAT"];
			ITable tabPST = myDB["PST"];
			lbPositions.Items.Clear();
			lbGates.Items.Clear();
			for( i = 0; i < tabPST.Count; i++)
			{
				olS = tabPST[i]["PNAM"];
				lbPositions.Items.Add(olS);
			}
			for( i = 0; i < tabGAT.Count; i++)
			{
				olS = tabGAT[i]["GNAM"];
				lbGates.Items.Add(olS);
			}
			//And now remove all already assigned positions
			for(i = 0; i < myDefs.Count; i++)
			{
				String strPos = myDefs[i]["POS"];
				if(myDefs[i]["TYPE"] == "POS")
				{
					int idx = lbPositions.FindStringExact(strPos);
					if(idx != -1)
					{
						lbPositions.Items.RemoveAt(idx);
					}
				}
				else
				{
					int idx = lbGates.FindStringExact(strPos);
					if(idx != -1)
					{
						lbGates.Items.RemoveAt(idx);
					}
				}
			}
		}
		public void OrganizeParents()
		{
			lblViewName.Parent = picProperties;
			lblFileName.Parent = picProperties;
			lblPositions.Parent = picProperties;
			lblGates.Parent = picProperties;
			lblImageSize.Parent = picProperties;
			lblAlignments.Parent = picProperties;
			lblPosType.Parent = picProperties;
			txtType.Parent = picProperties;
			panelAlignments.Parent = picProperties;
			groupDrawmode.Parent = picProperties;
			lblDefaulTimeFrame.Parent = picProperties;
			cbAutoRotate.Parent = picProperties;

			lblGridsize.Parent = picToolbar;
			cbShowGrid.Parent = picToolbar;
			button1.Parent = picToolbar;
			btnSave.Parent = picToolbar;
			btnDeleteAll.Parent = picToolbar;
		}
		private void picArea_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			Rectangle myR;
			int x = 0;
			int y = 0;
			Point p = new Point(0,0);
			String strX,
				strY;
			myR = new Rectangle(currMouseX - 20, currMouseY - 20, 40,40);
			strCurrPos = "";
			currMarkerX = -1;
			currMarkerY = -1;
			currPosTabLine = -1;
			for( int i = 0; i < myDefs.Count; i++)
			{
				strX = myDefs[i]["X"];
				strY = myDefs[i]["Y"];
				if(strX != "" && strY != "")
				{
					x = Convert.ToInt32(strX);
					y = Convert.ToInt32(strY);
					p = new Point(x, y);
					if( myR.Contains(p) == true)
					{
						strCurrPos = myDefs[i]["POS"];
						currPosTabLine = i;
						currMarkerX = x;// - panel1.AutoScrollPosition.X;
						currMarkerY = y;// - panel1.AutoScrollPosition.Y;
						txtCurrPos.Text = strCurrPos;
						txtType.Text = myDefs[i]["TYPE"];
						String strAlign = myDefs[i]["ALIGN"];
						if(strAlign != "")
						{
							int idx = Convert.ToInt32(strAlign);
							RadioButton olR = (RadioButton)omRbs[idx];
							olR.Checked = true;
						}
						if(myDefs[i]["ROTATE"] == "J")
						{
							cbAutoRotate.Checked = true;
						}
						else
						{
							cbAutoRotate.Checked = false;
						}
						if(oldPosTabLine != currPosTabLine)
						{
							picArea.Invalidate();
							oldPosTabLine = currPosTabLine;
						}
						i = myDefs.Count;
					}
				}//END if(strX != "" && strY != "")
			}//END for( int i = 0; i < myDefs.Count; i++)

		}//END picArea_MouseDown(

		private void rbLeftTop_CheckedChanged(object sender, System.EventArgs e)
		{
			for (int i = 0; i < omRbs.Count; i++)		
			{
				RadioButton olR = (RadioButton)omRbs[i];
				if(olR.Checked == true)
				{
					olR.BackColor = Color.LightGreen;
					if(currPosTabLine > -1)
					{
						imCurrentAlign = i;
						myDefs[currPosTabLine]["ALIGN"] = i.ToString();
					}
				}
				else
				{
					olR.BackColor = Color.Transparent;//Color.FromKnownColor(KnownColor.Control);
				}
			}
			picArea.Invalidate();
		}

		private void lbPositions_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			String olS;
			olS = lbPositions.Items[lbPositions.SelectedIndex].ToString()+ ",POS";
			DragDropEffects dde = DoDragDrop(olS, DragDropEffects.Copy);
		}
		private void lbGates_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			String olS;
			olS = lbGates.Items[lbGates.SelectedIndex].ToString()+ ",GATE";
			DragDropEffects dde = DoDragDrop(olS, DragDropEffects.Copy);
		}


		private void panel1_DragOver(object sender, System.Windows.Forms.DragEventArgs e)
		{
			Point myP = new Point(e.X, e.Y);
			myP = panel1.PointToClient(myP);
			currMouseX = myP.X;
			currMouseY = myP.Y;
			this.Text = "Viewpoint " + ViewPointName + " X: " + currMouseX.ToString() + " Y: " + currMouseY.ToString();
			if(e.Data.GetDataPresent(DataFormats.StringFormat) && imageLoaded == true)
			{
				if((e.AllowedEffect  & DragDropEffects.Copy) != 0)
				{
					e.Effect = DragDropEffects.Copy;
				}
			}
		}

		private void panel1_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			String olDropS;
			Point myP = new Point(e.X, e.Y);
			myP = panel1.PointToClient(myP);
			currMarkerX = myP.X;// + panel1.AutoScrollPosition.X;
			currMarkerY = myP.Y;// + panel1.AutoScrollPosition.X;
			currMarkerX = currMouseX - panel1.AutoScrollPosition.X;
			currMarkerY = currMouseY - panel1.AutoScrollPosition.Y;
			this.Text = "Viewpoint " + ViewPointName + " X: " + currMouseX.ToString() + " Y: " + currMouseY.ToString();
			if(e.Data.GetDataPresent(DataFormats.StringFormat))
			{
				String strValues="";
				olDropS = e.Data.GetData(DataFormats.StringFormat).ToString();
				string [] arrDropS = olDropS.Split(',');
				if(arrDropS[1] == "POS")
				{
					strCurrPos = arrDropS[0];
					txtCurrPos.Text = strCurrPos;
					txtType.Text = "POS";

					int idx = lbPositions.FindStringExact(strCurrPos);
					if(idx != -1)
					{
						lbPositions.Items.RemoveAt(idx);
					}
					if(bmGridActive == true)
					{
						SnapToGrid();
					}
					if(cbAutoRotate.Checked == true)
						strValues = strCurrPos + "," + currMarkerX.ToString() + "," + currMarkerY.ToString() + "," + imCurrentAlign.ToString() + ",J,POS" ;
					else
						strValues = strCurrPos + "," + currMarkerX.ToString() + "," + currMarkerY.ToString() + "," + imCurrentAlign.ToString() + ",N,POS" ;
				}
				if(arrDropS[1] == "GATE")
				{
					strCurrPos = arrDropS[0];
					txtCurrPos.Text = strCurrPos;
					txtType.Text = "GATE";
					int idx = lbGates.FindStringExact(strCurrPos);
					if(idx != -1)
					{
						lbGates.Items.RemoveAt(idx);
					}
					if(bmGridActive == true)
					{
						SnapToGrid();
					}
					if(cbAutoRotate.Checked == true)
						strValues = strCurrPos + "," + currMarkerX.ToString() + "," + currMarkerY.ToString() + "," + imCurrentAlign.ToString() + ",J,GATE" ;
					else
						strValues = strCurrPos + "," + currMarkerX.ToString() + "," + currMarkerY.ToString() + "," + imCurrentAlign.ToString() + ",N,GATE" ;
				}
				myDefs.Create(strValues);
				currPosTabLine = myDefs.Count-1;
				picArea.Invalidate();
			}		
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			int i=0;
			String strValues;
			String strName = txtViewPointName.Text;

			//First delete all existing ones in the data form
			for( i = omHiddenData.tabVPD.GetLineCount()-1; i >= 0; i--)
			{
				if(omHiddenData.tabVPD.GetFieldValue(i, "VIEW") == strName)
				{
					omHiddenData.tabVPD.DeleteLine(i);
				}
			}
			for( i = omHiddenData.tabVPL.GetLineCount()-1; i >= 0; i--)
			{
				if(omHiddenData.tabVPL.GetFieldValue(i, "VIEW") == strName)
				{
					omHiddenData.tabVPL.DeleteLine(i);
				}
			}
			//URNO,VIEW,FILE,GRID,DRAWMODE,IMAGESIZE,DEFTIMEFR
			strValues = "," + txtViewPointName.Text + "," + txtFileName.Text + "," + udGridSize.Value.ToString();//txtGridSize.Text;
			strValues += "," + strDrawMode[0].ToString() + "," + udImageSize.Value.ToString() + ",";
			strValues += udDefaultTimeframe.Value.ToString() + ",";
			omHiddenData.tabVPD.InsertTextLine(strValues, true);
			for(i = 0; i < myDefs.Count; i++)
			{
				strValues = "," + strName + "," + myDefs[i].FieldValues() + ",";
				omHiddenData.tabVPL.InsertTextLine(strValues, true);
			}
			
			this.Cursor = Cursors.WaitCursor;
			omHiddenData.SaveAllViewPoints(strName);
			this.Cursor = Cursors.Arrow;
			DE.Call_BirdViewSaved();

		}

		private void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
		}

		private void txtGridSize_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				picArea.Invalidate();
			}
			catch(Exception err)
			{
				string str = "";
				str = err.Message.ToString();
			}
		}

		private void cbShowGrid_CheckedChanged(object sender, System.EventArgs e)
		{
			currMarkerX = -200;
			currMarkerY = -200;
			//currMouseX = -1;
			//currMouseY = -1;
			picArea.Invalidate();
		}

		private void panel1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
		}


		private void Change_DrawMode(object sender, System.EventArgs e)
		{
			if(rbAsCircle.Checked == true)		
				strDrawMode = "CIRCLE";
			if(rbAsAircraft.Checked == true)
				strDrawMode = "AIRCRAFT";
			if(rbAsText.Checked == true)
				strDrawMode = "TEXT";

			picArea.Invalidate();
		}

		private void picArea_Click(object sender, System.EventArgs e)
		{
		
		}

		private void picProperties_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 0f, picProperties, Color.WhiteSmoke, Color.DarkGray);			
		}

		private void udImageSize_ValueChanged(object sender, System.EventArgs e)
		{
			picArea.Invalidate();
		}

		private void picToolbar_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picToolbar, Color.WhiteSmoke, Color.DarkGray);			
		}

		private void udImageSize_Leave(object sender, System.EventArgs e)
		{
			picArea.Invalidate();
		}

		private void rbLeftTop_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
//			Graphics g = e.Graphics;
//			Pen myP = new Pen(Brushes.Black, 2);
//			RadioButton olRB = rbLeftTop;
//			Rectangle olR = new Rectangle(olRB.Left, olRB.Top, olRB.Width, olRB.Height);
//			g.DrawLine(myP, olR.Left, olR.Top, olR.Width - 5, olR.Height - 10);
//			g.DrawLine(myP, olR.Width - 5, olR.Height - 10, olR.Width - 12, olR.Height - 20 );
		}

		private void mnuDelete_Select(object sender, System.EventArgs e)
		{
			if(currPosTabLine > -1)
			{
				myDefs.Remove(currPosTabLine);
				currPosTabLine = -1;
				txtCurrPos.Text = "<None>";
				picArea.Invalidate();
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			if(currPosTabLine > -1)
			{
				String olS = myDefs[currPosTabLine]["POS"];
				myDefs.Remove(currPosTabLine);
				ReloadPositions();
//				if(myDefs[currPosTabLine]["TYPE"] == "POS")
//				{
//					lbPositions.Items.Add(olS);
//				}
//				else
//				{
//					lbGates.Items.Add(olS);
//				}
				currPosTabLine = -1;
				txtCurrPos.Text = "<None>";
				picArea.Invalidate();
			}
		}

		private void btnDeleteAll_Click(object sender, System.EventArgs e)
		{
			myDefs.Clear();
			picArea.Invalidate();
			txtCurrPos.Text = "<None>";
			ReloadPositions();
		}

		private void frmViewPoint_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if(myDefs != null)
			{
				myDefs.Clear();
			}
		}

		private void cbAutoRotate_CheckedChanged(object sender, System.EventArgs e)
		{
			if(currPosTabLine > -1)
			{
				string strVal="J";
				if(cbAutoRotate.Checked == false)
				{
					strVal = "N";
				}
				myDefs[currPosTabLine]["ROTATE"] = strVal;
			}
		}

		private void udGridSize_ValueChanged(object sender, System.EventArgs e)
		{
			picArea.Invalidate();
		}


	}
}
