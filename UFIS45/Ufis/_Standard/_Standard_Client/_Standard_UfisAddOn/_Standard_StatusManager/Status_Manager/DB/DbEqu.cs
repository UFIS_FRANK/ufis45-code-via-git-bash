// EQUIPEMENT Information 
//   Related to Database
// AUNG MOE : 20080721
//

using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

using Ufis.Data;
using Ufis.Utils;

using Ufis.Status_Manager.Ent;
using Ufis.Status_Manager.DS;

namespace Ufis.Status_Manager.DB
{
    public class DbEqu
    {
        #region data member
        private static DbEqu _this = null;
        private static object _lockEquTableProp = new object();

        private IDatabase _myDB = null;
        private ITable _equTable = null;
        #endregion

        #region property member
        private IDatabase MyDb
        {
            get
            {
                if (_myDB == null) _myDB = UT.GetMemDB();
                return _myDB;
            }
        }

        private ITable EquTable
        {
            get
            {
                _equTable = MyDb["~EQU"]; //igu on 05/04/2011
                if (_equTable == null)
                {
                    lock (_lockEquTableProp)
                    {
                        _equTable = MyDb["~EQU"];//Get and check again //igu on 05/04/2011
                        if (_equTable == null)
                        {
                            _equTable = MyDb.Bind("~EQU", "EQU", //igu on 05/04/2011
                                "URNO,GCDE,ENAM",
                                "10,5,40",
                                "URNO,GCDE,ENAM");
                            //_equTable.UseTrackChg = true;
                            //_equTable.TrackChgImmediately = false;//Not to track the changes until "StartTrackChg" for the particular row.
                        }
                    }
                }
                return _equTable;
            }
        }
        #endregion

        public ITable GetEquTable()
        {
            return EquTable;
        }

        #region singleton
        private DbEqu()
        {
        }

        public static DbEqu GetInstance()
        {
            if (_this == null) _this = new DbEqu();
            return _this;
        }
        #endregion

        private static void LogMsg(string msg)
        {
            UT.LogMsg("DbEqu:" + msg);
        }

        #region lock table
        public void LockEquTableForWrite()
        {
            LogMsg("B4 Lock Equ Write");
            EquTable.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
            LogMsg("AF Lock Equ Write");
        }

        public void ReleaseEquTableWrite()
        {
            if (EquTable.Lock.IsWriterLockHeld)
            {
                LogMsg("B4 Release Lock Equ Write");
                EquTable.Lock.ReleaseWriterLock();
                LogMsg("AF Release Lock Equ Write");
            }
        }

        public void LockEquTableForRead()
        {
            LogMsg("B4 Lock Equ Read");
            EquTable.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
            LogMsg("AF Lock Equ Read");
        }

        public void ReleaseEquTableRead()
        {
            if (EquTable.Lock.IsReaderLockHeld)
            {
                LogMsg("B4 Release Lock Equ Read");
                EquTable.Lock.ReleaseReaderLock();
                LogMsg("AF Release Lock Equ Read");
            }
        }
        #endregion

        private void AddDataToDsJob(IRow row, DSJob dsJob)
        {
            DSJob.EQURow dtRow = dsJob.EQU.NewEQURow();

            dtRow.URNO = row["URNO"].ToString();
            dtRow.ENAM = row["ENAM"].ToString();
            dtRow.GCDE = row["GCDE"].ToString();
            dsJob.EQU.AddEQURow(dtRow);
        }

        /// <summary>
        /// Get Equipment Information and load them into passing dsJob
        /// </summary>
        /// <param name="dsJob">load the relevent Equipment info into this dataset</param>
        /// <param name="uequArr">UEQU array to load</param>
        public void GetEquInfo(DSJob dsJob, string[] uequArr)
        {
            dsJob.EQU.Clear();
            int cntArr = uequArr.Length;
            if (cntArr > 0)
            {
                try
                {
                    LockEquTableForRead();

                    string[] missingUequArr = new string[uequArr.Length];
                    int cntMissing = 0;
                    for (int idxUequ = 0; idxUequ < cntArr; idxUequ++)
                    {
                        string uequ = uequArr[idxUequ];
                        IRow[] rows = EquTable.RowsByIndexValue("URNO", uequ);
                        if ((rows == null) || (rows.Length < 1))
                        {
                            missingUequArr[cntMissing] = uequ;
                            cntMissing++;
                        }
                        else
                        {
                            AddDataToDsJob(rows[0], dsJob);
                        }
                    }

                    if (cntMissing > 0)
                    {
                        AddEquData("'" + string.Join("','", missingUequArr) + "'");
                        for (int i = 0; i < cntMissing; i++)
                        {
                            string uequ = missingUequArr[i];
                            IRow[] rows = EquTable.RowsByIndexValue("URNO", uequ);
                            if ((rows == null) || (rows.Length < 1))
                            {
                            }
                            else
                            {
                                AddDataToDsJob(rows[0], dsJob);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    ReleaseEquTableRead();
                }
            }
        }


        private void AddEquData(string uequSt)
        {
            EquTable.Load(string.Format(" WHERE URNO IN ({0})", uequSt));
        }

        /// <summary>
        /// Load EQUTAB Data
        /// Note: Existing In memory Data in EQUTAB will be cleared
        /// </summary>
        /// <param name="uequArrStForSelection">Comma seperated UEQU string array (e.g. Arr[0]==>65132523,65132556,65132745 Arr[0]==>6554324523,651345256,651387685 </param>
        /// <returns>result message</returns>
        public string LoadEquData(ArrayList uequArrStForSelection)
        {
            string result = "";

            //Select the EQUTAB records according to the flight

            try
            {
                LockEquTableForWrite();
                DeleteEquIndex();
                EquTable.Clear();

                LogMsg("LoadEQUData: Start");
                EquTable.TimeFieldsInitiallyInUtc = true;
                EquTable.TimeFieldsCurrentlyInUtc = true;

                LogMsg("LoadEQUData: b4 load");
                int cntArr = uequArrStForSelection.Count;
                for (int i = 0; i < cntArr; i++)
                {
                    try
                    {
                        if (uequArrStForSelection[i].ToString() != "")
                        {
                            AddEquData(uequArrStForSelection[i].ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadEquData:Err:" + ex.Message);
                    }
                }
                EquTable.Sort("URNO", true);
                CreateEquIndex();

                EquTable.Command("insert", ",IRT,");
                EquTable.Command("update", ",URT,");
                EquTable.Command("delete", ",DRT,");
                EquTable.Command("read", ",GFR,");

                result = "EQUTAB " + EquTable.Count + " Records loaded";

                LogMsg("EQUTAB <" + EquTable.Count + ">");
                return result;
            }
            catch (Exception ex)
            {
                LogMsg("LoadEQUData:Err:" + ex.Message);
            }
            finally
            {
                ReleaseEquTableWrite();
            }
            return result;
        }

        #region table index
        private void CreateEquIndex()
        {
            EquTable.CreateIndex("URNO", "URNO");
        }

        private void DeleteEquIndex()
        {
            try
            {
                EquTable.DeleteIndex("URNO");
            }
            catch (Exception)
            {
            }
        }
        #endregion
    }
}
