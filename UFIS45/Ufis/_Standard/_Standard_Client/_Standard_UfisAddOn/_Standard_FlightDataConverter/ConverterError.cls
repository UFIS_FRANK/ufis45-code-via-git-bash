VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ConverterError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private m_ErrMessage As String
Private m_ErrControl As Control

Public Property Get ErrorMessage() As String
    ErrorMessage = m_ErrMessage
End Property

Public Property Let ErrorMessage(ByVal value As String)
    m_ErrMessage = value
End Property

Public Property Get ErrorControl() As Control
    ErrorControl = m_ErrControl
End Property

Public Property Let ErrorControl(ByVal value As Control)
    m_ErrControl = value
End Property
