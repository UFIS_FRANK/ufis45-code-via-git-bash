Attribute VB_Name = "modFolderBrowser"
Option Explicit

Private Const BIF_RETURNONLYFSDIRS = 1
Private Const BIF_DONTGOBELOWDOMAIN = 2
Private Const MAX_PATH = 260

Private Declare Function SHBrowseForFolder Lib _
    "shell32" (lpbi As BrowseInfo) As Long

Private Declare Function SHGetPathFromIDList Lib _
    "shell32" (ByVal pidList As Long, ByVal lpBuffer _
    As String) As Long

Private Declare Function lstrcat Lib "kernel32" _
    Alias "lstrcatA" (ByVal lpString1 As String, ByVal _
    lpString2 As String) As Long

Private Type BrowseInfo
   hWndOwner As Long
   pIDLRoot As Long
   pszDisplayName As Long
   lpszTitle As Long
   ulFlags As Long
   lpfnCallback As Long
   lParam As Long
   iImage As Long
End Type

Public Function ShowFolderBrowser(Owner As Form, ByVal Title As String) As String
    'Opens a Browse Folders Dialog Box that displays the
    'directories in your computer
    Dim lpIDList As Long ' Declare Varibles
    Dim sBuffer As String
    Dim tBrowseInfo As BrowseInfo
    
    With tBrowseInfo
       .hWndOwner = Owner.hWnd ' Owner Form
       .lpszTitle = lstrcat(Title, "")
       .ulFlags = BIF_RETURNONLYFSDIRS + BIF_DONTGOBELOWDOMAIN
    End With
    
    lpIDList = SHBrowseForFolder(tBrowseInfo)
    
    If (lpIDList) Then
       sBuffer = Space(MAX_PATH)
       SHGetPathFromIDList lpIDList, sBuffer
       sBuffer = Left(sBuffer, InStr(sBuffer, vbNullChar) - 1)
    End If
    
    ShowFolderBrowser = sBuffer
End Function

