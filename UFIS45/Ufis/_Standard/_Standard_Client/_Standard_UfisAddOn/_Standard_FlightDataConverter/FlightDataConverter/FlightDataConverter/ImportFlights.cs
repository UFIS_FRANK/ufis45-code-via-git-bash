﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDataConverter
{
    public class ImportFlights
    {
        public class Flight
        {
            private DateTime _fromDate;
            private DateTime _toDate;
            private string _dailyFrequency;
            private string _weeklyFrequency;
            private string _origin;
            private string _destination;
            private string _viaList;
            private DateTime _scheduledTime;
            private string _aircraftType;
            private string _flightNature;
            private string _flightNumber;
            private string _airline;
            private string _flightNoOnly;
            private string _flightNoSuffix;
            private string _flightType;
            private string _adid;

            public DateTime FromDate
            {
                get { return _fromDate; }
                set { _fromDate = value; }
            }

            public DateTime ToDate
            {
                get { return _toDate; }
                set { _toDate = value; }
            }

            public string DailyFrequency
            {
                get { return _dailyFrequency; }
                set { _dailyFrequency = value; }
            }

            public string WeeklyFrequency
            {
                get { return _weeklyFrequency; }
                set { _weeklyFrequency = value; }
            }

            public DateTime ScheduledTime
            {
                get { return _scheduledTime; }
                set { _scheduledTime = value; }
            }

            public string Origin
            {
                get { return _origin; }
                set { _origin = value; }
            }

            public string Destination
            {
                get { return _destination; }
                set { _destination = value; }
            }

            public string ViaList
            {
                get { return _viaList; }
                set { _viaList = value; }
            }

            public string AircraftType
            {
                get { return _aircraftType; }
                set { _aircraftType = value; }
            }

            public string FlightNature
            {
                get { return _flightNature; }
                set { _flightNature = value; }
            }

            public string Airline
            {
                get { return _airline; }
                set { _airline = value; }
            }

            public string FlightNumber
            {
                get { return _flightNumber; }
                set { _flightNumber = value; }
            }

            public string FlightNoOnly
            {
                get { return _flightNoOnly; }
                set { _flightNoOnly = value; }
            }

            public string FlightNoSuffix
            {
                get { return _flightNoSuffix; }
                set { _flightNoSuffix = value; }
            }

            public string FlightType
            {
                get { return _flightType; }
                set { _flightType = value; }
            }

            public string ADID
            {
                get { return _adid; }
                set { _adid = value; }
            }
        }
        

        private Flight[] flights = new Flight[2];

        public Flight ArrivalFlight
        {
            get { return flights[0]; }
            set { flights[0] = value; }
        }

        public Flight DepartureFlight
        {
            get { return flights[1]; }
            set { flights[1] = value; }
        }

        public void Add(Flight flight)
        {
            if (flight.ADID == "A")
            {
                flights[0] = flight;
            }
            else
            {
                flights[1] = flight;
            }
        }

        public ImportFlights(Flight arrFlight, Flight depFlight)
        {
            if (arrFlight.ADID == "A")
            {
                flights[0] = arrFlight;
            }
            if (depFlight.ADID != "A")
            {
                flights[1] = depFlight;
            }
        }

        public override string ToString()
        {
            string strRet = string.Empty;

            string strFormat = string.Empty;
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 15; j++)
                {
                    strFormat = "{" + i.ToString() + "}";
                    if (j < 15)
                    {
                        strFormat += ",";
                    }
                }
                strFormat += "\n";
            }

            Flight arrFlight = this.ArrivalFlight;
            Flight depFlight = this.DepartureFlight;

            strRet = string.Format(strFormat,
                arrFlight.FromDate.ToString("yyyyMMdd"),
                arrFlight.ToDate.ToString("yyyyMMdd"),
                arrFlight.DailyFrequency,
                arrFlight.WeeklyFrequency,
                arrFlight.Origin,
                arrFlight.ViaList,
                arrFlight.ScheduledTime.ToString("yyyyMMddHHmmss"),
                arrFlight.AircraftType,
                arrFlight.FlightNature,
                arrFlight.FlightNumber,
                arrFlight.Airline,
                arrFlight.FlightNoOnly,
                arrFlight.FlightNoSuffix,
                arrFlight.Destination,
                arrFlight.FlightType,
                depFlight.FromDate.ToString("yyyyMMdd"),
                depFlight.ToDate.ToString("yyyyMMdd"),
                depFlight.DailyFrequency,
                depFlight.WeeklyFrequency,
                depFlight.Destination,
                depFlight.ViaList,
                depFlight.ScheduledTime.ToString("yyyyMMddHHmmss"),
                depFlight.AircraftType,
                depFlight.FlightNature,
                depFlight.FlightNumber,
                depFlight.Airline,
                depFlight.FlightNoOnly,
                depFlight.FlightNoSuffix,
                depFlight.Origin,
                depFlight.FlightType);

            return strRet;
        }
    }
}
