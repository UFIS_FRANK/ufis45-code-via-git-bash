﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace FlightDataConverter
{
    public partial class frmMain : Form
    {
        internal class ConverterError
        {
            private string _errMessage = string.Empty;
            private Control _errControl = null;
            
            public string ErrorMessage
            {
                get { return _errMessage; }
                set { _errMessage = value; }
            }

            public Control ErrorControl
            {
                get { return _errControl; }
                set { _errControl = value; }
            }
        }

        internal class FlightScheduleData
        {
            private string _name = string.Empty;
            private object[,] _flightSchedule = null;

            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public object[,] FlightSchedule
            {
                get { return _flightSchedule; }
                set { _flightSchedule = value; }
            }

            public FlightScheduleData(string name, object[,] data)
            {
                _name = name;
                _flightSchedule = data;
            }
        }

        internal class Routing
        {
            private string _airport;
            private DateTime _depTime; 
            private DateTime _arrTime;

            public string Airport
            {
                get { return _airport; }
                set { _airport = value; }
            }

            public DateTime DepartureTime
            {
                get { return _depTime; }
                set { _depTime = value; }
            }

            public DateTime ArrivalTime
            {
                get { return _arrTime; }
                set { _arrTime = value; }
            }

            public Routing(string airport, DateTime arrivalTime, DateTime departureTime)
            {
                _airport = airport;
                _arrTime = arrivalTime;
                _depTime = departureTime;
            }

            public override string ToString()
            {
                string strRet = string.Empty;
                string strTime = string.Empty;

                strRet = " " + _airport.PadRight(3) + ",";
                if (_arrTime != new DateTime())
                {
                    strTime = _arrTime.ToString("HHmm");
                }
                strRet += strTime.PadRight(4) + ",";
                strTime = string.Empty;
                if (_depTime != new DateTime())
                {
                    strTime = _depTime.ToString("HHmm");
                }
                strRet += strTime.PadRight(4);

                return strRet;
            }
        }

        //internal class Flight
        //{
        //    public enum FlightTypeEnum
        //    {
        //        ArrivalFlight,
        //        DepartureFlight
        //    }

        //    private FlightTypeEnum _adid = FlightTypeEnum.ArrivalFlight;
        //    private string _airline = string.Empty;
        //    private string _aircraft = string.Empty;
        //    private string _flno = string.Empty;
        //    private string _origOrDest = string.Empty;
        //    private string _lastOrNext = string.Empty;
        //    private DateTime _stTime = new DateTime();
        //    private string _doop = string.Empty;
        //    private int _overnight = 0;
        //    private int _seats = 0;

        //    public FlightTypeEnum ADID
        //    {
        //        get { return _adid; }
        //        set { _adid = value; }
        //    }

        //    public string Airline
        //    {
        //        get { return _airline; }
        //        set { _airline = value; }
        //    }

        //    public string AircraftType
        //    {
        //        get { return _aircraft; }
        //        set { _aircraft = value; }
        //    }

        //    public string FlightNumber
        //    {
        //        get { return _flno; }
        //        set { _flno = value; }
        //    }

        //    public string OrigOrDest
        //    {
        //        get { return _origOrDest; }
        //        set { _origOrDest = value; }
        //    }

        //    public string LastOrNext
        //    {
        //        get { return _lastOrNext; }
        //        set { _lastOrNext = value; }
        //    }

        //    public string DaysOfOperation
        //    {
        //        get { return _doop; }
        //        set { _doop = value; }
        //    }
            
        //    public DateTime StandardTime
        //    {
        //        get { return _stTime; }
        //        set { _stTime = value; }
        //    }

        //    public int Overnight
        //    {
        //        get { return _overnight; }
        //        set { _overnight = value; }
        //    }

        //    public int Seats
        //    {
        //        get { return _seats; }
        //        set { _seats = value; }
        //    }

        //    public string GetFlightADIDCode()
        //    {
        //        return ( _adid == FlightTypeEnum.ArrivalFlight ? "R" : "F");
        //    }

        //    public List<Routing> GetFlightRouting()
        //    {
        //        List<Routing> routing = new List<Routing>();

        //        if (_adid == FlightTypeEnum.ArrivalFlight)
        //        {
        //            routing.Add(new Routing(_origOrDest, new DateTime(), new DateTime()));
        //            if (_lastOrNext != _origOrDest)
        //            {
        //                routing.Add(new Routing(_lastOrNext, new DateTime(), new DateTime()));
        //            }
        //            routing.Add(new Routing("SIN", _stTime, new DateTime()));
        //        }
        //        else
        //        {
        //            routing.Add(new Routing("SIN", new DateTime(), _stTime));
        //            if (_lastOrNext != _origOrDest)
        //            {
        //                routing.Add(new Routing(_lastOrNext, new DateTime(), new DateTime()));
        //            }
        //            routing.Add(new Routing(_origOrDest, new DateTime(), new DateTime()));
        //        }
        //        return routing;
        //    }

        //    public List<int> GetDaysOfOperation()
        //    {
        //        List<int> doop = new List<int>();

        //        int intDay = 1;
        //        foreach (char day in _doop)
        //        {
        //            if (day != '0' && day.ToString() == intDay.ToString())
        //            {
        //                intDay = intDay + _overnight;
        //                if (intDay > 7)
        //                {
        //                    intDay -= 7;
        //                }
        //                doop.Add(intDay);
        //            }
        //            intDay++;
        //        }
                
        //        return doop;
        //    }
        //}

        private ConverterError ValidateUserInputs()
        {
            ConverterError converterError = new ConverterError();

            if (txtInputFile.Text.Trim() == "")
            {
                converterError.ErrorMessage = "Please specify the input file!";
                converterError.ErrorControl = txtInputFile;
                return converterError;
            }

            if (!System.IO.File.Exists(txtInputFile.Text))
            {
                converterError.ErrorMessage = string.Format("File {1} not found!", txtInputFile.Text);
                converterError.ErrorControl = txtInputFile;
                return converterError;
            }

            if (txtOutputFolder.Text.Trim() == "")
            {
                converterError.ErrorMessage = "Please specify the output folder!";
                converterError.ErrorControl = txtOutputFolder;
                return converterError;
            }

            if (!System.IO.Directory.Exists(txtOutputFolder.Text))
            {
                converterError.ErrorMessage = string.Format("Folder {1} not found!", txtOutputFolder.Text);
                converterError.ErrorControl = txtOutputFolder;
                return converterError;
            }

            return converterError;
        }

        private List<ConverterError> ConvertFile(string inputFile, string outputFolder)
        {
            List<ConverterError> converterErrors = new List<ConverterError>();
            ConverterError converterError = new ConverterError();

            List<FlightScheduleData> flightScheduleData = new List<FlightScheduleData>();

            Excel.Application xlApp = null;
            Excel.Workbook xlWorkbook = null;
            Excel.Worksheet xlWorksheet = null;
            
            try
            {
                xlApp = new Excel.ApplicationClass();
                if (xlApp == null)
                {
                    converterError.ErrorMessage = "EXCEL could not be started. Check that your office installation and project references are correct.";
                    converterErrors.Add(converterError);
                }
                else
                {
                    xlWorkbook = xlApp.Workbooks.Open(inputFile, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                    for (int i = 1; i <= xlWorkbook.Worksheets.Count; i++)
                    {
                        xlWorksheet = (Excel.Worksheet)xlWorkbook.Worksheets.get_Item(i);

                        string wsName = xlWorksheet.Name;
                        int rowCount = xlWorksheet.UsedRange.Rows.Count;
                        int colCount = xlWorksheet.UsedRange.Columns.Count;

                        Excel.Range range1 = (Excel.Range)xlWorksheet.Cells[1, 1];
                        Excel.Range range2 = (Excel.Range)xlWorksheet.Cells[rowCount, colCount];
                        Excel.Range range3 = xlWorksheet.get_Range(range1, range2);
                        object[,] values = range3.get_Value(Excel.XlRangeValueDataType.xlRangeValueDefault) as object[,];

                        flightScheduleData.Add(new FlightScheduleData(wsName, values));
                    }
                }
            }
            catch (Exception ex)
            {
                converterError.ErrorMessage = "Failed to open input file!" + Environment.NewLine + ex.Message;
                converterErrors.Add(converterError);
            }
            finally
            {
                if (xlWorkbook != null)
                {
                    xlWorkbook.Close(false, inputFile, false);
                }
                if (xlApp != null)
                {
                    xlApp.Quit();
                }

                if (xlWorksheet != null)
                    releaseObject(xlWorksheet);
                if (xlWorkbook != null)
                    releaseObject(xlWorkbook);
                if (xlApp != null)
                    releaseObject(xlApp);
            }

            foreach (FlightScheduleData fsData in flightScheduleData)
            {
                converterError = CreateFile(outputFolder, fsData);
                if (converterError.ErrorMessage != "")
                {
                    converterErrors.Add(converterError);
                }
            }

            return converterErrors;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        } 

        private ConverterError CreateFile(string outputFolder, FlightScheduleData flightData)
        {
            ConverterError converterError = new ConverterError();

            string strFilename = string.Format("{0}{1}{2}_score.txt", 
                outputFolder, System.IO.Path.DirectorySeparatorChar, 
                flightData.Name.Replace(' ', '_'));
            //string strSeason = "";
            List<string> colHeaders = new List<string>();
            
            try
            {
                StringBuilder sbFile = new StringBuilder();
                
                object[,] data = flightData.FlightSchedule;
                int rowCount = data.GetLength(0);
                int colCount = data.GetLength(1);

                for (int row = 1; row <= rowCount; row++)
                {
                    switch (row)
                    {
                        case 1:
                        case 2:
                        case 3:
                            //no use
                            break;
                        //case 2:
                        //    //season
                        //    strSeason = data[row, 1].ToString().Trim().Split(' ')[0];
                        //    sbInc.AppendLine(strSeason);
                        //    break;
                        case 4:
                            //header
                            for (int col = 1; col <= colCount; col++)
                            {
                                object cellData = data[row, col];
                                if (cellData != null)
                                {
                                    colHeaders.Add(cellData.ToString());
                                }
                            }
                            break;
                        default:
                            //data
                            DateTime fromDate = GetCellValueAsDate(data, row, "From", colHeaders);
                            DateTime toDate = GetCellValueAsDate(data, row, "To", colHeaders);
                            string arrAlc = GetCellValueAsString(data, row, "Arr", colHeaders); 
                            string arrFlno = GetCellValueAsString(data, row, "Arr Flt No", colHeaders);
                            string depAlc = GetCellValueAsString(data, row, "Dep", colHeaders);
                            string depFlno = GetCellValueAsString(data, row, "Dep Flt No", colHeaders);
                            string doop = GetCellValueAsString(data, row, "DOOP", colHeaders);
                            int seats = GetCellValueAsInt(data, row, "Seats", colHeaders);
                            string acType = GetCellValueAsString(data, row, "Actyp", colHeaders);
                            string orig = GetCellValueAsString(data, row, "Orig", colHeaders);
                            string last = GetCellValueAsString(data, row, "Last", colHeaders);
                            string next = GetCellValueAsString(data, row, "Next", colHeaders);
                            string dest = GetCellValueAsString(data, row, "Dest", colHeaders);
                            string overnight = GetCellValueAsString(data, row, "Overnight", colHeaders);
                            DateTime aTime = GetCellValueAsTime(data, row, "ATime", colHeaders);
                            DateTime dTime = GetCellValueAsTime(data, row, "DTime", colHeaders);
                            string aType = GetCellValueAsString(data, row, "AType", colHeaders);
                            string dType = GetCellValueAsString(data, row, "DType", colHeaders);

                            string arrTime = "";
                            string depTime = "";
                            DateTime blankTime = new DateTime();
                            if (aTime != blankTime) arrTime = aTime.ToString("HHmm");
                            if (dTime != blankTime) depTime = dTime.ToString("HHmm");

                            if (aType == "") aType = txtDefaultServiceType.Text;
                            if (dType == "") dType = txtDefaultServiceType.Text;

                            arrFlno = GetFormattedFlightNumber(arrFlno);
                            depFlno = GetFormattedFlightNumber(depFlno);
                            
                            string line = txtHOPO.Text.PadRight(3) +
                                arrAlc.PadRight(3) +
                                arrFlno.PadRight(5) +
                                depAlc.PadRight(3) +
                                depFlno.PadRight(5) +
                                fromDate.ToString("yyyyMMdd") +
                                toDate.ToString("yyyyMMdd") + 
                                doop.PadLeft(7, '0') +
                                seats.ToString("D3") +
                                acType.PadRight(3) +
                                orig.PadRight(3) +
                                last.PadRight(3) +
                                arrTime.PadRight(4) +
                                depTime.PadRight(4) +
                                overnight.PadRight(1) +
                                next.PadRight(3) +
                                dest.PadRight(3) +
                                aType +
                                dType;

                            sbFile.AppendLine(line);
                            
                            break;
                    }
                }

                System.IO.File.WriteAllText(strFilename, sbFile.ToString());
            }
            catch (Exception ex)
            {
                converterError.ErrorMessage = "Failed to create file " + strFilename + "!" + Environment.NewLine + ex.Message;
            }
            finally
            {
                
            }

            return converterError;
        }

        private string GetFormattedFlightNumber(string flno)
        {
            string strRet = string.Empty;
            string numberString = string.Empty;
            int itemIndex = 0;

            foreach (char item in flno)
            {
                if (Char.IsNumber(item))
                {
                    numberString += item.ToString();
                }
                else
                {
                    break;
                } 
                
                itemIndex++;
            }

            if (numberString.Length > 0 && numberString.Length < 3)
            {
                numberString = numberString.PadLeft(3, '0');
            }
            strRet = numberString;
            if (itemIndex < flno.Length)
            {
                strRet += flno.Substring(itemIndex);
            }

            return strRet;
        }

        private string GetCellValueAsString(object[,] data, int rowIndex, string colName, List<string> colHeaders)
        {
            string strRet = string.Empty;
            object objRet = GetCellValue(data, rowIndex, colName, colHeaders);
            if (objRet != null)
            {
                strRet = objRet.ToString();
            }
            return strRet;
        }

        private int GetCellValueAsInt(object[,] data, int rowIndex, string colName, List<string> colHeaders)
        {
            int intRet = 0;
            object objRet = GetCellValue(data, rowIndex, colName, colHeaders);
            if (objRet != null)
            {
                try { intRet = Convert.ToInt32(objRet); }
                catch { }
            }
            return intRet;
        }

        private DateTime GetCellValueAsDate(object[,] data, int rowIndex, string colName, List<string> colHeaders)
        {
            DateTime dtmRet = new DateTime();
            object objRet = GetCellValue(data, rowIndex, colName, colHeaders);
            if (objRet != null)
            {
                try { dtmRet = Convert.ToDateTime(objRet); }
                catch { }
            }
            return dtmRet;
        }

        private DateTime GetCellValueAsTime(object[,] data, int rowIndex, string colName, List<string> colHeaders)
        {
            DateTime dtmRet = new DateTime();
            string strRet = GetCellValueAsString(data, rowIndex, colName, colHeaders);
            if (strRet != "")
            {
                int intRet = 0;
                int.TryParse(strRet, out intRet);
                string time = intRet.ToString("D4");
                double hours = 0;
                double mins = 0;

                double.TryParse(time.Substring(0, 2), out hours);
                double.TryParse(time.Substring(2, 2), out mins);

                dtmRet = DateTime.Today;
                dtmRet = dtmRet.AddHours(hours);
                dtmRet = dtmRet.AddMinutes(mins);
            }
            return dtmRet;
        }

        private object GetCellValue(object[,] data, int rowIndex, string colName, List<string> colHeaders)
        {
            object objRet = null;
            int colIndex = colHeaders.IndexOf(colName);
            if (colIndex >= 0)
            {
                objRet = data[rowIndex, colIndex + 1];
            }

            return objRet;
        }

        public frmMain()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBrowseInputFile_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Open OAL Excel File";
            openFileDialog.Filter = "Excel Files|*.xls|All Files|*.*";
            openFileDialog.InitialDirectory = Application.StartupPath;
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtInputFile.Text = openFileDialog.FileName;

                if (txtOutputFolder.Text.Trim() == "")
                {
                    txtOutputFolder.Text = System.IO.Path.GetDirectoryName(txtInputFile.Text); 
                }
            }
        }

        private void btnBrowseOutputFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                txtOutputFolder.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            ConverterError converterError = ValidateUserInputs();

            if (converterError.ErrorMessage != "")
            {
                MessageBox.Show(converterError.ErrorMessage, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                if (converterError.ErrorControl != null)
                {
                    converterError.ErrorControl.Focus();
                }
            }
            else
            {
                List<ConverterError> converterErrors = ConvertFile(txtInputFile.Text, txtOutputFolder.Text);
                if (converterErrors.Count == 0)
                {
                    MessageBox.Show("Flight data file(s) have been successfully created!", 
                        this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);                    
                }
                else
                {
                    string strMessage = string.Empty;
                    foreach (ConverterError cvtError in converterErrors)
                    {
                        strMessage += cvtError.ErrorMessage + Environment.NewLine;
                    }
                    MessageBox.Show(strMessage, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);                    
                }
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            const string DEFAULT_HOPO = "SIN";
            const string DEFAULT_SERVICE_TYPE = "PAX";

            txtHOPO.Text = DEFAULT_HOPO;
            txtDefaultServiceType.Text = DEFAULT_SERVICE_TYPE;
        }

    }
}
