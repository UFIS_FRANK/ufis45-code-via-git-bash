﻿namespace FlightDataConverter
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.lblInputFile = new System.Windows.Forms.Label();
            this.txtInputFile = new System.Windows.Forms.TextBox();
            this.lblOutputFolder = new System.Windows.Forms.Label();
            this.txtOutputFolder = new System.Windows.Forms.TextBox();
            this.btnBrowseInputFile = new System.Windows.Forms.Button();
            this.btnBrowseOutputFolder = new System.Windows.Forms.Button();
            this.btnConvert = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.txtHOPO = new System.Windows.Forms.TextBox();
            this.lblHOPO = new System.Windows.Forms.Label();
            this.txtDefaultServiceType = new System.Windows.Forms.TextBox();
            this.lblDefaultServiceType = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // lblInputFile
            // 
            this.lblInputFile.AutoSize = true;
            this.lblInputFile.Location = new System.Drawing.Point(12, 9);
            this.lblInputFile.Name = "lblInputFile";
            this.lblInputFile.Size = new System.Drawing.Size(152, 13);
            this.lblInputFile.TabIndex = 0;
            this.lblInputFile.Text = "File to convert (OAL Excel file):";
            // 
            // txtInputFile
            // 
            this.txtInputFile.Location = new System.Drawing.Point(187, 6);
            this.txtInputFile.Name = "txtInputFile";
            this.txtInputFile.Size = new System.Drawing.Size(364, 20);
            this.txtInputFile.TabIndex = 1;
            // 
            // lblOutputFolder
            // 
            this.lblOutputFolder.AutoSize = true;
            this.lblOutputFolder.Location = new System.Drawing.Point(12, 36);
            this.lblOutputFolder.Name = "lblOutputFolder";
            this.lblOutputFolder.Size = new System.Drawing.Size(114, 13);
            this.lblOutputFolder.TabIndex = 3;
            this.lblOutputFolder.Text = "Save converted file to:";
            // 
            // txtOutputFolder
            // 
            this.txtOutputFolder.Location = new System.Drawing.Point(187, 32);
            this.txtOutputFolder.Name = "txtOutputFolder";
            this.txtOutputFolder.Size = new System.Drawing.Size(364, 20);
            this.txtOutputFolder.TabIndex = 4;
            // 
            // btnBrowseInputFile
            // 
            this.btnBrowseInputFile.Location = new System.Drawing.Point(551, 4);
            this.btnBrowseInputFile.Name = "btnBrowseInputFile";
            this.btnBrowseInputFile.Size = new System.Drawing.Size(31, 23);
            this.btnBrowseInputFile.TabIndex = 2;
            this.btnBrowseInputFile.TabStop = false;
            this.btnBrowseInputFile.Text = "...";
            this.btnBrowseInputFile.UseVisualStyleBackColor = true;
            this.btnBrowseInputFile.Click += new System.EventHandler(this.btnBrowseInputFile_Click);
            // 
            // btnBrowseOutputFolder
            // 
            this.btnBrowseOutputFolder.Location = new System.Drawing.Point(551, 29);
            this.btnBrowseOutputFolder.Name = "btnBrowseOutputFolder";
            this.btnBrowseOutputFolder.Size = new System.Drawing.Size(30, 23);
            this.btnBrowseOutputFolder.TabIndex = 5;
            this.btnBrowseOutputFolder.TabStop = false;
            this.btnBrowseOutputFolder.Text = "...";
            this.btnBrowseOutputFolder.UseVisualStyleBackColor = true;
            this.btnBrowseOutputFolder.Click += new System.EventHandler(this.btnBrowseOutputFolder_Click);
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(187, 131);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(87, 31);
            this.btnConvert.TabIndex = 10;
            this.btnConvert.Text = "C&onvert";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(494, 131);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 31);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtHOPO
            // 
            this.txtHOPO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHOPO.Location = new System.Drawing.Point(187, 58);
            this.txtHOPO.MaxLength = 3;
            this.txtHOPO.Name = "txtHOPO";
            this.txtHOPO.Size = new System.Drawing.Size(43, 20);
            this.txtHOPO.TabIndex = 7;
            // 
            // lblHOPO
            // 
            this.lblHOPO.AutoSize = true;
            this.lblHOPO.Location = new System.Drawing.Point(12, 62);
            this.lblHOPO.Name = "lblHOPO";
            this.lblHOPO.Size = new System.Drawing.Size(70, 13);
            this.lblHOPO.TabIndex = 6;
            this.lblHOPO.Text = "Home airport:";
            // 
            // txtDefaultServiceType
            // 
            this.txtDefaultServiceType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDefaultServiceType.Location = new System.Drawing.Point(187, 84);
            this.txtDefaultServiceType.MaxLength = 3;
            this.txtDefaultServiceType.Name = "txtDefaultServiceType";
            this.txtDefaultServiceType.Size = new System.Drawing.Size(43, 20);
            this.txtDefaultServiceType.TabIndex = 9;
            // 
            // lblDefaultServiceType
            // 
            this.lblDefaultServiceType.AutoSize = true;
            this.lblDefaultServiceType.Location = new System.Drawing.Point(12, 88);
            this.lblDefaultServiceType.Name = "lblDefaultServiceType";
            this.lblDefaultServiceType.Size = new System.Drawing.Size(104, 13);
            this.lblDefaultServiceType.TabIndex = 8;
            this.lblDefaultServiceType.Text = "Default service type:";
            // 
            // frmMain
            // 
            this.AcceptButton = this.btnConvert;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(594, 180);
            this.Controls.Add(this.txtDefaultServiceType);
            this.Controls.Add(this.lblDefaultServiceType);
            this.Controls.Add(this.txtHOPO);
            this.Controls.Add(this.lblHOPO);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnConvert);
            this.Controls.Add(this.btnBrowseOutputFolder);
            this.Controls.Add(this.btnBrowseInputFile);
            this.Controls.Add(this.txtOutputFolder);
            this.Controls.Add(this.lblOutputFolder);
            this.Controls.Add(this.txtInputFile);
            this.Controls.Add(this.lblInputFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Flight Data Converter";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label lblInputFile;
        private System.Windows.Forms.TextBox txtInputFile;
        private System.Windows.Forms.Label lblOutputFolder;
        private System.Windows.Forms.TextBox txtOutputFolder;
        private System.Windows.Forms.Button btnBrowseInputFile;
        private System.Windows.Forms.Button btnBrowseOutputFolder;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.TextBox txtHOPO;
        private System.Windows.Forms.Label lblHOPO;
        private System.Windows.Forms.TextBox txtDefaultServiceType;
        private System.Windows.Forms.Label lblDefaultServiceType;
    }
}

