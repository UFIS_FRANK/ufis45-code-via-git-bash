VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Flight Data Converter"
   ClientHeight    =   3165
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8940
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   3165
   ScaleWidth      =   8940
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog cdgOpen 
      Left            =   3600
      Top             =   1080
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   435
      Left            =   7680
      TabIndex        =   11
      Top             =   2400
      Width           =   1155
   End
   Begin VB.CommandButton cmdConvert 
      Caption         =   "C&onvert"
      Default         =   -1  'True
      Height          =   435
      Left            =   2760
      TabIndex        =   10
      Top             =   2400
      Width           =   1155
   End
   Begin VB.CommandButton cmdBrowseOutputFolder 
      Caption         =   "..."
      Height          =   375
      Left            =   8400
      TabIndex        =   9
      Top             =   600
      Width           =   375
   End
   Begin VB.CommandButton cmdBrowseInputFile 
      Caption         =   "..."
      Height          =   375
      Left            =   8400
      TabIndex        =   8
      Top             =   120
      Width           =   375
   End
   Begin VB.TextBox txtDefaultServiceType 
      Height          =   375
      Left            =   2760
      MaxLength       =   3
      TabIndex        =   6
      Top             =   1560
      Width           =   735
   End
   Begin VB.TextBox txtHOPO 
      Height          =   375
      Left            =   2760
      MaxLength       =   3
      TabIndex        =   4
      Top             =   1080
      Width           =   735
   End
   Begin VB.TextBox txtOutputFolder 
      Height          =   375
      Left            =   2760
      TabIndex        =   2
      Top             =   600
      Width           =   5655
   End
   Begin VB.TextBox txtInputFile 
      Height          =   375
      Left            =   2760
      TabIndex        =   1
      Top             =   120
      Width           =   5655
   End
   Begin VB.Label lblDefaultServiceType 
      Caption         =   "Default service type:"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   1560
      Width           =   2535
   End
   Begin VB.Label lblHOPO 
      Caption         =   "Home airport:"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   1080
      Width           =   2535
   End
   Begin VB.Label lblOutputFolder 
      Caption         =   "Save converted file to:"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   2535
   End
   Begin VB.Label lblInputFile 
      Caption         =   "File to convert (OAL Excel file):"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2535
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Function GetDirectoryName(ByVal FullPath As String)
    Dim intSepLoc As Integer
    
    GetDirectoryName = FullPath
    intSepLoc = InStrRev(FullPath, "\")
    If intSepLoc > 0 Then
        If intSepLoc = InStr(FullPath, "\") Then
            intSepLoc = intSepLoc + 1
        End If
        GetDirectoryName = Left(GetDirectoryName, intSepLoc - 1)
    End If
End Function

Private Function ToUpper(ByVal KeyAscii As Integer)
    ToUpper = Asc(UCase(Chr(KeyAscii)))
End Function

Private Function ValidateUserInputs() As ConverterError
    Dim oConverterError As New ConverterError
    
    If Trim(txtInputFile.Text) = "" Then
        oConverterError.ErrorMessage = "Please specify the input file!"
        oConverterError.ErrorControl = txtInputFile
        Set ValidateUserInputs = oConverterError
        Exit Function
    End If

    If Dir(txtInputFile.Text) = "" Then
        oConverterError.ErrorMessage = "File " & txtInputFile.Text & " not found!"
        oConverterError.ErrorControl = txtInputFile
        Set ValidateUserInputs = oConverterError
        Exit Function
    End If

    If Trim(txtOutputFolder.Text) = "" Then
        oConverterError.ErrorMessage = "Please specify the output folder!"
        oConverterError.ErrorControl = txtOutputFolder
        Set ValidateUserInputs = oConverterError
        Exit Function
    End If

    If Dir(txtOutputFolder.Text, vbDirectory) = "" Then
        oConverterError.ErrorMessage = "Folder " & txtOutputFolder & " not found!"
        oConverterError.ErrorControl = txtOutputFolder
        Set ValidateUserInputs = oConverterError
        Exit Function
    End If

    Set ValidateUserInputs = oConverterError
End Function

Private Function ConvertFile(ByVal InputFile As String, ByVal OutputFolder As String) As Collection
    Dim colConverterErrors As New Collection
    Dim oConverterError As New ConverterError
    Dim colFlightScheduleData As New Collection
    Dim oFlightScheduleData As FlightScheduleData
    
    Dim xlApp As Excel.Application
    Dim xlWorkbook As Excel.Workbook
    Dim xlWorksheet As Excel.Worksheet
    Dim xlRange1 As Excel.Range
    Dim xlRange2 As Excel.Range
    Dim xlRange3 As Excel.Range
    Dim i As Integer
    Dim strWsName As String
    Dim intRowCount As Integer
    Dim intColCount As Integer

    On Error GoTo ErrConvert
    
    Set xlApp = New Excel.Application
    If xlApp Is Nothing Then
        oConverterError.ErrorMessage = "EXCEL could not be started. Check that your office installation and project references are correct."
        colConverterErrors.Add oConverterError
    Else
        Set xlWorkbook = xlApp.Workbooks.Open(InputFile)
        For i = 1 To xlWorkbook.Worksheets.Count
            Set xlWorksheet = xlWorkbook.Worksheets.Item(i)

            strWsName = xlWorksheet.Name
            intRowCount = xlWorksheet.UsedRange.Rows.Count
            intColCount = xlWorksheet.UsedRange.Columns.Count

            Set xlRange1 = xlWorksheet.Cells.Item(1, 1)
            Set xlRange2 = xlWorksheet.Cells.Item(intRowCount, intColCount)
            Set xlRange3 = xlWorksheet.Range(xlRange1, xlRange2)
            
            Set oFlightScheduleData = New FlightScheduleData
            oFlightScheduleData.Name = strWsName
            oFlightScheduleData.FlightSchedule = xlRange3.Value(Excel.XlRangeValueDataType.xlRangeValueDefault)
            
            colFlightScheduleData.Add oFlightScheduleData
        Next i
    End If
    
    If Not (xlWorksheet Is Nothing) Then
        Set xlWorksheet = Nothing
    End If
    If Not (xlWorkbook Is Nothing) Then
        xlWorkbook.Close False
        Set xlWorkbook = Nothing
    End If
    If Not (xlApp Is Nothing) Then
        xlApp.Quit
        Set xlApp = Nothing
    End If
    
    For Each oFlightScheduleData In colFlightScheduleData
        Set oConverterError = CreateFile(OutputFolder, oFlightScheduleData)
        If oConverterError.ErrorMessage <> "" Then
            colConverterErrors.Add oConverterError
        End If
    Next oFlightScheduleData
    
    Set ConvertFile = colConverterErrors
    
    Exit Function
    
ErrConvert:
    oConverterError.ErrorMessage = "Failed to open input file!" & vbNewLine
    colConverterErrors.Add oConverterError
    
    If Not (xlWorksheet Is Nothing) Then
        Set xlWorksheet = Nothing
    End If
    If Not (xlWorkbook Is Nothing) Then
        xlWorkbook.Close False
        Set xlWorkbook = Nothing
    End If
    If Not (xlApp Is Nothing) Then
        xlApp.Quit
        Set xlApp = Nothing
    End If

    Set ConvertFile = colConverterErrors
End Function

Private Function CreateFile(ByVal OutputFolder As String, ByVal FlightData As FlightScheduleData) As ConverterError
    Dim oConverterError As New ConverterError
    Dim strFilename As String
    Dim ColHeaders As New Collection
    
    Dim vntData As Variant
    Dim vntCellData As Variant
    Dim intRowCount As Integer
    Dim intColCount As Integer
    Dim intRow As Integer
    Dim intCol As Integer
    
    Dim dtmFrom As Date
    Dim dtmTo As String
    Dim strArrAlc As String
    Dim strArrFlno As String
    Dim strDepAlc As String
    Dim strDepFlno As String
    Dim strDoop As String
    Dim intSeats As Integer
    Dim strAcType As String
    Dim strOrig As String
    Dim strLast As String
    Dim strNext As String
    Dim strDest As String
    Dim strOvernight As String
    Dim dtmArr As Date
    Dim dtmDep As String
    Dim strArrType As String
    Dim strDepType As String
    Dim strArrTime As String
    Dim strDepTime As String
    Dim dtmBlankTime As Date
    Dim strLine As String
    
    Dim strFileLine As String
    Dim intFileNo As Integer
    
    strFilename = OutputFolder
    If Right(strFilename, 1) <> "\" Then
        strFilename = strFilename & "\"
    End If
    strFilename = strFilename & _
        Replace(FlightData.Name, " ", "_") & _
        "_score.txt"
        
    On Error GoTo ErrCreate
    
    vntData = FlightData.FlightSchedule
    intRowCount = UBound(vntData)
    intColCount = UBound(vntData, 2)

    For intRow = 1 To intRowCount
        Select Case intRow
            Case 1, 2, 3
                'no use
            Case 4
                'header
                For intCol = 1 To intColCount
                    vntCellData = vntData(intRow, intCol)
                    If Not IsNull(vntCellData) Then
                        ColHeaders.Add intCol, CStr(vntCellData)
                    End If
                Next intCol
            Case Else
                'data
                dtmFrom = GetCellValueAsDate(vntData, intRow, "From", ColHeaders)
                dtmTo = GetCellValueAsDate(vntData, intRow, "To", ColHeaders)
                strArrAlc = GetCellValueAsString(vntData, intRow, "Arr", ColHeaders)
                strArrFlno = GetCellValueAsString(vntData, intRow, "Arr Flt No", ColHeaders)
                strDepAlc = GetCellValueAsString(vntData, intRow, "Dep", ColHeaders)
                strDepFlno = GetCellValueAsString(vntData, intRow, "Dep Flt No", ColHeaders)
                strDoop = GetCellValueAsString(vntData, intRow, "DOOP", ColHeaders)
                intSeats = GetCellValueAsInt(vntData, intRow, "Seats", ColHeaders)
                strAcType = GetCellValueAsString(vntData, intRow, "Actyp", ColHeaders)
                strOrig = GetCellValueAsString(vntData, intRow, "Orig", ColHeaders)
                strLast = GetCellValueAsString(vntData, intRow, "Last", ColHeaders)
                strNext = GetCellValueAsString(vntData, intRow, "Next", ColHeaders)
                strDest = GetCellValueAsString(vntData, intRow, "Dest", ColHeaders)
                strOvernight = GetCellValueAsString(vntData, intRow, "Overnight", ColHeaders)
                dtmArr = GetCellValueAsTime(vntData, intRow, "ATime", ColHeaders)
                dtmDep = GetCellValueAsTime(vntData, intRow, "DTime", ColHeaders)
                strArrType = GetCellValueAsString(vntData, intRow, "AType", ColHeaders)
                strDepType = GetCellValueAsString(vntData, intRow, "DType", ColHeaders)

                strArrTime = ""
                strDepTime = ""
                If dtmArr <> dtmBlankTime Then strArrTime = Format(dtmArr, "HHmm")
                If dtmDep <> dtmBlankTime Then strDepTime = Format(dtmDep, "HHmm")
                
                If strArrType = "" Then strArrType = txtDefaultServiceType.Text
                If strDepType = "" Then strDepType = txtDefaultServiceType.Text

                strArrFlno = GetFormattedFlightNumber(strArrFlno)
                strDepFlno = GetFormattedFlightNumber(strDepFlno)

                strLine = PadRight(txtHOPO.Text, 3) & _
                    PadRight(strArrAlc, 3) & _
                    PadRight(strArrFlno, 5) & _
                    PadRight(strDepAlc, 3) & _
                    PadRight(strDepFlno, 5) & _
                    Format(dtmFrom, "yyyyMMdd") & _
                    Format(dtmTo, "yyyyMMdd") & _
                    PadLeft(strDoop, 7, "0") & _
                    Format(intSeats, "000") & _
                    PadRight(strAcType, 3) & _
                    PadRight(strOrig, 3) & _
                    PadRight(strLast, 3) & _
                    PadRight(strArrTime, 4) & _
                    PadRight(strDepTime, 4) & _
                    PadRight(strOvernight, 1) & _
                    PadRight(strNext, 3) & _
                    PadRight(strDest, 3) & _
                    strArrType & _
                    strDepType

                strFileLine = strFileLine & strLine
                If intRow < intRowCount Then
                    strFileLine = strFileLine & vbNewLine
                End If
        End Select
    Next intRow

    intFileNo = FreeFile
    Open strFilename For Output As #intFileNo
    Print #intFileNo, strFileLine
    Close #intFileNo
    
    Set CreateFile = oConverterError
    
    Exit Function
    
ErrCreate:
    oConverterError.ErrorMessage = "Failed to create file " & strFilename & "!" & vbNewLine

    Set CreateFile = oConverterError
End Function

Private Function GetFormattedFlightNumber(ByVal Flno As String) As String
    Dim strRet As String
    Dim strNumberString As String
    Dim strItem As String
    Dim intItemIndex As Integer
    Dim intLen As Integer
    
    intLen = Len(Flno)
    For intItemIndex = 1 To intLen
        strItem = Mid(Flno, intItemIndex, 1)
        If IsNumeric(strItem) Then
            strNumberString = strNumberString & strItem
        Else
            Exit For
        End If
    Next intItemIndex

    If Len(strNumberString) > 0 And Len(strNumberString) < 3 Then
        strNumberString = PadLeft(strNumberString, 3, "0")
    End If
    strRet = strNumberString
    If intItemIndex <= Len(Flno) Then
        strRet = strRet & Mid(Flno, intItemIndex)
    End If

    GetFormattedFlightNumber = strRet
End Function

Private Function GetCellValueAsString(ByVal Data, ByVal RowIndex As Integer, ByVal colName As String, ColHeaders As Collection) As String
    Dim strRet As String
    Dim vntRet As Variant
    
    vntRet = GetCellValue(Data, RowIndex, colName, ColHeaders)
    If Not IsEmpty(vntRet) Then
        strRet = CStr(vntRet)
    End If
    
    GetCellValueAsString = strRet
End Function
 
Private Function GetCellValueAsInt(ByVal Data, ByVal RowIndex As Integer, ByVal colName As String, ColHeaders As Collection) As Integer
    Dim intRet As Integer
    Dim vntRet As Variant
    
    On Error GoTo ErrGetValue
    
    vntRet = GetCellValue(Data, RowIndex, colName, ColHeaders)
    If Not IsEmpty(vntRet) Then
        If IsNumeric(vntRet) Then
            intRet = CInt(vntRet)
        End If
    End If
    
ErrGetValue:
    GetCellValueAsInt = intRet
End Function

Private Function GetCellValueAsDate(ByVal Data, ByVal RowIndex As Integer, ByVal colName As String, ColHeaders As Collection) As Date
    Dim dtmRet As Date
    Dim vntRet As Variant
    
    On Error GoTo ErrGetValue
    
    vntRet = GetCellValue(Data, RowIndex, colName, ColHeaders)
    If Not IsEmpty(vntRet) Then
        If IsDate(vntRet) Then
            dtmRet = CDate(vntRet)
        End If
    End If
    
ErrGetValue:
    GetCellValueAsDate = dtmRet
End Function

Private Function GetCellValueAsTime(ByVal Data, ByVal RowIndex As Integer, ByVal colName As String, ColHeaders As Collection) As Date
    Dim dtmRet As Date
    Dim vntRet As Variant
    Dim intRet As Integer
    Dim strTime As String
    Dim dblHours As Double
    Dim dblMins As Double
    
    On Error GoTo ErrGetValue
    
    vntRet = GetCellValue(Data, RowIndex, colName, ColHeaders)
    If Not IsEmpty(vntRet) Then
        If IsNumeric(vntRet) Then
            intRet = CInt(vntRet)
            strTime = Format(intRet, "0000")
            
            dblHours = CDbl(Left(strTime, 2))
            dblMins = CDbl(Right(strTime, 2))
            
            dtmRet = Date
            dtmRet = DateAdd("h", dblHours, dtmRet)
            dtmRet = DateAdd("n", dblMins, dtmRet)
        End If
    End If
    
ErrGetValue:
    GetCellValueAsTime = dtmRet
End Function

Private Function GetCellValue(ByVal Data, ByVal RowIndex As Integer, ByVal colName As String, ColHeaders As Collection)
    Dim vntRet As Variant
    Dim intColIndex As Integer
    
    On Error GoTo ErrGetValue
    
    intColIndex = ColHeaders.Item(colName)
    vntRet = Data(RowIndex, intColIndex)
    
    GetCellValue = vntRet
    
    Exit Function
    
ErrGetValue:
    vntRet = Empty
    GetCellValue = vntRet
End Function

Private Function PadLeft(ByVal Text As String, ByVal Length As Integer, Optional ByVal PaddedChar As String) As String
    Dim strRet As String
    Dim intLen As Integer
    
    If PaddedChar = "" Then PaddedChar = " "
    
    strRet = Text
    intLen = Len(Text)
    If intLen <= Length Then
        strRet = String(Length - intLen, PaddedChar) & strRet
    End If
    
    PadLeft = strRet
End Function

Private Function PadRight(ByVal Text As String, ByVal Length As Integer, Optional ByVal PaddedChar As String) As String
    Dim strRet As String
    Dim intLen As Integer
    
    If PaddedChar = "" Then PaddedChar = " "
    
    strRet = Text
    intLen = Len(Text)
    If intLen <= Length Then
        strRet = strRet & String(Length - intLen, PaddedChar)
    End If
    
    PadRight = strRet
End Function

Private Sub cmdBrowseInputFile_Click()
    With cdgOpen
        .DialogTitle = "Open OAL Excel File"
        .Filter = "Excel Files|*.xls|All Files|*.*"
        .InitDir = App.Path
        .Flags = cdlOFNFileMustExist Or cdlOFNPathMustExist
        .ShowOpen
        
        If .FileName <> "" Then
            txtInputFile.Text = .FileName
            If Trim(txtOutputFolder.Text) = "" Then
                txtOutputFolder.Text = GetDirectoryName(txtInputFile.Text)
            End If
        End If
    End With
End Sub

Private Sub cmdBrowseOutputFolder_Click()
    Dim strFolderName As String
    
    strFolderName = ShowFolderBrowser(Me, "Please select output folder")
    If strFolderName <> "" Then
        txtOutputFolder.Text = strFolderName
    End If
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdConvert_Click()
    Dim oConverterError As ConverterError
    Dim colConverterErrors As Collection
    Dim strMessage As String
    
    Set oConverterError = ValidateUserInputs()

    If oConverterError.ErrorMessage <> "" Then
        MsgBox oConverterError.ErrorMessage, vbExclamation, Me.Caption
        If Not (oConverterError.ErrorControl Is Nothing) Then
            oConverterError.ErrorControl.SetFocus
        End If
    Else
        Set colConverterErrors = ConvertFile(txtInputFile.Text, txtOutputFolder.Text)
        If colConverterErrors.Count = 0 Then
            MsgBox "Flight data file(s) have been successfully created!", _
                vbInformation, Me.Caption
        Else
            strMessage = ""
            For Each oConverterError In colConverterErrors
                strMessage = strMessage & oConverterError.ErrorMessage & vbNewLine
            Next oConverterError
            MsgBox strMessage, vbExclamation, Me.Caption
        End If
    End If
End Sub

Private Sub Form_Load()
    Const DEFAULT_HOPO  As String = "SIN"
    Const DEFAULT_SERVICE_TYPE As String = "PAX"

    txtHOPO.Text = DEFAULT_HOPO
    txtDefaultServiceType.Text = DEFAULT_SERVICE_TYPE
End Sub

Private Sub txtDefaultServiceType_KeyPress(KeyAscii As Integer)
    KeyAscii = ToUpper(KeyAscii)
End Sub

Private Sub txtHOPO_KeyPress(KeyAscii As Integer)
    KeyAscii = ToUpper(KeyAscii)
End Sub
