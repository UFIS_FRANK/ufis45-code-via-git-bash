VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTelex 
   Caption         =   "Telex of Selected Entry"
   ClientHeight    =   4995
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6375
   Icon            =   "frmTelex.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4995
   ScaleWidth      =   6375
   ShowInTaskbar   =   0   'False
   Begin TABLib.TAB TlxTab 
      Height          =   855
      Index           =   0
      Left            =   3630
      TabIndex        =   2
      Top             =   510
      Visible         =   0   'False
      Width           =   1725
      _Version        =   65536
      _ExtentX        =   3043
      _ExtentY        =   1508
      _StockProps     =   64
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   1
      Top             =   4680
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10716
         EndProperty
      EndProperty
   End
   Begin VB.TextBox txtTelex 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   30
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   30
      Width           =   3165
   End
   Begin TABLib.TAB TlxTab 
      Height          =   855
      Index           =   1
      Left            =   3630
      TabIndex        =   3
      Top             =   1530
      Visible         =   0   'False
      Width           =   1725
      _Version        =   65536
      _ExtentX        =   3043
      _ExtentY        =   1508
      _StockProps     =   64
   End
End
Attribute VB_Name = "frmTelex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private strTLX_URNO As String
Private Sub Form_Load()
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngWidth As Single
    Dim sngHeight As Single
    
    txtTelex.FontSize = 8
    txtTelex.FontBold = True 'MyFontBold
    TlxTab(0).ResetContent
    TlxTab(1).ResetContent
    
'Read position from registry and move the window
    sngLeft = Val(GetSetting(appName:="HUB_Manager_Telex", _
                        section:="Startup", _
                        Key:="myLeft", _
                        Default:="-1"))
    sngTop = Val(GetSetting(appName:="HUB_Manager_Telex", _
                        section:="Startup", _
                        Key:="myTop", _
                        Default:="-1"))
    sngWidth = Val(GetSetting(appName:="HUB_Manager_Telex", _
                        section:="Startup", _
                        Key:="myWidth", _
                        Default:="-1"))
    sngHeight = Val(GetSetting(appName:="HUB_Manager_Telex", _
                        section:="Startup", _
                        Key:="myHeight", _
                        Default:="-1"))
                        
    If sngTop = -1 Or sngLeft = -1 Or sngWidth = -1 Or sngHeight = -1 Then
    Else
        sngLeft = Max(sngLeft, 0)
        sngTop = Max(sngTop, 0)
        sngWidth = Min(sngWidth, Screen.Width - 6000)
        sngHeight = Min(sngHeight, Screen.Height - 6000)
    
        Me.Move sngLeft, sngTop, sngWidth, sngHeight
    End If
End Sub
Public Sub TabReadTelex(TlxTabUrno As String)
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim tmpText As String
    Dim tmpSqlKey As String
    Dim LineNo As Long
    'kkh on 10/06/2008 MANTIS PRF : 0000069
    Dim strFKEY As String
    Dim i As Integer
    If TlxTabUrno <> strTLX_URNO Then
        strTLX_URNO = TlxTabUrno
        tmpText = ""
        tmpData = TlxTab(1).GetLinesByColumnValue(0, TlxTabUrno, 0)
        If tmpData <> "" Then
            LineNo = Val(tmpData)
            tmpText = TlxTab(1).GetColumnValue(LineNo, 1)
        Else
            TlxTab(0).ResetContent
            TlxTab(0).CedaServerName = strServer
            TlxTab(0).CedaPort = "3357"
            TlxTab(0).CedaHopo = strHopo
            TlxTab(0).CedaCurrentApplication = "HUB-Manager" & "," & frmData.GetApplVersion(True)
            TlxTab(0).CedaTabext = "TAB"
            TlxTab(0).CedaUser = strCurrentUser 'GetWindowsUserName()
            TlxTab(0).CedaWorkstation = frmData.aUfis.GetWorkStationName   'GetWorkstationName()
            TlxTab(0).CedaSendTimeout = "3"
            TlxTab(0).CedaReceiveTimeout = "240"
            TlxTab(0).CedaRecordSeparator = Chr(10)
            TlxTab(0).CedaIdentifier = "LoaTab"
            tmpCmd = "RT"
            tmpTable = "TLXTAB"
            'kkh on 10/06/2008 MANTIS PRF : 0000069
            'tmpFields = "TXT1,TXT2"
            If strHopo <> "BKK" Then
                tmpFields = "TXT1,TXT2,TKEY"
            Else
                tmpFields = "TXT1,TXT2,FKEY"
            End If
            tmpData = ""
            tmpSqlKey = "WHERE URNO=" & strTLX_URNO
            If strServer <> "LOCAL" Then
                Screen.MousePointer = 11
                TlxTab(0).CedaAction tmpCmd, tmpTable, tmpFields, tmpData, tmpSqlKey
                'Screen.MousePointer = 0
                'kkh on 10/06/2008 MANTIS PRF : 0000069
                strFKEY = TlxTab(0).GetColumnValue(0, 2)
                TlxTab(0).ResetContent
                If strHopo <> "BKK" Then
                    tmpSqlKey = "WHERE TKEY='" & strFKEY & "' order by MORE"
                Else
                    tmpSqlKey = "WHERE FKEY='" & strFKEY & "' order by MSNO"
                End If
                TlxTab(0).CedaAction tmpCmd, tmpTable, tmpFields, tmpData, tmpSqlKey

                If TlxTab(0).GetLineCount > 1 Then
                    For i = 0 To TlxTab(0).GetLineCount - 1
                        tmpText = tmpText & TlxTab(0).GetColumnValue(i, 0) & TlxTab(0).GetColumnValue(i, 1)
                    Next i
                Else
                    tmpText = TlxTab(0).GetColumnValue(0, 0) & TlxTab(0).GetColumnValue(0, 1)
                End If
                Screen.MousePointer = 0
            End If
            If tmpText = "" Then tmpText = "Telex not found. (TLXTAB.URNO=" & TlxTabUrno & ")"
            TlxTab(1).InsertTextLine TlxTabUrno & "," & tmpText, False
        End If
        tmpText = CleanString(tmpText, FOR_CLIENT, True)
        txtTelex.Text = tmpText
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngHeight As Single
    Dim sngWidth As Single
    
    sngLeft = Me.Left
    sngTop = Me.Top
    sngWidth = Me.Width
    sngHeight = Me.Height
    
    SaveSetting "HUB_Manager_Telex", _
                "Startup", _
                "myLeft", _
                CStr(sngLeft)
    SaveSetting "HUB_Manager_Telex", _
                "Startup", _
                "myTop", _
                CStr(sngTop)
    SaveSetting "HUB_Manager_Telex", _
                "Startup", _
                "myWidth", _
                CStr(sngWidth)
    SaveSetting "HUB_Manager_Telex", _
                "Startup", _
                "myHeight", _
                CStr(sngHeight)
                
    If UnloadMode = 0 Then
        Cancel = True
        frmMain.cmdTelex.value = 0
        Me.Hide
    End If
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - txtTelex.Left * 2
    If NewSize > 15 Then txtTelex.Width = NewSize
    NewSize = Me.ScaleHeight - StatusBar1.Height - 60
    If NewSize > 15 Then txtTelex.Height = NewSize
End Sub

