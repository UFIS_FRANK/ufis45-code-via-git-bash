VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{6782E133-3223-11D4-996A-0000863DE95C}#1.1#0"; "UGantt.ocx"
Begin VB.Form frmCompressedFligths 
   Caption         =   "Compressed Flights ..."
   ClientHeight    =   4590
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   9825
   Icon            =   "frmCompressedFligths.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4590
   ScaleWidth      =   9825
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox cbToolButton 
      Caption         =   "&Shrink to critical"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   2
      Left            =   7650
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   60
      Width           =   1650
   End
   Begin VB.PictureBox picToolBar 
      AutoRedraw      =   -1  'True
      Height          =   495
      Left            =   8100
      ScaleHeight     =   435
      ScaleWidth      =   1575
      TabIndex        =   9
      Top             =   420
      Width           =   1635
   End
   Begin VB.CheckBox cbToolButton 
      Caption         =   "&Reorganize"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   6420
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   60
      Width           =   1155
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   3735
      Visible         =   0   'False
      Width           =   1755
   End
   Begin VB.Frame fraCalc 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   795
      Left            =   2280
      TabIndex        =   4
      Top             =   3180
      Width           =   7635
      Begin VB.Label Label1 
         BackColor       =   &H00FFFF00&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Please wait while calculating ..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFF00&
         Height          =   675
         Left            =   60
         TabIndex        =   5
         Top             =   60
         Width           =   7515
      End
   End
   Begin VB.CheckBox cbToolButton 
      Caption         =   "&Now <|>>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   60
      Width           =   1155
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   4980
      Top             =   300
   End
   Begin MSComctlLib.StatusBar statusbar 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   1
      Top             =   4275
      Width           =   9825
      _ExtentX        =   17330
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16801
            Text            =   "stats"
            TextSave        =   "stats"
         EndProperty
      EndProperty
   End
   Begin UGANTTLib.UGantt gantt 
      Height          =   1935
      Left            =   60
      TabIndex        =   0
      Top             =   600
      Width           =   4575
      _Version        =   65536
      _ExtentX        =   8070
      _ExtentY        =   3413
      _StockProps     =   64
   End
   Begin MSComctlLib.Slider slDuration 
      Height          =   315
      Left            =   3300
      TabIndex        =   3
      ToolTipText     =   "Change visible time frame"
      Top             =   60
      Width           =   1995
      _ExtentX        =   3519
      _ExtentY        =   556
      _Version        =   393216
      Min             =   2
      Max             =   24
      SelStart        =   5
      TickFrequency   =   2
      Value           =   5
   End
   Begin VB.Label lblAboveText 
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFF00&
      Height          =   315
      Index           =   0
      Left            =   1500
      TabIndex        =   7
      Top             =   60
      Width           =   1395
   End
End
Attribute VB_Name = "frmCompressedFligths"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Function StoreMainGanttLineRecords() As Long
    Dim cnt As Long
    Dim strValue As String
    Dim myFrom As Date
    Dim myTo As Date
    Dim myT As TABLib.Tab
    Dim strLineNo As String
    Dim strBarKey As String
    Dim strAdid As String
    Dim strTisa As String
    Dim strTisd As String
    Dim strLineKey As String
    Dim strCompLineKey As String
    Dim interval As Long
    Dim llGanttLine As Long
    Dim strFlno As String
    Dim colBody As Long
    Dim leftTriaColor As Long
    Dim strStoa As String
    Dim datStoa As Date
    Dim strStod As String
    Dim datStod As Date
    Dim rightTriaColor As Long
    Dim strTriaID As String
    Dim datTimeFrameFrom As Date
    Dim datTimeFrameTo As Date
    Dim strLineValues As String
    Dim i As Long
    Dim strVal As String
    Dim blInsertIt As Boolean
    
    Set myT = frmData.tabCompressedView
    
    datTimeFrameFrom = CedaFullDateToVb(strTimeFrameFrom)
    datTimeFrameTo = CedaFullDateToVb(strTimeFrameTo)
    If TimesInUTC = False Then
        UTCOffset = frmData.GetUTCOffset
        datTimeFrameFrom = DateAdd("h", UTCOffset, datTimeFrameFrom)
        datTimeFrameTo = DateAdd("h", UTCOffset, datTimeFrameTo)
    End If
    
    cnt = frmData.tabData(0).GetLineCount
    myT.ResetContent
    For i = 0 To cnt - 1
        blInsertIt = True
        strValue = frmData.tabData(0).LogicalFieldList
        
        leftTriaColor = -1
        rightTriaColor = -1
        strBarKey = frmData.tabData(0).GetFieldValue(i, "URNO")
        strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
        strFlno = frmData.tabData(0).GetFieldValue(i, "FLNO")
        
        strLineValues = frmData.tabData(0).GetLineValues(i)
        If strAdid = "A" Then
            strValue = frmData.tabData(0).GetFieldValue(i, "TIFA")
            If Trim(strValue) <> "" Then
                interval = GetArrivalBarLenInMinutes(strBarKey, strValue)
                myFrom = CedaFullDateToVb(strValue)
                myTo = DateAdd("n", interval, myFrom)
                strLineKey = frmData.tabData(0).GetFieldValue(i, "PSTA")
                strStoa = frmData.tabData(0).GetFieldValue(i, "STOA")
                colBody = colorArrivalBar 'vbWhite
                If Trim(strStoa) <> "" Then
                    datStoa = CedaFullDateToVb(strStoa)
                    If frmMain.HasArrivalConflict(strBarKey, datStoa, False) = True Then
                        'warning colour orange during frmload
'                        If compressedFlightBarCritColour = True Then
                            colBody = vbRed
'                        Else
'                            If compressedFlightBarWarnColour = True Then
'                                colBody = colOrange
'                            End If
'                        End If
                    Else
                        If cbToolButton(2).value = 1 Then
                            blInsertIt = False
                        End If
                    End If
                End If
                strTisa = frmData.tabData(0).GetFieldValue(i, "TISA")
                leftTriaColor = frmMain.GetTisaColor(strTisa)
            End If
        End If
        If strAdid = "D" Then
            strValue = frmData.tabData(0).GetFieldValue(i, "TIFD")
            If Trim(strValue) <> "" Then
                interval = GetDepartureBarLenInMinutes(strBarKey, strValue)
                myTo = CedaFullDateToVb(strValue)
                myFrom = DateAdd("n", -interval, myTo)
                strLineKey = frmData.tabData(0).GetFieldValue(i, "PSTD")
                If HasConnectionConflicts(strBarKey) = False Then
                    colBody = colorDepartureBar 'colLightBlue
                Else
                    colBody = vbRed
                End If
                If cbToolButton(2).value = 1 Then
                    blInsertIt = False
                End If
                strTisd = frmData.tabData(0).GetFieldValue(i, "TISD")
                rightTriaColor = frmMain.GetTisdColor(strTisd)
            End If
        End If
        If myTo >= datTimeFrameFrom And myFrom <= datTimeFrameTo And blInsertIt = True Then
            strVal = strBarKey + "," + Format(myFrom, "YYYYMMDDhhmmss") + "," + _
                     Format(myTo, "YYYYMMDDhhmmss") + "," + strFlno + _
                     "," + CStr(colBody) + "," + _
                     CStr(leftTriaColor) + "," + CStr(rightTriaColor)
            myT.InsertTextLine strVal, False
        End If
    Next i
    StoreMainGanttLineRecords = myT.GetLineCount
    myT.Sort "1", True, True
End Function

Private Sub cbToolButton_Click(Index As Integer)
    Dim d As Date
    
    If Index = 0 Then
        cbToolButton(Index).value = 0
        d = Now
        If TimesInUTC = True Then
            d = DateAdd("h", -(UTCOffset + 1), d)
        Else
            d = DateAdd("h", -1, d)
        End If
        gantt.ScrollTo d
        lblAboveText(0) = Format(d, "dd.mm.YY")
        lblAboveText(0).Refresh
        gantt.Refresh
    End If
    If Index = 1 Then
        cbToolButton(Index).value = 0
        fraCalc.Visible = True
        fraCalc.Refresh
        Timer1.Enabled = True
        Timer1.interval = 5
    End If
    If Index = 2 Then
        If cbToolButton(Index).value = 1 Then
            cbToolButton(Index).backColor = vbGreen
        Else
            cbToolButton(Index).backColor = vbButtonFace
        End If
        Allocate
    End If
End Sub

Private Sub Command1_Click()
    MsgBox gantt.GetTotalBarCount
End Sub

Private Sub Form_Load()
    Dim myT As TABLib.Tab
    Dim TFFrom As Date
    Dim TFTo As Date
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngWidth As Single
    Dim sngHeight As Single
    
'Read position from registry and move the window
    sngLeft = Val(GetSetting(appName:="HUB_Manager_CompressedView", _
                        section:="Startup", _
                        Key:="myLeft", _
                        Default:="-1"))
    sngTop = Val(GetSetting(appName:="HUB_Manager_CompressedView", _
                        section:="Startup", _
                        Key:="myTop", _
                        Default:="-1"))
    sngWidth = Val(GetSetting(appName:="HUB_Manager_CompressedView", _
                        section:="Startup", _
                        Key:="myWidth", _
                        Default:="-1"))
    sngHeight = Val(GetSetting(appName:="HUB_Manager_CompressedView", _
                        section:="Startup", _
                        Key:="myHeight", _
                        Default:="-1"))
    
    If sngTop = -1 Or sngLeft = -1 Or sngWidth = -1 Or sngHeight = -1 Then
        Me.Top = 0
        Me.Left = 0
        Me.Height = Screen.Height - 6000
        Me.Width = Screen.Width - 6000
    Else
        sngLeft = Max(sngLeft, 0)
        sngTop = Max(sngTop, 0)
        sngWidth = Min(sngWidth, Screen.Width - 6000)
        sngHeight = Min(sngHeight, Screen.Height - 6000)
    
        Me.Move sngLeft, sngTop, sngWidth, sngHeight
    End If
'END: Read position from registry and move the window
    
    slDuration.value = 4
    InitGantt
    Set myT = frmData.tabCompressedView
    myT.ResetContent
    myT.HeaderString = "KEY,FROM,TO,TEXT,COLOR,LEFTTRIA,RIGHTTRIA"
    myT.LogicalFieldList = "KEY,FROM,TO,TEXT,COLOR,LEFTTRIA,RIGHTTRIA"
    myT.HeaderLengthString = "100,130,130,130,100,100,100"
    myT.EnableHeaderSizing True
    myT.ShowHorzScroller True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngHeight As Single
    Dim sngWidth As Single
    
    sngLeft = Me.Left
    sngTop = Me.Top
    sngWidth = Me.Width
    sngHeight = Me.Height
    
    SaveSetting "HUB_Manager_CompressedView", _
                "Startup", _
                "myLeft", _
                CStr(sngLeft)
    SaveSetting "HUB_Manager_CompressedView", _
                "Startup", _
                "myTop", _
                CStr(sngTop)
    SaveSetting "HUB_Manager_CompressedView", _
                "Startup", _
                "myWidth", _
                CStr(sngWidth)
    SaveSetting "HUB_Manager_CompressedView", _
                "Startup", _
                "myHeight", _
                CStr(sngHeight)
End Sub

Private Sub Form_Resize()
    gantt.Top = picToolBar.Height + 10 'cbToolButton(0).Height - cbToolButton(0) + 50
    If Me.ScaleWidth - 100 > 0 Then
        gantt.Width = Me.ScaleWidth - 100
    End If
    If Me.ScaleHeight - statusbar.Height - 30 - picToolBar.Height > 0 Then
        gantt.Height = Me.ScaleHeight - statusbar.Height - 30 - picToolBar.Height '(cbToolButton(0).Height - cbToolButton(0))
    End If
    gantt.Refresh
    picToolBar.Left = 0
    picToolBar.Top = 0
    Set cbToolButton(0).Container = picToolBar
    Set cbToolButton(1).Container = picToolBar
    Set cbToolButton(2).Container = picToolBar
    Set lblAboveText(0).Container = picToolBar
    Set slDuration.Container = picToolBar

    picToolBar.Width = Me.ScaleWidth
    DrawBackGround picToolBar, 7, True, False
    statusbar.ZOrder
End Sub

Private Sub Form_Unload(Cancel As Integer)
    bgfrmCompressedFlightsOpen = False
End Sub

Private Sub gantt_ActualTimescaleDate(ByVal TimescaleDate As Date)
    lblAboveText(0) = Format(TimescaleDate, "dd.mm.YY")
    lblAboveText(0).Refresh
End Sub

Private Sub gantt_OnLButtonDblClkBar(ByVal Key As String, ByVal nFlags As Integer)
    frmMain.ShowFeedingArrivals Key, Me
End Sub

Private Sub gantt_OnLButtonDownBar(ByVal Key As String, ByVal nFlags As Integer)
    Dim StartTime As Date
    Dim endTime As Date
    
    frmMain.SelectMainGanttBar Key, nFlags
End Sub

Private Sub gantt_OnMouseMoveBar(ByVal Key As String)
    Dim strValues As String
    Dim strVal As String
    Dim strRet As String
    Dim llCurrLine As Long
    Dim strAdid As String
    Dim myDat As Date
    
    statusbar.Panels(1).Text = ""
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", Key, 0)
    llCurrLine = frmData.tabData(0).GetNextResultLine
    If llCurrLine > -1 Then
        strAdid = frmData.tabData(0).GetFieldValue(llCurrLine, "ADID")
        If strAdid = "A" Then
            strValues = frmData.tabData(0).GetFieldValue(llCurrLine, "ORG3") + ": STA="
            strVal = frmData.tabData(0).GetFieldValue(llCurrLine, "STOA")
            If Trim(strVal) <> "" Then
                myDat = CedaFullDateToVb(strVal)
                If Trim(strVal) = "" Then strVal = "0"
                strValues = strValues + Format(myDat, "hh:mm")
                strVal = frmData.tabData(0).GetFieldValue(llCurrLine, "ETAI")
                If Trim(strVal) <> "" Then
                    myDat = CedaFullDateToVb(strVal)
                    strValues = strValues + " ETA=" + Format(myDat, "hh:mm")
                End If
            End If
        Else
            strValues = frmData.tabData(0).GetFieldValue(llCurrLine, "DES3") + ": STD="
            If Trim(strVal) = "" Then strVal = "0"
            strVal = frmData.tabData(0).GetFieldValue(llCurrLine, "STOD")
            myDat = CedaFullDateToVb(strVal)
            strValues = strValues + Format(myDat, "hh:mm")
            strVal = frmData.tabData(0).GetFieldValue(llCurrLine, "ETDI")
            If Trim(strVal) <> "" Then
                myDat = CedaFullDateToVb(strVal)
                strValues = strValues + " ETD=" + Format(myDat, "hh:mm")
            End If
        End If
    End If
    statusbar.Panels(1).Text = strValues
End Sub

Private Sub slDuration_Scroll()
    Dim i As Integer
    gantt.TimeScaleDuration = slDuration.value
    gantt.Refresh
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    Allocate
End Sub

Public Sub Allocate()
    Dim i As Long
    Dim d As Date
    Dim j As Long
    Dim k As Long
    Dim myT As TABLib.Tab
    Dim cntinsert As Long
    Dim tf() As UGANTTLib.UTIME_FRAME
    Dim blInserted As Boolean
    Dim blContinue As Boolean
    Dim llCurrT As Long 'Current line to myT
    Dim llCurrG As Long 'Current line of gantt
    Dim dStart As Date
    Dim dEnd As Date
    Dim cnt As Long
    Dim llTmp As Long
    Dim strKey As String
    Dim strTriaID As String
    Dim myCounter As Long
    Dim llAllocTabLine As Long
    Dim datTF As Date
    Dim datTT As Date
    Dim datFrom As Date
    Dim datTo As Date
    
    datFrom = CedaFullDateToVb(strTimeFrameFrom)
    datTo = CedaFullDateToVb(strTimeFrameTo)
    
    If TimesInUTC = False Then
        UTCOffset = frmData.GetUTCOffset
        datFrom = DateAdd("h", UTCOffset, datFrom)
        datTo = DateAdd("h", UTCOffset, datTo)
    End If
    
    Me.Caption = "Compressed flights: Loaded [" & datFrom & "] - [" & datTo & "]"
    
    InitGantt
    Set myT = frmData.tabCompressedView
    fraCalc.Visible = True
    fraCalc.ZOrder
    fraCalc.Refresh
    
    gantt.TimeScaleDuration = 12
    gantt.Refresh
    
    gantt.ScrollTo CedaFullDateToVb(strTimeFrameFrom)
    datTF = CedaFullDateToVb(strTimeFrameFrom)
    datTT = CedaFullDateToVb(strTimeFrameTo)
    datTT = DateAdd("d", 1000, datTT)
    statusbar.Panels(1).Text = "Preparing flight data ..."
    For i = 0 To 200
        gantt.AddLineAt i, CStr(i), CStr(i + 1)
        gantt.SetLineSeparator i, 1, vbBlack, 2, 1, 0
    Next i
    If TimesInUTC = True Then
        gantt.UTCOffsetInHours = -UTCOffset
    Else
        gantt.UTCOffsetInHours = 0
    End If
    gantt.Refresh
    
    Me.MousePointer = vbHourglass

    'iterate through the lines of the main gantt
    statusbar.Panels(1).Text = "Preparing line data ..."
    StoreMainGanttLineRecords

    blContinue = True
    cntinsert = 0
    gantt.Refresh
    statusbar.Panels(1).Text = "Calculating the gantt data ..."
    While blContinue = True
        blInserted = False
        cnt = myT.GetLineCount - 1
        For i = 0 To cnt
            dStart = CedaFullDateToVb(myT.GetFieldValue(i, "FROM"))
            datTF = DateAdd("n", -2, dStart)
            dEnd = CedaFullDateToVb(myT.GetFieldValue(i, "TO"))
            For j = 0 To gantt.TabLines - 1
                tf = gantt.GetGapsForTimeframe(j, datTF, datTT)
                For k = 0 To UBound(tf)
                    If dStart >= tf(k).fStart And dEnd <= tf(k).fEnd Then
                        blInserted = True
                        strKey = myT.GetFieldValue(i, "KEY")
                        gantt.AddBarToLine j, strKey, dStart, dEnd, myT.GetFieldValue(i, "TEXT"), 1, _
                                           vbCyan, vbBlack, vbRed, vbBlack, vbBlack, "", "", vbBlack, vbBlack
                        gantt.SetBarColor strKey, Val(myT.GetFieldValue(i, "COLOR")) 'colWhite
                        strTriaID = myT.GetFieldValue(i, "LEFTTRIA") + myT.GetFieldValue(i, "RIGHTTRIA")
                        gantt.SetBarTriangles strKey, strTriaID, Val(myT.GetFieldValue(i, "LEFTTRIA")), Val(myT.GetFieldValue(i, "RIGHTTRIA")), True
                        llCurrT = i
                        cntinsert = cntinsert + 1
                        If cntinsert Mod 50 = 0 Then
                            d = dStart
                            d = DateAdd("h", -2, d)
                            gantt.ScrollTo d
                            lblAboveText(0) = Format(d, "dd.mm.YY")
                            lblAboveText(0).Refresh
                            gantt.Refresh
                            'Me.Caption = cntinsert
                        End If
                        j = gantt.TabLines
                        i = cnt + 1
                        k = UBound(tf) + 1
                    End If
                Next k
                'ReDim tf(0)
            Next j
        Next i
        If blInserted = True Then
            'remove line from tab
            myT.DeleteLine llCurrT
        End If
        If myT.GetLineCount = 0 Then
            blContinue = False
        End If
    Wend
    d = Now
    If TimesInUTC = True Then
        d = DateAdd("h", -(UTCOffset + 1), d)
    Else
        d = DateAdd("h", -1, d)
    End If
    gantt.ScrollTo d
    lblAboveText(0) = Format(d, "dd.mm.YY")
    lblAboveText(0).Refresh
    fraCalc.Visible = False
    gantt.TimeScaleDuration = slDuration.value
    'gantt.TimeScaleDuration = 4
    gantt.Refresh
    Me.MousePointer = 0
    statusbar.Panels(1).Text = "Ready"
End Sub
Sub InitGantt()
    Dim datTF As Date
    Dim datTT As Date
    Dim TFFrom As Date
    Dim TFTo As Date

    gantt.ResetContent
    gantt.TabHeaderLengthString = "100"                   'init the width of the tab-columns
    gantt.TabSetHeaderText ("All Flights")                   'set the tab-headlines
    gantt.SplitterPosition = 100                          'the splitterposition depends on the
    gantt.TabBodyFontName = "Arial"
    gantt.TabFontSize = 14 'slFontSize.value
    gantt.TabHeaderFontName = "Arial"
    gantt.TabHeaderFontSize = 14
    gantt.BarOverlapOffset = 10
    gantt.BarNumberExpandLine = 0
    gantt.TabBodyFontBold = True
    gantt.TabHeaderFontBold = True
    If frmMain.cbToolButton(8).value = 0 Then
        gantt.LifeStyle = True
    Else
        gantt.LifeStyle = False
    End If
    TFFrom = CedaFullDateToVb(strTimeFrameFrom)
    TFFrom = DateAdd("d", -1, TFFrom)
    TFTo = CedaFullDateToVb(strTimeFrameTo)
    TFTo = DateAdd("d", 1, TFTo)
    gantt.TimeFrameFrom = TFFrom ' CedaFullDateToVb(strTimeFrameFrom)  'dateFrom
    gantt.TimeFrameTo = TFTo  ' CedaFullDateToVb(strTimeFrameTo)
End Sub



