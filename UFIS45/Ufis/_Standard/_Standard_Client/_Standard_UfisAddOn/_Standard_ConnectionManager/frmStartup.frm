VERSION 5.00
Begin VB.Form frmStartup 
   Caption         =   "Startup ..."
   ClientHeight    =   9315
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7650
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9315
   ScaleWidth      =   7650
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picFrame 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   9375
      Left            =   0
      ScaleHeight     =   9375
      ScaleWidth      =   7815
      TabIndex        =   0
      Top             =   0
      Width           =   7815
      Begin VB.TextBox lblHeader 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   465
         Index           =   0
         Left            =   1200
         Locked          =   -1  'True
         TabIndex        =   3
         Text            =   "HUB Manager"
         Top             =   300
         Width           =   6135
      End
      Begin VB.TextBox txtCurrentStatus 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   240
         TabIndex        =   2
         Text            =   "Connecting ..."
         Top             =   8340
         Width           =   7155
      End
      Begin VB.TextBox txtDoneStatus 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   240
         TabIndex        =   1
         Top             =   8700
         Width           =   7155
      End
      Begin VB.Timer Timer1 
         Interval        =   10
         Left            =   6300
         Top             =   660
      End
      Begin VB.Image Image3 
         Height          =   855
         Left            =   240
         Picture         =   "frmStartup.frx":0000
         Stretch         =   -1  'True
         Top             =   120
         Width           =   885
      End
      Begin VB.Image Image2 
         BorderStyle     =   1  'Fixed Single
         Height          =   7155
         Left            =   240
         Picture         =   "frmStartup.frx":9972
         Top             =   1140
         Width           =   7155
      End
      Begin VB.Image Image4 
         BorderStyle     =   1  'Fixed Single
         Height          =   960
         Left            =   240
         Picture         =   "frmStartup.frx":343FF
         Stretch         =   -1  'True
         Top             =   1140
         Visible         =   0   'False
         Width           =   2250
      End
   End
End
Attribute VB_Name = "frmStartup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    DrawBackGround picFrame, 7, True, True
    Me.Caption = "Startup on Server: " & strServer & " ..."
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    Me.Tag = Me.top
    Me.Refresh
End Sub
