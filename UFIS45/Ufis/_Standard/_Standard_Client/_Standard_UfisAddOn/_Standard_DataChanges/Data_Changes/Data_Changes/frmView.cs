using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Microsoft.Win32;
using Ufis.Utils;
using Ufis.Data;
using Data_Changes.Ctrl;

namespace Data_Changes
{
    /// <summary>
    /// Summary description for frmView.
    /// </summary>
    public class frmView : System.Windows.Forms.Form
    {
        #region ----- myMembers

        IDatabase myDB = null;
        string myEndSign = "'";
        //		string myEndSignLike = "%'";
        public string mySelection = "";
        public string mySelectionDeleted = "";
        public string myWindowText = "";
        public ArrayList myLocation = new ArrayList();
        //		string mySelRotation = " [ROTATIONS]";
        string mySelFlnu = " AND FLTN = '";
        string mySelFlns = " AND FLNS = '";
        string mySelAlc2 = " AND ALC2 = '";
        string mySelAlc3 = " AND ALC3 = '";
        string mySelAct3 = " AND ACT3 = '";
        string mySelAct5 = " AND ACT5 = '";
        string mySelTtyp = " AND TTYP = '";
        string mySelStyp = " AND STYP = '";
        string mySelRegn = " AND REGN = '";
        string mySelCsgn = " AND CSGN = '";
        public string omSelectedTabPage = "Flight Data";
        string[] arrFieldCodes;
        private bool bFirstTime = true;

        string mySaveFile = "C:\\Ufis\\System\\DATACHANGE_VIEW.txt";
        public static frmView myThis;
        private bool m_ACRloaded = false;
        public bool m_CSGNloaded = false;


        private bool m_isConflict;
        private Label lblUser;
        private TextBox txtUserValue1;
        private Label label25;

        //Common CKI related members
        string strFileNameCKI = "C:\\Ufis\\System\\DATACHANGE_CKI.txt";
        public string m_selCKI = "";

        #endregion ----- myMembers

        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageButtons;
        private System.Windows.Forms.PictureBox pictureBoxButton;
        private System.Windows.Forms.CheckBox cbUTC;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.ComponentModel.IContainer components;
        private ITable omTabTab = null; //nr
        private ITable omSysTab = null; //nr
        public string omTableCode = "";
        

        private string[] arrTableNames;
        private System.Windows.Forms.TextBox txtACTUrno;
        private System.Windows.Forms.TabPage FlightData;
        public System.Windows.Forms.RadioButton rbDeletedFlights;
        public System.Windows.Forms.RadioButton rbChanged;
        public System.Windows.Forms.RadioButton rbFilterButtons;
        private System.Windows.Forms.CheckBox cbRunway;
        private System.Windows.Forms.CheckBox cbExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbPosition;
        private System.Windows.Forms.CheckBox cbCheckin;
        private System.Windows.Forms.CheckBox cbLounge;
        private System.Windows.Forms.CheckBox cbBelt;
        private System.Windows.Forms.CheckBox cbGate;
        public System.Windows.Forms.CheckBox cbTakeOver;
        private System.Windows.Forms.Button btnNow;
        public System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.Label lblTo;
        public System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.CheckBox cbArrival;
        private System.Windows.Forms.TextBox txtCallSignValue;
        private System.Windows.Forms.TextBox txtACT5Value;
        private System.Windows.Forms.TextBox txtALT3Value;
        private System.Windows.Forms.TextBox txtFLNUValue;
        private System.Windows.Forms.TextBox txtFLTSValue;
        private System.Windows.Forms.TextBox txtREGNValue;
        private System.Windows.Forms.TextBox txtALTValue;
        private System.Windows.Forms.TextBox txtSTYValue;
        private System.Windows.Forms.TextBox txtNATValue;
        private System.Windows.Forms.TextBox txtACTValue;
        private System.Windows.Forms.Label lblCallSign;
        private System.Windows.Forms.CheckBox cbDeparture;
        private System.Windows.Forms.Button btnACR;
        private System.Windows.Forms.Button btnSTY;
        private System.Windows.Forms.Button btnNAT;
        private System.Windows.Forms.Button btnALC;
        private System.Windows.Forms.Label lblREGN;
        private System.Windows.Forms.Label lblSTY;
        private System.Windows.Forms.Label lblNAT;
        private System.Windows.Forms.Label lblFLNU;
        private System.Windows.Forms.Label lblALC;
        private System.Windows.Forms.Button btnACT;
        private System.Windows.Forms.Label lblACT;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButton1;
        public System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button5;
        public System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label12; 
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TabPage BasicData;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbTable;
        public System.Windows.Forms.CheckBox cbTakeOver_BD;
        private System.Windows.Forms.Button button7;
        public System.Windows.Forms.DateTimePicker dateTimePickerTo_BD;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.DateTimePicker dateTimePickerFrm_BD;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TabControl tabControl1;
        
        
        private TextBox txtUserValue3;
        private TextBox txtUserValue2;
        private CheckBox cbTowing;
        private TabPage CCAData;
        private DateTimePicker dtpOpenTo;
        private DateTimePicker dtpOpenFrom;
        private Label label21;
        private Label label19;
        private TextBox txtCounter;
        private DateTimePicker dtpCloseTo;
        private DateTimePicker dtpCloseFrom;
        private PictureBox pictureBox4;
        private GroupBox groupBox2;
        private Label label23;
        private Label label20;
        private GroupBox groupBox1;
        private Label label22;
        //private TextBox txtUserValue1;
        //private Label lblUser;
        //private TextBox txtUserValue2;
       private Label label24;
        //private TextBox txtUserValue3;
        
        private RadioButton rbFilterConflict;
        private CheckBox cbConflict;
        // Array of Table Names
        private string[] arrTableCodes;

        public bool IsTowing
        {
            get { return cbTowing.Checked; }
        }

        public bool IsArrival
        {
            get { return cbArrival.Checked; }
        }

        public bool IsDeparture
        {
            get { return cbDeparture.Checked; }
        }
        
        public bool IsConflict
        {
            get
            {
                return this.cbConflict.Checked;
            }
        }


        public bool IsConflictSelected
        {
            get
            {
                return this.rbFilterConflict.Checked;
            }
        }

        public string User1
        {
            get { return txtUserValue1.Text; }
        }

        public string User2
        {
            get { return txtUserValue2.Text; }
        }

        public string User3
        {
            get { return txtUserValue3.Text; }
        }
        //public string User2
        //{
        //    get { return txtUserValue2.Text; }
        //}

        //public string User3
        //{
        //    get { return txtUserValue3.Text; }
        //}


        public frmView()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            myThis = this;
            //
            // TODO: Add any constructor code after InitializeComponent call
            //

            Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\DATACHANGE.INI");

            string strTables = myIni.IniReadValue("BASIC_DATA", "TABLES");

            if (strTables == "")
            {
                this.tabControl1.TabPages.Remove(BasicData);
            }

            string strPath = myIni.path;

            if (strPath.Length > 0)
            {
                //string mySaveFile = "C:\\Ufis\\System\\DATACHANGE_VIEW.txt";
                mySaveFile = strPath.Substring(0, 2) + "\\Ufis\\System\\DATACHANGE_VIEW.txt";
                //string strFileNameCKI = "C:\\Ufis\\System\\DATACHANGE_CKI.txt";
                strFileNameCKI = strPath.Substring(0, 2) +  "\\Ufis\\System\\DATACHANGE_CKI.txt";

            }

            //Check for CKI TAB
            //string strTables = myIni.IniReadValue("BASIC_DATA", "TABLES");
            
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        public static frmView GetThis()
        {
            return myThis;
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(frmView));
            this.imageList2 = new ImageList(this.components);
            this.imageList1 = new ImageList(this.components);
            this.imageButtons = new ImageList(this.components);
            this.pictureBoxButton = new PictureBox();
            this.cbUTC = new CheckBox();
            this.btnClose = new Button();
            this.btnNew = new Button();
            this.btnApply = new Button();
            this.btnSave = new Button();
            this.txtACTUrno = new TextBox();
            this.toolTip1 = new ToolTip(this.components);
            this.cbRunway = new CheckBox();
            this.cbExit = new CheckBox();
            this.cbPosition = new CheckBox();
            this.cbCheckin = new CheckBox();
            this.cbLounge = new CheckBox();
            this.cbBelt = new CheckBox();
            this.cbGate = new CheckBox();
            this.btnNow = new Button();
            this.btnACR = new Button();
            this.btnSTY = new Button();
            this.btnNAT = new Button();
            this.btnALC = new Button();
            this.btnACT = new Button();
            this.button1 = new Button();
            this.button2 = new Button();
            this.button3 = new Button();
            this.button4 = new Button();
            this.button5 = new Button();
            this.button6 = new Button();
            this.button7 = new Button();
            this.FlightData = new TabPage();
            this.rbFilterConflict = new RadioButton();
            this.cbTowing = new CheckBox();
            this.rbDeletedFlights = new RadioButton();
            this.rbChanged = new RadioButton();
            this.rbFilterButtons = new RadioButton();
            this.label1 = new Label();
            this.cbTakeOver = new CheckBox();
            this.dateTimePickerTo = new DateTimePicker();
            this.lblTo = new Label();
            this.dateTimePickerFrom = new DateTimePicker();
            this.lblFrom = new Label();
            this.cbArrival = new CheckBox();
            this.txtCallSignValue = new TextBox();
            this.txtACT5Value = new TextBox();
            this.txtALT3Value = new TextBox();
            this.txtFLNUValue = new TextBox();
            this.txtFLTSValue = new TextBox();
            this.txtREGNValue = new TextBox();
            this.txtALTValue = new TextBox();
            this.txtSTYValue = new TextBox();
            this.txtNATValue = new TextBox();
            this.txtACTValue = new TextBox();
            this.lblCallSign = new Label();
            this.cbDeparture = new CheckBox();
            this.lblREGN = new Label();
            this.lblSTY = new Label();
            this.lblNAT = new Label();
            this.lblFLNU = new Label();
            this.lblALC = new Label();
            this.lblACT = new Label();
            this.pictureBox1 = new PictureBox();
            this.textBox1 = new TextBox();
            this.label2 = new Label();
            this.label3 = new Label();
            this.radioButton1 = new RadioButton();
            this.radioButton2 = new RadioButton();

            this.lblUser = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            
            this.label4 = new Label();
            this.label5 = new Label();
            this.label6 = new Label();
            this.label7 = new Label();
            this.label8 = new Label();
            this.label9 = new Label();
            this.label10 = new Label();
            this.pictureBox2 = new PictureBox();
            this.label11 = new Label();
            this.textBox2 = new TextBox();
            this.textBox3 = new TextBox();
            this.textBox4 = new TextBox();
            this.radioButton3 = new RadioButton();
            this.textBox5 = new TextBox();
            this.label12 = new Label();
            this.label13 = new Label();
            this.radioButton4 = new RadioButton();
            this.label14 = new Label();
            this.label15 = new Label();
            this.BasicData = new TabPage();
            this.panel1 = new Panel();
            this.label18 = new Label();
            this.cbTable = new ComboBox();
            this.cbTakeOver_BD = new CheckBox();
            this.dateTimePickerTo_BD = new DateTimePicker();
            this.label16 = new Label();
            this.dateTimePickerFrm_BD = new DateTimePicker();
            this.label17 = new Label();
            this.pictureBox3 = new PictureBox();
            this.tabControl1 = new TabControl();
            this.CCAData = new TabPage();
            this.groupBox2 = new GroupBox();
            this.label23 = new Label();
            this.label20 = new Label();
            this.dtpCloseTo = new DateTimePicker();
            this.dtpCloseFrom = new DateTimePicker();
            this.groupBox1 = new GroupBox();
            this.label22 = new Label();
            this.label19 = new Label();
            this.dtpOpenTo = new DateTimePicker();
            this.dtpOpenFrom = new DateTimePicker();
            this.txtCounter = new TextBox();
            this.label21 = new Label();
            this.pictureBox4 = new PictureBox();
            this.cbConflict = new CheckBox();
            ((ISupportInitialize)this.pictureBoxButton).BeginInit();
            this.FlightData.SuspendLayout();
            ((ISupportInitialize)this.pictureBox1).BeginInit();
            ((ISupportInitialize)this.pictureBox2).BeginInit();
            this.BasicData.SuspendLayout();
            ((ISupportInitialize)this.pictureBox3).BeginInit();
            this.tabControl1.SuspendLayout();
            this.CCAData.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((ISupportInitialize)this.pictureBox4).BeginInit();
            base.SuspendLayout();
            this.imageList2.ImageStream = (ImageListStreamer)manager.GetObject("imageList2.ImageStream");
            this.imageList2.TransparentColor = Color.White;
            this.imageList2.Images.SetKeyName(0, "");
            this.imageList2.Images.SetKeyName(1, "");
            this.imageList2.Images.SetKeyName(2, "");
            this.imageList2.Images.SetKeyName(3, "");
            this.imageList2.Images.SetKeyName(4, "");
            this.imageList2.Images.SetKeyName(5, "");
            this.imageList2.Images.SetKeyName(6, "");
            this.imageList2.Images.SetKeyName(7, "");
            this.imageList2.Images.SetKeyName(8, "");
            this.imageList2.Images.SetKeyName(9, "");
            this.imageList2.Images.SetKeyName(10, "");
            this.imageList2.Images.SetKeyName(11, "");
            this.imageList2.Images.SetKeyName(12, "");
            this.imageList2.Images.SetKeyName(13, "");
            this.imageList2.Images.SetKeyName(14, "");
            this.imageList2.Images.SetKeyName(15, "");
            this.imageList1.ImageStream = (ImageListStreamer)manager.GetObject("imageList1.ImageStream");
            this.imageList1.TransparentColor = Color.White;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "");
            this.imageList1.Images.SetKeyName(6, "");
            this.imageList1.Images.SetKeyName(7, "");
            this.imageList1.Images.SetKeyName(8, "");
            this.imageList1.Images.SetKeyName(9, "");
            this.imageList1.Images.SetKeyName(10, "");
            this.imageList1.Images.SetKeyName(11, "");
            this.imageList1.Images.SetKeyName(12, "");
            this.imageList1.Images.SetKeyName(13, "");
            this.imageList1.Images.SetKeyName(14, "");
            this.imageList1.Images.SetKeyName(15, "");
            this.imageList1.Images.SetKeyName(0x10, "");
            this.imageButtons.ImageStream = (ImageListStreamer)manager.GetObject("imageButtons.ImageStream");
            this.imageButtons.TransparentColor = Color.White;
            this.imageButtons.Images.SetKeyName(0, "");
            this.imageButtons.Images.SetKeyName(1, "");
            this.imageButtons.Images.SetKeyName(2, "");
            this.imageButtons.Images.SetKeyName(3, "");
            this.imageButtons.Images.SetKeyName(4, "");
            this.imageButtons.Images.SetKeyName(5, "");
            this.pictureBoxButton.BackColor = Color.Transparent;
            this.pictureBoxButton.Dock = DockStyle.Top;
            this.pictureBoxButton.Location = new Point(0, 0);
            this.pictureBoxButton.Name = "pictureBoxButton";
            this.pictureBoxButton.Size = new Size(0x200, 0x24);
            this.pictureBoxButton.TabIndex = 0x62;
            this.pictureBoxButton.TabStop = false;
            this.pictureBoxButton.Paint += new PaintEventHandler(this.pictureBoxButton_Paint);
            this.cbUTC.Appearance = Appearance.Button;
            this.cbUTC.BackColor = Color.Transparent;
            this.cbUTC.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.cbUTC.ImageAlign = ContentAlignment.MiddleLeft;
            this.cbUTC.ImageIndex = 0x10;
            this.cbUTC.ImageList = this.imageList1;
            this.cbUTC.Location = new Point(0, -1);
            this.cbUTC.Name = "cbUTC";
            this.cbUTC.Size = new Size(80, 0x24);
            this.cbUTC.TabIndex = 12;
            this.cbUTC.TabStop = false;
            this.cbUTC.Text = "&UTC";
            this.cbUTC.TextAlign = ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbUTC, "Switch to UTC/local times");
            this.cbUTC.UseVisualStyleBackColor = false;
            this.cbUTC.CheckedChanged += new EventHandler(this.cbUTC_CheckedChanged);
            this.btnClose.BackColor = Color.Transparent;

            this.txtUserValue1 = new System.Windows.Forms.TextBox();
            this.txtUserValue2 = new System.Windows.Forms.TextBox();
            this.txtUserValue3 = new System.Windows.Forms.TextBox();
            
            this.btnClose.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnClose.ImageAlign = ContentAlignment.MiddleLeft;
            this.btnClose.ImageIndex = 0;
            this.btnClose.ImageList = this.imageButtons;
            this.btnClose.Location = new Point(320, -1);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new Size(80, 0x24);
            this.btnClose.TabIndex = 0x10;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "&Close";
            this.toolTip1.SetToolTip(this.btnClose, "Close the view window");
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new EventHandler(this.btnClose_Click);
            this.btnNew.BackColor = Color.Transparent;
            this.btnNew.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnNew.ImageAlign = ContentAlignment.MiddleLeft;
            this.btnNew.ImageIndex = 3;
            this.btnNew.ImageList = this.imageButtons;
            this.btnNew.Location = new Point(240, -1);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new Size(80, 0x24);
            this.btnNew.TabIndex = 15;
            this.btnNew.TabStop = false;
            this.btnNew.Text = "&New";
            this.toolTip1.SetToolTip(this.btnNew, "Define a new current view");
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new EventHandler(this.btnNew_Click);
            this.btnApply.BackColor = Color.Transparent;
            
            
            
            this.btnApply.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnApply.ImageAlign = ContentAlignment.MiddleLeft;
            this.btnApply.ImageIndex = 1;
            this.btnApply.ImageList = this.imageButtons;
            this.btnApply.Location = new Point(160, -1);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new Size(80, 0x24);
            this.btnApply.TabIndex = 14;
            this.btnApply.TabStop = false;
            this.btnApply.Text = "&Apply";
            this.toolTip1.SetToolTip(this.btnApply, "Apply the current view definition and load the data");
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new EventHandler(this.btnApply_Click);
            this.btnSave.BackColor = Color.Transparent;
            
          
            
        
            
            
            
            this.btnSave.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnSave.ImageAlign = ContentAlignment.MiddleLeft;
            this.btnSave.ImageIndex = 5;
            this.btnSave.ImageList = this.imageButtons;
            this.btnSave.Location = new Point(80, -1);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new Size(80, 0x24);
            this.btnSave.TabIndex = 13;
            this.btnSave.TabStop = false;
            this.btnSave.Text = "&Save";
            this.toolTip1.SetToolTip(this.btnSave, "Save the current view definition");
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new EventHandler(this.btnSave_Click);
            this.txtACTUrno.Location = new Point(0x188, 80);
            this.txtACTUrno.Name = "txtACTUrno";
            this.txtACTUrno.Size = new Size(80, 20);
            this.txtACTUrno.TabIndex = 11;
            this.txtACTUrno.TabStop = false;
            this.txtACTUrno.Tag = "ACTU";
            this.txtACTUrno.Visible = false;
            this.txtACTUrno.TextChanged += new EventHandler(this.txtACTUrno_TextChanged);
            this.cbRunway.Appearance = Appearance.Button;
            this.cbRunway.BackColor = Color.Transparent;
            this.cbRunway.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.cbRunway.ImageAlign = ContentAlignment.MiddleRight;
            this.cbRunway.ImageIndex = 2;
            this.cbRunway.Location = new Point(400, 0x10);
            this.cbRunway.Name = "cbRunway";
            this.cbRunway.Size = new Size(80, 0x18);
            this.cbRunway.TabIndex = 0xb0;
            this.cbRunway.TabStop = false;
            this.cbRunway.Text = "&Runway";
            this.cbRunway.TextAlign = ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbRunway, "Select/deselect the Check-in source");
            this.cbRunway.UseVisualStyleBackColor = false;
            this.cbRunway.CheckedChanged += new EventHandler(this.cbRunway_CheckedChanged);
            this.cbExit.Appearance = Appearance.Button;
            this.cbExit.BackColor = Color.Transparent;
            this.cbExit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.cbExit.ImageAlign = ContentAlignment.MiddleRight;
            this.cbExit.ImageIndex = 2;
            this.cbExit.Location = new Point(0x100, 0x10);
            this.cbExit.Name = "cbExit";
            this.cbExit.Size = new Size(0x40, 0x18);
            this.cbExit.TabIndex = 0xaf;
            this.cbExit.TabStop = false;
            this.cbExit.Text = "&Exit";
            this.cbExit.TextAlign = ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbExit, "Select/deselect the Lounge source");
            this.cbExit.UseVisualStyleBackColor = false;
            this.cbExit.CheckedChanged += new EventHandler(this.cbExit_CheckedChanged);
            this.cbPosition.Appearance = Appearance.Button;
            this.cbPosition.BackColor = Color.Transparent;
            this.cbPosition.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.cbPosition.ImageAlign = ContentAlignment.MiddleRight;
            this.cbPosition.ImageIndex = 2;
            this.cbPosition.Location = new Point(0, 0x10);
            this.cbPosition.Name = "cbPosition";
            this.cbPosition.Size = new Size(0x40, 0x18);
            this.cbPosition.TabIndex = 170;
            this.cbPosition.TabStop = false;
            this.cbPosition.Text = "&Position";
            this.cbPosition.TextAlign = ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbPosition, "Select/deselect the position source");
            this.cbPosition.UseVisualStyleBackColor = false;
            this.cbPosition.CheckedChanged += new EventHandler(this.cbPosition_CheckedChanged);
            this.cbCheckin.Appearance = Appearance.Button;
            this.cbCheckin.BackColor = Color.Transparent;
            this.cbCheckin.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.cbCheckin.ImageAlign = ContentAlignment.MiddleRight;
            this.cbCheckin.ImageIndex = 2;
            this.cbCheckin.Location = new Point(320, 0x10);
            this.cbCheckin.Name = "cbCheckin";
            this.cbCheckin.Size = new Size(80, 0x18);
            this.cbCheckin.TabIndex = 0xae;
            this.cbCheckin.TabStop = false;
            this.cbCheckin.Text = "&Check-In";
            this.cbCheckin.TextAlign = ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbCheckin, "Select/deselect the Check-in source");
            this.cbCheckin.UseVisualStyleBackColor = false;
            this.cbCheckin.CheckedChanged += new EventHandler(this.cbCheckin_CheckedChanged);
            this.cbLounge.Appearance = Appearance.Button;
            this.cbLounge.BackColor = Color.Transparent;
            this.cbLounge.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.cbLounge.ImageAlign = ContentAlignment.MiddleRight;
            this.cbLounge.ImageIndex = 2;
            this.cbLounge.Location = new Point(0xc0, 0x10);
            this.cbLounge.Name = "cbLounge";
            this.cbLounge.Size = new Size(0x40, 0x18);
            this.cbLounge.TabIndex = 0xad;
            this.cbLounge.TabStop = false;
            this.cbLounge.Text = "&Lounge";
            this.cbLounge.TextAlign = ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbLounge, "Select/deselect the Lounge source");
            this.cbLounge.UseVisualStyleBackColor = false;
            this.cbLounge.CheckedChanged += new EventHandler(this.cbLounge_CheckedChanged);
            this.cbBelt.Appearance = Appearance.Button;
            this.cbBelt.BackColor = Color.Transparent;
            this.cbBelt.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.cbBelt.ImageAlign = ContentAlignment.MiddleRight;
            this.cbBelt.ImageIndex = 2;
            this.cbBelt.Location = new Point(0x80, 0x10);
            this.cbBelt.Name = "cbBelt";
            this.cbBelt.Size = new Size(0x40, 0x18);
            this.cbBelt.TabIndex = 0xac;
            this.cbBelt.TabStop = false;
            this.cbBelt.Text = "&Baggage";
            this.cbBelt.TextAlign = ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbBelt, "Select/deselect the Baggage source");
            this.cbBelt.UseVisualStyleBackColor = false;
            this.cbBelt.CheckedChanged += new EventHandler(this.cbBelt_CheckedChanged);
            this.cbGate.Appearance = Appearance.Button;
            this.cbGate.BackColor = Color.Transparent;
            this.cbGate.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.cbGate.ImageAlign = ContentAlignment.MiddleRight;
            this.cbGate.ImageIndex = 2;
            this.cbGate.Location = new Point(0x40, 0x10);
            this.cbGate.Name = "cbGate";
            this.cbGate.Size = new Size(0x40, 0x18);
            this.cbGate.TabIndex = 0xab;
            this.cbGate.TabStop = false;
            this.cbGate.Text = "&Gate";
            this.cbGate.TextAlign = ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbGate, "Select/deselect the Gate source");
            this.cbGate.UseVisualStyleBackColor = false;
            this.cbGate.CheckedChanged += new EventHandler(this.cbGate_CheckedChanged);
            this.btnNow.FlatStyle = FlatStyle.Popup;
            this.btnNow.ImageIndex = 4;
            this.btnNow.ImageList = this.imageList1;
            this.btnNow.Location = new Point(0x108, 0x58);
            this.btnNow.Name = "btnNow";
            this.btnNow.Size = new Size(16, 16);
            this.btnNow.TabIndex = 0xa7;
            this.btnNow.TabStop = false;
            this.toolTip1.SetToolTip(this.btnNow, "Change the time to now");
            this.btnNow.Click += new EventHandler(this.btnNow_Click);
            this.btnACR.FlatStyle = FlatStyle.Popup;
            this.btnACR.ImageIndex = 15;
            this.btnACR.ImageList = this.imageList2;
            this.btnACR.Location = new Point(0x100, 0x198);
            this.btnACR.Name = "btnACR";
            this.btnACR.Size = new Size(20, 20);
            this.btnACR.TabIndex = 0x9b;
            this.btnACR.TabStop = false;
            this.toolTip1.SetToolTip(this.btnACR, "Open look-up list for Registrations");
            this.btnACR.Click += new EventHandler(this.btnACR_Click);
            this.btnSTY.FlatStyle = FlatStyle.Popup;
            this.btnSTY.ImageIndex = 15;
            this.btnSTY.ImageList = this.imageList2;
            this.btnSTY.Location = new Point(0x100, 0x178);
            this.btnSTY.Name = "btnSTY";
            this.btnSTY.Size = new Size(20, 20);
            this.btnSTY.TabIndex = 0x9a;
            this.btnSTY.TabStop = false;
            this.toolTip1.SetToolTip(this.btnSTY, "Open look-up list for Service types");
            this.btnSTY.Click += new EventHandler(this.btnSTY_Click);
            this.btnNAT.FlatStyle = FlatStyle.Popup;
            this.btnNAT.ImageIndex = 15;
            this.btnNAT.ImageList = this.imageList2;
            this.btnNAT.Location = new Point(0x100, 0x158);
            this.btnNAT.Name = "btnNAT";
            this.btnNAT.Size = new Size(20, 20);
            this.btnNAT.TabIndex = 0x99;
            this.btnNAT.TabStop = false;
            this.toolTip1.SetToolTip(this.btnNAT, "Open look-up list for Nature codes");
            this.btnNAT.Click += new EventHandler(this.btnNAT_Click);
            this.btnALC.FlatStyle = FlatStyle.Popup;
            this.btnALC.ImageIndex = 15;
            this.btnALC.ImageList = this.imageList2;
            this.btnALC.Location = new Point(0x100, 0xf8);
            this.btnALC.Name = "btnALC";
            this.btnALC.Size = new Size(20, 20);
            this.btnALC.TabIndex = 0x97;
            this.btnALC.TabStop = false;
            this.toolTip1.SetToolTip(this.btnALC, "Open look-up list for Airlines");
            this.btnALC.Click += new EventHandler(this.btnALC_Click);
            this.btnACT.FlatStyle = FlatStyle.Popup;
            this.btnACT.ImageIndex = 15;
            this.btnACT.ImageList = this.imageList2;
            this.btnACT.Location = new Point(0x100, 0x138);
            this.btnACT.Name = "btnACT";
            this.btnACT.Size = new Size(20, 20);
            this.btnACT.TabIndex = 0x98;
            this.btnACT.TabStop = false;
            this.toolTip1.SetToolTip(this.btnACT, "Open look-up list for A/C Types");
            this.btnACT.Click += new EventHandler(this.btnACT_Click);
            this.button1.FlatStyle = FlatStyle.Popup;
            this.button1.ImageIndex = 15;
            this.button1.ImageList = this.imageList2;
            this.button1.Location = new Point(0xf8, 0xe8);
            this.button1.Name = "button1";
            this.button1.Size = new Size(20, 20);
            this.button1.TabIndex = 0x9a;
            this.button1.TabStop = false;
            this.toolTip1.SetToolTip(this.button1, "Open look-up list for Service types");
            this.button2.FlatStyle = FlatStyle.Popup;
            this.button2.ImageIndex = 15;
            this.button2.ImageList = this.imageList2;
            this.button2.Location = new Point(0xf8, 0x108);
            this.button2.Name = "button2";
            this.button2.Size = new Size(20, 20);
            this.button2.TabIndex = 0x9b;
            this.button2.TabStop = false;
            this.toolTip1.SetToolTip(this.button2, "Open look-up list for Registrations");
            this.button3.FlatStyle = FlatStyle.Popup;
            this.button3.ImageIndex = 15;
            this.button3.ImageList = this.imageList2;
            this.button3.Location = new Point(240, 320);
            this.button3.Name = "button3";
            this.button3.Size = new Size(20, 20);
            this.button3.TabIndex = 0x9a;
            this.button3.TabStop = false;
            this.toolTip1.SetToolTip(this.button3, "Open look-up list for Service types");
            this.button4.FlatStyle = FlatStyle.Popup;
            this.button4.ImageIndex = 15;
            this.button4.ImageList = this.imageList2;
            this.button4.Location = new Point(240, 0x160);
            this.button4.Name = "button4";
            this.button4.Size = new Size(20, 20);
            this.button4.TabIndex = 0x9b;
            this.button4.TabStop = false;
            this.toolTip1.SetToolTip(this.button4, "Open look-up list for Registrations");
            this.button5.FlatStyle = FlatStyle.Popup;
            this.button5.ImageIndex = 15;
            this.button5.ImageList = this.imageList2;
            this.button5.Location = new Point(0xf8, 0x120);
            this.button5.Name = "button5";
            this.button5.Size = new Size(20, 20);
            this.button5.TabIndex = 0x99;
            this.button5.TabStop = false;
            this.toolTip1.SetToolTip(this.button5, "Open look-up list for Nature codes");
            this.button6.FlatStyle = FlatStyle.Popup;
            this.button6.ImageIndex = 15;
            this.button6.ImageList = this.imageList2;
            this.button6.Location = new Point(0xf8, 0x100);
            this.button6.Name = "button6";
            this.button6.Size = new Size(20, 20);
            this.button6.TabIndex = 0x98;
            this.button6.TabStop = false;
            this.toolTip1.SetToolTip(this.button6, "Open look-up list for A/C Types");
            this.button7.FlatStyle = FlatStyle.Popup;
            this.button7.ImageIndex = 4;
            this.button7.ImageList = this.imageList1;
            this.button7.Location = new Point(320, 0x40);
            this.button7.Name = "button7";
            this.button7.Size = new Size(20, 20);
            this.button7.TabIndex = 190;
            this.button7.TabStop = false;
            this.toolTip1.SetToolTip(this.button7, "Change the time to now");
            this.button7.Visible = false;
           
            this.FlightData.Controls.Add(this.txtUserValue1);
            this.FlightData.Controls.Add(this.lblUser);
            this.FlightData.Controls.Add(this.cbConflict);
            this.FlightData.Controls.Add(this.rbFilterConflict);
            this.FlightData.Controls.Add(this.cbTowing);
            this.FlightData.Controls.Add(this.rbDeletedFlights);
            this.FlightData.Controls.Add(this.rbChanged);
            this.FlightData.Controls.Add(this.rbFilterButtons);
            this.FlightData.Controls.Add(this.cbRunway);
            this.FlightData.Controls.Add(this.cbExit);
            this.FlightData.Controls.Add(this.label1);
            this.FlightData.Controls.Add(this.cbPosition);
            this.FlightData.Controls.Add(this.cbCheckin);
            this.FlightData.Controls.Add(this.cbLounge);
            this.FlightData.Controls.Add(this.cbBelt);
            this.FlightData.Controls.Add(this.cbGate);
            this.FlightData.Controls.Add(this.cbTakeOver);
            this.FlightData.Controls.Add(this.btnNow);
            this.FlightData.Controls.Add(this.dateTimePickerTo);
            this.FlightData.Controls.Add(this.lblTo);
            this.FlightData.Controls.Add(this.dateTimePickerFrom);
            this.FlightData.Controls.Add(this.lblFrom);
            this.FlightData.Controls.Add(this.cbArrival);
            this.FlightData.Controls.Add(this.txtCallSignValue);
            this.FlightData.Controls.Add(this.txtACT5Value);
            this.FlightData.Controls.Add(this.txtALT3Value);
            this.FlightData.Controls.Add(this.txtFLNUValue);
            this.FlightData.Controls.Add(this.txtFLTSValue);
            this.FlightData.Controls.Add(this.txtREGNValue);
            this.FlightData.Controls.Add(this.txtALTValue);
            this.FlightData.Controls.Add(this.txtSTYValue);
            this.FlightData.Controls.Add(this.txtNATValue);
            this.FlightData.Controls.Add(this.txtACTValue);
            this.FlightData.Controls.Add(this.lblCallSign);
            this.FlightData.Controls.Add(this.cbDeparture);
            this.FlightData.Controls.Add(this.btnACR);
            this.FlightData.Controls.Add(this.btnSTY);
            this.FlightData.Controls.Add(this.btnNAT);
            this.FlightData.Controls.Add(this.btnALC);
            this.FlightData.Controls.Add(this.lblREGN);
            this.FlightData.Controls.Add(this.lblSTY);
            this.FlightData.Controls.Add(this.lblNAT);
            this.FlightData.Controls.Add(this.lblFLNU);
            this.FlightData.Controls.Add(this.lblALC);
            this.FlightData.Controls.Add(this.btnACT);
            this.FlightData.Controls.Add(this.lblACT);
            this.FlightData.Controls.Add(this.pictureBox1);
            this.FlightData.Controls.Add(this.textBox1);
            this.FlightData.Controls.Add(this.label2);
            this.FlightData.Controls.Add(this.label3);
            this.FlightData.Controls.Add(this.radioButton1);
            this.FlightData.Controls.Add(this.radioButton2);
            this.FlightData.Controls.Add(this.label4);
            this.FlightData.Controls.Add(this.label5);
            this.FlightData.Controls.Add(this.label6);
            this.FlightData.Controls.Add(this.button1);
            this.FlightData.Controls.Add(this.label7);
            this.FlightData.Controls.Add(this.label8);
            this.FlightData.Controls.Add(this.button2);
            this.FlightData.Controls.Add(this.button3);
            this.FlightData.Controls.Add(this.label9);
            this.FlightData.Controls.Add(this.label10);
            this.FlightData.Controls.Add(this.button4);
            this.FlightData.Controls.Add(this.pictureBox2);
            this.FlightData.Controls.Add(this.label11);
            this.FlightData.Controls.Add(this.textBox2);
            this.FlightData.Controls.Add(this.textBox3);
            this.FlightData.Controls.Add(this.textBox4);
            this.FlightData.Controls.Add(this.button5);
            this.FlightData.Controls.Add(this.radioButton3);
            this.FlightData.Controls.Add(this.textBox5);
            this.FlightData.Controls.Add(this.label12);
            this.FlightData.Controls.Add(this.label13);
            this.FlightData.Controls.Add(this.radioButton4);
            this.FlightData.Controls.Add(this.label14);
            this.FlightData.Controls.Add(this.label15);
            this.FlightData.Controls.Add(this.button6);

            this.lblUser.BackColor = System.Drawing.Color.Transparent;
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(32, 442);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(56, 16);
            this.lblUser.TabIndex = 185;
            this.lblUser.Text = "User ID:";

            this.txtUserValue1.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserValue1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUserValue1.Location = new System.Drawing.Point(120, 440);
            this.txtUserValue1.Name = "txtUserValue1";
            this.txtUserValue1.Size = new System.Drawing.Size(128, 20);
            this.txtUserValue1.TabIndex = 184;
            this.txtUserValue1.Tag = "USER_VAL";
            this.txtUserValue1.Text = "ALL USER";


            this.txtUserValue2.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserValue2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUserValue2.Location = new System.Drawing.Point(177, 88);
            this.txtUserValue2.Name = "txtUserValue2";
            this.txtUserValue2.Size = new System.Drawing.Size(126, 20);
            this.txtUserValue2.TabIndex = 195;
            this.txtUserValue2.Tag = "USER_VAL";
            this.txtUserValue2.Text = "ALL USER";


            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(40, 89);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(62, 16);
            this.label24.TabIndex = 196;
            this.label24.Text = "User ID:";
            
            this.FlightData.Location = new Point(4, 0x16);
            this.FlightData.Name = "FlightData";
            this.FlightData.Size = new Size(0x1e8, 0x1be);
            this.FlightData.TabIndex = 0;
            this.FlightData.Text = "Flight Data";
            this.FlightData.UseVisualStyleBackColor = true;
            this.rbFilterConflict.Location = new Point(120, 0xb9);
            this.rbFilterConflict.Name = "rbFilterConflict";
            this.rbFilterConflict.Size = new Size(0x68, 0x18);
            this.rbFilterConflict.TabIndex = 0xb5;
            this.rbFilterConflict.Text = "Conflict Flights";
            this.rbFilterConflict.CheckedChanged += new EventHandler(this.rbFilterConflict_CheckedChanged);
            this.cbTowing.Location = new Point(0x100, 0x89);
            this.cbTowing.Name = "cbTowing";
            this.cbTowing.Size = new Size(0x68, 0x18);
            this.cbTowing.TabIndex = 180;
            this.cbTowing.Text = "Towing";
            this.rbDeletedFlights.Location = new Point(120, 0x70);
            this.rbDeletedFlights.Name = "rbDeletedFlights";
            this.rbDeletedFlights.Size = new Size(0x68, 0x18);
            this.rbDeletedFlights.TabIndex = 0xb3;
            this.rbDeletedFlights.Text = "Deleted Flights";
            this.rbDeletedFlights.CheckedChanged += new EventHandler(this.chDeleted_CheckedChanged);
            this.rbChanged.Location = new Point(120, 0x89);
            this.rbChanged.Name = "rbChanged";
            this.rbChanged.Size = new Size(0x88, 0x18);
            this.rbChanged.TabIndex = 0xb2;
            this.rbChanged.Text = "All Changes for ...";
            this.rbChanged.CheckedChanged += new EventHandler(this.Check_chDeleted);
            this.rbFilterButtons.Location = new Point(120, 0xa1);
            this.rbFilterButtons.Name = "rbFilterButtons";
            this.rbFilterButtons.Size = new Size(120, 0x18);
            this.rbFilterButtons.TabIndex = 0xb1;
            this.rbFilterButtons.Text = "Location Changes";
            this.rbFilterButtons.CheckedChanged += new EventHandler(this.rbFilterButtons_CheckedChanged);
            this.label1.BackColor = SystemColors.ControlText;
            this.label1.Dock = DockStyle.Top;
            this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label1.ForeColor = Color.Lime;
            this.label1.Location = new Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x1e8, 14);
            this.label1.TabIndex = 0xa9;
            this.label1.Text = "Include Location Changes for :";
            this.label1.TextAlign = ContentAlignment.MiddleCenter;
            this.label1.Click += new EventHandler(this.label1_Click);
            this.cbTakeOver.Location = new Point(0x108, 0x38);
            this.cbTakeOver.Name = "cbTakeOver";
            this.cbTakeOver.Size = new Size(0x68, 0x20);
            this.cbTakeOver.TabIndex = 0xa8;
            this.cbTakeOver.Text = "Take over to changes";
            this.dateTimePickerTo.Format = DateTimePickerFormat.Custom;
            this.dateTimePickerTo.Location = new Point(120, 0x56);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new Size(0x80, 20);
            this.dateTimePickerTo.TabIndex = 0xa4;
            this.dateTimePickerTo.Value = new DateTime(0x7da, 12, 0x16, 0, 0, 0, 0);
            this.lblTo.BackColor = Color.Transparent;
            this.lblTo.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblTo.Location = new Point(0x20, 0x58);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new Size(0x40, 0x10);
            this.lblTo.TabIndex = 0xa6;
            this.lblTo.Text = "Flights To:";
            this.dateTimePickerFrom.Format = DateTimePickerFormat.Custom;
            this.dateTimePickerFrom.Location = new Point(120, 0x3e);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new Size(0x80, 20);
            this.dateTimePickerFrom.TabIndex = 0xa3;
            this.dateTimePickerFrom.Value = new DateTime(0x7da, 12, 0x16, 0, 0, 0, 0);
            this.lblFrom.BackColor = Color.Transparent;
            this.lblFrom.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblFrom.Location = new Point(0x20, 0x40);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new Size(80, 0x10);
            this.lblFrom.TabIndex = 0xa5;
            this.lblFrom.Text = "Flights From:";
            this.cbArrival.Location = new Point(0x100, 0x76);
            this.cbArrival.Name = "cbArrival";
            this.cbArrival.Size = new Size(0x68, 0x18);
            this.cbArrival.TabIndex = 160;
            this.cbArrival.Text = "Arrival";
            this.txtCallSignValue.CharacterCasing = CharacterCasing.Upper;
            this.txtCallSignValue.Location = new Point(120, 280);
            this.txtCallSignValue.Name = "txtCallSignValue";
            this.txtCallSignValue.Size = new Size(0x80, 20);
            this.txtCallSignValue.TabIndex = 0x9e;
            this.txtCallSignValue.Tag = "CSGNU_VAL";
            this.txtACT5Value.CharacterCasing = CharacterCasing.Upper;
            this.txtACT5Value.Location = new Point(0xb8, 0x138);
            this.txtACT5Value.Name = "txtACT5Value";
            this.txtACT5Value.Size = new Size(0x40, 20);
            this.txtACT5Value.TabIndex = 0x8d;
            this.txtACT5Value.Tag = "ACT5U_VAL";
            this.txtACT5Value.Leave += new EventHandler(this.txtACT5Value_Leave);
            this.txtALT3Value.CharacterCasing = CharacterCasing.Upper;
            this.txtALT3Value.Location = new Point(0xb8, 0xf8);
            this.txtALT3Value.Name = "txtALT3Value";
            this.txtALT3Value.Size = new Size(0x40, 20);
            this.txtALT3Value.TabIndex = 0x8b;
            this.txtALT3Value.Tag = "ALT3U_VAL";
            this.txtALT3Value.Leave += new EventHandler(this.txtALT3Value_Leave);
            this.txtFLNUValue.CharacterCasing = CharacterCasing.Upper;
            this.txtFLNUValue.Location = new Point(120, 0xd8);
            this.txtFLNUValue.MaxLength = 5;
            this.txtFLNUValue.Name = "txtFLNUValue";
            this.txtFLNUValue.Size = new Size(0x58, 20);
            this.txtFLNUValue.TabIndex = 0x88;
            this.txtFLNUValue.Tag = "FLNU_VAL";
            this.txtFLNUValue.Leave += new EventHandler(this.txtFLNUValue_TextChanged);
            this.txtFLTSValue.CharacterCasing = CharacterCasing.Upper;
            this.txtFLTSValue.Location = new Point(0xe0, 0xd8);
            this.txtFLTSValue.Name = "txtFLTSValue";
            this.txtFLTSValue.Size = new Size(0x18, 20);
            this.txtFLTSValue.TabIndex = 0x89;
            this.txtFLTSValue.Tag = "FLTS_VAL";
            this.txtREGNValue.CharacterCasing = CharacterCasing.Upper;
            this.txtREGNValue.Location = new Point(120, 0x198);
            this.txtREGNValue.Name = "txtREGNValue";
            this.txtREGNValue.Size = new Size(0x80, 20);
            this.txtREGNValue.TabIndex = 0x90;
            this.txtREGNValue.Tag = "REGNU_VAL";
            this.txtREGNValue.Leave += new EventHandler(this.txtREGNValue_Leave);
            this.txtALTValue.CharacterCasing = CharacterCasing.Upper;
            this.txtALTValue.Location = new Point(120, 0xf8);
            this.txtALTValue.Name = "txtALTValue";
            this.txtALTValue.Size = new Size(0x40, 20);
            this.txtALTValue.TabIndex = 0x8a;
            this.txtALTValue.Tag = "ALTU_VAL";
            this.txtALTValue.Leave += new EventHandler(this.txtALTValue_Leave);
            this.txtSTYValue.CharacterCasing = CharacterCasing.Upper;
            this.txtSTYValue.Location = new Point(120, 0x178);
            this.txtSTYValue.Name = "txtSTYValue";
            this.txtSTYValue.Size = new Size(0x80, 20);
            this.txtSTYValue.TabIndex = 0x8f;
            this.txtSTYValue.Tag = "STYU_VAL";
            this.txtSTYValue.Leave += new EventHandler(this.txtSTYValue_Leave);
            this.txtNATValue.CharacterCasing = CharacterCasing.Upper;
            this.txtNATValue.Location = new Point(120, 0x158);
            this.txtNATValue.Name = "txtNATValue";
            this.txtNATValue.Size = new Size(0x80, 20);
            this.txtNATValue.TabIndex = 0x8e;
            this.txtNATValue.Tag = "NATU_VAL";
            this.txtNATValue.Leave += new EventHandler(this.txtNATValue_Leave);
            this.txtACTValue.CharacterCasing = CharacterCasing.Upper;
            this.txtACTValue.Location = new Point(120, 0x138);
            this.txtACTValue.Name = "txtACTValue";
            this.txtACTValue.Size = new Size(0x40, 20);
            this.txtACTValue.TabIndex = 140;
            this.txtACTValue.Tag = "ACTU_VAL";
            this.txtACTValue.Leave += new EventHandler(this.txtACTValue_Leave);
            this.lblCallSign.BackColor = Color.Transparent;
            this.lblCallSign.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblCallSign.Location = new Point(0x20, 280);
            this.lblCallSign.Name = "lblCallSign";
            this.lblCallSign.Size = new Size(0x48, 0x10);
            this.lblCallSign.TabIndex = 0x9f;
            this.lblCallSign.Text = "Call Sign:";
            this.cbDeparture.Location = new Point(0x100, 0x9d);
            this.cbDeparture.Name = "cbDeparture";
            this.cbDeparture.Size = new Size(0x68, 0x18);
            this.cbDeparture.TabIndex = 0x9d;
            this.cbDeparture.Text = "Departure";
            this.lblREGN.BackColor = Color.Transparent;
            this.lblREGN.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblREGN.Location = new Point(0x20, 0x198);
            this.lblREGN.Name = "lblREGN";
            this.lblREGN.Size = new Size(0x48, 0x10);
            this.lblREGN.TabIndex = 150;
            this.lblREGN.Text = "Registration:";
            this.lblSTY.BackColor = Color.Transparent;
            this.lblSTY.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblSTY.Location = new Point(0x20, 0x178);
            this.lblSTY.Name = "lblSTY";
            this.lblSTY.Size = new Size(0x48, 0x10);
            this.lblSTY.TabIndex = 0x95;
            this.lblSTY.Text = "Servie Type:";
            this.lblNAT.BackColor = Color.Transparent;
            this.lblNAT.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblNAT.Location = new Point(0x20, 0x158);
            this.lblNAT.Name = "lblNAT";
            this.lblNAT.Size = new Size(80, 0x10);
            this.lblNAT.TabIndex = 0x94;
            this.lblNAT.Text = "Nature Code:";
            this.lblFLNU.BackColor = Color.Transparent;
            this.lblFLNU.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblFLNU.Location = new Point(0x20, 0xda);
            this.lblFLNU.Name = "lblFLNU";
            this.lblFLNU.Size = new Size(80, 0x10);
            this.lblFLNU.TabIndex = 0x91;
            this.lblFLNU.Text = "Flightnumber:";
            this.lblALC.BackColor = Color.Transparent;
            this.lblALC.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblALC.Location = new Point(0x20, 0xfa);
            this.lblALC.Name = "lblALC";
            this.lblALC.Size = new Size(0x40, 0x10);
            this.lblALC.TabIndex = 0x92;
            this.lblALC.Text = "Airline:";
            this.lblACT.BackColor = Color.Transparent;
            this.lblACT.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblACT.Location = new Point(0x20, 0x138);
            this.lblACT.Name = "lblACT";
            this.lblACT.Size = new Size(80, 0x10);
            this.lblACT.TabIndex = 0x93;
            this.lblACT.Text = "Aircraft Type:";
            this.pictureBox1.BackColor = Color.Transparent;
            this.pictureBox1.Dock = DockStyle.Fill;
            this.pictureBox1.Location = new Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Size(0x1e8, 0x1be);
            this.pictureBox1.TabIndex = 0x9c;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new PaintEventHandler(this.pictureBox1_Paint);
            this.textBox1.CharacterCasing = CharacterCasing.Upper;
            this.textBox1.Location = new Point(120, 0x108);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Size(0x80, 20);
            this.textBox1.TabIndex = 0x90;
            this.textBox1.Tag = "REGNU_VAL";
            this.label2.BackColor = Color.Transparent;
            this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label2.Location = new Point(0x18, 0x108);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x48, 0x10);
            this.label2.TabIndex = 150;
            this.label2.Text = "Registration:";
            this.label3.BackColor = Color.Transparent;
            this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label3.Location = new Point(0x18, 0x8a);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x48, 0x10);
            this.label3.TabIndex = 0x9f;
            this.label3.Text = "Call Sign:";
            this.radioButton1.Location = new Point(0x70, 42);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new Size(120, 0x18);
            this.radioButton1.TabIndex = 0xa2;
            this.radioButton1.Text = "All Changes for ...";
            this.radioButton2.Location = new Point(0x70, 0x13);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new Size(0x68, 0x18);
            this.radioButton2.TabIndex = 0xa1;
            this.radioButton2.Text = "Deleted Flights";
            this.label4.BackColor = Color.Transparent;
            this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label4.Location = new Point(0x18, 0xe8);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x48, 0x10);
            this.label4.TabIndex = 0x95;
            this.label4.Text = "Servie Type:";
            this.label5.BackColor = Color.Transparent;
            this.label5.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label5.Location = new Point(0x18, 200);
            this.label5.Name = "label5";
            this.label5.Size = new Size(80, 0x10);
            this.label5.TabIndex = 0x94;
            this.label5.Text = "Nature Code:";
            this.label6.BackColor = Color.Transparent;
            this.label6.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label6.Location = new Point(0x18, 0x48);
            this.label6.Name = "label6";
            this.label6.Size = new Size(80, 0x10);
            this.label6.TabIndex = 0x91;
            this.label6.Text = "Flightnumber:";
            this.label7.BackColor = Color.Transparent;
            this.label7.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label7.Location = new Point(0x18, 0x68);
            this.label7.Name = "label7";
            this.label7.Size = new Size(0x40, 0x10);
            this.label7.TabIndex = 0x92;
            this.label7.Text = "Airline:";
            this.label8.BackColor = Color.Transparent;
            this.label8.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label8.Location = new Point(0x18, 0xa8);
            this.label8.Name = "label8";
            this.label8.Size = new Size(80, 0x10);
            this.label8.TabIndex = 0x93;
            this.label8.Text = "Aircraft Type:";
            this.label9.BackColor = Color.Transparent;
            this.label9.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label9.Location = new Point(0x10, 0xc2);
            this.label9.Name = "label9";
            this.label9.Size = new Size(0x40, 0x10);
            this.label9.TabIndex = 0x92;
            this.label9.Text = "Airline:";
            this.label10.BackColor = Color.Transparent;
            this.label10.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label10.Location = new Point(0x10, 0x100);
            this.label10.Name = "label10";
            this.label10.Size = new Size(80, 0x10);
            this.label10.TabIndex = 0x93;
            this.label10.Text = "Aircraft Type:";
            this.pictureBox2.BackColor = Color.Transparent;
            this.pictureBox2.Dock = DockStyle.Fill;
            this.pictureBox2.Location = new Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Size(0x1e8, 0x1be);
            this.pictureBox2.TabIndex = 0x9c;
            this.pictureBox2.TabStop = false;
            this.label11.BackColor = Color.Transparent;
            this.label11.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label11.Location = new Point(0x10, 160);
            this.label11.Name = "label11";
            this.label11.Size = new Size(80, 0x10);
            this.label11.TabIndex = 0x91;
            this.label11.Text = "Flightnumber:";
            this.textBox2.CharacterCasing = CharacterCasing.Upper;
            this.textBox2.Location = new Point(120, 320);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Size(0x80, 20);
            this.textBox2.TabIndex = 0x8f;
            this.textBox2.Tag = "STYU_VAL";
            this.textBox3.CharacterCasing = CharacterCasing.Upper;
            this.textBox3.Location = new Point(120, 0x120);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Size(0x80, 20);
            this.textBox3.TabIndex = 0x8e;
            this.textBox3.Tag = "NATU_VAL";
            this.textBox4.CharacterCasing = CharacterCasing.Upper;
            this.textBox4.Location = new Point(0xb8, 0x100);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Size(0x40, 20);
            this.textBox4.TabIndex = 0x8d;
            this.textBox4.Tag = "ACT5U_VAL";
            this.radioButton3.Location = new Point(0x68, 0x68);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new Size(0x68, 0x18);
            this.radioButton3.TabIndex = 0xa1;
            this.radioButton3.Text = "Deleted Flights";
            this.textBox5.CharacterCasing = CharacterCasing.Upper;
            this.textBox5.Location = new Point(120, 0x160);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Size(0x80, 20);
            this.textBox5.TabIndex = 0x90;
            this.textBox5.Tag = "REGNU_VAL";
            this.label12.BackColor = Color.Transparent;
            this.label12.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label12.Location = new Point(0x10, 0x162);
            this.label12.Name = "label12";
            this.label12.Size = new Size(0x48, 0x10);
            this.label12.TabIndex = 150;
            this.label12.Text = "Registration:";
            this.label13.BackColor = Color.Transparent;
            this.label13.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label13.Location = new Point(0x10, 0xe2);
            this.label13.Name = "label13";
            this.label13.Size = new Size(0x48, 0x10);
            this.label13.TabIndex = 0x9f;
            this.label13.Text = "Call Sign:";
            this.radioButton4.Location = new Point(0x68, 0x81);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new Size(120, 0x18);
            this.radioButton4.TabIndex = 0xa2;
            this.radioButton4.Text = "All Changes for ...";
            this.label14.BackColor = Color.Transparent;
            this.label14.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label14.Location = new Point(0x10, 320);
            this.label14.Name = "label14";
            this.label14.Size = new Size(0x48, 0x10);
            this.label14.TabIndex = 0x95;
            this.label14.Text = "Servie Type:";
            this.label15.BackColor = Color.Transparent;
            this.label15.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label15.Location = new Point(0x10, 0x120);
            this.label15.Name = "label15";
            this.label15.Size = new Size(80, 0x10);
            this.label15.TabIndex = 0x94;
            this.label15.Text = "Nature Code:";



            this.BasicData.Controls.Add(this.label24);
            this.BasicData.Controls.Add(this.txtUserValue2);
            this.BasicData.Controls.Add(this.panel1);
            this.BasicData.Controls.Add(this.label18);
            this.BasicData.Controls.Add(this.cbTable);
            this.BasicData.Controls.Add(this.cbTakeOver_BD);
            this.BasicData.Controls.Add(this.button7);
            this.BasicData.Controls.Add(this.dateTimePickerTo_BD);
            this.BasicData.Controls.Add(this.label16);
            this.BasicData.Controls.Add(this.dateTimePickerFrm_BD);
            this.BasicData.Controls.Add(this.label17);
            this.BasicData.Controls.Add(this.pictureBox3);
            this.BasicData.Location = new Point(4, 0x16);
            this.BasicData.Name = "BasicData";
            this.BasicData.Size = new Size(0x1e8, 0x1be);
            this.BasicData.TabIndex = 1;
            this.BasicData.Text = "Basic Data";
            this.BasicData.UseVisualStyleBackColor = true;
            this.panel1.Location = new Point(8, 0x90);
            this.panel1.Name = "panel1";
            this.panel1.Size = new Size(0x1d0, 0x128);
            this.panel1.TabIndex = 0xc2;
            this.label18.BackColor = Color.Transparent;
            this.label18.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label18.Location = new Point(40, 0x72);
            this.label18.Name = "label18";
            this.label18.Size = new Size(0x48, 0x10);
            this.label18.TabIndex = 0xc1;
            this.label18.Text = "Table:";
            this.cbTable.ItemHeight = 13;
            this.cbTable.Location = new Point(0xb1, 0x70);
            this.cbTable.Name = "cbTable";
            this.cbTable.Size = new Size(0x80, 0x15);
            this.cbTable.TabIndex = 0xc0;
            this.cbTable.SelectedIndexChanged += new EventHandler(this.cbTable_SelectedIndexChanged);
            this.cbTable.SelectedValueChanged += new EventHandler(this.OnTableChange);
            this.cbTakeOver_BD.Checked = true;
            this.cbTakeOver_BD.CheckState = CheckState.Checked;
            this.cbTakeOver_BD.Location = new Point(320, 0x20);
            this.cbTakeOver_BD.Name = "cbTakeOver_BD";
            this.cbTakeOver_BD.Size = new Size(0x68, 0x20);
            this.cbTakeOver_BD.TabIndex = 0xbf;
            this.cbTakeOver_BD.Text = "Take over to changes";
            this.cbTakeOver_BD.Visible = false;
            this.dateTimePickerTo_BD.Format = DateTimePickerFormat.Custom;
            this.dateTimePickerTo_BD.Location = new Point(0xb1, 0x40);
            this.dateTimePickerTo_BD.Name = "dateTimePickerTo_BD";
            this.dateTimePickerTo_BD.Size = new Size(0x80, 20);
            this.dateTimePickerTo_BD.TabIndex = 0xbb;
            this.dateTimePickerTo_BD.Value = new DateTime(0x7da, 12, 0x16, 0x17, 0x3b, 0x3b, 0);

            this.label16.BackColor = Color.Transparent;
            this.label16.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label16.Location = new Point(40, 0x40);
            this.label16.Name = "label16";
            this.label16.Size = new Size(80, 0x10);
            this.label16.TabIndex = 0xbd;
            this.label16.Text = "Changes To:";
            this.dateTimePickerFrm_BD.Format = DateTimePickerFormat.Custom;
            this.dateTimePickerFrm_BD.Location = new Point(0xb1, 40);
            this.dateTimePickerFrm_BD.Name = "dateTimePickerFrm_BD";
            this.dateTimePickerFrm_BD.Size = new Size(0x80, 20);
            this.dateTimePickerFrm_BD.TabIndex = 0xba;
            this.dateTimePickerFrm_BD.Value = new DateTime(0x7da, 12, 0x16, 0, 0, 0, 0);


                  
            
            this.label17.BackColor = Color.Transparent;
            this.label17.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label17.Location = new Point(40, 40);
            this.label17.Name = "label17";
            this.label17.Size = new Size(0x58, 0x10);
            this.label17.TabIndex = 0xbc;
            this.label17.Text = "Changes From:";
            this.pictureBox3.BackColor = Color.Transparent;
            this.pictureBox3.Dock = DockStyle.Fill;
            this.pictureBox3.Location = new Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Size(0x1e8, 0x1be);
            this.pictureBox3.TabIndex = 0x9d;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Paint += new PaintEventHandler(this.pictureBox3_Paint);
            this.tabControl1.Controls.Add(this.FlightData);
            this.tabControl1.Controls.Add(this.BasicData);
            this.tabControl1.Controls.Add(this.CCAData);
            this.tabControl1.ItemSize = new Size(0x3f, 0x12);
            this.tabControl1.Location = new Point(8, 40);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new Size(0x1f0, 0x1d8);
            this.tabControl1.TabIndex = 0x73;
            this.tabControl1.SelectedIndexChanged += new EventHandler(this.TabPageChanged);

            this.CCAData.Controls.Add(this.label25);
            this.CCAData.Controls.Add(this.txtUserValue3);
            this.CCAData.Controls.Add(this.groupBox2);
            this.CCAData.Controls.Add(this.groupBox1);
            this.CCAData.Controls.Add(this.txtCounter);
            this.CCAData.Controls.Add(this.label21);
            this.CCAData.Controls.Add(this.pictureBox4);
            this.CCAData.Location = new Point(4, 0x16);
            this.CCAData.Name = "CCAData";
            this.CCAData.Size = new Size(0x1e8, 0x1be);
            this.CCAData.TabIndex = 2;
            this.CCAData.Text = "Common CKI";
            this.CCAData.UseVisualStyleBackColor = true;


            this.txtUserValue3.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserValue3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUserValue3.Location = new System.Drawing.Point(58, 223);
            this.txtUserValue3.Name = "txtUserValue3";
            this.txtUserValue3.Size = new System.Drawing.Size(126, 20);
            this.txtUserValue3.TabIndex = 197;
            this.txtUserValue3.Tag = "USER_VAL";
            this.txtUserValue3.Text = "ALL USER";

            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(9, 226);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(59, 16);
            this.label25.TabIndex = 198;
            this.label25.Text = "User ID:";

            this.groupBox2.BackColor = Color.Transparent;
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.dtpCloseTo);
            this.groupBox2.Controls.Add(this.dtpCloseFrom);
            this.groupBox2.Location = new Point(9, 0x5f);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new Size(470, 0x3f);
            this.groupBox2.TabIndex = 160;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Scheduled Close";
            this.label23.AutoSize = true;
            this.label23.Location = new Point(0xf8, 0x1b);
            this.label23.Name = "label23";
            this.label23.Size = new Size(20, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "To";
            this.label20.AutoSize = true;
            this.label20.Location = new Point(6, 0x1b);
            this.label20.Name = "label20";
            this.label20.Size = new Size(30, 13);
            this.label20.TabIndex = 4;
            this.label20.Text = "From";
            this.dtpCloseTo.Format = DateTimePickerFormat.Custom;
            this.dtpCloseTo.Location = new Point(0x10f, 0x18);
            this.dtpCloseTo.Name = "dtpCloseTo";
            this.dtpCloseTo.Size = new Size(0xb8, 20);
            this.dtpCloseTo.TabIndex = 6;
            this.dtpCloseFrom.Format = DateTimePickerFormat.Custom;
            this.dtpCloseFrom.Location = new Point(0x27, 0x18);
            this.dtpCloseFrom.Name = "dtpCloseFrom";
            this.dtpCloseFrom.Size = new Size(0xb8, 20);
            this.dtpCloseFrom.TabIndex = 5;
            this.groupBox1.BackColor = Color.Transparent;
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.dtpOpenTo);
            this.groupBox1.Controls.Add(this.dtpOpenFrom);
            this.groupBox1.Location = new Point(9, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(470, 0x43);
            this.groupBox1.TabIndex = 0x9f;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Scheduled Open";
            this.label22.AutoSize = true;
            this.label22.Location = new Point(0xf8, 0x20);
            this.label22.Name = "label22";
            this.label22.Size = new Size(20, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "To";
            this.label19.AutoSize = true;
            this.label19.Location = new Point(6, 0x1f);
            this.label19.Name = "label19";
            this.label19.Size = new Size(30, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "From";
            this.dtpOpenTo.Format = DateTimePickerFormat.Custom;
            this.dtpOpenTo.Location = new Point(0x10f, 0x1c);
            this.dtpOpenTo.Name = "dtpOpenTo";
            this.dtpOpenTo.Size = new Size(0xb8, 20);
            this.dtpOpenTo.TabIndex = 1;
            this.dtpOpenFrom.Format = DateTimePickerFormat.Custom;
            this.dtpOpenFrom.Location = new Point(0x27, 0x1c);
            this.dtpOpenFrom.Name = "dtpOpenFrom";
            this.dtpOpenFrom.Size = new Size(0xb8, 20);
            this.dtpOpenFrom.TabIndex = 0;
            this.txtCounter.Location = new Point(0x3a, 180);
            this.txtCounter.Name = "txtCounter";
            this.txtCounter.Size = new Size(0x197, 20);
            this.txtCounter.TabIndex = 7;
            this.label21.AutoSize = true;
            this.label21.Location = new Point(9, 0xb7);
            this.label21.Name = "label21";
            this.label21.Size = new Size(0x2c, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "Counter:";
            this.pictureBox4.BackColor = Color.Transparent;
            this.pictureBox4.Dock = DockStyle.Fill;
            this.pictureBox4.Location = new Point(0, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Size(0x1e8, 0x1be);
            this.pictureBox4.TabIndex = 0x9e;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Paint += new PaintEventHandler(this.pictureBox4_Paint);
            this.cbConflict.Location = new Point(0x100, 0xb1);
            this.cbConflict.Name = "cbConflict";
            this.cbConflict.Size = new Size(0x68, 0x18);
            this.cbConflict.TabIndex = 0xb6;
            this.cbConflict.Text = "Conflict";
            this.AutoScaleBaseSize = new Size(5, 13);
            base.ClientSize = new Size(0x200, 0x206);
            base.Controls.Add(this.tabControl1);
            base.Controls.Add(this.txtACTUrno);
            base.Controls.Add(this.cbUTC);
            base.Controls.Add(this.btnClose);
            base.Controls.Add(this.btnNew);
            base.Controls.Add(this.btnApply);
            base.Controls.Add(this.btnSave);
            base.Controls.Add(this.pictureBoxButton);
            base.Icon = (Icon)manager.GetObject("$this.Icon");
            base.Name = "frmView";
            this.Text = "Location Changes: Define Flights and Locations";
            base.Load += new EventHandler(this.frmView_Load);
            base.Activated += new EventHandler(this.frmView_Activated);
            base.Closing += new CancelEventHandler(this.frmView_Closing);
            base.Resize += new EventHandler(this.frmView_Resize);
            ((ISupportInitialize)this.pictureBoxButton).EndInit();
            this.FlightData.ResumeLayout(false);
            this.FlightData.PerformLayout();
            ((ISupportInitialize)this.pictureBox1).EndInit();
            ((ISupportInitialize)this.pictureBox2).EndInit();
            this.BasicData.ResumeLayout(false);
            ((ISupportInitialize)this.pictureBox3).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.CCAData.ResumeLayout(false);
            this.CCAData.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((ISupportInitialize)this.pictureBox4).EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();

          

        }
        #endregion

        private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            //UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke, Color.LightGray);
            UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);

        }
        private void pictureBox3_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            //UT.DrawGradient(e.Graphics, 90f, pictureBox3, Color.WhiteSmoke, Color.LightGray);
            UT.DrawGradient(e.Graphics, 90f, pictureBox3, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);

        }

        private void frmView_Resize(object sender, System.EventArgs e)
        {
            pictureBox1.Invalidate();
            pictureBox3.Invalidate();
            pictureBoxButton.Invalidate();
        }

        private void InitWithConflict()
        {
            if (CtrlConfig.GetInstance().GetLinkInfoFor("AFT").GetLinkTableList().Contains("CFL"))
            {
                this.m_isConflict = true;
            }
        }

 
       private void frmView_Load(object sender, System.EventArgs e)
        {
            this.InitWithConflict();
            this.myDB = UT.GetMemDB();
            this.cbUTC.Parent = this.pictureBoxButton;
            this.btnSave.Parent = this.pictureBoxButton;
            this.btnApply.Parent = this.pictureBoxButton;
            this.btnNew.Parent = this.pictureBoxButton;
            this.btnClose.Parent = this.pictureBoxButton;
            this.cbTakeOver.Parent = this.pictureBox1;
            this.btnNow.Parent = this.pictureBox1;
            this.btnALC.Parent = this.pictureBox1;
            this.btnACT.Parent = this.pictureBox1;
            this.btnNAT.Parent = this.pictureBox1;
            this.btnSTY.Parent = this.pictureBox1;
            this.btnACR.Parent = this.pictureBox1;
            this.lblFrom.Parent = this.pictureBox1;
            this.lblTo.Parent = this.pictureBox1;
            this.lblFLNU.Parent = this.pictureBox1;
            this.lblALC.Parent = this.pictureBox1;
            this.lblACT.Parent = this.pictureBox1;
            this.lblNAT.Parent = this.pictureBox1;
            this.lblSTY.Parent = this.pictureBox1;
            this.lblREGN.Parent = this.pictureBox1;
            this.lblCallSign.Parent = this.pictureBox1;
            this.cbPosition.Parent = this.pictureBox1;
            this.cbGate.Parent = this.pictureBox1;
            this.cbBelt.Parent = this.pictureBox1;
            this.cbLounge.Parent = this.pictureBox1;
            this.cbExit.Parent = this.pictureBox1;
            this.cbCheckin.Parent = this.pictureBox1;
            this.cbArrival.Parent = this.pictureBox1;
            this.cbDeparture.Parent = this.pictureBox1;
            this.cbRunway.Parent = this.pictureBox1;
            this.rbChanged.Parent = this.pictureBox1;
            this.rbDeletedFlights.Parent = this.pictureBox1;
            this.rbFilterButtons.Parent = this.pictureBox1;
            this.cbTowing.Parent = this.pictureBox1;
            txtUserValue1.Parent = this.pictureBox1;
            lblUser.Parent = this.pictureBox1;
           
           if (this.m_isConflict)
            {
                this.rbFilterConflict.Parent = this.pictureBox1;
                this.cbConflict.Parent = this.pictureBox1;
            }
            this.dateTimePickerFrm_BD.Parent = this.pictureBox3;
            this.dateTimePickerTo_BD.Parent = this.pictureBox3;
            this.cbTakeOver_BD.Parent = this.pictureBox3;
            this.label16.Parent = this.pictureBox3;
            this.label17.Parent = this.pictureBox3;
            this.label18.Parent = this.pictureBox3;
            this.cbTable.Parent = this.pictureBox3;
            this.button7.Parent = this.pictureBox3;
            this.panel1.Parent = this.pictureBox3;
            label24.Parent = pictureBox3;

            this.groupBox1.Parent = this.pictureBox4;
            this.groupBox2.Parent = this.pictureBox4;
            label25.Parent = pictureBox4;

            string strDTFormat = "dd.MM.yyyy - HH:mm";
            DateTime now = DateTime.Now;
            DateTime dtTmpFrom = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0, 0);
            DateTime dtTmpTo = new DateTime(now.Year, now.Month, now.Day, 0x17, 0x3b, 0x3b, 0);
            this.SetDTControls(strDTFormat, dtTmpFrom, dtTmpTo);
            if (!this.m_isConflict)
            {
                this.cbConflict.Visible = false;
                this.rbFilterConflict.Visible = false;
            }
            else
            {
                this.cbConflict.Visible = true;
                this.rbFilterConflict.Visible = true;
            }


            label21.Parent = pictureBox4;
            this.InitWithFile();
            this.InitCCKSelections();
            this.CheckChecked();
            this.Check_chDeleted();
            this.ReadRegistry();
            base.Focus();
            this.dateTimePickerFrom.Focus();
            rbDeletedFlights.Checked = true;
            rbFilterConflict.Location = new Point(120, 0xba);

         
            this.Height = 584;
            tabControl1.Height += 24;
       }

        private void SetDTControls(string strDTFormat, DateTime dtTmpFrom, DateTime dtTmpTo)
        {
            dateTimePickerFrom.CustomFormat = strDTFormat;
            dateTimePickerFrom.Value = dtTmpFrom;
            dateTimePickerTo.CustomFormat = strDTFormat;
            dateTimePickerTo.Value = dtTmpTo;

            dateTimePickerFrm_BD.CustomFormat = strDTFormat;
            dateTimePickerFrm_BD.Value = dtTmpFrom;
           
            dateTimePickerTo_BD.CustomFormat = strDTFormat;
            dateTimePickerTo_BD.Value = dtTmpTo;
               //*********************************
            //START DateTime fromat for Common CKI tab
            //*********************************

            //Schedule Open 
            dtpOpenFrom.CustomFormat = strDTFormat;
            dtpOpenFrom.Value = dtTmpFrom;
            dtpOpenTo.CustomFormat = strDTFormat;
            dtpOpenTo.Value = dtTmpTo;

            //Schedule close 
            dtpCloseFrom.CustomFormat = strDTFormat;
            dtpCloseFrom.Value = dtTmpFrom;
            dtpCloseTo.CustomFormat = strDTFormat;
            dtpCloseTo.Value = dtTmpTo;
            //*********************************
            //END DateTime fromat for Common CKI tab
            //*********************************

        }

        private void SetValueCCKTab(DateTime dtOpenFrom, DateTime dtOpenTo,DateTime dtCloseFrom, DateTime dtCloseTo,string counter)
        {
            //Schedule Open 
            dtpOpenFrom.Value = dtOpenFrom;
            dtpOpenTo.Value = dtOpenTo;

            //Schedule close 
            dtpCloseFrom.Value = dtCloseFrom;
            dtpCloseTo.Value = dtCloseTo;

            //Counter
            txtCounter.Text = counter.Trim();
            
        }
        private void WriteRegistry()
        {
            string theKey = "Software\\UFIS\\DATACHANGES\\ViewDialog";
            RegistryKey rk = Registry.CurrentUser.CreateSubKey(theKey);
            if (rk == null)
            {
                rk.CreateSubKey(theKey);
                rk = Registry.CurrentUser.OpenSubKey(theKey, true);
            }

            rk.SetValue("X", this.Left.ToString());
            rk.SetValue("Y", this.Top.ToString());
            rk.SetValue("Width", this.Width.ToString());
            rk.SetValue("Height", this.Height.ToString());
            rk.Close();
        }

        private void ReadRegistry()
        {
            int myX = -1, myY = -1, myW = -1, myH = -1;
            string regVal = "";
            string theKey = "Software\\UFIS\\DATACHANGES\\ViewDialog";
            RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
            if (rk != null)
            {
                regVal = rk.GetValue("X", "-1").ToString();
                myX = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Y", "-1").ToString();
                myY = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Width", "-1").ToString();
                myW = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Height", "-1").ToString();
                myH = Convert.ToInt32(regVal);
            }
            if (myW != -1 && myH != -1)
            {
                this.Left = myX;
                this.Top = myY;
                this.Width = myW;
                this.Height = myH;
            }
        }
        private void InitCCKSelections()
        {
            FileStream fsCCKText;
            try
            {
                fsCCKText = new FileStream(strFileNameCKI, FileMode.OpenOrCreate);

                StreamReader fstr_in = new StreamReader(fsCCKText);
                string[] txtArray = null;
                string txtLine;
                while ((txtLine = fstr_in.ReadLine()) != null)
                {
                    txtArray = txtLine.Split(new char[] { '|' });
                }
                if (txtArray.Length > 0)
                {
                    //txtArray[0] - Schedule Open From
                    //txtArray[1] - Schedule Open To
                    //txtArray[2] - Schedule Close From
                    //txtArray[3] - Schedule Close To
                    //txtArray[4] - Counter

                    SetValueCCKTab(UT.CedaTimeToDateTime(txtArray[0]),
                        UT.CedaTimeToDateTime(txtArray[1]),
                        UT.CedaTimeToDateTime(txtArray[2]),
                        UT.CedaTimeToDateTime(txtArray[3]),
                        txtArray[4]);
                }
            }
            catch(Exception)
            {
            }
            
        }
        private void InitWithFile()
        {
            string str;
            FileStream stream;
            UT.IsTimeInUtc = false;
            this.cbUTC.Checked = false;
            this.cbUTC.Text = "Local";
            this.cbPosition.Checked = true;
            this.cbGate.Checked = true;
            this.cbBelt.Checked = true;
            this.cbLounge.Checked = true;
            this.cbCheckin.Checked = true;
            this.cbExit.Checked = true;
            this.cbRunway.Checked = true;
            try
            {
                stream = new FileStream(this.mySaveFile, FileMode.OpenOrCreate);
            }
            catch (IOException)
            {
                return;
            }
            StreamReader reader = new StreamReader(stream);
            while ((str = reader.ReadLine()) != null)
            {
                this.cbUTC.Text = "Local";
                string[] strArray = str.Split(new char[] { '|' });
                
                
                if (strArray.Length >= 14)
                {
                    string[] strArray2 = strArray[1].Split(new char[] { ',' });
                    for (int i = 0; i < strArray2.Length; i++)
                    {
                        if (strArray2.GetValue(i).ToString() == "1")
                        {
                            if (i == 0)
                            {
                                this.cbUTC.Checked = true;
                                UT.IsTimeInUtc = true;
                                this.cbUTC.Text = "UTC";
                            }
                            if (i == 1)
                            {
                                this.cbPosition.Checked = true;
                            }
                            if (i == 2)
                            {
                                this.cbGate.Checked = true;
                            }
                            if (i == 3)
                            {
                                this.cbBelt.Checked = true;
                            }
                            if (i == 4)
                            {
                                this.cbLounge.Checked = true;
                            }
                            if (i == 5)
                            {
                                this.cbCheckin.Checked = true;
                            }
                            if (i == 6)
                            {
                                this.cbRunway.Checked = true;
                            }
                        }
                        else
                        {
                            if (i == 0)
                            {
                                this.cbUTC.Checked = false;
                                UT.IsTimeInUtc = false;
                                this.cbUTC.Text = "Local";
                            }
                            if (i == 1)
                            {
                                this.cbPosition.Checked = false;
                            }
                            if (i == 2)
                            {
                                this.cbGate.Checked = false;
                            }
                            if (i == 3)
                            {
                                this.cbBelt.Checked = false;
                            }
                            if (i == 4)
                            {
                                this.cbLounge.Checked = false;
                            }
                            if (i == 5)
                            {
                                this.cbCheckin.Checked = false;
                            }
                            if (i == 6)
                            {
                                this.cbRunway.Checked = false;
                            }
                        }
                    }
                    this.dateTimePickerFrom.Value = UT.CedaTimeToDateTime(strArray[2]);
                    this.dateTimePickerTo.Value = UT.CedaTimeToDateTime(strArray[3]);
                    this.txtFLNUValue.Text = strArray[4];
                    this.txtFLTSValue.Text = strArray[5];
                    this.txtALTValue.Text = strArray[6];
                    this.txtALT3Value.Text = strArray[7];
                    this.txtACTValue.Text = strArray[8];
                    this.txtACT5Value.Text = strArray[9];
                    this.txtNATValue.Text = strArray[10];
                    this.txtSTYValue.Text = strArray[11];
                    this.txtREGNValue.Text = strArray[12];
                    this.txtCallSignValue.Text = strArray[13];
                    this.txtUserValue1.Text = strArray[14];
                    txtUserValue2.Text = strArray[14];
                    txtUserValue3.Text = strArray[14];

                 
                }
                this.rbChanged.Checked = false;
                this.rbFilterButtons.Checked = false;
                this.cbArrival.Checked = false;
                this.cbDeparture.Checked = false;
                this.cbTowing.Checked = false;
                this.cbConflict.Checked = false;
                if (strArray.Length >= 0x11)
                {
                    if (strArray[14] == "1")
                    {
                        this.rbChanged.Checked = true;
                    }
                    if (strArray[15] == "1")
                    {
                        this.cbArrival.Checked = true;
                    }
                    if (strArray[0x10] == "1")
                    {
                        this.cbDeparture.Checked = true;
                    }
                }
                if (!this.rbChanged.Checked)
                {
                    this.cbArrival.Visible = false;
                    this.cbDeparture.Visible = false;
                    this.cbTowing.Visible = false;
                    this.cbConflict.Visible = false;
                }
                if (strArray.Length >= 0x12)
                {
                    if (strArray[0x11] == "1")
                    {
                        this.cbExit.Checked = true;
                    }
                    else
                    {
                        this.cbExit.Checked = false;
                    }
                }
                if ((strArray.Length >= 0x13) && (strArray[0x12] == "1"))
                {
                    this.cbTakeOver.Checked = true;
                }
                if ((strArray.Length >= 20) && (strArray[0x13] == "1"))
                {
                    this.rbDeletedFlights.Checked = true;
                }
                if ((strArray.Length >= 0x15) && (strArray[20] == "1"))
                {
                    this.rbFilterButtons.Checked = true;
                }
                if (strArray.Length >= 0x16)  //QDO 
                {
                    this.dateTimePickerFrm_BD.Value = UT.CedaTimeToDateTime(strArray[0x16]);
                    
                }
                if (strArray.Length >= 0x17)   //QDO
                {
                     this.dateTimePickerTo_BD.Value = UT.CedaTimeToDateTime(strArray[0x15]);
                    
                
                
                }
                if ((strArray.Length >= 0x18) && (strArray[0x17] == "1"))
                {
                    this.cbTakeOver_BD.Checked = true;
                }
                if ((strArray.Length >= 0x19) && (strArray[0x18] == "1"))
                {
                    this.cbTowing.Checked = true;
                }
                if (this.m_isConflict)
                {
                    if ((strArray.Length >= 0x1a) && (strArray[0x19] == "1"))
                    {
                        this.cbConflict.Checked = true;
                    }
                    if ((strArray.Length >= 0x1b) && (strArray[0x1a] == "1"))
                    {
                        this.rbFilterConflict.Checked = true;
                    }
                }
                this.Check_chDeleted();
                this.CheckChecked();
            }
            reader.Close();


        }

        private void btnACT_Click(object sender, System.EventArgs e)
        {
            System.Windows.Forms.DialogResult olResult;
            frmChoiceList olDlg = new frmChoiceList();
            olDlg.currTable = "ACT";
            olDlg.lblCaption.Text = "Aircrafts";
            olResult = olDlg.ShowDialog();
            if (olResult == DialogResult.OK)
            {
                int sel = olDlg.tabList.GetCurrentSelected();
                string strUrno = olDlg.tabList.GetColumnValue(sel, 0);
                txtACTUrno.Text = strUrno;
                txtACTValue.Text = olDlg.tabList.GetColumnValue(sel, 1);
                txtACTValue.ForeColor = Color.Black;
                txtACT5Value.Text = olDlg.tabList.GetColumnValue(sel, 2);
                txtACT5Value.ForeColor = Color.Black;
            }
            CheckValid();
        }

        private void txtACTValue_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtACTValue.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtACTValue.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["ACT"];
            bool blFound = false;
            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValue == myTab[i]["IATA-Code"])
                {
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }

            if (blFound == true)
                txtACTValue.ForeColor = Color.Black;
            else
                txtACTValue.ForeColor = Color.Red;

            CheckValid();
        }

        private void btnNow_Click(object sender, System.EventArgs e)
        {
            DateTime olNow;
            if (UT.IsTimeInUtc == true)
            {
                olNow = DateTime.UtcNow;
            }
            else
            {
                olNow = DateTime.Now;
            }
            // Selected Tab Page
            if ("Basic Data" == omSelectedTabPage)
            {

               // MessageBox.Show("dfddf");
                dateTimePickerTo_BD.Value = olNow;
            }
            else
                dateTimePickerTo.Value = olNow;
        }

        // check if any checkbox for locations are activated
        // if not set the label above to red because nothing can be found without a location
        private void CheckChecked()
        {
            if (cbPosition.Checked || cbGate.Checked || cbBelt.Checked || cbLounge.Checked || cbCheckin.Checked || cbExit.Checked || cbRunway.Checked)
                label1.ForeColor = Color.Lime;
            else
                label1.ForeColor = Color.Red;

            CheckValid();
        }

        // check for a valid search
        // if any control is red (invalid) the apply and save will be disabled
        private bool CheckValid()
        {
            btnApply.Enabled = true;
            btnSave.Enabled = true;

            if (label1.ForeColor == Color.Red
                || txtFLNUValue.ForeColor == Color.Red
                || txtFLTSValue.ForeColor == Color.Red
                || txtALTValue.ForeColor == Color.Red
                || txtALT3Value.ForeColor == Color.Red
                || txtACTValue.ForeColor == Color.Red
                || txtACT5Value.ForeColor == Color.Red
                || txtNATValue.ForeColor == Color.Red
                || txtSTYValue.ForeColor == Color.Red
                || txtREGNValue.ForeColor == Color.Red
                || txtCallSignValue.ForeColor == Color.Red)
            {
                btnApply.Enabled = false;
                btnSave.Enabled = false;
                return false;
            }

            return true;
        }

        // toggle for checked/unchecked
        private void cbPosition_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbPosition.Checked == true)
                cbPosition.BackColor = Color.LightGreen;
            else
                cbPosition.BackColor = Color.Transparent;

            cbPosition.Refresh();
            CheckChecked();
        }

        // toggle for checked/unchecked
        private void cbGate_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbGate.Checked == true)
                cbGate.BackColor = Color.LightGreen;
            else
                cbGate.BackColor = Color.Transparent;

            cbGate.Refresh();
            CheckChecked();
        }

        // toggle for checked/unchecked
        private void cbBelt_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbBelt.Checked == true)
                cbBelt.BackColor = Color.LightGreen;
            else
                cbBelt.BackColor = Color.Transparent;
            
            cbBelt.Refresh();
            CheckChecked();
        }

        // toggle for checked/unchecked
        private void cbLounge_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbLounge.Checked == true)
                cbLounge.BackColor = Color.LightGreen;
            else
                cbLounge.BackColor = Color.Transparent;

            cbLounge.Refresh();
            CheckChecked();
        }

        // toggle for checked/unchecked
        private void cbCheckin_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbCheckin.Checked == true)
                cbCheckin.BackColor = Color.LightGreen;
            else
                cbCheckin.BackColor = Color.Transparent;

            cbCheckin.Refresh();
            CheckChecked();
        }

        private void cbRunway_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbRunway.Checked == true)
                cbRunway.BackColor = Color.LightGreen;
            else
                cbRunway.BackColor = Color.Transparent;

            cbRunway.Refresh();
            CheckChecked();
        }

        private void pictureBoxButton_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            //UT.DrawGradient(e.Graphics, 90f, pictureBoxButton, Color.WhiteSmoke, Color.LightGray);
            UT.DrawGradient(e.Graphics, 90f, pictureBoxButton, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);

        }

        // do nothing just close
        private void btnClose_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        // toggle for utc/local request
        private void cbUTC_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbUTC.Checked == true)
            {
                cbUTC.Text = "UTC";
                UT.IsTimeInUtc = true;
            }
            else
            {
                cbUTC.Text = "Local";
                UT.IsTimeInUtc = false;
            }

            cbUTC.Refresh();
        }

        // check for valid input (invalid is red)
        private void txtNATValue_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtNATValue.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtNATValue.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["NAT"];
            bool blFound = false;

            string[] strArr = strValue.Split('/');
            string strValueTtyp = "";
            if (strArr.Length >= 1)
                strValueTtyp = strArr[0];

            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                string t = myTab[i]["TTYP"];
                if (strValueTtyp == myTab[i]["Type"])
                {
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }
            if (blFound == true)
                txtNATValue.ForeColor = Color.Black;
            else
                txtNATValue.ForeColor = Color.Red;

            CheckValid();
        }

        // check for valid input (invalid is red)
        private void txtSTYValue_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtSTYValue.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtSTYValue.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["STY"];
            bool blFound = false;

            string[] strArr = strValue.Split('/');
            string strValueStyp = "";
            if (strArr.Length >= 1)
                strValueStyp = strArr[0];

            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValueStyp == myTab[i]["Type"])
                {
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }
            if (blFound == true)
                txtSTYValue.ForeColor = Color.Black;
            else
                txtSTYValue.ForeColor = Color.Red;

            CheckValid();
        }

        // call ChoiceList
        private void btnALC_Click(object sender, System.EventArgs e)
        {
            System.Windows.Forms.DialogResult olResult;
            frmChoiceList olDlg = new frmChoiceList();
            olDlg.currTable = "ALT";
            olDlg.lblCaption.Text = "Airlines";
            olResult = olDlg.ShowDialog();
            if (olResult == DialogResult.OK)
            {
                int sel = olDlg.tabList.GetCurrentSelected();
                string strUrno = olDlg.tabList.GetColumnValue(sel, 0);
                txtACTUrno.Text = strUrno;
                txtALTValue.Text = olDlg.tabList.GetColumnValue(sel, 1);
                txtALTValue.ForeColor = Color.Black;
                txtALT3Value.Text = olDlg.tabList.GetColumnValue(sel, 2);
                txtALT3Value.ForeColor = Color.Black;
            }
            CheckValid();
        }

        // call ChoiceList
        private void btnNAT_Click(object sender, System.EventArgs e)
        {
            System.Windows.Forms.DialogResult olResult;
            frmChoiceList olDlg = new frmChoiceList();
            olDlg.currTable = "NAT";
            olDlg.lblCaption.Text = "Natures";
            olResult = olDlg.ShowDialog();
            if (olResult == DialogResult.OK)
            {
                int sel = olDlg.tabList.GetCurrentSelected();
                string strUrno = olDlg.tabList.GetColumnValue(sel, 0);
                txtACTUrno.Text = strUrno;
                txtNATValue.Text = olDlg.tabList.GetColumnValue(sel, 1) + "/" + olDlg.tabList.GetColumnValue(sel, 2);
                txtNATValue.ForeColor = Color.Black;
            }
            CheckValid();
        }

        // call ChoiceList
        private void btnSTY_Click(object sender, System.EventArgs e)
        {
            System.Windows.Forms.DialogResult olResult;
            frmChoiceList olDlg = new frmChoiceList();
            olDlg.currTable = "STY";
            olDlg.lblCaption.Text = "Services";
            olResult = olDlg.ShowDialog();
            if (olResult == DialogResult.OK)
            {
                int sel = olDlg.tabList.GetCurrentSelected();
                string strUrno = olDlg.tabList.GetColumnValue(sel, 0);
                txtACTUrno.Text = strUrno;
                txtSTYValue.Text = olDlg.tabList.GetColumnValue(sel, 1) + "/" + olDlg.tabList.GetColumnValue(sel, 2);
                txtSTYValue.ForeColor = Color.Black;
            }
            CheckValid();
        }

        // call ChoiceList. ACTTAB not loaded automaticlly in frmData (too many reccords for the startup) 
        // if it hasn't been loaded once the user will be ask if he want.
        private void btnACR_Click(object sender, System.EventArgs e)
        {
            if (!m_ACRloaded)
            {
                DialogResult results = MessageBox.Show(this, "Will you load the ACRTAB ? (This could take some time!)", "Loading ACRTAB ?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (results == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    m_ACRloaded = true;
                    frmData DataForm = new frmData();
                    DataForm.Hide();
                    DataForm.LoadACRData();
                    this.Cursor = Cursors.Arrow;
                }
            }

            if (m_ACRloaded)
            {
                System.Windows.Forms.DialogResult olResult;
                frmChoiceList olDlg = new frmChoiceList();
                olDlg.currTable = "ACR";
                olDlg.lblCaption.Text = "Registrations";
                olResult = olDlg.ShowDialog();
                if (olResult == DialogResult.OK)
                {
                    int sel = olDlg.tabList.GetCurrentSelected();
                    string strUrno = olDlg.tabList.GetColumnValue(sel, 0);
                    txtACTUrno.Text = strUrno;
                    string val1 = olDlg.tabList.GetColumnValue(sel, 1);
                    string val2 = olDlg.tabList.GetColumnValue(sel, 2);
                    string val3 = olDlg.tabList.GetColumnValue(sel, 3);
                    string strValue = val1 + "/" + val2 + "-" + val3;
                    txtREGNValue.Text = strValue;
                    txtREGNValue.ForeColor = Color.Black;
                }
                CheckValid();
            }
        }

        // check for valid input (invalid is red)
        private void txtALTValue_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtALTValue.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtALTValue.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["ALT"];
            bool blFound = false;
            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValue == myTab[i]["IATA-Code"])
                {
                    txtALT3Value.Text = myTab[i]["ICAO-Code"];
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }
            if (blFound == true)
                txtALTValue.ForeColor = Color.Black;
            else
                txtALTValue.ForeColor = Color.Red;

            CheckValid();
        }

        // check for valid input (invalid is red)
        private void txtREGNValue_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtREGNValue.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtREGNValue.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["ACR"];
            bool blFound = false;

            if (myTab == null)
                return;

            string[] strArr = strValue.Split('/');
            string strValueRegn = "";
            if (strArr.Length >= 1)
                strValueRegn = strArr[0];

            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValueRegn == myTab[i]["Registration"])
                {
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }
            if (blFound == true)
                txtREGNValue.ForeColor = Color.Black;
            else
                txtREGNValue.ForeColor = Color.Red;

            CheckValid();
        }

        // toggle for check/uncheck all location-boxes
        private void label1_Click(object sender, System.EventArgs e)
        {
            if (label1.ForeColor == Color.Red)
            {
                cbPosition.Checked = true;
                cbBelt.Checked = true;
                cbGate.Checked = true;
                cbExit.Checked = true;
                cbLounge.Checked = true;
                cbCheckin.Checked = true;
                cbRunway.Checked = true;
                label1.ForeColor = Color.Lime;
            }
            else
            {
                cbPosition.Checked = false;
                cbBelt.Checked = false;
                cbGate.Checked = false;
                cbExit.Checked = false;
                cbLounge.Checked = false;
                cbCheckin.Checked = false;
                cbRunway.Checked = false;
                label1.ForeColor = Color.Red;
            }
            CheckChecked();
        }

        // clears all entries in the view
        private void btnNew_Click(object sender, System.EventArgs e)
        {
            txtACTValue.Text = "";
            txtACT5Value.Text = "";
            txtALTValue.Text = "";
            txtALT3Value.Text = "";
            txtFLNUValue.Text = "";
            txtFLTSValue.Text = "";
            txtNATValue.Text = "";
            txtREGNValue.Text = "";
            txtSTYValue.Text = "";
            txtCallSignValue.Text = "";
            txtUserValue1.Text = "";
        }

        //saves the entries to mySaveFile (for next time call, default) and fill the selection for the flights
        private void btnSave_Click(object sender, System.EventArgs e)
        {
            if (!(this.CheckValid() && this.Check_cbAllFields()))
            {
                this.DialogResult = DialogResult.None;
                return;
            }
            else
            {
                frmMain.test = true;
                this.DialogResult = DialogResult.OK;

                this.Hide();


                

                if (this.rbDeletedFlights.Checked && ("Flight Data" == this.omSelectedTabPage))
                {
                    this.FillSelectionDeleted();
                }
                else if (this.omSelectedTabPage == "Common CKI")
                {
                    this.m_selCKI = this.FillCKISelection();
                }
                else
                {
                                    
                    this.FillSelection();
                }



                IOException exception;
                base.DialogResult = DialogResult.OK;
                base.Hide();
                string str = "|";
                string str2 = "";
                string str3 = "<Default>";
                str2 = (((str2 + (this.cbUTC.Checked ? "1," : "0,")) + (this.cbPosition.Checked ? "1," : "0,") + (this.cbGate.Checked ? "1," : "0,")) + (this.cbBelt.Checked ? "1," : "0,") + (this.cbLounge.Checked ? "1," : "0,")) + (this.cbCheckin.Checked ? "1," : "0,") + (this.cbRunway.Checked ? "1" : "0");
                string str4 = UT.DateTimeToCeda(this.dateTimePickerFrom.Value);
                string str5 = UT.DateTimeToCeda(this.dateTimePickerTo.Value);
                string str6 = str3 + str + str2 + str + str4 + str + str5 + str + this.txtFLNUValue.Text + str + this.txtFLTSValue.Text + str + this.txtALTValue.Text + str + this.txtALT3Value.Text + str + this.txtACTValue.Text + str + this.txtACT5Value.Text + str + this.txtNATValue.Text + str + this.txtSTYValue.Text + str + this.txtREGNValue.Text + str + this.txtCallSignValue.Text + str + txtUserValue1.Text + str;
                str2 = "";
                str2 = ((((((str2 + (this.rbChanged.Checked ? "1" : "0") + str) + (this.cbArrival.Checked ? "1" : "0") + str) + (this.cbDeparture.Checked ? "1" : "0") + str) + (this.cbExit.Checked ? "1" : "0") + str) + (this.cbTakeOver.Checked ? "1" : "0") + str) + (this.rbDeletedFlights.Checked ? "1" : "0") + str) + (this.rbFilterButtons.Checked ? "1" : "0") + str;
                str6 = str6 + str2;
                str4 = UT.DateTimeToCeda(this.dateTimePickerFrm_BD.Value);
                str5 = UT.DateTimeToCeda(this.dateTimePickerTo_BD.Value);
                str2 = this.cbTakeOver_BD.Checked ? "1" : "0";
                str2 = (str2 + str) + (this.cbTowing.Checked ? "1" : "0") + str;
                if (this.m_isConflict)
                {
                    str2 = (str2 + (this.cbConflict.Checked ? "1" : "0") + str) + (this.rbFilterConflict.Checked ? "1" : "0") + str;
                }
                string str7 = str6;
                str6 = str7 + str4 + str + str5 + str + str2;
                if (File.Exists(this.mySaveFile))
                {
                    File.Delete(this.mySaveFile);
                }
                FileStream stream = null;
                try
                {
                    stream = new FileStream(this.mySaveFile, FileMode.OpenOrCreate);
                }
                catch (IOException exception1)
                {
                    exception = exception1;
                    MessageBox.Show(this, this.mySaveFile, exception.Message + "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                StreamWriter writer = new StreamWriter(stream);
                try
                {
                    writer.Write(str6);
                }
                catch (IOException exception2)
                {
                    exception = exception2;
                    MessageBox.Show(this, this.mySaveFile, exception.Message + "Write Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                writer.Close();
                










                this.SaveCCKTabSelection();
            
            
            }

        }
       
        private void SaveCCKTabSelection()
        {
            try
            {
                string txtSep = "|";
              //  string txtChecked = "";
                string txtFile = "";

                string strOpenFrom = UT.DateTimeToCeda(dtpOpenFrom.Value);
                txtFile += strOpenFrom + txtSep;
                string strOpenTo = UT.DateTimeToCeda(dtpOpenTo.Value);
                txtFile += strOpenTo + txtSep;
                string strCloseFrom = UT.DateTimeToCeda(dtpCloseFrom.Value);
                txtFile += strCloseFrom + txtSep;
                string strCloseTo = UT.DateTimeToCeda(dtpCloseTo.Value);
                txtFile += strCloseTo + txtSep;
                string strCounter = txtCounter.Text.Trim();
                txtFile += strCounter;

                File.Delete(strFileNameCKI);
                FileStream fout = null;
                try
                {
                    fout = new FileStream(strFileNameCKI, FileMode.OpenOrCreate);
                }
                catch (IOException exc)
                {
                    MessageBox.Show(this, strFileNameCKI, exc.Message + "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                StreamWriter fstr_out = new StreamWriter(fout);
                try
                {
                    fstr_out.Write(txtFile);
                }
                catch (IOException exc)
                {
                    MessageBox.Show(this, strFileNameCKI, exc.Message + "Write Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                fstr_out.Close();
                fout.Close();
            }
            catch(Exception)
            {
            }

        }
        private string FillCKISelection()
        {
            string selCKI = "";
            //Scheduled open 
            DateTime dtOpenFrom = dtpOpenFrom.Value;
            DateTime dtOpenTo = dtpOpenTo.Value;
            DateTime datFrom, datTo;
            if (UT.IsTimeInUtc == false)
            {
                datFrom = UT.LocalToUtc(dtOpenFrom);
                datTo = UT.LocalToUtc(dtOpenTo);
            }
            else
            {
                datFrom = dtOpenFrom;
                datTo = dtOpenTo;
            }
            string strOpenFrom = UT.DateTimeToCeda(datFrom);
            string strOpenTo = UT.DateTimeToCeda(datTo);
            selCKI = "  (CKBS >= '" + strOpenFrom + "' AND CKBS <='" + strOpenTo + "') ";

            //Scheduled open 
            DateTime dtCloseFrom = dtpCloseFrom.Value;
            DateTime dtCloseTo = dtpCloseTo.Value;
            if (UT.IsTimeInUtc == false)
            {
                datFrom = UT.LocalToUtc(dtCloseFrom);
                datTo = UT.LocalToUtc(dtCloseTo);
            }
            else
            {
                datFrom = dtCloseFrom;
                datTo = dtCloseTo;
            }
            string strCloseFrom = UT.DateTimeToCeda(datFrom);
            string strCloseTo = UT.DateTimeToCeda(datTo);
            //selCKI += " AND CKES BETWEEN '" + strCloseFrom + "' AND '" + strCloseTo + "'";
            selCKI += "  AND (CKES >= '" + strCloseFrom + "' AND CKES <='" + strCloseTo + "') ";


            //counter
            string strCnt = txtCounter.Text.Trim();
            if (!string.IsNullOrEmpty(strCnt))
            {
                selCKI += " AND CKIC='" + strCnt + "'";
            }

            string txtTimeStyle = " [LOCAL] ";
            if (UT.IsTimeInUtc == true)
                txtTimeStyle = " [UTC] ";

           
            myWindowText = "Common CKI Change:" + txtTimeStyle + "Timeframe for Common CKI [" + dateTimePickerFrom.Value.ToString() + "] - [" +
                dateTimePickerTo.Value.ToString() + "]";
            return selCKI;
        }

       
        //mySelectionDeleted
        private void FillSelectionDeleted()
        {
            string txtTimeStyle = " [LOCAL] ";
            if (UT.IsTimeInUtc == true)
                txtTimeStyle = " [UTC] ";


            myWindowText = "Data Change:" + txtTimeStyle + "Timeframe for Deleted Flights [" + dateTimePickerFrom.Value.ToString() + "] - [" +
                dateTimePickerTo.Value.ToString() + "]";


            DateTime datFrom = dateTimePickerFrom.Value;
            DateTime datTo = dateTimePickerTo.Value;
            string strFrom = "";
            string strTo = "";

            if (UT.IsTimeInUtc == false)
            {
                datFrom = UT.LocalToUtc(datFrom);
                datTo = UT.LocalToUtc(datTo);
            }
            strFrom = UT.DateTimeToCeda(datFrom);
            strTo = UT.DateTimeToCeda(datTo);

            mySelectionDeleted = "AND (STOX BETWEEN '" + strFrom + "' AND '" + strTo + "') AND TRAN = 'D'";

        }
        
        private void FillSelection()
        {
            string str;
            string str2;
            string str3;
            DateTime time;
            DateTime time2;
            if ("Flight Data" == this.omSelectedTabPage)
            {
              
                
                string[] strArray;
                str = UT.DateTimeToCeda(this.dateTimePickerFrom.Value);
                str2 = UT.DateTimeToCeda(this.dateTimePickerTo.Value);
                str3 = " [LOCAL] ";
                if (UT.IsTimeInUtc)
                {
                    str3 = " [UTC] ";
                }
                this.myWindowText = "Location Change:" + str3 + "Timeframe for Flights [" + this.dateTimePickerFrom.Value.ToString() + "] - [" + this.dateTimePickerTo.Value.ToString() + "]";
                time = this.dateTimePickerFrom.Value;
                time2 = this.dateTimePickerTo.Value;
                if (!UT.IsTimeInUtc)
                {
                    time = UT.LocalToUtc(time);
                    time2 = UT.LocalToUtc(time2);
                }
                str = UT.DateTimeToCeda(time);
                str2 = UT.DateTimeToCeda(time2);
                string str4 = string.Empty;
                this.mySelection = "WHERE ((TIFA BETWEEN '" + str + "' AND '" + str2 + "') OR (TIFD BETWEEN '" + str + "' and '" + str2 + "'))";
                if (this.txtFLNUValue.Text != "")
                {
                    str4 = str4 + this.mySelFlnu + this.txtFLNUValue.Text + this.myEndSign;
                }
                if (this.txtFLTSValue.Text != "")
                {
                    str4 = str4 + this.mySelFlns + this.txtFLTSValue.Text + this.myEndSign;
                }
                if (this.txtALTValue.Text != "")
                {
                    str4 = str4 + this.mySelAlc2 + this.txtALTValue.Text + this.myEndSign;
                }
                if (this.txtALT3Value.Text != "")
                {
                    str4 = str4 + this.mySelAlc3 + this.txtALT3Value.Text + this.myEndSign;
                }
                if (this.txtACTValue.Text != "")
                {
                    str4 = str4 + this.mySelAct3 + this.txtACTValue.Text + this.myEndSign;
                }
                if (this.txtACT5Value.Text != "")
                {
                    str4 = str4 + this.mySelAct5 + this.txtACT5Value.Text + this.myEndSign;
                }
                if (this.txtNATValue.Text != "")
                {
                    strArray = this.txtNATValue.Text.Split(new char[] { '/' });
                    string text = "";
                    if (strArray.Length >= 1)
                    {
                        text = strArray[0];
                    }
                    else
                    {
                        text = this.txtNATValue.Text;
                    }
                    str4 = str4 + this.mySelTtyp + text + this.myEndSign;
                }
                if (this.txtSTYValue.Text != "")
                {
                    strArray = this.txtSTYValue.Text.Split(new char[] { '/' });
                    string str6 = "";
                    if (strArray.Length >= 1)
                    {
                        str6 = strArray[0];
                    }
                    else
                    {
                        str6 = this.txtSTYValue.Text;
                    }
                    str4 = str4 + this.mySelStyp + str6 + this.myEndSign;
                }
                if (this.txtREGNValue.Text != "")
                {
                    strArray = this.txtREGNValue.Text.Split(new char[] { '/' });
                    string str7 = "";
                    if (strArray.Length >= 1)
                    {
                        str7 = strArray[0];
                    }
                    else
                    {
                        str7 = this.txtREGNValue.Text;
                    }
                    str4 = str4 + this.mySelRegn + str7 + this.myEndSign;
                }
                if (this.txtCallSignValue.Text != "")
                {
                    strArray = this.txtCallSignValue.Text.Split(new char[] { '/' });
                    string str8 = "";
                    if (strArray.Length >= 1)
                    {
                        str8 = strArray[0];
                    }
                    else
                    {
                        str8 = this.txtCallSignValue.Text;
                    }
                    string str9 = this.txtCallSignValue.Text;
                    this.txtFLNUValue.Text = str9.Substring(3, str9.Length - 3);
                    this.txtALT3Value.Text = str9.Substring(0, 3);
                    str4 = str4 + this.mySelCsgn + str8 + this.myEndSign;
                }
                this.mySelection = string.Format("WHERE RKEY IN ( SELECT UNIQUE(RKEY) FROM AFTTAB {0}){1}", this.mySelection, str4);
                if (!this.cbArrival.Checked)
                {
                 
                    this.mySelection = this.mySelection + " AND ADID<>'A'";
                }
                if (!this.cbDeparture.Checked)
                {
               
                    
                    this.mySelection = this.mySelection + " AND ADID<>'D'";
                }
                if (!this.cbTowing.Checked)
                {
                    this.mySelection = this.mySelection + " AND NOT (FTYP IN ('T','G'))";
                }
                this.myLocation.Clear();
                if (this.cbPosition.Checked)
                {
                    this.myLocation.Add("PST");
                }
                if (this.cbGate.Checked)
                {
                    this.myLocation.Add("GAT");
                }
                if (this.cbBelt.Checked)
                {
                    this.myLocation.Add("BLT");
                }
                if (this.cbLounge.Checked)
                {
                    this.myLocation.Add("WRO");
                }
                if (this.cbExit.Checked)
                {
                    this.myLocation.Add("EXT");
                }
                if (this.cbCheckin.Checked)
                {
                    this.myLocation.Add("CCA");
                }
                if (this.cbRunway.Checked)
                {
                    this.myLocation.Add("RWY");
                }


              

            }
            else
            {
               
              
                str = UT.DateTimeToCeda(this.dateTimePickerFrm_BD.Value);
                str2 = UT.DateTimeToCeda(this.dateTimePickerTo_BD.Value);
                str3 = " [LOCAL] ";
                if (UT.IsTimeInUtc)
                {
                    str3 = " [UTC] ";
                }
                this.myWindowText = "Location Change:" + str3 + "Timeframe for Flights [" + this.dateTimePickerFrom.Value.ToString() + "] - [" + this.dateTimePickerTo.Value.ToString() + "]";
                time = this.dateTimePickerFrm_BD.Value;
                time2 = this.dateTimePickerTo_BD.Value;
                if (!UT.IsTimeInUtc)
                {
                    time = UT.LocalToUtc(time);
                    time2 = UT.LocalToUtc(time2);
                }
                str = UT.DateTimeToCeda(time);
                str2 = UT.DateTimeToCeda(time2);
                this.mySelection = "WHERE (TIME BETWEEN '" + str + "' AND '" + str2 + "')";
                int index = 0;
                string str10 = "";
                foreach (Control control in this.panel1.Controls)
                {
                    if (control.GetType().ToString() == "System.Windows.Forms.TextBox")
                    {
                        str10 = control.Text.Trim();
                        if ((str10 != "") && (index < this.arrFieldCodes.Length))
                        {
                            string mySelection = this.mySelection;
                            this.mySelection = mySelection + " AND " + this.arrFieldCodes[index] + "='" + str10 + this.myEndSign;
                        }
                        index++;
                    }
                }
            }

        }
        private void txtALT3Value_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtALT3Value.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtALT3Value.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["ALT"];
            bool blFound = false;
            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValue == myTab[i]["ICAO-Code"])
                {
                    txtALTValue.Text = myTab[i]["IATA-Code"];
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }

            if (blFound == true)
                txtALT3Value.ForeColor = Color.Black;
            else
                txtALT3Value.ForeColor = Color.Red;

            CheckValid();
        }

        private void txtACT5Value_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtACT5Value.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtACT5Value.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["ACT"];
            bool blFound = false;
            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValue == myTab[i]["ICAO-Code"])
                {
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }

            if (blFound == true)
                txtACT5Value.ForeColor = Color.Black;
            else
                txtACT5Value.ForeColor = Color.Red;

            CheckValid();
        }

        private bool Check_cbAllFields()
        {
            bool bLPrompt = false;
            string strPrompt = "";
            if ("Basic Data" == omSelectedTabPage) // Flight Data
            {
                if (cbTable.SelectedIndex == -1)
                {
                    bLPrompt = true;
                    strPrompt = "Please Select Basic Data Table";
                }
            }
            else if (rbChanged.Checked == true)
            {
                if (cbArrival.Checked == false && cbDeparture.Checked == false)
                {
                    bLPrompt = true;
                    strPrompt = "Arrival and/or Departure has to be checked.";
                }
                if (txtFLNUValue.Text.Equals("") || txtALT3Value.Text.Equals(""))
                {
                    if (txtCallSignValue.Text.Equals("")) //nr
                    {
                        bLPrompt = true;
                        strPrompt += "\nFlightnumber and Airline(ICAO-Code) or Call Sign has to be filled.";
                    }
                }
            }

            if (bLPrompt)
            {
                MessageBox.Show(this, strPrompt);
                return false;
            }

            return true;
        }

        private void btnApply_Click(object sender, System.EventArgs e)
        {

            if (!CheckValid() || !Check_cbAllFields())
            {
                this.DialogResult = DialogResult.None;
                return;
            }
            else
            {

              
                frmMain.test  = true;
                this.DialogResult = DialogResult.OK;
            }


            this.Hide();


            if ((rbDeletedFlights.Checked == true) && ("Flight Data" == omSelectedTabPage))
            {
                
                FillSelectionDeleted();
            }
            else if ("Common CKI" == omSelectedTabPage)
            {
               
                m_selCKI = FillCKISelection();
            }
            else
            {
               
                FillSelection();
            }
            
                 
        }

        private void frmView_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            WriteRegistry();
        }

        private void frmView_Activated(object sender, System.EventArgs e)
        {
            dateTimePickerFrom.Focus();


            /*
            Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\DATACHANGE.INI");

            string strTables = myIni.IniReadValue("BASIC_DATA", "TABLES");

            if(strTables == "")
            {
                this.tabControl1.TabPages.Remove(BasicData);
            }
            */
        }

        private void Check_chDeleted(object sender, System.EventArgs e)
        {
            Check_chDeleted();
        }

        private void Check_chDeleted()
        {
            if (this.rbChanged.Checked)
            {
                if (this.label1.ForeColor == Color.Red)
                {
                    this.label1.ForeColor = Color.Lime;
                }
                this.label1.Enabled = false;
                this.btnApply.Enabled = true;
                this.btnSave.Enabled = true;
                this.cbPosition.Checked = true;
                this.cbGate.Checked = true;
                this.cbExit.Checked = true;
                this.cbBelt.Checked = true;
                this.cbLounge.Checked = true;
                this.cbCheckin.Checked = true;
                this.cbRunway.Checked = true;
                this.btnApply.Enabled = true;
                this.btnSave.Enabled = true;
                this.cbPosition.Enabled = false;
                this.cbGate.Enabled = false;
                this.cbBelt.Enabled = false;
                this.cbLounge.Enabled = false;
                this.cbCheckin.Enabled = false;
                this.cbRunway.Enabled = false;
                this.cbExit.Enabled = false;
                this.txtFLNUValue.BackColor = Color.Yellow;
                this.txtALT3Value.BackColor = Color.Yellow;
                this.txtCallSignValue.BackColor = Color.Yellow;
                this.cbArrival.Visible = true;
                this.cbDeparture.Visible = true;
                this.cbTowing.Visible = true;
                this.btnALC.Enabled = true;
                this.btnACR.Enabled = false;
                this.btnACT.Enabled = false;
                this.btnNAT.Enabled = false;
                this.btnSTY.Enabled = false;
                this.txtSTYValue.Enabled = false;
                this.txtNATValue.Enabled = false;
                this.txtACTValue.Enabled = false;
                this.txtACT5Value.Enabled = false;
                this.txtREGNValue.Enabled = false;
                if (this.m_isConflict)
                {
                    this.cbConflict.Visible = true;
                }
            }
            else
            {
                this.cbPosition.Enabled = true;
                this.cbGate.Enabled = true;
                this.cbBelt.Enabled = true;
                this.cbLounge.Enabled = true;
                this.cbCheckin.Enabled = true;
                this.cbRunway.Enabled = true;
                this.cbExit.Enabled = true;
                this.label1.Enabled = true;
                this.CheckChecked();
                this.txtFLNUValue.BackColor = Color.White;
                this.txtALT3Value.BackColor = Color.White;
                this.txtCallSignValue.BackColor = Color.White;
                this.cbArrival.Visible = false;
                this.cbDeparture.Visible = false;
                this.cbTowing.Visible = false;
                this.cbConflict.Visible = false;
                this.btnACR.Enabled = true;
                this.btnACT.Enabled = true;
                this.btnNAT.Enabled = true;
                this.btnSTY.Enabled = true;
                this.txtSTYValue.Enabled = true;
                this.txtNATValue.Enabled = true;
                this.txtACTValue.Enabled = true;
                this.txtACT5Value.Enabled = true;
                this.txtREGNValue.Enabled = true;
            }

        }

        private void chDeleted_CheckedChanged(object sender, System.EventArgs e)
        {
            if (rbDeletedFlights.Checked == true)
            {
                cbPosition.Enabled = false;
                cbGate.Enabled = false;
                cbBelt.Enabled = false;
                cbLounge.Enabled = false;
                cbCheckin.Enabled = false;
                cbRunway.Enabled = false;
                cbExit.Enabled = false;
                label1.Enabled = false;

                btnACR.Enabled = false;
                btnACT.Enabled = false;
                btnNAT.Enabled = false;
                btnSTY.Enabled = false;
                btnALC.Enabled = false;

                txtFLNUValue.BackColor = Color.LightGoldenrodYellow;
                txtALT3Value.BackColor = Color.LightGoldenrodYellow;
                txtCallSignValue.BackColor = Color.LightGoldenrodYellow;

                txtALTValue.Enabled = false;
                txtFLNUValue.Enabled = false;
                txtFLTSValue.Enabled = false;
                txtALT3Value.Enabled = false;
                txtCallSignValue.Enabled = false;

                txtSTYValue.Enabled = false;
                txtNATValue.Enabled = false;
                txtACTValue.Enabled = false;
                txtACT5Value.Enabled = false;
                txtREGNValue.Enabled = false;

            }
            else
            {
                cbPosition.Enabled = true;
                cbGate.Enabled = true;
                cbBelt.Enabled = true;
                cbLounge.Enabled = true;
                cbCheckin.Enabled = true;
                cbRunway.Enabled = true;
                cbExit.Enabled = true;
                label1.Enabled = true;

                btnACR.Enabled = true;
                btnACT.Enabled = true;
                btnNAT.Enabled = true;
                btnSTY.Enabled = true;
                btnALC.Enabled = true;

                txtFLNUValue.BackColor = Color.White;
                txtALT3Value.BackColor = Color.White;
                txtCallSignValue.BackColor = Color.White;

                txtALTValue.Enabled = true;
                txtFLNUValue.Enabled = true;
                txtFLTSValue.Enabled = true;
                txtALT3Value.Enabled = true;
                txtCallSignValue.Enabled = true;

                txtSTYValue.Enabled = true;
                txtNATValue.Enabled = true;
                txtACTValue.Enabled = true;
                txtACT5Value.Enabled = true;
                txtREGNValue.Enabled = true;
            }
        }

        private void cbArrival_CheckedChanged(object sender, System.EventArgs e)
        {
            //			CheckValid();		
        }

        private void cbDeparture_CheckedChanged(object sender, System.EventArgs e)
        {
            //			CheckValid();		
        }

        private void txtFLNUValue_TextChanged(object sender, System.EventArgs e)
        {
            //			CheckValid();		
        }

        private void txtALT3Value_TextChanged(object sender, System.EventArgs e)
        {
            //			CheckValid();		
        }

        private void txtFLNUValue_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (!System.Char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        // toggle for checked/unchecked
        private void cbExit_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbExit.Checked == true)
                cbExit.BackColor = Color.LightGreen;
            else
                cbExit.BackColor = Color.Transparent;

            cbExit.Refresh();
            CheckChecked();
        }
        private void txtACTUrno_TextChanged(object sender, System.EventArgs e)
        {

        }
        /// <summary>
        /// Validates the Input Call Sign Data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCallSignValue_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtCallSignValue.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtCallSignValue.ForeColor = Color.Black;
                return; //Need no check
            }
            if (!m_CSGNloaded)
            {
                LoadCallSignData();
            }

            ITable myTab = myDB["AFT"];
            bool blFound = false;

            if ((myTab == null) || (myTab.Count == 0))
                return;

            string[] strArr = strValue.Split('/');
            string strValueCsgn = "";
            if (strArr.Length >= 1)
                strValueCsgn = strArr[0];

            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValueCsgn == myTab[i]["CallSign"])
                {
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }
            if (blFound == true)
                txtCallSignValue.ForeColor = Color.Black;
            else
                txtCallSignValue.ForeColor = Color.Red;

            CheckValid();
        }

        /// <summary>
        /// Prepares the Query and Loads the Call Sign data.
        /// </summary>
        private void LoadCallSignData()
        {
            DateTime datFrom = dateTimePickerFrom.Value;
            DateTime datTo = dateTimePickerTo.Value;
            if (UT.IsTimeInUtc == false)
            {
                datFrom = UT.LocalToUtc(datFrom);
                datTo = UT.LocalToUtc(datTo);
            }
            string strFrom = UT.DateTimeToCeda(datFrom);
            string strTo = UT.DateTimeToCeda(datTo);

            string myTimeSelection = "WHERE ((TIFA BETWEEN '" + strFrom + "' AND '" + strTo + "') OR (TIFD BETWEEN '" + strFrom + "' and '" + strTo + "'))";

            m_CSGNloaded = true;
            frmData DataForm = new frmData();
            DataForm.Hide();
            DataForm.LoadAFTCallSignData(myTimeSelection);
        }

        private void rbFilterButtons_CheckedChanged(object sender, System.EventArgs e)
        {
            if (rbFilterButtons.Checked == true)
            {
                cbPosition.Enabled = true;
                cbGate.Enabled = true;
                cbBelt.Enabled = true;
                cbLounge.Enabled = true;
                cbCheckin.Enabled = true;
                cbRunway.Enabled = true;
                cbExit.Enabled = true;
                label1.Enabled = true;

                btnACR.Enabled = true;
                btnACT.Enabled = true;
                btnNAT.Enabled = true;
                btnSTY.Enabled = true;

                txtSTYValue.Enabled = true;
                txtFLTSValue.Enabled = true;
                txtNATValue.Enabled = true;
                txtACTValue.Enabled = true;
                txtACT5Value.Enabled = true;
                txtREGNValue.Enabled = true;

            }

        }
        private void OnTableChange(object sender, System.EventArgs e)
        {
            RemovePanelcontrols();

            int indexTable = cbTable.SelectedIndex; // Get Selected Item Index
            omTableCode = arrTableCodes[indexTable];

            Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\DATACHANGE.INI");

            string strTables = myIni.IniReadValue(omTableCode, "IDENTIFIER");
            arrFieldCodes = strTables.Split(',');
            string[] arrFieldNames = new string[arrFieldCodes.Length];

            // Get the Field Names using Field codes
            omSysTab = myDB.Bind("SYS", "SYS", "URNO,FINA,TANA,ADDI,LOGD", "10,4,3,32,7", "URNO,FINA,TANA,ADDI");
            omSysTab.Clear();
            string StrTmpWhere = "WHERE TANA='" + arrTableCodes[indexTable] + "' ORDER BY FINA";
            omSysTab.Load(StrTmpWhere);

            bool bFound = false;
            string strTmp = "";
            for (int i = 0; i < arrFieldCodes.Length; i++)
            {
                for (int j = 0; j < omSysTab.Count; j++)
                {
                    // Check for the Field Code
                    if (arrFieldCodes[i] == omSysTab[j]["FINA"])
                    {
                        strTmp = omSysTab[j]["ADDI"]; //First Character with UpperCase
                        if (strTmp.Length > 1)
                        {
                            strTmp = strTmp.Substring(0, 1).ToUpper() + strTmp.Substring(1);
                        }

                        arrFieldNames[i] = strTmp;
                        bFound = true;
                        break;
                    }
                }
                if (bFound == false) //If the Field codes in the INI file are wrong
                {
                    MessageBox.Show("Invalid Field Codes in the INI file", "Daga Changes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                bFound = false;
            }

            // Add the controls for the Selected Tab
            int pos_Y = 6;
            string strCtrlName = "";
            for (int i = 0; i < arrFieldCodes.Length; i++)
            {
                strCtrlName = "lbl" + i.ToString();
                AddControl(new Label(), new Point(32, pos_Y), new Size(130, 25), arrFieldNames[i], strCtrlName);

                strCtrlName = "txtBox" + i.ToString();
                AddControl(new TextBox(), new Point(170, pos_Y), new Size(128, 20), " ", strCtrlName);

                pos_Y += 30;
            }
            panel1.Refresh();
        }
        /// <summary>
        /// 
        /// </summary>
        private void CreateTableNames()
        {

            //Read the DATACHANGE.INI file and display the corresponding Table names in the combo Box
            Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\DATACHANGE.INI");

            string strTables = myIni.IniReadValue("BASIC_DATA", "TABLES");
            string strTablesToAdd = string.Empty;
            string strTableNameToAdd = string.Empty;

            arrTableCodes = strTables.Split(',');
            

            if (arrTableCodes.Length > 0)
            {
                omTabTab = myDB.Bind("TAB", "TAB", "URNO,Table-Code,Table-Name", "10,3,32", "URNO,LTNA,LNAM");
                omTabTab.Clear();
                omTabTab.Load("ORDER BY LTNA");

                bool bFound = false;
                for (int i = 0; i < arrTableCodes.Length; i++)
                {
                    for (int j = 0; j < omTabTab.Count; j++)
                    {
                        // Add the Table Name
                        if (arrTableCodes[i] == omTabTab[j]["Table-Code"])
                        {
                            if (string.IsNullOrEmpty(strTablesToAdd))
                            {
                                strTablesToAdd = arrTableCodes[i];
                            }
                            else
                            {
                                strTablesToAdd = strTablesToAdd + "," + arrTableCodes[i];
                            }

                            bFound = true; 
                            break;
                        }
                    }
                    if (bFound == false) //If the Table codes in the file are wrong
                    {
                        //MessageBox.Show("Invalid Table Codes in the INI file", "Daga Changes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //return;
                       
                        //MessageBox.Show("The table " + arrTableCodes[i] + " is not found in Database!", "Data Changes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    bFound = false;
                }
                if (strTablesToAdd.Length > 0)
                {
                    arrTableCodes = strTablesToAdd.Split(',');
                    arrTableNames = new string[arrTableCodes.Length];
                    for (int i = 0; i < arrTableCodes.Length; i++)
                    {
                        for (int j = 0; j < omTabTab.Count; j++)
                        {
                            // Add the Table Name
                            if (arrTableCodes[i] == omTabTab[j]["Table-Code"])
                            {
                                arrTableNames[i] = omTabTab[j]["Table-Name"];
                            }
                        }

                    }
                    
                    cbTable.Items.AddRange(arrTableNames);
                }
            }
        }

        private void AddControl(Control aControl, Point Location, Size ctrlSize, string strText, string strName)
        {
            aControl.Location = Location;
            //			aControl.BackColor = System.Drawing.Color.Transparent;
            aControl.Size = ctrlSize;
            aControl.Text = strText;
            aControl.TabIndex = TabIndex;
            aControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
            aControl.Name = strName;
            panel1.Controls.Add(aControl);
        }
        /// <summary>
        /// Remove all the Dynamic Controls
        /// </summary>
        private void RemovePanelcontrols()
        {
            panel1.Controls.Clear();
            panel1.Refresh();

            foreach (System.Windows.Forms.Control olc in panel1.Controls)
            {
                panel1.Controls.Remove(olc);
            }


        }

        /// <summary>
        /// Select the Selected TabPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabPageChanged(object sender, System.EventArgs e)
        {
            omSelectedTabPage = this.tabControl1.SelectedTab.Text;

            if (omSelectedTabPage == "Basic Data" && bFirstTime == true)
            {
                CreateTableNames();
                bFirstTime = false;
            }

            if (omSelectedTabPage == "Basic Data")
            {
                this.btnSave.Enabled = false;
                this.btnNew.Enabled = false;
            }
            else
            {
                this.btnNew.Enabled = true;
                this.btnSave.Enabled = true;
            }
        }

        private void cbTable_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (omSelectedTabPage == "Basic Data")
            {
                this.btnSave.Enabled = false;
                this.btnNew.Enabled = false;
            }
            else
            {
                this.btnNew.Enabled = true;
                this.btnSave.Enabled = true;
            }
        }

        private void pictureBox4_Paint(object sender, PaintEventArgs e)
        {
            UT.DrawGradient(e.Graphics, 90f, pictureBox4, Color.AliceBlue, Color.LightSteelBlue);
        }

        private void rbFilterConflict_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbFilterConflict.Checked)
            {
                if (this.label1.ForeColor == Color.Red)
                {
                    this.label1.ForeColor = Color.Lime;
                }
                this.label1.Enabled = false;
                this.btnApply.Enabled = true;
                this.btnSave.Enabled = true;
                this.cbPosition.Checked = true;
                this.cbGate.Checked = true;
                this.cbExit.Checked = true;
                this.cbBelt.Checked = true;
                this.cbLounge.Checked = true;
                this.cbCheckin.Checked = true;
                this.cbRunway.Checked = true;
                this.btnApply.Enabled = true;
                this.btnSave.Enabled = true;
                this.cbPosition.Enabled = false;
                this.cbGate.Enabled = false;
                this.cbBelt.Enabled = false;
                this.cbLounge.Enabled = false;
                this.cbCheckin.Enabled = false;
                this.cbRunway.Enabled = false;
                this.cbExit.Enabled = false;
                this.txtFLNUValue.BackColor = Color.Yellow;
                this.txtALT3Value.BackColor = Color.Yellow;
                this.txtCallSignValue.BackColor = Color.Yellow;
                this.cbArrival.Visible = false;
                this.cbDeparture.Visible = false;
                this.cbTowing.Visible = false;
                this.btnALC.Enabled = true;
                this.btnACR.Enabled = false;
                this.btnACT.Enabled = false;
                this.btnNAT.Enabled = false;
                this.btnSTY.Enabled = false;
                this.txtSTYValue.Enabled = false;
                this.txtNATValue.Enabled = false;
                this.txtACTValue.Enabled = false;
                this.txtACT5Value.Enabled = false;
                this.txtREGNValue.Enabled = false;
                if (this.m_isConflict)
                {
                    this.cbConflict.Visible = false;
                    this.rbFilterConflict.Visible = true;
                }
            }
            else
            {
                this.cbPosition.Enabled = true;
                this.cbGate.Enabled = true;
                this.cbBelt.Enabled = true;
                this.cbLounge.Enabled = true;
                this.cbCheckin.Enabled = true;
                this.cbRunway.Enabled = true;
                this.cbExit.Enabled = true;
                this.cbPosition.Enabled = true;
                this.label1.Enabled = true;
                this.btnACR.Enabled = true;
                this.btnACT.Enabled = true;
                this.btnNAT.Enabled = true;
                this.btnSTY.Enabled = true;
                this.btnALC.Enabled = true;
                this.txtFLNUValue.BackColor = Color.White;
                this.txtALT3Value.BackColor = Color.White;
                this.txtCallSignValue.BackColor = Color.White;
                this.txtALTValue.Enabled = true;
                this.txtFLNUValue.Enabled = true;
                this.txtFLTSValue.Enabled = true;
                this.txtALT3Value.Enabled = true;
                this.txtCallSignValue.Enabled = true;
                this.txtSTYValue.Enabled = true;
                this.txtNATValue.Enabled = true;
                this.txtACTValue.Enabled = true;
                this.txtACT5Value.Enabled = true;
                this.txtREGNValue.Enabled = true;
            }


        }

        //private void groupBox1_Paint(object sender, PaintEventArgs e)
        //{
        //    UT.DrawGradient(e.Graphics, 90f, groupBox1, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);
        //}

        //private void groupBox2_Paint(object sender, PaintEventArgs e)
        //{
        //    UT.DrawGradient(e.Graphics, 90f, groupBox2, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);
        //}

       
    }
}
