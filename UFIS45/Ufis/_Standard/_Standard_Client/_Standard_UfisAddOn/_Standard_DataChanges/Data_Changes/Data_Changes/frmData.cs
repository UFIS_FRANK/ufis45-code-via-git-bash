using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

using Data_Changes.Ctrl;
using System.Collections.Generic;

namespace Data_Changes
{
	/// <summary>
	/// Summary description for frmData.
	/// </summary>
	public class frmData : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lblNAT;
		private System.Windows.Forms.Label lblSTY;
		private System.Windows.Forms.Label lblALT;
		private System.Windows.Forms.Label lblAFT;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label lblACR;
		private AxTABLib.AxTAB tabACR;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label lblACT;
		private AxTABLib.AxTAB tabACT;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Panel panel6;
		private AxTABLib.AxTAB tabALT;
		private AxTABLib.AxTAB tabNAT;
		private AxTABLib.AxTAB tabSTY;
		private System.Windows.Forms.Panel panel7;
		private AxTABLib.AxTAB tabAFT;
		private AxTABLib.AxTAB tabCCA;
		private System.Windows.Forms.Panel panel1;
		private AxTABLib.AxTAB tabSYS;
		//SISL
		private AxTABLib.AxTAB tabAFTCallSign;
		private System.Windows.Forms.Label lblCallSign;
		private System.Windows.Forms.Panel panel8;
				
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmData()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmData));
			this.lblALT = new System.Windows.Forms.Label();
			this.lblNAT = new System.Windows.Forms.Label();
			this.lblSTY = new System.Windows.Forms.Label();
			this.lblAFT = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.tabACR = new AxTABLib.AxTAB();
			this.lblACR = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.tabACT = new AxTABLib.AxTAB();
			this.lblACT = new System.Windows.Forms.Label();
			this.panel4 = new System.Windows.Forms.Panel();
			this.tabSTY = new AxTABLib.AxTAB();
			this.panel5 = new System.Windows.Forms.Panel();
			this.tabALT = new AxTABLib.AxTAB();
			this.panel6 = new System.Windows.Forms.Panel();
			this.tabNAT = new AxTABLib.AxTAB();
			this.panel7 = new System.Windows.Forms.Panel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.tabSYS = new AxTABLib.AxTAB();
			this.tabCCA = new AxTABLib.AxTAB();
			this.tabAFT = new AxTABLib.AxTAB();
			this.tabAFTCallSign = new AxTABLib.AxTAB();//SISL
			this.lblCallSign = new System.Windows.Forms.Label();
			this.panel8 = new System.Windows.Forms.Panel();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabACR)).BeginInit();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabACT)).BeginInit();
			this.panel4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabSTY)).BeginInit();
			this.panel5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabALT)).BeginInit();
			this.panel6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabNAT)).BeginInit();
			this.panel7.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabSYS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabCCA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAFT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAFTCallSign)).BeginInit();//SISL
			this.panel8.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblALT
			// 
			this.lblALT.Location = new System.Drawing.Point(0, 0);
			this.lblALT.Name = "lblALT";
			this.lblALT.Size = new System.Drawing.Size(64, 16);
			this.lblALT.TabIndex = 3;
			this.lblALT.Text = "ALT-Table";
			// 
			// lblNAT
			// 
			this.lblNAT.Location = new System.Drawing.Point(8, 0);
			this.lblNAT.Name = "lblNAT";
			this.lblNAT.Size = new System.Drawing.Size(64, 16);
			this.lblNAT.TabIndex = 5;
			this.lblNAT.Text = "NAT-Table";
			// 
			// lblSTY
			// 
			this.lblSTY.Location = new System.Drawing.Point(0, 8);
			this.lblSTY.Name = "lblSTY";
			this.lblSTY.Size = new System.Drawing.Size(64, 16);
			this.lblSTY.TabIndex = 7;
			this.lblSTY.Text = "STY-Table";
			// 
			// lblAFT
			// 
			this.lblAFT.Location = new System.Drawing.Point(88, 168);
			this.lblAFT.Name = "lblAFT";
			this.lblAFT.Size = new System.Drawing.Size(64, 16);
			this.lblAFT.TabIndex = 11;
			this.lblAFT.Text = "AFT-Table";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.tabACR);
			this.panel2.Controls.Add(this.lblACR);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 176);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(912, 56);
			this.panel2.TabIndex = 13;
			// 
			// tabACR
			// 
			this.tabACR.ContainingControl = this;
			this.tabACR.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabACR.Location = new System.Drawing.Point(0, 0);
			this.tabACR.Name = "tabACR";
			this.tabACR.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabACR.OcxState")));
			this.tabACR.Size = new System.Drawing.Size(912, 56);
			this.tabACR.TabIndex = 11;
			// 
			// lblACR
			// 
			this.lblACR.Location = new System.Drawing.Point(0, 0);
			this.lblACR.Name = "lblACR";
			this.lblACR.Size = new System.Drawing.Size(64, 16);
			this.lblACR.TabIndex = 1;
			this.lblACR.Text = "ACR-Table";
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.tabACT);
			this.panel3.Controls.Add(this.lblACT);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 232);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(912, 72);
			this.panel3.TabIndex = 14;
			// 
			// tabACT
			// 
			this.tabACT.ContainingControl = this;
			this.tabACT.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabACT.Location = new System.Drawing.Point(0, 0);
			this.tabACT.Name = "tabACT";
			this.tabACT.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabACT.OcxState")));
			this.tabACT.Size = new System.Drawing.Size(912, 72);
			this.tabACT.TabIndex = 3;
			// 
			// lblACT
			// 
			this.lblACT.Location = new System.Drawing.Point(0, 8);
			this.lblACT.Name = "lblACT";
			this.lblACT.Size = new System.Drawing.Size(100, 16);
			this.lblACT.TabIndex = 2;
			this.lblACT.Text = "ACT-Table";
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.tabSTY);
			this.panel4.Controls.Add(this.lblSTY);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel4.Location = new System.Drawing.Point(0, 304);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(912, 64);
			this.panel4.TabIndex = 15;
			// 
			// tabSTY
			// 
			this.tabSTY.ContainingControl = this;
			this.tabSTY.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabSTY.Location = new System.Drawing.Point(0, 0);
			this.tabSTY.Name = "tabSTY";
			this.tabSTY.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabSTY.OcxState")));
			this.tabSTY.Size = new System.Drawing.Size(912, 64);
			this.tabSTY.TabIndex = 8;
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.tabALT);
			this.panel5.Controls.Add(this.lblALT);
			this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel5.Location = new System.Drawing.Point(0, 56);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(912, 64);
			this.panel5.TabIndex = 16;
			// 
			// tabALT
			// 
			this.tabALT.ContainingControl = this;
			this.tabALT.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabALT.Location = new System.Drawing.Point(0, 0);
			this.tabALT.Name = "tabALT";
			this.tabALT.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabALT.OcxState")));
			this.tabALT.Size = new System.Drawing.Size(912, 64);
			this.tabALT.TabIndex = 4;
			// 
			// panel6
			// 
			this.panel6.Controls.Add(this.tabNAT);
			this.panel6.Controls.Add(this.lblNAT);
			this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel6.Location = new System.Drawing.Point(0, 120);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(912, 56);
			this.panel6.TabIndex = 16;
			// 
			// tabNAT
			// 
			this.tabNAT.ContainingControl = this;
			this.tabNAT.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabNAT.Location = new System.Drawing.Point(0, 0);
			this.tabNAT.Name = "tabNAT";
			this.tabNAT.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabNAT.OcxState")));
			this.tabNAT.Size = new System.Drawing.Size(912, 56);
			this.tabNAT.TabIndex = 6;
			// 
			// panel7
			// 
			this.panel7.Controls.Add(this.panel1);
			this.panel7.Controls.Add(this.tabCCA);
			this.panel7.Controls.Add(this.tabAFT);
			this.panel7.Controls.Add(this.lblAFT);
			this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel7.Location = new System.Drawing.Point(0, 368);
			this.panel7.Name = "panel7";
			this.panel7.Size = new System.Drawing.Size(912, 238);
			this.panel7.TabIndex = 17;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.tabSYS);
			this.panel1.Location = new System.Drawing.Point(0, 72);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(784, 80);
			this.panel1.TabIndex = 17;
			// 
			// tabSYS
			// 
			this.tabSYS.ContainingControl = this;
			this.tabSYS.Dock = System.Windows.Forms.DockStyle.Top;
			this.tabSYS.Location = new System.Drawing.Point(0, 0);
			this.tabSYS.Name = "tabSYS";
			this.tabSYS.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabSYS.OcxState")));
			this.tabSYS.Size = new System.Drawing.Size(784, 72);
			this.tabSYS.TabIndex = 17;
			// 
			// tabCCA
			// 
			this.tabCCA.ContainingControl = this;
			this.tabCCA.Dock = System.Windows.Forms.DockStyle.Top;
			this.tabCCA.Location = new System.Drawing.Point(0, 0);
			this.tabCCA.Name = "tabCCA";
			this.tabCCA.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabCCA.OcxState")));
			this.tabCCA.Size = new System.Drawing.Size(912, 72);
			this.tabCCA.TabIndex = 16;
			// 
			// tabAFT
			// 
			this.tabAFT.ContainingControl = this;
			this.tabAFT.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.tabAFT.Location = new System.Drawing.Point(0, 94);
			this.tabAFT.Name = "tabAFT";
			this.tabAFT.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabAFT.OcxState")));
			this.tabAFT.Size = new System.Drawing.Size(912, 144);
			this.tabAFT.TabIndex = 13;
			// 
			// tabAFTCallSign //SISL
			// 
			this.tabAFTCallSign.ContainingControl = this;
			this.tabAFTCallSign.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabAFTCallSign.Location = new System.Drawing.Point(0, 0);
			this.tabAFTCallSign.Name = "tabAFTCallSign";
			this.tabAFTCallSign.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabAFTCallSign.OcxState")));
			this.tabAFTCallSign.Size = new System.Drawing.Size(912, 56);
			this.tabAFTCallSign.TabIndex = 18;
			// 
			// lblCallSign //SISL
			// 
			this.lblCallSign.Location = new System.Drawing.Point(0, 0);
			this.lblCallSign.Name = "lblCallSign";
			this.lblCallSign.Size = new System.Drawing.Size(64, 16);
			this.lblCallSign.TabIndex = 1;
			this.lblCallSign.Text = "AFT-Table";
			// 
			// panel8 //SISL
			// 
			this.panel8.Controls.Add(this.tabAFTCallSign);
			this.panel8.Controls.Add(this.lblCallSign);
			this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel8.Location = new System.Drawing.Point(0, 0);
			this.panel8.Name = "panel8";
			this.panel8.Size = new System.Drawing.Size(912, 56);
			this.panel8.TabIndex = 19;
			// 
			// frmData
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(912, 606);
			this.Controls.Add(this.panel7);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel6);
			this.Controls.Add(this.panel5);
			this.Controls.Add(this.panel8);//SISL
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmData";
			this.Text = "frmData";
			this.Load += new System.EventHandler(this.frmData_Load);
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabACR)).EndInit();
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabACT)).EndInit();
			this.panel4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabSTY)).EndInit();
			this.panel5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabALT)).EndInit();
			this.panel6.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabNAT)).EndInit();
			this.panel7.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabSYS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabCCA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAFT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAFTCallSign)).EndInit();//SISL
			this.panel8.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion


		/// <summary>
		/// Loads all data along the definition in the tab's tag.
		/// </summary>
		public void LoadData()
		{
			frmStartup startDlg = new frmStartup();
			startDlg.Show();
			startDlg.Refresh();

			IDatabase myDB = UT.GetMemDB();
			//ACTTAB
			frmStartup.GetThis().txtLoading.Text = "Loading ACTTAB: Aircraft Types";
			frmStartup.GetThis().txtLoading.Refresh();
			ITable myTable = myDB.Bind("ACT", "ACT", "URNO,IATA-Code,ICAO-Code,Describtion", "10,3,5,32", "URNO,ACT3,ACT5,ACFN");
			myTable.Load("ORDER BY ACT3");
			myTable.CreateIndex("URNO", "URNO");
			myTable.FillVisualizer(tabACT); 
			tabACT.AutoSizeByHeader = true;
			tabACT.AutoSizeColumns();
//			frmStartup.GetThis().txtLoading.Text = "ACTTAB " + myTable.Count + " Records loaded";
//			frmStartup.GetThis().txtLoading.Refresh();

			frmStartup.GetThis().txtLoading.Text = "Loading ALTTAB: Airline Types";
			frmStartup.GetThis().txtLoading.Refresh();
			myTable = myDB.Bind("ALT", "ALT", "URNO,IATA-Code,ICAO-Code,Describtion,Terminal,Cash-Customer", "10,3,5,32,3,3", "URNO,ALC2,ALC3,ALFN,TERM,CASH");
			myTable.Load("ORDER BY ALC2");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("ALC2","IATA-Code");
			myTable.CreateIndex("ALC3","ICAO-Code");
			myTable.FillVisualizer(tabALT); 
			tabALT.AutoSizeByHeader = true;
			tabALT.AutoSizeColumns();
//			frmStartup.GetThis().txtLoading.Text = "ALTTAB " + myTable.Count + " Records loaded";
//			frmStartup.GetThis().txtLoading.Refresh();

			frmStartup.GetThis().txtLoading.Text = "Loading NATTAB: Natures";
			frmStartup.GetThis().txtLoading.Refresh();
			myTable = myDB.Bind("NAT", "NAT", "URNO,Type,Describtion", "10,10,32", "URNO,TTYP,TNAM");
			myTable.Load("ORDER BY TTYP");
			myTable.CreateIndex("URNO", "URNO");
			myTable.FillVisualizer(tabNAT); 
			tabNAT.AutoSizeByHeader = true;
			tabNAT.AutoSizeColumns();
//			frmStartup.GetThis().txtLoading.Text = "NATTAB " + myTable.Count + " Records loaded";
//			frmStartup.GetThis().txtLoading.Refresh();
/*
			frmStartup.GetThis().txtLoading.Text = "Loading ACRTAB: Registrations";
			frmStartup.GetThis().txtLoading.Refresh();
			myTable = myDB.Bind("ACR", "ACR", "URNO,Registration,IATA-Code,ICAO-Code,Describtion", "10,12,3,5,32", "URNO,REGN,ACT3,ACT5,CNAM");
			myTable.Load("ORDER BY REGN");
			myTable.CreateIndex("URNO", "URNO");
			myTable.FillVisualizer(tabACR); 
			tabACR.AutoSizeByHeader = true;
			tabACR.AutoSizeColumns();
//			frmStartup.GetThis().txtLoading.Text = "ACRTAB " + myTable.Count + " Records loaded";
//			frmStartup.GetThis().txtLoading.Refresh();
*/
//			frmStartup.GetThis().txtLoading.Text = "STYTAB " + myTable.Count + " Records loaded";
//			frmStartup.GetThis().txtLoading.Refresh();

			frmStartup.GetThis().txtLoading.Text = "Loading SYSTAB: System Table";
			frmStartup.GetThis().txtLoading.Refresh();
			LoadSYSData();
/*
  			myTable = myDB.Bind("SYS", "SYS", "TANA,FINA,ADDI,LOGD", "100,100,100,100", "TANA,FINA,ADDI,LOGD");
			myTable.Load("WHERE TANA='AFT' OR TANA ='CCA' ORDER BY FINA");
			myTable.CreateIndex("FINA", "FINA");
			myTable.CreateIndex("ADDI", "ADDI");
			myTable.FillVisualizer(tabSYS); 
			tabSYS.AutoSizeByHeader = true;
			tabSYS.AutoSizeColumns();
*/			
			
			//myTable = myDB.Bind("AFT", "AFT", "URNO,FTYP,ADID,STOA,STOD,TIFA,TIFD,FLNO,FLTN,FLNS,ALC2,ALC3,ACT3,ACT5,TTYP,STYP,REGN", "10,2,2,14,14,14,14,10,10,2,3,5,3,5,10,10,10", "URNO,FTYP,ADID,STOA,STOD,TIFA,TIFD,FLNO,FLTN,FLNS,ALC2,ALC3,ACT3,ACT5,TTYP,STYP,REGN");
			//SISL
			myTable = myDB.Bind("AFT", "AFT", "URNO,CallSign,FTYP,ADID,STOA,STOD,TIFA,TIFD,FLNO,FLTN,FLNS,ALC2,ALC3,ACT3,ACT5,TTYP,STYP,REGN", "10,12,2,2,14,14,14,14,10,10,2,3,5,3,5,10,10,10", "URNO,CSGN,FTYP,ADID,STOA,STOD,TIFA,TIFD,FLNO,FLTN,FLNS,ALC2,ALC3,ACT3,ACT5,TTYP,STYP,REGN");	
			myTable.CreateIndex("URNO", "URNO");
		
			frmStartup.GetThis().txtLoading.Text = "Loading STYTAB: Service Types";
			frmStartup.GetThis().txtLoading.Refresh();
			myTable = myDB.Bind("STY","STY","URNO,Type,Description","10,2,30","URNO,STYP,SNAM");
			myTable.Load("ORDER BY STYP");
			myTable.CreateIndex("URNO", "URNO");
			myTable.FillVisualizer(this.tabSTY); 
			tabSTY.AutoSizeByHeader = true;
			tabSTY.AutoSizeColumns();

			startDlg.Close();
		}


        /// <summary>
        /// Load data for given information
        /// </summary>
        /// <param name="table">DBTable Name</param>
        /// <param name="name">Name of the table to display to user</param>
        /// <param name="fields">DB Field Names to load into table</param>
        /// <param name="colLength">Column Lengths</param>
        /// <param name="refFieldAft">Field Name where Flight URNO was kept</param>
        /// <param name="sort">Field Names to be sorted (coma separated)</param>
        /// <param name="index">Field Names to be indexed</param>
        /// <returns></returns>
		public bool LoadXXXData(string table, string name, string fields, string colLength, string refFieldAft, string sort, string index)
		{
			if (table.Length != 3 || fields.Length == 0 || refFieldAft.Length == 0)
				return false;

			frmMain.GetThis().lblLocations.Text = "Loading " + name + "... (" + table + ")";
			frmMain.GetThis().lblLocations.Refresh();

			IDatabase myDB = UT.GetMemDB();
			ITable myTableAft = myDB["AFT"];
			if(myTableAft.Count == 0)
			{
				MessageBox.Show(this, "Can't select table without Flights", "No Flights found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				frmMain.GetThis().lblLocations.Text = table + myTableAft.Count + " Records loaded";
				frmMain.GetThis().lblLocations.Refresh();
				return false;
			}

			ITable myTable = myDB.Bind(table, table, fields, colLength, fields);
			myTable.Clear();

			string whereFLNUIn = "";
			StringBuilder strFLNU = new StringBuilder(1000);
			strFLNU.Append("WHERE " + refFieldAft + " IN (");
			for( int i = 0; i < myTableAft.Count; i++)
			{
				if( (i % 300) == 0 )
				{
					strFLNU.Append(myTableAft[i]["URNO"] + ")");
					whereFLNUIn = strFLNU.ToString();
					myTable.Load(whereFLNUIn);
					strFLNU.Remove(0,strFLNU.Length);
					whereFLNUIn = "";
					strFLNU.Append("WHERE " + refFieldAft + " IN (");
				}
				else
				{
					if( i == (myTableAft.Count - 1))
						strFLNU.Append(myTableAft[i]["URNO"] + ")");
					else
						strFLNU.Append(myTableAft[i]["URNO"] + ",");
				}
			}
			whereFLNUIn = strFLNU.ToString();
			if(whereFLNUIn != "WHERE " + refFieldAft + "IN (")
				myTable.Load(whereFLNUIn);

			string [] Index = index.Split(',');
			for(int i = 0; i < Index.Length; i++)
			{
				myTable.CreateIndex(Index[i], Index[i]);
			}

			myTable.Sort(sort, true);
			tabCCA.ResetContent();
			tabCCA.LogicalFieldList = fields;
			myTable.FillVisualizer(tabCCA); 
			tabCCA.AutoSizeByHeader = true;
			tabCCA.AutoSizeColumns();
			tabCCA.Refresh();

			frmMain.GetThis().lblLocations.Text = table + " " + myTable.Count + " Records loaded";
			frmMain.GetThis().lblLocations.Refresh();

			return true;
		}

		public bool LoadCCAData(string txtCCAselection)
		{
			frmMain.GetThis().lblLocations.Text = "Loading Check-In ... (CCATAB)";
			frmMain.GetThis().lblLocations.Refresh();

			IDatabase myDB = UT.GetMemDB();
			ITable myTableAft = myDB["AFT"];
			if(txtCCAselection.StartsWith("WHERE FLNU IN") == false && myTableAft.Count == 0)
			{
				//MessageBox.Show(this, "Can't select Check-In Counters without Flights", "No Flights found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				//frmMain.GetThis().lblLocations.Text = "CCATAB " + myTableAft.Count + " Records loaded";
				//frmMain.GetThis().lblLocations.Refresh();
				return false;
			}

			ITable myTable = myDB.Bind("CCA", "CCA", "URNO,FLNO,CNAM,FLNU,STOD", "30,30,30,30,100", "URNO,FLNO,CKIC,FLNU,STOD");
			myTable.Clear();

			if (txtCCAselection.Equals(""))
				return false;

			if (txtCCAselection.StartsWith("WHERE FLNU IN"))
			{
				string [] strArrUREF;
				strArrUREF = txtCCAselection.Split('#');
				for(int i = 0; i < strArrUREF.Length; i++)
				{
					string whereFLNUIn = "";
					whereFLNUIn = strArrUREF[i];
					//				MessageBox.Show(this, mySelection, "Updating LOGATB", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					myTable.Load(whereFLNUIn);
				}
			}
			else
			{
				string whereFLNUIn = "";
				StringBuilder strFLNU = new StringBuilder(1000);
				strFLNU.Append("WHERE FLNU IN (");
				for( int i = 0; i < myTableAft.Count; i++)
				{
					if(i > 0 && (i % 300) == 0 )
					{
						strFLNU.Append(myTableAft[i]["URNO"] + ")");
						whereFLNUIn = strFLNU.ToString();
						myTable.Load(whereFLNUIn);
						strFLNU.Remove(0,strFLNU.Length);
						whereFLNUIn = "";
						strFLNU.Append("WHERE FLNU IN (");
					}
					else
					{
						if( i == (myTableAft.Count - 1))
							strFLNU.Append(myTableAft[i]["URNO"] + ")");
						else
							strFLNU.Append(myTableAft[i]["URNO"] + ",");
					}
				}
				whereFLNUIn = strFLNU.ToString();
				if(whereFLNUIn != "WHERE FLNU IN (")
					myTable.Load(whereFLNUIn);
			}

			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("FLNU", "FLNU");
			myTable.Sort("FLNU,CNAM", true);
			tabCCA.ResetContent();
			tabCCA.LogicalFieldList = "URNO,FLNO,CNAM,FLNU";
			myTable.FillVisualizer(tabCCA); 
			tabCCA.AutoSizeByHeader = true;
			tabCCA.AutoSizeColumns();
			tabCCA.Refresh();

			frmMain.GetThis().lblLocations.Text = "CCATAB " + myTable.Count + " Records loaded";
			frmMain.GetThis().lblLocations.Refresh();

			return true;
		}

		public bool LoadACRData()
		{
			IDatabase myDB = UT.GetMemDB();
			//AFTTAB
			ITable myTable = myDB.Bind("ACR", "ACR", "URNO,Registration,IATA-Code,ICAO-Code,Describtion", "10,12,3,5,32", "URNO,REGN,ACT3,ACT5,CNAM");
			myTable.Load("ORDER BY REGN");
			myTable.CreateIndex("URNO", "URNO");
			myTable.FillVisualizer(tabACR); 
			tabACR.AutoSizeByHeader = true;
			tabACR.AutoSizeColumns();
			return true;
		}
		//SISL
		public bool LoadAFTCallSignData(string txtAFTselection)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB.Bind("AFT", "AFT", "URNO,CallSign,IATA-Code,ICAO-Code", "10,12,3,5", "URNO,CSGN,ACT3,ACT5");
			txtAFTselection += " ORDER BY CSGN";
			myTable.Load(txtAFTselection);
			myTable.CreateIndex("URNO", "URNO");
			myTable.FillVisualizer(tabAFTCallSign); 
			tabAFT.AutoSizeByHeader = true;
			tabAFT.AutoSizeColumns();
			return true;
		}

		public bool LoadSYSData()
		{
			IDatabase myDB = UT.GetMemDB();
			//AFTTAB
			ITable myTable = myDB.Bind("SYS", "SYS", "TANA,FINA,ADDI,LOGD", "100,100,100,100", "TANA,FINA,ADDI,LOGD");
			myTable.Load("WHERE TANA='AFT' OR TANA ='CCA' ORDER BY FINA");
			myTable.CreateIndex("FINA", "FINA");
			myTable.CreateIndex("ADDI", "ADDI");
			myTable.CreateIndex("TANA", "TANA");
			myTable.FillVisualizer(tabSYS); 
			tabSYS.AutoSizeByHeader = true;
			tabSYS.AutoSizeColumns();
			return true;
		}

		public bool LoadAFTData(string txtAFTselection)
		{
			frmMain.GetThis().lblLocations.Text = "Loading Flights ... (AFTTAB)";
			frmMain.GetThis().lblLocations.Refresh();


			IDatabase myDB = UT.GetMemDB();
			//AFTTAB
			ITable myTable = myDB.Bind("AFT", "AFT", "URNO,FTYP,ADID,STOA,STOD,TIFA,TIFD,FLNO,FLTN,FLNS,ALC2,ALC3,ACT3,ACT5,TTYP,STYP,REGN", "10,2,2,14,14,14,14,10,10,2,3,5,3,5,10,10,10", "URNO,FTYP,ADID,STOA,STOD,TIFA,TIFD,FLNO,FLTN,FLNS,ALC2,ALC3,ACT3,ACT5,TTYP,STYP,REGN");
//			ITable myTable = myDB["AFT"];
			myTable.Clear();

			if (txtAFTselection.Equals(""))
				return false;
        //    MessageBox.Show(txtAFTselection+"here");
			myTable.Load(txtAFTselection);
			myTable.CreateIndex("URNO", "URNO");
			myTable.Sort("ADID,FLTN", true);
			tabAFT.ResetContent();
			tabAFT.LogicalFieldList = "URNO,FTYP,ADID,STOA,STOD,TIFA,TIFD,FLNO,FLTN,FLNS,ALC2,ALC3,ACT3,ACT5,TTYP,STYP,REGN";

			int idx = 0;
			idx = UT.GetItemNo(tabAFT.LogicalFieldList, "TIFA");
			tabAFT.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD'.'MM'.'YYYY");
			idx = UT.GetItemNo(tabAFT.LogicalFieldList, "TIFD");
			tabAFT.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD'.'MM'.'YYYY");
			idx = UT.GetItemNo(tabAFT.LogicalFieldList, "STOA");
			tabAFT.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD'.'MM'.'YYYY");
			idx = UT.GetItemNo(tabAFT.LogicalFieldList, "STOD");
			tabAFT.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD'.'MM'.'YYYY");

			myTable.FillVisualizer(tabAFT); 
			tabAFT.AutoSizeByHeader = true;
			tabAFT.AutoSizeColumns();
			tabAFT.Refresh();

			frmMain.GetThis().lblLocations.Text = "AFTTAB " + myTable.Count + " Records loaded";
			frmMain.GetThis().lblLocations.Refresh();

			if(myTable.Count == 0)
			{
				return false;
			}

			return true;
		}
		private void frmData_Load(object sender, System.EventArgs e)
		{
		
		}


	}
}
