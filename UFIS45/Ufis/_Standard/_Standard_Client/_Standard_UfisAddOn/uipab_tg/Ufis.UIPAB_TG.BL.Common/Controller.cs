using System;
using System.Collections;
using System.Data;


using Microsoft.ApplicationBlocks.UIProcess;
using Ufis.UIPAB_TG.BL.Services;
using Ufis.UIPAB_TG.BL.Entities;
using Ufis.Utils;

namespace Ufis.UIPAB_TG.BL.Common
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	
	enum KindOfService
	{
		BasicService = 1, WebService = 2
	}

	public class Controller : ControllerBase
	{
		static Basic					myBasicService;
		static TG						myTGService;
		static UfisWS.UfisWebService	myBasicWebservice;
		KindOfService			localkindOfService;
	

		public Controller(Navigator navi) : base(navi)
		{
			/// <summary>
			/// Lokale Services werden auf Webservices oder BasicServices geeicht 
			/// </summary>

			this.SwitchKindOfService();
			 
			if(localkindOfService == KindOfService.WebService)
			{
				myBasicWebservice = new UfisWS.UfisWebService();
				myBasicWebservice.InitializeBasicService();
			}
			if(localkindOfService == KindOfService.BasicService && myBasicService == null)
			{
				myBasicService = new Basic(true);
				DateTime from  = new DateTime(2004,02,07,0,0,0);
				DateTime to    = new DateTime(2004,02,08,0,0,0);

				myTGService	   = new TG(myBasicService,from,to);
			}
		}


		private void SwitchKindOfService()
		{
		IniFile iniFile = new IniFile("c:\\ufis\\system\\ceda.ini");
		string kindOfService		= iniFile.IniReadValue("Global","Service");

		switch(kindOfService)
		{
		case "WebService":
				localkindOfService = KindOfService.WebService;
				break;

		case "BasicService":
				localkindOfService = KindOfService.BasicService;
				break;

		default:
				localkindOfService = KindOfService.BasicService;
				break;
		}
	
		}


		public void SelectService(string serviceName)
		{
			this.State.NavigateValue = serviceName;
			this.Navigate();
		}


		/*		public bool LoginToServer(string username,string password,out string errorMessage)
				{
					return myBasicService.LoginToServer("SIN1","SIN","TAB",username,password,"UIPAB_TG",out errorMessage);
				}
		*/
		/// <summary>
		/// Sorgt f�r das Login auf dem Server mit den �bergebenen Parametern
		/// </summary>
		/// <param name="pDatabase"></param>
		/// <param name="pUser"></param>
		/// <param name="pRole"></param>
		/// <param name="pPassword"></param>
		/// <returns></returns>
		/// 
		public bool LoginToServer(string username,string password,out string loginError)
		{
			LoginData();

			switch(this.localkindOfService)
			{
				case KindOfService.BasicService:
					return myBasicService.LoginToServer(this.State["LOGIN_DATABASE"].ToString(),
														this.State["LOGIN_HOPO"].ToString(),
														this.State["LOGIN_TABEXT"].ToString(),
															username,password,"UIPAB_TG",out loginError);
					
				case KindOfService.WebService:
					return myBasicWebservice.LoginToServer(this.State["LOGIN_DATABASE"].ToString(),
														this.State["LOGIN_HOPO"].ToString(),
														this.State["LOGIN_TABEXT"].ToString(),
														username,password,"UIPAB_TG",out loginError);

				default:
					return myBasicService.LoginToServer(this.State["LOGIN_DATABASE"].ToString(),
														this.State["LOGIN_HOPO"].ToString(),
														this.State["LOGIN_TABEXT"].ToString(),
														username,password,"UIPAB_TG",out loginError);
			}
		}

		
		public bool GetFlightData(out Entities.TG_Flight flight)
		{
			switch(this.localkindOfService)
			{
				case KindOfService.BasicService:
					return myTGService.GetFlightData(out flight);

				case KindOfService.WebService:
					flight = new TG_Flight();
					return true;
					//return  myBasicWebservice.GetFlightData(out  flight);

				default:
					return myTGService.GetFlightData(out flight);
			}
		}


		public void LoginData()
		{
			ArrayList values;
			
			switch(this.localkindOfService)
			{
				case KindOfService.BasicService:
					values = myBasicService.GetValuesFromIniFile();
					break;

				case KindOfService.WebService:
					values = new ArrayList(myBasicWebservice.GetValuesFromIniFile());
					break;

				default:
					values = myBasicService.GetValuesFromIniFile();
					break;
			}

			if (this.State.Contains("LOGIN_DATABASE"))
			{
				this.State["LOGIN_DATABASE"] = values[0];
			}
			else
			{
				this.State.Add("LOGIN_DATABASE",values[0]);
			}


			if (this.State.Contains("LOGIN_HOPO"))
			{
				this.State["LOGIN_HOPO"] = values[1];
			}
			else
			{
				this.State.Add("LOGIN_HOPO",values[1]);
			}


			if (this.State.Contains("LOGIN_TABEXT"))
			{
				this.State["LOGIN_TABEXT"] = values[2];
			}
			else
			{
				this.State.Add("LOGIN_TABEXT",values[2]);
			}
		}


		public DataRow CurrentFlight
		{
			get
			{
				if (this.State.Contains("CurrentFlightRow"))
				{
					return (DataRow) this.State["CurrentFlightRow"];
				}
				else
				{
					return null;
				}
			}
			set
			{
				if (this.State.Contains("CurrentFlightRow"))
				{
					this.State["CurrentFlightRow"] = value;
				}
				else
				{
					this.State.Add("CurrentFlightRow",value);
				}
			}
		}


		public bool LoadDatabase()
		{
			switch(this.localkindOfService)
			{
				case KindOfService.BasicService :
					return myTGService.LoadDatabase();

				case KindOfService.WebService :
					return true;
					//	return myBasicWebservice.LoadDatabase();

				default:
					return myTGService.LoadDatabase();
			}
			
		}


		public bool GetBusinessCaseData(string businessCase,out DataSet ds)
		{
			switch(this.localkindOfService)
			{
				case KindOfService.BasicService:
					return myTGService.GetBusinessCaseData(businessCase,out ds);

				case KindOfService.WebService:
					ds = new DataSet();
					return true;
				
					//return myBasicWebservice.GetBusinessCaseData(businessCase,out ds);

				default:
					return myTGService.GetBusinessCaseData(businessCase,out ds);
			}	
		}


		public bool Validate(DataRow row,out ArrayList errorMessages)
		{
			switch(this.localkindOfService)
			{
				case KindOfService.BasicService:
					return myTGService.Validate(row,out errorMessages);

				case KindOfService.WebService:
					errorMessages = new ArrayList();
					return true;
					//return myBasicWebservice.Validate(row,out errorMessages);

				default:
					return myTGService.Validate(row,out errorMessages);
			}

		}


		public bool Save(DataSet ds,out ArrayList errorMessages)
		{
			switch(this.localkindOfService)
			{
				case KindOfService.BasicService:
					return myTGService.Save(ds,out errorMessages);

				case KindOfService.WebService:
					errorMessages = new ArrayList();
					return true;
					// return myBasicWebservice.Save( ds,out errorMessages);

				default:
					return myTGService.Save(ds,out errorMessages);
			}


		}


		public bool GetBusinessDataPattern(DataColumn column,out string pattern)
		{
			switch(this.localkindOfService)
			{
				case KindOfService.BasicService:
					return myTGService.GetBusinessDataPattern(column,out pattern);

				case KindOfService.WebService:
					pattern = "huhu";
					return true;

					// return myBasicWebservice.GetBusinessDataPattern(column,out pattern);;

				default:
					return myTGService.GetBusinessDataPattern(column,out pattern);
			}
			
		}


		public bool InsertRow(DataTable dt,out ArrayList errorMessages)
		{
			switch(this.localkindOfService)
			{
				case KindOfService.BasicService:
					if (myTGService.InsertRow(dt,out errorMessages))
					{
						DataRow[] rows = dt.Select("","",DataViewRowState.Added);
						if (rows.Length == 1)
						{
							this.CurrentFlight = rows[0];
							SelectService("Insert");
							return true;
						}
						else
							return false;
					}
					else
						return false;

				case KindOfService.WebService:
					errorMessages = new ArrayList();
					return true;
					//return myBasicWebservice.InsertRow(dt,out  errorMessages);

				default:
					if (myTGService.InsertRow(dt,out errorMessages))
					{
						DataRow[] rows = dt.Select("","",DataViewRowState.Added);
						if (rows.Length == 1)
						{
							this.CurrentFlight = rows[0];
							SelectService("Insert");
							return true;
						}
						else
							return false;
					}
					else
						return false;
			}


		}


		public bool DeleteRow(DataRow row,out ArrayList errorMessages)
		{
			switch(this.localkindOfService)
			{
				case KindOfService.BasicService:
					return myTGService.DeleteRow(row,out errorMessages);
				
				case KindOfService.WebService:
					errorMessages = new ArrayList();
					return true;
					//return myBasicWebservice.DeleteRow(row,out errorMessages);;

				default:
					return myTGService.DeleteRow(row,out errorMessages);
			}
		}


		public bool DisposeApplication()
		{
			switch(this.localkindOfService)
			{
				case KindOfService.BasicService:
					return myBasicService.DisposeApplication();
				
				case KindOfService.WebService:
					return true;
					//return myBasicWebservice.DisposeApplication();

				default:
					return myBasicService.DisposeApplication();
			}
		}
	}
}
