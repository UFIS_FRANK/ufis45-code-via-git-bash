using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using Ufis.UIPAB_TG.BL.Services;

namespace Ufis.UIPAB_TG.BL.WebService 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		private System.ComponentModel.IContainer components = null;
		static	 Basic myBasicService;
		static   TG myTGService;

		public Global()
		{
			InitializeComponent();
		}	
		

		protected void Application_Start(Object sender, EventArgs e)
		{
			if(myBasicService == null)
			{
				myBasicService		 = new Basic(true);
				DateTime from		 = new DateTime(2004,02,07,0,0,0);
				DateTime to		     = new DateTime(2004,02,08,0,0,0);

				myTGService			 = new TG(myBasicService,from,to);

				Application["myBasic"]   = myBasicService;
				Application["myTG"]      = myTGService;	
			}
		}
 

		protected void Session_Start(Object sender, EventArgs e)
		{
	
		}


		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}


		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}


		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		
		protected void Application_Error(Object sender, EventArgs e)
		{

		}

	
		protected void Session_End(Object sender, EventArgs e)
		{

		}

	
		protected void Application_End(Object sender, EventArgs e)
		{

		}
			
		
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

