using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using Ufis.UIPAB_TG.BL.Services;
using Ufis.UIPAB_TG.BL.Entities;

namespace Ufis.UIPAB_TG.BL.WebService
{
	[WebService(Namespace="http://microsoft.com/webservices/")]
	public class UfisWebService : System.Web.Services.WebService
	{
		Basic		myBasicService = null;
		TG			myTGService	= null;

		DataSet		myBusinessCaseDataSet;

		public UfisWebService()
		{
			InitializeComponent();
		}


		#region Component Designer generated code
		
			private IContainer components = null;
				

		private void InitializeComponent()
		{
		}

		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
	#endregion

		#region Fertige 3 Methode

		[WebMethod]
		public void InitializeBasicService()
		{
			myBasicService		 = new Basic(true);
			DateTime from		 = new DateTime(2004,02,07,0,0,0);
			DateTime to		     = new DateTime(2004,02,08,0,0,0);

			myTGService			 = new TG(myBasicService,from,to);

			Session["myBasic"]   = myBasicService;
			Session["myTG"]      = myTGService;	
		}


		[WebMethod]
		public bool LoginToServer(string database, string hopo, string tabExt, string username, string password,string appName,out string errorMessage)
		{
			Basic myBasicService = (Basic) Application["myBasic"];
			return myBasicService.LoginToServer(database,hopo,tabExt,username,password,appName,out errorMessage);
		}
	
	
		[WebMethod]
		public ArrayList GetValuesFromIniFile()
		{
			Basic myBasicService = (Basic) Application["myBasic"];
			return myBasicService.GetValuesFromIniFile();
		}


		#endregion

		#region Baustelle 9 Methoden
/*		[WebMethod]
		public bool LoadDatabase()
		{
			return myTGService.LoadDatabase();
		}


		[WebMethod]
		public bool GetFlightData(out Entities.TG_Flight flight)
		{
			return myTGService.GetFlightData(out flight);
		}


		[WebMethod]
		public bool GetBusinessCaseData(string businessCase,out DataSet ds)
		{
			return myTGService.GetBusinessCaseData(businessCase,out ds);
		}


		[WebMethod]
		public bool Validate(DataRow row,out ArrayList errorMessages)
		{
			return myTGService.Validate(row,out errorMessages);
		}
		

		[WebMethod]
		public bool Save(DataSet ds,out ArrayList errorMessages)
		{
			return myTGService.Save(ds,out errorMessages);
		}
		

		[WebMethod]
		public bool GetBusinessDataPattern(DataColumn column,out string pattern)
		{
			return myTGService.GetBusinessDataPattern(column,out pattern);
		}


		[WebMethod]
		public bool InsertRow(DataTable dt,out ArrayList errorMessages)
		{
			if (myTGService.InsertRow(dt,out errorMessages))
			{
				DataRow[] rows = dt.Select("","",DataViewRowState.Added);
				if (rows.Length == 1)
				{
					this.CurrentFlight = rows[0];
					SelectService("Insert");
					return true;
				}
				else
					return false;
			}
			else
				return false;
		}


		[WebMethod]
		public bool DeleteRow(DataRow row,out ArrayList errorMessages)
		{
			return myTGService.DeleteRow(row,out errorMessages);
		}


		[WebMethod]
		public bool DisposeApplication()
		{
			return myBasicService.DisposeApplication();
		}
	
*/
		#endregion
	}
}
