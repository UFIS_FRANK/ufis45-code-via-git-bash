using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Ufis.Utils;
using Ufis.UIPAB_TG.BL.Common;
using Ufis.UIPAB_TG.BL.Entities;

namespace Ufis.UIPAB_TG.Win
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FlightDetail : Microsoft.ApplicationBlocks.UIProcess.WindowsFormView //System.Windows.Forms.Form
	{
		#region my members 
			DataRow origRow;
			DataSet ds = null;//origRow.Table.DataSet;
			DataTable dtCounter = null;//ds.Tables["Counter"];
			DataTable dtFlight = null;
			DataTable dtAct = null;
			DataView counterView = null;
			bool	 initialized = false;
			//Binding dTimeBinding
		#endregion my members 

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox cbAcType;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private AxTABLib.AxTAB tabCounter;
		private System.Windows.Forms.TextBox txtUrno;
		private Ufis.UIPAB_TG.Win.TextBoxRegex txtFlno;
		private System.Windows.Forms.DateTimePicker dtpTif;
		private System.Windows.Forms.ComboBox cbAdid;
		private System.Windows.Forms.DataGrid gridCounter;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.ErrorProvider errorProvider1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FlightDetail()
		{
			InitializeComponent();
		}



		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FlightDetail));
			this.txtUrno = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtFlno = new Ufis.UIPAB_TG.Win.TextBoxRegex();
			this.label3 = new System.Windows.Forms.Label();
			this.cbAcType = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.dtpTif = new System.Windows.Forms.DateTimePicker();
			this.label5 = new System.Windows.Forms.Label();
			this.cbAdid = new System.Windows.Forms.ComboBox();
			this.tabCounter = new AxTABLib.AxTAB();
			this.gridCounter = new System.Windows.Forms.DataGrid();
			this.btnClose = new System.Windows.Forms.Button();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider();
			((System.ComponentModel.ISupportInitialize)(this.tabCounter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridCounter)).BeginInit();
			this.SuspendLayout();
			// 
			// txtUrno
			// 
			this.txtUrno.Location = new System.Drawing.Point(104, 20);
			this.txtUrno.Name = "txtUrno";
			this.txtUrno.TabIndex = 0;
			this.txtUrno.Tag = "Id";
			this.txtUrno.Text = "textBox1";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Flight Id:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 52);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 16);
			this.label2.TabIndex = 3;
			this.label2.Text = "Flight No.:";
			// 
			// txtFlno
			// 
			this.txtFlno.BackColor = System.Drawing.Color.LightPink;
			this.txtFlno.ErrorProvider = null;
			this.txtFlno.InvalidTextBackColor = System.Drawing.Color.LightPink;
			this.txtFlno.Location = new System.Drawing.Point(104, 48);
			this.txtFlno.Name = "txtFlno";
			this.txtFlno.Pattern = Ufis.UIPAB_TG.Win.TextBoxRegex.Patterns.None;
			this.txtFlno.PatternString = "";
			this.txtFlno.TabIndex = 2;
			this.txtFlno.Tag = "FlightNumber";
			this.txtFlno.Text = "textBox2";
			this.txtFlno.UseColors = true;
			this.txtFlno.UseInvalidTextException = true;
			this.txtFlno.ValidTextBackColor = System.Drawing.Color.LightGreen;
			this.txtFlno.TextChanged += new System.EventHandler(this.txtFlno_TextChanged);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 84);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 16);
			this.label3.TabIndex = 4;
			this.label3.Text = "A/C Type:";
			// 
			// cbAcType
			// 
			this.cbAcType.Location = new System.Drawing.Point(104, 80);
			this.cbAcType.Name = "cbAcType";
			this.cbAcType.Size = new System.Drawing.Size(121, 21);
			this.cbAcType.TabIndex = 5;
			this.cbAcType.Tag = "AcCode";
			this.cbAcType.Text = "comboBox1";
			this.cbAcType.TextChanged += new System.EventHandler(this.cbAcType_TextChanged);
			this.cbAcType.SelectedIndexChanged += new System.EventHandler(this.cbAcType_SelectedIndexChanged);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 112);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(68, 16);
			this.label4.TabIndex = 6;
			this.label4.Text = "Actual Time:";
			// 
			// dtpTif
			// 
			this.dtpTif.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpTif.Location = new System.Drawing.Point(104, 108);
			this.dtpTif.Name = "dtpTif";
			this.dtpTif.Size = new System.Drawing.Size(120, 20);
			this.dtpTif.TabIndex = 7;
			this.dtpTif.Tag = "ActualTime";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 140);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 16);
			this.label5.TabIndex = 8;
			this.label5.Text = "Flight Type:";
			// 
			// cbAdid
			// 
			this.cbAdid.DisplayMember = "A";
			this.cbAdid.Location = new System.Drawing.Point(104, 136);
			this.cbAdid.Name = "cbAdid";
			this.cbAdid.Size = new System.Drawing.Size(121, 21);
			this.cbAdid.TabIndex = 9;
			this.cbAdid.Tag = "Type";
			// 
			// tabCounter
			// 
			this.tabCounter.Location = new System.Drawing.Point(12, 188);
			this.tabCounter.Name = "tabCounter";
			this.tabCounter.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabCounter.OcxState")));
			this.tabCounter.Size = new System.Drawing.Size(492, 172);
			this.tabCounter.TabIndex = 10;
			this.tabCounter.Tag = "Counter";
			// 
			// gridCounter
			// 
			this.gridCounter.DataMember = "";
			this.gridCounter.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.gridCounter.Location = new System.Drawing.Point(16, 380);
			this.gridCounter.Name = "gridCounter";
			this.gridCounter.Size = new System.Drawing.Size(488, 172);
			this.gridCounter.TabIndex = 11;
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(223, 568);
			this.btnClose.Name = "btnClose";
			this.btnClose.TabIndex = 12;
			this.btnClose.Text = "Close";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// FlightDetail
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(520, 602);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.gridCounter);
			this.Controls.Add(this.tabCounter);
			this.Controls.Add(this.cbAdid);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.dtpTif);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.cbAcType);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtFlno);
			this.Controls.Add(this.txtUrno);
			this.Controls.Add(this.label1);
			this.Name = "FlightDetail";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Flight Detail ...";
			this.TopMost = true;
			this.Load += new System.EventHandler(this.FlightDetail_Load);
			this.Closed += new System.EventHandler(this.FlightDetail_Closed);
			((System.ComponentModel.ISupportInitialize)(this.tabCounter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridCounter)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void FlightDetail_Closed(object sender, System.EventArgs e)
		{
			MyController.SelectService("Previous");
		}

		private void FlightDetail_Load(object sender, System.EventArgs e)
		{
			origRow = MyController.CurrentFlight;
			ds = origRow.Table.DataSet;
			dtCounter = ds.Tables["Counter"];
			dtFlight = ds.Tables["Flight"];
			dtAct = ds.Tables["AircraftTypes"];

			PrepareDataControls();
			FillCounters();
			try
			{
				string strFilter = "FlightId = '" + origRow["Id"] + "'";
				counterView = new DataView(dtCounter, strFilter, "Begin ASC", DataViewRowState.CurrentRows);
				gridCounter.DataSource = counterView;
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			this.initialized = true;
		}

		private void PrepareDataControls()
		{
			dtpTif.CustomFormat = "dd.MM.yyyy - HH:mm";

			try
			{
				DateTime datTif = UT.CedaFullDateToDateTime(origRow["ActualTimeArrival"].ToString());
				
				string adidName = "";
				string[] adidNames = new String[3];
				Type type;
				

				if(origRow["Type"].ToString() != "")
				{
					byte valu = (byte)origRow["Type"];
					type = ds.Tables["Flight"].Columns["Type"].DataType;
					adidName = Enum.GetName(type,valu);
				} else {
					type = dtFlight.Columns["Type"].DataType;
				}

				adidNames  = Enum.GetNames(type);
				for (int j = 0; j < adidNames.Length; j++)
				{							
					this.cbAdid.Items.Add(adidNames[j]);
				}
			

				//if (adidName.StartsWith("D"))
					datTif = UT.CedaFullDateToDateTime(origRow["ActualTimeDeparture"].ToString());
#if OLDSTYLE
				this.txtUrno.DataBindings.Add( "Text", origRow/*dtFlight*/, "Id");
				this.cbAdid.DataBindings.Add("Text", origRow, "Type");
				this.txtFlno.DataBindings.Add("Text", origRow, "FlightNumber");
				this.dtpTif.Value = datTif;
				this.cbAcType.DataSource = dtAct;
				this.cbAcType.DisplayMember = "Code3";
				this.cbAcType.DataBindings.Add("Text", origRow, "AcCode");
#else
				this.txtUrno.DataBindings.Add( "Text", origRow["Id"]/*dtFlight*/, "");
				this.cbAdid.DataBindings.Add("Text", adidName, "");
				
				

				
				this.txtFlno.DataBindings.Add("Text", origRow["FlightNumber"], "");
				string pattern;
				if (MyController.GetBusinessDataPattern(origRow.Table.Columns["FlightNumber"],out pattern))
				{
					this.txtFlno.Pattern = TextBoxRegex.Patterns.RegexPattern;
					this.txtFlno.PatternString = pattern;
					this.txtFlno.ErrorProvider = this.errorProvider1;
				}
				this.dtpTif.Value = datTif;
				origRow["ActualTimeDeparture"] = UT.DateTimeToCeda(datTif);
				origRow["ActualTimeArrival"] = UT.DateTimeToCeda(datTif);

				this.cbAcType.DataSource = dtAct;
				this.cbAcType.DisplayMember = "Code3";
				this.cbAcType.DataBindings.Add("Text", origRow["AcCode"], "");
#endif
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message.ToString());
			}
			
		}

		private void FillCounters()
		{
			string header = "";
			string lens = "";
			for(int i = 0; i < dtCounter.Columns.Count; i++)
			{
				string colName = dtCounter.Columns[i].Caption;
				if(i+1 < dtCounter.Columns.Count)
				{
					header += colName + ",";
					if(colName == "Id" || colName == "FlightId")
						lens += "0,"; 
					else
						lens += "100,"; 
				}
				else
				{
					header += colName;
					if(colName == "Id" || colName == "FlightId")
						lens += "0"; 
					else
						lens += "100"; 
				}
			}
			tabCounter.ResetContent();
			tabCounter.HeaderString = header;
			tabCounter.LogicalFieldList = header;
			tabCounter.HeaderLengthString = lens;
			tabCounter.EnableHeaderSizing(true);
			string strSelect = "FlightId = '" + origRow["Id"] + "'";
			DataRow [] childRows = dtCounter.Select(strSelect);
			for(int i = 0; i < childRows.Length; i++)
			{
				string str="";
				for(int j = 0; j < dtCounter.Columns.Count; j++)
				{
					if( j+1 < dtCounter.Columns.Count)
						str += childRows[i][dtCounter.Columns[j]] + ",";
					else
						str += childRows[i][dtCounter.Columns[j]];
				}
				tabCounter.InsertTextLine(str, false);
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void cbAcType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (this.initialized == true && this.cbAcType.SelectedIndex >= 0)
			{
				this.origRow["AcCode"] = this.cbAcType.Text;
			}
		}

		private void txtFlno_TextChanged(object sender, System.EventArgs e)
		{
			this.origRow["FlightNumber"] = this.txtFlno.Text;
		}

		private void cbAcType_TextChanged(object sender, System.EventArgs e)
		{
		}

		private Controller MyController
		{
			get
			{
				return (Controller)base.Controller;
			}
		}

	}
}
