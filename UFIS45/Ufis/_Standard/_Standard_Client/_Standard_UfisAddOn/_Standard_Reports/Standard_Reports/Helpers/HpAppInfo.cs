﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Ufis.Utilities;

namespace Standard_Reports.Helpers
{
    class HpAppInfo
    {
        private static HpAppInfo _hpAppInfo;
        private static AppInfo _appInfo;

        private HpAppInfo()
            : this(null)
        {
        }

        private HpAppInfo(string productCode)
        {
            _appInfo = new AppInfo() { ProductCode = productCode,
            ProductName = Application.ProductName,
            ProductTitle = Application.ProductName,
            Version = Application.ProductVersion };
        }

        public static AppInfo Current
        {
            get
            {
                if (_hpAppInfo == null)
                    _hpAppInfo = new HpAppInfo();

                return _appInfo;
            }
        }
    }
}
