﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

namespace Standard_Reports.Helpers
{
    public class HpParameters : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyname)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
        }

        private string _myParameter = string.Empty;
        public string MyParameter
        {
            get
            {
                return _myParameter;
            }
            set
            {
                _myParameter = value;
                OnPropertyChanged("Parameter");
            }
        }
        private string _header = string.Empty;
        public string Header
        {
            get
            {
                return _header;
            }
            set
            {
                _header = value;
                OnPropertyChanged("Header");
            }
        }
        private string _ReportFooterString = string.Empty;
        public string ReportFooterString
        {
            get
            {
                return _ReportFooterString;
            }
            set
            {
                _ReportFooterString = value;
                OnPropertyChanged("ReportFooterString");
            }
        }

        private string _ScenarioString = string.Empty;
        public string ScenarioString
        {
            get
            {
                return _ScenarioString;
            }
            set
            {
                _ScenarioString = value;
                OnPropertyChanged("ScenarioString");
            }
        }
        private string _GeneratedBy = string.Empty;
        public string GeneratedBy
        {
            get
            {
                return _GeneratedBy;
            }
            set
            {
                _GeneratedBy = value;
                OnPropertyChanged("GeneratedBy");
            }
        }
        private string _ReportNoteString = string.Empty;
        public string ReportNoteString
        {
            get
            {
                return _ReportNoteString;
            }
            set
            {
                _ReportNoteString = value;
                OnPropertyChanged("ReportNoteString");
            }
        }
        private string _ReportTelephoneString = string.Empty;
        public string ReportTelephoneString
        {
            get
            {
                return _ReportTelephoneString;
            }
            set
            {
                _ReportTelephoneString = value;
                OnPropertyChanged("ReportTelephoneString");
            }
        }
        private string _ReportEmailString = string.Empty;
        public string ReportEmailString
        {
            get
            {
                return _ReportEmailString;
            }
            set
            {
                _ReportEmailString = value;
                OnPropertyChanged("ReportEmailString");
            }
        }
    }
}
