﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;

namespace Standard_Reports.Entities
{
    public class entlcApprovalReport : Entity
    {
        private int _number;
        public int Number
        {
            get
            {
                return _number;
            }
            set
            {
                if (_number != value)
                {
                    _number = value;
                    OnPropertyChanged("Number");
                }
            }
        }
        private string _arrivalDate;
        public string ArrivalDate
        {
            get
            {
                return _arrivalDate;
            }
            set
            {
                if (_arrivalDate != value)
                {
                    _arrivalDate = value;
                    OnPropertyChanged("ArrivalDate");
                }
            }
        }

        private string _operatorName = "";
        public string OperatorName
        {
            get
            {
                return _operatorName;
            }
            set
            {
                if (_operatorName != value)
                {
                    _operatorName = value;
                    OnPropertyChanged("OperatorName");
                }
            }
        }

        private string _ac = "";
        public string Ac
        {
            get
            {
                return _ac;
            }
            set
            {
                if (_ac != value)
                {
                    _ac = value;
                    OnPropertyChanged("Ac");
                }
            }
        }
        private string _registrationNumber = "";
        public string RegistrationNumber
        {
            get
            {
                return _registrationNumber;
            }
            set
            {
                if (_registrationNumber != value)
                {
                    _registrationNumber = value;
                    OnPropertyChanged("RegistrationNumber");
                }
            }
        }

        private string _aircraftIATACode = "";
        public string AircraftIATACode
        {
            get
            {
                return _aircraftIATACode;
            }
            set
            {
                if (_aircraftIATACode != value)
                {
                    _aircraftIATACode = value;
                    OnPropertyChanged(" AircraftIATACode");
                }
            }
        }

        private string _callSign = "";
        public string CallSign
        {
            get
            {
                return _callSign;
            }
            set
            {
                if (_callSign != value)
                {
                    _callSign = value;
                    OnPropertyChanged("CallSign");
                }
            }
        }

        private string _operationalType = "";
        public string OperationalType
        {
            get
            {
                return _operationalType;
            }
            set
            {
                 if (value == "D")
                        value = "Y";
                    else
                        value = "N";
                    _operationalType = value;
                    OnPropertyChanged("OperationalType");
             }
        }

        private string _originAirportICAOCode = "";
        public string OriginAirportICAOCode
        {
            get
            {
                return _originAirportICAOCode;
            }
            set
            {
                if (_originAirportICAOCode != value)
                {
                    _originAirportICAOCode = value;
                    OnPropertyChanged("OriginAirportICAOCode");
                }
            }
        }

        private string _bestEstimatedArrivalTime;
        public string BestEstimatedArrivalTime
        {
            get
            {
                return _bestEstimatedArrivalTime;
            }
            set
            {
                if (_bestEstimatedArrivalTime != value)
                {
                    _bestEstimatedArrivalTime = value;
                    OnPropertyChanged("BestEstimatedArrivalTime");
                }
            }
        }

        private string _estEstimatedDepartureTime;
        public string BestEstimatedDepartureTime
        {
            get
            {
                return _estEstimatedDepartureTime;
            }
            set
            {
                if (_estEstimatedDepartureTime != value)
                {
                    _estEstimatedDepartureTime = value;
                    OnPropertyChanged("BestEstimatedDepartureTime");
                }
            }
        }

        private string _standardTimeOfArrival;
        public string StandardTimeOfArrival
        {
            get
            {
                return _standardTimeOfArrival;
            }
            set
            {
                if (_standardTimeOfArrival != value)
                {
                    _standardTimeOfArrival = value;
                    OnPropertyChanged("StandardTimeOfArrival");
                }
            }
        }

        private string _standardTimeOfDeparture;
        public string StandardTimeOfDeparture
        {
            get
            {
                return _standardTimeOfDeparture;
            }
            set
            {
                if (_standardTimeOfDeparture != value)
                {
                    _standardTimeOfDeparture = value;
                    OnPropertyChanged("StandardTimeOfDeparture");
                }
            }
        }

        private string _destinationAirportICAOCode = "";
        public string DestinationAirportICAOCode
        {
            get
            {
                return _destinationAirportICAOCode;
            }
            set
            {
                if (_destinationAirportICAOCode != value)
                {
                    _destinationAirportICAOCode = value;
                    OnPropertyChanged("DestinationAirportICAOCode");
                }
            }
        }

        private string _flightPermitNumber = "";
        public string FlightPermitNumber
        {
            get
            {
                return _flightPermitNumber;
            }
            set
            {
                if (_flightPermitNumber != value)
                {
                    _flightPermitNumber = value;
                    OnPropertyChanged("FlightPermitNumber");
                }
            }
        }

        private string _flightPurpose = "";
        public string FlightPurpose
        {
            get
            {
                return _flightPurpose;
            }
            set
            {
                if (_flightPurpose != value)
                {
                    _flightPurpose = value;
                    OnPropertyChanged("FlightPurpose");
                }
            }
        }
    }
}
