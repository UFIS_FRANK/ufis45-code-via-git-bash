﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ufis.Entities;
using Ufis.IO;

namespace Standard_Reports.Entities
{
    public class entlcPermitLetter : Entity
    {
        private string _flightPermitNumber = "";
        public string FlightPermitNumber
        {
            get
            {
                return _flightPermitNumber;
            }
            set
            {
                if (_flightPermitNumber != value)
                {
                    _flightPermitNumber = value;
                    OnPropertyChanged("FlightPermitNumber");
                }
            }
        }


        private string _logo1;
        public string Logo1
        {
            get
            {
                _logo1 = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.UfisSystem);
                _logo1 = _logo1 + "\\images\\logo1.jpg";
                return _logo1;
            }
        }

        private string _logo2;
        public string Logo2
        {
            get
            {
                _logo2 = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.UfisSystem);
                _logo2 = _logo2 + "\\images\\logo2.jpg";
                return _logo2;
            }
        }

        private string _email = "";
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                if (_email != value)
                {
                    _email = value;
                    OnPropertyChanged("Email");
                }
            }
        }


        private string _agtName = "";
        public string AgtName
        {
            get
            {
                return _agtName;
            }
            set
            {
                if (_agtName != value)
                {
                    _agtName = value;
                    OnPropertyChanged("AgtName");
                }
            }
        }

        private string _callSign = "";
        public string CallSign
        {
            get
            {
                return _callSign;
            }
            set
            {
                if (_callSign != value)
                {
                    _callSign = value;
                    OnPropertyChanged("CallSign");
                }
            }
        }

        private string _registraionNo = "";
        public string RegistraionNo
        {
            get
            {
                return _registraionNo;
            }
            set
            {
                if (_registraionNo != value)
                {
                    _registraionNo = value;
                    OnPropertyChanged("RegistraionNo");
                }
            }
        }

        private string _contact;
        public string Contact
        {
            get
            {
                return _contact;
            }
            set
            {
                if (_contact != value)
                {
                    _contact = value;
                    OnPropertyChanged("Contact");
                }
            }
        }

        private string _arrDate;
        public string ArrDate
        {
            get
            {
                return _arrDate;
            }
            set
            {
                if (_arrDate != value)
                {
                    _arrDate = value;
                    OnPropertyChanged("ArrDate");
                }
            }
        }


        private string _arrTime;
        public string ArrTime
        {
            get
            {
                return _arrTime;
            }
            set
            {
                if (_arrTime != value)
                {
                    _arrTime = value;
                    OnPropertyChanged("ArrTime");
                }
            }
        }

        private string _depDate;
        public string DepDate
        {
            get
            {
                return _depDate;
            }
            set
            {
                if (_depDate != value)
                {
                    _depDate = value;
                    OnPropertyChanged("DepDate");
                }
            }
        }

        private string _depTime;
        public string DepTime
        {
            get
            {
                return _depTime;
            }
            set
            {
                if (_depTime != value)
                {
                    _depTime = value;
                    OnPropertyChanged("DepTime");
                }
            }
        }

        private string _approvalDate;
        public string ApprovalDate
        {
            get
            {
                return _approvalDate;
            }
            set
            {
                if (_approvalDate != value)
                {
                    _approvalDate = value;
                    OnPropertyChanged("ApprovalDate");
                }
            }
        }

        private string _operatorName = "";
        public string OperatorName
        {
            get
            {
                return _operatorName;
            }
            set
            {
                if (_operatorName != value)
                {
                    _operatorName = value;
                    OnPropertyChanged("OperatorName");
                }
            }
        }

        private string _requestDate;
        public string RequestDate
        {
            get
            {
                return _requestDate;
            }
            set
            {
                if (_requestDate != value)
                {
                    _requestDate = value;
                    OnPropertyChanged("RequestDate");
                }
            }
        }


        private string _validFrom;
        public string ValidFrom
        {
            get
            {
                return _validFrom;
            }
            set
            {
                if (_validFrom != value)
                {
                    _validFrom = value;
                    OnPropertyChanged("ValidFrom");
                }
            }
        }

        private string _validTo;
        public string ValidTo
        {
            get
            {
                return _validTo;
            }
            set
            {
                if (_validTo != value)
                {
                    _validTo = value;
                    OnPropertyChanged("ValidTo");
                }
            }
        }


        private string _purpose;
        public string Purpose
        {
            get
            {
                return _purpose;
            }
            set
            {
                if (_purpose != value)
                {
                    _purpose = value;
                    OnPropertyChanged("Purpose");
                }
            }
        }

        private string _aircraftIATACode;
        public string AircraftIATACode
        {
            get
            {
                return _aircraftIATACode;
            }
            set
            {
                if (_aircraftIATACode != value)
                {
                    _aircraftIATACode = value;
                    OnPropertyChanged("AircraftIATACode");
                }
            }
        }
        private string _origin;
        public string Origin
        {
            get
            {
                return _origin;
            }
            set
            {
                if (_origin != value)
                {
                    _origin = value;
                    OnPropertyChanged("Origin");
                }
            }
        }
        private string _destination;
        public string Destination
        {
            get
            {
                return _destination;
            }
            set
            {
                if (_destination != value)
                {
                    _destination = value;
                    OnPropertyChanged("Destination");
                }
            }
        }
        private string _requestBay = "";
        public string RequestBay
        {
            get
            {
                return _requestBay;
            }
            set
            {
                if (_requestBay != value)
                {
                    _requestBay = value;
                    OnPropertyChanged("RequestBay");
                }
            }
        }
    }
}
