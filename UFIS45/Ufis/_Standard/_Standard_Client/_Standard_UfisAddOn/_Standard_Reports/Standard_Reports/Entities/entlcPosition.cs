﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ufis.Entities;

namespace Standard_Reports.Entities
{
    public class entlcPosition : Entity
    {
        // Fields...
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (_name != value)

                    _name = value;
                OnPropertyChanged("Name");
            }
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
