﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;

namespace Standard_Reports.Entities
{
     public class entlcTowing : Entity
    {
         private bool _readyForTowing;
         public bool ReadyForTowing
         {
             get { return _readyForTowing; }
             set
             {
                 if (_readyForTowing != value)
                 {
                     _readyForTowing = value;
                     OnPropertyChanged("ReadyForTowing");
                 }
             }
         }

         private string _registrationNumber;
         public string RegistrationNumber
         {
             get { return _registrationNumber; }
             set
             {
                 if (_registrationNumber != value)
                 {
                     _registrationNumber = value;
                     OnPropertyChanged("RegistrationNumber");
                 }
             }
         }

         private string _aircraftIATACode;
         public string AircraftIATACode
         {
             get { return _aircraftIATACode; }
             set
             {
                 if (_aircraftIATACode != value)
                 {
                     _aircraftIATACode = value;
                     OnPropertyChanged("AircraftIATACode");
                 }
             }
         }

         private string _arrivalFullFlightNumber;
         public string ArrivalFullFlightNumber
         {
             get { return _arrivalFullFlightNumber; }
             set
             {
                 if (_arrivalFullFlightNumber != value)
                 {
                     _arrivalFullFlightNumber = value;
                     OnPropertyChanged("ArrivalFullFlightNumber");
                 }
             }
         }

         private string _departureFullFlightNumber;
         public string DepartureFullFlightNumber
         {
             get { return _departureFullFlightNumber; }
             set
             {
                 if (_departureFullFlightNumber != value)
                 {
                     _departureFullFlightNumber = value;
                     OnPropertyChanged("DepartureFullFlightNumber");
                 }
             }
         }

         private string _positionOfDeparture;
         public string PositionOfDeparture
         {
             get { return _positionOfDeparture; }
             set
             {
                 if (_positionOfDeparture != value)
                 {
                     _positionOfDeparture = value;
                     OnPropertyChanged("PositionOfDeparture");
                 }
             }
         }

         private string _positionOfArrival;
         public string PositionOfArrival
         {
             get { return _positionOfArrival; }
             set
             {
                 if (_positionOfArrival != value)
                 {
                     _positionOfArrival = value;
                     OnPropertyChanged("PositionOfArrival");
                 }
             }
         }

         private String _standardTimeOfDeparture;
         public String StandardTimeOfDeparture
         {
             get { return _standardTimeOfDeparture; }
             set
             {
                 if (_standardTimeOfDeparture != value)
                 {
                     _standardTimeOfDeparture = value;
                     OnPropertyChanged("StandardTimeOfDeparture");
                 }
             }
         }

         private String _standardTimeOfArrival;
         public String StandardTimeOfArrival
         {
             get { return _standardTimeOfArrival; }
             set
             {
                 if (_standardTimeOfArrival != value)
                 {
                     _standardTimeOfArrival = value;
                     OnPropertyChanged("StandardTimeOfArrival");
                 }
             }
         }

         private String _offblockTime;
         public virtual String OffblockTime
         {
             get { return _offblockTime; }
             set
             {
                 if (_offblockTime != value)
                 {
                     _offblockTime = value;
                     OnPropertyChanged("OffblockTime");
                 }
             }
         }

         private String _onblockTime;
         public virtual String OnblockTime
         {
             get { return _onblockTime; }
             set
             {
                 if (_onblockTime != value)
                 {
                     _onblockTime = value;
                     OnPropertyChanged("OnblockTime");
                 }
             }
         }

    }
}
