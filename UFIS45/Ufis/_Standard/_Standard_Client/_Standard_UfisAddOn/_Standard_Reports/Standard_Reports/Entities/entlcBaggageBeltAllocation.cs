﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : entlcBeltAllocation.cs

Version         : 1.0.0
Created Date    : 16 - Feb - 2012
Complete Date   : 16 - Feb - 2012
Created By      : Phyoe Khaing Min

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;

namespace Standard_Reports.Entities
{
    public class entlcBaggageBeltAllocation : Entity
    {
        private int _number;
        public int Number
        {
            get
            {
                return _number;
            }
            set
            {
                if (_number != value)
                {
                    _number = value;
                    OnPropertyChanged("Number");
                }
            }
        }


        private string _beltAllocated = "";
        public string BeltAllocated
        {
            get
            {
                return _beltAllocated;
            }
            set
            {
                if (_beltAllocated != value)
                {
                    _beltAllocated = value;
                    OnPropertyChanged("BeltAllocated");
                }
            }
        }


        private DateTime? _scheduledFirstBag;
        public DateTime? ScheduledFirstBag
        {
            get
            {
                return _scheduledFirstBag;
            }
            set
            {
                if (_scheduledFirstBag != value)
                {
                    _scheduledFirstBag = value;
                    OnPropertyChanged("ScheduledFirstBag");
                }
            }
        }

        private string _firstBag;
        public string FirstBag
        {
            get
            {
                return _firstBag;
            }
            set
            {
                if (_firstBag != value)
                {
                    _firstBag = value;
                    OnPropertyChanged("FirstBag");
                }
            }
        }


        private string _lastBag;
        public string LastBag
        {
            get
            {
                return _lastBag;
            }
            set
            {
                if (_lastBag != value)
                {
                    _lastBag = value;
                    OnPropertyChanged("LastBag");
                }
            }
        }


        private string _onChockTime;
        public string OnChockTime
        {
            get
            {
                return _onChockTime;
            }
            set
            {
                if (_onChockTime != value)
                {
                    _onChockTime = value;
                    OnPropertyChanged("OnChockTime");
                }
            }
        }

        private string _onBlockTime;
        public string OnBlockTime
        {
            get
            {
                return _onBlockTime;
            }
            set
            {
                if (_onBlockTime != value)
                {
                    _onBlockTime = value;
                    OnPropertyChanged("OnBlockTime");
                }
            }
        }
        private string _beltUsed = "";
        public string BeltUsed
        {
            get
            {
                return _beltUsed;
            }
            set
            {
                if (_beltUsed != value)
                {
                    _beltUsed = value;
                    OnPropertyChanged("BeltUsed");
                }
            }
        }


        private string _terminal = "";
        public string Terminal
        {
            get
            {
                return _terminal;
            }
            set
            {
                if (_terminal != value)
                {
                    _terminal = value;
                    OnPropertyChanged("Terminal");
                }
            }
        }

        private string _arrival = "";
        public string Arrival
        {
            get
            {
                return _arrival;
            }
            set
            {
                if (_arrival != value)
                {
                    _arrival = value;
                    OnPropertyChanged("Arrival");
                }
            }
        }


        private string _bay = "";
        public string Bay
        {
            get
            {
                return _bay;
            }
            set
            {
                if (_bay != value)
                {
                    _bay = value;
                    OnPropertyChanged("Bay");
                }
            }
        }

        private string _from = "";
        public string From
        {
            get
            {
                return _from;
            }
            set
            {
                if (_from != value)
                {
                    _from = value;
                    OnPropertyChanged("From");
                }
            }
        }


        private string _onblToFirstBag;
        public string OnblToFirstBag
        {
            get
            {
                return _onblToFirstBag;
            }
            set
            {
                if (_onblToFirstBag != value)
                {
                    _onblToFirstBag = value;
                    OnPropertyChanged("OnblToFirstBag");
                }
            }
        }


        private int _urno;
        public int Urno
        {
            get
            {
                return _urno;
            }
            set
            {
                if (_urno != value)
                {
                    _urno = value;
                    OnPropertyChanged("Urno");
                }
            }
        }

        private string _onblToLastBag;
        public string OnblToLastBag
        {
            get
            {
                return _onblToLastBag;
            }
            set
            {
                if (_onblToLastBag != value)
                {
                    _onblToLastBag = value;
                    OnPropertyChanged("OnblToLastBag");
                }
            }
        }

        private string _chonToFirstBag;
        public string ChonToFirstBag
        {
            get
            {
                return _chonToFirstBag;
            }
            set
            {
                if (_chonToFirstBag != value)
                {
                    _chonToFirstBag = value;
                    OnPropertyChanged("ChonToFirstBag");
                }
            }
        }

        private string _baggageDelayReason;
        public string BaggageDelayReason
        {
            get
            {
                return _baggageDelayReason;
            }
            set
            {
                if (_baggageDelayReason != value)
                {
                    _baggageDelayReason = value;
                    OnPropertyChanged("BaggageDelayReason");
                }
            }
        }

        private string _chonToLastBag;
        public string ChonToLastBag
        {
            get
            {
                return _chonToLastBag;
            }
            set
            {
                if (_chonToLastBag != value)
                {
                    _chonToLastBag = value;
                    OnPropertyChanged("ChonToLastBag");
                }
            }
        }



        private string _customRequest = "";
        public string CustomRequest
        {
            get
            {
                return _customRequest;
            }
            set
            {
                if (_customRequest != value)
                {
                    _customRequest = value;
                    OnPropertyChanged("CustomRequest");
                }
            }
        }


        private string _aircraftIATACode;
        public string AircraftIATACode
        {
            get
            {
                return _aircraftIATACode;
            }
            set
            {
                if (_aircraftIATACode != value)
                {
                    _aircraftIATACode = value;
                    OnPropertyChanged("AircraftIATACode");
                }
            }
        }

        private string _sta = "";
        public string STA
        {
            get
            {
                return _sta;
            }
            set
            {
                if (_sta != value)
                {
                    _sta = value;
                    OnPropertyChanged("STA");
                }
            }
        }

        private string _aPos = "";
        public string APos
        {
            get
            {
                return _aPos;
            }
            set
            {
                if (_aPos != value)
                {
                    _aPos = value;
                    OnPropertyChanged("APos");
                }
            }
        }

        private string _belt = "";
        public string Belt
        {
            get
            {
                return _belt;
            }
            set
            {
                if (_belt != value)
                {
                    _belt = value;
                    OnPropertyChanged("Belt");
                }
            }
        }

        private string _aBag;
        public string ABag
        {
            get
            {
                return _aBag;
            }
            set
            {
                if (_aBag != value)
                {
                    _aBag = value;
                    OnPropertyChanged("ABag");
                }
            }
        }

        private string _APax;
        public string APax
        {
            get
            {
                return _APax;
            }
            set
            {
                if (_APax != value)
                {
                    _APax = value;
                    OnPropertyChanged("APax");
                }
            }
        }

        private string _customRqst = "";
        public string CustomRqst
        {
            get
            {
                return _customRqst;
            }
            set
            {
                if (_customRqst != value)
                {
                    _customRqst = value;
                    OnPropertyChanged("CustomRqst");
                }
            }
        }
        private string _remarks = "";
        public string Remarks
        {
            get
            {
                return _remarks;
            }
            set
            {
                if (_remarks != value)
                {
                    _remarks = value;
                    OnPropertyChanged("Remarks");
                }
            }
        }

        private IList<Object> _natureNames = null;
        public virtual IList<Object> NatureNames
        {
            get
            {
                return _natureNames;
            }
            set
            {
                if (_natureNames != value)
                {
                    _natureNames = value;

                    OnPropertyChanged("NatureNames");
                }
            }
        }

        private IList<Object> _natureCodes = null;
        public virtual IList<Object> NatureCodes
        {
            get
            {
                return _natureCodes;
            }
            set
            {
                if (_natureCodes != value)
                {
                    _natureCodes = value;

                    OnPropertyChanged("NatureCodes");
                }
            }
        }
    }
    public class entlcNature : Entity
    {
        // Fields...
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (_name != value)

                    _name = value;
                OnPropertyChanged("Name");
            }
        }

        private string _code;
        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                if (_code != value)

                    _code = value;
                OnPropertyChanged("Code");
            }
        }


        public override string ToString()
        {
            return Code + "-" + Name;
        }
    }
}
