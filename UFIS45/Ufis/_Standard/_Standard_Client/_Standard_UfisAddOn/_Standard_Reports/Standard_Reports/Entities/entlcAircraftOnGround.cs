﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;

namespace Standard_Reports.Entities
{
    public class entlcAircraftOnGround : Entity
    {
        private string _registrationNumber = "";
        public string RegistrationNumber
        {
            get
            {
                return _registrationNumber;
            }
            set
            {
                if (_registrationNumber != value)
                {
                    _registrationNumber = value;
                    OnPropertyChanged("RegistrationNumber");
                }
            }
        }

        private string _aircraftIATACode = "";
        public string AircraftIATACode
        {
            get
            {
                return _aircraftIATACode;
            }
            set
            {
                if (_aircraftIATACode != value)
                {
                    _aircraftIATACode = value;
                    OnPropertyChanged(" AircraftIATACode");
                }
            }
        }
        private string _callSign = "";
        public string CallSign
        {
            get
            {
                return _callSign;
            }
            set
            {
                if (_callSign != value)
                {
                    _callSign = value;
                    OnPropertyChanged("CallSign");
                }
            }
        }

        private string _landingTime;
        public string LandingTime
        {
            get
            {
                return _landingTime;
            }
            set
            {
                if (_landingTime != value)
                {
                    _landingTime = value;
                    OnPropertyChanged("LandingTime");
                }
            }
        }

        private string _operatorName = "";
        public string OperatorName
        {
            get
            {
                return _operatorName;
            }
            set
            {
                if (_operatorName != value)
                {
                    _operatorName = value;
                    OnPropertyChanged("OperatorName");
                }
            }
        }

        private string _operationalType = "";
        public string OperationalType
        {
            get
            {
                return _operationalType;
            }
            set
            {              
                    if (value == "D")
                        value = "Yes";
                    else value = "No";
                    _operationalType = value;
                    OnPropertyChanged("OperationalType");              
            }
        }

        private string _flightPermitNumber = "";
        public string FlightPermitNumber
        {
            get
            {
                return _flightPermitNumber;
            }
            set
            {
                if (_flightPermitNumber != value)
                {
                    _flightPermitNumber = value;
                    OnPropertyChanged("FlightPermitNumber");
                }
            }
        }

        private string _approvalDate;
        public string ApprovalDate
        {
            get
            {
                return _approvalDate;
            }
            set
            {
                if (_approvalDate != value)
                {
                    _approvalDate = value;
                    OnPropertyChanged("ApprovalDate");
                }
            }
        }

        private string _requestBay = "";
        public string RequestBay
        {
            get
            {
                return _requestBay;
            }
            set
            {
                if (_requestBay != value)
                {
                    _requestBay = value;
                    OnPropertyChanged("RequestBay");
                }
            }
        }
    }
}
