﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;

namespace Standard_Reports.Entities
{
    [Entity(SerializedName = "PST")]
    public class EntParkingStand : EntDbParkingStand
    {
        public IList<EntUnavailable> Unavailables { get; set; }
    }
}
