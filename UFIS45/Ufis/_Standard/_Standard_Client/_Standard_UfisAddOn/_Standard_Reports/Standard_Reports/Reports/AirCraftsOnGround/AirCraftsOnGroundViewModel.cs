﻿using System;
using System.Linq;
using System.Collections.Generic;
using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using Standard_Reports.DataAccess;
using Standard_Reports.Entities;



namespace Standard_Reports.Reports.AirCraftsOnGround
{
    public class AirCraftsOnGroundViewModel : WorkspaceViewModel
    {
        public WorkspaceViewModel _workspace;
        IList<EntDbFlightPermit> _permitList;
        EntDbFlight _flight;
        IList<entlcAircraftOnGround> _itemSource;
        IList<entlcAircraftOnGround> objAircraftOnGround;
        DateTime? date;

        public IList<entlcAircraftOnGround> ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource != value)
                {
                    _itemSource = value;
                    OnPropertyChanged("ItemSource");
                }
            }
        }

        public string GetParameterValue()
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbParameter> objParameter = DlCommon.GetParameterValue("ID_APR_FT");

            if (objParameter != null)
            {
                foreach (EntDbParameter objTmpParameter in objParameter)
                {
                    sReturnValue = objTmpParameter.Value;
                }
            }
            return sReturnValue;
        }

        public string GetLoginUserName(string USID)
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbSecured> objUser = DlCommon.GetLoginUserName(USID);

            if (objUser != null)
            {
                foreach (EntDbSecured objTmpUser in objUser)
                {
                    sReturnValue = objTmpUser.Name;
                }
            }
            return sReturnValue;
        }

        public void GetAirCraftOnGroundView(DateTime dtFrom, DateTime dtTo)
        {
            GetFlightPermits(dtFrom, dtTo);
            GetAirCraftOnGroundEntity();
            ItemSource = objAircraftOnGround;
        }

        private void GetAirCraftOnGroundEntity()
        {
            if (PermitList.Count > 0)
            {
                objAircraftOnGround = new List<entlcAircraftOnGround>();
                foreach (EntDbFlightPermit tmpFlightpermit in PermitList)
                {
                    Flight = DIAirCraftsOnGround.GetFlight(tmpFlightpermit.CallSign, tmpFlightpermit.RegistrationNumber, tmpFlightpermit.ArrivalDepartureId, tmpFlightpermit.ValidFrom.Value, tmpFlightpermit.ValidTo.Value);
                    entlcAircraftOnGround TmpobjAircraftOnGround = new entlcAircraftOnGround();
                    TmpobjAircraftOnGround.RegistrationNumber = tmpFlightpermit.RegistrationNumber;
                    
                    TmpobjAircraftOnGround.CallSign = tmpFlightpermit.CallSign;
                    TmpobjAircraftOnGround.OperatorName = tmpFlightpermit.OperatorName;
                    TmpobjAircraftOnGround.FlightPermitNumber = tmpFlightpermit.FlightPermitNumber;
                    date = tmpFlightpermit.ApprovalDate;
                    if (date.HasValue)
                    {
                        TmpobjAircraftOnGround.ApprovalDate = string.Format("{0:dd/MM/yyyy}", date.Value);
                    }
                    //TmpobjAircraftOnGround.RequestBay = tmpFlightpermit.RequestBay;
                     if (Flight != null && Flight.ADHO == "X")
                    {
                        date = Flight.LandingTime;
                        if (date.HasValue)
                        {
                            TmpobjAircraftOnGround.LandingTime = string.Format("{0:dd/MM/yyyy HHmm}", date.Value);
                        }
                        TmpobjAircraftOnGround.OperationalType = Flight.OperationalType;
                        TmpobjAircraftOnGround.AircraftIATACode = Flight.AircraftICAOCode;
                        TmpobjAircraftOnGround.RequestBay = Flight.PositionOfArrival;    
                        objAircraftOnGround.Add(TmpobjAircraftOnGround);
                    }
                    
                }
            }
        }
        private void GetFlightPermits(DateTime dtFrom, DateTime dtTo)
        {
            PermitList = DIAirCraftsOnGround.LoadFlightPermit(dtFrom, dtTo);
        }

        public IList<EntDbFlightPermit> PermitList
        {
            get
            {
                return _permitList;
            }
            set
            {
                if (_permitList != value)
                {
                    _permitList = value;
                    OnPropertyChanged("PermitList");
                }
            }
        }

        public EntDbFlight Flight
        {
            get
            {
                return _flight;
            }
            protected set
            {
                if (_flight != value)
                {
                    _flight = value;
                    OnPropertyChanged("Flight");
                }
            }
        }

        IList<EntDbFlight> _flightlist;
        public IList<EntDbFlight> FlightList
        {
            get
            {
                return _flightlist;
            }
            protected set
            {
                if (_flightlist != value)
                {
                    _flightlist = value;
                    OnPropertyChanged("FlightList");
                }
            }
        }
    }
}
