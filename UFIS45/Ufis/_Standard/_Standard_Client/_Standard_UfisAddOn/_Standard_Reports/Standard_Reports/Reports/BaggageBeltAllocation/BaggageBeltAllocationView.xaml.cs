﻿using System;
using System.Linq;
using System.Windows;
using System.Threading;
using DevExpress.Xpf.Grid;
using System.Globalization;
using System.Windows.Controls;
using DevExpress.Xpf.Printing;
using System.Collections.Generic;
using Standard_Reports.Helpers;
using Ufis.LoginWindow;
using Ufis.IO;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using Standard_Reports.Entities;
using System.Drawing.Printing;
using DevExpress.XtraPrinting;
using Ufis.Entities;

namespace Standard_Reports.Reports.BaggageBeltAllocation
{
    /// <summary>
    /// Interaction logic for BaggageBeltAllocationView.xaml
    /// </summary>
    public partial class BaggageBeltAllocationView : UserControl
    {
        #region +++ Variable Declaration +++
        public HpParameters param;
        public HpReportLogo Logo;
        public HpFooter Footer;
        public BaggageBeltAllocationViewModel objViewModel;
        string sFooterString = string.Empty;
        DateTime tfDate;
        DateTime ttDate;
        string strFilter = string.Empty;


        #endregion

        #region +++ Register Properties +++
        // Using a DependencyProperty as the backing store for BaggageBelt.  This enables animation, styling, binding, etc...

        public static readonly DependencyProperty BaggageProperty = DependencyProperty.Register("BaggageBelt", typeof(List<entlcBaggageBeltAllocation>), typeof(BaggageBeltAllocationViewModel), new UIPropertyMetadata(new List<entlcBaggageBeltAllocation>()));

        #endregion

        #region +++ Properties +++

        public List<entlcBaggageBeltAllocation> BaggageBelt
        {
            get
            {
                return (List<entlcBaggageBeltAllocation>)GetValue(BaggageProperty);
            }
            set
            {
                SetValue(BaggageProperty, value);
            }
        }

        #endregion

        #region +++ WPF Application Events +++

        private void Process(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(2000);
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                FetchData();
            }));
        }

        public BaggageBeltAllocationView()
        {
            //Change UFIS Date format into system.Thread
            CultureInfo ci = new CultureInfo(Thread.CurrentThread.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            Thread.CurrentThread.CurrentCulture = ci;
            InitializeComponent();
            dtpFromDate.SelectedDate = DateTime.Now.Date;
            dtpToDate.SelectedDate = DateTime.Now.Date;
            txtFromTime.Text = "0000";
            txtToTime.Text = "2359";

            natureComboBox.SelectAllItems();
        }

        private void barButtonPreview_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            if (gridBaggageBeltAllocation.VisibleRowCount > 0)
            {
                try
                {
                    EntUser objEntUser = new EntUser();
                    objEntUser = LoginViewModel.GetLastUser();
                    objViewModel = new BaggageBeltAllocationViewModel();
                    param = new HpParameters();
                    Footer = new HpFooter();
                    DateTime tDate = dtpFromDate.SelectedDate.Value;
                    string sDate = tDate.ToString("dd.MM.yyyy");
                    DateTime ttDate = dtpToDate.SelectedDate.Value;
                    string stDate = ttDate.ToString("dd.MM.yyyy");

                    sFooterString = objViewModel.GetParameterValue();

                    param.MyParameter = String.Format("                                    Arrival Belt Allocation Report From {0} {1} To {2} {3} {4} ", sDate, txtFromTime.Text, stDate, txtToTime.Text, HpUser.TLocalUTC == "L" ? "LT" : "UTC");

                    
                    param.Header = "                                                                        Airport Operations Control Center (AOCC)";

                    if (!string.IsNullOrEmpty(Helpers.HpUser.Sco))
                    {
                        sFooterString = String.Format("{0}    Scenario : {1}", sFooterString, Helpers.HpUser.Sco);
                    }

                    param.ReportFooterString = sFooterString;
                    param.GeneratedBy = string.Format("Printed By : {0}", objEntUser.UserId);

                    

                    ((HpParameters)Resources["paramet"]).MyParameter = param.MyParameter;
                    ((HpParameters)Resources["NewHeader"]).Header = param.Header;
                    ((HpParameters)Resources["Footer"]).ReportFooterString = param.ReportFooterString;
                    ((HpParameters)Resources["GenBy"]).GeneratedBy = param.GeneratedBy;

                    

                    string sLogoPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.UfisSystem);
                    sLogoPath = sLogoPath + "\\stdlogo.bmp";

                    Logo = new HpReportLogo();
                    Logo.RptLogo = new BitmapImage(new Uri(sLogoPath, UriKind.Absolute));
                    ((HpReportLogo)Resources["Logo"]).RptLogo = Logo.RptLogo;

                    PrintableControlLink link = new PrintableControlLink((TableView)gridBaggageBeltAllocation.View) { Margins = new Margins(50, 50, 30, 30), Landscape = true };
                    link.PageHeaderTemplate = (DataTemplate)Resources["detailPrintHeaderTemplate"];
                    link.PaperKind = PaperKind.A4;

                    link.PageFooterTemplate = (DataTemplate)Resources["DetailPrintFooterTemplate"];
                    link.ShowPrintPreviewDialog(Window.GetWindow(this));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            gridBaggageBeltAllocation.ShowLoadingPanel = false;
        }

        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dtpFromDate.SelectedDate.ToString() != "" && dtpToDate.SelectedDate.ToString() != "" && txtFromTime.Text != "" && txtToTime.Text != "" && dtpToDate.SelectedDate != null && dtpFromDate.SelectedDate != null && txtFromTime.Text.Length == 4 && txtToTime.Text.Length == 4)
                {
                        string sfDate, stDate;
                    tfDate = dtpFromDate.SelectedDate.Value;
                    ttDate = dtpToDate.SelectedDate.Value;
                    sfDate = String.Format("{0:yyyy.MM.dd} {1}", tfDate, txtFromTime.Text.Insert(2, ":"));
                    stDate = String.Format("{0:yyyy.MM.dd} {1}", ttDate, txtToTime.Text.Insert(2, ":"));

                    try
                    {
                        tfDate = Convert.ToDateTime(sfDate);
                        ttDate = Convert.ToDateTime(stDate);
                        strFilter =  natureComboBox.Text.Replace(";", "','");
                    }
                    catch
                    {
                        MessageBox.Show("Time value should be between 0000 and 2359.");
                    }

                    if (tfDate > ttDate)
                    {
                        MessageBox.Show("Invalid Date/time value.");
                    }

                    tfDate.AddHours(Convert.ToDouble(txtFromTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtFromTime.Text.Substring(2, 2))).AddSeconds(0);
                    ttDate.AddHours(Convert.ToDouble(txtToTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtToTime.Text.Substring(2, 2))).AddSeconds(59);

                    gridBaggageBeltAllocation.ShowLoadingPanel = true;

                    BackgroundWorker bw = new BackgroundWorker();

                    bw.DoWork += Process;
                    bw.RunWorkerCompleted += bgw_RunWorkerCompleted;
                    bw.RunWorkerAsync();
                }
                else
                {
                    MessageBox.Show("Invalid Date/Time value.");
                    gridBaggageBeltAllocation.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0}\n\r{1}\n\r{2}", ex.Message, ex.InnerException, ex.StackTrace));
            }
        }

        private void FetchData()
        {
            try
            {
                objViewModel = new BaggageBeltAllocationViewModel();
                objViewModel.GetBaggageBeltAllocationList(tfDate, ttDate, strFilter);
                gridBaggageBeltAllocation.ItemsSource = objViewModel.ItemSource;
            }
            catch(Exception ex)
            {
                if (ex.Message.Contains("Exception of type 'Ufis.Data.DataException' was thrown"))
                    MessageBox.Show("              Server connection was lost.\n Please click on the " + '"' + " View button " + '"' + " again.", "Baggage Belt Allocation Report");
              }
        }
    }
}
#endregion
