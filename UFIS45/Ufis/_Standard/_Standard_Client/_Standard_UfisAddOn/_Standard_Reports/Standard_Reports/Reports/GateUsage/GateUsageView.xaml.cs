﻿#region ++++ Source Code Information ++++
/*
        File Name       :   GatUsageView.xaml.cs
        Description     :   
 * 
 *      Developed By    : Siva
 *      Developed On    :
 
        Modified By     :   Phyoe Khiang Min    
 *      Modified On     :   6-Sep-2012
 *      Detail          :   Change Interval from 30 Minutes to 1 Hour, Add Note, Add Flight No and Gate Name
 
 */
#endregion

using System;
using System.Linq;
using System.Windows;
using System.Threading;
using System.Collections;
using System.Windows.Media;
using System.Windows.Input;
using System.Globalization;
using System.Windows.Controls;
using System.Drawing.Printing;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using System.Windows.Controls.Primitives;

using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Charts;
using DevExpress.Xpf.Printing;

using Ufis.IO;
using Ufis.Entities;
using Ufis.LoginWindow;
using Standard_Reports.Helpers;

namespace Standard_Reports.Reports.GateUsage
{
    /// <summary>
    /// Interaction logic for GateUsageView.xaml
    /// </summary>
    public partial class GateUsageView : UserControl
    {
        #region +++ Variable Declaration +++
        public HpParameters param;
        public HpReportLogo Logo;
        public HpFooter Footer;
        public GateUsageViewModel objViewModel;
        string sFooterString = "";
        DateTime tfDate = new DateTime();
        DateTime ttDate = new DateTime();
        private string strPlanAct = string.Empty;
        private string strRemoteContact = string.Empty;
        private string strArrDep = string.Empty;
        private static int totalHours = 24;

        private double[] _remoteArrivalFlight = new double[totalHours];
        private double[] _remoteDepartureFlight = new double[totalHours];
        private double[] _contactArrivalFlight = new double[totalHours];
        private double[] _contactDepartureFlight = new double[totalHours];

        private string[] _RemoteArrivalFlightNo = new string[totalHours];
        private string[] _RemoteDepartureFlightNo = new string[totalHours];
        private string[] _ContactArrivalFlightNo = new string[totalHours];
        private string[] _ContactDepartureFlightNo = new string[totalHours];

        private string[] _RemoteArrivalGateName = new string[totalHours];        
        private string[] _RemoteDepartureGateName = new string[totalHours];        
        private string[] _ContactArrivalGateName = new string[totalHours];        
        private string[] _ContactDepartureGateName = new string[totalHours];


        private string[] _argument = new string[totalHours];
        #endregion

        #region +++ Register Properties +++
        // Using a DependencyProperty as the backing store for MyInt.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyIntProperty =
            DependencyProperty.Register("MyInt", typeof(int), typeof(GateUsageView), new UIPropertyMetadata(1));
        #endregion

        #region +++ Properties +++
        public int MyInt
        {
            get { return (int)GetValue(MyIntProperty); }
            set { SetValue(MyIntProperty, value); }
        }
        #endregion

        #region +++ WPF Application Control Events +++
        public GateUsageView()
        {
            //Change UFIS Date format into system.Thread
            CultureInfo ci = new CultureInfo(Thread.CurrentThread.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            Thread.CurrentThread.CurrentCulture = ci;
            InitializeComponent();
            dtpFromDate.Text = DateTime.Now.Date.ToString("dd.MM.yyyy");
            dtpToDate.Text = DateTime.Now.Date.ToString("dd.MM.yyyy"); 
            txtFromTime.Text = "0000";
            txtToTime.Text = "2359";
            cboTemplate.SelectedIndex = 2;
            FillCustomAxisLabels();
        }

        private void barButtonPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            try
            {
                if (objViewModel != null)
                {

                    if (objViewModel.ItemSource != null)
                    {

                        EntUser objEntUser = new EntUser();
                        objEntUser = LoginViewModel.GetLastUser();

                        param = new HpParameters();
                        Footer = new HpFooter();
                        DateTime tDate = dtpFromDate.SelectedDate.Value;
                        string sDate = tDate.ToString("dd.MM.yyyy");
                        DateTime ttDate = dtpToDate.SelectedDate.Value;
                        string stDate = ttDate.ToString("dd.MM.yyyy");

                        sFooterString = objViewModel.GetEntParameterValue();

                        if (!string.IsNullOrEmpty(Helpers.HpUser.Sco))
                        {
                            sFooterString = String.Format("{0}    Scenario : {1}", sFooterString, Helpers.HpUser.Sco);
                        }

                        param.MyParameter = String.Format("Gate Usage Report from {0} {1} to {2} {3} {4} ", sDate, txtFromTime.Text, stDate, txtToTime.Text, String.Compare(HpUser.TLocalUTC, "L", false) == 0 ? "LT" : "UTC");
                        param.ReportFooterString = sFooterString;
                        param.GeneratedBy = string.Format("Printed By : {0}", objEntUser.UserId);

                        ((HpParameters)Resources["paramet"]).MyParameter = param.MyParameter;
                        ((HpParameters)Resources["Footer"]).ReportFooterString = param.ReportFooterString;
                        ((HpParameters)Resources["GenBy"]).GeneratedBy = param.GeneratedBy;

                        string sLogoPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.UfisSystem);
                        sLogoPath = sLogoPath + "\\stdlogo.bmp";

                        Logo = new HpReportLogo();
                        Logo.RptLogo = new BitmapImage(new Uri(sLogoPath, UriKind.Absolute));
                        ((HpReportLogo)Resources["Logo"]).RptLogo = Logo.RptLogo;


                        SimpleLink link = new SimpleLink();
                        link.DetailCount = 1;
                        link.Margins = new Margins(0, 0, 0, 0);
                        link.Landscape = true;
                        link.PaperKind = PaperKind.A4;
                        link.DetailTemplate = (DataTemplate)Resources["DetailPrintChartTemplate"];
                        link.PageHeaderTemplate = (DataTemplate)Resources["DetailPrintHeaderTemplate"];
                        link.PageFooterTemplate = (DataTemplate)Resources["DetailPrintFooterTemplate"];
                        link.CreateDetail += new EventHandler<DevExpress.Xpf.Printing.CreateAreaEventArgs>(link_CreateDetail);
                        link.CreateDocument(false);
                        link.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                        Mouse.OverrideCursor = null;
                        link.ShowPrintPreviewDialog(Window.GetWindow(this));
                        
                    }
                }
            }
            catch (Exception ex)
            { }
            finally
            {
                Mouse.OverrideCursor = null;
            }

        }

        void link_CreateDetail(object sender, CreateAreaEventArgs e)
        {
            VisualBrush brush = new VisualBrush(gateUsageChart);
            DrawingVisual visual = new DrawingVisual();
            DrawingContext context = visual.RenderOpen();

            context.DrawRectangle(brush, null, new Rect(0, 0, 1180, 574));
            context.Close();

            RenderTargetBitmap bmp = new RenderTargetBitmap((int)1180, (int)574, 96, 96, PixelFormats.Pbgra32);

            bmp.Render(visual);

            e.Data = bmp;
        }

        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            try
            {

                if (String.Compare(dtpFromDate.SelectedDate.ToString(), "", false) != 0 &&
                    String.Compare(dtpToDate.SelectedDate.ToString(), "", false) != 0 &&
                    String.Compare(txtFromTime.Text, "", false) != 0 &&
                    String.Compare(txtToTime.Text, "", false) != 0 && dtpToDate.SelectedDate != null && dtpFromDate.SelectedDate != null && txtFromTime.Text.Length >= 4 && txtToTime.Text.Length >= 4)
                {
                    objViewModel = new GateUsageViewModel();
                    string sfDate, stDate;
                    tfDate = dtpFromDate.SelectedDate.Value;
                    ttDate = dtpToDate.SelectedDate.Value;
                    sfDate = String.Format("{0:yyyy.MM.dd} {1}", tfDate, txtFromTime.Text.Insert(2, ":"));
                    stDate = String.Format("{0:yyyy.MM.dd} {1}", ttDate, txtToTime.Text.Insert(2, ":"));

                    try
                    {
                        tfDate = Convert.ToDateTime(sfDate);
                        ttDate = Convert.ToDateTime(stDate);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Time value should be between 0000 and 2359.");
                    }

                    if (tfDate > ttDate)
                    {
                        MessageBox.Show("From date time period should be greater than To date time period.");
                    }
                    else
                    {
                        TimeSpan ts = ttDate - tfDate;
                        int itotalHours = Convert.ToInt32(ts.TotalMinutes / 60);

                        if (ts.Days >= 1 && ts.Minutes >= 1 || itotalHours > totalHours)
                        {
                            MessageBox.Show(String.Format("Seleceted time period should not exceed a total of {0} hours.", totalHours));
                        }
                        else
                        {
                            DateTime temp = ttDate;
                            tfDate.AddHours(Convert.ToDouble(txtFromTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtFromTime.Text.Substring(2, 2))).AddSeconds(0);
                            if (txtToTime.Text.Substring(2, 2) == "00")
                            
                                ttDate =  ttDate.AddHours(- 1).AddMinutes(59).AddSeconds(Convert.ToDouble(59));                            
                            
                            //else
                            //    ttDate = ttDate.AddHours(Convert.ToDouble(txtToTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtToTime.Text.Substring(2, 2))).AddSeconds(Convert.ToDouble(59));

                            if (chkPlan.IsChecked.Value)
                                strPlanAct = "PLAN";
                            else if (chkAct.IsChecked.Value)
                                strPlanAct = "ACT";
                            if (chkRemoteContact.IsChecked.Value)
                                strRemoteContact = chkRemoteContact.Content.ToString();
                            else if (chkRemote.IsChecked.Value)
                                strRemoteContact = chkRemote.Content.ToString();
                            else if (chkContact.IsChecked.Value)
                                strRemoteContact = chkContact.Content.ToString();
                            if (chkArrDep.IsChecked.Value) strArrDep = chkArrDep.Content.ToString();
                            else if (chkArr.IsChecked.Value) strArrDep = chkArr.Content.ToString();
                            else if (chkDep.IsChecked.Value) strArrDep = chkDep.Content.ToString();

                            _argument = objViewModel.GetGateUsageReportDate(tfDate, ttDate, strPlanAct, strRemoteContact.ToUpper(), strArrDep.ToUpper());
                            CreateGateUsageBarSeries();

                            if (objViewModel.FlightSource != null)
                            {
                                _remoteArrivalFlight = objViewModel.FlightSource[0];
                                _remoteDepartureFlight = objViewModel.FlightSource[1];
                                _contactArrivalFlight = objViewModel.FlightSource[2];
                                _contactDepartureFlight = objViewModel.FlightSource[3];

                                _RemoteArrivalFlightNo = objViewModel.FlightNumber[0];
                                _RemoteDepartureFlightNo = objViewModel.FlightNumber[1];
                                _ContactArrivalFlightNo = objViewModel.FlightNumber[2];
                                _ContactDepartureFlightNo = objViewModel.FlightNumber[3];

                                _RemoteArrivalGateName = objViewModel.GateFullName[0];
                                _RemoteDepartureGateName = objViewModel.GateFullName[1];
                                _ContactArrivalGateName = objViewModel.GateFullName[2];
                                _ContactDepartureGateName = objViewModel.GateFullName[3];
                            }
                        }
                    }
                }

                else
                {
                    MessageBox.Show("Invalid Date/Time value.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0}\n\r{1}\n\r{2}", ex.Message, ex.InnerException, ex.StackTrace));
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }

        }

        private void gateUsageChart_MouseMove(object sender, MouseEventArgs e)
        {
            ChartHitInfo hitInfo = gateUsageChart.CalcHitInfo(e.GetPosition(gateUsageChart));
            double flightCount = 0.0;
            string sFullFlightName = "";
            string sGateFullName = "";
            if (hitInfo != null && hitInfo.SeriesPoint != null)
            {
                SeriesPoint point = hitInfo.SeriesPoint;

                int index = Array.IndexOf(_argument, point.Argument);
                if (index >=0)
                {
                    switch (point.Series.DisplayName)
                    {
                        case "Remote Arrival":
                            {
                                flightCount = _remoteArrivalFlight[index];
                                sFullFlightName = _RemoteArrivalFlightNo[index];
                                sGateFullName = _RemoteArrivalGateName[index];
                                break;
                            }
                        case "Remote Departure":
                            {
                                flightCount = _remoteDepartureFlight[index];
                                sFullFlightName = _RemoteDepartureFlightNo[index];
                                sGateFullName = _RemoteDepartureGateName[index];
                                break;
                            }
                        case "Contact Arrival":
                            {
                                flightCount = _contactArrivalFlight[index];
                                sFullFlightName = _ContactArrivalFlightNo[index];
                                sGateFullName = _ContactArrivalGateName[index];
                               break;
                            }
                        default:
                            {
                                flightCount = _contactDepartureFlight[index];
                                sFullFlightName = _ContactDepartureFlightNo[index];
                                sGateFullName = _ContactDepartureGateName[index];
                                break;
                            }
                    }
                }



                Gatetooltip_text.Text = string.Format("Series = {0}\nTotal No.of Gates = {1} \n Gate Name = {2} \n\nTotal No.of Flights = {3} \nFlight No = {4}",
                 point.Series.DisplayName, point.Value, sGateFullName, flightCount, sFullFlightName);

                Gatetooltip.Placement = PlacementMode.Mouse;
                Gatetooltip.IsOpen = true;
                Cursor = Cursors.Hand;
            }
            else
            {
                Gatetooltip.IsOpen = false;
                Cursor = Cursors.Arrow;
            }
        }

        private void gateUsageChart_MouseLeave(object sender, MouseEventArgs e)
        {
            Gatetooltip.IsOpen = false;
        }

        private void cboTemplate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            if (String.Compare(cboTemplate.Text, "", false) != 0)
            {
                string cmbvalue = "";
                ComboBoxItem curItem = ((ComboBoxItem)cboTemplate.SelectedItem);
                totalHours = Convert.ToInt32(cmbvalue = curItem.Content.ToString().Substring(0, 2).Trim());

                if (totalHours == 24)
                {
                    txtFromTime.Text = "0000";
                    txtToTime.Text = "2359";
                }
                else if (totalHours == 12)
                {
                    txtFromTime.Text = "0000";
                    txtToTime.Text = "1159";
                }
                else if (totalHours == 6)
                {
                    txtFromTime.Text = "0000";
                    txtToTime.Text = "0559";
                }

                objViewModel = new GateUsageViewModel();
                objViewModel.TotalHour = totalHours;

                _remoteArrivalFlight = new double[totalHours];
                _remoteDepartureFlight = new double[totalHours];
                _contactArrivalFlight = new double[totalHours];
                _contactDepartureFlight = new double[totalHours];

                _RemoteArrivalFlightNo = new string[totalHours];
                _RemoteDepartureFlightNo = new string[totalHours];
                _ContactArrivalFlightNo = new string[totalHours];
                _ContactDepartureFlightNo = new string[totalHours];

                _RemoteArrivalGateName = new string[totalHours];        
                _RemoteDepartureGateName = new string[totalHours];        
                _ContactArrivalGateName = new string[totalHours];        
                _ContactDepartureGateName = new string[totalHours];

                _argument = new string[totalHours];

            }
        }
        #endregion

        #region +++ Private & Public Events +++
        private void CreateGateUsageBarSeries()
        {
            HpSeriesTypeItem seriesTypeItem = new HpSeriesTypeItem(typeof(XYDiagram2D), typeof(BarSideBySideStackedSeries2D), "2D Side-By-Side Stacked Bars", 4) as HpSeriesTypeItem;

            if (seriesTypeItem == null)
                return;

            gateUsageChart.BeginInit();
            ClearCustomAxisLabels();
            FillCustomAxisLabels();
            gateUsageChartDiagram.Series.Clear();
            try
            {
                ArrayList objSeriesLable = SetSeriesLable();
                for (int i = 0; i < seriesTypeItem.SeriesCount; i++)
                {
                    Series series = (Series)Activator.CreateInstance(seriesTypeItem.SeriesType);
                    series.Label = new SeriesLabel();
                    series.DisplayName = objSeriesLable[i].ToString();
                    InitializeSeries(series, i + 1);
                    ISupportStackedGroup supportStackedGroup = series as ISupportStackedGroup;
                    if (supportStackedGroup != null)
                        supportStackedGroup.StackedGroup = i % 2;
                    ISupportTransparency supportTransparency = series as ISupportTransparency;
                    if (supportTransparency != null)
                        supportTransparency.Transparency = 0.3;
                    series.Label.FontSize = 14.00;
                    series.Label.ResolveOverlappingMode = ResolveOverlappingMode.Default;
                    gateUsageChartDiagram.Series.Add(series);

                }
            }
            finally
            {
                gateUsageChart.EndInit();
            }

        }

        private ArrayList SetSeriesLable()
        {
            ArrayList objSeriesLable = new ArrayList();
            objSeriesLable.Add("Remote Arrival");
            objSeriesLable.Add("Remote Departure");
            objSeriesLable.Add("Contact Arrival");
            objSeriesLable.Add("Contact Departure");
            return objSeriesLable;
        }

        private void InitializeSeries(Series series, int seriesNumber)
        {
            series.DataSource = objViewModel.ItemSource;
            series.ArgumentDataMember = "Argument";
            series.ValueDataMember = "Value" + seriesNumber.ToString();
        }

        void FillCustomAxisLabels()
        {
            XYDiagram2D diagram = (XYDiagram2D)gateUsageChart.Diagram;
            for (int i = 0; i < 100; i++)
            {
                diagram.AxisY.CustomLabels.Add(new CustomAxisLabel(i, String.Format("{0}", i.ToString())));
            }
        }

        void ClearCustomAxisLabels()
        {
            ((XYDiagram2D)gateUsageChart.Diagram).AxisY.CustomLabels.Clear();
        }
        #endregion
    }
} 