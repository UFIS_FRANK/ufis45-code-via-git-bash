﻿using System;
using System.Linq;
using System.Collections.Generic;
using Ufis.MVVM.ViewModel;
using Standard_Reports.Entities;
using Standard_Reports.DataAccess;
using Ufis.Entities;
using Ufis.Data;

namespace Standard_Reports.Reports.Towing
{
    public class TowingReportViewModel : WorkspaceViewModel
    {
        IList<entlcTowing> objTowingReport;

        private IList<entlcTowing> _itemSource;
        public IList<entlcTowing> ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource != value)
                {
                    _itemSource = value;
                    OnPropertyChanged("ItemSource");
                }
            }
        }

        private IList<EntDbFlight> _arrvalFlightlist;
        public IList<EntDbFlight> ArrivalFlightList
        {
            get
            {
                return _arrvalFlightlist;
            }
            set
            {
                if (_arrvalFlightlist != value)
                {
                    _arrvalFlightlist = value;
                    OnPropertyChanged("ArrivalFlightList");
                }
            }
        }

        private IList<EntDbFlight> _departureFlightList;
        public IList<EntDbFlight> DepartureFlightList
        {
            get 
            {
                return _departureFlightList;
            }
            set 
            {
                if (_departureFlightList != value)
                {
                    _departureFlightList = value;
                    OnPropertyChanged("DepartureFlightList");
                }
            }
        }

        private IList<EntDbFlight> _towingFlightList;
        public IList<EntDbFlight> TowingFlightList
        {
            get
            {
                return _towingFlightList;
            }
            set
            {
                if (_towingFlightList != value)
                {
                    _towingFlightList = value;
                    OnPropertyChanged("TowingFlightList");
                }
            }
        }

        public void GetTowingReport(DateTime dtFrom, DateTime dtTo)
        {
            GetFlightList(dtFrom, dtTo);
            GetTowingReportEntity();
            ItemSource = objTowingReport;
        }

        private void GetFlightList(DateTime dtFrom, DateTime dtTo)
        {
            IList<string> urnoList = new List<string>();
            TowingFlightList = DITowing.GetTowingFlight(dtFrom, dtTo);
            if (TowingFlightList.Count > 0)
            {
                foreach (EntDbFlight flight in TowingFlightList)
                {
                    urnoList.Add(flight.RelationKey.ToString());
                }

                if (urnoList.Count > 0)
                    ArrivalFlightList = DITowing.GetArrivalFlight(urnoList);
                    DepartureFlightList = DITowing.GetDepartureFlight(urnoList);
            }
        }


        public string GetConfiguration(string appName, string secName, string param)
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbConfiguration> objConfiguration = DITowing.GetConfiguration(appName,secName,param) ;

            if (objConfiguration != null)
            {
                foreach (EntDbConfiguration objTmpconfig in objConfiguration)
                {
                    sReturnValue = objTmpconfig.ParameterValue;
                }
            }
            return sReturnValue;
        }
           


        private void GetTowingReportEntity()
        {
            if (TowingFlightList.Count > 0)
            { 
                 objTowingReport = new List<entlcTowing>();
                 foreach (EntDbFlight tmpTowFlight in TowingFlightList)
                 {
                     entlcTowing TmpobjTowingReport = new entlcTowing();
                     TmpobjTowingReport.ReadyForTowing = tmpTowFlight.ReadyForTowing;
                     
                     TmpobjTowingReport.AircraftIATACode = tmpTowFlight.AircraftIATACode;
                     TmpobjTowingReport.RegistrationNumber = tmpTowFlight.RegistrationNumber;
                     TmpobjTowingReport.PositionOfArrival = tmpTowFlight.PositionOfArrival;
                     TmpobjTowingReport.PositionOfDeparture = tmpTowFlight.PositionOfDeparture;
                     if (tmpTowFlight.OnblockTime.HasValue)
                     TmpobjTowingReport.OnblockTime = String.Format("{0:dd/HHmm}", tmpTowFlight.OnblockTime.Value);
                     if (tmpTowFlight.OffblockTime.HasValue)
                     TmpobjTowingReport.OffblockTime = String.Format("{0:dd/HHmm}", tmpTowFlight.OffblockTime.Value);
                     if (tmpTowFlight.StandardTimeOfArrival.HasValue)
                         TmpobjTowingReport.StandardTimeOfArrival = String.Format("{0:dd/HHmm}", tmpTowFlight.StandardTimeOfArrival.Value);
                     if (tmpTowFlight.StandardTimeOfDeparture.HasValue)
                         TmpobjTowingReport.StandardTimeOfDeparture = String.Format("{0:dd/HHmm}", tmpTowFlight.StandardTimeOfDeparture.Value);
                     if (ArrivalFlightList != null && ArrivalFlightList.Count > 0)
                     {
                         foreach (EntDbFlight ArrFlight in ArrivalFlightList)
                         {
                             if (ArrFlight.RelationKey == tmpTowFlight.RelationKey)
                             {
                                 if (ArrFlight.FullFlightNumber != "")
                                     TmpobjTowingReport.ArrivalFullFlightNumber = ArrFlight.FullFlightNumber;
                                 else
                                     TmpobjTowingReport.ArrivalFullFlightNumber = ArrFlight.CallSign;
                              }
                         }
                     }
                     if (DepartureFlightList != null && DepartureFlightList.Count > 0)
                     {
                         foreach (EntDbFlight Depflight in DepartureFlightList)
                         {
                             if (Depflight.RelationKey == tmpTowFlight.RelationKey)
                             {
                                 if (Depflight.FullFlightNumber != "")
                                     TmpobjTowingReport.DepartureFullFlightNumber = Depflight.FullFlightNumber;
                                 else
                                     TmpobjTowingReport.DepartureFullFlightNumber = Depflight.CallSign;
                             }
                         }
                     }

                     objTowingReport.Add(TmpobjTowingReport);
                 }
            
            }
        }


        public string GetParameterValue()
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbParameter> objParameter = DlCommon.GetParameterValue("ID_APR_FT");

            if (objParameter != null)
            {
                foreach (EntDbParameter objTmpParameter in objParameter)
                {
                    sReturnValue = objTmpParameter.Value;
                }
            }
            return sReturnValue;
        }

        public string GetLoginUserName(string USID)
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbSecured> objUser = DlCommon.GetLoginUserName(USID);

            if (objUser != null)
            {
                foreach (EntDbSecured objTmpUser in objUser)
                {
                    sReturnValue = objTmpUser.Name;
                }
            }
            return sReturnValue;
        }
    }
}
