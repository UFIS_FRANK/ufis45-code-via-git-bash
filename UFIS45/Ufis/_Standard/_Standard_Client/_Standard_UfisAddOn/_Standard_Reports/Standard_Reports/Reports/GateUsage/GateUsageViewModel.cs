﻿using System;
using System.Linq;
using System.Collections.Generic;
using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using Standard_Reports.DataAccess;
using Standard_Reports.Helpers;
using Standard_Reports.Entities;

namespace Standard_Reports.Reports.GateUsage
{
    public class GateUsageViewModel : WorkspaceViewModel
    {
        #region +++ Variable Declaration +++
        public WorkspaceViewModel _workspace;
        IList<HpDataPoint> _itemSource;

        EntityCollectionBase<EntFlight> objArrivalFlights;
        EntityCollectionBase<EntFlight> objDepFlights;
        EntityCollectionBase<EntFlight> objArrivalFlights2;
        EntityCollectionBase<EntFlight> objDepFlights2;
        EntityCollectionBase<EntGate> objGateList;
        
        private double[] _remoteArrivalFlight;
        private double[] _contactArrivalFlight;
        private double[] _remoteDepartureFlight;
        private double[] _contactDepartureFlight;
        private double[] _remoteArrivalGate;
        private double[] _contactArrivalGate;
        private double[] _remoteDepartureGate;
        private double[] _contactDepartureGate;
        private string[] _argument;
        private string[] _RemoteArrivalFlightNo; //Remote Arrival
        private string[] _RemoteArrivalGateName; //Remote Arrival
        private string[] _RemoteDepartureFlightNo;
        private string[] _RemoteDepartureGateName;
        private string[] _ContactArrivalFlightNo;
        private string[] _ContactArrivalGateName;
        private string[] _ContactDepartureFlightNo;
        private string[] _ContactDepartureGateName;


        DateTime? dtBegin = new DateTime();
        DateTime? dtEnd = new DateTime();
        private string sGateName = "";

        private const int durations = 1; //Interval
        private static int totalDurations = 24; //Duration
        
        IList<double[]> _flightSource;
        IList<string[]> _FlightNumber;
        IList<string[]> _GateFullName;
        List<HpDataPoint> objGateUsageData = new List<HpDataPoint>();
        IList<double[]> objFlightUsageData = new List<double[]>();
        IList<string[]> objFlightNumber= new List<string[]>();
        IList<string[]> objGateFullName= new List<string[]>();
        #endregion

        #region +++ Properties +++
        public IList<HpDataPoint> ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource == value)
                    return;

                _itemSource = value;
                OnPropertyChanged("ItemSource");
            }
        }
        public IList<string[]> FlightNumber
        {        
            get
            {
                return _FlightNumber;
            }
            protected set
            {
                if (_FlightNumber == value)
                    return;

                _FlightNumber = value;
                OnPropertyChanged("FlightNo");
            }
        
        }
        public IList<string[]> GateFullName
        {
            get
            {
                return _GateFullName;
            }
            protected set
            {
                if (_GateFullName == value)
                    return;

                _GateFullName = value;
                OnPropertyChanged("GateName");
            }

        }
        public IList<double[]> FlightSource
        {
            get
            {
                return _flightSource;
            }
            protected set
            {
                if (_flightSource == value)
                    return;

                _flightSource = value;
                OnPropertyChanged("FlightSource");
            }
        }
        public int TotalHour
        {
            get
            {
                return totalDurations;
            }
            set
            {
                if (totalDurations == value)
                    return;

                totalDurations = value;
                OnPropertyChanged("TotalHour");
            }

        }
        private DateTime DateFrom
        {
            get;
            set;
        }
        private DateTime DateTo
        {
            get;
            set;
        }
        #endregion

        #region +++ Data Manipulation +++

        /// <summary>
        /// Get Flight list by Date Range
        /// </summary>
        /// <param name="sType">System.Strung containing sType</param> 
        private void GetFlights(string sType)
        {
            //Get Arrival Flight by date and time range
            objArrivalFlights = DlGateUsage.LoadArrivalFlightListByDateRange(DateFrom, DateTo, sType);
            //Get Departure Flight by date and time range
            objDepFlights = DlGateUsage.LoadDepartureFlightListByDateRange(DateFrom, DateTo, sType);
            //Get Arrival Flight by date and time range
            objArrivalFlights2 = DlGateUsage.LoadArrivalFlightListByDateRange2(DateFrom, DateTo, sType);
            //Get Departure Flight by date and time range
            objDepFlights2 = DlGateUsage.LoadDepartureFlightListByDateRange2(DateFrom, DateTo, sType);
            objGateList = DlGateUsage.GetGateList();
        }
        /// <summary>
        /// Retrieve Flight & Gate usage count
        /// </summary>
        /// <param name="dtFrom">System.DateTime containing dtFrom</param>
        /// <param name="dtTo">System.DateTime containing dtTo</param>
        /// <param name="sType">System.string containing sType</param>
        /// <param name="sFilter1">System.string contianing sFilter1</param>
        /// <param name="sFilter2">Syste.string containg sFilter2</param>
        /// <returns>Return System.string array</returns>
        public string[] GetGateUsageReportDate(DateTime dtFrom, DateTime dtTo, string sType, string sFilter1, string sFilter2)
        {
            DateFrom = dtFrom;
            DateTo = dtTo.AddSeconds(59);
            _remoteArrivalFlight = new double[totalDurations];
            _remoteDepartureFlight = new double[totalDurations];
            _contactArrivalFlight = new double[totalDurations];
            _contactDepartureFlight = new double[totalDurations];
            _remoteArrivalGate = new double[totalDurations];
            _remoteDepartureGate = new double[totalDurations];
            _contactArrivalGate = new double[totalDurations];
            _contactDepartureGate = new double[totalDurations];
            _RemoteArrivalFlightNo = new string[totalDurations];
            _RemoteArrivalGateName = new string[totalDurations];
            _RemoteDepartureFlightNo = new string[totalDurations];
            _RemoteDepartureGateName = new string[totalDurations];
            _ContactArrivalFlightNo = new string[totalDurations];
            _ContactArrivalGateName = new string[totalDurations];
            _ContactDepartureFlightNo = new string[totalDurations];
            _ContactDepartureGateName = new string[totalDurations];
            
            _argument = new string[totalDurations];
            GetFlights(sType);
            switch (sType)
            {
                case "PLAN":
                    {
                        //Get Planned Gate Usage
                        GetScheduleArrivalFlightUsageList();
                        GetScheduleDepartureFlightUsageList();
                        GetScheduleArrivalGateUsageList();
                        GetScheduleDepartureGateUsageList();
                        //Get Planned Gate Usage
                        GetScheduleArrivalFlightUsageList2();
                        GetScheduleDepartureFlightUsageList2();
                        GetScheduleArrivalGateUsageList2();
                        GetScheduleDepartureGateUsageList2();
                        break;
                    }
                case "ACT":
                    {
                        //Get Actual Gate Usage
                        GetActualArrivalGateUsageList();
                        GetActualDepartureGateUsageList();
                        GetActualArrivalFlightUsageList();
                        GetActualDepartureFlightUsageList();
                        //Get Actual Gate Usage
                        GetActualArrivalGateUsageList2();
                        GetActualDepartureGateUsageList2();
                        GetActualArrivalFlightUsageList2();
                        GetActualDepartureFlightUsageList2();
                        break;
                    }
            }

            CreateGateUsageChartDataSource(sFilter1, sFilter2);
            return _argument;
        }

        #region +++ Scheduled ++++
        #region +++ Gate 1 +++
        /// <summary>
        /// Get Schedule Arrival Flight Usage Data for the time frame
        /// </summary>
        private string GetScheduleArrivalFlightUsageList()
        {
            try
            {
                if (objArrivalFlights.Count > 0)
                {
                    foreach (EntFlight tmpArrivalFlights in objArrivalFlights)
                    {
                        if (objGateList == null)
                            continue;

                        EntGate objScheduleGate = objGateList.FirstOrDefault(d => String.Compare(d.GateName, tmpArrivalFlights.GateOfArrival1, false) == 0);

                        if (objScheduleGate == null)
                            continue;

                        if (tmpArrivalFlights.ArrivalScheduleBeginForGate1 != null)
                        {
                            dtBegin = tmpArrivalFlights.ArrivalScheduleBeginForGate1;
                        }
                        if (tmpArrivalFlights.ArrivalScheduleEndForGate1 != null)
                        {
                            dtEnd = tmpArrivalFlights.ArrivalScheduleEndForGate1;
                        }

                        if (!(dtBegin.HasValue && dtEnd.HasValue))
                            continue;

                        if (dtBegin <= DateFrom)
                            dtBegin = DateFrom;
                        if (dtEnd >= DateTo)
                            dtEnd = DateTo;

                        DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                        int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                        int toX = dtEnd.Value.Subtract(tempTime).Hours;
                        for (int j = fromX; j <= toX; j++)
                        {
                            if (String.Compare(objScheduleGate.BusGate, "X", false) == 0)
                            {
                                if (String.Compare(_RemoteArrivalFlightNo[j], null, false) != 0)
                                    _RemoteArrivalFlightNo[j] = _RemoteArrivalFlightNo[j] + ",";
                                _RemoteArrivalFlightNo[j] = _RemoteArrivalFlightNo[j] + tmpArrivalFlights.FullFlightNumber;
                                _remoteArrivalFlight[j]++;
                            }
                            else
                            {
                                if (String.Compare(_ContactArrivalFlightNo[j], null, false) != 0)
                                    _ContactArrivalFlightNo[j] = _ContactArrivalFlightNo[j] + ",";
                                _ContactArrivalFlightNo[j] = _ContactArrivalFlightNo[j] + tmpArrivalFlights.FullFlightNumber;

                                _contactArrivalFlight[j]++;
                            }
                        }
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        /// <summary>
        /// Get Schedule Arrival Gate Usage Data for the time frame
        /// </summary>
        private string GetScheduleArrivalGateUsageList()
        {
            try
            {

                if (objGateList.Count > 0 && objArrivalFlights.Count > 0)
                {
                    foreach (EntGate tmpScheduleGate in objGateList)
                    {
                        double[] tmpRemoteArrival = new double[totalDurations];
                        double[] tmpContactArrival = new double[totalDurations];

                        IEnumerable<EntFlight> tmpObjArrivalGates = objArrivalFlights.Where<EntFlight>(g => String.Compare(g.GateOfArrival1, tmpScheduleGate.GateName, false) == 0);

                        foreach (EntFlight tmpGate in tmpObjArrivalGates)
                        {
                            if (tmpGate.ArrivalScheduleBeginForGate1 != null)
                            {
                                dtBegin = tmpGate.ArrivalScheduleBeginForGate1;
                            }
                            if (tmpGate.ArrivalScheduleEndForGate1 != null)
                            {
                                dtEnd = tmpGate.ArrivalScheduleEndForGate1;
                            }
                            if (!(dtBegin.HasValue && dtEnd.HasValue))
                                continue;

                            if (dtBegin <= DateFrom)
                                dtBegin = DateFrom;
                            if (dtEnd >= DateTo)
                                dtEnd = DateTo;

                            DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                            int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                            int toX = dtEnd.Value.Subtract(tempTime).Hours;

                            for (int j = fromX; j <= toX; j++)
                            {
                                if (String.Compare(tmpScheduleGate.BusGate, "X", false) == 0)
                                {
                                    if (tmpRemoteArrival[j] == 0)
                                    {
                                        if (_RemoteArrivalGateName[j] != null)
                                            sGateName = _RemoteArrivalGateName[j];
                                        else
                                            sGateName = "";

                                        bool index = sGateName.Contains(tmpGate.GateOfArrival1);
                                        if (!index)
                                        {
                                            if (string.Compare(_RemoteArrivalGateName[j], null, false) != 0)
                                                _RemoteArrivalGateName[j] = _RemoteArrivalGateName[j] + ",";
                                            _RemoteArrivalGateName[j] = _RemoteArrivalGateName[j] + tmpGate.GateOfArrival1;
                                            _remoteArrivalGate[j]++;
                                            tmpRemoteArrival[j]++;
                                        }
                                    }
                                }
                                else
                                {
                                    if (tmpContactArrival[j] != 0)
                                        continue;

                                    if (_ContactArrivalGateName[j] != null)
                                        sGateName = _ContactArrivalGateName[j];
                                    else
                                        sGateName = "";


                                    bool index = sGateName.Contains(tmpGate.GateOfArrival1);
                                    if (!index)
                                    {
                                        if (string.Compare(_ContactArrivalGateName[j], null, false) != 0)
                                            _ContactArrivalGateName[j] = _ContactArrivalGateName[j] + ",";
                                        _ContactArrivalGateName[j] = _ContactArrivalGateName[j] + tmpGate.GateOfArrival1;
                                        _contactArrivalGate[j]++;
                                        tmpContactArrival[j]++;
                                    }
                                }
                            }
                        }

                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        /// <summary>
        /// Get Schedule Departure Flight Usage Data for the time frame
        /// </summary>
        private string GetScheduleDepartureFlightUsageList()
        {
            try
            {
                if (objDepFlights.Count > 0)
                {
                    foreach (EntFlight tmpDepartureFlights in objDepFlights)
                    {
                        {
                            if (objGateList != null)
                            {
                                EntGate objScheduleGate = objGateList.FirstOrDefault(d => String.Compare(d.GateName, tmpDepartureFlights.GateOfDeparture1, false) == 0);
                                if (objScheduleGate != null)
                                {
                                    if (tmpDepartureFlights.DepartureScheduleBeginForGate1 != null)
                                    {
                                        dtBegin = tmpDepartureFlights.DepartureScheduleBeginForGate1;
                                    }
                                    if (tmpDepartureFlights.DepartureScheduleEndForGate1 != null)
                                    {
                                        dtEnd = tmpDepartureFlights.DepartureScheduleEndForGate1;
                                    }
                                    if (dtBegin.HasValue && dtEnd.HasValue)
                                    {
                                        if (dtBegin <= DateFrom)
                                            dtBegin = DateFrom;
                                        if (dtEnd >= DateTo)
                                            dtEnd = DateTo;

                                        DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                                        int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                                        int toX = dtEnd.Value.Subtract(tempTime).Hours;

                                        for (int j = fromX; j <= toX; j++)
                                        {
                                            if (String.Compare(objScheduleGate.BusGate, "X", false) == 0)
                                            {
                                                if (String.Compare(_RemoteDepartureFlightNo[j], null, false) != 0)
                                                    _RemoteDepartureFlightNo[j] = _RemoteDepartureFlightNo[j] + ",";
                                                _RemoteDepartureFlightNo[j] = _RemoteDepartureFlightNo[j] + tmpDepartureFlights.FullFlightNumber;
                                                _remoteDepartureFlight[j]++;
                                            }
                                            else
                                            {
                                                if (String.Compare(_ContactDepartureFlightNo[j], null, false) != 0)
                                                    _ContactDepartureFlightNo[j] = _ContactDepartureFlightNo[j] + ",";
                                                _ContactDepartureFlightNo[j] = _ContactDepartureFlightNo[j] + tmpDepartureFlights.FullFlightNumber;
                                                _contactDepartureFlight[j]++;
                                            }
                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        /// <summary>
        /// Get Schedule Departure Gate Usage Data for the time frame
        /// </summary>
        private string GetScheduleDepartureGateUsageList()
        {
            try
            {
                if (objGateList.Count > 0 && objDepFlights.Count > 0)
                {
                    foreach (EntGate tmpScheduleGate in objGateList)
                    {
                        double[] tmpRemoteDeparture = new double[totalDurations];
                        double[] tmpContactDeparture = new double[totalDurations];

                        IEnumerable<EntFlight> tmpObjArrivalGates = objDepFlights.Where<EntFlight>(g => String.Compare(g.GateOfDeparture1, tmpScheduleGate.GateName, false) == 0);

                        foreach (EntFlight tmpGate in tmpObjArrivalGates)
                        {
                            if (tmpGate.DepartureScheduleBeginForGate1 != null)
                            {
                                dtBegin = tmpGate.DepartureScheduleBeginForGate1;
                            }
                            if (tmpGate.DepartureScheduleEndForGate1 != null)
                            {
                                dtEnd = tmpGate.DepartureScheduleEndForGate1;
                            }
                            if (dtBegin.HasValue && dtEnd.HasValue)
                            {
                                if (dtBegin <= DateFrom)
                                    dtBegin = DateFrom;
                                if (dtEnd >= DateTo)
                                    dtEnd = DateTo;

                                DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                                int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                                int toX = dtEnd.Value.Subtract(tempTime).Hours;

                                for (int j = fromX; j <= toX; j++)
                                {
                                    if (String.Compare(tmpScheduleGate.BusGate, "X", false) == 0)
                                    {
                                        if (tmpRemoteDeparture[j] == 0)
                                        {
                                            if (_RemoteDepartureGateName[j] != null)
                                                sGateName = _RemoteDepartureGateName[j];
                                            else
                                                sGateName = "";

                                            bool index = sGateName.Contains(tmpGate.GateOfDeparture1);
                                            if (!index)
                                            {
                                                if (string.Compare(_RemoteDepartureGateName[j], null, false) != 0)
                                                    _RemoteDepartureGateName[j] = _RemoteDepartureGateName[j] + ",";
                                                _RemoteDepartureGateName[j] = _RemoteDepartureGateName[j] + tmpGate.GateOfDeparture1;
                                                _remoteDepartureGate[j]++;
                                                tmpRemoteDeparture[j]++;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (tmpContactDeparture[j] != 0)
                                            continue;

                                        if (_ContactDepartureGateName[j] != null)
                                            sGateName = _ContactDepartureGateName[j];
                                        else
                                            sGateName = "";


                                        bool index = sGateName.Contains(tmpGate.GateOfDeparture1);
                                        if (!index)
                                        {
                                            if (string.Compare(_ContactDepartureGateName[j], null, false) != 0)
                                                _ContactDepartureGateName[j] = _ContactDepartureGateName[j] + ",";
                                            _ContactDepartureGateName[j] = _ContactDepartureGateName[j] + tmpGate.GateOfDeparture1;
                                            tmpContactDeparture[j]++;
                                            _contactDepartureGate[j]++;

                                        }
                                    }
                                }
                            }
                        }

                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        #endregion

        #region +++ Gate 2 +++
        /// <summary>
        /// Get Schedule Arrival Flight Usage list for Gate2
        /// </summary>
        /// <returns></returns>
        private string GetScheduleArrivalFlightUsageList2()
        {
            try
            {
                if (objArrivalFlights2.Count > 0)
                {
                    foreach (EntFlight tmpArrivalFlights in objArrivalFlights2)
                    {
                        if (objGateList == null)
                            continue;

                        EntGate objScheduleGate = objGateList.FirstOrDefault(d => String.Compare(d.GateName, tmpArrivalFlights.GateOfArrival2, false) == 0);

                        if (objScheduleGate == null)
                            continue;

                        if (tmpArrivalFlights.ArrivalScheduleBeginForGate2 != null)
                        {
                            dtBegin = tmpArrivalFlights.ArrivalScheduleBeginForGate2;
                        }
                        if (tmpArrivalFlights.ArrivalScheduleEndForGate2 != null)
                        {
                            dtEnd = tmpArrivalFlights.ArrivalScheduleEndForGate2;
                        }

                        if (!(dtBegin.HasValue && dtEnd.HasValue))
                            continue;

                        if (dtBegin <= DateFrom)
                            dtBegin = DateFrom;
                        if (dtEnd >= DateTo)
                            dtEnd = DateTo;

                        DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                        int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                        int toX = dtEnd.Value.Subtract(tempTime).Hours;
                        for (int j = fromX; j <= toX; j++)
                        {
                            if (String.Compare(objScheduleGate.BusGate, "X", false) == 0)
                            {
                                if (String.Compare(_RemoteArrivalFlightNo[j], null, false) != 0)
                                    _RemoteArrivalFlightNo[j] = _RemoteArrivalFlightNo[j] + ",";
                                _RemoteArrivalFlightNo[j] = _RemoteArrivalFlightNo[j] + tmpArrivalFlights.FullFlightNumber;
                                _remoteArrivalFlight[j]++;
                            }
                            else
                            {
                                if (String.Compare(_ContactArrivalFlightNo[j], null, false) != 0)
                                    _ContactArrivalFlightNo[j] = _ContactArrivalFlightNo[j] + ",";
                                _ContactArrivalFlightNo[j] = _ContactArrivalFlightNo[j] + tmpArrivalFlights.FullFlightNumber;
                                _contactArrivalFlight[j]++;
                            }
                        }
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        /// <summary>
        /// Get Schedule Arrival Gate 2 Usage List 
        /// </summary>
        /// <returns></returns>
        private string GetScheduleArrivalGateUsageList2()
        {
            try
            {

                if (objGateList.Count > 0 && objArrivalFlights2.Count > 0)
                {
                    foreach (EntGate tmpScheduleGate in objGateList)
                    {
                        double[] tmpRemoteArrival = new double[totalDurations];
                        double[] tmpContactArrival = new double[totalDurations];

                        IEnumerable<EntFlight> tmpObjArrivalGates = objArrivalFlights2.Where<EntFlight>(g => String.Compare(g.GateOfArrival2, tmpScheduleGate.GateName, false) == 0);

                        foreach (EntFlight tmpGate in tmpObjArrivalGates)
                        {
                            if (tmpGate.ArrivalScheduleBeginForGate2 != null)
                            {
                                dtBegin = tmpGate.ArrivalScheduleBeginForGate2;
                            }
                            if (tmpGate.ArrivalScheduleEndForGate2 != null)
                            {
                                dtEnd = tmpGate.ArrivalScheduleEndForGate2;
                            }
                            if (!(dtBegin.HasValue && dtEnd.HasValue))
                                continue;

                            if (dtBegin <= DateFrom)
                                dtBegin = DateFrom;
                            if (dtEnd >= DateTo)
                                dtEnd = DateTo;

                            DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                            int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                            int toX = dtEnd.Value.Subtract(tempTime).Hours;

                            for (int j = fromX; j <= toX; j++)
                            {
                                if (String.Compare(tmpScheduleGate.BusGate, "X", false) == 0)
                                {
                                    if (tmpRemoteArrival[j] == 0)
                                    {
                                        if (_RemoteArrivalGateName[j] != null)
                                            sGateName = _RemoteArrivalGateName[j];
                                        else
                                            sGateName = "";

                                        bool index = sGateName.Contains(tmpGate.GateOfArrival2);
                                        if (!index)
                                        {
                                            if (string.Compare(_RemoteArrivalGateName[j], null, false) != 0)
                                                _RemoteArrivalGateName[j] = _RemoteArrivalGateName[j] + ",";
                                            _RemoteArrivalGateName[j] = _RemoteArrivalGateName[j] + tmpGate.GateOfArrival2;
                                            _remoteArrivalGate[j]++;
                                            tmpRemoteArrival[j]++;
                                        }
                                    }
                                }
                                else
                                {
                                    if (tmpContactArrival[j] != 0)
                                        continue;

                                    if (_ContactArrivalGateName[j] != null)
                                        sGateName = _ContactArrivalGateName[j];
                                    else
                                        sGateName = "";


                                    bool index = sGateName.Contains(tmpGate.GateOfArrival2);
                                    if (!index)
                                    {
                                        if (string.Compare(_ContactArrivalGateName[j], null, false) != 0)
                                            _ContactArrivalGateName[j] = _ContactArrivalGateName[j] + ",";
                                        _ContactArrivalGateName[j] = _ContactArrivalGateName[j] + tmpGate.GateOfArrival2;
                                        _contactArrivalGate[j]++;
                                        tmpContactArrival[j]++;
                                    }
                                }
                            }
                        }

                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        /// <summary>
        /// Get Schedule Depature flight usage list
        /// </summary>
        /// <returns></returns>
        private string GetScheduleDepartureFlightUsageList2()
        {
            try
            {
                if (objDepFlights2.Count > 0)
                {
                    foreach (EntFlight tmpDepartureFlights in objDepFlights2)
                    {
                        {
                            if (objGateList != null)
                            {
                                EntGate objScheduleGate = objGateList.FirstOrDefault(d => String.Compare(d.GateName, tmpDepartureFlights.GateOfDeparture2, false) == 0);
                                if (objScheduleGate != null)
                                {
                                    if (tmpDepartureFlights.DepartureScheduleBeginForGate2 != null)
                                    {
                                        dtBegin = tmpDepartureFlights.DepartureScheduleBeginForGate2;
                                    }
                                    if (tmpDepartureFlights.DepartureScheduleEndForGate2 != null)
                                    {
                                        dtEnd = tmpDepartureFlights.DepartureScheduleEndForGate2;
                                    }
                                    if (dtBegin.HasValue && dtEnd.HasValue)
                                    {
                                        if (dtBegin <= DateFrom)
                                            dtBegin = DateFrom;
                                        if (dtEnd >= DateTo)
                                            dtEnd = DateTo;

                                        DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                                        int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                                        int toX = dtEnd.Value.Subtract(tempTime).Hours;

                                        for (int j = fromX; j <= toX; j++)
                                        {
                                            if (String.Compare(objScheduleGate.BusGate, "X", false) == 0)
                                            {
                                                if (String.Compare(_RemoteDepartureFlightNo[j], null, false) != 0)
                                                    _RemoteDepartureFlightNo[j] = _RemoteDepartureFlightNo[j] + ",";
                                                _RemoteDepartureFlightNo[j] = _RemoteDepartureFlightNo[j] + tmpDepartureFlights.FullFlightNumber;
                                                _remoteDepartureFlight[j]++;
                                            }
                                            else
                                            {
                                                if (String.Compare(_ContactDepartureFlightNo[j], null, false) != 0)
                                                    _ContactDepartureFlightNo[j] = _ContactDepartureFlightNo[j] + ",";
                                                _ContactDepartureFlightNo[j] = _ContactDepartureFlightNo[j] + tmpDepartureFlights.FullFlightNumber;
                                                _contactDepartureFlight[j]++;
                                            }
                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        /// <summary>
        /// Get Schedule Departure Gate2 Usage List
        /// </summary>
        /// <returns></returns>
        private string GetScheduleDepartureGateUsageList2()
        {
            try
            {
                if (objGateList.Count > 0 && objDepFlights2.Count > 0)
                {
                    foreach (EntGate tmpScheduleGate in objGateList)
                    {
                        double[] tmpRemoteDeparture = new double[totalDurations];
                        double[] tmpContactDeparture = new double[totalDurations];

                        IEnumerable<EntFlight> tmpObjArrivalGates = objDepFlights2.Where<EntFlight>(g => String.Compare(g.GateOfDeparture2, tmpScheduleGate.GateName, false) == 0);

                        foreach (EntFlight tmpGate in tmpObjArrivalGates)
                        {
                            if (tmpGate.DepartureScheduleBeginForGate2 != null)
                            {
                                dtBegin = tmpGate.DepartureScheduleBeginForGate2;
                            }
                            if (tmpGate.DepartureScheduleEndForGate2 != null)
                            {
                                dtEnd = tmpGate.DepartureScheduleEndForGate2;
                            }
                            if (dtBegin.HasValue && dtEnd.HasValue)
                            {
                                if (dtBegin <= DateFrom)
                                    dtBegin = DateFrom;
                                if (dtEnd >= DateTo)
                                    dtEnd = DateTo;

                                DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                                int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                                int toX = dtEnd.Value.Subtract(tempTime).Hours;

                                for (int j = fromX; j <= toX; j++)
                                {
                                    if (String.Compare(tmpScheduleGate.BusGate, "X", false) == 0)
                                    {
                                        if (tmpRemoteDeparture[j] == 0)
                                        {
                                            if (_RemoteDepartureGateName[j] != null)
                                                sGateName = _RemoteDepartureGateName[j];
                                            else
                                                sGateName = "";

                                            bool index = sGateName.Contains(tmpGate.GateOfDeparture2);
                                            if (!index)
                                            {
                                                if (string.Compare(_RemoteDepartureGateName[j], null, false) != 0)
                                                    _RemoteDepartureGateName[j] = _RemoteDepartureGateName[j] + ",";
                                                _RemoteDepartureGateName[j] = _RemoteDepartureGateName[j] + tmpGate.GateOfDeparture2;
                                                _remoteDepartureGate[j]++;
                                                tmpRemoteDeparture[j]++;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (tmpContactDeparture[j] != 0)
                                            continue;

                                        if (_ContactDepartureGateName[j] != null)
                                            sGateName = _ContactDepartureGateName[j];
                                        else
                                            sGateName = "";


                                        bool index = sGateName.Contains(tmpGate.GateOfDeparture2);
                                        if (!index)
                                        {
                                            if (string.Compare(_ContactDepartureGateName[j], null, false) != 0)
                                                _ContactDepartureGateName[j] = _ContactDepartureGateName[j] + ",";
                                            _ContactDepartureGateName[j] = _ContactDepartureGateName[j] + tmpGate.GateOfDeparture2;
                                            tmpContactDeparture[j]++;
                                            _contactDepartureGate[j]++;

                                        }
                                    }
                                }
                            }
                        }

                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        #endregion
        #endregion

        #region +++ Actual +++
        #region +++ Gate 1 +++
        /// <summary>
        /// Get Actual Arrival Flight Usage Data for the time frame
        /// </summary>
        private string GetActualArrivalFlightUsageList()
        {
            try
            {

                if (objArrivalFlights.Count > 0)
                {
                    foreach (EntFlight tmpArrivalFlights in objArrivalFlights)
                    {
                        if (objGateList == null)
                                continue;

                            EntGate objActualGate = objGateList.FirstOrDefault(d => String.Compare(d.GateName, tmpArrivalFlights.GateOfArrival1, false) == 0);
                            if (objActualGate == null)
                                continue;

                            if (tmpArrivalFlights.ArrivalActualBeginForGate1 != null && tmpArrivalFlights.ArrivalActualEndForGate1 != null)
                            {
                                dtBegin = tmpArrivalFlights.ArrivalActualBeginForGate1;
                                dtEnd = tmpArrivalFlights.ArrivalActualEndForGate1;
                            }
                            //else
                            //{
                            //    objArrivalFlights.Remove(tmpArrivalFlights);
                            //}
                           
                            if (!(dtBegin.HasValue && dtEnd.HasValue))
                                continue;

                            if (dtBegin <= DateFrom)
                                dtBegin = DateFrom;
                            if (dtEnd >= DateTo)
                                dtEnd = DateTo;

                            DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                            int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                            int toX = dtEnd.Value.Subtract(tempTime).Hours;

                            for (int j = fromX; j <= toX; j++)
                            {
                                if (String.Compare(objActualGate.BusGate, "X", false) == 0)
                                {
                                    if (String.Compare(_RemoteArrivalFlightNo[j], null, false) != 0)
                                        _RemoteArrivalFlightNo[j] = _RemoteArrivalFlightNo[j] + ",";
                                    _RemoteArrivalFlightNo[j] += _RemoteArrivalFlightNo[j] + tmpArrivalFlights.FullFlightNumber;
                                    _remoteArrivalFlight[j]++;
                                }
                                else
                                {
                                    if (String.Compare(_ContactArrivalFlightNo[j], null, false) != 0)
                                        _ContactArrivalFlightNo[j] = _ContactArrivalFlightNo[j] + ",";
                                    _ContactArrivalFlightNo[j] = _ContactArrivalFlightNo[j] + tmpArrivalFlights.FullFlightNumber;
                                    _contactArrivalFlight[j]++;

                                }
                            }
                        }
                    }
               return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        /// <summary>
        /// Get Actual Arrival Gate Usage Data for the time frame
        /// </summary>
        private string GetActualArrivalGateUsageList()
        {
            try
            {
                if (objGateList.Count > 0 && objArrivalFlights.Count > 0)
                {
                    foreach (EntGate tmpScheduleGate in objGateList)
                    {
                        double[] tmpRemoteArrival = new double[totalDurations];
                        double[] tmpContactArrival = new double[totalDurations];
                        IEnumerable<EntFlight> tmpObjArrivalGates = null;
                        foreach (EntFlight tmpGate in tmpObjArrivalGates)
                        {
                            if (tmpGate.ArrivalActualBeginForGate1 != null && tmpGate.ArrivalActualEndForGate1 != null)
                            {
                                dtBegin = tmpGate.ArrivalActualBeginForGate1;
                                dtEnd = tmpGate.ArrivalActualEndForGate1;
                           
                            if (!(dtBegin.HasValue && dtEnd.HasValue))
                                continue;

                            if (dtBegin <= DateFrom)
                                dtBegin = DateFrom;
                            if (dtEnd >= DateTo)
                                dtEnd = DateTo;

                            DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                            int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                            int toX = dtEnd.Value.Subtract(tempTime).Hours;

                            for (int j = fromX; j <= toX; j++)
                            {
                                if (String.Compare(tmpScheduleGate.BusGate, "X", false) == 0)
                                {
                                    if (tmpRemoteArrival[j] == 0)
                                    {
                                        if (_RemoteArrivalGateName[j] != null)
                                            sGateName = _RemoteArrivalGateName[j];
                                        else
                                            sGateName = "";


                                        bool index = sGateName.Contains(tmpGate.GateOfArrival1);
                                        if (!index)
                                        {
                                            if (string.Compare(_RemoteArrivalGateName[j], null, false) != 0)
                                                _RemoteArrivalGateName[j] = _RemoteArrivalGateName[j] + ",";
                                            _RemoteArrivalGateName[j] = _RemoteArrivalGateName[j] + tmpGate.GateOfArrival1;
                                            _remoteArrivalGate[j]++;
                                            tmpRemoteArrival[j]++;
                                        }

                                    }
                                }
                                else
                                {
                                    if (tmpContactArrival[j] != 0)
                                        continue;

                                    if (_ContactArrivalGateName[j] != null)
                                        sGateName = _ContactArrivalGateName[j];
                                    else
                                        sGateName = "";

                                    bool index = sGateName.Contains(tmpGate.GateOfArrival1);
                                    if (!index)
                                    {
                                        if (string.Compare(_ContactArrivalGateName[j], null, false) != 0)
                                            _ContactArrivalGateName[j] = _ContactArrivalGateName[j] + ",";
                                        _ContactArrivalGateName[j] = _ContactArrivalGateName[j] + tmpGate.GateOfArrival1;
                                        _contactArrivalGate[j]++;
                                        tmpContactArrival[j]++;
                                    }

                                }
                            }
                        }
                    }
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        /// <summary>
        /// Get Actual Departure Flight Usage Data for the time frame
        /// </summary>
        private string GetActualDepartureFlightUsageList()
        {
            try
            {
                if (objDepFlights.Count > 0)
                {
                    foreach (EntFlight tmpDepartureFlights in objDepFlights)
                    {
                         if (objGateList != null)
                            {
                                EntGate objActualGate = objGateList.FirstOrDefault(d => String.Compare(d.GateName, tmpDepartureFlights.GateOfDeparture1, false) == 0);
                                if (objActualGate != null)
                                {
                                    if (tmpDepartureFlights.DepartureActualBeginForGate1 != null && tmpDepartureFlights.DepartureActualEndForGate1 != null)
                                    {
                                        dtBegin = tmpDepartureFlights.DepartureActualBeginForGate1;
                                        dtEnd = tmpDepartureFlights.DepartureActualEndForGate1;
                                    }
                                    //else
                                    //{
                                    //    objDepFlights.Remove(tmpDepartureFlights);
                                    //}
                                    if (dtBegin.HasValue && dtEnd.HasValue)
                                    {
                                        if (dtBegin <= DateFrom)
                                            dtBegin = DateFrom;
                                        if (dtEnd >= DateTo)
                                            dtEnd = DateTo;

                                        DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                                        int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                                        int toX = dtEnd.Value.Subtract(tempTime).Hours;

                                        for (int j = fromX; j <= toX; j++)
                                        {
                                            if (String.Compare(objActualGate.BusGate, "X", false) == 0)
                                            {
                                                if (String.Compare(_RemoteDepartureFlightNo[j], null, false) != 0)
                                                    _RemoteDepartureFlightNo[j] = _RemoteDepartureFlightNo[j] + ",";
                                                _RemoteDepartureFlightNo[j] = _RemoteDepartureFlightNo[j] + tmpDepartureFlights.FullFlightNumber;

                                                _remoteDepartureFlight[j]++;
                                            }
                                            else
                                            {
                                                if (String.Compare(_ContactDepartureFlightNo[j], null, false) != 0)
                                                    _ContactDepartureFlightNo[j] = _ContactDepartureFlightNo[j] + ",";
                                                _ContactDepartureFlightNo[j] = _ContactDepartureFlightNo[j] + tmpDepartureFlights.FullFlightNumber;

                                                _contactDepartureFlight[j]++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                   return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        /// <summary>
        /// Get Actual Departure Gate Usage Data for the time frame
        /// </summary>
        private string GetActualDepartureGateUsageList()
        {
            try
            {

                if (objGateList.Count > 0 && objDepFlights.Count > 0)
                {
                    foreach (EntGate tmpScheduleGate in objGateList)
                    {
                        double[] tmpRemoteDeparture = new double[totalDurations];
                        double[] tmpContactDeparture = new double[totalDurations];

                        IEnumerable<EntFlight> tmpObjArrivalGates = objDepFlights.Where<EntFlight>(g => String.Compare(g.GateOfDeparture1, tmpScheduleGate.GateName, false) == 0);

                        foreach (EntFlight tmpGate in tmpObjArrivalGates)
                        {
                            if (tmpGate.DepartureActualBeginForGate1 != null && tmpGate.DepartureActualEndForGate1 != null)
                            {
                                dtBegin = tmpGate.DepartureActualBeginForGate1;
                                dtEnd = tmpGate.DepartureActualEndForGate1;

                                if (dtBegin.HasValue && dtEnd.HasValue)
                                {
                                    if (dtBegin <= DateFrom)
                                        dtBegin = DateFrom;
                                    if (dtEnd >= DateTo)
                                        dtEnd = DateTo;

                                    DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                                    int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                                    int toX = dtEnd.Value.Subtract(tempTime).Hours;

                                    for (int j = fromX; j <= toX; j++)
                                    {
                                        if (String.Compare(tmpScheduleGate.BusGate, "X", false) == 0)
                                        {
                                            if (tmpRemoteDeparture[j] == 0)
                                            {
                                                if (_RemoteDepartureGateName[j] != null)
                                                    sGateName = _RemoteDepartureGateName[j];
                                                else
                                                    sGateName = "";

                                                bool index = sGateName.Contains(tmpGate.GateOfDeparture1);
                                                if (!index)
                                                {
                                                    if (string.Compare(_RemoteDepartureGateName[j], null, false) != 0)
                                                        _RemoteDepartureGateName[j] = _RemoteDepartureGateName[j] + ",";
                                                    _RemoteDepartureGateName[j] = _RemoteDepartureGateName[j] + tmpGate.GateOfDeparture1;
                                                    _remoteDepartureGate[j]++;
                                                    tmpRemoteDeparture[j]++;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (tmpContactDeparture[j] != 0)
                                                continue;

                                            if (_ContactDepartureGateName[j] != null)
                                                sGateName = _ContactDepartureGateName[j];
                                            else
                                                sGateName = "";


                                            bool index = sGateName.Contains(tmpGate.GateOfDeparture1);
                                            if (!index)
                                            {
                                                if (string.Compare(_ContactDepartureGateName[j], null, false) != 0)
                                                    _ContactDepartureGateName[j] = _ContactDepartureGateName[j] + ",";
                                                _ContactDepartureGateName[j] = _ContactDepartureGateName[j] + tmpGate.GateOfDeparture1;
                                                _contactDepartureGate[j]++;
                                                tmpContactDeparture[j]++;

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        #endregion

        #region +++ Gate 2 +++
        /// <summary>
        /// Get Actual Arrival Flight Usage List2
        /// </summary>
        /// <returns></returns>
        private string GetActualArrivalFlightUsageList2()
        {
            try
            {

                if (objArrivalFlights2.Count > 0)
                {
                    foreach (EntFlight tmpArrivalFlights in objArrivalFlights2)
                    {
                       
                            if (objGateList == null)
                                continue;

                            EntGate objActualGate = objGateList.FirstOrDefault(d => String.Compare(d.GateName, tmpArrivalFlights.GateOfArrival2, false) == 0);
                            if (objActualGate == null)
                                continue;

                            if (tmpArrivalFlights.ArrivalActualBeginForGate2 != null && tmpArrivalFlights.ArrivalActualEndForGate2 != null)
                            {
                                dtBegin = tmpArrivalFlights.ArrivalActualBeginForGate2;
                                dtEnd = tmpArrivalFlights.ArrivalActualEndForGate2;
                            }
                            //else
                            //{                                
                            //    objArrivalFlights.Remove(tmpArrivalFlights);
                            //}
                            if (!(dtBegin.HasValue && dtEnd.HasValue))
                                continue;

                            if (dtBegin <= DateFrom)
                                dtBegin = DateFrom;
                            if (dtEnd >= DateTo)
                                dtEnd = DateTo;

                            DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                            int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                            int toX = dtEnd.Value.Subtract(tempTime).Hours;

                            for (int j = fromX; j <= toX; j++)
                            {
                                if (String.Compare(objActualGate.BusGate, "X", false) == 0)
                                {
                                    if (String.Compare(_RemoteArrivalFlightNo[j], null, false) != 0)
                                        _RemoteArrivalFlightNo[j] = _RemoteArrivalFlightNo[j] + ",";
                                    _RemoteArrivalFlightNo[j] += _RemoteArrivalFlightNo[j] + tmpArrivalFlights.FullFlightNumber;
                                    _remoteArrivalFlight[j]++;
                                }
                                else
                                {
                                    if (String.Compare(_ContactArrivalFlightNo[j], null, false) != 0)
                                        _ContactArrivalFlightNo[j] = _ContactArrivalFlightNo[j] + ",";
                                    _ContactArrivalFlightNo[j] = _ContactArrivalFlightNo[j] + tmpArrivalFlights.FullFlightNumber;
                                    _contactArrivalFlight[j]++;

                                }
                            }
                        }
                    }
                  return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }        
        /// <summary>
        /// Get Actual Arrival Gate2 Usage List
        /// </summary>
        /// <returns></returns>
        private string GetActualArrivalGateUsageList2()
        {
            try
            {
                if (objGateList.Count > 0 && objArrivalFlights2.Count > 0)
                {
                    foreach (EntGate tmpScheduleGate in objGateList)
                    {
                        double[] tmpRemoteArrival = new double[totalDurations];
                        double[] tmpContactArrival = new double[totalDurations];

                        IEnumerable<EntFlight> tmpObjArrivalGates = objArrivalFlights2.Where<EntFlight>(g => String.Compare(g.GateOfArrival2, tmpScheduleGate.GateName, false) == 0);

                        foreach (EntFlight tmpGate in tmpObjArrivalGates)
                        {
                            if (tmpGate.ArrivalActualBeginForGate2 != null && tmpGate.ArrivalActualEndForGate2 != null)
                            {
                                dtBegin = tmpGate.ArrivalActualBeginForGate2;
                                dtEnd = tmpGate.ArrivalActualEndForGate2;


                                if (!(dtBegin.HasValue && dtEnd.HasValue))
                                    continue;

                                if (dtBegin <= DateFrom)
                                    dtBegin = DateFrom;
                                if (dtEnd >= DateTo)
                                    dtEnd = DateTo;

                                DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                                int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                                int toX = dtEnd.Value.Subtract(tempTime).Hours;

                                for (int j = fromX; j <= toX; j++)
                                {
                                    if (String.Compare(tmpScheduleGate.BusGate, "X", false) == 0)
                                    {
                                        if (tmpRemoteArrival[j] == 0)
                                        {
                                            if (_RemoteArrivalGateName[j] != null)
                                                sGateName = _RemoteArrivalGateName[j];
                                            else
                                                sGateName = "";


                                            bool index = sGateName.Contains(tmpGate.GateOfArrival2);
                                            if (!index)
                                            {
                                                if (string.Compare(_RemoteArrivalGateName[j], null, false) != 0)
                                                    _RemoteArrivalGateName[j] = _RemoteArrivalGateName[j] + ",";
                                                _RemoteArrivalGateName[j] = _RemoteArrivalGateName[j] + tmpGate.GateOfArrival2;
                                                _remoteArrivalGate[j]++;
                                                tmpRemoteArrival[j]++;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        if (tmpContactArrival[j] != 0)
                                            continue;

                                        if (_ContactArrivalGateName[j] != null)
                                            sGateName = _ContactArrivalGateName[j];
                                        else
                                            sGateName = "";

                                        bool index = sGateName.Contains(tmpGate.GateOfArrival2);
                                        if (!index)
                                        {
                                            if (string.Compare(_ContactArrivalGateName[j], null, false) != 0)
                                                _ContactArrivalGateName[j] = _ContactArrivalGateName[j] + ",";
                                            _ContactArrivalGateName[j] = _ContactArrivalGateName[j] + tmpGate.GateOfArrival2;
                                            _contactArrivalGate[j]++;
                                            tmpContactArrival[j]++;
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }        
        /// <summary>
        /// Get Actual Departure Flight Usage List2
        /// </summary>
        /// <returns></returns>
        private string GetActualDepartureFlightUsageList2()
        {
            try
            {
                if (objDepFlights2.Count > 0)
                {
                    foreach (EntFlight tmpDepartureFlights in objDepFlights2)
                    {
                            
                            if (objGateList != null)
                            {
                                EntGate objActualGate = objGateList.FirstOrDefault(d => String.Compare(d.GateName, tmpDepartureFlights.GateOfDeparture2, false) == 0);
                                if (objActualGate != null)
                                {
                                    if (tmpDepartureFlights.DepartureActualBeginForGate2 != null && tmpDepartureFlights.DepartureActualEndForGate2 != null)
                                    {
                                        dtBegin = tmpDepartureFlights.DepartureActualBeginForGate2;
                                        dtEnd = tmpDepartureFlights.DepartureActualEndForGate2;
                                    }
                                    //else
                                    //{
                                    //    objDepFlights2.Remove(tmpDepartureFlights);
                                    //}
                                    if (dtBegin.HasValue && dtEnd.HasValue)
                                    {
                                        if (dtBegin <= DateFrom)
                                            dtBegin = DateFrom;
                                        if (dtEnd >= DateTo)
                                            dtEnd = DateTo;

                                        DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                                        int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                                        int toX = dtEnd.Value.Subtract(tempTime).Hours;

                                        for (int j = fromX; j <= toX; j++)
                                        {
                                            if (String.Compare(objActualGate.BusGate, "X", false) == 0)
                                            {
                                                if (String.Compare(_RemoteDepartureFlightNo[j], null, false) != 0)
                                                    _RemoteDepartureFlightNo[j] = _RemoteDepartureFlightNo[j] + ",";
                                                _RemoteDepartureFlightNo[j] = _RemoteDepartureFlightNo[j] + tmpDepartureFlights.FullFlightNumber;

                                                _remoteDepartureFlight[j]++;
                                            }
                                            else
                                            {
                                                if (String.Compare(_ContactDepartureFlightNo[j], null, false) != 0)
                                                    _ContactDepartureFlightNo[j] = _ContactDepartureFlightNo[j] + ",";
                                                _ContactDepartureFlightNo[j] = _ContactDepartureFlightNo[j] + tmpDepartureFlights.FullFlightNumber;

                                                _contactDepartureFlight[j]++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }        
        /// <summary>
        /// Get Actual Departure Gate 2 Usage List
        /// </summary>
        /// <returns></returns>
        private string GetActualDepartureGateUsageList2()
        {
            try
            {

                if (objGateList.Count > 0 && objDepFlights2.Count > 0)
                {
                    foreach (EntGate tmpScheduleGate in objGateList)
                    {
                        double[] tmpRemoteDeparture = new double[totalDurations];
                        double[] tmpContactDeparture = new double[totalDurations];

                        IEnumerable<EntFlight> tmpObjArrivalGates = objDepFlights2.Where<EntFlight>(g => String.Compare(g.GateOfDeparture2, tmpScheduleGate.GateName, false) == 0);

                        foreach (EntFlight tmpGate in tmpObjArrivalGates)
                        {
                            if (tmpGate.DepartureActualBeginForGate2 != null && tmpGate.DepartureActualEndForGate2 != null)
                            {
                                dtBegin = tmpGate.DepartureActualBeginForGate2;
                                dtEnd = tmpGate.DepartureActualEndForGate2;

                                if (dtBegin.HasValue && dtEnd.HasValue)
                                {
                                    if (dtBegin <= DateFrom)
                                        dtBegin = DateFrom;
                                    if (dtEnd >= DateTo)
                                        dtEnd = DateTo;

                                    DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                                    int fromX = dtBegin.Value.Subtract(tempTime).Hours;
                                    int toX = dtEnd.Value.Subtract(tempTime).Hours;

                                    for (int j = fromX; j <= toX; j++)
                                    {
                                        if (String.Compare(tmpScheduleGate.BusGate, "X", false) == 0)
                                        {
                                            if (tmpRemoteDeparture[j] == 0)
                                            {
                                                if (_RemoteDepartureGateName[j] != null)
                                                    sGateName = _RemoteDepartureGateName[j];
                                                else
                                                    sGateName = "";

                                                bool index = sGateName.Contains(tmpGate.GateOfDeparture2);
                                                if (!index)
                                                {
                                                    if (string.Compare(_RemoteDepartureGateName[j], null, false) != 0)
                                                        _RemoteDepartureGateName[j] = _RemoteDepartureGateName[j] + ",";
                                                    _RemoteDepartureGateName[j] = _RemoteDepartureGateName[j] + tmpGate.GateOfDeparture2;
                                                    _remoteDepartureGate[j]++;
                                                    tmpRemoteDeparture[j]++;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (tmpContactDeparture[j] != 0)
                                                continue;

                                            if (_ContactDepartureGateName[j] != null)
                                                sGateName = _ContactDepartureGateName[j];
                                            else
                                                sGateName = "";


                                            bool index = sGateName.Contains(tmpGate.GateOfDeparture2);
                                            if (!index)
                                            {
                                                if (string.Compare(_ContactDepartureGateName[j], null, false) != 0)
                                                    _ContactDepartureGateName[j] = _ContactDepartureGateName[j] + ",";
                                                _ContactDepartureGateName[j] = _ContactDepartureGateName[j] + tmpGate.GateOfDeparture2;
                                                _contactDepartureGate[j]++;
                                                tmpContactDeparture[j]++;

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        #endregion
        #endregion


        /// <summary>
        /// Create Chart Data Source
        /// </summary>
        /// <param name="sFilter1">System.string containing sFilter1</param>
        /// <param name="sFilter2">Syste.string containing sFilter2</param>        
        private void CreateGateUsageChartDataSource(string sFilter1, string sFilter2)
        {
            switch (sFilter1)
            {
                case "REMOTE":
                    {
                        switch (sFilter2)
                        {
                            case "ARR":
                                {
                                    TimeSpan timeDuration = new TimeSpan(DateFrom.Hour, 00, 0);

                                    for (int i = 0; i < totalDurations; i++)
                                    {
                                        string argument = String.Format("{0}:{1}0", timeDuration.Hours, timeDuration.Minutes);
                                        _argument[i] = argument;
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(argument, _remoteArrivalGate[i], 0.0, 0.0, 0.0));
                                        timeDuration = timeDuration.Add(new TimeSpan(durations,00 , 0));
                                    }
                                    break;
                                }
                            case "DEP":
                                {

                                    TimeSpan timeDuration = new TimeSpan(DateFrom.Hour, 00, 0);

                                    for (int i = 0; i < totalDurations; i++)
                                    {
                                        //string argument = timeDuration.Hours.ToString() + timeDuration.Minutes.ToString();
                                        string argument = String.Format("{0}:{1}0", timeDuration.Hours, timeDuration.Minutes);
                                        _argument[i] = argument;
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(argument, 0.0, _remoteDepartureGate[i], 0.0, 0.0));
                                        timeDuration = timeDuration.Add(new TimeSpan(durations, 00, 0));
                                    }

                                    break;
                                }
                            default:
                                {

                                    TimeSpan timeDuration = new TimeSpan(DateFrom.Hour, 00, 0);

                                    for (int i = 0; i < totalDurations; i++)
                                    {
                                        string argument = String.Format("{0}:{1}0", timeDuration.Hours, timeDuration.Minutes);
                                        _argument[i] = argument;
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(argument, _remoteArrivalGate[i], _remoteDepartureGate[i], 0.0, 0.0));
                                        timeDuration = timeDuration.Add(new TimeSpan(durations, 00, 0));
                                    }
                                    break;
                                }
                        }
                        break;
                    }
                case "CONTACT":
                    {
                        switch (sFilter2)
                        {
                            case "ARR":
                                {
                                    TimeSpan timeDuration = new TimeSpan(DateFrom.Hour, 00, 0);

                                    for (int i = 0; i < totalDurations; i++)
                                    {
                                        string argument = String.Format("{0}:{1}0", timeDuration.Hours, timeDuration.Minutes);
                                        _argument[i] = argument;
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(argument, 0.0, 0.0, _contactArrivalGate[i], 0.0));
                                        timeDuration = timeDuration.Add(new TimeSpan(durations, 00, 0));
                                    }
                                    break;
                                }
                            case "DEP":
                                {
                                    TimeSpan timeDuration = new TimeSpan(DateFrom.Hour, 00, 0);

                                    for (int i = 0; i < totalDurations; i++)
                                    {
                                        string argument = String.Format("{0}:{1}0", timeDuration.Hours, timeDuration.Minutes);
                                        _argument[i] = argument;
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(argument, 0.0, 0.0, 0.0, _contactDepartureGate[i]));
                                        timeDuration = timeDuration.Add(new TimeSpan(durations,00 , 0));
                                    }
                                    break;
                                }
                            default:
                                {
                                    TimeSpan timeDuration = new TimeSpan(DateFrom.Hour, 00, 0);

                                    for (int i = 0; i < totalDurations; i++)
                                    {
                                        string argument = String.Format("{0}:{1}0", timeDuration.Hours, timeDuration.Minutes);
                                        _argument[i] = argument;
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(argument, 0.0, 0.0, _contactArrivalGate[i], _contactDepartureGate[i]));
                                        timeDuration = timeDuration.Add(new TimeSpan(durations, 00, 0));
                                    }
                                    break;
                                }
                        }
                        break;
                    }
                default:
                    {
                        switch (sFilter2)
                        {
                            case "ARR":
                                {

                                    TimeSpan timeDuration = new TimeSpan(DateFrom.Hour, 00, 0);

                                    for (int i = 0; i < totalDurations; i++)
                                    {
                                        string argument = String.Format("{0}:{1}0", timeDuration.Hours, timeDuration.Minutes);
                                        _argument[i] = argument;
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(argument, _remoteArrivalGate[i], 0.0, _contactArrivalGate[i], 0.0));
                                        timeDuration = timeDuration.Add(new TimeSpan(durations, 00, 0));
                                    }

                                    break;
                                }
                            case "DEP":
                                {

                                    TimeSpan timeDuration = new TimeSpan(DateFrom.Hour, 00, 0);

                                    for (int i = 0; i < totalDurations; i++)
                                    {
                                        string argument = String.Format("{0}:{1}0", timeDuration.Hours, timeDuration.Minutes);
                                        _argument[i] = argument;
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(argument, 0.0, _remoteDepartureGate[i], 0.0, _contactDepartureGate[i]));
                                        timeDuration = timeDuration.Add(new TimeSpan(durations, 00, 0));
                                    }

                                    break;
                                }
                            default:
                                {
                                    TimeSpan timeDuration = new TimeSpan(DateFrom.Hour, 00, 0);

                                    for (int i = 0; i < totalDurations; i++)
                                    {
                                        string argument = String.Format("{0}:{1}0", timeDuration.Hours, timeDuration.Minutes);
                                        _argument[i] = argument;
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(argument, _remoteArrivalGate[i], _remoteDepartureGate[i], _contactArrivalGate[i], _contactDepartureGate[i]));
                                        timeDuration = timeDuration.Add(new TimeSpan(durations, 00, 0));
                                    }
                                    break;
                                }
                        }
                        break;
                    }
            }

            objFlightUsageData.Add(_remoteArrivalFlight);
            objFlightUsageData.Add(_remoteDepartureFlight);
            objFlightUsageData.Add(_contactArrivalFlight);
            objFlightUsageData.Add(_contactDepartureFlight);

            objFlightNumber.Add(_RemoteArrivalFlightNo);
            objFlightNumber.Add(_RemoteDepartureFlightNo);
            objFlightNumber.Add(_ContactArrivalFlightNo);
            objFlightNumber.Add(_ContactDepartureFlightNo);

            objGateFullName.Add(_RemoteArrivalGateName);
            objGateFullName.Add(_RemoteDepartureGateName);
            objGateFullName.Add(_ContactArrivalGateName);
            objGateFullName.Add(_ContactDepartureGateName);

            FlightNumber = objFlightNumber;
            GateFullName = objGateFullName;

            ItemSource = objGateUsageData;
            FlightSource = objFlightUsageData;
        }
        /// <summary>
        /// Get Parameter Value
        /// </summary>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetEntParameterValue()
        {
            string sReturnValue = "";

            EntityCollectionBase<EntParameter> objParameter = DlCommon.GetEntParameterValue("ID_APR_FT");

            if (objParameter != null)
            {
                foreach (EntParameter objTmpParameter in objParameter)
                {
                    sReturnValue = objTmpParameter.Value;
                }
            }
            return sReturnValue;
        }
        /// <summary>
        /// Get Login User Name
        /// </summary>
        /// <param name="USID">System.String containing USID</param>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetEntLoginUserName(string USID)
        {
            string sReturnValue = "";

            EntityCollectionBase<EntSecured> objUser = DlCommon.GetEntLoginUserName(USID);

            if (objUser != null)
            {
                foreach (EntSecured objTmpUser in objUser)
                {
                    sReturnValue = objTmpUser.Name;
                }
            }
            return sReturnValue;
        }
        #endregion

    }
}
