﻿using System;
using System.Linq;
using System.Windows;
using System.Threading;
using System.Globalization;
using System.Windows.Controls;
using System.Collections.Generic;
using Ufis.Entities;
using Standard_Reports.Helpers;
using DevExpress.Xpf.Charts;


namespace Standard_Reports.Reports.DelayTime
{
    /// <summary>
    /// Interaction logic for GateUsageView.xaml
    /// </summary>
    public partial class DelayTimeView : UserControl
    {
        public HpParameters param;
        public HpReportLogo Logo;
        public HpFooter Footer;
        public DelayTimeViewModel objViewModel;
        public IList<EntDbFlight> FlightList;



        #region +++ Register Properties +++
        // Using a DependencyProperty as the backing store for MyInt.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyIntProperty =
        DependencyProperty.Register("MyInt", typeof(int), typeof(DelayTimeView), new UIPropertyMetadata(1));
        #endregion

        #region +++ Properties +++
        public int MyInt
        {
            get
            {
                return (int)GetValue(MyIntProperty);
            }
            set
            {
                SetValue(MyIntProperty, value);
            }
        }
        #endregion

        #region +++ WPF Application Control Events +++
        public DelayTimeView()
        {
            CultureInfo ci = new CultureInfo(Thread.CurrentThread.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            Thread.CurrentThread.CurrentCulture = ci;
            InitializeComponent();
            dtpFromDate.Text = DateTime.Now.Date.AddYears(-2).ToString("dd.MM.yyyy");
            dtpToDate.Text = DateTime.Now.Date.ToString("dd.MM.yyyy");
            txtFromTime.Text = "0000";
            txtToTime.Text = "2359";
        }

        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            DateTime FromDate = dtpFromDate.SelectedDate.Value;
            DateTime ToDate = dtpToDate.SelectedDate.Value.AddHours(24);
            lblTotal.Content = "";
            DelayTimeViewModel viewModel = new DelayTimeViewModel();
            FlightList = viewModel.GetFlights(FromDate, ToDate, cboTemplate.Text);
            double delayTime = 0;
            int greaterthan = 0;
            int nodelay = 0;
            int lessthan = 0;
            pieChart.Points.Clear();

            if (FlightList.Count == 0)
            {
                MessageBox.Show("There is no flight found");
            }
            else
            {
                foreach (EntDbFlight flight in FlightList)
                {
                    if (cboTemplate.Text != "")
                        lblTotal.Content = "Total Flight Count : " + FlightList.Count + " for Airline Code ' " + cboTemplate.Text + " '";
                    else
                        lblTotal.Content = "Total Flight Count : " + FlightList.Count + " for all airlines.";
                    TimeSpan a = flight.OffblockTime.Value - flight.StandardTimeOfDeparture.Value;
                    delayTime = a.TotalMinutes;
                    if (delayTime > 15)
                        greaterthan += 1;
                    else
                        if (delayTime <= 15 && delayTime > 0)
                            lessthan += 1;
                        else
                            nodelay += 1;
                }
                // PieSeries2D p1 = new PieSeries2D();
                pieChart.Points.Add(new SeriesPoint("No Delay", nodelay));
                pieChart.Points.Add(new SeriesPoint("Delay Time <= 15 min", lessthan));
                pieChart.Points.Add(new SeriesPoint("Delay Time > 15 min", greaterthan));
                pieChart.PointOptions.Pattern = "{A} : ({V}) of Total " + FlightList.Count + " flight(s)";
            }
        }
        #endregion
    }
}
