﻿using System;
using System.Linq;
using System.Collections.Generic;
using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using Standard_Reports.DataAccess;
using Standard_Reports.Entities;


namespace Standard_Reports.Reports.ApprovalReport
{
    public class ApprovalReportViewModel : WorkspaceViewModel
    {
        public WorkspaceViewModel _workspace;
        IList<EntDbFlightPermit> _permitList;
        IList<entlcApprovalReport> _itemSource;
        IList<entlcApprovalReport> objApprovalReport;
        EntDbFlight _flight;
        EntDbFlight _depflight;
        DateTime? date;


        public IList<entlcApprovalReport> ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource != value)
                {
                    _itemSource = value;
                    OnPropertyChanged("ItemSource");
                }
            }
        }

        public void GetApprovalReport(DateTime dtFrom, DateTime dtTo)
        {
            GetFlightPermits(dtFrom, dtTo);
            GetApprovalReportEntity();
            ItemSource = objApprovalReport;
        }

        public string GetParameterValue()
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbParameter> objParameter = DlCommon.GetParameterValue("ID_APR_FT");

            if (objParameter != null)
            {
                foreach (EntDbParameter objTmpParameter in objParameter)
                {
                    sReturnValue = objTmpParameter.Value;
                }
            }
            return sReturnValue;
        }

        public string GetLoginUserName(string USID)
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbSecured> objUser = DlCommon.GetLoginUserName(USID);

            if (objUser != null)
            {
                foreach (EntDbSecured objTmpUser in objUser)
                {
                    sReturnValue = objTmpUser.Name;
                }
            }
            return sReturnValue;
        }

        private void GetApprovalReportEntity()
        {
            if (PermitList.Count > 0)
            {
                objApprovalReport = new List<entlcApprovalReport>();
                foreach (EntDbFlightPermit tmpFlightpermit in PermitList)
                {
                    Flight = DIApprovalReport.GetFlight(tmpFlightpermit.CallSign, tmpFlightpermit.RegistrationNumber, tmpFlightpermit.ArrivalDepartureId, tmpFlightpermit.ValidFrom.Value, tmpFlightpermit.ValidTo.Value);
                    entlcApprovalReport TmpobjApprovalReport = new entlcApprovalReport();
                    TmpobjApprovalReport.Number = objApprovalReport.Count + 1 ;
                    TmpobjApprovalReport.OperatorName = tmpFlightpermit.OperatorName;
                    TmpobjApprovalReport.RegistrationNumber = tmpFlightpermit.RegistrationNumber;
                    
                    TmpobjApprovalReport.CallSign = tmpFlightpermit.CallSign;
                    TmpobjApprovalReport.FlightPermitNumber = tmpFlightpermit.FlightPermitNumber;
                    TmpobjApprovalReport.FlightPurpose = tmpFlightpermit.FlightPurpose;
                    if (tmpFlightpermit.AircraftIATACode != "") 
                        TmpobjApprovalReport.Ac = "Y";
                    else 
                        TmpobjApprovalReport.Ac = "N";
                    if (Flight != null && Flight.ADHO == "X")
                    {
                        date = Flight.StandardTimeOfArrival;
                        if (date.HasValue)
                        {
                            TmpobjApprovalReport.ArrivalDate = string.Format("{0:dd/MM/yyyy}", date.Value);
                        }
                        TmpobjApprovalReport.OperationalType = Flight.OperationalType;
                        TmpobjApprovalReport.OriginAirportICAOCode = Flight.OriginAirportICAOCode;
                        date = Flight.BestEstimatedArrivalTime;
                        if (date.HasValue)
                        {
                            TmpobjApprovalReport.BestEstimatedArrivalTime = string.Format("{0:ddMM}", date.Value);
                        }
                      
                        date = Flight.BestEstimatedArrivalTime;
                        if (date.HasValue)
                        {
                            TmpobjApprovalReport.StandardTimeOfArrival = string.Format("{0:HHmm}", date.Value);
                        }
                        TmpobjApprovalReport.AircraftIATACode = Flight.AircraftICAOCode;
                        //TmpobjApprovalReport.StandardTimeOfDeparture = string.Format("{HHmm}", Flight.StandardTimeOfDeparture);

                        if (Flight.RelationKey != null)
                        //64208373
                        {
                            DepFlight = DIApprovalReport.GetDepartureFlight(Flight.RelationKey);
                            if (DepFlight != null)
                            {
                                TmpobjApprovalReport.DestinationAirportICAOCode = DepFlight.DestinationAirportICAOCode;
                                date = DepFlight.BestEstimatedDepartureTime;
                                if (date.HasValue)
                                {
                                    TmpobjApprovalReport.BestEstimatedDepartureTime = string.Format("{0:ddMM}", date.Value);
                                }
                                date = DepFlight.BestEstimatedDepartureTime;
                                if (date.HasValue)
                                {
                                    TmpobjApprovalReport.StandardTimeOfDeparture = string.Format("{0:HHmm}", date.Value);
                                }
                            }

                        }
                        
                            objApprovalReport.Add(TmpobjApprovalReport);
                    }
                 
                }
            }
        }

        private void GetFlightPermits(DateTime dtFrom, DateTime dtTo)
        {
            PermitList = DIApprovalReport.LoadFlightPermit(dtFrom, dtTo);
        }

        public IList<EntDbFlightPermit> PermitList
        {
            get
            {
                return _permitList;
            }
            set
            {
                if (_permitList != value)
                {
                    _permitList = value;
                    OnPropertyChanged("PermitList");
                }
            }
        }


        public EntDbFlight Flight
        {
            get
            {
                return _flight;
            }
            protected set
            {
                if (_flight != value)
                {
                    _flight = value;
                    OnPropertyChanged("Flight");
                }
            }
        }


        public EntDbFlight DepFlight
        {
            get
            {
                return _depflight;
            }
            protected set
            {
                if (_depflight != value)
                {
                    _depflight = value;
                    OnPropertyChanged("DepFlight");
                }
            }
        }
    }
}
