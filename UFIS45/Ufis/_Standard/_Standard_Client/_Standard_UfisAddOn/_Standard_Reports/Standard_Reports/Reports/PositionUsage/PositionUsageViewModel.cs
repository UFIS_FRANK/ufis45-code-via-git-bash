﻿using System;
using System.Linq;
using System.Collections.Generic;

using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using Ufis.IO;
using Standard_Reports.DataAccess;
using Standard_Reports.Entities;
using Standard_Reports.Helpers;
using Ufis.Data.Ceda;
using System.Data;
using System.Windows.Media;
using Standard_Reports.Entities;

namespace Standard_Reports.Reports.PositionUsage
{
    public class PositionUsageViewModel : WorkspaceViewModel
    {
        #region +++ Variable Declaration +++
        public WorkspaceViewModel _workspace;
        IList<HpDataPoint> _itemSource;
        EntityCollectionBase<EntFlight> objFlightList;
        EntityCollectionBase<EntParkingStand> objPositionList;
        EntityCollectionBase<EntUnavailable> objBlockList;
        EntityCollectionBase<EntUnavailable> objChangeableList;
        private double[] _flightPosition;
        private string[] _flightNo;
        private string[] _PositionName;
        private double[] _parkingPosition;
        private double[] _blockPosition;
        private double[] _ChangeablePosition;
        private string[] _PositionNameUnChangeable;
        private double[] _BlankPosition;
        DateTime? dtBegin = new DateTime();
        DateTime? dtEnd = new DateTime();
        //Total Hours to show
        //private int _totalHours = 24;
        //Duration of Chart
        private const int durations = 1;
        private static int totalDurations = 24;
        private string[] _argument;
        IList<double[]> _flightSource;
        IList<string[]> _FlightNoSource;
        IList<string[]> _PositionNames;
        IList<string[]> _PositionNameUnChangeables;
        List<HpDataPoint> objPositionUsageData = new List<HpDataPoint>();
        List<double[]> objFlightUsageData = new List<double[]>();
        List<string[]> objFlightNoData = new List<string[]>();
        List<string[]> objPositionName = new List<string[]>();
        List<string[]> objPositionNameUnChangeables = new List<string[]>();
        DataTable dtStands = new DataTable();
        string sPath = "";
        #endregion

        #region +++ Properties +++
        public IList<HpDataPoint> ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource == value)
                    return;

                _itemSource = value;
                OnPropertyChanged("ItemSource");
            }
        }
        public int TotalHour
        {
            get
            {
                return totalDurations;
            }
            set
            {
                if (totalDurations == value)
                    return;

                totalDurations = value;
                OnPropertyChanged("TotalHour");
            }
        }
        public IList<double[]> FlightSource
        {
            get
            {
                return _flightSource;
            }
            protected set
            {
                if (_flightSource == value)
                    return;

                _flightSource = value;
                OnPropertyChanged("FlightSource");
            }
        }
        public IList<string[]> FlightNoSource
        {
            get
            {
                return _FlightNoSource;
            }
            protected set
            {
                if (_FlightNoSource == value)
                    return;

                _FlightNoSource = value;
                OnPropertyChanged("FlightNoSource");
            }
        }
        public IList<string[]> PositionName
        {
            get
            {
                return _PositionNames;
            }
            protected set
            {
                if (_PositionNames  == value)
                    return;

                _PositionNames = value;
                OnPropertyChanged("PositionName");
            }
        }
        private DateTime DateFrom { get;
            set; }
        private DateTime DateTo { get;
            set; }
        public IList<string[]> PositionNameUnChangeable
        {
            get
            {
                return _PositionNameUnChangeables;
            }

            protected set
            {
                if (_PositionNameUnChangeables == value)
                    return;

                _PositionNameUnChangeables = value;
                OnPropertyChanged("PositionNameUnChangeable");
            }
        }
        #endregion

        #region +++ Data Manipulation +++
        /// <summary>
        /// Get Flight list by Date Range and Position
        /// </summary>
        /// <param name="sFilter">System.Strung containing sFilter</param>
        private void GetFlightList(string sFilter)
        {
           //Get Flight by date and time range 
            objFlightList = DlPositionUsage.LoadFlightListByDateRange(DateFrom, DateTo, sFilter);
            objPositionList = DlPositionUsage.GetPositionList(DateFrom, DateTo, sFilter);
            objBlockList = DlPositionUsage.GetBlockList(DateFrom, DateTo, sFilter);
            objChangeableList = DlPositionUsage.GetBlockListChangeable(DateFrom, DateTo, sFilter);
        }
        /// <summary>
        /// Generate Position usage Report
        /// </summary>
        /// <param name="dtFrom">System.DateTime containing dtFrom of User Keyin</param>
        /// <param name="dtTo">System.DateTime containing dtTo of User Keyin</param>
        /// <param name="bFilter">System.Boolean contianing bFilter for Showing Blocklist or not</param>
        /// <param name="sFilter">System.String containing sFilter for Stand List</param>
        /// <returns></returns>
        public string[] GetPositionUsageReportList(DateTime dtFrom, DateTime dtTo, bool bFilter, string sFilter)
        {
            _flightPosition = new double[totalDurations];
            _parkingPosition = new double[totalDurations];
            _blockPosition = new double[totalDurations];
            _ChangeablePosition = new double[totalDurations];
            _BlankPosition = new double[totalDurations];
            _flightNo = new string[totalDurations];
            _PositionName = new string[totalDurations];
            _PositionNameUnChangeable = new string[totalDurations];
            _argument = new string[totalDurations];
            sPath = Ufis.IO.CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.UfisTmp);
            DateFrom = dtFrom;
            DateTo = dtTo.AddSeconds(59);
            GetFlightList(sFilter);
            PrepareFlightUsageDataList();
            //PreparePositionUsageDataList();
            if (bFilter)
            {
                PrepareBlockUsageDataList();
                PrepareChangeableUsageDataList();
            }
            CreatePositionUsageChartDataSource(bFilter);
            return _argument;
        }
        /// <summary>
        /// Prepare Stand Usage count and Flight Counts
        /// </summary>
        private string PrepareFlightUsageDataList()
        {
            try
            {
                if (objFlightList.Count > 0)
                {
                    #region +++ Bind Backend data to Data table for Checking purpose (Can comment when launch) +++
                    ////DataTable for Backend Data
                    //DataTable dtFlightList = CreateFlightListTable();
                    ////Bind Backend data to DataTable
                    //foreach (EntDbFlight entFlight in objFlightList)
                    //{
                    //    dtFlightList.Rows.Add(entFlight.Urno, entFlight.RegistrationNumber, entFlight.FullFlightNumber, entFlight.RelationKey, entFlight.ArrivalDepartureId, entFlight.PositionOfArrival
                    //                            , entFlight.PositionOfDeparture, entFlight.TimeframeOfArrival, entFlight.TimeframeOfDeparture, entFlight.PositionArrivalScheduledStart,
                    //                            entFlight.PositionArrivalScheduledEnd, entFlight.PositionDepartureScheduledStart, entFlight.PositionDepartureScheduledEnd);
                    //}
                    ////dtFlightList.ReadXml(@"D:\CEDAFlightList.xml");
                    //// Get the DefaultViewManager of a DataTable.
                    //DataView viewFlt = dtFlightList.DefaultView;

                    //// By default, the first column sorted ascending.
                    //viewFlt.Sort = "RKEY, ADID";

                    //if (sPath != "")
                    //{
                    //    dtFlightList.TableName = "FlightList";
                    //    dtFlightList.WriteXml(sPath + @"\CEDAFlightList.xml");
                    //}
                    #endregion

                    //Table 1 to keep the flight records and time
                    DataTable dtFlights = CreateFlightTable();
                    //Table 2                   
                    dtStands = CreateDataTable();
                    string Stand = "";
                    //Generate List of RKEY who only has single RKEY on the Backend Data.
                    var objArrivalDepartureKey = objFlightList
                    .GroupBy(f => f.RelationKey)
                    .Where(f => f.Count() == 1)
                    .OrderBy(f => f.Key)
                    .Select(f => f.Key);
                    //Generate List of RKEY who only has multiple RKEY on the Backend Data.
                    var objRotationKey = objFlightList
                    .GroupBy(f => f.RelationKey)
                    .Where(f => f.Count() > 1)
                    .OrderBy(f => f.Key)
                    .Select(f => f.Key);

                    //Add to Table 2 Single RKEY Flighs
                    foreach (var vSingle in objArrivalDepartureKey)
                    {
                        //Get flight info for specific RKEY
                        EntFlight tmpSingle = objFlightList.FirstOrDefault(f => f.RelationKey == vSingle);
                        switch (tmpSingle.ArrivalDepartureId)
                        {
                            //Add Arrival Flight
                            case "A":
                                //dtStands.Rows.Add(tmpSingle.PositionOfArrival, tmpSingle.FullFlightNumber, tmpSingle.TimeframeOfArrival, DateTo);
                                if (tmpSingle.PositionArrivalScheduledEnd != null)
                                    dtStands.Rows.Add(tmpSingle.PositionOfArrival, tmpSingle.FullFlightNumber, tmpSingle.PositionArrivalScheduledStart, tmpSingle.PositionArrivalScheduledEnd);
                                else
                                    dtStands.Rows.Add(tmpSingle.PositionOfArrival, tmpSingle.FullFlightNumber, tmpSingle.PositionArrivalScheduledStart, DateTo);
                                break;
                            //Add Departure Flight
                            case "D":
                                dtStands.Rows.Add(tmpSingle.PositionOfDeparture, tmpSingle.FullFlightNumber, tmpSingle.PositionDepartureScheduledStart, tmpSingle.PositionDepartureScheduledEnd);
                                break;
                            //Add Rotation Status Flight
                            case "B":
                                Stand = String.Compare(tmpSingle.PositionOfDeparture, "", false) != 0 ? tmpSingle.PositionOfDeparture : tmpSingle.PositionOfArrival;
                                dtStands.Rows.Add(Stand, tmpSingle.FullFlightNumber, tmpSingle.PositionArrivalScheduledStart, tmpSingle.PositionArrivalScheduledEnd);
                                break;
                        }
                    }
                    #region +++ Write XML File (Can Commment out when launch) ++++
                    //if (sPath != "")
                    //{
                    //    //Single RKEY List
                    //    dtStands.TableName = "FlightList";
                    //    dtStands.WriteXml(sPath + @"\SingleRKey.xml");
                    //}
                    #endregion

                    //Add to Table 1 that RKEY has more than one flighs
                    foreach (var vFlight in objRotationKey)
                    {
                        //Get Flight list by RKEY and order by TIFA and TIFD
                        IEnumerable<EntFlight> tmpEnuFlight = objFlightList.Where<EntFlight>(p => p.RelationKey == vFlight).OrderBy(p => p.TimeframeOfArrival).ThenBy(f => f.TimeframeOfDeparture);
                        foreach (var tmpFlight in tmpEnuFlight)
                        {
                             if (String.Compare(tmpFlight.ArrivalDepartureId, "A", false) == 0)
                            {
                                dtFlights.Rows.Add(tmpFlight.PositionOfArrival, tmpFlight.FullFlightNumber, tmpFlight.TimeframeOfArrival, tmpFlight.RelationKey, tmpFlight.ArrivalDepartureId, tmpFlight.PositionArrivalScheduledStart, tmpFlight.PositionArrivalScheduledEnd);
                            }
                            if (String.Compare(tmpFlight.ArrivalDepartureId, "D", false) == 0)
                            {
                                dtFlights.Rows.Add(tmpFlight.PositionOfDeparture, tmpFlight.FullFlightNumber, tmpFlight.TimeframeOfDeparture, tmpFlight.RelationKey, tmpFlight.ArrivalDepartureId, tmpFlight.PositionDepartureScheduledStart, tmpFlight.PositionDepartureScheduledEnd);
                            }
                            if (String.Compare(tmpFlight.ArrivalDepartureId, "B", false) == 0)
                            {
                                if (!string.IsNullOrEmpty(tmpFlight.PositionOfArrival))
                                {
                                    dtFlights.Rows.Add(tmpFlight.PositionOfArrival, tmpFlight.FullFlightNumber, tmpFlight.TimeframeOfArrival, tmpFlight.RelationKey, "A", tmpFlight.PositionArrivalScheduledStart, tmpFlight.PositionArrivalScheduledEnd);
                                }
                                if (!string.IsNullOrEmpty(tmpFlight.PositionOfDeparture))
                                {
                                    dtFlights.Rows.Add(tmpFlight.PositionOfDeparture, tmpFlight.FullFlightNumber, tmpFlight.TimeframeOfDeparture, tmpFlight.RelationKey, "D", tmpFlight.PositionDepartureScheduledStart, tmpFlight.PositionDepartureScheduledEnd);
                                }
                            }
                            dtFlights.AcceptChanges();
                        }
                    }
                    #region +++ Write XML File (Can Commment out when launch) ++++
                    //if (sPath != "")
                    //{
                    //    dtFlights.TableName = "FlightList";
                    //    dtFlights.WriteXml(sPath + @"\FlightExtractList.xml");
                    //}
                    #endregion
                    // Get the DefaultViewManager of a DataTable.
                    DataView view = dtFlights.DefaultView;

                    // By default, the first column sorted ascending.
                    view.Sort = "RKey,Time";
                        //view.RowFilter = "FLNO = 'EY 232'";                    
                        string FLNO, RKEY;
                        DateTime Begin, End;
                     try
                        {
                            for (int i = 0; i < view.Count; i++)
                            {
                                if (view[i].Row["Time"].ToString().Trim()!="")
                                {
                                    RKEY = view[i].Row["RKEY"].ToString().Trim();
                                    if (String.Compare(view[i].Row["ADID"].ToString().Trim(), "A", false) == 0)
                                    {
                                        Stand = view[i].Row["Stand"].ToString().Trim();
                                        FLNO = view[i].Row["FLNO"].ToString().Trim();
                                        if (view[i].Row["Time"] != null) Begin = Convert.ToDateTime(view[i].Row["Time"]);
                                        else Begin = DateFrom;
                                        End = DateTo;
                                        if (i < view.Count - 1 &&
                                        String.Compare(view[i].Row["RKEY"].ToString().Trim(), view[i + 1].Row["RKEY"].ToString().Trim(), false) == 0 &&
                                        String.Compare(view[i + 1].Row["ADID"].ToString().Trim(), "D", false) == 0)
                                        {
                                            //if Stand is same
                                            if (String.Compare(view[i].Row["Stand"].ToString().Trim(), view[i + 1].Row["Stand"].ToString().Trim(), false) == 0)
                                            {
                                                FLNO = view[i + 1].Row["FLNO"].ToString().Trim();
                                                if (view[i + 1].Row["TIME"] != null) End = Convert.ToDateTime(view[i + 1].Row["TIME"]);
                                                else End = DateTo;
                                                dtStands.Rows.Add(Stand, FLNO, Begin, End, RKEY);
                                                i += 1;
                                                continue;
                                            }
                                            //If Arr and Dep Stand is not same, will insert two line.
                                            else
                                            {
                                                FLNO = view[i].Row["FLNO"].ToString().Trim();
                                                //if (view[i].Row["PStart"] != null)
                                                Begin = Convert.ToDateTime(view[i].Row["PStart"]);
                                                //else
                                                //Begin = Convert.ToDateTime(view[i].Row["Time"]);
                                                //if (view[i].Row["PEnd"] != null)
                                                End = Convert.ToDateTime(view[i].Row["PEnd"]);
                                                //else
                                                //End = Convert.ToDateTime(view[i + 1].Row["Time"]);

                                                dtStands.Rows.Add(Stand, FLNO, Begin, End, RKEY);

                                                i += 1;

                                                Stand = view[i].Row["Stand"].ToString().Trim();
                                                FLNO = view[i].Row["FLNO"].ToString().Trim();
                                                Begin = Convert.ToDateTime(view[i].Row["PStart"]);
                                                End = Convert.ToDateTime(view[i].Row["PEnd"]);

                                                dtStands.Rows.Add(Stand, FLNO, Begin, End, RKEY);

                                                continue;
                                            }
                                        }
                                        else
                                        {
                                            if (Begin <= End)
                                            {
                                                dtStands.Rows.Add(Stand, FLNO, Begin, End, RKEY);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Stand = view[i].Row["Stand"].ToString().Trim();
                                        FLNO = view[i].Row["FLNO"].ToString().Trim();
                                        Begin = DateFrom;
                                        End = Convert.ToDateTime(view[i].Row["Time"]);

                                        if (Begin <= End)
                                        {
                                            dtStands.Rows.Add(Stand, FLNO, Begin, End, RKEY);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            string msg;
                            msg = ex.Message.ToString();
                        }
                    #region +++ Write XML File (Can Commment out when launch) ++++
                    //if (sPath != "")
                    //{
                    //    dtStands.TableName = "FlightList";
                    //    dtStands.WriteXml(sPath + @"\CompleteFlightList.xml");
                    //}
                    #endregion
                    //Get the DefaultViewManager of a DataTable.
                    DataView dvStand = dtStands.DefaultView;
                    //By default, the first column sorted ascending.
                    dvStand.Sort = "Stand, Begin";
                    string tmpParkingStand = "";
                    string sFlightNo = "";

                    DataTable dtStandFlight = CreateStandFlight();
                    //Loop for Stand, Hour, Flight Count and Flight Name
                    foreach (DataRowView dr in dvStand)
                    {
                        tmpParkingStand = dr["Stand"].ToString().Trim();
                        sFlightNo = dr["FLNO"].ToString().Trim();
                        Boolean bContinue = false;
                        //Check correct stand
                        foreach (var vSTand in objPositionList)
                        {
                            if (String.Compare(vSTand.Name, tmpParkingStand, false) == 0)
                            {
                                bContinue = true;
                                break;
                            }
                        }
                        //If Stand is out of the scope, will not execute for this stand.
                        if (!bContinue)
                            continue;

                        dtBegin = Convert.ToDateTime(dr["Begin"]);
                        dtEnd = Convert.ToDateTime(dr["End"]);
                        //Checking DateTime Range whether withing parameter range or not
                        if (dtBegin < DateFrom)
                            dtBegin = DateFrom;
                        if (dtEnd > DateTo)
                            dtEnd = DateTo;

                        if ((dtBegin >= DateFrom && dtBegin < DateTo) && (dtEnd > DateFrom && dtEnd <= DateTo))
                        {
                            DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);
                            Int32 fromX = dtBegin.Value.Subtract(tempTime).Hours;
                            Int32 toX = dtEnd.Value.Subtract(tempTime).Hours;
                            for (int j = fromX; j <= toX; j++)
                            {
                                System.Data.DataRow[] foundRows;
                                foundRows = dtStandFlight.Select(String.Format("Stand = '{0}' AND Hour = {1}", tmpParkingStand.Trim(), j));
                                if (foundRows.Length > 0)
                                {
                                    if (foundRows[0].ItemArray[3].ToString().Trim() != sFlightNo.Trim())
                                    {
                                        dtStandFlight.Rows.Add(tmpParkingStand.Trim(), j, Convert.ToInt32(foundRows[0].ItemArray[2]) + 1, Convert.ToString(foundRows[0].ItemArray[3] + "," + sFlightNo.Trim()));
                                        foreach (var drRemove in foundRows)
                                        {
                                            dtStandFlight.Rows.Remove(drRemove);
                                        }
                                    }
                                }
                                else
                                    dtStandFlight.Rows.Add(tmpParkingStand.Trim(), j, 1, sFlightNo.Trim());
                            }
                        }
                    }
                    #region +++ Write XML File (Can Commment out when launch) ++++
                    //if (sPath != "")
                    //{
                    //    dtStandFlight.TableName = "PostionTable";
                    //    dtStandFlight.WriteXml(sPath + @"\FlightPosition.xml");
                    //}
                    #endregion
                    for (int i = 0; i < TotalHour ; i++)
                    {
                        try
                        {
                            DataTable dtTempStand = CreateStandFlight();
                            dtTempStand = dtStandFlight.Select("Hour = " + i).CopyToDataTable();
                            if (dtTempStand.Rows.Count > 0)
                                _parkingPosition[i] = dtTempStand.Rows.Count;
                        }
                        catch
                        {
                        }
                        try
                        {
                            object obj = dtStandFlight.Compute("Sum(FltCount)", "Hour=" + i);
                            _flightPosition[i] = Convert.ToDouble(obj);
                        }
                        catch
                        {
                        }
                        string sStand = "";
                        string arrStand = "";
                        string sFltNo = "";
                        string sFltNo2 = "";
                        try
                        {
                            DataTable dtTempStand2 = CreateStandFlight();
                            dtTempStand2 = dtStandFlight.Select("Hour = " + i).CopyToDataTable();
                            //Assign Position Name
                            for (int j = 0; j < dtTempStand2.Rows.Count; j++)
                            {
                                if (dtTempStand2.Rows[j][0] != null)
                                {
                                    if (j == 0)
                                    {
                                        arrStand = dtTempStand2.Rows[j][0].ToString();
                                        sStand = arrStand;
                                    }
                                    else
                                    {
                                        if (arrStand != dtTempStand2.Rows[j][0].ToString())
                                        {
                                            sStand = sStand + ", " + dtTempStand2.Rows[j][0].ToString().Trim();
                                            arrStand = dtTempStand2.Rows[j][0].ToString();
                                        }
                                    }
                                }
                            }
                            //Assign Flight No
                            for (int j = 0; j < dtTempStand2.Rows.Count; j++)
                            {
                                if (dtTempStand2.Rows[j][3] != null)
                                {
                                    if (j == 0)
                                    {
                                        sFltNo2 = dtTempStand2.Rows[j][3].ToString();
                                        sFltNo = sFltNo2;
                                    }
                                    else
                                    {
                                        if (sFltNo2 != dtTempStand2.Rows[j][3].ToString())
                                        {
                                            sFltNo = sFltNo + ", " + dtTempStand2.Rows[j][3].ToString().Trim();
                                            sFltNo2 = dtTempStand2.Rows[j][3].ToString().Trim();
                                        }
                                    }
                                }
                            }
                        }
                        catch
                        {
                        }
                        _flightNo[i] = sFltNo;
                        _PositionName[i] = sStand;
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        private DataTable CreateDataTable()
        {
            DataTable dtFlightList = new DataTable();
            dtFlightList.Columns.Add("Stand", typeof(string));
            dtFlightList.Columns.Add("FLNO", typeof(string));
            dtFlightList.Columns.Add("Begin", typeof(DateTime));
            dtFlightList.Columns.Add("End", typeof(DateTime));
            dtFlightList.Columns.Add("RKey", typeof(int));

            return dtFlightList;
        }
        private DataTable CreateStandFlight()
        {
            DataTable dtFlightList = new DataTable();
            dtFlightList.Columns.Add("Stand", typeof(string));
            dtFlightList.Columns.Add("Hour", typeof(int));
            dtFlightList.Columns.Add("FltCount", typeof(int));
            dtFlightList.Columns.Add("FltName", typeof(string));
            return dtFlightList;
        }
        private DataTable CreateFlightTable()
        {
            DataTable dtFlightList = new DataTable();
            dtFlightList.Columns.Add("Stand", typeof(string));
            dtFlightList.Columns.Add("FLNO", typeof(string));
            dtFlightList.Columns.Add("Time", typeof(DateTime));
            dtFlightList.Columns.Add("RKey", typeof(int));
            dtFlightList.Columns.Add("ADID", typeof(string));
            dtFlightList.Columns.Add("PStart", typeof(DateTime));
            dtFlightList.Columns.Add("PEnd", typeof(DateTime));

            return dtFlightList;
        }
        private DataTable CreateFlightListTable()
        {
            DataTable dtFlightList = new DataTable();
            dtFlightList.Columns.Add("Urno", typeof(string));
            dtFlightList.Columns.Add("Regn", typeof(string));
            dtFlightList.Columns.Add("FLNO", typeof(string));
            dtFlightList.Columns.Add("RKey", typeof(int));
            dtFlightList.Columns.Add("ADID", typeof(string));
            dtFlightList.Columns.Add("PSTA", typeof(string));
            dtFlightList.Columns.Add("PSTD", typeof(string));
            dtFlightList.Columns.Add("TIFA", typeof(DateTime));
            dtFlightList.Columns.Add("TIFD", typeof(DateTime));
            dtFlightList.Columns.Add("PABS", typeof(DateTime));
            dtFlightList.Columns.Add("PAES", typeof(DateTime));
            dtFlightList.Columns.Add("PDBS", typeof(DateTime));
            dtFlightList.Columns.Add("PDES", typeof(DateTime));

            return dtFlightList;
        }
        #region +++ Unuse Code +++
        /// <summary>
        /// Prepare Position Usage Data for  the Time Frame
        /// </summary>
        //private string PreparePositionUsageDataList()
        //{
        //    try
        //    {
        //        if (objPositionList.Count > 0 && objFlightList.Count > 0)
        //        {
        //            foreach (EntDbParkingStand tmpParkingStand in objPositionList)
        //            {
        //                double[] tmpParkingPosition = new double[totalDurations];

        //                IEnumerable<EntDbFlight> tmpObjPositionList = objFlightList.Where<EntDbFlight>(p => String.Compare(p.PositionOfArrival, tmpParkingStand.Name, false) == 0 ||
        //                                                                                                String.Compare(p.PositionOfDeparture, tmpParkingStand.Name, false) == 0)
        //                                                                                                .OrderBy(p => p.RelationKey)
        //                                                                                                .ThenBy(p => p.TimeframeOfArrival)
        //                                                                                                .ThenBy(f => f.TimeframeOfDeparture);
        //                //Get Single Arrival Flights
        //                var objArrivalDepartureKey = tmpObjPositionList
        //                                      .GroupBy(p => p.RelationKey)
        //                                      .Where(p => p.Count() == 1)
        //                                      .OrderBy(p => p.Key)
        //                                      .Select(p => p.Key);
        //                //Get Rotation flights
        //                var objRotationKey = tmpObjPositionList
        //                                           .GroupBy(p => p.RelationKey)
        //                                           .Where(p => p.Count() > 1)
        //                                           .OrderBy(p => p.Key)
        //                                           .Select(p => p.Key);

        //                #region Single Arrival Or Departure Flight Position Usage
        //                foreach (var tmpkey in objArrivalDepartureKey)
        //                {
        //                    EntDbFlight tmpFlight = tmpObjPositionList.FirstOrDefault(f => f.RelationKey == tmpkey);
        //                    //Arrival or Round Trip
        //                    if (String.Compare(tmpFlight.ArrivalDepartureId, "A", false) == 0 || String.Compare(tmpFlight.ArrivalDepartureId, "B", false) == 0)
        //                    {
        //                        if (String.Compare(tmpFlight.ArrivalDepartureId, "A", false) == 0)
        //                        {
        //                            dtBegin = tmpFlight.TimeframeOfArrival ?? DateFrom;
        //                            dtEnd = DateTo;
        //                        }
        //                        else
        //                        {
        //                            dtBegin = tmpFlight.TimeframeOfArrival ?? DateFrom;
        //                            dtEnd = tmpFlight.TimeframeOfDeparture ?? DateTo;

        //                        }
        //                        if (dtBegin.HasValue && dtEnd.HasValue)
        //                        {
        //                            if (dtBegin <= DateFrom)
        //                                dtBegin = DateFrom;
        //                            if (dtEnd >= DateTo)
        //                                dtEnd = DateTo;

        //                            if ((dtBegin >= DateFrom && dtBegin < DateTo) && (dtEnd > DateFrom && dtEnd <= DateTo))
        //                            {
        //                                DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);
        //                                Int32 fromX = dtBegin.Value.Subtract(tempTime).Hours;
        //                                Int32 toX = dtEnd.Value.Subtract(tempTime).Hours;
        //                                for (int j = fromX; j <= toX; j++)
        //                                {
        //                                    if (tmpParkingPosition[j] == 0)
        //                                    {
        //                                        _PositionName[j] = string.Compare(_PositionName[j], "", false) == 0 ? _PositionName[j] : String.Format("{0},{1}", _PositionName[j], tmpParkingStand.Name);
        //                                        _parkingPosition[j]++;
        //                                        tmpParkingPosition[j]++;
        //                                    }

        //                                }
        //                            }
        //                        }
        //                    }
        //                    //Departure
        //                    else
        //                    {
        //                        if (tmpFlight.TimeframeOfDeparture != null)
        //                            //TIFD
        //                            dtBegin = tmpFlight.TimeframeOfDeparture;
        //                        else
        //                            dtBegin = DateFrom;
        //                        if (tmpFlight.PositionDepartureScheduledStart != null)
        //                            //PDBS
        //                            dtEnd = tmpFlight.PositionDepartureScheduledStart;
        //                        else dtEnd = DateTo;

        //                        if (dtBegin.HasValue && dtEnd.HasValue)
        //                        {
        //                            if (dtBegin <= DateFrom)
        //                                dtBegin = DateFrom;
        //                            if (dtEnd >= DateTo)
        //                                dtEnd = DateTo;

        //                            if ((dtBegin >= DateFrom && dtBegin < DateTo) && (dtEnd > DateFrom && dtEnd <= DateTo))
        //                            {
        //                                DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);
        //                                Int32 fromX = dtBegin.Value.Subtract(tempTime).Hours;
        //                                Int32 toX = dtEnd.Value.Subtract(tempTime).Hours;
        //                                for (int j = fromX; j <= toX; j++)
        //                                {
        //                                    if (tmpParkingPosition[j] == 0)
        //                                    {
        //                                        _PositionName[j] = (string.Compare(_PositionName[j], "", false) == 0) ? _PositionName[j] : String.Format("{0} , {1}", _PositionName[j], tmpParkingStand.Name);
        //                                        _parkingPosition[j]++;
        //                                        tmpParkingPosition[j]++;
        //                                    }

        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //                #endregion

        //                foreach (var tmpkey in objRotationKey)
        //                {
        //                    IEnumerable<EntDbFlight> tmpRotationalFlightList = tmpObjPositionList.Where<EntDbFlight>(p => p.RelationKey == tmpkey).OrderBy(p => p.TimeframeOfArrival).ThenBy(f => f.TimeframeOfDeparture);

        //                    if (tmpRotationalFlightList.Count() == 2)
        //                    {
        //                        #region Rotation Without towing Flight Position Usage

        //                        EntDbFlight tmpRecord = new EntDbFlight();
        //                        tmpRecord = tmpRotationalFlightList.First<EntDbFlight>();
        //                        dtBegin = tmpRecord.TimeframeOfArrival != null ? tmpRecord.TimeframeOfArrival : DateFrom;
        //                        tmpRecord = null;
        //                        tmpRecord = new EntDbFlight();
        //                        tmpRecord = tmpRotationalFlightList.Last<EntDbFlight>();
        //                        if (tmpRecord.TimeframeOfDeparture != null)
        //                            dtEnd = tmpRecord.TimeframeOfDeparture;
        //                        else
        //                            dtEnd = DateTo;
        //                        if (dtBegin.HasValue && dtEnd.HasValue)
        //                        {
        //                            if (dtBegin <= DateFrom)
        //                                dtBegin = DateFrom;
        //                            if (dtEnd >= DateTo)
        //                                dtEnd = DateTo;

        //                            if ((dtBegin >= DateFrom && dtBegin < DateTo) && (dtEnd > DateFrom && dtEnd <= DateTo))
        //                            {
        //                                DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);
        //                                Int32 fromX = dtBegin.Value.Subtract(tempTime).Hours;
        //                                Int32 toX = dtEnd.Value.Subtract(tempTime).Hours;
        //                                for (int j = fromX; j <= toX; j++)
        //                                {
        //                                    if (tmpParkingPosition[j] != 0)
        //                                        continue;

        //                                    _PositionName[j] = (string.Compare(_PositionName[j], "", false) == 0) ? _PositionName[j] : String.Format("{0} , {1}", _PositionName[j], tmpParkingStand.Name);
        //                                    _parkingPosition[j]++;
        //                                    tmpParkingPosition[j]++;

        //                                }
        //                            }
        //                        }
        //                        #endregion
        //                    }
        //                    else if (tmpRotationalFlightList.Count() > 2)
        //                    {
        //                        #region Rotation With towing Flight Position Usage
        //                        IEnumerable<EntDbFlight> tmpRotationalTowingFlightList = tmpRotationalFlightList.OrderBy(f => f.TimeframeOfArrival).ThenBy(f => f.TimeframeOfDeparture);

        //                        string strPositionArrival = string.Empty;

        //                        int iFCount = 0;
        //                        foreach (var objRotFlights in tmpRotationalTowingFlightList)
        //                        {
        //                            strPositionArrival = !String.IsNullOrEmpty(objRotFlights.PositionOfArrival) ? objRotFlights.PositionOfArrival : objRotFlights.PositionOfDeparture;

        //                            if (objRotFlights.TimeframeOfArrival != null)
        //                            {
        //                                iFCount += 1;
        //                                dtBegin = objRotFlights.TimeframeOfArrival;
        //                                if (iFCount < 2)
        //                                    continue;
        //                            }
        //                            else
        //                            {
        //                                iFCount += 1;
        //                                dtBegin = DateFrom;
        //                                if (iFCount < 2)
        //                                    continue;
        //                            }

        //                            if (objRotFlights.TimeframeOfDeparture != null)
        //                            {
        //                                iFCount += 1;
        //                                dtEnd = objRotFlights.TimeframeOfDeparture;
        //                            }
        //                            else
        //                            {
        //                                iFCount += 1;
        //                                dtEnd = DateTo;
        //                            }

        //                            if (string.IsNullOrEmpty(strPositionArrival) || String.Compare(strPositionArrival, objRotFlights.PositionOfDeparture, false) != 0)
        //                                continue;

        //                            if (iFCount > 1)
        //                            {
        //                                if (dtBegin.HasValue && dtEnd.HasValue)
        //                                {
        //                                    if (dtBegin <= DateFrom)
        //                                        dtBegin = DateFrom;
        //                                    if (dtEnd >= DateTo)
        //                                        dtEnd = DateTo;

        //                                    if ((dtBegin >= DateFrom && dtBegin < DateTo) && (dtEnd > DateFrom && dtEnd <= DateTo))
        //                                    {
        //                                        DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);
        //                                        Int32 fromX = dtBegin.Value.Subtract(tempTime).Hours;
        //                                        Int32 toX = dtEnd.Value.Subtract(tempTime).Hours;
        //                                        for (int j = fromX; j <= toX; j++)
        //                                        {
        //                                            if (tmpParkingPosition[j] != 0)
        //                                                continue;

        //                                            iFCount = 0;
        //                                            _PositionName[j] = (string.Compare(_PositionName[j], "", false) == 0) ? _PositionName[j] : String.Format("{0} , {1}", _PositionName[j], tmpParkingStand.Name);
        //                                            _parkingPosition[j]++;
        //                                            tmpParkingPosition[j]++;

        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        #endregion
        //                    }
        //                }
        //            }
        //        }
        //        return "";
        //    }
        //    catch (Exception ex)
        //    {
        //        string msg = ex.StackTrace;
        //        return msg;
        //    }
        //}
        #endregion
        /// <summary>
        /// Prepare Blocked Position Usage Data  
        /// </summary>
        private string PrepareBlockUsageDataList()
        {
            try
            {
                if (objPositionList.Count > 0)
                {
                    foreach (EntParkingStand tmpPosition in objPositionList)
                    {
                        if (objPositionList != null)
                        {
                            tmpPosition.Unavailables = objBlockList.Where(d => d.RelatedUrno == tmpPosition.Urno).ToList();

                            if (tmpPosition.Unavailables.Count > 0)
                            {
                                foreach (EntUnavailable tmpBlock in tmpPosition.Unavailables)
                                {
                                    if (!tmpBlock.Changeable)
                                    {
                                        if (tmpBlock.ValidFrom != null)
                                            dtBegin = tmpBlock.ValidFrom;
                                        else
                                            dtBegin = DateFrom;
                                        if (tmpBlock.ValidTo != null)
                                            dtEnd = tmpBlock.ValidTo;
                                        else
                                            dtEnd = DateTo;

                                        if (dtBegin.HasValue && dtEnd.HasValue)
                                        {
                                            if (dtBegin <= DateFrom)
                                                dtBegin = DateFrom;

                                            if (dtEnd >= DateTo)
                                                dtEnd = DateTo;

                                            if ((dtBegin >= DateFrom && dtBegin < DateTo) && (dtEnd > DateFrom && dtEnd <= DateTo))
                                            {
                                                //DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, (DateFrom.Minute < 30 ? 0 : 30), 0);
                                                //TimeSpan fromX = dtBegin.Value.Subtract(tempTime);
                                                //TimeSpan toX = dtEnd.Value.Subtract(tempTime);

                                                //int i = fromX.Hours == 0 ? (fromX.Minutes < durations ? 0 : 1) : (fromX.Hours * 2 + (fromX.Minutes < durations ? 0 : 1));
                                                //int j = toX.Hours == 0 ? (toX.Minutes < durations ? 0 : 1) : (toX.Hours * 2 + (toX.Minutes < durations ? 0 : 1));
                                                DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);
                                                Int32 fromX = dtBegin.Value.Subtract(tempTime).Hours;
                                                Int32 toX = dtEnd.Value.Subtract(tempTime).Hours;
                                                for (int j = fromX; j <= toX; j++)
                                                {
                                                    _blockPosition[j]++;
                                                    _PositionNameUnChangeable[j] = (string.Compare(_PositionNameUnChangeable[j], " ", false) == 0) ? _PositionNameUnChangeable[j] : _PositionNameUnChangeable[j] + " , " + tmpPosition.Name;
                                                }
                                                //while (i <= j)
                                                //{
                                                //    _blockPosition[i]++;
                                                //    i++;
                                                //}
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        /// <summary>
        /// Changeable Block List
        /// </summary>
        /// <returns></returns>
        private string PrepareChangeableUsageDataList()
        {
            try
            {
                if (objPositionList.Count > 0)
                {
                    foreach (EntParkingStand tmpPosition in objPositionList)
                    {
                        if (objPositionList != null)
                        {
                            tmpPosition.Unavailables = objChangeableList.Where(d => d.RelatedUrno == tmpPosition.Urno).ToList();

                            if (tmpPosition.Unavailables.Count > 0)
                            {
                                foreach (EntUnavailable tmpBlock in tmpPosition.Unavailables)
                                {
                                    if (!tmpBlock.Changeable)
                                    {
                                        if (tmpBlock.ValidFrom != null)
                                            dtBegin = tmpBlock.ValidFrom;
                                        else
                                            dtBegin = DateFrom;
                                        if (tmpBlock.ValidTo != null)
                                            dtEnd = tmpBlock.ValidTo;
                                        else
                                            dtEnd = DateTo;

                                        if (dtBegin.HasValue && dtEnd.HasValue)
                                        {
                                            if (dtBegin <= DateFrom)
                                                dtBegin = DateFrom;

                                            if (dtEnd >= DateTo)
                                                dtEnd = DateTo;

                                            if ((dtBegin >= DateFrom && dtBegin < DateTo) && (dtEnd > DateFrom && dtEnd <= DateTo))
                                            {
                                                //DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, (DateFrom.Minute < 30 ? 0 : 30), 0);

                                                //TimeSpan fromX = dtBegin.Value.Subtract(tempTime);
                                                //TimeSpan toX = dtEnd.Value.Subtract(tempTime);

                                                //int i = fromX.Hours == 0 ? (fromX.Minutes < durations ? 0 : 1) : (fromX.Hours * 2 + (fromX.Minutes < durations ? 0 : 1));
                                                //int j = toX.Hours == 0 ? (toX.Minutes < durations ? 0 : 1) : (toX.Hours * 2 + (toX.Minutes < durations ? 0 : 1));
                                                DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);
                                                Int32 fromX = dtBegin.Value.Subtract(tempTime).Hours;
                                                Int32 toX = dtEnd.Value.Subtract(tempTime).Hours;
                                                for (int j = fromX; j <= toX; j++)
                                                {
                                                    _ChangeablePosition[j]++;
                                                }
                                                //while (i <= j)
                                                //{
                                                //    _blockPosition[i]++;
                                                //    i++;
                                                //}
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }

        private void CreatePositionUsageChartDataSource(bool bFilter)
        {
            switch (bFilter.ToString().ToUpper())
            {
                case "TRUE":
                    {

                        //TimeSpan timeDuration = new TimeSpan(DateFrom.Hour, (DateFrom.Minute < durations ? 0 : durations), 0);
                        TimeSpan timeDuration = new TimeSpan(DateFrom.Hour, 00, 0);

                        for (int i = 0; i < totalDurations; i++)
                        {
                            string argument = String.Format("{0}:{1}0", timeDuration.Hours, timeDuration.Minutes);
                            _argument[i] = argument;
                            objPositionUsageData.Add(HpDataPoint.CreateBarDataPoint(argument, _parkingPosition[i], _blockPosition[i], 0, 0));
                            timeDuration = timeDuration.Add(new TimeSpan(durations, 00, 0));
                        }
                        break;
                    }

                default:
                    {

                        //TimeSpan timeDuration = new TimeSpan(DateFrom.Hour, (DateFrom.Minute < durations ? 0 : durations), 0);
                        TimeSpan timeDuration = new TimeSpan(DateFrom.Hour, 00, 0);
                        for (int i = 0; i < totalDurations; i++)
                        {
                            string argument = String.Format("{0}:{1}0", timeDuration.Hours, timeDuration.Minutes);
                            _argument[i] = argument;
                            objPositionUsageData.Add(HpDataPoint.CreateBarDataPoint(argument, _parkingPosition[i], 0.00, 0.00, 0.00));
                            timeDuration = timeDuration.Add(new TimeSpan(durations, 00, 0));
                        }
                        break;
                    }
            }

            objFlightUsageData.Add(_flightPosition);
            objFlightNoData.Add(_flightNo);
            objPositionName.Add(_PositionName);
            FlightNoSource = objFlightNoData;
            PositionName = objPositionName;
            ItemSource = objPositionUsageData;
            FlightSource = objFlightUsageData;
            objPositionNameUnChangeables.Add(_PositionNameUnChangeable);
            PositionNameUnChangeable = objPositionNameUnChangeables;
        }
        /// <summary>
        /// Get Parameter Value
        /// </summary>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetParameterValue()
        {
            string sReturnValue = "";

            EntityCollectionBase<EntParameter> objParameter = DlCommon.GetEntParameterValue("ID_APR_FT");

            if (objParameter != null)
            {
                foreach (EntParameter objTmpParameter in objParameter)
                {
                    sReturnValue = objTmpParameter.Value;
                }
            }
            return sReturnValue;
        }
        /// <summary>
        /// Get Login User Name
        /// </summary>
        /// <param name="USID">System.String containing USID</param>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetLoginUserName(string USID)
        {
            string sReturnValue = "";

            EntityCollectionBase<EntSecured> objUser = DlCommon.GetEntLoginUserName(USID);

            if (objUser != null)
            {
                foreach (EntSecured objTmpUser in objUser)
                {
                    sReturnValue = objTmpUser.Name;
                }
            }
            return sReturnValue;
        }
        #endregion
    }
}
