﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Standard_Reports.DataAccess
{
    public class AppArguments
    {
        private static AppArguments _this;

        public int Urno { get; set; }
        public TimeZoneInfo TimeZone { get; set; }
        public string UserName { get; set; }
        public string ArrivalDepartureId { get; set; }
        public bool ReadOnly { get; set; }
        public static AppArguments Current
        {
            get
            {
                if (_this == null)
                    _this = new AppArguments();
                return _this;
            }
        }
    }
}
