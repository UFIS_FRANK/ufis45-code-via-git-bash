﻿using System;
using System.Linq;
using System.Collections.Generic;
using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using Standard_Reports.DataAccess;
using Standard_Reports.Entities;
using Ufis.Data.Ceda;


namespace Standard_Reports.Reports.PermitLetter
{
    public class PermitLetterViewModel : WorkspaceViewModel
    {
        public WorkspaceViewModel _workspace;
        EntDbFlightPermit _permit;
        entlcPermitLetter _itemSource;
        entlcPermitLetter objPermitLetter;
        EntDbFlight _depflight;
        EntDbFlight _flight;
        public entlcPermitLetter ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource != value)
                {
                    _itemSource = value;
                    OnPropertyChanged("ItemSource");
                }
            }
        }

        public void GetApprovalReport(int urno)
        {
            GetFlightPermits(urno);
            GetApprovalReportEntity();
            ItemSource = objPermitLetter;
        }


        public string GetParameterValue()
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbParameter> objParameter = DlCommon.GetParameterValue("ID_APR_FT");

            if (objParameter != null)
            {
                foreach (EntDbParameter objTmpParameter in objParameter)
                {
                    sReturnValue = objTmpParameter.Value;
                }
            }
            return sReturnValue;
        }

        public string GetLoginUserName(string USID)
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbSecured> objUser = DlCommon.GetLoginUserName(USID);

            if (objUser != null)
            {
                foreach (EntDbSecured objTmpUser in objUser)
                {
                    sReturnValue = objTmpUser.Name;
                }
            }
            return sReturnValue;
        }

        private void GetApprovalReportEntity()
        {
            if (Permit != null)
            {
                objPermitLetter = new entlcPermitLetter();
                Flight = DIPermitLetter.GetFlight(Permit.CallSign, Permit.RegistrationNumber, Permit.ArrivalDepartureId, Permit.ValidFrom.Value, Permit.ValidTo.Value);
                if (Permit.ArrivalDepartureId == "A")
                {              
                    if (Flight != null)
                        DepFlight = DIPermitLetter.GetDepartureFlight(Flight.RelationKey);              
                }
                else if (Permit.ArrivalDepartureId == "D")
                {
                    DepFlight = Flight = DIPermitLetter.GetFlight(Permit.CallSign, Permit.RegistrationNumber, Permit.ArrivalDepartureId, Permit.ValidFrom.Value, Permit.ValidTo.Value);
                    if (DepFlight!=null)
                        Flight = DIPermitLetter.GetArrivalFlight(DepFlight.RelationKey); 
                }
                entlcPermitLetter TmpobjPermitLetter = new entlcPermitLetter();
                TmpobjPermitLetter.FlightPermitNumber = Permit.FlightPermitNumber;
                TmpobjPermitLetter.Email = Permit.Email;
                TmpobjPermitLetter.AgtName = Permit.AgtName;
                TmpobjPermitLetter.OperatorName = Permit.OperatorName;
                TmpobjPermitLetter.Contact = Permit.Contact;
                TmpobjPermitLetter.CallSign = Permit.CallSign;
                TmpobjPermitLetter.RegistraionNo = Permit.RegistrationNumber;
                TmpobjPermitLetter.ApprovalDate = String.Format("{0:dd/MM/yyyy}", Permit.ApprovalDate);
                TmpobjPermitLetter.RequestDate = String.Format("{0:dd/MM/yyyy}", Permit.RequestDate);

                TmpobjPermitLetter.RequestBay = Permit.RequestBay;
                TmpobjPermitLetter.AircraftIATACode = Permit.AircraftIATACode;
                TmpobjPermitLetter.Purpose = Permit.FlightPurpose;
                
                    if (Flight!=null && Flight.StandardTimeOfArrival != null)
                    {
                        DateTime tmpArrival = TimeZoneInfo.ConvertTime(Flight.StandardTimeOfArrival.Value, TimeZoneInfo.Local);
                        TmpobjPermitLetter.ArrDate = String.Format("{0:dd/MM/yyyy}", tmpArrival);
                        TmpobjPermitLetter.ArrTime = String.Format("{0:HH:mm}", tmpArrival) + " LT";
                        TmpobjPermitLetter.Origin = Flight.OriginAirportICAOCode;
                        if (Flight.OriginAirportIATACode != "")
                            TmpobjPermitLetter.Origin = TmpobjPermitLetter.Origin + "/" + Flight.OriginAirportIATACode;
                    }
                    if (DepFlight != null && DepFlight.StandardTimeOfDeparture != null)
                    {
                        DateTime tmpDeparture = TimeZoneInfo.ConvertTime(DepFlight.StandardTimeOfDeparture.Value, TimeZoneInfo.Local);
                        TmpobjPermitLetter.DepDate = String.Format("{0:dd/MM/yyyy}", tmpDeparture);
                        TmpobjPermitLetter.DepTime = String.Format("{0:HH:mm}", tmpDeparture) + " LT";
                        TmpobjPermitLetter.Destination = DepFlight.DestinationAirportICAOCode;
                        if (DepFlight.DestinationAirportIATACode != "")
                            TmpobjPermitLetter.Destination = TmpobjPermitLetter.Destination + "/" + DepFlight.DestinationAirportIATACode;
                    }
                objPermitLetter = TmpobjPermitLetter;
            }
        }

        private void GetFlightPermits(int urno)
        {
            Permit = DIPermitLetter.LoadFlightPermit(urno);
        }

        public EntDbFlightPermit Permit
        {
            get
            {
                return _permit;
            }
            set
            {
                if (_permit != value)
                {
                    _permit = value;
                    OnPropertyChanged("Permit");
                }
            }
        }
        public EntDbFlight Flight
        {
            get
            {
                return _flight;
            }
            protected set
            {
                if (_flight != value)
                {
                    _flight = value;
                    OnPropertyChanged("Flight");
                }
            }
        }

        public EntDbFlight DepFlight
        {
            get
            {
                return _depflight;
            }
            protected set
            {
                if (_depflight != value)
                {
                    _depflight = value;
                    OnPropertyChanged("DepFlight");
                }
            }
        }
    }
}
