﻿using System;
using System.Linq;
using System.Collections.Generic;
using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using Standard_Reports.DataAccess;
using Standard_Reports.Entities;

namespace Standard_Reports.Reports.BaggageBeltAllocation
{
    public class BaggageBeltAllocationViewModel : WorkspaceViewModel
    {
        #region +++ Variable Declaration +++
        public WorkspaceViewModel _workspace;
        IList<entlcBaggageBeltAllocation> _itemSource;
        List<entlcBaggageBeltAllocation> objBaggageBeltAllocation;
        EntityCollectionBase<EntFlight> objArrivalFlights;
        EntFML objFMLTab;
        EntLoaTab objPaxOff;
        EntLoaTab objBagOff;
        DateTime? date;
        #endregion

        #region +++ Data Manipulation +++
        /// <summary>
        /// Get Flight list by Date Range
        /// </summary>
        /// <param name="sFrom">System.DateTime containing sFrom</param>
        /// <param name="sTo">System.Datetime containing sTo</param>
        /// <param name="sFilter">System.Datetime containing sFilter</param>
        private void GetFlights(DateTime sFrom, DateTime sTo, string sFilter)
        {
            objArrivalFlights = DlBaggageBeltAllocation.LoadArrivalFlightListByDateRange(sFrom, sTo, sFilter);
            if (objArrivalFlights != null)
            {
                IList<string> urnoList = new List<string>();

                foreach (EntFlight flight in objArrivalFlights)
                {
                    urnoList.Add(flight.Urno.ToString());
                }

                if (urnoList.Count > 0)
                    HValue = DlBaggageBeltAllocation.LoadHValue(urnoList);
            }
        }

        private void GetFMLTab(int urno)
        {
            objFMLTab = DlBaggageBeltAllocation.LoadDelayReasonFromFML(urno);
           
        }

        private void GetPaxOff(int urno)
        {
            objPaxOff = DlBaggageBeltAllocation.LoadPaxOffFromLOATAB(urno);

        }

        private void GetBagOff(int urno,string sadid)
        {
            objBagOff = DlBaggageBeltAllocation.LoadBagOffFromLOATAB(urno,sadid);

        }

        IList<EntHValue> _hValue;
        public IList<EntHValue> HValue
        {
            get
            {
                return _hValue;
            }
            set
            {
                if (_hValue != value)
                {
                    _hValue = value;
                    OnPropertyChanged("HValue");
                }
            }
        }
        /// <summary>
        /// Get Arrival Flight
        /// </summary>
        /// <param name="sFrom">System.DateTime Containing sFrom</param>
        /// <param name="sTo">Sytstem.DateTime containing sTo</param>
        /// <param name="sFilter">Sytstem.DateTime containing sFilter</param>
        /// <param name="sTo">Sytstem.DateTime containing sTo</param>
        /// <param name="sFilter">Sytstem.DateTime containing sFilter</param>
        public void GetBaggageBeltAllocationList(DateTime sFrom, DateTime sTo, string sFilter)
        {
            GetFlights(sFrom, sTo, sFilter);
            GetArrivalFlighsts();
            ItemSource = objBaggageBeltAllocation;
        }

        /// <summary>
        /// Do process for Baggage Belt 
        /// </summary>
        private void GetArrivalFlighsts()
        {
            if (objArrivalFlights.Count > 0)
            {
                objBaggageBeltAllocation = new List<entlcBaggageBeltAllocation>();
               
                foreach (EntFlight tmpObjArrivalFlight in objArrivalFlights)
                {                   
                    TimeSpan? tmpchonToFirstBag;
                    TimeSpan? tmponblToFirstBag;
                    int chontoFirstMin = 0;
                    int onbltoFirstMin = 0;
                    DateTime? tmpOnChockTime = null;
                    DateTime? tmpOnBlockTime = null;
                    DateTime? tmpFirstBag = null;
                    DateTime? tmpLastBag = null;
                    TimeSpan? tmpchonToLastBag;
                    TimeSpan? tmponblToLastBag;
                    int chontoLastMin = 0;
                    int onblToLastMin = 0;
                    entlcBaggageBeltAllocation TmpobjBaggageBeltAllocation = new entlcBaggageBeltAllocation();
                    TmpobjBaggageBeltAllocation.Number = objBaggageBeltAllocation.Count + 1;

                    if (HValue != null)
                    {
                        foreach (EntHValue h in HValue)
                        {
                            if (h.UrnoOfFlight == tmpObjArrivalFlight.Urno)
                            {
                                if (h.OriginalValue != "")
                                    TmpobjBaggageBeltAllocation.BeltAllocated = h.OriginalValue;
                                else
                                    TmpobjBaggageBeltAllocation.BeltAllocated = h.SecondValue;
                            }
                        }
                    }

                    TmpobjBaggageBeltAllocation.BeltUsed = tmpObjArrivalFlight.Belt1;
                    TmpobjBaggageBeltAllocation.AircraftIATACode = tmpObjArrivalFlight.AircraftIATACode;
                    
                    if (tmpObjArrivalFlight.FirstBag != null)
                    {
                        date = tmpObjArrivalFlight.FirstBag;
                        if (date.HasValue)
                        {
                            TmpobjBaggageBeltAllocation.FirstBag = String.Format("{0:HH:mm}", date.Value);
                            tmpFirstBag = Convert.ToDateTime(String.Format("{0:dd.MM.yyyy HH:mm tt}", date.Value));
                        }
                    }

                    if (tmpObjArrivalFlight.LastBag != null)
                    {
                        date = tmpObjArrivalFlight.LastBag;
                        if (date.HasValue)
                        {
                            TmpobjBaggageBeltAllocation.LastBag = String.Format("{0:HH:mm}", date.Value);
                            tmpLastBag = Convert.ToDateTime(String.Format("{0:dd.MM.yyyy HH:mm tt}", date.Value));
                        }
                    }

                    if (tmpObjArrivalFlight.OnblockTime != null)
                    {
                        date = tmpObjArrivalFlight.OnblockTime;
                        if (date.HasValue)
                        {
                            TmpobjBaggageBeltAllocation.OnBlockTime = String.Format("{0:HH:mm}", date.Value);
                            tmpOnBlockTime = Convert.ToDateTime(String.Format("{0:dd.MM.yyyy HH:mm tt}", date.Value));
                        }
                    }

                    if (tmpObjArrivalFlight.OnchockTime != null)
                    {
                        date = tmpObjArrivalFlight.OnchockTime;
                        if (date.HasValue)
                        {
                           TmpobjBaggageBeltAllocation.OnChockTime = String.Format("{0:HH:mm}", date.Value);
                           tmpOnChockTime = Convert.ToDateTime(String.Format("{0:dd.MM.yyyy HH:mm tt}", date.Value));
                        }
                    }

                    //if (tmpObjArrivalFlight.FirstBag != null && tmpObjArrivalFlight.OnchockTime != null)
                    if (tmpFirstBag!=null && tmpOnChockTime!=null)
                    {
                        //tmpchonToFirstBag = tmpObjArrivalFlight.FirstBag - tmpObjArrivalFlight.OnchockTime;
                        tmpchonToFirstBag=tmpFirstBag - tmpOnChockTime;
                        chontoFirstMin = Convert.ToInt16(tmpchonToFirstBag.Value.TotalMinutes);
                        //if (chontoFirstMin > 12)
                        //    TmpobjBaggageBeltAllocation.ChonToFirstBag = chontoFirstMin.ToString() + "*";
                        //else
                        TmpobjBaggageBeltAllocation.ChonToFirstBag = chontoFirstMin.ToString();
                    }

                    //if (tmpObjArrivalFlight.LastBag != null && tmpObjArrivalFlight.OnchockTime != null)
                    if (tmpLastBag!=null && tmpOnChockTime!=null)
                    {
                        //tmpchonToLastBag = tmpObjArrivalFlight.LastBag - tmpObjArrivalFlight.OnchockTime;
                        tmpchonToLastBag = tmpLastBag - tmpOnChockTime;
                        chontoLastMin = Convert.ToInt16(tmpchonToLastBag.Value.TotalMinutes);
                        //if (chontoLastMin > 28) 
                        //TmpobjBaggageBeltAllocation.ChonToLastBag = chontoLastMin.ToString() + "*";
                        //else
                        TmpobjBaggageBeltAllocation.ChonToLastBag = chontoLastMin.ToString();
                    }

                    //if (tmpObjArrivalFlight.FirstBag != null && tmpObjArrivalFlight.OnblockTime != null)
                    if (tmpFirstBag!=null && tmpOnBlockTime!=null)
                    {
                        //tmponblToFirstBag = tmpObjArrivalFlight.FirstBag - tmpObjArrivalFlight.OnblockTime;
                        tmponblToFirstBag = tmpFirstBag - tmpOnBlockTime;
                        onbltoFirstMin = Convert.ToInt16(tmponblToFirstBag.Value.TotalMinutes);
                        TmpobjBaggageBeltAllocation.OnblToFirstBag = onbltoFirstMin.ToString();
                    }

                    //if (tmpObjArrivalFlight.LastBag != null && tmpObjArrivalFlight.OnblockTime != null)
                    if (tmpLastBag!=null && tmpOnBlockTime!=null)
                    {
                        //tmponblToLastBag = tmpObjArrivalFlight.LastBag - tmpObjArrivalFlight.OnblockTime;
                        tmponblToLastBag = tmpLastBag - tmpOnBlockTime;
                        onblToLastMin = Convert.ToInt16(tmponblToLastBag.Value.TotalMinutes);
                        TmpobjBaggageBeltAllocation.OnblToLastBag = onblToLastMin.ToString();
                    }

                    TmpobjBaggageBeltAllocation.ScheduledFirstBag = tmpObjArrivalFlight.FirstBag;
                    TmpobjBaggageBeltAllocation.Terminal = tmpObjArrivalFlight.TerminalBaggageBelt1;

                    if (string.IsNullOrEmpty(tmpObjArrivalFlight.FullFlightNumber))
                        TmpobjBaggageBeltAllocation.Arrival = tmpObjArrivalFlight.CallSign;
                    else
                        TmpobjBaggageBeltAllocation.Arrival = tmpObjArrivalFlight.FullFlightNumber;

                    TmpobjBaggageBeltAllocation.From = tmpObjArrivalFlight.OriginAirportIATACode;

                    if (tmpObjArrivalFlight.StandardTimeOfArrival != null)
                    {
                        date = tmpObjArrivalFlight.StandardTimeOfArrival;
                        if (date.HasValue)
                        {
                            TmpobjBaggageBeltAllocation.STA = String.Format("{0:dd/HHmm}", date.Value);
                        }
                    }

                    TmpobjBaggageBeltAllocation.APos = tmpObjArrivalFlight.PositionOfArrival;

                  
                    //Zaw Min Tun
                    //Getting the delay reason from CRATAB and CRCTAB
                    //TmpobjBaggageBeltAllocation.BaggageDelayReason = DlBaggageBeltAllocation.GetDelayReasons(tmpObjArrivalFlight.Urno.ToString(), true, false); 

                    //get delay reason from FMLTAB
                    GetFMLTab(tmpObjArrivalFlight.Urno);
                    if (objFMLTab!=null) 
                        TmpobjBaggageBeltAllocation.BaggageDelayReason = objFMLTab.TEXT;

                    GetPaxOff(tmpObjArrivalFlight.Urno);
                    if (objPaxOff != null)
                        TmpobjBaggageBeltAllocation.APax = objPaxOff.VALU;
                    GetBagOff(tmpObjArrivalFlight.Urno, tmpObjArrivalFlight.ArrivalDepartureId);
                    if (objBagOff != null)
                        TmpobjBaggageBeltAllocation.ABag = objBagOff.VALU;

                    objBaggageBeltAllocation.Add(TmpobjBaggageBeltAllocation);
                    
                }
            }
        }

        /// <summary>
        /// Get Parameter Value
        /// </summary>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetParameterValue()
        {
            string sReturnValue = "";

            EntityCollectionBase<EntParameter> objParameter = DlCommon.GetEntParameterValue("ID_APR_FT");

            if (objParameter != null)
            {
                foreach (EntParameter objTmpParameter in objParameter)
                {
                    sReturnValue = objTmpParameter.Value;
                }
            }
            return sReturnValue;
        }

        /// <summary>
        /// Get Login User Name
        /// </summary>
        /// <param name="USID">System.String containing USID</param>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetLoginUserName(string USID)
        {
            string sReturnValue = "";

            EntityCollectionBase<EntSecured> objUser = DlCommon.GetEntLoginUserName(USID);

            if (objUser != null)
            {
                foreach (EntSecured objTmpUser in objUser)
                {
                    sReturnValue = objTmpUser.Name;
                }
            }
            return sReturnValue;
        }
        #endregion

        #region +++ Properties +++
        public IList<entlcBaggageBeltAllocation> ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource != value)
                {
                    _itemSource = value;
                    OnPropertyChanged("ItemSource");
                }
            }
        }
        #endregion
    }
}
