﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;

namespace Standard_Reports.Reports
{
    public class MyCoverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }


    [ValueConversion(typeof(string), typeof(IList<object>))]
    public class StringToObjectListConverter : IValueConverter
    {
        #region IValueConverter Members
        object IValueConverter.Convert(object value, Type targetType,
        object parameter, System.Globalization.CultureInfo culture)
        {
            string strValue = value as string;

            if (string.IsNullOrEmpty(strValue))
                return null;

            return new List<object>(strValue.Split(','));
        }

        object IValueConverter.ConvertBack(object value, Type targetType,
        object parameter, System.Globalization.CultureInfo culture)
        {
            IList<object> valueList = value as IList<object>;

            if (valueList == null)
                return string.Empty;

            return string.Join(",", valueList.Select(s => s.ToString()));
        }

        #endregion
    }
}
