﻿using System;
using System.Linq;
using System.Windows;
using System.Threading;
using System.Globalization;
using System.Windows.Controls;
using System.Collections.Generic;
using Ufis.Entities;
using Standard_Reports.Helpers;
using System.ComponentModel;
using Ufis.LoginWindow;
using Ufis.IO;
using System.Windows.Media.Imaging;
using DevExpress.Xpf.Printing;
using DevExpress.Xpf.Grid;
using System.Drawing.Printing;


namespace Standard_Reports.Reports.AirCraftsOnGround
{
    public partial class AirCraftsOnGroundView : UserControl
    {
        string sFooterString = string.Empty;
        public HpParameters param;
        public HpReportLogo Logo;
        public HpFooter Footer;
        public AirCraftsOnGroundViewModel objViewModel;
        public IList<EntDbFlight> FlightList;
        DateTime tfDate;
        DateTime ttDate;


        #region +++ Register Properties +++
        // Using a DependencyProperty as the backing store for MyInt.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyIntProperty =
        DependencyProperty.Register("MyInt", typeof(int), typeof(AirCraftsOnGroundViewModel), new UIPropertyMetadata(1));
        #endregion

        #region +++ Properties +++
        public int MyInt
        {
            get
            {
                return (int)GetValue(MyIntProperty);
            }
            set
            {
                SetValue(MyIntProperty, value);
            }
        }
        #endregion

        #region +++ WPF Application Control Events +++
        public AirCraftsOnGroundView()
        {
            CultureInfo ci = new CultureInfo(Thread.CurrentThread.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            Thread.CurrentThread.CurrentCulture = ci;
            InitializeComponent();
            dtpFromDate.Text = ("01.01.2013");
            dtpToDate.Text = DateTime.Now.Date.ToString("dd.MM.yyyy");
            txtFromTime.Text = "0000";
            txtToTime.Text = "2359";
        }

        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dtpFromDate.SelectedDate.ToString() != "" && dtpToDate.SelectedDate.ToString() != "" && txtFromTime.Text != "" && txtToTime.Text != "" && dtpToDate.SelectedDate != null && dtpFromDate.SelectedDate != null && txtFromTime.Text.Length == 4 && txtToTime.Text.Length == 4)
                {
                        string sfDate, stDate;
                    tfDate = dtpFromDate.SelectedDate.Value;
                    ttDate = dtpToDate.SelectedDate.Value;
                    sfDate = String.Format("{0:yyyy.MM.dd} {1}", tfDate, txtFromTime.Text.Insert(2, ":"));
                    stDate = String.Format("{0:yyyy.MM.dd} {1}", ttDate, txtToTime.Text.Insert(2, ":"));

                    try
                    {
                        tfDate = Convert.ToDateTime(sfDate);
                        ttDate = Convert.ToDateTime(stDate);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Time value should be between 0000 and 2359.");
                    }

                    if (tfDate > ttDate)
                    {
                        MessageBox.Show("Invalid Date/time value.");
                    }
                    tfDate.AddHours(Convert.ToDouble(txtFromTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtFromTime.Text.Substring(2, 2))).AddSeconds(0);
                    ttDate.AddHours(Convert.ToDouble(txtToTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtToTime.Text.Substring(2, 2))).AddSeconds(59);

                    grdAirCraftsOnGround.ShowLoadingPanel = true;

                    BackgroundWorker bw = new BackgroundWorker();
                    bw.DoWork += Process;
                    bw.RunWorkerCompleted += bgw_RunWorkerCompleted;
                    bw.RunWorkerAsync();
                }
                else
                {
                    MessageBox.Show("Invalid Date/Time value.");
                    grdAirCraftsOnGround.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0}\n\r{1}\n\r{2}", ex.Message, ex.InnerException, ex.StackTrace));
            }
        }

        private void Process(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(2000);
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                FetchData();
            }));
        }

        private void FetchData()
        {
            objViewModel = new AirCraftsOnGroundViewModel();
            objViewModel.GetAirCraftOnGroundView(tfDate, ttDate);

            grdAirCraftsOnGround.ItemsSource = objViewModel.ItemSource;
        }

        private void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            grdAirCraftsOnGround.ShowLoadingPanel = false;
        }

        private void barButtonPreview_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            if (grdAirCraftsOnGround.VisibleRowCount > 0)
            {
                Ufis.Entities.EntUser objEntUser = new Ufis.Entities.EntUser();
                objEntUser = LoginViewModel.GetLastUser();
                objViewModel = new AirCraftsOnGroundViewModel();
                param = new HpParameters();
                Footer = new HpFooter();
                DateTime tDate = dtpFromDate.SelectedDate.Value;
                string sDate = tDate.ToString("dd.MM.yyyy");
                DateTime ttDate = dtpToDate.SelectedDate.Value;
                string stDate = ttDate.ToString("dd.MM.yyyy");

                sFooterString = objViewModel.GetParameterValue();

                param.MyParameter = String.Format("Non-Scheduled Aircrafts on Ground Report From {0} {1} To {2} {3} {4} ", sDate, txtFromTime.Text, stDate, txtToTime.Text, HpUser.TLocalUTC == "L" ? "LT" : "UTC");
                param.Header = String.Format("                                                                    List of non-schedule aricrafts on ground between {0} and {2} ", sDate, txtFromTime.Text, stDate, txtToTime.Text, HpUser.TLocalUTC == "L" ? "LT" : "UTC");
                param.ReportFooterString = sFooterString;
                param.GeneratedBy = "Printed On " + string.Format("{0:f}", DateTime.Now);

                ((HpParameters)Resources["paramet"]).MyParameter = param.MyParameter;
                ((HpParameters)Resources["NewHeader"]).Header = param.Header;
                ((HpParameters)Resources["Footer"]).ReportFooterString = param.ReportFooterString;
                ((HpParameters)Resources["GenBy"]).GeneratedBy = param.GeneratedBy;

                string sLogoPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.UfisSystem);
                sLogoPath = sLogoPath + "\\stdlogo.bmp";

                Logo = new HpReportLogo();
                Logo.RptLogo = new BitmapImage(new Uri(sLogoPath, UriKind.Absolute));
                ((HpReportLogo)Resources["Logo"]).RptLogo = Logo.RptLogo;

                PrintableControlLink link = new PrintableControlLink((TableView)grdAirCraftsOnGround.View) { Margins = new Margins(50, 0, 30, 30), Landscape = true };
                link.PageHeaderTemplate = (DataTemplate)Resources["detailPrintHeaderTemplate"];
                link.PaperKind = PaperKind.A4;
                link.PageFooterTemplate = (DataTemplate)Resources["DetailPrintFooterTemplate"];
                link.ShowPrintPreviewDialog(Window.GetWindow(this));
            }
        }
    }
}
#endregion
