﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Globalization;
using System.Threading;
using System.ComponentModel;
using Ufis.LoginWindow;
using Standard_Reports.Helpers;
using Ufis.IO;
using DevExpress.Xpf.Printing;
using DevExpress.Xpf.Grid;
using System.Drawing.Printing;
using Ufis.Entities;
using Ufis.Data;


namespace Standard_Reports.Reports.Towing
{
    /// <summary>
    /// Interaction logic for TowingReportView.xaml
    /// </summary>
    public partial class TowingReportView : UserControl
    {
        public TowingReportViewModel objViewModel;
        public HpParameters param;
        public HpReportLogo Logo;
        public HpFooter Footer;
        string sFooterString = string.Empty;
        string sNoteString = string.Empty;
        string sEmailString = string.Empty;
        string sTelephoneString = string.Empty;
        DateTime tfDate;
        DateTime ttDate;
        #region +++ Register Properties +++
        // Using a DependencyProperty as the backing store for MyInt.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyIntProperty =
        DependencyProperty.Register("MyInt", typeof(int), typeof(TowingReportView), new UIPropertyMetadata(1));
        #endregion

        #region +++ Properties +++
        public int MyInt
        {
            get
            {
                return (int)GetValue(MyIntProperty);
            }
            set
            {
                SetValue(MyIntProperty, value);
            }
        }
        #endregion

        public TowingReportView()
        {
            CultureInfo ci = new CultureInfo(Thread.CurrentThread.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            Thread.CurrentThread.CurrentCulture = ci;
            InitializeComponent();
            dtpFromDate.Text = DateTime.Now.Date.ToString("dd.MM.yyyy");
            dtpToDate.Text = DateTime.Now.Date.ToString("dd.MM.yyyy");
            txtFromTime.Text = "0000";
            txtToTime.Text = "2359";
          
        }

        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dtpFromDate.SelectedDate.ToString() != "" && dtpToDate.SelectedDate.ToString() != "" && txtFromTime.Text != "" && txtToTime.Text != "" && dtpToDate.SelectedDate != null && dtpFromDate.SelectedDate != null && txtFromTime.Text.Length == 4 && txtToTime.Text.Length == 4)
                {
                    string sfDate, stDate;
                    tfDate = dtpFromDate.SelectedDate.Value;
                    ttDate = dtpToDate.SelectedDate.Value;
                    sfDate = String.Format("{0:yyyy.MM.dd} {1}", tfDate, txtFromTime.Text.Insert(2, ":"));
                    stDate = String.Format("{0:yyyy.MM.dd} {1}", ttDate, txtToTime.Text.Insert(2, ":"));

                    try
                    {
                        tfDate = Convert.ToDateTime(sfDate);
                        ttDate = Convert.ToDateTime(stDate);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Time value should be between 0000 and 2359.");
                    }

                    if (tfDate > ttDate)
                    {
                        MessageBox.Show("Invalid Date/time value.");
                    }
                    tfDate.AddHours(Convert.ToDouble(txtFromTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtFromTime.Text.Substring(2, 2))).AddSeconds(0);
                    ttDate.AddHours(Convert.ToDouble(txtToTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtToTime.Text.Substring(2, 2))).AddSeconds(59);

                    grdTowingReport.ShowLoadingPanel = true;

                    BackgroundWorker bw = new BackgroundWorker();
                    bw.DoWork += Process;
                    bw.RunWorkerCompleted += bgw_RunWorkerCompleted;
                    bw.RunWorkerAsync();
                }
                else
                {
                    MessageBox.Show("Invalid Date/Time value.");
                    grdTowingReport.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0}\n\r{1}\n\r{2}", ex.Message, ex.InnerException, ex.StackTrace));
            }
        }

        private void barButtonPreview_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
           
            if (grdTowingReport.VisibleRowCount > 0)
            {
                try
                {
                    Ufis.Entities.EntUser objEntUser = new Ufis.Entities.EntUser();
                    objEntUser = LoginViewModel.GetLastUser();
                    objViewModel = new TowingReportViewModel();


                    param = new HpParameters();
                    Footer = new HpFooter();
                    DateTime tDate = dtpFromDate.SelectedDate.Value;
                    string sDate = tDate.ToString("dd/MM/yyyy");
                    DateTime ttDate = dtpToDate.SelectedDate.Value;
                    string stDate = ttDate.ToString("dd/MM/yyyy");

                    sFooterString = objViewModel.GetParameterValue();
                    sNoteString = objViewModel.GetConfiguration("REPORTS", "TOWING", "FOOTER_CUSTOM_NOTE");
                    sTelephoneString = objViewModel.GetConfiguration("REPORTS", "TOWING", "TELEPHONE");
                    sEmailString = objViewModel.GetConfiguration("REPORTS", "TOWING", "EMAIL");

                    param.MyParameter = String.Format("                        Towing Report from {0} {1} to {2} {3} {4} ", sDate, txtFromTime.Text, stDate, txtToTime.Text, String.Compare(HpUser.TLocalUTC, "L", false) == 0 ? "LT" : "UTC");
                    param.Header = "                                                            Airport Operations Control Center (AOCC)";

                    param.ReportFooterString = sFooterString;

                    if (sNoteString.Contains("&#44;")) sNoteString = sNoteString.Replace("&#44;", ",");
                    param.ReportNoteString = sNoteString;
                    if (sTelephoneString.Contains("&#44;")) sTelephoneString = sTelephoneString.Replace("&#44;", ",");
                    param.ReportTelephoneString = sTelephoneString;
                    if (sEmailString.Contains("&#44;")) sEmailString = sEmailString.Replace("&#44;", ",");
                    param.ReportEmailString = sEmailString;

                    param.ReportNoteString = "Note : " + sNoteString;
                    param.ReportTelephoneString = sTelephoneString;
                    param.ReportEmailString = sEmailString;
                    param.GeneratedBy = string.Format("Printed By : {0}", objEntUser.UserId);
                    ((HpParameters)Resources["paramet"]).MyParameter = param.MyParameter;
                    ((HpParameters)Resources["NewHeader"]).Header = param.Header;
                    ((HpParameters)Resources["Footer"]).ReportFooterString = param.ReportFooterString;
                    ((HpParameters)Resources["Note"]).ReportNoteString = param.ReportNoteString;
                    ((HpParameters)Resources["Note"]).ReportTelephoneString = param.ReportTelephoneString;
                    ((HpParameters)Resources["Note"]).ReportEmailString = param.ReportEmailString;
                    ((HpParameters)Resources["GenBy"]).GeneratedBy = param.GeneratedBy;


                    string sLogoPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.UfisSystem);
                    sLogoPath = sLogoPath + "\\stdlogo.bmp";


                    Logo = new HpReportLogo();
                    Logo.RptLogo = new BitmapImage(new Uri(sLogoPath, UriKind.Absolute));
                    ((HpReportLogo)Resources["Logo"]).RptLogo = Logo.RptLogo;

                    PrintableControlLink link = new PrintableControlLink((TableView)grdTowingReport.View) { Margins = new Margins(50, 50, 30, 30), Landscape = false };
                    link.PageHeaderTemplate = (DataTemplate)Resources["detailPrintHeaderTemplate"];
                    link.PaperKind = PaperKind.A4;
                    link.PageFooterTemplate = (DataTemplate)Resources["DetailPrintFooterTemplate"];

                    link.ShowPrintPreviewDialog(Window.GetWindow(this));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.InnerException.ToString());
                }
            }
        }


        private void Process(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(2000);
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                FetchData();
            }));
        }

        private void FetchData()
        {
            objViewModel = new TowingReportViewModel();
            objViewModel.GetTowingReport(tfDate, ttDate);
            grdTowingReport.ItemsSource = objViewModel.ItemSource;
        }

        private void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            grdTowingReport.ShowLoadingPanel = false;
        }

    }
}
