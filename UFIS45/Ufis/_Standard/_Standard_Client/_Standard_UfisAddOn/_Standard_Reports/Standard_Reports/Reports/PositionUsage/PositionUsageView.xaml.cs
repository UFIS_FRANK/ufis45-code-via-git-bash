﻿using System;
using System.Linq;
using System.Windows;
using System.Threading;
using System.Globalization;
using System.Windows.Controls;
using DevExpress.Xpf.Printing;
using System.Collections.Generic;
using Ufis.IO;
using System.Windows.Media.Imaging;
using DevExpress.Xpf.Charts;
using System.Collections;
using System.Windows.Media;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Ufis.LoginWindow;
using Standard_Reports.Helpers;
using System.Drawing.Printing;
using Ufis.Entities;


namespace Standard_Reports.Reports.PositionUsage
{
    /// <summary>
    /// Interaction logic for PositionUsageView.xaml
    /// </summary>
    public partial class PositionUsageView : UserControl
    {
        #region +++ Variable Declaration +++
        public HpParameters param;
        public HpReportLogo Logo;
        //public HpChartLogo ChartLogo;
        public HpFooter Footer;
        public PositionUsageViewModel objViewModel;
        string sFooterString = "";
        DateTime tfDate;
        DateTime ttDate;
        //private string strRemoteContact = string.Empty;
        //private string strArrDep = string.Empty;
        string strFilter = string.Empty;
        private static int totalHours = 24;
        private double[] _flightPosition = new double[24];
        private string[] _flightNo = new string[24];
        private string[] _PositionName = new string[24];
        private string[] _argument = new string[24];
        private string[] _PositionNameUnChangeable = new string[24];
        #endregion


        #region +++ Register Properties +++
        // Using a DependencyProperty as the backing store for MyInt.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyIntProperty =
        DependencyProperty.Register("MyInt", typeof(int), typeof(PositionUsageView), new UIPropertyMetadata(1));
        #endregion

        #region +++ Properties +++
        public int MyInt
        {
            get
            {
                return (int)GetValue(MyIntProperty);
            }
            set
            {
                SetValue(MyIntProperty, value);
            }
        }
        #endregion

        #region +++ WPF Application Events +++
        public PositionUsageView()
        {
            //Change UFIS Date format into system.Thread
            CultureInfo ci = new CultureInfo(Thread.CurrentThread.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            Thread.CurrentThread.CurrentCulture = ci;
            InitializeComponent();
            dtpFromDate.Text = DateTime.Now.Date.ToString("dd.MM.yyyy");// "11.04.2012";
            dtpToDate.Text = DateTime.Now.Date.ToString("dd.MM.yyyy"); //"11.04.2012";
            txtFromTime.Text = "0000";
            txtToTime.Text = "2359";
            //FillCustomAxisLabels();
            positionComboBox.SelectAllItems();
            //UnChecked Items (Phyoe - 13-Aug-2012)
            //string sRemoveList = "AGA,F6,ADT,TWYG,135,PFA";            
            string sRemoveList = GetPOS_UnCheckList();
            cboTemplate.SelectedIndex = 2;
            if (!string.IsNullOrEmpty(sRemoveList.Trim()))
            {
                string[] resultsArray = sRemoveList.Split(new [] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                //Initialise to Ilist
                IList selectedList = positionComboBox.EditValue as IList;
                if (selectedList != null)
                {
                    //Loop the array
                    Array.ForEach(resultsArray, selectedList.Remove);
                }
            }
        }

        private void barButtonPreview_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            try
            {
                if (objViewModel != null)
                {
                    if (objViewModel.ItemSource != null)
                    {
                        EntUser objEntUser = new EntUser();
                        objEntUser = LoginViewModel.GetLastUser();

                        param = new HpParameters();
                        Footer = new HpFooter();
                        DateTime tDate = dtpFromDate.SelectedDate.Value;
                        string sDate = tDate.ToString("dd.MM.yyyy");
                        DateTime ttDate = dtpToDate.SelectedDate.Value;
                        string stDate = ttDate.ToString("dd.MM.yyyy");

                        sFooterString = objViewModel.GetParameterValue();

                        if (!string.IsNullOrEmpty(Helpers.HpUser.Sco))
                        {
                            sFooterString = String.Format("{0}    Scenario : {1}", sFooterString, Helpers.HpUser.Sco);
                        }

                        param.MyParameter = String.Format("Stand Usage Report from {0} {1} to {2} {3} {4} ", sDate, txtFromTime.Text, stDate, txtToTime.Text, String.Compare(HpUser.TLocalUTC, "L", false) == 0 ? "LT" : "UTC");
                        param.ReportFooterString = sFooterString;
                        param.GeneratedBy = string.Format("Printed By : {0}", objEntUser.UserId);


                        ((HpParameters)Resources["paramet"]).MyParameter = param.MyParameter;
                        ((HpParameters)Resources["Footer"]).ReportFooterString = param.ReportFooterString;
                        ((HpParameters)Resources["GenBy"]).GeneratedBy = param.GeneratedBy;
                        

                        string sLogoPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.UfisSystem);
                        sLogoPath = sLogoPath + "\\stdlogo.bmp";

                        //string sChartPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.UfisSystem);
                        //sChartPath = sChartPath + "\\PChartSample.bmp";

                        Logo = new HpReportLogo();
                        Logo.RptLogo = new BitmapImage(new Uri(sLogoPath, UriKind.Absolute));
                        ((HpReportLogo)Resources["Logo"]).RptLogo = Logo.RptLogo;

                        //ChartLogo = new HpChartLogo();
                        //ChartLogo.ChartLogo = new BitmapImage(new Uri(sChartPath, UriKind.Absolute));
                        //((HpChartLogo)Resources["ChartLogo"]).ChartLogo = ChartLogo.ChartLogo;

                        SimpleLink link = new SimpleLink();
                        link.DetailCount = 1;
                        link.Margins = new Margins(0, 0, 0, 0);
                        link.Landscape = true;
                        link.PaperKind = PaperKind.A4;
                        link.DetailTemplate = (DataTemplate)Resources["DetailPrintChartTemplate"];
                        link.PageHeaderTemplate = (DataTemplate)Resources["DetailPrintHeaderTemplate"];
                        link.PageFooterTemplate = (DataTemplate)Resources["DetailPrintFooterTemplate"];
                        link.CreateDetail += new EventHandler<DevExpress.Xpf.Printing.CreateAreaEventArgs>(link_CreateDetail);
                        link.CreateDocument(false);
                        link.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                        Mouse.OverrideCursor = null;
                        link.ShowPrintPreviewDialog(Window.GetWindow(this));
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }

        void link_CreateDetail(object sender, DevExpress.Xpf.Printing.CreateAreaEventArgs e)
        {
            VisualBrush brush = new VisualBrush(PositionUsageChart);
            DrawingVisual visual = new DrawingVisual();
            DrawingContext context = visual.RenderOpen();

            //context.DrawRectangle(brush, null, new Rect(0, 0, 1024, 525));
            context.DrawRectangle(brush, null, new Rect(0, 0, 1180, 574));

            context.Close();

            RenderTargetBitmap bmp = new RenderTargetBitmap((int)1180, (int)574, 96, 96,  PixelFormats.Pbgra32);
            bmp.Render(visual);

            e.Data = bmp;
        }

        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            GeneratePositionUsageReportDate();
        }

        private void GeneratePositionUsageReportDate()
        {
            Mouse.OverrideCursor = Cursors.Wait;
            try
            {
                if (dtpFromDate.SelectedDate.ToString() != "" && dtpToDate.SelectedDate.ToString() != "" && txtFromTime.Text != "" && txtToTime.Text != "" && dtpToDate.SelectedDate != null && dtpFromDate.SelectedDate != null && txtFromTime.Text.Length >= 4 && txtToTime.Text.Length >= 4)
                {
                    objViewModel = new PositionUsageViewModel();
                        string sfDate, stDate;
                    tfDate = dtpFromDate.SelectedDate.Value;
                    ttDate = dtpToDate.SelectedDate.Value;
                    sfDate = String.Format("{0:yyyy.MM.dd} {1}", tfDate, txtFromTime.Text.Insert(2, ":"));
                    stDate = String.Format("{0:yyyy.MM.dd} {1}", ttDate, txtToTime.Text.Insert(2, ":"));

                    try
                    {
                        tfDate = Convert.ToDateTime(sfDate);
                        ttDate = Convert.ToDateTime(stDate);
                        strFilter = positionComboBox.Text.Replace(";", "','");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(String.Format("Time value should be between{0} and {1}.", txtFromTime, txtToTime));
                    }

                    if (tfDate > ttDate)
                    {
                        MessageBox.Show("From date time period should be greater than To date time period.");
                    }
                    else
                    {
                        TimeSpan ts = ttDate - tfDate;
                        int itotalHours = Convert.ToInt32(ts.TotalMinutes / 60);

                        if (ts.Days >= 1 && ts.Minutes >= 1 || itotalHours > totalHours)
                        {
                            MessageBox.Show(String.Format("Seleceted time period should not exceed a total of {0} hours.", totalHours));
                        }
                        else
                        {
                            tfDate.AddHours(Convert.ToDouble(txtFromTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtFromTime.Text.Substring(2, 2))).AddSeconds(0);
                            if (txtToTime.Text.Substring(2, 2) == "00")
                                ttDate = ttDate.AddHours(-1).AddMinutes(59).AddSeconds(59);
                            //else
                            //    ttDate = ttDate.AddHours(Convert.ToDouble(txtToTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtToTime.Text.Substring(2, 2))).AddSeconds(59);

                            _argument = objViewModel.GetPositionUsageReportList(tfDate, ttDate, chkSpots.IsChecked.Value, strFilter);

                            CreatePositionUsageBarSeries();

                            if (objViewModel.FlightSource != null)
                            {
                                _flightPosition = objViewModel.FlightSource[0];
                            }
                            if (objViewModel.FlightNoSource != null)
                            {
                                _flightNo = objViewModel.FlightNoSource[0];
                            }
                            if (objViewModel.PositionName != null)
                            {
                                _PositionName = objViewModel.PositionName[0];
                            }
                            if (objViewModel.PositionNameUnChangeable != null)
                            {
                                _PositionNameUnChangeable = objViewModel.PositionNameUnChangeable[0];
                            }
                        }
                    }
                }

                else
                {
                    MessageBox.Show("Invalid Date/Time value.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0}\n\r{1}\n\r{2}", ex.Message, ex.InnerException, ex.StackTrace));
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }

        private void chkSpots_Checked(object sender, RoutedEventArgs e)
        {
            GeneratePositionUsageReportDate();
        }

        private void CreatePositionUsageBarSeries()
        {
            HpSeriesTypeItem seriesTypeItem = new HpSeriesTypeItem(typeof(XYDiagram2D), typeof(BarSideBySideStackedSeries2D), "2D Side-By-Side Stacked Bars", 2) as HpSeriesTypeItem;

            if (seriesTypeItem == null)
                return;

            PositionUsageChart.BeginInit();
            ClearCustomAxisLabels();
            //FillCustomAxisLabels();
            PositionUsageChartDiagram.Series.Clear();
            try
            {
                ArrayList objSeriesLable = SetSeriesLable();
                for (int i = 0; i < seriesTypeItem.SeriesCount; i++)
                {
                    Series series = (Series)Activator.CreateInstance(seriesTypeItem.SeriesType);
                    series.Label = new SeriesLabel();
                    series.DisplayName = objSeriesLable[i].ToString();

                    InitializeSeries(series, i + 1);
                    ISupportStackedGroup supportStackedGroup = series as ISupportStackedGroup;
                    if (supportStackedGroup != null)
                        supportStackedGroup.StackedGroup = i % 2;
                    ISupportTransparency supportTransparency = series as ISupportTransparency;
                    if (supportTransparency != null)
                        supportTransparency.Transparency = 0.3;
                    series.Label.FontSize = 14.00;
                    series.Label.ResolveOverlappingMode = ResolveOverlappingMode.Default;
                    PositionUsageChartDiagram.Series.Add(series);
                }
            }
            finally
            {
                PositionUsageChart.EndInit();
            }
        }
        private ArrayList SetSeriesLable()
        {
            ArrayList objSeriesLable = new ArrayList();
            objSeriesLable.Add("Occupied Stands");
            objSeriesLable.Add("Blocked Stands (Unchangeable)");
            //objSeriesLable.Add("");
            //objSeriesLable.Add(" Blocked Stands (Changeable)");
            return objSeriesLable;
        }

        private void InitializeSeries(Series series, int seriesNumber)
        {
            series.DataSource = objViewModel.ItemSource;
            series.ArgumentDataMember = "Argument";
            series.ValueDataMember = "Value" + seriesNumber;
        }
        /// <summary>
        /// Get Invalid Unchangeable Stands Name from Ceda.
        /// </summary>
        /// <returns>System.string containing Invalid Unchangeable Stands</returns>
        private string GetPOS_UnCheckList()
        {
            string sUnChkLst;

            string strCedaPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.Ceda);

            IniFile myIni = new IniFile(strCedaPath);

            sUnChkLst = !string.IsNullOrEmpty(myIni.IniReadValue("FIPSREPORT", "EXCLUDE_FROM_POS_USAGE")) ? Convert.ToString(myIni.IniReadValue("FIPSREPORT", "EXCLUDE_FROM_POS_USAGE")) : "";

            return sUnChkLst;
        }

        void ClearCustomAxisLabels()
        {
            ((XYDiagram2D)PositionUsageChart.Diagram).AxisY.CustomLabels.Clear();
        }

        private void PositionUsageChart_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ChartHitInfo hitInfo = PositionUsageChart.CalcHitInfo(e.GetPosition(PositionUsageChart));
            double flightCount = 0.0;
            string sFlightNo = "";
            string sPositionName = "";
            try
            {
                if (hitInfo != null && hitInfo.SeriesPoint != null)
                {
                    SeriesPoint point = hitInfo.SeriesPoint;

                    int index = Array.IndexOf(_argument, point.Argument);
                    if (index >= 0)
                    {
                        switch (point.Series.DisplayName)
                        {
                            case "Occupied Stands":
                                {

                                        flightCount = _flightPosition[index];
                                        sFlightNo = _flightNo[index];
                                        sPositionName = _PositionName[index];
                                    break;
                                }
                            default:
                                {
                                        flightCount = 0.0;
                                        sPositionName = _PositionNameUnChangeable[index];
                                    break;
                                }
                        }

                        //Positiontooltip_text.Text = string.Format("Series = {0}\nTotal No.of Parking Stands = {1} \nTotal No.of Flights = {2} \n List Of Flights = {3} \n Position Name = {4}",
                        //                                            point.Series.DisplayName, point.Value, flightCount,sFlightNo, sPositionName);

                        Positiontooltip_text.Text = string.Format("Series = {0}\nTotal No.of Parking Stands = {1} \nTotal No.of Flights = {2} \n List Of Flights = {3} \n\n List Of Stands = {4}",
                        point.Series.DisplayName, point.Value, flightCount, sFlightNo, sPositionName);
                        Positiontooltip.Placement = PlacementMode.Mouse;
                        Positiontooltip.IsOpen = true;
                        Cursor = Cursors.Hand;
                    }
                    else
                    {
                        Positiontooltip.IsOpen = false;
                        Cursor = Cursors.Arrow;
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
        }

        private void PositionUsageChart_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Positiontooltip.IsOpen = false;
        }

        private void cboTemplate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (String.Compare(cboTemplate.Text, "", false) != 0)
            {
                string cmbvalue = "";
                ComboBoxItem curItem = ((ComboBoxItem)cboTemplate.SelectedItem);
                totalHours = Convert.ToInt32(cmbvalue = curItem.Content.ToString().Substring(0, 2).Trim());

                if (totalHours == 24)
                {
                    txtFromTime.Text = "0000";
                    txtToTime.Text = "2359";
                }
                else
                    if (totalHours == 12)
                    {
                        txtFromTime.Text = "0000";
                        txtToTime.Text = "1159";
                    }
                    else
                        if (totalHours == 6)
                        {
                            txtFromTime.Text = "0000";
                            txtToTime.Text = "0559";
                        }

                objViewModel = new PositionUsageViewModel();
                objViewModel.TotalHour = totalHours;
                _flightPosition = new double[totalHours];
                _flightNo = new string[totalHours];
                _argument = new string[totalHours];
                _PositionName = new string[totalHours];
                _PositionNameUnChangeable = new string[totalHours];
            }
        }
        #endregion
    }
}
