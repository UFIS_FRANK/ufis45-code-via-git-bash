﻿using System;
using System.Linq;
using System.Windows;
using System.Threading;
using System.Windows.Controls;
using System.Collections.Generic;
using Ufis.Entities;
using Standard_Reports.Helpers;
using System.ComponentModel;
using Standard_Reports.DataAccess;
using System.Drawing.Printing;
using System.Windows.Media;




namespace Standard_Reports.Reports.PermitLetter
{
    public partial class PermitLetterView : UserControl
    {
        public HpParameters param;
        public HpReportLogo Logo;
        public HpFooter Footer;
        public PermitLetterViewModel objViewModel;
        public IList<EntDbFlight> FlightList;


        #region +++ Register Properties +++
        // Using a DependencyProperty as the backing store for MyInt.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyIntProperty =
        DependencyProperty.Register("MyInt", typeof(int), typeof(PermitLetterView), new UIPropertyMetadata(1));
        #endregion

        #region +++ Properties +++
        public int MyInt
        {
            get
            {
                return (int)GetValue(MyIntProperty);
            }
            set
            {
                SetValue(MyIntProperty, value);
            }
        }
        #endregion

        #region +++ WPF Application Control Events +++
        public PermitLetterView()
        {
            InitializeComponent();
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += Process;
            bw.RunWorkerCompleted += bgw_RunWorkerCompleted;
            bw.RunWorkerAsync();
        }

        private void Process(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(2000);
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                FetchData();
            }));
        }

        private void FetchData()
        {
            try
            {
                objViewModel = new PermitLetterViewModel();
                objViewModel.GetApprovalReport(AppArguments.Current.Urno);
                grdMain.DataContext = objViewModel.ItemSource;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //grdApprovalReport.Loaded = false;
        }

        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            // grdApprovalReport.ShowLoadingPanel = true;

            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += Process;
            bw.RunWorkerCompleted += bgw_RunWorkerCompleted;
            bw.RunWorkerAsync();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            bool ApprovalisDate = true;
            bool RequestisDate = true;
            bool reqbay = true;
            bool to = true;
            bool opr = true;
            bool purp = true;
            string errmsg = "Please enter the field(s) : \n";
            objViewModel = new PermitLetterViewModel();
            EntDbFlightPermit p = new EntDbFlightPermit();
            p.Urno = AppArguments.Current.Urno;
            p.Email = txtEmail.Text;
            p.Contact = txtContact.Text;
            if (txtApprovalDate.Text != "")
            {
                try
                {
                    string dateformat = "dd/MM/yyyy";
                    DateTime tmpapprovalDate =DateTime.ParseExact(txtApprovalDate.Text,dateformat,null);
                   p.ApprovalDate = tmpapprovalDate;
                }
                catch
                {
                    ApprovalisDate = false;
                    MessageBox.Show("Approval Date is not valid. Datetime format shoud be DD/MM/YYYY", "Invalid Approval Date");
                    return;
                }
            }
            else
            {
                ApprovalisDate = false;
                errmsg += "Approval Date \n";
            }
            if (txtRequestDate.Text != "")
            {
                try
                {
                    string dateformat = "dd/MM/yyyy";
                    DateTime tmpRequestDate = DateTime.ParseExact(txtRequestDate.Text,dateformat,null);
                    p.RequestDate = tmpRequestDate;
                }
                catch
                {
                    RequestisDate = false;
                    MessageBox.Show("Request Date is not valid. Datetime format shoud be DD/MM/YYYY", "Invalid Request Date",MessageBoxButton.OK,MessageBoxImage.Warning);
                    return;
                }
            }
            else
            {
                p.RequestDate = null;
            }
          
            if (txtHAgent.Text != "")
            {
                p.AgtName = txtHAgent.Text;
            }
            else
            {
                to = false;
                errmsg += "To \n";
            }
            if (txtOperatorNme.Text != "")
            {
                p.OperatorName = txtOperatorNme.Text;
            }
            else
            {
                opr = false;
                errmsg += "Operator Name \n";
            }
            if (txtPurpose.Text != "")
            {
                p.FlightPurpose = txtPurpose.Text;
            }
            else
            {
                purp = false;
                errmsg += "Purpose \n";
            }
            if (txtBay.Text != "")
            {
                p.RequestBay = txtBay.Text;
            }
            else
            {
                reqbay = false;
                errmsg += "Apron \n";
            }
           
            try
            {
                if (ApprovalisDate && RequestisDate && reqbay && to && opr && purp)
                {
                    DIPermitLetter.SavePermit(p);
                    MessageBox.Show("Save Successfully", "Permit Letter");
                    btnPrint.IsEnabled = true;
                }
                else
                {
                    MessageBox.Show(errmsg, "Information Required", MessageBoxButton.OK,MessageBoxImage.Warning);
                    btnPrint.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Permit Letter");
            }
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            //PrintDialog pd = new PrintDialog();

            //if (pd.ShowDialog() != true)
            //    return;
            //pd.PrintVisual(grdMain, "Printing Permit Letter");
            try
            {
                txtHAgent.Background = Brushes.White;
                txtApprovalDate.Background = Brushes.White;
                txtOperatorNme.Background = Brushes.White;
                txtPurpose.Background = Brushes.White;
                txtBay.Background = Brushes.White;
                System.Windows.Controls.PrintDialog Printdlg = new System.Windows.Controls.PrintDialog();
                Size pageSize = new Size(800,2000);
                if (Printdlg.ShowDialog() != true)
                {
                    txtHAgent.Background = Brushes.Yellow;
                    txtApprovalDate.Background = Brushes.Yellow;
                    txtOperatorNme.Background = Brushes.Yellow;
                    txtPurpose.Background = Brushes.Yellow;
                    txtBay.Background = Brushes.Yellow;
                    return;
                }
                              
                //sizing of the element.
                grdMain.Measure(pageSize);                
                grdMain.Arrange(new Rect(5, 5, pageSize.Width, pageSize.Height));
                Printdlg.PrintVisual(grdMain, "Printing Permit Letter");
                txtHAgent.Background = Brushes.Yellow;
                txtApprovalDate.Background = Brushes.Yellow;
                txtOperatorNme.Background = Brushes.Yellow;
                txtPurpose.Background = Brushes.Yellow;
                txtBay.Background = Brushes.Yellow;
                //PrintDialog pd = new PrintDialog();

                //if (pd.ShowDialog() != true)
                //    return;
                // pd.PrintVisual(grdMain, "Printing Permit Letter");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

        private void txtApprovalDate_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            btnPrint.IsEnabled = false;
        }

        private void txtHAgent_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            btnPrint.IsEnabled = false;
        }

        private void txtEmail_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            btnPrint.IsEnabled = false;
        }

        private void txtContact_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            btnPrint.IsEnabled = false;
        }

        private void txtRequestDate_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            btnPrint.IsEnabled = false;
        }

        private void txtOperatorNme_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            btnPrint.IsEnabled = false;
        }

        private void txtPurpose_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            btnPrint.IsEnabled = false;
        }

        private void txtBay_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            btnPrint.IsEnabled = false;
        }
    }
}

#endregion
