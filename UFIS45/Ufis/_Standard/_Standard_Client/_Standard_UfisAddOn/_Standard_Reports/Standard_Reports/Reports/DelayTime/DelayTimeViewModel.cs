﻿using System;
using System.Linq;
using System.Collections.Generic;

using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using Standard_Reports.DataAccess;


namespace Standard_Reports.Reports.DelayTime
{
    public class DelayTimeViewModel : WorkspaceViewModel
    {
        public WorkspaceViewModel _workspace;
        EntityCollectionBase<EntDbFlight> objDepFlights;

        public IList<EntDbFlight> GetFlights(DateTime dtFrom, DateTime dtTo, string sAirLine)
        {
            objDepFlights = DIDelayTime.LoadDepartureFlightListByDateRange(dtFrom, dtTo, sAirLine);
            return objDepFlights;
        }
    }
}
