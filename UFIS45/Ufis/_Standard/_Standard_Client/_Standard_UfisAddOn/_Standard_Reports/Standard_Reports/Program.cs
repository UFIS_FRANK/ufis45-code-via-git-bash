﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Ufis.MVVM.ViewModel;
using Ufis.Data;
using Ufis.LoginWindow;
using Ufis.NotificationWindow;
using Ufis.Utilities;
using Standard_Reports.DataAccess;
using Standard_Reports.Helpers;
using Standard_Reports.Shell;
using Ufis.Security;
using System.Windows.Media.Imaging;
using Standard_Reports.Reports.ApprovalReport;
using Standard_Reports.Reports.DelayTime;
using Standard_Reports.Reports.PositionUsage;
using Standard_Reports.Reports.BaggageBeltAllocation;
using Standard_Reports.Reports.AirCraftsOnGround;
using Standard_Reports.Reports.PermitLetter;
using Standard_Reports.Reports.Towing;
using Standard_Reports.Reports.GateUsage;

namespace Standard_Reports
{
    class App : Application
    {
        public App(string[] args)
        {
            InitializeComponent(args);
        }

        public event EventHandler BeforeMainWindowClosed;

        private void InitializeComponent(string[] args)
        {
            //Set current application infomation
            AppInfo appInfo = HpAppInfo.Current;
            appInfo.ProductCode = "FIPSReport";
            appInfo.ProductTitle = "Standard Reports";
            appInfo.Description = "";
            appInfo.Image = new BitmapImage(new Uri("../Images/BDPSUIF_128x128.png", UriKind.Relative));
            //Registration string to register at function list
            //If New Report has been increase, please see the next line and follow that syntex
            //SUBDirectory, FunctionCode, FunctionName, Type (B=Button, I=Menu("So far I only know it")) Status
            //appInfo.RegistrationString = "FIPSReport,InitModu,InitModu,Initialisieren (InitModu),B,-" +
            //    ",Report Menus,MBBALL,BaggageBelt Allocation,I,1";

            //Create shell window
            ShellView shell = new ShellView();
            this.MainWindow = shell;
            shell.BeforeClosed += OnMainWindowBeforeClosed;

            // Create the ViewModel to which 
            // the shell window binds.
            var viewModel = new ShellViewModel();

            // When the ViewModel asks to be closed, 
            // close the window.
            EventHandler handler = null;
            handler = (sender, e) =>
            {
                viewModel.RequestClose -= handler;
                shell.Close();
            };
            viewModel.RequestClose += handler;

            //check the prequisite requirements, if all ok
            //shows the login dialog
            string connectionError = CheckConnection();
            WorkspaceViewModel workspace = null;

            if (string.IsNullOrEmpty(connectionError))
            {
                IUfisUserAuthentication userAuthentication = DlUfisData.Current.UserAuthenticationManager;
                //check the arguments
                if (args.Length > 0)
                {
                    /*
                     * Argument Sequence. Note it down and follow the sequence
                     * *******************************************************
                     * 1. Report Type
                     * 2. UserID
                     * 3. Password
                     * 5. URNO (Use by Permit Letter)
                     * 6. Scenario No (For Multi Airport)
                     */
                    string[] arrSplitedArgs = args[0].Split('|');
                    string strFormId = arrSplitedArgs[0].Split(',')[0];
                    string userId = arrSplitedArgs[1].Split(',')[0];
                    string sLocalUTC = arrSplitedArgs[2].Split(',')[0];
                    string sSco = string.Empty ;
                    try
                    { 
                        sSco = arrSplitedArgs[4].Split(',')[0]; 
                    }
                    catch
                    {
                        //If hit exception, will be continue next
                    }
                    

                    if (String.Compare(sSco, "<None>", false) == 0 || !string.IsNullOrEmpty(sSco))
                    {
                        EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
                        Helpers.HpUser.Sco = sSco;
                        dataContext.Connection.TableExtension = sSco;
                    }
                    int sUrno = 0;
                    try
                    {
                        if (arrSplitedArgs[3].Length > 0)
                            sUrno = Convert.ToInt32(arrSplitedArgs[3].Split(',')[0]);
                    }
                    catch
                    {
                        //If hit exception, will be continue next
                    }
                    switch (strFormId)
                    {
                        case "PTUR":
                            viewModel.DisplayName = "Position Usage Report";
                            appInfo.Description = "";
                            //Registration string to register at function list
                            //If New Report has been increase, please see the next line and follow that syntex
                            //SUBDirectory, FunctionCode, FunctionName, Type (B=Button, I=Menu("So far I only know it")) Status
                            appInfo.RegistrationString = "FIPSReport,InitModu,InitModu,Initialisieren (InitModu),B,-" +
                            ",Report Menus,MPTUR,Position Usage Report,I,1";
                            HpUser.ActiveUser = new Ufis.Entities.EntUser() { UserId = userId };
                            HpUser.DirectAccess = true;
                            HpUser.TLocalUTC = sLocalUTC;                            
                            DlUfisData.Current.DataContext.Connection.UserName = userId;

                            workspace = new PositionUsageViewModel();
                            break;

                        case "APVR":
                            viewModel.DisplayName = "Flight Permit - Non-Scheduled Aircraft Landing Permission";
                            appInfo.RegistrationString = "FIPSReport,InitModu,InitModu,Initialisieren (InitModu),B,-" +
                            ",Report Menus,MAPVR,Flight Permit Approval Report,I,1";

                            HpUser.ActiveUser = new Ufis.Entities.EntUser() { UserId = userId };
                            HpUser.DirectAccess = true;
                            HpUser.TLocalUTC = sLocalUTC;                            
                            DlUfisData.Current.DataContext.Connection.UserName = userId;

                            workspace = new ApprovalReportViewModel();
                            break;
                        case "ACON":
                            viewModel.DisplayName = "Flight Permit - Non-Scheduled Aircraft On Ground Report";
                            appInfo.RegistrationString = "FIPSReport,InitModu,InitModu,Initialisieren (InitModu),B,-" +
                            ",Report Menus,MACON,Non-Scheduled Aircrafts On Ground Report,I,1";

                            HpUser.ActiveUser = new Ufis.Entities.EntUser() { UserId = userId };
                            HpUser.DirectAccess = true;
                            HpUser.TLocalUTC = sLocalUTC;                            
                            DlUfisData.Current.DataContext.Connection.UserName = userId;

                            workspace = new AirCraftsOnGroundViewModel();
                            break;

                        case "TOWR":
                            viewModel.DisplayName = "Towing Report";
                            appInfo.RegistrationString = "FIPSReport,InitModu,InitModu,Initialisieren (InitModu),B,-" +
                            ",Report Menus,MTOWR,Towing Report,I,1";

                            HpUser.ActiveUser = new Ufis.Entities.EntUser() { UserId = userId };
                            HpUser.DirectAccess = true;
                            HpUser.TLocalUTC = sLocalUTC;                            
                            DlUfisData.Current.DataContext.Connection.UserName = userId;

                            workspace = new TowingReportViewModel();
                            break;

                        case "DYTR":
                            viewModel.DisplayName = "Delay Time Report";
                            appInfo.RegistrationString = "FIPSReport,InitModu,InitModu,Initialisieren (InitModu),B,-" +
                            ",Report Menus,MDYTR,Delay Time Report,I,1";

                            HpUser.ActiveUser = new Ufis.Entities.EntUser() { UserId = userId };
                            HpUser.DirectAccess = true;
                            HpUser.TLocalUTC = sLocalUTC;                            
                            DlUfisData.Current.DataContext.Connection.UserName = userId;

                            workspace = new DelayTimeViewModel();
                            break;
                        case "GTUR":
                            viewModel.DisplayName = "Gate Usage Report";
                            appInfo.RegistrationString = "FIPSReport,InitModu,InitModu,Initialisieren (InitModu),B,-" +
                            ",Report Menus,MGTUR,Gate Usage Report,I,1";

                            HpUser.ActiveUser = new Ufis.Entities.EntUser() { UserId = userId };
                            HpUser.DirectAccess = true;
                            HpUser.TLocalUTC = sLocalUTC;                            
                            DlUfisData.Current.DataContext.Connection.UserName = userId;

                            workspace = new GateUsageViewModel();
                            break;
                        case "PERN":
                            viewModel.DisplayName = "Permit Letter";
                            appInfo.RegistrationString = "FIPSReport,InitModu,InitModu,Initialisieren (InitModu),B,-" +
                            ",Report Menus,MPERN,PermitLetter,I,1";

                            HpUser.ActiveUser = new Ufis.Entities.EntUser() { UserId = userId };
                            HpUser.DirectAccess = true;
                            HpUser.TLocalUTC = sLocalUTC;
                            Standard_Reports.DataAccess.AppArguments.Current.Urno = sUrno;
                            DlUfisData.Current.DataContext.Connection.UserName = userId;
                            workspace = new PermitLetterViewModel();
                            break;
                        case "BBAR":
                            viewModel.DisplayName = "Baggage Belt Allocation Report";
                            appInfo.Description = "";
                            //Registration string to register at function list
                            //If New Report has been increase, please see the next line and follow that syntex
                            //SUBDirectory, FunctionCode, FunctionName, Type (B=Button, I=Menu("So far I only know it")) Status
                            appInfo.RegistrationString = "FIPSReport,InitModu,InitModu,Initialisieren (InitModu),B,-" +
                            ",Report Menus,MBBAR,Baggage Belt Allocation,I,1";
                            HpUser.ActiveUser = new Ufis.Entities.EntUser() { UserId = userId };
                            HpUser.DirectAccess = true;
                            HpUser.TLocalUTC = sLocalUTC;                            
                            DlUfisData.Current.DataContext.Connection.UserName = userId;

                            workspace = new BaggageBeltAllocationViewModel();
                            break;
                        


                        default:
                            workspace = new NotificationViewModel() { NotificationInfo = new MessageInfo() { InfoText = string.Format("Invalid comand found: {0}", strFormId),
                            Severity = MessageInfo.MessageInfoSeverity.Error },
                            CanContinue = false };
                            break;
                    }
                }

                else
                {
                    LoginViewModel loginViewModel;
#if !XABPP
                    loginViewModel = new LoginViewModel(appInfo, userAuthentication, LoginViewModel.GetLastUser()) { AutoSaveLastUser = true };
#else
                loginViewModel = new LoginViewModel(appInfo, userAuthentication, false);
#endif
                    loginViewModel.ProjectInfo = DlUfisData.Current.ProjectInfo;
                    workspace = loginViewModel;
                }
            }
            //otherwise shows ErrorScreen
            else
            {
                workspace = new NotificationViewModel() { NotificationInfo = new MessageInfo() { InfoText = connectionError,
                Severity = MessageInfo.MessageInfoSeverity.Error },
                CanContinue = false };
            }
            viewModel.Workspace = workspace;
            //Check if LoginviewModel and Login Success cause of SSO
            //Skip the login windows and continue to Application.
            //Phyoe Khaing Min
            //24.Apr.2012
            if ((workspace is LoginViewModel) && ((LoginViewModel)workspace).LoginSucceeded)
            {
                workspace.CloseCommand.Execute(null);
            }
            // Allow all controls in the window to 
            // bind to the ViewModel by setting the 
            // DataContext, which propagates down 
            // the element tree.
            shell.DataContext = viewModel;

            shell.Show();
        }

        private string CheckConnection()
        {
            const string CONN_ERROR = "We could not connect to the application server(s).";
            const string CONN_ERROR_UNABLE_TO_CONNECT = "Could not connect to following server(s):";
            const string CONN_ERROR_NO_CONFIG = "Please make sure that you have the Ceda.ini configuration file in your system.";

            string strConnErr = null;

            string strAppName = HpAppInfo.Current.ProductCode;
            EntityDataContextBase dataContext = DlUfisData.CreateInstance(strAppName).DataContext;
            if (dataContext == null)
            {
                strConnErr = String.Format("{0}\n{1}", CONN_ERROR, CONN_ERROR_NO_CONFIG);
            }
            else
            {
                ConnectionBase connection = dataContext.Connection;
                if (connection == null)
                    strConnErr = String.Format("{0}\n{1}\n{2}", CONN_ERROR, CONN_ERROR_UNABLE_TO_CONNECT, connection.ServerList);
            }

            return strConnErr;
        }

        void OnMainWindowBeforeClosed(object sender, EventArgs e)
        {
            if (BeforeMainWindowClosed != null)
                BeforeMainWindowClosed(this, EventArgs.Empty);
        }
    }

    static class Program
    {
        static App _currentApp;

        [STAThread]
        static void Main(string[] args)
        {
            App app = new App(args);
            _currentApp = app;
            app.BeforeMainWindowClosed += OnBeforeMainWindowClosed;
            app.DispatcherUnhandledException += OnDispatcherUnhandledException;
            app.Run();
        }

        static void OnBeforeMainWindowClosed(object sender, EventArgs e)
        {
            DlUfisData dlUfisData = DlUfisData.Current;
            if (dlUfisData != null)
            {
                EntityDataContextBase dataContext = dlUfisData.DataContext;
                if (dataContext != null)
                {
                    ConnectionBase connection = dataContext.Connection;
                    if (connection != null)
                    {
                        if (connection.State == ConnectionState.Open)
                            connection.Close();
                        connection = null;
                    }
                    dataContext.Dispose();
                }
            }
        }

        static void OnDispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            string strException = "Application is going to close.\nException occured:\n";
            Exception CurrentException = e.Exception;
            while (CurrentException != null)
            {
                strException += CurrentException.Message + "\n";
                CurrentException = CurrentException.InnerException;
            }

            App app = _currentApp;
            ShellView shell = (ShellView)app.MainWindow;
            ShellViewModel viewModel = (ShellViewModel)shell.DataContext;
            WorkspaceViewModel workspace = new NotificationViewModel() { NotificationInfo = new MessageInfo() { InfoText = strException,
            Severity = MessageInfo.MessageInfoSeverity.Error },
            CanContinue = false };
            viewModel.Workspace = workspace;

            e.Handled = true;
        }
    }
}
