﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Data;
using Ufis.Entities;
using Standard_Reports.Helpers;
using Ufis.Data.Ceda;

namespace Standard_Reports.DataAccess
{
    public class DIAirCraftsOnGround
    {
        public static EntityCollectionBase<EntDbFlightPermit> LoadFlightPermit(DateTime dtFrom, DateTime dtTo)
        {
            string WHERE_CLAUSE = "";
            const string FIELD_LIST = "[ApprovalDate],[ValidFrom],[ValidTo],[ArrivalDepartureId],[OperatorName],[RegistrationNumber],[AircraftICAOCode],[CallSign],[FlightPermitNumber],[RequestBay]";

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(dtFrom, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(dtTo, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            WHERE_CLAUSE = String.Format("WHERE [ApprovalDate] BETWEEN '{0}' AND '{1}' AND [ArrivalDepartureId] = 'A'", sFromDate, sToDate);
            EntityCollectionBase<EntDbFlightPermit> objFlightPermitList = null;
            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntDbFlightPermit),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };
            objFlightPermitList = dataContext.OpenEntityCollection<EntDbFlightPermit>(command);

            return objFlightPermitList;
        }

        public static EntDbFlight GetFlight(string scsgn, string sregn, string sadid, DateTime svafr, DateTime svato)
        {
            const string FIELD_LIST = "[AircraftICAOCode],[StandardTimeOfArrival],[StandardTimeOfDeparture],[OperationalType],[LandingTime],[ArrivalDepartureId],[ADHO],[PositionOfArrival],[LandingTime]";
            string WHERE_CLAUSE = "";
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(svafr, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(svato, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            if (sregn != "")
                WHERE_CLAUSE = String.Format("WHERE [CallSign] = '{0}' AND [RegistrationNumber] = '{1}' AND [ArrivalDepartureId] = 'A' AND [StandardTimeOfArrival] <= '{3}' AND [StandardTimeOfArrival] >= '{4}' AND [LandingTime] <> '              '", scsgn, sregn, sadid, sFromDate, sToDate);
            else
                if (sregn == "")
                    WHERE_CLAUSE = String.Format("WHERE [CallSign] = '{0}' AND [RegistrationNumber] = '    ' AND [ArrivalDepartureId] = 'A' AND [StandardTimeOfArrival] <= '{3}' AND [StandardTimeOfArrival] >= '{4}' AND [LandingTime] <> '              '", scsgn, sregn, sadid, sFromDate, sToDate);
                //else
                //    if (sadid == "D" && sregn != "")
                //        WHERE_CLAUSE = String.Format("WHERE [CallSign] = '{0}' AND [RegistrationNumber] = '    ' AND [StandardTimeOfDeparture] <= '{3}' AND [StandardTimeOfDeparture] >= '{4}'", scsgn, sregn, sadid, sFromDate, sToDate);
                //    else
                //        if (sadid == "D" && sregn != "")
                //            WHERE_CLAUSE = String.Format("WHERE [CallSign] = '{0}' AND [RegistrationNumber] = '{1}' AND [StandardTimeOfDeparture] <= '{3}' AND [StandardTimeOfDeparture] >= '{4}'", scsgn, sregn, sadid, sFromDate, sToDate);
            EntityCollectionBase<EntDbFlight> objFlightList = null;
            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntDbFlight),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };

            objFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);
            EntDbFlight flight = objFlightList.FirstOrDefault();
            return flight;
        }
    }
}
