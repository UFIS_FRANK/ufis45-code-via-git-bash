﻿using System;
using Ufis.Data;
using System.Linq;
using Ufis.Entities;
using Ufis.Data.Ceda;
using System.Collections.Generic;
using Standard_Reports.Helpers;
using Standard_Reports.Entities;
using System.Text.RegularExpressions;

namespace Standard_Reports.DataAccess
{
    public class DlBaggageBeltAllocation
    {
        /// <summary>
        /// Load Flight List by date Range for Arrival
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <param name="sFilter">System.String containing sFilter</param>
        /// <returns>EntityCollection for Flight</returns>
        public static EntityCollectionBase<EntFlight> LoadArrivalFlightListByDateRange(DateTime sFDate, DateTime sTdate, string sFilter)
        {
            const string FIELD_LIST = "[Urno],[FullFlightNumber],[OriginAirportIATACode],[CallSign],[StandardTimeOfArrival],[PositionOfArrival],[Belt1],[TotalNumberOfBaggages],[NumberOfPassengers2],[TerminalBaggageBelt1],[Belt1],[FirstBag],[LastBag],[OnchockTime],[OnblockTime],[ScheduledFirstBag],[AircraftIATACode],[ArrivalDepartureId]";

            string WHERE_CLAUSE = string.Empty;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

           

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(sFDate, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(sTdate, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);

            WHERE_CLAUSE = String.Format("WHERE [OperationalType] IN ('O','S','B','Z')  AND [ArrivalDepartureId] = 'A' AND ([StandardTimeOfArrival] BETWEEN '{0}' AND '{1}' OR [BestEstimatedArrivalTime] BETWEEN '{0}' AND '{1}') AND [NatureCode] IN ('{2}') ORDER BY [StandardTimeOfArrival],[BestEstimatedArrivalTime]", sFromDate, sToDate, sFilter);

            EntityCollectionBase<EntFlight> objFlightList = null;

            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntFlight),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };
            objFlightList = dataContext.OpenEntityCollection<EntFlight>(command);

            return objFlightList;
        }

        public static EntFML LoadDelayReasonFromFML(int urno)
        {
            const string FIELD_LIST = "[FLightURNo],[TYPE],[TEXT],[TMTP]";

            string WHERE_CLAUSE = string.Empty;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

           

            WHERE_CLAUSE = String.Format("WHERE FLNU = '{0}' AND TYPE='CC' AND TMTP='BELT'", urno);

            EntityCollectionBase<EntFML> objDelayReasonList = null;
            EntFML objDelayReason = null;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntFML),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };
            objDelayReasonList = dataContext.OpenEntityCollection<EntFML>(command);

            objDelayReason = objDelayReasonList.FirstOrDefault();
            return objDelayReason;
        }

        public static EntLoaTab LoadPaxOffFromLOATAB(int urno)
        {
            const string FIELD_LIST = "[TYPE],[STYP],[SSTP],[SSST],[DSSN],[FLightURNo],[VALU]";

            string WHERE_CLAUSE = string.Empty;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

           

            WHERE_CLAUSE = String.Format("WHERE TYPE='DIS' AND STYP='T' AND SSTP='          ' AND SSST='CAL' AND DSSN='SYS' AND APC3='   ' AND FLNU = '{0}'", urno);
            EntityCollectionBase<EntLoaTab> objLoaTabList = null;
            EntLoaTab objLoaTab = null;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntLoaTab),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };
            objLoaTabList = dataContext.OpenEntityCollection<EntLoaTab>(command);

            objLoaTab = objLoaTabList.FirstOrDefault();
            return objLoaTab;
        }

        public static EntLoaTab LoadBagOffFromLOATAB(int urno,string sadid)
        {
            const string FIELD_LIST = "[TYPE],[STYP],[SSTP],[SSST],[DSSN],[FLightURNo],[VALU]";

            string WHERE_CLAUSE = string.Empty;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

           

            if (sadid == "A")
                WHERE_CLAUSE = String.Format("WHERE TYPE='BAG' AND STYP='LOC' AND SSTP='IFA' AND SSST='CAL' AND DSSN='SYS' AND APC3='   ' AND  FLNU = '{0}'", urno);
            else if (sadid == "D")
                WHERE_CLAUSE = String.Format("WHERE TYPE='BAG' AND STYP='LOC' AND SSTP='IFD' AND SSST='CAL' AND DSSN='SYS' AND AND APC3='   '  FLNU = '{0}'", urno);
            EntityCollectionBase<EntLoaTab> objLoaTabList = null;
            EntLoaTab objLoaTab = null;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntLoaTab),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };
            objLoaTabList = dataContext.OpenEntityCollection<EntLoaTab>(command);

            objLoaTab = objLoaTabList.FirstOrDefault();
            return objLoaTab;
        }

        public static IList<EntHValue> LoadHValue(IList<string> urnoList)
        {
            EntSystem objSys;
            objSys = DlBaggageBeltAllocation.LoadSys();
            const string FIELD_LIST = "[UrnoOfFlight],[FieldName],[TableName],[OriginalValue],[SecondValue]";
            string WHERE_CLAUSE = "";
            string parameter = null;
            for (int i = 0; i < urnoList.Count; i++)
            {
                if (i == 0)
                    parameter = urnoList[i];
                else
                    parameter += "," + urnoList[i];
            }
            if (objSys.LoggedData != null)
                WHERE_CLAUSE = String.Format(objSys.LoggedData + " WHERE [UrnoOfFlight] IN ({0}) AND FINA = 'BLT1' AND TANA = 'AFT' ORDER BY 'TIME'", parameter);
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

           

            EntityCollectionBase<EntHValue> objHValueList = null;
            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntHValue),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };

            objHValueList = dataContext.OpenEntityCollection<EntHValue>(command);

            return objHValueList;
        }


        public static EntSystem LoadSys()
        {
            const string FIELD_LIST = "[LoggedData],[FieldName],[TableName]";

            string WHERE_CLAUSE = String.Format("WHERE [FieldName] = 'BLT1' AND [TableName] = 'AFT'");

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
           
            IList<EntSystem> objSysList = null;
            EntSystem objSys = null;

            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntSystem),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };
            objSysList = dataContext.OpenEntityCollection<EntSystem>(command);
            objSys = objSysList.FirstOrDefault();
            return objSys;
        }
        /// <summary>
        /// Load Nature List 
        /// </summary>
        /// <param name=""></param>
        /// <returns>EntityCollection for Nature  List</returns>
        public static EntityCollectionBase<EntFlightNature> LoadNatureList()
        {
            const string FIELD_LIST = "[Code],[Name]";
            string WHERE_CLAUSE = "ORDER BY [Code]";


            EntityCollectionBase<EntFlightNature> objNatureList = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
           
            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntFlightNature),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };

            objNatureList = dataContext.OpenEntityCollection<EntFlightNature>(command);

            return objNatureList;
        }


        private static IList<entlcNature> _natureList;
        private static IList<EntFlightNature> objNatureList;
        public static IList<entlcNature> LoadNature()
        {
            if (null == _natureList)
            {
                _natureList = new List<entlcNature>();

                //Get Airline  List 
                objNatureList = LoadNatureList();

                foreach (EntFlightNature item in objNatureList)
                {
                    _natureList.Add(new entlcNature { Name = item.Name, Code = item.Code });
                }
            }

            return _natureList;
        }


        //Zaw Min Tun
        //2012-12-18
        //Getting the delay reason from CRCTAB + CRATAB
        #region
        public static String GetDelayReasons(string strFlUrno, bool bOnlyLatestInfo, bool bCode)
        {
            String strAns = "", strTmp = "";
            const string FIELD_LIST = "[DelayReasonURNO],[CRADateTime]";
            string WHERE_CLAUSE = String.Concat("WHERE FURN ='", strFlUrno, "' ORDER BY [CRADateTime] DESC");

            EntityCollectionBase<EntCraTab> colCras = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
           
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntCraTab),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            colCras = dataContext.OpenEntityCollection<EntCraTab>(command);

            if (colCras != null && colCras.Count > 0)
            {
                foreach (EntCraTab tmpCra in colCras)
                {
                    strTmp = LoadCRC(tmpCra.DelayReasonURNO,bCode);
                    if (!String.IsNullOrEmpty(strTmp))
                    {
                        if (!String.IsNullOrEmpty(strAns))
                            strAns = String.Concat(strAns, "/", strTmp);
                        else
                        {
                            strAns = strTmp;
                            if (bOnlyLatestInfo)
                                break;
                        }
                    }
                }
            }

            return strAns;
        }

        private static String LoadCRC(string strDealyReasonUrno, bool bCode)
        {
            string strAns = "";
            const string FIELD_LIST = "[DelayCode],[Remark]";
            string WHERE_CLAUSE = String.Concat("WHERE URNO = '", strDealyReasonUrno, "'");

            EntityCollectionBase<EntChangeReasonCodeTab> colCrcs = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
           
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntChangeReasonCodeTab),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            colCrcs = dataContext.OpenEntityCollection<EntChangeReasonCodeTab>(command);

            if (colCrcs != null && colCrcs.Count > 0)
            {
                foreach (EntChangeReasonCodeTab tmpCrc in colCrcs)
                {
                    if (bCode)
                        strAns = tmpCrc.DelayCode;
                    else
                        strAns = tmpCrc.Remark;

                    //Result will be only one, so even if more than one just take the first one and break
                    break;
                }
            }

            return strAns;
        }
        #endregion
    }
}
