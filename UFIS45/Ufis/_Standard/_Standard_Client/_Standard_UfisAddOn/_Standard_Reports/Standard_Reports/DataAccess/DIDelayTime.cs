﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Data;
using Ufis.Entities;
using Standard_Reports.Helpers;
using Ufis.Data.Ceda;

namespace Standard_Reports.DataAccess
{
    public class DIDelayTime
    {
        public static EntityCollectionBase<EntDbFlight> LoadDepartureFlightListByDateRange(DateTime dtFrom, DateTime dtTo, string sAirLine)
        {
            string WHERE_CLAUSE = "";
            const string FIELD_LIST = "[AirlineICAOCode],[AirlineIATACode],[StandardTimeOfDeparture],[OffblockTime],[ArrivalDepartureId],[OperationalType]";

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(dtFrom, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(dtTo, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            sAirLine = sAirLine.ToUpper();
            if (sAirLine != "")
                WHERE_CLAUSE = String.Format("WHERE [ArrivalDepartureId] = 'D' AND [OffblockTime] != ' ' AND [OperationalType] = 'O' AND [StandardTimeOfDeparture] BETWEEN '{0}' AND '{1}'  AND ([AirlineICAOCode] = '{2}' OR [AirlineIATACode] = '{2}')", sFromDate, sToDate, sAirLine);
            else
                WHERE_CLAUSE = String.Format("WHERE [ArrivalDepartureId] = 'D' AND [OffblockTime] != ' ' AND [OperationalType] = 'O' AND [StandardTimeOfDeparture] BETWEEN '{0}' AND '{1}'", sFromDate, sToDate, sAirLine);
            EntityCollectionBase<EntDbFlight> objDepartureFlightList = null;

            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntDbFlight),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };
            objDepartureFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);

            return objDepartureFlightList;
        }
    }
}
