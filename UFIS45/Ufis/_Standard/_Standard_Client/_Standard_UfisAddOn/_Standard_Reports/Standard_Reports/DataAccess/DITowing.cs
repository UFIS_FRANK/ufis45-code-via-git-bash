﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;
using Ufis.Data;
using Ufis.Data.Ceda;
using Standard_Reports.Helpers;
using Ufis.IO;


namespace Standard_Reports.DataAccess
{
    public class DITowing
    {
        
        public static EntityCollectionBase<EntDbFlight> GetTowingFlight(DateTime dtFrom, DateTime dtTo)
        {
            string WHERE_CLAUSE = "";
            const string FIELD_LIST = "[Urno],[ReadyForTowing],[FullFlightNumber],[RegistrationNumber],[AircraftIATACode],[PositionOfDeparture],[PositionOfArrival],[StandardTimeOfDeparture],[StandardTimeOfArrival],[OffblockTime],[OnblockTime],[RelationKey],[ArrivalDepartureId],[CallSign]";

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(dtFrom, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(dtTo, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            WHERE_CLAUSE = String.Format("WHERE [StandardTimeOfArrival] BETWEEN '{0}' AND '{1}' AND [StandardTimeOfDeparture] BETWEEN '{0}' AND '{1}' AND FTYP IN ('T','G')", sFromDate, sToDate);
            EntityCollectionBase<EntDbFlight> objFlightList = null;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };
            objFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);

            return objFlightList;
        }

        public static EntityCollectionBase<EntDbFlight> GetArrivalFlight(IList<string> srkeylist)
        {

            const string FIELD_LIST = "[Urno],[ReadyForTowing],[FullFlightNumber],[RegistrationNumber],[AircraftIATACode],[PositionOfDeparture],[PositionOfArrival],[StandardTimeOfDeparture],[StandardTimeOfArrival],[OffblockTime],[OnblockTime],[RelationKey],[ArrivalDepartureId],[CallSign]";
            string WHERE_CLAUSE = "";
            string parameter = null;
            for (int i = 0; i < srkeylist.Count; i++)
            {
                if (i == 0)
                    parameter = "([RelationKey] IN " + srkeylist[i] + " AND [ArrivalDepartureId] = 'A')";
                else
                    parameter += " OR ([RelationKey] IN " + srkeylist[i] + " AND [ArrivalDepartureId] = 'A')";
            }

            WHERE_CLAUSE = String.Format("WHERE {0}", parameter);
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            EntityCollectionBase<EntDbFlight> objFlightList = null;

            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);

            return objFlightList;
        }

        public static EntityCollectionBase<EntDbFlight> GetDepartureFlight(IList<string> srkeylist)
        {

            const string FIELD_LIST = "[Urno],[ReadyForTowing],[FullFlightNumber],[RegistrationNumber],[AircraftIATACode],[PositionOfDeparture],[PositionOfArrival],[StandardTimeOfDeparture],[StandardTimeOfArrival],[OffblockTime],[OnblockTime],[RelationKey],[ArrivalDepartureId],[CallSign]";
            string WHERE_CLAUSE = "";
            string parameter = null;
            for (int i = 0; i < srkeylist.Count; i++)
            {
                if (i == 0)
                    parameter = "([RelationKey] IN " + srkeylist[i] + " AND [ArrivalDepartureId] = 'D')";
                else
                    parameter += " OR ([RelationKey] IN " + srkeylist[i] + " AND [ArrivalDepartureId] = 'D')";
            }

            WHERE_CLAUSE = String.Format("WHERE {0}", parameter);
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            EntityCollectionBase<EntDbFlight> objFlightList = null;

            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);
          
            return objFlightList;
        }

        public static EntityCollectionBase<EntDbConfiguration> GetConfiguration(string appName, string secName, string param)
        {
            string strCedaPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.Ceda);
            IniFile myIni = new IniFile(strCedaPath);
            string strHopo =  myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
            const string FIELD_LIST = "[ApplicationName],[SectionName],[ParameterName],[ParameterValue],[HomeAirport]";
            string WHERE_CLAUSE = String.Format("WHERE [ApplicationName] = '{0}' and [SectionName] = '{1}' and [ParameterName] = '{2}' and [HomeAirport]='{3}'", appName, secName, param,strHopo);
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            EntityCollectionBase<EntDbConfiguration> objConfigList = null;
           
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbConfiguration),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objConfigList = dataContext.OpenEntityCollection<EntDbConfiguration>(command);

            return objConfigList;
        }
    }
}
