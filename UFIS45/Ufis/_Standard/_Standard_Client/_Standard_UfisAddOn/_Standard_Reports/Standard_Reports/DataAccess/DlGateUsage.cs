﻿#region +++ Source Code Information +++
/*
 *      File Name       :   DLGateUsage.cs
 *      Description     :   Retrieve data from Gate Usage report
 *      
 *      Created By      :   Siva
 *      Created On      :
 *      
 *      Updated By      :   Phyoe Khaing Min
 *      Updated On      :   6-Sep-2012
 *      Update Detail   :   Exclude Cancelled, No-op, Requested, Diverted, Rerouted flights while the app retrieve the flights.
 * 
 */
#endregion

using System;
using Ufis.Data;
using System.Linq;
using Ufis.Entities;
using Ufis.Data.Ceda;
using System.Collections.Generic;
using Standard_Reports.Helpers;
using Standard_Reports.Entities;

namespace Standard_Reports.DataAccess
{
    public class DlGateUsage
    {
        /// <summary>
        /// Load Schedule or Actual Flight List by date Range for Arrival
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <returns>EntityCollection for Arrival Flight</returns>
        public static EntityCollectionBase<EntFlight> LoadArrivalFlightListByDateRange(DateTime dtFrom, DateTime dtTo, string sType)
        {
            string WHERE_CLAUSE = "";
            const string FIELD_LIST = "[GateOfArrival1],[GateOfArrival2],[ArrivalScheduleBeginForGate1],[ArrivalScheduleEndForGate1],[ArrivalScheduleBeginForGate2],[ArrivalScheduleEndForGate2]," +
            "[ArrivalActualBeginForGate1],[ArrivalActualEndForGate1],[ArrivalActualBeginForGate2],[ArrivalActualEndForGate2],[FullFlightNumber]";

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();
           
            string sFromDate = objCedaDataConverter.DataItemToCedaItem(dtFrom, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(dtTo, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);

            switch (sType)
            {
                case "PLAN":
                    WHERE_CLAUSE = String.Format("WHERE ([ArrivalDepartureId] = 'A' OR [ArrivalDepartureId] = 'B') AND [GateOfArrival1] <> ' ' AND ([ArrivalScheduleBeginForGate1] BETWEEN '{0}' AND '{1}' OR [ArrivalScheduleEndForGate1] BETWEEN '{0}' AND '{1}'  OR '{0}' BETWEEN [ArrivalScheduleBeginForGate1] AND [ArrivalScheduleEndForGate1]  OR '{1}' BETWEEN [ArrivalScheduleBeginForGate1] AND [ArrivalScheduleEndForGate1]) AND FTYP IN ('O','Z','B','S','T','G') ORDER BY [ArrivalScheduleBeginForGate1],[ArrivalScheduleEndForGate1]", sFromDate, sToDate);
                    break;
                case "ACT":
                    WHERE_CLAUSE = String.Format("WHERE ([ArrivalDepartureId] = 'A' OR [ArrivalDepartureId] = 'B') AND [GateOfArrival1] <> ' ' AND  ([ArrivalActualBeginForGate1] BETWEEN '{0}' AND '{1}' OR [ArrivalActualEndForGate1] BETWEEN '{0}' AND '{1}' OR '{0}' BETWEEN [ArrivalActualBeginForGate1] AND [ArrivalActualEndForGate1]   OR  '{1}' BETWEEN  [ArrivalActualBeginForGate1] AND [ArrivalActualEndForGate1]) AND FTYP IN ('O','Z','B','S','T','G') ORDER BY [ArrivalActualBeginForGate1],[ArrivalActualEndForGate1]", sFromDate, sToDate);
                    break;
            }

            EntityCollectionBase<EntFlight> objArrivalFlightList = null;

            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntFlight),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };
            objArrivalFlightList = dataContext.OpenEntityCollection<EntFlight>(command);
          
            return objArrivalFlightList;
        }
        /// <summary>
        /// Load Schedule or Actual Flight List by date Range for Arrival
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <returns>EntityCollection for Arrival Flight</returns>
        public static EntityCollectionBase<EntFlight> LoadArrivalFlightListByDateRange2(DateTime dtFrom, DateTime dtTo, string sType)
        {
            string WHERE_CLAUSE = "";
            const string FIELD_LIST = "[GateOfArrival1],[GateOfArrival2],[ArrivalScheduleBeginForGate1],[ArrivalScheduleEndForGate1],[ArrivalScheduleBeginForGate2],[ArrivalScheduleEndForGate2]," +
            "[ArrivalActualBeginForGate1],[ArrivalActualEndForGate1],[ArrivalActualBeginForGate2],[ArrivalActualEndForGate2],[FullFlightNumber]";

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(dtFrom, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(dtTo, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);

            switch (sType)
            {
                case "PLAN":
                    WHERE_CLAUSE = String.Format("WHERE ([ArrivalDepartureId] = 'A' OR [ArrivalDepartureId] = 'B') AND [GateOfArrival2] <> ' ' AND ([ArrivalScheduleBeginForGate2] BETWEEN '{0}' AND '{1}' OR [ArrivalScheduleEndForGate2] BETWEEN '{0}' AND '{1}'  OR '{0}' BETWEEN [ArrivalScheduleBeginForGate2] AND [ArrivalScheduleEndForGate2]  OR '{1}' BETWEEN [ArrivalScheduleBeginForGate2] AND [ArrivalScheduleEndForGate2]) AND FTYP IN ('O','Z','B','S','T','G') ORDER BY [ArrivalScheduleBeginForGate2],[ArrivalScheduleEndForGate2]", sFromDate, sToDate);
                    break;
                case "ACT":
                    WHERE_CLAUSE = String.Format("WHERE ([ArrivalDepartureId] = 'A' OR [ArrivalDepartureId] = 'B') AND [GateOfArrival2] <> ' ' AND  ([ArrivalActualBeginForGate2] BETWEEN '{0}' AND '{1}' OR [ArrivalActualEndForGate2] BETWEEN '{0}' AND '{1}' OR '{0}' BETWEEN [ArrivalActualBeginForGate2] AND [ArrivalActualEndForGate2]   OR  '{1}' BETWEEN  [ArrivalActualBeginForGate2] AND [ArrivalActualEndForGate2]) AND FTYP IN ('O','Z','B','S','T','G') ORDER BY [ArrivalActualBeginForGate2],[ArrivalActualEndForGate2]", sFromDate, sToDate);
                    break;
            }

            EntityCollectionBase<EntFlight> objArrivalFlightList = null;

            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntFlight),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };

            objArrivalFlightList = dataContext.OpenEntityCollection<EntFlight>(command);
           
            return objArrivalFlightList;
        }
        /// <summary>
        /// Load Schedule or Actual Flight List by date Range for Departure
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <returns>EntityCollection for Departure Flight</returns>
        public static EntityCollectionBase<EntFlight> LoadDepartureFlightListByDateRange(DateTime dtFrom, DateTime dtTo, string sType)
        {
            string WHERE_CLAUSE = "";
            const string FIELD_LIST = "[GateOfDeparture1],[GateOfDeparture2],[DepartureScheduleBeginForGate1],[DepartureScheduleEndForGate1],[DepartureScheduleBeginForGate2],[DepartureScheduleEndForGate2]," +
            "[DepartureActualBeginForGate1],[DepartureActualEndForGate1],[DepartureActualBeginForGate2],[DepartureActualEndForGate2],[FullFlightNumber]";

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(dtFrom, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(dtTo, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            switch (sType)
            {
                case "PLAN":
                    WHERE_CLAUSE = String.Format("WHERE ([ArrivalDepartureId] = 'D' OR [ArrivalDepartureId] = 'B') AND [GateOfDeparture1] <> ' ' AND ([DepartureScheduleBeginForGate1] BETWEEN '{0}' AND '{1}' OR [DepartureScheduleEndForGate1] BETWEEN '{0}' AND '{1}'  OR '{0}' BETWEEN [DepartureScheduleBeginForGate1] AND [DepartureScheduleEndForGate1] OR '{1}'BETWEEN  [DepartureScheduleBeginForGate1] AND [DepartureScheduleEndForGate1]) AND FTYP IN ('O','Z','B','S','T','G') ORDER BY [DepartureScheduleBeginForGate1],[DepartureScheduleEndForGate1]", sFromDate, sToDate);
                    break;
                case "ACT":
                    WHERE_CLAUSE = String.Format("WHERE ([ArrivalDepartureId] = 'D' OR [ArrivalDepartureId] = 'B') AND [GateOfDeparture1] <> ' ' AND ([DepartureActualBeginForGate1] BETWEEN '{0}' AND '{1}' OR [DepartureActualEndForGate1] BETWEEN '{0}' AND '{1}' OR '{0}' BETWEEN [DepartureActualBeginForGate1] AND [DepartureActualEndForGate1]  OR '{1}' BETWEEN [DepartureActualBeginForGate1] AND [DepartureActualEndForGate1]) AND FTYP IN ('O','Z','B','S','T','G') ORDER BY [DepartureActualBeginForGate1],[DepartureActualEndForGate1]", sFromDate, sToDate);
                    break;
            }

            EntityCollectionBase<EntFlight> objDepartureFlightList = null;

            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntFlight),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };
            objDepartureFlightList = dataContext.OpenEntityCollection<EntFlight>(command);
            
            return objDepartureFlightList;
        }
        /// <summary>
        /// Load Schedule or Actual Flight List by date Range for Departure
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <returns>EntityCollection for Departure Flight</returns>
        public static EntityCollectionBase<EntFlight> LoadDepartureFlightListByDateRange2(DateTime dtFrom, DateTime dtTo, string sType)
        {
            string WHERE_CLAUSE = "";
            const string FIELD_LIST = "[GateOfDeparture1],[GateOfDeparture2],[DepartureScheduleBeginForGate1],[DepartureScheduleEndForGate1],[DepartureScheduleBeginForGate2],[DepartureScheduleEndForGate2]," +
            "[DepartureActualBeginForGate1],[DepartureActualEndForGate1],[DepartureActualBeginForGate2],[DepartureActualEndForGate2],[FullFlightNumber]";

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(dtFrom, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(dtTo, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            switch (sType)
            {
                case "PLAN":
                    WHERE_CLAUSE = String.Format("WHERE ([ArrivalDepartureId] = 'D' OR [ArrivalDepartureId] = 'B') AND [GateOfDeparture2] <> ' ' AND ([DepartureScheduleBeginForGate2] BETWEEN '{0}' AND '{1}' OR [DepartureScheduleEndForGate2] BETWEEN '{0}' AND '{1}'  OR '{0}' BETWEEN [DepartureScheduleBeginForGate2] AND [DepartureScheduleEndForGate2] OR '{1}'BETWEEN  [DepartureScheduleBeginForGate2] AND [DepartureScheduleEndForGate2]) AND FTYP IN ('O','Z','B','S','T','G') ORDER BY [DepartureScheduleBeginForGate2],[DepartureScheduleEndForGate2]", sFromDate, sToDate);
                    break;
                case "ACT":
                    WHERE_CLAUSE = String.Format("WHERE ([ArrivalDepartureId] = 'D' OR [ArrivalDepartureId] = 'B') AND [GateOfDeparture2] <> ' ' AND ([DepartureActualBeginForGate2] BETWEEN '{0}' AND '{1}' OR [DepartureActualEndForGate2] BETWEEN '{0}' AND '{1}' OR '{0}' BETWEEN [DepartureActualBeginForGate2] AND [DepartureActualEndForGate2]  OR '{1}' BETWEEN [DepartureActualBeginForGate2] AND [DepartureActualEndForGate2]) AND FTYP IN ('O','Z','B','S','T','G') ORDER BY [DepartureActualBeginForGate2],[DepartureActualEndForGate2]", sFromDate, sToDate);
                    break;
            }

            EntityCollectionBase<EntFlight> objDepartureFlightList = null;

            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntFlight),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };
            objDepartureFlightList = dataContext.OpenEntityCollection<EntFlight>(command);
            
            return objDepartureFlightList;
        }
        /// <summary>
        /// Get Gate List
        /// </summary>
        /// <param name="sFlightNo">System.string containing sFlightNo</param>
        /// <param name="lFlightUrno">System.string containing sFlightUrno</param>
        /// <returns>Return Entity Collection object of EntDbGate</returns>
        public static EntityCollectionBase<EntGate> GetGateList()
        {
            const string FIELD_LIST = "[GateName],[GateType],[BusGate]";
            string WHERE_CLAUSE = String.Format("ORDER BY [GateName]");


            EntityCollectionBase<EntGate> objGateList = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

           

            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntGate),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };

            objGateList = dataContext.OpenEntityCollection<EntGate>(command);

            return objGateList;
        }
    }
}
