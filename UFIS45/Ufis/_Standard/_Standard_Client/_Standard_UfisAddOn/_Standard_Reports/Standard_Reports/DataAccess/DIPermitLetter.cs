﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Data;
using Ufis.Entities;
using Standard_Reports.Helpers;
using Ufis.Data.Ceda;


namespace Standard_Reports.DataAccess
{
    public class DIPermitLetter
    {
        public static EntDbFlightPermit LoadFlightPermit(int urno)
        {
            string WHERE_CLAUSE = "";
            const string FIELD_LIST = "[Urno],[ApprovalDate],[ValidFrom],[ValidTo],[ArrivalDepartureId],[FlightPurpose],[OperatorName],[Email],[Contact],[AgtName],[AircraftIATACode],[CallSign],[FlightPermitNumber],[RequestDate],[RegistrationNumber],[RequestBay]";

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            WHERE_CLAUSE = String.Format("WHERE URNO = '{0}'", urno);
            EntityCollectionBase<EntDbFlightPermit> objFlightPermitList = null;
            EntDbFlightPermit objFlightPermit;
            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntDbFlightPermit),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };
            objFlightPermitList = dataContext.OpenEntityCollection<EntDbFlightPermit>(command);
            objFlightPermit = objFlightPermitList.FirstOrDefault();
            return objFlightPermit;
        }


        public static EntDbFlight GetFlight(string scsgn, string sregn, string sadid, DateTime svafr, DateTime svato)
        {
            const string FIELD_LIST = "[RelationKey],[StandardTimeOfArrival],[StandardTimeOfDeparture],[OperationalType],[OriginAirportIATACode],[OriginAirportICAOCode],[DestinationAirportIATACode],[DestinationAirportICAOCode],[BestEstimatedArrivalTime],[BestEstimatedDepartureTime]";
            string WHERE_CLAUSE = "";
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(svafr, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(svato, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);

            if (sadid == "A" && sregn != "")
                WHERE_CLAUSE = String.Format("WHERE [CallSign] = '{0}' AND [RegistrationNumber] = '{1}' AND [StandardTimeOfArrival] >= '{3}' AND [StandardTimeOfArrival] <= '{4}'", scsgn, sregn, sadid, sFromDate, sToDate);
            else
                if (sadid == "A" && sregn == "")
                    WHERE_CLAUSE = String.Format("WHERE [CallSign] = '{0}' AND [RegistrationNumber] = '    ' AND [StandardTimeOfArrival] >= '{3}' AND [StandardTimeOfArrival] <= '{4}'", scsgn, sregn, sadid, sFromDate, sToDate);
                else
                    if (sadid == "D" && sregn == "")
                        WHERE_CLAUSE = String.Format("WHERE [CallSign] = '{0}' AND [RegistrationNumber] = '    ' AND [StandardTimeOfDeparture] >= '{3}' AND [StandardTimeOfDeparture] <= '{4}'", scsgn, sregn, sadid, sFromDate, sToDate);
                    else
                        if (sadid == "D" && sregn != "")
                            WHERE_CLAUSE = String.Format("WHERE [CallSign] = '{0}' AND [RegistrationNumber] = '{1}' AND [StandardTimeOfDeparture] >= '{3}' AND [StandardTimeOfDeparture] <= '{4}'", scsgn, sregn, sadid, sFromDate, sToDate);
            EntityCollectionBase<EntDbFlight> objFlightList = null;

            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntDbFlight),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };

            objFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);
            EntDbFlight flight = objFlightList.FirstOrDefault();

            return flight;
        }

        public static EntDbFlight GetDepartureFlight(int? rKey)
        {
            const string FIELD_LIST = "[RelationKey],[StandardTimeOfArrival],[StandardTimeOfDeparture],[OperationalType],[OriginAirportIATACode],[OriginAirportICAOCode],[DestinationAirportIATACode],[DestinationAirportICAOCode],[BestEstimatedArrivalTime],[BestEstimatedDepartureTime]";
            string WHERE_CLAUSE = "";
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            WHERE_CLAUSE = String.Format("WHERE [ArrivalDepartureId] = 'D' AND [RelationKey] = '{0}' ", rKey);
            EntityCollectionBase<EntDbFlight> objDepartureFlightList = null;

            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntDbFlight),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };

            objDepartureFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);
            EntDbFlight depFlight = objDepartureFlightList.FirstOrDefault();

            return depFlight;
        }

        public static EntDbFlight GetArrivalFlight(int? urno)
        {
            const string FIELD_LIST = "[Urno],[StandardTimeOfArrival],[StandardTimeOfDeparture],[OperationalType],[OriginAirportIATACode],[OriginAirportICAOCode],[DestinationAirportIATACode],[DestinationAirportICAOCode],[BestEstimatedArrivalTime],[BestEstimatedDepartureTime]";
            string WHERE_CLAUSE = "";
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            WHERE_CLAUSE = String.Format("WHERE [ArrivalDepartureId] = 'A' AND [Urno] = '{0}' ", urno);
            EntityCollectionBase<EntDbFlight> objDepartureFlightList = null;

            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objDepartureFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);
            EntDbFlight depFlight = objDepartureFlightList.FirstOrDefault();

            return depFlight;
        }
        public static void SavePermit(EntDbFlightPermit permit)
        {
            string strErr = null;
            try
            {
                EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
                string attributes;
                if (permit.ApprovalDate != null && permit.RequestDate == null)
                    attributes = "EMLE,TELX,AGNT,OPNA,PURP,APRD,REQD,RBAY";
                else
                    if (permit.ApprovalDate == null && permit.RequestDate != null)
                        attributes = "EMLE,TELX,AGNT,OPNA,PURP,REQD,RBAY";
                    else
                        if (permit.ApprovalDate == null && permit.RequestDate == null)
                            attributes = "EMLE,TELX,AGNT,OPNA,PURP,RBAY";
                        else
                            attributes = "EMLE,TELX,AGNT,OPNA,PURP,APRD,REQD,RBAY";
                permit.SetEntityState(Entity.EntityState.Modified);
                dataContext.EntityAdapter.Update(permit, attributes);
            }
            catch (Exception ex)
            {
                strErr = ex.Message;
            }
        }
    }
}
