﻿using System;
using Ufis.Data;
using System.Linq;
using Ufis.Entities;
using Ufis.Data.Ceda;
using System.Collections.Generic;
using Standard_Reports.Helpers;
using Standard_Reports.Entities;


namespace Standard_Reports.DataAccess
{
    public class DlPositionUsage
    {
        /// <summary>
        /// Load Flight List by date Range and Position
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <param name="sPosition">System.String containing sPosition</param>
        /// <returns>EntityCollection for Arrival/Departure Flight</returns>
        public static EntityCollectionBase<EntFlight> LoadFlightListByDateRange(DateTime dtFrom, DateTime dtTo, string sPosition)
        {
            const string FIELD_LIST = "[Urno],[FullFlightNumber],[RelationKey],[ArrivalDepartureId],[PositionOfArrival],[PositionOfDeparture],[TimeframeOfArrival],[TimeframeOfDeparture],[PositionArrivalScheduledStart],[PositionArrivalScheduledEnd],[PositionDepartureScheduledStart],[PositionDepartureScheduledEnd]";
            string WHERE_CLAUSE = string.Empty;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            
           

            if (HpUser.TLocalUTC == "L")
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            else
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(dtFrom, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(dtTo, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);

            string sFDate = objCedaDataConverter.DataItemToCedaItem(dtFrom.AddDays(-14), typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 8, false);
            string sTDate = objCedaDataConverter.DataItemToCedaItem(dtTo.AddDays(+14), typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 8, false);

            WHERE_CLAUSE = String.Format("WHERE (((([StandardTimeOfArrival] BETWEEN '{0}' AND '{1}' AND [DestinationAirportIATACode] = '{2}') OR ([TimeframeOfArrival] BETWEEN '{0}' AND '{1}' OR ('{0}' BETWEEN [PositionArrivalScheduledStart] AND [PositionArrivalScheduledEnd] AND [PositionOfArrival] <> ' ') AND [DestinationAirportIATACode] = '{2}')) OR (([StandardTimeOfDeparture] BETWEEN '{0}' AND '{1}' AND [OriginAirportIATACode] = '{2}') OR ([TimeframeOfDeparture] BETWEEN '{0}' AND '{1}' AND [OriginAirportIATACode] = '{2}'))) AND ([PositionOfArrival] IN ('{5}') OR [PositionOfDeparture] IN ('{5}')) AND FTYP IN ( 'O','Z','B','S','T','G'))[ROTATIONS][FILTER=[OperationalType] IN ('O','Z','B','S','T','G') AND [ArrivalDepartureId] IN ('A','D','B')][USEFOGTAB,{3},{4}]", sFromDate, sToDate, (DlUfisData.Current.DataContext.Connection).HomeAirport, sFDate, sTDate, sPosition);
            //WHERE_CLAUSE = String.Format("WHERE (((([StandardTimeOfArrival] BETWEEN '{0}' AND '{1}' AND [DestinationAirportIATACode] = '{2}') OR ([TimeframeOfArrival] BETWEEN '{0}' AND '{1}' OR ('{0}' BETWEEN [PositionArrivalScheduledStart] AND [PositionArrivalScheduledEnd] AND [PositionOfArrival] <> ' ') AND [DestinationAirportIATACode] = '{2}')) OR (([StandardTimeOfDeparture] BETWEEN '{0}' AND '{1}' AND [OriginAirportIATACode] = '{2}') OR ([TimeframeOfDeparture] BETWEEN '{0}' AND '{1}' AND [OriginAirportIATACode] = '{2}'))) AND ([PositionOfArrival] IN ('{5}') OR [PositionOfDeparture] IN ('{5}')))[ROTATIONS][FILTER=[ArrivalDepartureId] IN ('A','D','B')][USEFOGTAB,{3},{4}]", sFromDate, sToDate, (DlUfisData.Current.DataContext.Connection).HomeAirport, sFDate, sTDate, sPosition);
            EntityCollectionBase<EntFlight> objFlightList = null;
            
            DataCommand command = new CedaEntitySelectFlightCommand() {
            //EntityType = typeof(EntDbFlight),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };
            objFlightList = dataContext.OpenEntityCollection<EntFlight>(command);

            return objFlightList;
        }

        /// <summary>
        /// Load Position List by date Range and Position
        /// </summary>
        /// <param name="sPosition">System.string containing sPosition</param>
        /// <returns>Return Entity Collection object of EntDbGate</returns>
        public static EntityCollectionBase<EntParkingStand> GetPositionList(DateTime dtFrom, DateTime dtTo, string sPosition)
        {
            const string FIELD_LIST = "[Urno],[Name],[ValidFrom],[ValidTo]";
            string WHERE_CLAUSE = string.Empty;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

           

            if (HpUser.TLocalUTC == "L")
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            else
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(dtFrom, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(dtTo, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);

            //WHERE_CLAUSE = String.Format("WHERE ([ValidFrom] < '{1}' AND ([ValidTo] >= '{0}' OR [ValidTo] = ' ')) AND [Name] IN ('{2}')  ORDER BY [ValidFrom],[ValidTo]", sFromDate, sToDate, sPosition);
            WHERE_CLAUSE = String.Format("WHERE [Name] IN ('{0}')  ORDER BY [ValidFrom],[ValidTo]", sPosition);

            EntityCollectionBase<EntParkingStand> objPositionList = null;
            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntParkingStand),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };

            objPositionList = dataContext.OpenEntityCollection<EntParkingStand>(command);

            return objPositionList;
        }

        /// <summary>
        /// Get Gate List by position
        /// </summary>
        /// <param name="sPosition">System.string containing sPosition</param>
        /// <returns>Return Entity Collection object of EntDbGate</returns>
        public static EntityCollectionBase<EntUnavailable> GetBlockList(DateTime dtFrom, DateTime dtTo, string sPosition)
        {
            const string FIELD_LIST = "[RelatedUrno],[ValidFrom],[ValidTo]";
            string WHERE_CLAUSE = string.Empty;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

           

            if (HpUser.TLocalUTC == "L")
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            else
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(dtFrom, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(dtTo, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            //TABN = PST AND TYPE <> 'X'
            WHERE_CLAUSE = String.Format("WHERE [ReferenceTable] = 'PST' AND [ActualChangeable] <> 'X' AND  ([ValidFrom] BETWEEN '{0}' AND '{1}' OR [ValidTo] BETWEEN '{0}' AND '{1}'  OR '{0}' BETWEEN [ValidFrom] AND [ValidTo] OR '{1}'BETWEEN  [ValidFrom] AND [ValidTo] OR [ValidTo] = ' ') ORDER BY [ValidFrom],[ValidTo]", sFromDate, sToDate);

            EntityCollectionBase<EntUnavailable> objBlockList = null;
            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntUnavailable),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };

            objBlockList = dataContext.OpenEntityCollection<EntUnavailable>(command);

            return objBlockList;
        }

        /// <summary>
        /// Get Gate List by position
        /// </summary>
        /// <param name="sPosition">System.string containing sPosition</param>
        /// <returns>Return Entity Collection object of EntDbGate</returns>
        public static EntityCollectionBase<EntUnavailable> GetBlockListChangeable(DateTime dtFrom, DateTime dtTo, string sPosition)
        {
            const string FIELD_LIST = "[RelatedUrno],[ValidFrom],[ValidTo]";
            string WHERE_CLAUSE = string.Empty;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

           

            if (HpUser.TLocalUTC == "L")
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            else
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(dtFrom, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(dtTo, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            //TABN = PST AND TYPE <> ' '
            WHERE_CLAUSE = String.Format("WHERE [ReferenceTable] = 'PST' AND [ActualChangeable] = 'X' AND  ([ValidFrom] BETWEEN '{0}' AND '{1}' OR [ValidTo] BETWEEN '{0}' AND '{1}'  OR '{0}' BETWEEN [ValidFrom] AND [ValidTo] OR '{1}'BETWEEN  [ValidFrom] AND [ValidTo] OR [ValidTo] = ' ')  ORDER BY [ValidFrom],[ValidTo]", sFromDate, sToDate);

            EntityCollectionBase<EntUnavailable> objBlockList = null;
            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntUnavailable),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };

            objBlockList = dataContext.OpenEntityCollection<EntUnavailable>(command);

            return objBlockList;
        }
        /// <summary>
        /// Load Position List 
        /// </summary>
        /// <param name=""></param>
        /// <returns>EntityCollection for Position  List</returns>
        public static EntityCollectionBase<EntParkingStand> LoadPositionList()
        {
            const string FIELD_LIST = "[Name]";
            string WHERE_CLAUSE = "ORDER BY [Name]";

            EntityCollectionBase<EntParkingStand> objPositionList = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

           

            DataCommand command = new CedaEntitySelectCommand() { EntityType = typeof(EntParkingStand),
            AttributeList = FIELD_LIST,
            EntityWhereClause = WHERE_CLAUSE };

            objPositionList = dataContext.OpenEntityCollection<EntParkingStand>(command);

            return objPositionList;
        }

        private static IList<entlcPosition> _positionList;
        private static IList<EntParkingStand> objPositionList;
        public static IList<entlcPosition> LoadPosition()
        {
            if (null == _positionList)
            {
                _positionList = new List<entlcPosition>();

                //Get Airline  List 
                objPositionList = LoadPositionList();

                foreach (EntParkingStand item in objPositionList)
                {
                    _positionList.Add(new entlcPosition { Name = item.Name });
                }
            }

            return _positionList;
        }
    }
}
