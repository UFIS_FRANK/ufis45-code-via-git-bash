﻿using Standard_Reports.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.LoginWindow;
using Ufis.MVVM.ViewModel;
using Ufis.NotificationWindow;
using System.ComponentModel;


namespace Standard_Reports.Shell
{
    /// <summary>
    /// The ViewModel for the application's shell window.
    /// </summary>
    public class ShellViewModel : WorkspaceViewModel
    {
        #region Fields
        WorkspaceViewModel _workspace;

        #endregion

        #region Constructor

        public ShellViewModel()
        {
            DisplayName = HpAppInfo.Current.ProductTitle;
        }

        #endregion

        public string DisplayName { get;
            set; }

        #region Workspace

        public WorkspaceViewModel Workspace
        {
            get
            {
                return _workspace;
            }
            set
            {
                if (_workspace == value)
                    return;

                //Detach the old workspace event handler
                if (_workspace != null)
                    _workspace.RequestClose -= OnWorkspaceRequestClose;

                //Attach the new workspace event handler
                _workspace = value;
                if (_workspace != null)
                    _workspace.RequestClose += OnWorkspaceRequestClose;

                OnPropertyChanged("Workspace");
            }
        }

        void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            WorkspaceViewModel workspace = sender as WorkspaceViewModel;

            if (workspace is NotificationViewModel) //check if the workspace if notification view            
            {
                NotificationViewModel notificationViewModel = workspace as NotificationViewModel;
                if (notificationViewModel.IsQuitCommandExecuted)
                {
                    CloseCommand.Execute(null);
                    workspace.Dispose();
                }
            }
            else
                if (workspace is LoginViewModel) //check if the workspace if login view
                {
                    LoginViewModel loginViewModel = workspace as LoginViewModel;
                    if (loginViewModel.LoginSucceeded)
                    {
                        loginViewModel.ShowLoadingBar = true;

                        //Set the current user
                        HpUser.ActiveUser = loginViewModel.User;
                        HpUser.Privileges = loginViewModel.UserPrivileges;
                        DataAccess.DlUfisData.Current.DataContext.Connection.UserName = HpUser.ActiveUser.UserId;

                        workspace.Dispose();

                        using (BackgroundWorker worker = new BackgroundWorker())
                        {
                            worker.DoWork += (o, d) =>
                            {
                                //WorkspaceViewModel mainWindowViewModel = new MainWindowViewModel();    
                                //WorkspaceViewModel mainWindowViewModel = new Standard_Reports.Reports.ApprovalReport.ApprovalReportViewModel();
                                //WorkspaceViewModel mainWindowViewModel = new Standard_Reports.Reports.PositionUsage.PositionUsageViewModel();// Standard_Reports.Reports.ApprovalReport.ApprovalReportViewModel();
                                WorkspaceViewModel mainWindowViewModel = new Standard_Reports.Reports.PermitLetter.PermitLetterViewModel();
                                Workspace = mainWindowViewModel;
                            };
                            worker.RunWorkerAsync();
                        }
                    }
                    else
                    {
                        CloseCommand.Execute(null);
                        workspace.Dispose();
                    }
                }
                else
                {
                    workspace.Dispose();
                }
        }

        #endregion
    }
}
