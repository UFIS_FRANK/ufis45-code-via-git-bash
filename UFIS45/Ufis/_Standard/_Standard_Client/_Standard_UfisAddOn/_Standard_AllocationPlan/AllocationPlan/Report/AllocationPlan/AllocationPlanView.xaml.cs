﻿#region +++++ Source Code Information +++++
/*
 * Developed By         :          Phyoe Khaing Min
 * Developed On         :          
 * Completed On         :           20-Feb-2012
 * Description          :           Get Flight Information from Flight Table (AFTTAB). You can filter by date and time range.
 * 
 * Updated On           :           18-Jun-2012
 * Updated By           :           Phyoe Khaing Min
 * Update Detail        :           Add No of PAX Breakdown for (First Class, Business Class and Economy Class)
 * 
 * Update On            :           4-September-2012
 * Updated By           :           Phyoe Khaing Min
 * Update Detail        :           To show correct sorting Order while user sort by STA and STD
 * 
 *                                  
 */
#endregion

using System;
using System.Linq;
using System.Windows;
using System.Threading;
using DevExpress.Xpf.Grid;
using System.Globalization;
using System.Windows.Controls;
using DevExpress.Xpf.Printing;
using DevExpress.XtraPrinting;
using System.Collections.Generic;
using Ufis.AllocationPlan.Helpers;
using Ufis.LoginWindow;
using Ufis.IO;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using DevExpress.Xpf.Bars;
using Ufis.Entities;
using System.Drawing.Printing;

namespace Ufis.AllocationPlan
{
    /// <summary>
    /// Interaction logic for AllocationPlanView.xaml
    /// </summary>
    public partial class AllocationPlanView : UserControl
    {
        #region +++ Variable Declaration +++
        public HpParameters param;
        public HpReportLogo Logo;
        public HpFooter Footer;        
        public AllocationPlanViewModel objViewModel;
        string sFooterString = "";
        DateTime tfDate;
        DateTime ttDate;
        #endregion

        #region +++ Register Properties +++
        // Using a DependencyProperty as the backing store for MyInt.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyIntProperty =
            DependencyProperty.Register("MyInt", typeof(int), typeof(AllocationPlanView), new UIPropertyMetadata(1));
        // Using a DependencyProperty as the backing store for MyData.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LoadingPanelVisibilityProperty =
            DependencyProperty.Register("y LoadingPanelVisibility", typeof(string), typeof(AllocationPlanView), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.AffectsRender));
        #endregion

        #region +++ Properties +++
        public int MyInt
        {
            get { return (int)GetValue(MyIntProperty); }
            set { SetValue(MyIntProperty, value); }
        }

        public Visibility LoadingPanelVisibility
        {
            get { return (Visibility)GetValue(LoadingPanelVisibilityProperty); }
            set { SetValue(LoadingPanelVisibilityProperty, value); }
        }
        #endregion 

        #region +++ WPF Application Events +++
        private void Process(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(2000);
            Dispatcher.BeginInvoke(new Action(() =>
                                        {
                                            FetchData();
                                        }));
        }

        public AllocationPlanView()
        {
            //Change UFIS Date format into system.Thread
            CultureInfo ci = new CultureInfo(Thread.CurrentThread.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            Thread.CurrentThread.CurrentCulture = ci;            
            InitializeComponent();
            dtpFromDate.SelectedDate = DateTime.Now.Date;
            dtpToDate.SelectedDate = DateTime.Now.Date;
            txtFromTime.Text = "0000";
            txtToTime.Text = "2359";
            
        }

        private void barButtonPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (gridAllocationPlan.VisibleRowCount > 0)
            {

                EntUser objEntUser = new EntUser();
                objEntUser = LoginViewModel.GetLastUser();


                objViewModel = new AllocationPlanViewModel();
                param = new HpParameters();
                Footer = new HpFooter();
                DateTime tDate = dtpFromDate.SelectedDate.Value;
                string sDate = tDate.ToString("dd.MM.yyyy");
                DateTime ttDate = dtpToDate.SelectedDate.Value;
                string stDate = ttDate.ToString("dd.MM.yyyy");

                sFooterString = objViewModel.GetParameterValue();

                param.MyParameter = String.Format("Allocation Plan from {0} {1} to {2} {3} {4} ", sDate, txtFromTime.Text, stDate, txtToTime.Text, String.Compare(HpUser.TLocalUTC, "L", false) == 0 ? "LT" : "UTC");
                param.Header1 = "                  Airport Operations Control Center (AOCC)";
                param.ReportFooterString = sFooterString;
                param.GeneratedBy = string.Format("Printed By : {0}", objViewModel.GetLoginUserName(objEntUser.UserId));

                ((HpParameters)Resources["paramet"]).MyParameter = param.MyParameter;
                ((HpParameters)Resources["NewHeader1"]).Header1 = param.Header1;
                ((HpParameters)Resources["Footer"]).ReportFooterString = param.ReportFooterString;
                ((HpParameters)Resources["GenBy"]).GeneratedBy = param.GeneratedBy;

                string sLogoPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.UfisSystem);
                sLogoPath = sLogoPath + "\\stdlogo.bmp";

                Logo = new HpReportLogo() { RptLogo = new BitmapImage(new Uri(sLogoPath, UriKind.Absolute)) };
                ((HpReportLogo)Resources["Logo"]).RptLogo = Logo.RptLogo;

                PrintableControlLink link = new PrintableControlLink((TableView)gridAllocationPlan.View) { Margins = new Margins(50, 50, 30, 30), Landscape = true };
                link.Margins.Left = 1;
                link.Margins.Right = 1;
                link.Margins.Bottom = 3;
                link.PageHeaderTemplate = (DataTemplate)Resources["detailPrintHeaderTemplate"];
                link.PaperKind = PaperKind.A4;
                link.PageFooterTemplate = (DataTemplate)Resources["DetailPrintFooterTemplate"];
                link.PrintingSystem.ExportOptions.Xls.TextExportMode = TextExportMode.Text;
                link.PrintingSystem.ExportOptions.Xlsx.TextExportMode = TextExportMode.Text;   
                link.ShowPrintPreviewDialog(Window.GetWindow(this));
            }
        }

        private void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            gridAllocationPlan.ShowLoadingPanel = false;
        }

        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (String.Compare(dtpFromDate.SelectedDate.ToString(), "", false) != 0 && String.Compare(dtpToDate.SelectedDate.ToString(), "", false) != 0 && String.Compare(txtFromTime.Text, "", false) != 0 && String.Compare(txtToTime.Text, "", false) != 0 && dtpToDate.SelectedDate != null && dtpFromDate.SelectedDate != null && txtFromTime.Text.Length == 4 && txtToTime.Text.Length == 4)                
                {
                    string sfDate, stDate;                      
                    tfDate = dtpFromDate.SelectedDate.Value;                      
                    ttDate = dtpToDate.SelectedDate.Value;                      
                    sfDate = String.Format("{0:yyyy.MM.dd} {1}", tfDate, txtFromTime.Text.Insert(2, ":"));                      
                    stDate = String.Format("{0:yyyy.MM.dd} {1}", ttDate, txtToTime.Text.Insert(2, ":"));
                    tfDate = Convert.ToDateTime(sfDate);
                    ttDate = Convert.ToDateTime(stDate);
                    if (tfDate > ttDate)
                    {                        
                        MessageBox.Show("Invalid Date/time value.");
                    }
                    tfDate.AddHours(Convert.ToDouble(txtFromTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtFromTime.Text.Substring(2, 2))).AddSeconds(0);                      
                    ttDate.AddHours(Convert.ToDouble(txtToTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtToTime.Text.Substring(2, 2))).AddSeconds(59);

                    gridAllocationPlan.ShowLoadingPanel = true;

                    BackgroundWorker bw = new BackgroundWorker();

                    bw.DoWork += Process;
                    bw.RunWorkerCompleted += bgw_RunWorkerCompleted;
                    bw.RunWorkerAsync();
                }                
                else                
                {                    
                    MessageBox.Show("Invalid Date/Time value.");
                    gridAllocationPlan.ItemsSource = null;                
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0}\n\r{1}\n\r{2}", ex.Message, ex.InnerException, ex.StackTrace));
            }

        }

        private void FetchData()
        {
            objViewModel = new AllocationPlanViewModel();

            objViewModel.GetAllocationPlanList(tfDate, ttDate);

            gridAllocationPlan.ItemsSource = objViewModel.ItemSource;
        }
        #endregion        
    }
}