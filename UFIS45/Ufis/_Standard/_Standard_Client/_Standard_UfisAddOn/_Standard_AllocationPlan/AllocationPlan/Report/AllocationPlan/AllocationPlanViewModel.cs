﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using Ufis.AllocationPlan.DataAccess;
using Ufis.AllocationPlan.Entities;

namespace Ufis.AllocationPlan
{
    public class AllocationPlanViewModel : WorkspaceViewModel
    {
        #region +++ Variable Declaration +++
        public WorkspaceViewModel _workspace;
        IList<entlcAllocationPlan> _itemSource;
        List<entlcAllocationPlan> objAllocationPlan = new List<entlcAllocationPlan>();
        EntityCollectionBase<EntDbFlight> objArrivalFlights;
        EntityCollectionBase<EntDbFlight> objDepFlights;
        Dictionary<int, int> _dicDep;
        DateTime? date;
        #endregion

        #region +++ Data Manipulation +++
        /// <summary>
        /// Get Flight list by Date Range
        /// </summary>
        /// <param name="sFrom">System.DateTime containing sFrom</param>
        /// <param name="sTo">System.Datetime containing sTo</param>
        private void GetFlights(DateTime sFrom, DateTime sTo)
        {
            //Get Arrival Flights by date and time range
            objArrivalFlights = DlAllocationPlan.LoadArrivalFlightListByDateRange(sFrom, sTo, "A");
            //Get Departure Flights by date and time range
            objDepFlights = DlAllocationPlan.LoadArrivalFlightListByDateRange(sFrom, sTo, "D");
        }
        /// <summary>
        /// Do process
        /// </summary>
        /// <param name="sFrom">System.DateTime Containing sFrom</param>
        /// <param name="sTo">Sytstem.DateTime containing sTo</param>
        public void GetAllocationPlanList(DateTime sFrom, DateTime sTo)
        {
            _dicDep = new Dictionary<int, int>();

            GetFlights(sFrom, sTo);
            GetArrivalRkeyList();
            GetDepartureFlighsts();
            objAllocationPlan.OrderBy(d => d.SCOL);
            ItemSource = objAllocationPlan;
        }
        /// <summary>
        /// Get Departure Flight
        /// </summary>
        private void GetDepartureFlighsts()
        {
            if (objDepFlights.Count > 0)
            {
                foreach (EntDbFlight tmpObjDepartureFlight in objDepFlights)
                {
                    if (!_dicDep.ContainsKey(tmpObjDepartureFlight.Urno))
                    {
                        try
                        {
                            _dicDep.Add(tmpObjDepartureFlight.Urno, tmpObjDepartureFlight.Urno);
                        }
                        catch (Exception)
                        {
                            //On Error resume next 
                        }
                        entlcAllocationPlan TmpobjAllocationPlan = new entlcAllocationPlan();

                        TmpobjAllocationPlan.Departure = string.IsNullOrEmpty(tmpObjDepartureFlight.FullFlightNumber) ? tmpObjDepartureFlight.CallSign : tmpObjDepartureFlight.FullFlightNumber;

                        TmpobjAllocationPlan.TO = tmpObjDepartureFlight.DestinationAirportIATACode;

                        if (tmpObjDepartureFlight.StandardTimeOfDeparture != null)
                        {
                            date = tmpObjDepartureFlight.StandardTimeOfDeparture; if (date.HasValue)
                            {
                                TmpobjAllocationPlan.STD = date.Value;
                                //TmpobjAllocationPlan.SCOL = TmpobjAllocationPlan.STD;
                            }
                        }
                        if (tmpObjDepartureFlight.TotalNumberOfPassengers != null)
                            TmpobjAllocationPlan.DPax = tmpObjDepartureFlight.TotalNumberOfPassengers;

                        if (tmpObjDepartureFlight.FirstClassPAX != null)
                            TmpobjAllocationPlan.DPaxF = tmpObjDepartureFlight.FirstClassPAX;

                        if (tmpObjDepartureFlight.BusinessClassPAX != null)
                            TmpobjAllocationPlan.DPaxB = tmpObjDepartureFlight.BusinessClassPAX;

                        if (tmpObjDepartureFlight.EcoClassPAX != null)
                            TmpobjAllocationPlan.DPaxE = tmpObjDepartureFlight.EcoClassPAX;

                        TmpobjAllocationPlan.DGate = tmpObjDepartureFlight.GateOfDeparture1;

                        TmpobjAllocationPlan.DStand = tmpObjDepartureFlight.PositionOfDeparture;

                        TmpobjAllocationPlan.DS = tmpObjDepartureFlight.OperationalType;                       

                        //EntityCollectionBase<EntDbCheckInCounterAllocation> objCICAllocation = DlAllocationPlan.GetCheckInCounter(tmpObjDepartureFlight.Urno);

                        //foreach (EntDbCheckInCounterAllocation sResult in objCICAllocation)
                        //{
                        //if (sResult.CheckInCounter != null)
                        //if (string.IsNullOrEmpty(TmpobjAllocationPlan.Counter))
                        //TmpobjAllocationPlan.Counter = sResult.CheckInCounter;
                        //else
                        //TmpobjAllocationPlan.Counter = String.Format("{0}, {1}", TmpobjAllocationPlan.Counter, sResult.CheckInCounter);
                        //}

                        //objCICAllocation = null;
                        if (string.IsNullOrEmpty(tmpObjDepartureFlight.CheckInCFrom))
                        {
                            TmpobjAllocationPlan.Counter = tmpObjDepartureFlight.CheckInCTo;
                        }
                        else if (string.IsNullOrEmpty(tmpObjDepartureFlight.CheckInCTo))
                        {
                            TmpobjAllocationPlan.Counter = tmpObjDepartureFlight.CheckInCFrom;
                        }
                        else if (!string.IsNullOrEmpty(tmpObjDepartureFlight.CheckInCTo) && !string.IsNullOrEmpty(tmpObjDepartureFlight.CheckInCFrom))
                        {
                            TmpobjAllocationPlan.Counter = String.Format("{0}-{1}", tmpObjDepartureFlight.CheckInCFrom, tmpObjDepartureFlight.CheckInCTo);
                        }
                        else
                        {
                            TmpobjAllocationPlan.Counter = "";
                        }

                        TmpobjAllocationPlan.Type = tmpObjDepartureFlight.AircraftIATACode;

                        TmpobjAllocationPlan.Reg = tmpObjDepartureFlight.RegistrationNumber;
                        if (tmpObjDepartureFlight.GateOfArrival1 != null)
                            TmpobjAllocationPlan.Gate = tmpObjDepartureFlight.GateOfArrival1;

                        TmpobjAllocationPlan.Belt = tmpObjDepartureFlight.Belt1;
                        TmpobjAllocationPlan.NatureCode = tmpObjDepartureFlight.NatureCode;

                        objAllocationPlan.Add(TmpobjAllocationPlan);
                    }

                }
            }
        }
        /// <summary>
        /// Get Arrival Flight
        /// </summary>
        /// <returns>Return System.string</returns>
        private string GetArrivalRkeyList()
        {
            try
            {
                if (objArrivalFlights.Count > 0)
                {
                    foreach (EntDbFlight tmpArrivalFlights in objArrivalFlights)
                    {
                        entlcAllocationPlan TmpobjAllocationPlan = new entlcAllocationPlan();
                        {
                            if (objDepFlights != null)
                            {
                                EntDbFlight objDepartureFlight = objDepFlights.FirstOrDefault(d => d.RelationKey == tmpArrivalFlights.RelationKey && d.Urno != tmpArrivalFlights.Urno);

                                TmpobjAllocationPlan.Arrival = string.IsNullOrEmpty(tmpArrivalFlights.FullFlightNumber) ? tmpArrivalFlights.CallSign : tmpArrivalFlights.FullFlightNumber;
                                TmpobjAllocationPlan.From = tmpArrivalFlights.OriginAirportIATACode;
                                if (tmpArrivalFlights.StandardTimeOfArrival != null)
                                {
                                    date = tmpArrivalFlights.StandardTimeOfArrival; if (date.HasValue)
                                    {
                                        TmpobjAllocationPlan.STA = date.Value;
                                        //TmpobjAllocationPlan.SCOL = TmpobjAllocationPlan.STA;
                                    }
                                }
                                if (objDepartureFlight != null)
                                {
                                    try
                                    {
                                        _dicDep.Add(objDepartureFlight.Urno, objDepartureFlight.Urno);
                                    }
                                    catch (Exception)
                                    {
                                        //On error resume next                               
                                    }

                                    if (string.IsNullOrEmpty(objDepartureFlight.FullFlightNumber))
                                    {
                                        TmpobjAllocationPlan.Departure = objDepartureFlight.CallSign;
                                    }
                                    else
                                    {
                                        TmpobjAllocationPlan.Departure = objDepartureFlight.FullFlightNumber;
                                    }

                                    TmpobjAllocationPlan.TO = objDepartureFlight.DestinationAirportIATACode;

                                    if (objDepartureFlight.StandardTimeOfDeparture != null)
                                    {

                                        date = objDepartureFlight.StandardTimeOfDeparture; if (date.HasValue)
                                        {
                                            TmpobjAllocationPlan.STD = date.Value;                                            
                                        }

                                    }
                                    if (objDepartureFlight.TotalNumberOfPassengers != null)
                                        TmpobjAllocationPlan.DPax = objDepartureFlight.TotalNumberOfPassengers;

                                    if (objDepartureFlight.FirstClassPAX != null)
                                        TmpobjAllocationPlan.DPaxF = objDepartureFlight.FirstClassPAX;

                                    if (objDepartureFlight.BusinessClassPAX != null)
                                        TmpobjAllocationPlan.DPaxB = objDepartureFlight.BusinessClassPAX;

                                    if (objDepartureFlight.EcoClassPAX != null)
                                        TmpobjAllocationPlan.DPaxE = objDepartureFlight.EcoClassPAX;

                                    TmpobjAllocationPlan.DGate = objDepartureFlight.GateOfDeparture1;

                                    TmpobjAllocationPlan.DStand = objDepartureFlight.PositionOfDeparture;

                                    TmpobjAllocationPlan.DS = objDepartureFlight.OperationalType;

                                    //EntityCollectionBase<EntDbCheckInCounterAllocation> objCICAllocation = DlAllocationPlan.GetCheckInCounter(objDepartureFlight.Urno);

                                    //foreach (EntDbCheckInCounterAllocation sResult in objCICAllocation)
                                    //{
                                    //    if (sResult.CheckInCounter != null)
                                    //        if (string.IsNullOrEmpty(TmpobjAllocationPlan.Counter))
                                    //            TmpobjAllocationPlan.Counter = sResult.CheckInCounter;
                                    //        else
                                    //            TmpobjAllocationPlan.Counter = String.Format("{0}, {1}", TmpobjAllocationPlan.Counter, sResult.CheckInCounter);
                                    //}
                                    //objCICAllocation = null;
                                    if (string.IsNullOrEmpty(objDepartureFlight.CheckInCFrom))
                                    {
                                        TmpobjAllocationPlan.Counter = objDepartureFlight.CheckInCTo;
                                    }
                                    else if (string.IsNullOrEmpty(objDepartureFlight.CheckInCTo))
                                    {
                                        TmpobjAllocationPlan.Counter = objDepartureFlight.CheckInCFrom;
                                    }
                                    else if (!string.IsNullOrEmpty(objDepartureFlight.CheckInCTo) && !string.IsNullOrEmpty(objDepartureFlight.CheckInCFrom))
                                    {
                                        TmpobjAllocationPlan.Counter = String.Format("{0}-{1}", objDepartureFlight.CheckInCFrom, objDepartureFlight.CheckInCTo);
                                    }
                                    else
                                    {
                                        TmpobjAllocationPlan.Counter = "";
                                    }
                                }
                                #region comment
                                //if (objDepartureFlight.Count > 0)
                                //{                                
                                //    foreach (EntDbFlight qResult in objDepartureFlight)
                                //    {
                                //        if (string.IsNullOrEmpty(qResult.FullFlightNumber))
                                //        {
                                //            TmpobjAllocationPlan.Departure = qResult.CallSign;
                                //        }
                                //        else
                                //        {
                                //            TmpobjAllocationPlan.Departure = qResult.FullFlightNumber;
                                //        }

                                //        TmpobjAllocationPlan.TO = qResult.DestinationAirportIATACode;

                                //        if (qResult.StandardTimeOfDeparture !=null)
                                //            TmpobjAllocationPlan.STD = qResult.StandardTimeOfDeparture.Value.ToString("HHmm");                                    

                                //        TmpobjAllocationPlan.DPax = qResult.TotalNumberOfPassengers;

                                //        TmpobjAllocationPlan.DGate = qResult.GateOfDeparture1;

                                //        TmpobjAllocationPlan.DStand = qResult.PositionOfDeparture;

                                //        TmpobjAllocationPlan.DS = qResult.OperationalType;

                                //        EntityCollectionBase<EntDbCheckInCounterAllocation> objCICAllocation = DlAllocationPlan.GetCheckInCounter(qResult.Urno);

                                //        foreach (EntDbCheckInCounterAllocation sResult in objCICAllocation)
                                //        {
                                //            if (sResult.CheckInCounter!= null)
                                //                if (string.IsNullOrEmpty(TmpobjAllocationPlan.Counter))
                                //                    TmpobjAllocationPlan.Counter = sResult.CheckInCounter;
                                //                else
                                //                    TmpobjAllocationPlan.Counter = String.Format("{0},{1}", TmpobjAllocationPlan.Counter, sResult.CheckInCounter);
                                //        }
                                //        objCICAllocation = null;
                                //    }
                                //}
                                //else
                                //{
                                //    TmpobjAllocationPlan.Departure = "";
                                //    TmpobjAllocationPlan.TO = "";
                                //    TmpobjAllocationPlan.STD = "";                                
                                //}
                                #endregion
                                TmpobjAllocationPlan.Type = tmpArrivalFlights.AircraftIATACode;

                                TmpobjAllocationPlan.Reg = tmpArrivalFlights.RegistrationNumber;

                                if (tmpArrivalFlights.PositionOfArrival != null)
                                    TmpobjAllocationPlan.Stand = tmpArrivalFlights.PositionOfArrival;

                                if (tmpArrivalFlights.GateOfArrival1 != null)
                                    TmpobjAllocationPlan.Gate = tmpArrivalFlights.GateOfArrival1;

                                TmpobjAllocationPlan.Belt = tmpArrivalFlights.Belt1;

                                if (tmpArrivalFlights.TotalNumberOfPassengers != null)
                                    TmpobjAllocationPlan.APax = tmpArrivalFlights.TotalNumberOfPassengers;

                                if(tmpArrivalFlights.FirstClassPAX != null)
                                    TmpobjAllocationPlan.APaxF = tmpArrivalFlights.FirstClassPAX;

                                if(tmpArrivalFlights.BusinessClassPAX !=null)
                                    TmpobjAllocationPlan.APaxB = tmpArrivalFlights.BusinessClassPAX;

                                if(tmpArrivalFlights.EcoClassPAX != null)
                                    TmpobjAllocationPlan.APaxE = tmpArrivalFlights.EcoClassPAX;

                                TmpobjAllocationPlan.NatureCode = tmpArrivalFlights.NatureCode;

                                TmpobjAllocationPlan.AS = tmpArrivalFlights.OperationalType;

                                objDepartureFlight = null;
                            }
                        }
                        objAllocationPlan.Add(TmpobjAllocationPlan);
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }
        /// <summary>
        /// Get Parameter Value
        /// </summary>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetParameterValue()
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbParameter> objParameter = DlAllocationPlan.GetParameterValue("ID_APR_FT");

            if (objParameter != null)
            {
                foreach (EntDbParameter objTmpParameter in objParameter)
                {
                    sReturnValue = objTmpParameter.Value;
                }
            }
            return sReturnValue;
        }
        /// <summary>
        /// Get Login User Name
        /// </summary>
        /// <param name="USID">System.String containing USID</param>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetLoginUserName(string USID)
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbSecured> objUser = DlAllocationPlan.GetLoginUserName(USID);

            if (objUser != null)
            {
                foreach (EntDbSecured objTmpUser in objUser)
                {
                    sReturnValue = objTmpUser.Name;
                }
            }
            return sReturnValue;
        }
        #endregion

        #region +++ Properties +++
        public IList<entlcAllocationPlan> ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource != value)
                {
                    _itemSource = value;
                    OnPropertyChanged("ItemSource");
                }
            }
        }
        #endregion

        #region +++ Unuse code +++
        //private void GetDepartureFlightNoRKEYList()
        //{
        //    if (objDepFlights.Count > 0)
        //    {
        //        foreach (EntDbFlight tmpArrFlights in objArrivalFlights)
        //        {
        //            entlcAllocationPlan TmpobjAllocationPlan = new entlcAllocationPlan();
        //            {
        //                if (objDepFlights != null)
        //                {
        //                    EntDbFlight tmpNoRKeyDepFlight = objDepFlights.FirstOrDefault(s => s.RelationKey != tmpArrFlights.RelationKey && s.Urno != tmpArrFlights.Urno);

        //                    List<EntDbFlight> HistoryDepFlights = new List<EntDbFlight>();
        //                    HistoryDepFlights.Add(tmpNoRKeyDepFlight);
        //                    Boolean binsertFlag = false;

        //                    foreach (EntDbFlight hisdepflight in HistoryDepFlights)
        //                    {
        //                        if (hisdepflight.Urno == tmpNoRKeyDepFlight.Urno)
        //                            binsertFlag = false;
        //                        else
        //                            binsertFlag = true;
        //                    }
        //                    if (binsertFlag)
        //                    {
        //                        if (tmpNoRKeyDepFlight != null)
        //                        {
        //                            if (string.IsNullOrEmpty(tmpNoRKeyDepFlight.FullFlightNumber))
        //                            {
        //                                TmpobjAllocationPlan.Departure = tmpNoRKeyDepFlight.CallSign;
        //                            }
        //                            else
        //                            {
        //                                TmpobjAllocationPlan.Departure = tmpNoRKeyDepFlight.FullFlightNumber;
        //                            }

        //                            TmpobjAllocationPlan.TO = tmpNoRKeyDepFlight.DestinationAirportIATACode;

        //                            if (tmpNoRKeyDepFlight.StandardTimeOfDeparture != null)
        //                            {
        //                                //string day = tmpNoRKeyDepFlight.StandardTimeOfDeparture.Value.Date.ToString("dd/mm/yyyy").Substring(0,2);                                        
        //                                //TmpobjAllocationPlan.STD = String.Format("{0} / {1:HHmm}", day, tmpNoRKeyDepFlight.StandardTimeOfDeparture.Value);
        //                                date = tmpNoRKeyDepFlight.StandardTimeOfDeparture; if (date.HasValue)
        //                                {
        //                                    TmpobjAllocationPlan.STD = string.Format("{0:dd/HHmm}", date.Value);
        //                                    TmpobjAllocationPlan.SCOL = TmpobjAllocationPlan.STD;
        //                                }
        //                            }
        //                            TmpobjAllocationPlan.DPax = tmpNoRKeyDepFlight.TotalNumberOfPassengers;

        //                            TmpobjAllocationPlan.DGate = tmpNoRKeyDepFlight.GateOfDeparture1;

        //                            TmpobjAllocationPlan.DStand = tmpNoRKeyDepFlight.PositionOfDeparture;

        //                            TmpobjAllocationPlan.DS = tmpNoRKeyDepFlight.OperationalType;

        //                            EntityCollectionBase<EntDbCheckInCounterAllocation> objCICAllocation = DlAllocationPlan.GetCheckInCounter(tmpNoRKeyDepFlight.Urno);

        //                            foreach (EntDbCheckInCounterAllocation sResult in objCICAllocation)
        //                            {
        //                                if (sResult.CheckInCounter != null)
        //                                    if (string.IsNullOrEmpty(TmpobjAllocationPlan.Counter))
        //                                        TmpobjAllocationPlan.Counter = sResult.CheckInCounter;
        //                                    else
        //                                        TmpobjAllocationPlan.Counter = String.Format("{0},{1}", TmpobjAllocationPlan.Counter, sResult.CheckInCounter);
        //                            }
        //                            objCICAllocation = null;

        //                            TmpobjAllocationPlan.Type = tmpNoRKeyDepFlight.AircraftIATACode;

        //                            TmpobjAllocationPlan.Reg = tmpNoRKeyDepFlight.RegistrationNumber;
        //                            if (tmpNoRKeyDepFlight.GateOfArrival1 != null)
        //                                TmpobjAllocationPlan.Gate = tmpNoRKeyDepFlight.GateOfArrival1;

        //                            TmpobjAllocationPlan.Belt = tmpNoRKeyDepFlight.Belt1;
        //                            TmpobjAllocationPlan.NatureCode = tmpNoRKeyDepFlight.NatureCode;
        //                        }
        //                    }
        //                }

        //            }
        //            if (!string.IsNullOrEmpty(TmpobjAllocationPlan.Departure))
        //                objAllocationPlan.Add(TmpobjAllocationPlan);
        //        }

        //    }

        //}
        ///// <summary>
        ///// Arrival Flight in Time Range
        ///// </summary>
        //private void GetArrivalFlightNoRKEYList()
        //{
        //    if (objDepFlights.Count > 0)
        //    {
        //        foreach (EntDbFlight tmpDepartureFlights in objDepFlights)
        //        {
        //            entlcAllocationPlan TmpobjAllocationPlan = new entlcAllocationPlan();
        //            {
        //                if (objArrivalFlights != null)
        //                {
        //                    EntDbFlight tmpNoRKeyArrFlight = objArrivalFlights.FirstOrDefault(s => s.RelationKey != tmpDepartureFlights.RelationKey && s.Urno != tmpDepartureFlights.Urno);
        //                    List<EntDbFlight> HistoryArrFlights = new List<EntDbFlight>();
        //                    HistoryArrFlights.Add(tmpNoRKeyArrFlight);
        //                    Boolean binsertFlag = false;

        //                    foreach (EntDbFlight hisarrflight in HistoryArrFlights)
        //                    {
        //                        if (hisarrflight.Urno == tmpNoRKeyArrFlight.Urno)
        //                            binsertFlag = false;
        //                        else
        //                            binsertFlag = true;
        //                    }
        //                    if (binsertFlag)
        //                    {
        //                        if (string.IsNullOrEmpty(tmpNoRKeyArrFlight.FullFlightNumber))
        //                        {
        //                            TmpobjAllocationPlan.Arrival = tmpNoRKeyArrFlight.CallSign;
        //                        }
        //                        else
        //                        {
        //                            TmpobjAllocationPlan.Arrival = tmpNoRKeyArrFlight.FullFlightNumber;
        //                        }
        //                        TmpobjAllocationPlan.From = tmpNoRKeyArrFlight.OriginAirportIATACode;
        //                        if (tmpNoRKeyArrFlight.StandardTimeOfArrival != null) 
        //                        {
        //                            DateTime? date = tmpNoRKeyArrFlight.StandardTimeOfDeparture;
        //                            string day = date.Value.Date.ToString("dd/mm/yyyy").Substring(0, 2);
        //                            if (!string.IsNullOrEmpty(day))
        //                                TmpobjAllocationPlan.STA = String.Format("{0} / {1:HHmm}", day, tmpNoRKeyArrFlight.StandardTimeOfArrival.Value);
        //                        }
        //                        TmpobjAllocationPlan.Type = tmpNoRKeyArrFlight.AircraftIATACode;

        //                        TmpobjAllocationPlan.Reg = tmpNoRKeyArrFlight.RegistrationNumber;
        //                        //Need to check for if data only have departure
        //                        if (tmpNoRKeyArrFlight.PositionOfArrival != null)
        //                            TmpobjAllocationPlan.Stand = tmpNoRKeyArrFlight.PositionOfArrival;

        //                        if (tmpNoRKeyArrFlight.GateOfArrival1 != null)
        //                            TmpobjAllocationPlan.Gate = tmpNoRKeyArrFlight.GateOfArrival1;

        //                        TmpobjAllocationPlan.Belt = tmpNoRKeyArrFlight.Belt1;
        //                        if (tmpNoRKeyArrFlight.TotalNumberOfPassengers != null)
        //                            TmpobjAllocationPlan.APax = tmpNoRKeyArrFlight.TotalNumberOfPassengers;


        //                        TmpobjAllocationPlan.NatureCode = tmpNoRKeyArrFlight.NatureCode;

        //                        TmpobjAllocationPlan.AS = tmpNoRKeyArrFlight.OperationalType;
        //                    }
        //                }
        //            }
        //            if (!string.IsNullOrEmpty(TmpobjAllocationPlan.Arrival))
        //                objAllocationPlan.Add(TmpobjAllocationPlan); 
        //        }

        //    }

        //}
        /// <summary>
        /// Get Rotation Flights
        /// </summary>
        /// <returns></returns>
        #endregion

    }
}
