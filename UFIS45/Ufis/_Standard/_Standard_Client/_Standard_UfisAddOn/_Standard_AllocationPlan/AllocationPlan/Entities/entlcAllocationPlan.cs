﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : entlcAllocationPlan.cs

Version         : 1.0.0
Created Date    : 16 - Feb - 2012
Complete Date   : 16 - Feb - 2012
Created By      : Phyoe Khaing Min
 * 
 * 
 * Updated On       :   04-September-2012
 * Updated By       :   Phyoe Khaing Min
 * Update Detial    :   Change the String Data Type of STA and STD to Nullable DateTime

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;

namespace Ufis.AllocationPlan.Entities
{
    public class entlcAllocationPlan : Entity
    {
        private string _Arrival = "";
        public string Arrival
        {
            get { return _Arrival; }
            set
            {
                if (String.Compare(_Arrival, value, false) == 0)
                    return;

                _Arrival = value;
                OnPropertyChanged("Arrival");
            }
        }
        private string _From = "";
        public string From
        {
            get { return _From; }
            set
            {
                if (String.Compare(_From, value, false) == 0)
                    return;

                _From = value;
                OnPropertyChanged("From");
            }
        }
        private DateTime? _STA;
        public DateTime? STA
        {
            get { return _STA; }
            set
            {
                if (_STA != value)
                {

                    _STA = value;
                    OnPropertyChanged("STA");
                }
            }
        }
        private string _SCOL = "";
        public string SCOL
        {
            get { return _SCOL; }
            set
            {
                if (String.Compare(_SCOL, value, false) == 0)
                    return;

                _SCOL = value;
                OnPropertyChanged("SCOL");
            }
        }
        private string _Departure = "";
        public string Departure
        {
            get { return _Departure; }
            set
            {
                if (String.Compare(_Departure, value, false) == 0)
                    return;

                _Departure = value;
                OnPropertyChanged("Departure");
            }
        }
        private DateTime? _STD ;
        public DateTime? STD
        {
            get { return _STD; }
            set
            {
                if (_STD != value)
                {

                    _STD = value;
                    OnPropertyChanged("STD");
                }
            }
        }
        private string _To = "";
        public string TO
        {
            get { return _To; }
            set
            {
                if (String.Compare(_To, value, false) == 0)
                    return;

                _To = value;
                OnPropertyChanged("TO");
            }
        }
        private string _NatureCode = "";
        public string NatureCode
        {
            get { return _NatureCode; }
            set
            {
                if (String.Compare(_NatureCode, value, false) == 0)
                    return;

                _NatureCode = value;
                OnPropertyChanged("NatureCode");
            } 
        }
        private string _Type = "";
        public string Type
        {
            get { return _Type; }
            set
            {
                if (String.Compare(_Type, value, false) == 0)
                    return;

                _Type = value;
                OnPropertyChanged("Type");
            }
        }
        private string _Reg = "";
        public string Reg
        {
            get { return _Reg; }
            set
            {
                if (String.Compare(_Reg, value, false) == 0)
                    return;

                _Reg = value;
                OnPropertyChanged("Reg");
            }
        }
        private string _Stand = "";
        public string Stand
        {
            get { return _Stand; }
            set
            {
                if (String.Compare(_Stand, value, false) == 0)
                    return;

                _Stand = value;
                OnPropertyChanged("Stand");
            }
        }
        private string _DStand = "";
        public string DStand
        {
            get { return _DStand; }
            set
            {
                if (String.Compare(_DStand, value, false) == 0)
                    return;

                _DStand = value;
                OnPropertyChanged("DStand");
            }
        }
        private string _Counter = "";
        public string Counter
        {
            get { return _Counter; }
            set
            {
                if (String.Compare(_Counter, value, false) == 0)
                    return;

                _Counter = value;
                OnPropertyChanged("Counter");
            }
        }
        private string _Gate = "";
        public string Gate
        {
            get { return _Gate; }
            set
            {
                if (String.Compare(_Gate, value, false) == 0)
                    return;

                _Gate = value;
                OnPropertyChanged("Gate");
            }
        }
        private string _DGate = "";
        public string DGate
        {
            get { return _DGate; }
            set
            {
                if (String.Compare(_DGate, value, false) == 0)
                    return;

                _DGate = value;
                OnPropertyChanged("DGate");
            }
        }
        private string _Belt = "";
        public string Belt
        {
            get { return _Belt; }
            set
            {
                if (String.Compare(_Belt, value, false) == 0)
                    return;

                _Belt = value;
                OnPropertyChanged("Belt");
            }
        }
        private int? _APax ;        
        public int? APax
        {
            get { return _APax; }
            set
            {
                if (_APax == value)
                    return;

                _APax = value;
                OnPropertyChanged("APax");
            }
        }
        private int? _DPax ;
        public int? DPax
        {
            get { return _DPax; }
            set
            {
                if (_DPax == value)
                    return;

                _DPax = value;
                OnPropertyChanged("DPax");
            }
        }
        private string _AS = "";
        public string AS
        {
            get { return _AS; }
            set
            {
                if (String.Compare(_AS, value, false) == 0)
                    return;

                _AS = value;
                OnPropertyChanged("AS");
            } 
        }
        private string _DS = "";
        public string DS
        {
            get { return _DS; }
            set
            {
                if (String.Compare(_DS, value, false) == 0)
                    return;

                _DS = value;
                OnPropertyChanged("DS");
            }
        }

        private int? _APaxF;

        public int? APaxF
        {
            get { return _APaxF; }
            set
            {
                if (_APaxF == value)
                    return;

                _APaxF = value;
                OnPropertyChanged("APaxF");
            }
        }
        private int? _APaxB;

        public int? APaxB
        {
            get { return _APaxB; }
            set
            {
                if (_APaxB == value)
                    return;

                _APaxB = value;
                OnPropertyChanged("APaxB");
            }
        }
        private int? _APaxE;

        public int? APaxE
        {
            get { return _APaxE; }
            set
            {
                if (_APaxE == value)
                    return;

                _APaxE = value;
                OnPropertyChanged("APaxE");
            }
        }


        private int? _DPaxF;

        public int? DPaxF
        {
            get { return _DPaxF; }
            set
            {
                if (_DPaxF == value)
                    return;

                _DPaxF = value;
                OnPropertyChanged("DPaxF");
            }
        }
        private int? _DPaxB;

        public int? DPaxB
        {
            get { return _DPaxB; }
            set
            {
                if (_DPaxB == value)
                    return;

                _DPaxB = value;
                OnPropertyChanged("DPaxB");
            }
        }
        private int? _DPaxE;

        public int? DPaxE
        {
            get { return _DPaxE; }
            set
            {
                if (_DPaxE == value)
                    return;

                _DPaxE = value;
                OnPropertyChanged("DPaxE");
            }
        }
    }
}
