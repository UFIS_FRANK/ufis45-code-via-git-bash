using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Ufis.Data
{
	/// <summary>
	/// The dummy form is a simple container to embedd all needed Ufis ActiveX components 
	/// in a single instance. These components are used by <see cref="IDatabaseImp"/>.
	/// </summary>
	/// <remarks>none.</remarks>
	internal class Dummy : System.Windows.Forms.Form
	{
		/// <summary>
		/// The <B>one and only</B> instance of the UCom component in the application.
		/// </summary>
		/// <remarks>none.</remarks>
		public AxUCOMLib.AxUCom axUCom1;
		/// <summary>
		/// The <B>one and only</B> instance of the UfisCom component in the application.
		/// </summary>
		/// <remarks>none.</remarks>
		public AxUFISCOMLib.AxUfisCom axUfisCom1;
		/// <summary>
		/// The instance of the Tab.ocx, which is necessary to get the version and build date
		/// for the about box: <see cref="Ufis.Utils.frmAbout"/>
		/// </summary>
		/// <remarks>none.</remarks>
		public AxTABLib.AxTAB axTAB1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		/// <remarks>none.</remarks>
		private System.ComponentModel.Container components = null;
		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <remarks>none.</remarks>
		public Dummy()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <remarks>none.</remarks>
		/// <param name="disposing">
		/// true to release both managed and unmanaged resources; false to release 
		/// only unmanaged resources.
		/// </param>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Dummy));
			this.axUCom1 = new AxUCOMLib.AxUCom();
			this.axUfisCom1 = new AxUFISCOMLib.AxUfisCom();
			this.axTAB1 = new AxTABLib.AxTAB();
			((System.ComponentModel.ISupportInitialize)(this.axUCom1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.axUfisCom1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.axTAB1)).BeginInit();
			this.SuspendLayout();
			// 
			// axUCom1
			// 
			this.axUCom1.Enabled = true;
			this.axUCom1.Location = new System.Drawing.Point(80, 16);
			this.axUCom1.Name = "axUCom1";
			this.axUCom1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axUCom1.OcxState")));
			this.axUCom1.Size = new System.Drawing.Size(100, 50);
			this.axUCom1.TabIndex = 0;
			// 
			// axUfisCom1
			// 
			this.axUfisCom1.Enabled = true;
			this.axUfisCom1.Location = new System.Drawing.Point(80, 80);
			this.axUfisCom1.Name = "axUfisCom1";
			this.axUfisCom1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axUfisCom1.OcxState")));
			this.axUfisCom1.Size = new System.Drawing.Size(100, 50);
			this.axUfisCom1.TabIndex = 1;
			// 
			// axTAB1
			// 
			this.axTAB1.Location = new System.Drawing.Point(80, 144);
			this.axTAB1.Name = "axTAB1";
			this.axTAB1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTAB1.OcxState")));
			this.axTAB1.Size = new System.Drawing.Size(100, 50);
			this.axTAB1.TabIndex = 2;
			// 
			// Dummy
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 266);
			this.Controls.Add(this.axTAB1);
			this.Controls.Add(this.axUfisCom1);
			this.Controls.Add(this.axUCom1);
			this.Name = "Dummy";
			((System.ComponentModel.ISupportInitialize)(this.axUCom1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.axUfisCom1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.axTAB1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Dummy_Load(object sender, System.EventArgs e)
		{
		
		}

		private void Dummy_Load_1(object sender, System.EventArgs e)
		{
		
		}
	}
}
