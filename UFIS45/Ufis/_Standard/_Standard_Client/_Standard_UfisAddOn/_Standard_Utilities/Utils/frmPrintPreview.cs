using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Export.Pdf;
using DataDynamics.ActiveReports.Export.Xls;


namespace Ufis.Utils
{
	/// <summary>
	/// The frmPrintPreview provides a windows form to display the Active Reports generated
	/// print preview. This form can be displayed in the same way as a regular windows form.
	/// The form contains an "Excel" and a "PDF" button in order to give the user the chance 
	/// to export the print content to an Excel sheet or and Acrobat Reader document format.
	/// </summary>
	/// <remarks>
	/// To use this form the Active Report development environment must be installed.
	/// </remarks>
	/// <example>
	/// The usage of this form is according to the regular windows form startup. You can
	/// decide to show the form modal or modeless according to the application's needs.
	/// <code>
	///	[C#]
	///	// The report must be defined by the help of the Active Reports designer, which
	///	// generates a class, e.g.:
	///	// public class rptStatusOverview : ActiveReport
	///	// In this case the rptStatusOverview constructor get a Tab.ocx reference, which
	///	// contains the data to be printed. It's your decision how to pride the data for
	///	// the report, this is independent from the frmPrintPreview.
	///	private void PrintMyReport()
	///	{
	///		// Should activate the wait cursor for the case of time consuming reports.
	///		this.Cursor = Cursors.WaitCursor;
	///		// rptStatusOverview must be derived from ActiveReports base class
	///		// tabList is the data grid (the type is Tab.ocx) as data source
	///		// for the report.
	///		rptStatusOverview rpt = new rptStatusOverview(tabList, CFC.CurrentViewName);
	///		frmPrintPreview preView = new frmPrintPreview(rpt);
	///		preView.Show();		//alternative: preView.ShowDialog(this); //to show modal
	///		// Revoke the wait cursor.
	///		this.Cursor = Cursors.Arrow;
	/// }
	/// </code>
	/// </example>
	public class frmPrintPreview : System.Windows.Forms.Form
	{
		private ActiveReport3 myReport = null;
		private DataDynamics.ActiveReports.Viewer.Viewer viewer1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.PictureBox picToolBar;
		private System.Windows.Forms.Button btnExcelExport;
		private System.Windows.Forms.Button btnPdfExport;
		private System.Windows.Forms.ImageList imageButtons;
		private DataDynamics.ActiveReports.Export.Html.HtmlExport htmlExport1;
		private DataDynamics.ActiveReports.Export.Pdf.PdfExport pdfExport1;
		private DataDynamics.ActiveReports.Export.Rtf.RtfExport rtfExport1;
		private DataDynamics.ActiveReports.Export.Text.TextExport textExport1;
		private DataDynamics.ActiveReports.Export.Xls.XlsExport xlsExport1;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Button btnTextExport;
		private float zoompercent = 0.8f;
		private bool bmUseSpreadBuilder = false;

		/// <summary>
		/// Constructor. 
		/// </summary>
		/// <param name="theReport">A reference to a ActiveReport derived instance,
		/// which is to be previewed.</param>
		/// <remarks>
		/// The report must be predefined and the report must handle the data to
		/// be previewed on his own.
		/// </remarks>
		public frmPrintPreview(ActiveReport3 theReport)
		{
			myReport = theReport;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// This property is responsible for the zooming factor when the report starts up.
		/// The default value is 0.8f.
		/// </summary>
		/// <remarks>
		/// The zoom factor depends on the screen setting, so you should not confuse the user
		/// by a too small or a too big factor. The factor is the percentage 0.5 = 50% 1.0 is 100%.
		/// </remarks>
		public float ZoomPercent
		{
			get{return zoompercent;}
			set{zoompercent = value;}
		}
		
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">Threaded indicator.</param>
		/// <remarks>none.</remarks>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmPrintPreview));
			this.viewer1 = new DataDynamics.ActiveReports.Viewer.Viewer();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnTextExport = new System.Windows.Forms.Button();
			this.imageButtons = new System.Windows.Forms.ImageList(this.components);
			this.btnPdfExport = new System.Windows.Forms.Button();
			this.btnExcelExport = new System.Windows.Forms.Button();
			this.picToolBar = new System.Windows.Forms.PictureBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.htmlExport1 = new DataDynamics.ActiveReports.Export.Html.HtmlExport();
			this.pdfExport1 = new DataDynamics.ActiveReports.Export.Pdf.PdfExport();
			this.rtfExport1 = new DataDynamics.ActiveReports.Export.Rtf.RtfExport();
			this.textExport1 = new DataDynamics.ActiveReports.Export.Text.TextExport();
			this.xlsExport1 = new DataDynamics.ActiveReports.Export.Xls.XlsExport();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// viewer1
			// 
			this.viewer1.BackColor = System.Drawing.SystemColors.Control;
			this.viewer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.viewer1.Location = new System.Drawing.Point(0, 0);
			this.viewer1.Name = "viewer1";
			this.viewer1.ReportViewer.CurrentPage = 0;
			this.viewer1.ReportViewer.MultiplePageCols = 3;
			this.viewer1.ReportViewer.MultiplePageRows = 2;
			this.viewer1.Size = new System.Drawing.Size(912, 558);
			this.viewer1.TabIndex = 0;
			this.viewer1.TableOfContents.Text = "Contents";
			this.viewer1.TableOfContents.Width = 200;
			this.viewer1.Toolbar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel1.Controls.Add(this.btnTextExport);
			this.panel1.Controls.Add(this.btnPdfExport);
			this.panel1.Controls.Add(this.btnExcelExport);
			this.panel1.Controls.Add(this.picToolBar);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(912, 40);
			this.panel1.TabIndex = 1;
			// 
			// btnTextExport
			// 
			this.btnTextExport.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnTextExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnTextExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnTextExport.ImageIndex = 2;
			this.btnTextExport.ImageList = this.imageButtons;
			this.btnTextExport.Location = new System.Drawing.Point(240, 0);
			this.btnTextExport.Name = "btnTextExport";
			this.btnTextExport.Size = new System.Drawing.Size(120, 36);
			this.btnTextExport.TabIndex = 3;
			this.btnTextExport.Text = "&Text-Export";
			this.btnTextExport.Click += new System.EventHandler(this.btnTextExport_Click);
			// 
			// imageButtons
			// 
			this.imageButtons.ImageSize = new System.Drawing.Size(19, 19);
			this.imageButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageButtons.ImageStream")));
			this.imageButtons.TransparentColor = System.Drawing.Color.White;
			// 
			// btnPdfExport
			// 
			this.btnPdfExport.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnPdfExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnPdfExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnPdfExport.ImageIndex = 1;
			this.btnPdfExport.ImageList = this.imageButtons;
			this.btnPdfExport.Location = new System.Drawing.Point(120, 0);
			this.btnPdfExport.Name = "btnPdfExport";
			this.btnPdfExport.Size = new System.Drawing.Size(120, 36);
			this.btnPdfExport.TabIndex = 2;
			this.btnPdfExport.Text = "&PDF-Export";
			this.btnPdfExport.Click += new System.EventHandler(this.btnPdfExport_Click);
			// 
			// btnExcelExport
			// 
			this.btnExcelExport.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnExcelExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnExcelExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnExcelExport.ImageIndex = 0;
			this.btnExcelExport.ImageList = this.imageButtons;
			this.btnExcelExport.Location = new System.Drawing.Point(0, 0);
			this.btnExcelExport.Name = "btnExcelExport";
			this.btnExcelExport.Size = new System.Drawing.Size(120, 36);
			this.btnExcelExport.TabIndex = 1;
			this.btnExcelExport.Text = "&Excel-Export";
			this.btnExcelExport.Click += new System.EventHandler(this.btnExcelExport_Click);
			// 
			// picToolBar
			// 
			this.picToolBar.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picToolBar.Location = new System.Drawing.Point(0, 0);
			this.picToolBar.Name = "picToolBar";
			this.picToolBar.Size = new System.Drawing.Size(908, 36);
			this.picToolBar.TabIndex = 0;
			this.picToolBar.TabStop = false;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.viewer1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 40);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(912, 558);
			this.panel2.TabIndex = 2;
			// 
			// pdfExport1
			// 
			this.pdfExport1.ImageResolution = 0;
			this.pdfExport1.NeverEmbedFonts = "Arial;Courier New;Times New Roman";
			//this.pdfExport1.Version = DataDynamics.ActiveReports.Export.Pdf.PDFVersion.PDF13;
			// 
			// textExport1
			// 
			this.textExport1.Encoding = ((System.Text.Encoding)(resources.GetObject("textExport1.Encoding")));
			// 
			// frmPrintPreview
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(912, 598);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmPrintPreview";
			this.Text = "Print Preview ...";
			this.Load += new System.EventHandler(this.frmPreview_Load);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmPreview_Load(object sender, System.EventArgs e)
		{
			if(myReport != null)
			{
				viewer1.ReportViewer.Zoom = zoompercent;
				//rptRules r = new rptRules();
				viewer1.Document = myReport.Document;
				myReport.Run();
			}
		}

		private void btnExcelExport_Click(object sender, System.EventArgs e)
		{
			try
			{	
				if(bmUseSpreadBuilder == false)
				{
					string fileName = "";
					SaveFileDialog dlg = new SaveFileDialog();
					dlg.Filter = "Excel-files (*.xls)|*.xls|All filed (*.*)|*.*";
					if(dlg.ShowDialog() == DialogResult.OK)
					{					
						Ufis.Utils.IniFile ini = new IniFile("C:\\Ufis\\System\\Ceda.ini");
						string strExcelPdfSame = "TRUE";
						strExcelPdfSame = ini.IniReadValue("FIPSREPORT", "EXCELANDPDFSAME");
						if(strExcelPdfSame.Equals("FALSE") == true && myReport.Sections["ReportHeader"] != null
							&& myReport.Sections["PageHeader"] != null && myReport.Sections["ReportHeader"].Visible == false)
						{
							myReport.Sections["ReportHeader"].Visible = true;
							myReport.Sections["ReportFooter"].Visible = true;
							myReport.Sections["PageHeader"].Visible = false;
							myReport.Sections["PageFooter"].Visible = false;
							myReport.Restart();
							myReport.Run();						
						}

						fileName = dlg.FileName;
						DataDynamics.ActiveReports.Export.Xls.XlsExport myXLS = new DataDynamics.ActiveReports.Export.Xls.XlsExport();					
						myXLS.RemoveVerticalSpace = true;
						myXLS.UseCellMerging = true;
						if(myReport.Sections["ReportHeader"] != null && myReport.Sections["ReportHeader"].Visible == true)
						{
							myXLS.AutoRowHeight = true;
						}
						myXLS.Export(myReport.Document, dlg.FileName);
					
						if(strExcelPdfSame.Equals("FALSE") == true&& myReport.Sections["ReportHeader"] != null
							&& myReport.Sections["PageHeader"] != null  && myReport.Sections["ReportHeader"].Visible == true)
						{
							myReport.Sections["ReportHeader"].Visible = false;
							myReport.Sections["ReportFooter"].Visible = false;
							myReport.Sections["PageHeader"].Visible = true;
							myReport.Sections["PageFooter"].Visible = true;
							myReport.Restart();
							myReport.Run();
						}
					}
				}
			}
			catch (Exception exx)
			{
				MessageBox.Show(this, "Error Creating Excel file.\n"+exx.ToString());
			}	
		}

		private void btnPdfExport_Click(object sender, System.EventArgs e)
		{
			DataDynamics.ActiveReports.Export.Pdf.PdfExport xPDF = new DataDynamics.ActiveReports.Export.Pdf.PdfExport();
			try
			{
				string fileName = "";
				SaveFileDialog dlg = new SaveFileDialog();
				dlg.Filter = "PDF-files (*.pdf)|*.pdf|All filed (*.*)|*.*";
				if(dlg.ShowDialog() == DialogResult.OK)
				{
					fileName = dlg.FileName;
					xPDF.Export(myReport.Document, fileName);
				}
			}
			catch (Exception exx)
			{
				MessageBox.Show(this, "Error Creating PDF file.\n"+exx.ToString());
			}	
		}

		private void btnTextExport_Click(object sender, System.EventArgs e)
		{
			DataDynamics.ActiveReports.Export.Text.TextExport xText = new DataDynamics.ActiveReports.Export.Text.TextExport();
			try
			{				
				Ufis.Utils.IniFile ini = new IniFile("C:\\Ufis\\System\\Ceda.ini");
				string strExcelSeparator = "";
				strExcelSeparator = ini.IniReadValue("FIPSREPORT", "EXCELSEPARATOR");
				if(strExcelSeparator.Length > 1)
				{
					if(strExcelSeparator.Equals("TAB") == true || 
					   strExcelSeparator.Equals("tab") == true ||
					   strExcelSeparator.Equals(@"\t") == true)						
					{
						strExcelSeparator = "\t";
					}
				}

				string fileName = "";
				SaveFileDialog dlg = new SaveFileDialog();
				dlg.Filter = "Text-files (*.txt)|*.txt|All filed (*.*)|*.*";
				if(dlg.ShowDialog() == DialogResult.OK)
				{
					fileName = dlg.FileName;
					if(strExcelSeparator.Length > 0)
					{
						xText.TextDelimiter = strExcelSeparator;
					}
                    /*
					myReport.Sections["ReportHeader"].Visible = true;
					myReport.Sections["ReportFooter"].Visible = true;
					myReport.Sections["PageHeader"].Visible = false;
					myReport.Sections["PageFooter"].Visible = false;					
					myReport.Run();	
                     */
					xText.Export(myReport.Document, fileName);
                    /*
					myReport.Sections["ReportHeader"].Visible = false;
					myReport.Sections["ReportFooter"].Visible = false;
					myReport.Sections["PageHeader"].Visible = true;
					myReport.Sections["PageFooter"].Visible = true;					
					myReport.Run();	
                     */
				}
			}
			catch (Exception exx)
			{
				MessageBox.Show(this, "Error Creating Text file.\n"+exx.ToString());
			}	
		
		}

		/// <summary>
		/// Sets the bool to decide whether to use default excel export or use SpreadBuilder APIs
		/// </summary>
		public void UseSpreadBuilder(bool bpUseSpreadBuilderForExcelExport)
		{
			bmUseSpreadBuilder = bpUseSpreadBuilderForExcelExport;
		}


		/// <summary>
		/// Returns the ExcelExport button
		/// </summary>
		public System.Windows.Forms.Button GetBtnExcelExport()
		{
			return btnExcelExport;
		}
	}
}
