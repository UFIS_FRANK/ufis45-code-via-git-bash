using System;
using System.Collections;

namespace Ufis.Data
{
	/// <summary>
	/// This delegate will be called by the framework for all "insert" 
	/// and "update" broadcasts to decide whether to process the broadcast or not.
	/// </summary>
	/// <param name="pReqId">User ID</param>
	/// <param name="pDest1">TO DO</param>
	/// <param name="pDest2">TO DO</param>
	/// <param name="pCmd">Router command</param>
	/// <param name="pObject">Database table name</param>
	/// <param name="pSeq">TO DO</param>
	/// <param name="pTws">Application relevant field</param>
	/// <param name="pTwe">User,Wks,application</param>
	/// <param name="pSelection">SQL Where statement</param>
	/// <param name="pFields">Database table fields.</param>
	/// <param name="pData">Data</param>
	/// <param name="pBcNum">Broadcast number</param>
	/// <returns>Bool value, which indicates whether this broadcast has to be processed by Ufis.Data.</returns>
	/// <remarks>
	/// The application can register for this delegate to decide whether a broadcast 
	/// has to be processed. This is reasonable for a FIPS application, which has
	/// loaded a certain time frame, so that all broadcasts for flight data, which are
	/// not within this time frame will not be taken into account. For this purpose the
	/// delegate is sent and the application decides via return value, if this broadcast 
	/// is within the time frame or not.
	/// <P></P><P></P><B>NOTE:</B><P></P><P></P>
	/// There is a multi level mechanism to incorporate broadcasts. The following cases
	/// or a combination of the cases must be taken into account for application 
	/// development:
	/// <P></P>
	/// <list type="bullet">
	///		<listheader>
	///			<description>ajksjljk</description>
	///		</listheader>
	///		<item>
	///			Automatically done by <see cref="IDatabase"/> and <see cref="ITable"/>.
	///			This is the case for table oriented broadcasts and no specific further
	///			methods are defined by the application developer to care about the broadcasts.
	///			The broadcast data will be incorporated into the respective <B>ITable's</B> rows.
	///			In this case the application will get no notification that a broadcast occured.
	///			This might be reasonable for basic data, which have to be looked up or presented
	///			in choice list but not more.
	///		</item>
	///		<item>
	///			A braodcast was received, which has nothing to do with a specific ITable
	///			object, but the application has to react nevertheless on that broadcast. For this
	///			purpose the <see cref="DatabaseEventHandler"/> delegate is called. You must 
	///			register for this delegate and write an <B>event method</B> to get the braodcast
	///			information as <see cref="DatabaseEventArgs"/> parameter to decide the further
	///			processing. This is useful for "SBC" commands and the "CLO" command, which are
	///			independent from database tables. This delegate is produced from the 
	///			<see cref="IDatabase"/> instance.
	///		</item>
	///		<item>
	///			A broadcast was received for a table. This table contains 1.000.000 records. 
	///			Normally you decide to load just a subset of all records following a certain
	///			criteria (SQL where clause), meaning that you are insterested in broadcasts that
	///			fit this criteria. The Ufis.Data namespace class instances cannot decide for you
	///			which data is relevant for the application. In order to make the decision, if a
	///			broadcast is interesting for the application we need some kind of callback 
	///			mechanism. The <see cref="ITable.OnUpdateRequestHandler"/> and the delegate <see cref="UpdateRequestHandler"/>
	///			handle the callback structure. You application code must register for this
	///			delegate to get the information which current broadcast is received. The 
	///			<B>event method</B> must examinate the content of the broadcast and 
	///			<B>return true or false</B>
	///			to inform the <B>ITable</B> object about to consider it in or not.
	///			Usually you application had loaded flight data for a certain time frame. This
	///			is the way to test, if the broadcast's data fit into this time frame or not.
	///		</item>
	///		<item>
	///			A table sends a delegate that it has processed a broadcast and stored the data
	///			to the inner list of <see cref="IRow"/>s. Your applicationo must register for
	///			the <see cref="DatabaseTableEventHandler"/> delegate event to get the information
	///			about the new data.
	///		</item>
	/// </list>
	/// </remarks>
	public delegate bool UpdateRequestHandler(string pReqId, string pDest1, string pDest2,string pCmd, string pObject, string pSeq,string pTws, string pTwe, string pSelection, string pFields, string pData, string pBcNum);

	/// <summary>
	/// ITable is an interface and implemented in <see cref="ITableImp"/> class.
	/// The application developer may not use the ITableImp class directly, because
	/// this interface provides all necessary interfaces for development requirements.
	/// </summary>
	/// <remarks>
	/// All classes in the namespace Ufis.Data handle ITable objects for external application
	/// concerns. An ITable instance is never directly created by the application. It uses always
	/// the <see cref="IDatabase.Bind"/> method, which returns a valid ITable instance.
	/// </remarks>
	/// <example>
	/// This example demonstrates the correct instantiation of an ITable object.
	/// <code>
	/// [C#]
	/// private ITable CreateAdressesTable()
	/// {
	///		IDatabase myDB = UT.GetMemDB();
	///		ITable myAdresses = myDBmyDB.Bind("Adresses", "CADR", "ID,Name,Zip,City", "10,32,10,32", "ID,Name,Zip,City");
	///		return myAdresses;
	/// }
	/// </code>
	/// </example>
	public interface ITable
	{
		/// <summary>
		/// Returns whether this instance is syncronized or not.
		/// </summary>
		/// <remarks>none</remarks>
		bool IsSyncronized
		{
			get;
		}

		/// <summary>
		/// Returns the syncronized instance of this instance.
		/// </summary>
		/// <remarks>none</remarks>
		object	SyncRoot
		{
			get;
		}

		/// <summary>
		/// Returns the locker for this instance.
		/// </summary>
		/// <remarks>none</remarks>
		System.Threading.ReaderWriterLock	Lock
		{
			get;
		}

		/// <summary>
		/// Register or unregister an application specific 
		/// handler for the UpdateRequestHandler delegate
		/// </summary>
		/// <remarks>
		/// This delegate must be registered for the case that the application must
		/// make the decision whether a broadcast has to be processes or not. For most
		/// cases the application
		/// <P></P><P></P><B>NOTE:</B><P></P><P></P>
		/// There is a multi level mechanism to incorporate broadcasts. The following cases
		/// or a combination of the cases must be taken into account for application 
		/// development:
		/// <P></P>
		/// <list type="bullet">
		///		<listheader>
		///			<description>ajksjljk</description>
		///		</listheader>
		///		<item>
		///			Automatically done by <see cref="IDatabase"/> and <see cref="ITable"/>.
		///			This is the case for table oriented broadcasts and no specific further
		///			methods are defined by the application developer to care about the broadcasts.
		///			The broadcast data will be incorporated into the respective <B>ITable's</B> rows.
		///			In this case the application will get no notification that a broadcast occured.
		///			This might be reasonable for basic data, which have to be looked up or presented
		///			in choice list but not more.
		///		</item>
		///		<item>
		///			A braodcast was received, which has nothing to do with a specific ITable
		///			object, but the application has to react nevertheless on that broadcast. For this
		///			purpose the <see cref="DatabaseEventHandler"/> delegate is called. You must 
		///			register for this delegate and write an <B>event method</B> to get the braodcast
		///			information as <see cref="DatabaseEventArgs"/> parameter to decide the further
		///			processing. This is useful for "SBC" commands and the "CLO" command, which are
		///			independent from database tables. This delegate is produced from the 
		///			<see cref="IDatabase"/> instance.
		///		</item>
		///		<item>
		///			A broadcast was received for a table. This table contains 1.000.000 records. 
		///			Normally you decide to load just a subset of all records following a certain
		///			criteria (SQL where clause), meaning that you are insterested in broadcasts that
		///			fit this criteria. The Ufis.Data namespace class instances cannot decide for you
		///			which data is relevant for the application. In order to make the decision, if a
		///			broadcast is interesting for the application we need some kind of callback 
		///			mechanism. The <see cref="ITable.OnUpdateRequestHandler"/> and the delegate <see cref="UpdateRequestHandler"/>
		///			handle the callback structure. You application code must register for this
		///			delegate to get the information which current broadcast is received. The 
		///			<B>event method</B> must examinate the content of the broadcast and 
		///			<B>return true or false</B>
		///			to inform the <B>ITable</B> object about to consider it in or not.
		///			Usually you application had loaded flight data for a certain time frame. This
		///			is the way to test, if the broadcast's data fit into this time frame or not.
		///		</item>
		///		<item>
		///			A table sends a delegate that it has processed a broadcast and stored the data
		///			to the inner list of <see cref="IRow"/>s. Your applicationo must register for
		///			the <see cref="DatabaseTableEventHandler"/> delegate event to get the information
		///			about the new data.
		///		</item>
		/// </list>
		/// </remarks>
		/// <example>
		/// This example show how the application registers for the <B>OnUpdateRequestHandler</B> 
		/// with calls AFT_OnUpdateRequestHandler. The event method decides if the broadcast
		/// fits the time frame and the FTYP criteria.
		/// <code>
		/// static public void InitDBObjects(Ufis.Utils.BCBlinker blinker)
		/// {
		/// 	myBCBlinker = blinker;
		/// 	myDB = UT.GetMemDB();
		/// 	aftTAB = myDB["AFT"];
		/// 	ostTAB = myDB["OST"];
		/// 
		/// 	aftTAB.OnUpdateRequestHandler = new UpdateRequestHandler(AFT_OnUpdateRequestHandler);
		/// 
		/// }
		/// static bool AFT_OnUpdateRequestHandler(string pReqId, string pDest1, string pDest2,
		///										  string pCmd, string pObject, string pSeq,
		///										  string pTws, string pTwe, string pSelection, 
		///										  string pFields, string pData, string pBcNum)
		/// {
		/// 	bool blRet = false;
		/// 	bool tifaInRange = false;
		/// 	bool tifdInRange = false;
		/// 	DateTime datTifa;
		/// 	DateTime datTifd;
		/// 	int idxTifa = UT.GetItemNo(pFields, "TIFA");
		/// 	int idxTifd = UT.GetItemNo(pFields, "TIFD");
		/// 	int idxFtyp = UT.GetItemNo(pFields, "FTYP");
		/// 	
		/// 	if(idxTifa != -1)
		/// 	{
		/// 		datTifa = UT.CedaFullDateToDateTime(UT.GetItem(pData, idxTifa, ","));
		/// 		if( datTifa &lt;= UT.TimeFrameTo &amp;&amp; datTifa &gt;= UT.TimeFrameFrom)
		/// 		{
		/// 			tifaInRange = true;
		/// 		}
		/// 	}
		/// 	if(idxTifd != -1)
		/// 	{
		/// 		datTifd = UT.CedaFullDateToDateTime(UT.GetItem(pData, idxTifd, ","));
		/// 		if( datTifd &lt;= UT.TimeFrameTo &amp;&amp; datTifd &gt;= UT.TimeFrameFrom)
		/// 		{
		/// 			tifdInRange = true;
		/// 		}
		/// 	}
		/// 	if(tifaInRange == true || tifdInRange == true)
		/// 	{
		/// 		if(idxFtyp != -1)
		/// 		{
		/// 			string strFtyp = UT.GetItem(pData, idxFtyp, ",");
		/// 			if( strFtyp == "O" || strFtyp == "S" | strFtyp == "X")
		/// 			{
		/// 				blRet = true;
		/// 			}
		/// 		}
		/// 		else
		/// 		{
		/// 			blRet = true;
		/// 		}
		/// 	}
		///		return blRet;	// accept data to be worked into the AFT data in myDB["AFT"]
		/// }
		/// </code>
		/// </example>
		UpdateRequestHandler	OnUpdateRequestHandler
		{
			set;
			get;
		}

		/// <summary>
		/// Return or modify the broadcast handling state of this table.
		/// </summary>
		/// <remarks>This property is currently not activated and has no effect.</remarks>
		bool	HandleBroadcasts
		{
			set;
			get;
		}

		/// <summary>
		/// Return or modify the field list of this table.
		/// </summary>
		/// <remarks>
		/// The list of fields is set in the <see cref="IDatabase.Bind"/> method.
		/// You should not modify it with this property.
		/// </remarks>
		ArrayList	FieldList
		{
			set;
			get;
		}

		/// <summary>
		/// Returns the field list of this table as a string.
		/// </summary>
		/// <remarks>none</remarks>
		string	FieldListString 
		{
			get;
		}

		/// <summary>
		/// Returns the field length of this table as a string.
		/// </summary>
		/// <remarks>none</remarks>
		string FieldLenString
		{
			get;
		}
		/// <summary>
		/// Return or modify the list of unique fields for this table.
		/// Unique fields are fields, that may occur only once in an ITable's <see cref="IRow"/>
		/// Array. When the developer call the <see cref="ITable.Add"/> method the ITable instance
		/// makes a lookup in the unique fields list and identifies whether this value already
		/// exists in and of the list of rows. For the case of existance the new record will
		/// not be inserted.
		/// </summary>
		/// <remarks>
		/// You should only set the unique fields for key values to avoid confusion.
		/// </remarks>
		ArrayList	UniqueFields
		{
			set;
			get;
		}


		/// <summary>
		/// Return or modify the sort order for this table. Set this property to true
		/// to indicate an ascending sort and false to indicate a descending sort order.
		/// </summary>
		/// <remarks>It is not necessary to set this property, because the <see cref="ITable.Sort"/>
		/// method has the sort order parameter. It is interesting to use the get property
		/// to identify how the ITable's rows are currently sorted.</remarks>
		bool SortAscending
		{
			set;
			get;
		}


		/// <summary>
		/// Returns the load state of this table. The property is set to true 
		/// when the application has called the <see cref="ITable.Load"/> method.
		/// </summary>
		/// <remarks>Use this property to identify if a table is already loaded.</remarks>
		bool	Loaded
		{
			get;
		}

		/// <summary>
		/// Return or modify the Row at the given index of this table.
		/// ITable contains an inner list of <see cref="IRow"/>s. It is very
		/// comfortable to iterate through the records by using the indexer.
		/// </summary>
		/// <param name="index">Integer based index of the inner list of IRows</param>
		/// <remarks>none</remarks>
		/// <exception cref="OverflowException">index out of bounds</exception>
		/// <example>
		/// The following example demonstrates how to iterate through the records.
		/// <code>
		/// ITable myTab = myDB["GRN"];
		/// StringBuilder strBuffer = new StringBuilder(10000);				
		/// string strTab="";
		/// for(int i = 0; i &lt; myTab.Count; i++)
		/// {
		/// 	IRow row = myTab[i];
		/// 	strTab = row["TABN"];
		/// 	if(strTab != "")
		/// 	{
		/// 		strTab = strTab.Substring(0, 3);
		/// 		if(strTab == currTable)
		/// 		{
		/// 			strBuffer.Append(row["URNO"] + "," + row["GRPN"] + "," + row["TABN"] + "\n");
		/// 		}
		/// 	}
		/// }
		/// //do something with the strBuffer
		/// </code>
		/// </example>
		IRow this[int index]
		{
			get;
			set;
		}

		/// <summary>
		/// Returns the current name(s) for the given command of this table.
		/// </summary>
		/// <param name="index">
		/// This is a string, which describes the desired command.
		/// Possible index parameters are:
		/// <list type="bullet">
		///		<item>"read"</item>
		///		<item>"update"</item>
		///		<item>"insert"</item>
		///		<item>"delete"</item>
		/// </list>
		/// </param>
		/// <returns>The mapped router command(s) for the parameter.</returns>
		/// <remarks>
		/// The following commands are per default mapped:
		/// <list type="table">
		///		<listheader>
		///			<term>SQL Command</term>
		///			<description>CEDA mapping</description>
		///		</listheader>
		///		<item>
		///			<term>SELECT</term>
		///			<description>RT</description>
		///		</item>
		///		<item>	
		///			<term>INSERT</term>
		///			<description>IRT</description>
		///		</item>
		///		<item>
		///			<term>UPDATE</term>
		///			<description>URT</description>
		///		</item>
		///		<item>
		///			<term>DELETE</term>
		///			<description>DRT</description>
		///		</item>
		/// </list>
		/// Currently the "insert","update" and "delete" commands will be directly supported 
		/// by the framework. Pleae note : commands use the following format ",cmd1,cmd2,".
		/// Some CEDA Handler support other router command for select, insert, update, join etc.
		/// The flight handler uses the following commands:
		/// <list type="bullet">
		///		<item>GFR: SELECT</item>
		///		<item>JOF: Join Flights</item>
		///		<item>DFR: Delete</item>
		///		<item>UFR: Update</item>
		///		<item>IFR: Insert</item>
		///		<item>etc.</item>
		/// </list>
		/// With the help of calling <B>Command(index);</B> you can get the read, insert, update
		/// and delete command for that ITable instance.
		/// </remarks>
		/// <example>
		/// The following example demonstrates how to define the specialized flight handler
		/// commands and map them for the <B>ITable</B> object, which is responsible for
		/// flight data:
		/// <code>
		/// //set the commands
		/// myTable.Command("read",",GFR,");
		/// myTable.Command("insert",",ISF,");
		/// myTable.Command("update",",UFR,UPS,UPJ,"); // It is possible to map more commands to "update"
		/// myTable.Command("delete",",DFR,");
		/// 
		/// //Get the set command for e.g. "update"
		/// string updateCommand = myTable.Command("update");
		/// //The string now contains: <B>",UFR,UPS,UPJ,"</B>
		/// </code>
		/// </example>
		string Command(string index);

		/// <summary>
		/// Returns the current name(s) for the given command of this table.
		/// </summary>
		/// <param name="index">
		/// This is a string, which describes the desired command.
		/// Possible index parameters are:
		/// <list type="bullet">
		///		<item>"read"</item>
		///		<item>"update"</item>
		///		<item>"insert"</item>
		///		<item>"delete"</item>
		/// </list>
		/// </param>
		/// <param name="value">This is the string of router command that are to be
		/// mapped for the index-parameter.</param>
		/// <returns>The mapped router command(s) for the parameter.</returns>
		/// <remarks>
		/// The following commands are per default mapped:
		/// <list type="table">
		///		<listheader>
		///			<term>SQL Command</term>
		///			<description>CEDA mapping</description>
		///		</listheader>
		///		<item>
		///			<term>SELECT</term>
		///			<description>RT</description>
		///		</item>
		///		<item>	
		///			<term>INSERT</term>
		///			<description>IRT</description>
		///		</item>
		///		<item>
		///			<term>UPDATE</term>
		///			<description>URT</description>
		///		</item>
		///		<item>
		///			<term>DELETE</term>
		///			<description>DRT</description>
		///		</item>
		/// </list>
		/// Currently the "insert","update" and "delete" commands will be directly supported 
		/// by the framework. Pleae note : commands use the following format ",cmd1,cmd2,".
		/// Some CEDA Handler support other router command for select, insert, update, join etc.
		/// The flight handler uses the following commands:
		/// <list type="bullet">
		///		<item>GFR: SELECT</item>
		///		<item>JOF: Join Flights</item>
		///		<item>DFR: Delete</item>
		///		<item>UFR: Update</item>
		///		<item>IFR: Insert</item>
		///		<item>etc.</item>
		/// </list>
		/// With the help of calling <B>Command(index);</B> you can get the read, insert, update
		/// and delete command for that ITable instance.
		/// </remarks>
		/// <example>
		/// The following example demonstrates how to define the specialized flight handler
		/// commands and map them for the <B>ITable</B> object, which is responsible for
		/// flight data:
		/// <code>
		/// //set the commands
		/// myTable.Command("read",",GFR,");
		/// myTable.Command("insert",",ISF,");
		/// myTable.Command("update",",UFR,UPS,UPJ,"); // It is possible to map more commands to "update"
		/// myTable.Command("delete",",DFR,");
		/// 
		/// //Get the set command for e.g. "update"
		/// string updateCommand = myTable.Command("update");
		/// //The string now contains: <B>",UFR,UPS,UPJ,"</B>
		/// </code>
		/// </example>
		void Command(string index,string value);

		/// <summary>
		/// Loads all data specified by the given where clause into this table 
		/// if connected to a database.	The definition of the database table and 
		/// the database fields are set in the <see cref="IDatabase.Bind"/> method.
		/// </summary>
		/// <param name="where">SQL where clause.</param>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>To load data from the database your <see cref="IDatabase"/>
		/// instance must be connected to the database.
		/// </remarks>
		bool	Load(string where);

		/// <summary>
		/// Saves this table if modified and connected to a database. The table contains
		/// an array of <see cref="IRow"/>s and an internal index of all updated, inserted
		/// and deleted rows to identify changes very fast.
		/// </summary>
		/// <returns>True = success, false = failed.</returns>
		/// <remarks>
		/// <P><B>Note:</B></P> The <B>Save</B> method stores all changes record by record. 
		/// To perform a fast storage of data please use the <see cref="Release"/> 
		/// method, which uses the CEDA-Release Handler to write data in blocks
		/// into the database.<P><B>Note:</B></P> Your <see cref="IDatabase"/> instance
		/// must be connected to the database.
		/// <P><B>Note:</B></P>If you want to save single row changes please use
		/// the method <see cref="Save(IRow)"/> with the <see cref="IRow"/> parameter.
		/// </remarks>
		bool	Save();

		/// <summary>
		/// Saves a single row if modified and if this table is connected to a database.
		/// </summary>
		/// <param name="row">The modified row</param>
		/// <returns>True = success, false = failed.</returns>
		/// <remarks>Use this method to save data changes on a single row. If you 
		/// want to collect more changes befor you save all changes use the <see cref="Save()"/>
		/// method without parameters.</remarks>
		bool	Save(IRow row);

		/// <summary>
		/// Different from the <see cref="Save()"/> method <B>Release</B> does not call the
		/// server for each single record to be saved. It sends a set of records 
		/// (with <B>noOfPackages</B> records) at once to the server. 
		/// For this purpose it iterates throug the internal index of modified rows 
		/// and creates packages.
		/// </summary>
		/// <param name="noOfPackages">Number of records for one package to be collected
		/// and to be sent at once.</param>
		/// <param name="relCmd">
		/// The command for the CEDA release handler.
		/// The release Handler usually produces broadcasts after release, informs the 
		/// CEDA action process and the CEDA loghdl.
		/// <P>The following commands for the CEDA release handler are available</P>
		/// <P></P>
		/// <list type="table">
		///		<listheader>
		///			<term>Command</term>
		///			<description>Description</description>
		///		</listheader>
		///		<item>
		///			<term>LATE</term>
		///			<description>The CEDA release handler returns after all work is done.</description>
		///		</item>
		///		<item>
		///			<term>QUICK</term>
		///			<description>The CEDA release handler returns before he stores the data.
		///			The advantage is that the application can continue while the release 
		///			handler is busy. The disadvantage is that you only should use it if you do
		///			not depend on SBC broadcasts.
		///			</description>
		///		</item>
		///		<item>
		///			<term>BC</term>
		///			<description>The CEDA release handler will produce a broadcast.</description>
		///		</item>
		///		<item>
		///			<term>NOBC</term>
		///			<description>The CEDA release handler will <B>not</B> produce a broadcast.</description>
		///		</item>
		///		<item>
		///			<term>ACTION</term>
		///			<description>The CEDA release handler will trigger the CEDA action process.</description>
		///		</item>
		///		<item>
		///			<term>NOACTION</term>
		///			<description>The CEDA release handler will <B>not</B> trigger the CEDA action process</description>
		///		</item>
		///		<item>
		///			<term>LOG</term>
		///			<description>The CEDA release handler will trigger the CEDA loghdl.</description>
		///		</item>
		///		<item>
		///			<term>NOLOG</term>
		///			<description>The CEDA release handler will <B>not</B> trigger the CEDA loghdl.</description>
		///		</item>
		/// </list>
		/// </param>
		/// <param name="sbcCmd">Describe the SBC command that must be send after successful
		/// release, which produces a broadcast for a set of datachanges.
		/// The application must decide which string is reasonable for this <B>Release</B> call.
		/// </param>
		/// <returns>True = success, failed.</returns>
		/// <remarks>This mothod is useful for huge data changes that must be stored very performant.</remarks>
		/// <example>
		/// The following example demonstrates the usage of the <B>Release</B> method.
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// ITable myAdresses = myDB["ADRESSES"];
		/// for(int i = 0; i &lt; myAdresses.Count; i++)
		/// {
		///		myAdresses[i]["ID"] = i.ToString(); //Change the id for demo purposes
		///		myAdresses[i].State = State.Modified.
		/// }
		/// // This call sends packages of 300 records to the CEDA release handler,
		/// // will not produce an SBC broadcast, late answers from CEDA relhdl,
		/// // action, bchdl, loghdl will not be triggered.
		/// myAdresses.Release(300,"LATE,NOBC,NOACTION,NOLOG", null);
		/// </code>
		/// </example>
		bool	Release(int noOfPackages,string relCmd,string sbcCmd);

		/// <summary>
		/// Creates a single row from the given data in the string.
		/// </summary>
		/// <param name="data">The string, which contains the data in
		/// a <see cref="FieldSeparator"/> separated list.</param>
		/// <returns>The new created <B>IRow</B> instance.</returns>
		/// <remarks><seealso cref="CreateEmptyRow"/>
		/// The data string must contain as many items (separated) as previously
		/// defined before in the <see cref="IDatabase.Bind"/> method.</remarks>
		IRow	Create(string data);

		/// <summary>
		/// Creates multiple rows from the given string array
		/// </summary>
		/// <param name="dataList">A string array, which contains elements as
		/// <see cref="FieldSeparator"/> delimited field values.</param>
		/// <returns>An IRow array with the new records.</returns>
		/// <remarks>Each data string element must contain as many items (separated) 
		/// as previously defined before in the <see cref="IDatabase.Bind"/> method.</remarks>
		/// <example>
		/// The following example demonstrates the usage of <B>Create</B>.
		/// <code>
		/// [C#]
		/// private IRow[] CreateSomeRecords()
		/// {
		///		IDatabase myDB = UT.GetMemDB();
		///		ITable myAdr = myDB["ADRESSES"];
		///		string [] datalist = new string[10];
		///		for( int i = 0; i &lt; 10; i++)
		///		{
		///			datalist[i] = i.ToString() + ",John,39399,Denver";
		///		}
		///		IRow [] rows = myAdresse.Create(datalist);
		///		return rows;
		/// }
		/// </code>
		/// </example>
		IRow[]	Create(string[] dataList);

		/// <summary>
		/// Creates multiple rows from the data string, which contains multiple
		/// records separated with <B>rowSeparator</B> and within a single record the
		/// field values are separated by the <see cref="FieldSeparator"/>, which is
		/// per defaule a comma.
		/// </summary>
		/// <param name="data">string, which contains multiple
		/// records separated with <B>rowSeparator</B> and within a single record the
		/// field values are separated by the <see cref="FieldSeparator"/>, which is
		/// per defaule a comma.
		/// </param>
		/// <param name="rowSeparator">The delimeter of the records.</param>
		/// <returns>An IRow array with the new records.</returns>
		/// <remarks>Each data string record part must contain as many items (separated) 
		/// as previously defined before in the <see cref="IDatabase.Bind"/> method.</remarks>
		/// <example>
		/// The following example demonstrates the usage of <B>Create</B>.
		/// <code>
		/// [C#]
		/// private IRow[] CreateSomeRecords()
		/// {
		///		IDatabase myDB = UT.GetMemDB();
		///		ITable myAdr = myDB["ADRESSES"];
		///		string data = "";
		///		for( int i = 0; i &lt; 10; i++)
		///		{
		///			// use "\n" as record separator.
		///			datalist += i.ToString() + ",John,39399,Denver" + "\n";
		///		}
		///		IRow [] rows = myAdresse.Create(data, "\n");
		///		return rows;
		/// }
		/// </code>
		/// </example>
		IRow[]	Create(string data,char rowSeparator);

		/// <summary>
		/// Creates a single empty row. The internal organizational structure is prepared.
		/// The <see cref="IDatabase.Bind"/> method defines the number of field, lengths etc.
		/// </summary>
		/// <returns>The new created empty row.</returns>
		/// <remarks>It is strongly recommended to use <B>CreateEmptyRow</B> to
		///			 create new records to avoid confusion concerning the field list etc.
		/// </remarks>
		/// <example>
		/// The following example demonstrates the usage of <B>CreateEmptyRow</B>.
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// ITable myAdresses = myDB["ADRESSES"];
		///	IRow row = myAdresses.CreateEmptyRow();
		///	
		///	row["ID"] = myAdresses.GetNextUrno();
		///	row["Name"] = "John";
		///	row["Zip"] = "434";
		///	row["City"] = "Dallas";
		///	row.State = State.Created.
		///	
		///	//Add the row to myAdresse
		///	myAdresses.Add(row);
		/// </code>
		/// </example>
		IRow		CreateEmptyRow();

		/// <summary>
		/// Creates multiple empty rows. The internal organizational structure is prepared.
		/// The <see cref="IDatabase.Bind"/> method defines the number of field, lengths etc.
		/// </summary>
		/// <param name="count">Specifies the number of new rows to be created.</param>
		/// <returns>An IRow array of new rows.</returns>
		/// <remarks>The best way to generate new rows is to use this method. Please do not
		/// create new rows on your own to avoid confusion concerning the field list etc.</remarks>
		/// <example>
		/// The following example demonstrates the usage of <B>CreateEmptyRows</B>.
		/// <code>
		/// IDatabase myDB = UT.GetMemDB();
		/// ITable myAdresses = myDB["ADRESSES"];
		///	IRow [] rows = myAdresses.CreateEmptyRows(10);
		///	
		///	// Fill the new rows with reasonable values.
		///	
		///	//Add the rows to myAdresse
		///	for ( int i = 0; i &lt; 10; i++)
		///	{
		///		myAdresses.AddRow(row[i]);
		///	}
		/// </code>
		/// </example>
		IRow []	CreateEmptyRows(int count);

		/// <summary>
		/// Returns the count of rows currently associated to this table.
		/// </summary>
		/// <returns>The number of rows in the current ITable instance.</returns>
		/// <remarks>none.</remarks>
		int		Count
		{
			get;
		}

		/// <summary>
		/// Creates an index for the specified field and stores it with the 
		/// specified name in this table. The index can be used for fast row access.
		/// </summary>
		/// <param name="name">The name of the index to be created.</param>
		/// <param name="field">The logical field to be indexed.</param>
		/// <remarks>Use this method e.g. for key or id fields to speed up 
		/// your performance.
		/// <seealso cref="IDatabase.IndexUpdateImmediately"/>
		/// <seealso cref="ITable.ReorganizeIndexes"/>
		/// <seealso cref="ITable.RowsByIndexValue"/>
		/// </remarks>
		/// <example>
		/// The following example demonstrates the usage of <B>CreateIndex</B> and
		/// the access of rows by using the new index.
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// ITable myAdresses = myDB["ADRESSES"];
		/// 
		/// myAdresses.CreateIndex("NAME_IDX", "Name");
		/// IRows [] rows = myAdresses.RowsByIndexValue("NAME_IDX", "John");
		/// for(int i = 0; i &lt; rows.length; i++)
		/// {
		///		//Do something with rows[i]
		/// }
		/// </code>
		/// </example>
		void	CreateIndex(string name,string field);

		/// <summary>
		/// Removes the index with the specified name from this table.
		/// </summary>
		/// <param name="name">Name of the index to be removed.</param>
		/// <remarks>Please be careful with <B>DeleteIndex</B>, because the rest of
		/// you code might assume that the index exists and use it.
		/// <seealso cref="CreateIndex"/></remarks>
		/// <example>
		/// The following example demonstrates the usage of <B>DeleteIndex</B>.
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// ITable myAdresses = myDB["ADRESSES"];
		/// 
		/// // Create the index "NAME_IDX" for tmp. usage.
		/// myAdresses.CreateIndex("NAME_IDX", "Name");
		/// IRows [] rows = myAdresses.RowsByIndexValue("NAME_IDX", "John");
		/// for(int i = 0; i &lt; rows.length; i++)
		/// {
		///		//Do something with rows[i]
		/// }
		/// // Delete it if not longer needed.
		/// myAdresses.DeleteIndex("NAME_IDX");
		/// </code>
		/// </example>
		void	DeleteIndex(string name);

		/// <summary>
		/// Returns all line numbers for all rows matching the value of the specified index.
		/// An index is specified for a certain field. This method looks up all row values, which
		/// match the criteria.
		/// </summary>
		/// <param name="name">Name of the index.</param>
		/// <param name="valu">Value of the field to match the index value.</param>
		/// <returns>An integer array of line numbers where rows are found matching the value.</returns>
		/// <remarks>
		/// none. <seealso cref="CreateIndex"/>
		/// <seealso cref="RowsByIndexValue"/>
		/// </remarks>
		/// <example>
		/// The following example demonstrates the usage of <B>LinesByIndexValue</B> and
		/// the access of rows by using the new index.
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// ITable myAdresses = myDB["ADRESSES"];
		/// 
		/// myAdresses.CreateIndex("NAME_IDX", "Name");
		/// int [] lineNumbers = myAdresses.LinesByIndexValue("NAME_IDX", "John");
		/// for(int i = 0; i &lt; int.length; i++)
		/// {
		///		if(myAdresses[lineNumbers[i]]["Name"] == "John")
		///		{
		///			//Do something with the row
		///		}
		/// }
		/// </code>
		/// </example>
		int[] LinesByIndexValue(string name, string valu);

		/// <summary>
		/// Returns all all rows matching the value of the specified index.
		/// An index is specified for a certain field. This method looks up all row values, which
		/// match the criteria.
		/// </summary>
		/// <param name="name">Name of the index.</param>
		/// <param name="valu">Value of the field to match the index value.</param>
		/// <returns>An IRow array of matching records.</returns>
		/// <remarks>
		/// none. <seealso cref="CreateIndex"/>
		/// <seealso cref="LinesByIndexValue"/>
		/// </remarks>
		/// <example>
		/// The following example demonstrates the usage of <B>DeleteIndex</B> and
		/// the access of rows by using the new index.
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// ITable myAdresses = myDB["ADRESSES"];
		/// 
		/// // Create the index "NAME_IDX" for tmp. usage.
		/// myAdresses.CreateIndex("NAME_IDX", "Name");
		/// IRows [] rows = myAdresses.RowsByIndexValue("NAME_IDX", "John");
		/// for(int i = 0; i &lt; rows.length; i++)
		/// {
		///		//Do something with rows[i]
		/// }
		/// </code>
		/// </example>
		IRow[]	RowsByIndexValue(string name,string valu);

		/// <summary>
		/// Returns all row values for the specified fields matching the value 
		/// of the specified index.
		/// </summary>
		/// <param name="useIndexName">Name of the index.</param>
		/// <param name="key">Value to find matching data.</param>
		/// <param name="fieldList">The list of field that shall be returned.</param>
		/// <returns>A string array of fields. Each element of the string array
		/// contains the field values of the specified fieldList separated by
		/// the currently set <see cref="FieldSeparator"/></returns>
		/// <remarks>none.</remarks>
		string[]	FieldValuesByIndexValue(string useIndexName, string key, string fieldList);

		/// <summary>
		/// Sorts the table with the values of the specified field and the given sort order.
		/// </summary>
		/// <param name="sortFields">Comma separated list of logical field names.</param>
		/// <param name="ascending">true = sort ascending, false = sort descending.</param>
		/// <remarks>It strongly recommended to define the correct field lengths in the
		/// <see cref="IDatabase.Bind"/> method to ensure a correct phone book like sorting.
		/// </remarks>
		/// <example>
		/// The following example demonstrates the usage of <B>Sort</B>.
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// ITable myAdresses = myDB["ADRESSES"];
		/// 
		/// myAdress.Sort("Name,City", true);
		/// </code>
		/// </example>
		void Sort(string sortFields, bool ascending);

		/// <summary>
		/// Reorganizes all indexes for this table.
		/// </summary>
		/// <remarks>If the property <see cref="IDatabase.IndexUpdateImmediately"/> is set
		/// you do not need to call <B>ReorganizeIndexes</B>. Otherwise you must
		/// call it for each change in each row. This is very time consuming because
		/// the entire indexes are destroyed and rebuild.</remarks>
		void ReorganizeIndexes();

		/// <summary>
		/// Returns whether this table has been modified or not. This means that the table 
		/// has changed rows.
		/// </summary>
		/// <remarks>none.</remarks>
		bool Modified
		{
			get;
		}

		/// <summary>
		/// Removes all rows from this table. All changes, which are not save in the database
		/// will be discarded.
		/// </summary>
		/// <remarks>Call <B>Clear</B> to clean up you <B>ITable</B> instance.</remarks>
		void Clear();

		/// <summary>
		/// Writes the entire content of this table to the file specified <B>file path</B>.
		/// </summary>
		/// <param name="filePath">The path and file name of the destination file.</param>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>
		/// none.
		/// <seealso cref="ITable.ReadFromFile"/>
		/// </remarks>
		bool WriteToFile(string filePath);

		/// <summary>
		/// Reads a previously written file into the <B>ITable's</B> inner list of rows.
		/// </summary>
		/// <param name="filePath">The path and file name of the source file.</param>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>
		/// Please ensure that the field list structure is not different since you have saved
		/// the <B>ITable's</B> content to the file.
		/// <seealso cref="ITable.WriteToFile"/>
		/// </remarks>
		bool ReadFromFile(string filePath);

		/// <summary>
		/// Fills the specified visualizer with the contents of this table
		/// </summary>
		/// <param name="Visualizer">
		/// A reference to an instance of a Tab.ocx control to be filled.
		/// </param>
		/// <returns>The number of records filled into the Tab.ocx control.</returns>
		/// <remarks>
		/// You should have defined the Tab.ocx HeaderString and other settings as
		/// demonstrated in the example below.
		/// </remarks>
		/// <example>
		/// The following example demonstrates the usage of <B>FillVisualizer</B>.
		/// <code>
		/// [C#]
		/// private void FillTab()
		/// {
		///		IDatabase myDB = UT.GetMemDB();
		///		ITable myAdresses = myDBmyDB.Bind("Adresses", "CADR", "ID,Name,Zip,City", "10,32,10,32", "ID,Name,Zip,City");
		///		myAdresses.Load(""); // select the entire table without condition
		///		// tabData must have been initialized and assigned to a windows.form
		///		// AxTABLib.AxTAB tabData = new AxTABLib.AxTAB();
		///		if(myAdresses != null)
		///		{
		///			tabData.HeaderString = "Id,Name,Zip,City";
		///			tabData.HeaderLengthString = "50,200,50,180";
		///			tabData.LogicalFieldList = "Id,Name,Zip,City";
		///			tabData.FontName = "Arial";
		///			tabData.FontSize = 15;
		///			tabData.HeaderFontSize = 15;
		///			tabData.LineHeight = 16;
		///			tabData.ColumnWidthString = "10,32,10,32"; // As defined in Bind method.
		///			tabData.SetTabFontBold(true);
		///			myAdresses.FillVisualizer(tabData);
		///		}
		///	}
		/// </code>
		/// </example>
		int FillVisualizer(AxTABLib.AxTAB Visualizer);

		/// <summary>
		/// Adds a row to this table
		/// </summary>
		/// <param name="row">The <B>IRow</B> instance to be added.</param>
		/// <returns>the line number of this row</returns>
		/// <remarks>none.</remarks>
		/// <example>
		/// The following example demonstrates the usage of <B>Add()</B>.
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// ITable myAdresses = myDB["ADRESSES"];
		///	IRow row = myAdresses.CreateEmptyRow();
		///	
		///	row["ID"] = myAdresses.GetNextUrno();
		///	row["Name"] = "John";
		///	row["Zip"] = "434";
		///	row["City"] = "Dallas";
		///	row.State = State.Created.
		///	
		///	//Add the row to myAdresse
		///	myAdresses.Add(row);
		/// </code>
		/// </example>
		int Add(IRow row);

		/// <summary>
		/// Removes the specified row from this table
		/// </summary>
		/// <param name="row">The IRow instance to be removed.</param>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>none.</remarks>
		/// <example>
		/// The following example demonstrates the usage of <B>Remove</B>.
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// ITable myAdresses = myDB["ADRESSES"];
		/// 
		/// myAdresses.CreateIndex("NAME_IDX", "Name");
		/// IRows [] rows = myAdresses.RowsByIndexValue("NAME_IDX", "John");
		/// // now let's remove all "John"s from myAdresses
		/// for(int i = 0; i &lt; rows.length; i++)
		/// {
		///		myAdresses.Remove(rows[i]);
		/// }
		/// </code>
		/// </example>
		bool Remove(IRow row);

        /// <summary>
        /// Removes the given rows from this table
        /// </summary>
        /// <param name="rows">The IRows instance to be removed.</param>
        /// <returns>true = success, false = failed.</returns>
        /// <remarks>none.</remarks>
        /// <example>
        /// The following example demonstrates the usage of <B>Remove</B>.
        /// <code>
        /// [C#]
        /// IDatabase myDB = UT.GetMemDB();
        /// ITable myAdresses = myDB["ADRESSES"];
        /// 
        /// myAdresses.CreateIndex("NAME_IDX", "Name");
        /// IRows [] rows = myAdresses.RowsByIndexValue("NAME_IDX", "John");
        /// // now let's remove all "John"s from myAdresses
        ///		myAdresses.Remove(rows);
        /// </code>
        /// </example>
        bool Remove(IRow[] rows);

		/// <summary>
		/// Removes the row specified by it's line number from this table.
		/// </summary>
		/// <param name="lineNo">The line number as index for <B>ITable</B>.</param>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>none.</remarks>
		/// <example>
		/// The following example demonstrates the usage of <B>Remove</B>.
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// ITable myAdresses = myDB["ADRESSES"];
		/// 
		/// // We remove all rows from ITable from the last to the first
		/// for( int i = myAdresses.Count; i &gt;= 0; i--)
		/// {
		///		myAdresses.Remove(i);
		/// }
		/// </code>
		/// </example>
		bool Remove(int lineNo);

		/// <summary>
		/// Return or modify the field separator of this table. The default field separator
		/// is a comma. When you use the method <see cref="ITable.Create"/> the 
		/// <B>FieldSeparator</B> will be used to extract the field values from the data string.
		/// </summary>
		/// <remarks>
		/// When you use the default separator (comma), be carful with user entry that might
		/// also contain commas => you must map the comma to another character to avoid data
		/// confusion.
		/// </remarks>
		char FieldSeparator
		{
			set;
			get;
		}

		/// <summary>
		/// Return or modify the time format initially used for the time fields of this table.
		/// This property specified if the data is stored in UTC or local in the database.
		/// </summary>
		/// <remarks>
		/// This poperty must be used in combination with the properties:
		/// <P></P>
		/// <list type="bullet">
		///		<item><see cref="TimeFieldsCurrentlyInUtc"/></item>
		///		<item><see cref="TimeFields"/></item>
		/// </list>
		/// You do not have to care about broadcasts whether they are in UTC or not and
		/// about your data representation. After the definition of the TimeFieldsInitiallyInUtc,
		/// TimeFieldsCurrentlyInUtc and TimeFields properties everything is done for you
		/// in you ITable instance automatically.
		/// </remarks>
		/// <example>
		/// The following example demonstrates the usage of:
		/// <P></P><B>TimeFieldsInitiallyInUtc</B>, <B>TimeFieldsCurrentlyInUtc</B>
		/// and <B>TimeFields</B>.
		/// <code>
		/// [C#]
		/// myTable.TimeFieldsInitiallyInUtc = true;
		/// myTable.TimeFieldsCurrentlyInUtc = true;
		///	myTable.TimeFields = "STOA,STOD,TIFA,TIFD,TMOA,ETAI,ETDI,ONBL,OFBL,PABS,PAES,PDBS,PDES,PDBA,PDEA,PABA,PAEA";
		/// </code>
		/// </example>
		bool TimeFieldsInitiallyInUtc
		{
			set;
			get;
		}

		/// <summary>
		/// Return or modify the time format initially used for the time fields of this table.
		/// It depends on the application requirements whether the date/time values must be
		/// displayed in UTC or in local times. This bool type specifies if the fields must be 
		/// UTC or not.
		/// </summary>
		/// <remarks>
		/// This poperty must be used in combination with the properties:
		/// <P></P>
		/// <list type="bullet">
		///		<item><see cref="TimeFieldsInitiallyInUtc"/></item>
		///		<item><see cref="TimeFields"/></item>
		/// </list>
		/// You do not have to care about broadcasts whether they are in UTC or not and
		/// about your data representation. After the definition of the TimeFieldsInitiallyInUtc,
		/// TimeFieldsCurrentlyInUtc and TimeFields properties everything is done for you
		/// in you ITable instance automatically.
		/// </remarks>
		/// <example>
		/// The following example demonstrates the usage of:
		/// <P></P><B>TimeFieldsInitiallyInUtc</B>, <B>TimeFieldsCurrentlyInUtc</B>
		/// and <B>TimeFields</B>.
		/// <code>
		/// [C#]
		/// myTable.TimeFieldsInitiallyInUtc = true;
		/// myTable.TimeFieldsCurrentlyInUtc = true;
		///	myTable.TimeFields = "STOA,STOD,TIFA,TIFD,TMOA,ETAI,ETDI,ONBL,OFBL,PABS,PAES,PDBS,PDES,PDBA,PDEA,PABA,PAEA";
		/// </code>
		/// </example>
		bool TimeFieldsCurrentlyInUtc
		{
			set;
			get;
		}

		/// <summary>
		/// Return or modify the time format initially used for the time fields of this table.
		/// This string property must contain a comma separated list of database field names,
		/// which are to be considered as date/time fields.
		/// </summary>
		/// <remarks>
		/// This poperty must be used in combination with the properties:
		/// <P></P>
		/// <list type="bullet">
		///		<item><see cref="TimeFieldsCurrentlyInUtc"/></item>
		///		<item><see cref="TimeFieldsInitiallyInUtc"/></item>
		/// </list>
		/// You do not have to care about broadcasts whether they are in UTC or not and
		/// about your data representation. After the definition of the TimeFieldsInitiallyInUtc,
		/// TimeFieldsCurrentlyInUtc and TimeFields properties everything is done for you
		/// in you ITable instance automatically.
		/// </remarks>
		/// <example>
		/// The following example demonstrates the usage of:
		/// <P></P><B>TimeFieldsInitiallyInUtc</B>, <B>TimeFieldsCurrentlyInUtc</B>
		/// and <B>TimeFields</B>.
		/// <code>
		/// [C#]
		/// myTable.TimeFieldsInitiallyInUtc = true;
		/// myTable.TimeFieldsCurrentlyInUtc = true;
		///	myTable.TimeFields = "STOA,STOD,TIFA,TIFD,TMOA,ETAI,ETDI,ONBL,OFBL,PABS,PAES,PDBS,PDES,PDBA,PDEA,PABA,PAEA";
		/// </code>
		/// </example>
		string TimeFields
		{
			set;
			get;
		}

        /// <summary>
        /// Use the track changes facility for this table.
        /// If it is set (TRUE), need to use the "StartTrackChg" and "StopTrackChg" of IROW
        /// </summary>
        bool UseTrackChg
        {//AM 20080314
            get;
            set;
        }

        /// <summary>
        /// This setting will take effect when (UseTrackChg is TRUE).
        /// Track the changes immediately.
        /// If it is set (TRUE), changes will start tracking when the row was created.
        /// Otherwise, changes will start tracking after "StartTrackChg" of IROW.
        /// </summary>
        bool TrackChgImmediately
        {//AM 20080314
            get;
            set;
        }

        /// <summary>
        /// To use in Where Cluase to put urno in "'" (single quote) or as number
        //     If 'true' - use single quote
        //     else      - use number
        /// </summary>
        bool IsUrnoInSt
        {//AM:20080416 
            get;
            set;
        }

        /// <summary>
        /// Allow to save to database
        ///    If 'true' - allow to save to database (default)
        ///    Else      - not allow to save
        /// </summary>
        bool AllowToSaveToDatabase
        {
            get;
            set;
        }

	}
}
