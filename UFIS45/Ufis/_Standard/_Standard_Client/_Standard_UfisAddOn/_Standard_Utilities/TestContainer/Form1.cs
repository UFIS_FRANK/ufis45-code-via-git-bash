using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using Ufis.Data;

namespace TestContainer
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button btnConnect;
		private System.Windows.Forms.TextBox txtTableExt;
		private System.Windows.Forms.TextBox txtHopo;
		private System.Windows.Forms.TextBox txtUserName;
		private System.Windows.Forms.TextBox txtServer;
		private System.Windows.Forms.Button btnDisconnect;
		private System.Windows.Forms.TextBox txtTableName;
		private System.Windows.Forms.TextBox txtLengths;
		private System.Windows.Forms.TextBox txtDbFields;
		private System.Windows.Forms.TextBox txtFields;
		private System.Windows.Forms.TextBox txtDbTableName;
		private System.Windows.Forms.Button btnBind;
		private System.Windows.Forms.Button btnUnbind;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.TextBox txtLoadName;
		private System.Windows.Forms.TextBox txtLoadWhere;
		private AxTABLib.AxTAB tabSource;
		private System.Windows.Forms.Label lblSourceRecords;
		private System.Windows.Forms.Label lblDestRecords;
		private AxTABLib.AxTAB tabDest;
		private System.Windows.Forms.CheckBox chkIndexUpdateImmediately;
		private System.Windows.Forms.CheckBox chkSaveImmediately;
		private System.Windows.Forms.Button btnCreateIndex;
		private System.Windows.Forms.TextBox txtCreateIndexName;
		private System.Windows.Forms.TextBox txtCreateIndexField;
		private System.Windows.Forms.Button btnLinesByIndex;
		private System.Windows.Forms.TextBox txtLinesByIndexName;
		private System.Windows.Forms.TextBox txtLinesByIndexValue;
		private System.Windows.Forms.Button btnRowsByIndexValue;
		private System.Windows.Forms.Button btnFieldValues;
		private System.Windows.Forms.TextBox txtFieldValuesTable;
		private System.Windows.Forms.TextBox txtFieldValuesIndexName;
		private System.Windows.Forms.TextBox txtFieldValuesIndexValue;
		private System.Windows.Forms.TextBox txtFieldValuesFields;
		private System.Windows.Forms.Button btnSetFieldValues;
		private System.Windows.Forms.TextBox txtSetFieldValuesValues;
		private System.Windows.Forms.TextBox txtSetFieldValuesFields;
		private System.Windows.Forms.TextBox txtSetFieldValuesLine;
		private System.Windows.Forms.TextBox txtSetFieldValuesTable;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.TextBox txtSaveTable;
		private System.Windows.Forms.Button btnRelease;
		private System.Windows.Forms.TextBox txtReleaseTable;
		private System.Windows.Forms.TextBox txtReleaseNoOfPackages;
		private System.Windows.Forms.TextBox txtReleaseRelCmd;
		private System.Windows.Forms.TextBox txtReleaseSbcCmd;
		private System.Windows.Forms.Button btnWriteToFile;
		private System.Windows.Forms.Button btnReadFromFile;
		private System.Windows.Forms.TextBox txtReadFromFileTable;
		private System.Windows.Forms.TextBox txtWriteToFileTable;
		private System.Windows.Forms.TextBox txtWriteToFilePath;
		private System.Windows.Forms.TextBox txtReadFromFilePath;
		private Ufis.Utils.BCBlinker bcBlinker1;
		private System.Windows.Forms.CheckBox chkTimeFieldsInitiallyInUtc;
		private System.Windows.Forms.CheckBox chkTimeFieldsCurrentlyInUtc;
		private	Ufis.Data.IDatabase	myDB = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.tabSource = new AxTABLib.AxTAB();
			this.lblSourceRecords = new System.Windows.Forms.Label();
			this.btnConnect = new System.Windows.Forms.Button();
			this.txtServer = new System.Windows.Forms.TextBox();
			this.txtTableExt = new System.Windows.Forms.TextBox();
			this.txtHopo = new System.Windows.Forms.TextBox();
			this.txtUserName = new System.Windows.Forms.TextBox();
			this.btnDisconnect = new System.Windows.Forms.Button();
			this.btnBind = new System.Windows.Forms.Button();
			this.txtTableName = new System.Windows.Forms.TextBox();
			this.txtLengths = new System.Windows.Forms.TextBox();
			this.txtDbFields = new System.Windows.Forms.TextBox();
			this.txtFields = new System.Windows.Forms.TextBox();
			this.txtDbTableName = new System.Windows.Forms.TextBox();
			this.btnUnbind = new System.Windows.Forms.Button();
			this.btnLoad = new System.Windows.Forms.Button();
			this.txtLoadName = new System.Windows.Forms.TextBox();
			this.txtLoadWhere = new System.Windows.Forms.TextBox();
			this.lblDestRecords = new System.Windows.Forms.Label();
			this.tabDest = new AxTABLib.AxTAB();
			this.chkIndexUpdateImmediately = new System.Windows.Forms.CheckBox();
			this.chkSaveImmediately = new System.Windows.Forms.CheckBox();
			this.btnCreateIndex = new System.Windows.Forms.Button();
			this.txtCreateIndexName = new System.Windows.Forms.TextBox();
			this.txtCreateIndexField = new System.Windows.Forms.TextBox();
			this.btnLinesByIndex = new System.Windows.Forms.Button();
			this.txtLinesByIndexName = new System.Windows.Forms.TextBox();
			this.txtLinesByIndexValue = new System.Windows.Forms.TextBox();
			this.btnRowsByIndexValue = new System.Windows.Forms.Button();
			this.btnFieldValues = new System.Windows.Forms.Button();
			this.txtFieldValuesTable = new System.Windows.Forms.TextBox();
			this.txtFieldValuesIndexName = new System.Windows.Forms.TextBox();
			this.txtFieldValuesIndexValue = new System.Windows.Forms.TextBox();
			this.txtFieldValuesFields = new System.Windows.Forms.TextBox();
			this.btnSetFieldValues = new System.Windows.Forms.Button();
			this.txtSetFieldValuesValues = new System.Windows.Forms.TextBox();
			this.txtSetFieldValuesFields = new System.Windows.Forms.TextBox();
			this.txtSetFieldValuesLine = new System.Windows.Forms.TextBox();
			this.txtSetFieldValuesTable = new System.Windows.Forms.TextBox();
			this.txtSaveTable = new System.Windows.Forms.TextBox();
			this.btnSave = new System.Windows.Forms.Button();
			this.txtReleaseTable = new System.Windows.Forms.TextBox();
			this.btnRelease = new System.Windows.Forms.Button();
			this.txtReleaseNoOfPackages = new System.Windows.Forms.TextBox();
			this.txtReleaseRelCmd = new System.Windows.Forms.TextBox();
			this.txtReleaseSbcCmd = new System.Windows.Forms.TextBox();
			this.txtReadFromFileTable = new System.Windows.Forms.TextBox();
			this.btnReadFromFile = new System.Windows.Forms.Button();
			this.txtWriteToFileTable = new System.Windows.Forms.TextBox();
			this.btnWriteToFile = new System.Windows.Forms.Button();
			this.txtWriteToFilePath = new System.Windows.Forms.TextBox();
			this.txtReadFromFilePath = new System.Windows.Forms.TextBox();
			this.bcBlinker1 = new Ufis.Utils.BCBlinker();
			this.chkTimeFieldsInitiallyInUtc = new System.Windows.Forms.CheckBox();
			this.chkTimeFieldsCurrentlyInUtc = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.tabSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabDest)).BeginInit();
			this.SuspendLayout();
			// 
			// tabSource
			// 
			this.tabSource.Location = new System.Drawing.Point(16, 40);
			this.tabSource.Name = "tabSource";
			this.tabSource.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabSource.OcxState")));
			this.tabSource.Size = new System.Drawing.Size(1200, 152);
			this.tabSource.TabIndex = 0;
			// 
			// lblSourceRecords
			// 
			this.lblSourceRecords.Location = new System.Drawing.Point(16, 8);
			this.lblSourceRecords.Name = "lblSourceRecords";
			this.lblSourceRecords.TabIndex = 1;
			this.lblSourceRecords.Text = "0";
			// 
			// btnConnect
			// 
			this.btnConnect.Location = new System.Drawing.Point(16, 400);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.TabIndex = 2;
			this.btnConnect.Text = "Connect";
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// txtServer
			// 
			this.txtServer.Location = new System.Drawing.Point(104, 400);
			this.txtServer.Name = "txtServer";
			this.txtServer.Size = new System.Drawing.Size(48, 20);
			this.txtServer.TabIndex = 3;
			this.txtServer.Text = "SIN1";
			// 
			// txtTableExt
			// 
			this.txtTableExt.Location = new System.Drawing.Point(272, 400);
			this.txtTableExt.Name = "txtTableExt";
			this.txtTableExt.Size = new System.Drawing.Size(48, 20);
			this.txtTableExt.TabIndex = 6;
			this.txtTableExt.Text = "TAB";
			// 
			// txtHopo
			// 
			this.txtHopo.Location = new System.Drawing.Point(216, 400);
			this.txtHopo.Name = "txtHopo";
			this.txtHopo.Size = new System.Drawing.Size(48, 20);
			this.txtHopo.TabIndex = 7;
			this.txtHopo.Text = "SIN";
			// 
			// txtUserName
			// 
			this.txtUserName.Location = new System.Drawing.Point(160, 400);
			this.txtUserName.Name = "txtUserName";
			this.txtUserName.Size = new System.Drawing.Size(48, 20);
			this.txtUserName.TabIndex = 8;
			this.txtUserName.Text = "KBI";
			// 
			// btnDisconnect
			// 
			this.btnDisconnect.Location = new System.Drawing.Point(328, 400);
			this.btnDisconnect.Name = "btnDisconnect";
			this.btnDisconnect.TabIndex = 9;
			this.btnDisconnect.Text = "Disconnect";
			this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
			// 
			// btnBind
			// 
			this.btnBind.Location = new System.Drawing.Point(16, 432);
			this.btnBind.Name = "btnBind";
			this.btnBind.TabIndex = 10;
			this.btnBind.Text = "Bind";
			this.btnBind.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// txtTableName
			// 
			this.txtTableName.Location = new System.Drawing.Point(104, 432);
			this.txtTableName.Name = "txtTableName";
			this.txtTableName.Size = new System.Drawing.Size(48, 20);
			this.txtTableName.TabIndex = 11;
			this.txtTableName.Text = "AFT";
			// 
			// txtLengths
			// 
			this.txtLengths.Location = new System.Drawing.Point(216, 456);
			this.txtLengths.Name = "txtLengths";
			this.txtLengths.Size = new System.Drawing.Size(792, 20);
			this.txtLengths.TabIndex = 12;
			this.txtLengths.Text = "100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,1" +
				"00,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100";
			this.txtLengths.TextChanged += new System.EventHandler(this.txtLengths_TextChanged);
			// 
			// txtDbFields
			// 
			this.txtDbFields.Location = new System.Drawing.Point(216, 480);
			this.txtDbFields.Name = "txtDbFields";
			this.txtDbFields.Size = new System.Drawing.Size(792, 20);
			this.txtDbFields.TabIndex = 13;
			this.txtDbFields.Text = "URNO,RKEY,FTYP,ADID,FLNO,GTA1,GTD1,LAND,ONBS,PSTA,PSTD,STOA,STOD,TIFA,TIFD,TISA,T" +
				"ISD,TMOA,TTYP,ORG3,DES3,FLTI,ETAI,ETDI,ONBL,OFBL,VIA3,ACT3,PABA,PAEA,PABS,PAES,P" +
				"DBA,PDEA,PDBS,PDES,VIAL,ADER";
			// 
			// txtFields
			// 
			this.txtFields.Location = new System.Drawing.Point(216, 432);
			this.txtFields.Name = "txtFields";
			this.txtFields.Size = new System.Drawing.Size(792, 20);
			this.txtFields.TabIndex = 14;
			this.txtFields.Text = "URNO,RKEY,FTYP,ADID,FLNO,GTA1,GTD1,LAND,ONBS,PSTA,PSTD,STOA,STOD,TIFA,TIFD,TISA,T" +
				"ISD,TMOA,TTYP,ORG3,DES3,FLTI,ETAI,ETDI,ONBL,OFBL,VIA3,ACT3,PABA,PAEA,PABS,PAES,P" +
				"DBA,PDEA,PDBS,PDES,VIAL,ADER";
			this.txtFields.TextChanged += new System.EventHandler(this.txtFields_TextChanged);
			// 
			// txtDbTableName
			// 
			this.txtDbTableName.Location = new System.Drawing.Point(160, 432);
			this.txtDbTableName.Name = "txtDbTableName";
			this.txtDbTableName.Size = new System.Drawing.Size(48, 20);
			this.txtDbTableName.TabIndex = 15;
			this.txtDbTableName.Text = "AFT";
			// 
			// btnUnbind
			// 
			this.btnUnbind.Location = new System.Drawing.Point(1024, 432);
			this.btnUnbind.Name = "btnUnbind";
			this.btnUnbind.TabIndex = 16;
			this.btnUnbind.Text = "Unbind";
			this.btnUnbind.Click += new System.EventHandler(this.btnUnbind_Click);
			// 
			// btnLoad
			// 
			this.btnLoad.Location = new System.Drawing.Point(16, 520);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.TabIndex = 17;
			this.btnLoad.Text = "Load";
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click_1);
			// 
			// txtLoadName
			// 
			this.txtLoadName.Location = new System.Drawing.Point(104, 520);
			this.txtLoadName.Name = "txtLoadName";
			this.txtLoadName.Size = new System.Drawing.Size(48, 20);
			this.txtLoadName.TabIndex = 18;
			this.txtLoadName.Text = "AFT";
			// 
			// txtLoadWhere
			// 
			this.txtLoadWhere.Location = new System.Drawing.Point(216, 520);
			this.txtLoadWhere.Name = "txtLoadWhere";
			this.txtLoadWhere.Size = new System.Drawing.Size(792, 20);
			this.txtLoadWhere.TabIndex = 19;
			this.txtLoadWhere.Text = "WHERE TIFA BETWEEN \'20031013000000\' AND \'20031103235900\'";
			// 
			// lblDestRecords
			// 
			this.lblDestRecords.Location = new System.Drawing.Point(16, 200);
			this.lblDestRecords.Name = "lblDestRecords";
			this.lblDestRecords.TabIndex = 21;
			this.lblDestRecords.Text = "0";
			// 
			// tabDest
			// 
			this.tabDest.Location = new System.Drawing.Point(16, 232);
			this.tabDest.Name = "tabDest";
			this.tabDest.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabDest.OcxState")));
			this.tabDest.Size = new System.Drawing.Size(1200, 152);
			this.tabDest.TabIndex = 20;
			// 
			// chkIndexUpdateImmediately
			// 
			this.chkIndexUpdateImmediately.Location = new System.Drawing.Point(712, 400);
			this.chkIndexUpdateImmediately.Name = "chkIndexUpdateImmediately";
			this.chkIndexUpdateImmediately.Size = new System.Drawing.Size(160, 24);
			this.chkIndexUpdateImmediately.TabIndex = 22;
			this.chkIndexUpdateImmediately.Text = "Index Update Immediately";
			this.chkIndexUpdateImmediately.CheckedChanged += new System.EventHandler(this.chkIndexUpdateImmediately_CheckedChanged);
			// 
			// chkSaveImmediately
			// 
			this.chkSaveImmediately.Location = new System.Drawing.Point(888, 400);
			this.chkSaveImmediately.Name = "chkSaveImmediately";
			this.chkSaveImmediately.Size = new System.Drawing.Size(120, 24);
			this.chkSaveImmediately.TabIndex = 23;
			this.chkSaveImmediately.Text = "Save Immediately";
			this.chkSaveImmediately.CheckedChanged += new System.EventHandler(this.chkSaveImmediately_CheckedChanged);
			// 
			// btnCreateIndex
			// 
			this.btnCreateIndex.Location = new System.Drawing.Point(16, 560);
			this.btnCreateIndex.Name = "btnCreateIndex";
			this.btnCreateIndex.Size = new System.Drawing.Size(80, 23);
			this.btnCreateIndex.TabIndex = 24;
			this.btnCreateIndex.Text = "Create Index";
			this.btnCreateIndex.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtCreateIndexName
			// 
			this.txtCreateIndexName.Location = new System.Drawing.Point(104, 560);
			this.txtCreateIndexName.Name = "txtCreateIndexName";
			this.txtCreateIndexName.Size = new System.Drawing.Size(48, 20);
			this.txtCreateIndexName.TabIndex = 25;
			this.txtCreateIndexName.Text = "TOM";
			// 
			// txtCreateIndexField
			// 
			this.txtCreateIndexField.Location = new System.Drawing.Point(160, 560);
			this.txtCreateIndexField.Name = "txtCreateIndexField";
			this.txtCreateIndexField.Size = new System.Drawing.Size(48, 20);
			this.txtCreateIndexField.TabIndex = 26;
			this.txtCreateIndexField.Text = "URNO";
			// 
			// btnLinesByIndex
			// 
			this.btnLinesByIndex.Location = new System.Drawing.Point(16, 592);
			this.btnLinesByIndex.Name = "btnLinesByIndex";
			this.btnLinesByIndex.Size = new System.Drawing.Size(80, 23);
			this.btnLinesByIndex.TabIndex = 27;
			this.btnLinesByIndex.Text = "LinesByIndex";
			this.btnLinesByIndex.Click += new System.EventHandler(this.btnLinesByIndex_Click);
			// 
			// txtLinesByIndexName
			// 
			this.txtLinesByIndexName.Location = new System.Drawing.Point(104, 592);
			this.txtLinesByIndexName.Name = "txtLinesByIndexName";
			this.txtLinesByIndexName.Size = new System.Drawing.Size(48, 20);
			this.txtLinesByIndexName.TabIndex = 28;
			this.txtLinesByIndexName.Text = "TOM";
			// 
			// txtLinesByIndexValue
			// 
			this.txtLinesByIndexValue.Location = new System.Drawing.Point(160, 592);
			this.txtLinesByIndexValue.Name = "txtLinesByIndexValue";
			this.txtLinesByIndexValue.Size = new System.Drawing.Size(48, 20);
			this.txtLinesByIndexValue.TabIndex = 29;
			this.txtLinesByIndexValue.Text = "Value";
			// 
			// btnRowsByIndexValue
			// 
			this.btnRowsByIndexValue.Location = new System.Drawing.Point(216, 592);
			this.btnRowsByIndexValue.Name = "btnRowsByIndexValue";
			this.btnRowsByIndexValue.Size = new System.Drawing.Size(112, 23);
			this.btnRowsByIndexValue.TabIndex = 30;
			this.btnRowsByIndexValue.Text = "RowsByIndexValue";
			this.btnRowsByIndexValue.Click += new System.EventHandler(this.btnRowsByIndexValue_Click);
			// 
			// btnFieldValues
			// 
			this.btnFieldValues.Location = new System.Drawing.Point(16, 624);
			this.btnFieldValues.Name = "btnFieldValues";
			this.btnFieldValues.TabIndex = 31;
			this.btnFieldValues.Text = "FieldValues";
			this.btnFieldValues.Click += new System.EventHandler(this.btnFieldValues_Click);
			// 
			// txtFieldValuesTable
			// 
			this.txtFieldValuesTable.Location = new System.Drawing.Point(104, 624);
			this.txtFieldValuesTable.Name = "txtFieldValuesTable";
			this.txtFieldValuesTable.Size = new System.Drawing.Size(48, 20);
			this.txtFieldValuesTable.TabIndex = 32;
			this.txtFieldValuesTable.Text = "AFT";
			// 
			// txtFieldValuesIndexName
			// 
			this.txtFieldValuesIndexName.Location = new System.Drawing.Point(160, 624);
			this.txtFieldValuesIndexName.Name = "txtFieldValuesIndexName";
			this.txtFieldValuesIndexName.Size = new System.Drawing.Size(48, 20);
			this.txtFieldValuesIndexName.TabIndex = 33;
			this.txtFieldValuesIndexName.Text = "TOM";
			// 
			// txtFieldValuesIndexValue
			// 
			this.txtFieldValuesIndexValue.Location = new System.Drawing.Point(216, 624);
			this.txtFieldValuesIndexValue.Name = "txtFieldValuesIndexValue";
			this.txtFieldValuesIndexValue.Size = new System.Drawing.Size(48, 20);
			this.txtFieldValuesIndexValue.TabIndex = 34;
			this.txtFieldValuesIndexValue.Text = "Value";
			// 
			// txtFieldValuesFields
			// 
			this.txtFieldValuesFields.Location = new System.Drawing.Point(280, 624);
			this.txtFieldValuesFields.Name = "txtFieldValuesFields";
			this.txtFieldValuesFields.Size = new System.Drawing.Size(392, 20);
			this.txtFieldValuesFields.TabIndex = 35;
			this.txtFieldValuesFields.Text = "FIELDS";
			// 
			// btnSetFieldValues
			// 
			this.btnSetFieldValues.Location = new System.Drawing.Point(8, 656);
			this.btnSetFieldValues.Name = "btnSetFieldValues";
			this.btnSetFieldValues.Size = new System.Drawing.Size(88, 23);
			this.btnSetFieldValues.TabIndex = 36;
			this.btnSetFieldValues.Text = "SetFieldValues";
			this.btnSetFieldValues.Click += new System.EventHandler(this.btnSetFieldValues_Click);
			// 
			// txtSetFieldValuesValues
			// 
			this.txtSetFieldValuesValues.Location = new System.Drawing.Point(608, 656);
			this.txtSetFieldValuesValues.Name = "txtSetFieldValuesValues";
			this.txtSetFieldValuesValues.Size = new System.Drawing.Size(392, 20);
			this.txtSetFieldValuesValues.TabIndex = 40;
			this.txtSetFieldValuesValues.Text = "Values";
			// 
			// txtSetFieldValuesFields
			// 
			this.txtSetFieldValuesFields.Location = new System.Drawing.Point(216, 656);
			this.txtSetFieldValuesFields.Name = "txtSetFieldValuesFields";
			this.txtSetFieldValuesFields.Size = new System.Drawing.Size(384, 20);
			this.txtSetFieldValuesFields.TabIndex = 39;
			this.txtSetFieldValuesFields.Text = "Fields";
			// 
			// txtSetFieldValuesLine
			// 
			this.txtSetFieldValuesLine.Location = new System.Drawing.Point(160, 656);
			this.txtSetFieldValuesLine.Name = "txtSetFieldValuesLine";
			this.txtSetFieldValuesLine.Size = new System.Drawing.Size(48, 20);
			this.txtSetFieldValuesLine.TabIndex = 38;
			this.txtSetFieldValuesLine.Text = "0";
			// 
			// txtSetFieldValuesTable
			// 
			this.txtSetFieldValuesTable.Location = new System.Drawing.Point(104, 656);
			this.txtSetFieldValuesTable.Name = "txtSetFieldValuesTable";
			this.txtSetFieldValuesTable.Size = new System.Drawing.Size(48, 20);
			this.txtSetFieldValuesTable.TabIndex = 37;
			this.txtSetFieldValuesTable.Text = "AFT";
			// 
			// txtSaveTable
			// 
			this.txtSaveTable.Location = new System.Drawing.Point(104, 688);
			this.txtSaveTable.Name = "txtSaveTable";
			this.txtSaveTable.Size = new System.Drawing.Size(48, 20);
			this.txtSaveTable.TabIndex = 42;
			this.txtSaveTable.Text = "AFT";
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(16, 688);
			this.btnSave.Name = "btnSave";
			this.btnSave.TabIndex = 41;
			this.btnSave.Text = "Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// txtReleaseTable
			// 
			this.txtReleaseTable.Location = new System.Drawing.Point(104, 720);
			this.txtReleaseTable.Name = "txtReleaseTable";
			this.txtReleaseTable.Size = new System.Drawing.Size(48, 20);
			this.txtReleaseTable.TabIndex = 44;
			this.txtReleaseTable.Text = "AFT";
			// 
			// btnRelease
			// 
			this.btnRelease.Location = new System.Drawing.Point(16, 720);
			this.btnRelease.Name = "btnRelease";
			this.btnRelease.TabIndex = 43;
			this.btnRelease.Text = "Release";
			this.btnRelease.Click += new System.EventHandler(this.btnRelease_Click);
			// 
			// txtReleaseNoOfPackages
			// 
			this.txtReleaseNoOfPackages.Location = new System.Drawing.Point(160, 720);
			this.txtReleaseNoOfPackages.Name = "txtReleaseNoOfPackages";
			this.txtReleaseNoOfPackages.Size = new System.Drawing.Size(48, 20);
			this.txtReleaseNoOfPackages.TabIndex = 45;
			this.txtReleaseNoOfPackages.Text = "100";
			// 
			// txtReleaseRelCmd
			// 
			this.txtReleaseRelCmd.Location = new System.Drawing.Point(216, 720);
			this.txtReleaseRelCmd.Name = "txtReleaseRelCmd";
			this.txtReleaseRelCmd.Size = new System.Drawing.Size(48, 20);
			this.txtReleaseRelCmd.TabIndex = 46;
			this.txtReleaseRelCmd.Text = "REL";
			// 
			// txtReleaseSbcCmd
			// 
			this.txtReleaseSbcCmd.Location = new System.Drawing.Point(272, 720);
			this.txtReleaseSbcCmd.Name = "txtReleaseSbcCmd";
			this.txtReleaseSbcCmd.Size = new System.Drawing.Size(48, 20);
			this.txtReleaseSbcCmd.TabIndex = 47;
			this.txtReleaseSbcCmd.Text = "SBC";
			// 
			// txtReadFromFileTable
			// 
			this.txtReadFromFileTable.Location = new System.Drawing.Point(104, 784);
			this.txtReadFromFileTable.Name = "txtReadFromFileTable";
			this.txtReadFromFileTable.Size = new System.Drawing.Size(48, 20);
			this.txtReadFromFileTable.TabIndex = 51;
			this.txtReadFromFileTable.Text = "AFT";
			// 
			// btnReadFromFile
			// 
			this.btnReadFromFile.Location = new System.Drawing.Point(16, 784);
			this.btnReadFromFile.Name = "btnReadFromFile";
			this.btnReadFromFile.TabIndex = 50;
			this.btnReadFromFile.Text = "ReadFromFile";
			this.btnReadFromFile.Click += new System.EventHandler(this.btnReadFromFile_Click);
			// 
			// txtWriteToFileTable
			// 
			this.txtWriteToFileTable.Location = new System.Drawing.Point(104, 752);
			this.txtWriteToFileTable.Name = "txtWriteToFileTable";
			this.txtWriteToFileTable.Size = new System.Drawing.Size(48, 20);
			this.txtWriteToFileTable.TabIndex = 49;
			this.txtWriteToFileTable.Text = "AFT";
			// 
			// btnWriteToFile
			// 
			this.btnWriteToFile.Location = new System.Drawing.Point(16, 752);
			this.btnWriteToFile.Name = "btnWriteToFile";
			this.btnWriteToFile.TabIndex = 48;
			this.btnWriteToFile.Text = "WriteToFile";
			this.btnWriteToFile.Click += new System.EventHandler(this.btnWriteToFile_Click);
			// 
			// txtWriteToFilePath
			// 
			this.txtWriteToFilePath.Location = new System.Drawing.Point(160, 752);
			this.txtWriteToFilePath.Name = "txtWriteToFilePath";
			this.txtWriteToFilePath.Size = new System.Drawing.Size(384, 20);
			this.txtWriteToFilePath.TabIndex = 52;
			this.txtWriteToFilePath.Text = "Filepath";
			// 
			// txtReadFromFilePath
			// 
			this.txtReadFromFilePath.Location = new System.Drawing.Point(160, 784);
			this.txtReadFromFilePath.Name = "txtReadFromFilePath";
			this.txtReadFromFilePath.Size = new System.Drawing.Size(384, 20);
			this.txtReadFromFilePath.TabIndex = 53;
			this.txtReadFromFilePath.Text = "Filepath";
			// 
			// bcBlinker1
			// 
			this.bcBlinker1.BackColor = System.Drawing.SystemColors.Control;
			this.bcBlinker1.Location = new System.Drawing.Point(1168, 8);
			this.bcBlinker1.Name = "bcBlinker1";
			this.bcBlinker1.Size = new System.Drawing.Size(44, 24);
			this.bcBlinker1.TabIndex = 54;
			// 
			// chkTimeFieldsInitiallyInUtc
			// 
			this.chkTimeFieldsInitiallyInUtc.Location = new System.Drawing.Point(432, 400);
			this.chkTimeFieldsInitiallyInUtc.Name = "chkTimeFieldsInitiallyInUtc";
			this.chkTimeFieldsInitiallyInUtc.TabIndex = 55;
			this.chkTimeFieldsInitiallyInUtc.Text = "Time Fields initially in UTC";
			this.chkTimeFieldsInitiallyInUtc.CheckedChanged += new System.EventHandler(this.chkTimeFieldsInitiallyInUtc_CheckedChanged);
			// 
			// chkTimeFieldsCurrentlyInUtc
			// 
			this.chkTimeFieldsCurrentlyInUtc.Location = new System.Drawing.Point(560, 400);
			this.chkTimeFieldsCurrentlyInUtc.Name = "chkTimeFieldsCurrentlyInUtc";
			this.chkTimeFieldsCurrentlyInUtc.TabIndex = 56;
			this.chkTimeFieldsCurrentlyInUtc.Text = "Time Fields currently in UTC";
			this.chkTimeFieldsCurrentlyInUtc.CheckedChanged += new System.EventHandler(this.chkTimeFieldsCurrentlyInUtc_CheckedChanged);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(1232, 814);
			this.Controls.Add(this.chkTimeFieldsCurrentlyInUtc);
			this.Controls.Add(this.chkTimeFieldsInitiallyInUtc);
			this.Controls.Add(this.bcBlinker1);
			this.Controls.Add(this.txtReadFromFilePath);
			this.Controls.Add(this.txtWriteToFilePath);
			this.Controls.Add(this.txtReadFromFileTable);
			this.Controls.Add(this.txtWriteToFileTable);
			this.Controls.Add(this.txtReleaseSbcCmd);
			this.Controls.Add(this.txtReleaseRelCmd);
			this.Controls.Add(this.txtReleaseNoOfPackages);
			this.Controls.Add(this.txtReleaseTable);
			this.Controls.Add(this.txtSaveTable);
			this.Controls.Add(this.txtSetFieldValuesValues);
			this.Controls.Add(this.txtSetFieldValuesFields);
			this.Controls.Add(this.txtSetFieldValuesLine);
			this.Controls.Add(this.txtSetFieldValuesTable);
			this.Controls.Add(this.txtFieldValuesFields);
			this.Controls.Add(this.txtFieldValuesIndexValue);
			this.Controls.Add(this.txtFieldValuesIndexName);
			this.Controls.Add(this.txtFieldValuesTable);
			this.Controls.Add(this.txtLinesByIndexValue);
			this.Controls.Add(this.txtLinesByIndexName);
			this.Controls.Add(this.txtCreateIndexField);
			this.Controls.Add(this.txtCreateIndexName);
			this.Controls.Add(this.txtLoadWhere);
			this.Controls.Add(this.txtLoadName);
			this.Controls.Add(this.txtDbTableName);
			this.Controls.Add(this.txtFields);
			this.Controls.Add(this.txtDbFields);
			this.Controls.Add(this.txtLengths);
			this.Controls.Add(this.txtTableName);
			this.Controls.Add(this.txtUserName);
			this.Controls.Add(this.txtHopo);
			this.Controls.Add(this.txtTableExt);
			this.Controls.Add(this.txtServer);
			this.Controls.Add(this.btnReadFromFile);
			this.Controls.Add(this.btnWriteToFile);
			this.Controls.Add(this.btnRelease);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.btnSetFieldValues);
			this.Controls.Add(this.btnFieldValues);
			this.Controls.Add(this.btnRowsByIndexValue);
			this.Controls.Add(this.btnLinesByIndex);
			this.Controls.Add(this.btnCreateIndex);
			this.Controls.Add(this.chkSaveImmediately);
			this.Controls.Add(this.chkIndexUpdateImmediately);
			this.Controls.Add(this.lblDestRecords);
			this.Controls.Add(this.tabDest);
			this.Controls.Add(this.btnLoad);
			this.Controls.Add(this.btnUnbind);
			this.Controls.Add(this.btnBind);
			this.Controls.Add(this.btnDisconnect);
			this.Controls.Add(this.btnConnect);
			this.Controls.Add(this.lblSourceRecords);
			this.Controls.Add(this.tabSource);
			this.Name = "Form1";
			this.Text = "Ufis Database Test Container";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.tabSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabDest)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			myDB = Database.Create();
			this.chkSaveImmediately.Checked			= myDB.SaveImmediately;
			this.chkIndexUpdateImmediately.Checked  = myDB.IndexUpdateImmediately;
			tabSource.SetInternalLineBuffer(true);
		}

		private void btnConnect_Click(object sender, System.EventArgs e)
		{
			bool result = this.myDB.Connect(txtServer.Text,txtUserName.Text,txtHopo.Text,txtTableExt.Text,"TestCon");
			this.myDB.OnEventHandler += new DatabaseEventHandler(this.DatabaseEventHandler);
			this.myDB.OnTableEventHandler += new DatabaseTableEventHandler(this.TableEventHandler);
		}

		private void lblUserName_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnDisconnect_Click(object sender, System.EventArgs e)
		{
			this.myDB.OnEventHandler	  -= new DatabaseEventHandler(this.DatabaseEventHandler);
			this.myDB.OnTableEventHandler -= new DatabaseTableEventHandler(this.TableEventHandler);
			this.myDB.Disconnect();
		}

		private void btnLoad_Click(object sender, System.EventArgs e)
		{
			ITable myTable = this.myDB.Bind(this.txtTableName.Text,this.txtDbTableName.Text,this.txtFields.Text,this.txtLengths.Text,this.txtDbFields.Text);
			if (this.txtDbTableName.Text == "AFT")
			{
				myTable.Command("insert",",ISF,");
				myTable.Command("update",",UFR,URT,");
				myTable.Command("delete",",DFR,");
				myTable.TimeFields = "STOA,STOD,TIFA,TIFD,TMOA,ETAI,ETDI,ONBL,OFBL,PABS,PAES,PDBS,PDES";
				myTable.OnUpdateRequestHandler = new UpdateRequestHandler(this.UpdateRequestHandler);
			}

			this.chkTimeFieldsInitiallyInUtc.Checked = myTable.TimeFieldsInitiallyInUtc;
			this.chkTimeFieldsCurrentlyInUtc.Checked = myTable.TimeFieldsCurrentlyInUtc;

		}

		private void btnUnbind_Click(object sender, System.EventArgs e)
		{
			ITable myTable = this.myDB[this.txtTableName.Text];
			this.myDB.Unbind(this.txtTableName.Text);
			if (this.txtDbTableName.Text == "AFT")
			{
				myTable.OnUpdateRequestHandler -= new UpdateRequestHandler(this.UpdateRequestHandler);
			}
		}

		private void txtFields_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		private void txtLengths_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		private void btnLoad_Click_1(object sender, System.EventArgs e)
		{

            MessageBox.Show(this.txtLoadName.Text);
            ITable myTable = this.myDB[this.txtLoadName.Text];
			myTable.Clear();

          
            if (myTable.Load(this.txtLoadWhere.Text))
			{
				this.tabSource.ResetContent();
				this.tabSource.SetInternalLineBuffer(true);
				this.tabSource.HeaderString = this.txtFields.Text;
				tabSource.LogicalFieldList = this.tabSource.HeaderString;
				this.tabSource.HeaderLengthString = this.txtLengths.Text;
				this.tabSource.ShowHorzScroller(true);
				this.tabSource.EnableHeaderSizing(true);
				this.tabSource.AutoSizeByHeader = true;
				this.tabSource.EnableInlineEdit(true);
				this.tabSource.InplaceEditUpperCase = false;
				this.tabSource.IndexCreate("URNO",0);
				StringBuilder olS = new StringBuilder(10000);
				for (int i = 0; i < myTable.Count; i++)
				{
					IRow row = myTable[i];
					olS.Append(row.FieldValues() + "\n");
					//this.tabSource.InsertTextLine(row.FieldValues(),false);
				}
                IRow newRow = myTable.CreateEmptyRow();
                //newRow[]="";
                myTable.Add(newRow);
                myTable.Save(newRow);

				this.tabSource.InsertBuffer(olS.ToString(), "\n");
				this.tabSource.Refresh();
				this.lblSourceRecords.Text = "Records : " + this.tabSource.GetLineCount().ToString();
				this.tabSource.AutoSizeColumns();
				this.tabSource.IndexCreate("URNO",0);
				//				this.tabSource.Sort("0", true, true);
			}
			
					
		}

		private void chkIndexUpdateImmediately_CheckedChanged(object sender, System.EventArgs e)
		{
			this.myDB.IndexUpdateImmediately = this.chkIndexUpdateImmediately.Checked;		
		}

		private void chkSaveImmediately_CheckedChanged(object sender, System.EventArgs e)
		{
			this.myDB.SaveImmediately = this.chkSaveImmediately.Checked;		
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			ITable myTable = this.myDB["AFT"];
			myTable.CreateIndex(this.txtCreateIndexName.Text,this.txtCreateIndexField.Text);
		}

		private void btnLinesByIndex_Click(object sender, System.EventArgs e)
		{
			ITable myTable = this.myDB["AFT"];
			int[] lines = myTable.LinesByIndexValue(this.txtLinesByIndexName.Text,this.txtLinesByIndexValue.Text);
			if(lines != null)
			{
				StringBuilder msg = new StringBuilder(10000);
				for ( int i = 0; i < lines.Length; i++)
				{
					msg.Append(lines[i].ToString());
					msg.Append(",");
				}
				MessageBox.Show(msg.ToString());
			}

		
		}

		private void btnRowsByIndexValue_Click(object sender, System.EventArgs e)
		{
			int i=0;
			this.tabDest.ResetContent();
			ITable myTable = this.myDB["AFT"];
			IRow[] rows = myTable.RowsByIndexValue(this.txtLinesByIndexName.Text,this.txtLinesByIndexValue.Text);
			if (rows != null)
			{
				StringBuilder strLine= new StringBuilder(10000);
				for( i = 0; i < rows.Length; i++)
				{
					for(int j = 0; j < myTable.FieldList.Count; j++)
					{
						if ((j+1) == myTable.FieldList.Count)
							strLine.Append(rows[i][(string)myTable.FieldList[j]]);
						else
							strLine.Append(rows[i][(string)myTable.FieldList[j]] + ",");
					}
					strLine.Append("\n");
				}
				tabDest.InsertBuffer(strLine.ToString(), "\n");
			}
			tabDest.AutoSizeColumns();
			tabDest.Refresh();
			this.lblDestRecords.Text = tabDest.GetLineCount().ToString() + " Records";
		}

		private void btnFieldValues_Click(object sender, System.EventArgs e)
		{
			int i=0;
			tabDest.ResetContent();

			ITable myTable = this.myDB[this.txtFieldValuesTable.Text];
			IRow emptyRow  = myTable.CreateEmptyRow();

			string [] lineValues = myTable.FieldValuesByIndexValue(this.txtFieldValuesIndexName.Text,this.txtFieldValuesIndexValue.Text,this.txtFieldValuesFields.Text);
			StringBuilder tmp = new StringBuilder(10000);
			if (lineValues != null)
			{
				for(i = 0; i < lineValues.Length; i++)
				{
					tmp.Append(lineValues[i] + '\n');
				}
			}
			if(tmp.Length > 0)
			{
				tmp.Remove(tmp.Length-1, 1);
			}
			tabDest.InsertBuffer(tmp.ToString(), "\n");
			tabDest.Refresh();
			lblDestRecords.Text = tabDest.GetLineCount().ToString();

		}

		private void btnSetFieldValues_Click(object sender, System.EventArgs e)
		{
			ITable myTable = this.myDB[this.txtSetFieldValuesTable.Text];
			myTable[Convert.ToInt32(this.txtSetFieldValuesLine.Text)].SetFieldValues(this.txtSetFieldValuesFields.Text,this.txtSetFieldValuesValues.Text);
			tabDest.ResetContent();
			string values = myTable[Convert.ToInt32(this.txtSetFieldValuesLine.Text)].FieldValues(this.txtSetFieldValuesFields.Text);
			tabDest.InsertTextLine(values, true);
			tabDest.AutoSizeColumns();
			lblDestRecords.Text = tabDest.GetLineCount().ToString();
		
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			ITable myTable = this.myDB[this.txtSaveTable.Text];
			myTable.Save();
		
		}

		private void btnRelease_Click(object sender, System.EventArgs e)
		{
			ITable myTable = this.myDB[this.txtReleaseTable.Text];
			myTable.Release(Convert.ToInt32(this.txtReleaseNoOfPackages.Text),this.txtReleaseRelCmd.Text,this.txtReleaseSbcCmd.Text);
		
		}

		private void btnWriteToFile_Click(object sender, System.EventArgs e)
		{
			ITable myTable = this.myDB[this.txtWriteToFileTable.Text];
			myTable.WriteToFile(this.txtWriteToFilePath.Text);
		}

		private void btnReadFromFile_Click(object sender, System.EventArgs e)
		{
			ITable myTable = this.myDB[this.txtReadFromFileTable.Text];
			myTable.ReadFromFile(this.txtReadFromFilePath.Text);
			this.tabSource.ResetContent();
			this.tabSource.SetInternalLineBuffer(true);
			this.tabSource.HeaderString = this.txtFields.Text;
			this.tabSource.HeaderLengthString = this.txtLengths.Text;
			this.tabSource.ShowHorzScroller(true);
			this.tabSource.EnableHeaderSizing(true);
			this.tabSource.AutoSizeByHeader = true;
			this.tabSource.EnableInlineEdit(true);
			this.tabSource.InplaceEditUpperCase = false;
			StringBuilder olS = new StringBuilder(10000);
			for (int i = 0; i < myTable.Count; i++)
			{
				IRow row = myTable[i];
				olS.Append(row.FieldValues() + "\n");
				//this.tabSource.InsertTextLine(row.FieldValues(),false);
			}
			this.tabSource.InsertBuffer(olS.ToString(), "\n");
			this.tabSource.Refresh();
			this.lblSourceRecords.Text = "Records : " + this.tabSource.GetLineCount().ToString();
			this.tabSource.AutoSizeColumns();
		
		}

		bool UpdateRequestHandler(string pReqId, string pDest1, string pDest2,string pCmd, string pObject, string pSeq,string pTws, string pTwe, string pSelection, string pFields, string pData, string pBcNum)
		{
			return true;	// accept in any case for testing issues
		}

		void BcEventHandler(string pReqId, string pDest1, string pDest2,string pCmd, string pObject, string pSeq,string pTws, string pTwe, string pSelection, string pFields, string pData, string pBcNum)
		{
			switch(pCmd)
			{
				case "DRT" :
					ITable myTable = this.myDB[pObject.Substring(0,3)];
					if (myTable != null)
					{
						myTable.Clear();
						if (myTable.Load(this.txtLoadWhere.Text))
						{
							this.tabSource.ResetContent();
							this.tabSource.SetInternalLineBuffer(true);
							this.tabSource.HeaderString = this.txtFields.Text;
							tabSource.LogicalFieldList = this.tabSource.HeaderString;
							this.tabSource.HeaderLengthString = this.txtLengths.Text;
							this.tabSource.ShowHorzScroller(true);
							this.tabSource.EnableHeaderSizing(true);
							this.tabSource.AutoSizeByHeader = true;
							this.tabSource.EnableInlineEdit(true);
							this.tabSource.InplaceEditUpperCase = false;
							this.tabSource.IndexCreate("URNO",0);
							StringBuilder olS = new StringBuilder(10000);
							for (int i = 0; i < myTable.Count; i++)
							{
								IRow row = myTable[i];
								olS.Append(row.FieldValues() + "\n");
								//this.tabSource.InsertTextLine(row.FieldValues(),false);
							}
							this.tabSource.InsertBuffer(olS.ToString(), "\n");
							this.tabSource.Refresh();
							this.lblSourceRecords.Text = "Records : " + this.tabSource.GetLineCount().ToString();
							this.tabSource.AutoSizeColumns();
							this.tabSource.IndexCreate("URNO",0);
							//				this.tabSource.Sort("0", true, true);
						}
					}
					break;
			}
		}

		void TableEventHandler(object obj,DatabaseTableEventArgs eventArgs)
		{
			ITable myTable = this.myDB[eventArgs.table];
			if (myTable != null)
			{
				string lineNo;
				int currLine=-1;
				if (myTable.Command("insert").IndexOf(","+eventArgs.command+",") >= 0)
				{
					this.tabSource.InsertTextLine(eventArgs.row.FieldValues(),false);												
				}
				else if (myTable.Command("update").IndexOf(","+eventArgs.command+",") >= 0)
				{
					lineNo = this.tabSource.GetLinesByIndexValue("URNO",eventArgs.row["URNO"],0);
					currLine=tabSource.GetNextResultLine();
					if(currLine > -1)
					{
						string fields = this.txtFields.Text;
						string data   = eventArgs.row.FieldValues();
						this.tabSource.SetFieldValues(currLine,fields,data);
					}
				}
				else if (myTable.Command("delete").IndexOf(","+eventArgs.command+",") >= 0)
				{
					lineNo = this.tabSource.GetLinesByIndexValue("URNO",eventArgs.row["URNO"],0);
					currLine=tabSource.GetNextResultLine();
					if(currLine > -1)
					{
						this.tabSource.DeleteLine(currLine);
					}
				}

				this.tabSource.IndexCreate("URNO",0);
				this.tabSource.Refresh();
			}
		}

		void DatabaseEventHandler(object obj,DatabaseEventArgs eventArgs)
		{
			this.bcBlinker1.Blink();						
			if (eventArgs.command == "SBC")
			{
				switch(eventArgs.obj)
				{
					case "RELDEM" :
						ITable myTable = this.myDB["DEM"];
						if (myTable != null)
						{
							myTable.Clear();
							if (myTable.Load(this.txtLoadWhere.Text))
							{
								this.tabSource.ResetContent();
								this.tabSource.SetInternalLineBuffer(true);
								this.tabSource.HeaderString = this.txtFields.Text;
								tabSource.LogicalFieldList = this.tabSource.HeaderString;
								this.tabSource.HeaderLengthString = this.txtLengths.Text;
								this.tabSource.ShowHorzScroller(true);
								this.tabSource.EnableHeaderSizing(true);
								this.tabSource.AutoSizeByHeader = true;
								this.tabSource.EnableInlineEdit(true);
								this.tabSource.InplaceEditUpperCase = false;
								this.tabSource.IndexCreate("URNO",0);
								StringBuilder olS = new StringBuilder(10000);
								for (int i = 0; i < myTable.Count; i++)
								{
									IRow row = myTable[i];
									olS.Append(row.FieldValues() + "\n");
									//this.tabSource.InsertTextLine(row.FieldValues(),false);
								}
								this.tabSource.InsertBuffer(olS.ToString(), "\n");
								this.tabSource.Refresh();
								this.lblSourceRecords.Text = "Records : " + this.tabSource.GetLineCount().ToString();
								this.tabSource.AutoSizeColumns();
								this.tabSource.IndexCreate("URNO",0);
								//				this.tabSource.Sort("0", true, true);
							}
						}

						break;
				}
			}
		}

		private void chkTimeFieldsCurrentlyInUtc_CheckedChanged(object sender, System.EventArgs e)
		{
			ITable myTable = this.myDB[this.txtDbTableName.Text];
			if (myTable != null)
			{
				myTable.TimeFieldsCurrentlyInUtc = this.chkTimeFieldsCurrentlyInUtc.Checked;	
				this.tabSource.ResetContent();
				this.tabSource.SetInternalLineBuffer(true);
				this.tabSource.HeaderString = this.txtFields.Text;
				tabSource.LogicalFieldList = this.tabSource.HeaderString;
				this.tabSource.HeaderLengthString = this.txtLengths.Text;
				this.tabSource.ShowHorzScroller(true);
				this.tabSource.EnableHeaderSizing(true);
				this.tabSource.AutoSizeByHeader = true;
				this.tabSource.EnableInlineEdit(true);
				this.tabSource.InplaceEditUpperCase = false;
				this.tabSource.IndexCreate("URNO",0);
				StringBuilder olS = new StringBuilder(10000);
				for (int i = 0; i < myTable.Count; i++)
				{
					IRow row = myTable[i];
					olS.Append(row.FieldValues() + "\n");
					//this.tabSource.InsertTextLine(row.FieldValues(),false);
				}
				this.tabSource.InsertBuffer(olS.ToString(), "\n");
				this.tabSource.Refresh();
				this.lblSourceRecords.Text = "Records : " + this.tabSource.GetLineCount().ToString();
				this.tabSource.AutoSizeColumns();
				this.tabSource.IndexCreate("URNO",0);
			}
		}

		private void chkTimeFieldsInitiallyInUtc_CheckedChanged(object sender, System.EventArgs e)
		{
			ITable myTable = this.myDB[this.txtDbTableName.Text];
			if (myTable != null)
				myTable.TimeFieldsInitiallyInUtc = this.chkTimeFieldsInitiallyInUtc.Checked;	
		
		}
	}
}
