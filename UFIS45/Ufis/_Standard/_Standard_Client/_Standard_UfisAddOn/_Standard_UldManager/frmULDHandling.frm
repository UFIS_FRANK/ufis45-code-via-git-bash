VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmULDHandling 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ULD Handling"
   ClientHeight    =   10455
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   14475
   Icon            =   "frmULDHandling.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10455
   ScaleWidth      =   14475
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdRefreshLoaTab 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   7080
      TabIndex        =   38
      Top             =   480
      Width           =   1275
   End
   Begin TABLib.TAB tabStatistik 
      Height          =   960
      Left            =   9450
      TabIndex        =   30
      Top             =   600
      Visible         =   0   'False
      Width           =   3210
      _Version        =   65536
      _ExtentX        =   5662
      _ExtentY        =   1693
      _StockProps     =   64
   End
   Begin VB.CommandButton cmdSync 
      Caption         =   "sync"
      Height          =   375
      Left            =   3960
      TabIndex        =   29
      Top             =   9960
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox picGroup 
      AutoRedraw      =   -1  'True
      Height          =   8055
      Index           =   1
      Left            =   150
      ScaleHeight     =   7995
      ScaleWidth      =   14175
      TabIndex        =   11
      Top             =   1650
      Width           =   14235
      Begin MSComCtl2.DTPicker dtTime 
         Height          =   330
         Left            =   3645
         TabIndex        =   31
         Top             =   6075
         Visible         =   0   'False
         Width           =   2715
         _ExtentX        =   4789
         _ExtentY        =   582
         _Version        =   393216
         Format          =   51773443
         UpDown          =   -1  'True
         CurrentDate     =   38013
      End
      Begin TABLib.TAB tabBDI 
         Height          =   1095
         Left            =   75
         TabIndex        =   36
         Top             =   6435
         Width           =   7125
         _Version        =   65536
         _ExtentX        =   12568
         _ExtentY        =   1931
         _StockProps     =   64
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4350
         TabIndex        =   26
         Top             =   7620
         Width           =   1095
      End
      Begin VB.CommandButton cmdAddULD 
         Caption         =   "&Add ULD"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   90
         TabIndex        =   19
         Top             =   7620
         Width           =   1095
      End
      Begin VB.TextBox txtFlightRemark 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   7305
         MaxLength       =   1900
         MultiLine       =   -1  'True
         TabIndex        =   13
         Top             =   6420
         Width           =   6780
      End
      Begin TABLib.TAB tabUld 
         Height          =   6075
         Left            =   45
         TabIndex        =   12
         Top             =   45
         Width           =   14070
         _Version        =   65536
         _ExtentX        =   24818
         _ExtentY        =   10716
         _StockProps     =   64
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Baggage Departure Information:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   90
         TabIndex        =   35
         Top             =   6210
         Width           =   3225
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Flight related remark:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   7320
         TabIndex        =   14
         Top             =   6225
         Width           =   3225
      End
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "&Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   8790
      TabIndex        =   22
      Top             =   9960
      Width           =   1275
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print Report"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1500
      TabIndex        =   21
      Top             =   9960
      Width           =   1275
   End
   Begin VB.CommandButton cmdShowTelex 
      Caption         =   "Show &Telex"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   150
      TabIndex        =   20
      Top             =   9960
      Width           =   1275
   End
   Begin VB.PictureBox picGroup 
      AutoRedraw      =   -1  'True
      Height          =   2385
      Index           =   2
      Left            =   150
      ScaleHeight     =   2325
      ScaleWidth      =   12105
      TabIndex        =   17
      Top             =   7440
      Visible         =   0   'False
      Width           =   12165
      Begin VB.CommandButton cmdCopyAll 
         Caption         =   "C&opy All"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   25
         Top             =   1980
         Width           =   1275
      End
      Begin VB.CommandButton cmdCopySelected 
         Caption         =   "&Copy Sel."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   90
         TabIndex        =   24
         Top             =   1980
         Width           =   1155
      End
      Begin TABLib.TAB tabLoaTab 
         Height          =   1815
         Left            =   60
         TabIndex        =   18
         Top             =   60
         Width           =   9705
         _Version        =   65536
         _ExtentX        =   17119
         _ExtentY        =   3201
         _StockProps     =   64
      End
   End
   Begin VB.PictureBox picCommon 
      AutoRedraw      =   -1  'True
      Height          =   585
      Left            =   9600
      ScaleHeight     =   525
      ScaleWidth      =   495
      TabIndex        =   15
      Top             =   9930
      Width           =   555
   End
   Begin VB.PictureBox picGroup 
      AutoRedraw      =   -1  'True
      Height          =   1125
      Index           =   0
      Left            =   150
      ScaleHeight     =   1065
      ScaleWidth      =   14175
      TabIndex        =   0
      Top             =   180
      Width           =   14235
      Begin VB.TextBox txtFlight 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   4995
         Locked          =   -1  'True
         TabIndex        =   32
         Top             =   540
         Width           =   975
      End
      Begin TABLib.TAB tabTmp 
         Height          =   705
         Left            =   8790
         TabIndex        =   27
         Top             =   255
         Visible         =   0   'False
         Width           =   1005
         _Version        =   65536
         _ExtentX        =   1773
         _ExtentY        =   1244
         _StockProps     =   64
      End
      Begin VB.TextBox txtFlight 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   4995
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   195
         Width           =   975
      End
      Begin VB.TextBox txtFlight 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   2850
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   540
         Width           =   975
      End
      Begin VB.TextBox txtFlight 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   2850
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   180
         Width           =   975
      End
      Begin VB.TextBox txtFlight 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   540
         Width           =   975
      End
      Begin VB.TextBox txtFlight 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   1
         Tag             =   "FLNO"
         Top             =   180
         Width           =   975
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "A/C Reg:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4095
         TabIndex        =   33
         Top             =   555
         Width           =   840
      End
      Begin VB.Label lblApt 
         BackStyle       =   0  'Transparent
         Caption         =   "Apt:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4110
         TabIndex        =   10
         Top             =   210
         Width           =   615
      End
      Begin VB.Label lblEst 
         BackStyle       =   0  'Transparent
         Caption         =   "Est:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2040
         TabIndex        =   9
         Top             =   570
         Width           =   615
      End
      Begin VB.Label lblSched 
         BackStyle       =   0  'Transparent
         Caption         =   "Sched:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2010
         TabIndex        =   8
         Top             =   210
         Width           =   615
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Date:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   150
         TabIndex        =   7
         Top             =   570
         Width           =   615
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Flight:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   150
         TabIndex        =   6
         Top             =   210
         Width           =   615
      End
   End
   Begin VB.Label lblCaption 
      BackStyle       =   0  'Transparent
      Caption         =   "<X> Entries"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C000C0&
      Height          =   285
      Index           =   3
      Left            =   4785
      TabIndex        =   37
      Top             =   1395
      Width           =   4965
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Flight related remark:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   270
      TabIndex        =   34
      Top             =   7875
      Width           =   3225
   End
   Begin VB.Label lblCaption 
      BackStyle       =   0  'Transparent
      Caption         =   "<X> Entries"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C000C0&
      Height          =   285
      Index           =   2
      Left            =   150
      TabIndex        =   28
      Top             =   1410
      Width           =   4965
   End
   Begin VB.Label lblCaption 
      BackStyle       =   0  'Transparent
      Caption         =   "Data from Interfaces"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF80FF&
      Height          =   285
      Index           =   1
      Left            =   150
      TabIndex        =   23
      Top             =   7170
      Width           =   2115
   End
   Begin VB.Label lblCaption 
      BackStyle       =   0  'Transparent
      Caption         =   "Fix flight related:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C000C0&
      Height          =   285
      Index           =   0
      Left            =   45
      TabIndex        =   16
      Top             =   0
      Visible         =   0   'False
      Width           =   1575
   End
End
Attribute VB_Name = "frmULDHandling"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public omAftUrno As String
Public omCurrAdid As String
Public omFlightRemark As String
Public bmIsInplaceActive As Boolean
Public lmCurrEditLine As Long
Public lmCurrEditCol As Long
Public currEditString As String
Public bmEditErr As Boolean
Public myTabScrollPos As Long
Public strTimePickerField As String

Private Sub cmdAddULD_Click()
    AddNewUld
    SetStatistik
End Sub

Public Sub SetStatistik()
    Dim uldCnt As Integer
    Dim emptyCnt As Integer
    Dim i As Long
    Dim strValue As String
    Dim strPos As String
    Dim strOldPos As String
    
    '### SHA20060412 New Statistics Display
    Dim Bcnt As Integer
    Dim Mcnt As Integer
    Dim Ccnt As Integer
    
    tabStatistik.ResetContent
    tabStatistik.SetFieldSeparator "|"
    tabStatistik.HeaderString = tabUld.HeaderString
    tabStatistik.LogicalFieldList = tabUld.LogicalFieldList
    tabStatistik.HeaderLengthString = tabUld.HeaderLengthString
    tabStatistik.ColumnWidthString = tabUld.ColumnWidthString
    tabStatistik.InsertBuffer tabUld.GetBuffer(0, tabUld.GetLineCount - 1, vbLf), vbLf
    tabStatistik.Sort "0", True, True
    strPos = ""
    strOldPos = ""
    For i = 0 To tabStatistik.GetLineCount - 1
        strPos = tabStatistik.GetFieldValue(i, "POSF")
        If strPos <> strOldPos Then
            strValue = tabStatistik.GetFieldValue(i, "CONT")
            If Is50(strPos) = False Then
                '### SHA20060412 ERROR: the cont value comes as BY,BJ,.. !
                'If strValue = "M" Or strValue = "B" Or strValue = "C" Then uldCnt = uldCnt +
                If strValue = "M" Or left$(strValue, 1) = "B" Or strValue = "C" Then uldCnt = uldCnt + 1
                If strValue = "X" Then emptyCnt = emptyCnt + 1
                
                '### SHA20060412 New Statistics Display
                If left$(strValue, 1) = "B" Then Bcnt = Bcnt + 1
                If strValue = "C" Then Ccnt = Ccnt + 1
                If strValue = "M" Then Mcnt = Mcnt + 1
                
            End If
        End If
        strOldPos = strPos
    Next i
    
    lblCaption(2).Caption = " Entries ULD: " & uldCnt & "  Empty: " & emptyCnt & "  Total: " & CStr(tabUld.GetLineCount)
    
    '### SHA20060412 New Statistics Display
    lblCaption(3).Caption = " Baggage: " & Bcnt & "  Cargo: " & Ccnt & "  Mail: " & Mcnt
    
End Sub

Public Function Is50(strPOSF As String) As Boolean
    Dim i As Integer
    Dim str As String
    Dim blRet As Boolean
    
    blRet = False
    For i = 50 To 59
        str = CStr(i)
        If InStr(1, strPOSF, str) > 0 Then
            blRet = True
            i = 60
        End If
    Next i
    Is50 = blRet
End Function
Public Sub AddNewUld(Optional bpRefresh As Boolean = True)
    Dim strEmptyLine As String
    Dim cnt As Long
    Dim strUrno As String
    Dim strUsr As String
    Dim myDat As Date
    Dim strDate As String
    Dim strValues As String
    
    myDat = Now
    strDate = MakeCedaDate(myDat)
    If strCurrentUser = "" Then
        strUsr = "Default"
    End If
    strEmptyLine = "||||||||||||||||||"
    tabUld.InsertTextLine strEmptyLine, False
    strUrno = frmData.GetNextUrno
    strValues = strValues + strUrno + "|"
    strValues = strValues + omAftUrno + "|"
    strValues = strValues + strDate + "|"
    strValues = strValues + strUsr + "|"
    strValues = strValues + strDate + "|"
    strValues = strValues + strUsr + "|"
    cnt = tabUld.GetLineCount - 1
    tabUld.SetLineTag cnt, ""
    tabUld.SetFieldValues cnt, "URNO,FLNU,CDAT,USEC,LSTU,USEU", strValues
    tabUld.SetLineColor cnt, vbBlack, vbYellow
    If bpRefresh = True Then
        tabUld.OnVScrollTo cnt
        tabUld.Refresh
    End If
    lblCaption(2).Caption = CStr(tabUld.GetLineCount) & " Entries"
End Sub

Private Sub cmdExit_Click()
    Unload Me
End Sub

Private Sub cmdPrint_Click()
    Dim rpt As New rptUldsForFlight
    Dim rpt_D As New rptUldsForFlight_D
    If omCurrAdid = "A" Then
        rpt.Show , Me
    Else
        rpt_D.Show , Me
    End If
End Sub

Private Sub cmdRefreshLoaTab_Click()
    Screen.MousePointer = vbHourglass
'    frmData.LoadData
'    frmMain.DoArrivalFilter
'    frmMain.DoDepartureFilter

    frmMain.ReloadLOATABForFlnu omAftUrno
    SetData omAftUrno
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdSave_Click()
    Dim strMC As String
    Dim strLine As String
    Dim strVal As String
    Dim strRet As String
    Dim llLine As Long
    Dim i As Long
    Dim cnt As Long
    Dim strCommand As String
    Dim bCol As Long
    Dim tCol As Long
    Dim strUrno As Long 'urno for update/delete where clause
    Dim myIDX As Integer
    Dim strFieldList As String
    Dim strLinesToDelete As String
    Dim strFrmDataToDelete As String
    Dim strCont As String
    Dim strCurrValues As String
    Dim strFrmDataValues As String
    Dim strUsr As String
    Dim olIRT As String, olURT As String, olDRT As String
    Dim olUpdateString As String, olInsertString As String, olDeleteString As String
    Dim ilNoOfUpdates As Integer, ilNoOfDeletes As Integer, ilNoOfInserts As Integer
    Dim ilCurrentCount As Integer
    Dim ilTotalCount As Integer
    Dim strSBCData As String
    Dim myDate As Date
    Dim strTmp As String
    Dim strWhere As String

    ilNoOfUpdates = 0
    ilNoOfDeletes = 0
    ilNoOfInserts = 0
    ilCurrentCount = 0
    ilTotalCount = 0

    
    If strCurrentUser = "" Then
        strUsr = "Default"
    End If

    tabUld.EnableInlineEdit False
    
    cnt = tabUld.GetLineCount - 1
    'frmData: URNO,FLNU,ULDN,TYPE,CONF,CONT,ORIG,DEST,POSF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA,RESE,CDAT,LSTU,USEC,USEU
    'TabUld:  POSF,ULDN,CONT,DEST,CONF,TIMF,TIMH,REMA,URNO,FLNU,CDAT,USEC,LSTU,USEU
    'amended by khoo on 20/07/2006, added DSSN
    strFieldList = "URNO,FLNU,ULDN,TYPE,CONF,CONT,ADDI,ORIG,DEST,DSSN,POSF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA,RESE,CDAT,LSTU,USEC,USEU"
    'strFieldList = "URNO,FLNU,ULDN,TYPE,CONF,CONT,ADDI,ORIG,DEST,POSF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA,RESE,CDAT,LSTU,USEC,USEU"
    olIRT = "*CMD*,ULDTAB,IRT," & CStr(ItemCount(strFieldList, ",")) & ","
    olURT = "*CMD*,ULDTAB,URT," & CStr(ItemCount(strFieldList, ",")) & ","
    olDRT = "*CMD*,ULDTAB,DRT,-1"

    olInsertString = olIRT + strFieldList + vbLf 'CString("\n");
    olUpdateString = olURT + strFieldList + ",[URNO=:VURNO]" + vbLf 'CString("\n");
    olDeleteString = olDRT + ",[URNO=:VURNO]" + vbLf 'CString("\n");
    Synchronise "ULDTAB", True
    For i = 0 To cnt
        strCommand = ""
        tabUld.GetLineColor i, tCol, bCol
        If bCol <> vbWhite Then
            'amended by khoo on 20/07/2006, added DSSN
            'strCurrValues = tabUld.GetFieldValues(i, "POSF,ULDN,CONT,ADDI,DEST,CONF,TIMF,TIMH,REMA,URNO,FLNU,CDAT,USEC,LSTU,USEU")
            strCurrValues = tabUld.GetFieldValues(i, "DSSN,POSF,ULDN,CONT,ADDI,DEST,CONF,TIMF,TIMH,REMA,URNO,FLNU,CDAT,USEC,LSTU,USEU")
            strUrno = tabUld.GetFieldValues(i, "URNO")
            strRet = frmData.tabData(5).GetLinesByIndexValue("URNO", strUrno, 0)
            llLine = frmData.tabData(5).GetNextResultLine
            Select Case (bCol)
                Case vbYellow
                    strCommand = "IRT"
                    strVal = tabUld.GetFieldValues(i, "URNO,FLNU,ULDN") ',RESE,CDAT,LSTU,USEC,USEU")
                    strVal = strVal + "||" + tabUld.GetFieldValues(i, "CONF,CONT,ADDI")
                    'amended by khoo on 20/07/2006, added DSSN
                    strVal = strVal + "||" + tabUld.GetFieldValues(i, "DEST,DSSN,POSF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA")
                    'strVal = strVal + "||" + tabUld.GetFieldValues(i, "DEST,POSF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA")
                    strVal = strVal + "||" + tabUld.GetFieldValues(i, "CDAT,LSTU,USEC,USEU")
                    strVal = CleanString(strVal, FOR_SERVER, False)
                    strVal = Replace(strVal, "|", ",", 1, -1, vbBinaryCompare)
                    strFrmDataValues = strVal
                    strVal = CleanNullValues(strVal)
                    tabUld.SetLineTag i, strCurrValues
                    frmData.tabData(5).InsertTextLine strFrmDataValues, False
                    olInsertString = olInsertString + strVal + vbLf
'                    If frmData.CallServer("IRT", "ULDTAB", strFieldList, strVal, "", "230") = False Then
'                            MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
'                    End If
                    ilCurrentCount = ilCurrentCount + 1
                    ilTotalCount = ilTotalCount + 1
                    ilNoOfInserts = ilNoOfInserts + 1
                Case vbGreen
                    strCommand = "URT"
                    strVal = tabUld.GetFieldValues(i, "URNO,FLNU,ULDN") ',RESE,CDAT,LSTU,USEC,USEU")
                    strVal = strVal + "||" + tabUld.GetFieldValues(i, "CONF,CONT,ADDI")
                    'amended by khoo on 21/07/2006, added DSSN
                    strVal = strVal + "||" + tabUld.GetFieldValues(i, "DEST,DSSN,POSF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA")
                    'strVal = strVal + "||" + tabUld.GetFieldValues(i, "DEST,POSF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA")
                    strVal = strVal + "||" + tabUld.GetFieldValues(i, "CDAT")
                    strVal = strVal + "|" + MakeCedaDate(Now)
                    strVal = strVal + "|" + tabUld.GetFieldValues(i, "USEC")
                    strVal = strVal + "|" + strCurrentUser
                    strVal = CleanString(strVal, FOR_SERVER, False)
                    strVal = Replace(strVal, "|", ",", 1, -1, vbBinaryCompare)
                    strFrmDataValues = strVal
                    strVal = CleanNullValues(strVal)
                    tabUld.SetFieldValues i, "USEU,LSTU", strUsr + "|" + MakeCedaDate(Now)
                    'amended by khoo on 21/07/2006, added DSSN
                    strCurrValues = tabUld.GetFieldValues(i, "DSSN,POSF,ULDN,CONT,ADDI,DEST,CONF,TIMF,TIMH,REMA,URNO,FLNU,CDAT,USEC,LSTU,USEU")
                    'strCurrValues = tabUld.GetFieldValues(i, "POSF,ULDN,CONT,ADDI,DEST,CONF,TIMF,TIMH,REMA,URNO,FLNU,CDAT,USEC,LSTU,USEU")
                    tabUld.SetLineTag i, strCurrValues
                    If llLine > -1 Then
                        frmData.tabData(5).SetFieldValues llLine, strFieldList, strFrmDataValues
                        strVal = CleanNullValues(strVal)
                        olUpdateString = olUpdateString + strVal + "," + CStr(strUrno) + vbLf
'                        If frmData.CallServer("URT", "ULDTAB", strFieldList, strVal, "WHERE URNO=" & strUrno, "230") = False Then
'                            MsgBox "Update Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
'                        End If
                        ilCurrentCount = ilCurrentCount + 1
                        ilTotalCount = ilTotalCount + 1
                        ilNoOfUpdates = ilNoOfUpdates + 1
                    Else
                        strVal = tabUld.GetFieldValues(i, "URNO,FLNU,ULDN") ',RESE,CDAT,LSTU,USEC,USEU")
                        strVal = strVal + "||" + tabUld.GetFieldValues(i, "CONF,CONT,ADDI")
                        'amended by khoo on 21/07/2006, added DSSN
                        strVal = strVal + "||" + tabUld.GetFieldValues(i, "DEST,DSSN,POSF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA")
                        'strVal = strVal + "||" + tabUld.GetFieldValues(i, "DEST,POSF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA")
                        strVal = strVal + "||" + tabUld.GetFieldValues(i, "CDAT,LSTU,USEC,USEU")
                        strVal = CleanString(strVal, FOR_SERVER, False)
                        strVal = Replace(strVal, "|", ",", 1, -1, vbBinaryCompare)
                        strFrmDataValues = strVal
                        strVal = CleanNullValues(strVal)
                        tabUld.SetLineTag i, strCurrValues
                        frmData.tabData(5).InsertTextLine strFrmDataValues, False
                        strVal = CleanNullValues(strVal)
                        olInsertString = olInsertString + strVal + vbLf
'                        If frmData.CallServer("IRT", "ULDTAB", strFieldList, strVal, "", "230") = False Then
'                            MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
'                        End If
                        ilCurrentCount = ilCurrentCount + 1
                        ilTotalCount = ilTotalCount + 1
                        ilNoOfInserts = ilNoOfInserts + 1
                    End If
                Case colOrange
                    strCommand = "DRT"
                    If llLine > -1 Then
                        tabTmp.InsertTextLine CStr(llLine), False
                        strLinesToDelete = strLinesToDelete + CStr(i) + ","
                        olDeleteString = olDeleteString + CStr(strUrno) + vbLf
'                        If frmData.CallServer("DRT", "ULDTAB", "", "", "WHERE URNO=" & strUrno, "230") = False Then
'                            MsgBox "Delete Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
'                        End If
                        ilCurrentCount = ilCurrentCount + 1
                        ilTotalCount = ilTotalCount + 1
                        ilNoOfDeletes = ilNoOfDeletes + 1
                    End If
            End Select
            tabUld.SetLineColor i, tCol, vbWhite
            tabUld.Refresh
            ' 500 rows in stings?
            If ilCurrentCount = 500 Then
                If ilNoOfInserts > 0 Then
                    frmData.CallServer "REL", "ULDTAB", "LATE,NOBC,NOLOG", olInsertString, "LATE,NOBC,NOLOG", "230"
                End If
                If ilNoOfUpdates > 0 Then
                    frmData.CallServer "REL", "ULDTAB", "LATE,NOBC,NOLOG", olUpdateString, "LATE,NOBC,NOLOG", "230"
                End If
                If ilNoOfDeletes > 0 Then
                    frmData.CallServer "REL", "ULDTAB", "LATE,NOBC,NOLOG", olDeleteString, "LATE,NOBC,NOLOG", "230"
                End If
                ilCurrentCount = 0
                ilNoOfInserts = 0
                ilNoOfUpdates = 0
                ilNoOfDeletes = 0
                olInsertString = olIRT + strFieldList + vbLf 'CString("\n");
                olUpdateString = olURT + strFieldList + ",[URNO=:VURNO]" + vbLf 'CString("\n");
                olDeleteString = olDRT + ",[URNO=:VURNO]" + vbLf 'CString("\n");
            End If
            
        End If
    Next i
    If ilNoOfInserts > 0 Then
        If frmData.CallServer("REL", "ULDTAB", "", olInsertString, "LATE,NOBC,NOLOG", "230") = False Then
            MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
        End If
    End If
    If ilNoOfUpdates > 0 Then
        If frmData.CallServer("REL", "ULDTAB", "", olUpdateString, "LATE,NOBC,NOLOG", "230") = False Then
            MsgBox "Update Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
        End If
    End If
    If ilNoOfDeletes > 0 Then
        If frmData.CallServer("REL", "ULDTAB", "LATE,NOBC,NOLOG", olDeleteString, "LATE,NOBC,NOLOG", "230") = False Then
            MsgBox "Delete Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
        End If
    End If
    
    ' Been any database action?
    If ilTotalCount > 0 Then
        'frmData.CallServer "SBC", "ULDTAB", "RELULD", frmData.aUfis.GetWorkStationName, omAftUrno, "230"
        frmData.CallServer "SBC", "ULDTAB", "RELULD", frmData.ULogin.GetWorkStationName, omAftUrno, "230"
        frmData.CallServer "TRIG", "TRIG", "FLNU", omAftUrno, "ULD", "230"
    End If
    
    Synchronise "ULDTAB", False
    
    If Len(strLinesToDelete) > 0 Then
        If Mid(strLinesToDelete, Len(strLinesToDelete), 1) = "," Then
            strLinesToDelete = Mid(strLinesToDelete, 1, Len(strLinesToDelete) - 1)
        End If
        cnt = ItemCount(strLinesToDelete, ",")
        For i = cnt - 1 To 0 Step -1
            llLine = GetRealItem(strLinesToDelete, CInt(i), ",")
            tabUld.DeleteLine llLine
        Next i
        tabUld.SetCurrentSelection -1
    End If
    tabTmp.Sort "0", True, True
    cnt = tabTmp.GetLineCount - 1
    For i = cnt To 0 Step -1
        frmData.tabData(5).DeleteLine CLng(tabTmp.GetColumnValue(i, 0))
    Next i
    tabUld.Refresh
    tabTmp.ResetContent
    'Sort and recreate the indexes
    frmData.tabData(5).Sort "0,1", True, True
    tabUld.EnableInlineEdit True
    bmIsInplaceActive = False
'**** Handle the flight related remark field
    If Len(txtFlightRemark.Text) > 0 Then
        strRet = frmData.tabData(6).GetLinesByIndexValue("RURN", omAftUrno, 0)
        llLine = frmData.tabData(6).GetNextResultLine
        myDate = Now
        strTmp = MakeCedaDate(myDate)
        strFieldList = "URNO,RURN,APPL,RTAB,PURP,TEXT,CDAT,LSTU,USEC,USEU"
        If llLine > -1 Then 'value is available
            strUrno = frmData.tabData(6).GetFieldValue(llLine, "URNO")
            strWhere = "WHERE URNO='" & strUrno & "'"
            strVal = frmData.tabData(6).GetFieldValues(llLine, "URNO,RURN,APPL,RTAB,PURP") + ","
            strVal = strVal + CleanString(txtFlightRemark.Text, FOR_SERVER, True) + "," + frmData.tabData(6).GetFieldValue(llLine, "CDAT") + ","
            strVal = strVal + strTmp + "," + frmData.tabData(6).GetFieldValue(llLine, "USEC") + ","
            strVal = strVal + strCurrentUser
            If frmData.CallServer("URT", "REMTAB", strFieldList, strVal, strWhere, "230") = False Then
                MsgBox "Update failed for Flight related remark!"
            End If
        Else
            ' A new record must be inserted
            strVal = frmData.GetNextUrno + "," + omAftUrno + ","
            strVal = strVal + "ULDMGR,AFT,REMA," + CleanString(txtFlightRemark.Text, FOR_SERVER, True) + ","
            strVal = strVal + strTmp + "," + strTmp + "," + strCurrentUser + "," + strCurrentUser
            If frmData.CallServer("IRT", "REMTAB", strFieldList, strVal, "", "230") = False Then
                MsgBox "Insert failed for Flight related remark!"
            End If
        End If
    End If
    lblCaption(2).Caption = CStr(tabUld.GetLineCount) & " Entries"

End Sub

Public Function Synchronise(strTableName As String, bpActive As Boolean) As Boolean
    Dim olCurrentTime As Date
    Dim olToTime As Date
    Dim strFrom As String
    Dim strTo As String
    Dim strData As String
    Dim strSelection As String
    Dim blRet As Boolean
    
    blRet = True
    olCurrentTime = Now
    olToTime = DateAdd("n", 2, olCurrentTime)
    strFrom = MakeCedaDate(olCurrentTime)
    strTo = MakeCedaDate(olToTime)
    If bpActive = True Then
        strData = "*CMD*,SYNTAB,SYNC," + strTableName + ",N," + strFrom + "," + strTo + ", "
        strSelection = "LATE"
        frmData.SetServerParameters
        frmData.aUfis.CallServer "REL", "", "", strData, strSelection, "230"
        If frmData.aUfis.Selection = "NOT OK" Then
            blRet = False
        End If
         
         
    Else
        strData = "*CMD*,SYNTAB,SYNC," + strTableName + ", ," + strFrom + "," + strTo + ", "
        strSelection = "LATE"
        frmData.SetServerParameters
        frmData.aUfis.CallServer "REL", "", "", strData, strSelection, "230"
    End If
    Synchronise = blRet
End Function

Private Sub cmdShowTelex_Click()
    Dim llLine As Long
    Dim strRet As String
    Dim strAftUrno As String
    Dim strUrnoList As String
    
    frmMain.tabTmp.ResetContent
    frmMain.tabTmp.HeaderString = "URNO"
    frmMain.tabTmp.HeaderLengthString = "100"
    frmMain.tabTmp.LogicalFieldList = "URNO"
    frmMain.tabTmp.SetUniqueFields "URNO"
    
     'tabFlights(1).GetCurrentSelected
    strRet = frmData.tabData(3).GetLinesByIndexValue("FLNU", omAftUrno, 0)
    llLine = frmData.tabData(3).GetNextResultLine
    strUrnoList = ""
    While llLine > -1
        frmMain.tabTmp.InsertTextLine frmData.tabData(3).GetFieldValue(llLine, "RURN"), False
        llLine = frmData.tabData(3).GetNextResultLine
    Wend
    strUrnoList = frmMain.tabTmp.GetBuffer(0, frmMain.tabTmp.GetLineCount - 1, ",")
    frmTelex.ReadTelexes strUrnoList
    frmTelex.Show , frmMain
End Sub


Private Sub cmdSync_Click()
    Dim blRet As Boolean

    blRet = Synchronise("ULDTAB", True)
    blRet = Synchronise("ULDTAB", True)
    blRet = Synchronise("ULDTAB", False)
    blRet = Synchronise("ULDTAB", True)

End Sub

Private Sub dtTime_Change()
    Dim curSel As Long
    Dim myDat As Date
    Dim strDat As String
    
    curSel = tabUld.GetCurrentSelected
    If curSel > -1 Then
        myDat = dtTime.Value
        strDat = Format(myDat, "hh:mm\/DD")
        tabUld.SetFieldValues curSel, strTimePickerField, strDat
        CheckValuesChanged curSel
    End If
End Sub

Private Sub dtTime_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim curSel As Long
    Dim myDat As Date
    Dim strDat As String
        
    curSel = tabUld.GetCurrentSelected
    If curSel > -1 Then
        myDat = dtTime.Value
        strDat = Format(myDat, "hh:mm\/DD")
        tabUld.SetFieldValues curSel, strTimePickerField, strDat
        CheckValuesChanged curSel
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If lmCurrEditCol = 5 Or lmCurrEditCol = 6 Then
        If Chr(KeyAscii) = ":" Or Chr(KeyAscii) = "/" Or (KeyAscii >= 48 And KeyAscii <= 57) Or KeyAscii = 8 Then
        Else
            KeyAscii = 0
        End If
    End If
End Sub

Private Sub Form_Load()
    Dim ilLeft As Long
    Dim ilTop As Long
    Dim ilWidth As Long
    Dim ilHeight As Long
    Dim thW As Long
    thW = Me.Width
    
    myTabScrollPos = 0
'Read position from registry and move the window
    ilLeft = GetSetting(AppName:="ULD_Manager_ULDHANDLING", _
                        section:="Startup", _
                        Key:="myLeft", _
                        Default:=-1)
    ilTop = GetSetting(AppName:="ULD_Manager_ULDHANDLING", _
                        section:="Startup", _
                        Key:="myTop", _
                        Default:=-1)
    ilWidth = GetSetting(AppName:="ULD_Manager_ULDHANDLING", _
                        section:="Startup", _
                        Key:="myWidth", _
                        Default:=-1)
    ilHeight = GetSetting(AppName:="ULD_Manager_ULDHANDLING", _
                        section:="Startup", _
                        Key:="myHeight", _
                        Default:=-1)
    ilWidth = thW
    If ilTop = -1 Or ilLeft = -1 Or ilWidth = -1 Or ilHeight = -1 Then
    Else
        Me.Move ilLeft, ilTop, ilWidth, ilHeight
    End If
    
    picCommon.left = 0
    picCommon.top = 0
    picCommon.Width = Me.ScaleWidth
    picCommon.Height = Me.ScaleHeight
    Set picGroup(0).Container = picCommon
    Set picGroup(1).Container = picCommon
    Set picGroup(2).Container = picCommon
    Set cmdShowTelex.Container = picCommon
    Set cmdPrint.Container = picCommon
    Set cmdExit.Container = picCommon
    Set lblCaption(0).Container = picCommon
    Set lblCaption(1).Container = picCommon
    Set lblCaption(2).Container = picCommon
    '### SHA20060412
    Set lblCaption(3).Container = picCommon
    DrawObjBackgrounds
    
    dtTime.CustomFormat = "HH:mm/dd"
     
    tabTmp.ResetContent
    tabTmp.HeaderString = "LINE"
    tabTmp.LogicalFieldList = "LINE"
    tabTmp.HeaderLengthString = "100"
    tabTmp.LineHeight = 16
    tabTmp.FontName = "Courier New"
    tabTmp.FontSize = 16
    tabTmp.SetHeaderFont 18, True, False, True, 0, "Courier New"
    tabTmp.EnableHeaderSizing True
    tabTmp.SetTabFontBold True
    
End Sub

Public Sub SetFlightRemark(strRema As String)
    omFlightRemark = strRema
    txtFlightRemark.Text = omFlightRemark
End Sub

Public Sub SetData(strAftUrno As String)
    omAftUrno = strAftUrno
    MyInit
    SetStatistik
End Sub


Public Sub DrawObjBackgrounds()
'    DrawBackGround picGroup(0), 7, True, True, 50
'    DrawBackGround picGroup(1), 7, True, True, 50
'    DrawBackGround picGroup(2), 7, True, True, 50
'    DrawBackGround picCommon, 7, True, True, 40

    DrawBackGround picGroup(0), 7, True, True, 20
    DrawBackGround picGroup(1), 7, True, True, 90
    DrawBackGround picGroup(2), 7, True, True, 90
    DrawBackGround picCommon, 7, True, True, 100
End Sub

Public Sub MyInit()
    Dim llLine As Long
    Dim strValue As String
    Dim strDSSN As String
    Dim strPOSF As String
    Dim blAddIt As Boolean
    Dim totalCnt As Long
    Dim uldCnt As Integer
    Dim emptyCnt As Integer
    Dim strMWO As String
    Dim strContent As String
    Dim strSSTP As String
    Dim i As Long

    '### SHA20060412 New Statistics Display
    Dim Bcnt As Integer
    Dim Mcnt As Integer
    Dim Ccnt As Integer


    For i = 0 To txtFlight.count - 1
        txtFlight(i).Text = ""
    Next i
    totalCnt = 0
    emptyCnt = 0
    uldCnt = 0
    bmIsInplaceActive = False
    frmData.tabData(0).GetLinesByIndexValue "URNO", omAftUrno, 0
    llLine = frmData.tabData(0).GetNextResultLine
    If llLine > -1 Then
        omCurrAdid = frmData.tabData(0).GetFieldValue(llLine, "ADID")
        If omCurrAdid = "A" Then
            Me.Caption = "ULD-Arrival Handling ..."
            txtFlight(0).Text = frmData.tabData(0).GetFieldValue(llLine, "FLNO")
            strValue = frmData.tabData(0).GetFieldValue(llLine, "STOA")
            If strValue <> "" Then
                txtFlight(1).Text = Format(CedaFullDateToVb(strValue), "DD.MM.YY")
            End If
            strValue = frmData.tabData(0).GetFieldValue(llLine, "STOA")
            If strValue <> "" Then
                txtFlight(2).Text = Format(CedaFullDateToVb(strValue), "hh:mm")
            End If
            strValue = frmData.tabData(0).GetFieldValue(llLine, "ETAI")
            If strValue <> "" Then
                txtFlight(3).Text = Format(CedaFullDateToVb(strValue), "hh:mm")
            End If
            txtFlight(4).Text = frmData.tabData(0).GetFieldValue(llLine, "ORG3")
            txtFlight(5).Text = frmData.tabData(0).GetFieldValue(llLine, "REGN")
            lblSched.Caption = "STA:"
            lblEst.Caption = "ETA:"
            lblApt.Caption = "Orig.:"
        Else
            Me.Caption = "ULD-Departure Handling ..."
            txtFlight(0).Text = frmData.tabData(0).GetFieldValue(llLine, "FLNO")
            strValue = frmData.tabData(0).GetFieldValue(llLine, "STOD")
            If strValue <> "" Then
                txtFlight(1).Text = Format(CedaFullDateToVb(strValue), "DD.MM.YY")
            End If
            strValue = frmData.tabData(0).GetFieldValue(llLine, "STOD")
            If strValue <> "" Then
                txtFlight(2).Text = Format(CedaFullDateToVb(strValue), "hh:mm")
            End If
            strValue = frmData.tabData(0).GetFieldValue(llLine, "ETDI")
            If strValue <> "" Then
                txtFlight(3).Text = Format(CedaFullDateToVb(strValue), "hh:mm")
            End If
            txtFlight(4).Text = frmData.tabData(0).GetFieldValue(llLine, "DES3")
            txtFlight(5).Text = frmData.tabData(0).GetFieldValue(llLine, "REGN")
            lblSched.Caption = "STD:"
            lblEst.Caption = "ETD:"
            lblApt.Caption = "Dest.:"
        End If
    End If
    InitTabs
    
    frmData.tabData(5).GetLinesByIndexValue "FLNU", omAftUrno, 0
    llLine = frmData.tabData(5).GetNextResultLine
    While llLine > -1
        'amended by khoo on 20/07/2006, added DSSN
        'changes at frmData also, by add the DSSN in the tabData(5)-> Properties -> Tag
        'ULDTAB;URNO,FLNU,ULDN,TYPE,CONF,CONT,ADDI,ORIG,DEST,DSSN,POSF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA,RESE,CDAT,LSTU,USEC,USEU;
        'amended by khoo on 11/04/2008, reset strValue
        strValue = ""
        strValue = strValue + frmData.tabData(5).GetFieldValues(llLine, "DSSN,POSF,ULDN,CONT") + ",,"
        'strValue = frmData.tabData(5).GetFieldValues(llLine, "POSF,ULDN,CONT") + ",,"
        strValue = strValue + frmData.tabData(5).GetFieldValues(llLine, "ADDI,DEST,CONF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA,URNO,FLNU,CDAT,USEC,LSTU,USEU")
        strValue = Replace(strValue, ",", "|", 1, -1, vbBinaryCompare)
        strValue = CleanString(strValue, FOR_CLIENT, False)
        tabUld.InsertTextLine strValue, False
        tabUld.SetLineTag tabUld.GetLineCount - 1, strValue
        llLine = frmData.tabData(5).GetNextResultLine
    Wend
    ' if no data was in ULDTAB for the current flight
    ' insert the CPM Messages as default if the are any
    frmData.tabData(3).GetLinesByIndexValue "FLNU", omAftUrno, 0
    llLine = frmData.tabData(3).GetNextResultLine
    If tabUld.GetLineCount = 0 Then
        While llLine > -1
            blAddIt = False
            strValue = frmData.tabData(3).GetFieldValues(llLine, "DSSN")
            strDSSN = strValue
            strSSTP = frmData.tabData(3).GetFieldValues(llLine, "SSTP")
            'If strValue = "CPM" Or strValue = "DLS" Or strValue = "KRI" Then
            ' !!!!!!!!!!!!!!!!!!!! SHA TEST LINE
            ' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            If strValue = "CPM" Or strValue = "DLS" Or strValue = "KRI" Or strValue = "BRS" Then
                strPOSF = frmData.tabData(3).GetFieldValue(llLine, "ULDP")
                strValue = Trim(frmData.tabData(3).GetFieldValue(llLine, "ULDT"))
                If ((strValue = "" And Is50(strPOSF) = True And omCurrAdid = "A") Or (strValue <> "" And omCurrAdid = "A")) Then
                    blAddIt = True
                End If
                If omCurrAdid = "D" Then
                    If strDSSN = "KRI" And strValue <> "" Then
                        blAddIt = True
                    End If
                    If strDSSN = "DLS" And strValue <> "" Then
                        blAddIt = True
                    End If
                    '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    If strDSSN = "BRS" And strValue <> "" Then
                        blAddIt = True
                    End If
                    '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    
                End If
                If strSSTP = "N" Then
                    blAddIt = False
                End If
                If blAddIt = True Then 'Then the line must be inserted
                    AddNewUld False
                    '"POSF,ULDN,CONT,ADDI,DEST,CONF,TIMF,TIMH,REMA,URNO,FLNU,CDAT,USEC,LSTU,USEU"
                    
                    tabUld.SetFieldValues tabUld.GetLineCount - 1, "DSSN", strDSSN
                    
                    strMWO = frmData.tabData(3).GetLineValues(llLine)
                    strValue = frmData.tabData(3).GetFieldValue(llLine, "ULDP")
                    tabUld.SetFieldValues tabUld.GetLineCount - 1, "POSF", strValue
                    strValue = frmData.tabData(3).GetFieldValue(llLine, "ULDT")
                    tabUld.SetFieldValues tabUld.GetLineCount - 1, "ULDN", strValue
                    strValue = frmData.tabData(3).GetFieldValue(llLine, "APC3")
                    tabUld.SetFieldValues tabUld.GetLineCount - 1, "DEST", strValue
                    strValue = frmData.tabData(3).GetFieldValue(llLine, "ADDI")
                    tabUld.SetFieldValues tabUld.GetLineCount - 1, "ADDI", strValue
                    
                    ' added by kkh on 09/11/2006 to show the rmrk from LOATAB
                    strValue = frmData.tabData(3).GetFieldValue(llLine, "RMRK")
                    tabUld.SetFieldValues tabUld.GetLineCount - 1, "REMA", strValue
                    
                    strValue = frmData.tabData(3).GetFieldValue(llLine, "SSTP")
                    'strValue = GetContent(strValue)
                    '### SHA20060412 ERROR: the cont value comes as BY,BJ,.. !
                    'If (strValue = "B" Or strValue = "C" Or strValue = "M") Then uldCnt = uldCnt + 1
                    If (left$(strValue, 1) = "B" Or strValue = "C" Or strValue = "M") Then uldCnt = uldCnt + 1
                    If (strValue = "X") Then emptyCnt = emptyCnt + 1
                    totalCnt = totalCnt + 1
                    
                    '### SHA20060412 New Statistics Display
                    If left$(strValue, 1) = "B" Then Bcnt = Bcnt + 1
                    If strValue = "C" Then Ccnt = Ccnt + 1
                    If strValue = "M" Then Mcnt = Mcnt + 1
                    
                    tabUld.SetFieldValues tabUld.GetLineCount - 1, "CONT", strValue
                End If
            End If
            llLine = frmData.tabData(3).GetNextResultLine
        Wend
    End If
    lblCaption(2).Caption = " Entries ULD: " & uldCnt & "  Empty: " & emptyCnt & "  Total: " & CStr(tabUld.GetLineCount)
    lblCaption(2).Refresh
    
    
    '### SHA20060412 New Statistics Display
    lblCaption(3).Caption = " Baggage: " & Bcnt & "  Cargo: " & Ccnt & "  Mail: " & Mcnt
    
    
    
    tabUld.SetCurrentSelection 0
    tabUld.Refresh
    tabLoaTab.Refresh
    
'**** Handling of the flight related remark field
    txtFlightRemark.Text = ""
    frmData.tabData(6).GetLinesByIndexValue "RURN", omAftUrno, 0
    llLine = frmData.tabData(6).GetNextResultLine
    If llLine > -1 Then
        txtFlightRemark.Text = CleanString(frmData.tabData(6).GetFieldValue(llLine, "TEXT"), FOR_CLIENT, True)
    End If
    
'*** Handling of BDITAB (Bagge Departure Information)
    InitBDI
End Sub

Public Sub InitBDI()
    Dim llLine As Long
    Dim strRet As String
    
    tabBDI.ResetContent
    tabBDI.HeaderString = "URNO,Time,Information"
    tabBDI.LogicalFieldList = "URNO,TIME,TEXT"
    tabBDI.HeaderLengthString = "-1,80,410"
    tabBDI.LifeStyle = True
    tabBDI.FontName = "Arial"
    tabBDI.FontSize = 14
    tabBDI.HeaderFontSize = 14
    tabBDI.LineHeight = 14
    tabBDI.EnableInlineEdit True
    tabBDI.InplaceEditUpperCase = False
    tabBDI.PostEnterBehavior = 2
    'tabBDI.InplaceEditSendKeyEvents = True
    tabBDI.DateTimeSetColumnFormat 1, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm"
    
    tabBDI.ColumnWidthString = "10,20,100"
    tabBDI.NoFocusColumns = "0,1"
    
    strRet = frmData.tabData(7).GetLinesByIndexValue("FLNU", omAftUrno, 0)
    llLine = frmData.tabData(7).GetNextResultLine
    
    While llLine > -1
        tabBDI.InsertTextLine frmData.tabData(7).GetFieldValues(llLine, "URNO,TIME,TEXT"), False
        tabBDI.SetLineTag tabBDI.GetLineCount - 1, "SAVED"
        llLine = frmData.tabData(7).GetNextResultLine
    Wend
    tabBDI.Sort "1", False, True
    tabBDI.InsertTextLineAt 0, ",,", True
    tabBDI.Refresh
End Sub
Public Function GetContent(strSSTP As String) As String
    Dim strRet As String
    
'    'check for BAGGAGE content
'    If strSSTP = "B" Or strSSTP = "TB" Or strSSTP = "FB" Or _
'                strSSTP = "Q" Or strSSTP = "D" Or strSSTP = "F" Or _
'                InStr(1, strSSTP, "B") = 1 Then
'        strRet = "B"
'    End If
'
'    'check for MAIL content
'    If strRet = "" Then
'        If strSSTP = "M" Then
'            strRet = "M"
'        End If
'    End If
'    'check for CARGO content
'    If strRet = "" Then
'        If strSSTP = "Z" Or strSSTP = "H" Or strSSTP = "TC" Or _
'           strSSTP = "W" Or strSSTP = "U" Or _
'           strSSTP = "E" Or strSSTP = "C" Or strSSTP = "LC" _
'           Then
'            strRet = "C"
'        End If
'    End If
'    'check empty
'    'If strSSTP = "N" Or strSSTP = "X" Then 'customer considers this to be wrong
'    If strSSTP = "X" Then
'        strRet = "X"
'    End If
    GetContent = strSSTP 'strRet
End Function
Public Sub InitTabs()
    Dim strColor As String
    Dim strComboValues As String
    Dim llColno As Long
    
    strColor = vbBlue & ", " & vbBlue
    tabUld.ResetContent
    tabUld.SetFieldSeparator "|"
    
    If omCurrAdid = "A" Then
        'tabUld.HeaderString = "Pos.|ULD-No|Content| |Additional Details|Dest.|Conf.|Time-f-A/C|T. at B/C/M|OFLD|TAAC|TFBU|Remark|URNO|FLNU|CDAT|USEC|LSTU|USEU"
        tabUld.HeaderString = "Src.|Pos.|ULD-No|Content| |Additional Details|Dest.|Conf.|Time-f-A/C|T. at B/C/M|OFLD|TAAC|TFBU|Remark|URNO|FLNU|CDAT|USEC|LSTU|USEU"
        'URNO,FLNU,ULDN,TYPE,CONF,CONT,ORIG,DEST,POSF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA,RESE,CDAT,LSTU,USEC,USEU;
        tabUld.LogicalFieldList = "DSSN,POSF,ULDN,CONT,COMBO,ADDI,DEST,CONF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA,URNO,FLNU,CDAT,USEC,LSTU,USEU"
        tabUld.CursorDecoration "DSSN,POSF,ULDN,CONT,COMBO,ADDI,DEST,CONF,TIMF,TIMH,REMA,URNO", "T,B", "2,2", strColor
        tabUld.HeaderLengthString = "25,40,110,80,20,200,40,40,90,115,-1,-1,-1,230,80,80,140,80,140,80"
        llColno = GetRealItemNo(tabUld.LogicalFieldList, "OFLD")
        tabUld.ResetColumnBoolProperty llColno
    Else
        'tabUld.HeaderString = "Pos.|ULD-No|Content| |Additional Details|Dest.|Conf.|T. at A/C|T. fm. B/C/M|OffLd|T. fm A/C|T. at B/C/M|Remark|URNO|FLNU|CDAT|USEC|LSTU|USEU"
        tabUld.HeaderString = "Src.|Pos.|ULD-No|Content| |Additional Details|Dest.|Conf.|T. at A/C|T. fm. B/C/M|OffLd|T. fm A/C|T. at B/C/M|Remark|URNO|FLNU|CDAT|USEC|LSTU|USEU"
        tabUld.LogicalFieldList = "DSSN,POSF,ULDN,CONT,COMBO,ADDI,DEST,CONF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA,URNO,FLNU,CDAT,USEC,LSTU,USEU"
        tabUld.CursorDecoration "DSSN,POSF,ULDN,CONT,COMBO,ADDI,DEST,CONF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA,URNO", "T,B", "2,2", strColor
        tabUld.HeaderLengthString = "25,40,110,80,20,200,40,40,70,70,35,70,70,130,80,80,140,80,140,80"
        llColno = GetRealItemNo(tabUld.LogicalFieldList, "OFLD")
        tabUld.SetColumnBoolProperty llColno, "Y", ""
    End If
    tabUld.ColumnWidthString = "3,10,12,15,10,128,4,1,14,14,1,14,14,999,10,10,14,32,14,32"
    tabUld.LifeStyle = True
    tabUld.FontName = "Arial"
    tabUld.FontSize = 14
    tabUld.ComboMinWidth = 20
    tabUld.HeaderFontSize = 14
    tabUld.LineHeight = 18
    tabUld.SetTabFontBold True
    tabUld.ShowHorzScroller True
    tabUld.EnableHeaderSizing True
    tabUld.AutoSizeByHeader = True
    llColno = GetRealItemNo(tabUld.LogicalFieldList, "CONF")
    tabUld.SetColumnBoolProperty llColno, "Y", ""
    llColno = GetRealItemNo(tabUld.LogicalFieldList, "CDAT")
    tabUld.DateTimeSetColumnFormat llColno, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm"
    llColno = GetRealItemNo(tabUld.LogicalFieldList, "LSTU")
    tabUld.DateTimeSetColumnFormat llColno, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm"
    tabUld.DefaultCursor = False
    strComboValues = "" & vbLf
    strComboValues = strComboValues & "B" & vbLf
    strComboValues = strComboValues & "M" & vbLf
    strComboValues = strComboValues & "C" & vbLf
    strComboValues = strComboValues & "X" & vbLf
    strComboValues = strComboValues & "MIX" & vbLf
    strComboValues = strComboValues & "NF" & vbLf
    strComboValues = strComboValues & "N" & vbLf
    strComboValues = strComboValues & "Q" & vbLf
    strComboValues = strComboValues & "S" & vbLf
    strComboValues = strComboValues & "" & vbLf
    tabUld.ComboResetObject "CONTENT"
    tabUld.CreateComboObject "CONTENT", 3, 1, "", 50
    tabUld.ComboSetColumnLengthString "CONTENT", "80"
    tabUld.SetComboColumn "CONTENT", 3
    tabUld.ComboAddTextLines "CONTENT", strComboValues, vbLf
    tabUld.ComboMinWidth = 10
'    tabUld.NoFocusColumns = "2,4,5,6,8,9,10,11,12,13"
    tabUld.NoFocusColumns = "3,6,7,8,9,10,11,13,14,15,16,17,18"
    tabUld.EnableInlineEdit True

    tabLoaTab.ResetContent
                             'DSSN,ULDT,ULDP,APC3,VALU,URNO,RURN
    tabLoaTab.HeaderString = "DSSN,ULDT,ULDP,Dest.,Valu,URNO,RURN"
    tabLoaTab.LogicalFieldList = "DSSN,ULDT,ULDP,APC3,VALU,URNO,RURN"
    tabLoaTab.HeaderLengthString = "40,150,70,40,90,100,100"
    tabLoaTab.LifeStyle = True
    tabLoaTab.CursorLifeStyle = True
    tabLoaTab.FontName = "Arial"
    tabLoaTab.FontSize = 14
    tabLoaTab.HeaderFontSize = 14
    tabLoaTab.AutoSizeByHeader = True
    tabLoaTab.SetTabFontBold True
    tabLoaTab.ShowHorzScroller True
    tabLoaTab.EnableHeaderSizing True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim ilLeft As Long
    Dim ilTop As Long
    Dim ilHeight As Long
    Dim ilWidth As Long
    
    ilLeft = Me.left
    ilTop = Me.top
    ilWidth = Me.Width
    ilHeight = Me.Height
    SaveSetting "ULD_Manager_ULDHANDLING", _
                "Startup", _
                "myLeft", _
                ilLeft
    SaveSetting "ULD_Manager_ULDHANDLING", _
                "Startup", _
                "myTop", ilTop
    SaveSetting "ULD_Manager_ULDHANDLING", _
                "Startup", _
                "myWidth", ilWidth
    SaveSetting "ULD_Manager_ULDHANDLING", _
                "Startup", _
                "myHeight", ilHeight
    If UnloadMode = 0 Then
        Cancel = True
        Me.Hide
    End If
End Sub



Private Sub tabBDI_EditPositionChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    If LineNo > 0 Then
        tabBDI.SetInplaceEdit 0, 2, True
        tabBDI.SetCurrentSelection 0
    End If
End Sub

Private Sub tabBDI_InplaceEditCell(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    Dim s As String
    Dim dat As Date
    Dim strText As String
    Dim strTime As String
    Dim strUrno As String
    Dim strValues As String
    Dim strTag As String
    
    strTag = tabBDI.GetLineTag(LineNo)
    dat = Now
    strText = NewValue
    If LineNo = 0 And strTag = "" And strText <> "" Then
        'MsgBox strBDIText
        tabBDI.EnableInlineEdit False
        strUrno = frmData.GetNextUrno()
        strTime = Format(dat, "YYYYMMDDhhmmss")
        'strValues = frmData.GetNextUrno() + "," + Format(dat, "YYYYMMDDhhmmss") + "," + strText
        tabBDI.SetFieldValues 0, "URNO", strUrno
        tabBDI.SetFieldValues 0, "TIME", strTime
        tabBDI.SetFieldValues 0, "TEXT", strText
        tabBDI.SetLineTag 1, "SAVED"
        strValues = strUrno + "," + omAftUrno + "," + strTime + "," + strText
        If frmData.CallServer("IRT", "BDITAB", "URNO,FLNU,TIME,TEXT", strValues, "", "230") = False Then
            MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
        Else
            frmData.tabData(7).InsertTextLine strValues, False
            frmData.tabData(7).Sort "1", True, True
        End If
        tabBDI.EnableInlineEdit True
        tabBDI.InsertTextLineAt 0, " , , ", False
        tabBDI.Refresh
    End If
End Sub

Private Sub tabBDI_SendMouseMove(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim strVal As String
    If ColNo = 2 Then
        strVal = tabBDI.GetFieldValue(LineNo, "TEXT")
        tabBDI.ToolTipText = strVal
    End If
End Sub

Private Sub tabUld_BoolPropertyChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As Boolean)
    CheckValuesChanged LineNo
End Sub

Private Sub tabUld_CloseInplaceEdit(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strValue As String
    
    CheckValuesChanged LineNo
    If bmIsInplaceActive = True Then
        bmIsInplaceActive = False
    End If
End Sub

Private Sub tabUld_ComboSelChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal OldValue As String, ByVal NewValues As String)
    If NewValues = "" Then
        tabUld.SetInplaceEdit LineNo, 2, True
    Else
        tabUld.SetFieldValues LineNo, "CONT", NewValues
        tabUld.SetFieldValues LineNo, "COMBO", ""
        tabUld.Refresh
    End If
    CheckValuesChanged LineNo
    SetStatistik
End Sub

Private Sub tabUld_EditPositionChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    CheckValuesChanged LineNo
    If bmIsInplaceActive = False Then
        bmIsInplaceActive = True
    End If
    lmCurrEditCol = ColNo
    lmCurrEditLine = LineNo
End Sub

Private Sub tabUld_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    If bmIsInplaceActive = False Then
        If Key = vbKeyDelete Then
            SetLineDeleted
        End If
        tabUld.Refresh
    End If
End Sub

Private Sub tabUld_InplaceEditCell(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    Dim i As Long
    i = 0
End Sub

Private Sub tabUld_OnVScroll(ByVal LineNo As Long)
    myTabScrollPos = LineNo
End Sub

Private Sub tabUld_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    CheckValuesChanged LineNo
    strTimePickerField = ""
    SetTimePicker

End Sub

Private Sub tabUld_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
'    If LineNo > -1 And (ColNo = GetRealItemNo(tabUld.LogicalFieldList, "TIMF") Or _
'                        ColNo = GetRealItemNo(tabUld.LogicalFieldList, "TIMH")) Then
    If LineNo > -1 Then
        Select Case (ColNo)
            Case GetRealItemNo(tabUld.LogicalFieldList, "TIMF")
                strTimePickerField = "TIMF"
                SetTimePicker
            Case GetRealItemNo(tabUld.LogicalFieldList, "TIMH")
                strTimePickerField = "TIMH"
                SetTimePicker
            Case GetRealItemNo(tabUld.LogicalFieldList, "TAAC")
                strTimePickerField = "TAAC"
                SetTimePicker
            Case GetRealItemNo(tabUld.LogicalFieldList, "TFBU")
                strTimePickerField = "TFBU"
                SetTimePicker
            Case Else
                strTimePickerField = ""
                dtTime.Visible = False
        End Select
        
'        If ColNo = GetRealItemNo(tabUld.LogicalFieldList, "TIMF") Then
'            strTimePickerField = "TIMF"
'            SetTimePicker
'        Else
'            strTimePickerField = "TIMH"
'            SetTimePicker
'        End If
'    Else
'        strTimePickerField = ""
'        dtTime.Visible = False
    End If
End Sub

Private Sub SetTimePicker()
    Dim i As Integer
    Dim currSel As Long
    Dim strTime As String
    Dim tmpLeft As Long
    Dim tmpRight As Long
    Dim tmpFaktor As Long
    Dim tmpTop As Long
    Dim arrHeader() As String
    Dim idx As Long
    Dim datTime As Date
    Dim strField As String
    
    strField = strTimePickerField
    If strField = "" Then
        dtTime.Visible = False
        Exit Sub
    End If
    currSel = tabUld.GetCurrentSelected
    If currSel > -1 Then
        dtTime.Visible = True
        strTime = tabUld.GetFieldValue(currSel, strField)
        tmpLeft = 0
        tmpRight = 0
        tmpFaktor = currSel - tabUld.GetVScrollPos + 1
        tmpTop = tabUld.top + (tmpFaktor * (tabUld.LineHeight * 15))
        arrHeader = Split(tabUld.HeaderLengthString, ",")
        idx = GetRealItemNo(tabUld.LogicalFieldList, strField)
        For i = 0 To idx - 1
            tmpLeft = tmpLeft + CLng(arrHeader(i) * 15)
        Next i
        tmpLeft = tmpLeft + tabUld.left
        tmpRight = tmpLeft + CLng(arrHeader(idx) * 15)
        dtTime.top = tmpTop
        dtTime.left = tmpLeft
        dtTime.Width = (tmpRight) - (tmpLeft)
    
        If strTime <> "" Then
            datTime = CedaFullDateToVb(strTime)
        Else
            datTime = Now
        End If
        dtTime.Value = datTime
        'dtTime.ShowUpDown = True
        dtTime.Visible = True
        dtTime.SetFocus
    End If
End Sub

Private Sub tabUld_SendMouseMove(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim strVal As String
    Dim ItemNo As Long
    
    ItemNo = GetRealItemNo(tabUld.LogicalFieldList, "REMA")
    If ColNo = ItemNo Then
        strVal = tabUld.GetFieldValue(LineNo, "REMA")
        If strVal <> "" Then
            tabUld.ToolTipText = strVal
        Else
            tabUld.ToolTipText = ""
        End If
    Else
        tabUld.ToolTipText = ""
    End If
End Sub

Public Sub CheckValuesChanged(LineNo As Long)
    Dim strColor As String
    Dim i As Long
    Dim strValues As String
    
    Dim tCol As Long
    Dim bCol As Long
    
    tabUld.GetLineColor LineNo, tCol, bCol
    If LineNo >= 0 Then
        'strValues = tabUld.GetFieldValues(LineNo, "POSF,ULDN,CONT") + "||"
        strValues = tabUld.GetFieldValues(LineNo, "DSSN,POSF,ULDN,CONT") + "||"
        strValues = strValues + tabUld.GetFieldValues(LineNo, "ADDI,DEST,CONF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA,URNO,FLNU,CDAT,USEC,LSTU,USEU") 'tabUld.GetLineValues(LineNo)
        If strValues <> tabUld.GetLineTag(LineNo) Then
            If bCol = vbWhite Then
                tabUld.SetLineColor LineNo, tCol, vbGreen
            End If
            If bCol = colOrange Then
                'tabConnections.SetLineColor LineNo, tCol, vbGreen
            End If
        Else
            If bCol = vbGreen Then
                tabUld.SetLineColor LineNo, tCol, vbWhite
            End If
        End If
        
        Debug.Print strValues
        Debug.Print tabUld.GetLineTag(LineNo)
        tabUld.Refresh
    End If
End Sub

Public Sub SetLineDeleted()
    Dim llCurrLine As Long
    Dim colBack As Long
    Dim colText As Long
    Dim strVal As String
    Dim strOldVal As String
    Dim llLine As Long
    Dim strCount As String
    
    
    llCurrLine = tabUld.GetCurrentSelected
    'kkh DSSN added on 14/04/2008
    'strVal = tabUld.GetFieldValues(llCurrLine, "POSF,ULDN,CONT,ADDI,DEST,CONF,TIMF,TIMH,REMA,URNO,FLNU,CDAT,USEC,LSTU,USEU")
    strVal = tabUld.GetFieldValues(llCurrLine, "DSSN,POSF,ULDN,CONT") + "||"
    strVal = strVal + tabUld.GetFieldValues(llCurrLine, "ADDI,DEST,CONF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA,URNO,FLNU,CDAT,USEC,LSTU,USEU")
    strOldVal = tabUld.GetLineTag(llCurrLine)
    tabUld.GetLineColor llCurrLine, colText, colBack
    If colBack = vbYellow Then
        tabUld.DeleteLine llCurrLine
        tabUld.SetCurrentSelection 0
    Else
        If colBack = colOrange Then
            If strVal = strOldVal Then
                tabUld.SetLineColor llCurrLine, colText, vbWhite
            Else
                tabUld.SetLineColor llCurrLine, colText, vbGreen
            End If
        Else
            tabUld.SetLineColor llCurrLine, colText, colOrange
        End If
    End If
    tabUld.Refresh
    lblCaption(2).Caption = CStr(tabUld.GetLineCount) & " Entries"
End Sub

Private Sub tabUld_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim llTimfCol As Long
    Dim llTimhCol As Long
    Dim llTAAC As Long
    Dim llTFBU As Long
    Dim myDat As Date
    Dim strDat As String
    Dim strCurrVal As String
    
    llTimfCol = GetRealItemNo(tabUld.LogicalFieldList, "TIMF")
    llTimhCol = GetRealItemNo(tabUld.LogicalFieldList, "TIMH")
    llTAAC = GetRealItemNo(tabUld.LogicalFieldList, "TAAC")
    llTFBU = GetRealItemNo(tabUld.LogicalFieldList, "TFBU")
    myDat = Now
    strDat = Format(myDat, "hh:mm\/DD") 'MakeCedaDate(myDat)
    strCurrVal = tabUld.GetColumnValue(LineNo, ColNo)
    strCurrVal = Trim(strCurrVal)
    If llTimfCol = ColNo Or llTimhCol = ColNo Or llTAAC = ColNo Or llTFBU = ColNo Then
        If strCurrVal = "" Then
            tabUld.SetColumnValue LineNo, ColNo, strDat
        Else
            tabUld.SetColumnValue LineNo, ColNo, ""
        End If
        CheckValuesChanged LineNo
    End If
End Sub
