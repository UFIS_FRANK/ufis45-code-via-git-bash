VERSION 5.00
Begin VB.Form frmJobDetails 
   Caption         =   "Staff Information (InfoPC) - Job Details"
   ClientHeight    =   9990
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10170
   Icon            =   "frmJobDetails.frx":0000
   LinkTopic       =   "Form4"
   LockControls    =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9990
   ScaleWidth      =   10170
   StartUpPosition =   1  'CenterOwner
   Tag             =   "150"
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4200
      TabIndex        =   33
      Top             =   9300
      Width           =   1695
   End
   Begin VB.Frame frameJobDetails 
      Caption         =   "Job Details"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3150
      Left            =   120
      TabIndex        =   21
      Top             =   240
      Width           =   9855
      Begin VB.Label lblWorkGroup 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "A2BR"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   7230
         TabIndex        =   64
         Tag             =   "Status"
         Top             =   1095
         Width           =   2430
      End
      Begin VB.Label lblJobRemark 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2310
         TabIndex        =   63
         Tag             =   "Duty"
         Top             =   2535
         Width           =   7380
      End
      Begin VB.Label lblJobGridInformation 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Gate: 01"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   3
         Left            =   2295
         TabIndex        =   62
         Tag             =   "Location"
         Top             =   1590
         Width           =   7380
      End
      Begin VB.Label lblJobGridInformation 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Duty"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   4
         Left            =   2295
         TabIndex        =   61
         Tag             =   "Duty"
         Top             =   2055
         Width           =   7380
      End
      Begin VB.Label lblJobGridInformation 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "A1XY"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   1
         Left            =   2295
         TabIndex        =   60
         Tag             =   "Fctc"
         Top             =   1095
         Width           =   2970
      End
      Begin VB.Label lblJobGridInformation 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Planned"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   2
         Left            =   7230
         TabIndex        =   59
         Tag             =   "Status"
         Top             =   600
         Width           =   2430
      End
      Begin VB.Label Label19 
         Caption         =   "Work group:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5520
         TabIndex        =   58
         Top             =   1140
         Width           =   1695
      End
      Begin VB.Label lblJobGridInformation 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "88:88/DD - 88:88/DD"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   0
         Left            =   2295
         TabIndex        =   30
         Tag             =   "Job"
         Top             =   615
         Width           =   2970
      End
      Begin VB.Label Label23 
         Caption         =   "Location:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   1635
         Width           =   1365
      End
      Begin VB.Label Label29 
         Caption         =   "Status:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5520
         TabIndex        =   27
         Top             =   660
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Flight Remark:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   2580
         Width           =   2130
      End
      Begin VB.Label Label24 
         Caption         =   "Job function:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   1140
         Width           =   1950
      End
      Begin VB.Label Label26 
         Caption         =   "Job time:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   660
         Width           =   1695
      End
      Begin VB.Label Label20 
         Caption         =   "Duty:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   2110
         Width           =   840
      End
   End
   Begin VB.Frame frameOutbound 
      Caption         =   "Departure 02.06.2003"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4635
      Left            =   5160
      TabIndex        =   7
      Top             =   4485
      Width           =   4815
      Begin VB.Label lblFlightOutbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "123"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   7
         Left            =   3525
         TabIndex        =   56
         Tag             =   "CKIT"
         Top             =   4050
         Width           =   1035
      End
      Begin VB.Label lblFlightOutbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "123"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   5
         Left            =   1575
         TabIndex        =   55
         Tag             =   "CKIF"
         Top             =   4050
         Width           =   1035
      End
      Begin VB.Label Label30 
         Caption         =   "to:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3015
         TabIndex        =   57
         Top             =   4095
         Width           =   495
      End
      Begin VB.Label lblFlightOutbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "123"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   6
         Left            =   1575
         TabIndex        =   54
         Tag             =   "BAZ1,BAZ2,BAZ3,BAZ4,BAZ5,BAZ6"
         Top             =   3585
         Width           =   2970
      End
      Begin VB.Label lblFlightOutbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "123"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   4
         Left            =   3525
         TabIndex        =   53
         Tag             =   "GTD1"
         Top             =   3135
         Width           =   1035
      End
      Begin VB.Label lblFlightOutbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "123"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   3
         Left            =   1575
         TabIndex        =   52
         Tag             =   "PSTD"
         Top             =   3135
         Width           =   1035
      End
      Begin VB.Label lblFlightOutboundTime 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "12:34"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   3
         Left            =   3450
         TabIndex        =   48
         Tag             =   "AIRB"
         Top             =   2295
         Width           =   1035
      End
      Begin VB.Label lblFlightOutboundTime 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "12:34"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   2
         Left            =   3450
         TabIndex        =   47
         Tag             =   "ETDI"
         Top             =   1815
         Width           =   1035
      End
      Begin VB.Label lblFlightOutboundTime 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "12:34"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   1
         Left            =   1320
         TabIndex        =   46
         Tag             =   "OFBL"
         Top             =   2295
         Width           =   1035
      End
      Begin VB.Label lblFlightOutboundTime 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "12:34"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   0
         Left            =   1320
         TabIndex        =   45
         Tag             =   "STOD"
         Top             =   1815
         Width           =   1035
      End
      Begin VB.Label lblFlightOutbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "1234"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   2
         Left            =   2340
         TabIndex        =   44
         Tag             =   "DES4"
         Top             =   975
         Width           =   900
      End
      Begin VB.Label lblFlightOutbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "123"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   1
         Left            =   1335
         TabIndex        =   43
         Tag             =   "DES3"
         Top             =   975
         Width           =   810
      End
      Begin VB.Label lblFlightOutbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "SQ 1234 XYZ"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   0
         Left            =   1335
         TabIndex        =   42
         Tag             =   "FLNO"
         Top             =   495
         Width           =   1920
      End
      Begin VB.Label Label12 
         Caption         =   "C/I from:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   4095
         Width           =   1395
      End
      Begin VB.Label Label18 
         Caption         =   "Gate:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2730
         TabIndex        =   19
         Top             =   3180
         Width           =   975
      End
      Begin VB.Line Line4 
         X1              =   120
         X2              =   4575
         Y1              =   2880
         Y2              =   2880
      End
      Begin VB.Label Label17 
         Caption         =   "ETD:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2595
         TabIndex        =   18
         Top             =   1860
         Width           =   855
      End
      Begin VB.Label Label10 
         Caption         =   "Targ.Box:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   3630
         Width           =   1305
      End
      Begin VB.Label Label28 
         Caption         =   "To:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   1020
         Width           =   1095
      End
      Begin VB.Label Label16 
         Caption         =   "Flight:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   533
         Width           =   1095
      End
      Begin VB.Label Label15 
         Caption         =   "OffBl:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   2340
         Width           =   975
      End
      Begin VB.Label Label14 
         Caption         =   "ParkSt:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   3180
         Width           =   1215
      End
      Begin VB.Label Label13 
         Caption         =   "STD:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   1860
         Width           =   975
      End
      Begin VB.Label Label11 
         Caption         =   "Airb.:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2595
         TabIndex        =   8
         Top             =   2340
         Width           =   975
      End
      Begin VB.Line Line2 
         X1              =   120
         X2              =   4575
         Y1              =   1560
         Y2              =   1560
      End
   End
   Begin VB.Frame frameInbound 
      Caption         =   "Arrival 02.06.2003"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4635
      Left            =   135
      TabIndex        =   2
      Top             =   4485
      Width           =   4815
      Begin VB.Label lblFlightInbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "123"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   5
         Left            =   1365
         TabIndex        =   51
         Tag             =   "BLT1,BLT2"
         Top             =   3630
         Width           =   2970
      End
      Begin VB.Label lblFlightInbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "123"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   4
         Left            =   3300
         TabIndex        =   50
         Tag             =   "GTA1"
         Top             =   3195
         Width           =   1035
      End
      Begin VB.Label lblFlightInbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "123"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   3
         Left            =   1365
         TabIndex        =   49
         Tag             =   "PSTA"
         Top             =   3195
         Width           =   1035
      End
      Begin VB.Label lblFlightInboundTime 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "12:34"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   3
         Left            =   3465
         TabIndex        =   41
         Tag             =   "ONBL"
         Top             =   2295
         Width           =   1035
      End
      Begin VB.Label lblFlightInboundTime 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "12:34"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   2
         Left            =   3465
         TabIndex        =   40
         Tag             =   "ETAI"
         Top             =   1815
         Width           =   1035
      End
      Begin VB.Label lblFlightInboundTime 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "12:34"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   1
         Left            =   1350
         TabIndex        =   39
         Tag             =   "LAND"
         Top             =   2295
         Width           =   1035
      End
      Begin VB.Label lblFlightInboundTime 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "12:34"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   0
         Left            =   1350
         TabIndex        =   38
         Tag             =   "STOA"
         Top             =   1815
         Width           =   1035
      End
      Begin VB.Label Label6 
         Caption         =   "STA:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   37
         Top             =   1860
         Width           =   975
      End
      Begin VB.Label lblFlightInbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "1234"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   2
         Left            =   2370
         TabIndex        =   36
         Tag             =   "ORG4"
         Top             =   975
         Width           =   900
      End
      Begin VB.Label lblFlightInbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "123"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   1
         Left            =   1365
         TabIndex        =   35
         Tag             =   "ORG3"
         Top             =   975
         Width           =   810
      End
      Begin VB.Label lblFlightInbound 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "SQ 1234 XYZ"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   0
         Left            =   1365
         TabIndex        =   34
         Tag             =   "FLNO"
         Top             =   495
         Width           =   1920
      End
      Begin VB.Label Label5 
         Caption         =   "ParkSt:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   3240
         Width           =   1095
      End
      Begin VB.Label Label9 
         Caption         =   "Belt:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   3660
         Width           =   975
      End
      Begin VB.Label Label25 
         Caption         =   "Gate:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2520
         TabIndex        =   16
         Top             =   3240
         Width           =   975
      End
      Begin VB.Line Line3 
         X1              =   120
         X2              =   4680
         Y1              =   2880
         Y2              =   2880
      End
      Begin VB.Label Label27 
         Caption         =   "From:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   1013
         Width           =   1095
      End
      Begin VB.Line Line1 
         X1              =   120
         X2              =   4680
         Y1              =   1560
         Y2              =   1560
      End
      Begin VB.Label Label8 
         Caption         =   "OnBl:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2640
         TabIndex        =   6
         Top             =   2340
         Width           =   975
      End
      Begin VB.Label Label7 
         Caption         =   "ETA:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2640
         TabIndex        =   5
         Top             =   1860
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "Land:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   2340
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "Flight:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   533
         Width           =   1095
      End
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   120
      TabIndex        =   0
      Top             =   3405
      Width           =   9855
      Begin VB.Label lblJobGridInformation 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Registration"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   6
         Left            =   7230
         TabIndex        =   32
         Tag             =   "Regn"
         Top             =   360
         Width           =   2430
      End
      Begin VB.Label lblJobGridInformation 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "ACT3/ACT5"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   5
         Left            =   1785
         TabIndex        =   31
         Tag             =   "ACT"
         Top             =   360
         Width           =   2955
      End
      Begin VB.Label Label22 
         Caption         =   "A/C Type:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   405
         Width           =   1365
      End
      Begin VB.Label Label1 
         Caption         =   "Registration:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5160
         TabIndex        =   1
         Top             =   405
         Width           =   2055
      End
   End
End
Attribute VB_Name = "frmJobDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdClose_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    Me.Hide
End Sub
 
Public Sub ShowJobDetails()
    Dim llLineNo As Long
    Dim i As Integer
    Dim j As Integer
    Dim ilIdx As Integer
    Dim strAftUrnos As String
    Dim strAftUrno As String
    Dim strJobUrno As String
    Dim strLineNo As String
    Dim strADID As String

    Dim blFillInbound As Boolean
    Dim blFillOutbound As Boolean

    EmptyAll

    llLineNo = frmMain.TAB_JobInformation.GetCurrentSelected

    If llLineNo > -1 Then
        If Len(frmMain.TAB_JobInformation.GetFieldValue(llLineNo, "ARR")) > 0 Then
            blFillInbound = True
        End If
        If Len(frmMain.TAB_JobInformation.GetFieldValue(llLineNo, "DEP")) > 0 Then
            blFillOutbound = True
        End If

        ' filling the labels just displaying the information of the job grid
        For i = 0 To lblJobGridInformation.count - 1 Step 1
            lblJobGridInformation.Item(i).Caption = frmMain.TAB_JobInformation.GetFieldValue(llLineNo, lblJobGridInformation.Item(i).Tag)
        Next i

        ' getting the flight URNOs and fill the flight depending fields
        Dim ilCntAftUrnos As Integer
        strJobUrno = frmMain.TAB_JobInformation.GetLineTag(llLineNo)
        strAftUrnos = frmData.GetAftUrnosByJob(strJobUrno)
        ilCntAftUrnos = ItemCount(strAftUrnos, ",")
        For i = 1 To ilCntAftUrnos Step 1
            strAftUrno = GetItem(strAftUrnos, i, ",")
            strLineNo = frmData.TabAFT.GetLinesByIndexValue("URNO", strAftUrno, 0)
            If IsNumeric(strLineNo) = True Then
                If i = 1 And blFillInbound = True Then
                    FillCollectionText lblFlightInbound, frmData.TabAFT, CLng(strLineNo)
                    FillCollectionTime lblFlightInboundTime, frmData.TabAFT, CLng(strLineNo)
                Else
                    If blFillOutbound = True Then
                        If ilCntAftUrnos = 1 Or i = 2 Then
                            FillCollectionText lblFlightOutbound, frmData.TabAFT, CLng(strLineNo)
                            FillCollectionTime lblFlightOutboundTime, frmData.TabAFT, CLng(strLineNo)
                        End If
                    End If
                End If
            End If
        Next i

        ' set frame caption


        ' fill the job remark
        lblJobRemark.Caption = frmData.GetJobFlightRemark(frmMain.TAB_JobInformation.GetLineTag(llLineNo))

        ' fill the working group and officer
        lblWorkGroup.Caption = frmData.GetTeamCodeByJob(strJobUrno)
        
    End If

    Me.Show
End Sub

Private Sub EmptyAll()
    Dim i As Integer

    frameJobDetails.Caption = ""
    frameInbound = "Arrival"
    frameOutbound = "Departure"
    lblJobRemark.Caption = ""
    lblWorkGroup.Caption = ""

    For i = 0 To lblJobGridInformation.count - 1 Step 1
        lblJobGridInformation.Item(i).Caption = ""
    Next i

    For i = 0 To lblFlightInbound.count - 1 Step 1
        lblFlightInbound.Item(i).Caption = ""
    Next i

    For i = 0 To lblFlightInboundTime.count - 1 Step 1
        lblFlightInboundTime.Item(i).Caption = ""
    Next i

    For i = 0 To lblFlightOutbound.count - 1 Step 1
        lblFlightOutbound.Item(i).Caption = ""
    Next i

    For i = 0 To lblFlightOutboundTime.count - 1 Step 1
        lblFlightOutboundTime.Item(i).Caption = ""
    Next i
End Sub

Private Sub FillCollectionText(ByRef rControls, ByRef rTab As TABLib.Tab, lLineNo As Long)
    Dim i As Integer
    Dim j As Integer
    Dim strField As String
    Dim strValue As String
    Dim strTmp As String

    For i = 0 To rControls.count - 1 Step 1
        strTmp = ""
        For j = 1 To ItemCount(rControls.Item(i).Tag, ",")
            strField = GetItem(rControls.Item(i).Tag, j, ",")
            strValue = rTab.GetFieldValue(lLineNo, strField)
            If Len(strValue) > 0 And Len(strTmp) > 0 Then
                strTmp = strTmp & ","
            End If
            strTmp = strTmp & strValue
        Next j
        rControls.Item(i).Caption = strTmp
    Next i
End Sub

Private Sub FillCollectionTime(ByRef rControls, ByRef rTab As TABLib.Tab, lLineNo As Long)
    Dim i As Integer
    Dim dateTmp As Date
    Dim strTmp As String

    For i = 0 To rControls.count - 1 Step 1
        strTmp = rTab.GetFieldValue(lLineNo, rControls.Item(i).Tag)
        If Len(strTmp) > 0 Then
            dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
            rControls.Item(i).Caption = Format(dateTmp, "hh:mm")
        End If
    Next i
End Sub

